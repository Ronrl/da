#pragma once
#define DA_CLIENT_CLOSE_EVENT _T("Global\\DA_Client_close_event")
#define PACKET_LENGTH 1024
#define INTERACTION_SERVICE_MAP_NAME _T("Global\\DeskAlertsServiceMapping")
#define INTERACTION_SERVICE_EVENT_NAME _T("Global\\DeskAlertsServiceInteractionEvent")
#define INTERACTION_SERVICE_READY_EVENT_NAME _T("Global\\DeskAlertsServiceReady")
#define INTERACTION_SERVICE_MAP_READY_EVENT_NAME _T("Global\\DeskAlertsServiceMappingReady")
#define HASH_CONST ("fGhe2")

#define MAX_KEY_LENGTH (255+1)
#define MAX_VALUE_NAME (16383+1)

#define REG_KEY 101

#define NO_MENU						(0x1)
#define NO_ALERTLOADER				(0x2)
#define NO_UPDATE					(0x4)
#define NO_DATABASE					(0x8)
#define NO_ALERTS_RESTORE			(0x10)
#define NO_REGISTRATION				(0x20)
#define NO_SCREENSAVER				(0x40)
#define NO_FIRST_START				(0x80)
#define DISABLE_EXTERNAL_LINKS		(0x100)
#define ALLOW_MULTIPLE_INSTANCES	(0x200)
#define NO_WALLPAPER				(0x400)

namespace InteractionConstants
{
	typedef struct{
		WCHAR m_src[MAX_PATH];
		WCHAR m_dest[MAX_PATH];
		WCHAR m_exeName[MAX_PATH];
		WCHAR m_baseName[MAX_PATH];
		DWORD sid;
		WCHAR m_version[MAX_PATH];
	} UPDATEDATA, *PUPDATEDATA;

	typedef struct {
		WCHAR m_path[MAX_KEY_LENGTH];
		WCHAR m_name[MAX_VALUE_NAME];
		WCHAR m_format[MAX_PATH];
		WCHAR m_wow6432[MAX_PATH];
		WCHAR *m_strVal;
		ULONG m_strLen;
		DWORD m_dwVal;
		ULONGLONG m_qwVal;
		DWORD m_type;
	} REGISTRYDATA, *PREGISTRYDATA;

	typedef struct {
		DWORD pid;
		long encoding;
		BOOL UTC;
		BOOL utf8;
		INT64 history_id;
		int windowsCountDelta;
		RECT windowRect;
		WCHAR *param;
		WCHAR *viewName;
		WCHAR *url;
		WCHAR *post;
		WCHAR *expire;
		WCHAR *acknowledgement;
		WCHAR *create_date;
		WCHAR *title;
		WCHAR *width;
		WCHAR *height;
		WCHAR *position;
		WCHAR *autoclose;
		WCHAR *topTemplateHTML;
		WCHAR *bottomTemplateHTML;
		WCHAR *visible;
		WCHAR *cdata;
		WCHAR *ticker;
		WCHAR *userid;
		WCHAR *alertid;
		WCHAR *survey;
		WCHAR *schedule;
		WCHAR *recurrence;
		WCHAR *urgent;
		WCHAR *searchTerm;
		WCHAR *desktopName;
	} WINDOWDATA, *PWINDOWDATA;


	typedef struct {
		INT64 alertId;
		DWORD pid;
	} CLOSEWINDOWDATA, *PCLOSEWINDOWDATA;


	typedef struct{
		typedef struct {
			DWORD isLast;
			DWORD length; // not use, reserved;
		} HEADER, *PHEADER;
		HEADER header;
		unsigned char data[PACKET_LENGTH];
	} PACKET, *PPACKET;

	typedef enum RequestDataType {
		REQUESTDATATYPE_UNKNOWN,
		REQUESTDATATYPE_UPDATE,
		REQUESTDATATYPE_REGISTRY,
		REQUESTDATATYPE_WINDOW,
		REQUESTDATATYPE_CLOSEWINDOW
	} REQUESTDATAPACKET;

	typedef struct {
		typedef struct {
			unsigned int hash[6];
			unsigned int length;
			RequestDataType dataType;
		} HEADER, *PHEADER;
		HEADER header;
		char *data;
	} REQUESTPACKET, *PREQUESTPACKET;

	#define getNextString(x) windowData->x + (wcslen(windowData->x)+1);
	PWINDOWDATA parseWindowData(PREQUESTPACKET packet);

}

using namespace InteractionConstants;
