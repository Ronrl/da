// AlertAPP.h: interface for the CAlertAPP class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TESTAPP_H__3D916FFB_17FD_424F_B91D_4CF63A10DF49__INCLUDED_)
#define AFX_TESTAPP_H__3D916FFB_17FD_424F_B91D_4CF63A10DF49__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DeskAlerts.h"
#include "ProtocolImpl.h"

class CAlertSink :
	public PassthroughAPP::CInternetProtocolSinkWithSP<CAlertSink>,
	public IHttpSecurity,
	public IAlertSink
{
	typedef PassthroughAPP::CInternetProtocolSinkWithSP<CAlertSink> BaseClass;
public:

	BEGIN_COM_MAP(CAlertSink)
		COM_INTERFACE_ENTRY(IHttpSecurity)
		COM_INTERFACE_ENTRY(IAlertSink)
		COM_INTERFACE_ENTRY_CHAIN(BaseClass)
	END_COM_MAP()

	BEGIN_SERVICE_MAP(CAlertSink)
		SERVICE_ENTRY(IID_IHttpSecurity)
	END_SERVICE_MAP()

	CAlertSink() : m_phwnd(NULL), m_skipHttpsCheck(FALSE) {}

private:
	//IAlertSink
	HWND m_phwnd;
	BOOL m_skipHttpsCheck;

public:
	//IAlertSink
	STDMETHOD(SetParams)(
		/* [in] */ HWND hwnd,
		/* [in] */ BOOL skipHttpsCheck);

public:
	//IWindowForBindingUI
	STDMETHOD(GetWindow)(
		/* [in] */ REFGUID rguidReason,
		/* [out] */ HWND *phwnd);


public:
	//IHttpSecurity
	STDMETHOD(OnSecurityProblem)(
		/* [in] */ DWORD dwProblem);
};

class CAlertAPP;
typedef PassthroughAPP::CustomSinkStartPolicy<CAlertAPP, CAlertSink>
	AlertStartPolicy;

class CAlertAPP :
	public PassthroughAPP::CInternetProtocol<AlertStartPolicy>
{
};

#endif // !defined(AFX_TESTAPP_H__3D916FFB_17FD_424F_B91D_4CF63A10DF49__INCLUDED_)
