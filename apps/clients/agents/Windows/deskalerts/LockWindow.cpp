#include "StdAfx.h"

#include "LockWindow.h"
#include "mvc.h"
#include <algorithm>

CLockWindow* CLockWindow::_self=NULL;
volatile LONG CLockWindow::_refcount=0;
CComAutoCriticalSection CLockWindow::_cs;
vector<HWND> CLockWindow::m_windows;

void CLockWindow::_AddWindow(HWND windowToLock)
{
	if(!m_windowToLock)
	{
		m_windowToLock = windowToLock;
		SetWindowPos(windowToLock, 0, 0, 0, 0, SWP_NOMOVE|SWP_NOSIZE|SWP_NOACTIVATE);
	}
	m_windows.push_back(windowToLock);
}


LRESULT CLockWindow::OnClose(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled)
{
	bHandled = TRUE;
	return S_OK;
}

LRESULT CLockWindow::OnSetFocus(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	return S_OK;
}


CLockWindow::CLockWindow() : m_windowToLock(NULL)
{
	const int left = GetSystemMetrics(SM_XVIRTUALSCREEN);
	const int right = GetSystemMetrics(SM_CXVIRTUALSCREEN) + left;
	const int top = GetSystemMetrics(SM_YVIRTUALSCREEN);
	const int bottom = GetSystemMetrics(SM_CYVIRTUALSCREEN) + top;

	CRect rect(left,top,right,bottom);
	//::GetWindowRect(GetDesktopWindow(), &rect);

	HWND parent = (HWND)_iAlertModel->getProperty(CString(_T("mainHWND")));
	if(!parent) parent = GetDesktopWindow();

	if (Create(parent, &rect, NULL, WS_POPUP, WS_EX_TOPMOST|WS_EX_LAYERED))
	{
		SetLayeredWindowAttributes(m_hWnd, NULL, 100, LWA_ALPHA);
		ShowWindow(SW_SHOWNA);
		UpdateWindow();
		ModifyStyleEx(WS_EX_APPWINDOW, 0);
		//GlobalAddAtom
		RegisterHotKey(m_hWnd, 1, MOD_ALT, VK_TAB);
		RegisterHotKey(m_hWnd, 2, MOD_ALT, VK_ESCAPE);
		RegisterHotKey(m_hWnd, 3, MOD_ALT|MOD_SHIFT, VK_TAB);
		RegisterHotKey(m_hWnd, 4, MOD_ALT|MOD_SHIFT, VK_ESCAPE);

		/*UINT uOldValue;
		SystemParametersInfo(SPI_SCREENSAVERRUNNING, TRUE, &uOldValue, 0);*/
	}
	EnableWindow(FALSE);
}

CLockWindow::~CLockWindow(void)
{
	UnregisterHotKey(m_hWnd, 1);
	UnregisterHotKey(m_hWnd, 2);
	UnregisterHotKey(m_hWnd, 3);
	UnregisterHotKey(m_hWnd, 4);

	/*UINT uOldValue;
	SystemParametersInfo(SPI_SCREENSAVERRUNNING, FALSE, &uOldValue, 0);*/

	DestroyWindow();
}

void CLockWindow::AddWindow(HWND wnd)
{
	_cs.Lock();
	release_try
	{
		if(!_self) _self = new CLockWindow();
		_refcount++;
		_self->_AddWindow(wnd);
	}
	release_catch_tolog(_T("LockWindow add exception"));
	_cs.Unlock();
}

void CLockWindow::RemoveWindow(HWND wnd)
{
	_cs.Lock();
	release_try
	{
		vector<HWND>::const_iterator it = std::find(m_windows.begin(), m_windows.end(), wnd);
		if(it != m_windows.end())
		{
			m_windows.erase(it);
			if(!--_refcount)
			{
				delete_catch(_self);
				_self=NULL;
			}
		}
	}
	release_catch_tolog(_T("LockWindow remove exception"));
	_cs.Unlock();
}