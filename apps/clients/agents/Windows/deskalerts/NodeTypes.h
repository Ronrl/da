//////////////
//Node Types//
//////////////


#define NODETYPE int

#define NODETYPE_BASE 0xFF

#define T_TOOLBAR (NODETYPE_BASE + 0x01)
#define T_BUTTON (NODETYPE_BASE + 0x02)
#define T_COMBO (NODETYPE_BASE + 0x03)
#define T_MENU (NODETYPE_BASE + 0x04)
#define T_MENU_ITEM (NODETYPE_BASE + 0x05)