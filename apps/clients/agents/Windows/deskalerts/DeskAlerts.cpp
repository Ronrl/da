//
//  DeskAlerts.cpp
//  DeskAlerts Client
//  Common module
//
//  Copyright Softomate 2006-2009. All rights reserved.
//

#include "stdafx.h"
#include "CmdDeskAlerts.h"
#include "alertcontroller.h"
#include "AlertModel.h"

CAlertController *pController;
_bstr_t bsKey = _bstr_t("");
_bstr_t bsRootPath = _bstr_t("");

extern "C" void CreateAlerts(HWND m_Wnd)
{
	release_try
	{
		CAlertUtils::Key = bsKey;
		CAlertController::m_sAppDataPath = bsRootPath.Detach();
	}
	release_end_try
	pController = new CAlertController();
	pController->setMainHWND(m_Wnd);
	pController->createComponent();
}


extern "C" void InitializeAlerts(HWND m_Wnd, DWORD initFlags, HWND& sysMenuHWND)
{
	release_try
	{
		CAlertUtils::Key = bsKey;
		CAlertController::m_sAppDataPath = bsRootPath.Detach();
	}
	release_end_try
	pController = new CAlertController();
	pController->setMainHWND(m_Wnd);
	pController->setInitFlags(initFlags);
	pController->createComponent();
	sysMenuHWND = pController->getSysMenuHWND();
}

extern "C" void DestroyAlerts()
{
	if (pController)
	{
		pController->destroyComponent();
		delete_catch(pController);
	}
}

#pragma warning(push)
#pragma warning(disable:4190)
extern "C" CString Invoke(CString sFunc, CString sParam)
{
	if(sFunc == _T("GetProperty"))
	{
		if(_iAlertModel)
			return _iAlertModel->getPropertyString(sParam);
		else
		{
			InterlockedExchangePointer((void**)&_iAlertModel, new CAlertModel());
			_iAlertModel->createComponent();
			sParam = _iAlertModel->getPropertyString(sParam);
		}
	}

	if(sFunc == _T("StandBy"))
	{
		if(_iAlertController)
		{
			if (sParam == _T("On"))	//-- Turn On Stand By
			{
				CStandByEvent disEvent(
					CString(_T("enable")),
					CString(_T("standby")),
					CString(_T("standby")));
				_iAlertController->actionPerformed(disEvent);
			}
			else				//-- Turn Off
			{
				CStandByEvent disEvent(
					CString(_T("disable")),
					CString(_T("standby")),
					CString(_T("standby")));
				_iAlertController->actionPerformed(disEvent);
			}
			return sParam;
		}
	}
	if (sFunc == _T("Update"))
	{
		if(_iAlertController)
		{
			SysMenuEvent disEvent(
				CString(_T("update")),
				CString(_T("autoupdate")),
				CString(_T("update")));
			_iAlertController->actionPerformed(disEvent);
			return sParam;
		}
	}
	if (sFunc == _T("History"))
	{
		if(_iAlertController)
		{
			SysMenuEvent disEvent(
				CString(_T("select")),
				CString(_T("sysmenu")),
				CString(_T("history"))
				);
			release_try
			{
				_iAlertController->actionPerformed(disEvent);
			}
			release_catch_tolog(_T("History action performed exception"))
		}
		return sFunc;
	}
	if (sFunc == _T("AddTaskbarIcon"))
	{
		CSysMenu *menu = (CSysMenu*)_iAlertView->getViewByName(CString(_T("sysmenu")));
		if (menu != NULL)
			menu->ChangeIcon(0, NULL, NIM_ADD);
		return sParam;
	}
	if(sFunc == _T("SetKey"))
	{
		bsKey = sParam;
		return sParam;
	}
	if(sFunc == _T("SetRootPath"))
	{
		bsRootPath = sParam;
		return sParam;
	}
	// in other case SetProperty
	if (_iAlertModel)
		_iAlertModel->setPropertyString(sFunc, sParam);

	return sParam;
}
#pragma warning(pop)

extern "C" void InvokeChar(TCHAR *sFunc, TCHAR *sParam)
{
	Invoke(CString(sFunc), CString(sParam));
}

#pragma warning(push)
#pragma warning(disable:4100)
PTL_BEGIN_MODULE()
	//DETERMINE_COMMAND(_T("ALERTS_OLD"),CmdDeskAlerts)
PTL_END_MODULE()
#pragma warning(pop)

PTL_STANDARD_PLUGIN_INTERFACE (); //macross includes the text below
