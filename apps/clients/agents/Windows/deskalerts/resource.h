//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by DeskAlerts.rc
//
#define IDI_SYSDEF                      5
#define IDD_ABOUTBOX                    100
#define IDD_ALERTS_DLG                  105
#define IDR_MAINFRAME                   128
#define IDI_ICON1                       201
#define IDI_ICON2                       207
#define IDC_EDIT_LOGIN                  1000
#define IDC_EDIT_RECEIVE                1001
#define IDC_CHECK_SOUND                 1002
#define IDC_EDIT_PASWD                  1004

// Next default values for new objects
//
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        210
#define _APS_NEXT_COMMAND_VALUE         32775
#define _APS_NEXT_CONTROL_VALUE         1004
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
