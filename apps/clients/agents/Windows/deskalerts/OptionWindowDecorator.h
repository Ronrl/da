#pragma once

#include "IInclude.h"

#include "HTMLView.h"

class COptionWindowDecorator
{
public:
	COptionWindowDecorator(CHTMLView& _cHTMLView);
	~COptionWindowDecorator(void);

	void createOptionPage();
	bool show();

	CHTMLView& cHTMLView;
};
