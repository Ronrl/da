//
//  InstallUninstallUpdate.h
//  DeskAlerts Client
//  Common module
//
//  Copyright Softomate 2006-2009. All rights reserved.
//

#pragma once

#include ".\alertcontroller.h"
#include "stdafx.h"
#include <Setupapi.h>
//#include "globalfunc.h"


void install(HINSTANCE hInstance);
void uninstall(CString reason);

void update(BOOL manualUpdate);


HRESULT CreateShortcut(/*in*/ LPCTSTR lpszFileName,
					   /*in*/ LPCTSTR lpszDesc,
					   /*in*/ LPCTSTR lpszShortcutPath);


UINT CALLBACK CabinetCallback(
							  PVOID pCtx,
							  UINT uNotify,
							  UINT_PTR uParam1,
							  UINT_PTR uParam2
							  );


DWORD ExpandCabinet(
					IN PCTSTR pszCabinetPath,
					OUT PTSTR pszSetupPath
					);

BOOL updateToolbar(BOOL manualUpdate = TRUE);
BOOL UpdateToolbar(CString urlOfUpdate, CString urlOfVersion, CString dllName);
void __cdecl UpdaterThread(PVOID pvParam);

void UNregServer();
void Unregistration(CString reason);
int VersionCompare(CString strVer1, CString strVer2);







