#pragma once

#include "IInclude.h"

#include "PropertyStore.h"
#include "ModelStore.h"

class CAlertModel: public IAlertModel
{
public:
	CAlertModel(void);
	~CAlertModel(void);

	void createComponent();
	void destroyComponent();

public:

	//Property
	void addPropertyListener(IPropertyListener* newListener);
	void removePropertyListener(IPropertyListener* newListener);
	CString getPropertyString(CString& promertyName, CString *def = NULL, const int encode = 0, const bool fixURI = false);
	void setPropertyString(CString& promertyName,CString& promertyValue);
	CString buildPropertyString(CString& promertyVal, const int encode = 0, const bool fixURI = false);

	DWORD getProperty(CString& promertyName);
	void setProperty(CString& promertyName,DWORD promertyValue);

	//Model
	void addModelListener(IModelListener* newListener);
	void removeModelListener(IModelListener* newListener);
	NodeALERTS& getModel(CString& promertyName);

	//Alert
	void addAlertListener(IAlertListener* newListener);
	void removeAlertListener(IAlertListener* newListener);
	LPVOID getAlert(CString& promertyName);
	//void saveAlert(CString& fileName);
	int getStatesIndex();

	void setImageList(HIMAGELIST);
	HIMAGELIST getImageList();

	void setHotImageList(HIMAGELIST);
	HIMAGELIST getHotImageList();

	bool EnableActiveDirectory();
private:
	HWND mainHWND;
	CPropertyStore cPropertyStore;
	CModelStore cModelStore;

	WTL::CImageList imageList;
	WTL::CImageList hotImageList;



};
