#pragma once
#include <tchar.h>
#include <Windows.h>
#include <vector>
//#include "InetUtils.h"

#define VERSION_SIZE 32			 /* max size of version string */
#define MAX_URL_SIZE 256

#define VERSION_URL "/api/v1/client/version"
#define SLOT_URL "/api/v1/client/slot"
#define DOWNLOAD_FILE_URL "/api/v1/client/download"
#define DELETE_SLOT_URL "/api/v1/client/slot/delete"

#define INSTALL_ATTEMPTS_REG_KEY "SOFTWARE\\ALERTS00001\\DeskAlerts\\autoupdate"
#define MAX_ATTEMPT_COUNT 2

#define TIMER_CHECK_VERSION (1 * 1000 * 60 * 60) /*  check version call (sec)*/

#define TIMER_CHECK_SLOT (100 * 1000) /* check download permission call (sec)*/
//#define TIMER_CHECK_SLOT_NEXT (30 * 1000)  /* next check download permission call (sec)*/



class UpdateChecker
{
typedef BOOL(UpdateChecker::*PF)();

public:
	static /*UpdateChecker*/void  start(CString baseServerURL, CString currentVersion, CString deskbarId, unsigned pullPeriod,
		void (*callback)(CString url, CString version));
//    static UpdateChecker * getInstance() {
//        if(!p_instance)           
//            p_instance = new UpdateChecker();
//        return p_instance;
	CString getCurrentVersion();
	
private:
	UpdateChecker(void);
	UpdateChecker(CString baseServerUrl, CString currentVersion, CString deskbarId, unsigned pullPeriod,  void (*callback)(CString url, CString version));
	~UpdateChecker(void);

	unsigned operationPeriod_;
	unsigned pullPeriod_;
	static UpdateChecker checker_;

	void (*callback_)(CString url, CString version);
	CString host_;
	CString baseServerUrl_;
	CString versionUrl_;
	CString slotUrl_;
	CString currentVersion_;
	CString newVersion_;
	CString deskbarId_;
	CString slotId_;
	CString downloadUrl_;

	//rest methods
	BOOL updateAvailCheck();
	BOOL permissionToDownloadingCheck();
	//BOOL downloadFile();
	BOOL deleteSlot();
	BOOL waitUpdateFromService();

	BOOL checkVersionAttemptCount(CString version);

	/*PF  *pOperations_ ;	*/
	std::vector<PF> vOperations_ ;
    unsigned operationIndex_;

	static unsigned WINAPI  checkerThread(PVOID );
	static UpdateChecker *pInstance_;
};

