#pragma once
#include "IInclude.h"
#include "WindowCommand.h"

#define UTM_SHOWMULTIWINDOW (WM_APP+301)
#define UTM_SHOWNEWALERTWINDOW (WM_APP+302)
#define UTM_HIDEMULTIWINDOW (WM_APP+303)
#define UTM_CLOSESCREENSAVER (WM_APP+304)
#define UTM_DESKTOP_CLOSED (WM_APP+305)
#define WM_SHOW_WINDOWS (WM_APP+306)
#define CHECK_SCREENSAVER_TIMER (WM_APP+307)
#define CHECK_LIFETIME_EXPIRATION (WM_APP+308)

class CDesktopParentWindow :
	public CFrameWindowImpl<CDesktopParentWindow>
{
	public:
	DECLARE_WND_CLASS(_T("DeskAlertsWindow"))
	CDesktopParentWindow();
	~CDesktopParentWindow(void);
};

class CDesktopMultiWindow :
	public CFrameWindowImpl<CDesktopMultiWindow>,
	public IView,
	public ISwitchDesktopListener
	
{
public:
	enum ERRORS {
		NO_ERRORS,
		DESKTOP_DENIED,
		SCREENSAVER_ACTIVE
	};

	BEGIN_MSG_MAP(CMultiWindowCommand)
//		MESSAGE_HANDLER(WM_WTSSESSION_CHANGE, OnSessionChanged)
		MESSAGE_HANDLER(WM_SHOW_WINDOWS, OnShowWindows)
		MESSAGE_HANDLER(WM_TIMER, OnTimer)
	END_MSG_MAP()
//	LRESULT OnSessionChanged(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnShowWindows(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnTimer(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	CDesktopMultiWindow(NodeALERTS::NodeVIEWS::NodeWINDOW* param);
	~CDesktopMultiWindow(void);
	bool show(CString param, shared_ptr<showData> ex, BOOL isNewAlert = FALSE);
	void showAndCheckError(CString param, shared_ptr<showData> ex, DWORD *errorCode, BOOL isNewAlert = FALSE);
	void hide(CString param);

	struct desktopThreadData {
		HANDLE readyEvent;
		NodeALERTS::NodeVIEWS::NodeWINDOW* data;
		HDESK desktop;
	};

	LRESULT OnSwitchDesktop(CString *closedDesktopName, CString *openedDesktopName);

private:
	static DWORD WINAPI desktopThread(LPVOID pv);

	BOOL desktopThreadExists(CString desktopName);
	DWORD addDesktopThread(HDESK hDesktop, CString desktopName, CString param, shared_ptr<showData> ex );
	DWORD getDesktopThreadId(CString desktopName);

	DWORD m_desktopThredId;
	CMultiWindowCommand* winCmd;

	CComAutoCriticalSection m_csThreads;
	map<CString,DWORD> m_desktopThreads;

	NodeALERTS::NodeVIEWS::NodeWINDOW data;

	CComAutoCriticalSection m_csWindows;
	std::vector<shared_ptr<showData>>* m_windowsForFutureShowing;

	static BOOL IsScreensaverActive();
	void showSavedWindows(CString * openedDesktopName);
};
