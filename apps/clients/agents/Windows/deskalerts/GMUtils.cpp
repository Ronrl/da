//
//  GMUtils.cpp
//  DeskAlerts Client
//  Common module
//
//  Copyright Softomate 2006-2009. All rights reserved.
//

#include "StdAfx.h"
#include "GMUtils.h"
#include <wchar.h>

CGMUtils::CGMUtils(void)
{
	//sRegistryKey = _T("CGM00001");
}

CGMUtils::~CGMUtils(void)
{
}

HKEY  CGMUtils::GetCompanyRegistryKey()
{
// 	HKEY hSoftKey = NULL;
// 	HKEY hCompanyKey = NULL;
// 	if (RegOpenKeyEx(HKEY_CURRENT_USER, _T("software"), 0, KEY_WRITE|KEY_READ,
// 		&hSoftKey) == ERROR_SUCCESS)
// 	{
// 		DWORD dw;
// 		RegCreateKeyEx(hSoftKey, _T("CGM00001"), 0, REG_NONE,
// 			REG_OPTION_NON_VOLATILE, KEY_WRITE|KEY_READ, NULL,
// 			&hCompanyKey, &dw);
// 	}
// 	if (hSoftKey != NULL)
// 		RegCloseKey(hSoftKey);

	CRegKey hKey;
	if (hKey.Create(HKEY_CURRENT_USER,CGMUtils::GetCompanyKey()) != ERROR_SUCCESS)
	{
		//::MessageBox(0,_T("Error 45"),_T("GetCompanyRegistryKey"),0);
	}
	return hKey.Detach();
}

PSID CGMUtils::GetCurrentUserSid (PTOKEN_USER & usr)
{
	PSID pRet = NULL;
	HANDLE hToken = NULL, hProcess = GetCurrentProcess();

	DWORD dwSize = 0;

	if(OpenProcessToken(hProcess, TOKEN_QUERY, &hToken))
	{
		GetTokenInformation(hToken, TokenUser, usr, 0, &dwSize);
		if (GetLastError() == ERROR_INSUFFICIENT_BUFFER)
		{
			usr = (PTOKEN_USER)HeapAlloc(GetProcessHeap(), 0, dwSize+4);
            if (usr != nullptr)
            {
                if (GetTokenInformation(hToken, TokenUser, usr, dwSize + 4, &dwSize))
                {
                    pRet = usr->User.Sid;
                }
            }
		}
		CloseHandle(hToken);
	}
	return pRet;
}

CString CGMUtils::GetCurrentUserSidString()
{
	PTOKEN_USER usr = NULL;
	int n = 0;
	PSID pSid = NULL;
	CString strRet, tmp;
	PSID_IDENTIFIER_AUTHORITY sia;
	PUCHAR pCnt = NULL;
	PDWORD pSub = NULL;

	pSid = GetCurrentUserSid(usr);
	if(pSid && IsValidSid(pSid))
	{
		sia = GetSidIdentifierAuthority(pSid);
		if (sia->Value[0] != 0 || sia->Value[1] != 0)
			strRet.Format(_T("%s0x%.2x%.2x%.2x%.2x%.2x%.2x-"), _T("S-1-"),
			sia->Value[0], sia->Value[1], sia->Value[2], sia->Value[3],
			sia->Value[4], sia->Value[5]);
		else
		{
			n = (sia->Value[2] << 8);
			n = ((n + sia->Value[3]) << 8);
			n = ((n + sia->Value[4]) << 8);
			n = (n + sia->Value[5]);
			strRet.Format(_T("%s%d"), _T("S-1-"), n);
		}
		pCnt = GetSidSubAuthorityCount(pSid);
		for(n=0; n<(*pCnt); n++)
		{
			pSub = GetSidSubAuthority(pSid, n);
			if(GetLastError())
				break;
			tmp.Format(_T("-%u"), (*pSub));
			strRet += tmp;
		}
	}
	HeapFree(GetProcessHeap(), 0, usr);
	return strRet;
}

CString CGMUtils::GetPreCompanyKey()
{
	CString str = L"Software";
	if (IsWindowsVistaOrGreater())
	{
		CString sid = GetCurrentUserSidString();
		str += L"\\Microsoft\\Internet Explorer\\InternetRegistry\\REGISTRY\\USER\\" + sid;
		str += L"\\Software";
	}
	return str;
}

CString CGMUtils::GetCompanyKey()
{
	return GetPreCompanyKey() + L"\\" + _T("TrafficTactics");
}
