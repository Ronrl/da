#pragma once

#include "IComponent.h"
#include "IActionListeners.h"
#include "OptionConf.h"
#include "ServiceBridge.h"
#include "SwitchDesktopHandler.h"

class IView;
class CWallpaperManager;
class CScreensaverManager;
class CLockscreenManager;
class CJAClient;
class CJAClient_v2;

class IAlertController: public IActionListener,IAlertListener,IPropertyListener,IModelListener
{
public:
	virtual ServiceBridge* getServiceBridge()=0;
	virtual CSwitchDesktopHandler* getSwitchDesktopHandler()=0;
	virtual CWallpaperManager* getWallpaperManager()=0;
	virtual CScreensaverManager* getScreensaverManager()=0;
	virtual CLockscreenManager* getLockscreenManager()=0;
	virtual CJAClient* getJAClient()=0;
	virtual CJAClient_v2* getJAClient_v2() = 0;
	virtual int getInitFlags() = 0;
	virtual void setInitFlags(int initFlags) = 0;
	virtual void refreshState() = 0;
	virtual void loadAppCredentials() = 0;
};


class IAlertModel: public IComponent
{
public:

	virtual void addPropertyListener(IPropertyListener* newListener) = 0;
	virtual void removePropertyListener(IPropertyListener* newListener) = 0;

	virtual void addAlertListener(IAlertListener* newListener) = 0;
	virtual void removeAlertListener(IAlertListener* newListener) = 0;

	virtual void addModelListener(IModelListener* newListener) = 0;
	virtual void removeModelListener(IModelListener* newListener) = 0;


	//Property
	virtual CString getPropertyString(CString& promertyName, CString *def = NULL, const int encode = 0, const bool fixURI = false) = 0;
	virtual void setPropertyString(CString& promertyName,CString& promertyValue) = 0;
	virtual CString buildPropertyString(CString& promertyVal, const int encode = 0, const bool fixURI = false) = 0;

	virtual DWORD getProperty(CString& promertyName) = 0;
	virtual void setProperty(CString& promertyName,DWORD promertyValue) = 0;

	//Model
	virtual NodeALERTS& getModel(CString& promertyName) = 0;

	//Alert
	virtual LPVOID getAlert(CString& promertyName) = 0;

	//virtual void saveAlert(CString& fileName) = 0;

	virtual void setImageList(HIMAGELIST) = 0;
	virtual HIMAGELIST getImageList() = 0;

	virtual void setHotImageList(HIMAGELIST) = 0;
	virtual HIMAGELIST getHotImageList() = 0;
	virtual int getStatesIndex() = 0;

};

struct showData {
	showData() : encoding(CP_UTF8), UTC(TRUE), utf8(TRUE), history_id(-1), windowsCountDelta(0)
	{
		windowRect.top = 0L;
		windowRect.bottom = 0L;
		windowRect.left = 0L;
		windowRect.right = 0L;
	}
	CString url;
	CString post;
	long encoding;
	CString expire;
	CString acknowledgement;
	CString create_date;
	CString title;
	CString width;
	CString height;
	CString position;
	CString docked;
	CString autoclose;
	CString topTemplateHTML;
	CString bottomTemplateHTML;
	BOOL UTC;
	BOOL utf8;
	sqlite_int64 history_id;
	CString visible;
	CString cdata;
	CString ticker;
	CString userid;
	CString alertid;
	CString survey;
	CString schedule;
	CString recurrence;
	CString urgent;
	CString searchTerm;
	int windowsCountDelta;
	CString desktopName;
	CString unread;
    CString Class;
	CString resizable;
	CString self_deletable;
	RECT windowRect;
	CString date;
	CString to_date;
	CString hide_close;
};

class IAlertView: public IAlertListener,IPropertyListener,IModelListener,IComponent
{
public:
	virtual void showViewByName(CString& viewName,CString& param, shared_ptr<showData> ex, BOOL isNewAlert = FALSE) = 0;
};

class IAlertView2: public IAlertView
{
public:
	virtual bool showViewByName2(CString& viewName,CString& param, shared_ptr<showData> ex, BOOL isNewAlert = FALSE) = 0;
};

class IAlertView3: public IAlertView2
{
public:
	virtual IView* getViewByName(CString& viewName) = 0;
	virtual void closeExpiredAlerts() = 0;
};

class IMVCCommand
{
public:
	virtual CString getName() = 0;
	virtual void run(IActionEvent& event) = 0;
	virtual void stop() = 0;
	virtual BOOL isCanStart() = 0;
	virtual BOOL isAutoStart() = 0;
};

class IView
{
public:
	CString param;
	shared_ptr<showData> data;
	virtual bool show(CString _param, shared_ptr<showData> ex, BOOL isNewAlert = FALSE) = 0;
	virtual void hide(CString _param){};
};

extern IAlertModel* _iAlertModel;
extern IAlertController* _iAlertController;
extern IAlertView3* _iAlertView;
