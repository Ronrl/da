#include "StdAfx.h"
#include "propertystore.h"
#include "DatabaseManagr.h"

#include <lm.h>
#include <Dsgetdc.h>
#include "BrowserUtils.h"
#include "IPCTraceMacros.h"

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#define SECURITY_WIN32
#include <Security.h>

CPropertyStore::CPropertyStore(void) : eDirGetContextName(NULL), eDirGetUserName(NULL), m_ADEDInit(FALSE), m_AD(FALSE), m_AzureAD(FALSE), m_SFAD(FALSE), m_ED(FALSE)
{
	m_cs_propString.Init();
}

std::map<CString,CString> CPropertyStore::loadPropertyFromRegistry(void)
{
	std::map<CString,CString> registryProp;
	CRegKey keyAppID;
	keyAppID.Attach(CAlertUtils::GetAppRegistryKey());
	//TCHAR    achKey[MAX_KEY_LENGTH];   // buffer for subkey name
    //DWORD    cbName;                   // size of name string
    TCHAR    achClass[MAX_PATH] = TEXT("");  // buffer for class name
    DWORD    cchClassName = MAX_PATH;  // size of class string
    DWORD    cSubKeys=0;               // number of subkeys
    DWORD    cbMaxSubKey;              // longest subkey size
    DWORD    cchMaxClass;              // longest class string
    DWORD    cValues;              // number of values for key
    DWORD    cchMaxValue;          // longest value name
    DWORD    cbMaxValueData;       // longest value data
    DWORD    cbSecurityDescriptor; // size of security descriptor
    FILETIME ftLastWriteTime;      // last write time

    DWORD i, retCode;

    TCHAR * achValue = new TCHAR[MAX_VALUE_NAME];
    DWORD cchValue = MAX_VALUE_NAME;

	TCHAR * cValueStr = new TCHAR[MAX_VALUE_NAME];
	DWORD pdwCountStr = MAX_VALUE_NAME;
	if (!keyAppID) return registryProp;
    // Get the class name and the value count.
    retCode = RegQueryInfoKey(
		keyAppID.m_hKey,         // key handle
        achClass,                // buffer for class name
        &cchClassName,           // size of class string
        NULL,                    // reserved
        &cSubKeys,               // number of subkeys
        &cbMaxSubKey,            // longest subkey size
        &cchMaxClass,            // longest class string
        &cValues,                // number of values for this key
        &cchMaxValue,            // longest value name
        &cbMaxValueData,         // longest value data
        &cbSecurityDescriptor,   // security descriptor
        &ftLastWriteTime);       // last write time

    // Enumerate the subkeys, until RegEnumKeyEx fails.
	 if (cValues)
    {
        for (i=0, retCode=ERROR_SUCCESS; i<cValues; i++)
        {
            cchValue = MAX_VALUE_NAME;
            achValue[0] = '\0';

			retCode = RegEnumValue(keyAppID.m_hKey, i,
                achValue,
                &cchValue,
                NULL,
                NULL,
                NULL,
                NULL);


            if (retCode == ERROR_SUCCESS )
            {
				pdwCountStr = MAX_VALUE_NAME;
				cValueStr[0] = '\0';
				if(ERROR_SUCCESS==keyAppID.QueryStringValue(achValue, cValueStr, &pdwCountStr))
				{
					registryProp.insert(pair<CString,CString>(achValue,cValueStr));
				}
            }
        }
    }
	
	CAlertUtils::RecursiveDeleteKey(keyAppID.m_hKey, CString());
	
	delete[] cValueStr;
	delete[] achValue;

	return registryProp;
}


void CPropertyStore::loadProperty(void)
{
	std::map<CString,CString> registryProp = loadPropertyFromRegistry();

	if (registryProp.count(CString("deskalerts_id")))
	{
		release_try
		{
			std::map<CString,CString>::const_iterator it = registryProp.find(CString("deskalerts_id"));
			if (it!=registryProp.end())
			{
				CString deskalertsId = it->second;
				CString appDataPath=CAlertUtils::getAppDataPath(deskalertsId);
				CString oldDbPath=appDataPath+_T("\\history.dat");
				
				CString newDbPath = CAlertUtils::databasePath();

				if (PathFileExists(oldDbPath))
				{
					MoveFile(oldDbPath,newDbPath);
				}
				CAlertUtils::RecursiveDeleteDirectory(appDataPath);
			}
		}
		release_catch_tolog(_T("registryProp while exception"));
	}

	if (CAlertUtils::allUsersAppDataPath(TRUE) != CAlertUtils::allUsersAppDataPath(FALSE))
	{
		CString envAllUsersAppDataPath = CAlertUtils::allUsersAppDataPath(TRUE);
		CString allUsersAppDataPath = CAlertUtils::allUsersAppDataPath(FALSE);	
		CAlertUtils::RecursiveMoveDirectory(envAllUsersAppDataPath,allUsersAppDataPath);
		CAlertUtils::RecursiveDeleteDirectory(envAllUsersAppDataPath);
	}

	CDatabaseManager *databaseManager = CDatabaseManager::shared(true);

	if (databaseManager)
	{
		release_try
		{
			std::map<CString,CString>::iterator it = registryProp.begin();
			while(it != registryProp.end())
			{
				databaseManager->setProperty(it->first,it->second);
				it++;
			}
		}
		release_catch_tolog(_T("registryProp while exception"));

		m_cs_propString.Lock();
		release_try
		{
			databaseManager->loadProperty(m_propString);
		}
		release_catch_tolog(_T("loadProperty exception"));
		m_cs_propString.Unlock();
	}
}

CPropertyStore::~CPropertyStore(void)
{
	CDatabaseManager *db = CDatabaseManager::shared(true);
	if (db) delete_catch(db);
	db = CDatabaseManager::shared(true,true);
	if (db) delete_catch(db);
}

//--------------------------------------------------------------------------------

void CPropertyStore::addPropertyListener(IPropertyListener* newListener)
{
	m_actionListeners.push_back(newListener);
}

void CPropertyStore::removePropertyListener(IPropertyListener* newListener)
{
	std::vector<IPropertyListener*>::iterator it = m_actionListeners.begin();
	while (it != m_actionListeners.end())
	{
		if ( (*it) == newListener )
		{
			m_actionListeners.erase(it);
			break;
		}
		it++;
	}
}
//--------------------------------------------------------------------------------


CString CPropertyStore::buildPropertyString(CString& param, const int encode, const bool fixURI)
{
	CString propertyVal = param;
	int st=0;
	while(1)
	{
		st = propertyVal.Find('%', st);
		if (st<0) break;
		int fn = propertyVal.Find('%', st+1);
		if (fn<0) break;
		CString innerProp = propertyVal.Mid(st+1,fn-st-1);
		CString newPropVal = getPropertyString(innerProp, NULL, encode);
		if (newPropVal != innerProp)
			propertyVal = propertyVal.Left(st) + newPropVal + propertyVal.Mid(fn+1);
		st = fn+1;
	}
	return encode && fixURI ? BrowserUtils::fixURI(propertyVal, encode) : propertyVal;
}

void CPropertyStore::eDirectoryLoad()
{
	if(!eDirGetContextName && !eDirGetUserName && LoadLibraryEx(_T("NETWIN32.DLL"), NULL, LOAD_LIBRARY_AS_DATAFILE))
	{
		HMODULE hDLL = nullptr ;//= LoadLibrary(_T("eDirectory.dll"));
		if(hDLL)
		{
			eDirGetContextName = (PEDGCN) GetProcAddress(hDLL, "eDirGetContextName");
			eDirGetUserName = (PEDGUN) GetProcAddress(hDLL, "eDirGetUserName");
		}
	}
}

void CPropertyStore::ADEDInit()
{
	if (m_ADEDInit)
	{
		return;
	}

	if(EnableEDirectory()) 
	{
		eDirectoryLoad();
	}

	CString domain;
	domain.GetEnvironmentVariable(_T("USERDNSDOMAIN"));
	//domain.GetEnvironmentVariable(_T("USERDOMAIN"));
	m_AD = EnableActiveDirectory() && !domain.IsEmpty();
	m_SFAD = EnableSyncFreeActiveDirectory() && !domain.IsEmpty();
	m_ED = eDirGetUserName && eDirGetContextName && !CString((char*)eDirGetContextName()).IsEmpty();

	m_ADEDInit = TRUE;

	///domain.GetEnvironmentVariable(_T("USERDOMAIN"));

	m_AzureAD = EnableActiveDirectory() && domain == _T("AzureAD") ? TRUE : FALSE;
}


CString CPropertyStore::getPropertyString(CString& propertyName, CString *def, const int encode, const bool fixURI)
{
	CString res = def ? buildPropertyString(*def, encode, fixURI) : propertyName;
	bool need_escape = true, found = true;

	_bstr_t strUser;
	_bstr_t strDomain;


	if(propertyName == "firstURL")
	{
		need_escape = true;
	}
	if (propertyName == _T("user_name"))
	{
		
		bool un_found = false;
		m_cs_propString.Lock();
		release_try
		{
			std::map<CString,CString>::const_iterator it = m_propString.find(_T("user_name"));
			un_found = (it!=m_propString.end());
			if(un_found)
				res = it->second;
		}
		release_catch_tolog(_T("getPropertyString find exception"));
		m_cs_propString.Unlock();
		bool isReadADcredentiansOnlyFromDB = _iAlertModel->getPropertyString(CString(_T("readADcredentiansOnlyFromDB"))).Compare(_T("1")) == 0 ? true : false;
		if(!un_found && !isReadADcredentiansOnlyFromDB)
		{
			if(EnableOtherDirectory())
			{
				res.GetEnvironmentVariable(_T("USERNAME"));
				if( res == "" ){
					DWORD procId = GetCurrentProcessId();
					CAlertUtils::GetUserFromProcess( procId,   strUser,  strDomain);
					res = strUser.copy();
				}
			}
			else
			{
				ADEDInit();
				if(m_AD || m_SFAD)
				{
					//in ActiveDirectory Mode return Windows login name
					res.GetEnvironmentVariable(_T("USERNAME"));
					if( res == "" ){
						DWORD procId = GetCurrentProcessId();
						CAlertUtils::GetUserFromProcess( procId,   strUser,  strDomain);
						res = strUser.copy();
					}
				}
				else if (m_AzureAD) 
				{
					//in Azure AD return UPN. Maybe UNB specific case - need research and testing
					EXTENDED_NAME_FORMAT nameFormat = NameUserPrincipal;
					DWORD length = 1023;
					TCHAR username[1024];
					if (GetUserNameEx(nameFormat, username, &length)) 
					{
						res = username;
					}
				}
				else if(m_ED)
				{
					res = CA2T((char*)eDirGetUserName());
				}
			}
		}
	}
	else if (propertyName == _T("domain"))
	{
		bool isReadADcredentiansOnlyFromDB = _iAlertModel->getPropertyString(CString(_T("readADcredentiansOnlyFromDB"))).Compare(_T("1")) == 0 ? true : false;
		if(!EnableRegistration() && !EnableOtherDirectory() && !isReadADcredentiansOnlyFromDB)
		{
			ADEDInit();
			if(m_AD || m_SFAD)
				res.GetEnvironmentVariable(_T("USERDOMAIN"));
		}
	}
	else if (propertyName == _T("fulldomain"))
	{
		bool isReadADcredentiansOnlyFromDB = _iAlertModel->getPropertyString(CString(_T("readADcredentiansOnlyFromDB"))).Compare(_T("1")) == 0 ? true : false;
		if(!EnableRegistration() && !EnableOtherDirectory() && !isReadADcredentiansOnlyFromDB)
		{
			ADEDInit();
			if(m_AD || m_SFAD)
			{
				res.GetEnvironmentVariable(_T("USERDNSDOMAIN"));
			}
			else if (m_AzureAD)
			{
				res = _T("Azure Active Directory");
			}
		}
	}
	else if (propertyName == _T("compdomain"))
	{
		bool isReadADcredentiansOnlyFromDB = _iAlertModel->getPropertyString(CString(_T("readADcredentiansOnlyFromDB"))).Compare(_T("1")) == 0 ? true : false;
		if (isReadADcredentiansOnlyFromDB)
		{
			//
		}
		else
		{
			PDOMAIN_CONTROLLER_INFO pDomainInfo = NULL;
			if(DsGetDcName(NULL, NULL, NULL, NULL, DS_IS_DNS_NAME, &pDomainInfo) == ERROR_SUCCESS)
			{
				res = pDomainInfo->DomainName;
			}
			if(pDomainInfo)
				NetApiBufferFree(pDomainInfo);
		}
	}
	else if (propertyName == _T("edir"))
	{
		if(!EnableRegistration() && !EnableOtherDirectory())
		{
			ADEDInit();
			if(m_ED)
				res = CA2T((char*)eDirGetContextName());
		}
	}
	else if (propertyName == _T("COMPUTERNAME"))
	{
		res.GetEnvironmentVariable(_T("COMPUTERNAME"));
	}
	else if (propertyName == _T("system_hash"))
	{
		res = CAlertUtils::GetSystemHash();
	}
	else if(propertyName==_T("version.txt_version"))
	{
		res = CAlertUtils::GetVersionTxtVersion("");
	}
	else if(propertyName==_T("ip"))
	{
		CRegKey key;
		if(key.Open(HKEY_LOCAL_MACHINE, _T("SYSTEM\\CurrentControlSet\\services\\Tcpip\\Parameters\\Interfaces"), KEY_READ) == ERROR_SUCCESS)
		{
			CString subName;
			CAtlList<CString> list;
			DWORD i = 0, nameLength = 256;
			while(key.EnumKey(i, subName.GetBuffer(nameLength), &nameLength) == ERROR_SUCCESS)
			{
				subName.ReleaseBuffer(nameLength);
				CRegKey subkey;
				if(subkey.Open(key.m_hKey, subName, KEY_READ) == ERROR_SUCCESS)
				{
					DWORD valueLength;
					CString value;
					if(subkey.QueryStringValue(_T("DhcpIPAddress"), value.GetBuffer(valueLength = MAX_PATH), &valueLength) == ERROR_SUCCESS ||
						subkey.QueryMultiStringValue(_T("DhcpIPAddress"), value.GetBuffer(valueLength = MAX_PATH), &valueLength) == ERROR_SUCCESS ||
						subkey.QueryStringValue(_T("IPAddress"), value.GetBuffer(valueLength = MAX_PATH), &valueLength) == ERROR_SUCCESS ||
						subkey.QueryMultiStringValue(_T("IPAddress"), value.GetBuffer(valueLength = MAX_PATH), &valueLength) == ERROR_SUCCESS)
					{
						value.ReleaseBuffer(valueLength);
						value.Trim();
						if(value != _T("0.0.0.0") &&
							list.Find(value) == NULL)
						{
							list.AddTail(value);
							if(!res.IsEmpty())
								res += _T(",");
							res += value;
						}
					}
				}
				nameLength = 256;
				i++;
			}
		}
	}
	else if(propertyName==_T("locale"))
	{
		res.ReleaseBuffer(GetLocaleInfo(GetUserDefaultUILanguage(), LOCALE_SABBREVLANGNAME, res.GetBuffer(LOCALE_NAME_MAX_LENGTH), LOCALE_NAME_MAX_LENGTH));
	}
	else if(propertyName==_T("hash"))
	{
		res = CAlertUtils::userHash(getPropertyString(CString(_T("user_name")), &CString()));
	}
	else
	{
		found = false;
		std::vector<NodeALERTS::NodeSETTINGS::NodeREGISTRY*>::iterator rit = m_propRegistry.begin();
		while(rit != m_propRegistry.end())
		{
			if((*rit)->m_id == propertyName)
			{
				if(!(*rit)->m_path.IsEmpty())
				{
					CRegKey key = CAlertUtils::parseRegPath((*rit)->m_path, (*rit)->m_wow6432, KEY_READ);
					if(key)
					{
						if((*rit)->m_type == _T("key"))
						{
							res = CAlertUtils::QueryRegKey(key, (*rit)->m_path, (*rit)->m_format);
						}
						else
						{
							res = CAlertUtils::QueryRegValue(key, (*rit)->m_variable, (*rit)->m_format);
						}
					}
				}
				found = true;
				break;
			}
			++rit;
		}
		if(!found)
		{
			need_escape = false;
			m_cs_propString.Lock();
			release_try
			{
				std::map<CString,CString>::const_iterator it = m_propString.find(propertyName);
				found = (it!=m_propString.end());
				if(found)
					res = it->second;
			}
			release_catch_tolog(_T("getPropertyString find2 exception"));
			m_cs_propString.Unlock();
			if(found)
			{
				res = buildPropertyString(res, encode, fixURI);
			}
		}
	}
	if(encode && found && need_escape)
		res = BrowserUtils::encodeURIComponent(res, encode);

	return res;
}
void ParseRegXML(HKEY parent, CComPtr<IXMLDOMElement> &pElem)
{
	CComBSTR tag;
	if(SUCCEEDED(pElem->get_tagName(&tag)) && SUCCEEDED(tag.ToUpper()))
	{
		if(tag == L"VALUE")
		{
			CComVariant vName, vOldName, vValue;
			if(SUCCEEDED(pElem->getAttribute(CComBSTR(L"old_name"), &vOldName)) && vOldName.vt == VT_BSTR)
			{
				::RegDeleteValue(parent, vOldName.bstrVal);
			}
			if(SUCCEEDED(pElem->getAttribute(CComBSTR(L"name"), &vName)) && vName.vt == VT_BSTR &&
				SUCCEEDED(pElem->getAttribute(CComBSTR(L"value"), &vValue)) && vValue.vt == VT_BSTR )
			{
				::RegSetValueEx(parent, vName.bstrVal, NULL, REG_SZ, reinterpret_cast<const BYTE*>(vValue.bstrVal), (lstrlen(vValue.bstrVal)+1)*sizeof(TCHAR));
			}
		}
		else if(tag == L"KEY")
		{
			CRegKey key;
			CComVariant vName, vOldName;
			if(SUCCEEDED(pElem->getAttribute(CComBSTR(L"name"), &vName)) && vName.vt == VT_BSTR)
			{
				if(SUCCEEDED(pElem->getAttribute(CComBSTR(L"old_name"), &vOldName)) && vOldName.vt == VT_BSTR)
				{
					CAlertUtils::RecursiveRenameKey(parent, vOldName.bstrVal, vName.bstrVal);
				}
				if(key.Create(parent, vName.bstrVal, REG_NONE, REG_OPTION_NON_VOLATILE, KEY_WRITE) == ERROR_SUCCESS)
					parent = key;
			}
			else if(SUCCEEDED(pElem->getAttribute(CComBSTR(L"old_name"), &vOldName)) && vOldName.vt == VT_BSTR)
			{
				CAlertUtils::RecursiveDeleteKey(parent, vOldName.bstrVal);
				parent = NULL;
			}
			if(parent)
			{
				CComPtr<IXMLDOMNodeList> pList;
				if(SUCCEEDED(pElem->get_childNodes(&pList)) && pList)
				{
					long length = 0;
					if(SUCCEEDED(pList->get_length(&length)) && length > 0)
					{
						for(long i = 0; i < length; i++)
						{
							CComPtr<IXMLDOMNode> pChild;
							if(SUCCEEDED(pList->get_item(i, &pChild)) && pChild)
							{
								CComQIPtr<IXMLDOMElement> pChildElem = pChild;
								if(pChildElem)
								{
									ParseRegXML(parent, pChildElem);
								}
							}
						}
					}
				}
			}
		}
	}
}

void CPropertyStore::setPropertyString(CString& propertyName, CString& propertyValue, BOOL replace, BOOL common)
{
	m_cs_propString.Lock();
	release_try
	{
		bool isRegistry = false;
		std::vector<NodeALERTS::NodeSETTINGS::NodeREGISTRY*>::iterator rit = m_propRegistry.begin();
		while(rit != m_propRegistry.end())
		{
			if((*rit)->m_id == propertyName && !(*rit)->m_path.IsEmpty())
			{
				CRegKey key = CAlertUtils::parseRegPath((*rit)->m_path, (*rit)->m_wow6432, KEY_READ, TRUE);
				if(key)
				{
					const REGSAM rgs = key.m_samWOW64;
					if(key.Open(key.m_hKey, NULL, KEY_WRITE) == ERROR_SUCCESS)
					{
						key.m_samWOW64 = rgs;
						if((*rit)->m_format == _T("xml"))
						{
							CComPtr<IXMLDOMDocument> pDoc;
							::CoInitialize(NULL);
							if(SUCCEEDED(pDoc.CoCreateInstance(__uuidof(DOMDocument))) && pDoc)
							{
								VARIANT_BOOL succeeded;
								if(SUCCEEDED(pDoc->loadXML(CComBSTR(propertyValue), &succeeded)) && succeeded)
								{
									CComPtr<IXMLDOMElement> pRoot;
									if(SUCCEEDED(pDoc->get_documentElement(&pRoot)) && pRoot)
									{
										ParseRegXML(key, pRoot);
									}
								}
							}
						}
						else
						{
							if((*rit)->m_type != _T("key"))
								key.SetStringValue((*rit)->m_variable, propertyValue);
						}
					}
					else
					{
                        unique_ptr<REGISTRYDATA> psi(new REGISTRYDATA());
						CStringW::CopyChars(psi->m_path, sizeof(psi->m_path)/sizeof(TCHAR), (*rit)->m_path, (*rit)->m_path.GetLength()+1);
						CStringW::CopyChars(psi->m_name, sizeof(psi->m_name)/sizeof(TCHAR), (*rit)->m_variable, (*rit)->m_variable.GetLength()+1);
						CStringW::CopyChars(psi->m_format, sizeof(psi->m_format)/sizeof(TCHAR), (*rit)->m_format, (*rit)->m_format.GetLength()+1);
						CStringW::CopyChars(psi->m_wow6432, sizeof(psi->m_wow6432)/sizeof(TCHAR), (*rit)->m_wow6432, (*rit)->m_wow6432.GetLength()+1);
						psi->m_strVal = propertyValue.GetBuffer();
						psi->m_strLen = propertyValue.GetLength()+1;
						psi->m_dwVal = 0;
						psi->m_qwVal = 0;

						if((*rit)->m_type == _T("key"))
							psi->m_type = REG_KEY;
						else
							psi->m_type = REG_SZ;

						_iAlertController->getServiceBridge()->sendRegistryRequest(psi.get());
					}
					isRegistry = true;
					break;
				}
			}
			++rit;
		}
		if(!isRegistry)
		{
			if (replace) m_propString.erase(propertyName);
			if (m_propString.find(propertyName) == m_propString.end()) CAlertUtils::setValue(propertyName, propertyValue, common);
			m_propString.insert(pair<CString,CString>(propertyName,propertyValue));
		}
	}
	release_catch_tolog(_T("setPropertyString exception"));
	m_cs_propString.Unlock();
}

//-------------------------------------------------------------------------------------

DWORD CPropertyStore::getProperty(CString& propertyName)
{
	DWORD res=0;

	std::map<CString,DWORD>::const_iterator it = m_propData.find(propertyName);
	if(it != m_propData.end())
	{
		res = it->second;
	}
	return res;
}

void CPropertyStore::setProperty(CString& propertyName, DWORD propertyValue, BOOL replace)
{
	if(replace) m_propData.erase(propertyName);
	m_propData.insert(pair<CString,DWORD>(propertyName,propertyValue));
}

bool CPropertyStore::EnableEDirectory()
{
	static bool res = (keyExists(_T("vy0wm1iphva12qer4y.key")) || keyExists(_T("vy0wm1iph.key")));
	return res;
}

bool CPropertyStore::EnableActiveDirectory()
{
	return getPropertyString(CString(L"adEnabled")).Compare(L"1") == 0;
// Obsolete
//	static bool res = (keyExists(_T("hg29fhh847c03nu08f.key")) || keyExists(_T("hg29fhh84.key")));
//	return res;
}

bool CPropertyStore::EnableSyncFreeActiveDirectory()
{
	return getPropertyString(CString(L"synFreeAdEnabled")).Compare(L"1") == 0;
// Obsolete
//	static bool res = (keyExists(_T("kamewq9nsb8k0o22iv.key")) || keyExists(_T("kamewq9ns.key")));
//	return res;
}

bool CPropertyStore::EnableOtherDirectory()
{
	static bool res = (keyExists(_T("f8x4c0hwl.key")));
	return res;
}

bool CPropertyStore::EnableRegistration()
{
	bool res = true;
	m_cs_propString.Lock();
	release_try
	{
		std::map<CString,CString>::iterator it = m_propString.find(_T("user_name"));
		res = (it!=m_propString.end());
	}
	release_catch_tolog(_T("EnableRegistration exception"));
	m_cs_propString.Unlock();
	return res;
}

bool CPropertyStore::keyExists(CString key)
{
	//check key file
	CString FileName = getPropertyString(CString("root_path"));
	FileName += _T("\\") + key;
	const HANDLE hKeyFile = CreateFile(FileName,
		GENERIC_READ, FILE_SHARE_READ, NULL,
		OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if(hKeyFile == INVALID_HANDLE_VALUE)
	{
		return false;
	}
	else
	{
		CloseHandle(hKeyFile);
		return true;
	}
}

void CPropertyStore::setRegistry(std::vector<NodeALERTS::NodeSETTINGS::NodeREGISTRY*> registry)
{
	m_propRegistry = registry;
}
