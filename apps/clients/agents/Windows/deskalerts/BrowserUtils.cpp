#include "stdafx.h"
#include "BrowserUtils.h"

void BrowserUtils::openUrlIfSecure(CString sIn, const int encode)
{
	CString command = urlSecure(sIn, encode);
	if(!command.IsEmpty())
	{
		const HINSTANCE hinst = ::ShellExecute(NULL, L"open", command, NULL, NULL, SW_SHOW);
		ATLASSERT(hinst > (HINSTANCE)SE_ERR_DLLNOTFOUND);
		if(hinst <= (HINSTANCE)SE_ERR_DLLNOTFOUND)
		{
			command.Replace(_T("^"), _T("^^"));
			command.Replace(_T("<"), _T("^<"));
			command.Replace(_T(">"), _T("^>"));
			command.Replace(_T("&"), _T("^&"));
			command.Replace(_T("|"), _T("^|"));
			command = _T("/c \"start ") + command + _T("\"");
			const HINSTANCE hinst = ::ShellExecute(NULL, L"open", _T("cmd.exe"), command, NULL, SW_HIDE);
			ATLASSERT(hinst > (HINSTANCE)SE_ERR_DLLNOTFOUND);
			DBG_UNREFERENCED_LOCAL_VARIABLE(hinst);
		}
	}
}

CString BrowserUtils::urlSecure(CString sIn, const int encode)
{
	sIn = getURLfromPath(sIn, encode);
	if(sIn.Find(_T("file://"))==0)
	{
		CUrl url;
		if(url.CrackUrl(sIn))
		{
			CString path = url.GetUrlPath();
			CString ext = PathFindExtension(path);
			if(ext.CompareNoCase(_T(".htm")) &&
				ext.CompareNoCase(_T(".html")))
			{
				sIn.Empty();
			}
		}
		else
		{
			sIn.Empty();
		}
	}
	return sIn;
}

CString BrowserUtils::getURLfromPath(CString sIn, const int encode)
{
	if(sIn.Find(_T("://"))==-1)
	{
		sIn = _T("file:///") + sIn;
		sIn.Replace(_T('\\'), _T('/'));
	}
	return fixURI(sIn, encode);
}

CString BrowserUtils::fixURI(CString sIn, const int encode)
{
	return sIn.Find(_T("://"))==-1?sIn:URLEncode(sIn, _T("~!@#$&*()_+-=;':?,./%"), encode);
}

CString BrowserUtils::escape(CString sIn, const int encode)
{
	return URLEncode(sIn, _T("@*_+-./"), encode);
}

CString BrowserUtils::encodeURI(CString sIn, const int encode)
{
	return URLEncode(sIn, _T("~!@#$&*()_+-=;':?,./"), encode);
}

CString BrowserUtils::encodeURIComponent(CString sIn, const int encode)
{
	return URLEncode(sIn, _T("~!*()_-'."), encode);
}

CString BrowserUtils::URLEncode(CString sIn, LPCTSTR exclude, const int encode)
{
	const static LPCTSTR hexequiv = _T("0123456789ABCDEF");

	CString sOut;
	// process each code point
	for (int i = 0; i < sIn.GetLength(); i++)
	{
		CString n = sIn.Mid(i, 1);
		if( (n[0] >= _T('a') && n[0] <= _T('z')) ||
			(n[0] >= _T('A') && n[0] <= _T('Z')) ||
			(n[0] >= _T('0') && n[0] <= _T('9')) ||
			(exclude && _tcsstr(exclude, n)))
		{
			sOut += n;
		}
		else
		{
			CT2A inStr(n, encode);
			for (size_t i = 0; i < strlen(inStr); i++)
			{
				sOut += _T("%");
				sOut += hexequiv[(inStr[i] >> 4) & 0xF];
				sOut += hexequiv[inStr[i] & 0xF];
			}
		}
	}
	return sOut;
}

INT64 BrowserUtils::variantToInt64(const VARIANT &var, INT64 def)
{
	if(var.vt == (VT_BYREF|VT_VARIANT))
		def = variantToInt64(*var.pvarVal, def);

	////////////////////////////

	else if(var.vt == VT_BSTR)
		def = _wtoi64(CComBSTR(var.bstrVal));

	else if(var.vt == VT_I1)
		def = var.cVal;
	else if(var.vt == VT_UI1)
		def = var.bVal;

	else if(var.vt == VT_I2)
		def = var.iVal;
	else if(var.vt == VT_UI2)
		def = var.uiVal;

	else if(var.vt == VT_INT)
		def = var.intVal;
	else if(var.vt == VT_UINT)
		def = var.uintVal;

	else if(var.vt == VT_I4)
		def = var.lVal;
	else if(var.vt == VT_UI4)
		def = var.ulVal;

	else if(var.vt == VT_I8)
		def = var.llVal;
	else if(var.vt == VT_UI8)
		def = var.ullVal;

	else if(var.vt == VT_R4)
		def = (INT64)var.fltVal;
	else if(var.vt == VT_R8)
		def = (INT64)var.dblVal;

	else if(var.vt == VT_BOOL)
		def = var.boolVal;

	////////////////////////////

	else if(var.vt == (VT_BYREF|VT_BSTR))
		def = _wtoi64(CComBSTR(*var.pbstrVal));

	else if(var.vt == (VT_BYREF|VT_I1))
		def = *var.pcVal;
	else if(var.vt == (VT_BYREF|VT_UI1))
		def = *var.pbVal;

	else if(var.vt == (VT_BYREF|VT_I2))
		def = *var.piVal;
	else if(var.vt == (VT_BYREF|VT_UI2))
		def = *var.puiVal;

	else if(var.vt == (VT_BYREF|VT_INT))
		def = *var.pintVal;
	else if(var.vt == (VT_BYREF|VT_UINT))
		def = *var.puintVal;

	else if(var.vt == (VT_BYREF|VT_I4))
		def = *var.plVal;
	else if(var.vt == (VT_BYREF|VT_UI4))
		def = *var.pulVal;

	else if(var.vt == (VT_BYREF|VT_I8))
		def = *var.pllVal;
	else if(var.vt == (VT_BYREF|VT_UI8))
		def = *var.pullVal;

	else if(var.vt == (VT_BYREF|VT_R4))
		def = (INT64)*var.pfltVal;
	else if(var.vt == (VT_BYREF|VT_R8))
		def = (INT64)*var.pdblVal;

	else if(var.vt == (VT_BYREF|VT_BOOL))
		def = *var.pboolVal;

	return def;
}

CString &BrowserUtils::variantToString(const VARIANT &var, CString &def)
{
	TCHAR buff[65];
	errno_t err = ERROR_EMPTY;

	const VARTYPE vt_wo_ref = var.vt & (~VT_BYREF);

	if(var.vt == (VT_BYREF|VT_VARIANT))
		def = variantToString(*var.pvarVal, def);

	////////////////////////////
	else if(vt_wo_ref == VT_BSTR)
		def = var.vt & VT_BYREF ? *var.pbstrVal : var.bstrVal;

	else if(vt_wo_ref == VT_I1)
		err = _ltot_s(var.vt & VT_BYREF ? *var.pcVal : var.cVal, buff, 33, 10);
	else if(vt_wo_ref == VT_UI1)
		err = _ultot_s(var.vt & VT_BYREF ? *var.pbVal : var.bVal, buff, 33, 10);

	else if(vt_wo_ref == VT_I2)
		err = _ltot_s(var.vt & VT_BYREF ? *var.piVal : var.iVal, buff, 33, 10);
	else if(vt_wo_ref == VT_UI2)
		err = _ultot_s(var.vt & VT_BYREF ? *var.puiVal : var.uiVal, buff, 33, 10);

	else if(vt_wo_ref == VT_INT)
		err = _ltot_s(var.vt & VT_BYREF ? *var.pintVal : var.intVal, buff, 33, 10);
	else if(vt_wo_ref == VT_UINT)
		err = _ultot_s(var.vt & VT_BYREF ? *var.puintVal : var.uintVal, buff, 33, 10);

	else if(vt_wo_ref == VT_I4)
		err = _ltot_s(var.vt & VT_BYREF ? *var.plVal : var.lVal, buff, 33, 10);
	else if(vt_wo_ref == VT_UI4)
		err = _ultot_s(var.vt & VT_BYREF ? *var.pulVal : var.ulVal, buff, 33, 10);

	else if(vt_wo_ref == VT_I8)
		err = _i64tot_s(var.vt & VT_BYREF ? *var.pllVal : var.llVal, buff, 65, 10);
	else if(vt_wo_ref == VT_UI8)
		err = _ui64tot_s(var.vt & VT_BYREF ? *var.pullVal : var.ullVal, buff, 65, 10);

	else if(vt_wo_ref == VT_R4 || vt_wo_ref == VT_R8)
	{
		const double val = vt_wo_ref == VT_R4 ?
			(var.vt & VT_BYREF ? *var.pfltVal :var.fltVal) :
			(var.vt & VT_BYREF ? *var.pdblVal :var.dblVal);
		CStringA cvbuff;
		if(_gcvt_s(cvbuff.GetBuffer(_CVTBUFSIZE), _CVTBUFSIZE, val, _CVTBUFSIZE-2) == ERROR_SUCCESS)
		{
			cvbuff.ReleaseBuffer();
			cvbuff.TrimRight(_T('.'));
			def = cvbuff;
		}
	}

	else if(vt_wo_ref == VT_BOOL)
	{
		const VARIANT_BOOL val = var.vt & VT_BYREF ? *var.pboolVal : var.boolVal;
		def = val ? _T("1") : _T("0");
	}

	if(err == ERROR_SUCCESS)
		def = buff;

	return def;
}
