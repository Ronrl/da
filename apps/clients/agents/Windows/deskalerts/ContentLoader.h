#pragma once

class CContentLoader
{
	ATL::CString m_URL;
	ATL::CString m_body;

private:
	ATL::CString LoadFromFile(ATL::CString &iFname);

public:
	CContentLoader();

	ATL::CString GetText(ATL::CString &iUrl, bool iRefresh=false);

	~CContentLoader();
};
