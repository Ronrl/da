#pragma once

//#include "IInclude.h"

/////////////////////////////////////////////////////////////////////////////
// Hook on
//

class IWinMessageFilter
{
public:
	virtual BOOL WinProcessWindowMessage(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam,LRESULT& lResult, DWORD dwMsgMapID = 0) = 0;
};

class CMyPageHook : public CWindowImpl<CMyPageHook>
{
public:

	BOOL ProcessWindowMessage(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam, LRESULT& lResult, DWORD dwMsgMapID = 0);	
	
	CMyPageHook(HWND SubHWND, IWinMessageFilter* _mFilter);
	~CMyPageHook();
	
private:
	IWinMessageFilter* mFilter;
};

