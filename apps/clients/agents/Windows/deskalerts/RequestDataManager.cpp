#include "stdafx.h"
#include "RequestDataManager.h"
#include <time.h>
#include "sha1.h"
#include <math.h>

RequestDataManager::RequestDataManager(ServiceDataManager * dataManager): m_packet_count(NULL), m_last_packet_size(NULL)
{
	m_dataManager = dataManager;
}

RequestDataManager::~RequestDataManager(void)
{
}

RequestDataType RequestDataManager::getDataType()
{
	return REQUESTDATATYPE_UNKNOWN;
}

unsigned int RequestDataManager::getPacketsCount()
{
	return m_packet_count;
}

PPACKET RequestDataManager::getPacket(void *data, unsigned int id)
{
	unsigned int packet_size;
	PPACKET packet=new PACKET();
	if (id < m_packet_count-1)
	{
		packet->header.isLast=0;
		packet_size = PACKET_LENGTH;
	}
	else
	{
		packet->header.isLast=1;
		packet_size = m_last_packet_size;
	}
	void *packetData=(void *)((char *)data+PACKET_LENGTH*id);
	memcpy(packet->data,packetData,packet_size);
	return packet;
}


bool RequestDataManager::structHash(void *data, unsigned length, unsigned *res)
{
	SHA1 sha1;
	srand((unsigned int)time(NULL));
	int rnd = rand();
	sha1.Input((unsigned char*)data, length);
	sha1.Input((unsigned char*)&rnd, sizeof(rnd));
	sha1.Input((unsigned char*)HASH_CONST, 5);
	res[5] = rnd;
	return sha1.Result(res);
}

bool RequestDataManager::checkStructHash(void *data, unsigned length, unsigned *hash)
{
	SHA1 sha1;
	int rnd = hash[5];
	unsigned res[6];
	sha1.Input((unsigned char*)data, length);
	sha1.Input((unsigned char*)&rnd, sizeof(rnd));
	sha1.Input((unsigned char*)HASH_CONST, 5);
	res[5] = rnd;
	bool result=false;
	if (sha1.Result((unsigned*)res))
	{
		result=true;
		for (unsigned i=0; i<5; i++)
		{
			if (res[i]!=hash[i])
			{
				result=false;
				break;
			}
		}
	}
	return result;
}

DWORD RequestDataManager::serialize(IStream* pStream)
{

	//REQUESTPACKET p;
    REQUESTPACKET *p = new REQUESTPACKET();
	pStream->Write(p, sizeof(REQUESTPACKET), NULL);
	m_dataManager->serialize(pStream);

	HGLOBAL hGlobal = NULL;
	GetHGlobalFromStream(pStream, &hGlobal);

	LPVOID pData = GlobalLock(hGlobal);

	STATSTG stat1;
	pStream->Stat(&stat1, 0);

	unsigned int dataLength=(unsigned int)stat1.cbSize.LowPart - sizeof(REQUESTPACKET);

	PREQUESTPACKET packet = (PREQUESTPACKET)pData;
    if (packet != nullptr)
    {
        packet->data = (char*)pData + sizeof(REQUESTPACKET);

        RequestDataManager::structHash(packet->data, dataLength, packet->header.hash);
        packet->header.length = dataLength;
        packet->header.dataType = m_dataManager->getDataType();
    }
	GlobalUnlock(hGlobal);

	m_packet_count = (unsigned int)ceil((dataLength+sizeof(REQUESTPACKET))*1.0/PACKET_LENGTH);
	m_last_packet_size = (dataLength+sizeof(REQUESTPACKET))%PACKET_LENGTH;

	return S_FALSE;
}