#include "stdafx.h"
#include "SwitchDesktopHandler.h"
#include <winbase.h>
#include "AlertUtils.h"
#include <algorithm>


CSwitchDesktopHandler::CSwitchDesktopHandler(void): m_thread(NULL), m_threadId(NULL)
{
	Create();
}

CSwitchDesktopHandler::~CSwitchDesktopHandler(void)
{
	stop();
	if (IsWindow())
	{
		DestroyWindow();
	}
}

void CSwitchDesktopHandler::start()
{
	m_thread = CreateThread(NULL, 0, CSwitchDesktopHandler::handlerThread,(LPVOID)m_hWnd, 0, &m_threadId);
}

DWORD WINAPI CSwitchDesktopHandler::handlerThread(LPVOID pv)
{
	DWORD res = 1;
	release_try
	{
		CString oldDesktopName;
		CString newDesktopName;
		
		::SetTimer(NULL,NULL,500,NULL);
		MSG msg;
		while(GetMessage(&msg, NULL, 0, 0) > 0) //important to check > 0, see MSDN
		{
			release_try
			{
				if(msg.message == UTM_STOP_HANDLER)
				{
					SetEvent((HANDLE)msg.lParam);
					break;
				}
				else if(msg.message == WM_TIMER)
				{
					const HDESK desktop = OpenInputDesktop(0,FALSE,DESKTOP_CREATEWINDOW);
					if(desktop)
					{
						newDesktopName = CAlertUtils::getDesktopName(desktop);
						CloseDesktop(desktop);
					}
					else
					{
						newDesktopName = _T("WinLogon");
					}

					if(!oldDesktopName.IsEmpty())
					{
						if(newDesktopName.MakeLower() != oldDesktopName.MakeLower())
						{
							PostMessageDelBoth<CString,CString>((HWND)pv, UTM_SWITCH_DESKTOP, new CString(oldDesktopName), new CString(newDesktopName));
						}
					}
					oldDesktopName = newDesktopName;
				}
				else
				{
					TranslateMessage(&msg);
					DispatchMessage(&msg);
				}
			}
			release_catch_expr_and_tolog(res = 3, _T("SwitchDesktop loop exception"))
		}
	}
	release_catch_expr_and_tolog(res = 3, _T("SwitchDesktop thread exception"))

	return res;
}

void CSwitchDesktopHandler::stop()
{
	if(m_thread)
	{
		DWORD exitCode;
		if(GetExitCodeThread(m_thread,&exitCode))
		{
			if(exitCode == STILL_ACTIVE)
			{
				const HANDLE event = CreateEvent(NULL, TRUE, FALSE, NULL);
                if (event != NULL)
                {
                    if (!PostThreadMessage(m_threadId, UTM_STOP_HANDLER, NULL, (LPARAM)event) ||
                        WaitForSingleObject(event, 5000) == WAIT_OBJECT_0)
                    {
                        CloseHandle(event);
                    }
                }
			}
		}
		CloseHandle(m_thread);
		m_thread = NULL;
	}
}

LRESULT CSwitchDesktopHandler::OnSwitchDesktop(UINT /*uMsg*/, WPARAM wParam, LPARAM lParam, BOOL& bHandled )
{
	CString *closedDesktopName = (CString*)wParam;
	CString *openedDesktopName = (CString*)lParam;
	m_cs.Lock();
	release_try
	{
		vector<ISwitchDesktopListener*>::iterator it = m_listeners.begin();
		while(it != m_listeners.end())
		{
			(*it)->OnSwitchDesktop(closedDesktopName, openedDesktopName);
			it++;
		}
	}
	release_catch_tolog(_T("SwitchDesktop switch exception"))
	m_cs.Unlock();
	delete_catch(closedDesktopName);
	delete_catch(openedDesktopName);
	bHandled = TRUE;
	return S_OK;
}

void CSwitchDesktopHandler::addListener( ISwitchDesktopListener* listener )
{
	m_cs.Lock();
	release_try
	{
		m_listeners.push_back(listener);
	}
	release_catch_tolog(_T("SwitchDesktop add exception"))
	m_cs.Unlock();
}

void CSwitchDesktopHandler::removeListener( ISwitchDesktopListener* listener )
{
	m_cs.Lock();
	release_try
	{
		const vector<ISwitchDesktopListener*>::const_iterator it = std::find(m_listeners.begin(), m_listeners.end(), listener);
		if (it != m_listeners.end())
		{
			m_listeners.erase(it);
		}
	}
	release_catch_tolog(_T("SwitchDesktop remove exception"))
	m_cs.Unlock();
}