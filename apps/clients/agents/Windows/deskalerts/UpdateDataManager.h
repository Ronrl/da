#pragma once
#include "ServiceDataManager.h"


class UpdateDataManager : public ServiceDataManager
{
private:
	CStringW m_updateRoot;
	CStringW m_sRootPath;
	CStringW m_version;
public:
	UpdateDataManager(void);
	~UpdateDataManager(void);
	DWORD serialize(IStream* pStream);
	void setData(CStringW &updateRoot,CStringW &sRootPath, CStringW &version);
	RequestDataType getDataType();
};
