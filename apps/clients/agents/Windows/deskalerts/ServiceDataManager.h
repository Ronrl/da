#pragma once

class ServiceDataManager
{
public:
	virtual DWORD serialize(IStream* pStream) = 0;
	virtual RequestDataType getDataType()=0;
};
