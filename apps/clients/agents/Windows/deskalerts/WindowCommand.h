#pragma once

#include "IInclude.h"

#include "HTMLView.h"
#include "OptionWindowDecorator.h"
#include "EventHandler.h"
#include "AlertUtils.h"
#include <windows.h>
#include <synchapi.h>
#include <map>
#include <vector>
#include <tuple>
#include <atlctl.h>
#include "DatabaseManagr.h"
#include ".\findertext.h"
#include "ContentLoader.h"
#include "LockWindow.h"
#include "..\DpiHelper\DpiHelper.h"
#include <list>
#define WM_KILL_ME (WM_APP+101)
#define _RELOAD_TIMER_	100
#define _SCREENSAVER_DESKTOP_TIMER_	101

#define DEFAULT_WINDOW_WIDTH  400
#define DEFAULT_WINDOW_HEIGHT 400
#define MIN_WINDOW_WIDTH      200
#define MIN_WINDOW_HEIGHT     260

class CWindowCommand;

typedef tuple<CWindowCommand*, CString, shared_ptr<showData>> ShowedWinData;

class CMultiWindowCommand:
	public CWindowImpl<CMultiWindowCommand>,
	public IView
{

public:
	CMultiWindowCommand(NodeALERTS::NodeVIEWS::NodeWINDOW* param, HWND parentHWND);
	~CMultiWindowCommand();

	vector<shared_ptr<showData>>* closeAndGetNotReadUrgentAlerts();
	void closeAllExpiredAlerts();

	int m_iWindowCount;

	bool show(CString param, shared_ptr<showData> ex, BOOL isNewAlert = FALSE);
	void hide(CString param);

	HWND getParentHWND();

	NodeALERTS::NodeVIEWS::NodeWINDOW data;

	BEGIN_MSG_MAP(CMultiWindowCommand)
		MESSAGE_HANDLER(WM_KILL_ME, OnDropWindow)
	END_MSG_MAP()
	LRESULT OnDropWindow(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
private:
	std::vector<CWindowCommand*> m_windows;
	std::vector<ShowedWinData> m_notOpenedUrgent;
	std::vector<ShowedWinData> m_notOpenedUsual;
	HWND m_parentHWND;
};


class CWindowCommand:public IView,
	public CWindowImpl<CWindowCommand,CWindow>,
	public IMyDispatchListener,
	public IDocCompleteListener
{
public:
	
private:
	CContentLoader JSLoader;
	CContentLoader CSSLoader;

	struct CallFuncStruct{

		CallFuncStruct(CComBSTR _strFuncName, CString &_csParam)
			: strFuncName(_strFuncName), pvarParams(new CSimpleArray<CComVariant>())
		{
			pvarParams->Add(CComVariant(_csParam));
		}
		CallFuncStruct(CComBSTR _strFuncName, shared_ptr<CSimpleArray<CComVariant>> _pvarParams)
			: strFuncName(_strFuncName), pvarParams(_pvarParams)
		{
		}
		CComBSTR strFuncName;
		shared_ptr<CSimpleArray<CComVariant>> pvarParams;
	};

	int AlertsInWindow;
	map<CString, pair<int,shared_ptr<showData>>> NotReadAlerts;
	CString m_url;
	CString m_post;
	int m_encoding;
	sqlite_int64 m_history_id;
	POINT m_resingStartDiff;
	POINT m_resingPoint;
	void InjectJS(CString &URL, CString &URLDef);
	void InjectCSS(CString &URL, CString &URLDef);

	std::deque<shared_ptr<CallFuncStruct>> bodyCalls;
	std::deque<shared_ptr<CallFuncStruct>> captionCalls;

	CComQIPtr<IHTMLDocument2> GetDocumentFromView(CHTMLView *view);
	void CallJsQueue(std::deque<shared_ptr<CallFuncStruct>> &queue, CHTMLView *view);
	
	ULONG m_lastCallbackId;
	map<ULONG, CComPtr<IDispatch>> m_callbacks;

	CComVariant* CloseAPI();
	CComVariant* PrintAPI();
	CComVariant* PostponeAPI(DISPPARAMS* pDispParams);
	CComVariant* SetStatusAPI();
	CComVariant* OpenURLAPI(CString);
	CComVariant* IsExternalBrowserEnableAPI();
	CComVariant* showAlertFromHistory(DISPPARAMS* pDispParams);
	static void CALLBACK CWindowCommand::showPostponedAlertByTimer(HWND hwnd, UINT uMsg, UINT timerId, DWORD dwTime);
public:
	CWindowCommand(NodeALERTS::NodeVIEWS::NodeWINDOW *param,CMultiWindowCommand* winDisp);
	~CWindowCommand(void);

	BEGIN_MSG_MAP(CHTMLView)
		MESSAGE_HANDLER(WM_CREATE, OnCreate)
		MESSAGE_HANDLER(WM_CLOSE, OnClose)
		MESSAGE_HANDLER(WM_ERASEBKGND, OnErasebkgnd)
		MESSAGE_HANDLER(WM_TIMER, OnTimer)
		MESSAGE_HANDLER(WM_CALLBACK_INVOKE, OnCallbackInvoke)
		MESSAGE_HANDLER(WM_MOUSEMOVE, onMouseMove)
		MESSAGE_HANDLER(WM_LBUTTONUP, onLeftMouseButtonUp)
	END_MSG_MAP()

	LRESULT OnCreate(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT OnClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnErasebkgnd(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnTimer(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnCallbackInvoke(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT onMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT onLeftMouseButtonUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	CComVariant* DispInvoke(CString name,DISPPARAMS *pDispParams);
	BOOL MakeWindowTransparent( HWND hWnd, COLORREF color, unsigned char factor, DWORD flags);
	virtual void OnDocComplete(CHTMLView* HTMLView);
	virtual BOOL OnNavigateError(CHTMLView* HTMLView, CString URL, CString TargetFrameName, LONG StatusCode);
	BOOL CallJsInView(shared_ptr<CallFuncStruct> callFuncStruct, CHTMLView* view);
	void Reload();
	void CallJs(shared_ptr<CallFuncStruct> callFuncStruct, BOOL inToolbar = FALSE);
	bool show(CString param, shared_ptr<showData> ex, BOOL isNewAlert = FALSE);
	void RegisterAccessBar(HWND hwndAccessBar, LPRECT lprc, UINT uEdge);
	void UnregisterAccessBar(HWND hwndAccessBar, UINT uEdge, BOOL isSwitchDesktop = FALSE);
	void SetUrl(CString URL, shared_ptr<showData> ex);
	map<CString, pair<int,shared_ptr<showData>>> &getNotReadAlerts();

	CRect Reposition(long m_width, long m_heigh, bool checkOverflow = true);

	NodeALERTS::NodeVIEWS::NodeWINDOW data;

	CComPtr<CComObject<CEventHandler>> m_pHandler;
	CHTMLView* cHTMLView;
	CHTMLView* cHTMLTool;

	CMultiWindowCommand* winDisp;

	shared_ptr<CFinderText> m_pFinderText;

	BOOL m_bInitialized, m_manualclose;
	PSLWA pSetLayeredWindowAttributes;
};

class PostponeManager
{
private:
	std::list<std::pair<UINT_PTR, INT64>> *_listPostponedAlerts;
	CRITICAL_SECTION		myCS;
public:
	void add(pair<UINT_PTR, INT64> e)
	{
		::EnterCriticalSection(&myCS);
		_listPostponedAlerts->push_back(e); 
		::LeaveCriticalSection(&myCS);
	}
	void remove(INT64 AlertID)
	{
		::EnterCriticalSection(&myCS);
		auto it = _listPostponedAlerts->begin();
		while (it != _listPostponedAlerts->end()) 
		{
			if (it->second == AlertID)
			{
				_listPostponedAlerts->erase(it, it);
				break;
			}
		}
		::LeaveCriticalSection(&myCS);
	}
	const std::list<std::pair<UINT_PTR, INT64>> listPostponedAlerts() const { return *_listPostponedAlerts; }
private:
	static PostponeManager* instance_;
public:
	static PostponeManager* Get() { return instance_; }
	static void Initialize() { 
		if (instance_ == nullptr)
		{
			instance_ = new PostponeManager(); 
			instance_->_listPostponedAlerts = new std::list<std::pair<UINT_PTR, INT64>>();
			::InitializeCriticalSection(&(instance_->myCS));
		}
	}
};