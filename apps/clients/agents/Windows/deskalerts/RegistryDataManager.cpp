#include "stdafx.h"
#include "RegistryDataManager.h"

RegistryDataManager::RegistryDataManager(void): m_regData(NULL)
{
}

RegistryDataManager::~RegistryDataManager(void)
{
}

void RegistryDataManager::setData(PREGISTRYDATA regData)
{
	m_regData = regData;
}

RequestDataType RegistryDataManager::getDataType()
{
	return REQUESTDATATYPE_REGISTRY;
}

DWORD RegistryDataManager::serialize(IStream* pStream)
{
	pStream->Write(m_regData, sizeof(REGISTRYDATA), NULL);
	pStream->Write(m_regData->m_strVal, m_regData->m_strLen*sizeof(WCHAR), NULL);
	return 0;
}