#include "StdAfx.h"
#include <time.h>
#include <Windows.h>
#include ".\windowcommand.h"
#include ".\alertcontroller.h"
#include ".\historystorage.h"
#include ".\alertloader.h"
#include <algorithm>
#include <atlsafe.h>
#include "BrowserUtils.h"
#include <shellapi.h>

PostponeManager* PostponeManager::instance_ = nullptr;
CMultiWindowCommand::CMultiWindowCommand(NodeALERTS::NodeVIEWS::NodeWINDOW* param, HWND parentHWND) : data(*param), m_iWindowCount(0), m_parentHWND(parentHWND)
{
	//	HWND parentHWND = (HWND)_iAlertModel->getProperty(CString(_T("mainHWND")));
	m_hWnd = Create(m_parentHWND, CRect(), data.m_caption, 0, 0);

}

bool CMultiWindowCommand::show(CString Url, shared_ptr<showData> ex, BOOL isNewAlert)
{
    DeskAlertsAssert(Url);
    DeskAlertsAssert(ex);
	BOOL result = FALSE;
	if (data.m_single != _T("1") || m_windows.size() == 0)
	{
		CWindowCommand* winCom = new CWindowCommand(&data, this);
        DeskAlertsAssert(winCom);
		if (m_windows.size() < data.m_usualWindowsInOneTime ||
			(ex && ex->urgent == _T("1") && m_windows.size() < data.m_urgentWindowsInOneTime))
		{
			result = winCom->show(Url, ex, isNewAlert);
			if (TRUE == result)
			{
				m_windows.push_back(winCom);
				if (!ex || CRect(ex->windowRect).IsRectNull())
				{
					m_iWindowCount++;
				}
			}
			else
			{
				delete_catch(winCom);
			}
		}
		else
		{
			ShowedWinData win(winCom, Url, ex);
			if (ex && ex->urgent == _T("1"))
				m_notOpenedUrgent.push_back(win);
			else
				m_notOpenedUsual.push_back(win);
		}
	}
	else //not first single
	{
        if (data.m_visible != _T("false"))
        {
            m_windows.back()->ShowWindow(SW_SHOWNA);
        }
		if (data.m_visible != _T("showna"))
		{
			m_windows.back()->SetFocus();
		}
		m_windows.back()->SetUrl(Url, ex);
		result = TRUE;
	}
	return result;
}

void CMultiWindowCommand::hide(CString param)
{
	release_try
	{
		while (!m_windows.empty())
		{
			delete_catch(m_windows.back());
			m_windows.pop_back();
		}
		while (!m_notOpenedUrgent.empty())
		{
			delete_catch(get<0>(m_notOpenedUrgent.back()));
			m_notOpenedUrgent.pop_back();
		}
		while (!m_notOpenedUsual.empty())
		{
			delete_catch(get<0>(m_notOpenedUsual.back()));
			m_notOpenedUsual.pop_back();
		}
	}
	release_catch_tolog(_T("MultiWindow hide exception"));
}

LRESULT CMultiWindowCommand::OnDropWindow(UINT /*uMsg*/, WPARAM wParam, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	CWindowCommand* winKill = (CWindowCommand*)wParam;
	release_try
	{
		vector<CWindowCommand*>::iterator it = m_windows.begin();
		while (it != m_windows.end())
		{
			if (*it == winKill)
			{
				delete_catch(winKill);
				m_windows.erase(it);
				const HANDLE pHandle = GetCurrentProcess();
				SetProcessWorkingSetSize(pHandle, (SIZE_T)-1, (SIZE_T)-1);
				CloseHandle(pHandle);
				break;
			}
			it++;
		}
		if (m_windows.size() < data.m_urgentWindowsInOneTime && !m_notOpenedUrgent.empty())
		{
			ShowedWinData &win = m_notOpenedUrgent.back();

			CWindowCommand* winCom = get<0>(win);
			CString &Url = get<1>(win);
			shared_ptr<showData> &ex = get<2>(win);

			if (winCom->show(Url, ex))
			{
				m_windows.push_back(winCom);

				if (!ex || CRect(ex->windowRect).IsRectNull())
				{
					m_iWindowCount++;
				}
				m_notOpenedUrgent.pop_back();
			}
		}
		if (m_windows.size() < data.m_usualWindowsInOneTime && !m_notOpenedUsual.empty())
		{
			ShowedWinData &win = m_notOpenedUsual.back();

			CWindowCommand* winCom = get<0>(win);
			CString &Url = get<1>(win);
			shared_ptr<showData> &ex = get<2>(win);

			
			if (winCom->show(Url, ex))
			{
				m_windows.push_back(winCom);
				if (!ex || CRect(ex->windowRect).IsRectNull())
				{
					m_iWindowCount++;
				}
				m_notOpenedUsual.pop_back();
			}
		}

		if (m_windows.empty())
		{
			m_iWindowCount = 0;
		}
	}
	release_catch_tolog(_T("MultiWindow drop exception"));

	return S_OK;
}

HWND CMultiWindowCommand::getParentHWND()
{
	return m_parentHWND;
}

CMultiWindowCommand::~CMultiWindowCommand()
{
	hide(CString());
	if (IsWindow())
	{
		DestroyWindow();
	}
}


void CMultiWindowCommand::closeAllExpiredAlerts()
{
	time_t rawtime;
	struct tm  timeinfo;
	char buffer[80];

	time(&rawtime);
	localtime_s(&timeinfo, &rawtime);

	// timeinfo = localtime(&rawtime);


	strftime(buffer, 80, "%Y-%m-%d %I:%M:%S", &timeinfo);
	std::string now_date(buffer);


	//vector<shared_ptr<showData>>* windowsData = new vector<shared_ptr<showData>>();

	vector<CWindowCommand*>::iterator it = m_windows.begin();
	while (it != m_windows.end())
	{
		bool needToReload = false;
		map<CString, pair<int, shared_ptr<showData>>> &notReadAlerts = (*it)->getNotReadAlerts();

		if (!notReadAlerts.empty())
		{
			map<CString, pair<int, shared_ptr<showData>>>::iterator notReadAlertsIt = notReadAlerts.begin();
			while (notReadAlertsIt != notReadAlerts.end())
			{

				if (notReadAlertsIt->second.second)
				{
					shared_ptr<showData> alertData = notReadAlertsIt->second.second;
					CT2A ascii(alertData->to_date, CP_UTF8);
					std::string expired_date(ascii.m_psz);

					if (expired_date > now_date)
					{
						if ((*it)->IsWindow())
						{
							(*it)->GetWindowRect(&notReadAlertsIt->second.second->windowRect);
						}
						//windowsData->push_back(notReadAlertsIt->second.second);
						notReadAlertsIt = notReadAlerts.erase(notReadAlertsIt);
						needToReload = true;
					}
				}
				else
				{
					notReadAlertsIt++;
				}
			}
		}
		if (notReadAlerts.empty())
		{
			delete_catch((*it));
			it = m_windows.erase(it);
			if (m_windows.empty())
				m_iWindowCount = 0;
		}
		else
		{
			if (needToReload)
				(*it)->Reload();
			it++;
		}
	}
}
vector<shared_ptr<showData>>* CMultiWindowCommand::closeAndGetNotReadUrgentAlerts()
{
	vector<shared_ptr<showData>>* windowsData = new vector<shared_ptr<showData>>();

	vector<CWindowCommand*>::iterator it = m_windows.begin();
	while (it != m_windows.end())
	{
		bool needToReload = false;
		map<CString, pair<int, shared_ptr<showData>>> &notReadAlerts = (*it)->getNotReadAlerts();
		if (!notReadAlerts.empty())
		{
			map<CString, pair<int, shared_ptr<showData>>>::iterator notReadAlertsIt = notReadAlerts.begin();
			while (notReadAlertsIt != notReadAlerts.end())
			{
				if (notReadAlertsIt->second.second && notReadAlertsIt->second.second->urgent == _T("1"))
				{
					if ((*it)->IsWindow())
					{
						(*it)->GetWindowRect(&notReadAlertsIt->second.second->windowRect);
					}
					windowsData->push_back(notReadAlertsIt->second.second);
					notReadAlertsIt = notReadAlerts.erase(notReadAlertsIt);
					needToReload = true;
				}
				else
				{
					notReadAlertsIt++;
				}
			}
		}
		if (notReadAlerts.empty())
		{
            BOOL isSwitchDeskTop = TRUE;

            if ((*it)->data.m_ticker == CString(L"1"))
            {
                (*it)->UnregisterAccessBar((*it)->winDisp->getParentHWND(), ABE_BOTTOM, isSwitchDeskTop);
            }

			delete_catch((*it));
			it = m_windows.erase(it);
			if (m_windows.empty())
				m_iWindowCount = 0;
		}
		else
		{
			if (needToReload)
				(*it)->Reload();
			it++;
		}
	}
	while (!m_notOpenedUrgent.empty())
	{
		delete_catch(get<0>(m_notOpenedUrgent.back()));
		windowsData->push_back(get<2>(m_notOpenedUrgent.back()));
		m_notOpenedUrgent.pop_back();
	}

	return windowsData;
}

//----------------------------------------------------------------------

CWindowCommand::CWindowCommand(NodeALERTS::NodeVIEWS::NodeWINDOW *param, CMultiWindowCommand* _winDisp) :
	data(*param),
	winDisp(_winDisp),
	cHTMLView(NULL),
	cHTMLTool(NULL),
	m_bInitialized(false),
	m_manualclose(true),
	m_encoding(_iAlertModel->getProperty(CString(_T("encoding")))),
	m_history_id(-1),
	m_lastCallbackId(0),
    AlertsInWindow(NULL), 
    pSetLayeredWindowAttributes(NULL)
{
    m_resingStartDiff.x = NULL;
    m_resingStartDiff.y = NULL;
    m_resingPoint.x = NULL;
    m_resingPoint.y = NULL;
	PostponeManager::Initialize();
	CComObject<CEventHandler>::CreateInstance(&m_pHandler);
	(*m_pHandler).AddRef();
}

CWindowCommand::~CWindowCommand(void)
{
	CLockWindow::RemoveWindow(m_hWnd);

	m_pHandler->removeMyDispatchListener(this);

	if (cHTMLView)
	{
		cHTMLView->destroyComponent();
		delete_catch(cHTMLView);
	}
	if (cHTMLTool)
	{
		cHTMLTool->destroyComponent();
		delete_catch(cHTMLTool);
	}
	if (IsWindow())
		DestroyWindow();
}


/************************************************************************/
/*                                                                      */
/************************************************************************/

LRESULT CWindowCommand::OnCreate(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled)
{
	bHandled = FALSE;
	return 0;
}

LRESULT CWindowCommand::OnClose(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled)
{
	DispInvoke(_T("close"), NULL);
	bHandled = TRUE;
	return 0;
}

LRESULT CWindowCommand::OnErasebkgnd(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL & bHandled)
{
	bHandled = FALSE;
	return 0;
}

void CWindowCommand::Reload()
{
	CComPtr<IWebBrowser2> b = cHTMLView->getWebBrowser();
	if (b)
	{
		CComVariant vtEmpty, vtPostData, vtHeaders;
		if (!m_post.IsEmpty())
		{
			CComSafeArray<char, VT_UI1> psa;
			// post data is specified.
			CT2A postData(_iAlertModel->buildPropertyString(m_post, m_encoding ? m_encoding : CP_UTF8, true));
			psa.Add(strlen(postData), postData);
			vtPostData = psa;
			vtHeaders = L"Content-Type: application/x-www-form-urlencoded\n";
		}
		HRESULT resNavigate = b->Navigate2(&CComVariant(_iAlertModel->buildPropertyString(m_url, m_encoding ? m_encoding : CP_UTF8, true)), &vtEmpty, &vtEmpty, &vtPostData, &vtHeaders);
		DeskAlertsAssert(SUCCEEDED(resNavigate));
	}
}

LRESULT CWindowCommand::OnTimer(UINT /*uMsg*/, WPARAM wParam, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	if (wParam == _RELOAD_TIMER_)
	{
		Reload();
		//winDisp->closeAllExpiredAlerts();
	}
	else if (wParam == _SCREENSAVER_DESKTOP_TIMER_)
	{
		::SetWindowPos(GetParent(), HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE);
	}
	return 0;
}




void CWindowCommand::CallJsQueue(std::deque<shared_ptr<CallFuncStruct>> &queue, CHTMLView *view)
{
	CComQIPtr<IHTMLDocument2> pDoc = GetDocumentFromView(view);
	if (pDoc)
	{
		CComPtr<IDispatch> pScript;
		if (SUCCEEDED(pDoc->get_Script(&pScript)) && pScript)
		{
			std::deque<shared_ptr<CallFuncStruct>>::iterator it = queue.begin();
			while (it != queue.end())
			{
				shared_ptr<CallFuncStruct> callFuncStruct = *it;
				if (pScript.InvokeN(callFuncStruct->strFuncName, callFuncStruct->pvarParams->GetData(), callFuncStruct->pvarParams->GetSize()) == S_OK)
				{
					it = queue.erase(it);
				}
				else
				{
					it++;
				}
			}
		}
	}
}

CComQIPtr<IHTMLDocument2> CWindowCommand::GetDocumentFromView(CHTMLView *view)
{
	CComPtr<IWebBrowser2> pBrowser;
	CComQIPtr<IHTMLDocument2> pDoc;
	pBrowser = view->getWebBrowser();

	if (pBrowser)
	{
		CComPtr<IDispatch> pDocDisp;
		if (SUCCEEDED(pBrowser->get_Document(&pDocDisp)) && pDocDisp)
		{
			pDoc = pDocDisp;
		}
	}
	return pDoc;
}

BOOL CWindowCommand::CallJsInView(shared_ptr<CallFuncStruct> callFuncStruct, CHTMLView* view)
{
	CComQIPtr<IHTMLDocument2> pDoc = GetDocumentFromView(view);
	if (pDoc)
	{
		CComBSTR state;
		if (SUCCEEDED(pDoc->get_readyState(&state)) && (state == L"complete"/* || state == L"interactive"*/))
		{
			//Retrieving script object
			CComPtr<IDispatch> pScript;
			if (SUCCEEDED(pDoc->get_Script(&pScript)) && pScript)
			{
				return pScript.InvokeN(callFuncStruct->strFuncName, callFuncStruct->pvarParams->GetData(), callFuncStruct->pvarParams->GetSize()) == S_OK;
			}
		}
	}
	return FALSE;
}

void CWindowCommand::CallJs(shared_ptr<CallFuncStruct> callFuncStruct, BOOL inToolbar /*= FALSE*/)
{
	CHTMLView* view = (inToolbar) ? cHTMLTool : cHTMLView;
	if (!CallJsInView(callFuncStruct, view))
	{
		(inToolbar) ? captionCalls.push_back(callFuncStruct) : bodyCalls.push_back(callFuncStruct);
	}
}

void CWindowCommand::InjectJS(ATL::CString &URL, CString &URLDef)
{
	CComPtr<IDispatch> pDisp = 0;
	CComPtr<IWebBrowser2> b = cHTMLView->getWebBrowser();
	if (b && SUCCEEDED(b->get_Document(&pDisp)) && pDisp)
	{
		//add script to document
		CComQIPtr<IHTMLDocument2> pDoc2 = pDisp;
		if (pDoc2)
		{
			CComPtr<IHTMLWindow2> pWin;
			if (SUCCEEDED(pDoc2->get_parentWindow(&pWin)) && pWin)
			{
				CComVariant v;
				CString text = JSLoader.GetText(URL);
				if (text.IsEmpty() && GetLastError() != 0)
				{
					text = JSLoader.GetText(URLDef);
				}
				if (!text.IsEmpty())
				{
					pWin->execScript(CComBSTR(text), CComBSTR(_T("JavaScript")), &v);
				}
			}
		}
	}
}

void CWindowCommand::InjectCSS(ATL::CString &URL, CString &URLDef)
{
	CComDispatchDriver dispatch;
	CComPtr<IWebBrowser2> b = cHTMLView->getWebBrowser();
	if (b && SUCCEEDED(b->get_Document(&dispatch)) && dispatch)
	{
		//add css to document
		CComQIPtr<IHTMLDocument2> doc2 = dispatch;

		CComPtr<IHTMLStyleSheet> css;
		if (doc2 && SUCCEEDED(doc2->createStyleSheet(CComBSTR(), 0, &css)) && css)
		{
			CString text = CSSLoader.GetText(URL);
			if (text.IsEmpty() && GetLastError() != 0)
			{
				text = CSSLoader.GetText(URLDef);
			}
			if (!text.IsEmpty())
			{
				CString str = _iAlertModel->buildPropertyString(text);
				str.Replace(_T("%width%"), data.m_width);
				str.Replace(_T("%height%"), data.m_height);
				str.Replace(_T("%topmargin%"), data.m_topmargin);
				str.Replace(_T("%bottommargin%"), data.m_bottommargin);
				str.Replace(_T("%leftmargin%"), data.m_leftmargin);
				str.Replace(_T("%rightmargin%"), data.m_rightmargin);
				str.Replace(_T("%transpcolor%"), data.m_transpcolor);
				css->put_cssText(CComBSTR(str));
			}
		}
	}
}

void CWindowCommand::OnDocComplete(CHTMLView* HTMLView)
{
	if (HTMLView == cHTMLView) //body
	{
		if (!data.m_bodyclassname.IsEmpty())
		{
			CComDispatchDriver dispatch;
			CComQIPtr<IHTMLDocument2> doc2;
			CComPtr<IWebBrowser2> b = cHTMLView->getWebBrowser();
			if (b && SUCCEEDED(b->get_Document(&dispatch)) && dispatch && (doc2 = dispatch))
			{
				CComPtr<IHTMLElement> body;
				if (SUCCEEDED(doc2->get_body(&body)) && body)
				{
					CComBSTR className;
					body->get_className(&className);
					className += _T(" ") + data.m_bodyclassname;
					body->put_className(className);
				}
			}
		}
		/*if(!data.m_frameclassname.IsEmpty())
		{
			CComPtr<IHTMLElement> body;
			if(doc2 && SUCCEEDED(doc2->get_body(&body)) && body)
			{
				CComBSTR className;
				body->get_className(&className);
				className += _T(" ") + data.m_frameclassname;
				body->put_className(className);
			}
		}*/
		const int encode = _iAlertModel->getProperty(CString(_T("encoding")));
		CString css_url = _iAlertModel->buildPropertyString(data.m_customcss, encode, true);

		if (css_url.Find(_iAlertModel->getPropertyString(CString(_T("server")))) == -1
			&& data.m_captionhref.Find(_iAlertModel->getPropertyString(CString(_T("root_path")))) == -1 && data.m_captionhref.MakeLower().Find(CString(_T("%root_path%"))) == -1)
		{
			css_url = CString(_T("file:////")) + _iAlertModel->getPropertyString(CString(_T("root_path"))) + css_url;
		}

		CString css_url_def = _iAlertModel->buildPropertyString(data.m_customcss_def, encode, true);

		if (css_url_def.Find(_iAlertModel->getPropertyString(CString(_T("server")))) == -1
			&& data.m_captionhref.Find(_iAlertModel->getPropertyString(CString(_T("root_path")))) == -1 && data.m_captionhref.MakeLower().Find(CString(_T("%root_path%"))) == -1)
		{
			css_url_def = CString(_T("file:////")) + _iAlertModel->getPropertyString(CString(_T("root_path"))) + css_url_def;
		}

		if (!css_url.IsEmpty())
		{
			InjectCSS(css_url, css_url_def);
		}
		CString js_url = _iAlertModel->buildPropertyString(data.m_customjs, encode, true);

		if (js_url.Find(_iAlertModel->getPropertyString(CString(_T("server")))) == -1
			&& data.m_captionhref.Find(_iAlertModel->getPropertyString(CString(_T("root_path")))) == -1 && data.m_captionhref.Find(CString(_T("%root_path%"))) == -1)
		{
			js_url = CString(_T("file:////")) + _iAlertModel->getPropertyString(CString(_T("root_path"))) + js_url;
		}

		CString js_url_def = _iAlertModel->buildPropertyString(data.m_customjs_def, encode, true);

		if (js_url_def.Find(_iAlertModel->getPropertyString(CString(_T("server")))) == -1
			&& data.m_captionhref.Find(_iAlertModel->getPropertyString(CString(_T("root_path")))) == -1 && data.m_captionhref.Find(CString(_T("%root_path%"))) == -1)
		{
			js_url_def = CString(_T("file:////")) + _iAlertModel->getPropertyString(CString(_T("root_path"))) + js_url_def;
		}
		if (!js_url.IsEmpty())
		{
			InjectJS(js_url, js_url_def);

			//get unreadable alerts
			std::vector<int> alert_ids;
			for (map<CString, pair<int, shared_ptr<showData>>>::iterator it = NotReadAlerts.begin(); it != NotReadAlerts.end(); ++it)
			{
				alert_ids.push_back(it->second.first);
			}

			std::sort(alert_ids.begin(), alert_ids.end());
			//create string
			CString str;
			CString elem;
			for (size_t i = 0; i < alert_ids.size(); i++)
			{
				if (str.GetLength() > 0) str += ",";
				elem.Format(_T("%d"), alert_ids[i]);
				str += elem;
			}

			shared_ptr<CallFuncStruct> searchFuncStruct(new CallFuncStruct(_T("setSearchTerm"), data.m_searchTerm));
			CallJsInView(searchFuncStruct, HTMLView);

			shared_ptr<CallFuncStruct> callFuncStruct(new CallFuncStruct(_T("Initial"), str));
			CallJsInView(callFuncStruct, HTMLView);
		}
		CallJsQueue(bodyCalls, cHTMLView);
	}
	else if (HTMLView == cHTMLTool) //caption
	{
		CallJsQueue(captionCalls, cHTMLTool);
	}
	const HANDLE pHandle = GetCurrentProcess();
	SetProcessWorkingSetSize(pHandle, (SIZE_T)-1, (SIZE_T)-1);
	CloseHandle(pHandle);
}

BOOL CWindowCommand::OnNavigateError(CHTMLView* HTMLView, CString URL, CString /*TargetFrameName*/, LONG /*StatusCode*/)
{
	if (HTMLView == cHTMLTool/* &&
		400 <= StatusCode && StatusCode <= 599*/ &&
		!data.m_captionhref_def.IsEmpty() &&
		data.m_captionhref != data.m_captionhref_def)
	{
		data.m_captionhref = data.m_captionhref_def;
		data.m_skinId = data.m_skinId_def;

		data.m_captionhref_def.Empty();
		data.m_skinId_def.Empty();

		CComVariant vtEmpty, vtPostData, vtHeaders;

		if (data.m_captionhref.MakeUpper().Find(_iAlertModel->getPropertyString(CString(_T("server"))).MakeUpper()) == -1
			&& data.m_captionhref.MakeUpper().Find(_iAlertModel->getPropertyString(CString(_T("root_path"))).MakeUpper()) == -1 && data.m_captionhref.Find(CString(_T("%root_path%"))) == -1)
		{
			data.m_captionhref = CString(_T("file:////")) + _iAlertModel->getPropertyString(CString(_T("root_path"))) + data.m_captionhref;
		}


		HRESULT resNavigate = cHTMLTool->getWebBrowser()->Navigate2(&CComVariant(_iAlertModel->buildPropertyString(data.m_captionhref, _iAlertModel->getProperty(CString(_T("encoding"))), true)), &vtEmpty, &vtEmpty, &vtEmpty, &vtEmpty);
		DeskAlertsAssert(SUCCEEDED(resNavigate));
	}
	return FALSE;
}

CRect CWindowCommand::Reposition(long m_width, long m_heigh, bool checkOverflow)
{
	CRect rect, tmpRect;
	BOOL systemparams_success = SystemParametersInfo(SPI_GETWORKAREA, 0, &rect, 0);
	DeskAlertsAssert(systemparams_success);
	int mw = rect.Width();
	int mh = rect.Height();
	int mx = rect.left;
	int my = rect.top;

    //todo: рефакторинг: проверять валидность second.second, выполнять вычисление для каждого/проверять тип алерта

	map<CString, pair<int, shared_ptr<showData>>>::iterator it = NotReadAlerts.begin();
    DeskAlertsAssert(it->second.second);
	CRect newRect = it->second.second->windowRect;

	if (!newRect.IsRectNull())
	{
		if (data.m_position == _T("top") || data.m_position == _T("bottom") || data.m_position == _T("middle"))
		{
			m_width = mw;
			if (data.m_position == _T("bottom"))
				newRect.top = mh - m_heigh;
			else if (data.m_position == _T("middle"))
				newRect.top = mh / 2 - m_heigh / 2;
		}
		newRect.right = newRect.left + m_width;
		newRect.bottom = newRect.top + m_heigh;
		while (it != NotReadAlerts.end())
		{
			it->second.second->windowRect = newRect;
			it++;
		}
		return newRect;
	}

	const int iWindowCount = data.m_iWindowCount;
	if (data.m_position == _T("center"))
	{
		newRect.top = my + mh / 2 - m_heigh / 2 + 20 * iWindowCount;
		newRect.bottom = my + mh / 2 + m_heigh / 2 + 20 * iWindowCount;
		newRect.left = mx + mw / 2 - m_width / 2 + 20 * iWindowCount;
		newRect.right = mx + mw / 2 + m_width / 2 + 20 * iWindowCount;
	}
	else if (data.m_position == _T("right-bottom") || data.m_position == _T("bottom-right") || data.m_position == _T(""))
	{
		newRect.top = my + mh - m_heigh - 20 * iWindowCount;
		newRect.bottom = my + mh - 20 * iWindowCount;
		newRect.left = mx + mw - m_width - 20 * iWindowCount;
		newRect.right = mx + mw - 20 * iWindowCount;
	}
	else if (data.m_position == _T("left-top") || data.m_position == _T("top-left"))
	{
		newRect.top = my + 20 * iWindowCount;
		newRect.bottom = my + m_heigh + 20 * iWindowCount;
		newRect.left = mx + 20 * iWindowCount;
		newRect.right = mx + m_width + 20 * iWindowCount;
	}
	else if (data.m_position == _T("left-bottom") || data.m_position == _T("bottom-left"))
	{
		newRect.top = my + mh - m_heigh - 20 * iWindowCount;
		newRect.bottom = my + mh - 20 * iWindowCount;
		newRect.left = mx + 20 * iWindowCount;
		newRect.right = mx + m_width + 20 * iWindowCount;
	}
	else if (data.m_position == _T("right-top") || data.m_position == _T("top-right"))
	{
		newRect.top = my + 20 * iWindowCount;
		newRect.bottom = my + m_heigh + 20 * iWindowCount;
		newRect.left = mx + mw - m_width - 20 * iWindowCount;
		newRect.right = mx + mw - 20 * iWindowCount;
	}
	else if (data.m_position == _T("bottom"))
	{
		newRect.top = my + mh - m_heigh;
		newRect.bottom = my + mh;
		newRect.left = mx;
		newRect.right = mx + mw;
		if (data.m_docked == _T("1")) {
			if (SubtractRect(tmpRect, rect, newRect)) {
				systemparams_success = SystemParametersInfo(SPI_SETWORKAREA, sizeof(tmpRect), tmpRect, SPIF_UPDATEINIFILE | SPIF_SENDWININICHANGE);
				DeskAlertsAssert(systemparams_success);
			}
		}
		checkOverflow = false;
	}
	else if (data.m_position == _T("1"))
	{
		newRect.top = my + mh - m_heigh;
		newRect.bottom = my + mh;
		newRect.left = mx;
		newRect.right = mx + mw;
		if (SubtractRect(tmpRect, rect, newRect)) {
			systemparams_success = SystemParametersInfo(SPI_SETWORKAREA, sizeof(tmpRect), tmpRect, SPIF_UPDATEINIFILE | SPIF_SENDWININICHANGE);
			DeskAlertsAssert(systemparams_success);
		}
		checkOverflow = false;
	}
	else if (data.m_position == _T("top"))
	{
		newRect.top = my;
		newRect.bottom = my + m_heigh;
		newRect.left = mx;
		newRect.right = mx + mw;
		checkOverflow = false;
		/*if (SubtractRect(tmpRect, rect, newRect)){
			SystemParametersInfo(SPI_SETWORKAREA,sizeof(tmpRect),tmpRect,SPIF_UPDATEINIFILE | SPIF_SENDWININICHANGE);
		}*/
	}
	else if (data.m_position == _T("middle"))
	{
		newRect.top = my + mh / 2 - m_heigh / 2;
		newRect.bottom = my + mh / 2 + m_heigh / 2;
		newRect.left = mx;
		newRect.right = mx + mw;
		checkOverflow = false;
	}
	else if (data.m_position == _T("maximized") || data.m_position == _T("maximize"))
	{
		newRect.top = my;
		newRect.bottom = my + mh;
		newRect.left = mx;
		newRect.right = mx + mw;
		checkOverflow = false;
	}
	else if (data.m_position == _T("fullscreen"))
	{
		newRect.top = 0;
		newRect.bottom = GetSystemMetrics(SM_CYSCREEN);
		newRect.left = 0;
		newRect.right = GetSystemMetrics(SM_CXSCREEN);
		checkOverflow = false;
	}
	else
	{
		DeskAlertsUnexpected();
	}

	//-- If alert size bigger than display size - make it smaller
	if (m_width > mw) {
		newRect.left = 0;
		newRect.right = GetSystemMetrics(SM_CXSCREEN);
	}
	if (m_heigh > mh) {
		newRect.top = 0;
		newRect.bottom = GetSystemMetrics(SM_CYSCREEN);
	}

	if (checkOverflow && (
		newRect.top < rect.top ||
		newRect.left < rect.left ||
		newRect.bottom > rect.bottom ||
		newRect.right > rect.right))
	{
		winDisp->m_iWindowCount = 0;
		data.m_iWindowCount = winDisp->m_iWindowCount + data.m_windowsCountDelta;
		newRect = Reposition(m_width, m_heigh, false);
	}

	return newRect;
}

#define updateDataFromIt(x) if(!(*it)->x.IsEmpty()) data.x = (*it)->x;

bool CWindowCommand::show(CString url, shared_ptr<showData> ex, BOOL isNewAlert)
{
	AlertsInWindow = 1;
	m_url = url;
	NotReadAlerts[m_url] = pair<int, shared_ptr<showData>>(0, ex);

    //todo: возможно тут нужен асерт
	if (ex)
	{
		data.m_acknowledgement = ex->acknowledgement;
		data.m_urgent = ex->urgent;
		data.m_create_date = ex->create_date;
		data.m_date = ex->date;
		data.m_title = ex->title;
		data.m_UTC = ex->UTC;
		data.m_alertid = ex->alertid;
		data.m_autoclose = ex->autoclose;
		data.m_resizable = ex->resizable;
		data.m_to_date = ex->to_date;
		data.m_hide_close = ex->hide_close;
		data.m_ticker = ex->ticker;
		if (!ex->position.IsEmpty()) data.m_position = ex->position;
		if (!ex->docked.IsEmpty()) data.m_docked = ex->docked;
		if (!ex->self_deletable.IsEmpty()) data.m_self_deletable = ex->self_deletable;
		if (!ex->width.IsEmpty()) data.m_width = ex->width;
		if (!ex->height.IsEmpty()) data.m_height = ex->height;
		data.m_topTemplateHTML = ex->topTemplateHTML;
		data.m_bottomTemplateHTML = ex->bottomTemplateHTML;
		if (!ex->expire.IsEmpty()) data.m_expire = ex->expire;
		data.m_searchTerm = ex->searchTerm;
		m_post = ex->post;
		m_encoding = ex->encoding;
		m_history_id = ex->history_id;

		if (CRect(ex->windowRect).IsRectNull())
		{
			data.m_iWindowCount = winDisp->m_iWindowCount + ex->windowsCountDelta;
			data.m_windowsCountDelta = ex->windowsCountDelta;
		}

		if (!ex->cdata.IsEmpty())
		{
			NodeALERT mNodeALERT;
			XMLParser parser(&mNodeALERT, _T("ALERT"));
			parser.Parse(_T("<ALERT>") + ex->cdata + _T("</ALERT>"), false);
			vector<NodeALERTS::NodeVIEWS::NodeWINDOW*>::iterator it = mNodeALERT.m_windows.begin();
			while (it != mNodeALERT.m_windows.end())
			{
				updateDataFromIt(m_command);
				updateDataFromIt(m_img);
				updateDataFromIt(m_captionhref);
				updateDataFromIt(m_skinId);
				updateDataFromIt(m_resizable);
				updateDataFromIt(m_caption);
				updateDataFromIt(m_customjs);
				updateDataFromIt(m_customcss);
				updateDataFromIt(m_bodyclassname);
				updateDataFromIt(m_frameclassname);

				updateDataFromIt(m_width);
				updateDataFromIt(m_height);

				updateDataFromIt(m_leftmargin);
				updateDataFromIt(m_topmargin);
				updateDataFromIt(m_rightmargin);
				updateDataFromIt(m_bottommargin);
				updateDataFromIt(m_confirmation);
				updateDataFromIt(m_position);
				updateDataFromIt(m_docked);
				updateDataFromIt(m_self_deletable);
				//////////////////////////////////////////////////////////////////////////////////////////////
				/// just no!! don't load it from history! it is DA Client configuration value, not alert value
				//updateDataFromIt(m_transparency);
				//updateDataFromIt(m_transpcolor);

				updateDataFromIt(m_visible);
				updateDataFromIt(m_single);
				updateDataFromIt(m_lockdesktop);

				updateDataFromIt(m_acknowledgement);
				updateDataFromIt(m_urgent);
				updateDataFromIt(m_create_date);
				updateDataFromIt(m_title);
				updateDataFromIt(m_autoclose);
				updateDataFromIt(m_alertid);
				updateDataFromIt(m_topTemplateHTML);
				updateDataFromIt(m_bottomTemplateHTML);

				updateDataFromIt(m_expire);

				updateDataFromIt(m_skip_https_check);

				++it;
			}
		}
	}

	CString capt = data.m_captionhref;
	bool titled = true;
	m_url = url;
    DeskAlertsAssert(m_pHandler);
	CComPtr<IDispatch> sp_Handler = m_pHandler;
	m_pHandler->addMyDispatchListener(this);
	if (capt != _T("")) titled = false;

	int m_width = CAlertUtils::str2int(data.m_width, DEFAULT_WINDOW_WIDTH);
	int m_heigh = CAlertUtils::str2int(data.m_height, DEFAULT_WINDOW_HEIGHT);

	CRect minibrowserRect;
    CRect minibrowserRectAppBar;
    DPI_AWARENESS_CONTEXT old_DPIAware(NULL);
    
    //////////////////////////////////////////////////////
    /// set bottom ticker appbar area based on current monitor DPI 
    /// for win 8.1 or greater
    /// calc Scaling 
    double yscreenScale = 1;
    double xscreenScale = 1;
    int screenWorkAreaRealWidth = 0;
    if (ex->ticker && data.m_position == "bottom")
    {
        CRect rect;
        HMONITOR hOrigMonitor = ::MonitorFromWindow(winDisp->getParentHWND() , MONITOR_DEFAULTTOPRIMARY);
        DeskAlertsAssert(hOrigMonitor);
        MONITORINFO _monitorinfo;
        _monitorinfo.cbSize = sizeof(MONITORINFO);
        BOOL isMonitorInfo = GetMonitorInfoA(hOrigMonitor, &_monitorinfo);
        DeskAlertsAssert(isMonitorInfo);
        APPBARDATA abd;
        abd.cbSize = sizeof(APPBARDATA);
        abd.hWnd = m_hWnd;
        
        //if (SHAppBarMessage(ABM_GETTASKBARPOS, &abd))
        //{
        //    yscreenScale = (double)(abd.rc.bottom - abd.rc.top) / (double)(_monitorinfo.rcMonitor.bottom - _monitorinfo.rcWork.bottom);
        //    xscreenScale = (double)(abd.rc.right - abd.rc.left) / (double)(_monitorinfo.rcMonitor.right - _monitorinfo.rcWork.left);
        //}
        /////////////////////////////
        // get scaling by DpiHelper
        {
            std::vector<DISPLAYCONFIG_PATH_INFO> pathsV;
            std::vector<DISPLAYCONFIG_MODE_INFO> modesV;
            int flags = QDC_ONLY_ACTIVE_PATHS;
            /*if (false == */DpiHelper::GetPathsAndModes(pathsV, modesV, flags);
            for (const auto& path : pathsV)
            {
                //get display name
                auto adapterLUID = path.targetInfo.adapterId;
                auto targetID = path.targetInfo.id;
                auto sourceID = path.sourceInfo.id;

                DpiHelper::DPIScalingInfo dpiInfo = DpiHelper::GetDPIScalingInfo(adapterLUID, sourceID);

                DISPLAYCONFIG_TARGET_DEVICE_NAME deviceName;
                deviceName.header.size = sizeof(deviceName);
                deviceName.header.type = DISPLAYCONFIG_DEVICE_INFO_GET_TARGET_NAME;
                deviceName.header.adapterId = adapterLUID;
                deviceName.header.id = targetID;
                /*if (ERROR_SUCCESS != */DisplayConfigGetDeviceInfo(&deviceName.header);
                if (DISPLAYCONFIG_OUTPUT_TECHNOLOGY_INTERNAL == deviceName.outputTechnology)
                {
                }
                UINT IndexMode = 0UL;
                for (const auto& modes : modesV)
                {

                    if (IndexMode == path.sourceInfo.modeInfoIdx && modes.sourceMode.position.x == 0L && modes.sourceMode.position.y == 0L)
                    {
                        yscreenScale = xscreenScale = (double)dpiInfo.current/(double)100;
                    }
                    IndexMode++;
                }

            }
            
        }
        
        screenWorkAreaRealWidth = _monitorinfo.rcWork.right - _monitorinfo.rcWork.left;// _monitorinfo.rcMonitor.right - _monitorinfo.rcWork.left;
    }
    /////////////////////////////
    /// set border to webbrowser
    minibrowserRect = Reposition(m_width, m_heigh);
    minibrowserRectAppBar = minibrowserRect;
    
	HWND parentHWND = winDisp->getParentHWND();
	BOOL skipHttpsCheck = data.m_skip_https_check == _T("true") || data.m_skip_https_check == _T("1");
	// CR killall5: fix equal expression
	if (!skipHttpsCheck && data.m_skip_https_check != _T("false") && data.m_skip_https_check != _T("0") && _iAlertModel)
	{
		CString strSkipHttpsCheck = _iAlertModel->getPropertyString(CString(_T("skip_https_check")));
		skipHttpsCheck = (strSkipHttpsCheck == _T("1") || strSkipHttpsCheck == _T("true"));
	}

	DWORD style;
	if (titled)
	{
		style = WS_POPUP | \
			WS_CAPTION | \
			WS_SYSMENU | \
			WS_THICKFRAME | \
			WS_MINIMIZEBOX | \
			WS_MAXIMIZEBOX;
		cHTMLTool = NULL;
	}
	else
	{
		style = WS_POPUP;
	}
    /////////////////////////////////////
    /// reg AppBar border base on scaling
    /// only for bottom tickers
    if (data.m_visible != _T("false"))
    {
        if (ex->ticker && data.m_position == "bottom")
        {
            minibrowserRectAppBar.top *= yscreenScale;
            minibrowserRectAppBar.bottom *= yscreenScale;
            if (screenWorkAreaRealWidth) minibrowserRectAppBar.right = screenWorkAreaRealWidth;
            RegisterAccessBar(parentHWND, minibrowserRectAppBar, ABE_BOTTOM);
            _iAlertModel->setProperty(CString(_T("appbarAccessHWND")), (DWORD)parentHWND);
        }
    }
	HWND hWnd = Create(parentHWND, minibrowserRect, _T("about:blank"), style, WS_EX_TOPMOST);
	DeskAlertsAssert(hWnd);
	if (!m_hWnd)
	{
		DeskAlertsUnexpected();
		return false;
	}
	SetWindowText(data.m_caption);

	CRect clientRecTool;
	CRect clientRecView;
	CRect clientOrig;

	BOOL getclientrect_success = ::GetClientRect(m_hWnd, &clientOrig);
	DeskAlertsAssert(getclientrect_success);
	clientRecView = clientOrig;
	if (!titled)
	{
		clientRecTool = clientOrig;
		//clientRecTool.bottom = 30;
		clientRecView.top = CAlertUtils::str2int(data.m_topmargin, 32);
		clientRecView.bottom -= CAlertUtils::str2int(data.m_bottommargin, 10);
		clientRecView.left += CAlertUtils::str2int(data.m_leftmargin, 10);
		clientRecView.right -= CAlertUtils::str2int(data.m_rightmargin, 10);

		cHTMLTool = new CHTMLView();
		HWND resHWND = cHTMLTool->HTMLCreate(m_hWnd, minibrowserRect, _T("about:blank"),
			WS_CHILD | \
			WS_CLIPCHILDREN | \
			WS_CLIPSIBLINGS | \
			WS_OVERLAPPED, 0,
			skipHttpsCheck);
		if (NULL == resHWND) 
		{ 
			delete_catch( cHTMLTool);
			delete_catch(cHTMLView);
			::DestroyWindow(hWnd);
			return false; 
		}
		if (_iAlertController->getInitFlags() & DISABLE_EXTERNAL_LINKS)
		{
			cHTMLTool->disableExternalLinks();
		}
        //todo: Добавить валидацию по размеру экрана монитора
		BOOL movewindow = cHTMLTool->MoveWindow(clientRecTool, TRUE);
		DeskAlertsAssert(movewindow);

		cHTMLTool->AddDocCompleteListener(this);

		CComVariant vtEmpty;
		try
		{
			CString captionHref = data.m_captionhref.MakeUpper();
			if (captionHref.Find(_iAlertModel->getPropertyString(CString(_T("server"))).MakeUpper()) == -1
				&& captionHref.Find(_iAlertModel->getPropertyString(CString(_T("root_path"))).MakeUpper()) == -1 && captionHref.MakeLower().Find(CString(_T("%root_path%"))) == -1)
			{
				captionHref = CString(_T("file:////")) + _iAlertModel->getPropertyString(CString(_T("root_path"))) + data.m_captionhref;
			}


			HRESULT resNavigate = cHTMLTool->getWebBrowser()->Navigate2(&CComVariant(_iAlertModel->buildPropertyString(captionHref, _iAlertModel->getProperty(CString(_T("encoding"))), false)), &vtEmpty, &vtEmpty, &vtEmpty, &vtEmpty);
			if (HRESULT_FROM_WIN32(ERROR_CANCELLED) == resNavigate)
			{
				delete_catch(cHTMLTool);
				delete_catch(cHTMLView);
				::DestroyWindow(hWnd);
				return false;
			}
				
            DeskAlertsAssert(SUCCEEDED(resNavigate));

		}
		catch(...)
		{
			DeskAlertsUnexpected();
		}
		//release_catch_tolog(_T("HTMLTool Navigate2 external browser call exception"));//todo: идентифицировать экс

		HRESULT hr = cHTMLTool->SetExternalDispatch(sp_Handler);
		DeskAlertsAssert(SUCCEEDED(hr));
	}

	//alert body
	cHTMLView = new CHTMLView();
	HWND hWndTools = cHTMLView->HTMLCreate(m_hWnd, minibrowserRect, _T("about:blank"),
		WS_CHILD | \
		WS_CLIPCHILDREN | \
		WS_CLIPSIBLINGS | \
		WS_HSCROLL | \
		WS_VSCROLL | \
		WS_OVERLAPPED, 0,
		skipHttpsCheck);
	if (NULL == hWndTools) 
	{
		delete_catch(cHTMLTool);
		delete_catch(cHTMLView);
		::DestroyWindow(hWnd);
		return false;
	}
	if (_iAlertController->getInitFlags() & DISABLE_EXTERNAL_LINKS)
	{
		cHTMLView->disableExternalLinks();
	}

	BOOL movewindow = cHTMLView->MoveWindow(clientRecView, TRUE);
	DeskAlertsAssert(movewindow);
	HRESULT hr = cHTMLView->SetExternalDispatch(sp_Handler);
	DeskAlertsAssert(SUCCEEDED(hr));

	CComVariant vtEmpty, vtPostData, vtHeaders;
	if (!m_post.IsEmpty())
	{
		CComSafeArray<char, VT_UI1> psa;
		// post data is specified.
		CT2A postData(_iAlertModel->buildPropertyString(m_post, m_encoding ? m_encoding : CP_UTF8, true));
		psa.Add(strlen(postData), postData);
		vtPostData = psa;
		vtHeaders = L"Content-Type: application/x-www-form-urlencoded\n";
	}

	cHTMLView->AddDocCompleteListener(this);

	try
	{
			if (data.m_captionhref.MakeUpper().Find(_iAlertModel->getPropertyString(CString(_T("server"))).MakeUpper()) == -1
				&& data.m_captionhref.MakeUpper().Find(_iAlertModel->getPropertyString(CString(_T("root_path"))).MakeUpper()) == -1 && data.m_captionhref.MakeLower().Find(CString(_T("%root_path%"))) == -1)
			{
				data.m_captionhref = CString(_T("file:////")) + _iAlertModel->getPropertyString(CString(_T("root_path"))) + data.m_captionhref;
			}

            HRESULT resNavigate = cHTMLView->getWebBrowser()->Navigate2(&CComVariant(_iAlertModel->buildPropertyString(url, m_encoding ? m_encoding : CP_UTF8, true)), &vtEmpty, &vtEmpty, &vtPostData, &vtHeaders);
			if (HRESULT_FROM_WIN32(ERROR_CANCELLED) == resNavigate)
			{
				delete_catch(cHTMLTool);
				delete_catch(cHTMLView);
				::DestroyWindow(hWnd);
				return false;
			}
            DeskAlertsAssert(SUCCEEDED(resNavigate));
	}
	catch(...)
	{
		DeskAlertsUnexpected();
	}

	m_pFinderText = shared_ptr<CFinderText>(new CFinderText(cHTMLView->getWebBrowser()));
	DWORD color = 0, flags = 0;
	unsigned char transp = 0;
	if (!data.m_transpcolor.IsEmpty())
	{
		color = CAlertUtils::hexstr2long(data.m_transpcolor, 0);
		flags |= LWA_COLORKEY;
	}
	if (!data.m_transparency.IsEmpty())
	{
		int itr = CAlertUtils::str2int(data.m_transparency, 255);
		if (itr > 255) itr = 255;
		if (itr < 0) itr = 0;
		transp = itr & 255;
		flags |= LWA_ALPHA;
	}
	if (flags)
	{
		BOOL maketransparent = MakeWindowTransparent(this->m_hWnd, color, transp, flags);
		DeskAlertsAssert(maketransparent);
	}
	BOOL setpos_success = cHTMLView->SetWindowPos(HWND_TOP, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE | SWP_NOACTIVATE);
	DeskAlertsAssert(setpos_success);

	ModifyStyleEx(WS_EX_APPWINDOW, 0);
	if (cHTMLTool) cHTMLTool->ModifyStyleEx(WS_EX_APPWINDOW, 0);
	cHTMLView->ModifyStyleEx(WS_EX_APPWINDOW, 0);

	BOOL show_success;
	if (cHTMLTool)
	{
		show_success = cHTMLTool->ShowWindow(SW_SHOW);
		DeskAlertsAssert(show_success == 0);
	}
	show_success = cHTMLView->ShowWindow(SW_SHOW);
	DeskAlertsAssert(show_success == 0);

	if (data.m_visible != _T("false"))
	{
		show_success = ShowWindow(SW_SHOWNA);
		DeskAlertsAssert(show_success == 0);
		if (data.m_visible != _T("showna")) cHTMLView->SetFocus();
	}


	if (data.m_lockdesktop == _T("true"))
	{
		CLockWindow::AddWindow(m_hWnd);
	}

		int sec = CAlertUtils::str2sec(data.m_expire, 0);
		if (sec > 0)
			SetTimer(_RELOAD_TIMER_, sec * 1000);

	HDESK desktop = GetThreadDesktop(GetCurrentThreadId());
	CString desktopName = CAlertUtils::getDesktopName(desktop);
	if (desktopName.MakeLower() == _T("screen-saver"))
	{
		SetTimer(_SCREENSAVER_DESKTOP_TIMER_, 1000);
	}
	//
	// the code below is necessary to implement alert termination
	//
    if (ex->ticker && data.m_position == "bottom" && old_DPIAware != NULL)
    {
        //SetThreadDpiAwarenessContext(old_DPIAware);
    }
	if (isNewAlert)
	{
		sqlite_int64 alertId = (sqlite_int64)_wtoi(ex->alertid);
		auto dataBase = CDatabaseManager::shared(true);
		if (dataBase != nullptr)
		{
			dataBase->AddToAlertId_hWnds(alertId, (sqlite_int64)hWnd, (sqlite_int64)hWndTools);
		}
	}
	if (show_success == 0)
	{
		CDatabaseManager* dataBase = CDatabaseManager::shared(true);
		if (dataBase != nullptr)
		{
			dataBase->setAlertShowed((CAlertUtils::str2int64(ex->alertid)));
		}
	}
	return show_success == 0;
}

static APPBARDATA s_ticker_top;
static APPBARDATA s_ticker_bottom;
static int s_ticker_border = 1;

void AppBarSetPos(UINT uEdge, PAPPBARDATA pabd, BOOL isSwitchDesktop = FALSE)
{
    if (!SHAppBarMessage(ABM_SETPOS, pabd))
    {
        if(!isSwitchDesktop)
        DeskAlertsUnexpected();
    }
	

	pabd->rc.top = pabd->rc.top + s_ticker_border;
	pabd->rc.bottom = pabd->rc.bottom - s_ticker_border;
	MoveWindow(pabd->hWnd, pabd->rc.left, pabd->rc.top, pabd->rc.right - pabd->rc.left, pabd->rc.bottom - pabd->rc.top, TRUE);
	pabd->rc.top = pabd->rc.top - s_ticker_border;
	pabd->rc.bottom = pabd->rc.bottom + s_ticker_border;

	switch (uEdge)
	{
	case ABE_TOP:
		s_ticker_top = *pabd;
		if (s_ticker_bottom.hWnd != nullptr)
		{
			MoveWindow(s_ticker_bottom.hWnd, s_ticker_bottom.rc.left, s_ticker_bottom.rc.top, s_ticker_bottom.rc.right - s_ticker_bottom.rc.left, s_ticker_bottom.rc.bottom - s_ticker_bottom.rc.top, TRUE);
		}
		break;

	case ABE_BOTTOM:
		s_ticker_bottom = *pabd;
		if (s_ticker_top.hWnd != nullptr)
		{
			MoveWindow(s_ticker_top.hWnd, s_ticker_top.rc.left, s_ticker_top.rc.top, s_ticker_top.rc.right - s_ticker_top.rc.left, s_ticker_top.rc.bottom - s_ticker_top.rc.top, TRUE);
		}
		break;
	}
}

void CWindowCommand::RegisterAccessBar(HWND hwndAccessBar, LPRECT lprc, UINT uEdge)
{
	APPBARDATA abd;
	abd.cbSize = sizeof(APPBARDATA);
	abd.hWnd = hwndAccessBar;

	if (!SHAppBarMessage(ABM_NEW, &abd))
	{
		return;
	}

	abd.rc = *lprc;
	abd.uEdge = uEdge;

	AppBarSetPos(uEdge, &abd);
}

void CWindowCommand::UnregisterAccessBar(HWND hwndAccessBar, UINT uEdge, BOOL isSwitchDesktop)
{
	APPBARDATA abd;
	abd.cbSize = sizeof(APPBARDATA);
	abd.hWnd = hwndAccessBar;
	if (!SHAppBarMessage(ABM_REMOVE, &abd))
	{
        return;
	}

	switch (uEdge)
	{
	case ABE_TOP:
		s_ticker_top.hWnd = nullptr;
		AppBarSetPos(ABE_BOTTOM, &s_ticker_bottom, isSwitchDesktop);
		break;

	case ABE_BOTTOM:
		s_ticker_bottom.hWnd = nullptr;
		AppBarSetPos(ABE_TOP, &s_ticker_top, isSwitchDesktop);
		break;
	}
}

CComVariant* CWindowCommand::DispInvoke(CString name, DISPPARAMS *pDispParams)
{
#ifndef DEBUG
	if (g_bDebugMode)
#endif
	{
		CString stringArgs;
		if (pDispParams && pDispParams->cArgs > 0)
		{
			for (UINT i = pDispParams->cArgs; i > 0; i--)
			{
				const UINT id = i - 1;
				if (i != pDispParams->cArgs)
				{
					stringArgs += _T(", ");
				}
				if (pDispParams->rgvarg[id].vt == VT_DISPATCH)
					stringArgs += _T("[object]");
				else if (pDispParams->rgvarg[id].vt == VT_NULL)
					stringArgs += _T("[null]");
				else if (pDispParams->rgvarg[id].vt == VT_EMPTY)
					stringArgs += _T("[empty]");
				else if (pDispParams->rgvarg[id].vt & VT_BSTR)
					stringArgs += _T("\"") + BrowserUtils::variantToString(pDispParams->rgvarg[id]) + _T("\"");
				else
					stringArgs += BrowserUtils::variantToString(pDispParams->rgvarg[id]);
			}
		}
	}
	CComVariant* res = NULL;
	if (name.CompareNoCase(_T("close")) == 0)
	{
		CString url;
		if (data.m_confirmation == _T("1")
			&& !(url = _iAlertModel->getPropertyString(CString(_T("confirmationUrl")), &CString(), _iAlertModel->getProperty(CString(_T("encoding"))), true)).IsEmpty())
		{
			time_t tTime;  // C run-time time (defined in <time.h>)
			time(&tTime);  // Get the current time from the
			CString app;
			app.Format(L"%s%cread=1&closetime=%I64d", url, (url.Find(_T('?')) >= 0 ? _T('&') : _T('?')), tTime);
			//CAlertUtils::URLDownloadToFile(app);
			CAlertUtils::AsyncURLRequest(app);
		}
		HDESK desktop = GetThreadDesktop(GetCurrentThreadId());
		CString desktopName = CAlertUtils::getDesktopName(desktop);
		if (desktopName.MakeLower() == _T("winlogon"))
		{
			_iAlertController->getServiceBridge()->sendCloseWindowRequest(m_history_id);
		}
		else
		{
			CHistoryStorage::MarkAlertAsClosed(m_history_id);
			map<CString, pair<int, shared_ptr<showData>>>::iterator it = NotReadAlerts.begin();
			while (it != NotReadAlerts.end())
			{
				CHistoryStorage::MarkAlertAsClosed(it->second.second->history_id);
				if (it->second.second->self_deletable == "1") {
					CHistoryStorage::DeleteAlertByID(_ttoi(it->second.second->alertid));
				}
				it++;
			}
		}


		

		/*if (data.m_ticker && data.m_position == "top")
		{
			UnregisterAccessBar(m_hWnd, ABE_TOP);
		}
		else */if (data.m_ticker && data.m_position == "bottom")
		{
			UnregisterAccessBar(winDisp->getParentHWND() /*m_hWnd*/, ABE_BOTTOM);
		}
        //ShowWindow(SW_HIDE);
        DeleteFile(this->m_url);
        BOOL isPost = ::PostMessage(winDisp->m_hWnd, WM_KILL_ME, (WPARAM)this, 0);
        DeskAlertsAssert(isPost);
		//CRect rect, tmpRect;
		//if (data.m_position == "bottom" || data.m_position == "1" || data.m_position == "top") {
		//	SystemParametersInfo(SPI_GETWORKAREA, 0, &tmpRect, 0);
		//	this->GetWindowRect(&rect);
		//}
		//if (data.m_position == "bottom" || data.m_position == "1") {
		//	if (UnionRect(&tmpRect, &tmpRect, &rect)) {
		//		tmpRect.top -= workAreaDeltaBottom;
		//		tmpRect.bottom += workAreaDeltaTop;
		//		SystemParametersInfo(SPI_SETWORKAREA, sizeof(tmpRect), tmpRect, SPIF_UPDATEINIFILE | SPIF_SENDWININICHANGE);
		//	}
		//}

	}
	else if (name.CompareNoCase(_T("hide")) == 0)
	{
        ShowWindow(SW_HIDE);
	}
	else if (name.CompareNoCase(_T("hidePostponedAlert")) == 0)
	{
		ShowWindow(SW_HIDE);
	}
	else if (name.CompareNoCase(_T("show")) == 0)
	{
		ShowWindow(SW_SHOWNA);
		/*cHTMLTool->ShowWindow(SW_SHOW);
		cHTMLView->ShowWindow(SW_SHOW);*/
	}
	else if (name.CompareNoCase(_T("focus")) == 0)
	{
		SetActiveWindow();
		cHTMLView->SetFocus();
	}
	else if (name.CompareNoCase(_T("getFrameDocument")) == 0)
	{
		if (pDispParams->cArgs == 1)
		{
			CComVariant var0 = pDispParams->rgvarg[0];
			if (var0.vt == VT_DISPATCH)
			{
				CComQIPtr<IWebBrowser2> frame = var0.pdispVal;
				if (frame)
				{
					CComPtr<IDispatch> doc;
					if (SUCCEEDED(frame->get_Document(&doc)) && doc)
						res = new CComVariant(doc);
				}
			}
		}
	}
	else if (name.CompareNoCase(_T("setProperty")) == 0)
	{
		if (pDispParams->cArgs >= 2)
		{
			CString strVar0;
			CString strVar1;
			CComVariant var0 = pDispParams->rgvarg[0];
			CComVariant var1 = pDispParams->rgvarg[1];
			if (var0.vt == VT_BSTR)
				strVar1 = var0.bstrVal;
			if (var1.vt == VT_BSTR)
				strVar0 = var1.bstrVal;

			if (strVar0 == _T("manualclose"))
				m_manualclose = (strVar1 == _T("1"));
			else
			{
				_iAlertModel->setPropertyString(strVar0, strVar1);
				res = new CComVariant(_iAlertModel->getPropertyString(CString(strVar0)));
				if (strVar0 == _T("user_name") || strVar0 == _T("domain_name") || strVar0 == _T("md5_password")) {
					_iAlertController->loadAppCredentials();
				}
			}
		}
	}
	else if (name.CompareNoCase(_T("getProperty")) == 0)
	{
		if (pDispParams->cArgs >= 1)
		{
			CComBSTR strVar0;
			CComVariant var0 = pDispParams->rgvarg[pDispParams->cArgs - 1];
			if (var0.vt == VT_BSTR)
				strVar0 = var0.bstrVal;
			CComBSTR str;
			if (strVar0 == _T("skin_id"))
			{
				str = data.m_skinId;
			}
			else if (strVar0 == _T("alertid"))
				str = data.m_alertid;
			else if (strVar0 == _T("acknowledgement"))
				str = data.m_acknowledgement;
			else if (strVar0 == _T("urgent"))
				str = data.m_urgent;
			else if (strVar0 == _T("autoclose"))
				str = data.m_autoclose;
			else if (strVar0 == _T("create_date"))
				str = data.m_create_date;
			else if (strVar0 == _T("date"))
				str = data.m_date;
			else if (strVar0 == _T("title"))
				str = data.m_title;
			else if (strVar0 == _T("caption"))
				str = data.m_caption;
			else if (strVar0 == _T("height"))
				str = data.m_height;
			else if (strVar0 == _T("width"))
				str = data.m_width;
			else if (strVar0 == _T("topmargin"))
				str = data.m_topmargin;
			else if (strVar0 == _T("bottommargin"))
				str = data.m_bottommargin;
			else if (strVar0 == _T("leftmargin"))
				str = data.m_leftmargin;
			else if (strVar0 == _T("rightmargin"))
				str = data.m_rightmargin;
			else if (strVar0 == _T("confirmation"))
				str = data.m_confirmation;
			else if (strVar0 == _T("position"))
				str = data.m_position;
			else if (strVar0 == _T("docked"))
				str = data.m_docked;
			else if (strVar0 == _T("self_deletable"))
				str = data.m_self_deletable;
			else if (strVar0 == _T("transparency"))
				str = data.m_transparency;
			else if (strVar0 == _T("transpcolor"))
				str = data.m_transpcolor;
			else if (strVar0 == _T("visible"))
				str = data.m_visible;
			else if (strVar0 == _T("single"))
				str = data.m_single;
			else if (strVar0 == _T("lockdesktop"))
				str = data.m_lockdesktop;
			else if (strVar0 == _T("resizable"))
				str = data.m_resizable;
			else if (strVar0 == _T("top_template"))
			{
				str = data.m_topTemplateHTML;
			}
			else if (strVar0 == _T("bottom_template"))
			{
				str = data.m_bottomTemplateHTML;
			}
			else if (strVar0 == _T("to_date"))
			{
				str = data.m_to_date;
			}
			else if (strVar0 == _T("hide_close"))
			{
				str = data.m_hide_close;
			}
			else if (pDispParams->cArgs == 1)
			{
				str = _iAlertModel->getPropertyString(CString(strVar0));
			}
			else
			{
				CComBSTR strVar1;
				CComVariant var1 = pDispParams->rgvarg[pDispParams->cArgs - 2];
				if (var1.vt == VT_BSTR)
					strVar1 = var1.bstrVal;
				str = _iAlertModel->getPropertyString(CString(strVar0), &CString(strVar1));
			}
			res = new CComVariant(str);
		}
	}
	else if (name.CompareNoCase(_T("moveTo")) == 0)
	{
		if (pDispParams->cArgs >= 2)
		{
			const LONG dx = (LONG)BrowserUtils::variantToInt64(pDispParams->rgvarg[0]);
			const LONG dy = (LONG)BrowserUtils::variantToInt64(pDispParams->rgvarg[1]);
			CRect rec;
			this->GetWindowRect(&rec);
			rec.MoveToXY(rec.left + dx, rec.top + dy);
			this->MoveWindow(rec, TRUE);
		}
	}
	else if (name.CompareNoCase(_T("showAlertFromHistory")) == 0)
	{
		res = showAlertFromHistory(pDispParams);
	}
	else if (name.CompareNoCase(_T("getHistoryRange")) == 0)
	{

		int paramsCount = pDispParams->cArgs;
		if (paramsCount >= 2)
		{
			CComBSTR searchTerm;
			CComPtr<IDispatch> pCallbackFunc;
			INT64 startPos = BrowserUtils::variantToInt64(pDispParams->rgvarg[paramsCount - 1]);
			INT64 count = BrowserUtils::variantToInt64(pDispParams->rgvarg[paramsCount - 2]);

			if (paramsCount >= 3)
			{
				CComVariant param3 = pDispParams->rgvarg[paramsCount - 3];
				if (param3.vt == VT_BSTR)
				{
					searchTerm = param3.bstrVal;
				}

				if (paramsCount >= 4)
				{
					CComVariant param4 = pDispParams->rgvarg[paramsCount - 4];
					if (param4.vt == VT_DISPATCH)
					{
						pCallbackFunc = param4.pdispVal;
					}
				}
			}

			release_try
			{
				CDatabaseManager *_database = CDatabaseManager::shared(true);
				CString sTmp = _T("");
				if (_database)
				{
					CString sTimeFormat = _iAlertModel->getPropertyString(CString(_T("datetime_format")), &CString(_T("%Y-%m-%d %H:%M:%S")));
					m_callbacks.insert(pair<ULONG,CComPtr<IDispatch>>(m_lastCallbackId,pCallbackFunc));
					_database->GetHistoryRange(CString(_T("")), &sTmp, sTimeFormat,startPos,count,CString(searchTerm),true,m_lastCallbackId++,m_hWnd);
				}
				res = new CComVariant(sTmp);
			}
			release_catch_tolog(_T("GetHistory range exception"));
		}
	}
	else if (name.CompareNoCase(_T("resizeByBody")) == 0)
	{
		//if (pDispParams->cArgs >= 2 )
		//{
		//	CComVariant var0 = pDispParams->rgvarg[0];
		//	CComVariant var1 = pDispParams->rgvarg[1];
		//	long clientWidth, clientHeight, currWidth, currHeight;
		//	long differenceWidth = 0;
		//	long differenceHeight = 0;
		//	RECT rect;
		//	if (var0.vt == VT_I4 && var1.vt == VT_I4)
		//	{
		//		clientHeight = var1.intVal;
		//		clientWidth = var0.intVal;
		//	}
		//	// resizing body
		//	cHTMLView -> GetWindowRect(&rect);
		//	
		//	currWidth = rect.right - rect.left;
		//	currHeight = rect.bottom - rect.top;

		//	differenceWidth = clientWidth - currWidth;
		//	differenceHeight = clientHeight - currHeight;
		//		
		//	cHTMLView->ResizeClient(clientWidth, clientHeight);
		//
		//	// resizing caption
		//	cHTMLTool->GetWindowRect(&rect);
		//	currWidth = rect.right - rect.left;
		//	currHeight = rect.bottom - rect.top;
		//	cHTMLTool->ResizeClient(currWidth + differenceWidth, currHeight + differenceHeight);
		//		
		//	this->GetWindowRect(&rect);
		//	currWidth=rect.right-rect.left;
		//	currHeight=rect.bottom-rect.top;
		//	this->ResizeClient(currWidth + differenceWidth, currHeight + differenceHeight);

		//	res = new CComVariant(currWidth);

		//}
	}
	else if (name.CompareNoCase(_T("isResizible")) == 0)
	{
		res = new CComVariant((data.m_position == _T("maximized") || data.m_position == _T("maximize") || data.m_position == _T("fullscreen")) ? 0 : 1);
	}
	else if (name.CompareNoCase(_T("capturemouse")) == 0)
	{
		::PostMessage(m_hWnd, WM_SYSCOMMAND, SC_MOVE | HTCAPTION, 0);
	}
	else if (name.CompareNoCase(_T("startResizing")) == 0)
	{
		if (GetCursorPos(&m_resingPoint))
		{
			CRect rect;
			if (this->GetWindowRect(&rect))
			{
				m_resingStartDiff.x = rect.right - m_resingPoint.x;
				m_resingStartDiff.y = rect.bottom - m_resingPoint.y;
				SetCapture();
			}
		}
	}
	else if (name.CompareNoCase(_T("Search")) == 0)
	{
		if (pDispParams->cArgs >= 1)
		{
			CString strVar0;
			CComVariant var0 = pDispParams->rgvarg[0];
			if (var0.vt == VT_BSTR)
				strVar0 = var0.bstrVal;

			if (m_pFinderText)
			{
				m_pFinderText->find(strVar0);
			}
		}
	}
	else if (name.CompareNoCase(_T("DeleteHistory")) == 0)
	{
		this->EnableWindow(false);
		res = new CComVariant();
		res->vt = VT_BOOL;
		if (::MessageBox(this->m_hWnd, _iAlertModel->getPropertyString(CString(_T("delete_history")), &CString(_T("Are you sure you want to clear history?"))), _iAlertModel->getPropertyString(CString(_T("deskalerts_name")), &CString(_T("DeskAlerts"))), MB_YESNO | MB_ICONQUESTION) == IDYES)
		{
			CHistoryStorage::DeleteHistory();
			//PostMessage(WM_CLOSE, 0, 0);
			res->boolVal = VARIANT_TRUE;
		}
		else
			res->boolVal = VARIANT_FALSE;
		this->EnableWindow(true);
		_iAlertController->refreshState();
	}
	else if (name.CompareNoCase(_T("DeleteHistoryById")) == 0)
	{
		if (pDispParams->cArgs >= 1)
		{
			this->EnableWindow(false);
			res = new CComVariant();
			res->vt = VT_BOOL;
			if (::MessageBox(this->m_hWnd, _iAlertModel->getPropertyString(CString(_T("delete_alert")), &CString(_T("Are you sure you want to delete current alert from history?"))), _iAlertModel->getPropertyString(CString(_T("deskalerts_name")), &CString(_T("DeskAlerts"))), MB_YESNO | MB_ICONQUESTION) == IDYES)
			{
				const sqlite_int64 id = BrowserUtils::variantToInt64(pDispParams->rgvarg[0]);
				CHistoryStorage::DeleteAlert(id);
				res->boolVal = VARIANT_TRUE;
			}
			else
				res->boolVal = VARIANT_FALSE;
			this->EnableWindow(true);
			_iAlertController->refreshState();
		}
	}
	else if (name.CompareNoCase(_T("DeleteHistoryByAlertId")) == 0)
	{
		if (pDispParams->cArgs >= 1)
		{
			this->EnableWindow(false);
			res = new CComVariant();
			res->vt = VT_BOOL;
			const sqlite_int64 id = BrowserUtils::variantToInt64(pDispParams->rgvarg[0]);
			CHistoryStorage::DeleteAlertByID(id);
			res->boolVal = VARIANT_TRUE;
			_iAlertController->refreshState();
		}
	}
	else if (name.CompareNoCase(_T("registerOK")) == 0)
	{
		SysMenuEvent cEventOff(
			CString(_T("disable")),
			CString(_T("sysmenu")),
			CString(_T("alert"))
		);
		_iAlertController->actionPerformed(cEventOff);

		SysMenuEvent cEventOn(
			CString(_T("enable")),
			CString(_T("sysmenu")),
			CString(_T("alert"))
		);
		_iAlertController->actionPerformed(cEventOn);
	}
	else if (name.CompareNoCase(_T("ReloadAlert")) == 0)
	{
		SysMenuEvent cEventOff(
			CString(_T("disable")),
			CString(_T("sysmenu")),
			CString(_T("alert"))
		);
		_iAlertController->actionPerformed(cEventOff);

		SysMenuEvent cEventOn(
			CString(_T("enable")),
			CString(_T("sysmenu")),
			CString(_T("alert"))
		);
		_iAlertController->actionPerformed(cEventOn);
	}
	else if (name.CompareNoCase(_T("Exit")) == 0)
	{
		SysMenuEvent cEventOn(
			CString(_T("exit")),
			CString(_T("sysmenu")),
			CString(_T("exit"))
		);
		_iAlertController->actionPerformed(cEventOn);
	}
	else if (name.CompareNoCase(_T("getHistory")) == 0)
	{
		release_try
		{
			CDatabaseManager *_database = CDatabaseManager::shared(true);
			CString sTmp;
			if (_database)
			{
				CString sTimeFormat = _iAlertModel->getPropertyString(CString(_T("datetime_format")), &CString(_T("%Y-%m-%d %H:%M:%S")));
				_database->GetHistory(CString(_T("")), sTmp, sTimeFormat);
			}
			res = new CComVariant(sTmp);
		}
		release_catch_tolog(_T("GetHistory Exception"));
	}
	else if (name.CompareNoCase(_T("QueryURL")) == 0)
	{
		if (pDispParams->cArgs >= 1)
		{
			CString strVar0;
			CComVariant var0 = pDispParams->rgvarg[pDispParams->cArgs - 1];
			if (var0.vt == VT_BSTR)
			{
				strVar0 = var0.bstrVal;
				if (!strVar0.IsEmpty())
				{
					CString result;
					CAlertUtils::URLDownloadToFile(strVar0, NULL, &result);
					res = new CComVariant(result);
				}
			}
		}
	}
	else if (name.CompareNoCase(_T("getBodyHeight")) == 0)
	{
		long result;
		RECT rect;
		cHTMLView->GetWindowRect(&rect);
		result = rect.bottom - rect.top;
		res = new CComVariant(result);
	}
	else if (name.CompareNoCase(_T("getBodyWidth")) == 0)
	{
		long result;
		RECT rect;
		cHTMLView->GetWindowRect(&rect);
		result = rect.right - rect.left;
		res = new CComVariant(result);
	}
	else if (name.CompareNoCase(_T("getCaptionHeight")) == 0)
	{
		long result;
		RECT rect;
		cHTMLTool->GetWindowRect(&rect);
		result = rect.bottom - rect.top;
		res = new CComVariant(result);
	}
	else if (name.CompareNoCase(_T("getCaptionWidth")) == 0)
	{
		long result;
		RECT rect;
		cHTMLTool->GetWindowRect(&rect);
		result = rect.right - rect.left;
		res = new CComVariant(result);
	}
	// 	else if (name.CompareNoCase(_T("setCaptionHeight"))==0)
	// 	{
	// 		if (pDispParams->cArgs == 1)
	// 		{
	// 			const LONG height = (LONG)BrowserUtils::variantToInt64(pDispParams->rgvarg[0]);
	// 			CRect rect;
	// 			cHTMLTool->GetWindowRect(&rect);
	// 			cHTMLTool->ResizeClient(rect.right-rect.left, height);
	// 			this->ResizeClient(rect.right-rect.left, height);
	// 				
	// 
	// 			res = new CComVariant(height);
	// 		}
	// 	}
	// 	else if (name.CompareNoCase(_T("setCaptionWidth"))==0)
	// 	{
	// 		if (pDispParams->cArgs == 1)
	// 		{
	// 			const LONG width = (LONG)BrowserUtils::variantToInt64(pDispParams->rgvarg[0]);
	// 			CRect rect;
	// 			cHTMLTool->GetWindowRect(&rect);
	// 	
	// 			cHTMLTool->ResizeClient(width, rect.bottom-rect.top);
	// 
	// 			this->ResizeClient(width, rect.bottom-rect.top);
	// 			res = new CComVariant(width);
	// 		}
	// 	}
	else if (name.CompareNoCase(_T("getBodyTop")) == 0)
	{
		RECT rectView;
		RECT rectTool;
		cHTMLView->GetWindowRect(&rectView);
		cHTMLTool->GetWindowRect(&rectTool);
		res = new CComVariant(rectView.top - rectTool.top);
	}
	else if (name.CompareNoCase(_T("setBodyHeight")) == 0)
	{
		if (pDispParams->cArgs == 1)
		{
			const LONG height = (LONG)BrowserUtils::variantToInt64(pDispParams->rgvarg[0]);
			CRect rect;
			cHTMLView->GetWindowRect(&rect);
			cHTMLView->ResizeClient(rect.right - rect.left, height);
			res = new CComVariant(height);
		}
	}
	else if (name.CompareNoCase(_T("setBodyWidth")) == 0)
	{
		if (pDispParams->cArgs == 1)
		{
			const LONG width = (LONG)BrowserUtils::variantToInt64(pDispParams->rgvarg[0]);
			RECT rect;
			cHTMLView->GetWindowRect(&rect);
			cHTMLView->ResizeClient(width, rect.bottom - rect.top);
			res = new CComVariant(width);
		}
	}
	else if (name.CompareNoCase(_T("setCaptionHeightByBodyHeight")) == 0)
	{
		if (pDispParams->cArgs == 1)
		{
			const LONG newHeight = (LONG)BrowserUtils::variantToInt64(pDispParams->rgvarg[0]);

			LONG currWidth, currHeight, diffHeight;
			CRect rect, tmpRect;

			// set body height
			cHTMLView->GetWindowRect(&rect);
			currHeight = rect.bottom - rect.top;
			currWidth = rect.right - rect.left;
			cHTMLView->ResizeClient((int)currWidth, (int)newHeight);

			// calc difference
			diffHeight = newHeight - currHeight;

			if (diffHeight != 0) {
				if (cHTMLTool)
				{
					// get caption height
					cHTMLTool->GetWindowRect(&rect);
					currHeight = rect.bottom - rect.top;
					currWidth = rect.right - rect.left;

					// set caption height
					cHTMLTool->ResizeClient(currWidth, currHeight + diffHeight);
				}

				SystemParametersInfo(SPI_GETWORKAREA, 0, &tmpRect, 0);
				this->GetWindowRect(&rect);
				if (UnionRect(&tmpRect, &tmpRect, &rect)) {
					SystemParametersInfo(SPI_SETWORKAREA, sizeof(tmpRect), tmpRect, SPIF_UPDATEINIFILE | SPIF_SENDWININICHANGE);
				}
				currWidth = rect.right - rect.left;
				currHeight = rect.bottom - rect.top;
				this->ResizeClient(currWidth, currHeight + diffHeight);

				// get new rect
				rect = Reposition(currWidth, currHeight + diffHeight);

				// set new rect
				this->SetWindowPos(GetWindow(GW_OWNER), rect, SWP_NOACTIVATE | SWP_NOSIZE | SWP_NOZORDER);
			}
			res = new CComVariant(currHeight + diffHeight);
		}
	}
	else if (name.CompareNoCase(_T("setCaptionWidthByBodyWidth")) == 0)
	{
		if (pDispParams->cArgs == 1)
		{
			const LONG newWidth = (LONG)BrowserUtils::variantToInt64(pDispParams->rgvarg[0]);

			LONG currWidth, currHeight, diffWidth;
			CRect rect;

			// set body height
			cHTMLView->GetWindowRect(&rect);
			currHeight = rect.bottom - rect.top;
			currWidth = rect.right - rect.left;
			cHTMLView->ResizeClient((int)newWidth, (int)currHeight);

			// calc difference
			diffWidth = newWidth - currWidth;

			if (cHTMLTool)
			{
				// get caption height
				cHTMLTool->GetWindowRect(&rect);
				currHeight = rect.bottom - rect.top;
				currWidth = rect.right - rect.left;

				// set caption height
				cHTMLTool->ResizeClient(currWidth + diffWidth, currHeight);
			}

			this->GetWindowRect(&rect);
			currWidth = rect.right - rect.left;
			currHeight = rect.bottom - rect.top;
			this->ResizeClient(currWidth + diffWidth, currHeight);

			// get new rect
			rect = Reposition(currWidth + diffWidth, currHeight);

			// set new rect
			this->SetWindowPos(GetWindow(GW_OWNER), rect, SWP_NOACTIVATE | SWP_NOSIZE | SWP_NOZORDER);

			res = new CComVariant(currWidth + diffWidth);
		}
	}
	else if (name.CompareNoCase(_T("setBodyTop")) == 0)
	{
		if (pDispParams->cArgs == 1)
		{
			const LONG top = (LONG)BrowserUtils::variantToInt64(pDispParams->rgvarg[0]);
			RECT rectView;
			cHTMLView->GetWindowRect(&rectView);
			ScreenToClient(&rectView);
			rectView.bottom += top - rectView.top; //for back compatibility of setBodyTop function
			rectView.top = top;
			cHTMLView->SetWindowPos(NULL, &rectView, SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOACTIVATE);
			res = new CComVariant(top);
		}
	}
	else if (name.CompareNoCase(_T("setTopMargin")) == 0)
	{
		if (pDispParams->cArgs == 1)
		{
			const LONG top = (LONG)BrowserUtils::variantToInt64(pDispParams->rgvarg[0]);
			RECT rectView;
			cHTMLView->GetWindowRect(&rectView);
			ScreenToClient(&rectView);
			rectView.top = top;
			cHTMLView->SetWindowPos(NULL, &rectView, SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOACTIVATE);
			res = new CComVariant(top);
		}
	}
	else if (name.CompareNoCase(_T("setLeftMargin")) == 0)
	{
		if (pDispParams->cArgs == 1)
		{
			const LONG left = (LONG)BrowserUtils::variantToInt64(pDispParams->rgvarg[0]);
			RECT rectView;
			cHTMLView->GetWindowRect(&rectView);
			ScreenToClient(&rectView);
			rectView.left = left;
			cHTMLView->SetWindowPos(NULL, &rectView, SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOACTIVATE);
			res = new CComVariant(left);
		}
	}
	else if (name.CompareNoCase(_T("setRightMargin")) == 0)
	{
		if (pDispParams->cArgs == 1)
		{
			const LONG right = (LONG)BrowserUtils::variantToInt64(pDispParams->rgvarg[0]);
			RECT rectView;
			RECT rectTool;
			GetClientRect(&rectTool);
			cHTMLView->GetWindowRect(&rectView);
			ScreenToClient(&rectView);
			rectView.right = rectTool.right - right;
			cHTMLView->SetWindowPos(NULL, &rectView, SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOACTIVATE);
			res = new CComVariant(right);
		}
	}
	else if (name.CompareNoCase(_T("setBottomMargin")) == 0)
	{
		if (pDispParams->cArgs == 1)
		{
			const LONG bottom = (LONG)BrowserUtils::variantToInt64(pDispParams->rgvarg[0]);
			RECT rectView;
			RECT rectTool;
			GetClientRect(&rectTool);
			cHTMLView->GetWindowRect(&rectView);
			ScreenToClient(&rectView);
			rectView.bottom = rectTool.bottom - bottom;
			cHTMLView->SetWindowPos(NULL, &rectView, SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOACTIVATE);
			res = new CComVariant(bottom);
		}
	}
	else if (name.CompareNoCase(_T("format")) == 0)
	{
		if (pDispParams->cArgs >= 1)
		{
			CString strVar0;
			CComVariant var0 = pDispParams->rgvarg[0];
			if (var0.vt == VT_BSTR)
			{
				strVar0 = var0.bstrVal;
			}
			release_try
			{
				CDatabaseManager *_database = CDatabaseManager::shared(true);
				CString sTmp = _T("");
				CString sTimeFormat = _iAlertModel->getPropertyString(CString(_T("datetime_format")), &CString(_T("%Y-%m-%d %H:%M:%S")));
				if (_database)
				{
					_database->GetFormatedDateTime(strVar0, sTmp, sTimeFormat, data.m_UTC);
				}
				else
				{
					sqlite3* db;
					sqlite3_open(":memory:", &db);
					if (db)
					{
						CString req = _T("SELECT strftime('") + sTimeFormat + _T("', ") + strVar0 + _T(", 'unixepoch'");
						if (data.m_UTC) req += _T(", 'localtime'");
						req += _T(");");

						sqlite3_stmt* stmt;

						int ret = sqlite3_prepare_v2(db, CT2A(req, CP_UTF8), -1, &stmt, 0);
						if (ret == SQLITE_OK)
						{
							if (sqlite3_step(stmt) == SQLITE_ROW)
							{
								sTmp = CA2T((char*)sqlite3_column_text(stmt, 0), CP_UTF8);
							}
						}
						sqlite3_finalize(stmt);
					}
					sqlite3_close(db);
				}
				res = new CComVariant(sTmp);
			}
			release_catch_tolog(_T("GetFormatedDateTime Exception"));
		}
	}
	///////////////////////////////////////////////////////
	/// Skins 3.0
	/// usage in JScode:
	/// winwows.external.DeskAlerts(MethodName[,params,..])
	else if (name.CompareNoCase(_T("DeskAlerts")) == 0)
	{
		if (pDispParams->cArgs >= 1)
		{
			CString strMethodAPI;
			CComVariant varMethodAPI = pDispParams->rgvarg[pDispParams->cArgs-1];
			if (varMethodAPI.vt == VT_BSTR)
			{
				strMethodAPI = varMethodAPI.bstrVal;
				if (strMethodAPI.CompareNoCase(_T("Close")) == 0) 
				{
					res = CloseAPI();
				}
				else if (strMethodAPI.CompareNoCase(_T("Print")) == 0) 
				{
					res = PrintAPI();
				}
				else if (strMethodAPI.CompareNoCase(_T("Postpone")) == 0) 
				{
					res = PostponeAPI(pDispParams);
				}
				else if (strMethodAPI.CompareNoCase(_T("SetStatus")) == 0) 
				{
					res = SetStatusAPI();
				}
				else if (strMethodAPI.CompareNoCase(_T("OpenURL")) == 0) 
				{
					if (pDispParams->cArgs >= 2)
					{
						CString _url(pDispParams->rgvarg[1].bstrVal);
						res = OpenURLAPI(_url);
					}
				}
				else if (strMethodAPI.CompareNoCase(_T("IsExternalBrowserEnable")) == 0) 
				{
					res = IsExternalBrowserEnableAPI();
				}
			}
		}
	}
	else
	{
		//forward function call
		BOOL inToolbar;
		if ((inToolbar = (name.CompareNoCase(_T("callInCaption")) == 0)) || (name.CompareNoCase(_T("callInBody")) == 0))
		{
			const UINT cArgs = pDispParams->cArgs;
			if (cArgs >= 1)
			{
				CComVariant varFunc = pDispParams->rgvarg[cArgs - 1]; //first argument
				if (varFunc.vt == VT_BSTR)
				{
					shared_ptr<CSimpleArray<CComVariant>> params(new CSimpleArray<CComVariant>);
					for (UINT i = 0; i < cArgs - 1; i++)
					{
						params->Add(pDispParams->rgvarg[i]);
					}
					shared_ptr<CallFuncStruct> callFuncStruct(new CallFuncStruct(varFunc.bstrVal, params));
					CallJs(callFuncStruct, inToolbar);
				}
			}
		}
		else if ((((inToolbar = (name.CompareNoCase(_T("readIt")) == 0)) || (name.CompareNoCase(_T("readItNR")) == 0)) && (pDispParams->cArgs == 3)) ||
			((name.CompareNoCase(_T("autoclose")) == 0) && (pDispParams->cArgs == 1)))
		{
			CString strVar0;
			CString strVar1;
			CString strVar2;
			if (pDispParams->cArgs == 3)
			{
				CComVariant var0 = pDispParams->rgvarg[0];
				CComVariant var1 = pDispParams->rgvarg[1];
				CComVariant var2 = pDispParams->rgvarg[2];
				if (var0.vt == VT_BSTR && var1.vt == VT_BSTR && var2.vt == VT_BSTR)
				{
					strVar0 = var2.bstrVal;
					strVar1 = var1.bstrVal;
					strVar2 = var0.bstrVal;
				}
			}
			else if (pDispParams->cArgs == 1)
			{
				CComVariant var0 = pDispParams->rgvarg[0];
				if (var0.vt == VT_BSTR)
				{
					strVar0 = var0.bstrVal;
				}
			}
			if (!strVar1.IsEmpty())
			{

				CString url = _iAlertModel->getPropertyString(CString(_T("readitURL")), &CString(_T("%server%/alert_read.asp?alert_id=%alert_id%&user_id=%user_id%")), _iAlertModel->getProperty(CString(_T("encoding"))), true);
				/*url.Replace(_T("%alert_id%"), strVar1);
				url.Replace(_T("%user_id%"), strVar2);
				CAlertUtils::AsyncURLRequest(url);
				*/

				SYSTEMTIME lt;
				GetLocalTime(&lt);
				CString localTime = "";
				localTime.Format(_T("%02d.%02d.%d|%02d:%02d:%02d"), lt.wDay, lt.wMonth, lt.wYear, lt.wHour, lt.wMinute, lt.wSecond);

				JAReq_AlertRead alertReadReq("", localTime, _ttoi(strVar1)/*, 1*/);
				CJAClient *client = _iAlertController->getJAClient();
				BOOL isUTF8;
                if (! name.CompareNoCase(_T("readItNR")) == 0)
                {
                    client->SendRequest((JARequest *)&alertReadReq, FALSE, NULL, FALSE, NULL, &isUTF8);
                }

				/*furl.Format( _T("%s/%s"), _iAlertModel->getPropertyString(CString("server")), alertReadReq.getURL() );
				std::string  strAlertReadReq = alertReadReg.toJSON();
				CString csReq = CString( strAlertReadReq.c_str() );
				CString headers = _T("Authorization: ") + authorization+ _T("\nContent-Type: application/json; charset=utf-8 ");
				HRESULT hres = CAlertUtils::URLDownloadToFile( furl, NULL, 0, 0, 0, &headers, &csReq );*/
				CHistoryStorage::MarkAlertAsClosedByIdAndUser(_ttoi(strVar1), _ttoi(strVar2));
			}
			std::map<CString, pair<int, shared_ptr<showData>>>::iterator it = NotReadAlerts.find(strVar0);
			if (it != NotReadAlerts.end()) {
				if (it->second.second->self_deletable == "1") {
					CHistoryStorage::DeleteAlertByID(_ttoi(it->second.second->alertid));
				}
				NotReadAlerts.erase(it);
			}
			if (NotReadAlerts.empty())
			{
				PostMessage(WM_CLOSE, 0, 0);
			}
			else if (inToolbar) //readIt
			{
				CComPtr<IWebBrowser2> b = cHTMLView->getWebBrowser();
				if (b)
				{
					CComVariant vtEmpty, vtPostData, vtHeaders;
					if (!m_post.IsEmpty())
					{
						CComSafeArray<char, VT_UI1> psa;
						// post data is specified.
						CT2A postData(_iAlertModel->buildPropertyString(m_post, m_encoding ? m_encoding : CP_UTF8, true));
						psa.Add(strlen(postData), postData);
						vtPostData = psa;
						vtHeaders = L"Content-Type: application/x-www-form-urlencoded\n";
					}
					HRESULT resNavigate = b->Navigate2(&CComVariant(_iAlertModel->buildPropertyString(m_url, m_encoding ? m_encoding : CP_UTF8, true)), &vtEmpty, &vtEmpty, &vtPostData, &vtHeaders);
					DeskAlertsAssert(SUCCEEDED(resNavigate));
				}
			}
		}
		//else if(name.CompareNoCase(_T("closeticker")))
		//{
		//	if(pDispParams->cArgs == 2)
		//	{
		//		CString alertId = pDispParams->rgvarg[0];
		//		CString userId  = pDispParams->rgvarg[1];
		//		CHistoryStorage::MarkAlertAsClosedByIdAndUser(_ttoi(alertId),_ttoi(userId));
		//	}
		//}

	}

	return res;
}

BOOL CWindowCommand::MakeWindowTransparent(HWND hWnd, COLORREF color, unsigned char factor, DWORD flags)
{
	BOOL res = false;
	//START_ERR_HANDLER
	{
		/* First, see if we can get the API call we need. If we've tried
		* once, we don't need to release_try again. */
		if (!m_bInitialized)
		{
			HMODULE hDLL = LoadLibrary(_T("user32"));
            if (hDLL)
            {
                pSetLayeredWindowAttributes =
                    (PSLWA)GetProcAddress(hDLL, "SetLayeredWindowAttributes");
                m_bInitialized = TRUE;
            }
		}
		if (pSetLayeredWindowAttributes != NULL)
		{
			/* Windows need to be layered to be made transparent. This is done
			* by modifying the extended style bits to contain WS_EX_LAYARED. */
			SetLastError(0);
			::SetWindowLong(hWnd,
				GWL_EXSTYLE,
				::GetWindowLong(hWnd, GWL_EXSTYLE) | WS_EX_LAYERED);
			if (GetLastError() == 0)
			{
				/* Now, we need to set the 'layered window attributes'. This
				* is where the alpha values get set. */
				res = pSetLayeredWindowAttributes(hWnd,
					color,
					factor,
					flags);
			}
		}
	}
	//END_ERR_HANDLER(_T("CWidget::MakeWindowTransparent"));
	return res;
}

void CWindowCommand::SetUrl(CString URL, shared_ptr<showData> ex)
{
	CString parsedURL = _iAlertModel->buildPropertyString(URL);
	CString parsedmURL = _iAlertModel->buildPropertyString(m_url);
	if (parsedURL == parsedmURL) return;
	CComPtr<IWebBrowser2> b = cHTMLView->getWebBrowser();
	if (b)
	{
		//merge files
		CString mergedURL;
		mergedURL.Format(_T("%I64u_merged.html"), GetTickCount64());
		m_url = URL + mergedURL;
		mergedURL = parsedURL + mergedURL;
		CopyFile(parsedURL, mergedURL, FALSE);
		CAlertUtils::mergeFiles(mergedURL, parsedmURL);
        //DeleteFile(parsedmURL);
        //DeleteFile(parsedURL);
        NotReadAlerts[URL] = pair<int, shared_ptr<showData>>(AlertsInWindow, ex);
		AlertsInWindow++;
		m_post = !ex->post.IsEmpty() ? ex->post : _T("");
		CComVariant vtEmpty, vtPostData, vtHeaders;
		if (!m_post.IsEmpty())
		{
			CComSafeArray<char, VT_UI1> psa;
			// post data is specified.
			CT2A postData(_iAlertModel->buildPropertyString(m_post, m_encoding ? m_encoding : CP_UTF8, true));
			psa.Add(strlen(postData), postData);
			vtPostData = psa;
			vtHeaders = L"Content-Type: application/x-www-form-urlencoded\n";
		}
		HRESULT resNavigate = b->Navigate2(&CComVariant(_iAlertModel->buildPropertyString(m_url, m_encoding ? m_encoding : CP_UTF8, true)), &vtEmpty, &vtEmpty, &vtPostData, &vtHeaders);
		DeskAlertsAssert(SUCCEEDED(resNavigate));
	}
}

LRESULT CWindowCommand::OnCallbackInvoke(UINT /*uMsg*/, WPARAM wParam, LPARAM lParam, BOOL& /*bHandled*/)
{
	ULONG callbackId = (ULONG)lParam;
	CString *response = (CString*)wParam;

	map<ULONG, CComPtr<IDispatch>>::const_iterator it = m_callbacks.find(callbackId);
	if (it != m_callbacks.end())
	{
		CComPtr<IDispatch> dispatch = it->second;
		dispatch.Invoke1((DISPID)0L, &CComVariant(*response));
		m_callbacks.erase(it);
	}
	delete_catch(response);

	return S_OK;
}

map<CString, pair<int, shared_ptr<showData>>> &CWindowCommand::getNotReadAlerts()
{
	return NotReadAlerts;
}

LRESULT CWindowCommand::onMouseMove(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	POINT p;
	if (GetCursorPos(&p))
	{
		LONG diffX = p.x - m_resingPoint.x;
		LONG diffY = p.y - m_resingPoint.y;
		CRect rect;
		if (this->GetWindowRect(&rect))
		{
			LONG width = rect.right - rect.left + diffX;
			LONG height = rect.bottom - rect.top + diffY;

			if (width < MIN_WINDOW_WIDTH)
			{
				width = MIN_WINDOW_WIDTH;
				diffX = width - rect.right + rect.left;
				m_resingPoint.x = rect.left + width - m_resingStartDiff.x;
			}
			else
			{
				m_resingPoint.x = p.x;
			}

			if (height < MIN_WINDOW_HEIGHT)
			{
				height = MIN_WINDOW_HEIGHT;
				diffY = height - rect.bottom + rect.top;
				m_resingPoint.y = rect.top + height - m_resingStartDiff.y;
			}
			else
			{
				m_resingPoint.y = p.y;
			}

			this->ResizeClient(width, height, TRUE);

			cHTMLView->GetWindowRect(&rect);
			width = rect.right - rect.left + diffX;
			height = rect.bottom - rect.top + diffY;
			cHTMLView->ResizeClient(width, height, TRUE);

			if (cHTMLTool)
			{
				cHTMLTool->GetWindowRect(&rect);
				width = rect.right - rect.left + diffX;
				height = rect.bottom - rect.top + diffY;
				cHTMLTool->ResizeClient(width, height, TRUE);
			}
		}
	}
	return 0;
}

LRESULT CWindowCommand::onLeftMouseButtonUp(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	ReleaseCapture();
	return 0;
}

CComVariant* CWindowCommand::CloseAPI() 
{
	CString url;
	if (data.m_confirmation == _T("1")
		&& !(url = _iAlertModel->getPropertyString(CString(_T("confirmationUrl")), &CString(), _iAlertModel->getProperty(CString(_T("encoding"))), true)).IsEmpty())
	{
		time_t tTime;  // C run-time time (defined in <time.h>)
		time(&tTime);  // Get the current time from the
		CString app;
		app.Format(L"%s%cread=1&closetime=%I64d", url, (url.Find(_T('?')) >= 0 ? _T('&') : _T('?')), tTime);
		//CAlertUtils::URLDownloadToFile(app);
		CAlertUtils::AsyncURLRequest(app);
	}
	HDESK desktop = GetThreadDesktop(GetCurrentThreadId());
	CString desktopName = CAlertUtils::getDesktopName(desktop);
	if (desktopName.MakeLower() == _T("winlogon"))
	{
		_iAlertController->getServiceBridge()->sendCloseWindowRequest(m_history_id);
	}
	else
	{
		CHistoryStorage::MarkAlertAsClosed(m_history_id);
		map<CString, pair<int, shared_ptr<showData>>>::iterator it = NotReadAlerts.begin();
		while (it != NotReadAlerts.end())
		{
			CHistoryStorage::MarkAlertAsClosed(it->second.second->history_id);
			if (it->second.second->self_deletable == "1") {
				CHistoryStorage::DeleteAlertByID(_ttoi(it->second.second->alertid));
			}
			it++;
		}
	}
	if (data.m_ticker && data.m_position == "bottom")
	{
		UnregisterAccessBar(winDisp->getParentHWND() /*m_hWnd*/, ABE_BOTTOM);
	}

	DeleteFile(this->m_url);
	BOOL isPost = ::PostMessage(winDisp->m_hWnd, WM_KILL_ME, (WPARAM)this, 0);
	DeskAlertsAssert(isPost);
	CComVariant* res = new CComVariant(true);
	return res;
}
CComVariant* CWindowCommand::PrintAPI() { return NULL; }
//////////////////////////////////////////////////////////////
/// using: window.external.deskalerts("Postpone", id, seconds);
CComVariant* CWindowCommand::PostponeAPI(DISPPARAMS* pDispParams)
{
	CComVariant* res = new CComVariant(VARIANT_FALSE);
	if (pDispParams->cArgs >= 3)
	{
		*res = CComVariant(VARIANT_FALSE);
		CComVariant idvar = pDispParams->rgvarg[1];
		CComVariant secvar = pDispParams->rgvarg[0];
		INT64 alertId = BrowserUtils::variantToInt64(idvar);
		UINT sec = secvar.uintVal;
		CComVariant* res = CWindowCommand::CloseAPI();
		if (NULL == PostponeManager::Get())
			PostponeManager::Initialize();
		UINT_PTR timerID = ::SetTimer(0, 0, sec, (TIMERPROC)&showPostponedAlertByTimer);
		PostponeManager::Get()->add(std::pair<UINT_PTR, INT64 >(timerID, alertId));
		CloseAPI();
		DWORD hr = GetLastError();
		hr;
	}
	return res; 
}
CComVariant* CWindowCommand::SetStatusAPI() { return NULL; }
CComVariant* CWindowCommand::OpenURLAPI(CString url) 
{ 
	CComVariant* res = IsExternalBrowserEnableAPI();
	if (res->boolVal)
	{
		ShellExecute(0, 0, url, 0, 0, SW_SHOW);
	}
	return res;
}
CComVariant* CWindowCommand::IsExternalBrowserEnableAPI()
{
	CComVariant* res = new CComVariant(true);
	return res;
}
CComVariant* CWindowCommand::showAlertFromHistory(DISPPARAMS* pDispParams)
{
	if (pDispParams->cArgs >= 1)
	{
		CComVariant var0;
		CComVariant var1;

		CComBSTR searchTerm;
		var0 = pDispParams->rgvarg[pDispParams->cArgs - 1];
		if (pDispParams->cArgs >= 2)
		{
			var1 = pDispParams->rgvarg[pDispParams->cArgs - 2];
			if (var1.vt == VT_BSTR)
			{
				searchTerm = var1.bstrVal;
			}
		}

		INT64 id = BrowserUtils::variantToInt64(var0);
		CDatabaseManager* db = CDatabaseManager::shared(false);
		if (db)
		{
			CString window;
			shared_ptr<showData> alert = shared_ptr<showData>(new showData());
			alert->windowsCountDelta = 1;
			db->getAlertFromHistory(id, window, alert);
			db->markAlertAsRead(id);
			_iAlertController->refreshState();
			alert->searchTerm = searchTerm;

			/////////////////////////////////////
			/// dont show screensavers from history!!!!
			if (alert->Class != _T("2"))
			{
				if (window.IsEmpty())
				{
					window = CAlertUtils::findAlertWindowName();
					_iAlertView->showViewByName(window, alert->url, alert);
				}
				else if (!_iAlertView->showViewByName2(window, alert->url, alert))
				{
					window = CAlertUtils::findAlertWindowName();
					_iAlertView->showViewByName(window, alert->url, alert);
				}
			}
		}
	}
	CComVariant* res = new CComVariant(VARIANT_TRUE);
	return res;
}
void CALLBACK  CWindowCommand::showPostponedAlertByTimer(HWND hwnd, UINT uMsg, UINT timerId, DWORD dwTime)
{
	auto _list = PostponeManager::Get()->listPostponedAlerts();
	auto _it = _list.begin();
	while (_it != _list.end())
	{
		if (_it->first == timerId)
		{
			{
				INT64 id = _it->second;
				CDatabaseManager* db = CDatabaseManager::shared(false);
				if (db)
				{
					CString window;
					shared_ptr<showData> alert = shared_ptr<showData>(new showData());
					alert->windowsCountDelta = 1;
					db->getAlertFromHistory(id, window, alert);
					//db->markAlertAsRead(id);
					_iAlertController->refreshState();
					if (window.IsEmpty())
					{
						window = CAlertUtils::findAlertWindowName();
						_iAlertView->showViewByName(window, alert->url, alert);
					}
					else if (!_iAlertView->showViewByName2(window, alert->url, alert))
					{
						window = CAlertUtils::findAlertWindowName();
						_iAlertView->showViewByName(window, alert->url, alert);
					}
				}
				PostponeManager::Get()->remove(id);
				::KillTimer(0, timerId);
			}
			break;
		}
		_it++;
	}
	//printf("Hello");
}
