#pragma once

class IComponent
{
public:
	virtual void createComponent() = 0;
	virtual void destroyComponent() = 0;
};