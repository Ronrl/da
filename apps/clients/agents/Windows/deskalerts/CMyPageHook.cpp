#include "StdAfx.h"
#include "CMyPageHook.h"

CMyPageHook::CMyPageHook(HWND SubHWND, IWinMessageFilter* _mFilter)
{
	mFilter = _mFilter;
	SubclassWindow(SubHWND);
}

CMyPageHook::~CMyPageHook()
{
	if(IsWindow())
	{
		const HWND unsWnd = UnsubclassWindow(TRUE);
		if(!unsWnd)
		{
			m_hWnd = NULL;
		}
	}
}

BOOL CMyPageHook::ProcessWindowMessage(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam,
						  LRESULT& lResult, DWORD dwMsgMapID)
{
	return mFilter->WinProcessWindowMessage(hWnd, uMsg, wParam, lParam,lResult, dwMsgMapID);
}
