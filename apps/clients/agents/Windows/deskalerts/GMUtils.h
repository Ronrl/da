//
//  GMUtils.h
//  DeskAlerts Client
//  Common module
//
//  Copyright Softomate 2006-2009. All rights reserved.
//

#pragma once

class CGMUtils
{

public:


	CGMUtils(void);
	~CGMUtils(void);

	static HKEY GetCompanyRegistryKey();
	static HKEY GetAppRegistryKey();

	static CString GetProgPath();

	static PSID GetCurrentUserSid (PTOKEN_USER & usr);
	static CString GetCurrentUserSidString();
	static CString GetPreCompanyKey();
	static CString GetCompanyKey();
};
