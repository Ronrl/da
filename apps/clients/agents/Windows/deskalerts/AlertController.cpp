﻿#include "StdAfx.h"
#include "alertcontroller.h"
#include "AlertLoader.h"
#include "HistoryStorage.h"
#include ".\InstallUninstallUpdate.h"
#include <atlsafe.h>
#include "RegistryMonitor.h"
#include "HTMLView.h"
#include "../Service/GetProcessID.h"
#include "WallpaperManager.h"
#include "ScreensaverManager.h"
#include "LockscreenManager.h"
#include <Lmcons.h>

IAlertModel* _iAlertModel = NULL;
IAlertController* _iAlertController = NULL;
IAlertView3* _iAlertView = NULL;

_bstr_t CAlertUtils::Key;
CString CAlertController::m_sAppDataPath;

HWND CAlertController::mainFrameHWND;

CCommand::CCommand(NodeALERTS::NodeCOMMANDS::NodeCOMMAND *cmd): m_cmd(*cmd)
{
}

void CCommand::stop()
{
}

BOOL CCommand::isCanStart()
{
	return TRUE;
}

BOOL CCommand::isAutoStart()
{
	return m_cmd.m_autostart == _T("1");
}

COpenWindowCommand::COpenWindowCommand(NodeALERTS::NodeCOMMANDS::NodeOPENWINDOW* param): CCommand(param), winData(*param)
{
}

COpenWindowCommand::~COpenWindowCommand(void)
{
}

CString COpenWindowCommand::getName()
{
	return _T("OPTION");
}

void COpenWindowCommand::run(IActionEvent& /*event*/)
{
	release_try
	{
		shared_ptr<showData> ex(new showData());
		ex->url = winData.m_filename;
		ex->post = winData.m_post;
		ex->expire = winData.m_expire;
		ex->encoding = winData.m_encoding ? winData.m_encoding : _iAlertModel->getProperty(CString(_T("encoding")));
		_iAlertView->showViewByName(winData.m_window, winData.m_filename, ex);
	}
	release_catch_tolog(_T("COpenWindowCommand exception"));
}

CExit::CExit(NodeALERTS::NodeCOMMANDS::NodeEXIT *param) : CCommand(param)
{
}

CString CExit::getName()
{
	return _T("EXIT");
}

void CExit::run(IActionEvent& /*event*/)
{
	//appendToErrorFile(L"Run EXIT command");
	if (_iAlertModel)
	{
		_iAlertModel->setPropertyString(CString(_T("AlertMode")),CString(_T("normal")));
		HWND mainHWND =(HWND)_iAlertModel->getProperty( CString(_T("mainHWND")));
        HWND appbaraccessHWND = (HWND)_iAlertModel->getProperty(CString(_T("appbarAccessHWND")));

        APPBARDATA abd;
        abd.cbSize = sizeof(APPBARDATA);
        abd.hWnd = appbaraccessHWND;
        SHAppBarMessage(ABM_REMOVE, &abd);

        ::PostMessage(mainHWND,WM_CLOSE,0,0);
	}
}

CUninstall::CUninstall(NodeALERTS::NodeCOMMANDS::NodeUNINSTALL *param) : CCommand(param)
{
}

CString CUninstall::getName()
{
	return _T("UNINSTALL");
}

void CUninstall::run(IActionEvent& /*event*/)
{
	uninstall(_T(""));
}

CUpdate::CUpdate(NodeALERTS::NodeCOMMANDS::NodeUPDATE *param) : CCommand(param)
{
}

CString CUpdate::getName()
{
	return _T("UPDATE");
}

void CUpdate::run(IActionEvent& event)
{
	if (event.getSourceName() == _T("autoupdate"))
		update(false);
	else
		update(true);
}

CDisable::CDisable(NodeALERTS::NodeCOMMANDS::NodeDISABLE* _param) : CCommand(_param), param(*_param)
{
}

CString CDisable::getName()
{
	return _T("DISABLE");
}

void CDisable::run(IActionEvent& /*event*/)
{
	if (_iAlertModel)
	{
		CSysMenu *menu = (CSysMenu*)_iAlertView->getViewByName(CString(_T("sysmenu")));
		CString disableState = _iAlertModel->getPropertyString(CString(_T("disableState")));

		if (disableState==_T("on"))
		{
			_iAlertModel->setPropertyString(CString(_T("disableState")),CString(_T("off")));
			_iAlertModel->setPropertyString(CString(_T("disableNewState")),CString(_T("off")));
			_iAlertModel->setPropertyString(CString(_T("disableCaption")),CString(_T("")));
			CDisableAlertEvent disEventOff(
				CString(_T("disable")),
				CString(_T("disable")),
				CString(param.m_alert));
			_iAlertController->actionPerformed(disEventOff);
			CDisableAlertEvent disEventOn(
				CString(_T("enable")),
				CString(_T("disable")),
				CString(param.m_alert));
			_iAlertController->actionPerformed(disEventOn);
		}
		else
		{
			_iAlertModel->setPropertyString(CString(_T("disableState")),CString(_T("on")));
			CDisableAlertEvent disEventOff(
				CString(_T("disable")),
				CString(_T("disable")),
				CString(param.m_alert));
			_iAlertController->actionPerformed(disEventOff);
			CDisableAlertEvent disEventOn(
				CString(_T("enable")),
				CString(_T("disable")),
				CString(param.m_alert));
			_iAlertController->actionPerformed(disEventOn);
		}
		if (menu) menu->refreshIcon();
	}
}

CUnread::CUnread(NodeALERTS::NodeCOMMANDS::NodeUNREAD* _param) : CCommand(_param), param(*_param)
{
}

CString CUnread::getName()
{
	return _T("UNREAD");
}

void CUnread::run(IActionEvent& /*event*/)
{
	if (_iAlertModel)
	{
		if (_iAlertModel->getStatesIndex()&_MESSAGES_UNREAD_)
		{
			SysMenuEvent cSysMenuEvent(
				CString(_T("select")),
				CString(_T("sysmenu")),
				CString(param.m_yes)
				);
			_iAlertController->actionPerformed(cSysMenuEvent);
		}
		else
		{
			SysMenuEvent cSysMenuEvent(
				CString(_T("select")),
				CString(_T("sysmenu")),
				CString(param.m_no)
				);
			_iAlertController->actionPerformed(cSysMenuEvent);
		}
	}
}

CSysMenuCommand::CSysMenuCommand(NodeALERTS::NodeCOMMANDS::NodeSYSMENU* _param) : CCommand(_param), param(*_param)
{
}

CString CSysMenuCommand::getName()
{
	return _T("UNREAD");
}

void CSysMenuCommand::run(IActionEvent& /*event*/)
{
	if (_iAlertModel)
	{
		CSysMenu *menu = (CSysMenu*)_iAlertView->getViewByName(CString(_T("sysmenu")));
		if (menu)
		{
			::PostMessage(menu->m_hWnd,32968,NULL,WM_RBUTTONUP);
		}
	}
}

///////////////////////////////////////////////////

volatile LONG CStandBy::m_expire(0L);

CStandBy::CStandBy(NodeALERTS::NodeCOMMANDS::NodeSTANDBY* _param) : CCommand(_param), param(*_param), m_requestTimer(0)
{
}

CStandBy::~CStandBy()
{
	if(m_requestTimer)
	{
		::KillTimer(NULL, m_requestTimer);
	}
}

CString CStandBy::getName()
{
	return _T("STANDBY");
}

CString CStandBy::getStandbyUrl(BOOL enabled, int nextrequest)
{

	const int encode = _iAlertModel->getProperty(CString(_T("encoding")));
	CString url =_iAlertModel->getPropertyString(CString(_T("standbyUrl")), &CString(), encode, true);
	if(!url.IsEmpty())
	{
		url.Replace(_T("%standby%"), enabled ? _T("true") : _T("false"));

		CString tmp; tmp.Format(_T("%cnextrequest=%d"), (url.Find(_T('?'))>=0 ? _T('&') : _T('?')), nextrequest);
		url += tmp;
		
		url += _T("&domain=") + _iAlertModel->getPropertyString(CString(_T("domain")), &CString(), encode);
		url += _T("&fulldomain=") + _iAlertModel->getPropertyString(CString(_T("fulldomain")), &CString(), encode);
		url += _T("&compdomain=") + _iAlertModel->getPropertyString(CString(_T("compdomain")), &CString(), encode);
		url += _T("&edir=") + _iAlertModel->getPropertyString(CString(_T("edir")), &CString(), encode);
	}
	return url;
}

VOID CALLBACK CStandBy::TimerProc(HWND /*hwnd*/, UINT /*uMsg*/, UINT_PTR /*idEvent*/, DWORD /*dwTime*/)
{
	CAlertUtils::URLDownloadToFile(getStandbyUrl(TRUE, m_expire));
}

void CStandBy::run(IActionEvent& event)
{
	CSysMenu *menu = (CSysMenu*)_iAlertView->getViewByName(CString(_T("sysmenu")));
	if (event.getEventName() == CString(_T("enable")))
	{
		_iAlertModel->setPropertyString(CString(_T("standByState")),CString(_T("on")));
		CStandByEvent cEventOff(
			CString(_T("disable")),
			CString(_T("standby")),
			CString(_T("alert"))
			);
		_iAlertController->actionPerformed(cEventOff);

		CStandByEvent cEventOn(
			CString(_T("enable")),
			CString(_T("standby")),
			CString(_T("alert"))
			);
		_iAlertController->actionPerformed(cEventOn);

		int expire = CAlertUtils::str2sec(_iAlertModel->getPropertyString(CString(_T("standby_expire"))), 60);
		if(expire == 0)
		{
			expire = CAlertUtils::str2sec(_iAlertModel->getPropertyString(CString(_T("standby_confirm_expire"))), 0);
			if(expire == 0)
			{
				expire = CAlertUtils::str2sec(_iAlertModel->getPropertyString(CString(_T("normal_expire"))), 60);
			}
			expire = expire > 0 ? expire : 60;
			InterlockedExchange(&m_expire, expire);
			m_requestTimer = ::SetTimer(NULL, 0, expire*1000, (TIMERPROC)TimerProc);
		}
		CAlertUtils::URLDownloadToFile(getStandbyUrl(TRUE, expire));
	}
	else
	{
		_iAlertModel->setPropertyString(CString(_T("standByState")),CString(_T("off")));
		CStandByEvent cEventOff(
			CString(_T("disable")),
			CString(_T("standby")),
			CString(_T("alert"))
			);
		_iAlertController->actionPerformed(cEventOff);

		CStandByEvent cEventOn(
			CString(_T("enable")),
			CString(_T("alert")),
			CString(_T("alert"))
			);
		_iAlertController->actionPerformed(cEventOn);

		if(m_requestTimer)
		{
			::KillTimer(NULL, m_requestTimer);
		}

		int expire = CAlertUtils::str2sec(_iAlertModel->getPropertyString(CString(_T("normal_expire")), &CString()), 60);
		CAlertUtils::URLDownloadToFile(getStandbyUrl(FALSE, expire > 0 ? expire : 60));
	}
	if (menu) menu->refreshIcon();
}

//<BROWSERJAMP name="webjump" filename="%firstURL%"/>
CBrowserJump::CBrowserJump(NodeALERTS::NodeCOMMANDS::NodeBROWSERJAMP* _param) : CCommand(_param), param(*_param)
{
}

CString CBrowserJump::getName()
{
	return _T("CBrowserJump");
}

void CBrowserJump::run(IActionEvent& /*event*/)
{
	//::ShellExecute(NULL, _T("open"), _iAlertModel->buildPropertyString(param.m_filename), NULL, NULL, SW_SHOWNORMAL);
	if (SUCCEEDED(OleInitialize(NULL)))
	{
		if(_iAlertModel)
		{
			CComPtr<IWebBrowser2> pBrowser2;
			CoCreateInstance(CLSID_InternetExplorer, NULL, CLSCTX_LOCAL_SERVER,
							   IID_IWebBrowser2, (void**)&pBrowser2);
			if (pBrowser2)
			{
				const int encode = param.m_encoding ? param.m_encoding : _iAlertModel->getProperty(CString(_T("encoding")));
				CComVariant vtEmpty, vtPostData, vtHeaders;
				if(!param.m_post.IsEmpty())
				{
					CComSafeArray<char, VT_UI1> psa;
					// post data is specified.
					CT2A postData(_iAlertModel->buildPropertyString(param.m_post, encode?encode:CP_UTF8, true));
					psa.Add(strlen(postData), postData);
					vtPostData = psa;
					vtHeaders = L"Content-Type: application/x-www-form-urlencoded\n";
				}

				HRESULT hr = pBrowser2->Navigate(CComBSTR(_iAlertModel->buildPropertyString(param.m_filename, encode?encode:CP_UTF8, true)), &vtEmpty, &vtEmpty, &vtPostData, &vtHeaders);
				if (SUCCEEDED(hr))
				{
					pBrowser2->put_Visible(VARIANT_TRUE);
				}
				else
				{
					pBrowser2->Quit();
				}
			}
			OleUninitialize();
		}
	}
}

CString CHistory::getName()
{
	return _T("CHistory");
}

void CHistory::run(IActionEvent& event)
{
	if (event.getEventName()==_T("history"))
	{
		CHistoryActionEvent* evnt = (CHistoryActionEvent*)&event;
		CHistoryStorage::InsertAlertInHistory(evnt->getAlertData(), evnt->getWindow(), evnt->getHistoryId(), evnt->getIsCreated());
	}
	else
	{
		COpenWindowCommand::run(event);
	}
}

CRunApp::CRunApp(NodeALERTS::NodeCOMMANDS::NodeRUNAPP* _param) : CCommand(_param), param(*_param)
{
}

CString CRunApp::getName()
{
	return _T("CRunApp");
}

void CRunApp::run(IActionEvent& /*event*/)
{
	std::vector<NodeALERTS::NodeCOMMANDS::NodeRUNAPP::NodeWINDOWS*>::iterator rit = param.m_windows.begin();
	while(rit != param.m_windows.end())
	{
		if(!(*rit)->m_file.IsEmpty())
		{
			CString file = (*rit)->m_file;
			CString workdir = (*rit)->m_workdir;
			CString params = (*rit)->m_params;

			CString sysroot;
			sysroot.GetEnvironmentVariable(_T("SystemRoot"));
			if(sysroot.GetAt(sysroot.GetLength()-1) != _T('\\'))
			{
				sysroot += _T('\\');
			}

			CString sys32 = sysroot + _T("System32");
			CString sys64 = sysroot + _T("SysNative");
			if (!PathFileExists(sys64))
				sys64 = sys32;

			DWORD size = ExpandEnvironmentStrings((*rit)->m_file, NULL, 0);
			if(size > 0)
				file.ReleaseBuffer(ExpandEnvironmentStrings((*rit)->m_file, file.GetBuffer(size), size));
			
			CString prog;
			prog.GetEnvironmentVariable(_T("ProgramFiles"));
			if(prog.GetAt(prog.GetLength()-1) == _T('\\'))
			{
				prog.Truncate(prog.GetLength()-1);
			}

			CString comm;
			comm.GetEnvironmentVariable(_T("CommonProgramFiles"));
			if(comm.GetAt(comm.GetLength()-1) == _T('\\'))
			{
				comm.Truncate(comm.GetLength()-1);
			}

			file.Replace(_T("%ProgramFiles(x86)%"), prog);
			file.Replace(_T("%ProgramW6432%"), prog);
			file.Replace(_T("%CommonProgramFiles(x86)%"), comm);
			file.Replace(_T("%CommonProgramW6432%"), comm);
			file.Replace(_T("%SystemDir%"), sys32);
			file.Replace(_T("%SystemDir(x86)%"), sys32);
			file.Replace(_T("%SystemDirW6432%"), sys64);

			if (!workdir.IsEmpty())
			{
				size = ExpandEnvironmentStrings((*rit)->m_workdir, NULL, 0);
				if(size > 0)
					workdir.ReleaseBuffer(ExpandEnvironmentStrings((*rit)->m_workdir, workdir.GetBuffer(size), size));

				workdir.Replace(_T("%ProgramFiles(x86)%"), prog);
				workdir.Replace(_T("%ProgramW6432%"), prog);
				workdir.Replace(_T("%CommonProgramFiles(x86)%"), comm);
				workdir.Replace(_T("%CommonProgramW6432%"), comm);
				workdir.Replace(_T("%SystemDir%"), sys32);
				workdir.Replace(_T("%SystemDir(x86)%"), sys32);
				workdir.Replace(_T("%SystemDirW6432%"), sys64);
			}
			const HINSTANCE hinst = ::ShellExecute(0,0,file, params, workdir, SW_SHOW);
			ATLASSERT(hinst > (HINSTANCE)SE_ERR_DLLNOTFOUND);
			DBG_UNREFERENCED_LOCAL_VARIABLE(hinst);
		}
		++rit;
	}
}

IMVCCommand* CCommandFactory::create(NodeALERTS::NodeCOMMANDS::NodeCOMMAND* param)
{

	if (param->m_tagName==_T("UNINSTALL"))
	{
		return new CUninstall((NodeALERTS::NodeCOMMANDS::NodeUNINSTALL*)param);
	}
	if (param->m_tagName==_T("UPDATE"))
	{
		return new CUpdate((NodeALERTS::NodeCOMMANDS::NodeUPDATE*)param);
	}
	if (param->m_tagName==_T("DISABLE"))
	{
		return new CDisable((NodeALERTS::NodeCOMMANDS::NodeDISABLE*)param);
	}

	if (param->m_tagName==_T("STANDBY"))
	{
		return new CStandBy((NodeALERTS::NodeCOMMANDS::NodeSTANDBY*)param);
	}

	//<BROWSERJAMP name="webjamp" filename="%firstURL%"/>
	if (param->m_tagName==_T("BROWSERJAMP"))
	{
		return new CBrowserJump((NodeALERTS::NodeCOMMANDS::NodeBROWSERJAMP*)param);
	}

	if (param->m_tagName==_T("OPENWINDOW"))
	{
	    //appendToErrorFile(L"Run OPENWINDOW command");
		return new COpenWindowCommand((NodeALERTS::NodeCOMMANDS::NodeOPENWINDOW*)param);
	}

	//<MINIBROWSERJAMP name="minibrowser" window="minibrowserwindow" filename="%firstURL%"/>
	if ((param->m_tagName==_T("MINIBROWSERJAMP")) || (param->m_tagName==_T("MINIBROWSERJUMP")))
	{
		return new CMiniBrowserJamp((NodeALERTS::NodeCOMMANDS::NodeMINIBROWSERJAMP*)param);
	}

	if (param->m_tagName==_T("OPTION"))
	{
	    //appendToErrorFile(L"Run OPTION command");
		return new COption((NodeALERTS::NodeCOMMANDS::NodeOPTION*)param);
	}
	if (param->m_tagName==_T("HISTORY"))
	{
		NodeALERTS::NodeCOMMANDS::NodeHISTORY* paramHist = (NodeALERTS::NodeCOMMANDS::NodeHISTORY*)param;
		//paramHist->m_filename = _iAlertModel->getPropertyString(CString(_T("history_file")));
		return new CHistory(paramHist);
	}
	if (param->m_tagName==_T("RUNAPP"))
	{
		return new CRunApp((NodeALERTS::NodeCOMMANDS::NodeRUNAPP*)param);
	}
	if (param->m_tagName==_T("EXIT"))
	{
		return new CExit((NodeALERTS::NodeCOMMANDS::NodeEXIT*)param);
	}

	if (param->m_tagName==_T("ALERT"))
	{
		return new CAlertLoader((NodeALERTS::NodeCOMMANDS::NodeALERTCOMMAND*)param);
	}

	if (param->m_tagName==_T("UNREAD"))
	{
		return new CUnread((NodeALERTS::NodeCOMMANDS::NodeUNREAD*)param);
	}

	if (param->m_tagName==_T("SYSMENU"))
	{
		return new CSysMenuCommand((NodeALERTS::NodeCOMMANDS::NodeSYSMENU*)param);
	}
	return NULL;
}

//-------------------------------------------------------------------------------

CAlertController::CAlertController(void): m_screensaverManager(NULL), cAlertModel(NULL),
	mainHWND(NULL), m_bComponentCreated(FALSE), m_sysMenuHWND(NULL)
{
	m_serviceBridge = ServiceBridge::Instance();
	m_switchDesktopHandler = new CSwitchDesktopHandler();
	m_wallpaperManager = new CWallpaperManager();
	m_lockscreenManager = new CLockscreenManager();
	m_JAClient = new CJAClient(); 
	m_JAClient_v2 = new CJAClient_v2();
	isEnableAPIv2 = true;
}

CAlertController::~CAlertController(void)
{
	m_serviceBridge->FreeInstance();
	if (m_switchDesktopHandler) delete_catch(m_switchDesktopHandler);
	if(m_screensaverManager) delete_catch(m_screensaverManager);
	//if(cAlertModel) delete_catch(cAlertModel); TODO: fix this, but yet do not delete
	if(m_wallpaperManager) delete_catch(m_wallpaperManager);
}

ServiceBridge* CAlertController::getServiceBridge()
{
	return m_serviceBridge;
}


void CAlertController::loadAppCredentials()
{
	const int encode = _iAlertModel->getProperty(CString(_T("encoding")));
	CString domain_name = _iAlertModel->getPropertyString(CString(_T("fulldomain")), &CString(), encode);
	CString comp_domain_name = _iAlertModel->getPropertyString(CString(_T("compdomain")), &CString(), encode);
	if( domain_name.IsEmpty() || domain_name=="" )
		domain_name = _iAlertModel->getPropertyString(CString(_T("compdomain")), &CString(), encode);
	if( (domain_name.IsEmpty() || domain_name == "") && 
				_iAlertModel->getPropertyString(CString(_T("adEnabled")), &CString()) == "1" )
		domain_name = _iAlertModel->getPropertyString(CString(_T("domain_name")), &CString(), encode);

	//if( m_JAClient == NULL )
	//	m_JAClient = new CJAClient();
	//domainName, CString fullDomainName, CString compDomainName,  CString computerName
	m_JAClient->setCredentials(
		_iAlertModel->getPropertyString(CString("server")), 
		_iAlertModel->getPropertyString(CString("backup_server")),
		_iAlertModel->getPropertyString(CString("user_name"), &CString()),
		_iAlertModel->getPropertyString(CString("md5_password"), &CString()),
		domain_name,
		domain_name,
		comp_domain_name,
		_iAlertModel->getPropertyString(CString("COMPUTERNAME"), &CString()),
		_iAlertModel->getPropertyString(CString("ip")),
		_iAlertModel->getPropertyString(CString("version")),
		_iAlertModel->getPropertyString(CString("deskalerts_id")),
		_iAlertModel->getPropertyString(CString(_T("adEnabled")), &CString()) == "1",
        _iAlertModel->getPropertyString(CString(_T("synFreeAdEnabled")), &CString()) == "1"
	);
	m_JAClient_v2->setCredentials(_iAlertModel->getPropertyString(CString("server")),
		_iAlertModel->getPropertyString(CString("backup_server")),
		_iAlertModel->getPropertyString(CString("user_name"), &CString()),
		_iAlertModel->getPropertyString(CString("md5_password"), &CString()),
		domain_name,
		domain_name,
		comp_domain_name,
		_iAlertModel->getPropertyString(CString("COMPUTERNAME"), &CString()),
		_iAlertModel->getPropertyString(CString("ip")),
		_iAlertModel->getPropertyString(CString("version")),
		_iAlertModel->getPropertyString(CString("deskalerts_id")),
		_iAlertModel->getPropertyString(CString(_T("adEnabled")), &CString()) == "1",
		_iAlertModel->getPropertyString(CString(_T("synFreeAdEnabled")), &CString()) == "1"
	);
}

void CAlertController::setDefaultParams()
{
	if (mainHWND)
		_iAlertModel->setProperty(CString(_T("mainHWND")),(DWORD)mainHWND);
	
	HDESK hDesktop = GetThreadDesktop(GetCurrentThreadId());
	if (hDesktop)
	{
		_iAlertModel->setPropertyString(CString(_T("mainDesktopName")),CAlertUtils::getDesktopName(hDesktop));
	}
    if (hDesktop)
    {
        CloseDesktop(hDesktop);
    }

	if(_iAlertModel->getPropertyString(CString(_T("deskalerts_id")), &CString()).IsEmpty())
	{
		_iAlertModel->setPropertyString(CString(_T("deskalerts_id")), CAlertUtils::CreateGUID());
		_iAlertModel->setPropertyString(CString(_T("first_start")), CString(_T("1")));
	}
	else
		_iAlertModel->setPropertyString(CString(_T("first_start")), CString(_T("0")));

	if(!_iAlertModel->getPropertyString(CString(_T("usersession_id")), &CString()).IsEmpty())
	{
		_iAlertModel->setPropertyString(CString(_T("usersession_id")), CAlertUtils::CreateGUID());
	}

	_iAlertModel->setPropertyString(CString(_T("AlertMode")), CString(_T("normal")));
	_iAlertModel->setPropertyString(CString(_T("disableState")), CString(_T("off")));
	_iAlertModel->setPropertyString(CString(_T("standByState")), CString(_T("off")));
	_iAlertModel->setPropertyString(CString(_T("disableNewState")), CString(_T("off")));
	_iAlertModel->setPropertyString(CString(_T("unreadMessagesState")), CString(_T("off")));
	
	TCHAR path[MAX_PATH];
	ZeroMemory(path, MAX_PATH*sizeof(TCHAR));
	::GetModuleFileName(NULL,path,MAX_PATH);
	if(((_tcsstr(path,_T("iexplore.exe")) != NULL) || (_tcsstr(path,_T("explorer.exe")) != NULL)))
	{
		// If opened from toolbar or deskbar
		// iserg and @ntoxa were here in 2008
		CString strTp = CAlertUtils::getAppDataPath(_iAlertModel->getPropertyString(CString(_T("deskalerts_id"))));
		_iAlertModel->setPropertyString(CString(_T("user_path")), strTp);
		strTp.Replace(_T("\\Cache"),_T(""));
		_iAlertModel->setPropertyString(CString(_T("root_path")), strTp);
	}
	else
	{
		if(m_sAppDataPath != _T(""))
		{
			// If opened from stand alone application
			_iAlertModel->setPropertyString(CString(_T("root_path")), CAlertUtils::getDir(m_sAppDataPath));
		} else {
			_iAlertModel->setPropertyString(CString(_T("root_path")), CAlertUtils::getDir(CString(path)));
		}
		_iAlertModel->setPropertyString(CString(_T("user_path")), CAlertUtils::getAppDataPath(_iAlertModel->getPropertyString(CString(_T("deskalerts_id")))));
	}
	
	NodeALERTS& alNod =_iAlertModel->getModel(CString(_T("")));

	_iAlertModel->setProperty(CString(_T("encoding")), alNod.m_encoding);

	CString icons = alNod.m_icons;
	if(!icons.IsEmpty())
	{
		CImageList imList;

		CBitmap b(AtlLoadBitmapImage((TCHAR*)icons.GetString(),LR_LOADFROMFILE));
		if(b)
		{
			CSize bSize;
			b.GetSize(bSize);
			bSize.cx = bSize.cy;

			/*BITMAP bmpInfo = {0};
			b.GetBitmap(&bmpInfo);

			if(bmpInfo.bmBitsPixel < 32)*/
				imList.CreateFromImage((TCHAR*)icons.GetString(),bSize.cy,0,CLR_DEFAULT,IMAGE_BITMAP,LR_CREATEDIBSECTION|LR_LOADFROMFILE);
			/*else
			{
				imList.Create(bSize.cy, bSize.cy, ILC_COLOR32, 1, 1);
				imList.Add(b);
			}*/
			_iAlertModel->setImageList(imList.Detach());
		}
	}

	CString hot_icons = alNod.m_hot_icons;
	if(!hot_icons.IsEmpty())
	{
		CImageList imList;
		CBitmap b(AtlLoadBitmapImage((TCHAR*)hot_icons.GetString(),LR_LOADFROMFILE));
		if(b)
		{
			CSize bSize;
			b.GetSize(bSize);
			bSize.cx = bSize.cy;

			/*BITMAP bmpInfo = {0};
			b.GetBitmap(&bmpInfo);

			if(bmpInfo.bmBitsPixel < 32)*/
				imList.CreateFromImage((TCHAR*)hot_icons.GetString(),bSize.cy,0,CLR_DEFAULT,IMAGE_BITMAP,LR_CREATEDIBSECTION|LR_LOADFROMFILE);
			/*else
			{
				imList.Create(bSize.cy, bSize.cy, ILC_COLOR32, 1, 1);
				imList.Add(b);
			}*/
			_iAlertModel->setHotImageList(imList.Detach());
		}
	}

	if(!alNod.m_name.IsEmpty())
	{
		_iAlertModel->setPropertyString(CString(_T("deskalerts_name")), alNod.m_name);
	}
	else
	{
		_iAlertModel->setPropertyString(CString(_T("deskalerts_name")), CString(_T("DeskAlerts")));
	}
}

void CAlertController::createComponent()
{
	if (m_bComponentCreated)
		return;
	m_bComponentCreated = TRUE;
	InterlockedExchange(&g_bDebugMode, FALSE);
	m_iMode = _NORMAL_;
	_iAlertController = this;
	DeskAlertsAssert(_iAlertController);
	if(!_iAlertModel)
	{
		InterlockedExchangePointer((void**)&_iAlertModel, new CAlertModel());
		_iAlertModel->createComponent();
	}
	
	cAlertModel = _iAlertModel;
	setDefaultParams();
	InterlockedExchange(&g_bDebugMode, (_iAlertModel->getPropertyString(CString("debug_mode")) == _T("1")));
	//const int encode = data.m_encoding ? data.m_encoding : _iAlertModel->getProperty(CString(_T("encoding")));
	loadAppCredentials();

	cAlertView.createComponent();
	m_sysMenuHWND = cAlertView.getSysMenuHWND();
	_iAlertView = &cAlertView;
	m_serviceBridge->Start();
	m_switchDesktopHandler->start();
	m_wallpaperManager->showWallpapers();

	release_try
	{
		NodeALERTS& nodeALERTS = _iAlertModel->getModel(CString(_T("commands")));
		if(nodeALERTS.m_comands)
		{
			vector<NodeALERTS::NodeCOMMANDS::NodeCOMMAND*> vComands = nodeALERTS.m_comands->m_comand;
			vector<NodeALERTS::NodeCOMMANDS::NodeCOMMAND*>::iterator it = vComands.begin();
			while(it != vComands.end())
			{
				if (
					!((*it)->m_name == _T("alert") && (_iAlertController->getInitFlags() & NO_ALERTLOADER)) &&
					!((*it)->m_name == _T("update") && (_iAlertController->getInitFlags() & NO_UPDATE))
				)
				{
					IMVCCommand* iCom = CCommandFactory::create(*it);
					if(iCom)
					{
						//appendToErrorFile(_T("Creating command %s"), (*it)->m_name);
						commandMap.insert(pair<CString,IMVCCommand*>((*it)->m_name,iCom));
						if ((*it)->m_name == _T("alert"))
						{
							CAlertLoader* al = dynamic_cast<CAlertLoader*>(iCom);
							if (al != NULL)
								al->setMainFrameHWND(mainFrameHWND);
						}
					}
					else
					{
						logSystemError(L"createComponent", GetLastError());
					}
				}
				it++;
			}
		}
	}
	release_catch_tolog(L"AlertController create exception");


	if (_iAlertModel->getPropertyString(CString("synFreeAdEnabled")) == "1")
	{
		HRESULT hr = NULL;
		CoInitialize(NULL);
		CString furl("");
		CString uname = _iAlertModel->getPropertyString(CString("user_name"));
		CString server = _iAlertModel->getPropertyString(CString("server"));
		CString fulldomain = _iAlertModel->getPropertyString(CString(_T("fulldomain")), &CString(), 0);
		//CString alertsFilePath = getTempFileName(_T("serverxml"),0);
		if (_iAlertModel->getPropertyString(CString(_T("deskalerts_id")), &CString()).IsEmpty())
		{
			_iAlertModel->setPropertyString(CString(_T("deskalerts_id")), CAlertUtils::CreateGUID());
			_iAlertModel->setPropertyString(CString(_T("first_start")), CString(_T("1")));
		}

		CString deskbarId = _iAlertModel->getPropertyString(CString("deskalerts_id"));

		TCHAR name [ UNLEN + 1 ];
		DWORD size = UNLEN + 1;
		GetUserName((TCHAR*)name, &size);
		IADsUser *pObject;
		hr = ADsGetObject(_T("WinNT://")+fulldomain+_T("/")+name+_T(",user"), IID_IADsUser, (void**) &pObject);
		//VARIANT *mobile = NULL;
		//VariantInit( mobile ) ;
		CString em(_T(""));
		if(SUCCEEDED(hr))
		{
			BSTR *email = new BSTR();
			pObject->get_EmailAddress(email);
			em = (LPCSTR)email;
			delete(email);

			//pObject->get_TelephoneMobile(mobile);
		}

		//CString mob = (LPCSTR)mobile->bstrVal;

		furl.Format(_T("%s/register.asp?domain_name=%s&uname=%s&uphone=&uemail=%s&syncFree=1&deskbar_id=%s"),server, fulldomain,uname, em, deskbarId);

		const HRESULT hDwnldres = CAlertUtils::URLDownloadToFile(furl,NULL);
		if (hDwnldres == S_OK)
		{

		}
		CoUninitialize();
	}


	//////////////////////////////////////////////
	//	Log in if not logged yet				//
	if (!(_iAlertController->getInitFlags() & NO_REGISTRATION))
	{
		if(_iAlertModel->getPropertyString(CString("user_name"), &CString()).IsEmpty())
		{
			SysMenuEvent cSysMenuEvent(
				CString(_T("select")),
				CString(_T("sysmenu")),
				CString(_T("minibrowser"))
				);
			_iAlertController->actionPerformed(cSysMenuEvent);
		}
	}
	if (!(_iAlertController->getInitFlags() & NO_FIRST_START))
	{
		if(_iAlertModel->getPropertyString(CString("first_start"), &CString(_T("0"))) == _T("1") && !_iAlertModel->getPropertyString(CString("urlAfterInstall"), &CString()).IsEmpty())
		{
			SysMenuEvent cSysMenuEvent(
				CString(_T("select")),
				CString(_T("sysmenu")),
				CString(_T("afterinstall"))
				);
			_iAlertController->actionPerformed(cSysMenuEvent);
		}
	}
	if (!(_iAlertController->getInitFlags() & NO_SCREENSAVER))
	{
		m_screensaverManager = new CScreensaverManager();
	}

	if (!(_iAlertController->getInitFlags() & NO_ALERTS_RESTORE))
	{
		restoreNotClosedAlerts();
	}
	
	CRegKey key = CAlertUtils::getDesktopRegKey();
	_iAlertModel->setPropertyString(CString(_T("user_screensaver")),CAlertUtils::QueryRegValue(key,_T("SCRNSAVE.EXE"),CString()));

	runAutoStart();

	const HANDLE pHandle = GetCurrentProcess();
	SetProcessWorkingSetSize(pHandle, (SIZE_T)-1, (SIZE_T)-1);
	CloseHandle(pHandle);
}


void CAlertController::destroyComponent()
{
	CScreensaverManager *screensaverManager = _iAlertController->getScreensaverManager();
	screensaverManager->disableScreensaver();

	CString tmp = CAlertUtils::getTempPath();
	m_bComponentCreated = FALSE;
	std::multimap<CString,IMVCCommand*>::iterator it = commandMap.begin();
	while (it != commandMap.end())
	{
		it->second->stop();
		it++;
	}

	cAlertView.destroyComponent();
	cAlertModel->destroyComponent();
	_iAlertView = NULL;

	if(m_JAClient)delete_catch(m_JAClient);
	if (m_JAClient_v2)delete_catch(m_JAClient_v2);

	if(!g_bDebugMode && !tmp.IsEmpty())
	{
		CAlertUtils::RecursiveDeleteDirectory(tmp);
	}
}

void CAlertController::setMainHWND(HWND& _mainHWND)
{
	mainHWND = _mainHWND;
	mainFrameHWND = _mainHWND;
}

HWND& CAlertController::getMainHWND()
{
	return mainHWND;
}

void CAlertController::runAutoStart()
{
	release_try
	{
		std::multimap<CString,IMVCCommand*>::iterator it = commandMap.begin();
		while(it != commandMap.end())
		{
			if(it->second->isAutoStart() && it->second->isCanStart())
			{
				SysMenuEvent cSysMenuEvent(CString(_T("autostart")), CString(_T("autostart")), it->first);
				it->second->run(cSysMenuEvent);
			}
			it++;
		}
	}
	release_catch_tolog(_T("runAutoStart exception"));
}

void CAlertController::refreshState()
{
	CString countStr;
	INT64 count = CDatabaseManager::shared(true)->getUnreadCount();
	CSysMenu *menu = (CSysMenu*)_iAlertView->getViewByName(CString(_T("sysmenu")));
	if (count > 0)
	{
		countStr.Format(_T("%I64d"),count);
		CString sUnreaded = __T("You have unread alerts");
		// sUnreaded.Replace(_T("%count"), countStr);
		_iAlertModel->setPropertyString(CString(_T("unreadMessagesState")), CString(_T("on")));
		_iAlertModel->setPropertyString(CString(_T("unreadCaption")), sUnreaded);
		if (menu != NULL) menu->refreshIcon();
	}
	else
	{
		_iAlertModel->setPropertyString(CString(_T("disableNewState")), CString(_T("off")));
		_iAlertModel->setPropertyString(CString(_T("disableCaption")), CString(_T("")));
		_iAlertModel->setPropertyString(CString(_T("unreadMessagesState")), CString(_T("off")));
		_iAlertModel->setPropertyString(CString(_T("unreadCaption")), CString(_T("")));
		if (menu != NULL) menu->refreshIcon();

	}
}

void CAlertController::restoreNotClosedAlerts()
{
	CDatabaseManager *db = CDatabaseManager::shared(false);
	if(db)
	{
		release_try
		{
			multimap<CString,shared_ptr<showData>> alerts;
			db->getNotClosedAlerts(alerts);

			multimap<CString,shared_ptr<showData>>::iterator it = alerts.begin();
			while(it != alerts.end())
			{
				_iAlertView->showViewByName(CString(it->first), it->second->url, it->second);
				it++;
			}

			refreshState();
		}
		release_catch_tolog(_T("restoreNotClosedAlerts exception"));
	}
}
//---------------------------------------------------------------------------------------------



//IActionListener
void CAlertController::actionPerformed(IActionEvent& event)
{
	release_try
	{
		bool found = false;
		CString& comName = event.getCommandName();
		std::multimap<CString,IMVCCommand*>::iterator it = commandMap.begin();
		while (it != commandMap.end())
		{
			if (
				!(comName == _T("alert") && (_iAlertController->getInitFlags() & NO_ALERTLOADER)) &&
				!(comName == _T("update") && (_iAlertController->getInitFlags() & NO_UPDATE))
			)
			{
				if(it->first == comName)
				{
					if(it->second->isCanStart())
					{
						it->second->run(event);
					}
					found = true;
				}
			}
			else
			{
				found = true;
			}
			it++;
		}
		if(!found)
		{
			// logDiagnosticMessage(_T("Command not found %s"), comName);
		}
	}
	release_catch_tolog(_T("Action Exception"));
}

//IAlertListener
void CAlertController::alertUpdated(IActionEvent& /*event*/)
{
}

//IModelListener
void CAlertController::alertModelUpdated(IModelEvent& /*event*/)
{
}

void CAlertController::propertyChanged(IPropertyChangeEvent& /*propCh*/)
{
}

void CAlertController::setInitFlags(int initFlags)
{
	m_initFlags = initFlags;
}

int CAlertController::getInitFlags()
{
	return m_initFlags;
}

CSwitchDesktopHandler* CAlertController::getSwitchDesktopHandler()
{
	return m_switchDesktopHandler;
}

CWallpaperManager* CAlertController::getWallpaperManager()
{
	return m_wallpaperManager;
}

CLockscreenManager* CAlertController::getLockscreenManager()
{
	return m_lockscreenManager;
}

CScreensaverManager* CAlertController::getScreensaverManager()
{
	return m_screensaverManager;
}

CJAClient * CAlertController::getJAClient()
{
	return m_JAClient;
}

CJAClient_v2* CAlertController::getJAClient_v2()
{
	return m_JAClient_v2;
}