#pragma once

#include "IInclude.h"

#include "SysMenu.h"
#include "HTMLView.h"
#include <map>


class CViewFactory
{
public:
	static IView* create(NodeALERTS::NodeVIEWS::NodeVIEW* param);
};

class CAlertView: public IAlertView3
{
public:
	CAlertView(void);
	~CAlertView(void);

//IComponent
public:
	void createComponent();
	void destroyComponent();

public:

	//IAlertListener
	void alertUpdated(IActionEvent& event);
	//IModelListener
 	void alertModelUpdated(IModelEvent& event);
	//CAlertView
	void showViewByName(CString& viewName,CString& param, shared_ptr<showData> ex, BOOL isNewAlert);
	void closeExpiredAlerts();
	bool showViewByName2(CString& viewName,CString& param, shared_ptr<showData> ex, BOOL isNewAlert);
	IView* getViewByName(CString& viewName);
	void closeOptions();
//	void closeExpiredAlerts();
	void propertyChanged(IPropertyChangeEvent& propCh);

	HWND getSysMenuHWND(VOID) 
	{
		return m_sysMenuHWND; 
	}
	//

private:

	std::map<CString,IView*> viewMap;

	HWND m_sysMenuHWND;

	//CSysMenu sysMenu;
	//CHTMLView cHTMLView;

};
