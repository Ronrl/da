#include "stdafx.h"
#include "AlertsDlg.h"

CAlertsDlg::CAlertsDlg(Localization *pLocale, VirtualToolbar *tbCtrl, CAlertController *pController)
{
	this->tbCtrl = tbCtrl;
	Locale = pLocale;
	this->pController = pController;
}

CAlertsDlg::~CAlertsDlg()
{
}

LRESULT CAlertsDlg::OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	Locale->localizeDialog(*this);

	WTL::CString isSound = pController->cAlertModel.getPropertyString(WTL::CString(_T("play_sound")));
	if (isSound == _T("1"))
		CheckDlgButton(IDC_CHECK_SOUND, 1);

	SetDlgItemText(IDC_EDIT_LOGIN, pController->cAlertModel.getPropertyString(WTL::CString(_T("user_name"))).GetBuffer(0));
	SetDlgItemText(IDC_EDIT_PASWD, pController->cAlertModel.getPropertyString(WTL::CString(_T("user_pswd"))).GetBuffer(0));
	SetDlgItemText(IDC_EDIT_RECEIVE,pController->cAlertModel.getPropertyString(WTL::CString(_T("normal_expire"))).GetBuffer(0));

	if(pController->cAlertModel.EnableActiveDirectory())
	{
		SendDlgItemMessage(IDC_EDIT_LOGIN, WM_ENABLE, (WPARAM) 0, (LPARAM) 0); 
		SendDlgItemMessage(IDC_EDIT_PASWD, WM_ENABLE, (WPARAM) 0, (LPARAM) 0);
		SendDlgItemMessage(IDC_EDIT_LOGIN, EM_SETREADONLY , (WPARAM) 1, (LPARAM) 0); 
		SendDlgItemMessage(IDC_EDIT_PASWD, EM_SETREADONLY , (WPARAM) 1, (LPARAM) 0);
	}

	return bHandled = TRUE;
}

LRESULT CAlertsDlg::OnClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	delete this;
	return bHandled = TRUE;
}
	

BOOL CAlertsDlg::OnApply()
{

	TCHAR strTmp[1024];
	GetDlgItemText(IDC_EDIT_LOGIN, strTmp, 1023);
	WTL::CString mLogin(strTmp);
	memset(strTmp, 0, 1023);
	GetDlgItemText(IDC_EDIT_PASWD, strTmp, 1023);
	WTL::CString mPaswd(strTmp);
	memset(strTmp, 0, 1023);

	GetDlgItemText(IDC_EDIT_RECEIVE, strTmp, 1023);
	WTL::CString mReceive(strTmp);

	WTL::CString mSound = (IsDlgButtonChecked(IDC_CHECK_SOUND)!=0)?_T("1"):_T("0");

	pController->cAlertModel.setPropertyString(WTL::CString(_T("normal_expire")), mReceive);
	pController->cAlertModel.setPropertyString(WTL::CString(_T("play_sound")), mSound);


	if(
		(pController->cAlertModel.getPropertyString(WTL::CString("user_name")) != mLogin)||
		(pController->cAlertModel.getPropertyString(WTL::CString("user_pswd")) != mPaswd)
		)
	{
		
		WTL::CString sUrl;
		sUrl.Format(L"%s/register.asp?uname=%s&upass=%s&desk_id=%s",
				pController->cAlertModel.getPropertyString(WTL::CString("serverpath")),
				mLogin.GetBuffer(0), 
				mPaswd.GetBuffer(0),
				pController->cAlertModel.getPropertyString(WTL::CString("deskalerts_id"))
				);

		USES_CONVERSION;
		TCHAR *ptr = tbCtrl->GetProgPath();
		WTL::CString  tmpFilePath (ptr);
		tbCtrl->DeallocMemory(ptr);

		tmpFilePath += WTL::CString("Cache\\register.html");
		//::MessageBox(0,sUrl,tmpFilePath,0);
		CoInitialize(0);
		HRESULT hResult = URLDownloadToFile(NULL, sUrl, tmpFilePath, 0, NULL);
		CoUninitialize();
		if( hResult == S_OK ) 
		{
			//::MessageBox(0,sUrl, tmpFilePath, 0);
			FILE* myFile=_tfopen(tmpFilePath, _T("r"));
			if(myFile)
			{
				char buf[10*1024];
				int cnt=0, rdcnt=0;
				memset(buf, 0, sizeof(buf));
				do
				{
					rdcnt = fread(buf+cnt, 1, 1024, myFile);
					cnt += rdcnt;
				} while ((rdcnt == 1024) && (cnt<1024*9));

				fclose(myFile);

				WTL::CString sTmp(buf);
				if(sTmp.Find(L"Error! This user name already exists")>=0)
				{
					::MessageBox(0,L"Error! This user name already exists.",L"Log In Error", 0);
					DeleteFile(tmpFilePath.GetBuffer(0));
					return false;
				} else if (sTmp.Find(L"Error! User name should be more then 4 symbols")>=0){
					::MessageBox(0,L"Error! User name should be more then 4 symbols.",L"Log In Error", 0);
					DeleteFile(tmpFilePath.GetBuffer(0));
					return false;
				} else {
					pController->cAlertModel.setPropertyString(WTL::CString(_T("user_name")), mLogin);
					pController->cAlertModel.setPropertyString(WTL::CString(_T("user_pswd")), mPaswd);
					//pController->destroyComponent();
					//pController->createComponent();
				}
				fclose(myFile);
				DeleteFile(tmpFilePath.GetBuffer(0));
			}
		} else {
			::MessageBox(0,L"Could not connect to server.",L"DeskAlerts Error",MB_ICONERROR);
		}
	}

	return true;
}