#include "stdafx.h"
#include "ServiceBridge.h"
#include "AlertUtils.h"
#include "UpdateDataManager.h"
#include "RequestDataManager.h"
#include "RegistryDataManager.h"
#include "ShowWindowDataManager.h"
#include "HistoryStorage.h"
#include "CloseWindowDataManager.h"
#include <time.h>

ServiceBridge* ServiceBridge ::_self=NULL;
volatile LONG ServiceBridge :: _refcount=0;
DWORD listenCloseEventThreadId(0);
DWORD recieveThreadId(0);

ServiceBridge::ServiceBridge(void)
{
	Create();
}

ServiceBridge::~ServiceBridge(void)
{
	if (IsWindow()) DestroyWindow();
}

ServiceBridge* ServiceBridge::Instance()
{
	if(InterlockedIncrement(&_refcount) == 1)
	{
		_self = new ServiceBridge();
	}
	return _self;
}

void ServiceBridge::FreeInstance()
{
	if(InterlockedDecrement(&_refcount) == 0)
	{
		delete_catch(this);
		_self=NULL;
	}
}

DWORD WINAPI ServiceBridge::listenCloseEventThread(LPVOID /*pv*/)
{
	DWORD res = 1;
	DWORD startTick = time(NULL);
	while(true)
	{
		const HANDLE event = OpenEvent(EVENT_ALL_ACCESS, FALSE, DA_CLIENT_CLOSE_EVENT);
		if(event && WaitForSingleObject(event, 0) == WAIT_OBJECT_0)
		{
			SetEvent(event); //if old service compatibility
			DWORD currentTick = time(NULL);
			if( currentTick - startTick > 10 ){
				HWND mainHWND =(HWND)_iAlertModel->getProperty(CString(_T("mainHWND")));
				::PostMessage(mainHWND,WM_CLOSE,0,0);
			}
			break;
		}
		Sleep(1000);
	}
	return res;
}


void ServiceBridge::Start()
{
	/*const HANDLE hThread = */
	CreateThread(NULL, 0, ServiceBridge::listenCloseEventThread, NULL, 0, &listenCloseEventThreadId);
	StartRecieveMessages();
}

DWORD WINAPI ServiceBridge::recieveThread(LPVOID pv)
{
	DWORD pid = GetCurrentProcessId();
	CString pidStr;
	pidStr.Format(_T("%d"),pid);

	while(true)
	{
		HANDLE m_readyEventHandle = NULL;
		HANDLE m_mappingReadyEventHandle  = NULL;
		HANDLE m_mapping = NULL;
		HANDLE m_eventHandle = NULL;

		while(!(m_readyEventHandle&&m_mappingReadyEventHandle&&m_mapping&&m_eventHandle))
		{
			m_readyEventHandle = OpenEvent(EVENT_ALL_ACCESS, FALSE, INTERACTION_SERVICE_READY_EVENT_NAME + pidStr);
			m_mappingReadyEventHandle = OpenEvent(EVENT_ALL_ACCESS, FALSE, INTERACTION_SERVICE_MAP_READY_EVENT_NAME + pidStr);
			m_mapping = OpenFileMapping(FILE_MAP_ALL_ACCESS, FALSE, INTERACTION_SERVICE_MAP_NAME + pidStr);
			m_eventHandle = OpenEvent(EVENT_ALL_ACCESS, FALSE, INTERACTION_SERVICE_EVENT_NAME + pidStr);
			Sleep(1000);
		}	
		SetEvent(m_readyEventHandle);
		SetEvent(m_mappingReadyEventHandle);
		bool reading=1;
		std::vector<PPACKET> packets;
		while(reading)
		{
			if(WaitForSingleObjectWithMsgLoop(m_eventHandle, INFINITE) == WAIT_OBJECT_0)
			{
					void* buffer = (void *)MapViewOfFile(m_mapping, FILE_MAP_READ, 0, 0, 0);

					void * tmpBuffer = (void*)(new char[sizeof(PACKET)]);
					memcpy(tmpBuffer,buffer,sizeof(PACKET));
					PPACKET packet = (PPACKET)tmpBuffer;

					packets.push_back(packet);
					if (packet->header.isLast)
					{
						reading=0;
					}

					UnmapViewOfFile(buffer);
					SetEvent(m_mappingReadyEventHandle);
			}
			else
			{
				break;
			}
		}

		IStream* pStream = NULL;
		CreateStreamOnHGlobal(NULL, TRUE, &pStream);

		std::vector<PPACKET>::iterator it=packets.begin();
		while(it!=packets.end())
		{

			pStream->Write((*it)->data,PACKET_LENGTH,NULL);
			it++;
		}

		HGLOBAL hGlobal = NULL;
		GetHGlobalFromStream(pStream, &hGlobal);

		LPVOID pData = GlobalLock(hGlobal);

		if (pData) {

			PREQUESTPACKET resultPacket = (PREQUESTPACKET)pData;
			unsigned int packetLength=sizeof(REQUESTPACKET)+resultPacket->header.length;


			void * tmpPacket= (void*)(new char[packetLength]);

			memcpy(tmpPacket,pData,packetLength);

			resultPacket = (PREQUESTPACKET)tmpPacket;
			resultPacket->data = (char*)tmpPacket + sizeof(REQUESTPACKET);
			if (RequestDataManager::checkStructHash(resultPacket->data,resultPacket->header.length,resultPacket->header.hash))
			{
				//PWINDOWDATA alert = (PWINDOWDATA)resultPacket->data;
				//alert->m_html = (WCHAR*)(resultPacket->data + sizeof(WINDOWDATA));
				//parcelManager->sendParcel(ServiceBridge::createParcel((void*)resultPacket));
				PostMessageDelWParam<REQUESTPACKET>((HWND)pv, WM_SERVICE_REQUEST, resultPacket, NULL);
			}
		}

		GlobalUnlock(hGlobal);
		pStream->Release();
		CloseHandle(m_readyEventHandle);
		CloseHandle(m_mappingReadyEventHandle);
		CloseHandle(m_mapping);
		CloseHandle(m_eventHandle);
	}
	return 0;
}

void ServiceBridge::StartRecieveMessages()
{
	/*const HANDLE hThread = */CreateThread(NULL, 0, ServiceBridge::recieveThread,(LPVOID)m_hWnd, 0, &recieveThreadId);
}

BOOL ServiceBridge::sendShowWindowRequest(CString &viewName, CString param,shared_ptr<showData> ex)
{
	BOOL res = FALSE;
	ShowWindowDataManager *dataManager=new ShowWindowDataManager();
	dataManager->setData(viewName, param, ex);
	res=sendRequestToService(dataManager);
	delete_catch(dataManager);
	return res;
}

BOOL ServiceBridge::sendCloseWindowRequest(INT64 alertId)
{
	BOOL res = FALSE;
	CloseWindowDataManager *dataManager = new CloseWindowDataManager();
	dataManager->setData(alertId);
	res=sendRequestToService(dataManager);
	delete_catch(dataManager);
	return res;
}


BOOL ServiceBridge::sendUpdateRequest(CStringW &updateRoot,CStringW &sRootPath, CStringW &version)
{
 	BOOL res = FALSE;
	UpdateDataManager *dataManager=new UpdateDataManager();
	dataManager->setData(updateRoot,sRootPath, version);
	res=sendRequestToService(dataManager);
	delete_catch(dataManager);
	return res;
}

BOOL ServiceBridge::sendRegistryRequest(PREGISTRYDATA regData)
{
	BOOL res = FALSE;
	RegistryDataManager *dataManager=new RegistryDataManager();
	dataManager->setData(regData);
	res=sendRequestToService(dataManager);
	delete_catch(dataManager);
	return res;
}

BOOL ServiceBridge::checkService()
{
	BOOL res = FALSE;
	const HANDLE eventHandle = OpenEvent(EVENT_MODIFY_STATE, FALSE, INTERACTION_SERVICE_EVENT_NAME);
	if(eventHandle)
	{
		HANDLE serviceReadyHandle=OpenEvent(EVENT_ALL_ACCESS, FALSE, INTERACTION_SERVICE_READY_EVENT_NAME);
		HANDLE mappingServiceReadyHandle=OpenEvent(EVENT_ALL_ACCESS, FALSE, INTERACTION_SERVICE_MAP_READY_EVENT_NAME);
		if (serviceReadyHandle && mappingServiceReadyHandle)
		{
			res = TRUE;
		}
	}
	return res;
}

BOOL ServiceBridge::sendRequestToService(ServiceDataManager* dataManager)
{
	BOOL res = FALSE;
	const HANDLE eventHandle = OpenEvent(EVENT_MODIFY_STATE, FALSE, INTERACTION_SERVICE_EVENT_NAME);
	if(eventHandle)
	{
		HANDLE serviceReadyHandle=OpenEvent(EVENT_ALL_ACCESS, FALSE, INTERACTION_SERVICE_READY_EVENT_NAME);
		HANDLE mappingServiceReadyHandle=OpenEvent(EVENT_ALL_ACCESS, FALSE, INTERACTION_SERVICE_MAP_READY_EVENT_NAME);
		if(serviceReadyHandle && mappingServiceReadyHandle)
		{
			if(WaitForSingleObjectWithMsgLoop(serviceReadyHandle, INFINITE) == WAIT_OBJECT_0)
			{
				// Split into packets and send synchronously
				RequestDataManager requestDataManager(dataManager);
				IStream* pStream = NULL;
				CreateStreamOnHGlobal(NULL, TRUE, &pStream);
				requestDataManager.serialize(pStream);
				HGLOBAL hGlobal = NULL;
				GetHGlobalFromStream(pStream, &hGlobal);

				LPVOID pData = GlobalLock(hGlobal);

				for(unsigned int packetId = 0; packetId < requestDataManager.getPacketsCount(); packetId++)
				{
					if(WaitForSingleObjectWithMsgLoop(mappingServiceReadyHandle, INFINITE) != WAIT_OBJECT_0)
					{
						break;
					}
					HANDLE mapping = OpenFileMapping(FILE_MAP_ALL_ACCESS, FALSE, INTERACTION_SERVICE_MAP_NAME);
					if(mapping)
					{
						void* buffer = MapViewOfFile(mapping, FILE_MAP_WRITE, 0, 0, sizeof(PACKET));

						PPACKET packet = requestDataManager.getPacket(pData, packetId);
                        if (buffer != nullptr)
                        {
                            memcpy(buffer, (void*)packet, sizeof(PACKET));
                        }
						SetEvent(eventHandle);
						res = TRUE;
					}
					else
					{
						SetEvent(serviceReadyHandle);
					}
				}
			}
		}
	}
	return res;
}

LRESULT ServiceBridge::OnServiceRequest(UINT /*uMsg*/, WPARAM wParam, LPARAM /*lParam*/, BOOL& bHandled)
{
	PREQUESTPACKET packet = (PREQUESTPACKET)(wParam);
	DWORD dataType=packet->header.dataType;
	if (dataType==REQUESTDATATYPE_WINDOW)
	{
		shared_ptr<showData> ex(new showData());
		PWINDOWDATA windowData = parseWindowData(packet);

		CString param = windowData->param;
		CString viewName = windowData->viewName;
		ex->encoding = windowData->encoding;
		ex->UTC = windowData->UTC;
		ex->utf8 = windowData->utf8;
		ex->history_id = windowData->history_id;
		ex->windowsCountDelta = windowData->windowsCountDelta;
		ex->windowRect = windowData->windowRect;
		ex->url = windowData->url;
		ex->post = windowData->post;
		ex->expire = windowData->expire;
		ex->acknowledgement = windowData->acknowledgement;
		ex->create_date = windowData->create_date;
		ex->title = windowData->title;
		ex->width = windowData->width;
		ex->height = windowData->height;
		ex->position = windowData->position;
		ex->autoclose = windowData->autoclose;
		ex->topTemplateHTML = windowData->topTemplateHTML;
		ex->bottomTemplateHTML = windowData->bottomTemplateHTML;
		ex->visible = windowData->visible;
		ex->cdata = windowData->cdata;
		ex->ticker = windowData->ticker;
		ex->userid = windowData->userid;
		ex->alertid = windowData->alertid;
		ex->survey = windowData->survey;
		ex->schedule = windowData->schedule;
		ex->recurrence = windowData->recurrence;
		ex->urgent = windowData->urgent;
		ex->searchTerm = windowData->searchTerm;
		ex->desktopName = windowData->desktopName;
		
		_iAlertView->showViewByName(viewName, ex->url, ex);
	}
	else if (dataType==REQUESTDATATYPE_CLOSEWINDOW)
	{
		PCLOSEWINDOWDATA closeWindowData = (PCLOSEWINDOWDATA)(packet->data);
		CHistoryStorage::MarkAlertAsClosed(closeWindowData->alertId);
	}
	
    deleteArray_catch(packet);

	bHandled = TRUE;
	return S_OK;
}

