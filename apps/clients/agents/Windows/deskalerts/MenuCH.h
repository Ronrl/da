#include "afxtempl.h"
#if !defined(CMenuCH_h)
#define CMenuCH_h

#define MIT_ICON	 1
#define MIT_COLOR	 2
#define MIT_XP		 3

#define COLOR_TEXT_BKGND GetSysColor(COLOR_MENU)

HBITMAP ReplaceColor(HBITMAP hBitmap, COLORREF crColorSrc, COLORREF crColorDst, int width, int height);

class CMenuItem
{
public:
	CMenuItem(LPCSTR strText, UINT uid, CBitmap* pBitmap=NULL,HICON hIcon=NULL)
	{
		lstrcpy(m_szText,strText);
		m_pBitmap = pBitmap;
		m_hIcon   = hIcon;
		m_ID = uid;
	}
public:
	char	  m_szText[128];		// message text 
	char*     m_pszHotkey;			// hotkey text
	CBitmap*  m_pBitmap;			
	HICON	  m_hIcon;
	int		  m_nWidth,			
		      m_nHeight;
	UINT	  m_ID;
};
class CMenuCH:public CMenu
{
public:
	CMenuCH();
	~CMenuCH();
	void clear();
public:
	void SetMenuWidth(DWORD width);
	void SetMenuHeight(DWORD height);
	void SetMenuType(UINT nType);
	BOOL AppendMenu(UINT nFlags,UINT nIDNewItem,LPCSTR lpszNewItem,
					UINT nIDBitmap,HICON hIcon=NULL);
	BOOL AppendMenu(UINT nFlags,UINT nIDNewItem,LPCSTR lpszNewItem,
					HBITMAP hBitmap,HICON hIcon=NULL);
	virtual void MeasureItem(LPMEASUREITEMSTRUCT lpMIS);
	void SetMenuItemCaption(UINT uid, CString strNew);
	void setHWND(HWND hwnd);

protected:
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDIS);
	void DrawXPMenu(LPDRAWITEMSTRUCT lpDIS);
	void DrawColorMenu(LPDRAWITEMSTRUCT lpDIS);
	void DrawIconMenu(LPDRAWITEMSTRUCT lpDIS);
	void DrawCheckMark(CDC* pDC,int x,int y,COLORREF color);
protected:
	UINT        m_nType;
	DWORD 		m_Width,
				m_Height;
	CTypedPtrList<CPtrList,CMenuItem *> m_MenuList;

	HWND m_hWnd;

public:
	COLORREF	m_SelColor;
	int			m_curSel;


//private:
//	std::vector<HBITMAP> vBitMaps;

};
#endif