﻿#include "StdAfx.h"
#include "alertloader.h"
#include "AlertUtils.h"
#include "AlertView.h"
#include <time.h>
#include <ctime>
#include <atltime.h>
#include <windows.h>
#include <Lmcons.h>
#include <Mmsystem.h>
#include "SysMenu.h"
#pragma comment(lib,"Winmm")
#include "GMUtils.h"
#include "OptionConf.h"
#include "WallpaperManager.h"
#include "LockscreenManager.h"
#include "ScreensaverManager.h"
#include "DatabaseManagr.h"
#include "WindowCommand.h"
#include <fstream>
#include <algorithm>
#include "AlertsJAClient.h"
#include "UpdateCheckerJAClient.h"
#include "rapidjson/filereadstream.h"
#include <cstdio>
#include <sys/stat.h>


#define TOP_TEMPLATE_START_SEPARATOR "<!-- top_template_start -->"
#define TOP_TEMPLATE_END_SEPARATOR "<!-- top_template_end -->"
#define BOTTOM_TEMPLATE_START_SEPARATOR "<!-- bottom_template2_start -->"
#define BOTTOM_TEMPLATE_END_SEPARATOR "<!-- bottom_template2_end -->"


// utilites
bool compareTime(int expire, bool needUpdate);
vector<wstring> CheckUserGroups(IADsUser *pUser);
int checksum(std::ifstream& file);


//rd_user CAlertLoader::m_urd;

CAlertLoader::CAlertLoader(NodeALERTS::NodeCOMMANDS::NodeALERTCOMMAND* param): CCommand(param), data(*param),
m_dwThreadId(0), m_timerThread(NULL),m_dwThreadAdId(0), m_timerThreadAd(NULL), m_mainFrameHWND(NULL)
{
	//	HRESULT hr = CoInitialize(NULL);
	//	hr;
	//	Notification::INotificationPtr tmp(__uuidof(Notification::DeskAlertsNotification));
	//	notifier = tmp;
	//	notifier(__uuidof(Notification::DeskAlertsNotification));
	//	notifier->SubscribeToSessionLockEvent();

	//	CoUninitialize();

	m_parentHWND = ::GetDesktopWindow();
	
	CAlertUtils::GetCitrixSessionIp();
}

CAlertLoader::~CAlertLoader(void)
{
	stop();
}

void CAlertLoader::setMainFrameHWND(HWND hWnd)
{
	m_mainFrameHWND = hWnd;
}
/************************************************************************/
/* Window event                                                         */
/************************************************************************/

LRESULT CAlertLoader::OnCreate(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	return 0;
}

LRESULT CAlertLoader::OnDestroy(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	stop();
	return 0;
}

LRESULT CAlertLoader::OnNotifyXMLUpdate(UINT /*uMsg*/, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	DeskAlertsAssert(_iAlertModel);
	if (_iAlertModel)
	{
		if (lParam == NULL)
		{
			if(wParam == NULL && _iAlertModel->getPropertyString(CString(_T("AlertMode"))) == _T("offline"))
			{
				_iAlertModel->setPropertyString(CString(_T("AlertMode")), CString(_T("normal")));
				IView *pMenu = (CSysMenu*)_iAlertView->getViewByName(CString(_T("sysmenu")));
				if(pMenu)
				{
					CSysMenu *menu = (CSysMenu *) pMenu;
					menu->refreshIcon();
				}
			}
			// exit if there are no alert to show
		}
		else
		{
            DeskAlertsAssert(NULL != wParam);
            if (NULL != wParam)
            {
                CString* xmlPath = (CString*)lParam;
                alertListUpdated(*xmlPath, shared_ptr<NodeALERT>((NodeALERT*)wParam));
                delete_catch(xmlPath);
            }
		}
		bHandled = TRUE;
	}
	return S_OK;
}

LRESULT CAlertLoader::OnNotifyXMLUnreaded(UINT /*uMsg*/, WPARAM wParam, LPARAM /*lParam*/, BOOL& bHandled)
{
	if (_iAlertModel)
	{
		CSysMenu *menu = (CSysMenu*)_iAlertView->getViewByName(CString(_T("sysmenu")));
		if (*((CString *)wParam) != _T("0"))
		{
			CString sUnreaded = CString(_T("You have unread alerts"));
			// sUnreaded.Replace(_T("%count"), *((CString *)wParam));
			// sUnreaded.Replace(_T("%s"), *((CString *)wParam)); //for back compatibility
			_iAlertModel->setPropertyString(CString(_T("disableNewState")), CString(_T("on")));
			_iAlertModel->setPropertyString(CString(_T("disableCaption")), sUnreaded);
			if (menu != NULL) menu->refreshIcon();
		}
		bHandled = TRUE;
	}
	return S_OK;
}

LRESULT CAlertLoader::OnPurgeHistory(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled)
{
	if(_iAlertModel)
	{
		CString sHistoryExpire = _iAlertModel->getPropertyString(CString("history_expire"), &CString());
		if(!sHistoryExpire.IsEmpty() && sHistoryExpire != _T("0"))
		{
			CString sMode = _iAlertModel->getPropertyString(CString("history_expire_mode"), &CString());
			if (!sMode.IsEmpty() && sMode != _T("0"))
			{
				CString sTime = _iAlertModel->getPropertyString(CString("history_expire_time"), &CString());
				if (sTime.IsEmpty()) sTime = _T("00:00");
				CHistoryStorage::PurgeAlertsAtTime(sHistoryExpire, sTime);
			}
			else
			{
				CHistoryStorage::PurgeAlerts(sHistoryExpire);
			}
		}
		bHandled = TRUE;
	}
	return S_OK;
}
LRESULT CAlertLoader::OnErrorConnection(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled)
{
	if(_iAlertModel)
	{
		_iAlertModel->setPropertyString(CString(_T("AlertMode")), CString(_T("offline")));
		CSysMenu *menu = (CSysMenu*)_iAlertView->getViewByName(CString(_T("sysmenu")));
		CString sError = _iAlertModel->getPropertyString( CString(_T("connectionError")), &CString(_T("Cannot establish a connection.")) );
		_iAlertModel->setPropertyString(CString(_T("modeCaption")),sError);
		if (menu != NULL) menu->refreshIcon();

		appendToErrorFile(L"Server response:\tTimeout. Connection lost\n");

		// Necessary to start DeskAlerts' restart timer
		::PostMessage(m_mainFrameHWND, UWM_NOTIFY_LOST_CONNECTION, NULL, NULL);		

		bHandled = TRUE;
	}
	return S_OK;
}
// Notifies mainframe that connection is established and restart timer should be reset
LRESULT CAlertLoader::OnEstablishedConnection(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled)
{
	if(_iAlertModel)
	{
		appendToErrorFile(L"Server response:\tConnection established\n");

		//
		// Necessary to stop DeskAlerts' restart timer
		//
		::PostMessage(m_mainFrameHWND, UWM_NOTIFY_ESTABLISHED_CONNECTION, NULL, NULL);		
		//
		//
		//

		bHandled = TRUE;
	}
	return S_OK;
}

//
// Terminate alerts on server demand
//
LRESULT CAlertLoader::OnKillAlert(UINT /*uMsg*/, WPARAM wParam, LPARAM /*lParam*/, BOOL& bHandled)
{
	if(_iAlertModel)
	{
		CDatabaseManager *db = CDatabaseManager::shared(true);
		sqlite_int64 alertId = (sqlite_int64)wParam;
		sqlite_int64 hWnd1 = 0, hWnd2 = 0;
		if(db)
		{
			bool isTicker = db->CheckIfTicker(alertId);

			if (isTicker) 
			{
				// Redirect closing to js code

				// Get existing property
				CString property = _iAlertModel->getPropertyString(CString(_T("terminated_alerts_ids")));

				// Check that the alertId is already contained
				int position = 0;
				bool isAlreadyContained = false;
				CString token = property.Tokenize(_T(","), position);
				while (!token.IsEmpty())
				{
					if (_ttoi(token) == alertId) 
					{
						isAlreadyContained = true;
						break;
					}
					token = property.Tokenize(_T(","), position);
				}

				// Add alertId to property
				if (!isAlreadyContained) {
					property.Format(_T("%s,%d"), property, alertId);

					// Set new property
					_iAlertModel->setPropertyString(CString(_T("terminated_alerts_ids")), property);
				}
			}
			else 
			{
				// Close windows manually

				// Get windows hWnds
				db->GethWndByAlertId(alertId, &hWnd1, &hWnd2);

				// Process if only the alert is displayed
				if (hWnd1 > 0 || hWnd2 > 0) 
				{
					if (hWnd1 > 0)
						::PostMessage((HWND)hWnd1, WM_CLOSE, 0, 0);
					if (hWnd2 > 0)
						::PostMessage((HWND)hWnd2, WM_CLOSE, 0, 0);
					
					// Clean local database
					db->DeleteFromAlertId_hWnd(alertId);
				}
			}
		}
		bHandled = TRUE;
	}
	return S_OK;
}

LRESULT CAlertLoader::OnSetVariable(UINT /*uMsg*/, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	if(_iAlertModel)
	{
		CString* name = (CString*)wParam;
		CString* value = (CString*)lParam;
		_iAlertModel->setPropertyString(*name,*value);
		delete_catch(name);
		delete_catch(value);
		bHandled = TRUE;
	}
	return S_OK;
}

LRESULT CAlertLoader::OnScreensavers(UINT /*uMsg*/, WPARAM wParam, LPARAM /*lParam*/, BOOL& bHandled)
{
	if(_iAlertModel)
	{
		vector<NodeALERT*>* alerts = (vector<NodeALERT*>*)wParam;
		vector<NodeALERT*>::iterator it = alerts->begin();
		while(it != alerts->end())
		{
			NodeALERT* curNode = (NodeALERT*) (*it);
			curNode->setIsUnread(false);
			curNode->m_class = "2";
            BOOL newCreated = FALSE;
			addToHistory(shared_ptr<NodeALERT>(curNode),CString(), newCreated);
			it++;
		}
		delete_catch(alerts);
		bHandled = TRUE;
	}
	return S_OK;
}

LRESULT CAlertLoader::OnWallpapers(UINT /*uMsg*/, WPARAM wParam, LPARAM /*lParam*/, BOOL& bHandled)
{
	if(_iAlertModel)
	{
		vector<NodeALERT*>* alerts = (vector<NodeALERT*>*)wParam;
		vector<NodeALERT*>::iterator it = alerts->begin();

		while(it != alerts->end())
		{
			NodeALERT* curNode = (NodeALERT*) (*it);
			curNode->m_class = "8";
			curNode->setIsUnread(false);
            BOOL newCreated = FALSE;
			addToHistory(shared_ptr<NodeALERT>(curNode),CString(),newCreated);
			it++;
		}
		delete_catch(alerts);
		_iAlertController->getWallpaperManager()->showWallpapers();
		bHandled = TRUE;
	}
	return S_OK;
}

LRESULT CAlertLoader::OnLockscreens(UINT /*uMsg*/, WPARAM wParam, LPARAM /*lParam*/, BOOL& bHandled)
{
	if (_iAlertModel)
	{
		vector<NodeALERT*>* alerts = (vector<NodeALERT*>*)wParam;
		vector<NodeALERT*>::iterator it = alerts->begin();

		while (it != alerts->end())
		{
			NodeALERT* curNode = (NodeALERT*)(*it);
			curNode->m_class = "4096";
			curNode->setIsUnread(false);
			BOOL newCreated = FALSE;
			addToHistory(shared_ptr<NodeALERT>(curNode), CString(), newCreated);
			it++;
		}
		delete_catch(alerts);
		_iAlertController->getLockscreenManager()->showLockscreens();
		bHandled = TRUE;
	}
	return S_OK;
}


/************************************************************************/
/*                                                                      */
/************************************************************************/

CString CAlertLoader::getName()
{
	return _T("ALERT");
}

void CAlertLoader::run(IActionEvent& event)
{
	CString str = event.getEventName();
	if (str==_T("enable") || str==_T("autostart"))
	{
		myRun(event);
	}
	else
	{
		myStop();
	}
}

void CAlertLoader::stop()
{
	myStop();
}

BOOL CAlertLoader::isAutoStart()
{
	return TRUE;
}

void CAlertLoader::myRun(IActionEvent& event)
{
	if (_iAlertModel)
	{
		appendToErrorFile(L"Lets start AlertLoader: %s", event.getSourceName());
		HWND tmp = (HWND)_iAlertModel->getProperty(CString(_T("mainHWND")));
		if (tmp)
		{
			m_parentHWND = tmp;
		}

		if(!m_hWnd) Create(m_parentHWND);
		createTimerThreadForAdSync(event);
		createTimerThread(event);

		appendToErrorFile(L"AlertLoader has been started: %s", event.getSourceName());
	}
}

void CAlertLoader::myStop()
{
	appendToErrorFile(L"Lets stop AlertLoader with thread handle: %p", m_timerThread);
	if(m_timerThread)
	{
		//WaitForSingleObjectWithMsgLoop();
		DWORD exitCode;
		if(GetExitCodeThread(m_timerThread, &exitCode))
		{
			if(exitCode == STILL_ACTIVE)
			{
				const HANDLE event = CreateEvent(NULL, TRUE, FALSE, NULL);
				if(!::PostThreadMessage(m_dwThreadId,UWM_STOP_THREAD,0,(LPARAM)event) ||
					WaitForSingleObject(event, 5000) == WAIT_OBJECT_0)
				{
					CloseHandle(event);
				}
			}
		}
		CloseHandle(m_timerThread);
		m_timerThread = NULL;
	}
	if(m_timerThreadAd)
	{
		//WaitForSingleObjectWithMsgLoop();
		DWORD exitCode;
		if(GetExitCodeThread(m_timerThreadAd, &exitCode))
		{
			if(exitCode == STILL_ACTIVE)
			{
				const HANDLE event = CreateEvent(NULL, TRUE, FALSE, NULL);
                if (event != nullptr)
                {
                    if (!::PostThreadMessage(m_dwThreadAdId, UWM_STOP_THREAD, 0, (LPARAM)event) ||
                        WaitForSingleObject(event, 5000) == WAIT_OBJECT_0)
                    {
                        CloseHandle(event);
                    }
                }
			}
		}
		CloseHandle(m_timerThreadAd);
		m_timerThreadAd = NULL;
	}
	if (::GetParent(m_hWnd)==::GetDesktopWindow()) this->DestroyWindow();
	appendToErrorFile(L"AlertLoader has been stopped with thread handle: %p", m_timerThread);
}


/************************************************************************/
/*                                                                      */
/************************************************************************/



BOOL FuckYeahChecker()
{
	BOOL result = TRUE; //you may show alerts
	//CString str = L"Software\\DeskAlertsPlugin";
	CString str = CGMUtils::GetPreCompanyKey() + L"\\" + _T("DeskAlertsPlugin");
	CRegKey hKey;
	if (ERROR_SUCCESS == hKey.Open(HKEY_CURRENT_USER, str, KEY_READ))
	{
		DWORD dwValue = 0;
		if(ERROR_SUCCESS == hKey.QueryDWORDValue(_T("DisableAlerts"), dwValue))
		{
			if (0 != dwValue)
				result = FALSE; //you should disable alerts
		}
		hKey.Close();
	}
	return result;
}

char * convertToCharArray(CString str)
{
	const int len = str.GetLength();
	char * output = new char[len + 1];
	
	strcpy_s(output, len, CStringA(str).GetString());
	output[len] = '\0';
	
	return output;
}

void CAlertLoader::synchronizeScreensavers( ThreadData* thData, CString hash, bool isBackup, CString authorization )
{
	if (_iAlertModel->getPropertyString(CString(_T("screensaverEnabled"))) != _T("1")) return;
	CString hashPropName = _T("screensaversHash");
	if (_iAlertModel->getPropertyString(hashPropName, &CString()) != hash)
	{
		CScreensaverManager *screensaverManager = _iAlertController->getScreensaverManager();
		if (screensaverManager)
		{
			if (hash.IsEmpty())
			{
				screensaverManager->disableScreensaver();
			}
			else
			{
				screensaverManager->enableScreensaver();
			}
		}
		SYSTEMTIME lt;           
		GetLocalTime(&lt);
		CString furl2 = "";
		CString screenUrl = !isBackup ? thData->screensaversUrl : thData->backupScreensaversUrl;
		furl2.Format(_T("%s&localtime=%02d.%02d.%d|%02d:%02d:%02d"), screenUrl, lt.wDay,lt.wMonth,lt.wYear,lt.wHour,lt.wMinute,lt.wSecond);
		
		CString resultXML;
		/*const HRESULT hres = */CAlertUtils::URLDownloadToFile(furl2, NULL, &resultXML, nullptr, NULL);
		NodeSYNCHRONIZEDALERTS alerts;
		XMLParser parser((XmlNode *)&alerts, _T("SCREENSAVERS"));
		parser.Parse(resultXML,false);
		CDatabaseManager *db = CDatabaseManager::shared(true);
		if(db)
		{
			db->synchronizeAlerts(alerts.m_alerts,_T("2"));
		}
		vector<NodeALERT*>::iterator alertsIterator = alerts.m_alerts->begin();

		// save mediafile to tmp storage
		CString resultHTML;
		CString tmpPath = CAlertUtils::getWallpapersPath();
		CString url;
		CString fileName;
		//

		CString localTime = "";
		localTime.Format(_T("%02d.%02d.%d|%02d:%02d:%02d"), lt.wDay,lt.wMonth,lt.wYear,lt.wHour,lt.wMinute,lt.wSecond);												
		JAReq_Received receivedReg( authorization, localTime);


		while(alertsIterator != alerts.m_alerts->end())
		{
			NodeALERT* curNode = *alertsIterator;
			curNode->m_isUTF8 = FALSE;
			CString alertHref ;
			alertHref.Format(_T("%s&domain=%s&fulldomain=%s&compdomain=%s"), curNode->m_href, thData->domain_name, thData->authorization == UserName?"":thData->domain_name, thData->domain_name);
			HRESULT hres = CAlertUtils::URLDownloadToFile(alertHref, NULL, &resultHTML, &curNode->m_isUTF8,
				_iAlertModel->getPropertyString(CString(_T("decryption"))) == CString(_T("1"))?&_iAlertModel->getPropertyString(CString(_T("decryption_key")), &CString(_T("nj6kjvk58s8wb2d73"))):NULL);
			// check if it's a powerpoint made screensaver
			if(resultHTML.Find(_TEXT("screensaver_type=\"powerpoint\"")) >= 0
				|| resultHTML.Find(_TEXT("screensaver_type=\"video\"")) >= 0)
			{
				url = CAlertUtils::ExtractHref(resultHTML, _T("http://"));
				if (url.IsEmpty()) 
				{
					url = CAlertUtils::ExtractHref(resultHTML, _T("https://"));
				}
				fileName = tmpPath + CString(L"\\screensaver_") + CAlertUtils::ExtractFileName(url);
				CString modifiedHtml = CAlertUtils::ReplaceHref(resultHTML, fileName);
				curNode->m_html = modifiedHtml;
				hres = CAlertUtils::URLDownloadToFile(url, fileName, NULL, nullptr, NULL);
			}
			else
			{
				curNode->m_html = resultHTML;
			}

			if (hres == S_OK)
			{
				receivedReg.AddScreensaverId( atol(CT2A(curNode->m_alertid)), 1 );
			}
			else
			{
				logSystemError(L"Download screensavers data", GetLastError());
			}

			alertsIterator++;
			
		}

		_iAlertModel->setPropertyString(hashPropName, hash);

		if( receivedReg.getScreensaversList().capacity() > 0 )
		{
			furl2.Format( _T("%s/%s"), !isBackup ? thData->server : thData->backup_server, receivedReg.getURL() );
			std::string  confirmReq = receivedReg.toJSON();
			CString csReq = CString(confirmReq.c_str());
			CString headers = _T("Authorization: ") + authorization+ _T("\nContent-Type: application/json; charset=utf-8 ");
			CString alertsFilePath = CAlertUtils::getTempFileName(_T("serverxml"),0);
			HRESULT hres = CAlertUtils::URLDownloadToFile( furl2, /*alertsFilePath*/NULL, 0, 0, 0, &headers, &csReq );
		}

		::PostMessage(thData->hwnd, UWM_NOTIFY_SCREENSAVERS,(WPARAM)(alerts.m_alerts),NULL);
	}
	else if (hash.IsEmpty())
	{
		CScreensaverManager *screensaverManager = _iAlertController->getScreensaverManager();
		if (screensaverManager)
		{
			screensaverManager->disableScreensaver();
		}
	}
}

void CAlertLoader::synchronizeWallpapers(ThreadData* thData, CString hash, bool isBackup, CString authorization )
{
	CString hashPropName = _T("wallpapersHash");
	//as state hash summ changed as Wallpapers syncronize need for
	if (_iAlertModel->getPropertyString(hashPropName, &CString()) == hash) 
	    return; 
	CDatabaseManager *db = CDatabaseManager::shared(true);

	CString tmpPath = CAlertUtils::getWallpapersPath();

	WIN32_FIND_DATA search_data;

	memset(&search_data, 0, sizeof(WIN32_FIND_DATA));
	CString path = tmpPath+"\\*.*";
	HANDLE handle = FindFirstFile(path, &search_data);

	bool needtoreload = false;
	int folderempty = 0;
	int foldercount = 0;
	CString alertid;
	vector<CString*> alertsInFolder; // = new vector<CString*>();
	while(handle != INVALID_HANDLE_VALUE)
	{

		CString fileName = search_data.cFileName;

		// ignore screensaver files and directories
		if (fileName.Find(TEXT("screensaver_")) != -1 || fileName.Find(TEXT("lockscreen_")) != -1
			|| fileName == "." || fileName == "..")
		{
			if(FindNextFile(handle, &search_data) == FALSE)
				break;
			continue;
		}
		//
		int k=0;
		CString tmpFileName = fileName;

		string stdfileName = string(CT2CA(fileName));
		if (strstr(stdfileName.c_str(),"wallpaper")!= NULL)
		{
			foldercount++;
		}
		else
		{
			CString fullFileName = tmpPath+CString("\\")+fileName;
			DeleteFile(fullFileName);
		}

		alertid = fileName;
		alertid.Replace(_T("wallpaper_"),0);
		bool isid = false;
		while(alertid.Find('_')!= -1)
		{
			isid = true;
			alertid.Delete(alertid.GetLength()-1,alertid.GetLength()-1);
		}
		if (isid)
		{
			alertsInFolder.push_back(&alertid);
		}


		while(fileName.Find('_')!= -1)
		{
			folderempty++;
			k++;
			fileName.Delete(0,fileName.Find('_')+1);
		}
		if (k>0)
		{
			fileName.Delete(fileName.Find('.'),4);
			CString sum;
			std::ifstream file;
			file.open(tmpPath+ CString("\\")+tmpFileName,std::ifstream::binary);
			sum.Format(_T("%d"), checksum(file));
			file.close();
			if (sum != fileName)
			{
				DeleteFile(tmpPath+ CString("\\")+tmpFileName);
				if(db)
				{
					sqlite_int64 alid = 0;
					string ss = string(CT2CA(alertid));
					alid = atoi(ss.c_str());
					db->DeleteAlert(alid);
				}
				needtoreload = true;
			}

		}
		if(FindNextFile(handle, &search_data) == FALSE)
			break;
	}

	CString serverUrl = !isBackup ? _iAlertModel->getPropertyString(CString(_T("server"))) : _iAlertModel->getPropertyString(CString(_T("backup_server")));
	CString wallpaperUrl = !isBackup ? thData->wallpapersUrl : thData->backupWallpapersUrl;

	SYSTEMTIME lt;           
	GetLocalTime(&lt);
	LONG TimeZoneMinutes = 0;
	TIME_ZONE_INFORMATION TimeZoneInformation;
	if (TIME_ZONE_ID_INVALID != GetTimeZoneInformation(
		&TimeZoneInformation))
	{
		TimeZoneMinutes = -TimeZoneInformation.Bias;
	}
	LONG TimeZoneSec = TimeZoneMinutes * 60;
	CString furl2 = "";
	furl2.Format(_T("%s&localtime=%02d.%02d.%d|%02d:%02d:%02d&timezone=%d"), wallpaperUrl, lt.wDay,lt.wMonth,lt.wYear,lt.wHour,lt.wMinute,lt.wSecond, TimeZoneSec);

	CString resultXML;
	/*const HRESULT hres = */CAlertUtils::URLDownloadToFile(furl2,NULL,&resultXML,nullptr,NULL);
	NodeSYNCHRONIZEDALERTS alerts;
	XMLParser parser((XmlNode *)&alerts, _T("WALLPAPERS"));
	parser.Parse(resultXML, false);
	vector<CString*>::iterator alertsinfolderiterator = alertsInFolder.begin();
	vector<NodeALERT*>::iterator alertsIterator = alerts.m_alerts->begin();
	int gettedalertscount = alerts.m_alerts->size();
	if ((folderempty == 0)||(foldercount<gettedalertscount))
	{
		while(alertsIterator != alerts.m_alerts->end())
		{
			bool found = false;
			NodeALERT* curNode = *alertsIterator;
			CString idtodelete;
			idtodelete = curNode->m_alertid;
			alertsinfolderiterator = alertsInFolder.begin();
			while(alertsinfolderiterator != alertsInFolder.end())
			{
				CString* folderNode = *alertsinfolderiterator;

				if (curNode->m_alertid == folderNode->GetString())
				{
					found = true;
					break;
				}
				alertsinfolderiterator++;

			}
			if (!found)
			{
				if(db)
				{
					sqlite_int64 alid = 0;
					string ss = string(CT2CA(idtodelete));
					alid = atoi(ss.c_str());
					db->DeleteAlertByID(alid);
				}
			}
			alertsIterator++;

		}
		needtoreload = true;
	}


	//Close the handle after use or memory/resource leak
	FindClose(handle);

	if ((_iAlertModel->getPropertyString(hashPropName, &CString()) != hash) || (needtoreload))
	{
		if(db)
		{
			db->synchronizeAlerts(alerts.m_alerts,_T("8"));
		}

		CString localTime = "";
		localTime.Format(_T("%02d.%02d.%d|%02d:%02d:%02d"), lt.wDay,lt.wMonth,lt.wYear,lt.wHour,lt.wMinute,lt.wSecond);												
		JAReq_Received receivedReg( authorization, localTime);

		alertsIterator = alerts.m_alerts->begin();
		while(alertsIterator != alerts.m_alerts->end())
		{
			NodeALERT* curNode = *alertsIterator;
			curNode->m_isUTF8 = FALSE;
			CString resultHTML;
			CString alertHref;
			alertHref.Format(_T("%s&domain=%s&fulldomain=%s&compdomain=%s"), curNode->m_href, thData->domain_name, thData->authorization == UserName ? "" : thData->domain_name, thData->domain_name);
			
			if(alertHref.Find(_T("://")) == -1)
			{
				alertHref = serverUrl + _T("/") + alertHref;
			}

			HRESULT hres = CAlertUtils::URLDownloadToFile(alertHref,NULL,&resultHTML,&curNode->m_isUTF8,
				_iAlertModel->getPropertyString(CString(_T("decryption"))) == CString(_T("1"))?&_iAlertModel->getPropertyString(CString(_T("decryption_key")), &CString(_T("nj6kjvk58s8wb2d73"))):NULL);

			CString imgUrl = resultHTML;

			IStream *pSomeImg = NULL; // source image format is not important
			IStream *pMyJpeg = NULL;
			CreateStreamOnHGlobal(NULL, TRUE, &pMyJpeg);

			CString szPath;
			CString imgPath;
			srand(GetTickCount64());
			imgPath.Format(_T("wallpaper_%s.jpg"),curNode->m_alertid);

			szPath = tmpPath + _T("\\") + imgPath;
			if(imgUrl.Find(_T("://")) == -1)
				imgUrl = serverUrl + _T("/") + imgUrl;

			DeleteFile(szPath);
			hres = CAlertUtils::URLDownloadToFile(imgUrl,szPath,NULL,FALSE,NULL);
			CString newPath;

			if (SHCreateStreamOnFile(szPath,STGM_READ,&pSomeImg) == S_OK)
			{
				int quality = 75;
				CString mimeType= "image/jpeg";
				if (!IsWindowsVistaOrGreater())
				{
					mimeType = "image/bmp";
				}

				if (Gdiplus::Ok == CAlertUtils::imageToImage(pSomeImg, pMyJpeg, mimeType, quality))
				{
					pSomeImg->Release();
					const HANDLE imgFile = CreateFile(szPath.GetString(), GENERIC_WRITE, NULL, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
					STATSTG stat;
					pMyJpeg->Stat(&stat, 0);
					HGLOBAL hGlobal = NULL;
					GetHGlobalFromStream(pMyJpeg, &hGlobal);
					LPVOID pData = GlobalLock(hGlobal);
					DWORD writtenBytes;
					WriteFile(imgFile,pData,stat.cbSize.LowPart,&writtenBytes,NULL);
					std::ifstream file;
					GlobalUnlock(hGlobal);
					pMyJpeg->Release();
					CloseHandle(imgFile);
					file.open(szPath.GetString(),std::ifstream::binary);
					int crc = checksum(file);
					imgPath.Delete(imgPath.Find('.'),4);
					imgPath.Format(imgPath+"_%d.jpg",crc);
					file.close();
					
					newPath = tmpPath + _T("\\") + imgPath;
					DeleteFile(newPath);
					CopyFile(szPath, newPath, FALSE);
				}
				else
				{
					pSomeImg->Release();
				}
			}

			DeleteFile(szPath);
			curNode->m_html = imgPath;
			//If file exist on user's machine our job is done.
			struct stat buffer;
			
			if (stat (CT2A((LPCTSTR)newPath), &buffer) == 0)
			{
				receivedReg.AddWallpaperId( atol(CT2A(curNode->m_alertid)), 1);
			}
			else
			{
				logSystemError(L"Download wallpapers data", GetLastError());
			}

			alertsIterator++;
		}

		_iAlertModel->setPropertyString(hashPropName, hash);

		if( receivedReg.getWallpapersList().capacity() > 0 )
		{
			furl2.Format( _T("%s/%s"), !isBackup ? thData->server : thData->backup_server, receivedReg.getURL() );
			std::string  confirmReq = receivedReg.toJSON();
			CString csReq = CString(confirmReq.c_str());
			CString headers = _T("Authorization: ") + authorization+ _T("\nContent-Type: application/json; charset=utf-8 ");
			CString alertsFilePath = CAlertUtils::getTempFileName(_T("serverxml"),0);
			HRESULT hres = CAlertUtils::URLDownloadToFile( furl2, /*alertsFilePath*/NULL, 0, 0, 0, &headers, &csReq );
		}

		::PostMessage(thData->hwnd, UWM_NOTIFY_WALLPAPERS,(WPARAM)(alerts.m_alerts),NULL);
	}
}


////////////////////////////////////


void CAlertLoader::synchronizeLockscreens(ThreadData* thData, CString hash, bool isBackup, CString authorization)
{
	CString hashPropName = _T("lockscreensHash");
	//as state hash summ changed as Wallpapers syncronize need for
	if (_iAlertModel->getPropertyString(hashPropName, &CString()) == hash)
		return;

	CDatabaseManager *db = CDatabaseManager::shared(true);

	CString tmpPath = CAlertUtils::getLockscreensPath();

	WIN32_FIND_DATA search_data;

	memset(&search_data, 0, sizeof(WIN32_FIND_DATA));
	CString path = tmpPath + "\\*.*";
	HANDLE handle = FindFirstFile(path, &search_data);

	bool needtoreload = false;
	int folderempty = 0;
	int foldercount = 0;
	CString alertid;
	vector<CString*> alertsInFolder; // = new vector<CString*>();
	while (handle != INVALID_HANDLE_VALUE)
	{

		CString fileName = search_data.cFileName;

		// ignore screensaver files and directories
		if (fileName.Find(TEXT("screensaver_")) != -1 || fileName.Find(TEXT("wallpapers_")) != -1
			|| fileName == "." || fileName == "..")
		{
			if (FindNextFile(handle, &search_data) == FALSE)
				break;
			continue;
		}
		//
		int k = 0;
		CString tmpFileName = fileName;

		string stdfileName = string(CT2CA(fileName));
		if (strstr(stdfileName.c_str(), "lockscreen") != NULL)
		{
			foldercount++;
		}
		else
		{
			CString fullFileName = tmpPath + CString("\\") + fileName;
			DeleteFile(fullFileName);
		}

		alertid = fileName;
		alertid.Replace(_T("lockscreen_"), 0);
		bool isid = false;
		while (alertid.Find('_') != -1)
		{
			isid = true;
			alertid.Delete(alertid.GetLength() - 1, alertid.GetLength() - 1);
		}
		if (isid)
		{
			alertsInFolder.push_back(&alertid);
		}


		while (fileName.Find('_') != -1)
		{
			folderempty++;
			k++;
			fileName.Delete(0, fileName.Find('_') + 1);
		}
		if (k > 0)
		{
			fileName.Delete(fileName.Find('.'), 4);
			CString sum;
			std::ifstream file;
			file.open(tmpPath + CString("\\") + tmpFileName, std::ifstream::binary);
			sum.Format(_T("%d"), checksum(file));
			file.close();
			if (sum != fileName)
			{
				DeleteFile(tmpPath + CString("\\") + tmpFileName);
				if (db)
				{
					sqlite_int64 alid = 0;
					string ss = string(CT2CA(alertid));
					alid = atoi(ss.c_str());
					db->DeleteAlert(alid);
				}
				needtoreload = true;
			}

		}
		if (FindNextFile(handle, &search_data) == FALSE)
			break;
	}

	CString serverUrl = !isBackup ? _iAlertModel->getPropertyString(CString(_T("server"))) : _iAlertModel->getPropertyString(CString(_T("backup_server")));
	CString lockscreenUrl = !isBackup ? thData->lockscreensUrl : thData->backupLockscreensUrl;

	SYSTEMTIME lt;
	GetLocalTime(&lt);
	CString furl2 = "";
	furl2.Format(_T("%s&localtime=%02d.%02d.%d|%02d:%02d:%02d"), lockscreenUrl, lt.wDay, lt.wMonth, lt.wYear, lt.wHour, lt.wMinute, lt.wSecond);

	CString resultXML;
	/*const HRESULT hres = */CAlertUtils::URLDownloadToFile(furl2, NULL, &resultXML, nullptr, NULL);
	NodeSYNCHRONIZEDALERTS alerts;
	XMLParser parser((XmlNode *)&alerts, _T("LOCKSCREENS"));
	parser.Parse(resultXML, false);
	vector<CString*>::iterator alertsinfolderiterator = alertsInFolder.begin();
	vector<NodeALERT*>::iterator alertsIterator = alerts.m_alerts->begin();
	int gettedalertscount = alerts.m_alerts->size();
	if ((folderempty == 0) || (foldercount < gettedalertscount))
	{
		while (alertsIterator != alerts.m_alerts->end())
		{
			bool found = false;
			NodeALERT* curNode = *alertsIterator;
			CString idtodelete;
			idtodelete = curNode->m_alertid;
			alertsinfolderiterator = alertsInFolder.begin();
			while (alertsinfolderiterator != alertsInFolder.end())
			{
				CString* folderNode = *alertsinfolderiterator;

				if (curNode->m_alertid == folderNode->GetString())
				{
					found = true;
					break;
				}
				alertsinfolderiterator++;

			}
			if (!found)
			{
				if (db)
				{
					sqlite_int64 alid = 0;
					string ss = string(CT2CA(idtodelete));
					alid = atoi(ss.c_str());
					db->DeleteAlertByID(alid);
				}
			}
			alertsIterator++;

		}
		needtoreload = true;
	}


	//Close the handle after use or memory/resource leak
	FindClose(handle);

	if ((_iAlertModel->getPropertyString(hashPropName, &CString()) != hash) || (needtoreload))
	{
		if (db)
		{
			db->synchronizeAlerts(alerts.m_alerts, _T("4096"));
		}

		CString localTime = "";
		localTime.Format(_T("%02d.%02d.%d|%02d:%02d:%02d"), lt.wDay, lt.wMonth, lt.wYear, lt.wHour, lt.wMinute, lt.wSecond);
		JAReq_Received receivedReg(authorization, localTime);

		alertsIterator = alerts.m_alerts->begin();
		while (alertsIterator != alerts.m_alerts->end())
		{
			NodeALERT* curNode = *alertsIterator;
			curNode->m_isUTF8 = FALSE;
			CString resultHTML;
			CString alertHref;
			alertHref.Format(_T("%s&domain=%s&fulldomain=%s&compdomain=%s"), curNode->m_href, thData->domain_name, thData->authorization == UserName ? "" : thData->domain_name, thData->domain_name);

			if (alertHref.Find(_T("://")) == -1)
			{
				alertHref = serverUrl + _T("/") + alertHref;
			}

			HRESULT hres = CAlertUtils::URLDownloadToFile(alertHref, NULL, &resultHTML, &curNode->m_isUTF8,
				_iAlertModel->getPropertyString(CString(_T("decryption"))) == CString(_T("1")) ? &_iAlertModel->getPropertyString(CString(_T("decryption_key")), &CString(_T("nj6kjvk58s8wb2d73"))) : NULL);

			CString imgUrl = resultHTML;

			IStream *pSomeImg = NULL; // source image format is not important
			IStream *pMyJpeg = NULL;
			CreateStreamOnHGlobal(NULL, TRUE, &pMyJpeg);

			CString szPath;
			CString imgPath;
			srand(GetTickCount64());
			imgPath.Format(_T("lockscreen_%s.jpg"), curNode->m_alertid);

			szPath = tmpPath + _T("\\") + imgPath;
			if (imgUrl.Find(_T("://")) == -1)
				imgUrl = serverUrl + _T("/") + imgUrl;

			DeleteFile(szPath);
			hres = CAlertUtils::URLDownloadToFile(imgUrl, szPath, NULL, FALSE, NULL);
			CString newPath;

			if (SHCreateStreamOnFile(szPath, STGM_READ, &pSomeImg) == S_OK)
			{
				int quality = 75;
				CString mimeType = "image/jpeg";
				//if (!IsWindowsVistaOrGreater())
				//{
				//	mimeType = "image/bmp";
				//}

				if (Gdiplus::Ok == CAlertUtils::imageToImage(pSomeImg, pMyJpeg, mimeType, quality))
				{
					pSomeImg->Release();
					const HANDLE imgFile = CreateFile(szPath.GetString(), GENERIC_WRITE, NULL, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
					STATSTG stat;
					pMyJpeg->Stat(&stat, 0);
					HGLOBAL hGlobal = NULL;
					GetHGlobalFromStream(pMyJpeg, &hGlobal);
					LPVOID pData = GlobalLock(hGlobal);
					DWORD writtenBytes;
					WriteFile(imgFile, pData, stat.cbSize.LowPart, &writtenBytes, NULL);
					std::ifstream file;
					GlobalUnlock(hGlobal);
					pMyJpeg->Release();
					CloseHandle(imgFile);
					file.open(szPath.GetString(), std::ifstream::binary);
					int crc = checksum(file);
					imgPath.Delete(imgPath.Find('.'), 4);
					imgPath.Format(imgPath + "_%d.jpg", crc);
					file.close();

					newPath = tmpPath + _T("\\") + imgPath;
					DeleteFile(newPath);
					CopyFile(szPath, newPath, FALSE);
				}
				else
				{
					pSomeImg->Release();
				}
			}

			DeleteFile(szPath);
			curNode->m_html = imgPath;
			//If file exist on user's machine our job is done.
			struct stat buffer;

			if (stat(CT2A((LPCTSTR)newPath), &buffer) == 0)
			{
				receivedReg.AddLockscreenId(atol(CT2A(curNode->m_alertid)), 1);
			}
			else
			{
				logSystemError(L"Download lockscreens data", GetLastError());
			}

			alertsIterator++;
		}

		_iAlertModel->setPropertyString(hashPropName, hash);

		if (receivedReg.getLockscreensList().capacity() > 0)
		{
			furl2.Format(_T("%s/%s"), !isBackup ? thData->server : thData->backup_server, receivedReg.getURL());
			std::string  confirmReq = receivedReg.toJSON();
			CString csReq = CString(confirmReq.c_str());
			CString headers = _T("Authorization: ") + authorization + _T("\nContent-Type: application/json; charset=utf-8 ");
			CString alertsFilePath = CAlertUtils::getTempFileName(_T("serverxml"), 0);
			HRESULT hres = CAlertUtils::URLDownloadToFile(furl2, /*alertsFilePath*/NULL, 0, 0, 0, &headers, &csReq);
		}

		::PostMessage(thData->hwnd, UWM_NOTIFY_LOCKSCREENS, (WPARAM)(alerts.m_alerts), NULL);
	}
}




///////////////////////////////////

void CAlertLoader::synchronizeAlerts(ThreadData* thData/*, rd_user urd*/ )
{
	SYSTEMTIME lt;
	GetLocalTime(&lt);
	ResponseObj roAlerts;
	rd_alerts ard;
	CString localTime="";

	localTime.Format(_T("%02d.%02d.%d|%02d:%02d:%02d"), lt.wDay,lt.wMonth,lt.wYear,lt.wHour,lt.wMinute,lt.wSecond);												
	JAReq_Alerts alertsRequest( _iAlertController->getJAClient()->getAuthorization(), localTime );
	JAReq_getUserContents_v2 alertsRequest_v2(_iAlertController->getJAClient_v2()->getAuthorization(), localTime);

	CString alertsFilePath = CAlertUtils::getTempFileName(_T("serverxml"),0);
	BOOL isUTF8;
    BOOL itsOK;
    BOOL isToFile = false;
    BOOL isToString = true;
    string resString;
    //////////////////////////////////////////
    // request to GET /api/v1/user/content
    // is server is not support that API, then:
    // request to POST /api/v1/user/alerts
	BOOL isRecursionBlocked = FALSE;
	int httpRequestStatus = -1;

	/////////////////////////////////////////
	////// Using APIv2 example:
	//std::list<int> types{ 1, 2, 3, 4, 8 };
	//JAReq_getUserContents_v2 _getUserContents(_iAlertController->getJAClient_v2()->getAuthorization(), localTime);
	//JAReq_getUserHistory_v2 _getUserHistory(_iAlertController->getJAClient_v2()->getAuthorization(), 0, 1, "qwe", types, "create date");
	//JAReq_getUserTikers_v2 _getUserTikers(_iAlertController->getJAClient_v2()->getAuthorization());
	//
	//itsOK = _iAlertController->getJAClient_v2()->SendRequest((JARequest_v2*)&_getUserContents, isToFile, alertsFilePath, isToString, &resString, &isUTF8, isRecursionBlocked, &httpRequestStatus);
	//itsOK = _iAlertController->getJAClient_v2()->SendRequest((JARequest_v2*)&_getUserHistory, isToFile, alertsFilePath, isToString, &resString, &isUTF8, isRecursionBlocked, &httpRequestStatus);
	//itsOK = _iAlertController->getJAClient_v2()->SendRequest((JARequest_v2*)&_getUserTikers, isToFile, alertsFilePath, isToString, &resString, &isUTF8, isRecursionBlocked, &httpRequestStatus);

	if (((CAlertController*)_iAlertController)->IsEnableAPIv2())
		itsOK = _iAlertController->getJAClient_v2()->SendRequest((JARequest_v2*)&alertsRequest_v2, isToFile, alertsFilePath, isToString, &resString, &isUTF8, isRecursionBlocked, &httpRequestStatus);
	else
		itsOK = _iAlertController->getJAClient()->SendRequest((JARequest*)&alertsRequest, isToFile, alertsFilePath, isToString, &resString, &isUTF8, isRecursionBlocked, &httpRequestStatus);
	///////////////////////////////////////////
	/// PostMessage lifesignal to WatchDog
	if (httpRequestStatus != -1)
	{
		MSG Message;
		Message.message = UWM_LIFE_SIGNAL;
		Message.lParam = thData->expire;
		Message.wParam = NULL;
		CAlertUtils::PostMessageToWatchDog(Message);
	}

    if (!itsOK) 
    {
		////////////////////////////
		/// 304 = not modified
		if (httpRequestStatus == 304)
		{
			return;
		}
		///////////////////////////
		//// no access to APIv2, switch to APIv1
		if (((CAlertController*)_iAlertController)->IsEnableAPIv2())
		{
			((CAlertController*)_iAlertController)->SwitchAPI();
			return;
		}
		///////////////////////////
		//// no access to main server, switch to backup server to APIv2
		if (httpRequestStatus != 200)
		{
			_iAlertController->getJAClient()->SwtichServer();
			if (!((CAlertController*)_iAlertController)->IsEnableAPIv2())
				((CAlertController*)_iAlertController)->SwitchAPI();
			return; 
		}
    }
	Document doc;
	const char * chBody;
	//Распарсить из файла
	CT2A szFP(alertsFilePath);

	string allAlertsData = isToFile ? CAlertUtils::readFile2String(szFP) : resString;
	doc.Parse(allAlertsData.c_str());

    CDatabaseManager* dataBase = CDatabaseManager::shared(false);
    vector<CDatabaseManager::ReceivedAlert> vReceivedAlerts = dataBase->getVectorReceivedNotShowedAlerts();
    
	if( doc.HasMember("data") )
    {
		rapidjson::Value&  docData = doc["data"];
		if( docData.HasMember("alerts") )
		{
			const Value& alertsFromServer = docData["alerts"];
			assert(alertsFromServer.IsArray());

			GetLocalTime(&lt);
			localTime.Format(_T("%02d.%02d.%d|%02d:%02d:%02d"), lt.wDay,lt.wMonth,lt.wYear,lt.wHour,lt.wMinute,lt.wSecond);												
			JAReq_Received receivedReq( alertsRequest.getAutorization(), localTime);

            for (SizeType i = 0; i < alertsFromServer.Size(); i++) 
            {
                int id = alertsFromServer[i]["id"].GetInt();
                CString body = alertsFromServer[i]["body"].GetString();

                receivedReq.AddAlertId(id, 1);

                if (dataBase->isAlertReceived(id))
                {
                    continue;
                }
                dataBase->setReceived(id, body);
                vReceivedAlerts.push_back(CDatabaseManager::ReceivedAlert(id, body));
            }

			if( receivedReq.getAlertsList().capacity() > 0 )
			{
				_iAlertController->getJAClient()->SendRequest((JARequest *)&receivedReq, FALSE, NULL, FALSE, NULL, &isUTF8);
			}
		}

		if( docData.HasMember("terminatedAlerts") ) {
			const rapidjson::Value& alerts = docData["terminatedAlerts"];
			assert(alerts.IsArray());

			for (SizeType i = 0; i < alerts.Size(); i++) {
				long alertId = alerts[i].GetInt64();
				::PostMessage(thData->hwnd, UWM_NOTIFY_KILL_ALERT, alertId, NULL);

				JAReq_TerminateReceived terminatedReceivedReq(alertsRequest.getAutorization(), alertId);
				_iAlertController->getJAClient()->SendRequest((JARequest *)&terminatedReceivedReq, FALSE, NULL,FALSE, NULL, &isUTF8);
			}	
		}

		CString screensaverHash="";
		CString wallpaperHash="";

		if( docData.HasMember("screensaversHash") )
		{
			screensaverHash = docData["screensaversHash"].GetString();
			synchronizeScreensavers( thData, screensaverHash, _iAlertController->getJAClient()->getisBackupServer(), _iAlertController->getJAClient()->getAuthorization() );
		}
		if( docData.HasMember("wallpapersHash") )
		{
			wallpaperHash =  docData["wallpapersHash"].GetString();
			synchronizeWallpapers( thData, wallpaperHash, _iAlertController->getJAClient()->getisBackupServer(), _iAlertController->getJAClient()->getAuthorization() );
		}

 		if (docData.HasMember("lockscreensHash"))
		{
			CString lockscreenHash = docData["lockscreensHash"].GetString();
			synchronizeLockscreens(thData, lockscreenHash, _iAlertController->getJAClient()->getisBackupServer(), _iAlertController->getJAClient()->getAuthorization());
		}
	}
    
    for (size_t i = 0; i < vReceivedAlerts.size(); i++)
    {
        _int64 id = vReceivedAlerts[i].ID;
        CString body = vReceivedAlerts[i].body;
        _int64 historyID = 0;//crutch, костыль для правильного вызова isAlertInHistory
        if (!dataBase->isAlertInHistory(id, &historyID))
        {

            NodeALERT *curNode;
            CString szPath = CString(CAlertUtils::getTempFileName(_T("alert"), i));
            //////////////////////////////////////
            /// create and save body to (szPath)
            //HANDLE alertFile = CreateFile(szPath.GetString(), GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
            //if (INVALID_HANDLE_VALUE != alertFile)
            //{
            //    DWORD dwBytesWritten;
            //    CStringA data = CT2A(body); // do not convert encoding
            //    WriteFile(alertFile, data.GetBuffer(), data.GetLength(), &dwBytesWritten, 0);
            //    HRESULT hres = GetLastError();
            //    CloseHandle(alertFile);
            //    if (hres != S_OK)
            //    {
            //        appendToErrorFile(L"Error write Alert: %s ", *szPath);
            //        continue;
            //    }
            //}

            curNode = new NodeALERT();

            XMLParser parser((XmlNode *)curNode, _T("ALERT"));

            if (parser.Parse(body, false) == ERR_NONE)
            {
                curNode->Determine(&parser);
                curNode->m_isUTF8 = TRUE;
                CString resultHTML;

                CString *szPath = new CString(CAlertUtils::getTempFileName(_T("alert"), i));
                ///////////////////////////////////
                /// download from network
                if (!dataBase->isAlertDownloaded(id))
                {
                    HRESULT hres = CAlertUtils::URLDownloadToFile(curNode->m_href, NULL, &resultHTML, &curNode->m_isUTF8,
                        _iAlertModel->getPropertyString(CString(_T("decryption"))) == CString(_T("1")) ? &_iAlertModel->getPropertyString(CString(_T("decryption_key")), &CString(_T("nj6kjvk58s8wb2d73"))) : NULL);
                    dataBase->setDownloaded(id, resultHTML);
                }
                ///////////////////////////////////
                /// or download from db.dat
                else
                {
                    resultHTML = dataBase->getDownloaded(id);
                }

                curNode->m_html = CAlertLoader::bodyPrepare(resultHTML, curNode);
                DWORD dwBytesWritten;
                CStringA currentAlertData = CT2A(resultHTML, CP_UTF8);
                if (!curNode->m_isUTF8)currentAlertData = CT2A(resultHTML);

                const HANDLE alertFile = CreateFile(szPath->GetString(), GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
                HRESULT hres = GetLastError();
                if (INVALID_HANDLE_VALUE == alertFile)
                {
                    logSystemError(L"Loading URL to local file", hres);
                    delete szPath;
                    continue;
                }
                WriteFile(alertFile, currentAlertData, currentAlertData.GetLength(), &dwBytesWritten, 0);
                CloseHandle(alertFile);

                PostMessageDelBoth<NodeALERT, CString>(thData->hwnd, UWM_NOTIFY_XML_UPDATE, curNode, szPath);
                Sleep(10);
            }
        }
    }

	::PostMessage(thData->hwnd, UWM_NOTIFY_ESTABLISHED_CONNECTION, NULL, NULL);
	const HANDLE pHandle = GetCurrentProcess();
	SetProcessWorkingSetSize(pHandle, (SIZE_T)-1, (SIZE_T)-1);
	CloseHandle(pHandle);
}

bool compareTime(int expire, bool needUpdate)
{
	CDatabaseManager *db = CDatabaseManager::shared(false);
	bool hasProperty = false;
	CString lastSync("");
	bool difference = false;
	if(db)
	{
		hasProperty = db->hasProperty(CString("lastSyncGroups"));

		if (hasProperty)
		{
			lastSync = db->getProperty(CString("lastSyncGroups"));
			int last = _wtoi(lastSync);
			time_t t = time ( 0 );   // get time now
			long int secondsNow = static_cast<long int>(t);
			if (secondsNow - last > expire) 
			{
				difference = true;
				if (needUpdate)
				{
					CString sendsToInsert;
					sendsToInsert.Format( _T( "%d"), secondsNow );
					db->setProperty(CString("lastSyncGroups"),sendsToInsert);
				}
			}
		}
		else
		{
			difference = true;
			if (needUpdate)
			{
				time_t t = time ( 0 );   // get time now
				long int secondsNow = static_cast<long int>(t);
				CString sendsToInsert;
				sendsToInsert.Format( _T( "%d"), secondsNow );
				db->setProperty(CString("lastSyncGroups"),sendsToInsert);
			}
		}

	}

	return difference;
}

DWORD WINAPI CAlertLoader::SynchronizationFunc(LPVOID lpParam)
{
	DWORD res = 1;
	//SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_BELOW_NORMAL);
	release_try
	{
		srand(GetTickCount64());

		/*const HRESULT hRes = */::CoInitialize(NULL);
		ThreadData* thData = reinterpret_cast<ThreadData*>(lpParam);

		//appendToErrorFile(L"started SynchronizationFunc url=%s expire=%d", thData->url, thData->expire);

		BOOL dowloading = FALSE;

		UINT mTimerTime = thData->expire*1000;
		UINT_PTR mTimer = ::SetTimer(NULL, NULL, mTimerTime, NULL);
		::PostMessage(NULL, WM_TIMER, NULL, NULL);
		release_try
		{
			MSG msg;
			// Main message loop:
			while(GetMessage(&msg, NULL, 0, 0) > 0) //important to check > 0, see MSDN
			{
				release_try
				{
					if(msg.message == WM_TIMER)
					{
						if(_iAlertModel)
						{
							if(!FuckYeahChecker())
							{
								if(mTimerTime != thData->expire*1000)
								{
									::KillTimer(NULL, mTimer);
									mTimerTime = thData->expire*1000;
									mTimer = ::SetTimer(NULL,NULL,mTimerTime,NULL);
								}
							}
							else
							{
								if (!dowloading)
								{
									dowloading = TRUE;
									release_try
									{
										CString alertsFilePath = CAlertUtils::getTempFileName(_T("serverxml"),0);


										bool needSync = compareTime(thData->expire, false);

										if (needSync)
										{
											IADsUser *pObject;
											HRESULT hr = NULL;
											CoInitialize(NULL);
											vector<wstring> vector_groups;
											//const int encode = data.m_encoding ? data.m_encoding : _iAlertModel->getProperty(CString(_T("encoding")));
											CString fulldomain = _iAlertModel->getPropertyString(CString(_T("fulldomain")), &CString(), 0);
											TCHAR name [ UNLEN + 1 ];
											DWORD size = UNLEN + 1;
											GetUserName((TCHAR*)name, &size);
											hr = ADsGetObject(_T("WinNT://")+fulldomain+_T("/")+name+_T(",user"), IID_IADsUser, (void**) &pObject);//LDAP://CN=alik,DC=softomate,DC=net
											//appendToErrorFile(_T("WinNT://")+fulldomain+_T("/")+name+_T(",user \n"));
											if(SUCCEEDED(hr))
											{
												vector_groups = CheckUserGroups(pObject);
											}
											CoUninitialize();
											CString furl;
											furl.Format(_T("%s?domain=%s&uname=%s"), thData->url, fulldomain, name);

											vector<wstring> add;
											vector<wstring> remove;

											CDatabaseManager *db = CDatabaseManager::shared(false);
											bool difference = false;
											if(db)
											{
												difference = db->synchronizeGroups(vector_groups, add, remove, false);
											}


											if (difference)
											{
												int cn = 0;
												CString tmp;
												if (!add.empty())
												{
													tmp = furl;
													furl.Format(_T("%s&groups_to_add="), tmp);
													while (!add.empty())
													{
														if (cn != 0)
														{
															tmp = furl;
															furl.Format(_T("%s%s"), tmp, _T(","));
														}
														tmp = furl;
														furl.Format(_T("%s%s"), tmp, CString((*add.begin()).c_str()));
														++cn;
														add.erase(add.begin());
													}
												}

												cn = 0;
												if (!remove.empty())
												{
													tmp = furl;
													furl.Format(_T("%s&groups_to_remove="), tmp);
													while (!remove.empty())
													{
														if (cn != 0)
														{
															tmp = furl;
															furl.Format(_T("%s%s"), tmp, _T(","));
														}
														tmp = furl;
														furl.Format(_T("%s%s"), tmp, CString((*remove.begin()).c_str()));
														++cn;
														remove.erase(remove.begin());
													}
												}

												//ÝÒÀ ÔÓÍÊÖÈß ÄÅËÀÅÒ ÇÀÏÐÎÑ ÍÀ ÑÅÐÂÅÐ
												const HRESULT hDwnldres = CAlertUtils::URLDownloadToFile(furl,alertsFilePath);
												if (hDwnldres==S_OK)
												{
													compareTime(-1, true);
													db->synchronizeGroups(vector_groups, add, remove, true);
												}
												else
												{
													::PostMessage(thData->hwnd, UWM_NOTIFY_OFFLINE, NULL, NULL);
												}
											}
										}
									}
									release_catch_expr_and_tolog(res = 3, L"Loading file exception");
									dowloading = FALSE;
								}
								::KillTimer(NULL, mTimer);
								mTimerTime = thData->expire*1000;
								mTimer = ::SetTimer(NULL,NULL,mTimerTime,NULL);
							}
						}
					}
					else if(msg.message == UWM_STOP_THREAD)
					{
						::KillTimer(NULL, mTimer);
						SetEvent((HANDLE)msg.lParam);
						break;
					}
					else if(msg.message == UWM_RELOAD_DATA)
					{
					}
					else
					{
						TranslateMessage(&msg);
						DispatchMessage(&msg);
					}
				}
				release_catch_expr_and_tolog(res = 3, _T("AlertLoader loop exception"))
			}
		}
		release_catch_expr_and_tolog(res = 3, _T("AlertLoader get message exception"))

			::CoUninitialize();
		delete_catch(thData);
	}
	release_catch_expr_and_tolog(res = 3, _T("AlertLoader threat exception"))

		return res;
}



sqlite_int64 CAlertLoader::addToHistory(shared_ptr<NodeALERT> curNode, CString &window, BOOL &isCreated)
{
	sqlite_int64 history_id = -1;

	release_try
	{
		if(curNode && curNode->m_history != _T("0") && curNode->m_history.CollateNoCase(_T("false")))
		{
			CHistoryActionEvent act(
				CString(_T("history")),
				CString(_T("loader")),
				CString(data.m_history),
				curNode,
				window,
				&history_id, &isCreated);
			_iAlertController->actionPerformed(act);
			//_iAlertModel->saveAlert(xmlPath);
		}
	}
	release_catch_tolog(L"Failed to save alert into the database: %s", window);
	return history_id;
}


void PlaySoundIncomingAlert()
{
		CString isPlay = _iAlertModel->getPropertyString(CString(_T("play_sound")));

		CoUninitialize();
		if (isPlay==_T("1"))
		{
			CString sfile =_iAlertModel->getPropertyString(CString(_T("sound_file")));
			if (sfile!=_T(""))
			{
				PlaySound(sfile, NULL, SND_FILENAME|SND_ASYNC );
			}
		}
}


void CAlertLoader::alertListUpdated(CString xmlPath, shared_ptr<NodeALERT> curNode)
{
	DeskAlertsAssert(_iAlertModel);
	DeskAlertsAssert(curNode);
	if (_iAlertModel && curNode)
	{
		//////////////////
		// Get window name
		CString window = data.m_window;
		CString unobtrusiveState =  _iAlertModel->getPropertyString(CString(_T("disableState")));

		curNode->setIsUnread( (unobtrusiveState == _T("on") || curNode->m_class == "16") && curNode->m_urgent == _T("0") );

		if(data.m_windows.size() > 0)
		{
			vector<NodeALERTS::NodeCOMMANDS::NodeALERTCOMMAND::NodeALERTWINDOW*>::iterator it = data.m_windows.begin();
			while(it != data.m_windows.end())
			{


				//TODO: call condition
				CString cdata = (*it)->m_cdata;
				cdata.Remove(_T(' '));
				cdata.Remove(_T('\''));
				cdata.Remove(_T('\"'));
				if(cdata == _T("#ticker#==1") && curNode->m_ticker == _T("1")||
					cdata == _T("#ticker#==0") && curNode->m_ticker == _T("0")||
					cdata == _T("#ticker_position#==middle") && curNode->m_ticker_position == _T("middle")||
					cdata == _T("#ticker_position#==top") && curNode->m_ticker_position == _T("top"))
				{
					if(!(*it)->m_window.IsEmpty())
						window = (*it)->m_window;
				}
				++it;
			}
		}

		//////////////////
		// Add to history
        BOOL newCreated = FALSE;
		sqlite_int64 history_id = addToHistory(curNode, window, newCreated);

		if (newCreated) 
		{
            CDatabaseManager *databaseManager = CDatabaseManager::shared(true);
            DeskAlertsAssert(databaseManager);
            const INT64 alert_id = CAlertUtils::str2int64(curNode->m_alertid);
            databaseManager->cleanDownloaded(alert_id);
			PlaySoundIncomingAlert();
		}
		//	CString countStr;
		//	INT64 count = CDatabaseManager::shared(true)->getUnreadCount();
		//	countStr.Format(_T("You have new message(s)" ),count);

		//	HRESULT hr = CoInitialize(NULL);

		//	hr;
		//	Notification::INotificationPtr notifPtr(__uuidof(Notification::DeskAlertsNotification));

		//	notifier->CreateAndShowNotification(CComBSTR(countStr));

		if (curNode->m_class != "16")
		{
			try {
				shared_ptr<showData> ex(new showData());
				//////////////////
				// Add acknowledgement button
				ex->url = xmlPath;
				ex->acknowledgement = (curNode->m_acknowledgement == _T("1") && !curNode->m_userid.IsEmpty() ? _T("1") : _T("0"));
				ex->create_date = curNode->m_createDate;
				ex->title = curNode->m_title;
				ex->UTC = curNode->m_UTC;
				ex->width = curNode->m_iWidth;
				ex->height = curNode->m_iHeight;
				ex->position = curNode->m_position;
				ex->docked = curNode->m_docked;
				ex->autoclose = curNode->m_autoclose;
				ex->topTemplateHTML=curNode->m_topTemplateHTML;
				ex->bottomTemplateHTML=curNode->m_bottomTemplateHTML;
				ex->history_id = history_id;
				ex->visible = curNode->m_visible;
				ex->cdata = curNode->m_inner; //set inner XML
				ex->ticker = curNode->m_ticker;
				ex->userid = curNode->m_userid;
				ex->alertid = curNode->m_alertid;
				ex->survey = curNode->m_survey;
				ex->schedule = curNode->m_schedule;
				ex->recurrence = curNode->m_recurrence;
				ex->urgent = curNode->m_urgent;
				ex->utf8 = curNode->m_isUTF8;
				ex->resizable = curNode->m_resizable;
				ex->self_deletable = curNode->m_self_deletable;
				ex->to_date = curNode->m_to_date;
				ex->hide_close = curNode->m_hide_close;

				appendCustomHtmlToAlertFile(ex);

				//////////////////
				// ... and show
				DeskAlertsAssert( !window.IsEmpty() );
		        if (!window.IsEmpty() && (!curNode->getIsUnread()))
	            {
			        DeskAlertsAssert(_iAlertView);
				    _iAlertView->showViewByName(window, xmlPath, ex, TRUE);
				}
			}
			catch (...)
			{
				DeskAlertsUnexpected();
			}

		}
        DeskAlertsAssert(_iAlertController);
		_iAlertController->refreshState();
	}
}

void CAlertLoader::createTimerThread(IActionEvent& event)
{
	if(_iAlertModel)
	{
		//if(!_iAlertModel->getPropertyString(CString(_T("user_name")), &CString()).IsEmpty())
		//{
			const int encode = data.m_encoding ? data.m_encoding : _iAlertModel->getProperty(CString(_T("encoding")));
			ThreadData* thData = new ThreadData();
			thData->hwnd = m_hWnd;

			//
			thData->backup_server = _iAlertModel->getPropertyString(CString("backup_server"));
			thData->server = _iAlertModel->getPropertyString(CString("server"));			
			thData->client_version = _iAlertModel->getPropertyString(CString("version"));			
			thData->user_name = _iAlertModel->getPropertyString(CString("user_name"), &CString());			
			thData->deskbar_id = _iAlertModel->getPropertyString(CString("deskalerts_id"));			
			thData->ip = _iAlertModel->getPropertyString(CString("ip"));			
			//thData->domain_name = _iAlertModel->getPropertyString(CString("fulldomain"));
			
			BOOL adEnabled = _iAlertModel->getPropertyString(CString(_T("synFreeAdEnabled"))) == "1" ||
				_iAlertModel->getPropertyString(CString(_T("adEnabled")), &CString()) == "1" ;

			thData->domain_name = _iAlertModel->getPropertyString(CString(_T("fulldomain")), &CString(), encode);
			if( thData->domain_name.IsEmpty() || thData->domain_name=="" )
				thData->domain_name = _iAlertModel->getPropertyString(CString(_T("compdomain")), &CString(), encode);
			if( (thData->domain_name.IsEmpty() || thData->domain_name == "") && adEnabled )
				thData->domain_name = _iAlertModel->getPropertyString(CString(_T("domain_name")), &CString(), encode);
			//if( thData->domain_name == "domain" || thData->domain_name == "fulldomain" )thData->domain_name = "";
			thData->computer_name = _iAlertModel->getPropertyString(CString("COMPUTERNAME"));	
			
			
			thData->authorization = !adEnabled || (thData->domain_name.IsEmpty() || thData->domain_name=="") ?UserName:Domain; 
			
			thData->md5_password = "pass";//_iAlertModel->getPropertyString(CString("domain"));			
			//

			thData->pixelUrl = _iAlertModel->getPropertyString(CString(_T("pixelUrl")), &CString(), _iAlertModel->getProperty(CString(_T("encoding"))), true);
			thData->url = _iAlertModel->buildPropertyString(data.m_filename, encode, true);			

			thData->backupUrl = _iAlertModel->getPropertyString(CString("backup_server"));

			if (event.getSourceName() == CString(_T("standby")))
			{
				thData->expire = CAlertUtils::str2sec(_iAlertModel->getPropertyString(CString(_T("standby_expire"))), 60);
			}
			else
			{
				thData->expire = CAlertUtils::str2sec(_iAlertModel->getPropertyString(CString(_T("normal_expire"))), 60);
			}
			if(thData->expire > 0)
			{
				CString tmp; tmp.Format(_T("%cnextrequest=%d"), (thData->url.Find(_T('?'))>=0 ? _T('&') : _T('?')), thData->expire);
				thData->url += tmp;

				//thData->url += _T("&domain=") + _iAlertModel->getPropertyString(CString(_T("domain")), &CString(), encode);
				thData->url += _T("&domain=") + thData->domain_name;
				thData->url += _T("&fulldomain=") + thData->domain_name;
				thData->url += _T("&compdomain=") + _iAlertModel->getPropertyString(CString(_T("compdomain")), &CString(), encode);
				thData->url += _T("&edir=") + _iAlertModel->getPropertyString(CString(_T("edir")), &CString(), encode);


				int stateIndex = _iAlertModel->getStatesIndex();
				if(stateIndex & _DISABLE_)
				{
					thData->url += _T("&disable=1");
				}
				if(stateIndex & _STANDBY_)
				{
					thData->url += _T("&standby=1");
				}

				if(!thData->backupUrl.IsEmpty())
				{
					thData->backupUrl = _iAlertModel->getPropertyString(CString("backup_base_url"));
					thData->backupUrl += tmp;

					thData->backupUrl += _T("&domain=") + _iAlertModel->getPropertyString(CString(_T("domain")), &CString(), encode);
					thData->backupUrl += _T("&fulldomain=") + _iAlertModel->getPropertyString(CString(_T("fulldomain")), &CString(), encode);
					thData->backupUrl += _T("&compdomain=") + _iAlertModel->getPropertyString(CString(_T("compdomain")), &CString(), encode);
					thData->backupUrl += _T("&edir=") + _iAlertModel->getPropertyString(CString(_T("edir")), &CString(), encode);


					int stateIndex = _iAlertModel->getStatesIndex();
					if(stateIndex & _DISABLE_)
					{
						thData->backupUrl += _T("&disable=1");
					}
					if(stateIndex & _STANDBY_)
					{
						thData->backupUrl += _T("&standby=1");
					}
				}			

				thData->screensaversUrl = _iAlertModel->getPropertyString(CString(_T("screensavers_url")), &CString());
				if(!thData->screensaversUrl.IsEmpty())
				{
					CString tmp; tmp.Format(_T("%cnextrequest=%d"), (thData->url.Find(_T('?'))>=0 ? _T('&') : _T('?')), thData->expire);
					thData->screensaversUrl += tmp;

					thData->screensaversUrl += _T("&domain=") + thData->domain_name;
					thData->screensaversUrl += _T("&fulldomain=") + (thData->authorization == UserName ? "" : thData->domain_name);
					thData->screensaversUrl += _T("&compdomain=") + _iAlertModel->getPropertyString(CString(_T("compdomain")), &CString(), encode);
					thData->screensaversUrl += _T("&edir=") + _iAlertModel->getPropertyString(CString(_T("edir")), &CString(), encode);


					if(stateIndex & _DISABLE_)
					{
						thData->screensaversUrl += _T("&disable=1");
					}
					if(stateIndex & _STANDBY_)
					{
						thData->screensaversUrl += _T("&standby=1");
					}
				}

				thData->backupScreensaversUrl = _iAlertModel->getPropertyString(CString(_T("backup_screensavers_url")), &CString());
				if(!thData->backupScreensaversUrl.IsEmpty())
				{
					CString tmp; tmp.Format(_T("%cnextrequest=%d"), (thData->backupUrl.Find(_T('?'))>=0 ? _T('&') : _T('?')), thData->expire);
					thData->backupScreensaversUrl += tmp;

					thData->backupScreensaversUrl += _T("&domain=") + thData->domain_name;
					thData->backupScreensaversUrl += _T("&fulldomain=") + (thData->authorization == UserName ? "" : thData->domain_name);
					thData->backupScreensaversUrl += _T("&compdomain=") + _iAlertModel->getPropertyString(CString(_T("compdomain")), &CString(), encode);
					thData->backupScreensaversUrl += _T("&edir=") + _iAlertModel->getPropertyString(CString(_T("edir")), &CString(), encode);

					if(stateIndex & _DISABLE_)
					{
						thData->backupScreensaversUrl += _T("&disable=1");
					}
					if(stateIndex & _STANDBY_)
					{
						thData->backupScreensaversUrl += _T("&standby=1");
					}
				}


				thData->wallpapersUrl = _iAlertModel->getPropertyString(CString(_T("wallpapers_url")));
				if(!thData->wallpapersUrl.IsEmpty())
				{
					CString tmp; tmp.Format(_T("%cnextrequest=%d"), (thData->url.Find(_T('?'))>=0 ? _T('&') : _T('?')), thData->expire);
					thData->wallpapersUrl += tmp;

					thData->wallpapersUrl += _T("&domain=") + thData->domain_name;
					thData->wallpapersUrl += _T("&fulldomain=") + (thData->authorization == UserName ? "" : thData->domain_name);
					thData->wallpapersUrl += _T("&compdomain=") + _iAlertModel->getPropertyString(CString(_T("compdomain")), &CString(), encode);
					thData->wallpapersUrl += _T("&edir=") + _iAlertModel->getPropertyString(CString(_T("edir")), &CString(), encode);

					if(stateIndex & _STANDBY_)
					{
						thData->wallpapersUrl += _T("&standby=1");
					}
				}

				thData->backupWallpapersUrl = _iAlertModel->getPropertyString(CString(_T("backup_wallpapers_url")));
				if(!thData->backupWallpapersUrl.IsEmpty())
				{
					CString tmp; tmp.Format(_T("%cnextrequest=%d"), (thData->backupUrl.Find(_T('?'))>=0 ? _T('&') : _T('?')), thData->expire);
					thData->backupWallpapersUrl += tmp;

					thData->backupWallpapersUrl += _T("&domain=") + thData->domain_name;
					thData->backupWallpapersUrl += _T("&fulldomain=") + (thData->authorization == UserName?"": thData->domain_name);
					thData->backupWallpapersUrl += _T("&compdomain=") + _iAlertModel->getPropertyString(CString(_T("compdomain")), &CString(), encode);
					thData->backupWallpapersUrl += _T("&edir=") + _iAlertModel->getPropertyString(CString(_T("edir")), &CString(), encode);



					if(stateIndex & _STANDBY_)
					{
						thData->backupWallpapersUrl += _T("&standby=1");
					}
				}


				thData->lockscreensUrl = _iAlertModel->getPropertyString(CString(_T("lockscreens_url")));
				thData->backupLockscreensUrl = _iAlertModel->getPropertyString(CString(_T("backup_lockscreens_url")));


				m_timerThread = BEGINTHREADEX(
					NULL,                         // default security attributes
					0,                            // use default stack size
					CAlertLoader::AlertLoaderFunc1,// thread function
					thData,						  // argument to thread function
					0,                            // use default creation flags
					&m_dwThreadId);                 // returns the thread identifier

				appendToErrorFile(L"Started BEGINTHREADEX with id %lu", m_dwThreadId);
				appendToErrorFile(L"Started BEGINTHREADEX with handle %p", m_timerThread);
			}
		//}
	}
}



void CAlertLoader::createTimerThreadForAdSync(IActionEvent& event)
{
	if(_iAlertModel)
	{
		if(!_iAlertModel->getPropertyString(CString(_T("user_name")), &CString()).IsEmpty() && _iAlertModel->getPropertyString(CString(_T("synFreeAdEnabled"))) == "1")
		{
			const int encode = data.m_encoding ? data.m_encoding : _iAlertModel->getProperty(CString(_T("encoding")));
			ThreadData* thData = new ThreadData();
			thData->hwnd = m_hWnd;
			thData->url = _iAlertModel->buildPropertyString(_iAlertModel->getPropertyString(CString(_T("syncGroups"))), encode, true);

			if (event.getSourceName() == CString(_T("standby")))
			{
				thData->expire = CAlertUtils::str2sec(_iAlertModel->getPropertyString(CString(_T("standby_sync_free_expire"))), 60);
			}
			else
			{
				thData->expire = CAlertUtils::str2sec(_iAlertModel->getPropertyString(CString(_T("normal_sync_free_expire"))), 60);
			}

			if(thData->expire > 0)
			{

				m_timerThreadAd = BEGINTHREADEX(
					NULL,                         // default security attributes
					0,                            // use default stack size
					CAlertLoader::SynchronizationFunc,// thread function
					thData,						  // argument to thread function
					0,                            // use default creation flags
					&m_dwThreadAdId);                 // returns the thread identifier


			}
		}
	}
}


void CAlertLoader::addAlertListener(IAlertListener* /*newListener*/)
{
}

void CAlertLoader::removeAlertListener(IAlertListener* /*newListener*/)
{
}

LPVOID CAlertLoader::getAlert(CString& /*propertyName*/)
{
	return NULL;
}

BOOL CAlertLoader::IsReadyToDownload(CString name)
{
	BOOL res=false;
	if(_iAlertModel)
	{
		CString valueName = _T("download_timestamp");
		if(name) valueName += _T("_") + name;

		time_t t;
		int last_download = CAlertUtils::str2int(CAlertUtils::getValue(valueName, _T("0")), 0);
		int expired_time = CAlertUtils::str2sec(_iAlertModel->getPropertyString(CString(_T("normal_expire"))), 60);
		if ((time(&t) - last_download)>= expired_time)
		{
			res = true;
			TCHAR tmp[65];
			_i64tot_s(t, tmp, 65, 10);
			CAlertUtils::setValue(valueName, tmp);
		}
	}
	return res;
}

void CAlertLoader::appendCustomHtmlToAlertFile(shared_ptr<showData> &ex)
{
	CString file = ex->url;
	file.Replace(_T("\\"), _T("\\\\"));

	CString sData;
	if(ex->acknowledgement == "1")
	{
		sData += CString("<!-- cannot close --><div></div>");
		//	sData += "<div name='readItDiv'  file='" + file + "' alertId='" + ex->alertid + "' userId='" + ex->userid + "' ></div>";
		sData += CString("<!-- readit = '") + file + CString("', '") + ex->alertid + CString("', '") + ex->userid + CString("' -->");

	}
	if(ex->cdata)
	{
		sData += CString("<!-- cdata = '") + CAlertUtils::escapeXML(ex->cdata) + CString("' -->");
		sData += CString("<div id='cdata' data='")+ CAlertUtils::escapeXML(ex->cdata) + CString("'></div>");

	}
	if(ex->to_date)
	{
		sData += CString("<!-- todate = '") + ex->to_date + CString("', '") + ex->alertid + CString("', '")  + ex->userid + CString("', '") + file + CString("' -->");

		sData += CString("<div id='todate' date='") + ex->to_date + CString("' alert_id='")+ ex->alertid + CString("' user_id='")+ ex->userid + CString("' file='")+ file + CString("' ></div>");
	}
	const int autoclose = CAlertUtils::str2int(ex->autoclose);
	if(autoclose != 0)
	{
		//important!
		CString autoclose_date_str;
		int autoclose = _ttoi(ex->autoclose);
		if(ex->date != ""){
			int create_date = _ttoi(ex->date);
			int autoclose_date = create_date + abs(autoclose);
			autoclose_date_str.Format(_T("%d"), autoclose_date);
		}
		else{
			int autoclose_date = time(0) + abs(autoclose);
			autoclose_date_str.Format(_T("%d"), autoclose_date);
		}

		sData += CString("<!-- autoclose = '") + ex->autoclose + CString("', '") + file + CString("', autoclose_date = '") + autoclose_date_str + CString("' -->");
		sData += CString("<div id='autoclose' value='")+ex->autoclose+ CString("' file='")+ file+ CString("'></div>");
		if(autoclose > 0)
		{
			sData += CString("<!-- cannot close -->");
		}
	}
	sData += CString("<!-- alert_id = '") + ex->alertid + CString("' -->");
	sData += CString("<!-- title = '") + ex->title + CString("' -->");
	sData += CString("<div id='title' value='")+ ex->title+ CString("' /><div>");
	CAlertUtils::appendToFile(ex->url, sData, ex->utf8);
}

int checksum(std::ifstream& file) 
{
	int sum = 0;

	int word = 0;
	while (file.read(reinterpret_cast<char*>(&word), sizeof(word))) {
		sum += word;
	}

	if (file.gcount()) {
		word &= (~0U >> ((sizeof(int) - file.gcount()) * 8));
		sum += word;
	}

	return sum;
}



vector<wstring> CheckUserGroups(IADsUser *pUser)
{
	vector<wstring> vector_groups;
	IADsMembers *pGroups;
	HRESULT hr = S_OK;
	hr = pUser->Groups(&pGroups);
	pUser->Release();
	if (FAILED(hr)) return vector_groups;

	IUnknown *pUnk;
	hr = pGroups->get__NewEnum(&pUnk);
	if (FAILED(hr)) return vector_groups;
	pGroups->Release();

	IEnumVARIANT *pEnum;
	hr = pUnk->QueryInterface(IID_IEnumVARIANT,(void**)&pEnum);
	if (FAILED(hr)) return vector_groups;

	pUnk->Release();

	// Enumerate.
	BSTR bstr;
	VARIANT var;
	IADs *pADs;
	ULONG lFetch;
	IDispatch *pDisp;
	VariantInit(&var);
	hr = pEnum->Next(1, &var, &lFetch);
	while(hr == S_OK)
	{
		if (lFetch == 1)
		{
			pDisp = V_DISPATCH(&var);
			pDisp->QueryInterface(IID_IADs, (void**)&pADs);
			pADs->get_Name(&bstr);
			wstring str(bstr,SysStringLen(bstr));
			vector_groups.push_back(str);
			SysFreeString(bstr);
			pADs->Release();
		}
		VariantClear(&var);
		pDisp=NULL;
		hr = pEnum->Next(1, &var, &lFetch);
	};
	hr = pEnum->Release();
	return vector_groups;
}

using namespace rapidjson;

// Load content new API handler 
DWORD WINAPI CAlertLoader::AlertLoaderFunc1( LPVOID lpParam )
{
	DWORD res = 1;
	//SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_BELOW_NORMAL);
	release_try
	{
		srand(GetTickCount64());

		/*const HRESULT hRes = */::CoInitialize(NULL);
		ThreadData* thData = reinterpret_cast<ThreadData*>(lpParam);

		//appendToErrorFile(L"started AlertLoaderFunc url=%s expire=%d", thData->url, thData->expire);

		BOOL dowloading = FALSE;

		UINT mTimerTime = thData->expire*1000;
		UINT_PTR mTimer = ::SetTimer(NULL, NULL, mTimerTime, NULL);
		::PostMessage(thData->hwnd, WM_TIMER, NULL, NULL);
		release_try
		{
			MSG msg;
			// Main message loop:
			while(GetMessage(&msg, NULL, 0, 0) > 0) //important to check > 0, see MSDN
			{
				release_try
				{
					if(msg.message == WM_TIMER)
					{
						if(_iAlertModel)
						{
							if(!FuckYeahChecker())
							{
								if(mTimerTime != thData->expire*1000)
								{
									::KillTimer(NULL, mTimer);
									mTimerTime = thData->expire*1000;
									mTimer = ::SetTimer(NULL,NULL,mTimerTime,NULL);
								}
							}
							else
							{
								if (!thData->pixelUrl.IsEmpty())
								{
									CString pixelFilePath =  CAlertUtils::getTempFileName(_T("pixel"),0);
									CAlertUtils::checkPixel(thData->pixelUrl, pixelFilePath);
								}
								if (!dowloading)
								{
									dowloading = TRUE;
									release_try
									{
										//CString alertsFilePath = getTempFileName(_T("serverxml"),0);
										{										
											bool isBackup = false;
											synchronizeAlerts( thData );

											CString backUrl;
											backUrl = thData->backupUrl;
										}
									}
									release_catch_expr_and_tolog(res = 3, L"Loading file exception");
									dowloading = FALSE;
								}
								::KillTimer(NULL, mTimer);
								mTimerTime = thData->expire*1000;
								mTimer = ::SetTimer(NULL,NULL,mTimerTime,NULL);
							}
						}
					}
					else if(msg.message == UWM_STOP_THREAD)
					{
						::KillTimer(NULL, mTimer);
						SetEvent((HANDLE)msg.lParam);
						break;
					}
					else if(msg.message == UWM_RELOAD_DATA)
					{
					}
					else
					{
						TranslateMessage(&msg);
						DispatchMessage(&msg);
					}
				}
				release_catch_expr_and_tolog(res = 3, _T("AlertLoader loop exception"))
			}
		}
		release_catch_expr_and_tolog(res = 3, _T("AlertLoader get message exception"))

		::CoUninitialize();
		delete_catch(thData);
	}
	release_catch_expr_and_tolog(res = 3, _T("AlertLoader threat exception"))

		return res;
}

CString CAlertLoader::bodyPrepare( CString resultHTML, NodeALERT *  curNode ){

	int topTemplateStartPos = resultHTML.Find(CString(TOP_TEMPLATE_START_SEPARATOR));
	if (topTemplateStartPos>=0)
	{
		CString topTemplateHTML= resultHTML.Mid(topTemplateStartPos+CString(TOP_TEMPLATE_START_SEPARATOR).GetLength());
		int topTemplateEndPos=topTemplateHTML.Find(CString(TOP_TEMPLATE_END_SEPARATOR));
		curNode->m_topTemplateHTML=topTemplateHTML.Mid(0,topTemplateEndPos);
		resultHTML.Delete(topTemplateStartPos,topTemplateEndPos+CString(TOP_TEMPLATE_END_SEPARATOR).GetLength()+CString(TOP_TEMPLATE_START_SEPARATOR).GetLength());
	}

	int bottomTemplateStartPos = resultHTML.Find(CString(BOTTOM_TEMPLATE_START_SEPARATOR));
	if (bottomTemplateStartPos >= 0)
	{
		CString bottomTemplateHTML = resultHTML.Mid(bottomTemplateStartPos+CString(BOTTOM_TEMPLATE_START_SEPARATOR).GetLength());
		int bottomTemplateEndPos=bottomTemplateHTML.Find(CString(BOTTOM_TEMPLATE_END_SEPARATOR));
		bottomTemplateHTML=bottomTemplateHTML.Mid(0,bottomTemplateEndPos);

		curNode->m_bottomTemplateHTML=bottomTemplateHTML;
		resultHTML.Delete(bottomTemplateStartPos,bottomTemplateEndPos+CString(BOTTOM_TEMPLATE_END_SEPARATOR).GetLength()+CString(BOTTOM_TEMPLATE_START_SEPARATOR).GetLength());
	}
	return resultHTML;
}
