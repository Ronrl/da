// AlertAPP.cpp: implementation of the CAlertAPP class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "AlertAPP.h"
#include <wininet.h>

//IAlertSink
STDMETHODIMP CAlertSink::SetParams(
	/* [in] */ HWND hwnd,
	/* [in] */ BOOL skipHttpsCheck)
{
	m_phwnd = hwnd;
	m_skipHttpsCheck = skipHttpsCheck;
	return S_OK;
}

//IWindowForBindingUI
STDMETHODIMP CAlertSink::GetWindow(
	/* [in] */ REFGUID rguidReason,
	/* [out] */ HWND *phwnd)
{
	CComPtr<IWindowForBindingUI> objWindowForBindingUI;
	HRESULT hr = QueryServiceFromClient(&objWindowForBindingUI);
	if(SUCCEEDED(hr) && objWindowForBindingUI)
		hr = objWindowForBindingUI->GetWindow(rguidReason, phwnd);
	else
	{
		if(phwnd)
		{
			*phwnd = m_phwnd;
			hr = S_OK;
		}
		else
			hr = E_INVALIDARG;
	}
	return hr;
}

//IHttpSecurity
STDMETHODIMP CAlertSink::OnSecurityProblem(
	/* [in] */ DWORD dwProblem)
{
	HRESULT res = S_OK;
	CComPtr<IHttpSecurity> objHttpSecurity;
	if(SUCCEEDED(QueryServiceFromClient(&objHttpSecurity)) && objHttpSecurity)
		res = objHttpSecurity->OnSecurityProblem(dwProblem);
	else
	{
		switch(dwProblem)
		{
		case ERROR_INTERNET_SEC_CERT_DATE_INVALID:
		case ERROR_INTERNET_SEC_CERT_CN_INVALID:
		case ERROR_INTERNET_INVALID_CA:
		case ERROR_INTERNET_SEC_INVALID_CERT:
		case ERROR_INTERNET_SEC_CERT_ERRORS:
		case ERROR_INTERNET_SEC_CERT_REV_FAILED:
		case ERROR_INTERNET_SEC_CERT_NO_REV:
		case ERROR_INTERNET_SEC_CERT_REVOKED:
			res = m_skipHttpsCheck ? S_OK : S_FALSE;
			break;
		case ERROR_INTERNET_CLIENT_AUTH_CERT_NEEDED:
			res = S_FALSE;
			break;
		case ERROR_HTTP_REDIRECT_NEEDS_CONFIRMATION:
			res = RPC_E_RETRY;
			break;
		}
	}
	return res;
	
}


