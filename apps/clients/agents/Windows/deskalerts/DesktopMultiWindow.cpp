#include "stdafx.h"
#include "DesktopMultiWindow.h"
#include <Wtsapi32.h>


CDesktopParentWindow::CDesktopParentWindow()
{
	
	Create();
	SetParent(GetDesktopWindow());
}

CDesktopParentWindow::~CDesktopParentWindow(void)
{
	if (IsWindow()) DestroyWindow();
}

CDesktopMultiWindow::CDesktopMultiWindow(NodeALERTS::NodeVIEWS::NodeWINDOW* param):data(*param), m_windowsForFutureShowing(NULL), m_desktopThredId(NULL)
{
	Create();
	_iAlertController->getSwitchDesktopHandler()->addListener(this);
	
	this->winCmd = new CMultiWindowCommand(&data, (HWND)_iAlertModel->getProperty(CString(_T("mainHWND"))));
	//SetTimer(CHECK_LIFETIME_EXPIRATION, 3000);
// 	WTSRegisterSessionNotification(m_hWnd, NOTIFY_FOR_THIS_SESSION);
}

CDesktopMultiWindow::~CDesktopMultiWindow(void)
{
	_iAlertController->getSwitchDesktopHandler()->removeListener(this);
	if(IsWindow()) DestroyWindow();
}

DWORD WINAPI CDesktopMultiWindow::desktopThread(LPVOID pv)
{
	DWORD res = 1;
	release_try
	{
		CoInitialize(NULL);
		desktopThreadData *thData = reinterpret_cast<desktopThreadData*>(pv);
		SetThreadDesktop(thData->desktop);
		SetEvent(thData->readyEvent);
		
		HWND parentHWND = (HWND)_iAlertModel->getProperty(CString(_T("mainHWND")));
		HDESK hDesktop = GetThreadDesktop(GetCurrentThreadId());

		CDesktopParentWindow *parentWindow = NULL;

		CString desktopName;
		if (hDesktop)
		{
			CString mainDesktopName = _iAlertModel->getPropertyString(CString(_T("mainDesktopName")));
			desktopName = CAlertUtils::getDesktopName(hDesktop);
			if (mainDesktopName != desktopName)
			{
				parentWindow = new CDesktopParentWindow();
				parentHWND = parentWindow->m_hWnd;
			}
		}	
		CMultiWindowCommand *winDisp = new CMultiWindowCommand(thData->data, parentHWND);
        if (hDesktop)
        {
            CloseDesktop(hDesktop);
        }
		delete_catch(thData->data);
		
		release_try
		{
			MSG msg;
			while(GetMessage(&msg, NULL, 0, 0) > 0) //important to check > 0, see MSDN
			{
				try
				{
					if (msg.message == UTM_SHOWMULTIWINDOW || msg.message == UTM_SHOWNEWALERTWINDOW)
					{
						CString *param = (CString*)msg.wParam;
						shared_ptr<showData> *ex = (shared_ptr<showData>*)msg.lParam;
						DeskAlertsAssert(winDisp);
						winDisp->show(*param, *ex, msg.message == UTM_SHOWNEWALERTWINDOW);
						delete_catch(param);
						delete_catch(ex);
					}
					else if (msg.message == UTM_HIDEMULTIWINDOW)
					{
						CString *param = (CString*)msg.wParam;
						winDisp->hide(*param);
						delete_catch(param);
						SetEvent((HANDLE)msg.lParam);
						break;
					}
					else if (msg.message == UTM_DESKTOP_CLOSED)
					{	
						CString* openedDesktopName = (CString*)msg.wParam;
						vector<shared_ptr<showData>>* windowsData = winDisp->closeAndGetNotReadUrgentAlerts();
						if(windowsData->empty())
						{
							delete_catch(windowsData);
							delete_catch(openedDesktopName);
						}
						else
						{
							PostMessageDelBoth<vector<shared_ptr<showData>>,CString>((HWND)msg.lParam, WM_SHOW_WINDOWS, windowsData, openedDesktopName);
						}
					}
					else
					{
						TranslateMessage(&msg);
						DispatchMessage(&msg);
					}
				}
				catch(...)
				{
					res = 3;
					DeskAlertsUnexpected();
				}
			}
		}
		release_catch_expr_and_tolog(res = 3, _T("DesktopMulti get message exception"))

		if (parentWindow) delete_catch(parentWindow);
		delete_catch(winDisp);
		CoUninitialize();
	}
	release_catch_expr_and_tolog(res = 3, _T("DesktopMulti threat exception"))
	
	return res;
}


bool CDesktopMultiWindow::show(CString src_param, shared_ptr<showData> ex, BOOL isNewAlert)
{
	bool result = false;
	DWORD errorCode = NO_ERRORS;
	showAndCheckError(src_param, ex, &errorCode, isNewAlert);
	if (errorCode==SCREENSAVER_ACTIVE)
	{
		HRESULT windowslock_success = m_csWindows.Lock();
		DeskAlertsAssert(S_OK == windowslock_success);
		try
		{
            if (!m_windowsForFutureShowing)
            {
                m_windowsForFutureShowing = new std::vector<shared_ptr<showData>>();
            }
            //do not dublicate in alerts  queue to show
            if (m_windowsForFutureShowing->end() == std::find(m_windowsForFutureShowing->begin(), m_windowsForFutureShowing->end(), ex))
            {
                m_windowsForFutureShowing->push_back(ex);
            }
		}
		catch(...)
		{
			DeskAlertsUnexpected();
		}
		windowslock_success = m_csWindows.Unlock();
		DeskAlertsAssert(S_OK == windowslock_success);
		//start screensaver leave handling
		auto timer_handle = SetTimer(CHECK_SCREENSAVER_TIMER, 500);
		DeskAlertsAssert(timer_handle);
	}
	else if (errorCode==DESKTOP_DENIED)
	{
		if (!_iAlertController->getServiceBridge()->sendShowWindowRequest(data.m_name,src_param,ex))
		{
			HRESULT windowslock_success = m_csWindows.Lock();
			DeskAlertsAssert(S_OK == windowslock_success);
			try
			{
				if (!m_windowsForFutureShowing)
				{
					m_windowsForFutureShowing = new std::vector<shared_ptr<showData>>();
				}
                //do not dublicate in alerts  queue to show
                if (m_windowsForFutureShowing->end() == std::find(m_windowsForFutureShowing->begin(), m_windowsForFutureShowing->end(), ex))
                {
                    m_windowsForFutureShowing->push_back(ex);
                }
			}
			catch(...)
			{
				DeskAlertsUnexpected();
			}
			windowslock_success = m_csWindows.Unlock();
			DeskAlertsAssert(S_OK == windowslock_success);
		}
	}
	else
	{
		DeskAlertsAssert(NO_ERRORS == errorCode);
	}
	return true;
}

void CDesktopMultiWindow::showAndCheckError(CString src_param, shared_ptr<showData> ex, DWORD *errorCode, BOOL isNewAlert)
{
	DWORD error = NO_ERRORS;
	CString showDesktopName = ex->desktopName.IsEmpty() ? CString() : ex->desktopName;
	HDESK hDesktop;
	CString desktopName;
	if (ex->urgent==_T("1"))
	{
		BOOL wakeup_successfull = ::PostMessage(HWND_BROADCAST, WM_SYSCOMMAND, SC_MONITORPOWER, (LPARAM) -1); // turn on monitor
		DeskAlertsAssert(wakeup_successfull);
		hDesktop = OpenInputDesktop(0, FALSE, DESKTOP_CREATEWINDOW);

		if (hDesktop)
		{
			desktopName = CAlertUtils::getDesktopName(hDesktop);
		}
		else
		{
			desktopName = _T("WinLogon");
			error = DESKTOP_DENIED;
		}
		if (!showDesktopName.IsEmpty())
		{
			if (desktopName.MakeLower() != showDesktopName.MakeLower())
			{
				if (desktopName.MakeLower() == _T("winlogon") && hDesktop)
				{
					error = DESKTOP_DENIED;
				}	
			}
		}
		else
		{
            appendToErrorFile_TMP(L"\tat alertid: %s, desktopname %s, ticker:%s\n", ex->alertid.GetString(), ex->desktopName.GetString(), ex->ticker.GetString());
		}
	}
	else
	{
		desktopName = _iAlertModel->getPropertyString(CString(_T("mainDesktopName")));
		hDesktop = OpenDesktop(desktopName, 0, FALSE, DESKTOP_CREATEWINDOW);
		DeskAlertsAssert(hDesktop);
	}
	
	ex->desktopName = desktopName;

	if (error == NO_ERRORS)
	{
		if(ex->urgent==_T("1") || !IsScreensaverActive())
		{
			DWORD desktopThreadId;
			HRESULT threadslock_success = m_csThreads.Lock();
			DeskAlertsAssert(S_OK == threadslock_success);
			release_try
			{
				if (!desktopThreadExists(desktopName.MakeLower()))
				{
					desktopThreadId = addDesktopThread(hDesktop,desktopName.MakeLower(),src_param,ex);
				}
				else
				{
					desktopThreadId = getDesktopThreadId(desktopName.MakeLower());
				}
			}
			release_catch_tolog(_T("Desktop threads processing exception"))
			threadslock_success = m_csThreads.Unlock();
			DeskAlertsAssert(S_OK == threadslock_success);
			
            DeskAlertsAssert(desktopThreadId);
			if(desktopThreadId)
			{
				try {
					BOOL res;
					if (isNewAlert)
					{
						res = PostThreadMessageDelBoth<CString, shared_ptr<showData>>(desktopThreadId, UTM_SHOWNEWALERTWINDOW, new CString(src_param), new shared_ptr<showData>(ex));
						DeskAlertsAssert(res);
					}
					else
					{
						res = PostThreadMessageDelBoth<CString, shared_ptr<showData>>(desktopThreadId, UTM_SHOWMULTIWINDOW, new CString(src_param), new shared_ptr<showData>(ex));
						DeskAlertsAssert(res);
					}
				}
				catch (...)
				{
					DeskAlertsUnexpected();
				}
			}
		}
		else
		{
			error = SCREENSAVER_ACTIVE; //don't show usual windows under screensaver
		}
	}
    if (hDesktop)
    {
        CloseDesktop(hDesktop);
    }

	if (errorCode)
	{
		*errorCode = error;
	}
}

BOOL CDesktopMultiWindow::desktopThreadExists(CString desktopName)
{
	BOOL result = TRUE;
	HRESULT threadlock_successfull = m_csThreads.Lock();
	DeskAlertsAssert(S_OK == threadlock_successfull);
	release_try
	{
		auto thread = m_desktopThreads.find(desktopName);
		if(thread == m_desktopThreads.end())
		{
			result = FALSE;
		}
		else
		{
			auto handle = OpenThread(SYNCHRONIZE, FALSE, thread->second);
			result = handle && WaitForSingleObject(handle, 30) != WAIT_OBJECT_0;
			if (!result) {
				m_desktopThreads.erase(thread);
			}
		}
	}
	release_catch_tolog(_T("Desktop thread exists exception"))
	threadlock_successfull = m_csThreads.Unlock();
	DeskAlertsAssert(S_OK == threadlock_successfull);
	return result;
}

DWORD CDesktopMultiWindow::getDesktopThreadId(CString desktopName)
{
	DWORD res = 0;
	m_csThreads.Lock();
	release_try
	{
		map<CString,DWORD>::const_iterator it = m_desktopThreads.find(desktopName);
		if(it != m_desktopThreads.end())
		{
			res = it->second;
		}
	}
	release_catch_tolog(_T("Desktop get thread exception"))
	m_csThreads.Unlock();
	return res;
}


DWORD CDesktopMultiWindow::addDesktopThread(HDESK hDesktop, CString desktopName, CString param, shared_ptr<showData> ex )
{
	DWORD result = 0;
	CDesktopMultiWindow::desktopThreadData *thData = new CDesktopMultiWindow::desktopThreadData();
	thData->desktop=hDesktop;
	thData->readyEvent = CreateEvent(0, FALSE, FALSE, 0);
	thData->data = new NodeALERTS::NodeVIEWS::NodeWINDOW(data);
	DWORD threadId;
	const HANDLE hThread = CreateThread(NULL, 0, CDesktopMultiWindow::desktopThread,(LPVOID)thData, 0, &threadId);
    DeskAlertsAssert(hThread);
	if (hThread)
	{
        if (thData->readyEvent)
		if(WaitForSingleObjectWithMsgLoop(thData->readyEvent, INFINITE) == WAIT_OBJECT_0)
		{
            HRESULT threadslock_success = m_csThreads.Lock();
            DeskAlertsAssert(S_OK == threadslock_success);
            //release_try
			try
			{
				m_desktopThreads.insert(pair<CString,DWORD>(desktopName,threadId));
			}
            catch (...) { DeskAlertsUnexpected(); }
			//release_catch_tolog(_T("Desktop add thread exception"))
            HRESULT threadsUnlock_success = m_csThreads.Unlock();
            DeskAlertsAssert(S_OK == threadslock_success);
			result = threadId;
		}
	}
	return result;
}

void CDesktopMultiWindow::hide(CString srcparam)
{
	m_csThreads.Lock();
	release_try
	{
		map<CString,DWORD>::iterator it = m_desktopThreads.begin();
		while(it != m_desktopThreads.end())
		{
			const HANDLE event = CreateEvent(NULL, TRUE, FALSE, NULL);
            if (event != nullptr)
            {
                if (PostThreadMessageDelWParam<CString>(it->second, UTM_HIDEMULTIWINDOW, new CString(srcparam), (LPARAM)event))
                {
                    WaitForSingleObjectWithMsgLoop(event, INFINITE);
                }
                CloseHandle(event);
            }
			it++;
		}
	}
	release_catch_tolog(_T("Desktop hide exception"))
	m_csThreads.Unlock();
}

LRESULT CDesktopMultiWindow::OnSwitchDesktop(CString *closedDesktopName, CString *openedDesktopName)
{
	if(desktopThreadExists(*closedDesktopName))
	{
		PostThreadMessageDelWParam<CString>(getDesktopThreadId(*closedDesktopName), UTM_DESKTOP_CLOSED, new CString(*openedDesktopName), (LPARAM)m_hWnd);
	}
	else if((*closedDesktopName).MakeLower() == _T("winlogon") && !_iAlertController->getServiceBridge()->checkService())
	{
		showSavedWindows(openedDesktopName);
	}
	return S_OK;
}

LRESULT CDesktopMultiWindow::OnShowWindows(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	vector<shared_ptr<showData>>* windowsData = (vector<shared_ptr<showData>>*)wParam;
	CString* openedDesktopName = (CString*)lParam;
	CString mainDesktopName = _iAlertModel->getPropertyString(CString(_T("mainDesktopName"))).MakeLower();

	vector<shared_ptr<showData>>::iterator it = windowsData->begin();
	while(it != windowsData->end())
	{
		(*it)->desktopName = CString(*openedDesktopName);
		if(mainDesktopName == "winlogon")
		{
            if (TRUE == _iAlertController->getServiceBridge()->sendShowWindowRequest(data.m_name, (*it)->url, (*it)))
                it = windowsData->erase(it);
            //not need it++, couse `erase` return next element after deleted
		}
		else
		{
			show((*it)->url,(*it), uMsg);
            it++;
		}
		
	}

	delete_catch(openedDesktopName);
	//delete_catch(windowsData);

	bHandled = TRUE;
	return S_OK;
}

LRESULT CDesktopMultiWindow::OnTimer(UINT /*uMsg*/, WPARAM wParam, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	
	
	if (wParam == CHECK_SCREENSAVER_TIMER)
	{
		if(!IsScreensaverActive())
		{
			CString mainDesktopName = _iAlertModel->getPropertyString(CString(_T("mainDesktopName"))).MakeLower();
			showSavedWindows(&mainDesktopName);
			KillTimer(CHECK_SCREENSAVER_TIMER);
		}
	}
	else if(wParam == CHECK_LIFETIME_EXPIRATION)
	{
		//winCmd->closeAllExpiredAlerts();
		//_iAlertView->closeExpiredAlerts();
	}
	return S_OK;
}

BOOL CDesktopMultiWindow::IsScreensaverActive()
{
	BOOL ans=0;
	BOOL success = SystemParametersInfo(SPI_GETSCREENSAVERRUNNING,0,&ans,0);
	DeskAlertsAssert(success);
	return ans;
}

void CDesktopMultiWindow::showSavedWindows(CString *openedDesktopName)
{
	m_csWindows.Lock();
	release_try
	{
		if(m_windowsForFutureShowing)
		{
			BOOL res = PostMessageDelBoth<vector<shared_ptr<showData>>,CString>(m_hWnd, WM_SHOW_WINDOWS, m_windowsForFutureShowing, new CString(*openedDesktopName));
            if (!res) //ZERO value is fail, then m_windowsForFutureShowing has deleted
            {
                m_windowsForFutureShowing = NULL;
            }
		}
	}
	release_end_try
	m_csWindows.Unlock();
}
