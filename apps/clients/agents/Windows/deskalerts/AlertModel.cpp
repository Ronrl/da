#include "StdAfx.h"
#include "alertmodel.h"
#include "HistoryStorage.h"
#define UWM_LIFE_SIGNAL WM_APP + 322
CAlertModel::CAlertModel(void) : mainHWND(NULL)
{
}

CAlertModel::~CAlertModel(void)
{
}

void CAlertModel::createComponent()
{
	//appendToErrorFile(L"Lets create AlertModel");
	release_try
	{
		TCHAR path[MAX_PATH];
		ZeroMemory(path, MAX_PATH*sizeof(TCHAR));
		::GetModuleFileName(_Module.GetResourceInstance(),path,MAX_PATH);
		CString sPath = path;
		int st = sPath.ReverseFind('\\');
		if (st)
		{
			sPath = sPath.Mid(0,st+1);
		}
		cPropertyStore.loadProperty();
		const int confLoaded = cModelStore.Parse(sPath);
		CString allUsersAppDataProfilePath = CAlertUtils::allUsersAppDataProfilePath()+"\\";
		CString appDataConfPath = allUsersAppDataProfilePath+"conf.xml";
		if(PathFileExists(appDataConfPath))
		{
			cModelStore.Parse(allUsersAppDataProfilePath);
		}

		cPropertyStore.setPropertyString(CString(_T("deskalerts_loaded")), CString(confLoaded == ERR_NONE ? _T("1") : _T("0")), TRUE);

		NodeALERTS& nodeALERTS = cModelStore.getModel(CString(_T("property")));
		if(nodeALERTS.m_settings)
		{
			std::vector<NodeALERTS::NodeSETTINGS::NodePROPERTY*> vProp = nodeALERTS.m_settings->m_property;
			std::vector<NodeALERTS::NodeSETTINGS::NodePROPERTY*>::iterator it = vProp.begin();
			while(it != vProp.end())
			{
				if ((*it)->m_const == CString(_T("1")))
				{
					cPropertyStore.setPropertyString((*it)->m_id,(*it)->m_default,TRUE,TRUE);
				}
				else
					cPropertyStore.setPropertyString((*it)->m_id,(*it)->m_default,FALSE);
				it++;
			}
			cPropertyStore.setRegistry(nodeALERTS.m_settings->m_registry);
		}

		MSG message;
		message.message = UWM_LIFE_SIGNAL;
		message.lParam = 2 * CAlertUtils::str2int(getPropertyString(CString(_T("normal_expire"))));
		message.wParam = NULL;
		CAlertUtils::PostMessageToWatchDog(message);
	}
	release_catch_tolog(L"Property loading exception");
}

void CAlertModel::destroyComponent()
{
	//appendToErrorFile(L"AlertModel has been destroyed");
}

void CAlertModel::addPropertyListener(IPropertyListener* newListener)
{
	cPropertyStore.addPropertyListener(newListener);
}
void CAlertModel::removePropertyListener(IPropertyListener* newListener)
{
	cPropertyStore.removePropertyListener(newListener);
}

void CAlertModel::addAlertListener(IAlertListener* /*newListener*/)
{
	//cAlertLoader.addAlertListener(newListener);
}

void CAlertModel::removeAlertListener(IAlertListener* /*newListener*/)
{
	//cAlertLoader.removeAlertListener(newListener);
}

void CAlertModel::addModelListener(IModelListener* newListener)
{
	cModelStore.addModelListener(newListener);
}

void CAlertModel::removeModelListener(IModelListener* newListener)
{
	cModelStore.removeModelListener(newListener);
}

//Property
CString CAlertModel::getPropertyString(CString& promertyName, CString *def, const int encode, const bool fixURI)
{
	return cPropertyStore.getPropertyString(promertyName, def, encode, fixURI);
}

void CAlertModel::setPropertyString(CString& promertyName,CString& promertyValue)
{
	cPropertyStore.setPropertyString(promertyName,promertyValue);
}

CString CAlertModel::buildPropertyString(CString& promertyVal, const int encode, const bool fixURI)
{
	return cPropertyStore.buildPropertyString(promertyVal, encode, fixURI);
}


DWORD CAlertModel::getProperty(CString& promertyName)
{
	return cPropertyStore.getProperty(promertyName);
}

void CAlertModel::setProperty(CString& promertyName,DWORD promertyValue)
{
	cPropertyStore.setProperty(promertyName,promertyValue);
}

//Model
NodeALERTS& CAlertModel::getModel(CString& promertyName)
{
	return cModelStore.getModel(promertyName);
}

//Alert
LPVOID CAlertModel::getAlert(CString& /*promertyName*/)
{
	return NULL;//cAlertLoader.getAlert(promertyName);
}

void CAlertModel::setImageList(HIMAGELIST hImageList)
{
	imageList.Attach(hImageList);
}

HIMAGELIST CAlertModel::getImageList()
{
	return imageList.m_hImageList;
}

void CAlertModel::setHotImageList(HIMAGELIST hHotImageList)
{
	hotImageList.Attach(hHotImageList);
}

HIMAGELIST CAlertModel::getHotImageList()
{
	return hotImageList.m_hImageList;
}

bool CAlertModel::EnableActiveDirectory()
{
	return cPropertyStore.EnableActiveDirectory();
}

int CAlertModel::getStatesIndex()
{
	CString alertMode = getPropertyString(CString(_T("AlertMode")));
	CString disableState = getPropertyString(CString(_T("disableState")));
	CString standByState = getPropertyString(CString(_T("standByState")));
	CString disableNewState = getPropertyString(CString(_T("disableNewState")));
	CString unreadMessagesState = getPropertyString(CString(_T("unreadMessagesState")));
	int index=0;
	if (alertMode == _T("offline")) index|=_OFFLINE_;
	if (disableState == _T("on")) index|=_DISABLE_;
	if (standByState == _T("on")) index|=_STANDBY_;
	if (disableNewState == _T("on")) index|=_DISABLE_|_DISABLE_NEW_;
	if (unreadMessagesState == _T("on")) index|=_MESSAGES_UNREAD_;

	return index;
}
