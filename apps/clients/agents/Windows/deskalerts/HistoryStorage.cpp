#include "StdAfx.h"
#include "AlertUtils.h"
#include ".\historystorage.h"
#include <fstream>
#include <iostream>
#include <time.h>
#include "atlrx.h"

CHistoryStorage::CHistoryStorage(void)
{
}

CHistoryStorage::~CHistoryStorage(void)
{
}

void CHistoryStorage::InsertAlertInHistory(shared_ptr<NodeALERT> alertData, CString window, sqlite_int64 *history_id, BOOL *isCreated)
{
	if (alertData)
	{
		CDatabaseManager *_database = CDatabaseManager::shared(true);
		if(_database)
		{
			_database->AddToHistory(alertData, window, history_id, isCreated);
		}
	} 
}

void CHistoryStorage::PurgeAlerts(CString Expire)
{
	long iDate;
	iDate = (long )time(NULL);
	int iExpire = CAlertUtils::str2int(Expire, 0);

	CDatabaseManager *_database = CDatabaseManager::shared(true);
	if(_database)
	{
		_database->PurgeHistory(iExpire);
	}
}

void CHistoryStorage::PurgeAlertsAtTime(CString Expire, CString Time)
{
	long iDate;
	iDate = (long )time(NULL);
	int iExpire = CAlertUtils::str2int(Expire, 0);

	CDatabaseManager *_database = CDatabaseManager::shared(true);
	if(_database)
	{
		_database->PurgeHistoryAtTime(iExpire, Time);
	}
}

void CHistoryStorage::DeleteAlert(sqlite_int64 id)
{
	CDatabaseManager *_database = CDatabaseManager::shared(true);
	if(_database)
	{
		_database->DeleteAlert(id);
	}
}

void CHistoryStorage::DeleteAlertByID(sqlite_int64 alertid)
{
	CDatabaseManager *_database = CDatabaseManager::shared(true);
	if(_database)
	{
		_database->DeleteAlertByID(alertid);
	}
}

void CHistoryStorage::DeleteHistory()
{
	CDatabaseManager *_database = CDatabaseManager::shared(true);
	if(_database)
	{
		_database->DeleteHistory();
	}
}

void CHistoryStorage::MarkAlertAsClosed(sqlite_int64 history_id)
{
	CDatabaseManager *_database = CDatabaseManager::shared(true);
	if(_database)
	{
		_database->markAlertAsClosed(history_id);
	}
}

void CHistoryStorage::MarkAlertAsClosedByIdAndUser(sqlite_int64 alert_id, sqlite_int64 user_id)
{
	CDatabaseManager *_database = CDatabaseManager::shared(true);
	if(_database)
	{
		_database->markAlertAsClosedByIdAndUser(alert_id, user_id);
	}
}