#include "stdafx.h"
#include "LockscreenManager.h"
#include "DatabaseManagr.h"
#include "AlertUtils.h"
#include <vector>
#include <gdiplus.h>
#include <shlobj.h>


CLockscreenAlert::CLockscreenAlert(void) : m_autoclose(NULL), m_id(NULL)
{
}

CLockscreenAlert::~CLockscreenAlert(void)
{
}



CLockscreenManager::CLockscreenManager(void) : m_thread(NULL), m_threadId(0)
{
	Create();
	m_thread = CreateThread(NULL, 0, CLockscreenManager::LockscreenThread, NULL, 0, &m_threadId);
}

CLockscreenManager::~CLockscreenManager(void)
{
	if (m_thread)
	{
		this->restorePreviousLockscreen();
		DWORD exitCode;
		if (GetExitCodeThread(m_thread, &exitCode))
		{
			if (exitCode == STILL_ACTIVE)
			{
				const HANDLE event = CreateEvent(NULL, TRUE, FALSE, NULL);
				if (event != nullptr)
				{
					if (!::PostThreadMessage(m_threadId, UWM_STOP_THREAD, 0, (LPARAM)event) ||
						WaitForSingleObject(event, 5000) == WAIT_OBJECT_0)
					{
						CloseHandle(event);
					}
				}
			}
		}
		CloseHandle(m_thread);
	}
	if (IsWindow())
		DestroyWindow();
}

void CLockscreenManager::setLockscreen(CString fileName, BOOL absolutePath)
{
	LockscreenStruct *lsSt = new LockscreenStruct();
	lsSt->fileName = fileName;
	lsSt->absolutePath = absolutePath;

	PostThreadMessageDelWParam<LockscreenStruct>(m_threadId, UTM_SET_LOCKSCREEN, lsSt, NULL);
}

DWORD WINAPI CLockscreenManager::LockscreenThread(LPVOID /*pv*/)
{
	DWORD res = 1;
	release_try
	{
		MSG msg;
		while (GetMessage(&msg, NULL, 0, 0) > 0) //important to check > 0, see MSDN
		{
			release_try
			{
				if (msg.message == UWM_STOP_THREAD)
				{
					SetEvent((HANDLE)msg.lParam);
					break;
				}
				else if (msg.message == UTM_SET_LOCKSCREEN)
				{
					LockscreenStruct *lsSt = (LockscreenStruct*)msg.wParam;
					CString fileName = lsSt->fileName;
					BOOL absolutePath = lsSt->absolutePath;

					CString imgPath;
					if (absolutePath)
					{
						imgPath = fileName;
					}
					else
					{
						imgPath = CAlertUtils::getLockscreensPath();
						imgPath = imgPath + _T("\\") + fileName;
					}

					HKEY hKey;
					LONG lResult;
					DWORD value = 1;

					lResult = RegOpenKeyEx(HKEY_LOCAL_MACHINE, L"SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\PersonalizationCSP", 0, KEY_READ | KEY_WRITE | KEY_WOW64_64KEY, &hKey);

					if (lResult == ERROR_SUCCESS)
					{
						RegSetValueEx(hKey, L"LockScreenImagePath", 0, REG_SZ, (BYTE*)imgPath.GetBuffer(), sizeof(wchar_t)*imgPath.GetLength());
						RegSetValueEx(hKey, L"LockScreenImageUrl", 0, REG_SZ, (BYTE*)imgPath.GetBuffer(), sizeof(wchar_t)*imgPath.GetLength());
						RegSetValueEx(hKey, L"LockScreenImageStatus", 0, REG_DWORD, (BYTE*)&value, sizeof(value));

						RegCloseKey(hKey);
					}

					
					if (lsSt) delete_catch(lsSt);
				}
				else
				{
					::TranslateMessage(&msg);
					::DispatchMessage(&msg);
				}
			}

			release_catch_expr_and_tolog(res = 3, _T("LockscreenrManager loop exception"))
		}
	}
	
	release_catch_expr_and_tolog(res = 3, _T("LockscreenManager thread exception"))

	return res;
}

void CLockscreenManager::showLockscreens()
{
	KillTimer(_LOCKSCREENS_TIMER_);
	std::vector<CLockscreenAlert*>::iterator it = m_alerts.begin();

	while (it != m_alerts.end())
	{
		delete_catch((*it));
		it = m_alerts.erase(it);
	}

	CDatabaseManager *db = CDatabaseManager::shared(true);
	if (db)
	{
		db->getLockscreenAlerts(m_alerts);
	}

	m_curIt = m_alerts.begin();

	if (m_curIt != m_alerts.end())
	{
		CString userLockscreen = _iAlertModel->getPropertyString(CString(_T("user_lockscreen")), &CString(_T("")));

	

		setLockscreen((*m_curIt)->m_fileName);
		SetTimer(_LOCKSCREENS_TIMER_, ((*m_curIt)->m_autoclose == 0) ? USER_TIMER_MAXIMUM : (*m_curIt)->m_autoclose * 1000, NULL);
	}
	else
	{
		restorePreviousLockscreen();
	}
}

LRESULT CLockscreenManager::OnTimer(UINT /*uMsg*/, WPARAM wParam, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	if (wParam == _LOCKSCREENS_TIMER_)
	{
		m_curIt++;
		if (m_curIt == m_alerts.end()) m_curIt = m_alerts.begin();
		setLockscreen((*m_curIt)->m_fileName);
		KillTimer(_LOCKSCREENS_TIMER_);
		SetTimer(_LOCKSCREENS_TIMER_, ((*m_curIt)->m_autoclose == 0) ? USER_TIMER_MAXIMUM : (*m_curIt)->m_autoclose * 1000, NULL);
	}
	return S_OK;
}

void CLockscreenManager::restorePreviousLockscreen()
{
	setLockscreen("", TRUE);
}
