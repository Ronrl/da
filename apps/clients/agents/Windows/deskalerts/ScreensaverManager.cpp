#include "stdafx.h"
#include "ScreensaverManager.h"
#include "AlertUtils.h"
#include "RegistryMonitor.h"

CScreensaverManager::CScreensaverManager(void): m_registryMonitor(NULL)
{
	if(_iAlertModel->getPropertyString(CString(_T("screensaverEnabled"))) == _T("1"))
	{
		start();
	}
}

CScreensaverManager::~CScreensaverManager(void)
{
	if (m_registryMonitor) delete_catch(m_registryMonitor);
}

void CScreensaverManager::enableScreensaver()
{
	if (_iAlertModel->getPropertyString(CString(_T("screensaverEnabled"))) != _T("1")) return;
	CRegKey key = CAlertUtils::getDesktopRegKey();
	if (_iAlertModel->getPropertyString(CString(_T("screensaver_enabled"))) != _T("1"))
	{
		_iAlertModel->setPropertyString(CString(_T("user_screensaver")),CAlertUtils::QueryRegValue(key,_T("SCRNSAVE.EXE"),CString()));
	}
	
	key.SetStringValue(_T("SCRNSAVE.EXE"), CAlertUtils::getScreensaverPath(false));
    TCHAR * cValueStr = new TCHAR[MAX_VALUE_NAME];
    DWORD pdwCountStr = MAX_VALUE_NAME;
    //if there is no "ScreenSaveTimeOut" reg key (NO one user click on screensaver settings "ok" button still), then create new parametr )
    if (ERROR_SUCCESS != key.QueryStringValue(_T("ScreenSaveTimeOut"), cValueStr, &pdwCountStr)) 
    {
        key.SetStringValue(_T("ScreenSaveTimeOut"), _T("300"));
    }
	
	key.SetStringValue(_T("ScreenSaveActive"), _T("1"));
	_iAlertModel->setPropertyString(CString(_T("screensaver_enabled")),CString(_T("1")));
}

void CScreensaverManager::disableScreensaver()
{
	if (_iAlertModel->getPropertyString(CString(_T("screensaverEnabled"))) != _T("1")) return;
	CRegKey key = CAlertUtils::getDesktopRegKey();
	key.SetStringValue(_T("SCRNSAVE.EXE"), _iAlertModel->getPropertyString(CString(_T("user_screensaver")),&CString()));
	_iAlertModel->setPropertyString(CString(_T("screensaver_enabled")),CString(_T("0")));	
	_iAlertModel->setPropertyString(CString(_T("screensaversHash")), CString(_T("")));
	key.SetStringValue(_T("ScreenSaveActive"), _T("0"));
}

void CScreensaverManager::start()
{
	CRegKey key = CAlertUtils::getDesktopRegKey();
	m_registryMonitor = new RegistryMonitor();
	m_registryMonitor->startRegMonitoring(key, &(CScreensaverManager::registryMonitorCallback), NULL);
}

int CScreensaverManager::registryMonitorCallback(LPVOID)
{
	CRegKey key = CAlertUtils::getDesktopRegKey();
	CString val = CAlertUtils::QueryRegValue(key,_T("SCRNSAVE.EXE"),CString());

	if (_iAlertModel->getPropertyString(CString(_T("screensaver_enabled"))) == _T("1"))
	{
		if ( val != CAlertUtils::getScreensaverPath(false))
		{
			key.SetStringValue(_T("SCRNSAVE.EXE"),CAlertUtils::getScreensaverPath(false));
		}
        TCHAR * cValueStr = new TCHAR[MAX_VALUE_NAME];
        DWORD pdwCountStr = MAX_VALUE_NAME;
        //if there is no "ScreenSaveTimeOut" reg key (NO one user click on screensaver settings "ok" button still), then create new parametr )
        if (ERROR_SUCCESS != key.QueryStringValue(_T("ScreenSaveTimeOut"), cValueStr, &pdwCountStr))
        {
            key.SetStringValue(_T("ScreenSaveTimeOut"), _T("300"));
        }
		/*if (CAlertUtils::QueryRegValue(key,_T("ScreenSaveTimeOut"),CString()).IsEmpty())
		{
			key.SetStringValue(_T("ScreenSaveTimeOut"),_T("60"));
		}*/
		if (CAlertUtils::QueryRegValue(key,_T("ScreenSaveActive"),CString()).IsEmpty())
		{
			key.SetStringValue(_T("ScreenSaveActive"),_T("1"));
		}
	}
	else
	{
		_iAlertModel->setPropertyString(CString(_T("user_screensaver")),CAlertUtils::QueryRegValue(key,_T("SCRNSAVE.EXE"),CString()));
	}
	return 0;
}