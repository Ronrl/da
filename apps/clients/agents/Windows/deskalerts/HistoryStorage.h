#pragma once
#include "DatabaseManagr.h"

class CHistoryStorage
{
public:
	CHistoryStorage(void);
	~CHistoryStorage(void);

	static void InsertAlertInHistory(shared_ptr<NodeALERT> alertData, CString window, sqlite_int64 *history_id, BOOL *isCreated);
	static void PurgeAlerts(CString Expire);
	static void PurgeAlertsAtTime(CString Expire, CString Time);
	static void DeleteAlert(sqlite_int64 id);
	static void DeleteAlertByID(sqlite_int64 alertid);
	static void DeleteHistory(void);
	static void MarkAlertAsClosed(sqlite_int64 history_id);
	static void MarkAlertAsClosedByIdAndUser(sqlite_int64 alert_id, sqlite_int64 user_id);
};
