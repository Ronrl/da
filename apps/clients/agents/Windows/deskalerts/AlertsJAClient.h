#pragma once
#include <stdexcept>
#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"
#include "JARequest.h"
#include "JAResponse.h"
#include<time.h>  
#include<list>

#define MD5_SIZE 32
#define CHECK_API_V1_USER_CONTENTS_PERIOD 120*CLOCKS_PER_SEC 
//
//Responses
//
struct UserAlert {
	int alertId ;
	
	TCHAR * url;
	TCHAR * body;
};


// Client responce data

class rd_user : public ResponseData
{
public :
	rd_user(CString data){};
	rd_user(){};
	~rd_user(){};

	CString getData();
	ResponseData * fromJSON(const rapidjson::Value& doc) ;
	CString getAuthorization(){ 
		return _authorization==""|| _authorization.IsEmpty()?"dummy":_authorization; 
	}
	void setAuthorization(CString value){ _authorization = value; }
private:
	CString _data;
	CString _authorization;
	ResponseData * rd_user::fromJSONValue(const rapidjson::Value& doc);

};


class rd_alerts : public ResponseData
{
public :
    rd_alerts(CString data)
    {
        alerts = new UserAlert();
        memset((void *)& wallpapersHash, '\0', sizeof(TCHAR) * (MD5_SIZE + 1) );
        memset((void *)& screensaversHash, '\0', sizeof(TCHAR) * (MD5_SIZE + 1));
		memset((void *)& lockscreensHash, '\0', sizeof(TCHAR) * (MD5_SIZE + 1));
    };
	rd_alerts()
    {
        alerts = new UserAlert();
        memset((void *)& wallpapersHash, '\0', sizeof(TCHAR) * (MD5_SIZE + 1));
        memset((void *)& screensaversHash, '\0', sizeof(TCHAR) * (MD5_SIZE + 1));
		memset((void *)& lockscreensHash, '\0', sizeof(TCHAR) * (MD5_SIZE + 1));
    };
	~rd_alerts(){};

	CString getData();
	ResponseData * fromJSON(const rapidjson::Value& doc) ;

	CString getScreensaversHash(){ return _screensaversHash; };
	CString getWallpapersHash(){ return _wallpapersHash; };
private:
	CString _data;
	CString _wallpapersHash;
	CString _screensaversHash;
	CString _lockscreensHash;

	UserAlert *alerts;
	TCHAR wallpapersHash[MD5_SIZE+1];
    TCHAR screensaversHash[MD5_SIZE+1];
	TCHAR lockscreensHash[MD5_SIZE + 1];
};

class rd_wallpaper_id_list : public ResponseData
{
public :
    rd_wallpaper_id_list(CString data) { alerts = new UserAlert(); };
	rd_wallpaper_id_list(){ alerts = new UserAlert(); };
	~rd_wallpaper_id_list(){};

	CString getData();
	ResponseData * fromJSON(const rapidjson::Value& doc) ;
private:
	CString _data;
	UserAlert *alerts;
};

class rd_lockscreen_id_list : public ResponseData
{
public:
	rd_lockscreen_id_list(CString data) { alerts = new UserAlert(); };
	rd_lockscreen_id_list() { alerts = new UserAlert(); };
	~rd_lockscreen_id_list() {};

	CString getData();
	ResponseData * fromJSON(const rapidjson::Value& doc);
private:
	CString _data;
	UserAlert *alerts;
};


class rd_screensaver_id_list : public ResponseData
{
public :
	rd_screensaver_id_list(CString data)
    {
        alerts = new UserAlert();
    };
	rd_screensaver_id_list()
    {
        alerts = new UserAlert();
    };
	~rd_screensaver_id_list(){};

	CString getData();
	ResponseData * fromJSON(const rapidjson::Value& doc) ;
private:
	CString _data;
	UserAlert *alerts;
};


// "userAlerts": [
//            {
//                "alertId": 1024,
//                "htmlMarkup": "<html><head><meta http-equiv='content-type' content='text/html; charset = utf-8'/></head><body style='margin: 5px; overflow: auto;'><p><span style='color: #333333; font-family: Arial, sans-serif;'><p>Test body</p><!-- html_title = 'Test title' --></span></p></body></html>"
//            }
//        ],
//        "wallpapersHash": "624D82924638812B15441CB6E2369F1A",
//        "screensaversHash": "C4CA4238A0B923820DCC509A6F75849B"
	

//
//  Requests
//
class JAReq_User : JARequest
{
public :
	JAReq_User(	AuthorizationType  authorizationType, 
				CString userName,
				CString computerName,
				CString domainName,
				CString ip,
				CString clientVersion,
				CString	deskbarId,
				CString md5Password,
				CString compDomainName
					):	_authorizationType ( authorizationType ),		_userName ( userName ),		_computerName ( computerName ),
									_domainName (domainName),		_ip (ip),		_clientVersion (clientVersion),		_deskbarId (deskbarId),		
									_md5Password (md5Password.IsEmpty()? "3FC0A7ACF087F549AC2B266BAF94B8B1" : md5Password ),
									_deviceType(1),
									_compDomainName(compDomainName)
	{};
	
	std::string toJSON() ;
	const CString getURL();
protected :
	AuthorizationType  _authorizationType;
	CString _authorization;
	CString _userName;
	CString _computerName;
	CString _domainName;
	CString _ip;
	CString _clientVersion;
	CString	_deskbarId;
	CString _md5Password;
	WORD _deviceType;
	CString _compDomainName;
};


class JAReq_Alerts : JARequest
{
public :
	JAReq_Alerts(	CString  authorization, CString localTime ):	_authorization ( authorization ),	_localTime( localTime ){};
	
	CString getAutorization(){
		return _authorization;
	}

	std::string toJSON() ;
	const CString getURL();
protected :
	CString  _authorization;
	CString  _localTime ;
};

class JAReq_getUserContents_v2 : JARequest_v2 
{
public:
	JAReq_getUserContents_v2(CString  authorization, CString localTime) : _authorization(authorization), _localTime(localTime) {};

	CString getAutorization() {
		return _authorization;
	}

	std::string toJSON();
	const CString getURL();
protected:
	CString  _authorization;
	CString  _localTime;
};

class JAReq_getUserHistory_v2 : JARequest_v2
{
public:
	JAReq_getUserHistory_v2(CString  authorization) : _authorization(authorization) {};
	JAReq_getUserHistory_v2(CString  authorization, int from, int count, CString query, std::list<int> types, CString order) : JAReq_getUserHistory_v2(authorization)
	{
		_from = from; 
		_count = count; 
		_query = query;
		_types = types;
		_order = order;
	};
	CString getAutorization() {
		return _authorization;
	}
	std::string toJSON();
	const CString getURL();
	const CString getTypesString();
	
protected:
	int _from;
	int _count;
	CString _query;
	std::list<int> _types;
	CString _order;
	CString  _authorization;
};

class JAReq_getUserTikers_v2 : JARequest_v2
{
public:
	JAReq_getUserTikers_v2(CString  authorization) : _authorization(authorization) {};

	CString getAutorization() {
		return _authorization;
	}

	std::string toJSON();
	const CString getURL();
protected:
	CString  _authorization;
};
using namespace rapidjson;

struct AlertConfirmation{
	long alertId;
	int status;
};

class JAReq_Received : JARequest
{
public :
	JAReq_Received(	CString authorization, CString localTime ): _authorization(authorization), _localTime( localTime ){ 
	};
	~JAReq_Received(){ 
	}

	CString getAuthorization(){
		return _authorization;
	}

	std::string toJSON() ;
	const CString getURL();
	void AddAlertId( long alertId, int status );
	void AddScreensaverId( long alertId, int status );
	void AddWallpaperId( long alertId, int status );
	void AddLockscreenId(long alertId, int status);

	const std::vector<AlertConfirmation> getAlertsList(){ return _alerts; };
	const std::vector<AlertConfirmation> getScreensaversList(){ return _screensavers; };
	const std::vector<AlertConfirmation> getWallpapersList(){ return _wallpapers; };
	const std::vector<AlertConfirmation> getLockscreensList() { return _lockscreens; };
protected :
	CString _authorization;
	CString  _localTime ;

	std::vector<AlertConfirmation> _alerts; 
	std::vector<AlertConfirmation> _wallpapers;
	std::vector<AlertConfirmation> _screensavers;
	std::vector<AlertConfirmation> _lockscreens;

	void array2JsonWriter( const char * arrayName, rapidjson::Writer<StringBuffer> *writer,  std::vector<AlertConfirmation>  alertsArray);
};

class JAReq_TerminateReceived : JARequest
{
public:
	JAReq_TerminateReceived(CString authorization, long alertId) : _authorization(authorization), _alertId(alertId){
	};
	~JAReq_TerminateReceived() {
	}

	CString getAuthorization() {
		return _authorization;
	}

	std::string toJSON();
	const CString getURL();

protected:
	CString _authorization;
	long _alertId;
};

//alertsRead
class JAReq_AlertRead : JARequest
{
public :
	JAReq_AlertRead(	CString authorization, CString localTime , int alertId/*, int status*/ ): 
	   _authorization(authorization), _localTime( localTime ), _alertId( alertId )/*, _status( status )*/{ 
	};
	~JAReq_AlertRead(){ 
	}

	CString getAuthorization(){
		return _authorization==""|| _authorization.IsEmpty()?"dummy":_authorization;
	}

	std::string toJSON() ;
	const CString getURL();
protected :
	CString _authorization;
	CString  _localTime ;

	int _alertId; 
	//int _status;
};

//Send crash logs
class JAReq_CrashLogs : JARequest
{
public:
	JAReq_CrashLogs() {
	};
	~JAReq_CrashLogs() {
	}

	std::string toJSON();
	const CString getURL();
};


// Request helper
class CJAClient
{
public:
	bool SendRequest(JARequest* request, BOOL isToFile, LPCTSTR szFileName, BOOL isToString, string* resStringName, BOOL* isUTF8, BOOL isRecursionBlocked = FALSE, int* httpRequestStatus = NULL);
	bool Connect();
	CJAClient(): _authType(Domain), _adEnabled(NULL), _ETag(CString()), _statusAPIv1UserContent(statusAPIv1UserContent::UNKNOW), _synFreeAdEnabled(NULL), _isbackupServer(NULL){};
	CJAClient(CString server, CString backupServer, CString userName, CString md5Passwd,
		CString domainName, CString fullDomainName, CString computerName,
		CString ip, CString clientVersion, CString deskbarId, BOOL adEnabled, BOOL synFreeAdEnabled, BOOL isbackupServer) :
		_server(server), _backupServer(backupServer), _userName(userName), _md5Password(md5Passwd),
		_domainName(domainName), _ip(ip), _clientVersion(clientVersion), _deskbarId(deskbarId), _adEnabled(adEnabled), _authType(Domain), _ETag(CString()), _statusAPIv1UserContent(statusAPIv1UserContent::UNKNOW), _synFreeAdEnabled(synFreeAdEnabled), _isbackupServer(isbackupServer){};

	void setCredentials(CString server, CString backupServer, CString userName, CString md5Passwd,
		CString domainName, CString fullDomainName, CString compDomainName,  CString computerName,
		CString ip, CString clientVersion, CString deskbarId, BOOL adEnabled, BOOL synFreeAdEnabled)
	{
		_server = server;
		_backupServer = backupServer;
		_userName = userName;
		_md5Password = md5Passwd;
		_domainName = domainName;
		_compDomainName = compDomainName;
		_fullDomainName = fullDomainName;
		_ip = ip;
		_clientVersion = clientVersion;
		_deskbarId = deskbarId;
		_computerName = computerName;
		_adEnabled = adEnabled;
        _synFreeAdEnabled = synFreeAdEnabled;
	}
	CString getAuthorization() {
		return _authorization == "" || _authorization.IsEmpty() ? "dummy" : _authorization;
	}
    void setETag(CString srcETag) 
    {
        _ETag = srcETag;
    }
    CString getETag() 
    {
        return _ETag;
    }
	CString getServer() {
		return _server;
	}
    int getAPIv1UserContentStatus()
    {
        return _statusAPIv1UserContent;
    }
    void setAPIv1UserContentStatus(int newStatus)
    {
        if (statusAPIv1UserContent::ENABLE == newStatus ||
            statusAPIv1UserContent::DISABLE == newStatus
            )
        {
            _statusAPIv1UserContent = newStatus;
        }
    }
    enum statusAPIv1UserContent {
        UNKNOW,ENABLE,DISABLE
    };
    CString getheader_IfNoneMatch()
    {
        CString empty_ETag(_T("\"\""));
        CString ret = CString(_T("if-none-match: ")) + _iAlertModel->getPropertyString(CString(_T("ETag")),&empty_ETag);
        return ret;
    }
    void SwtichServer() 
    {
        _isbackupServer = !_isbackupServer;
    }
    BOOL getisBackupServer() 
    {
        return _isbackupServer;
    }
protected:
	CString _authorization;
	AuthorizationType _authType;
	CString _userName;
	CString _server;
	CString _backupServer;
    BOOL _isbackupServer;
	CString _computerName;
	CString _domainName;
	CString _compDomainName;
	CString _fullDomainName;
	CString _ip;
	CString _clientVersion;
	CString _deskbarId;
	CString _md5Password;
	BOOL	_adEnabled;
    BOOL _synFreeAdEnabled;
    CString _ETag;
    int _statusAPIv1UserContent;
    
};


// Request helper
class CJAClient_v2
{
public:
	bool SendRequest(JARequest_v2* request, BOOL isToFile, LPCTSTR szFileName, BOOL isToString, string* resStringName, BOOL* isUTF8, BOOL isRecursionBlocked = FALSE, int* httpRequestStatus = NULL);
	bool Connect();
	CJAClient_v2() : _authType(Domain), _adEnabled(NULL), _ETag(CString()), _statusAPIv1UserContent(statusAPIv1UserContent::UNKNOW), _synFreeAdEnabled(NULL), _isbackupServer(NULL) {};
	CJAClient_v2(CString server, CString backupServer, CString userName, CString md5Passwd,
		CString domainName, CString fullDomainName, CString computerName,
		CString ip, CString clientVersion, CString deskbarId, BOOL adEnabled, BOOL synFreeAdEnabled, BOOL isbackupServer) :
		_server(server), _backupServer(backupServer), _userName(userName), _md5Password(md5Passwd),
		_domainName(domainName), _ip(ip), _clientVersion(clientVersion), _deskbarId(deskbarId), _adEnabled(adEnabled), _authType(Domain), _ETag(CString()), _statusAPIv1UserContent(statusAPIv1UserContent::UNKNOW), _synFreeAdEnabled(synFreeAdEnabled), _isbackupServer(isbackupServer) {};

	void setCredentials(CString server, CString backupServer, CString userName, CString md5Passwd,
		CString domainName, CString fullDomainName, CString compDomainName, CString computerName,
		CString ip, CString clientVersion, CString deskbarId, BOOL adEnabled, BOOL synFreeAdEnabled)
	{
		_server = server;
		_backupServer = backupServer;
		_userName = userName;
		_md5Password = md5Passwd;
		_domainName = domainName;
		_compDomainName = compDomainName;
		_fullDomainName = fullDomainName;
		_ip = ip;
		_clientVersion = clientVersion;
		_deskbarId = deskbarId;
		_computerName = computerName;
		_adEnabled = adEnabled;
		_synFreeAdEnabled = synFreeAdEnabled;
	}
	CString getAuthorization() {
		return _authorization == "" || _authorization.IsEmpty() ? "dummy" : _authorization;
	}
	void setETag(CString srcETag)
	{
		_ETag = srcETag;
	}
	CString getETag()
	{
		return _ETag;
	}
	CString getServer() {
		return _server;
	}
	int getAPIv1UserContentStatus()
	{
		return _statusAPIv1UserContent;
	}
	void setAPIv1UserContentStatus(int newStatus)
	{
		if (statusAPIv1UserContent::ENABLE == newStatus ||
			statusAPIv1UserContent::DISABLE == newStatus
			)
		{
			_statusAPIv1UserContent = newStatus;
		}
	}
	enum statusAPIv1UserContent {
		UNKNOW, ENABLE, DISABLE
	};
	CString getheader_IfNoneMatch()
	{
		CString empty_ETag(_T("\"\""));
		CString ret = CString(_T("if-none-match: ")) + _iAlertModel->getPropertyString(CString(_T("ETag")), &empty_ETag);
		return ret;
	}
	void SwtichServer()
	{
		_isbackupServer = !_isbackupServer;
	}
	BOOL getisBackupServer()
	{
		return _isbackupServer;
	}
protected:
	CString _authorization;
	AuthorizationType _authType;
	CString _userName;
	CString _server;
	CString _backupServer;
	BOOL _isbackupServer;
	CString _computerName;
	CString _domainName;
	CString _compDomainName;
	CString _fullDomainName;
	CString _ip;
	CString _clientVersion;
	CString _deskbarId;
	CString _md5Password;
	BOOL	_adEnabled;
	BOOL _synFreeAdEnabled;
	CString _ETag;
	int _statusAPIv1UserContent;

};