//
//  CmdDeskAlerts.cpp
//  DeskAlerts Client
//  Common module
//
//  Copyright Softomate 2006-2009. All rights reserved.
//

//////////////////////////////////////////////////////////////////////
// implementation of the CmdDeskAlerts class.
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "CmdDeskAlerts.h"
#include "AlertsDlg.h"

#include "resource.h"
#include "AlertApplView.h"


//#include "MainFrm.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

volatile LONG iObjCount=0;

CmdDeskAlerts::CmdDeskAlerts()
{
	hWnd = 0;
}

CmdDeskAlerts::~CmdDeskAlerts()
{
}

int CmdDeskAlerts::OnInit(LPVOID pData)
{
	if(pData != NULL)
		tbCtrl = (VirtualToolbar*)pData;
	if (!_iAlertModel)
	{
		/*
		hWnd = tbCtrl->GetTBWindow();
		TCHAR *ptr = tbCtrl->GetToolbarKey();
		CString  RegKey (ptr);
		tbCtrl->DeallocMemory(ptr);
		RegKey = RegKey.Mid(RegKey.Find(L"\\")+1);
		CAlertUtils::Key = RegKey;

		ptr = tbCtrl->GetProgPath();
		CString  AppPath (ptr);
		tbCtrl->DeallocMemory(ptr);
		CAlertController::m_sAppDataPath = AppPath + L"Cache";
		*/
		alertController.setMainHWND(hWnd);
		alertController.createComponent();
	}
	return 0;
}

int CmdDeskAlerts::OnQuit(LPVOID /*pData*/)
{
	alertController.destroyComponent();
	return 0;
}
int CmdDeskAlerts::Apply(VirtualToolbar* /*pwndToolbar*/, bool /*bIsPressed*/)
{
	SysMenuEvent cSysMenuEvent(
		CString(_T("select")),
		CString(_T("sysmenu")),
		CString(_T("history"))
		);
	alertController.actionPerformed(cSysMenuEvent);
	return ERR_NONE;
}
/*
PROPSHEETPAGE* CmdDeskAlerts::GetPropertyPage()
{
	CAlertsDlg *pDlg = new CAlertsDlg(&Locale, tbCtrl, &alertController);
	pDlg->SetTitle(Locale.localizeString(_T("alerts.header")));
	return *pDlg;
}*/

