#include "StdAfx.h"
#include "Constants.h"
PWINDOWDATA InteractionConstants::parseWindowData(PREQUESTPACKET packet){
	PWINDOWDATA windowData = (PWINDOWDATA)packet->data; 
	windowData->param = (WCHAR*)(packet->data + sizeof(WINDOWDATA));
	windowData->viewName = getNextString(param);
	windowData->url = getNextString(viewName);
	windowData->post = getNextString(url);
	windowData->expire = getNextString(post);
	windowData->acknowledgement = getNextString(expire);
	windowData->create_date = getNextString(acknowledgement);
	windowData->title = getNextString(create_date);
	windowData->width = getNextString(title);
	windowData->height = getNextString(width);
	windowData->position = getNextString(height);
	windowData->autoclose = getNextString(position);
	windowData->topTemplateHTML = getNextString(autoclose);
	windowData->bottomTemplateHTML = getNextString(topTemplateHTML);
	windowData->visible = getNextString(bottomTemplateHTML);
	windowData->cdata = getNextString(visible);
	windowData->ticker = getNextString(cdata);
	windowData->userid = getNextString(ticker);
	windowData->alertid = getNextString(userid);
	windowData->survey = getNextString(alertid);
	windowData->schedule = getNextString(survey);
	windowData->recurrence = getNextString(schedule);
	windowData->urgent = getNextString(recurrence);
	windowData->searchTerm = getNextString(urgent);
	windowData->desktopName = getNextString(searchTerm);
	return windowData;
}