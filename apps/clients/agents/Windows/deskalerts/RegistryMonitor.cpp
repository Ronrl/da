#include "stdafx.h"
#include "RegistryMonitor.h"

RegistryMonitor::RegistryMonitor(): m_threadId(0), m_thread(NULL), m_callbackFunc(NULL), m_pv(NULL)
{
	Create();
}

RegistryMonitor::~RegistryMonitor()
{
	if(m_thread)
	{
		DWORD exitCode;
		if(GetExitCodeThread(m_thread, &exitCode))
		{
			if(exitCode == STILL_ACTIVE)
			{
				const HANDLE event = CreateEvent(NULL, TRUE, FALSE, NULL);
                if (event != nullptr)
                {
                    if (!PostThreadMessage(m_threadId, UTM_STOP_MONITOR, NULL, (LPARAM)event) ||
                        WaitForSingleObject(event, 5000) == WAIT_OBJECT_0)
                    {
                        CloseHandle(event);
                    }
                }
			}
		}
		CloseHandle(m_thread);
	}
	if (IsWindow()) DestroyWindow();
}

DWORD WINAPI RegistryMonitor::notifyThread(LPVOID pv)
{
	DWORD res = 1;
	release_try
	{
		//open key
		ThreadStruct *thStruct = reinterpret_cast<ThreadStruct*>(pv);
		const HKEY key = thStruct->key.m_hKey;
		DWORD dwStatus;
		do
		{
			// Create an event.
			const HANDLE h = CreateEvent(NULL, TRUE, FALSE, NULL);
			if(h == NULL)
			{
				res = 2;
				break;
			}

			// Watch the registry key for a change of value.
			if(RegNotifyChangeKeyValue(key, TRUE, REG_NOTIFY_CHANGE_LAST_SET, h, TRUE) != ERROR_SUCCESS)
			{
				res = 2;
				CloseHandle(h);
				break;
			}

			// Wait for an event to occur
			do
			{
				dwStatus = MsgWaitForMultipleObjects(1, &h, FALSE, INFINITE, QS_ALLINPUT);
				if(dwStatus == WAIT_OBJECT_0)
				{
					::PostMessage(thStruct->hwnd, UWM_VALUE_CHANGED, NULL, NULL);
				}
				else if(dwStatus == WAIT_OBJECT_0+1)
				{
					release_try
					{
						MSG msg;
						while(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) //no need to check > 0, see MSDN
						{
							release_try
							{
								if(msg.message == WM_QUIT || msg.message == UTM_STOP_MONITOR)
								{
									if(msg.message == UTM_STOP_MONITOR)
									{
										SetEvent((HANDLE)msg.lParam);
									}
									dwStatus = WAIT_ABANDONED;
									break; // abandoned due to WM_QUIT or UTM_STOP_MONITOR
								}
								else
								{
									TranslateMessage(&msg);
									DispatchMessage(&msg);
								}
							}
							release_catch_expr_and_tolog(res = 3, _T("RegistryMonitor msg loop exception"))
						}
					}
					release_catch_expr_and_tolog(res = 3, _T("RegistryMonitor msg peek exception"))
				}
				else
				{
					res = 2;
				}
			}
			while(dwStatus == WAIT_OBJECT_0+1); //continue to wait
			CloseHandle(h);
		}
		while(dwStatus == WAIT_OBJECT_0); //new wait after registry changes

		delete_catch(thStruct);
	}
	release_catch_expr_and_tolog(res = 3, _T("RegistryMonitor thread exception"))

	return res;
}

void RegistryMonitor::startRegMonitoring(CRegKey key, int (*callbackFunc)(LPVOID pv),LPVOID pv)
{
	m_callbackFunc = callbackFunc;
	m_pv = pv;
	ThreadStruct *thStruct = new ThreadStruct();
	thStruct->key = key;
	thStruct->hwnd = m_hWnd;
	m_thread = CreateThread(NULL, 0, RegistryMonitor::notifyThread,(LPVOID)thStruct, 0, &m_threadId);
}

LRESULT RegistryMonitor::OnValueChanged(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	if(m_callbackFunc)
	{
		m_callbackFunc(m_pv);
	}
	return S_OK;
}

