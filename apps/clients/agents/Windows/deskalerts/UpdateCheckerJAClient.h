#pragma once

#include <stdexcept>
#include "JAResponse.h"
#include "rapidjson/document.h"
#include "AlertUtils.h"


#define MD5_SIZE 32

/*class ResponseData
{
public:
	ResponseData(void){};
	~ResponseData(void){};
	virtual  ResponseData * fromJSON(const rapidjson::Value& doc) = 0;
};
*/

class rd_Version : public ResponseData
{
public :
	rd_Version(){};
	rd_Version(CString version){};
	~rd_Version(){};

	CString getVersion(){ return _version; }

    ResponseData * fromJSON(const rapidjson::Value& doc) 
	{
		return fromJSONValue(doc["data"]);    
	}

private:
	CString _version;
	
	ResponseData * fromJSONValue(const rapidjson::Value& doc) 
	{
		/*if(!doc.HasMember("data"))
                throw std::runtime_error("missing fields");*/

		if( doc.IsObject() && doc.HasMember("version")) _version = doc["version"].GetString();
		else _version = "";

        return (ResponseData *)this;    
	}

};

class rd_Slot : public ResponseData
{
public :
	rd_Slot(CString slotId){};
	rd_Slot(){};
	~rd_Slot(){};
	CString getSlotId(){ return _slotId; }
	CString getDownloadUrl(){ return _downloadUrl; }
	ResponseData * fromJSON(const rapidjson::Value& doc) 
	{
		return fromJSONValue(doc["data"]);    
	}
private:
	CString _slotId;
	CString _downloadUrl;
	ResponseData * fromJSONValue(const rapidjson::Value& doc) 
	{
        _slotId = doc["slotId"].GetString();
		_downloadUrl = doc["url"].GetString();
        return (ResponseData *)this;    
	}
};

class rd_Download : public ResponseData
{
public :
	rd_Download(CString downloadUrl){};
	rd_Download(){};
	~rd_Download();
	CString getDownloadUrl(){ return _downloadUrl; }
	ResponseData * fromJSON(const rapidjson::Value& doc) 
	{
		_downloadUrl = doc["data"].GetString();    
		return (ResponseData *)this;
	}
private:
	CString _downloadUrl;
};

class rd_Delete : public ResponseData
{
public :
	rd_Delete(CString data){};
	rd_Delete(){};
	~rd_Delete(){};

	CString getData(){ return _data; }
	ResponseData * fromJSON(const rapidjson::Value& doc) 
	{
		_data = doc["data"].GetString();    
		return (ResponseData *)this;
	}
private:
	CString _data;
};
/*
class ResponseObj
{
public :
	ResponseObj(){};
	~ResponseObj(){};
	ResponseObj(ResponseData * responseData, CString status){ 
		_responseData = responseData;
		_status = status;
	} ;

	ResponseData * getResponseData(){ return _responseData ; }

	CString getStatus(){ return _status; }
    void fromJSON(const rapidjson::Value& doc, ResponseData * responseData) 
	{
		_responseData = responseData;
		
		_responseData->fromJSON(doc);
        CString status = doc["status"].GetString();
		_status = status;
		
        return;
	}


private :
	ResponseData * _responseData;
	CString  _status;
};
*/
