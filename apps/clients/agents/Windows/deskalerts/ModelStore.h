#pragma once

#include "IInclude.h"

#include "OptionConf.h"
#include <vector>

class CModelStore
{
public:
	CModelStore(void);
	~CModelStore(void);
public:
	int Parse(CString sPath = CString());
	void addModelListener(IModelListener* newListener);
	void removeModelListener(IModelListener* newListener);
	NodeALERTS& getModel(CString& propertyName);

private:
	NodeALERTS nodeALERTS;
};
