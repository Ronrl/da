#pragma once

#include "IInclude.h"
#include "AlertUtils.h"
#include <map>

typedef char* (*PEDGCN)(void);
typedef char* (*PEDGUN)(void);

class CPropertyChangeEvent: public IPropertyChangeEvent, IComponent
{
	CString propertyName;
	CString oldValueString;
	CString newValueString;

	LPVOID oldValue;
	LPVOID newValue;

public:
	CPropertyChangeEvent(CString& _propertyName,CString& _oldValue,CString& _newValue) : propertyName(_propertyName),
		oldValueString(_oldValue), newValueString(_newValue), oldValue(nullptr), newValue(nullptr)
	{
	}

	CPropertyChangeEvent(CString& _propertyName,LPVOID& _oldValue,LPVOID& _newValue)
	{
		propertyName = _propertyName;
		oldValue = _oldValue;
		newValue = _newValue;
	}

	CString& getPropertyName()
	{
		return propertyName;
	}
	CString& getOldValueString()
	{
		return oldValueString;
	}
	CString& getNewValueString()
	{
		return newValueString;
	}
	LPVOID getOldValue()
	{
		return oldValue;
	}
	LPVOID getNewValue()
	{
		return newValue;
	}
};


class CPropertyStore
{
public:
	CPropertyStore(void);
	~CPropertyStore(void);

public:
	void ADEDInit();
	void eDirectoryLoad();
	void loadProperty(void);

	void addPropertyListener(IPropertyListener* newListener);
	void removePropertyListener(IPropertyListener* newListener);

	//Property
	CString getPropertyString(CString& promertyName, CString *def = NULL, const int encode = 0, const bool fixURI = false);
	void setPropertyString(CString& propertyName, CString& promertyValue, BOOL replace = TRUE, BOOL common = FALSE);
	CString buildPropertyString(CString& propertyVal, const int encode = 0, const bool fixURI = false);

	DWORD getProperty(CString& propertyName);
	void setProperty(CString& propertyName, DWORD propertyValue, BOOL replace = TRUE);
	bool EnableActiveDirectory();
	bool EnableSyncFreeActiveDirectory();
	bool EnableEDirectory();
	bool EnableOtherDirectory();
	bool EnableRegistration();
	bool keyExists(CString key);

	void setRegistry(std::vector<NodeALERTS::NodeSETTINGS::NodeREGISTRY*> registry);
private:
	CString getAppDataPath(CString deskalertsId);
	static CString getDir(CString& param);
	std::map<CString,CString> loadPropertyFromRegistry(void);
	std::vector<IPropertyListener* > m_actionListeners;
	std::map<CString,CString> m_propString;
	std::map<CString,DWORD> m_propData;
	CComCriticalSection m_cs_propString; //define critical section

	std::vector<NodeALERTS::NodeSETTINGS::NodeREGISTRY*> m_propRegistry;

	PEDGCN eDirGetContextName;
	PEDGUN eDirGetUserName;

	BOOL m_ADEDInit;
	BOOL m_AD;
	BOOL m_AzureAD;
	BOOL m_SFAD;
	BOOL m_ED;
};
