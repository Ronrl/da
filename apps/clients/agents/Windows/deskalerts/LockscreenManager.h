#pragma once

class CLockscreenAlert
{
public:
	CString m_fileName;
	int m_autoclose;
	//CString m_position;
	INT64 m_id;
	CLockscreenAlert(void);
	~CLockscreenAlert(void);
};

#define _LOCKSCREENS_TIMER_	102

class CLockscreenManager : public CFrameWindowImpl<CLockscreenManager>
{
public:
	enum {
		UWM_STOP_THREAD = (WM_APP + 310),
		UTM_SET_LOCKSCREEN = (WM_APP + 311)
	};
	CLockscreenManager(void);
	~CLockscreenManager(void);

	BEGIN_MSG_MAP(CLockscreenManager)
		MESSAGE_HANDLER(WM_TIMER, OnTimer)
	END_MSG_MAP()

	LRESULT OnTimer(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	void showLockscreens();
private:
	struct LockscreenStruct {
		//CString position;
		CString fileName;
		BOOL absolutePath;
		LockscreenStruct() :absolutePath(NULL) {}
	};

	void setLockscreen(CString fileName, BOOL absolutePath = FALSE);
	void restorePreviousLockscreen();
	static DWORD WINAPI LockscreenThread(LPVOID pv);
	std::vector<CLockscreenAlert*>::iterator m_curIt;
	std::vector<CLockscreenAlert*> m_alerts;
	HANDLE m_thread;
	DWORD m_threadId;
};
