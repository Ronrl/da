#pragma once
#include "resource.h"
#include "AlertController.h"

class CAlertsDlg:
	public CPropertyPageImpl<CAlertsDlg>  
{
	CAlertController *pController;
	Localization *Locale;
	VirtualToolbar* tbCtrl;
public:
	enum { IDD = IDD_ALERTS_DLG };


	CAlertsDlg(Localization *pLocale, VirtualToolbar *tbCtrl, CAlertController *pController);
	virtual ~CAlertsDlg(void);


	BEGIN_MSG_MAP(CAlertsDlg)
		MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
		MESSAGE_HANDLER(WM_CLOSE, OnClose)
		COMMAND_CODE_HANDLER( EN_CHANGE , OnEditChange)
		CHAIN_MSG_MAP(CPropertyPageImpl<CAlertsDlg>)
	END_MSG_MAP()

	LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	BOOL OnApply();

	LRESULT OnEditChange(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
	{
		SetModified(TRUE);
		return 0;
	}
};
