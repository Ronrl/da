#pragma once


//--------------------------------------------------------------------------------------------------------
class IActionEvent
{
public:
	virtual CString& getEventName() = 0;
	virtual CString& getCommandName() = 0;
	virtual CString& getSourceName() = 0;
};
class IActionListener
{
public:
 	virtual void actionPerformed(IActionEvent& event) = 0;
};
//--------------------------------------------------------------------------------------------------------


//--------------------------------------------------------------------------------------------------------
class IAlertEvent
{
public:
	virtual CString& getFileName() = 0;
	virtual CString& getEventName() = 0;
	virtual CString& getSourceName() = 0;
};
class IAlertListener
{
public:
	virtual void alertUpdated(IActionEvent& event) = 0;
};
//--------------------------------------------------------------------------------------------------------


//--------------------------------------------------------------------------------------------------------
class IModelEvent
{
public:
	virtual CString& getEventName() = 0;
	virtual CString& getSourceName() = 0;
};

class IModelListener
{
public:
 	virtual void alertModelUpdated(IModelEvent& event) = 0;
};
//--------------------------------------------------------------------------------------------------------


//--------------------------------------------------------------------------------------------------------
class IPropertyChangeEvent
{
public:
	CString& getPropertyName();
	CString& getOldValueString();
	CString& getNewValueString();

	LPVOID getOldValue();
	LPVOID getNewValue();
};

class IPropertyListener
{
public:
	virtual void propertyChanged(IPropertyChangeEvent& propCh) = 0;
};
//--------------------------------------------------------------------------------------------------------
