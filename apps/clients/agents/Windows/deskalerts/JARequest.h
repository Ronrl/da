#pragma once
#include <stdexcept>

//#include "rapidjson/document.h"
#include "rapidjson/stringbuffer.h"
#include "JAResponse.h"


enum AuthorizationType { Domain, Computer, UserName };

class JARequest
{
public :
	JARequest(): _responseData(NULL), reqVERB(CAlertUtils::requestVerb::OLDcalcVerbByPostData){ };
	JARequest(ResponseData * responseData): reqVERB(CAlertUtils::requestVerb::OLDcalcVerbByPostData){ _responseData = responseData; };
	~JARequest(){};
    
	virtual  std::string toJSON()=0;
	virtual const CString getURL()=0;
	DWORD SendRequest();
    const int getVERB()
    {
        return reqVERB;
    }

	ResponseData * getResponseData(){ return _responseData ; };
private :
	ResponseData * _responseData;
protected:

    const void setVERB(int newVERB)
    {
        reqVERB = newVERB;
    }
    int reqVERB;
};

class JARequest_v2
{
public:
    JARequest_v2() : _responseData(NULL), reqVERB(CAlertUtils::requestVerb::OLDcalcVerbByPostData) { };
    JARequest_v2(ResponseData* responseData) : reqVERB(CAlertUtils::requestVerb::OLDcalcVerbByPostData) { _responseData = responseData; };
    ~JARequest_v2() {};

    virtual  std::string toJSON() = 0;
    virtual const CString getURL() = 0;
    DWORD SendRequest();
    const int getVERB()
    {
        return reqVERB;
    }

    ResponseData* getResponseData() { return _responseData; };
private:
    ResponseData* _responseData;
protected:

    const void setVERB(int newVERB)
    {
        reqVERB = newVERB;
    }
    int reqVERB;
};

