#pragma once
#include <stdexcept>
#include "rapidjson/document.h"

#define MD5_SIZE 32

class ResponseData
{
public:
	ResponseData(void){};
	~ResponseData(void){};
	virtual  ResponseData * fromJSON(const rapidjson::Value& doc) = 0;
};



class ResponseObj
{
public :
	ResponseObj(): _responseData(NULL){};
	~ResponseObj(){};
	ResponseObj(ResponseData * responseData, CString status){ 
		_responseData = responseData;
		_status = status;
	} ;

	ResponseData * getResponseData(){ return _responseData ; }

	CString getStatus(){ return _status; }
    void fromJSON(const rapidjson::Value& doc, ResponseData * responseData) 
	{
		_responseData = responseData;
		
		_responseData->fromJSON(doc);
        CString status = doc["status"].GetString();
		_status = status;
		
        return;/* (ResponseObj *)&result;*/    
	}
private :
	ResponseData * _responseData;
	CString  _status;
};




	




