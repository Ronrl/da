#include "StdAfx.h"
#include ".\eventhandler.h"

void CEventHandler::addMyDispatchListener( IMyDispatchListener* p )
{
	displists.push_back(p);
}

void CEventHandler::removeMyDispatchListener( IMyDispatchListener* p )
{
	std::vector<IMyDispatchListener*>::iterator it;
	it = displists.begin();
	while (it!=displists.end())
	{
		if ((*it)==p)
		{
			displists.erase(it);
			break;
		} else
			it++;
	}
}

HRESULT STDMETHODCALLTYPE CEventHandler::GetTypeInfoCount(
	/* [out] */ UINT* /*pctinfo*/)
{
	return E_NOTIMPL;
}

HRESULT STDMETHODCALLTYPE CEventHandler::GetTypeInfo(
	/* [in] */ UINT /*iTInfo*/,
	/* [in] */ LCID /*lcid*/,
	/* [out] */ ITypeInfo** /*ppTInfo*/)
{
	return E_NOTIMPL;
}

HRESULT STDMETHODCALLTYPE CEventHandler::GetIDsOfNames(
	/* [in] */ REFIID /*riid*/,
	/* [size_is][in] */ LPOLESTR *rgszNames,
	/* [in] */ UINT /*cNames*/,
	/* [in] */ LCID /*lcid*/,
	/* [size_is][out] */ DISPID *rgDispId)
{
	std::map<CString,int>::iterator it;
	it = nameId.find(CString(*rgszNames));
	if (it!=nameId.end())
	{
		*rgDispId = it->second;
	}
	else
	{
		idName.insert(std::pair<int,CString>(id,CString(*rgszNames)));
		nameId.insert(std::pair<CString,int>(CString(*rgszNames),id));
		*rgDispId = id;
		id++;
	}
	return S_OK;
}

HRESULT STDMETHODCALLTYPE CEventHandler::Invoke(
	/* [in] */ DISPID dispIdMember,
	/* [in] */ REFIID /*riid*/,
	/* [in] */ LCID /*lcid*/,
	/* [in] */ WORD /*wFlags*/,
	/* [out][in] */ DISPPARAMS *pDispParams,
	/* [out] */ VARIANT *pVarResult,
	/* [out] */ EXCEPINFO* /*pExcepInfo*/,
	/* [out] */ UINT* /*puArgErr*/)
{
	CString name;
	std::map<int,CString>::const_iterator itm = idName.find(dispIdMember);
	if (itm!=idName.end())
	{
		name = itm->second;
	}
	CComVariant *res = NULL;
	std::vector<IMyDispatchListener*>::iterator itv = displists.begin();
	while(itv!=displists.end())
	{
		CComVariant *tmpres = (*itv)->DispInvoke(name,pDispParams);
		if(tmpres)
		{
			if(res) delete_catch(res);
			res = tmpres;
		}
		itv++;
	}
	if(res!=NULL && pVarResult!=NULL)
	{
		res->Detach(pVarResult);
	}
	if(res) delete_catch(res);
	return S_OK;
}