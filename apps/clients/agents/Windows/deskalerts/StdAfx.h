//
//  stdafx.h
//  DeskAlerts Client
//  Common module
//
//  Copyright Softomate 2006-2009. All rights reserved.
//

#pragma once
#ifndef _HAS_TR1
#define _HAS_TR1 1
#endif

#include <memory>
//using namespace std::tr1;

#if !defined(AFX_STDAFX_H__1CCC2E75_F59B_4A7B_8F1E_8E89DF186C6B__INCLUDED_)
#define AFX_STDAFX_H__1CCC2E75_F59B_4A7B_8F1E_8E89DF186C6B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// ATL secured functions.
#define _SECURE_ATL 1

// STL errors handling.
// _SECURE_SCL=1 for enabled state, 0 for disabled.
// _SECURE_SCL_THROWS=1 for throwing exception, 0 for abnormal program termination.
#ifdef _SECURE_SCL
#undef _SECURE_SCL
#endif
#ifdef _SECURE_SCL_THROWS
#undef _SECURE_SCL_THROWS
#endif
#define _SECURE_SCL 1
#define _SECURE_SCL_THROWS 1

// Insert your headers here
#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers
#define _NORMAL_		(0x00)
#define _OFFLINE_		(0x01)
#define _STANDBY_		(0x02)
#define _DISABLE_		(0x04)
#define _DISABLE_NEW_	(0x08)
#define _MESSAGES_UNREAD_	(0x10)
#define _MESSAGES_UNREAD_AND_UNOBTRUSIVE_MODE	(0x14)

#ifndef release_try
#ifdef DEBUG
#define release_try {
#define release_end_try }
#define release_catch_all } if(0) {
#define release_catch_end }
#define release_catch_tolog(...) }
#define release_catch_expr_and_tolog(...) }
#define delete_catch(x) delete x; //use debug and/or brain!
#define deleteArray_catch(x) delete[] x;

#else
#define release_try try {
#define release_end_try } catch(...) {}
#define release_catch_all } catch(...) {
#define release_catch_end }
//#define release_catch_tolog(...) } catch(...) { appendToErrorFile(__VA_ARGS__); }
#define release_catch_tolog(...) } catch(...) { logException(); }
//#define release_catch_expr_and_tolog(x, ...) } catch(...) { x; appendToErrorFile(__VA_ARGS__); }
#define release_catch_expr_and_tolog(x, ...) } catch(...) { x; /*logException();*/ }
#define delete_catch(x) try{ delete x; } catch(...) {} //ha-ha-ha! we need to avoid stupid crashes.
#define deleteArray_catch(x) try{ delete[] x; } catch(...) {}
#endif
#endif

#define _WTL_NO_CSTRING
#include <atlstr.h>
#include <windows.h>
#include <wininet.h>
#include <stdlib.h>
#include <string.h>
#include <tchar.h>
#include <iads.h>
#include <Adshlp.h>
#include <sstream>

#pragma warning(push)
#pragma warning(disable:4100)
#include <ptl.h>
#include <abstract.h>
#pragma warning(pop)

#include <wtlincl.h>
#include <errors.h>
#include <process.h>

#include <atlbase.h>
#include <atlapp.h>

#include <atlhost.h>
#include <atlwin.h>
#include <atlctl.h>

#include <atlframe.h>
#include <atlctrls.h>
#include <atldlgs.h>


#include <atlmisc.h>

#include <atlutil.h>

using namespace PTL;
// debug info
#include "IPCTraceMacros.h"

#include "sqlite3.h"

#include "Constants.h"

#include "mvc.h"

#include "PostFunctions.h"

extern volatile LONG g_bDebugMode;

#define BEGINTHREADEX(lpsa, cbStack, lpStartAddr, lpvThreadParm, fdwCreate, lpIDThread)((HANDLE)_beginthreadex((void *) (lpsa),(unsigned) (cbStack),(unsigned int (__stdcall *)(void *)) (lpStartAddr),(void *) (lpvThreadParm),(unsigned) (fdwCreate),(unsigned *) (lpIDThread)))

extern inline DWORD WaitForSingleObjectWithMsgLoop(__in HANDLE hHandle, __in DWORD dwMilliseconds);

#endif // !defined(AFX_STDAFX_H__1CCC2E75_F59B_4A7B_8F1E_8E89DF186C6B__INCLUDED_)

#define DeskAlertsAssert(expr)  if (!(expr)){int *qwe =0; *qwe =123213;}else{}
#define DeskAlertsUnexpected() DeskAlertsAssert(0)

#define DISABLE_API_V1_USER_ALERTS