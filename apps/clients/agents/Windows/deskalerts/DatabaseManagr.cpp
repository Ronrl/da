//
//  DatabaseManagr.cpp
//  DeskAlerts Client
//  Common module
//
//  Copyright Softomate 2006-2009. All rights reserved.
//

#include "stdafx.h"
#include "DatabaseManagr.h"
#include "AlertUtils.h"
#include "mvc.h"
#include "alertloader.h"
#include <math.h>
#include "WallpaperAlert.h"
#include "LockscreenManager.h"
#include <algorithm>


CDatabaseManager::CDatabaseManager(BOOL common)
{
	m_database = NULL;
	CString dbPath = CAlertUtils::databasePath(common);
	OpenDatabase(dbPath);
}

CDatabaseManager::~CDatabaseManager()
{
	CloseDatabase();
}

CComAutoCriticalSection g_cs_shared_db;
CDatabaseManager* CDatabaseManager::shared(bool create, BOOL common)
{
	if (_iAlertController && (_iAlertController->getInitFlags() & NO_DATABASE))
	{
		return NULL;
	}
	static CDatabaseManager *dbm = NULL;
	static CDatabaseManager *dbmCommon = NULL;
	if(create)
	{
		g_cs_shared_db.Lock();
		release_try
		{
			//if (common && !dbmCommon)
			if (common )
			{
				if (  !dbmCommon)
					dbmCommon = new CDatabaseManager(common);
			}
			else if (!dbm)
			{
				dbm = new CDatabaseManager();
			}
		}
		release_catch_tolog(_T("Get shared DB exception"))
		g_cs_shared_db.Unlock();
	}

	if (common) {return dbmCommon;}
	else return dbm;
}


void stripTags(sqlite3_context* cx, int /*cnt*/, sqlite3_value** v)
{
	CString str = CA2T(reinterpret_cast<const char*>(sqlite3_value_text(v[0])), CP_UTF8);
	str = CAlertUtils::stripHtmlTags(str);
	sqlite3_result_text(cx,CT2A(str.GetBuffer()),str.GetLength()*sizeof(TCHAR),SQLITE_TRANSIENT);
}

void onWallpaperDelete(sqlite3_context* /*cx*/, int /*cnt*/, sqlite3_value** v)
{
	CString filePath = CA2T(reinterpret_cast<const char*>(sqlite3_value_text(v[0])), CP_UTF8);
	filePath = CAlertUtils::getWallpapersPath() + _T("\\") + filePath;
	DeleteFile(filePath);
}

// handle event "delete lockscreen"
void onLockscreenDelete(sqlite3_context* /*cx*/, int /*cnt*/, sqlite3_value** v)
{
	CString filePath = CA2T(reinterpret_cast<const char*>(sqlite3_value_text(v[0])), CP_UTF8);
	filePath = CAlertUtils::getLockscreensPath() + _T("\\") + filePath;
	DeleteFile(filePath);
}

// handle event "delete screensaver"
void onScreensaverDelete(sqlite3_context* /*cx*/, int /*cnt*/, sqlite3_value** v) {
	//CString html = CA2T(reinterpret_cast<const char*>(sqlite3_value_text(v[0])), CP_UTF8);
	//CString filePath = CAlertUtils::ExtractFilePath(html, "file=\"", '\"');
	//DeleteFile(filePath);
}

void CDatabaseManager::OpenDatabase(CString sPath)
{
	int ret = SQLITE_FAIL;
	m_cs_db.Lock();
	release_try
	{
		ret = sqlite3_open16((void*)sPath.GetString(), &m_database);
	}
	release_catch_tolog(_T("Open DB exception"))
	m_cs_db.Unlock();

	if(SQLITE_OK != ret)
	{
		m_cs_db.Lock();
		release_try
		{
			//logDbError(L"Problems with opening database: %s. \nsqlite error: %s", sPath, CA2T(sqlite3_errmsg(m_database), CP_UTF8));
		}
		release_end_try
		m_cs_db.Unlock();
		m_database = NULL;
	}
	else
	{
		if(!IsTableExists(_T("history")))
		{
			if(!InitDatabase())
			{
				m_cs_db.Lock();
				release_try
				{
					//logDbError(L"Database initialisation failed! \nsqlite error: %s", CA2T(sqlite3_errmsg(m_database), CP_UTF8));
				}
				release_end_try
				m_cs_db.Unlock();
			}
		}
		else
		{
			UpdateTables();
		}
	}
}

void CDatabaseManager::CloseDatabase()
{
	ATLASSERT(m_database);
	if(m_database)
	{
		m_cs_db.Lock();
		release_try
		{
			sqlite3_close(m_database);
		}
		release_catch_tolog(_T("Close DB exception"))
		m_cs_db.Unlock();
	}
	m_database = NULL;
}

BOOL CDatabaseManager::IsTableExists(CString table_name)
{
	BOOL res = FALSE;
	ATLASSERT(m_database);
	if(m_database)
	{
		m_cs_db.Lock();
		release_try
		{
			table_name.Replace(_T("`"), _T("``"));
			CString SQLTest;
			SQLTest.Format(_T("SELECT name FROM sqlite_master WHERE (type='table' OR type='view') AND name = '%s'"), table_name);

			sqlite3_stmt* stmt;

			if (SQLITE_OK == sqlite3_prepare_v2(m_database, CT2A(SQLTest, CP_UTF8), -1, &stmt, 0))
			{
				if(sqlite3_step(stmt) == SQLITE_ROW)
				{
					res = TRUE;
				}
			}
		}
		release_catch_tolog(_T("IsDatabaseValid exception"));
		m_cs_db.Unlock();
	}
	return res;
}

BOOL CDatabaseManager::InitDatabase()
{
	DropTables();
	const BOOL res = CreateTables();
	UpdateTables();
	return res;
}

void CDatabaseManager::DropTables()
{
	ATLASSERT(m_database);
	if(m_database)
	{
		m_cs_db.Lock();
		release_try
		{
			TCHAR SQLDropTable[] = _T("DROP TABLE `history`;     \
                                       DROP TABLE `received`;    \
                                       DROP TABLE `downloaded`;  \
									   DROP TABLE `property`;");
			sqlite3_exec(m_database,CT2A(SQLDropTable),NULL,NULL,NULL);
		}
		release_catch_tolog(_T("DropTables exception"))
		m_cs_db.Unlock();
	}
}

BOOL CDatabaseManager::CreateTables()
{
	BOOL res = TRUE;
	ATLASSERT(m_database);
	if(m_database)
	{
		m_cs_db.Lock();
		release_try
		{
			TCHAR SQLCreateTables[] =
				_T("\
					CREATE TABLE IF NOT EXISTS `history`(		\
						`id` INTEGER primary key AUTOINCREMENT,	\
						`alert` TEXT,							\
						`date` DATETIME,						\
						`title` TEXT,							\
						`acknow` BOOLEAN,						\
						`create_date` DATETIME,					\
						`window` TEXT,							\
						`top` TEXT,								\
						`bottom` TEXT,							\
						`alert_id` INTEGER,						\
						`user_id` INTEGER,						\
						`width` TEXT,							\
						`height` TEXT,							\
						`position` TEXT,						\
						`visible` TEXT,							\
						`autoclose` INTEGER,					\
						`cdata` TEXT,							\
						`utf8` BOOL,							\
						`ticker` BOOL,							\
						`survey` BOOL,							\
						`schedule` BOOL,						\
						`recurrence` BOOL,						\
						`urgent` BOOL,							\
						`manual_closed` BOOL,					\
						`class` INTEGER DEFAULT 1,				\
						`unread` INTEGER DEFAULT 1,				\
						`resizable` BOOL DEFAULT 0,				\
                        `deleted` BOOL DEFAULT 0,               \
						`to_date` DATETIME						\
					);											\
					CREATE TABLE IF NOT EXISTS `property`(		\
						`id` INTEGER primary key AUTOINCREMENT,	\
						`name` TEXT,							\
						`value` TEXT							\
					);											\
					CREATE TABLE IF NOT EXISTS `groups`(		\
						`id` INTEGER primary key AUTOINCREMENT,	\
						`name` TEXT								\
					);											\
					CREATE TABLE IF NOT EXISTS `alertId_hWnds`(	\
						`alertId` INTEGER PRIMARY KEY,			\
						`hWndTools` INTEGER,					\
						`hWndBody` INTEGER						\
					);											\
					CREATE TABLE IF NOT EXISTS `received`(      \
						`alert_id` INTEGER PRIMARY KEY,         \
						`showed` BOOL DEFAULT 0,                \
						`body` TEXT                             \
					);                                          \
					CREATE TABLE IF NOT EXISTS `downloaded`(    \
						`alert_id` INTEGER PRIMARY KEY,         \
						`content` TEXT                          \
					);                                          \
					"
				);
			res = (SQLITE_OK == sqlite3_exec(m_database,CT2A(SQLCreateTables),NULL,NULL,NULL));
			//if(!res)
				//logDbError(L"CreateTables failed. \nsqlite error: %s", CA2T(sqlite3_errmsg(m_database), CP_UTF8));
		}
		release_catch_tolog(_T("CreateTables exception"))
		m_cs_db.Unlock();
	}
	return res;
}

BOOL CDatabaseManager::IsColumnExists(CString column_name, CString table_name)
{
	BOOL res = FALSE;
	ATLASSERT(m_database);
	if(m_database)
	{
		m_cs_db.Lock();
		release_try
		{
			column_name.Replace(_T("`"), _T("``"));
			table_name.Replace(_T("`"), _T("``"));
			CString SQLTest;
			SQLTest.Format(_T("SELECT `%s` FROM %s LIMIT 1"), column_name, table_name);
			res = (SQLITE_OK == sqlite3_exec(m_database,CT2A(SQLTest),NULL,NULL,NULL));
		}
		release_catch_tolog(_T("Failed column %s in %s checking"), column_name, table_name);
		m_cs_db.Unlock();
	}
	return res;
}

BOOL CDatabaseManager::AddColumnIfNotExists(CString column_name, CString table_name, CString type, CString def)
{
	BOOL res = FALSE;
	ATLASSERT(m_database);
	if(m_database)
	{
		res = IsColumnExists(column_name, table_name);
		if(!res)
		{
			m_cs_db.Lock();
			release_try
			{
				column_name.Replace(_T("`"), _T("``"));
				table_name.Replace(_T("`"), _T("``"));
				CString SQLQuery;
				if(!def.IsEmpty())
				{
					def = _T("DEFAULT ") + def;
				}
				SQLQuery.Format(_T("ALTER TABLE `%s` ADD `%s` %s %s"), table_name, column_name, type, def);
				res = (SQLITE_OK == sqlite3_exec(m_database,CT2A(SQLQuery),NULL,NULL,NULL));
			}
			release_catch_tolog(_T("Failed to add %s column to %s table"), column_name, table_name);
			m_cs_db.Unlock();
		}
	}
	return res;
}

void CDatabaseManager::UpdateTables()
{
	ATLASSERT(m_database);
	if(m_database)
	{
		release_try
		{
			m_cs_db.Lock();
			release_try
			{
				sqlite3_create_function(m_database,"stripTags",1,SQLITE_ANY,0,&stripTags,0,0);
				sqlite3_create_function(m_database,"onWallpaperDelete",1,SQLITE_ANY,0,&onWallpaperDelete,0,0);
				sqlite3_create_function(m_database,"onScreensaverDelete",1,SQLITE_ANY,0,&onScreensaverDelete,0,0);
				sqlite3_create_function(m_database, "onLockscreenDelete", 1, SQLITE_ANY, 0, &onLockscreenDelete, 0, 0);
			}
			release_catch_tolog(_T("DB function creating exception"))
			m_cs_db.Unlock();

			AddColumnIfNotExists(_T("title"), _T("history"), _T("TEXT"));
			AddColumnIfNotExists(_T("acknow"), _T("history"), _T("BOOL"));
			if(!IsColumnExists(_T("create_date"), _T("history")))
			{
				m_cs_db.Lock();
				release_try
				{
					TCHAR SQLUpdateTable[] = _T("ALTER TABLE `history` ADD `create_date` DATETIME;");
					sqlite3_exec(m_database,CT2A(SQLUpdateTable),NULL,NULL,NULL);
					if(IsColumnExists(_T("create"), _T("history")))
					{
						TCHAR SQLUpdateTable1[] = _T("UPDATE `history` SET `create_date`=datetime(`create`,'unixepoch');");
						sqlite3_exec(m_database,CT2A(SQLUpdateTable1),NULL,NULL,NULL);

						//TCHAR SQLUpdateTable2[] = _T("ALTER TABLE `history` DROP COLUMN `create`;");
						//sqlite3_exec(m_Database,CT2A(SQLUpdateTable2),NULL,NULL,NULL);
					}
				}
				release_catch_tolog(_T("DB history creating exception"))
				m_cs_db.Unlock();
			}
			AddColumnIfNotExists(_T("window"), _T("history"), _T("TEXT"));
			AddColumnIfNotExists(_T("top"), _T("history"), _T("TEXT"));
			AddColumnIfNotExists(_T("bottom"), _T("history"), _T("TEXT"));
			AddColumnIfNotExists(_T("alert_id"), _T("history"), _T("INTEGER"));
			AddColumnIfNotExists(_T("user_id"), _T("history"), _T("INTEGER"));
			AddColumnIfNotExists(_T("width"), _T("history"), _T("TEXT"));
			AddColumnIfNotExists(_T("height"), _T("history"), _T("TEXT"));
			AddColumnIfNotExists(_T("position"), _T("history"), _T("TEXT"));
			AddColumnIfNotExists(_T("visible"), _T("history"), _T("TEXT"));
			AddColumnIfNotExists(_T("autoclose"), _T("history"), _T("INTEGER"));
			AddColumnIfNotExists(_T("cdata"), _T("history"), _T("TEXT"));
			AddColumnIfNotExists(_T("utf8"), _T("history"), _T("BOOL"));
			AddColumnIfNotExists(_T("ticker"), _T("history"), _T("BOOL"));
			AddColumnIfNotExists(_T("survey"), _T("history"), _T("BOOL"));
			AddColumnIfNotExists(_T("schedule"), _T("history"), _T("BOOL"));
			AddColumnIfNotExists(_T("recurrence"), _T("history"), _T("BOOL"));
			AddColumnIfNotExists(_T("urgent"), _T("history"), _T("BOOL"));
			AddColumnIfNotExists(_T("class"), _T("history"), _T("INTEGER"), _T("1"));
			AddColumnIfNotExists(_T("unread"), _T("history"), _T("INTEGER"), _T("0"));
			AddColumnIfNotExists(_T("resizable"), _T("history"), _T("BOOL"), _T("0"));
            AddColumnIfNotExists(_T("deleted"), _T("history"), _T("BOOL"), _T("0"));
			AddColumnIfNotExists(_T("to_date"), _T("history"), _T("DATETIME"));
			if(!IsColumnExists(_T("manual_closed"), _T("history")))
			{
				m_cs_db.Lock();
				release_try
				{
					TCHAR SQLUpdateTable[] = _T("ALTER TABLE `history` ADD `manual_closed` BOOL");
					sqlite3_exec(m_database,CT2A(SQLUpdateTable),NULL,NULL,NULL);

					TCHAR SQLUpdateTable1[] = _T("UPDATE `history` SET `manual_closed`=1");
					sqlite3_exec(m_database,CT2A(SQLUpdateTable1),NULL,NULL,NULL);
				}
				release_catch_tolog(_T("DB manual_closed creating exception"))
				m_cs_db.Unlock();
			}

			m_cs_db.Lock();
			release_try
			{
				CString req = _T("CREATE INDEX IF NOT EXISTS IX_manual_closed_class_date ON history (manual_closed, class, date)");
				sqlite3_stmt* stmt;

				int ret = sqlite3_prepare_v2(m_database, CT2A(req, CP_UTF8), -1, &stmt, 0);
				if (ret == SQLITE_OK)
				{
					ret = sqlite3_step(stmt);
				}
			}
			release_catch_tolog(_T("DB manual_closed index creating exception"))
			m_cs_db.Unlock();

			if(!IsTableExists(_T("property")))
			{
				m_cs_db.Lock();
				release_try
				{
					TCHAR SQLCreateTable[] =
						_T("\
							CREATE TABLE IF NOT EXISTS `property`(	\
							`id` INTEGER primary key AUTOINCREMENT,	\
							`name` TEXT,							\
							`value` TEXT							\
							);"
						);
					sqlite3_exec(m_database,CT2A(SQLCreateTable),NULL,NULL,NULL);
				}
				release_catch_tolog(_T("DB property creating exception"))
				m_cs_db.Unlock();
			}

			m_cs_db.Unlock();

			if(!IsTableExists(_T("groups")))
			{
				m_cs_db.Lock();
				release_try
				{
					TCHAR SQLCreateTable[] =
						_T("\
							CREATE TABLE IF NOT EXISTS `groups`(		\
								`id` INTEGER primary key AUTOINCREMENT,		\
								`name` TEXT								\
							);"
						);
					sqlite3_exec(m_database,CT2A(SQLCreateTable),NULL,NULL,NULL);
				}
				release_catch_tolog(_T("DB groups creating exception"))
				m_cs_db.Unlock();
			}


			if(!IsTableExists(_T("history_content")))
			{
				m_cs_db.Lock();
				release_try
				{
					TCHAR SQLCreateTable[] =_T("CREATE TABLE history_ids (history_id INTEGER, UNIQUE(history_id))");
					int ret = sqlite3_exec(m_database,CT2A(SQLCreateTable),NULL,NULL,NULL);
					CString a =sqlite3_errmsg(m_database);
					TCHAR SQLCreateVirtualTable[] =_T("CREATE VIRTUAL TABLE history_data USING fts3(content,title);");
					ret = sqlite3_exec(m_database,CT2A(SQLCreateVirtualTable),NULL,NULL,NULL);
					a =sqlite3_errmsg(m_database);
					TCHAR SQLCreateView[] =_T("CREATE VIEW history_content AS SELECT history_ids.rowid AS rowid, history_id, content, title\
														FROM history_ids JOIN history_data ON history_ids.rowid = history_data.rowid;");
					ret = sqlite3_exec(m_database,CT2A(SQLCreateView),NULL,NULL,NULL);
					a =sqlite3_errmsg(m_database);
					TCHAR SQLCreateInsertDataTrigger[] =
						_T("\
							CREATE TRIGGER history_content_insert INSTEAD OF INSERT ON history_content						\
							BEGIN																											\
								INSERT INTO history_ids (history_id) VALUES (NEW.history_id);							\
								INSERT INTO history_data (rowid, content, title) VALUES (last_insert_rowid(), NEW.content, NEW.title);	\
							END																												\
							"
							);
					ret = sqlite3_exec(m_database,CT2A(SQLCreateInsertDataTrigger),NULL,NULL,NULL);
					TCHAR SQLCreateDeleteDataTrigger[] =
						_T("\
							CREATE TRIGGER history_content_delete INSTEAD OF DELETE ON history_content						\
							BEGIN																							\
								DELETE FROM history_ids WHERE rowid = OLD.rowid;											\
								DELETE FROM history_data WHERE rowid = OLD.rowid;											\
							END																								\
							"
							);
					ret = sqlite3_exec(m_database,CT2A(SQLCreateDeleteDataTrigger),NULL,NULL,NULL);

					CString req;
					sqlite3_stmt* stmt = NULL;
					CString checkReq = _T("SELECT 1 FROM `property` WHERE `name` = 'history_count' LIMIT 1");
					ret = sqlite3_prepare_v2(m_database, CT2A(checkReq, CP_UTF8), -1, &stmt, 0);
					if(ret == SQLITE_OK)
					{
						if(sqlite3_step(stmt) == SQLITE_ROW)
						{
							req = _T("PRAGMA synchronous = OFF; UPDATE `property` SET `value`= (SELECT count(1) FROM history WHERE `deleted` = 0) WHERE `name` == 'history_count'");
						}
						else
						{
							req = _T("PRAGMA synchronous = OFF; INSERT INTO `property` (`name`, `value`) VALUES('history_count', (SELECT count(1) FROM history WHERE `deleted` = 0))");
						}
					}
					else
					{
						//logDbError(L"Problems checking property on set. \nsqlite error: %s", CA2T(sqlite3_errmsg(m_database), CP_UTF8));
					}
					sqlite3_finalize(stmt);
					ret = sqlite3_exec(m_database,CT2A(req, CP_UTF8),NULL,NULL,NULL);

					ret = sqlite3_exec(m_database,"PRAGMA synchronous = OFF; INSERT INTO history_content (history_id, content, title) SELECT id, stripTags(alert), title FROM history",NULL,NULL,NULL);
				}
				release_catch_tolog(_T("DB history_content creating exception"))
				m_cs_db.Unlock();
			}

			if (!IsTableExists(_T("received")))
			{
				m_cs_db.Lock();
				release_try
				{
					TCHAR SQLCreateTable[] =
						_T("\
						CREATE TABLE IF NOT EXISTS `received`(  \
							`alert_id` INTEGER PRIMARY KEY, \
							`showed` BOOL DEFAULT 0,        \
							`body` TEXT                     \
						)");
					sqlite3_exec(m_database,CT2A(SQLCreateTable),NULL,NULL,NULL);
				}
				release_catch_tolog(_T("DB TABLE received creating exception"))
				m_cs_db.Unlock();
			}

            if (!IsTableExists(_T("downloaded")))
            {
                m_cs_db.Lock();
                release_try
                {
                    TCHAR SQLCreateTable[] =
                        _T("\
						CREATE TABLE IF NOT EXISTS `downloaded`(    \
						`alert_id` INTEGER PRIMARY KEY,         \
						`content` TEXT                          \
					    )");
                    sqlite3_exec(m_database,CT2A(SQLCreateTable),NULL,NULL,NULL);
                }
                    release_catch_tolog(_T("DB TABLE downloaded creating exception"))
                    m_cs_db.Unlock();
            }
			m_cs_db.Lock();
			release_try
			{
				TCHAR SQLRemoveInsertAlertTrigger[] =_T("DROP TRIGGER IF EXISTS insert_history_trigger;");
				int ret = sqlite3_exec(m_database,CT2A(SQLRemoveInsertAlertTrigger),NULL,NULL,NULL);
				TCHAR SQLCreateInsertAlertTrigger[] =
					_T("\
					   CREATE TRIGGER IF NOT EXISTS insert_history_trigger AFTER INSERT ON history FOR EACH ROW WHEN NEW.class<>8 and NEW.class<>4096 \
					   BEGIN																												\
					   UPDATE property SET value = CAST(value AS INTEGER) + 1 WHERE name='history_count';									\
					   INSERT INTO history_content (history_id, content, title) VALUES(NEW.id, stripTags(NEW.alert), NEW.title);			\
					   END																													\
					   "
					   );
				ret = sqlite3_exec(m_database,CT2A(SQLCreateInsertAlertTrigger),NULL,NULL,NULL);

				TCHAR SQLRemoveInsertWallpaperTrigger[] = _T("DROP TRIGGER IF EXISTS insert_wallpaper_trigger;");
				ret = sqlite3_exec(m_database, CT2A(SQLRemoveInsertWallpaperTrigger), NULL, NULL, NULL);

				TCHAR SQLCreateInsertWallpaperTrigger[] =
					_T("\
					   CREATE TRIGGER IF NOT EXISTS insert_wallpaper_trigger AFTER INSERT ON history FOR EACH ROW WHEN NEW.class=8 or NEW.class=4096 \
					   BEGIN																												\
						UPDATE property SET value = CAST(value AS INTEGER) + 1 WHERE name='history_count';									\
						INSERT INTO history_content (history_id, content, title) VALUES(NEW.id,'', NEW.title);								\
					   END																													\
					   "
					   );
				ret = sqlite3_exec(m_database,CT2A(SQLCreateInsertWallpaperTrigger),NULL,NULL,NULL);

				TCHAR SQLRemoveDeleteAlertTrigger[] =_T("DROP TRIGGER IF EXISTS delete_history_trigger;");
				ret = sqlite3_exec(m_database,CT2A(SQLRemoveDeleteAlertTrigger),NULL,NULL,NULL);
				TCHAR SQLCreateDeleteAlertTrigger[] =
					_T("\
					   CREATE TRIGGER IF NOT EXISTS delete_history_trigger AFTER DELETE ON history FOR EACH ROW					    \
					   BEGIN																										\
					   UPDATE property SET value = CAST(value AS INTEGER) - 1 WHERE name='history_count';							\
					   DELETE FROM history_content WHERE OLD.id = history_id;														\
					   END																											\
				       "
				);
				ret = sqlite3_exec(m_database,CT2A(SQLCreateDeleteAlertTrigger),NULL,NULL,NULL);

				TCHAR SQLRemoveDeleteWallpaperTrigger[] =_T("DROP TRIGGER IF EXISTS delete_wallpaper_trigger;");
				ret = sqlite3_exec(m_database,CT2A(SQLRemoveDeleteWallpaperTrigger),NULL,NULL,NULL);
				TCHAR SQLCreateDeleteWallpaperTrigger[] =
					_T("\
						CREATE TRIGGER IF NOT EXISTS delete_wallpaper_trigger AFTER DELETE ON history FOR EACH ROW WHEN OLD.class = 8			\
						BEGIN																													\
							SELECT onWallpaperDelete(OLD.alert);																				\
						END																														\
					");
				ret = sqlite3_exec(m_database,CT2A(SQLCreateDeleteWallpaperTrigger),NULL,NULL,NULL);

				TCHAR SQLRemoveDeleteLockscreenTrigger[] = _T("DROP TRIGGER IF EXISTS delete_lockscreen_trigger;");
				ret = sqlite3_exec(m_database, CT2A(SQLRemoveDeleteLockscreenTrigger), NULL, NULL, NULL);
				TCHAR SQLCreateDeleteLockscreenTrigger[] =
					_T("\
						CREATE TRIGGER IF NOT EXISTS delete_lockscreen_trigger AFTER DELETE ON history FOR EACH ROW WHEN OLD.class = 4096			\
						BEGIN																													\
							SELECT onLockscreenDelete(OLD.alert);																				\
						END																														\
					");
				ret = sqlite3_exec(m_database, CT2A(SQLCreateDeleteLockscreenTrigger), NULL, NULL, NULL);


				// Set trigger on screensavers deleting
				TCHAR SQLRemoveDeleteScreensaverTrigger[] = _TEXT("DROP TRIGGER IF EXISTS delete_screensaver_trigger");
				ret = sqlite3_exec(m_database, CT2A(SQLRemoveDeleteScreensaverTrigger), NULL, NULL, NULL);
				TCHAR SQLCreateDeleteScreensaverTrigger[] = 
					_TEXT("																													\
							CREATE TRIGGER IF NOT EXISTS delete_screensaver_trigger AFTER DELETE ON history FOR EACH ROW WHEN OLD.class = 2	\
							BEGIN																											\
								SELECT onScreensaverDelete(OLD.alert);																		\
							END																												\
						  ");
				ret = sqlite3_exec(m_database, CT2A(SQLCreateDeleteScreensaverTrigger), NULL, NULL, NULL);
				if (ret == 0)
					ret = 0;
				/*
					TODO: Add trigger on screensaver delete like above
				*/
			}
			release_catch_tolog(_T("DB trigers creating exception"))
			m_cs_db.Unlock();
		}


		if (!IsTableExists(_T("alertId_hWnds")))
		{
			m_cs_db.Lock();
			release_try
			{
				TCHAR SQLCreateTable[] =
					_T("\
						CREATE TABLE IF NOT EXISTS `alertId_hWnds`(	\
							`alertId` INTEGER PRIMARY KEY,			\
							`hWndTools` INTEGER,					\
							`hWndBody` INTEGER						\
							);"
					);
				sqlite3_exec(m_database,CT2A(SQLCreateTable),NULL,NULL,NULL);
			}
			release_catch_tolog(_T("DB table creating exception"))
			m_cs_db.Unlock();
		}
		release_catch_tolog(_T("DB UpdateTables exception"))
	}
}

HRESULT CDatabaseManager::GetFormatedDateTime(CString _sDate, CString& sResponse, CString sFormat, BOOL UTC)
{
	ATLASSERT(m_database);
	if (m_database)
	{
		m_cs_db.Lock();
		release_try
		{
			CString req = _T("SELECT strftime('") + sFormat + _T("', ") + _sDate + _T(", 'unixepoch'");
			if(UTC) req += _T(", 'localtime'");
			req += _T(");");

			sqlite3_stmt* stmt;

			int ret = sqlite3_prepare_v2(m_database, CT2A(req, CP_UTF8), -1, &stmt, 0);
			if (ret == SQLITE_OK)
			{
				if (sqlite3_step(stmt) == SQLITE_ROW)
				{
					sResponse = CA2T((char*)sqlite3_column_text(stmt, 0), CP_UTF8);
				}
			}
			sqlite3_finalize(stmt);
		}
		release_catch_tolog(_T("DB GetFormatedDateTime exception"))
		m_cs_db.Unlock();
	}
	return S_OK;
}

HRESULT CDatabaseManager::GetHistory(CString _sDate, CString& sResponse, CString sFormat)
{
	HRESULT hr = S_OK;
	ATLASSERT(m_database);
	if (m_database)
	{
		m_cs_db.Lock();
		release_try
		{
			CString req = _T("SELECT DISTINCT `id`, `top`, `alert`, `bottom`, strftime('") + sFormat 
				+ _T("', `date`), `title`, `acknow`, strftime('") + sFormat 
				+ _T("', `create_date`), strftime('") + sFormat 
				+ _T("', `to_date`)  FROM `history` WHERE `deleted` = 0 ORDER BY `id` DESC;");

			sqlite3_stmt* stmt;

			int ret = sqlite3_prepare_v2(m_database, CT2A(req, CP_UTF8), -1, &stmt, 0);
			if (ret == SQLITE_OK)
			{
				while (sqlite3_step(stmt) == SQLITE_ROW)
				{
					CString sId, sAlert, sDate, sTitle, sAcknow, sCreate, sToDate;
					sId = CA2T((char*)sqlite3_column_text(stmt, 0), CP_UTF8);
					sAlert = CAlertUtils::escapeJSON((TCHAR*)CA2T((char*)sqlite3_column_text(stmt, 1), CP_UTF8));
					sAlert += CAlertUtils::escapeJSON((TCHAR*)CA2T((char*)sqlite3_column_text(stmt, 2), CP_UTF8));
					sAlert += CAlertUtils::escapeJSON((TCHAR*)CA2T((char*)sqlite3_column_text(stmt, 3), CP_UTF8));
					sDate = CA2T((char*)sqlite3_column_text(stmt, 4), CP_UTF8);
					sTitle = CAlertUtils::escapeJSON((TCHAR*)CA2T((char*)sqlite3_column_text(stmt, 5), CP_UTF8));
					sAcknow = CA2T((char*)sqlite3_column_text(stmt, 6), CP_UTF8);
					sCreate = CA2T((char*)sqlite3_column_text(stmt, 7),CP_UTF8);
					sToDate = CA2T((char*)sqlite3_column_text(stmt, 8),CP_UTF8);
                    sId =       CString("\"id\":")        + sId;
                    sDate =     CString("\"date\":\"")    + sDate   + CString("\"");
                    sAlert =    CString("\"alert\":\"")   + sAlert  + CString("\"");
                    sTitle =    CString("\"title\":\"")   + sTitle  + CString("\"");
                    sAcknow =   CString("\"acknow\":")    + sAcknow;
                    sCreate =   CString("\"create\":\"")  + sCreate + CString("\"");
                    sToDate =   CString("\"to_date\":\"") + sToDate + CString("\"");
                    CString StringJSONobj;
                    StringJSONobj += CString("{");
                    StringJSONobj += sId     + CString(",");
                    StringJSONobj += sDate   + CString(",");
                    StringJSONobj += sAlert  + CString(",");
                    StringJSONobj += sAcknow + CString(",");
                    StringJSONobj += sTitle  + CString(",");
                    StringJSONobj += sCreate + CString(",");
                    StringJSONobj += sToDate;
                    StringJSONobj += CString("},");
                    sResponse += StringJSONobj;
				}
				sResponse.TrimRight(_T(","));
				sResponse = CString("([") + sResponse + CString("]);");
			}
			else
			{
				hr = E_FAIL;
			}
			sqlite3_finalize(stmt);
		}
		release_catch_tolog(_T("DB GetHistory exception"))
		m_cs_db.Unlock();
	}
	return hr;
}

HRESULT CDatabaseManager::getHistoryRangeFromDb(CString _sDate,CString *sResponse, CString sFormat, sqlite_int64 startPos, sqlite_int64 count, CString searchTerm)
{
	HRESULT hr = S_OK;
	ATLASSERT(m_database);
	sqlite_int64 rowsCount = 0;
	CString searchSQL;
	sqlite3_stmt* stmt = NULL;
	searchTerm.Replace(_T("'"), _T("''"));
	if (m_database)
	{
		m_cs_db.Lock();
		release_try
		{
			int ret;

			sqlite3_exec(m_database, "PRAGMA synchronous = OFF", NULL, NULL, NULL);

			if (searchTerm != "")
			{
				CString countReq;
				countReq.Format(_T("SELECT count(h.rowid) \
                                      FROM history_content hc, history h \
                                     WHERE h.id = hc.history_id and h.deleted = 0 and \
                                           (hc.content LIKE '%%%s%%' OR hc.title LIKE '%%%s%%') "),searchTerm, searchTerm);
			
				ret = sqlite3_prepare_v2(m_database, CT2A(countReq, CP_UTF8), -1, &stmt, 0);
				if(ret == SQLITE_OK && stmt)
				{
					if(sqlite3_step(stmt) == SQLITE_ROW)
					{
						rowsCount = sqlite3_column_int64(stmt, 0);
					}
				}
				else
				{
					//logDbError(L"Problems get count of history. \nsqlite error: %s", CA2T(sqlite3_errmsg(m_database), CP_UTF8));
				}
				sqlite3_finalize(stmt);
			}
			else
			{
				rowsCount = _ttoi(getProperty("history_count"));
			}

			if (!searchTerm.IsEmpty())
			{
				searchSQL.Format(_T("WHERE deleted = 0 AND id IN (SELECT history_id FROM history_content WHERE content LIKE '%%%s%%' OR title LIKE '%%%s%%')"),searchTerm, searchTerm);
			}
			else
			{
				searchSQL.Format(_T("WHERE deleted = 0"));
			}

				
			CString req;
			req.Format(_T("SELECT								\n\
							`id`,								\n\
							`top`,								\n\
							`alert`,							\n\
							`bottom`,							\n\
							strftime('%s',`date`),				\n\
							`title`,							\n\
							`acknow`,							\n\
							strftime('%s',`create_date`),		\n\
							class,								\n\
							urgent,								\n\
							unread,								\n\
							position,							\n\
							ticker,								\n\
							strftime('%s',`to_date`)		\n\
							FROM `history`						\n\
							%s									\n\
							ORDER BY id DESC LIMIT %I64d OFFSET %I64d	\n\
						"),sFormat,sFormat,sFormat,searchSQL,count,startPos);

			ret = sqlite3_prepare_v2(m_database, CT2A(req, CP_UTF8), -1, &stmt, 0);
			if(ret == SQLITE_OK)
			{
				CString sId, sAlert, sDate, sTitle, sAcknow, sCreate, sClass, sUrgent, sUnread, sPosition, sTicker, sToDate;
				while(sqlite3_step(stmt) == SQLITE_ROW)
				{
					sId = CA2T((char*)sqlite3_column_text(stmt, 0), CP_UTF8);
					sAlert = CAlertUtils::escapeJSON((TCHAR*)CA2T((char*)sqlite3_column_text(stmt, 1), CP_UTF8));
					sAlert += CAlertUtils::escapeJSON((TCHAR*)CA2T((char*)sqlite3_column_text(stmt, 2), CP_UTF8));
					sAlert += CAlertUtils::escapeJSON((TCHAR*)CA2T((char*)sqlite3_column_text(stmt, 3), CP_UTF8));
					sDate = CAlertUtils::escapeJSON((TCHAR*)CA2T((char*)sqlite3_column_text(stmt, 4), CP_UTF8));
					sTitle = CAlertUtils::escapeJSON((TCHAR*)CA2T((char*)sqlite3_column_text(stmt, 5), CP_UTF8));
					sAcknow = CA2T((char*)sqlite3_column_text(stmt, 6), CP_UTF8);
					sCreate = CAlertUtils::escapeJSON((TCHAR*)CA2T((char*)sqlite3_column_text(stmt, 7),CP_UTF8));
					sClass = CA2T((char*)sqlite3_column_text(stmt, 8),CP_UTF8);
					sUrgent = CA2T((char*)sqlite3_column_text(stmt, 9),CP_UTF8);
					sUnread = CA2T((char*)sqlite3_column_text(stmt, 10),CP_UTF8);
					sPosition = CA2T((char*)sqlite3_column_text(stmt, 11),CP_UTF8);
					sTicker = CA2T((char*)sqlite3_column_text(stmt, 12),CP_UTF8);
					sToDate = CAlertUtils::escapeJSON((TCHAR*)CA2T((char*)sqlite3_column_text(stmt, 13), CP_UTF8)); //
                    sId = CString("\"id\":")               + sId                      ;
                    sDate = CString("\"date\":\"")         + sDate     + CString("\"");
                    sAlert = CString("\"alert\":\"")       + sAlert    + CString("\"");
                    sTitle = CString("\"title\":\"")       + sTitle    + CString("\"");
                    sAcknow = CString("\"acknow\":")       + sAcknow                  ;
                    sCreate = CString("\"create\":\"")     + sCreate   + CString("\"");
                    sClass = CString("\"class\":")         + sClass                   ;
                    sUrgent = CString("\"urgent\":\"")     + sUrgent   + CString("\"");
                    sUnread = CString("\"unread\":\"")     + sUnread   + CString("\"");
                    sPosition = CString("\"position\":\"") + sPosition + CString("\"");
                    sTicker = CString("\"ticker\":\"")     + sTicker   + CString("\"");
                    sToDate = CString("\"to_date\":\"")    + sToDate   + CString("\"");
                    CString StringJSONobj;
                    StringJSONobj += CString("{")            ;
                    StringJSONobj += sId + CString(",")      ;
                    StringJSONobj += sDate + CString(",")    ;
                    StringJSONobj += sAlert + CString(",")   ;
                    StringJSONobj += sAcknow + CString(",")  ;
                    StringJSONobj += sTitle + CString(",")   ;
                    StringJSONobj += sCreate + CString(",")  ;
                    StringJSONobj += sClass + CString(",")   ;
                    StringJSONobj += sUrgent + CString(",")  ;
                    StringJSONobj += sUnread + CString(",")  ;
                    StringJSONobj += sPosition + CString(",");
                    StringJSONobj += sTicker + CString(",")  ;
                    StringJSONobj += sToDate                 ;
                    StringJSONobj += CString("},")           ;
                    *sResponse += StringJSONobj;
				}
				(*sResponse).TrimRight(_T(","));
				CString histCount;
				histCount.Format(_T("%I64d"),rowsCount);
				*sResponse = CString("({\"historyCount\":") + histCount + CString(",\"historyContent\":[") + *sResponse + CString("]});");
			}
			else
			{
				hr = E_FAIL;
			}
			sqlite3_finalize(stmt);
		}
		release_catch_tolog(_T("DB getHistoryRangeFromDb exception"))
		m_cs_db.Unlock();
	}

	return hr;
}

DWORD WINAPI CDatabaseManager::runInThread(LPVOID pv)
{
	DWORD res = 1;
	release_try
	{
		ThreadStruct *thSt = (ThreadStruct*)pv;
		if (thSt->functionName == "getHistoryRange")
		{
			HistoryRangeStruct *hist = (HistoryRangeStruct*)thSt->data;
			CDatabaseManager *m_db = thSt->m_db;

			CString _sDate = hist->_sDate;
			CString sFormat = hist->sFormat;
			sqlite_int64 startPos = hist->startPos;
			sqlite_int64 count = hist->count;
			CString searchTerm = hist->searchTerm;
			CString *response = new CString();
		
			m_db->getHistoryRangeFromDb(_sDate,response, sFormat,startPos,count,searchTerm);
			PostMessageDelWParam<CString>(hist->callbackWindow, WM_CALLBACK_INVOKE, response, (LPARAM)(hist->callbackId));
		}
		else
		{
			res = 2;
		}
		delete_catch(thSt);
	}
	release_catch_expr_and_tolog(res = 3, _T("DatabaseManager runInThread exception"))

	return res;
}

HRESULT CDatabaseManager::GetHistoryRange(CString _sDate,CString *sResponse, CString sFormat, sqlite_int64 startPos, sqlite_int64 count, CString searchTerm, BOOL async, ULONG callbackId, HWND callbackWindow)
{
	if(async)
	{
		ThreadStruct *thSt= new ThreadStruct();
		thSt->m_db = this;
		thSt->functionName="getHistoryRange";
		thSt->threadId = GetCurrentThreadId();

		CString sRequestResponse;
		HistoryRangeStruct *hist= new HistoryRangeStruct();
		hist->_sDate =_sDate;
		hist->count = count;
		hist->callbackId = callbackId;
		hist->callbackWindow = callbackWindow;
		hist->searchTerm = searchTerm;
		hist->sFormat = sFormat;
		hist->startPos = startPos;

		thSt->data = (void*)hist;
	
		CreateThread(NULL, 0, CDatabaseManager::runInThread,(void*)thSt, 0, NULL);
	}
	else
	{
		getHistoryRangeFromDb(_sDate,sResponse, sFormat,startPos,count,searchTerm);
	}

	return S_OK;

}

HRESULT CDatabaseManager::AddToHistory(shared_ptr<NodeALERT> alertData, CString window, sqlite_int64 *history_id, BOOL *isCreated)
{
	ATLASSERT(m_database);
	if(m_database && alertData)
	{
		m_cs_db.Lock();
		release_try
		{
			CString html = alertData->m_html,
					title = alertData->m_title,
					topTemplate = alertData->m_topTemplateHTML,
					bottomTemplate = alertData->m_bottomTemplateHTML,
					createDate = alertData->m_createDate,
					width = alertData->m_iWidth,
					height = alertData->m_iHeight,
					position = alertData->m_position,
					visible = alertData->m_visible,
					toDate = alertData->m_to_date,
					cdata = alertData->m_inner; //inner XML

			if (position ==""){
				position = alertData->m_docked == _T("1") ? "1" : "";
			}

			const int acknow = alertData->m_acknowledgement == _T("1") ? 1 : 0,
						utf8 = alertData->m_isUTF8 ? 1 : 0,
						ticker = alertData->m_ticker == _T("1") ? 1 : 0,
						autoclose = CAlertUtils::str2int(alertData->m_autoclose),
						survey = CAlertUtils::str2int(alertData->m_survey),
						schedule = CAlertUtils::str2int(alertData->m_schedule),
						recurrence = CAlertUtils::str2int(alertData->m_recurrence),
						urgent = CAlertUtils::str2int(alertData->m_urgent),
						alertClass = CAlertUtils::str2int(alertData->m_class),
						resizable = CAlertUtils::str2int(alertData->m_resizable);
			const INT64 user_id = CAlertUtils::str2int64(alertData->m_userid),
						alert_id = CAlertUtils::str2int64(alertData->m_alertid);

			const BOOL UTC = alertData->m_UTC;

			window.Replace(_T("'"), _T("''"));
			html.Replace(_T("'"), _T("''"));
			title.Replace(_T("'"), _T("''"));
			topTemplate.Replace(_T("'"), _T("''"));
			bottomTemplate.Replace(_T("'"), _T("''"));
			width.Replace(_T("'"), _T("''"));
			height.Replace(_T("'"), _T("''"));
			position.Replace(_T("'"), _T("''"));
			visible.Replace(_T("'"), _T("''"));
			cdata.Replace(_T("'"), _T("''"));

			CString SQLString;
			SQLString.Format(_T("INSERT INTO `history` (			\n\
									`alert`,						\n\
									`title`,						\n\
									`window`,						\n\
									`top`,							\n\
									`bottom`,						\n\
									`width`,						\n\
									`height`,						\n\
									`position`,						\n\
									`visible`,						\n\
									`cdata`,						\n\
									`date`,							\n\
									`user_id`,						\n\
									`alert_id`,						\n\
									`autoclose`,					\n\
									`acknow`,						\n\
									`utf8`,							\n\
									`ticker`,						\n\
									`survey`,						\n\
									`schedule`,						\n\
									`recurrence`,					\n\
									`urgent`,						\n\
									`create_date`,					\n\
									`manual_closed`,				\n\
									`class`,						\n\
									`unread`,						\n\
									`resizable`,					\n\
								    `to_date`						\n\
								)									\n\
								VALUES (							\n\
									'%s',							\n\
									'%s',							\n\
									'%s',							\n\
									'%s',							\n\
									'%s',							\n\
									'%s',							\n\
									'%s',							\n\
									'%s',							\n\
									'%s',							\n\
									'%s',							\n\
									datetime('now','localtime'),	\n\
									%I64d,							\n\
									%I64d,							\n\
									%d,								\n\
									%d,								\n\
									%d,								\n\
									%d,								\n\
									%d,								\n\
									%d,								\n\
									%d,								\n\
									%d,								\n\
									datetime('%s','unixepoch'%s),	\n\
									0,								\n\
									%d,								\n\
									%d,								\n\
									%d,								\n\
									datetime('%s','unixepoch')		)"),
								html,
								title,
								window,
								topTemplate,
								bottomTemplate,
								width,
								height,
								position,
								visible,
								cdata,
								user_id,
								alert_id,
								autoclose,
								acknow,
								utf8,
								ticker,
								survey,
								schedule,
								recurrence,
								urgent,
								createDate,
								UTC?_T(", 'localtime'"):_T(""),
								alertClass,
								alertData->getIsUnread() ? 1 : 0,
								resizable,
								toDate
							);

            sqlite_int64 _history_id;
            if (!(CDatabaseManager::isAlertInHistory(alert_id, &_history_id)))
            {
                const int ret = sqlite3_exec(m_database, CT2A(SQLString, CP_UTF8), NULL, NULL, NULL);
                if (ret != SQLITE_OK)
                {
                    *isCreated = FALSE;
                    //logDbError(L"Problems inserting history. \nsqlite error: %s", CA2T(sqlite3_errmsg(m_database), CP_UTF8));
                }
                else if (history_id)
                {
                    *history_id = sqlite3_last_insert_rowid(m_database);
                    *isCreated = TRUE;
                }
			}
			else 
            {
                *history_id = _history_id;
                *isCreated = FALSE;
			}
		}
		release_catch_tolog(_T("DB AddToHistory exception"))
		m_cs_db.Unlock();
	}
	return S_OK;
}

HRESULT CDatabaseManager::PurgeHistory(long unsigned int iDate)
{
	ATLASSERT(m_database);
	if(m_database)
	{
		m_cs_db.Lock();
		release_try
		{
			CStringA SQLString;
			SQLString.Format("UPDATE `history` SET `deleted` = 1 WHERE `date` < datetime('now','-%lu day') AND manual_close <> 1", iDate);
			int ret;
			ret = sqlite3_exec(m_database, "begin;", NULL, NULL, NULL);
			ret = sqlite3_exec(m_database, SQLString, NULL, NULL, NULL);
			ret = sqlite3_exec(m_database, "UPDATE `property` SET `value` = (SELECT count(1) from `history` where `deleted` = 0) WHERE `name` = 'history_count'", NULL, NULL, NULL);
			ret = sqlite3_exec(m_database, "commit;", NULL, NULL, NULL);
			//if(ret != SQLITE_OK)
				//logDbError(L"Problems deleting history days: %lu. \nsqlite error: %s", iDate, CA2T(sqlite3_errmsg(m_database), CP_UTF8));
		}
		release_catch_tolog(_T("DB PurgeHistory exception"))
		m_cs_db.Unlock();
	}
	return S_OK;
}

HRESULT CDatabaseManager::PurgeHistoryAtTime(unsigned long int iDate, CString Time)
{
	ATLASSERT(m_database);
	if(m_database)
	{
		m_cs_db.Lock();
		release_try
		{
			CStringA SQLString;
			if(iDate > 1)
			{
				iDate -= 1;
                SQLString.Format("UPDATE `history` SET `deleted` = 1 WHERE `date` < (date('now','-%lu day') + time('%s')) AND manual_close <> 1", iDate, Time);
			}
			else
			{
				SQLString.Format("UPDATE `history` SET `deleted` = 1 WHERE `date` < (date('now') + time('%s')) AND manual_close <> 1", Time);
			}
			int ret;
			ret = sqlite3_exec(m_database, "begin;", NULL, NULL, NULL);
			ret = sqlite3_exec(m_database, SQLString, NULL, NULL, NULL);
			ret = sqlite3_exec(m_database, "UPDATE `property` SET `value` = (SELECT count(1) from `history` where `deleted` = 0) WHERE `name` = 'history_count'", NULL, NULL, NULL);
			ret = sqlite3_exec(m_database, "commit;", NULL, NULL, NULL);
			//if(ret != SQLITE_OK)
				//logDbError(L"Problems deleting history days: %lu at time: %s. \nsqlite error: %s", iDate, Time, CA2T(sqlite3_errmsg(m_database), CP_UTF8));
		}
		release_catch_tolog(_T("DB PurgeHistoryAtTime exception"))
		m_cs_db.Unlock();
	}
	return S_OK;
}

HRESULT CDatabaseManager::DeleteAlert(sqlite_int64 id)
{
	ATLASSERT(m_database);
	if(m_database)
	{
		m_cs_db.Lock();
		release_try
		{
			CStringA SQLString;
			SQLString.Format("UPDATE `history` SET `deleted` = 1 WHERE id = %I64d", id);
			int ret;
			ret = sqlite3_exec(m_database, "begin;", NULL, NULL, NULL);
			ret = sqlite3_exec(m_database, SQLString, NULL, NULL, NULL);
			ret = sqlite3_exec(m_database, "UPDATE `property` SET `value` = (SELECT count(1) from `history` where `deleted` = 0) WHERE `name` = 'history_count'", NULL, NULL, NULL);
			ret = sqlite3_exec(m_database, "UPDATE `history` SET `unread` = 0 WHERE `deleted` = 1", NULL, NULL, NULL);
			ret = sqlite3_exec(m_database, "commit;", NULL, NULL, NULL);
			//if(ret != SQLITE_OK)
				//logDbError(L"Problems deleting alert from history by id: %I64d. \nsqlite error: %s", id, CA2T(sqlite3_errmsg(m_database), CP_UTF8));
		}
		release_catch_tolog(L"DB DeleteAlert exception")
		m_cs_db.Unlock();
	}
	return S_OK;
}

HRESULT CDatabaseManager::DeleteAlertByID(sqlite_int64 alertid)
{
	ATLASSERT(m_database);
	if(m_database)
	{
		m_cs_db.Lock();
		release_try
		{
			CStringA SQLString;
			SQLString.Format("UPDATE `history` SET `deleted` = 1 WHERE alert_id = %I64d", alertid);
			int ret;
			ret = sqlite3_exec(m_database, "begin;", NULL, NULL, NULL);
			ret = sqlite3_exec(m_database, SQLString, NULL, NULL, NULL);
			ret = sqlite3_exec(m_database, "UPDATE `history` SET `unread` = 0 WHERE `deleted` = 1", NULL, NULL, NULL);
			ret = sqlite3_exec(m_database, "UPDATE `property` SET `value` = (SELECT count(1) from `history` where `deleted` = 0) WHERE `name` = 'history_count'", NULL, NULL, NULL);
			ret = sqlite3_exec(m_database, "commit;", NULL, NULL, NULL);
			//if(ret != SQLITE_OK)
				//logDbError(L"Problems deleting alert from history by id: %I64d. \nsqlite error: %s", alertid, CA2T(sqlite3_errmsg(m_database), CP_UTF8));
		}
		release_catch_tolog(_T("DB DeleteAlert exception"))
			m_cs_db.Unlock();
	}
	return S_OK;
}

HRESULT CDatabaseManager::DeleteHistory()
{
	ATLASSERT(m_database);
	if(m_database)
	{
		m_cs_db.Lock();
		release_try
		{
			int ret;
			ret = sqlite3_exec(m_database, "begin;", NULL, NULL, NULL);
			ret = sqlite3_exec(m_database, "UPDATE `history` SET `deleted` = 1 WHERE class <> 2 AND class <> 8", NULL, NULL, NULL);
			ret = sqlite3_exec(m_database, "UPDATE `history` SET `unread` = 0 WHERE `deleted` = 1", NULL, NULL, NULL);
			ret = sqlite3_exec(m_database, "UPDATE `property` SET `value` = (SELECT count(1) from `history` where `deleted` = 0) WHERE `name` = 'history_count'", NULL, NULL, NULL);
			ret = sqlite3_exec(m_database, "commit;", NULL, NULL, NULL);
			//if(ret != SQLITE_OK)
				//logDbError(L"Problems deleting all history. \nsqlite error: %s", CA2T(sqlite3_errmsg(m_database), CP_UTF8));
		}
		release_catch_tolog(_T("DB DeleteHistory exception"))
		m_cs_db.Unlock();
	}
	return S_OK;
}

HRESULT CDatabaseManager::setAlertShowed(sqlite_int64 alertid) {
    HRESULT returnValue = S_FALSE;
    ATLASSERT(m_database);
    if (m_database)
    {
        m_cs_db.Lock();
        release_try
        {
           CString req;
           req.Format(_T("UPDATE `received`         \
                    	    SET `showed` = 1	    \
                       	  WHERE `alert_id` = %I64d 	\
           	"), alertid);
           int ret = sqlite3_exec(m_database, CT2A(req, CP_UTF8), NULL, NULL, NULL);
           if (ret == SQLITE_OK)
           {
               returnValue = S_OK;
           }
        }
        release_catch_tolog(_T("DB setAlertShowed exception"))
        m_cs_db.Unlock();
    }
    return returnValue;
}

HRESULT CDatabaseManager::setReceived(sqlite_int64 alertid, CString body) 
{
    HRESULT returnValue = S_FALSE;
    ATLASSERT(m_database);
    if (m_database)
    {
        m_cs_db.Lock();
        release_try
        {
           CString req;
    
           req.Format(_T("INSERT INTO `received`                     \
                                 ( `alert_id`, `showed`, `body` )    \
                          VALUES                                     \
                                 ( %I64d, 0, '%s' )                    \
           	"), alertid, body);
           int ret = sqlite3_exec(m_database, CT2A(req, CP_UTF8), NULL, NULL, NULL);
           if (ret == SQLITE_OK)
           {
               returnValue = S_OK;
           }
        }
        release_catch_tolog(_T("DB setReceived exception"))
        m_cs_db.Unlock();
    }
    return returnValue;
}

HRESULT CDatabaseManager::setDownloaded(sqlite_int64 alertid, CString content)
{
    HRESULT returnValue = S_FALSE;
    content.Replace(_T("'"), _T("''"));
    ATLASSERT(m_database);
    if (m_database)
    {
        m_cs_db.Lock();
        release_try
        {
           CString req;

           req.Format(_T("INSERT INTO `downloaded`            \
                                 ( `alert_id`, `content` )    \
                          VALUES                              \
                                 ( %I64d, '%s' )              \
           	"), alertid, content);
           int ret = sqlite3_exec(m_database, CT2A(req, CP_UTF8), NULL, NULL, NULL);
           if (ret == SQLITE_OK)
           {
               returnValue = S_OK;
           }
        }
            release_catch_tolog(_T("DB setDownloaded exception"))
            m_cs_db.Unlock();
    }
    return returnValue;
}

CString CDatabaseManager::getDownloaded(sqlite_int64 id)
{
    CString resString;
    ATLASSERT(m_database);
    if (m_database)
    {
        m_cs_db.Lock();
        release_try
        {

            CString req;
            req.Format(_T("SELECT D.alert_id, D.content          \
                                FROM downloaded D                \
                               WHERE D.alert_id = %I64d          \
						"), id);

            sqlite3_stmt* stmt = NULL;
            const int ret = sqlite3_prepare_v2(m_database, CT2A(req, CP_UTF8), -1, &stmt, 0);
            if (ret == SQLITE_OK && stmt)
            {
                if (sqlite3_step(stmt) == SQLITE_ROW)
                {
                    resString = CA2T((char*)sqlite3_column_text(stmt, 1), CP_UTF8);
                }
            }
            sqlite3_finalize(stmt);
        }
            release_catch_tolog(_T("DB getDownloaded exception"))
            m_cs_db.Unlock();
    }
    return resString;
}

HRESULT CDatabaseManager::cleanDownloaded(sqlite_int64 alertid) 
{
    HRESULT returnValue = S_FALSE;
    ATLASSERT(m_database);
    if (m_database)
    {
        m_cs_db.Lock();
        release_try
        {
           CString req;
           req.Format(_T("DELETE FROM `downloaded`      \
                          WHERE  `alert_id` = %I64d     \
           	"), alertid);
           int ret = sqlite3_exec(m_database, CT2A(req, CP_UTF8), NULL, NULL, NULL);
           if (ret == SQLITE_OK)
           {
               returnValue = S_OK;
           }
        }
        release_catch_tolog(_T("DB cleanDownloaded exception"))
        m_cs_db.Unlock();
    }
    return returnValue;
}

void CDatabaseManager::setProperty(CString name, CString value)
{
	ATLASSERT(m_database);
	if(m_database)
	{
		m_cs_db.Lock();
		release_try
		{
			name.Replace(_T("'"), _T("''"));
			value.Replace(_T("'"), _T("''"));

			CString req;
			CString checkReq;
			checkReq.Format(_T("SELECT 1 FROM `property` WHERE `name` = '%s'"), name);
			
			sqlite3_stmt* stmt;
			int ret = sqlite3_prepare_v2(m_database, CT2A(checkReq, CP_UTF8), -1, &stmt, 0);
			if(ret == SQLITE_OK)
			{
				if(sqlite3_step(stmt) == SQLITE_ROW)
				{
					req.Format(_T("PRAGMA synchronous = OFF; UPDATE `property` SET `value`= '%s' WHERE `name` == '%s'"), value, name);
				}
				else
				{
					req.Format(_T("PRAGMA synchronous = OFF; INSERT INTO `property` (`name`, `value`) VALUES('%s', '%s')"), name, value);
				}
			}
			//else
				//logDbError(L"Problems checking property on set. \nsqlite error: %s", CA2T(sqlite3_errmsg(m_database), CP_UTF8));
			sqlite3_finalize(stmt);
			ret = sqlite3_exec(m_database,CT2A(req, CP_UTF8),NULL,NULL,NULL);
			//if(ret != SQLITE_OK)
				//logDbError(L"Problems setting property: %s. \nsqlite error: %s", name, CA2T(sqlite3_errmsg(m_database), CP_UTF8));
		}
		release_catch_tolog(_T("DB setProperty exception"))
		m_cs_db.Unlock();
	}
}

bool CDatabaseManager::hasProperty(CString name)
{
	bool res = false;
	ATLASSERT(m_database);
	if(m_database)
	{
		m_cs_db.Lock();
		release_try
		{
			name.Replace(_T("'"), _T("''"));

			CString req;
			req.Format(_T("SELECT 1 FROM `property` WHERE `name` == '%s'"), name);

			sqlite3_stmt* stmt;
			int ret = sqlite3_prepare_v2(m_database, CT2A(req, CP_UTF8), -1, &stmt, 0);
			if (ret == SQLITE_OK)
			{
				if (sqlite3_step(stmt) == SQLITE_ROW)
				{
					res = true;
				}
			}
			//else
				//logDbError(L"Problems checking property. \nsqlite error: %s", CA2T(sqlite3_errmsg(m_database), CP_UTF8));
			sqlite3_finalize(stmt);
		}
		release_catch_tolog(_T("DB hasProperty exception"))
		m_cs_db.Unlock();
	}
	return res;
}

CString CDatabaseManager::getProperty(CString name)
{
	CString sResponse;
	ATLASSERT(m_database);
	if(m_database)
	{
		m_cs_db.Lock();
		release_try
		{
			name.Replace(_T("'"), _T("''"));

			CString req;
			req.Format(_T("SELECT `value` FROM `property` WHERE `name` = '%s'"), name);

			sqlite3_stmt* stmt;

			int ret = sqlite3_prepare_v2(m_database, CT2A(req, CP_UTF8), -1, &stmt, 0);
			if (ret == SQLITE_OK)
			{
				if (sqlite3_step(stmt) == SQLITE_ROW)
				{
					sResponse = CA2T((char*)sqlite3_column_text(stmt, 0), CP_UTF8);
				}
			}
			//else
				//logDbError(L"Problems getting property. \nsqlite error: %s", CA2T(sqlite3_errmsg(m_database), CP_UTF8));
			sqlite3_finalize(stmt);
		}
		release_catch_tolog(_T("DB getProperty exception"))
		m_cs_db.Unlock();
	}
	return sResponse;
}

HRESULT CDatabaseManager::loadProperty(map<CString,CString> &prop)
{
	ATLASSERT(m_database);
	if (m_database)
	{
		m_cs_db.Lock();
		release_try
		{
			CString req =_T("SELECT `name`, `value` FROM `property`");

			sqlite3_stmt* stmt = NULL;
			int ret = sqlite3_prepare_v2(m_database, CT2A(req, CP_UTF8), -1, &stmt, 0);
			if (ret == SQLITE_OK && stmt)
			{
				while (sqlite3_step(stmt) == SQLITE_ROW)
				{
					CString name, value;
					name = CA2T((char*)sqlite3_column_text(stmt, 0), CP_UTF8);
					value = CA2T((char*)sqlite3_column_text(stmt, 1), CP_UTF8);
					prop.insert(std::pair<CString,CString>(name,value));
				}
			}
			//else
				//logDbError(L"Problems getting properies. \nsqlite error: %s", CA2T(sqlite3_errmsg(m_database), CP_UTF8));
			sqlite3_finalize(stmt);
		}
		release_catch_tolog(_T("DB loadProperty exception"))
		m_cs_db.Unlock();
	}
	return S_OK;
}

void CDatabaseManager::markAlertAsClosed(sqlite_int64 history_id)
{
	ATLASSERT(m_database);
	if(m_database)
	{
		m_cs_db.Lock();
		release_try
		{
			CStringA SQLString;
			SQLString.Format("UPDATE `history` SET `manual_closed` = 1, to_date = NULL WHERE `id` = %I64d", history_id);
			const int ret = sqlite3_exec(m_database,SQLString,NULL,NULL,NULL);
			//if(ret != SQLITE_OK)
				//logDbError(L"Problems with set manual closed by id: %I64d. \nsqlite error: %s", history_id, CA2T(sqlite3_errmsg(m_database), CP_UTF8));
		}
		release_catch_tolog(_T("DB markAlertAsClosed exception"))
		m_cs_db.Unlock();
	}
}

void CDatabaseManager::markAlertAsTerminated(sqlite_int64 alert_id)
{
	ATLASSERT(m_database);
	if(m_database)
	{
		m_cs_db.Lock();
		release_try
		{
			CStringA SQLString;
			SQLString.Format("UPDATE `history` SET `autoclosed` = 2, to_date = NULL WHERE `alert_id` = %I64d", alert_id);
			const int ret = sqlite3_exec(m_database,SQLString,NULL,NULL,NULL);
			//if(ret != SQLITE_OK)
				//logDbError(L"Problems with set autoclosed by alert_id: %I64d. \nsqlite error: %s", alert_id, CA2T(sqlite3_errmsg(m_database), CP_UTF8));
		}
		release_catch_tolog(_T("DB markAlertAsClosed exception"))
		m_cs_db.Unlock();
	}
}

void CDatabaseManager::markAlertAsClosedByIdAndUser(sqlite_int64 alert_id, sqlite_int64 user_id)
{
	ATLASSERT(m_database);
	if(m_database)
	{
		m_cs_db.Lock();
		release_try
		{
			CStringA SQLString;
			int ret;
			// opeartor DELETE is necessary to delete information about alert frokm table alertId_hWnds 
			// since the closed alert can not be terminated
			ret = sqlite3_exec(m_database, "begin;", NULL, NULL, NULL);
			SQLString.Format("UPDATE `history` SET `manual_closed` = 1 WHERE `alert_id` = %I64d AND `user_id` = %I64d", alert_id, user_id);
			ret = sqlite3_exec(m_database, SQLString, NULL, NULL, NULL);
			SQLString.Format("DELETE FROM alertId_hWnds WHERE alertId = %I64d", alert_id);
			ret = sqlite3_exec(m_database,SQLString,NULL,NULL,NULL);
			ret = sqlite3_exec(m_database, "commit;", NULL, NULL, NULL);
			//if(ret != SQLITE_OK)
				//logDbError(L"Problems with set manual closed by id: %I64d. \nsqlite error: %s", alert_id, CA2T(sqlite3_errmsg(m_database), CP_UTF8));
		}
		release_catch_tolog(_T("DB markAlertAsClosed exception"))
			m_cs_db.Unlock();
	}
}

void CDatabaseManager::markAlertAsRead(sqlite_int64 history_id)
{
	ATLASSERT(m_database);
	if(m_database)
	{
		m_cs_db.Lock();
		release_try
		{
			CStringA SQLString;
			SQLString.Format("UPDATE `history` SET `unread` = 0 WHERE `id` = %I64d", history_id);
			const int ret = sqlite3_exec(m_database,SQLString,NULL,NULL,NULL);
			//if(ret != SQLITE_OK)
				//logDbError(L"Problems with set manual closed by id: %I64d. \nsqlite error: %s", history_id, CA2T(sqlite3_errmsg(m_database), CP_UTF8));
		}
		release_catch_tolog(_T("DB markAlertAsClosed exception"))
			m_cs_db.Unlock();
	}
}

void CDatabaseManager::getNotClosedAlerts(multimap<CString,shared_ptr<showData>> &alerts)
{
	ATLASSERT(m_database);
	if(m_database)
	{
		m_cs_db.Lock();
		release_try
		{
			CString req =_T("SELECT `id`,						\
								`window`,						\
								`alert`,						\
								`title`,						\
								`top`,							\
								`bottom`,						\
								`width`,						\
								`height`,						\
								`position`,						\
								`visible`,						\
								`cdata`,						\
								`autoclose`,					\
								`acknow`,						\
								`utf8`,							\
								`ticker`,						\
								`user_id`,						\
								`alert_id`,						\
								`survey`,						\
								`schedule`,						\
								`recurrence`,					\
								`urgent`,						\
								strftime('%s',`create_date`),	\
								`class`,						\
								`resizable`,					\
								strftime('%s',`date`),			\
								strftime('%s',`to_date`)		\
							FROM `history`						\
							WHERE `manual_closed` = 0 AND unread = 0 and `deleted` = 0 \
							ORDER BY `id` ASC					\
						");

			sqlite3_stmt* stmt = NULL;
			const int ret = sqlite3_prepare_v2(m_database, CT2A(req, CP_UTF8), -1, &stmt, 0);
			if(ret == SQLITE_OK && stmt)
			{
				int i = 0;
				while(sqlite3_step(stmt) == SQLITE_ROW)
				{
					CString window;
					shared_ptr<showData> alert = shared_ptr<showData>(new showData());
					getAlertData(stmt, i, window, alert);
					alert->to_date = CA2T((char*)sqlite3_column_text(stmt, 23), CP_UTF8);
					CAlertLoader::appendCustomHtmlToAlertFile(alert);
					alerts.insert(pair<CString,shared_ptr<showData>>(window,alert));
					i++;
				}
			}
			//else
				//logDbError(L"Problems getting not closed alerts from history. \nsqlite error: %s", CA2T(sqlite3_errmsg(m_database), CP_UTF8));
			sqlite3_finalize(stmt);
		}
		release_catch_tolog(_T("DB getNotClosedAlerts exception"))
		m_cs_db.Unlock();
	}
}

vector<CDatabaseManager::ReceivedAlert> CDatabaseManager::getVectorReceivedNotShowedAlerts()
{
    vector< CDatabaseManager::ReceivedAlert> resVector;
    ATLASSERT(m_database);
    if (m_database)
    {
        m_cs_db.Lock();
        release_try
        {
            
            CString req = _T("SELECT R.alert_id, R.body             \
                               FROM  received R                     \
                               WHERE R.showed = 0                   \
						");

            sqlite3_stmt* stmt = NULL;
            const int ret = sqlite3_prepare_v2(m_database, CT2A(req, CP_UTF8), -1, &stmt, 0);
            if (ret == SQLITE_OK && stmt)
            {
                CDatabaseManager::ReceivedAlert receivedAlert;
                while (sqlite3_step(stmt) == SQLITE_ROW)
                {
                    receivedAlert.ID = sqlite3_column_int64(stmt, 0);
                    receivedAlert.body = CA2T((char*)sqlite3_column_text(stmt, 1), CP_UTF8);
                    resVector.push_back(receivedAlert);
                }
            }
            sqlite3_finalize(stmt);
        }
        release_catch_tolog(_T("DB getVectorReceivedAlerts exception"))
        m_cs_db.Unlock();
    }
    return resVector;
}

void CDatabaseManager::getAlertData(sqlite3_stmt* stmt, const int i, CString &window, shared_ptr<showData> &alert)
{
	alert->history_id = sqlite3_column_int64(stmt, 0);
	window = CA2T((char*)sqlite3_column_text(stmt, 1), CP_UTF8);
	CString html = CA2T((char*)sqlite3_column_text(stmt, 2), CP_UTF8);
	alert->title = CA2T((char*)sqlite3_column_text(stmt, 3), CP_UTF8);
	alert->topTemplateHTML = CA2T((char*)sqlite3_column_text(stmt, 4), CP_UTF8);
	alert->bottomTemplateHTML = CA2T((char*)sqlite3_column_text(stmt, 5), CP_UTF8);
	alert->width = CA2T((char*)sqlite3_column_text(stmt, 6), CP_UTF8);
	alert->height = CA2T((char*)sqlite3_column_text(stmt, 7), CP_UTF8);
	alert->position = CA2T((char*)sqlite3_column_text(stmt, 8), CP_UTF8);
	alert->visible = CA2T((char*)sqlite3_column_text(stmt, 9), CP_UTF8);
	alert->cdata = CA2T((char*)sqlite3_column_text(stmt, 10), CP_UTF8);
	alert->autoclose = CA2T((char*)sqlite3_column_text(stmt, 11), CP_UTF8);
	alert->acknowledgement = CA2T((char*)sqlite3_column_text(stmt, 12), CP_UTF8);
	alert->utf8 = sqlite3_column_int(stmt, 13) != 0;
	alert->ticker = CA2T((char*)sqlite3_column_text(stmt, 14), CP_UTF8);
	alert->userid = CA2T((char*)sqlite3_column_text(stmt, 15), CP_UTF8);
	alert->alertid = CA2T((char*)sqlite3_column_text(stmt, 16), CP_UTF8);
	alert->survey = CA2T((char*)sqlite3_column_text(stmt, 17), CP_UTF8);
	alert->schedule = CA2T((char*)sqlite3_column_text(stmt, 18), CP_UTF8);
	alert->recurrence = CA2T((char*)sqlite3_column_text(stmt, 19), CP_UTF8);
	alert->urgent = CA2T((char*)sqlite3_column_text(stmt, 20), CP_UTF8);
	alert->create_date = CA2T((char*)sqlite3_column_text(stmt, 21), CP_UTF8);
	alert->UTC = FALSE;
    alert->Class = CA2T((char*)sqlite3_column_text(stmt, 22), CP_UTF8);
	alert->resizable = CA2T((char*)sqlite3_column_text(stmt, 23), CP_UTF8);
	alert->date = CA2T((char*)sqlite3_column_text(stmt, 24), CP_UTF8);
	alert->to_date = CA2T((char*)sqlite3_column_text(stmt, 25), CP_UTF8);
	
	if (alert->Class == "8" || alert->Class == "4096")
	{
		alert->utf8 = TRUE;
		CString imgSrc;
		if (alert->Class == "4096")
			imgSrc = _T("file:///") + CAlertUtils::getLockscreensPath() + _T("\\") + html;
		else
			imgSrc = _T("file:///") + CAlertUtils::getWallpapersPath() + _T("\\") + html;
		imgSrc.Replace(_T('\\'), _T('/'));
		CString imgHtml;
		CString styleHtml;
		if (alert->position == _T("1"))
		{
			imgHtml = _T("<div style='width:100%; height:100%; ' id='bg_img'></div>");
			styleHtml = _T("background-repeat: no-repeat; background-position: 50% 50%; background-attachment: fixed;");
		}
		else if (alert->position == _T("2"))
		{
			imgHtml = _T("<div style='width:100%; height:100%; ' id='bg_img'></div>");
			styleHtml = _T("background-repeat: repeat;");
		}
		else if (alert->position == _T("3"))
		{
			imgHtml.Format(_T("<img style='width:100%%; height:100%%; position:absolute; ' src='%s'/>"), imgSrc);
		}
		else
		{
			imgHtml = _T("<div style='width:100%; height:100%; ' id='bg_img'></div>");
			styleHtml = _T("background-repeat: no-repeat; background-position: 50% 50%; background-attachment: fixed;");
		}
		html.Format(_T("\
						<html>\n\
							<head>\n\
								<meta charset=\"utf-8\"/>\n\
								<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\"/>\n\
								<style>\n\
									html, body {\n\
										padding:0px;\n\
										margin:0px;\n\
										width:100%%;\n\
										height:100%%;\n\
										overflow:hidden;\n\
										border:0px;\n\
									}\n\
									#bg_img {\n\
										background-image: url('%s');\n\
										%s\n\
									}\n\
								</style>\n\
							</head>\n\
							<body>\n\
								%s\n\
							</body>\n\
						</html>"), imgSrc, styleHtml, imgHtml);
		alert->position = _T("fullscreen");
	}

	CString alertFilePath = CAlertUtils::getTempFileName(_T("restored"), i);
	const HANDLE alertFile = CreateFile(alertFilePath, GENERIC_WRITE, 0, NULL,CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	DWORD dwBytesWritten;
	CStringA data = CT2A(html, CP_UTF8) ;
	if (!alert->utf8) data = CT2A(html);
	WriteFile(alertFile, data, data.GetLength(), &dwBytesWritten, 0);
	CloseHandle(alertFile);

	alert->url = alertFilePath;
}

void CDatabaseManager::getAlertFromHistory(sqlite_int64 id, CString &window, shared_ptr<showData> alert)
{
	ATLASSERT(m_database);
	if(m_database)
	{
		m_cs_db.Lock();
		release_try
		{
			CString req;
			req.Format(_T("SELECT `id`,						\
							`window`,						\
							`alert`,						\
							`title`,						\
							`top`,							\
							`bottom`,						\
							`width`,						\
							`height`,						\
							`position`,						\
							`visible`,						\
							`cdata`,						\
							`autoclose`,					\
							`acknow`,						\
							`utf8`,							\
							`ticker`,						\
							`user_id`,						\
							`alert_id`,						\
							`survey`,						\
							`schedule`,						\
							`recurrence`,					\
							`urgent`,						\
							strftime('%%s',`create_date`),	\
							`class`,						\
							`resizable`,					\
							strftime('%%s',`date`),			\
							strftime('%%s',`to_date`)		\
							FROM `history`					\
							WHERE `id` = %I64d				\
							"), id);
			sqlite3_stmt* stmt = NULL;
			const int ret = sqlite3_prepare_v2(m_database, CT2A(req, CP_UTF8), -1, &stmt, 0);
			if(ret == SQLITE_OK && stmt)
			{
				if(sqlite3_step(stmt) == SQLITE_ROW)
				{
					getAlertData(stmt, 0, window, alert);
					alert->autoclose.Empty();
					alert->acknowledgement.Empty();
					CAlertLoader::appendCustomHtmlToAlertFile(alert);
				}
			}
			//else
				//logDbError(L"Problems opening alert from history. \nsqlite error: %s", CA2T(sqlite3_errmsg(m_database), CP_UTF8));
			sqlite3_finalize(stmt);
		}
		release_catch_tolog(_T("DB getAlertFromHistory exception"))
		m_cs_db.Unlock();
	}
}

bool CDatabaseManager::isAlertReceived(sqlite_int64 id)
{
    bool returnValue = false;
    ATLASSERT(m_database);
    if (m_database)
    {
        m_cs_db.Lock();
        release_try
        {
            CString req;
            req.Format(_T("SELECT  1                    \
		    				FROM `received`		        \
		    				WHERE `alert_id` = %I64d 	\
		    				"), id);
            sqlite3_stmt* stmt = NULL;
            const int ret = sqlite3_prepare_v2(m_database, CT2A(req, CP_UTF8), -1, &stmt, 0);
            if (ret == SQLITE_OK && stmt)
            {
                if (sqlite3_step(stmt) == SQLITE_ROW)
                    returnValue = true;
            }
            sqlite3_finalize(stmt);
        }
        release_catch_tolog(_T("DB isAlertReceived exception"))
        m_cs_db.Unlock();
    }
    return returnValue;
}

bool CDatabaseManager::isAlertDownloaded(sqlite_int64 id) 
{
    bool returnValue = false;
    ATLASSERT(m_database);
    if (m_database)
    {
        m_cs_db.Lock();
        release_try
        {
            CString req;
            req.Format(_T("SELECT  1                    \
		    				FROM `downloaded`           \
		    				WHERE `alert_id` = %I64d 	\
		    				"), id);
            sqlite3_stmt* stmt = NULL;
            const int ret = sqlite3_prepare_v2(m_database, CT2A(req, CP_UTF8), -1, &stmt, 0);
            if (ret == SQLITE_OK && stmt)
            {
                if (sqlite3_step(stmt) == SQLITE_ROW)
                    returnValue = true;
            }
            sqlite3_finalize(stmt);
        }
            release_catch_tolog(_T("DB isAlertDownloaded exception"))
            m_cs_db.Unlock();
    }
    return returnValue;
}

bool CDatabaseManager::isAlertInHistory(sqlite_int64 alert_id, sqlite_int64 *history_id)
{
    bool returnValue = false;
    ATLASSERT(m_database);
    if (m_database)
    {
        m_cs_db.Lock();
        release_try
        {
            CString req;
            req.Format(_T("SELECT  `id`                 \
		    				FROM `history`		        \
		    				WHERE `alert_id` = %I64d 	\
		    				"), alert_id);

            sqlite3_stmt* stmt = NULL;
            const int ret = sqlite3_prepare_v2(m_database, CT2A(req, CP_UTF8), -1, &stmt, 0);
            if (ret == SQLITE_OK && stmt)
            {
                if (sqlite3_step(stmt) == SQLITE_ROW)
                {
                    *history_id = sqlite3_column_int64(stmt, 0);
                    returnValue = true;
                }
            }
            sqlite3_finalize(stmt);
        }
        release_catch_tolog(_T("DB isAlertInHistory exception"))
        m_cs_db.Unlock();
    }
    return returnValue;
}

void CDatabaseManager::synchronizeAlerts(std::vector<NodeALERT*> *alerts, CString alertClass)
{
	ATLASSERT(m_database);
	if(m_database)
	{
		m_cs_db.Lock();
		release_try
		{
			CString req;
			req.Format(_T("SELECT `alert_id`						\
							FROM `history`					\
							WHERE `class` = %s				\
							"),alertClass);
			sqlite3_stmt* stmt = NULL;
			int ret = sqlite3_prepare_v2(m_database, CT2A(req, CP_UTF8), -1, &stmt, 0);
			CString removeIds;
			if(ret == SQLITE_OK && stmt)
			{
				while(sqlite3_step(stmt) == SQLITE_ROW)
				{
					sqlite_int64 alertId = sqlite3_column_int64(stmt, 0);

					BOOL founded = false;
					std::vector<NodeALERT*>::iterator rit = alerts->begin();
					while(rit != alerts->end())
					{
						if(CAlertUtils::str2int64((*rit)->m_alertid) == alertId)
						{
							founded = true;
							alerts->erase(rit);
							break;
						}
						else
						{
							++rit;
						}
					}
					if (!founded)
					{
						CString id;
						id.Format(_T("%I64d"), alertId);
						if (removeIds.IsEmpty())
						{
							removeIds = id;
						}
						else
						{
							removeIds = removeIds +_T(",") + id;
						}
					}
				}
				
			}
			sqlite3_finalize(stmt);
			if(removeIds.GetLength() > 0)
			{
				CString deleteReq;
				deleteReq.Format(TEXT("DELETE FROM history WHERE alert_id in (%s)"), removeIds);
				ret = sqlite3_exec(m_database,CT2A(deleteReq),NULL,NULL,NULL);
			}
		}
		release_catch_tolog(_T("DB synchronizeAlerts exception"))
		m_cs_db.Unlock();
	}
}

//returns True if there is a difference between *groups and DB 
bool CDatabaseManager::synchronizeGroups(vector<wstring> groups, vector<wstring> &add, vector<wstring> &remove,bool needUpdate)
{
	bool difference = false;
	ATLASSERT(m_database);
	if(m_database)
	{
		m_cs_db.Lock();
		release_try
		{

			if (!groups.empty())
			{
				CString req;
				req.Format(_T("SELECT `name`	\
								FROM `groups`"));
				sqlite3_stmt* stmt = NULL;
				int ret = sqlite3_prepare_v2(m_database, CT2A(req, CP_UTF8), -1, &stmt, 0);
				if(ret == SQLITE_OK)
				{
					while(sqlite3_step(stmt) == SQLITE_ROW)
					{					
						vector<wstring>::iterator pos;
						CString tmp(sqlite3_column_text(stmt, 0));
						
						wstring str(tmp);
						pos = find(groups.begin(),groups.end(),str);
						if( groups.end() == pos )
						{
							difference = true;
							if (needUpdate)
							{
								pos = groups.begin();
								CString sql("");
								sql.Format(_T("DELETE FROM `groups` WHERE `name`= '%s'"),str.c_str());

								const int ret = sqlite3_exec(m_database,CT2A(sql,CP_UTF8),NULL,NULL,NULL);
								if(ret != SQLITE_OK)
								{
									//logDbError(L"Problems removing groups. \nsqlite error: %s", CA2T(sqlite3_errmsg(m_database), CP_UTF8));
								}
								
							}
								remove.push_back(str);
						}
						else
						{
							groups.erase(pos);
						}
					}				
				}
				while (!groups.empty())
				{
					difference = true;
					
					add.push_back(*groups.begin());

					if (needUpdate)
					{	
						CString str;
						str.Format(_T("INSERT INTO groups(name) VALUES ('%s')"),(*groups.begin()).c_str());				

						const int ret = sqlite3_exec(m_database,CT2A(str,CP_UTF8),NULL,NULL,NULL);
						if(ret != SQLITE_OK)
						{
							//logDbError(L"Problems inserting groups. \nsqlite error: %s", CA2T(sqlite3_errmsg(m_database), CP_UTF8));
						}
					}
					groups.erase(groups.begin());
				}
			}
		}
		release_catch_tolog(_T("DB synchronizeGroups exception"))
		m_cs_db.Unlock();
	}
	return difference;
}

void CDatabaseManager::getWallpaperAlerts(std::vector<CWallpaperAlert*> &alerts)
{
	ATLASSERT(m_database);
	if(m_database)
	{
		m_cs_db.Lock();
		release_try
		{
			sqlite3_stmt* stmt = NULL;
			CString req =_T("SELECT alert, autoclose, position, id FROM history WHERE class = 8 ORDER BY id DESC");
			const int ret = sqlite3_prepare_v2(m_database, CT2A(req, CP_UTF8), -1, &stmt, 0);
			if (ret == SQLITE_OK)
			{
				while(sqlite3_step(stmt) == SQLITE_ROW)
				{
					CWallpaperAlert *alert = new CWallpaperAlert();

					alert->m_fileName = CA2T((char*)sqlite3_column_text(stmt, 0), CP_UTF8);
					alert->m_autoclose = sqlite3_column_int(stmt, 1);
					alert->m_position = CA2T((char*)sqlite3_column_text(stmt, 2), CP_UTF8);
					alert->m_id = sqlite3_column_int64(stmt, 3);
					alerts.push_back(alert);
				}
			}
			sqlite3_finalize(stmt);
		}
		release_catch_tolog(_T("DB getWallpaperAlerts exception"))
		m_cs_db.Unlock();
	}
}

void CDatabaseManager::getLockscreenAlerts(std::vector<CLockscreenAlert*> &alerts)
{
	ATLASSERT(m_database);
	if (m_database)
	{
		m_cs_db.Lock();
		release_try
		{
			sqlite3_stmt* stmt = NULL;
			CString req = _T("SELECT alert, autoclose, position, id FROM history WHERE class = 4096 ORDER BY id DESC");
			const int ret = sqlite3_prepare_v2(m_database, CT2A(req, CP_UTF8), -1, &stmt, 0);
			if (ret == SQLITE_OK)
			{
				while (sqlite3_step(stmt) == SQLITE_ROW)
				{
					CLockscreenAlert *alert = new CLockscreenAlert();

					alert->m_fileName = CA2T((char*)sqlite3_column_text(stmt, 0), CP_UTF8);
					alert->m_id = sqlite3_column_int64(stmt, 3);
					alerts.push_back(alert);
				}
			}
			sqlite3_finalize(stmt);
		}
		
		release_catch_tolog(_T("DB getLockscreenAlerts exception"))
		m_cs_db.Unlock();
	}
}

sqlite_int64 CDatabaseManager::getUnreadCount()
{
	sqlite_int64 count = 0;
	ATLASSERT(m_database);
	if(m_database)
	{
		m_cs_db.Lock();
		release_try
		{
			sqlite3_stmt* stmt = NULL;
			CString req =_T("SELECT count(*) FROM history WHERE unread = 1");
			const int ret = sqlite3_prepare_v2(m_database, CT2A(req, CP_UTF8), -1, &stmt, 0);
			if (ret == SQLITE_OK)
			{
				while(sqlite3_step(stmt) == SQLITE_ROW)
				{
					count = sqlite3_column_int64(stmt, 0);
				}
			}
			sqlite3_finalize(stmt);
		}
		release_catch_tolog(_T("DB getunreadcount exception"))
		m_cs_db.Unlock();
	}
	return count;
}

// the methods below are necessary to implement alerts termination

// Add information about alert window hWnd into table alertId_hWnd
void CDatabaseManager::AddToAlertId_hWnds(sqlite_int64 alertId, sqlite_int64 hWndTools, sqlite_int64 hWndBody)
{
	ATLASSERT(m_database);
	if(m_database)
	{
		m_cs_db.Lock();
		release_try
		{
			sqlite3_stmt* stmt = NULL;
			CString req;
			req.Format(_T("INSERT INTO `alertId_hWnds`(`alertId`, `hWndTools`, `hWndBody`) VALUES(%I64d, %I64d, %I64d)"), alertId, hWndTools, hWndBody);
			int ret = sqlite3_prepare_v2(m_database, CT2A(req, CP_UTF8), -1, &stmt, 0);
			if (ret == SQLITE_OK)
			{
				ret = sqlite3_exec(m_database, CT2A(req, CP_UTF8), NULL, NULL, NULL);
				if(ret != SQLITE_OK)
				{
					//logDbError(L"Problems inserting alert hWnd. \nsqlite error: %s", CA2T(sqlite3_errmsg(m_database), CP_UTF8));
				}
			}
			sqlite3_finalize(stmt);
		}
		release_catch_tolog(_T("DB getunreadcount exception"))
		m_cs_db.Unlock();
	}
}

// Delete information about alert window hWnd from table alertId_hWnd
void CDatabaseManager::DeleteFromAlertId_hWnd(sqlite_int64 alertId)
{
	ATLASSERT(m_database);
	if(m_database)
	{
		m_cs_db.Lock();
		release_try
		{
			sqlite3_stmt* stmt = NULL;
			CString req;
			req.Format(_T("DELETE FROM `alertId_hWnds` WHERE `alertId` = %I64d"), alertId);
			int ret = sqlite3_prepare_v2(m_database, CT2A(req, CP_UTF8), -1, &stmt, 0);
			if (ret == SQLITE_OK)
			{
				ret = sqlite3_exec(m_database, CT2A(req, CP_UTF8), NULL, NULL, NULL);
				if(ret != SQLITE_OK)
				{
					//logDbError(L"Problems deleting alert hWnd. \nsqlite error: %s", CA2T(sqlite3_errmsg(m_database), CP_UTF8));
				}
			}
			sqlite3_finalize(stmt);
		}
		release_catch_tolog(_T("DB getunreadcount exception"))
		m_cs_db.Unlock();
	}
}

// return hWnd by alertId. Necessary to close alert windows
void CDatabaseManager::GethWndByAlertId(sqlite_int64 alertId, sqlite_int64* hWndTools, sqlite_int64* hWndBody)
{
	ATLASSERT(m_database);
	if(m_database)
	{
		m_cs_db.Lock();
		release_try
		{
			sqlite3_stmt* stmt = NULL;
			CString req;
			req.Format(_T("SELECT `hWndTools`, `hWndBody` FROM `alertId_hWnds` WHERE `alertId` = %I64d"), alertId);
			const int ret = sqlite3_prepare_v2(m_database, CT2A(req, CP_UTF8), -1, &stmt, 0);
			if (ret == SQLITE_OK)
			{
				while(sqlite3_step(stmt) == SQLITE_ROW)
				{
					*hWndTools = sqlite3_column_int64(stmt, 0);
					*hWndBody = sqlite3_column_int64(stmt, 1);
				}
			}
			sqlite3_finalize(stmt);
		}
		release_catch_tolog(_T("DB getunreadcount exception"))
		m_cs_db.Unlock();
	}
}

bool CDatabaseManager::CheckIfTicker(sqlite_int64 alertId) {
	bool result = false;

	ATLASSERT(m_database);
	if (m_database)
	{
		m_cs_db.Lock();
		release_try
		{
			CString req;
			req.Format(_T("SELECT `ticker`					\
							FROM `history`					\
							WHERE `alert_id` = %I64d		\
							"), alertId);

			sqlite3_stmt* stmt = NULL;
			const int ret = sqlite3_prepare_v2(m_database, CT2A(req, CP_UTF8), -1, &stmt, 0);
			if (ret == SQLITE_OK)
			{
				while (sqlite3_step(stmt) == SQLITE_ROW)
				{
					result = sqlite3_column_int(stmt, 0) == 1;
				}
			}

			sqlite3_finalize(stmt);
		}
		release_catch_tolog(_T("DB CheckIfTicker exception"))
		m_cs_db.Unlock();
	}

	return result;
}
