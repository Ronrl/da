////////////////////////////////////////
//XML Parser Definition
///////////////////////////////////////


#pragma once

#include "stdafx.h"
#include "errors.h"
#include "NodeTypes.h"

#include <msxml.h>


//WORK
#include <windows.h>
//#import "msxml3.dll"  named_guids
#include <atlbase.h>
#include <atlwin.h>

#include <atlapp.h>
#include <atlctrls.h>


#include <atlmisc.h>
#include <atlstr.h>

#include <atldlgs.h>

#include <vector>
#include <stack>

using namespace std;


class XMLParser;
class XmlNode;
class Menu;
int DetermineString(XMLParser *pParser, TCHAR *pName,CString *pString);


class XmlNode
{

public:
	CString m_cdata;
	CString m_tagName;

	bool m_bFlag = false;
	virtual void Determine(XMLParser *pParser);
	virtual void DetermineAttrib(XMLParser *pParser);

};


////////////////////////////
//Node structure for Parser
////////////////////////////

typedef struct tagNODESTRUCT
{
	XmlNode *pNode;
	CComPtr<IXMLDOMNode> spNode;
	bool isFirstChild;
    tagNODESTRUCT():isFirstChild(NULL) {}    
}NODESTRUCT;


////////////////////////////
//Determine Templates
////////////////////////////


template <class T> int DetermineNode (XMLParser *pParser,TCHAR *pTAG, T **result)
{
	if (pParser->IsTAGParsed())
		return ERR_ALREADY_PARSED;
	if (result == NULL)
		return ERR_NULL_POINTER;

	CComBSTR bstrTAG(pTAG);
	CComBSTR bstrNodeName;
	IXMLDOMNode *spNode = pParser->GetCurrentNode();

	spNode->get_nodeName(&bstrNodeName);
	NODESTRUCT NodeStruct;

	if (bstrTAG == bstrNodeName)
	{
		if(!*result) *result = new T;

		NodeStruct.pNode = (XmlNode *)*result;
		NodeStruct.isFirstChild = true;

		pParser->PushCurrentNode(NodeStruct);
		pParser->SetTAGParsed();
		return 0;
	}

	return ERR_NOT_FOUND;
}

template <class T> void DetermineNodeVector (XMLParser *pParser,TCHAR *pTAG, vector <T*> *vec)
{
	T *pNode = NULL;
	if (!DetermineNode(pParser,pTAG,&pNode))
		vec->push_back(pNode);
}




////////////////////////////
//XML Parser Main Class
////////////////////////////
class XMLParser
{
	XmlNode *pToolbar;
	stack <NODESTRUCT> NodeStack;
	IXMLDOMNode **spNode;
	bool bIsTAGParsed;
	TCHAR *RootNodeName;
public:
	int Parse(CString pString,bool IsFromFile=true);
	IXMLDOMNode* GetCurrentNode() {return *spNode;}
	void SetTAGParsed(bool bVal = true) {bIsTAGParsed = bVal;}
	bool IsTAGParsed() {return bIsTAGParsed;}
	void PushCurrentNode (NODESTRUCT NodeStruct);

	int DetermineString(TCHAR *pName, CString *pString);
	int DetermineUInt(TCHAR *pName, unsigned int *pInt, unsigned int default = 0);
	int DetermineInt(TCHAR *pName, int *pInt, int default = 0);
	int DetermineLong(TCHAR *pName, long *pLong, long default = 0L);
	int DetermineVariant(TCHAR *pName, CComVariant *pVariant);
	int DetermineCDATA(CString *pString);
	int DetermineXML(CString *pString);
	int DetermineInnerXML(CString *pString);
	XMLParser(XmlNode *_pToolbar, TCHAR *_name):pToolbar(_pToolbar),RootNodeName(_name)
	{bIsTAGParsed = false;spNode = new IXMLDOMNode*;}
	virtual ~XMLParser(){delete_catch(spNode);}

};
