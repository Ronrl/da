//
//  IInclude.h
//  DeskAlerts Client
//  Common module
//
//  Copyright Softomate 2006-2009. All rights reserved.
//

#pragma once

#include "IComponent.h"
#include "IActionListeners.h"
#include "sqlite3.h"

class NodeALERT;

class DefActionEvent: public IActionEvent
{
	CString eventName;
	CString sourceName;
	CString commandName;

public:

	DefActionEvent()
	{
	}

	DefActionEvent(CString _eventName,CString _sourceName,CString _commandName):
		eventName(_eventName),sourceName(_sourceName),commandName(_commandName)
	{
	}

public:
	CString& getCommandName()
	{
		return commandName;
	}
	CString& getEventName()
	{
		return eventName;
	}
	CString& getSourceName()
	{
		return sourceName;
	}
};

class CHistoryActionEvent: public IActionEvent
{
	CString eventName;
	CString sourceName;
	CString commandName;
	CString window;
	shared_ptr<NodeALERT> alertData;
	sqlite_int64 *history_id;
    BOOL *isCreated;

public:
	CHistoryActionEvent(CString _eventName, CString _sourceName, CString _commandName, shared_ptr<NodeALERT> _alertData, CString _window, sqlite_int64 *_history_id, BOOL *_isCreated):
		eventName(_eventName), sourceName(_sourceName), commandName(_commandName), alertData(_alertData), window(_window), history_id(_history_id), isCreated(_isCreated)
	{
	}

public:
	CString& getCommandName()
	{
		return commandName;
	}
	CString& getEventName()
	{
		return eventName;
	}
	CString& getSourceName()
	{
		return sourceName;
	}
	shared_ptr<NodeALERT> getAlertData()
	{
		return alertData;
	}
	CString& getWindow()
	{
		return window;
	}
	sqlite_int64 *getHistoryId()
	{
		return history_id;
	}
    BOOL* getIsCreated()
    {
        return isCreated;
    }
};



class CDisableAlertEvent: public IActionEvent
{
	CString eventName;
	CString sourceName;
	CString commandName;

public:
	CDisableAlertEvent(CString _eventName,CString _sourceName,CString _commandName):
		eventName(_eventName),sourceName(_sourceName),commandName(_commandName)
	{
	}

public:
	CString& getCommandName()
	{
		return commandName;
	}
	CString& getEventName()
	{
		return eventName;
	}
	CString& getSourceName()
	{
		return sourceName;
	}
};


class CStandByEvent: public IActionEvent
{
	CString eventName;
	CString sourceName;
	CString commandName;

public:
	CStandByEvent(CString _eventName,CString _sourceName,CString _commandName):
		eventName(_eventName),sourceName(_sourceName),commandName(_commandName)
	{
	}

public:
	CString& getCommandName()
	{
		return commandName;
	}
	CString& getEventName()
	{
		return eventName;
	}
	CString& getSourceName()
	{
		return sourceName;
	}
};