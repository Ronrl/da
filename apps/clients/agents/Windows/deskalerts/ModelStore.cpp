#include "StdAfx.h"
#include ".\modelstore.h"

CModelStore::CModelStore()
{
}

CModelStore::~CModelStore(void)
{
}

int CModelStore::Parse(CString sPath)
{
	XMLParser parser(&nodeALERTS,_T("ALERTS"));
	sPath += "conf.xml";
	return parser.Parse(sPath,true);
}

//-------------------------------------------------------------------------------------
void CModelStore::addModelListener(IModelListener* /*newListener*/)
{
}

void CModelStore::removeModelListener(IModelListener* /*newListener*/)
{
}

NodeALERTS& CModelStore::getModel(CString& /*propertyName*/)
{
	return nodeALERTS;
}
