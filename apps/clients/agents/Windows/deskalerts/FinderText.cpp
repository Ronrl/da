//
//  findertext.cpp
//  DeskAlerts Client
//  Common module
//
//  Copyright Softomate 2006-2009. All rights reserved.
//

#include "StdAfx.h"
#include ".\findertext.h"


CFinderText::CFinderText(CComPtr<IWebBrowser2> _pIE, CString param)
{
	CComQIPtr<IDispatch,&IID_IDispatch> pDisp;
	_pIE->get_Document(&pDisp);
	lpHtmlDocument = pDisp;
	match = param;
	SearchIndex = 1;
}

CFinderText::CFinderText(CComQIPtr<IHTMLDocument2, &IID_IHTMLDocument2> _lpHtmlDocument, CString param)
{
	lpHtmlDocument =_lpHtmlDocument;
	match = param;
	SearchIndex = 1;
}


CFinderText::~CFinderText(void)
{
    lpHtmlDocument.Detach();
}

void CFinderText::find(CString search)
{
	if (lastSearch!=search)
	{
		SearchIndex = 1;
		ClearSearchResults(lpHtmlDocument,_T("DeskAlertHistory"));
		FindText2(lpHtmlDocument,_bstr_t(search), 0, match, _T("DeskAlertHistory"));
	}
	else
	{
		SearchIndex++;
	}
	WORD CurrentIndex = SearchIndex;
	ScrollToFindText(lpHtmlDocument,CurrentIndex, _T("DeskAlertHistory"));
	if (CurrentIndex)
	{
		CurrentIndex = 1;
		SearchIndex = 1;
		ScrollToFindText(lpHtmlDocument,SearchIndex, _T("DeskAlertHistory"));
	}
}



void CFinderText::ClearSearchResults(CComQIPtr<IHTMLDocument2, &IID_IHTMLDocument2> lpHtmlDocument,_bstr_t searchID)
{
	if(lpHtmlDocument)
	{
		CComBSTR testid(searchID.length()+1,(const TCHAR*)searchID);
		CComBSTR testtag(5,"SPAN");

		CComPtr<IHTMLElementCollection> lpAllElements;
		lpHtmlDocument->get_all(&lpAllElements);

		CComQIPtr<IHTMLBodyElement> lpBody;
		CComPtr<IHTMLElement> lpBodyElm;
		lpHtmlDocument->get_body(&lpBodyElm);

		lpBody = lpBodyElm;
		if(lpBody)
		{
			CComPtr<IUnknown> lpUnk;
			if (SUCCEEDED(lpAllElements->get__newEnum(&lpUnk)) && lpUnk)
			{
				CComQIPtr<IEnumVARIANT> lpNewEnum = lpUnk;
				VARIANT varElement;
				while (lpNewEnum->Next(1, &varElement, NULL) == S_OK)
				{
					CComPtr<IHTMLElement> lpElement;
					_ASSERTE(varElement.vt == VT_DISPATCH);		
					varElement.pdispVal->QueryInterface(IID_IHTMLElement,(LPVOID*)&lpElement);	
					if (lpElement)
					{
						CComBSTR id;
						CComBSTR tag;
						lpElement->get_id(&id);
						lpElement->get_tagName(&tag);
						if((tag==testtag) && (id == testid))
						{
							BSTR innerText;
							lpElement->get_innerHTML(&innerText);
							lpElement->put_outerHTML(innerText);
						}
					}
					VariantClear(&varElement);
				}
			}
		}
		else
		{
			CComPtr<IHTMLFramesCollection2> frames;
			lpHtmlDocument->get_frames(&frames);
			if(frames)
			{
				long p;
				frames->get_length(&p);
				for(long f=0;f<p;f++)
				{
					variant_t frame;
					frames->item(&_variant_t(f),&frame);
					if((IDispatch*)frame)
					{
						IHTMLWindow2  *elem = 0;
						((IDispatch*)frame)->QueryInterface(IID_IHTMLWindow2, (void**)&elem);
						if(elem)
						{
							IHTMLDocument2*  lpHtmlDocument2 = NULL;
							elem->get_document(&lpHtmlDocument2);
							ClearSearchResults(lpHtmlDocument2,searchID);
						}
					}
				}
			}
		}
	}
}

void CFinderText::ScrollToFindText(CComQIPtr<IHTMLDocument2, &IID_IHTMLDocument2> lpHtmlDocument,WORD& NumItem, _bstr_t searchID)
{
	if(!lpHtmlDocument)
		return;
	
	CComBSTR testid(searchID.length()+1,(const TCHAR*)searchID);
	CComBSTR testtag(5,"SPAN");

	IHTMLElementCollection *lpAllElements;
	lpHtmlDocument->get_all(&lpAllElements);

	IHTMLBodyElement *lpBody;
	IHTMLElement *lpBodyElm;
	lpHtmlDocument->get_body(&lpBodyElm);

	if(!lpBodyElm) return;

	lpBodyElm->QueryInterface(IID_IHTMLBodyElement,(void**)&lpBody);
	lpBodyElm->Release();
	
	BOOL bComplite = FALSE;
	if(lpBody)
	{
		IUnknown *lpUnk;
		IEnumVARIANT *lpNewEnum = NULL;
		if (SUCCEEDED(lpAllElements->get__newEnum(&lpUnk)) && lpUnk != NULL)
		{
			lpUnk->QueryInterface(IID_IEnumVARIANT,(void**)&lpNewEnum);
			lpUnk->Release();
			VARIANT varElement;
			IHTMLElement *lpElement;
			while ((lpNewEnum->Next(1, &varElement, NULL) == S_OK))
			{
				_ASSERTE(varElement.vt == VT_DISPATCH);
				varElement.pdispVal->QueryInterface(IID_IHTMLElement,(void**)&lpElement);
				if (lpElement)
				{
					CComBSTR id;
					CComBSTR tag;
					lpElement->get_id(&id);
					lpElement->get_tagName(&tag);
					if(tag==testtag)
					{
						ShowHiddenElement(lpElement);
						if (!bComplite)
						{
							NumItem--;

							if (NumItem == 0)
							{
								VARIANT Start;
								Start.vt = VT_BOOL;
								Start.boolVal = VARIANT_TRUE;
								bComplite = TRUE;
								/*const HRESULT hr = */lpElement->scrollIntoView(Start);
								VariantClear(&Start);
							}
						}
					}
					lpElement->Release();
				}	
				VariantClear(&varElement);
			}
		}
		if(lpNewEnum)
			lpNewEnum->Release();
	}
	else
	{
		IHTMLFramesCollection2 *frames = 0;
		lpHtmlDocument->get_frames(&frames);
		if(frames)
		{
			long p;
			frames->get_length(&p);
			for(long f=0;f<p;f++)
			{
				variant_t frame;
				frames->item(&_variant_t(f),&frame);
				if((IDispatch*)frame)
				{
					IHTMLWindow2  *elem = 0;
					((IDispatch*)frame)->QueryInterface(IID_IHTMLWindow2, (void**)&elem);
					if(elem)
					{
						IHTMLDocument2*  lpHtmlDocument2 = NULL;
						elem->get_document(&lpHtmlDocument2);
						elem->Release();
						ScrollToFindText(lpHtmlDocument2, NumItem ,searchID);
					}
					((IDispatch*)frame)->Release();
				}
			}	(IDispatch*)frames->Release();
		}
	}
	if (lpAllElements) lpAllElements->Release();
	if (lpBody) lpBody->Release();
}

void CFinderText::ShowHiddenElement(CComQIPtr<IHTMLDOMNode> pElem)
{
	CComPtr<IHTMLDOMNode> pParent;
	HRESULT hr = pElem->get_parentNode(&pParent);
	while(SUCCEEDED(hr) && pParent)
	{
		CComQIPtr <IHTMLDOMNode> pCurrentParent = pParent;
		pParent.Release();
		hr = pCurrentParent->get_parentNode(&pParent);
		CComQIPtr<IHTMLElement> pParentElem = pCurrentParent;
		if (pParentElem)
		{
			CComPtr<IHTMLStyle> pStyle ;
			if(SUCCEEDED(pParentElem->get_style(&pStyle)) && pStyle)
			{
				CComBSTR display;
				if(SUCCEEDED(pStyle->get_display(&display)) && display==_T("hidden"))
				{
					display = _T("");
					pStyle->put_display(display);
				}
			}
		}
	}
}
void CFinderText::FindText2(CComQIPtr<IHTMLDocument2, &IID_IHTMLDocument2> lpHtmlDocument,_bstr_t searchText, long lFlags , _bstr_t matchStyle, _bstr_t searchID )
{
	if(!lpHtmlDocument)
		return;

	IHTMLElement *lpBodyElm;
	IHTMLBodyElement *lpBody;
	IHTMLTxtRange *lpTxtRange;

	lpHtmlDocument->get_body(&lpBodyElm);
	if(!lpBodyElm)
		return;
	//lpHtmlDocument->Release();

	lpBodyElm->QueryInterface(IID_IHTMLBodyElement,(void**)&lpBody);
	lpBodyElm->Release();

	if(lpBody)
	{
		lpBody->createTextRange(&lpTxtRange);

		CComBSTR html=NULL;
		CComBSTR newhtml=NULL;
		CComBSTR search(searchText.copy(false));
		long t;
		VARIANT_BOOL bFound;
		HRESULT h;

		//		lpTxtRange->get_htmlText(&html);
		while(lpTxtRange->findText(search,0,lFlags,&bFound)==S_OK && bFound==VARIANT_TRUE)
		{
			newhtml.Empty();
			html.Empty();
			h = lpTxtRange->get_text(&html);
			if (!SUCCEEDED(h))break;
			newhtml	.Append(_T("<span id=\""));
			newhtml.Append((LPCTSTR)searchID);
			newhtml.Append(_T("\" style=\""));
			newhtml.Append((LPCTSTR)matchStyle);
			newhtml.Append(_T("\" class=\"DeskAlertsSearch\">"));
			if(searchText==_bstr_t(_T(" ")))
				newhtml.Append("&nbsp;"); // doesn't work very well, but prevents (some) troubles
			else
				newhtml.AppendBSTR(html);
			newhtml.Append(_T("</span>"));
			SysFreeString(html);
			h = lpTxtRange->pasteHTML(newhtml);
			//lpTxtRange->scrollIntoView();
			newhtml.Empty();
			if(!SUCCEEDED(h))break;
			h = lpTxtRange->moveStart((BSTR)CComBSTR(_T("Character")),1,&t);
			if(!SUCCEEDED(h))break;
			h = lpTxtRange->moveEnd((BSTR)CComBSTR(_T("Textedit")),1,&t);
			if(!SUCCEEDED(h))break;;
		}
		lpBody->Release();
		lpTxtRange->Release();
	}

	{

		////////////////////////////////
		IOleContainer* pContainer;

		// Get the container
		HRESULT hr = lpHtmlDocument->QueryInterface(IID_IOleContainer,
			(void**)&pContainer);
		if (FAILED(hr))
			return ;
		IEnumUnknown* pEnumerator;

		// Get an enumerator for the frames
		hr = pContainer->EnumObjects(OLECONTF_EMBEDDINGS, &pEnumerator);
		pContainer->Release();

		if (FAILED(hr))
			return ;

		IUnknown* pUnk;
		ULONG uFetched;

		// Enumerate and refresh all the frames
		for (UINT i = 0; S_OK == pEnumerator->Next(1, &pUnk, &uFetched); i++)
		{
			IWebBrowser2* pBrowser;

			hr = pUnk->QueryInterface(IID_IWebBrowser2, (void**)&pBrowser);
			pUnk->Release();

			if (SUCCEEDED(hr))
			{
				// Refresh the frame
				IDispatch*  lpDisp = NULL;
				IHTMLDocument2*  lpHtmlDocument2 = NULL;
				/*const HRESULT hr = */pBrowser->get_Document(&lpDisp);
				if(lpDisp)
					lpDisp->QueryInterface(IID_IHTMLDocument2, (void**)&lpHtmlDocument2);
				CComPtr<IHTMLWindow2> win = NULL;
				if (lpHtmlDocument2)
				{
					lpHtmlDocument2->get_parentWindow(&win);
					FindText2(lpHtmlDocument2, searchText, lFlags, matchStyle, searchID);
					lpHtmlDocument2->Release();
				}
				pBrowser->Release();
			}
		}

		pEnumerator->Release();
	}
}
