#pragma once

#include "XMLParser.h"


struct NodeALERTS: public XmlNode
{
	struct NodeCOMMANDS : public XmlNode
	{
		struct NodeCOMMAND : public XmlNode
		{
			CString m_name;
			CString m_autostart;
			NodeCOMMAND()
			{
				m_tagName = _T("COMMAND");
			}
			void Determine(XMLParser *pParser)
			{
				XmlNode::Determine(pParser);
			}
			void DetermineAttrib(XMLParser *pParser)
			{
				XmlNode::DetermineAttrib(pParser);
				pParser->DetermineString(_T("name"),&m_name);
				pParser->DetermineString(_T("autostart"),&m_autostart);
			}
		};
		//<UNINSTALL name="uninstall"/>
		struct NodeUNINSTALL : public NodeCOMMAND
		{
			NodeUNINSTALL()
			{
				m_tagName = _T("UNINSTALL");
			}
			void Determine(XMLParser *pParser)
			{
				NodeCOMMAND::Determine(pParser);
			}
			void DetermineAttrib(XMLParser *pParser)
			{
				NodeCOMMAND::DetermineAttrib(pParser);
			}
		};
		//<UPDATE name="update"/>
		struct NodeUPDATE : public NodeCOMMAND
		{
			NodeUPDATE()
			{
				m_tagName = _T("UPDATE");
			}
			void Determine(XMLParser *pParser)
			{
				NodeCOMMAND::Determine(pParser);
			}
			void DetermineAttrib(XMLParser *pParser)
			{
				NodeCOMMAND::DetermineAttrib(pParser);
			}
		};
		//<DISABLE name="disable" alert="alert"/>
		struct NodeDISABLE : public NodeCOMMAND
		{
			CString m_alert;
			NodeDISABLE()
			{
				m_tagName = _T("DISABLE");
			}
			void Determine(XMLParser *pParser)
			{
				NodeCOMMAND::Determine(pParser);
			}
			void DetermineAttrib(XMLParser *pParser)
			{
				NodeCOMMAND::DetermineAttrib(pParser);
				pParser->DetermineString(_T("alert"),&m_alert);
			}
		};

		//<UNREAD name="unread" yes="history" no="sysmenu"/>
		struct NodeUNREAD : public NodeCOMMAND
		{
			CString m_yes;
			CString m_no;
			NodeUNREAD()
			{
				m_tagName = _T("UNREAD");
			}
			void Determine(XMLParser *pParser)
			{
				NodeCOMMAND::Determine(pParser);
			}
			void DetermineAttrib(XMLParser *pParser)
			{
				NodeCOMMAND::DetermineAttrib(pParser);
				pParser->DetermineString(_T("yes"),&m_yes);
				pParser->DetermineString(_T("no"),&m_no);
			}
		};

		//<UNREAD name="unread" yes="history" no="sysmenu"/>
		struct NodeSYSMENU : public NodeCOMMAND
		{
			NodeSYSMENU()
			{
				m_tagName = _T("SYSMENU");
			}
			void Determine(XMLParser *pParser)
			{
				NodeCOMMAND::Determine(pParser);
			}
			void DetermineAttrib(XMLParser *pParser)
			{
				NodeCOMMAND::DetermineAttrib(pParser);
			}
		};

		struct NodeSTANDBY : public NodeCOMMAND
		{
			CString m_sExpire;
			NodeSTANDBY()
			{
				m_tagName = _T("STANDBY");
			}
			void Determine(XMLParser *pParser)
			{
				NodeCOMMAND::Determine(pParser);
			}
			void DetermineAttrib(XMLParser *pParser)
			{
				NodeCOMMAND::DetermineAttrib(pParser);
				pParser->DetermineString(_T("expire"),&m_sExpire);
			}
		};
		//<BROWSERJAMP name="webjamp" filename="%firstURL%"/>
		struct NodeBROWSERJAMP : public NodeCOMMAND
		{
			CString m_filename;
			CString m_post;
			int m_encoding;
			NodeBROWSERJAMP() : m_encoding(0)
			{
				m_tagName = _T("BROWSERJAMP");
			}
			void Determine(XMLParser *pParser)
			{
				NodeCOMMAND::Determine(pParser);
			}
			void DetermineAttrib(XMLParser *pParser)
			{
				NodeCOMMAND::DetermineAttrib(pParser);
				pParser->DetermineString(_T("filename"),&m_filename);
				pParser->DetermineString(_T("post"),&m_post);
				pParser->DetermineInt(_T("encoding"),&m_encoding);
			}
		};

		struct NodeOPENWINDOW : public NodeBROWSERJAMP
		{
			CString m_window;
			CString m_expire;
			NodeOPENWINDOW()
			{
				m_tagName = _T("OPENWINDOW");
			}
			void Determine(XMLParser *pParser)
			{
				NodeCOMMAND::Determine(pParser);
			}
			void DetermineAttrib(XMLParser *pParser)
			{
				NodeBROWSERJAMP::DetermineAttrib(pParser);
				pParser->DetermineString(_T("window"),&m_window);
				pParser->DetermineString(_T("expire"),&m_expire);
			}
		};

		//<MINIBROWSERJAMP name="minibrowser" window="minibrowserwindow" filename="%firstURL%"/>
		struct NodeMINIBROWSERJAMP : public NodeOPENWINDOW
		{
			NodeMINIBROWSERJAMP()
			{
				m_tagName = _T("MINIBROWSERJAMP");
			}
			void Determine(XMLParser *pParser)
			{
				NodeOPENWINDOW::Determine(pParser);
			}
			void DetermineAttrib(XMLParser *pParser)
			{
				NodeOPENWINDOW::DetermineAttrib(pParser);
			}
		};

		//<OPTION name="option" window="optionwindow1" filename="%root_path%\options.html"/>
		struct NodeOPTION : public NodeOPENWINDOW
		{
			NodeOPTION()
			{
				m_tagName = _T("OPTION");
			}
			void Determine(XMLParser *pParser)
			{
				NodeOPENWINDOW::Determine(pParser);
			}
			void DetermineAttrib(XMLParser *pParser)
			{
				NodeOPENWINDOW::DetermineAttrib(pParser);
			}
		};
		//<HISTORY name="history" window="historywindow1" filename="%root_path%\history.html"/>
		struct NodeHISTORY : public NodeOPENWINDOW
		{
			NodeHISTORY()
			{
				m_tagName = _T("HISTORY");
			}
			void Determine(XMLParser *pParser)
			{
				NodeOPENWINDOW::Determine(pParser);
			}
			void DetermineAttrib(XMLParser *pParser)
			{
				NodeOPENWINDOW::DetermineAttrib(pParser);
			}
		};

		struct NodeALERTCOMMAND : public NodeOPENWINDOW
		{
			struct NodeALERTWINDOW : public XmlNode
			{
				CString m_window;
				NodeALERTWINDOW()
				{
					m_tagName = _T("ALERTWINDOW");
				}
				void Determine(XMLParser *pParser)
				{
					XmlNode::Determine(pParser);
				}
				void DetermineAttrib(XMLParser *pParser)
				{
					XmlNode::DetermineAttrib(pParser);
					pParser->DetermineString(_T("name"),&m_window);
				}
			};
			struct NodeALERTRUN : public XmlNode
			{
				CString m_command;
				NodeALERTRUN()
				{
					m_tagName = _T("ALERTRUN");
				}
				void Determine(XMLParser *pParser)
				{
					XmlNode::Determine(pParser);
				}
				void DetermineAttrib(XMLParser *pParser)
				{
					XmlNode::DetermineAttrib(pParser);
					pParser->DetermineString(_T("command"),&m_command);
				}
			};
			CString m_history;
			CString m_expire;
			NodeALERTCOMMAND()
			{
				m_tagName = _T("ALERT");
			}
			void Determine(XMLParser *pParser)
			{
				NodeOPENWINDOW::Determine(pParser);
				DetermineNodeVector<NodeALERTWINDOW>(pParser,_T("WINDOW"),&m_windows);
				DetermineNodeVector<NodeALERTRUN>(pParser,_T("RUN"),&m_runs);
			}
			void DetermineAttrib(XMLParser *pParser)
			{
				NodeOPENWINDOW::DetermineAttrib(pParser);
				pParser->DetermineString(_T("expire"),&m_expire);
				pParser->DetermineString(_T("history"),&m_history);
			}
			vector<NodeALERTWINDOW*> m_windows;
			vector<NodeALERTRUN*> m_runs;
		};

		//<EXIT name="exit"/>
		struct NodeEXIT : public NodeCOMMAND
		{
			NodeEXIT()
			{
				m_tagName = _T("EXIT");
			}
			void Determine(XMLParser *pParser)
			{
				NodeCOMMAND::Determine(pParser);
			}
			void DetermineAttrib(XMLParser *pParser)
			{
				NodeCOMMAND::DetermineAttrib(pParser);
			}
		};

		//<RUNAPP name="ranapp">
			//<WINDOWS file="" working_directory="" params=""  />
			//<!--WINDOWS path="notepad.exe" /-->
			//<MAC file="" />
		//</RUNAPP>
		struct NodeRUNAPP : public NodeCOMMAND
		{
			struct NodeWINDOWS : public XmlNode
			{
				CString m_file;
				CString m_workdir;
				CString m_params;
				NodeWINDOWS()
				{
					m_tagName = _T("WINDOWS");
				}
				void Determine(XMLParser *pParser)
				{
					XmlNode::Determine(pParser);
				}
				void DetermineAttrib(XMLParser *pParser)
				{
					XmlNode::DetermineAttrib(pParser);
					pParser->DetermineString(_T("file"),&m_file);
					pParser->DetermineString(_T("working_directory"),&m_workdir);
					pParser->DetermineString(_T("params"),&m_params);
				}
			};

			NodeRUNAPP()
			{
				m_tagName = _T("RUNAPP");
			}
			void Determine(XMLParser *pParser)
			{
				NodeCOMMAND::Determine(pParser);

				DetermineNodeVector<NodeWINDOWS>(pParser,_T("WINDOWS"),(vector <NodeWINDOWS*>*)&m_windows);
			}
			void DetermineAttrib(XMLParser *pParser)
			{
				NodeCOMMAND::DetermineAttrib(pParser);
			}

			vector<NodeWINDOWS*> m_windows;
		};

		NodeCOMMANDS()
		{
			m_tagName = _T("COMMANDS");
		}
		void Determine(XMLParser *pParser)
		{
			XmlNode::Determine(pParser);

			DetermineNodeVector<NodeUNINSTALL>(pParser,_T("UNINSTALL"),(vector <NodeUNINSTALL*>*)&m_comand);
			DetermineNodeVector<NodeUPDATE>(pParser,_T("UPDATE"),(vector <NodeUPDATE*>*)&m_comand);
			DetermineNodeVector<NodeDISABLE>(pParser,_T("DISABLE"),(vector <NodeDISABLE*>*)&m_comand);
			DetermineNodeVector<NodeBROWSERJAMP>(pParser,_T("BROWSERJUMP"),(vector <NodeBROWSERJAMP*>*)&m_comand);
			DetermineNodeVector<NodeBROWSERJAMP>(pParser,_T("BROWSERJAMP"),(vector <NodeBROWSERJAMP*>*)&m_comand);
			DetermineNodeVector<NodeMINIBROWSERJAMP>(pParser,_T("MINIBROWSERJUMP"),(vector <NodeMINIBROWSERJAMP*>*)&m_comand);
			DetermineNodeVector<NodeMINIBROWSERJAMP>(pParser,_T("MINIBROWSERJAMP"),(vector <NodeMINIBROWSERJAMP*>*)&m_comand);
			DetermineNodeVector<NodeOPENWINDOW>(pParser,_T("OPENWINDOW"),(vector <NodeOPENWINDOW*>*)&m_comand);
			DetermineNodeVector<NodeOPTION>(pParser,_T("OPTION"),(vector <NodeOPTION*>*)&m_comand);
			DetermineNodeVector<NodeHISTORY>(pParser,_T("HISTORY"),(vector <NodeHISTORY*>*)&m_comand);
			DetermineNodeVector<NodeALERTCOMMAND>(pParser,_T("ALERT"),(vector <NodeALERTCOMMAND*>*)&m_comand);
			DetermineNodeVector<NodeRUNAPP>(pParser,_T("RUNAPP"),(vector <NodeRUNAPP*>*)&m_comand);
			DetermineNodeVector<NodeEXIT>(pParser,_T("EXIT"),(vector <NodeEXIT*>*)&m_comand);
			DetermineNodeVector<NodeSTANDBY>(pParser,_T("STANDBY"),(vector <NodeSTANDBY*>*)&m_comand);
			DetermineNodeVector<NodeUNREAD>(pParser,_T("UNREAD"),(vector <NodeUNREAD*>*)&m_comand);
			DetermineNodeVector<NodeSYSMENU>(pParser,_T("SYSMENU"),(vector <NodeSYSMENU*>*)&m_comand);
		}
		void DetermineAttrib(XMLParser *pParser)
		{
			XmlNode::DetermineAttrib(pParser);
		}
		vector<NodeCOMMAND*>  m_comand;
	};

	struct NodeSETTINGS : public XmlNode
	{
		struct NodeSETTING : public XmlNode
		{
			CString m_id;
			CString m_default;

			NodeSETTING()
			{
				m_tagName = _T("SETTING");
			}
			void Determine(XMLParser *pParser)
			{
				XmlNode::Determine(pParser);
			}
			void DetermineAttrib(XMLParser *pParser)
			{
				XmlNode::DetermineAttrib(pParser);
				pParser->DetermineString(_T("id"),&m_id);
				pParser->DetermineString(_T("default"),&m_default);
			}
		};
		struct NodePROPERTY : public NodeSETTING
		{
			CString m_const;
			NodePROPERTY()
			{
				m_tagName = _T("PROPERTY");
			}
			void Determine(XMLParser *pParser)
			{
				NodeSETTING::Determine(pParser);
			}
			void DetermineAttrib(XMLParser *pParser)
			{
				NodeSETTING::DetermineAttrib(pParser);
				pParser->DetermineString(_T("const"),&m_const);
			}
		};

		struct NodeREGISTRY : public NodePROPERTY
		{
			CString m_path;
			CString m_variable;
			CString m_type;
			CString m_format;
			CString m_wow6432;
			NodeREGISTRY()
			{
				m_tagName = _T("REGISTRY");
			}
			void Determine(XMLParser *pParser)
			{
				NodePROPERTY::Determine(pParser);
			}
			void DetermineAttrib(XMLParser *pParser)
			{
				NodePROPERTY::DetermineAttrib(pParser);
				pParser->DetermineString(_T("path"),&m_path);
				pParser->DetermineString(_T("variable"),&m_variable);
				pParser->DetermineString(_T("type"),&m_type);
				pParser->DetermineString(_T("format"),&m_format);
				pParser->DetermineString(_T("wow6432"),&m_wow6432);
			}
		};

		NodeSETTINGS()
		{
			m_tagName = _T("SETTINGS");
		}

		void Determine(XMLParser *pParser)
		{
			XmlNode::Determine(pParser);
			DetermineNodeVector<NodePROPERTY>(pParser,_T("PROPERTY"),(vector <NodePROPERTY*>*)&m_property);
			DetermineNodeVector<NodePROPERTY>(pParser,_T("MESSAGE"),(vector <NodePROPERTY*>*)&m_property);
			DetermineNodeVector<NodePROPERTY>(pParser,_T("URL"),(vector <NodePROPERTY*>*)&m_property);
			DetermineNodeVector<NodePROPERTY>(pParser,_T("CHECKBOX"),(vector <NodePROPERTY*>*)&m_property);
			DetermineNodeVector<NodePROPERTY>(pParser,_T("TEXT"),(vector <NodePROPERTY*>*)&m_property);
			DetermineNodeVector<NodeREGISTRY>(pParser,_T("REGISTRY"),(vector <NodeREGISTRY*>*)&m_registry);
		}
		void DetermineAttrib(XMLParser *pParser)
		{
			XmlNode::DetermineAttrib(pParser);
		}
		vector<NodePROPERTY*> m_property;
		vector<NodeREGISTRY*> m_registry;
	};

	struct NodeVIEWS : public XmlNode
	{
		struct NodeVIEW : public XmlNode
		{
			CString m_name;
			NodeVIEW()
			{
				m_tagName = _T("VIEW");
			}
			void Determine(XMLParser *pParser)
			{
				XmlNode::Determine(pParser);
			}
			void DetermineAttrib(XMLParser *pParser)
			{
				XmlNode::DetermineAttrib(pParser);
				pParser->DetermineString(_T("name"),&m_name);
			}
		};

		struct NodeITEM : public NodeVIEW
		{
			CString m_command;
			CString m_img;
			CString m_standbyimg;
			CString m_disableimg;
			CString m_disablenewimg;
			CString m_offlineimg;
			CString m_caption;
			CString m_pressed_caption;
			CString m_pressed_img;
			CString m_unread_img;

			NodeITEM()
			{
				m_tagName = _T("ITEM");
			}
			void Determine(XMLParser *pParser)
			{
				NodeVIEW::Determine(pParser);
			}
			void DetermineAttrib(XMLParser *pParser)
			{
				NodeVIEW::DetermineAttrib(pParser);
				pParser->DetermineString(_T("command"),&m_command);
				pParser->DetermineString(_T("img"),&m_img);
				pParser->DetermineString(_T("standby_img"),&m_standbyimg);
				pParser->DetermineString(_T("disable_img"),&m_disableimg);
				pParser->DetermineString(_T("unreaded_img"),&m_disablenewimg);
				pParser->DetermineString(_T("offline_img"),&m_offlineimg);
				pParser->DetermineString(_T("caption"),&m_caption);
				pParser->DetermineString(_T("pressed_caption"),&m_pressed_caption);
				pParser->DetermineString(_T("pressed_img"),&m_pressed_img);
				pParser->DetermineString(_T("unread_img"),&m_unread_img);
			}
		};

		struct NodeSEPARATOR : public NodeITEM
		{
			NodeSEPARATOR()
			{
				m_tagName = _T("SEPARATOR");
			}
			void Determine(XMLParser *pParser)
			{
				NodeITEM::Determine(pParser);
			}
			void DetermineAttrib(XMLParser *pParser)
			{
				NodeITEM::DetermineAttrib(pParser);
			}
		};

		struct NodeMENU : public NodeITEM
		{
			NodeMENU()
			{
				m_tagName = _T("MENU");
			}
			void Determine(XMLParser *pParser)
			{
				NodeITEM::Determine(pParser);
				DetermineNodeVector<NodeMENU>(pParser,_T("MENU"),(vector <NodeMENU* >*)&m_item);
				DetermineNodeVector<NodeITEM>(pParser,_T("ITEM"),(vector <NodeITEM* >*)&m_item);
				DetermineNodeVector<NodeSEPARATOR>(pParser,_T("SEPARATOR"),(vector <NodeSEPARATOR* >*)&m_item);
			}
			void DetermineAttrib(XMLParser *pParser)
			{
				NodeITEM::DetermineAttrib(pParser);
			}
			vector<NodeITEM* > m_item;
		};


		struct NodeSYSMENU : public NodeMENU
		{
			CString m_icons;
			NodeSYSMENU()
			{
				m_tagName = _T("SYSMENU");
			}
			void Determine(XMLParser *pParser)
			{
				NodeMENU::Determine(pParser);
			}
			void DetermineAttrib(XMLParser *pParser)
			{
				NodeMENU::DetermineAttrib(pParser);
				pParser->DetermineString(_T("icons"),&m_icons);
			}
		};

		struct NodeWINDOW : public NodeVIEW
		{
			CString m_command;
			CString m_img;
			CString m_captionhref, m_captionhref_def;
			CString m_skinId, m_skinId_def;
			CString m_caption;
			CString m_customjs, m_customjs_def;
			CString m_customcss, m_customcss_def;
			CString m_bodyclassname;
			CString m_frameclassname;

			CString m_width;
			CString m_height;

			CString m_leftmargin;
			CString m_topmargin;
			CString m_rightmargin;
			CString m_bottommargin;
			CString m_confirmation;
			CString m_position;
			CString m_ticker;
			CString m_docked;

			CString m_transparency;
			CString m_transpcolor;

			CString m_visible;
			CString m_single;
			CString m_lockdesktop;

			CString m_acknowledgement;
			CString m_create_date;
			CString m_date;
			CString m_title;
			CString m_autoclose;
			CString m_alertid;
			CString m_topTemplateHTML;
			CString m_bottomTemplateHTML;
			CString m_searchTerm;
			CString m_resizable;
			CString m_self_deletable;
			CString m_urgent;

			CString m_expire;

			CString m_skip_https_check;

			CString m_to_date;
			CString m_hide_close;

			int m_iWindowCount;
			int m_windowsCountDelta;

			size_t m_urgentWindowsInOneTime;
			size_t m_usualWindowsInOneTime;

			BOOL m_UTC;

			NodeWINDOW() : m_iWindowCount(0), m_windowsCountDelta(0), m_UTC(FALSE),
				m_urgentWindowsInOneTime(60), m_usualWindowsInOneTime(30)
			{
				m_tagName = _T("WINDOW");
			}
			void Determine(XMLParser *pParser)
			{
				NodeVIEW::Determine(pParser);
			}
			void DetermineAttrib(XMLParser *pParser)
			{
				NodeVIEW::DetermineAttrib(pParser);
				pParser->DetermineString(_T("command"),&m_command);
				pParser->DetermineString(_T("img"),&m_img);
				pParser->DetermineString(_T("captionhref"),&m_captionhref);
				pParser->DetermineString(_T("skin_id"),&m_skinId);
				pParser->DetermineString(_T("caption"),&m_caption);
				pParser->DetermineString(_T("customjs"),&m_customjs);
				pParser->DetermineString(_T("customcss"),&m_customcss);
				pParser->DetermineString(_T("bodyclassname"), &m_bodyclassname);
				pParser->DetermineString(_T("frameclassname"), &m_frameclassname);

				pParser->DetermineString(_T("width"),&m_width);
				pParser->DetermineString(_T("height"),&m_height);

				pParser->DetermineString(_T("leftmargin"),&m_leftmargin);
				pParser->DetermineString(_T("topmargin"),&m_topmargin);
				pParser->DetermineString(_T("rightmargin"),&m_rightmargin);
				pParser->DetermineString(_T("bottommargin"),&m_bottommargin);
				pParser->DetermineString(_T("confirmation"),&m_confirmation);
				pParser->DetermineString(_T("position"),&m_position);
				pParser->DetermineString(_T("docked"),&m_docked);
				pParser->DetermineString(_T("self_deletable"),&m_self_deletable);

				pParser->DetermineString(_T("transparency"),&m_transparency);
				pParser->DetermineString(_T("transp_color"),&m_transpcolor);

				pParser->DetermineString(_T("visible"),&m_visible);
				pParser->DetermineString(_T("single"),&m_single);
				pParser->DetermineString(_T("lockdesktop"),&m_lockdesktop);
				pParser->DetermineString(_T("to_date"), &m_to_date);
				pParser->DetermineString(_T("hide_close"), &m_hide_close);
				pParser->DetermineString(_T("expire"),&m_expire);

				pParser->DetermineString(_T("skip_https_check"),&m_skip_https_check);

				pParser->DetermineUInt(_T("urgents"),&m_urgentWindowsInOneTime,60);
				pParser->DetermineUInt(_T("usuals"),&m_usualWindowsInOneTime,30);

				m_captionhref_def = m_captionhref;
				m_customjs_def = m_customjs;
				m_customcss_def = m_customcss;
				m_skinId_def = m_skinId;
			}
		};

		NodeVIEWS()
		{
			m_tagName = _T("VIEWS");
		}
		void Determine(XMLParser *pParser)
		{
			XmlNode::Determine(pParser);
			DetermineNodeVector<NodeSYSMENU>(pParser,_T("SYSMENU"),(vector <NodeSYSMENU* >*)&m_view);
			DetermineNodeVector<NodeWINDOW>(pParser,_T("WINDOW"),(vector <NodeWINDOW* >*)&m_view);
		}
		void DetermineAttrib(XMLParser *pParser)
		{
			XmlNode::DetermineAttrib(pParser);
		}
		vector<NodeVIEW*> m_view;
	};

	NodeCOMMANDS* m_comands;
	NodeSETTINGS* m_settings;
	NodeVIEWS* m_views;
	CString m_name;
	CString m_icons;
	CString m_hot_icons;
	int m_encoding;

	NodeALERTS() : m_comands(NULL), m_settings(NULL), m_views(NULL), m_encoding(0)
	{
		m_tagName = _T("ALERTS");
	}

	void Determine(XMLParser *pParser)
	{
		XmlNode::Determine(pParser);
		DetermineNode<NodeCOMMANDS>(pParser,_T("COMMANDS"),&m_comands);
		DetermineNode<NodeSETTINGS>(pParser,_T("SETTINGS"),&m_settings);
		DetermineNode<NodeVIEWS>(pParser,_T("VIEWS"),&m_views);
	}
	void DetermineAttrib(XMLParser *pParser)
	{
		XmlNode::DetermineAttrib(pParser);
		pParser->DetermineString(_T("name"),&m_name);
		pParser->DetermineString(_T("icons"),&m_icons);
		pParser->DetermineString(_T("hot_icons"),&m_hot_icons);
		pParser->DetermineInt(_T("encoding"), &m_encoding, CP_UTF8);
	}
};