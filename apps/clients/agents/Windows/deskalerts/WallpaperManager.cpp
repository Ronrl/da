#include "stdafx.h"
#include "WallpaperManager.h"
#include "DatabaseManagr.h"
#include "WallpaperAlert.h"
#include "AlertUtils.h"
#include <vector>
#include <gdiplus.h>
#include <shlobj.h>

CWallpaperManager::CWallpaperManager(void): m_thread(NULL), m_threadId(0)
{
	Create();
	m_thread = CreateThread(NULL, 0, CWallpaperManager::WallpaperThread,NULL, 0, &m_threadId);
}

CWallpaperManager::~CWallpaperManager(void)
{
	if (m_thread)
	{
		restoreUserWallpaper();
		DWORD exitCode;
		if(GetExitCodeThread(m_thread, &exitCode))
		{
			if (exitCode == STILL_ACTIVE)
			{
				const HANDLE event = CreateEvent(NULL, TRUE, FALSE, NULL);
                if (event != nullptr)
                {
                    if (!::PostThreadMessage(m_threadId, UWM_STOP_THREAD, 0, (LPARAM)event) ||
                        WaitForSingleObject(event, 5000) == WAIT_OBJECT_0)
                    {
                        CloseHandle(event);
                    }
                }
			}
		}
		CloseHandle(m_thread);
	}
	if (IsWindow())
		DestroyWindow();
}

void CWallpaperManager::setWallpaper(CString position, CString fileName, BOOL absolutePath)
{
	WallpaperStruct *wpSt = new WallpaperStruct();
	wpSt->position = position;
	wpSt->fileName = fileName;
	wpSt->absolutePath = absolutePath;

	PostThreadMessageDelWParam<WallpaperStruct>(m_threadId, UTM_SET_WALLPAPER, wpSt, NULL);
}

DWORD WINAPI CWallpaperManager::WallpaperThread(LPVOID /*pv*/)
{
	DWORD res = 1;
	release_try
	{
		MSG msg;
		while(GetMessage(&msg, NULL, 0, 0) > 0) //important to check > 0, see MSDN
		{
			release_try
			{
				if(msg.message == UWM_STOP_THREAD)
				{
					SetEvent((HANDLE)msg.lParam);
					break;
				}
				else if(msg.message == UTM_SET_WALLPAPER)
				{
					WallpaperStruct *wpSt = (WallpaperStruct*)msg.wParam;
					CString position = wpSt->position; 
					CString fileName = wpSt->fileName;
					BOOL absolutePath = wpSt->absolutePath;
					
					CString imgPath;
					if(absolutePath)
					{
						imgPath = fileName;
					}
					else
					{
						imgPath = CAlertUtils::getWallpapersPath();
						imgPath = imgPath + _T("\\") + fileName;
					}

					CRegKey key = CAlertUtils::getDesktopRegKey();
					if(position == _T("1"))
					{
						key.SetStringValue(_T("WallpaperStyle"),_T("0"));
						key.SetStringValue(_T("TileWallpaper"),_T("0"));
					}
					else if(position == _T("2"))
					{
						key.SetStringValue(_T("WallpaperStyle"),_T("0"));
						key.SetStringValue(_T("TileWallpaper"),_T("1"));
					}
					else if(position == _T("3"))
					{
						key.SetStringValue(_T("WallpaperStyle"),_T("2"));
						key.SetStringValue(_T("TileWallpaper"),_T("0"));
					}
					else if(position == _T("4"))
					{
						key.SetStringValue(_T("WallpaperStyle"),_T("10"));
						key.SetStringValue(_T("TileWallpaper"),_T("0"));
					}
					else if(position == _T("5"))
					{
						key.SetStringValue(_T("WallpaperStyle"),_T("6"));
						key.SetStringValue(_T("TileWallpaper"),_T("0"));
					}
                    else if (position == _T("6"))
                    {
                        key.SetStringValue(_T("WallpaperStyle"), _T("22"));
                        key.SetStringValue(_T("TileWallpaper"), _T("0"));
                    }
					if(imgPath == _T("solid_color"))
					{
						imgPath.Empty();
					}
					SystemParametersInfo(SPI_SETDESKWALLPAPER, 0, (LPWSTR)imgPath.GetString(), SPIF_SENDWININICHANGE|SPIF_UPDATEINIFILE);
					
					if(!IsWindowsVistaOrGreater())
					{
						CComPtr<IActiveDesktop> pDesk;
						if(SUCCEEDED(pDesk.CoCreateInstance(CLSID_ActiveDesktop)) && pDesk)
						{
							pDesk->ApplyChanges(AD_APPLY_ALL|AD_APPLY_FORCE);
						}
						//::PostMessage(FindWindow(_T("Progman"),_T("Program Manager")),WM_COMMAND,MAKEWPARAM(41504,1),0);
					}
					if(wpSt) delete_catch(wpSt);
				}
				else
				{
					::TranslateMessage(&msg);
					::DispatchMessage(&msg);
				}
			}
			release_catch_expr_and_tolog(res = 3, _T("WallpaperManager loop exception"))
		}
	}
	release_catch_expr_and_tolog(res = 3, _T("WallpaperManager thread exception"))

	return res;
}

void CWallpaperManager::showWallpapers()
{
	KillTimer(_WALLPAPERS_TIMER_);
	std::vector<CWallpaperAlert*>::iterator it = m_alerts.begin();

	while(it != m_alerts.end())
	{
		delete_catch((*it));
		it = m_alerts.erase(it);
	}

	CDatabaseManager *db = CDatabaseManager::shared(true);
	if (db)
	{
		db->getWallpaperAlerts(m_alerts);
	}

	m_curIt = m_alerts.begin();
	
	if(m_curIt != m_alerts.end())
	{
		CString userWallpaper = _iAlertModel->getPropertyString(CString(_T("user_wallpaper")),&CString(_T("")));

		if (userWallpaper.IsEmpty())
		{
			//	TCHAR oldWallPaper[MAX_PATH];
			//	SystemParametersInfo(SPI_GETDESKWALLPAPER, MAX_PATH, &oldWallPaper, 0);
			CRegKey key = CAlertUtils::getDesktopRegKey();
			DWORD valueLength;
			CString style;
			CString tile;
			CString oldWallPaper;
			key.QueryStringValue(_T("Wallpaper"), oldWallPaper.GetBufferSetLength(valueLength = MAX_PATH), &valueLength);
			key.QueryStringValue(_T("WallpaperStyle"), style.GetBufferSetLength(valueLength = MAX_PATH), &valueLength);
			key.QueryStringValue(_T("TileWallpaper"), tile.GetBufferSetLength(valueLength = MAX_PATH), &valueLength);

			if (oldWallPaper.IsEmpty())
			{
				oldWallPaper = _T("solid_color");
			}
			_iAlertModel->setPropertyString(CString(_T("user_wallpaper")),CString(oldWallPaper));

			CString position;
			if (style == _T("0") && tile == _T("0"))
			{
				position =_T("1");
			}
			else if (style == _T("0") && tile == _T("1"))
			{
				position =_T("2");
			}
			else if (style == _T("2") && tile == _T("0"))
			{
				position =_T("3");
			}
			else if (style == _T("10") && tile == _T("0"))
			{
				position =_T("4");
			}
			else if (style == _T("6") && tile == _T("0"))
			{
				position =_T("5");
			}
            else if (style == _T("20") && tile == _T("0"))
            {
                position = _T("6");
            }
			_iAlertModel->setPropertyString(CString(_T("user_wallpaper_position")),position);
		}

		setWallpaper((*m_curIt)->m_position,(*m_curIt)->m_fileName);
		SetTimer(_WALLPAPERS_TIMER_,((*m_curIt)->m_autoclose == 0) ? USER_TIMER_MAXIMUM :(*m_curIt)->m_autoclose*1000,NULL);
	}
	else
	{
		restoreUserWallpaper();
	}
}

LRESULT CWallpaperManager::OnTimer(UINT /*uMsg*/, WPARAM wParam, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	if (wParam == _WALLPAPERS_TIMER_)
	{
		m_curIt++;
		if (m_curIt == m_alerts.end()) m_curIt = m_alerts.begin();
		setWallpaper((*m_curIt)->m_position,(*m_curIt)->m_fileName);
		KillTimer(_WALLPAPERS_TIMER_);
		SetTimer(_WALLPAPERS_TIMER_,((*m_curIt)->m_autoclose == 0) ? USER_TIMER_MAXIMUM :(*m_curIt)->m_autoclose*1000,NULL);
	}
	return S_OK;
}

void CWallpaperManager::restoreUserWallpaper()
{
	CString userWallpaper = _iAlertModel->getPropertyString(CString(_T("user_wallpaper")),&CString(_T("")));
	if (!userWallpaper.IsEmpty())
	{
		CString position = _iAlertModel->getPropertyString(CString(_T("user_wallpaper_position")),&CString(_T("")));
		setWallpaper(position,userWallpaper,TRUE);
	}
	_iAlertModel->setPropertyString(CString(_T("user_wallpaper")),CString(_T("")));
}
