// IPCTraceMacros.h: include file IPCTraceMacros.
//

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef REALDEBUGDEFINED
#define REALDEBUGDEFINED

#if (defined _DEBUG || defined _IPCRELEASETRACE)
#define IPCTRACE(message)                                           \
{                                                                   \
    HWND hReceiver = ::FindWindow(NULL, _T("IPCTrace"));            \
    if (hReceiver)                                                  \
    {                                                               \
        COPYDATASTRUCT cds;                                         \
        ZeroMemory(&cds, sizeof(COPYDATASTRUCT));                   \
        cds.dwData = 0x00031337;                                    \
        cds.cbData = _tcslen(message) + sizeof(TCHAR);              \
        cds.lpData = message;                                       \
        ::SendMessage(hReceiver, WM_COPYDATA, NULL, (LPARAM) &cds); \
    }                                                               \
}
#else // defined _DEBUG || defined _IPCRELEASETRACE
#define IPCTRACE (void(0))
#endif // !_DEBUG

#include <stdarg.h>
#include <tchar.H>
#include <stdio.h>

#ifdef ENABLE_IPCTRACE
#define MyDebug RealDebug
#else
#define MyDebug ((void)__noop)
#endif

void RealDebug(LPCSTR szFormat, ...);

#ifndef LOG_FLAG
//
// Added by Andrey Maximov 27/03/2018
// If this flag is true - the logging is need
//
// int F_LOG = 0;
#endif LOG_FLAG

#define TRACE_MAX_STACK_FRAMES 1024
#define TRACE_MAX_FUNCTION_NAME_LENGTH 1024

extern void appendToErrorFile(const TCHAR *szFormat, ...);
extern void appendToErrorFile_TMP(const TCHAR *szFormat, ...);
extern void logException();
extern void logSystemError(LPCTSTR action, DWORD errCode);
extern void logHttpRequest(LPCTSTR szURL);
extern void logHttpStatus(int nStatusCode);


#endif
