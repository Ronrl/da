#include "stdafx.h"
#include "ContentLoader.h"
#include "stdio.h"
#include <locale.h>
#include "AlertUtils.h"

CContentLoader::CContentLoader()
{
}


//-----------------------------------------------------------------------------------------------------------------------------------
ATL::CString CContentLoader::LoadFromFile(ATL::CString &iFname)
{
	setlocale(LC_ALL, "");
	//open file
	FILE *fp = NULL;
	_tfopen_s(&fp, iFname, _T("r"));
	if(!fp) return "";
	ATL::CString res;
	wchar_t buf[500];
	int size = 500;

	//read to end
	while(!feof(fp))
	{
		if(fgetws(buf, size, fp) == NULL)
			break;
		if(ferror(fp))
			break;
		res += buf;
		size = 500;
	}
	fclose(fp);

	return res;
}


//-----------------------------------------------------------------------------------------------------------------------------------
ATL::CString CContentLoader::GetText(ATL::CString &URL, bool iRefresh)
{
	//check if need refresh
	if(m_body.IsEmpty() || iRefresh || m_URL != URL)
	{
		//download script to memory
		if(URL.Find(_T("://")) >= 0)
		{
			//remote download
			m_body.Empty();
			CAlertUtils::URLDownloadToFile(URL, NULL, &m_body);
		}
		else
		{
			//local download
			m_body = LoadFromFile(URL);
		}
		m_URL = URL;
	}
	//done
	return m_body;
}


//-----------------------------------------------------------------------------------------------------------------------------------
CContentLoader::~CContentLoader()
{
}
