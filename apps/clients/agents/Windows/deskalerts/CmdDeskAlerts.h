//
//  CmdDeskAlerts.h
//  DeskAlerts Client
//  Common module
//
//  Copyright Softomate 2006-2009. All rights reserved.
//

#pragma once

#include "alertcontroller.h"

class CmdDeskAlerts  : public
		PluginCommand<CmdDeskAlerts>
{
	HWND hWnd;
	//tbCtrl variable defined in base class
public:
	CString m_strName;
	Localization Locale;

	PTL_BEGIN_MESSAGE_MAP(Locale)

		DEFAULT_MESSAGE(_T("alerts.header"), _T("DeskAlerts"))
		DEFAULT_MESSAGE(_T("alerts.title"),  _T("Alerts Settings"))
		DEFAULT_MESSAGE(_T("alerts.login"),  _T("User Name:"))
		DEFAULT_MESSAGE(_T("alerts.password"),  _T("User Password:"))
		DEFAULT_MESSAGE(_T("alerts.expire"), _T("Expire period:"))
		DEFAULT_MESSAGE(_T("alerts.sound"),  _T("Play alert sound:"))
		DEFAULT_MESSAGE(_T("alerts.minute"),  _T("min."))

	PTL_END_MESSAGE_MAP()

	PTL_BEGIN_PROPERTY_MAP()
		DETERMINE_PROPERTY(_T("name"),m_strName,TRUE)
		DETERMINE_LOCALE(Locale)
	PTL_END_PROPERTY_MAP()

	PTL_BEGIN_EVENT_MAP()
		EVENT_HANDLER(_T("AfterInit"), OnInit)
		EVENT_HANDLER(_T("AfterReInit"), OnInit)
		EVENT_HANDLER(_T("explorerQuit"), OnQuit)
	PTL_END_EVENT_MAP()

	virtual int Apply(VirtualToolbar *pwndToolbar,bool bIsPressed);
	int OnInit(LPVOID pData);
	int OnQuit(LPVOID pData);
	CAlertController alertController;
	CmdDeskAlerts();
	virtual ~CmdDeskAlerts();
	//	virtual PROPSHEETPAGE* GetPropertyPage();
};
