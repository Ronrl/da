#include "stdafx.h"
#include "CloseWindowDataManager.h"

CloseWindowDataManager::CloseWindowDataManager(void) : m_alertId(NULL)
{
}

CloseWindowDataManager::~CloseWindowDataManager(void)
{
}

void CloseWindowDataManager::setData(INT64 alertId)
{
	m_alertId = alertId;
}

InteractionConstants::RequestDataType CloseWindowDataManager::getDataType()
{
	return REQUESTDATATYPE_CLOSEWINDOW;
}

DWORD CloseWindowDataManager::serialize( IStream* pStream )
{
	CLOSEWINDOWDATA closeWindowData;
	closeWindowData.alertId = m_alertId;
	closeWindowData.pid = GetCurrentProcessId();
	pStream->Write(&closeWindowData, sizeof(CLOSEWINDOWDATA), NULL);
	return 0;
}
