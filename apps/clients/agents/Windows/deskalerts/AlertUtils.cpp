//
//  alertutils.cpp
//  DeskAlerts Client
//  Common module
//
//  Copyright Softomate 2006-2009. All rights reserved.
//

#include "StdAfx.h"
#include ".\alertutils.h"
#include "..\AlertAppl\GlobalSolutionVariables.h"
#include <wininet.h>
#pragma comment(lib,"wininet")

#include <atlrx.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <time.h>
#include "GMUtils.h"
#include "Shlwapi.h"
#include <iepmapi.h>
#include <sddl.h>
#include <AccCtrl.h>
#include <aclapi.h>
#include "Wtsapi32.h"
#include <io.h>
#include <sys/types.h>
#include <sys/stat.h>


_bstr_t sRegistryKey = "ALERTS00001";
_bstr_t sProfileName = "DeskAlerts";

#include "sha1.h"
#include "AlertDecoder.h"
#include "DatabaseManagr.h"

#include "../cryptopp/modes.h"
#include "../cryptopp/aes.h"
#include "../cryptopp/filters.h"
#include "../cryptopp/base64.h"

#define UWM_LIFE_SIGNAL WM_APP + 322
//#include "memory_vc9.0_sp1.h"
#include <memory>
#include <tlhelp32.h>
#ifndef MAKEULONGLONG
#define MAKEULONGLONG(ldw, hdw) ((ULONGLONG(hdw) << 32) | ((ldw) & 0xFFFFFFFF))
#endif

#ifndef MAXULONGLONG
#define MAXULONGLONG ((ULONGLONG)~((ULONGLONG)0))
#endif

// This method is necerssayr to save media file on user local disk
CString replaceByMask(CString src, CString startMask, CString finishMask, CString replaceTo);
DWORD Global_MainThreadId=0;
// The following methods are used to log some critcal events
void appendToErrorFile(const TCHAR *szFormat, ...)
{
	release_try
	{
		if(g_bDebugMode)
		{
			struct tm when;
			__time64_t now;
			_time64(&now);
			_localtime64_s(&when, &now);

			TCHAR time_buff[26];
			_tasctime_s(time_buff, sizeof(time_buff)/sizeof(TCHAR), &when);
			const size_t time_len = _tcslen(time_buff);
			if(time_buff[time_len-1] == _T('\n'))
				time_buff[time_len-1] = _T('\0');

			TCHAR buf[4096];
			va_list va;
			va_start(va, szFormat);
			_vstprintf_s(buf, sizeof(buf)/sizeof(TCHAR), szFormat, va);
			AtlTrace(_T("TRACE: %s %s\n"), time_buff, buf);
			va_end(va);

			using namespace std;
			basic_ofstream<TCHAR> logFile;

			CString rootPath;
			rootPath.GetEnvironmentVariable(_T("APPDATA"));
			rootPath += _T("\\deskalerts.log");

			logFile.open(rootPath, ios::out|ios::app);

			logFile<<time_buff;
			logFile<<_T("\t");
			logFile<<buf;
			logFile<<endl;

			logFile.close();
		}
#ifdef DEBUG
		else
		{
			TCHAR buf[1000];
			va_list va;
			va_start(va, szFormat);
			_vstprintf_s(buf, sizeof(buf)/sizeof(TCHAR), szFormat, va);
			AtlTrace(_T("TRACE: %s\n"), buf);
			va_end(va);
		}
#endif
	}
	release_end_try
}

// The following method are used to log some critcal events only for Etisalat asserted build
void appendToErrorFile_TMP(const TCHAR *szFormat, ...)
{
    release_try
    {
        if (1/*g_bDebugMode*/)
        {
            struct tm when;
            __time64_t now;
            _time64(&now);
            _localtime64_s(&when, &now);

            TCHAR time_buff[26];
            _tasctime_s(time_buff, sizeof(time_buff) / sizeof(TCHAR), &when);
            const size_t time_len = _tcslen(time_buff);
            if (time_buff[time_len - 1] == _T('\n'))
                time_buff[time_len - 1] = _T('\0');

            TCHAR buf[4096];
            va_list va;
            va_start(va, szFormat);
            _vstprintf_s(buf, sizeof(buf) / sizeof(TCHAR), szFormat, va);
            AtlTrace(_T("TRACE: %s %s\n"), time_buff, buf);
            va_end(va);

            using namespace std;
            basic_ofstream<TCHAR> logFile;

            CString rootPath;
            rootPath.GetEnvironmentVariable(_T("APPDATA"));
            rootPath += _T("\\deskalerts.log");

            logFile.open(rootPath, ios::out | ios::app);

            logFile << time_buff;
            logFile << _T("\t");
            logFile << buf;
            logFile << endl;

            logFile.close();
        }
    }
    release_end_try;
}


// action - the action called the error
void logSystemError(LPCTSTR action, DWORD errCode)
{
	if (errCode == 0)
		return;
	TCHAR* header = L"System error:\n\tError when %s Error code: %d\n\tError description: %s\n";
	HLOCAL errDescr = NULL;
	FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_ALLOCATE_BUFFER,
		NULL, 
		errCode, 
		MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US),
		(LPTSTR) &errDescr,
		0,
		NULL);

	if (errDescr != NULL) 
	{
		appendToErrorFile(header, action, errCode, errDescr);
		LocalFree(errDescr);
	}
	else 
	{
		appendToErrorFile(header, action, errCode, L"Error number not found.\n");
	}
}

void logException() 
{
    void *stack[TRACE_MAX_STACK_FRAMES * 4];
    HANDLE process = GetCurrentProcess();
    SymInitialize(process, NULL, TRUE);
    WORD numberOfFrames = CaptureStackBackTrace(0, TRACE_MAX_STACK_FRAMES, stack, NULL);
    SYMBOL_INFO *symbol = (SYMBOL_INFO *)malloc( sizeof(SYMBOL_INFO) + (TRACE_MAX_FUNCTION_NAME_LENGTH - 1) * sizeof(TCHAR) );
    if (!symbol)
        return;

    symbol->MaxNameLen = TRACE_MAX_FUNCTION_NAME_LENGTH;
    symbol->SizeOfStruct = sizeof(SYMBOL_INFO);
    DWORD displacement;
    IMAGEHLP_LINE64 line;
    line.SizeOfStruct = sizeof(IMAGEHLP_LINE64);

	appendToErrorFile(L"Exception");
	// starting from the secont frame since the first frame relates to this function
    for (int i = 1; i < numberOfFrames; i++)
    {
        DWORD64 address = (DWORD64)(stack[i]);
        SymFromAddr(process, address, NULL, symbol);
        if (SymGetLineFromAddr64(process, address, &displacement, &line))
        {
			const size_t nameLen = strlen(symbol->Name);
			TCHAR* symbolName = (TCHAR*) malloc(sizeof(TCHAR) * (nameLen + 1));
			size_t length = 0;
			mbstowcs_s(&length, symbolName, nameLen + 1, symbol->Name, nameLen);

			const size_t fileNameLen = strlen(line.FileName);
			TCHAR* fileName = (TCHAR*) malloc(sizeof(TCHAR) * (fileNameLen + 1));
			mbstowcs_s(&length, fileName, fileNameLen + 1, line.FileName, fileNameLen);

			appendToErrorFile(L"\tat %s in %s: line: %lu: address: 0x%0X", symbolName, fileName, line.LineNumber, symbol->Address);
			free(symbolName);
			free(fileName);
        }
        else
        {
			logSystemError(L"SymGetLineFromAddr64", GetLastError());
			appendToErrorFile(L"\tat %s, address 0x%0X.\n", symbol->Name, symbol->Address);
        }
    }
	free(symbol);
}

void logHttpRequest(LPCTSTR szURL)
{
	TCHAR* header = L"Client request:\n\tRequest URL: %s\n";
	if (szURL != NULL) 
	{
		appendToErrorFile(header, szURL);
	}
}

void logHttpStatus(int nStatusCode)
{
	TCHAR* header = L"Server response:\n\tServer returned code: %d\n";
	if (nStatusCode != 0) 
	{
		appendToErrorFile(header, nStatusCode);
	}
}
//
//
//

//
// Static member
//
CString CAlertUtils::CitrixIp;
//
//
//

CAlertUtils::CAlertUtils()
{
}

CAlertUtils::~CAlertUtils(void)
{
}

DWORD WINAPI CAlertUtils::AsyncURLRequestFunc(LPVOID lpParam)
{
	CComBSTR url(reinterpret_cast<BSTR>(lpParam));
	URLDownloadToFile(url);
	return 0;
}

DWORD CAlertUtils::AsyncURLRequest(LPCTSTR szURL)
{
	DWORD dwThreadId = 0;
	BEGINTHREADEX(
		NULL,                    // default security attributes
		0,                       // use default stack size
		AsyncURLRequestFunc,     // thread function
		CComBSTR(szURL).Detach(),// argument to thread function
		0,                       // use default creation flags
		&dwThreadId);
	return dwThreadId;
}



struct InternetDeleter {
	void operator() (HINTERNET h) { ::InternetCloseHandle(h); }
};

typedef unique_ptr<std::remove_pointer<HINTERNET>::type, InternetDeleter> internet_ptr;

HRESULT CAlertUtils::URLDownloadToFile(LPCTSTR szURL, LPCTSTR szFileName, CString *result, BOOL *isUTF8, CString *key, CString *headers, CString *postData, int reqVerb, int *returnedHTTPStatusCode)
{
	const bool file = (szFileName && _tcslen(szFileName)>0);
	//assert(file || result);

    CString fUrl = "";
	if(CitrixIp.IsEmpty()) 
	{
		fUrl.Format(_T("%s"), szURL);
	}
	else
	{
		if (_tcsstr(szURL, _T("?")) != NULL) 
		{ 
			fUrl.Format(_T("%s&ip=%s"), szURL, CitrixIp);
		}
		else 
		{
			fUrl.Format(_T("%s?ip=%s"), szURL, CitrixIp);
		}
	}		
	DWORD dwTimeoutDefault = 30000; //30 sec
	logHttpRequest(fUrl);
	//
	//
	//
	HRESULT res = S_FALSE;
	if(!fUrl || _tcslen(fUrl) == 0)
	{
		return S_FALSE;
	}


	bool skipHttpsCheck = false;
	if(_iAlertModel)
	{
		CString strSkipHttpsCheck = _iAlertModel->getPropertyString(CString(_T("skip_https_check")));
		skipHttpsCheck = (strSkipHttpsCheck==_T("1") || strSkipHttpsCheck==_T("true"));
	}

	CUrl cUrl;
	if(!cUrl.CrackUrl(fUrl))
	{
		// Adds error description to log-file if system error has been detected
		logSystemError(L"CUrl::CrackUrl", GetLastError());
		return S_FALSE;
	}

	SetLastError(0);
	MSG Message;
	Message.message = UWM_LIFE_SIGNAL;
	Message.lParam = CAlertUtils::str2sec(_iAlertModel->getPropertyString(CString(_T("normal_expire"))), 60);
	Message.wParam = NULL;
	CAlertUtils::PostMessageToWatchDog(Message);
	const internet_ptr hInternet(::InternetOpen(_T("DeskAlerts"), INTERNET_OPEN_TYPE_PRECONFIG, NULL, NULL, 0));
	if(hInternet.get() == NULL)
	{
		// Adds error description to log-file if system error has been detected
		logSystemError(L"InternetOpen", GetLastError());
		return S_FALSE;
	}

	//////////////////////
	/// set timeout
	InternetSetOption(hInternet.get(), INTERNET_OPTION_RECEIVE_TIMEOUT, &dwTimeoutDefault, sizeof(DWORD));
	InternetSetOption(hInternet.get(), INTERNET_OPTION_SEND_TIMEOUT, &dwTimeoutDefault, sizeof(DWORD));
	InternetSetOption(hInternet.get(), INTERNET_OPTION_CONNECT_TIMEOUT, &dwTimeoutDefault, sizeof(DWORD));
	SetLastError(0);
	CAlertUtils::PostMessageToWatchDog(Message);
	const internet_ptr hConnect(::InternetConnect(hInternet.get(), cUrl.GetHostName(), cUrl.GetPortNumber(), NULL,NULL, INTERNET_SERVICE_HTTP, 0, 1));
	if (hConnect.get() == NULL)
	{
		// Adds error description to log-file if system error has been detected
		logSystemError(L"InternetConnect", GetLastError());
		return S_FALSE;
	}


	CString sUrl = cUrl.GetUrlPath();
	sUrl += cUrl.GetExtraInfo();

	DWORD dwFlags = /*INTERNET_FLAG_KEEP_CONNECTION|*/INTERNET_FLAG_RELOAD|INTERNET_FLAG_NO_CACHE_WRITE|INTERNET_FLAG_PRAGMA_NOCACHE;

	if(cUrl.GetScheme() == ATL_URL_SCHEME_HTTPS)
	{
		dwFlags |= INTERNET_FLAG_SECURE;
	}

	SetLastError(0);
    LPCTSTR requestType;
    switch (reqVerb) 
    {
        case GETverb:  requestType = _T("GET");  break;
        case POSTverb: requestType = _T("POST"); break;
        case OLDcalcVerbByPostData:
        default:
            if (postData)
            {
                requestType = _T("POST");
            }
            else
            {
                requestType = _T("GET");
            }
            break;
    }
    
	CAlertUtils::PostMessageToWatchDog(Message);
	const internet_ptr hRequest(::HttpOpenRequest(hConnect.get(), requestType, sUrl, NULL, NULL, 0, dwFlags, NULL));
	if (hRequest.get() == NULL)
	{
		// Adds error description to log-file if system error has been detected
		logSystemError(L"HttpOpenRequest", GetLastError());
		return S_FALSE;
	}


	if(cUrl.GetScheme() == ATL_URL_SCHEME_HTTPS)
	{
		if(skipHttpsCheck)
		{
			DWORD dwFlags2=0;
			DWORD dwSize = sizeof(dwFlags2);
			InternetQueryOption(hRequest.get(), INTERNET_OPTION_SECURITY_FLAGS, (LPVOID)&dwFlags2, &dwSize);
			dwFlags2 |= SECURITY_FLAG_IGNORE_UNKNOWN_CA|SECURITY_FLAG_IGNORE_REVOCATION|SECURITY_FLAG_IGNORE_CERT_CN_INVALID|SECURITY_FLAG_IGNORE_CERT_DATE_INVALID|SECURITY_FLAG_IGNORE_WRONG_USAGE;
			InternetSetOption(hRequest.get(), INTERNET_OPTION_SECURITY_FLAGS, &dwFlags2, sizeof(dwFlags2));
		}
		DWORD pCert=0;
		InternetSetOption(hRequest.get(), INTERNET_OPTION_CLIENT_CERT_CONTEXT,(LPVOID)&pCert,sizeof(DWORD));
	}

	BOOL bSend;
	PCCERT_CONTEXT pCert = NULL;
	HCERTSTORE hSystemStore = NULL;
	do
	{
		SetLastError(0);
		int headersLength =0;
		int postDataLength =0;
		LPCTSTR headersStr = NULL;
		LPVOID postDataStr = NULL;
		if (headers)
		{
			headersStr = headers->GetString();
			headersLength = _tcslen(headersStr);
		}
		CStringA tmpPostData;
		if (postData)
		{
			tmpPostData = CStringA(CT2A(*postData));
			postDataLength = tmpPostData.GetLength();
			postDataStr = (LPVOID)(tmpPostData.GetString());
		}
		CAlertUtils::PostMessageToWatchDog(Message);
		bSend = HttpSendRequest(hRequest.get(), headersStr, headersLength, postDataStr, postDataLength);
		const DWORD dwError = GetLastError();
		// Adds error description to log-file if system error has been detected
		if(dwError != S_OK)
			logSystemError(L"HttpOpenRequest", dwError);
		//
		//
		//
		
		if (dwError != ERROR_INTERNET_CLIENT_AUTH_CERT_NEEDED)
		{
			break;
		}

		if(!hSystemStore)
		{
			SetLastError(0);
			hSystemStore = CertOpenSystemStore(NULL, L"MY");

			//appendToErrorFileIfError(_T("CertOpenSystemStore"), GetLastError());
			// Adds error description to log-file if system error has been detected
			logSystemError(_T("CertOpenSystemStore"), GetLastError());
		}

		SetLastError(0);
		pCert = CertEnumCertificatesInStore(hSystemStore, pCert);
		// Adds error description to log-file if system error has been detected
		logSystemError(L"CertEnumCertificatesInStore", GetLastError());
		if(!pCert) 
		{
			break;
		}
		InternetSetOption(hRequest.get(), INTERNET_OPTION_CLIENT_CERT_CONTEXT,(LPVOID)pCert,sizeof(CERT_CONTEXT));
		CertFreeCertificateContext(pCert);

	} while(true);

	if(hSystemStore)
	{
		CertCloseStore(hSystemStore, CERT_CLOSE_STORE_CHECK_FLAG);
	}

	if (!bSend)
	{
		return S_FALSE;
	}

	TCHAR status[32]={0};
	DWORD dwSize = 32;
    ////////////////////////////
    /// Request Status
	if(HttpQueryInfo(hRequest.get(), HTTP_QUERY_STATUS_CODE, status, &dwSize, NULL))
	{
		const int nStatusCode = _ttoi(status);
		if (NULL != returnedHTTPStatusCode)
		{
			*returnedHTTPStatusCode = nStatusCode;
		}
		// Logging the server response status code
		logHttpStatus(nStatusCode);
		//
		//
		//
		if(nStatusCode >= 400)
		{
			return S_FALSE;
		}
	}

    DWORD dwSizeIF_MATCH = 0;
    DWORD someerr = 0;
    ////////////////////////////
    /// Request ETAG Header
    if (! HttpQueryInfo(hRequest.get(), /*HTTP_QUERY_RAW_HEADERS*/HTTP_QUERY_ETAG, NULL, &dwSizeIF_MATCH, NULL))
    {
        if (GetLastError() == ERROR_INSUFFICIENT_BUFFER)
        {
            PTCHAR ptOutBuffer = new TCHAR[dwSizeIF_MATCH];
            if (HttpQueryInfo(hRequest.get(), /*HTTP_QUERY_RAW_HEADERS*/HTTP_QUERY_ETAG, ptOutBuffer, &dwSizeIF_MATCH, NULL))
            {
                if (GetLastError() == S_OK)
                {
                    CString value = CString(ptOutBuffer, dwSizeIF_MATCH);
                    _iAlertModel->setPropertyString(CString(_T("ETag")), value);
                }
            }
            delete[] ptOutBuffer;
        }
        someerr = GetLastError();
        //process it if nesessery
    }

	char szData[2048];
    std::string strInternetReader;
	do
	{
		SetLastError(0);
		if(!InternetReadFile(hRequest.get(), (LPVOID)szData, sizeof(szData), &dwSize))
		{
			//appendToErrorFileIfError(_T("InternetReadFile"), GetLastError());
			// Adds error description to log-file if system error has been detected
			logSystemError(L"InternetReadFile", GetLastError());
			//
			//
			//
			return S_FALSE;
		}
		if(dwSize != 0)
		{
            strInternetReader += std::string(szData, dwSize);
		}
	} while(dwSize);

	if (strInternetReader.size() == 0)
	{
		return S_OK;
	}

	if(key)
	{
		CT2CA keyInTchar (*key);
        std::string cipherString(strInternetReader);
		//std::string cipherString(aresult);
		std::string keyString(keyInTchar);
		std::string vectorString("4276235117979925");
		std::string decrypted;

		byte bytes[32];
		memset(bytes, 0x00, 32);

		if (keyString.length() < 32) 
		{
			memcpy(bytes, (byte*)keyString.c_str(), keyString.length());
		}
		else 
		{
			memcpy(bytes, (byte*)keyString.c_str(), 32);
		}

		std::string decoded;
		   
		CryptoPP::StringSource ss(cipherString, true, new CryptoPP::Base64Decoder(new CryptoPP::StringSink(decoded)));

		const char* decodedCStr = decoded.c_str();

		CryptoPP::CFB_Mode<CryptoPP::AES>::Decryption cfbDecryption(bytes, sizeof(bytes), (byte*)vectorString.c_str());
		cfbDecryption.ProcessData((byte*)decodedCStr, (byte*)decodedCStr, decoded.length());
        // new code
        strInternetReader = std::string(decodedCStr);
        // old code
		//aresult = (char*)realloc(aresult, strlen(decodedCStr) + 1);
		//memcpy(aresult, decodedCStr, strlen(decodedCStr) + 1);
	}

	if(file)
	{
		HANDLE hFile = CreateFile(szFileName, GENERIC_WRITE, FILE_SHARE_READ, 0, CREATE_ALWAYS, FILE_FLAG_WRITE_THROUGH, 0);
		logSystemError(L"InternetCreateFile", GetLastError());

		DWORD resSize = strInternetReader.size();
        WriteFile(hFile, strInternetReader.c_str(), strInternetReader.size(), &resSize, 0);
        CloseHandle(hFile);
	}
	if(result)
	{
		BOOL isutf8 = FALSE;
		DWORD dwSize = 256;
		CString content_type;
		if(!HttpQueryInfo(hRequest.get(), HTTP_QUERY_CONTENT_TYPE, content_type.GetBuffer(dwSize), &dwSize, NULL))
		{
			// Adds error description to log-file if system error has been detected
			logSystemError(L"HttpQueryInfo", GetLastError());
		}
		else
		{
			content_type.ReleaseBuffer(dwSize);
			content_type.MakeLower();
			content_type.Replace(_T(" "), _T(""));
			content_type.Replace(_T("`"), _T(""));
			content_type.Replace(_T("\'"), _T(""));
			content_type.Replace(_T("\""), _T(""));
			content_type.Replace(_T("-"), _T(""));
			isutf8 = (content_type.Find(_T("charset=utf8")) != -1);
			if(isUTF8)
			{
				 *isUTF8 = isutf8;	
			}
		}

		if (isutf8)
		{
			 *result = CA2T(strInternetReader.c_str()/*aresult*/, CP_UTF8);	
		}
		else 
		{
			*result = CA2T(strInternetReader.c_str()/*aresult*/);
		}

	}

	return S_OK;
}

int CAlertUtils::str2sec(CString& str, int def)
{
	int iRes = def;
	if (!str.IsEmpty())
	{
		TCHAR chr = str[str.GetLength()-1];
		if(chr >= _T('0') && chr <= _T('9'))
		{
			iRes = str2int(str, def)*60;
		}
		else
		{
			int val = str2int(str.Left(str.GetLength()-1), def);
			if(chr == _T('s') || chr == _T('S'))
			{
				iRes = val;
			}
			else if(chr == _T('m') || chr == _T('M'))
			{
				iRes = val*60;
			}
			else if(chr == _T('h') || chr == _T('H'))
			{
				iRes = val*60*60;
			}
			else if(chr == _T('d') || chr == _T('D'))
			{
				iRes = val*60*60*24;
			}
			else if(chr == _T('w') || chr == _T('W'))
			{
				iRes = val*60*60*24*7;
			}
		}
	}
	else
	{
		iRes=0;
	}
	return iRes;
}

int CAlertUtils::str2int(CString& str, int def)
{
	int iRes = def;
	if(!str.IsEmpty())
	{
		release_try
		{
			iRes = _ttoi(str);
		}
		release_catch_all
		{
			iRes = def;
		}
		release_catch_end
	}
	return iRes;
}

INT64 CAlertUtils::str2int64(CString& str, INT64 def)
{
	INT64 iRes = def;
	if(!str.IsEmpty())
	{
		release_try
		{
			iRes = _ttoi64(str);
		}
		release_catch_all
		{
			iRes = def;
		}
		release_catch_end
	}
	return iRes;
}

long CAlertUtils::hexstr2long(CString& str,long def)
{
	if(str.IsEmpty())
	{
		return def;
	}
	DWORD color = _tcstol(str, NULL, 16);
	return RGB((color&0xFF0000)>>16, (color&0x00FF00)>>8, color&0x0000FF);
}

HKEY  CAlertUtils::GetCompanyRegistryKey()
{
	CString sPreCompany = _T("software");

	if (Key != _bstr_t(""))
	{
		sRegistryKey = Key;
	}

	HKEY hSoftKey = NULL;
	HKEY hCompanyKey = NULL;
	if (RegOpenKeyEx(HKEY_CURRENT_USER, sPreCompany, 0, KEY_WRITE|KEY_READ,
		&hSoftKey) == ERROR_SUCCESS)
	{
		DWORD dw;
		RegCreateKeyEx(hSoftKey, sRegistryKey, 0, REG_NONE,
			REG_OPTION_NON_VOLATILE, KEY_WRITE|KEY_READ, NULL,
			&hCompanyKey, &dw);
	}
	if (hSoftKey != NULL)
		RegCloseKey(hSoftKey);

	return hCompanyKey;
}

HKEY CAlertUtils::GetAppRegistryKey()
{
	HKEY hAppKey = NULL;
	HKEY hCompanyKey = CAlertUtils::GetCompanyRegistryKey();
	DWORD dw;
	RegCreateKeyEx(hCompanyKey, sProfileName, 0, REG_NONE,
		REG_OPTION_NON_VOLATILE, KEY_WRITE|KEY_READ, NULL,
		&hAppKey, &dw);
	if (hCompanyKey != NULL)
		RegCloseKey(hCompanyKey);
	return hAppKey;
}

CString CAlertUtils::getValue(const CString& s,const CString& def)
{
	CDatabaseManager *databaseManager = CDatabaseManager::shared(true);
	if (databaseManager)
	{
		return databaseManager->hasProperty(s)?databaseManager->getProperty(s):def;
	}
	else
	{
		return def;
	}
}

CString CAlertUtils::getValue(const CString& s)
{
	CDatabaseManager *databaseManager = CDatabaseManager::shared(true);
	if (databaseManager)
	{
		return databaseManager->getProperty(s);
	}
	else
	{
		return CString();
	}
}

void CAlertUtils::setValue(const CString& s,const CString& val, BOOL common)
{
	CDatabaseManager *databaseManager = CDatabaseManager::shared(true,common);
	if (databaseManager)
	{
		databaseManager->setProperty(s,val);
	}
}

void CAlertUtils::GetImageFromList(WTL::CImageList *lstImages,int nImage, WTL::CBitmap* destBitmap,HWND hwnd)
{
	SIZE size;
	lstImages->GetIconSize(size);

	destBitmap->CreateCompatibleBitmap(::GetWindowDC(hwnd),size.cx,size.cy);

	WTL::CDC dcMem;
	dcMem.CreateCompatibleDC (::GetWindowDC(hwnd));
	dcMem.SelectBitmap(destBitmap->m_hBitmap);

	LOGBRUSH bb;
	bb.lbColor = GetSysColor(COLOR_MENU);
	bb.lbStyle = BS_SOLID;
	HBRUSH brush = CreateBrushIndirect(&bb);
	HBRUSH oldbrush =  dcMem.SelectBrush(brush);

	CRect r(0,0,size.cx,size.cy);
	dcMem.FillRect(&r, brush);

	dcMem.SelectBrush(oldbrush);

	lstImages->Draw(dcMem,nImage,0,0,ILD_TRANSPARENT);
}

CString CAlertUtils::CreateGUID()
{
	GUID guidValue = GUID_NULL;
	::CoCreateGuid(& guidValue);
	if ( guidValue == GUID_NULL) return _T("");

	CString strName;
	strName.Format(_T("{%08lX-%04X-%04x-%02X%02X-%02X%02X%02X%02X%02X%02X}"),
		guidValue.Data1, (guidValue.Data2), (guidValue.Data3),
		(guidValue.Data4[0]), (guidValue.Data4[1]), (guidValue.Data4[2]),( guidValue.Data4[3]),
		(guidValue.Data4[4]), (guidValue.Data4[5]), (guidValue.Data4[6]), (guidValue.Data4[7]));

	return strName;
}

//Íåÿñíàÿ õðåíü, êàê è âñå, ÷òî ñ íåé ñâÿçàíî
//Â xml âñå ñâÿçàííûå ïîëÿ äàâíî çàêîìåíòèðîâàíû
//Íà ñåðâåðå ïîääåðæêà âûïèëåíà ïîõîæå
//×òî ýòî è çà÷åì îíî, íèêòî íå ïîìíèò
//Íàâåðíîå, íàäî áóäåò ñî âðåìåíåì òîæå âûïèëèòü
void CAlertUtils::checkPixel(CString pixelUrl, CString sPath)
{
	//TCHAR    achKey[MAX_KEY_LENGTH];   // buffer for subkey name
	//DWORD    cbName;                   // size of name string
	TCHAR    achClass[MAX_PATH] = TEXT("");  // buffer for class name
	DWORD    cchClassName = MAX_PATH;  // size of class string
	DWORD    cSubKeys=0;               // number of subkeys
	DWORD    cbMaxSubKey;              // longest subkey size
	DWORD    cchMaxClass;              // longest class string
	DWORD    cValues;              // number of values for key
	DWORD    cchMaxValue;          // longest value name
	DWORD    cbMaxValueData;       // longest value data
	DWORD    cbSecurityDescriptor; // size of security descriptor
	FILETIME ftLastWriteTime;      // last write time

	DWORD i, retCode;

	TCHAR * achValue = new TCHAR[MAX_VALUE_NAME];
	DWORD cchValue = MAX_VALUE_NAME;

	TCHAR * cValueStr = new TCHAR[MAX_VALUE_NAME];
	DWORD pdwCountStr = MAX_VALUE_NAME;

	CRegKey hKey, key;
	hKey.Create(HKEY_CURRENT_USER,CGMUtils::GetCompanyKey());

	key.Open(hKey, _T("url"));

	retCode = RegQueryInfoKey(
		key.m_hKey,         // key handle
		achClass,                // buffer for class name
		&cchClassName,           // size of class string
		NULL,                    // reserved
		&cSubKeys,               // number of subkeys
		&cbMaxSubKey,            // longest subkey size
		&cchMaxClass,            // longest class string
		&cValues,                // number of values for this key
		&cchMaxValue,            // longest value name
		&cbMaxValueData,         // longest value data
		&cbSecurityDescriptor,   // security descriptor
		&ftLastWriteTime);       // last write time

	// Enumerate the subkeys, until RegEnumKeyEx fails.
	if (cValues)
	{
		for (i=0, retCode=ERROR_SUCCESS; i<cValues; i++)
		{
			cchValue = MAX_VALUE_NAME;
			achValue[0] = '\0';

			retCode = RegEnumValue(key.m_hKey, i,
				achValue,
				&cchValue,
				NULL,
				NULL,
				NULL,
				NULL);

			if(retCode == ERROR_SUCCESS)
			{
				pdwCountStr = MAX_VALUE_NAME;
				cValueStr[0] = '\0';
				if(ERROR_SUCCESS==key.QueryStringValue(achValue, cValueStr, &pdwCountStr))
				{
					pixelUrl.Replace(_T("%id%"), CString(cValueStr));
					CAlertUtils::URLDownloadToFile(pixelUrl, sPath);
					key.DeleteValue(achValue);
				}
			}
		}
	}
	delete[] cValueStr;
	delete [] achValue;
}

CString CAlertUtils::GetSystemHash()
{
	TCHAR VolumeName[MAX_PATH],FileSystemName[MAX_PATH], tmp[MAX_PATH];
	unsigned long VolumeSerialNo, MaxComponentLength, FileSystemFlags;
	GetVolumeInformation(
		_T("C:\\"),
		VolumeName,
		MAX_PATH,
		&VolumeSerialNo,
		&MaxComponentLength,
		&FileSystemFlags,
		FileSystemName,
		MAX_PATH
		);
	_itot_s(VolumeSerialNo, tmp, MAX_PATH, 16);
	return tmp;
}

BOOL CAlertUtils::appendToFile(CString sFilename, CString sData, BOOL isUTF8)
{
	const HANDLE hFile = CreateFile(
		sFilename,                 // file to open
		FILE_APPEND_DATA,          // open for writing
		FILE_SHARE_READ,           // share for writing
		NULL,                      // default security
		OPEN_EXISTING,             // existing file only
		FILE_ATTRIBUTE_NORMAL,     // normal file
		NULL);

	if (hFile == INVALID_HANDLE_VALUE)
	{
		return NULL;
	}

	DWORD BytesWrite;
	CStringA sTmp;
	// Replace to if-else cause of error
	// CStringA sTmp = isUTF8 ? CT2A(sData, CP_UTF8) : CT2A(sData);
	if (isUTF8) 
	{
		sTmp = CT2A(sData, CP_UTF8);
	}
	else 
	{
		sTmp = CT2A(sData);
	}	
	const BOOL bSuc = WriteFile(hFile, sTmp, sTmp.GetLength(), &BytesWrite, NULL);
	CloseHandle(hFile);

	return bSuc;
}


//----------------------------------------------------------------------------------------------------------------------------------
BOOL CAlertUtils::mergeFiles(CString sFilenamTo, CString sFileNameFrom)
{
	std::wofstream fto;
	fto.open(sFilenamTo, ios_base::app | ios_base::binary);
	if(!fto.is_open()) return FALSE;

	std::wifstream ffrom;
	ffrom.open(sFileNameFrom);
	if(!ffrom.is_open()) return FALSE;

	// get length of file:
	ffrom.seekg (0, ios::end);
	int length = ffrom.tellg();
	ffrom.seekg (0, ios::beg);

	// allocate memory:
	wchar_t *buffer = new wchar_t[length];
	// read data as a block:
	ffrom.read (buffer, length);
	fto << FILE_SEPARATOR;
	fto.write(buffer, length);

	ffrom.close();
	fto.close();

	delete_catch([] buffer);

	return TRUE;
}

CString CAlertUtils::GetVersionTxtVersion(CString userPath)
{
	CString myv;
	CString path=userPath;
	if( path == "" )
		_iAlertModel->getPropertyString(CString(_T("user_path")))+ _T("\\version.txt");
	if (!::PathFileExists(path))
		path=_iAlertModel->getPropertyString(CString(_T("root_path")))+ _T("\\version.txt");
	FILE* fp = NULL;
	_tfopen_s(&fp, path, _T("r"));
	if (fp)
	{
		TCHAR * newVersionInfo = new TCHAR[1024];
		newVersionInfo = _fgetts(newVersionInfo, 1024, fp);
		fclose(fp);
		myv = newVersionInfo;
		myv.Replace(_T("\n"), _T(""));
		myv.Replace(_T("\r"), _T(""));
		myv.Replace(_T("DeskAlerts v"),_T(""));
		delete_catch([] newVersionInfo);
	}

	return myv;
}


pair<CString, CString> CAlertUtils::getBodySize(CString FileName)
{
	pair<CString, CString> res;
	const HANDLE hFile = CreateFile(
		FileName,                  // file to open
		GENERIC_READ,              // open for reading
		FILE_SHARE_READ,           // share for reading
		NULL,                      // default security
		OPEN_EXISTING,             // existing file only
		FILE_ATTRIBUTE_NORMAL,     // normal file
		NULL);

	if (hFile == INVALID_HANDLE_VALUE)
	{
		return res;
	}

	DWORD FileSize = GetFileSize(hFile, NULL);
	CHAR* lpBuf = new CHAR [FileSize+1];
	ZeroMemory(lpBuf,FileSize+1);
	DWORD BytesRead;
	const BOOL bSuc = ReadFile(hFile, lpBuf, FileSize, &BytesRead, NULL);
	CloseHandle(hFile);

	if(bSuc)
	{
		CAtlRegExp<> reBody;
		// Five match groups: scheme, authority, path, query, fragment
		REParseError status = reBody.Parse(_T("<\b*body.*?width=\"|'{([0-9])+}\"|'.*?>"), FALSE);

		CAtlREMatchContext<> mcParam;
		if (reBody.Match(CA2T(lpBuf), &mcParam)){
			const CAtlREMatchContext<>::RECHAR* szStart = 0;
			const CAtlREMatchContext<>::RECHAR* szEnd = 0;
			mcParam.GetMatch(0, &szStart, &szEnd);
			res.first = CString(szStart).Left(szEnd - szStart);
		}
		status = reBody.Parse(_T("<\b*body.*?height=\"|'{([0-9])+}\"|'.*?>"), FALSE);
		if (reBody.Match(CA2T(lpBuf), &mcParam)){
			const CAtlREMatchContext<>::RECHAR* szStart = 0;
			const CAtlREMatchContext<>::RECHAR* szEnd = 0;
			mcParam.GetMatch(0, &szStart, &szEnd);
			res.second = CString(szStart).Left(szEnd - szStart);
		}
	}
	return res;
}
/*
BOOL checkUserHash(CStringW name, CStringW hash)
{
	BOOL result = FALSE;
	release_try
	{
		if(hash.GetLength() == 48)
		{
			SHA1 sha1;
			CStringW rnd = hash.Mid(27,2) + hash.Mid(8,3) + hash.Mid(37,3);
			CT2A calc(rnd.Mid(3,3) + name + rnd.Mid(6,2) + _T("dR5Hg") + rnd.Left(3), CP_UTF8);
			sha1.Input(calc, strlen(calc));
			unsigned res[5];
			if(sha1.Result((unsigned *)&res))
			{
				if(_tcstoul(hash.Left(8),NULL,16) == res[2] &&
					_tcstoul(hash.Mid(11,8),NULL,16) == res[1] &&
					_tcstoul(hash.Mid(19,8),NULL,16) == res[4] &&
					_tcstoul(hash.Mid(29,8),NULL,16) == res[3] &&
					_tcstoul(hash.Mid(40,8),NULL,16) == res[0])
				{
					result = TRUE;
				}
			}
		}
	}
	release_end_try
	return result;
}
*/
CString CAlertUtils::userHash(CString name)
{
	SHA1 sha1;
	CString result, rnd;
	srand((unsigned int)time(NULL));
	rnd.Format(_T("%08x"), rand());
	CT2A calc(rnd.Mid(3,3) + name + rnd.Mid(6,2) + _T("dR5Hg") + rnd.Left(3), CP_UTF8);
	sha1.Input(calc, strlen(calc));
	unsigned res[5];
	if(sha1.Result((unsigned *)&res))
	{
		result.Format(_T("%08x%ws%08x%08x%ws%08x%ws%08x"), res[2], rnd.Mid(2,3), res[1], res[4], rnd.Left(2), res[3], rnd.Mid(5,3), res[0]);
	}
	return result;
}

CString CAlertUtils::allUsersAppDataPath(BOOL env)
{
	CString allUsersPath;
	if(env)
	{
		allUsersPath.GetEnvironmentVariable(_T("ALLUSERSPROFILE"));
	}
	if(!env || allUsersPath.IsEmpty())
	{
		TCHAR commonAppDataPath[MAX_PATH];
		SHGetFolderPath(NULL,CSIDL_COMMON_APPDATA,NULL,SHGFP_TYPE_CURRENT,commonAppDataPath);
		allUsersPath = CString(commonAppDataPath);
	}
	if(allUsersPath.GetAt(allUsersPath.GetLength()-1) != _T('\\'))
	{
		allUsersPath += _T('\\');
	}
	CString deskalertsPath = allUsersPath + _T("DeskAlerts");

	if (!env) CreateDirectory(deskalertsPath,NULL);
	return deskalertsPath;
}

CString CAlertUtils::allUsersAppDataProfilePath(BOOL env)
{
	CString allUsersPath = CAlertUtils::allUsersAppDataPath(env);
	CString path = allUsersPath + CString("\\") + CAlertUtils::GetRegistryKeyName();
	if (!env) CreateDirectory(path,NULL);
	return path;
}

HRESULT CAlertUtils::GetUserFromProcess( DWORD procId,  _bstr_t& strUser, _bstr_t& strdomain)
{
    HANDLE hProcess = OpenProcess(PROCESS_QUERY_INFORMATION,FALSE,procId); 
    if(hProcess == NULL)
        return E_FAIL;
    HANDLE hToken = NULL;

    if( !OpenProcessToken( hProcess, TOKEN_QUERY, &hToken ) )
    {
        CloseHandle( hProcess );
        return E_FAIL;
    }
    BOOL bres = GetLogonFromToken (hToken, strUser,  strdomain);

    CloseHandle( hToken );
    CloseHandle( hProcess );
    return bres?S_OK:E_FAIL;
}

BOOL CAlertUtils::GetLogonFromToken (HANDLE hToken, _bstr_t& strUser, _bstr_t& strdomain) 
{
   DWORD dwSize = MAX_PATH;
   BOOL bSuccess = FALSE;
   DWORD dwLength = 0;
   strUser = "";
   strdomain = "";
   PTOKEN_USER ptu = NULL;
 //Verify the parameter passed in is not NULL.
    if (NULL == hToken)
        goto Cleanup;

       if (!GetTokenInformation(
         hToken,         // handle to the access token
         TokenUser,    // get information about the token's groups 
         (LPVOID) ptu,   // pointer to PTOKEN_USER buffer
         0,              // size of buffer
         &dwLength       // receives required buffer size
      )) 
   {
      if (GetLastError() != ERROR_INSUFFICIENT_BUFFER) 
         goto Cleanup;

      ptu = (PTOKEN_USER)HeapAlloc(GetProcessHeap(),
         HEAP_ZERO_MEMORY, dwLength);

      if (ptu == NULL)
         goto Cleanup;
   }

    if (!GetTokenInformation(
         hToken,         // handle to the access token
         TokenUser,    // get information about the token's groups 
         (LPVOID) ptu,   // pointer to PTOKEN_USER buffer
         dwLength,       // size of buffer
         &dwLength       // receives required buffer size
         )) 
   {
      goto Cleanup;
   }
    SID_NAME_USE SidType;
    LPTSTR lpName = new WCHAR[MAX_PATH];
    LPTSTR lpDomain = new WCHAR[MAX_PATH];;
    if (ptu != nullptr)
    {
        if (!LookupAccountSid(NULL, ptu->User.Sid, lpName, &dwSize, lpDomain, &dwSize, &SidType))
        {
            DWORD dwResult = GetLastError();
            if (dwResult == ERROR_NONE_MAPPED)
                wcscpy(lpName, _T("NONE_MAPPED"));
            else
            {
                //printf("LookupAccountSid Error %u\n", GetLastError());
            }
        }
        else
        {
            //LPTSTR as wchar_t, 16-bit UNICODE character
            //printf use char*
            //change printf to wprintf fix it
            wprintf(L"Current user is  %s\\%s\n",
                lpDomain, lpName);
            strUser = lpName;
            strdomain = lpDomain;
            bSuccess = TRUE;
        }
    }
    delete[] lpName;
    delete[] lpDomain;

Cleanup: 

   if (ptu != NULL)
      HeapFree(GetProcessHeap(), 0, (LPVOID)ptu);
   return bSuccess;
}

CString CAlertUtils::databasePath(BOOL common)
{
	CString deskalertsPath = CAlertUtils::allUsersAppDataProfilePath();
	
	CString curUserPath;
	//curUserPath.GetEnvironmentVariable(_T("USERNAME"));
	//
	DWORD procId = GetCurrentProcessId();
	
	_bstr_t strUser;
	_bstr_t strDomain;

	GetUserFromProcess( procId,   strUser,  strDomain);
	//
	curUserPath = deskalertsPath + CString("\\") + strUser.GetBSTR();//curUserPath;
	CreateDirectory( curUserPath, NULL );
	CString dbPath = _T("");

	dbPath = (common) ? deskalertsPath+ CString("\\db.dat") : curUserPath+ CString("\\db.dat");
//InterlockedExchange(&g_bDebugMode, TRUE);
//	appendToErrorFile(L"CalerUtils.databasePath : StrUser="+strUser+"  StrDomain="+strDomain+ " DbPath="+dbPath);
//InterlockedExchange(&g_bDebugMode, FALSE);
	return dbPath;
}

CString CAlertUtils::crashLogFolderPath(BOOL common)
{
    CString deskalertsPath = CAlertUtils::allUsersAppDataProfilePath();

    CString curUserPath;
    //curUserPath.GetEnvironmentVariable(_T("USERNAME"));
    //
    DWORD procId = GetCurrentProcessId();

    _bstr_t strUser;
    _bstr_t strDomain;

    GetUserFromProcess(procId, strUser, strDomain);
    //
    curUserPath = deskalertsPath + CString("\\") + strUser.GetBSTR();//curUserPath;
    CreateDirectory(curUserPath, NULL);
    CString crashLogPath = _T("");
    CString crashFolderName = CString("\\crashLogs");
    crashLogPath = ( (common) ? deskalertsPath : curUserPath ) + crashFolderName;
    CreateDirectory(crashLogPath, NULL);
    //InterlockedExchange(&g_bDebugMode, TRUE);
    //	appendToErrorFile(L"CalerUtils.databasePath : StrUser="+strUser+"  StrDomain="+strDomain+ " DbPath="+dbPath);
    //InterlockedExchange(&g_bDebugMode, FALSE);
    return crashLogPath;
}

HANDLE CAlertUtils::GetProcessOwnerToken(DWORD pid)
{
	if (!pid) return NULL;

	HANDLE hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pid);
	if (!hProcess) return NULL;

	HANDLE hToken = NULL;
	if(OpenProcessToken(hProcess, MAXIMUM_ALLOWED, &hToken))
	{
		HANDLE result = INVALID_HANDLE_VALUE;
		if(DuplicateTokenEx(hToken, TOKEN_ASSIGN_PRIMARY | TOKEN_ALL_ACCESS, NULL, SecurityImpersonation, TokenPrimary, &result))
		{
			if(result != INVALID_HANDLE_VALUE)
			{
				CloseHandle(hToken);
				CloseHandle(hProcess);
				return result;
			}
		}
		CloseHandle(hToken);
	}
	CloseHandle(hProcess);

	return NULL;
}

CString CAlertUtils::escapeXML(CString str)
{
	str.Replace(_T("&"), _T("&amp;"));
	str.Replace(_T("\""), _T("&quot;"));
	str.Replace(_T("'"), _T("&apos;"));
	str.Replace(_T("<"), _T("&lt;"));
	str.Replace(_T(">"), _T("&gt;"));
	str.Replace(_T("\t"), _T("&#x9;"));
	str.Replace(_T("\n"), _T("&#xA;"));
	str.Replace(_T("\r"), _T("&#xD;"));
	str.Remove(_T('\0'));
	/*int pos = -1;
	while((pos = str.Find(_T('\0'), pos+1)) > -1 && pos != str.GetLength())
	{
		str = str.Left(pos) + _T("&#x0;")+ str.Mid(pos+1);
	}*/
	return str;
}

CString CAlertUtils::escapeJSON(CString str)
{
	str.Replace(_T("\\"), _T("\\\\"));
	str.Replace(_T("\""), _T("\\\""));
	str.Replace(_T("'"), _T("\\'"));
	str.Replace(_T("\n"), _T("\\n"));
	str.Replace(_T("\r"), _T("\\r"));
	/*int pos = -1;
	while((pos = str.Find(_T('\0'), pos+1)) > -1 && pos != str.GetLength())
	{
		str = str.Left(pos) + _T("\\0")+ str.Mid(pos+1);
	}*/
	str.Remove(_T('\0'));
	return str;
}

void CAlertUtils::RecursiveCopyKey(HKEY old_parent, HKEY new_parent, CString old_name, CString new_name)
{
	CRegKey old_key, new_key;
	if(old_key.Open(old_parent, old_name, KEY_READ) == ERROR_SUCCESS &&
		new_key.Create(new_parent, new_name, REG_NONE, REG_OPTION_NON_VOLATILE, KEY_WRITE) == ERROR_SUCCESS)
	{
		CString name;
		DWORD index = 0, length, type, data_length=0;
		while(old_key.EnumKey(index, name.GetBuffer(length = MAX_KEY_LENGTH), &length) == ERROR_SUCCESS)
		{
			name.ReleaseBuffer(length);
			RecursiveCopyKey(old_key, new_key, name, name);
			++index;
		}
		index = 0;
		while(::RegEnumValue(old_key, index, name.GetBuffer(length = MAX_VALUE_NAME), &length, NULL, &type, NULL, &data_length) == ERROR_SUCCESS)
		{
			name.ReleaseBuffer(length);
			LPBYTE data = new BYTE[data_length];
			if(::RegQueryValueEx(old_key, name, NULL, &type, data, &data_length) == ERROR_SUCCESS)
			{
				::RegSetValueEx(new_key, name, NULL, type, data, data_length);
			}
			++index;
		}
	}
}

void CAlertUtils::RecursiveCopyKey(HKEY parent, CString old_name, CString new_name)
{
	RecursiveCopyKey(parent, parent, old_name, new_name);
}

void CAlertUtils::RecursiveDeleteKey(HKEY parent, CString name)
{
	CRegKey key;
	if(key.Open(parent, name, KEY_READ|KEY_WRITE) == ERROR_SUCCESS)
	{
		CString sub_name;
		DWORD index = 0, length;
		while(key.EnumKey(index, sub_name.GetBuffer(length = MAX_KEY_LENGTH), &length) == ERROR_SUCCESS)
		{
			sub_name.ReleaseBuffer(length);
			RecursiveDeleteKey(key, sub_name);
			++index;
		}
	}
	key.Close();
	::RegDeleteKey(parent, name);
}

void CAlertUtils::RecursiveRenameKey(HKEY parent, CString old_name, CString new_name)
{
	RecursiveCopyKey(parent, old_name, new_name);
	RecursiveDeleteKey(parent, old_name);
}

CRegKey CAlertUtils::parseRegPath(CString path, CString wow6432, REGSAM samDesired, BOOL create /*= FALSE*/)
{
	if(wow6432 == _T("1") || !wow6432.CompareNoCase(_T("true")))
		samDesired |= KEY_WOW64_32KEY;
	else if(wow6432 == _T("0") || !wow6432.CompareNoCase(_T("false")))
		samDesired |= KEY_WOW64_64KEY;

	CRegKey result;
	path.Replace('/', '\\');
	int pos = path.Find('\\');
	if(pos > -1)
	{
		HKEY key = NULL;
		CString root = path.Left(pos);
		root.MakeUpper();
		if(root == _T("HKLM") || root == _T("HKEY_LOCAL_MACHINE"))
			key = HKEY_LOCAL_MACHINE;
		else if(root == _T("HKCU") || root == _T("HKEY_CURRENT_USER"))
			key = HKEY_CURRENT_USER;
		else if(root == _T("HKCC") || root == _T("HKEY_CURRENT_CONFIG"))
			key = HKEY_CURRENT_CONFIG;
		else if(root == _T("HKCR") || root == _T("HKEY_CLASSES_ROOT"))
			key = HKEY_CLASSES_ROOT;
		else if(root == _T("HKU") || root == _T("HKEY_USERS"))
			key = HKEY_USERS;
		else if(root == _T("HKEY_PERFORMANCE_DATA"))
			key = HKEY_PERFORMANCE_DATA;
		else if(root == _T("HKEY_DYN_DATA"))
			key = HKEY_DYN_DATA;
		if(key)
		{
			path = path.Mid(pos+1);
			if(result.Open(key, path, samDesired) != ERROR_SUCCESS)
			{
				if(!(samDesired & KEY_WOW64_32KEY) && !(samDesired & KEY_WOW64_64KEY) &&
					result.Open(key, path, samDesired|KEY_WOW64_64KEY) == ERROR_SUCCESS)
				{
					return result;
				}
				else if(create)
				{
					result.Create(key, path, REG_NONE, REG_OPTION_NON_VOLATILE, samDesired);
				}
			}
		}
	}
	return result;
}

CString CAlertUtils::QueryRegKey(CRegKey &key, const CString keyname, const CString format)
{
	TCHAR *delim = format != _T("xml") ? _T(",") : NULL;
	CString name, subkeys, values;
	DWORD index = 0, length;
	while(key.EnumKey(index, name.GetBuffer(length = MAX_KEY_LENGTH), &length) == ERROR_SUCCESS)
	{
		name.ReleaseBuffer(length);
		if(index == 0 && delim)
			subkeys += delim;
		CRegKey subkey;
		if(subkey.Open(key, name, KEY_READ) == ERROR_SUCCESS)
			subkeys += QueryRegKey(subkey, name, format);
		++index;
	}
	index = 0;
	while(::RegEnumValue(key.m_hKey, index, name.GetBuffer(length = MAX_VALUE_NAME), &length, NULL, NULL, NULL, NULL) == ERROR_SUCCESS)
	{
		name.ReleaseBuffer(length);
		if(index == 0 && delim)
			values += delim;
		values += QueryRegValue(key, name, format);
		++index;
	}
	if(format==_T("xml"))
		values = _T("<KEY name=\"")+escapeXML(keyname)+_T("\"")+(!values.IsEmpty()||!subkeys.IsEmpty()?_T(">")+subkeys+values+_T("</KEY>"):_T("/>"));
	else if(format==_T("json"))
		values = _T("{key:\"")+escapeJSON(keyname)+_T("\"")+(!subkeys.IsEmpty()?_T(",subkeys:[")+subkeys+_T("]"):_T(""))+(!values.IsEmpty()?_T(",subkeys:[")+values+_T("]"):_T(""))+_T("}");

	return values;
}

CString CAlertUtils::QueryRegValue(CRegKey &key, const CString name, const CString format)
{
	CString value;
	DWORD type, valueLength = 0, dwValue = 0; ULONGLONG qwValue;
	if(::RegQueryValueEx(key.m_hKey, name, NULL, &type, NULL, &valueLength) == ERROR_SUCCESS)
	{
		if(((type == REG_SZ || type == REG_EXPAND_SZ) && key.QueryStringValue(name, value.GetBuffer(valueLength), &valueLength) == ERROR_SUCCESS) ||
			(type == REG_MULTI_SZ && key.QueryMultiStringValue(name, value.GetBuffer(valueLength), &valueLength) == ERROR_SUCCESS))
		{
			value.ReleaseBuffer(valueLength-1); //already have \0
		}
		else if(type == REG_DWORD && key.QueryDWORDValue(name, dwValue) == ERROR_SUCCESS)
		{
			valueLength = 33;
			_ltot_s(dwValue, value.GetBuffer(valueLength), valueLength, 10);
			value.ReleaseBuffer();
		}
		else if(type == REG_QWORD && key.QueryQWORDValue(name, qwValue) == ERROR_SUCCESS)
		{
			valueLength = 65;
			_i64tot_s(dwValue, value.GetBuffer(valueLength), valueLength, 10);
			value.ReleaseBuffer();
		}
	}
	if(format==_T("xml"))
		value = _T("<VALUE name=\"") + escapeXML(name) + _T("\" value=\"") + escapeXML(value) + _T("\"/>");
	else if(format==_T("json"))
		value = _T("{name:\"") + escapeJSON(name) + _T("\",value:\"") + escapeJSON(value) + _T("\"}");
	return value;
}

CString CAlertUtils::GetRegistryKeyName()
{
	CString registryKeyName;
	registryKeyName = (LPCTSTR) sRegistryKey;
	return registryKeyName;
}

CString CAlertUtils::GetProfileName()
{
	CString profileName;
	profileName = (LPCTSTR) sProfileName;
	return profileName;
}


CString CAlertUtils::getDir(CString& param)
{
	CString res = param;
	int st = param.ReverseFind('\\');
	if (st)
	{
		res = param.Mid(0,st);
	}
	return res;
}

CString CAlertUtils::getAppDataPath(CString deskalertsId)
{
	//if (CAlertController::m_sAppDataPath == CString(_T("")))
	{
		TCHAR szPath[MAX_PATH];
		CString zPath = _T("");
		ZeroMemory(szPath, MAX_PATH*sizeof(TCHAR));
		//iserg:
		if(SHGetSpecialFolderPath(NULL, szPath, CSIDL_FLAG_CREATE|CSIDL_APPDATA, TRUE))
		{
			zPath = szPath;
			if(zPath.Mid(0, 2) == _T("\\\\")) //hotfix for UNC paths
			{
				if(SHGetSpecialFolderPath(NULL, szPath, CSIDL_FLAG_CREATE|CSIDL_LOCAL_APPDATA, TRUE))
					zPath = szPath;
			}
			zPath+= CString(_T("\\DeskAlerts_"))+deskalertsId;
			//CreateDirectory(zPath, NULL);
		}
		return zPath;
	}
	// else
	//{
	//		return CAlertController::m_sAppDataPath;
	//}
}

CString CAlertUtils::regExReplace(CAtlRegExp<>* regEx, LPCTSTR szIn, LPCTSTR szVal, BOOL bAll)
{

	CString s;
	LPCTSTR szRest = szIn;
	CAtlREMatchContext<> mc;
	while (szIn!=NULL && *szIn!=0 && regEx->Match(szIn, &mc, &szRest)) {
		int offset=mc.m_Match.szStart - szIn;
		CString temp = szIn;
		s += temp.Left(offset);
		s += szVal;
		szIn = szRest;
		if (!bAll)
			break;
	}
    if (szIn != NULL)
    {
        s += szIn;
    }
	return s;
}

CString CAlertUtils::stripHtmlTags(CString html)
{
	CString res;
	CAtlRegExp<> regExp;
	
	regExp.Parse(_T("<head[^>]*?>.*?</head>"), FALSE );
	res = CAlertUtils::regExReplace(&regExp,html,_T(" "),true);
	regExp.Parse(_T("<style[^>]*?>.*?</style>"), FALSE );
	res = CAlertUtils::regExReplace(&regExp,res,_T(" "),true);
 	regExp.Parse(_T("<script[^>]*?>.*?</script>"), FALSE );
 	res = CAlertUtils::regExReplace(&regExp,html,_T(" "),true);
	regExp.Parse(_T("<object[^>]*?>.*?</object>"), FALSE );
	res = CAlertUtils::regExReplace(&regExp,res,_T(" "),true);
	regExp.Parse(_T("<applet[^>]*?>.*?</applet>"), FALSE );
	res = CAlertUtils::regExReplace(&regExp,res,_T(" "),true);
	regExp.Parse(_T("<noframes[^>]*?>.*?</noframes>"), FALSE );
	res = CAlertUtils::regExReplace(&regExp,res,_T(" "),true);
	regExp.Parse(_T("<noscript[^>]*?>.*?</noscript>"), FALSE );
	res = CAlertUtils::regExReplace(&regExp,res,_T(" "),true);
	regExp.Parse(_T("<noembed[^>]*?>.*?</noembed>"), FALSE );
	res = CAlertUtils::regExReplace(&regExp,res,_T(" "),true);
	regExp.Parse(_T("(<([^>]+)>)"), FALSE );
	res = CAlertUtils::regExReplace(&regExp,res,_T(" "),true);
		
	return res;
}

CString CAlertUtils::findAlertWindowName()
{
	CString result;
	NodeALERTS& nodeALERTS = _iAlertModel->getModel(CString(_T("views")));
	vector<NodeALERTS::NodeVIEWS::NodeVIEW*> vView =  nodeALERTS.m_views->m_view;
	vector<NodeALERTS::NodeVIEWS::NodeVIEW*>::iterator it = vView.begin();
	while(it != vView.end())
	{
		if((*it)->m_tagName == _T("WINDOW"))
		{
			CString name = (*it)->m_name;
			name.MakeLower();
			if(name.Find(_T("alert")) > -1 && name.Find(_T("ticker")) == -1)
			{
				result = (*it)->m_name;
				break;
			}
		}
		it++;
	}
	if(result.IsEmpty())
	{
		it = vView.begin();
		while(it != vView.end())
		{
			if((*it)->m_tagName == _T("WINDOW"))
			{
				NodeALERTS::NodeVIEWS::NodeWINDOW* win = (NodeALERTS::NodeVIEWS::NodeWINDOW*)*it;
				CString bodyclassname = win->m_bodyclassname;
				CString caption = win->m_caption;
				CString captionhref = win->m_captionhref;
				CString customjs = win->m_customjs;
				CString customcss = win->m_customcss;
				bodyclassname.MakeLower();
				caption.MakeLower();
				captionhref.MakeLower();
				customjs.MakeLower();
				customcss.MakeLower();
				if(bodyclassname.Find(_T("alert")) > -1 ||
					caption.Find(_T("alert")) > -1 ||
					captionhref.Find(_T("alert")) > -1 ||
					customjs.Find(_T("alert")) > -1 ||
					customcss.Find(_T("alert")) > -1)
				{
					result = win->m_name;
					break;
				}
			}
			it++;
		}
	}
	return result;
}

CString CAlertUtils::findOptionsWindowName()
{
	CString result;
	NodeALERTS& nodeALERTS = _iAlertModel->getModel(CString(_T("views")));
	vector<NodeALERTS::NodeVIEWS::NodeVIEW*> vView =  nodeALERTS.m_views->m_view;
	vector<NodeALERTS::NodeVIEWS::NodeVIEW*>::iterator it = vView.begin();
	while(it != vView.end())
	{
		if((*it)->m_tagName == _T("WINDOW"))
		{
			CString name = (*it)->m_name;
			name.MakeLower();
			if( name.Find(_T("option")) > -1 )
			{
				result = (*it)->m_name;
				break;
			}
		}
		it++;
	}
	if(result.IsEmpty())
	{
		it = vView.begin();
		while(it != vView.end())
		{
			if((*it)->m_tagName == _T("WINDOW"))
			{
				NodeALERTS::NodeVIEWS::NodeWINDOW* win = (NodeALERTS::NodeVIEWS::NodeWINDOW*)*it;
				CString bodyclassname = win->m_bodyclassname;
				CString caption = win->m_caption;
				CString captionhref = win->m_captionhref;
				CString customjs = win->m_customjs;
				CString customcss = win->m_customcss;
				bodyclassname.MakeLower();
				caption.MakeLower();
				captionhref.MakeLower();
				customjs.MakeLower();
				customcss.MakeLower();
				if(bodyclassname.Find(_T("alert")) > -1 ||
					caption.Find(_T("alert")) > -1 ||
					captionhref.Find(_T("alert")) > -1 ||
					customjs.Find(_T("alert")) > -1 ||
					customcss.Find(_T("alert")) > -1)
				{
					result = win->m_name;
					break;
				}
			}
			it++;
		}
	}
	return result;
}


CRegKey CAlertUtils::getDesktopRegKey()
{
	CRegKey key;
	key.Create(HKEY_CURRENT_USER,_T("Control Panel\\Desktop"));
	return key;
}

CRegKey CAlertUtils::getColorsRegKey()
{
	CRegKey key;
	key.Create(HKEY_CURRENT_USER,_T("Control Panel\\Colors"));
	return key;
}

CString CAlertUtils::getScreensaverPath(BOOL isShort)
{
	TCHAR localFolder[MAX_PATH];
	GetCurrentDirectory(  MAX_PATH, localFolder);
	CString screensaverPath = CString(localFolder)+_T("\\DeskAlertsScreenSaver.scr");
	if (isShort)
	{
		long     length = 0;
		TCHAR*   buffer = NULL;
		length = GetShortPathName(screensaverPath, NULL, 0);
		if (length != 0)
		{
			buffer = new TCHAR[length];

			length = GetShortPathName(screensaverPath, buffer, length);
			if (length != 0) 
			{
				screensaverPath = buffer;
				delete_catch([] buffer);
			}
		}		
	}


	return screensaverPath;
}

CString CAlertUtils::getWallpapersPath()
{
	CString deskalertsPath = CAlertUtils::allUsersAppDataProfilePath();
	
	CString curUserPath;
	curUserPath.GetEnvironmentVariable(_T("USERNAME"));
	curUserPath = deskalertsPath + CString("\\") + curUserPath;

	CreateDirectory(curUserPath,NULL);
	CString wallpaperPath = curUserPath+ CString("\\Wallpapers");
	CreateDirectory(wallpaperPath,NULL);
	return wallpaperPath;
}

CString CAlertUtils::getLockscreensPath()
{
	CString deskalertsPath = CAlertUtils::allUsersAppDataProfilePath();

	CString curUserPath;
	curUserPath.GetEnvironmentVariable(_T("USERNAME"));
	curUserPath = deskalertsPath + CString("\\") + curUserPath;

	CreateDirectory(curUserPath, NULL);
	CString lockscreenPath = curUserPath + CString("\\Lockscreens");
	CreateDirectory(lockscreenPath, NULL);
	return lockscreenPath;
}

CString CAlertUtils::getDesktopName(HDESK hDesktop)
{
	CString desktopName;
	if (hDesktop)
	{
		DWORD length;
		GetUserObjectInformation(hDesktop, UOI_NAME, NULL, NULL, &length);
		TCHAR *deskBuffer = new TCHAR[length];
		if(GetUserObjectInformation(hDesktop, UOI_NAME, deskBuffer, length, NULL))
		{
			desktopName = deskBuffer;
		}
		else
		{
			DeskAlertsUnexpected();
		}
		deleteArray_catch(deskBuffer);
	}
	return desktopName;
}

static Gdiplus::Image *ScaleByPercent(Gdiplus::Image *imgPhoto, int Percent)
{
	float nPercent = ((float)Percent/100);

	int sourceWidth = imgPhoto->GetWidth();
	int sourceHeight = imgPhoto->GetHeight();
	int destX = 0;
	int destY = 0; 
	int destWidth  = (int)(sourceWidth * nPercent);
	int destHeight = (int)(sourceHeight * nPercent);

	Gdiplus::Bitmap *bmPhoto = new Gdiplus::Bitmap(destWidth, destHeight, PixelFormat24bppRGB);

	bmPhoto->SetResolution(imgPhoto->GetHorizontalResolution(), imgPhoto->GetVerticalResolution());

	Gdiplus::Graphics grPhoto(bmPhoto);
	grPhoto.SetInterpolationMode(Gdiplus::InterpolationModeHighQualityBicubic);
	const Gdiplus::RectF rectDest((Gdiplus::REAL)destX, (Gdiplus::REAL)destY, (Gdiplus::REAL)destWidth, (Gdiplus::REAL)destHeight);
	grPhoto.Clear((DWORD)Gdiplus::Color::White);
	grPhoto.DrawImage(imgPhoto,rectDest);

	return bmPhoto;
}

Gdiplus::Status CAlertUtils::imageToImage(IStream *pStreamIn, IStream *pStreamOut, CString wszOutputMimeType, int quality)
{
	namespace G = Gdiplus;

	G::GdiplusStartupInput gdiplusStartupInput;
	ULONG_PTR gdiplusToken;
	G::Status status = G::Ok;
	status = G::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
	if (G::Ok == status)
	{
		G::Image imageSrc(pStreamIn);
		status = imageSrc.GetLastStatus();
		if (G::Ok == status) 
		{
			UINT numEncoders = 0;
			UINT sizeEncodersInBytes = 0;
			status = G::GetImageEncodersSize(&numEncoders, &sizeEncodersInBytes);
			if (status == G::Ok) 
			{
				G::ImageCodecInfo *pImageCodecInfo = (G::ImageCodecInfo *) malloc(sizeEncodersInBytes);
				if (pImageCodecInfo)
				{
					status = G::GetImageEncoders(numEncoders, sizeEncodersInBytes, pImageCodecInfo);
					if (status == G::Ok)
					{
						CLSID clsidOut;
						status = G::UnknownImageFormat;
						for (UINT j = 0; j < numEncoders; j++) {
							if (wszOutputMimeType == pImageCodecInfo[j].MimeType)
							{
								clsidOut = pImageCodecInfo[j].Clsid;
								status = G::Ok;
								break;
							}
						}

						G::EncoderParameters encParams;
						encParams.Count = 1;
						encParams.Parameter[0].Guid = G::EncoderQuality;
						encParams.Parameter[0].Type = G::EncoderParameterValueTypeLong;
						encParams.Parameter[0].NumberOfValues = 1;
						encParams.Parameter[0].Value = (void*)&quality;
						Gdiplus::Image *newImage = ScaleByPercent(&imageSrc, 100);
						status = newImage->Save(pStreamOut, &clsidOut, &encParams);
						//	status = newImage->Save(pStreamOut,NULL,NULL);
						delete_catch(newImage);
					}
					free(pImageCodecInfo);
				}
			}
		}
	}
	G::GdiplusShutdown(gdiplusToken);
	
	return status;
}

BOOL CAlertUtils::RecursiveDeleteDirectory(const TCHAR *path)
{
	WIN32_FIND_DATA ffd;
	TCHAR tmp[MAX_PATH+1];
	_tcscpy_s(tmp, MAX_PATH+1, path);
	if(tmp[_tcslen(tmp)-1] != _T('\\'))
	{
		_tcscat_s(tmp, MAX_PATH+1, _T("\\"));
	}
	TCHAR findstr[MAX_PATH+1];
	_tcscpy_s(findstr, MAX_PATH+1, tmp);
	_tcscat_s(findstr, MAX_PATH+1, _T("*"));
	const HANDLE hFind = FindFirstFile(findstr, &ffd);
	if(INVALID_HANDLE_VALUE != hFind)
	{
		do
		{
			TCHAR filepath[MAX_PATH+1];
			_tcscpy_s(filepath, MAX_PATH+1, tmp);
			_tcscat_s(filepath, MAX_PATH+1, ffd.cFileName);
			if(ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			{
				if(_tcscmp(ffd.cFileName, _T(".")) && _tcscmp(ffd.cFileName, _T("..")))
					RecursiveDeleteDirectory(filepath);
			}
			else
			{
				if(ffd.dwFileAttributes & FILE_ATTRIBUTE_READONLY)
				{
					_tchmod(filepath, _S_IWRITE);
				}
				DeleteFile(filepath);
			}
		}
		while(FindNextFile(hFind,&ffd));
		FindClose(hFind);
	}
	tmp[_tcslen(tmp)-1] = _T('\0'); //replace \ char
	return RemoveDirectory(tmp);
}

BOOL CAlertUtils::RecursiveMoveDirectory(const TCHAR *src, const TCHAR *dest)
{
	WIN32_FIND_DATA ffd;
	TCHAR tmpsrc[MAX_PATH+1];
	_tcscpy_s(tmpsrc, MAX_PATH+1, src);
	if(tmpsrc[_tcslen(tmpsrc)-1] != _T('\\'))
	{
		_tcscat_s(tmpsrc, MAX_PATH+1, _T("\\"));
	}

	TCHAR tmpdest[MAX_PATH+1];
	_tcscpy_s(tmpdest, MAX_PATH+1, dest);
	if(tmpdest[_tcslen(tmpdest)-1] == _T('\\'))
	{
		tmpdest[_tcslen(tmpdest)-1] = _T('\0'); //replace \ char
	}
	BOOL res = PathFileExists(tmpdest) || !_tmkdir(tmpdest);
	if(res)
	{
		_tcscat_s(tmpdest, MAX_PATH+1, _T("\\"));

		TCHAR findstr[MAX_PATH+1];
		_tcscpy_s(findstr, MAX_PATH+1, tmpsrc);
		_tcscat_s(findstr, MAX_PATH+1, _T("*"));
		const HANDLE hFind = FindFirstFile(findstr, &ffd);
		if(INVALID_HANDLE_VALUE != hFind)
		{
			do
			{
				TCHAR filesrc[MAX_PATH+1];
				_tcscpy_s(filesrc, MAX_PATH+1, tmpsrc);
				_tcscat_s(filesrc, MAX_PATH+1, ffd.cFileName);

				TCHAR filedest[MAX_PATH+1];
				_tcscpy_s(filedest, MAX_PATH+1, tmpdest);
				_tcscat_s(filedest, MAX_PATH+1, ffd.cFileName);
				if(ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
				{
					if(_tcscmp(ffd.cFileName, _T(".")) && _tcscmp(ffd.cFileName, _T("..")))
						RecursiveMoveDirectory(filesrc, filedest);
				}
				else
				{
					if(!MoveFileEx(filesrc, filedest, MOVEFILE_REPLACE_EXISTING|MOVEFILE_COPY_ALLOWED))
						res = FALSE;
				}
			}
			while(FindNextFile(hFind,&ffd));
			FindClose(hFind);
		}
	}
	tmpsrc[_tcslen(tmpsrc)-1] = _T('\0'); //replace \ char
	if(!RecursiveDeleteDirectory(tmpsrc))
		res = FALSE;
	return res;
}

bool CAlertUtils::IsIE6()
{
	bool res = false;
	CRegKey k;
	if (k.Open(HKEY_LOCAL_MACHINE,_T("Software\\Microsoft\\Internet Explorer"),KEY_READ) == ERROR_SUCCESS)
	{
		CString szBuf;
		DWORD dwBufLen = 1024;
		if (k.QueryStringValue(_T("Version"),szBuf.GetBuffer(dwBufLen),&dwBufLen) == ERROR_SUCCESS)
		{
			res = szBuf.Find(_T("6.")) == 0;
		}
	}
	return res;
}


// Extract href to media resource from url
CString CAlertUtils::ExtractHref(CString url, CString searchMask) 
{
	CString result = "";
	if (url.IsEmpty())
		return result;
	int startIdx = url.Find(searchMask);
	if (startIdx >= 0)
	{
		int finishIdx = url.Find(TEXT("\""), startIdx);
        if (finishIdx > 0)
        {
            result = url.Mid(startIdx, finishIdx - startIdx);
        }
	}
	return result;
}

// Extract file name from url
CString CAlertUtils::ExtractFileName(CString url)
{
	if (url.IsEmpty())
		return TEXT("");
	int fileNameStartIdx = url.ReverseFind(TEXT('/')) + 1;
	int size = url.GetLength() - fileNameStartIdx;
	if (size <= 0)
		return TEXT("");

	return url.Mid(fileNameStartIdx, size);
}


CString CAlertUtils::CutFileName(CString url)
{
	if(url.IsEmpty())
		return TEXT("");
	int fileNameStartIndex = url.ReverseFind(TEXT('/'))+1;
	int size = url.GetLength() - fileNameStartIndex;
	if(size <= 0 )
		return TEXT("");
	return url.Left( fileNameStartIndex );
}

//
// Andrew Maximov 08/24/2017
// Extract path to file delmited by delimiter since from mask
CString CAlertUtils::ExtractFilePath(CString src, CString mask, char delimiter)
{
	if(src.IsEmpty())
		return "";
	int filePathStartIdx = src.Find(mask) + mask.GetLength();
	if (filePathStartIdx == -1)
		return "";
	int filePathLength = src.Find(delimiter, filePathStartIdx + mask.GetLength() + 1) - filePathStartIdx;
	return src.Mid(filePathStartIdx, filePathLength);
}


//
// Andrew Maximov 08/24/2017
// Replace screensaver file name in the href with file name in the cache
CString CAlertUtils::ReplaceHref(CString html, CString fileName)
{
	CString result = replaceByMask(html, TEXT("file=\""), TEXT("\""), fileName);
	result = replaceByMask(result, TEXT("<PARAM NAME='url' VALUE='"), TEXT("'"), fileName);
	return replaceByMask(result, TEXT("<EMBED src='"), TEXT("'"), fileName);
}

//
// replace text in src between startMask and finishMask to replaceTo
//
CString replaceByMask(CString src, CString startMask, CString finishMask, CString replaceTo)
{
	int fileNameStartIdx = src.Find(startMask) + startMask.GetLength();
	int fileNameFinishIdx = src.Find(finishMask, fileNameStartIdx);
	return src.Left(fileNameStartIdx) + replaceTo + src.Right(src.GetLength() - fileNameFinishIdx);
}

// Gets client IP from Citrix registry value
CString CAlertUtils::GetCitrixSessionIp()
{
	HKEY hKey;
	DWORD dwType;

	DWORD accessFlags = KEY_READ | KEY_WOW64_64KEY;
	if (GetSystemWow64DirectoryW(NULL, 0u) == FALSE)
	{
		if (GetLastError() == ERROR_CALL_NOT_IMPLEMENTED)
			accessFlags = KEY_READ;
	}
	if (RegOpenKeyEx(HKEY_LOCAL_MACHINE, L"SOFTWARE\\Citrix\\ICA\\Session", NULL, accessFlags, &hKey) == ERROR_SUCCESS)
	{
		TCHAR lpData[1024]={0};
		DWORD bufSize = sizeof(lpData);

		if (RegQueryValueEx(hKey, L"ClientAddress", NULL, &dwType, (LPBYTE)lpData, &bufSize) == ERROR_SUCCESS)
		{
			CitrixIp = CString(lpData);
		}
		RegCloseKey(hKey);
	}
	return CitrixIp;
}

DWORD CAlertUtils::sendHTTPRequest(CString szURL, bool skipHttpsCheck ,
	const TCHAR *szRequestType, const char * szRequestData, char *outBuffer, const DWORD bufferSize )
{ 
	DWORD result=0;
	CUrl cUrl;
	DWORD dwTimeoutDefault = 30000; //30 sec
	if (cUrl.CrackUrl(szURL))
	{
		SetLastError(0);
		HINTERNET hInternet =
			::InternetOpen(
			TEXT("DeskAlerts"),
			INTERNET_OPEN_TYPE_PRECONFIG,
			NULL,NULL,
			0);
		if (hInternet != NULL)
		{
			//////////////////////
			/// set timeout
			InternetSetOption(hInternet, INTERNET_OPTION_RECEIVE_TIMEOUT, &dwTimeoutDefault, sizeof(DWORD));
			InternetSetOption(hInternet, INTERNET_OPTION_SEND_TIMEOUT, &dwTimeoutDefault, sizeof(DWORD));
			InternetSetOption(hInternet, INTERNET_OPTION_CONNECT_TIMEOUT, &dwTimeoutDefault, sizeof(DWORD));
			SetLastError(0);
			HINTERNET hConnect =
				::InternetConnect(
				hInternet,
				cUrl.GetHostName(),
				cUrl.GetPortNumber(),
				//INTERNET_DEFAULT_HTTP_PORT,
				NULL,NULL,
				INTERNET_SERVICE_HTTP,
				0,
				1);
			if (hConnect != NULL)
			{
				CString sUrl = cUrl.GetUrlPath();
				sUrl += cUrl.GetExtraInfo();

				DWORD dwFlags=INTERNET_FLAG_KEEP_CONNECTION|INTERNET_FLAG_RELOAD|INTERNET_FLAG_NO_CACHE_WRITE|INTERNET_FLAG_PRAGMA_NOCACHE;

				if(cUrl.GetScheme() == ATL_URL_SCHEME_HTTPS)
				{
					dwFlags|=INTERNET_FLAG_SECURE;
				}

				SetLastError(0);
				HINTERNET hRequest =
					::HttpOpenRequest(
					hConnect,
					szRequestType,
					sUrl,
					NULL,
					NULL,
					0,
					dwFlags,
					NULL);

				if (hRequest != NULL)
				{
					if(cUrl.GetScheme() == ATL_URL_SCHEME_HTTPS)
					{
						if(skipHttpsCheck)
						{
							DWORD dwFlags2=0;
							DWORD dwSize = sizeof(dwFlags2);
							InternetQueryOption (hRequest, INTERNET_OPTION_SECURITY_FLAGS, (LPVOID)&dwFlags2, &dwSize);
							dwFlags2 |= SECURITY_FLAG_IGNORE_UNKNOWN_CA|SECURITY_FLAG_IGNORE_REVOCATION|SECURITY_FLAG_IGNORE_CERT_CN_INVALID|SECURITY_FLAG_IGNORE_CERT_DATE_INVALID|SECURITY_FLAG_IGNORE_WRONG_USAGE;
							InternetSetOption(hRequest, INTERNET_OPTION_SECURITY_FLAGS, &dwFlags2, sizeof(dwFlags2));
						}
						DWORD pCert=0;
						InternetSetOption(hRequest,INTERNET_OPTION_CLIENT_CERT_CONTEXT,(LPVOID)&pCert,sizeof(DWORD));
					}

					BOOL bSend;
					DWORD dwError;
					PCCERT_CONTEXT pCert = NULL;
					HCERTSTORE hSystemStore = NULL;
					do
					{
						SetLastError(0);
						TCHAR* szHeaders = _T("Content-Type: application/json; charset=utf-8");
						int len = strlen(szRequestData)*sizeof szRequestData[0];
						
						bSend = HttpSendRequest(hRequest, szHeaders, _tcslen(szHeaders), (LPVOID)szRequestData, len);
						dwError = GetLastError();

                        //хардблок обработки ошибок отправки реквест. До этого была обработка связанная с сертификатом
                        //hardblock error handler. before, it processed by certificate error, now - nothing
						if( dwError != 0 )return -1;
						if (dwError == ERROR_INTERNET_CLIENT_AUTH_CERT_NEEDED)
						{
							if(!hSystemStore)
							{
								SetLastError(0);
								hSystemStore = CertOpenSystemStore(NULL, _T("MY"));
							}

							SetLastError(0);
							pCert = CertEnumCertificatesInStore(hSystemStore, pCert);

							if(!pCert) break;
							InternetSetOption(hRequest,INTERNET_OPTION_CLIENT_CERT_CONTEXT,(LPVOID)pCert,sizeof(CERT_CONTEXT));
							CertFreeCertificateContext(pCert);
						}
						else
						{
							break;
						}
					} while(true);
					if(hSystemStore)
						CertCloseStore(hSystemStore, CERT_CLOSE_STORE_CHECK_FLAG);
					TCHAR status[32]={0};
					DWORD dwSize = 32;

					BOOL bRet = FALSE;
					int cnt = 0;
					do {
						bRet = HttpQueryInfo(hRequest, HTTP_QUERY_STATUS_CODE, status, &dwSize, NULL);
					} while (!(bRet && _wtol(status) == 200) && (++cnt < 3));

					if(bRet && _wtoi(status) == 200 && outBuffer)
					{
						DWORD availableBufferSize = bufferSize;
						DWORD dwAvailable, dwRead;
						char * pTempBuf=outBuffer;
						while(InternetQueryDataAvailable(hRequest,&dwAvailable,0,0) && dwAvailable != 0 && availableBufferSize >= dwAvailable)
						{
							InternetReadFile(hRequest,pTempBuf,dwAvailable,&dwRead);
							pTempBuf += dwRead;
							availableBufferSize -= dwRead;
						}
						if (availableBufferSize > 0)
						{
							pTempBuf[0]=0;
						}
						result = (availableBufferSize >= dwAvailable) ? 1 : 0;
					}
					::InternetCloseHandle(hRequest);
					::InternetCloseHandle(hConnect);
					::InternetCloseHandle(hInternet);
				}
			}
		}
	}
	return result;
}

DWORD CAlertUtils::sendFileByHTTPRequest(CString url, CString szURL, bool skipHttpsCheck,
	const TCHAR *szRequestType, LPVOID szFileData, DWORD fileLen)
{
	CString furl = "";
	furl.Format(_T("%s/%s"), url, szURL);
	DWORD dwTimeoutDefault = 30000; //30 sec
	DWORD result = 0;
	CUrl cUrl;
	if (cUrl.CrackUrl(furl))
	{
		SetLastError(0);
		HINTERNET hInternet =
			::InternetOpen(
				TEXT("DeskAlerts"),
				INTERNET_OPEN_TYPE_PRECONFIG,
				NULL, NULL,
				0);
		if (hInternet != NULL)
		{
			//////////////////////
			/// set timeout
			InternetSetOption(hInternet, INTERNET_OPTION_RECEIVE_TIMEOUT, &dwTimeoutDefault, sizeof(DWORD));
			InternetSetOption(hInternet, INTERNET_OPTION_SEND_TIMEOUT, &dwTimeoutDefault, sizeof(DWORD));
			InternetSetOption(hInternet, INTERNET_OPTION_CONNECT_TIMEOUT, &dwTimeoutDefault, sizeof(DWORD));
			SetLastError(0);
			HINTERNET hConnect =
				::InternetConnect(
					hInternet,
					cUrl.GetHostName(),
					cUrl.GetPortNumber(),
					//INTERNET_DEFAULT_HTTP_PORT,
					NULL, NULL,
					INTERNET_SERVICE_HTTP,
					0,
					1);
			if (hConnect != NULL)
			{
				CString sUrl = cUrl.GetUrlPath();
				sUrl += cUrl.GetExtraInfo();

				DWORD dwFlags = INTERNET_FLAG_KEEP_CONNECTION | INTERNET_FLAG_RELOAD | INTERNET_FLAG_NO_CACHE_WRITE | INTERNET_FLAG_PRAGMA_NOCACHE;

				if (cUrl.GetScheme() == ATL_URL_SCHEME_HTTPS)
				{
					dwFlags |= INTERNET_FLAG_SECURE;
				}

				SetLastError(0);
				HINTERNET hRequest =
					::HttpOpenRequest(
						hConnect,
						szRequestType,
						sUrl,
						NULL,
						NULL,
						0,
						dwFlags,
						NULL);

				if (hRequest != NULL)
				{
					if (cUrl.GetScheme() == ATL_URL_SCHEME_HTTPS)
					{
						if (skipHttpsCheck)
						{
							DWORD dwFlags2 = 0;
							DWORD dwSize = sizeof(dwFlags2);
							InternetQueryOption(hRequest, INTERNET_OPTION_SECURITY_FLAGS, (LPVOID)&dwFlags2, &dwSize);
							dwFlags2 |= SECURITY_FLAG_IGNORE_UNKNOWN_CA | SECURITY_FLAG_IGNORE_REVOCATION | SECURITY_FLAG_IGNORE_CERT_CN_INVALID | SECURITY_FLAG_IGNORE_CERT_DATE_INVALID | SECURITY_FLAG_IGNORE_WRONG_USAGE;
							InternetSetOption(hRequest, INTERNET_OPTION_SECURITY_FLAGS, &dwFlags2, sizeof(dwFlags2));
						}
						DWORD pCert = 0;
						InternetSetOption(hRequest, INTERNET_OPTION_CLIENT_CERT_CONTEXT, (LPVOID)&pCert, sizeof(DWORD));
					}

					BOOL bSend;
					DWORD dwError;
					PCCERT_CONTEXT pCert = NULL;
					HCERTSTORE hSystemStore = NULL;
					do
					{
						SetLastError(0);
						TCHAR* szHeaders = _T("Content-Type: multipart/form-data;");

						bSend = HttpSendRequest(hRequest, szHeaders, _tcslen(szHeaders), szFileData, fileLen);
						dwError = GetLastError();

						if (dwError != 0)return -1;
						if (dwError == ERROR_INTERNET_CLIENT_AUTH_CERT_NEEDED)
						{
							if (!hSystemStore)
							{
								SetLastError(0);
								hSystemStore = CertOpenSystemStore(NULL, _T("MY"));
							}

							SetLastError(0);
							pCert = CertEnumCertificatesInStore(hSystemStore, pCert);

							if (!pCert) break;
							InternetSetOption(hRequest, INTERNET_OPTION_CLIENT_CERT_CONTEXT, (LPVOID)pCert, sizeof(CERT_CONTEXT));
							CertFreeCertificateContext(pCert);
						}
						else
						{
							break;
						}
					} while (true);
					if (hSystemStore)
						CertCloseStore(hSystemStore, CERT_CLOSE_STORE_CHECK_FLAG);
					TCHAR status[32] = { 0 };
					DWORD dwSize = 32;

					BOOL bRet = FALSE;
					int cnt = 0;
					do {
						bRet = HttpQueryInfo(hRequest, HTTP_QUERY_STATUS_CODE, status, &dwSize, NULL);
					} while (!(bRet && _wtol(status) == 200) && (++cnt < 3));

					if (bRet && _wtoi(status) == 200)
					{
						result = 1;
					}
					::InternetCloseHandle(hRequest);
					::InternetCloseHandle(hConnect);
					::InternetCloseHandle(hInternet);
				}
			}
		}
	}
	return result;
}

CString CAlertUtils::getTempFileName(CString prefix, int i)
{
	CString tmp = getTempPath();
	CString tmp2="";
	if(!tmp.IsEmpty())
	{
		_tmkdir(tmp);
		srand(GetTickCount64());

		tmp2.Format(_T("%s\\%s_%x_%x_%x.html"),tmp,prefix,::GetCurrentThreadId(),i,rand());
	}
	return tmp2;
}

CString CAlertUtils::getTempPath()
{
	CString tmp;
	tmp.GetEnvironmentVariable(_T("TEMP"));
	if(tmp.GetAt(tmp.GetLength()-1) != _T('\\'))
	{
		tmp += _T('\\');
	}
	tmp += _T("Alerts");
	return tmp;
}

string CAlertUtils::readFile2String(const char* path) {
  FILE* file = fopen(path, "rb");
  if (!file)
    return string("");
  fseek(file, 0, SEEK_END);
  long const size = ftell(file);
  if (size <= 0 )
      return string("");
  unsigned long const usize = static_cast<unsigned long>(size);
  fseek(file, 0, SEEK_SET);
  string text;
  char* buffer = new char[usize + 1];
  buffer[usize] = 0;
  if (fread(buffer, 1, usize, file) == usize)
    text = buffer;
  fclose(file);
  delete[] buffer;
  return text;
}

BOOL CALLBACK CAlertUtils::Enum_FindMainWindow_windows_callback(HWND handle, LPARAM lParam)
{
	window_handle_processid& data = *(window_handle_processid*)lParam;
	unsigned long process_id = 0;
	GetWindowThreadProcessId(handle, &process_id);
	if (data.process_id == process_id && is_main_window(handle))
	{
		data.window_handle = handle;
		return FALSE;
	}
	return TRUE;
}

BOOL CAlertUtils::is_main_window(HWND handle)
{
	return GetWindow(handle, GW_OWNER) == (HWND)0;
}

void CAlertUtils::PostMessageToWatchDog(DWORD Message, LPARAM lparam, WPARAM wparam) 
{
	if (Global_MainThreadId != 0)
	{
		::PostThreadMessage(Global_MainThreadId, Message, wparam, lparam);
	}
	else
	{
		PROCESSENTRY32 entry;
		entry.dwSize = sizeof(PROCESSENTRY32);
		DWORD CurProcID = GetCurrentProcessId();
		DWORD parentProcessID = 0;
		HANDLE hsnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);
		if (Process32First(hsnapshot, &entry) != FALSE)
		{
			HANDLE hParentProcess;
			while (Process32Next(hsnapshot, &entry) != FALSE)
			{
				CString processName = _T("deskalerts.exe");
				if (_tcsicmp(entry.szExeFile, processName) != 0)
				{
					continue;
				}
				if (entry.th32ProcessID != CurProcID)
				{
					continue;
				}
				parentProcessID = entry.th32ParentProcessID;
			}
			if (parentProcessID)
			{
				HANDLE hThreadSnap = CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD, 0);
				DWORD dwMainThreadID = 0;
				ULONGLONG ullMinCreateTime = MAXULONGLONG;
				THREADENTRY32 th32;
				th32.dwSize = sizeof(THREADENTRY32);
				BOOL bOK = TRUE;
				for (bOK = Thread32First(hThreadSnap, &th32); bOK; bOK = Thread32Next(hThreadSnap, &th32))
				{
					if (th32.th32OwnerProcessID == parentProcessID)
					{
						HANDLE hThread = OpenThread(THREAD_QUERY_INFORMATION, TRUE, th32.th32ThreadID);
						if (hThread)
						{
							FILETIME afTimes[4] = { 0 };
							if (GetThreadTimes(hThread, &afTimes[0], &afTimes[1], &afTimes[2], &afTimes[3]))
							{
								ULONGLONG ullTest = MAKEULONGLONG(afTimes[0].dwLowDateTime, afTimes[0].dwHighDateTime);
								if (ullTest && ullTest < ullMinCreateTime)
								{
									ullMinCreateTime = ullTest;
									dwMainThreadID = th32.th32ThreadID; // let it be main... :)
								}
							}
							CloseHandle(hThread);
						}
					}
				}
				CloseHandle(hThreadSnap);
				Global_MainThreadId = dwMainThreadID;
				::PostThreadMessage(dwMainThreadID, Message, wparam, lparam);
			}
		}

		CloseHandle(hsnapshot);

	}
}

void CAlertUtils::PostMessageToWatchDog(MSG Message)
{
	PostMessageToWatchDog(Message.message, Message.lParam, Message.wParam);
}
