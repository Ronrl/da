
#pragma once

#if !defined(AFX_POSTFUNCTIONS_H__B0580FFF_1E07_4e55_946C_BC8B26901C36__INCLUDED_)
#define AFX_POSTFUNCTIONS_H__B0580FFF_1E07_4e55_946C_BC8B26901C36__INCLUDED_

// PostThreadMessage
template<class L>
inline BOOL PostThreadMessageDelLParam(
									   __in DWORD idThread,
									   __in UINT Msg,
									   __in WPARAM wParam,
									   __in const L *lParam)
{
	const BOOL res = PostThreadMessage(idThread, Msg, wParam, (LPARAM)lParam);
	if(!res && lParam)
	{
		delete_catch(lParam);
	}
	return res;
}

template<class W>
inline BOOL PostThreadMessageDelWParam(
									   __in DWORD idThread,
									   __in UINT Msg,
									   __in const W *wParam,
									   __in LPARAM lParam)
{
	const BOOL res = PostThreadMessage(idThread, Msg, (WPARAM)wParam, lParam);
	if(!res && wParam)
	{
		delete_catch(wParam);
	}
	return res;
}

template<class W, class L>
inline BOOL PostThreadMessageDelBoth(
									 __in DWORD idThread,
									 __in UINT Msg,
									 __in const W *wParam,
									 __in const L *lParam)
{
	const BOOL res = PostThreadMessage(idThread, Msg, (WPARAM)wParam, (LPARAM)lParam);
	if(!res)
	{
		if(wParam) delete_catch(wParam);
		if(lParam) delete_catch(lParam);
	}
	return res;
}

// PostMessage
template<class L>
inline BOOL PostMessageDelLParam(
								 __in_opt HWND hWnd,
								 __in UINT Msg,
								 __in WPARAM wParam,
								 __in const L *lParam)
{
	const BOOL res = PostMessage(hWnd, Msg, wParam, (LPARAM)lParam);
	if(!res && lParam)
	{
		delete_catch(lParam);
	}
	return res;
}

template<class W>
inline BOOL PostMessageDelWParam(
								 __in_opt HWND hWnd,
								 __in UINT Msg,
								 __in const W *wParam,
								 __in LPARAM lParam)
{
	const BOOL res = PostMessage(hWnd, Msg, (WPARAM)wParam, lParam);
	if(!res && wParam)
	{
		delete_catch(wParam);
	}
	return res;
}

template<class W, class L>
inline BOOL PostMessageDelBoth(
							   __in_opt HWND hWnd,
							   __in UINT Msg,
							   __in const W *wParam,
							   __in const L *lParam)
{
	const BOOL res = PostMessage(hWnd, Msg, (WPARAM)wParam, (LPARAM)lParam);
	if(!res)
	{
		if(wParam) delete_catch(wParam);
		if(lParam) delete_catch(lParam);
	}
	return res;
}

#endif // !defined(AFX_POSTFUNCTIONS_H__B0580FFF_1E07_4e55_946C_BC8B26901C36__INCLUDED_)
