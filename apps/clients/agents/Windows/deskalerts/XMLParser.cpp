////////////////////////////////////////
//XML Parser Implementation
///////////////////////////////////////


#include "stdafx.h"
#include "XMLParser.h"

void XmlNode::Determine(XMLParser* /*pParser*/)
{
}

void XmlNode::DetermineAttrib(XMLParser *pParser)
{
	pParser->DetermineCDATA(&m_cdata);
}

int XMLParser::Parse(CString pString, bool IsFromFile)
{
	CComPtr<IXMLDOMDocument> spXMLDOM;
	HRESULT hr = spXMLDOM.CoCreateInstance(__uuidof(DOMDocument));
	if(FAILED(hr))
	{
		return ERR_CANNOT_CREATE_MSXML_OBJECT;
	}
	if(spXMLDOM.p == NULL)
	{
		return  ERR_CANNOT_CREATE_MSXML_OBJECT;
	}

	// Load the XML document..
	VARIANT_BOOL bSuccess = false;

	//load XML from file
	if(IsFromFile)
	{
		hr = spXMLDOM->load(CComVariant(pString),&bSuccess);
	}
	else
	{
		hr = spXMLDOM->loadXML(CComBSTR(pString),&bSuccess);
	}

	if(FAILED(hr) || !bSuccess)
	{
		return ERR_XML_LOAD_FAILED;
	}

	CComBSTR NodeName(RootNodeName);

	hr = spXMLDOM->selectSingleNode(NodeName,spNode);

	if(FAILED(hr) || hr == S_FALSE)
	{
		pString.MakeLower();
		NodeName = pString;
		if(*spNode) (*spNode)->Release();
		hr = spXMLDOM->selectSingleNode(NodeName,spNode);
		if(FAILED(hr) || hr == S_FALSE)
		{
			return ERR_INVALID_ROOT_NODE;
		}
	}

	/////////////////////////////////
	///////Parsing Algorithm/////////
	/////////////////////////////////

	NODESTRUCT NodeStruct;

	NodeStruct.spNode = *spNode;
	NodeStruct.pNode = pToolbar;
	NodeStruct.pNode->m_bFlag = false;
	NodeStruct.isFirstChild = true;

	DWORD depth = NodeStack.size();

	NodeStack.push(NodeStruct);
	while (NodeStack.size() > depth)
	{
		NodeStruct = NodeStack.top();

		if (*spNode) (*spNode)->Release();

		if (NodeStruct.isFirstChild)
		{
			NodeStruct.pNode->DetermineAttrib(this);
			hr = NodeStruct.spNode->get_firstChild(spNode);
			NodeStruct.isFirstChild = false;
		}
		else
			hr = NodeStruct.spNode->get_nextSibling(spNode);

		if (FAILED(hr) || hr == S_FALSE)
		{
			bIsTAGParsed = true;

			NodeStack.pop();
		}
		else
		{
			NodeStruct.spNode = *spNode;
			NodeStack.pop();
			NodeStack.push(NodeStruct);
			bIsTAGParsed = false;
			NodeStruct.pNode->Determine(this);
		}
	}

	if (*spNode) (*spNode)->Release();
	return ERR_NONE;
}

void XMLParser::PushCurrentNode(NODESTRUCT NodeStruct)
{
	NodeStruct.spNode = *spNode;
	NodeStack.push(NodeStruct);
}

int XMLParser::DetermineString(TCHAR *pName, CString *pString)
{
	if (!pString)
		return ERR_NULL_POINTER;
	if (!pString->IsEmpty())
		return ERR_CANT_ERASE_OLD_DATA;

	CComVariant varValue;
	int result = DetermineVariant(pName, &varValue);
	if (result == ERR_NONE && varValue.vt == VT_BSTR)
		*pString = varValue.bstrVal;
	return result;
}

#if !defined(MIN)
#define MIN(a,b)	((a) < (b) ? (a) : (b))
#endif
int XMLParser::DetermineUInt(TCHAR *pName, unsigned int *pUInt, unsigned int default)
{
	long result = *pUInt;
	const int res = DetermineLong(pName, &result, default);
	if(result > MAXUINT) result = MAXUINT;
	*pUInt = result;
	return res;
}

int XMLParser::DetermineInt(TCHAR *pName, int *pInt, int default)
{
	long result = *pInt;
	const int res = DetermineLong(pName, &result, default);
	*pInt = result;
	return res;
}

int XMLParser::DetermineLong(TCHAR *pName, long *pLong, long default)
{
	if (!pLong)
		return ERR_NULL_POINTER;
	if (!*pLong)
		return ERR_CANT_ERASE_OLD_DATA;

	*pLong = default;

	CComVariant varValue;
	int result = DetermineVariant(pName, &varValue);
	if(result == ERR_NONE)
	{
		if(varValue.vt == VT_I4)
			*pLong = varValue.lVal;
		else if(varValue.vt == VT_BSTR && _tcslen(varValue.bstrVal) > 0)
			*pLong = _ttol(varValue.bstrVal);
	}
	return result;
}

int XMLParser::DetermineVariant(TCHAR *pName, CComVariant *pVariant)
{
	if (!pVariant)
		return ERR_NULL_POINTER;
	if (pVariant->vt != VT_EMPTY)
		return ERR_CANT_ERASE_OLD_DATA;

	CComQIPtr<IXMLDOMElement> spElement = GetCurrentNode();

	HRESULT hr = spElement->getAttribute(CComBSTR(pName), pVariant);

	if(FAILED(hr) || hr == S_FALSE)
	{
		return ERR_NOT_FOUND;
	}

	return ERR_NONE;
}

int XMLParser::DetermineCDATA(CString *pString)
{
	if (!pString)
		return ERR_NULL_POINTER;
	if (!pString->IsEmpty())
		return ERR_CANT_ERASE_OLD_DATA;

	CComPtr<IXMLDOMNode> _spNode = GetCurrentNode();

	CComVariant varValue;
	HRESULT hr = _spNode->get_nodeTypedValue(&varValue);

	if (hr == S_FALSE || FAILED(hr))
	{
		return ERR_NOT_FOUND;
	}

	if (varValue.vt == VT_BSTR)
		*pString = varValue.bstrVal;

	return ERR_NONE;
}

int XMLParser::DetermineXML(CString *pString)
{
	if (!pString)
		return ERR_NULL_POINTER;
	if (!pString->IsEmpty())
		return ERR_CANT_ERASE_OLD_DATA;

	CComPtr<IXMLDOMNode> _spNode = GetCurrentNode();

	CComBSTR bstrValue;
	HRESULT hr = _spNode->get_xml(&bstrValue);

	if (hr == S_FALSE || FAILED(hr))
	{
		return ERR_NOT_FOUND;
	}

	*pString = bstrValue;

	return ERR_NONE;
}

int XMLParser::DetermineInnerXML(CString *pString)
{
	if (!pString)
		return ERR_NULL_POINTER;
	if (!pString->IsEmpty())
		return ERR_CANT_ERASE_OLD_DATA;

	CComPtr<IXMLDOMNode> _spNode = GetCurrentNode();

	HRESULT hr;
	pString->Empty();

	CComPtr<IXMLDOMNodeList> pList;
	if(SUCCEEDED(hr = _spNode->get_childNodes(&pList)) && pList)
	{
		long length = 0;
		if(SUCCEEDED(hr = pList->get_length(&length)) && length > 0)
		{
			for(long i = 0; i < length; i++)
			{
				CComPtr<IXMLDOMNode> pChild;
				if(SUCCEEDED(hr = pList->get_item(i, &pChild)) && pChild)
				{
					CComBSTR bstrValue;
					if(SUCCEEDED(hr = pChild->get_xml(&bstrValue)))
						*pString += bstrValue;
				}
			}
		}
	}

	if (hr == S_FALSE || FAILED(hr))
	{
		return ERR_NOT_FOUND;
	}

	return ERR_NONE;
}
