#include "StdAfx.h"
#include "sysmenu.h"
#include "resource.h"
#include "AlertUtils.h"
#include <VSStyle.h>
#include <uxtheme.h>


CSysMenu::CSysMenu(NodeALERTS::NodeVIEWS::NodeVIEW* mStrc)
{
	parentHWND =	::GetDesktopWindow();
	m_nType		  = MIT_ICON;		// default menu type (Icon Menu)
	m_menuStruct = (NodeALERTS::NodeVIEWS::NodeMENU*)mStrc;
}

CSysMenu::~CSysMenu(void)
{
}

void CSysMenu::ChangeIcon(int iMode, CString *sCaption, int iType/* = NIM_MODIFY*/)
{
	CImageList imList;
	imList.Attach(_iAlertModel->getImageList());

	sysIcon.LoadIcon(IDI_SYSDEF,0,0);

	NOTIFYICONDATA notifyIconData;
	ZeroMemory(&notifyIconData, sizeof(notifyIconData));
	notifyIconData.cbSize = sizeof(notifyIconData);
	notifyIconData.uFlags = NIF_ICON | NIF_TIP | NIF_MESSAGE;

	if (m_menuStruct->m_img!=_T(""))
		notifyIconData.hIcon = imList.ExtractIcon(CAlertUtils::str2int(m_menuStruct->m_img,0));
	else
		notifyIconData.hIcon = sysIcon;

	if(iMode == _STANDBY_)
	{
		if (m_menuStruct->m_standbyimg!=_T(""))
			notifyIconData.hIcon = imList.ExtractIcon(CAlertUtils::str2int(m_menuStruct->m_standbyimg,0));
	}
	else if (iMode == _DISABLE_)
	{
		if (m_menuStruct->m_disableimg!=_T(""))
			notifyIconData.hIcon = imList.ExtractIcon(CAlertUtils::str2int(m_menuStruct->m_disableimg,0));

	}
	else if (iMode == _DISABLE_NEW_)
	{
		if (m_menuStruct->m_disablenewimg!=_T(""))
			notifyIconData.hIcon = imList.ExtractIcon(CAlertUtils::str2int(m_menuStruct->m_disablenewimg,0));
	}
	else if (iMode == _OFFLINE_)
	{
		if (m_menuStruct->m_offlineimg!=_T(""))
			notifyIconData.hIcon = imList.ExtractIcon(CAlertUtils::str2int(m_menuStruct->m_offlineimg,0));
	}
	else if (iMode == _MESSAGES_UNREAD_)
	{
		if (m_menuStruct->m_unread_img!=_T(""))
			notifyIconData.hIcon = imList.ExtractIcon(CAlertUtils::str2int(m_menuStruct->m_unread_img,0));
	}
	else if (iMode == _MESSAGES_UNREAD_AND_UNOBTRUSIVE_MODE)
	{
		if (m_menuStruct->m_unread_img != _T(""))
			notifyIconData.hIcon = imList.ExtractIcon(CAlertUtils::str2int(m_menuStruct->m_disablenewimg, 0));
	}

	notifyIconData.hWnd = m_hWnd;
	notifyIconData.uCallbackMessage = UWM_NOTIFYICON;
	if(!sCaption) sCaption = &_iAlertModel->getPropertyString(CString(_T("deskalerts_name")), &CString(_T("DeskAlerts")));
	_tcscpy_s(notifyIconData.szTip, sizeof(notifyIconData.szTip)/sizeof(TCHAR), *sCaption);

	::Shell_NotifyIcon(iType, &notifyIconData);

	imList.Detach();
}

bool CSysMenu::show(CString param, shared_ptr<showData> ex, BOOL /*isNewAlert*/)
{
	if (_iAlertModel)
	{
		parentHWND = (HWND)_iAlertModel->getProperty(CString(_T("mainHWND")));
	}
	this->Create(parentHWND);
	m_cMenu = createMenu(m_cMenu);
	return true;
}

void CSysMenu::hide(CString param)
{
	if (::GetParent(m_hWnd)==::GetDesktopWindow()) this->DestroyWindow();
	for(UINT i = 0; i<m_mItems.size(); i++)
	{
		if(m_mItems.back())
			delete_catch(m_mItems.back());
		m_mItems.pop_back();
	}
	//delete_catch(m_cMenu);
}

/************************************************************************/
/* Window                                                               */
/************************************************************************/

LRESULT CSysMenu::OnCreate(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{

	CImageList imList;
	imList.Attach(_iAlertModel->getImageList());

	sysIcon.LoadIcon(IDI_SYSDEF,0,0);

	NOTIFYICONDATA notifyIconData;
	ZeroMemory(&notifyIconData, sizeof(notifyIconData));
	notifyIconData.cbSize = sizeof(notifyIconData);
	notifyIconData.uFlags = NIF_ICON | NIF_TIP | NIF_MESSAGE;
	if (m_menuStruct->m_img!=_T(""))
	{
		notifyIconData.hIcon = imList.ExtractIcon(CAlertUtils::str2int(m_menuStruct->m_img,0));
	}
	else
	{
		notifyIconData.hIcon = sysIcon;
	}
	
	notifyIconData.uCallbackMessage = UWM_NOTIFYICON;
	_tcscpy_s(notifyIconData.szTip, sizeof(notifyIconData.szTip)/sizeof(TCHAR),_iAlertModel->getPropertyString(CString(_T("deskalerts_name")), &CString(_T("DeskAlerts"))));
	///////////////////////////////////
	/// Destroy old TrayMenu icon 
	CString hwndStringIn;
	CString hwndStringInEmpty("");
	hwndStringIn = _iAlertModel->getPropertyString(CString(_T("DAtrayHWND")),&hwndStringInEmpty);
	if (hwndStringIn != hwndStringInEmpty)
	{
		HWND inBaseHWND = (HWND)_ttoi(hwndStringIn.GetString());
		notifyIconData.hWnd = inBaseHWND;
		::Shell_NotifyIcon(NIM_DELETE, &notifyIconData);
	}
	///////////////////////////////////
	/// Create new TrayMenu icon 
	CString hwndStringOut;
	notifyIconData.hWnd = m_hWnd;
	hwndStringOut.Format(_T("%d"), notifyIconData.hWnd);
	if (::Shell_NotifyIcon(NIM_ADD, &notifyIconData))
	{
		_iAlertModel->setPropertyString(CString(_T("DAtrayHWND")), hwndStringOut);
	}


	imList.Detach();

	return 0;
}

LRESULT CSysMenu::OnDestroy(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	NOTIFYICONDATA notifyIconData;
	ZeroMemory(&notifyIconData, sizeof(notifyIconData));
	notifyIconData.cbSize = sizeof(notifyIconData);
	notifyIconData.hWnd = m_hWnd;
	::Shell_NotifyIcon(NIM_DELETE, &notifyIconData);
	return 0;
}


LRESULT CSysMenu::OnNotifyIcon(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM lParam, BOOL& /*bHandled*/)
{
	if(lParam == WM_RBUTTONUP)
	{
		showMenu();
	}
	else if (lParam == WM_LBUTTONDOWN)
	{
		if (!m_menuStruct->m_command.IsEmpty())
		{
			SysMenuEvent cSysMenuEvent(
				CString(_T("select")),
				CString(_T("sysmenu")),
				CString(m_menuStruct->m_command)
				);
			_iAlertController->actionPerformed(cSysMenuEvent);
		}
		else
		{
			showMenu();
		}
	}
	return 0;
}


LRESULT CSysMenu::OnCommand(UINT /*uMsg*/, WPARAM wParam, LPARAM /*lParam*/, BOOL& bHandled)
{
	int typeCom = HIWORD(wParam);
	int wID = LOWORD(wParam);
	HTHEME theme = OpenThemeData(m_hWnd, VSCLASS_MENU);
	if (typeCom == 0) //if menu
	{
		WTL::CMenuItemInfo mif;
		mif.fMask = MIIM_DATA | MIIM_ID;
		m_cMenu.GetMenuItemInfo(wID, FALSE, &mif);
		CMenuItem * pItem = (CMenuItem *)mif.dwItemData;
		CString menuCommand = pItem->m_sCommand;

		if (!pItem->m_szPressedText.IsEmpty())
		{
			CString sTmp = pItem->m_szText;
			pItem->m_szText = pItem->m_szPressedText;
			pItem->m_szPressedText = sTmp;
			if (theme)
			{
				mif.fMask |= MIIM_STRING;
				mif.dwTypeData = pItem->m_szText.GetBuffer();
				mif.cch = pItem->m_szText.GetLength();
			}
		}
		if ( pItem->m_iPressedImage >= 0)
		{
			CBitmap *pTmp;
			pTmp = pItem->m_pBitmap;
			pItem->m_pBitmap = pItem->m_pPressedBitmap;
			pItem->m_pPressedBitmap = pTmp;
			swap(pItem->m_iImage, pItem->m_iPressedImage);
			if (theme)
			{
				mif.fMask |= MIIM_BITMAP;
				mif.hbmpItem = pItem->m_pBitmap->m_hBitmap;
			}
		}
		if (theme)
		{
			m_cMenu.SetMenuItemInfo(wID, FALSE, &mif);
		}
		SysMenuEvent cSysMenuEvent(
			CString(_T("select")),
			CString(_T("sysmenu")),
			CString(menuCommand)
			);
		_iAlertController->actionPerformed(cSysMenuEvent);


		CSysMenu::destroyMenu(m_cMenu);
	}

	bHandled = TRUE;
	return S_OK;
}

/************************************************************************/
/* MENU                                                                 */
/************************************************************************/

WTL::CMenu& CSysMenu::destroyMenu(WTL::CMenu& menu)
{
	return menu;
}

WTL::CMenu& CSysMenu::createMenu(WTL::CMenu& menu,NodeALERTS::NodeVIEWS::NodeMENU* item,int& menuID,HWND hwnd)
{
	menu.DestroyMenu();
	menu.CreatePopupMenu();

	HTHEME theme = OpenThemeData(hwnd, VSCLASS_MENU);

	if (!item->m_item.empty())
	{
		vector<NodeALERTS::NodeVIEWS::NodeITEM*>::iterator it;
		it = item->m_item.begin();
		int i=0;
		while(it!=item->m_item.end())
		{
			WTL::CMenuItemInfo minfo;

			if ((*it)->m_tagName==CString(_T("SEPARATOR")))
			{
				minfo.fType = MFT_SEPARATOR/*| MFT_OWNERDRAW*/;
				minfo.fMask = MIIM_ID | MIIM_FTYPE;
			}
			else
			{
				minfo.fType = MFT_STRING;
				if (!theme)
				{
					minfo.fType |= MFT_OWNERDRAW;
				}
				
				minfo.fMask = MIIM_STRING | MIIM_DATA | MIIM_ID | MIIM_FTYPE;
				minfo.fState = MFS_ENABLED;
				minfo.dwTypeData = (*it)->m_caption.GetBuffer();
				minfo.cch = (*it)->m_caption.GetLength();

				CMenuItem* pItem = new CMenuItem((*it)->m_caption, menuID);
				m_mItems.push_back(pItem);
				pItem->m_szPressedText = (*it)->m_pressed_caption;
				pItem->m_sCommand = (*it)->m_command;
				minfo.dwItemData = (ULONG_PTR) pItem;
				minfo.wID = menuID;
				menuID++;
					CImageList imList;
					imList.Attach(_iAlertModel->getImageList());
					CBitmap *btm = new CBitmap();
					CAlertUtils::GetImageFromList(&imList, CAlertUtils::str2int((*it)->m_img, -1),btm, hwnd);
					pItem->m_pBitmap = btm;
					pItem->m_iImage  = CAlertUtils::str2int((*it)->m_img, -1);
					if (theme)
					{
						minfo.fMask |= MIIM_BITMAP;
						minfo.hbmpItem = btm->m_hBitmap;
					}
					if ((*it)->m_pressed_img!=_T(""))
					{
						CBitmap *btm = new CBitmap();
						CAlertUtils::GetImageFromList(&imList, CAlertUtils::str2int((*it)->m_pressed_img, -1),btm, hwnd);
						pItem->m_pPressedBitmap = btm;
						pItem->m_iPressedImage  = CAlertUtils::str2int((*it)->m_pressed_img, -1);
					}
					imList.Detach();
				if ((*it)->m_tagName==CString(_T("MENU")))
				{
					minfo.fMask |= MIIM_SUBMENU;
					minfo.hSubMenu = CSysMenu::createMenu( *(new WTL::CMenu()),
						(NodeALERTS::NodeVIEWS::NodeMENU*)(*it),menuID,hwnd).Detach();
				}
			}

			menu.InsertMenuItem(i,TRUE,&minfo);

			i++;
			it++;
		}
	}
	return menu;
}

WTL::CMenu& CSysMenu::createMenu(WTL::CMenu& popupMenu)
{
	int menuID = 1;
	if (m_menuStruct)
	{
		return CSysMenu::createMenu(popupMenu,m_menuStruct,menuID,m_hWnd);
	}
	return popupMenu;
}

void CSysMenu::showMenu()
{
	if (m_menuStruct)
	{
	//	WTL::CMenu& cMenu = createMenu(m_cMenu);
		if (m_cMenu.IsMenu())
		{
			POINT pt;
			::GetCursorPos(&pt);
			if (parentHWND==::GetDesktopWindow())
				::SetForegroundWindow(m_hWnd);
			else
				::SetForegroundWindow(parentHWND);
			m_cMenu.TrackPopupMenu(NULL,pt.x, pt.y,m_hWnd,NULL);
		}
		PostMessage(WM_NULL, 0, 0);
	}
}


LRESULT CSysMenu::OnDrawItem(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM lParam, BOOL& /*bHandled*/)
{
	DrawItem((LPDRAWITEMSTRUCT) lParam);
	return S_OK;
}

LRESULT CSysMenu::OnMeasureItem(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM lParam, BOOL& /*bHandled*/)
{
	MeasureItem((LPMEASUREITEMSTRUCT) lParam);
	return S_OK;
}

void CSysMenu::MeasureItem(LPMEASUREITEMSTRUCT lpMIS)
{
	UINT state = m_cMenu.GetMenuState(lpMIS->itemID, MF_BYCOMMAND);

	CMenuItem* pItem = (CMenuItem *)(lpMIS->itemData);
	if (m_hWnd)
	{
		HDC hdc = ::GetDC(m_hWnd);
		SIZE size;
		GetTextExtentPoint32(hdc, pItem->m_szText,
			_tcslen(pItem->m_szText), &size);
		lpMIS->itemWidth = size.cx+10;
		::ReleaseDC( NULL, hdc );
	}
	else
		lpMIS->itemWidth =  pItem->m_nWidth+20;

	if( state & MF_SEPARATOR )		// item state is separator
		lpMIS->itemHeight = 6;
	else								// other item state
		lpMIS->itemHeight = 20;//pItem->m_nHeight;
}


void CSysMenu::DrawXPMenu(LPDRAWITEMSTRUCT lpDIS)
{
	HDC hDC = lpDIS->hDC;
	CDC* pDC = new CDC(hDC);
	//WTL::CDCHandle *pDC = new WTL::CDCHandle(lpDIS->hDC);

	CMenuItem* pItem = reinterpret_cast<CMenuItem *>(lpDIS->itemData);
	CRect rectFull(&lpDIS->rcItem);

	// get icon region and text region
	CRect rectIcon(rectFull.left+1,rectFull.top,rectFull.left+20,rectFull.top+20);
	CRect rectText(rectIcon.right+5,rectFull.top,rectFull.right,rectFull.bottom);

	UINT state = m_cMenu.GetMenuState(lpDIS->itemID, MF_BYCOMMAND);
	//rectFull.OffsetRect(22,0);
	if (lpDIS->itemAction & ODA_DRAWENTIRE)
	{
		// paint the brush and icon item in requested
		CRect rect;
		// paint item background
		pDC->FillRect(rectFull,(COLOR_MENU));
		pDC->FillRect(rectIcon,(COLOR_MENU));

		if( pItem->m_pBitmap )
		{	// the item has bitmap - repaint icon region and bitmap
			CDC memDC;
			memDC.CreateCompatibleDC(hDC);
			memDC.SelectBitmap(pItem->m_pBitmap->m_hBitmap);
			pDC->BitBlt(rectIcon.left+2,rectIcon.top+2,16,16, memDC.m_hDC,0,0,SRCAND);
		}

		// draw display text
		pDC->SetBkMode(TRANSPARENT);
		rect.CopyRect(rectText);
		rect.DeflateRect(2,2,2,2);
		rect.OffsetRect(6,2);
		if( state & MFS_DISABLED )	// item has been disabled
			pDC->SetTextColor(GetSysColor(COLOR_3DFACE));
		pDC->DrawText(pItem->m_szText,lstrlen(pItem->m_szText),rect,DT_LEFT|DT_SINGLELINE);
	}

	if ((lpDIS->itemState & ODS_SELECTED) && (lpDIS->itemAction & (ODA_SELECT | ODA_DRAWENTIRE)))
	{
		// item has been selected
		if( state & MFS_DISABLED )
			return;					// item has been disabled
		CRect  rect;
		rect.CopyRect(rectFull);
		pDC->FillRect(rect,COLOR_HIGHLIGHT);
		pDC->SetBkMode(TRANSPARENT);
		if( pItem->m_pBitmap )
		{	// the item has bitmap - repaint icon region and bitmap

			CImageList imList;
			imList.Attach(_iAlertModel->getImageList());
			imList.Draw(pDC->m_hDC, pItem->m_iImage, CPoint(rectIcon.left+2, rectIcon.top+2), ILD_TRANSPARENT);
		}
		rect.CopyRect(rectText);
		rect.DeflateRect(2,2,2,2);
		rect.OffsetRect(6,2);
		pDC->SetTextColor(GetSysColor(COLOR_HIGHLIGHTTEXT));
		pDC->DrawText(pItem->m_szText,lstrlen(pItem->m_szText),rect,DT_LEFT|DT_SINGLELINE);
	}

	if (!(lpDIS->itemState & ODS_SELECTED) && (lpDIS->itemAction & ODA_SELECT))
	{
		// item has been de-selected
		CRect rect;
		rect.CopyRect(rectIcon);

		pDC->FillRect(rectFull,(COLOR_MENU));

		if( pItem->m_pBitmap )
		{	// the item has bitmap - repaint icon region and bitmap

			CDC memDC;
			memDC.CreateCompatibleDC(hDC);
			memDC.SelectBitmap(pItem->m_pBitmap->m_hBitmap);
			pDC->BitBlt(rectIcon.left+2,rectIcon.top+2,16,16, memDC.m_hDC,0,0,SRCCOPY);
		}

		// draw display text

		rect.CopyRect(rectText);
		rect.DeflateRect(2,2,2,2);
		rect.OffsetRect(6,2);
		pDC->SetBkMode(TRANSPARENT);
		if( state & MFS_DISABLED ) // item has been disabled
			pDC->SetTextColor(COLOR_3DLIGHT);

		pDC->DrawText(pItem->m_szText,lstrlen(pItem->m_szText),rect,DT_LEFT|DT_SINGLELINE);
	}
}

void CSysMenu::DrawItem(LPDRAWITEMSTRUCT lpDIS)
{
	DrawXPMenu(lpDIS);
}

void CSysMenu::DrawCheckMark(CDC* /*pDC*/,int /*x*/,int /*y*/,COLORREF /*color*/)
{
}

void CSysMenu::refreshIcon(int stateIndex)
{
	if(stateIndex & _OFFLINE_)
		ChangeIcon(_OFFLINE_,&_iAlertModel->getPropertyString(CString(_T("modeCaption"))));
	else if(stateIndex & _STANDBY_)
		ChangeIcon(_STANDBY_);
	else if(stateIndex & _DISABLE_NEW_)
		ChangeIcon(_DISABLE_NEW_, &_iAlertModel->getPropertyString(CString(_T("disableCaption"))));
	else if (stateIndex & _DISABLE_)
	{
		if (stateIndex & _MESSAGES_UNREAD_)
		{
			ChangeIcon(_MESSAGES_UNREAD_AND_UNOBTRUSIVE_MODE, &_iAlertModel->getPropertyString(CString(_T("unreadCaption"))));
		}
		else
		{
			ChangeIcon(_DISABLE_);
		}
	}
	else if(stateIndex & _MESSAGES_UNREAD_)
		ChangeIcon(_MESSAGES_UNREAD_, &_iAlertModel->getPropertyString(CString(_T("unreadCaption"))));
	else
		ChangeIcon(_NORMAL_);
}

