//
//  AlertUtils.h
//  DeskAlerts Client
//  Common module
//
//  Copyright Softomate 2006-2009. All rights reserved.
//

#pragma once

#include "IInclude.h"
#include "atlrx.h"
#include <gdiplus.h>
#include <atlstr.h>

#define FILE_SEPARATOR _T("<!-- separator -->")

class CAlertUtils
{
public:

	CAlertUtils();
	~CAlertUtils(void);
	
	static CString CitrixIp;

	static _bstr_t Key;
	static DWORD WINAPI AsyncURLRequestFunc(LPVOID lpParam);
	static DWORD AsyncURLRequest(LPCTSTR szURL);
	static HRESULT URLDownloadToFile(LPCTSTR szURL, LPCTSTR szFileName = NULL
		, CString *result = NULL, BOOL *isUTF8 = NULL, CString *key = NULL
		, CString *headers = NULL, CString *postData = NULL, int reqVerb = OLDcalcVerbByPostData, int *returnedHTTPStatusCode = NULL);


	static int str2sec(CString& str, int def = 0);
	static int str2int(CString& str,int def = 0);
	static INT64 str2int64(CString& str, INT64 def = 0LL);
	static long hexstr2long(CString& str,long def);

	static HKEY GetCompanyRegistryKey();
	static HKEY GetAppRegistryKey();
	static CString getValue(const CString& s,const CString& def);
	static CString getValue(const CString& s);
	static void setValue(const CString& s,const CString& val, BOOL common = false);
	static void GetImageFromList(WTL::CImageList *lstImages,int nImage, WTL::CBitmap* destBitmap,HWND hwnd);
	static CString CreateGUID();
	static void checkPixel(CString pixelUrl, CString sPath);
	static CString GetSystemHash();
	static BOOL appendToFile(CString sFilename, CString sData, BOOL isUTF8 = FALSE);
	static BOOL mergeFiles(CString sFilenamTo, CString sFileNameFrom);
	static CString GetVersionTxtVersion(CString userPath);
	static pair<CString, CString>  getBodySize(CString FileName);
	static CString userHash(CString name);
	static HANDLE GetProcessOwnerToken(DWORD pid);
	static CString escapeXML(CString str);
	static CString escapeJSON(CString str);
	static void CAlertUtils::RecursiveCopyKey(HKEY old_parent, HKEY new_parent, CString old_name, CString new_name);
	static void CAlertUtils::RecursiveCopyKey(HKEY parent, CString old_name, CString new_name);
	static void CAlertUtils::RecursiveDeleteKey(HKEY parent, CString name);
	static void RecursiveRenameKey(HKEY parent, CString old_name, CString new_name);
	static CRegKey parseRegPath(CString path, CString wow6432, REGSAM samDesired, BOOL create = FALSE);
	static CString QueryRegKey(CRegKey &key, const CString keyname, const CString format);
	static CString QueryRegValue(CRegKey &key, const CString name, const CString format);
	static CString GetRegistryKeyName();
	static CString GetProfileName();
	static CString databasePath(BOOL common=false);
    static CString __declspec(dllexport) crashLogFolderPath(BOOL common = false);
	static CString getDir(CString& param);
	static CString getAppDataPath(CString deskalertsId);
	static CString allUsersAppDataPath(BOOL env = FALSE);
	static CString allUsersAppDataProfilePath(BOOL env = FALSE);
	static CString stripHtmlTags(CString html);
	static CString regExReplace(CAtlRegExp<> *regEx, LPCTSTR szIn, LPCTSTR szVal, BOOL bAll);
	static CString findAlertWindowName();
	static CString findOptionsWindowName();
	static CRegKey getDesktopRegKey();
	static CRegKey getColorsRegKey();
	static CString getScreensaverPath(BOOL isShort = TRUE);
	static BOOL RunProcessAsUser(HANDLE primaryToken,CString processPath, CString arguments);
	static std::vector<HANDLE> getUsersTokensWithOpenedClients(CString name);
	static CString getDesktopName(HDESK hDesktop);
	static CString getWallpapersPath();
	static CString getLockscreensPath();
	static Gdiplus::Status imageToImage(IStream *pStreamIn, IStream *pStreamOut, CString wszOutputMimeType, int quality);
	static BOOL RecursiveDeleteDirectory(const TCHAR *path);
	static BOOL RecursiveMoveDirectory(const TCHAR *src, const TCHAR *dest);
	static bool IsIE6();

	// these methods are necessary to save screensavers to temporary storage
	static CString ExtractHref(CString url, CString searchMask = "http://");
	static CString ExtractFileName(CString url);
    static CString CutFileName(CString url);
	static CString ExtractFilePath(CString src, CString mask, char delimiter);
	static CString ReplaceHref(CString url, CString fileName);
	// Gets the computer physical IP from registry
	static CString GetCitrixSessionIp();
	//
	static HRESULT GetUserFromProcess(const DWORD procId,  _bstr_t& strUser, _bstr_t& strdomain);
	static BOOL GetLogonFromToken (HANDLE hToken, _bstr_t& strUser, _bstr_t& strdomain); 
	static DWORD sendHTTPRequest(CString szURL, bool skipHttpsCheck ,const TCHAR *szRequestType, const char * szRequestData, char *outBuffer, const DWORD bufferSize );
	static DWORD sendFileByHTTPRequest(CString url, CString szURL, bool skipHttpsCheck, const TCHAR *szRequestType, LPVOID szFileData, DWORD fileLen);
	static CString getTempFileName(CString prefix,int i);
	static CString getTempPath();
	static string readFile2String(const char* path);
    enum requestVerb {
        OLDcalcVerbByPostData = 0,
        GETverb = 1,
        POSTverb = 2
    };
	static BOOL CALLBACK Enum_FindMainWindow_windows_callback(HWND handle, LPARAM lParam);
	struct window_handle_processid {
		unsigned long process_id;
		HWND window_handle;
	};
	static BOOL is_main_window(HWND handle);
	static void PostMessageToWatchDog(DWORD Message, LPARAM lparam, WPARAM wparam);
	static void PostMessageToWatchDog(MSG Message);
};


