#include "stdafx.h"
#include "DocHostUIHandler.h"

CDocHostUIHandler::CDocHostUIHandler()
{
}

CDocHostUIHandler::~CDocHostUIHandler()
{
}

void CDocHostUIHandler::setDefaultHostUIHandler(CComPtr<IDocHostUIHandler> spDefaultDocHostUIHandler)
{
	m_spDefaultDocHostUIHandler = spDefaultDocHostUIHandler;
}

void CDocHostUIHandler::setDefaultOleCommandTarget(CComPtr<IOleCommandTarget> spDefaultOleCommandTarget)
{
	m_spDefaultOleCommandTarget = spDefaultOleCommandTarget;
}

// IDocHostUIHandler
STDMETHODIMP CDocHostUIHandler::ShowUI(DWORD dwID, IOleInPlaceActiveObject FAR* pActiveObject,
								 IOleCommandTarget FAR* pCommandTarget,
								 IOleInPlaceFrame  FAR* pFrame,
								 IOleInPlaceUIWindow FAR* pDoc)
{
	if (m_spDefaultDocHostUIHandler)
		return m_spDefaultDocHostUIHandler->ShowUI(dwID, pActiveObject, pCommandTarget, pFrame, pDoc);
	return E_NOTIMPL;
}

STDMETHODIMP CDocHostUIHandler::GetHostInfo(DOCHOSTUIINFO FAR *pInfo)
{
	if (m_spDefaultDocHostUIHandler)
		return m_spDefaultDocHostUIHandler->GetHostInfo(pInfo);
	return S_OK;
}

STDMETHODIMP CDocHostUIHandler::HideUI(void)
{
	if (m_spDefaultDocHostUIHandler)
		return m_spDefaultDocHostUIHandler->HideUI();
	return E_NOTIMPL;
}

STDMETHODIMP CDocHostUIHandler::UpdateUI(void)
{
	if (m_spDefaultDocHostUIHandler)
		return m_spDefaultDocHostUIHandler->UpdateUI();
	return E_NOTIMPL;
}

STDMETHODIMP CDocHostUIHandler::EnableModeless(BOOL fEnable)
{
	if (m_spDefaultDocHostUIHandler)
		return m_spDefaultDocHostUIHandler->EnableModeless(fEnable);
	return E_NOTIMPL;
}

STDMETHODIMP CDocHostUIHandler::OnDocWindowActivate(BOOL fActivate)
{
	if (m_spDefaultDocHostUIHandler)
		return m_spDefaultDocHostUIHandler->OnDocWindowActivate(fActivate);
	return E_NOTIMPL;
}

STDMETHODIMP CDocHostUIHandler::OnFrameWindowActivate(BOOL fActivate)
{
	if (m_spDefaultDocHostUIHandler)
		return m_spDefaultDocHostUIHandler->OnFrameWindowActivate(fActivate);
	return E_NOTIMPL;
}

STDMETHODIMP CDocHostUIHandler::ResizeBorder(LPCRECT prcBorder, IOleInPlaceUIWindow FAR* pUIWindow, BOOL fFrameWindow)
{
	if (m_spDefaultDocHostUIHandler)
		return m_spDefaultDocHostUIHandler->ResizeBorder(prcBorder, pUIWindow, fFrameWindow);
	return E_NOTIMPL;
}

STDMETHODIMP CDocHostUIHandler::GetOptionKeyPath(LPOLESTR FAR* pchKey, DWORD dw)
{
	if (m_spDefaultDocHostUIHandler)
		return m_spDefaultDocHostUIHandler->GetOptionKeyPath(pchKey, dw);
	return E_FAIL;
}

STDMETHODIMP CDocHostUIHandler::GetDropTarget(IDropTarget* pDropTarget, IDropTarget** ppDropTarget)
{
	if (m_spDefaultDocHostUIHandler)
		return m_spDefaultDocHostUIHandler->GetDropTarget(pDropTarget, ppDropTarget);
	return E_NOTIMPL;
}

STDMETHODIMP CDocHostUIHandler::TranslateUrl(DWORD dwTranslate, OLECHAR* pchURLIn, OLECHAR** ppchURLOut)
{
	if (m_spDefaultDocHostUIHandler)
		return m_spDefaultDocHostUIHandler->TranslateUrl(dwTranslate, pchURLIn, ppchURLOut);
	return E_NOTIMPL;
}

STDMETHODIMP CDocHostUIHandler::FilterDataObject(IDataObject* pDO, IDataObject** ppDORet)
{
	if (m_spDefaultDocHostUIHandler)
		return m_spDefaultDocHostUIHandler->FilterDataObject(pDO, ppDORet);
	return E_NOTIMPL;
}

STDMETHODIMP CDocHostUIHandler::GetExternal(IDispatch** ppDispatch)
{
	if (m_spDefaultDocHostUIHandler)
		return m_spDefaultDocHostUIHandler->GetExternal(ppDispatch);
	return E_NOTIMPL;
}

STDMETHODIMP CDocHostUIHandler::ShowContextMenu(DWORD dwID, POINT FAR* ppt, IUnknown FAR* pcmdTarget, IDispatch FAR* pdispReserved)
{
	if(m_spDefaultDocHostUIHandler)
		return m_spDefaultDocHostUIHandler->ShowContextMenu(dwID, ppt, pcmdTarget, pdispReserved);
	return S_OK;
}

STDMETHODIMP CDocHostUIHandler::TranslateAccelerator(LPMSG pMsg, const GUID FAR* pguidCmdGroup, DWORD nCmdID)
{
	if (m_spDefaultDocHostUIHandler)
	{
		return m_spDefaultDocHostUIHandler->TranslateAccelerator(pMsg, pguidCmdGroup, nCmdID);
	}
	return E_NOTIMPL;
}

STDMETHODIMP CDocHostUIHandler::Exec(
							   /*[in]*/ const GUID *pguidCmdGroup,
							   /*[in]*/ DWORD nCmdID,
							   /*[in]*/ DWORD nCmdExecOpt,
							   /*[in]*/ VARIANTARG *pvaIn,
							   /*[in,out]*/ VARIANTARG *pvaOut)
{

	if (nCmdID == OLECMDID_SHOWSCRIPTERROR)
	{
		if (!g_bDebugMode)
		{
			(*pvaOut).vt = VT_BOOL;
			(*pvaOut).boolVal = VARIANT_TRUE;
			return S_OK;
		}
		else
		{
			if (m_spDefaultOleCommandTarget&&m_spDefaultOleCommandTarget!= (IOleCommandTarget*)this)
				return m_spDefaultOleCommandTarget->Exec(pguidCmdGroup, nCmdID, nCmdExecOpt, pvaIn, pvaOut);
		}
	}
	return E_NOTIMPL;
}


STDMETHODIMP CDocHostUIHandler::QueryStatus(
									  /* [unique][in] */ const GUID *pguidCmdGroup,
									  /* [in] */ ULONG cCmds,
									  /* [out][in][size_is] */ OLECMD prgCmds[  ],
									  /* [unique][out][in] */ OLECMDTEXT *pCmdText)
{	
	if (m_spDefaultOleCommandTarget)
		return m_spDefaultOleCommandTarget->QueryStatus(pguidCmdGroup, cCmds, prgCmds, pCmdText);
	return E_NOTIMPL;
}


STDMETHODIMP CDocHostUIHandler::ShowMessage(HWND /*hwnd*/, LPOLESTR /*lpstrText*/, LPOLESTR /*lpstrCaption*/, DWORD /*dwType*/, LPOLESTR /*lpstrHelpFile*/, DWORD /*dwHelpContext*/, LRESULT* /*plResult*/)
{
	return S_FALSE;
}

STDMETHODIMP CDocHostUIHandler::ShowHelp(HWND /*hwnd*/, LPOLESTR /*pszHelpFile*/, UINT /*uCommand*/, DWORD /*dwData*/, POINT /*ptMouse*/, IDispatch* /*pDispatchObjectHit*/)
{
	return S_FALSE;
}
