#pragma once
#include "ServiceDataManager.h"

class RequestDataManager : public ServiceDataManager
{
private:
	ServiceDataManager * m_dataManager;
	unsigned int m_packet_count, m_last_packet_size;
	static bool structHash(void *data, unsigned length, unsigned *res);
public:
	RequestDataManager(ServiceDataManager * dataManager);
	~RequestDataManager(void);
	DWORD serialize(IStream* pStream);
	RequestDataType getDataType();
	unsigned int getPacketsCount();
	PPACKET getPacket(void *data, unsigned int id);
	static bool checkStructHash(void *data, unsigned length, unsigned *hash);
};
