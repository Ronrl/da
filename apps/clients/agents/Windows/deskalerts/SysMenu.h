#pragma once

#include "IInclude.h"
#include "OptionConf.h"
//#include "MyMenu.h"

//#define CUSTOM_DRAW was fixed not custom draw

class SysMenuEvent: public IActionEvent
{
	CString eventName;
	CString sourceName;
	CString commandName;

public:
	SysMenuEvent(CString _eventName,CString _sourceName,CString _commandName):
	  eventName(_eventName),sourceName(_sourceName),commandName(_commandName)
	{
	}
	~SysMenuEvent()
	{
	}

public:
	CString& getCommandName()
	{
		return commandName;
	}
	CString& getEventName()
	{
		return eventName;
	}
	CString& getSourceName()
	{
		return sourceName;
	}
};

#define MIT_ICON	 1
#define MIT_COLOR	 2
#define MIT_XP		 3

#define COLOR_TEXT_BKGND GetSysColor(COLOR_MENU)


class CMenuItem
{
public:
	CMenuItem(CString strText, UINT uid, CBitmap* pBitmap = NULL, HICON hIcon = NULL) : m_szText(strText), m_pBitmap(pBitmap), m_hIcon(hIcon), m_ID(uid),
		m_nWidth(100), m_nHeight(20), m_iImage(-1), m_iPressedImage(-1), m_pPressedBitmap(nullptr)
	{
	}
public:
	CString		m_szText;			// message text
	CString		m_szPressedText;	// message text

	CString		m_pszHotkey;		// hotkey text
	CBitmap*	m_pBitmap,  *m_pPressedBitmap;

	HICON		m_hIcon;
	int			m_nWidth, m_nHeight;
	UINT		m_ID;
	CString		m_sCommand;
	int			m_iImage, m_iPressedImage;

};


class CSysMenu : public IView,public CWindowImpl<CSysMenu>
{
	enum { UWM_NOTIFYICON = (WM_APP + 200)};

private:
	HWND parentHWND;
	CIcon sysIcon;

	//CTypedPtrList<CPtrList,CMenuItem *> m_MenuList;
public:
	CSysMenu(NodeALERTS::NodeVIEWS::NodeVIEW* mStrc);
	~CSysMenu(void);
	std::vector<CMenuItem*> m_mItems;
public:
	DECLARE_WND_CLASS(_T("SysMenu"))

	BEGIN_MSG_MAP(CSysMenu)
		MESSAGE_HANDLER(WM_MEASUREITEM, OnMeasureItem)
		MESSAGE_HANDLER(WM_CREATE, OnCreate)
		MESSAGE_HANDLER(WM_DESTROY, OnDestroy)
		MESSAGE_HANDLER(WM_COMMAND, OnCommand)
		MESSAGE_HANDLER(UWM_NOTIFYICON, OnNotifyIcon)
		MESSAGE_HANDLER(WM_DRAWITEM, OnDrawItem)
	END_MSG_MAP()

	LRESULT OnCreate	(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT OnDestroy	(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT OnCommand	(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT OnDrawItem	(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT OnMeasureItem(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);

	LRESULT OnNotifyIcon(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);

	WTL::CMenu& createMenu( WTL::CMenu& popupMenu);

private:
	void showMenu();

	WTL::CMenu& createMenu(WTL::CMenu& menu,NodeALERTS::NodeVIEWS::NodeMENU* item,int& menuID,HWND hwnd);
	WTL::CMenu& destroyMenu(WTL::CMenu& menu);

	UINT        m_nType;
public:

	void ChangeIcon(int iMode, CString *sCaption = NULL, int iType = NIM_MODIFY);
	bool show(CString param, shared_ptr<showData> ex, BOOL isNewAlert = FALSE);
	void hide(CString param);
	void refreshIcon(int stateIndex = _iAlertModel->getStatesIndex());

	//static BOOL CSysMenu::AppendMenu(UINT nFlags,UINT nIDNewItem,LPCWSTR lpszNewItem,
	//					 HBITMAP hBitmap=NULL,HICON hIcon=NULL);
public:

	NodeALERTS::NodeVIEWS::NodeMENU* m_menuStruct;
	WTL::CMenu m_cMenu;

protected:
	void MeasureItem(LPMEASUREITEMSTRUCT lpMIS);
	void DrawItem(LPDRAWITEMSTRUCT lpDIS);
	void DrawXPMenu(LPDRAWITEMSTRUCT lpDIS);
	void DrawCheckMark(CDC* pDC,int x,int y,COLORREF color);
};
