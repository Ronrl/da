#pragma once

#include "IInclude.h"

#include "AlertView.h"
#include "AlertModel.h"
#include "ServiceBridge.h"
#include <map>
#include "SwitchDesktopHandler.h"
#include "AlertsJAClient.h"

#define MYWM_KILLME_PLEASE_I_DONT_WONT_LIVE (WM_APP+101)


class CCommand : public IMVCCommand
{
	NodeALERTS::NodeCOMMANDS::NodeCOMMAND m_cmd;
public:
	CCommand(NodeALERTS::NodeCOMMANDS::NodeCOMMAND *cmd);
	virtual void stop();
	virtual BOOL isCanStart();
	virtual BOOL isAutoStart();
};

class COpenWindowCommand : public CCommand
{
public:
	COpenWindowCommand(NodeALERTS::NodeCOMMANDS::NodeOPENWINDOW* param);
	~COpenWindowCommand(void);

public:
	virtual CString getName();
	virtual void run(IActionEvent& event);
protected:
	NodeALERTS::NodeCOMMANDS::NodeOPENWINDOW winData;
};

class CExit : public CCommand
{
public:
	CExit(NodeALERTS::NodeCOMMANDS::NodeEXIT *param);
	CString getName();
	void run(IActionEvent& event);
};

class CUninstall : public CCommand
{
public:
	CUninstall(NodeALERTS::NodeCOMMANDS::NodeUNINSTALL *param);
	CString getName();
	void run(IActionEvent& event);
};

class CUpdate :public CCommand
{
public:
	CUpdate(NodeALERTS::NodeCOMMANDS::NodeUPDATE *param);
	CString getName();
	void run(IActionEvent& event);
};

class CDisable :public CCommand
{
	NodeALERTS::NodeCOMMANDS::NodeDISABLE param;
public:
	CDisable(NodeALERTS::NodeCOMMANDS::NodeDISABLE* _param);
	CString getName();
	void run(IActionEvent& event);
};

class CUnread :public CCommand
{
	NodeALERTS::NodeCOMMANDS::NodeUNREAD param;
public:
	CUnread(NodeALERTS::NodeCOMMANDS::NodeUNREAD* _param);
	CString getName();
	void run(IActionEvent& event);
};

class CSysMenuCommand :public CCommand
{
	NodeALERTS::NodeCOMMANDS::NodeSYSMENU param;
public:
	CSysMenuCommand(NodeALERTS::NodeCOMMANDS::NodeSYSMENU* _param);
	CString getName();
	void run(IActionEvent& event);
};



class CStandBy :public CCommand
{
	NodeALERTS::NodeCOMMANDS::NodeSTANDBY param;
public:
	CStandBy(NodeALERTS::NodeCOMMANDS::NodeSTANDBY* _param);
	~CStandBy();
	CString getName();
	void run(IActionEvent& event);
private:
	UINT_PTR m_requestTimer;
	static volatile LONG m_expire;
	static VOID CALLBACK TimerProc(HWND hwnd, UINT uMsg, UINT_PTR idEvent, DWORD dwTime);
	static CString getStandbyUrl(BOOL enabled, int nextrequest);
};

class CBrowserJump :public CCommand
{
	NodeALERTS::NodeCOMMANDS::NodeBROWSERJAMP param;
public:
	CBrowserJump(NodeALERTS::NodeCOMMANDS::NodeBROWSERJAMP* _param);
	CString getName();
	void run(IActionEvent& event);
};


class CMiniBrowserJamp :public COpenWindowCommand
{
public:
	CMiniBrowserJamp(NodeALERTS::NodeCOMMANDS::NodeMINIBROWSERJAMP* param) : COpenWindowCommand(param){}
};


class COption :public COpenWindowCommand
{
public:
	COption(NodeALERTS::NodeCOMMANDS::NodeOPTION* param) : COpenWindowCommand(param){}
};

class CHistory :public COpenWindowCommand
{
public:
	CHistory(NodeALERTS::NodeCOMMANDS::NodeHISTORY* param) : COpenWindowCommand(param){}
	CString getName();
	void run(IActionEvent& event);
};

class CRunApp: public CCommand
{
	NodeALERTS::NodeCOMMANDS::NodeRUNAPP param;
public:
	CRunApp(NodeALERTS::NodeCOMMANDS::NodeRUNAPP* _param);
	CString getName();
	void run(IActionEvent& event);
};


class CCommandFactory
{
public:
	static IMVCCommand* create(NodeALERTS::NodeCOMMANDS::NodeCOMMAND* param);
};


//----------------------------------------------------------------------------------------------
class RegistryMonitor;
class CLockscreenManager;

class CAlertController: public IAlertController
{
private:
	HWND mainHWND;
	HWND m_sysMenuHWND;
	static HWND mainFrameHWND;
	ServiceBridge *m_serviceBridge;
	CScreensaverManager *m_screensaverManager;
	CSwitchDesktopHandler *m_switchDesktopHandler;
	CWallpaperManager *m_wallpaperManager;
	CLockscreenManager *m_lockscreenManager;
	CJAClient *m_JAClient;
	CJAClient_v2* m_JAClient_v2;
	int m_initFlags;
	bool isEnableAPIv2;
public:
	CAlertController(void);
	~CAlertController(void);

protected:

public:
	void runAutoStart();
	void restoreNotClosedAlerts();
	void loadAppCredentials();

	//IComponent
	void createComponent();
	void destroyComponent();
	//IActionListener
 	void actionPerformed(IActionEvent& event);
	//IAlertListener
	void alertUpdated(IActionEvent& event);
	//IModelListener
 	void alertModelUpdated(IModelEvent& event);
	//IPropertyListener
	void propertyChanged(IPropertyChangeEvent& propCh);

	static CString m_sAppDataPath;
	CSwitchDesktopHandler* getSwitchDesktopHandler();
	ServiceBridge* getServiceBridge();
	CWallpaperManager* getWallpaperManager();
	CLockscreenManager* getLockscreenManager();
	CScreensaverManager* getScreensaverManager();
	CJAClient    *getJAClient();
	CJAClient_v2 *getJAClient_v2();
	void setInitFlags(int initFlags);
	int getInitFlags();
private:
	//CString getAppDataPath();
	//static CString getDir(CString& param);
	
	void setDefaultParams();
	POINT lastScreenMousePt;
	BOOL m_bComponentCreated;
public:
	void setMainHWND(HWND& _mainHWND);
	HWND& getMainHWND();
	// This is necessary to remove icon on application restart
	HWND getSysMenuHWND()
	{
		return m_sysMenuHWND;
	}
	//
	static DWORD WINAPI screensaverDesktopWindow(LPVOID pv);
	void refreshState();
	std::multimap<CString,IMVCCommand*> commandMap;
	CAlertView cAlertView;
	IAlertModel *cAlertModel;
	int m_iMode;
	void SwitchAPI() 
	{
		isEnableAPIv2 = !isEnableAPIv2;
	}
	bool IsEnableAPIv2() 
	{
		return isEnableAPIv2;
	}
};