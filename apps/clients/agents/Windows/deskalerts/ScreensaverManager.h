#pragma once

class RegistryMonitor;

class CScreensaverManager
{
public:
	CScreensaverManager(void);
	~CScreensaverManager(void);
	void enableScreensaver();
	void disableScreensaver();
	void start();
private:
	RegistryMonitor *m_registryMonitor;
	static int registryMonitorCallback(LPVOID pv);
};
