//
//  DatabaseManagr.h
//  DeskAlerts Client
//  Common module
//
//  Copyright Softomate 2006-2009. All rights reserved.
//


#pragma once

class NodeALERT;
class CWallpaperAlert;
class CLockscreenAlert;
struct showData;

#define WM_CALLBACK_INVOKE (WM_APP+111)

class CDatabaseManager
{
public:
	CDatabaseManager(BOOL common=false);
	~CDatabaseManager();

private:
	CString m_path;
	sqlite3 *m_database;
	int openDb();
	void closeDb();
	void OpenDatabase(CString sPath);
	void CloseDatabase();
	BOOL IsTableExists(CString table_name);
	BOOL InitDatabase();;
	void DropTables();
	BOOL CreateTables();
	BOOL IsColumnExists(CString column_name, CString table_name);
	BOOL AddColumnIfNotExists(CString column_name, CString table_name, CString type, CString def = CString());
	void UpdateTables();
	void getAlertData(sqlite3_stmt* stmt, const int i, CString &window, shared_ptr<showData> &alert);
	CComAutoCriticalSection m_cs_db; //define critical section
	struct HistoryRangeStruct
	{
		CString _sDate;
		CString sFormat;
		sqlite_int64 startPos;
		sqlite_int64 count;
		CString searchTerm;
		ULONG callbackId;
		HWND callbackWindow;
        HistoryRangeStruct():startPos(NULL), count(NULL){}
	};

	struct ThreadStruct
	{
		CString functionName;
		void *data;
		DWORD threadId;
		CDatabaseManager *m_db;
        ThreadStruct(): m_db(nullptr) {}
	};
	static DWORD WINAPI runInThread(LPVOID pv);
	HRESULT getHistoryRangeFromDb(CString _sDate,CString *sResponse, CString sFormat, sqlite_int64 startPos, sqlite_int64 count, CString searchTerm);

public:
    struct ReceivedAlert {
        _int64 ID;
        CString body;

        ReceivedAlert(const _int64 _id, const CString _body) : ID(_id), body(_body) {}
        ReceivedAlert() :ID(0) {}
    };
    struct DownloadedAlert {
        _int64 ID;
        CStringA Content;

        DownloadedAlert(const _int64 _id, const CStringA _Content) : ID(_id), Content(_Content) {}
        DownloadedAlert() :ID(0) {}
    };
	
    HRESULT GetFormatedDateTime(CString _sDate, CString& sResponse, CString sFormat, BOOL UTC);
	HRESULT GetHistory(CString sDate,CString& sResponce, CString sFormat);
	HRESULT GetHistoryRange(CString _sDate, CString *sResponse, CString sFormat, sqlite_int64 startPos, sqlite_int64 count, CString searchTerm = CString(),BOOL async = FALSE, ULONG callbackId = -1, HWND callbackWindow = NULL);
	HRESULT AddToHistory(shared_ptr<NodeALERT> alertData, CString window, sqlite_int64 *history_id, BOOL *isCreated);
	HRESULT PurgeHistory(long unsigned int iDate);
	HRESULT PurgeHistoryAtTime(unsigned long int iDate, CString Time);
	HRESULT DeleteAlert(sqlite_int64 id);
	HRESULT DeleteAlertByID(sqlite_int64 alertid);
	HRESULT DeleteHistory();
    HRESULT setAlertShowed(sqlite_int64 alertid);
    HRESULT setReceived(sqlite_int64 alertid, CString body);
    HRESULT setDownloaded(sqlite_int64 alertid, CString content);
    CString getDownloaded(sqlite_int64 alertid);
    HRESULT cleanDownloaded(sqlite_int64 alertid);
	void setProperty(CString name, const CString value);
	bool hasProperty(CString name);
	CString getProperty(CString name);
	HRESULT loadProperty(map<CString,CString> &prop);
	void markAlertAsClosed(sqlite_int64 history_id);
	void markAlertAsTerminated(sqlite_int64 alert_id);
	void markAlertAsClosedByIdAndUser(sqlite_int64 alert_id, sqlite_int64 user_id);
	void getNotClosedAlerts(multimap<CString, shared_ptr<showData>> &alerts);
    vector<ReceivedAlert> getVectorReceivedNotShowedAlerts();
	void getAlertFromHistory(sqlite_int64 id, CString &window, shared_ptr<showData> alert);
    bool isAlertReceived(sqlite_int64 id);
    bool isAlertDownloaded(sqlite_int64 id);
    bool isAlertInHistory(sqlite_int64 Alert_id, sqlite_int64 *history_id);
	void getWallpaperAlerts(std::vector<CWallpaperAlert*> &alerts);
	void synchronizeAlerts(std::vector<NodeALERT*> *alerts, CString alertClass);
	void getLockscreenAlerts(std::vector<CLockscreenAlert*> &alerts);

	bool synchronizeGroups(vector<wstring> groups, vector<wstring> &add, vector<wstring> &remove, bool needUpdate);
	sqlite_int64 getUnreadCount();
	static CDatabaseManager* shared(bool create, BOOL common = FALSE);
	void markAlertAsRead(sqlite_int64 history_id);

	//
	// the methods below are necessary to implement alerts termination
	//
	void AddToAlertId_hWnds(sqlite_int64 alertId, sqlite_int64 hWndTools, sqlite_int64 hWndBody);
	void DeleteFromAlertId_hWnd(sqlite_int64 alertId);
	void GethWndByAlertId(sqlite_int64 alertId, sqlite_int64* hWndTools, sqlite_int64* hWndBody);
	bool CheckIfTicker(sqlite_int64 alertId);
	//

};
