#pragma once

#define UTM_STOP_HANDLER (WM_APP+501)
#define UTM_SWITCH_DESKTOP (WM_APP+502)

class ISwitchDesktopListener
{
public:
	virtual LRESULT OnSwitchDesktop(CString *closedDesktopName, CString *openedDesktopName) = 0;
};

class CSwitchDesktopHandler :
	public CFrameWindowImpl<CSwitchDesktopHandler>
{
public:
	CSwitchDesktopHandler(void);
	~CSwitchDesktopHandler(void);
	BEGIN_MSG_MAP(CSwitchDesktopHandler)
		MESSAGE_HANDLER(UTM_SWITCH_DESKTOP, OnSwitchDesktop)
	END_MSG_MAP()

	void addListener(ISwitchDesktopListener* listener);
	void removeListener(ISwitchDesktopListener* listener);

	LRESULT OnSwitchDesktop(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	void start();
	void stop();

private:
	CComAutoCriticalSection m_cs;
	vector<ISwitchDesktopListener*> m_listeners;
	HANDLE m_thread;
	DWORD m_threadId;
	static DWORD WINAPI handlerThread(LPVOID pv);
};
