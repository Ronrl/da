#include "stdafx.h"
#include "ShowWindowDataManager.h"

ShowWindowDataManager::ShowWindowDataManager(void):m_windowData(NULL)
{
}

ShowWindowDataManager::~ShowWindowDataManager(void)
{
}

void ShowWindowDataManager::setData(CString viewName, CString param, shared_ptr<showData> windowData)
{
	m_windowData = windowData;
	m_param = param;
	m_viewName = viewName;
}

RequestDataType ShowWindowDataManager::getDataType()
{
	return REQUESTDATATYPE_WINDOW;
}

#define WriteData(x) pStream->Write(m_windowData->x, (m_windowData->x.GetLength()+1)*sizeof(WCHAR), NULL);

DWORD ShowWindowDataManager::serialize(IStream* pStream)
{
	WINDOWDATA windowData;
	windowData.pid = GetCurrentProcessId();
	windowData.encoding = m_windowData->encoding;
	windowData.UTC = m_windowData->UTC;
	windowData.utf8 = m_windowData->utf8;
	windowData.history_id = m_windowData->history_id;
	windowData.windowsCountDelta = m_windowData->windowsCountDelta;
	windowData.windowRect = m_windowData->windowRect;
	pStream->Write(&windowData, sizeof(WINDOWDATA), NULL);
	pStream->Write(m_param, (m_param.GetLength()+1)*sizeof(WCHAR), NULL);
	pStream->Write(m_viewName, (m_viewName.GetLength()+1)*sizeof(WCHAR), NULL);
	WriteData(url);
	WriteData(post);
	WriteData(expire);
	WriteData(acknowledgement);
	WriteData(create_date);
	WriteData(title);
	WriteData(width);
	WriteData(height);
	WriteData(position);
	WriteData(autoclose);
	WriteData(topTemplateHTML);
	WriteData(bottomTemplateHTML);
	WriteData(visible);
	WriteData(cdata);
	WriteData(ticker);
	WriteData(userid);
	WriteData(alertid);
	WriteData(survey);
	WriteData(schedule);
	WriteData(recurrence);
	WriteData(urgent);
	WriteData(searchTerm);
	WriteData(desktopName);
	
	return 0;
}