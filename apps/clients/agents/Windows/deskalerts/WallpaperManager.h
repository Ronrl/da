#pragma once
class CWallpaperAlert;

#define _WALLPAPERS_TIMER_	102

class CWallpaperManager : public CFrameWindowImpl<CWallpaperManager>
{
public:
	enum { UWM_STOP_THREAD = (WM_APP + 310),
		UTM_SET_WALLPAPER = (WM_APP + 311)
	};
	CWallpaperManager(void);
	~CWallpaperManager(void);	

	BEGIN_MSG_MAP(CWallpaperManager)
		MESSAGE_HANDLER(WM_TIMER, OnTimer)
	END_MSG_MAP()

	LRESULT OnTimer(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	void showWallpapers();
private:
	struct WallpaperStruct{
		CString position; 
		CString fileName; 
		BOOL absolutePath;
        WallpaperStruct():absolutePath(NULL){}
	};

	void setWallpaper(CString position, CString fileName, BOOL absolutePath = FALSE);
	void restoreUserWallpaper();
	static DWORD WINAPI WallpaperThread(LPVOID pv);
	std::vector<CWallpaperAlert*>::iterator m_curIt;
	std::vector<CWallpaperAlert*> m_alerts;
	HANDLE m_thread;
	DWORD m_threadId;
};
