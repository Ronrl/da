//
//  stdafx.cpp
//  DeskAlerts Client
//  Common module
//
//  Copyright Softomate 2006-2009. All rights reserved.
//

#include "stdafx.h"
#include <pdh.h>
#include <time.h>

volatile LONG g_bDebugMode = FALSE;

inline DWORD WaitForSingleObjectWithMsgLoop(__in HANDLE hHandle, __in DWORD dwMilliseconds)
{
	DWORD dwStatus = 0;
	// Wait for an event to occur
	do
	{
		time_t t;
		time(&t);
		dwStatus = MsgWaitForMultipleObjects(1, &hHandle, FALSE, dwMilliseconds, QS_ALLINPUT);
		if(dwStatus == WAIT_OBJECT_0+1) //on message
		{
			if(dwMilliseconds != INFINITE)
			{
				time_t nt;
				time(&nt);
				if(dwMilliseconds < nt - t)
					dwStatus = WAIT_TIMEOUT;
				else
					dwMilliseconds -= (DWORD)(nt - t);
			}
			release_try
			{
				MSG msg;
				while(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) //no need to check > 0, see MSDN
				{
					release_try
					{
						if(msg.message == WM_QUIT)
						{
							dwStatus = WAIT_ABANDONED;
							break; // abandoned due to WM_QUIT
						}
						else
						{
							TranslateMessage(&msg);
							DispatchMessage(&msg);
						}
					}
					release_catch_tolog(_T("Wait msg loop exception"))
				}
			}
			release_catch_tolog(_T("Wait msg peek exception"))
		}
	}
	while(dwStatus == WAIT_OBJECT_0+1); //continue to wait
	return dwStatus;
}
