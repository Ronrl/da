#include "StdAfx.h"
#include "alertview.h"
#include "HTMLView.h"
#include "OptionWindowDecorator.h"
#include "OptionConf.h"
#include "WindowCommand.h"
#include "DesktopMultiWindow.h"
#include <time.h>


IView* CViewFactory::create(NodeALERTS::NodeVIEWS::NodeVIEW* param)
{
	if (param->m_tagName==_T("SYSMENU"))
	{
			CSysMenu* pMenu = new CSysMenu(param);
			shared_ptr<showData> ex(new showData());
			pMenu->show(CString(), ex);
			return pMenu;
	}
	if (param->m_tagName==_T("WINDOW"))
	{
		//return new CMultiWindowCommand((NodeALERTS::NodeVIEWS::NodeWINDOW*)param);
		return new CDesktopMultiWindow((NodeALERTS::NodeVIEWS::NodeWINDOW*)param);

	}

	return NULL;
}

//-------------------------------------------------------------------------------------------------------
CAlertView::CAlertView(void) : m_sysMenuHWND (NULL)
{
}

CAlertView::~CAlertView(void)
{
}


void CAlertView::createComponent()
{
	release_try
	{
		NodeALERTS& nodeALERTS = _iAlertModel->getModel(CString(_T("views")));
		if(nodeALERTS.m_views)
		{
			vector<NodeALERTS::NodeVIEWS::NodeVIEW*> vView = nodeALERTS.m_views->m_view;
			vector<NodeALERTS::NodeVIEWS::NodeVIEW*>::iterator it = vView.begin();
			while(it != vView.end())
			{
				if (!((_iAlertController->getInitFlags() & NO_MENU) && (*it)->m_name == _T("sysmenu")))
				{
					IView* iViw = CViewFactory::create(*it);
					if(iViw)
					{
						//appendToErrorFile(L"Creating view: %s", (*it)->m_name);
						viewMap.insert(pair<CString,IView*>((*it)->m_name, iViw));
						// This is necessary to remove icon on application restart
						if ((*it)->m_name == _T("sysmenu"))
							m_sysMenuHWND = ((CSysMenu*)iViw)->m_hWnd;
						//
					}
				}
				it++;
			}
		}
	}
	release_catch_tolog(L"Creating AlertView Exception");
}

void CAlertView::destroyComponent()
{
	//appendToErrorFile(L"Lets destroy AlertView");
	std::map<CString,IView*>::iterator it = viewMap.begin();
	while(it != viewMap.end())
	{
		release_try
		{
			it->second->hide(CString());
		}
		release_catch_tolog(L"AlertView destroy exception");
		it++;
	}
	//appendToErrorFile(L"AlertView has been destroyed");
}


void CAlertView::closeOptions()
{
	//appendToErrorFile(L"Lets close options view");
	std::map<CString,IView*>::iterator it = viewMap.begin();
	while(it != viewMap.end())
	{
		release_try
		{
			if( it->first.Find(_T("option"))>-1)
				it->second->hide(CString());
		}
		release_catch_tolog(L"AlertView close options view exception");
		it++;
	}
	//appendToErrorFile(L"AlertView has been destroyed");
}

//IAlertListener
void CAlertView::alertUpdated(IActionEvent& /*event*/)
{
}

//IModelListener
void CAlertView::alertModelUpdated(IModelEvent& /*event*/)
{
}

//CAlertView
void CAlertView::showViewByName(CString& viewName,CString& param, shared_ptr<showData> ex, BOOL isNewAlert)
{
    bool resShow = showViewByName2(viewName, param, ex, isNewAlert);
    //DeskAlertsAssert(resShow);
}

bool CAlertView::showViewByName2(CString& viewName,CString& param, shared_ptr<showData> ex, BOOL isNewAlert)
{
	bool result = true;
	std::map<CString,IView*>::const_iterator it = viewMap.find(viewName);

    //DeskAlertsAssert(it != viewMap.end());
	
    if(it!=viewMap.end())
	{
		it->second->data = ex;
		it->second->param = param;
		it->second->show(param, ex, isNewAlert);
	}
	else
	{
		result = false;
        appendToErrorFile_TMP(_T("showViewByName: Can not found view: %s, %s, %s "), viewName, ex->alertid, ex->Class);
	}
	return result;
}

IView* CAlertView::getViewByName(CString& viewName)
{
	std::map<CString,IView*>::const_iterator it = viewMap.find(viewName);
	if(it!=viewMap.end())
	{
		return (it->second);
	}
	else
	{
		//appendToErrorFile(_T("getViewByName: Can not found view: %s"), viewName);
		return NULL;
	}
}

void CAlertView::closeExpiredAlerts()
{
	std::map<CString,IView*>::iterator it = viewMap.begin();

		time_t nowTime;
		struct tm  timeinfo;
		char buffer[80];

		 time (&nowTime);
		 localtime_s(&timeinfo, &nowTime);

		// timeinfo = localtime(&rawtime);


		strftime(buffer,80,"%Y-%m-%d %I:%M:%S", &timeinfo);
		//appendToErrorFile(_T("EXPIRED_ALERTS_CHECK\n"));
		std::string now_date(buffer);	

	while(it != viewMap.end())
	{
		shared_ptr<showData> data = it->second->data;
		
		//appendToErrorFile(_T("WINDOW NAME: "));
		//appendToErrorFile(it->first);
		if(!data)
		{
			it++;
			continue;
		}

		CT2A ascii(data->to_date, CP_UTF8);
		std::string expired_date(ascii.m_psz);

		time_t exprTime = (const time_t)atol(expired_date.c_str());


		CString nowTimeStr;
		nowTimeStr.Format(_T("%I64d"), nowTime);

		CString exprTimeStr;
		exprTimeStr.Format(_T("%I64d"), exprTime);


		//appendToErrorFile(_T("NOW: "));
		//CA2T(std::to_string(nowtime))
		//appendToErrorFile( nowTimeStr );
		//appendToErrorFile(_T("EXPIRED DATE: "));
		//appendToErrorFile( exprTimeStr );
		if( exprTime < nowTime )
		{
			//appendToErrorFile(_T("CLOSE IT!"));
			it->second->hide(it->second->param);
		}

		it++;
	}
}

void CAlertView::propertyChanged(IPropertyChangeEvent& /*propCh*/)
{
}

