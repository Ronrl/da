#pragma once
#include "servicedatamanager.h"

class CloseWindowDataManager :
	public ServiceDataManager
{
private:
	INT64 m_alertId;
public:
	CloseWindowDataManager(void);
	~CloseWindowDataManager(void);
	DWORD serialize(IStream* pStream);
	void setData(INT64 alertId);
	RequestDataType getDataType();
};
