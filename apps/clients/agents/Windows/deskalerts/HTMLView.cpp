#include "StdAfx.h"
#include "htmlview.h"
#include "CustomInternetSecurityImpl.h"
#include <shellapi.h>

#include "AlertAPP.h"
#include "ProtocolCF.h"
#include <wininet.h>
#include "DocHostUIHandler.h"
#include "BrowserUtils.h"

typedef PassthroughAPP::CMetaFactory<PassthroughAPP::CAlertSinkClassFactoryProtocol,CAlertAPP> MetaFactory;

CHTMLView::CHTMLView(void): m_browserInitialized(FALSE), m_cMyPageHook(NULL), m_destroyEvent(NULL)
{
	CComObject<CEventHandler>::CreateInstance(&m_pHandler);
	(*m_pHandler).AddRef();
	m_pHandler->addMyDispatchListener(this);
}

CHTMLView::~CHTMLView(void)
{
	if (m_cMyPageHook) delete_catch(m_cMyPageHook);
}

void CHTMLView::AddDocCompleteListener(IDocCompleteListener *iNewListener)
{
	//don't add listener twice
	std::vector<IDocCompleteListener*>::iterator it = vDocCompleteListeners.begin();
	while(it!=vDocCompleteListeners.end())
	{
		if ((*it)==iNewListener)
		{
			return;
		}
		++it;
	}
	vDocCompleteListeners.push_back(iNewListener);
}


void CHTMLView::DeleteDocCompleteListener(IDocCompleteListener *iListener)
{
	std::vector<IDocCompleteListener*>::iterator it = vDocCompleteListeners.begin();
	while(it!=vDocCompleteListeners.end())
	{
		if ((*it)==iListener)
		{
			vDocCompleteListeners.erase(it);
			break;
		}
		++it;
	}
}
/************************************************************************/
/* HTML Events                                                          */
/************************************************************************/
STDMETHODIMP CHTMLView::OnDownloadComplete()
{
	detach();
	attach();
	return S_OK;
}

STDMETHODIMP CHTMLView::OnDocumentComplete(IDispatch *pDisp, VARIANT* URL)
{
	if(m_spBrowser.IsEqualObject(pDisp))
	{
		if(m_cMyPageHook==NULL)
		{
			HWND wnd = ::GetWindow(m_hWnd, GW_CHILD);
			TCHAR ClassName[200] = {0};
			while(wnd)
			{
				::GetClassName(wnd, ClassName, 200);
				if(_tcsicmp(ClassName, _T("Internet Explorer_Server")) == 0)
					break;
				wnd = ::GetWindow(wnd, GW_CHILD);
			}
			if(wnd) 
			{
				m_cMyPageHook = new CMyPageHook(wnd, (IWinMessageFilter*)this);
			}
		}
		CString url = BrowserUtils::variantToString(*URL);
		if(m_browserInitialized || url != _T("about:blank")) //skip document complete from first about:blank
		{
			std::vector<IDocCompleteListener*>::iterator it = vDocCompleteListeners.begin();
			while(it!=vDocCompleteListeners.end())
			{
				(*it)->OnDocComplete(this);
				++it;
			}
		}
		m_browserInitialized = TRUE;
		SetEvent(m_destroyEvent);
		
			/*errno_t err = 0; //uncomment to swith on deleting temp files after load
			CString fileurl = url;
			fileurl.Replace(_T("file:///"),_T(""));
			const size_t newsizea = (fileurl.GetLength() + 1);
			char * filetoopen =  new char[newsizea];
			err = strcpy_s(filetoopen, newsizea,CT2A(fileurl));
			if (err==0) 
			{
				FILE *file;
				err = fopen_s(&file, filetoopen, "r");
				if (err==0) 
				{
					TCHAR szFilePath[MAX_PATH + _ATL_QUOTES_SPACE];
					DWORD dwFLen = 0;
					dwFLen = ::GetModuleFileName(NULL, szFilePath + 1, MAX_PATH);
					szFilePath[0] = _T('\"');
					szFilePath[dwFLen + 1] = _T('\"');
					szFilePath[dwFLen + 2] = 0;
					CString rootPath = szFilePath;
					rootPath.Replace(_T("\\DeskAlerts.exe"),_T(""));
					rootPath.Replace(_T("\""),_T(""));
					fclose(file);
					fileurl.Replace(_T("/"),_T("\\"));
					fileurl.MakeUpper();
					rootPath.MakeUpper();
					if (fileurl.Find(rootPath)<0)
					{
						remove(filetoopen);
					}
				}
			}*/
		}
	
	
	return S_OK;
}


STDMETHODIMP CHTMLView::OnNavigateError(IDispatch *pDisp, VARIANT *URL, VARIANT *TargetFrameName, VARIANT *StatusCode, VARIANT_BOOL *Cancel)
{
	if(m_spBrowser.IsEqualObject(pDisp))
	{
		CString url = BrowserUtils::variantToString(*URL);
		CString name = BrowserUtils::variantToString(*TargetFrameName);
		LONG status = (LONG)BrowserUtils::variantToInt64(*StatusCode);
		std::vector<IDocCompleteListener*>::iterator it = vDocCompleteListeners.begin();
		while(it!=vDocCompleteListeners.end())
		{
			if((*it)->OnNavigateError(this, url, name, status))
			{
				*Cancel = VARIANT_TRUE;
			}
			++it;
		}
	}
	return S_OK;
}

STDMETHODIMP CHTMLView::OnNavigateComplete2(IDispatch *pDisp, VARIANT* URL)
{
	if(m_spBrowser.IsEqualObject(pDisp))
	{
		detach();
		CComDispatchDriver pDocDisp;
		if(SUCCEEDED(m_spBrowser->get_Document(&pDocDisp)) && pDocDisp)
		{
			CComQIPtr<IHTMLDocument2> pDoc2 = pDocDisp;
			if(pDoc2)
			{
				CComBSTR state;
				if(SUCCEEDED(pDoc2->get_readyState(&state)) &&
					(state == L"complete"/* || state == L"interactive"*/))
				{
					OnDocumentComplete(pDisp, URL);
				}
			}
		}
		attach();
	}
	return S_OK;
}

STDMETHODIMP CHTMLView::OnProgressChange(long /*Progress*/, long /*ProgressMax*/)
{
	return S_OK;
}

STDMETHODIMP CHTMLView::OnQuit()
{
	return S_OK;
}

/************************************************************************/
/* Window events                                                        */
/************************************************************************/
CNewBrowser::CNewBrowser(HWND hParent, IDispatch **ppDisp)
{
	CRect rect;
	Create(hParent, rect , _T("about:blank"), WS_CHILD|WS_CLIPCHILDREN|WS_CLIPSIBLINGS|WS_OVERLAPPED, 0);
	if(SUCCEEDED(QueryControl(&m_spBrowser)) && m_spBrowser)
	{
		DispEventAdvise(m_spBrowser);
		m_spBrowser.QueryInterface(ppDisp);
	}
}

STDMETHODIMP CNewBrowser::BeforeNavigate2(IDispatch* /*pDisp*/, VARIANT *url, VARIANT* /*Flags*/, VARIANT* /*TargetFrameName*/, VARIANT* /*PostData*/, VARIANT* /*Headers*/, VARIANT_BOOL *Cancel)
{
	const LRESULT res = ::SendMessage(::GetParent(GetParent()), WM_OPENLINK, (WPARAM)url, 0);
	if (!res)
	{
		CString csUrl;
		if(url && url->vt == VT_BSTR)
			csUrl = url->bstrVal;
		if(!csUrl.IsEmpty())
		{
			BrowserUtils::openUrlIfSecure(csUrl);
		}
	}
	*Cancel = VARIANT_TRUE;
	DispEventUnadvise(m_spBrowser);
	PostMessage(WM_CLOSE, 0, 0);

	return S_OK;
}

void CHTMLView::disableExternalLinks()
{
	m_disableExternalLinks = TRUE;
}

STDMETHODIMP CHTMLView::OnNewWindow3(IDispatch **ppDisp, VARIANT_BOOL *Cancel, DWORD /*dwFlags*/, BSTR /*bstrUrlContext*/, BSTR bstrUrl)
{
	if (m_disableExternalLinks)
	{
		*Cancel = VARIANT_TRUE;
	}
	else
	{
		if(ppDisp)
		{
			new CNewBrowser(m_hWnd, ppDisp); // for handling anchor # arguments
			*Cancel = VARIANT_FALSE;
		}
		else
		{
			BrowserUtils::openUrlIfSecure(bstrUrl);
			*Cancel = VARIANT_TRUE;
		}
	}
	return S_OK;
}

LRESULT CHTMLView::OnCreate(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled)
{
	bHandled = FALSE;
	return S_OK;
}

LRESULT CHTMLView::OnActivate(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	return S_OK;
}

LRESULT CHTMLView::OnTimer(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	return S_OK;
}

LRESULT CHTMLView::OnClose(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
    ////////////////////////////////////
    /// No one user windows should be hidden!!
	/// ShowWindow(FALSE);
	return S_OK;
}

LRESULT CHTMLView::OnShow(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	return S_OK;
}

LRESULT CHTMLView::OnDestroy(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled)
{
	if(m_spCFHTTPS || m_spCFHTTP || m_spCFFILE)
	{
		CComPtr<IInternetSession> spSession;
		CoInternetGetSession(0, &spSession, 0);
		if(m_spCFHTTPS)
		{
			spSession->UnregisterNameSpace(m_spCFHTTPS, L"https");
			m_spCFHTTPS.Release();
			m_spCFHTTPS = NULL;
		}
		if(m_spCFHTTP)
		{
			spSession->UnregisterNameSpace(m_spCFHTTP, L"http");
			m_spCFHTTP.Release();
			m_spCFHTTP = NULL;
		}
		if (m_spCFFILE)
		{
			spSession->UnregisterNameSpace(m_spCFFILE, L"file");
			m_spCFFILE.Release();
			m_spCFFILE = NULL;
		}
	}

	vDocCompleteListeners.clear();
	SetExternalDispatch(NULL);

	m_pHandler->removeMyDispatchListener(this);
	detach();
	try{
	DispEventUnadvise(m_spBrowser);
	} catch(...){}

	bHandled = FALSE;

	return S_OK;
}

LRESULT CHTMLView::OnErasebkgnd(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL & bHandled)
{
	bHandled= FALSE;
	return S_OK;
}

/************************************************************************/
/*                                                                      */
/************************************************************************/

HWND CHTMLView::HTMLCreate(HWND hWndParent, _U_RECT rect, LPCTSTR szWindowName, DWORD dwStyle , DWORD dwExStyle, BOOL skipHttpsCheck)
{

	HRESULT hr;
	

	HWND m_hWndClient = Create(hWndParent, rect, szWindowName, dwStyle, dwExStyle);
	DeskAlertsAssert(m_hWndClient);

	hr = QueryControl(&m_spBrowser);
	DeskAlertsAssert(SUCCEEDED(hr));

	CComPtr<CComObject<CCustomInternetSecurityImpl>> pSecurityImpl;
	hr = CComObject<CCustomInternetSecurityImpl>::CreateInstance(&pSecurityImpl);
	if(SUCCEEDED(hr))
	{
		(*pSecurityImpl).AddRef();
		pSecurityImpl->SetParams(m_hWnd, skipHttpsCheck);

		CComPtr<IObjectWithSite> pObjWithSite;
		hr = QueryHost(&pObjWithSite);
		if (SUCCEEDED(hr) && pObjWithSite)
		{
			CComPtr<IUnknown> unk;
			hr = pSecurityImpl->QueryInterface(&unk);
			hr = pObjWithSite->SetSite(unk);
		}
		else
		{
			DeskAlertsUnexpected();
		}
	}

//////////////////////////////////////////////////////////////////////////
	CComPtr<IInternetSession> spSession;
	CoInternetGetSession(0, &spSession, 0);
	MetaFactory::CreateInstance(CLSID_HttpSProtocol, &m_spCFHTTPS);
	MetaFactory::CreateInstance(CLSID_HttpProtocol, &m_spCFHTTP);
	MetaFactory::CreateInstance(CLSID_FileProtocol, &m_spCFFILE);

	CComQIPtr<IAlertSink> pSink;
	//PassthroughAPP::CAlertSinkClassFactoryProtocol* palertSync = dynamic_cast<PassthroughAPP::CAlertSinkClassFactoryProtocol*>(m_spCFHTTP.p);
	if(pSink = m_spCFHTTP)
		pSink->SetParams(m_hWnd, skipHttpsCheck);
	if(pSink = m_spCFHTTPS)
		pSink->SetParams(m_hWnd, skipHttpsCheck);
	if(pSink = m_spCFFILE)
		pSink->SetParams(m_hWnd, skipHttpsCheck);


	spSession->RegisterNameSpace(m_spCFHTTPS, CLSID_NULL, L"https", 0, 0, 0);
	spSession->RegisterNameSpace(m_spCFHTTP, CLSID_NULL, L"http", 0, 0, 0);
	spSession->RegisterNameSpace(m_spCFFILE, CLSID_NULL, L"file", 0, 0, 0);


//////////////////////////////////////////////////////////////////////////

	DispEventAdvise(m_spBrowser);
	m_spBrowser->put_RegisterAsDropTarget(VARIANT_FALSE);
	m_spBrowser->put_Silent(VARIANT_FALSE);
	m_disableExternalLinks = FALSE;


	CComObject<CDocHostUIHandler>::CreateInstance(&m_docHostUIHandler);
	(*m_docHostUIHandler).AddRef();

	CComPtr<IDispatch> spDisp;

	hr = m_spBrowser->get_Document(&spDisp);
	CComQIPtr<IOleObject, &IID_IOleObject> spOleObject(spDisp);
	if(spOleObject)
	{
		CComPtr<IOleClientSite> spClientSite;
		hr = spOleObject->GetClientSite(&spClientSite);
		if(SUCCEEDED(hr) && spClientSite)
		{							
			CComQIPtr<IDocHostUIHandler, &IID_IDocHostUIHandler> defUIHandler = spClientSite;
			m_docHostUIHandler->setDefaultHostUIHandler(defUIHandler);
			CComQIPtr<IOleCommandTarget, &IID_IOleCommandTarget> defOleCommandTarget = spClientSite;
			m_docHostUIHandler->setDefaultOleCommandTarget(defOleCommandTarget);
		}
		else
		{
			DeskAlertsUnexpected();
		}
	}
	else
	{
		return NULL;
	}

	CComQIPtr<ICustomDoc> pCustomDoc = spDisp;
	hr = pCustomDoc->SetUIHandler(m_docHostUIHandler);
	DeskAlertsAssert(SUCCEEDED(hr));

	return m_hWndClient;
}

CComPtr<IWebBrowser2> CHTMLView::getWebBrowser()
{
	return m_spBrowser;
}

void CHTMLView::createComponent()
{
}

void CHTMLView::destroyComponent()
{

	CComVariant vtEmpty;
	HRESULT resNavigate = m_spBrowser->Navigate2(&CComVariant(_T("about:blank")), &vtEmpty, &vtEmpty, &vtEmpty, &vtEmpty);
	DeskAlertsAssert(SUCCEEDED(resNavigate));

	if(IsWindow())
	{
		DestroyWindow();
	}
}

void CHTMLView::addWinMessageFilter(IWinMessageFilter* filter)
{
	vMFilter.push_back(filter);
}

void CHTMLView::removeWinMessageFilter(IWinMessageFilter* filter)
{
	std::vector<IWinMessageFilter*>::iterator it = vMFilter.begin();
	while(it!=vMFilter.end())
	{
		if((*it)==filter)
		{
			vMFilter.erase(it);
			break;
		}
		it++;
	}
}

BOOL CHTMLView::WinProcessWindowMessage(HWND /*hWnd*/, UINT uMsg, WPARAM wParam, LPARAM lParam, LRESULT& /*lResult*/, DWORD /*dwMsgMapID*/)
{
	BOOL res = FALSE;

	if((uMsg >= WM_KEYFIRST && uMsg <= WM_KEYLAST &&
		wParam!=VK_LEFT && wParam!=VK_RIGHT && wParam!=VK_BACK && wParam!=VK_DOWN && wParam!=VK_UP) ||
		(uMsg >= WM_MOUSEFIRST && uMsg <= WM_MOUSELAST))
	{
		//const HWND hWndCtl = ::GetFocus();
		//if(IsChild(hWndCtl))
		{
			MSG msg = { m_hWnd, uMsg, wParam, lParam, 0, { 0, 0 } };
			SendMessage(WM_FORWARDMSG, 0, (LPARAM)&msg);
			::SendMessage(::GetParent(m_hWnd), WM_FORWARDMSG, 0, (LPARAM)&msg);
		}
	}
	return res;
}

CComVariant* CHTMLView::DispInvoke(CString /*name*/, DISPPARAMS* /*pDispParams*/)
{
	CComBSTR URL;
	m_spBrowser->get_LocationURL(&URL);
	OnDocumentComplete(m_spBrowser,&CComVariant(URL));
	return NULL;
}

CComQIPtr<IHTMLWindow3> CHTMLView::getWindow(CComPtr<IWebBrowser2> browser)
{
	CComQIPtr<IHTMLWindow3> res;
	CComPtr<IDispatch> docDisp;
	DeskAlertsAssert(browser);
	HRESULT getDocResult = browser->get_Document(&docDisp);
	if (SUCCEEDED(getDocResult) && docDisp)
	{
		CComQIPtr<IHTMLDocument2> pDoc = docDisp;
		if(pDoc)
		{
			CComQIPtr<IHTMLWindow2> pWin;
			if(SUCCEEDED(pDoc->get_parentWindow(&pWin)) && pWin)
			{
				CComQIPtr<IHTMLWindow3> pWin3 = pWin;
				if(pWin3)
				{
					res = pWin3;
				}
			}
		}
	}
	return res;

}

HRESULT CHTMLView::attach()
{
	VARIANT_BOOL res = VARIANT_FALSE;
	CComQIPtr<IHTMLWindow3> win3 = getWindow(m_spBrowser);
	if(win3)
	{
		win3->attachEvent(CComBSTR(L"onload"), m_pHandler, &res);
	}
	return S_OK;
}

HRESULT CHTMLView::detach()
{
	CComQIPtr<IHTMLWindow3> win3 = getWindow(m_spBrowser);
	if(win3)
	{
		win3->detachEvent(CComBSTR(L"onload"), m_pHandler);
	}
	return S_OK;
}

