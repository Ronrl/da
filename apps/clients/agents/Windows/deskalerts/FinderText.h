//
//  findertext.h
//  DeskAlerts Client
//  Common module
//
//  Copyright Softomate 2006-2009. All rights reserved.
//
#pragma once

#include <comutil.h>

class CFinderText
{
	CComQIPtr<IHTMLDocument2, &IID_IHTMLDocument2> lpHtmlDocument;
	_bstr_t match;
	CString lastSearch;
	WORD SearchIndex;

public:
	CFinderText(CComPtr<IWebBrowser2> _pIE,	CString param  = _T("background-color:#0000ff;color:white;"));
	CFinderText(CComQIPtr<IHTMLDocument2, &IID_IHTMLDocument2> lpHtmlDocument,	CString param  = _T("background-color:#0000ff;color:white;"));
	~CFinderText(void);

	void find(CString search);


	static void ClearSearchResults(CComQIPtr<IHTMLDocument2, &IID_IHTMLDocument2> lpHtmlDocument,
							_bstr_t searchID);
	static void ScrollToFindText(CComQIPtr<IHTMLDocument2, &IID_IHTMLDocument2> lpHtmlDocument,
						WORD& NumItem, _bstr_t searchID);
	static void ShowHiddenElement(CComQIPtr<IHTMLDOMNode> pElem);
	static void FindText2(CComQIPtr<IHTMLDocument2, &IID_IHTMLDocument2> lpHtmlDocument,
								_bstr_t searchText, long lFlags , _bstr_t matchStyle, _bstr_t searchID );

};
