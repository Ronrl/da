#pragma once

class RegistryMonitor : public CFrameWindowImpl<RegistryMonitor>
{
public:

	enum {
		UWM_VALUE_CHANGED = WM_APP + 401,
		UTM_STOP_MONITOR
	};

	BEGIN_MSG_MAP(RegistryMonitor)
		MESSAGE_HANDLER(UWM_VALUE_CHANGED, OnValueChanged)
	END_MSG_MAP()

	RegistryMonitor(void);
	~RegistryMonitor(void);
	static DWORD WINAPI notifyThread(LPVOID pv);
	void startRegMonitoring(CRegKey key, int (*callbackFunc)(LPVOID pv),LPVOID pv);
	
private: 
	int (*m_callbackFunc)(LPVOID pv);
	LPVOID m_pv;
	HANDLE m_thread;
	DWORD m_threadId;
	struct ThreadStruct
	{
		CRegKey key;
		HWND hwnd;
        ThreadStruct():hwnd(NULL){}
	};
	LRESULT OnValueChanged(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
};