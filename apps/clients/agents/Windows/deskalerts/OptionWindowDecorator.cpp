#include "StdAfx.h"
#include ".\optionwindowdecorator.h"

COptionWindowDecorator::COptionWindowDecorator(CHTMLView& _cHTMLView):cHTMLView(_cHTMLView)
{
}

COptionWindowDecorator::~COptionWindowDecorator(void)
{
}


void COptionWindowDecorator::createOptionPage()
{
	CRect minibrowserRect;
	int m_width = 400;
	int m_heigh = 400;
	int mx = ::GetSystemMetrics(SM_CXMAXIMIZED);
	int my = ::GetSystemMetrics(SM_CYMAXIMIZED);

	minibrowserRect.top = my - m_heigh-10;
	minibrowserRect.bottom = my-10;

	minibrowserRect.left = mx - m_width-10;
	minibrowserRect.right = mx-10;

	if (::IsWindow(cHTMLView.m_hWnd))
	{
		cHTMLView.MoveWindow(minibrowserRect,TRUE);
	}
	else
	{
		HWND parentHWND = (HWND)_iAlertModel->getProperty(CString(_T("mainHWND")));
		cHTMLView.HTMLCreate(parentHWND, minibrowserRect, _T("about:blank"),
			WS_HSCROLL			| \
			WS_VSCROLL			| \
			WS_POPUP			| \
			WS_CAPTION			| \
			WS_SYSMENU			| \
			WS_THICKFRAME		| \
			WS_MINIMIZEBOX		| \
			WS_MAXIMIZEBOX, /*WS_EX_CLIENTEDGE*/0, TRUE);

		if (_iAlertController->getInitFlags() & DISABLE_EXTERNAL_LINKS)
		{
			cHTMLView.disableExternalLinks();
		}

		cHTMLView.SetWindowText(_T("Alert Option"));
	}
}

bool COptionWindowDecorator::show()
{
	cHTMLView.ShowWindow(SW_SHOWNORMAL);
	return true;
}
