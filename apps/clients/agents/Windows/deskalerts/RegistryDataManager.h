#pragma once
#include "ServiceDataManager.h"


class RegistryDataManager : public ServiceDataManager
{
private:
	PREGISTRYDATA m_regData;
public:
	RegistryDataManager(void);
	~RegistryDataManager(void);
	DWORD serialize(IStream* pStream);
	void setData(PREGISTRYDATA regData);
	RequestDataType getDataType();
};
