#pragma once

#include "stdafx.h"

namespace BrowserUtils
{
	void openUrlIfSecure(CString sIn, const int encode = CP_UTF8);
	CString urlSecure(CString sIn, const int encode = CP_UTF8);
	CString getURLfromPath(CString sIn, const int encode = CP_UTF8);
	CString fixURI(CString sIn, const int encode = CP_UTF8);
	CString escape(CString sIn, const int encode = CP_UTF8);
	CString encodeURI(CString sIn, const int encode = CP_UTF8);
	CString encodeURIComponent(CString sIn, const int encode = CP_UTF8);
	CString URLEncode(CString sIn, LPCTSTR exclude, const int encode = CP_UTF8);

	INT64 variantToInt64(const VARIANT &var, INT64 def = 0LL);
	CString &variantToString(const VARIANT &var, CString &def = CString());
};
