#pragma once

//#include "IInclude.h"
#include "CMyPageHook.h"
#include <vector>
#include <exdispid.h>

#include "CustomInternetSecurityImpl.h"
#include "EventHandler.h"
#include "IComponent.h"

#define WM_OPENLINK (WM_APP+1)

typedef DWORD (WINAPI *PSLWA)(HWND, DWORD, BYTE, DWORD);

class CDocHostUIHandler;
class CHTMLView;
class IDocCompleteListener
{
public:
	virtual void OnDocComplete(CHTMLView* HTMLView) = 0;
	virtual BOOL OnNavigateError(CHTMLView* HTMLView, CString URL, CString TargetFrameName, LONG StatusCode) = 0;
};

class CNewBrowser:
	public IDispEventImpl<1, CNewBrowser, &DIID_DWebBrowserEvents2, &LIBID_SHDocVw, 1, 0>,
	public CWindowImpl<CNewBrowser,CAxWindow>
{
public:
	CNewBrowser(HWND hParent, IDispatch **ppDisp);

	BEGIN_SINK_MAP(CNewBrowser)
		SINK_ENTRY_EX(1, DIID_DWebBrowserEvents2, DISPID_BEFORENAVIGATE2, BeforeNavigate2)
	END_SINK_MAP()

	BEGIN_MSG_MAP(CNewBrowser)
	END_MSG_MAP()

	STDMETHOD(BeforeNavigate2)(IDispatch *pDisp, VARIANT *url, VARIANT *Flags, VARIANT *TargetFrameName, VARIANT *PostData, VARIANT *Headers, VARIANT_BOOL *Cancel);

private:
	CComPtr<IWebBrowser2> m_spBrowser;
};


class CHTMLView:
	public IDispEventImpl<1, CHTMLView, &DIID_DWebBrowserEvents2, &LIBID_SHDocVw, 1, 0>,
	public CWindowImpl<CHTMLView,CAxWindow>,
	public IComponent,
	public IWinMessageFilter,
	public IMyDispatchListener
{
public:
	CHTMLView(void);
	~CHTMLView(void);

	BEGIN_SINK_MAP(CHTMLView)
		SINK_ENTRY_EX(1, DIID_DWebBrowserEvents2, DISPID_DOWNLOADCOMPLETE , OnDownloadComplete)
		SINK_ENTRY_EX(1, DIID_DWebBrowserEvents2, DISPID_NAVIGATECOMPLETE2 , OnNavigateComplete2)
		SINK_ENTRY_EX(1, DIID_DWebBrowserEvents2, DISPID_PROGRESSCHANGE , OnProgressChange)
		SINK_ENTRY_EX(1, DIID_DWebBrowserEvents2, DISPID_ONQUIT, OnQuit)
		SINK_ENTRY_EX(1, DIID_DWebBrowserEvents2, DISPID_NEWWINDOW3, OnNewWindow3)
		SINK_ENTRY_EX(1, DIID_DWebBrowserEvents2, DISPID_NAVIGATEERROR, OnNavigateError)
	END_SINK_MAP()

	STDMETHOD(OnDownloadComplete)();
	STDMETHOD(OnProgressChange)(long Progress, long ProgressMax);
	STDMETHOD(OnDocumentComplete) (IDispatch *pDisp, VARIANT *URL);
	STDMETHOD(OnNavigateComplete2)(IDispatch *pDisp, VARIANT *URL);
	STDMETHOD(OnQuit)();
	STDMETHOD(OnNewWindow3)(IDispatch **ppDisp, VARIANT_BOOL *Cancel, DWORD dwFlags, BSTR bstrUrlContext, BSTR bstrUrl);
	STDMETHOD(OnNavigateError)(IDispatch *pDisp, VARIANT *URL, VARIANT *TargetFrameName, VARIANT *StatusCode, VARIANT_BOOL *Cancel);

	BEGIN_MSG_MAP(CHTMLView)
		MESSAGE_HANDLER(WM_CREATE, OnCreate)
		MESSAGE_HANDLER(WM_ACTIVATE, OnActivate)
		MESSAGE_HANDLER(WM_TIMER, OnTimer)
		MESSAGE_HANDLER(WM_CLOSE, OnClose)
		MESSAGE_HANDLER(WM_SHOWWINDOW, OnShow)
		MESSAGE_HANDLER(WM_DESTROY, OnDestroy)
		MESSAGE_HANDLER(WM_ERASEBKGND, OnErasebkgnd)
	END_MSG_MAP()

	LRESULT OnCreate(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL & bHandled);
	LRESULT OnActivate(UINT uMsg, WPARAM wParam, LPARAM lParam,BOOL& bHandled);
	LRESULT OnTimer(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnShow(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnErasebkgnd(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	HWND HTMLCreate(HWND hWndParent, _U_RECT rect, LPCTSTR szWindowName, DWORD dwStyle , DWORD dwExStyle, BOOL skipHttpsCheck);

	void createComponent();
	void destroyComponent();

	CComPtr<IWebBrowser2> getWebBrowser();

	void addWinMessageFilter(IWinMessageFilter* filter);
	void removeWinMessageFilter(IWinMessageFilter* filter);

	BOOL WinProcessWindowMessage(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam,LRESULT& lResult, DWORD dwMsgMapID);

	void Refresh(){ m_spBrowser->Refresh(); }
	void AddDocCompleteListener(IDocCompleteListener *iNewListener);
	void DeleteDocCompleteListener(IDocCompleteListener *iListener);

	CComVariant* DispInvoke(CString name,DISPPARAMS *pDispParams);

	void disableExternalLinks();
	
private:
	CComPtr<IWebBrowser2> m_spBrowser;
	CComPtr<CComObject<CDocHostUIHandler>> m_docHostUIHandler;
	BOOL m_browserInitialized;
	HANDLE m_destroyEvent;
	BOOL m_disableExternalLinks;
	CMyPageHook* m_cMyPageHook;
	
	std::vector<IWinMessageFilter*> vMFilter;
	std::vector<IDocCompleteListener*> vDocCompleteListeners;

	CComQIPtr<IHTMLWindow3> getWindow(CComPtr<IWebBrowser2> browser);
	HRESULT attach();
	HRESULT detach();
	CComPtr<IClassFactory> m_spCFHTTPS;
	CComPtr<IClassFactory> m_spCFHTTP;
	CComPtr<IClassFactory> m_spCFFILE;
	CComPtr<CComObject<CEventHandler>> m_pHandler;
};
