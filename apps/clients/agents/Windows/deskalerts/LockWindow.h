#pragma once

#include "StdAfx.h"

template <class T, COLORREF t_crBrushColor>
class CPaintBkgnd
{
public:
	CPaintBkgnd() { m_hbrBkgnd = CreateSolidBrush(t_crBrushColor); }
	~CPaintBkgnd() { DeleteObject ( m_hbrBkgnd ); }

	BEGIN_MSG_MAP(CPaintBkgnd)
		MESSAGE_HANDLER(WM_ERASEBKGND, OnEraseBkgnd)
	END_MSG_MAP()

	LRESULT OnEraseBkgnd(UINT /*uMsg*/, WPARAM wParam, LPARAM /*lParam*/, BOOL& /*bHandled*/)
	{
		T* pT = static_cast<T*>(this);
		HDC dc = (HDC) wParam;
		RECT rcClient;

		pT->GetClientRect ( &rcClient );
		FillRect ( dc, &rcClient, m_hbrBkgnd );
		return 1;    // we painted the background
	}

protected:
	HBRUSH m_hbrBkgnd;
};

class CLockWindow : public CWindowImpl<CLockWindow, CWindow>,
	public CPaintBkgnd<CLockWindow, RGB(0,0,0)>
{
	

private:
	typedef CPaintBkgnd<CLockWindow, RGB(0,0,0)> CPaintBkgndBase;
	static CComAutoCriticalSection _cs;
	static CLockWindow* CLockWindow::_self;
	static volatile LONG CLockWindow::_refcount;
	HWND m_windowToLock;
	static vector<HWND> m_windows;

	CLockWindow();
	~CLockWindow();

	void _AddWindow(HWND windowToLock);

public:

	static void AddWindow(HWND wnd);
	static void RemoveWindow(HWND wnd);

	BEGIN_MSG_MAP(CLockWindow)
		MESSAGE_HANDLER(WM_CLOSE, OnClose)
		MESSAGE_HANDLER(WM_SETFOCUS, OnSetFocus)
		CHAIN_MSG_MAP(CPaintBkgndBase)
	END_MSG_MAP()

	LRESULT OnClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnSetFocus(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
};
