#include "StdAfx.h"
#include "AlertUtils.h"
#include "AlertsJAClient.h"
#include "CrashLogsManager.h"

//TODO: Add logging
bool CrashLogsManager::DoSomething() {
	CString crashLogsFolderPath = CAlertUtils::crashLogFolderPath();
	CString pathMask = crashLogsFolderPath + _T("\\*");
	CString server = _iAlertController->getJAClient()->getServer();

	WIN32_FIND_DATA fileInfo;
	HANDLE hf = FindFirstFile(pathMask, &fileInfo);
	if (hf != INVALID_HANDLE_VALUE)
	{
		do
		{
			CString filename = fileInfo.cFileName;
			if (filename == _T(".") || filename == _T("..")) {
				continue;
			}

			CString filepath = crashLogsFolderPath + _T("\\") + filename;
			bool result = CrashLogsManager::SendFileToServer(server, filepath);

			if (result == true) {
				DeleteFile(filepath);
			}
		} 
		while (FindNextFile(hf, &fileInfo) != 0);

		FindClose(hf);
	}

	return true;
}

// TODO: Add file deleting from directory
// TODO: Add error handling
bool CrashLogsManager::SendFileToServer(CString server, CString filepath)
{
	char *buffer;	// Buffer containing file                
	FILE *pFile;	// File pointer
	long lSize;		// Size of file
	
	// Open file
	CT2A formatted_path(filepath);
	pFile = fopen(formatted_path, "rb");
	if (pFile == NULL)
	{
		return false;
	}

	// Calc file size:
	fseek(pFile, 0, SEEK_END);
	lSize = ftell(pFile);
    if (lSize <= 0)
        return false;

    unsigned long const usize = static_cast<unsigned long>(lSize);
	rewind(pFile);

	// Allocate memory to contain the whole file:
	buffer = (char*)malloc(sizeof(char)*usize);
	if (buffer == NULL)
	{
		return false;
	}

	// Copy the file into the buffer:
	size_t copiedSize = fread(buffer, 1, usize, pFile);
	rewind(pFile);
	if (copiedSize != usize)
	{
		return false;
	}

	// Close the file
	fclose(pFile);

	// Send the file
	JAReq_CrashLogs request;
	DWORD result = CAlertUtils::sendFileByHTTPRequest(server, request.getURL(), true, _T("POST"), buffer, usize);

	// Clean buffer
	free(buffer);

	return result == 1;
}