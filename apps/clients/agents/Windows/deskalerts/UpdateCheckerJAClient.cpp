#include "stdafx.h"
#include "UpdateChecker.h"
//#include "InetUtils.cpp"
#include "rapidjson/document.h"
#include "UpdateCheckerJAClient.h"

UpdateChecker * UpdateChecker::pInstance_ = NULL;

unsigned WINAPI checkerThread(PVOID pvParam);

DWORD sendHTTPRequest(CString szURL, bool skipHttpsCheck ,
	const TCHAR *szRequestType, const char * szRequestData, char *outBuffer, const DWORD bufferSize );


UpdateChecker::UpdateChecker(void):callback_(NULL)
{
}

UpdateChecker::UpdateChecker(CString baseServerUrl, CString currentVersion, CString deskbarId, unsigned pullPeriod,  void (*callback)(CString url, CString version))
{
	host_ = baseServerUrl;
	baseServerUrl_ = baseServerUrl;
	versionUrl_ =  baseServerUrl+_T(VERSION_URL);
	slotUrl_ =  baseServerUrl+_T(SLOT_URL);
	currentVersion_ = currentVersion;
	deskbarId_ = deskbarId;
	callback_ = callback;

	operationPeriod_ = TIMER_CHECK_VERSION;
	pullPeriod_ = pullPeriod;
	//pInstance_ = this;
	operationIndex_ = 0;

	//pOperations_ =  new PF[4];

	//pOperations_[0] = &UpdateChecker::updateAvailCheck;
	//pOperations_[1] = &UpdateChecker::permissionToDownloadingCheck;
	//pOperations_[2] = &UpdateChecker::downloadFile;
	//pOperations_[3] = &UpdateChecker::waitUpdateFromService;
	vOperations_.push_back(&UpdateChecker::updateAvailCheck);
	vOperations_.push_back(&UpdateChecker::permissionToDownloadingCheck);
	//vOperations_.push_back(&UpdateChecker::downloadFile);
	vOperations_.push_back(&UpdateChecker::deleteSlot);
	vOperations_.push_back(&UpdateChecker::waitUpdateFromService);
}


UpdateChecker::~UpdateChecker(void)
{
}

void  UpdateChecker::start(CString baseServerURL, CString currentVersion, CString deskbarId, unsigned pullPeriod,  void (*callback)(CString url, CString version) )
{
	//if( checker )
	pInstance_ = new UpdateChecker( baseServerURL, currentVersion, deskbarId, pullPeriod, callback);

	_beginthreadex(NULL, 0, checkerThread, (PVOID)pInstance_, 0, NULL);
	//(&checkerThread, 0, (void *));
	//creaate and run thread
	//return checker;
	return ;
}

////rest methods
BOOL UpdateChecker::updateAvailCheck()
{
	operationPeriod_ = TIMER_CHECK_VERSION;
	char outBuffer[1025];
	outBuffer[0] = '0';

	//if( !CInetUtils::sendRequest(versionUrl_, newVersion_, VERSION_SIZE) ) return false;
	if( CAlertUtils::sendHTTPRequest( versionUrl_, true,	_T("GET"), 	"", outBuffer, 1024)!=1) return false;
	
	//extract json
	rapidjson::Document doc;
	doc.Parse(outBuffer);
	
	ResponseObj ro;//* ro = new ResponseObj();
	rd_Version vrd;//* vrd = new rd_Version();
	ro.fromJSON(doc, (ResponseData *)&vrd);

	newVersion_ = vrd.getVersion();
	if( ro.getStatus() == "Success" ){
		if( _tcscmp( newVersion_, currentVersion_ ) == 0 ) 
			return false;

		if( !checkVersionAttemptCount(newVersion_) )
			return false;
		return true;
	}
	return false;
}

BOOL UpdateChecker::permissionToDownloadingCheck()
{
	char outBuffer[1025];
	outBuffer[0] = '0';
	operationPeriod_ = pullPeriod_;

	char szReqTempl[]="{\"deskbarId\": \"%s\",\"version\": \"%s\"}";

	//char *req = new char[MAX_PATH*5];
	char req [MAX_PATH*5];
	USES_CONVERSION;
	LPSTR lpId = W2A( deskbarId_ );
	LPSTR lpVer = W2A( newVersion_ );
	sprintf_s(req,MAX_PATH*5,szReqTempl, lpId, lpVer );
	if( CAlertUtils::sendHTTPRequest( slotUrl_, true,	_T("POST"), 	req, outBuffer, 1024)!=1) return false;

	if( strlen(outBuffer) == 0 )return false;
	
	//extract json
	rapidjson::Document doc;
	doc.Parse(outBuffer);
	
	ResponseObj  ro;//*ro = new ResponseObj();
	rd_Slot  vslot;//*vslot = new rd_Slot();
	ro.fromJSON(doc, (ResponseData *)&vslot);

	if( ro.getStatus()=="Success" ){
		slotId_=vslot.getSlotId();
		downloadUrl_=vslot.getDownloadUrl();
		//if( callback_!=NULL )callback_(downloadUrl_, newVersion_);
		
		return true;
	}
	
	return false;
}


/*
BOOL UpdateChecker::downloadFile()
{
	char outBuffer[1025];
	outBuffer[0] = '0';
	operationPeriod_ = pullPeriod_;
	CString url = baseServerUrl_+_T(DOWNLOAD_FILE_URL);
	char szReqTempl[]="{\"slotId\": \"%s\"}";

	char *req = new char[MAX_PATH*5];
	USES_CONVERSION;
	LPSTR lpslotId = W2A( slotId_ );
	sprintf_s(req,MAX_PATH*5,szReqTempl, lpslotId );
	if( sendHTTPRequest( url, true,	_T("POST"), 	req, outBuffer, 1024)!=1) return false;

	if( strlen(outBuffer) == 0 )return false;
	
	//extract json
	rapidjson::Document doc;
	doc.Parse(outBuffer);
	
	ResponseObj * ro = new ResponseObj();
	rd_Download * vdwd = new rd_Download();
	ro->fromJSON(doc, vdwd);

	if( ro->getStatus()=="Success" ){
		downloadUrl_=vdwd->getDownloadUrl();
//download


		return true;
	}

	return false;
}*/

BOOL UpdateChecker::deleteSlot()
{
	char outBuffer[1025];
	outBuffer[0] = '0';
	operationPeriod_ = INFINITE;

	CString url = baseServerUrl_+_T(DELETE_SLOT_URL);
	char szReqTempl[]="{\"slotId\": \"%s\"}";

	//char *req = new char[MAX_PATH*5];
	char req[MAX_PATH*5] ;
	USES_CONVERSION;
	LPSTR lpslotId = W2A( slotId_ );
	sprintf_s(req,MAX_PATH*5,szReqTempl, lpslotId );
	if( CAlertUtils::sendHTTPRequest( url, true,	_T("POST"), 	req, outBuffer, 1024)!=1) return false;

	if( strlen(outBuffer) == 0 )return false;
	
	//extract json
	rapidjson::Document doc;
	doc.Parse(outBuffer);
	
	ResponseObj  ro;//*ro = new ResponseObj();
	rd_Delete  vdel;//*vdel = new rd_Delete();
	ro.fromJSON(doc,(ResponseData *) &vdel);
		
	if( callback_!=NULL )callback_(downloadUrl_, newVersion_);

	//if( ro->getStatus()=="Success" ){
	//	return true;
	//}

	return false;
}


BOOL UpdateChecker::waitUpdateFromService()
{
	operationPeriod_ = INFINITE;
	return true;
	//send messag to caller thread about waiting update from service
}

CString UpdateChecker::getCurrentVersion(){
	return "";
}


unsigned WINAPI UpdateChecker::checkerThread(PVOID /*pvParam*/)
{
	BOOL res;
	while(1){
		if( pInstance_->vOperations_[pInstance_->operationIndex_] )
		{
			res = (pInstance_->*pInstance_->vOperations_[pInstance_->operationIndex_])();
			
			if ( res ) 
			{
				pInstance_->operationIndex_++;
				if( pInstance_->operationIndex_ == pInstance_->vOperations_.size() )pInstance_->operationIndex_ = 0;
				continue;
			}
		}
		Sleep( pInstance_->operationPeriod_ );
	}
	return 0;
}


BOOL UpdateChecker::checkVersionAttemptCount(CString version)
{
	HKEY hKey;
	TCHAR regVersion[256] = {0};
	DWORD dwSize = MAX_PATH;

	CString regPath= _T(INSTALL_ATTEMPTS_REG_KEY) ;
	LONG lRes = RegOpenKeyEx(HKEY_LOCAL_MACHINE, regPath, 0, KEY_READ|KEY_WOW64_32KEY, &hKey);

	if(lRes != ERROR_SUCCESS){
		regPath.Format( _T("Error: RegOpenKeyEx: %d  dwRet=%d") ,lRes, GetLastError() );	
		return true;
	}

	LONG nError = ::RegQueryValueEx( hKey, _T("version"), 
			0, 
			NULL,
			(LPBYTE)regVersion, 
			&dwSize);

	if( nError != ERROR_SUCCESS ) return true;
	if( regVersion != version ) return true;

    DWORD dwBufferSize(sizeof(DWORD));
    DWORD nResult(0);
    nError = ::RegQueryValueExW(hKey, _T("attempt_count"),
        0,
        NULL,
        (LPBYTE)&nResult,
        &dwBufferSize);
    if (nError == ERROR_SUCCESS )
    {
        if(nResult>=MAX_ATTEMPT_COUNT) return false;

    }
    return true;
}
	//bool bDoesNotExistsSpecifically (lRes == ERROR_FILE_NOT_FOUND);

	/*std::wstring strValueOfBinDir;
	std::wstring strKeyDefaultValue;
	GetStringRegKey(hKey, L"BinDir", strValueOfBinDir, L"bad");
	GetStringRegKey(hKey, L"", strKeyDefaultValue, L"bad");

LONG GetDWORDRegKey(HKEY hKey, const std::wstring &strValueName, DWORD &nValue, DWORD nDefaultValue)
{
    nValue = nDefaultValue;
    DWORD dwBufferSize(sizeof(DWORD));
    DWORD nResult(0);
    LONG nError = ::RegQueryValueExW(hKey,
        strValueName.c_str(),
        0,
        NULL,
        reinterpret_cast<LPBYTE>(&nResult),
        &dwBufferSize);
    if (ERROR_SUCCESS == nError)
    {
        nValue = nResult;
    }
    return nError;
}


LONG GetBoolRegKey(HKEY hKey, const std::wstring &strValueName, bool &bValue, bool bDefaultValue)
{
    DWORD nDefValue((bDefaultValue) ? 1 : 0);
    DWORD nResult(nDefValue);
    LONG nError = GetDWORDRegKey(hKey, strValueName.c_str(), nResult, nDefValue);
    if (ERROR_SUCCESS == nError)
    {
        bValue = (nResult != 0) ? true : false;
    }
    return nError;
}


LONG GetStringRegKey(HKEY hKey, const std::wstring &strValueName, std::wstring &strValue, const std::wstring &strDefaultValue)
{
    strValue = strDefaultValue;
    WCHAR szBuffer[512];
    DWORD dwBufferSize = sizeof(szBuffer);
    ULONG nError;
    nError = RegQueryValueExW(hKey, strValueName.c_str(), 0, NULL, (LPBYTE)szBuffer, &dwBufferSize);
    if (ERROR_SUCCESS == nError)
    {
        strValue = szBuffer;
    }
    return nError;
}
*/


