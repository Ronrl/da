#pragma once
#include <map>
#include <vector>


class IMyDispatchListener
{
public:
	virtual CComVariant* DispInvoke(CString name,DISPPARAMS *pDispParams) = 0;
};

class CEventHandler :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CEventHandler, &CLSID_NULL>,
	public IDispatch
{
	std::map<int,CString> idName;
	std::map<CString,int> nameId;
	int id;

	std::vector<IMyDispatchListener*> displists;

public:
	CEventHandler() : id(DISPID_VALUE+1)
	{

	}

	DECLARE_PROTECT_FINAL_CONSTRUCT()
	DECLARE_NO_REGISTRY()

	BEGIN_COM_MAP(CEventHandler)
		COM_INTERFACE_ENTRY(IDispatch)
	END_COM_MAP()

public:

	void addMyDispatchListener(IMyDispatchListener* p);
	void removeMyDispatchListener(IMyDispatchListener* p);

	//IDispatch
	HRESULT STDMETHODCALLTYPE GetTypeInfoCount(
		/* [out] */ UINT *pctinfo);

	HRESULT STDMETHODCALLTYPE GetTypeInfo(
		/* [in] */ UINT iTInfo,
		/* [in] */ LCID lcid,
		/* [out] */ ITypeInfo **ppTInfo);

	HRESULT STDMETHODCALLTYPE GetIDsOfNames(
		/* [in] */ REFIID riid,
		/* [size_is][in] */ LPOLESTR *rgszNames,
		/* [in] */ UINT cNames,
		/* [in] */ LCID lcid,
		/* [size_is][out] */ DISPID *rgDispId);

	HRESULT STDMETHODCALLTYPE Invoke(
		/* [in] */ DISPID dispIdMember,
		/* [in] */ REFIID riid,
		/* [in] */ LCID lcid,
		/* [in] */ WORD wFlags,
		/* [out][in] */ DISPPARAMS *pDispParams,
		/* [out] */ VARIANT *pVarResult,
		/* [out] */ EXCEPINFO *pExcepInfo,
		/* [out] */ UINT *puArgErr);

};
