// CustomInternetSecurityImpl.h : Declaration of the CCustomInternetSecurityImpl

#pragma once
#include "resource.h"       // main symbols
#include "DeskAlerts.h"

#if defined(_WIN32_WCE) && !defined(_CE_DCOM) && !defined(_CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA)
#error "Single-threaded COM objects are not properly supported on Windows CE platform, such as the Windows Mobile platforms that do not include full DCOM support. Define _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA to force ATL to support creating single-thread COM object's and allow use of it's single-threaded COM object implementations. The threading model in your rgs file was set to 'Free' as that is the only threading model supported in non DCOM Windows CE platforms."
#endif


// CCustomInternetSecurityImpl

class ATL_NO_VTABLE CCustomInternetSecurityImpl :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CCustomInternetSecurityImpl, &CLSID_CustomInternetSecurityImpl>,
	public IDispatchImpl<ICustomInternetSecurityImpl, &IID_ICustomInternetSecurityImpl, &LIBID_DESKALERTSLib, /*wMajor =*/ 1, /*wMinor =*/ 0>,
	public IServiceProviderImpl<CCustomInternetSecurityImpl>,
	public IInternetSecurityManager,
	public IOleCommandTarget,
//	public IOleClientSite,
	public IHttpSecurity
{
public:

	CCustomInternetSecurityImpl() : m_phwnd(NULL), m_skipHttpsCheck(FALSE) {}

	DECLARE_PROTECT_FINAL_CONSTRUCT()
	DECLARE_NO_REGISTRY()

	BEGIN_SERVICE_MAP(CCustomInternetSecurityImpl)
		SERVICE_ENTRY(IID_IInternetSecurityManager)
		SERVICE_ENTRY(IID_IHttpSecurity)
		SERVICE_ENTRY(IID_IWindowForBindingUI)
	END_SERVICE_MAP()

	BEGIN_COM_MAP(CCustomInternetSecurityImpl)
		COM_INTERFACE_ENTRY(IInternetSecurityManager)
		COM_INTERFACE_ENTRY(IServiceProvider)
		COM_INTERFACE_ENTRY(ICustomInternetSecurityImpl)
		COM_INTERFACE_ENTRY(IDispatch)
		COM_INTERFACE_ENTRY(IOleCommandTarget)
//		COM_INTERFACE_ENTRY(IOleClientSite)
		COM_INTERFACE_ENTRY(IHttpSecurity)
		COM_INTERFACE_ENTRY(IWindowForBindingUI)
	END_COM_MAP()

private:
	//ICustomInternetSecurityImpl
	HWND m_phwnd;
	BOOL m_skipHttpsCheck;

public:
	//ICustomInternetSecurityImpl
	STDMETHOD(SetParams)(
	/* [in] */ HWND hwnd,
	/* [in] */ BOOL skipHttpsCheck);

public:
	//IInternetSecurityManager
	STDMETHOD(SetSecuritySite)(
		/* [unique][in] */ __RPC__in_opt IInternetSecurityMgrSite *pSite);

	STDMETHOD(GetSecuritySite)(
		/* [out] */ __RPC__deref_out_opt IInternetSecurityMgrSite **ppSite);

	STDMETHOD(MapUrlToZone)(
		/* [in] */ __RPC__in LPCWSTR pwszUrl,
		/* [out] */ __RPC__out DWORD *pdwZone,
		/* [in] */ DWORD dwFlags);
	STDMETHOD(GetSecurityId)(
		/* [in] */ __RPC__in LPCWSTR pwszUrl,
		/* [size_is][out] */ __RPC__out_ecount_full(*pcbSecurityId) BYTE *pbSecurityId,
		/* [out][in] */ __RPC__inout DWORD *pcbSecurityId,
		/* [in] */ DWORD_PTR dwReserved);

	STDMETHOD(ProcessUrlAction)(
		/* [in] */ __RPC__in LPCWSTR pwszUrl,
		/* [in] */ DWORD dwAction,
		/* [size_is][out] */ __RPC__out_ecount_full(cbPolicy) BYTE *pPolicy,
		/* [in] */ DWORD cbPolicy,
		/* [in] */ __RPC__in BYTE *pContext,
		/* [in] */ DWORD cbContext,
		/* [in] */ DWORD dwFlags,
		/* [in] */ DWORD dwReserved);

	STDMETHOD(QueryCustomPolicy)(
		/* [in] */ __RPC__in LPCWSTR pwszUrl,
		/* [in] */ __RPC__in REFGUID guidKey,
		/* [size_is][size_is][out] */ __RPC__deref_out_ecount_full_opt(*pcbPolicy) BYTE **ppPolicy,
		/* [out] */ __RPC__out DWORD *pcbPolicy,
		/* [in] */ __RPC__in BYTE *pContext,
		/* [in] */ DWORD cbContext,
		/* [in] */ DWORD dwReserved);

	STDMETHOD(SetZoneMapping)(
		/* [in] */ DWORD dwZone,
		/* [in] */ __RPC__in LPCWSTR lpszPattern,
		/* [in] */ DWORD dwFlags);

	STDMETHOD(GetZoneMappings)(
		/* [in] */ DWORD dwZone,
		/* [out] */ __RPC__deref_out_opt IEnumString **ppenumString,
		/* [in] */ DWORD dwFlags);

public:
	// IOleCommandTarget
	STDMETHOD (QueryStatus)(
		/* [unique][in] */ const GUID *pguidCmdGroup,
		/* [in] */ ULONG cCmds,
		/* [out][in][size_is] */ OLECMD prgCmds[  ],
		/* [unique][out][in] */ OLECMDTEXT *pCmdText);
	STDMETHOD(Exec)(
		/*[in]*/ const GUID *pguidCmdGroup,
		/*[in]*/ DWORD nCmdID,
		/*[in]*/ DWORD nCmdExecOpt,
		/*[in]*/ VARIANTARG *pvaIn,
		/*[in,out]*/ VARIANTARG *pvaOut);

public:
	//IOleClientSite
	STDMETHODIMP SaveObject(){ return E_NOTIMPL; }

	STDMETHODIMP GetMoniker(
		/* [in] */ DWORD /*dwAssign*/,
		/* [in] */ DWORD /*dwWhichMoniker*/,
		/* [out] */ IMoniker** /*ppmk*/){ return E_NOTIMPL; }

	STDMETHODIMP GetContainer(
		/* [out] */ IOleContainer** /*ppContainer*/){ return E_NOTIMPL; }

	STDMETHODIMP ShowObject(){ return S_OK; }

	STDMETHODIMP OnShowWindow(
		/* [in] */ BOOL /*fShow*/){ return S_OK; }

	STDMETHODIMP RequestNewObjectLayout(){ return E_NOTIMPL; }

public:
	//IWindowForBindingUI
	STDMETHOD(GetWindow)(
		/* [in] */ REFGUID rguidReason,
		/* [out] */ HWND *phwnd);

public:
	//IHttpSecurity
	STDMETHOD(OnSecurityProblem)(
		/* [in] */ DWORD dwProblem);
};
