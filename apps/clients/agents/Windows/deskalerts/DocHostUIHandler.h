#pragma once

class CDocHostUIHandler:
	public CComObjectRootEx<CComSingleThreadModel>,
	public IDispatchImpl<IDocHostUIHandler, &IID_IDocHostUIHandler, &LIBID_MSHTML>,
	public IOleCommandTarget,
	IDocHostShowUI
{
public:
	BEGIN_COM_MAP(CDocHostUIHandler)
		COM_INTERFACE_ENTRY(IDocHostUIHandler)
		COM_INTERFACE_ENTRY(IOleCommandTarget)
		COM_INTERFACE_ENTRY(IDocHostShowUI)
	END_COM_MAP()

public:
	CDocHostUIHandler();
	~CDocHostUIHandler();

public:
	void setDefaultHostUIHandler(CComPtr<IDocHostUIHandler> spDefaultDocHostUIHandler);
	void setDefaultOleCommandTarget(CComPtr<IOleCommandTarget> spDefaultOleCommandTarget);
	// IDocHostUIHandler
public:

	STDMETHOD(ShowContextMenu)(DWORD dwID, POINT FAR* ppt, IUnknown FAR* pcmdTarget, IDispatch FAR* pdispReserved);
	STDMETHOD(ShowUI)(DWORD dwID, IOleInPlaceActiveObject FAR* pActiveObject,
		IOleCommandTarget FAR* pCommandTarget,
		IOleInPlaceFrame  FAR* pFrame,
		IOleInPlaceUIWindow FAR* pDoc);

	STDMETHOD(GetHostInfo)(DOCHOSTUIINFO FAR *pInfo);
	STDMETHOD(HideUI)(void);
	STDMETHOD(UpdateUI)(void);
	STDMETHOD(EnableModeless)(BOOL fEnable);
	STDMETHOD(OnDocWindowActivate)(BOOL fActivate);
	STDMETHOD(OnFrameWindowActivate)(BOOL fActivate);
	STDMETHOD(ResizeBorder)(LPCRECT prcBorder, IOleInPlaceUIWindow FAR* pUIWindow, BOOL fFrameWindow);
	STDMETHOD(TranslateAccelerator)(LPMSG lpMsg, const GUID FAR* pguidCmdGroup, DWORD nCmdID);
	STDMETHOD(GetOptionKeyPath)(LPOLESTR FAR* pchKey, DWORD dw);
	STDMETHOD(GetDropTarget)(IDropTarget* pDropTarget, IDropTarget** ppDropTarget);
	STDMETHOD(TranslateUrl)(DWORD dwTranslate, OLECHAR* pchURLIn, OLECHAR** ppchURLOut);
	STDMETHOD(FilterDataObject)(IDataObject* pDO, IDataObject** ppDORet);
	STDMETHOD(GetExternal)(IDispatch** ppDispatch);

	// IOleCommandTarget
public:
	STDMETHOD (QueryStatus)(
		/* [unique][in] */ const GUID *pguidCmdGroup,
		/* [in] */ ULONG cCmds,
		/* [out][in][size_is] */ OLECMD prgCmds[  ],
		/* [unique][out][in] */ OLECMDTEXT *pCmdText);
	STDMETHOD(Exec)(
		/*[in]*/ const GUID *pguidCmdGroup,
		/*[in]*/ DWORD nCmdID,
		/*[in]*/ DWORD nCmdExecOpt,
		/*[in]*/ VARIANTARG *pvaIn,
		/*[in,out]*/ VARIANTARG *pvaOut);

	//IDocHostShowUI
public:
	STDMETHOD(ShowMessage)(HWND hwnd,
		LPOLESTR lpstrText,
		LPOLESTR lpstrCaption,
		DWORD dwType,
		LPOLESTR lpstrHelpFile,
		DWORD dwHelpContext,
		LRESULT *plResult);

	STDMETHOD(ShowHelp)(
		HWND hwnd,
		LPOLESTR pszHelpFile,
		UINT uCommand,
		DWORD dwData,
		POINT ptMouse,
		IDispatch *pDispatchObjectHit);

private:
	CComQIPtr<IDocHostUIHandler, &IID_IDocHostUIHandler> m_spDefaultDocHostUIHandler;
	CComQIPtr<IOleCommandTarget, &IID_IOleCommandTarget> m_spDefaultOleCommandTarget;
};
