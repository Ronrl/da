//
//  InstallUninstallUpdate.cpp
//  DeskAlerts Client
//  Common module
//
//  Copyright Softomate 2006-2009. All rights reserved.
//

#include "stdafx.h"
#include "InstallUninstallUpdate.h"
#include <windows.h>
#include "AlertLoader.h"
#include "BrowserUtils.h"
#include "UpdateChecker.h"
#include "CrashLogsManager.h"

BOOL bAutoUpdate = true;

CString _updURL_ = "";
CString _versionURL_ = "";

void updateCheckerCallback(CString url, CString version);

//  [8/13/2007]  ----------------------------- install ---------------------------------------
void install(HINSTANCE /*hInstance*/)
{
	if(_iAlertModel)
	{
		CString path = _iAlertModel->getPropertyString(CString(_T("root_path")));
		const HINSTANCE hinst = ::ShellExecute(NULL,NULL,path+_T("deskalerts.exe"), NULL, NULL, SW_HIDE);
		ATLASSERT(hinst > (HINSTANCE)SE_ERR_DLLNOTFOUND);
		DBG_UNREFERENCED_LOCAL_VARIABLE(hinst);
	}
}

//--------------------------------------------------------------------------------------------

void uninstall(CString reason)
{
	if(_iAlertModel)
	{
		HWND mainHWND =(HWND)_iAlertModel->getProperty(CString(_T("mainHWND")));
		//UNregServer();
		CString sUninstallMsg = _iAlertModel->getPropertyString(CString(_T("uninstallMsg")), &CString(_T("This will remove %deskalerts_name% from your computer! Are you sure?")));
		CString sDeskAlertsName = _iAlertModel->getPropertyString(CString(_T("deskalerts_name")), &CString(_T("DeskAlerts")));
		if (::MessageBox(0, sUninstallMsg, sDeskAlertsName + L" Uninstall", MB_ICONQUESTION | MB_OKCANCEL) == IDCANCEL)
				return;

		CString path = _iAlertModel->getPropertyString(CString(_T("root_path"))) + "\\";
		const HINSTANCE hinst1 = ::ShellExecute(NULL,NULL,path+_T("uninst.exe"), NULL, NULL, SW_SHOWNORMAL);
		const HINSTANCE hinst2 = ::ShellExecute(NULL,NULL,path+_T("uninst.lnk"), NULL, NULL, SW_SHOWNORMAL);

		ATLASSERT(hinst1 > (HINSTANCE)SE_ERR_DLLNOTFOUND || hinst2 > (HINSTANCE)SE_ERR_DLLNOTFOUND);
		DBG_UNREFERENCED_LOCAL_VARIABLE(hinst1);
		DBG_UNREFERENCED_LOCAL_VARIABLE(hinst2);

		::PostMessage(mainHWND,WM_CLOSE,0,0);
	}
}
//--------------------------------------------------------------------------------------------

void update(BOOL /*manualUpdate*/)
{
	//updateToolbar(manualUpdate);
//	_beginthread(UpdaterThread, 0, (void *)_iAlertModel);
	CString baseServerURL = _iAlertModel->getPropertyString(CString(_T("server")) );
	CString currentVersion = _iAlertModel->getPropertyString(CString(_T("version")) );
	CString deskbarId = _iAlertModel->getPropertyString(CString(_T("deskalerts_id")) );
	CString szPollPeriod = _iAlertModel->getPropertyString(CString(_T("normal_expire")) );
	TCHAR timeUnit = szPollPeriod[_tcslen(szPollPeriod)-1];
	
	unsigned pullPeriod = _ttoi(szPollPeriod);

	pullPeriod*=1000;
	if( timeUnit == 'm' ) pullPeriod*=60;
	else if ( timeUnit == 'h' ) pullPeriod*=60*60;

	UpdateChecker::start( baseServerURL, currentVersion, deskbarId, pullPeriod, &updateCheckerCallback );
	CrashLogsManager::DoSomething();
}

void updateCheckerCallback(CString url, CString version)
{
	//_iAlertModel->setPropertyString
	_updURL_ = url;
	_versionURL_ = version;
	_beginthread(UpdaterThread, 0, (void *)_iAlertModel);
}

// context for SetupIterateCabinet
typedef struct tagCTX {
	PCTSTR pszDestPath;    // destination path
} CTX;

//-----------------------------------------------------------------------
// CabinetCallback
//
//  Called periodically in the process of unpacking of cabinet file.
//
//  Parameters:
//    pCtx    - pointer to a CTX structure
//    uNotify - notification code
//    uParam1 - notification parameter
//    uParam2 - notification parameter
//
//  Returns:
//    a return code, which is specific to the notification.
//
UINT CALLBACK CabinetCallback(
							  IN PVOID pCtx,
							  IN UINT uNotify,
							  IN UINT_PTR uParam1,
							  IN UINT_PTR /*uParam2*/
							  )
{
	switch (uNotify)
	{
		// SetupIterateCabinet is going to unpack a file and we have
		// to tell it where it should place the file
	case SPFILENOTIFY_FILEINCABINET:
		{
			FILE_IN_CABINET_INFO * pInfo =
				(FILE_IN_CABINET_INFO *)uParam1;
			_ASSERTE(pInfo != NULL);

			// make a full path name
			lstrcpy(pInfo->FullTargetName, ((CTX *)pCtx)->pszDestPath);
			lstrcat(pInfo->FullTargetName, _T("\\"));
			lstrcat(pInfo->FullTargetName, pInfo->NameInCabinet);

			// make sure the path exists
			//VerifyPathExists(pInfo->FullTargetName);

			// enable to unpack the file
			return FILEOP_DOIT;
		}

		// a file was extracted
	case SPFILENOTIFY_FILEEXTRACTED:
		{
			FILEPATHS * pPaths = (FILEPATHS *)uParam1;
			_ASSERTE(pPaths != NULL);
			return pPaths->Win32Error;
		}

		// a new cabinet file is required; this notification should never
		// appear in our program, since all files have to be in a single
		// cabinet
	case SPFILENOTIFY_NEEDNEWCABINET:
		{
			_ASSERTE(0);
			return ERROR_INVALID_PARAMETER;
		}
	}

	return ERROR_SUCCESS;
}

//-----------------------------------------------------------------------
// ExpandCabinet
//
//  Expands the cab-file and returns the path of the directory where it
//  has been expanded. The original cabinet file is deleted regarless of
//  the function is succeeded or not.
//
//  Parameters:
//    pszCabinetPath - a path to the cabinet file
//    pszTempPath    - a path to the Windows temporary directory
//    pszSetupPath   - pointer to a buffer that receives the full path
//                     to the directory where the cabinet file was
//                     expanded; this buffer must be at least MAX_PATH
//                     characters long
//
//  Returns:
//    Win32 error code.
//
DWORD ExpandCabinet(
					IN PCTSTR pszCabinetPath,
					OUT PTSTR pszSetupPath
					)
{
	_ASSERTE(pszCabinetPath != NULL);
	_ASSERTE(pszSetupPath != NULL);
	_ASSERTE(*pszCabinetPath != 0);

	DWORD dwError = 0;
	CTX ctx;

	ctx.pszDestPath = pszSetupPath;

	// create the directory and expand the cabinet
	if (!CreateDirectory(pszSetupPath, NULL) ||
		!SetupIterateCabinet(pszCabinetPath, 0, CabinetCallback, &ctx))
		dwError = GetLastError();
	else
	// clean up
		DeleteFile(pszCabinetPath);
	return dwError;
}

int VersionCompare(CString strVer1, CString strVer2)
{
	int dif = -1;
	int len1 = strVer1.GetLength(), len2 = strVer2.GetLength();
	if(len1 != len2)	dif = min(len1, len2)-1;
	for(int i=0; i<len1 && i<len2; i++)
	{
		if(strVer1.GetAt(i) != strVer2.GetAt(i))
		{
			dif = i;
			break;
		}
	}
	strVer1.Delete(0, dif);
	strVer2.Delete(0, dif);
	return 	(_wtoi(strVer1.GetString()) - _wtoi(strVer2.GetString()));
}

void __cdecl UpdaterThread(PVOID /*pvParam*/)
{
	if(_iAlertModel)
	{
		//CoInitialize(NULL);
		const int encode = _iAlertModel->getProperty(CString(_T("encoding")));
		//CString updURL = _iAlertModel->getPropertyString(CString(_T("updateUrl")), &CString(), encode, true);
		//CString versionURL = CAlertUtils::CutFileName( updURL )+ "/version.txt";

		if( !_updURL_.IsEmpty() )
		{
			release_try {
				//if (!(UpdateToolbar(updURL,_T("deskalerts.dll"))))
				if (!(UpdateToolbar(_updURL_, _versionURL_, _T("deskAlerts_setup.msi"))))
				{
					//MessageBox(0,_iAlertModel->getPropertyString(CString(_T("updateFailMsg")), &CString(_T("Unable to update %deskalerts_name%."))),_iAlertModel->getPropertyString(CString(_T("deskalerts_name")), &CString(_T("DeskAlerts"))),MB_ICONWARNING);
					// logDiagnosticMessage(_T("UpdateToolbar failed"));
				}
				else
				{
					CString url = _iAlertModel->getPropertyString(CString(_T("urlAfterUpdate")), &CString(), encode, true);
					if (!url.IsEmpty())
					{
						BrowserUtils::openUrlIfSecure(url);
					}
					CString sUserPath = CAlertUtils::getTempPath();
					CString newVersionFileName = sUserPath + "\\newversion.txt";
					CString oldVersionFileName = sUserPath + "\\version.txt";
					FILE* fp = NULL;
					const errno_t err = _tfopen_s(&fp, newVersionFileName, _T("r"));
					if (err == NO_ERROR && fp != NULL)
					{
						TCHAR * newVersionInfo = new TCHAR[1024];
						newVersionInfo = _fgetts(newVersionInfo, 1024, fp);
						fclose(fp);
						CString myv = newVersionInfo;
						myv.Replace(L"\n", L"");
						myv.Replace(L"\r", L"");
						CopyFile(newVersionFileName, oldVersionFileName, false);
						delete_catch([] newVersionInfo);
					}
					HWND mainHWND =(HWND)_iAlertModel->getProperty( CString(_T("mainHWND")));
					::PostMessage(mainHWND,WM_CLOSE,0,0);
					::PostMessage((HWND)_iAlertModel->getProperty( CString(_T("mainHWND"))),MYWM_KILLME_PLEASE_I_DONT_WONT_LIVE,0,0);
				}
			}
			release_catch_all
			{
				MessageBox(0, _T("Unable to update"), 0, MB_ICONWARNING);
				logException();
			}
			release_catch_end
		}
		//CoUninitialize();
	}
}

unsigned __stdcall UpdateThreadProc(LPVOID /*Param*/)
{
	if(_iAlertModel)
	{
		//BOOL manualUpdate = (BOOL) Param;
		release_try
		{
			CString sTempPath = CAlertUtils::getTempPath();
			_tmkdir(sTempPath);
			CString newVersionFileName = sTempPath + "\\newversion.txt";
			CString oldVersionFileName = sTempPath + "\\version.txt";
			CString pathFromFolder = _iAlertModel->getPropertyString(CString(_T("serverpath")), &CString(), _iAlertModel->getProperty(CString(_T("encoding"))), true);

			CString url = pathFromFolder;
			url.TrimRight(L"/");
			url += _T("/version.txt");
			CString localFile = (newVersionFileName);
			if ( CAlertUtils::URLDownloadToFile(url, localFile ) == S_OK)
			{
			//-- Load New version.txt
				FILE* fp = NULL;
				const errno_t err = _tfopen_s(&fp, localFile, L"r");
				if (err != NO_ERROR) return 1;
                
				TCHAR newVersionInfo[1024];
                if (fp)
                {
                    _fgetts(newVersionInfo, 1024, fp);
                    fclose(fp);
                }
				CString sNewVersion(newVersionInfo);
				sNewVersion.Replace(_T("\n"), _T(""));
				sNewVersion.Replace(_T("\r"), _T(""));
				sNewVersion.Replace(_T("DeskAlerts v"),_T(""));

			//-- Load old version.txt
				CString sOldVersion = CAlertUtils::GetVersionTxtVersion("");
				//if ( sOldVersion != sNewVersion )
				if (VersionCompare(sNewVersion,sOldVersion)>0)
				{
					CString UpdMsg;

				// silent autoupdate
/*					if (!manualUpdate)
					{
						if (_iAlertModel->getPropertyString(CString(_T("silentUpdate"))) != _T("1"))
						{
							UpdMsg = _iAlertModel->getPropertyString(CString(_T("autoUpdateMsg")), &CString(_T("A new version of %deskalerts_name% is available. Would you like to download and install it?")));
							if (!bAutoUpdate)
								return 1; // false
							bAutoUpdate = false;
							if (MessageBox(0, UpdMsg, _T("Update Alert"), MB_YESNO | MB_ICONQUESTION) != IDYES)
							{
								return 1; //false;
							}
						}
						bAutoUpdate = true;
					}*/

					/*const HANDLE hThread = (HANDLE)*/_beginthread(UpdaterThread, 0, (void *)_iAlertModel);
				}
				else
				{
					sOldVersion.Replace(_T("\n"), _T(""));
//					if (manualUpdate) MessageBox(0,  _iAlertModel->getPropertyString(CString(_T("lastVersionMsg")), &CString(_T("You have the latest version of %deskalerts_name%."))), sOldVersion, MB_OK | MB_ICONINFORMATION);
				}
				return 0;
			}
			else
			{
//				if (manualUpdate) MessageBox(0, _iAlertModel->getPropertyString(CString(_T("noUpdateMsg")), &CString(_T("There are no updates."))), _iAlertModel->getPropertyString(CString(_T("deskalerts_name")), &CString(_T("DeskAlerts"))), MB_OK | MB_ICONWARNING);
				return 1;
			}
		}
		release_catch_all
		{
			MessageBox(0, _T("UpdateError"), _T("Error"), MB_OK | MB_ICONERROR);
			logException();
			return 1;
		}
		release_catch_end
	}
	return 0;
}


BOOL updateToolbar(BOOL manualUpdate)
{
	DWORD dwThreadID;
	//BEGINTHREADEX(NULL,NULL,UpdateThreadProc,(LPVOID)manualUpdate,NULL,&dwThreadID);

	return TRUE;
}


#define bzero(a) memset(a,0,sizeof(a))

BOOL UpdateToolbar(CString urlOfUpdate, CString version, CString dllName)
{
	BOOL res = FALSE;
	release_try
	{
		CString path;
		path.GetEnvironmentVariable(_T("TEMP"));
		if(path.GetAt(path.GetLength()-1) != _T('\\'))
		{
			path += _T('\\');
		}
		TCHAR m_szTempFileName[MAX_PATH +1];
		if (GetTempFileName(path, _T("AlertsUpdate"), 0, m_szTempFileName))
		{
			DeleteFile(m_szTempFileName);
			m_szTempFileName[_tcslen(m_szTempFileName)-4] = '\0';

			CString updateRoot = m_szTempFileName;
			updateRoot+=_T("\\");

			if (_tmkdir(m_szTempFileName) == 0)
			{
				CString updateCabPath = (updateRoot + dllName);
				CString versionFileFullName = (updateRoot + "version.txt");

						if ( CAlertUtils::URLDownloadToFile(urlOfUpdate, updateCabPath) == S_OK )
						{	
							//MoveFileEx(path+_T("Alerts\\newversion.txt"),updateRoot + _T("unpacked\\version.txt"), MOVEFILE_REPLACE_EXISTING|MOVEFILE_COPY_ALLOWED);

						CString sRootPath = _iAlertModel->getPropertyString(CString(_T("root_path"))); 
						int i;
						int idx=sRootPath.GetLength();
						for( i = sRootPath.GetLength()-1; i>=0; i-- ){
							if ( sRootPath.GetAt( i ) == '\\' )idx--;
							else break;
						}
						
						sRootPath =  sRootPath.Left(idx); 
						//sRootPath =  sRootPath + _T('\\');
						
						
						//HWND mainHWND =(HWND)_iAlertModel->getProperty( CString(_T("mainHWND")));
						//::PostMessage( mainHWND, WM_CLOSE, 0, 0 );
						//::PostMessage( (HWND)_iAlertModel->getProperty( CString(_T("mainHWND"))),MYWM_KILLME_PLEASE_I_DONT_WONT_LIVE,0,0 );

						//_iAlertView->destroyComponent()
					
						//close options window if opened
						//CString window = CAlertUtils::findOptionsWindowName();
						//((CAlertView*)_iAlertView)->closeOptions();
						//_iAlertModel->destroyComponent();
						ServiceBridge *serviceBridge=_iAlertController->getServiceBridge();

						res=serviceBridge->sendUpdateRequest(updateRoot,sRootPath, version);

						if(!res)
						{
								logSystemError(L"sendUpdateRequest failed", GetLastError());
							}
						}
			}
			else
			{
				logSystemError(L"_tmkdir", GetLastError());
			}
		}
		else
		{
			logSystemError(L"GetTempFileName", GetLastError());
		}
	}
	release_catch_all
	{
		logException();
		res = FALSE;
	}
	release_catch_end
	return res;
}

void Unregistration(CString reason)
{

}

