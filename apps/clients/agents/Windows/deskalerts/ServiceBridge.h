#pragma once

#include "ServiceDataManager.h"

#define WM_SERVICE_REQUEST (WM_APP+112)


struct showData;

class ServiceBridge : public CFrameWindowImpl<ServiceBridge>
{
public:
	BEGIN_MSG_MAP(CMultiWindowCommand)
		MESSAGE_HANDLER(WM_SERVICE_REQUEST, OnServiceRequest)
	END_MSG_MAP()
	LRESULT OnServiceRequest(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	static ServiceBridge* Instance();
	void FreeInstance();
	BOOL sendUpdateRequest(CStringW &updateRoot, CStringW &sRootPath, CStringW &version);
	BOOL sendRegistryRequest(PREGISTRYDATA regData);
	BOOL sendShowWindowRequest(CString &vieName, CString param, shared_ptr<showData> ex);
	BOOL sendCloseWindowRequest(INT64 alertId);
	void Start();
	void StartRecieveMessages();
	static DWORD WINAPI ServiceBridge::recieveThread(LPVOID pv);
	BOOL checkService();
protected:
	ServiceBridge(void);
	~ServiceBridge(void);
private:
	static ServiceBridge* _self;
	static volatile LONG _refcount;
	BOOL sendRequestToService(ServiceDataManager* data);
	static DWORD WINAPI listenCloseEventThread(LPVOID pv);
};
