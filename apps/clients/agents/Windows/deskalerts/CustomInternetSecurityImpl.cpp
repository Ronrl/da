// CustomInternetSecurityImpl.cpp : Implementation of CCustomInternetSecurityImpl

#include "stdafx.h"
#include "CustomInternetSecurityImpl.h"
#include <wininet.h>

//ICustomInternetSecurityImpl
STDMETHODIMP CCustomInternetSecurityImpl::SetParams(
	/* [in] */ HWND hwnd,
	/* [in] */ BOOL skipHttpsCheck)
{
	m_phwnd = hwnd;
	m_skipHttpsCheck = skipHttpsCheck;
	return S_OK;
}

//IInternetSecurityManager
STDMETHODIMP CCustomInternetSecurityImpl::SetSecuritySite(
	/* [unique][in] */ __RPC__in_opt IInternetSecurityMgrSite* /*pSite*/)
{
	return INET_E_DEFAULT_ACTION;
}

STDMETHODIMP CCustomInternetSecurityImpl::GetSecuritySite(
	/* [out] */ __RPC__deref_out_opt IInternetSecurityMgrSite** /*ppSite*/)
{
	return INET_E_DEFAULT_ACTION;
}

STDMETHODIMP CCustomInternetSecurityImpl::MapUrlToZone(
	/* [in] */ __RPC__in LPCWSTR /*pwszUrl*/,
	/* [out] */ __RPC__out DWORD *pdwZone,
	/* [in] */ DWORD /*dwFlags*/)
{
	*pdwZone = URLZONE_PREDEFINED_MIN;//URLZONE_TRUSTED;
	return S_OK;
}

STDMETHODIMP CCustomInternetSecurityImpl::GetSecurityId(
	/* [in] */ __RPC__in LPCWSTR /*pwszUrl*/,
	/* [size_is][out] */ __RPC__out_ecount_full(*pcbSecurityId) BYTE* /*pbSecurityId*/,
	/* [out][in] */ __RPC__inout DWORD* /*pcbSecurityId*/,
	/* [in] */ DWORD_PTR /*dwReserved*/)
{
	return INET_E_DEFAULT_ACTION;
}

STDMETHODIMP CCustomInternetSecurityImpl::ProcessUrlAction(
	/* [in] */ __RPC__in LPCWSTR /*pwszUrl*/,
	/* [in] */ DWORD /*dwAction*/,
	/* [size_is][out] */ __RPC__out_ecount_full(cbPolicy) BYTE *pPolicy,
	/* [in] */ DWORD /*cbPolicy*/,
	/* [in] */ __RPC__in BYTE* /*pContext*/,
	/* [in] */ DWORD /*cbContext*/,
	/* [in] */ DWORD /*dwFlags*/,
	/* [in] */ DWORD /*dwReserved*/)
{
	if(pPolicy != NULL)
	{
		*pPolicy = URLPOLICY_ALLOW;
		return S_OK;
	}
	return INET_E_DEFAULT_ACTION;
}

STDMETHODIMP CCustomInternetSecurityImpl::QueryCustomPolicy(
	/* [in] */ __RPC__in LPCWSTR /*pwszUrl*/,
	/* [in] */ __RPC__in REFGUID /*guidKey*/,
	/* [size_is][size_is][out] */ __RPC__deref_out_ecount_full_opt(*pcbPolicy) BYTE** /*ppPolicy*/,
	/* [out] */ __RPC__out DWORD* /*pcbPolicy*/,
	/* [in] */ __RPC__in BYTE* /*pContext*/,
	/* [in] */ DWORD /*cbContext*/,
	/* [in] */ DWORD /*dwReserved*/)
{
	return INET_E_DEFAULT_ACTION;
}

STDMETHODIMP CCustomInternetSecurityImpl::SetZoneMapping(
	/* [in] */ DWORD /*dwZone*/,
	/* [in] */ __RPC__in LPCWSTR /*lpszPattern*/,
	/* [in] */ DWORD /*dwFlags*/)
{
	return INET_E_DEFAULT_ACTION;
}

STDMETHODIMP CCustomInternetSecurityImpl::GetZoneMappings(
	/* [in] */ DWORD /*dwZone*/,
	/* [out] */ __RPC__deref_out_opt IEnumString** /*ppenumString*/,
	/* [in] */ DWORD /*dwFlags*/)
{
	return INET_E_DEFAULT_ACTION;
}

//IOleCommandTarget
STDMETHODIMP CCustomInternetSecurityImpl::QueryStatus(
									  /* [unique][in] */ const GUID* /*pguidCmdGroup*/,
									  /* [in] */ ULONG /*cCmds*/,
									  /* [out][in][size_is] */ OLECMD /*prgCmds*/ [],
									  /* [unique][out][in] */ OLECMDTEXT* /*pCmdText*/)
{
	return E_NOTIMPL;
}

STDMETHODIMP CCustomInternetSecurityImpl::Exec(
							   /*[in]*/ const GUID *pguidCmdGroup,
							   /*[in]*/ DWORD nCmdID,
							   /*[in]*/ DWORD /*nCmdExecOpt*/,
							   /*[in]*/ VARIANT* /*pvaIn*/,
							   /*[in,out]*/ VARIANT *pvaOut)
{
	HRESULT hr = E_NOTIMPL;
	if(pguidCmdGroup && IsEqualGUID(*pguidCmdGroup, CGID_DocHostCommandHandler))
	{
		switch(nCmdID)
		{
			case OLECMDID_SHOWSCRIPTERROR:
			{
				pvaOut->vt = VT_BOOL;
				// Continue running scripts on the page.
				pvaOut->boolVal = VARIANT_TRUE;
				hr = S_OK;
			}
		}
	}
	return hr;
}

//IWindowForBindingUI
STDMETHODIMP CCustomInternetSecurityImpl::GetWindow(
	/* [in] */ REFGUID /*rguidReason*/,
	/* [out] */ HWND *phwnd)
{
	HRESULT hr = E_INVALIDARG;
	if(phwnd)
	{
		*phwnd = m_phwnd;
		hr = S_OK;
	}
	return hr;
}

//IHttpSecurity
STDMETHODIMP CCustomInternetSecurityImpl::OnSecurityProblem(
	/* [in] */ DWORD dwProblem)
{
	HRESULT res = S_OK;
	switch(dwProblem)
	{
		case ERROR_INTERNET_SEC_CERT_DATE_INVALID:
		case ERROR_INTERNET_SEC_CERT_CN_INVALID:
		case ERROR_INTERNET_INVALID_CA:
		case ERROR_INTERNET_SEC_INVALID_CERT:
		case ERROR_INTERNET_SEC_CERT_ERRORS:
		case ERROR_INTERNET_SEC_CERT_REV_FAILED:
		case ERROR_INTERNET_SEC_CERT_NO_REV:
		case ERROR_INTERNET_SEC_CERT_REVOKED:
			res = m_skipHttpsCheck ? S_OK : S_FALSE;
			break;
		case ERROR_INTERNET_CLIENT_AUTH_CERT_NEEDED:
			res = S_FALSE;
			break;
		case ERROR_HTTP_REDIRECT_NEEDS_CONFIRMATION:
			res = RPC_E_RETRY;
			break;
	}
	return res;
}
