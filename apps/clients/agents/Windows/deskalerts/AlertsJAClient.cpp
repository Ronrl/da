#include "StdAfx.h"
#include "AlertUtils.h"
#include "AlertsJAClient.h"

//
// Responses
//

// rd_user
	ResponseData * rd_user::fromJSON(const rapidjson::Value& doc) 
	{
		//_data = doc["data"].GetString();    
		//_authorization = doc["Authorization"].GetString();
		//return (ResponseData *)this;
		if (!doc.HasMember("data")) return NULL;

		return fromJSONValue(doc["data"]); 
	}

	ResponseData * rd_user::fromJSONValue(const rapidjson::Value& doc) 
	{
        _authorization = doc["authorization"].GetString();
        return (ResponseData *)this;    
	}

// rd_alerts
	CString rd_alerts::getData(){ return _data; }
	ResponseData * rd_alerts::fromJSON(const rapidjson::Value& doc) 
	{
		_data = doc["data"].GetString();    
		_screensaversHash = doc["screensaversHash"].GetString();
		_wallpapersHash = doc["wallpapersHash"].GetString();
		_lockscreensHash = doc["lockscreensHash"].GetString();
		return (ResponseData *)this;
	}


	CString rd_wallpaper_id_list::getData(){ return _data; }
	ResponseData * rd_wallpaper_id_list::fromJSON(const rapidjson::Value& doc) 
	{
		_data = doc["data"].GetString();    
		return (ResponseData *)this;
	}

	CString rd_lockscreen_id_list::getData() { return _data; }
	ResponseData * rd_lockscreen_id_list::fromJSON(const rapidjson::Value& doc)
	{
		_data = doc["data"].GetString();
		return (ResponseData *)this;
	}

	CString rd_screensaver_id_list::getData(){ return _data; }
	ResponseData * rd_screensaver_id_list::fromJSON(const rapidjson::Value& doc) 
	{
		_data = doc["data"].GetString();    
		return (ResponseData *)this;
	}


// "userAlerts": [
//            {
//                "alertId": 1024,
//                "htmlMarkup": "<html><head><meta http-equiv='content-type' content='text/html; charset = utf-8'/></head><body style='margin: 5px; overflow: auto;'><p><span style='color: #333333; font-family: Arial, sans-serif;'><p>Test body</p><!-- html_title = 'Test title' --></span></p></body></html>"
//            }
//        ],
//        "wallpapersHash": "624D82924638812B15441CB6E2369F1A",
//        "screensaversHash": "C4CA4238A0B923820DCC509A6F75849B"



using namespace rapidjson;
//
// Requests
//

//
const CString JAReq_User::getURL()
{
	return _authorizationType == Domain? _T("api/v1/security/token/aduser"):_T("api/v1/security/token/user");
}

std::string JAReq_User::toJSON() 
{
	rapidjson::StringBuffer s;
	rapidjson::Writer<StringBuffer> writer(s);
    writer.StartObject();
    writer.Key("userName");
    //writer.String((LPCSTR)(_bstr_t)_userName);
	CStringA szUserName = CT2A(_userName,  CP_UTF8);
	writer.String( szUserName );

    writer.Key("computerName");
    CStringA szCompName = CT2A(_computerName, CP_UTF8);
    writer.String(szCompName);

	writer.Key("ip");
    writer.String((LPCSTR)(_bstr_t)_ip);

	writer.Key("clientVersion");
    writer.String((LPCSTR)(_bstr_t)_clientVersion);

	writer.Key("deskbarId");
	writer.String((LPCSTR)(_bstr_t)_deskbarId);
	
	writer.Key( "deviceType" );
	writer.Uint( _deviceType );

	writer.Key("pollPeriod");
	writer.String((LPCSTR)(_bstr_t)_iAlertModel->getPropertyString(CString(_T("normal_expire"))));

	writer.Key("computerDomainName");
	writer.String((LPCSTR)(_bstr_t)_compDomainName);

	if( _authorizationType == Domain )
	{
		writer.Key("domainName");
		writer.String((LPCSTR)(_bstr_t)_domainName);
		writer.Key("md5Password");
		writer.String((LPCSTR)(_bstr_t)(""));
	}else{
		writer.Key("domainName");
		writer.String((LPCSTR)(_bstr_t)(""));
		writer.Key("md5Password");
		writer.String((LPCSTR)(_bstr_t)_md5Password);
	}
	writer.EndObject();
    if (writer.IsComplete()) {
        try {
			return std::string(s.GetString(), s.GetLength());
        }
        catch ( std::exception& e ) {
            //logging->error("Exception Encountered parsing JSON");
            //logging->error(e.what());
        }
    }
 //   else {
        return "";
 //   }
}

////////////////////////////////////////
/// api/v1/user/contents?timezone=
std::string JAReq_Alerts::toJSON() 
{
	rapidjson::StringBuffer s;
	rapidjson::Writer<StringBuffer> writer(s);
    writer.StartObject();

//    writer.Key("authorization");
//    writer.String((LPCSTR)(_bstr_t)_authorization);
	writer.Key("localTime");
    writer.String((LPCSTR)(_bstr_t)_localTime);
	
	writer.EndObject();
    if (writer.IsComplete()) {
        try {
            return s.GetString();
        }
        catch (std::exception& e) {
            //logging->error("Exception Encountered parsing JSON");
            //logging->error(e.what());
        }
    }
 //   else {
        return "";
 //   }
}
const CString JAReq_Alerts::getURL()
{
#ifdef DISABLE_API_V1_USER_ALERTS
    CString retStr;
    LONG TimeZoneMinutes = 0;
    TIME_ZONE_INFORMATION TimeZoneInformation;
    if (TIME_ZONE_ID_INVALID != GetTimeZoneInformation(
        &TimeZoneInformation))
    {
        TimeZoneMinutes = -TimeZoneInformation.Bias;
    }
    retStr.Format(_T("api/v1/user/contents?timezone=%d"), TimeZoneMinutes * 60);
    setVERB(CAlertUtils::requestVerb::GETverb);
    return retStr;
#else
    bool isDisable = _iAlertController->getJAClient()->DISABLE == _iAlertController->getJAClient()->getAPIv1UserContentStatus();
    bool isTimeToNewTry = false;
    if (isDisable)
    {
        isTimeToNewTry = clock() - _iAlertModel->getProperty(CString(_T("TimeLastCheckNewContentsAPI"))) > CHECK_API_V1_USER_CONTENTS_PERIOD;
    }
    if ( ! isDisable || isTimeToNewTry )
    {
        CString retStr;
        LONG TimeZoneMinutes = 0;
        TIME_ZONE_INFORMATION TimeZoneInformation;
        if (TIME_ZONE_ID_INVALID != GetTimeZoneInformation(
            &TimeZoneInformation))
        {
            TimeZoneMinutes = -TimeZoneInformation.Bias;
        }
        retStr.Format(_T("api/v1/user/contents?timezone=%d"), TimeZoneMinutes * 60);
        _iAlertModel->setProperty(CString(_T("TimeLastCheckNewContentsAPI")), clock());
        setVERB(CAlertUtils::requestVerb::GETverb);
        return retStr;
    }
    else
    {
        setVERB(CAlertUtils::requestVerb::POSTverb);
        return _T("api/v1/user/alerts");
    }
#endif
}

////////////////////////////////////////
/// api/v2/user/contents?timezone=
std::string JAReq_getUserContents_v2::toJSON(){ return ""; }
const CString JAReq_getUserContents_v2::getURL()
{
    CString retStr;
    LONG TimeZoneMinutes = 0;
    TIME_ZONE_INFORMATION TimeZoneInformation;
    if (TIME_ZONE_ID_INVALID != GetTimeZoneInformation(
        &TimeZoneInformation))
    {
        TimeZoneMinutes = -TimeZoneInformation.Bias;
    }
    retStr.Format(_T("api/v2/user/contents?timezone=%d"), TimeZoneMinutes * 60);
    setVERB(CAlertUtils::requestVerb::GETverb);
    return retStr;

}
///////////////////////////////////////////////////////
/// api/v2/content/history?from=&count=&query=&type=&order=
std::string JAReq_getUserHistory_v2::toJSON() { return ""; }
const CString JAReq_getUserHistory_v2::getURL() 
{
    CString retStr;
    retStr.Format(_T("api/v2/content/history?from=%d&count=%d&query=%s%s&order=%s"),
                                            _from,  _count,  _query,getTypesString(), _order);
    setVERB(CAlertUtils::requestVerb::GETverb);
    return retStr;
}
const CString JAReq_getUserHistory_v2::getTypesString()
{
    CString retStr;
    for each (auto type in _types)
    {
        retStr.Format(_T("%s&type=%d"), retStr, type);
    }
    return retStr;
}
///////////////////////////////////////////////////////
/// api/v2/user/content/tikers
std::string JAReq_getUserTikers_v2::toJSON() { return ""; }
const CString JAReq_getUserTikers_v2::getURL()
{
    setVERB(CAlertUtils::requestVerb::GETverb);
    return _T("api/v1/user/content/tikers");
}
/////////JAReq_Received methods

void JAReq_Received::array2JsonWriter(const char * arrayName, rapidjson::Writer<StringBuffer> *writer, std::vector<AlertConfirmation>  alertsArray)
{
	writer->Key(arrayName);
	writer->StartArray();
	int size = alertsArray.capacity() ;
	for( int i = 0; i < size; i++ )
	{
		writer->StartObject();
		writer->Key("id");
		writer->Int64( alertsArray[i].alertId );
		writer->Key("receivedStatus");
		writer->Int( alertsArray[i].status );
		writer->EndObject();
	}
	writer->EndArray();
}

std::string JAReq_Received::toJSON() 
{
	rapidjson::StringBuffer s;
	rapidjson::Writer<StringBuffer> writer(s);
    writer.StartObject();
	{
		array2JsonWriter("alerts", &writer, _alerts );
		array2JsonWriter("screensavers", &writer, _screensavers );
		array2JsonWriter("wallpapers", &writer, _wallpapers );
		array2JsonWriter("lockscreens", &writer, _lockscreens);
	}

	writer.EndObject();

    if (writer.IsComplete()) {
        try {
            return s.GetString();
        }
        catch (std::exception& e) {
            //logging->error("Exception Encountered parsing JSON");
            //logging->error(e.what());
        }
    }
 //   else {
        return "";
 //   }
}

const CString JAReq_Received::getURL()
{
	return _T("api/v1/user/alerts/received");
}

void JAReq_Received::AddAlertId(long alertId, int status)
{
	AlertConfirmation ac;
	ac.alertId = alertId;
	ac.status = status;

	_alerts.push_back(ac);
}

void JAReq_Received::AddScreensaverId(long alertId, int status)
{
	AlertConfirmation ac;
	ac.alertId = alertId;
	ac.status = status;

	_screensavers.push_back(ac);
}

void JAReq_Received::AddWallpaperId(long alertId, int status)
{
	AlertConfirmation ac;
	ac.alertId = alertId;
	ac.status = status;

	_wallpapers.push_back(ac);
}

void JAReq_Received::AddLockscreenId(long alertId, int status)
{
	AlertConfirmation ac;
	ac.alertId = alertId;
	ac.status = status;

	_lockscreens.push_back(ac);
}

/////////JAReq_TerminateReceived methods

std::string JAReq_TerminateReceived::toJSON()
{
	return "";
}

const CString JAReq_TerminateReceived::getURL()
{
	CString url;
	url.Format(_T("api/v1/user/alerts/terminate/%ld"), _alertId);
	return url;
}

/////////JAReq_AlertRead methods

std::string JAReq_AlertRead::toJSON() 
{
	rapidjson::StringBuffer s;
	rapidjson::Writer<StringBuffer> writer(s);
    writer.StartObject();
    writer.Key("alertId");
	writer.Uint( _alertId );
    
//	writer.Key("readStatus");
//	writer.Uint( _status );

	writer.EndObject();
    if (writer.IsComplete()) {
        try {
			return std::string(s.GetString(), s.GetLength());
        }
        catch (std::exception& e) {
            //logging->error("Exception Encountered parsing JSON");
            //logging->error(e.what());
        }
    }
    return "";
}

const CString JAReq_AlertRead::getURL()
{
	return _T("api/v1/user/alerts/read");
}

/////////JAReq_CrashLogs methods

std::string JAReq_CrashLogs::toJSON()
{
	return "";
}

const CString JAReq_CrashLogs::getURL()
{
	return _T("api/v1/error/client");
}

// JAClient

bool CJAClient::SendRequest( JARequest *request, BOOL isToFile, LPCTSTR szFileName, BOOL isToString, string *resStringName,  BOOL *isUTF8, BOOL isRecursionBlocked, int *httpRequestStatus)
{
    CString furl = "";
    bool result = true;
    CString urlAPI = request->getURL();
    furl.Format(_T("%s/%s"), _isbackupServer ? _backupServer : _server, urlAPI);
    std::string  strRequest = request->toJSON();
    CString csReq = CString(strRequest.c_str());
    CString headers = _T("Authorization: ") + getAuthorization() +
        _T("\nContent-Type: application/json; charset=utf-8 ") +
        _T("\n") + getheader_IfNoneMatch();
        
    CString resCString;
    HRESULT hres = CAlertUtils::URLDownloadToFile(furl, isToFile? szFileName:NULL, &resCString, 0, 0, &headers, &csReq , request->getVERB(), httpRequestStatus);
    
    #ifndef DISABLE_API_V1_USER_ALERTS
    BOOL isItWasNewContentAPI = urlAPI.Find(_T("api/v1/user/contents")) != -1;
    if (isItWasNewContentAPI)
    {
        setAPIv1UserContentStatus(hres == S_OK ? ENABLE : DISABLE);
    }
    #endif
    if (hres != S_OK)
    {
        return false;
    }

    Document doc;
    //const char * chBody;
    //Распарсить из файла
    //CString requestFilePath = CAlertUtils::getTempFileName(_T("JARequest"),0);
    
    string data = "";
    //////////////////////
    // if to string
    if (isToString && resStringName)
    {
        data = string(CT2CA(resCString));
        *resStringName = data;
    }
    //////////////////////
    // if to file (old resolution)
    if (isToFile && szFileName)
    {
        CT2A szFP(szFileName);
        data = CAlertUtils::readFile2String(szFP);
    }

	if (data.length() == 0) return false;
    

    doc.Parse(data.c_str());
    BOOL isBackup = false;
    if (doc.HasMember("status")) {
        CString status = doc["status"].GetString();
        if (status == "Success")
        {
            result = true;
        }
        else if (status == "NA")
        {
            if (CJAClient::Connect())
            {
                /////////////////////////////////////
                /// so, if all new received tokens call "NA" for `getcontents`, stackoverflow will be
                if (isRecursionBlocked) return true;
                else
                {
                    isRecursionBlocked = true;
                    result = CJAClient::SendRequest(request, isToFile, szFileName, isToString, resStringName, isUTF8, isRecursionBlocked);
                }
            }
             
        }
        else
        {
            result = false;
        }
    }
    
    return result;
}

bool CJAClient::Connect()
{
	ResponseObj roUser;
	rd_user urd;

	_authType = (_adEnabled && !_domainName.IsEmpty() && _domainName != "" ) ||
                (_synFreeAdEnabled && !_domainName.IsEmpty() && _domainName != "")
                ? Domain : UserName;

	JAReq_User request( _authType, _userName, _computerName, _domainName,
		_ip, _clientVersion, _deskbarId, _md5Password, _compDomainName );

	CString furl;
	furl.Format(_T("%s/%s"), _isbackupServer ? _backupServer: _server, request.getURL() );

	std::string req = request.toJSON();

	char outBuffer[1025];
	outBuffer[0] = '0';
	CString requestResult="";

	CString csReq = CString( req.c_str() );
	CString headers = _T("\nContent-Type: application/json; charset=utf-8 ");
	HRESULT hDwnldres = CAlertUtils::URLDownloadToFile( furl, NULL, &requestResult, 0, 0, &headers, &csReq );
    /////////////////////////////////////
    /// switch server
	if( hDwnldres != S_OK )
	{
        ///////////////////////////////
        /// if url in config is emtry
		if(_isbackupServer && _backupServer.IsEmpty() ) return false;

        furl.Format(_T("%s/%s"), _isbackupServer ? _backupServer : _server, request.getURL());
		hDwnldres = CAlertUtils::URLDownloadToFile( furl, NULL, &requestResult, 0, 0, &headers, &csReq );
		if( hDwnldres != S_OK  ) return false;
	}

	rapidjson::Document doc;
	doc.Parse(CT2A(requestResult) );

	roUser.fromJSON(doc, (ResponseData *)&urd);

	if( roUser.getStatus()!=_T("Success") )return false;
	//CAlertLoader::m_urd.setAuthorization ( roUser.getResponseData() );
	_authorization = ((rd_user *)roUser.getResponseData())->getAuthorization();
	return true;
}

bool CJAClient_v2::SendRequest(JARequest_v2* request, BOOL isToFile, LPCTSTR szFileName, BOOL isToString, string* resStringName, BOOL* isUTF8, BOOL isRecursionBlocked, int* httpRequestStatus)
{
    CString furl = "";
    bool result = true;
    CString urlAPI = request->getURL();
    furl.Format(_T("%s/%s"), _isbackupServer ? _backupServer : _server, urlAPI);
    std::string  strRequest = request->toJSON();
    CString csReq = CString(strRequest.c_str());
    CString headers = _T("Authorization: ") + getAuthorization() +
        _T("\nContent-Type: application/json; charset=utf-8 ") +
        _T("\n") + getheader_IfNoneMatch();

    CString resCString;
    HRESULT hres = CAlertUtils::URLDownloadToFile(furl, isToFile ? szFileName : NULL, &resCString, 0, 0, &headers, &csReq, request->getVERB(), httpRequestStatus);

#ifndef DISABLE_API_V1_USER_ALERTS
    BOOL isItWasNewContentAPI = urlAPI.Find(_T("api/v1/user/contents")) != -1;
    if (isItWasNewContentAPI)
    {
        setAPIv1UserContentStatus(hres == S_OK ? ENABLE : DISABLE);
    }
#endif
    if (hres != S_OK)
    {
        return false;
    }

    Document doc;
    //const char * chBody;
    //Распарсить из файла
    //CString requestFilePath = CAlertUtils::getTempFileName(_T("JARequest"),0);

    string data = "";
    //////////////////////
    // if to string
    if (isToString && resStringName)
    {
        data = string(CT2CA(resCString));
        *resStringName = data;
    }
    //////////////////////
    // if to file (old resolution)
    if (isToFile && szFileName)
    {
        CT2A szFP(szFileName);
        data = CAlertUtils::readFile2String(szFP);
    }

    if (data.length() == 0) return false;


    doc.Parse(data.c_str());
    BOOL isBackup = false;
    if (doc.HasMember("status")) {
        CString status = doc["status"].GetString();
        if (status == "Success")
        {
            result = true;
        }
        else if (status == "NA")
        {
            if (CJAClient_v2::Connect())
            {
                /////////////////////////////////////
                /// so, if all new received tokens call "NA" for `getcontents`, stackoverflow will be
                if (isRecursionBlocked) return true;
                else
                {
                    isRecursionBlocked = true;
                    result = CJAClient_v2::SendRequest(request, isToFile, szFileName, isToString, resStringName, isUTF8, isRecursionBlocked);
                }
            }

        }
        else
        {
            result = false;
        }
    }

    return result;
}

bool CJAClient_v2::Connect()
{
    ResponseObj roUser;
    rd_user urd;

    _authType = (_adEnabled && !_domainName.IsEmpty() && _domainName != "") ||
        (_synFreeAdEnabled && !_domainName.IsEmpty() && _domainName != "")
        ? Domain : UserName;

    JAReq_User request(_authType, _userName, _computerName, _domainName,
        _ip, _clientVersion, _deskbarId, _md5Password, _compDomainName);

    CString furl;
    furl.Format(_T("%s/%s"), _isbackupServer ? _backupServer : _server, request.getURL());

    std::string req = request.toJSON();

    char outBuffer[1025];
    outBuffer[0] = '0';
    CString requestResult = "";

    CString csReq = CString(req.c_str());
    CString headers = _T("\nContent-Type: application/json; charset=utf-8 ");
    HRESULT hDwnldres = CAlertUtils::URLDownloadToFile(furl, NULL, &requestResult, 0, 0, &headers, &csReq);
    /////////////////////////////////////
    /// switch server
    if (hDwnldres != S_OK)
    {
        ///////////////////////////////
        /// if url in config is emtry
        if (_isbackupServer && _backupServer.IsEmpty()) return false;

        furl.Format(_T("%s/%s"), _isbackupServer ? _backupServer : _server, request.getURL());
        hDwnldres = CAlertUtils::URLDownloadToFile(furl, NULL, &requestResult, 0, 0, &headers, &csReq);
        if (hDwnldres != S_OK) return false;
    }

    rapidjson::Document doc;
    doc.Parse(CT2A(requestResult));

    roUser.fromJSON(doc, (ResponseData*)&urd);

    if (roUser.getStatus() != _T("Success"))return false;
    //CAlertLoader::m_urd.setAuthorization ( roUser.getResponseData() );
    _authorization = ((rd_user*)roUser.getResponseData())->getAuthorization();
    return true;
}
