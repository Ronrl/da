#pragma once
#include "ServiceDataManager.h"


class ShowWindowDataManager: public ServiceDataManager
{
private:
	shared_ptr<showData> m_windowData;
	CString m_param;
	CString m_viewName;
public:
	ShowWindowDataManager(void);
	~ShowWindowDataManager(void);
	DWORD serialize(IStream* pStream);
	void setData(CString viewName, CString param,shared_ptr<showData> windowData);
	RequestDataType getDataType();
};
