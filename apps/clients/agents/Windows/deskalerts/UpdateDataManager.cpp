#include "stdafx.h"
#include "UpdateDataManager.h"

UpdateDataManager::UpdateDataManager(void)
{
}

UpdateDataManager::~UpdateDataManager(void)
{
}

void UpdateDataManager::setData(CStringW &updateRoot,CStringW &sRootPath, CStringW &version)
{
	m_updateRoot=updateRoot;
	m_sRootPath=sRootPath;
	m_version = version;
}

RequestDataType UpdateDataManager::getDataType()
{
	return REQUESTDATATYPE_UPDATE;
}

DWORD UpdateDataManager::serialize(IStream* pStream)
{
	UPDATEDATA data;
	CStringW unpackedPath = (m_updateRoot+_T("unpacked"));
	CStringW exePath = (m_sRootPath+_T("\\")+_T("deskalerts.exe"));

	CStringW::CopyChars(data.m_baseName, sizeof(data.m_baseName)/sizeof(TCHAR), m_updateRoot, m_updateRoot.GetLength()+1);
	CStringW::CopyChars(data.m_dest, sizeof(data.m_dest)/sizeof(TCHAR), m_sRootPath, m_sRootPath.GetLength()+1);
	CStringW::CopyChars(data.m_exeName, sizeof(data.m_exeName)/sizeof(TCHAR), exePath, exePath.GetLength()+1);
	CStringW::CopyChars(data.m_src, sizeof(data.m_src)/sizeof(TCHAR), unpackedPath, unpackedPath.GetLength()+1);
	CStringW::CopyChars(data.m_version, sizeof(data.m_version)/sizeof(TCHAR), m_version, m_version.GetLength()+1);
	ProcessIdToSessionId(GetCurrentProcessId(),&data.sid);

	pStream->Write(&data, sizeof(UPDATEDATA), NULL);

	return 0;
}