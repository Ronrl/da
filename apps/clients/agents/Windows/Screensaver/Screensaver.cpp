// BallFusion.cpp : Defines the entry point for the application.
//
#include "stdafx.h"
#include "windows.h"
#include "ScrnSave.h"
#include "resource.h"
#include "ScreensaverAlert.h"
#include "DatabaseManager.h"
#include "Utils.h"
#include "HTMLView.h"
#include <shellapi.h>
#include <sys\stat.h>
#include <fstream>
#include <time.h>
#include <Wtsapi32.h>
#include "BrowserUtils.h"

#define SS_TIMER (WM_APP+601)
#define SS_EXEC_LINK (WM_APP+602)


struct Monitor {
	CHTMLView *win;
};

CComModule _Module;
std::vector<ScreensaverAlert> alerts;
std::vector<ScreensaverAlert>::iterator curIt;
std::vector<Monitor> monitors;

bool isFirstMouseMove = false;
int mouseX;
int mouseY;

DWORD desktopThreadId;


DWORD WINAPI desktopThread(LPVOID /*pv*/)
{
	DWORD res = 1;
	release_try
	{
		res = SetThreadDesktop(OpenDesktop(_T("default"), 0, FALSE, DESKTOP_CREATEWINDOW));
		
		MSG msg;
		while(GetMessage(&msg, NULL, 0, 0) > 0) //important to check > 0, see MSDN
		{
			release_try
			{
				if (msg.message == SS_EXEC_LINK)
				{
					CString *url = (CString*)msg.wParam;

					PROCESS_INFORMATION pi;
					STARTUPINFO si;
					HANDLE hToken;
					HANDLE hDuplicateToken;

					memset(&si,0,sizeof(si));
					CString fullDesktopName = CString(_T("winsta0\\default"));
					si.lpDesktop = fullDesktopName.GetBuffer();
					si.cb = sizeof(si);

					OpenProcessToken(GetCurrentProcess(), TOKEN_QUERY|TOKEN_DUPLICATE|TOKEN_ADJUST_SESSIONID, &hToken);

					DuplicateTokenEx(hToken, MAXIMUM_ALLOWED, 0, SecurityImpersonation, TokenPrimary, &hDuplicateToken);
					CloseHandle(hToken);

					CString command = BrowserUtils::urlSecure(*url);
					if(!command.IsEmpty())
					{
						command = _T("cmd.exe /c \"start ") + command + _T("\"");
						CreateProcessAsUser(hDuplicateToken, NULL, command.GetBuffer(), 0, 0, False, CREATE_NO_WINDOW, NULL, NULL, &si, &pi);
					}

					CloseHandle(hDuplicateToken);

					delete_catch(url);
					::PostMessage((HWND)msg.lParam,WM_DESTROY,0,0);
				}
				else
				{
					TranslateMessage(&msg);
					DispatchMessage(&msg);
				}
			}
			release_end_try
		}
	}
	release_end_try
		
	return res;
}

void destroySSWindow(CHTMLView *win)
{
	release_try
	{
		if (win)
		{
			win->destroyComponent();
			delete_catch(win);
			win = NULL;
		}
	}
	release_end_try
}

void destroySSWindows(HWND /*hWnd*/)
{
	release_try
	{
		std::vector<Monitor>::iterator monitorsIt = monitors.begin();
		while (monitorsIt != monitors.end())
		{
			destroySSWindow((*monitorsIt).win);
			monitorsIt++;
		}
		monitors.clear();
	}
	release_end_try
}

CHTMLView* createSSWindow(HWND hWnd, CRect rect)
{
	CHTMLView *win = new CHTMLView();

	win->HTMLCreate(hWnd, rect, _T("about:blank"),
		WS_CHILD|WS_VISIBLE, 0, FALSE);
	win->MoveWindow(rect, TRUE);
	return win;
}


void navigateSSWindow(CHTMLView *win,CString url)
{
	release_try
	{
		CComVariant vtEmpty;
		HRESULT resNavigate = win->getWebBrowser()->Navigate2(&CComVariant(url), &vtEmpty, &vtEmpty, &vtEmpty, &vtEmpty);
		DeskAlertsAssert(SUCCEEDED(resNavigate));
		HANDLE pHandle = GetCurrentProcess();
		SetProcessWorkingSetSize(pHandle,(SIZE_T) -1,(SIZE_T) -1);
		CloseHandle(pHandle);
	}
	release_end_try
}

void navigateSSWindows(CString url)
{
	release_try
	{
		std::vector<Monitor>::iterator monitorsIt = monitors.begin();
		while (monitorsIt != monitors.end())
		{
			navigateSSWindow((*monitorsIt).win,url);
			monitorsIt++;
		}
	}
	release_end_try
}

volatile LONG destroyMessagePosted = FALSE;

void onMouseMove(HWND hWnd, LPARAM /*lParam*/)
{
	release_try
	{
		if (fChildPreview) return;
		if (!isFirstMouseMove)
		{
			POINT p;
			if (GetCursorPos(&p))
			{
				mouseX = p.x;
				mouseY = p.y;
			}
			isFirstMouseMove =true;
		}
		else
		{
			POINT p;
			if (GetCursorPos(&p))
			{
				if (mouseX != p.x || mouseY!=p.y)
				{
					if (!destroyMessagePosted)
					{
						InterlockedExchange(&destroyMessagePosted, TRUE);
						PostMessage(hWnd,WM_DESTROY,NULL,NULL);
					}
				}
			}
			
		}
	}
	release_end_try
}

void onKeyPress(HWND hWnd, LPARAM /*lParam*/)
{
	release_try
	{
		if (fChildPreview) return;
		if (!destroyMessagePosted)
		{
			InterlockedExchange(&destroyMessagePosted, TRUE);
			PostMessage(hWnd,WM_DESTROY,NULL,NULL);
		}
	}
	release_end_try
}

void onMouseKeyPress(HWND hWnd, LPARAM /*lParam*/)
{
	release_try
	{
		if (fChildPreview) return;
		if (!destroyMessagePosted)
		{
			InterlockedExchange(&destroyMessagePosted, TRUE);
			PostMessage(hWnd,WM_DESTROY,NULL,NULL);
		}
	}
	release_end_try
}

BOOL CALLBACK paintEnumProc(
	HMONITOR /*hMonitor*/,	// handle to display monitor
	HDC /*hdc1*/,	// handle to monitor DC
	LPRECT lprcMonitor,	// monitor intersection rectangle
	LPARAM data	// data
)
{
	release_try
	{
		RECT rc = *lprcMonitor;
		// you have the rect which has coordinates of the monitor

		Monitor monitor;
		monitor.win = createSSWindow((HWND)data,rc);
		monitors.push_back(monitor);
	}
	release_end_try

	return TRUE;
}

LRESULT WINAPI ScreenSaverProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{	
	LRESULT res = S_OK;

	release_try
	{
		CComVariant vtEmpty;
		switch(message)
		{
		case WM_CREATE:
			{
				if (fChildPreview)
				{
					CRect rect;
					GetClientRect(hWnd , &rect);
					Monitor monitor;
					monitor.win = createSSWindow(hWnd,rect);
					monitors.push_back(monitor);
					TCHAR szModulePath[MAX_PATH] = _T("");
					GetModuleFileName(hMainInstance, szModulePath, MAX_PATH);
					CString strUrl = _T("res://");
					strUrl += szModulePath;
					strUrl += _T("/logo.htm");
					navigateSSWindows(strUrl);
				}
				else
				{
					CreateThread(NULL, 0, desktopThread,0, 0, &desktopThreadId);

					DatabaseManager m_dbm(Utils::databasePath());

					m_dbm.getScreensaverAlerts(alerts);

					int i=0;
					std::vector<ScreensaverAlert>::iterator rit = alerts.begin();
					while(rit != alerts.end())
					{
						(*rit).m_fileName = Utils::getTempFileName(_T("DeskAlerts_Screensaver"),i++);
						HANDLE alertFile = CreateFile((*rit).m_fileName, GENERIC_WRITE, 0, NULL,CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
						DWORD dwBytesWritten;
						CStringA data = CT2A((*rit).m_html, CP_UTF8);
						if(!(*rit).m_utf8  )data = CT2A((*rit).m_html);
						WriteFile(alertFile, data, data.GetLength(), &dwBytesWritten, 0);
						CloseHandle(alertFile);
						rit++;
					}
				}
			}
		break;
		case WM_TIMER:
			if (wParam == SS_TIMER)
			{
				curIt++;
				if (curIt == alerts.end()) curIt = alerts.begin();
				navigateSSWindows((*curIt).m_fileName);	
				SetTimer(hWnd,SS_TIMER,((*curIt).m_autoclose == 0) ? USER_TIMER_MAXIMUM :(*curIt).m_autoclose*1000,NULL);
			}
		break;

		case WM_OPENLINK:
			{
				CComBSTR bstrUrl;
				VARIANT *url = (VARIANT*)wParam;
				if(url && url->vt == VT_BSTR)
					bstrUrl = url->bstrVal;

				if(bstrUrl.Length() != 0)
				{
					CString *urlstr = new CString(bstrUrl);
					PostThreadMessage(desktopThreadId,SS_EXEC_LINK,(WPARAM)urlstr,(LPARAM)hWnd);
				}
				return 1;
			}
		break;

		case WM_FORWARDMSG:
		{
			const LPMSG msg = (LPMSG)lParam;
			PostMessage(hWnd,msg->message,msg->wParam,msg->lParam);
			return S_OK;
		}
		break;
		case WM_LBUTTONDOWN:
		case WM_MBUTTONDOWN:
		case WM_RBUTTONDOWN:
			onMouseKeyPress(hWnd,lParam);
			return S_OK;
		break;
		case WM_KEYDOWN:
		case WM_KEYUP:
			onKeyPress(hWnd,lParam);
			return S_OK;
		break;
		case WM_MOUSEMOVE:
			onMouseMove(hWnd,lParam);
			return S_OK;
		break;
		case WM_DESTROY:
			Utils::RecursiveDeleteDirectory(Utils::getTempPath());
			return S_OK;
		break;
		case WM_PAINT:
			{
				PAINTSTRUCT ps = {0};
				HDC hdcE = BeginPaint(hWnd, &ps );

				if (!fChildPreview)
				{
					EnumDisplayMonitors(hdcE,NULL, paintEnumProc, (LPARAM)hWnd);
				}
				
				EndPaint(hWnd, &ps);

				curIt = alerts.begin();

				if (!fChildPreview)
				{
					if(curIt != alerts.end())
					{
						navigateSSWindows((*curIt).m_fileName);
						SetTimer(hWnd,SS_TIMER,((*curIt).m_autoclose == 0) ? USER_TIMER_MAXIMUM :(*curIt).m_autoclose*1000,NULL);
					}
					else
					{
						TCHAR szModulePath[MAX_PATH] = _T("");
						GetModuleFileName(hMainInstance, szModulePath, MAX_PATH);
						CString strUrl = _T("res://");
						strUrl += szModulePath;
						strUrl += _T("/black.htm");
						navigateSSWindows(strUrl);
					}
				}
			}
			break;
		}
		release_try
		{
			res = DefScreenSaverProc(hWnd, message, wParam, lParam);
		}
		release_catch_all
		{
			res = S_FALSE;
		}
		release_catch_end
	}
	release_end_try

	return res;
}


BOOL WINAPI ScreenSaverConfigureDialog(HWND hDlg, UINT message, WPARAM /*wParam*/, LPARAM /*lParam*/)
{
	release_try
	{
		switch(message)
		{
		case WM_INITDIALOG:
			MessageBox(hDlg,_T("This screen saver has no options that can be configured"),_T("DeskAlerts Screensaver"),MB_ICONINFORMATION);
			EndDialog(hDlg,1);
			return TRUE;
		break;
		case WM_CLOSE:
			EndDialog(hDlg,1);
		break;
		}
	}
	release_end_try
	return FALSE;
}

BOOL WINAPI RegisterDialogClasses(HANDLE /*hInst*/)
{
	return TRUE;
}
