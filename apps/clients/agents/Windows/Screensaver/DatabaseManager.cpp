#include "stdafx.h"
#include "DatabaseManager.h"
#include "ScreensaverAlert.h"

DatabaseManager::DatabaseManager(CString path)
{
	m_Database = NULL;
	m_Database = openDatabase(path);
}

DatabaseManager::~DatabaseManager(void)
{
	CloseDatabase();
}

void DatabaseManager::CloseDatabase()
{
	ATLASSERT(m_Database);
	sqlite3_close(m_Database);
}

sqlite3* DatabaseManager::openDatabase(CString path)
{
	sqlite3* database = NULL;
	int ret = sqlite3_open16((void*)(wchar_t*)path.GetString(), &database);
	CString a = sqlite3_errmsg(database);
	if (SQLITE_OK != ret)
	{
		database = NULL;
	}
	return database;
}

HRESULT DatabaseManager::getScreensaverAlerts(std::vector<ScreensaverAlert> &alerts)
{
	if (m_Database)
	{
		CString req =_T("SELECT alert, autoclose, utf8 FROM history WHERE class=2 ORDER BY id DESC");
		
		sqlite3_stmt* stmt;

		ATLASSERT(m_Database);
		int ret = sqlite3_prepare_v2(m_Database, CT2A(req, CP_UTF8), -1, &stmt, 0);
		if (ret == SQLITE_OK)
		{
			while (sqlite3_step(stmt) == SQLITE_ROW)
			{
				ScreensaverAlert alert;

				alert.m_html = CA2T((char*)sqlite3_column_text(stmt, 0), CP_UTF8);
				alert.m_autoclose = sqlite3_column_int(stmt, 1);
				alert.m_utf8 = sqlite3_column_int(stmt, 2);
				alerts.push_back(alert);
			}
		}
		sqlite3_finalize(stmt);
	}
	return S_OK;
}

