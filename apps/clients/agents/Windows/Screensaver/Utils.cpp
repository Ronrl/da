#include "stdafx.h"
#include "Utils.h"
#include <sys\stat.h>
#include <io.h>

CString sRegistryKey = "ALERTS00001";
CString sProfileName = "DeskAlerts";

CString Utils::GetRegistryKeyName()
{
	CString registryKeyName;
	registryKeyName = (LPCTSTR) sRegistryKey;
	return registryKeyName;
}

CString Utils::allUsersAppDataPath(BOOL env)
{
	CString allUsersPath;
	if(env)
	{
		allUsersPath.GetEnvironmentVariable(_T("ALLUSERSPROFILE"));
	}
	if(!env || allUsersPath.IsEmpty())
	{
		TCHAR commonAppDataPath[MAX_PATH];
		SHGetFolderPath(NULL,CSIDL_COMMON_APPDATA,NULL,SHGFP_TYPE_CURRENT,commonAppDataPath);
		allUsersPath = CString(commonAppDataPath);
	}
	if(allUsersPath.GetAt(allUsersPath.GetLength()-1) != _T('\\'))
	{
		allUsersPath += _T('\\');
	}
	CString deskalertsPath = allUsersPath + _T("DeskAlerts");

	if (!env) CreateDirectory(deskalertsPath,NULL);
	return deskalertsPath;
}

CString Utils::allUsersAppDataProfilePath(BOOL env)
{
	CString allUsersPath = Utils::allUsersAppDataPath(env);
	CString path = allUsersPath + "\\" + Utils::GetRegistryKeyName();
	if (!env) CreateDirectory(path,NULL);
	return path;
}

CString Utils::databasePath(BOOL common)
{
	CString deskalertsPath = Utils::allUsersAppDataProfilePath();
	
	CString curUserPath;
	curUserPath.GetEnvironmentVariable(_T("USERNAME"));
	curUserPath = deskalertsPath + "\\" + curUserPath;
	
	CreateDirectory(curUserPath,NULL);
	CString dbPath = (common) ? deskalertsPath+"\\db.dat" : curUserPath+"\\db.dat";
	return dbPath;
}

CString Utils::getTempPath()
{
	CString tmp;
	tmp.GetEnvironmentVariable(_T("TEMP"));
	if(tmp.GetAt(tmp.GetLength()-1) != _T('\\'))
	{
		tmp += _T('\\');
	}
	tmp += _T("DeakAlertsScreensavers");
	return tmp;
}

CString Utils::getTempFileName(CString prefix,int i)
{
	CString tmp = Utils::getTempPath();
	CString path;
	if(!tmp.IsEmpty())
	{
		_tmkdir(tmp);
		srand(GetTickCount());
		path.Format(_T("%s\\%s_%x_%x_%x.html"), static_cast<LPCWSTR>(tmp), static_cast<LPCWSTR>(prefix),::GetCurrentThreadId(),i,rand());
	}
	return path;
}

BOOL Utils::RecursiveDeleteDirectory(const TCHAR *path)
{
	WIN32_FIND_DATA ffd;
	TCHAR tmp[MAX_PATH+1];
	_tcscpy_s(tmp, MAX_PATH+1, path);
	if(tmp[_tcslen(tmp)-1] != _T('\\'))
	{
		_tcscat_s(tmp, MAX_PATH+1, _T("\\"));
	}
	TCHAR findstr[MAX_PATH+1];
	_tcscpy_s(findstr, MAX_PATH+1, tmp);
	_tcscat_s(findstr, MAX_PATH+1, _T("*"));
	const HANDLE hFind = FindFirstFile(findstr, &ffd);
	if(INVALID_HANDLE_VALUE != hFind)
	{
		do
		{
			TCHAR filepath[MAX_PATH+1];
			_tcscpy_s(filepath, MAX_PATH+1, tmp);
			_tcscat_s(filepath, MAX_PATH+1, ffd.cFileName);
			if(ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			{
				if(_tcscmp(ffd.cFileName, _T(".")) && _tcscmp(ffd.cFileName, _T("..")))
					RecursiveDeleteDirectory(filepath);
			}
			else
			{
				if(ffd.dwFileAttributes & FILE_ATTRIBUTE_READONLY)
				{
					_tchmod(filepath, _S_IWRITE);
				}
				DeleteFile(filepath);
			}
		}
		while(FindNextFile(hFind, &ffd));
		FindClose(hFind);
	}
	tmp[_tcslen(tmp)-1] = _T('\0'); //replace \ char
	return RemoveDirectory(tmp);
}
