#pragma once

namespace Utils
{
	CString GetRegistryKeyName();
	CString allUsersAppDataPath(BOOL env = FALSE);
	CString allUsersAppDataProfilePath(BOOL env = FALSE);
	CString databasePath(BOOL common = FALSE);
	CString getTempPath();
	CString getTempFileName(CString prefix,int i);
	BOOL RecursiveDeleteDirectory(const TCHAR *path);
};
