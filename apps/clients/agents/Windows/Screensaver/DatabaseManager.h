#pragma once
#include "sqlite3.h"
#include <vector>

class ScreensaverAlert;

class DatabaseManager
{
public:
	DatabaseManager(CString path);
	~DatabaseManager(void);
	//HRESULT loadProperty(std::map<CString,CString> &prop);
	HRESULT getScreensaverAlerts(std::vector<ScreensaverAlert> &alerts);
private:
	sqlite3* m_Database;
	sqlite3* openDatabase(CString path);
	void CloseDatabase();
};
