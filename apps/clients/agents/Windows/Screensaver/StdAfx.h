// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__A9DB83DB_A9FD_11D0_BFD1_444553540000__INCLUDED_)
#define AFX_STDAFX_H__A9DB83DB_A9FD_11D0_BFD1_444553540000__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers

// ATL secured functions.
#define _SECURE_ATL 1

// STL errors handling.
// _SECURE_SCL=1 for enabled state, 0 for disabled.
// _SECURE_SCL_THROWS=1 for throwing exception, 0 for abnormal program termination.
#ifdef _SECURE_SCL
#undef _SECURE_SCL
#endif
#ifdef _SECURE_SCL_THROWS
#undef _SECURE_SCL_THROWS
#endif
//#define _SECURE_SCL 1
//#define _SECURE_SCL_THROWS 1

#ifndef release_try
#ifdef DEBUG
#define release_try {
#define release_end_try }
#define release_catch_all } if(0) {
#define release_catch_end }
#define release_catch_tolog }
#define delete_catch(x) delete x; //use debug and/or brain!
#else
#define release_try try {
#define release_end_try } catch(...) {}
#define release_catch_all } catch(...) {
#define release_catch_end }
#define release_catch_tolog } catch(...) {/*no log*/}
#define delete_catch(x) try{ delete x; } catch(...) {} //ha-ha-ha! we need to avoid stupid crashes.
#endif
#endif

#define _WTL_NO_CSTRING

#include <windows.h>
/*
#ifdef UNICODE // if the program is set to use Unicode,
#pragma comment(lib, "./ScrnSavW.lib") // Use Unicode version of scrnsave (is ScrnSavW)
#else // or if it is ANSI or Multi-Byte,
#pragma comment(lib, "./ScrnSave.lib") // Use ANSI version of scrnsave.
#endif // end of if
*/

#include <atlstr.h>

#include <windows.h>
#include <errors.h>
#include <process.h>

#include <atlbase.h>
#include <atlapp.h>

#include <atlhost.h>
#include <atlwin.h>
#include <atlctl.h>

#include <atlframe.h>
#include <atlctrls.h>
#include <atldlgs.h>


#include <atlmisc.h>

#include <atlutil.h>

extern volatile LONG g_bDebugMode;
extern inline DWORD WaitForSingleObjectWithMsgLoop(__in HANDLE hHandle, __in DWORD dwMilliseconds);

#endif // !defined(AFX_STDAFX_H__A9DB83DB_A9FD_11D0_BFD1_444553540000__INCLUDED_)
