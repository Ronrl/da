#pragma once

#include "stdafx.h"

CString CreateGUID();
CString FindGUIDinFile(CString FileName);

const bool cmd_cd(const CString dir);
const bool cmd_mkdir(const CString dir);
const bool cmd_rmdir(const CString dir);
const bool cmd_del(const CString from);
const bool cmd_copy(const CString from, const CString to);
const bool cmd_move(const CString from, const CString to);

const bool exec_cmd(const CString app_spawn);
const bool exec_cmd_Nt(const CString app_spawn);
const bool exec_cmd_notNt(const CString app_spawn);
const bool exec_external(const CString app_spawn);

const bool isNT();
void encode(char* str, long len);
void decode(char* str, long len);

class Utils
{
public:
	Utils(void);
	~Utils(void);
};
