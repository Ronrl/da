﻿// debConverter.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include "utils.h"
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

using namespace std;


string ToString(size_t sz) {
 
  stringstream ss;
 
  ss << sz;
 
  return ss.str();
}

string UpToLow(string str) {
    for (int i=0;i<strlen(str.c_str());i++) 
        if (str[i] >= 0x41 && str[i] <= 0x5A) 
            str[i] = str[i] + 0x20;
    return str;
}


void concatFiles(CString first,CString second)
{
	ifstream firstFile;
	firstFile.open(first);
	ifstream secondFile;
	secondFile.open(second);
	string firstStr="";
	string secondStr="";
	string tmpStr="";
	
	if (firstFile.is_open()) {
		while (!firstFile.eof()) 
		{
			getline(firstFile,tmpStr);
			if (firstStr!="")
			{
				 firstStr+="\n"+tmpStr;
			}
			else
			{
				firstStr+=tmpStr;
			}
			
		}
	}

	if (secondFile.is_open()) {
		while (!secondFile.eof()) 
		{
			getline(secondFile,tmpStr);
			if (secondStr!="")
			{
				 secondStr+="\n"+tmpStr;
			}
			else
			{
				secondStr+=tmpStr;
			}
			
		}
	}


	 firstFile.close();
	 secondFile.close();


		ofstream control_File_output;
		control_File_output.open(first);
		firstStr+="\n"+secondStr;
		control_File_output << firstStr;
		control_File_output.close();

}

void patchRepoPath(CString controlFilePath,CString confFilePath)
{
	//::MessageBox(0,0,0,0);
	ifstream control_File;
	control_File.open(controlFilePath);
	ifstream conf_File;
	conf_File.open(confFilePath);
	string updateControl="";
	string tmpStr;
	string fileStr;
	string updateString;
	int i;
	int j;
	
		if (conf_File.is_open()) {
		while (!conf_File.eof()) 
		{
			getline(conf_File,tmpStr);
			if (fileStr!="")
			{
				 fileStr+="\n"+tmpStr;
			}
			else
			{
				fileStr+=tmpStr;
			}
			
		}
	}


	i=fileStr.find("<PROPERTY id=\"server\" default=\"");
	i+=31;
	tmpStr=fileStr.substr(i);
	i=tmpStr.find_first_of('\"');
	updateString=tmpStr.substr(0,i);
	
	i=fileStr.find("<PROPERTY id=\"serverpath\" default=\"%server%");
	i+=43;
	tmpStr=fileStr.substr(i);
	i=tmpStr.find_first_of('\"');
	updateString+=tmpStr.substr(0,i);
	updateString="deb "+updateString+"update/ softomate office";


	control_File.seekg( 0, std::ios_base::end );
	tmpStr.resize( control_File.tellg() );
	control_File.seekg( 0, std::ios_base::beg );

control_File.read( (char*)tmpStr.data(), tmpStr.size() );
i= tmpStr.find("%repoString%");
tmpStr.replace(i,12,updateString);


control_File.close();
conf_File.close();
		ofstream control_File_output;
		control_File_output.open(controlFilePath);
		control_File_output << tmpStr;
		control_File_output.close();

}


void createUpdateControlFile(CString controlFilePath,CString updateControlFilePath,string updatePath)
{
	ifstream control_File;
	control_File.open(controlFilePath);
	string updateControl="";
	string tmpStr;
		if (control_File.is_open()) {
		while (!control_File.eof()) 
		{
			getline(control_File,tmpStr);
			int i=tmpStr.find("Maintainer:");
			if (i>=0)
			{
				tmpStr+="\nFilename: "+updatePath;
			}
			
			if (updateControl!="")
			{
				 updateControl+="\n"+tmpStr;
			}
			else
			{
				updateControl+=tmpStr;
			}
			
		}
	}

		control_File.close();
		ofstream control_File_output;
		control_File_output.open(updateControlFilePath);
		control_File_output << updateControl;
		control_File_output.close();

}

void addHashInUpdateControl(CString updateControlFilePath,CString packagePath)
{
	size_t fileSize = 0;
	
	ifstream infile(packagePath);
	if(infile.is_open())
	{
		infile.seekg(0, ios::end ); // move to end of file
		fileSize = infile.tellg();
	}
	
	exec_cmd("sha256 -f \""+packagePath+"\" > hash");
	exec_cmd("sha1 -f \""+packagePath+"\" >> hash");
	exec_cmd("md5 \""+packagePath+"\" >> hash");
	//::MessageBox(0,0,0,0);

	string tmpStr;
	ifstream hash_File;
	hash_File.open("hash");

	string sha256String;
	string sha1String;
	string md5String;
	int j;
	if (hash_File.is_open()) 
	{
	//	while (!hash_File.eof()) 
	//	{
			getline(hash_File,tmpStr);
			j=tmpStr.find_last_of('|');
			sha256String=tmpStr.substr(j+2);
			getline(hash_File,tmpStr);
			j=tmpStr.find_last_of('|');
			sha1String=tmpStr.substr(j+2);
			getline(hash_File,tmpStr);
			j=tmpStr.find_first_of(' ');
			md5String=tmpStr.substr(0,j);	
		//	getline(hash_File,tmpStr);
	//	}
	}

	hash_File.close();

	ifstream control_File;
	control_File.open(updateControlFilePath);
	string updateControl="";
	
		if (control_File.is_open()) {
		while (!control_File.eof()) 
		{
			getline(control_File,tmpStr);
			int i=tmpStr.find("Filename:");
			if (i>=0)
			{
				tmpStr+="\nSize: "+ToString(fileSize);
				tmpStr+="\nSHA256: "+UpToLow(sha256String);
				tmpStr+="\nSHA1: "+UpToLow(sha1String);
				tmpStr+="\nMD5sum: "+UpToLow(md5String);
			}
			
			if (updateControl!="")
			{
				 updateControl+="\n"+tmpStr;
			}
			else
			{
				updateControl+=tmpStr;
			}
			
		}
	}
		control_File.close();
		ofstream control_File_output;
		control_File_output.open(updateControlFilePath);
		control_File_output << updateControl;
		control_File_output.close();
	
}

void patchVersion(string versionStr, CString filePath)
{
	
	ifstream control_File;
	control_File.open(filePath);
	string control="";
	string tmpStr;
		if (control_File.is_open()) {
		while (!control_File.eof()) 
		{
			getline(control_File,tmpStr);
			int i=tmpStr.find("Version:");
			if (i>=0)
			{
				tmpStr="Version: " + versionStr;
			}
			
			if (control!="")
			{
				 control+="\n"+tmpStr;
			}
			else
			{
				control+=tmpStr;
			}
			
		}
	}

		control_File.close();
		ofstream control_File_output;
		control_File_output.open(filePath);
		control_File_output << control;
		control_File_output.close();
}


int _tmain(int argc, _TCHAR* argv[])
{
	//::MessageBox(0,0,0,0);
	srand(GetTickCount());
	const TCHAR *arcName = _T("Archive.pax.gz");
	CString temp, tempPath, appPath, tb_name, tb_bundle, tb_cab = _T("deskalert.cab");

	appPath = argv[0];
	if (argc > 1) 
	{
		tb_cab = argv[1];
	}

	//get name of toolbar binary
	int pos = tb_cab.ReverseFind(_T('\\'));
	tb_name = tb_cab.Mid(pos+1);
	tb_name.Replace(_T(".cab"), _T(""));
	tb_name.Replace(_T(".CAB"), _T(""));
	//tb_name = _T("SoftomateToolbar");

	//name of bundle (don't change)
	//tb_bundle = tb_name;
//	tb_bundle = _T("DeskAlerts");
//	tb_bundle += _T(".app");

	//get application path
	pos = appPath.ReverseFind(_T('\\'));
	appPath = appPath.Left(pos+1);

	if(appPath.IsEmpty())
	{
		::GetModuleFileName(0, appPath.GetBuffer(MAX_PATH), MAX_PATH);
		appPath.ReleaseBuffer();
		pos = appPath.ReverseFind(_T('\\'));
		appPath = appPath.Left(pos+1);
	}
	if(appPath.IsEmpty())
	{
		::GetCurrentDirectory(MAX_PATH, appPath.GetBuffer(MAX_PATH));
		appPath.ReleaseBuffer();
	}
	if(appPath.Right(1) != _T('\\'))
	{
		appPath += _T('\\');
	}
	cmd_cd(appPath); //for be care



	
	if (argc > 1)
	{
	 	if (_tcscmp(argv[1], _T("hash")) == 0)
		{
			addHashInUpdateControl(appPath+"\\updateControls\\i386\\Packages",appPath+"\\update\\update\\DeskAlerts_i386.deb");
			addHashInUpdateControl(appPath+"\\updateControls\\amd64\\Packages",appPath+"\\update\\update\\DeskAlerts_amd64.deb");
			addHashInUpdateControl(appPath+"\\updateControls\\Packages",appPath+"\\update\\update\\DeskAlertsRepo.deb");
			concatFiles(appPath+"\\updateControls\\i386\\Packages",appPath+"\\updateControls\\Packages");
			concatFiles(appPath+"\\updateControls\\amd64\\Packages",appPath+"\\updateControls\\Packages");
			return 0;
		}
	
	}


	



	//get temp path
	::GetTempPath(MAX_PATH, tempPath.GetBuffer(MAX_PATH));
	tempPath.ReleaseBuffer();

	if(tempPath.Right(1) != _T('\\'))
	{
		tempPath += _T('\\');
	}

	//create temp folder
	temp.Format(_T("deskalerts_deb_converter_%d"), rand()); //without \ for rmdir
	tempPath += temp;

	//make temp dir
	cmd_mkdir(tempPath);

	//extract the cab file of a project
	cmd_mkdir(tempPath + _T("\\deskalerts.cab"));
	temp = _T("expand.exe -F:* \"");
	temp += tb_cab;
	temp += _T("\" \"");
	temp += tempPath;
	temp += _T("\\deskalerts.cab\"");
	exec_external(temp);
	cmd_del("deskalert.cab");
	cmd_copy(tempPath+"\\deskalerts.cab\\*",appPath+"\\usr\\share\\Softomate\\DeskAlerts\\html\\");
	
 	cmd_rmdir(tempPath);

	ifstream versionFile;
	versionFile.open(appPath+"\\usr\\share\\Softomate\\DeskAlerts\\html\\version.txt");
	string versionStr;
	if (versionFile.is_open()) {
		while (!versionFile.eof()) 
		{
			getline(versionFile,versionStr);
			int i=versionStr.find_first_of('v');
			versionStr = versionStr.substr(i+1);
			break;
		}
	}
	versionFile.close();
	
	patchVersion(versionStr,appPath+"\\controls\\control_i386");
	patchVersion(versionStr,appPath+"\\controls\\control_amd64");
	patchVersion(versionStr,appPath+"\\controls\\control_repo");
	patchRepoPath(appPath+"\\scripts\\postinst_deskalertsRepo",appPath+"\\usr\\share\\Softomate\\DeskAlerts\\html\\conf.xml");
	createUpdateControlFile(appPath+"\\controls\\control_i386",appPath+"\\updateControls\\i386\\Packages","update/DeskAlerts_i386.deb");
	createUpdateControlFile(appPath+"\\controls\\control_amd64",appPath+"\\updateControls\\amd64\\Packages","update/DeskAlerts_amd64.deb");
	createUpdateControlFile(appPath+"\\controls\\control_repo",appPath+"\\updateControls\\Packages","update/DeskAlertsRepo.deb");
	
	return 0;
}

