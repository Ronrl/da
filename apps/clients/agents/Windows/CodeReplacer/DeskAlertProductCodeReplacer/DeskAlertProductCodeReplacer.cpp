// DeskAlertProductCodeReplacer.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <Rpc.h>
#include <string>
#include <fstream>
#include <iostream>
#include <boost/regex.hpp>

int _tmain(int argc, _TCHAR* argv[])
{
	if (argc < 3)
	{
		std::cout << "Wrong first parameter\n";
		return 0;
	}

	//setlocale(LC_CTYPE, "Russian");

	std::ifstream ifs(argv[1]);
	if (ifs.is_open() == false)
	{
		std::cout << std::string("Unable to open file ") + argv[1];
		return 0;
	}

	std::string wxs;
	ifs.seekg(0, std::ios_base::end);
	int pos = ifs.tellg();
	ifs.seekg(0, std::ios_base::beg);
	wxs.resize(pos, ' ');
	ifs.read(const_cast<char*>(wxs.c_str()), pos);
	ifs.close();

	GUID newGuid = { 0 };
	UuidCreate( &newGuid );
	unsigned char* chr(NULL);
	UuidToString(&newGuid, &chr);
	std::string guid((char*)chr);
	std::transform(guid.begin(), guid.end(), guid.begin(), toupper);
	RpcStringFree(&chr);

	boost::regex xRegEx("(ProductCode=)+((\")|('))*((\\w+)|(-+))+((\")|('))*");
	std::string xFormatString("ProductCode=");
	xFormatString += "\"" + guid + "\"";
	boost::smatch xResults;

	wxs = boost::regex_replace(wxs, xRegEx, xFormatString, boost::match_default);

	std::ofstream ofs(argv[2], std::ios_base::ate);
	ofs << wxs;
	ofs.close();

	return 0;
}

