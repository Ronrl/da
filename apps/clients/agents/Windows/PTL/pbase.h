#pragma once

// Plugin template library v1.0

class VirtualCommand;

namespace PTL {

class Enumerator;
class Localization;

#define PLUG_EVENTS_MAP() \
		public: int Invoke(TCHAR* pName, void *pData) \
		{\
			if (!_tcscmp(pName,_T("PluginParseData")))\
			{\
				ParseData(this);\
				return ERR_NONE;\
			}

#define PLUG_MAP_ENTRY(_NAME,_HANDLER) \
		if (_tcscmp(pName,_NAME)==0) \
		return _HANDLER(pData);

#define PLUG_EVENTS_MAP_END() \
		return ERR_NONE; } 

#define PTL_BEGIN_PROPERTY_MAP() \
	public:\
	PropertyBase cbGetProperty(TCHAR *name, BOOL determining) {

#define DETERMINE_PROPERTY(_NAME, _VAR, _DETERMINE) \
	if(!determining || determining && _DETERMINE) \
		if(_tcscmp(name, _NAME) == 0) \
			return PropertyBase(&_VAR,NULL);

#define DETERMINE_PROPERTY_HANDLED(_NAME, _VAR, _DETERMINE,_HANDLER) \
	if(!determining || determining && _DETERMINE) \
		if(_tcscmp(name, _NAME) == 0) \
			return PropertyBase(&_VAR,_HANDLER);

#define DETERMINE_COLLECTION(_NAME, _VAR)\
	if(_tcscmp(name, _NAME) == 0) \
			return PropertyBase(&_VAR);

#define DETERMINE_COLLECTION2(_NAME, _VAR, _SUBNAME)\
	if(_tcscmp(name, _NAME) == 0) \
		return PropertyBase(&_VAR, PropertyBase::PT_SUBITERATOR, _SUBNAME);

#define DETERMINE_LOCALE(_VAR)\
	if(_tcscmp(name, _T("MESSAGES")) == 0)\
		return PropertyBase(&_VAR.CurrentLocale(), PropertyBase::PT_SUBITERATOR, _T("MESSAGE"));

#define PTL_END_PROPERTY_MAP() \
	return PropertyBase(); \
	}

#define PTL_PROPERTY_CHANGED_HANDLER(_HANDLER) void OnPropertyChanged(){_HANDLER();}


#define PTL_BEGIN_EVENT_MAP() \
	public:\
	int Invoke(TCHAR* pName, void *pData) { \
	if(_tcscmp(pName, _T("ToolbarPtr")) == 0) { \
		tbCtrl = (VirtualToolbar*)pData; \
		return ERR_NONE; \
		}
#define EVENT_HANDLER(_NAME, _HANDLER) \
	if(_tcscmp(pName, _NAME) == 0) \
		return _HANDLER(pData);
#define EVENT_HANDLER2(_NAME, _HANDLER2) \
	if(_tcsstr(pName, _NAME)) \
		return _HANDLER2(_NAME,pData);

#define PTL_END_EVENT_MAP() \
	return ERR_NONE;\
	}

#define PTL_BEGIN_MESSAGE_MAP(_LOCALE) \
	public:\
	void cbInitMessages() {\
	Localization &l = _LOCALE;

#define PTL_BEGIN_MESSAGE_MAP2(_LOCALE,_LOCALE2) \
	public:\
	void cbInitMessages() {\
	Localization &l = _LOCALE;\
	Localization &l2 = _LOCALE2;

#define DEFAULT_MESSAGE(_ID, _MSG) \
	l.DefaultLocale().AddMessage(_ID, _MSG);

#define DEFAULT_MESSAGE2(_ID, _MSG) \
	l2.DefaultLocale().AddMessage(_ID, _MSG);

#define PTL_END_MESSAGE_MAP() \
	}

class PropertyChangedHandler
{
public:
	virtual void OnPropertyChanged()=0;
};

template<typename T, typename B>
class PTLBase : public B
{
protected:
	struct PropertyBase {
		enum PTYPE {PT_NONE, PT_INT, PT_STRING, PT_ITERATOR, PT_SUBITERATOR} Type;
		union
		{
			CString *strValue;
			int *intValue;
			Enumerator *enumValue;			
		} Value;
		CString param;
		PropertyChangedHandler *pCommand;
		PropertyBase() :Type(PT_NONE), pCommand(NULL), Value( {nullptr} ) {};
		PropertyBase(CString *strVal,PropertyChangedHandler *ptr):Type(PT_STRING),pCommand(ptr){Value.strValue=strVal;}
		PropertyBase(int *intVal,PropertyChangedHandler *ptr):Type(PT_INT),pCommand(ptr) {Value.intValue=intVal;}
		PropertyBase(Enumerator *enumValue, PTYPE nType = PT_ITERATOR, CString additional = _T("")):Type(nType),pCommand(NULL) {Value.enumValue=enumValue;param=additional;}
	};
	
	VirtualToolbar *tbCtrl;
	DWORD dwArgCount;
	DWORD dwArgSize;
	CString sName;

	

public:

	PTLBase():dwArgCount(0),dwArgSize(0),tbCtrl(NULL) {};
	virtual ~PTLBase() {};

	TCHAR* GetProperty(TCHAR *pName, TCHAR *pDefaultValue)
	{
		PropertyBase prop = (reinterpret_cast<T*>(this))->cbGetProperty(pName, FALSE);
		if(prop.Type == PropertyBase::PT_STRING)
		{
			DWORD dwSizeNeeded = (prop.Value.strValue->GetLength()+1)*sizeof(TCHAR);
			TCHAR *buf = NULL;
			buf = (TCHAR*) tbCtrl->AllocMemory(dwSizeNeeded);
			_stprintf_s(buf, prop.Value.strValue->GetLength()+1, _T("%s"), prop.Value.strValue->GetBuffer(0));

			return buf;
		}

		TCHAR *pResult = (TCHAR*)tbCtrl->AllocMemory((_tcslen(pDefaultValue) + 1) * sizeof(TCHAR));
		_tcscpy_s(pResult, _tcslen(pDefaultValue)+1, pDefaultValue);
	
		return pResult;
	}

	int GetProperty(TCHAR *pName, int iDefaultValue)
	{
		PropertyBase prop = (reinterpret_cast<T*>(this))->cbGetProperty(pName, FALSE);
		if(prop.Type == PropertyBase::PT_INT)
			return *(prop.Value.intValue);

		return iDefaultValue;
	}

	void SetProperty(TCHAR *pName, TCHAR *pValue)
	{
		PropertyBase prop = (reinterpret_cast<T*>(this))->cbGetProperty(pName, FALSE);
		if(prop.Type == PropertyBase::PT_STRING)
			*prop.Value.strValue = pValue;
		if (prop.pCommand)
			prop.pCommand->OnPropertyChanged();
	}

	void SetProperty(TCHAR *pName, int iValue)
	{
		PropertyBase prop = (reinterpret_cast<T*>(this))->cbGetProperty(pName, FALSE);
		if(prop.Type == PropertyBase::PT_INT)
			*prop.Value.intValue = iValue;
		if (prop.pCommand)
			prop.pCommand->OnPropertyChanged();
	}

	void ParseData(B *pCommand)
	{
		if(pCommand->pPluginNode == NULL) return;

		for (DWORD i = 0; i< pCommand->pPluginNode->m_iChildrenCount; i++)
		{
			PropertyBase prop = (reinterpret_cast<T*>(this))->cbGetProperty(pCommand->pPluginNode->m_pChildren[i].m_pName, TRUE);
			switch(prop.Type)
			{
				case PropertyBase::PT_INT :
					{
						*prop.Value.intValue = _ttoi(pCommand->pPluginNode->m_pChildren[i].m_pData);
						break;
					}
				case PropertyBase::PT_STRING :
					{
						*prop.Value.strValue = pCommand->pPluginNode->m_pChildren[i].m_pData;
						break;
					}
				case PropertyBase::PT_ITERATOR :
					{
						*prop.Value.enumValue = Enumerator(pCommand->pPluginNode, pCommand->pPluginNode->m_pChildren[i].m_pName);
						break;
					}
				case PropertyBase::PT_SUBITERATOR :
					{
						*prop.Value.enumValue = Enumerator(pCommand->pPluginNode, pCommand->pPluginNode->m_pChildren[i].m_pName);
						prop.Value.enumValue->StepInto(prop.param);
						break;
					}
			}
		}
	}

	void Init()
	{
		// Called on command construction
	};

	void Update(B *pItem)
	{
		// Called when command's properties are changed (Sync)
	};

	void Destroy()
	{
		// Called before destruction
	};

	void Init(VirtualToolbar *pToolbar)
	{
		tbCtrl = pToolbar;
		ParseData(this);
		(reinterpret_cast<T*>(this))->cbInitMessages();
		(reinterpret_cast<T*>(this))->Init();
	}

	void Sync(B *pItem)
	{
		VirtualPluginNode *pTempNode = (reinterpret_cast<B*>(this))->pPluginNode;
		(reinterpret_cast<B*>(this))->pPluginNode = pItem->pPluginNode;
		pItem->pPluginNode = pTempNode;
		ParseData(this);
		(reinterpret_cast<T*>(this))->Update(pItem);
	}

	void Release(VirtualToolbar *pToolbar)
	{
		(reinterpret_cast<T*>(this))->Destroy();
		pToolbar->ReleasePluginTree((reinterpret_cast<B*>(this))->pPluginNode);
		delete this;
	}

	void cbInitMessages()
	{
		
	}
};

template<typename T>
class PluginCommand : public PTLBase<T, VirtualToolbarCommand>
{
protected:
	PluginCommand()
	{
		reinterpret_cast<VirtualToolbarCommand*>(this)->pPluginNode = NULL;
	}

	int Invoke(TCHAR* pName, void *pData)
	{
		return ERR_NONE;
	}

	bool IsChecked()
	{
		return false;
	}

	bool IsEnabled()
	{
		return true;
	}

	void ParseAllVars(TCHAR*& ppStr)
	{
		
	}

	PROPSHEETPAGE* GetPropertyPage()
	{
		return NULL;
	}
};

template<typename T>
class PluginItem : public PTLBase<T, VirtualToolbarItem>
{
protected:

	PluginItem()
	{
		reinterpret_cast<VirtualToolbarItem*>(this)->pPluginNode = NULL;
	}

	bool IsDraggable(VirtualToolbar *pwndToolbar,bool bText)
	{
		return false;
	}
	bool IsFocused(VirtualToolbar* pwndToolbar)
	{
		return false;
	}	

	int Create(VirtualToolbar *pwndToolbar)
	{
		Init(pwndToolbar);
		Create();
		return 0;
	}

	virtual void Create()
	{
		
	}
	
	
	int Change(VirtualToolbar *pwndToolbar)
	{
	
		return 0;		
	}
	int AutoMove(VirtualToolbar *pwndToolbar, int cx)
	{
		return 0;
	}	
	

	void ProcessMenu(VirtualToolbar *pwndToolbar, int x, int y)
	{
	
	}
	
	DWORD OnItemPrePaint(VirtualToolbar *pwndToolbar, LPNMCUSTOMDRAW pnmh)
	{
		
		return 0;
	}

	DWORD OnItemPostPaint(VirtualToolbar *pwndToolbar, LPNMCUSTOMDRAW pnmh)
	{
		
		return 0;
	}

	void OnOpen(VirtualToolbar *pwndToolbar, IHTMLDocument3 *pDoc)
	{

	}

	void OnClick(VirtualToolbar *pwndToolbar, CString strItemID)
	{
	
	}

	BOOL StopMouseEvents(VirtualToolbar *pwndToolbar)
	{
		return FALSE;
	}

	int ParseAllVars(VirtualToolbar *pwndToolbar, TCHAR *&pStr, int nMode, int iEncoding)
	{

		return 0;		
	};
	
	TCHAR* GetToolTip(VirtualToolbar *pwndToolbar)
	{
		if (!pwndToolbar)
			return NULL;
		TCHAR *pBuf = (TCHAR*)pwndToolbar->AllocMemory(sizeof(TCHAR));	
		*pBuf = 0;
		return pBuf;
	}

	HRESULT TranslateAcceleratorIO(VirtualToolbar *pwndToolbar,LPMSG pMsg)
	{
		return S_FALSE;
	};

	void DoDrop(VirtualToolbar *pwndToolbar, bool bText)
	{

	}

	int Invoke(TCHAR* pName, void *pData)
	{
		return ERR_NONE;
	};


};

};