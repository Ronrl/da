#pragma once

#include "penum.h"
#include <map>

using namespace std;

namespace PTL
{

class ManualMessagesEnumerator : public Enumerator
{
	map<CString, CString> TheMessages;
	map<CString, CString>::iterator Current;

public:

	void AddMessage(CString sID, CString sDefault)
	{
		TheMessages[sID] = sDefault;
	}

	virtual bool IsInitialized()
	{
		return TheMessages.size()>0;
	}

	ManualMessagesEnumerator()
	{
	}

	virtual void Reset()
	{
		Current = TheMessages.begin();
	}

	virtual bool Finished()
	{
		return Current==TheMessages.end();
	}

	virtual bool Next()
	{
		if(Finished()) return false;
		Current++;
		return true;		
	}

	virtual Enumerator* GetEnumerator(CString measure)
	{
		return NULL;
	}

	virtual VirtualPluginNode* GetNode()
	{
		return NULL;
	}

	virtual int GetIntValue(CString sName, int iDefault = -1)
	{
		return iDefault;
	}

	virtual CString GetStrValue(CString sName, CString sDefault = _T(""))
	{
		if(Finished()) return sDefault;

		if(sName == _T("id"))
		{
			return Current->first;
		}
		if(sName == _T("default"))
		{
			return Current->second;
		}

		return sDefault;
	}

};

}