#pragma once

#include <windows.h>
#include "..\common\wtlincl.h"
#include "..\common\errors.h"
#include "..\common\abstract.h"
#include "penum.h"
#include "penumstub.h"
#include "pbase.h"
#include "plocale.h"
#include "pplugin.h"
#include "ptempl.h"
#include "pmenu.h"
#include "pfind.h"

#define GetItemByID GetItemByName