#pragma once 

#include <set>

using namespace std;

namespace PTL {
	
class PTLNodeMatch
{
public:
	virtual bool operator()(VirtualToolbarCommand *pNode) {return false;};
	virtual bool operator()(VirtualToolbarItem *pNode) {return false;};
};

class InvokeMatch : public PTLNodeMatch
{
	CString sEntity;
public:
	InvokeMatch(CString entity):sEntity(entity){};

	virtual bool operator()(VirtualToolbarCommand *pNode)
	{
		BOOL bResult = FALSE;
		pNode->Invoke((_T("Is")+sEntity).GetBuffer(0), (void*)&bResult);
		return bResult != FALSE;
	}

	virtual bool operator()(VirtualToolbarItem *pNode)
	{
		BOOL bResult = FALSE;
		pNode->Invoke((_T("Is")+sEntity).GetBuffer(0), (void*)&bResult);
		return bResult != FALSE;
	}
};

class AttributeMatch : public PTLNodeMatch
{
	CString sAttrName;
	CString sAttrValue;
	int nAttrValue;
	enum {STRING, NUMERIC} nType;
	VirtualToolbar *tbCtrl;

	bool Match(VirtualToolbarNode *pNode)
	{
		bool bResult = false;
		if(nType == STRING)
		{
			TCHAR *pValue = pNode->GetProperty(sAttrName.GetBuffer(0), _T(""));
			bResult = sAttrValue == pValue;
			tbCtrl->DeallocMemory(pValue);
		}
		else if(nType == NUMERIC)
		{
			int nValue = pNode->GetProperty(sAttrName.GetBuffer(0), -1);
			bResult = nValue == nAttrValue;
		}

		return bResult;
	}

public:
	AttributeMatch(VirtualToolbar *pToolbar, CString attribute, CString value):sAttrName(attribute),sAttrValue(value), nAttrValue(0),nType(STRING),tbCtrl(pToolbar) {};
	AttributeMatch(VirtualToolbar *pToolbar, CString attribute, int value):sAttrName(attribute),nAttrValue(value),nType(NUMERIC),tbCtrl(pToolbar) {};

	virtual bool operator()(VirtualToolbarCommand *pNode)
	{
		return Match(pNode);
	}

	virtual bool operator()(VirtualToolbarItem *pNode)
	{
		return Match(pNode);
	}
};

class And : public PTLNodeMatch
{
	PTLNodeMatch &C1;
	PTLNodeMatch &C2;

public:
	And(PTLNodeMatch& c1, PTLNodeMatch& c2):C1(c1), C2(c2) {};

	virtual bool operator()(VirtualToolbarCommand *pNode)
	{
		return C1(pNode) && C2(pNode);
	}

	virtual bool operator()(VirtualToolbarItem *pNode)
	{
		return C1(pNode) && C2(pNode);
	}
};

class Or : public PTLNodeMatch
{
	PTLNodeMatch &C1;
	PTLNodeMatch &C2;

public:
	Or(PTLNodeMatch& c1, PTLNodeMatch& c2):C1(c1), C2(c2) {};

	virtual bool operator()(VirtualToolbarCommand *pNode)
	{
		return C1(pNode) || C2(pNode);
	}

	virtual bool operator()(VirtualToolbarItem *pNode)
	{
		return C1(pNode) || C2(pNode);
	}
};

class Not : public PTLNodeMatch
{
	PTLNodeMatch &C1;

public:
	Not(PTLNodeMatch& c1):C1(c1) {};

	virtual bool operator()(VirtualToolbarCommand *pNode)
	{
		return !C1(pNode);
	}

	virtual bool operator()(VirtualToolbarItem *pNode)
	{
		return !C1(pNode);
	}
};

template<typename CMDCLASS>
std::set<CMDCLASS*> PTLFindAllNodes(VirtualToolbar *tbCtrl, PTLNodeMatch& match, int nodeType = IT_COMMANDS)
{
	std::set<CMDCLASS*> result;
	NodeIterator* pI = tbCtrl->CreateNodeIterator(nodeType);

	try {

		for (;!pI->IsBlocked();(*pI)++)
		{
			bool bMatch = false;
			if(nodeType == IT_COMMANDS)
				bMatch = match((VirtualToolbarCommand*)(**pI));
			else if (nodeType == IT_ITEMS)
				bMatch = match((VirtualToolbarItem*)(**pI));

			if(bMatch)
				result.insert(static_cast<CMDCLASS*>(**pI));
		}
		pI->Release();

	} catch(...)	{
	}

	return result;
}

template<typename CMDCLASS>
CMDCLASS* PTLFindNode(VirtualToolbar *tbCtrl, PTLNodeMatch& match, int nodeType = IT_COMMANDS)//CString PROP)
{
	NodeIterator* pI = tbCtrl->CreateNodeIterator(nodeType);
	try {

		for (;!pI->IsBlocked();(*pI)++)
		{
			bool bMatch = false;
			if(nodeType == IT_COMMANDS)
				bMatch = match((VirtualToolbarCommand*)(**pI));
			else if (nodeType == IT_ITEMS)
				bMatch = match((VirtualToolbarItem*)(**pI));

			if(bMatch)
			{
				CMDCLASS *pResult = static_cast<CMDCLASS*>(**pI);
				pI->Release();
				return pResult;
			}
		}
		pI->Release();

	} catch(...)
	{
	}

	return NULL;
}

};