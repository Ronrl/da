#pragma once

#include <vector>

using std::vector;

#define VIRTUAL_COMMAND 0
#define VIRTUAL_ITEM 1

#define PTL_BEGIN_MODULE() class PluginModule:public PTL::PTLModule<PluginModule> {\
	public:\
	void* ProcessTag(bool bReg, UINT nType, TCHAR* sName, VirtualToolbar* pToolbar, int iPlugID)\
	{\
		void *pResult = NULL;

#define DETERMINE_ITEM(_TAG, _CLASS)\
	if(bReg) {\
			pToolbar->RegisterItem(_TAG, iPlugID);\
		}\
	else if(nType==VIRTUAL_ITEM && sName!=0) {\
		if(_tcscmp(_TAG, sName) == 0)\
			pResult = new _CLASS();\
		}

#define DETERMINE_COMMAND(_TAG, _CLASS)\
	if(bReg) {\
		if(pToolbar)\
			pToolbar->RegisterCommand(_TAG, iPlugID);\
		}\
	else if(nType==VIRTUAL_COMMAND && sName!=0) {\
		if(_tcscmp(_TAG, sName) == 0)\
			pResult = new _CLASS();\
		}

#define PTL_END_MODULE()\
	return pResult;\
	}\
	} _PTL_Module;\
\
CComModule _Module;\
\
BOOL WINAPI DllMain(HANDLE hDll, DWORD dwReason, LPVOID lpvReserved)\
{\
	if(dwReason == DLL_PROCESS_ATTACH)\
	{\
		_PTL_Module.hThisDll = (HINSTANCE)hDll;\
		_Module.Init(NULL, _PTL_Module.hThisDll);\
	}\
	if(dwReason == DLL_PROCESS_DETACH)\
	{\
		_Module.Term();\
	}\
\
	return TRUE;\
}

#define PTL_END_MODULE_EX(ObjectMap)\
	return pResult;\
	}\
	} _PTL_Module;\
\
CComModule _Module;\
\
BOOL WINAPI DllMain(HANDLE hDll, DWORD dwReason, LPVOID lpvReserved)\
{\
	if(dwReason == DLL_PROCESS_ATTACH)\
	{\
		_PTL_Module.hThisDll = (HINSTANCE)hDll;\
		_Module.Init(ObjectMap, _PTL_Module.hThisDll);\
	}\
	if(dwReason == DLL_PROCESS_DETACH)\
	{\
		_Module.Term();\
	}\
\
	return TRUE;\
}


namespace PTL {

	template <typename T>
		class PTLModule//: public CComModule
	{
	public:

		HINSTANCE hThisDll;
		
		bool PlugInit(VirtualToolbar* pwndToolbar, int iPlugID)
		{	
			(reinterpret_cast<T*>(this))->ProcessTag(true, 0, 0, pwndToolbar, iPlugID);
			return true;
		}

		void* PlugCreate(TCHAR *pTAG, bool bIsItem)
		{
			return (reinterpret_cast<T*>(this))->ProcessTag(false, bIsItem ? VIRTUAL_ITEM : VIRTUAL_COMMAND, pTAG, NULL, 0);
		}		
	};
};

#ifdef UNICODE

#define PTL_STANDARD_PLUGIN_INTERFACE()\
extern "C" bool __stdcall PlugInit(VirtualToolbar* pwndToolbar, int iPlugID)\
{\
	return _PTL_Module.PlugInit(pwndToolbar, iPlugID);\
}\
\
extern "C" void* __stdcall PlugCreate(TCHAR *pTAG, bool bIsItem)\
{\
	return _PTL_Module.PlugCreate(pTAG, bIsItem);\
}\
extern "C" void __stdcall PlugTerm() {}\
extern "C" void __stdcall PlugInvoke(TCHAR *, void *) {}\
extern "C" bool __stdcall IsUnicode(){return true;}

#else

#define PTL_STANDARD_PLUGIN_INTERFACE()\
extern "C" bool __stdcall PlugInit(VirtualToolbar* pwndToolbar, int iPlugID)\
{\
	return _PTL_Module.PlugInit(pwndToolbar, iPlugID);\
}\
\
extern "C" void* __stdcall PlugCreate(TCHAR *pTAG, bool bIsItem)\
{\
	return _PTL_Module.PlugCreate(pTAG, bIsItem);\
}\
extern "C" void __stdcall PlugTerm() {}\
extern "C" void __stdcall PlugInvoke(TCHAR *, void *) {}\
extern "C" bool __stdcall IsUnicode(){return false;}

#endif

