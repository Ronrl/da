#pragma once

#include "penumstub.h"

namespace PTL {

class Localization
{
	PTL::ManualMessagesEnumerator defaultLocale;
	PTL::Enumerator currentLocale;

	static BOOL CALLBACK EnumChilds(HWND hWnd,LPARAM param)
	{
		Localization *pThis=(Localization*)param;
		pThis->LocalizeOneWindow(hWnd);
		return TRUE;
	}

	void LocalizeOneWindow(HWND hWnd)
	{
		TCHAR szTitle[1024]={0};
		GetWindowText(hWnd,szTitle,1024);
		CString localizedCaption = localizeString(szTitle);

		if (localizedCaption.GetLength()!=0)
			SetWindowText(hWnd,localizedCaption.GetBuffer(0));
	}


public:

	Localization() {};

	CString localizeString(CString sString)
	{
		if (sString.Find(_T("[["))==0)
			sString=sString.Mid(2,sString.GetLength()-2);
		if (sString.Find(_T("]]"))==sString.GetLength()-2)
			sString=sString.Mid(0,sString.GetLength()-2);

		CString sDef = sString;
		for (defaultLocale.Reset();!defaultLocale.Finished();defaultLocale.Next())
		{
			if(defaultLocale.GetStrValue(_T("id")) == sDef)
			{
				sDef = defaultLocale.GetStrValue(_T("default"));
				break;
			}
		}

		CString sResult = sDef;
		for (currentLocale.Reset();!currentLocale.Finished();currentLocale.Next())
		{
			if(currentLocale.GetStrValue(_T("id")) == sString)
			{
				sResult = currentLocale.GetStrValue(_T("default"));
				break;
			}
		}

		return sResult;
	}

	void localizeText(CString& sText)
	{
		for(currentLocale.Reset();!currentLocale.Finished();currentLocale.Next())
		{
			CString what = _T("[[") + currentLocale.GetStrValue(_T("id")) + _T("]]");
			CString def = currentLocale.GetStrValue(_T("default"));
			sText.Replace(what,def);
		}

		for(defaultLocale.Reset();!defaultLocale.Finished();defaultLocale.Next())
		{
			CString what = _T("[[") + defaultLocale.GetStrValue(_T("id")) + _T("]]");
			CString def = defaultLocale.GetStrValue(_T("default"));
			sText.Replace(what,def);
		}
	}

	void localizeDialog(HWND hDialog)
	{
		EnumChildWindows(hDialog,EnumChilds,(LPARAM)this);
		LocalizeOneWindow(hDialog);
	}

	PTL::Enumerator& CurrentLocale()
	{
		return currentLocale;
	}

	PTL::ManualMessagesEnumerator& DefaultLocale()
	{
		return defaultLocale;
	}
	
	bool isCurrentLocaleInitialized()
	{
		return currentLocale.IsInitialized();
	}
};

};