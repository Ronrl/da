#pragma once

namespace PTL {

class Enumerator
{
	CString sMeasure;
	VirtualPluginNode *pRoot;
	int nIterator;

	VirtualPluginNode* GetAttributeNode(CString sName)
	{
		if(static_cast<DWORD>(nIterator) < pRoot->m_iChildrenCount)
		{
			VirtualPluginNode *pChild = &pRoot->m_pChildren[nIterator];
			
			for(DWORD i=0;i<pChild->m_iChildrenCount;i++)
			{
				VirtualPluginNode *pNextAttrib = &pChild->m_pChildren[i];
				if(pNextAttrib->m_bIsAttrib)
				{
					if(sName == pNextAttrib->m_pName)
					{
						return pNextAttrib;
					}
				}
			}
		}
		return NULL;
	}

public:

	bool StepInto(CString newMeasure)
	{
		if(!pRoot) return false;

		if(static_cast<DWORD>(nIterator) < pRoot->m_iChildrenCount)
		{
			if(!pRoot->m_pChildren[nIterator].m_bIsAttrib)
			{
				pRoot = &pRoot->m_pChildren[nIterator];
				sMeasure = newMeasure;
				Reset();
				return true;
			}
		}
		return false;
	}

public:

	virtual bool IsInitialized()
	{
		return pRoot != 0;
	}

	Enumerator()
	{
		nIterator = 0;
		pRoot = NULL;
		sMeasure = "";
	}

	Enumerator(VirtualPluginNode *root, CString measure)
	{
		nIterator = 0;
		pRoot = root;
		sMeasure = measure;
		Reset();
	}

	virtual void Reset()
	{
		if(!pRoot) return;
		
		nIterator = 0;
		bool bResult = static_cast<DWORD>(nIterator) < pRoot->m_iChildrenCount;
		if(bResult)
		{
			if(sMeasure != pRoot->m_pChildren[nIterator].m_pName)
				Next();
		}
	}

	virtual bool Finished()
	{
		if(!pRoot) return true;
		return static_cast<DWORD>(nIterator) >= pRoot->m_iChildrenCount;
	}

	virtual bool Next()
	{
		if(!pRoot) return false;

		bool bResult = true;
		while(bResult){
			bResult = static_cast<DWORD>(++nIterator) < pRoot->m_iChildrenCount;
			if(bResult)
			{
				if(sMeasure == pRoot->m_pChildren[nIterator].m_pName)
					break;
			}
		}
		return bResult;		
	}

	virtual Enumerator* GetEnumerator(CString measure)
	{
		if(!pRoot) return NULL;

		if(static_cast<DWORD>(nIterator)<pRoot->m_iChildrenCount)
		{
			if(!pRoot->m_pChildren[nIterator].m_bIsAttrib)
				return new Enumerator(&pRoot->m_pChildren[nIterator], measure);
		}
		return NULL;
	}

	virtual VirtualPluginNode* GetNode()
	{
		if(!pRoot) return NULL;

		if(static_cast<DWORD>(nIterator) < pRoot->m_iChildrenCount)
		{
			return &pRoot->m_pChildren[nIterator];
		}
		return NULL;
	}

	virtual int GetIntValue(CString sName, int iDefault = -1)
	{
		if(!pRoot) return iDefault;

		VirtualPluginNode *pAttr = GetAttributeNode(sName);
		if(pAttr) return _ttoi(pAttr->m_pData);
		return iDefault;
	}

	virtual CString GetStrValue(CString sName, CString sDefault = _T(""))
	{
		if(!pRoot) return sDefault;

		VirtualPluginNode *pAttr = GetAttributeNode(sName);
		if(pAttr) return pAttr->m_pData;
		return sDefault;
	}
};

};