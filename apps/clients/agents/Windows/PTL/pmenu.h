#pragma once

namespace PTL {

	class MenuSeparator : public VirtualMenuItem
	{
	public:
		MenuSeparator(UINT id, UINT pos=100)
		{
			m_iID = id;
			m_bIsSeparator = TRUE;
			m_iInsertBefore = pos;
			m_iImg = -1;
			m_pCaption = m_pCommand = m_pHint = NULL;
		}
	};

	class MenuItem : public VirtualMenuItem
	{
	public:
		MenuItem(VirtualToolbar *tbCtrl, UINT id, CString sCaption, CString sCommand, CString sHint = _T(""), int nImg=-1, UINT pos=100)
		{
			m_iID = id;
			m_bIsSeparator = FALSE;
			m_iInsertBefore = pos;
			m_iImg = nImg;

			m_pCaption = (TCHAR*)tbCtrl->AllocMemory((sCaption.GetLength()+1)*sizeof(TCHAR));
			_tcscpy_s(m_pCaption, sCaption.GetLength()+1, sCaption.GetBuffer(0));

			m_pCommand = (TCHAR*)tbCtrl->AllocMemory((sCommand.GetLength()+1)*sizeof(TCHAR));
			_tcscpy_s(m_pCommand, sCommand.GetLength()+1, sCommand.GetBuffer(0));

			m_pHint = (TCHAR*)tbCtrl->AllocMemory((sHint.GetLength()+1)*sizeof(TCHAR));
			_tcscpy_s(m_pHint, sHint.GetLength()+1, sHint.GetBuffer(0));
		};
	};
}