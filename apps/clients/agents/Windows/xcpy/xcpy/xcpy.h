#pragma once

#include "resource.h"

BOOL RecursiveCopyDirectory(const TCHAR *src, const TCHAR *dest);
BOOL RecursiveDeleteDirectory(const TCHAR *path);
TCHAR* RemoveQuoteChars(TCHAR *src);