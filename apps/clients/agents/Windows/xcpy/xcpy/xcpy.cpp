// xcpy.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "xcpy.h"
#include <shellapi.h>
#include <atlbase.h>
#include <string>
#include <sys\stat.h>

#define MAX_LOADSTRING 100

TCHAR seps[]   = _T(";");

int APIENTRY _tWinMain(HINSTANCE hInstance,
                     HINSTANCE /*hPrevInstance*/,
                     LPTSTR    lpCmdLine,
                     int       /*nCmdShow*/)
{
	TCHAR* src = NULL;
	TCHAR* dest = NULL;
	TCHAR* exeName = NULL;
	TCHAR* baseName = NULL;
	TCHAR* fromTemp = NULL;

	int i=0;

	/* Establish string and get the first token: */
	TCHAR *lpCmdLineCpy = _tcsdup(lpCmdLine);
	TCHAR *context = NULL;
	TCHAR *token = _tcstok_s(lpCmdLineCpy, seps, &context);
	while( token != NULL )
	{
		if (i==0)
		{
			src = RemoveQuoteChars(_tcsdup(token));
		}
		else if (i==1)
		{
			dest = RemoveQuoteChars(_tcsdup(token));
		}
		else if (i==2)
		{
			exeName = RemoveQuoteChars(_tcsdup(token));
		}
		else if (i==3)
		{
			baseName = RemoveQuoteChars(_tcsdup(token));
		}
		else if (i==4)
		{
			fromTemp = RemoveQuoteChars(_tcsdup(token));
		}
		else
		{
			break;
		}

		// Get next token:
		token = _tcstok_s(NULL, seps, &context);
		i++;
	}

	do
	{
		if(fromTemp==NULL || _tcscmp(fromTemp, _T("1"))) //if usual run, copy self to temp and re-run
		{
			TCHAR path[MAX_PATH+1];
			GetModuleFileName(hInstance, path, MAX_PATH+1);

			TCHAR tmp[MAX_PATH+1];
			size_t len = MAX_PATH+1;
			_tgetenv_s(&len, tmp, MAX_PATH+1, _T("TEMP"));
			if(tmp[_tcslen(tmp)-1] != _T('\\'))
			{
				_tcscat_s(tmp, MAX_PATH+1, _T("\\"));
			}
			_tcscat_s(tmp, MAX_PATH+1, _T("Alerts"));
			_tmkdir(tmp); //for be sure create Alerts folder in temp

			TCHAR newpath[MAX_PATH+1];
			_stprintf_s(newpath, MAX_PATH+1, _T("%s\\xpy.exe"), tmp);
			i = 1;
			while(PathFileExists(newpath) && i<1000) //for be sure ;-)
			{
				_stprintf_s(newpath, MAX_PATH+1, _T("%s\\xpy[%d].exe"), tmp, i++);
			}
			if(CopyFile(path, newpath, FALSE))
			{
				const TCHAR add_chars[] = _T(";1");
				const size_t args_len = _tcslen(lpCmdLine) + sizeof(add_chars)/sizeof(TCHAR);
				TCHAR *args = new TCHAR[args_len+1];
				_tcscpy_s(args, args_len, lpCmdLine);
				_tcscat_s(args, args_len, add_chars);

				const HINSTANCE hinst = ::ShellExecute(NULL, _T("open"), newpath, args, NULL, SW_HIDE);
				ATLASSERT(hinst > (HINSTANCE)SE_ERR_DLLNOTFOUND);
				DBG_UNREFERENCED_LOCAL_VARIABLE(hinst);

				delete_catch(args);
				break;
			}
		}
		release_try
		{
			TCHAR* newExeName = _tcsdup(exeName);
			PathStripPath(newExeName);
			const size_t new_len = _tcslen(src) + _tcslen(newExeName) + 2;
			TCHAR *newExePath = new TCHAR[new_len+1];
			_tcscpy_s(newExePath, new_len, src);
			if(newExePath[_tcslen(newExePath)-1] != _T('\\'))
			{
				_tcscat_s(newExePath, MAX_PATH+1, _T("\\"));
			}
			_tcscat_s(newExePath, new_len, newExeName);
			if(PathFileExists(newExePath))
			{
				while(PathFileExists(exeName))
				{
					DeleteFile(exeName);
					Sleep(100);
				}
			}
		}
		release_end_try

		RecursiveCopyDirectory(src, dest);

		RecursiveDeleteDirectory(baseName);

		const HINSTANCE hinst = ::ShellExecute(NULL, _T("open"), exeName, _T("/update"), NULL, SW_SHOW);
		ATLASSERT(hinst > (HINSTANCE)SE_ERR_DLLNOTFOUND);
		DBG_UNREFERENCED_LOCAL_VARIABLE(hinst);
	}
	while(false);

	if (src!=NULL) delete_catch(src);
	if (dest!=NULL) delete_catch(dest);
	if (exeName != NULL) delete_catch(exeName);
	if (baseName != NULL) delete_catch(baseName);
	if (fromTemp != NULL) delete_catch(fromTemp);

	return 0;
}

BOOL RecursiveDeleteDirectory(const TCHAR *path)
{
	WIN32_FIND_DATA ffd;
	TCHAR tmp[MAX_PATH+1];
	_tcscpy_s(tmp, MAX_PATH+1, path);
	if(tmp[_tcslen(tmp)-1] != _T('\\'))
	{
		_tcscat_s(tmp, MAX_PATH+1, _T("\\"));
	}
	TCHAR findstr[MAX_PATH+1];
	_tcscpy_s(findstr, MAX_PATH+1, tmp);
	_tcscat_s(findstr, MAX_PATH+1, _T("*"));
	const HANDLE hFind = FindFirstFile(findstr, &ffd);
	if(INVALID_HANDLE_VALUE != hFind)
	{
		do
		{
			TCHAR filepath[MAX_PATH+1];
			_tcscpy_s(filepath, MAX_PATH+1, tmp);
			_tcscat_s(filepath, MAX_PATH+1, ffd.cFileName);
			if(ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			{
				if(_tcscmp(ffd.cFileName, _T(".")) && _tcscmp(ffd.cFileName, _T("..")))
					RecursiveDeleteDirectory(filepath);
			}
			else
			{
				if(ffd.dwFileAttributes & FILE_ATTRIBUTE_READONLY)
				{
					_tchmod(filepath, _S_IWRITE);
				}
				DeleteFile(filepath);
			}
		}
		while(FindNextFile(hFind,&ffd));
		FindClose(hFind);
	}
	tmp[_tcslen(tmp)-1] = _T('\0'); //replace \ char
	return RemoveDirectory(tmp);
}

BOOL RecursiveCopyDirectory(const TCHAR *src, const TCHAR *dest)
{
	WIN32_FIND_DATA ffd;
	TCHAR tmpsrc[MAX_PATH+1];
	_tcscpy_s(tmpsrc, MAX_PATH+1, src);
	if(tmpsrc[_tcslen(tmpsrc)-1] != _T('\\'))
	{
		_tcscat_s(tmpsrc, MAX_PATH+1, _T("\\"));
	}

	TCHAR tmpdest[MAX_PATH+1];
	_tcscpy_s(tmpdest, MAX_PATH+1, dest);
	if(tmpdest[_tcslen(tmpdest)-1] == _T('\\'))
	{
		tmpdest[_tcslen(tmpdest)-1] = _T('\0'); //replace \ char
	}
	BOOL res = PathFileExists(tmpdest) || !_tmkdir(tmpdest);
	if(res)
	{
		_tcscat_s(tmpdest, MAX_PATH+1, _T("\\"));

		TCHAR findstr[MAX_PATH+1];
		_tcscpy_s(findstr, MAX_PATH+1, tmpsrc);
		_tcscat_s(findstr, MAX_PATH+1, _T("*"));
		const HANDLE hFind = FindFirstFile(findstr, &ffd);
		if(INVALID_HANDLE_VALUE != hFind)
		{
			do
			{
				TCHAR filesrc[MAX_PATH+1];
				_tcscpy_s(filesrc, MAX_PATH+1, tmpsrc);
				_tcscat_s(filesrc, MAX_PATH+1, ffd.cFileName);

				TCHAR filedest[MAX_PATH+1];
				_tcscpy_s(filedest, MAX_PATH+1, tmpdest);
				_tcscat_s(filedest, MAX_PATH+1, ffd.cFileName);
				if(ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
				{
					if(_tcscmp(ffd.cFileName, _T(".")) && _tcscmp(ffd.cFileName, _T("..")))
						RecursiveCopyDirectory(filesrc, filedest);
				}
				else
				{
					if(!CopyFile(filesrc, filedest, FALSE))
						res = FALSE;
				}
			}
			while(FindNextFile(hFind,&ffd));
			FindClose(hFind);
		}
	}
	return res;
}

TCHAR* RemoveQuoteChars(TCHAR *src)
{
	TCHAR* dest = src;
	if(dest)
	{
		if(dest[0] == _T('"'))
		{
			dest[_tcslen(dest)-1] = _T('\0');
			dest = _tcsdup(dest+1);
			delete_catch(src);
		}
	}
	return dest;
}