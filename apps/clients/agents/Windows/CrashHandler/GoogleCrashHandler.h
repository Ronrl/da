#pragma once
#include <string>
#include <windows.h>

void __cdecl initCrashHandler();
typedef struct {
    /* expression, function, and file are 0-terminated UTF-16 strings.  They
     * may be truncated if necessary, but should always be 0-terminated when
     * written to a file.
     * Fixed-length strings are used because MiniDumpWriteDump doesn't offer
     * a way for user streams to point to arbitrary RVAs for strings. */
    uint16_t expression[128];  /* Assertion that failed... */
    uint16_t function[128];    /* ...within this function... */
    uint16_t file[128];        /* ...in this file... */
    uint32_t line;             /* ...at this line. */
    uint32_t type;
} MDRawAssertionInfo;
typedef bool (*MinidumpCallback)(const wchar_t* dump_path,
    const wchar_t* minidump_id,
    void* context,
    EXCEPTION_POINTERS* exinfo,
    MDRawAssertionInfo* assertion,
    bool succeeded);
//using std::wstring;

class GoogleCrashHandler {
//private:
    static void* ExceptionHandler;
    
public:
    std::wstring Path;
    GoogleCrashHandler(const std::wstring& dump_folder, MinidumpCallback);
    std::wstring getPath();
    ~GoogleCrashHandler();

};