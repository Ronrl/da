//--------------------------------------------------------------------
//   Copyright � 1999-2006 BestToolbars.net - All Rights Reserved
//--------------------------------------------------------------------

/*----------------------------------------------------------------------
By using this file you agree to accept the terms of  the Source code
licence http://www.besttoolbars.net/scoc.html. You  should carefully
read the following terms and conditions before using this software.

Source Code License. BestToolbars.net grants to Licensee, under BestToolbars.net
Proprietary Rights in the Software, a limited, non-exclusive, non-transferable,
revocable, internal use license to reproduce, modify, or otherwise use the Software
Source Code for Licensee's own internal use within Licensee's enterprise and solely
for the purpose of developing and testing the Software or Licensee's modifications of
the Software.

THIS SOFTWARE  IS  PROVIDED  ``AS IS''  AND  ANY  EXPRESSED OR IMPLIED WARRANTIES, INCLUDING,
BUT NOT  LIMITED TO, THE  IMPLIED WARRANTIES OF  MERCHANTABILITY AND FITNESS FOR A PARTICULAR
PURPOSE ARE  DISCLAIMED. IN NO EVENT SHALL BestToolbars.net OR ITS CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE  GOODS OR  SERVICES; LOSS  OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN  CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING  NEGLIGENCE OR OTHERWISE)  ARISING IN ANY WAY OUT OF THE
USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------*/

////////////////////////////////////////
//Toolbar 4
//Toolbar Interfaces
///////////////////////////////////////

///////////////////////////////////////
//Modified on 23.10.2007,17.00
///////////////////////////////////////
#pragma once

#include "errors.h"
#include <Mshtml.h>
//

//ParseAllVars mode
#define PAV_ALL 1
#define PAV_TOOLBAR 2
#define PAV_EDITABLE_AND_CHECKABLE 4
#define PAV_URLENCODE 8
#define PAV_INDEPENDENT 16
#define PAV_EX_IGNORE_HTML_VARS 32

class VirtualToolbarItem;
class VirtualToolbarItemVisitor;

struct SHOWMENUDATA
{
	WCHAR *pMenuID;
	int x;
	int y;
};

typedef struct tagPUB_INSTANCE
{
	// Frame IE window. Uses to handle window moves
	HWND hWndIEFrame;
	// Worker window
	HWND	hWndExplorer;	
	// Toolbar window
	HWND	hWndTB;	
	bool	m_bShow;
	void	*pToolbarObj;
}
PUB_INSTANCE;

typedef struct tagBeforeNavigateData
{ 
	TCHAR *url;
	TCHAR *frame;
} BEFORENAVIGATEDATA;

typedef struct tagBeforeNavigateData2Ex
{ 
	IDispatch* pDisp;
	VARIANT* URL;
	VARIANT* Flags;
	VARIANT* TargetFrameName;
	VARIANT* PostData;
	VARIANT* Headers;
	VARIANT_BOOL* Cancel;
} BEFORENAVIGATEDATA2EX;

typedef struct tagDocumentCompleteDataEx
{ 
	IDispatch* pDisp;
	VARIANT* URL;
} DOCUMENTCOMPLETEDATAEX;

struct NAVIGATEERROR
{
	IDispatch *pDisp;
	VARIANT *URL;
	VARIANT *TargetFrameName;
	VARIANT *StatusCode;
	VARIANT_BOOL *Cancel;
};

typedef struct tagBannerDocumentComplete
{ 
	IDispatch* pDisp;
	VARIANT* URL;
	VARIANT* BannerId;
} BANNERDOCUMENTCOMPLETE;

typedef struct tagDownloadProgress
{
	WCHAR szUrl[1024];
	DWORD dwTotalLenght;
	DWORD dwDownloadedLenght;
} DOWNLOADPROGRESS;

///////////////////////////////////////
//InvokeStruct
/////////////////////////////////////
typedef struct tagInvokeStruct
{
	WCHAR *pName;
	void *pData;
}INVOKESTRUCT;


///////////////////////////////////////
//PluginInvokeStruct
/////////////////////////////////////

struct PluginInvokeStruct
{
    VARIANT* varIn;
    VARIANT* varOut;
    int status;
};

struct BHOInvoke
{	
	WCHAR szGUID[39];
	WCHAR szRegKey[1024];
	IWebBrowser2 *pWebBrowser;
	HMODULE hDLL;
	void *pData;
};


///////////////////////////////////////
//Plugin Node class
//////////////////////////////////////

class VirtualPluginNode	

{
public:
	bool m_bIsAttrib;	//true if the node is attribute, false if the node is TAG
	WCHAR *m_pData;		//Attribute value, ignored if m_bIsAttrib is false, must be null in this case
	WCHAR *m_pName;		//Attribute/TAG name
	VirtualPluginNode *m_pChildren;	//Children' array	
	DWORD m_iChildrenCount;	//children' count
	DWORD m_iChildrenSize;	//children' array size
};

class VirtualToolbar;

////////////////////////////
//Abstract XMLNode
////////////////////////////
class XMLParser;

class XMLNode

{
	
public:	
	bool m_bFlag = false;
	virtual void Determine(XMLParser *pParser){};
	virtual void DetermineAttrib(XMLParser *pParser){};	

};

///////////////////////////////////////
//Toolbar Node abstract class (for items & commands)
/////////////////////////////////////

class VirtualToolbarNode:
	public XMLNode
{
public:
	VirtualPluginNode *pPluginNode;
	virtual WCHAR* GetProperty(WCHAR *pName, WCHAR *pDefaultValue)=0;
	virtual int GetProperty(WCHAR *pName, int iDefaultValue)=0;
	virtual void SetProperty(WCHAR *pName, WCHAR *pValue)=0;
	virtual void SetProperty(WCHAR *pName, int iValue)=0;	
//delete any item data
	virtual void Release(VirtualToolbar *pwndToolbar)=0;	
	VirtualToolbarNode(){pPluginNode = NULL;}

};


//////////////////////////////////////
//Callback interface for registered messages
/////////////////////////////////////

class RegisteredMessageCallback
{
public:
	virtual LRESULT OnMsgCallback(MSG &msg) = 0;
};


//////////////////////////////////////
//Node Iterator
/////////////////////////////////////

//mode

#define IT_ITEMS 0x01
#define IT_COMMANDS 0x02

class NodeIterator
{
public:	
	virtual VirtualToolbarNode* operator * ()=0;
	virtual VirtualToolbarNode* operator ++ (int) = 0;
	virtual VirtualToolbarNode* operator -- (int) = 0;
	virtual bool IsBlocked()=0;
	virtual void SetToBeginning()=0;
	virtual void Release()=0;
};

//Menu Item

//when m_bIsSeparator == true
//m_iImg, m_pCaption, m_pCommand, m_pHint members are ignored and must be NULL

//Memory must be allocated using AllocMemory() for pointers,
//It's deallocated AUTOMATICALLY after inserting menu item
class VirtualMenuItem
{
public:
	bool m_bIsSeparator;	//separator menu item
	int m_iID;				//Item identifier, used for plugin menu items only
	int m_iInsertBefore;		//Item position		

	int m_iImg;				//Item image identifier

	WCHAR *m_pCaption;	
	WCHAR *m_pCommand;	
	WCHAR *m_pHint;
};
class VirtualMenuItemEx:
	public VirtualMenuItem
{
public:
	virtual void Release()=0;
};


//IMenu interface

class IMenuIterator;

class IMenu
{
	
public:	
	virtual IMenu*	InsertSubMenu(int iPos)=0;						//Inserts SubMenu
	virtual void	InsertItem(VirtualMenuItem &item)=0;			//Inserts Menu Item	
	virtual void	DeleteItem(int iID)=0;							//Deletes item by item identifier		
	virtual void	Release()=0;
	virtual IMenuIterator* GetIterator()=0;
	
};

class IMenuIterator
{
public:
	virtual VirtualMenuItemEx* operator ++(int)=0;				//increases iterator position
	virtual VirtualMenuItemEx* operator --(int)=0;				//decreases iterator position
	virtual VirtualMenuItemEx* operator *()=0;					//returns current iterator value		
	virtual IMenuIterator* GetSubItemIterator()=0;					//returns pointer to iterator for current item, if there's no submenu,
																//return value is NULL
																//otherwise returns NULL
	virtual void SetToBeginning()=0;
	virtual bool IsBlocked()=0;									//if index is out of bounds, iterator is in blocked status
	virtual void Release()=0;									//Should be called after using Iterator
};

class IMenuIteratorEx:
	public IMenuIterator
{
public:
	virtual void SetItem(VirtualMenuItemEx &item)=0;
};
///////////////////////////////////////
//Toolbar Item Node abstract class
/////////////////////////////////////
class VirtualToolbarItem:
	public VirtualToolbarNode
	
{
public:		
	int m_ID;						//item identifier	
	int m_CommandID;				//command identifier	
//properties	
	virtual bool IsDraggable(VirtualToolbar *pwndToolbar,bool bText)=0;
	virtual bool IsFocused(VirtualToolbar* pwndToolbar)=0;

//creation/motion
	virtual int Create(VirtualToolbar *pwndToolbar)=0;				//Creates control
	virtual int Change(VirtualToolbar *pwndToolbar, int extraLen = 0)=0;				//Changes created control
	virtual int AutoMove(VirtualToolbar *pwndToolbar, int cx, int extraSize = 0)=0;				//Changes created control
	
//menu
	virtual void ProcessMenu(VirtualToolbar *pwndToolbar, int x, int y)=0;

//events	
	virtual DWORD OnItemPrePaint(VirtualToolbar *pwndToolbar, LPNMCUSTOMDRAW pnmh)=0;
	virtual DWORD OnItemPostPaint(VirtualToolbar *pwndToolbar, LPNMCUSTOMDRAW pnmh)=0;
	virtual void OnOpen(VirtualToolbar *pwndToolbar, IHTMLDocument3 *pDoc)=0;
	virtual void OnClick(VirtualToolbar *pwndToolbar, CString strItemID)=0;	
	virtual BOOL StopMouseEvents(VirtualToolbar *pwndToolbar)=0;
//other
	virtual WCHAR* GetToolTip(VirtualToolbar *pwndToolbar)=0;
	virtual HRESULT TranslateAcceleratorIO(VirtualToolbar *pwndToolbar,MSG *pMsg)=0;
	virtual void Sync(VirtualToolbarItem *pItem)=0;
	virtual void DoDrop(VirtualToolbar *pwndToolbar, bool bText)=0;
	virtual int Invoke(WCHAR* pName, void *pData)=0;
	virtual int ParseAllVars(VirtualToolbar *pwndToolbar, WCHAR *&pStr, int nMode=PAV_ALL, int iEncoding=65001)=0;
	virtual void Accept(VirtualToolbarItemVisitor* visitor) { }

};

///////////////////////////////////////
//Toolbar Command Node abstract class
/////////////////////////////////////
class VirtualToolbarCommand:
	public VirtualToolbarNode
{
public:			
	//properties
	virtual bool IsChecked()=0;
	virtual bool IsEnabled()=0;	
	virtual PROPSHEETPAGE* GetPropertyPage()=0;

	//executing/initializing
	virtual int Apply(VirtualToolbar *pwndToolbar,bool bIsPressed)=0;	
	virtual void Init(VirtualToolbar *pwndToolbar)=0;
		
	//other
	virtual int Invoke(WCHAR* pName, void *pData)=0;
	virtual void Sync(VirtualToolbarCommand *pCommand)=0;
};


///////////////////////////////////////
//Toolbar Window abstract class
/////////////////////////////////////
class VirtualToolbar
{
public:
	HFONT m_hFont;
public:				
	//items / commands 
	virtual int RegisterCommand(WCHAR*, int iPluginID)=0;	//Adds command to command parser list,returns ERR_NONE
											//if added successfully
	virtual int RegisterItem(WCHAR* pItem,int iPluginID)=0;//Adds item to item parser list,returns ERR_NONE
										//if added successfully
	virtual void EnableButtonByCommand(WCHAR* pCommand, BOOL bEnabled)=0;
	virtual void PushButtonByCommand(WCHAR* pCommand, BOOL bPushed)=0;

	//visibility
	virtual int GetToolbarItemId(WCHAR* pID,  int iDefault)=0;
	virtual void SetToolbarItemId(WCHAR* pID, int iValue)=0;

	//registry

	virtual WCHAR*	GetRegValue(WCHAR *pName, WCHAR *pDefault, WCHAR *pSubkey=NULL)=0;
	virtual int	GetRegValue(WCHAR *pName, int iDefault, WCHAR *pSubkey=NULL)=0;
	virtual void	SetRegValue(WCHAR *pName, WCHAR* pValue, WCHAR *pSubkey=NULL)=0;
	virtual void	SetRegValue(WCHAR *pName, int iValue, WCHAR *pSubkey=NULL)=0;	
	virtual WCHAR*	GetToolbarKey()=0;	//returns toolbar registry key
	virtual WCHAR*  GetRegDESValue(WCHAR *pName,WCHAR* pDefault)=0; 
	virtual void	SetRegDESValue(WCHAR* pName,WCHAR* pValue)=0;
	virtual HKEY	GetAppRegKey()=0;

	//toolbar properties
	
	virtual WCHAR*	GetProperty(WCHAR* pName, WCHAR* pDefault)=0;
	virtual int	GetProperty(WCHAR* pName, int  iDefault)=0;
	virtual void	SetProperty(WCHAR* pName, WCHAR*  pDefault)=0;
	virtual void	SetProperty(WCHAR* pName,int iDefault)=0;

	virtual WCHAR*	GetPropertyById(WCHAR *pID, WCHAR *pProperty, WCHAR *pDefault)=0;
	virtual int	GetPropertyById(WCHAR *pID, WCHAR *pProperty, int iDefault)=0;
	virtual void	SetPropertyById(WCHAR *pID, WCHAR *pProperty, WCHAR *pValue)=0;
	virtual void	SetPropertyById(WCHAR *pID, WCHAR *pProperty, int iValue)=0;	

	virtual HINSTANCE GetInstance()=0;	//instance of the toolbar DLL
	virtual HWND GetTBWindow()=0;		//returns toolbar window
	virtual WCHAR *GetProgPath()=0;	//returns toolbar working directory
	virtual bool IsDesignMode()=0;		//returns true if toolbar's open in TB Studio	
	virtual bool GetReadyState()=0;		//returns ready state of toolbar

	//COM interfaces
	virtual IDispatch* GetIDispatch()=0;			//returns IDispatch interface of IToolbarObj
	virtual IWebBrowser2* GetIWebBrowser()=0;		//returns IWebBrowser2 interface of IToolbarObj
	virtual HRESULT BrowserNavigate(WCHAR* pURL)=0;
	
	//reloading/updating

	virtual void Reload()=0;	//Redownloads all include_xml's and reload's toolbar.		
	virtual void UpdateToolbar()=0;//Calls Change() for all items	
	
	//access to items' / commands' vectors

	virtual WCHAR*	GetIdByCommand(WCHAR *pCommand)=0;
	virtual VirtualToolbarItem* GetItemByName(WCHAR *pName)=0;	//pName - item's ID
	virtual VirtualToolbarCommand* GetCommandByName(WCHAR *pName)=0;	
	virtual VirtualToolbarItem* AddItemByTag(WCHAR *pTag)=0;
	virtual VirtualToolbarCommand* AddCommandByTag(WCHAR *pTag)=0;
	
	//node iterator, call NodeIterator::Release() when you don't need it anymore
	virtual NodeIterator* CreateNodeIterator(int nMode /*IT_ITEMS or IT_COMMANDS*/)=0;

	virtual IMenu* GetIMenu(WCHAR *pID /*BUTTON IDENTIFIER*/)=0; //IMenu interface
	
	//custom XML
	virtual WCHAR* GetCustomXML(WCHAR* pID)=0;
	virtual void SetCustomXML(WCHAR* pID, WCHAR* pData)=0;
	
	//memory management
	virtual void* AllocMemory(size_t size)=0;
	virtual void DeallocMemory(void *)=0;
	virtual void ReleasePluginTree(VirtualPluginNode *pTree)=0;

	//URLs/Messages
	virtual WCHAR* GetURLByID(WCHAR *pURLName)=0;
	virtual WCHAR* GetMessageByID(WCHAR *pMessageName,WCHAR *pDefault)=0;

	//other
	virtual int Invoke(WCHAR* pName, void *pData)=0;			
	
	virtual void	ParseAllVars(WCHAR*& pStr, int nMode = PAV_ALL, int nEncoding = 65001)=0;	
	virtual void FocusChange(BOOL bFocus)=0;

	//plugin messages
	virtual int RegisterMessage(UINT uMsg, RegisteredMessageCallback *pCallback)=0;
	virtual int UnregisterMessage(RegisteredMessageCallback *pCallback)=0;

	
};


class VirtualUpdater
{
public:
	virtual	WCHAR *GetProgPath(bool f=false)=0;
	virtual void SetValue(WCHAR *pName, WCHAR *pValue)=0;
	virtual WCHAR *GetValue(WCHAR *pName, WCHAR *pDef)=0;
	virtual void DeallocMemory(void *pMem)=0;
	virtual void * AllocMemory(size_t size)=0;
	virtual void RemoveKey()=0;
	virtual void Release()=0;
	virtual void CloseAllRunningIE()=0;
	virtual void DeleteDir(TCHAR *pDir)=0;
	virtual void SelfDestruct(TCHAR *pDir)=0;
	virtual void UnregisterServer() = 0;
	virtual void UnloadAllSingletons() = 0;
};

class VirtualScriptHost
{	
public:
	virtual BOOL InitializeScriptFrame()=0;	
	virtual void RunScript()=0;
	virtual void LaunchScript()=0;
	virtual void LaunchScript(CComVariant param)=0;
	virtual void BeforeNavigate()=0;
	virtual void DocumentComplete(TCHAR* strType)=0;
	virtual STDMETHODIMP_(ULONG) AddRef()=0;
	virtual STDMETHODIMP_(ULONG) Release()=0;
	virtual ULONG GetRefCount()=0;
	virtual void FrameComplete(CComVariant param)=0;
	virtual void FrameBeforeNavigate(CComVariant param)=0;
};