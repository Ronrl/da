//--------------------------------------------------------------------
//   Copyright � 1999-2006 BestToolbars.net - All Rights Reserved
//--------------------------------------------------------------------

/*----------------------------------------------------------------------
By using this file you agree to accept the terms of  the Source code
licence http://www.besttoolbars.net/scoc.html. You  should carefully
read the following terms and conditions before using this software.

Source Code License. BestToolbars.net grants to Licensee, under BestToolbars.net
Proprietary Rights in the Software, a limited, non-exclusive, non-transferable,
revocable, internal use license to reproduce, modify, or otherwise use the Software
Source Code for Licensee's own internal use within Licensee's enterprise and solely
for the purpose of developing and testing the Software or Licensee's modifications of
the Software.

THIS SOFTWARE  IS  PROVIDED  ``AS IS''  AND  ANY  EXPRESSED OR IMPLIED WARRANTIES, INCLUDING,
BUT NOT  LIMITED TO, THE  IMPLIED WARRANTIES OF  MERCHANTABILITY AND FITNESS FOR A PARTICULAR
PURPOSE ARE  DISCLAIMED. IN NO EVENT SHALL BestToolbars.net OR ITS CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE  GOODS OR  SERVICES; LOSS  OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN  CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING  NEGLIGENCE OR OTHERWISE)  ARISING IN ANY WAY OUT OF THE
USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------*/
#ifndef DHTML_EVENT_H
#define DHTML_EVENT_H

template <class T> class CHtmlEventObject : public IDispatch  
{

typedef void (T::*EVENTFUNCTIONCALLBACK)(DISPID id, VARIANT* pVarResult);

public:
   CHtmlEventObject() { m_cRef = 0; }	
   ~CHtmlEventObject() {}

   HRESULT __stdcall QueryInterface(REFIID riid, void** ppvObject)
   {
      *ppvObject = NULL;

      if (IsEqualGUID(riid, IID_IUnknown))
         *ppvObject = reinterpret_cast<void**>(this);

      if (IsEqualGUID(riid, IID_IDispatch))
         *ppvObject = reinterpret_cast<void**>(this);

      if (*ppvObject)
      {
         ((IUnknown*)*ppvObject)->AddRef();
         return S_OK;
      }
      else return E_NOINTERFACE;
   }

   DWORD __stdcall AddRef()
   {
      return InterlockedIncrement(&m_cRef);
   }

   DWORD __stdcall Release()
   {
      if (InterlockedDecrement(&m_cRef) == 0)
      {
         delete this;
         return 0;
      }
      return m_cRef;
   }

   STDMETHOD(GetTypeInfoCount)(unsigned int FAR* pctinfo)
   { return E_NOTIMPL; }

   STDMETHOD(GetTypeInfo)(unsigned int iTInfo, LCID  lcid, ITypeInfo FAR* FAR*  ppTInfo)
   { return E_NOTIMPL; }

   STDMETHOD(GetIDsOfNames)(REFIID riid, OLECHAR FAR* FAR* rgszNames, unsigned int cNames, LCID lcid, DISPID FAR* rgDispId)
   { return S_OK; }

   STDMETHOD(Invoke)(DISPID dispIdMember, REFIID riid, LCID lcid,
      WORD wFlags, DISPPARAMS* pDispParams, VARIANT* pVarResult,
      EXCEPINFO * pExcepInfo, UINT * puArgErr)
   {
	  //IDispatch::Invoke(dispIdMember,riid,lcid,wFlags,pDispParams, pVarResult,pExcepInfo, puArgErr);
      
	  if (DISPID_VALUE == dispIdMember)
	  {	
         (m_pT->*m_pFunc)(m_id, pVarResult);
	  }
      else
      {
         AtlTrace(_T("Invoke dispid = %d\n"), dispIdMember);
      }
	
      return S_OK;
   }

public:
   static LPDISPATCH CreateHandler(T* pT,
                        EVENTFUNCTIONCALLBACK pFunc, DISPID id)
   {
      CHtmlEventObject<T>* pFO = new CHtmlEventObject<T>;
      pFO->m_pT = pT;
      pFO->m_pFunc = pFunc;
      pFO->m_id = id;
      return reinterpret_cast<LPDISPATCH>(pFO);
   }

protected:
   T* m_pT;
   EVENTFUNCTIONCALLBACK m_pFunc;
   DISPID m_id;
   long m_cRef;
};

//-- advanced class: can call previous event handler before fires this and also releases it when need
template <class T> class CHtmlEventObjectEx : public IDispatch  
{

typedef void (T::*EVENTFUNCTIONCALLBACK)(DISPID id, VARIANT* pVarResult);

public:
   CHtmlEventObjectEx() : m_pdispPrevHandler(NULL), m_cRef(1) {}
   ~CHtmlEventObjectEx() {}

   HRESULT __stdcall QueryInterface(REFIID riid, void** ppvObject)
   {
      *ppvObject = NULL;

      if (IsEqualGUID(riid, IID_IUnknown))
         *ppvObject = reinterpret_cast<void**>(this);

      if (IsEqualGUID(riid, IID_IDispatch))
         *ppvObject = reinterpret_cast<void**>(this);

      if (*ppvObject)
      {
         ((IUnknown*)*ppvObject)->AddRef();
         return S_OK;
      }
      else return E_NOINTERFACE;
   }

   DWORD __stdcall AddRef()
   {
	  if(m_pdispPrevHandler)
	  {
		  const int ref = m_pdispPrevHandler->AddRef();
	  }
      return InterlockedIncrement(&m_cRef);
   }

   DWORD __stdcall Release()
   {
	  if(m_pdispPrevHandler)
	  {
		  const int ref = m_pdispPrevHandler->Release();
	  }
      if (InterlockedDecrement(&m_cRef) == 0)
      {
         delete this;
         return 0;
      }
      return m_cRef;
   }

   STDMETHOD(GetTypeInfoCount)(unsigned int FAR* pctinfo)
   { return E_NOTIMPL; }

   STDMETHOD(GetTypeInfo)(unsigned int iTInfo, LCID  lcid, ITypeInfo FAR* FAR*  ppTInfo)
   { return E_NOTIMPL; }

   STDMETHOD(GetIDsOfNames)(REFIID riid, OLECHAR FAR* FAR* rgszNames, unsigned int cNames, LCID lcid, DISPID FAR* rgDispId)
   { return S_OK; }

   STDMETHOD(Invoke)(DISPID dispIdMember, REFIID riid, LCID lcid,
      WORD wFlags, DISPPARAMS* pDispParams, VARIANT* pVarResult,
      EXCEPINFO * pExcepInfo, UINT * puArgErr)
   {
	  //IDispatch::Invoke(dispIdMember,riid,lcid,wFlags,pDispParams, pVarResult,pExcepInfo, puArgErr);
      
	  if (DISPID_VALUE == dispIdMember)
	  {	
		 if(m_pdispPrevHandler)
		 {
			 m_pdispPrevHandler->Invoke(dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr);
		 }
         (m_pT->*m_pFunc)(m_id, pVarResult);
	  }
      else
      {
         AtlTrace(_T("Invoke dispid = %d\n"), dispIdMember);
      }
	
      return S_OK;
   }

public:
   static LPDISPATCH CreateHandler(T* pT, EVENTFUNCTIONCALLBACK pFunc, DISPID id, LPDISPATCH lpPrevHandler =NULL)
   {
      CHtmlEventObjectEx<T>* pFO = new CHtmlEventObjectEx<T>;
      pFO->m_pT = pT;
      pFO->m_pFunc = pFunc;
      pFO->m_id = id;
	  pFO->m_pdispPrevHandler = lpPrevHandler;
      return reinterpret_cast<LPDISPATCH>(pFO);
   }

protected:
   T* m_pT;
   EVENTFUNCTIONCALLBACK m_pFunc;
   DISPID m_id;
   LPDISPATCH m_pdispPrevHandler;
   long m_cRef;
};

#endif DHTML_EVENT_H