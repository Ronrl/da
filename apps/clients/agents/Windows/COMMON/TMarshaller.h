#pragma once

#define WM_GETIWEBBROWSER WM_USER + 1032
#define WM_DELIWEBBROWSER WM_USER + 1033

template <class INTERFACE, const IID* _IID = &__uuidof(INTERFACE)>
class TMarshaller
{
  LPSTREAM pStm;

  public:

	  INTERFACE *pCurrent;

	  bool Pack(INTERFACE *pCurrent)
	  {
		  this->pCurrent = pCurrent;
		  if(!pCurrent) return false;

		  if(pStm)
		  {
#ifndef NDEBUG
			  ::MessageBox(0,_T("Stream for marshaling is already busy!"), _T("Error"), MB_OK | MB_ICONSTOP);  
#endif
			  return false;
		  }

		  HRESULT hr = CoMarshalInterThreadInterfaceInStream(*_IID,pCurrent,&pStm);

		  if(FAILED(hr))
		  {
#ifndef NDEBUG
			  TCHAR msg[128];
			  _stprintf_s(msg,128, _T("Unable to marshal access object: 0x%08X"), hr);//_stprintf(msg, _T("Unable to marshal access object: 0x%08X"), hr);
			  ::MessageBox(0,msg, _T("Error"), MB_OK | MB_ICONSTOP);  
#endif
		  } 
		  return SUCCEEDED(hr);
	  }

	  void Unpack()
	  {
		  HRESULT hr = CoGetInterfaceAndReleaseStream(pStm,*_IID,(void**)&pCurrent);
		  pStm = NULL;
		  if(FAILED(hr))
		  {
#ifndef NDEBUG
			  TCHAR msg[128];
			  _stprintf_s(msg,128, _T("Unable to unmarshal access object: 0x%08X"), hr);//_stprintf(msg, _T("Unable to unmarshal access object: 0x%08X"), hr);
			  ::MessageBox(0,msg, _T("Error"), MB_OK | MB_ICONSTOP);  
#endif
		  } 
	  }

	  TMarshaller() : pCurrent(NULL), pStm(NULL)
	  {
	  }
};