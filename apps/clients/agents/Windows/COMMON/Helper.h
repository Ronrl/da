//--------------------------------------------------------------------
//   Copyright � 1999-2006 BestToolbars.net - All Rights Reserved
//--------------------------------------------------------------------

/*----------------------------------------------------------------------
By using this file you agree to accept the terms of  the Source code
licence http://www.besttoolbars.net/scoc.html. You  should carefully
read the following terms and conditions before using this software.

Source Code License. BestToolbars.net grants to Licensee, under BestToolbars.net
Proprietary Rights in the Software, a limited, non-exclusive, non-transferable,
revocable, internal use license to reproduce, modify, or otherwise use the Software
Source Code for Licensee's own internal use within Licensee's enterprise and solely
for the purpose of developing and testing the Software or Licensee's modifications of
the Software.

THIS SOFTWARE  IS  PROVIDED  ``AS IS''  AND  ANY  EXPRESSED OR IMPLIED WARRANTIES, INCLUDING,
BUT NOT  LIMITED TO, THE  IMPLIED WARRANTIES OF  MERCHANTABILITY AND FITNESS FOR A PARTICULAR
PURPOSE ARE  DISCLAIMED. IN NO EVENT SHALL BestToolbars.net OR ITS CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE  GOODS OR  SERVICES; LOSS  OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN  CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING  NEGLIGENCE OR OTHERWISE)  ARISING IN ANY WAY OUT OF THE
USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------*/

////////////////////////////////////////
//Toolbar 3.0
//Helper Module
//Abstract interfaces and exports definitions
///////////////////////////////////////

#pragma once

#include "abstract.h"
#include "wtlincl.h"

//Download Task
struct DOWNLOADTASK
{
	//input
	CString strURL;						//URL
	CString strFileName;				//File Name (may not be used if output in buffer)
	CString strHeaders;					//HTTP additional headers
	bool bNoCache;						//if  true, DownloadManager deletes cached file before
										//downloading
	bool bNoReload;
};

struct SYNCREMOTEFILE
{
	WCHAR **ptr;
	WCHAR *fn;
	int iExpire;
	bool bSync;
};

//helper abstraction

class VirtualToolbarHelper
{
	
public:
	virtual bool isInDownloadQueue(CString fileName)=0;
	virtual WCHAR* GetDomain()=0;
	virtual WCHAR* GetTitle()=0;
	virtual WCHAR* GetMeta()=0;
	virtual WCHAR* GetClipBoard()=0;
	virtual WCHAR* GetRef()=0;
	virtual WCHAR* GetCharset()=0;	
	virtual void GetHtml(WCHAR **pstrHtmlText, WCHAR **pstrHtml)=0;

	//Download Manager routines
	virtual HRESULT SyncRemoteFile(WCHAR **pstrResult, WCHAR* strURL,		//Adds url to download queue, returns MD5 hashed value
					bool bSyncDownload=false,
					bool bNoCache=false,
					bool bRemovable=false,
					bool bNoReload=false)=0;	
	virtual void Release()=0;	

	//
	virtual void DNSCatch_Init(WCHAR *strDNSURL, WCHAR *strDNSWILD)=0;
	virtual void DNSCatch_Apply(IWebBrowser2 *pWebBrowser,VARIANT *URL, VARIANT* StatusCode,VARIANT_BOOL *& Cancel)=0;
	virtual void AutoSearch_Init(WCHAR *strURL, WCHAR *strMask, WCHAR *szToolbarId, WCHAR *szAffId)=0;
	virtual WCHAR * AutoSearch_GetURL()=0;
	virtual void AutoSearch_SetURL(WCHAR *pURL)=0;
	virtual void UserAgent_Init(WCHAR *pAgent)=0;
	virtual void SideSearch_Init(WCHAR *pHref)=0;
};

//exports
typedef bool (CALLBACK* HELPER_ISUNICODE)();
typedef VirtualToolbarHelper* (CALLBACK* HELPER_CREATE)(VirtualToolbar *);
typedef HRESULT (STDAPICALLTYPE* HELPER_REG)();



class VirtualHashedName
{
public:
	virtual WCHAR* GetHashedName(BYTE* pBuf, UINT nLength)=0;
	virtual WCHAR* GetHashedPath(BYTE* pBuf, UINT nLength, WCHAR *szDir)=0;
	virtual void Release()=0;
};


class HelperData
{
public:
	bool bLoaded;
	HMODULE hHelper;
	VirtualToolbarHelper *m_pHelperObj;		
};