#include "StdAfx.h"
#include "alertloader.h"
#include "AlertUtils.h"
#include "AlertView.h"
#include <time.h>
#include <ctime>
#include <atltime.h>
#include <windows.h>
#include <Lmcons.h>
#include <Mmsystem.h>
#include "SysMenu.h"
#pragma comment(lib,"Winmm")
#include "GMUtils.h"
#include "OptionConf.h"
#include "WallpaperManager.h"
#include "ScreensaverManager.h"
#include "DatabaseManagr.h"
#include "WindowCommand.h"
#include <fstream>
#include <algorithm>

//
// A. Maximov. 24.07.2017
// Necessary to restart application on lost connection
//
#include "../RestartAPI/RestartAPI.h"

#define TOP_TEMPLATE_START_SEPARATOR "<!-- top_template_start -->"
#define TOP_TEMPLATE_END_SEPARATOR "<!-- top_template_end -->"
#define BOTTOM_TEMPLATE_START_SEPARATOR "<!-- bottom_template2_start -->"
#define BOTTOM_TEMPLATE_END_SEPARATOR "<!-- bottom_template2_end -->"
//
// utilites
//
bool compareTime(int expire, bool needUpdate);
vector<wstring> CheckUserGroups(IADsUser *pUser);
int checksum(std::ifstream& file);
//
//
//
CAlertLoader::CAlertLoader(NodeALERTS::NodeCOMMANDS::NodeALERTCOMMAND* param): CCommand(param), data(*param),
	m_dwThreadId(0), m_timerThread(NULL),m_dwThreadAdId(0), m_timerThreadAd(NULL)
{
//	HRESULT hr = CoInitialize(NULL);
//	hr;
//	Notification::INotificationPtr tmp(__uuidof(Notification::DeskAlertsNotification));
//	notifier = tmp;
//	notifier(__uuidof(Notification::DeskAlertsNotification));
//	notifier->SubscribeToSessionLockEvent();

//	CoUninitialize();
	
	m_parentHWND = ::GetDesktopWindow();
}

CAlertLoader::~CAlertLoader(void)
{
	stop();
}

void CAlertLoader::setMainFrameHWND(HWND hWnd)
{
	m_mainFrameHWND = hWnd;
}
/************************************************************************/
/* Window event                                                         */
/************************************************************************/

LRESULT CAlertLoader::OnCreate(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	return 0;
}

LRESULT CAlertLoader::OnDestroy(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	stop();
	return 0;
}

LRESULT CAlertLoader::OnNotifyXMLUpdate(UINT /*uMsg*/, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	if (_iAlertModel)
	{
		if (lParam == NULL)
		{
			if(wParam == NULL && _iAlertModel->getPropertyString(CString(_T("AlertMode"))) == _T("offline"))
			{
				_iAlertModel->setPropertyString(CString(_T("AlertMode")), CString(_T("normal")));
				IView *pMenu = (CSysMenu*)_iAlertView->getViewByName(CString(_T("sysmenu")));
				if(pMenu)
				{
					CSysMenu *menu = (CSysMenu *) pMenu;
					if (menu != NULL) menu->refreshIcon();
				}
			}
			// exit if there are no alert to show
		}
		else
		{
			CString* xmlPath = (CString*)lParam;
			alertListUpdated(*xmlPath, shared_ptr<NodeALERT>((NodeALERT*)wParam));
			delete_catch(xmlPath);
		}
		bHandled = TRUE;
	}
	return S_OK;
}

LRESULT CAlertLoader::OnNotifyXMLUnreaded(UINT /*uMsg*/, WPARAM wParam, LPARAM /*lParam*/, BOOL& bHandled)
{
	if (_iAlertModel)
	{
		CSysMenu *menu = (CSysMenu*)_iAlertView->getViewByName(CString(_T("sysmenu")));
		if (*((CString *)wParam) != _T("0"))
		{
			CString sUnreaded = _iAlertModel->getPropertyString(CString(_T("unreadedMsg")), &CString(_T("You have %count unreaded alerts")));
			sUnreaded.Replace(_T("%count"), *((CString *)wParam));
			sUnreaded.Replace(_T("%s"), *((CString *)wParam)); //for back compatibility
			_iAlertModel->setPropertyString(CString(_T("disableNewState")), CString(_T("on")));
			_iAlertModel->setPropertyString(CString(_T("disableCaption")), sUnreaded);
			if (menu != NULL) menu->refreshIcon();
		}
		bHandled = TRUE;
	}
	return S_OK;
}

LRESULT CAlertLoader::OnPurgeHistory(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled)
{
	if(_iAlertModel)
	{
		CString sHistoryExpire = _iAlertModel->getPropertyString(CString("history_expire"), &CString());
		if(!sHistoryExpire.IsEmpty() && sHistoryExpire != _T("0"))
		{
			CString sMode = _iAlertModel->getPropertyString(CString("history_expire_mode"), &CString());
			if (!sMode.IsEmpty() && sMode != _T("0"))
			{
				CString sTime = _iAlertModel->getPropertyString(CString("history_expire_time"), &CString());
				if (sTime.IsEmpty()) sTime = _T("00:00");
				CHistoryStorage::PurgeAlertsAtTime(sHistoryExpire, sTime);
			}
			else
			{
				CHistoryStorage::PurgeAlerts(sHistoryExpire);
			}
		}
		bHandled = TRUE;
	}
	return S_OK;
}
LRESULT CAlertLoader::OnErrorConnection(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled)
{
	if(_iAlertModel)
	{
		_iAlertModel->setPropertyString(CString(_T("AlertMode")), CString(_T("offline")));
		CSysMenu *menu = (CSysMenu*)_iAlertView->getViewByName(CString(_T("sysmenu")));
		CString sError = _iAlertModel->getPropertyString(CString(_T("connectionError")), &CString(_T("Cannot establish a connection.")));
		_iAlertModel->setPropertyString(CString(_T("modeCaption")),sError);
		if (menu != NULL) menu->refreshIcon();

		//
		// Necessary to start DeskAlerts' restart timer
		//
		::PostMessage(m_mainFrameHWND, UWM_NOTIFY_LOST_CONNECTION, NULL, NULL);		
		//
		//
		//
		
		bHandled = TRUE;
	}
	return S_OK;
}
//
// A. Maxmiov 28/07/2017
// Notifies mainframe that connection is established and restart timer should be reset
//
LRESULT CAlertLoader::OnEstablishedConnection(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled)
{
	if(_iAlertModel)
	{
		//
		// Necessary to stop DeskAlerts' restart timer
		//
		::PostMessage(m_mainFrameHWND, UWM_NOTIFY_ESTABLISHED_CONNECTION, NULL, NULL);		
		//
		//
		//

		bHandled = TRUE;
	}
	return S_OK;
}
LRESULT CAlertLoader::OnSetVariable(UINT /*uMsg*/, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	if(_iAlertModel)
	{
		CString* name = (CString*)wParam;
		CString* value = (CString*)lParam;
		_iAlertModel->setPropertyString(*name,*value);
		delete_catch(name);
		delete_catch(value);
		bHandled = TRUE;
	}
	return S_OK;
}

LRESULT CAlertLoader::OnScreensavers(UINT /*uMsg*/, WPARAM wParam, LPARAM /*lParam*/, BOOL& bHandled)
{
	if(_iAlertModel)
	{
		vector<NodeALERT*>* alerts = (vector<NodeALERT*>*)wParam;
		vector<NodeALERT*>::iterator it = alerts->begin();
		while(it != alerts->end())
		{
			NodeALERT* curNode = (NodeALERT*) (*it);
			curNode->m_class = "2";
			addToHistory(shared_ptr<NodeALERT>(curNode),CString());
			it++;
		}
		delete_catch(alerts);
		bHandled = TRUE;
	}
	return S_OK;
}

LRESULT CAlertLoader::OnWallpapers(UINT /*uMsg*/, WPARAM wParam, LPARAM /*lParam*/, BOOL& bHandled)
{
	if(_iAlertModel)
	{
		vector<NodeALERT*>* alerts = (vector<NodeALERT*>*)wParam;
		vector<NodeALERT*>::iterator it = alerts->begin();
	
		while(it != alerts->end())
		{
			NodeALERT* curNode = (NodeALERT*) (*it);
			curNode->m_class = "8";

			addToHistory(shared_ptr<NodeALERT>(curNode),CString());
			it++;
		}
		delete_catch(alerts);
		_iAlertController->getWallpaperManager()->showWallpapers();
		bHandled = TRUE;
	}
	return S_OK;
}

/************************************************************************/
/*                                                                      */
/************************************************************************/

CString CAlertLoader::getName()
{
	return _T("ALERT");
}

void CAlertLoader::run(IActionEvent& event)
{
	CString str = event.getEventName();
	if (str==_T("enable") || str==_T("autostart"))
	{
		myRun(event);
	}
	else
	{
		myStop();
	}
}

void CAlertLoader::stop()
{
	myStop();
}

BOOL CAlertLoader::isAutoStart()
{
	return TRUE;
}

void CAlertLoader::myRun(IActionEvent& event)
{
	if (_iAlertModel)
	{
		appendToErrorFile(L"Lets start AlertLoader: %s", event.getSourceName());
		HWND tmp = (HWND)_iAlertModel->getProperty(CString(_T("mainHWND")));
		if (tmp)
		{
			m_parentHWND = tmp;
		}

		if(!m_hWnd) Create(m_parentHWND);
		createTimerThreadForAdSync(event);
		createTimerThread(event);

		
		appendToErrorFile(L"AlertLoader has been started: %s", event.getSourceName());
	}
}

void CAlertLoader::myStop()
{
	appendToErrorFile(L"Lets stop AlertLoader with thread handle: %p", m_timerThread);
	if(m_timerThread)
	{
		//WaitForSingleObjectWithMsgLoop();
		DWORD exitCode;
		if(GetExitCodeThread(m_timerThread, &exitCode))
		{
			if(exitCode == STILL_ACTIVE)
			{
				const HANDLE event = CreateEvent(NULL, TRUE, FALSE, NULL);
				if(!::PostThreadMessage(m_dwThreadId,UWM_STOP_THREAD,0,(LPARAM)event) ||
					WaitForSingleObject(event, 5000) == WAIT_OBJECT_0)
				{
					CloseHandle(event);
				}
			}
		}
		CloseHandle(m_timerThread);
		m_timerThread = NULL;
	}
	if(m_timerThreadAd)
	{
		//WaitForSingleObjectWithMsgLoop();
		DWORD exitCode;
		if(GetExitCodeThread(m_timerThreadAd, &exitCode))
		{
			if(exitCode == STILL_ACTIVE)
			{
				const HANDLE event = CreateEvent(NULL, TRUE, FALSE, NULL);
				if(!::PostThreadMessage(m_dwThreadAdId,UWM_STOP_THREAD,0,(LPARAM)event) ||
					WaitForSingleObject(event, 5000) == WAIT_OBJECT_0)
				{
					CloseHandle(event);
				}
			}
		}
		CloseHandle(m_timerThreadAd);
		m_timerThreadAd = NULL;
	}
	if (::GetParent(m_hWnd)==::GetDesktopWindow()) this->DestroyWindow();
	appendToErrorFile(L"AlertLoader has been stopped with thread handle: %p", m_timerThread);
}


/************************************************************************/
/*                                                                      */
/************************************************************************/


CString CAlertLoader::getTempPath()
{
	CString tmp;
	tmp.GetEnvironmentVariable(_T("TEMP"));
	if(tmp.GetAt(tmp.GetLength()-1) != _T('\\'))
	{
		tmp += _T('\\');
	}
	tmp += _T("Alerts");
	return tmp;
}

CString CAlertLoader::getTempFileName(CString prefix, int i)
{
 CString tmp = CAlertLoader::getTempPath();
 CString tmp2="";
 if(!tmp.IsEmpty())
 {
  _tmkdir(tmp);
  srand(GetTickCount());
  
  tmp2.Format(_T("%s\\%s_%x_%x_%x.html"),tmp,prefix,::GetCurrentThreadId(),i,rand());
 }
 return tmp2;
}

BOOL FuckYeahChecker()
{
	BOOL result = TRUE; //you may show alerts
	//CString str = L"Software\\DeskAlertsPlugin";
	CString str = CGMUtils::GetPreCompanyKey() + L"\\" + _T("DeskAlertsPlugin");
	CRegKey hKey;
	if (ERROR_SUCCESS == hKey.Open(HKEY_CURRENT_USER, str, KEY_READ))
	{
		DWORD dwValue = 0;
		if(ERROR_SUCCESS == hKey.QueryDWORDValue(_T("DisableAlerts"), dwValue))
		{
			if (0 != dwValue)
				result = FALSE; //you should disable alerts
		}
		hKey.Close();
	}
	return result;
}

void CAlertLoader::synchronizeScreensavers(ThreadData* thData, CString hash)
{
	CString hashPropName = _T("screensaversHash");
	if (_iAlertModel->getPropertyString(hashPropName, &CString()) != hash)
	{
		CScreensaverManager *screensaverManager = _iAlertController->getScreensaverManager();
		if (screensaverManager)
		{
			if (hash.IsEmpty())
			{
				screensaverManager->disableScreensaver();
			}
			else
			{
				screensaverManager->enableScreensaver();
			}
		}
		SYSTEMTIME lt;           
		GetLocalTime(&lt);
		CString furl2 = "";
		furl2.Format(_T("%s&localtime=%02d.%02d.%d|%02d:%02d:%02d"), thData->screensaversUrl, lt.wDay,lt.wMonth,lt.wYear,lt.wHour,lt.wMinute,lt.wSecond);

		CString resultXML;
		/*const HRESULT hres = */CAlertUtils::URLDownloadToFile(furl2, NULL, &resultXML, false, NULL);
		NodeSYNCHRONIZEDALERTS alerts;
		XMLParser parser((XmlNode *)&alerts, _T("SCREENSAVERS"));
		parser.Parse(resultXML,false);
		CDatabaseManager *db = CDatabaseManager::shared(true);
		if(db)
		{
			db->synchronizeAlerts(alerts.m_alerts,_T("2"));
		}
		vector<NodeALERT*>::iterator alertsIterator = alerts.m_alerts->begin();

		// save mediafile to tmp storage
		CString resultHTML;
		CString tmpPath = CAlertUtils::getWallpapersPath();
		CString url;
		CString fileName;
		//

		while(alertsIterator != alerts.m_alerts->end())
		{
			NodeALERT* curNode = *alertsIterator;
			curNode->m_isUTF8 = FALSE;
			CString alertHref = curNode->m_href;

			//disabled decryption: not implemented on server side
			HRESULT hres = CAlertUtils::URLDownloadToFile(alertHref, NULL, &resultHTML, &curNode->m_isUTF8,
				_iAlertModel->getPropertyString(CString(_T("decryption"))) == CString(_T("1"))?&_iAlertModel->getPropertyString(CString(_T("decryption_key")), &CString(_T("nj6kjvk58s8wb2d73"))):NULL);
			// check if it's a powerpoint made screensaver
			if(resultHTML.Find(_TEXT("screensaver_type=\"powerpoint\"")) >= 0)
			{
				url = CAlertUtils::ExtractHref(resultHTML);
				fileName = tmpPath + L"\\screensaver_" + CAlertUtils::ExtractFileName(url);
				CString modifiedHtml = CAlertUtils::ReplaceHref(resultHTML, fileName);
				curNode->m_html = modifiedHtml;//resultHTML;
			}
			else
			{
				curNode->m_html = resultHTML;
			}
			if (hres!=S_OK)
			{
				appendToErrorFile(L"Fail to download screensavers data from %s", curNode->m_href);
			}
			alertsIterator++;
		}
		if (fileName.GetLength() > 0 && url.GetLength() > 0)
			CAlertUtils::URLDownloadToFile(url, fileName, NULL, false, NULL);
		_iAlertModel->setPropertyString(hashPropName, hash);
		::PostMessage(thData->hwnd, UWM_NOTIFY_SCREENSAVERS,(WPARAM)(alerts.m_alerts),NULL);
/* the old version

			NodeALERT* curNode = *alertsIterator;
			curNode->m_isUTF8 = FALSE;
			CString resultHTML;
			CString alertHref = curNode->m_href;

			//disabled decryption: not implemented on server side
			HRESULT hres = CAlertUtils::URLDownloadToFile(alertHref,NULL,&resultHTML,&curNode->m_isUTF8,
				_iAlertModel->getPropertyString(CString(_T("decryption"))) == CString(_T("1"))?&_iAlertModel->getPropertyString(CString(_T("decryption_key")), &CString(_T("nj6kjvk58s8wb2d73"))):NULL);

			curNode->m_html = resultHTML;

			if (hres!=S_OK)
			{
				appendToErrorFile(L"Fail to download screensavers data from %s", curNode->m_href);
			}
			alertsIterator++;
		}
*/
	}
}

void CAlertLoader::synchronizeWallpapers(ThreadData* thData, CString hash)
{
	CDatabaseManager *db = CDatabaseManager::shared(true);
	CString hashPropName = _T("wallpapersHash");
	CString tmpPath = CAlertUtils::getWallpapersPath();
	
	WIN32_FIND_DATA search_data;

	memset(&search_data, 0, sizeof(WIN32_FIND_DATA));
	CString path = tmpPath+"\\*.*";
	HANDLE handle = FindFirstFile(path, &search_data);

	bool needtoreload = false;
	int folderempty = 0;
	int foldercount = 0;
	CString alertid;
	vector<CString*> *alertsInFolder = new vector<CString*>();
	while(handle != INVALID_HANDLE_VALUE)
	{
		
		CString fileName = search_data.cFileName;

		// ignore screensaver files and directories
		if (fileName.Find(TEXT("screensaver_")) != -1
			|| fileName == "." || fileName == "..")
		{
			if(FindNextFile(handle, &search_data) == FALSE)
				break;
			continue;
		}
		//
		int k=0;
		CString tmpFileName = fileName;

		string stdfileName = string(CT2CA(fileName));
		if (strstr(stdfileName.c_str(),"wallpaper")!= NULL)
		{
			foldercount++;
		}
		else
		{
			DeleteFile(tmpPath+"\\"+fileName);
		}

		alertid = fileName;
		alertid.Replace(_T("wallpaper_"),0);
		bool isid = false;
		while(alertid.Find('_')!= -1)
		{
			isid = true;
			alertid.Delete(alertid.GetLength()-1,alertid.GetLength()-1);
		}
		if (isid)
		{
			alertsInFolder->push_back(&alertid);
		}
		

		while(fileName.Find('_')!= -1)
		{
			folderempty++;
			k++;
			fileName.Delete(0,fileName.Find('_')+1);
		}
		
		
		if (k>0)
		{
			fileName.Delete(fileName.Find('.'),4);
			CString sum;
			std::ifstream file;
			file.open(tmpPath+"\\"+tmpFileName,std::ifstream::binary);
			sum.Format(_T("%d"), checksum(file));
			file.close();
			if (sum != fileName)
			{
				DeleteFile(tmpPath+"\\"+tmpFileName);
				if(db)
				{
					sqlite_int64 alid = 0;
					string ss = string(CT2CA(alertid));
					alid = atoi(ss.c_str());
					db->DeleteAlert(alid);
				}
				needtoreload = true;
			}
			
		}

		if(FindNextFile(handle, &search_data) == FALSE)
			break;
	}
	
	

	CString serverUrl = _iAlertModel->getPropertyString(CString(_T("server")));


	SYSTEMTIME lt;           
	GetLocalTime(&lt);
	CString furl2 = "";
	furl2.Format(_T("%s&localtime=%02d.%02d.%d|%02d:%02d:%02d"), thData->wallpapersUrl, lt.wDay,lt.wMonth,lt.wYear,lt.wHour,lt.wMinute,lt.wSecond);


	CString resultXML;
	/*const HRESULT hres = */CAlertUtils::URLDownloadToFile(furl2,NULL,&resultXML,false,NULL);
	NodeSYNCHRONIZEDALERTS alerts;
	XMLParser parser((XmlNode *)&alerts, _T("WALLPAPERS"));
	parser.Parse(resultXML, false);
	vector<CString*>::iterator alertsinfolderiterator = alertsInFolder->begin();
	vector<NodeALERT*>::iterator alertsIterator = alerts.m_alerts->begin();
	int gettedalertscount = alerts.m_alerts->size();
	if ((folderempty == 0)||(foldercount<gettedalertscount))
	{
		
		while(alertsIterator != alerts.m_alerts->end())
		{
			bool found = false;
			NodeALERT* curNode = *alertsIterator;
			CString idtodelete;
			idtodelete = curNode->m_alertid;
			alertsinfolderiterator = alertsInFolder->begin();
			while(alertsinfolderiterator != alertsInFolder->end())
			{
				CString* folderNode = *alertsinfolderiterator;
				
				if (curNode->m_alertid == folderNode->GetString())
				{
					found = true;
					break;
				}
				alertsinfolderiterator++;

			}
			if (!found)
			{
				if(db)
				{
					sqlite_int64 alid = 0;
					string ss = string(CT2CA(idtodelete));
					alid = atoi(ss.c_str());
					db->DeleteAlertByID(alid);
				}
			}
			alertsIterator++;
			
		}
		

		needtoreload = true;
	}


	//Close the handle after use or memory/resource leak
	FindClose(handle);

	if ((_iAlertModel->getPropertyString(hashPropName, &CString()) != hash) || (needtoreload))
	{
		

		
			
			if(db)
			{
				db->synchronizeAlerts(alerts.m_alerts,_T("8"));
			}
		
		

		alertsIterator = alerts.m_alerts->begin();
		while(alertsIterator != alerts.m_alerts->end())
		{
			NodeALERT* curNode = *alertsIterator;
			curNode->m_isUTF8 = FALSE;
			CString resultHTML;
			CString alertHref = curNode->m_href;
			if(alertHref.Find(_T("://")) == -1)
				alertHref = serverUrl + _T("/") + alertHref;

			HRESULT hres = CAlertUtils::URLDownloadToFile(alertHref,NULL,&resultHTML,&curNode->m_isUTF8,NULL);
			//disabled decryption: not implemented on server side
			//	_iAlertModel->getPropertyString(CString(_T("decryption"))) == CString(_T("1"))?&_iAlertModel->getPropertyString(CString(_T("decryption_key")), &CString(_T("nj6kjvk58s8wb2d73"))):NULL);
			
			CString imgUrl = resultHTML;
		
			IStream *pSomeImg = NULL; // source image format is not important
			IStream *pMyJpeg = NULL;
			CreateStreamOnHGlobal(NULL, TRUE, &pMyJpeg);
			
			CString szPath;
			CString imgPath;
			srand(GetTickCount());
			imgPath.Format(_T("wallpaper_%s.jpg"),curNode->m_alertid);
			
			szPath = tmpPath + _T("\\") + imgPath;
			if(imgUrl.Find(_T("://")) == -1)
				imgUrl = serverUrl + _T("/") + imgUrl;


	
			DeleteFile(szPath);
			hres = CAlertUtils::URLDownloadToFile(imgUrl,szPath,NULL,FALSE,NULL);

			if (SHCreateStreamOnFile(szPath,STGM_READ,&pSomeImg) == S_OK)
			{
				OSVERSIONINFO osvi;
				ZeroMemory(&osvi, sizeof(OSVERSIONINFO));
				osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
				GetVersionEx(&osvi);

				int quality = 75;
				CString mimeType= "image/jpeg";
				if (osvi.dwMajorVersion < 6) 
				{
					mimeType = "image/bmp";
				}
				
				if (Gdiplus::Ok == CAlertUtils::imageToImage(pSomeImg, pMyJpeg, mimeType, quality))
				{
					pSomeImg->Release();
					const HANDLE imgFile = CreateFile(szPath.GetString(),GENERIC_WRITE,NULL,NULL,CREATE_ALWAYS,FILE_ATTRIBUTE_NORMAL,NULL);
					STATSTG stat;
					pMyJpeg->Stat(&stat, 0);
					HGLOBAL hGlobal = NULL;
					GetHGlobalFromStream(pMyJpeg, &hGlobal);
					LPVOID pData = GlobalLock(hGlobal);
					DWORD writtenBytes;
					WriteFile(imgFile,pData,stat.cbSize.LowPart,&writtenBytes,NULL);
					std::ifstream file;
					GlobalUnlock(hGlobal);
					pMyJpeg->Release();
					CloseHandle(imgFile);
					file.open(szPath.GetString(),std::ifstream::binary);
					int crc = checksum(file);
					imgPath.Delete(imgPath.Find('.'),4);
					imgPath.Format(imgPath+"_%d.jpg",crc);
					file.close();
					CString newPath;
					newPath = tmpPath + _T("\\") + imgPath;
					DeleteFile(newPath);
					CopyFile(szPath, newPath, FALSE);
				


				}
				else
				{
					pSomeImg->Release();
				}
				
			}
			DeleteFile(szPath);
			curNode->m_html = imgPath;
					
			if (hres!=S_OK)
			{
				appendToErrorFile(L"Fail to download wallpapers data from %s", curNode->m_href);
			}

			alertsIterator++;
		}

		_iAlertModel->setPropertyString(hashPropName, hash);
		::PostMessage(thData->hwnd, UWM_NOTIFY_WALLPAPERS,(WPARAM)(alerts.m_alerts),NULL);
	}
}

DWORD WINAPI CAlertLoader::AlertLoaderFunc(LPVOID lpParam)
{
	DWORD res = 1;
	//SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_BELOW_NORMAL);
	release_try
	{
		srand(GetTickCount());

		/*const HRESULT hRes = */::CoInitialize(NULL);
		ThreadData* thData = reinterpret_cast<ThreadData*>(lpParam);

		appendToErrorFile(L"started AlertLoaderFunc url=%s expire=%d", thData->url, thData->expire);

		BOOL dowloading = FALSE;

		UINT mTimerTime = thData->expire*1000;
		UINT_PTR mTimer = ::SetTimer(NULL, NULL, mTimerTime, NULL);
		::PostMessage(thData->hwnd, WM_TIMER, NULL, NULL);
		release_try
		{
			MSG msg;
			// Main message loop:
			while(GetMessage(&msg, NULL, 0, 0) > 0) //important to check > 0, see MSDN
			{
				release_try
				{
					if(msg.message == WM_TIMER)
					{
						if(_iAlertModel)
						{
							if(!FuckYeahChecker())
							{
								if(mTimerTime != thData->expire*1000)
								{
									::KillTimer(NULL, mTimer);
									mTimerTime = thData->expire*1000;
									mTimer = ::SetTimer(NULL,NULL,mTimerTime,NULL);
								}
							}
							else
							{
								if (!thData->pixelUrl.IsEmpty())
								{
									CString pixelFilePath =  getTempFileName(_T("pixel"),0);
									CAlertUtils::checkPixel(thData->pixelUrl, pixelFilePath);
								}
								if (!dowloading)
								{
									dowloading = TRUE;
									release_try
									{
										CString alertsFilePath = getTempFileName(_T("serverxml"),0);

										//if (thData->cAlertLoader->IsReadyToDownload(thData->cAlertLoader->data.m_name))
										{
											CString furl;
											furl.Format(_T("%s&rnd=%d"), thData->url, rand());

											// START BLOCK - The following code adds an entry to the local time in a query to the server          
											SYSTEMTIME lt;           
											GetLocalTime(&lt);
											CString furl2 = "";
											furl2.Format(_T("%s&localtime=%02d.%02d.%d|%02d:%02d:%02d"), furl, lt.wDay,lt.wMonth,lt.wYear,lt.wHour,lt.wMinute,lt.wSecond);
											furl = furl2;

											// request to server
											const HRESULT hDwnldres = CAlertUtils::URLDownloadToFile(furl,alertsFilePath);
											if (hDwnldres == S_OK)
											{
												appendToErrorFile(_T("Begin to parse %s"), alertsFilePath);
												NodeTOOLBAR mNodeTOOLBAR;
												XMLParser parser((XmlNode *)&mNodeTOOLBAR, _T("TOOLBAR"));
												if(parser.Parse(alertsFilePath,true) == ERR_NONE)
												{
													::PostMessage(thData->hwnd, UWM_NOTIFY_XML_UPDATE, NULL, NULL);
												}
												if (!g_bDebugMode) DeleteFile(alertsFilePath);

												int i=0;
												CString screensaverHash;
												CString wallpaperHash;
												if (!mNodeTOOLBAR.m_alerts.empty())
												{
													vector<NodeTOOLBAR::XmlNode*>::iterator it;
													it = mNodeTOOLBAR.m_alerts.begin();
													while (it!=mNodeTOOLBAR.m_alerts.end())
													{
														if ((*it)->m_tagName == _T("ALERT"))
														{
															NodeALERT* curNode = (NodeALERT*) (*it);
															CString *szPath = new CString(getTempFileName(_T("alert"),i++));

															curNode->m_isUTF8 = FALSE;
															CString resultHTML;
															HRESULT hres = CAlertUtils::URLDownloadToFile(curNode->m_href,NULL,&resultHTML,&curNode->m_isUTF8,
															_iAlertModel->getPropertyString(CString(_T("decryption"))) == CString(_T("1"))?&_iAlertModel->getPropertyString(CString(_T("decryption_key")), &CString(_T("nj6kjvk58s8wb2d73"))):NULL);

															int topTemplateStartPos = resultHTML.Find(CString(TOP_TEMPLATE_START_SEPARATOR));
															if (topTemplateStartPos>=0)
															{
																CString topTemplateHTML= resultHTML.Mid(topTemplateStartPos+CString(TOP_TEMPLATE_START_SEPARATOR).GetLength());
																int topTemplateEndPos=topTemplateHTML.Find(CString(TOP_TEMPLATE_END_SEPARATOR));
																curNode->m_topTemplateHTML=topTemplateHTML.Mid(0,topTemplateEndPos);
																resultHTML.Delete(topTemplateStartPos,topTemplateEndPos+CString(TOP_TEMPLATE_END_SEPARATOR).GetLength()+CString(TOP_TEMPLATE_START_SEPARATOR).GetLength());
															}

															int bottomTemplateStartPos = resultHTML.Find(CString(BOTTOM_TEMPLATE_START_SEPARATOR));
															if (bottomTemplateStartPos>=0)
															{
																CString bottomTemplateHTML= resultHTML.Mid(bottomTemplateStartPos+CString(BOTTOM_TEMPLATE_START_SEPARATOR).GetLength());
																int bottomTemplateEndPos=bottomTemplateHTML.Find(CString(BOTTOM_TEMPLATE_END_SEPARATOR));
																bottomTemplateHTML=bottomTemplateHTML.Mid(0,bottomTemplateEndPos);

																curNode->m_bottomTemplateHTML=bottomTemplateHTML;
																resultHTML.Delete(bottomTemplateStartPos,bottomTemplateEndPos+CString(BOTTOM_TEMPLATE_END_SEPARATOR).GetLength()+CString(BOTTOM_TEMPLATE_START_SEPARATOR).GetLength());
															}

															curNode->m_html = resultHTML;
															const HANDLE alertFile = CreateFile(szPath->GetString(), GENERIC_WRITE, 0, NULL,CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
															
															DWORD dwBytesWritten;
															CStringA data = curNode->m_isUTF8 ? CT2A(resultHTML, CP_UTF8) : CT2A(resultHTML);
															WriteFile(alertFile, data, data.GetLength(), &dwBytesWritten, 0);
															CloseHandle(alertFile);

															//pair<CString, CString> alertSize= CAlertUtils::getBodySize(*szPath);
															//curNode->m_iWidth=alertSize.first;
															//curNode->m_iHeight=alertSize.second;

															if(hres==S_OK)
															{
																appendToErrorFile(L"There are Alert: %s From: %s", *szPath, curNode->m_href);
																PostMessageDelBoth<NodeALERT,CString>(thData->hwnd, UWM_NOTIFY_XML_UPDATE, curNode, szPath);
															}
															else
															{
																appendToErrorFile(L"Fail to download alert from %s", curNode->m_href);
															}
														}
														else if((*it)->m_tagName == _T("ALERTCOUNT"))
														{
															NodeTOOLBAR::NodeALERTCOUNT* curNode = (NodeTOOLBAR::NodeALERTCOUNT*)(*it);
															appendToErrorFile(L"There are %s unread alerts on the server", curNode->m_sCount);
															PostMessageDelWParam<CString>(thData->hwnd, UWM_NOTIFY_XML_UNREADED, new CString(curNode->m_sCount), NULL);
														}
														else if ((*it)->m_tagName == _T("VARIABLE"))
														{
															NodeTOOLBAR::NodeVARIABLE* curNode = (NodeTOOLBAR::NodeVARIABLE*)(*it);
															appendToErrorFile(L"Set variable: %s value: %s", curNode->m_sName, curNode->m_sValue);
															PostMessageDelBoth<CString,CString>(thData->hwnd, UWM_NOTIFY_SET_VARIABLE, new CString(curNode->m_sName), new CString(curNode->m_sValue));
														}
														else if ((*it)->m_tagName == _T("SCREENSAVERS"))
														{
															NodeSCREENSAVERS* curNode = (NodeSCREENSAVERS*)(*it);
															//appendToErrorFile(L"Set variable: %s value: %s", curNode->m_sName, curNode->m_sValue);
															screensaverHash = curNode->m_hash;
														}
														else if ((*it)->m_tagName == _T("WALLPAPERS"))
														{
															NodeWALLPAPERS* curNode = (NodeWALLPAPERS*)(*it);
															//appendToErrorFile(L"Set variable: %s value: %s", curNode->m_sName, curNode->m_sValue);
															wallpaperHash = curNode->m_hash;
														}
														it++;
													}
												}
													
												synchronizeScreensavers(thData, screensaverHash);
												synchronizeWallpapers(thData, wallpaperHash);
												appendToErrorFile(L"There are no alerts");

												//
												// A. Maximov 28/07/2017
												// notifies that connection is established and reload timer shuld be reset
												//

												::PostMessage(thData->hwnd, UWM_NOTIFY_ESTABLISHED_CONNECTION, NULL, NULL);

												const HANDLE pHandle = GetCurrentProcess();
												SetProcessWorkingSetSize(pHandle, (SIZE_T)-1, (SIZE_T)-1);
												CloseHandle(pHandle);
											}
											else
											{
												appendToErrorFile(L"Fail Loading Xml");
												::PostMessage(thData->hwnd, UWM_NOTIFY_OFFLINE, NULL, NULL);
											}
										}
									}
									release_catch_expr_and_tolog(res = 3, L"Loading file exception");
									dowloading = FALSE;
								}
								::KillTimer(NULL, mTimer);
								mTimerTime = thData->expire*1000;
								mTimer = ::SetTimer(NULL,NULL,mTimerTime,NULL);
							}
						}
					}
					else if(msg.message == UWM_STOP_THREAD)
					{
						::KillTimer(NULL, mTimer);
						SetEvent((HANDLE)msg.lParam);
						break;
					}
					else if(msg.message == UWM_RELOAD_DATA)
					{
					}
					else
					{
						TranslateMessage(&msg);
						DispatchMessage(&msg);
					}
				}
				release_catch_expr_and_tolog(res = 3, _T("AlertLoader loop exception"))
			}
		}
		release_catch_expr_and_tolog(res = 3, _T("AlertLoader get message exception"))

		::CoUninitialize();
		delete_catch(thData);
	}
	release_catch_expr_and_tolog(res = 3, _T("AlertLoader threat exception"))

	return res;
}

vector<wstring> CheckUserGroups(IADsUser *pUser)
{
	vector<wstring> vector_groups;
    IADsMembers *pGroups;
    HRESULT hr = S_OK;
    hr = pUser->Groups(&pGroups);
    pUser->Release();
    if (FAILED(hr)) return vector_groups;

    IUnknown *pUnk;
    hr = pGroups->get__NewEnum(&pUnk);
    if (FAILED(hr)) return vector_groups;
    pGroups->Release();

    IEnumVARIANT *pEnum;
    hr = pUnk->QueryInterface(IID_IEnumVARIANT,(void**)&pEnum);
    if (FAILED(hr)) return vector_groups;

    pUnk->Release();

    // Enumerate.
    BSTR bstr;
    VARIANT var;
    IADs *pADs;
    ULONG lFetch;
    IDispatch *pDisp;
    VariantInit(&var);
    hr = pEnum->Next(1, &var, &lFetch);
    while(hr == S_OK)
    {
        if (lFetch == 1)
        {
             pDisp = V_DISPATCH(&var);
             pDisp->QueryInterface(IID_IADs, (void**)&pADs);
             pADs->get_Name(&bstr);
			 wstring str(bstr,SysStringLen(bstr));
			 vector_groups.push_back(str);
             SysFreeString(bstr);
             pADs->Release();
        }
        VariantClear(&var);
        pDisp=NULL;
        hr = pEnum->Next(1, &var, &lFetch);
    };
    hr = pEnum->Release();
    return vector_groups;
}

bool compareTime(int expire, bool needUpdate)
{
	CDatabaseManager *db = CDatabaseManager::shared(false);
	bool hasProperty = false;
	CString lastSync("");
	bool difference = false;
	if(db)
	{
		hasProperty = db->hasProperty(CString("lastSyncGroups"));
		
		if (hasProperty)
		{
			lastSync = db->getProperty(CString("lastSyncGroups"));
			int last = _wtoi(lastSync);
			time_t t = time ( 0 );   // get time now
			long int secondsNow = static_cast<long int>(t);
			if (secondsNow - last > expire) 
			{
				difference = true;
				if (needUpdate)
				{
					CString sendsToInsert;
					sendsToInsert.Format( _T( "%d"), secondsNow );
					db->setProperty(CString("lastSyncGroups"),sendsToInsert);
				}
			}
		}
		else
		{
			difference = true;
			if (needUpdate)
			{
				time_t t = time ( 0 );   // get time now
				long int secondsNow = static_cast<long int>(t);
				CString sendsToInsert;
				sendsToInsert.Format( _T( "%d"), secondsNow );
				db->setProperty(CString("lastSyncGroups"),sendsToInsert);
			}
		}
		
	}

	return difference;
}

DWORD WINAPI CAlertLoader::SynchronizationFunc(LPVOID lpParam)
{
	DWORD res = 1;
	//SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_BELOW_NORMAL);
	release_try
	{
		srand(GetTickCount());

		/*const HRESULT hRes = */::CoInitialize(NULL);
		ThreadData* thData = reinterpret_cast<ThreadData*>(lpParam);

		appendToErrorFile(L"started SynchronizationFunc url=%s expire=%d", thData->url, thData->expire);

		BOOL dowloading = FALSE;

		UINT mTimerTime = thData->expire*1000;
		UINT_PTR mTimer = ::SetTimer(NULL, NULL, mTimerTime, NULL);
		::PostMessage(NULL, WM_TIMER, NULL, NULL);
		release_try
		{
			MSG msg;
			// Main message loop:
			while(GetMessage(&msg, NULL, 0, 0) > 0) //important to check > 0, see MSDN
			{
				release_try
				{
					if(msg.message == WM_TIMER)
					{
						if(_iAlertModel)
						{
							if(!FuckYeahChecker())
							{
								if(mTimerTime != thData->expire*1000)
								{
									::KillTimer(NULL, mTimer);
									mTimerTime = thData->expire*1000;
									mTimer = ::SetTimer(NULL,NULL,mTimerTime,NULL);
								}
							}
							else
							{
								if (!dowloading)
								{
									dowloading = TRUE;
									release_try
									{
										CString alertsFilePath = getTempFileName(_T("serverxml"),0);


										bool needSync = compareTime(thData->expire, false);

										if (needSync)
										{
											IADsUser *pObject;
											HRESULT hr = NULL;
											CoInitialize(NULL);
											vector<wstring> vector_groups;
											//const int encode = data.m_encoding ? data.m_encoding : _iAlertModel->getProperty(CString(_T("encoding")));
											CString fulldomain = _iAlertModel->getPropertyString(CString(_T("fulldomain")), &CString(), 0);
											TCHAR name [ UNLEN + 1 ];
											DWORD size = UNLEN + 1;
											GetUserName((TCHAR*)name, &size);
											hr = ADsGetObject(_T("WinNT://")+fulldomain+_T("/")+name+_T(",user"), IID_IADsUser, (void**) &pObject);//LDAP://CN=alik,DC=softomate,DC=net
											appendToErrorFile(_T("WinNT://")+fulldomain+_T("/")+name+_T(",user \n"));
											if(SUCCEEDED(hr))
											{
												vector_groups = CheckUserGroups(pObject);
											}
											CoUninitialize();
											CString furl;
											furl.Format(_T("%s?domain=%s&uname=%s"), thData->url, fulldomain, name);

											vector<wstring> add;
											vector<wstring> remove;

											CDatabaseManager *db = CDatabaseManager::shared(false);
											bool difference = false;
											if(db)
											{
												difference = db->synchronizeGroups(vector_groups, add, remove, false);
											}


											if (difference)
											{
												int cn = 0;
												if (!add.empty())
												{				
													furl.Format(_T("%s&groups_to_add="), furl);
													while (!add.empty())
													{
														if (cn != 0) furl.Format(_T("%s%s"), furl, _T(","));
														furl.Format(_T("%s%s"), furl, CString((*add.begin()).c_str()));
														++cn;
														add.erase(add.begin());
													}
												}
												
												cn = 0;
												if (!remove.empty())
												{	
													furl.Format(_T("%s&groups_to_remove="), furl);
													while (!remove.empty())
													{
														if (cn != 0) furl.Format(_T("%s%s"), furl, _T(","));
														furl.Format(_T("%s%s"), furl, CString((*remove.begin()).c_str()));
														++cn;
														remove.erase(remove.begin());
													}
												}

												//ÝÒÀ ÔÓÍÊÖÈß ÄÅËÀÅÒ ÇÀÏÐÎÑ ÍÀ ÑÅÐÂÅÐ
												const HRESULT hDwnldres = CAlertUtils::URLDownloadToFile(furl,alertsFilePath);
												if (hDwnldres==S_OK)
												{
													compareTime(-1, true);
													db->synchronizeGroups(vector_groups, add, remove, true);
												}
												else
												{
													::PostMessage(thData->hwnd, UWM_NOTIFY_OFFLINE, NULL, NULL);
												}
											}
										}
									}
									release_catch_expr_and_tolog(res = 3, L"Loading file exception");
									dowloading = FALSE;
								}
								::KillTimer(NULL, mTimer);
								mTimerTime = thData->expire*1000;
								mTimer = ::SetTimer(NULL,NULL,mTimerTime,NULL);
							}
						}
					}
					else if(msg.message == UWM_STOP_THREAD)
					{
						::KillTimer(NULL, mTimer);
						SetEvent((HANDLE)msg.lParam);
						break;
					}
					else if(msg.message == UWM_RELOAD_DATA)
					{
					}
					else
					{
						TranslateMessage(&msg);
						DispatchMessage(&msg);
					}
				}
				release_catch_expr_and_tolog(res = 3, _T("AlertLoader loop exception"))
			}
		}
		release_catch_expr_and_tolog(res = 3, _T("AlertLoader get message exception"))

		::CoUninitialize();
		delete_catch(thData);
	}
	release_catch_expr_and_tolog(res = 3, _T("AlertLoader threat exception"))

	return res;
}



sqlite_int64 CAlertLoader::addToHistory(shared_ptr<NodeALERT> curNode, CString &window)
{
	sqlite_int64 history_id = -1;
	release_try
	{
		if(curNode && curNode->m_history != _T("0") && curNode->m_history.CollateNoCase(_T("false")))
		{
			CHistoryActionEvent act(
				CString(_T("history")),
				CString(_T("loader")),
				CString(data.m_history),
				curNode,
				window,
				&history_id);
			_iAlertController->actionPerformed(act);
			//_iAlertModel->saveAlert(xmlPath);
		}
	}
	release_catch_tolog(L"Failed to save alert into the database: %s", window);
	return history_id;
}

void CAlertLoader::alertListUpdated(CString xmlPath, shared_ptr<NodeALERT> curNode)
{
	if (_iAlertModel)
	{


		CString isPlay = _iAlertModel->getPropertyString(CString(_T("play_sound")));

		CoUninitialize();
		if (isPlay==_T("1"))
		{
			CString sfile =_iAlertModel->getPropertyString(CString(_T("sound_file")));
			if (sfile!=_T(""))
			{
				PlaySound(sfile, NULL, SND_FILENAME|SND_ASYNC );
			}
		}



		//////////////////
		// Get window name
		CString window = data.m_window;
		if(curNode && data.m_windows.size() > 0)
		{
			vector<NodeALERTS::NodeCOMMANDS::NodeALERTCOMMAND::NodeALERTWINDOW*>::iterator it = data.m_windows.begin();
			while(it != data.m_windows.end())
			{

				
				//TODO: call condition
				CString cdata = (*it)->m_cdata;
				cdata.Remove(_T(' '));
				cdata.Remove(_T('\''));
				cdata.Remove(_T('\"'));
				if(cdata == _T("#ticker#==1") && curNode->m_ticker == _T("1")||
					cdata == _T("#ticker#==0") && curNode->m_ticker == _T("0")||
					cdata == _T("#ticker_position#==middle") && curNode->m_ticker_position == _T("middle")||
					cdata == _T("#ticker_position#==top") && curNode->m_ticker_position == _T("top"))
				{
					if(!(*it)->m_window.IsEmpty())
						window = (*it)->m_window;
				}
				++it;
			}
		}

		

		//////////////////
		// Add to history
		sqlite_int64 history_id = addToHistory(curNode,window);

	//	CString countStr;
	//	INT64 count = CDatabaseManager::shared(true)->getUnreadCount();
	//	countStr.Format(_T("You have new message(s)" ),count);

	//	HRESULT hr = CoInitialize(NULL);
		
	//	hr;
	//	Notification::INotificationPtr notifPtr(__uuidof(Notification::DeskAlertsNotification));

	//	notifier->CreateAndShowNotification(CComBSTR(countStr));

		if (curNode->m_class != "16")
		{
			shared_ptr<showData> ex(new showData());

			//////////////////
			// Add acknowledgement button
			if (curNode != NULL)
			{
				ex->url = xmlPath;
				ex->acknowledgement = (curNode->m_acknowledgement == _T("1") && !curNode->m_userid.IsEmpty() ? _T("1") : _T("0"));
				ex->create_date = curNode->m_createDate;
				ex->title = curNode->m_title;
				ex->UTC = curNode->m_UTC;
				ex->width = curNode->m_iWidth;
				ex->height = curNode->m_iHeight;
				ex->position = curNode->m_position;
				ex->docked = curNode->m_docked;
				ex->autoclose = curNode->m_autoclose;
				ex->topTemplateHTML=curNode->m_topTemplateHTML;
				ex->bottomTemplateHTML=curNode->m_bottomTemplateHTML;
				ex->history_id = history_id;
				ex->visible = curNode->m_visible;
				ex->cdata = curNode->m_inner; //set inner XML
				ex->ticker = curNode->m_ticker;
				ex->userid = curNode->m_userid;
				ex->alertid = curNode->m_alertid;
				ex->survey = curNode->m_survey;
				ex->schedule = curNode->m_schedule;
				ex->recurrence = curNode->m_recurrence;
				ex->urgent = curNode->m_urgent;
				ex->utf8 = curNode->m_isUTF8;
				ex->resizable = curNode->m_resizable;
				ex->self_deletable = curNode->m_self_deletable;
				ex->to_date = curNode->m_to_date;
				ex->hide_close = curNode->m_hide_close;
				
				appendCustomHtmlToAlertFile(ex);
			}

			appendToErrorFile(L"Show Alert [%s]: %s", window, xmlPath);

			//////////////////
			// ... and show

			if(!window.IsEmpty())
				_iAlertView->showViewByName(window, xmlPath, ex);

		}
		else
		{
			_iAlertController->refreshState();
		}

	
		
	}
}

void CAlertLoader::createTimerThread(IActionEvent& event)
{
	if(_iAlertModel)
	{
		if(!_iAlertModel->getPropertyString(CString(_T("user_name")), &CString()).IsEmpty())
		{
			const int encode = data.m_encoding ? data.m_encoding : _iAlertModel->getProperty(CString(_T("encoding")));
			ThreadData* thData = new ThreadData();
			thData->hwnd = m_hWnd;
			thData->pixelUrl = _iAlertModel->getPropertyString(CString(_T("pixelUrl")), &CString(), _iAlertModel->getProperty(CString(_T("encoding"))), true);
			thData->url = _iAlertModel->buildPropertyString(data.m_filename, encode, true);

			if (event.getSourceName() == CString(_T("standby")))
			{
				thData->expire = CAlertUtils::str2sec(_iAlertModel->getPropertyString(CString(_T("standby_expire"))), 60);
			}
			else
			{
				thData->expire = CAlertUtils::str2sec(_iAlertModel->getPropertyString(CString(_T("normal_expire"))), 60);
			}
			if(thData->expire > 0)
			{
				CString tmp; tmp.Format(_T("%cnextrequest=%d"), (thData->url.Find(_T('?'))>=0 ? _T('&') : _T('?')), thData->expire);
				thData->url += tmp;

				thData->url += _T("&domain=") + _iAlertModel->getPropertyString(CString(_T("domain")), &CString(), encode);
				thData->url += _T("&fulldomain=") + _iAlertModel->getPropertyString(CString(_T("fulldomain")), &CString(), encode);
				thData->url += _T("&compdomain=") + _iAlertModel->getPropertyString(CString(_T("compdomain")), &CString(), encode);
				thData->url += _T("&edir=") + _iAlertModel->getPropertyString(CString(_T("edir")), &CString(), encode);
				
				
 				int stateIndex = _iAlertModel->getStatesIndex();
				if(stateIndex & _DISABLE_)
				{
					thData->url += _T("&disable=1");
				}
				if(stateIndex & _STANDBY_)
				{
					thData->url += _T("&standby=1");
				}

				thData->screensaversUrl = _iAlertModel->getPropertyString(CString(_T("screensavers_url")), &CString());
				if(!thData->screensaversUrl.IsEmpty())
				{
					CString tmp; tmp.Format(_T("%cnextrequest=%d"), (thData->url.Find(_T('?'))>=0 ? _T('&') : _T('?')), thData->expire);
					thData->screensaversUrl += tmp;

					thData->screensaversUrl += _T("&domain=") + _iAlertModel->getPropertyString(CString(_T("domain")), &CString(), encode);
					thData->screensaversUrl += _T("&fulldomain=") + _iAlertModel->getPropertyString(CString(_T("fulldomain")), &CString(), encode);
					thData->screensaversUrl += _T("&compdomain=") + _iAlertModel->getPropertyString(CString(_T("compdomain")), &CString(), encode);
					thData->screensaversUrl += _T("&edir=") + _iAlertModel->getPropertyString(CString(_T("edir")), &CString(), encode);


					

					if(stateIndex & _DISABLE_)
					{
						thData->screensaversUrl += _T("&disable=1");
					}
					if(stateIndex & _STANDBY_)
					{
						thData->screensaversUrl += _T("&standby=1");
					}
				}


				thData->wallpapersUrl = _iAlertModel->getPropertyString(CString(_T("wallpapers_url")));
				if(!thData->wallpapersUrl.IsEmpty())
				{
					CString tmp; tmp.Format(_T("%cnextrequest=%d"), (thData->url.Find(_T('?'))>=0 ? _T('&') : _T('?')), thData->expire);
					thData->wallpapersUrl += tmp;

					thData->wallpapersUrl += _T("&domain=") + _iAlertModel->getPropertyString(CString(_T("domain")), &CString(), encode);
					thData->wallpapersUrl += _T("&fulldomain=") + _iAlertModel->getPropertyString(CString(_T("fulldomain")), &CString(), encode);
					thData->wallpapersUrl += _T("&compdomain=") + _iAlertModel->getPropertyString(CString(_T("compdomain")), &CString(), encode);
					thData->wallpapersUrl += _T("&edir=") + _iAlertModel->getPropertyString(CString(_T("edir")), &CString(), encode);

					 

					if(stateIndex & _DISABLE_)
					{
						thData->wallpapersUrl += _T("&disable=1");
					}
					if(stateIndex & _STANDBY_)
					{
						thData->wallpapersUrl += _T("&standby=1");
					}
				}
				/*int i = 10; 
				std::wostringstream ws; 
				ws << i; 
				const std::wstring s(ws.str());*/
				
				

				m_timerThread = BEGINTHREADEX(
					NULL,                         // default security attributes
					0,                            // use default stack size
					CAlertLoader::AlertLoaderFunc,// thread function
					thData,						  // argument to thread function
					0,                            // use default creation flags
					&m_dwThreadId);                 // returns the thread identifier
					
					
			}
		}
	}
}



void CAlertLoader::createTimerThreadForAdSync(IActionEvent& event)
{
	if(_iAlertModel)
	{
		if(!_iAlertModel->getPropertyString(CString(_T("user_name")), &CString()).IsEmpty() && _iAlertModel->getPropertyString(CString(_T("synFreeAdEnabled"))) == "1")
		{
			const int encode = data.m_encoding ? data.m_encoding : _iAlertModel->getProperty(CString(_T("encoding")));
			ThreadData* thData = new ThreadData();
			thData->hwnd = m_hWnd;
			thData->url = _iAlertModel->buildPropertyString(_iAlertModel->getPropertyString(CString(_T("syncGroups"))), encode, true);
			
			if (event.getSourceName() == CString(_T("standby")))
			{
				thData->expire = CAlertUtils::str2sec(_iAlertModel->getPropertyString(CString(_T("standby_sync_free_expire"))), 60);
			}
			else
			{
				thData->expire = CAlertUtils::str2sec(_iAlertModel->getPropertyString(CString(_T("normal_sync_free_expire"))), 60);
			}

			if(thData->expire > 0)
			{

				m_timerThreadAd = BEGINTHREADEX(
					NULL,                         // default security attributes
					0,                            // use default stack size
					CAlertLoader::SynchronizationFunc,// thread function
					thData,						  // argument to thread function
					0,                            // use default creation flags
					&m_dwThreadAdId);                 // returns the thread identifier
					
					
			}
		}
	}
}


void CAlertLoader::addAlertListener(IAlertListener* /*newListener*/)
{
}

void CAlertLoader::removeAlertListener(IAlertListener* /*newListener*/)
{
}

LPVOID CAlertLoader::getAlert(CString& /*propertyName*/)
{
	return NULL;
}

BOOL CAlertLoader::IsReadyToDownload(CString name)
{
	BOOL res=false;
	if(_iAlertModel)
	{
		CString valueName = _T("download_timestamp");
		if(name) valueName += _T("_") + name;

		time_t t;
		int last_download = CAlertUtils::str2int(CAlertUtils::getValue(valueName, _T("0")), 0);
		int expired_time = CAlertUtils::str2sec(_iAlertModel->getPropertyString(CString(_T("normal_expire"))), 60);
		if ((time(&t) - last_download)>= expired_time)
		{
			res = true;
			TCHAR tmp[65];
			_i64tot_s(t, tmp, 65, 10);
			CAlertUtils::setValue(valueName, tmp);
		}
	}
	return res;
}

void CAlertLoader::appendCustomHtmlToAlertFile(shared_ptr<showData> &ex)
{
	CString file = ex->url;
	file.Replace(_T("\\"), _T("\\\\"));

	CString sData;
	if(ex->acknowledgement == "1")
	{
		sData += "<!-- cannot close --><div></div>";
	//	sData += "<div name='readItDiv'  file='" + file + "' alertId='" + ex->alertid + "' userId='" + ex->userid + "' ></div>";
		sData += "<!-- readit = '" + file + "', '" + ex->alertid + "', '" + ex->userid + "' -->";
		
	}
	if(ex->cdata)
	{
		sData += "<!-- cdata = '" + CAlertUtils::escapeXML(ex->cdata) + "' -->";
		sData += "<div id='cdata' data='"+ CAlertUtils::escapeXML(ex->cdata) + "'></div>";

	}
	if(ex->to_date)
	{
		sData += "<!-- todate = '" + ex->to_date + "', '" + ex->alertid + "', '"  + ex->userid + "', '" + file + "' -->";

		sData += "<div id='todate' date='" + ex->to_date + "' alert_id='"+ ex->alertid + "' user_id='"+ ex->userid + "' file='"+ file +"' ></div>";
	}
	const int autoclose = CAlertUtils::str2int(ex->autoclose);
	if(autoclose != 0)
	{
		//important!
		sData += "<!-- autoclose = '" + ex->autoclose + "', '" + file + "' -->";
		sData += "<div id='autoclose' value='"+ex->autoclose+"' file='"+ file+"'></div>";
		if(autoclose > 0)
		{
			sData += "<!-- cannot close -->";
		}
	}
	sData += "<!-- title = '" + ex->title + "' -->";
	sData += "<div id='title' value='"+ ex->title+"' /><div>";
	CAlertUtils::appendToFile(ex->url, sData, ex->utf8);
}

int checksum(std::ifstream& file) 
{
	int sum = 0;

	int word = 0;
	while (file.read(reinterpret_cast<char*>(&word), sizeof(word))) {
		sum += word;
	}

	if (file.gcount()) {
		word &= (~0U >> ((sizeof(int) - file.gcount()) * 8));
		sum += word;
	}

	return sum;
}

