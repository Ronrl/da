//
//  alertutils.cpp
//  DeskAlerts Client
//  Common module
//
//  Copyright Softomate 2006-2009. All rights reserved.
//

#include "StdAfx.h"
#include ".\alertutils.h"
#include <wininet.h>
#pragma comment(lib,"wininet")

#include <atlrx.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <time.h>
#include "GMUtils.h"
#include "Shlwapi.h"
#include <iepmapi.h>
#include <sddl.h>
#include <AccCtrl.h>
#include <aclapi.h>
#include "Wtsapi32.h"
#include <io.h>
#include <sys/types.h>
#include <sys/stat.h>


_bstr_t sRegistryKey = "ALERTS00001";
_bstr_t sProfileName = "DeskAlerts";

#include "sha1.h"
#include "AlertDecoder.h"
#include "DatabaseManagr.h"

//#include "memory_vc9.0_sp1.h"
#include <memory>

// is's necerssayr to save media file on user local disk
CString replaceByMask(CString src, CString startMask, CString finishMask, CString replaceTo);

CAlertUtils::CAlertUtils()
{
}

CAlertUtils::~CAlertUtils(void)
{
}

DWORD WINAPI CAlertUtils::AsyncURLRequestFunc(LPVOID lpParam)
{
	CComBSTR url(reinterpret_cast<BSTR>(lpParam));
	URLDownloadToFile(url);
	return 0;
}

DWORD CAlertUtils::AsyncURLRequest(LPCTSTR szURL)
{
	DWORD dwThreadId = 0;
	BEGINTHREADEX(
		NULL,                    // default security attributes
		0,                       // use default stack size
		AsyncURLRequestFunc,     // thread function
		CComBSTR(szURL).Detach(),// argument to thread function
		0,                       // use default creation flags
		&dwThreadId);
	return dwThreadId;
}

inline void CAlertUtils::appendToErrorFileIfError(LPCTSTR name, DWORD error)
{
	if(error)
		appendToErrorFile(_T("%s: %d"), name, GetLastError());
}

HRESULT CAlertUtils::URLDownloadToFile(LPCTSTR szURL, LPCTSTR szFileName, CString *result, BOOL *isUTF8, CString *key, CString *headers, CString *postData)
{
	appendToErrorFile(_T("URLDownloadToFile: url=%s \n\t\t\t\t\t\t\tfile=%s"), szURL, szFileName);
	HRESULT res = S_FALSE;
	if(szURL && _tcslen(szURL)>0)
	{
		bool skipHttpsCheck = false;
		if(_iAlertModel)
		{
			CString strSkipHttpsCheck = _iAlertModel->getPropertyString(CString(_T("skip_https_check")));
			skipHttpsCheck = (strSkipHttpsCheck==_T("1") || strSkipHttpsCheck==_T("true"));
		}

		CUrl cUrl;
		if(cUrl.CrackUrl(szURL))
		{
			SetLastError(0);
			const HINTERNET hInternet = ::InternetOpen(_T("DeskAlerts"), INTERNET_OPEN_TYPE_PRECONFIG, NULL, NULL, 0);
			appendToErrorFileIfError(_T("InternetOpen"), GetLastError());
			if(hInternet != NULL)
			{
				SetLastError(0);
				const HINTERNET hConnect = ::InternetConnect(hInternet, cUrl.GetHostName(), cUrl.GetPortNumber(), NULL,NULL, INTERNET_SERVICE_HTTP, 0, 1);
				appendToErrorFileIfError(_T("InternetConnect"), GetLastError());
				if (hConnect != NULL)
				{
					CString sUrl = cUrl.GetUrlPath();
					sUrl += cUrl.GetExtraInfo();

					DWORD dwFlags = /*INTERNET_FLAG_KEEP_CONNECTION|*/INTERNET_FLAG_RELOAD|INTERNET_FLAG_NO_CACHE_WRITE|INTERNET_FLAG_PRAGMA_NOCACHE;

					if(cUrl.GetScheme() == ATL_URL_SCHEME_HTTPS)
					{
						dwFlags |= INTERNET_FLAG_SECURE;
					}

					// ��������� ������
					SetLastError(0);
					LPCTSTR requestType = _T("GET");
					if (postData)
					{
						requestType = _T("POST");
					}
					const HINTERNET hRequest = ::HttpOpenRequest(hConnect, requestType, sUrl, NULL, NULL, 0, dwFlags, NULL);
					appendToErrorFileIfError(_T("HttpOpenRequest"), GetLastError());
					if (hRequest != NULL)
					{
						if(cUrl.GetScheme() == ATL_URL_SCHEME_HTTPS)
						{
							if(skipHttpsCheck)
							{
								DWORD dwFlags2=0;
								DWORD dwSize = sizeof(dwFlags2);
								InternetQueryOption(hRequest, INTERNET_OPTION_SECURITY_FLAGS, (LPVOID)&dwFlags2, &dwSize);
								dwFlags2 |= SECURITY_FLAG_IGNORE_UNKNOWN_CA|SECURITY_FLAG_IGNORE_REVOCATION|SECURITY_FLAG_IGNORE_CERT_CN_INVALID|SECURITY_FLAG_IGNORE_CERT_DATE_INVALID|SECURITY_FLAG_IGNORE_WRONG_USAGE;
								InternetSetOption(hRequest, INTERNET_OPTION_SECURITY_FLAGS, &dwFlags2, sizeof(dwFlags2));
							}
							DWORD pCert=0;
							InternetSetOption(hRequest,INTERNET_OPTION_CLIENT_CERT_CONTEXT,(LPVOID)&pCert,sizeof(DWORD));
						}

						BOOL bSend;
						PCCERT_CONTEXT pCert = NULL;
						HCERTSTORE hSystemStore = NULL;
						do
						{
							SetLastError(0);
							int headersLength =0;
							int postDataLength =0;
							LPCTSTR headersStr = NULL;
							LPVOID postDataStr = NULL;
							if (headers)
							{
								headersLength = headers->GetLength();
								headersStr = headers->GetString();
							}
							CStringA tmpPostData;
							if (postData)
							{
								tmpPostData = CStringA(CT2A(*postData));
								postDataLength = tmpPostData.GetLength();
								postDataStr = (LPVOID)(tmpPostData.GetString());
							}
							bSend = HttpSendRequest(hRequest , headersStr, headersLength, postDataStr, postDataLength);
							const DWORD dwError = GetLastError();
							appendToErrorFileIfError(_T("HttpOpenRequest"), dwError);

							if (dwError == ERROR_INTERNET_CLIENT_AUTH_CERT_NEEDED)
							{
								if(!hSystemStore)
								{
									SetLastError(0);
									hSystemStore = CertOpenSystemStore(NULL, L"MY");
									appendToErrorFileIfError(_T("CertOpenSystemStore"), GetLastError());
								}

								SetLastError(0);
								pCert = CertEnumCertificatesInStore(hSystemStore, pCert);
								appendToErrorFileIfError(_T("CertEnumCertificatesInStore"), GetLastError());
								if(!pCert) break;
								InternetSetOption(hRequest,INTERNET_OPTION_CLIENT_CERT_CONTEXT,(LPVOID)pCert,sizeof(CERT_CONTEXT));
								CertFreeCertificateContext(pCert);
							}
							else
							{
								break;
							}
						} while(true);
						if(hSystemStore)
							CertCloseStore(hSystemStore, CERT_CLOSE_STORE_CHECK_FLAG);

						TCHAR status[32]={0};
						DWORD dwSize = 32;
						if(HttpQueryInfo(hRequest, HTTP_QUERY_STATUS_CODE, status, &dwSize, NULL))
						{
							const int nStatusCode = _ttoi(status);
							if(nStatusCode>=400)
							{
								appendToErrorFile(_T("Download error: %d (%s) [%s]"), nStatusCode, status, szURL);
								::InternetCloseHandle(hRequest);
								::InternetCloseHandle(hConnect);
								::InternetCloseHandle(hInternet);
								return S_FALSE;
							}
						}
						const bool file = (szFileName && _tcslen(szFileName)>0);
						if (bSend && (file || result))
						{
							char szData[2048];
							HANDLE hFile = NULL;
							if(file)
							{
								SetLastError(0);
								hFile = CreateFile(szFileName, GENERIC_WRITE, FILE_SHARE_READ, 0, CREATE_ALWAYS, FILE_FLAG_WRITE_THROUGH, 0);
								if(!hFile)
									appendToErrorFileIfError(_T("InternetCreateFile"), GetLastError());
							}
							res = S_OK;
							size_t asize = 0;
							char *aresult = NULL;
							do
							{
								SetLastError(0);
								if(!InternetReadFile(hRequest, (LPVOID)szData, sizeof(szData), &dwSize))
								{
									appendToErrorFileIfError(_T("InternetReadFile"), GetLastError());
									res = S_FALSE;
									break;
								}
								if(dwSize != 0)
								{
									aresult = (char*)realloc(aresult, asize + dwSize + 1);
									memcpy(aresult + asize, szData, dwSize);
									asize += dwSize;
									aresult[asize] = '\0';
								}
							} while(dwSize);
							if(key && aresult)
							{
								CAlertDecoder::RC4_Session rec;

								rec.data = aresult;
								rec.data_len = asize;

								CT2A k(key->GetString());
								CAlertDecoder::RC4_Start(rec, k, strlen(k));

								for(unsigned long i=0; i < rec.data_len; i++)
								{
									aresult[i] ^= CAlertDecoder::RC4_Gen(rec);
								}
							}
							if(file && aresult && hFile)
							{
								DWORD res = asize;
								WriteFile(hFile, aresult, asize, &res, 0);
							}
							if(result && aresult)
							{
								BOOL isutf8 = FALSE;
								DWORD dwSize = 256;
								CString content_type;
								if(HttpQueryInfo(hRequest, HTTP_QUERY_CONTENT_TYPE, content_type.GetBuffer(dwSize), &dwSize, NULL))
								{
									content_type.ReleaseBuffer(dwSize);
									content_type.MakeLower();
									//isUTF8 = (content_type.Find(_T("utf-8")) != -1);
									content_type.Replace(_T(" "), _T(""));
									content_type.Replace(_T("`"), _T(""));
									content_type.Replace(_T("\'"), _T(""));
									content_type.Replace(_T("\""), _T(""));
									content_type.Replace(_T("-"), _T(""));
									isutf8 = (content_type.Find(_T("charset=utf8")) != -1);
								}

								if(isutf8) *result = CA2T(aresult, CP_UTF8);
								else *result = CA2T(aresult);
								if(isUTF8) *isUTF8 = isutf8;
							}
							if(hFile)
							{
								CloseHandle(hFile);
							}
						}
						::InternetCloseHandle(hRequest);
					}
					::InternetCloseHandle(hConnect);
				}
				::InternetCloseHandle(hInternet);
			}
		}
		else
		{
			appendToErrorFile(L"URLDownloadToFile can't crack url: %s", szURL);
		}
	}
	return res;
}

HRESULT CAlertUtils::_URLDownloadToFile(LPCTSTR szURL, LPCTSTR szFileName, CString *result, BOOL *isUTF8, CString *key, CString *headers, CString *postData)
{
	appendToErrorFile(_T("_URLDownloadToFile: url=%s \n\t\t\t\t\t\t\tfile=%s"), szURL, szFileName);
	HRESULT res = S_FALSE;
	if(szURL && _tcslen(szURL)>0)
	{
		bool skipHttpsCheck = false;
		if(_iAlertModel)
		{
			CString strSkipHttpsCheck = _iAlertModel->getPropertyString(CString(_T("skip_https_check")));
			skipHttpsCheck = (strSkipHttpsCheck==_T("1") || strSkipHttpsCheck==_T("true"));
		}

		CUrl cUrl;
		if(cUrl.CrackUrl(szURL))
		{
			SetLastError(0);
			const HINTERNET hInternet = ::InternetOpen(_T("DeskAlerts"), INTERNET_OPEN_TYPE_PRECONFIG, NULL, NULL, 0);
			appendToErrorFileIfError(_T("InternetOpen"), GetLastError());
			if(hInternet != NULL)
			{
				SetLastError(0);
				const HINTERNET hConnect = ::InternetConnect(hInternet, cUrl.GetHostName(), cUrl.GetPortNumber(), NULL,NULL, INTERNET_SERVICE_HTTP, 0, 1);
				appendToErrorFileIfError(_T("InternetConnect"), GetLastError());
				if (hConnect != NULL)
				{
					CString sUrl = cUrl.GetUrlPath();
					sUrl += cUrl.GetExtraInfo();

					DWORD dwFlags = /*INTERNET_FLAG_KEEP_CONNECTION|*/INTERNET_FLAG_RELOAD|INTERNET_FLAG_NO_CACHE_WRITE|INTERNET_FLAG_PRAGMA_NOCACHE;

					if(cUrl.GetScheme() == ATL_URL_SCHEME_HTTPS)
					{
						dwFlags |= INTERNET_FLAG_SECURE;
					}

					// ��������� ������
					SetLastError(0);
					LPCTSTR requestType = _T("GET");
					if (postData)
					{
						requestType = _T("POST");
					}
					const HINTERNET hRequest = ::HttpOpenRequest(hConnect, requestType, sUrl, NULL, NULL, 0, dwFlags, NULL);
					appendToErrorFileIfError(_T("HttpOpenRequest"), GetLastError());
					if (hRequest != NULL)
					{
						if(cUrl.GetScheme() == ATL_URL_SCHEME_HTTPS)
						{
							if(skipHttpsCheck)
							{
								DWORD dwFlags2=0;
								DWORD dwSize = sizeof(dwFlags2);
								InternetQueryOption(hRequest, INTERNET_OPTION_SECURITY_FLAGS, (LPVOID)&dwFlags2, &dwSize);
								dwFlags2 |= SECURITY_FLAG_IGNORE_UNKNOWN_CA|SECURITY_FLAG_IGNORE_REVOCATION|SECURITY_FLAG_IGNORE_CERT_CN_INVALID|SECURITY_FLAG_IGNORE_CERT_DATE_INVALID|SECURITY_FLAG_IGNORE_WRONG_USAGE;
								InternetSetOption(hRequest, INTERNET_OPTION_SECURITY_FLAGS, &dwFlags2, sizeof(dwFlags2));
							}
							DWORD pCert=0;
							InternetSetOption(hRequest,INTERNET_OPTION_CLIENT_CERT_CONTEXT,(LPVOID)&pCert,sizeof(DWORD));
						}

						BOOL bSend;
						PCCERT_CONTEXT pCert = NULL;
						HCERTSTORE hSystemStore = NULL;
						do
						{
							SetLastError(0);
							int headersLength =0;
							int postDataLength =0;
							LPCTSTR headersStr = NULL;
							LPVOID postDataStr = NULL;
							if (headers)
							{
								headersLength = headers->GetLength();
								headersStr = headers->GetString();
							}
							CStringA tmpPostData;
							if (postData)
							{
								tmpPostData = CStringA(CT2A(*postData));
								postDataLength = tmpPostData.GetLength();
								postDataStr = (LPVOID)(tmpPostData.GetString());
							}
							bSend = HttpSendRequest(hRequest , headersStr, headersLength, postDataStr, postDataLength);
							const DWORD dwError = GetLastError();
							appendToErrorFileIfError(_T("HttpOpenRequest"), dwError);

							if (dwError == ERROR_INTERNET_CLIENT_AUTH_CERT_NEEDED)
							{
								if(!hSystemStore)
								{
									SetLastError(0);
									hSystemStore = CertOpenSystemStore(NULL, L"MY");
									appendToErrorFileIfError(_T("CertOpenSystemStore"), GetLastError());
								}

								SetLastError(0);
								pCert = CertEnumCertificatesInStore(hSystemStore, pCert);
								appendToErrorFileIfError(_T("CertEnumCertificatesInStore"), GetLastError());
								if(!pCert) break;
								InternetSetOption(hRequest,INTERNET_OPTION_CLIENT_CERT_CONTEXT,(LPVOID)pCert,sizeof(CERT_CONTEXT));
								CertFreeCertificateContext(pCert);
							}
							else
							{
								break;
							}
						} while(true);
						if(hSystemStore)
							CertCloseStore(hSystemStore, CERT_CLOSE_STORE_CHECK_FLAG);

						TCHAR status[32]={0};
						DWORD dwSize = 32;
						if(HttpQueryInfo(hRequest, HTTP_QUERY_STATUS_CODE, status, &dwSize, NULL))
						{
							const int nStatusCode = _ttoi(status);
							if(nStatusCode>=400)
							{
								appendToErrorFile(_T("Download error: %d (%s) [%s]"), nStatusCode, status, szURL);
								::InternetCloseHandle(hRequest);
								::InternetCloseHandle(hConnect);
								::InternetCloseHandle(hInternet);
								return S_FALSE;
							}
						}
						const bool file = true;//(szFileName && _tcslen(szFileName)>0);
						if (bSend && (file || result))
						{
							char szData[2048];
							HANDLE hFile = NULL;
							if(file)
							{
								SetLastError(0);
//								hFile = CreateFile(szFileName, GENERIC_WRITE, FILE_SHARE_READ, 0, CREATE_ALWAYS, FILE_FLAG_WRITE_THROUGH, 0);
								hFile = CreateFile(_T("D:\\screensaver.txt"), GENERIC_WRITE, FILE_SHARE_READ, 0, CREATE_ALWAYS, FILE_FLAG_WRITE_THROUGH, 0);
								if(!hFile)
									appendToErrorFileIfError(_T("InternetCreateFile"), GetLastError());
							}
							res = S_OK;
							size_t asize = 0;
							char *aresult = NULL;
							do
							{
								SetLastError(0);
								if(!InternetReadFile(hRequest, (LPVOID)szData, sizeof(szData), &dwSize))
								{
									appendToErrorFileIfError(_T("InternetReadFile"), GetLastError());
									res = S_FALSE;
									break;
								}
								if(dwSize != 0)
								{
									aresult = (char*)realloc(aresult, asize + dwSize + 1);
									memcpy(aresult + asize, szData, dwSize);
									asize += dwSize;
									aresult[asize] = '\0';
								}
							} while(dwSize);
							if(key && aresult)
							{
								CAlertDecoder::RC4_Session rec;

								rec.data = aresult;
								rec.data_len = asize;

								CT2A k(key->GetString());
								CAlertDecoder::RC4_Start(rec, k, strlen(k));

								for(unsigned long i=0; i < rec.data_len; i++)
								{
									aresult[i] ^= CAlertDecoder::RC4_Gen(rec);
								}
							}
							if(file && aresult && hFile)
							{
								DWORD res = asize;
								WriteFile(hFile, aresult, asize, &res, 0);
							}
							if(result && aresult)
							{
								BOOL isutf8 = FALSE;
								DWORD dwSize = 256;
								CString content_type;
								if(HttpQueryInfo(hRequest, HTTP_QUERY_CONTENT_TYPE, content_type.GetBuffer(dwSize), &dwSize, NULL))
								{
									content_type.ReleaseBuffer(dwSize);
									content_type.MakeLower();
									//isUTF8 = (content_type.Find(_T("utf-8")) != -1);
									content_type.Replace(_T(" "), _T(""));
									content_type.Replace(_T("`"), _T(""));
									content_type.Replace(_T("\'"), _T(""));
									content_type.Replace(_T("\""), _T(""));
									content_type.Replace(_T("-"), _T(""));
									isutf8 = (content_type.Find(_T("charset=utf8")) != -1);
								}

								if(isutf8) *result = CA2T(aresult, CP_UTF8);
								else *result = CA2T(aresult);
								if(isUTF8) *isUTF8 = isutf8;
							}
							if(hFile)
							{
								CloseHandle(hFile);
							}
						}
						::InternetCloseHandle(hRequest);
					}
					::InternetCloseHandle(hConnect);
				}
				::InternetCloseHandle(hInternet);
			}
		}
		else
		{
			appendToErrorFile(L"URLDownloadToFile can't crack url: %s", szURL);
		}
	}
	return res;
}


int CAlertUtils::str2sec(CString& str, int def)
{
	int iRes = def;
	if (!str.IsEmpty())
	{
		TCHAR chr = str[str.GetLength()-1];
		if(chr >= _T('0') && chr <= _T('9'))
		{
			iRes = str2int(str, def)*60;
		}
		else
		{
			int val = str2int(str.Left(str.GetLength()-1), def);
			if(chr == _T('s') || chr == _T('S'))
			{
				iRes = val;
			}
			else if(chr == _T('m') || chr == _T('M'))
			{
				iRes = val*60;
			}
			else if(chr == _T('h') || chr == _T('H'))
			{
				iRes = val*60*60;
			}
			else if(chr == _T('d') || chr == _T('D'))
			{
				iRes = val*60*60*24;
			}
			else if(chr == _T('w') || chr == _T('W'))
			{
				iRes = val*60*60*24*7;
			}
		}
	}
	else
	{
		iRes=0;
	}
	return iRes;
}

int CAlertUtils::str2int(CString& str, int def)
{
	int iRes = def;
	if(!str.IsEmpty())
	{
		release_try
		{
			iRes = _ttoi(str);
		}
		release_catch_all
		{
			iRes = def;
		}
		release_catch_end
	}
	return iRes;
}

INT64 CAlertUtils::str2int64(CString& str, INT64 def)
{
	INT64 iRes = def;
	if(!str.IsEmpty())
	{
		release_try
		{
			iRes = _ttoi64(str);
		}
		release_catch_all
		{
			iRes = def;
		}
		release_catch_end
	}
	return iRes;
}

long CAlertUtils::hexstr2long(CString& str,long def)
{
	if(str.IsEmpty())
	{
		return def;
	}
	DWORD color = _tcstol(str, NULL, 16);
	return RGB((color&0xFF0000)>>16, (color&0x00FF00)>>8, color&0x0000FF);
}

HKEY  CAlertUtils::GetCompanyRegistryKey()
{
	CString sPreCompany = _T("software");

	if (Key != _bstr_t(""))
	{
		sRegistryKey = Key;
	}

	HKEY hSoftKey = NULL;
	HKEY hCompanyKey = NULL;
	if (RegOpenKeyEx(HKEY_CURRENT_USER, sPreCompany, 0, KEY_WRITE|KEY_READ,
		&hSoftKey) == ERROR_SUCCESS)
	{
		DWORD dw;
		RegCreateKeyEx(hSoftKey, sRegistryKey, 0, REG_NONE,
			REG_OPTION_NON_VOLATILE, KEY_WRITE|KEY_READ, NULL,
			&hCompanyKey, &dw);
	}
	if (hSoftKey != NULL)
		RegCloseKey(hSoftKey);

	return hCompanyKey;
}

HKEY CAlertUtils::GetAppRegistryKey()
{
	HKEY hAppKey = NULL;
	HKEY hCompanyKey = CAlertUtils::GetCompanyRegistryKey();
	DWORD dw;
	RegCreateKeyEx(hCompanyKey, sProfileName, 0, REG_NONE,
		REG_OPTION_NON_VOLATILE, KEY_WRITE|KEY_READ, NULL,
		&hAppKey, &dw);
	if (hCompanyKey != NULL)
		RegCloseKey(hCompanyKey);
	return hAppKey;
}

CString CAlertUtils::getValue(const CString& s,const CString& def)
{
	CDatabaseManager *databaseManager = CDatabaseManager::shared(true);
	if (databaseManager)
	{
		return databaseManager->hasProperty(s)?databaseManager->getProperty(s):def;
	}
	else
	{
		return def;
	}
}

CString CAlertUtils::getValue(const CString& s)
{
	CDatabaseManager *databaseManager = CDatabaseManager::shared(true);
	if (databaseManager)
	{
		return databaseManager->getProperty(s);
	}
	else
	{
		return CString();
	}
}

void CAlertUtils::setValue(const CString& s,const CString& val, BOOL common)
{
	CDatabaseManager *databaseManager = CDatabaseManager::shared(true,common);
	if (databaseManager)
	{
		databaseManager->setProperty(s,val);
	}
}

void CAlertUtils::GetImageFromList(WTL::CImageList *lstImages,int nImage, WTL::CBitmap* destBitmap,HWND hwnd)
{
	SIZE size;
	lstImages->GetIconSize(size);

	destBitmap->CreateCompatibleBitmap(::GetWindowDC(hwnd),size.cx,size.cy);

	WTL::CDC dcMem;
	dcMem.CreateCompatibleDC (::GetWindowDC(hwnd));
	dcMem.SelectBitmap(destBitmap->m_hBitmap);

	LOGBRUSH bb;
	bb.lbColor = GetSysColor(COLOR_MENU);
	bb.lbStyle = BS_SOLID;
	HBRUSH brush = CreateBrushIndirect(&bb);
	HBRUSH oldbrush =  dcMem.SelectBrush(brush);

	CRect r(0,0,size.cx,size.cy);
	dcMem.FillRect(&r, brush);

	dcMem.SelectBrush(oldbrush);
	DeleteObject(brush);

	lstImages->Draw(dcMem,nImage,0,0,ILD_TRANSPARENT);
}

void appendToErrorFile(const TCHAR *szFormat, ...)
{
	release_try
	{
		//g_bDebugMode = TRUE;
		if(g_bDebugMode)
		{
			struct tm when;
			__time64_t now;
			_time64(&now);
			_localtime64_s(&when, &now);

			TCHAR time_buff[26];
			_tasctime_s(time_buff, sizeof(time_buff)/sizeof(TCHAR), &when);
			const size_t time_len = _tcslen(time_buff);
			if(time_buff[time_len-1] == _T('\n'))
				time_buff[time_len-1] = _T('\0');

			TCHAR buf[4096];
			va_list va;
			va_start(va, szFormat);
			_vstprintf_s(buf, sizeof(buf)/sizeof(TCHAR), szFormat, va);
			AtlTrace(_T("TRACE: %s %s\n"), time_buff, buf);
			va_end(va);

			using namespace std;
			basic_ofstream<TCHAR> logFile;

			CString rootPath;
			rootPath.GetEnvironmentVariable(_T("APPDATA"));
			rootPath += _T("\\deskalerts.log");

			logFile.open(rootPath, ios::out|ios::app);

			logFile<<time_buff;
			logFile<<_T("\t");
			logFile<<buf;
			logFile<<endl;

			logFile.close();
		}
#ifdef DEBUG
		else
		{
			TCHAR buf[1000];
			va_list va;
			va_start(va, szFormat);
			_vstprintf_s(buf, sizeof(buf)/sizeof(TCHAR), szFormat, va);
			AtlTrace(_T("TRACE: %s\n"), buf);
			va_end(va);
		}
#endif
	}
	release_end_try
}

CString CAlertUtils::CreateGUID()
{
	GUID guidValue = GUID_NULL;
	::CoCreateGuid(& guidValue);
	if ( guidValue == GUID_NULL) return _T("");

	CString strName;
	strName.Format(_T("{%08lX-%04X-%04x-%02X%02X-%02X%02X%02X%02X%02X%02X}"),
		// first copy...
		guidValue.Data1, (guidValue.Data2), (guidValue.Data3),
		(guidValue.Data4[0]), (guidValue.Data4[1]), (guidValue.Data4[2]),( guidValue.Data4[3]),
		(guidValue.Data4[4]), (guidValue.Data4[5]), (guidValue.Data4[6]), (guidValue.Data4[7]),

		(guidValue.Data1), (guidValue.Data2), (guidValue.Data3),
		(guidValue.Data4[0]), (guidValue.Data4[1]), (guidValue.Data4[2]), (guidValue.Data4[3]),
		(guidValue.Data4[4]), (guidValue.Data4[5]), (guidValue.Data4[6]), (guidValue.Data4[7]));

	return strName;
}

//������� �����, ��� � ���, ��� � ��� �������
//� xml ��� ��������� ���� ����� ���������������
//�� ������� ��������� �������� ������
//��� ��� � ����� ���, ����� �� ������
//��������, ���� ����� �� �������� ���� ��������
void CAlertUtils::checkPixel(CString pixelUrl, CString sPath)
{
	//TCHAR    achKey[MAX_KEY_LENGTH];   // buffer for subkey name
	//DWORD    cbName;                   // size of name string
	TCHAR    achClass[MAX_PATH] = TEXT("");  // buffer for class name
	DWORD    cchClassName = MAX_PATH;  // size of class string
	DWORD    cSubKeys=0;               // number of subkeys
	DWORD    cbMaxSubKey;              // longest subkey size
	DWORD    cchMaxClass;              // longest class string
	DWORD    cValues;              // number of values for key
	DWORD    cchMaxValue;          // longest value name
	DWORD    cbMaxValueData;       // longest value data
	DWORD    cbSecurityDescriptor; // size of security descriptor
	FILETIME ftLastWriteTime;      // last write time

	DWORD i, retCode;

	TCHAR  achValue[MAX_VALUE_NAME];
	DWORD cchValue = MAX_VALUE_NAME;

	TCHAR cValueStr[MAX_VALUE_NAME];
	DWORD pdwCountStr = MAX_VALUE_NAME;

	CRegKey hKey, key;
	hKey.Create(HKEY_CURRENT_USER,CGMUtils::GetCompanyKey());

	key.Open(hKey, _T("url"));

	retCode = RegQueryInfoKey(
		key.m_hKey,         // key handle
		achClass,                // buffer for class name
		&cchClassName,           // size of class string
		NULL,                    // reserved
		&cSubKeys,               // number of subkeys
		&cbMaxSubKey,            // longest subkey size
		&cchMaxClass,            // longest class string
		&cValues,                // number of values for this key
		&cchMaxValue,            // longest value name
		&cbMaxValueData,         // longest value data
		&cbSecurityDescriptor,   // security descriptor
		&ftLastWriteTime);       // last write time

	// Enumerate the subkeys, until RegEnumKeyEx fails.
	if (cValues)
	{
		for (i=0, retCode=ERROR_SUCCESS; i<cValues; i++)
		{
			cchValue = MAX_VALUE_NAME;
			achValue[0] = '\0';

			retCode = RegEnumValue(key.m_hKey, i,
				achValue,
				&cchValue,
				NULL,
				NULL,
				NULL,
				NULL);

			if(retCode == ERROR_SUCCESS)
			{
				pdwCountStr = MAX_VALUE_NAME;
				cValueStr[0] = '\0';
				if(ERROR_SUCCESS==key.QueryStringValue(achValue, cValueStr, &pdwCountStr))
				{
					pixelUrl.Replace(_T("%id%"), CString(cValueStr));
					appendToErrorFile(L"piXel request: %s to %s", pixelUrl, sPath);
					/*const HRESULT hres = */CAlertUtils::URLDownloadToFile(pixelUrl, sPath);
					key.DeleteValue(achValue);
				}
			}
		}
	}
}

CString CAlertUtils::GetSystemHash()
{
	TCHAR VolumeName[MAX_PATH],FileSystemName[MAX_PATH], tmp[MAX_PATH];
	unsigned long VolumeSerialNo, MaxComponentLength, FileSystemFlags;
	GetVolumeInformation(
		_T("C:\\"),
		VolumeName,
		MAX_PATH,
		&VolumeSerialNo,
		&MaxComponentLength,
		&FileSystemFlags,
		FileSystemName,
		MAX_PATH
		);
	_itot_s(VolumeSerialNo, tmp, 16);
	return tmp;
}

BOOL CAlertUtils::appendToFile(CString sFilename, CString sData, BOOL isUTF8)
{
	const HANDLE hFile = CreateFile(
		sFilename,                 // file to open
		FILE_APPEND_DATA,          // open for writing
		FILE_SHARE_READ,           // share for writing
		NULL,                      // default security
		OPEN_EXISTING,             // existing file only
		FILE_ATTRIBUTE_NORMAL,     // normal file
		NULL);

	if (hFile == INVALID_HANDLE_VALUE)
	{
		return NULL;
	}

	DWORD BytesWrite;
	CStringA sTmp = isUTF8 ? CT2A(sData, CP_UTF8) : CT2A(sData);
	const BOOL bSuc = WriteFile(hFile, sTmp, sTmp.GetLength(), &BytesWrite, NULL);
	CloseHandle(hFile);

	return bSuc;
}


//----------------------------------------------------------------------------------------------------------------------------------
BOOL CAlertUtils::mergeFiles(CString sFilenamTo, CString sFileNameFrom)
{
	std::wofstream fto;
	fto.open(sFilenamTo, ios_base::app | ios_base::binary);
	if(!fto.is_open()) return FALSE;

	std::wifstream ffrom;
	ffrom.open(sFileNameFrom);
	if(!ffrom.is_open()) return FALSE;

	// get length of file:
	ffrom.seekg (0, ios::end);
	int length = ffrom.tellg();
	ffrom.seekg (0, ios::beg);

	// allocate memory:
	wchar_t *buffer = new wchar_t[length];
	// read data as a block:
	ffrom.read (buffer, length);
	fto << FILE_SEPARATOR;
	fto.write(buffer, length);

	ffrom.close();
	fto.close();

	delete_catch([] buffer);

	return TRUE;
}

CString CAlertUtils::GetVersionTxtVersion()
{
	CString myv;
	CString path=_iAlertModel->getPropertyString(CString(_T("user_path")))+ _T("\\version.txt");
	if (!::PathFileExists(path))
		path=_iAlertModel->getPropertyString(CString(_T("root_path")))+ _T("\\version.txt");
	FILE* fp = NULL;
	_tfopen_s(&fp, path, _T("r"));
	if (fp)
	{
		TCHAR * newVersionInfo = new TCHAR[1024];
		newVersionInfo = _fgetts(newVersionInfo, 1024, fp);
		fclose(fp);
		myv = newVersionInfo;
		myv.Replace(_T("\n"), _T(""));
		myv.Replace(_T("\r"), _T(""));
		myv.Replace(_T("DeskAlerts v"),_T(""));
		delete_catch([] newVersionInfo);
	}

	return myv;
}

pair<CString, CString> CAlertUtils::getBodySize(CString FileName)
{
	pair<CString, CString> res;
	const HANDLE hFile = CreateFile(
		FileName,                  // file to open
		GENERIC_READ,              // open for reading
		FILE_SHARE_READ,           // share for reading
		NULL,                      // default security
		OPEN_EXISTING,             // existing file only
		FILE_ATTRIBUTE_NORMAL,     // normal file
		NULL);

	if (hFile == INVALID_HANDLE_VALUE)
	{
		return res;
	}

	DWORD FileSize = GetFileSize(hFile, NULL);
	CHAR* lpBuf = new CHAR [FileSize+1];
	ZeroMemory(lpBuf,FileSize+1);
	DWORD BytesRead;
	const BOOL bSuc = ReadFile(hFile, lpBuf, FileSize, &BytesRead, NULL);
	CloseHandle(hFile);

	if(bSuc)
	{
		CAtlRegExp<> reBody;
		// Five match groups: scheme, authority, path, query, fragment
		REParseError status = reBody.Parse(_T("<\b*body.*?width=\"|'{([0-9])+}\"|'.*?>"), FALSE);

		CAtlREMatchContext<> mcParam;
		if (reBody.Match(CA2T(lpBuf), &mcParam)){
			const CAtlREMatchContext<>::RECHAR* szStart = 0;
			const CAtlREMatchContext<>::RECHAR* szEnd = 0;
			mcParam.GetMatch(0, &szStart, &szEnd);
			res.first = CString(szStart).Left(szEnd - szStart);
		}
		status = reBody.Parse(_T("<\b*body.*?height=\"|'{([0-9])+}\"|'.*?>"), FALSE);
		if (reBody.Match(CA2T(lpBuf), &mcParam)){
			const CAtlREMatchContext<>::RECHAR* szStart = 0;
			const CAtlREMatchContext<>::RECHAR* szEnd = 0;
			mcParam.GetMatch(0, &szStart, &szEnd);
			res.second = CString(szStart).Left(szEnd - szStart);
		}
	}
	return res;
}
/*
BOOL checkUserHash(CStringW name, CStringW hash)
{
	BOOL result = FALSE;
	release_try
	{
		if(hash.GetLength() == 48)
		{
			SHA1 sha1;
			CStringW rnd = hash.Mid(27,2) + hash.Mid(8,3) + hash.Mid(37,3);
			CT2A calc(rnd.Mid(3,3) + name + rnd.Mid(6,2) + _T("dR5Hg") + rnd.Left(3), CP_UTF8);
			sha1.Input(calc, strlen(calc));
			unsigned res[5];
			if(sha1.Result((unsigned *)&res))
			{
				if(_tcstoul(hash.Left(8),NULL,16) == res[2] &&
					_tcstoul(hash.Mid(11,8),NULL,16) == res[1] &&
					_tcstoul(hash.Mid(19,8),NULL,16) == res[4] &&
					_tcstoul(hash.Mid(29,8),NULL,16) == res[3] &&
					_tcstoul(hash.Mid(40,8),NULL,16) == res[0])
				{
					result = TRUE;
				}
			}
		}
	}
	release_end_try
	return result;
}
*/
CString CAlertUtils::userHash(CString name)
{
	SHA1 sha1;
	CString result, rnd;
	srand((unsigned int)time(NULL));
	rnd.Format(_T("%08x"), rand());
	CT2A calc(rnd.Mid(3,3) + name + rnd.Mid(6,2) + _T("dR5Hg") + rnd.Left(3), CP_UTF8);
	sha1.Input(calc, strlen(calc));
	unsigned res[5];
	if(sha1.Result((unsigned *)&res))
	{
		result.Format(_T("%08x%ws%08x%08x%ws%08x%ws%08x"), res[2], rnd.Mid(2,3), res[1], res[4], rnd.Left(2), res[3], rnd.Mid(5,3), res[0]);
	}
	return result;
}

CString CAlertUtils::allUsersAppDataPath(BOOL env)
{
	CString allUsersPath;
	if(env)
	{
		allUsersPath.GetEnvironmentVariable(_T("ALLUSERSPROFILE"));
	}
	if(!env || allUsersPath.IsEmpty())
	{
		TCHAR commonAppDataPath[MAX_PATH];
		SHGetFolderPath(NULL,CSIDL_COMMON_APPDATA,NULL,SHGFP_TYPE_CURRENT,commonAppDataPath);
		allUsersPath = CString(commonAppDataPath);
	}
	if(allUsersPath.GetAt(allUsersPath.GetLength()-1) != _T('\\'))
	{
		allUsersPath += _T('\\');
	}
	CString deskalertsPath = allUsersPath + _T("DeskAlerts");

	if (!env) CreateDirectory(deskalertsPath,NULL);
	return deskalertsPath;
}

CString CAlertUtils::allUsersAppDataProfilePath(BOOL env)
{
	CString allUsersPath = CAlertUtils::allUsersAppDataPath(env);
	CString path = allUsersPath + "\\" + CAlertUtils::GetRegistryKeyName();
	if (!env) CreateDirectory(path,NULL);
	return path;
}


CString CAlertUtils::databasePath(BOOL common)
{
	CString deskalertsPath = CAlertUtils::allUsersAppDataProfilePath();
	
	CString curUserPath;
	curUserPath.GetEnvironmentVariable(_T("USERNAME"));
	curUserPath = deskalertsPath + "\\" + curUserPath;

	CreateDirectory(curUserPath,NULL);
	CString dbPath = (common) ? deskalertsPath+"\\db.dat" : curUserPath+"\\db.dat";
	return dbPath;
}

HANDLE CAlertUtils::GetProcessOwnerToken(DWORD pid)
{
	if (!pid) return NULL;

	HANDLE hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pid);
	if (!hProcess) return NULL;

	HANDLE hToken = NULL;
	if(OpenProcessToken(hProcess, MAXIMUM_ALLOWED, &hToken))
	{
		HANDLE result = INVALID_HANDLE_VALUE;
		if(DuplicateTokenEx(hToken, TOKEN_ASSIGN_PRIMARY | TOKEN_ALL_ACCESS, NULL, SecurityImpersonation, TokenPrimary, &result))
		{
			if(result != INVALID_HANDLE_VALUE)
			{
				CloseHandle(hToken);
				CloseHandle(hProcess);
				return result;
			}
		}
		CloseHandle(hToken);
	}
	CloseHandle(hProcess);

	return NULL;
}

CString CAlertUtils::escapeXML(CString str)
{
	str.Replace(_T("&"), _T("&amp;"));
	str.Replace(_T("\""), _T("&quot;"));
	str.Replace(_T("'"), _T("&apos;"));
	str.Replace(_T("<"), _T("&lt;"));
	str.Replace(_T(">"), _T("&gt;"));
	str.Replace(_T("\t"), _T("&#x9;"));
	str.Replace(_T("\n"), _T("&#xA;"));
	str.Replace(_T("\r"), _T("&#xD;"));
	str.Remove(_T('\0'));
	/*int pos = -1;
	while((pos = str.Find(_T('\0'), pos+1)) > -1 && pos != str.GetLength())
	{
		str = str.Left(pos) + _T("&#x0;")+ str.Mid(pos+1);
	}*/
	return str;
}

CString CAlertUtils::escapeJSON(CString str)
{
	str.Replace(_T("\\"), _T("\\\\"));
	str.Replace(_T("\""), _T("\\\""));
	str.Replace(_T("'"), _T("\\'"));
	str.Replace(_T("\n"), _T("\\n"));
	str.Replace(_T("\r"), _T("\\r"));
	/*int pos = -1;
	while((pos = str.Find(_T('\0'), pos+1)) > -1 && pos != str.GetLength())
	{
		str = str.Left(pos) + _T("\\0")+ str.Mid(pos+1);
	}*/
	str.Remove(_T('\0'));
	return str;
}

void CAlertUtils::RecursiveCopyKey(HKEY old_parent, HKEY new_parent, CString old_name, CString new_name)
{
	CRegKey old_key, new_key;
	if(old_key.Open(old_parent, old_name, KEY_READ) == ERROR_SUCCESS &&
		new_key.Create(new_parent, new_name, REG_NONE, REG_OPTION_NON_VOLATILE, KEY_WRITE) == ERROR_SUCCESS)
	{
		CString name;
		DWORD index = 0, length, type, data_length;
		while(old_key.EnumKey(index, name.GetBuffer(length = MAX_KEY_LENGTH), &length) == ERROR_SUCCESS)
		{
			name.ReleaseBuffer(length);
			RecursiveCopyKey(old_key, new_key, name, name);
			++index;
		}
		index = 0;
		while(::RegEnumValue(old_key, index, name.GetBuffer(length = MAX_VALUE_NAME), &length, NULL, &type, NULL, &data_length) == ERROR_SUCCESS)
		{
			name.ReleaseBuffer(length);
			LPBYTE data = new BYTE[data_length];
			if(::RegQueryValueEx(old_key, name, NULL, &type, data, &data_length) == ERROR_SUCCESS)
			{
				::RegSetValueEx(new_key, name, NULL, type, data, data_length);
			}
			++index;
		}
	}
}

void CAlertUtils::RecursiveCopyKey(HKEY parent, CString old_name, CString new_name)
{
	RecursiveCopyKey(parent, parent, old_name, new_name);
}

void CAlertUtils::RecursiveDeleteKey(HKEY parent, CString name)
{
	CRegKey key;
	if(key.Open(parent, name, KEY_READ|KEY_WRITE) == ERROR_SUCCESS)
	{
		CString sub_name;
		DWORD index = 0, length;
		while(key.EnumKey(index, sub_name.GetBuffer(length = MAX_KEY_LENGTH), &length) == ERROR_SUCCESS)
		{
			sub_name.ReleaseBuffer(length);
			RecursiveDeleteKey(key, sub_name);
			++index;
		}
	}
	key.Close();
	::RegDeleteKey(parent, name);
}

void CAlertUtils::RecursiveRenameKey(HKEY parent, CString old_name, CString new_name)
{
	RecursiveCopyKey(parent, old_name, new_name);
	RecursiveDeleteKey(parent, old_name);
}

CRegKey CAlertUtils::parseRegPath(CString path, CString wow6432, REGSAM samDesired, BOOL create /*= FALSE*/)
{
	if(wow6432 == _T("1") || !wow6432.CompareNoCase(_T("true")))
		samDesired |= KEY_WOW64_32KEY;
	else if(wow6432 == _T("0") || !wow6432.CompareNoCase(_T("false")))
		samDesired |= KEY_WOW64_64KEY;

	CRegKey result;
	path.Replace('/', '\\');
	int pos = path.Find('\\');
	if(pos > -1)
	{
		HKEY key = NULL;
		CString root = path.Left(pos);
		root.MakeUpper();
		if(root == _T("HKLM") || root == _T("HKEY_LOCAL_MACHINE"))
			key = HKEY_LOCAL_MACHINE;
		else if(root == _T("HKCU") || root == _T("HKEY_CURRENT_USER"))
			key = HKEY_CURRENT_USER;
		else if(root == _T("HKCC") || root == _T("HKEY_CURRENT_CONFIG"))
			key = HKEY_CURRENT_CONFIG;
		else if(root == _T("HKCR") || root == _T("HKEY_CLASSES_ROOT"))
			key = HKEY_CLASSES_ROOT;
		else if(root == _T("HKU") || root == _T("HKEY_USERS"))
			key = HKEY_USERS;
		else if(root == _T("HKEY_PERFORMANCE_DATA"))
			key = HKEY_PERFORMANCE_DATA;
		else if(root == _T("HKEY_DYN_DATA"))
			key = HKEY_DYN_DATA;
		if(key)
		{
			path = path.Mid(pos+1);
			if(result.Open(key, path, samDesired) != ERROR_SUCCESS)
			{
				if(!(samDesired & KEY_WOW64_32KEY) && !(samDesired & KEY_WOW64_64KEY) &&
					result.Open(key, path, samDesired|KEY_WOW64_64KEY) == ERROR_SUCCESS)
				{
					return result;
				}
				else if(create)
				{
					result.Create(key, path, REG_NONE, REG_OPTION_NON_VOLATILE, samDesired);
				}
			}
		}
	}
	return result;
}

CString CAlertUtils::QueryRegKey(CRegKey &key, const CString keyname, const CString format)
{
	TCHAR *delim = format != _T("xml") ? _T(",") : NULL;
	CString name, subkeys, values;
	DWORD index = 0, length;
	while(key.EnumKey(index, name.GetBuffer(length = MAX_KEY_LENGTH), &length) == ERROR_SUCCESS)
	{
		name.ReleaseBuffer(length);
		if(index == 0 && delim)
			subkeys += delim;
		CRegKey subkey;
		if(subkey.Open(key, name, KEY_READ) == ERROR_SUCCESS)
			subkeys += QueryRegKey(subkey, name, format);
		++index;
	}
	index = 0;
	while(::RegEnumValue(key.m_hKey, index, name.GetBuffer(length = MAX_VALUE_NAME), &length, NULL, NULL, NULL, NULL) == ERROR_SUCCESS)
	{
		name.ReleaseBuffer(length);
		if(index == 0 && delim)
			values += delim;
		values += QueryRegValue(key, name, format);
		++index;
	}
	if(format==_T("xml"))
		values = _T("<KEY name=\"")+escapeXML(keyname)+_T("\"")+(!values.IsEmpty()||!subkeys.IsEmpty()?_T(">")+subkeys+values+_T("</KEY>"):_T("/>"));
	else if(format==_T("json"))
		values = _T("{key:\"")+escapeJSON(keyname)+_T("\"")+(!subkeys.IsEmpty()?_T(",subkeys:[")+subkeys+_T("]"):_T(""))+(!values.IsEmpty()?_T(",subkeys:[")+values+_T("]"):_T(""))+_T("}");
	if(delim)
		delete_catch(delim);
	return values;
}

CString CAlertUtils::QueryRegValue(CRegKey &key, const CString name, const CString format)
{
	CString value;
	DWORD type, valueLength = 0, dwValue = 0; ULONGLONG qwValue;
	if(::RegQueryValueEx(key.m_hKey, name, NULL, &type, NULL, &valueLength) == ERROR_SUCCESS)
	{
		if(((type == REG_SZ || type == REG_EXPAND_SZ) && key.QueryStringValue(name, value.GetBuffer(valueLength), &valueLength) == ERROR_SUCCESS) ||
			(type == REG_MULTI_SZ && key.QueryMultiStringValue(name, value.GetBuffer(valueLength), &valueLength) == ERROR_SUCCESS))
		{
			value.ReleaseBuffer(valueLength-1); //already have \0
		}
		else if(type == REG_DWORD && key.QueryDWORDValue(name, dwValue) == ERROR_SUCCESS)
		{
			_ltot_s(dwValue, value.GetBuffer(valueLength = 33), valueLength, 10);
			value.ReleaseBuffer();
		}
		else if(type == REG_QWORD && key.QueryQWORDValue(name, qwValue) == ERROR_SUCCESS)
		{
			_i64tot_s(dwValue, value.GetBuffer(valueLength = 65), valueLength, 10);
			value.ReleaseBuffer();
		}
	}
	if(format==_T("xml"))
		value = _T("<VALUE name=\"") + escapeXML(name) + _T("\" value=\"") + escapeXML(value) + _T("\"/>");
	else if(format==_T("json"))
		value = _T("{name:\"") + escapeJSON(name) + _T("\",value:\"") + escapeJSON(value) + _T("\"}");
	return value;
}

CString CAlertUtils::GetRegistryKeyName()
{
	CString registryKeyName;
	registryKeyName = (LPCTSTR) sRegistryKey;
	return registryKeyName;
}

CString CAlertUtils::GetProfileName()
{
	CString profileName;
	profileName = (LPCTSTR) sProfileName;
	return profileName;
}


CString CAlertUtils::getDir(CString& param)
{
	CString res = param;
	int st = param.ReverseFind('\\');
	if (st)
	{
		res = param.Mid(0,st);
	}
	return res;
}

CString CAlertUtils::getAppDataPath(CString deskalertsId)
{
	//if (CAlertController::m_sAppDataPath == CString(_T("")))
	{
		TCHAR szPath[MAX_PATH];
		CString zPath = _T("");
		ZeroMemory(szPath, MAX_PATH*sizeof(TCHAR));
		//iserg:
		if(SHGetSpecialFolderPath(NULL, szPath, CSIDL_FLAG_CREATE|CSIDL_APPDATA, TRUE))
		{
			zPath = szPath;
			if(zPath.Mid(0, 2) == _T("\\\\")) //hotfix for UNC paths
			{
				if(SHGetSpecialFolderPath(NULL, szPath, CSIDL_FLAG_CREATE|CSIDL_LOCAL_APPDATA, TRUE))
					zPath = szPath;
			}
			zPath+= CString(_T("\\DeskAlerts_"))+deskalertsId;
			//CreateDirectory(zPath, NULL);
		}
		return zPath;
	}
	// else
	//{
	//		return CAlertController::m_sAppDataPath;
	//}
}

CString CAlertUtils::regExReplace(CAtlRegExp<>* regEx, LPCTSTR szIn, LPCTSTR szVal, BOOL bAll)
{

	CString s;
	LPCTSTR szRest = szIn;
	CAtlREMatchContext<> mc;
	while (szIn!=NULL && *szIn!=0 && regEx->Match(szIn, &mc, &szRest)) {
		int offset=mc.m_Match.szStart - szIn;
		CString temp = szIn;
		s += temp.Left(offset);
		s += szVal;
		szIn = szRest;
		if (!bAll)
			break;
	}
	s += szIn;
	return s;
}

CString CAlertUtils::stripHtmlTags(CString html)
{
	CString res;
	CAtlRegExp<> regExp;
	
	regExp.Parse(_T("<head[^>]*?>.*?</head>"), FALSE );
	res = CAlertUtils::regExReplace(&regExp,html,_T(" "),true);
	regExp.Parse(_T("<style[^>]*?>.*?</style>"), FALSE );
	res = CAlertUtils::regExReplace(&regExp,res,_T(" "),true);
 	regExp.Parse(_T("<script[^>]*?>.*?</script>"), FALSE );
 	res = CAlertUtils::regExReplace(&regExp,html,_T(" "),true);
	regExp.Parse(_T("<object[^>]*?>.*?</object>"), FALSE );
	res = CAlertUtils::regExReplace(&regExp,res,_T(" "),true);
	regExp.Parse(_T("<applet[^>]*?>.*?</applet>"), FALSE );
	res = CAlertUtils::regExReplace(&regExp,res,_T(" "),true);
	regExp.Parse(_T("<noframes[^>]*?>.*?</noframes>"), FALSE );
	res = CAlertUtils::regExReplace(&regExp,res,_T(" "),true);
	regExp.Parse(_T("<noscript[^>]*?>.*?</noscript>"), FALSE );
	res = CAlertUtils::regExReplace(&regExp,res,_T(" "),true);
	regExp.Parse(_T("<noembed[^>]*?>.*?</noembed>"), FALSE );
	res = CAlertUtils::regExReplace(&regExp,res,_T(" "),true);
	regExp.Parse(_T("(<([^>]+)>)"), FALSE );
	res = CAlertUtils::regExReplace(&regExp,res,_T(" "),true);
		
	return res;
}

CString CAlertUtils::findAlertWindowName()
{
	CString result;
	NodeALERTS& nodeALERTS = _iAlertModel->getModel(CString(_T("views")));
	vector<NodeALERTS::NodeVIEWS::NodeVIEW*> vView =  nodeALERTS.m_views->m_view;
	vector<NodeALERTS::NodeVIEWS::NodeVIEW*>::iterator it = vView.begin();
	while(it != vView.end())
	{
		if((*it)->m_tagName == _T("WINDOW"))
		{
			CString name = (*it)->m_name;
			name.MakeLower();
			if(name.Find(_T("alert")) > -1 && name.Find(_T("ticker")) == -1)
			{
				result = (*it)->m_name;
				break;
			}
		}
		it++;
	}
	if(result.IsEmpty())
	{
		it = vView.begin();
		while(it != vView.end())
		{
			if((*it)->m_tagName == _T("WINDOW"))
			{
				NodeALERTS::NodeVIEWS::NodeWINDOW* win = (NodeALERTS::NodeVIEWS::NodeWINDOW*)*it;
				CString bodyclassname = win->m_bodyclassname;
				CString caption = win->m_caption;
				CString captionhref = win->m_captionhref;
				CString customjs = win->m_customjs;
				CString customcss = win->m_customcss;
				bodyclassname.MakeLower();
				caption.MakeLower();
				captionhref.MakeLower();
				customjs.MakeLower();
				customcss.MakeLower();
				if(bodyclassname.Find(_T("alert")) > -1 ||
					caption.Find(_T("alert")) > -1 ||
					captionhref.Find(_T("alert")) > -1 ||
					customjs.Find(_T("alert")) > -1 ||
					customcss.Find(_T("alert")) > -1)
				{
					result = win->m_name;
					break;
				}
			}
			it++;
		}
	}
	return result;
}

CRegKey CAlertUtils::getDesktopRegKey()
{
	CRegKey key;
	key.Create(HKEY_CURRENT_USER,_T("Control Panel\\Desktop"));
	return key;
}

CRegKey CAlertUtils::getColorsRegKey()
{
	CRegKey key;
	key.Create(HKEY_CURRENT_USER,_T("Control Panel\\Colors"));
	return key;
}

CString CAlertUtils::getScreensaverPath(BOOL isShort)
{
	isShort = false;
	TCHAR localFolder[MAX_PATH];
	GetCurrentDirectory(  MAX_PATH, localFolder);
	CString screensaverPath = CString(localFolder)+_T("\\DeskAlertsScreenSaver.scr");
	if (isShort)
	{
		long     length = 0;
		TCHAR*   buffer = NULL;
		length = GetShortPathName(screensaverPath, NULL, 0);
		if (length != 0)
		{
			buffer = new TCHAR[length];

			length = GetShortPathName(screensaverPath, buffer, length);
			if (length != 0) 
			{
				screensaverPath = buffer;
				delete_catch([] buffer);
			}
		}		
	}


	return screensaverPath;
}

CString CAlertUtils::getWallpapersPath()
{
	CString deskalertsPath = CAlertUtils::allUsersAppDataProfilePath();
	
	CString curUserPath;
	curUserPath.GetEnvironmentVariable(_T("USERNAME"));
	curUserPath = deskalertsPath + "\\" + curUserPath;

	CreateDirectory(curUserPath,NULL);
	CString wallpaperPath = curUserPath+ "\\Wallpapers";
	CreateDirectory(wallpaperPath,NULL);
	return wallpaperPath;
}

CString CAlertUtils::getDesktopName(HDESK hDesktop)
{
	CString desktopName;
	if (hDesktop)
	{
		DWORD length;
		GetUserObjectInformation(hDesktop, UOI_NAME, NULL, NULL, &length);
		TCHAR *deskBuffer = new TCHAR[length];
		if(GetUserObjectInformation(hDesktop, UOI_NAME, deskBuffer, length, NULL))
		{
			desktopName = deskBuffer;
		}
		delete_catch(deskBuffer);
	}
	return desktopName;
}

static Gdiplus::Image *ScaleByPercent(Gdiplus::Image *imgPhoto, int Percent)
{
	float nPercent = ((float)Percent/100);

	int sourceWidth = imgPhoto->GetWidth();
	int sourceHeight = imgPhoto->GetHeight();
	int destX = 0;
	int destY = 0; 
	int destWidth  = (int)(sourceWidth * nPercent);
	int destHeight = (int)(sourceHeight * nPercent);

	Gdiplus::Bitmap *bmPhoto = new Gdiplus::Bitmap(destWidth, destHeight, PixelFormat24bppRGB);

	bmPhoto->SetResolution(imgPhoto->GetHorizontalResolution(), imgPhoto->GetVerticalResolution());

	Gdiplus::Graphics grPhoto(bmPhoto);
	grPhoto.SetInterpolationMode(Gdiplus::InterpolationModeHighQualityBicubic);
	const Gdiplus::RectF rectDest((Gdiplus::REAL)destX, (Gdiplus::REAL)destY, (Gdiplus::REAL)destWidth, (Gdiplus::REAL)destHeight);
	grPhoto.Clear((DWORD)Gdiplus::Color::White);
	grPhoto.DrawImage(imgPhoto,rectDest);

	return bmPhoto;
}

Gdiplus::Status CAlertUtils::imageToImage(IStream *pStreamIn, IStream *pStreamOut, CString wszOutputMimeType, int quality)
{
	namespace G = Gdiplus;

	G::GdiplusStartupInput gdiplusStartupInput;
	ULONG_PTR gdiplusToken;
	G::Status status = G::Ok;
	status = G::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
	if (G::Ok == status)
	{
		G::Image imageSrc(pStreamIn);
		status = imageSrc.GetLastStatus();
		if (G::Ok == status) 
		{
			UINT numEncoders = 0;
			UINT sizeEncodersInBytes = 0;
			status = G::GetImageEncodersSize(&numEncoders, &sizeEncodersInBytes);
			if (status == G::Ok) 
			{
				G::ImageCodecInfo *pImageCodecInfo = (G::ImageCodecInfo *) malloc(sizeEncodersInBytes);
				status = G::GetImageEncoders(numEncoders, sizeEncodersInBytes, pImageCodecInfo);
				if (status == G::Ok)
				{
					CLSID clsidOut;
					status = G::UnknownImageFormat;
					for (UINT j = 0; j < numEncoders; j ++) {
						if (wszOutputMimeType == pImageCodecInfo[j].MimeType ) 
						{
							clsidOut = pImageCodecInfo[j].Clsid;
							status = G::Ok;
							break;
						}
					}
			
					G::EncoderParameters encParams;
					encParams.Count = 1;
					encParams.Parameter[0].Guid = G::EncoderQuality;
					encParams.Parameter[0].Type = G::EncoderParameterValueTypeLong;
					encParams.Parameter[0].NumberOfValues = 1;
					encParams.Parameter[0].Value = (void*)&quality;
					Gdiplus::Image *newImage = ScaleByPercent(&imageSrc,100);
					status = newImage->Save(pStreamOut,&clsidOut,&encParams);
				//	status = newImage->Save(pStreamOut,NULL,NULL);
					delete_catch(newImage);
				}
				free(pImageCodecInfo);
			}
		}
	}
	G::GdiplusShutdown(gdiplusToken);
	
	return status;
}

BOOL CAlertUtils::RecursiveDeleteDirectory(const TCHAR *path)
{
	WIN32_FIND_DATA ffd;
	TCHAR tmp[MAX_PATH+1];
	_tcscpy_s(tmp, MAX_PATH+1, path);
	if(tmp[_tcslen(tmp)-1] != _T('\\'))
	{
		_tcscat_s(tmp, MAX_PATH+1, _T("\\"));
	}
	TCHAR findstr[MAX_PATH+1];
	_tcscpy_s(findstr, MAX_PATH+1, tmp);
	_tcscat_s(findstr, MAX_PATH+1, _T("*"));
	const HANDLE hFind = FindFirstFile(findstr, &ffd);
	if(INVALID_HANDLE_VALUE != hFind)
	{
		do
		{
			TCHAR filepath[MAX_PATH+1];
			_tcscpy_s(filepath, MAX_PATH+1, tmp);
			_tcscat_s(filepath, MAX_PATH+1, ffd.cFileName);
			if(ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			{
				if(_tcscmp(ffd.cFileName, _T(".")) && _tcscmp(ffd.cFileName, _T("..")))
					RecursiveDeleteDirectory(filepath);
			}
			else
			{
				if(ffd.dwFileAttributes & FILE_ATTRIBUTE_READONLY)
				{
					_tchmod(filepath, _S_IWRITE);
				}
				DeleteFile(filepath);
			}
		}
		while(FindNextFile(hFind,&ffd));
		FindClose(hFind);
	}
	tmp[_tcslen(tmp)-1] = _T('\0'); //replace \ char
	return RemoveDirectory(tmp);
}

BOOL CAlertUtils::RecursiveMoveDirectory(const TCHAR *src, const TCHAR *dest)
{
	WIN32_FIND_DATA ffd;
	TCHAR tmpsrc[MAX_PATH+1];
	_tcscpy_s(tmpsrc, MAX_PATH+1, src);
	if(tmpsrc[_tcslen(tmpsrc)-1] != _T('\\'))
	{
		_tcscat_s(tmpsrc, MAX_PATH+1, _T("\\"));
	}

	TCHAR tmpdest[MAX_PATH+1];
	_tcscpy_s(tmpdest, MAX_PATH+1, dest);
	if(tmpdest[_tcslen(tmpdest)-1] == _T('\\'))
	{
		tmpdest[_tcslen(tmpdest)-1] = _T('\0'); //replace \ char
	}
	BOOL res = PathFileExists(tmpdest) || !_tmkdir(tmpdest);
	if(res)
	{
		_tcscat_s(tmpdest, MAX_PATH+1, _T("\\"));

		TCHAR findstr[MAX_PATH+1];
		_tcscpy_s(findstr, MAX_PATH+1, tmpsrc);
		_tcscat_s(findstr, MAX_PATH+1, _T("*"));
		const HANDLE hFind = FindFirstFile(findstr, &ffd);
		if(INVALID_HANDLE_VALUE != hFind)
		{
			do
			{
				TCHAR filesrc[MAX_PATH+1];
				_tcscpy_s(filesrc, MAX_PATH+1, tmpsrc);
				_tcscat_s(filesrc, MAX_PATH+1, ffd.cFileName);

				TCHAR filedest[MAX_PATH+1];
				_tcscpy_s(filedest, MAX_PATH+1, tmpdest);
				_tcscat_s(filedest, MAX_PATH+1, ffd.cFileName);
				if(ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
				{
					if(_tcscmp(ffd.cFileName, _T(".")) && _tcscmp(ffd.cFileName, _T("..")))
						RecursiveMoveDirectory(filesrc, filedest);
				}
				else
				{
					if(!MoveFileEx(filesrc, filedest, MOVEFILE_REPLACE_EXISTING|MOVEFILE_COPY_ALLOWED))
						res = FALSE;
				}
			}
			while(FindNextFile(hFind,&ffd));
			FindClose(hFind);
		}
	}
	tmpsrc[_tcslen(tmpsrc)-1] = _T('\0'); //replace \ char
	if(!RecursiveDeleteDirectory(tmpsrc))
		res = FALSE;
	return res;
}

bool CAlertUtils::IsIE6()
{
	bool res = false;
	CRegKey k;
	if (k.Open(HKEY_LOCAL_MACHINE,_T("Software\\Microsoft\\Internet Explorer"),KEY_READ) == ERROR_SUCCESS)
	{
		CString szBuf;
		DWORD dwBufLen = 1024;
		if (k.QueryStringValue(_T("Version"),szBuf.GetBuffer(dwBufLen),&dwBufLen) == ERROR_SUCCESS)
		{
			res = szBuf.Find(_T("6.")) == 0;
		}
	}
	return res;
}


//
// Andrew Maximov 08/22/2017
// Extract href to media resource from url
//
CString CAlertUtils::ExtractHref(CString url) 
{
	CString result = "";
	if (url.IsEmpty())
		return result;
	int startIdx = url.Find(TEXT("http://"));
	if (startIdx >= 0)
	{
		int finishIdx = url.Find(TEXT("\""), startIdx);
		if (startIdx >= 0 && finishIdx > 0)
			result = url.Mid(startIdx, finishIdx - startIdx);
	}
	return result;
}

//
// Andrew Maximov 08/23/2017
// Extract file name from url
//
CString CAlertUtils::ExtractFileName(CString url)
{
	if (url.IsEmpty())
		return TEXT("");
	int fileNameStartIdx = url.ReverseFind(TEXT('/')) + 1;
	int size = url.GetLength() - fileNameStartIdx;
	if (size <= 0)
		return TEXT("");

	return url.Mid(fileNameStartIdx, size);
}

//
// Andrew Maximov 08/24/2017
// Replace screensaver file name in the href with file name in the cache
//
CString CAlertUtils::ReplaceHref(CString html, CString fileName)
{
	CString result = replaceByMask(html, TEXT("file=\""), TEXT("\""), fileName);
	result = replaceByMask(result, TEXT("<PARAM NAME='url' VALUE='"), TEXT("'"), fileName);
	return replaceByMask(result, TEXT("<EMBED src='"), TEXT("'"), fileName);
}

//
// replace text in src between startMask and finishMask to replaceTo
//
CString replaceByMask(CString src, CString startMask, CString finishMask, CString replaceTo)
{
	int fileNameStartIdx = src.Find(startMask) + startMask.GetLength();
	int fileNameFinishIdx = src.Find(finishMask, fileNameStartIdx);
	return src.Left(fileNameStartIdx) + replaceTo + src.Right(src.GetLength() - fileNameFinishIdx);
}
