#pragma once

#include "AlertController.h"

#include "XMLParser.h"
#include "alertutils.h"
#include "HistoryStorage.h"
#include "DesktopMultiWindow.h"

/*

<TOOLBAR>
<ALERT id="alert" href="http://basis.alertgear.com/demo_deskalerts/get_alert.asp?id=52&amp;user_id=19"
timeout="-1" expire=""  plugin="alert:ID3"/>
</TOOLBAR>
*/

class CAlertLoader;

class NodeALERT : public XmlNode
{
public:
	CString m_id;
	CString m_href;
	CString m_timeout;
	CString m_expire;
	CString m_plugin;
	CString m_iWidth;
	CString m_iHeight;
	CString m_userid;
	CString m_alertid;
	CString m_title;
	CString m_acknowledgement;
	CString m_createDate;
	CString m_position;
	CString m_autoclose;
	CString m_html;
	CString m_topTemplateHTML;
	CString m_bottomTemplateHTML;
	CString m_visible;
	CString m_ticker;
	CString m_ticker_position;
	CString m_history;
	CString m_survey;
	CString m_schedule;
	CString m_recurrence;
	CString m_urgent;
	CString m_inner;
	CString m_class;
	CString m_resizable;
	CString m_docked;
	CString m_self_deletable;
	CString m_to_date;
	CString m_hide_close;

	BOOL m_UTC, m_isUTF8;

	vector<NodeALERTS::NodeVIEWS::NodeWINDOW*> m_windows;

	NodeALERT()
	{
		m_tagName = _T("ALERT");
	}
	void Determine(XMLParser *pParser)
	{
		XmlNode::Determine(pParser);
		DetermineNodeVector<NodeALERTS::NodeVIEWS::NodeWINDOW>(pParser, _T("WINDOW"), &m_windows);
	}
	void DetermineAttrib(XMLParser *pParser)
	{
		XmlNode::DetermineAttrib(pParser);
		pParser->DetermineString(_T("id"),		&m_id);
		pParser->DetermineString(_T("href"),	&m_href);
		pParser->DetermineString(_T("timeout"),	&m_timeout);
		pParser->DetermineString(_T("expire"),	&m_expire);
		pParser->DetermineString(_T("plugin"),	&m_plugin);
		pParser->DetermineString(_T("width"),	&m_iWidth);
		pParser->DetermineString(_T("height"),	&m_iHeight);
		pParser->DetermineString(_T("acknown"), &m_acknowledgement);
		pParser->DetermineString(_T("user_id"), &m_userid);
		pParser->DetermineString(_T("alert_id"), &m_alertid);
		pParser->DetermineString(_T("create_date"), &m_createDate);
		pParser->DetermineString(_T("title"), &m_title);
		pParser->DetermineString(_T("position"), &m_position);
		pParser->DetermineString(_T("autoclose"), &m_autoclose);
		pParser->DetermineString(_T("visible"), &m_visible);
		pParser->DetermineString(_T("ticker"), &m_ticker);
		pParser->DetermineString(_T("ticker_position"), &m_ticker_position);
		pParser->DetermineString(_T("survey"), &m_survey);
		pParser->DetermineString(_T("schedule"), &m_schedule);
		pParser->DetermineString(_T("recurrence"), &m_recurrence);
		pParser->DetermineString(_T("urgent"), &m_urgent);
		pParser->DetermineString(_T("history"), &m_history);
		pParser->DetermineString(_T("class_id"), &m_class);
		pParser->DetermineString(_T("resizable"), &m_resizable);
		pParser->DetermineString(_T("docked"), &m_docked);
		pParser->DetermineString(_T("self_deletable"), &m_self_deletable);
		pParser->DetermineString(_T("to_date"), &m_to_date);
		pParser->DetermineString(_T("hide_close"), &m_hide_close);
		pParser->DetermineInnerXML(&m_inner);
		CString str;
		m_UTC = (pParser->DetermineString(_T("utc"), &str) == ERR_NONE && str == _T("1"));
	}
};

class NodeSYNCHRONIZEDALERTS : public XmlNode
{
public:
	CString m_hash;
	vector<NodeALERT*> *m_alerts;

	NodeSYNCHRONIZEDALERTS() : m_alerts(new vector<NodeALERT*>())
	{
	}

	void Determine(XMLParser *pParser)
	{
		XmlNode::Determine(pParser);
		DetermineNodeVector<NodeALERT>(pParser, _T("ALERT"), m_alerts);
	}
	void DetermineAttrib(XMLParser *pParser)
	{
		XmlNode::DetermineAttrib(pParser);
		pParser->DetermineString(_T("hash"),&m_hash);

	}
};

class NodeSCREENSAVERS : public NodeSYNCHRONIZEDALERTS
{
public:
	NodeSCREENSAVERS()
	{
		m_tagName = _T("SCREENSAVERS");
	}
};

class NodeWALLPAPERS : public NodeSYNCHRONIZEDALERTS
{
public:
	NodeWALLPAPERS()
	{
		m_tagName = _T("WALLPAPERS");
	}
};

class NodeTOOLBAR : public XmlNode
{
public:
	
	struct NodeALERTCOUNT : public XmlNode
	{
		CString m_sCount;
		NodeALERTCOUNT()
		{
			m_tagName = _T("ALERTCOUNT");
		}
		void Determine(XMLParser *pParser)
		{
			XmlNode::Determine(pParser);
		}
		void DetermineAttrib(XMLParser *pParser)
		{
			XmlNode::DetermineAttrib(pParser);
			pParser->DetermineString(_T("count"),&m_sCount);
		}
	};
	struct NodeVARIABLE : public XmlNode
	{
		CString m_sName;
		CString m_sValue;
		NodeVARIABLE()
		{
			m_tagName = _T("VARIABLE");
		}
		void Determine(XMLParser *pParser)
		{
			XmlNode::Determine(pParser);
		}
		void DetermineAttrib(XMLParser *pParser)
		{
			XmlNode::DetermineAttrib(pParser);
			if(pParser->DetermineString(_T("id"),&m_sName) == ERR_NOT_FOUND)
			{
				pParser->DetermineString(_T("name"),&m_sName);
			}
			if(pParser->DetermineString(_T("value"),&m_sValue) == ERR_NOT_FOUND)
			{
				pParser->DetermineString(_T("default"),&m_sValue);
			}
		}
	};

	vector<XmlNode*>  m_alerts;

	void Determine(XMLParser *pParser)
	{
		XmlNode::Determine(pParser);
		DetermineNodeVector<NodeALERT>(pParser,_T("ALERT"),(vector <NodeALERT*>*)&m_alerts);
		DetermineNodeVector<NodeALERTCOUNT>(pParser,_T("ALERTCOUNT"),(vector <NodeALERTCOUNT*>*)&m_alerts);
		DetermineNodeVector<NodeSCREENSAVERS>(pParser,_T("SCREENSAVERS"),(vector <NodeSCREENSAVERS*>*)&m_alerts);
		DetermineNodeVector<NodeWALLPAPERS>(pParser,_T("WALLPAPERS"),(vector <NodeWALLPAPERS*>*)&m_alerts);
	}
	void DetermineAttrib(XMLParser *pParser)
	{
		XmlNode::DetermineAttrib(pParser);
	}
};

class CAlertLoader: public CFrameWindowImpl<CAlertLoader>, public CCommand
{
	enum { UWM_NOTIFY_XML_UPDATE = (WM_APP + 310),
			UWM_STOP_THREAD = (WM_APP + 311),
			UWM_RELOAD_DATA = (WM_APP + 312),
			UWM_NOTIFY_XML_UNREADED = (WM_APP + 313),
			UWM_NOTIFY_OFFLINE = (WM_APP + 314),
			UWM_NOTIFY_PURGE_HISTORY = (WM_APP + 315),
			UWM_NOTIFY_SET_VARIABLE = (WM_APP + 316),
			UWM_NOTIFY_SCREENSAVERS = (WM_APP + 317),
			UWM_NOTIFY_WALLPAPERS = (WM_APP + 318),
			// the message below sends when connection lost and need to start reload timer
			UWM_NOTIFY_LOST_CONNECTION = (WM_APP + 319),
			// the message below sends when connection established and need to stop reload timer
			UWM_NOTIFY_ESTABLISHED_CONNECTION = (WM_APP + 320)
	};

public:
//	Notification::INotificationPtr notifier;
	DECLARE_WND_CLASS(_T("AlertLoader"))
	
	BEGIN_MSG_MAP(CAlertLoader)
		MESSAGE_HANDLER(WM_CREATE, OnCreate)
		MESSAGE_HANDLER(WM_DESTROY, OnDestroy)
		MESSAGE_HANDLER(UWM_NOTIFY_XML_UPDATE, OnNotifyXMLUpdate)
		MESSAGE_HANDLER(UWM_NOTIFY_XML_UNREADED, OnNotifyXMLUnreaded)
		MESSAGE_HANDLER(UWM_NOTIFY_PURGE_HISTORY, OnPurgeHistory)
		MESSAGE_HANDLER(UWM_NOTIFY_OFFLINE, OnErrorConnection)
		MESSAGE_HANDLER(UWM_NOTIFY_ESTABLISHED_CONNECTION, OnEstablishedConnection)
		MESSAGE_HANDLER(UWM_NOTIFY_SET_VARIABLE, OnSetVariable)
		MESSAGE_HANDLER(UWM_NOTIFY_SCREENSAVERS, OnScreensavers)
		MESSAGE_HANDLER(UWM_NOTIFY_WALLPAPERS, OnWallpapers)
		MESSAGE_HANDLER(UWM_NOTIFY_ESTABLISHED_CONNECTION, OnEstablishedConnection)
	END_MSG_MAP()

	LRESULT OnSessionChange(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT OnSysCommand(UINT , WPARAM wParam , LPARAM , BOOL& );
	LRESULT OnCreate(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT OnDestroy(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT OnNotifyXMLUpdate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnNotifyXMLUnreaded(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnPurgeHistory(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnErrorConnection(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnSetVariable(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnScreensavers(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnWallpapers(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	//
	// Necessary for restart DeskAlerts
	//
	LRESULT OnEstablishedConnection(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

public:
	CAlertLoader(NodeALERTS::NodeCOMMANDS::NodeALERTCOMMAND* param);
	~CAlertLoader(void);

public:
	void addAlertListener(IAlertListener* newListener);
	void removeAlertListener(IAlertListener* newListener);
	LPVOID getAlert(CString& /*promertyName*/);
	static CString getTempPath();
	static CString getTempFileName(CString prefix,int i);
	static void appendCustomHtmlToAlertFile(shared_ptr<showData> &ex);

private:
	//
	// Maximov A. 25.07.2017
	// It's necessary for restart DeskAlerts when lost connection
	// Start
	//
	HWND m_mainFrameHWND;
	HWND m_sysMenuHWND;
	//
	// It's necessary for restart DeskAlerts when lost connection
	// Finish
	//

	DWORD m_dwThreadId;
	HANDLE m_timerThread;
	DWORD m_dwThreadAdId;
	HANDLE m_timerThreadAd;
	void myRun(IActionEvent& event);
	void myStop();

public:
	void setMainFrameHWND(HWND hWnd);
	CString getName();
	void run(IActionEvent& event);
	void stop();
	BOOL isAutoStart();

private:

	struct ThreadData
	{
		HWND hwnd;
		CString url;
		CString pixelUrl;
		CString screensaversUrl;
		CString wallpapersUrl;
		UINT expire;
	};

	//static vector<wstring> CheckUserGroups(IADsUser *pUser);
	void createTimerThread(IActionEvent& event);
	void createTimerThreadForAdSync(IActionEvent&);
	void createLifeTimeExpirationCheckThread();
	void alertListUpdated(CString xmlPath, shared_ptr<NodeALERT> curNode = shared_ptr<NodeALERT>());
	sqlite_int64 addToHistory(shared_ptr<NodeALERT> curNode, CString &window);

	static DWORD WINAPI AlertLoaderFunc(LPVOID lpParam);
	static DWORD WINAPI SynchronizationFunc(LPVOID lpParam);

	HWND m_parentHWND;

	NodeALERTS::NodeCOMMANDS::NodeALERTCOMMAND data;

	BOOL IsReadyToDownload(CString name = CString());
	static void synchronizeScreensavers(ThreadData* thData, CString hash);
	static void synchronizeWallpapers(ThreadData* thData, CString hash);
};
