#include "StdAfx.h"
#include "Encoder.h"

Encoder::Encoder(void)
{
}

Encoder::~Encoder(void)
{
}

CString Encoder::encodeURIComponent(CString sIn, const int encode)
{
	return URLEncode(sIn, _T("~!*()_-'."), encode);
}

CString Encoder::URLEncode(CString sIn, LPCTSTR exclude, const int encode)
{
	const static LPCTSTR hexequiv = _T("0123456789ABCDEF");

	CString sOut;
	// process each code point
	for (int i = 0; i < sIn.GetLength(); i++)
	{
		CString n = sIn.Mid(i, 1);
		if( (n >= L'a' && n <= L'z') ||
			(n >= L'A' && n <= L'Z') ||
			(n >= L'0' && n <= L'9') ||
			(exclude && _tcsstr(exclude, n)))
		{
			sOut += n;
		}
		else
		{
			CT2A inStr(n, encode);
			for (size_t i = 0; i < strlen(inStr); i++)
			{
				sOut += L"%";
				sOut += hexequiv[(inStr[i] >> 4) & 0xF];
				sOut += hexequiv[inStr[i] & 0xF];
			}
		}
	}

	return sOut;
}