//
//  RC4.cpp
//  DeskAlerts Client
//  Common module
//
//  Copyright Softomate 2006-2012. All rights reserved.
//

#include "stdafx.h"
#include "RC4.h"

/*
RC4::RC4(void)
{
}

RC4::~RC4(void)
{
}
*/


void RC4::swap_char(unsigned char *a, unsigned char *b)
{
	unsigned char t;
	t = a[0]; a[0] = b[0]; b[0] = t;
};

//===================================================================================

void RC4::RC4_Start(RC4_Session &session, const char *key, unsigned long key_len)
{
	unsigned long i, j=0;
	unsigned char kbox[256];
	for (i=0; i<SBOX_SIZE_DIV_8; i++)
	{
		session.sbox[i]     = (i     & 0xFF);
		session.sbox[i+32]  = (i+32  & 0xFF);
		session.sbox[i+64]  = (i+64  & 0xFF);
		session.sbox[i+96]  = (i+96  & 0xFF);
		session.sbox[i+128] = (i+128 & 0xFF);
		session.sbox[i+160] = (i+160 & 0xFF);
		session.sbox[i+192] = (i+192 & 0xFF);
		session.sbox[i+224] = (i+224 & 0xFF);
	};
	if (key_len > 0)
	{
		for (i=0; i<256; i++)
		{
			kbox[i] = key[j];
			if (++j == key_len) { j = 0; };
		};
	}
	else
	{

		for (i=0; i<256; i++) { kbox[i] = 0; };
	};
	j = 0;
	for (i=0; i<SBOX_SIZE; i++)
	{
		j = (j + kbox[i] + session.sbox[i]) & 0xFF;
		swap_char(&session.sbox[i], &session.sbox[j]);
	};
	session.i = 0;
	session.j = 0;
};

//===================================================================================

void RC4::RC4_F(RC4_Session &session)
{
	unsigned char xi, a, b;
	a = session.i;
	b = session.j;
	for (unsigned long i=0; i<session.data_len; i++)
	{
		a++;
		b += session.sbox[a];
		swap_char(&session.sbox[a], &session.sbox[b]);
		xi = session.sbox[b] + session.sbox[a];
		session.data[i] ^= session.sbox[xi];
	};
	session.i = a;
	session.j = b;
};

//===================================================================================

unsigned char RC4::RC4_Gen(RC4_Session &session)
{
	unsigned char xi, a, b;
	a = session.i;
	b = session.j;
	a++;
	b += session.sbox[a];
	swap_char(&session.sbox[a], &session.sbox[b]);
	xi = session.sbox[b] + session.sbox[a];
	xi = session.sbox[xi];
	session.i = a;
	session.j = b;
	return xi;
};

//===================================================================================



LRESULT RC4::decode(const char *source, const char *dest, char *k)
{
	char *teststr;
	long sz ;

	FILE* fp = fopen(source, "rb" );
	if (fp)
	{
		fseek(fp, 0, SEEK_END);
		sz = ftell(fp);
		if (sz == 0)
			return S_FALSE;
		fseek(fp, 0, SEEK_SET);
		teststr = new char[(sz + 2)];
		int numread = fread(teststr, 1, sz, fp);
		fclose(fp);
		teststr[sz] = '\0';
	}

	RC4_Session rec;

	if ( k==NULL )
	{
		k = new char[100];
		sprintf(k, "nj6kjvk58s8wb2d73");
	}

	rec.data = teststr;
	rec.data_len = sz;


	RC4_Start(rec, k, strlen(k));

	FILE *fp2 = fopen(dest, "w");
	for (int i=0; i<sz; i++)
	{
		teststr[i] ^= RC4_Gen(rec);
		fprintf(fp2, "%c", teststr[i]);
	};
	fclose(fp2);

	return S_OK;
}