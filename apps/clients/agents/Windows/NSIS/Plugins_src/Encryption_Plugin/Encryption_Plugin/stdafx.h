// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"
/*
#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS      // some CString constructors will be explicit

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN            // Exclude rarely-used stuff from Windows headers
#endif
*/
#include <atlbase.h>

#include <iostream>
// Windows Header Files:
#include <windows.h>

#ifdef ENCRYPTPLUGIN_EXPORTS
#define ENCRYPTPLUGIN_API __declspec(dllexport)
#else
#define ENCRYPTPLUGIN_API __declspec(dllimport)
#endif