#include <initguid.h>
#include <windows.h>
#include <iostream>
#include <winnt.h>

#include "iadmw.h" // COM Interface header
#include "iiscnfg.h" // MD_ & IIS_MD_ #defines


#include <string>

#include "pluginapi.h"

// Here we assume that client of this class runs on the same computer, not remotelly.
class ENCRYPTPLUGIN_API CEncryptPlugin {

public:	


};

extern "C" ENCRYPTPLUGIN_API void RC4_Encrypt( HWND hwndParent, int string_size, 
											  char *variables, stack_t **stacktop);


extern "C" ENCRYPTPLUGIN_API void URLEncode( HWND hwndParent, int string_size, 
											  char *variables, stack_t **stacktop);