#include <initguid.h>
#include <windows.h>
#include <iostream>
#include <winnt.h>

#include "iadmw.h" // COM Interface header
#include "iiscnfg.h" // MD_ & IIS_MD_ #defines


#include <string>

#include "pluginapi.h"

extern "C" ENCRYPTPLUGIN_API void GetWebSitesWithDetails( HWND hwndParent, int string_size, 
												   char *variables, stack_t **stacktop);