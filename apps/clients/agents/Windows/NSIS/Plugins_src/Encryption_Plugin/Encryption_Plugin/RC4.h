//
//  RC4.h
//  DeskAlerts Client
//  Common module
//
//  Copyright Softomate 2006-2012. All rights reserved.
//

#pragma once

#include "conio.h"
#include "stdio.h"
#include <string.h>
#include "tchar.h"
#include "stdio.h"

class RC4
{
public:

#define SBOX_SIZE 256
#define SBOX_SIZE_DIV_8 32

	typedef struct
	{
		unsigned char i;
		unsigned char j;
		unsigned char sbox[SBOX_SIZE];
		char *data;
		unsigned long data_len;
	} RC4_Session;

	static void swap_char(unsigned char *a, unsigned char *b);
	static void RC4_Start(RC4_Session &session, const char *key, unsigned long key_len);
	static void RC4_F(RC4_Session &session);
	static unsigned char RC4_Gen(RC4_Session &session);
	static LRESULT decode(const char *source, const char *dest, char *k);

	//RC4(void);
	//~RC4(void);
};
