// Encryption_plugin.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "Encryption_plugin.h"
#include "pluginapi.h"


#include <stddef.h>
#include <string>
#include <sstream>

#include <iostream>
#include <fstream>
#include <atlstr.h>
#include "RC4.h"
#include "Encoder.h"


#define INITGUID

#define MAKESTR(VAR,LEN) \
	TCHAR *VAR = new TCHAR[LEN]; \
	memset(VAR, 0, sizeof(TCHAR) * LEN)

namespace
{
	static std::wstring swzVirtDir;

	char* narrow( const std::wstring& str )
	{
		std::ostringstream stm ;
		const std::ctype<char>& ctfacet = std::use_facet< std::ctype<char> >( stm.getloc() ) ;

		for( size_t i=0 ; i < str.size() ; ++i )
			stm << ctfacet.narrow( str[i], 0 ) ;

		std::string strTemp = stm.str();
		char* c = new char [ strTemp.size()+1];
		strcpy(c, strTemp.c_str());
		return c;
	}

}

// The one and only application object


extern "C" ENCRYPTPLUGIN_API void RC4_Encrypt( HWND hwndParent, int string_size, 
													   char *variables, stack_t **stacktop)
{
	EXDLL_INIT();

	char *encodedString = new char[g_stringsize];
	popstringn(encodedString,0); 

	char *encryptKey = new char[g_stringsize];
	popstringn(encryptKey,0); 

	char *contentLength = new char[g_stringsize];
	popstringn(contentLength,0); 

	RC4::RC4_Session rec;

	rec.data = encodedString;
	rec.data_len = atoi(contentLength);

	RC4::RC4_Start(rec, encryptKey,strlen(encryptKey));

	for (unsigned long i=0; i < rec.data_len; i++)
	{
		rec.data[i] ^= RC4::RC4_Gen(rec);
	}

	pushstring(rec.data);
}

extern "C" ENCRYPTPLUGIN_API void URLEncode( HWND hwndParent, int string_size, char *variables, stack_t **stacktop )
{
	EXDLL_INIT();

	char *url = new char[g_stringsize];
	popstringn(url,0); 

	CString urlStr(url);

	CString res = Encoder::encodeURIComponent(urlStr,CP_UTF8);

	pushstring(CW2A(res.GetBuffer()));

}
