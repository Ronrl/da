﻿// test.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "../Encryption_Plugin/RC4.h"
#include <atlstr.h>

#include <string>
#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <wininet.h>


/* Converts a hex character to its integer value */
char from_hex(char ch) {
	return isdigit(ch) ? ch - '0' : tolower(ch) - 'a' + 10;
}

/* Converts an integer value to its hex character*/
char to_hex(char code) {
	static char hex[] = "0123456789abcdef";
	return hex[code & 15];
}

/* Returns a url-encoded version of str */
/* IMPORTANT: be sure to free() the returned string after use */
char *url_encode(char *str) {
	char *pstr = str, *buf = (char*)malloc(strlen(str) * 3 + 1), *pbuf = buf;
	while (*pstr) {
		if (isalnum(*pstr) || *pstr == '-' || *pstr == '_' || *pstr == '.' || *pstr == '~') 
			*pbuf++ = *pstr;
		else if (*pstr == ' ') 
			*pbuf++ = '+';
		else 
			*pbuf++ = '%', *pbuf++ = to_hex(*pstr >> 4), *pbuf++ = to_hex(*pstr & 15);
		pstr++;
	}
	*pbuf = '\0';
	return buf;
}

/* Returns a url-decoded version of str */
/* IMPORTANT: be sure to free() the returned string after use */
char *url_decode(char *str) {
	char *pstr = str, *buf = (char*)malloc(strlen(str) + 1), *pbuf = buf;
	while (*pstr) {
		if (*pstr == '%') {
			if (pstr[1] && pstr[2]) {
				*pbuf++ = from_hex(pstr[1]) << 4 | from_hex(pstr[2]);
				pstr += 2;
			}
		} else if (*pstr == '+') { 
			*pbuf++ = ' ';
		} else {
			*pbuf++ = *pstr;
		}
		pstr++;
	}
	*pbuf = '\0';
	return buf;
}

inline BYTE toHex(const BYTE &x)
{
	return x > 9 ? x + 55: x + 48;
}

CString URLEncode2(CString sIn)
{
	CString sOut;

	const int nLen = sIn.GetLength() + 1;

	register LPBYTE pOutTmp = NULL;
	LPBYTE pOutBuf = NULL;
	register LPBYTE pInTmp = NULL;
	LPBYTE pInBuf =(LPBYTE)sIn.GetBuffer(nLen);
	BYTE b = 0;

	//alloc out buffer
	pOutBuf = (LPBYTE)sOut.GetBuffer(nLen  * 3 - 2);//new BYTE [nLen  * 3];

	if(pOutBuf)
	{
		pInTmp	= pInBuf;
		pOutTmp = pOutBuf;

		// do encoding
		while (*pInTmp)
		{
			if(isalnum(*pInTmp))
				*pOutTmp++ = *pInTmp;
			else
				if(isspace(*pInTmp))
					*pOutTmp++ = '+';
				else
				{
					*pOutTmp++ = '%';
					*pOutTmp++ = toHex(*pInTmp>>4);
					*pOutTmp++ = toHex(*pInTmp%16);
				}
				pInTmp++;
		}
		*pOutTmp = '\0';
		//sOut=pOutBuf;
		//delete [] pOutBuf;
		sOut.ReleaseBuffer();
	}
	sIn.ReleaseBuffer();
	return sOut;
}

CString URLEncode(CString sIn, LPCTSTR exclude, const int encode = CP_UTF8);
CString encodeURIComponent(CString sIn, const int encode)
{
	return URLEncode(sIn, _T("~!*()_-'."), encode);
}

CString URLEncode(CString sIn, LPCTSTR exclude, const int encode)
{
	const static LPCTSTR hexequiv = _T("0123456789ABCDEF");

	CString sOut;
	// process each code point
	for (int i = 0; i < sIn.GetLength(); i++)
	{
		CString n = sIn.Mid(i, 1);
		if( (n >= L'a' && n <= L'z') ||
			(n >= L'A' && n <= L'Z') ||
			(n >= L'0' && n <= L'9') ||
			(exclude && _tcsstr(exclude, n)))
		{
			sOut += n;
		}
		else
		{
			CT2A inStr(n, encode);
			for (size_t i = 0; i < strlen(inStr); i++)
			{
				sOut += L"%";
				sOut += hexequiv[(inStr[i] >> 4) & 0xF];
				sOut += hexequiv[inStr[i] & 0xF];
			}
		}
	}

	return sOut;
}




int _tmain(int argc, _TCHAR* argv[])
{
	
}

