#pragma once


//-------------------------------------------------------------------------------------------
// Internal use routines

bool	KillProc( char *szProcess );


//-------------------------------------------------------------------------------------------
// Exported routines

extern "C" __declspec(dllexport) void	KillProcess( HWND		hwndParent, 
													 int		string_size,
													 char		*variables, 
													 stack_t	**stacktop );