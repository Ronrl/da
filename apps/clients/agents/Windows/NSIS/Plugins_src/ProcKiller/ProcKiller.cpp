#include "stdafx.h"
#include "ProcKiller.h"
#include "string.h"
#include "windows.h"
#include <atlstr.h>
#include <TLHELP32.h>
#include <aclapi.h>

#define TA_SUCCESS_KILL	1
#define TA_FAILED	2
#define TA_SUCCESS_CLEAN 3

// main DLL entry
BOOL WINAPI		_DllMainCRTStartup( HANDLE	hInst, 
									ULONG	ul_reason_for_call,
									LPVOID	lpReserved )
{
    return TRUE;
}

BOOL SetPrivilege(
				  HANDLE hToken,          // token handle
				  LPCTSTR Privilege,      // Privilege to enable/disable
				  BOOL bEnablePrivilege   // TRUE to enable.  FALSE to disable
				  )
{
	TOKEN_PRIVILEGES tp;
	LUID luid;
	TOKEN_PRIVILEGES tpPrevious;
	DWORD cbPrevious=sizeof(TOKEN_PRIVILEGES);

	if(!LookupPrivilegeValue( NULL, Privilege, &luid )) return FALSE;

	// 
	// first pass.  get current privilege setting
	// 
	tp.PrivilegeCount           = 1;
	tp.Privileges[0].Luid       = luid;
	tp.Privileges[0].Attributes = 0;

	AdjustTokenPrivileges(
		hToken,
		FALSE,
		&tp,
		sizeof(TOKEN_PRIVILEGES),
		&tpPrevious,
		&cbPrevious
		);

	if (GetLastError() != ERROR_SUCCESS) return FALSE;

	// 
	// second pass.  set privilege based on previous setting
	// 
	tpPrevious.PrivilegeCount       = 1;
	tpPrevious.Privileges[0].Luid   = luid;

	if(bEnablePrivilege) {
		tpPrevious.Privileges[0].Attributes |= (SE_PRIVILEGE_ENABLED);
	}
	else {
		tpPrevious.Privileges[0].Attributes ^= (SE_PRIVILEGE_ENABLED &
			tpPrevious.Privileges[0].Attributes);
	}

	AdjustTokenPrivileges(
		hToken,
		FALSE,
		&tpPrevious,
		cbPrevious,
		NULL,
		NULL
		);

	if (GetLastError() != ERROR_SUCCESS) return FALSE;

	return TRUE;
}

bool CreateSecurityAttributes(ACCESS_MASK amAccessRights, PSECURITY_ATTRIBUTES* ppSecurityAttributes)
{
	PSECURITY_DESCRIPTOR pSecDesc = NULL;
	PSECURITY_ATTRIBUTES pSecAttr = NULL;
	PACL pDacl = NULL;
	PSID pSidEveryone = NULL;
	PSID pSidSystem = NULL;
	DWORD dwSidSize = SECURITY_MAX_SID_SIZE;
	EXPLICIT_ACCESS ExplicitAccess [2];
	DWORD dwResult = ERROR_SUCCESS;

	//
	// Check input parameters
	//

	if (! ppSecurityAttributes) return false;

	//
	// Allocate security attributes
	//

	pSecAttr = (PSECURITY_ATTRIBUTES) HeapAlloc (GetProcessHeap (), HEAP_ZERO_MEMORY, sizeof (SECURITY_ATTRIBUTES) * 2);
	if (! pSecAttr) return false;

	//
	// Allocate security descriptor
	//

	pSecDesc = (PSECURITY_DESCRIPTOR) HeapAlloc (GetProcessHeap (), HEAP_ZERO_MEMORY, SECURITY_DESCRIPTOR_MIN_LENGTH * 2);
	if (! pSecDesc)
	{
		HeapFree (GetProcessHeap (), 0, pSecAttr);
		return false;
	}

	//
	// Initialize security descriptor
	//

	if (! InitializeSecurityDescriptor (
		pSecDesc,
		SECURITY_DESCRIPTOR_REVISION))
	{
		HeapFree (GetProcessHeap (), 0, pSecAttr);
		HeapFree (GetProcessHeap (), 0, pSecDesc);
		return false;
	}

	//
	// Allocate storage for SIDs
	//

	pSidEveryone = (PSID) HeapAlloc (GetProcessHeap (), HEAP_ZERO_MEMORY, dwSidSize);
	if (! pSidEveryone)
	{
		HeapFree (GetProcessHeap (), 0, pSecAttr);
		HeapFree (GetProcessHeap (), 0, pSecDesc);
		return false;
	}

	pSidSystem = (PSID) HeapAlloc (GetProcessHeap (), HEAP_ZERO_MEMORY, dwSidSize);
	if (! pSidSystem)
	{
		HeapFree (GetProcessHeap (), 0, pSecAttr);
		HeapFree (GetProcessHeap (), 0, pSecDesc);
		HeapFree (GetProcessHeap (), 0, pSidEveryone);
		return false;
	}

	//
	// Get SID for any user
	//

	if (! CreateWellKnownSid (
		WinWorldSid,
		NULL,
		pSidEveryone,
		&dwSidSize))
	{
		HeapFree (GetProcessHeap (), 0, pSecAttr);
		HeapFree (GetProcessHeap (), 0, pSecDesc);
		HeapFree (GetProcessHeap (), 0, pSidEveryone);
		HeapFree (GetProcessHeap (), 0, pSidSystem);
		return false;
	}

	//
	// Get SID for 'SYSTEM' account
	//

	dwSidSize = SECURITY_MAX_SID_SIZE;

	if (! CreateWellKnownSid (
		WinLocalSystemSid,
		NULL,
		pSidSystem,
		&dwSidSize))
	{
		HeapFree (GetProcessHeap (), 0, pSecAttr);
		HeapFree (GetProcessHeap (), 0, pSecDesc);
		HeapFree (GetProcessHeap (), 0, pSidEveryone);
		HeapFree (GetProcessHeap (), 0, pSidSystem);
		return false;
	}

	//
	// 'Everyone' account
	//

	ExplicitAccess [0].grfAccessPermissions = amAccessRights;
	ExplicitAccess [0].grfAccessMode = GRANT_ACCESS;
	ExplicitAccess [0].grfInheritance = CONTAINER_INHERIT_ACE | OBJECT_INHERIT_ACE | SUB_CONTAINERS_AND_OBJECTS_INHERIT;
	ExplicitAccess [0].Trustee.TrusteeForm = TRUSTEE_IS_SID;
	ExplicitAccess [0].Trustee.TrusteeType = TRUSTEE_IS_WELL_KNOWN_GROUP;
	ExplicitAccess [0].Trustee.ptstrName  = (LPTSTR) pSidEveryone;

	//
	// 'SYSTEM' account
	//

	ExplicitAccess [1].grfAccessPermissions = amAccessRights;
	ExplicitAccess [1].grfAccessMode = GRANT_ACCESS;
	ExplicitAccess [1].grfInheritance = CONTAINER_INHERIT_ACE | OBJECT_INHERIT_ACE | SUB_CONTAINERS_AND_OBJECTS_INHERIT;
	ExplicitAccess [1].Trustee.TrusteeForm = TRUSTEE_IS_SID;
	ExplicitAccess [1].Trustee.TrusteeType = TRUSTEE_IS_WELL_KNOWN_GROUP;
	ExplicitAccess [1].Trustee.ptstrName  = (LPTSTR) pSidSystem;

	//
	// Set up ACL object
	//

	dwResult = SetEntriesInAcl (
		sizeof (ExplicitAccess) / sizeof (EXPLICIT_ACCESS),
		ExplicitAccess,
		NULL,
		&pDacl);

	if (dwResult != ERROR_SUCCESS)
	{
		HeapFree (GetProcessHeap (), 0, pSecAttr);
		HeapFree (GetProcessHeap (), 0, pSecDesc);
		HeapFree (GetProcessHeap (), 0, pSidEveryone);
		HeapFree (GetProcessHeap (), 0, pSidSystem);
		return false;
	}

	//
	// Update security descriptor
	//

	if (! SetSecurityDescriptorDacl (pSecDesc, TRUE, pDacl, TRUE))
	{
		HeapFree (GetProcessHeap (), 0, pSecAttr);
		HeapFree (GetProcessHeap (), 0, pSecDesc);
		HeapFree (GetProcessHeap (), 0, pSidEveryone);
		HeapFree (GetProcessHeap (), 0, pSidSystem);
		LocalFree (pDacl);
		return false;
	}

	//
	// Compose security attributes
	//

	pSecAttr -> bInheritHandle = FALSE;
	pSecAttr -> nLength = sizeof (SECURITY_ATTRIBUTES);
	pSecAttr -> lpSecurityDescriptor = pSecDesc;

	//
	// Return security security
	//

	*ppSecurityAttributes = pSecAttr;

	//
	// Success ;)
	//

	return true;
}


bool	KillProc( char *szProcess )
{
	char		szProcessName[ 1024 ];
	char		szProcessNameWE[ 1024 ];
	char		szCurrentProcessName[ 1024 ];
	DWORD		dPID[ 1024 ];
	DWORD		dPIDSize( 1024 );
	DWORD		dSize( 1024 );
	HANDLE		hProcess;
	HMODULE		phModule[ 1024 ];

	//
	// make the name lower case
	//
	memset( szProcessName, 0, 1024*sizeof(char) );
	memset( szProcessNameWE,0, 1024*sizeof(char) );
	sprintf( szProcessNameWE, "%s%s", szProcess,".exe" );
	sprintf( szProcessName, "%s", szProcess );
	strlwr( szProcessName );
	strlwr( szProcessNameWE );

	PROCESSENTRY32 entry;
	entry.dwSize = sizeof(PROCESSENTRY32);

	const HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);

	if (Process32First(snapshot, &entry) == TRUE)
	{
		do
		{
			PROCESSENTRY32 entry;
			entry.dwSize = sizeof(PROCESSENTRY32);


			if (Process32First(snapshot, &entry) == TRUE)
			{
				while (Process32Next(snapshot, &entry) == TRUE)
				{
					CString processName =  szProcessName;
					processName += _T(".exe");
					if (_tcsicmp(entry.szExeFile, processName) == 0)
					{

						HANDLE hToken;

						if(!OpenThreadToken(GetCurrentThread(), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, FALSE, &hToken))
						{
							if (GetLastError() == ERROR_NO_TOKEN)
							{
								if (ImpersonateSelf(SecurityImpersonation))
								{
									OpenThreadToken(GetCurrentThread(), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, FALSE, &hToken);
								}
							}
						}	

						if (hToken)
						{
							SetPrivilege(hToken, SE_DEBUG_NAME, TRUE);

							HANDLE hProcess = OpenProcess(PROCESS_TERMINATE|SYNCHRONIZE, FALSE,entry.th32ProcessID);
							if (hProcess)
							{
								if (WaitForSingleObject(hProcess,5000)==WAIT_TIMEOUT)
								{
									TerminateProcess(hProcess,0);
									WaitForSingleObject(hProcess,5000);
								}
							}

							SetPrivilege(hToken, SE_DEBUG_NAME, FALSE);
						}
					}
				}
			}
		}
		while (Process32Next(snapshot, &entry) == TRUE);
	}
	CloseHandle(snapshot);

	return false;
}

//-------------------------------------------------------------------------------------------
extern "C" __declspec(dllexport) void	KillProcess( HWND		hwndParent, 
													 int		string_size,
													 char		*variables, 
													 stack_t	**stacktop )
{
	char		szParameter[ 1024 ];

	EXDLL_INIT();
	{
		popstring( szParameter );

		if( true == KillProc( szParameter ) )
			wsprintf( szParameter, "1" );
		else
			wsprintf( szParameter, "0" );

		setuservariable( INST_R0, szParameter );
	}
}