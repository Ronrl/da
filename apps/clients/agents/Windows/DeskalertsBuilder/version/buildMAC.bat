@echo on
pushd "%~dp0"
copy /y ..\..\release\deskalert.cab ..\mac\deskalerts.cab
cd ..\mac
converter.exe
move /y deskalerts.zip ..\..\release\deskalert.zip
move /y deskalerts_setup.zip ..\..\release\deskalerts_setup.zip
del deskalerts.cab
popd
