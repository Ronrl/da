@echo on
pushd "%~dp0"
if exist deskalerts.cab del deskalerts.cab
if exist deskalerts_setup.cab del deskalerts_setup.cab
if exist deskalerts_setup.exe del deskalerts_setup.exe

call cabarc -r n deskalerts_setup.cab *.bmp *.gif *.png *.jpg *.html *.xml *.txt deskalerts.dll deskalerts.exe xcpy.exe *.wav *.js *.css  hg29fhh847c03nu08f.key  

move /y deskalerts_setup.cab ..\nsisdesk\deskalerts_setup.cab
cd ..\nsisdesk

call cab2NSIS.bat    "/DPRODUCT_VERSION=0.0.0.0"  "/DPRODUCT_NAME=DeskAlerts"
if not exist deskalerts_setup.exe goto end1
move /y deskalerts_setup.exe ..\..\release\deskalerts_setup.exe
:end1

call cab2NSIS.bat    "/DPRODUCT_VERSION=0.0.0.0"  "/DIsSilent" "/DPRODUCT_NAME=DeskAlerts"
if not exist deskalerts_setup.exe goto end2
move /y deskalerts_setup.exe ..\..\release\deskalerts_setup_silent.exe
:end2

move /y deskalerts_setup.cab ..\..\release\deskalert.cab
cd ..\version
copy /y version.txt ..\..\release\version.txt
popd
