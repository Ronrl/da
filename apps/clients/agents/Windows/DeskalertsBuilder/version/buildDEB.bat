@echo on
pushd "%~dp0"
copy /y ..\..\release\deskalert.cab ..\deb\deskalert.cab
cd ..\deb
del /F /Q usr\share\Softomate\DeskAlerts\html\* updateControls\i386\* updateControls\amd64\*
copy /y scripts\postinst_deskalertsRepo scripts\postinst_deskalertsRepo_origin
debConverter.exe

del usr\share\Softomate\DeskAlerts\html\*.exe
del usr\share\Softomate\DeskAlerts\html\*.dll
copy /y usr\share\Softomate\DeskAlerts\html\conf.xml etc\deskalerts\deskalerts.conf
copy /y scripts\postrm_deskalerts postrm
copy /y scripts\prerm_deskalerts prerm
copy /y scripts\preinst_deskalerts preinst
copy /y scripts\postinst_deskalerts postinst

::remove icon script
if exist usr\share\Softomate\DeskAlerts\desktopIcon.sh del usr\share\Softomate\DeskAlerts\desktopIcon.sh
 
copy /y binaries\DeskAlerts_i386 usr\bin\DeskAlerts
copy /y binaries\DeskAlertsModule_i386 usr\bin\DeskAlertsModule
copy /y controls\control_i386 control
chmod 0755 -R usr
chmod 0755 -R etc
chmod -x usr/share/gnome/autostart/DeskAlerts.desktop usr/share/applications/DeskAlerts.desktop usr/share/pixmaps/DeskAlerts.png usr/share/doc/DeskAlerts/copyright usr/share/doc/DeskAlerts/changelog.gz usr/share/man/man8/DeskAlerts.8.gz usr/share/man/man8/DeskAlerts-static.8.gz etc/deskalerts/deskalerts.conf etc/xdg/autostart/DeskAlerts.desktop
chmod -x -R usr/share/Softomate/DeskAlerts/html
chmod 0755 usr/bin/DeskAlerts
chmod 0755 usr/bin/DeskAlertsModule
chmod 0755 usr/share/Softomate/DeskAlerts/html
chmod 0755 control postinst postrm preinst
chmod 0644 conffiles
tar c --owner=root --group=root -f data.tar ./usr ./etc
gzip -f data.tar
tar c --owner=root --group=root -f ./control.tar ./control ./preinst ./postinst ./postrm ./conffiles
gzip -f control.tar
ar -r DeskAlerts_i386.deb debian-binary control.tar.gz data.tar.gz

copy /y DeskAlerts_i386.deb update\update\DeskAlerts_i386.deb

move /y DeskAlerts_i386.deb ..\..\release\DeskAlerts_i386.deb

copy /y binaries\DeskAlerts_amd64 usr\bin\DeskAlerts
copy /y binaries\DeskAlertsModule_amd64 usr\bin\DeskAlertsModule
copy /y controls\control_amd64 control
chmod 0755 -R usr
chmod 0755 -R etc
chmod -x usr/share/gnome/autostart/DeskAlerts.desktop usr/share/applications/DeskAlerts.desktop usr/share/pixmaps/DeskAlerts.png usr/share/doc/DeskAlerts/copyright usr/share/doc/DeskAlerts/changelog.gz usr/share/man/man8/DeskAlerts.8.gz usr/share/man/man8/DeskAlerts-static.8.gz etc/deskalerts/deskalerts.conf etc/xdg/autostart/DeskAlerts.desktop
chmod -x -R usr/share/Softomate/DeskAlerts/html
chmod 0755 usr/bin/DeskAlerts
chmod 0755 usr/bin/DeskAlertsModule
chmod 0755 usr/share/Softomate/DeskAlerts/html
chmod 0755 control postinst postrm preinst
chmod 0644 conffiles
tar c --owner=root --group=root -f data.tar ./usr ./etc
gzip -f data.tar
tar c --owner=root --group=root -f control.tar ./control ./preinst ./postinst ./postrm ./conffiles
gzip -f control.tar
ar -r DeskAlerts_amd64.deb debian-binary control.tar.gz data.tar.gz


copy /y DeskAlerts_amd64.deb update\update\DeskAlerts_amd64.deb

move /y DeskAlerts_amd64.deb ..\..\release\DeskAlerts_amd64.deb

copy /y controls\control_repo control
copy /y scripts\postinst_deskalertsRepo postinst_win
sed s/\\r// postinst_win > postinst
del postinst_win
mkdir usr\share\doc\deskalertsRepo
copy /y usr\share\doc\deskalerts\copyright usr\share\doc\deskalertsRepo\copyright
copy /y usr\share\doc\deskalerts\changelog.gz usr\share\doc\deskalertsRepo\changelog.gz
chmod 0755 usr/share/doc/deskalertsRepo
chmod 0644 usr/share/doc/deskalertsRepo/copyright
chmod 0644 usr/share/doc/deskalertsRepo/changelog.gz
tar c --owner=root --group=root -f data.tar ./usr/share/doc/deskalertsRepo
gzip -f data.tar
tar c --owner=root --group=root -f control.tar ./control ./postinst
gzip -f control.tar
ar -r DeskAlertsRepo.deb debian-binary control.tar.gz data.tar.gz
del usr\share\doc\deskalertsRepo\copyright
del usr\share\doc\deskalertsRepo\changelog.gz
rmdir usr\share\doc\deskalertsRepo
del scripts\postinst_deskalertsRepo
move /y scripts\postinst_deskalertsRepo_origin scripts\postinst_deskalertsRepo
copy /y DeskAlertsRepo.deb update\update\DeskAlertsRepo.deb
move /y DeskAlertsRepo.deb ..\..\release\DeskAlertsRepo.deb

debConverter.exe hash
gzip updateControls/i386/Packages
gzip updateControls/amd64/Packages
copy /y updateControls\i386\Packages.gz update\dists\softomate\office\binary-i386\Packages.gz
copy /y updateControls\amd64\Packages.gz update\dists\softomate\office\binary-amd64\Packages.gz

7za a updateDeb -tzip -y update
move /y updateDeb.zip ..\..\release\updateDeb.zip
