@echo on
pushd "%~dp0"
dapcr.exe deskalerts_setup.wxs tmp.wxs
cd ..\msi
wix\heat.exe dir "..\version" -o "skinslist.wxs" -scom -frag -srd -sreg -cg "skinsGroup" -gg -dr "APPLICATIONROOTDIRECTORY"
wix\candle.exe -ext wix\WixUtilExtension.dll skinslist.wxs ..\version\tmp.wxs 
wix\light.exe -ext wix\WixUIExtension.dll -ext wix\WixUtilExtension.dll -cultures:en-us tmp.wixobj skinslist.wixobj -out ..\..\release\deskalerts_setup.msi -b "..\version"
del skinlist.wxs
del tmp.wixobj
cd ..\version
del tmp.wxs
del ..\..\release\deskalerts_setup.wixpdb
popd
