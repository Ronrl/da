
!macro EnableButtons
	Push $0
	GetDlgItem $0 $HWNDPARENT 1
	EnableWindow $0 1

	GetDlgItem $0 $HWNDPARENT 2
	EnableWindow $0 1

	GetDlgItem $0 $HWNDPARENT 3
	EnableWindow $0 1
	Pop $0
!macroend

!macro DisableButtons
	Push $0
	GetDlgItem $0 $HWNDPARENT 1
	EnableWindow $0 0

	GetDlgItem $0 $HWNDPARENT 2
	EnableWindow $0 0

	GetDlgItem $0 $HWNDPARENT 3
	EnableWindow $0 0
	Pop $0
!macroend

!define EnableButtons '!insertmacro "EnableButtons"'
!define DisableButtons '!insertmacro "DisableButtons"'
