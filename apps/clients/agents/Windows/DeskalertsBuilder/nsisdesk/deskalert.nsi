!ifndef PRODUCT_NAME
	!define PRODUCT_NAME "DeskAlerts"
	!define BRANDING_TEXT "DeskAlerts http://www.deskalerts.com"
!else
	!define BRANDING_TEXT "${PRODUCT_NAME}"
!endif
!define PRODUCT_PUBLISHER "Softomate, LLC."
!define PRODUCT_COMPANY "Softomate"
!define PRODUCT_WEB_SITE "http://www.softomate.com"
!define PRODUCT_DIR_REGKEY "Software\Microsoft\Windows\CurrentVersion\App Paths\DeskAlerts.exe"
!define PRODUCT_UNINST_KEY "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_NAME}"
!define PRODUCT_UNINST_ROOT_KEY "HKLM"
!define PRODUCT_STARTMENU_REGVAL "NSIS:StartMenuDir"

!ifdef NOSERVER
	!define TRIAL
!endif

!include "WinMessages.nsh"
!include "FileFunc.nsh"
!include "UAC.nsh"
!insertmacro GetParameters
!insertmacro un.GetParameters
!insertmacro un.GetOptions
!include "XML.nsh"
;!include "LogicLib.nsh"

; MUI 1.67 compatible ------
!include "MUI.nsh"

; MUI Settings
!define MUI_HEADERIMAGE
!define MUI_HEADERIMAGE_BITMAP "deskalerts.bmp" ; optional

; MUI Settings / Wizard
;!define MUI_WELCOMEFINISHPAGE_BITMAP "dlgbmp.bmp"
;!define MUI_UNWELCOMEFINISHPAGE_BITMAP "dlgbmp.bmp"

;!define MUI_HEADERIMAGE_RIGHT
!define MUI_ABORTWARNING
!define MUI_HEADERIMAGE_BITMAP_NOSTRETCH
!define MUI_HEADER_TRANSPARENT_TEXT
!define MUI_ICON "${NSISDIR}\Contrib\Graphics\Icons\modern-install.ico"
!define MUI_UNICON "${NSISDIR}\Contrib\Graphics\Icons\modern-uninstall.ico"

; Language Selection Dialog Settings
;!ifndef NOARP
  !define MUI_LANGDLL_REGISTRY_ROOT "${PRODUCT_UNINST_ROOT_KEY}"
  !define MUI_LANGDLL_REGISTRY_KEY "${PRODUCT_UNINST_KEY}"
  !define MUI_LANGDLL_REGISTRY_VALUENAME "NSIS:Language"
;!endif

; Welcome page
!insertmacro MUI_PAGE_WELCOME

; License page
LicenseLangString license ${LANG_ENGLISH} "License.rtf"
!insertmacro MUI_PAGE_LICENSE $(license)

; Directory page
!define MUI_PAGE_CUSTOMFUNCTION_LEAVE "DirectoryLeave"
!insertmacro MUI_PAGE_DIRECTORY

; Start menu page
var ICONS_GROUP
!define MUI_STARTMENUPAGE_NODISABLE
!define MUI_STARTMENUPAGE_DEFAULTFOLDER "${PRODUCT_NAME}"
!define MUI_STARTMENUPAGE_REGISTRY_ROOT "${PRODUCT_UNINST_ROOT_KEY}"
!define MUI_STARTMENUPAGE_REGISTRY_KEY "${PRODUCT_UNINST_KEY}"
!define MUI_STARTMENUPAGE_REGISTRY_VALUENAME "${PRODUCT_STARTMENU_REGVAL}"

Page custom SetCustom ValidateCustom ;Custom page. InstallOptions gets called in SetCustom.

!insertmacro MUI_PAGE_STARTMENU Application $ICONS_GROUP

; Instfiles page
!insertmacro MUI_PAGE_INSTFILES

; Finish page
;!define MUI_FINISHPAGE_RUN "$INSTDIR\DeskAlerts.exe"
;!insertmacro MUI_PAGE_FINISH

; Uninstaller pages
!insertmacro MUI_UNPAGE_INSTFILES

; Language files
!insertmacro MUI_LANGUAGE "English"
;!insertmacro MUI_LANGUAGE "Russian"

; MUI end ------

Function openLinkNewWindow
  Push $3
  Exch
  Push $2
  Exch
  Push $1
  Exch
  Push $0
  Exch
 
  ReadRegStr $0 HKCR "http\shell\open\command" ""
# Get browser path
    DetailPrint $0
  StrCpy $2 '"'
  StrCpy $1 $0 1
  StrCmp $1 $2 +2 # if path is not enclosed in " look for space as final char
    StrCpy $2 ' '
  StrCpy $3 1
  loop:
    StrCpy $1 $0 1 $3
    DetailPrint $1
    StrCmp $1 $2 found
    StrCmp $1 "" found
    IntOp $3 $3 + 1
    Goto loop
 
  found:
    StrCpy $1 $0 $3
    StrCmp $2 " " +2
      StrCpy $1 '$1"'
 
  Pop $0
  Exec '$1 $0'
  Pop $0
  Pop $1
  Pop $2
  Pop $3
FunctionEnd
 
!macro _OpenURL URL
Push "${URL}"
Call openLinkNewWindow
!macroend
 
!define OpenURL '!insertmacro "_OpenURL"'

;--------------------------------
;Add additional information
VIProductVersion "${PRODUCT_VERSION}"
VIAddVersionKey /LANG=${LANG_ENGLISH} "ProductName" "DeskAlerts Client"
VIAddVersionKey /LANG=${LANG_ENGLISH} "CompanyName" "${PRODUCT_PUBLISHER}"
VIAddVersionKey /LANG=${LANG_ENGLISH} "LegalCopyright" "Copyright (c) 2006-2009 Softomate"
VIAddVersionKey /LANG=${LANG_ENGLISH} "FileDescription" "DeskAlerts Client Installer"
VIAddVersionKey /LANG=${LANG_ENGLISH} "FileVersion" "${PRODUCT_VERSION}"
VIAddVersionKey /LANG=${LANG_ENGLISH} "OriginalFilename" "deskalerts_setup.exe"
!ifdef NOSERVER
	VIAddVersionKey /LANG=${LANG_ENGLISH} "Comments" "Trial Version without server"
!else
	!ifdef TRIAL
		VIAddVersionKey /LANG=${LANG_ENGLISH} "Comments" "Trial Version"
	!endif
!endif
;--------------------------------

Name "${PRODUCT_NAME}"
BrandingText "${BRANDING_TEXT}"
OutFile "deskalerts_setup.exe"
InstallDir "$PROGRAMFILES\${PRODUCT_NAME}"
InstallDirRegKey HKLM "${PRODUCT_DIR_REGKEY}" ""
XPStyle on
RequestExecutionLevel user
ShowInstDetails show
ShowUnInstDetails show

!macro InitUAC thing
uac_tryagain:
!insertmacro UAC_RunElevated
${Switch} $0
${Case} 0
	${IfThen} $1 = 1 ${|} Quit ${|} ;we are the outer process, the inner process has done its work, we are done
	${IfThen} $3 <> 0 ${|} ${Break} ${|} ;we are admin, let the show go on
	${If} $1 = 3 ;RunAs completed successfully, but with a non-admin user
		MessageBox mb_YesNo|mb_IconExclamation|mb_TopMost|mb_SetForeground "This ${thing} requires admin privileges, try again" /SD IDNO IDYES uac_tryagain IDNO 0
	${EndIf}
	;fall-through and die
${Case} 1223
	MessageBox mb_IconStop|mb_TopMost|mb_SetForeground "This ${thing} requires admin privileges, aborting!"
	Quit
${Case} 1062
	MessageBox mb_IconStop|mb_TopMost|mb_SetForeground "Logon service not running, aborting!"
	Quit
${Default}
	MessageBox mb_IconStop|mb_TopMost|mb_SetForeground "Unable to elevate , error $0"
	Quit
${EndSwitch}
 
SetShellVarContext all
!macroend

var uid

ReserveFile "test.ini"

Function .onInit
	!insertmacro InitUAC "installer"
	
	${GetParameters} $R0
	${GetOptions} "$R0" "/uid" $uid

	!ifdef IsSilent
		SetSilent silent
	!else
		${GetOptions} "$R0" "/s" $R1
		IfErrors 0 silent
		${GetOptions} "$R0" "-s" $R1
		IfErrors 0 silent
		${GetOptions} "$R0" "/silent" $R1
		IfErrors 0 silent
		${GetOptions} "$R0" "-silent" $R1
		IfErrors 0 silent
			Goto end
		silent:
			SetSilent silent
		end:
	!endif

	InitPluginsDir
	File /oname=$PLUGINSDIR\test.ini "test.ini"
	SetShellVarContext All
FunctionEnd

Function DirectoryLeave
	IfFileExists "$INSTDIR\Uninstall ${PRODUCT_NAME}.lnk" 0 if_not_exist
		;MessageBox MB_ICONINFORMATION|MB_OK "Please uninstall ${PRODUCT_NAME} first."
		Abort
	if_not_exist:
FunctionEnd

Section "MainSection" SEC01
	SetOutPath "$PLUGINSDIR"
	
	File /oname=$PLUGINSDIR\DA_killer.dll "..\version\DA_killer.dll"
	System::Call "DA_killer::CloseClients() i.r0 ? u"
	
	SetOutPath "$INSTDIR"

	ProcKiller::KillProcess "DeskAlerts"
	nsExec::Exec 'sc stop DeskAlerts_Service'
	nsExec::Exec 'sc delete DeskAlerts_Service'
	DeleteRegKey HKEY_CURRENT_USER "Software\ALERTS00001"

	File /r ".\tmp\*.*"

	WriteRegStr HKCU "Software\ALERTS00001\Deskalerts" "UID" "$uid"
	WriteRegStr HKLM "Software\ALERTS00001\Deskalerts" "installed" "1"
	WriteRegDWORD HKLM "Software\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_BROWSER_EMULATION" "deskalerts.exe" 0x2AF9

; Shortcuts
	ReadINIStr $0 "$PLUGINSDIR\test.ini" "Field 2" "State"
	StrCmp $0 1 all_users
		SetShellVarContext Current
		Goto end_users
	all_users:
		SetShellVarContext All
	end_users:

	!insertmacro MUI_STARTMENU_WRITE_BEGIN Application

	CreateDirectory "$SMPROGRAMS\$ICONS_GROUP"
	CreateShortCut "$SMPROGRAMS\$ICONS_GROUP\${PRODUCT_NAME}.lnk" "$INSTDIR\DeskAlerts.exe"
	CreateShortCut "$SMSTARTUP\${PRODUCT_NAME}.lnk" "$INSTDIR\DeskAlerts.exe" "/newid"

!ifndef NODESKICON
	CreateShortCut "$DESKTOP\${PRODUCT_NAME}.lnk" "$INSTDIR\DeskAlerts.exe"
!endif
	!insertmacro MUI_STARTMENU_WRITE_END

	${xml::LoadFile} "$OutDir\conf.xml" $0
	${xml::RootElement} $0 $1
	find:
	${xml::FindNextElement} "PROPERTY" $0 $1
	StrCmp $1 "0" 0 notopenUrl
	${xml::GetAttribute} "id" $0 $1 
	StrCmp $0 "urlAfterInstall" 0 find
	${xml::GetAttribute} "default" $0 $1
	StrCmp "$0" "" notopenUrl
	${OpenURL} $0
	notopenUrl:
	${xml::FindCloseElement}
	${xml::Unload}

	SetShellVarContext all
	CreateDirectory '$APPDATA\DeskAlerts\ALERTS00001'

	StrCpy $2 "(S-1-1-0)" ; Everyone
	StrCpy $3 "FullAccess"
	Push "successful"
	AccessControl::DisableFileInheritance /NOUNLOAD '$APPDATA\DeskAlerts'
	Pop $0
	StrCmp $0 "successful" rights_clear
		Pop $1
		Goto rights_print
	rights_clear:
		Push "successful"
		AccessControl::ClearOnFile /NOUNLOAD '$APPDATA\DeskAlerts' '$2' '$3'
		Pop $0
		StrCmp $0 "successful" rights_print
		Pop $1
	rights_print:
	detailprint "Set rights: $0"

		
	Push "$OutDir\Notification.dll" 
	Call RegisterDotNet
	Pop $0 # return value/error/timeout
	DetailPrint ""
	DetailPrint " Return value: $0"
	DetailPrint ""
	
	
	FileOpen $0 "$APPDATA\DeskAlerts\ALERTS00001\conf.xml" w
	FileWrite $0 '<?xml version="1.0" encoding="utf-8"?>'
	FileWrite $0 '<ALERTS>'
	FileWrite $0 '<SETTINGS>'
	FileWrite $0 '<PROPERTY id="UID" default="'
	FileWrite $0 $uid
	FileWrite $0 '" const="1"/>'
	FileWrite $0 "</SETTINGS>"
	FileWrite $0 "</ALERTS>"
	FileClose $0

	IfSilent not_run_if_silent
		!insertmacro UAC_AsUser_ExecShell "" "$OutDir\deskalerts.exe" "" "" ""
	not_run_if_silent:
	IfFileExists "$OutDir\DeskAlertsService.exe" 0 service_not_exists
		nsExec::Exec '$OutDir\DeskAlertsService.exe -i'
		nsExec::Exec '$OutDir\DeskAlertsService.exe -s'
	service_not_exists:
	
	IfFileExists "$OutDir\DeskAlertsScreenSaver.scr" 0 screensaver_not_exists
		IfFileExists "$LOCALAPPDATA\DeskAlertsScreensaver.scr" 0 +2
			Delete "$LOCALAPPDATA\DeskAlertsScreensaver.scr"

	screensaver_not_exists:

SectionEnd

Function RegisterDotNet

Exch $R0
Push $R1

ReadRegStr $R1 HKEY_LOCAL_MACHINE \
"Software\Microsoft\.NETFramework" "InstallRoot"

IfFileExists $R1\v4.0.30319\regasm.exe FileExists
  MessageBox MB_ICONSTOP|MB_OK "Microsoft .NET Framework 4.0 was not detected!"
Abort

FileExists:

ExecWait '"$R1\v4.0.30319\regasm.exe" "$R0" /silent'

Pop $R1
Pop $R0

FunctionEnd

Section -AdditionalIcons
!ifndef NOUM
	ReadINIStr $0 "$PLUGINSDIR\test.ini" "Field 2" "State"
	StrCmp $0 1 all_users
		SetShellVarContext Current
		Goto end_users
	all_users:
		SetShellVarContext All
	end_users:
	!insertmacro MUI_STARTMENU_WRITE_BEGIN Application
	CreateShortCut "$SMPROGRAMS\$ICONS_GROUP\Uninstall ${PRODUCT_NAME}.lnk " "$INSTDIR\uninst.exe"
	!insertmacro MUI_STARTMENU_WRITE_END
!endif
SectionEnd

Section -Post
	WriteUninstaller "$INSTDIR\uninst.exe"
	;MessageBox MB_ICONINFORMATION|MB_OK "Make uninst"
	WriteRegStr HKLM "${PRODUCT_DIR_REGKEY}" "" "$INSTDIR\DeskAlerts.exe"

!ifndef NOARP
	WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayName" "$(^Name)"
	WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "UninstallString" "$INSTDIR\uninst.exe"
	WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayIcon" "$INSTDIR\DeskAlerts.exe"
	WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayVersion" "${PRODUCT_VERSION}"
	WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "URLInfoAbout" "${PRODUCT_WEB_SITE}"
	WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "Publisher" "${PRODUCT_PUBLISHER}"
!endif
SectionEnd


Function un.onUninstSuccess
	HideWindow
	;MessageBox MB_ICONINFORMATION|MB_OK "$(^Name) was successfuly removed."
FunctionEnd

Function un.onInit
	!insertmacro InitUAC "uninstaller"
	SetShellVarContext current
	!insertmacro MUI_UNGETLANGUAGE
	${un.GetParameters} $R0
	${un.GetOptions} "$R0" "/s" $R1
	IfErrors 0 silent
	${un.GetOptions} "$R0" "-s" $R1
	IfErrors 0 silent
	${un.GetOptions} "$R0" "/silent" $R1
	IfErrors 0 silent
	${un.GetOptions} "$R0" "-silent" $R1
	IfErrors 0 silent
		MessageBox MB_ICONQUESTION|MB_YESNO|MB_DEFBUTTON2 "This will remove the $(^Name) from your computer! Are you sure?" IDYES end
		Abort
	silent:
		SetSilent silent
	end:
FunctionEnd

Function SetCustom
	!insertmacro MUI_HEADER_TEXT "Create shortcuts" "Shortcut/startup options"
	Push $0
		InstallOptions::dialog "$PLUGINSDIR\test.ini"
		Pop $0
	Pop $0
FunctionEnd

Function ValidateCustom
FunctionEnd

Section Uninstall
	Sleep 3000
		
	;"#32770 (Dialog)"
	;FindWindow $0 "DeskAlertsDialog"
	;SendMessage $0 ${WM_CLOSE} 0 0
	System::Call "$INSTDIR\deskalerts.dll::onUninstall() ? u"
	
	SetOutPath "$PLUGINSDIR"
	
	File /oname=$PLUGINSDIR\DA_killer.dll "..\version\DA_killer.dll"	
	System::Call "DA_killer::CloseClients() i.r0 ? u"
	
	SetOutPath "$INSTDIR"

	loop:
		FindWindow $0 "#32770 (Dialog)" "DeskAlertsDialog"
		IntCmp $0 0 done
		IsWindow $0 0 done
		System::Call 'user32::PostMessageA(i,i,i,i) i($0,${WM_CLOSE},0,0)'
		Sleep 100
		Goto loop
	done:

	loop2:
		FindWindow $0 "#32770" "DeskAlertsDialog"
		IntCmp $0 0 done2
		IsWindow $0 0 done2
		System::Call 'user32::PostMessageA(i,i,i,i) i($0,${WM_CLOSE},0,0)'
		Sleep 100
		Goto loop2
	done2:

	ProcKiller::KillProcess "DeskAlerts"

	!insertmacro MUI_STARTMENU_GETFOLDER "Application" $ICONS_GROUP

	;Exec '"regsvr32.exe" /s /u "$INSTDIR\deskalerts.dll"'

	Sleep 100

	SetShellVarContext current
	Delete "$SMPROGRAMS\$ICONS_GROUP\Uninstall ${PRODUCT_NAME}.lnk "
	Delete "$DESKTOP\${PRODUCT_NAME}.lnk"
	Delete "$SMSTARTUP\${PRODUCT_NAME}.lnk"
	Delete "$SMPROGRAMS\$ICONS_GROUP\${PRODUCT_NAME}.lnk"
	RMDir "$SMPROGRAMS\$ICONS_GROUP"

	SetShellVarContext all
	Delete "$SMPROGRAMS\$ICONS_GROUP\Uninstall ${PRODUCT_NAME}.lnk "
	Delete "$DESKTOP\${PRODUCT_NAME}.lnk"
	Delete "$SMSTARTUP\${PRODUCT_NAME}.lnk"
	Delete "$SMPROGRAMS\$ICONS_GROUP\${PRODUCT_NAME}.lnk"
	RMDir "$SMPROGRAMS\$ICONS_GROUP"

	IfFileExists "$INSTDIR\DeskalertsService.exe" service_is_exists
		RMDir /r "$APPDATA\DeskAlerts\ALERTS00001"
	service_is_exists:
	
	IfFileExists "$LOCALAPPDATA\DeskAlertsScreensaver.scr" 0 +2
		Delete "$LOCALAPPDATA\DeskAlertsScreensaver.scr"

	screensaver_not_exists:
	
	RMDir "$APPDATA\DeskAlerts"

	Delete "$INSTDIR\*.html"
	Delete "$INSTDIR\*.htm"
	Delete "$INSTDIR\*.png"
	Delete "$INSTDIR\*.gif"
	Delete "$INSTDIR\*.bmp"
	Delete "$INSTDIR\*.txt"
	Delete "$INSTDIR\*.wav"
	Delete "$INSTDIR\*.dll"
	Delete "$INSTDIR\*.tlb"
	Delete "$INSTDIR\*.exe"
	Delete "$INSTDIR\*.xml"
	Delete "$INSTDIR\*.js"
	Delete "$INSTDIR\*.key"
	Delete "$INSTDIR\*.css"
	Delete "$INSTDIR\*.jpg"
	Delete "$INSTDIR\*.jpeg"
	RMDir /r "$INSTDIR\Cache"

	Sleep 1000

	Delete /REBOOTOK "$INSTDIR\unins.exe"

	RMDir /REBOOTOK $INSTDIR
	DeleteRegKey ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}"
	DeleteRegKey HKLM "${PRODUCT_DIR_REGKEY}"
	SetAutoClose false

	DeleteRegKey HKEY_CURRENT_USER "Software\ALERTS00001"
	DeleteRegKey HKLM "Software\ALERTS00001"
SectionEnd