const msiDoActionStatusNoAction      = 0 '&H0
const msiDoActionStatusSuccess       = 1 '&H1
const msiDoActionStatusUserExit      = 2 '&H2
const msiDoActionStatusFailure       = 3 '&H3
const msiDoActionStatusSuspend       = 4 '&H4
const msiDoActionStatusFinished      = 5 '&H5
const msiDoActionStatusWrongState    = 6 '&H6
const msiDoActionStatusBadActionData = 7 '&H7

Function TranslateSidToName()
	On Error Resume Next
	Err.Clear
	Dim SID : SID = Session.Property(Session.Property("PROPERTY_TO_BE_TRANSLATED"))
	Dim objWMIService : Set objWMIService = GetObject("winmgmts:\\.\root\cimv2")
	Dim objAccount : Set objAccount = objWMIService.Get("Win32_SID.SID='" & SID & "'")
	If Err.Number = 0 Then
		Session.Property(Session.Property("PROPERTY_TO_BE_TRANSLATED")) = objAccount.AccountName
	Else
		Session.Property(Session.Property("PROPERTY_TO_BE_TRANSLATED")) = ""
	End If
	TranslateSidToName = msiDoActionStatusNoAction
End Function
