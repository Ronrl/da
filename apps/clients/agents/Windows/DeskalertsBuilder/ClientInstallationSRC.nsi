!define PRODUCT_NAME "Client/Agent installation builder"
!define PRODUCT_PUBLISHER "Softomate, LLC."
!define PRODUCT_WEB_SITE "http://www.softomate.com"
!define BRANDING_TEXT "DeskAlerts http://www.deskalerts.com"
!define TESTCONNECTPATH "get_xml.asp"

;-------------------------------- Addons Names
!define nameTrial "Trial Version"
!define nameTrialWoSrv "Demo Version"
!define nameADAddon "AD/LDAP module"
!define nameEDAddon "eDirectory module"
!define nameODAddon "Other LDAP module"
!define nameMacVersion "Mac version"
!define nameLinuxVersion "Linux version"
!define nameDecryptAddon "Encryption module"
!define nameComputerNameAddon "Computer Name module"
!define nameScreenSaverAddon "Screen Saver module"
!define nameSyncFreeADAddon "Sync-free AD/LDAP module"
;--------------------------------

!ifdef NOSERVER
	!define TRIAL
	!ifdef DECRYPT
		!undef DECRYPT
	!endif
!else
	!ifndef DECRYPT & TRIAL
		!define DECRYPT
	!endif	
!endif

Var URL
Var BACKURL
Var checkBack
Var decrypt
Var show_decrypt_page
Var FIRSTRUN_URL
Var FIRSTRUN_AD
Var FIRSTRUN_SFAD
Var FIRSTRUN_InstOptions
Var FIRSTRUN_ADV

!define VERDIR "$EXEDIR\data\version"

!define XMLDIR "$EXEDIR\data\blank\conf.xml"
!define WIXDIR "$EXEDIR\data\blank\deskalerts_setup.wxs"
!define BATDIR "$EXEDIR\data\blank\alerts.bat"

!define XMLDIRDEST "${VERDIR}\conf.xml"
!define WIXDIRDEST "${VERDIR}\deskalerts_setup.wxs"
!define BATDIRDEST "${VERDIR}\buildEXE.bat"
!define INIDIRDEST "${VERDIR}\service.ini"

!define REPLASEDURL "http://yoursite.com"
!define REPLASEDBACKURL "http://backyoursite.com"
!define NOSERVERURL "http://basis.deskalerts.com"
!define RUNBATEXE "${BATDIRDEST}"
!define RUNBATMSI "${VERDIR}\buildMSI.bat"
!define RUNBATMAC "${VERDIR}\buildMAC.bat"
!define RUNBATDEB "${VERDIR}\buildDEB.bat"
!define RELEASE "$EXEDIR\release"

!define REGPATH "Software\Softomate\ClientInstallation"
!define SERVERREGPATH "Software\Softomate\ServerInstallation"

!define FILESEXP "*.bmp *.gif *.png *.jpg *.html *.xml *.txt deskalerts.dll deskalerts.exe xcpy.exe *.wav *.js *.css" ;DeskAlertsService.exe - hardcoded

!define DEBDESKICONTO "$EXEDIR\data\deb\usr\share\Softomate\DeskAlerts\desktopIcon.sh"
!define DEBDESKICONFROM "$EXEDIR\data\deb\desktopIcon.sh"

;--------------------------------
;Includes
!include "LogicLib.nsh"
!include "MUI.nsh"
!include "FileFunc.nsh"
!include "StrRep.nsh"
!include "HideItems.nsh"
!include "DisableButtons.nsh"
!include "ReplaceInFile.nsh"
!include "WordFunc.nsh"
!include "controls.nsh"
;--------------------------------
!insertmacro GetFileName
!insertmacro WordFind
!define ReplaceInFile '!insertmacro "ReplaceInFile"'
;--------------------------------
;General
;Name and file
Name "${PRODUCT_NAME}"
Caption "${PRODUCT_NAME}"
SubCaption 3 " "
BrandingText "${BRANDING_TEXT}"
OutFile "ClientInstallation.exe"
XPStyle on
RequestExecutionLevel user

ReserveFile "url.ini"
ReserveFile "thanks.ini"

;Get last url from registry if available
;;InstallDirRegKey HKCU "Software\Client installation" ""
;--------------------------------
;Interface Configuration
!define MUI_HEADERIMAGE
;!define MUI_HEADERIMAGE_BITMAP "deskalerts.bmp" ; optional
!ifdef eightClient
	!define MUI_HEADERIMAGE_BITMAP "deskalerts8.bmp" ; optional
!else
	!ifdef sevenClient
		!define MUI_HEADERIMAGE_BITMAP "deskalerts7.bmp" ; optional
	!else
		!ifdef sixClient
			!define MUI_HEADERIMAGE_BITMAP "deskalerts6.bmp" ; optional
		!else
			!ifdef fiveClient
				!define MUI_HEADERIMAGE_BITMAP "deskalerts5.bmp" ; optional
			!else
				!define MUI_HEADERIMAGE_BITMAP "deskalerts.bmp" ; optional
			!endif
		!endif
	!endif
!endif
!define MUI_ABORTWARNING
!define MUI_HEADERIMAGE_BITMAP_NOSTRETCH
!define MUI_HEADER_TRANSPARENT_TEXT
;--------------------------------
;Interface Init
!insertmacro MUI_LANGUAGE "English"
;--------------------------------

;--------------------------------
;Add additional information
VIProductVersion "${PRODUCT_VERSION}"
VIAddVersionKey /LANG=${LANG_ENGLISH} "ProductName" "Deskalerts Client"
VIAddVersionKey /LANG=${LANG_ENGLISH} "CompanyName" "${PRODUCT_PUBLISHER}"
VIAddVersionKey /LANG=${LANG_ENGLISH} "LegalCopyright" "Copyright (c) 2006-2011 Softomate"
VIAddVersionKey /LANG=${LANG_ENGLISH} "FileVersion" "${PRODUCT_VERSION}"
VIAddVersionKey /LANG=${LANG_ENGLISH} "ProductVersion" "${PRODUCT_VERSION}"
VIAddVersionKey /LANG=${LANG_ENGLISH} "OriginalFilename" "ClientInstallation.exe"
;--------------------------------

!define delim ""
!define COMMENTS "Included standard addons:$\r$\n"
!ifdef isADAddon
	!define TMP1 "${COMMENTS}${delim}${nameADAddon}"
	!undef COMMENTS
	!define COMMENTS "${TMP1}"
	!undef delim
	!define delim ",$\r$\n"
!endif
!ifdef isSyncFreeADAddon
	!define TMP11 "${COMMENTS}${delim}${nameSyncFreeADAddon}"
	!undef COMMENTS
	!define COMMENTS "${TMP11}"
	!undef delim
	!define delim ",$\r$\n"
!endif
!ifdef isEDAddon
	!define TMP2 "${COMMENTS}${delim}${nameEDAddon}"
	!undef COMMENTS
	!define COMMENTS "${TMP2}"
	!undef delim
	!define delim ",$\r$\n"
!endif
!ifdef isODAddon
	!define TMP3 "${COMMENTS}${delim}${nameODAddon}"
	!undef COMMENTS
	!define COMMENTS "${TMP3}"
	!undef delim
	!define delim ",$\r$\n"
!endif
!ifdef withMacVersion
	!define TMP4 "${COMMENTS}${delim}${nameMacVersion}"
	!undef COMMENTS
	!define COMMENTS "${TMP4}"
	!undef delim
	!define delim ",$\r$\n"
!endif
!ifdef withLinuxVersion
	!define TMP5 "${COMMENTS}${delim}${nameLinuxVersion}"
	!undef COMMENTS
	!define COMMENTS "${TMP5}"
	!undef delim
	!define delim ",$\r$\n"
!endif
!ifdef DECRYPT
	!define TMP6 "${COMMENTS}${delim}${nameDecryptAddon}"
	!undef COMMENTS
	!define COMMENTS "${TMP6}"
	!undef delim
	!define delim ",$\r$\n"
!endif
!ifdef isCNAddon
	!define TMP7 "${COMMENTS}${delim}${nameComputerNameAddon}"
	!undef COMMENTS
	!define COMMENTS "${TMP7}"
	!undef delim
	!define delim ",$\r$\n"
!endif
!ifdef isScrsvrAddon
	!define TMP8 "${COMMENTS}${delim}${nameScreenSaverAddon}"
	!undef COMMENTS
	!define COMMENTS "${TMP8}"
	!undef delim
	!define delim ",$\r$\n"
!endif
!ifdef TMP1 | TMP2 | TMP3 | TMP4 | TMP5 | TMP6 | TMP7 | TMP8 | TM11
!else
	!undef COMMENTS
	!define COMMENTS "Without standard modules"
!endif
!ifdef NOSERVER
	!define TMP9 "${nameTrialWoSrv}.$\r$\n${COMMENTS}"
	!undef COMMENTS
	!define COMMENTS "${TMP9}"
!else
	!ifdef TRIAL
		!define TMP9 "${nameTrial}.$\r$\n${COMMENTS}"
		!undef COMMENTS
		!define COMMENTS "${TMP9}"
	!endif
!endif
VIAddVersionKey /LANG=${LANG_ENGLISH} "Comments" "${COMMENTS}"
;VIAddVersionKey /LANG=${LANG_ENGLISH} "FileDescription" "${PRODUCT_NAME} Installer.$\r$\n${COMMENTS}"
VIAddVersionKey /LANG=${LANG_ENGLISH} "FileDescription" "${PRODUCT_NAME} Installer"
;--------------------------------
;Pages

!ifndef NOSERVER
	Page custom SetUrl ValidateUrl
	Page custom SetCertValid ValidateCert
!endif
Page custom SetAD ValidateAD
Page custom SetInstOptions ValidateInstOptions
Page custom SetADV ValidateADV

!ifdef DECRYPT
	Page custom SetDecrypt ValidateDecrypt
!endif
Page custom SetThanks ValidateThanks

;Page instfiles

Section "Components"
	;CreateDirectory "$SMPROGRAMS\$ICONS_GROUP"

	;Get Install Options dialog user input
	ReadINIStr $R0 "$PLUGINSDIR\url.ini" "Field 2" "State"
	DetailPrint "server path is: $R0"
	ReadINIStr $R0 "$PLUGINSDIR\url.ini" "Field 5" "State"
	DetailPrint "is validate: $R0"
	DetailPrint "readed: $1"
SectionEnd

!include "XML.nsh"

Function .onInit

	IfFileExists ${BATDIR} blank_batch_exists
		MessageBox MB_OK|MB_ICONSTOP "Error: the package is corrupted.$\r$\nTry to re-extract and run builder again.$\r$\nIf it does not help please download this package again."
		Abort
blank_batch_exists:

	StrCpy $show_decrypt_page "1"
	StrCpy $FIRSTRUN_URL "1"
	StrCpy $FIRSTRUN_AD "1"
	StrCpy $FIRSTRUN_SFAD "1"
	StrCpy $FIRSTRUN_InstOptions "1"
	StrCpy $FIRSTRUN_ADV "1"

	;Extract InstallOptions files
	;$PLUGINSDIR will automatically be removed when the installer closes
	InitPluginsDir
!ifndef NOSERVER
	File /oname=$PLUGINSDIR\url.ini "url.ini"
	File /oname=$PLUGINSDIR\httpsValidQuestion.ini "httpsValidQuestion.ini"
!endif
	File /oname=$PLUGINSDIR\thanks.ini "thanks.ini"
	File /oname=$PLUGINSDIR\adldap.ini "adldap.ini"
	File /oname=$PLUGINSDIR\instoptions.ini "instoptions.ini"
	File /oname=$PLUGINSDIR\adv.ini "adv.ini"
!ifdef DECRYPT
	File /oname=$PLUGINSDIR\decrypt.ini "decrypt.ini"
!endif

FunctionEnd

Function SetThanks

	!insertmacro MUI_HEADER_TEXT "Thank you!" "Installation builder final step"

!ifndef NOSERVER
	${StrReplace} $0 "//" "/" "$URL/update/"
	${StrReplace} $0 ":/" "://" $0
	WriteINIStr "$PLUGINSDIR\thanks.ini" "Field 4" "State" $0
!else
	${HideItem} "$PLUGINSDIR\thanks.ini" "Field 3"
	${HideItem} "$PLUGINSDIR\thanks.ini" "Field 4"
!endif
	WriteINIStr "$PLUGINSDIR\thanks.ini" "Field 1" "State" "${RELEASE}\"

	Push $R0
	InstallOptions::dialog "$PLUGINSDIR\thanks.ini"
	Pop $R0
	Pop $R0
FunctionEnd

Function ValidateThanks
	ExecShell "open" "${RELEASE}"
FunctionEnd

!ifndef NOSERVER

Function SetUrl

	!insertmacro MUI_HEADER_TEXT "Enter URL to the server" "and Validate DeskAlerts server is configured"

	StrCmp $FIRSTRUN_URL "1" 0 not_first_run_url
		ReadRegStr $0 HKCU "${REGPATH}" "lasturl"
		StrCmp $0 "" 0 url_end
	
		ReadRegStr $0 HKCU "${SERVERREGPATH}" "alerts_dir"
		StrCmp $0 "" 0 url_end

		StrCpy $0 "${REPLASEDURL}"
	url_end:

		WriteINIStr "$PLUGINSDIR\url.ini" "Field 2" "State" $0

		StrCpy $FIRSTRUN_URL "0"
	not_first_run_url:

	Push $R0
	InstallOptions::dialog "$PLUGINSDIR\url.ini"
	Pop $R0
	Pop $R0
FunctionEnd

Function ValidateUrl

	ReadINIStr $R0 "$PLUGINSDIR\url.ini" "Field 5" "State"
	ReadINIStr $checkBack "$PLUGINSDIR\url.ini" "Field 8" "State"
	ReadINIStr $URL "$PLUGINSDIR\url.ini" "Field 2" "State"
	ReadINIStr $BACKURL "$PLUGINSDIR\url.ini" "Field 7" "State"
	
	WriteRegStr HKCU "${REGPATH}" "lasturl" $URL
	StrCmp $R0 0 checkBackup
		GetTempFileName $R0
		${DisableButtons}
		Banner::show /NOUNLOAD /set 76 "${PRODUCT_NAME}" "Validating ... "
		Banner::getWindow /NOUNLOAD
		inetc::get /caption "Validating alert server" /SILENT /nocancel "$URL/${TESTCONNECTPATH}"  $R0 /end		
		Pop $0
		StrCmp $0 "OK" 0 showError
		FileOpen $4 $R0 r
		FileRead $4 $1 8
		StrCmp $1 "<TOOLBAR" checkRes
		FileRead $4 $1 8
		StrCmp $1 "<TOOLBAR" checkRes
		FileRead $4 $1 8
		StrCmp $1 "<TOOLBAR" checkRes
			FileClose $4 ; and close the file
			Banner::destroy
			${EnableButtons}
			MessageBox MB_ICONEXCLAMATION|MB_OK "Sorry, DeskAlerts Server is not present on this address, please try again"
			Abort
		checkRes:
		FileClose $4 ; and close the file
		Banner::destroy
		${EnableButtons}
		Goto checkBackup
		
		showError:
		Banner::destroy
		${EnableButtons}
		MessageBox MB_ICONEXCLAMATION|MB_OK "Validation failed.$\r$\nPossible reasons: server URL is not correct, server database is not setup."
		Abort

	checkBackup:
		${If} $BACKURL == ""
			Goto done
		${Else}
			StrCmp $checkBack 0 done
			GetTempFileName $R0
			${DisableButtons}
			Banner::show /NOUNLOAD /set 76 "${PRODUCT_NAME}" "Validating backup.. "
			Banner::getWindow /NOUNLOAD
			inetc::get /caption "Validating backup alert server" /SILENT /nocancel "$BACKURL/${TESTCONNECTPATH}"  $R0 /end		
			Pop $0
			StrCmp $0 "OK" 0 showError1
			FileOpen $4 $R0 r
			FileRead $4 $1 8
			StrCmp $1 "<TOOLBAR" checkRes1
			FileRead $4 $1 8
			StrCmp $1 "<TOOLBAR" checkRes1
			FileRead $4 $1 8
			StrCmp $1 "<TOOLBAR" checkRes1
				FileClose $4 ; and close the file
				Banner::destroy
				${EnableButtons}
				MessageBox MB_ICONEXCLAMATION|MB_OK "Sorry, Backup DeskAlerts Server is not present on this address, please try again"
				Abort
			checkRes1:
			FileClose $4 ; and close the file
			Banner::destroy
			${EnableButtons}
			Goto done
			
			showError1:
			Banner::destroy
			${EnableButtons}
			MessageBox MB_ICONEXCLAMATION|MB_OK "Backup Validation failed.$\r$\nPossible reasons: server URL is not correct, server database is not setup."
			Abort
		${EndIf} 

	done:
FunctionEnd

Function SetCertValid
	!insertmacro MUI_HEADER_TEXT "Is your certificate valid?" "Ensure your certificate is valid"
 
	ReadINIStr $URL "$PLUGINSDIR\url.ini" "Field 2" "State"
	StrCpy $0 $URL 8
	
	StrCmp $0 "https://" https not_https
	https:
		InstallOptions::dialog "$PLUGINSDIR\httpsValidQuestion.ini"	
	not_https:
FunctionEnd

Function ValidateCert
FunctionEnd

!endif

Function Build
	${DisableButtons}
	Banner::show /NOUNLOAD /set 76 "${PRODUCT_NAME}" "Building..."
	Banner::getWindow /NOUNLOAD

	CopyFiles "${XMLDIR}" "${XMLDIRDEST}"
	CopyFiles "${WIXDIR}" "${WIXDIRDEST}"
	CopyFiles "${BATDIR}" "${BATDIRDEST}"

!ifndef NOSERVER
	${ReplaceInFile} "${XMLDIRDEST}" "${REPLASEDURL}" "$URL"
	${ReplaceInFile} "${XMLDIRDEST}" "${REPLASEDBACKURL}" "$BACKURL"

	; write service ini
	WriteINIStr "${INIDIRDEST}" "LockScreen" "base_url" "$URL"
	WriteINIStr "${INIDIRDEST}" "LockScreen" "backup_base_url" "$BACKURL"
	WriteINIStr "${INIDIRDEST}" "LockScreen" "alert_app_name" "DeskAlerts.exe"


	ReadINIStr $0 "$PLUGINSDIR\adv.ini" "${pullPeriodValueField}" "State"
	ReadINIStr $1 "$PLUGINSDIR\adv.ini" "${pullPeriodUnitField}" "State"
		StrCmp $1 "second(s)" +5 0
		StrCmp $1 "hour(s)" 0 +2
			IntOp $0 $0 * 3600
		StrCmp $1 "minute(s)" 0 +2
			IntOp $0 $0 * 60

	WriteINIStr "${INIDIRDEST}" "LockScreen" "interval" "$0"


!else
	${ReplaceInFile} "${XMLDIRDEST}" "${REPLASEDURL}" "${NOSERVERURL}"
!endif

	ReadINIStr $0 "$PLUGINSDIR\adv.ini" "${pullPeriodValueField}" "State"
	ReadINIStr $1 "$PLUGINSDIR\adv.ini" "${pullPeriodUnitField}" "State"
		StrCmp $1 "minute(s)" +5 0
		StrCmp $1 "hour(s)" 0 +2
			StrCpy $0 "$0h"
		StrCmp $1 "second(s)" 0 +2
			StrCpy $0 "$0s"

	
	${ReplaceInFile} "${XMLDIRDEST}" "%normal_expire%" "$0"
	${ReplaceInFile} "${XMLDIRDEST}" "%standby_expire%" "$0"
	
	ReadINIStr $0 "$PLUGINSDIR\adldap.ini" "Field 13" "State"
	ReadINIStr $1 "$PLUGINSDIR\adldap.ini" "Field 14" "State"
		StrCmp $1 "hour(s)" 0 +2
			StrCpy $0 "$0h"
		StrCmp $1 "minute(s)" 0 +2
			StrCpy $0 "$0m"
	
	${ReplaceInFile} "${XMLDIRDEST}" "%normal_sync_free_expire%" "$0"
	${ReplaceInFile} "${XMLDIRDEST}" "%standby_sync_free_expire%" "$0"
	
	ReadINIStr $0 "$PLUGINSDIR\adv.ini" "${dontShowAnyDateField}" "State"
	StrCmp $0 "1" 0 not_date_mode_0
		${ReplaceInFile} "${XMLDIRDEST}" "%alert_date_mode%" "0"
	not_date_mode_0:
	
	ReadINIStr $0 "$PLUGINSDIR\adv.ini" "${showCreationDateField}" "State"
	StrCmp $0 "1" 0 not_date_mode_1
		${ReplaceInFile} "${XMLDIRDEST}" "%alert_date_mode%" "1"
	not_date_mode_1:
	
	ReadINIStr $0 "$PLUGINSDIR\adv.ini" "${showReceptionDateField}" "State"
	StrCmp $0 "1" 0 not_date_mode_2
		${ReplaceInFile} "${XMLDIRDEST}" "%alert_date_mode%" "2"
	not_date_mode_2:
	
	ReadINIStr $0 "$PLUGINSDIR\adv.ini" "${postponeDisableField}" "State"
	StrCmp $0 "1" 0 not_postpone_mode_0
		${ReplaceInFile} "${XMLDIRDEST}" "%postpone_mode%" "0"
	not_postpone_mode_0:
	
	ReadINIStr $0 "$PLUGINSDIR\adv.ini" "${postponeEnableAllField}" "State"
	StrCmp $0 "1" 0 not_postpone_mode_1
		${ReplaceInFile} "${XMLDIRDEST}" "%postpone_mode%" "1"
	not_postpone_mode_1:
	
	ReadINIStr $0 "$PLUGINSDIR\adv.ini" "${postponeEnableNonUrgentField}" "State"
	StrCmp $0 "1" 0 not_postpone_mode_2
		${ReplaceInFile} "${XMLDIRDEST}" "%postpone_mode%" "2"
	not_postpone_mode_2:
	
	ReadINIStr $1 "$PLUGINSDIR\adv.ini" "${disableTrayIconField}" "State"
	StrCmp "$1" "1" 0 if_not0
		Call HideDeskbarIcon
	if_not0:

	ReadINIStr $1 "$PLUGINSDIR\httpsValidQuestion.ini" "Field 2" "State"
	${ReplaceInFile} "${XMLDIRDEST}" "%https_check_val%" $1

	ReadINIStr $1 "$PLUGINSDIR\adv.ini" "${hideHelpButtonField}" "State"
	StrCmp "$1" "1" 0 if_not1
		${ReplaceInFile} "${XMLDIRDEST}" "<ITEM caption=$\"Help$\" command=$\"help$\" img=$\"10$\"/>" ""
	if_not1:

	ReadINIStr $2 "$PLUGINSDIR\adv.ini" "${hideUnunstallButtonField}" "State"
	StrCmp "$2" "1" 0 if_not2
		${ReplaceInFile} "${XMLDIRDEST}" "<ITEM caption=$\"Uninstall$\" command=$\"uninstall$\" img=$\"5$\"/>" ""
	if_not2:

	StrCmp "$1$2" "11" 0 if_not3
		${ReplaceInFile} "${XMLDIRDEST}" "<SEPARATOR id=$\"up_separator$\"/>" ""
	if_not3:

	ReadINIStr $3 "$PLUGINSDIR\adv.ini" "${hideDisableAlertsButtonField}" "State"
	StrCmp "$3" "1" 0 if_not4
		${ReplaceInFile} "${XMLDIRDEST}" "<ITEM caption=$\"Unobtrusive Mode$\" pressed_caption=$\"Standard mode$\" pressed_img=$\"0$\" img=$\"2$\" command=$\"disable$\"/>" ""
	if_not4:

	ReadINIStr $4 "$PLUGINSDIR\adv.ini" "${hideExitButtonField}" "State"
	StrCmp "$4" "1" 0 if_not5
		${ReplaceInFile} "${XMLDIRDEST}" "<ITEM caption=$\"Exit$\" command=$\"exit$\" img=$\"14$\"/>" ""
	if_not5:

	StrCmp "$3$4" "11" 0 if_not6
		${ReplaceInFile} "${XMLDIRDEST}" "<SEPARATOR id=$\"exit_separator$\"/>" ""
	if_not6:


	ReadINIStr $5 "$PLUGINSDIR\adv.ini" "${hideHistoryButtonField}" "State"
	StrCmp "$5" "1" 0 if_not7
		${ReplaceInFile} "${XMLDIRDEST}" "<ITEM caption=$\"History$\" command=$\"history$\" img=$\"13$\"/>" ""
	if_not7:

	ReadINIStr $6 "$PLUGINSDIR\adv.ini" "${hideOptionsButtonField}" "State"
	StrCmp "$6" "1" 0 if_not8
		${ReplaceInFile} "${XMLDIRDEST}" "<ITEM caption=$\"Options$\" command=$\"option$\" img=$\"11$\"/>" ""
	if_not8:

	StrCmp "$5$6" "11" 0 if_not9
		${ReplaceInFile} "${XMLDIRDEST}" "<SEPARATOR id=$\"up_separator$\"/>" ""
	if_not9:

	StrCmp "$1$2$5$6" "1111" 0 if_not10
		${ReplaceInFile} "${XMLDIRDEST}" "<SEPARATOR id=$\"exit_separator$\"/>" ""
	if_not10:
	
	ReadINIStr $7 "$PLUGINSDIR\adv.ini" "${hideFeedbackButtonField}" "State"
	StrCmp "$7" "1" 0 if_not11
		${ReplaceInFile} "${XMLDIRDEST}" "<ITEM caption=$\"Feedback$\" command=$\"feedback$\" img=$\"16$\"/>" ""
	if_not11:
	

	

	ReadINIStr $0 "$PLUGINSDIR\instoptions.ini" "${httpTextField}" "State"
	ReadINIStr $1 "$PLUGINSDIR\instoptions.ini" "${openURLAfterUpdateField}" "State"
	StrCmp "$1" "1" +2 0
		StrCpy $0 ""

	${ReplaceInFile} "${XMLDIRDEST}" "%server%/after_update.html" "$0"

	
	ReadINIStr $0 "$PLUGINSDIR\instoptions.ini" "${allowAlertsOnLockedScreensField}" "State"
	
;	StrCmp "$0" "1" 0 not_service_enabled
		${ReplaceInFile} "${WIXDIRDEST}" "<!--%SERVICE_COMPONENT_SECTION%-->" "<Component Id='DeskAlertsService' Guid='' DiskId='1'>$\r$\n$\t$\t$\t$\t<File Id='DeskAlertsService' Name='DeskAlertsService.exe' DiskId='1' src='..\version\DeskAlertsService.exe'/>$\r$\n</Component>"
		${ReplaceInFile} "${WIXDIRDEST}" "<!--%SERVICE_REF_SECTION%-->" "<ComponentRef Id='DeskAlertsService'/>"
		${ReplaceInFile} "${WIXDIRDEST}" "<!--%SERVICE_CUSTOM_ACTION%-->" "<CustomAction Id='ExecDAS_stop_cmd' Property='ExecDAS_stop' Value='&quot;sc&quot; stop DeskAlerts_Service' Execute='immediate'/>$\r$\n$\t$\t\
			<CustomAction Id='ExecDAS_delete_cmd' Property='ExecDAS_delete' Value='&quot;sc&quot; delete DeskAlerts_Service' Execute='immediate'/>$\r$\n$\t$\t\
			<CustomAction Id='ExecDAS_install_cmd' Property='ExecDAS_install' Value='&quot;[APPLICATIONROOTDIRECTORY]DeskAlertsService.exe&quot; -i' Execute='immediate'/>$\r$\n$\t$\t\
			<CustomAction Id='ExecDAS_uninstall_cmd' Property='ExecDAS_uninstall' Value='&quot;[APPLICATIONROOTDIRECTORY]DeskAlertsService.exe&quot; -u' Execute='immediate'/>$\r$\n$\t$\t\
			<CustomAction Id='ExecDAS_start_cmd' Property='ExecDAS_start' Value='&quot;[APPLICATIONROOTDIRECTORY]DeskAlertsService.exe&quot; -s' Execute='immediate'/>$\r$\n$\t$\t\
			<CustomAction Id='ExecDAS_stop_on_upgrade_cmd' Property='ExecDAS_stop_on_upgrade' Value='&quot;sc&quot; stop DeskAlerts_Service' Execute='immediate'/>"
		${ReplaceInFile} "${WIXDIRDEST}" "<!--%SERVICE_CUSTOM_QUITE_ACTION%-->" "<CustomAction Id='ExecDAS_stop' BinaryKey='WixCA' DllEntry='CAQuietExec' Execute='deferred' Return='ignore' Impersonate='no'/>$\r$\n$\t$\t\
			<CustomAction Id='ExecDAS_delete' BinaryKey='WixCA' DllEntry='CAQuietExec' Execute='deferred' Return='ignore' Impersonate='no'/>$\r$\n$\t$\t\
			<CustomAction Id='ExecDAS_install' BinaryKey='WixCA' DllEntry='CAQuietExec' Execute='deferred' Return='check' Impersonate='no'/>$\r$\n$\t$\t\
			<CustomAction Id='ExecDAS_start' BinaryKey='WixCA' DllEntry='CAQuietExec' Execute='deferred' Return='check' Impersonate='no'/>$\r$\n$\t$\t\
			<CustomAction Id='ExecDAS_stop_on_upgrade' BinaryKey='WixCA' DllEntry='CAQuietExec' Execute='deferred' Return='ignore' Impersonate='no'/>"
		${ReplaceInFile} "${WIXDIRDEST}" "<!--%SERVICE_EXECUTE_SECTION%-->" "<Custom Action='ExecDAS_stop_cmd' Before='ExecDAS_stop'>NOT REMOVE</Custom>$\r$\n$\t$\t\
				<Custom Action='ExecDAS_stop' Before='ExecDAS_delete_cmd'>NOT REMOVE</Custom>$\r$\n$\t$\t\
				<Custom Action='ExecDAS_delete_cmd' Before='ExecDAS_delete'>NOT REMOVE</Custom>$\r$\n$\t$\t\
				<Custom Action='ExecDAS_delete' Before='InstallFiles'>NOT REMOVE</Custom>$\r$\n$\t$\t\
				<Custom Action='ExecDAS_install_cmd' After='WriteRegistryValues'>NOT REMOVE</Custom>$\r$\n$\t$\t\
				<Custom Action='ExecDAS_install' After='ExecDAS_install_cmd'>NOT REMOVE</Custom>$\r$\n$\t$\t\
				<Custom Action='ExecDAS_start_cmd' After='ExecDAS_install'>NOT REMOVE</Custom>$\r$\n$\t$\t\
				<Custom Action='ExecDAS_start' After='ExecDAS_start_cmd'>NOT REMOVE</Custom>$\r$\n$\t$\t\
				<Custom Action='ExecDAS_stop_on_upgrade_cmd' Before='ExecDAS_stop_on_upgrade'>UPGRADINGPRODUCTCODE</Custom>$\r$\n$\t$\t\
				<Custom Action='ExecDAS_stop_on_upgrade' Before='RemoveRegistryValues'>UPGRADINGPRODUCTCODE</Custom>"
		${ReplaceInFile} "${BATDIRDEST}" "<!--%SERVICE%-->" " DeskAlertsService.exe"
	Goto service_finish
	
	not_service_enabled:
	
		${ReplaceInFile} "${BATDIRDEST}" "<!--%SERVICE%-->" ""
		
	service_finish:
	
		${ReplaceInFile} "${WIXDIRDEST}" "<!--%CLIENT_EXECUTE_STEP%-->" "InstallFinalize"
	
	
	ReadINIStr $1 "$PLUGINSDIR\adldap.ini" "Field 1" "State"
	StrCmp "$1" "1" 0 if_not_equal ;AD/LDAP
		;${ReplaceInFile} "${XMLDIRDEST}" "%KEY%" "<LOCALHTML name='key' href='hg29fhh847c03nu08f.key'/>"
		;${ReplaceInFile} "${XMLDIRDEST}" "%firstURLstart%" "<!--"
		;${ReplaceInFile} "${XMLDIRDEST}" "%firstURLend%" "-->"

		${ReplaceInFile} "${WIXDIRDEST}" "<!--%KEY%-->" "<File Id='hg29fhh847c03nu08f.key' Name='hg29fhh847c03nu08f.key' Source='..\version\hg29fhh847c03nu08f.key' />"
		${ReplaceInFile} "${BATDIRDEST}" "<!--%KEY%-->" " hg29fhh847c03nu08f.key"
		${ReplaceInFile} "${XMLDIRDEST}" "%AD%" "1"

	if_not_equal:
		ReadINIStr $1 "$PLUGINSDIR\adldap.ini" "Field 11" "State"
		StrCmp "$1" "1" 0 if_not_equal2 ;eDirectory
			${ReplaceInFile} "${WIXDIRDEST}" "<!--%KEY%-->" "<File Id='kamewq9nsb8k0o22iv.key' Name='kamewq9nsb8k0o22iv.key' Source='..\version\kamewq9nsb8k0o22iv.key'/>"
			${ReplaceInFile} "${BATDIRDEST}" "<!--%KEY%-->" " kamewq9nsb8k0o22iv.key"
			${ReplaceInFile} "${XMLDIRDEST}" "%SyncFreeAD%" "1"
				
		Goto equal_finish
		if_not_equal2:
			${ReplaceInFile} "${XMLDIRDEST}" "%SyncFreeAD%" "0"
			ReadINIStr $1 "$PLUGINSDIR\adldap.ini" "Field 5" "State"
			StrCmp "$1" "1" 0 if_not_equal3 ;eDirectory
				${ReplaceInFile} "${WIXDIRDEST}" "<!--%KEY%-->" "<File Id='vy0wm1iphva12qer4y.key' Name='vy0wm1iphva12qer4y.key' Source='..\version\vy0wm1iphva12qer4y.key'/>$\r$\n$\t$\t$\t$\t<File Id='eDirectory.dll' Name='eDirectory.dll' Source='..\version\eDirectory.dll' />"
				${ReplaceInFile} "${BATDIRDEST}" "<!--%KEY%-->" " vy0wm1iphva12qer4y.key eDirectory.dll"
				${ReplaceInFile} "${XMLDIRDEST}" "%AD%" "1"
				
			Goto equal_finish
			if_not_equal3:

				ReadINIStr $1 "$PLUGINSDIR\adldap.ini" "Field 8" "State"
				StrCmp "$1" "1" 0 if_not_equal4 ;Other Directory
					${ReplaceInFile} "${WIXDIRDEST}" "<!--%KEY%-->" "<File Id='f8x4c0hwl.key' Name='f8x4c0hwl.key' Source='..\version\f8x4c0hwl.key'/>"
					${ReplaceInFile} "${BATDIRDEST}" "<!--%KEY%-->" " f8x4c0hwl.key"
					${ReplaceInFile} "${XMLDIRDEST}" "%AD%" "1"
					
				Goto equal_finish
				if_not_equal4:

					${ReplaceInFile} "${WIXDIRDEST}" "<!--%KEY%-->" ''
					${ReplaceInFile} "${BATDIRDEST}" "<!--%KEY%-->" ''
					${ReplaceInFile} "${XMLDIRDEST}" "%AD%" "0"

					;deskalerts_setup.wxs
	equal_finish:

  ; Decryption add-on
!ifdef DECRYPT
	ReadINIStr $0           "$PLUGINSDIR\decrypt.ini" "Field 1" "State"
	ReadINIStr $1           "$PLUGINSDIR\decrypt.ini" "Field 2" "State"

	StrCmp "$1" "1" 0 if_not_dis
		${ReplaceInFile} "${XMLDIRDEST}" "%DECRYPT%"     "0"
		${ReplaceInFile} "${XMLDIRDEST}" "%DECRYPT_KEY%" ""

		Goto decrypt_finish
	if_not_dis:
		${ReplaceInFile} "${XMLDIRDEST}" '%DECRYPT%'     "1"
		${ReplaceInFile} "${XMLDIRDEST}" '%DECRYPT_KEY%' $0
	decrypt_finish:
!else
	${ReplaceInFile} "${XMLDIRDEST}" "<PROPERTY id=$\"decryption$\" default=$\"%DECRYPT%$\" const=$\"1$\"/>" ""
	${ReplaceInFile} "${XMLDIRDEST}" "<PROPERTY id=$\"decryption_key$\" default=$\"%DECRYPT_KEY%$\" const=$\"1$\"/>" ""
!endif

	StrCpy $0 ""
	ReadINIStr $1 "$PLUGINSDIR\instoptions.ini" "${hideUninstallFromStartMenuField}" "State"
	StrCmp "$1" "1" 0 if_unmenu
		StrCpy $0 "/DNOUM"
		${ReplaceInFile} "${WIXDIRDEST}" " Title=$\"Uninstall Shortcut$\" Level=$\"1$\"" " Title=$\"Uninstall Shortcut$\" Level=$\"0$\""
	if_unmenu:

	${ReplaceInFile} "${BATDIRDEST}" "%NoUninstallMenu%" "$0"

	StrCpy $0 "/DNOARP"
	ReadINIStr $1 "$PLUGINSDIR\instoptions.ini" "${hideUninstallFromAddRemove}" "State"
	StrCmp "$1" "1" if_not_unprog 0
		StrCpy $0 ""
		${ReplaceInFile} "${WIXDIRDEST}" "<Property Id=$\"ARPSYSTEMCOMPONENT$\" Value=$\"1$\"/>" ""
	if_not_unprog:

	${ReplaceInFile} "${BATDIRDEST}" "%NoAddRemProgs%" "$0"

	StrCpy $0 ""
	ReadINIStr $1 "$PLUGINSDIR\instoptions.ini" "${createDesktopShortcut}" "State"
	StrCmp "$1" "0" 0 if_deskicon
		StrCpy $0 "/DNODESKICON"
		Delete "${DEBDESKICONTO}"
		${ReplaceInFile} "${WIXDIRDEST}" " Title=$\"Desktop Shortcut$\" Level=$\"1$\"" " Title=$\"Desktop Shortcut$\" Level=$\"0$\""
		Goto deskicon_end
	if_deskicon:
		CopyFiles "${DEBDESKICONFROM}" "${DEBDESKICONTO}"
	deskicon_end:

	${ReplaceInFile} "${BATDIRDEST}" "%NoDeskIcon%" "$0"


	;ReadINIStr $0 "$PLUGINSDIR\instoptions.ini" "${enableAutoUpdateField}" "State"
	${ReplaceInFile} "${XMLDIRDEST}" "%autoupdate%" "$1"
	
	;ReadINIStr $0 "$PLUGINSDIR\instoptions.ini" "${enableSilentAutoUpdateField}" "State"
	;${ReplaceInFile} "${XMLDIRDEST}" "%silent_update%" "$0"


	${ReplaceInFile} "${BATDIRDEST}" "<!--%FILES%-->" "${FILESEXP}"

	${ReplaceInFile} "${WIXDIRDEST}" "%version%" "${PRODUCT_VERSION}"
	${ReplaceInFile} "${XMLDIRDEST}" "%version%" "${PRODUCT_VERSION}"
	${ReplaceInFile} "${BATDIRDEST}" "%version%" "/DPRODUCT_VERSION=${PRODUCT_VERSION}"
	
	StrCpy $9 ""
!ifdef TRIAL
	StrCpy $9 " Trial Version"
!endif
!ifdef NOSERVER
	StrCpy $9 " Demo Version"
!endif

	!ifdef NOSERVER
			${ReplaceInFile} "${BATDIRDEST}" "%opt%" "/DNOSERVER"
	!else
		!ifdef TRIAL
			${ReplaceInFile} "${BATDIRDEST}" "%opt%" "/DTRIAL"
		!else
			${ReplaceInFile} "${BATDIRDEST}" "%opt%" ""
		!endif
	!endif

	StrCpy $8 "DeskAlerts$9"

	${xml::LoadFile} "${XMLDIRDEST}" $0
	StrCmp $0 0 0 defname
	${xml::RootElement} $0 $1
	StrCmp $1 0 0 defname
	${xml::GetAttribute} "name" $0 $1
	StrCmp $1 0 0 defname
	StrCmp "$0" "" defname
		DetailPrint "set product name to $0"
		StrCpy $8 "$0$9"
	defname:
	${xml::Unload}

	${ReplaceInFile} "${BATDIRDEST}" "%name%" "/DPRODUCT_NAME=$8"
	${ReplaceInFile} "${WIXDIRDEST}" "%name%" "$8"
	
	ReadINIStr $1 "$PLUGINSDIR\adldap.ini" "Field 9" "State"
	StrCmp "$1" "1" 0 if_not_equal_cn
		${ReplaceInFile} "${XMLDIRDEST}" "<!-- register_mode_block_begin -->" "<!--"
		${ReplaceInFile} "${XMLDIRDEST}" "<!-- register_mode_block_end -->" "-->"
		${ReplaceInFile} "${XMLDIRDEST}" "<!-- computer_name_block_begin" ""
		${ReplaceInFile} "${XMLDIRDEST}" "$\tcomputer_name_block_end -->" ""
		Goto if_cn_end
	if_not_equal_cn:
		${ReplaceInFile} "${XMLDIRDEST}" "<!-- register_mode_block_begin -->" ""
		${ReplaceInFile} "${XMLDIRDEST}" "<!-- register_mode_block_end -->" ""
		${ReplaceInFile} "${XMLDIRDEST}" "<!-- computer_name_block_begin" "<!--"
		${ReplaceInFile} "${XMLDIRDEST}" "$\tcomputer_name_block_end -->" "-->"
	if_cn_end:
	
	ReadINIStr $1 "$PLUGINSDIR\adldap.ini" "Field 10" "State"
	StrCmp "$1" "1" 0 if_not_equal_scrsrv
		${ReplaceInFile} "${XMLDIRDEST}" "%SCRSVR%" "1"
		${ReplaceInFile} "${WIXDIRDEST}" "<!--%SCREENSAVER_COMPONENT_SECTION%-->" 	"<DirectoryRef Id='WinDir'>$\r$\n$\t$\t\
																						<Component Id='Screensaver' Guid='D5D3E9A8-0E78-4728-B5AB-2592C5ADF079'>$\r$\n$\t$\t\
																							<File Id='DeskAlertsScreensaver.scr' Name='DeskAlertsScreensaver.scr' Source='..\version\DeskAlertsScreensaver.scr'/>$\r$\n\
																						</Component>$\r$\n\	
																					</DirectoryRef>"
		${ReplaceInFile} "${WIXDIRDEST}" "<!--%SCREENSAVER_REF_SECTION%-->" "<ComponentRef Id='Screensaver'/>"
		${ReplaceInFile} "${BATDIRDEST}" "<!--%SCREENSAVER%-->" " DeskAlertsScreenSaver.scr"
		
	Goto if_scrsrv_end
	if_not_equal_scrsrv:
		${ReplaceInFile} "${XMLDIRDEST}" "%SCRSVR%" "0"
		${ReplaceInFile} "${WIXDIRDEST}" "<!--%SCREENSAVER_COMPONENT_SECTION%-->" ""
		${ReplaceInFile} "${WIXDIRDEST}" "<!--%SCREENSAVER_REF_SECTION%-->" ""
		${ReplaceInFile} "${BATDIRDEST}" "<!--%SCREENSAVER%-->" ""
	if_scrsrv_end:

;--------------------------------
; Generate file list
;--------------------------------
	ClearErrors
	FileOpen $3 ${WIXDIRDEST} "r"

	GetTempFileName $4
	FileOpen $5 $4 "w"
	before_loop:
		FileRead $3 $6
		IfErrors leave
		${WordFind} $6 "<!--%FILES%-->" "E+1" $7
		IfErrors 0 found
		ClearErrors
		FileWrite $5 "$6"
	Goto before_loop

	found:
	ClearErrors
	;SetOutPath "${VERDIR}"
	StrCpy $6 "1"
	findwords_loop:
		${WordFind} "${FILESEXP}" " " "E+$6" $7
		IfErrors findwords_done
		FindFirst $0 $1 "${VERDIR}\$7"
		files_loop:
			StrCmp $1 "" files_done
			md5dll::GetMD5String "$1"
			Pop $2
			FileWrite $5 "$\r$\n$\t$\t$\t$\t<File Id='file_$2' Name='$1' Source='..\version\$1'/>"
			FindNext $0 $1
			Goto files_loop
		files_done:
		FindClose $0
		IntOp $6 $6 + 1
		Goto findwords_loop
	findwords_done:

	ClearErrors
	afterloop:
		FileRead $3 $6
		IfErrors leave
		FileWrite $5 "$6"
	Goto afterloop
	leave:
	FileClose $3
	FileClose $5
	Delete "${WIXDIRDEST}"
	Rename "$4" "${WIXDIRDEST}"
;--------------------------------

	;SetOutPath "${$INSTDIR}"

	Delete "${XMLDIRDEST}.old"
	Delete "${WIXDIRDEST}.old"
	Delete "${BATDIRDEST}.old"

	;nsExec::ExecToStack '"${RUNBATEXE}"'
	Pop $0
	Pop $1

	;IfFileExists "${RELEASE}\deskalert.cab" exe_installer_exists
	;	MessageBox MB_ICONSTOP|MB_OK "Installers building was failed.$\r$\n$\r$\nPossible reasons:$\r$\n$\r$\n1) You have no access rights to folder: ${RELEASE}$\r$\n$\r$\n2) This builder are corrupted. Try to re-extract and run builder again. If it does not help please download this package again."
	;	Banner::destroy
	;	${EnableButtons}
	;	Quit
	;	Abort
	exe_installer_exists:

	ReadINIStr $1 "$PLUGINSDIR\adldap.ini" "Field 4" "State"
	StrCmp "$1" "1" 0 if_not_equal_msi
		nsExec::ExecToStack '"${RUNBATMSI}"'
		Pop $0
		Pop $1
	if_not_equal_msi:

	ReadINIStr $0 "$PLUGINSDIR\adldap.ini" "Field 6" "State"
	StrCmp "$0" "1" 0 if_not_equal_mac
		nsExec::ExecToStack '"${RUNBATMAC}"'
		Pop $2
		Pop $3
		WriteINIStr "$PLUGINSDIR\thanks.ini" "Field 3" "Text" "For updates do not forget to upload deskalert.cab, deskalert.zip and version.txt to the folder:"
	if_not_equal_mac:
     
	ReadINIStr $1 "$PLUGINSDIR\adldap.ini" "Field 7" "State"
	StrCmp "$1" "1" 0 if_not_equal_deb
		nsExec::ExecToStack '"${RUNBATDEB}"'
		Pop $2
		Pop $3
		StrCmp "$0" "1" 0 if_not_equal_debmac
			WriteINIStr "$PLUGINSDIR\thanks.ini" "Field 3" "Text" "For updates do not forget to upload deskalert.cab, deskalert.zip, deskalert.tar.gz and version.txt to the folder:"
			Goto if_not_equal_deb
		if_not_equal_debmac:
			WriteINIStr "$PLUGINSDIR\thanks.ini" "Field 3" "Text" "For updates do not forget to upload deskalert.cab, deskalert.tar.gz and version.txt to the folder:"
	if_not_equal_deb:
	
	Banner::destroy
	${EnableButtons}
FunctionEnd

Function HideDeskbarIcon
	FileOpen $3 ${XMLDIRDEST} "r"

	GetTempFileName $4
	FileOpen $5 $4 "w"

	before_loop:
		ClearErrors
		FileRead $3 $6
		IfErrors leave
		FileWrite $5 "$6"
		${WordFind} $6 "<VIEWS" "E+1" $7
		IfErrors 0 open_sysmenu_loop
	Goto before_loop

	open_sysmenu_loop:
		ClearErrors
		FileRead $3 $6
		IfErrors leave
		${WordFind} $6 "<SYSMENU" "E+1" $7
		IfErrors 0 found_loop
		FileWrite $5 "$6"
		${WordFind} $6 "</VIEWS>" "E+1" $7
		IfErrors 0 afterloop
	Goto open_sysmenu_loop

	found_loop:
		FileRead $3 $6
		IfErrors leave
		${WordFind} $6 "</SYSMENU>" "E+1" $7
		IfErrors 0 afterloop
	Goto found_loop

	afterloop:
		FileRead $3 $6
		IfErrors leave
		FileWrite $5 "$6"
	Goto afterloop

	leave:
	FileClose $3
	FileClose $5
	Delete "${XMLDIRDEST}"
	Rename "$4" "${XMLDIRDEST}"
FunctionEnd

Function SetAD
	!insertmacro MUI_HEADER_TEXT "Modules and platforms settings" "If some options are disabled that means your license does not$\n$\rinclude them and you need to contact sales@deskalerts.com"	

	StrCmp $FIRSTRUN_AD "1" 0 not_first_run_ad
	StrCmp $FIRSTRUN_SFAD "1" 0 not_first_run_ad
!ifndef NOSERVER

		StrCpy $2 0
		ReadINIStr $0 "$PLUGINSDIR\url.ini" "Field 5" "State"

		
		IfFileExists "${VERDIR}\hg29fhh847c03nu08f.key" exist_ad 0
			WriteINIStr "$PLUGINSDIR\adldap.ini" "Field 1" "Flags" "DISABLED"
			Goto not_exist_ad
		exist_ad:
			StrCmp $0 "1" 0 not_exist_ad ;disabled url validation
			StrCmp $2 "1" not_exist_ad 0 ;if ED found - don't check AD
			GetTempFileName $1
			inetc::get /caption "Validating Active Directory" /SILENT /nocancel "$URL/admin/modules.asp?mod=ad" $1 /end
			FileOpen $4 $1 r
			FileRead $4 $2 1
			FileClose $4 ; and close the file
			StrCmp $2 "1" 0 not_exist_ad
				WriteINIStr "$PLUGINSDIR\adldap.ini" "Field 1" "State" "1"
		not_exist_ad:
		
		IfFileExists "${VERDIR}\kamewq9nsb8k0o22iv.key" exist_sfad 0
			WriteINIStr "$PLUGINSDIR\adldap.ini" "Field 11" "Flags" "DISABLED"
		exist_sfad:
			StrCmp $0 "1" 0 not_exist_sfad ;disabled url validation
			StrCmp $2 "1" not_exist_sfad 0 ;if ED found - don't check AD
			GetTempFileName $1
			inetc::get /caption "Validating Sync-Free Active Directory" /SILENT /nocancel "$URL/admin/modules.asp?mod=sfad" $1 /end
			FileOpen $4 $1 r
			FileRead $4 $2 1
			FileClose $4 ; and close the file
			StrCmp $2 "1" 0 not_exist_sfad
				WriteINIStr "$PLUGINSDIR\adldap.ini" "Field 11" "State" "1"
				ReadINIStr $2 "$PLUGINSDIR\adldap.ini" "Field 13" "HWND"
				EnableWindow $2 "1"
				ReadINIStr $2 "$PLUGINSDIR\adldap.ini" "Field 14" "HWND"
				EnableWindow $2 "1"
				ReadRegStr $0 HKCU "${REGPATH}" "syncfreeperiod"		
				StrCmp $0 "" +2 0
					WriteINIStr "$PLUGINSDIR\adldap.ini" "Field 13" "State" $0

				ReadRegStr $0 HKCU "${REGPATH}" "syncfreeunit"
				StrCmp $0 "" +2 0
					WriteINIStr "$PLUGINSDIR\adldap.ini" "Field 14" "State" $0
		not_exist_sfad:		
		
		IfFileExists "${VERDIR}\vy0wm1iphva12qer4y.key" exist_ed 0
			WriteINIStr "$PLUGINSDIR\adldap.ini" "Field 5" "Flags" "DISABLED"
		exist_ed:
			StrCmp $0 "1" 0 not_exist_ed ;disabled url validation
				GetTempFileName $1
				inetc::get /caption "Validating eDirectory" /SILENT /nocancel "$URL/admin/modules.asp?mod=ed" $1 /end
				FileOpen $4 $1 r
				FileRead $4 $2 1
				FileClose $4 ; and close the file
				StrCmp $2 "1" 0 not_exist_ed
					WriteINIStr "$PLUGINSDIR\adldap.ini" "Field 5" "State" "1"
		not_exist_ed:

		IfFileExists "${VERDIR}\f8x4c0hwl.key" exist_od 0
			WriteINIStr "$PLUGINSDIR\adldap.ini" "Field 8" "Flags" "DISABLED"
		exist_od:
			StrCmp $0 "1" 0 not_exist_od ;disabled url validation
				GetTempFileName $1
				inetc::get /caption "Validating Other Directory" /SILENT /nocancel "$URL/admin/modules.asp?mod=od" $1 /end
				FileOpen $4 $1 r
				FileRead $4 $2 1
				FileClose $4 ; and close the file
				StrCmp $2 "1" 0 not_exist_od
					WriteINIStr "$PLUGINSDIR\adldap.ini" "Field 8" "State" "1"
		not_exist_od:
!else
		WriteINIStr "$PLUGINSDIR\adldap.ini" "Field 1" "Flags" "DISABLED"
		WriteINIStr "$PLUGINSDIR\adldap.ini" "Field 5" "Flags" "DISABLED"
		WriteINIStr "$PLUGINSDIR\adldap.ini" "Field 8" "Flags" "DISABLED"
		WriteINIStr "$PLUGINSDIR\adldap.ini" "Field 11" "Flags" "DISABLED"
!endif
		ReadRegStr $0 HKCU "${REGPATH}" "msi"
		StrCmp $0 "" +2 0
			WriteINIStr "$PLUGINSDIR\adldap.ini" "Field 4" "State" $0
			
		!ifdef isCNAddon
			WriteINIStr "$PLUGINSDIR\adldap.ini" "Field 9" "State" "1"
			ReadRegStr $0 HKCU "${REGPATH}" "cn_addon"
			StrCmp $0 "" +2 0
				WriteINIStr "$PLUGINSDIR\adldap.ini" "Field 9" "State" $0
		!else
			WriteINIStr "$PLUGINSDIR\adldap.ini" "Field 9" "Flags" "DISABLED"
		!endif
		
		!ifdef isScrsvrAddon
			WriteINIStr "$PLUGINSDIR\adldap.ini" "Field 10" "State" "1"
			ReadRegStr $0 HKCU "${REGPATH}" "scrsrv_addon"
			StrCmp $0 "" +2 0
				WriteINIStr "$PLUGINSDIR\adldap.ini" "Field 10" "State" $0
		!else
			WriteINIStr "$PLUGINSDIR\adldap.ini" "Field 10" "Flags" "DISABLED"
		!endif
		
	
		IfFileExists "${RUNBATMAC}" exist_mac 0
			WriteINIStr "$PLUGINSDIR\adldap.ini" "Field 6" "Flags" "DISABLED"
			Goto not_exist_mac
		exist_mac:
			ReadRegStr $0 HKCU "${REGPATH}" "mac"
			StrCmp $0 "" +2 0
				WriteINIStr "$PLUGINSDIR\adldap.ini" "Field 6" "State" "1"
		not_exist_mac:

		IfFileExists "${RUNBATDEB}" exist_deb 0
			WriteINIStr "$PLUGINSDIR\adldap.ini" "Field 7" "Flags" "DISABLED"
			Goto not_exist_deb
		exist_deb:
			ReadRegStr $0 HKCU "${REGPATH}" "deb"
			StrCmp $0 "" +2 0
				WriteINIStr "$PLUGINSDIR\adldap.ini" "Field 7" "State" "1"
		not_exist_deb:

		StrCpy $FIRSTRUN_AD "0"
	not_first_run_ad:
		
	Push $R0
	InstallOptions::dialog "$PLUGINSDIR\adldap.ini"
	Pop $R0
	Pop $R0
FunctionEnd

Function ValidateAD
		
	ReadINIStr $0 "$PLUGINSDIR\adldap.ini" "Field 13" "State"
	IntCmp $0 0 0 +3 +3
		MessageBox MB_OK "Synchronization request frequency value cannot be zero"
		Abort
	WriteRegStr HKCU "${REGPATH}" "syncfreeperiod" $0
	
	ReadINIStr $0 "$PLUGINSDIR\adldap.ini" "Field 14" "State"
	WriteRegStr HKCU "${REGPATH}" "syncfreeunit" $0
		
	ReadINIStr $0 "$PLUGINSDIR\adldap.ini" "Settings" "State"
	
	IntCmp $0 0 if_next 0 0 ;if $0 = 0 was clicked next button otherwise it was notify
	
		IntCmp $0 1 0 +4 +4 ;AD/LDAP
			ReadINIStr $1 "$PLUGINSDIR\adldap.ini" "Field 5" "HWND"
			ReadINIStr $2 "$PLUGINSDIR\adldap.ini" "Field 8" "HWND"
			StrCpy $3 ""
			
		IntCmp $0 5 0 +4 +4 ;eDirectory
			ReadINIStr $1 "$PLUGINSDIR\adldap.ini" "Field 1" "HWND"
			ReadINIStr $2 "$PLUGINSDIR\adldap.ini" "Field 8" "HWND"
			ReadINIStr $3 "$PLUGINSDIR\adldap.ini" "Field 11" "HWND"
		
		IntCmp $0 8 0 +4 +4 ;Other Directory
			ReadINIStr $1 "$PLUGINSDIR\adldap.ini" "Field 1" "HWND"
			ReadINIStr $2 "$PLUGINSDIR\adldap.ini" "Field 5" "HWND"
			ReadINIStr $3 "$PLUGINSDIR\adldap.ini" "Field 11" "HWND"
			
		IntCmp $0 11 0 +4 +4 ;Sync-free
			ReadINIStr $1 "$PLUGINSDIR\adldap.ini" "Field 5" "HWND"
			ReadINIStr $2 "$PLUGINSDIR\adldap.ini" "Field 8" "HWND"
			StrCpy $3 ""
			
		SendMessage $1 ${BM_SETCHECK} 0 0
		SendMessage $2 ${BM_SETCHECK} 0 0
		SendMessage $3 ${BM_SETCHECK} 0 0
		
        ;enable or disable syncFree base on "Use Windows Active Directory" state
        ReadINIStr $0 "$PLUGINSDIR\adldap.ini" "Field 1" "State" 
        ReadINIStr $1 "$PLUGINSDIR\adldap.ini" "Field 11" "HWND"
        
        StrCmp $0 "0" setSyncFreeUnchecked not_setSyncFreeUnchecked
        setSyncFreeUnchecked:
        SendMessage $1 ${BM_SETCHECK} ${BST_UNCHECKED} 0
        WriteINIStr "$PLUGINSDIR\adldap.ini" "Field 11" "State" "0"
        not_setSyncFreeUnchecked:
        EnableWindow $1 $0
        
		ReadINIStr $1 "$PLUGINSDIR\adldap.ini" "Field 11" "State"
		ReadINIStr $2 "$PLUGINSDIR\adldap.ini" "Field 13" "HWND"
		EnableWindow $2 $1
		ReadINIStr $2 "$PLUGINSDIR\adldap.ini" "Field 14" "HWND"
		EnableWindow $2 $1
        
		Abort
	if_next:

	ReadINIStr $0 "$PLUGINSDIR\adldap.ini" "Field 4" "State"
	WriteRegStr HKCU "${REGPATH}" "msi" $0

	ReadINIStr $0 "$PLUGINSDIR\adldap.ini" "Field 6" "State"
	WriteRegStr HKCU "${REGPATH}" "mac" $0

	ReadINIStr $0 "$PLUGINSDIR\adldap.ini" "Field 7" "State"
	WriteRegStr HKCU "${REGPATH}" "deb" $0	
	
	ReadINIStr $0 "$PLUGINSDIR\adldap.ini" "Field 9" "State"
	WriteRegStr HKCU "${REGPATH}" "cn_addon" $0	
	
	ReadINIStr $0 "$PLUGINSDIR\adldap.ini" "Field 10" "State"
	WriteRegStr HKCU "${REGPATH}" "scrsrv_addon" $0	
FunctionEnd

Function SetADV
	
	!insertmacro MUI_HEADER_TEXT "Client interface options" "If some options are disabled that means your license does not$\n$\rinclude them and you need to contact sales@deskalerts.com"
	
	
	
	StrCmp $FIRSTRUN_ADV "1" 0 not_first_run_adv	
		ReadRegStr $0 HKCU "${REGPATH}" "pullvalue"
		StrCmp $0 "" +2 0
			WriteINIStr "$PLUGINSDIR\adv.ini" "${pullPeriodValueField}" "State" $0

		ReadRegStr $0 HKCU "${REGPATH}" "pullunit"
		StrCmp $0 "" +2 0
			WriteINIStr "$PLUGINSDIR\adv.ini" "${pullPeriodUnitField}" "State" $0

		ReadRegStr $0 HKCU "${REGPATH}" "help"
		StrCmp $0 "" +2 0
			WriteINIStr "$PLUGINSDIR\adv.ini" "${hideHelpButtonField}" "State" $0
			
			ReadRegStr $0 HKCU "${REGPATH}" "feedback"
		StrCmp $0 "" +2 0
			WriteINIStr "$PLUGINSDIR\adv.ini" "${hideFeedbackButtonField}" "State" $0

		ReadRegStr $0 HKCU "${REGPATH}" "uninstall"
		StrCmp $0 "" +2 0
			WriteINIStr "$PLUGINSDIR\adv.ini" "${hideUnunstallButtonField}" "State" $0

		ReadRegStr $0 HKCU "${REGPATH}" "disable"
		StrCmp $0 "" +2 0
			WriteINIStr "$PLUGINSDIR\adv.ini" "${hideDisableAlertsButtonField}" "State" $0

		ReadRegStr $0 HKCU "${REGPATH}" "exit"
		StrCmp $0 "" +2 0
			WriteINIStr "$PLUGINSDIR\adv.ini" "${hideExitButtonField}" "State" $0

		ReadRegStr $0 HKCU "${REGPATH}" "history"
		StrCmp $0 "" +2 0
			WriteINIStr "$PLUGINSDIR\adv.ini" "${hideHistoryButtonField}" "State" $0

		ReadRegStr $0 HKCU "${REGPATH}" "options"
		StrCmp $0 "" +2 0
			WriteINIStr "$PLUGINSDIR\adv.ini" "${hideOptionsButtonField}" "State" $0
		
		ReadRegStr $0 HKCU "${REGPATH}" "datemode"
		StrCmp $0 "0" 0 +4
			WriteINIStr "$PLUGINSDIR\adv.ini" "${dontShowAnyDateField}" "State" 1
			WriteINIStr "$PLUGINSDIR\adv.ini" "${showCreationDateField}" "State" 0
			WriteINIStr "$PLUGINSDIR\adv.ini" "${showReceptionDateField}" "State" 0

		StrCmp $0 "1" 0 +4
			WriteINIStr "$PLUGINSDIR\adv.ini" "${dontShowAnyDateField}" "State" 0
			WriteINIStr "$PLUGINSDIR\adv.ini" "${showCreationDateField}" "State" 1
			WriteINIStr "$PLUGINSDIR\adv.ini" "${showReceptionDateField}" "State" 0
		StrCmp $0 "2" 0 +4
			WriteINIStr "$PLUGINSDIR\adv.ini" "${dontShowAnyDateField}" "State" 0
			WriteINIStr "$PLUGINSDIR\adv.ini" "${showCreationDateField}" "State" 0
			WriteINIStr "$PLUGINSDIR\adv.ini" "${showReceptionDateField}" "State" 1
			
		ReadRegStr $0 HKCU "${REGPATH}" "postponemode"
		StrCmp $0 "0" 0 +4
			WriteINIStr "$PLUGINSDIR\adv.ini" "${postponeDisableField}" "State" 1
			WriteINIStr "$PLUGINSDIR\adv.ini" "${postponeEnableAllField}" "State" 0
			WriteINIStr "$PLUGINSDIR\adv.ini" "${postponeEnableNonUrgentField}" "State" 0

		StrCmp $0 "1" 0 +4
			WriteINIStr "$PLUGINSDIR\adv.ini" "${postponeDisableField}" "State" 0
			WriteINIStr "$PLUGINSDIR\adv.ini" "${postponeEnableAllField}" "State" 1
			WriteINIStr "$PLUGINSDIR\adv.ini" "${postponeEnableNonUrgentField}" "State" 0
		StrCmp $0 "2" 0 +4
			WriteINIStr "$PLUGINSDIR\adv.ini" "${postponeDisableField}" "State" 0
			WriteINIStr "$PLUGINSDIR\adv.ini" "${postponeEnableAllField}" "State" 0
			WriteINIStr "$PLUGINSDIR\adv.ini" "${postponeEnableNonUrgentField}" "State" 1
			
		ReadRegStr $0 HKCU "${REGPATH}" "noicon"
		StrCmp $0 "" +2 0
			WriteINIStr "$PLUGINSDIR\adv.ini" "${disableTrayIconField}" "State" $0
			
		StrCmp $0 "1" 0 withicon
			WriteINIStr "$PLUGINSDIR\adv.ini" "${hideHelpButtonField}" "Flags" "DISABLED" 
			WriteINIStr "$PLUGINSDIR\adv.ini" "${hideFeedbackButtonField}" "Flags" "DISABLED" 
			WriteINIStr "$PLUGINSDIR\adv.ini" "${hideUnunstallButtonField}" "Flags" "DISABLED"
			WriteINIStr "$PLUGINSDIR\adv.ini" "${hideDisableAlertsButtonField}" "Flags" "DISABLED"
			WriteINIStr "$PLUGINSDIR\adv.ini" "${hideExitButtonField}" "Flags" "DISABLED"
			WriteINIStr "$PLUGINSDIR\adv.ini" "${hideHistoryButtonField}" "Flags" "DISABLED"
			WriteINIStr "$PLUGINSDIR\adv.ini" "${hideOptionsButtonField}" "Flags" "DISABLED"
			
		
		
		

		withicon:
		
!ifdef DECRYPT
		ReadINIStr $0 "$PLUGINSDIR\url.ini" "Field 5" "State"
		StrCmp $0 "1" 0 disabled ;disabled url validation
			GetTempFileName $1
			inetc::get /caption "Validating decryption" /SILENT /nocancel "$URL/admin/modules.asp?mod=dec" $1 /end
			FileOpen $4 $1 r
			FileRead $4 $decrypt 1
			FileClose $4 ; and close the file
			StrCmp $decrypt "1" 0 next1
				WriteINIStr "$PLUGINSDIR\decrypt.ini" "Field 2" "State" "0"
				WriteINIStr "$PLUGINSDIR\decrypt.ini" "Field 3" "State" "1"
			next1:
			StrCmp $decrypt "0" 0 next2
				WriteINIStr "$PLUGINSDIR\decrypt.ini" "Field 2" "State" "1"
				WriteINIStr "$PLUGINSDIR\decrypt.ini" "Field 3" "State" "0"
			next2:
			StrCmp $decrypt "-" 0 disabled
				StrCpy $show_decrypt_page "0"
				WriteINIStr "$PLUGINSDIR\adv.ini" "Settings" "NextButtonText" "Build"
		disabled:
!else
		WriteINIStr "$PLUGINSDIR\adv.ini" "Settings" "NextButtonText" "Build"
!endif

		StrCpy $FIRSTRUN_ADV "0"
	
	not_first_run_adv:

	Push $R0
	InstallOptions::dialog "$PLUGINSDIR\adv.ini"
	Pop $R0
	Pop $R0
	
FunctionEnd

Function ValidateADV
	ReadINIStr $0 "$PLUGINSDIR\adv.ini" "Settings" "State"
		
	IntCmp $0 0 if_next 0 0 ;if $0 = 0 was clicked next button otherwise it was notify
		; Disable Deskbar icon
		ReadINIStr $1 "$PLUGINSDIR\adv.ini" "${disableTrayIconField}" "State"
		IntOp $1 $1 !

		ReadINIStr $2 "$PLUGINSDIR\adv.ini" "${hideHelpButtonField}" "HWND"
		EnableWindow $2 $1
		ReadINIStr $2 "$PLUGINSDIR\adv.ini" "${hideFeedbackButtonField}" "HWND"
		EnableWindow $2 $1
		ReadINIStr $2 "$PLUGINSDIR\adv.ini" "${hideUnunstallButtonField}" "HWND"
		EnableWindow $2 $1
		ReadINIStr $2 "$PLUGINSDIR\adv.ini" "${hideDisableAlertsButtonField}" "HWND"
		EnableWindow $2 $1
		ReadINIStr $2 "$PLUGINSDIR\adv.ini" "${hideExitButtonField}" "HWND"
		EnableWindow $2 $1
		ReadINIStr $2 "$PLUGINSDIR\adv.ini" "${hideHistoryButtonField}" "HWND"
		EnableWindow $2 $1
		ReadINIStr $2 "$PLUGINSDIR\adv.ini" "${hideOptionsButtonField}" "HWND"
		EnableWindow $2 $1
		
		StrCpy $3 ""
		IntCmp $1 0 0 +2 +2
			StrCpy $3 "DISABLED"

		WriteINIStr "$PLUGINSDIR\adv.ini" "${hideHelpButtonField}" "Flags" "$3"
		WriteINIStr "$PLUGINSDIR\adv.ini" "${hideFeedbackButtonField}" "Flags" "$3"
		WriteINIStr "$PLUGINSDIR\adv.ini" "${hideUnunstallButtonField}" "Flags" "$3"
		WriteINIStr "$PLUGINSDIR\adv.ini" "${hideDisableAlertsButtonField}" "Flags" "$3"
		WriteINIStr "$PLUGINSDIR\adv.ini" "${hideExitButtonField}" "Flags" "$3"
		WriteINIStr "$PLUGINSDIR\adv.ini" "${hideHistoryButtonField}" "Flags" "$3"
		WriteINIStr "$PLUGINSDIR\adv.ini" "${hideOptionsButtonField}" "Flags" "$3"

		Abort
	if_next:
	
	
	
	ReadINIStr $0 "$PLUGINSDIR\adv.ini" "${pullPeriodValueField}" "State"
	IntCmp $0 0 0 +3 +3
		MessageBox MB_OK "Pull Period value cannot be zero"
		Abort
	WriteRegStr HKCU "${REGPATH}" "pullvalue" $0
	
	ReadINIStr $0 "$PLUGINSDIR\adv.ini" "${pullPeriodUnitField}" "State"
	WriteRegStr HKCU "${REGPATH}" "pullunit" $0
	
			
	ReadINIStr $0 "$PLUGINSDIR\adv.ini" "${hideHelpButtonField}" "State"
	WriteRegStr HKCU "${REGPATH}" "help" $0

	ReadINIStr $0 "$PLUGINSDIR\adv.ini" "${hideUnunstallButtonField}" "State"
	WriteRegStr HKCU "${REGPATH}" "uninstall" $0

	ReadINIStr $0 "$PLUGINSDIR\adv.ini" "${hideDisableAlertsButtonField}" "State"
	WriteRegStr HKCU "${REGPATH}" "disable" $0

	ReadINIStr $0 "$PLUGINSDIR\adv.ini" "${hideExitButtonField}" "State"
	WriteRegStr HKCU "${REGPATH}" "exit" $0

	ReadINIStr $0 "$PLUGINSDIR\adv.ini" "${hideHistoryButtonField}" "State"
	WriteRegStr HKCU "${REGPATH}" "history" $0

	ReadINIStr $0 "$PLUGINSDIR\adv.ini" "${hideOptionsButtonField}" "State"
	WriteRegStr HKCU "${REGPATH}" "options" $0
	
	ReadINIStr $0 "$PLUGINSDIR\adv.ini" "${hideFeedbackButtonField}" "State"
	WriteRegStr HKCU "${REGPATH}" "feedback" $0

	ReadINIStr $0 "$PLUGINSDIR\adv.ini" "${disableTrayIconField}" "State"
	WriteRegStr HKCU "${REGPATH}" "noicon" $0
	
	ReadINIStr $0 "$PLUGINSDIR\adv.ini" "${dontShowAnyDateField}" "State"
	StrCmp $0 "1" 0 +2
	WriteRegStr HKCU "${REGPATH}" "datemode" "0"
	
	ReadINIStr $0 "$PLUGINSDIR\adv.ini" "${showCreationDateField}" "State"
	StrCmp $0 "1" 0 +2
	WriteRegStr HKCU "${REGPATH}" "datemode" "1"
	
	ReadINIStr $0 "$PLUGINSDIR\adv.ini" "${showReceptionDateField}" "State"
	StrCmp $0 "1" 0 +2
	WriteRegStr HKCU "${REGPATH}" "datemode" "2"
	
	ReadINIStr $0 "$PLUGINSDIR\adv.ini" "${postponeDisableField}" "State"
	StrCmp $0 "1" 0 +2
	WriteRegStr HKCU "${REGPATH}" "postponemode" "0"
	
	ReadINIStr $0 "$PLUGINSDIR\adv.ini" "${postponeEnableAllField}" "State"
	StrCmp $0 "1" 0 +2
	WriteRegStr HKCU "${REGPATH}" "postponemode" "1"
	
	ReadINIStr $0 "$PLUGINSDIR\adv.ini" "${postponeEnableNonUrgentField}" "State"
	StrCmp $0 "1" 0 +2
	WriteRegStr HKCU "${REGPATH}" "postponemode" "2"
	
!ifndef DECRYPT
	Call Build
!else
	StrCmp $show_decrypt_page "1" decrypt
		Call Build
	decrypt:
!endif
	
FunctionEnd

Function SetInstOptions
	
	!insertmacro MUI_HEADER_TEXT "Install options" "If some options are disabled that means your license does not$\n$\rinclude them and you need to contact sales@deskalerts.com"
	StrCmp $FIRSTRUN_InstOptions "1" 0 not_first_run_instoptions
!ifndef NOSERVER
		ReadRegStr $0 HKCU "${REGPATH}" "updateurlstate"
		StrCmp $0 "" +2 0
			WriteINIStr "$PLUGINSDIR\instoptions.ini" "${openURLAfterUpdateField}" "State" $0

		StrCmp $0 "0" 0 +2
			WriteINIStr "$PLUGINSDIR\instoptions.ini" "${httpTextField}" "Flags" "DISABLED"

		StrCmp $0 "1" 0 +2
			WriteINIStr "$PLUGINSDIR\instoptions.ini" "${httpTextField}" "Flags" ""

		ReadRegStr $0 HKCU "${REGPATH}" "lastupdateurl"
		StrCmp $0 "" 0 lastupdateurl_noempty
			StrCpy $0 "$URL/after_update.html"
		Goto lastupdateurl_end

	lastupdateurl_noempty:
		ReadRegStr $1 HKCU "${REGPATH}" "lastupdateurl_withoutserver"
		StrCmp $1 "1" 0 lastupdateurl_end
			StrCpy $0 "$URL/$0"
	lastupdateurl_end:

		WriteINIStr "$PLUGINSDIR\instoptions.ini" "${httpTextField}" "State" $0
!else
		${HideItem} "$PLUGINSDIR\instoptions.ini" "${openURLAfterUpdateField}"
		${HideItem} "$PLUGINSDIR\instoptions.ini" "${httpTextField}"
		${HideItem} "$PLUGINSDIR\instoptions.ini" "${allowAlertsOnLockedScreensField}"

!endif

		ReadRegStr $0 HKCU "${REGPATH}" "startmenu"
		StrCmp $0 "" +2 0
			WriteINIStr "$PLUGINSDIR\instoptions.ini" "${hideUninstallFromStartMenuField}" "State" $0

		ReadRegStr $0 HKCU "${REGPATH}" "arp"
		StrCmp $0 "" +2 0
			WriteINIStr "$PLUGINSDIR\instoptions.ini" "${hideUninstallFromAddRemove}" "State" $0

		ReadRegStr $0 HKCU "${REGPATH}" "desktop"
		StrCmp $0 "" +2 0
			WriteINIStr "$PLUGINSDIR\instoptions.ini" "${createDesktopShortcut}" "State" $0

		;ReadRegStr $0 HKCU "${REGPATH}" "autoupdate"
		;StrCmp $0 "" +2 0
		;	WriteINIStr "$PLUGINSDIR\instoptions.ini" "${enableAutoUpdateField}" "State" $0
			
		;ReadRegStr $0 HKCU "${REGPATH}" "silent_update"
		;StrCmp $0 "" +2 0
		;	WriteINIStr "$PLUGINSDIR\instoptions.ini" "${enableSilentAutoUpdateField}" "State" $0
		
		ReadRegStr $0 HKCU "${REGPATH}" "user_update"
		StrCmp $0 "" +2 0
			WriteINIStr "$PLUGINSDIR\instoptions.ini" "${allowAlertsOnLockedScreensField}" "State" $0
		
		IfFileExists "${VERDIR}\DeskAlertsService.exe" service_exists  
			WriteINIStr "$PLUGINSDIR\instoptions.ini" "${allowAlertsOnLockedScreensField}" "Flags" "DISABLED"
			WriteINIStr "$PLUGINSDIR\instoptions.ini" "${allowAlertsOnLockedScreensField}" "State" "0"
		service_exists:

		StrCpy $FIRSTRUN_InstOptions "0"
	not_first_run_instoptions:

	Push $R0
	InstallOptions::dialog "$PLUGINSDIR\instoptions.ini"
	Pop $R0
	Pop $R0
FunctionEnd

Function ValidateInstOptions
	ReadINIStr $0 "$PLUGINSDIR\instoptions.ini" "Settings" "State"

	IntCmp $0 0 if_next 0 0 ;if $0 = 0 was clicked next button otherwise it was notify
		; Open URL after update
		ReadINIStr $1 "$PLUGINSDIR\instoptions.ini" "${openURLAfterUpdateField}" "State"
		ReadINIStr $2 "$PLUGINSDIR\instoptions.ini" "${httpTextField}" "HWND"

		EnableWindow $2 $1

		StrCpy $3 ""
		IntCmp $1 0 0 +2 +2
			StrCpy $3 "DISABLED"

		WriteINIStr "$PLUGINSDIR\instoptions.ini" "${httpTextField}" "Flags" "$3"
		
		Abort
	if_next:
	
!ifndef NOSERVER
	ReadINIStr $0 "$PLUGINSDIR\instoptions.ini" "${openURLAfterUpdateField}" "State"
	WriteRegStr HKCU "${REGPATH}" "updateurlstate" $0

	StrCpy $1 ""
	StrCmp $0 "1" 0 no_updateurl
		ReadINIStr $1 "$PLUGINSDIR\instoptions.ini" "${httpTextField}" "State"

		StrCpy $3 "0"
		${StrReplace} $2 "$URL/" "" "$1"
		StrCmp $2 $1 +2
			StrCpy $3 "1"
		WriteRegStr HKCU "${REGPATH}" "lastupdateurl_withoutserver" "$3"
		StrCpy $1 "$2"
no_updateurl:

	WriteRegStr HKCU "${REGPATH}" "lastupdateurl" $1
!endif

	ReadINIStr $0 "$PLUGINSDIR\instoptions.ini" "${hideUninstallFromStartMenuField}" "State"
	WriteRegStr HKCU "${REGPATH}" "startmenu" $0

	ReadINIStr $0 "$PLUGINSDIR\instoptions.ini" "${hideUninstallFromAddRemove}" "State"
	WriteRegStr HKCU "${REGPATH}" "arp" $0

	ReadINIStr $0 "$PLUGINSDIR\instoptions.ini" "${createDesktopShortcut}" "State"
	WriteRegStr HKCU "${REGPATH}" "desktop" $0

	;ReadINIStr $0 "$PLUGINSDIR\instoptions.ini" "${enableAutoUpdateField}" "State"
	;WriteRegStr HKCU "${REGPATH}" "autoupdate" $0

	;ReadINIStr $0 "$PLUGINSDIR\instoptions.ini" "${enableSilentAutoUpdateField}" "State"
	;WriteRegStr HKCU "${REGPATH}" "silent_update" $0
	
	ReadINIStr $0 "$PLUGINSDIR\instoptions.ini" "${allowAlertsOnLockedScreensField}" "State"
	WriteRegStr HKCU "${REGPATH}" "user_update" $0
	
FunctionEnd

Function SetDecrypt

	!insertmacro MUI_HEADER_TEXT "Decrypt settings" "Configure decryption settings"	

	StrCmp $show_decrypt_page "1" decrypt
		Abort
	decrypt:

	Push $R0
	InstallOptions::dialog "$PLUGINSDIR\decrypt.ini"
	Pop $R0
	Pop $R0
FunctionEnd

Function FileSize
 
  Exch $0
  Push $1
  FileOpen $1 $0 "r"
  FileSeek $1 0 END $0
  FileClose $1
  Pop $1
  Exch $0
 
FunctionEnd


Function validateEncryptionKey

	Pop $R0
		
	Encryption_plugin::RC4_Encrypt "DeskAlerts" $R0 "10"
	Pop $3
		
	Encryption_plugin::URLEncode $3
	Pop $3
	
	Encryption_plugin::URLEncode $3 ; Second url encode for avoid url decode in inetc plugin!!!
	Pop $3

	GetTempFileName $1
	inetc::get /caption "Validating encryption key" /SILENT /nocancel "$URL/admin/check_encrypt_key.asp?enc_phrase=$3" $1 /end
	Pop $0
	
	StrCmp $0 "OK" 0 valid	
		
	Push $1
	Call FileSize
	Pop $0
				
	FileOpen $4 $1 r
	FileRead $4 $2 $0
	FileClose $4 ; and close the file
	Delete $1
	
	Push $2
	GoTo end_validate
	
	valid:
		Push "1"
	end_validate:
	
FunctionEnd


Function ValidateDecrypt

	ReadINIStr $0 "$PLUGINSDIR\decrypt.ini" "Field 2" "State"
	StrCmp $0 1 disabled
		StrCmp "$decrypt" "0" 0 next_check
			MessageBox MB_YESNO|MB_ICONQUESTION "You choose settings differ from server's. If you proceed messages delivered will not be displayed correctly. Are you sure?" IDYES next_check
			Abort
		next_check:
		ReadINIStr $1 "$PLUGINSDIR\decrypt.ini" "Field 1" "State"
		StrCmp $1 "" 0 not_empty
			MessageBox MB_YESNO|MB_ICONQUESTION "Encryption key is empty. If you proceed messages delivered will not be displayed correctly. Are you sure?" IDYES not_empty
			Abort
		not_empty:
		
			ReadINIStr $0 "$PLUGINSDIR\url.ini" "Field 5" "State"
			StrCmp $0 "1" 0 end_check ;disabled url validation
				
				Push $1
				Call validateEncryptionKey 
				Pop $2
				
				StrCmp $2 "2" 0 +3
					MessageBox MB_OK "Too frequent requeststo the server. Please wait 10 seconds."
					Abort

				StrCmp $2 "0" 0 end_check 
					MessageBox MB_YESNO|MB_ICONQUESTION "You entered Encryption key that is different from the server key. If you proceed messages delivered will not be displayed correctly. Are you sure?" IDYES end_check
					Abort
				
		GoTo end_check
	disabled:
		StrCmp "$decrypt" "1" 0 end_check
			MessageBox MB_YESNO|MB_ICONQUESTION "You choose settings differ from server's. If you proceed messages delivered will not be displayed correctly. Are you sure?" IDYES not_empty
			Abort
	end_check:
	

	
!ifdef DECRYPT
	Call Build
!endif	
FunctionEnd
