#!/bin/bash
set -e # fail on any error
set -u # treat unset variables as errors

# ======[ Trap Errors ]======#
set -E # let shell functions inherit ERR trap

# Trap non-normal exit signals:
# 1/HUP, 2/INT, 3/QUIT, 15/TERM, ERR
trap err_handler 1 2 3 15 ERR
function err_handler {
local exit_status=${1:-$?}
logger -s -p "syslog.err" -t "ootync.deb" "supersh.deb script '$0' error code $exit_status (line $BASH_LINENO: '$BASH_COMMAND')"
exit $exit_status
}


rm_conffile() {
    local PKGNAME="$1"
    local CONFFILE="$2"

    [ -e "$CONFFILE" ] || return 0

    local md5sum="$(md5sum $CONFFILE | sed -e 's/ .*//')"
    local old_md5sum="$(dpkg-query -W -f='${Conffiles}' $PKGNAME | \
            sed -n -e "\' $CONFFILE ' { s/ obsolete$//; s/.* //; p }")"
    if [ "$md5sum" != "$old_md5sum" ]; then
        echo "Obsolete conffile $CONFFILE has been modified by you."
        echo "Saving as $CONFFILE.dpkg-bak ..."
        mv -f "$CONFFILE" "$CONFFILE".dpkg-bak
    else
        echo "Removing obsolete conffile $CONFFILE ..."
        mv -f "$CONFFILE" "$CONFFILE".dpkg-del
    fi
}

case "$1" in
install|upgrade)

if dpkg-maintscript-helper supports rm_conffile 2>/dev/null; then
   dpkg-maintscript-helper rm_conffile /etc/deskalerts/deskalerts.conf deskalerts -- "$@"
else
	 rm_conffile deskalerts "/etc/deskalerts/deskalerts.conf"
fi
esac


exit 0

