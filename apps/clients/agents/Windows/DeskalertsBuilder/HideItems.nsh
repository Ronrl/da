
!macro HideItem INFILE INITEM
	WriteINIStr "${INFILE}" "${INITEM}" "Left" "0"
	WriteINIStr "${INFILE}" "${INITEM}" "Right" "0"
	WriteINIStr "${INFILE}" "${INITEM}" "Top" "0"
	WriteINIStr "${INFILE}" "${INITEM}" "Bottom" "0"
!macroend
 
!define HideItem '!insertmacro "HideItem"'