#pragma once
#include <tchar.h>
#include <Windows.h>

#define VERSION_SIZE 32			 /* max size of version string */

#define CURRENT_VERSION 1
#define OLD_VERSION 0

#define CURRENT_APP _T("deskalerts.exe")
#define OLD_APP _T("backup\\deskalerts.exe")

class CVersionControl {
	static TCHAR versionUrl[256];
public:
	static BOOL convertToLongVersion(const TCHAR* vers, OUT DWORD64 &version);
	static BOOL isReleasedNewVersion(const TCHAR* local, const TCHAR* srv);
	static BOOL getVersionFromFile(OUT TCHAR (&localVersion)[VERSION_SIZE], const TCHAR* filename = _T(".\\version.txt"));
	static BOOL getVersionFromServer(OUT TCHAR (&serverVersion)[VERSION_SIZE]);
	static void setUnstable(int workVers);
	static int getStable(int unstable);
	static int getStable();
	static const TCHAR* getAppPath(int appVers);
	static BOOL backupStable();
	static void setVersionUrl(const TCHAR* serverAddress);
};