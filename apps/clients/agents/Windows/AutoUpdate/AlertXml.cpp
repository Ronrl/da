#include "stdafx.h"
#include "AlertXml.h"
#include <Windows.h>

CAlertXml::CAlertXml()
{
	CoInitialize(NULL);
	loaded = FALSE;
	HRESULT result = spXmlDoc.CoCreateInstance(__uuidof(DOMDocument));
       instance = SUCCEEDED(result) && spXmlDoc.p != NULL;
}

CAlertXml::~CAlertXml()
{
	spXmlDoc.Release();
	CoUninitialize();
}

BOOL CAlertXml::load(const TCHAR* xmlFile)
{
	VARIANT_BOOL load;
	HRESULT result = spXmlDoc->load(CComVariant(xmlFile),&load);
	loaded = SUCCEEDED(result) && load != FALSE;
	return loaded;
}

BOOL CAlertXml::elementsByTag(const TCHAR* tag, CComPtr<IXMLDOMNodeList> &nodeList)
{
	if (!loaded)
		return FALSE;
	HRESULT result = spXmlDoc->getElementsByTagName(CComBSTR(tag), &nodeList); 
	return result == S_OK && nodeList.p != NULL;
}

BOOL CAlertXml::elementByAttribute(const TCHAR* tag, const TCHAR* attr, const TCHAR* attrValue, /*out*/ CComQIPtr<IXMLDOMElement> &element)
{
	CComPtr<IXMLDOMNodeList> nodeList;
	if (elementsByTag(tag, nodeList))
	{
		CComPtr<IXMLDOMNode> spNode;
		VARIANT value;
		long nodeCount;
		HRESULT result = nodeList->get_length(&nodeCount);
		if (result != S_OK)
			return FALSE;

		for (long id = 0; id < nodeCount; ++id)
		{
			result = nodeList->get_item(id, &spNode);
			if (result != S_OK || spNode.p == NULL)
				return FALSE;
			element = spNode;
			result = element->getAttribute(CComBSTR(attr), &value);
			if (result == S_OK && wcscmp(value.bstrVal, attrValue) == 0)
			{
				return TRUE;
			}
			spNode = NULL;
		}
	}
	return FALSE;
}