#include "stdafx.h"
#include "VersionControl.h"
#include "InetUtils.h"
#include "DirectoryUtils.h"


TCHAR CVersionControl::versionUrl[256] = { 0 };

BOOL CVersionControl::convertToLongVersion(const TCHAR* vers, OUT DWORD64 &version)
{
	//components of the version
	DWORD64 major, minor, build, revision;
	TCHAR stage(_T('\0'));
	//delete the version prefix
	if (_tcsncmp(vers, _T("DeskAlerts v"), _tcslen(_T("DeskAlerts v"))) == 0)
		vers += _tcslen(_T("DeskAlerts v"));
	int scanned = _stscanf_s(vers, _T("%u.%u.%u.%u%c"), &major, &minor, &build, &revision, &stage);
	if (scanned != 4 && scanned != 5)
		return FALSE;
	stage = (stage == 'r' ? 0x3 : stage - 'a') + 1;//development stage accounting
	//check of the stage: none, a, b, c, r
	if (stage < 0 || stage > 4)
		stage = 0;
	//digital representation of the version (64 bits): [ major - 10 bits ][ minor - 14 bits ][ build - 18 bits ][ revision - 19 bits ][ stage - 3 bits]
	version = (major << 54) | ((minor & 0x3FFF) << 40) | ((build & 0x3FFFF) << 22) | ((revision & 0x7FFFF) << 3) | stage;
	return TRUE;
}

BOOL CVersionControl::isReleasedNewVersion(const TCHAR* local, const TCHAR* srv)
{
	DWORD64 locVers, srVers;
	return (convertToLongVersion(local, locVers) && convertToLongVersion(srv, srVers) &&
		locVers < srVers);
}

BOOL CVersionControl::getVersionFromFile(OUT TCHAR (&localVersion)[VERSION_SIZE], const TCHAR* filename)
{
	HANDLE hVersion = CreateFile(filename, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (INVALID_HANDLE_VALUE == hVersion)
		return FALSE;
	DWORD dwRead;
	char buffer[VERSION_SIZE];
	BOOL isRead = ReadFile(hVersion, buffer, sizeof(char) * VERSION_SIZE, &dwRead, NULL);
	CloseHandle(hVersion);
	if (isRead == FALSE || dwRead == 0)
		return FALSE;
	buffer[dwRead] = '\0';
#ifdef  _UNICODE
	isRead = (mbstowcs_s(NULL, localVersion, buffer, dwRead) == 0);
#else
	isRead = (strcpy_s(localVersion, buffer) == 0);
#endif
	return isRead;
}

BOOL CVersionControl::getVersionFromServer(OUT TCHAR (&serverVersion)[VERSION_SIZE])
{
	if (versionUrl[0] == '\0')
		return FALSE;
	return CInetUtils::sendRequest(versionUrl, serverVersion, VERSION_SIZE);
}

void CVersionControl::setUnstable(int appVers)
{
	if (CURRENT_VERSION == appVers && !CDirectUtils::isExistDirectory(_T(".\\backup")))
			return;
	CopyFile(((CURRENT_VERSION == appVers) ? _T(".\\backup\\version.txt") : _T(".\\version.txt")),
				_T(".\\stable"), FALSE);
}

int CVersionControl::getStable(int unstable)
{
	if (OLD_VERSION == unstable || !CDirectUtils::isFileExist(OLD_APP))
		return CURRENT_VERSION;
	return OLD_VERSION;
}

int CVersionControl::getStable()
{
	if (CDirectUtils::isFileExist(_T(".\\stable")))
	{
		TCHAR stable[VERSION_SIZE], current[VERSION_SIZE];
		getVersionFromFile(stable, _T(".\\stable"));
		getVersionFromFile(current);
		return (_tcscmp(stable, current) == 0);
	}
	return CURRENT_VERSION;
}

const TCHAR* CVersionControl::getAppPath(int appVers)
{
	if (OLD_VERSION == appVers && CDirectUtils::isFileExist(OLD_APP))
		return OLD_APP;
	return CURRENT_APP;
}

BOOL CVersionControl::backupStable()
{
	static CDirectUtils dirUtils;
	static BOOL init = FALSE;
	if (init == FALSE)
	{
		dirUtils.pushIgnoreFile(_T("backup"), FILE_ATTRIBUTE_DIRECTORY);
		dirUtils.pushIgnoreFile(_T("installer.msi"), FILE_ATTRIBUTE_ARCHIVE);
		init = TRUE;
	}
	
	BOOL result = TRUE;
	if (getStable() == CURRENT_VERSION)
	{
		//delete an old backup 
		dirUtils.deleteDirectory(_T("backup"));
		//backup current version
		result = dirUtils.copyDirectory(_T(".\\"), _T("backup"));
	}
	return result;
}

void CVersionControl::setVersionUrl(const TCHAR* serverAddress)
{
	_stprintf_s(versionUrl, _T("%s/api/version"), serverAddress);
}