#include "stdafx.h"
#include <windows.h>
#include <ShellAPI.h>

#include "DirectoryUtils.h"
#include "VersionControl.h"
#include "InetUtils.h"
#include "DeskAlertControl.h"
#include "AlertXml.h"

#pragma comment(lib, "User32.lib")
#pragma comment(lib, "shell32.lib")

#define CHECK_DELAY (600 * 1000) /* time between checks in milliseconds */
typedef CVersionControl VContr;
typedef CDeskAlertControl DACont;

int exitError(const TCHAR* msg)
{
	MessageBox(0, msg, _T("DeskAlerts"), MB_OK | MB_ICONERROR);
	return EXIT_FAILURE;
}

BOOL getAddress(TCHAR (&fileURL)[256])
{
	TCHAR srvAddr[128];
	//get server address from config
	if (CAlertXml::getServerAddress(srvAddr) == FALSE)
		return FALSE;
	VContr::setVersionUrl(srvAddr);
	//set the address of the file to download
	_stprintf_s(fileURL, _T("%s/version/deskalerts_setup.msi"), srvAddr);
	return TRUE;
}

BOOL isUpdated()
{
	//current version updated
	if (DACont::isWaited())
	{
		//copy the old config
		CopyFile(_T("backup\\conf.xml"), _T("conf.xml"), FALSE);
		//try to grab control and run the application
		if (DACont::lock())
		{
			//delete old version updater (it is saved in the backup directory)
			TCHAR exeName[_MAX_PATH], oldName[_MAX_PATH + 16];
			GetModuleFileName(NULL, exeName, _MAX_PATH);
			_stprintf_s(oldName, _T("%s_old"), exeName);
			DeleteFile(oldName);
			DeleteFile(_T(".\\installer.msi"));
			return TRUE;
		}
	}
	return FALSE;
}

int WINAPI _tWinMain(HINSTANCE /*hInstance*/, HINSTANCE /*hPrevInstance*/, LPTSTR /*lpCmdLine*/, int /*nCmdShow*/)
{
	if (DACont::lock() == FALSE && isUpdated() == FALSE)
		return EXIT_FAILURE;
	TCHAR locVersion[VERSION_SIZE], servVersion[VERSION_SIZE], fileUrl[256];
	if (VContr::getVersionFromFile(locVersion) == FALSE)
		exitError(_T("Error getting the current version!"));//log
	if (getAddress(fileUrl) == FALSE)
		exitError(_T("Failed to get the server address from the configuration file!"));//log

	while(TRUE)
	{
		if (VContr::getVersionFromServer(servVersion) &&
			VContr::isReleasedNewVersion(locVersion, servVersion))
		{
			//download new version packet from server
			if (CInetUtils::downloadFile(fileUrl, _T(".\\installer.msi")))
			{
				if (VContr::backupStable())
				{
					//сhange the name of the current application to install the new version correctly
					TCHAR exeName[_MAX_PATH], oldName[_MAX_PATH + 16];
					GetModuleFileName(NULL, exeName, _MAX_PATH);
					_stprintf_s(oldName, _T("%s_old"), exeName);
					MoveFile(exeName, oldName);

					//run installer a new version
					ShellExecute(NULL, _T("open"), _T("msiexec"), _T("/i installer.msi /qn /passive"), NULL, SW_HIDE);
					
					//wait until the new version is installed
					DACont::wait();
					break;
				}
			}
		}
		//waiting for a new check
		Sleep(CHECK_DELAY);
	}
	DACont::release();
	return EXIT_SUCCESS;
}