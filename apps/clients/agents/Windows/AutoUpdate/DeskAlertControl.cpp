#include "stdafx.h"
#include <tchar.h>
#include <process.h>
#include <ShellAPI.h>
#include "DeskAlertControl.h"
#include "VersionControl.h"
#include "ProcessUtils.h"

#define RUN_TIMEOUT 500 /* timeout for running DeskAlerts in milliseconds */

CDeskAlertControl* CDeskAlertControl::pInstance = NULL;

inline DWORD waitObject(HANDLE handle, DWORD dwTime)
{
	DWORD result = WaitForSingleObject(handle, dwTime);
	ReleaseMutex(handle);//release of a mutex, because WaitForSingleObject blocks it
	return result;
}

BOOL CDeskAlertControl::setMutex()
{
	if (m_updMutex != NULL)
		return TRUE;
	BOOL status = FALSE;
	m_updMutex = CreateMutex(0, FALSE, _T("DeskAlertUpdater")); /* blocking the launch of other updaters */
	if (m_updMutex != NULL)
	{
		//check whether the mutex is locked
		if (WaitForSingleObject(m_updMutex, 20) == WAIT_TIMEOUT)
			m_updMutex = NULL;
		else
		{
			m_appMutex = CreateMutex(0, FALSE, _T("DeskAlertAppStatus")); /* named mutex for the DeskAlerts application */
			status = TRUE;
		}
	}
	return status;
}

CDeskAlertControl::CDeskAlertControl()
{
	setMutex();
}
	
CDeskAlertControl::~CDeskAlertControl()
{
	release();
}

unsigned WINAPI CDeskAlertControl::mutexThread(LPVOID)
{
	int workVersion = CVersionControl::getStable();
	while(pInstance->m_updMutex != NULL)
	{
		//waiting for application mutex lock
		Sleep(RUN_TIMEOUT);
		//get the reason for the release of the application, to monitor the work
		DWORD releaseReason = waitObject(pInstance->m_appMutex, INFINITE);
		//if the application crashed
		if (WAIT_ABANDONED == releaseReason)
		{
			//the working version is labeled "unstable"
			CVersionControl::setUnstable(workVersion);
			workVersion = CVersionControl::getStable(workVersion);
			//run "stable" version
			ShellExecute(NULL, _T("open"), CVersionControl::getAppPath(workVersion), NULL, NULL, SW_HIDE);
		}
		//WAIT_OBJECT_0 - application correct completed
	}
	return S_OK;
}

void CDeskAlertControl::run()
{
	if (m_updMutex != NULL)
	{
		//create a thread for work monitoring
		_beginthreadex(NULL, 0, mutexThread, NULL, 0, NULL);
		//run stable deskalert
		TCHAR path[MAX_PATH];
		//get full path stable version
		const TCHAR* stable = CVersionControl::getAppPath(CVersionControl::getStable());
		GetCurrentDirectory(sizeof(path), path);
		_tcscat_s(path, _T("\\"));
		_tcscat_s(path, stable);
		//get process ID working version
		DWORD pId = CProcUtils::getProcessId(CURRENT_APP);
		//if the version is not stable
		if (CProcUtils::processPathCmp(path, pId) == FALSE)
		{
			CProcUtils::killProcess(pId);
			ShellExecute(NULL, _T("open"), stable, _T("/newid"), NULL, SW_NORMAL);
		}
	}
}

BOOL CDeskAlertControl::lock()
{
	static CDeskAlertControl control;
	if (pInstance == NULL)
	{
		pInstance = &control;
		control.run();
	}
	else
		if (control.m_updMutex == NULL && control.setMutex())
			control.run();
	return (control.m_updMutex != NULL);
}

void CDeskAlertControl::wait()
{
	HANDLE hInstall = CreateMutex(0, FALSE, _T("DeskAlertInstall")); /* blocking the launch of other updaters */
	while (waitObject(hInstall, 10) != WAIT_TIMEOUT)
		Sleep(50); /* sleep 50 ms to not load the processor */
	CloseHandle(hInstall);
}

BOOL CDeskAlertControl::isWaited()
{
	BOOL result = FALSE;
	HANDLE hInstall = OpenMutex(MUTEX_ALL_ACCESS, FALSE,  _T("DeskAlertInstall"));
	if (hInstall != NULL)
	{
		if (WAIT_OBJECT_0 == WaitForSingleObject(hInstall, 10))
		{
			Sleep(65); /* wait for the old version to see us and come to the end. */
			ReleaseMutex(hInstall);
			result = TRUE;
		}
		CloseHandle(hInstall);
	}
	return result;
}

void CDeskAlertControl::release()
{
	if (pInstance == NULL)
		return;
	CloseHandle(pInstance->m_appMutex);
	ReleaseMutex(pInstance->m_updMutex);
	if (CloseHandle(pInstance->m_updMutex))
		pInstance->m_updMutex = NULL;
}
