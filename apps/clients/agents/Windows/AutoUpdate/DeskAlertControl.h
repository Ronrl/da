#pragma once
#include <Windows.h>

class CDeskAlertControl
{
	HANDLE m_appMutex;
	HANDLE m_updMutex;
	static CDeskAlertControl *pInstance;
	
	CDeskAlertControl();
	~CDeskAlertControl();
	BOOL setMutex();
	void run();

	static unsigned WINAPI mutexThread(LPVOID);
public:
	static BOOL lock();
	static void wait();
	static BOOL isWaited();
	static void release();
};