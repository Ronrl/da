#pragma once
#import "msxml3.dll"  named_guids
#include <tchar.h>
#include <atlbase.h>

class CAlertXml
{
	CComPtr<IXMLDOMDocument> spXmlDoc;
	BOOL instance;
	BOOL loaded;

public:
	CAlertXml();
	~CAlertXml();
	BOOL load(const TCHAR* xmlFile);
	BOOL elementsByTag(const TCHAR* tag, CComPtr<IXMLDOMNodeList> &nodeList);
	BOOL elementByAttribute(const TCHAR* tag, const TCHAR* attr, const TCHAR* attrValue, /*out*/ CComQIPtr<IXMLDOMElement> &element);
	
	template<unsigned addrSize>
	static BOOL getServerAddress(/*out*/ TCHAR (&addr)[addrSize])
	{
		CAlertXml config;
		//load config file
		if (config.load(_T("conf.xml")))
		{
			CComQIPtr<IXMLDOMElement> server;
			VARIANT address;
			//looking for a server address
			if (config.elementByAttribute(_T("PROPERTY"), _T("id"), _T("server"), server))
			{
				//read the server address
				HRESULT result = server->getAttribute(CComBSTR(_T("default")), &address);
				if (result == S_OK && address.vt != VT_NULL)
				{
					_tcscpy_s(addr, address.bstrVal);
					return TRUE;
				}
			}
		}
		return FALSE;
	}
};