#pragma once
#include <vector>
#include <tchar.h>

struct stIgnoreFile
{
	const TCHAR *filename; 
	DWORD fileAttrib;
};

class CDirectUtils
{
	std::vector<stIgnoreFile> m_ignores;

	BOOL isIgnoreFile(const WIN32_FIND_DATA &filedata);
public:
	void pushIgnoreFile(const stIgnoreFile &file);
	void pushIgnoreFile(const TCHAR* filename, DWORD attrib);
	BOOL copyDirectory(const TCHAR *src, const TCHAR *dest);
	BOOL deleteDirectory(const TCHAR *path);

	static BOOL isExistDirectory(const TCHAR *dir);
	static BOOL isFileExist(const TCHAR *filename);

	template<DWORD count>
	void pushIgnoreFile(const stIgnoreFile (&files)[count])
	{
		for (DWORD iter = 0; iter < count; ++iter)
			m_ignores.push_back(files[iter]);
	}
};