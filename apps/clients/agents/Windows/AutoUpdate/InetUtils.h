#pragma once
#include <wininet.h>
#include <urlmon.h>
#include <tchar.h>

class CDownloadStatus : public IBindStatusCallback
{
	BOOL isComplete;
public:
	virtual HRESULT STDMETHODCALLTYPE GetPriority(LONG*);
	virtual HRESULT STDMETHODCALLTYPE OnLowResource(DWORD);
	virtual HRESULT STDMETHODCALLTYPE OnStopBinding(HRESULT, LPCWSTR);
	virtual HRESULT STDMETHODCALLTYPE GetBindInfo(DWORD*, BINDINFO*);
	virtual HRESULT STDMETHODCALLTYPE OnDataAvailable(DWORD, DWORD, FORMATETC*, STGMEDIUM*);
	virtual HRESULT STDMETHODCALLTYPE OnObjectAvailable(REFIID, IUnknown*);
    virtual HRESULT STDMETHODCALLTYPE OnProgress(ULONG, ULONG, ULONG, LPCWSTR);
	//IUnknown:
	HRESULT STDMETHODCALLTYPE QueryInterface(const IID &, void **);
	ULONG STDMETHODCALLTYPE AddRef(void);
	ULONG STDMETHODCALLTYPE Release(void);
	HRESULT STDMETHODCALLTYPE OnStartBinding(DWORD, IBinding*);

	BOOL isSuccessful() const;
};

class CInetUtils
{
public:
	static BOOL sendRequest(const TCHAR *urlAddr, TCHAR *outBuffer, const DWORD bufferSize);
	BOOL sendHTTPRequest1(const TCHAR *host,  const TCHAR * url,const TCHAR *requestType, const TCHAR *requestData, TCHAR *outBuffer, const DWORD bufferSize);
	//static DWORD sendHTTPRequest(CString szURL, bool skipHttpsCheck /*= true*/, const TCHAR *szRequestType, const  char *szRequestData, char *outBuffer, const DWORD bufferSize );

	static BOOL downloadFile(const TCHAR *urlAddr, const TCHAR *filename);
	static BOOL sendHTTPRequest(const TCHAR *host, const TCHAR * url,const TCHAR *requestType, const TCHAR *request, TCHAR *outBuffer, const DWORD bufferSize);
};