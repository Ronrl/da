#pragma once
#include <Windows.h>
#include <tchar.h>
#include <tlhelp32.h>
#include <Psapi.h>
#pragma comment(lib, "Psapi.lib")

class CProcUtils
{
public:

	static BOOL killProcess(DWORD processID)
	{
		HANDLE hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, processID);
		return INVALID_HANDLE_VALUE != hProcess && TerminateProcess(hProcess, 0) && CloseHandle(hProcess);
	}

	const TCHAR* getProcessNameByPath(const TCHAR* procPath)
	{
		size_t len = _tcslen(procPath) - 1;
		while (len >= 0 && procPath[len] != '\\' && procPath[len] != '/')
			--len;
		return procPath + len + 1;
	}

	static BOOL processPathCmp(const TCHAR* procPath, DWORD pId)
	{
		TCHAR filename[_MAX_PATH];
		BOOL result = FALSE;

		HANDLE processHandle = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, pId);
		if (processHandle != NULL)
		{
			if (GetModuleFileNameEx(processHandle, NULL, filename, MAX_PATH) != 0)
				result = _tcsstr(filename, procPath) != NULL;
			CloseHandle(processHandle);
		}
		return result;
	}

	static DWORD getProcessId(const TCHAR* procName)
	{
		if (procName == NULL)
			return 0;
		PROCESSENTRY32 entry;
		entry.dwSize = sizeof(PROCESSENTRY32);
		DWORD resultPid = 0;

		HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);
		if (Process32First(snapshot, &entry) == TRUE)
		{
			while (Process32Next(snapshot, &entry) == TRUE)
			{
				if (_tcscmp(entry.szExeFile, procName) == 0)
				{
					resultPid = entry.th32ProcessID;
					break;
				}
			}
		}
		CloseHandle(snapshot);
		return resultPid;
	}

	template<DWORD procCount>
	static BOOL closeProcess(const TCHAR* (&procNames)[procCount])
	{
		PROCESSENTRY32 entry;
		entry.dwSize = sizeof(PROCESSENTRY32);
		DWORD killCount = 0;

		HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);
		if (Process32First(snapshot, &entry) == TRUE)
		{
			while (Process32Next(snapshot, &entry) == TRUE)
			{
				DWORD procId = 0;
				while (procId < procCount && _tcscmp(entry.szExeFile, procNames[procId]))
					++procId;
			
				if (procId < procCount)
				{
					if (killProcess(entry.th32ProcessID))
						++killCount;
					if (procCount == killCount)
						break;
				}
			}
		}
		CloseHandle(snapshot);
		return TRUE;
	}
};