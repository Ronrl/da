#include "stdafx.h"
#include "InetUtils.h"
#include <windows.h>
#pragma comment(lib, "wininet.lib")
#pragma comment(lib, "Urlmon.lib")
#pragma comment(lib, "crypt32.lib")

#pragma warning(disable:4100)

HRESULT STDMETHODCALLTYPE CDownloadStatus::GetPriority(LONG *priority)
{
	return E_NOTIMPL;
}

 HRESULT STDMETHODCALLTYPE CDownloadStatus::OnLowResource(DWORD reserved)
{
	return S_OK;
}

HRESULT STDMETHODCALLTYPE CDownloadStatus::OnStopBinding(HRESULT hresult, LPCWSTR error)
{
	isComplete = (hresult == 1);
	return E_NOTIMPL;
}

HRESULT STDMETHODCALLTYPE CDownloadStatus::GetBindInfo(DWORD *bind, BINDINFO *bindinfo)
{
	return E_NOTIMPL;
}

HRESULT STDMETHODCALLTYPE CDownloadStatus::OnDataAvailable(DWORD grfBSCF, DWORD dwSize, FORMATETC*, STGMEDIUM*)
{
	return E_NOTIMPL;
}

HRESULT STDMETHODCALLTYPE CDownloadStatus::OnObjectAvailable(REFIID riid, IUnknown *punk)
{
	return E_NOTIMPL;
}
 
HRESULT STDMETHODCALLTYPE CDownloadStatus::OnProgress(ULONG bytes, ULONG maxBytes, ULONG statusCode, LPCWSTR szText)
{
	return S_OK;
}

HRESULT STDMETHODCALLTYPE CDownloadStatus::QueryInterface(const IID &, void **)
{
	return E_NOINTERFACE;
}
	
ULONG STDMETHODCALLTYPE CDownloadStatus::AddRef(void)
{
	return S_OK;
}
	
ULONG STDMETHODCALLTYPE CDownloadStatus::Release(void)
{
	//Download complete
	//TODO: integrity check
	return S_OK;
}
	
HRESULT STDMETHODCALLTYPE CDownloadStatus::OnStartBinding(DWORD reserved, IBinding *bind)
{
	isComplete = FALSE;
	return E_NOTIMPL;
}

BOOL CDownloadStatus::isSuccessful() const
{
	return isComplete;
}

BOOL CInetUtils::sendRequest(const TCHAR *urlAddr, TCHAR *outBuffer, const DWORD bufferSize)
{
	BOOL status = FALSE;
	HINTERNET hSession = InternetOpen(_T("DeskAlertsUpdate"), INTERNET_OPEN_TYPE_PRECONFIG,  NULL, NULL, 0);            
	if (hSession != NULL) 
	{
		HINTERNET hRequest = InternetOpenUrl(hSession, urlAddr, NULL, 0, INTERNET_FLAG_NO_CACHE_WRITE | INTERNET_FLAG_NO_COOKIES, 0);
		if (hRequest != NULL)
		{
			DWORD dwAvailSize = 0, dwReaded = 0;
			if (InternetQueryDataAvailable(hRequest, &dwAvailSize, 0, 0))
			{
				if (dwAvailSize >= bufferSize)
					dwAvailSize = bufferSize - 1;
				char *pBuffer = new char[dwAvailSize + 1];
				if (InternetReadFile(hRequest, pBuffer, dwAvailSize, &dwReaded))
				{
					pBuffer[dwReaded] = 0;
			#ifdef  _UNICODE
					status = (mbstowcs_s(NULL, outBuffer, bufferSize, pBuffer, dwReaded) == 0);
			#else
					status = (strcpy_s(localVersion, bufferSize, buffer) == 0);
			#endif
				}
				delete[] pBuffer;
			}
			InternetCloseHandle(hRequest);
		}
		InternetCloseHandle(hSession);
	}
	return status;
}



BOOL CInetUtils::sendHTTPRequest(const TCHAR *host,  const TCHAR * url,const TCHAR *requestType, const TCHAR *requestData, TCHAR *outBuffer, const DWORD bufferSize)
{
	BOOL status = FALSE;
    HINTERNET hIntSession = 
      ::InternetOpen(_T("DeskAlertsAutoUpdate"), INTERNET_OPEN_TYPE_DIRECT, NULL, NULL, 0);

    HINTERNET hHttpSession = 
      InternetConnect(hIntSession, host, INTERNET_DEFAULT_HTTP_PORT, 0, 0, INTERNET_SERVICE_HTTP, 0, NULL);

    HINTERNET hHttpRequest = HttpOpenRequest( hHttpSession, /*_T("GET")*/ requestType, url, 0, 0, 0, INTERNET_FLAG_RELOAD, 0);

    TCHAR* szHeaders = _T("Content-Type: application/json; charset=utf-8");//_T("Content-Type: text/html\nMySpecialHeder: whatever");
    //CHAR szReq[1024] = "";
    if( !HttpSendRequest(hHttpRequest, szHeaders, _tcslen(szHeaders), (LPVOID)requestData, _tcslen(requestData))) {
      DWORD dwErr = GetLastError();
      /// handle error
	  if( dwErr!=0 )return false;
    }

	
    //CHAR szBuffer[1025];
    DWORD dwRead=0;
    while(::InternetReadFile(hHttpRequest, outBuffer, sizeof(outBuffer)-1, &dwRead) && dwRead) {
      outBuffer[dwRead] = 0;
      //OutputDebugStringA(szBuffer);
      dwRead=0;
    }

    ::InternetCloseHandle(hHttpRequest);
    ::InternetCloseHandle(hHttpSession);
    ::InternetCloseHandle(hIntSession);	return status;
}

BOOL CInetUtils::downloadFile(const TCHAR *urlAddr, const TCHAR *filename)
{
	CDownloadStatus callback;
	HRESULT hResult = URLDownloadToFile(0, urlAddr, filename, 0, &callback);
	return hResult == S_OK && callback.isSuccessful();
}