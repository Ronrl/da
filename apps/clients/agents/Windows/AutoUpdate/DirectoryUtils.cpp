#include "stdafx.h"
#include "DirectoryUtils.h"
#include <windows.h>
#include <sys/stat.h>

BOOL CDirectUtils::isIgnoreFile(const WIN32_FIND_DATA &filedata)
{
	std::vector<stIgnoreFile>::iterator iter = m_ignores.begin();
	for(; iter != m_ignores.end(); ++iter)
		if ((filedata.dwFileAttributes & iter->fileAttrib) &&	//matching file attributes
			_tcscmp(filedata.cFileName, iter->filename) == 0)	//match file names
			return TRUE;
	return FALSE;
}

void CDirectUtils::pushIgnoreFile(const stIgnoreFile &file)
{
	m_ignores.push_back(file);
}

void CDirectUtils::pushIgnoreFile(const TCHAR* filename, DWORD attrib)
{
	stIgnoreFile addedFile = {filename, attrib};
	m_ignores.push_back(addedFile);
}

BOOL CDirectUtils::isExistDirectory(const TCHAR *dir)
{
	DWORD attrib = GetFileAttributes(dir);
	return INVALID_FILE_ATTRIBUTES != attrib && (attrib & FILE_ATTRIBUTE_DIRECTORY);
}

BOOL CDirectUtils::isFileExist(const TCHAR *filename)
{
	return INVALID_FILE_ATTRIBUTES != GetFileAttributes(filename);
}

BOOL CDirectUtils::copyDirectory(const TCHAR *src, const TCHAR *dest)
{
	WIN32_FIND_DATA ffd;
	TCHAR tmpsrc[MAX_PATH + 1];
	_tcscpy_s(tmpsrc, MAX_PATH + 1, src);
	if (tmpsrc[_tcslen(tmpsrc) - 1] != _T('\\'))
	{
		_tcscat_s(tmpsrc, MAX_PATH + 1, _T("\\"));
	}

	TCHAR tmpdest[MAX_PATH + 1];
	_tcscpy_s(tmpdest, MAX_PATH + 1, dest);
	if (tmpdest[_tcslen(tmpdest) - 1] == _T('\\'))
	{
		tmpdest[_tcslen(tmpdest) - 1] = _T('\0'); //replace \ char
	}

	BOOL res = isExistDirectory(tmpdest) || !_tmkdir(tmpdest);
	if (res)
	{
		_tcscat_s(tmpdest, MAX_PATH + 1, _T("\\"));

		TCHAR findstr[MAX_PATH + 1];
		_tcscpy_s(findstr, MAX_PATH + 1, tmpsrc);
		_tcscat_s(findstr, MAX_PATH + 1, _T("*"));
		const HANDLE hFind = FindFirstFile(findstr, &ffd);
		if (INVALID_HANDLE_VALUE != hFind)
		{
			do
			{
				if (isIgnoreFile(ffd))
					continue;
				
				TCHAR filesrc[MAX_PATH + 1];
				_tcscpy_s(filesrc, MAX_PATH + 1, tmpsrc);
				_tcscat_s(filesrc, MAX_PATH + 1, ffd.cFileName);

				TCHAR filedest[MAX_PATH + 1];
				_tcscpy_s(filedest, MAX_PATH + 1, tmpdest);
				_tcscat_s(filedest, MAX_PATH + 1, ffd.cFileName);
				if (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
				{
					if (_tcscmp(ffd.cFileName, _T(".")) && _tcscmp(ffd.cFileName, _T("..")) && _tcscmp(ffd.cFileName, dest))
						copyDirectory(filesrc, filedest);
				}
				else
				{
					if (!CopyFile(filesrc, filedest, FALSE))
						res = FALSE;
				}
			} while (FindNextFile(hFind, &ffd));
			FindClose(hFind);
		}
	}
	return res;
}

BOOL CDirectUtils::deleteDirectory(const TCHAR *path)
{
	WIN32_FIND_DATA ffd;
	TCHAR tmp[MAX_PATH + 1];
	_tcscpy_s(tmp, MAX_PATH + 1, path);
	if (tmp[_tcslen(tmp) - 1] != _T('\\'))
	{
		_tcscat_s(tmp, MAX_PATH + 1, _T("\\"));
	}
	TCHAR findstr[MAX_PATH + 1];
	_tcscpy_s(findstr, MAX_PATH + 1, tmp);
	_tcscat_s(findstr, MAX_PATH + 1, _T("*"));
	const HANDLE hFind = FindFirstFile(findstr, &ffd);
	if (INVALID_HANDLE_VALUE != hFind)
	{
		do
		{
			TCHAR filepath[MAX_PATH + 1];
			_tcscpy_s(filepath, MAX_PATH + 1, tmp);
			_tcscat_s(filepath, MAX_PATH + 1, ffd.cFileName);
			if (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			{
				if (_tcscmp(ffd.cFileName, _T(".")) && _tcscmp(ffd.cFileName, _T("..")))
					deleteDirectory(filepath);
			}
			else
			{
				if (ffd.dwFileAttributes & FILE_ATTRIBUTE_READONLY)
				{
					_tchmod(filepath, _S_IWRITE);
				}
				DeleteFile(filepath);
			}
		} while (FindNextFile(hFind, &ffd));
		FindClose(hFind);
	}
	tmp[_tcslen(tmp) - 1] = _T('\0'); //replace \ char
	return RemoveDirectory(tmp);
}

/*BOOL CDirectUtils::moveDirectory(const TCHAR *src, const TCHAR *dest)
{
	WIN32_FIND_DATA ffd;
	TCHAR tmpsrc[MAX_PATH + 1];
	_tcscpy_s(tmpsrc, MAX_PATH + 1, src);
	if (tmpsrc[_tcslen(tmpsrc) - 1] != _T('\\'))
	{
		_tcscat_s(tmpsrc, MAX_PATH + 1, _T("\\"));
	}

	TCHAR tmpdest[MAX_PATH + 1];
	_tcscpy_s(tmpdest, MAX_PATH + 1, dest);
	if (tmpdest[_tcslen(tmpdest) - 1] == _T('\\'))
	{
		tmpdest[_tcslen(tmpdest) - 1] = _T('\0'); //replace \ char
	}

	BOOL res = isExistDirectory(tmpdest) || !_tmkdir(tmpdest);
	if (res)
	{
		_tcscat_s(tmpdest, MAX_PATH + 1, _T("\\"));

		TCHAR findstr[MAX_PATH + 1];
		_tcscpy_s(findstr, MAX_PATH + 1, tmpsrc);
		_tcscat_s(findstr, MAX_PATH + 1, _T("*"));
		const HANDLE hFind = FindFirstFile(findstr, &ffd);
		if (INVALID_HANDLE_VALUE != hFind)
		{
			do
			{
				if (isIgnoreFile(ffd))
					continue;
					
				TCHAR filesrc[MAX_PATH + 1];
				_tcscpy_s(filesrc, MAX_PATH + 1, tmpsrc);
				_tcscat_s(filesrc, MAX_PATH + 1, ffd.cFileName);

				TCHAR filedest[MAX_PATH + 1];
				_tcscpy_s(filedest, MAX_PATH + 1, tmpdest);
				_tcscat_s(filedest, MAX_PATH + 1, ffd.cFileName);
				if (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
				{
					if (_tcscmp(ffd.cFileName, _T(".")) && _tcscmp(ffd.cFileName, _T("..")))
						moveDirectory(filesrc, filedest);
				}
				else
				{
					if (!MoveFile(filesrc, filedest))
						res = FALSE;
				}
			} while (FindNextFile(hFind, &ffd));
			FindClose(hFind);
		}
	}
	return res;
}*/