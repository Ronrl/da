/******************************************************************************

  %name: nwdirect.h %
  %version: 14 %
  %date_modified: Fri Jun 15 10:23:21 2007 %
  $Copyright:

  Copyright (c) 1989-2007 Novell, Inc.  All Rights Reserved.

  USE AND REDISTRIBUTION OF THIS WORK IS SUBJECT TO THE DEVELOPER LICENSE
  AGREEMENT OR OTHER AGREEMENT THROUGH WHICH NOVELL, INC. MAKES THE WORK
  AVAILABLE. THIS WORK MAY NOT BE ADAPTED WITHOUT NOVELL'S PRIOR WRITTEN
  CONSENT.

  NOVELL PROVIDES THE WORK "AS IS," WITHOUT ANY EXPRESS OR IMPLIED WARRANTY,
  INCLUDING WITHOUT LIMITATION THE IMPLIED WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT. NOVELL, THE AUTHORS
  OF THE WORK, AND THE OWNERS OF COPYRIGHT IN THE WORK ARE NOT LIABLE FOR ANY
  CLAIM, DAMAGES, OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT,
  OR OTHERWISE, ARISING FROM, OUT OF, OR IN CONNECTION WITH THE WORK OR THE
  USE OR OTHER DEALINGS IN THE WORK.$

 *****************************************************************************/

#if ! defined ( NWDIRECT_H )
#define NWDIRECT_H

#if ! defined ( NTYPES_H )
#include "ntypes.h"
#endif

#if ! defined ( NWAPIDEF_H )
#include "nwapidef.h"
#endif

#if ! defined ( NWCALDEF_H )
#include "nwcaldef.h"
#endif

#include "npackon.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct
{
   nuint32 totalBlocks;
   nuint32 availableBlocks;

   nuint32 purgeableBlocks;       /* set to zero if a dirHandle is present */
   nuint32 notYetPurgeableBlocks;/*....when the NWGetDIrSpaceInfo() is called */

   nuint32 totalDirEntries;
   nuint32 availableDirEntries;
   nuint32 reserved;
   nuint8  sectorsPerBlock;
   nuint8  volLen;
   nuint8  volName[NW_MAX_VOLUME_NAME_LEN];
} DIR_SPACE_INFO;

/* Trustee Access Rights in a network directory */
/* NOTE: TA_OPEN is obsolete in 3.x */
#ifndef TA_NONE
#define TA_NONE       0x00
#define TA_READ       0x01
#define TA_WRITE      0x02
#define TA_OPEN       0x04
#define TA_CREATE     0x08
#define TA_DELETE     0x10
#define TA_OWNERSHIP  0x20
#define TA_SEARCH     0x40
#define TA_MODIFY     0x80
#define TA_ALL        0xFB
#endif

typedef struct
{
   nuint32  objectID;
   nuint16  objectRights;
} TRUSTEE_INFO;

typedef struct
{
      nuint8  level;
      nuint32 max;
      nuint32 current;
} NW_LIMIT_LIST_INNER;

typedef struct
{
   nuint8 numEntries;
   NW_LIMIT_LIST_INNER list[102];
} NW_LIMIT_LIST;

N_EXTERN_LIBRARY( NWCCODE )
NWAddTrusteeToDirectory
(
   NWCONN_HANDLE       conn,
   NWDIR_HANDLE        dirHandle,
   const nstr8 N_FAR * path,
   nuint32             trusteeID,
   nuint8              rightsMask
);

N_EXTERN_LIBRARY( NWCCODE )
NWAddTrusteeToDirectoryExt
(
   NWCONN_HANDLE       conn,
   NWDIR_HANDLE        dirHandle,
   const nstr8 N_FAR * path,
   nuint32             trusteeID,
   nuint8              rightsMask
);

N_EXTERN_LIBRARY( NWCCODE )
NWDeleteTrusteeFromDirectory
(
   NWCONN_HANDLE       conn,
   NWDIR_HANDLE        dirHandle,
   const nstr8 N_FAR * path,
   nuint32             objID
);

N_EXTERN_LIBRARY( NWCCODE )
NWDeleteTrusteeFromDirectoryExt
(
   NWCONN_HANDLE       conn,
   NWDIR_HANDLE        dirHandle,
   const nstr8 N_FAR * path,
   nuint32             objID
);

N_EXTERN_LIBRARY( NWCCODE )
NWGetEffectiveDirectoryRights
(
   NWCONN_HANDLE       conn,
   NWDIR_HANDLE        dirHandle,
   const nstr8 N_FAR * path,
   pnuint16            rightsMask
);

N_EXTERN_LIBRARY( NWCCODE )
NWModifyMaximumRightsMask
(
   NWCONN_HANDLE       conn,
   NWDIR_HANDLE        dirHandle,
   const nstr8 N_FAR * path,
   nuint8              revokeRightsMask,
   nuint8              grantRightsMask
);

N_EXTERN_LIBRARY( NWCCODE )
NWScanDirectoryForTrustees
(
   NWCONN_HANDLE       conn,
   NWDIR_HANDLE        dirHandle,
   const nstr8 N_FAR * searchPath,
   pnuint16            iterHandle,
   pnstr8              dirName,
   pnuint32            dirDateTime,
   pnuint32            ownerID,
   pnuint32            trusteeIDs,
   pnuint8             trusteeRights
);

N_EXTERN_LIBRARY( NWCCODE )
NWScanDirectoryForTrustees2
(
   NWCONN_HANDLE        conn,
   NWDIR_HANDLE         dirHandle,
   const nstr8  N_FAR * searchPath,
   pnuint32             iterHandle,
   pnstr8               dirName,
   pnuint32             dirDateTime,
   pnuint32             ownerID,
   TRUSTEE_INFO N_FAR * trusteeList
);

#define NWScanDirectoryInformation(a, b, c, d, e, f, g, h) \
        NWIntScanDirectoryInformation(a, b, c, d, e, f, g, h, 0)

N_EXTERN_LIBRARY( NWCCODE )
NWIntScanDirectoryInformation
(
   NWCONN_HANDLE       conn,
   NWDIR_HANDLE        dirHandle,
   const nstr8 N_FAR * searchPath,
   pnuint16            iterHandle,
   pnstr8              dirName,
   pnuint32            dirDateTime,
   pnuint32            ownerID,
   pnuint8             rightsMask,
   nuint16             augmentFlag
);

N_EXTERN_LIBRARY( NWCCODE )
NWIntScanDirectoryInformation2
(
   NWCONN_HANDLE       conn,
   NWDIR_HANDLE        dirHandle,
   const nstr8 N_FAR * searchPath,
   pnuint8             sequence,
   pnstr8              dirName,
   pnuint32            dirDateTime,
   pnuint32            ownerID,
   pnuint8             rightsMask,
   nuint16             augmentFlag
);

N_EXTERN_LIBRARY( NWCCODE )
NWSetDirectoryInformation
(
   NWCONN_HANDLE       conn,
   NWDIR_HANDLE        dirHandle,
   const nstr8 N_FAR * path,
   nuint32             dirDateTime,
   nuint32             ownerID,
   nuint8              rightsMask
);

N_EXTERN_LIBRARY( NWCCODE )
NWAllocPermanentDirectoryHandle
(
   NWCONN_HANDLE        conn,
   NWDIR_HANDLE         dirHandle,
   const nstr8  N_FAR * dirPath,
   NWDIR_HANDLE N_FAR * newDirHandle,
   pnuint8              effectiveRights
);

N_EXTERN_LIBRARY( NWCCODE )
NWAllocTemporaryDirectoryHandle
(
   NWCONN_HANDLE        conn,
   NWDIR_HANDLE         dirHandle,
   const nstr8  N_FAR * dirPath,
   NWDIR_HANDLE N_FAR * newDirHandle,
   pnuint8              rightsMask
);

N_EXTERN_LIBRARY( NWCCODE )
NWDeallocateDirectoryHandle
(
   NWCONN_HANDLE  conn,
   NWDIR_HANDLE   dirHandle
);

N_EXTERN_LIBRARY( NWCCODE )
NWSetDirectoryHandlePath
(
   NWCONN_HANDLE       conn,
   NWDIR_HANDLE        sourceDirHandle,
   const nstr8 N_FAR * dirPath,
   NWDIR_HANDLE        destDirHandle
);

N_EXTERN_LIBRARY( NWCCODE )
NWGetDirectoryHandlePath
(
   NWCONN_HANDLE  conn,
   NWDIR_HANDLE   dirHandle,
   pnstr8         dirPath
);

N_EXTERN_LIBRARY( NWCCODE )
NWGetDirectoryHandlePathExt
(
   NWCONN_HANDLE  conn,
   NWDIR_HANDLE   dirHandle,
   pnstr8         dirPath
);

N_EXTERN_LIBRARY( NWCCODE )
NWCreateDirectory
(
   NWCONN_HANDLE       conn,
   NWDIR_HANDLE        dirHandle,
   const nstr8 N_FAR * dirPath,
   nuint8              accessMask
);

N_EXTERN_LIBRARY( NWCCODE )
NWDeleteDirectory
(
   NWCONN_HANDLE       conn,
   NWDIR_HANDLE        dirHandle,
   const nstr8 N_FAR * dirPath
);

N_EXTERN_LIBRARY( NWCCODE )
NWRenameDirectory
(
   NWCONN_HANDLE       conn,
   NWDIR_HANDLE        dirHandle,
   const nstr8 N_FAR * oldName,
   const nstr8 N_FAR * newName
);

N_EXTERN_LIBRARY( NWCCODE )
NWSetDirSpaceLimit
(
   NWCONN_HANDLE  conn,
   NWDIR_HANDLE   dirHandle,
   nuint32        spaceLimit
);

N_EXTERN_LIBRARY( NWCCODE )
NWGetDirSpaceLimitList
(
   NWCONN_HANDLE  conn,
   NWDIR_HANDLE   dirHandle,
   pnuint8        returnBuf
);

N_EXTERN_LIBRARY( NWCCODE )
NWGetDirSpaceLimitList2
(
   NWCONN_HANDLE  conn,
   NWDIR_HANDLE   dirHandle,
   NW_LIMIT_LIST N_FAR * limitList
);

N_EXTERN_LIBRARY( NWCCODE )
NWGetDirSpaceInfo
(
   NWCONN_HANDLE  conn,
   NWDIR_HANDLE   dirHandle,
   nuint16        volNum,
   DIR_SPACE_INFO N_FAR * spaceInfo
);

#ifndef TF_READ_ONLY
#define TF_NORMAL          0x0000
#define TF_READ_ONLY       0x0001L
#define TF_HIDDEN          0x0002L
#define TF_SYSTEM          0x0004L
#define TF_EXECUTE_ONLY    0x0008L

#define TF_DIRECTORY       0x0010L
#define TF_NEEDS_ARCHIVED  0x0020L
#define TF_EXECUTE_CONFIRM 0X0040L
#define TF_SHAREABLE       0x0080L

#define TF_LOW_SEARCH_BIT  0x0100L
#define TF_MID_SEARCH_BIT  0x0200L
#define TF_HI_SEARCH_BIT   0x0400L
#define TF_PRIVATE         0x0800L

#define TF_TRANSACTIONAL   0x1000L
#define TF_INDEXED         0x2000L
#define TF_READ_AUDIT      0x4000L
#define TF_WRITE_AUDIT     0x8000L

#define TF_PURGE           0x10000L
#define TF_RENAME_INHIBIT  0x20000L
#define TF_DELETE_INHIBIT  0x40000L
#define TF_COPY_INHIBIT    0x80000L
#define TF_AUDITING_BIT   0x00100000L
#endif

/* DIRECTORY ATTRIBUTES */

#define TD_HIDDEN          TF_HIDDEN
#define TD_SYSTEM          TF_SYSTEM
#define TD_PURGE           TF_PURGE
#define TD_PRIVATE         TF_PRIVATE
#define TD_VISIBLE         TF_PRIVATE
#define TD_RENAME_INHIBIT  TF_RENAME_INHIBIT
#define TD_DELETE_INHIBIT  TF_DELETE_INHIBIT

#ifdef __cplusplus
}
#endif

   /* The NLM LibC x-plat libraries do not support obsolete apis
   */
#include <stddef.h>
#if !defined(__NOVELL_LIBC__)
   #ifdef INCLUDE_OBSOLETE
      #include "obsolete/o_direct.h"
   #endif
#endif /* !defined(__NOVELL_LIBC__) */

#include "npackoff.h"
#endif
