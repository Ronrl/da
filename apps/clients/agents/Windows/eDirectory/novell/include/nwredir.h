/******************************************************************************

  %name: nwredir.h %
  %version: 5 %
  %date_modified: Fri Jun 15 10:23:56 2007 %
  $Copyright:

  Copyright (c) 1989-2007 Novell, Inc.  All Rights Reserved.

  USE AND REDISTRIBUTION OF THIS WORK IS SUBJECT TO THE DEVELOPER LICENSE
  AGREEMENT OR OTHER AGREEMENT THROUGH WHICH NOVELL, INC. MAKES THE WORK
  AVAILABLE. THIS WORK MAY NOT BE ADAPTED WITHOUT NOVELL'S PRIOR WRITTEN
  CONSENT.

  NOVELL PROVIDES THE WORK "AS IS," WITHOUT ANY EXPRESS OR IMPLIED WARRANTY,
  INCLUDING WITHOUT LIMITATION THE IMPLIED WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT. NOVELL, THE AUTHORS
  OF THE WORK, AND THE OWNERS OF COPYRIGHT IN THE WORK ARE NOT LIABLE FOR ANY
  CLAIM, DAMAGES, OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT,
  OR OTHERWISE, ARISING FROM, OUT OF, OR IN CONNECTION WITH THE WORK OR THE
  USE OR OTHER DEALINGS IN THE WORK.$

 *****************************************************************************/
#if ! defined ( NWREDIR_H )
#define NWREDIR_H

#if ! defined ( NTYPES_H )
#include "ntypes.h"
#endif

#if ! defined ( NWCALDEF_H )
# include "nwcaldef.h"
#endif

#include "npackon.h"

#ifdef __cplusplus
extern "C" {
#endif

N_EXTERN_LIBRARY( NWCCODE )
NWParseUNCPath
(
   pnstr8   pbstrUNCPath,
   NWCONN_HANDLE N_FAR * conn,
   pnstr8   pbstrServerName,
   pnstr8   pbstrVolName,
   pnstr8   pbstrPath,
   pnstr8   pbstrNWPath
);

N_EXTERN_LIBRARY( NWCCODE )
NWParseUNCPathConnRef
(
   pnstr8   pbstrUNCPath,
   pnuint32 pluConnRef,
   pnstr8   pbstrServerName,
   pnstr8   pbstrVolName,
   pnstr8   pbstrPath,
   pnstr8   pbstrNWPath
);

#ifdef __cplusplus
}
#endif

#include "npackoff.h"
#endif   /* NWREDIR_H */
