/******************************************************************************

  %name: nwdstype.h %
  %version: 7 %
  %date_modified: Fri Jun 15 10:23:41 2007 %
  $Copyright:

  Copyright (c) 1989-2007 Novell, Inc.  All Rights Reserved.

  USE AND REDISTRIBUTION OF THIS WORK IS SUBJECT TO THE DEVELOPER LICENSE
  AGREEMENT OR OTHER AGREEMENT THROUGH WHICH NOVELL, INC. MAKES THE WORK
  AVAILABLE. THIS WORK MAY NOT BE ADAPTED WITHOUT NOVELL'S PRIOR WRITTEN
  CONSENT.

  NOVELL PROVIDES THE WORK "AS IS," WITHOUT ANY EXPRESS OR IMPLIED WARRANTY,
  INCLUDING WITHOUT LIMITATION THE IMPLIED WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT. NOVELL, THE AUTHORS
  OF THE WORK, AND THE OWNERS OF COPYRIGHT IN THE WORK ARE NOT LIABLE FOR ANY
  CLAIM, DAMAGES, OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT,
  OR OTHERWISE, ARISING FROM, OUT OF, OR IN CONNECTION WITH THE WORK OR THE
  USE OR OTHER DEALINGS IN THE WORK.$

 *****************************************************************************/

#if ! defined ( NWDSTYPE_H )
#define NWDSTYPE_H

#ifndef NWDSCCODE
#define NWDSCCODE    int
#endif

/*******************************************************************/
/*                                                                 */
/*    !!!!! The following types have been obsoleted !!!!!!!!!      */
/*                                                                 */
/*   The following have been obsoleted - use types found in        */
/*   ntypes.h                                                      */
/*                                                                 */
/*   ntypes.h contains equivalent types for each of the typedefs   */
/*   listed below.  For example "uint32" is "nuint32" in ntypes.h  */
/*                                                                 */
/*   These typedefs also conflicted with defines in the WinSock2   */
/*   headers on NetWare.  The decision was made to obsolete these  */
/*   types to eliminate conflicts.                                 */ 
/*******************************************************************/

#if ! defined ( NTYPES_H )
#include "ntypes.h"
#endif

#ifdef INCLUDE_OBSOLETE

typedef unsigned long uint32;
typedef signed long int32;
typedef unsigned short uint16;
typedef signed short int16;
typedef unsigned char uint8;
typedef signed char int8;

#ifndef NWUNSIGNED
#define NWUNSIGNED unsigned
#endif

#endif /* #ifdef INCLUDE_OBSOLETE */

#endif /* NWDSTYPE_H */
