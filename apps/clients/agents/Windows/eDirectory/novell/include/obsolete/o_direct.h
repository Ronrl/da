/******************************************************************************

  %name: o_direct.h %
  %version: 3 %
  %date_modified: Fri Jun 15 10:24:24 2007 %
  $Copyright:

  Copyright (c) 1999-2007 Novell, Inc.  All Rights Reserved.

  USE AND REDISTRIBUTION OF THIS WORK IS SUBJECT TO THE DEVELOPER LICENSE
  AGREEMENT OR OTHER AGREEMENT THROUGH WHICH NOVELL, INC. MAKES THE WORK
  AVAILABLE. THIS WORK MAY NOT BE ADAPTED WITHOUT NOVELL'S PRIOR WRITTEN
  CONSENT.

  NOVELL PROVIDES THE WORK "AS IS," WITHOUT ANY EXPRESS OR IMPLIED WARRANTY,
  INCLUDING WITHOUT LIMITATION THE IMPLIED WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT. NOVELL, THE AUTHORS
  OF THE WORK, AND THE OWNERS OF COPYRIGHT IN THE WORK ARE NOT LIABLE FOR ANY
  CLAIM, DAMAGES, OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT,
  OR OTHERWISE, ARISING FROM, OUT OF, OR IN CONNECTION WITH THE WORK OR THE
  USE OR OTHER DEALINGS IN THE WORK.$

 *****************************************************************************/


/*
 * This file contains prototypes for library function calls that are being
 * deprecated. They are preserved here in the interest of all legacy software
 * that depends on them. Please update such software to use the preferred API
 * calls.
 *
 * DO NOT INCLUDE THIS HEADER EXPLICITLY!!
 *
 * Include "nwdirect.h" and use a compiler switch to define INCLUDE_OBSOLETE
 * (i.e. -DINCLUDE_OBSOLETE).
 */

#ifndef _OBSOLETE_NWDIRECT_H
#define _OBSOLETE_NWDIRECT_H

#ifdef __cplusplus
extern "C" {
#endif

#define NWScanDirectoryInformation2(a, b, c, d, e, f, g, h) \
        NWIntScanDirectoryInformation2(a, b, c, d, e, f, g, h, 0)


N_EXTERN_LIBRARY( NWCCODE )
NWSaveDirectoryHandle
(
   NWCONN_HANDLE  conn,
   NWDIR_HANDLE   dirHandle,
   pnstr8         saveBuffer
);

N_EXTERN_LIBRARY( NWCCODE )
NWRestoreDirectoryHandle
(
   NWCONN_HANDLE        conn,
   const nstr8  N_FAR * saveBuffer,
   NWDIR_HANDLE N_FAR * newDirHandle,
   pnuint8              rightsMask
);

#ifdef __cplusplus
}
#endif

#endif   /* _OBSOLETE_NWDIRECT_H */
