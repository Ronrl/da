/******************************************************************************

  %name: o_file.h %
  %version: 4 %
  %date_modified: Fri Jun 15 10:24:30 2007 %
  $Copyright:

  Copyright (c) 1999-2007 Novell, Inc.  All Rights Reserved.

  USE AND REDISTRIBUTION OF THIS WORK IS SUBJECT TO THE DEVELOPER LICENSE
  AGREEMENT OR OTHER AGREEMENT THROUGH WHICH NOVELL, INC. MAKES THE WORK
  AVAILABLE. THIS WORK MAY NOT BE ADAPTED WITHOUT NOVELL'S PRIOR WRITTEN
  CONSENT.

  NOVELL PROVIDES THE WORK "AS IS," WITHOUT ANY EXPRESS OR IMPLIED WARRANTY,
  INCLUDING WITHOUT LIMITATION THE IMPLIED WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT. NOVELL, THE AUTHORS
  OF THE WORK, AND THE OWNERS OF COPYRIGHT IN THE WORK ARE NOT LIABLE FOR ANY
  CLAIM, DAMAGES, OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT,
  OR OTHERWISE, ARISING FROM, OUT OF, OR IN CONNECTION WITH THE WORK OR THE
  USE OR OTHER DEALINGS IN THE WORK.$

 *****************************************************************************/


/*
 * This file contains prototypes for library function calls that are being
 * deprecated. They are preserved here in the interest of all legacy software 
 * that depends on them. Please update such software to use the preferred API 
 * calls. 
 *
 * DO NOT INCLUDE THIS HEADER EXPLICITLY!!
 *
 * Include "nwconnec.h" and use a compiler switch to define INCLUDE_OBSOLETE
 * (i.e. -DINCLUDE_OBSOLETE).
 */

#ifndef _OBSOLETE_NWFILE_H
#define _OBSOLETE_NWFILE_H

#define NWScanFileInformation2(a, b, c, d, e, f) \
        NWIntScanFileInformation2(a, b, c, d, e, f, 0)

#define NWFileSearchContinue(a, b, c, d, e, f, g) \
        NWIntFileSearchContinue(a, b, c, d, e, f, g, 0)

#define NWEraseFiles(a, b, c, d) \
        NWIntEraseFiles(a, b, c, d, 0)

#ifdef __cplusplus
extern "C" {
#endif

/* Obsolete function prototypes */

#ifdef __cplusplus
}
#endif

#endif
