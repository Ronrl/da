/******************************************************************************

  %name: nwdsasa.h %
  %version: 11 %
  %date_modified: Fri Jun 15 10:23:25 2007 %
  $Copyright:

  Copyright (c) 1989-2007 Novell, Inc.  All Rights Reserved.

  USE AND REDISTRIBUTION OF THIS WORK IS SUBJECT TO THE DEVELOPER LICENSE
  AGREEMENT OR OTHER AGREEMENT THROUGH WHICH NOVELL, INC. MAKES THE WORK
  AVAILABLE. THIS WORK MAY NOT BE ADAPTED WITHOUT NOVELL'S PRIOR WRITTEN
  CONSENT.

  NOVELL PROVIDES THE WORK "AS IS," WITHOUT ANY EXPRESS OR IMPLIED WARRANTY,
  INCLUDING WITHOUT LIMITATION THE IMPLIED WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT. NOVELL, THE AUTHORS
  OF THE WORK, AND THE OWNERS OF COPYRIGHT IN THE WORK ARE NOT LIABLE FOR ANY
  CLAIM, DAMAGES, OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT,
  OR OTHERWISE, ARISING FROM, OUT OF, OR IN CONNECTION WITH THE WORK OR THE
  USE OR OTHER DEALINGS IN THE WORK.$

 *****************************************************************************/

#if ! defined ( NWDSASA_H )
#define NWDSASA_H

#if ! defined ( NTYPES_H )
#include "ntypes.h"
#endif

#if ! defined ( NWCALDEF_H )
#include "nwcaldef.h"
#endif

#if ! defined ( NWDSDC_H )
#include "nwdsdc.h"     /* for NWDSContextHandle typedef */
#endif

#include "npackon.h"

#define SESSION_KEY_SIZE   16
typedef nuint8 NWDS_Session_Key_T[SESSION_KEY_SIZE];  /* Optional session key */
typedef NWDS_Session_Key_T N_FAR *  pNWDS_Session_Key_T;

#ifdef __cplusplus
extern "C" {
#endif

N_EXTERN_LIBRARY (NWDSCCODE)
NWDSAuthenticateConn
(
   NWDSContextHandle context,
   NWCONN_HANDLE     connHandle
);

N_EXTERN_LIBRARY (NWDSCCODE)
NWDSAuthenticateConnEx
(
   NWDSContextHandle context,
   NWCONN_HANDLE     connHandle
);

N_EXTERN_LIBRARY (NWDSCCODE)
NWDSChangeObjectPassword
(
   NWDSContextHandle context,
   nflag32           pwdOption,
   pnstr8            objectName,
   pnstr8            oldPassword,
   pnstr8            newPassword
);

N_EXTERN_LIBRARY (NWDSCCODE)
NWDSGenerateObjectKeyPair
(
   NWDSContextHandle contextHandle,
   pnstr8            objectName,
   pnstr8            objectPassword,
   nflag32           pwdOption
);

N_EXTERN_LIBRARY (NWDSCCODE)
NWDSGenerateObjectKeyPair2
(
   NWDSContextHandle  context,
   pnstr8             objectName,
   nuint32            pseudoID,
   nuint32            pwdLen,
   pnstr8             pwdHash,
   nuint32            optionsFlag
);

N_EXTERN_LIBRARY (NWDSCCODE)
NWDSLogin
(
   NWDSContextHandle context,
   nflag32           optionsFlag,
   pnstr8            objectName,
   pnstr8            password,
   nuint32           validityPeriod
);

N_EXTERN_LIBRARY (NWDSCCODE)
NWDSLogout
(
   NWDSContextHandle context
);

N_EXTERN_LIBRARY (NWDSCCODE)
NWDSVerifyObjectPassword
(
   NWDSContextHandle context,
   nflag32           optionsFlag,
   pnstr8            objectName,
   pnstr8            password
);


   /* The following APIs support extended and international characters in
    * passwords - see nwdsdefs.h for a list of supported password 
    * formats
   */
N_EXTERN_LIBRARY (NWDSCCODE)
NWDSGenerateKeyPairEx
(
   NWDSContextHandle context,
   pnstr8            objectName,
   nuint32           pwdFormat,
   nptr              pwd,
   nuint32           pwdOption
);

N_EXTERN_LIBRARY (NWDSCCODE)
NWDSChangePwdEx
(
   NWDSContextHandle context,
   pnstr8            objectName,
   nuint32           pwdFormat,
   nptr              oldPwd,
   nptr              newPwd,
   nuint32           pwdOption
);

N_EXTERN_LIBRARY (NWDSCCODE)
NWDSLoginEx
(
   NWDSContextHandle context,
   pnstr8            objectName,
   nuint32           pwdFormat,
   nptr              pwd
);

N_EXTERN_LIBRARY (NWDSCCODE)
NWDSVerifyPwdEx
(
   NWDSContextHandle context,
   pnstr8            objectName,
   nuint32           pwdFormat,
   nptr              pwd
);


#if defined( N_PLAT_NLM )
N_GLOBAL_LIBRARY( NWCCODE )
NWDSLoginAsServer
(
	NWDSContextHandle       context
);
#endif

#ifdef __cplusplus
}
#endif


   /* The NLM LibC x-plat libraries do not support obsolete apis
   */
#include <stddef.h>
#if !defined(__NOVELL_LIBC__)
   #ifdef INCLUDE_OBSOLETE
      #include "obsolete/o_dsasa.h"
   #endif
#endif /* !defined(__NOVELL_LIBC__) */

#include "npackoff.h"
#endif
