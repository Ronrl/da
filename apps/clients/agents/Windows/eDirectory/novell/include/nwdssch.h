/******************************************************************************

  %name: nwdssch.h %
  %version: 6 %
  %date_modified: Fri Jun 15 10:23:40 2007 %
  $Copyright:

  Copyright (c) 1989-2007 Novell, Inc.  All Rights Reserved.

  USE AND REDISTRIBUTION OF THIS WORK IS SUBJECT TO THE DEVELOPER LICENSE
  AGREEMENT OR OTHER AGREEMENT THROUGH WHICH NOVELL, INC. MAKES THE WORK
  AVAILABLE. THIS WORK MAY NOT BE ADAPTED WITHOUT NOVELL'S PRIOR WRITTEN
  CONSENT.

  NOVELL PROVIDES THE WORK "AS IS," WITHOUT ANY EXPRESS OR IMPLIED WARRANTY,
  INCLUDING WITHOUT LIMITATION THE IMPLIED WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT. NOVELL, THE AUTHORS
  OF THE WORK, AND THE OWNERS OF COPYRIGHT IN THE WORK ARE NOT LIABLE FOR ANY
  CLAIM, DAMAGES, OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT,
  OR OTHERWISE, ARISING FROM, OUT OF, OR IN CONNECTION WITH THE WORK OR THE
  USE OR OTHER DEALINGS IN THE WORK.$

 *****************************************************************************/

#if ! defined ( NWDSSCH_H )
#define NWDSSCH_H

#if ! defined ( NTYPES_H )
#include "ntypes.h"
#endif

#if ! defined ( NWDSTYPE_H )
#include "nwdstype.h"
#endif

#if ! defined ( NWDSBUFT_H )
#include "nwdsbuft.h"
#endif

#if ! defined ( NWDSATTR_H ) 
#include "nwdsattr.h"
#endif


#ifdef __cplusplus
extern "C" {
#endif

N_EXTERN_LIBRARY (NWDSCCODE)
NWDSDefineAttr
(
   NWDSContextHandle context,
   pnstr8            attrName,
   pAttr_Info_T      attrDef
);

N_EXTERN_LIBRARY (NWDSCCODE)
NWDSDefineClass
(
   NWDSContextHandle context,
   pnstr8            className,
   pClass_Info_T     classInfo,
   pBuf_T            classItems
);

N_EXTERN_LIBRARY (NWDSCCODE)
NWDSListContainableClasses
(
   NWDSContextHandle context,
   pnstr8            parentObject,
   pnint_ptr         iterationHandle,
   pBuf_T            containableClasses
);

N_EXTERN_LIBRARY (NWDSCCODE)
NWDSModifyClassDef
(
   NWDSContextHandle context,
   pnstr8            className,
   pBuf_T            optionalAttrs
);

N_EXTERN_LIBRARY (NWDSCCODE)
NWDSReadAttrDef
(
   NWDSContextHandle context,
   nuint32           infoType,
   nbool8            allAttrs,
   pBuf_T            attrNames,
   pnint_ptr         iterationHandle,
   pBuf_T            attrDefs
);

N_EXTERN_LIBRARY (NWDSCCODE)
NWDSReadClassDef
(
   NWDSContextHandle context,
   nuint32           infoType,
   nbool8            allClasses,
   pBuf_T            classNames,
   pnint_ptr         iterationHandle,
   pBuf_T            classDefs
);

N_EXTERN_LIBRARY (NWDSCCODE)
NWDSRemoveAttrDef
(
   NWDSContextHandle context,
   pnstr8            attrName
);

N_EXTERN_LIBRARY (NWDSCCODE)
NWDSRemoveClassDef
(
   NWDSContextHandle context,
   pnstr8            className
);

N_EXTERN_LIBRARY (NWDSCCODE)
NWDSSyncSchema
(
   NWDSContextHandle context,
   pnstr8            server,
   nuint32           seconds
);

#ifdef __cplusplus
}
#endif
#endif /* NWDSSCH_H */
