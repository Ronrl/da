/******************************************************************************

  %name: nwdsacl.h %
  %version: 5 %
  %date_modified: Fri Jun 15 10:23:22 2007 %
  $Copyright:

  Copyright (c) 1989-2007 Novell, Inc.  All Rights Reserved.

  USE AND REDISTRIBUTION OF THIS WORK IS SUBJECT TO THE DEVELOPER LICENSE
  AGREEMENT OR OTHER AGREEMENT THROUGH WHICH NOVELL, INC. MAKES THE WORK
  AVAILABLE. THIS WORK MAY NOT BE ADAPTED WITHOUT NOVELL'S PRIOR WRITTEN
  CONSENT.

  NOVELL PROVIDES THE WORK "AS IS," WITHOUT ANY EXPRESS OR IMPLIED WARRANTY,
  INCLUDING WITHOUT LIMITATION THE IMPLIED WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT. NOVELL, THE AUTHORS
  OF THE WORK, AND THE OWNERS OF COPYRIGHT IN THE WORK ARE NOT LIABLE FOR ANY
  CLAIM, DAMAGES, OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT,
  OR OTHERWISE, ARISING FROM, OUT OF, OR IN CONNECTION WITH THE WORK OR THE
  USE OR OTHER DEALINGS IN THE WORK.$

 *****************************************************************************/

#if ! defined ( NWDSACL_H )
#define NWDSACL_H

#if ! defined ( NTYPES_H )
#include "ntypes.h"
#endif

#if! defined ( NWDSTYPE_H )
#include "nwdstype.h"
#endif

#if ! defined ( NWDSBUFT_H ) /* Needed to defined pBuf_T */
#include "nwdsbuft.h"
#endif

#if ! defined ( NWDSDC_H )   /* Needed to defined NWDSContextHandle */
#include "nwdsdc.h"
#endif

#include "npackon.h"

#ifdef __cplusplus
   extern "C" {
#endif

N_EXTERN_LIBRARY (NWDSCCODE)
NWDSGetEffectiveRights
(
   NWDSContextHandle context,
   pnstr8            subjectName,
   pnstr8            objectName,
   pnstr8            attrName,
   pnuint32          privileges
);

N_EXTERN_LIBRARY (NWDSCCODE)
NWDSListAttrsEffectiveRights
(
   NWDSContextHandle context,
   pnstr8            objectName,
   pnstr8            subjectName,
   nbool8            allAttrs,
   pBuf_T            attrNames,
   pnint_ptr         iterationHandle,
   pBuf_T            privilegeInfo
);

#ifdef __cplusplus
   }
#endif

#include "npackoff.h"
#endif   /* NWDSACL_H */
