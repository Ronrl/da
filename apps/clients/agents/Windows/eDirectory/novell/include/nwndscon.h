/******************************************************************************

  %name: nwndscon.h %
  %version: 10 %
  %date_modified: Fri Jun 15 10:23:53 2007 %
  $Copyright:

  Copyright (c) 1989-2007 Novell, Inc.  All Rights Reserved.

  USE AND REDISTRIBUTION OF THIS WORK IS SUBJECT TO THE DEVELOPER LICENSE
  AGREEMENT OR OTHER AGREEMENT THROUGH WHICH NOVELL, INC. MAKES THE WORK
  AVAILABLE. THIS WORK MAY NOT BE ADAPTED WITHOUT NOVELL'S PRIOR WRITTEN
  CONSENT.

  NOVELL PROVIDES THE WORK "AS IS," WITHOUT ANY EXPRESS OR IMPLIED WARRANTY,
  INCLUDING WITHOUT LIMITATION THE IMPLIED WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT. NOVELL, THE AUTHORS
  OF THE WORK, AND THE OWNERS OF COPYRIGHT IN THE WORK ARE NOT LIABLE FOR ANY
  CLAIM, DAMAGES, OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT,
  OR OTHERWISE, ARISING FROM, OUT OF, OR IN CONNECTION WITH THE WORK OR THE
  USE OR OTHER DEALINGS IN THE WORK.$

 *****************************************************************************/

#if ! defined ( NWNDSCON_H )
#define NWNDSCON_H

#if ! defined ( NTYPES_H )
#include "ntypes.h"
#endif

#if ! defined ( NWCALDEF_H )
#include "nwcaldef.h"
#endif

#define NWNDS_CONNECTION         0x0001
#define NWNDS_LICENSED           0x0002
#define NWNDS_AUTHENTICATED      0x0004
#define NWNDS_PACKET_BURST_AVAIL 0x0001
#define NWNDS_NEEDED_MAX_IO      0x0040
#define SYSTEM_LOCK              0x0
#define TASK_LOCK                0x4
#define SYSTEM_DISCONNECT        0x0
#define TASK_DISCONNECT          0x1

#define ALLREADY_ATTACHED        0x1
#define ATTACHED_NOT_AUTH        0X2
#define ATTACHED_AND_AUTH        0X4

#ifdef __cplusplus
   extern "C" {
#endif


N_EXTERN_LIBRARY (NWCCODE)
NWSetPreferredDSTree
(
   nuint16  length,
   pnuint8  treeName
);


#ifdef __cplusplus
   }
#endif

   /* The NLM LibC x-plat libraries do not support obsolete apis
   */
#include <stddef.h>
#if !defined(__NOVELL_LIBC__)
   #ifdef INCLUDE_OBSOLETE
      #include "obsolete/o_ndscon.h"
   #endif
#endif /* !defined(__NOVELL_LIBC__) */

#endif /* NWNDSCON_H */
