/******************************************************************************

  %name: nwconnec.h %
  %version: 13 %
  %date_modified: Fri Jun 15 10:23:18 2007 %
  $Copyright:

  Copyright (c) 1989-2007 Novell, Inc.  All Rights Reserved.

  USE AND REDISTRIBUTION OF THIS WORK IS SUBJECT TO THE DEVELOPER LICENSE
  AGREEMENT OR OTHER AGREEMENT THROUGH WHICH NOVELL, INC. MAKES THE WORK
  AVAILABLE. THIS WORK MAY NOT BE ADAPTED WITHOUT NOVELL'S PRIOR WRITTEN
  CONSENT.

  NOVELL PROVIDES THE WORK "AS IS," WITHOUT ANY EXPRESS OR IMPLIED WARRANTY,
  INCLUDING WITHOUT LIMITATION THE IMPLIED WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT. NOVELL, THE AUTHORS
  OF THE WORK, AND THE OWNERS OF COPYRIGHT IN THE WORK ARE NOT LIABLE FOR ANY
  CLAIM, DAMAGES, OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT,
  OR OTHERWISE, ARISING FROM, OUT OF, OR IN CONNECTION WITH THE WORK OR THE
  USE OR OTHER DEALINGS IN THE WORK.$

 *****************************************************************************/

#if ! defined ( NWCONNECT_H )
#define NWCONNECT_H

#if ! defined ( NWCALDEF_H )
# include "nwcaldef.h"
#endif

#include "npackon.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct tNWINET_ADDR
{
  nuint8   networkAddr[4];
  nuint8   netNodeAddr[6];
  nuint16  socket;
  nuint16  connType;  /* 3.11 and above only: 0=not in use, 2=NCP over IPX, 4=AFP */
} NWINET_ADDR;

#define CONNECTION_AVAILABLE            0x0001
#define CONNECTION_PRIVATE              0x0002  /* obsolete */
#define CONNECTION_LOGGED_IN            0x0004
#define CONNECTION_LICENSED             0x0004
#define CONNECTION_BROADCAST_AVAILABLE  0x0008
#define CONNECTION_ABORTED              0x0010
#define CONNECTION_REFUSE_GEN_BROADCAST 0x0020
#define CONNECTION_BROADCASTS_DISABLED  0x0040
#define CONNECTION_PRIMARY              0x0080
#define CONNECTION_NDS                  0x0100
#define CONNECTION_PNW                  0x4000  /* obsolete */
#define CONNECTION_AUTHENTICATED        0x8000


/* End of new connection model calls. */

N_EXTERN_LIBRARY( NWCCODE )
NWLockConnection
(
   NWCONN_HANDLE connHandle
);

N_EXTERN_LIBRARY( NWCCODE )
NWGetConnectionInformation
(
   NWCONN_HANDLE  connHandle,
   nuint16        connNumber,
   pnstr8         pObjName,
   pnuint16       pObjType,
   pnuint32       pObjID,
   pnuint8        pLoginTime
);

N_EXTERN_LIBRARY( NWCCODE )
NWGetInternetAddress
(
   NWCONN_HANDLE connHandle,
   nuint16        connNumber,
   pnuint8       pInetAddr
);

N_EXTERN_LIBRARY( NWCCODE )
NWGetInetAddr
(
   NWCONN_HANDLE  connHandle,
   nuint16        connNum,
   NWINET_ADDR N_FAR * pInetAddr
);

N_EXTERN_LIBRARY( NWCCODE )
NWClearConnectionNumber
(
   NWCONN_HANDLE  connHandle,
   nuint16        connNumber
);

N_EXTERN_LIBRARY( NWCCODE )
NWGetDefaultConnRef
(
   pnuint32 pConnReference
);

N_EXTERN_LIBRARY( NWCCODE )
NWGetObjectConnectionNumbers
(
   NWCONN_HANDLE        connHandle,
   const nstr8 N_FAR *  pObjName,
   nuint16              objType,
   pnuint16             pNumConns,
   pnuint16             pConnHandleList,
   nuint16              maxConns
);

N_EXTERN_LIBRARY( NWCCODE )
NWGetConnListFromObject
(
   NWCONN_HANDLE  connHandle,
   nuint32        objID,
   nuint32        searchConnNum,
   pnuint16       pConnListLen,
   pnuint32       pConnList
);

N_EXTERN_LIBRARY( NWCCODE )
NWGetPreferredServer
(
   NWCONN_HANDLE N_FAR * pConnHandle
);

N_EXTERN_LIBRARY( NWCCODE )
NWSetPreferredServer
(
   NWCONN_HANDLE connHandle
);

#ifdef __cplusplus
}
#endif

   /* The NLM LibC x-plat libraries do not support obsolete apis
   */
#include <stddef.h>
#if !defined(__NOVELL_LIBC__)
   #ifdef INCLUDE_OBSOLETE
      #include "obsolete/o_connec.h"
   #endif
#endif /* !defined(__NOVELL_LIBC__) */

#include "npackoff.h"
#endif   /* NWCONNECT_H */
