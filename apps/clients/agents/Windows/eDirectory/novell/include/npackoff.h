/******************************************************************************

  %name: npackoff.h %
  %version: 6 %
  %date_modified: Fri Jun 15 10:23:17 2007 %
  $Copyright:

  Copyright (c) 1989-2007 Novell, Inc.  All Rights Reserved.

  USE AND REDISTRIBUTION OF THIS WORK IS SUBJECT TO THE DEVELOPER LICENSE
  AGREEMENT OR OTHER AGREEMENT THROUGH WHICH NOVELL, INC. MAKES THE WORK
  AVAILABLE. THIS WORK MAY NOT BE ADAPTED WITHOUT NOVELL'S PRIOR WRITTEN
  CONSENT.

  NOVELL PROVIDES THE WORK "AS IS," WITHOUT ANY EXPRESS OR IMPLIED WARRANTY,
  INCLUDING WITHOUT LIMITATION THE IMPLIED WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT. NOVELL, THE AUTHORS
  OF THE WORK, AND THE OWNERS OF COPYRIGHT IN THE WORK ARE NOT LIABLE FOR ANY
  CLAIM, DAMAGES, OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT,
  OR OTHERWISE, ARISING FROM, OUT OF, OR IN CONNECTION WITH THE WORK OR THE
  USE OR OTHER DEALINGS IN THE WORK.$

 *****************************************************************************/

/* this header sets packing back to default for different compilers */

#if defined (__WATCOMC__)
# if (__WATCOMC__ >= 1050)
#  pragma pack(pop)
# else
#  pragma pack()
# endif
#elif defined (__MWERKS__)
# if (__MWERKS__ >= 0x2100)
#  pragma pack(pop)
# else
#  pragma pack()
# endif
#elif defined(__ECC__) || defined(__ECPP__)
# pragma pack(pop)
#elif defined (_MSC_VER)
# if (_MSC_VER >= 900)
#  pragma pack(pop)
# else
#  pragma pack()
# endif
#elif defined (N_PLAT_UNIX)
#else
# pragma pack()
#endif


#ifdef N_PACK_1
#undef N_PACK_1
#endif

