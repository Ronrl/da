/******************************************************************************

  %name: npackon.h %
  %version: 6 %
  %date_modified: Fri Jun 15 10:23:17 2007 %
  $Copyright:

  Copyright (c) 1989-2007 Novell, Inc.  All Rights Reserved.

  USE AND REDISTRIBUTION OF THIS WORK IS SUBJECT TO THE DEVELOPER LICENSE
  AGREEMENT OR OTHER AGREEMENT THROUGH WHICH NOVELL, INC. MAKES THE WORK
  AVAILABLE. THIS WORK MAY NOT BE ADAPTED WITHOUT NOVELL'S PRIOR WRITTEN
  CONSENT.

  NOVELL PROVIDES THE WORK "AS IS," WITHOUT ANY EXPRESS OR IMPLIED WARRANTY,
  INCLUDING WITHOUT LIMITATION THE IMPLIED WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT. NOVELL, THE AUTHORS
  OF THE WORK, AND THE OWNERS OF COPYRIGHT IN THE WORK ARE NOT LIABLE FOR ANY
  CLAIM, DAMAGES, OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT,
  OR OTHERWISE, ARISING FROM, OUT OF, OR IN CONNECTION WITH THE WORK OR THE
  USE OR OTHER DEALINGS IN THE WORK.$

 *****************************************************************************/

/* this header sets packing to 1 for different compilers */

#if defined (_MSC_VER) && !defined(__BORLANDC__)
# if (_MSC_VER > 600)
#  pragma warning(disable:4103)
# endif
#elif defined (__BORLANDC__)
# if (__BORLANDC__ >= 0x500)
#  pragma warn -pck
# endif
#endif

#if defined (__BORLANDC__)
# if (__BORLANDC__ >= 0x500)
#  pragma pack(push)
# endif
#elif defined (__WATCOMC__)
# if (__WATCOMC__ >= 1050)
#  pragma pack(push)
# endif
#elif defined (__MWERKS__)
# if (__MWERKS__ >= 0x2100)
#  pragma pack(push)
# endif
#elif defined(__ECC__) || defined(__ECPP__)
# pragma pack(push)
#elif defined (_MSC_VER) 
# if (_MSC_VER >= 900)
#  pragma pack(push)
# endif
#endif

#if defined(N_PLAT_NLM)\
	|| defined(N_PACK_1)\
	|| defined(N_FORCE_INT_16)

# pragma pack(1)

#elif defined(N_PLAT_UNIX) || defined(N_ARCH_64)
/* let compiler decide default packing per CPU type */
#else

# pragma pack(4)

#endif
