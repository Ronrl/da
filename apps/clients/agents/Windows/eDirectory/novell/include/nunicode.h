/******************************************************************************

  %name: nunicode.h %
  %version: 31 %
  %date_modified: Fri Jun 15 10:23:05 2007 %
  $Copyright:

  Copyright (c) 1989-2007 Novell, Inc.  All Rights Reserved.

  USE AND REDISTRIBUTION OF THIS WORK IS SUBJECT TO THE DEVELOPER LICENSE
  AGREEMENT OR OTHER AGREEMENT THROUGH WHICH NOVELL, INC. MAKES THE WORK
  AVAILABLE. THIS WORK MAY NOT BE ADAPTED WITHOUT NOVELL'S PRIOR WRITTEN
  CONSENT.

  NOVELL PROVIDES THE WORK "AS IS," WITHOUT ANY EXPRESS OR IMPLIED WARRANTY,
  INCLUDING WITHOUT LIMITATION THE IMPLIED WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT. NOVELL, THE AUTHORS
  OF THE WORK, AND THE OWNERS OF COPYRIGHT IN THE WORK ARE NOT LIABLE FOR ANY
  CLAIM, DAMAGES, OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT,
  OR OTHERWISE, ARISING FROM, OUT OF, OR IN CONNECTION WITH THE WORK OR THE
  USE OR OTHER DEALINGS IN THE WORK.$

 *****************************************************************************/

#if !defined(NUNICODE_H)
#define NUNICODE_H


#include <stddef.h>

   /* For LIBC builds the XPlat libraries use the LIBC unicode and 
    * localization support.  LIBC is the next generation of the c-runtime
    * on NetWare.  All other platforms will continue to use unicode.h
    * NOTE:  stddef.h in the LIBC sdk defines __NOVELL_LIBC__ 
    *  
   */
#if defined(__NOVELL_LIBC__)
   
   #include "unilib.h"

      /* unilib.h doesn't define the following with are used significantly
       * in the XPlat SDK.  Define them for the XPlat SDK.
      */

      /* NOTE:  LibC WinSock2 #defines "unicode" inside ws2defs.h.  
       * If LibC WinSock2 headers have been included, undefine unicode
       * and typdef it the way XPlat SDK expects it.
      */
   #if defined(unicode) 
      #undef unicode
   #endif
   
   #ifndef UNICODE_TYPE_DEFINED
   #define UNICODE_TYPE_DEFINED
   typedef unicode_t    unicode;  /* use LibC's unicode_t type */  
   #endif 

   typedef unicode *  punicode;
   typedef unicode ** ppunicode;

#else /* All non-LibC builds */
   
   #include "unicode.h"

#endif /* #if defined(__NOVELL_LIBC__) */ 
					 

#endif /* #if !defined(NUNICODE_H) */
