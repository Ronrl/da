/******************************************************************************

  %name: nwcalls.h %
  %version: 5 %
  %date_modified: Fri Jun 15 10:23:15 2007 %
  $Copyright:

  Copyright (c) 1989-2007 Novell, Inc.  All Rights Reserved.

  USE AND REDISTRIBUTION OF THIS WORK IS SUBJECT TO THE DEVELOPER LICENSE
  AGREEMENT OR OTHER AGREEMENT THROUGH WHICH NOVELL, INC. MAKES THE WORK
  AVAILABLE. THIS WORK MAY NOT BE ADAPTED WITHOUT NOVELL'S PRIOR WRITTEN
  CONSENT.

  NOVELL PROVIDES THE WORK "AS IS," WITHOUT ANY EXPRESS OR IMPLIED WARRANTY,
  INCLUDING WITHOUT LIMITATION THE IMPLIED WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT. NOVELL, THE AUTHORS
  OF THE WORK, AND THE OWNERS OF COPYRIGHT IN THE WORK ARE NOT LIABLE FOR ANY
  CLAIM, DAMAGES, OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT,
  OR OTHERWISE, ARISING FROM, OUT OF, OR IN CONNECTION WITH THE WORK OR THE
  USE OR OTHER DEALINGS IN THE WORK.$

 *****************************************************************************/

#if ! defined ( NWCALLS_H )
#define NWCALLS_H

#if ! defined ( NWAPIDEF_H )
#include "nwapidef.h"
#endif

#if ! defined ( NWCALDEF_H )
#include "nwcaldef.h"
#endif

#if ! defined ( NWALIAS_H )
#include "nwalias.h"
#endif

#if ! defined ( NOAFP_INC )
#include "nwafp.h"
#endif

#if ! defined ( NOBINDRY_INC )
#include "nwbindry.h"
#endif

#if ! defined ( NOCONNECT_INC )
#include "nwconnec.h"
#endif

#if ! defined ( NODEL_INC )
#include "nwdel.h"
#endif

#if ! defined ( NODENTRY_INC )
#include "nwdentry.h"
#endif

#if ! defined ( NODIRECTORY_INC )
#include "nwdirect.h"
#endif

#if ! defined ( NODPATH_INC )
#include "nwdpath.h"
#endif

#if ! defined ( NOEA_INC )
#include "nwea.h"
#endif

#if ! defined ( NOERROR_INC )
#include "nwerror.h"
#endif

#if ! defined ( NOFILES_INC )
#include "nwfile.h"
#endif

#if ! defined ( NOMISC_INC )
#include "nwmisc.h"
#endif

#if ! defined ( NOMESSAGES_INC )
#include "nwmsg.h"
#endif

#if ! defined ( NONAMSPC_INC )
#include "nwnamspc.h"
#endif

#if ! defined ( NOPRINT_INC )
#include "nwprint.h"
#endif

#if ! defined ( NOQUEUE_INC )
#include "nwqms.h"
#endif

#if ! defined ( NOSERVER_INC )
#include "nwserver.h"
#endif

#if ! defined ( NOSYNC_INC )
#include "nwsync.h"
#endif

#if ! defined ( NONTTS_INC )
#include "nwtts.h"
#endif

#if ! defined ( NOVOL_INC )
#include "nwvol.h"
#endif

#if ! defined ( NOACCT_INC )
#include "nwacct.h"
#endif

#if ! defined ( NOFSE_INC )
#include "nwfse.h"
#endif

#if ! defined ( NOMIGRATE_INC )
#include "nwmigrat.h"
#endif

#if ! defined ( NOSM_INC )
#include "nwsm.h"
#endif

#endif /* NWCALLS_INC */
