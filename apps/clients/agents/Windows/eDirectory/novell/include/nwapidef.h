/******************************************************************************

  %name: nwapidef.h %
  %version: 5.1.1 %
  %date_modified: Fri Jun 15 10:23:11 2007 %
  $Copyright:

  Copyright (c) 1989-2007 Novell, Inc.  All Rights Reserved.

  USE AND REDISTRIBUTION OF THIS WORK IS SUBJECT TO THE DEVELOPER LICENSE
  AGREEMENT OR OTHER AGREEMENT THROUGH WHICH NOVELL, INC. MAKES THE WORK
  AVAILABLE. THIS WORK MAY NOT BE ADAPTED WITHOUT NOVELL'S PRIOR WRITTEN
  CONSENT.

  NOVELL PROVIDES THE WORK "AS IS," WITHOUT ANY EXPRESS OR IMPLIED WARRANTY,
  INCLUDING WITHOUT LIMITATION THE IMPLIED WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT. NOVELL, THE AUTHORS
  OF THE WORK, AND THE OWNERS OF COPYRIGHT IN THE WORK ARE NOT LIABLE FOR ANY
  CLAIM, DAMAGES, OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT,
  OR OTHERWISE, ARISING FROM, OUT OF, OR IN CONNECTION WITH THE WORK OR THE
  USE OR OTHER DEALINGS IN THE WORK.$

 *****************************************************************************/

#if ! defined ( NWAPIDEF_H )
#define NWAPIDEF_H

/* Miscellaneous string lengths (constant) */
/* NOTE: These max values include a byte for null  */
#define NW_MAX_USER_NAME_LEN              49
#define NW_MAX_VOLUME_NAME_LEN            17
#define NW_MAX_SERVER_NAME_LEN            49
#define NW_MAX_TREE_NAME_LEN              33
#define NW_MAX_SERVICE_TYPE_LEN           49

/* Miscellaneous unicode string sizes in bytes (constant) */

#define NW_MAX_USER_NAME_BYTES              2 * NW_MAX_USER_NAME_LEN
#define NW_MAX_VOLUME_NAME_BYTES            2 * NW_MAX_VOLUME_NAME_LEN
#define NW_MAX_SERVER_NAME_BYTES            2 * NW_MAX_SERVER_NAME_LEN
#define NW_MAX_TREE_NAME_BYTES              2 * NW_MAX_TREE_NAME_LEN
#define NW_MAX_SERVICE_TYPE_BYTES           2 * NW_MAX_SERVICE_TYPE_LEN

/* PrintFlags (nuint16 value) */
#define NW_PRINT_FLAG_RELEASE             0x0001
#define NW_PRINT_FLAG_SUPPRESS_FF         0x0002
#define NW_PRINT_FLAG_TEXT_FILE           0x0004
#define NW_PRINT_FLAG_PRINT_BANNER        0x0008
#define NW_PRINT_FLAG_NOTIFY              0x0010

/* Print string lengths (constant) */
#define NW_MAX_JOBDESCR_LEN               50
#define NW_MAX_FORM_NAME_LEN              13
#define NW_MAX_BANNER_NAME_LEN            13
#define NW_MAX_QUEUE_NAME_LEN             65

/* Client Types : these are returned by NWGetClientType */
#define NW_NETX_SHELL   1
#define NW_VLM_REQ      2
#define NW_CLIENT32     3
#define NW_NT_REQ       4
#define NW_OS2_REQ      5
#define NW_NLM_REQ      6

#endif  /* NWAPIDEF_INC */
