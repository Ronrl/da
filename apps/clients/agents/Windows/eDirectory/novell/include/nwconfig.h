/******************************************************************************

  %name: nwconfig.h %
  %version: 4 %
  %date_modified: Fri Jun 15 10:23:17 2007 %
  $Copyright:

  Copyright (c) 1989-2007 Novell, Inc.  All Rights Reserved.

  USE AND REDISTRIBUTION OF THIS WORK IS SUBJECT TO THE DEVELOPER LICENSE
  AGREEMENT OR OTHER AGREEMENT THROUGH WHICH NOVELL, INC. MAKES THE WORK
  AVAILABLE. THIS WORK MAY NOT BE ADAPTED WITHOUT NOVELL'S PRIOR WRITTEN
  CONSENT.

  NOVELL PROVIDES THE WORK "AS IS," WITHOUT ANY EXPRESS OR IMPLIED WARRANTY,
  INCLUDING WITHOUT LIMITATION THE IMPLIED WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT. NOVELL, THE AUTHORS
  OF THE WORK, AND THE OWNERS OF COPYRIGHT IN THE WORK ARE NOT LIABLE FOR ANY
  CLAIM, DAMAGES, OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT,
  OR OTHERWISE, ARISING FROM, OUT OF, OR IN CONNECTION WITH THE WORK OR THE
  USE OR OTHER DEALINGS IN THE WORK.$

 *****************************************************************************/

#if ! defined ( NWCONFIG_H )
#define NWCONFIG_H

#include "npackon.h"

#define T_OPTIONAL    0x80

#define T_NUMBER      0x01
#define T_INDEX       0x02
#define T_STRING      0x03
#define T_HEX_STRING  0x04
#define T_HEX_NUMBER  0x05
#define T_LONG_NUMBER 0x06
#define T_LONG_HEX    0x07

#define T_SET_1       0x10
#define T_SET_2       0x11
#define T_SET_3       0x12
#define T_SET_4       0x13
#define T_SET_5       0x14
#define T_SET_6       0x15
#define T_SET_7       0x16
#define T_SET_8       0x17
#define T_SET_9       0x18
#define T_SET_10      0x19
#define T_SET_11      0x1A
#define T_SET_12      0x1B
#define T_SET_13      0x1C
#define T_SET_14      0x1D
#define T_SET_15      0x1E
#define T_SET_16      0x1F

#define MAX_PARAMETERS        8
#define MAX_SECTION_NAME_SIZE 32
#define MAX_VALUE_SIZE        80
#define MAX_SET_ELEMENTS      20

typedef struct
{
  int numberOfElements;
  int *elementCode;
  char N_FAR * N_FAR *elementName;
  int N_FAR *elementValue;
} SetTableStruct;

typedef struct
{
  int paramType;
  long defaultValue;
} TypeDefaultStruct;

typedef union
{
  char N_FAR *string;
  unsigned int number;
  unsigned long longNumber;
} PARAMETER_TABLE_TYPE;

typedef struct
{
  int keywordCode;
  char N_FAR *keyword;
  void (N_FAR *function)(PARAMETER_TABLE_TYPE N_FAR *);
  TypeDefaultStruct typeDefault[MAX_PARAMETERS];
} GrammarTableStruct;

#ifdef __cplusplus
extern "C" {
#endif
N_EXTERN_LIBRARY( int )
NWParseConfig(
  PCHAR configFile,
  PCHAR sectionName,
  UINT  sectionInstance,
  UINT  grammarTableSize,
  GrammarTableStruct N_FAR *grammarTable,
  SetTableStruct N_FAR *setTable);
#ifdef __cplusplus
}
#endif

#include "npackoff.h"
#endif
