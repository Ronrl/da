/******************************************************************************

  %name: nwitr.h %
  %version: 14 %
  %date_modified: Fri Jun 15 10:23:46 2007 %
  $Copyright:

  Copyright (c) 1998-2007 Novell, Inc.  All Rights Reserved.

  USE AND REDISTRIBUTION OF THIS WORK IS SUBJECT TO THE DEVELOPER LICENSE
  AGREEMENT OR OTHER AGREEMENT THROUGH WHICH NOVELL, INC. MAKES THE WORK
  AVAILABLE. THIS WORK MAY NOT BE ADAPTED WITHOUT NOVELL'S PRIOR WRITTEN
  CONSENT.

  NOVELL PROVIDES THE WORK "AS IS," WITHOUT ANY EXPRESS OR IMPLIED WARRANTY,
  INCLUDING WITHOUT LIMITATION THE IMPLIED WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT. NOVELL, THE AUTHORS
  OF THE WORK, AND THE OWNERS OF COPYRIGHT IN THE WORK ARE NOT LIABLE FOR ANY
  CLAIM, DAMAGES, OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT,
  OR OTHERWISE, ARISING FROM, OUT OF, OR IN CONNECTION WITH THE WORK OR THE
  USE OR OTHER DEALINGS IN THE WORK.$

 *****************************************************************************/

/* Public include file for Large Virtual List Iterator */

#if !defined( NWITR_H )
#define NWITR_H

#include "ntypes.h"
#include "nwdsbuft.h"

#define DS_ITR_FIRST     0          /* First entry position in list */
#define DS_ITR_LAST   1000          /* Last  entry position in list */
#define DS_ITR_EOF    1001          /* End-of-file position. */

#define DS_ITR_UNICODE_STRING    0  /* Indicates a unicode string */
#define DS_ITR_BYTE_STRING       2  /* Indicates a byte string */

#define DS_ITR_PREFER_SCALABLE   0  /* If can't get scalable, emulate [not supported in FCS] */
#define DS_ITR_REQUIRE_SCALABLE  1  /* If can't get scalable, return error */
#define DS_ITR_FORCE_EMULATION   2  /* Always force emulation mode */
#define DS_ITR_ANY_SERVER        3  /* Get any server */

N_EXTERN_LIBRARY (NWDSCCODE)
NWDSItrCreateList
(
   NWDSContextHandle context,
   pnstr8            baseObjectName,      /* Starting object to search */
   pnstr8            className,           /* Class name if List operation */
   pnstr8            subordinateName,     /* RDN if List operation */
   nuint32           scalability,         /* Require or prefer SKADS server */
   nuint32           timeout,             /* Timeout in milliseconds */
   pnuint_ptr        pIterator            /* Returned Iterator Ptr */
);

N_EXTERN_LIBRARY (NWDSCCODE)
NWDSItrCreateSearch
(
   NWDSContextHandle context,
   pnstr8            baseObjectName,      /* Starting object to search */
   nint              scope,               /* Object, immed subord or subtree */
   nbool8            searchAliases,       /* True to follow aliases */
   pBuf_T            filter,              /* Search filter */
   pTimeStamp_T      pTimeFilter,         /* Filter on modification time */
   nuint32           infoType,            /* Names only, or names and attrib */
   nbool8            allAttrs,            /* True = return all attributes */
   pBuf_T            attrNames,           /* List of attributes to return */
   pnstr8            indexSelect,         /* Index selection string */
   pnstr8            sortKey,             /* Attributes to sort on */
   nuint32           scalability,         /* Require or prefer SKADS server */
   nuint32           timeout,             /* Timeout in milliseconds */
   pnuint_ptr        pIterator            /* Returned Iterator Ptr */
);

N_EXTERN_LIBRARY (NWDSCCODE)
NWDSItrDestroy(nuint_ptr Iterator);

N_EXTERN_LIBRARY (NWDSCCODE)
NWDSItrClone(nuint_ptr Iterator, pnuint_ptr pNewIterator);

N_EXTERN_LIBRARY (nbool8)
NWDSItrAtFirst(nuint_ptr Iterator);

N_EXTERN_LIBRARY (nbool8)
NWDSItrAtEOF(nuint_ptr Iterator);

N_EXTERN_LIBRARY (NWDSCCODE)
NWDSItrGetInfo(nuint_ptr Iterator, pnbool8 pIsScalable, pnbool8 pIisPositionable);

N_EXTERN_LIBRARY (NWDSCCODE)
NWDSItrGetPosition(nuint_ptr Iterator, pnuint32 pPosition, nuint32 timeout);

N_EXTERN_LIBRARY (NWDSCCODE)
NWDSItrSetPosition(nuint_ptr Iterator, nuint32 position, nuint32 timeout);

N_EXTERN_LIBRARY (NWDSCCODE)
NWDSItrSetPositionFromIterator(nuint_ptr Iterator, nuint_ptr srcIterator,
                               nuint32 timeout);

N_EXTERN_LIBRARY (NWDSCCODE)
NWDSItrTypeDown(nuint_ptr Iterator, pnstr8 attrString, pnstr8 value,
                nuint32 byteUniFlag, nuint32 timeout);

N_EXTERN_LIBRARY (NWDSCCODE)
NWDSItrSkip(nuint_ptr Iterator, nint32 numToSkip, nuint32 timeout,
            pnint32 pNumSkipped);

N_EXTERN_LIBRARY (NWDSCCODE)
NWDSItrGetNext(nuint_ptr Iterator, nuint32 numEntries, nuint32 timeout,
               pnint32 pIterationHandle, pBuf_T pData);

N_EXTERN_LIBRARY (NWDSCCODE)
NWDSItrGetPrev(nuint_ptr Iterator, nuint32 numEntries, nuint32 timeout,
               pnint32 pIterationHandle, pBuf_T pData);

N_EXTERN_LIBRARY (NWDSCCODE)
NWDSItrGetCurrent(nuint_ptr Iterator, pnint32 pIterationHandle, pBuf_T pData);

N_EXTERN_LIBRARY (NWDSCCODE)
NWDSItrCount(nuint_ptr Iterator, nuint32 timeout, nuint32 maxCount,
             nbool8 updatePosition, pnuint32 pCount);

#endif
