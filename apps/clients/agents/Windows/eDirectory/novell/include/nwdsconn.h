/******************************************************************************

  %name: nwdsconn.h %
  %version: 4 %
  %date_modified: Fri Jun 15 10:23:29 2007 %
  $Copyright:

  Copyright (c) 1989-2007 Novell, Inc.  All Rights Reserved.

  USE AND REDISTRIBUTION OF THIS WORK IS SUBJECT TO THE DEVELOPER LICENSE
  AGREEMENT OR OTHER AGREEMENT THROUGH WHICH NOVELL, INC. MAKES THE WORK
  AVAILABLE. THIS WORK MAY NOT BE ADAPTED WITHOUT NOVELL'S PRIOR WRITTEN
  CONSENT.

  NOVELL PROVIDES THE WORK "AS IS," WITHOUT ANY EXPRESS OR IMPLIED WARRANTY,
  INCLUDING WITHOUT LIMITATION THE IMPLIED WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT. NOVELL, THE AUTHORS
  OF THE WORK, AND THE OWNERS OF COPYRIGHT IN THE WORK ARE NOT LIABLE FOR ANY
  CLAIM, DAMAGES, OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT,
  OR OTHERWISE, ARISING FROM, OUT OF, OR IN CONNECTION WITH THE WORK OR THE
  USE OR OTHER DEALINGS IN THE WORK.

 *****************************************************************************/

#if ! defined ( NWDSCONN_H )
#define NWDSCONN_H

#if ! defined ( NTYPES_H )
#include "ntypes.h"
#endif

#if ! defined ( NWCALDEF_H )
#include "nwcaldef.h"
#endif

#if ! defined ( NWDSDC_H )
#include "nwdsdc.h"
#endif

#ifdef __cplusplus
   extern "C" {
#endif


N_EXTERN_LIBRARY (NWDSCCODE)
NWDSOpenConnToNDSServer
(
   NWDSContextHandle context,
   pnstr8            serverName,
   pNWCONN_HANDLE    connHandle
);

N_EXTERN_LIBRARY (NWDSCCODE)
NWDSGetDefNameContext
(
   NWDSContextHandle context,
   nuint             nameContextLen,
   pnstr8            nameContext
);

N_EXTERN_LIBRARY (NWDSCCODE)
NWDSSetDefNameContext
(
   NWDSContextHandle context,
   nuint             nameContextLen,
   pnstr8            nameContext
);

N_EXTERN_LIBRARY (NWDSCCODE)
NWDSGetMonitoredConnRef
(
   NWDSContextHandle context,
   pnuint32          connRef
);

N_EXTERN_LIBRARY (NWDSCCODE)
NWDSOpenMonitoredConn
(
   NWDSContextHandle context,
   pNWCONN_HANDLE    connHandle
);

N_EXTERN_LIBRARY (NWDSCCODE)
NWDSScanConnsForTrees
(
   NWDSContextHandle context,
   nuint             numOfPtrs,
   pnuint            numOfTrees,
   ppnstr8           treeBufPtrs
);

N_EXTERN_LIBRARY (NWDSCCODE)
NWDSScanForAvailableTrees
(
   NWDSContextHandle context,
   NWCONN_HANDLE     connHandle,
   pnstr             scanFilter,
   pnint32           scanIndex,
   pnstr             treeName
);

N_EXTERN_LIBRARY (NWDSCCODE)
NWDSReturnBlockOfAvailableTrees
(
   NWDSContextHandle context,
   NWCONN_HANDLE     connHandle,
   pnstr             scanFilter,
   pnstr             lastBlocksString,
   pnstr             endBoundString,
   nuint32           maxTreeNames,           
   ppnstr            arrayOfNames,
   pnuint32          numberOfTrees,
   pnuint32          totalUniqueTrees
);

N_EXTERN_LIBRARY (NWDSCCODE)
NWDSCanDSAuthenticate
(
   NWDSContextHandle context
);


#ifdef __cplusplus
   }
#endif
#endif /* NWDSCONN_H */

