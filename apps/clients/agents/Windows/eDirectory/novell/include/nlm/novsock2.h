#ifndef __novsock2_h__
#define __novsock2_h__
/*============================================================================
=  Novell Software Development Kit
=
=  Copyright (C) 2002 Novell, Inc. All Rights Reserved.
=
=  This work is subject to U.S. and international copyright laws and treaties.
=  Use and redistribution of this work is subject  to  the  license  agreement
=  accompanying  the  software  development kit (SDK) that contains this work.
=  However, no part of this work may be revised and/or  modified  without  the
=  prior  written consent of Novell, Inc. Any use or exploitation of this work
=  without authorization could subject the perpetrator to criminal  and  civil
=  liability.
=
=  Source(s): Novell header
=
=  novsock2.h
==============================================================================
=  For use by CLib consumers
=
=  This header is sometimes included by components that have moved up to a
=  later version of the Novell WinSock 2 NDK. The consuming component may use
=  such a header, but still maintain allegiance to the CLib NDK in which case
=  this header includes ws2nlm.h, the WinSock 2 header in use by CLib code.
==============================================================================
*/
#include <ws2nlm.h>

#endif
