#ifndef _SYS_SOCKIO_H_
#define _SYS_SOCKIO_H_
/*============================================================================
=  NetWare C NLM Runtime Library source code
=
=  Unpublished Copyright (C) 1997 by Novell, Inc. All rights reserved.
=
=  (C) Copyright 1982, 1985, 1986 Regents of the University of California.
=  All rights reserved. The Berkeley software License Agreement specifies the
=  terms and conditions for redistribution.
=
=  No part of this file may be duplicated, revised, translated, localized or
=  modified in any manner or compiled, linked or uploaded or downloaded to or
=  from any computer system without the prior written consent of Novell, Inc.
=
=  sys/sockio.h
==============================================================================
*/

#define SIOCATMARK      8              /* at oob mark? */
#define SIOCDGRAMSIZE   500
#define IP_INBOUND_IF   501
#define IP_OUTBOUND_IF  502

#endif
