/*============================================================================
=
=  NetWare NLM Library source code
=
=  Unpublished Copyright (C) 1995 by Novell, Inc. All rights reserved.
=
=  No part of this file may be duplicated, revised, translated, localized or
=  modified in any manner or compiled, linked or uploaded or downloaded to or
=  from any computer system without the prior written consent of Novell, Inc.
=
=  fileeng.h
==============================================================================
*/
#ifdef _FIND_OLD_HEADERS_
# error This is an obsolete, Novell SDK header!
#else
# include <nwfile.h>
# include <nwfattr.h>
#endif
