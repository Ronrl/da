/**********************************************************************
*
*	C Header:		namspc.h
*	Instance:		api.1
*	Description:	
*	%created_by:	rbateman %
*	%date_created:	Tue Feb 11 17:22:24 1997 %
*
**********************************************************************/
#ifndef _api.1_namspc_h_H
#define _api.1_namspc_h_H

#ifndef lint
static char    *_api.1_namspc_h = "@(#) %filespec: namspc.h-1 %  (%full_filespec: namspc.h-1:incl:api.1 %)";
#endif

/* Everything else goes here */

#endif
