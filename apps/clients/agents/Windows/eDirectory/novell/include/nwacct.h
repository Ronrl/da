/******************************************************************************

  %name: nwacct.h %
  %version: 5 %
  %date_modified: Fri Jun 15 10:23:07 2007 %
  $Copyright:

  Copyright (c) 1989-2007 Novell, Inc.  All Rights Reserved.

  USE AND REDISTRIBUTION OF THIS WORK IS SUBJECT TO THE DEVELOPER LICENSE
  AGREEMENT OR OTHER AGREEMENT THROUGH WHICH NOVELL, INC. MAKES THE WORK
  AVAILABLE. THIS WORK MAY NOT BE ADAPTED WITHOUT NOVELL'S PRIOR WRITTEN
  CONSENT.

  NOVELL PROVIDES THE WORK "AS IS," WITHOUT ANY EXPRESS OR IMPLIED WARRANTY,
  INCLUDING WITHOUT LIMITATION THE IMPLIED WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT. NOVELL, THE AUTHORS
  OF THE WORK, AND THE OWNERS OF COPYRIGHT IN THE WORK ARE NOT LIABLE FOR ANY
  CLAIM, DAMAGES, OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT,
  OR OTHERWISE, ARISING FROM, OUT OF, OR IN CONNECTION WITH THE WORK OR THE
  USE OR OTHER DEALINGS IN THE WORK.$$

 *****************************************************************************/

#if ! defined ( NWACCT_H )
#define NWACCT_H

#if ! defined ( NTYPES_H )
#include "ntypes.h"
#endif

#if ! defined ( NWCALDEF_H )
#include "nwcaldef.h"
#endif

#include "npackon.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct
{
   nuint32  objectID;
   nint32   amount;
} HOLDS_INFO;

typedef struct
{
   nuint16  holdsCount;
   HOLDS_INFO holds[16];
} HOLDS_STATUS;

N_EXTERN_LIBRARY( NWCCODE )
NWGetAccountStatus
(
   NWCONN_HANDLE        conn,
   nuint16              objType,
   const nstr8 N_FAR *  objName,
   pnint32              balance,
   pnint32              limit,
   HOLDS_STATUS N_FAR * holds
);

N_EXTERN_LIBRARY( NWCCODE )
NWQueryAccountingInstalled
(
   NWCONN_HANDLE  conn,
   pnuint8        installed
);

N_EXTERN_LIBRARY( NWCCODE )
NWSubmitAccountCharge
(
   NWCONN_HANDLE       conn,
   nuint16             objType,
   const nstr8 N_FAR * objName,
   nuint16             serviceType,
   nint32              chargeAmt,
   nint32              holdCancelAmt,
   nuint16             noteType,
   const nstr8 N_FAR * note
);

N_EXTERN_LIBRARY( NWCCODE )
NWSubmitAccountHold
(
   NWCONN_HANDLE       conn,
   nuint16             objType,
   const nstr8 N_FAR * objName,
   nint32              holdAmt
);

N_EXTERN_LIBRARY( NWCCODE )
NWSubmitAccountNote
(
   NWCONN_HANDLE       conn,
   nuint16             objType,
   const nstr8 N_FAR * objName,
   nuint16             serviceType,
   nuint16             noteType,
   const nstr8 N_FAR * note
);

#ifdef __cplusplus
}
#endif

#include "npackoff.h"
#endif
