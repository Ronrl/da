/******************************************************************************

  %name: nwdsname.h %
  %version: 4 %
  %date_modified: Fri Jun 15 10:23:37 2007 %
  $Copyright:

  Copyright (c) 1989-2007 Novell, Inc.  All Rights Reserved.

  USE AND REDISTRIBUTION OF THIS WORK IS SUBJECT TO THE DEVELOPER LICENSE
  AGREEMENT OR OTHER AGREEMENT THROUGH WHICH NOVELL, INC. MAKES THE WORK
  AVAILABLE. THIS WORK MAY NOT BE ADAPTED WITHOUT NOVELL'S PRIOR WRITTEN
  CONSENT.

  NOVELL PROVIDES THE WORK "AS IS," WITHOUT ANY EXPRESS OR IMPLIED WARRANTY,
  INCLUDING WITHOUT LIMITATION THE IMPLIED WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT. NOVELL, THE AUTHORS
  OF THE WORK, AND THE OWNERS OF COPYRIGHT IN THE WORK ARE NOT LIABLE FOR ANY
  CLAIM, DAMAGES, OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT,
  OR OTHERWISE, ARISING FROM, OUT OF, OR IN CONNECTION WITH THE WORK OR THE
  USE OR OTHER DEALINGS IN THE WORK.$

 *****************************************************************************/

#if ! defined ( NWDSNAME_H )
#define NWDSNAME_H

#if ! defined ( NTYPES_H )
#include "ntypes.h"
#endif

#if ! defined ( NWDSTYPE_H )
#include "nwdstype.h"
#endif

#if ! defined ( NWCALDEF_H )
#include "nwcaldef.h"
#endif

#if! defined ( NWDSDC_H )
#include "nwdsdc.h"
#endif


#ifdef __cplusplus
   extern "C" {
#endif

N_EXTERN_LIBRARY (NWDSCCODE)
NWDSAbbreviateName
(
   NWDSContextHandle context,
   pnstr8            inName,
   pnstr8            abbreviatedName
);

N_EXTERN_LIBRARY (NWDSCCODE)
NWDSCanonicalizeName
(
   NWDSContextHandle context,
   pnstr8            objectName,
   pnstr8            canonName
);

N_EXTERN_LIBRARY (NWDSCCODE)
NWDSRemoveAllTypes
(
   NWDSContextHandle context,
   pnstr8            name,
   pnstr8            typelessName
);

N_EXTERN_LIBRARY (NWDSCCODE)
NWDSResolveName
(
   NWDSContextHandle    context,
   pnstr8               objectName,
   NWCONN_HANDLE  N_FAR *conn,
   pnuint32             objectID
);

N_EXTERN_LIBRARY (NWDSCCODE)
NWDSCIStringsMatch
(
   NWDSContextHandle context,
   pnstr8            string1,
   pnstr8            string2,
   pnint             matches
);

#ifdef __cplusplus
   }
#endif
#endif /* NWDSNAME_H */
