/******************************************************************************

  %name: nwmsg.h %
  %version: 9 %
  %date_modified: Fri Jun 15 10:23:50 2007 %
  $Copyright:

  Copyright (c) 1989-2007 Novell, Inc.  All Rights Reserved.

  USE AND REDISTRIBUTION OF THIS WORK IS SUBJECT TO THE DEVELOPER LICENSE
  AGREEMENT OR OTHER AGREEMENT THROUGH WHICH NOVELL, INC. MAKES THE WORK
  AVAILABLE. THIS WORK MAY NOT BE ADAPTED WITHOUT NOVELL'S PRIOR WRITTEN
  CONSENT.

  NOVELL PROVIDES THE WORK "AS IS," WITHOUT ANY EXPRESS OR IMPLIED WARRANTY,
  INCLUDING WITHOUT LIMITATION THE IMPLIED WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT. NOVELL, THE AUTHORS
  OF THE WORK, AND THE OWNERS OF COPYRIGHT IN THE WORK ARE NOT LIABLE FOR ANY
  CLAIM, DAMAGES, OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT,
  OR OTHERWISE, ARISING FROM, OUT OF, OR IN CONNECTION WITH THE WORK OR THE
  USE OR OTHER DEALINGS IN THE WORK.$

 *****************************************************************************/

#if ! defined ( NWMSG_H )
#define NWMSG_H

#if ! defined ( NWCALDEF_H )
# include "nwcaldef.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

N_EXTERN_LIBRARY( NWCCODE )
NWDisableBroadcasts
(
   NWCONN_HANDLE  conn
);

N_EXTERN_LIBRARY( NWCCODE )
NWEnableBroadcasts
(
   NWCONN_HANDLE  conn
);

N_EXTERN_LIBRARY( NWCCODE )
NWSendBroadcastMessage
(
   NWCONN_HANDLE         conn,
   const nstr8   N_FAR * message,
   nuint16               connCount,
   const nuint16 N_FAR * connList,
   pnuint8               resultList
);

N_EXTERN_LIBRARY( NWCCODE )
NWGetBroadcastMessage
(
   NWCONN_HANDLE  conn,
   pnstr8         message
);

N_EXTERN_LIBRARY( NWCCODE )
NWSetBroadcastMode
(
   NWCONN_HANDLE  conn,
   nuint16        mode
);

N_EXTERN_LIBRARY( NWCCODE )
NWBroadcastToConsole
(
   NWCONN_HANDLE       conn,
   const nstr8 N_FAR * message
);

N_EXTERN_LIBRARY( NWCCODE )
NWSendConsoleBroadcast
(
   NWCONN_HANDLE       conn,
   const nstr8 N_FAR * message,
   nuint16             connCount,
   pnuint16            connList
);


#ifdef __cplusplus
}
#endif

   /* The NLM LibC x-plat libraries do not support obsolete apis
   */
#include <stddef.h>
#if !defined(__NOVELL_LIBC__)
   #ifdef INCLUDE_OBSOLETE
      #include "obsolete/o_msg.h"
   #endif
#endif /* !defined(__NOVELL_LIBC__) */

#endif
