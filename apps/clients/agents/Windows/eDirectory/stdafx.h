//
//  stdafx.h
//  DeskAlerts Client
//  eDirectory module
//
//  Created by Maqentaer
//  Copyright Softomate 2006-2009. All rights reserved.
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers

// ATL secured functions.
#define _SECURE_ATL 1

// STL errors handling.
// _SECURE_SCL=1 for enabled state, 0 for disabled.
// _SECURE_SCL_THROWS=1 for throwing exception, 0 for abnormal program termination.
#ifdef _SECURE_SCL
#undef _SECURE_SCL
#endif
#ifdef _SECURE_SCL_THROWS
#undef _SECURE_SCL_THROWS
#endif
//#define _SECURE_SCL 1
//#define _SECURE_SCL_THROWS 1

#ifndef release_try
#ifdef DEBUG
#define release_try {
#define release_end_try }
#define release_catch_all } if(0) {
#define release_catch_end }
#define delete_catch(x) delete x; //use debug and/or brain!
#else
#define release_try try {
#define release_end_try } catch(...) {}
#define release_catch_all } catch(...) {
#define release_catch_end }
#define delete_catch(x) try{ delete x; } catch(...) {} //ha-ha-ha! we need to avoid stupid crashes.
#endif
#endif

#define _WTL_NO_CSTRING

#include <windows.h>

