//
//  eDirectory.cpp
//  DeskAlerts Client
//  eDirectory module
//
//  Created by Maqentaer
//  Copyright Softomate 2006-2009. All rights reserved.
//

#include "stdafx.h"
#include "eDirectory.h"

EDIRECTORY_API char* eDirGetContextName()
{
	NWDSContextHandle context;

	NWDSCreateContextHandle(&context);

	char objC[MAX_DN_CHARS+1];
	memset(objC, 0, MAX_DN_CHARS);
	NWDSGetContext(context, DCK_NAME_CONTEXT, objC);

	NWDSFreeContext(context);

	char* result = new char[strlen(objC)+1];
	strcpy_s(result, strlen(objC)+1, objC);
	return result;
}

EDIRECTORY_API char* eDirGetUserName()
{
	NWDSContextHandle context;

	NWDSCreateContextHandle(&context);

	char objDN[MAX_DN_CHARS+1];
	memset(objDN, 0, MAX_DN_CHARS);
	NWDSWhoAmI(context, objDN);

	NWDSFreeContext(context);

	char* result = NULL;
	if(!strncmp(objDN, "CN=", 3))
	{
		result = new char[strlen(objDN)-2];
		strcpy_s(result, strlen(objDN)-2, objDN+3);
	}
	else
	{
		result = new char[strlen(objDN)+1];
		strcpy_s(result, strlen(objDN)+1, objDN);
	}
	return result;
}
