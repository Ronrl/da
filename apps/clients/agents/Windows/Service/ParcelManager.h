#pragma once
#include "Parcel.h"
#include <deque>

class ParcelManager
{
public:
	ParcelManager(void);
	~ParcelManager(void);
	Parcel* recieveParcel();
	void sendParcel(Parcel* parcel);
private:
	std::deque<Parcel*> m_parcels;
	HANDLE m_event;
	CRITICAL_SECTION m_criticalSection;
};
