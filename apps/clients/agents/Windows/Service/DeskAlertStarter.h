#pragma once
#include <string.h>
#include <stdio.h>
#include <tlhelp32.h>
#include <tchar.h>
#include <windows.h>

#define STATUS_INFO_LENGTH_MISMATCH 0xc0000004
// NTQUERYSYSTEMINFORMATION

//
// Класс для запуска DeskAlerts после выхода из режима гибернации
//
class DeskAlertStarter
{
private:

public:
	static BOOL StartDeskAlert();
	static BOOL IsDeskAlertRun(const WCHAR* processName);
};