#include "StdAfx.h"
#include "Uninstaller.h"
#include "Utils.h"
#include "ConfigManager.h"

HANDLE Uninstaller::timer = NULL;

Uninstaller::Uninstaller(void)
{
}

Uninstaller::~Uninstaller(void)
{
}

bool Uninstaller::runUninstall()
{
	TCHAR filename[_MAX_PATH];
	GetModuleFileName(NULL,filename,_MAX_PATH);
	CString appDirectory;
	appDirectory = filename;
	appDirectory = appDirectory.Left(appDirectory.ReverseFind('\\'));
	CString tempPath;
	::GetTempPath(MAX_PATH, tempPath.GetBuffer(MAX_PATH));
	tempPath.ReleaseBuffer();

	if(tempPath.GetAt(tempPath.GetLength()-1) != _T('\\'))
	{
		tempPath += _T('\\');
	}

	if (Uninstaller::writeUninstallBatFile(tempPath,"DeskalertsServiceUninstall.bat"))
	{
		CString argStr=_T("\"")+CString(filename)+_T("\" \"")+appDirectory+_T("\" \"")+Utils::allUsersAppDataPath()+_T("\"");
		ShellExecute(NULL,NULL,tempPath+_T("\\DeskalertsServiceUninstall.bat"), argStr, NULL, SW_HIDE);
		Utils::RecursiveDeleteDirectory(Utils::allUsersAppDataProfilePath());
		MoveFileEx(appDirectory,NULL,MOVEFILE_DELAY_UNTIL_REBOOT);
		Utils::UninstallService();
		Utils::CloseService();		
	}
	return true;
}

bool Uninstaller::writeUninstallBatFile(CString tempPath, CString tempBatName)
{
	TCHAR temp[] =
		_T(":Repeat\r\n")
		_T("timeout /t 1\r\n")
		_T("del \"%~1\"\r\n")
		_T("if exist \"%~1\" goto Repeat\r\n")
		_T("del /F /Q \"%~2\"\r\n")
		_T("rmdir \"%~2\"\r\n")
		_T("rmdir \"%~3\"\r\n")
		_T("del \"%~0\"");

	tempPath = tempPath + tempBatName;
	HANDLE hf = NULL;
	hf = CreateFileW(tempPath, GENERIC_WRITE, 0, NULL,
		CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL) ;

	if (hf != INVALID_HANDLE_VALUE)
	{
		DWORD len;
		std::string str  = CT2A(temp, CP_UTF8);
		WriteFile(hf, str.c_str(), str.length(), &len, NULL);
		CloseHandle(hf);
		return true;
	}

	return false;
}


bool Uninstaller::sendUninstallRequest(std::vector<CString>& uninstallUrls)
{
	std::vector<CString>::iterator it = uninstallUrls.begin();
	while(it!=uninstallUrls.end())
	{
		if (Utils::sendRequest(*it))
		{		
			it = uninstallUrls.erase(it);
		}
		else
		{
			++it;
		}	
	}
	if(uninstallUrls.empty())
	{
		return true;
	}
	return false;
}

void Uninstaller::tryUninstall(std::vector<CString>& uninstallUrls)
{
	BOOL succes = false;
	if (!timer)
		Uninstaller::initTimer();

	if (timer)
	{
		while (!succes)
		{
			if (WaitForSingleObjectWithMsgLoop(timer, INFINITE) == WAIT_OBJECT_0)
			{
				if (sendUninstallRequest(uninstallUrls))
				{
					succes = true;
				}
			}
		}
		runUninstall();
	}
}

void  Uninstaller::initTimer()
{
	if ( (timer = CreateWaitableTimer(NULL, FALSE, _T("uninstallerTimer"))) )
	{
		LARGE_INTEGER li;
		SYSTEMTIME st;
		GetSystemTime(&st);
		const int nTimerUnitsPerSecond = 10000000;
		li.QuadPart = -1;
		li.QuadPart *= nTimerUnitsPerSecond;
		SetWaitableTimer(timer, &li, 5000, NULL, NULL, FALSE);
	}
}

bool Uninstaller::serviceUninstall(std::map<CString,std::map<CString,CString>> usersProperties,std::map<CString,CString> commonProperties)
{
	std::map<CString,std::map<CString,CString>>::iterator it = usersProperties.begin();
	std::vector<CString> uninstallUrls;
	while (it!=usersProperties.end())
	{
		CString uninstallUrl = Utils::getPropertyString(CString("uninstallUrl"),it->second,&CString());
		if (!uninstallUrl.IsEmpty())
		{
			uninstallUrls.push_back(uninstallUrl);
		}
		++it;
	}
	
	if (sendUninstallRequest(uninstallUrls))
	{
		runUninstall();
	}
	else
	{
		CString tryStr = Utils::getPropertyString(CString("successful_uninstall_request"),commonProperties,&CString());
		if(tryStr == "1")
		{
			tryUninstall(uninstallUrls);
		}
	}
	return true;
}