﻿#include "stdafx.h"
#include <windows.h>
#include <atlstr.h>
#include <Shlobj_core.h>
#include <Sddl.h>

#include <time.h>

#include <aes.h>
#include <modes.h>
#include <filters.h>
#include <base64.h>

#include "rapidjson/document.h"

#include "Utils.h"

#include "LockscreenManager.h"

LockscreenManager * LockscreenManager::_instance = nullptr;

CString getLockscreensPath()
{
	CString deskalertsPath = Utils::allUsersAppDataProfilePath();
	CString lockscreenPath = deskalertsPath + CString("\\Lockscreens");
	CreateDirectory(lockscreenPath, NULL);

	return lockscreenPath;
}

CString CreateGUID()
{
	GUID guidValue = GUID_NULL;
	::CoCreateGuid(&guidValue);
	if (guidValue == GUID_NULL) return _T("");

	CString strName;
	strName.Format(_T("{%08lX-%04X-%04x-%02X%02X-%02X%02X%02X%02X%02X%02X}"),
		guidValue.Data1, (guidValue.Data2), (guidValue.Data3),
		(guidValue.Data4[0]), (guidValue.Data4[1]), (guidValue.Data4[2]), (guidValue.Data4[3]),
		(guidValue.Data4[4]), (guidValue.Data4[5]), (guidValue.Data4[6]), (guidValue.Data4[7]));

	return strName;
}

HRESULT LockscreenManager::Download(CString szURL, LPCTSTR szFileName, CString *result, BOOL *isUTF8, CString *key, 
	CString *headers, CString *postData, int reqVerb)
{
	CString fUrl = szURL;
	//if (CitrixIp.IsEmpty())
	//{
		//fUrl.Format(_T("%s"), szURL);
	//}
	//else
	//{
	//	if (_tcsstr(szURL, _T("?")) != NULL)
	//	{
	//		fUrl.Format(_T("%s&ip=%s"), szURL, CitrixIp);
	//	}
	//	else
	//	{
	//		fUrl.Format(_T("%s?ip=%s"), szURL, CitrixIp);
	//	}
	//}

	//logHttpRequest(fUrl);
	//
	//
	//
	HRESULT res = S_FALSE;
	if (fUrl && _tcslen(fUrl) > 0)
	{
		bool skipHttpsCheck = false;
		//if (_iAlertModel)
		//{
		//	CString strSkipHttpsCheck = _iAlertModel->getPropertyString(CString(_T("skip_https_check")));
		//	skipHttpsCheck = (strSkipHttpsCheck == _T("1") || strSkipHttpsCheck == _T("true"));
		//}

		CUrl cUrl;
		if (cUrl.CrackUrl(fUrl))
		{
			SetLastError(0);
			const HINTERNET hInternet = ::InternetOpen(_T("DeskAlerts"), INTERNET_OPEN_TYPE_PRECONFIG, NULL, NULL, 0);
			if (hInternet != NULL)
			{
				SetLastError(0);
				const HINTERNET hConnect = ::InternetConnect(hInternet, cUrl.GetHostName(), cUrl.GetPortNumber(), NULL, NULL, INTERNET_SERVICE_HTTP, 0, 1);
				if (hConnect != NULL)
				{
					CString sUrl = cUrl.GetUrlPath();
					sUrl += cUrl.GetExtraInfo();

					DWORD dwFlags = /*INTERNET_FLAG_KEEP_CONNECTION|*/INTERNET_FLAG_RELOAD | INTERNET_FLAG_NO_CACHE_WRITE | INTERNET_FLAG_PRAGMA_NOCACHE;

					if (cUrl.GetScheme() == ATL_URL_SCHEME_HTTPS)
					{
						dwFlags |= INTERNET_FLAG_SECURE;
					}

					SetLastError(0);
					LPCTSTR requestType;
					switch (reqVerb)
					{
					case GETverb:  requestType = _T("GET");  break;
					case POSTverb: requestType = _T("POST"); break;
					case OLDcalcVerbByPostData:
					default:
						if (postData)
						{
							requestType = _T("POST");
						}
						else
						{
							requestType = _T("GET");
						}
						break;
					}


					const HINTERNET hRequest = ::HttpOpenRequest(hConnect, requestType, sUrl, NULL, NULL, 0, dwFlags, NULL);
					if (hRequest != NULL)
					{
						if (cUrl.GetScheme() == ATL_URL_SCHEME_HTTPS)
						{
							if (skipHttpsCheck)
							{
								DWORD dwFlags2 = 0;
								DWORD dwSize = sizeof(dwFlags2);
								InternetQueryOption(hRequest, INTERNET_OPTION_SECURITY_FLAGS, (LPVOID)&dwFlags2, &dwSize);
								dwFlags2 |= SECURITY_FLAG_IGNORE_UNKNOWN_CA | SECURITY_FLAG_IGNORE_REVOCATION | SECURITY_FLAG_IGNORE_CERT_CN_INVALID | SECURITY_FLAG_IGNORE_CERT_DATE_INVALID | SECURITY_FLAG_IGNORE_WRONG_USAGE;
								InternetSetOption(hRequest, INTERNET_OPTION_SECURITY_FLAGS, &dwFlags2, sizeof(dwFlags2));
							}
							DWORD pCert = 0;
							InternetSetOption(hRequest, INTERNET_OPTION_CLIENT_CERT_CONTEXT, (LPVOID)&pCert, sizeof(DWORD));
						}

						BOOL bSend;
						PCCERT_CONTEXT pCert = NULL;
						HCERTSTORE hSystemStore = NULL;
						do
						{
							SetLastError(0);
							int headersLength = 0;
							int postDataLength = 0;
							LPCTSTR headersStr = NULL;
							LPVOID postDataStr = NULL;
							if (headers)
							{
								headersLength = headers->GetLength();
								headersStr = headers->GetString();
							}
							CStringA tmpPostData;
							if (postData)
							{
								tmpPostData = CStringA(CT2A(*postData));
								postDataLength = tmpPostData.GetLength();
								postDataStr = (LPVOID)(tmpPostData.GetString());
							}
							bSend = HttpSendRequest(hRequest, headersStr, headersLength, postDataStr, postDataLength);
							const DWORD dwError = GetLastError();
							// Adds error description to log-file if system error has been detected
							//if (dwError != S_OK)
							//	wprintf(L"HttpOpenRequest %%ul", dwError);
							

							if (dwError == ERROR_INTERNET_CLIENT_AUTH_CERT_NEEDED)
							{
								if (!hSystemStore)
								{
									SetLastError(0);
									hSystemStore = CertOpenSystemStore(NULL, L"MY");

									//appendToErrorFileIfError(_T("CertOpenSystemStore"), GetLastError());
									// Adds error description to log-file if system error has been detected
									//wprintf(_T("CertOpenSystemStore %%ul"), GetLastError());
								}

								SetLastError(0);
								pCert = CertEnumCertificatesInStore(hSystemStore, pCert);
								// Adds error description to log-file if system error has been detected
								//logSystemError(L"CertEnumCertificatesInStore", GetLastError());
								if (!pCert) break;
								InternetSetOption(hRequest, INTERNET_OPTION_CLIENT_CERT_CONTEXT, (LPVOID)pCert, sizeof(CERT_CONTEXT));
								CertFreeCertificateContext(pCert);
							}
							else
							{
								break;
							}
						} while (true);
						if (hSystemStore)
							CertCloseStore(hSystemStore, CERT_CLOSE_STORE_CHECK_FLAG);

						TCHAR status[32] = { 0 };
						DWORD dwSize = 32;
						////////////////////////////
						/// Request Status
						if (HttpQueryInfo(hRequest, HTTP_QUERY_STATUS_CODE, status, &dwSize, NULL))
						{
							const int nStatusCode = _ttoi(status);

							// Logging the server response status code
							//logHttpStatus(nStatusCode);
							//
							//
							//

							if (nStatusCode >= 400)
							{
								::InternetCloseHandle(hRequest);
								::InternetCloseHandle(hConnect);
								::InternetCloseHandle(hInternet);
								return S_FALSE;
							}
						}
						{
							PTCHAR ptOutBuffer = NULL;
							DWORD dwSizeIF_MATCH = 0;
							DWORD someerr = 0;
							////////////////////////////
							/// Request ETAG Header
							if (!HttpQueryInfo(hRequest, /*HTTP_QUERY_RAW_HEADERS*/HTTP_QUERY_ETAG, ptOutBuffer, &dwSizeIF_MATCH, NULL))
							{
								if (GetLastError() == ERROR_INSUFFICIENT_BUFFER)
								{
									ptOutBuffer = new TCHAR[dwSizeIF_MATCH];
									if (HttpQueryInfo(hRequest, /*HTTP_QUERY_RAW_HEADERS*/HTTP_QUERY_ETAG, ptOutBuffer, &dwSizeIF_MATCH, NULL))
									{
										if (GetLastError() == S_OK)
										{
											CString value = CString(ptOutBuffer);
											//_iAlertModel->setPropertyString(CString(_T("ETag")), value);
										}
									}
								}
								someerr = GetLastError();
								//process it if nesessery
							}
							if (ptOutBuffer)
							{
								delete[] ptOutBuffer;
							}
						}
						const bool file = (szFileName && _tcslen(szFileName) > 0);
						if (bSend && (file || result))
						{
							char szData[2048];
							HANDLE hFile = NULL;
							if (file)
							{
								SetLastError(0);
								hFile = CreateFile(szFileName, GENERIC_WRITE, FILE_SHARE_READ, 0, CREATE_ALWAYS, FILE_FLAG_WRITE_THROUGH, 0);
								//if(!hFile)
								//	appendToErrorFileIfError(_T("InternetCreateFile"), GetLastError());
								// Adds error description to log-file if system error has been detected
								//logSystemError(L"InternetCreateFile", GetLastError());
								//
								//
								//
							}
							res = S_OK;
							size_t asize = 0;
							//char *aresult = NULL;

							std::string strInternetReader;
							do
							{
								SetLastError(0);
								if (!InternetReadFile(hRequest, (LPVOID)szData, sizeof(szData), &dwSize))
								{
									//appendToErrorFileIfError(_T("InternetReadFile"), GetLastError());
									// Adds error description to log-file if system error has been detected
									//logSystemError(L"InternetReadFile", GetLastError());
									//
									//
									//
									res = S_FALSE;
									break;
								}
								if (dwSize != 0)
								{
									strInternetReader += std::string(szData, dwSize);
									asize += dwSize;
									strInternetReader[asize] = '\0';
								}
							} while (dwSize);
							if (key && strInternetReader.size() != 0/* aresult*/)
							{
								CT2CA keyInTchar(*key);
								std::string cipherString(strInternetReader);
								//std::string cipherString(aresult);
								std::string keyString(keyInTchar);
								std::string vectorString("4276235117979925");
								std::string decrypted;

								byte bytes[32];
								memset(bytes, 0x00, 32);

								if (keyString.length() < 32)
								{
									memcpy(bytes, (byte*)keyString.c_str(), keyString.length());
								}
								else
								{
									memcpy(bytes, (byte*)keyString.c_str(), 32);
								}

								std::string decoded;

								CryptoPP::StringSource ss(cipherString, true, new CryptoPP::Base64Decoder(new CryptoPP::StringSink(decoded)));

								const char* decodedCStr = decoded.c_str();

								CryptoPP::CFB_Mode<CryptoPP::AES>::Decryption cfbDecryption(bytes, sizeof(bytes), (byte*)vectorString.c_str());
								cfbDecryption.ProcessData((byte*)decodedCStr, (byte*)decodedCStr, decoded.length());
								// new code
								strInternetReader = std::string(decodedCStr);
								// old code
								//aresult = (char*)realloc(aresult, strlen(decodedCStr) + 1);
								//memcpy(aresult, decodedCStr, strlen(decodedCStr) + 1);
							}
							if (file && strInternetReader.size() != 0/*aresult */ && hFile)
							{
								DWORD resSize = asize;
								WriteFile(hFile, strInternetReader.c_str()/*aresult*/, strInternetReader.size()/* asize*/, &resSize, 0);
							}
							if (result && strInternetReader.size() != 0 /*aresult*/)
							{
								BOOL isutf8 = FALSE;
								DWORD size = 256;
								CString content_type;
								if (HttpQueryInfo(hRequest, HTTP_QUERY_CONTENT_TYPE, content_type.GetBuffer(size), &size, NULL))
								{
									content_type.ReleaseBuffer(size);
									content_type.MakeLower();
									//isUTF8 = (content_type.Find(_T("utf-8")) != -1);
									content_type.Replace(_T(" "), _T(""));
									content_type.Replace(_T("`"), _T(""));
									content_type.Replace(_T("\'"), _T(""));
									content_type.Replace(_T("\""), _T(""));
									content_type.Replace(_T("-"), _T(""));
									isutf8 = (content_type.Find(_T("charset=utf8")) != -1);
								}
								else
								{
									// Adds error description to log-file if system error has been detected
									//logSystemError(L"HttpQueryInfo", GetLastError());
								}

								if (isutf8) *result = CA2T(strInternetReader.c_str()/*aresult*/, CP_UTF8);
								else *result = CA2T(strInternetReader.c_str()/*aresult*/);
								if (isUTF8) *isUTF8 = isutf8;
							}
							if (hFile)
							{
								CloseHandle(hFile);
							}
						}
						::InternetCloseHandle(hRequest);
					}
					else
					{
						// Adds error description to log-file if system error has been detected
						wprintf(L"HttpOpenRequest %ul", GetLastError());
					}
					::InternetCloseHandle(hConnect);
				}
				else
				{
					// Adds error description to log-file if system error has been detected
					wprintf(L"InternetConnect %ul", GetLastError());
				}
				::InternetCloseHandle(hInternet);
			}
			else
			{
				// Adds error description to log-file if system error has been detected
				wprintf(L"InternetOpen %ul", GetLastError());
			}
		}
		else
		{
			// Adds error description to log-file if system error has been detected
			wprintf(L"CUrl::CrackUrl %ul", GetLastError());
		}
	}
	return res;
}


VOID CALLBACK OnLockscreenTimer(HWND, UINT, UINT, DWORD)
{
	// TODO Uncomment when the delivery of content to the computer will be possible
	//LockscreenManager::Instance()->CheckLockscreens();
}

DWORD WINAPI LockscreenThread(LPVOID)
{
	MSG msg;
	UINT timer = SetTimer(NULL, 0, 1000 * LockscreenManager::Instance()->GetInterval(), (TIMERPROC)OnLockscreenTimer);

	while (GetMessage(&msg, NULL, 0, 0) > 0)
	{

		if (msg.message == WM_QUIT)
		{
			KillTimer(NULL, timer);
			break;
		}
		else
		{
			::TranslateMessage(&msg);
			::DispatchMessage(&msg);
		}
	}

	return 0;
}



void LockscreenManager::Login(CString username, CString password)
{
	CString headers = _T("\nContent-Type: application/json; charset=utf-8 ");
	CString requestResult = "";
	CString postData;

	CString ip;

	postData.Format(L"{DeskbarId:\"%s\",UserName:\"%s\",ComputerName:\"%s\",ComputerDomainName:\"%s\",Ip:\"%s\",ClientDeviceType:%d,ClientVersion:\"%s\",Md5Password:\"%s\",PollPeriod:\"%ds\"}",
		deskalertsId.GetString(), username.GetString(), this->computerName.GetString(), this->domainName.GetString(),
		ip.GetString(), 3, L"", password.GetString(), interval);


	CString url;

	url.Format(L"%s/api/v1/security/token/user", serverUrl.GetString());

	HRESULT hDwnldres = Download(url, NULL, &requestResult, 0, 0, &headers, &postData, POSTverb);

	if (hDwnldres == S_OK)
	{
		rapidjson::Document d;
		d.Parse(CT2A(requestResult));

		
		if (!d.HasMember("status") || d["status"] != "Success" ||
			!d.HasMember("data") || !d["data"].HasMember("authorization"))
		{
			isLogged = false;
			return;
		}

		auto data = d["data"].GetObjectW();
			 
		this->token = data["authorization"].GetString();

	}
}

LockscreenManager::LockscreenManager() : m_thread(NULL), m_threadId(0)
{
	// Get domain name
	computerName.GetEnvironmentVariable(_T("COMPUTERNAME"));
	domainName.GetEnvironmentVariable(_T("USERDOMAIN"));

	char buf[2048];

	CString iniFileName;
	iniFileName.Format(L"%s\\service.ini", Utils::allUsersAppDataProfilePath().GetString());

	GetPrivateProfileString(L"LockScreen", L"base_url", L"", (LPWSTR)buf, sizeof(buf), iniFileName.GetString());

	
	// TODO Подумать, куда вынести путь к сервису
	baseUrl.Format(L"%s/api/v1/computer/content/lockscreens?timezone=", buf);
	serverUrl.Format(L"%s", buf);
	

	GetPrivateProfileString(L"LockScreen", L"backup_base_url", L"", (LPWSTR)buf, sizeof(buf), iniFileName.GetString());
	if (buf[0] != 0)
	{
		baseBackupUrl.Format(L"%s/api/v1/computer/content/lockscreens?timezone=", buf);
		serverBackupUrl.Format(L"%s", buf);
	}
	else
	{
		wprintf(L"Backup server not defined in service.ini");
		baseBackupUrl = "";
	}
	interval = GetPrivateProfileInt(L"LockScreen", L"interval", 120, iniFileName.GetString());

	GetPrivateProfileString(L"LockScreen", L"deskalerts_id", L"", (LPWSTR)buf, sizeof(buf), iniFileName.GetString());
	deskalertsId.Format(L"%s", buf);

	if (deskalertsId.IsEmpty())
	{
		deskalertsId = CreateGUID();
		WritePrivateProfileString(L"LockScreen", L"deskalerts_id", deskalertsId.GetString(), iniFileName.GetString());
	}

	//Sleep(30000);
	

	GetPrivateProfileString(L"LockScreen", L"alert_app_name", L"", (LPWSTR)buf, sizeof(buf), iniFileName.GetString());

	clientProcessName = (LPWSTR)buf;

	m_thread = CreateThread(NULL, 0, &LockscreenThread, NULL, 0, &m_threadId);
}

LockscreenManager::~LockscreenManager()
{
	if (m_thread)	
	{
		PostThreadMessage(m_threadId, WM_QUIT, 0, 0);
		WaitForSingleObject(m_thread, INFINITE);
		CloseHandle(m_thread);
	}
}


LockscreenManager* LockscreenManager::Instance()
{
	if (LockscreenManager::_instance == nullptr)
	{
		LockscreenManager::_instance = new LockscreenManager();
	}

	return _instance;
}


bool LockscreenManager::CreateRegistryKey()
{
	const LPWSTR keyName = L"SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\PersonalizationCSP";
	HKEY hKey;
	LONG nError = RegOpenKeyEx(HKEY_LOCAL_MACHINE, keyName, NULL, KEY_ALL_ACCESS | KEY_WOW64_64KEY, &hKey);

	if (nError == ERROR_FILE_NOT_FOUND)
	{
		nError = RegCreateKeyEx(HKEY_LOCAL_MACHINE, keyName, NULL, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS | KEY_WOW64_64KEY, NULL, &hKey, NULL);
	}

	if (nError)
		wprintf(L"Could not find or create key %s", keyName);

	bool bResult = false;

	PSECURITY_DESCRIPTOR sd = nullptr;

	const TCHAR* szSD =
		TEXT("D:")                  // Discretionary ACL
		TEXT("(A;OICI;GA;;;AU)")    // Allow full control to all authenticated users
		TEXT("(A;OICI;GA;;;BA)")    // Allow full control to administrators
		TEXT("(A;OICI;GA;;;SY)")	// Allow full control to system
		//TEXT("(A;OICI;GA;;;IU)")	
		TEXT("(A;OICI;GA;;;WD)");	// Allow full control to everyone

	if (ConvertStringSecurityDescriptorToSecurityDescriptor((LPCTSTR)szSD, SDDL_REVISION_1, &sd, 0))
	{
		auto result = RegSetKeySecurity(hKey, DACL_SECURITY_INFORMATION, sd);
		if (ERROR_SUCCESS == result)
			bResult = true;
		else
			SetLastError(result);

		LocalFree(sd);
	}

	RegCloseKey(hKey);

	// Disable background blur
	const LPWSTR bgkeyName = L"SOFTWARE\\Policies\\Microsoft\\Windows\\System";
	nError = RegOpenKeyEx(HKEY_LOCAL_MACHINE, bgkeyName, NULL, KEY_ALL_ACCESS | KEY_WOW64_64KEY, &hKey);

	if (nError == ERROR_FILE_NOT_FOUND)
	{
		nError = RegCreateKeyEx(HKEY_LOCAL_MACHINE, bgkeyName, NULL, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS | KEY_WOW64_64KEY, NULL, &hKey, NULL);
	}

	DWORD value = 1;
	RegSetValueEx(hKey, L"DisableAcrylicBackgroundOnLogon", 0, REG_DWORD, (BYTE*)&value, sizeof(value));


	return bResult;
}


void LockscreenManager::CheckLockscreens()
{
	CreateRegistryKey();


	if (Utils::IsProcessExists(this->clientProcessName))
		return;

	Login(computerName, CString("3FC0A7ACF087F549AC2B266BAF94B8B1"));

	CString headers;
	headers.Format(L"\nContent-Type: application/json; charset=utf-8\nAuthorization: %s", token.GetString());
	CString url;

	SYSTEMTIME lt;
	GetLocalTime(&lt);
	LONG TimeZoneMinutes = 0;
	TIME_ZONE_INFORMATION TimeZoneInformation;
	if (TIME_ZONE_ID_INVALID != GetTimeZoneInformation(&TimeZoneInformation))
	{
		TimeZoneMinutes = -TimeZoneInformation.Bias;
	}

	LONG TimeZoneSec = TimeZoneMinutes * 60;

	url.Format(L"%s%d", this->baseUrl.GetString(), TimeZoneSec);
		//ltm.tm_mday, ltm.tm_mon, 1900 + ltm.tm_year, ltm.tm_hour, ltm.tm_min, ltm.tm_sec);
	
	CString requestResult = "";

	HRESULT hDwnldres = Download(url.GetString(), NULL, &requestResult, 0, 0, &headers);

	if (hDwnldres != S_OK)
	{
		wprintf(L"Error downloading alerts from %s", url.GetBuffer());
		wprintf(L"Switch to backup server");

		if (baseBackupUrl != "")
		{
			// Trying backup server
			url.Format(L"%s%d", this->baseBackupUrl.GetString(), TimeZoneSec);
				//ltm.tm_mday, ltm.tm_mon, 1900 + ltm.tm_year, ltm.tm_hour, ltm.tm_min, ltm.tm_sec);

			requestResult = "";
			hDwnldres = Download(url, NULL, &requestResult, 0, 0, &headers);
		}
	}

	if (hDwnldres == S_OK)
	{
		rapidjson::Document d;
		d.Parse(CT2A(requestResult));

		if (!d.HasMember("status") || d["status"] != "Success")
			return;

		if (d.HasMember("data") && d["data"].IsArray())
		{
			auto arr = d["data"].GetArray();

			int maxId = -1;
			CString lockscreenUrl = "";
			
			for (auto i = arr.Begin(); i != arr.End(); ++i)
			{
				if (i->HasMember("href") && i->HasMember("alertId"))
				{
					if ((*i)["alertId"].GetInt() > maxId)
					{
						maxId = (*i)["alertId"].GetInt();
						lockscreenUrl = (*i)["href"].GetString();
					}
				}
				else 
				{
					wprintf(L"AlertID or image url not found in a incoming message");
				}
			}


			if (maxId > 0 && maxId != this->lastAlertId)
			{
				CString filename;
				filename.Format(L"%s\\%d.jpg", getLockscreensPath().GetString(), maxId);

				if (!this->IsDownloaded(filename))
				{

					HRESULT res = Download(lockscreenUrl, (LPCTSTR)filename.GetBuffer(), NULL, 0, 0, &headers);
					if (res != S_OK)
					{
						wprintf(L"Error downloading image from %s", lockscreenUrl.GetBuffer());
						return;
					}
				}
				
				if (SetLockscreen(filename))
				{
					this->lastAlertId = maxId;
					/// TODO Сделать чудо, чтобы обои поменялись без входа пользоватля в систему
					SHChangeNotify(SHCNE_ASSOCCHANGED, 0, 0, 0);
				}
				else
				{
					wprintf(L"Error setting lockscreen image %s", filename.GetString());
				}
			}
		}
	}
	else
	{
		wprintf(L"Error downloading alerts from %s", url.GetBuffer());
	}
}

bool LockscreenManager::IsDownloaded(const CString &url) const
{
	WIN32_FIND_DATA FindFileData;
	HANDLE handle = FindFirstFile(url.GetString(), &FindFileData);
	bool found = (handle != INVALID_HANDLE_VALUE);

	if (found)
	{
		FindClose(handle);
	}

	return found;
}


bool LockscreenManager::SetLockscreen(CString filename)
{
	HKEY hKey;
	LONG lResult;
	
	// 1 - Successfully downloaded or copied.
	DWORD imageStatus = 1;
	
	lResult = RegOpenKeyEx(HKEY_LOCAL_MACHINE, L"SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\PersonalizationCSP", 0, KEY_READ | KEY_WRITE | KEY_WOW64_64KEY, &hKey);

	if (lResult == ERROR_SUCCESS)
	{
		RegSetValueEx(hKey, L"LockScreenImagePath", 0, REG_SZ, (BYTE*)filename.GetBuffer(), sizeof(wchar_t)*filename.GetLength());
		RegSetValueEx(hKey, L"LockScreenImageUrl", 0, REG_SZ, (BYTE*)filename.GetBuffer(), sizeof(wchar_t)*filename.GetLength());
		RegSetValueEx(hKey, L"LockScreenImageStatus", 0, REG_DWORD, (BYTE*)&imageStatus, sizeof(imageStatus));

		RegCloseKey(hKey);		

		return true;
	}

	return false;
}

