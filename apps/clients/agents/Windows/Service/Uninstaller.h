#pragma once
#include "stdafx.h"
#include <map>
#include <vector>

class Uninstaller
{
public:
	Uninstaller(void);
	~Uninstaller(void);
	static bool serviceUninstall(std::map<CString,std::map<CString,CString>> usersProperties,std::map<CString,CString> commonProperties);
private:
	static HANDLE timer;
	static bool writeUninstallBatFile(CString tempPath, CString tempBatName);
	static bool runUninstall();
	static void initTimer();
	static void tryUninstall(std::vector<CString>& uninstallUrls);
	static bool sendUninstallRequest(std::vector<CString>& uninstallUrls);
};
