#pragma once
#include "stdafx.h"
#include "sqlite3.h"
#include <map>

class DatabaseManager
{
public:
	DatabaseManager(CString path);
	~DatabaseManager(void);
	HRESULT loadProperty(std::map<CString,CString> &prop);
private:
	sqlite3* m_Database;
	sqlite3* openDatabase(CString path);
	void CloseDatabase();
};
