#include "StdAfx.h"
#include "ParcelManager.h"

ParcelManager::ParcelManager(void)
{
	m_event = CreateEvent(NULL, FALSE, FALSE, NULL);
	InitializeCriticalSection(&m_criticalSection);
}

ParcelManager::~ParcelManager(void)
{
	CloseHandle(m_event);
	DeleteCriticalSection(&m_criticalSection);
}

Parcel* ParcelManager::recieveParcel()
{
	Parcel *parcel = NULL;
	while(TRUE)
	{
        if (!TryEnterCriticalSection(&m_criticalSection)) {
            continue;
        }
		if (!m_parcels.empty())
		{
			parcel = m_parcels.front();
			m_parcels.pop_front();
		}
		LeaveCriticalSection(&m_criticalSection);
		if (parcel)
		{
			break;
		}
		Sleep(100);
	}

	return parcel;
}

void ParcelManager::sendParcel(Parcel* parcel)
{
	EnterCriticalSection(&m_criticalSection);

		m_parcels.push_back(parcel);

	LeaveCriticalSection(&m_criticalSection);
}