#pragma once

enum requestVerb 
{
	OLDcalcVerbByPostData = 0,
	GETverb = 1,
	POSTverb = 2
};

#define DEFAULT_APP_NAME L"DeskAlerts.exe"

class LockscreenManager
{
private:
	HANDLE m_thread;
	DWORD m_threadId;

	CString deskalertsId;
	CString serverUrl;
	CString serverBackupUrl;


	// Last recived Alert ID
	int lastAlertId;

	// Computer and domain names
	CString computerName;
	CString domainName;

	/// base and backup api endpoint
	CString baseUrl;
	CString baseBackupUrl;

	// DeskAlert process name, default DeskAlert.exe
	CString clientProcessName;

	// Auth token
	CString token;

	bool isLogged;



	// Update interval in secnds, default 120
	int interval;

	static LockscreenManager *_instance;

private:
	bool IsDownloaded(const CString &url) const;
	void Login(CString username, CString password);

	HRESULT LockscreenManager::Download(CString szURL, LPCTSTR szFileName = NULL
		, CString *result = NULL, BOOL *isUTF8 = NULL, CString *key = NULL
		, CString *headers = NULL, CString *postData = NULL, int reqVerb = GETverb);

private:
	LockscreenManager();
	~LockscreenManager();


public:
	static LockscreenManager *Instance();

	void CheckLockscreens();
	bool SetLockscreen(CString filename);
	bool CreateRegistryKey();

	inline int GetInterval() { return this->interval; }
};

