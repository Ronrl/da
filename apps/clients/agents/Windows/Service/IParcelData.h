#pragma once

class IParcelData
{
public:
	IParcelData(void);
	virtual ~IParcelData(void);
	virtual void set(void*data)=0;
	virtual void* get()=0;
};
