#include "StdAfx.h"
#include "Utils.h"
#include <map>
#include "GetProcessID.h"
#include <sddl.h>
#include <AccCtrl.h>
#include <aclapi.h>
#include <tlhelp32.h>
#include "Wtsapi32.h"
#include "Userenv.h"
#include <shellapi.h>
#include "sha1.h"
#include <algorithm>
#include "atlsecurity.h"
#include <io.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <PSAPI.h>
#include <fstream>


_bstr_t sRegistryKey = "ALERTS00001";
_bstr_t sProfileName = "DeskAlerts";
_bstr_t sAutoUpdateRegKey = "SOFTWARE\\"+sRegistryKey+"\\"+sProfileName+"\\autoupdate";

std::map<CString,CString> propString;
CComCriticalSection m_cs; //define critical section
HANDLE serviceEvent;
HANDLE g_internetTimer = 0;
HANDLE g_updateTimer = 0;

Utils::Utils(void)
{
}

Utils::~Utils(void)
{
}

HKEY Utils::GetCompanyRegistryKey()
{
	CString sPreCompany = _T("software");
	HKEY hSoftKey = NULL;
	HKEY hCompanyKey = NULL;

	if (RegOpenKeyEx(HKEY_LOCAL_MACHINE,sPreCompany,0,KEY_WRITE|KEY_READ,&hSoftKey) == ERROR_SUCCESS)
	{
		RegOpenKeyEx(hSoftKey, sRegistryKey, 0, KEY_WRITE|KEY_READ, &hCompanyKey);
	}
	if (hSoftKey != NULL)
	RegCloseKey(hSoftKey);

	return hCompanyKey;
}

std::vector<HANDLE> Utils::getUsersTokensWithOpenedClients(CString name)
{
	std::vector<HANDLE> usersTokens;
	std::vector<DWORD> SetOfPID;
	GetProcessID(name.GetBuffer(),SetOfPID);

	std::vector<DWORD>::iterator rit = SetOfPID.begin();
	while(rit != SetOfPID.end())
	{
		const HANDLE token = Utils::GetProcessOwnerToken(*rit);
		usersTokens.push_back(token); //don't forget to close handle
		++rit;
	}

	return usersTokens;
}

HANDLE Utils::GetProcessOwnerToken(DWORD pid)
{
	if (!pid) return NULL;

	HANDLE hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pid);
	if (!hProcess) return NULL;

	HANDLE hToken = NULL;
	if(OpenProcessToken(hProcess, MAXIMUM_ALLOWED, &hToken))
	{
		HANDLE result = INVALID_HANDLE_VALUE;
		if(DuplicateTokenEx(hToken, TOKEN_ASSIGN_PRIMARY | TOKEN_ALL_ACCESS| TOKEN_ADJUST_PRIVILEGES, NULL, SecurityImpersonation, TokenPrimary, &result))
		{
			if(result != INVALID_HANDLE_VALUE)
			{
				CloseHandle(hToken);
				CloseHandle(hProcess);
				return result;
			}
		}
		CloseHandle(hToken);
	}
	CloseHandle(hProcess);

	return NULL;
}

BOOL Utils::RunProcessAsUser(HANDLE primaryToken,CString processPath, CString arguments)
{
	STARTUPINFO StartupInfo;
	PROCESS_INFORMATION processInfo;
	memset(&StartupInfo,0,sizeof(StartupInfo));
	StartupInfo.cb = sizeof(STARTUPINFO);
	HANDLE dupUserToken;

//	SECURITY_ATTRIBUTES Security1;
//	SECURITY_ATTRIBUTES Security2;

	CString command = "\"";
	command += processPath;
	command += "\"";

	if (!arguments.IsEmpty())
	{
		command += " ";
		command += arguments;
	}

	void* lpEnvironment = NULL;

	// Get all necessary environment variables of logged in user
	// to pass them to the process
	/*const BOOL resultEnv = */
	DuplicateTokenEx(primaryToken, MAXIMUM_ALLOWED | TOKEN_QUERY | TOKEN_DUPLICATE | TOKEN_IMPERSONATE, 
		NULL, SecurityIdentification, TokenPrimary, & dupUserToken);
	//CreateEnvironmentBlock(out envBlock, dupUserToken, false);	
	
	CreateEnvironmentBlock(&lpEnvironment, dupUserToken/* primaryToken*/, FALSE);

	// Start the process on behalf of the current user
	const BOOL result = CreateProcessAsUser(dupUserToken /*primaryToken*/, 0,
		command.GetBuffer(), 0,
		0, FALSE, CREATE_NO_WINDOW | NORMAL_PRIORITY_CLASS |
		CREATE_UNICODE_ENVIRONMENT, /*NULL*/lpEnvironment, 0,
		&StartupInfo, &processInfo);
	DestroyEnvironmentBlock(lpEnvironment);
	CloseHandle(primaryToken);

	return result;
}


//BOOL Utils::RunProcessAsUser(HANDLE primaryToken,CString processPath, CString arguments)
//{
//	STARTUPINFO StartupInfo;
//	PROCESS_INFORMATION processInfo;
//	memset(&StartupInfo,0,sizeof(StartupInfo));
//	StartupInfo.cb = sizeof(STARTUPINFO);
//
////	SECURITY_ATTRIBUTES Security1;
////	SECURITY_ATTRIBUTES Security2;
//
//	CString command = "\"";
//	command += processPath;
//	command += "\"";
//
//	if (!arguments.IsEmpty())
//	{
//		command += " ";
//		command += arguments;
//	}
//
//	void* lpEnvironment = NULL;
//
//	// Get all necessary environment variables of logged in user
//	// to pass them to the process
//	/*const BOOL resultEnv = */CreateEnvironmentBlock(&lpEnvironment,
//		primaryToken, FALSE);
//
//	// Start the process on behalf of the current user
//	const BOOL result = CreateProcessAsUser(primaryToken, 0,
//		command.GetBuffer(), 0,
//		0, FALSE, CREATE_NO_WINDOW | NORMAL_PRIORITY_CLASS |
//		CREATE_UNICODE_ENVIRONMENT, NULL, 0,
//		&StartupInfo, &processInfo);
//	DestroyEnvironmentBlock(lpEnvironment);
//	CloseHandle(primaryToken);
//
//	return result;
//}


BOOL Utils::ShellExecuteAndWait(HWND hwnd, LPCTSTR lpOperation, LPCTSTR lpFile, LPCTSTR lpParameters, LPCTSTR lpDirectory, INT nShowCmd, BOOL wait)
{
	LPSHELLEXECUTEINFO lpExecInfo = new SHELLEXECUTEINFO();    //setup the ShellExecuteEx
	lpExecInfo->cbSize = sizeof(SHELLEXECUTEINFO);
	lpExecInfo->hwnd = hwnd;
	lpExecInfo->lpFile = lpFile;
	lpExecInfo->lpParameters = lpParameters;
	lpExecInfo->lpDirectory=lpDirectory;
	lpExecInfo->lpVerb = lpOperation;
	lpExecInfo->nShow = nShowCmd;
	lpExecInfo->fMask = SEE_MASK_NOCLOSEPROCESS;  //ensures hProcess gets the process handle

	const BOOL retValue = ShellExecuteEx(lpExecInfo);
	if(retValue)
	{
		if(wait)
		{
			WaitForSingleObjectWithMsgLoop(lpExecInfo->hProcess, INFINITE);
		}
	}
	return retValue;
}

BOOL Utils::executeCommandLine(CString cmdLine, DWORD & exitCode)
{
   PROCESS_INFORMATION processInformation = {0};
   STARTUPINFO startupInfo                = {0};
   startupInfo.cb                         = sizeof(startupInfo);
   int nStrBuffer                         = cmdLine.GetLength() + 50;


   BOOL result = CreateProcess(NULL, cmdLine.GetBuffer(nStrBuffer), 
                               NULL, NULL, FALSE, 
                               NORMAL_PRIORITY_CLASS | CREATE_NO_WINDOW, 
                               NULL, NULL, &startupInfo, &processInformation);
   cmdLine.ReleaseBuffer();


   if (!result)
   {
      LPVOID lpMsgBuf;
      DWORD dw = GetLastError();
      FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS, 
                    NULL, dw, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPTSTR) &lpMsgBuf, 0, NULL);

      CString strError = (LPTSTR) lpMsgBuf;
      //TRACE(_T("::executeCommandLine() failed at CreateProcess()\nCommand=%s\nMessage=%s\n\n"), cmdLine, strError);

      LocalFree(lpMsgBuf);

      return FALSE;
   }
   else
   {
      WaitForSingleObject( processInformation.hProcess, /*INFINITE*/2*60000 );

      result = GetExitCodeProcess(processInformation.hProcess, &exitCode);

      CloseHandle( processInformation.hProcess );
      CloseHandle( processInformation.hThread );

      if (!result)
      {
         //TRACE(_T("Executed command but couldn't get exit code.\nCommand=%s\n"), cmdLine);
         return FALSE;
      }
      return TRUE;
   }
}

HANDLE Utils::GetCurrentUserToken()
{
	HANDLE currentToken = 0;
	HANDLE primaryToken = 0;

	int dwSessionId = 0;

	PWTS_SESSION_INFO pSessionInfo = 0;
	DWORD dwCount = 0;

	// Get the list of all terminal sessions
	WTSEnumerateSessions(WTS_CURRENT_SERVER_HANDLE, 0, 1, &pSessionInfo, &dwCount);

	//int dataSize = sizeof(WTS_SESSION_INFO);

	// look over obtained list in search of the active session
	for (DWORD i = 0; i < dwCount; ++i)
	{
		WTS_SESSION_INFO si = pSessionInfo[i];
		if (WTSActive == si.State)
		{
			// If the current session is active � store its ID
			dwSessionId = si.SessionId;
			break;
		}
	}

	// Get token of the logged in user by the active session ID
	BOOL bRet = WTSQueryUserToken(dwSessionId, &currentToken);
	if (bRet == false)
	{
		return 0;
	}

	bRet = DuplicateTokenEx(currentToken, TOKEN_ASSIGN_PRIMARY | TOKEN_ALL_ACCESS, 0, SecurityImpersonation, TokenPrimary, &primaryToken);
	if (bRet == false)
	{
		return 0;
	}
	return primaryToken;
	//return 0;
}

bool Utils::CreateSecurityAttributes(ACCESS_MASK amAccessRights, PSECURITY_ATTRIBUTES* ppSecurityAttributes)
{
	PSECURITY_DESCRIPTOR pSecDesc = NULL;
	PSECURITY_ATTRIBUTES pSecAttr = NULL;
	PACL pDacl = NULL;
	PSID pSidEveryone = NULL;
	PSID pSidSystem = NULL;
	DWORD dwSidSize = SECURITY_MAX_SID_SIZE;
	EXPLICIT_ACCESS ExplicitAccess [2];
	DWORD dwResult = ERROR_SUCCESS;

	//
	// Check input parameters
	//

	if (! ppSecurityAttributes) return false;

	//
	// Allocate security attributes
	//

	pSecAttr = (PSECURITY_ATTRIBUTES) HeapAlloc (GetProcessHeap (), HEAP_ZERO_MEMORY, sizeof (SECURITY_ATTRIBUTES) * 2);
	if (! pSecAttr) return false;

	//
	// Allocate security descriptor
	//

	pSecDesc = (PSECURITY_DESCRIPTOR) HeapAlloc (GetProcessHeap (), HEAP_ZERO_MEMORY, SECURITY_DESCRIPTOR_MIN_LENGTH * 2);
	if (! pSecDesc)
	{
		HeapFree (GetProcessHeap (), 0, pSecAttr);
		return false;
	}

	//
	// Initialize security descriptor
	//

	if (! InitializeSecurityDescriptor (
		pSecDesc,
		SECURITY_DESCRIPTOR_REVISION))
	{
		HeapFree (GetProcessHeap (), 0, pSecAttr);
		HeapFree (GetProcessHeap (), 0, pSecDesc);
		return false;
	}

	//
	// Allocate storage for SIDs
	//

	pSidEveryone = (PSID) HeapAlloc (GetProcessHeap (), HEAP_ZERO_MEMORY, dwSidSize);
	if (! pSidEveryone)
	{
		HeapFree (GetProcessHeap (), 0, pSecAttr);
		HeapFree (GetProcessHeap (), 0, pSecDesc);
		return false;
	}

	pSidSystem = (PSID) HeapAlloc (GetProcessHeap (), HEAP_ZERO_MEMORY, dwSidSize);
	if (! pSidSystem)
	{
		HeapFree (GetProcessHeap (), 0, pSecAttr);
		HeapFree (GetProcessHeap (), 0, pSecDesc);
		HeapFree (GetProcessHeap (), 0, pSidEveryone);
		return false;
	}

	//
	// Get SID for any user
	//

	if (! CreateWellKnownSid (
		WinWorldSid,
		NULL,
		pSidEveryone,
		&dwSidSize))
	{
		HeapFree (GetProcessHeap (), 0, pSecAttr);
		HeapFree (GetProcessHeap (), 0, pSecDesc);
		HeapFree (GetProcessHeap (), 0, pSidEveryone);
		HeapFree (GetProcessHeap (), 0, pSidSystem);
		return false;
	}

	//
	// Get SID for 'SYSTEM' account
	//

	dwSidSize = SECURITY_MAX_SID_SIZE;

	if (! CreateWellKnownSid (
		WinLocalSystemSid,
		NULL,
		pSidSystem,
		&dwSidSize))
	{
		HeapFree (GetProcessHeap (), 0, pSecAttr);
		HeapFree (GetProcessHeap (), 0, pSecDesc);
		HeapFree (GetProcessHeap (), 0, pSidEveryone);
		HeapFree (GetProcessHeap (), 0, pSidSystem);
		return false;
	}

	//
	// 'Everyone' account
	//

	ExplicitAccess [0].grfAccessPermissions = amAccessRights;
	ExplicitAccess [0].grfAccessMode = GRANT_ACCESS;
	ExplicitAccess [0].grfInheritance = CONTAINER_INHERIT_ACE | OBJECT_INHERIT_ACE | SUB_CONTAINERS_AND_OBJECTS_INHERIT;
	ExplicitAccess [0].Trustee.TrusteeForm = TRUSTEE_IS_SID;
	ExplicitAccess [0].Trustee.TrusteeType = TRUSTEE_IS_WELL_KNOWN_GROUP;
	ExplicitAccess [0].Trustee.ptstrName  = (LPTSTR) pSidEveryone;

	//
	// 'SYSTEM' account
	//

	ExplicitAccess [1].grfAccessPermissions = amAccessRights;
	ExplicitAccess [1].grfAccessMode = GRANT_ACCESS;
	ExplicitAccess [1].grfInheritance = CONTAINER_INHERIT_ACE | OBJECT_INHERIT_ACE | SUB_CONTAINERS_AND_OBJECTS_INHERIT;
	ExplicitAccess [1].Trustee.TrusteeForm = TRUSTEE_IS_SID;
	ExplicitAccess [1].Trustee.TrusteeType = TRUSTEE_IS_WELL_KNOWN_GROUP;
	ExplicitAccess [1].Trustee.ptstrName  = (LPTSTR) pSidSystem;

	//
	// Set up ACL object
	//

	dwResult = SetEntriesInAcl (
		sizeof (ExplicitAccess) / sizeof (EXPLICIT_ACCESS),
		ExplicitAccess,
		NULL,
		&pDacl);

	if (dwResult != ERROR_SUCCESS)
	{
		HeapFree (GetProcessHeap (), 0, pSecAttr);
		HeapFree (GetProcessHeap (), 0, pSecDesc);
		HeapFree (GetProcessHeap (), 0, pSidEveryone);
		HeapFree (GetProcessHeap (), 0, pSidSystem);
		return false;
	}

	//
	// Update security descriptor
	//

	if (! SetSecurityDescriptorDacl (pSecDesc, TRUE, pDacl, TRUE))
	{
		HeapFree (GetProcessHeap (), 0, pSecAttr);
		HeapFree (GetProcessHeap (), 0, pSecDesc);
		HeapFree (GetProcessHeap (), 0, pSidEveryone);
		HeapFree (GetProcessHeap (), 0, pSidSystem);
		LocalFree (pDacl);
		return false;
	}

	//
	// Compose security attributes
	//

	pSecAttr -> bInheritHandle = FALSE;
	pSecAttr -> nLength = sizeof (SECURITY_ATTRIBUTES);
	pSecAttr -> lpSecurityDescriptor = pSecDesc;

	//
	// Return security security
	//

	*ppSecurityAttributes = pSecAttr;

	//
	// Success ;)
	//

	return true;
}

void Utils::UninstallService()
{
	TCHAR serviceExeName[_MAX_PATH];
	GetModuleFileName(NULL,serviceExeName,_MAX_PATH);
	Utils::ShellExecuteAndWait(0,0,serviceExeName, _T("-u"), 0, SW_HIDE);
}

void Utils::CloseService()
{
	TCHAR serviceExeName[_MAX_PATH];
	GetModuleFileName(NULL,serviceExeName,_MAX_PATH);
	Utils::ShellExecuteAndWait(0,0,serviceExeName, _T("-k"), 0, SW_HIDE);
}



HANDLE Utils::GetTokenOfLoggedOnUser()
{
	HANDLE hToken;
	HANDLE hProcess;
	DWORD dwPID;

	std::vector<DWORD> SetOfPID;
	GetProcessID(_T("explorer"),SetOfPID);
	if (SetOfPID.empty())		// Process is not running
	{
		return NULL;
	}
	else					// Process is running
	{
		dwPID=SetOfPID[0];
	}
	// find PID of 'explorer.exe'
	// HOWTO: Enumerate Applications in Win32
	// http://support.microsoft.com/support/kb/articles/Q175/0/30.ASP

	hProcess    =   OpenProcess (   PROCESS_ALL_ACCESS,
		TRUE,
		dwPID
		);

	if  (   !OpenProcessToken   (   hProcess,
		TOKEN_QUERY,
		&hToken
		)
		)   return  (   INVALID_HANDLE_VALUE);

	return ( hToken);
}

DWORD Utils::GetPIDOfLoggedOnUser()
{
//	HANDLE hToken;
//	HANDLE hProcess;
	DWORD dwPID;

	std::vector<DWORD> SetOfPID;
	GetProcessID(_T("explorer"),SetOfPID);
	if (SetOfPID.empty())		// Process is not running
	{
		return NULL;
	}
	else
	{
		dwPID=SetOfPID[0];
	}
	return dwPID;
}

HKEY Utils::GetAppRegistryKey()
{
	HKEY hAppKey = NULL;
	HKEY hCompanyKey = Utils::GetCompanyRegistryKey();
	RegOpenKeyEx(hCompanyKey, sProfileName, 0, KEY_WRITE|KEY_READ,&hAppKey);
	if (hCompanyKey != NULL)
		RegCloseKey(hCompanyKey);
	return hAppKey;
}

bool Utils::appRegistryKeyExists()
{
	if (!Utils::GetAppRegistryKey())
	{
		return false;
	}
	return true;
}

DWORD Utils::loadProperty(void)
{
	DWORD result=1;
	CRegKey keyAppID;
	keyAppID.Attach(Utils::GetAppRegistryKey());
	//TCHAR    achKey[MAX_KEY_LENGTH];   // buffer for subkey name
	//DWORD    cbName;                   // size of name string
	TCHAR    achClass[MAX_PATH] = TEXT("");  // buffer for class name
	DWORD    cchClassName = MAX_PATH;  // size of class string
	DWORD    cSubKeys=0;               // number of subkeys
	DWORD    cbMaxSubKey;              // longest subkey size
	DWORD    cchMaxClass;              // longest class string
	DWORD    cValues;              // number of values for key
	DWORD    cchMaxValue;          // longest value name
	DWORD    cbMaxValueData;       // longest value data
	DWORD    cbSecurityDescriptor; // size of security descriptor
	FILETIME ftLastWriteTime;      // last write time

	DWORD i, retCode;

	TCHAR  *achValue = new TCHAR[MAX_VALUE_NAME];
	DWORD cchValue = MAX_VALUE_NAME;

    TCHAR  *cValueStr = new TCHAR[MAX_VALUE_NAME];
	DWORD pdwCountStr = MAX_VALUE_NAME;

	// Get the class name and the value count.
	retCode = RegQueryInfoKey(
		keyAppID.m_hKey,         // key handle
		achClass,                // buffer for class name
		&cchClassName,           // size of class string
		NULL,                    // reserved
		&cSubKeys,               // number of subkeys
		&cbMaxSubKey,            // longest subkey size
		&cchMaxClass,            // longest class string
		&cValues,                // number of values for this key
		&cchMaxValue,            // longest value name
		&cbMaxValueData,         // longest value data
		&cbSecurityDescriptor,   // security descriptor
		&ftLastWriteTime);       // last write time

	// Enumerate the subkeys, until RegEnumKeyEx fails.
	if (cValues)
	{
		propString.clear();
		for (i=0, retCode=ERROR_SUCCESS; i<cValues; i++)
		{
			cchValue = MAX_VALUE_NAME;
			achValue[0] = '\0';

			retCode = RegEnumValue(keyAppID.m_hKey, i,
				achValue,
				&cchValue,
				NULL,
				NULL,
				NULL,
				NULL);

			if (retCode == ERROR_SUCCESS )
			{
				pdwCountStr = MAX_VALUE_NAME;
				cValueStr[0] = '\0';
				if(ERROR_SUCCESS==keyAppID.QueryStringValue(achValue, cValueStr, &pdwCountStr))
				{
					propString.insert(std::pair<CString,CString>(achValue,cValueStr));
				}
			}
		}
	}
	else
	{
		result=0;
	}
	keyAppID.Close();
	return result;
}

CString Utils::getValue(const CString& s,const CString& def)
{
	CRegKey keyAppID;
	keyAppID.Attach(Utils::GetAppRegistryKey());
	TCHAR cValue[1024];
	DWORD pdwCount = 1000;

	if(ERROR_SUCCESS==keyAppID.QueryStringValue(s, cValue, &pdwCount))
	{
		keyAppID.Close();
		return cValue;
	}
	keyAppID.Close();
	return def;
}

CString* Utils::getValue(const CString& s)
{
	CRegKey keyAppID;
	keyAppID.Attach(Utils::GetAppRegistryKey());
	TCHAR cValue[1024];
	DWORD pdwCount = 1000;

	if(ERROR_SUCCESS==keyAppID.QueryStringValue(s, cValue, &pdwCount))
	{
		keyAppID.Close();
		return new CString(cValue);
	}
	keyAppID.Close();
	return NULL;
}

void Utils::setValue(const CString& s,const CString& val)
{
	CRegKey keyAppID;
	keyAppID.Attach(Utils::GetAppRegistryKey());
	keyAppID.SetStringValue(s, val);
	keyAppID.Close();
}

CString Utils::getPropertyString(CString& propertyName,std::map<CString,CString> &properties, CString *def)
{
	CString res = def?*def:propertyName;

	std::map<CString,CString>::iterator it = properties.find(propertyName);
	if (it!=properties.end())
	{
		res = it->second;
		res = buildPropertyString(res, properties);
	}

	return res;
}

void Utils::setPropertyString(CString& promertyName,CString& promertyValue,BOOL replace)
{
	if (replace) propString.erase(promertyName);
	if (propString.find(promertyName) == propString.end()) Utils::setValue(promertyName, promertyValue);
	propString.insert(std::pair<CString,CString>(promertyName,promertyValue));
}

CString Utils::buildPropertyString(CString& param, std::map<CString,CString> &properties)
{
	CString promertyVal = param;
	int st=0;
	while(1)
	{
		st = promertyVal.Find('%', st);
		if (st<0) break;
		int fn = promertyVal.Find('%', st+1);
		if (fn<0) break;
		CString innerProp = promertyVal.Mid(st+1,fn-st-1);
		CString innerPropProc = promertyVal.Mid(st,fn-st+1);

		CString newPropVal = getPropertyString(innerProp,properties);
		if (newPropVal != innerProp)
			promertyVal.Replace(innerPropProc,newPropVal);
		st = fn+1;
	}
	return promertyVal;
}

DWORD Utils::sendRequest(CString szURL, bool skipHttpsCheck /*= true*/)
{
	DWORD result=0;
	CUrl cUrl;
	if (cUrl.CrackUrl(szURL))
	{
		SetLastError(0);
		HINTERNET hInternet =
			::InternetOpen(
			TEXT("Alerts"),
			INTERNET_OPEN_TYPE_PRECONFIG,
			NULL,NULL,
			0);
		if (hInternet != NULL)
		{
			SetLastError(0);
			HINTERNET hConnect =
				::InternetConnect(
				hInternet,
				cUrl.GetHostName(),
				cUrl.GetPortNumber(),
				//INTERNET_DEFAULT_HTTP_PORT,
				NULL,NULL,
				INTERNET_SERVICE_HTTP,
				0,
				1);
			if (hConnect != NULL)
			{
				CString sUrl = cUrl.GetUrlPath();
				sUrl += cUrl.GetExtraInfo();

				DWORD dwFlags=INTERNET_FLAG_KEEP_CONNECTION|INTERNET_FLAG_RELOAD|INTERNET_FLAG_NO_CACHE_WRITE|INTERNET_FLAG_PRAGMA_NOCACHE;

				if(cUrl.GetScheme() == ATL_URL_SCHEME_HTTPS)
				{
					dwFlags|=INTERNET_FLAG_SECURE;
				}

				SetLastError(0);
				HINTERNET hRequest =
					::HttpOpenRequest(
					hConnect,
					TEXT("GET"),
					sUrl,
					NULL,
					NULL,
					0,
					dwFlags,
					NULL);

				if (hRequest != NULL)
				{
					if(cUrl.GetScheme() == ATL_URL_SCHEME_HTTPS)
					{
						if(skipHttpsCheck)
						{
							DWORD dwFlags2=0;
							DWORD dwSize = sizeof(dwFlags2);
							InternetQueryOption (hRequest, INTERNET_OPTION_SECURITY_FLAGS, (LPVOID)&dwFlags2, &dwSize);
							dwFlags2 |= SECURITY_FLAG_IGNORE_UNKNOWN_CA|SECURITY_FLAG_IGNORE_REVOCATION|SECURITY_FLAG_IGNORE_CERT_CN_INVALID|SECURITY_FLAG_IGNORE_CERT_DATE_INVALID|SECURITY_FLAG_IGNORE_WRONG_USAGE;
							InternetSetOption(hRequest, INTERNET_OPTION_SECURITY_FLAGS, &dwFlags2, sizeof(dwFlags2));
						}
						DWORD pCert=0;
						InternetSetOption(hRequest,INTERNET_OPTION_CLIENT_CERT_CONTEXT,(LPVOID)&pCert,sizeof(DWORD));
					}

					BOOL bSend;
					DWORD dwError;
					PCCERT_CONTEXT pCert = NULL;
					HCERTSTORE hSystemStore = NULL;
					do
					{
						SetLastError(0);
						bSend = HttpSendRequest(hRequest, NULL, 0, NULL, 0);
						dwError = GetLastError();

						if (dwError == ERROR_INTERNET_CLIENT_AUTH_CERT_NEEDED)
						{
							if(!hSystemStore)
							{
								SetLastError(0);
								hSystemStore = CertOpenSystemStore(NULL, _T("MY"));
							}

							SetLastError(0);
                            if (!hSystemStore) break;
							pCert = CertEnumCertificatesInStore(hSystemStore, pCert);

							if(!pCert) break;
							InternetSetOption(hRequest,INTERNET_OPTION_CLIENT_CERT_CONTEXT,(LPVOID)pCert,sizeof(CERT_CONTEXT));
							CertFreeCertificateContext(pCert);
						}
						else
						{
							break;
						}
					} while(true);
					if(hSystemStore)
						CertCloseStore(hSystemStore, CERT_CLOSE_STORE_CHECK_FLAG);
					TCHAR status[32]={0};
					DWORD dwSize = 32;

					BOOL bRet = FALSE;
					int cnt = 0;
					do {
						bRet = HttpQueryInfo(hRequest, HTTP_QUERY_STATUS_CODE, status, &dwSize, NULL);
					} while (!(bRet && _wtol(status) == 200) && (++cnt < 3));

					if(bRet && _wtoi(status) == 200)
					{
						result = 1;
					}
					::InternetCloseHandle(hRequest);
					::InternetCloseHandle(hConnect);
					::InternetCloseHandle(hInternet);
				}
			}
		}
	}
	return result;
}

void Utils::startInternetMonitoring()
{
	g_internetTimer = CreateWaitableTimer(NULL, FALSE, _T("internetTimer"));
	LARGE_INTEGER li;
	const int nTimerUnitsPerSecond = 10000000;
	li.QuadPart = -1;
	li.QuadPart *= nTimerUnitsPerSecond;
    if (g_internetTimer)
    {
        SetWaitableTimer(g_internetTimer, &li, 300000, NULL, NULL, FALSE);
    }
}

CString Utils::GetRegistryKeyName()
{
	CString registryKeyName;
	registryKeyName = (LPCTSTR) sRegistryKey;
	return registryKeyName;
}

CString Utils::GetProfileName()
{
	CString profileName;
	profileName = (LPCTSTR) sProfileName;
	return profileName;
}


CString Utils::allUsersAppDataPath(BOOL env)
{
	CString allUsersPath;
	if(env)
	{
		allUsersPath.GetEnvironmentVariable(_T("ALLUSERSPROFILE"));
	}
	if(!env || allUsersPath.IsEmpty())
	{
		TCHAR commonAppDataPath[MAX_PATH];
		SHGetFolderPath(NULL,CSIDL_COMMON_APPDATA,NULL,SHGFP_TYPE_CURRENT,commonAppDataPath);
		allUsersPath = CString(commonAppDataPath);
	}
	if(allUsersPath.GetAt(allUsersPath.GetLength()-1) != _T('\\'))
	{
		allUsersPath += _T('\\');
	}
	CString deskalertsPath = allUsersPath + _T("DeskAlerts");

	if (!env) CreateDirectory(deskalertsPath,NULL);
	return deskalertsPath;
}

CString Utils::allUsersAppDataProfilePath(BOOL env)
{
	CString allUsersPath = Utils::allUsersAppDataPath(env);
	CString path = allUsersPath + "\\" + Utils::GetRegistryKeyName();
	if (!env) CreateDirectory(path,NULL);
	return path;
}


std::vector<CString> Utils::getUsersDatabaseFiles()
{
	std::vector<CString> files;
	CString allUsersAppDataProfilePath = Utils::allUsersAppDataProfilePath();
	WIN32_FIND_DATA FindFileData;
	const HANDLE hFind = FindFirstFile(allUsersAppDataProfilePath+"\\*",&FindFileData);
	if(hFind != INVALID_HANDLE_VALUE)
	{
		do
		{
			if((FindFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) &&
				_tcscmp(FindFileData.cFileName, _T(".")) && _tcscmp(FindFileData.cFileName, _T("..")))
			{
				CString dbPath=allUsersAppDataProfilePath+"\\"+FindFileData.cFileName +"\\db.dat";
				if (PathFileExists(dbPath))
				{
					files.push_back(dbPath);
				}
			}
		}
		while(FindNextFile(hFind,&FindFileData));
		FindClose(hFind);
	}
	return files;
}

CString Utils::getCommonDatabasePath()
{
	return Utils::allUsersAppDataProfilePath() + "\\db.dat";
}

CString Utils::getDesktopName(HDESK hDesktop)
{
	CString desktopName;
	if (hDesktop)
	{
		DWORD length;
		GetUserObjectInformation(hDesktop, UOI_NAME, NULL, NULL, &length);
		TCHAR *deskBuffer = new TCHAR[length];
		if(GetUserObjectInformation(hDesktop, UOI_NAME, deskBuffer, length, NULL))
		{
			desktopName = deskBuffer;
		}
		deleteArray_catch(deskBuffer);
	}
	return desktopName;
}

CString Utils::getProccessPath(HANDLE processHandle)
{
	CString result;
	TCHAR filename[MAX_PATH];
	if (processHandle != NULL) {
		if (GetModuleFileNameEx(processHandle, NULL, filename, MAX_PATH)) {
			result = filename;
		}
	} 
	return result;
}

BOOL Utils::RecursiveDeleteDirectory(const TCHAR *path)
{
	WIN32_FIND_DATA ffd;
	TCHAR tmp[MAX_PATH+1];
	_tcscpy_s(tmp, MAX_PATH+1, path);
	if(tmp[_tcslen(tmp)-1] != _T('\\'))
	{
		_tcscat_s(tmp, MAX_PATH+1, _T("\\"));
	}
	TCHAR findstr[MAX_PATH+1];
	_tcscpy_s(findstr, MAX_PATH+1, tmp);
	_tcscat_s(findstr, MAX_PATH+1, _T("*"));
	const HANDLE hFind = FindFirstFile(findstr, &ffd);
	if(INVALID_HANDLE_VALUE != hFind)
	{
		do
		{
			TCHAR filepath[MAX_PATH+1];
			_tcscpy_s(filepath, MAX_PATH+1, tmp);
			_tcscat_s(filepath, MAX_PATH+1, ffd.cFileName);
			if(ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			{
				if(_tcscmp(ffd.cFileName, _T(".")) && _tcscmp(ffd.cFileName, _T("..")))
					RecursiveDeleteDirectory(filepath);
			}
			else
			{
				if(ffd.dwFileAttributes & FILE_ATTRIBUTE_READONLY)
				{
					_tchmod(filepath, _S_IWRITE);
				}
				DeleteFile(filepath);
			}
		}
		while(FindNextFile(hFind, &ffd));
		FindClose(hFind);
	}
	tmp[_tcslen(tmp)-1] = _T('\0'); //replace \ char
	return RemoveDirectory(tmp);
}

int Utils::CopyDirectory(CString szSourceDirectory, CString szDestinationDirectory, BOOL destCreate)
{
  CString     strSource;               // Source file
  CString     strDestination;          // Destination file
  CString     strPattern;              // Pattern
  HANDLE          hFile;                   // Handle to file
  WIN32_FIND_DATA FileInformation;         // File information


  strPattern = szSourceDirectory + _T("\\*.*");

  // Create destination directory
  //if(::CreateDirectory(refstrDestinationDirectory.c_str(), 0) == FALSE)
  //  return ::GetLastError();
	//check directory exists
  if( !Utils::DirExists(szDestinationDirectory) )
	  if( destCreate )
	  {
		  if( !CreateDirectory(szDestinationDirectory,NULL) )
			  return 1;
	  }else return 1;

	TCHAR szFullName[MAX_PATH + 1];
	TCHAR szFileName[MAX_PATH];
	TCHAR dir[MAX_PATH];
	TCHAR drive[3];
	TCHAR  ext[5];
	GetModuleFileName(NULL, szFullName, MAX_PATH + 1);
	_wsplitpath_s(szFullName, drive, dir, szFileName, ext);
	 //wcscat_s(szFileName ,ext);

  hFile = ::FindFirstFile(strPattern, &FileInformation);


  if(hFile != INVALID_HANDLE_VALUE)
  {
    do
    {
      if( FileInformation.cFileName[0] != '.' 
		  && wcsstr(FileInformation.cFileName,szFileName)==NULL 
		  && wcscmp(FileInformation.cFileName,_T("skins"))
		  //&& wcscmp(FileInformation.cFileName,_T("conf.xml"))
		  !=0 )
      {
        strSource      = szSourceDirectory + "\\" + FileInformation.cFileName;
        strDestination = szDestinationDirectory + "\\" + FileInformation.cFileName;

        if(FileInformation.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
        {
		  // Copy subdirectory
		  if (CopyDirectory(strSource, strDestination, destCreate))
		  {
		    ::FindClose(hFile);
		    return 0;
		  }
        }
        else
        {
		  // Copy file
		  if (::CopyFile(strSource, strDestination, False) == FALSE)
		  {
		    ::FindClose(hFile);
		    return ::GetLastError();
		  }
        }
      }
    } while(::FindNextFile(hFile, &FileInformation) != FALSE);

    // Close handle
    ::FindClose(hFile);

    DWORD dwError = ::GetLastError();
    if(dwError != ERROR_NO_MORE_FILES)
      return dwError;
  }

  return 0;
}
/*
void Utils::ZipDirectory(TCHAR *sPath, TCHAR *zipFileName)
{
	HZIP hz;
	hz = CreateZip(zipFileName, 0); 
	//if (hz == 0) msg(_T("* Failed to create folders.zip"));

	Utils::ZipDir(sPath, hz);
	CloseZip(hz);
}

void Utils::UnZipDirectory(TCHAR *zipFileName, TCHAR *destPath)
{
	HZIP hz;
	ZRESULT zr;
	ZIPENTRY ze;

	hz = OpenZip(zipFileName, 0);
	if (hz == 0) return;

	zr = SetUnzipBaseDir(hz, destPath);
	GetZipItem(hz, 0, &ze); 
	zr=ZR_MORE;
	int index = 0;
	while (zr == ZR_MORE)
	{
		zr = GetZipItem(hz, index, &ze); 
		if (zr != ZR_OK) break;
		zr = UnzipItem(hz, index++, ze.name); 
		if (zr != ZR_OK) continue;
	}
	zr = CloseZip(hz); 
}

void Utils::ZipDir(TCHAR *sPath,  HZIP hz) {
	WIN32_FIND_DATA pFILEDATA;
	ZRESULT zr;
	
	TCHAR src[MAX_PATH]; //	wsprintf(src, _T("\\temp\\%s"), fdat.cFileName);
	TCHAR dst[MAX_PATH]; //wsprintf(dst, _T("out_%s"), fdat.cFileName);
	TCHAR findWhat[MAX_PATH]; 
	wsprintf(findWhat, _T("%s\\*.*"),sPath);

	HANDLE hFile = FindFirstFile( findWhat, &pFILEDATA);

	if (hFile != INVALID_HANDLE_VALUE) {
		//CHAR * chBuf;
		findWhat[wcslen(findWhat) - wcslen(wcsstr(findWhat,_T("*.*")))] = '\0';
		do {
			if (wcslen(pFILEDATA.cFileName) == 1 && wcschr(pFILEDATA.cFileName, '.') != NULL)
				if (FindNextFile(hFile, &pFILEDATA) == 0)
					break;
			if (wcslen(pFILEDATA.cFileName) == 2 && wcsstr(pFILEDATA.cFileName, _T("..")) != NULL)
				if (FindNextFile(hFile, &pFILEDATA) == 0)
					break;
			if (pFILEDATA.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
				zr = ZipAddFolder(hz, sPath);
				wcscat_s( findWhat, wcslen(findWhat) + wcslen(pFILEDATA.cFileName)+1, pFILEDATA.cFileName );
				ZipDir( findWhat, hz );

				findWhat[wcslen(findWhat) - wcslen(pFILEDATA.cFileName) - 1] = '\0';
			}
			else {
				wsprintf(src, _T("%s%s"), findWhat, pFILEDATA.cFileName);
				wsprintf(dst, _T("%s"), pFILEDATA.cFileName);

				zr = ZipAdd(hz, src, src);
				//if (zr != ZR_OK) 
				//	msg(  pFILEDATA.cFileName );
				//else msg(sFullName);
				
			}
		} while (FindNextFile(hFile, &pFILEDATA));
	}
}
*/

bool Utils::DirExists( const CString fname )
{
    if( wcslen(fname) == 0 )
    {
        return false;
    }
    DWORD dwAttrs = ::GetFileAttributesW( fname );
    if( dwAttrs == DWORD(-1) )
    {
        DWORD dLastError = GetLastError();
        if(    ERROR_TOO_MANY_NAMES == dLastError 
            || ERROR_SHARING_VIOLATION == dLastError 
            || ERROR_TOO_MANY_SESS == dLastError 
            || ERROR_SHARING_BUFFER_EXCEEDED == dLastError )
        {
            return true;
        }else
        {
            return false;
        }
    }
    return true;
}

void Utils::IncremetFailedAttemtCount(CString version)
{

	HKEY hKey;
	//CString regPath= _T(INSTALL_ATTEMPTS_REG_KEY) ;
	DWORD nData=0;
	DWORD dwBufferSize(sizeof(DWORD));
	DWORD dwDisposition;
	TCHAR regVersion[256] = {0};
	DWORD dwSize = MAX_PATH;
	
	LONG lRes = RegOpenKeyEx(HKEY_LOCAL_MACHINE, sAutoUpdateRegKey, 0, KEY_WRITE|KEY_READ|KEY_WOW64_32KEY, &hKey);	

	if(lRes != ERROR_SUCCESS){
		lRes = RegCreateKeyEx(HKEY_LOCAL_MACHINE, sAutoUpdateRegKey, 0, NULL, 0 ,
			KEY_WRITE|KEY_WOW64_32KEY, NULL, &hKey, &dwDisposition);
		if( lRes != ERROR_SUCCESS )return;
	}else{
		lRes = ::RegQueryValueEx( hKey, _T("version"), 
			0, 
			NULL,
			(LPBYTE)regVersion, 
			&dwSize);
			
		if( lRes == ERROR_SUCCESS  )
		{
			if( version == regVersion ){
				lRes = ::RegQueryValueExW(hKey, _T("attempt_count"),
				0,
				NULL,
				/*reinterpret_cast<LPBYTE>*/(LPBYTE)(&nData),
				&dwBufferSize);
				if (lRes!= ERROR_SUCCESS ){
					nData=0;
				}
			}
		}
	}
	nData++;
	RegSetValueEx (hKey, _T("attempt_count"), 0, REG_DWORD, (const BYTE*)&nData, dwBufferSize);
	RegSetValueEx (hKey, _T("version"), 0, REG_SZ, (BYTE *)(version.GetBuffer()), _tcslen(version) * sizeof(TCHAR));
    //if (lRes == ERROR_SUCCESS) {
    RegCloseKey(hKey);
		
	return ;
}

// Gets OS version info from registry
void Utils::GetOsVersion(OSVERSIONINFO *versionInformation)
{
	HKEY hKey;
	DWORD dwType;
	if (RegOpenKeyEx(HKEY_LOCAL_MACHINE, L"SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion", NULL, KEY_READ, &hKey) == ERROR_SUCCESS)
	{
		TCHAR lpData[1024] = { 0 };
		DWORD bufSize = sizeof(lpData);

		if (RegQueryValueEx(hKey, L"CurrentVersion", NULL, &dwType, (LPBYTE)lpData, &bufSize) == ERROR_SUCCESS)
		{
			CString strVersionNo = CString(lpData);
			int sepPos = strVersionNo.Find('.');
			CString strMajorVer = strVersionNo.Left(sepPos);
			CString strMinorVer = strVersionNo.Right(strVersionNo.GetLength() - sepPos - 1);

			versionInformation->dwMajorVersion = atoi((char*)strMajorVer.GetBuffer());
			versionInformation->dwMinorVersion = atoi((char*)strMinorVer.GetBuffer());
		}
		RegCloseKey(hKey);
	}
}

//
// Disable Windows 10 lockscreen via registry
//
void Utils::DisableW10Lockscreen()
{
	OSVERSIONINFO versionInfo;
	GetOsVersion(&versionInfo);
	if (versionInfo.dwMajorVersion == 6 && versionInfo.dwMinorVersion == 3) // Windows 10 detected
	{
		HKEY hKey;
		DWORD dwKeyValue = 1;
		if (RegOpenKeyEx(HKEY_LOCAL_MACHINE, L"SOFTWARE\\Policies\\Microsoft\\Windows", NULL, KEY_ALL_ACCESS, &hKey) == ERROR_SUCCESS)
		{
			HKEY hPersonalization = NULL;
			if (RegCreateKeyEx(hKey, L"Personalization", 0, 0, 0, KEY_ALL_ACCESS, NULL, &hPersonalization, 0) == ERROR_SUCCESS) {
				RegSetValueEx(hPersonalization, L"NoLockScreen", NULL, REG_DWORD, (BYTE*)&dwKeyValue, sizeof(dwKeyValue));
			}
		}
	}
}

BOOL Utils::GetLogonFromToken(HANDLE hToken, _bstr_t& strUser, _bstr_t& strdomain)
{
    DWORD dwSize = MAX_PATH;
    BOOL bSuccess = FALSE;
    DWORD dwLength = 0;
    strUser = "";
    strdomain = "";
    PTOKEN_USER ptu = NULL;
    //Verify the parameter passed in is not NULL.
    if (NULL == hToken)
        goto Cleanup;

    if (!GetTokenInformation(
        hToken,         // handle to the access token
        TokenUser,    // get information about the token's groups 
        (LPVOID)ptu,   // pointer to PTOKEN_USER buffer
        0,              // size of buffer
        &dwLength       // receives required buffer size
    ))
    {
        if (GetLastError() != ERROR_INSUFFICIENT_BUFFER)
            goto Cleanup;

        ptu = (PTOKEN_USER)HeapAlloc(GetProcessHeap(),
            HEAP_ZERO_MEMORY, dwLength);

        if (ptu == NULL)
            goto Cleanup;
    }

    if (!GetTokenInformation(
        hToken,         // handle to the access token
        TokenUser,    // get information about the token's groups 
        (LPVOID)ptu,   // pointer to PTOKEN_USER buffer
        dwLength,       // size of buffer
        &dwLength       // receives required buffer size
    ))
    {
        goto Cleanup;
    }
    SID_NAME_USE SidType;
    LPTSTR lpName = new WCHAR[MAX_PATH];
    LPTSTR lpDomain = new WCHAR[MAX_PATH];;
    if (ptu != nullptr)
    {
        if (!LookupAccountSid(NULL, ptu->User.Sid, lpName, &dwSize, lpDomain, &dwSize, &SidType))
        {
            DWORD dwResult = GetLastError();
            if (dwResult == ERROR_NONE_MAPPED)
            {
            }// wcscpy(lpName, _T("NONE_MAPPED"));
            else
            {
                //printf("LookupAccountSid Error %u\n", GetLastError());
            }
        }
        else
        {
            //LPTSTR as wchar_t, 16-bit UNICODE character
            //printf use char*
            //change printf to wprintf fix it
            wprintf(L"Current user is  %s\\%s\n",
                lpDomain, lpName);
            strUser = lpName;
            strdomain = lpDomain;
            bSuccess = TRUE;
        }
    }
    delete[] lpName;
    delete[] lpDomain;

Cleanup:

    if (ptu != NULL)
        HeapFree(GetProcessHeap(), 0, (LPVOID)ptu);
    return bSuccess;
}

bool Utils::IsProcessExists(const TCHAR* szProcessName)
{
	HANDLE hSnap;
	hSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if (hSnap == NULL)
	{
		return false;
	}
	PROCESSENTRY32 proc;
	proc.dwSize = sizeof(PROCESSENTRY32W);

	if (Process32First(hSnap, &proc))
	{
		do {
			if (!wcscmp(proc.szExeFile, szProcessName))
			{
				return true;
			}
		} while (Process32Next(hSnap, &proc));
	}

	return false;
}
