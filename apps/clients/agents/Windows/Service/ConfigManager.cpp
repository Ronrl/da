#include "StdAfx.h"
#include "ConfigManager.h"

ConfigManager::ConfigManager(CString &configPath)
{
	m_configPath = configPath;

	::CoInitialize(NULL);
	if(SUCCEEDED(m_pXmlDoc.CoCreateInstance(__uuidof(DOMDocument))) && m_pXmlDoc)
	{
		VARIANT_BOOL succeeded;
		if(FAILED(m_pXmlDoc->load(CComVariant(m_configPath) ,&succeeded)) || !succeeded)
		{
			CComPtr<IXMLDOMParseError> errorObj;
			m_pXmlDoc->get_parseError(&errorObj);
			CComBSTR errorMsg;
			errorObj->get_srcText(&errorMsg);
			m_lastError = errorMsg;
		}
	}
}

ConfigManager::~ConfigManager(void)
{
}

CString ConfigManager::getProperty(CString &name, CString *def)
{
	CString result = (def) ? *def : CString();
	if(m_pXmlDoc)
	{
		CComPtr<IXMLDOMElement> pRoot;
		if(SUCCEEDED(m_pXmlDoc->get_documentElement(&pRoot)) && pRoot)
		{
			CComQIPtr<IXMLDOMNode> pPropNode;
			if (SUCCEEDED(pRoot->selectSingleNode(CComBSTR(_T("SETTINGS/PROPERTY[@id='") + name + _T("']")), &pPropNode)) && pPropNode)
			{
				CComQIPtr<IXMLDOMElement> pProp = pPropNode;
				if(pProp)
				{
					CComVariant propVar;
					if(SUCCEEDED(pProp->getAttribute(CComBSTR("default"), &propVar)) && propVar.vt == VT_BSTR)
					{
						result = propVar.bstrVal;
					}
				}
				else
				{
					m_lastError = "Can not parse property";
				}
			}
			else
			{
				m_lastError = "Can not retrieve property";
			}
		}
	}
	return result;
}