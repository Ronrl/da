#include "StdAfx.h"
#include "RegistryMonitor.h"
#include "Utils.h"

DWORD monitoringThreadId(0);

ParcelManager* RegistryMonitor::m_parcelManager = NULL;

RegistryMonitor::RegistryMonitor(void)
{
}

RegistryMonitor::~RegistryMonitor(void)
{
}

DWORD WINAPI RegistryMonitor::notifyThread(LPVOID /*pv*/)
{
	//open key

	while(true)
	{
		HKEY key;
		LSTATUS err;
		HANDLE h;
		key = Utils::GetAppRegistryKey();
		if (!key)
		{
			return 2;
		}

		// Create an event.
		h = CreateEvent(NULL, TRUE, FALSE, NULL);
		if (h == NULL)
		{
			return 2;
		}

		// Watch the registry key for a change of value.
		const DWORD filter = REG_NOTIFY_CHANGE_LAST_SET;
		err = RegNotifyChangeKeyValue(key, TRUE, filter, h, TRUE);
		if (err != ERROR_SUCCESS)
		{
			return 2;
		}

		if(!Utils::appRegistryKeyExists())
		{
			m_parcelManager->sendParcel(RegistryMonitor::createParcel("clientUninstalled"));
			return 2;
		}

		// Wait for an event to occur.
		if (WaitForSingleObjectWithMsgLoop(h, INFINITE) == WAIT_OBJECT_0)
		{
			//some actions
			if(!Utils::appRegistryKeyExists())
			{
				m_parcelManager->sendParcel(RegistryMonitor::createParcel("clientUninstalled"));
				return 2;
			}
		}

		//close key
		RegCloseKey(key);

		//close handle
		if (!CloseHandle(h))
		{
			//return 2;
		}

	}
	return 1;
}

void RegistryMonitor::startRegMonitoring(ParcelManager* parcelManager)
{
	/*const HANDLE hThread = */CreateThread(NULL, 0, RegistryMonitor::notifyThread,NULL, 0, &monitoringThreadId);
	m_parcelManager = parcelManager;
}


Parcel* RegistryMonitor::createParcel(CString eventName,CString keyName,CString value)
{
	RegistryStruct *reg=new RegistryStruct();
	reg->eventName=eventName;
	reg->keyName=keyName;
	reg->value=value;
	RegistryParcelData *registryParcelData= new RegistryParcelData();
	registryParcelData->set((void*)reg);
	Parcel* parcel=new Parcel();
	parcel->setData(registryParcelData);
	parcel->senderName="registryMonitor";
	return parcel;
}

void RegistryParcelData::set(void* data)
{
	m_data=data;
}

void* RegistryParcelData::get()
{
	return m_data;
}