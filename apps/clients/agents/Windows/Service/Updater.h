#pragma once
#include "ClientManager.h"


class Updater
{
public:
	static bool update(PUPDATEDATA psi,ClientManager* clientManager);
private:
	static BOOL updateClient(PUPDATEDATA psi);
	static BOOL updateService(PUPDATEDATA psi);
	static BOOL backupInstallDir(PUPDATEDATA psi);
	static BOOL restoreInstallDir(PUPDATEDATA psi);
};
