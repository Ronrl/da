#include "StdAfx.h"
#include "DatabaseManager.h"

DatabaseManager::DatabaseManager(CString path)
{
	m_Database = NULL;
	m_Database = openDatabase(path);
}

DatabaseManager::~DatabaseManager(void)
{
	CloseDatabase();
}

void DatabaseManager::CloseDatabase()
{
	ATLASSERT(m_Database);
	sqlite3_close(m_Database);
}

sqlite3* DatabaseManager::openDatabase(CString path)
{
	sqlite3* database = NULL;
	int ret = sqlite3_open16((void*)path.GetString(), &database);
	if (SQLITE_OK != ret)
	{
		CString sTmp;
		database = NULL;
	}
	return database;
}

HRESULT DatabaseManager::loadProperty(std::map<CString,CString> &prop)
{
	if (m_Database)
	{
		CString req =_T("SELECT `name`, `value` FROM `property`");

		sqlite3_stmt* stmt;

		ATLASSERT(m_Database);
		int ret = sqlite3_prepare_v2(m_Database, CT2A(req, CP_UTF8), -1, &stmt, 0);
		if (ret == SQLITE_OK)
		{
			while (sqlite3_step(stmt) == SQLITE_ROW)
			{
				CString name, value;
				name = CA2T((char*)sqlite3_column_text(stmt, 0), CP_UTF8);
				value = CA2T((char*)sqlite3_column_text(stmt, 1), CP_UTF8);
				prop.insert(std::pair<CString,CString>(name,value));
			}
		}
		sqlite3_finalize(stmt);
	}
	return S_OK;
}
