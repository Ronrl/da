//  Service.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Utils.h"
#include "RegistryWriter.h"
#include "RegistryMonitor.h"
#include "ParcelManager.h"
#include "Uninstaller.h"
#include "Updater.h"

#include "LockscreenManager.h"

#define		MAX_NUM_OF_PROCESS		4
/** Window Service **/
VOID ServiceMainProc();
VOID Install(TCHAR* pPath, TCHAR* pName);
VOID Uninstall(TCHAR* pName);
VOID WriteLog(TCHAR* pFile, TCHAR* pMsg);
BOOL KillService(TCHAR* pName);
BOOL RunService(TCHAR* pName);
VOID ExecuteSubProcess();
VOID ProcMonitorThread(VOID *);
BOOL StartProcess(int ProcessIndex);
VOID EndProcess(int ProcessIndex);
VOID AttachProcessNames();

VOID WINAPI ServiceMain(DWORD dwArgc, LPTSTR *lpszArgv);
VOID WINAPI ServiceHandler(DWORD fdwControl);


/** Window Service **/
const int nBufferSize = 500;
TCHAR pServiceName[nBufferSize+1];
TCHAR pExeFile[nBufferSize+1];
TCHAR lpCmdLineData[nBufferSize+1];
TCHAR pLogFile[nBufferSize+1];
BOOL ProcessStarted = TRUE;

CRITICAL_SECTION		myCS;
SERVICE_TABLE_ENTRY		lpServiceStartTable[] =
{
	{pServiceName, ServiceMain},
	{NULL, NULL}
};

/*LPTSTR ProcessNames[] = { "C:\\n.exe",
						 "C:\\a.exe",
						 "C:\\b.exe",
						 "C:\\c.exe" };*/
LPTSTR ProcessNames[MAX_NUM_OF_PROCESS];

SERVICE_STATUS_HANDLE   hServiceStatusHandle;
SERVICE_STATUS          ServiceStatus;
PROCESS_INFORMATION	pProcInfo[MAX_NUM_OF_PROCESS];

int _tmain(int argc, _TCHAR* argv[])
{
	if(argc >= 2)
		_tcscpy_s(lpCmdLineData, sizeof(lpCmdLineData)/sizeof(TCHAR), argv[1]);
	ServiceMainProc();
	return 0;
}

VOID ServiceMainProc()
{
	::InitializeCriticalSection(&myCS);
	// initialize variables for .exe and .log file names
	TCHAR pModuleFile[nBufferSize+1];
	DWORD dwSize = GetModuleFileName(NULL, pModuleFile, nBufferSize);
	pModuleFile[dwSize] = 0;
	if(dwSize>4 && pModuleFile[dwSize-4] == '.')
	{
		_stprintf_s(pExeFile, sizeof(pExeFile)/sizeof(TCHAR), _T("%s"), pModuleFile);
		pModuleFile[dwSize-4] = 0;
		_stprintf_s(pLogFile, sizeof(pLogFile)/sizeof(TCHAR), _T("%s.log"), pModuleFile);
	}
	_tcscpy_s(pServiceName,sizeof(pServiceName)/sizeof(TCHAR),_T("DeskAlerts_Service"));

	if(_tcsicmp(_T("-i"),lpCmdLineData) == 0 || _tcsicmp(_T("/i"),lpCmdLineData) == 0)
		Install(pExeFile, pServiceName);
	else if(_tcsicmp(_T("-k"),lpCmdLineData) == 0 || _tcsicmp(_T("/k"),lpCmdLineData) == 0)
		KillService(pServiceName);
	else if(_tcsicmp(_T("-u"),lpCmdLineData) == 0 || _tcsicmp(_T("/u"),lpCmdLineData) == 0)
		Uninstall(pServiceName);
	else if(_tcsicmp(_T("-s"),lpCmdLineData) == 0 || _tcsicmp(_T("/s"),lpCmdLineData) == 0)
		RunService(pServiceName);
	else if(_tcsicmp(_T("-is"),lpCmdLineData) == 0 || _tcsicmp(_T("/is"),lpCmdLineData) == 0 ||
		_tcsicmp(_T("-si"),lpCmdLineData) == 0 || _tcsicmp(_T("/si"),lpCmdLineData) == 0)
	{
		Install(pExeFile, pServiceName);
		RunService(pServiceName);
	}
	else if(_tcsicmp(_T("-ku"),lpCmdLineData) == 0 || _tcsicmp(_T("/ku"),lpCmdLineData) == 0 ||
		_tcsicmp(_T("-uk"),lpCmdLineData) == 0 || _tcsicmp(_T("/uk"),lpCmdLineData) == 0)
	{
		KillService(pServiceName);
		Uninstall(pServiceName);
	}
	else
		ExecuteSubProcess();
}

VOID Install(TCHAR* pPath, TCHAR* pName)
{
	SC_HANDLE schSCManager = OpenSCManager( NULL, NULL, SC_MANAGER_CREATE_SERVICE);
	const size_t TEMP_SIZE = 121;
	TCHAR pTemp[TEMP_SIZE];
	if (schSCManager==0)
	{
		long nError = GetLastError();
		_stprintf_s(pTemp, _T("OpenSCManager failed, error code = %d\n"), nError);
		WriteLog(pLogFile, pTemp);
		return;
	}
	TCHAR escaped_path[MAX_PATH];
	_stprintf_s(escaped_path, _T("\"%s\""), pPath);
	SC_HANDLE schService = CreateService
	(
		schSCManager,              /* SCManager database      */
		pName,                     /* name of service         */
		pName,                     /* service name to display */
		SERVICE_ALL_ACCESS,        /* desired access          */
		SERVICE_WIN32_OWN_PROCESS, /* service type            */
		SERVICE_AUTO_START,        /* start type              */
		SERVICE_ERROR_NORMAL,      /* error control type      */
		escaped_path,              /* service's binary        */
		NULL,                      /* no load ordering group  */
		NULL,                      /* no tag identifier       */
		NULL,                      /* no dependencies         */
		NULL,                      /* LocalSystem account     */
		NULL                       /* no password             */
	);
	if (schService==0)
	{
		long nError =  GetLastError();
		_stprintf_s(pTemp, _T("Failed to create service %s, error code = %d\n"), pName, nError);
		WriteLog(pLogFile, pTemp);
		return;
	}

	_stprintf_s(pTemp, _T("Service %s installed\n"), pName);
	WriteLog(pLogFile, pTemp);

	CloseServiceHandle(schService);
	CloseServiceHandle(schSCManager);
}

VOID Uninstall(TCHAR* pName)
{
	SC_HANDLE schSCManager = OpenSCManager( NULL, NULL, SC_MANAGER_ALL_ACCESS);
	if (schSCManager==0)
	{
		long nError = GetLastError();
		TCHAR pTemp[121];
		_stprintf_s(pTemp, sizeof(pTemp)/sizeof(TCHAR), _T("OpenSCManager failed, error code = %d\n"), nError);
		WriteLog(pLogFile, pTemp);
	}
	else
	{
		SC_HANDLE schService = OpenService( schSCManager, pName, SERVICE_ALL_ACCESS);
		if (schService==0)
		{
			long nError = GetLastError();
			TCHAR pTemp[121];
			_stprintf_s(pTemp, sizeof(pTemp)/sizeof(TCHAR), _T("OpenService failed, error code = %d\n"), nError);
			WriteLog(pLogFile, pTemp);
		}
		else
		{
			if(!DeleteService(schService))
			{
				TCHAR pTemp[121];
				_stprintf_s(pTemp, sizeof(pTemp)/sizeof(TCHAR), _T("Failed to delete service %s\n"), pName);
				WriteLog(pLogFile, pTemp);
			}
			else
			{
				TCHAR pTemp[121];
				_stprintf_s(pTemp, sizeof(pTemp)/sizeof(TCHAR), _T("Service %s removed\n"),pName);
				WriteLog(pLogFile, pTemp);
			}
			CloseServiceHandle(schService);
		}
		CloseServiceHandle(schSCManager);
	}
	DeleteFile(pLogFile);
}

VOID WriteLog(TCHAR* pFile, TCHAR* pMsg)
{
	// write error or other information into log file
	::EnterCriticalSection(&myCS);
	release_try
	{
		SYSTEMTIME oT;
		::GetLocalTime(&oT);
		FILE* pLog = NULL;
		_tfopen_s(&pLog, pFile,_T("a"));
		if(pLog)
		{
			//fprintf(pLog,"%02d/%02d/%04d, %02d:%02d:%02d\n    %s",oT.wMonth,oT.wDay,oT.wYear,oT.wHour,oT.wMinute,oT.wSecond, pMsg);
			_ftprintf(pLog, _T("%02d/%02d/%04d, %02d:%02d:%02d\n    %s"), oT.wMonth, oT.wDay, oT.wYear, oT.wHour, oT.wMinute, oT.wSecond, pMsg);
			fclose(pLog);
		}
	}
	release_end_try
	::LeaveCriticalSection(&myCS);
}

BOOL KillService(TCHAR* pName)
{
	// kill service with given name
	SC_HANDLE schSCManager = OpenSCManager( NULL, NULL, SC_MANAGER_ALL_ACCESS);
	if (schSCManager==0)
	{
		long nError = GetLastError();
		TCHAR pTemp[121];
		_stprintf_s(pTemp, sizeof(pTemp)/sizeof(TCHAR), _T("OpenSCManager failed, error code = %d\n"), nError);
		WriteLog(pLogFile, pTemp);
	}
	else
	{
		// open the service
		SC_HANDLE schService = OpenService( schSCManager, pName, SERVICE_ALL_ACCESS);
		if (schService==0)
		{
			long nError = GetLastError();
			TCHAR pTemp[121];
			_stprintf_s(pTemp, sizeof(pTemp)/sizeof(TCHAR), _T("OpenService failed, error code = %d\n"), nError);
			WriteLog(pLogFile, pTemp);
		}
		else
		{
			// call ControlService to kill the given service
			SERVICE_STATUS status;
			if(ControlService(schService,SERVICE_CONTROL_STOP,&status))
			{
				CloseServiceHandle(schService);
				CloseServiceHandle(schSCManager);
				return TRUE;
			}
			else
			{
				long nError = GetLastError();
				TCHAR pTemp[121];
				_stprintf_s(pTemp, sizeof(pTemp)/sizeof(TCHAR), _T("ControlService failed, error code = %d\n"), nError);
				WriteLog(pLogFile, pTemp);
			}
			CloseServiceHandle(schService);
		}
		CloseServiceHandle(schSCManager);
	}
	return FALSE;
}

BOOL RunService(TCHAR* pName)
{
	// run service with given name
	SC_HANDLE schSCManager = OpenSCManager( NULL, NULL, SC_MANAGER_ALL_ACCESS);
	if (schSCManager==0)
	{
		long nError = GetLastError();
		TCHAR pTemp[121];
		_stprintf_s(pTemp, sizeof(pTemp)/sizeof(TCHAR), _T("OpenSCManager failed, error code = %d\n"), nError);
		WriteLog(pLogFile, pTemp);
	}
	else
	{
		// open the service
		SC_HANDLE schService = OpenService( schSCManager, pName, SERVICE_ALL_ACCESS);
		if (schService==0)
		{
			long nError = GetLastError();
			TCHAR pTemp[121];
			_stprintf_s(pTemp, sizeof(pTemp)/sizeof(TCHAR), _T("OpenService failed, error code = %d\n"), nError);
			WriteLog(pLogFile, pTemp);
		}
		else
		{
			// call StartService to run the service
			if(StartService(schService, 0, (const TCHAR**)NULL))
			{
				CloseServiceHandle(schService);
				CloseServiceHandle(schSCManager);
				return TRUE;
			}
			else
			{
				long nError = GetLastError();
				TCHAR pTemp[121];
				_stprintf_s(pTemp, sizeof(pTemp)/sizeof(TCHAR), _T("StartService failed, error code = %d\n"), nError);
				WriteLog(pLogFile, pTemp);
			}
			CloseServiceHandle(schService);
		}
		CloseServiceHandle(schSCManager);
	}
	return FALSE;
}


VOID ExecuteSubProcess()
{
	/*if(_beginthread(ProcMonitorThread, 0, NULL) == -1)
	{
		long nError = GetLastError();
		TCHAR pTemp[121];
		_stprintf_s(pTemp, sizeof(pTemp)/sizeof(TCHAR), _T("StartService failed, error code = %d\n"), nError);
		WriteLog(pLogFile, pTemp);
	}*/
	if(!StartServiceCtrlDispatcher(lpServiceStartTable))
	{
		long nError = GetLastError();
		TCHAR pTemp[121];
		_stprintf_s(pTemp, sizeof(pTemp)/sizeof(TCHAR), _T("StartServiceCtrlDispatcher failed, error code = %d\n"), nError);
		WriteLog(pLogFile, pTemp);
	}
	::DeleteCriticalSection(&myCS);
}

size_t ExecuteProcess(std::wstring FullPathToExe, std::wstring Parameters, size_t SecondsToWait)
{
	size_t iReturnVal = 0, iPos = 0;
	DWORD dwExitCode = 0;
	std::wstring sTempStr = L"";

	/* - NOTE - You should check here to see if the exe even exists */

	/* Add a space to the beginning of the Parameters */
	if (Parameters.size() != 0)
	{
		if (Parameters[0] != L' ')
		{
			Parameters.insert(0,L" ");
		}
	}

	/* The first parameter needs to be the exe itself */
	sTempStr = FullPathToExe;
	iPos = sTempStr.find_last_of(L"\\");
	sTempStr.erase(0, iPos +1);
	Parameters = sTempStr.append(Parameters);

	/* CreateProcessW can modify Parameters thus we allocate needed memory */
	wchar_t * pwszParam = new wchar_t[Parameters.size() + 1];
	const wchar_t* pchrTemp = Parameters.c_str();
	wcscpy_s(pwszParam, Parameters.size() + 1, pchrTemp);

	/* CreateProcess API initialization */
	STARTUPINFOW siStartupInfo;
	PROCESS_INFORMATION piProcessInfo;
	memset(&siStartupInfo, 0, sizeof(siStartupInfo));
	memset(&piProcessInfo, 0, sizeof(piProcessInfo));
	siStartupInfo.cb = sizeof(siStartupInfo);
	siStartupInfo.lpDesktop = _T("winsta0\\default");
	if (CreateProcessW(const_cast<LPCWSTR>(FullPathToExe.c_str()),
		pwszParam, 0, 0, false,
		CREATE_DEFAULT_ERROR_MODE, 0, 0,
		&siStartupInfo, &piProcessInfo) != false)
	{ 
		/* Watch the process. */
		dwExitCode = WaitForSingleObjectWithMsgLoop(piProcessInfo.hProcess, (SecondsToWait * 1000));
	} 
	else 
	{ 
		/* CreateProcess failed */
		iReturnVal = GetLastError();
	} 

	/* Free memory */
	delete_catch([]pwszParam);
	pwszParam = 0;

	/* Release handles */
	CloseHandle(piProcessInfo.hProcess);
	CloseHandle(piProcessInfo.hThread);

	return iReturnVal;
}


bool EnablePrivileges(LPCTSTR * ppPrivs, DWORD dwCnt,
					  PTOKEN_PRIVILEGES* ppOldPrivs = NULL,HANDLE hTok = 0)
{
	bool retval = false;
	PTOKEN_PRIVILEGES pOldPrivs = NULL;
	release_try
	{
		size_t sz = sizeof(TOKEN_PRIVILEGES)+
			sizeof(LUID_AND_ATTRIBUTES)*(dwCnt-1);

		//alloc mem in stack
		PTOKEN_PRIVILEGES pPriv = (PTOKEN_PRIVILEGES)_alloca(sz);
		DWORD RetLen,*pRetLen = NULL;

		//alloc mem for old values
		if (ppOldPrivs != NULL){
			pOldPrivs = (PTOKEN_PRIVILEGES)malloc(sz);
			pRetLen = &RetLen;
		}

		//fill the buff
		pPriv->PrivilegeCount = dwCnt;
		for(DWORD i = 0; i < dwCnt; i++)
		{
			pPriv->Privileges[i].Attributes = SE_PRIVILEGE_ENABLED;
			if (!LookupPrivilegeValue(NULL, ppPrivs[i],
				&pPriv->Privileges[i].Luid))
				throw 1;
		}

		//set rights
		bool bMyTok = false;
		if (!hTok){
			if (!OpenProcessToken(GetCurrentProcess(),
				TOKEN_QUERY|TOKEN_ADJUST_PRIVILEGES,&hTok))
				throw 1;
			bMyTok = true;
		}
		retval = AdjustTokenPrivileges(hTok, TRUE, pPriv, sz,
			pOldPrivs, pRetLen) ? true : false;

		if (bMyTok) CloseHandle(hTok);
	}
	release_end_try

	if (ppOldPrivs != NULL)
		*ppOldPrivs = pOldPrivs;

	return retval;
};

VOID WINAPI ServiceMain(DWORD /*dwArgc*/, LPTSTR* /*lpszArgv*/)
{
	ServiceStatus.dwServiceType        = SERVICE_WIN32;
	ServiceStatus.dwCurrentState       = SERVICE_START_PENDING;
	ServiceStatus.dwControlsAccepted   = SERVICE_ACCEPT_STOP | SERVICE_ACCEPT_SHUTDOWN | SERVICE_ACCEPT_PAUSE_CONTINUE;
	ServiceStatus.dwWin32ExitCode      = 0;
	ServiceStatus.dwServiceSpecificExitCode = 0;
	ServiceStatus.dwCheckPoint         = 0;
	ServiceStatus.dwWaitHint           = 0;

	hServiceStatusHandle = RegisterServiceCtrlHandler(pServiceName, ServiceHandler);
	if (hServiceStatusHandle==0)
	{
		long nError = GetLastError();
		TCHAR pTemp[121];
		_stprintf_s(pTemp, sizeof(pTemp)/sizeof(TCHAR), _T("RegisterServiceCtrlHandler failed, error code = %d\n"), nError);
		WriteLog(pLogFile, pTemp);
		return;
	}

	// Initialization complete - report running status
	ServiceStatus.dwCurrentState       = SERVICE_RUNNING;
	ServiceStatus.dwCheckPoint         = 0;
	ServiceStatus.dwWaitHint           = 0;
	if(!SetServiceStatus(hServiceStatusHandle, &ServiceStatus))
	{
		long nError = GetLastError();
		TCHAR pTemp[121];
		_stprintf_s(pTemp, sizeof(pTemp)/sizeof(TCHAR), _T("SetServiceStatus failed, error code = %d\n"), nError);
		WriteLog(pLogFile, pTemp);
	}

	Utils::DisableW10Lockscreen();

	LockscreenManager::Instance()->CreateRegistryKey();

	ParcelManager *parcelManager= new ParcelManager();

	RegistryMonitor::startRegMonitoring(parcelManager);
	ClientManager *clientManager=new ClientManager("DeskAlerts");
    clientManager->updateSecureDesktopClientsInfo();
	clientManager->StartRecieveMessages(parcelManager);

	Parcel *parcel;
	while((parcel=parcelManager->recieveParcel()))
	{
		if (parcel->senderName=="registryMonitor")
		{
			RegistryStruct* reg=(RegistryStruct*)(parcel->getData()->get());
			if (reg->eventName=="clientUninstalled")
			{
				Uninstaller::serviceUninstall(clientManager->getPropertiesForUsers(),clientManager->getCommonProperties());
			/*	if(Uninstaller::serviceUninstall())
				{
					delete_catch(parcel);
					return;
				}*/
			}
		}
		else if (parcel->senderName=="ClientManager")
		{
            BOOL isPacketSent = FALSE;
			PREQUESTPACKET packet = (PREQUESTPACKET)(parcel->getData()->get());
			DWORD dataType=packet->header.dataType;
			unsigned int packetSize = packet->header.length;
			if (dataType==REQUESTDATATYPE_UPDATE)
			{
				PUPDATEDATA updateData = (PUPDATEDATA)packet->data;
				if (Updater::update(updateData,clientManager))
				{
					//delete_catch(parcel);
					//Utils::UninstallService();
					//Utils::CloseService();
				}
			}
			else if (dataType==REQUESTDATATYPE_REGISTRY)
			{ 
				PREGISTRYDATA regData = (PREGISTRYDATA)packet->data;
				regData->m_strVal = (WCHAR*)(packet->data + sizeof(REGISTRYDATA));
				RegistryWriter::WriteInRegistry(regData);
			}
			else if (dataType==REQUESTDATATYPE_WINDOW)
			{
				PWINDOWDATA windowData;
				windowData = parseWindowData(packet);
                isPacketSent = clientManager->sendShowWindowRequest(windowData, packetSize);
                if (!isPacketSent)
                {
                    parcelManager->sendParcel(clientManager->createParcel((void*)packet));
                }
			}
			else if (dataType==REQUESTDATATYPE_CLOSEWINDOW)
			{
				PCLOSEWINDOWDATA closeWindowData = (PCLOSEWINDOWDATA)(packet->data);
                isPacketSent = clientManager->sendCloseWindowRequest(closeWindowData);
                if (!isPacketSent)
                {
                    parcelManager->sendParcel(clientManager->createParcel((void*)packet));
                }
			}
            if (isPacketSent)
            {
                delete_catch(packet);
            }
		}
		delete_catch(parcel);
	}

    /*
	for(int iLoop = 0; iLoop < MAX_NUM_OF_PROCESS; iLoop++)
	{
		pProcInfo[iLoop].hProcess = 0;
		StartProcess(iLoop);
	}*/
}

/*
VOID AttachProcessNames()
{
	LPTSTR lpszpath;
	lpszpath = new TCHAR[nBufferSize];
	memset(lpszpath,0x00,sizeof(lpszpath));
	DWORD dwSize = GetModuleFileName(NULL, lpszpath, nBufferSize);
	lpszpath[dwSize] = 0;
	while(lpszpath[dwSize] != '\\' && dwSize != 0)
	{
		lpszpath[dwSize] = 0; dwSize--;
	}
	for(int iLoop = 0; iLoop < MAX_NUM_OF_PROCESS; iLoop++)
	{
		ProcessNames[iLoop] = (TCHAR*) malloc(nBufferSize);
		memset(ProcessNames[iLoop], 0x00, nBufferSize);
		_tcscpy(ProcessNames[iLoop],lpszpath);
	}
	strcat(ProcessNames[0], "1.exe");
	strcat(ProcessNames[1], "2.exe");
	strcat(ProcessNames[2], "3.exe");
	strcat(ProcessNames[3], "4.exe");
}
*/



VOID WINAPI ServiceHandler(DWORD fdwControl)
{
	switch(fdwControl)
	{
		case SERVICE_CONTROL_STOP:
		case SERVICE_CONTROL_SHUTDOWN:
			ProcessStarted = FALSE;
			ServiceStatus.dwWin32ExitCode = 0;
			ServiceStatus.dwCurrentState  = SERVICE_STOPPED;
			ServiceStatus.dwCheckPoint    = 0;
			ServiceStatus.dwWaitHint      = 0;
			// terminate all processes started by this service before shutdown
			{
				for(int i = MAX_NUM_OF_PROCESS - 1 ; i >= 0; i--)
				{
					EndProcess(i);
					delete_catch(ProcessNames[i]);
				}
			}
			break;
		case SERVICE_CONTROL_PAUSE:
			ServiceStatus.dwCurrentState = SERVICE_PAUSED;
			break;
		case SERVICE_CONTROL_CONTINUE:
			ServiceStatus.dwCurrentState = SERVICE_RUNNING;
			break;
		case SERVICE_CONTROL_INTERROGATE:
			break;
		default:
			if(fdwControl>=128&&fdwControl<256)
			{
				int nIndex = fdwControl&0x7F;
				// bounce a single process
				if(nIndex < MAX_NUM_OF_PROCESS)
				{
					EndProcess(nIndex);
					StartProcess(nIndex);
				}
				// bounce all processes
				else if(nIndex==127)
				{
					for(int i = MAX_NUM_OF_PROCESS-1; i >= 0; i--)
					{
						EndProcess(i);
					}
					for(int i = 0; i < MAX_NUM_OF_PROCESS; i++)
					{
						StartProcess(i);
					}
				}
			}
			else
			{
				TCHAR pTemp[121];
				_stprintf_s(pTemp, sizeof(pTemp)/sizeof(TCHAR), _T("Unrecognized opcode %d\n"), fdwControl);
				WriteLog(pLogFile, pTemp);
			}
	};
	if (!SetServiceStatus(hServiceStatusHandle, &ServiceStatus))
	{
		long nError = GetLastError();
		TCHAR pTemp[121];
		_stprintf_s(pTemp, sizeof(pTemp)/sizeof(TCHAR), _T("SetServiceStatus failed, error code = %d\n"), nError);
		WriteLog(pLogFile, pTemp);
	}
}

BOOL StartProcess(int ProcessIndex)
{
	STARTUPINFO startUpInfo = { sizeof(STARTUPINFO),NULL,_T(""),NULL,0,0,0,0,0,0,0,STARTF_USESHOWWINDOW,0,0,NULL,0,0,0};
	startUpInfo.wShowWindow = SW_SHOW;
	startUpInfo.lpDesktop = NULL;
	if(CreateProcess(NULL, ProcessNames[ProcessIndex],NULL,NULL,FALSE,NORMAL_PRIORITY_CLASS,\
					 NULL,NULL,&startUpInfo,&pProcInfo[ProcessIndex]))
	{
		Sleep(1000);
		return TRUE;
	}
	else
	{
		long nError = GetLastError();
		TCHAR pTemp[256];
		_stprintf_s(pTemp, sizeof(pTemp)/sizeof(TCHAR),_T("Failed to start program '%s', error code = %d\n"), ProcessNames[ProcessIndex], nError);
		WriteLog(pLogFile, pTemp);
		return FALSE;
	}
}

VOID EndProcess(int ProcessIndex)
{
	if(ProcessIndex >=0 && ProcessIndex < MAX_NUM_OF_PROCESS)
	{
		if(pProcInfo[ProcessIndex].hProcess)
		{
			// post a WM_QUIT message first
			PostThreadMessage(pProcInfo[ProcessIndex].dwThreadId, WM_QUIT, 0, 0);
			Sleep(1000);
			// terminate the process by force
			TerminateProcess(pProcInfo[ProcessIndex].hProcess, 0);
		}
	}
}
VOID ProcMonitorThread(VOID *)
{
	while(ProcessStarted != FALSE)
	{
		DWORD dwCode;
		for(int iLoop = 0; iLoop < MAX_NUM_OF_PROCESS; iLoop++)
		{
			if(::GetExitCodeProcess(pProcInfo[iLoop].hProcess, &dwCode) && pProcInfo[iLoop].hProcess != NULL)
			{
				if(dwCode != STILL_ACTIVE)
				{
					if(StartProcess(iLoop))
					{
						TCHAR pTemp[121];
						_stprintf_s(pTemp, sizeof(pTemp)/sizeof(TCHAR), _T("Restarted process %d\n"), iLoop);
						WriteLog(pLogFile, pTemp);
					}
				}
			}
		}
	}
}
