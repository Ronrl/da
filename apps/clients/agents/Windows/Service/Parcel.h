#pragma once
#include "stdafx.h"
#include "IParcelData.h"

class Parcel
{
public:
	CString senderName;
	Parcel(void);
	~Parcel(void);
	void setData(IParcelData* data);
	IParcelData* getData();
private:
	IParcelData* m_data;
};
