#include "StdAfx.h"
#include "RegistryWriter.h"

namespace RegistryWriter
{
	void RecursiveCopyKey(HKEY old_parent, HKEY new_parent, CString old_name, CString new_name)
	{
		CRegKey old_key, new_key;
		if(old_key.Open(old_parent, old_name, KEY_READ) == ERROR_SUCCESS &&
			new_key.Create(new_parent, new_name, REG_NONE, REG_OPTION_NON_VOLATILE, KEY_WRITE) == ERROR_SUCCESS)
		{
			CString name;
			DWORD index = 0, length, type, data_length = 0;
			while(old_key.EnumKey(index, name.GetBuffer(length = MAX_KEY_LENGTH), &length) == ERROR_SUCCESS)
			{
				name.ReleaseBuffer(length);
				RecursiveCopyKey(old_key, new_key, name, name);
				++index;
			}
			index = 0;
			while(::RegEnumValue(old_key, index, name.GetBuffer(length = MAX_VALUE_NAME), &length, NULL, &type, NULL, &data_length) == ERROR_SUCCESS)
			{
				name.ReleaseBuffer(length);
				LPBYTE data = new BYTE[data_length];
				if(::RegQueryValueEx(old_key, name, NULL, &type, data, &data_length) == ERROR_SUCCESS)
				{
					::RegSetValueEx(new_key, name, NULL, type, data, data_length);
				}
				++index;
			}
		}
	}
	void RecursiveCopyKey(HKEY parent, CString old_name, CString new_name)
	{
		RecursiveCopyKey(parent, parent, old_name, new_name);
	}
	void RecursiveDeleteKey(HKEY parent, CString name)
	{
		CRegKey key;
		if(key.Open(parent, name, KEY_READ|KEY_WRITE) == ERROR_SUCCESS)
		{
			CString sub_name;
			DWORD index = 0, length;
			while(key.EnumKey(index, sub_name.GetBuffer(length = MAX_KEY_LENGTH), &length) == ERROR_SUCCESS)
			{
				sub_name.ReleaseBuffer(length);
				RecursiveDeleteKey(key, sub_name);
				++index;
			}
		}
		key.Close();
		::RegDeleteKey(parent, name);
	}
	void RecursiveRenameKey(HKEY parent, CString old_name, CString new_name)
	{
		RecursiveCopyKey(parent, old_name, new_name);
		RecursiveDeleteKey(parent, old_name);
	}
	void ParseRegXML(HKEY parent, CComPtr<IXMLDOMElement> &pElem)
	{
		CComBSTR tag;
		if(SUCCEEDED(pElem->get_tagName(&tag)) && SUCCEEDED(tag.ToUpper()))
		{
			if(tag == L"VALUE")
			{
				CComVariant vName, vOldName, vValue;
				if(SUCCEEDED(pElem->getAttribute(CComBSTR(L"old_name"), &vOldName)) && vOldName.vt == VT_BSTR)
				{
					::RegDeleteValue(parent, vOldName.bstrVal);
				}
				if(SUCCEEDED(pElem->getAttribute(CComBSTR(L"name"), &vName)) && vName.vt == VT_BSTR &&
					SUCCEEDED(pElem->getAttribute(CComBSTR(L"value"), &vValue)) && vValue.vt == VT_BSTR )
				{
					::RegSetValueEx(parent, vName.bstrVal, NULL, REG_SZ, reinterpret_cast<const BYTE*>(vValue.bstrVal), (lstrlen(vValue.bstrVal)+1)*sizeof(TCHAR));
				}
			}
			else if(tag == L"KEY")
			{
				CRegKey key;
				CComVariant vName, vOldName;
				if(SUCCEEDED(pElem->getAttribute(CComBSTR(L"name"), &vName)) && vName.vt == VT_BSTR)
				{
					if(SUCCEEDED(pElem->getAttribute(CComBSTR(L"old_name"), &vOldName)) && vOldName.vt == VT_BSTR)
					{
						RecursiveRenameKey(parent, vOldName.bstrVal, vName.bstrVal);
					}
					if(key.Create(parent, vName.bstrVal, REG_NONE, REG_OPTION_NON_VOLATILE, KEY_WRITE) == ERROR_SUCCESS)
						parent = key;
				}
				else if(SUCCEEDED(pElem->getAttribute(CComBSTR(L"old_name"), &vOldName)) && vOldName.vt == VT_BSTR)
				{
					RecursiveDeleteKey(parent, vOldName.bstrVal);
					parent = NULL;
				}
				if(parent)
				{
					CComPtr<IXMLDOMNodeList> pList;
					if(SUCCEEDED(pElem->get_childNodes(&pList)) && pList)
					{
						long length = 0;
						if(SUCCEEDED(pList->get_length(&length)) && length > 0)
						{
							for(long i = 0; i < length; i++)
							{
								CComPtr<IXMLDOMNode> pChild;
								if(SUCCEEDED(pList->get_item(i, &pChild)) && pChild)
								{
									CComQIPtr<IXMLDOMElement> pChildElem = pChild;
									if(pChildElem)
									{
										ParseRegXML(parent, pChildElem);
									}
								}
							}
						}
					}
				}
			}
		}
	}
	CRegKey parseRegPath(CString path, CString wow6432, REGSAM samDesired, BOOL create = FALSE)
	{
		if(wow6432 == _T("1") || wow6432.CompareNoCase(_T("true")))
			samDesired |= KEY_WOW64_32KEY;
		else if(wow6432 == _T("0") || wow6432.CompareNoCase(_T("false")))
			samDesired |= KEY_WOW64_64KEY;

		CRegKey result;
		path.Replace('/', '\\');
		int pos = path.Find('\\');
		if(pos > -1)
		{
			HKEY key = NULL;
			CString root = path.Left(pos);
			root.MakeUpper();
			if(root == _T("HKLM") || root == _T("HKEY_LOCAL_MACHINE"))
				key = HKEY_LOCAL_MACHINE;
			else if(root == _T("HKCU") || root == _T("HKEY_CURRENT_USER"))
				key = HKEY_CURRENT_USER;
			else if(root == _T("HKCC") || root == _T("HKEY_CURRENT_CONFIG"))
				key = HKEY_CURRENT_CONFIG;
			else if(root == _T("HKCR") || root == _T("HKEY_CLASSES_ROOT"))
				key = HKEY_CLASSES_ROOT;
			else if(root == _T("HKU") || root == _T("HKEY_USERS"))
				key = HKEY_USERS;
			else if(root == _T("HKEY_PERFORMANCE_DATA"))
				key = HKEY_PERFORMANCE_DATA;
			else if(root == _T("HKEY_DYN_DATA"))
				key = HKEY_DYN_DATA;
			if(key)
			{
				path = path.Mid(pos+1);
				if(result.Open(key, path, samDesired) != ERROR_SUCCESS)
				{
					if(!(samDesired&KEY_WOW64_32KEY) && !(samDesired&KEY_WOW64_64KEY) &&
						result.Open(key, path, samDesired|KEY_WOW64_64KEY) == ERROR_SUCCESS)
					{
						return result;
					}
					if(create)
					{
						result.Create(key, path, REG_NONE, REG_OPTION_NON_VOLATILE, samDesired);
					}
				}
			}
		}
		return result;
	}
	DWORD WriteInRegistry(PREGISTRYDATA psi)
	{
		CRegKey key = parseRegPath(psi->m_path, psi->m_wow6432, KEY_WRITE);
		if(key)
		{
			if(!_tcsicmp(psi->m_format, _T("xml")))
			{
				CComPtr<IXMLDOMDocument> pDoc;
				::CoInitialize(NULL);
				if(SUCCEEDED(pDoc.CoCreateInstance(__uuidof(DOMDocument))) && pDoc)
				{
					VARIANT_BOOL succeeded;
					if(SUCCEEDED(pDoc->loadXML(CComBSTR(psi->m_strVal), &succeeded)) && succeeded)
					{
						CComPtr<IXMLDOMElement> pRoot;
						if(SUCCEEDED(pDoc->get_documentElement(&pRoot)) && pRoot)
						{
							ParseRegXML(key, pRoot);
						}
					}
				}
			}
			else if(!_tcsicmp(psi->m_format, _T("json")))
			{
				
			}
			else
			{
				switch(psi->m_type)
				{
					case REG_SZ:
					case REG_EXPAND_SZ:
						key.SetStringValue(psi->m_name, psi->m_strVal, psi->m_type);
						break;
					case REG_MULTI_SZ:
						key.SetMultiStringValue(psi->m_name, psi->m_strVal);
						break;
					case REG_DWORD:
						key.SetDWORDValue(psi->m_name, psi->m_dwVal);
						break;
					case REG_QWORD:
						key.SetQWORDValue(psi->m_name, psi->m_qwVal);
						break;
					case REG_BINARY:
						key.SetBinaryValue(psi->m_name, psi->m_strVal, psi->m_strLen);
						break;
				}
			}
		}
		return 0;
	}
}