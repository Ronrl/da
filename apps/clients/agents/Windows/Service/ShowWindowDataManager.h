#pragma once
#include "../deskalerts/ServiceDataManager.h"

class ShowWindowDataManager: public ServiceDataManager
{
private:
	PWINDOWDATA m_windowData;
	long m_dataLen;
public:
	ShowWindowDataManager(void);
	~ShowWindowDataManager(void);
	DWORD serialize(IStream* pStream);
	void setData(PWINDOWDATA windowData, long dataLen);
	RequestDataType getDataType();
};
