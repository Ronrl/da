#pragma once
#include "stdafx.h"

class ConfigManager
{
public:
	ConfigManager(CString &configPath);
	~ConfigManager(void);
	CString getProperty(CString &name, CString *def = NULL);
private:
	CString m_configPath;
	CComPtr<IXMLDOMDocument> m_pXmlDoc;
	CString m_lastError;
};
