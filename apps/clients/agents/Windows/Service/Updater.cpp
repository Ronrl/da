#include "StdAfx.h"
#include "Updater.h"
#include "Utils.h"
#include <algorithm>
#include <Wtsapi32.h>


BOOL Updater::updateClient(PUPDATEDATA psi)
{
	TCHAR serviceExeName[_MAX_PATH];
	GetModuleFileName(NULL,serviceExeName,_MAX_PATH);
	//TCHAR *baseName = psi->m_baseName;
	//TCHAR *exeName = psi->m_exeName;
	TCHAR *src = psi->m_src;
	TCHAR *dest = psi->m_dest;
	TCHAR *baseName = psi->m_baseName;

	TCHAR cmdMsiTempl[] = 	_T("msiexec /a \"%sdeskalerts_setup.msi\" /quiet  /passive /qn TARGETDIR=\"%s\"");
	TCHAR dstUnpacked[] = _T("%s\\DeskAlerts");

	TCHAR *cmd = new TCHAR[MAX_PATH*5];

	_stprintf_s(cmd,MAX_PATH*5,cmdMsiTempl, baseName, src);
	
	DWORD  exitCode = 0;
	Utils::executeCommandLine( cmd, exitCode );
	int res = 0;
	if( exitCode == 0){
		_stprintf_s(cmd ,MAX_PATH*5, dstUnpacked,src);
		res = Utils::CopyDirectory(cmd, dest, false);
	}
	deleteArray_catch(cmd);
	return res==0;
}


/*
BOOL Updater::updateClient(PUPDATEDATA psi)
{
	TCHAR serviceExeName[_MAX_PATH];
	GetModuleFileName(NULL,serviceExeName,_MAX_PATH);
	//TCHAR *baseName = psi->m_baseName;
	TCHAR *exeName = psi->m_exeName;
	TCHAR *src = psi->m_src;
	TCHAR *dest = psi->m_dest;
	TCHAR *baseName = psi->m_baseName;

	TCHAR templ[] =
		_T("chcp 65001\r\n")
		_T("(\r\n")
		_T(":Repeat\r\n")
		_T("del \"%s\"\r\n")
		_T("if exist \"%s\" goto Repeat\r\n")
		_T("xcopy /Y /S /E /I /C \"%s\\DeskAlerts\" \"%s\" > log.txt 2>&1\r\n")
		_T("del \"%%~dp0\\_copy.bat\"")
		_T(")\r\n");

	TCHAR cmdMsiTempl[] =
		_T("msiexec /a \"%sdeskalerts_setup.msi\" /quiet  /passive /qn TARGETDIR=\"%s\"");

	TCHAR *cmd = new TCHAR[MAX_PATH*5];
	TCHAR temppath[MAX_PATH];      // absolute path of temporary .bat file
	const TCHAR tempbatname[] = _T("da_copy_client.bat");

	_stprintf_s(cmd,MAX_PATH*5,cmdMsiTempl, baseName, src);
	
	DWORD  exitCode = 0;
	Utils::executeCommandLine( cmd, exitCode );
	
	if( exitCode != 0)return false;
	
	GetTempPath(MAX_PATH, temppath);
	_tcscat_s(temppath, sizeof(temppath)/sizeof(TCHAR), tempbatname);
	HANDLE hf ;
	hf = CreateFileW(temppath, GENERIC_WRITE, 0, NULL,
		CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL) ;
	if (hf != INVALID_HANDLE_VALUE)
	{
		DWORD len;
		TCHAR *bat = new TCHAR[MAX_PATH*5];
		_stprintf_s(bat,MAX_PATH*5,templ,exeName,exeName,src,dest);

		std::string str  = CT2A(bat, CP_UTF8);
		WriteFile(hf, str.c_str(), str.length(), &len, NULL);
		deleteArray_catch(bat);
		CloseHandle(hf) ;
		Utils::ShellExecuteAndWait(NULL, _T("open"), temppath, NULL, NULL, SW_HIDE);
	}
	return true;
}
*/


BOOL Updater::updateService(PUPDATEDATA psi)
{
	TCHAR serviceExeName[_MAX_PATH];
	CString srcServiceExeName;

	GetModuleFileName(NULL,serviceExeName,_MAX_PATH);

	//TCHAR *baseName = psi->m_baseName;
	//TCHAR *exeName = psi->m_exeName;
	TCHAR *src = psi->m_src;
	//TCHAR *dest = psi->m_dest;

	srcServiceExeName=CString(src)+CString("\\DeskAlerts\\DeskAlertsService.exe");

	TCHAR templ[] =
		_T("chcp 65001\r\n")
		//_T("(\r\n")
		//_T(":Repeat\r\n")
		//_T("del \"%s\"\r\n")
		//_T("if exist \"%s\" goto Repeat\r\n")
		_T("\"%s\" -ku\r\n")
		_T("copy \"%s\" \"%s\"\r\n")
		//_T("rmdir /S /Q \"%s\"\r\n")
		_T("\"%s\" -is\r\n")
		_T("del \"%%~dp0\\_copy.bat\"\r\n")
		_T(")\r\n");

	const TCHAR tempbatname[] = _T("da_update_service.bat");
	TCHAR temppath[MAX_PATH];      // absolute path of temporary .bat file
	GetTempPath(MAX_PATH, temppath);
	_tcscat_s(temppath, sizeof(temppath)/sizeof(TCHAR),tempbatname);


	//DWORD errNo;
	//if(::CopyFile(srcServiceExeName.GetString(), serviceExeName, False) == FALSE){
    //       errNo = ::GetLastError();
	//	   return false;
	//}
	HANDLE hf ;
	hf = CreateFileW(temppath, GENERIC_WRITE, 0, NULL,
		CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL) ;
	if (hf != INVALID_HANDLE_VALUE)
	{
		DWORD len;
		TCHAR *bat = new TCHAR[MAX_PATH*8] ;
		_stprintf_s(bat,MAX_PATH*8,templ, serviceExeName, srcServiceExeName.GetString(),/*baseName,*/serviceExeName,serviceExeName);
		std::string str  = CT2A(bat, CP_UTF8);
		WriteFile(hf, str.c_str(), str.length(), &len, NULL);
		deleteArray_catch(bat);
		CloseHandle(hf) ;
	     ShellExecute(NULL, _T("open"), temppath, NULL, NULL, SW_HIDE);

		//DWORD  exitCode = 0;
		//Utils::executeCommandLine( temppath, exitCode );

		//if( exitCode!=0 ) return false;
	}
	return true;
}

BOOL Updater::backupInstallDir( PUPDATEDATA psi )
{
	CString destFileName;
	/*destFileName.Format(_T("%sbackup.zip"),psi->m_baseName);
	Utils::ZipDirectory( psi->m_dest, CT2W(destFileName) );*/
	destFileName.Format(_T("%sbackup"),psi->m_baseName);
	Utils::CopyDirectory(psi->m_dest, destFileName, true);
	return true;
}

BOOL Updater::restoreInstallDir( PUPDATEDATA psi )
{
	CString zipFileName;
	/*zipFileName.Format(_T("%sbackup.zip"),psi->m_baseName);

	Utils::UnZipDirectory( CT2W(zipFileName), psi->m_dest );*/
	zipFileName.Format(_T("%sbackup"),psi->m_baseName);
	Utils::CopyDirectory(zipFileName, psi->m_dest, false);
	return true;
}


bool Updater::update(PUPDATEDATA psi, ClientManager* clientManager)
{
	std::vector<HANDLE> usersTokens=Utils::getUsersTokensWithOpenedClients(CString("DeskAlerts"));
	HANDLE senderToken;
	WTSQueryUserToken(psi->sid, &senderToken);

//#ifndef NDEBUG
//	TCHAR pTemp[256];
//	_stprintf_s(pTemp, sizeof(pTemp)/sizeof(TCHAR), _T("Update process beginned\n"), pName);
//	WriteLog(pLogFile, pTemp);
//#endif

	if (std::count( usersTokens.begin(), usersTokens.end(), senderToken) <= 0) usersTokens.push_back(senderToken );
	
	clientManager->closeAllOpenedClients();
	
	BOOL backup = backupInstallDir(psi);
	
	BOOL res = updateClient(psi);
	//BOOL res = false;
	if(  !res ){
		Utils::IncremetFailedAttemtCount(psi->m_version);
		if(backup ) restoreInstallDir(psi);
		return false;
	}

	std::vector<HANDLE>::iterator rit = usersTokens.begin();
	BOOL result=true;
	while( rit != usersTokens.end() )
	{
		if( !Utils::RunProcessAsUser( *rit, CString(psi->m_exeName),"/update") ){
			result=false;
			if( backup )restoreInstallDir(psi);
			break;
		}
#ifdef NDEBUG		
		if (result) CloseHandle(*rit);
#endif
		++rit;
	}
#ifdef NDEBUG		
	CloseHandle(senderToken);
#endif

	if( !result )
    {
		Utils::IncremetFailedAttemtCount(psi->m_version);
	}
    else 
    {
        Updater::updateService(psi);
    }

    return result ? true : false;
}


