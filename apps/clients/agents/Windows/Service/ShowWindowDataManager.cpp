#include "stdafx.h"
#include "ShowWindowDataManager.h"

ShowWindowDataManager::ShowWindowDataManager(void): m_windowData(NULL)
{
}

ShowWindowDataManager::~ShowWindowDataManager(void)
{
	//delete m_windowData;
}

void ShowWindowDataManager::setData(PWINDOWDATA windowData, long dataLen)
{
	m_windowData = windowData;
	m_dataLen = dataLen;
}

RequestDataType ShowWindowDataManager::getDataType()
{
	return REQUESTDATATYPE_WINDOW;
}

DWORD ShowWindowDataManager::serialize(IStream* pStream)
{
	pStream->Write(m_windowData, m_dataLen, NULL);
	return 0;
}