#include "StdAfx.h"
#include "Parcel.h"

Parcel::Parcel(void) : senderName("unknown"), m_data(nullptr)
{
}

Parcel::~Parcel(void)
{
	delete_catch(m_data);
}

void Parcel::setData(IParcelData* data)
{
	m_data=data;
}

IParcelData* Parcel::getData()
{
	return m_data;
}
