#pragma once
#include "stdafx.h"
#include "ParcelManager.h"

class RegistryParcelData : public IParcelData
{
public:
	void set(void* data);
	void* get();
    RegistryParcelData(): m_data(NULL){}
private:
	void *m_data;
};

struct RegistryStruct
{
	CString eventName;
	CString keyName;
	CString value;
};


class RegistryMonitor
{
public:
	RegistryMonitor(void);
	~RegistryMonitor(void);
	static DWORD WINAPI notifyThread(LPVOID pv);
	static void startRegMonitoring(ParcelManager* parcelManager);
private:
	static ParcelManager* m_parcelManager;
	static Parcel* createParcel(CString eventName,CString keyName="",CString value="");
};
