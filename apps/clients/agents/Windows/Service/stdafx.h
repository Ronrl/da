// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

// ATL secured functions.
#define _SECURE_ATL 1

// STL errors handling.
// _SECURE_SCL=1 for enabled state, 0 for disabled.
// _SECURE_SCL_THROWS=1 for throwing exception, 0 for abnormal program termination.
#ifdef _SECURE_SCL
#undef _SECURE_SCL
#endif
#ifdef _SECURE_SCL_THROWS
#undef _SECURE_SCL_THROWS
#endif
//#define _SECURE_SCL 1
//#define _SECURE_SCL_THROWS 1

#ifndef release_try
#ifdef DEBUG
#define release_try {
#define release_end_try }
#define release_catch_all } if(0) {
#define release_catch_end }
#define release_catch_tolog(...) }
#define delete_catch(x) delete x; //use debug and/or brain!
#define deleteArray_catch(x) delete [] x;
#else
#define release_try try {
#define release_end_try } catch(...) {}
#define release_catch_all } catch(...) {
#define release_catch_end }
#define release_catch_tolog(...) } catch(...) {/*no log*/}
#define delete_catch(x) try{ delete x; } catch(...) {} //ha-ha-ha! we need to avoid stupid crashes.
#define deleteArray_catch(x) try{ delete [] x; } catch(...) {}
#endif
#endif

#define _WTL_NO_CSTRING

#include <process.h>
#include <iostream>
#include <tchar.h>
#include <stdio.h>
#include <windows.h>
#include <winbase.h>
#include <winsvc.h>
#include <atlstr.h>

#include <atlbase.h>
#include <atlapp.h>

#include <atlhost.h>
#include <atlwin.h>
#include <atlctl.h>

#include <atlframe.h>
#include <atlctrls.h>
#include <atldlgs.h>


#include <atlutil.h>
#include <atlmisc.h>
#include <comdef.h>
#include <Wininet.h>
#include <Wincrypt.h>

#include "../deskalerts/Constants.h"

extern inline DWORD WaitForSingleObjectWithMsgLoop(__in HANDLE hHandle, __in DWORD dwMilliseconds);

