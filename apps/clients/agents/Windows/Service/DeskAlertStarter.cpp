#include "StdAfx.h"
#include "DeskAlertStarter.h"

//
// Àíäðåé Ìàêñèìîâ
// 20.07.2017
// Ïðîâåðÿåò, çàïóùåí ëè ïðîöåññ DeskAlerts.Exe 
//
BOOL DeskAlertStarter::IsDeskAlertRun(const WCHAR* processName)
{
   HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
   PROCESSENTRY32 pe;
   pe.dwSize = sizeof(PROCESSENTRY32);
   Process32First(hSnapshot, &pe);

   while(true) {
	   if(_tcsicmp(pe.szExeFile, processName) == 0) return true;
       if(!Process32Next(hSnapshot, &pe)) return false;
   }
}


//
// Àíäðåé Ìàêñèìîâ
// 20.07.2017
// Ïðîâåðÿåò, çàïóùåí ëè ïðîöåññ DeskAlert.Exe, è çàïóñêåò åãî, åñëè îí íå çàïóùåí
//
BOOL DeskAlertStarter::StartDeskAlert()
{
	WIN32_FIND_DATA findFileData;
	char* path32 = "C:\\Program Files (x86)\\DeskAlerts\\DeskAlerts.exe";
	char* path64 = "C:\\Program Files\\DeskAlerts\\DeskAlerts.exe";

	if (FindFirstFile(L"C:\\Program Files (x86)\\DeskAlerts\\DeskAlerts.exe", &findFileData) != INVALID_HANDLE_VALUE)
	{
		WinExec(path32, NULL);
		return TRUE;
	}
	else if (FindFirstFile(L"C:\\Program Files\\DeskAlerts\\DeskAlerts.exe", &findFileData) != INVALID_HANDLE_VALUE) 
	{
		WinExec(path64, NULL);
		return TRUE;
	};
	return FALSE;
}