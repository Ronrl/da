//
//  Utils.h
//  DeskAlerts Client
//  Application
//
//  Copyright Softomate 2006-2009. All rights reserved.
//
#pragma once
#include <vector>
#include <map>

#include "ZipUnzip/zip.h"
#include "ZipUnzip/unzip.h"


class Utils
{
public:
	Utils(void);
	~Utils(void);
	static HKEY GetAppRegistryKey();
	static HKEY GetCompanyRegistryKey();
	static DWORD loadProperty(void);
	static CString getValue(const CString& s,const CString& def);
	static CString* getValue(const CString& s);
	static void setValue(const CString& s,const CString& val);
	static CString getPropertyString(CString& propertyName,std::map<CString,CString> &properties, CString *def = NULL);
	static void setPropertyString(CString& promertyName,CString& promertyValue,BOOL replase = TRUE);
	static CString buildPropertyString(CString& param,std::map<CString,CString> &properties);
	static DWORD sendUninstallRequest();
	static void startInternetMonitoring();
	static void startUpdateMonitoring();
	static bool appRegistryKeyExists();
	static bool CreateSecurityAttributes(ACCESS_MASK amAccessRights,PSECURITY_ATTRIBUTES* ppSecurityAttributes);
	static void UninstallService();
	static void CloseService();
	static BOOL RunProcessAsUser(HANDLE primaryToken,CString processPath, CString arguments);
	static HANDLE GetCurrentUserToken();
	static std::vector<HANDLE> getUsersTokensWithOpenedClients(CString name);
	static HANDLE GetProcessOwnerToken(DWORD pid);
	
	static BOOL ShellExecuteAndWait(HWND hwnd, LPCTSTR lpOperation, LPCTSTR lpFile, LPCTSTR lpParameters, LPCTSTR lpDirectory, INT nShowCmd, BOOL wait=true);
	static BOOL executeCommandLine(CString cmdLine, DWORD & exitCode);
	
	static CString allUsersAppDataPath(BOOL env = FALSE);
	static CString allUsersAppDataProfilePath(BOOL env = FALSE);
	static std::vector<CString> getUsersDatabaseFiles();
	static CString getCommonDatabasePath();
	static CString GetRegistryKeyName();
	static CString GetProfileName();
	static DWORD sendRequest(CString szURL, bool skipHttpsCheck = true);
	static BOOL RecursiveDeleteDirectory(const TCHAR *path);
	static int CopyDirectory(const CString szDirectory, const CString szDestinationDirectory, BOOL destCreate);
	static bool DirExists( const CString fname );
    static void ZipDir(TCHAR *sPath,  HZIP hz);
	static void ZipDirectory(TCHAR *sPath, TCHAR *zipFileName);
	static void UnZipDirectory(TCHAR * zipFileName, TCHAR * destPath);
	static void IncremetFailedAttemtCount(CString version);
	static CString getDesktopName(HDESK hDesktop);
	static CString getProccessPath(HANDLE processHandle);
	static void GetOsVersion(OSVERSIONINFO *versionInformation);
	static void DisableW10Lockscreen();
    static BOOL GetLogonFromToken(HANDLE hToken, _bstr_t& strUser, _bstr_t& strdomain);
	static bool IsProcessExists(const TCHAR* szProcessName);

private:
	static HANDLE GetTokenOfLoggedOnUser();
	static DWORD GetPIDOfLoggedOnUser();
};
