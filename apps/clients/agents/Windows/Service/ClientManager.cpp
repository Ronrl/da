#include "StdAfx.h"
#include "ClientManager.h"
#include "Utils.h"
#include "GetProcessID.h"
#include <vector>
#include "DatabaseManager.h"
#include "../deskalerts/HTMLView.h"
#include "../deskalerts/ServiceDataManager.h"
#include "../deskalerts/RequestDataManager.h"
#include "../deskalerts/CloseWindowDataManager.h"
#include <Wtsapi32.h>
#include <Userenv.h>
#include "ShowWindowDataManager.h"
#include <tlhelp32.h>

DWORD recieveThreadId(0);
HANDLE ClientManager::m_readyEventHandle = NULL;
HANDLE ClientManager::m_mappingReadyEventHandle = NULL;
HANDLE ClientManager::m_mapping = NULL;
HANDLE ClientManager::m_eventHandle = NULL;

ClientManager::ClientManager(CString name): m_closeClientEvent(NULL)
{
	m_clientName = name;
	PSECURITY_ATTRIBUTES eventSa;
	PSECURITY_ATTRIBUTES mapSa;
	Utils::CreateSecurityAttributes(EVENT_ALL_ACCESS, &eventSa);
	Utils::CreateSecurityAttributes(FILE_MAP_ALL_ACCESS, &mapSa);
	m_readyEventHandle = CreateEvent(eventSa, FALSE, FALSE, INTERACTION_SERVICE_READY_EVENT_NAME);
    if (m_readyEventHandle)
    {
        SetEvent(m_readyEventHandle);
    }
	m_mappingReadyEventHandle = CreateEvent(eventSa, FALSE, FALSE, INTERACTION_SERVICE_MAP_READY_EVENT_NAME);
    if (m_mappingReadyEventHandle)
    {
        SetEvent(m_mappingReadyEventHandle);
    }

	m_mapping = CreateFileMapping(INVALID_HANDLE_VALUE, mapSa, PAGE_READWRITE, 0, sizeof(PACKET), INTERACTION_SERVICE_MAP_NAME);

	m_eventHandle = CreateEvent(eventSa, FALSE, FALSE, INTERACTION_SERVICE_EVENT_NAME);
}

ClientManager::~ClientManager(void)
{
	CloseHandle(m_mapping);
	CloseHandle(m_eventHandle);
}


DWORD WINAPI ClientManager::recieveThread(LPVOID pv)
{
	ParcelManager *parcelManager = (ParcelManager*)pv;

	if(m_eventHandle)
	{
		while(true)
		{
			bool reading=1;
			std::vector<PPACKET> packets;
			while (reading)
			{
				if(WaitForSingleObjectWithMsgLoop(m_eventHandle, INFINITE) == WAIT_OBJECT_0)
				{
					if(m_mapping)
					{
						void* buffer = (void *)MapViewOfFile(m_mapping, FILE_MAP_READ, 0, 0, 0);

						void * tmpBuffer = (void*)(new char[sizeof(PACKET)]);
						memcpy(tmpBuffer,buffer,sizeof(PACKET));
						PPACKET packet = (PPACKET)tmpBuffer;

						packets.push_back(packet);
						if (packet->header.isLast)
						{
							reading=0;
						}

						UnmapViewOfFile(buffer);
						SetEvent(m_mappingReadyEventHandle);
					}
				}
				else
				{
					break;
				}
			}

			IStream* pStream = NULL;
			CreateStreamOnHGlobal(NULL, TRUE, &pStream);

			std::vector<PPACKET>::iterator it=packets.begin();
			while(it!=packets.end())
			{
				pStream->Write((*it)->data,PACKET_LENGTH,NULL);
				it++;
			}

			HGLOBAL hGlobal = NULL;
			GetHGlobalFromStream(pStream, &hGlobal);

			LPVOID pData = GlobalLock(hGlobal);

			if (pData)
			{
				PREQUESTPACKET resultPacket = (PREQUESTPACKET)pData;
				unsigned int packetLength=sizeof(REQUESTPACKET)+resultPacket->header.length;

				void * tmpPacket= (void*)(new char[packetLength]);

				memcpy(tmpPacket,pData,packetLength);

				resultPacket = (PREQUESTPACKET)tmpPacket;
				resultPacket->data = (char*)tmpPacket + sizeof(REQUESTPACKET);

				if (RequestDataManager::checkStructHash(resultPacket->data,resultPacket->header.length,resultPacket->header.hash))
				{
					parcelManager->sendParcel(ClientManager::createParcel((void*)resultPacket));
				}
			}

			GlobalUnlock(hGlobal);
			pStream->Release();
			SetEvent(m_readyEventHandle);
		}
	}

	return 0;
}

void ClientManager::StartRecieveMessages(ParcelManager* parcelManager)
{
	m_parcelManager = parcelManager;
	/*const HANDLE hThread = */CreateThread(NULL, 0, ClientManager::recieveThread, (LPVOID)m_parcelManager, 0, &recieveThreadId);
}

BOOL ClientManager::closeAllOpenedClients()
{
	BOOL result = TRUE;
	std::vector<DWORD> SetOfPID;
	GetProcessID(m_clientName.GetBuffer(),SetOfPID);

	PSECURITY_ATTRIBUTES sa;
	Utils::CreateSecurityAttributes(EVENT_ALL_ACCESS, &sa);
	const HANDLE closeClientEvent = CreateEvent(sa, TRUE, TRUE, DA_CLIENT_CLOSE_EVENT);
	Sleep(2000); //wait client loop
	release_try
	{
		std::vector<DWORD>::iterator rit = SetOfPID.begin();
		while(rit != SetOfPID.end())
		{
			const HANDLE token = OpenProcess(PROCESS_ALL_ACCESS,FALSE,*rit);
			if(WaitForSingleObjectWithMsgLoop(token, 10000) == WAIT_TIMEOUT)
			{
				result = TerminateProcess(token,0);
				WaitForSingleObjectWithMsgLoop(token, INFINITE);
			}
			CloseHandle(token);
			++rit;
		}
	}
	release_catch_all
	{
		CloseHandle(closeClientEvent);
	}
	release_catch_end

	return result;
}

Parcel* ClientManager::createParcel(void* data)
{
	ClientManagerParcelData *parcelData= new ClientManagerParcelData();
	parcelData->set(data);
	Parcel* parcel=new Parcel();
	parcel->setData(parcelData);
	parcel->senderName="ClientManager";
	return parcel;
}

void ClientManager::mergeProperties(std::map<CString,CString> &firstProperties, std::map<CString,CString> &secondProperties)
{
	std::map<CString,CString>::iterator it = secondProperties.begin();
	while(it != secondProperties.end())
	{
		if (firstProperties.count(it->first)<=0)
		{
			firstProperties.insert(std::pair<CString,CString>(it->first,it->second));	
		}
		++it;
	}
}

std::map<CString,std::map<CString,CString>> ClientManager::getPropertiesForUsers()
{
	std::map<CString,std::map<CString,CString>> usersProperties;
	std::vector<CString> dbFiles= Utils::getUsersDatabaseFiles();

	std::map<CString,CString> commonProperties = getCommonProperties();

	std::vector<CString>::iterator rit = dbFiles.begin();
	while(rit != dbFiles.end())
	{
		std::map<CString,CString> curUserProperties;
		DatabaseManager *dbManager = new DatabaseManager(*rit);
		dbManager->loadProperty(curUserProperties);
		mergeProperties(curUserProperties,commonProperties);
		usersProperties.insert(std::pair<CString,std::map<CString,CString>>(*rit,curUserProperties));
		delete_catch(dbManager);
		++rit;
	}
	return usersProperties;
}

std::map<CString,CString> ClientManager::getCommonProperties()
{
	std::map<CString,CString> properties;
	CString dbFile= Utils::getCommonDatabasePath();
	DatabaseManager *dbManager = new DatabaseManager(dbFile);
	dbManager->loadProperty(properties);
	delete_catch(dbManager);
	return properties;
}

std::vector<ClientManager::secureDesktopClient>::iterator ClientManager::findDesktopClient(CString desktopName, DWORD sid)
{
	std::vector<secureDesktopClient>::iterator it = m_secureDesktopClients.begin();
	
	while (it!=m_secureDesktopClients.end())
	{
		if ((*it).desktopName == desktopName && (*it).sid == sid)
		{
			return it;
		}
		it++;
	}
	return it;
}

BOOL ClientManager::startClient(CString path, CString desktopName, DWORD sid, DWORD *processId)
{
	BOOL result = FALSE;
	PROCESS_INFORMATION pi;
	STARTUPINFO si;
	HANDLE hToken;
	HANDLE hDuplicateToken;

	memset(&si,0,sizeof(si));
	CString fullDesktopName = CString(_T("winsta0\\")) + desktopName;
	si.lpDesktop = fullDesktopName.GetBuffer();
	si.cb = sizeof(si);

	OpenProcessToken(GetCurrentProcess(), TOKEN_QUERY|TOKEN_DUPLICATE|TOKEN_ADJUST_SESSIONID, &hToken);


	HANDLE senderToken;
	WTSQueryUserToken(sid, &senderToken);

	void* lpEnvironment = NULL;

	// Get all necessary environment variables of logged in user
	// to pass them to the process
	/*const BOOL resultEnv = */CreateEnvironmentBlock(&lpEnvironment,
		senderToken, FALSE);
	CloseHandle(senderToken);


	DuplicateTokenEx(hToken, MAXIMUM_ALLOWED, 0, SecurityImpersonation, TokenPrimary, &hDuplicateToken);
	CloseHandle(hToken);

	BOOL tokenInformationIsSet = SetTokenInformation(hDuplicateToken, TokenSessionId, &sid, sizeof(sid));
    tokenInformationIsSet;

	CString command = _T("\"");
	command += path;
	command += _T("\"");
	command += _T(" ");
	command += _T("/secureDesktop");
    command += _T(" -child");

	if (CreateProcessAsUser(hDuplicateToken, NULL, command.GetBuffer(), 0, 0, False, CREATE_NEW_CONSOLE|CREATE_UNICODE_ENVIRONMENT, lpEnvironment, NULL, &si, &pi))
	{
		*processId = pi.dwProcessId;
		result = TRUE;
	}

	DestroyEnvironmentBlock(lpEnvironment);
	CloseHandle(hDuplicateToken);
	
	return result;
}

BOOL ClientManager::processStarted(CString processName, HANDLE hProcess)
{
	BOOL started = FALSE;
	DWORD exitCode;
	if(GetExitCodeProcess(hProcess,&exitCode))
	{
		if (exitCode == STILL_ACTIVE)
		{
			CString path= Utils::getProccessPath(hProcess);
			CString runningProcessName = PathFindFileName(path);
			if (runningProcessName.MakeLower() == processName.MakeLower())
			{
				started = TRUE;
			}
		}
	}
	return started;
}

BOOL ClientManager::sendShowWindowRequest(PWINDOWDATA windowData, long dataLen)
{
	BOOL res = FALSE;
	CString desktopName = CString(windowData->desktopName);
	DWORD sid;
    std::vector<DWORD> vProcessID;
	if (ProcessIdToSessionId(windowData->pid,&sid))
	{
		DWORD processId = 0;
		BOOL started = FALSE;
		CString processPath;
		HANDLE hProcess = OpenProcess(PROCESS_ALL_ACCESS,FALSE,windowData->pid);
		if (hProcess)
		{
			processPath = Utils::getProccessPath(hProcess);
            CloseHandle(hProcess);
		}
		
		if (desktopName.MakeLower() == _T("winlogon"))
		{
			std::vector<secureDesktopClient>::iterator it = findDesktopClient(desktopName, sid);
			if (it!=m_secureDesktopClients.end())
			{
				hProcess = OpenProcess(PROCESS_ALL_ACCESS,FALSE,(*it).pid);
				if (hProcess)
				{
					started = processStarted((*it).processName,hProcess);
				}
				CloseHandle(hProcess);

				if (!started)
				{
					m_secureDesktopClients.erase(it);
				}
				else
				{
					processId = (*it).pid;
                    vProcessID.push_back(processId);
				}
			}	

			if (!started)
			{
				if(startClient(processPath,desktopName,sid,&processId))
				{
                    vProcessID.push_back(processId);
					secureDesktopClient client;
					client.desktopName = desktopName;
					client.pid = processId;
					client.processName = PathFindFileName(processPath);
					ProcessIdToSessionId(processId,&(client.sid));
					m_secureDesktopClients.push_back(client);
					started = TRUE;
				}
			}
		}
		else
		{
			PROCESSENTRY32 entry;
			entry.dwSize = sizeof(PROCESSENTRY32);

			HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);

			if (Process32First(snapshot, &entry) != FALSE)
			{
				while (Process32Next(snapshot, &entry) != FALSE)
				{
					CString processName =  m_clientName + _T(".exe");
					if (_tcsicmp(entry.szExeFile, processName) == 0)
					{
						/*const HANDLE*/ hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, entry.th32ProcessID);
						if (hProcess)
						{
							DWORD clientSid;
							ProcessIdToSessionId(entry.th32ProcessID,&clientSid);
							if (clientSid == sid && entry.th32ProcessID != windowData->pid)
							{
								started = processStarted(processName,hProcess);
								processId = entry.th32ProcessID;
                                vProcessID.push_back(processId);
                                //break;
							}
							CloseHandle(hProcess);
						}
					}
				}
			}
			CloseHandle(snapshot);
		}

		if (started)
		{
			ShowWindowDataManager *dataManager=new ShowWindowDataManager();
			dataManager->setData(windowData,dataLen);
            vProcessID.push_back(processId);
            for each (DWORD procID in vProcessID)
            {
                res |= sendRequestToClient(dataManager, procID);
            }
			
			delete_catch(dataManager);
		}
		
	}
	return res;
}


BOOL ClientManager::sendCloseWindowRequest( PCLOSEWINDOWDATA closeWindowData )
{
	BOOL res = FALSE;
	DWORD sid;
    std::vector<DWORD> vProcessID;
	if (ProcessIdToSessionId(closeWindowData->pid,&sid))
	{
		DWORD processId = 0;
		BOOL started = FALSE;
		CString processPath;
		HANDLE hProcess = OpenProcess(PROCESS_ALL_ACCESS,FALSE,closeWindowData->pid);
		if (hProcess)
		{
			processPath = Utils::getProccessPath(hProcess);
            CloseHandle(hProcess);
		}


		PROCESSENTRY32 entry;
		entry.dwSize = sizeof(PROCESSENTRY32);

		HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);

		if (Process32First(snapshot, &entry) != FALSE)
		{
			while (Process32Next(snapshot, &entry) != FALSE)
			{
				CString processName =  m_clientName + _T(".exe");
				if (_tcsicmp(entry.szExeFile, processName) == 0)
				{
					/*const HANDLE*/ hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, entry.th32ProcessID);
					if (hProcess)
					{
						DWORD clientSid;
						ProcessIdToSessionId(entry.th32ProcessID,&clientSid);
						if (clientSid == sid && entry.th32ProcessID != closeWindowData->pid)
						{
							started = processStarted(processName,hProcess);
							processId = entry.th32ProcessID;
                            vProcessID.push_back(processId);
							break;
						}
						CloseHandle(hProcess);
					}
				}
			}
		}
		CloseHandle(snapshot);

		if (started)
		{
			CloseWindowDataManager *dataManager=new CloseWindowDataManager();
			dataManager->setData(closeWindowData->alertId);
            vProcessID.push_back(processId);
            for each (DWORD procID in vProcessID)
            {
                res |= sendRequestToClient(dataManager, procID);
            }
			delete_catch(dataManager);
		}
	}

	return res;
}


BOOL ClientManager::sendRequestToClient(ServiceDataManager* dataManager,DWORD pid)
{
	BOOL res = FALSE;
	CString pidStr;
	pidStr.Format(_T("%d"),pid);

	static HANDLE readyEventHandle;
	static HANDLE mappingReadyEventHandle;
	static HANDLE mapping;
	static HANDLE eventHandle;

	PSECURITY_ATTRIBUTES sa;
	Utils::CreateSecurityAttributes(EVENT_ALL_ACCESS, &sa);
	PSECURITY_ATTRIBUTES eventSa;
	PSECURITY_ATTRIBUTES mapSa;
	Utils::CreateSecurityAttributes(EVENT_ALL_ACCESS, &eventSa);
	Utils::CreateSecurityAttributes(FILE_MAP_ALL_ACCESS, &mapSa);
	readyEventHandle = CreateEvent(eventSa, FALSE, FALSE, INTERACTION_SERVICE_READY_EVENT_NAME + pidStr);
	
	mappingReadyEventHandle = CreateEvent(eventSa, FALSE, FALSE, INTERACTION_SERVICE_MAP_READY_EVENT_NAME + pidStr);

	mapping = CreateFileMapping(INVALID_HANDLE_VALUE, mapSa, PAGE_READWRITE, 0, sizeof(PACKET), INTERACTION_SERVICE_MAP_NAME + pidStr);
	eventHandle = CreateEvent(eventSa, FALSE, FALSE, INTERACTION_SERVICE_EVENT_NAME + pidStr);

	if(eventHandle)
	{
		if (readyEventHandle && mappingReadyEventHandle)
		{
			if(WaitForSingleObjectWithMsgLoop(readyEventHandle, 1000) == WAIT_OBJECT_0)
			{
				// Split into packets and send synchronously
				RequestDataManager requestDataManager(dataManager);
				IStream* pStream = NULL;
				CreateStreamOnHGlobal(NULL, TRUE, &pStream);
				requestDataManager.serialize(pStream);
				HGLOBAL hGlobal = NULL;
				GetHGlobalFromStream(pStream, &hGlobal);

				LPVOID pData = GlobalLock(hGlobal);

				for (unsigned int packetId = 0; packetId < requestDataManager.getPacketsCount(); packetId++)
				{
					if(WaitForSingleObjectWithMsgLoop(mappingReadyEventHandle, INFINITE) != WAIT_OBJECT_0)
					{
						break;
					}
					HANDLE mapping1 = OpenFileMapping(FILE_MAP_ALL_ACCESS, FALSE, INTERACTION_SERVICE_MAP_NAME + pidStr);
					if(mapping1)
					{
						void* buffer = MapViewOfFile(mapping1, FILE_MAP_WRITE, 0, 0, sizeof(PACKET));

						PPACKET packet = requestDataManager.getPacket(pData, packetId);
                        if (buffer != nullptr)
                        {
                            memcpy(buffer, (void*)packet, sizeof(PACKET));
                        }
						SetEvent(eventHandle);
						res = TRUE;
					}
				}
			}
		}
	}
    if (readyEventHandle) CloseHandle(readyEventHandle);
	if (mappingReadyEventHandle) CloseHandle(mappingReadyEventHandle);
	if (mapping) CloseHandle(mapping);
	if (eventHandle) CloseHandle(eventHandle);
	return res;
}

void ClientManager::updateSecureDesktopClientsInfo()
{
    m_secureDesktopClients.clear();

    PROCESSENTRY32 entry;
    entry.dwSize = sizeof(PROCESSENTRY32);
    HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);

    if (Process32First(snapshot, &entry) != FALSE)
    {
        while (Process32Next(snapshot, &entry) != FALSE)
        {
            CString processName = m_clientName + _T(".exe");
            if (_tcsicmp(entry.szExeFile, processName) == 0)
            {
                const HANDLE hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, entry.th32ProcessID);
                if (hProcess)
                {
                    DWORD clientSid;
                    ProcessIdToSessionId(entry.th32ProcessID, &clientSid);

                    HANDLE hFoundProcessToken;
                    HANDLE hCurrentProcessToken;
                    
                    OpenProcessToken(hProcess, MAXIMUM_ALLOWED, &hFoundProcessToken);
                    OpenProcessToken(GetCurrentProcess(), MAXIMUM_ALLOWED, &hCurrentProcessToken);

                    _bstr_t procStrUser="";
                    _bstr_t procStrdomain="";
                    _bstr_t serviceStrUser = "";
                    _bstr_t serviceStrdomain = "";

                    Utils::GetLogonFromToken(hFoundProcessToken, procStrUser, procStrdomain);
                    Utils::GetLogonFromToken(hCurrentProcessToken, serviceStrUser, serviceStrdomain);
                    
                    if (procStrUser == serviceStrUser && procStrdomain== serviceStrdomain)
                    {
                        secureDesktopClient client;
                        client.desktopName = _T("winlogon");
                        client.pid = entry.th32ProcessID;
                        client.sid = clientSid;
                        CString processPath = Utils::getProccessPath(hProcess);
                        client.processName = PathFindFileName(processPath);
                        m_secureDesktopClients.push_back(client);
                    }
                }
            }
        }
    }
}

void ClientManagerParcelData::set(void* data)
{
	m_data=data;
}

void* ClientManagerParcelData::get()
{
	return m_data;
}