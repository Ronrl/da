#pragma once
#include "stdafx.h"
#include "ParcelManager.h"
#include <vector>
#include <map>

class ClientManagerParcelData : public IParcelData
{
public:
    ClientManagerParcelData():m_data(nullptr){}
	void set(void* data);
	void* get();
private:
	void *m_data;
};

class ServiceDataManager;


class ClientManager: public CFrameWindowImpl<ClientManager>
{
public:
	ClientManager(CString name);
	~ClientManager(void);
	BOOL closeAllOpenedClients();
	void StartRecieveMessages(ParcelManager* parcelManager);
	std::map<CString,std::map<CString,CString>> getPropertiesForUsers();
	std::map<CString,CString> getCommonProperties();
	BOOL sendRequestToClient(ServiceDataManager* dataManager,DWORD pid);
	BOOL ClientManager::sendShowWindowRequest(PWINDOWDATA windowData, long dataLen);
	BOOL ClientManager::sendCloseWindowRequest(PCLOSEWINDOWDATA closeWindowData);
    static Parcel* createParcel(void* data);
    void updateSecureDesktopClientsInfo();
private:

	struct secureDesktopClient
	{
		DWORD sid;
		DWORD pid;
		CString desktopName;
		CString processName;
        secureDesktopClient():sid(NULL),pid(NULL) {}
	};

	std::vector<secureDesktopClient> m_secureDesktopClients;

	HANDLE m_closeClientEvent;
	static HANDLE m_readyEventHandle;
	static HANDLE m_mappingReadyEventHandle;
	static HANDLE m_mapping;
	static HANDLE m_eventHandle;
	CString m_clientName;
	ParcelManager* m_parcelManager;
	static DWORD WINAPI recieveThread(LPVOID pv);
	
	void mergeProperties(std::map<CString,CString> &firstProperties, std::map<CString,CString> &secondProperties);
	std::vector<secureDesktopClient>::iterator findDesktopClient(CString desktopName, DWORD sid);
	BOOL startClient(CString path, CString desktopName, DWORD sid, DWORD *processId);
	BOOL processStarted(CString processName, HANDLE hProcess);
};
