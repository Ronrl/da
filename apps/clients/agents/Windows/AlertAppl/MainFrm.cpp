//
//  MainFrm.cpp
//  DeskAlerts Client
//  Application
//
//  Copyright Softomate 2006-2009. All rights reserved.
//

#include "stdafx.h"
#include "resource.h"
#include <time.h>
#include "aboutdlg.h"
#include "AlertApplView.h"
#include "MainFrm.h"
#include "AlertUtils.h"
#include "../deskalerts/Constants.h"
#include "../deskalerts/IPCTraceMacros.h"

#include <stdio.h>

DWORD WINAPI ThreadProc(CONST LPVOID lpParam);

VOID CALLBACK RestartApplication(HWND /*wHandle*/, UINT /*param1*/, UINT_PTR /*param2*/, DWORD /*param3*/);

CMainFrame::CMainFrame(DWORD initFlags): m_hLib(NULL), m_initFlags(initFlags), m_reloadTimer(NULL), m_sysMenuHWND(NULL), m_statMutex(NULL), m_iNormalToStandby(NULL), m_iUpdateExpire(NULL), m_iMode(NULL)
{
	m_restartTimerIsActive = FALSE;
}

VOID CMainFrame::InitUpdater()
{
	BYTE waitCount = 0;
	const BYTE maxWait = 6;
	//run the updater (for autorun)
	ShellExecute(NULL, _T("open"), _T("deskalerts_updater.exe"), NULL, NULL, SW_HIDE);
	do
	{
		//waiting for the full launch and version change, if there was an update
		Sleep(50);
		m_statMutex = OpenMutex(MUTEX_ALL_ACCESS, FALSE, _T("DeskAlertAppStatus"));
		++waitCount;
	} while (m_statMutex == NULL && waitCount <= maxWait);/* max wait 300 ms*/
	
	if (m_statMutex != NULL)
		WaitForSingleObject(m_statMutex, 100);//lock mutex
}

CString CMainFrame::Invoke(CString sFunc, CString sParam)
{
	CString sResult;
	if(m_hLib)
	{
		release_try
		{
			CString (*pFunc)(CString, CString);
			(FARPROC &)pFunc = GetProcAddress(m_hLib, "Invoke");
			sResult = pFunc(sFunc, sParam);
		}
		release_end_try
	}
	return sResult;
}

LRESULT CMainFrame::OnCreate(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	m_iNormalToStandby = 0;
	m_iUpdateExpire	   = 0;
	m_iMode = _NORMAL_;

	m_hLib = LoadLibrary(_T("deskalerts.dll"));
	if(m_hLib)
	{
		BOOL canStart = FALSE;
		if (!(m_initFlags & ALLOW_MULTIPLE_INSTANCES))
		{
			CString sDeskAlertsName = Invoke("GetProperty", "deskalerts_name");
			if(sDeskAlertsName == "deskalerts_name") sDeskAlertsName = "DeskAlerts";

			CString mutexName = sDeskAlertsName + _T("_mutex");
			/*const HANDLE hMutex = */CreateMutex(NULL,TRUE,mutexName);
			if(GetLastError() == ERROR_ALREADY_EXISTS)
			{
				/*CString sInUseMsg = Invoke("GetProperty", "alreadyInUseMsg");
				if(sInUseMsg != L"alreadyInUseMsg")
					::MessageBox(0, sInUseMsg, sDeskAlertsName, MB_ICONWARNING);
				else
					::MessageBox(0, sDeskAlertsName + _T(" already in use"), sDeskAlertsName, MB_ICONWARNING);*/
				PostMessage(WM_CLOSE, 0, 0);
			}
			else
			{
				canStart = TRUE;
			}
		}
		else
		{
			canStart = TRUE;
		}
		
		if(canStart)
		{
			//run updater and wait is initillization
			//InitUpdater();

			HWND sysMenuHWND;
			void (*pInitializeAlerts)(HWND, int, HWND&);
			(FARPROC &)pInitializeAlerts = GetProcAddress(m_hLib, "InitializeAlerts");
			pInitializeAlerts(m_hWnd, m_initFlags, sysMenuHWND);
			m_sysMenuHWND = sysMenuHWND;

			CString sDeskAlertsLoaded = Invoke("GetProperty", "deskalerts_loaded");
			if(sDeskAlertsLoaded == "0")
			{
				CString sDeskAlertsName = Invoke("GetProperty", "deskalerts_name");
				if(sDeskAlertsName == "deskalerts_name") sDeskAlertsName = "DeskAlerts";

				CString msg = _T("Can't to load configuration file: conf.xml\nApplication will be closed.");

#ifndef TRIAL_BUILD
				msg += _T("\n\nPlease contact your system administrator.");
#else
				msg += _T("\n\nPlease contact support@deskalerts.com.");
#endif

				::MessageBox(0, msg, sDeskAlertsName, MB_ICONSTOP|MB_OK);

				PostMessage(WM_CLOSE, 0, 0);
			}
			else
			{
				CString sNormalToStandby = Invoke("GetProperty", "normaltostandbyexpire");
				if(sNormalToStandby != CString(_T("normaltostandbyexpire")))
					m_iNormalToStandby	= CAlertUtils::str2sec(sNormalToStandby, -1);

				CString sIsUpdate = Invoke("GetProperty", "update_automatically");
				SetTimer(_UPDATE_TIMER_, m_iUpdateExpire, NULL);
			}
		}
	}
	else
	{
		PostMessage(WM_CLOSE, 0, 0);
	}
	return 0;
}

LRESULT CMainFrame::OnDestroy(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled)
{
	// unregister message filtering and idle updates
	release_try
	{
		void (*pDestroyAlerts)();
		(FARPROC &)pDestroyAlerts = GetProcAddress(m_hLib, "DestroyAlerts");
		pDestroyAlerts();
		bHandled = TRUE;
	}
	release_end_try
	PostQuitMessage(0);
	return 0;
}

LRESULT CMainFrame::OnFileExit(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	PostMessage(WM_CLOSE);
	return 0;
}

LRESULT CMainFrame::OnFileNew(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	return 0;
}

LRESULT CMainFrame::OnAppAbout(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	CAboutDlg dlg;
	dlg.DoModal();
	return 0;
}

void CMainFrame::standBy(bool onOff, bool /*manual*/)
{
	if (onOff) //-- Turn on Stand By Mode
	{
		if(m_iMode != _STANDBY_)
		{
			Invoke("StandBy", "On");
			m_iMode = _STANDBY_;
		}
	}
	else	//-- Turn Off
	{
		if(m_iMode == _STANDBY_)
		{
			m_iMode = _NORMAL_;
			Invoke("StandBy", "Off");
		}
	}
}

LRESULT CMainFrame::OnTimer(UINT /*uMsg*/, WPARAM wParam, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	//stand-by timer
	switch (wParam)
	{
		case _STANDBY_TIMER_:
			{
				LASTINPUTINFO lii;
				lii.cbSize = sizeof(lii);
				if(GetLastInputInfo(&lii))
				{
					//Get tick count as difference
					const UINT64 nDifIdle = GetTickCount64() - lii.dwTime;
					if(nDifIdle < m_iNormalToStandby*1000)
					{
						standBy(false, false);
                        ULARGE_INTEGER uli;
                        uli.QuadPart = m_iNormalToStandby * 1000 - nDifIdle;
						SetTimer(_STANDBY_TIMER_, uli.LowPart, NULL);
					}
					else if(m_iMode != _STANDBY_)
					{
						standBy(true, false);
						SetTimer(_STANDBY_TIMER_, 1000, NULL);
					}
				}
			}
			break;
		case _UPDATE_TIMER_:
			{
				KillTimer(_UPDATE_TIMER_);
				Invoke("Update", "auto");
			}
			break;
		case _RELOAD_TIMER_:
			{
				::PostMessage(this->m_hWnd, UWM_NOTIFY_RESTART_APPLICATION, NULL, NULL);
			}
			break;
	}
	return S_OK;
}

LRESULT CMainFrame::OnClose(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled)
{
	CAlertUtils::purgeCache();
	DestroyWindow();
	bHandled = TRUE;
	//release control mutex
	if ( m_statMutex != NULL )
	{
		ReleaseMutex(m_statMutex);
		CloseHandle(m_statMutex);
	}
	return 0;
}

LRESULT CMainFrame::OnTaskBarCreated(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	Invoke("AddTaskbarIcon", "");
	return 0;
}

