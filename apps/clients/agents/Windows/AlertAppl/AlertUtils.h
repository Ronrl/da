//
//  AlertUtils.h
//  DeskAlerts Client
//  Application
//
//  Copyright Softomate 2006-2009. All rights reserved.
//

#pragma once

extern void appendToErrorFile(const TCHAR *szFormat, ...);

class CAlertUtils
{
public:

	CAlertUtils(void);
	~CAlertUtils(void);

//	static HRESULT URLDownloadToFile(LPCTSTR szURL,LPCTSTR szFileName);

	static int str2sec(CString& str, int def = 0);
	static int str2int(CString& str, int def = 0);
	static void purgeCache(void);
	static void DisableW10Lockscreen();
	static void GetOsVersion(OSVERSIONINFO *versionInformation);
	static CString GetCitrixIp();
//	static HKEY GetCompanyRegistryKey();
//	static HKEY GetAppRegistryKey();
//	static CString getValue(const CString& s,const CString& def);
//	static CString* getValue(const CString& s);
//	static void setValue(const CString& s,const CString& val);
//	static void GetImageFromList(WTL::CImageList *lstImages,int nImage, WTL::CBitmap* destBitmap,HWND hwnd);
    static CString crashLogFolderPath(BOOL common = false);
    static bool startChildProcessAndWait(LPTSTR lpstrCmdLine);
    static void setExitOnParentExit();
};
