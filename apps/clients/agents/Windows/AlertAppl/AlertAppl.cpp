//
//  AlertAppl.cpp
//  DeskAlerts Client
//  Application
//
//  Copyright Softomate 2006-2009. All rights reserved.
//

#include "stdafx.h"

#include "resource.h"

#include "AlertApplView.h" 
#include "aboutdlg.h"
#include "MainFrm.h"
#include "AlertUtils.h"
#include "GlobalSolutionVariables.h"
#include "../deskalerts/Constants.h"
#include "../deskalerts/DeskAlerts.h"
//#include "../CrashHandler/exception_handler.h"
#include "../CrashHandler/GoogleCrashHandler.h"

#include <stdio.h>
BOOL updateService(CString startupPath);

typedef HRESULT (STDAPICALLTYPE* DLLREG)();

CAppModule _Module;
DWORD Global_MainThreadId;

#define DeskAlertsAssert(expr)  if (!(expr)){ int *qwe = 0; *qwe = 123; }else{}
#define DeskAlertsUnexpected() DeskAlertsAssert(0)

int Run(LPTSTR lpstrCmdLine = NULL, int /*nCmdShow*/ = SW_SHOWDEFAULT)
{
	DWORD initFlags = 0L;
	BOOL newid = FALSE;
#ifdef DEBUG
	//Sleep(30000);
#endif	

	if (lpstrCmdLine && _tcscmp(lpstrCmdLine, _T("")) != 0)
	{
		int nArgs;
		LPWSTR *szArglist = CommandLineToArgvW(lpstrCmdLine, &nArgs);
		if (szArglist)
		{
			for(int i=0; i<nArgs; i++)
			{
				if (_tcsicmp(szArglist[i], _T("/newid")) == 0)
				{
					newid = TRUE;
				}
				else if (_tcsicmp(szArglist[i], _T("/secureDesktop")) == 0)
				{
					initFlags = NO_MENU|NO_ALERTLOADER|NO_UPDATE|NO_ALERTS_RESTORE|DISABLE_EXTERNAL_LINKS|ALLOW_MULTIPLE_INSTANCES|NO_DATABASE|NO_FIRST_START|NO_SCREENSAVER|NO_REGISTRATION;
				}else if (_tcsicmp(szArglist[i], _T("/update")) == 0)
				{
					//initFlags = 
					//updateService("");
					//if( g_bDebugMode )						Sleep(30000);
					newid=false;
				}
				CString cmdArgString(szArglist[i]);
				if (cmdArgString.Find(L"-MainThread") != -1)
				{
					int pos = cmdArgString.Find(L"=");
					CString threadStr = cmdArgString.Mid(pos+1);
					DWORD dwNO = _ttoi((LPCTSTR)threadStr);
					//int a = std::stoi(threadStr.GetString());
					//DWORD ab = (DWORD)a;
					//ab;
					//a;
					//Global_MainThreadId
					Global_MainThreadId = dwNO;
				}
			}
			LocalFree(szArglist);
		}
		
	}

	CMessageLoop theLoop;
	_Module.AddMessageLoop(&theLoop);

	CMainFrame wndMain(initFlags);

	if(wndMain.CreateEx() == NULL)
	{
		ATLTRACE(_T("Main window creation failed!\n"));
		return 0;
	}

	wndMain.ShowWindow(SW_HIDE);

	if(newid)
		wndMain.Invoke(_T("usersession_id")); 

	int nRet = theLoop.Run();
	_Module.RemoveMessageLoop();
	return nRet;
}

bool minidump_callback(const wchar_t* dump_path, const wchar_t* minidump_id, void* context, EXCEPTION_POINTERS* exinfo, MDRawAssertionInfo* assertion, bool succeeded)

{
    //Sleep(10000);
    //std::wstring filename = dump_path;//L"_dump_path_";
    //filename += L"\\";
    //filename += minidump_id;
    //filename += L".dmp";
    dump_path;
    minidump_id;
    context;
    exinfo;
    assertion;
    succeeded;
    //
    //HANDLE fH;
    //FILETIME creationTime;
    //SYSTEMTIME sysTime;
    //fH = CreateFile(filename.c_str(), GENERIC_READ, 0, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
    //if (fH != INVALID_HANDLE_VALUE)
    //{
    //    GetFileTime(fH, &creationTime, 0, 0);
    //    FileTimeToSystemTime(&creationTime, &sysTime);
    //    //std::cout << sysTime.wDay << "." << sysTime.wMonth << "." << sysTime.wYear <<
    //    //    " " << sysTime.wHour << ":" << sysTime.wHour << std::endl;
    //    CloseHandle(fH);
    //
    //
    //    std::wstring newname = dump_path;
    //    newname += L"\\";
    //    newname += std::to_wstring(sysTime.wYear); newname += L"-";
    //    newname += std::to_wstring(sysTime.wMonth); newname += L"-";
    //    newname += std::to_wstring(sysTime.wDay); newname += L"_";
    //    newname += std::to_wstring(sysTime.wHour); newname += L"-";
    //    newname += std::to_wstring(sysTime.wMinute); newname += L"_";
    //    std::wstring zipname = newname;
    //    zipname += L"daclientCrashLog.zip";
    //    newname += minidump_id;
    //    
    //    newname += L".dmp";
    //    
    //    int result;
    //    result = _wrename(filename.c_str(), newname.c_str());
    //
    //    std::wstring commandToZip = L"";
    //    commandToZip += L"powershell Compress -Archive -Path ";
    //    commandToZip += newname;
    //    commandToZip += L" -Update -DestinationPath ";
    //    commandToZip += zipname;
    //    commandToZip.size();
    //
    //    system(commandToZip);
    //    
    //}
    //
    //
    //
    ////std::map<std::wstring, std::wstring> files;
    ////files.insert(std::make_pair(filename, filename));
    ////
    ////// At this point you may include custom data to be part of the crash report.
    ////std::map<std::wstring, std::wstring> userCustomData;
    ////userCustomData.insert(std::make_pair(L"desc", L"Hello World"));
    ////
    ////sender.SendCrashReport(L"https://api.raygun.com/entries/breakpad?apikey=paste_your_api_key_here", userCustomData, files, 0);
    //
    return true;
}


int WINAPI _tWinMain(HINSTANCE hInstance, HINSTANCE /*hPrevInstance*/, LPTSTR lpstrCmdLine, int nCmdShow)
{
    ////////////////////////////////////////
    // GoogleCrashHandler constructor build 
    // google_breakpad::ExceptionHandler and start crashLogging
    CString crashLogPath = CAlertUtils::crashLogFolderPath();
    GoogleCrashHandler* p = new GoogleCrashHandler(crashLogPath.GetString(), minidump_callback); p;

    ///////////////////////////////////////////////////////////
    // main process watch child process only, restart is neseccery
    // child process is work as WinClient DeskAlerts only
    bool isChildProcess = CAlertUtils::startChildProcessAndWait(lpstrCmdLine);
    if (isChildProcess)
    {
        ///////////////////////
        // is need - set addiction to life parent proc
        // CAlertUtils::setExitOnParentExit();
    }
    else
    {
        return 0;
    }
    
	CAlertUtils::DisableW10Lockscreen();

	int nRet = 0;
	HRESULT hRes = ::CoInitialize(NULL);
    ///////////////////////////////////////
    // If you are running on NT 4.0 or higher you can use the following call instead to
    // make the EXE free threaded. This means that calls come in on a random RPC thread.
    //	HRESULT hRes = ::CoInitializeEx(NULL, COINIT_MULTITHREADED);

	ATLASSERT(SUCCEEDED(hRes));
    //////////////////////////////////////
	// this resolves ATL window thunking problem when Microsoft Layer for Unicode (MSLU) is used
	::DefWindowProc(NULL, 0, 0, 0L);

	AtlInitCommonControls(ICC_BAR_CLASSES);	// add flags to support other controls

	hRes = _Module.Init(NULL, hInstance);
	ATLASSERT(SUCCEEDED(hRes));

	AtlAxWinInit();

	nRet = Run(lpstrCmdLine, nCmdShow);

	_Module.Term();
	::CoUninitialize();

	return nRet;
}

/*
BOOL updateService(CString startupPath)
{
	TCHAR serviceExeName[_MAX_PATH];
	TCHAR *src;
	CString srcServiceExeName;
	//CString serviceExeName( "DeskAlertsService.exe");

	GetModuleFileName(NULL,serviceExeName,_MAX_PATH);

	srcServiceExeName=startupPath+"\\"+serviceExeName;

	TCHAR templ[] =
		_T("chcp 65001\r\n")
		_T("(\r\n")
		_T(":Repeat\r\n")
		_T("del \"%s\"\r\n")
		_T("if exist \"%s\" goto Repeat\r\n")
		_T("copy \"%s\" \"%s\"\r\n")
		_T("rmdir /S /Q \"%s\"\r\n")
		_T("\"%s\" -i\r\n")
		_T("\"%s\" -s\r\n")
		_T("del \"%%~dp0\\_copy.bat\"\r\n")
		_T(")\r\n");

	const TCHAR tempbatname[] = _T("da_update_service.bat");
	TCHAR temppath[MAX_PATH];      // absolute path of temporary .bat file
	GetTempPath(MAX_PATH, temppath);
	_tcscat_s(temppath, sizeof(temppath)/sizeof(TCHAR),tempbatname);
	HANDLE hf ;
	hf = CreateFileW(temppath, GENERIC_WRITE, 0, NULL,
		CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL) ;
	if (hf != INVALID_HANDLE_VALUE)
	{
		DWORD len;
		TCHAR *bat = new TCHAR[MAX_PATH*8] ;
		_stprintf_s(bat,MAX_PATH*8, templ, serviceExeName, serviceExeName, srcServiceExeName.GetString(), serviceExeName, baseName, serviceExeName, serviceExeName);
		std::string str  = CT2A(bat, CP_UTF8);
		WriteFile(hf, str.c_str(), str.length(), &len, NULL);
		delete_catch(bat);
		CloseHandle(hf) ;
		ShellExecute(NULL, _T("open"), temppath, NULL, NULL, SW_HIDE);
	}
	return true;
}

void stopUpdateService()
{
	SC_HANDLE serviceDbHandle = OpenSCManager(NULL,NULL,SC_MANAGER_ALL_ACCESS);
	SC_HANDLE serviceHandle = OpenService(serviceDbHandle, "DeskAlertService", SC_MANAGER_ALL_ACCESS);

	SERVICE_STATUS_PROCESS status;
	DWORD bytesNeeded;
	QueryServiceStatusEx(serviceHandle, SC_STATUS_PROCESS_INFO,(LPBYTE) &status,sizeof(SERVICE_STATUS_PROCESS), &bytesNeeded);

	if (status.dwCurrentState == SERVICE_RUNNING)
	{// Stop it
		BOOL b = ControlService(serviceHandle, SERVICE_CONTROL_STOP, (LPSERVICE_STATUS) &status);
		//if (b)
		//{
		//	std::cout << "Service stopped." << std::endl; } else { std::cout << "Service failed to stop." << std::endl; }
		//}
	}
}*/