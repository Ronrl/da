//
//  AlertUtils.cpp
//  DeskAlerts Client
//  Application
//
//  Copyright Softomate 2006-2009. All rights reserved.
//

#include "StdAfx.h"
#include ".\alertutils.h"

#include <wininet.h>
#pragma comment(lib,"wininet")
#include <windows.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <crtdefs.h>
#include <time.h>
#include <ctime>
#define UWM_LIFE_SIGNAL WM_APP + 322

// The following methods are used to log some critcal events
void appendToErrorFile(const TCHAR *szFormat, ...)
{
	release_try
	{
		if(1)//g_bDebugMode)
		{
			struct tm when;
			__time64_t now;
			_time64(&now);
			_localtime64_s(&when, &now);

			TCHAR time_buff[26];
			_tasctime_s(time_buff, sizeof(time_buff)/sizeof(TCHAR), &when);
			const size_t time_len = _tcslen(time_buff);
			if(time_buff[time_len-1] == _T('\n'))
				time_buff[time_len-1] = _T('\0');

			TCHAR buf[4096];
			va_list va;
			va_start(va, szFormat);
			_vstprintf_s(buf, sizeof(buf)/sizeof(TCHAR), szFormat, va);
			AtlTrace(_T("TRACE: %s %s\n"), time_buff, buf);
			va_end(va);

			using namespace std;
			basic_ofstream<TCHAR> logFile;

			CString rootPath;
			rootPath.GetEnvironmentVariable(_T("APPDATA"));
			rootPath += _T("\\deskalerts.log");

			logFile.open(rootPath, ios::out|ios::app);

			logFile<<time_buff;
			logFile<<_T("\t");
			logFile<<buf;
			logFile<<endl;

			logFile.close();
		}
#ifdef DEBUG
		else
		{
			TCHAR buf[1000];
			va_list va;
			va_start(va, szFormat);
			_vstprintf_s(buf, sizeof(buf)/sizeof(TCHAR), szFormat, va);
			AtlTrace(_T("TRACE: %s\n"), buf);
			va_end(va);
		}
#endif
	}
	release_end_try
}

CAlertUtils::CAlertUtils(void)
{
}

CAlertUtils::~CAlertUtils(void)
{
}

int CAlertUtils::str2sec(CString& str, int def)
{
	int iRes = def;
	if (!str.IsEmpty())
	{
		TCHAR chr = str[str.GetLength()-1];
		if(chr >= _T('0') && chr <= _T('9'))
		{
			iRes = str2int(str, def)*60;
		}
		else
		{
			int val = str2int(str.Left(str.GetLength()-1), def);
			if(chr == _T('s') || chr == _T('S'))
			{
				iRes = val;
			}
			else if(chr == _T('m') || chr == _T('M'))
			{
				iRes = val*60;
			}
			else if(chr == _T('h') || chr == _T('H'))
			{
				iRes = val*60*60;
			}
			else if(chr == _T('d') || chr == _T('D'))
			{
				iRes = val*60*60*24;
			}
			else if(chr == _T('w') || chr == _T('W'))
			{
				iRes = val*60*60*24*7;
			}
		}
	}
	else
	{
		iRes=0;
	}
	return iRes;
}

int CAlertUtils::str2int(CString& str, int def)
{
	int iRes = def;
	if(!str.IsEmpty())
	{
		release_try
		{
			iRes = _ttoi(str);
		}
		release_catch_all
		{
			iRes = def;
		}
		release_catch_end
	}
	return iRes;
}

/*
*
* this code deletes all files in temporary folder,
* don't using shell-windows
*
* START
*
*/
void CAlertUtils::purgeCache(void)
{
	CString tmp;
	tmp.GetEnvironmentVariable(_T("TEMP"));
	CString fullDir;
	fullDir.Format(_T("%s\\Alerts\\"), static_cast<LPCWSTR>(tmp));
	CString fullMask;
	fullMask.Format(_T("%s\\Alerts\\*.*"), static_cast<LPCWSTR>(tmp));

	HANDLE hFind;
	WIN32_FIND_DATA find_data;
    
	hFind=FindFirstFile(fullMask, &find_data);
	if (hFind != INVALID_HANDLE_VALUE)
	{
		do
		{
			CString fileName = find_data.cFileName;
			if(!(find_data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) 
				&& fileName != "."
				&& fileName != "..")
			{
				DeleteFile(fullDir + fileName);
			}
		} while(FindNextFile(hFind, &find_data));
	}
}
/*
* FINISH
*/

// Gets OS version info from registry
void CAlertUtils::GetOsVersion(OSVERSIONINFO *versionInformation)
{
	HKEY hKey;
	DWORD dwType;
	if (RegOpenKeyEx(HKEY_LOCAL_MACHINE, L"SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion", NULL, KEY_READ, &hKey) == ERROR_SUCCESS)
	{
		TCHAR lpData[1024]={0};
		DWORD bufSize = sizeof(lpData);

		if (RegQueryValueEx(hKey, L"CurrentVersion", NULL, &dwType, (LPBYTE)lpData, &bufSize) == ERROR_SUCCESS)
		{
			CString strVersionNo = CString(lpData);
			int sepPos = strVersionNo.Find('.');
			CString strMajorVer = strVersionNo.Left(sepPos);
			CString strMinorVer = strVersionNo.Right(strVersionNo.GetLength() - sepPos - 1);

			versionInformation->dwMajorVersion = atoi((char*)strMajorVer.GetBuffer());
			versionInformation->dwMinorVersion = atoi((char*)strMinorVer.GetBuffer());
		}
		RegCloseKey(hKey);
	}
}

//
// Disable Windows 10 lockscreen via registry
//
void CAlertUtils::DisableW10Lockscreen()
{
	OSVERSIONINFO versionInfo;
	GetOsVersion(&versionInfo);
	if (versionInfo.dwMajorVersion == 6 && versionInfo.dwMinorVersion == 3) // Windows 10 detected
	{
		HKEY hKey;
		DWORD dwKeyValue = 1;
		if (RegOpenKeyEx(HKEY_LOCAL_MACHINE, L"SOFTWARE\\Policies\\Microsoft\\Windows\\Personalization", NULL, KEY_ALL_ACCESS, &hKey) == ERROR_SUCCESS)
		{
			RegSetValueEx(hKey, L"NoLockScreen", NULL, REG_DWORD, (BYTE*)&dwKeyValue, sizeof(dwKeyValue));
		}
	}
}
//
// get writing .dmp files folder
// using exported static method from deskalerts.dll!"CAlertUtils::crashLogFolderPath"
//
CString CAlertUtils::crashLogFolderPath(BOOL common)
{
    HINSTANCE h;
    CString(*DllcrashPath) (BOOL);
    CString crashLogPath = _T("");

    h = LoadLibrary(L"deskalerts.dll");
    if (h)
    {
        DllcrashPath = (CString(*) (BOOL))
            GetProcAddress(h, "crashLogFolderPath");
        if (DllcrashPath)
        {
            crashLogPath = DllcrashPath(common);
        }
        FreeLibrary(h);
    }
    return crashLogPath;
}
///////////////////////////////
// start "-child" process, if current process is not "-child"
void dropinfo(STARTUPINFO &si, PROCESS_INFORMATION &pi) {
    OutputDebugString(L"call dropinfo");
    if (si.hStdInput) CloseHandle(si.hStdInput);
    if (si.hStdOutput) CloseHandle(si.hStdOutput);
    if (si.hStdError) CloseHandle(si.hStdError);
    if (pi.hProcess) CloseHandle(pi.hProcess);
    if (pi.hThread) CloseHandle(pi.hThread);
    ZeroMemory(&si, sizeof(si));
    si.cb = sizeof(si);
    ZeroMemory(&pi, sizeof(pi));
};
bool CAlertUtils::startChildProcessAndWait(LPTSTR lpstrCmdLine)
{
    ////////////////////////////
    // get cmd Args
    CString cmdLine(lpstrCmdLine);
    LPWSTR *szArglist;
    int nArgs;
    szArglist = CommandLineToArgvW(GetCommandLineW(), &nArgs);
	DWORD missHeartBeatExitCode = 2;

    if (cmdLine.Find(L"-child") == -1)
    {
        ////////////////////////////
        // build new cmd Args
        CString childCmdLine;
        for (int i = 0; i < nArgs; i++)
        {
            if (i == 0)
            {
                childCmdLine += "\"" + CString(szArglist[0]) + "\"";
                continue;
            }
            childCmdLine += " ";
            childCmdLine += szArglist[i];
        }
        childCmdLine += " ";
        childCmdLine += "-child";
		childCmdLine += " ";
		DWORD curThreadID = GetCurrentThreadId();
		curThreadID;
		CString tmpString;
		tmpString.Format(_T("%u"), curThreadID);
		childCmdLine += "-MainThread="+ tmpString;

        DWORD err;
        DWORD exitCode = 0;
        DWORD MAXSLEEPTIME = 32'000;
        DWORD MINSLEEPTIME = 1'000;
        DWORD restartTime = MINSLEEPTIME;
        STARTUPINFO si;
        PROCESS_INFORMATION pi;
        ZeroMemory(&si, sizeof(si));
        si.cb = sizeof(si);
        ZeroMemory(&pi, sizeof(pi));
        SetLastError(0);
		time_t timeAfterLifeSignal = 0;
		time_t TimeStartLoop = 0;
		time_t TimeEndLoop = 0;
		time_t MaxValueHeartBeat = 300;
		time_t MinValueHeartBeat = 35;
		time_t TimeReload = MaxValueHeartBeat;
		
        ////////////////////////////
        // start child proc, get info
        BOOL isCreated = CreateProcess(NULL,   // No module name (use command line)
            childCmdLine.GetBuffer(),        // Command line
            NULL,           // Process handle not inheritable
            NULL,           // Thread handle not inheritable
            FALSE,          // Set handle inheritance to FALSE
            0,              // No creation flags
            NULL,           // Use parent's environment blocks
            NULL,           // Use parent's starting directory 
            &si,            // Pointer to STARTUPINFO structure
            &pi);           // Pointer to PROCESS_INFORMATION structure

        while (1)
        {
			TimeStartLoop = std::time(nullptr);

            /////////////////////////////////////
            /// check the child process is ended
            DWORD res = WaitForSingleObject(pi.hProcess, 1000);

            if (!isCreated)
            {
                err = GetLastError();
                ExitProcess(err);
                //todo: обработать err, при необходимости перезапустить процесс либо завершить
                break;
            }

            if (GetExitCodeProcess(pi.hProcess, &exitCode) == FALSE)
            {
                switch (res)
                {
                case WAIT_OBJECT_0: break;
                case WAIT_TIMEOUT: exitCode = STILL_ACTIVE; break;
                default:
                    //todo: logto err = GetLastError();
                    exitCode = 2;//some custom exitCode, that we cant get exit code child process
                }
            }

            switch (exitCode)
            {
            //is working:
            case STILL_ACTIVE: break;
            //standart exit:
            case 0: ExitProcess(exitCode); break;
            //resstart:
            default:
                if (restartTime <= MAXSLEEPTIME)
                {
                    Sleep(restartTime);
					if (exitCode != missHeartBeatExitCode)
					{
						restartTime *= 2;
					}
                    ///////////////////////////////////////
                    //// perhaps, when we not get ExitCodeProcess, but process is started should to
                    //TerminateProcess(pi.hProcess, 0);
                    ///////////////////////////////////////
                    //// todo:
                    //// Handles in STARTUPINFO or STARTUPINFOEX must be closed with CloseHandle when they are no longer needed. (see msdn, "CreateProcess")
                    //dropinfo(si, pi);
                    isCreated = CreateProcess(NULL,   // No module name (use command line)
                        childCmdLine.GetBuffer(),        // Command line
                        NULL,           // Process handle not inheritable
                        NULL,           // Thread handle not inheritable
                        FALSE,          // Set handle inheritance to FALSE
                        0,              // No creation flags
                        NULL,           // Use parent's environment blocks
                        NULL,           // Use parent's starting directory 
                        &si,            // Pointer to STARTUPINFO structure
                        &pi);           // Pointer to PROCESS_INFORMATION structure
					TimeReload = MaxValueHeartBeat;
					timeAfterLifeSignal = 0;
					break;
                }
                else
                {
                    int msgboxID = MessageBox(
                        NULL,
                        (LPCWSTR)L"Your DeskAlerts client application has failed to run after the crash.\n\
All the details of this crash were sent to the server. Please contact your system administrator for the details.\n\
Click 'Try again' to attempt to restart the application.",
                        (LPCWSTR)L"DeskAlerts has crashed",
                        MB_ICONWARNING | MB_RETRYCANCEL | MB_DEFBUTTON2
                        );

                    switch (msgboxID)
                    {
                    case IDCANCEL:
                        ExitProcess(exitCode);
                        break;
                    case IDRETRY:
                        restartTime = MINSLEEPTIME;
                        //// Handles in STARTUPINFO or STARTUPINFOEX must be closed with CloseHandle when they are no longer needed. (see msdn, "CreateProcess")
                        //TerminateProcess(pi.hProcess, 0);
                        //dropinfo(si, pi);
                        isCreated = CreateProcess(NULL,   // No module name (use command line)
                            childCmdLine.GetBuffer(),        // Command line
                            NULL,           // Process handle not inheritable
                            NULL,           // Thread handle not inheritable
                            FALSE,          // Set handle inheritance to FALSE
                            0,              // No creation flags
                            NULL,           // Use parent's environment blocks
                            NULL,           // Use parent's starting directory 
                            &si,            // Pointer to STARTUPINFO structure
                            &pi);           // Pointer to PROCESS_INFORMATION structure
						TimeReload = MaxValueHeartBeat;
						timeAfterLifeSignal = 0;
                        break;
                    }
                }
                break;
            }

            /////////////////////////////////////
            /// check life signal
			MSG msg;
			// Main message loop:
			if (::PeekMessage(&msg, NULL, NULL, NULL, PM_NOREMOVE))
			{
				if (GetMessage(&msg, NULL, 0, 0) > 0) //important to check > 0, see MSDN
				{
					release_try
						if (msg.message == UWM_LIFE_SIGNAL)
						{
							timeAfterLifeSignal = 0;
							TimeReload = 2 * msg.lParam;
							if (TimeReload > MaxValueHeartBeat)
								TimeReload = MaxValueHeartBeat;
							else if (TimeReload < MinValueHeartBeat)
								TimeReload = MinValueHeartBeat;
							continue;
						}
						else
						{
							TranslateMessage(&msg);
							DispatchMessage(&msg);
						}
					release_end_try
				}
			}

			TimeEndLoop = std::time(nullptr);
			timeAfterLifeSignal += TimeEndLoop - TimeStartLoop;
			/////////////////////////////////////
			/// restart afer life signal timeout
			if (timeAfterLifeSignal > TimeReload )
			{
				//TerminateProcess(pi.hProcess, missHeartBeatExitCode);
				timeAfterLifeSignal = 0;
			}
        }
    }
	else
	{
		return true;
	}
}

DWORD WINAPI waitExitProcess(HANDLE h)
{
    h;
    //while (WaitForSingleObject(h, 1000) != WAIT_TIMEOUT)
    //{
    //    
    //}
    //ExitProcess(0);
    ////WaitForSingleObject(h, INFINITE);
    ////ExitProcess(0);
    return 0;
}

void CAlertUtils::setExitOnParentExit() 
{
    //HANDLE hPseudoProc = GetCurrentProcess();
    //
    ////Реальные дескрипторы
    //HANDLE hProc;
    //
    //DuplicateHandle(hPseudoProc, hPseudoProc, hPseudoProc, &hProc, 0, FALSE, DUPLICATE_SAME_ACCESS);
    //CreateThread(NULL, 0, &waitExitProcess, hProc, 0, NULL);
}
//