//
//  MainFrm.h
//  DeskAlerts Client
//  Application
//
//  Copyright Softomate 2006-2009. All rights reserved.
//

#pragma once

#define _STANDBY_TIMER_	111
#define _UPDATE_TIMER_	112
// Necessary to reload application on lost connection
#define _RELOAD_TIMER_  113
// Restart timeout
#define LOST_CONNECTION_TIMEOUT 30000//10800000
//
//
//
#define _NORMAL_	0
#define _STANDBY_	1
#define _DISABLE_	2

// the message below sends when connection lost and need to start reload timer
#define UWM_NOTIFY_LOST_CONNECTION (WM_APP + 319)
// the message below sends when connection established and need to stop reload timer
#define UWM_NOTIFY_ESTABLISHED_CONNECTION (WM_APP + 320)
#define UWM_NOTIFY_RESTART_APPLICATION (WM_APP + 321)


static UINT UWM_TASKBARCREATED = ::RegisterWindowMessage(_T("TaskbarCreated"));

class CMainFrame :
		public CFrameWindowImpl<CMainFrame>,
		public CUpdateUI<CMainFrame>
{
private:
	HMODULE m_hLib;
	DWORD m_initFlags;

	WORD m_reloadTimer;
	BOOL m_restartTimerIsActive;

	HWND m_sysMenuHWND;
	//mutex to manage stability from updater
	HANDLE m_statMutex;

public:
	CMainFrame(DWORD initFlags);

	DWORD m_iNormalToStandby,
		  m_iUpdateExpire,
		  m_iMode;

	CString Invoke(CString sFunc, CString sParam=CString(_T("")));

	DECLARE_FRAME_WND_CLASS(NULL, IDR_MAINFRAME)

	BEGIN_UPDATE_UI_MAP(CMainFrame)
	END_UPDATE_UI_MAP()

	BEGIN_MSG_MAP(CMainFrame)
		MESSAGE_HANDLER(WM_TIMER, OnTimer)
		MESSAGE_HANDLER(UWM_TASKBARCREATED, OnTaskBarCreated)
		MESSAGE_HANDLER(WM_CREATE, OnCreate)
		MESSAGE_HANDLER(WM_CLOSE, OnClose)
		MESSAGE_HANDLER(WM_DESTROY, OnDestroy)
		COMMAND_ID_HANDLER(ID_APP_EXIT, OnFileExit)
		COMMAND_ID_HANDLER(ID_FILE_NEW, OnFileNew)
		COMMAND_ID_HANDLER(ID_APP_ABOUT, OnAppAbout)
		CHAIN_MSG_MAP(CUpdateUI<CMainFrame>)
		CHAIN_MSG_MAP(CFrameWindowImpl<CMainFrame>)
	END_MSG_MAP()

protected:
// Handler prototypes (uncomment arguments if needed):
//	LRESULT MessageHandler(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
//	LRESULT CommandHandler(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
//	LRESULT NotifyHandler(int /*idCtrl*/, LPNMHDR /*pnmh*/, BOOL& /*bHandled*/)
	LRESULT OnTimer(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnCreate(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT OnClose(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT OnDestroy(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled);
	LRESULT OnFileExit(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	LRESULT OnFileNew(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	LRESULT OnAppAbout(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	LRESULT OnTaskBarCreated(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL & bHandled);
	LRESULT OnPowerMsgRcvd(UINT /*uMsg*/, WPARAM wParam, LPARAM /*lParam*/, BOOL& bHandled);

	// Necessary to restart DeskAlerts if connection lost
	LRESULT OnLostConnection(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled);
	LRESULT OnEstablishedConnection(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled);
	LRESULT OnRestartApplication(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled);

	VOID CALLBACK RestartApplication(HWND wHandle, UINT /*param1*/, UINT_PTR /*param2*/, DWORD /*param3*/);
	//
	//
	//
	
	VOID InitUpdater();
public:
	void standBy(bool onOff,bool manual);
	//CAlertController alertController;
};
