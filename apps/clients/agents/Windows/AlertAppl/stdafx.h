//
//  stdafx.h
//  DeskAlerts Client
//  Application
//
//  Copyright Softomate 2006-2009. All rights reserved.
//

#pragma once

// Change these values to use different versions
//#define WINVER		0x0500
//#define _WIN32_WINNT	0x0501
//#define _WIN32_IE	0x0501
//#define _RICHEDIT_VER	0x0200

// ATL secured functions.
#define _SECURE_ATL 1

// STL errors handling.
// _SECURE_SCL=1 for enabled state, 0 for disabled.
// _SECURE_SCL_THROWS=1 for throwing exception, 0 for abnormal program termination.
#ifdef _SECURE_SCL
#undef _SECURE_SCL
#endif
#ifdef _SECURE_SCL_THROWS
#undef _SECURE_SCL_THROWS
#endif
#define _SECURE_SCL 1
#define _SECURE_SCL_THROWS 1

#ifndef release_try
#ifdef DEBUG
#define release_try {
#define release_end_try }
#define release_catch_all } if(0) {
#define release_catch_end }
#define delete_catch(x) delete x; //use debug and/or brain!
#else
#define release_try try {
#define release_end_try } catch(...) {}
#define release_catch_all } catch(...) {
#define release_catch_end }
#define delete_catch(x) try{ delete x; } catch(...) {} //ha-ha-ha! we need to avoid stupid crashes.
#endif
#endif

#define _WTL_NO_CSTRING

#include <atlstr.h>

#include <atlbase.h>
#include <atlapp.h>

extern CAppModule _Module;

#include <atlhost.h>
#include <atlwin.h>
#include <atlctl.h>

#include <atlframe.h>
#include <atlctrls.h>
#include <atldlgs.h>


#include <atlutil.h>
#include <atlmisc.h>



