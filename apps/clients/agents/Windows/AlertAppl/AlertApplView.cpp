//
//  AlertApplView.cpp
//  DeskAlerts Client
//  Application
//
//  Copyright Softomate 2006-2009. All rights reserved.
//

#include "stdafx.h"
#include "resource.h"

#include "AlertApplView.h"

BOOL CAlertApplView::PreTranslateMessage(MSG* pMsg)
{
	const UINT uMsg = pMsg->message;
	if(!((uMsg>=WM_KEYFIRST && uMsg<=WM_KEYLAST) ||
		(uMsg>=WM_MOUSEFIRST && uMsg<=WM_MOUSELAST)))
		return FALSE;

	// give HTML page a chance to translate this message
	return (BOOL)SendMessage(WM_FORWARDMSG, 0, (LPARAM)pMsg);
}
