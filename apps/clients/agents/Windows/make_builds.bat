@ECHO OFF
cls
if %~d0 == \\ (
	echo.
	echo This script can't be run from UNC path
	echo.
	pause
	goto :eof
)
rem //////////////////////////////////////////////////////////
rem ////       move to and call MSVS settings
rem //////////////////////////////////////////////////////////
cd /d "%ProgramFiles(x86)%"||cd /d "%ProgramFiles%"
cd "Microsoft Visual Studio\2017\Enterprise\"
call "C:\Program Files (x86)\Microsoft Visual Studio\2017\Enterprise\Common7\Tools\VsDevCmd.bat"
cd /d "%~dp0"
echo.
echo.Building project with Visual Studio...
echo.
echo.^>^>Release

rem //////////////////////////////////////////////////////////
rem ////       build Release and Trial version of 'DeskAlerts.sln', then check '.dll' and '.exe' files
rem //////////////////////////////////////////////////////////

devenv.com DeskAlerts.sln /build "Release"
if not "%ERRORLEVEL%" == "0" devenv.com DeskAlerts.sln /rebuild "Release|Win32"
echo.
echo.^>^>Release Trial
devenv.com DeskAlerts.sln /build "Release Trial"
echo.
echo.
echo.
if not exist bin\DeskAlerts.dll (
	echo. ERROR: Can't find 'DeskAlerts.dll' file.
	echo.
	pause
	exit /b 1
)
for %%F in (bin\DeskAlerts.dll) do if %%~zF GTR 2300000 (
	rem echo. ERROR: 'DeskAlerts.dll' is DEBUG version.
	rem echo.
	rem pause
	rem exit /b 1
)
if not exist bin\DeskAlerts.exe (
	echo. ERROR: Can't find 'DeskAlerts.exe' file.
	echo.
	pause
	exit /b 1
)
for %%F in (bin\DeskAlerts.exe) do if %%~zF GTR 500000 (
	rem echo. ERROR: 'DeskAlerts.exe' is DEBUG version.
	rem echo.
	rem pause
	rem exit /b 1
)
if not exist bin\DeskAlerts.trial.exe (
	echo. WARNING: Can't find 'DeskAlerts.trial.exe' file.
	echo.
)
for %%F in (bin\DeskAlerts.trial.exe) do if %%~zF GTR 500000 (
	rem echo. ERROR: 'DeskAlerts.trial.exe' is DEBUG version.
	rem echo.
	rem pause
	rem exit /b 1
)
if not exist bin\eDirectory.dll (
	echo. WARNING: Can't find 'eDirectory.dll' file.
	echo.
)
for %%F in (bin\eDirectory.dll) do if %%~zF GTR 500000 (
	echo. ERROR: 'eDirectory.dll' is DEBUG version.
	echo.
	pause
	exit /b 1
)

if not exist bin\DA_killer.dll (
	echo. ERROR: Can't find 'DA_killer.dll' file.
	echo.
	pause
	exit /b 1
)
if exist __Release\data\version\icon.ico (
	echo. ATTENTION!!! FOUND ICON FILE
	echo.
)
rem set revision=0
rem for /f "tokens=5" %%i in ('SubWCRev .^|find "Last committed at revision"') do set revision=%%i
rem echo Number of sources revision: %revision% [will be add to version]
rem echo Version format: major.minor.build.revision
rem set "style=0.0.0"
rem set "format=^[0-9][0-9]*\.[0-9][0-9]*\.[0-9][0-9]*$"
echo.

rem //////////////////////////////////////////////////////////
rem ////       entering product version
rem //////////////////////////////////////////////////////////

set "style=0.0.0.0abrc"
set "format=^[0-9][0-9]*\.[0-9][0-9]*\.[0-9][0-9]*\.[0-9a-z][0-9a-z]*$"

IF "%1" == "teamcity" (
	set version=%2
) ELSE (
	if not exist lastversion.log (
		goto again
	) ELSE (
		goto readVersionFile
	)
	:readVersionFile
	set /p version=<lastversion.log
	echo %version%|findstr /i /r "%format%" > nul
	if errorlevel 1 goto again
	echo Please push Return key for keep version %version%
	set /p version=or enter other version in style %style%  
	goto check
	:again
	set /p version=Please enter version in style %style%  
	:check
	echo %version%|findstr /i /r "%format%" > nul
	if errorlevel 1 echo Wrong version string. & goto again
)


echo %version%>lastversion.log
rem set attr=/DPRODUCT_VERSION=%version%.%revision%
set attr=/DPRODUCT_VERSION=%version%

for /f "delims=." %%a in ("%version%") do set major=%%a
if %major% gtr 4 set attr=%attr% /DfiveClient
if %major% gtr 5 set attr=%attr% /DsixClient
if %major% gtr 6 set attr=%attr% /DsevenClient
if %major% gtr 7 set attr=%attr% /DeightClient

ECHO.
set choices=n
set lastchoices=______
if not exist lastchoices.log goto no_lastchoices
set /p lastchoices=<lastchoices.log
set lastchoices=%lastchoices%______
:no_lastchoices

rem //////////////////////////////////////////////////////////
rem ////       entering AD options
rem //////////////////////////////////////////////////////////


IF "%1" == "teamcity" (
	set ad=y
	set attr=%attr% /DisADAddon
) ELSE (
	goto again_ad
	:again_ad
	set ad=%lastchoices:~1,1%
	set text=
	if not "%ad%" == "_" set text=last value: %ad%
	set /p ad=Do you want to add "AD add-on" support? %text%[y/n] 
	if /i "%ad:~0,1%"=="n" set ad=n& goto no_ad
	if /i not "%ad:~0,1%"=="y" goto again_ad
	set attr=%attr% /DisADAddon
	set ad=y
	:no_ad
	set choices=%choices%%ad%
)


rem //////////////////////////////////////////////////////////
rem ////       entering SyncFree options
rem //////////////////////////////////////////////////////////

IF "%1" == "teamcity" (
	set sfad=y
	set attr=%attr% /DisSynFreeADAddon
) ELSE (
	goto again_sfad
	:again_sfad
	set sfad=%lastchoices:~2,1%
	set text=
	if not "%sfad%" == "_" set text=last value: %sfad%
	set /p sfad=Do you want to add "Sync-Free AD add-on" support? %text%[y/n] 
	if /i "%sfad:~0,1%"=="n" set sfad=n& goto no_sfad
	if /i not "%sfad:~0,1%"=="y" goto again_sfad
	set attr=%attr% /DisSynFreeADAddon
	set sfad=y
	:no_sfad
	set choices=%choices%%sfad%
)

rem //////////////////////////////////////////////////////////
rem ////       entering ComputerName options
rem //////////////////////////////////////////////////////////

IF "%1" == "teamcity" (
	set cn=y
	set attr=%attr% /DisCNAddon
) ELSE (
	goto again_cn
	:again_cn
	set cn=%lastchoices:~3,1%
	set text=
	if not "%cn%" == "_" set text=last value: %cn%
	set /p cn=Do you want to add "Computer Name add-on" support? %text%[y/n] 
	if /i "%cn:~0,1%"=="n" set cn=n& goto no_cn
	if /i not "%cn:~0,1%"=="y" goto again_cn
	set attr=%attr% /DisCNAddon
	set cn=y
	:no_cn
	set choices=%choices%%cn%
)

rem //////////////////////////////////////////////////////////
rem ////       entering Trial options
rem //////////////////////////////////////////////////////////

IF "%1" == "teamcity" (
	set dec=y
	set attr=%attr% /DDECRYPT
) ELSE (
	goto again_dec
	:again_dec
	set dec=%lastchoices:~4,1%
	set text=
	if not "%dec%" == "_" set text=last value: %dec%
	set /p dec=Do you want to add decryption support to trial version? %text%[y/n] 
	if /i "%dec:~0,1%"=="n" set dec=n& goto no_dec
	if /i not "%dec:~0,1%"=="y" goto again_dec
	set attr=%attr% /DDECRYPT
	set dec=y
	:no_dec
	set choices=%choices%%dec%
)


rem IF "%1" == "teamcity" (
	set serv=y
rem ) ELSE (
rem	goto again_serv
rem    :again_serv
rem	set serv=%lastchoices:~5,1%
rem	set text=
rem	if not "%serv%" == "_" set text="last value: %serv%"
rem	set /p serv=Do you want to add the service? %text%[y/n] 
rem	if /i "%serv:~0,1%"=="n" set serv=n& goto no_serv
rem	if /i not "%serv:~0,1%"=="y" goto again_serv
rem 	set serv=y
	if not exist bin\DeskAlertsService.exe (
		echo.
		echo. ERROR: Can't create with service.
		echo.
		pause
		exit /b 1
	)
	:no_serv
	set choices=%choices%%serv%
rem )

rem //////////////////////////////////////////////////////////
rem ////       entering ScreenSaver options
rem //////////////////////////////////////////////////////////

IF "%1" == "teamcity" (
	set scrsvr=y
	set attr=%attr% /DisScrsvrAddon
) ELSE (
	goto again_scrsvr
	:again_scrsvr
	set scrsvr=%lastchoices:~6,1%
	set text=
	if not "%scrsvr%" == "_" set text=last value: %scrsvr%
	set /p scrsvr=Do you want to add "Screen Saver add-on" support? %text%[y/n] 
	if /i "%scrsvr:~0,1%"=="n" set scrsvr=n& goto no_scrsvr
	if /i not "%scrsvr:~0,1%"=="y" goto again_scrsvr
	set attr=%attr% /DisScrsvrAddon
	set scrsvr=y
	if not exist bin\DeskAlertsScreenSaver.scr (
		echo.
		echo. ERROR: Can't create with Screen Saver. bin\DeskAlertsScreenSaver.scr is not exists.
		echo.
		pause
		exit /b 1
	)
	:no_scrsvr
	set choices=%choices%%scrsvr%
)


ECHO %choices%>lastchoices.log
ECHO.

mkdir "client.%version%"

echo Delete existent archives
del "___Output\client.%version%.zip" /Q 2>nul
del "___Output\client.%version%.trial.zip" /Q 2>nul
del "___Output\client.%version%.trial.nosrv.zip" /Q 2>nul
del "___Output\client.%version%.trial.noserver.zip" /Q 2>nul
del "client.%version%\*" /Q /F 2>nul

echo Copying new files
mkdir "client.%version%\data"
mkdir "client.%version%\release"

xcopy /Y /E /Q /I /R "__Release\data" "client.%version%\data"
xcopy /Y /E /Q /I /R "DeskalertsBuilder\blank" "client.%version%\data\blank"
xcopy /Y /E /Q /I /R "DeskalertsBuilder\mac" "client.%version%\data\mac"
xcopy /Y /E /Q /I /R "DeskalertsBuilder\deb" "client.%version%\data\deb"
xcopy /Y /E /Q /I /R "DeskalertsBuilder\etc" "client.%version%\data\etc"
xcopy /Y /E /Q /I /R "DeskalertsBuilder\msi" "client.%version%\data\msi"
xcopy /Y /E /Q /I /R "DeskalertsBuilder\nsisdesk" "client.%version%\data\nsisdesk"
xcopy /Y /E /Q /I /R "DeskalertsBuilder\version" "client.%version%\data\version"

if '%ad%'=='y' goto y_ad
del "client.%version%\data\version\hg29fhh847c03nu08f.key" /Q /F
:y_ad

if '%sfad%'=='y' goto y_sfad
del "client.%version%\data\version\kamewq9nsb8k0o22iv.key" /Q /F
:y_sfad

if '%ed%'=='y' goto y_ed
del "client.%version%\data\version\vy0wm1iphva12qer4y.key" /Q /F
goto end_ed
:y_ed
copy /B /Y "bin\eDirectory.dll" "client.%version%\data\version\eDirectory.dll"
call :setversion "client.%version%\data\version\eDirectory.dll" "" "%version%"
:end_ed

if '%od%'=='y' goto y_od
del "client.%version%\data\version\f8x4c0hwl.key" /Q /F
:y_od

if '%mac%'=='y' goto y_mac
del "client.%version%\data\version\buildMAC.bat" /Q /F
rmdir "client.%version%\data\mac" /Q /S
goto end_mac
:y_mac
if exist __Release\data\version\icon.icns (
	copy /b /y __Release\data\version\icon.icns client.%version%\data\mac\icon.icns
)
:end_mac

if '%linux%'=='y' goto y_linux
del "client.%version%\data\version\buildDEB.bat" /Q /F
rmdir "client.%version%\data\deb" /Q /S
goto end_linux
:y_linux
if exist __Release\data\version\icon.png (
	copy /b /y __Release\data\version\icon.png client.%version%\data\deb\usr\share\pixmaps\DeskAlerts.png
)
:end_linux

copy /B /Y "bin\DeskAlerts.dll" "client.%version%\data\version\deskalerts.dll"
copy /B /Y "bin\DeskAlerts.exe" "client.%version%\data\version\deskalerts.exe"
copy /B /Y "bin\deskalerts_updater.exe" "client.%version%\data\version\deskalerts_updater.exe"
copy /B /Y "bin\DA_killer.dll" "client.%version%\data\version\DA_killer.dll"
rem copy the debug information for the correct description of the exception
copy /B /Y "bin\*.pdb" "client.%version%\data\version\"

call :setversion "client.%version%\data\version\deskalerts.dll" "%version%" "%version%"
call :setversion "client.%version%\data\version\deskalerts.exe" "%version%" "%version%"

set sign="client.%version%\data\version\deskalerts_updater.exe" "%version%" "%version%"
if not '%serv%'=='y' goto n_serv
copy /B /Y "bin\DeskAlertsService.exe" "client.%version%\data\version\DeskAlertsService.exe"
call :setversion "client.%version%\data\version\DeskAlertsService.exe" "%version%" "%version%"
set /a sigb+="client.%version%\data\version\DeskAlertsService.exe"
:n_serv

if not '%scrsvr%'=='y' goto n_scrsvr
copy /B /Y "bin\DeskAlertsScreenSaver.scr" "client.%version%\data\version\DeskAlertsScreenSaver.scr"
call :setversion "client.%version%\data\version\DeskAlertsScreenSaver.scr" "%version%" "%version%"
:n_scrsvr

if exist __Release\data\version\icon.ico (
	reshacker.exe -addoverwrite "client.%version%\data\version\deskalerts.exe", "client.%version%\data\version\deskalerts.exe", "__Release\data\version\icon.ico", ICONGROUP, 128, 1033
)
if exist __Release\data\version\icon.ico (
	reshacker.exe -addoverwrite "client.%version%\data\version\deskalerts_updater.exe", "client.%version%\data\version\deskalerts_updater.exe", "__Release\data\version\icon.ico", ICONGROUP, 128, 1033
)

NSIS\makensis %attr% DeskalertsBuilder\ClientInstallationSRC > build.log

DeskalertsBuilder\signtool.exe sign /tr http://timestamp.comodoca.com/?td=sha256 /td sha256 /fd sha256 /f DeskalertsBuilder\CodeSignCertificate.pfx /p DeskAlerts /a DeskalertsBuilder\ClientInstallation.exe

move /Y "DeskalertsBuilder\ClientInstallation.exe" "client.%version%\ClientInstallation.exe"

echo DeskAlerts v%version%>"client.%version%\data\version\version.txt"
echo Clear working files

del "client.%version%\data\release\*" /Q /F
del "___Output\DeskAlerts.Client.v%version%%name%.zip" /Q /F

7za a "___Output\DeskAlerts.Client.v%version%%name%.zip" "client.%version%\*" > nul

if not exist bin\DeskAlerts.trial.exe (
	echo.
	echo. ERROR: Can't create trial versions!
	echo.
) else (
	copy /B /Y "bin\DeskAlerts.trial.exe" "client.%version%\data\version\deskalerts.exe"
	if exist __Release\data\version\icon.ico (
		reshacker.exe -addoverwrite "client.%version%\data\version\deskalerts.exe", "client.%version%\data\version\deskalerts.exe", "__Release\data\version\icon.ico", ICONGROUP, 128, 1033
	)

	if exist __Release\data\version\icon.ico (
		reshacker.exe -addoverwrite "client.%version%\data\version\deskalerts.exe", "client.%version%\data\version\deskalerts.exe", "__Release\data\version\icon.ico", ICONGROUP, 128, 1033
	)

	NSIS\makensis %attr% /DTRIAL DeskalertsBuilder\ClientInstallationSRC > build.trial.log
	rem DeskalertsBuilder\signtool.exe sign /f DeskalertsBuilder\ToolbarStudioInc.pfx /p softomate54Pas /t http://tsa.starfieldtech.com/ /v DeskalertsBuilder\ClientInstallation.exe > nul
	move /Y "DeskalertsBuilder\ClientInstallation.exe" "client.%version%\ClientInstallation.exe"

	del "___Output\DeskAlerts.Client.v%version%%name%.trial.zip" /Q /F
	7za a "___Output\DeskAlerts.Client.v%version%%name%.trial.zip" "client.%version%\*" > nul

	NSIS\makensis %attr% /DNOSERVER DeskalertsBuilder\ClientInstallationSRC > build.noserver.log
	rem DeskalertsBuilder\signtool.exe sign /f DeskalertsBuilder\ToolbarStudioInc.pfx /p softomate54Pas /t http://tsa.starfieldtech.com/ /v DeskalertsBuilder\ClientInstallation.exe > nul
	move /Y "DeskalertsBuilder\ClientInstallation.exe" "client.%version%\ClientInstallation.exe"

	del "___Output\DeskAlerts.Client.v%version%%name%.trial.nosrv.zip" /Q /F
	7za a "___Output\DeskAlerts.Client.v%version%%name%.trial.nosrv.zip" "client.%version%\*" > nul
)
copy /B /Y "bin\*.pdb" "___Output\"

:loop
rd /s /q "client.%version%" > nul
if exist "client.%version%" goto loop

pause

goto :eof

:setversion

set "binpath=%~1"
set "fileversion=%~2"
set "prodversion=%~3"
set "rcfile=%temp%\%random%%random%.rc"

reshacker.exe -extract "%binpath%", "%rcfile%", VERSIONINFO, 1, 1033
if not "%fileversion%" == "" (
	call :replace "FILEVERSION" "FILEVERSION %fileversion:.=,%" "%rcfile%"
	call :replace "VALUE ""FileVersion""" "		VALUE ""FileVersion"", ""%fileversion:.=, %""" "%rcfile%"
)
if not "%prodversion%" == "" (
	call :replace "PRODUCTVERSION" "PRODUCTVERSION %prodversion:.=,%" "%rcfile%"
	call :replace "VALUE ""ProductVersion""" "		VALUE ""ProductVersion"", ""%prodversion:.=, %""" "%rcfile%"
)
rc.exe /nologo /fo "%rcfile%.res" "%rcfile%"
reshacker.exe -addoverwrite "%binpath%", "%binpath%", "%rcfile%.res", VERSIONINFO, 1, 1033
del "%rcfile%.res" /Q /F
del "%rcfile%" /Q /F

exit /b 0

:replace
setlocal enabledelayedexpansion

set "search=%~1"
set "replace=%~2"
set "infile=%~3"
set "outfile=%~4"

set "replace=%replace:""="%"

if "%outfile%" == "" goto usetemp
if /i "%infile%" == "%outfile%" goto usetemp
goto start

:usetemp
set usetemp=1
set "outfile=%temp%\%random%%random%.tmp"

:start
setlocal enabledelayedexpansion
del /q /f "%outfile%" >nul 2>&1

set /a skip=0
for /f "tokens=1 delims=:" %%i in ('findstr /n /c:"%search%" "%infile%"') do (
	call :before %%i !skip!
	1>>"%outfile%" echo.%replace:&=^&%
	set /a skip=%%i
)
call :after !skip!

if not "%usetemp%" == "1" goto :eof
move /y "%outfile%" "%infile%" >nul 2>&1
exit /b 0

:before
setlocal enabledelayedexpansion
set /a pos=%2+1
if not "%2" == "0" (set "doskip=skip=%2") else (set "doskip=")
for /f "tokens=1* %doskip% delims=:" %%i in ('findstr /n /r ".*" "%infile%"') do (
	if !pos! == %1 (
		exit /b 0
	)
	1>>"%outfile%" echo.%%j
	set /a pos+=1
)
exit /b 0

:after
setlocal enabledelayedexpansion
if not "%1" == "0" (set "doskip=skip=%1") else (set "doskip=")
for /f "tokens=1* %doskip% delims=:" %%i in ('findstr /n /r ".*" "%infile%"') do (
	1>>"%outfile%" echo.%%j
)
exit /b 0
