// DeskAlerts Converter.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "image.h"
#include "utils.h"

int _tmain(int argc, _TCHAR* argv[])
{
	//::MessageBox(0,0,0,0);
	srand(GetTickCount());

	const TCHAR *arcName = _T("Archive.pax.gz");
	CString temp, tempPath, appPath, tb_name, tb_bundle, tb_cab = _T("deskalerts.cab");

	appPath = argv[0];
	if (argc > 1) 
	{
		tb_cab = argv[1];
	}

	//get name of toolbar binary
	int pos = tb_cab.ReverseFind(_T('\\'));
	tb_name = tb_cab.Mid(pos+1);
	tb_name.Replace(_T(".cab"), _T(""));
	tb_name.Replace(_T(".CAB"), _T(""));
	//tb_name = _T("SoftomateToolbar");

	//name of bundle (don't change)
	//tb_bundle = tb_name;
	tb_bundle = _T("DeskAlerts");
	tb_bundle += _T(".app");

	//get application path
	pos = appPath.ReverseFind(_T('\\'));
	appPath = appPath.Left(pos+1);

	if(appPath.IsEmpty())
	{
		::GetModuleFileName(0, appPath.GetBuffer(MAX_PATH), MAX_PATH);
		appPath.ReleaseBuffer();
		pos = appPath.ReverseFind(_T('\\'));
		appPath = appPath.Left(pos+1);
	}
	if(appPath.IsEmpty())
	{
		::GetCurrentDirectory(MAX_PATH, appPath.GetBuffer(MAX_PATH));
		appPath.ReleaseBuffer();
	}
	if(appPath.Right(1) != _T('\\'))
	{
		appPath += _T('\\');
	}
	cmd_cd(appPath); //for be care

	//get temp path
	::GetTempPath(MAX_PATH, tempPath.GetBuffer(MAX_PATH));
	tempPath.ReleaseBuffer();

	if(tempPath.Right(1) != _T('\\'))
	{
		tempPath += _T('\\');
	}

	//create temp folder
	temp.Format(_T("deskalerts_converter_%d"), rand()); //without \ for rmdir
	tempPath += temp;

	//make temp dir
	cmd_mkdir(tempPath);

	//extract the cab file of a project
	cmd_mkdir(tempPath + _T("\\deskalerts.cab"));
	temp = _T("expand.exe -F:* \"");
	temp += tb_cab;
	temp += _T("\" \"");
	temp += tempPath;
	temp += _T("\\deskalerts.cab\"");
	exec_external(temp);

	//change current dir to temp path
	cmd_cd(tempPath);

	//extract the installer ingot
	temp = _T("\"");
	temp += appPath;
	temp += _T("7za.exe\" x -y \"");
	temp += appPath;
	temp += _T("DeskAlerts.pkg.zip\"");
	exec_external(temp);

	//extract the toolbar ingot from installer
	temp = _T("\"");
	temp += appPath;
	temp += _T("bsdtar.exe\" -x -f \"DeskAlerts.pkg\\Contents\\Archive.pax.gz\" -p");
	exec_external(temp);


	//convert images
	//cmd_cd(_T("toolbar.cab"));
	//init_img(_T("icons.bmp"));

	//save_icons();	
	//save_images();

	//change current dir to temp path
	cmd_cd(tempPath);

	//clear
	cmd_del(_T("DeskAlerts.app\\Contents\\Resources\\html\\*.*"));

	//copy files from cab to resources folder
	cmd_copy(_T("deskalerts.cab\\*"), _T("DeskAlerts.app\\Contents\\Resources\\html\\"));

	//delete unused files in resources folder
	cmd_cd(_T("DeskAlerts.app\\Contents\\Resources\\html\\"));
	cmd_del(_T("*.dll *.exe"));

	//copy icon
	cmd_copy(appPath + _T("icon.icns"), tempPath + _T("\\DeskAlerts.app\\Contents\\Resources\\da.icns"));

	//rename toolbar binary file
	//cmd_cd(_T("..\\MacOS"));
	//cmd_move(_T("SoftomateToolbar"), tb_name);

	//rename toolbar bundle
	cmd_cd(tempPath);
	//cmd_move(_T("DeskAlerts.app"), tb_bundle);

	//pack app
	temp = _T("\"");
	temp += appPath;
	temp += _T("7za.exe\" a -tzip -y \"");
	temp += appPath;
	temp += _T("\\");
	temp += tb_name;
	temp += _T(".zip\" \"");
	temp += tb_bundle;
	temp += _T("\\*\"");
	exec_external(temp);

	//pack bundle to installer
	temp = _T("\"");
	temp += appPath;
	temp += _T("bsdtar.exe\" -c -z --format pax -f \"DeskAlerts.pkg\\Contents\\Archive.pax.gz\" \"");
	temp += tb_bundle;
	temp += _T("\"");
	exec_external(temp);

	//rename toolbar installer
	//cmd_move(_T("DeskAlerts.pkg"), tb_name + _T(".pkg"));

	//pack installer
	temp = _T("\"");
	temp += appPath;
	temp += _T("7za.exe\" a -tzip -y \"");
	temp += appPath;
	temp += _T("\\");
	temp += tb_name;
	temp += _T("_setup.zip\" \"");
	temp += tb_name;
	temp += _T(".pkg\\*\" __MACOSX\\*");
	exec_external(temp);

	//delete the temp files
	cmd_cd(appPath); //we can't delete current dir
	cmd_rmdir(tempPath);

	return 0;
}

