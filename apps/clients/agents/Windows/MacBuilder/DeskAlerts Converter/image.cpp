#include <stdio.h>
#include "stdafx.h"
#include "image.h"
#include "FreeImage.h"
#include <string>

FIBITMAP *imgs = NULL, *imgs2 = NULL;



static void MyMessageFunc(FREE_IMAGE_FORMAT fif, const char *message) 
{
	printf ("\nFreeImage error: %s\n", message);
}



void init_img (const TCHAR *fn) 
{
    FreeImage_SetOutputMessage(MyMessageFunc);
	_tprintf (_T("loading img from %s\n"), fn);
    imgs = FreeImage_Load(FIF_BMP, CT2A(fn), 0);
	_tprintf (_T("\timg size %d*%d\n"), FreeImage_GetWidth(imgs), FreeImage_GetHeight(imgs));
}


void init_hot_img (const TCHAR *fn) 
{
	_tprintf (_T("loading hot img from %s\n"), fn);
    imgs2 = FreeImage_Load(FIF_BMP, CT2A(fn), 0);
	_tprintf (_T("\timg size %d*%d\n"), FreeImage_GetWidth(imgs2), FreeImage_GetHeight(imgs2));
}


static void SaveTransparent (FIBITMAP *h, const char *fn, FIBITMAP *trans = NULL) 
{
    FIBITMAP *p=NULL, *transp=NULL;
    BYTE Transparency[256], tr;
	int i;

	p=ConvertTo8Bits(h);		
	
	if (trans == NULL) 
	{		
		transp=p;
	}
	else
	{
		transp=ConvertTo8Bits(trans);		
	}
	
	FreeImage_SetTransparent (p, true);
	FreeImage_GetPixelIndex (transp, 0, (FreeImage_GetHeight(transp)-1), &tr);

    for (i = 0; i < 256; i++) {
        Transparency[i] = 0xFF;
    }
	Transparency[tr] = 0x00;

    FreeImage_SetTransparencyTable (p, Transparency, 256);

	if (!FreeImage_Save (FIF_PNG, p, fn, 0))
		printf ("Error: can't convert image from %s\n", fn);    
}


FIBITMAP* ConvertTo8Bits(FIBITMAP* im)
{
	int bpp = FreeImage_GetBPP(im);
	FIBITMAP *im8bpp=NULL, *im24bpp=NULL;

	if (bpp == 8) 
	{
		im8bpp = im;
	}
	else
	{
		if (bpp > 8) 
		{
			im24bpp = FreeImage_ConvertTo24Bits(im);
			im8bpp = FreeImage_ColorQuantize(im24bpp, FIQ_WUQUANT);
			if (im8bpp == NULL)
				im8bpp = FreeImage_ColorQuantize(im24bpp, FIQ_NNQUANT);
		}
		else
			im8bpp = FreeImage_ConvertTo8Bits(im);
	}

	if (im8bpp == NULL) 
	{
		_tprintf (_T("Warning: if toolbar icons or images will be grayscale change its colour depth to 8 bits"));
		im8bpp = FreeImage_ConvertTo8Bits(im);
	}
	return im8bpp;
}

void save_icons()
{
	char fn[80];
    FIBITMAP *p, *tr=NULL, *y=NULL;
	FIBITMAP *im=imgs;
	
	if (im == NULL) return;

	int h=FreeImage_GetHeight(im);
	int w=FreeImage_GetWidth(im);

	y=ConvertTo8Bits(im);	

	tr = FreeImage_Copy(y, 0, 0, h, h);
	int i;
	for(i=1; i<= (w/h); i++)
	{		
		p = FreeImage_Copy(y, h*(i-1), 0, h*i, h);   
		
//		sprintf (fn, "img%d.png", i);
		sprintf (fn, "%d.png", i);
		SaveTransparent (p, fn, tr);
		FreeImage_Unload (p);
	}

	im=imgs2;
	if (im == NULL) return;
	h=FreeImage_GetHeight(im);
	w=FreeImage_GetWidth(im);

	y=ConvertTo8Bits(im);

	FreeImage_Unload (tr);
	tr = FreeImage_Copy(y, 0, 0, h, h);

	for(i=1; i<= (w/h); i++)
	{		
		p = FreeImage_Copy(y, h*(i-1), 0, h*i, h);
		
   		sprintf (fn, "himg%d.png", i);
		SaveTransparent (p, fn, tr);
		FreeImage_Unload (p);
	}
}

void request_image (const TCHAR *image) 
{
    CString fn = image;
    FIBITMAP *p;

	_tprintf (_T("Converting image from %s\n"), image);
    p = FreeImage_Load(FIF_BMP, CT2A(image), 0);
    if (!p) _tprintf (_T("\terror loading \"%s\"\n"), image);
	
	fn.Replace(_T("BMP"), _T("png"));
	fn.Replace(_T("bmp"), _T("png"));

	SaveTransparent (p, CT2A(fn));
	FreeImage_Unload (p);
}

void save_images()
{
	WIN32_FIND_DATA FileData; 
	HANDLE hSearch; 
	 
	BOOL fFinished = FALSE; 
 
	hSearch = FindFirstFile(_T("*.bmp"), &FileData); 

	if (hSearch == INVALID_HANDLE_VALUE) 
	{ 
		_tprintf(_T("No .bmp files found."));
		return;
	} 

	while (!fFinished) 
	{  			
		request_image(FileData.cFileName);

		if (!FindNextFile(hSearch, &FileData)) 
		{
			if (GetLastError() == ERROR_NO_MORE_FILES) 
			{ 
				fFinished = TRUE; 
			} 
			else 
			{ 
				printf("Couldn't find next file."); 
				return;
			} 
		}
	} 
	FindClose(hSearch);
}