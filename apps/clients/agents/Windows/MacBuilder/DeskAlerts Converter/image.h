#ifndef __IMAGE_H__
#define __IMAGE_H__

#include "FreeImage.h"

void init_img (const TCHAR *fn);
void init_hot_img (const TCHAR *fn);
void request_img (int img);
void request_hot_img (int img);
void request_image (const char *image);
void save_icons();
void save_images();
FIBITMAP* ConvertTo8Bits(FIBITMAP* im);

#endif /* __IMAGE_H__ */
