//
//  AlertViewCell.h
//  Deskalert Mobile
//
//  Created by Дмитрий Кайгородов on 12.03.15.
//  Copyright (c) 2015 Deskalert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlertViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel* titleLabel;

@property (weak, nonatomic) IBOutlet UILabel* dateLabel;
@property (weak, nonatomic) IBOutlet UIImageView *unreadIndicator;
@property (weak, nonatomic) IBOutlet UIImageView *type;

@end
