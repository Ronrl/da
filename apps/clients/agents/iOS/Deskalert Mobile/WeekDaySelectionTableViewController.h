//
//  WeekDaySelectionTableViewController.h
//  Deskalerts Mobile
//
//  Created by Дмитрий Кайгородов on 04/04/16.
//  Copyright © 2016 Deskalert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Unobtrusive.h"
#import "UnobtrusiveDetailsViewController.h"

@interface WeekDaySelectionTableViewController : UITableViewController

@property (nonatomic) Unobtrusive* objectToAdd;
@property (nonatomic) NSMutableArray* days;

@end
