//
//  ViewController.h
//  Deskalert Mobile
//
//  Created by Дмитрий Кайгородов on 10.03.15.
//  Copyright (c) 2015 Deskalert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FirstWelcomeView : UIViewController

@property (weak, nonatomic) IBOutlet UIBarButtonItem *nextButton;
@property (weak, nonatomic) IBOutlet UITextField *hostNameField;

@end

