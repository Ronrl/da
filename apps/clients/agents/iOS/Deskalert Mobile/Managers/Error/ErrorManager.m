//
//  ErrorManager.m
//  Deskalerts
//
//  Created by Ярослав Долговых on 10/02/2020.
//  Copyright © 2020 Deskalert. All rights reserved.
//

#import "ErrorManager.h"
#import <UIKit/UIKit.h>

@implementation ErrorManager

+(void) showAlertWithMessage:(NSString *) message {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)  message:message delegate:nil cancelButtonTitle: NSLocalizedString(@"Ok" , nil) otherButtonTitles:nil];
    [alert show];
}

+(void) showAlertByError:(NSError *) error {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)  message:error.localizedDescription delegate:nil cancelButtonTitle: NSLocalizedString(@"Ok" , nil) otherButtonTitles:nil];
    [alert show];
}
@end
