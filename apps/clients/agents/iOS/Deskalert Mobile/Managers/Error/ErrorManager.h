//
//  ErrorManager.h
//  Deskalerts
//
//  Created by Ярослав Долговых on 10/02/2020.
//  Copyright © 2020 Deskalert. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ErrorManager : NSObject
+(void) showAlertWithMessage:(NSString *) message;
+(void) showAlertByError:(NSError *) error;
@end

NS_ASSUME_NONNULL_END
