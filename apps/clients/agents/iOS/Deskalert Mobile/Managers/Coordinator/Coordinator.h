//
//  Coordinator.h
//  Deskalerts
//
//  Created by Ярослав Долговых on 14/02/2020.
//  Copyright © 2020 Deskalert. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface Coordinator : NSObject

@property (strong, nonatomic) UIWindow *window;
+ (instancetype) shared;
-(void) activate: (UIWindow*) window;
-(void) setRegistrationStackViewControllers;
-(void) setMainStackViewControllers;
-(UIViewController *) showProcessingViewControllerOnTop:(UIViewController *) viewController;
@end

NS_ASSUME_NONNULL_END
