//
//  Coordinator.m
//  Deskalerts
//
//  Created by Ярослав Долговых on 14/02/2020.
//  Copyright © 2020 Deskalert. All rights reserved.
//

#import "Coordinator.h"
#import "UserSettings.h"
#import "DeskAlertServer.h"
#import "ProcessingViewController.h"

@implementation Coordinator
+ (instancetype) shared {
    static Coordinator *shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[Coordinator alloc] init];
    });
    return shared;
}

-(void) activate: (UIWindow*) window {
    self.window = window;
    if (![UserSettings sharedInstance].hasLaunchOnce) {
//    if (true) {
        [self setWelcomeViewController];
    } else {
        [self setDefaultSettings];
        [self setMainStackViewControllers];
    }
}


-(void) setWelcomeViewController {
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *welcomeViewController = [mainStoryboard instantiateViewControllerWithIdentifier:@"WelcomeViewController"];
    self.window.rootViewController = welcomeViewController;
    [self.window makeKeyAndVisible];
}

-(void) setRegistrationStackViewControllers {
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *firstViewController = [mainStoryboard instantiateViewControllerWithIdentifier:@"RegistrationStackNavigationController"];
    self.window.rootViewController = firstViewController;
    [self.window makeKeyAndVisible];
}

-(void) setMainStackViewControllers {
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *mainViewController = [mainStoryboard instantiateViewControllerWithIdentifier:@"tabView"];
    self.window.rootViewController = mainViewController;
    [self.window makeKeyAndVisible];
}


//MARK: - trash
-(void) clearPushNotifications {
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 1];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 0];
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
}

-(void) setDefaultSettings {
    [[DeskAlertServer defaultServer] parseAllRelevantAlerts:YES andAlertId:-1 andFirstPressedAlertId:-1];
    [self clearPushNotifications];
}
//MARK: -
-(UIViewController *) showProcessingViewControllerOnTop:(UIViewController *) viewController {
    viewController.modalPresentationStyle = UIModalPresentationOverFullScreen;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ProcessingViewController *processingViewController = [mainStoryboard instantiateViewControllerWithIdentifier:@"ProcessingViewController"];
    
    [viewController presentViewController:processingViewController animated:true completion:nil];
//    [viewController presentModalViewController:processingViewController animated:true];
    return processingViewController;
    
}

@end
