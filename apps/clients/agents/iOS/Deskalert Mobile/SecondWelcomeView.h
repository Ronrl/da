//
//  SecondWelcomeView.h
//  Deskalert Mobile
//
//  Created by Дмитрий Кайгородов on 10.03.15.
//  Copyright (c) 2015 Deskalert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"


@interface SecondWelcomeView : UIViewController<MBProgressHUDDelegate, UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *loginButton;
@property (weak, nonatomic) IBOutlet UITextField *loginField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (weak, nonatomic) IBOutlet UITextField *domainField;
@property MBProgressHUD *progressBar;

@end
