//
//  UnobtrusiveTableViewCell.h
//  Deskalerts Mobile
//
//  Created by Дмитрий Кайгородов on 06/04/16.
//  Copyright © 2016 Deskalert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UnobtrusiveTableViewCell : UITableViewCell

@property (nonatomic) int uId;
@property (weak, nonatomic) IBOutlet UILabel *from;
@property (weak, nonatomic) IBOutlet UILabel *to;
@property (weak, nonatomic) IBOutlet UILabel *repeat;
@property (weak, nonatomic) IBOutlet UISwitch *enabled;

@end
