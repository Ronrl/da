//
//  DeskAlertResponseDefines.h
//  Deskalert Mobile
//
//  Created by Дмитрий Кайгородов on 13.03.15.
//  Copyright (c) 2015 Deskalert. All rights reserved.
//

#ifndef Deskalert_Mobile_DeskAlertResponseDefines_h
#define Deskalert_Mobile_DeskAlertResponseDefines_h

#define responseDeskAlertRegistrationSuccess @"Registration was successfully completed."
#define responseDeskAlertLoginSuccess @"Thank you for signing up with existing account!"
#define responseDeskAlertLoginAlreadyExists @"Error! This user name already exists." //unused?
#define responseDeskAlertIncorrectLoginData @"Error! Username is taken, password provided is incorrect."
#define responseDeskAlertShortUserName @"Error! User name should be more than 1 symbol."
#define responseDeskAlertShortPassword @"Error! Password should be more than 1 symbol."
#define responseDeskAlertIncorrectSimbolInUsername @"Error! You can not registrate the Username with symbol: <>\\/%:;=,+*?>{}\""

#define responseDeskAlertServerConfiguredActiveDirectory @"<b>Your server configured to run in Active Directory mode. <br>This client doesn't support it or you have old version of client. <br>Please ask your administrator for appropriate desktop client.</b>"

#define responseDeskAlertDontHaveSynchronized @"Your server configured to run in Active Directory mode with mixed devices authentication but You don't have synchronized domains with DeskAlerts."

#define responseDeskAlertLoginOrPasswordIncorrect @"Error! Provided login/password pair is incorrect"
#define responseDeskAlertCredentialsNotFoundAD @"Error! Provided credentials are not found in AD domain synchronized with DeskAlerts"
#define responseDeskAlertProvidedPasswordIncorrect @"Error! Provided password is incorrect"
#define responseDeskAlertReasonDomainIsNull @"For some reason domain is null! Please contact technical support."

#endif
