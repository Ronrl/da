//
//  ViewController.m
//  Deskalert Mobile
//
//  Created by Дмитрий Кайгородов on 10.03.15.
//  Copyright (c) 2015 Deskalert. All rights reserved.
//

#import "DeskAlertServer.h"
#import "FirstWelcomeView.h"
#import "UserSettings.h"
#import "ErrorManager.h"
#import "Coordinator.h"
#import "ProcessingViewController.h"
#import "MBProgressHUD.h"

@interface FirstWelcomeView ()
@property MBProgressHUD *progressBar;
@end

@implementation FirstWelcomeView
- (IBAction)nextClick:(id)sender {    
    NSString* hostName = self.hostNameField.text;
    NSURL * url;
    if (![hostName hasPrefix:@"https://"] && ![hostName hasPrefix:@"http://"]) {
        url = [[NSURL alloc] initWithScheme:@"https" host:hostName path:@"/"];
    } else {
        url = [[NSURL alloc] initWithString:hostName];
    }
    
    [self.progressBar showAnimated:YES whileExecutingBlock:^{
        dispatch_sync(dispatch_get_main_queue(), ^{
            ValidationData validationData;
            NSURLComponents * components = [NSURLComponents componentsWithURL:url resolvingAgainstBaseURL:YES];
            validationData = [[DeskAlertServer defaultServer] validateServer: components.URL];
            if(!validationData.urlValid && [components.scheme isEqual: @"https"]) {
                components.scheme = @"http";
                validationData = [[DeskAlertServer defaultServer] validateServer: components.URL];
                if(!validationData.urlValid) {
                    [ErrorManager showAlertByError:validationData.error];
                    return;
                }
            }
            
            [UserSettings sharedInstance].serverURL = components.URL;
            [self performSegueWithIdentifier:@"1to2" sender:self];
        });
    }];
}


- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (IBAction)onHostNameChanged:(id)sender {
    UITextField *textField = (UITextField*)sender;
    self.nextButton.enabled = [textField.text length] > 0;
}

- (NSUInteger) supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.progressBar = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:self.progressBar];
    self.progressBar.delegate = self;
    self.progressBar.labelText = NSLocalizedString(@"Proc", nil);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
