function checkLang(current, langs)
{
	var currents = current.toLowerCase().replace(" ", "").split(",");
	var languages = langs.toLowerCase().replace(" ", "").split(",");
	for(var i=0; i<currents.length; i++)
	{
		for(var j=0; j<languages.length; j++)
		{
			if(currents[i].indexOf(languages[j])==0 ||
				languages[j].indexOf(currents[i])==0)
			{
				return true;
			}
		}
	}
	return false;
}
function replaceWithInput(elem, name)
{
	var inp = document.createElement("input");
	inp.type = "submit";
	if(elem.tagName.toLowerCase() == "img")
	{
		var func = elem.onclick||'';
		if(typeof(func) == 'function')
			inp.onclick = function() {
				func();
				return false;
			};
		else
			inp.onclick = func+";return false";
	}
	else
	{
		inp.onclick = elem.onclick;
	}
	inp.name = elem.name;
	inp.value = name;
	elem.parentNode.replaceChild(inp, elem);
}
function onContextMenuMain(e)
{
	if(!e) e = window.event;
	e.returnValue = false;
	e.cancelBubble = true;
	return false;
}
function getDocumentSize(doc)
{
	var size ={
		height: 0,
		width: 0
	};
	var body = doc.body;
	if (!doc.compatMode || doc.compatMode=="CSS1Compat")
	{
		var topMargin = parseInt(body.currentStyle.marginTop, 10) || 0;
		var bottomMargin = parseInt(body.currentStyle.marginBottom, 10) || 0;
		var leftMargin = parseInt(body.currentStyle.marginLeft, 10) || 0;
		var rightMargin = parseInt(body.currentStyle.marginRight, 10) || 0;

		size.width=Math.max(body.offsetWidth + leftMargin + rightMargin, doc.documentElement.clientWidth, doc.documentElement.scrollWidth);
		size.height=Math.max(body.offsetHeight + topMargin + bottomMargin, doc.documentElement.clientHeight, doc.documentElement.scrollHeight);
	}
	else
	{
		size.width = body.scrollWidth;
		size.height = body.scrollHeight;
	}
	
	return size;
}
function resizeByBody(docSize)
{	
	var isResizible = window.external.isResizible();
	if (isResizible) {
		
		var docWidth = docSize.width;
		var docHeight = docSize.height;
		var curBodyWidth = window.external.getBodyWidth();
		var curBodyHeight = window.external.getBodyHeight();
		if (curBodyWidth>docWidth) docWidth=curBodyWidth;
		if (curBodyHeight>docHeight) docHeight=curBodyHeight;
		window.external.setCaptionWidthByBodyWidth(docWidth);
		window.external.setCaptionHeightByBodyHeight(docHeight);
	}
}
var initialized = false;
function Initial(a)
{
	setTimeout(Initial_Timeout, 1);
}
function Initial_Timeout()
{
	if(initialized) return;
	else initialized = true;
	try
	{
		var bottom_template = window.external.getProperty('bottom_template', '');
		if(bottom_template)
		{
			document.body.innerHTML += bottom_template;
		}
		var html = document.body.innerHTML;
		var match = html.match(/<!-- *begin_lang[^>]*-->([\w\W]*?)<!-- *end_lang *-->/ig);
		if(match)
		{
			var locale = window.external.getProperty("customlocale", "");
			if(!locale) locale = window.external.getProperty("locale", "");
			var default_lang, use_dafault = true;
			for(var j=0; j<match.length; j++)
			{
				var lang = match[j].match(/<!-- *begin_lang[^>]*lang *= *(['"])([^"']*)\1[^>]*-->/i);
				if(lang && checkLang(locale, lang[2]))
				{
					html = match[j];
					use_dafault = false;
					var title = match[j].match(/<!-- *begin_lang[^>]*title *= *(['"])([^"']*)\1[^>]*-->/i);
					if(title && title[2])
						window.external.callInCaption('setTitle', title[2]);
					break;
				}
				var is_default = match[j].match(/<!-- *begin_lang[^>]*is_default *= *(['"])([^"']*)\1[^>]*-->/i);
				if(is_default && (is_default[2]!=0 || is_default[2].toLowerCase()=="true"))
					default_lang = match[j];
			}
			if(use_dafault && default_lang)
				html = default_lang;
			document.body.innerHTML = html
		}
		var root_path, close_button, save_button, submit_button;
		try {
			root_path = window.external.getProperty("root_path").replace(/\\/g, "/");
			close_button = window.external.getProperty("closeButton").replace(/\\/g, "/");
			save_button = window.external.getProperty("saveButton").replace(/\\/g, "/");
			submit_button = window.external.getProperty("submitButton").replace(/\\/g, "/");
		} catch(e) {
			root_path = null;
			close_button = null;
			save_button = null;
			submit_button = null;
		}
		if(close_button || save_button || submit_button)
		{
			var images = document.getElementsByTagName("img");
			for(var j = 0; j < 2; j++)
			{
				if(j==1) images = document.getElementsByTagName("input");
				for(var i = 0; i < images.length; i++)
				{
					if(images[i].tagName.toLowerCase() == "img" || images[i].getAttribute("type").toLowerCase() == "image")
					{
						var src = images[i].getAttribute("src");
						if(close_button && /.*admin\/images\/(langs\/\w+\/?|)close_button\.gif/i.test(src))
						{
							if(/^[\w\s]+$/.test(close_button))
								replaceWithInput(images[i], close_button);
							else
								images[i].src = "file:///" + close_button;
						}
						else if(save_button && /.*admin\/images\/(langs\/\w+\/?|)save\.gif/i.test(src))
						{
							if(/^[\w\s]+$/.test(save_button))
								replaceWithInput(images[i], save_button);
							else
								images[i].src = "file:///" + save_button;
						}
						else if(submit_button && /.*admin\/images\/(langs\/\w+\/?|)submit_button\.gif/i.test(src))
						{
							if(/^[\w\s]+$/.test(submit_button))
								replaceWithInput(images[i], submit_button);
							else
								images[i].src = "file:///" + submit_button;
						}
					}
				}
			}
		}
		document.body.scroll="auto";
		document.oncontextmenu = onContextMenuMain;

		var headTag = document.getElementsByTagName('head')[0]; 
		/*var base = document.createElement('base'); 
		base.setAttribute("href", "file:///" + root_path);
		headTag.appendChild(base);*/

		var imp = document.createElement('link');
		imp.setAttribute("type", "text/css");
		imp.setAttribute("rel", "stylesheet");
		imp.setAttribute("href", "file:///" + root_path + "/skin.css");
		headTag.appendChild(imp);

		document.body.className = "regcontent";

		// turn on autoresizing alert by body of document
		//var docSize = getDocumentSize(document);
		//resizeByBody(docSize);
	}
	catch(e)
	{
	}
}


