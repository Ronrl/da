//
//  Unobtrusive.h
//  Deskalerts Mobile
//
//  Created by Дмитрий Кайгородов on 04/04/16.
//  Copyright © 2016 Deskalert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Unobtrusive : NSObject

@property (nonatomic) int uId;
@property (nonatomic, strong) NSDate* from;
@property (nonatomic, strong) NSDate* to;
@property (nonatomic, strong) NSArray* repeat;
@property (nonatomic) BOOL isEnabled;

- (NSString*) asJsonString;
+ (Unobtrusive*) fromDictionary:(NSDictionary*) dict;
- (NSString*) daysAsString;
@end
