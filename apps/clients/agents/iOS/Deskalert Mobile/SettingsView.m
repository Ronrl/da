//
//  SettingsView.m
//  Deskalert Mobile
//
//  Created by Дмитрий Кайгородов on 11.03.15.
//  Copyright (c) 2015 Deskalert. All rights reserved.
//

#import "SettingsView.h"
#import "UserSettings.h"
#import "DeskAlertServer.h"
#import "ErrorManager.h"

@interface SettingsView ()

@end

@implementation SettingsView

- (void)viewDidLoad {
    [super viewDidLoad];

    self.hostNameField.text = [UserSettings sharedInstance].serverURL.absoluteString;
    self.loginField.text = [UserSettings sharedInstance].login;
    self.passWordField.text = [UserSettings sharedInstance].password;
    self.domainField.text = [UserSettings sharedInstance].domain;
    [self.soundSwitch setOn:[UserSettings sharedInstance].useSound];
   // NSLog(@"VIEW DID LOAD");
    // Do any additional setup after loading the view.
    
   // [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onUnregisterPushNotifications) name:@"unregistered" object:nil];
    self.progressBar = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:self.progressBar];
    self.progressBar.delegate = self;
    self.progressBar.labelText = NSLocalizedString(@"Proc", nil);
    if(self.isDemo)
    {
        [self.header removeFromSuperview];
    }
    
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:61.0f/255.0f green:162.0f/255.0f blue:66.0f/255.0f alpha:1.0f];
}

- (void) viewDidAppear:(BOOL)animated
{
    if(self.isDemo)
    {
        CGRect labelRect  = self.hosNameLabel.frame;
        CGRect hostFieldRect = self.hostNameField.frame;
        
        [self.hosNameLabel setFrame:CGRectMake(labelRect.origin.x, 15 , labelRect.size.width, labelRect.size.height)];
        [self.hostNameField setFrame: CGRectMake(hostFieldRect.origin.x, 15, hostFieldRect.size.width, hostFieldRect.size.height)];
    }
}


- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section > 0)
        return 48;
    
    if(indexPath.row == 0 && !self.isDemo)
    {
        return 91;
    }
    
    return 48;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 1 && indexPath.row == 0)
    {
        UIViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"unobtrusive"];
        //[self performSegueWithIdentifier:@"unobtrusive" sender:self];
        
        [self presentViewController:vc animated:YES completion:nil];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void) onUnregisterPushNotifications
{
    
}

- (IBAction)fieldChanged:(id)sender
{
    self.saveButton.enabled = [self canSave];
}

- (void) saveUserSettings {
    [UserSettings sharedInstance].serverURL = [[NSURL alloc] initWithString: self.hostNameField.text];
    [UserSettings sharedInstance].login = self.loginField.text;
    [UserSettings sharedInstance].password = self.passWordField.text;
    [UserSettings sharedInstance].domain = self.domainField.text;
    [UserSettings sharedInstance].useSound = self.soundSwitch.isOn;
    [UserSettings saveUserSettings];
}


- (NSUInteger) supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}


- (void) goToBackToMenu {
    [self.tabBarController.delegate tabBarController:self.tabBarController shouldSelectViewController:[[self.tabBarController viewControllers] objectAtIndex:0]];
    [self.tabBarController setSelectedIndex:0];
}

- (BOOL) loginDataChanged {
    return ![[UserSettings sharedInstance].serverURL.absoluteString isEqualToString: self.hostNameField.text ]|| ![[UserSettings sharedInstance].login  isEqualToString: self.loginField.text] || ![[UserSettings sharedInstance].password isEqualToString: self.passWordField.text] || ![[UserSettings sharedInstance].domain isEqualToString:self.domainField.text];
}


- (IBAction) saveSettings:(id)sender {
    if([self loginDataChanged]) {
        DeskAlertServer *server = [DeskAlertServer defaultServer];
        
        NSString* hostName = self.hostNameField.text;
        NSString* login = self.loginField.text;
        NSString* password = self.passWordField.text;
        NSString* domain = self.domainField.text;
    
        NSURL * url;
        if (![hostName hasPrefix:@"https://"] && ![hostName hasPrefix:@"http://"]) {
            url = [[NSURL alloc] initWithScheme:@"https" host:hostName path:@"/"];
        } else {
            url = [[NSURL alloc] initWithString:hostName];
        }
        
        ValidationData validationData;
        NSURLComponents * components = [NSURLComponents componentsWithURL:url resolvingAgainstBaseURL:YES];
        validationData = [[DeskAlertServer defaultServer] validateServer: components.URL];
        if(!validationData.urlValid && [components.scheme isEqual: @"https"]) {
            components.scheme = @"http";
            validationData = [[DeskAlertServer defaultServer] validateServer: components.URL];
            if(!validationData.urlValid) {
                [ErrorManager showAlertByError:validationData.error];
                return;
            }
        }
        url = components.URL;
        
        if(![server checkInternetConnection:YES])
            return;
        
        BOOL __block success;
        [self.progressBar showAnimated:YES whileExecutingBlock:^{
            dispatch_sync(dispatch_get_main_queue(), ^{
                success = [server validateOrRegisterUserWithLogin: login
                                                      andPassword: password
                                                      onServerURL: url
                                                        andDomain: domain];
                 [server unregisterDevice];
            });
            if(!success) {
                dispatch_sync(dispatch_get_main_queue(), ^{
                    [self saveUserSettings];
                    [server unregisterDevice];
                    [server registerDeviceWithToken:[UserSettings sharedInstance].device_token];
                    [self goToBackToMenu];
                });

            } else {
                dispatch_sync(dispatch_get_main_queue(), ^{
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"WrongPass", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"TryAgain" , nil) otherButtonTitles:nil];
                    [alert show];
                });
            }
        }];
        return;
    }
    [self saveUserSettings];
    [self goToBackToMenu];
}

- (BOOL) canSave {
    return [self.hostNameField.text length] > 0 && [self.loginField.text length] > 0 && [self.passWordField.text length] > 0;
}

- (IBAction)asd:(UITextField *)sender {
}
@end
