//
//  UnobtrusiveTableViewCell.m
//  Deskalerts Mobile
//
//  Created by Дмитрий Кайгородов on 06/04/16.
//  Copyright © 2016 Deskalert. All rights reserved.
//

#import "UnobtrusiveTableViewCell.h"

@implementation UnobtrusiveTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)onSwitchChanged:(id)sender {
    
    NSNumber* _id = [NSNumber numberWithInt:self.uId];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UnobtrusiveSwitch" object:_id userInfo:nil];
}

@end
