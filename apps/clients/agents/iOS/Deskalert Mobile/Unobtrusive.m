//
//  Unobtrusive.m
//  Deskalerts Mobile
//
//  Created by Дмитрий Кайгородов on 04/04/16.
//  Copyright © 2016 Deskalert. All rights reserved.
//

#import "Unobtrusive.h"

@implementation Unobtrusive


- (NSString*) asJsonString
{
        NSDateFormatter* formatter = [NSDateFormatter new];
    
        [formatter setDateFormat:@"HH:mm"];
    
        NSString* fromString = [formatter stringFromDate:self.from];
        NSString* toString = [formatter stringFromDate:self.to];
    
        NSString* repeatDays = [self daysAsString];
    
        NSDictionary* dict = @{@"from": fromString, @"to" : toString, @"days": repeatDays};

        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    
        NSString * jsonString;
        if (! jsonData) {
            NSLog(@"Got an error: %@", error);
        } else {
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
    
    return jsonString;
    
}

- (NSString*) daysAsString
{
    NSArray* days = [NSArray arrayWithObjects:@"mon", @"tue", @"wed", @"thu", @"fri", @"sat", @"sun",nil];
    
    NSMutableArray* repeatDays = [NSMutableArray new];
    for(NSNumber* day in self.repeat)
    {
        int dayNumber = [day intValue];
        
        NSString* dayString = [days objectAtIndex:dayNumber-1];
        
        [repeatDays addObject:dayString];
        
        
    }
    
    return [repeatDays componentsJoinedByString:@","];
}

- (void) daysFromString:(NSString*) daysStr
{
     NSArray* days = [NSArray arrayWithObjects:@"mon", @"tue", @"wed", @"thu", @"fri", @"sat", @"sun",nil];
    
    NSArray* stringArray = [daysStr componentsSeparatedByString:@","];
    
    NSMutableArray* indexArray = [NSMutableArray new];
    
    for(NSString* day in stringArray)
    {
        if(!day || [day length] == 0)
            continue;
        
        int index = [days indexOfObject:day] + 1;
        
        [indexArray addObject:[NSNumber numberWithInt:index]];
    }
    
    self.repeat = indexArray;
    
}

+ (Unobtrusive*) fromDictionary:(NSDictionary *)dict
{
    Unobtrusive* u = [Unobtrusive new];
    
    NSDateFormatter* formatter = [NSDateFormatter new];
    
    [formatter setDateFormat:@"HH:mm"];
    
    u.from = [formatter dateFromString:[dict valueForKey:@"from"]];
    u.to  = [formatter dateFromString:[dict valueForKey:@"to"]];
    u.uId = [[dict valueForKey:@"id"] intValue];
    u.isEnabled = [[dict valueForKey:@"enabled"] intValue] == 1;
    
    [u daysFromString:[dict valueForKey:@"days"]];
    
    return u;
    
}
@end
