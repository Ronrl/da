//
//  UnobtrusiveService.m
//  Deskalerts Mobile
//
//  Created by Дмитрий Кайгородов on 05/04/16.
//  Copyright © 2016 Deskalert. All rights reserved.
//

#import "UnobtrusiveService.h"
#import "Unobtrusive.h"

@implementation UnobtrusiveService


- (id) init
{
    self = [super init];
    
    if(self)
    {
        
        
        NSArray* coll = [[UnobtrusiveNetworkController sharedInstance] arrayOfUnobtrusives] ;
        
        self.collection = [NSMutableArray new];
        for(NSDictionary* u in coll)
        {
            [self.collection addObject:[Unobtrusive fromDictionary:u]];
            
        }
    }
    
    return self;
}

- (BOOL) addUnobtrusive:(Unobtrusive *)u
{
    UnobtrusiveNetworkController* unc = [UnobtrusiveNetworkController sharedInstance];
    int newId = [unc addUnobtrusive:u];
    if(newId > 0)
    {
        
        u.uId = newId;
        [self.collection addObject:u];
        return YES;
        
    }
    
    return NO;
}

- (BOOL) deleteUnobtrusive:(Unobtrusive *)u
{
    UnobtrusiveNetworkController* unc = [UnobtrusiveNetworkController sharedInstance];
    
    if([unc deleteUnobtrusive:u])
    {
        [self.collection removeObject:u];
        return YES;
        
    }
    
    return NO;
}

- (void) switchUnobtrusive:(Unobtrusive*) u
{
    UnobtrusiveNetworkController* unc = [UnobtrusiveNetworkController sharedInstance];
    
    [unc switchUnobtrusive:u];
    
}

- (NSArray*) unobtrusives
{
    return self.collection;
}

- (Unobtrusive*) unobtrusiveById:(int)uId
{
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"uId = %d", uId];
    
    NSArray* arrayResult = [self.unobtrusives filteredArrayUsingPredicate:predicate];
    
    return [arrayResult objectAtIndex:0];
}

@end
