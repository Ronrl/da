//
//  UnobtrusiveListViewController.h
//  Deskalerts Mobile
//
//  Created by Дмитрий Кайгородов on 04/04/16.
//  Copyright © 2016 Deskalert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UnobtrusiveTableViewCell.h"
#import "Unobtrusive.h"
#import "UnobtrusiveService.h"
#import "UnobtrusiveDetailsViewController.h"

@interface UnobtrusiveListViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property  NSMutableArray* unobtrusives;
@property UnobtrusiveService* unobtrusiveService;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *editBtn;

@property BOOL isEditMode;
@end
