//
//  ContactUsViewController.h
//  Deskalerts Mobile
//
//  Created by Dmitrii on 30/12/15.
//  Copyright © 2015 Deskalert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface ContactUsViewController : UIViewController<MFMailComposeViewControllerDelegate>

@end
