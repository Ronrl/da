//
//  UnobrrusiveDetailsViewController.m
//  Deskalerts Mobile
//
//  Created by Дмитрий Кайгородов on 04/04/16.
//  Copyright © 2016 Deskalert. All rights reserved.
//

#import "UnobtrusiveDetailsViewController.h"

@interface UnobtrusiveDetailsViewController ()

@end

@implementation UnobtrusiveDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    if(!self.objectToAdd)
    {
        self.objectToAdd = [Unobtrusive new];
        self.isEditMode = NO;
    }
    else
    {
        self.fromDate.date = self.objectToAdd.from;
        self.toDate.date = self.objectToAdd.to;
        self.isEditMode = YES;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)unwind:(UIStoryboardSegue*)sender
{
    
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 3;
}
- (IBAction)onSaveClick:(id)sender {
    
    self.objectToAdd.from = self.fromDate.date;
    self.objectToAdd.to = self.toDate.date;
    //self.objectToAdd.isEnabled = YES;
    
    if(!self.isEditMode)
    {
        self.objectToAdd.isEnabled = YES;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"UnobtrusiveAdd" object:self.objectToAdd];
    }
    else
    {
        [[UnobtrusiveNetworkController sharedInstance] updateUnobtrusive:self.objectToAdd];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"UnobtrusiveUpdate" object:self.objectToAdd];
    }
    
    [self performSegueWithIdentifier:@"unwindSegue" sender:self];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return 1;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger section = indexPath.section;
    NSInteger row = indexPath.row;
    
    if(section == 2 && row == 0)
    {
        [self performSegueWithIdentifier:@"weekday" sender:self];
    }
    
    
}
- (IBAction)toDetails:(UIStoryboardSegue*)sender
{
  //  UnobtrusiveDetailsViewController* detailsVC = (UnobtrusiveDetailsViewController*)sender.destinationViewController;
    
    //self.objectToAdd.repeat = self.days;
   // detailsVC.objectToAdd = self.objectToAdd;
    WeekDaySelectionTableViewController* weekdayVc = (WeekDaySelectionTableViewController*)sender.sourceViewController;
    self.objectToAdd.repeat = weekdayVc.days;
    
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    if([segue.identifier isEqualToString:@"weekday"])
    {
    WeekDaySelectionTableViewController* vc = (WeekDaySelectionTableViewController*)segue.destinationViewController;
    
    vc.objectToAdd = self.objectToAdd;
    }
}
/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
