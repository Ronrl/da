//
//  UnobtrusiveListViewController.m
//  Deskalerts Mobile
//
//  Created by Дмитрий Кайгородов on 04/04/16.
//  Copyright © 2016 Deskalert. All rights reserved.
//

#import "UnobtrusiveListViewController.h"

@interface UnobtrusiveListViewController ()

@end

@implementation UnobtrusiveListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    self.unobtrusiveService = [[UnobtrusiveService alloc] init];
    self.unobtrusives = [[self.unobtrusiveService unobtrusives] mutableCopy];
    self.isEditMode = NO;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"UnobtrusiveAdd" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(UnobtrusiveAdd:) name:@"UnobtrusiveAdd" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"UnobtrusiveSwitch" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(UnobtrusiveSwitch:) name:@"UnobtrusiveSwitch" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"UnobtrusiveDelete" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(UnobtrusiveDelete:) name:@"UnobtrusiveDelete" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"UnobtrusiveUpdate" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(UnobtrusiveUpdate:) name:@"UnobtrusiveUpdate" object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) UnobtrusiveUpdate:(NSNotification*) notification
{
    self.isEditMode = NO;
    self.editBtn.title = @"Edit";
    [self.tableView reloadData];
}
- (void) UnobtrusiveAdd:(NSNotification*) notification
{
    if([self.unobtrusiveService addUnobtrusive:notification.object])
    {
        [self.unobtrusives addObject:notification.object];
        [self.tableView reloadData];
    }
}
- (IBAction)onCancelClick:(id)sender {
    
    UIViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"tabView"];
    [[NSUserDefaults standardUserDefaults] setObject:@3 forKey:@"tab"];
    [self presentViewController:vc animated:YES completion:nil];
}

- (void) UnobtrusiveSwitch: (NSNotification*) notification
{
    
    int uId = [notification.object intValue];
    Unobtrusive* u = [self.unobtrusiveService unobtrusiveById:uId];
    [self.unobtrusiveService switchUnobtrusive:u];
}

- (void) UnobtrusiveDelete: (NSNotification*) notification
{
    int uId = [notification.object intValue];
    Unobtrusive* u = [self.unobtrusiveService unobtrusiveById:uId];
    if([self.unobtrusiveService deleteUnobtrusive:u])
    {
        [self.unobtrusives removeObject:notification.object];
        [self.tableView reloadData];
    }
}



- (IBAction)toList:(UIStoryboardSegue*)sender
{
    
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}
- (IBAction)addButtonClick:(id)sender {
    
    [self performSegueWithIdentifier:@"add" sender:self];
    
}

- (IBAction)onEditClick:(id)sender {
    
    self.isEditMode = !self.isEditMode;
    
    if(self.isEditMode)
    {
        self.editBtn.title = @"Done";
      //  self.editButtonItem.title = @"Done";
                        
    }
    else
    {
        self.editBtn.title = @"Edit";
       // self.editButtonItem.title = @"Edit";
    }
    [self.tableView reloadData];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return [self.unobtrusives count];
}

- (NSArray*) tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    UITableViewRowAction *button2 = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:NSLocalizedString(@"Delete", nil) handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                     {
                                      //   Alert* alertToDelete = [self.fetchedResultsController objectAtIndexPath:indexPath];
                                       //  [self.managedObjectContext deleteObject:alertToDelete];
                                      //   NSError *error = nil;
                                       //  [self.managedObjectContext save:&error];
                                         
                                         Unobtrusive* u = [self.unobtrusives objectAtIndex:indexPath.row];
                                         if([self.unobtrusiveService deleteUnobtrusive:u])
                                         {
                                             [self.unobtrusives removeObjectAtIndex:indexPath.row];
                                             [self.tableView reloadData];
                                         }

                                     }];
    //   button2.backgroundColor = [UIColor blueColor]; //arbitrary color
    button2.backgroundColor = [UIColor redColor];//[UIColor colorWithRed:77/255.0 green:174/255.0 blue:74/255.0 alpha:1.0];
    
    return @[button2];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString* identifier = self.isEditMode ? @"unobtrusiveEdit" : @"unobtrusive";
    UnobtrusiveTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier  forIndexPath:indexPath];
    
    NSDateFormatter* fmt = [NSDateFormatter new];
    [fmt setDateFormat:@"HH:mm"];
    
    Unobtrusive* current = [self.unobtrusives objectAtIndex:indexPath.row];
    cell.from.text = [fmt stringFromDate:current.from];
    cell.to.text = [fmt stringFromDate:current.to];
    cell.repeat.text = [current daysAsString];
    cell.uId = current.uId;
    
    [cell.enabled setOn:current.isEnabled];
    

    
    
    // Configure the cell...
    
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.isEditMode)
    {
//        Unobtrusive* unobtrusiveToDelete = [self.unobtrusives objectAtIndex:indexPath.row];
//        
//        if([self.unobtrusiveService deleteUnobtrusive:unobtrusiveToDelete])
//        {
//            
//        }
        Unobtrusive* editableObject = [self.unobtrusives objectAtIndex:indexPath.row];
        
         [self performSegueWithIdentifier:@"add" sender:editableObject];
        
    }
}


- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"add"] && self.isEditMode)
    {
        UnobtrusiveDetailsViewController* details = segue.destinationViewController;
        details.objectToAdd = sender;
    }
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
