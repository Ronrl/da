//
//  SettingsView.h
//  Deskalert Mobile
//
//  Created by Дмитрий Кайгородов on 11.03.15.
//  Copyright (c) 2015 Deskalert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface SettingsView : UITableViewController<MBProgressHUDDelegate, UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITextField *hostNameField;
@property (weak, nonatomic) IBOutlet UITextField *loginField;
@property (weak, nonatomic) IBOutlet UITextField *passWordField;
@property (weak, nonatomic) IBOutlet UITextField *domainField;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *saveButton;
@property (weak, nonatomic) IBOutlet UISwitch *soundSwitch;
@property MBProgressHUD *progressBar;
@property (weak, nonatomic) IBOutlet UINavigationBar *header;
@property (weak, nonatomic) IBOutlet UILabel *hosNameLabel;

@property BOOL isDemo;
- (BOOL) canSave;
@end
