//
//  AlertDetailsView.m
//  Deskalert Mobile
//
//  Created by Дмитрий Кайгородов on 23.03.15.
//  Copyright (c) 2015 Deskalert. All rights reserved.
//

#import "AlertDetailsView.h"
#import "DatabaseManager.h"

@interface AlertDetailsView (){
    NSUserDefaults *defaults;
}

@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;

@property (nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
- (IBAction)okButtonPressed:(UIButton *)sender;
- (IBAction)printButton:(UIButton *)sender;
- (IBAction)postpone:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *toolButtonsPanelHeightConstraint;
@property (weak, nonatomic) IBOutlet UIView *toolButtonsPanelView;
@end

@implementation AlertDetailsView

- (void)generateAlertHtml {
    [self.alertDisplayView.scrollView setShowsVerticalScrollIndicator:YES];
    [self.alertDisplayView setDelegate:self];
    //Add links for dates
    
    NSString* htmlBody = self.alert.message;
    NSError* err;
    NSRegularExpression* dateRegex = [[NSRegularExpression alloc] initWithPattern:@"[^\\d](\\d{4}|\\d{2})(\\.|\\-|\\/)\\d{2}(\\.|\\-|\\/)(\\d{4}|\\d{2})( \\d{2}\\:\\d{2}(\\:\\d{2})?)?" options:NSRegularExpressionCaseInsensitive error:&err];
    
    NSString* searchString = htmlBody;

    NSRegularExpression* phoneRegex = [[NSRegularExpression alloc] initWithPattern:@"(?:(?:\\+?\\d{1,3}\\s*(?:[.-]\\s*)?)?(?:(\\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]‌‌​)\\s*)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\\s*(?:[.-]\\s*)?)([2-9]1[02-9]‌‌​|[2-9][02-9]1|[2-9][02-9]{2})\\s*(?:[.-]\\s*)?([0-9]{4})\\s*(?:\\s*(?:#|x\\.?|ext\\.?|extension)\\s*(\\d+)\\s*)?" options:NSRegularExpressionCaseInsensitive error:&err];
    searchString = htmlBody;
    
    
    NSArray* phones = [phoneRegex matchesInString:searchString options:0 range:NSMakeRange(0, [searchString length])];
    
    if(phones && [phones count] > 0)
    {
        NSString* newHTML = @"";
        int index = 0;
        for(NSTextCheckingResult* result in phones)
        {
            
            NSString* phone = [[searchString substringWithRange:result.range] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] ;
            
            NSString* phoneLink = [NSString stringWithFormat:@"<a href='tel:\\\\%@'>%@</a>", phone, phone];
            NSString* subString = [searchString substringWithRange:NSMakeRange(index, result.range.location + result.range.length  - index)];
            index += (result.range.location + result.range.length - index);
            subString = [subString stringByReplacingOccurrencesOfString:phone withString:phoneLink];
            
            newHTML = [newHTML stringByAppendingString:subString];
            
        }
        
        htmlBody = newHTML;
        
    }
    
    
    htmlBody = [htmlBody stringByReplacingOccurrencesOfString:@"<img" withString:@"<a href='desk://closealert'><img"];
    htmlBody = [htmlBody stringByReplacingOccurrencesOfString:@"Close();'/>" withString:@"Close();'></a>"];
    htmlBody = [htmlBody stringByReplacingOccurrencesOfString:@"src='" withString:[NSString stringWithFormat:@"src='%@", [UserSettings sharedInstance].serverURL.absoluteString]];
    
    htmlBody = [htmlBody stringByReplacingOccurrencesOfString:@"<img" withString:@"<a href='desk://closealert'><img"];
    
    
    
    
    NSLog(@"SKINPATH = %@", self.alert.skinPath);
    
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
    
    NSString* creationDateString = [NSString stringWithFormat:@"Creation date: <br>%@", [formatter stringFromDate:self.alert.recivedDate]];
    
    NSString *title = [self.alert.htmlTitle length] == 0 ? self.alert.title : self.alert.htmlTitle;
    _alertTitle = title;
    
    NSString *isHtmlMark = [self.alert.htmlTitle length] == 0 ? @"false" : @"true";
    
    
    CGSize screenRect =[[UIScreen mainScreen] bounds].size;
    int width = self.view.bounds.size.width * 0.8;
    int height = screenRect.height * 0.43;

    CGFloat contentBottomPoint = [self.alertDisplayView convertPoint:CGPointMake(11, 120 + height) toView:[UIApplication sharedApplication].keyWindow].y;
    
    if( fabs(contentBottomPoint - screenRect.height) < 30 ) {
        height -= 100;
    }
    
    if(![self.alert.skinPath hasPrefix:@"http"] && ![self.alert.skinPath hasPrefix:@"https"]) {
        self.alert.skinPath = [NSString stringWithFormat:@"%@admin/%@", [UserSettings sharedInstance].serverURL.absoluteString,  self.alert.skinPath];
    }

    if ([htmlBody containsString:@"printable_alert"]) {
        _print = 1;
    }
                            
    self.alertHtml = [NSString stringWithFormat:kAlertTemplate, self.alert.skinPath, 138, 7, 0, 6, width, height, htmlBody, creationDateString, title, isHtmlMark];
    
    NSLog(@"%@", self.alertHtml);
    self.alertDisplayView.scrollView.scrollEnabled = YES;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self generateAlertHtml];
    
    if (self.alert.isAcknown.boolValue == YES && _print == 1) {
        [self.toolButtonsPanelView setHidden: NO];
        [self.toolButtonsPanelHeightConstraint setConstant:90];
    } else if(self.alert.isAcknown.boolValue == NO && _print == 1) {
        [self.toolButtonsPanelView setHidden: NO];
        [self.toolButtonsPanelHeightConstraint setConstant:40];
    } else if(self.alert.isAcknown.boolValue == YES && _print == 0) {
        [self.toolButtonsPanelView setHidden: NO];
        [self.toolButtonsPanelHeightConstraint setConstant:90];
    } else {
        [self.toolButtonsPanelView setHidden: YES];
        [self.toolButtonsPanelHeightConstraint setConstant:0];
    }

    [self.okButton setHidden:!self.alert.isAcknown.boolValue];
    [self.postpone setHidden:!self.alert.isAcknown.boolValue];
    [self.pickerView setHidden:!self.alert.isAcknown.boolValue];
    [self.printButton setHidden:!_print];
    
    NSString *baseURL = @""; // [UserSettings sharedInstance].hostName;
    [self.alertDisplayView loadHTMLString:self.alertHtml baseURL:[NSURL URLWithString:baseURL]];
    
    
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id <UIViewControllerTransitionCoordinator>)coordinator
{
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    
    
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        
    } completion:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        
        [self.alertDisplayView reload];
        [self generateAlertHtml];
        
        if (self.alert.isAcknown.boolValue == YES && _print == 1) {
            [self.toolButtonsPanelView setHidden: NO];
            [self.toolButtonsPanelHeightConstraint setConstant:90];
        } else if(self.alert.isAcknown.boolValue == NO && _print == 1) {
            [self.toolButtonsPanelView setHidden: NO];
            [self.toolButtonsPanelHeightConstraint setConstant:40];
        } else if(self.alert.isAcknown.boolValue == YES && _print == 0) {
            [self.toolButtonsPanelView setHidden: NO];
            [self.toolButtonsPanelHeightConstraint setConstant:90];
        } else {
            [self.toolButtonsPanelView setHidden: YES];
            [self.toolButtonsPanelHeightConstraint setConstant:0];
        }
        
        [self.okButton setHidden:self.alert.isAcknown.boolValue];
        [self.postpone setHidden:self.alert.isAcknown.boolValue];
        [self.pickerView setHidden:self.alert.isAcknown.boolValue];
        [self.printButton setHidden:_print];
        
        NSString *baseURL = @""; // [UserSettings sharedInstance].hostName;
        [self.alertDisplayView loadHTMLString:self.alertHtml baseURL:[NSURL URLWithString:baseURL]];
    }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}




- (void) viewWillAppear:(BOOL)animated{    
    
}

- (void)okButtonPressed:(UIButton *)sender
{
    [self.activityIndicator setHidden:NO];
    [UIApplication.sharedApplication beginIgnoringInteractionEvents];
    
    NSURL *requestURL = [[UserSettings sharedInstance].serverURL URLByAppendingPathComponent:@"alert_read.asp"];
    
    NSMutableArray *items = [[NSMutableArray alloc] init];
    NSNumber *alertId = self.alert.alertId;
    NSString *alertIdString = [alertId stringValue];
    
    [items addObject:[NSURLQueryItem queryItemWithName:@"alert_id" value:alertIdString]];
    [items addObject:[NSURLQueryItem queryItemWithName: @"uname" value: self.alert.username]];
 
    NSURLComponents *tmp = [[NSURLComponents alloc] init];
    [tmp setQueryItems:items];
    
    NSString *queryString = [[tmp URL] query];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL: requestURL];
    
    [request setHTTPMethod:@"POST"];
    
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[queryString length]] forHTTPHeaderField:@"Content-length"];
    
    [request setHTTPBody:[queryString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse * _Nullable response, NSData * _Nullable data, NSError * _Nullable connectionError) {
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        if (httpResponse.statusCode == 200) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [UIApplication.sharedApplication endIgnoringInteractionEvents];
                [self.activityIndicator setHidden:YES];
                [self dismissViewControllerAnimated:YES completion:nil];
            });
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.activityIndicator setHidden:YES];
                [UIApplication.sharedApplication endIgnoringInteractionEvents];
                
                // Show alert with error if there is one
                UIAlertController * alert = [UIAlertController
                                             alertControllerWithTitle:@"Error"
                                             message:[connectionError localizedDescription]
                                             preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *okayButton = [UIAlertAction
                                             actionWithTitle:@"OK"
                                             style:UIAlertActionStyleDefault
                                             handler:nil];
                [alert addAction:okayButton];
                [self presentViewController:alert animated:YES completion:nil];
            });
        }
        
    }];
}
- (BOOL) webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    
        if([[[request URL] absoluteString] hasPrefix:@"desk:"])
    {
        [self dismissViewControllerAnimated:YES completion:nil];
        return YES;
    }
    
    else if([[[request URL] absoluteString] hasPrefix:@"applewebdata:"])
    {
        NSString* fullUrl = [[request URL] absoluteString];
        NSString* validURL = [NSString stringWithFormat:@"http://%@", [fullUrl componentsSeparatedByString:@"/"][2]];
        NSLog(@"%@", validURL);
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:validURL]];

        return NO;

    }
    else if([[[request URL] absoluteString] hasPrefix:@"http:"] && navigationType == UIWebViewNavigationTypeLinkClicked)
    {
        NSString* url = [[[request URL] absoluteString] stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
        return NO;
    }
    else if([[[request URL] absoluteString] hasPrefix:@"date:"])
    {
        NSString* date = [[[request URL] absoluteString] substringFromIndex:6] ;
        
        EKEventStore *eventStore = [[EKEventStore alloc] init];
        
        EKAuthorizationStatus eventAuthStatus = [EKEventStore authorizationStatusForEntityType:EKEntityTypeEvent];
        
        if(eventAuthStatus == EKAuthorizationStatusNotDetermined)
        {
            [eventStore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError * _Nullable error) {
                
                if(granted)
                {
                    [self presentEventViewControllerForDate:date inStore:eventStore];
                }
                else
                {
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Error!" message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                }
            }];
        }
        else if(eventAuthStatus == EKAuthorizationStatusAuthorized)
        {
            [self presentEventViewControllerForDate:date inStore:eventStore];
        }
        else
        {
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Error!" message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
    else if([[[request URL] absoluteString] hasPrefix:@"tel:"])
    {
        NSString* phone = [[[[request URL] absoluteString] stringByReplacingOccurrencesOfString:@"tel:/" withString:@"tel//:"] stringByReplacingOccurrencesOfString:@"+" withString:@""];
        NSLog(@"%@", phone);
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phone]];
    }
    
    if([[[request URL] absoluteString] containsString:@"SurveyAnswersStatistic.aspx"] && navigationType == UIWebViewNavigationTypeOther)
    {
        NSData* data = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];

        NSString *responseString = [[NSString alloc] initWithBytes:data.bytes length:data.length encoding:NSUTF8StringEncoding];
        

        int width = [[UIScreen mainScreen] bounds].size.width*0.9;
        int height = [[UIScreen mainScreen] bounds].size.height*0.47;

        if(![self.alert.skinPath containsString:@"default"])
        {
            height = [[UIScreen mainScreen] bounds].size.height*0.445;
        }

        NSString *newDiv = [NSString stringWithFormat:@"<html xmlns=\"http://www.w3.org/1999/xhtml\"><div style='width:%dpx; height:%dpx; overflow:scroll; text-align:center;'>",width, height];

        responseString = [responseString stringByReplacingOccurrencesOfString:@"<html xmlns=\"http://www.w3.org/1999/xhtml\">" withString:newDiv];
        
        responseString = [responseString stringByReplacingOccurrencesOfString:@"height: 20px; width: 203px;'>&nbsp;</div></td>" withString:@"height: 20px; width: 100px;'>&nbsp;</div></td>"];
        responseString = [responseString stringByReplacingOccurrencesOfString:@"</html>" withString:@"</div></html>"];

        responseString = [responseString stringByReplacingOccurrencesOfString:@"<img" withString:@"<a href='desk://closealert'><img"];
        responseString = [responseString stringByReplacingOccurrencesOfString:@"Close();'/>" withString:@"Close();'></a>"];
        
        responseString = [responseString stringByReplacingOccurrencesOfString:@"src='" withString:[NSString stringWithFormat:@"src='%@", [UserSettings sharedInstance].serverURL.absoluteString]];
        responseString = [responseString stringByReplacingOccurrencesOfString:@"\"" withString:@"'"];
        responseString = [responseString stringByReplacingOccurrencesOfString:@"\r" withString:@""];
        responseString = [responseString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        
        
        if ( [UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)
        {
            NSString* wrapScript = @"var wrapper = document.createElement('div'); wrapper.style.textAlign = 'center'; var selected_frame = document.getElementById('alertContentFrame'); var frame_clone = selected_frame.cloneNode(true); selected_frame.parentNode.insertBefore(wrapper, selected_frame); selected_frame.remove(); wrapper.appendChild(frame_clone);";
            [webView stringByEvaluatingJavaScriptFromString:wrapScript];
        }

        NSString* js = [NSString stringWithFormat:@"var doc = document.getElementById('alertContentFrame').contentWindow.document; doc.open(); doc.write(\"%@\"); doc.close();", responseString];
        [webView stringByEvaluatingJavaScriptFromString:js];
        
        return NO;
    }
    
    if(navigationType == UIWebViewNavigationTypeFormSubmitted)
    {
        NSData* data = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];

        NSString *responseString = [[NSString alloc] initWithBytes:data.bytes length:data.length encoding:NSUTF8StringEncoding];
        responseString = [responseString stringByReplacingOccurrencesOfString:@"<img" withString:@"<a href='desk://closealert'><img"];
        responseString = [responseString stringByReplacingOccurrencesOfString:@"Close();'/>" withString:@"Close();'></a>"];
        responseString = [responseString stringByReplacingOccurrencesOfString:@"src='" withString:[NSString stringWithFormat:@"src='%@", [UserSettings sharedInstance].serverURL.absoluteString]];
        responseString = [responseString stringByReplacingOccurrencesOfString:@"\"" withString:@"'"];
        responseString = [responseString stringByReplacingOccurrencesOfString:@"\r" withString:@""];
        responseString = [responseString stringByReplacingOccurrencesOfString:@"\n" withString:@""];

        int width = [[UIScreen mainScreen] bounds].size.width*0.9;
        int height = [[UIScreen mainScreen] bounds].size.height*0.47;

        if(![self.alert.skinPath containsString:@"default"])
        {
            height = [[UIScreen mainScreen] bounds].size.height*0.445;
        }

        NSString *newDiv = [NSString stringWithFormat:@"<html><div style='width:%dpx; height:%dpx; overflow:scroll; text-align:center;'>",width, height];

        responseString = [[responseString stringByReplacingOccurrencesOfString:@"<html>" withString:newDiv] stringByReplacingOccurrencesOfString:@"</html>" withString:@"</div></html>"];

        if ( [UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)
        {
            NSString* wrapScript = @"var wrapper = document.createElement('div'); wrapper.style.textAlign = 'center'; var selected_frame = document.getElementById('alertContentFrame'); var frame_clone = selected_frame.cloneNode(true); selected_frame.parentNode.insertBefore(wrapper, selected_frame); selected_frame.remove(); wrapper.appendChild(frame_clone);";
            [webView stringByEvaluatingJavaScriptFromString:wrapScript];
        }

        NSString* js = [NSString stringWithFormat:@"var doc = document.getElementById('alertContentFrame').contentWindow.document; doc.open(); doc.write(\"%@\"); doc.close();", responseString];
        [webView stringByEvaluatingJavaScriptFromString:js];

        return NO;
    }
    if ( navigationType == UIWebViewNavigationTypeLinkClicked )
    {
        [[UIApplication sharedApplication] openURL:[request URL]];
        return NO;
    }
    
    return YES;
}

- (void) presentEventViewControllerForDate:(NSString*) date inStore: (EKEventStore*) eventStore
{
    date = [date stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
    EKEvent* event = [EKEvent eventWithEventStore:eventStore];
    event.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    event.startDate = [self dateFromString:date];
    
    EKEventEditViewController* eventViewController = [[EKEventEditViewController alloc] init];
    eventViewController.event = event;
    eventViewController.eventStore = eventStore;
    eventViewController.editViewDelegate = self;
    
    [self presentViewController:eventViewController animated:YES completion:nil];
}

- (void) eventEditViewController:(EKEventEditViewController *)controller didCompleteWithAction:(EKEventEditViewAction)action
{
    [controller dismissViewControllerAnimated:YES completion:nil];
}

- (void)printButton:(UIButton *)sender {
    
    UIGraphicsBeginImageContextWithOptions(self.alertDisplayView.bounds.size, YES, 0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [self.alertDisplayView.layer renderInContext:context];
    UIImage *imageFromCurrentView = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIPrintInteractionController *printController = [UIPrintInteractionController sharedPrintController];
    printController.printingItem = imageFromCurrentView;
    UIPrintInfo *printInfo = [UIPrintInfo printInfo];
    printInfo.outputType = UIPrintInfoOutputGrayscale;
    printController.printInfo = printInfo;
    printController.showsPageRange = YES;
    
    
    void (^completionHandler)(UIPrintInteractionController *, BOOL, NSError *) = ^(UIPrintInteractionController *printController, BOOL completed, NSError *error) {
        if (!completed && error) {
            NSLog(@"FAILED! due to error in domain %@ with error code %ld", error.domain, (long)error.code);
        }
    };
    
    [printController presentAnimated:YES completionHandler:completionHandler];
}

- (IBAction)postpone:(UIButton *)sender {
    
}

- (IBAction)backClick:(id)sender
{
    [DatabaseManager alertProcessingAfterClose: _alert];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (NSUInteger) supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    UIViewController* vc = segue.destinationViewController;
    NSNumber* tabNum = [[NSUserDefaults standardUserDefaults] valueForKey:@"tab"];
    [vc.tabBarController setSelectedIndex: [tabNum integerValue]];
    // [[NSNotificationCenter defaultCenter] postNotificationName:@"changeTab" object:[NSNumber numberWithInt:self.returnTab]];
}

#define kDateFormatRegex @"regex"
#define kDateFormatString @"format"

- (NSDate *)dateFromString:(NSString *)string {
    static NSArray *formatStrings = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatStrings = @[@{kDateFormatRegex: @"^\\d{4}-\\d{2}-\\d{2}",    kDateFormatString: @"yyyy-MM-dd"},
                          @{kDateFormatRegex: @"^\\d{2}-\\d{2}-\\d{4}",    kDateFormatString: @"dd-MM-yyyy"},
                          @{kDateFormatRegex: @"^\\d{4}.\\d{2}.\\d{2}",    kDateFormatString: @"yyyy.MM.dd"},
                          @{kDateFormatRegex: @"^\\d{2}.\\d{2}.\\d{4}",    kDateFormatString: @"dd.MM.yyyy"},
                          @{kDateFormatRegex: @"^\\d{4}\\\\d{2}:\\\\d{2}",    kDateFormatString: @"yyyy\\MM\\dd"},
                          @{kDateFormatRegex: @"^\\d{2}\\\\d{2}\\\\d{4}",    kDateFormatString: @"dd\\MM\\yyyy"},
                          @{kDateFormatRegex: @"^\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}",     kDateFormatString: @"yyyy-MM-dd HH:mm:ss"},
                          @{kDateFormatRegex: @"^\\d{2}-\\d{2}-\\d{4} \\d{2}:\\d{2}:\\d{2}",    kDateFormatString: @"dd-MM-yyyy HH:mm:ss"},
                          @{kDateFormatRegex: @"^\\d{4}.\\d{2}.\\d{2} \\d{2}:\\d{2}:\\d{2}", kDateFormatString: @"yyyy.MM.dd HH:mm:ss"},
                          @{kDateFormatRegex: @"^\\d{4}.\\d{2}.\\d{2} \\d{2}:\\d{2}", kDateFormatString: @"yyyy.MM.dd HH:mm"},
                          @{kDateFormatRegex: @"^\\d{2}.\\d{2}.\\d{4} \\d{2}:\\d{2}", kDateFormatString: @"dd.MM.yyyy HH:mm"},
                          @{kDateFormatRegex: @"^\\d{2}.\\d{2}.\\d{4} \\d{2}:\\d{2}:\\d{2}",     kDateFormatString: @"dd.MM.yyyy HH:mm:ss"},
                          @{kDateFormatRegex: @"^\\d{4}\\\\d{2}:\\\\d{2} \\d{2}:\\d{2}:\\d{2}",    kDateFormatString: @"yyyy\\MM\\dd HH:mm:ss"},
                          @{kDateFormatRegex: @"^\\d{2}\\\\d{2}\\\\d{4} \\d{2}:\\d{2}:\\d{2}", kDateFormatString: @"dd\\MM\\yyyy HH:mm:ss"}];
    });
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.locale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
    formatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    for (NSDictionary *dictionary in formatStrings) {
        NSRange range = [string rangeOfString:dictionary[kDateFormatRegex] options:NSRegularExpressionSearch];
        if (range.length == [string length] && range.location != NSNotFound) {
            formatter.dateFormat = dictionary[kDateFormatString];
            return [formatter dateFromString:string];
        }
    }
    
    return nil;
}

-(NSArray*) rihgtCompomentArray
{
    return [NSArray arrayWithObjects:@"minutes",@"hours",@"days",nil];
}

-(NSArray*) leftCompomentSecondArray
{
    return [NSArray arrayWithObjects:@"10 sec",@"20 sec",@"30 sec",nil];
}

-(NSArray*) leftCompomentMinuteArray
{
    return [NSArray arrayWithObjects:@"1 min",@"2 min",@"5 min",@"10 min",@"15 min",@"30 min",@"45 min",nil];
}
-(NSArray*) leftCompomentHourArray
{
    return [NSArray arrayWithObjects:@"1 hour",@"2 hours",@"5 hours",@"12 hours",nil];
}
-(NSArray*) leftCompomentDayArray
{
    return [NSArray arrayWithObjects:@"1 day",@"2 days",@"3 days",nil];
}
- (int)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (component == 0)
        return [[self rihgtCompomentArray] count];
    if (component == 1)
        {
            if ([self.pickerView selectedRowInComponent:0] == 0)
                return [[self leftCompomentMinuteArray] count];
            if ([self.pickerView selectedRowInComponent:0] == 1)
                return [[self leftCompomentHourArray] count];
            if ([self.pickerView selectedRowInComponent:0] == 2)
                return [[self leftCompomentDayArray] count];
        }
    return 0;
}
- (int)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 2;
}
- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    
    if (component == 0)
        return [[self rihgtCompomentArray] objectAtIndex:row];
    if (component == 1)
    {
        if ([self.pickerView selectedRowInComponent:0] == 0)
            return [[self leftCompomentMinuteArray] objectAtIndex:row];
        if ([self.pickerView selectedRowInComponent:0] == 1)
            return [[self leftCompomentHourArray] objectAtIndex:row];
        if ([self.pickerView selectedRowInComponent:0] == 2)
            return [[self leftCompomentDayArray] objectAtIndex:row];
    }
    return @"";
}
-(void) pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (component == 0)
        [self.pickerView reloadComponent:1];
}

- (IBAction)postponeStart:(id)sender {
    [self.activityIndicator setHidden:NO];
    [defaults setBool:YES forKey:@"notificationIsActive"];
    [defaults synchronize];
    //self.message.text=@"Notifications Started";
    int min = 60;
    int hour = 3600;
    int day = 86400;
    int mas_min[7] = {1,2,5,10,15,30,45};
    int mas_hour[4] = {1,2,5,12};
    int mas_day[3] = {1,2,3};
    
    NSTimeInterval  interval = 0.0;
    
     if ([self.pickerView selectedRowInComponent:0] == 0)
     {
         for (NSUInteger i = 0; i < [[self leftCompomentMinuteArray] count]; ++i)
         {
             if ([self.pickerView selectedRowInComponent:1] == i)
             {
                 interval = mas_min[i] * min;//1 minute from now
             }
         }
     }
    if ([self.pickerView selectedRowInComponent:0] == 1)
    {
        for (NSUInteger i = 0; i < [[self leftCompomentHourArray] count]; ++i)
        {
            if ([self.pickerView selectedRowInComponent:1] == i)
            {
                interval = mas_hour[i] * hour;
            }
        }
    }
    if ([self.pickerView selectedRowInComponent:0] == 2)
    {
        for (NSUInteger i = 0; i < [[self leftCompomentDayArray] count]; ++i)
        {
            if ([self.pickerView selectedRowInComponent:1] == i)
            {
                interval = mas_day[i] * day;
            }
        }
    }
    UILocalNotification* localNotification = [[UILocalNotification alloc] init];
    localNotification.timeZone = [NSTimeZone defaultTimeZone];
    localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:interval];
    localNotification.alertBody = _alertTitle;
   // localNotification.repeatInterval = NSCalendarUnitDay;//NSCalendarUnitMinute; //Repeating.
    localNotification.soundName = UILocalNotificationDefaultSoundName;
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
     [self dismissViewControllerAnimated:YES completion:nil];
}
@end
