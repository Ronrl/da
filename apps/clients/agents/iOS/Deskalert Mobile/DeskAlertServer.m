//
//  DeskAlertServer.m
//  Deskalert Mobile
//
//  Created by Дмитрий Кайгородов on 12.03.15.
//  Copyright (c) 2015 Deskalert. All rights reserved.
//

#import "DeskAlertServer.h"
#import "UserSettings.h"
#import "DeskAlertResponseDefines.h"
#import "TBXML.h"
#import "Reachability.h"
#import "NSString+HTML.h"
#import "AppDelegate.h"
#import "RNEncryptor.h"
#import "RNDecryptor.h"
#import "RNCryptor.h"
#import <EventKit/EventKit.h>
#import <CommonCrypto/CommonCryptor.h>
#import <CommonCrypto/CommonKeyDerivation.h>
#import <Security/Security.h>


@implementation DeskAlertServer

//NSInteger * ServerErrorInvalidCertificate = 18446744073709550414;
//NSInteger * ServerErrorHostnameCouldNotBeFound = 18446744073709550613;
//NSInteger * ServerErrorNotConnectToServer_0  = 18446744073709550;
//NSInteger * ServerErrorNotConnectToServer_1 = 18446744073709550612;

- (NSArray*) badSkins
{
    return [NSArray arrayWithObjects:@"{04f84380-b31d-4167-8b83-c6642e3d25a8}", @"{138f9281-0c11-4c48-983e-d4ca8076748b}", @"{c28f6977-3254-418c-b96e-bda43ce94ff4}", nil];
}

- (int) detectValueOfError{
    
    if([respString rangeOfString:responseDeskAlertRegistrationSuccess].location != NSNotFound ||
       [respString rangeOfString:responseDeskAlertLoginSuccess].location != NSNotFound)
        return 0;
    
    else if([respString rangeOfString:responseDeskAlertIncorrectLoginData].location != NSNotFound)
        return 1;
    else if([respString rangeOfString:responseDeskAlertShortUserName].location != NSNotFound)
        return 2;
    else if([respString rangeOfString:responseDeskAlertShortPassword].location != NSNotFound)
        return 3;
    else if([respString rangeOfString:responseDeskAlertIncorrectSimbolInUsername].location != NSNotFound)
        return 4;
    else if([respString rangeOfString:responseDeskAlertServerConfiguredActiveDirectory].location != NSNotFound)
        return 5;
    else if([respString rangeOfString:responseDeskAlertCredentialsNotFoundAD].location != NSNotFound)
        return 6;
    else if ([respString rangeOfString:responseDeskAlertProvidedPasswordIncorrect].location != NSNotFound)
        return 7;
    else if ([respString rangeOfString:responseDeskAlertReasonDomainIsNull].location != NSNotFound)
        return 8;
    else if ([respString rangeOfString:responseDeskAlertDontHaveSynchronized].location != NSNotFound)
        return 10;
    else if ([respString rangeOfString:responseDeskAlertLoginOrPasswordIncorrect].location != NSNotFound)
        return 11;
    else
        return 9;
}

// Statyc field it is need to Unit tests
static NSString *respString = @"";

- (int) validateOrRegisterUserWithLogin:(NSString *)login andPassword:(NSString *)password onServerURL:(NSURL *)serverURL andDomain:(NSString*) domain {
    NSURL *requestURL = [serverURL URLByAppendingPathComponent:@"registerform.aspx"];
    NSError *error;
    
    NSMutableArray *items = [[NSMutableArray alloc] init];
    [items addObject:[NSURLQueryItem queryItemWithName:@"client_id" value:@"3"]];
    [items addObject:[NSURLQueryItem queryItemWithName:@"uname" value:login]];
    [items addObject:[NSURLQueryItem queryItemWithName:@"upass" value:password]];
    [items addObject:[NSURLQueryItem queryItemWithName:@"desk_id" value:[UserSettings sharedInstance].guid]];
    [items addObject:[NSURLQueryItem queryItemWithName:@"domain_name" value:domain]];
    
    NSURLComponents *tmp = [[NSURLComponents alloc] init];
    [tmp setQueryItems:items];
    
    NSString *queryStringValidateOrRegisterUser = [[tmp URL] query];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL: requestURL];
    
    [request setHTTPMethod:@"POST"];
    
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[queryStringValidateOrRegisterUser length]] forHTTPHeaderField:@"Content-length"];
    
    [request setHTTPBody:[queryStringValidateOrRegisterUser dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSData* recievedData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:&error];
    
    NSString* responseString = [[NSString alloc] initWithBytes:[recievedData bytes] length:[recievedData length] encoding:NSUTF8StringEncoding];
    
    respString = responseString;
    
    return [self detectValueOfError];
    
}

-(ValidationData) validateServer: (NSURL *)url {
    NSURL *requestURL = [url URLByAppendingPathComponent:@"register.asp"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
    [request setHTTPMethod:@"GET"];
    [request setTimeoutInterval: 3];
    
    ValidationData data;
    
    NSError* error;
    NSData* recievedData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:&error];
    if(!recievedData || error) {
        data.urlValid = false;
        data.error = error;
    } else {
        data.urlValid = true;
    }
    return data;
}

- (BOOL) isNetworkAvailable
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    
    NetworkStatus status = [reachability currentReachabilityStatus];
    return status != NotReachable;
}

- (BOOL) checkInternetConnection:(BOOL) showAlertIfNotAvailable
{
    BOOL avail = [self isNetworkAvailable];
    
    if(showAlertIfNotAvailable && !avail)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)  message:NSLocalizedString(@"NetFail", nil) delegate:nil cancelButtonTitle: NSLocalizedString(@"TryAgain" , nil) otherButtonTitles:nil];
        
        [alert show];
    }
    return avail;
}

- (BOOL) isDemo {
    NSURL *requestURL = [[UserSettings sharedInstance].serverURL URLByAppendingPathComponent:@"demo_checker.asp"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL: requestURL];
    NSData* recievedData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    
    if(recievedData) {
        NSString* responseString = [[NSString alloc] initWithBytes:[recievedData bytes] length:[recievedData length] encoding:NSUTF8StringEncoding];
        return [responseString isEqualToString:@"1"];
    }
    return NO;
}

- (void) registerDeviceWithToken:(NSData*)token {
    if(![self checkInternetConnection:YES])
        return;
    
    [UserSettings sharedInstance].device_token = token;
    [UserSettings saveUserSettings];
    
    NSMutableString *strToken = [NSMutableString string];
    if (!token) {
        [strToken appendFormat:@"fake:%@", [UserSettings sharedInstance].guid];
    } else {
        const char *data = [token bytes];

        for (NSUInteger i = 0; i < [token length]; i++) {
            [strToken appendFormat:@"%02.2hhx", data[i]];
        }
    }
    
    
    NSURL *requestURL = [[UserSettings sharedInstance].serverURL URLByAppendingPathComponent:@"register_device.asp"];
    
    NSMutableArray *items = [[NSMutableArray alloc] init];
    [items addObject:[NSURLQueryItem queryItemWithName:@"sdk_int" value:@"0"]];
    [items addObject:[NSURLQueryItem queryItemWithName:@"device_type" value:@"3"]];
    [items addObject:[NSURLQueryItem queryItemWithName:@"device_id" value:strToken]];
    [items addObject:[NSURLQueryItem queryItemWithName:@"user_name" value:[UserSettings sharedInstance].login]];
    [items addObject:[NSURLQueryItem queryItemWithName:@"guid" value:[UserSettings sharedInstance].guid]];
    [items addObject:[NSURLQueryItem queryItemWithName:@"domain_name" value:[UserSettings sharedInstance].domain]];
    
    NSURLComponents *tmp = [[NSURLComponents alloc] init];
    [tmp setQueryItems:items];
    
    NSString *queryStringRegisterDeviceWithToken = [[tmp URL] query];
  
    NSLog(@" QSTR = %@", queryStringRegisterDeviceWithToken);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL: requestURL];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[queryStringRegisterDeviceWithToken length]] forHTTPHeaderField:@"Content-length"];
    
    [request setHTTPBody:[queryStringRegisterDeviceWithToken dataUsingEncoding:NSUTF8StringEncoding]];
    [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
}


- (void) unregisterDevice {
    if(![self checkInternetConnection:YES])
        return;
    
    NSString* strToken = [[[UserSettings sharedInstance].device_token description] stringByTrimmingCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    strToken = [strToken stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSURL *requestURL = [[UserSettings sharedInstance].serverURL URLByAppendingPathComponent:@"unregister_device.asp"];
    
    NSMutableArray *items = [[NSMutableArray alloc] init];
    [items addObject:[NSURLQueryItem queryItemWithName:@"sdk_int" value:@"0"]];
    [items addObject:[NSURLQueryItem queryItemWithName:@"device_type" value:@"3"]];
    [items addObject:[NSURLQueryItem queryItemWithName:@"device_id" value:strToken]];
    [items addObject:[NSURLQueryItem queryItemWithName:@"user_name" value:[UserSettings sharedInstance].login]];
    [items addObject:[NSURLQueryItem queryItemWithName:@"guid" value:[UserSettings sharedInstance].guid]];
    [items addObject:[NSURLQueryItem queryItemWithName:@"domain_name" value:[UserSettings sharedInstance].domain]];
    
    NSURLComponents *tmp = [[NSURLComponents alloc] init];
    [tmp setQueryItems:items];
    
    NSString *queryStringUnregisterDevice = [[tmp URL] query];

    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL: requestURL];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[queryStringUnregisterDevice length]] forHTTPHeaderField:@"Content-length"];
    
    [request setHTTPBody:[queryStringUnregisterDevice dataUsingEncoding:NSUTF8StringEncoding]];
    [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    
}

+ (instancetype) defaultServer
{
    static id server;
    static dispatch_once_t token;
    dispatch_once(&token, ^{
    
        server = [[super alloc] initUniqueInstance];
        
    });
    
    return server;
}

- (instancetype) initUniqueInstance
{
    return [super init];
}

-(NSDictionary*)getSymbols
{
    NSDictionary* dict = @{@"&quot;"     : @"\"",
                           @"&amp;"      : @"&",
                           @"&apos;"     : @"\'",
                           @"&lt;"       : @"<",
                           @"&gt;"       : @">",
                           @"&nbsp;"     : @"\u00A0",
                           @"&iexcl;"    : @"\u00A1",
                           @"&cent;"     : @"\u00A2",
                           @"&pound;"    : @"\u00A3",
                           @"&curren;"   : @"\u00A4",
                           @"&yen;"      : @"\u00A5",
                           @"&brvbar;"   : @"\u00A6",
                           @"&sect;"     : @"\u00A7",
                           @"&uml;"      : @"\u00A8",
                           @"&copy;"     : @"\u00A9",
                           @"&ordf;"     : @"\u00AA",
                           @"&laquo;"    : @"\u00AB",
                           @"&not;"      : @"\u00AC",
                           @"&shy;"      : @"\u00AD",
                           @"&reg;"      : @"\u00AE",
                           @"&macr;"     : @"\u00AF",
                           @"&deg;"      : @"\u00B0",
                           @"&plusmn;"   : @"\u00B1",
                           @"&sup2;"     : @"\u00B2",
                           @"&sup3;"     : @"\u00B3",
                           @"&acute;"    : @"\u00B4",
                           @"&micro;"    : @"\u00B5",
                           @"&para;"     : @"\u00B6",
                           @"&middot;"   : @"\u00B7",
                           @"&cedil;"    : @"\u00B8",
                           @"&sup1;"     : @"\u00B9",
                           @"&ordm;"     : @"\u00BA",
                           @"&raquo;"    : @"\u00BB",
                           @"&frac14;"   : @"\u00BC",
                           @"&frac12;"   : @"\u00BD",
                           @"&frac34;"   : @"\u00BE",
                           @"&iquest;"   : @"\u00BF",
                           @"&Agrave;"   : @"\u00C0",
                           @"&Aacute;"   : @"\u00C1",
                           @"&Acirc;"    : @"\u00C2",
                           @"&Atilde;"   : @"\u00C3",
                           @"&Auml;"     : @"\u00C4",
                           @"&Aring;"    : @"\u00C5",
                           @"&AElig;"    : @"\u00C6",
                           @"&Ccedil;"   : @"\u00C7",
                           @"&Egrave;"   : @"\u00C8",
                           @"&Eacute;"   : @"\u00C9",
                           @"&Ecirc;"    : @"\u00CA",
                           @"&Euml;"     : @"\u00CB",
                           @"&Igrave;"   : @"\u00CC",
                           @"&Iacute;"   : @"\u00CD",
                           @"&Icirc;"    : @"\u00CE",
                           @"&Iuml;"     : @"\u00CF",
                           @"&ETH;"      : @"\u00D0",
                           @"&Ntilde;"   : @"\u00D1",
                           @"&Ograve;"   : @"\u00D2",
                           @"&Oacute;"   : @"\u00D3",
                           @"&Ocirc;"    : @"\u00D4",
                           @"&Otilde;"   : @"\u00D5",
                           @"&Ouml;"     : @"\u00D6",
                           @"&times;"    : @"\u00D7",
                           @"&Oslash;"   : @"\u00D8",
                           @"&Ugrave;"   : @"\u00D9",
                           @"&Uacute;"   : @"\u00DA",
                           @"&Ucirc;"    : @"\u00DB",
                           @"&Uuml;"     : @"\u00DC",
                           @"&Yacute;"   : @"\u00DD",
                           @"&THORN;"    : @"\u00DE",
                           @"&szlig;"    : @"\u00DF",
                           @"&agrave;"   : @"\u00E0",
                           @"&aacute;"   : @"\u00E1",
                           @"&acirc;"    : @"\u00E2",
                           @"&atilde;"   : @"\u00E3",
                           @"&auml;"     : @"\u00E4",
                           @"&aring;"    : @"\u00E5",
                           @"&aelig;"    : @"\u00E6",
                           @"&ccedil;"   : @"\u00E7",
                           @"&egrave;"   : @"\u00E8",
                           @"&eacute;"   : @"\u00E9",
                           @"&ecirc;"    : @"\u00EA",
                           @"&euml;"     : @"\u00EB",
                           @"&igrave;"   : @"\u00EC",
                           @"&iacute;"   : @"\u00ED",
                           @"&icirc;"    : @"\u00EE",
                           @"&iuml;"     : @"\u00EF",
                           @"&eth;"      : @"\u00F0",
                           @"&ntilde;"   : @"\u00F1",
                           @"&ograve;"   : @"\u00F2",
                           @"&oacute;"   : @"\u00F3",
                           @"&ocirc;"    : @"\u00F4",
                           @"&otilde;"   : @"\u00F5",
                           @"&ouml;"     : @"\u00F6",
                           @"&divide;"   : @"\u00F7",
                           @"&oslash;"   : @"\u00F8",
                           @"&ugrave;"   : @"\u00F9",
                           @"&uacute;"   : @"\u00FA",
                           @"&ucirc;"    : @"\u00FB",
                           @"&uuml;"     : @"\u00FC",
                           @"&yacute;"   : @"\u00FD",
                           @"&thorn;"    : @"\u00FE",
                           @"&yuml;"     : @"\u00FF",
                           @"&OElig;"    : @"\u0152",
                           @"&oelig;"    : @"\u0153",
                           @"&Scaron;"   : @"\u0160",
                           @"&scaron;"   : @"\u0161",
                           @"&Yuml;"     : @"\u0178",
                           @"&fnof;"     : @"\u0192",
                           @"&circ;"     : @"\u02C6",
                           @"&tilde;"    : @"\u02DC",
                           @"&Alpha;"    : @"\u0391",
                           @"&Beta;"     : @"\u0392",
                           @"&Gamma;"    : @"\u0393",
                           @"&Delta;"    : @"\u0394",
                           @"&Epsilon;"  : @"\u0395",
                           @"&Zeta;"     : @"\u0396",
                           @"&Eta;"      : @"\u0397",
                           @"&Theta;"    : @"\u0398",
                           @"&Iota;"     : @"\u0399",
                           @"&Kappa;"    : @"\u039A",
                           @"&Lambda;"   : @"\u039B",
                           @"&Mu;"       : @"\u039C",
                           @"&Nu;"       : @"\u039D",
                           @"&Xi;"       : @"\u039E",
                           @"&Omicron;"  : @"\u039F",
                           @"&Pi;"       : @"\u03A0",
                           @"&Rho;"      : @"\u03A1",
                           @"&Sigma;"    : @"\u03A3",
                           @"&Tau;"      : @"\u03A4",
                           @"&Upsilon;"  : @"\u03A5",
                           @"&Phi;"      : @"\u03A6",
                           @"&Chi;"      : @"\u03A7",
                           @"&Psi;"      : @"\u03A8",
                           @"&Omega;"    : @"\u03A9",
                           @"&alpha;"    : @"\u03B1",
                           @"&beta;"     : @"\u03B2",
                           @"&gamma;"    : @"\u03B3",
                           @"&delta;"    : @"\u03B4",
                           @"&epsilon;"  : @"\u03B5",
                           @"&zeta;"     : @"\u03B6",
                           @"&eta;"      : @"\u03B7",
                           @"&theta;"    : @"\u03B8",
                           @"&iota;"     : @"\u03B9",
                           @"&kappa;"    : @"\u03BA",
                           @"&lambda;"   : @"\u03BB",
                           @"&mu;"       : @"\u03BC",
                           @"&nu;"       : @"\u03BD",
                           @"&xi;"       : @"\u03BE",
                           @"&omicron;"  : @"\u03BF",
                           @"&pi;"       : @"\u03C0",
                           @"&rho;"      : @"\u03C1",
                           @"&sigmaf;"   : @"\u03C2",
                           @"&sigma;"    : @"\u03C3",
                           @"&tau;"      : @"\u03C4",
                           @"&upsilon;"  : @"\u03C5",
                           @"&phi;"      : @"\u03C6",
                           @"&chi;"      : @"\u03C7",
                           @"&psi;"      : @"\u03C8",
                           @"&omega;"    : @"\u03C9",
                           @"&thetasym;" : @"\u03D1",
                           @"&upsih;"    : @"\u03D2",
                           @"&piv;"      : @"\u03D6",
                           @"&ensp;"     : @"\u2002",
                           @"&emsp;"     : @"\u2003",
                           @"&thinsp;"   : @"\u2009",
                           @"&zwnj;"     : @"\u200C",
                           @"&zwj;"      : @"\u200D",
                           @"&lrm;"      : @"\u200E",
                           @"&rlm;"      : @"\u200F",
                           @"&ndash;"    : @"\u2013",
                           @"&mdash;"    : @"\u2014",
                           @"&lsquo;"    : @"\u2018",
                           @"&rsquo;"    : @"\u2019",
                           @"&sbquo;"    : @"\u201A",
                           @"&ldquo;"    : @"\u201C",
                           @"&rdquo;"    : @"\u201D",
                           @"&bdquo;"    : @"\u201E",
                           @"&dagger;"   : @"\u2020",
                           @"&Dagger;"   : @"\u2021",
                           @"&bull;"     : @"\u2022",
                           @"&hellip;"   : @"\u2026",
                           @"&permil;"   : @"\u2030",
                           @"&prime;"    : @"\u2032",
                           @"&Prime;"    : @"\u2033",
                           @"&lsaquo;"   : @"\u2039",
                           @"&rsaquo;"   : @"\u203A",
                           @"&oline;"    : @"\u203E",
                           @"&frasl;"    : @"\u2044",
                           @"&euro;"     : @"\u20AC",
                           @"&image;"    : @"\u2111",
                           @"&weierp;"   : @"\u2118",
                           @"&real;"     : @"\u211C",
                           @"&trade;"    : @"\u2122",
                           @"&alefsym;"  : @"\u2135",
                           @"&larr;"     : @"\u2190",
                           @"&uarr;"     : @"\u2191",
                           @"&rarr;"     : @"\u2192",
                           @"&darr;"     : @"\u2193",
                           @"&harr;"     : @"\u2194",
                           @"&crarr;"    : @"\u21B5",
                           @"&lArr;"     : @"\u21D0",
                           @"&uArr;"     : @"\u21D1",
                           @"&rArr;"     : @"\u21D2",
                           @"&dArr;"     : @"\u21D3",
                           @"&hArr;"     : @"\u21D4",
                           @"&forall;"   : @"\u2200",
                           @"&part;"     : @"\u2202",
                           @"&exist;"    : @"\u2203",
                           @"&empty;"    : @"\u2205",
                           @"&nabla;"    : @"\u2207",
                           @"&isin;"     : @"\u2208",
                           @"&notin;"    : @"\u2209",
                           @"&ni;"       : @"\u220B",
                           @"&prod;"     : @"\u220F",
                           @"&sum;"      : @"\u2211",
                           @"&minus;"    : @"\u2212",
                           @"&lowast;"   : @"\u2217",
                           @"&radic;"    : @"\u221A",
                           @"&prop;"     : @"\u221D",
                           @"&infin;"    : @"\u221E",
                           @"&ang;"      : @"\u2220",
                           @"&and;"      : @"\u2227",
                           @"&or;"       : @"\u2228",
                           @"&cap;"      : @"\u2229",
                           @"&cup;"      : @"\u222A",
                           @"&int;"      : @"\u222B",
                           @"&there4;"   : @"\u2234",
                           @"&sim;"      : @"\u223C",
                           @"&cong;"     : @"\u2245",
                           @"&asymp;"    : @"\u2248",
                           @"&ne;"       : @"\u2260",
                           @"&equiv;"    : @"\u2261",
                           @"&le;"       : @"\u2264",
                           @"&ge;"       : @"\u2265",
                           @"&sub;"      : @"\u2282",
                           @"&sup;"      : @"\u2283",
                           @"&nsub;"     : @"\u2284",
                           @"&sube;"     : @"\u2286",
                           @"&supe;"     : @"\u2287",
                           @"&oplus;"    : @"\u2295",
                           @"&otimes;"   : @"\u2297",
                           @"&perp;"     : @"\u22A5",
                           @"&sdot;"     : @"\u22C5",
                           @"&lceil;"    : @"\u2308",
                           @"&rceil;"    : @"\u2309",
                           @"&lfloor;"   : @"\u230A",
                           @"&rfloor;"   : @"\u230B",
                           @"&lang;"     : @"\u2329",
                           @"&rang;"     : @"\u232A",
                           @"&loz;"      : @"\u25CA",
                           @"&spades;"   : @"\u2660",
                           @"&clubs;"    : @"\u2663",
                           @"&hearts;"   : @"\u2665",
                           @"&diams;"    : @"\u2666"};
    return dict;
}

- (void) deleteAlertWithId:(NSInteger)alertId forUser:(NSString *)userName {
    NSString *urlAttributes = [[NSString alloc] initWithFormat:@"DeleteAlert.aspx?userName=%@&alertId=%ld", userName, (long)alertId];
    NSURL *requestURL = [[UserSettings sharedInstance].serverURL URLByAppendingPathComponent: urlAttributes];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
    NSData* data = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    NSLog(@"%@", data);
}

- (void) parseAllRelevantAlerts:(BOOL) needToDisplay andAlertId: (NSInteger) alertId andFirstPressedAlertId: (NSInteger) firstPressedAlertId
{
    UserSettings* settings = [UserSettings sharedInstance];
    if (!settings.serverURL) {
        return;
    }
    NSString * version = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
    
    NSDictionary* infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString* appID = infoDictionary[@"CFBundleIdentifier"];
    NSURL* urla = [NSURL URLWithString:[NSString stringWithFormat:@"http://itunes.apple.com/lookup?bundleId=%@", appID]];
    NSData* data = [NSData dataWithContentsOfURL:urla];
    NSString* appStoreVersion = version;
    if (data != nil) {
        NSDictionary* lookup = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        if ([lookup[@"resultCount"] integerValue] == 1){
            appStoreVersion = lookup[@"results"][0][@"version"];
        }
    }
    

    NSURL *getAlertsURL = [settings.serverURL URLByAppendingPathComponent:@"getalerts.aspx"];
    NSURLComponents *relevantAlerts = [[NSURLComponents alloc] initWithURL:getAlertsURL resolvingAgainstBaseURL:YES];

    NSMutableArray *items = [[NSMutableArray alloc] init];
    [items addObject:[NSURLQueryItem queryItemWithName:@"uname" value:settings.login]];
    [items addObject:[NSURLQueryItem queryItemWithName:@"computer" value:@"mobile"]];
    [items addObject:[NSURLQueryItem queryItemWithName:@"cnt" value:@"cont"]];
    [items addObject:[NSURLQueryItem queryItemWithName:@"deskbar_id" value:settings.guid]];
    [items addObject:[NSURLQueryItem queryItemWithName:@"client_version" value:appStoreVersion]];
    [items addObject:[NSURLQueryItem queryItemWithName:@"fulldomain" value:settings.domain]];

    [relevantAlerts setQueryItems:items];
    
    NSURL *urlRelevantAlerts = [relevantAlerts URL];
    
    [self retrieveAlerts:needToDisplay andAlertId:alertId fromSource:urlRelevantAlerts andFirstPressedAlertId: firstPressedAlertId];
}

- (void) retrieveAlerts:(BOOL) needToDisplay andAlertId: (NSInteger) alertId fromSource: (NSURL*) url andFirstPressedAlertId: (NSInteger) firstPressedAlertId
{
    if(![self checkInternetConnection:YES])
        return;
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        if(data != nil && data.length != 0) {
            NSString *xmlString = [[NSString alloc] initWithBytes:data.bytes length:data.length encoding:NSUTF8StringEncoding];
            
            NSError *err = nil;
            
            TBXML* sourceXml = [TBXML newTBXMLWithXMLString:xmlString error:&err];
            
            TBXMLElement *element = [TBXML childElementNamed:@"ALERT" parentElement:sourceXml.rootXMLElement];
            if(!element)
            {
                if(needToDisplay && alertId > 0)
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"showAlert" object:[NSNumber numberWithInteger:alertId]];
                }
                return;
            }
            
            if(!err)
            {
                do{
                
                    if([[TBXML elementName:element] isEqualToString:@"ALERT"])
                    {
                        
                        BOOL isTicker = [[TBXML valueOfAttributeNamed:@"ticker" forElement:element] boolValue];
                        
                        NSString* title = [TBXML valueOfAttributeNamed:@"title" forElement:element];
                        
                        NSString* stringURL = [TBXML valueOfAttributeNamed:@"href" forElement:element];
                        
                        for (NSString *key in [self getSymbols])
                        {
                            if([stringURL containsString:key])
                            {
                                stringURL = [stringURL stringByReplacingOccurrencesOfString:key withString:[[self getSymbols] valueForKey:key]];
                            }
                        }
                        
                        NSString* webStringURL = [stringURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                        
                        NSURL *alertUrl = [NSURL URLWithString:webStringURL];
                        
                        NSMutableURLRequest *alertRequest = [NSMutableURLRequest requestWithURL:alertUrl];
                        
                        
                         NSData * alertData = [NSURLConnection sendSynchronousRequest:alertRequest returningResponse:nil error:nil];
                        
                        NSString *alertHtmlString = @"";
                        
                        alertHtmlString =  [[NSString alloc] initWithBytes:alertData.bytes length:alertData.length encoding:NSUTF8StringEncoding];
                        
                        if (alertHtmlString == nil)
                        {
                            alertHtmlString = @"";
                        }
                        
                        
                        
                        alertHtmlString = [[alertHtmlString componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@" "];
                        
                        //escape double quotes
                        alertHtmlString = [alertHtmlString stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""];

                        alertHtmlString = [alertHtmlString stringByReplacingOccurrencesOfString:@"</script>" withString:@"<\\/script>"];
                        
                        
                        alertHtmlString = [alertHtmlString stringByReplacingOccurrencesOfString:@"cols='50'" withString:@"cols='40'"];
                        
                        NSInteger aId = [[TBXML valueOfAttributeNamed:@"alert_id" forElement:element] integerValue];
                        
                        NSNumber *survey = [NSNumber numberWithInt:[[TBXML valueOfAttributeNamed:@"survey" forElement:element] intValue] ];
                        
                        NSNumber *isAcknown = [NSNumber numberWithInt:[[TBXML valueOfAttributeNamed:@"acknown" forElement:element] intValue]];
                        
                        
                        NSError* err = nil;
                        // <!-- html_title=.*<em>.*<\/em>.*
                        NSRegularExpression *regex = [[NSRegularExpression alloc] initWithPattern:@"<!-- html_title=.*" options:NSRegularExpressionCaseInsensitive error:&err];
                        
                        NSArray* comments = [regex matchesInString:alertHtmlString options:0 range:NSMakeRange(0, [alertHtmlString length])];
                        title = [title stringByDecodingHTMLEntities];

                        NSString* titleString = @"";
                        if([comments count] > 0)
                        {
                        
                            NSTextCheckingResult *match = [comments objectAtIndex:0];
                            titleString = [alertHtmlString substringWithRange:match.range];
                            titleString = [titleString stringByDecodingHTMLEntities];

                            
                            regex = [[NSRegularExpression alloc] initWithPattern:@"\'.*\'" options:NSRegularExpressionCaseInsensitive error:&err];
                        
                            comments = [regex matchesInString:titleString options:0 range:NSMakeRange(0, [titleString length])];
                        
                            if([comments count]> 0)
                            {
                                match = [comments objectAtIndex:0];
                                titleString = [titleString substringWithRange:match.range];
                                titleString = [titleString stringByTrimmingCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@"\'\\"]];
                                
                            }

                            
                        }
                        
                        for (NSString *key in [self getSymbols])
                        {
                            if([title containsString:key])
                            {
                                title = [title stringByReplacingOccurrencesOfString:key withString:[[self getSymbols] valueForKey:key]];
                            }
                        }
                        
                        
                        //Parse margins
                        TBXMLElement* windowElem = [TBXML childElementNamed:@"WINDOW" parentElement:element];
                        
                        NSString* strTopMargin = [TBXML valueOfAttributeNamed:@"topmargin" forElement:windowElem];
                        NSString* strLeftMargin = [TBXML valueOfAttributeNamed:@"leftmargin" forElement:windowElem];
                        NSString* strBottomMargin = [TBXML valueOfAttributeNamed:@"bottommargin" forElement:windowElem];
                        NSString* strRightMargin = [TBXML valueOfAttributeNamed:@"rightmargin" forElement:windowElem];
                        
                        NSString* caption = [TBXML valueOfAttributeNamed:@"captionhref" forElement:windowElem];
                    
                        if(isTicker)
                            caption = [caption stringByReplacingOccurrencesOfString:@"tickercaption" withString:@"alertcaption"];
                            
                        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
                        formatter.numberStyle = NSNumberFormatterDecimalStyle;
                        
                        NSNumber* topMargin = [formatter numberFromString:strTopMargin];
                        NSNumber* leftMargin = [formatter numberFromString:strLeftMargin];
                        NSNumber* bottomMargin = [formatter numberFromString:strBottomMargin];
                        NSNumber* rightMargin = [formatter numberFromString:strRightMargin];
                            
                        if([leftMargin intValue] < 7)
                            leftMargin = [NSNumber numberWithInt:[leftMargin intValue] + 10];
                        
                        NSDictionary* margins = [[NSDictionary alloc] initWithObjectsAndKeys:topMargin, @"top", bottomMargin ,@"bottom", leftMargin, @"left", rightMargin, @"right",  nil];

                        NSNumber* isSelfDestructed = [NSNumber numberWithBool:[[TBXML valueOfAttributeNamed:@"self_deletable" forElement:element] boolValue]];
                    
                        NSMutableDictionary* alertInfo = [[NSMutableDictionary alloc] init];
                        [alertInfo setValue:title forKey:@"title"];
                        [alertInfo setValue:[NSDate date] forKey:@"createdDate"];
                        [alertInfo setValue:alertHtmlString forKey:@"data"];
                        [alertInfo setValue:caption forKey:@"skinpath"];
                        [alertInfo setValue:margins forKey:@"margins"];
                        [alertInfo setValue:titleString forKey:@"htmlTitle"];
                        [alertInfo setValue:[NSNumber numberWithBool:needToDisplay] forKey:@"needToDisplay"];
                        [alertInfo setValue:[NSNumber numberWithInteger:aId] forKey:@"id"];
                        [alertInfo setValue:survey forKey:@"survey"];
                        [alertInfo setValue:isAcknown forKey:@"isAcknown"];
                        [alertInfo setValue:[NSNumber numberWithInteger:firstPressedAlertId] forKey:@"firstPressedAlertId"];
                        [alertInfo setValue:isSelfDestructed forKey:@"isSelfDestructed"];
                        
                        [[NSNotificationCenter defaultCenter ] postNotificationName:@"recieveAlert" object:alertInfo];
                    }
                } while((element = element->nextSibling));
            }
        }
        
    }];

}
@end
