//
//  UserSettings.m
//  Deskalert Mobile
//
//  Created by Дмитрий Кайгородов on 10.03.15.
//  Copyright (c) 2015 Deskalert. All rights reserved.
//

#import "UserSettings.h"

@implementation UserSettings

+ (UserSettings*) sharedInstance
{
    
    static dispatch_once_t pred;
    static id shared = nil;
    dispatch_once(&pred, ^{
        shared = [[super alloc] initUniqueInstance];
    });
    return shared;
}

- (instancetype) initUniqueInstance
{
    return [super init];
}

+ (void) saveUserSettings {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];    
    [defaults setURL:[UserSettings sharedInstance].serverURL forKey:@"serverURL"];
    [defaults setObject:[UserSettings sharedInstance].login forKey:@"login"];
    [defaults setObject:[UserSettings sharedInstance].password forKey:@"password"];
    [defaults setObject:[UserSettings sharedInstance].domain forKey:@"domain"];
    [defaults setBool:[UserSettings sharedInstance].useSound forKey:@"useSound"];
    [defaults setBool:[UserSettings sharedInstance].hasLaunchOnce forKey:@"hasLaunchOnce"];
    [defaults setObject:[UserSettings sharedInstance].device_token forKey:@"token"];
    [defaults setInteger:[UserSettings sharedInstance].lastAlertId forKey:@"lastAlertId"];
}

+(NSString*)GUIDString {
    CFUUIDRef theUUID = CFUUIDCreate(NULL);
    CFStringRef string = CFUUIDCreateString(NULL, theUUID);
  //  CFRelease(theUUID);
    NSString* guid = (__bridge NSString *)string;
    return guid;
}

-(void) changeLastAlertValueIfNecessary: (NSInteger) newLastAlertId
{
    if(self.lastAlertId < newLastAlertId)
    {
        self.lastAlertId = newLastAlertId;
        [[NSUserDefaults standardUserDefaults] setInteger:newLastAlertId forKey:@"lastAlertId"];
    }
}

+ (void) loadUserSettings {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    
    NSString* guid = [defaults stringForKey:@"guid"];
    
    if(guid == nil) {
        [UserSettings sharedInstance].guid = [UserSettings GUIDString];
        [defaults setObject:[UserSettings sharedInstance].guid forKey:@"guid" ];
        [defaults synchronize];
    }
    else [UserSettings sharedInstance].guid = guid;
    

    NSLog(@"GUID = %@", [UserSettings sharedInstance].guid);
    [UserSettings sharedInstance].serverURL = [defaults URLForKey:@"serverURL"];
    [UserSettings sharedInstance].login = [defaults stringForKey:@"login"];
    [UserSettings sharedInstance].password = [defaults stringForKey:@"password"];
    [UserSettings sharedInstance].domain = [defaults stringForKey:@"domain"];
    [UserSettings sharedInstance].useSound = [defaults boolForKey:@"useSound"];
    [UserSettings sharedInstance].hasLaunchOnce = [defaults boolForKey:@"hasLaunchOnce"];
    [UserSettings sharedInstance].device_token = [defaults objectForKey:@"token"];
    [UserSettings sharedInstance].lastAlertId = [defaults integerForKey:@"lastAlertId"];
    
    if ([UserSettings sharedInstance].hasLaunchOnce) {
        if (![UserSettings dataMigrationCompleted]) {
            NSString *oldServerPath = [defaults stringForKey:@"hostName"];
            if(oldServerPath != nil) {
                [defaults removeObjectForKey:@"hostName"];
                NSURL *url = [[NSURL alloc] initWithString:oldServerPath];
                if (url != nil) {
                    [defaults setURL:url forKey:@"serverURL"];
                } else {
                    //Error invalid host name
                }
            }
        }
    }
}
- (IBAction)domain:(id)sender {
}

+ (BOOL) isFirstApplicationLaunches {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSInteger numberOfApplicationLaunches = [defaults integerForKey: @"NumberOfApplicationLaunches"];
    numberOfApplicationLaunches += 1;
    [defaults setInteger: numberOfApplicationLaunches forKey:@"NumberOfApplicationLaunches"];
    return numberOfApplicationLaunches == 1;
}
+ (BOOL) dataMigrationCompleted {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL tmpDataMigrationCompleted = [defaults boolForKey:@"DataMigrationCompleted"];
    [defaults setBool:true forKey:@"DataMigrationCompleted"];
    return tmpDataMigrationCompleted;
}
@end
