//
//  SearchResultController.h
//  Deskalert Mobile
//
//  Created by Дмитрий Кайгородов on 21.03.15.
//  Copyright (c) 2015 Deskalert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlertViewCell.h"
#import "Alert.h"
#import "AlertDetailsView.h"

@interface SearchResultController : UIViewController<UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate, UIAlertViewDelegate, UISearchControllerDelegate, UISearchResultsUpdating>


@property (strong, nonatomic) NSMutableArray* result;
@property (weak, nonatomic) IBOutlet UITableView *resultTable;
@property (weak) NSManagedObjectContext* managedObjectContext;

@end
