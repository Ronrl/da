//
//  AlertDetailsView.h
//  Deskalert Mobile
//
//  Created by Дмитрий Кайгородов on 23.03.15.
//  Copyright (c) 2015 Deskalert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Alert.h"
#import "UserSettings.h"
#import <EventKitUI/EventKitUI.h>
#import <EventKit/EventKit.h>
#import "NSString+HTML.h"
#import "CCMPlayNDropView.h"
//43.3%
//299px - w
//360px - h
#define kAlertTemplate @"<iframe id=\"skinFrame\" style=\"transform: scale(0.93,1);left: -10px; width:95%%; height:95.5%%; position:absolute; margin:0px; border:0px;\" src='%@' align=\"left\" ></iframe> <iframe id=\"alertContentFrame\" style=\"position:relative; top:%d; left:%d; bottom:%d; right:%d; width: 95%%\"  frameBorder=\"0\"></iframe><script type=\"text/javascript\"> skinFrame = document.getElementById('skinFrame').onload = function() {var doc = document.getElementById('alertContentFrame').contentWindow.document;doc.open(); doc.write(\"<div style='width:%dpx; height:%dpx; overflow:scroll; '>%@</div>\"); doc.close(); var skinFrame = document.getElementById('skinFrame').contentWindow; skinFrame.document.getElementById('create').innerHTML = \"%@\"; skinFrame.setTitle(\"%@\", %@);  skinFrame.document.getElementById(\"close_img\").style.display = \"none\"; } </script>"


@interface AlertDetailsView : UIViewController<UIWebViewDelegate, EKEventEditViewDelegate, CCMPlayNDropViewDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *alertDisplayView;
@property (strong, nonatomic) Alert* alert;
@property (strong, nonatomic) NSString* alertHtml;
@property int returnTab;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *backButton;
@property (weak, nonatomic) IBOutlet UIButton *okButton;
@property (weak, nonatomic) IBOutlet UIButton *printButton;
@property int print;
@property (strong, nonatomic) NSString* alertTitle;
@property (weak, nonatomic) IBOutlet UIButton *postpone;
- (IBAction)postponeStart:(id)sender;

@end
