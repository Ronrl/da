//
//  UnobtrusiveNetworkController.h
//  Deskalerts Mobile
//
//  Created by Дмитрий Кайгородов on 05/04/16.
//  Copyright © 2016 Deskalert. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserSettings.h"
#import "Unobtrusive.h"


@interface UnobtrusiveNetworkController : NSObject

+(instancetype) alloc __attribute__((unavailable("alloc not available, call sharedInstance instead")));
-(instancetype) init __attribute__((unavailable("init not available, call sharedInstance instead")));
+(instancetype) new __attribute__((unavailable("new not available, call sharedInstance instead")));


- (NSArray*) arrayOfUnobtrusives;
- (int) addUnobtrusive:(Unobtrusive*) u;
- (BOOL) deleteUnobtrusive:(Unobtrusive*) u;
- (BOOL) switchUnobtrusive:(Unobtrusive*) u;
- (void) updateUnobtrusive:(Unobtrusive*) u;
+ (instancetype) sharedInstance;
@end
