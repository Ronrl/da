//
//  SearchResultController.m
//  Deskalert Mobile
//
//  Created by Дмитрий Кайгородов on 21.03.15.
//  Copyright (c) 2015 Deskalert. All rights reserved.
//

#import "SearchResultController.h"

@interface SearchResultController ()

@end

@implementation SearchResultController

@synthesize result = _result;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    AlertViewCell* cell = (AlertViewCell*) [tableView dequeueReusableCellWithIdentifier:@"AlertCell" forIndexPath:indexPath];
    
    NSManagedObject *record = [self.result objectAtIndex:indexPath.row];
    
    
    
    NSString *title = [record valueForKey:@"title"];
    
    if([title length] > 10)
    {
        NSString *visiblePart = [title substringToIndex:10];
        title = [visiblePart stringByAppendingString:@"..."];
    }

    [cell.titleLabel setText:title];
    
    NSDate *creationDate =  [record valueForKey:@"recivedDate"];
    NSDateFormatter *formater = [[NSDateFormatter alloc] init];
    [formater setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    NSString* dateString = [formater stringFromDate:creationDate];
        [cell.dateLabel setText:dateString];
    
    BOOL unread = [[record valueForKey:@"unread"] boolValue];
    
    cell.unreadIndicator.image = unread ? [UIImage imageNamed:@"thread_new-16.png"] : nil;
    
    return cell;
    
}

- (NSUInteger) supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [self.result count];
}


- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"fromSearchToDetails" sender:self];
}


- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"fromSearchToDetails"])
    {
        
        AlertDetailsView* adv = segue.destinationViewController;
        NSIndexPath* indexPath = [self.resultTable indexPathForSelectedRow];
        Alert* selectedAlert = [self.result objectAtIndex:indexPath.row];
        
        if([[selectedAlert valueForKey:@"unread" ] boolValue])
        {
            [UIApplication sharedApplication].applicationIconBadgeNumber--;
            [selectedAlert setValue:@0 forKey:@"unread"];
            [self.managedObjectContext save:nil];
        }
        adv.alert = selectedAlert;
        [self.resultTable reloadData];
        
        
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
