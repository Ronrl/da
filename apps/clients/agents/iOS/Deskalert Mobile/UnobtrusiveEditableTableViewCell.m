//
//  UnobtrusiveEditableTableViewCell.m
//  Deskalerts Mobile
//
//  Created by Дмитрий Кайгородов on 08/04/16.
//  Copyright © 2016 Deskalert. All rights reserved.
//

#import "UnobtrusiveEditableTableViewCell.h"

@implementation UnobtrusiveEditableTableViewCell

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (IBAction)onDeleteClick:(id)sender {
    
    NSNumber* _id = [NSNumber numberWithInt:self.uId];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UnobtrusiveDelete" object:_id userInfo:nil];
}




@end
