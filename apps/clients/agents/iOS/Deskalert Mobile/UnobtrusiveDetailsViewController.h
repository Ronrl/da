//
//  UnobrrusiveDetailsViewController.h
//  Deskalerts Mobile
//
//  Created by Дмитрий Кайгородов on 04/04/16.
//  Copyright © 2016 Deskalert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Unobtrusive.h"
#import "WeekDaySelectionTableViewController.h"
#import "UnobtrusiveNetworkController.h"

@interface UnobtrusiveDetailsViewController : UITableViewController

@property (nonatomic) Unobtrusive* objectToAdd;
@property (weak, nonatomic) IBOutlet UIDatePicker *fromDate;
@property (weak, nonatomic) IBOutlet UIDatePicker *toDate;
@property (nonatomic) BOOL isEditMode;

@end
