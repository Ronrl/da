//
//  WelcomeViewController.m
//  Deskalerts
//
//  Created by Ярослав Долговых on 14/02/2020.
//  Copyright © 2020 Deskalert. All rights reserved.
//

#import "WelcomeViewController.h"
#import "Coordinator.h"

@interface WelcomeViewController ()
@property (weak, nonatomic) IBOutlet UIButton *setupButton;
@end

@implementation WelcomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (IBAction)setupButtonAction:(UIButton *)sender {
    [[Coordinator shared] setRegistrationStackViewControllers];
}
@end
