//
//  UnobtrusiveNetworkController.m
//  Deskalerts Mobile
//
//  Created by Дмитрий Кайгородов on 05/04/16.
//  Copyright © 2016 Deskalert. All rights reserved.
//

#import "UnobtrusiveNetworkController.h"

@implementation UnobtrusiveNetworkController

+ (instancetype) sharedInstance {
    static id server;
    static dispatch_once_t token;
    dispatch_once(&token, ^{
        
        server = [[super alloc] initUniqueInstance];
        
    });
    return server;
}

- (instancetype) initUniqueInstance {
    return [super init];
}

- (NSArray*) arrayOfUnobtrusives {
    UserSettings* settings = [UserSettings sharedInstance];
    NSString* urlAttributes = [NSString stringWithFormat:@"UnobtrusiveService.aspx?action=get&user=%@", settings.login];
    NSURL* requestURL = [settings.serverURL URLByAppendingPathComponent:urlAttributes];
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:requestURL];
    NSData* data = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    NSArray* result = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    return result;
}

- (void) updateUnobtrusive:(Unobtrusive*) unobtrusiveData {
    UserSettings* settings = [UserSettings sharedInstance];
    NSURL* requestURL = [settings.serverURL URLByAppendingPathComponent:@"UnobtrusiveService.aspx"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
    
    NSString* queryString = [NSString stringWithFormat:@"action=update&data=%@&id=%d", [unobtrusiveData asJsonString],  unobtrusiveData.uId ];
    [request setHTTPMethod:@"POST"];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[queryString length]] forHTTPHeaderField:@"Content-length"];
    
    [request setHTTPBody:[queryString dataUsingEncoding:NSUTF8StringEncoding]];
     NSError* err = nil;
    [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:&err];
}

- (int) addUnobtrusive:(Unobtrusive *) unobtrusiveData {
    UserSettings* settings = [UserSettings sharedInstance];
    NSURL* requestURL = [settings.serverURL URLByAppendingPathComponent:@"UnobtrusiveService.aspx"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
    
    NSString* queryString = [NSString stringWithFormat:@"action=add&data=%@&user=%@", [unobtrusiveData asJsonString], settings.login];
    [request setHTTPMethod:@"POST"];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[queryString length]] forHTTPHeaderField:@"Content-length"];
    
    [request setHTTPBody:[queryString dataUsingEncoding:NSUTF8StringEncoding]];
    NSError* err = nil;
    
    NSData* data = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:&err];
    
    NSString* strId = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    int newId = 0;
    
    if(strId && [strId length] > 0 && !err)
    {
        newId = [strId intValue];
    }
    else
    {
        newId = -1;
    }
    
    return newId;
}

- (BOOL) deleteUnobtrusive:(Unobtrusive *) unobtrusiveData {
    UserSettings* settings = [UserSettings sharedInstance];
    NSURL* requestURL = [settings.serverURL URLByAppendingPathComponent:@"UnobtrusiveService.aspx"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
    
    NSString* queryString = [NSString stringWithFormat:@"action=del&data=%d", unobtrusiveData.uId];
    [request setHTTPMethod:@"POST"];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[queryString length]] forHTTPHeaderField:@"Content-length"];
    
    [request setHTTPBody:[queryString dataUsingEncoding:NSUTF8StringEncoding]];
     NSError* err = nil;
    [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:&err];
    
     return err == nil;
}

- (BOOL) switchUnobtrusive:(Unobtrusive *) unobtrusiveData {
    UserSettings* settings = [UserSettings sharedInstance];
    NSURL* requestURL = [settings.serverURL URLByAppendingPathComponent:@"UnobtrusiveService.aspx"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
    
    NSString* queryString = [NSString stringWithFormat:@"action=switch&data=%d", unobtrusiveData.uId];
    [request setHTTPMethod:@"POST"];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[queryString length]] forHTTPHeaderField:@"Content-length"];
    
    [request setHTTPBody:[queryString dataUsingEncoding:NSUTF8StringEncoding]];
    NSError* err = nil;
    [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:&err];
    
    return err == nil;
}
@end
