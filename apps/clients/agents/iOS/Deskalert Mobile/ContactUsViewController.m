//
//  ContactUsViewController.m
//  Deskalerts Mobile
//
//  Created by Dmitrii on 30/12/15.
//  Copyright © 2015 Deskalert. All rights reserved.
//

#import "ContactUsViewController.h"

@interface ContactUsViewController ()

@end

@implementation ContactUsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:61.0f/255.0f green:162.0f/255.0f blue:66.0f/255.0f alpha:1.0f];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)onPhoneClick:(id)sender {
    
    UIButton* phoneButton = (UIButton*) sender;
    
    NSString* phoneString = phoneButton.titleLabel.text;
    
    NSURL* url = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", phoneString]];
    [[UIApplication sharedApplication] openURL: url];
}
- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    NSString* message  = @"";
    switch (result)
    {

        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            message = @"Your request was succesfully sent";
            break;
        case MFMailComposeResultFailed:
            message = @"Error occured on sending request";
            break;
        default:
            break;
    }
    
    

    
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
    if([message length] > 0)
    {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DeskAlerts" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
    }
}

- (IBAction)onMailSendClick:(id)sender
{
    NSString *emailTitle = @"DeskAlerts information request";
    // Email Content
    NSString *messageBody = @"";
    // To address
    NSArray *toRecipents = [NSArray arrayWithObject:@"sales@deskalerts.com"];
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setSubject:emailTitle];
    [mc setMessageBody:messageBody isHTML:NO];
    [mc setToRecipients:toRecipents];
    
    // Present mail view controller on screen
    [self presentViewController:mc animated:YES completion:NULL];
}

@end
