//
//  SecondWelcomeView.m
//  Deskalert Mobile
//
//  Created by Дмитрий Кайгородов on 10.03.15.
//  Copyright (c) 2015 Deskalert. All rights reserved.
//

#import "UserSettings.h"
#import "SecondWelcomeView.h"
#import "DeskAlertServer.h"
#import "FirstWelcomeView.h"

@interface SecondWelcomeView ()

@end

@implementation SecondWelcomeView

@synthesize scrollView;
@synthesize loginField, passwordField, domainField;

- (IBAction)loginChanged:(id)sender {
    
    self.loginButton.enabled = [self.loginField.text length ] > 0 && [self.passwordField.text length] > 0;
}

// Method get parameter and show Error message
- (void)showErrorMessageForUser:(int)success {
    switch (success) {
        case 0:
        {
            [UserSettings sharedInstance].login = self.loginField.text;
            [UserSettings sharedInstance].password = self.passwordField.text;
            [UserSettings sharedInstance].domain = self.domainField.text;
            [self performSegueWithIdentifier:@"2to3" sender:self];
        }
        break;
        case 1: {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)   message:NSLocalizedString(@"ConnFail", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"TryAgain" , nil), NSLocalizedString(@"CahangeHost", nil), nil];
            [alert show];
        }
        break;
        case 2: {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"ShortName", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"TryAgain", nil), NSLocalizedString(@"CahangeHost", nil), nil];
            [alert show];
        }
        break;
        case 3: {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"ShortPassword", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"TryAgain", nil), NSLocalizedString(@"CahangeHost", nil), nil];
            [alert show];
        }
        break;
        case 4: {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"WrongName", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"TryAgain", nil), NSLocalizedString(@"CahangeHost", nil), nil];
            [alert show];
        }
        break;
        case 5:{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"ConfigurAD", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"TryAgain" , nil), NSLocalizedString(@"CahangeHost", nil), nil];
            
            [alert show];
        }
        break;
        case 6:{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"CredentialsNotFoundInAD", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"TryAgain", nil), NSLocalizedString(@"CahangeHost", nil), nil];
            
            [alert show];
        }
        break;
        case 7:{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"ProvidedPasswordIncorrect", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"TryAgain", nil), NSLocalizedString(@"CahangeHost", nil), nil];
            [alert show];
        }
        break;
        case 8:{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"DomainIsNull", nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"TryAgain", nil), NSLocalizedString(@"CahangeHost", nil), nil];
            [alert show];
        }
        break;
        case 9:{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"SomeError", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"TryAgain", nil), NSLocalizedString(@"CahangeHost", nil), nil];
            
            [alert show];
        }
        break;
        case 10:{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"DontHaveSynchronized", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"TryAgain", nil), NSLocalizedString(@"ChangeHost", nil), nil];
            [alert show];
        }
        break;
        case 11:{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"Login/PasswordIncorrect", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"TryAgain", nil), NSLocalizedString(@"ChangeHost", nil), nil];
            
            [alert show];
        }
        break;
        default:
            break;
    }
}

- (IBAction)loginClick:(id)sender {
    DeskAlertServer *server = [DeskAlertServer defaultServer];
    
    if(![server checkInternetConnection:YES])
        return;
    
    int __block success;
    [self.progressBar showAnimated:YES whileExecutingBlock:^{
        dispatch_sync(dispatch_get_main_queue(), ^{
            success = [server validateOrRegisterUserWithLogin:self.loginField.text andPassword:self.passwordField.text onServerURL: [UserSettings sharedInstance].serverURL andDomain:self.domainField.text];
            [self showErrorMessageForUser:success];
        });
    }];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if(buttonIndex == 1)
    {
        FirstWelcomeView *view = [self.storyboard instantiateViewControllerWithIdentifier:@"hostNameView"];
        [self presentViewController:view animated:YES completion:nil];
    }
}

- (IBAction)passwordChanged:(id)sender {
    
     self.loginButton.enabled = [self.loginField.text length ] > 0 && [self.passwordField.text length] > 0;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.progressBar = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:self.progressBar];
    self.progressBar.delegate = self;
    self.progressBar.labelText = NSLocalizedString(@"Proc", nil);
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
    
    // Add action for hiding keyboaard if tap to fre space.
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
}

-(void)keyboardWasShown:(NSNotification*) nNotification{
    
    [scrollView setContentOffset:CGPointMake(0.0, domainField.frame.origin.y - passwordField.frame.origin.y) animated:YES];
    
}
-(void)keyboardWillBeHidden:(NSNotification*)aNotification{
    UIEdgeInsets contentInsents = UIEdgeInsetsZero;
    scrollView.contentInset = contentInsents;
    scrollView.scrollIndicatorInsets = contentInsents;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// Hide keyboard if tap to free space and scrolling go back.
-(void)dismissKeyboard{
    [loginField resignFirstResponder];
    [passwordField resignFirstResponder];
    [domainField resignFirstResponder];
    [scrollView setContentOffset:CGPointMake(0.0, domainField.frame.origin.y-domainField.frame.origin.y) animated:YES];
}

-(void) dialloc{
    [[NSNotificationCenter defaultCenter] removeObserver:nil];
}

// Added action by button "return" if taping to button, then the keyboard will be hidden and scrolling go to back
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    [scrollView setContentOffset:CGPointMake(0.0, domainField.frame.origin.y-domainField.frame.origin.y) animated:YES];
    return YES;
}

- (NSUInteger) supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
