//
//  DatabaseManager.h
//  Deskalerts
//
//  Created by Ярослав Долговых on 26/12/2019.
//  Copyright © 2019 Deskalert. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Alert.h"

NS_ASSUME_NONNULL_BEGIN

@interface DatabaseManager : NSObject

+ (void) alertProcessingAfterClose:(Alert*) alert;

@end

NS_ASSUME_NONNULL_END
