//
//  DatabaseManager.m
//  Deskalerts
//
//  Created by Ярослав Долговых on 26/12/2019.
//  Copyright © 2019 Deskalert. All rights reserved.
//

#import "DatabaseManager.h"
#import "AppDelegate.h"

@implementation DatabaseManager

+ (void) alertProcessingAfterClose:(Alert*) alert {
    NSManagedObjectContext* context = [AppDelegate sharedInstance].managedObjectContext;
    
    if (alert.isSelfDestructed) {
        NSString *predicateFormat = @"alertId == %@";
        NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFormat, alert.alertId];
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Alert"];
        
        [fetchRequest setPredicate:predicate];
        
        NSError* err = nil;
        NSArray* result = [context executeFetchRequest:fetchRequest error:&err];
        
        if (result.firstObject != nil) {
            Alert* alert = result.firstObject;
            if (alert.isSelfDestructed.boolValue == YES) {
                [context deleteObject:alert];
                [context save:nil];
            }
        }
    }
}
@end
