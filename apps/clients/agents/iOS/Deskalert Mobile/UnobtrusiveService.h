//
//  UnobtrusiveService.h
//  Deskalerts Mobile
//
//  Created by Дмитрий Кайгородов on 05/04/16.
//  Copyright © 2016 Deskalert. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Unobtrusive.h"
#import "UnobtrusiveNetworkController.h"

@interface UnobtrusiveService : NSObject


@property NSMutableArray* collection;

- (BOOL) addUnobtrusive:(Unobtrusive*) u;
- (BOOL) deleteUnobtrusive:(Unobtrusive*) u;
- (BOOL) switchUnobtrusive:(Unobtrusive*) u;
- (Unobtrusive*) unobtrusiveById:(int) uId;
- (NSArray*) unobtrusives;


@end
