//
//  DeskAlertsTabBarViewController.m
//  Deskalerts Mobile
//
//  Created by Dmitrii on 21/12/15.
//  Copyright © 2015 Deskalert. All rights reserved.
//

#import "DeskAlertsTabBarViewController.h"

@interface DeskAlertsTabBarViewController ()

@end

@implementation DeskAlertsTabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    BOOL isDemo = [[DeskAlertServer defaultServer] isDemo];
    NSMutableArray *viewControllers = [self.viewControllers mutableCopy];
    if(isDemo)
    {
      //  self.moreNavigationController.navigationItem.
        UIViewController* moreViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"navVIew"];
        moreViewController.tabBarItem.title = @"More";
        moreViewController.tabBarItem.image = [UIImage imageNamed:@"more"];
        [viewControllers addObject:moreViewController];
        self.viewControllers = viewControllers;
    }
    else
    {
        SettingsView* settingsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingsView"];
        settingsViewController.tabBarItem.title = @"Settings";
        settingsViewController.tabBarItem.image = [UIImage imageNamed:@"Settings"];
        settingsViewController.isDemo = NO;
        
        [viewControllers addObject:settingsViewController];
        self.viewControllers = viewControllers;
    }
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSInteger tabNum = [[[NSUserDefaults standardUserDefaults] valueForKey:@"tab"] integerValue];
    
   if(tabNum >= 0)
    [self setSelectedIndex:tabNum];
}






- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
