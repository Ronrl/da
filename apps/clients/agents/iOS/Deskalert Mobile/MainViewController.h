//
//  MainViewController.h
//  Deskalert Mobile
//
//  Created by Дмитрий Кайгородов on 11.03.15.
//  Copyright (c) 2015 Deskalert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "SearchResultController.h"
#import "AlertViewCell.h"
#import "Alert.h"
#import "UserSettings.h"
#import "DeskAlertServer.h"
#import "AlertDetailsView.h"

@interface MainViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate, UIAlertViewDelegate, UISearchControllerDelegate, UISearchResultsUpdating>
{
    BOOL validated;
}
@property IBInspectable NSString* contentType;
@property (weak, nonatomic) IBOutlet UINavigationBar *navBar;
@property (weak, readonly) NSManagedObjectContext* managedObjectContext;
@property (weak, nonatomic) IBOutlet UITableView *alertTable;
@property (strong, nonatomic) NSArray* filteredArray;
@property (strong, nonatomic) NSFetchRequest* searchRequest;
@property (strong, nonatomic) UIRefreshControl* refreshControl;
@property int tabToOpen;



- (void) alertRecieved:(NSNotification*) notification;
- (void) deleteAlerts: (NSNotification*) notification;
- (void) showAlertWithIdIfExists:(NSInteger) alertId;

@end


