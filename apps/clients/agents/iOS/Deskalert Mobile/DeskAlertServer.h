//
//  DeskAlertServer.h
//  Deskalert Mobile
//
//  Created by Дмитрий Кайгородов on 12.03.15.
//  Copyright (c) 2015 Deskalert. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef struct {
    BOOL urlValid;
    NSError* error;
} ValidationData;

@interface DeskAlertServer : NSObject

 

+(instancetype) alloc __attribute__((unavailable("alloc not available, call sharedInstance instead")));
-(instancetype) init __attribute__((unavailable("init not available, call sharedInstance instead")));
+(instancetype) new __attribute__((unavailable("new not available, call sharedInstance instead")));

- (int) validateOrRegisterUserWithLogin:(NSString *)login andPassword:(NSString *)password onServerURL:(NSURL *)serverURL andDomain:(NSString*) domain;

- (int) detectValueOfError;

- (void) registerDeviceWithToken:(NSData*)token;
- (void) unregisterDevice;
+ (instancetype) defaultServer;
- (void) retrieveAlerts:(BOOL) needToDisplay andAlertId: (NSInteger) alertId fromSource:(NSURL*) url andFirstPressedAlertId: (NSInteger) firstPressedAlertId;
- (void) parseAllRelevantAlerts:(BOOL) needToDisplay andAlertId: (NSInteger) alertId andFirstPressedAlertId:(NSInteger) firstPressedAlertId;
//- (void) synchronizeHistory:(BOOL) needToDisplay andAlertId: (NSInteger) alertId;
- (BOOL) checkInternetConnection:(BOOL) showAlertIfNotAvailable;
- (void) deleteAlertWithId: (NSInteger) alertId forUser: (NSString*) userName;
- (BOOL) isDemo;

-(ValidationData) validateServer: (NSURL *)url;
@end
