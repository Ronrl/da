//
//  DeskAlertsTabBarViewController.h
//  Deskalerts Mobile
//
//  Created by Dmitrii on 21/12/15.
//  Copyright © 2015 Deskalert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SettingsView.h"
#import "DeskAlertServer.h"


@interface DeskAlertsTabBarViewController : UITabBarController


@end
