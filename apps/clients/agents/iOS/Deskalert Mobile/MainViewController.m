//
//  MainViewController.m
//  Deskalert Mobile
//
//  Created by Дмитрий Кайгородов on 11.03.15.
//  Copyright (c) 2015 Deskalert. All rights reserved.
//

#import "MainViewController.h"
#import "UIViewController+MaryPopin.h"

//#import "AppDelegate.h"

@interface MainViewController () <NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property UISearchController* searchController;

@end

@implementation MainViewController


- (void) viewWillAppear:(BOOL)animated
{

    [super viewWillAppear:animated];
   // [self validateUserAsync:NO];
    [self loadAlertsForCurrentUser];
   // [self validateUserAsync:NO];
    self.alertTable.contentSize = CGSizeMake(self.alertTable.frame.size.width, self.alertTable.contentSize.height);

    
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    self.alertTable.contentSize = CGSizeMake(self.alertTable.frame.size.width, self.alertTable.contentSize.height);
}


- (void) viewDidDisappear:(BOOL)animated
{
  //  [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super viewDidDisappear:animated];
 
    

}



- (void) showAlertWithIdIfExists:(NSInteger)alertId
{
    NSString *predicateFormat = @"alertId == %i";


    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFormat, alertId];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Alert"];

    [fetchRequest setPredicate:predicate];

    NSError* err = nil;
    NSArray* result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&err];


    if([result count] == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"AlertFail", nil) delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];

        [alert show];
        return;
    }
    Alert* alert = [result objectAtIndex:0];

    if(alert != nil)
    {

        AlertDetailsView *adv = [self.storyboard instantiateViewControllerWithIdentifier:@"details"];
        adv.alert = alert;
        [alert setValue:@0 forKey:@"unread"];
        [self.managedObjectContext save:nil];


        UIViewController* topController = [UIApplication sharedApplication].keyWindow.rootViewController;

        if(topController.presentedViewController)
            topController = topController.presentedViewController;

        [topController presentViewController:adv animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"AlertFail", nil)  delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];

        [alert show];
    }
}

-(void) viewDidAppear:(BOOL)animated
{
   
    NSNumber* tabNum = [[NSUserDefaults standardUserDefaults] valueForKey:@"tab"];
    
    if(![tabNum isEqualToNumber:@-1])
    {
        [self.tabBarController setSelectedIndex:[tabNum integerValue]];
        [[NSUserDefaults standardUserDefaults] setValue:@-1 forKey:@"tab"];
    }
    
    [super viewDidAppear:animated];
    self.alertTable.allowsMultipleSelectionDuringEditing = NO;
    self.alertTable.allowsMultipleSelection = NO;
  
    self.alertTable.contentInset = UIEdgeInsetsMake(0, 0, 120, 0);
    [self.alertTable reloadData];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath

{
    return 55;
    
}



- (void)viewDidLoad {
    
    [super viewDidLoad];

    NSString* searchControllerId = @"ResultController";
    if([self.contentType isEqualToString:@"all"])
    {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(alertRecieved:) name:@"recieveAlert" object:nil];
        searchControllerId = @"ResultControllerAll";
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deleteAlerts:) name:@"deleteAlerts" object:nil];
    
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showAlert:) name:@"showAlert" object:nil];
        
    
    UINavigationController *navController = [[self storyboard] instantiateViewControllerWithIdentifier:searchControllerId];
    
    ((SearchResultController*)navController).managedObjectContext = self.managedObjectContext;
    
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:navController];
    self.searchController.searchResultsUpdater = self;
    
    self.searchController.searchBar.frame = CGRectMake(self.searchController.searchBar.frame.origin.x, self.searchController.searchBar.frame.origin.y, self.searchController.searchBar.frame.size.width, 44.0);
    
    self.searchController.searchBar.tintColor = [UIColor colorWithRed:77/255.0 green:174/255.0 blue:74/255.0 alpha:1.0];
    
    [[UIBarButtonItem appearanceWhenContainedInInstancesOfClasses:[NSArray arrayWithObject:[UISearchBar class]]] setTitle:@"Cancel"];
    
    self.searchController.hidesNavigationBarDuringPresentation = YES;
 
    self.alertTable.tableHeaderView = self.searchController.searchBar;

    self.refreshControl = [[UIRefreshControl alloc] init];

    [self.refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.alertTable addSubview:self.refreshControl];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(closeOpenedAlerts) name:@"closeOpenedAlerts" object:nil];
}

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"closeOpenedAlerts" object:nil];
}

- (void) closeOpenedAlerts
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) refresh:(id) sender
{
    UIRefreshControl* rc = (UIRefreshControl*)sender;
    [[DeskAlertServer defaultServer] parseAllRelevantAlerts:NO andAlertId:-1 andFirstPressedAlertId:-1];
    [rc endRefreshing];
}

- (void) updateSearchResultsForSearchController:(UISearchController *)searchController
{
    NSString* searchText = searchController.searchBar.text;
    

    NSArray* result = [self searchForText:searchText];
    
    if(result)
    {
        SearchResultController *src = (SearchResultController*) self.searchController.searchResultsController;
        src.result = [result mutableCopy];
        
        [src.resultTable reloadData];
    }
}

-(void) showAlert:(NSNotification*) aNotification
{
    NSInteger alertId = [aNotification.object integerValue];
}

-(NSFetchRequest*) searchRequest
{
    if (_searchRequest != nil)
    {
        return _searchRequest;
    }
    
    _searchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Alert" inManagedObjectContext:self.managedObjectContext];
    [_searchRequest setEntity:entity];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"title" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObjects:sortDescriptor, nil];
    [_searchRequest setSortDescriptors:sortDescriptors];
    
    return _searchRequest;
}

- (NSMutableArray*) searchForText:(NSString *)searchText
{
    if (self.managedObjectContext)
    {
        NSString *predicateFormat = @"title BEGINSWITH[cd] %@ and username like[c] %@";
        
        
        if([self.contentType isEqualToString:@"alerts"])
        {
            predicateFormat = [predicateFormat stringByAppendingString:@" and survey = 0"];
        }
        else if([self.contentType isEqualToString:@"surveys"])
        {
            predicateFormat = [predicateFormat stringByAppendingString:@" and survey = 1"];
        }

        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFormat, searchText, [UserSettings sharedInstance].login];
        [self.searchRequest setPredicate:predicate];
        
        NSError *error = nil;
        return [[self.managedObjectContext executeFetchRequest:self.searchRequest error:&error] mutableCopy];
        
        
    }
    return nil;
}



- (void) validateUserAsync:(BOOL)resetValidation
{
    if(resetValidation)
        validated = YES;
    
    if(!validated)
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            int ok = [[DeskAlertServer defaultServer] validateOrRegisterUserWithLogin:[UserSettings sharedInstance].login andPassword:[UserSettings sharedInstance].password onServerURL:[UserSettings sharedInstance].serverURL andDomain:[UserSettings sharedInstance].domain];
            
            validated = ok;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if(validated)
                {
                    
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection error" message:@"Can not login into your account. Probably, it has been deleted." delegate:self cancelButtonTitle:@"Login from another account." otherButtonTitles: nil];
                    
                    [alert show];
                }
                
            });
            
        });
    
    }
}

- (void) loadAlertsForCurrentUser
{
    
    NSLog(@"Loading data for user %@", [UserSettings sharedInstance].login);
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Alert"];
    NSSortDescriptor *sd = [NSSortDescriptor sortDescriptorWithKey:@"recivedDate" ascending:NO];
    [fetchRequest setSortDescriptors:@[sd]];
    NSString* predicateString = @"username like[c] %@";
    
    if([self.contentType isEqualToString:@"alerts"])
    {
        predicateString = [predicateString stringByAppendingString:@" AND survey = 0"];
    }
    else if([self.contentType isEqualToString:@"surveys"])
    {
        predicateString = [predicateString stringByAppendingString:@" AND survey = 1"];
    }
    NSPredicate* filter = [NSPredicate predicateWithFormat:predicateString, [UserSettings sharedInstance].login];
    [fetchRequest setPredicate:filter];
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    

    [self.fetchedResultsController setDelegate:self];
    
    NSError* error = nil;
    
    
    [self.fetchedResultsController performFetch:&error];
    

    if(error)
    {
        NSLog(@"Fetch failed");
    }
    

}


- (NSArray*) tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath
{
 
   
    UITableViewRowAction *button2 = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:NSLocalizedString(@"Delete", nil) handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                     {
                                         Alert* alertToDelete = [self.fetchedResultsController objectAtIndexPath:indexPath];
                                         NSInteger alertId = [[alertToDelete alertId] integerValue];
                                         [self.managedObjectContext deleteObject:alertToDelete];
                                         NSError *error = nil;
                                         [self.managedObjectContext save:&error];
                                         [[DeskAlertServer defaultServer] deleteAlertWithId:alertId forUser:[UserSettings sharedInstance].login];
                                     }];
 //   button2.backgroundColor = [UIColor blueColor]; //arbitrary color
     button2.backgroundColor = [UIColor redColor];//[UIColor colorWithRed:77/255.0 green:174/255.0 blue:74/255.0 alpha:1.0];
    
    return @[button2];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == alertView.cancelButtonIndex && ![[alertView title] isEqualToString:@"Error"])
    {
       // [self performSegueWithIdentifier:@"SettingsSegue" sender:self];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        UIViewController *settingsView = [storyboard instantiateViewControllerWithIdentifier:@"SettingsView"];
        
        
        [self presentViewController:settingsView animated:YES completion:nil];

    }
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
     //   NSLog(@"%ld", indexPath.row);
        Alert* alertToDelete = [self.fetchedResultsController objectAtIndexPath:indexPath];
        [self.managedObjectContext deleteObject:alertToDelete];
        NSError *error = nil;
        [self.managedObjectContext save:&error];
      
      //  [self.alertTable deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath ]withRowAnimation:UITableViewRowAnimationFade];
        
    }
}

-(BOOL) tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    
 
        if( type ==  NSFetchedResultsChangeInsert) {
            [self.alertTable insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];

        }
       else if( type == NSFetchedResultsChangeDelete)  {
            [self.alertTable deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];

        }
    
     // [self.alertTable reloadData];

    
}



- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id )sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    

            
         if( type ==  NSFetchedResultsChangeInsert) {
            [self.alertTable insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            
         }
            
        else if( type ==  NSFetchedResultsChangeInsert) {
            [self.alertTable deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];

        }
    
}


- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    NSLog(@"SECTIONS = %ld", [[self.fetchedResultsController sections] count]);
    return  [[self.fetchedResultsController sections] count] ;
    //return  1;
}

- (void) controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.alertTable beginUpdates];
}

- (void) controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.alertTable endUpdates];
}
- (NSManagedObjectContext*) managedObjectContext
{
    return [AppDelegate sharedInstance].managedObjectContext;
}



- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray* sections = [self.fetchedResultsController sections];
    id<NSFetchedResultsSectionInfo> sectionInfo = [sections objectAtIndex:section];
    
        NSLog(@"ROWS = %ld", [sectionInfo numberOfObjects]);
    return [sectionInfo numberOfObjects];
}


- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
 
    AlertViewCell* cell = (AlertViewCell*) [tableView dequeueReusableCellWithIdentifier:@"AlertCell" forIndexPath:indexPath];
    
    NSManagedObject *record = [self.fetchedResultsController objectAtIndexPath:indexPath];
    NSString *title = [record valueForKey:@"title"];
    [cell.titleLabel setText:title];
    
    NSDate *creationDate =  [record valueForKey:@"recivedDate"];
    NSDateFormatter *formater = [[NSDateFormatter alloc] init];
    [formater setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
    NSString* dateString = [formater stringFromDate:creationDate];
    [cell.dateLabel setText:dateString];
    
    if(cell.type)
    {
        NSNumber* survey = [record valueForKey:@"survey"];
        NSString* imageName = [survey isEqualToNumber:@0] ? @"alerts" : @"Survey";
        
        cell.type.image = [UIImage imageNamed:imageName];
    }
    BOOL unread = [[record valueForKey:@"unread"] boolValue];
    cell.unreadIndicator.image = unread ? [UIImage imageNamed:@"thread_new-16.png"] : nil;
    
    return cell;
    
}

- (void) deleteAlerts: (NSNotification*) notification
{
    NSString* alertIds = notification.object;
    
    NSArray* alertsIdsArray = [alertIds componentsSeparatedByString:@","];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Alert" inManagedObjectContext:self.managedObjectContext];
    
    for(NSString* alertId in alertsIdsArray)
    {
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"alertId = %@", alertId];
        request.predicate = predicate;
        request.entity = entity;
        NSError *error = nil;
        NSArray *objs = [self.managedObjectContext executeFetchRequest:request error:&error];
        
        if([objs count]> 0)
        {
            NSManagedObject* alert = [objs objectAtIndex:0];
            [self.managedObjectContext deleteObject:alert];
            [self.managedObjectContext save:&error];
        }
    }
    

    
}

- (void) alertRecieved: (NSNotification*) notification
{
    NSDictionary* alertInfo = notification.object;
    
    NSString* title = [alertInfo objectForKey:@"title"];
    NSDate* date = [alertInfo objectForKey:@"createdDate"];
    NSString* alertBody = [alertInfo objectForKey:@"data"];
    NSString* skinPath = [alertInfo objectForKey:@"skinpath"];
    NSDictionary* margins = [alertInfo objectForKey:@"margins"];
    NSString* titleHtml = [alertInfo objectForKey:@"htmlTitle"];
    NSNumber* alertId = [alertInfo objectForKey:@"id"];
    NSNumber* survey = [alertInfo objectForKey:@"survey"];
    NSNumber *isAcknown = [alertInfo objectForKey:@"isAcknown"];
    NSNumber *firstAlertPresseId = [alertInfo objectForKey:@"firstPressedAlertId"];
    NSNumber* isSelfDestructed = [alertInfo objectForKey:@"isSelfDestructed"];
    
    [self addAlertWithTitle:title thatHasMessage:alertBody andCreatedAt:date andWithSkin:skinPath andMargins:margins andHtmlTitle:titleHtml andId:[alertId integerValue] andSurveyFlag:survey isAcknown: isAcknown isSelfDestructed: isSelfDestructed];
    
    [[UserSettings sharedInstance] changeLastAlertValueIfNecessary:[alertId integerValue]];
    BOOL needToDisplay = NO;
    if (firstAlertPresseId == alertId){
        needToDisplay = [[alertInfo objectForKey:@"needToDisplay"] boolValue];
    }
    
    if(needToDisplay)
    {
        [UIApplication sharedApplication].applicationIconBadgeNumber--;
        [self showAlertWithIdIfExists:[alertId integerValue]];
    }
    

}

- (void) addAlertWithTitle:(NSString*) title thatHasMessage:(NSString*) msg andCreatedAt: (NSDate*) date andWithSkin:(NSString*) skinpath andMargins:(NSDictionary*) margins andHtmlTitle:(NSString*) titleHtml andId:(NSInteger) alertId andSurveyFlag:(NSNumber*) survey isAcknown:(NSNumber *) isAcknown isSelfDestructed: (NSNumber *) isSelfDestructed
{
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Alert" inManagedObjectContext:self.managedObjectContext];

    
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"alertId = %d", alertId];
    request.predicate = predicate;
    request.entity = entity;
    NSError *error = nil;
    NSArray *objs = [self.managedObjectContext executeFetchRequest:request error:&error];
    
    if(error || [objs count] > 0)
        return;
    
    
    
    NSManagedObject *alertToAdd = [[NSManagedObject alloc] initWithEntity:entity insertIntoManagedObjectContext:self.managedObjectContext];
    
    [alertToAdd setValue:title forKey:@"title"];
    [alertToAdd setValue:msg forKey:@"message"];
    [alertToAdd setValue:date forKey:@"recivedDate"];
    [alertToAdd setValue:skinpath forKey:@"skinPath"];
    [alertToAdd setValue:[margins objectForKey:@"top"] forKey:@"topMargin"];
    [alertToAdd setValue:[margins objectForKey:@"left"] forKey:@"leftMargin"];
    [alertToAdd setValue:[margins objectForKey:@"bottom"] forKey:@"bottomMargin"];
    [alertToAdd setValue:[margins objectForKey:@"right"] forKey:@"rightMargin"];
    [alertToAdd setValue:[UserSettings sharedInstance].login forKey:@"username"];
    [alertToAdd setValue:@1 forKey:@"unread"];
    [alertToAdd setValue:titleHtml forKey:@"htmlTitle"];
    [alertToAdd setValue:[NSNumber numberWithInteger:alertId] forKey:@"alertId"];
    [alertToAdd setValue:survey forKey:@"survey"];
    [alertToAdd setValue:isAcknown forKey:@"isAcknown"];
    [alertToAdd setValue:isSelfDestructed forKey:@"isSelfDestructed"];
    
    NSError* err = nil;
    
    if(![self.managedObjectContext save:&err])
    {
       if(err)
       {
           NSLog(@"Can not save alert to DB");
       }
    }
}

- (IBAction)unwindToMainView :(UIStoryboardSegue*)sender
{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
   [self performSegueWithIdentifier:@"alertDetailsSegue" sender:self];
}

- (NSUInteger) supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void) presentAlertPopup
{
    AlertDetailsView* adv = [self.storyboard instantiateViewControllerWithIdentifier:@"details"];
    NSIndexPath* indexPath = [self.alertTable indexPathForSelectedRow];
    Alert* selectedAlert = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    if([[selectedAlert valueForKey:@"unread" ] boolValue])
    {
        [UIApplication sharedApplication].applicationIconBadgeNumber--;
        [selectedAlert setValue:@0 forKey:@"unread"];
        [self.managedObjectContext save:nil];
    }
    adv.alert = selectedAlert;
    int tabNum = (int)[self.tabBarController selectedIndex];
    [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithInt:tabNum] forKey:@"tab"];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"alertDetailsSegue"]) {
        
        AlertDetailsView* adv = segue.destinationViewController;
        NSIndexPath* indexPath = [self.alertTable indexPathForSelectedRow];
        Alert* selectedAlert = [self.fetchedResultsController objectAtIndexPath:indexPath];
        
        if([[selectedAlert valueForKey:@"unread" ] boolValue])
        {
            [UIApplication sharedApplication].applicationIconBadgeNumber--;
            [selectedAlert setValue:@0 forKey:@"unread"];
            [self.managedObjectContext save:nil];
        }
        adv.alert = selectedAlert;
       // adv.returnTab = (int)[self.tabBarController selectedIndex];
        int tabNum = (int)[self.tabBarController selectedIndex];
        [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithInt:tabNum] forKey:@"tab"];
        [self.alertTable reloadData];
        
    }
}


@end
