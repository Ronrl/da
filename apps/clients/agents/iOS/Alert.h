//
//  Alert.h
//  Deskalert Mobile
//
//  Created by Дмитрий Кайгородов on 04.04.15.
//  Copyright (c) 2015 Deskalert. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Alert : NSManagedObject

@property (nonatomic, retain) NSNumber * bottomMargin;
@property (nonatomic, retain) NSNumber * leftMargin;
@property (nonatomic, retain) NSString * message;
@property (nonatomic, retain) NSDate * recivedDate;
@property (nonatomic, retain) NSNumber * rightMargin;
@property (nonatomic, retain) NSString * skinPath;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSNumber * topMargin;
@property (nonatomic, retain) NSString * username;
@property (nonatomic, retain) NSNumber * unread;
@property (nonatomic, retain) NSString * htmlTitle;
@property (nonatomic, retain) NSString * alertId;
@property (nonatomic, retain) NSNumber * survey;
@property (nonatomic, retain) NSNumber * isAcknown;
@property (nonatomic, retain) NSNumber * isPrint;
@property (nonatomic, retain) NSNumber * isSelfDestructed;

@end
