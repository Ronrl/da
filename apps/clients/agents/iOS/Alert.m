//
//  Alert.m
//  Deskalert Mobile
//
//  Created by Дмитрий Кайгородов on 04.04.15.
//  Copyright (c) 2015 Deskalert. All rights reserved.
//

#import "Alert.h"


@implementation Alert

@dynamic bottomMargin;
@dynamic leftMargin;
@dynamic message;
@dynamic recivedDate;
@dynamic rightMargin;
@dynamic skinPath;
@dynamic title;
@dynamic topMargin;
@dynamic username;
@dynamic unread;
@dynamic htmlTitle;
@dynamic alertId;
@dynamic survey;
@dynamic isAcknown;
@dynamic isPrint;
@dynamic isSelfDestructed;
@end
