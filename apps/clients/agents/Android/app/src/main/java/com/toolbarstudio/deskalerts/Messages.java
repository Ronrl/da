package com.toolbarstudio.deskalerts;

import java.util.Iterator;
import java.util.LinkedList;

/**
 * Created by brazil on 27.03.2016.
 */
public class Messages {
    private LinkedList<Notification> full_list;

    public Messages(LinkedList<Notification> list) {
        full_list = list;
    }

    public static LinkedList<Notification> filterSurveysNotifications(LinkedList<Notification> list) {
        LinkedList<Notification> result = new LinkedList<>();
        for (Notification n : list) {
            if (n.survey_flag) {
                result.add(n);
            }
        }
        return result;
    }

    public static LinkedList<Notification> filterUrgentNotifications(LinkedList<Notification> list) {
        LinkedList<Notification> result = new LinkedList<>();
        for (Notification n : list) {
            if (n.urgent_flag) {
                result.add(n);
            }
        }
        return result;
    }

    public static LinkedList<Notification> filterFavoriteNotifications(LinkedList<Notification> list) {
        LinkedList<Notification> result = new LinkedList<>();
        for (Notification n : list) {
            if (n.favorite_flag) {
                result.add(n);
            }
        }
        return result;
    }

    public static LinkedList<Notification> filterAlertNotifications(LinkedList<Notification> list) {
        LinkedList<Notification> result = new LinkedList<>();
        for (Notification n : list) {
            if (!n.survey_flag) {
                result.add(n);
            }
        }
        return result;
    }

    public static LinkedList<Notification> filterNotReadNotifications(LinkedList<Notification> list) {
        LinkedList<Notification> result = new LinkedList<>();
        for (Notification n : list) {
            if (!n.read_flag) {
                result.add(n);
            }
        }
        return result;
    }

    public void add(Notification notification) {
        full_list.add(0, notification);
    }

    public void add(LinkedList<Notification> notifications) {
        full_list.addAll(0, notifications);
    }

    public void delete(Notification notification) {
        for(Iterator<Notification> it = full_list.iterator(); it.hasNext(); ) {
            if(it.next().id_db==notification.id_db) {
                it.remove();
                break;
            }
        }
    }

    public LinkedList<Notification> getAll() {
        return full_list;
    }

    public LinkedList<Notification> getAlertsOnly() {
        LinkedList<Notification> result = new LinkedList<>();
        for (Notification notification : full_list) {
            if (!notification.survey_flag) {
                result.add(notification);
            }
        }
        return result;
    }

    public LinkedList<Notification> getSurveysOnly() {
        LinkedList<Notification> result = new LinkedList<>();
        for (Notification notification : full_list) {
            if (notification.survey_flag) {
                result.add(notification);
            }
        }
        return result;
    }
}
