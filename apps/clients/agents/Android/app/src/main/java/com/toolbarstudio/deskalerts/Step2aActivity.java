package com.toolbarstudio.deskalerts;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by brazil on 28.03.2016.
 */
public class Step2aActivity extends BaseStepActivity {

    private Activity thisActivity;
    private EditText user_name_te;
    private EditText password_te;
    private EditText domain_te;
    private Settings settings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.step2a_activity);
        settings = Settings.get(this);

        thisActivity = this;

        user_name_te = findViewById(R.id.login_field);
        user_name_te.setText(settings.user_name);
        password_te = findViewById(R.id.password_field);
        password_te.setText("");
        domain_te = findViewById(R.id.domain_field);
        domain_te.setText(settings.domain);

        View next_button = findViewById(R.id.next_button);
        next_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settings.generateGuid(true);
                if (isNetworkOnline()) {
                    if (user_name_te.getText().toString().equals("")) {
                        Toast toast = Toast.makeText(getApplicationContext(),
                                "" + thisActivity.getResources().getString(R.string.steps_toast_fill_login), Toast.LENGTH_SHORT);
                        toast.show();
                        return;
                    }
                    if (password_te.getText().toString().equals("")) {
                        Toast toast = Toast.makeText(getApplicationContext(),
                                "" + thisActivity.getResources().getString(R.string.steps_toast_fill_password), Toast.LENGTH_SHORT);
                        toast.show();
                        return;
                    }
                    settings.tmp_user_name = user_name_te.getText().toString();
                    settings.tmp_password = password_te.getText().toString();
                    settings.tmp_domain = domain_te.getText().toString();

                    Intent intent = new Intent(thisActivity, Step3Activity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(thisActivity, R.string.bad_network_connection, Toast.LENGTH_LONG).show();
                }
            }
        });

        View back_button = findViewById(R.id.back_button);
        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        View sign_up_button = findViewById(R.id.sign_up_button);
        sign_up_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(thisActivity, Step2bActivity.class);
                startActivity(intent);
            }
        });
    }

    private boolean isNetworkOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}
