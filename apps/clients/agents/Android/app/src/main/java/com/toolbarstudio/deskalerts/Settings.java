package com.toolbarstudio.deskalerts;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.UUID;

/**
 * Created by brazil on 27.03.2016.
 */
public class Settings {

    private volatile static Settings INSTANCE;

    String host;
    String user_name;
    String domain;
    String guid;
    private String deleted_alerts;
    private long last_alert_id;
    String key;

    String tmp_host;
    String tmp_user_name;
    String tmp_guid;

    String tmp_password;
    String tmp_domain;

    private Settings(Context context){
        load(context);
    }

    public static Settings get(Context context){

        if(INSTANCE == null){
            synchronized (Settings.class){
                if(INSTANCE == null){
                    INSTANCE = new Settings(context);
                }
            }
        }
        return INSTANCE;
    }

    public void save(Context context) {
        host = tmp_host;
        user_name = tmp_user_name;
        domain = tmp_domain;
        guid = tmp_guid;
        last_alert_id = 0;
        deleted_alerts = "";

        tmp_host = "";
        tmp_user_name = "";
        tmp_guid = "";
        tmp_password = "";
        tmp_domain="";

        SharedPreferences preferences = context.getSharedPreferences("preferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("host", host);
        editor.putString("user_name", user_name);
        editor.putString("domain", domain);
        editor.putString("guid", guid);
        editor.putLong("last_alert_id", last_alert_id);
        editor.putString("deleted_alerts", deleted_alerts);
        editor.putString("encryption_key", key);
        editor.commit();
    }

    private void load(Context context) {
        SharedPreferences preferences = context.getSharedPreferences("preferences", Context.MODE_PRIVATE);
        host = preferences.getString("host", "demo.deskalerts.com");
        user_name = preferences.getString("user_name", "");
        domain = preferences.getString("domain", "");
        guid = preferences.getString("guid", "");
        last_alert_id = preferences.getLong("last_alert_id", 0);
        deleted_alerts = preferences.getString("deleted_alerts", "");
        key = preferences.getString("encryption_key", "");
        boolean loaded = true;
    }

    public void saveLastAlertId(Context context, long lastAlertId) {
        last_alert_id = lastAlertId;
        SharedPreferences preferences = context.getSharedPreferences("preferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong("last_alert_id", lastAlertId);
        editor.commit();
    }

    public void saveDeletedAlerts(Context context, String deletedAlerts) {
        deleted_alerts = deletedAlerts;
        SharedPreferences preferences = context.getSharedPreferences("preferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("deleted_alerts", deletedAlerts);
        editor.commit();
    }

    public void saveEncryptionKey(Context context, String encryptionKey) {
        key = encryptionKey;
        SharedPreferences preferences = context.getSharedPreferences("preferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("encryption_key", key);
        editor.commit();
    }

    public String generateGuid(boolean isForce) {
        if (guid == null || guid.equals("") || isForce) {
            tmp_guid = UUID.randomUUID().toString().toUpperCase();
        } else {
            tmp_guid = guid;
        }
        return tmp_guid;
    }
}
