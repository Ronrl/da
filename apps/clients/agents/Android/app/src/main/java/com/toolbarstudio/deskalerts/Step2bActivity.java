package com.toolbarstudio.deskalerts;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by brazil on 28.03.2016.
 */
public class Step2bActivity extends BaseStepActivity {

    private Activity thisActivity;

    private EditText user_name_te;
    private EditText password_te;
    private EditText password_again_te;
    private Settings settings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.step2b_activity);
        settings = Settings.get(this);

        thisActivity = this;

        user_name_te = findViewById(R.id.login_field);
        password_te = findViewById(R.id.password_field);
        password_again_te = findViewById(R.id.password_again_field);

        View next_button = findViewById(R.id.next_button);
        next_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settings.generateGuid(true);
                if (user_name_te.getText().toString().equals("")) {
                    Toast toast = Toast.makeText(getApplicationContext(),
                            "" + thisActivity.getResources().getString(R.string.steps_toast_fill_login), Toast.LENGTH_SHORT);
                    toast.show();
                    return;
                }
                if (password_te.getText().toString().equals("")) {
                    Toast toast = Toast.makeText(getApplicationContext(),
                            "" + thisActivity.getResources().getString(R.string.steps_toast_fill_password), Toast.LENGTH_SHORT);
                    toast.show();
                    return;
                }
                if (!password_te.getText().toString().equals(password_again_te.getText().toString())) {
                    Toast toast = Toast.makeText(getApplicationContext(),
                            "" + thisActivity.getResources().getString(R.string.steps_toast_equals_passwords), Toast.LENGTH_SHORT);
                    toast.show();
                    return;
                }
                settings.tmp_user_name = user_name_te.getText().toString();
                settings.tmp_password = password_te.getText().toString();


                Intent intent = new Intent(thisActivity, Step3Activity.class);
                startActivity(intent);
            }
        });
        View back_button = findViewById(R.id.back_button);
        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }

}
