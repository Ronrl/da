package com.toolbarstudio.deskalerts;

import android.content.Context;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.LinkedList;


/**
 * Created by brazil on 26.03.2016.
 */
public class MainFragmentPagerAdapter extends FragmentPagerAdapter {

    private Context context;
    private Messages messages;
    private Fragment fragment_all;
    private Fragment fragment_alerts;
    private Fragment fragment_survey;
    private Fragment fragment_more;
    private Handler handler;

    public MainFragmentPagerAdapter(FragmentManager fm, Context context, Messages messages) {
        super(fm);
        this.context = context;
        this.messages = messages;
        fragment_all = ListFragment.createListFragment(context, messages.getAll(), context.getResources().getString(R.string.page_title_all), this);
        fragment_alerts = ListFragment.createListFragment(context, messages.getAlertsOnly(), false, context.getResources().getString(R.string.page_title_alerts), this);
        fragment_survey = ListFragment.createListFragment(context, messages.getSurveysOnly(), false, context.getResources().getString(R.string.page_title_surveys), this);
        fragment_more = MoreFragment.createListFragment(context);
    }

    public void setHandler(Handler handler) {
        this.handler = handler;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return fragment_all;
            case 1:
                return fragment_alerts;
            case 2:
                return fragment_survey;
            case 3:
                return fragment_more;
            default:
                return ListFragment.createListFragment(context, null, context.getResources().getString(R.string.page_title_all), this);
        }
    }

    @Override
    public int getCount() {
        return 4;
    }

    public void addNotifications(LinkedList<Notification> notifications) {
        ((ListFragment) fragment_all).addNotifications(notifications);
        ((ListFragment) fragment_alerts).addNotifications(Messages.filterAlertNotifications(notifications));
        ((ListFragment) fragment_survey).addNotifications(Messages.filterSurveysNotifications(notifications));
    }

    public void deleteNotification(Notification notification) {
        ((ListFragment) fragment_all).deleteNotification(notification);
        ((ListFragment) fragment_alerts).deleteNotification(notification);
        ((ListFragment) fragment_survey).deleteNotification(notification);
    }

    public void refreshAllLists() {
        ((ListFragment) fragment_all).refresh();
        ((ListFragment) fragment_alerts).refresh();
        ((ListFragment) fragment_survey).refresh();
    }

    public void sendEmptyMessageToMainHandler(int i) {
        handler.sendEmptyMessage(i);
    }

    public void deleteFromMessages(Notification notification) {
        messages.delete(notification);
    }
}
