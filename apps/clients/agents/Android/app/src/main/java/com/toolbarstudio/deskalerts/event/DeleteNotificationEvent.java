package com.toolbarstudio.deskalerts.event;

import com.toolbarstudio.deskalerts.Notification;

public class DeleteNotificationEvent {

    private Notification notification;

    public DeleteNotificationEvent(Notification notification) {
        this.notification = notification;
    }

    public Notification getNotification() {
        return notification;
    }
}
