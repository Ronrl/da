package com.toolbarstudio.deskalerts;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.net.http.SslError;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.ConsoleMessage;
import android.webkit.JsResult;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by brazil on 28.03.2016.
 */
@SuppressLint("SetJavaScriptEnabled")
public class NotificationActivity extends AppCompatActivity {

    private Activity thisActivity;
    private WebView web_view;
    private WebView web_view_skin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notification_activity);
        final Settings settings = Settings.get(this);

        thisActivity = this;
        long notificationDbId = getIntent().getLongExtra("notificationDbId", 0);
        Log.d("Notification", "Open NotificationActivity for NotificationDbId=" + notificationDbId);
        final Notification notification = DBHelper.getNotificationByDbId(this, notificationDbId);
        if (notification == null) {
            Log.e("Notification", "Notification " + notificationDbId + "Doesn't exist");
            finish();
        }else {
            Button acknowledgementButton = findViewById(R.id.acknowledgement_button);
            if (notification.acknowledgementFlag) {
                acknowledgementButton.setVisibility(View.VISIBLE);
            }

            acknowledgementButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AjaxHelper.acknowledgeAlert(thisActivity.getApplicationContext(), settings.host, settings.user_name, notification, thisActivity);
                }
            });

            web_view = findViewById(R.id.web_view);
            web_view.getSettings().setJavaScriptEnabled(true);
            web_view.getSettings().setDomStorageEnabled(true);

            web_view.setWebChromeClient(new WebChromeClient() {
                @Override
                public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
                    Log.d("WEBVIEW", consoleMessage.message());
                    if (consoleMessage.message().contains("Uncaught ReferenceError") ||
                            consoleMessage.message().contains("Uncaught TypeError")) {
                        thisActivity.finish();
                    }
                    return super.onConsoleMessage(consoleMessage);
                }

                @Override
                public boolean onJsAlert(WebView view, String url, String message, final android.webkit.JsResult result)
                {
                    new AlertDialog.Builder(NotificationActivity.this)
                            .setMessage(message)
                            .setPositiveButton(android.R.string.ok,
                                    new AlertDialog.OnClickListener()
                                    {
                                        public void onClick(DialogInterface dialog, int wicht)
                                        {
                                            result.confirm();
                                        }
                                    }).setCancelable(false)
                            .create()
                            .show();
                    return true;
                }

                @Override
                public boolean onJsConfirm(WebView view, String url, String message, final JsResult result) {

                    AlertDialog dialog =new AlertDialog.Builder(view.getContext()).
                            setMessage(message).
                            setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    result.cancel();
                                }
                            }).
                            setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    result.confirm();
                                }
                            })
                            .create();
                    dialog.show();
                    return true;
                }
            });

            web_view.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    view.loadUrl(url);
                    return false;
                }

                @Override
                public void onPageFinished(WebView view, String url) {
                    if (notification.selfDestructingFlag) {
                        DBHelper.deleteForUser(thisActivity.getApplicationContext(), notification, settings.user_name);
                    }
                }
            });

            web_view_skin = findViewById(R.id.web_view_skin);
            web_view_skin.getSettings().setJavaScriptEnabled(true);
            web_view_skin.getSettings().setDomStorageEnabled(true);

            web_view_skin.setWebViewClient(new WebViewClient() {
                @Override
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(view, url);
                    web_view_skin.loadUrl("javascript:setHtmlTitle(\"" + notification.title + "\");");
                    web_view_skin.setVisibility(View.VISIBLE);
                }

                private List<Integer> getAllSslErrorMessageCodes(SslError error) {
                    List<Integer> errorCodeMessageCodes = new ArrayList<>(1);

                    if (error.hasError(SslError.SSL_DATE_INVALID)) {
                        errorCodeMessageCodes.add(R.string.message_certificate_date_invalid);
                    }
                    if (error.hasError(SslError.SSL_EXPIRED)) {
                        errorCodeMessageCodes.add(R.string.message_certificate_expired);
                    }
                    if (error.hasError(SslError.SSL_IDMISMATCH)) {
                        errorCodeMessageCodes.add(R.string.message_certificate_domain_mismatch);
                    }
                    if (error.hasError(SslError.SSL_NOTYETVALID)) {
                        errorCodeMessageCodes.add(R.string.message_certificate_not_yet_valid);
                    }
                    if (error.hasError(SslError.SSL_UNTRUSTED)) {
                        errorCodeMessageCodes.add(R.string.message_certificate_untrusted);
                    }
                    if (error.hasError(SslError.SSL_INVALID)) {
                        errorCodeMessageCodes.add(R.string.message_certificate_invalid);
                    }

                    return errorCodeMessageCodes;
                }

                @Override
                public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
                    List<Integer> errorCodeMessageCodes = getAllSslErrorMessageCodes(error);

                    StringBuilder stringBuilder = new StringBuilder();
                    for (Integer messageCode : errorCodeMessageCodes) {
                        stringBuilder.append(" - ").append(thisActivity.getString(messageCode)).append('\n');
                    }
                    String alertMessage =
                            thisActivity.getString(R.string.message_insecure_connection, stringBuilder.toString());

                    AlertDialog.Builder builder = new AlertDialog.Builder(thisActivity);
                    builder.setTitle(thisActivity.getString(R.string.title_warning));
                    builder.setMessage(alertMessage)
                            .setCancelable(true)
                            .setPositiveButton(thisActivity.getString(R.string.action_yes),
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int id) {
                                            handler.proceed();
                                        }
                                    })
                            .setNegativeButton(thisActivity.getString(R.string.action_no),
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int id) {
                                            handler.cancel();
                                        }
                                    });
                    builder.create().show();
                }

            });

            web_view_skin.setWebChromeClient(new WebChromeClient() {
                @Override
                public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
                    Log.d("WEBVIEW", consoleMessage.message());
                    if (consoleMessage.message().contains("Uncaught ReferenceError") ||
                            consoleMessage.message().contains("Uncaught TypeError")) {
                        thisActivity.finish();
                    }
                    if (consoleMessage.message().contains("Closed")) {
                        thisActivity.finish();
                    }
                    return super.onConsoleMessage(consoleMessage);
                }
            });

            //@vkg
            ImageView close_button_iv = findViewById(R.id.close_button_image_view);
            close_button_iv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Toast.makeText(YourActivityName.this,
                    Log.d("NotificationActivity", "The favorite list would appear on clicking this icon");
                    thisActivity.finish();
                }
            });
            //@vkg

            int leftMargin = notification.leftMargin == 0 ?
                    (int) getResources().getDimension(R.dimen.default_left_margin) :
                    (int) convertPixelsToDp(notification.leftMargin + 5);
            int topMargin = notification.topMargin == 0 ?
                    (int) getResources().getDimension(R.dimen.default_top_margin) :
                    (int) convertPixelsToDp(notification.topMargin + 5);
            int rightMargin = notification.rightMargin == 0 ?
                    (int) getResources().getDimension(R.dimen.default_right_margin) :
                    (int) convertPixelsToDp(notification.rightMargin + 5);
            int bottomMargin = notification.bottomMargin == 0 ?
                    (int) getResources().getDimension(R.dimen.default_bottom_margin) :
                    (int) convertPixelsToDp(notification.bottomMargin + 5);

            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            layoutParams.setMargins(leftMargin, topMargin, rightMargin, bottomMargin);
            web_view.setLayoutParams(layoutParams);

            TextView title_tv = findViewById(R.id.title_tv);
            title_tv.setVisibility(View.GONE);
            TextView date_tv = findViewById(R.id.date_tv);
            date_tv.setVisibility(View.GONE);

            DBHelper.loadAlertForUser(thisActivity, notification, web_view, web_view_skin,
                    settings.user_name);
        }
    }

    private float convertPixelsToDp(float px) {
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        return px * ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (isFinishing()) {
            web_view.stopLoading();
            web_view_skin.stopLoading();
        }
    }
}
