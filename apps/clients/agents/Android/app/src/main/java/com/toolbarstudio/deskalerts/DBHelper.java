package com.toolbarstudio.deskalerts;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.webkit.WebView;

import com.androidquery.util.XmlDom;
import com.toolbarstudio.deskalerts.event.DeleteNotificationEvent;

import org.greenrobot.eventbus.EventBus;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by brazil on 27.03.2016.
 */
public class DBHelper extends SQLiteOpenHelper {

    private static final ReentrantLock lock = new ReentrantLock();

    private static final String[] bad_skins = {"c28f6977-3254-418c-b96e-bda43ce94ff4", "138f9281-0c11-4c48-983e-d4ca8076748b", "04f84380-b31d-4167-8b83-c6642e3d25a8"};

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");

    private DBHelper(Context context) {
        super(context, "notifications", null, 4);
    }

    static LinkedList<Notification> insertNotificationsForUser(Context context, XmlDom xmlDom, String userName, boolean historyInsert) {
        lock.lock();
        final Settings settings = Settings.get(context);

        final LinkedList<Notification> addedNotifications = new LinkedList<>();
        try {
            final LinkedList<Notification> currentNotifications = DBHelper.getAllNotificationsFromBaseForUser(context, userName);
            DBHelper dbHelper = new DBHelper(context);
            SQLiteDatabase db = dbHelper.getWritableDatabase();

            long biggestId = 0;

            for (XmlDom alert_node : xmlDom.children("ALERT")) {
                if (isNeedToSkip(currentNotifications, alert_node)) {
                    continue;
                }

                long id = Long.parseLong(alert_node.attr("alert_id"));

                if (biggestId < id) {
                    biggestId = id;
                }

                Notification notification = insertInDatabase(db, alert_node, userName, settings.host, historyInsert);
                addedNotifications.add(0, notification);
            }
            settings.saveLastAlertId(context, biggestId);
            dbHelper.close();
        } finally {
            lock.unlock();
        }
        return addedNotifications;
    }

    private static Notification insertInDatabase(SQLiteDatabase db, XmlDom alertNode, String userName, String host, boolean historyInsert) {
        String skinUrl = "";
        int leftMargin = 0;
        int topMargin = 0;
        int rightMargin = 0;
        int bottomMargin = 0;

        long id = Long.parseLong(alertNode.attr("alert_id"));

        String url = alertNode.attr("href");
        url = Utilities.replaceHostInUrl(url, host);

        if (alertNode.children("WINDOW").size() > 0) {
            skinUrl = alertNode.child("WINDOW").attr("captionhref");
            skinUrl = Utilities.replaceHostInUrl(skinUrl, host);
            leftMargin = Integer.parseInt(alertNode.child("WINDOW").attr("leftmargin"));
            topMargin = Integer.parseInt(alertNode.child("WINDOW").attr("topmargin"));
            rightMargin = Integer.parseInt(alertNode.child("WINDOW").attr("rightmargin"));
            bottomMargin = Integer.parseInt(alertNode.child("WINDOW").attr("bottommargin"));
        }

        String title = alertNode.attr("title");
        boolean urgent_flag = alertNode.attr("urgent").equals("1");
        boolean survey_flag = alertNode.attr("survey").equals("1");
        boolean acknowledgement = alertNode.attr("acknown").equals("1");
        boolean selfDestructing = alertNode.attr("self_deletable").equals("1");
        String urgent = alertNode.attr("urgent");
        String survey = alertNode.attr("survey");
        long timestamp = Long.parseLong(alertNode.attr("create_date"));
        String info_text = dateFormat.format(new Date(Long.parseLong(alertNode.attr("create_date") + "000")));
        String date = alertNode.attr("create_date");

        ContentValues cv = new ContentValues();
        cv.put("id", id);
        cv.put("user_name", userName);
        cv.put("url", url);
        cv.put("skin_url", skinUrl);
        cv.put("left_margin", leftMargin);
        cv.put("top_margin", topMargin);
        cv.put("right_margin", rightMargin);
        cv.put("bottom_margin", bottomMargin);
        cv.put("title", title.replaceAll("<[^>]*>", ""));
        cv.put("urgent", urgent);
        cv.put("survey", survey);
        cv.put("acknowledgement", acknowledgement);
        cv.put("self_destructing", selfDestructing);
        if (historyInsert) {
            cv.put("old_post", "1");
        } else {
            cv.put("old_post", "0");
        }
        cv.put("favorite", "0");
        cv.put("date", date);

        long id_db = db.insert("notifications", null, cv);
        return new Notification(id_db, id, timestamp, title.replaceAll("<[^>]*>", ""), info_text, urgent_flag, survey_flag, historyInsert, false, acknowledgement, selfDestructing, leftMargin, topMargin, rightMargin, bottomMargin);
    }

    private static boolean isNeedToSkip(LinkedList<Notification> currentNotifications, XmlDom alert_node) {
        long id;
        id = Long.parseLong(alert_node.attr("alert_id"));
        for (Notification notification : currentNotifications) {
            if (notification.id == id) {
                return true;
            }
        }
        return false;
    }

    static LinkedList<Notification> getAllNotificationsFromBaseForUser(Context context, String userName) {

        String selection = "user_name" + " LIKE ?";
        String like = "%" + userName + "%";
        String[] args = new String[]{like};

        DBHelper dbHelper = new DBHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor cursor = db.query("notifications", null, selection, args, null, null, "date DESC");
        LinkedList<Notification> result = notificationCursorToList(cursor);
        dbHelper.close();
        return result;
    }

    private static LinkedList<Notification> notificationCursorToList(Cursor cursor){
        LinkedList<Notification> result = new LinkedList<>();
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                int id_dbColIndex = cursor.getColumnIndex("id_db");
                int idColIndex = cursor.getColumnIndex("id");
                int titleColIndex = cursor.getColumnIndex("title");
                int dateColIndex = cursor.getColumnIndex("date");
                int urgentColIndex = cursor.getColumnIndex("urgent");
                int surveyColIndex = cursor.getColumnIndex("survey");
                int old_postColIndex = cursor.getColumnIndex("old_post");
                int favoriteColIndex = cursor.getColumnIndex("favorite");
                int selfDestructingColIndex = cursor.getColumnIndex("self_destructing");
                int acknowledgementColIndex = cursor.getColumnIndex("acknowledgement");

                int leftMarginColIndex = cursor.getColumnIndex("left_margin");
                int topMarginColIndex = cursor.getColumnIndex("top_margin");
                int rightMarginColIndex = cursor.getColumnIndex("right_margin");
                int bottomMarginColIndex = cursor.getColumnIndex("bottom_margin");

                long id_db;
                long id;
                long timestamp;
                String title;
                String info_text;
                boolean urgent_flag;
                boolean survey_flag;
                boolean old_post_flag;
                boolean favorite_flag;
                boolean selfDestructingFlag;
                boolean acknowledgementFlag;

                int leftMargin;
                int topMargin;
                int rightMargin;
                int bottomMargin;

                do {
                    id_db = cursor.getLong(id_dbColIndex);
                    id = cursor.getLong(idColIndex);
                    title = cursor.getString(titleColIndex);
                    timestamp = cursor.getLong(dateColIndex);
                    info_text = dateFormat.format(new Date(Long.parseLong(cursor.getString(dateColIndex) + "000")));
                    urgent_flag = (cursor.getInt(urgentColIndex) != 0);
                    survey_flag = (cursor.getInt(surveyColIndex) != 0);
                    old_post_flag = (cursor.getInt(old_postColIndex) != 0);
                    favorite_flag = (cursor.getInt(favoriteColIndex) != 0);
                    selfDestructingFlag = (cursor.getInt(selfDestructingColIndex) != 0);
                    acknowledgementFlag = (cursor.getInt(acknowledgementColIndex) != 0);

                    leftMargin = cursor.getInt(leftMarginColIndex);
                    topMargin = cursor.getInt(topMarginColIndex);
                    rightMargin = cursor.getInt(rightMarginColIndex);
                    bottomMargin = cursor.getInt(bottomMarginColIndex);

                    result.add(new Notification(id_db, id, timestamp, title, info_text, urgent_flag, survey_flag, old_post_flag, favorite_flag, acknowledgementFlag, selfDestructingFlag, leftMargin, topMargin, rightMargin, bottomMargin));
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        return result;
    }

    public static Notification getNotificationByDbId(Context context, long notificationDbId){
        String selection = "id_db=?";
        String[] args = new String[]{String.valueOf(notificationDbId)};

        DBHelper dbHelper = new DBHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor cursor = db.query("notifications", null, selection, args, null, null, "date DESC");
        LinkedList<Notification> notificationList = notificationCursorToList(cursor);
        Notification result = notificationList.isEmpty() ? null : notificationList.getFirst();
        dbHelper.close();
        return result;
    }

    static void loadAlertForUser(Context context, Notification notification,
                                 WebView webView, WebView webView_skin,
                                 String userName) {
        DBHelper dbHelper = new DBHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        String url = "";
        String skin_url = "";
        String DEFAULT_SKIN_URL = Settings.get(context).host + "/admin/skins/default/version/alertcaption.html";
        String html = "";

        String selection = "id_db = ? AND (user_name LIKE ?)";
        String[] selectionAgrs = new String[]{"" + notification.id_db, "%" + userName + "%"};

        Cursor cursor = db.query("notifications", new String[]{"url", "html", "skin_url"}, selection, selectionAgrs, null, null, null);

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                int urlColIndex = cursor.getColumnIndex("url");
                int htmlColIndex = cursor.getColumnIndex("html");
                int skinColIndex = cursor.getColumnIndex("skin_url");
                url = cursor.getString(urlColIndex).replace("{", "%7B").replace("}", "%7D");
                skin_url = cursor.getString(skinColIndex);
                if (skin_url != null) {
                    skin_url = skin_url.replace("{", "%7B").replace("}", "%7D");
                } else skin_url = "";
                if (skin_url.equals("")) {
                    skin_url = DEFAULT_SKIN_URL;
                }
                html = cursor.getString(htmlColIndex);

                if (html == null) html = "";
                Log.d("REQUEST", "url: " + url + " need load from sever: " + html.equals(""));
                Log.d("REQUEST", "skin_url: " + skin_url);
            }
            cursor.close();
        }
        dbHelper.close();

        for (String s : bad_skins) {
            if (skin_url.contains(s)) {
                skin_url = DEFAULT_SKIN_URL;
            }
        }

        AjaxHelper.getSkin(context, skin_url, webView_skin);

        if (html.equals("")) {
            AjaxHelper.getAlert(context, url, userName, webView, notification);
        } else {
            if (webView != null) {
                webView.loadDataWithBaseURL(url, html, "text/html", "en_US", null);
            }
        }
    }

    static void saveContentForUser(Context context, String ajax_html, Notification notification, String userName) {
        DBHelper dbHelper = new DBHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        String selection = "id_db = ? AND (user_name LIKE ?)";
        String[] selectionAgrs = new String[]{"" + notification.id_db, "%" + userName + "%"};

        ContentValues cv = new ContentValues();
        cv.put("html", ajax_html);
        cv.put("content", ajax_html.replaceAll("<(.*?)>", " ").replaceAll("<(.*?)", " ").replaceFirst("(.*?)>", " ").replaceAll("&nbsp;", " ").replaceAll("&amp;", " ").toLowerCase());
        db.update("notifications", cv, selection, selectionAgrs);
        dbHelper.close();
    }

    static void setReadFlagForUser(Context context, Notification notification, String userName) {
        DBHelper dbHelper = new DBHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        String selection = "id_db = ? AND (user_name LIKE ?)";
        String[] selectionAgrs = new String[]{"" + notification.id_db, "%" + userName + "%"};

        ContentValues cv = new ContentValues();
        cv.put("old_post", (notification.read_flag) ? (1) : (0));
        db.update("notifications", cv, selection, selectionAgrs);
        dbHelper.close();
    }

    static void setFavoriteFlagForUser(Context context, Notification notification, String userName) {
        DBHelper dbHelper = new DBHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        String selection = "id_db = ? AND (user_name LIKE ?)";
        String[] selectionAgrs = new String[]{"" + notification.id_db, "%" + userName + "%"};

        ContentValues cv = new ContentValues();
        cv.put("favorite", (notification.favorite_flag) ? (1) : (0));
        db.update("notifications", cv, selection, selectionAgrs);
        dbHelper.close();
    }

    static void deleteForUser(Context context, Notification notification, String userName) {
        DBHelper dbHelper = new DBHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        String selection = "id_db = ? AND (user_name LIKE ?)";
        String[] selectionAgrs = new String[]{"" + notification.id_db, "%" + userName + "%"};

        db.delete("notifications", selection, selectionAgrs);
        dbHelper.close();

        EventBus.getDefault().post(new DeleteNotificationEvent(notification));
    }

    static LinkedList<Notification> searchNotificationsForUser(Context context,
                                                               LinkedList<Notification> list,
                                                               boolean title_search,
                                                               boolean content_search,
                                                               NotificationTypeForFilter typeForFilter,
                                                               String looking_str,
                                                               String userName) {

        switch (typeForFilter) {
            case all:
                break;
            case favorite:
                list = Messages.filterFavoriteNotifications(list);
                break;
            case urgent:
                list = Messages.filterUrgentNotifications(list);
                break;
            case not_read:
                list = Messages.filterNotReadNotifications(list);
                break;
            default:
                break;
        }


        if (looking_str == null || looking_str.equals("")) {
            return list;
        }

        looking_str = looking_str.toLowerCase();

        DBHelper dbHelper = new DBHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor cursor;

        LinkedList<Notification> result = new LinkedList<>();

        String selection;
        String[] selectionAgrs;

        if (title_search && content_search) {
            selection = "id_db = ? AND ((title LIKE ?) OR (content LIKE ?)) AND (user_name LIKE ?)";
            selectionAgrs = new String[]{"", "%" + looking_str + "%", "%" + looking_str + "%", "%" + userName + "%"};
        } else if (title_search) {
            selection = "id_db = ? AND title LIKE ? AND (user_name LIKE ?)";
            selectionAgrs = new String[]{"", "%" + looking_str + "%", "%" + userName + "%"};
        } else if (content_search) {
            selection = "id_db = ? AND content LIKE ? AND (user_name LIKE ?)";
            selectionAgrs = new String[]{"", "%" + looking_str + "%", "%" + userName + "%"};
        } else return new LinkedList<>();

        for (Notification notification : list) {
            selectionAgrs[0] = "" + notification.id_db;
            cursor = db.query("notifications", null, selection, selectionAgrs, null, null, null);

            if (cursor.moveToFirst()) {
                if (!(notification.survey_flag && !title_search)) {
                    result.add(notification);
                }
            }
            cursor.close();
        }
        dbHelper.close();

        return result;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table notifications ("
                + "id_db integer primary key autoincrement,"
                + "id integer,"
                + "left_margin integer,"
                + "top_margin integer,"
                + "right_margin integer,"
                + "bottom_margin integer,"
                + "user_name text,"
                + "url text,"
                + "skin_url text,"
                + "title text,"
                + "html text,"
                + "content text,"
                + "survey integer,"
                + "urgent integer,"
                + "favorite integer,"
                + "acknowledgement integer,"
                + "self_destructing integer,"
                + "old_post integer,"
                + "date integer,"
                + "skin_id text"
                + ");");
        Log.d("NOTIFICATIONS_DB", "DB HAS BEEN CREATED");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS notifications;");
        onCreate(db);
    }
}