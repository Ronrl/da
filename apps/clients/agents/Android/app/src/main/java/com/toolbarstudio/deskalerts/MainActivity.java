package com.toolbarstudio.deskalerts;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.toolbarstudio.deskalerts.event.CheckAlertsEvent;
import com.toolbarstudio.deskalerts.event.DeleteNotificationEvent;
import com.toolbarstudio.deskalerts.fcm.QuickstartPreferences;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String TAG = "MAIN_ACTIVITY";
    public static final String REGISTRATION_ACTION = "com.deskalerts.action.DEVICE_REGISTER";
    public static final String EXTRA_REGISTRATION_OLD_TOKEN = "com.descalerts.android_client.registration.old_token";
    public static final String EXTRA_REGISTRATION_NEW_TOKEN = "com.descalerts.android_client.registration.new_token";


    private Activity thisActivity;
    private ViewPager viewPager;
    private View tabs_container;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private boolean isReceiverRegistered;
    private boolean isRegistered = false;

    private MainFragmentPagerAdapter pageAdapter;
    private int currentViewPagerItem;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(null);
        setContentView(R.layout.activity_main);
        EventBus.getDefault().register(this);

        thisActivity = this;
        final Settings settings = Settings.get(this);
        Messages messages = new Messages(DBHelper.getAllNotificationsFromBaseForUser(this, settings.user_name));

        viewPager = findViewById(R.id.view_pager);
        pageAdapter = new MainFragmentPagerAdapter(getSupportFragmentManager(), this, messages);
        viewPager.setAdapter(pageAdapter);

        TabsOnClickListener tabsOnClickListener = new TabsOnClickListener();

        tabs_container = findViewById(R.id.tabs_container);
        View allNotificationsButton = findViewById(R.id.all_notifications_button);
        View alertsButton = findViewById(R.id.alerts_button);
        View surveysButton = findViewById(R.id.surveys_button);
        View moreButton = findViewById(R.id.more_button);
        allNotificationsButton.setOnClickListener(tabsOnClickListener);
        alertsButton.setOnClickListener(tabsOnClickListener);
        surveysButton.setOnClickListener(tabsOnClickListener);
        moreButton.setOnClickListener(tabsOnClickListener);

        //Installation.id(thisActivity);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                choiceIcon(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                SharedPreferences sharedPreferences =
                        PreferenceManager.getDefaultSharedPreferences(context);
                boolean sentToken = sharedPreferences
                        .getBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, false);
                if (sentToken) {
                    Log.d(TAG, getString(R.string.gcm_send_message));
                } else {
                    Log.d(TAG, getString(R.string.token_error_message));
                }

                if( intent.getAction() == REGISTRATION_ACTION && isRegistered ){
                    String oldToken = intent.getStringExtra(EXTRA_REGISTRATION_OLD_TOKEN);
                    String newToken = intent.getStringExtra(EXTRA_REGISTRATION_NEW_TOKEN);
                    sendRegistration2Server(oldToken, newToken);
                }
            }
        };


        registerReceiver();

        // is RegistrationJobIntentService needed?
        /*if (checkPlayServices()) {
            RegistrationJobIntentService.enqueueWork(this, new Intent(this, RegistrationJobIntentService.class));
        }*/
        final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        // login_wizard_button
        // refresh all lists
        // auto-hide keyboard
        // auto-hide tabs
        Handler handler = new Handler() {
            public void handleMessage(android.os.Message msg) {
                switch (msg.what) {
                    case 0: // login_wizard_button
                        Intent intent = new Intent(thisActivity, Step0Activity.class);
                        startActivity(intent);
                        finishAffinity();
                        break;
                    case 2: // refresh all lists
                        pageAdapter.refreshAllLists();
                        break;
                    case 3: // auto-hide keyboard
                        try {
                            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                            tabs_container.setVisibility(View.VISIBLE);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                    case 4: // auto-hide tabs
                        tabs_container.setVisibility(View.GONE);
                        break;
                }
            }
        };
        pageAdapter.setHandler(handler);
        if (settings.user_name == null || settings.user_name.equals("")) {
            isRegistered = false;
            handler.sendEmptyMessage(0);
        } else {
            isRegistered = true;
        }
        refreshToken();
        AjaxHelper.initialize();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onCheckAlerts(CheckAlertsEvent event) {
        Settings settings = Settings.get(this);
        AjaxHelper.checkNewAlerts(thisActivity, settings.host, settings.user_name, settings.guid, pageAdapter);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDeleteNotification(DeleteNotificationEvent event) {
        pageAdapter.deleteNotification(event.getNotification());
        pageAdapter.deleteFromMessages(event.getNotification());
        pageAdapter.refreshAllLists();
    }


    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver();
        final Settings settings = Settings.get(this);
        AjaxHelper.checkNewAlerts(this, settings.host, settings.user_name, settings.guid, pageAdapter);
    }

    @Override
    protected void onPause() {
        //LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        //unregisterReceiver(mRegistrationBroadcastReceiver);

        isReceiverRegistered = false;
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        if( (viewPager.getCurrentItem() != 3)  )
            super.onBackPressed();
        else {
            viewPager.setCurrentItem(currentViewPagerItem);
            currentViewPagerItem = viewPager.getCurrentItem();
        }
    }

    private void registerReceiver() {
        if (!isReceiverRegistered) {
            LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                    new IntentFilter(REGISTRATION_ACTION));
            isReceiverRegistered = true;
        }
    }

    private void sendRegistration2Server(String oldToken, String newToken){
        final Settings settings = Settings.get(this);
        AjaxHelper.deviceUnRegister( thisActivity, settings.host, settings.user_name, settings.domain, settings.guid, oldToken );
        AjaxHelper.deviceRegistration( thisActivity, settings.host, settings.user_name,   settings.domain, settings.guid, newToken, android.os.Build.VERSION.SDK_INT );
    }

    private void refreshToken() {
        SharedPreferences prefs = getSharedPreferences("TOKEN_PREF", MODE_PRIVATE);
        String token = prefs.getString("token", "");

        Log.e("NEW_INACTIVITY_TOKEN", token);

        if (TextUtils.isEmpty(token)) {
            FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( thisActivity, new OnSuccessListener<InstanceIdResult>() {
                @Override
                public void onSuccess(InstanceIdResult instanceIdResult) {
                    String newToken = instanceIdResult.getToken();
                    Log.e("newToken", newToken );
                    if (newToken!=null){
                        String oldToken = getSharedPreferences("TOKEN_PREF", MODE_PRIVATE).getString("token", "");
                        SharedPreferences.Editor editor = getSharedPreferences("TOKEN_PREF", MODE_PRIVATE).edit();
                        editor.putString("token", newToken );
                        editor.putString("prev_token", oldToken );
                        editor.apply();

                        // is RegistrationJobIntentService needed?
                        //RegistrationJobIntentService.enqueueWork(MainActivity.this, new Intent(MainActivity.this, RegistrationJobIntentService.class));
                        if(isRegistered){
                            sendRegistration2Server(oldToken, newToken);
                        }
                    }
                }
            });
        }
    }


    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    private void choiceIcon(int position) {
        ArrayList<View> icons = new ArrayList<>();
        icons.add(findViewById(R.id.all_iv));
        icons.add(findViewById(R.id.all_iv_m));
        icons.add(findViewById(R.id.alerts_iv));
        icons.add(findViewById(R.id.alerts_iv_m));
        icons.add(findViewById(R.id.surveys_iv));
        icons.add(findViewById(R.id.surveys_iv_m));
        icons.add(findViewById(R.id.more_iv));
        icons.add(findViewById(R.id.more_iv_m));

        for (int i = 0; i < icons.size(); i++) {
            if (i % 2 == 0) {
                icons.get(i).setVisibility(View.VISIBLE);
            } else {
                icons.get(i).setVisibility(View.INVISIBLE);
            }
        }
        icons.get(position * 2).setVisibility(View.INVISIBLE);
        icons.get(position * 2 + 1).setVisibility(View.VISIBLE);
    }

    class TabsOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            currentViewPagerItem = viewPager.getCurrentItem();
            switch (v.getId()) {
                case R.id.all_notifications_button:
                    viewPager.setCurrentItem(0, true);
                    choiceIcon(0);
                    break;
                case R.id.alerts_button:
                    viewPager.setCurrentItem(1, true);
                    choiceIcon(1);
                    break;
                case R.id.surveys_button:
                    viewPager.setCurrentItem(2, true);
                    choiceIcon(2);
                    break;
                case R.id.more_button:
                    viewPager.setCurrentItem(3, true);
                    choiceIcon(3);
                    break;
            }
        }
    }
}
