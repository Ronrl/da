package com.toolbarstudio.deskalerts;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * Created by brazil on 26.03.2016.
 */
public class ListFragment extends Fragment {

    private MainFragmentPagerAdapter pagerAdapter;
    private EditText searchField;
    private boolean searchByTitle = true;
    private boolean searchByContent = true;
    private NotificationTypeForFilter typeForFilter = NotificationTypeForFilter.all;
    private Context context;
    private LinkedList<Notification> list;
    private View emptyListMark;
    private ListFragmentAdapter adapter;
    private boolean show_type_flag = true;
    private String title;
    private View startSearchButton;
    private View stopSearchButton;
    private Spinner spinnerViewBy;
    private Spinner spinnerSearchBy;

    public static Fragment createListFragment(Context context, LinkedList<Notification> list, String title, MainFragmentPagerAdapter pagerAdapter) {
        ListFragment fragment = new ListFragment();
        fragment.context = context;

        Collections.sort(list, new Comparator<Notification>() {
            @Override
            public int compare(Notification n1, Notification n2) {
                return new Date(n2.timestamp).compareTo(new Date(n1.timestamp));
            }
        });

        fragment.list = list;
        fragment.title = title;
        fragment.pagerAdapter = pagerAdapter;
        return fragment;
    }

    public static Fragment createListFragment(Context context, LinkedList<Notification> list, boolean show_type_flag, String title, MainFragmentPagerAdapter pagerAdapter) {
        ListFragment fragment = new ListFragment();
        fragment.context = context;

        Collections.sort(list, new Comparator<Notification>() {
            @Override
            public int compare(Notification n1, Notification n2) {
                return new Date(n2.timestamp).compareTo(new Date(n1.timestamp));
            }
        });

        fragment.list = list;
        fragment.show_type_flag = show_type_flag;
        fragment.title = title;
        fragment.pagerAdapter = pagerAdapter;
        return fragment;
    }

    public void addNotifications(LinkedList<Notification> add_list) {
        if (list == null) {
            list = add_list;
        } else {
            list.addAll(0, add_list);
        }

        LinkedList<Notification> temp = new LinkedList<>(list);

        Collections.sort(list, new Comparator<Notification>() {
            @Override
            public int compare(Notification n1, Notification n2) {
                return new Date(n2.timestamp).compareTo(new Date(n1.timestamp));
            }
        });

        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }
        if (list.size() > 0) {
            emptyListMark.setVisibility(View.GONE);
            if (add_list != null) {
                adapter.notifyDataSetChanged();
            }
        }
    }

    public void deleteNotification(Notification notification) {
        if (list != null) {
            for(Iterator<Notification> it = list.iterator(); it.hasNext(); ) {
                if(it.next().id_db==notification.id_db) {
                    it.remove();
                    break;
                }
            }
        }
        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }
    }

    public void refresh() {
        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        if (context == null) {
            return new View(getContext());
        }
        final Settings settings = Settings.get(context);

        View view = inflater.inflate(R.layout.list_fragment, container, false);

        emptyListMark = view.findViewById(R.id.no_notifications_container);

        if (list != null && list.size() > 0) {
            emptyListMark.setVisibility(View.GONE);
        }

        ListView listView = view.findViewById(R.id.list_view);
        adapter = new ListFragmentAdapter(context, list, show_type_flag, pagerAdapter);
        listView.setAdapter(adapter);

        ((TextView) view.findViewById(R.id.head_title_tv)).setText(title);


        startSearchButton = view.findViewById(R.id.start_search_button);
        startSearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                search(settings.user_name);
                hideKeyboard();
            }
        });
        stopSearchButton = view.findViewById(R.id.stop_search_button);
        stopSearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchField.setText("");
                deactivateSearchButtons();
                spinnerSearchBy.setSelection(0);
                spinnerViewBy.setSelection(0);
                typeForFilter = NotificationTypeForFilter.all;
                search(settings.user_name);
            }
        });
        spinnerViewBy = view.findViewById(R.id.view_by_spinner);
        spinnerViewBy.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        typeForFilter = NotificationTypeForFilter.all;
                        break;
                    case 1:
                        typeForFilter = NotificationTypeForFilter.favorite;
                        break;
                    case 2:
                        typeForFilter = NotificationTypeForFilter.urgent;
                        break;
                    case 3:
                        typeForFilter = NotificationTypeForFilter.not_read;
                        break;
                    default:
                        typeForFilter = NotificationTypeForFilter.all;
                        break;
                }
                search(settings.user_name);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerSearchBy = view.findViewById(R.id.search_by_spinner);
        spinnerSearchBy.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        searchByTitle = true;
                        searchByContent = true;
                        break;
                    case 1:
                        searchByTitle = true;
                        searchByContent = false;
                        break;
                    case 2:
                        searchByTitle = false;
                        searchByContent = true;
                        break;
                    default:
                        break;
                }
                search(settings.user_name);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        searchField = view.findViewById(R.id.search_field);
        searchField.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activateSearchButtons();
                hideTabs();
            }
        });
        searchField.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    activateSearchButtons();
                    hideTabs();
                } else {
                    deactivateSearchButtons();
                    hideKeyboard();
                }
            }
        });
        View.OnTouchListener onTouchListener = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideKeyboard();
                return false;
            }
        };
        spinnerViewBy.setOnTouchListener(onTouchListener);
        spinnerSearchBy.setOnTouchListener(onTouchListener);
        listView.setOnTouchListener(onTouchListener);

        return view;
    }

    private void search(String userName) {
        adapter.setList(DBHelper.searchNotificationsForUser(context, list, searchByTitle, searchByContent, typeForFilter, searchField.getText().toString(), userName));
    }

    private void hideKeyboard() {
        pagerAdapter.sendEmptyMessageToMainHandler(3);
    }

    private void hideTabs() {
        pagerAdapter.sendEmptyMessageToMainHandler(4);
    }

    private void activateSearchButtons() {
        startSearchButton.setVisibility(View.VISIBLE);
        stopSearchButton.setVisibility(View.VISIBLE);
    }

    private void deactivateSearchButtons() {
        startSearchButton.setVisibility(View.GONE);
        stopSearchButton.setVisibility(View.GONE);
    }

    public void deleteNotifications(String ids) {
        if (list == null) {
            return;
        }

        LinkedList<Notification> notificationsToRemove = new LinkedList<>();

        for (Notification notification : this.list) {
            if (ids.contains(Long.toString(notification.id))) {
                notificationsToRemove.add(notification);
            }
        }

        this.list.removeAll(notificationsToRemove);
        if (this.adapter != null) {
            adapter.notifyDataSetChanged();
        }
    }
}
