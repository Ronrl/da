/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.toolbarstudio.deskalerts.fcm;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.toolbarstudio.deskalerts.event.CheckAlertsEvent;
import com.toolbarstudio.deskalerts.MainActivity;
import com.toolbarstudio.deskalerts.R;

//import org.json.JSONException;
//import org.json.JSONObject;

import org.greenrobot.eventbus.EventBus;

import java.util.Calendar;
//import java.util.Date;
import java.util.Map;

import static com.toolbarstudio.deskalerts.MainActivity.EXTRA_REGISTRATION_NEW_TOKEN;
import static com.toolbarstudio.deskalerts.MainActivity.EXTRA_REGISTRATION_OLD_TOKEN;
import static com.toolbarstudio.deskalerts.MainActivity.REGISTRATION_ACTION;

public class MyFcmListenerService extends FirebaseMessagingService {
    private static final String TAG = "DESKALERTS_FCM";

    @Override
    public void onMessageReceived(RemoteMessage message){
        String from = message.getFrom();
        Map data = message.getData();
        String title = data.get("title").toString().replaceAll("&nbsp;", "\u00A0");

        Log.d(TAG, "From: " + from);
        Log.d(TAG, "Message: " + message);
        sendNotification(title);
    }

    @Override
    public void onNewToken(String token) {
        Log.d(TAG, "Refreshed token: " + token);

        SharedPreferences prefs = getSharedPreferences("TOKEN_PREF", MODE_PRIVATE);
        String oldToken = prefs.getString("token", "");

        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("token", token );
        editor.putString("prev_token", oldToken );
        editor.apply();
        // is RegistrationJobIntentService needed?
        //RegistrationJobIntentService.enqueueWork(this, new Intent(this, RegistrationJobIntentService.class));

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.

        sendRegistrationToServer(oldToken, token);
    }

    private void sendRegistrationToServer(String oldToken, String newToken){
        Intent intent = new Intent();
        intent.setAction(REGISTRATION_ACTION);
        intent.putExtra( EXTRA_REGISTRATION_OLD_TOKEN , oldToken);
        intent.putExtra( EXTRA_REGISTRATION_NEW_TOKEN, newToken);
        //intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        LocalBroadcastManager.getInstance(getBaseContext()).sendBroadcast(intent);
    }
    //public void onMessageReceived(String from, Bundle data) {
    //    String message = data.getString("title");
    //    Log.d(TAG, "From: " + from);
    //    Log.d(TAG, "Message: " + message);
    //    sendNotification(message);
    //}

    private boolean isAllowedToday() {
        SharedPreferences preferences = this.getSharedPreferences("schedule", Context.MODE_PRIVATE);
        Calendar calendar = Calendar.getInstance();
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);

        switch (dayOfWeek) {
            case Calendar.MONDAY:
                return preferences.getBoolean("mon", true);
            case Calendar.TUESDAY:
                return preferences.getBoolean("tue", true);
            case Calendar.WEDNESDAY:
                return preferences.getBoolean("wed", true);
            case Calendar.THURSDAY:
                return preferences.getBoolean("thu", true);
            case Calendar.FRIDAY:
                return preferences.getBoolean("fri", true);
            case Calendar.SATURDAY:
                return preferences.getBoolean("sat", true);
            case Calendar.SUNDAY:
                return preferences.getBoolean("sun", true);
            default:
                return true;
        }
    }

    private void sendNotification(String message) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);
        String channelId = "da-channel-01";
        String channelName = "DA Notification Channel Name";
        int importance = NotificationManager.IMPORTANCE_HIGH;

        //Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        boolean allowedToday = isAllowedToday();
        int current_h = calendar.get(Calendar.HOUR);
        int current_m = calendar.get(Calendar.MINUTE);

        SharedPreferences preferences = this.getSharedPreferences("schedule", Context.MODE_PRIVATE);
        int start_h = preferences.getInt("start_h", 0);
        int start_m = preferences.getInt("start_m", 0);
        int end_h = preferences.getInt("end_h", 0);
        if (end_h == 0) {
            end_h = 24;
        }
        int end_m = preferences.getInt("end_m", 0);
        if (end_m == 0) {
            end_m = 60;
        }
        int colorNotification = getResources().getColor(R.color.notification);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationManager notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(
                    channelId, channelName, importance);
            notificationManager.createNotificationChannel(mChannel);
        }

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.drawable.ic_notification)
                .setColor(colorNotification)
                .setContentTitle("You have new message")
                .setContentText(message)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent);

        Log.w("ALLOWED_TODAY", "" + allowedToday);
        Log.w("TIME_HOURS", "" + start_h + " - " + current_h + " - " + end_h + " - " + (start_h <= current_h && end_h >= current_h));
        Log.w("TIME_MINUTES", "" + start_m + " - " + current_m + " - " + end_m + " - " + (start_m <= current_m && end_m >= current_m));
        Log.w("TIME_TOTAL", "" + (allowedToday && start_h <= current_h && end_h >= current_h && start_m <= current_m && end_m >= current_m));

        if (allowedToday && start_h <= current_h && end_h >= current_h && start_m <= current_m && end_m >= current_m) {
            notificationBuilder.setSound(defaultSoundUri);
        }

        notificationManager.notify(0 , notificationBuilder.build());

        EventBus.getDefault().post(new CheckAlertsEvent());
    }
}
