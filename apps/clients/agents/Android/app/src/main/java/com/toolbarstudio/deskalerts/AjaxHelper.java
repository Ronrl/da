package com.toolbarstudio.deskalerts;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.util.XmlDom;
import com.toolbarstudio.deskalerts.event.PingError;
import com.toolbarstudio.deskalerts.event.RegistrationError;
import com.toolbarstudio.deskalerts.event.SuccessfulPing;
import com.toolbarstudio.deskalerts.event.SuccessfulRegistration;

import org.apache.http.conn.ssl.SSLSocketFactory;
import org.greenrobot.eventbus.EventBus;

import java.security.KeyStore;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.toolbarstudio.deskalerts.Utilities.decryptAlertBody;
import static com.toolbarstudio.deskalerts.Utilities.replaceHostInUrl;

public class AjaxHelper {

    static void initialize() {
        try {
            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            trustStore.load(null, null);
            MySslSocketFactory sf = new MySslSocketFactory(trustStore);
            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
            AjaxCallback.setSSF(sf);
        } catch (Exception e) {
            Log.d("SOCKET_ERROR", "Error! " + e.getMessage());
        }
    }

    static void pingServer(final Context context, String hostname, String http) {
        AQuery aq = new AQuery(context);
        String url;

        if (!(hostname.contains("http://") || hostname.contains("https://"))) {
            url = http + hostname;
        } else {
            url = hostname;
        }
        if (url.endsWith("/")) {
            url += "register.asp";
        } else {
            url += "/register.asp";
        }

        Log.d("AFTER_INSTALL", url);
        aq.ajax(url, String.class, new AjaxCallback<String>() {
            @Override
            public void callback(String url, String object, AjaxStatus status) {
                if (status.getCode() == 200) {
                    EventBus.getDefault().post(new SuccessfulPing());
                } else {
                    EventBus.getDefault().post(new PingError());
                }
            }
        });
    }

    static void install(final Context context, String hostname, String guid) {
        AQuery aq = new AQuery(context);
        String url;

        if (!hostname.startsWith("http://") && !hostname.startsWith("https://")) {
            url = "http://" + hostname + "/after_install.asp?deskbar_id=%7B" + guid + "%7D&client_id=3";
        } else {
            url = hostname + "/after_install.asp?deskbar_id=%7B" + guid + "%7D&client_id=3";
        }

        Log.d("AFTER_INSTALL", url);
        aq.ajax(url, String.class, new AjaxCallback<String>() {
            @Override
            public void callback(String url, String object, AjaxStatus status) {
                if (object != null) {
                    Log.d("AFTER_INSTALL", "object: " + object + "\n" + "status: " + status.getCode());
                    EventBus.getDefault().post(new SuccessfulRegistration());
                } else {
                    EventBus.getDefault().post(new RegistrationError());
                    Log.d("AFTER_INSTALL", "Error! Status: " + status.getCode());
                    Toast.makeText(context, context.getResources().getText(R.string.ajax_bad_callback), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    static void userRegistration(final Context context, final String hostname, final String username, final String password,  final String guid,
                                 final WebView webView, final View nextButton, final String domain) {
        AQuery aq = new AQuery(context);
        String url;

        if (!hostname.startsWith("http://") && !hostname.startsWith("https://")) {
            url = "http://" + hostname + "/register.asp";
        } else {
            url = hostname + "/register.asp";
        }

        HashMap<String, Object> params = new HashMap<>();
        params.put("uname", username);
        params.put("upass", password);
        params.put("domain", domain);
        params.put("deskbar_id", "%7B" + guid + "%7D");
        params.put("client_id", 3);
        params.put("form", 1);
        params.put("x", 32);
        params.put("y", 13);

        Log.d("REGISTRATION_USER", url);
        aq.ajax(url, params, String.class, new AjaxCallback<String>() {
            @Override
            public void callback(String url, String string, AjaxStatus status) {
                if (string != null) {
                    if (string.contains("alt=\"Close\" />")) {
                        string = string.replace("<input src", "<!--input src").replace("alt=\"Close\" />", "alt=\"Close\"-->");
                    }
                    if (string.contains("Error")) {
                        string = string.replace("<div id=\"mainForm\"", "<!--div id=\"mainForm\"");
                        nextButton.setVisibility(View.GONE);
                    } else {
                        nextButton.setVisibility(View.VISIBLE);
                    }
                    webView.loadDataWithBaseURL(null, string, "text/html", "en_US", null);
                    AjaxHelper.getEncryptionKey(context, hostname, username, password);
                    Log.d("REGISTRATION_USER", string);
                } else {
                    Log.d("REGISTRATION_USER", "Error! Status: " + status.getCode());
                    Toast.makeText(context, context.getResources().getText(R.string.ajax_bad_callback), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    static void deviceRegistration(final Context context, String hostname, String user_name, String domain_name, String guid, String token, int sdk) {
        AQuery aq = new AQuery(context);
        String url = "";
        String thisToken;

        if (!hostname.startsWith("http://") && !hostname.startsWith("https://")) {
            url = "http://"; // + hostname + "/register_device.asp?device_id=" + token + "&user_name=" + user_name + "&device_type=2&GUID=" + guid;
        }
        url += hostname + "/register_device.asp?device_id=" + token + "&user_name=" + user_name + "&domain_name=" + domain_name + "&device_type=2&GUID=" + guid+ "&sdk_int=" + sdk;
        Log.d("REGISTRATION", "url: " + url);
        aq.ajax(url, String.class, new AjaxCallback<String>() {
            @Override
            public void callback(String url, String string, AjaxStatus status) {
                if (string != null) {
                    Log.d("REGISTRATION", string);
                } else {
                    Log.d("REGISTRATION", "Error! Status: " + status.getCode());
                    Toast.makeText(context, context.getResources().getString(R.string.ajax_bad_gcm_callback), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    static void deviceUnRegister(final Context context, String hostname, String user_name, String domain_name, String guid, String token) {
        AQuery aq = new AQuery(context);
        String url = "";

        if (!hostname.startsWith("http://") && !hostname.startsWith("https://")) {
            url = "http://"; //+ hostname + "/unregister_device.asp?device_id=" + token + "&user_name=" + user_name + "&device_type=2&GUID=" + guid;
        } //else {
          //  url = hostname + "/unregister_device.asp?device_id=" + token + "&user_name=" + user_name + "&device_type=2&GUID=" + guid;
        //}
        url += hostname + "/unregister_device.asp?device_id=" + token + "&user_name=" + user_name + "&domain_name=" + domain_name + "&device_type=2&GUID=" + guid;

        Log.d("UNREGISTRATION_GCM", "url: " + url);
        aq.ajax(url, String.class, new AjaxCallback<String>() {
            @Override
            public void callback(String url, String string, AjaxStatus status) {
                if (string != null) {
                    Log.d("UNREGISTRATION_GCM", string);
                } else {
                    Log.d("UNREGISTRATION_GCM", "Error! Status: " + status.getCode());
                    Toast.makeText(context, context.getResources().getString(R.string.ajax_bad_gcm_callback), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    static void checkNewAlerts(final Context context, String hostname, final String user_name, String guid, final MainFragmentPagerAdapter pagerAdapter) {
        AQuery aq = new AQuery(context);
        String url = "";

        //else {
        if (!hostname.startsWith("http://") && !hostname.startsWith("https://")) {
            url = "http://";
        }
        url += (hostname + "/getalerts.aspx?uname=" + user_name + "&computer=mobile" + "&fulldomain="+Settings.get(context).domain + "&cnt=cont&deskbar_id=%7B" + guid + "%7D&client_version=" + BuildConfig.VERSION_NAME);
                //context.getString(R.string.
        // ));

        Log.d("CHECK_NEW_ALERTS", "url: " + url);
        aq.ajax(url, XmlDom.class, new AjaxCallback<XmlDom>() {
            public void callback(String url, XmlDom xmlDom, AjaxStatus status) {
                if (xmlDom != null) {
                    Log.d("XML : ", xmlDom.toString());

                    if (xmlDom.children("ALERT") != null && xmlDom.children("ALERT").size() < 1) {
                        return;
                    }

                    LinkedList<Notification> newNotifications;
                    newNotifications = DBHelper.insertNotificationsForUser(context, xmlDom, user_name, false);
                    pagerAdapter.addNotifications(newNotifications);
                    pagerAdapter.refreshAllLists();
                }
            }
        });
    }

    private static void getEncryptionKey(final Context context, String hostname, final String username, final String password) {
        AQuery aq = new AQuery(context);
        String url;

        if (!hostname.startsWith("http://") && !hostname.startsWith("https://")) {
            url = "http://" + hostname + "/api/EncryptionKey" + "/GetEncryptionKey";
        } else {
            url = hostname +"/api/EncryptionKey" + "/GetEncryptionKey";
        }

        Map<String, Object> params = new HashMap<>();
        params.put("Username", username);
        params.put("Password", password);

        aq.ajax(url, params, String.class, new AjaxCallback<String>() {
            @Override
            public void callback(String url, String object, AjaxStatus status) {
                if (object != null && !object.isEmpty() && object.length() < 32) {
                    Settings.get(context).saveEncryptionKey(context, object.substring(1, object.length() - 1));
                } else {
                    Settings.get(context).saveEncryptionKey(context, "");
                }
            }
        });
    }

    static void getAlert(final Context context, String url, final String user_name, final WebView webView, final Notification notification) {
        AQuery aq = new AQuery(context);

        aq.ajax(url, String.class, new AjaxCallback<String>() {
            @Override
            public void callback(String url, String ajax_html, AjaxStatus status) {
                if (webView == null || ajax_html == null) {
                    Log.d("RESPONSE_ALERT_ERROR", "webView: " + webView + ", html: " + ajax_html);
                    return;
                }
                if (Settings.get(context).key != null && !Settings.get(context).key.isEmpty()) {
                    try {
                        ajax_html = decryptAlertBody(ajax_html, Settings.get(context).key);
                        ajax_html = replaceHostnameInJavascript(ajax_html, Settings.get(context).host);
                        webView.loadDataWithBaseURL(url, ajax_html, "text/html", "en_US", null);
                        DBHelper.saveContentForUser(context, ajax_html, notification, user_name);
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.d("RESPONSE_ALERT_ERROR", "webView: " + webView + ", html: " + ajax_html);
                    }
                } else {
                    ajax_html = replaceHostnameInJavascript(ajax_html, Settings.get(context).host);
                    webView.loadDataWithBaseURL(url, ajax_html, "text/html", "en_US", null);
                    DBHelper.saveContentForUser(context, ajax_html, notification, user_name);
                }
            }
        });
    }

    static void acknowledgeAlert(final Context context, final String hostname, final String username, final Notification notification, final Activity notificationActivity) {
        AQuery aq = new AQuery(context);
        String url;

        if (!hostname.startsWith("http://") && !hostname.startsWith("https://")) {
            url = "http://" + hostname + "/alert_read.asp?alert_id=" + notification.id + "&uname=" + username;
        } else {
            url = hostname + "/alert_read.asp?alert_id=" + notification.id + "&uname=" + username;
        }

        Log.d("ACKNOWLEDGE_ALERT", "url: " + url);
        aq.ajax(url, String.class, new AjaxCallback<String>() {
            public void callback(String url, String response, AjaxStatus status) {
                if (response != null && !response.isEmpty()) {
                    notificationActivity.finish();
                }
            }
        });
    }

    private static String replaceHostnameInJavascript(String html, String host) {
        Pattern pattern = Pattern.compile("location\\.replace\\(\"(.*?)\"\\)");
        Matcher matcher = pattern.matcher(html);
        if (matcher.find()) {
            String locationString = matcher.group();
            String urlString = matcher.group(1);
            urlString = replaceHostInUrl(urlString, host);
            html = html.replace(locationString, "location.replace(\"" + urlString + "\")");

            pattern = Pattern.compile("xhr.open\\('POST', '(.*?)'\\)");
            matcher = pattern.matcher(html);
            if (matcher.find()) {
                String xhrString = matcher.group();
                urlString = matcher.group(1);
                urlString = replaceHostInUrl(urlString, host);
                html = html.replace(xhrString, "xhr.open('POST', '" + urlString + "')");
            }
        }
        return html;
    }

    static void getSkin(final Context context, String skinUrl, final WebView webView) {
        AQuery aq = new AQuery(context);

        aq.ajax(skinUrl, String.class, new AjaxCallback<String>() {
            @Override
            public void callback(String url, String ajax_html, AjaxStatus status) {
                if (webView != null && ajax_html != null) {
                    String new_ajax_html = ajax_html.replaceAll("window\\.external\\.Close\\(\\);", "window.external.Close();console.log(\"Closed\");");
                    webView.loadDataWithBaseURL(url, new_ajax_html, "text/html", "en_US", null);
                } else {
                    Log.d("RESPONSE_SKIN_ERROR", "webView: " + webView + ", html: " + ajax_html);
                }
            }
        });
    }
}
