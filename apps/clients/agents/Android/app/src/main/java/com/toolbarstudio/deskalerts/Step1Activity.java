package com.toolbarstudio.deskalerts;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.toolbarstudio.deskalerts.event.PingError;
import com.toolbarstudio.deskalerts.event.RegistrationError;
import com.toolbarstudio.deskalerts.event.SuccessfulPing;
import com.toolbarstudio.deskalerts.event.SuccessfulRegistration;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;


/**
 * Created by brazil on 28.03.2016.
 */
public class Step1Activity extends BaseStepActivity {

    private Activity thisActivity;
    private EditText hostname_et;
    private ProgressBar progressBar;
    private boolean pingTry;
    private Settings settings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.step1_activity);
        settings = Settings.get(this);
        thisActivity = this;
        settings.generateGuid(false);

        hostname_et = findViewById(R.id.hostname_et);
        if (settings.host != null && !settings.host.equals("")) {
            hostname_et.setText(settings.host);
        } else {
            hostname_et.setText(thisActivity.getResources().getString(R.string.steps_default_host));
        }

        progressBar = findViewById(R.id.ping_progress);

        View next_button = findViewById(R.id.next_button);
        next_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pingTry = false;

                if (hostname_et.getText().toString().equals("")) {
                    Toast toast = Toast.makeText(getApplicationContext(),
                            "" + thisActivity.getResources().getString(R.string.steps_toast_fill_hostname), Toast.LENGTH_SHORT);
                    toast.show();
                } else {
                    if (isNetworkOnline()) {
                        settings.tmp_host = hostname_et.getText().toString();
                        progressBar.setVisibility(View.VISIBLE);
                        pingServerWithHttp("http://");
                    } else {
                        Toast.makeText(thisActivity, R.string.bad_network_connection, Toast.LENGTH_LONG).show();
                    }
                }

            }
        });
        View back_button = findViewById(R.id.back_button);
        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //finishAffinity();
    }

    private boolean isNetworkOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    private void pingServerWithHttp(String http) {
        AjaxHelper.pingServer(thisActivity, settings.tmp_host, http);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSuccessfulRegistration(SuccessfulRegistration event) {
        progressBar.setVisibility(View.GONE);

        Intent intent = new Intent(thisActivity, Step2aActivity.class);
        startActivity(intent);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSuccessfulPing(SuccessfulPing event) {
        if (!(settings.tmp_host.contains("http://") || settings.tmp_host.contains("https://"))) {
            if (pingTry) {
                settings.tmp_host = "https://" + settings.tmp_host;
            } else {
                settings.tmp_host = "http://" + settings.tmp_host;
            }
        }
        AjaxHelper.install(thisActivity, settings.tmp_host, settings.tmp_guid);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onPingError(PingError event) {
        if (settings.tmp_host.contains("http://") || settings.tmp_host.contains("https://")) {
            progressBar.setVisibility(View.GONE);
            Toast.makeText(this, getResources().getText(R.string.ajax_bad_callback), Toast.LENGTH_LONG).show();
        } else {
            if (pingTry) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(this, getResources().getText(R.string.ajax_bad_callback), Toast.LENGTH_LONG).show();
            } else {
                pingTry = true;
                pingServerWithHttp("https://");
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onRegistrationError(RegistrationError event) {
        progressBar.setVisibility(View.GONE);
        Toast.makeText(this, getResources().getText(R.string.ajax_bad_callback), Toast.LENGTH_LONG).show();
    }

}
