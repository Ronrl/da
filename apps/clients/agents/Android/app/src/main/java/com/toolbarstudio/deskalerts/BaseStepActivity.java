package com.toolbarstudio.deskalerts;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

public abstract class BaseStepActivity extends AppCompatActivity {

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        final Settings settings = Settings.get(this);
        savedInstanceState.putString("tmp_host", settings.tmp_host);
        savedInstanceState.putString("tmp_user_name", settings.tmp_user_name);
        savedInstanceState.putString("tmp_guid", settings.tmp_guid);
        savedInstanceState.putString("tmp_password", settings.tmp_password);
        savedInstanceState.putString("tmp_domain", settings.tmp_domain);
        Log.d("BaseStepActivity", "saveState" + savedInstanceState.toString());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState != null) {
            Log.d("BaseStepActivity", "restoreState" + savedInstanceState.toString());
            final Settings settings = Settings.get(this);
            settings.tmp_host = savedInstanceState.getString("tmp_host");
            settings.tmp_user_name = savedInstanceState.getString("tmp_user_name");
            settings.tmp_guid = savedInstanceState.getString("tmp_guid");
            settings.tmp_password = savedInstanceState.getString("tmp_password");
            settings.tmp_domain = savedInstanceState.getString("tmp_domain");
        }
    }
}
