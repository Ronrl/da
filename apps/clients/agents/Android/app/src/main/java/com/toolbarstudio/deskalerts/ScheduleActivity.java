package com.toolbarstudio.deskalerts;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.TimePicker;

/**
 * Created by brazil on 28.03.2016.
 */
public class ScheduleActivity extends AppCompatActivity {

    private Activity thisActivity;

    private TimePickerDialog tpd_start;
    private TimePickerDialog tpd_end;

    private TextView start_time_view;
    private TextView end_time_view;

    private int start_h = 0;
    private int start_m = 0;
    private int end_h = 0;
    private int end_m = 0;

    private CheckBox mon_cb;
    private CheckBox tue_cb;
    private CheckBox wed_cb;
    private CheckBox thu_cb;
    private CheckBox fri_cb;
    private CheckBox sat_cb;
    private CheckBox sun_cb;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.schedule_activity);

        thisActivity = this;

        start_time_view = findViewById(R.id.start_time_field);
        end_time_view = findViewById(R.id.end_time_field);

        tpd_start = new TimePickerDialog(thisActivity, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                start_time_view.setText(String.format(" %02d : %02d", hourOfDay, minute));
                start_h = hourOfDay;
                start_m = minute;
                if (start_h > end_h) {
                    end_h = start_h;
                    minute = 0;
                }
                if (end_h == start_h && start_m >= end_m) {
                    end_m = start_m + 1;
                }
                if (end_m > 59) {
                    end_h += minute / 60;
                    end_m = minute % 60;
                }
                if (end_h > 23) {
                    end_h = 0;
                    end_m = 59;
                }
                end_time_view.setText(String.format(" %02d : %02d", end_h, end_m));
            }
        }, 0, 0, true);
        tpd_end = new TimePickerDialog(thisActivity, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                if (hourOfDay < start_h) {
                    hourOfDay = start_h;
                    minute = 0;
                }
                if (hourOfDay == start_h && start_m >= minute) {
                    minute = start_m + 1;
                }
                if (minute > 59) {
                    hourOfDay += minute / 60;
                    minute = minute % 60;
                }
                if (hourOfDay > 23) {
                    hourOfDay = 0;
                }
                end_time_view.setText(String.format(" %02d : %02d", hourOfDay, minute));
                end_h = hourOfDay;
                end_m = minute;
            }
        }, 0, 0, true);

        start_time_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tpd_start.show();
            }
        });
        end_time_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tpd_end.show();
            }
        });

        mon_cb = findViewById(R.id.mon_cb);
        tue_cb = findViewById(R.id.tue_cb);
        wed_cb = findViewById(R.id.wed_cb);
        thu_cb = findViewById(R.id.thu_cb);
        fri_cb = findViewById(R.id.fri_cb);
        sat_cb = findViewById(R.id.sat_cb);
        sun_cb = findViewById(R.id.sun_cb);

        SharedPreferences preferences = thisActivity.getSharedPreferences("schedule", Context.MODE_PRIVATE);
        start_h = preferences.getInt("start_h", 0);
        start_m = preferences.getInt("start_m", 0);
        end_h = preferences.getInt("end_h", 0);
        end_m = preferences.getInt("end_m", 0);
        start_time_view.setText(String.format(" %02d : %02d", start_h, start_m));
        end_time_view.setText(String.format(" %02d : %02d", end_h, end_m));
        mon_cb.setChecked(preferences.getBoolean("mon", true));
        tue_cb.setChecked(preferences.getBoolean("tue", true));
        wed_cb.setChecked(preferences.getBoolean("wed", true));
        thu_cb.setChecked(preferences.getBoolean("thu", true));
        fri_cb.setChecked(preferences.getBoolean("fri", true));
        sat_cb.setChecked(preferences.getBoolean("sat", true));
        sun_cb.setChecked(preferences.getBoolean("sun", true));


        View save = findViewById(R.id.save_button);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences preferences = thisActivity.getSharedPreferences("schedule", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putInt("start_h", start_h);
                editor.putInt("start_m", start_m);
                editor.putInt("end_h", end_h);
                editor.putInt("end_m", end_m);
                editor.putBoolean("mon", mon_cb.isChecked());
                editor.putBoolean("tue", tue_cb.isChecked());
                editor.putBoolean("wed", wed_cb.isChecked());
                editor.putBoolean("thu", thu_cb.isChecked());
                editor.putBoolean("fri", fri_cb.isChecked());
                editor.putBoolean("sat", sat_cb.isChecked());
                editor.putBoolean("sun", sun_cb.isChecked());
                editor.commit();
                finish();
            }
        });


    }
}
