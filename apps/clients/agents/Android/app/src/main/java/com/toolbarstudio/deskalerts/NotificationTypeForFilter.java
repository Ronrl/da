package com.toolbarstudio.deskalerts;

/**
 * Created by brazil on 28.03.2016.
 */
public enum NotificationTypeForFilter {
    all, favorite, urgent, not_read
}
