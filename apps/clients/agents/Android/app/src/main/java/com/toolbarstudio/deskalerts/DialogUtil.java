package com.toolbarstudio.deskalerts;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.content.ContextCompat;

public final class DialogUtil {

    private static Dialog dialog;

    public static void openDialogProgress(final Context context, final String title, final String message) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setTitle(title);

        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.LOLLIPOP) {
            progressDialog.setIndeterminateDrawable(ContextCompat.getDrawable(context, R.drawable.progressdialog_circle));
            progressDialog.setIndeterminate(true);
        }

        progressDialog.setMessage(message);
        progressDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                DialogUtil.dialog = null;
            }
        });
        progressDialog.setCancelable(false);
        dialog = progressDialog;
        try {
            progressDialog.show();
        } catch (Exception ignore) {

        }
    }

    public static void hideDialog() {
        try {
            if (dialog != null && dialog.isShowing()) {
                dialog.dismiss();
            }
        } catch (Exception e) {
            dialog = null;
        }
    }


}
