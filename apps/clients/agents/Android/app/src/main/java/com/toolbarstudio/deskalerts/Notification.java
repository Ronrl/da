package com.toolbarstudio.deskalerts;

import android.content.Context;

/**
 * Created by brazil on 27.03.2016.
 */
public class Notification {
    public long id_db;
    public long id;
    public long timestamp;
    public String title;
    public String info_text;
    public boolean urgent_flag = false;
    public boolean survey_flag = false;
    public boolean read_flag = false;
    public boolean favorite_flag = false;
    public boolean acknowledgementFlag = false;
    public boolean selfDestructingFlag = false;

    public int leftMargin;
    public int topMargin;
    public int rightMargin;
    public int bottomMargin;

    public Notification(long id_db, long id, long timestamp, String title, String info_text, boolean urgent_flag,
                        boolean survey_flag, boolean read_flag, boolean favorite_flag, boolean acknowledgementFlag, boolean selfDestructingFlag,
                        int leftMargin, int topMargin, int rightMargin, int bottomMargin) {
        this.id_db = id_db;
        this.id = id;
        this.timestamp = timestamp;
        this.title = title;
        this.info_text = info_text;
        this.urgent_flag = urgent_flag;
        this.survey_flag = survey_flag;
        this.read_flag = read_flag;
        this.favorite_flag = favorite_flag;
        this.acknowledgementFlag = acknowledgementFlag;
        this.selfDestructingFlag = selfDestructingFlag;

        this.leftMargin = leftMargin;
        this.topMargin = topMargin;
        this.rightMargin = rightMargin;
        this.bottomMargin = bottomMargin;
    }

    public void setReadFlag(Context context, String userName) {
        read_flag = true;
        DBHelper.setReadFlagForUser(context, this, userName);
    }

    public void switchReadFlag(Context context, String userName) {
        read_flag = !read_flag;
        DBHelper.setReadFlagForUser(context, this, userName);
    }

    public void switchFavoriteFlag(Context context, String userName) {
        favorite_flag = !favorite_flag;
        DBHelper.setFavoriteFlagForUser(context, this, userName);
    }

    public void delete(Context context, String userName, MainFragmentPagerAdapter mainFragmentPagerAdapter) {
        mainFragmentPagerAdapter.deleteNotification(this);
        mainFragmentPagerAdapter.deleteFromMessages(this);
        DBHelper.deleteForUser(context, this, userName);
    }
}
