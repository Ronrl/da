package com.toolbarstudio.deskalerts;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by brazil on 27.03.2016.
 */
public class MoreFragment extends Fragment {
    private Context context;


    public static Fragment createListFragment(Context context) {
        MoreFragment fragment = new MoreFragment();
        fragment.context = context;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.more_fragment, container, false);

        View schedule_button = view.findViewById(R.id.schedule_button);
        schedule_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ScheduleActivity.class);
                startActivity(intent);

            }
        });
        View contact_button = view.findViewById(R.id.contact_us_button);
        contact_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ContactUsActivity.class);
                startActivity(intent);
            }
        });
        View login_wizard_button = view.findViewById(R.id.login_wizard_button);
        login_wizard_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, Step0Activity.class);
                startActivity(intent);
            }
        });

        return view;

    }
}
