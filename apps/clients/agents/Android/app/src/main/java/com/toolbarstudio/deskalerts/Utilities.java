package com.toolbarstudio.deskalerts;

import android.util.Base64;

import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class Utilities {
    public static String decryptAlertBody(final String html, String key) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        AlgorithmParameterSpec ivSpec = new IvParameterSpec("4276235117979925".getBytes(Charset.forName("US-ASCII")));
        byte[] keyBytes = key.getBytes(Charset.forName("US-ASCII"));
        byte[] preparedBytes = new byte[32];
        System.arraycopy(keyBytes, 0, preparedBytes, 0, keyBytes.length < 32 ? keyBytes.length : preparedBytes.length);
        SecretKeySpec newKey = new SecretKeySpec(preparedBytes, "AES");

        Cipher cipher;
        cipher = Cipher.getInstance("AES/CFB/NoPadding");
        cipher.init(Cipher.DECRYPT_MODE, newKey, ivSpec);
        byte[] decodedBytes = Base64.decode(html, Base64.DEFAULT);
        byte[] bytes = cipher.doFinal(decodedBytes);
        return new String(bytes, Charset.forName("UTF-8"));
    }

    public static String replaceHostInUrl(String url, String newHost) {
        try {
            URL originalUrl = new URL(url);
            if (!newHost.contains("http://") && !newHost.contains("https://")) {
                newHost = "http://" + newHost;
            }
            URL hostUrl = new URL(newHost);
            URL newUrl = new URL(originalUrl.getProtocol(), hostUrl.getHost(), originalUrl.getPort(), originalUrl.getFile());
            return newUrl.toString();
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return url;
        }
    }
}
