package com.toolbarstudio.deskalerts;

import android.app.Dialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.LinkedList;

/**
 * Created by brazil on 27.03.2016.
 */
public class ListFragmentAdapter extends BaseAdapter {
    private Context context;
    private LinkedList<Notification> list;
    private MainFragmentPagerAdapter pagerAdapter;
    private boolean show_type_flag = true;
    private Dialog dialog;
    private TextView open_button;
    private TextView favorite_button;
    private TextView read_button;
    private TextView delete_button;
    private Settings settings;

    public ListFragmentAdapter(Context context, LinkedList<Notification> list, boolean show_type_flag, MainFragmentPagerAdapter pagerAdapter) {
        this.context = context;
        settings = Settings.get(context);

        if (list == null) {
            this.list = new LinkedList<>();
        } else {
            this.list = list;
        }
        this.show_type_flag = show_type_flag;
        this.pagerAdapter = pagerAdapter;
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.notification_dialog);
        open_button = dialog.findViewById(R.id.open_button);
        favorite_button = dialog.findViewById(R.id.favorite_button);
        read_button = dialog.findViewById(R.id.old_post_button);
        delete_button = dialog.findViewById(R.id.delete_button);
    }

    public void setList(LinkedList<Notification> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        if (list != null) {
            return list.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, final View convertView, final ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = convertView;
        if (view == null) {
            view = inflater.inflate(R.layout.list_item, parent, false);
        }
        Holder holder = new Holder(view, (Notification) getItem(position));

        view.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (list.get(position).favorite_flag) {
                    favorite_button.setText(context.getResources().getString(R.string.alert_dialog_fav_off));
                } else {
                    favorite_button.setText(context.getResources().getString(R.string.alert_dialog_fav_on));
                }
                if (list.get(position).read_flag) {
                    read_button.setText(context.getResources().getString(R.string.alert_dialog_read_off));
                } else {
                    read_button.setText(context.getResources().getString(R.string.alert_dialog_read_on));
                }

                open_button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.cancel();
                        list.get(position).setReadFlag(context, settings.user_name);
                        Intent intent = new Intent(context, NotificationActivity.class);
                        intent.putExtra("notificationDbId", list.get(position).id_db);
                        context.startActivity(intent);
                        pagerAdapter.refreshAllLists();

                        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                        notificationManager.cancelAll();
                    }
                });
                favorite_button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        list.get(position).switchFavoriteFlag(context, settings.user_name);
                        dialog.cancel();
                        pagerAdapter.refreshAllLists();
                    }
                });
                read_button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        list.get(position).switchReadFlag(context, settings.user_name);
                        dialog.cancel();
                        pagerAdapter.refreshAllLists();
                    }
                });
                delete_button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        list.get(position).delete(context, settings.user_name, pagerAdapter);
                        dialog.cancel();
                        pagerAdapter.refreshAllLists();
                    }
                });
                dialog.show();
                return true;
            }
        });
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                list.get(position).setReadFlag(context, settings.user_name);
                Intent intent = new Intent(context, NotificationActivity.class);
                intent.putExtra("notificationDbId", list.get(position).id_db);
                context.startActivity(intent);
                pagerAdapter.refreshAllLists();

                NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.cancelAll();
            }
        });

        return view;
    }

    private class Holder {
        View main_layout;
        View urgent_mark;
        ImageView type_mark;
        TextView title_tv;
        TextView info_tv;
        View favorite_flag;

        Holder(View view, Notification notification) {
            main_layout = view.findViewById(R.id.main_layout);
            urgent_mark = view.findViewById(R.id.urgent_mark);
            type_mark = view.findViewById(R.id.type_mark);
            title_tv = view.findViewById(R.id.title_tv);
            info_tv = view.findViewById(R.id.info_tv);
            favorite_flag = view.findViewById(R.id.favorite_flag);

            title_tv.setText(notification.title.replaceAll("&nbsp;", "\u00A0"));
            info_tv.setText(notification.info_text);

            if (show_type_flag) {
                type_mark.setVisibility(View.VISIBLE);
                if (notification.survey_flag) {
                    type_mark.setImageResource(R.drawable.icon_surveys);
                } else {
                    type_mark.setImageResource(R.drawable.icon_alerts);
                }
            } else {
                type_mark.setVisibility(View.GONE);
            }
            if (notification.urgent_flag) {
                urgent_mark.setVisibility(View.VISIBLE);
            } else {
                urgent_mark.setVisibility(View.INVISIBLE);
            }
            if (notification.read_flag) {
                main_layout.setBackgroundColor(context.getResources().getColor(R.color.active_bg));
            } else {
                main_layout.setBackgroundColor(context.getResources().getColor(R.color.mark_bg));
            }
            if (notification.favorite_flag) {
                favorite_flag.setVisibility(View.VISIBLE);
            } else {
                favorite_flag.setVisibility(View.INVISIBLE);
            }
        }
    }
}
