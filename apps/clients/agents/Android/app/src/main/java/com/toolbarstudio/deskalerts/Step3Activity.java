package com.toolbarstudio.deskalerts;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;

/**
 * Created by brazil on 28.03.2016.
 */
public class Step3Activity extends BaseStepActivity {

    private Activity thisActivity;

    private Settings settings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.step3_start);
        settings = Settings.get(this);

        thisActivity = this;

        WebView webView = findViewById(R.id.web_view);
        View next_button = findViewById(R.id.next_button);
        if (next_button != null) {
            next_button.setVisibility(View.GONE);
            next_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(thisActivity, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);

                    SharedPreferences prefs = getSharedPreferences("TOKEN_PREF", MODE_PRIVATE);
                    String token = prefs.getString("token", "");

                    AjaxHelper.deviceUnRegister( thisActivity, settings.host, settings.user_name, settings.domain, settings.guid, token);
                    AjaxHelper.deviceRegistration( thisActivity, settings.tmp_host, settings.tmp_user_name,  settings.tmp_domain, settings.tmp_guid, token, android.os.Build.VERSION.SDK_INT );
                    settings.save(thisActivity);
                }
            });
        }

        AjaxHelper.userRegistration(thisActivity, settings.tmp_host, settings.tmp_user_name, settings.tmp_password, settings.tmp_guid, webView, next_button, settings.tmp_domain);

        View back_button = findViewById(R.id.back_button);
        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
