//
//  AppDelegate.swift
//  DeskAlertsScreenSaverTest
//
//  Created by Sergey Dmitriev on 28/12/2018.
//  Copyright © 2018 Toolbarstudio Inc. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    @IBOutlet weak var window: NSWindow!
    lazy var screenSaverView = DeskAlertsScreenSaverView(frame: NSZeroRect, isPreview: false)

    func applicationDidFinishLaunching(_ aNotification: Notification) {
        if let screenSaverView = screenSaverView,
            let contentView = window.contentView {
            screenSaverView.frame = contentView.bounds;
            contentView.addSubview(screenSaverView);
        }
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

