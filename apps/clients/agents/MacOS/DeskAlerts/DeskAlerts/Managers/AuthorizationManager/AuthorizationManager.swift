//
//  AuthorizationManager.swift
//  DeskAlerts
//
//  Created by Ярослав Долговых on 14/11/2019.
//  Copyright © 2019 Toolbarstudio Inc. All rights reserved.
//

import Foundation
import Cocoa

enum AuthorizationType: String, Codable {
    case activeDirectory = "ActiveDirectory"
    case activeDirectoryWithSyncFree = "ActiveDirectoryWithSyncFree"
    case computerName = "ComputerName"
    case selfRegistaration = "SelfRegistaration"
}

enum AuthorizationState {
    case notAuthorized
    case authorized
}

fileprivate struct Keys {
    static let deskbarId = "DADeskbarId"
}

fileprivate struct APIV1Constants {
    static let requestTokenForUser = "/api/v1/security/token/user"
    static let requestTokenForADUser = "/api/v1/security/token/aduser"
}

fileprivate struct Constants {
    static let registrationFormPath = "/RegisterForm.aspx?"
    static let clientType = "client_id=3"
    static let defaultUserPassword = "upass=hed"
    static let disableUserNameChange = "disableUnameChange=1"
    static let syncFree = "syncfree=1"
    static let ampersandMask = "&"
    
    static let deskbarIDPrefix = "desk_id="
    static let userNamePrefix = "uname="
    static let domainNamePrefix = "domain_name="
    
    static let adEnabled = "ad_enabled=1"
}

class AuthorizationManager {
    static let shared = AuthorizationManager()
    
    var authorizationType: AuthorizationType = .activeDirectory
    
    init() {
        authorizationType = ClientSettings.shared.settingsList.authorizationType
    }
    
    var deskbarId: String {
        get {
            return Config.SessionUser!.deskalerts_id!
        }
    }
    var token: String?
    
    var computerName = Host.current().localizedName!.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    

    
    //MARK: - RegisterUser
    func registerUser() {
        showRegistrationForm()
//        switch authorizationType {
//        case .activeDirectoryWithSyncFree:
//            break
//        case .selfRegistaration, .computerName, .activeDirectory:
//            showRegistrationForm()
//        }
    }

    //MARK: - GenerateRegistrationFormPath
    private func generateRegistrationFormPath() -> String? {
        let serverPath = ClientSettings.shared.settingsList.serverPath
        var urlString = serverPath + Constants.registrationFormPath
        var urlParameters = [String]()
        
        let deskbarIdParameter = Constants.deskbarIDPrefix + deskbarId
        urlParameters.append(deskbarIdParameter)
        urlParameters.append(Constants.clientType)
        
        switch authorizationType {
        case .activeDirectory:
            if let domainName = DomainManager.shared.domainName,
               let domainNameForURL = domainName.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed),
               let userName = Config.SessionUser?.name {
                let domainNameParameter = Constants.domainNamePrefix + domainNameForURL
                urlParameters.append(domainNameParameter)
                let userNameParameter = Constants.userNamePrefix + userName
                urlParameters.append(userNameParameter)
                urlParameters.append(Constants.defaultUserPassword)
            } else {
                print("Error: invalid domainName or userName")
            }
            
            urlParameters.append(Constants.disableUserNameChange)
            urlParameters.append(Constants.adEnabled)
        case .computerName:
            let computerNameParameter = Constants.userNamePrefix + computerName
            urlParameters.append(computerNameParameter)
            urlParameters.append(Constants.defaultUserPassword)
            urlParameters.append(Constants.disableUserNameChange)
        case .selfRegistaration:
            if let _ = DomainManager.shared.domainName {
                urlParameters.append(Constants.adEnabled)
            }
        case .activeDirectoryWithSyncFree:
            if let userName = Config.SessionUser?.name {
                let userNameParameter = Constants.userNamePrefix + userName
                urlParameters.append(userNameParameter)
                urlParameters.append(Constants.defaultUserPassword)
            } else {
                print("Error: invalid userName")
            }
            if let domainName = DomainManager.shared.domainName,
                let domainNameForURL = domainName.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) {
                let domainNameParameter = Constants.domainNamePrefix + domainNameForURL
                urlParameters.append(domainNameParameter)
            } else {
                print("Error: invalid domainName")
            }
            
            urlParameters.append(Constants.disableUserNameChange)
//            urlParameters.append(Constants.adEnabled)
            urlParameters.append(Constants.syncFree)
        }
        
        for parameter in urlParameters {
            urlString += parameter
            if let parameterIndex = urlParameters.index(of: parameter), parameterIndex < urlParameters.count - 1 {
                urlString += Constants.ampersandMask
            }
        }
        print(urlString)
        return urlString
    }
    
    //MARK: - ShowRegistrationForm
    private func showRegistrationForm() {
        guard let registrationFormPath = generateRegistrationFormPath() else { return }
        
        let alert = Alert()
        guard let windowDataXMLList = Config.XML?["ALERTS"]["VIEWS"]["WINDOW"] else { return }
        do {
            let windowDataXML = try windowDataXMLList.withAttribute("name","minibrowserwindow")
            let miniBrowserWindow = Config.shared.GetAlertWindowFromXml(xml: windowDataXML, context: nil, alert: alert)
            alert.addToWindows(miniBrowserWindow)
            let storyboard = NSStoryboard(name: .windowCommandStoryboard, bundle: nil)
            guard let windowCommand = storyboard.instantiateController(withIdentifier: .windowCommandSceneIdentifier) as? WindowCommand else { return }
            windowCommand.Show(alert: alert,
                               href: registrationFormPath,
                               islocal: false,
                               includedtickeralerts: nil,
                               includedtickeralertscontent: nil,
                               windowType: .authorizationForm)
            
        } catch {
            print(error.localizedDescription)
            return
        }
    }
}
