//
//  DatabaseManager.swift
//  DeskAlerts
//
//  Created by Ярослав Долговых on 03/09/2019.
//  Copyright © 2019 Toolbarstudio Inc. All rights reserved.
//

import Foundation
import Cocoa
import Alamofire
import SWXMLHash

private struct Constants {
    static let emptyString = ""
}

enum BaseContentType: String, CaseIterable {
    case alert
    case wallpaper
}

private enum Entity: String {
    case alert = "Alert"
    case alertWindow = "AlertWindow"
    case user = "User"
}

private enum AlertPropertyKeys: String {
    case id = "id"
    case users = "users"
    case isClosed = "isclosed"
    case alertID = "alert_id"
}

class DatabaseManager {
    static let shared = DatabaseManager()
    private let context = CoreDataManager.instance.managedObjectContext
    
    func getAlert(by id: String) -> Alert? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        let entityDescription = NSEntityDescription.entity(forEntityName: Entity.alert.rawValue, in: context)
        let predicate = NSPredicate(format: "%K == %@", argumentArray: [AlertPropertyKeys.alertID.rawValue, id])
        fetchRequest.entity = entityDescription
        fetchRequest.predicate = predicate
        do {
            guard let results = try context.fetch(fetchRequest) as? [Alert] else { return nil }
            return results.first
        } catch {
            print(error.localizedDescription)
            return nil
        }
    }
    
    func getAlerts(by user: User, type: BaseContentType, isClosed: Bool) -> [Alert] {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        let entityDescription = NSEntityDescription.entity(forEntityName: Entity.alert.rawValue, in: context)
        
        let predicate = NSPredicate(format: "%K == %@ AND %K == %@ AND %K == %@", argumentArray: [AlertPropertyKeys.users.rawValue, user,
             AlertPropertyKeys.id.rawValue, type.rawValue,
             AlertPropertyKeys.isClosed.rawValue, isClosed])
        
        fetchRequest.entity = entityDescription
        fetchRequest.predicate = predicate
        do {
            guard let results = try context.fetch(fetchRequest) as? [Alert] else {
                DebugLogger.showAlert(with: "Error load alerts from database")
                return [Alert]()
            }
            return results
        } catch {
            print(error.localizedDescription)
            return [Alert]()
        }
    }
    
    
    func setAllWallpapersStateIsClosed() {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        let entityDescription = NSEntityDescription.entity(forEntityName: Entity.alert.rawValue, in: self.context)
        fetchRequest.entity = entityDescription
        do {
            guard let alerts = try context.fetch(fetchRequest) as? [Alert] else { return }
            let filteredAlerts = alerts.filter { (alert) -> Bool in
                alert.alert_id != nil && alert.class_id == "8"
            }
            
            for item in filteredAlerts {
                item.isclosed = true
            }
            CoreDataManager.instance.saveContext()
        }
        catch {
            print(error.localizedDescription)
        }
    }
    
    func processingAlertsBeforeClosing(_ alerts: [Alert]) {
        for item in alerts {
            guard let id = item.alert_id else { return }
            processingAlertBeforeClosing(by: id)
        }
    }
    
    func processingAlertBeforeClosing(alert: Alert) {
        guard let id = alert.alert_id else { return }
        processingAlertBeforeClosing(by: id)
    }
    
    func processingAlertBeforeClosing(by id: String) {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        let entityDescription = NSEntityDescription.entity(forEntityName: Entity.alert.rawValue, in: context)
        fetchRequest.entity = entityDescription
        fetchRequest.predicate = NSPredicate(format: "alert_id == %@", id)
        do {
            guard let alerts = try context.fetch(fetchRequest) as? [Alert] else { return }
            for item in alerts {
                if item.self_deletable == "1" {
//                    context.delete(item)
                }
                sendReadItMessage(alert: item)
                item.isreaded = true
                item.isclosed = true
                item.acknowledged = true
            }
            CoreDataManager.instance.saveContext()
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func sendReadItMessage(alert: Alert) {
        if alert.haveAcknowledgeProperty {
            guard let alertID = alert.alert_id else { return }
            guard let token = AuthorizationManager.shared.token else { return }
            RequestManager.shared.sendConfirmationOfReadAlert(by: alertID, token: token)
        }
    }
    
    func haveUnreadAlert() -> Bool {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        let entityDescription = NSEntityDescription.entity(forEntityName: Entity.alert.rawValue, in: context)
        fetchRequest.entity = entityDescription
        fetchRequest.predicate = NSPredicate(format: "%K == %@ AND %K == %@", argumentArray: ["id", "alert", "isreaded", false])
        
        do {
            let unreadAlerts = try context.fetch(fetchRequest)
            if unreadAlerts.count == 0 {
                return false
            } else {
                return true
            }
        } catch {
            print(error.localizedDescription)
            return false
        }
    }
    
    func setReadedState(from alert: Alert) {
        guard let id = alert.alert_id else { return }
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        let entityDescription = NSEntityDescription.entity(forEntityName: Entity.alert.rawValue, in: context)
        fetchRequest.entity = entityDescription
        fetchRequest.predicate = NSPredicate(format: "alert_id == %@", id)
        do {
            guard let tmpAlert = try context.fetch(fetchRequest).first as? Alert else { return }
            tmpAlert.isreaded = true
            CoreDataManager.instance.saveContext()
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func getAlertsNotClosedInPreviousSession() -> [Alert]? {
        guard let sessionUser = Config.SessionUser else { return nil }
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        let entityDescription = NSEntityDescription.entity(forEntityName: Entity.alert.rawValue, in: context)
        fetchRequest.entity = entityDescription
        do {
            guard let alerts = try context.fetch(fetchRequest) as? [Alert] else { return nil }
            
            let notClosedAlerts = alerts.filter { (alert) -> Bool in
                let todayDate = Date()
                guard let toDate = alert.to_date as NSString? else { return false }
                guard let windows = alert.windows else { return false }
                
                let endLifeDate = Date(timeIntervalSince1970: toDate.doubleValue)
                if alert.isclosed == false &&
                alert.users == sessionUser &&
                endLifeDate >= todayDate &&
                alert.class_id != "16" &&
                windows.count > 0 {
                    return true
                }
                return false
            }
            
            return notClosedAlerts
        }
        catch {
           print(error.localizedDescription)
           return nil
        }
    }
    
    func remove(_ alert: Alert) {
        context.delete(alert)
        CoreDataManager.instance.saveContext()
    }
    
    func alertIsReceived(by id: String) -> Bool {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        let entityDescription = NSEntityDescription.entity(forEntityName: Entity.alert.rawValue, in: context)
        fetchRequest.entity = entityDescription
        fetchRequest.predicate = NSPredicate(format: "alert_id == %@", id)
        
        do {
            guard let alerts = try context.fetch(fetchRequest) as? [Alert] else { return false }
            if alerts.count != 0 && alerts.first?.class_id != "8" {
                return true
            } else {
                return false
            }
        } catch {
            print(error.localizedDescription)
        }
        return false
    }
    
    func setClosedStatusForAllScreensavers() {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        let entityDescription = NSEntityDescription.entity(forEntityName: Entity.alert.rawValue, in: context)
        fetchRequest.entity = entityDescription
        fetchRequest.predicate = NSPredicate(format: "%K == %@", argumentArray: ["id", "screensaver"])

        do {
            guard let screensavers = try context.fetch(fetchRequest) as? [Alert] else { return }
            for item in screensavers {
                item.isclosed = true
            }
            CoreDataManager.instance.saveContext()
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func deleteAllAlerts() {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        let entityDescription = NSEntityDescription.entity(forEntityName: Entity.alert.rawValue, in: context)
        fetchRequest.entity = entityDescription
        do {
            if let results = try context.fetch(fetchRequest) as? [Alert] {
                for item in results {
                    remove(item)
                }
            }
        } catch {
            DebugLogger.showAlert(with: "Error load alerts from database")
            print(error.localizedDescription)
        }
    }
    
    func removeAlertFromHistoryByID(id: String) {
        guard let alert = getAlert(by: id) else { return }
        remove(alert)
    }
    
    func insert(alert: Alert) {
        context.insert(alert)
        CoreDataManager.instance.saveContext()
    }
    
    func setIsClosedStateFalseToAlert(by xml: XMLIndexer) {
        guard let id = xml.element?.attribute(by: "alert_id")?.text else { return }
        guard let alert = getAlert(by: id) else { return }
        alert.isclosed = false
        CoreDataManager.instance.saveContext()
    }
    
    //MARK: - User method
    func getUsersFromDatabase() -> [User]? {
        let userName = NSUserName()
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        let entityDescription = NSEntityDescription.entity(forEntityName: "User", in: CoreDataManager.instance.managedObjectContext)
        fetchRequest.entity = entityDescription
        fetchRequest.predicate = NSPredicate(format: "name == %@", userName)
        do {
            guard let users = try context.fetch(fetchRequest) as? [User] else {
                return nil
            }
            return users
        } catch {
            print(error.localizedDescription)
        }
        return nil
    }
    
    func deleteAllUsersFromDatabase() {
        guard let users = getUsersFromDatabase() else { return }
        do {
            for item in users {
                context.delete(item)
            }
            try context.save()
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func insertUserToDatabase(user: User) {
        do {
            context.insert(user)
            try context.save()
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func getCountActiveScreenSavers() -> Int {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        let entityDescription = NSEntityDescription.entity(forEntityName: Entity.alert.rawValue, in: context)
        fetchRequest.entity = entityDescription
        fetchRequest.predicate = NSPredicate(format: "%K == %@ AND %K == %@",
                                             argumentArray: ["id", "screensaver", "isclosed", "NO"])

        do {
            guard let screensavers = try context.fetch(fetchRequest) as? [Alert] else { return 0 }
            return screensavers.count
        } catch {
            print(error.localizedDescription)
        }
        return 0
    }
}
