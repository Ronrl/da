//
//  DebugLogger.swift
//  DeskAlerts
//
//  Created by Ярослав Долговых on 25/02/2020.
//  Copyright © 2020 Toolbarstudio Inc. All rights reserved.
//

import Foundation
import Cocoa

class DebugLogger {
    class func showAlert(with messageText: String,
                         filePath: String = #file,
                         functionName: String = #function,
                         lineNumber: Int = #line) {
        if ClientSettings.shared.settingsList.enableDebugMode {
            guard let fileName = filePath.split(separator: "/").last else { return }
            let alert = NSAlert()
            alert.messageText = messageText
            alert.informativeText = "file: \(fileName)\nfunction: \(functionName)\nline: \(lineNumber)"
            alert.alertStyle = .warning
            alert.addButton(withTitle: "OK")
            alert.runModal()
        }
    }
}
