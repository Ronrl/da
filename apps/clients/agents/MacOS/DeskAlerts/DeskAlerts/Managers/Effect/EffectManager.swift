//
//  EffectManager.swift
//  DeskAlerts
//
//  Created by Ярослав Долговых on 10/09/2019.
//  Copyright © 2019 Toolbarstudio Inc. All rights reserved.
//

import Foundation
import AVFoundation

private struct Constants {
    static let audioFileName = "notify"
    static let audioFileType = "wav"
    static let audioFileSubdirectory = "Resources"
}

class EffectManager {
    static let shared = EffectManager()
    //MARK: - Sound
    fileprivate lazy var soundReceivingAlert: AVAudioPlayer? = {
        guard let url = Bundle.main.url(forResource: Constants.audioFileName, withExtension: Constants.audioFileType, subdirectory: Constants.audioFileSubdirectory) else {
            print("Error create sound url")
            return nil
        }
        
        do {
            let player = try AVAudioPlayer(contentsOf: url)
            return player
        } catch let error {
            print(error.localizedDescription)
            return nil
        }
    }()
    
    func playSoundReceivingAlert() {
        guard let player = soundReceivingAlert else { return }
        player.play()
    }
}
