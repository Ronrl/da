//
//  DomainManager.swift
//  DeskAlerts
//
//  Created by Ярослав Долговых on 27/11/2019.
//  Copyright © 2019 Toolbarstudio Inc. All rights reserved.
//

import Foundation

fileprivate struct Constants {
    static let dsAttributeTypeNativeUserPrincipalName = "dsAttrTypeNative:userPrincipalName"
    static let altSecurityIdentities = "AltSecurityIdentities"
    static let authenticationAuthority = "AuthenticationAuthority"
    static let id = "id"
    static let groupsPrefix = "groups="
    static let groupSeparator: Character = ","
    static let domainLevelSeparator: Character = "."
    static let domainNameSeparator: Character = "@"
}

class DomainManager {
    
    static let shared = DomainManager()
    
    //MARK: - Public
    var domainName: String? = nil
    var userDomainGroups: [String]? {
        guard let domainName = domainName else { return nil }
        DebugLogger.showAlert(with: "Debug domainName: \(domainName)")
        guard let shorDomainName = domainName.split(separator: Constants.domainLevelSeparator).first else { return nil }
        DebugLogger.showAlert(with: "Debug shorDomainName: \(shorDomainName)")
        let uppercasedDomainName = shorDomainName.uppercased()
        DebugLogger.showAlert(with: "Debug uppercasedDomainName: \(uppercasedDomainName)")
        guard let allUserGroups = getUserGroups() else { return nil }
        DebugLogger.showAlert(with: "Debug allUserGroups: \(allUserGroups)")
        let userGroupsByDomainName = getUserGroups(by: uppercasedDomainName, userGroups: allUserGroups)
        DebugLogger.showAlert(with: "Debug userGroupsByDomainName: \(userGroupsByDomainName)")
        return userGroupsByDomainName
    }
    
    private func getDomainNameFromAuthenticationAuthorityProperty() -> String? {
        if let authenticationAuthority = getDSCLData(by: Constants.authenticationAuthority) {
            let items = authenticationAuthority.split(separator: ";")
            for item in items {
                if item.contains(Constants.domainNameSeparator) {
                    print(item)
                    guard let domainName = item.split(separator: Constants.domainNameSeparator).last else {
                        DebugLogger.showAlert(with: "Error: splite string for domainNameSeparator(@)")
                        return nil
                    }
                    return String(domainName)
                }
            }
            DebugLogger.showAlert(with: "In AuthenticationAuthority property domain name not found")
        } else {
            DebugLogger.showAlert(with: "Error: AuthenticationAuthority field not found")
        }
        return nil
    }
    
    //MARK: - Init
    init() {
        if let domainName = getDomainNameFromAuthenticationAuthorityProperty() {
            DebugLogger.showAlert(with: "Domain name by AuthenticationAuthority = \(domainName)")
            self.domainName = domainName
            return
        }
        if let userPrincipalName = getDSCLData(by: Constants.dsAttributeTypeNativeUserPrincipalName) {
            if let domainName = getDomainName(by: userPrincipalName) {
                self.domainName = domainName
                DebugLogger.showAlert(with: "Domain name by userPrincipalName = \(domainName)")
                return
            } else {
                DebugLogger.showAlert(with: "Error parsing data: \(userPrincipalName)")
            }
        } else {
            DebugLogger.showAlert(with: "Error: UserPrincipalName field not found")
        }

        if let altSecurityIdentities = getDSCLData(by: Constants.altSecurityIdentities) {
            if let domainName = getDomainName(by: altSecurityIdentities) {
                self.domainName = domainName
                DebugLogger.showAlert(with: "Domain name by altSecurityIdentities = \(domainName)")
                return
            } else {
                DebugLogger.showAlert(with: "Error parsing data: \(altSecurityIdentities)")
            }
        } else {
            DebugLogger.showAlert(with: "Error: AltSecurityIdentities field not found")
        }

        DebugLogger.showAlert(with: "Error: Domain name is not found. Set to default empty string.")
        self.domainName = String.emptyString
    }
    
    //MARK: - Private
    private func getUserGroups(by domainName: String, userGroups: String) -> [String] {
        let groups = userGroups.split(separator: Constants.groupSeparator)
        var domainGroup = [String]()

        for item in groups {
            if item.contains(domainName) {
                
                guard let range = item.range(of: "\(domainName)\\") else { continue }
                let startIndex = range.upperBound
                let endIndex = item.index(item.endIndex, offsetBy: -1)
                
                let groupName = item[startIndex..<endIndex]
                
                domainGroup.append(String(groupName))
            }
        }
        return domainGroup
    }
    
    private func getDomainName(by dsclData: String) -> String? {
        guard let domainName = dsclData.split(separator: "@").last else { return nil }
        let trimmedDomainName = domainName.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        return String(trimmedDomainName)
    }
    
    private func getUserGroups() -> String? {
        let pipe = Pipe()
        let task = Process()
        task.launchPath = "/usr/bin/id"
        task.standardOutput = pipe
        
        let handle = pipe.fileHandleForReading
        task.launch()
        task.waitUntilExit()
        
        if task.terminationStatus == 0 {
            let data = handle.readDataToEndOfFile()
            guard let stringData = String(data: data, encoding: .ascii) else { return nil }
            print(stringData)
            let tmpStringData = stringData.replacingOccurrences(of: "\n", with: "")
            guard let groupsListString = tmpStringData.components(separatedBy: Constants.groupsPrefix).last else { return nil }
            return groupsListString
        }
        return nil
    }
    
    private func getDSCLData(by dsAttribute: String) -> String? {
        let arguments = ["/Search", "-read", "/Users/" + NSUserName(), dsAttribute]
        let pipe = Pipe()
        let task = Process()
        task.launchPath = "/usr/bin/dscl"
        task.arguments = arguments
        task.standardOutput = pipe
        
        let handle = pipe.fileHandleForReading
        task.launch()
        task.waitUntilExit()
        
        if task.terminationStatus == 0 {
            let data = handle.readDataToEndOfFile()
            guard let stringData = String(data: data, encoding: .ascii) else { return nil }
            if stringData.isEmpty { return nil }
            print(stringData)
            return stringData
        }
        return nil
    }
}
