//
//  WindowCoordinator.swift
//  DeskAlerts
//
//  Created by Ярослав Долговых on 15/10/2019.
//  Copyright © 2019 Toolbarstudio Inc. All rights reserved.
//

import Foundation

class WindowCoordinator {
    static let shared = WindowCoordinator()
    
    func terminateAlerts(by alertsID: [Int]) {
        for item in alertsID {
            let stringID = String(item)
            closeWindow(by: stringID)
            RequestManager.shared.sendСonfirmationOfTerminateAlert(by: stringID)
        }
    }
    
    private func closeWindow(by alertID: String) {
        guard let terminatedAlert = DatabaseManager.shared.getAlert(by: alertID) else {
            print("\(#function) - Error: alert with the given id is not in the database")
            return
        }
        guard let closedWindow = searchWindowDisplaying(terminatedAlert) else { return }
        DatabaseManager.shared.processingAlertBeforeClosing(alert: terminatedAlert)
        closedWindow.Close()
        if terminatedAlert.isTiker {
            rebootTickerStack(by: closedWindow, with: terminatedAlert)
        }
    }
    
    private func searchWindowDisplaying(_ alert: Alert) -> WindowCommand? {
        guard let windowContainer = Config.WindowContainer else { return nil }
        for window in windowContainer {
            var alertsInWindow = [Alert]()
            guard let baseAlert = window.alert else { continue }
            alertsInWindow.append(baseAlert)
            if let includedTickerAlerts = window.includedTickerAlerts {
                alertsInWindow.append(contentsOf: includedTickerAlerts)
            }
            for item in alertsInWindow {
                if item == alert {
                    return window
                }
            }
        }
        return nil
    }
    
    private func rebootTickerStack(by window: WindowCommand, with terminatedTicker: Alert) {
        guard let baseTicker = window.alert else { return }
        let includedTickers = window.includedTickerAlerts ?? [Alert]()
        var tickerStack = [Alert]()
        tickerStack.append(baseTicker)
        tickerStack.append(contentsOf: includedTickers)
        
        tickerStack.removeAll { (item) -> Bool in
            item == terminatedTicker
        }
        showTickerStack(tickerStack)
    }
    
    private func showTickerStack(_ tickers: [Alert]) {
        var tmpArray = tickers
        if tmpArray.count > 0 {
            if let baseTiker = tmpArray.first {
                tmpArray.removeFirst()
                Loader.sharedInstance.redisplayTikers(baseTiker: baseTiker, supplementTikers: tmpArray)
            }
        }
    }
    
    func testTerminate(with delay: Double, and alertID: String) {
        Timer.scheduledTimer(withTimeInterval: delay, repeats: false) { (timer) in
            self.closeWindow(by: alertID)
        }
    }
}
