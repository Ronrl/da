//
//  SettingManager.swift
//  DeskAlerts
//
//  Created by Ярослав Долговых on 10/09/2019.
//  Copyright © 2019 Toolbarstudio Inc. All rights reserved.
//

import Foundation

private struct Key {
    static let audioEffect = "DAAudioEffect"
    static let defaultParameters = "DADefaultParameters"
    static let unobtrusiveMode = "DAUnobtrusiveMode"
}

class SettingsManager {
    static let shared = SettingsManager()
    
    private var isDefaultParametersSet: Bool {
        get {
            return UserDefaults.standard.bool(forKey: Key.defaultParameters)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: Key.defaultParameters)
        }
    }
    //MARK: - Activate
    func activate() {
        if !isDefaultParametersSet {
            isDefaultParametersSet = true
            audioEffectEnabled = true
            unobtrusiveModeEnabled = false
        }
    }
    
    //MARK: - AudioEffectEnabled
    var audioEffectEnabled: Bool {
        get {
            return UserDefaults.standard.bool(forKey: Key.audioEffect)
        }
        
        set {
            UserDefaults.standard.set(newValue, forKey: Key.audioEffect)
        }
    }
    //MARK: - UnobtrusiveModeEnabled
    var unobtrusiveModeEnabled: Bool = false
}
