//
//  ScreenSaverManager.swift
//  DeskAlerts
//
//  Created by Ярослав Долговых on 31/03/2020.
//  Copyright © 2020 Toolbarstudio Inc. All rights reserved.
//

import Cocoa

struct ScreenSaverData {
    var idleTime: String
    var type: String
    var moduleName: String
    var path: String
}

private struct Constants {
    static let deskAlertsScreenSaverPath = "/Library/Screen Savers/DeskAlertsScreenSaver.saver"
    static let launchPath = "/bin/sh"
    static let argumentIdleTime = "defaults -currentHost read com.apple.screensaver idleTime"
    static let argumentModuleDict = "defaults -currentHost read com.apple.screensaver moduleDict"
    static let terminationStatusSuccess: Int32 = 0
}

private let deskAlertsScreenSaverData = ScreenSaverData(idleTime: "60",
                                                               type: "0",
                                                               moduleName: "DeskAlertsScreenSaver",
                                                               path: "/Library/Screen Savers/DeskAlertsScreenSaver.saver")
class ScreenSaverManager {
    static let shared = ScreenSaverManager()
    var defaultScreenSaverData = ScreenSaverData(idleTime: "", type: "", moduleName: "", path: "")
    
    func activate() {
        saveDefaultScreenSaver()
        updateScreenSaverState()
    }
    
    private func saveDefaultScreenSaver() {
        guard let idleTime = getIdleTime() else { return }
        guard var screenSaverData = getDefaultCurrentHostData() else { return }
        screenSaverData.idleTime = idleTime
        defaultScreenSaverData = screenSaverData
    }
    
    private func getIdleTime() -> String? {
        let task = Process()
        task.launchPath = Constants.launchPath
        task.arguments = ["-c", Constants.argumentIdleTime]
        let pipe = Pipe()
        task.standardOutput = pipe
        task.launch()
        task.waitUntilExit()
        let handle = pipe.fileHandleForReading
        if task.terminationStatus == Constants.terminationStatusSuccess {
            let data = handle.readDataToEndOfFile()
            var stringData = String(data: data, encoding: .ascii)
            stringData = stringData?.replacingOccurrences(of: "\n", with: "")
            return stringData
        }
        return nil
    }
    
    private func getDefaultCurrentHostData() -> ScreenSaverData? {
        var tmpScreenSaverData = ScreenSaverData(idleTime: "", type: "", moduleName: "", path: "")
        
        let task = Process()
        task.launchPath = Constants.launchPath
        task.arguments = ["-c", Constants.argumentModuleDict]

        let pipe = Pipe()
        task.standardOutput = pipe
        task.standardError = pipe
        task.launch()
        task.waitUntilExit()

        let handle = pipe.fileHandleForReading
        if task.terminationStatus == Constants.terminationStatusSuccess {
            let data = handle.readDataToEndOfFile()
            let stringData = String(data: data, encoding: .ascii)
            var components = stringData?.components(separatedBy: ";\n")
            components![0] = components![0].replacingOccurrences(of: "{\n    moduleName = ", with: "")
            components![1] = components![1].replacingOccurrences(of: "    path = \"", with: "")
            components![1] = components![1].replacingOccurrences(of: "\"", with: "")
            components![2] = components![2].replacingOccurrences(of: "    type = ", with: "")

            tmpScreenSaverData.moduleName = components![0]
            tmpScreenSaverData.path = components![1]
            tmpScreenSaverData.type = components![2]
            
        }
        return tmpScreenSaverData
    }
    func updateScreenSaverState() {
        let countActiveScreenSavers = DatabaseManager.shared.getCountActiveScreenSavers()
        if countActiveScreenSavers == 0 {
            setDefaultScreenSaver()
        } else {
            setDeskAlertsScreenSaver()
        }
    }
    
    private func setDefaultScreenSaver() {
        setScreenSaverData(data: defaultScreenSaverData)
    }
    
    private func setDeskAlertsScreenSaver() {
        setScreenSaverData(data: deskAlertsScreenSaverData)
    }
    
    private func setScreenSaverData(data: ScreenSaverData) {
        let fileManager = FileManager.default
        var argument = "defaults -currentHost write com.apple.screensaver idleTime " + data.idleTime
        argument += " && "
        argument += "defaults -currentHost write com.apple.screensaver"
        argument += " moduleDict -dict path -string \"" + data.path + "\""
        argument += " moduleName -string \"" + data.moduleName + "\""
        argument += " type -string \"" + data.type + "\""
        if fileManager.fileExists(atPath: data.path) {
            let task = Process()
            task.launchPath = "/bin/sh"
            task.arguments = ["-c", argument]
            let pipe = Pipe()
            task.standardOutput = pipe
            task.standardError = pipe
            task.launch()
            task.waitUntilExit()
        } else {
            //Error
        }
    }
}
