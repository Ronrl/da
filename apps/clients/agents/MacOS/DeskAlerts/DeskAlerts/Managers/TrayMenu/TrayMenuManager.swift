//
//  TrayMenuManager.swift
//  DeskAlerts
//
//  Created by Ярослав Долговых on 10/09/2019.
//  Copyright © 2019 Toolbarstudio Inc. All rights reserved.
//

import Foundation
import Cocoa

class CustomMenuItem: NSMenuItem {
    var type: DropDownMenuItemType = .options
}

enum DropDownMenuItemType: String {
    case options = "Options"
    case history = "History"
    case mode = "Unobtrusive Mode"
    case uninstall = "Uninstall"
    case exit = "Exit"
    case help = "Help"
    
    var localizeTitle: String {
        switch self {
        case .options:
            return String.localized(key: "DDMOptions")
        case .history:
            return String.localized(key: "DDMHistory")
        case .mode:
            if SettingsManager.shared.unobtrusiveModeEnabled {
                return String.localized(key: "DDMStadardMode")
            } else {
                return String.localized(key: "DDMUnobtrusiveMode")
            }
        case .uninstall:
            return String.localized(key: "DDMUninstall")
        case .exit:
            return String.localized(key: "DDMExit")
        case .help:
            return String.localized(key: "DDMHelp")
        }
    }
    var image: NSImage {
        switch self {
        case .options:
            return NSImage(named: "trayIcon_11")!
        case .history:
            return NSImage(named: "trayIcon_13")!
        case .mode:
            if SettingsManager.shared.unobtrusiveModeEnabled {
                return NSImage(named: "trayIcon_0")!
            } else {
                return NSImage(named: "trayIcon_2")!
            }
        case .uninstall:
            return NSImage(named: "trayIcon_5")!
        case .exit:
            return NSImage(named: "trayIcon_14")!
        case .help:
            return NSImage(named: "trayIcon_10")!
        }
    }
    var action: Selector? {
        let trayMenuManager = TrayMenuManager.shared
        switch self {
        case .options:
            return #selector(trayMenuManager.optionsButtonAction(sender:))
        case .history:
            return #selector(trayMenuManager.historyButtonAction(sender:))
        case .mode:
            return #selector(trayMenuManager.modeButtonAction(sender:))
        case .uninstall:
            return #selector(trayMenuManager.uninstallButtonAction(sender:))
        case .exit:
            return #selector(trayMenuManager.exiteButtonAction(sender:))
        case .help:
            return nil
        }
    }
}

enum MenuState {
    case active
    case activeHaveUnreadMessage
    case unauthorized
    case unobtrusive
    case unobtrusiveHaveUnreadMessage
    
    //TODO: убрать форскасты, переделать на безопасный способ извлечения картинок
    var icon: NSImage {
        switch self {
        case .active:
            return NSImage(named: "trayIcon_0")!
        case .activeHaveUnreadMessage:
            return NSImage(named: "trayIcon_15")!
        case .unauthorized:
            return NSImage(named: "trayIcon_1")!
        case .unobtrusive:
            return NSImage(named: "trayIcon_2")!
        case .unobtrusiveHaveUnreadMessage:
            return NSImage(named: "trayIcon_3")!
        }
    }
}

class TrayMenuManager {
    static let shared = TrayMenuManager()
    
    private let statusItem = NSStatusBar.system.statusItem(withLength: 16)
    private let dropDownMenu = NSMenu()
    private(set) var state: MenuState = .unauthorized
    private var menuItemsByConfig = [DropDownMenuItemType]()
    
    func updateState() {
        let haveUnreadAlert = DatabaseManager.shared.haveUnreadAlert()
        let isUserAuthorized = JsonApiClient().token != ""
        if isUserAuthorized {
            if SettingsManager.shared.unobtrusiveModeEnabled {
                if haveUnreadAlert {
                    state = .unobtrusiveHaveUnreadMessage
                } else {
                    state = .unobtrusive
                }
            } else {
                if haveUnreadAlert {
                    state = .activeHaveUnreadMessage
                } else {
                    state = .active
                }
            }
        } else {
            state = .unauthorized
        }
        updateStatusItem(by: state)
        updateDropDownMenu()
    }
    
    private func updateStatusItem(by state: MenuState) {
        DispatchQueue.main.async {
            self.statusItem.image = state.icon
        }
    }
    
    func createDropDownMenu() {
        guard let menuItemsData = Config.XML?["ALERTS"]["VIEWS"]["SYSMENU"]["ITEM"].all else { return }
        
        menuItemsByConfig = [DropDownMenuItemType]()
        for item in menuItemsData {
            guard let caption = item.element?.attribute(by: "caption")?.text else { continue }
            guard let dropDownMenuItemType = DropDownMenuItemType(rawValue: caption) else { continue }
            menuItemsByConfig.append(dropDownMenuItemType)
        }
        
        for item in menuItemsByConfig {
            let menuItem = CustomMenuItem()
            menuItem.type = item
            menuItem.title = item.localizeTitle
            menuItem.image = item.image
            menuItem.action = item.action
            menuItem.target = self
            dropDownMenu.addItem(menuItem)
        }
        statusItem.menu = dropDownMenu
    }
    
    private func updateDropDownMenu() {
        guard let modeItem = dropDownMenu.items.first(where: { (item) -> Bool in
            guard let customMenuItem = item as? CustomMenuItem else { return false }
            return customMenuItem.type == .mode
        }) else { return }
        modeItem.title = DropDownMenuItemType.mode.localizeTitle
        modeItem.image = DropDownMenuItemType.mode.image
    }
    //MARK: - Button Actions
    @objc fileprivate func optionsButtonAction(sender : NSMenuItem) {
        do {
            let filePath = Bundle.main.path(forResource: "options", ofType: "html", inDirectory: "Resources")
            let alert = Alert()
            guard let windowFromConfig = try Config.XML?["ALERTS"]["VIEWS"]["WINDOW"].withAttribute("name","optionwindow1") else {
                DebugLogger.showAlert(with: "Error: property 'optionwindow1' not found in config file")
                return
            }
            
            let miniBrowserWindow = Config.shared.GetAlertWindowFromXml(xml: windowFromConfig, context: nil, alert: alert)
            alert.addToWindows(miniBrowserWindow)
            let storyboard = NSStoryboard(name: .windowCommandStoryboard, bundle: nil)
            guard let vc = storyboard.instantiateController(withIdentifier: .windowCommandSceneIdentifier) as? WindowCommand else { return }
            vc.Show(alert: alert,href: filePath,islocal: false,includedtickeralerts: nil,includedtickeralertscontent: nil, windowType: .options)
        } catch {
            print(error.localizedDescription)
        }
    }
    @objc fileprivate func historyButtonAction(sender : NSMenuItem) {
        do {
            let alert = Alert()
            guard let windowFromConfig = try Config.XML?["ALERTS"]["VIEWS"]["WINDOW"].withAttribute("name","historywindow1") else { return }
            let miniBrowserWindow = Config.shared.GetAlertWindowFromXml(xml: windowFromConfig, context: nil, alert: alert)
            alert.addToWindows(miniBrowserWindow)
            let storyboard = NSStoryboard(name: .windowCommandStoryboard,bundle: nil)
            guard let vc = storyboard.instantiateController(withIdentifier: .windowCommandSceneIdentifier) as? WindowCommand else { return }
            vc.Show(alert: alert,href: nil,islocal: false,includedtickeralerts: nil,includedtickeralertscontent: nil, windowType: .history)
        } catch {
            print(error.localizedDescription)
        }
    }
    @objc fileprivate func uninstallButtonAction(sender : NSMenuItem) {
        let answer = Config.shared.dialogOKCancel(question: String.localized(key: "DeleteUser"), text: "")
        if (answer == true)
        {
            let files = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].path
            do {
                
                try FileManager.default.removeItem(atPath: files + "/DeskAlerts.sqlite")
                try FileManager.default.removeItem(atPath: files + "/DeskAlerts.sqlite-shm")
                try FileManager.default.removeItem(atPath: files + "/DeskAlerts.sqlite-wal")
                
            } catch {
                print("Could not clear temp folder: \(error)")
            }
            NSApplication.shared.terminate(self)
        }
    }
    @objc fileprivate func modeButtonAction(sender : NSMenuItem) {
        SettingsManager.shared.unobtrusiveModeEnabled = !SettingsManager.shared.unobtrusiveModeEnabled
        TrayMenuManager.shared.updateState()
    }
    @objc fileprivate func exiteButtonAction(sender : NSMenuItem) {
        NSApplication.shared.terminate(self)
    }
    @objc fileprivate func defaultButtonAction(sender : NSMenuItem) {
        print(sender.title)
    }
}




