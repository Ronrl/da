//
//  CrashReportManager.swift
//  DeskAlerts
//
//  Created by Ярослав Долговых on 10/12/2019.
//  Copyright © 2019 Toolbarstudio Inc. All rights reserved.
//

import Foundation
import Alamofire

private struct Constants {
    static let crashReportDirectoryPath = "/Users/yaroslavdolgovyh/Library/Logs/DiagnosticReports"
    static let sendReportRequestPath = "/api/v1/error/client/mac"
    static let successStatusCode: Int = 200
}

class CrashReportManager {
    static let shared = CrashReportManager()
    
    func checkCrashReports() {
        guard let appDisplayName = Bundle.main.displayName else { return }
        guard let url = URL(string: Constants.crashReportDirectoryPath) else { return }
        do {
            let urls = try FileManager.default.contentsOfDirectory(at: url, includingPropertiesForKeys: nil, options: .skipsHiddenFiles)
            for item in urls {
                if item.path.contains(appDisplayName) {
                    sendCrashReportToServer(fileURL: item)
                }
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    
    private func sendCrashReportToServer(fileURL: URL) {
        let serverPath = ClientSettings.shared.settingsList.serverPath
        let requestPath = serverPath + Constants.sendReportRequestPath
        guard let operationURL = URL(string: requestPath) else { return }
        Alamofire.upload(fileURL, to: operationURL, method: .post, headers: nil).response { (defaultDataResponse) in
            if defaultDataResponse.response?.statusCode == Constants.successStatusCode {
                self.deleteFile(by: fileURL)
            }
        }
    }
    
    private func deleteFile(by url: URL) {
        do {
            try FileManager.default.removeItem(at: url)
        } catch {
            print(error.localizedDescription)
        }
    }
}
