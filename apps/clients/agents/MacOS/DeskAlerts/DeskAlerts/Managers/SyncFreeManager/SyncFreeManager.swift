//
//  SyncFreeManager.swift
//  DeskAlerts
//
//  Created by Fedr on 02/12/2019.
//  Copyright © 2019 Toolbarstudio Inc. All rights reserved.
//

import Foundation
import Alamofire

private struct Constants {
    static let defaultResynchronizationTimeInterval: TimeInterval = 60*60*24*6
    static let synchronizationGroupRequestPath = "/sync_groups.asp?"
    static let startSynchronizationProcessPath = "/process_sync.asp"
    static let domainPrefix = "domain="
    static let userNamePrefix = "uname="
    static let groupToAddPrefix = "groups_to_add="
    static let parametersSepator = "&"
    static let groupSeparator = ","
    static let delayTime: TimeInterval = 2.0
    static let successStatusCode: Int = 200
}

class SyncFreeManager {
    static let shared = SyncFreeManager()
    
    private var resynchronizationTimeInterval = Constants.defaultResynchronizationTimeInterval
    private var synchronizationTime: Timer? = nil
    
    init() {
        resynchronizationTimeInterval = ClientSettings.shared.settingsList.synFreeResynchronizationTimeInterval
    }
    
    func startSynchronization() {
        Timer.scheduledTimer(withTimeInterval: resynchronizationTimeInterval, repeats: false) { (timer) in
            self.sendDomaiUserGroupDataToServer()
            self.updateSynchronizationTime()
        }
        
    }
    
    private func updateSynchronizationTime() {
        synchronizationTime = Timer.scheduledTimer(withTimeInterval: resynchronizationTimeInterval, repeats: false, block: { (timer) in
            self.updateSynchronizationTime()
            self.sendDomaiUserGroupDataToServer()
            self.startSynchronizationOnServer()
        })
    }
    
    private func sendDomaiUserGroupDataToServer() {
        let severPath = ClientSettings.shared.settingsList.serverPath
        var requestPath = severPath + Constants.synchronizationGroupRequestPath
        
        let userName = NSUserName()
        guard let domainName = DomainManager.shared.domainName else { return }
        guard let domainNameForURL = domainName.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) else { return }
        guard let userDomainGroups = DomainManager.shared.userDomainGroups else { return }
        
        requestPath += Constants.domainPrefix + domainNameForURL + Constants.parametersSepator
        requestPath += Constants.userNamePrefix + userName + Constants.parametersSepator
        requestPath += Constants.groupToAddPrefix
        
        for item in userDomainGroups {
            guard let urlFriendlyItem = item.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) else { continue }
            requestPath += urlFriendlyItem
            if let groupIndex = userDomainGroups.index(of: item), groupIndex < userDomainGroups.count - 1 {
                requestPath += Constants.groupSeparator
            }
        }

        guard let url = URL(string: requestPath) else { return }
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).response { (defaultDataResponse) in
            if defaultDataResponse.response?.statusCode == Constants.successStatusCode {
                self.startSynchronization()
            }
        }
    }
    
    private func startSynchronizationOnServer() {
        let severPath = ClientSettings.shared.settingsList.serverPath
        let requestPath = severPath + Constants.startSynchronizationProcessPath
        guard let url = URL(string: requestPath) else { return }
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil)
    }
    
}
