//
//  RequestManager.swift
//  DeskAlerts
//
//  Created by Ярослав Долговых on 22/10/2019.
//  Copyright © 2019 Toolbarstudio Inc. All rights reserved.
//

import Foundation
import Alamofire

private enum APIV1: String {
    case confirmationOfTerminateAlert = "/api/v1/user/alerts/terminate/"
    case confirmationOfReadAlert = "/api/v1/user/alerts/read/"
}

private enum Keys: String {
    case alertID = "alertID"
    case authorization = "Authorization"
}

private struct Constants {
    static let alertIDPrefix = "alert_id="
    static let userNamePrefix = "uname="
    static let urlParameterSeparator = "&"
    static let successStatusCode: Int = 200
}

class RequestManager {
    static let shared = RequestManager()
    
    let serverPath = ClientSettings.shared.settingsList.serverPath
    
    func sendСonfirmationOfTerminateAlert(by id: String) {
        guard let url = URL(string: serverPath + APIV1.confirmationOfTerminateAlert.rawValue + id) else { return }
        let header = JsonApiClient._header
        Alamofire.request(url, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: header)
    }
    
    func sendConfirmationOfReadAlert(by alertID: String, token: String) {
        guard let url = URL(string: serverPath + APIV1.confirmationOfReadAlert.rawValue) else { return }
        let header: [String: String] = [Keys.authorization.rawValue: token]
        let body: [String: String] = [Keys.alertID.rawValue: alertID]
        Alamofire.request(url, method: .post, parameters: body, encoding: JSONEncoding.default, headers: header).response { (defaultDataResponse) in
            let statusCode = defaultDataResponse.response?.statusCode
            if statusCode != Constants.successStatusCode {
                DebugLogger.showAlert(with: "Error read confirmation token: \(header[Keys.authorization.rawValue]), alertID: \(body[Keys.alertID.rawValue]), statusCode: \(statusCode)")
            }
        }
    }
}
