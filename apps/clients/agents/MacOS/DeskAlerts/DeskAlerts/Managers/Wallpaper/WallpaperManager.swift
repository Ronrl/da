//
//  WallpaperManager.swift
//  DeskAlerts
//
//  Created by Ярослав Долговых on 03/09/2019.
//  Copyright © 2019 Toolbarstudio Inc. All rights reserved.
//

import Foundation
import Cocoa

private enum WallpaperDisplayStyle: String {
    case centered = "1"
    case tilled = "2"
    case stretched = "3"
    
    var options: [NSWorkspace.DesktopImageOptionKey: Any] {
        var tmpOptions = [NSWorkspace.DesktopImageOptionKey: Any]()
        switch self {
        case .centered:
            tmpOptions[.imageScaling] = NSImageScaling.scaleNone.rawValue
            tmpOptions[.allowClipping] = false
            tmpOptions[.fillColor] = NSColor.black
        case .tilled:
            tmpOptions[.imageScaling] = NSImageScaling.scaleAxesIndependently.rawValue
            tmpOptions[.allowClipping] = true
            tmpOptions[.fillColor] = NSColor.black
        case .stretched:
            tmpOptions[.imageScaling] = NSImageScaling.scaleAxesIndependently.rawValue
            tmpOptions[.allowClipping] = true
            tmpOptions[.fillColor] = NSColor.black
        }
        return tmpOptions
    }
}

private struct Constants {
    static let replacementStringFirst = "<img src=\"data:image/jpeg;base64,"
    static let replacementStringSecond = "\"/>"
    static let emptyString = ""
    
    static let fileNamePrefix = "wallpaper_"
    static let fileNamePostfix = ".jpeg"
    
    static let numberOfColumnInTile: Int = 3
    static let numberOfRowInTile: Int = 3
    
    static let blackSquareImageName = "defaultWallpaper"
    static let defaultWallpaperDisplayStyle: WallpaperDisplayStyle = .stretched
}

private struct Key {
    static let imageURL = "DefaultUserWallpaperImageURL"
    static let options = "DefaultUserWallpaperDisplayStyle"
}

private struct DefaultUserWallpaper {
    let imageURL: URL
    let options: [NSWorkspace.DesktopImageOptionKey: Any]
}



class WallpaperManager {
    static let shared = WallpaperManager()
    
    private var wallpapers = [Alert]()
    private var displayedWallpaperIndex: Int = 0
    private var wallpaperChangeTimer: Timer?
    private var defaultUserWallpaper: DefaultUserWallpaper!
    
    func activate() {
        saveDefaultUserWallpaper()
    }
    
    private func saveDefaultUserWallpaper() {
        let workspace = NSWorkspace.shared
        let screens = NSScreen.screens
        guard let firstScreen = screens.first else { return }
        guard let url = workspace.desktopImageURL(for: firstScreen) else { return }
        guard let options = workspace.desktopImageOptions(for: firstScreen) else { return }
        defaultUserWallpaper = DefaultUserWallpaper(imageURL: url, options: options)
    }
    
    private func getDefaultUserWallpaper() -> DefaultUserWallpaper {
        let url = Bundle.main.urlForImageResource(Constants.blackSquareImageName)!
        let defaultWallpaper = DefaultUserWallpaper(imageURL: url, options: Constants.defaultWallpaperDisplayStyle.options)
        
        guard let tmpImageURL = UserDefaults.standard.url(forKey: Key.imageURL) else {
            return defaultWallpaper
        }
        guard let tmpOptions = UserDefaults.standard.object(forKey: Key.options) as? [NSWorkspace.DesktopImageOptionKey: Any] else {
            return defaultWallpaper
        }
        return DefaultUserWallpaper(imageURL: tmpImageURL, options: tmpOptions)
    }
    
    //MARK: - UpdateWallpaperList
    func updateWallpaperList() {
        guard let sessionUser = Config.SessionUser, let _ = sessionUser.user_name else {
            print("Error: \(#function)  - invalid sessionUser")
            return
        }
        wallpapers = DatabaseManager.shared.getAlerts(by: sessionUser, type: .wallpaper, isClosed: false)
        wallpaperChangeTimer?.invalidate()
        wallpaperChangeTimer = nil
        if wallpapers.count != 0 {
            displayedWallpaperIndex = 0
            setWallpaperToScreens()
            updateWallpaperChangeTimer()
        } else {
            setDefaultWallpaper()
        }
    }
    
    //MARK: - SetDefaultWallpaper
    private func setDefaultWallpaper() {
        let workspace = NSWorkspace.shared
        let screens = NSScreen.screens
        do {
            for item in screens {
                try workspace.setDesktopImageURL(defaultUserWallpaper.imageURL, for: item, options: defaultUserWallpaper.options)
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    
    //MARK: - UpdateWallpaperChangeTimer
    private func updateWallpaperChangeTimer() {
        let wallpaper = wallpapers[displayedWallpaperIndex]
        guard let wallpaperDisplayTimeString = wallpaper.autoclose,
              let wallpaperDisplayTime = TimeInterval(wallpaperDisplayTimeString) else {
            print("Invalid wallpaperDisplayTime \(#function)")
            return
        }
        
        wallpaperChangeTimer = Timer.scheduledTimer(withTimeInterval: wallpaperDisplayTime, repeats: false, block: { (timer) in
            self.updateDisplayedWallpaperIndex()
            self.updateWallpaperChangeTimer()
            self.setWallpaperToScreens()
        })
    }
    
    //MARK: - UpdateDisplayedWallpaperIndex
    private func updateDisplayedWallpaperIndex() {
        displayedWallpaperIndex += 1
        if displayedWallpaperIndex > wallpapers.count - 1 {
            displayedWallpaperIndex = 0
        }
    }
    
    //MARK: - SetWallpaperToScreens
    private func setWallpaperToScreens() {
        let wallpaper = wallpapers[displayedWallpaperIndex]
        guard let wallpaperID = wallpaper.alert_id else {
            print("Invalid wallpaper id")
            return
        }
        let workspace = NSWorkspace.shared
        let screens = NSScreen.screens
        
        guard let displayStyleString = wallpaper.position, let displayStyle = WallpaperDisplayStyle(rawValue: displayStyleString) else {
            print("Invalid position wallpaper")
            return
        }
        
        guard let imageData = getImageData(from: wallpaper) else { return }
        guard let image = getImage(by: imageData) else { return }
        
        for item in screens {
            guard let convertedImage = convert(image: image, by: displayStyle, and: item) else { return }
            guard let screenIndex = screens.index(of: item) else { return }
            guard let imageURL = generateImageURL(by: wallpaperID, and: screenIndex) else { return }
            if save(convertedImage, to: imageURL) {
                do {
                    try workspace.setDesktopImageURL(imageURL, for: item, options: displayStyle.options)
                } catch {
                    print(error.localizedDescription)
                }
            }
        }
    }
}

//MARK: Support function
extension WallpaperManager {
    private func convert(image: NSImage, by displayStyle: WallpaperDisplayStyle, and screen: NSScreen) -> NSImage? {
        switch displayStyle {
        case .centered:
            return image
        case .tilled:
            return createTile(from: image, numberOfColumn: Constants.numberOfColumnInTile, numberOfRow: Constants.numberOfRowInTile)
        case .stretched:
            return image
        }
    }
    
    private func createTile(from image: NSImage, numberOfColumn: Int, numberOfRow: Int) -> NSImage {
        let tmpImageSize = NSSize(width: Int(image.size.width) * numberOfColumn, height: Int(image.size.height) * numberOfRow)
        let tmpImage = NSImage(size: tmpImageSize)
        tmpImage.lockFocus()
        for column in 0..<numberOfColumn {
            for row in 0..<numberOfRow {
                let rect = NSRect(x: column * Int(image.size.width),
                                  y: row * Int(image.size.height),
                                  width: Int(image.size.width),
                                  height: Int(image.size.height))
                image.draw(in: rect)
            }
        }
        tmpImage.unlockFocus()
        
        return tmpImage
    }
    
    private func generateImageURL(by wallpaperID: String, and screenIndex: Int) -> URL? {
        let fileName = Constants.fileNamePrefix + wallpaperID + "_\(screenIndex)" + Constants.fileNamePostfix
        guard let documentsURL = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first else {
            return nil
        }
        let url = documentsURL.appendingPathComponent(fileName)
        return url
    }
    
    //MARK: - SaveByURL
    private func save(_ image: NSImage, to url: URL) -> Bool {
        guard let imageData = image.tiffRepresentation else { return false }
        do {
            try imageData.write(to: url)
            return true
        } catch {
            print(error.localizedDescription)
            return false
        }
    }
    
    //MARK: - GetImageByData
    private func getImage(by data: Data) -> NSImage? {
        guard let image = NSImage(data: data) else {
            print("Error convert data to image")
            return nil
        }
        return image
    }
    
    //MARK: - GetImageData
    private func getImageData(from wallpaper: Alert) -> Data? {
        guard let alertText = wallpaper.alerttext else {
            print("Invalid alerttext wallpaper")
            return nil
        }
        var base64String = alertText.replacingOccurrences(of: Constants.replacementStringFirst, with: Constants.emptyString)
        base64String = base64String.replacingOccurrences(of: Constants.replacementStringSecond, with: Constants.emptyString)
        let decodedData = Data(base64Encoded: base64String, options: .ignoreUnknownCharacters)
        return decodedData
    }
}
