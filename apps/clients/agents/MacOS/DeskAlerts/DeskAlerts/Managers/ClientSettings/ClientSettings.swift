//
//  ClientSettings.swift
//  DeskAlerts
//
//  Created by Ярослав Долговых on 26/11/2019.
//  Copyright © 2019 Toolbarstudio Inc. All rights reserved.
//

import Foundation

fileprivate struct Constants {
    static let settingsListFileName = "ClientSettings"
    static let settingsListFileType = "plist"
}

struct SettingsList: Codable {
    let serverPath: String
    let authorizationType: AuthorizationType
    let synFreeResynchronizationTimeInterval: TimeInterval
    let enableDebugMode: Bool
    let clientVersion: String
    let normalExpire: TimeInterval
    
    enum CodingKeys: String, CodingKey {
        case serverPath = "ServerPath"
        case authorizationType = "AuthorizationType"
        case synFreeResynchronizationTimeInterval = "SynFreeResynchronizationTimeInterval"
        case enableDebugMode = "EnableDebugMode"
        case clientVersion = "ClientVersion"
        case normalExpire = "NormalExpire"
    }
}

class ClientSettings {
    static let shared = ClientSettings()
    
    var settingsList: SettingsList!
    
    
    

    init() {
        loadData()
    }
    
    func loadData() {
        guard let url = Bundle.main.url(forResource: Constants.settingsListFileName, withExtension: Constants.settingsListFileType) else { return }
        do {
            let data = try Data(contentsOf: url)
            let decoder = PropertyListDecoder()
            settingsList = try decoder.decode(SettingsList.self, from: data)
            print(settingsList)
        } catch {
            print(error.localizedDescription)
        }
    }
}
