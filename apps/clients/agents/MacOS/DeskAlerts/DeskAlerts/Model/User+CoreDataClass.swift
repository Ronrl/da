//
//  User+CoreDataClass.swift
//  DeskAlerts
//
//  Created by mihail on 26/04/2017.
//  Copyright © 2017 Toolbarstudio Inc. All rights reserved.
//

import Foundation
import CoreData

@objc(User)
public class User: NSManagedObject {
    convenience init() {
        // Описание сущности
        let entity = NSEntityDescription.entity(forEntityName: "User",  in: CoreDataManager.instance.managedObjectContext)
    
        // Создание нового объекта
        self.init(entity: entity!, insertInto: nil)
    }

}
