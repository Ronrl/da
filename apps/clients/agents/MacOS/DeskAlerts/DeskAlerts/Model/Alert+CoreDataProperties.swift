//
//  Alert+CoreDataProperties.swift
//  DeskAlerts
//
//  Created by mihail on 26/04/2017.
//  Copyright © 2017 Toolbarstudio Inc. All rights reserved.
//

import Foundation
import CoreData


extension Alert {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Alert> {
        return NSFetchRequest<Alert>(entityName: "Alert");
    }

    @NSManaged public var acknown: String?
    @NSManaged public var acknowledged: Bool
    @NSManaged public var alert_id: String?
    @NSManaged public var autoclose: String?
    @NSManaged public var alerttext: String?
    @NSManaged public var class_id: String?
    @NSManaged public var create_date: String?
    @NSManaged public var docked: String?
    @NSManaged public var expire: String?
    @NSManaged public var filename: String?
    @NSManaged public var heigth: String?
    @NSManaged public var hide_close: String?
    @NSManaged public var history: String?
    @NSManaged public var history_title: String?
    @NSManaged public var href: String?
    @NSManaged public var id: String?
    @NSManaged public var isclosed: Bool
    @NSManaged public var isreaded: Bool
    @NSManaged public var name: String?
    @NSManaged public var plugin: String?
    @NSManaged public var position: String?
    @NSManaged public var priority: String?
    @NSManaged public var recivetime: NSDate?
    @NSManaged public var resizable: String?
    @NSManaged public var schedule: String?
    @NSManaged public var self_deletable: String?
    @NSManaged public var survey: String?
    @NSManaged public var ticker: String?
    @NSManaged public var ticker_position: String?
    @NSManaged public var tickerspeed: String?
    @NSManaged public var title: String?
    @NSManaged public var to_date: String?
    @NSManaged public var urgent: String?
    @NSManaged public var user_id: String?
    @NSManaged public var utc: String?
    @NSManaged public var visible: String?
    @NSManaged public var width: String?
    @NSManaged public var users: User?
    @NSManaged public var windows: NSSet?

}

// MARK: Generated accessors for windows
extension Alert {

    @objc(addWindowsObject:)
    @NSManaged public func addToWindows(_ value: AlertWindow)

    @objc(removeWindowsObject:)
    @NSManaged public func removeFromWindows(_ value: AlertWindow)

    @objc(addWindows:)
    @NSManaged public func addToWindows(_ values: NSSet)

    @objc(removeWindows:)
    @NSManaged public func removeFromWindows(_ values: NSSet)

}

extension Alert {
    var isHighPriorityAlert: Bool {
        guard let highPriorityAlertProperty = self.urgent else { return false }
        if highPriorityAlertProperty == "1" {
            return true
        } else {
            return false
        }
    }
    
    
    var isResizable: Bool {
        guard let resizableProperty = self.resizable else { return false }
        if resizableProperty == "1" {
            return true
        } else {
            return false
        }
    }
    
    var isSelfDeletable: Bool {
        guard let selfDeletableProperty = self.self_deletable else { return false }
        if selfDeletableProperty == "1" {
            return true
        } else {
            return false
        }
    }
    
    var isTiker: Bool {
        guard let tickerStringProperty = self.ticker else { return false }
        if tickerStringProperty == "1" {
            return true
        } else {
            return false
        }
    }
    
    var isWallpaper: Bool {
        return self.id == "wallpaper"
    }
    
    var isScreensaver: Bool {
        return self.id == "screensaver"
    }
    
    var haveAcknowledgeProperty: Bool {
        get {
            guard let acknowledgeStringProperty = self.acknown else { return false }
            if acknowledgeStringProperty == "1" {
                return true
            } else {
                return false
            }
        }
    }
    
    static func ==(lhs: Alert, rhs: Alert) -> Bool {
        guard let leftAlertID = lhs.alert_id else { return false }
        guard let rightAlertID = rhs.alert_id else { return false }
        return leftAlertID == rightAlertID
    }
}
