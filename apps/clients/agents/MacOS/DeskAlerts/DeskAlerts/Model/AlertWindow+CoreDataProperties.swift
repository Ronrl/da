//
//  AlertWindow+CoreDataProperties.swift
//  DeskAlerts
//
//  Created by mihail on 26/04/2017.
//  Copyright © 2017 Toolbarstudio Inc. All rights reserved.
//

import Foundation
import CoreData


extension AlertWindow {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<AlertWindow> {
        return NSFetchRequest<AlertWindow>(entityName: "AlertWindow");
    }

    @NSManaged public var bottommargin: String?
    @NSManaged public var captionhref: String?
    @NSManaged public var comfirmation: String?
    @NSManaged public var customjs: String?
    @NSManaged public var heigth: String?
    @NSManaged public var leftmargin: String?
    @NSManaged public var name: String?
    @NSManaged public var position: String?
    @NSManaged public var rightmargin: String?
    @NSManaged public var single: String?
    @NSManaged public var skin_id: String?
    @NSManaged public var topmargin: String?
    @NSManaged public var transp_color: String?
    @NSManaged public var transparency: String?
    @NSManaged public var visible: String?
    @NSManaged public var width: String?
    @NSManaged public var alerts: Alert?

}
