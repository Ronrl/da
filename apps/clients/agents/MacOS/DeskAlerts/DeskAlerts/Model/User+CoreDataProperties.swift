//
//  User+CoreDataProperties.swift
//  DeskAlerts
//
//  Created by mihail on 26/04/2017.
//  Copyright © 2017 Toolbarstudio Inc. All rights reserved.
//

import Foundation
import CoreData


extension User {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<User> {
        return NSFetchRequest<User>(entityName: "User");
    }
    
    @NSManaged public var debug_mode: String?
    @NSManaged public var deskalerts_id: String?
    @NSManaged public var deskalerts_name: String?
    @NSManaged public var disable: String?
    @NSManaged public var email: String?
    @NSManaged public var isSilent: String?
    @NSManaged public var modmail: String?
    @NSManaged public var modsms: String?
    @NSManaged public var name: String?
    @NSManaged public var normal_expire: String?
    @NSManaged public var phone: String?
    @NSManaged public var play_sound: String?
    @NSManaged public var root_path: String?
    @NSManaged public var unreadedmessagesstate: String?
    @NSManaged public var user_id: String?
    @NSManaged public var user_name: String?
    @NSManaged public var md5_password: String?
    @NSManaged public var user_path: String?
    @NSManaged public var user_wallpaper: String?
    @NSManaged public var user_screensaver: String?
    @NSManaged public var screensaverstatus: String?
    @NSManaged public var usersession_id: String?
    @NSManaged public var alerts: NSSet?
    @NSManaged public var domain_name: String?
    
    
}

// MARK: Generated accessors for alerts
extension User {
    
    @objc(addAlertsObject:)
    @NSManaged public func addToAlerts(_ value: Alert)
    
    @objc(removeAlertsObject:)
    @NSManaged public func removeFromAlerts(_ value: Alert)
    
    @objc(addAlerts:)
    @NSManaged public func addToAlerts(_ values: NSSet)
    
    @objc(removeAlerts:)
    @NSManaged public func removeFromAlerts(_ values: NSSet)
    
}

