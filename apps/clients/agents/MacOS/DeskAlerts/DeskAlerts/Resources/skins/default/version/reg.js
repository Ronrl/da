var searchTerm;
function setSearchTerm(_searchTerm)
{
	searchTerm = _searchTerm;
}
function checkLang(current, langs)
{
	var currents = current.toLowerCase().replace(" ", "").split(",");
	var languages = langs.toLowerCase().replace(" ", "").split(",");
	for(var i=0; i<currents.length; i++)
	{
		for(var j=0; j<languages.length; j++)
		{
			if(currents[i].indexOf(languages[j])==0 ||
				languages[j].indexOf(currents[i])==0)
			{
				return true;
			}
		}
	}
	return false;
}
function replaceWithInput(elem, name)
{
	var inp = document.createElement("input");
	inp.type = "submit";
	if(elem.tagName.toLowerCase() == "img")
	{
		var func = elem.onclick||'';
		if(typeof(func) == 'function')
			inp.onclick = function() {
				func();
				return false;
			};
		else
			inp.onclick = func+";return false";
	}
	else
	{
		inp.onclick = elem.onclick;
	}
	inp.name = elem.name;
	inp.value = name;
	elem.parentNode.replaceChild(inp, elem);
}
function onContextMenuMain(e)
{
	if(!e) e = window.event;
	e.returnValue = false;
	e.cancelBubble = true;
	return false;
}
function getDocumentSize(doc)
{
	var size ={
		height: 0,
		width: 0
	};
	var body = doc.body;
	if (!doc.compatMode || doc.compatMode=="CSS1Compat")
	{
		var topMargin = parseInt(body.currentStyle.marginTop, 10) || 0;
		var bottomMargin = parseInt(body.currentStyle.marginBottom, 10) || 0;
		var leftMargin = parseInt(body.currentStyle.marginLeft, 10) || 0;
		var rightMargin = parseInt(body.currentStyle.marginRight, 10) || 0;

		size.width=Math.max(body.offsetWidth + leftMargin + rightMargin, doc.documentElement.clientWidth, doc.documentElement.scrollWidth);
		size.height=Math.max(body.offsetHeight + topMargin + bottomMargin, doc.documentElement.clientHeight, doc.documentElement.scrollHeight);
	}
	else
	{
		size.width = body.scrollWidth;
		size.height = body.scrollHeight;
	}
	return size;
}
function resizeByBody()
{
	try{
		var isResizible = window.external.isResizible();
		if (isResizible)
		{
			var size = getDocumentSize(document);
			var curBodyWidth = window.external.getBodyWidth();
			var curBodyHeight = window.external.getBodyHeight();
			if (curBodyWidth > size.width) size.width=curBodyWidth;
			if (curBodyHeight > size.height) size.height=curBodyHeight;
			window.external.setCaptionWidthByBodyWidth(size.width);
			window.external.setCaptionHeightByBodyHeight(size.height);
		}
	}catch(e){}
}
var initialized = false;
function Initial(a)
{
	setTimeout(Initial_Timeout, 1);
}
function Initial_Timeout()
{		
	if(initialized) return;
	else initialized = true;
	try
	{
		var html = document.body.innerHTML;
		var top_template = window.external.getProperty('top_template', '')||'';
		var bottom_template = window.external.getProperty('bottom_template', '')||'';
		var match = html.match(/<!-- *begin_lang[^>]*-->([\w\W]*?)<!-- *end_lang *-->/ig);
		if(match)
		{
			var locale = window.external.getProperty("customlocale", "");
			if(!locale) locale = window.external.getProperty("locale", "");
			var default_lang, use_dafault = true;
			for(var j=0; j<match.length; j++)
			{
				var lang = match[j].match(/<!-- *begin_lang[^>]*lang *= *(['"])([^"']*)\1[^>]*-->/i);
				if(lang && checkLang(locale, lang[2]))
				{
					html = html.replace(/<!-- *begin_lang[^>]*-->([\w\W]*)<!-- *end_lang *-->/i, match[j]);
					use_dafault = false;
					var title = match[j].match(/<!-- *begin_lang[^>]*title *= *(['"])([^"']*)\1[^>]*-->/i);
					if(title && title[2])
						window.external.callInCaption('setTitle', title[2]);
					break;
				}
				var is_default = match[j].match(/<!-- *begin_lang[^>]*is_default *= *(['"])([^"']*)\1[^>]*-->/i);
				if(is_default && (is_default[2]!=0 || is_default[2].toLowerCase()=="true"))
					default_lang = match[j];
			}
			if(use_dafault && default_lang)
				html = html.replace(/<!-- *begin_lang[^>]*-->([\w\W]*)<!-- *end_lang *-->/i, default_lang);
		}
		
		var root_path, close_button, save_button, submit_button, readIt_button, alert_auto_size = false;
		var skinId = null;
		try {
			skinId = window.external.getProperty("skin_id","");
		} catch(e) {}
		if(skinId)
		{
			try {
				var server = window.external.getProperty("server");
				close_button = server + "/admin/skins/" + skinId + "/version/close_button.gif";
				save_button = server + "/admin/skins/" + skinId + "/version/save.gif";
				submit_button = server + "/admin/skins/" + skinId + "/version/submit_button.gif";
				readIt_button = server + "/admin/skins/" + skinId + "/version/read_it.gif";
			} catch(e) {
				root_path = null;
				close_button = null;
				save_button = null;
				submit_button = null;
			}
		}
		else
		{
			try {
				root_path = window.external.getProperty("root_path").replace(/\\/g, "/");
				close_button = window.external.getProperty("closeButton").replace(/\\/g, "/");
				save_button = window.external.getProperty("saveButton").replace(/\\/g, "/");
				submit_button = window.external.getProperty("submitButton").replace(/\\/g, "/");
			} catch(e) {
				root_path = null;
				close_button = null;
				save_button = null;
				submit_button = null;
			}
		}

		document.body.innerHTML = top_template + html + bottom_template;

		if(close_button || save_button || submit_button)
		{
		
			var images = document.getElementsByTagName("img");
			for(var j = 0; j < 2; j++)
			{
				if(j==1) images = document.getElementsByTagName("input");
				for(var i = 0; i < images.length; i++)
				{
					if(images[i].tagName.toLowerCase() == "img" || images[i].getAttribute("type").toLowerCase() == "image")
					{
						var src = images[i].getAttribute("src");
						if(close_button && /.*admin\/images\/(langs\/\w+\/?|)close_button\.gif/i.test(src))
						{
							if(/^[\w\s]+$/.test(close_button))
								replaceWithInput(images[i], close_button);
							else
							{
								images[i].src = (/https?:\/\//i.test(close_button) ? "" : "file:///") + close_button;
							}

						}
						else if(save_button && /.*admin\/images\/(langs\/\w+\/?|)save\.gif/i.test(src))
						{
							if(/^[\w\s]+$/.test(save_button))
								replaceWithInput(images[i], save_button);
							else
							{
								images[i].src = (/https?:\/\//i.test(save_button) ? "" : "file:///") + save_button;
							}
						}
						else if(submit_button && /.*admin\/images\/(langs\/\w+\/?|)submit_button\.gif/i.test(src))
						{	
							if(/^[\w\s]+$/.test(submit_button))
								replaceWithInput(images[i], submit_button);
							else
							{
								images[i].src = (/https?:\/\//i.test(submit_button) ? "" : "file:///") + submit_button;
							}
						}
					}
				}
			}
		}
		
		document.body.scroll="auto";
		document.oncontextmenu = onContextMenuMain;

		var headTag = document.getElementsByTagName('head')[0]; 

		var imp = document.createElement('link');
		imp.setAttribute("type", "text/css");
		imp.setAttribute("rel", "stylesheet");
		
		if (skinId)
		{
			var skinBase =  server + "/admin/skins/" + skinId + "/version/";
			var baseElement = document.createElement("BASE");
			baseElement.setAttribute("href",skinBase);
			headTag.appendChild(baseElement);
			imp.setAttribute("href", skinBase + "/skin.css");
		}
		else
		{
			imp.setAttribute("href", "file:///" + root_path + "/skin.css");
		}
		
		headTag.appendChild(imp);

		document.body.className = "regcontent";
		
		// turn on autoresizing alert by body of document
		resizeByBody();
		setTimeout(resizeByBody, 50);
		setTimeout(resizeByBody, 100);
		
		try {
			window.external.search(searchTerm);
		} catch(e) {}
	}
	catch(e)
	{
	}
}
