//
//  FileManager.swift
//  DeskAlerts
//
//  Created by Ярослав Долговых on 30/01/2020.
//  Copyright © 2020 Toolbarstudio Inc. All rights reserved.
//

import Foundation

extension FileManager {
    static var documentsDirectoryPath: String? {
        return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first?.path
    }
}
