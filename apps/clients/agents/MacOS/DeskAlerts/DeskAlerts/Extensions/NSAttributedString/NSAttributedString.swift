//
//  NSAttributedString.swift
//  MyVirtualBoyFriend
//
//  Created by user on 14/03/2019.
//  Copyright © 2019 User. All rights reserved.
//

import Foundation
import Cocoa

extension NSAttributedString {
    static func + (left: NSAttributedString, right: NSAttributedString) -> NSMutableAttributedString {
        let result = NSMutableAttributedString()
        result.append(left)
        result.append(right)
        return result
    }
    
    convenience init(text: String, font: NSFont, textColor: NSColor) {
        self.init(string: text, attributes: [.font : font, .foregroundColor: textColor])
    }
}
