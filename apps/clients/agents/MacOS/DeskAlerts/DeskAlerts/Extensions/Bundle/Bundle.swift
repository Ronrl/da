//
//  Bundle.swift
//  DeskAlerts
//
//  Created by Ярослав Долговых on 10/12/2019.
//  Copyright © 2019 Toolbarstudio Inc. All rights reserved.
//

import Foundation

extension Bundle {
    var displayName: String? {
        return object(forInfoDictionaryKey: "CFBundleName") as? String
    }
}
