//
//  File.swift
//  DeskAlerts
//
//  Created by Ярослав Долговых on 10/09/2019.
//  Copyright © 2019 Toolbarstudio Inc. All rights reserved.
//

import Foundation

private struct Constants {
    static let urlDotCode = "%2e"
    static let emptyString = ""
}

extension String {
    static let emptyString = ""
    
    var boolValue: Bool {
        return NSString(string: self).boolValue
    }

    func replaceDotForUrl() -> String? {
        return self.replacingOccurrences(of: ".", with: Constants.urlDotCode)
    }
    
    func encodeForURL() -> String? {
        guard let tmpString = self.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) else {
            print("Error encoding to url")
            return nil
        }
        return tmpString
    }
    var isEmpty: Bool {
        return self == Constants.emptyString
    }
    
    static func localized(key: String) -> String {
        return NSLocalizedString(key, comment: "")
    }
    
    func stringByAddingPercentEncodingForRFC3986() -> String? {
        let unreserved = "-._~/?:"
        let allowed = NSMutableCharacterSet.alphanumeric()
        allowed.addCharacters(in: unreserved)
        return addingPercentEncoding(withAllowedCharacters: allowed as CharacterSet)
    }
    
}
