//
//  NSStoryboard.Name.swift
//  DeskAlerts
//
//  Created by Sergey Dmitriev on 28/12/2018.
//  Copyright © 2018 Toolbarstudio Inc. All rights reserved.
//

import AppKit

extension NSStoryboard.Name {
    
    static let windowCommandStoryboard = "WindowCommand"
    
}

extension NSStoryboard.SceneIdentifier {
    
    static let windowCommandSceneIdentifier = "WindowCommand"
    
}
