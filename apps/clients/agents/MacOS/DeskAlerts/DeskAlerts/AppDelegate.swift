//
//  AppDelegate.swift
//  DeskAlerts
//
//  Created by Admin on 10.04.17.
//  Copyright © 2017 Toolbarstudio Inc. All rights reserved.
//

import Cocoa
import CoreData
import CoreServices
import Foundation

class DeskAlertsApplication: NSApplication {
    
    let strongDelegate = AppDelegate()
    
    override init() {
        super.init()
        self.delegate = strongDelegate
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

@NSApplicationMain
class AppDelegate: NSObject {}

extension AppDelegate: NSApplicationDelegate {
    
    func applicationDidFinishLaunching(_ aNotification: Notification) {
        ScreenSaverManager.shared.activate()
        ClientSettings.shared.loadData()
        SettingsManager.shared.activate()
        CrashReportManager.shared.checkCrashReports()
        WallpaperManager.shared.activate()
    }
        
    @objc func screenLocked() {
        Config.isLockScreen = true
    }
    
    @objc func screenUnlocked() {
        Config.isLockScreen = false
        if let windowQueue = Config.WindowQueue {
            for window in windowQueue {
                if let notification = window.notification {
                    NSUserNotificationCenter.default.removeDeliveredNotification(notification)
                }
                guard let alert = window.alert else { continue }
                Loader.sharedInstance.showAlertFromLocalHtml(alert: alert)
            }
        }
    }
    
    func applicationWillTerminate(_ aNotification: Notification) {
    }
    
    func applicationShouldTerminate(_ sender: NSApplication) -> NSApplication.TerminateReply {
        CoreDataManager.instance.saveContext()
        return .terminateNow
    }
    
}

