//
//  AlertRequest.swift
//  DeskAlerts
//
//  Created by Admin on 15/04/2019.
//  Copyright © 2019 Toolbarstudio Inc. All rights reserved.
//
import Alamofire
import SWXMLHash
import CryptoSwift

fileprivate struct Keys {
    static let terminatedAlertsID = "terminatedAlerts"
    static let status = "status"
    static let data = "data"
}



class RequestAlerts: RequestBase{
    private let jac = JsonApiClient()
    private var _localTime: String!
    private var _alertBody: String!
    private var _alertId: String!
    override var alertBody: String{
        get{
            if _alertBody == nil{
                _alertBody = ""
            }
            return _alertBody
        }set{
            _alertBody = newValue
        }
    }
    override var alertId: String{
        get{
            if _alertId == nil{
                _alertId = ""
            }
            return _alertId
        }set{
            _alertId = newValue
        }
    }
    private var localTime: String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy|HH:mm:ss"
        let dateInFormat = dateFormatter.string(from: NSDate() as Date)
        _localTime = dateInFormat
        
        return _localTime
    }
    override func getUrl() -> String {
        let urlAlerts = "\(jac.CURRENT_SERVER_URL)\(jac.POSTALERTS)"
        return urlAlerts
    }
    override func getBody() -> [String : Any] {
        let body = ["localTime":"\(localTime)"]
        return body
    }
    
    //MARK: - HandlerRequest
    override func handlerRequest(resp: DataResponse<Any>) -> String {
        let result = resp.result
        if let dict = result.value as? [String:Any] {
            if let stat = dict["status"] as? String {
                if let data = dict["data"] as? [String:Any] {
                    updateAlerts(by: data)
                    updateWallpaperHash(by: data)
                    updateScreensaversHash(by: data)
                    parsingTerminateAlertsID(by: data)
                }
                self.jac.statusContent = stat
            }
        }
        return self.jac.statusContent
    }
    //MARK: - UpdateAlerts
    private func updateAlerts(by data: [String:Any]) {
        if let alerts = data["alerts"] as? [Dictionary<String,Any>] {
            for index in 0..<alerts.count{
                if let id = alerts[index]["id"] as? Int{
                    self._alertId = "\(id)"
                }
                if let body = alerts[index]["body"] as? String {
                    self._alertBody = body
                    let responceVithContent = "<TOOLBAR>\(body)</TOOLBAR>"
                    print("responceVithContent \(responceVithContent)")
                    let parseResult = SWXMLHash.parse(responceVithContent)
                    Loader.sharedInstance.LoadAlert(toolbar: parseResult)
                }
            }
        }
    }
    //MARK: - UpdateWallpaperHash
    private func updateWallpaperHash(by data: [String:Any]) {
        if let wallpapersHash = data["wallpapersHash"] as? String {
            let wallpHash = wallpapersHash
            if wallpHash != Config.WallpapersHash{
                Config.WallpapersHash = wallpHash
                Config.WallpapersHashChanged = true
            }else{
                Config.WallpapersHashChanged = false
            }
        }
    }
    //MARK: - UpdateScreensaversHash
    private func updateScreensaversHash(by data: [String:Any]) {
        if let screensaversHash = data["screensaversHash"] as? String {
            let screenHash = screensaversHash
            if screenHash != Config.ScreensaversHash{
                Config.ScreensaversHash = screenHash
                Config.ScreensaversHashChanged = true
            }else{
                Config.ScreensaversHashChanged = false
            }
        }
    }
    //MARK: - ParsingTerminateAlertsID
    private func parsingTerminateAlertsID(by data: [String:Any]) {
        guard let terminatedAlertsID = data[Keys.terminatedAlertsID] as? [Int] else { return }
        if terminatedAlertsID.count > 0 {
            WindowCoordinator.shared.terminateAlerts(by: terminatedAlertsID)
        }
    }
}

