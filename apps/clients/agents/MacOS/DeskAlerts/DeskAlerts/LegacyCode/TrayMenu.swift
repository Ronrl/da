import Foundation
import Cocoa


//TODO: убрать этот клас совсем, на данным момент он является точкой входа!!!
class TrayMenu:NSObject  {
    
    static var sharedInstance = TrayMenu()
    override func awakeFromNib() {
        let filePath = Bundle.main.path(forResource: "conf", ofType: "xml", inDirectory: "Resources")
        if (((filePath) != nil)&&(!(filePath?.isEmpty)!)) {
            let contentData = FileManager.default.contents(atPath: filePath!)
            if (((contentData) != nil)&&(!(contentData?.isEmpty)!)) {
                let content = NSString(data: contentData!, encoding: String.Encoding.utf8.rawValue) as String?
                if (((content) != nil)&&(!(content?.isEmpty)!)) {
                    Config.XML = Config.shared.CreateConfig(content: content!)
                    if ((Config.XML?.children.count)==0) {
                        Config.shared.ErrorLoadingConfig()
                    }
                    else {
                        TrayMenuManager.shared.createDropDownMenu()
                        TrayMenuManager.shared.updateState()
                        
                        Config.AlertsLoader = Loader.sharedInstance
                        Config.AlertsLoader?.restoreAlerts()
                    }
                    
                }
                else {
                    Config.shared.ErrorLoadingConfig()
                }
            }
            else {
                Config.shared.ErrorLoadingConfig()
            }
        }
        else {
            Config.shared.ErrorLoadingConfig()
        }
    }
}
