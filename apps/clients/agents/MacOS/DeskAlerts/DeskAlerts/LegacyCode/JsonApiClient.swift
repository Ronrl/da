//
//  JsonApiClient.swift
//  DeskAlerts
//
//  Created by Admin on 04/04/2019.
//  Copyright © 2019 Toolbarstudio Inc. All rights reserved.
//

import Foundation
import Alamofire

class JsonApiClient{
    
    private static var _token:String!
    private static var _statusContent: String!
    private static var _statusToken: String!
    static var _header:[String:String]!
    private static var _statusReceived: String!
    let CURRENT_SERVER_URL = ClientSettings.shared.settingsList.serverPath
    let ADUSER = "/api/v1/security/token/aduser"
    let NONADUSER = "/api/v1/security/token/user"
    let POSTALERTS = "/api/v1/user/alerts" //получение новых алертов, Hash Wallpapers и Hash Screensavers
    let POSTWALLPAPERS = "/api/v1/user/content/wallpapers" //получение wallpapers
    let POSTSCREENSAVERS = "/api/v1/user/content/screensavers" //получение screensavers
    let POSTRECEIVED = "/api/v1/user/alerts/received" //отправка списка полученных алертов
    let POSTREAD = "/api/v1/user/content/read" //отправка подтверждения о прочтении алерта
    var statusReceived: String{
        get{
            if JsonApiClient._statusReceived == nil{
                JsonApiClient._statusReceived = ""
            }
            return JsonApiClient._statusReceived
        }
        set{JsonApiClient._statusReceived = newValue}
    }
    var statusContent: String{
        get{
            if JsonApiClient._statusContent == nil{
                JsonApiClient._statusContent = ""
            }
            return JsonApiClient._statusContent
        }
        set{JsonApiClient._statusContent = newValue}
    }
    var statusToken: String{
        get{
            if JsonApiClient._statusToken == nil{
                JsonApiClient._statusToken = ""
            }
            return JsonApiClient._statusToken
        }
        set{JsonApiClient._statusToken = newValue}
    }
    var header: [String:String]{
        get{
            if JsonApiClient._header == nil{
                JsonApiClient._header = ["Authorization:":""]
            }
            return JsonApiClient._header
        }
        set{JsonApiClient._header = newValue}
    }
    var token : String {
        get{
            if JsonApiClient._token == nil
            {
                JsonApiClient._token = ""
            }
            return JsonApiClient._token
        }
        set{ JsonApiClient._token = newValue }
    }
    func send(requestContent: RequestBase){
        let url = requestContent.getUrl()
        let body = requestContent.getBody()
        Alamofire.request(url, method: .post, parameters: body, encoding: JSONEncoding.default, headers: header).responseJSON
            { response in
                switch response.result{
                case .success(_):
                    self.statusContent = requestContent.handlerRequest(resp: response)
                case .failure(let error):
                    print("Error: can't get response: \(error)") 
                }
            }
    }
}
