//
//  Get_Token.swift
//  DeskAlerts
//
//  Created by Admin on 04/04/2019.
//  Copyright © 2019 Toolbarstudio Inc. All rights reserved.
//
import Foundation
import Alamofire

private struct Constants {
    static let defaultMD5Password = ""
}

private enum DeviceType: String {
    case windowsDesktop = "1"
    case android = "2"
    case iOS = "3"
    case macDesktop = "4"
}

class RequestToken: RequestBase{
    private var _adClient:Bool = false
    private var _md5Password: String!
    private var _deviceType: String = DeviceType.windowsDesktop.rawValue
    private var _userName: String!
    private var _deskbarId: String!
    private var _iP: String!
    private var _computerName: String!
    private var _domain: String!
    private var _fullDomain: String!
    private var _compDomain: String!
    private let jac = JsonApiClient()
    private var adClient:Bool {
        let authorizationType = AuthorizationManager.shared.authorizationType
        if authorizationType == .selfRegistaration,
            let doaminName = Config.SessionUser?.domain_name,
            doaminName != "" {
            return true
        }
        if authorizationType == .activeDirectory || authorizationType == .activeDirectoryWithSyncFree {
            return true
        } else {
            return false
        }
    }
    private var computerName: String {
        _computerName = ""
        return _computerName
    }
    private var domain: String{
        _domain = Config.shared.GetSettingsPropertyValue(id: "adEnabled")
        return _domain
    }
    private var fullDomain: String {
        if let sessionUserDomainName = Config.SessionUser?.domain_name, sessionUserDomainName != "" {
            return sessionUserDomainName
        } else {
            guard let domainName = DomainManager.shared.domainName else {
                return ""
            }
            return domainName
        }
    }
    private var compDomain:String {
        if let sessionUserDomainName = Config.SessionUser?.domain_name, sessionUserDomainName != "" {
            return sessionUserDomainName
        } else {
            guard let domainName = DomainManager.shared.domainName else {
                return ""
            }
            return domainName
        }
    }
    
    private var md5Password: String {
        _md5Password = Constants.defaultMD5Password
        if let md5Password = Config.SessionUser?.md5_password, !md5Password.isEmpty {
            _md5Password = md5Password
        }
        return _md5Password
    }
    
    private var deviceType: String{
        return _deviceType
    }
    
    //TODO: УЖАСНЫЙ ГОВНОГОД, ИСПРАВИТЬ ПЕРЕРАБОТКОЙ МЕХАНИЗМА ПОЛУЧЕНИЯ ТОКЕНОВ И МАНИПУЛЯЦИЯМИ С ДАННЫМИ ПОЛЬЗОВАТЕЛЯ!!!
    private var userName: String {
        get {
            guard let sessionUser = Config.SessionUser else {
                _userName = ""
                return ""
            }
            if let userName = sessionUser.user_name, userName != "" {
                _userName = userName
                return userName
            } else if let userName = sessionUser.name, userName != "" {
                _userName = userName
                return userName
            } else {
                _userName = ""
                return _userName
            }
        }
    }
    
    private var deskbarId: String{
        _deskbarId = (Config.SessionUser?.deskalerts_id)
        return _deskbarId!
    }

    private var iP: String{
        _iP = getIPAddresses().joined(separator:",")
        return _iP
    }
    // MARK: Get ip adreses.
    private func getIPAddresses() -> [String] {
        var addresses = [String]()
        // Get list of all interfaces on the local machine:
        var ifaddr : UnsafeMutablePointer<ifaddrs>?
        guard getifaddrs(&ifaddr) == 0 else { return [] }
        guard let firstAddr = ifaddr else { return [] }
        // For each interface ...
        for ptr in sequence(first: firstAddr, next: { $0.pointee.ifa_next }) {
            let flags = Int32(ptr.pointee.ifa_flags)
            let addr = ptr.pointee.ifa_addr.pointee
            
            // Check for running IPv4, IPv6 interfaces. Skip the loopback interface.
            if (flags & (IFF_UP|IFF_RUNNING|IFF_LOOPBACK)) == (IFF_UP|IFF_RUNNING) {
                if addr.sa_family == UInt8(AF_INET) || addr.sa_family == UInt8(AF_INET6) {
                    
                    // Convert interface address to a human readable string:
                    var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                    if (getnameinfo(ptr.pointee.ifa_addr, socklen_t(addr.sa_len), &hostname, socklen_t(hostname.count),
                                    nil, socklen_t(0), NI_NUMERICHOST) == 0) {
                        let address = String(cString: hostname)
                        addresses.append(address)
                    }
                }
            }
        }
        freeifaddrs(ifaddr)
        return addresses
    }
    
     override func getUrl() -> String{
        if adClient {
            return "\(jac.CURRENT_SERVER_URL)\(jac.ADUSER)"
        } else {
            return "\(jac.CURRENT_SERVER_URL)\(jac.NONADUSER)"
        }
    }
    
     override func getBody() -> [String:Any]{
        var body = [String : Any]()
        if adClient {
            body = ["DeskbarId": deskbarId,
                    "UserName": userName,
                    "ComputerName": "MAC",
                    "ComputerDomainName": fullDomain,
                    "Ip": iP,
                    "DeviceType": deviceType,
                    "ClientVersion": ClientSettings.shared.settingsList.clientVersion,
                    "DomainName": fullDomain]
        } else {
           body = ["deskbarId": deskbarId,
                   "userName": userName,
                   "computerName": computerName,
                   "ip": iP,
                   "deviceType": deviceType,
                   "clientVersion": ClientSettings.shared.settingsList.clientVersion,
                   "md5Password": md5Password]
        }
        return body
    }
    
    override func handlerRequest(resp: DataResponse<Any>) -> String {
        let result = resp.result
        if let dict = result.value as? [String:Any]{
            if let stat = dict["status"] as? String {
                if stat == "Success"{
                    if let data = dict["data"] as? [String:Any]{
                        if let token = data["authorization"] as? String{
                            self.jac.token = token
                            AuthorizationManager.shared.token = token
                            self.jac.header = ["Authorization":"\(token)"]
                            print("I got token \(token)")
                        }
                    }
                    self.jac.statusToken = stat
                }else{
                    print("Error: status can't be get")
                }
            }
        }
        return self.jac.statusToken
    }
}
