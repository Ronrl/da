//
//  Loader.swift
//  DeskAlerts
//
//  Created by Admin on 11.04.17.
//  Copyright © 2017 Toolbarstudio Inc. All rights reserved.
//
import Foundation
import Alamofire
import Cocoa
import SWXMLHash
import SINQ
import ScreenSaver
import CryptoSwift

private struct Constants {
    static let separatorString = "<!-- separator -->"
    static let htmlTitleRegularExpression = "<!-- html_title = '(.*?)' -->"
}

class Loader {
    static let sharedInstance = Loader()
    var LoaderTimer:Timer?
    let context = CoreDataManager.instance.managedObjectContext
    let jsonApiClient = JsonApiClient()
    let requestToken = RequestToken()
    let requestAalert = RequestAlerts()
    let requestWallpaper = RequestWalpapers()
    let requestScreensaver = RequestScreensavers()
    let received = Received()
    let read = Read()
    var alerts = [String:String]()
    var wallpapers = [String:String]()
    var screensavers = [String:String]()
    
    //MARK: - Init
    init() {
        if AuthorizationManager.shared.authorizationType == .activeDirectoryWithSyncFree {
            SyncFreeManager.shared.startSynchronization()
        }
        if Config.SessionUser?.user_name == nil {
            AuthorizationManager.shared.registerUser()
        }
        LoaderTimer = Timer.scheduledTimer(timeInterval: ClientSettings.shared.settingsList.normalExpire, target: self, selector: #selector(HandleTimer), userInfo: nil, repeats: true)
        LoaderTimer?.fire()
    }
    @objc func HandleTimer() -> Void {
        self.LoadToolbar()
    }
    func randomString(fromchars: String, length: Int) -> String {
        let letters : String = fromchars
        let len = UInt32(letters.length)
        var randomString = ""
        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            let nextChar = letters[ Int(rand)]
            randomString += nextChar
        }
        
        return randomString
    }
    func LoadAlert(toolbar: XMLIndexer) -> Void {
        let error = toolbar["TOOLBAR"]["ERROR"]
        if (error.element != nil)
        {
            if (!(error.element?.text.isEmpty)!)
            {
                let errortext = error.element?.text
                if (errortext?.lowercased().range(of:"user does not exists") != nil) {
                    AuthorizationManager.shared.registerUser()
                }
            }
        }
        else {
            alerts = SetAlerts(toolbar: toolbar)
            wallpapers = SetWallpapers(toolbar: toolbar)
            screensavers = SetScreensavers(toolbar: toolbar)
            if alerts != ["":""] || wallpapers != ["":""] || screensavers != ["":""]{
                received.wasReceived = ["alerts": [alerts], "wallpapers": [wallpapers], "screensavers": [screensavers]]
                jsonApiClient.send(requestContent: received)
                self.alerts.removeAll()
                self.wallpapers.removeAll()
                self.screensavers.removeAll()
            }
        }
        TrayMenuManager.shared.updateState()
    }
    func SetAlerts(toolbar: XMLIndexer) -> [String:String] {
        for alertXML in toolbar["TOOLBAR"]["ALERT"].all {
            guard let alert = Config.shared.GetAlertFromXml(alert: alertXML, context: context) else { continue }
            guard let alerttext = getAlertText(alert: alert) else { continue }
            let isHighPriorityAlert = alerttext.urgent == "1"
            
            context.insert(alerttext)
            CoreDataManager.instance.saveContext()
            if !SettingsManager.shared.unobtrusiveModeEnabled {
                if alerttext.class_id != "16" {
                    showAlertFromLocalHtml(alert: alerttext)
                    if SettingsManager.shared.audioEffectEnabled || isHighPriorityAlert {
                        EffectManager.shared.playSoundReceivingAlert()
                    }
                }
            } else {
                if isHighPriorityAlert {
                    showAlertFromLocalHtml(alert: alerttext)
                    EffectManager.shared.playSoundReceivingAlert()
                }
            }
            alerts = ["id":alerttext.alert_id, "receivedStatus":"1"] as! [String : String]
        }
        TrayMenuManager.shared.updateState()
        return alerts
    }
    
    func SetWallpapers(toolbar: XMLIndexer)-> [String:String]
    {
        for wallpaper in toolbar["TOOLBAR"]["WALLPAPERS"].all
        {
            if (Config.WallpapersHash == wallpaper.element?.attribute(by: "hash")?.text) {
                
                var uname = ""
                
                if let tmpUserName = Config.SessionUser?.user_name?.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed), tmpUserName != "" {
                    uname = tmpUserName
                } else if let tmpUserName = Config.SessionUser?.name?.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed), tmpUserName != "" {
                    uname = tmpUserName
                }
                
                var domaiName = ""
                
                if let sessionUserDomainName = Config.SessionUser?.domain_name, sessionUserDomainName != "" {
                    domaiName = sessionUserDomainName
                } else if let tmpDomainName = DomainManager.shared.domainName {
                    domaiName = tmpDomainName
                }
                
                guard let templateWallpapersURL = Config.shared.GetSettingsPropertyValue(id: "wallpapers_url") else {
                    DebugLogger.showAlert(with: "Error: property 'wallpapers_url' not found in config file")
                    return [String: String]()
                }
                
                let wallpapersurl = templateWallpapersURL.replacingOccurrences(of: "%hash%", with: Config.WallpapersHash != nil ? Config.WallpapersHash! : "").replacingOccurrences(of: "%user_name%", with: uname).replacingOccurrences(of: "%domain%", with: domaiName).replacingOccurrences(of: "%fulldomain%", with: domaiName).replacingOccurrences(of: "%compdomain%", with: domaiName)
                
                if !wallpapersurl.isEmpty {
                    Alamofire.request(wallpapersurl).validate().responseString{ response in
                        if (response.result.isSuccess)
                        {
                            var alerthtml:String?
                            alerthtml = response.result.value
                            
                            if (alerthtml != nil)
                            {
                                alerthtml = alerthtml?.description
                                let wallpapersxml = SWXMLHash.parse(alerthtml!)
                                if (wallpapersxml.all.count>0)
                                {
                                    DatabaseManager.shared.setAllWallpapersStateIsClosed()
                                    
                                    if (wallpapersxml["WALLPAPERS"]["ALERT"].all.count>0)
                                    {
                                        for wallpaperalert in wallpapersxml["WALLPAPERS"]["ALERT"].all
                                        {
                                            do {
                                                guard let wallalert = Config.shared.GetAlertFromXml(alert: wallpaperalert, context: self.context) else { return }
                                                //OR next possibility
                                                let id = wallalert.alert_id
                                                wallalert.id = "wallpaper"
                                                let hist = self.getAlertText(alert: wallalert, addjavascript: false)
                                                //Use image's path to create NSData
                                                let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
                                                let entityDescription = NSEntityDescription.entity(forEntityName: "Alert", in: self.context)
                                                fetchRequest.entity = entityDescription
                                                if !self.getWallpaperImageFromServer(hist!, fetchRequest){
                                                    break
                                                }
                                                self.requestWallpaper.wallpaperId = "\(id ?? "")"
                                            }
                                        }
                                    }
                                    if (Config.FirstTimeWallpaperChanging == nil) || (Config.FirstTimeWallpaperChanging == true) {
                                        Config.FirstTimeWallpaperChanging = false;
                                    }
                                    DispatchQueue.main.async {
                                        WallpaperManager.shared.updateWallpaperList()
                                    }
                                    Config.WallpapersHash = wallpaper.element?.attribute(by: "hash")?.text
                                }
                            }
                            self.wallpapers = ["id": self.requestWallpaper.wallpaperId , "receivedStatus":"1"]
                            
                        }
                        else
                        {
                            print("error request server")
                        }
                    }
                }
                else
                {
                    Config.WallpapersHash = ""
                    Config.FirstTimeWallpaperChanging = true
//                    Wallpaper.sharedInstance.Stop()
                }
            }
            else
            {
                Config.WallpapersHashChanged = false
            }
        }
        return wallpapers
    }
    func getWallpaperImageFromServer(_ hist: Alert, _ fetchRequest: NSFetchRequest<NSFetchRequestResult>) -> Bool{
        
        guard let imagePath = hist.alerttext else {
            DebugLogger.showAlert(with: "Error: invalid imagePath")
            return false
        }

        guard let imagePathURLFriendly = imagePath.stringByAddingPercentEncodingForRFC3986() else {
            DebugLogger.showAlert(with: "Error: encodeForURL imagePath. \(imagePath)")
            return false
        }

        guard let imageURL = URL(string: imagePathURLFriendly) else {
            DebugLogger.showAlert(with: "Error: invalid imagePathURLFriendly. \(imagePathURLFriendly)")
            return false
        }
        
        do {
            let imageData = try Data.init(contentsOf: imageURL)
            let strBase64 = imageData.base64EncodedString(options: .lineLength64Characters)
            hist.alerttext = "<img src=\"data:image/jpeg;base64," + strBase64 + "\"/>"
            let ress = try self.self.context.fetch(fetchRequest)
            let results2 = from(ress as! [Alert]).whereTrue{($0.alert_id != nil)&&($0.alert_id == (hist.alert_id)!)&&($0.recivetime != hist.recivetime)}.toArray()
            for result in results2
            {
                self.context.delete(result)
            }
            hist.isclosed = false
            hist.isreaded = true
            self.self.context.insert(hist)
            CoreDataManager.instance.saveContext()
        } catch {
            DebugLogger.showAlert(with: error.localizedDescription)
        }
        return true
    }
    func SetScreensavers(toolbar: XMLIndexer) -> [String:String] {
        for screensaver in toolbar["TOOLBAR"]["SCREENSAVERS"].all {
            if (Config.ScreensaversHash == screensaver.element?.attribute(by: "hash")?.text) {
                var uname = ""
                
                if let tmpUserName = Config.SessionUser?.user_name?.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed), tmpUserName != "" {
                    uname = tmpUserName
                } else if let tmpUserName = Config.SessionUser?.name?.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed), tmpUserName != "" {
                    uname = tmpUserName
                }
                
                var domaiName = ""
                
                if let sessionUserDomainName = Config.SessionUser?.domain_name, sessionUserDomainName != "" {
                    domaiName = sessionUserDomainName
                } else if let tmpDomainName = DomainManager.shared.domainName {
                    domaiName = tmpDomainName
                }
                
                guard let templateScreensaversURL = Config.shared.GetSettingsPropertyValue(id: "screensavers_url") else {
                    DebugLogger.showAlert(with: "Error: property 'screensavers_url' not found in config file")
                    return [String: String]()
                }

                let screensaversurl = templateScreensaversURL.replacingOccurrences(of: "%hash%", with: Config.ScreensaversHash != nil ? Config.ScreensaversHash! : "").replacingOccurrences(of: "%user_name%", with: uname).replacingOccurrences(of: "%domain%", with: domaiName).replacingOccurrences(of: "%fulldomain%", with: domaiName).replacingOccurrences(of: "%compdomain%", with: domaiName)
                
                if !screensaversurl.isEmpty {
                    Alamofire.request(screensaversurl).validate().responseString{ response in
                        if (response.result.isSuccess) {
                            var alerthtml:String?
                            alerthtml = response.result.value
                            if (alerthtml != nil) {
                                alerthtml = alerthtml?.description
                                let screensaversxml = SWXMLHash.parse(alerthtml!)
                                DatabaseManager.shared.setClosedStatusForAllScreensavers()
                                if (screensaversxml.all.count>0) {
                                    if (screensaversxml["SCREENSAVERS"]["ALERT"].all.count>0) {
                                        for screensaveralert in screensaversxml["SCREENSAVERS"]["ALERT"].all {
                                            guard let wallalert = Config.shared.GetAlertFromXml(alert: screensaveralert, context: self.context) else {
                                                DatabaseManager.shared.setIsClosedStateFalseToAlert(by: screensaveralert)
                                                continue
                                            }
                                            
                                            let id = wallalert.alert_id
                                            wallalert.id = "screensaver"
                                            guard let hist = self.getAlertText(alert: wallalert, addjavascript: false) else { return }
                                            var htmlforcheckvideo = hist.alerttext
                                            for math in (hist.alerttext?.matches(for: "<OBJECT.*?>([\\s\\S]*?)<\\/OBJECT>").enumerated())!
                                            {
                                                let gettingvideourl = math.element.split(separator: "'")
                                                var width = from(gettingvideourl).whereTrue{$0.contains("width")}.firstOrNil()
                                                var height = from(gettingvideourl).whereTrue{$0.contains("height")}.firstOrNil()
                                                var file = from(gettingvideourl).whereTrue{$0.contains("src")}.firstOrNil()
                                                var newvideobject = "<video"
                                                if (width != nil)
                                                {
                                                    let index = gettingvideourl.index(of: width!)! + 1
                                                    width = gettingvideourl[index]
                                                    newvideobject = newvideobject + " width = \"" + (width?.description)! + "\""
                                                }
                                                if (height != nil)
                                                {
                                                    let index = gettingvideourl.index(of: height!)! + 1
                                                    height = gettingvideourl[index]
                                                    newvideobject = newvideobject + " height = \"" + (height?.description)! + "\""
                                                }
                                                newvideobject = newvideobject + " autoplay loop preload>"
                                                if (file != nil)
                                                {
                                                    let index = gettingvideourl.index(of: file!)! + 1
                                                    file = gettingvideourl[index]
                                                    newvideobject = newvideobject + "<source src=\"" + (file?.description)! + "\">"
                                                }
                                                newvideobject = newvideobject + "Video is unavailible</video>"
                                                
                                                htmlforcheckvideo = htmlforcheckvideo?.replacingOccurrences(of: math.element, with: newvideobject)
                                                
                                            }
                                            hist.alerttext = htmlforcheckvideo
                                            DatabaseManager.shared.removeAlertFromHistoryByID(id: hist.id!)
                                            hist.isclosed = false
                                            hist.isreaded = true
                                            DatabaseManager.shared.insert(alert: hist)
                                            
                                            self.requestScreensaver.screensaverId = "\(id ?? "")"
                                        }
                                        
                                    }
                                }
                            }
                            
                            self.screensavers = ["id":self.requestScreensaver.screensaverId, "receivedStatus":"1"]
                        }
                        else
                        {
                            print("error request server")
                        }
                        ScreenSaverManager.shared.updateScreenSaverState()
                    }
                }
            }
            else
            {
                Config.ScreensaversHashChanged = false
            }
        }
        return screensavers
    }
   
    func stringFromBytes(_ bytes: [UInt8]) -> String {
        return NSString(bytes: bytes, length: bytes.count, encoding: String.Encoding.utf8.rawValue)! as String
    }
    func getAlertText(alert: Alert, addjavascript: Bool? = true) -> Alert? {
        let result = alert
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "dd.MM.yyyy|HH:mm:ss"
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = NSLocale.init(localeIdentifier: "en_US") as Locale?
        
        let datestr = dateFormatter.string(from: Date())
        let date = dateFormatter.date(from: datestr)
        result.recivetime = date! as NSDate
        result.isclosed = false
        result.isreaded = false
        if (result.id=="alert")
        {
            if (result.windows?.count == 0)
            {
                if (result.ticker == "0")
                {
                    do
                    {
                        let alertwindow = try Config.shared.GetAlertWindowFromXml(xml: (Config.XML?["ALERTS"]["VIEWS"]["WINDOW"].withAttribute("name","alertwindow1"))!, context: result.managedObjectContext!, alert: result)
                        result.addToWindows(alertwindow)
                        
                    }
                    catch {
                        let fetchError = error as NSError
                        print(fetchError)
                    }
                }
                else
                {
                    do
                    {
                        let alertwindow = try Config.shared.GetAlertWindowFromXml(xml: (Config.XML?["ALERTS"]["VIEWS"]["WINDOW"].withAttribute("name","tickerwindow1"))!, context: result.managedObjectContext!, alert: result)
                        result.addToWindows(alertwindow)
                    }
                    catch{
                        
                    }
                    
                    
                }
            }
            
            if (result.ticker == "1")
            {
                let alertwindow = Array(result.windows?.allObjects as! [AlertWindow]).first
                
                if (alertwindow != nil)
                {
                    if (alertwindow?.position != nil)
                    {
                        if (alertwindow?.position?.isEmpty)!
                        {
                            if (result.ticker_position != nil)
                            {
                                if (!(result.ticker_position?.isEmpty)!)
                                {
                                    alertwindow?.position = result.ticker_position
                                }
                                else
                                {
                                    alertwindow?.position = "bottom"
                                }
                                
                            }
                            else
                            {
                                alertwindow?.position = "bottom"
                            }
                            
                        }
                    }
                    else
                    {
                        if (result.ticker_position != nil)
                        {
                            if (!(result.ticker_position?.isEmpty)!)
                            {
                                alertwindow?.position = result.ticker_position
                            }
                            else
                            {
                                alertwindow?.position = "bottom"
                            }
                            
                        }
                        else
                        {
                            alertwindow?.position = "bottom"
                        }
                    }
                    if (alertwindow?.width == nil)
                    {
                            alertwindow?.width  = NSScreen.main?.frame.width.description
                    }
                    if (alertwindow?.heigth == nil)
                    {
                        do
                        {
                            let window = try Config.XML?["ALERTS"]["VIEWS"]["WINDOW"].withAttribute("name","tickerwindow1")
                            
                            let element = window?.element
                            let height = element?.attribute(by: "height")?.text
                            alertwindow?.heigth = height
                        }
                        catch {
                            let fetchError = error as NSError
                            print(fetchError)
                        }
                    }
                }
                if (result.width == nil)
                {
                    result.width = NSScreen.main?.frame.width.description
                }
                
                if (result.heigth == nil)
                {
                    do
                    {
                        let window = try Config.XML?["ALERTS"]["VIEWS"]["WINDOW"].withAttribute("name","tickerwindow1")
                        
                        let element = window?.element
                        let height = element?.attribute(by: "height")?.text
                        result.heigth = height
                    }
                    catch {
                        let fetchError = error as NSError
                        print(fetchError)
                    }
                }
            }
        }
        var serv = ""
        for char in (result.href)!
        {
            
            if (char != "?")
            {
                serv = serv + char.description
            }
            else
            {
                break
            }
            
        }
        let requestalert = serv + "?" + result.href!.replacingOccurrences(of: serv, with: "").replacingOccurrences(of: "?", with: "").addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        
        let response =  Alamofire.request(requestalert).validate().responseData()
        
        if (response.result.isSuccess) {
            guard var alerthtml =  String.init(data: response.result.value!, encoding: String.Encoding.utf8) else {
                DebugLogger.showAlert(with: "Invalid alerthtml")
                return nil
            }
            
            if !alerthtml.isEmpty {
                if (addjavascript)!
                {
                    var customjs = ""
                    let alertwindow = Array(result.windows?.allObjects as! [AlertWindow]).first
                    if (alertwindow != nil)
                    {
                        customjs = (FileManager.default.fileExists(atPath: (alertwindow?.customjs)!)
                            ? String.init(data: FileManager.default.contents(atPath: (alertwindow?.customjs)!)!, encoding: String.Encoding.utf8)
                                
                            : Alamofire.request((alertwindow?.customjs!)!).validate().responseString(encoding: String.Encoding.utf8).result.value)!
                        
                        
                        var headpos = -1
                        for head in (alerthtml.matches(for: "<head[^>]*>"))
                        {
                            headpos = (alerthtml.indexOf(ofstring: head))! + head.length
                        }
                        
                        var bodypos = -1
                        
                        
                        for body in (alerthtml.matches(for: "<body[^>]*>"))
                        {
                            bodypos = (alerthtml.indexOf(ofstring: body))! + body.length
                        }
                        
                        let jquery = String.init(data: FileManager.default.contents(atPath: (Config.serviceFolder + "jquery.min.js" ))!, encoding: String.Encoding.utf8)!
                        let json = String.init(data: FileManager.default.contents(atPath: (Config.serviceFolder + "json2.js" ))!, encoding: String.Encoding.utf8)!
                        
                        var pos = 0
                        var assignedlength = 0
                        if (headpos != -1)
                        {
                            pos = headpos
                            let headstr = "\n<script type=\"text/javascript\">\n"
                            
                            alerthtml = alerthtml.insert(string: headstr, ind: pos)
                            pos = pos + headstr.length
                            assignedlength = assignedlength + headstr.length
                            
                            alerthtml = alerthtml.insert(string: customjs, ind: pos)
                            pos = pos + customjs.length
                            assignedlength = assignedlength + customjs.length
                            
                            let endstr = "</script>"
                            alerthtml = alerthtml.insert(string: endstr, ind: pos)
                            pos = pos + endstr.length
                            assignedlength = assignedlength + endstr.length
                            
                            alerthtml = alerthtml.insert(string: headstr, ind: pos)
                            pos = pos + headstr.length
                            assignedlength = assignedlength + headstr.length
                            
                            alerthtml = alerthtml.insert(string: jquery, ind: pos)
                            pos = pos + jquery.length
                            assignedlength = assignedlength + jquery.length
                            
                            alerthtml = alerthtml.insert(string: endstr, ind: pos)
                            pos = pos + endstr.length
                            assignedlength = assignedlength + endstr.length
                            
                            alerthtml = alerthtml.insert(string: headstr, ind: pos)
                            pos = pos + headstr.length
                            assignedlength = assignedlength + headstr.length
                            
                            alerthtml = alerthtml.insert(string: json, ind: pos)
                            pos = pos + json.length
                            assignedlength = assignedlength + json.length
                            
                            alerthtml = alerthtml.insert(string: endstr, ind: pos)
                            pos = pos + endstr.length
                            assignedlength = assignedlength + endstr.length
                            
                            let fullscreenvideodisablecss = "<style>video::-webkit-media-controls-fullscreen-button {display: none;}</style"
                            alerthtml = alerthtml.insert(string: fullscreenvideodisablecss, ind: pos)
                            pos = pos + fullscreenvideodisablecss.length
                            assignedlength = assignedlength + fullscreenvideodisablecss.length
                            
                            var csspath = URL(fileURLWithPath: (alertwindow?.customjs!)!)
                            csspath.deleteLastPathComponent()
                            let stringcsspath = csspath.absoluteString + "/skin.css"
                            let customcss = FileManager.default.fileExists(atPath: (stringcsspath))
                                ? String.init(data: FileManager.default.contents(atPath: (stringcsspath))!, encoding: String.Encoding.utf8)!
                                    
                                : Alamofire.request((stringcsspath)).validate().responseString(encoding: String.Encoding.utf8).result.value!
                            
                            let endstring = "<style media=\"screen\" type=\"text/css\">\n" + customcss +
                            "\n</style>";
                            
                            alerthtml = alerthtml.insert(string: endstring, ind: pos)
                            pos = pos + endstring.length
                            assignedlength = assignedlength + endstring.length
                            
                        }
                        else
                        {
                            pos = 0
                            
                            for doctype in (alerthtml.matches(for: "<!DOCTYPE[^>]*>"))
                            {
                                pos = (alerthtml.indexOf(ofstring: doctype))! + doctype.length
                            }
                            
                            for html in (alerthtml.matches(for: "<body[^>]*>"))
                            {
                                pos = (alerthtml.indexOf(ofstring: html))!
                            }
                            
                            let headstr = "\n<head><script type=\"text/javascript\">\n"
                            
                            alerthtml = alerthtml.insert(string: headstr, ind: pos)
                            pos = pos + headstr.length
                            assignedlength = assignedlength + headstr.length
                            
                            alerthtml = alerthtml.insert(string: customjs, ind: pos)
                            pos = pos + customjs.length
                            assignedlength = assignedlength + customjs.length
                            
                            let endstr = "</script>"
                            alerthtml = alerthtml.insert(string: endstr, ind: pos)
                            pos = pos + endstr.length
                            assignedlength = assignedlength + endstr.length
                            
                            alerthtml = alerthtml.insert(string: headstr, ind: pos)
                            pos = pos + headstr.length
                            assignedlength = assignedlength + headstr.length
                            
                            alerthtml = alerthtml.insert(string: jquery, ind: pos)
                            pos = pos + jquery.length
                            assignedlength = assignedlength + jquery.length
                            
                            alerthtml = alerthtml.insert(string: endstr, ind: pos)
                            pos = pos + endstr.length
                            assignedlength = assignedlength + endstr.length
                            
                            alerthtml = alerthtml.insert(string: headstr, ind: pos)
                            pos = pos + headstr.length
                            assignedlength = assignedlength + headstr.length
                            
                            alerthtml = alerthtml.insert(string: json, ind: pos)
                            pos = pos + json.length
                            assignedlength = assignedlength + json.length
                            
                            alerthtml = alerthtml.insert(string: endstr, ind: pos)
                            pos = pos + endstr.length
                            assignedlength = assignedlength + endstr.length
                            
                            let fullscreenvideodisablecss = "<style>video::-webkit-media-controls-fullscreen-button {display: none;}</style"
                            alerthtml = alerthtml.insert(string: fullscreenvideodisablecss, ind: pos)
                            pos = pos + fullscreenvideodisablecss.length
                            assignedlength = assignedlength + fullscreenvideodisablecss.length
                            
                            var csspath = URL(fileURLWithPath: (alertwindow?.customjs!)!)
                            csspath.deleteLastPathComponent()
                            let stringcsspath = csspath.absoluteString + "/skin.css"
                            let customcss = FileManager.default.fileExists(atPath: (stringcsspath))
                                ? String.init(data: FileManager.default.contents(atPath: (stringcsspath))!, encoding: String.Encoding.utf8)!
                                    
                                : Alamofire.request((stringcsspath)).validate().responseString(encoding: String.Encoding.utf8).result.value!
                            
                            let endstring = "<style media=\"screen\" type=\"text/css\">\n" + customcss +
                            "\n</style></head>";
                            
                            alerthtml = alerthtml.insert(string: endstring, ind: pos)
                            pos = pos + endstring.length
                            assignedlength = assignedlength + endstring.length
                        }
                        
                        if (bodypos != -1)
                        {
                            bodypos = bodypos + assignedlength
                            pos = bodypos
                        }
                        
                        let alertidstr = "<!-- alertid = '" + result.alert_id! +
                            "' --> <div id='alertid' alertid='" + result.alert_id! + "'></div>"
                        
                        alerthtml = alerthtml.insert(string: alertidstr, ind: pos)
                        pos = pos + alertidstr.length
                        
                        if (result.acknown == "1")
                        {
                            let acknownstr =  "<!-- readit = '" + result.alert_id! + "', '" + result.alert_id! +
                                "', '" +
                                result.user_id! +
                                "' --><!-- cannot close --><div id='readItDiv' file='" +
                                result.alert_id! + "' alertId='" +
                                result.alert_id! + "' userId='" + result.user_id! + "' ></div>"
                            alerthtml = alerthtml.insert(string: acknownstr, ind: pos)
                            pos = pos + acknownstr.length
                            read.acknowAlertId = result.alert_id!
                            jsonApiClient.send(requestContent: read)
                        }
                        
                        if (result.hide_close == "1")
                        {
                            let acknownstr =  "<!-- cannot close -->"
                            alerthtml = alerthtml.insert(string: acknownstr, ind: pos)
                            pos = pos + acknownstr.length
                        }
                        
                        if (Int(result.autoclose!)! > 0)
                        {
                            let autoclosestr = "<!-- autoclose = '\(result.autoclose!)', '\(result.alert_id!)' --><div id ='autoclose' value='\(result.autoclose!)' file='\(result.alert_id!)'></div><!-- cannot close -->"

                            alerthtml = alerthtml.insert(string: autoclosestr, ind: pos)
                            pos = pos + autoclosestr.length
                        }
                        
                        if (Int(result.autoclose!)! < 0)
                        {
                            let autoclosestr = "<!-- autoclose = '" + result.autoclose! + "', '" + result.alert_id!  +
                                "' --><div id='autoclose' value='" + result.autoclose! + "' file='" +
                                result.alert_id!  + "'></div>"
                            alerthtml = alerthtml.insert(string: autoclosestr, ind: pos)
                            pos = pos + autoclosestr.length
                        }
                        
                        if (!(result.to_date?.isEmpty)!)
                        {
                            let todatestr = "<!-- todate = '" + result.to_date! + "', '" + result.alert_id! + "', '" +
                                result.user_id! + "', '" + result.alert_id! +
                                "' --><div id='todate' date='" +
                                result.to_date! + "' alert_id='" + result.alert_id! +
                                "' user_id='" + result.user_id! + "' file='" + result.alert_id! +
                            "' ></div>";
                            alerthtml = alerthtml.insert(string: todatestr, ind: pos)
                            pos = pos + todatestr.length
                        }
                        if (result.tickerspeed != nil)
                        {
                            let tickerspeedstrstr = "<!-- tickerspeed = '" + result.tickerspeed! + "' --><div id='tickerspeed' value='" + result.tickerspeed! + "'></div>"
                            alerthtml = alerthtml.insert(string: tickerspeedstrstr, ind: pos)
                            pos = pos + tickerspeedstrstr.length
                        }
                        
                        if result.survey == nil ||
                           result.survey == "0" ||
                           result.survey == ""  ||
                           result.survey == "1" {
                            if var trueTitle = alerthtml.matches(for: Constants.htmlTitleRegularExpression).last,
                                trueTitle != Constants.htmlTitleRegularExpression {
                                trueTitle = trueTitle.replacingOccurrences(of: "<!-- html_title = '", with: "")
                                trueTitle = trueTitle.replacingOccurrences(of: "' -->", with: "")
                                trueTitle = trueTitle.replacingOccurrences(of: "\\", with: "")
                                
                                result.title = trueTitle
                            }
                        }
                    }
                }
                result.alerttext = alerthtml
            }
            else
            {
                print("error loading alert text")
            }
            result.users = Config.SessionUser
            
            return result
        }
        else
        {
            return nil
        }
    }
    func showAlertFromLocalHtml(alert: Alert) {
        guard let alertText = alert.alerttext else {
            DebugLogger.showAlert(with: "Inavalid alert text")
            return
        }
        if alert.isTiker {
            show(tiker: alert, content: alertText)
        } else {
//            if alert.isWallpaper {
//                guard let encodeImageURLString = alertText.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) else { return }
//                showAlert(alert: alert, alerthtml: encodeImageURLString)
//            } else {
//                showAlert(alert: alert, alerthtml: alertText)
//            }
            showAlert(alert: alert, alerthtml: alertText)
            
        }
    }
    
    private func showAlert(alert: Alert, alerthtml:String) {
        let storyboard = NSStoryboard(name: .windowCommandStoryboard, bundle: nil)
        guard let windowCommand = storyboard.instantiateController(withIdentifier: .windowCommandSceneIdentifier) as? WindowCommand else { return }
        windowCommand.Show(alert: alert, href: alerthtml, islocal: true, includedtickeralerts: nil, includedtickeralertscontent: nil, windowType: .baseContent)
    }
    
    private func getCombinedTickerData(by baseTiker: Alert, with supplementTikers: [Alert]) -> String? {
        guard let startIndexOfBaseTickerContent = getStartIndexOfTickerContent(from: baseTiker) else { return nil }
        
        guard var tmpTikerContent = baseTiker.alerttext else { return nil }
        for item in supplementTikers {
            guard let content = getContent(from: item) else { continue }
            tmpTikerContent = tmpTikerContent.insert(string: content, ind: startIndexOfBaseTickerContent)
            
            tmpTikerContent = tmpTikerContent.insert(string: Constants.separatorString, ind: startIndexOfBaseTickerContent + content.length)
        }
        return tmpTikerContent
    }
    
    private func getStartIndexOfTickerContent(from tiker: Alert) -> Int? {
        guard let tikerContent = tiker.alerttext else { return nil }
        var startIndexOfTickerContent: Int?
        for math in tikerContent.matches(for: "<head.*?>([\\s\\S]*?)<\\/head>").enumerated() {
            var tmpTikerContent = tikerContent.replacingOccurrences(of: math.element, with: "")
            
            tmpTikerContent = tmpTikerContent.replacingOccurrences(of: "<html>", with: "")
            tmpTikerContent = tmpTikerContent.replacingOccurrences(of: "</html>", with: "")
            tmpTikerContent = tmpTikerContent.replacingOccurrences(of: "</body>", with: "")
            if let tmpIndex = tikerContent.indexOf(ofstring: tmpTikerContent) {
                startIndexOfTickerContent = tmpIndex
            }
        }
        return startIndexOfTickerContent
    }
    
    private func getContent(from tiker: Alert) -> String? {
        guard let tikerContent = tiker.alerttext else { return nil }
        var tmpContent = ""
        for math in tikerContent.matches(for: "<head.*?>([\\s\\S]*?)<\\/head>").enumerated() {
            tmpContent = tikerContent.replacingOccurrences(of: math.element, with: "")
            
            for math1 in (tikerContent.matches(for:  "<body[^>]*>"))
            {
                tmpContent = tmpContent.replacingOccurrences(of: math1, with: "")
            }
            tmpContent = tmpContent.replacingOccurrences(of: "<html>", with: "")
            tmpContent = tmpContent.replacingOccurrences(of: "</html>", with: "")
            tmpContent = tmpContent.replacingOccurrences(of: "</body>", with: "")
        }
        return tmpContent
    }
    
    private func show(tiker: Alert, content: String) {
        guard let windowContainer = Config.WindowContainer else { return }
        
        let filteredWindowCommands = windowContainer.filter({ (windowCommand) -> Bool in
            guard let alert = windowCommand.alert else { return false }
            guard let windows = alert.windows?.allObjects as? [AlertWindow] else { return false }
            guard let alerts = windows.first?.alerts else { return false }
            if alerts.ticker == "1" && alerts.ticker_position == tiker.ticker_position {
                return true
            }
            return false
        })
        guard let firstWindowCommand = filteredWindowCommands.first else {
            showAlert(alert: tiker, alerthtml: content)
            return
        }
        
        var supplementTikers = [Alert]()
        guard let firstWindowCommandAlert = firstWindowCommand.alert else { return }
        supplementTikers.append(firstWindowCommandAlert)
        if let includedTickerAlerts = firstWindowCommand.includedTickerAlerts {
            supplementTikers.append(contentsOf: includedTickerAlerts)
        }
        
        guard let combinedTickerData = getCombinedTickerData(by: tiker, with: supplementTikers) else { return }
        firstWindowCommand.Close()
        showCombinedTicker(tiker: tiker, with: combinedTickerData, and: supplementTikers)
    }
    
    private func showCombinedTicker(tiker: Alert, with combinedTickerData: String, and supplementTikers: [Alert]) {
        let storyboard = NSStoryboard(name: .windowCommandStoryboard, bundle: nil)
        guard let windowCommand = storyboard.instantiateController(withIdentifier: .windowCommandSceneIdentifier) as? WindowCommand else { return }
        
        windowCommand.Show(alert: tiker, href: combinedTickerData, islocal: true, includedtickeralerts: supplementTikers, includedtickeralertscontent: nil, windowType: .baseContent)
    }
    
    func redisplayTikers(baseTiker: Alert, supplementTikers: [Alert]) {
        guard let combinedTickerData = getCombinedTickerData(by: baseTiker, with: supplementTikers) else { return }
        showCombinedTicker(tiker: baseTiker, with: combinedTickerData, and: supplementTikers)
    }
    
    func userNotificationCenter(center: NSUserNotificationCenter, didActivateNotification notification: NSUserNotification) {
        switch (notification.activationType) {
        case .replied:
            guard let res = notification.response else { return }
            print("User replied: \(res.string)")
        default:
            break;
        }
    }
    func LoadToolbar()->Void{
        jsonApiClient.send(requestContent: requestAalert)
        if jsonApiClient.statusContent == "NA"{
            jsonApiClient.send(requestContent: requestToken)
        }
        if Config.WallpapersHashChanged == true {
            jsonApiClient.send(requestContent: requestWallpaper)
        }
        
        if Config.ScreensaversHashChanged == true {
            jsonApiClient.send(requestContent: requestScreensaver)
        }
    }
    func restoreAlerts() {
        guard let notClosedAlerts = DatabaseManager.shared.getAlertsNotClosedInPreviousSession() else { return }
        for alert in notClosedAlerts {
            if !alert.isWallpaper, !alert.isScreensaver {
                showAlertFromLocalHtml(alert: alert)
            }
        }
    }
}
