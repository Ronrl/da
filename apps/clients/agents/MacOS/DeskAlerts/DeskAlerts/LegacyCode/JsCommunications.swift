//
//  JsCommunications.swift
//  DeskAlerts
//
//  Created by Admin on 01.05.17.
//  Copyright © 2017 Toolbarstudio Inc. All rights reserved.
//

import Foundation
import Cocoa
import Alamofire
import SINQ
import SWXMLHash
import WebKit

// Custom protocol must be declared with `@objc`
@objc protocol HtmlInteropClassJSExports : JSExport {
    func getProperty(arg0: String?, arg1: String?)->String
    func getProperty(arg0: String?)->String
    func Close()
    func close(closewindow:NSObject?, id:NSObject?)
    func Exit()
    func CloseTicker(id:NSObject?, includedtickers:NSObject?)
    func setProperty(property:NSObject?, value:NSObject?)
    func callInCaption(script:NSObject?, arg1:NSObject?, arg2:NSObject?, arg3:NSObject?)
    func callInBody(script:NSObject?, arg1:NSObject?, arg2:NSObject?, arg3:NSObject?)
    func autoclose(id:NSObject?)
    func getBodyWidth()->String
    func getBodyTop()->String
    func setBodyHeight(height:NSObject?)
    func getBodyHeight()->String
    func format(datetimeformat:NSObject?)->String
    func startResizing()
    func queryURL(url:NSObject)->String
    func hide()
    func show()
    func focus()
    func getFrameDocument()
    func IsResizible()->String
    func setCaptionHeightByBodyHeight(height: NSObject?)
    func setCaptionWidthByBodyWidth(width: NSObject?)
    func readIt(arg0:NSObject?, arg1:NSObject?, arg2:NSObject?)
    func capturemouse()
    func fullScreen()
    func quitFullScreen()
    func reloadAlert()
    func printPage()
    var external: HtmlInteropClass?{get}
}


extension URL {
    
    var mimeType: String? {
        guard let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, pathExtension as NSString, nil)?.takeRetainedValue() else {
            return nil
        }
        
        return UTTypeCopyPreferredTagWithClass(uti, kUTTagClassMIMEType)?.takeRetainedValue() as String?
    }
    
}



class HtmlInteropClass:NSObject, HtmlInteropClassJSExports
{
    var external: HtmlInteropClass?
    var Data:Alert?
    var Owner:WindowCommand
    var IsFullscreen:Bool = false
    var TempPosition:String?
    init(data:Alert, owner: WindowCommand)
    {
        Owner = owner
        Data = data
        TempPosition = Data?.position
        super.init()
        external = self
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override class func webScriptName(for aSelector: Selector) -> String?
    {
        if aSelector == #selector(getProperty(arg0:))
        {
            return "getProperty"
        }
        
        if aSelector == #selector(getProperty(arg0:arg1:))
        {
            return "getProperty"
        }
        
        if aSelector == #selector(close(closewindow:id:))
        {
            return "close"
        }

        if aSelector == #selector(Close)
        {
            return "Close"
        }
        
        if aSelector == #selector(CloseTicker(id:includedtickers:))
        {
            return "closeTicker"
        }

        if aSelector == #selector(Exit)
        {
            return "Exit"
        }

        if aSelector == #selector(setProperty(property:value:))
        {
            return "setProperty"
        }

        if aSelector == #selector(callInCaption(script:arg1:arg2:arg3:))
        {
            return "callInCaption"
        }

        if aSelector == #selector(callInBody(script:arg1:arg2:arg3:))
        {
            return "callInBody"
        }

        if aSelector == #selector(autoclose(id:))
        {
            return "autoclose"
        }

        if aSelector == #selector(getBodyWidth)
        {
            return "getBodyWidth"
        }

        if aSelector == #selector(getBodyTop)
        {
            return "getBodyTop"
        }

        if aSelector == #selector(setBodyHeight(height:))
        {
            return "setBodyHeight"
        }

        if aSelector == #selector(getBodyHeight)
        {
            return "getBodyHeight"
        }

        if aSelector == #selector(format(datetimeformat:))
        {
            return "format"
        }

        if aSelector == #selector(startResizing)
        {
            return "startResizing"
        }

        if aSelector == #selector(queryURL(url:))
        {
            return "queryURL"
        }

        if aSelector == #selector(hide)
        {
            return "hide"
        }

        if aSelector == #selector(show)
        {
            return "show"
        }

        if aSelector == #selector(focus)
        {
            return "focus"
        }

        if aSelector == #selector(getFrameDocument)
        {
            return "getFrameDocument"
        }

        if aSelector == #selector(IsResizible)
        {
            return "IsResizible"
        }

        if aSelector == #selector(setCaptionHeightByBodyHeight(height:))
        {
            return "setCaptionHeightByBodyHeight"
        }

        if aSelector == #selector(setCaptionWidthByBodyWidth(width:))
        {
            return "setCaptionWidthByBodyWidth"
        }

        if aSelector == #selector(readIt(arg0:arg1:arg2:))
        {
            return "readIt"
        }

        if aSelector == #selector(capturemouse)
        {
            return "capturemouse"
        }

        if aSelector == #selector(fullScreen)
        {
            return "fullScreen"
        }

        if aSelector == #selector(quitFullScreen)
        {
            return "quitFullScreen"
        }

        if aSelector == #selector(reloadAlert)
        {
            return "reloadAlert"
        }
        
        if aSelector == #selector(printPage)
        {
            return "printPage"
        }

        return nil
    }
    
    override class func isSelectorExcluded(fromWebScript aSelector: Selector)
        -> Bool {
            if aSelector == #selector(getProperty(arg0:))
            {
                return false
            }

            if aSelector == #selector(getProperty(arg0:arg1:))
            {
                return false
            }

            if aSelector == #selector(close(closewindow:id:))
            {
                return false
            }

            if aSelector == #selector(Close)
            {
                return false
            }

            if aSelector == #selector(CloseTicker(id:includedtickers:))
            {
                return false
            }

            if aSelector == #selector(Exit)
            {
                return false
            }

            if aSelector == #selector(setProperty(property:value:))
            {
                return false
            }

            if aSelector == #selector(callInCaption(script:arg1:arg2:arg3:))
            {
                return false
            }

            if aSelector == #selector(callInBody(script:arg1:arg2:arg3:))
            {
                return false
            }

            if aSelector == #selector(autoclose(id:))
            {
                return false
            }

            if aSelector == #selector(getBodyWidth)
            {
                return false
            }

            if aSelector == #selector(getBodyTop)
            {
                return false
            }

            if aSelector == #selector(setBodyHeight(height:))
            {
                return false
            }

            if aSelector == #selector(getBodyHeight)
            {
                return false
            }

            if aSelector == #selector(format(datetimeformat:))
            {
                return false
            }

            if aSelector == #selector(startResizing)
            {
                return false
            }

            if aSelector == #selector(queryURL(url:))
            {
                return false
            }

            if aSelector == #selector(hide)
            {
                return false
            }

            if aSelector == #selector(show)
            {
                return false
            }

            if aSelector == #selector(focus)
            {
                return false
            }

            if aSelector == #selector(getFrameDocument)
            {
                return false
            }

            if aSelector == #selector(IsResizible)
            {
                return false
            }

            if aSelector == #selector(setCaptionHeightByBodyHeight(height:))
            {
                return false
            }

            if aSelector == #selector(setCaptionWidthByBodyWidth(width:))
            {
                return false
            }

            if aSelector == #selector(readIt(arg0:arg1:arg2:))
            {
                return false
            }

            if aSelector == #selector(capturemouse)
            {
                return false
            }

            if aSelector == #selector(fullScreen)
            {
                return false
            }

            if aSelector == #selector(quitFullScreen)
            {
                return false
            }

            if aSelector == #selector(reloadAlert)
            {
                return false
            }
            
            if aSelector == #selector(printPage)
            {
                return false
            }
            
            return true
    }
    
    
    func printPage(){
        
        let src = Owner.MainBrowser.stringByEvaluatingJavaScript(from: "document.documentElement.innerHTML") //or outter
        Owner.MainBrowser.stringByEvaluatingJavaScript(from: "document.open()")
        Owner.MainBrowser.stringByEvaluatingJavaScript(from: "document.write('<h1>\(Owner.alert!.title!)</h1>\(String(describing: src))')")
   
        Owner.MainBrowser.stringByEvaluatingJavaScript(from: """
document.body.style.overflow = 'hidden';
document.body.innerHTML = document.body.innerHTML.replace(/Optional\\("/g, '');
document.body.innerHTML = document.body.innerHTML.replace(/"\\)/g, '');
""")
            Owner.MainBrowser.printView(nil)
    }
    
    func Exit()
    {
        NSApplication.shared.terminate(self)
    }
    
    func getProperty(arg0: String?) -> String
    {
        return getProperty(arg0: arg0, arg1: "")
    }
    func getProperty(arg0: String?, arg1: String?) -> String
    {
        var property:String? = ""
        switch arg0 {
        case .some("id"):
            property = Data?.id ?? ""
            break;
        case .some("alert_header"):
            property = Config.shared.GetSettingsPropertyValue(id: "alert_header")
            break;
        case .some("minibrowser_header"):
            property = Config.shared.GetSettingsPropertyValue(id: "minibrowser_header")
            break;
        case .some("history_header"):
            property = Config.shared.GetSettingsPropertyValue(id: "history_header")
            break;
        case .some("options_header"):
            property = Config.shared.GetSettingsPropertyValue(id: "options_header")
            break;
        case .some("creation_text"):
            property = Config.shared.GetSettingsPropertyValue(id: "creation_text")
            break;
        case .some("alert_date_mode"):
            property = Config.shared.GetSettingsPropertyValue(id: "alert_date_mode")
            break;
        case .some("debug_mode"):
            property = Config.shared.GetSettingsPropertyValue(id: "debug_mode")
            break;
        case .some("readitButton"):
            property = Config.shared.GetSettingsPropertyValue(id: "readitButton")
            break;
        case .some("printButton"):
            property = Config.shared.GetSettingsPropertyValue(id: "printButton")
            break;
        case .some("readit_at_bottom"):
            property = Config.shared.GetSettingsPropertyValue(id: "readitButton")
            break;
        case .some("submitButton"):
            property = Config.shared.GetSettingsPropertyValue(id: "submitButton")
            break;
        case .some("postponeButton"):
            property = Config.shared.GetSettingsPropertyValue(id: "postponeButton")
            break;
        case .some("searchText"):
            property = Config.shared.GetSettingsPropertyValue(id: "searchText")
            break;
        case .some("update_expire"):
            property = Config.shared.GetSettingsPropertyValue(id: "update_expire")
            break;
        case .some("firstURL"):
            property = Config.shared.GetSettingsPropertyValue(id: "firstURL")
            break;
        case .some("send_text"):
            property = Config.shared.GetSettingsPropertyValue(id: "send_text")
            break;
        case .some("root_path"):
            property = Config.serviceFolder
            break;
        case .some("closeButton"):
            property = Config.shared.GetSettingsPropertyValue(id: "closeButton")
            break;
        case .some("saveButton"):
            property = Config.shared.GetSettingsPropertyValue(id: "saveButton")
            break;
        case .some("postpone"):
            property = Config.shared.GetSettingsPropertyValue(id: "postpone")
            break;
        case .some("postpone_mode"):
            property = Config.shared.GetSettingsPropertyValue(id: "postpone_mode")
            break;
            
        case .some("top_template"):
            break;
        case .some("bottom_template"):
            break;
            
        case .some("alertid"):
            property = (Data?.alert_id);
            break;
        case .some("acknowledgement"):
            if(!(Data?.acknowledged)!)
            {
                property = (Data?.acknown)
            }
            break;
        case .some("urgent"):
            property = (Data?.urgent);
            break;
        case .some("autoclose"):
            property = (Data?.autoclose);
            break;
        case .some("create_date"):
            property = (Data?.create_date);
            break;
        case .some("date"):
            let dateFormatter = DateFormatter()
            dateFormatter.locale = NSLocale.current
            dateFormatter.dateStyle = .medium
            dateFormatter.timeStyle = .medium
            property = dateFormatter.string(from: (Data?.recivetime)! as Date)
            break;
        case .some("title"):
            if let title = Data?.title {
                property = title
            } else {
                property = ""
            }
            break;
        case .some("skin_id"):
            property = Array(Data?.windows?.allObjects as! [AlertWindow]).first?.skin_id
            break;
        case .some("alert_auto_size"):
            property = "0";
            break;
        case .some("caption"):
            property = "";
            break;
        case .some("height"):
            property = (Data?.heigth);
            break;
        case .some("width"):
            property = (Data?.width);
            break;
        case .some("topmargin"):
            property = (Array(Data?.windows?.allObjects as! [AlertWindow]).first?.topmargin)!
            break;
        case .some("bottommargin"):
            property = (Array(Data?.windows?.allObjects as! [AlertWindow]).first?.bottommargin)
            break;
        case .some("leftmargin"):
            property = (Array(Data?.windows?.allObjects as! [AlertWindow]).first?.leftmargin)
            break;
        case .some("rightmargin"):
            property = (Array(Data?.windows?.allObjects as! [AlertWindow]).first?.rightmargin)
            break
        case .some("confirmation"):
            property = (Array(Data?.windows?.allObjects as! [AlertWindow]).first?.comfirmation)
            break;
        case .some("position"):
            property = (Data?.position)
            break;
        case .some("docked"):
            property = (Data?.docked)
            break;
       /* case .some("self_deletable"):
            property = (Data?.self_deletable)
            break;*/
        case .some("transparency"):
            property = (Array(Data?.windows?.allObjects as! [AlertWindow]).first?.transparency)
            break;
        case .some("transpcolor"):
            property = (Array(Data?.windows?.allObjects as! [AlertWindow]).first?.transp_color)
            break;
        case .some("visible"):
            property = (Data?.visible)
            break;
        case .some("single"):
            property = (Array(Data?.windows?.allObjects as! [AlertWindow]).first?.single)
            break;
        case .some("lockdesktop"):
            break;
        case .some("resizable"):
            property = (Data?.resizable);
            break;
        case .some("toptemplate"):
            break;
        case .some("bottomtemplate"):
            break;
        case .some("to_date"):
            property = (Data?.to_date);
            break;
        case .some("modmail"):
            break;
        case .some("modsms"):
            break;
        case .some("languagesURL"):
            property = Config.shared.GetSettingsPropertyValue(id: "languagesURL")
            break;
        case .some("modulesURL"):
            property = Config.shared.GetSettingsPropertyValue(id: "modulesURL")
            break;
        case .some("AlertMode"):
            property = ""
            break;
        case .some("user_name"):
            if let userName = Config.SessionUser?.user_name {
                property = userName
            } else if let userName = Config.SessionUser?.name {
                property = userName
            }
            break;
        case .some("md5_password"):
            property = (Config.SessionUser?.md5_password)
            break;
            
        case .some("email"):
            property = (Config.SessionUser?.email)
            break;
        case .some("phone"):
            property = (Config.SessionUser?.phone)
            break;
        case .some("changeUsernameUrl"):
            property = Config.shared.GetSettingsPropertyValue(id: "changeUsernameUrl")
            break;
        case .some("server"):
            property = ClientSettings.shared.settingsList.serverPath
            break;
        case .some("version.txt_version"):
            let txtversion = FileManager.default.fileExists(atPath: Config.serviceFolder + "version.txt")
            if (txtversion == true)
            {
                
                let fileversiontring =  NSString(data: FileManager.default.contents(atPath: Config.serviceFolder + "version.txt")!, encoding: String.Encoding.utf8.rawValue)! as String
                let unneeded = "DeskAlerts v";
                property = String(fileversiontring[fileversiontring.index(fileversiontring.startIndex, offsetBy: unneeded.length)..<fileversiontring.endIndex])
            }
            break;
        case .some("version"):
            property = ClientSettings.shared.settingsList.clientVersion
            break;
        case .some("adEnabled"):
            property = Config.shared.GetSettingsPropertyValue(id: "adEnabled")
            break
        case .some("synFreeAdEnabled"):
            property = Config.shared.GetSettingsPropertyValue(id: "synFreeAdEnabled")
            break
        case .some("fulldomain"):
            //
            // "HERE MUST BE IMPLEMENTATION FOR DOMAIN"
            break
            
        case .some("domain_name"):
            property = Config.shared.GetSettingsPropertyValue(id: "domain_name")
            break
            
        case .some("edir"):
            //todo her will edir return
            break;
            
        case .some("isSilent"):
            property = Config.shared.GetSettingsPropertyValue(id: "isSilent")
            break
        case .some("cont"):
            property = Config.shared.GetSettingsPropertyValue(id: "cont")
            break
        case .some("deskalerts_id"):
            property = (Config.SessionUser?.deskalerts_id)
            break;
            
        case .some("history_expire"):
            property = Config.shared.GetSettingsPropertyValue(id: "history_expire")
            break
            
        case .some("update_automatically"):
            property = Config.shared.GetSettingsPropertyValue(id: "update_automatically")
            break
        case .some("isHtml"):
            property = "1"
            break;
        case .some("js_datetime_format"):
            property = Config.shared.GetSettingsPropertyValue(id: "update_automatically")
            break
            
        case .some("play_sound"):
            property = (Config.SessionUser?.play_sound)
            break;
            
        case .some("customlocale"):
            property = "UTF-8";
            break;
            
        case .some("locale"):
            property = "UTF-8";
            break;
            
        case .some("indexes"):
            property = Owner.TickerIndexes;
            break;
            
        case .some("hide_close"):
            if (Data?.acknown=="1")
            {
                property = Int((Data?.autoclose)!)! >= 0 ? "1" : "0";
                
            }
            else
            {
                property = Int((Data?.autoclose)!)! > 0 ? "1" : "0";
            }
            break;
            
        case .some("option_auto_size"):
            property = Config.shared.GetSettingsPropertyValue(id: "option_auto_size")
            break
        case .some("connectionError"):
            property = Config.shared.GetSettingsPropertyValue(id: "connectionError")
            break
        default:
            property = ""
        }
        if (property == nil)
        {
            return ""
        }
        return property!
        
    }
    
    func sendRequestAndMarcAlertClosed(id:String)
    {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
            // Create Entity Description
            let entityDescription = NSEntityDescription.entity(forEntityName: "Alert", in: CoreDataManager.instance.managedObjectContext)
            // Configure Fetch Request
            fetchRequest.entity = entityDescription
            fetchRequest.predicate = NSPredicate(format: "alert_id == %@", (id))
            do {
                let result = try CoreDataManager.instance.managedObjectContext.fetch(fetchRequest)
                for alert in result
                {
                    let dbalert = alert as! Alert
                    if (dbalert.acknown=="1")
                    {
                        if let readitURL = Config.shared.GetSettingsPropertyValue(id: "readitURL"),
                           !readitURL.isEmpty {
                            let userName = Config.SessionUser?.user_name
                            let request = readitURL + "?alert_id=" + id + "&uname=" + userName!
                            _ = Alamofire.request(request).response(completionHandler: { (defaultDataResponse) in
                                print(defaultDataResponse)
                            })
                        }
                    }
                    dbalert.isclosed = true
                    dbalert.acknowledged = true
                }
                
                CoreDataManager.instance.saveContext()
            }
                
            catch {
                let fetchError = error as NSError
                print(fetchError)
            }

    }
    
    func Close()
    {
        return close(closewindow: nil, id: nil)
    }
    
    
    
    private func closeTickerStack(tickers: [Alert], with closedTikerID: String?) {
        var simpleAlerts = [Alert]()
        var acknowledgedAlerts = [Alert]()
        var supplementTikers = tickers
        
        for item in tickers {
            if item.haveAcknowledgeProperty {
                acknowledgedAlerts.append(item)
            } else {
                simpleAlerts.append(item)
            }
        }
        //ситуация когда закрываем акнолиджмент тикер
        if let closedTikerID = closedTikerID {
            supplementTikers.removeAll { (ticker) -> Bool in
                ticker.alert_id == closedTikerID
            }
            DatabaseManager.shared.processingAlertBeforeClosing(by: closedTikerID)
        } else {
            supplementTikers.removeAll { (alert) -> Bool in
                for item in simpleAlerts {
                    if alert.alert_id == item.alert_id {
                        return true
                    }
                }
                return false
            }
            DatabaseManager.shared.processingAlertsBeforeClosing(simpleAlerts)
        }
        
        if supplementTikers.count > 0 {
            if let baseTiker = supplementTikers.first {
                supplementTikers.removeFirst()
                Loader.sharedInstance.redisplayTikers(baseTiker: baseTiker, supplementTikers: supplementTikers)
            }
        }
    }
    

    func close(closewindow:NSObject?, id:NSObject?) {
        print(closewindow as Any)
        print(id as Any)
        print(Owner.alert?.alert_id)
        print(Owner.alert?.title)
        print(Owner.alert?.ticker_position)
        
        if let alert = Owner.alert {
            if alert.isTiker {
                var tickers = [Alert]()
                tickers.append(alert)
                if let includedTickerAlerts = Owner.includedTickerAlerts {
                    tickers.append(contentsOf: includedTickerAlerts)
                }
                if let id = id as? String {
                    closeTickerStack(tickers: tickers, with: id)
                } else {
                    closeTickerStack(tickers: tickers, with: nil)
                }
            } else {
                DatabaseManager.shared.processingAlertsBeforeClosing([alert])
            }
        }
        if closewindow != nil {
            let toclose = closewindow as? Bool
            if toclose == true || toclose == nil {
                self.Owner.Close()
                showAlertFromQueue()
            }
        }
        else {
            self.Owner.Close()
        }
        TrayMenuManager.shared.updateState()
    }
    
    private func showAlertFromQueue() {
        guard let queue = Config.WindowQueue else { return }
        if queue.count > 0 {
            guard let windowCommand = queue.first else { return }
            guard let alert = windowCommand.alert else { return }
            guard let href = windowCommand.alertHref else { return }
            Loader.sharedInstance.showAlertFromLocalHtml(alert: alert)
        }
    }
    
    @objc func CloseTicker(id:NSObject?, includedtickers:NSObject?)
    {
        let stringid = (id as? NSNumber)?.description
        close(closewindow: true as NSObject,id: id)
        if (includedtickers != nil)
        {
            let wsobject = includedtickers as? WebScriptObject
            if (wsobject != nil)
            {
                let jsval = wsobject?.jsValue()
                if (jsval != nil)
                {
                    let valuearray = jsval?.toArray()
                    if (valuearray != nil)
                    {
                        var included = valuearray as? [String]
                        if (included != nil)
                        {
                            
                            if ((stringid != nil)||(stringid != ""))
                            {
                                included =  included?.filter{$0 as String != stringid}
                                if (included != nil)
                                {
                                    if ((included?.count)! > 0)
                                    {
                                        for alertid in included!
                                        {
                                            let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
                                            // Create Entity Description
                                            let entityDescription = NSEntityDescription.entity(forEntityName: "Alert", in: CoreDataManager.instance.managedObjectContext)
                                            // Configure Fetch Request
                                            fetchRequest.entity = entityDescription
                                            fetchRequest.predicate = NSPredicate(format: "alert_id == %@", alertid)
                                            do {
                                                guard let alert  = try CoreDataManager.instance.managedObjectContext.fetch(fetchRequest).first as? Alert else { return }
                                                if !alert.isreaded {
                                                    Loader.sharedInstance.showAlertFromLocalHtml(alert: alert)
                                                }
                                            } catch {
                                                let fetchError = error as NSError
                                                print(fetchError)
                                            }
                                        }
                                        
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    func setProperty(property:NSObject?, value:NSObject?)
    {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        // Create Entity Description
        let entityDescription = NSEntityDescription.entity(forEntityName: "User", in: CoreDataManager.instance.managedObjectContext)
        // Configure Fetch Request
        fetchRequest.entity = entityDescription
        fetchRequest.predicate = NSPredicate(format: "name == %@", (Config.SessionUser?.name)!)
        
        guard let property = property as? String else { return }
        if property == "play_sound" {
            guard let value = value as? String else { return }
            SettingsManager.shared.audioEffectEnabled = value.boolValue
        }
        
        do {
            let user  = try CoreDataManager.instance.managedObjectContext.fetch(fetchRequest).first as! User
            let availiblekeys = user.entity.attributeKeys
            if (availiblekeys.contains(property))
            {
                user.setValue(value, forKey: property)
                CoreDataManager.instance.saveContext()
                Config.SessionUser = user
            }
            
        } catch {
            let fetchError = error as NSError
            print(fetchError)
        }
    }
    
    
    func callInCaption(script:NSObject?, arg1:NSObject?, arg2:NSObject?, arg3:NSObject?)
    {
        var scritpstring = script as? String
        if (scritpstring != nil)
        {
            scritpstring = scritpstring! + "("
            
            
            if (arg1 != nil)
            {
                let typedetect = arg1 as? String
                if (typedetect != nil)
                {
                    scritpstring = scritpstring! + "\"" + typedetect! + "\""
                }
                else
                {
                    let booltype = arg1 as? Bool
                    if (booltype != nil)
                    {
                        scritpstring = scritpstring! + (booltype?.description)!
                    }else{
                        scritpstring = scritpstring! + ", \"\(arg2!)\""
                    }
                }
            }
            if (arg2 != nil)
            {
                var typedetect = arg2 as? String
                if (typedetect != nil)
                {
                    if typedetect!.contains("printable_alert"){
                        typedetect = "true"
                    }
                    scritpstring = scritpstring! + " , \"" + typedetect! + "\""
                }
                else
                {
                    let booltype = arg2 as? Bool
                    if (booltype != nil)
                    {
                        scritpstring = scritpstring! + ", " + (booltype?.description)!
                    }
                    else{
                        scritpstring = scritpstring! + ", \"\(arg2!)\""
                    }
                }
                
            }
            if (arg3 != nil)
            {
                let typedetect = arg3 as? String
                if (typedetect != nil)
                {
                    scritpstring = scritpstring! + " , \"" + typedetect! + "\""
                }
                else
                {
                    let booltype = arg3 as? Bool
                    if (booltype != nil)
                    {
                        scritpstring = scritpstring! + ", " + (booltype?.description)!
                    }
                }
            }
            
            scritpstring = scritpstring! + ");"
            let _ = self.Owner.CaptionBrowser?.windowScriptObject.evaluateWebScript(scritpstring)
        }
        
        
    }
    
    func callInBody(script:NSObject?, arg1:NSObject?, arg2:NSObject?, arg3:NSObject?)
    {
        var scritpstring = script as? String
        
        if (scritpstring != nil)
        {
            scritpstring = scritpstring! + "("
            
            
            if (arg1 != nil)
            {
                let typedetect = arg1 as? String
                if (typedetect != nil)
                {
                    scritpstring = scritpstring! + "\"" + typedetect! + "\""
                }
                else
                {
                    let booltype = arg1 as? Bool
                    if (booltype != nil)
                    {
                        scritpstring = scritpstring! + (booltype?.description)!
                    }
                }
            }
            if (arg2 != nil)
            {
                let typedetect = arg2 as? String
                if (typedetect != nil)
                {
                    scritpstring = scritpstring! + " , \"" + typedetect! + "\""
                }
                else
                {
                    let booltype = arg1 as? Bool
                    if (booltype != nil)
                    {
                        scritpstring = scritpstring! + ", " + (booltype?.description)!
                    }
                }
                
            }
            if (arg3 != nil)
            {
                let typedetect = arg3 as? String
                if (typedetect != nil)
                {
                    scritpstring = scritpstring! + " , \"" + typedetect! + "\""
                }
                else
                {
                    let booltype = arg1 as? Bool
                    if (booltype != nil)
                    {
                        scritpstring = scritpstring! + ", " + (booltype?.description)!
                    }
                }
            }
			
            scritpstring = scritpstring! + ");"
            let _ = self.Owner.MainBrowser?.windowScriptObject.evaluateWebScript(scritpstring)
        }
    }
    
    
    func autoclose(id:NSObject?)
    {
        
    }
    
    func containsXhtml(input:String)->Bool
    {
        let html = SWXMLHash.parse("<wraper>"+input+"</wraper>");
        return html.all.count != 1
    }
    
    
    func convertXhtmlEntities(input:String) -> String
    {
        var output = input
        output = output.replacingOccurrences(of: "&amp;", with: "amp_token")
        output = output.replacingOccurrences(of: "&", with: "&amp;")
        output = output.replacingOccurrences(of: "amp_token", with: "&amp;")
        output = output.replacingOccurrences(of: "< ", with: "&lt; ")
        return output
    }
    
    
    
    func getBodyWidth()->String
    {
        let property = Int((Owner.MainBrowser?.frame.width)!)
        return property.description
        
    }
    
    func getBodyTop()->String
    {
        let property = Int(((Owner.MainBrowser?.frame.origin.y)!+(Owner.MainBrowser?.frame.height)!))
        return property.description
        
    }
    
    func getBodyHeight()->String
    {
        let property = Int((self.Owner.MainBrowser?.frame
            .height)!)
        return property.description
    }
    
    func IsResizible()->String
    {
        return (Data?.resizable)!
    }
    
    func format(datetimeformat:NSObject?) -> String {
        var result = ""
        
        result = Config.shared.GetSettingsPropertyValue(id: "datetime_format")!
        if (result != "")
        {
            if (result == "%s")
            {
                let timestamp = DateFormatter.localizedString(from: NSDate() as Date, dateStyle: .short, timeStyle: .short)
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = timestamp
                result = dateFormatter.string(from: Date(timeIntervalSince1970: TimeInterval(Double(datetimeformat as! String)!)))
            }
            else{
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = result
                result = dateFormatter.string(from: Date(timeIntervalSince1970: TimeInterval(Double(datetimeformat as! String)!)))
            }
            
        }
        else
        {
            let timestamp = DateFormatter.localizedString(from: NSDate() as Date, dateStyle: .short, timeStyle: .short)
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = timestamp
            result = dateFormatter.string(from: Date(timeIntervalSince1970: TimeInterval(Double(datetimeformat as! String)!)))
        }
        
        return result
    }
    
    func queryURL(url:NSObject)->String
    {
        var result = ""
        var uri = url as? String
        if (uri != nil)
        {
            if (uri?.contains("file:///"))!
            {
                uri = uri?.replacingOccurrences(of: "file:///", with: "")
            }
            if (FileManager.default.fileExists(atPath: uri!))
            {
                result =  (NSString(data: FileManager.default.contents(atPath: uri!)!, encoding: String.Encoding.utf8.rawValue) as String?)!
            }
            else
            {
                let response = Alamofire.request(uri!).validate().responseString(encoding: String.Encoding.utf8)
                if let responsestring = response.result.value {
                    result = responsestring
                }
            }
            
        }
        return result
    }
    
    
    func setBodyHeight(height:NSObject?)
    {
        if (height != nil)
        {
            var boddyheight = height as! Float
            boddyheight = boddyheight - 10
            DispatchQueue.main.async() {
                let currentheight = Float((self.Owner.MainBrowser?.frame.height)!)
                if (boddyheight <  currentheight)
                {
                    let newy = Float((self.Owner.MainBrowser?.frame.origin.y)!) + (currentheight - boddyheight) + 10
                    self.Owner.MainBrowser?.frame = NSRect.init(x: (self.Owner.MainBrowser?.frame.origin.x)!, y: CGFloat(newy), width: (self.Owner.MainBrowser?.frame.width)!, height: CGFloat(boddyheight))
                }
                else
                {
                    
                    let newy = Float((self.Owner.MainBrowser?.frame.origin.y)!) - boddyheight - currentheight + 10
                    self.Owner.MainBrowser?.frame = NSRect.init(x: (self.Owner.MainBrowser?.frame.origin.x)!, y: CGFloat(newy), width: (self.Owner.MainBrowser?.frame.width)!, height: CGFloat(boddyheight))
                }
            }
            
        }
    }
    
    
    
    
    
    
    func startResizing()
    {
        
    }
    
    
    
    func hide()
    {
        DispatchQueue.main.async{
            self.Owner.window?.orderOut(self)
        }
    }
    
    func show()
    {
        DispatchQueue.main.async{
            self.Owner.window?.makeKeyAndOrderFront(self)
        }
    }
    
    func focus()
    {
        DispatchQueue.main.async{
            self.Owner.window?.makeKeyAndOrderFront(self)
        }
    }
    
    func getFrameDocument()
    {
        
    }
    
    
    
    func setCaptionHeightByBodyHeight(height: NSObject?) {
        
        DispatchQueue.main.async{
            let bodyheight = height as? Double
            if (bodyheight != nil)
            {
                let window =  Array(self.Owner.alert?.windows?.allObjects as! [AlertWindow]).first
                window?.heigth = (bodyheight! + Double((window?.topmargin)!)! + Double((window?.bottommargin)!)!).description
                let newindows:NSSet? = NSSet.init(object: window!)
                self.Owner.alert?.windows = newindows
                self.Owner.initParentWindow(windowType: .baseContent)
            }
        }
    }
    
    func setCaptionWidthByBodyWidth(width: NSObject?)
    {
        DispatchQueue.main.async{
            let bodywidth = width as? Double
            if (bodywidth != nil)
            {
                let window =  Array(self.Owner.alert?.windows?.allObjects as! [AlertWindow]).first
                window?.width = (bodywidth! + Double((window?.leftmargin)!)! + Double((window?.rightmargin)!)!).description
                let newindows:NSSet? = NSSet.init(object: window!)
                self.Owner.alert?.windows = newindows
                self.Owner.initParentWindow(windowType: .baseContent)
            }
        }
        
    }
    
    
    func readIt(arg0:NSObject?, arg1:NSObject?, arg2:NSObject?)
    {
        guard let alert = self.Data else {
            self.close(closewindow: nil, id: nil)
            return
        }
        if alert.isTiker {
            CloseTicker(id: arg0, includedtickers: nil)
		}
        else {
            self.close(closewindow: nil, id: nil)
        }
    }
    
    
    func capturemouse()
    {
        self.Owner.captured = true
        self.Owner.CaptionBrowser.mouseDown(with: .init())
        
    }
    
    func fullScreen() {
        print(String(describing: self.Owner.alert?.position))
        self.Owner.alert?.position = "printPage" //fullscreen
        self.Owner.initParentWindow(windowType: .baseContent)
    }
    
    func quitFullScreen() {
        self.Owner.alert?.position = "right-bottom"
        self.Owner.initParentWindow(windowType: .baseContent)
    }
    
    func reloadAlert() {
    }
}

