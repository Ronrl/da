//
//  Config.swift
//  DeskAlerts
//
//  Created by Admin on 10.04.17.
//  Copyright © 2017 Toolbarstudio Inc. All rights reserved.
//

import Foundation
import Cocoa
import SINQ
import SWXMLHash
import Alamofire

extension String {
    var length: Int {
        return self.count
    }
    
    subscript (i: Int) -> String {
        return self[i ..< i + 1]
    }
    
    func substring(from: Int) -> String {
        return self[min(from, length) ..< length]
    }
    
    func substring(to: Int) -> String {
        return self[0 ..< max(0, to)]
    }
    
    subscript (r: Range<Int>) -> String {
        let range = Range(uncheckedBounds: (lower: max(0, min(length, r.lowerBound)),
                                            upper: min(length, max(0, r.upperBound))))
        let start = index(startIndex, offsetBy: range.lowerBound)
        let end = index(start, offsetBy: range.upperBound - range.lowerBound)
        return String(self[(start ..< end)])
    }
    
    func matches(for regex: String) -> [String] {
        
        do {
            
            
            let regex = try NSRegularExpression(pattern: regex, options: [.caseInsensitive] )
            let nsString = self as NSString
            let results = regex.matches(in: self, range: NSRange(location: 0 as Int, length: nsString.length))
            
            
            return results.map { nsString.substring(with: $0.range)}
        } catch let error {
            print("invalid regex: \(error.localizedDescription)")
            return []
        }
    }
    
    static func matches(for regex: String, in text: String) -> [String] {
        
        do {
            let regex = try NSRegularExpression(pattern: regex)
            let nsString = text as NSString
            let results = regex.matches(in: text, range: NSRange(location: 0, length: nsString.length))
            return results.map { nsString.substring(with: $0.range)}
        } catch let error {
            print("invalid regex: \(error.localizedDescription)")
            return []
        }
    }
    
    func indexOf(ofstring: String) -> Int? {
        guard let range: Range<String.Index> = self.range(of: ofstring) else { return nil }
        let index: Int = self.distance(from: self.startIndex, to: range.lowerBound)
        return index
    }
    
    mutating func insert(string:String,ind:Int) -> String {
        return  String(self.prefix(ind)) + string + String(self.suffix(self.count-ind))
    }
//    var dropLast: String {
//        return dropLast()
//    }
    
}
extension NSDate {
    func isGreaterThanDate(dateToCompare: NSDate) -> Bool {
        //Declare Variables
        var isGreater = false
        
        //Compare Values
        if self.compare(dateToCompare as Date) == ComparisonResult.orderedDescending {
            isGreater = true
        }
        
        //Return Result
        return isGreater
    }
    
    func isLessThanDate(dateToCompare: NSDate) -> Bool {
        //Declare Variables
        var isLess = false
        
        //Compare Values
        if self.compare(dateToCompare as Date) == ComparisonResult.orderedAscending {
            isLess = true
        }
        
        //Return Result
        return isLess
    }
    
    func equalToDate(dateToCompare: NSDate) -> Bool {
        //Declare Variables
        var isEqualTo = false
        
        //Compare Values
        if self.compare(dateToCompare as Date) == ComparisonResult.orderedSame {
            isEqualTo = true
        }
        
        //Return Result
        return isEqualTo
    }
    
    func addDays(daysToAdd: Int) -> NSDate {
        let secondsInDays: TimeInterval = Double(daysToAdd) * 60 * 60 * 24
        let dateWithDaysAdded: NSDate = self.addingTimeInterval(secondsInDays)
        
        //Return Result
        return dateWithDaysAdded
    }
    
    func addHours(hoursToAdd: Int) -> NSDate {
        let secondsInHours: TimeInterval = Double(hoursToAdd) * 60 * 60
        let dateWithHoursAdded: NSDate = self.addingTimeInterval(secondsInHours)
        
        //Return Result
        return dateWithHoursAdded
    }
}

extension NSWindow {
    func titlebarHeight() -> Double {
        let contentHeight = contentRect(forFrameRect: frame).height
        return Double(frame.height - contentHeight)
    }
}

extension NSImage {
    var jpgData: Data? {
        guard let tiffRepresentation = tiffRepresentation, let bitmapImage = NSBitmapImageRep(data: tiffRepresentation) else { return nil }
        return bitmapImage.representation(using: .jpeg, properties: [:])
    }
    func jpgWrite(to url: URL, options: Data.WritingOptions = .atomic) -> Bool {
        do {
            try jpgData?.write(to: url, options: options)
            return true
        } catch {
            print(error.localizedDescription)
            return false
        }
    }
}

class Config {
    
    static let shared = Config()
    
    static var XMLString:String?
    
    static var XML:XMLIndexer? = nil
    
    static var AlertsLoader:Loader?
    
    static var TaskbarIcon:String? = nil
        
    static var Computername:String?
    
    static var serviceFolder = Bundle.main.bundlePath + "/Contents/Resources/Resources/"
    
    static var WindowContainer:Array<WindowCommand>?
    
    static var WindowQueue:Array<WindowCommand>?
    
    static var SessionUser:User?
    
    static var WallpapersHash:String?
    
    static var WallpapersHashChanged:Bool?
    
    static var FirstTimeWallpaperChanging:Bool?
    
    static var FirstTimeScreensaverChanging:Bool?
    
    static var UserOldWallpaperPath:String?
    
    static var ScreensaversHash:String?
    
    static var ScreensaversHashChanged:Bool?
    
    static var UserOldScreensaverPath:String?
    
    static var IconIndex:Int? = 0
    
    static var FullDomain: String?
    
    static var isLockScreen:Bool = false
    
    func deletefromWindowContainer(index: Int)
    {
        Config.WindowContainer?.remove(at: index)
    }
    
    func ErrorLoadingConfig()
    {
        let myPopup: NSAlert = NSAlert()
        myPopup.messageText = "Default configuration file not loaded. DeskAlerts will be closed."
        myPopup.informativeText = "Error loading application."
        myPopup.alertStyle = .critical
        myPopup.addButton(withTitle: "Ok")
        let res = myPopup.runModal()
        if res == NSApplication.ModalResponse.alertFirstButtonReturn {
            NSApplication.shared.terminate(self)
        }
        NSApplication.shared.terminate(self)
    }
    
    
    
    func CreateConfig(content: String) -> XMLIndexer? {
        if let domainName = DomainManager.shared.domainName {
            Config.FullDomain = domainName
        } else {
            Config.FullDomain = ""
        }

        if let usersFromDataBase = DatabaseManager.shared.getUsersFromDatabase() {
            if usersFromDataBase.count > 1 {
                let firstUser = usersFromDataBase[0]
                DatabaseManager.shared.deleteAllUsersFromDatabase()
                DatabaseManager.shared.insertUserToDatabase(user: firstUser)
                Config.SessionUser = firstUser
                
            } else if usersFromDataBase.count == 0 {
                let user = User()
                user.name = NSUserName()
                DatabaseManager.shared.insertUserToDatabase(user: user)
                Config.SessionUser = user
            } else if usersFromDataBase.count == 1 {
                Config.SessionUser = usersFromDataBase[0]
            }
        }
        
        //MARK: -
        Config.WindowContainer = [WindowCommand]()
        Config.WindowQueue = [WindowCommand]()

        var trimstring = content
        Config.XML =  SWXMLHash.parse(trimstring)

        let menuiconspath = Bundle.main.path(forResource: "icons", ofType: "bmp", inDirectory: "Resources")
        

        Config.serviceFolder = (URL.init(string: menuiconspath!)?.deletingLastPathComponent().path)! + "/"
        trimstring = trimstring.replacingOccurrences(of: "%root_path%\\", with: Config.serviceFolder)
        trimstring = trimstring.replacingOccurrences(of: "%root_path%/", with: Config.serviceFolder)
        trimstring = trimstring.replacingOccurrences(of: "%server%", with: ClientSettings.shared.settingsList.serverPath)
        trimstring = trimstring.replacingOccurrences(of: "%COMPUTERNAME%", with: AuthorizationManager.shared.computerName)
        
        guard let cont = Config.shared.GetSettingsPropertyValue(id: "cont") else {
            DebugLogger.showAlert(with: "")
            return nil
        }
        trimstring = trimstring.replacingOccurrences(of: "%cont%", with: cont)
        
        Config.XMLString = setDefaultParameters(trimstring: trimstring)
        Config.XML = SWXMLHash.parse(setDefaultParameters(trimstring: trimstring))

        let task = Process()
        task.launchPath = "/bin/sh"
        task.arguments = ["-c", "defaults -currentHost read com.apple.screensaver"]
        let pipe = Pipe()
        task.standardOutput = pipe
        task.standardError = pipe
        task.launch()
        task.waitUntilExit()

        return Config.XML
    }
    
    func setDefaultParameters(trimstring: String) -> String {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        let entityDescription = NSEntityDescription.entity(forEntityName: "User", in: CoreDataManager.instance.managedObjectContext)
        fetchRequest.entity = entityDescription
        guard let userName = Config.SessionUser?.name else {
            DebugLogger.showAlert(with: "Error: invalid user name from database")
            return ""
        }
        fetchRequest.predicate = NSPredicate(format: "name == %@", userName)
        do {
            if let user = try CoreDataManager.instance.managedObjectContext.fetch(fetchRequest).first as? User {
                if user.deskalerts_id == nil {
                    user.setValue(NSUUID().uuidString, forKey: "deskalerts_id")
                }
                if user.unreadedmessagesstate == nil {
                    user.setValue("off", forKey: "unreadedmessagesstate")
                }
                user.usersession_id = NSUUID().uuidString
                if user.debug_mode == nil, let debugMode = GetSettingsPropertyValue(id: "debug_mode") {
                    user.setValue(debugMode, forKey: "debug_mode")
                }
                if user.disable == nil, let disable = GetSettingsPropertyValue(id: "disable") {
                    user.setValue(disable, forKey: "disable")
                }
                if user.normal_expire == nil, let normalExpire = GetSettingsPropertyValue(id: "normal_expire"){
                    user.setValue(normalExpire, forKey: "normal_expire")
                }
                if user.play_sound == nil, let playSound = GetSettingsPropertyValue(id: "play_sound") {
                    user.setValue(playSound, forKey: "play_sound")
                }
                Config.SessionUser = user
                CoreDataManager.instance.saveContext()
                var resultString = trimstring
                for attribute in user.entity.attributesByName {
                    if let token = user.value(forKey: attribute.key) as? String,
                        attribute.key != "user_name" {
                        resultString = resultString.replacingOccurrences(of: "%"+attribute.key+"%", with: token)
                    }
                }
                return resultString
            }
        } catch {
            let fetchError = error as NSError
            print(fetchError)
            return String.emptyString
        }
        return String.emptyString
    }
    
    func GetSettingsPropertyValue(id: String) -> String? {
        do {
            let text = try Config.XML?["ALERTS"]["SETTINGS"]["PROPERTY"].withAttribute("id", id).element?.attribute(by: "default")?.text
            return text

        } catch {
            DebugLogger.showAlert(with: "Error: property '\(id)' not found in config file")
            return nil
        }
    }
    
    func GetAlertFromXml(alert:XMLIndexer, context: NSManagedObjectContext?) -> Alert? {
        guard let id = alert.element?.attribute(by: "alert_id")?.text else { return nil }
        if !DatabaseManager.shared.alertIsReceived(by: id),
        let context = context,
        let result = NSEntityDescription.insertNewObject(forEntityName: "Alert", into: context) as? Alert {
            result.acknown = alert.element?.attribute(by: "acknown")?.text
            result.alert_id = alert.element?.attribute(by: "alert_id")?.text
            result.autoclose = alert.element?.attribute(by: "autoclose")?.text
            result.class_id = alert.element?.attribute(by: "class_id")?.text
            result.create_date = alert.element?.attribute(by: "create_date")?.text
            result.docked = alert.element?.attribute(by: "docked")?.text
            result.expire = alert.element?.attribute(by: "expire")?.text
            result.filename = alert.element?.attribute(by: "filename")?.text
            result.heigth = alert.element?.attribute(by: "height")?.text
            result.history = alert.element?.attribute(by: "history")?.text
            result.href = alert.element?.attribute(by: "href")?.text
            result.id = alert.element?.attribute(by: "id")?.text
            result.name = alert.element?.attribute(by: "name")?.text
            result.plugin = alert.element?.attribute(by: "plugin")?.text
            result.position = alert.element?.attribute(by: "position")?.text
            result.priority = alert.element?.attribute(by: "priority")?.text
            result.resizable = alert.element?.attribute(by: "resizable")?.text
            result.schedule = alert.element?.attribute(by: "schedule")?.text
            result.self_deletable = alert.element?.attribute(by: "self_deletable")?.text
            result.ticker = alert.element?.attribute(by: "ticker")?.text
            result.ticker_position = alert.element?.attribute(by: "ticker_position")?.text
            result.title = alert.element?.attribute(by: "title")?.text
            result.to_date = alert.element?.attribute(by: "to_date")?.text
            result.urgent = alert.element?.attribute(by: "urgent")?.text
            result.user_id = alert.element?.attribute(by: "user_id")?.text
            result.utc = alert.element?.attribute(by: "utc")?.text
            result.visible = alert.element?.attribute(by: "visible")?.text
            result.width = alert.element?.attribute(by: "width")?.text
            result.survey = alert.element?.attribute(by: "survey")?.text
            result.hide_close = alert.element?.attribute(by: "hide_close")?.text
            let xmlwindow = alert["WINDOW"].element
            if xmlwindow != nil {
                let window = GetAlertWindowFromXml(xml: alert["WINDOW"], context: result.managedObjectContext, alert: result)
                if (window.captionhref != nil)
                {
                    result.addToWindows(window)
                }
            }
            return result
        } else {
            return nil
        }
    }
    
    func GetAlertWindowFromXml(xml: XMLIndexer, context: NSManagedObjectContext?, alert: Alert)->AlertWindow
    {
        var window = AlertWindow()
        if (context != nil)
        {
            window = NSEntityDescription.insertNewObject(forEntityName: "AlertWindow", into: context!) as! AlertWindow
        }
        window.bottommargin = xml.element?.attribute(by: "bottommargin")?.text
        if (xml.element?.attribute(by: "captionhref")?.text != nil)
        {
            if (!(xml.element?.attribute(by: "captionhref")?.text.contains(Config.serviceFolder))!)
            {
                var xmlstring = (xml.element?.attribute(by: "captionhref")?.text)!
                xmlstring = Config.serviceFolder + xmlstring
                
                window.captionhref = xmlstring
            }
            else
            {
                window.captionhref = xml.element?.attribute(by: "captionhref")?.text
            }
        }
        window.comfirmation = xml.element?.attribute(by: "confirmation")?.text
        if (xml.element?.attribute(by: "customjs")?.text != nil)
        {
            if (!(xml.element?.attribute(by: "customjs")?.text.contains(Config.serviceFolder))!)
            {
                window.customjs = Config.serviceFolder + (xml.element?.attribute(by: "customjs")?.text)!
            }
            else
            {
                window.customjs = xml.element?.attribute(by: "customjs")?.text
            }
        }
        window.heigth = xml.element?.attribute(by: "height")?.text
        window.leftmargin = xml.element?.attribute(by: "leftmargin")?.text
        window.name = xml.element?.attribute(by: "name")?.text
        window.position = xml.element?.attribute(by: "position")?.text
        window.rightmargin = xml.element?.attribute(by: "rightmargin")?.text
        window.single = xml.element?.attribute(by: "single")?.text
        window.skin_id = xml.element?.attribute(by: "skin_id")?.text
        window.topmargin = xml.element?.attribute(by: "topmargin")?.text
        window.visible = xml.element?.attribute(by: "visible")?.text
        window.width = xml.element?.attribute(by: "width")?.text
        window.transparency = xml.element?.attribute(by: "transparency")?.text
        window.transp_color = xml.element?.attribute(by: "transp_color")?.text
        window.alerts = alert
        return window
    }
    
    func dialogOKCancel(question: String, text: String) -> Bool {
        let alert = NSAlert()
        alert.messageText = question
        alert.informativeText = text
        alert.alertStyle = .warning
        alert.addButton(withTitle: String.localized(key: "DialogOK"))
        alert.addButton(withTitle: String.localized(key: "DialogCancel"))
        return alert.runModal() == .alertFirstButtonReturn
    }
}

