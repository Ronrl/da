import Cocoa
import Foundation
import CoreData

private struct Constants {
    static let iconAlertFileNamePrefix = "icon_"
    static let iconAlertFileType = "png"
    static let iconAlertFileDirectory = "Resources"
    static let unreadAlertFont = NSFont.boldSystemFont(ofSize: 13)
}

class DataSource: NSObject {
    let context: NSManagedObjectContext
    
    init(context: NSManagedObjectContext) {
        self.context = context
        super.init()
    }
    
    lazy var arrayController: NSArrayController = {
        let arrayController = NSArrayController()
        arrayController.managedObjectContext = self.context
        arrayController.entityName = "Alert"
        arrayController.automaticallyPreparesContent = false
        arrayController.fetchPredicate = NSPredicate(format: "%K == %@ OR %K == %@ AND %K == %@", argumentArray: ["self_deletable", "0", "self_deletable", "1", "isreaded", false])
        arrayController.sortDescriptors = [NSSortDescriptor(key: "recivetime", ascending: false)]
        
        return arrayController
    }()
}

extension WindowCommand: NSTableViewDelegate, NSTableViewDataSource
{
    fileprivate func detectMnemonicsInToTitle(_ objects: [Alert]?, _ row: Int) {
        if objects![row].title!.contains("&nbsp;"){
            objects![row].history_title = objects![row].title?.replacingOccurrences(of: "&nbsp;", with: "", options: .regularExpression, range: nil)
        }
    }
    
    func tableView(_ tableView: NSTableView, objectValueFor tableColumn: NSTableColumn?, row: Int) -> Any? {
        tableColumn?.isEditable = false
        
        guard let alerts = datasource.arrayController.arrangedObjects as? [Alert] else { return nil }
        let alert = alerts[row]
        
        if tableColumn == tableView.tableColumns[0] {
            if alerts[row].history_title == nil
            {
                alerts[row].history_title = alerts[row].title?.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
            }
            detectMnemonicsInToTitle(alerts, row)
            
            guard let alertType = alerts[row].id else { return nil }
            guard let icon = getAlertIcon(by: alertType) else { return nil }
            return icon
        }
        
        if tableColumn == tableView.tableColumns[1] {
            guard let historyTitle = alert.history_title else { return "" }
            if alert.isreaded {
                return historyTitle
            } else {
                return NSAttributedString(text: historyTitle, font: Constants.unreadAlertFont, textColor: NSColor.black)
            }
        }
        return nil
        
    }
    
    private func getAlertIcon(by alertType: String) -> NSImage? {
        let iconFileName = Constants.iconAlertFileNamePrefix + alertType
        
        guard let filePath = Bundle.main.path(forResource: iconFileName,
                                              ofType: Constants.iconAlertFileType,
                                              inDirectory: Constants.iconAlertFileDirectory) else { return nil }
        
        guard let contentData = FileManager.default.contents(atPath: filePath) else { return nil }
        
        return NSImage.init(data: contentData)
    }
    
  
    
    func tableView(_ tableView: NSTableView, dataCellFor tableColumn: NSTableColumn?, row: Int) -> NSCell? {
        if tableColumn == tableView.tableColumns[3]
        {
            let filePath = Bundle.main.path(forResource: "icon_delete", ofType: "png", inDirectory: "Resources")
            
            if (((filePath) != nil)&&(!(filePath?.isEmpty)!))
            {
                let contentData = FileManager.default.contents(atPath: filePath!)
                let buttonimage =  NSImage.init(data: contentData!)
                return NSButtonCell.init(imageCell: buttonimage)
                
            }
            //HistoryDeleteButton.init(row: row, ds: datasource, tv: tableView)
        }
        return nil
    }
    
    
    
    func numberOfRows(in tableView: NSTableView) -> Int {
        let objects = datasource.arrayController.arrangedObjects as? [Alert]
        if (objects != nil)
        {
            return objects!.count
        }
        else
        {
            return 0
        }
    }
    
    func UpdateHistory(request: NSFetchRequest<NSFetchRequestResult>?) {
        HistoryTitle.bind(.value, to: datasource.arrayController, withKeyPath: "arrangedObjects.history_title", options: nil)
        HistoryReciveTime.bind(.value, to: datasource.arrayController, withKeyPath: "arrangedObjects.recivetime", options: nil)
        do {
            try datasource.arrayController.fetch(with: request, merge: false)
        } catch {
            
        }
    }
}
