//
//  Received.swift
//  DeskAlerts
//
//  Created by Admin on 07/05/2019.
//  Copyright © 2019 Toolbarstudio Inc. All rights reserved.
//
import Foundation
import Alamofire
class Received: RequestBase{
    private let jac = JsonApiClient()
    private var _wasReceived: [String:Any]!
    var wasReceived: [String:Any] {
        get{
            if _wasReceived == nil{
                _wasReceived = ["alerts": [], "wallpapers": [], "screensavers": []]
            }
            return _wasReceived
        }
        set{ _wasReceived = newValue }
    }
    override func getUrl() -> String {
        let urlReceived = "\(jac.CURRENT_SERVER_URL)\(jac.POSTRECEIVED)"
        return urlReceived
    }
    override func getBody() -> [String : Any] {
        let body = wasReceived
        return body
    }
}
