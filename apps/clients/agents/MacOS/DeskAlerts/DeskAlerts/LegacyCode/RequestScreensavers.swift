//
//  RequestScreensavers.swift
//  DeskAlerts
//
//  Created by Admin on 24/04/2019.
//  Copyright © 2019 Toolbarstudio Inc. All rights reserved.
//
import Alamofire
import CryptoSwift
import SWXMLHash
class RequestScreensavers: RequestBase {
    private var _screensaverId: String?
    var screensaverId: String{
        get{
            if _screensaverId == nil{
                _screensaverId = ""
            }
            return _screensaverId!
        }
        set{
            _screensaverId = newValue
        }
    }
    
    private let jac = JsonApiClient()
    private var _localTime: String!
    private var localTime: String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy|HH:mm:ss"
        let dateInFormat = dateFormatter.string(from: NSDate() as Date)
        _localTime = dateInFormat
        
        return _localTime
    }
    override func getUrl() -> String {
        let urlScreensavers = "\(jac.CURRENT_SERVER_URL)\(jac.POSTSCREENSAVERS)"
        return urlScreensavers
    }
    override func getBody() -> [String : Any] {
        let body = ["localTime":"\(localTime)"]
        return body
    }
    override func handlerRequest(resp: DataResponse<Any>) -> String {
        let result = resp.result
        if let dict = result.value as? [String : Any]{
            if let stat = dict["status"] as? String {
                let hash = Config.ScreensaversHash
                let heshContent = "<TOOLBAR><SCREENSAVERS hash=\"\(hash!)\"/></TOOLBAR>"
                let parseResult = SWXMLHash.parse(heshContent)
                Loader.sharedInstance.LoadAlert(toolbar: parseResult)
                self.jac.statusContent = stat
            }
        }
        return self.jac.statusContent
    }
}
