//
//  Read.swift
//  DeskAlerts
//
//  Created by Admin on 17/05/2019.
//  Copyright © 2019 Toolbarstudio Inc. All rights reserved.
//
import Alamofire
class Read: RequestBase{
    private var _acknowAlertId: String!
    var acknowAlertId: String{
        get{
            if _acknowAlertId == nil{
                _acknowAlertId = ""
            }
            return _acknowAlertId
        }set{
            _acknowAlertId = newValue
        }
    }
    let jac = JsonApiClient()
    
    override func getUrl() -> String {
        let urlRead = "\(jac.CURRENT_SERVER_URL)\(jac.POSTREAD)"
        return urlRead
    }
    override func getBody() -> [String : Any] {
        let body = ["alertId":acknowAlertId]
        return body
    }
}
