//
//  RequestBase.swift
//  DeskAlerts
//
//  Created by Admin on 25/03/2019.
//  Copyright © 2019 Toolbarstudio Inc. All rights reserved.
//

import Foundation
import Alamofire

class RequestBase {
    var alertBody: String = ""
    var alertId: String = ""
    func getUrl() -> String{ return "empty URL" }
    func getBody() -> [String:Any]{ return ["empty body":0] }
    func handlerRequest(resp: DataResponse<Any>) -> String { return "" }
}
