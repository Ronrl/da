//
//  WindowCommand.swift
//  DeskAlerts
//
//  Created by mihail on 26/04/2017.
//  Copyright © 2017 Toolbarstudio Inc. All rights reserved.
//

import Foundation
import Cocoa
import SINQ
import WebKit
import Alamofire

enum WindowType {
    case baseContent
    case history
    case options
    case authorizationForm
}

enum AlertPriority: String {
    case `default` = "0"
    case high = "1"
}

enum HistoryColumnType: Int {
    case contentTypeIcon = 0
    case title = 1
    case receivedTime = 2
    case delete = 3
}


private struct Constants {
    static let indentBetweenWindows: CGFloat = 20
    static let maxNumberOfWindowsOnScreen: Int = 9
    static let tableTitleBarIndex: Int = -1
}



extension NSCoder {
    class func empty() -> NSCoder {
        let data = NSMutableData()
        let archiver = NSKeyedArchiver(forWritingWith: data)
        archiver.finishEncoding()
        return NSKeyedUnarchiver(forReadingWith: data as Data)
    }
}




@IBDesignable
class WindowCommand: NSViewController, WebFrameLoadDelegate, WebUIDelegate, WebEditingDelegate, WebPolicyDelegate, WebDownloadDelegate, WebResourceLoadDelegate, NSWindowDelegate
{
    
    @IBOutlet weak var HistoryTableView:NSScrollView!
    @IBOutlet weak var CaptionBrowser:CustomView!
    @IBOutlet var topView: NSView!
    @IBOutlet weak var MainBrowser: CustomView!
    @IBOutlet weak var HistoryType: NSTableColumn!
    @IBOutlet weak var HistoryTitle: NSTableColumn!
    @IBOutlet weak var HistoryReciveTime: NSTableColumn!
    @IBOutlet weak var HistoryDelete: NSTableColumn!
    @IBOutlet weak var HistoryTable: NSTableView!
    @IBOutlet weak var ClearHistory: NSButton!
    @IBOutlet weak var SearchFiled: NSTextField!
    @IBOutlet weak var SearchButton: NSButton!
    
    
    var coder: NSCoder?
    var window: NSWindow?
    var allowLoad: Int = 0
    var captured:Bool?
    var parentWindowParams:AlertWindow?
    var alertHref:String?
    var isLocal:Bool?
    var alert:Alert?
    var includedTickerAlerts:Array<Alert>?
    var includedTickerAlertsContent:Array<String>?
    var webView:WebView?
    var isFullscreen:Bool = false
    var tempPosition:String?
    var isHistory:Bool = false
    var isHiden:Bool = false

    var alertRect = CGRect.zero

    var currentEvent: NSEvent?
    let datasource = DataSource(context: CoreDataManager.instance.managedObjectContext)
    var currentSize:NSSize?
    var notification: NSUserNotification!
    var canMoveWindow: Int?
    
    public var TickerIndexes:String{
        
        if (alertHref != nil)
        {
            
            let count:Int = (alertHref!.matches(for: "<!-- separator -->").count)
            if (count>=0)
            {
                var indexes=""
                for i in 0..<count
                {
                    indexes += (String(i) + ",");
                }
                
                if indexes.length > 0
                {
                    indexes.removeLast()
                }
                else
                {
                    indexes = "0";
                }

                
                return indexes;
            }
            else
            {
                return "0"
            }
        }
        else
        {
            return "0"
        }
    }
    //MARK: - History Actions
    @IBAction func ClearHistoryButton(_ sender: Any) {
        let answer = Config.shared.dialogOKCancel(question: String.localized(key: "ClearHistory"), text: "")
        if answer == true {
            DatabaseManager.shared.deleteAllAlerts()
        }
    }
    
    @IBAction func HistorySearch(_ sender: Any) {
        let searchString = SearchFiled.stringValue
        if searchString.isEmpty {
            UpdateHistory(request: nil)
        } else {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
            let entityDescription = NSEntityDescription.entity(forEntityName: "Alert", in: CoreDataManager.instance.managedObjectContext)
            fetchRequest.entity = entityDescription
            fetchRequest.predicate = NSPredicate(format: "%K CONTAINS[c] %@", "title", searchString)
            UpdateHistory(request: fetchRequest)
        }
    }
    
    
    @IBAction func HistoryTableClicked(_ sender: Any) {
        guard let table = sender as? NSTableView else { return }
        if table.clickedRow != Constants.tableTitleBarIndex {
            let clickedColumnType = HistoryColumnType(rawValue: table.clickedColumn)
            switch clickedColumnType {
            case .contentTypeIcon?, .title?, .receivedTime?: showWindow(clickedRow: table.clickedRow)
                case .delete?: showDeleteAlertDialog(clickedRow: table.clickedRow)
                default: break
            }
        }
    }
    
    private func showWindow(clickedRow: Int) {
        guard let alerts = datasource.arrayController.arrangedObjects as? [Alert] else { return }
        let alert = alerts[clickedRow]
        guard let alertType = alert.id else { return }
        
        switch alertType {
            case "wallpaper": showWallpaperPreview(wallpaper: alert)
            case "screensaver": break
            default: Loader.sharedInstance.showAlertFromLocalHtml(alert: alert)
        }

    }
    private func showWallpaperPreview(wallpaper: Alert) {
        do {
            let wallpaperPreviewWindow = try Config.shared.GetAlertWindowFromXml(xml: (Config.XML?["ALERTS"]["VIEWS"]["WINDOW"].withAttribute("name","alertwindow1"))!, context: datasource.context, alert: wallpaper)
            wallpaperPreviewWindow.position = "fullscreen"
            wallpaper.acknown = "0"
            wallpaper.autoclose = "0"
            wallpaper.windows = nil
            wallpaper.addToWindows(wallpaperPreviewWindow)
            Loader.sharedInstance.showAlertFromLocalHtml(alert: wallpaper)
        } catch {
            print(error.localizedDescription)
        }
    }
        
    
    private func showDeleteAlertDialog(clickedRow: Int) {
        let answer = Config.shared.dialogOKCancel(question: String.localized(key: "DeleteAlert"), text: "")
            if answer == true {
                guard let alerts = self.datasource.arrayController.arrangedObjects as? [Alert] else { return }
                let deketedAlert = alerts[clickedRow]
                let window = Config.WindowContainer?.first(where: {$0.alert == deketedAlert})
                window?.CaptionBrowser?.windowScriptObject.evaluateWebScript("window.external.close();")
                DatabaseManager.shared.remove(deketedAlert)
                TrayMenuManager.shared.updateState()
                WallpaperManager.shared.updateWallpaperList()
            }
    }
    //MARK: -
    public func Show(alert: Alert, href: String?, islocal: Bool?, includedtickeralerts:Array<Alert>?, includedtickeralertscontent:Array<String>?, ishiden:Bool = false, windowType: WindowType) {
        self.coder = NSCoder.empty()
        self.isLocal = islocal
        self.alert = alert
        self.tempPosition = self.alert?.position
        self.alertHref = href
        self.includedTickerAlerts = includedtickeralerts
        self.includedTickerAlertsContent = includedtickeralertscontent
        self.isHistory = windowType == .history
        self.isHiden = ishiden
        self.window = NSWindow.init(contentViewController: self)
        self.window?.isOpaque = false
        self.window?.backgroundColor = NSColor.clear
        self.window?.hasShadow = true
        self.window?.isReleasedWhenClosed = true
        self.window?.acceptsMouseMovedEvents = true
        self.window?.allowsConcurrentViewDrawing = true
        self.window?.ignoresMouseEvents = false
        self.window?.backingType = NSWindow.BackingStoreType.buffered
        self.window?.alphaValue = 0
        self.window?.level = NSWindow.Level(rawValue: Int(CGWindowLevelForKey(.maximumWindow)))
        self.window?.isMovableByWindowBackground = true
        self.window?.titleVisibility = .hidden
        self.window?.titlebarAppearsTransparent = true
        self.window?.delegate = self
        
        if alert.isResizable {
            self.window?.styleMask = [.fullSizeContentView, .titled, .resizable]
        } else {
            if (alert.priority != nil && alert.recivetime != nil){
                self.window?.styleMask = [.fullSizeContentView, .titled, .fullScreen]
                self.window?.isMovable = false
                self.window?.isMovableByWindowBackground = false
            } else {
                  self.window?.styleMask = [.fullSizeContentView, .titled]
            }
        }
        
        if alert.position == "fullscreen" {
            self.window?.isMovable = false
            self.window?.isMovableByWindowBackground = false
            self.window?.styleMask = [.fullSizeContentView, .titled, .fullScreen]
        }
        
        self.window?.standardWindowButton(.closeButton)?.isHidden = true
        self.window?.standardWindowButton(.documentIconButton)?.isHidden = true
        self.window?.standardWindowButton(.documentVersionsButton)?.isHidden = true
        self.window?.standardWindowButton(.fullScreenButton)?.isHidden = true
        self.window?.standardWindowButton(.miniaturizeButton)?.isHidden = true
        self.window?.standardWindowButton(.toolbarButton)?.isHidden = true
        self.window?.standardWindowButton(.zoomButton)?.isHidden = true
        self.window?.makeKeyAndOrderFront(self)
        let controller = NSWindowController(window: self.window)
        controller.showWindow(self)

        if Config.isLockScreen {
            if alert.isHighPriorityAlert {
                    notification = NSUserNotification()
                    notification.title = Config.shared.GetSettingsPropertyValue(id: "alert_header")
                    notification.informativeText = alert.title
                    notification.soundName = NSUserNotificationDefaultSoundName
                    NSUserNotificationCenter.default.deliver(notification)
                    Config.WindowQueue?.append(self)
                    Close()
                    return
                } else {
                    Config.WindowQueue?.append(self)
                    Close()
                    return
                }
        } else {
            let results2 = from(Config.WindowQueue!).whereTrue{($0.alert?.alert_id == self.alert?.alert_id)}.toArray()
            for result in results2
            {
                let ind = (Config.WindowQueue?.index(of: result))
                if (ind != nil)
                {
                    Config.WindowQueue?.remove(at: ind!)
                }
            }
        }
        switch windowType {
        case .baseContent:
            if let windowContainer = Config.WindowContainer,
                windowContainer.count > Constants.maxNumberOfWindowsOnScreen {
                if alert.isHighPriorityAlert {
                    Config.WindowQueue?.append((Config.WindowContainer?[(Config.WindowContainer?.count)! - 1])!)
                    Config.WindowContainer?[(Config.WindowContainer?.count)! - 1].Close()
                    initParentWindow(windowType: windowType)
                    DatabaseManager.shared.setReadedState(from: alert)
                } else {
                    Config.WindowQueue?.append(self)
                    Close()
                }
            } else {
                initParentWindow(windowType: windowType)
                DatabaseManager.shared.setReadedState(from: alert)
            }
        case .history, .options, .authorizationForm:
            initParentWindow(windowType: windowType)
        }
    }
    
    func window(_ window: NSWindow, shouldDragDocumentWith event: NSEvent, from dragImageLocation: NSPoint, with pasteboard: NSPasteboard) -> Bool {
        return true
    }
    
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    override func viewDidAppear() {
        SearchFiled.isEditable = true
        SearchFiled.isEnabled = true
        SearchFiled.translatesAutoresizingMaskIntoConstraints = true
       
        allowLoad = 0
        
    }
    
    func initParentWindow(windowType: WindowType) {
        //TODO: какаято непонятная дичь!
        guard let alert = alert else {
            window?.close()
            if let windowContainer = Config.WindowContainer {
                if let index = windowContainer.index(of: self) {
                    Config.shared.deletefromWindowContainer(index: index)
                }
            }
            return
        }
    
        if let windows = alert.windows, windows.count == 0 {
            CoreDataManager.instance.managedObjectContext.delete(alert)
            CoreDataManager.instance.saveContext()
            
            window?.close()
            if let windowContainer = Config.WindowContainer {
                if let index = windowContainer.index(of: self) {
                    Config.shared.deletefromWindowContainer(index: index)
                }
            }
            return
        }
        
        //================
        guard let mainScreen = NSScreen.screens.first else { return }
        let screenRectangle = mainScreen.frame
        let alertWidthString = alert.width ?? "100"
        let alertHeigthString = alert.heigth ?? "100"
        let alertWidth = NSString(string: alertWidthString).doubleValue
        let alertHeigth = NSString(string: alertHeigthString).doubleValue
        //================resize fix===========
        if(CGFloat(alertWidth) > screenRectangle.width || CGFloat(alertHeigth) > screenRectangle.height) {
            alert.position = "fullscreen"
        }
        //=======================
        parentWindowParams = Array(alert.windows?.allObjects as! [AlertWindow]).first
        switch windowType {
            case .baseContent:
                if alert.id == "alert" {
                    if alert.ticker == "1" {
                        alertRect.origin.x = 0.0
                        alertRect.size.width = screenRectangle.width
                        alertRect.size.height = CGFloat(alertHeigth)
                        switch alert.ticker_position{
                        case "top":
                            alertRect.origin.y = screenRectangle.height
                        case "bottom":
                            alertRect.origin.y = CGFloat(-alertHeigth)
                        case "middle":
                            alertRect.origin.y = CGFloat(screenRectangle.height / 2.0) - CGFloat(alertHeigth / 2.0)
                        default:
                            print("Error: invalid tiker position")
                        }
                    } else {
                        alertRect.size.width = CGFloat(alertWidth)
                        alertRect.size.height = CGFloat(alertHeigth)
                        switch alert.position {
                        case "printPage": //TODO: какойто непонятный кейс!
                            alertRect.origin.x = 2000
                            alertRect.origin.y = 2000
                        case "fullscreen":
                            alertRect.origin.x = screenRectangle.origin.x
                            alertRect.origin.y = screenRectangle.origin.y
                            alertRect.size.width = screenRectangle.width
                            alertRect.size.height = screenRectangle.height
                        case .some("right-bottom"):
                            var isPositionFound = false
                            var tmpLeftMargin = screenRectangle.width - CGFloat(alertWidth)
                            var tmpTopMargin: CGFloat =  0.0
                            
                            while !isPositionFound {
                                guard let windowContainer = Config.WindowContainer else {
                                    isPositionFound = true
                                    continue
                                }
                                guard let _ = windowContainer.first(where: { (windowCommand) -> Bool in
                                    windowCommand.alertRect.origin.x == tmpLeftMargin &&
                                        windowCommand.alertRect.origin.y == tmpTopMargin
                                }) else {
                                    isPositionFound = true
                                    continue
                                }
                                
                                tmpLeftMargin -= Constants.indentBetweenWindows
                                tmpTopMargin += Constants.indentBetweenWindows
                            }
                            alertRect.origin.x = tmpLeftMargin
                            alertRect.origin.y = tmpTopMargin
                            
                        case .some("right-top"):
                            var isPositionFound = false
                            var tmpLeftMargin = screenRectangle.width - CGFloat(alertWidth)
                            var tmpTopMargin =  screenRectangle.height - CGFloat(alertHeigth)
                            
                            while !isPositionFound {
                                guard let windowContainer = Config.WindowContainer else {
                                    isPositionFound = true
                                    continue
                                }
                                guard let _ = windowContainer.first(where: { (windowCommand) -> Bool in
                                    windowCommand.alertRect.origin.x == tmpLeftMargin &&
                                        windowCommand.alertRect.origin.y == tmpTopMargin
                                }) else {
                                    isPositionFound = true
                                    continue
                                }
                                
                                tmpLeftMargin -= Constants.indentBetweenWindows
                                tmpTopMargin -= Constants.indentBetweenWindows
                            }
                            alertRect.origin.x = tmpLeftMargin
                            alertRect.origin.y = tmpTopMargin
                        case .some("left-top"):
                            var isPositionFound = false
                            var tmpLeftMargin: CGFloat = 0
                            var tmpTopMargin = screenRectangle.height - CGFloat(alertHeigth)
                            
                            while !isPositionFound {
                                guard let windowContainer = Config.WindowContainer else {
                                    isPositionFound = true
                                    continue
                                }
                                guard let _ = windowContainer.first(where: { (windowCommand) -> Bool in
                                    windowCommand.alertRect.origin.x == tmpLeftMargin &&
                                        windowCommand.alertRect.origin.y == tmpTopMargin
                                }) else {
                                    isPositionFound = true
                                    continue
                                }
                                
                                tmpLeftMargin += Constants.indentBetweenWindows
                                tmpTopMargin -= Constants.indentBetweenWindows
                            }
                            alertRect.origin.x = tmpLeftMargin
                            alertRect.origin.y = tmpTopMargin
                        case .some("left-bottom"):
                            var isPositionFound = false
                            var tmpLeftMargin: CGFloat = 0
                            var tmpTopMargin: CGFloat = 0
                            
                            while !isPositionFound {
                                guard let windowContainer = Config.WindowContainer else {
                                    isPositionFound = true
                                    continue
                                }
                                guard let _ = windowContainer.first(where: { (windowCommand) -> Bool in
                                    windowCommand.alertRect.origin.x == tmpLeftMargin &&
                                        windowCommand.alertRect.origin.y == tmpTopMargin
                                }) else {
                                    isPositionFound = true
                                    continue
                                }
                                
                                tmpLeftMargin += Constants.indentBetweenWindows
                                tmpTopMargin += Constants.indentBetweenWindows
                            }
                            alertRect.origin.x = tmpLeftMargin
                            alertRect.origin.y = tmpTopMargin
                            
                        case .some("center"):
                            var isPositionFound = false
                            var tmpLeftMargin = CGFloat(screenRectangle.width / 2.0) - CGFloat(alertWidth / 2.0)
                            var tmpTopMargin = CGFloat(screenRectangle.height / 2.0) - CGFloat(alertHeigth / 2.0)
                            
                            while !isPositionFound {
                                guard let windowContainer = Config.WindowContainer else {
                                    isPositionFound = true
                                    continue
                                }
                                guard let _ = windowContainer.first(where: { (windowCommand) -> Bool in
                                    windowCommand.alertRect.origin.x == tmpLeftMargin &&
                                        windowCommand.alertRect.origin.y == tmpTopMargin
                                }) else {
                                    isPositionFound = true
                                    continue
                                }
                                
                                tmpLeftMargin -= Constants.indentBetweenWindows
                                tmpTopMargin += Constants.indentBetweenWindows
                            }
                            alertRect.origin.x = tmpLeftMargin
                            alertRect.origin.y = tmpTopMargin
                        default:
                            print("Error: invalid alert")
                            alertRect.origin.x = 0
                            alertRect.origin.y = 0
                        }
                    }
                } else if alert.id == "wallpaper" {
                    alertRect = CGRect(x: 0, y: 0, width: screenRectangle.width, height: screenRectangle.height)
                }
            case .authorizationForm:
                alertRect = CGRect(x: screenRectangle.width / 2 - 200, y: screenRectangle.height / 2 - 200, width: 350, height: 450)
            case .options:
                alertRect = CGRect(x: screenRectangle.width - 500, y: 0, width: 500, height: 500)
            case .history:
                alertRect = CGRect(x: screenRectangle.width - 500, y: 0, width: 500, height: 500)
        }
        
        window?.setFrame(alertRect, display: true)
        
        topView.frame = NSRect(x: 0, y: 0, width: alertRect.width, height: alertRect.height)
        CaptionBrowser?.frame = NSRect(x: 0, y: 0, width: alertRect.width, height: alertRect.height)
        
        let preferences = WebPreferences()
        preferences.allowsAnimatedImageLooping = true
        preferences.allowsAnimatedImages = true
        preferences.arePlugInsEnabled = true
        preferences.autosaves = true
        preferences.cacheModel = WebCacheModel.documentBrowser
        preferences.isJavaEnabled = true
        preferences.isJavaScriptEnabled = true
        preferences.javaScriptCanOpenWindowsAutomatically = true
        preferences.loadsImagesAutomatically = true
        preferences.privateBrowsingEnabled = true
        preferences.shouldPrintBackgrounds = true
        preferences.tabsToLinks = true
        preferences.usesPageCache = false
        preferences.suppressesIncrementalRendering = true
        
        if CaptionBrowser != nil,
        let tmpParentWindowParams = alert.windows?.allObjects.first as? AlertWindow {
            parentWindowParams = tmpParentWindowParams
            CaptionBrowser.Owner = self
            MainBrowser.Owner = self
            
            guard let captionHref = tmpParentWindowParams.captionhref else {
                DebugLogger.showAlert(with: "Invalid captionhref")
                return
            }
            
            if !FileManager.default.fileExists(atPath: captionHref),
                let url = URL(string: captionHref) {
                let request = URLRequest(url: url)
                CaptionBrowser.mainFrame.load(request)
            } else {
                do {
                    let url = URL(fileURLWithPath: captionHref)
                    let htmlString = try String(contentsOf: url, encoding: .utf8)
                    CaptionBrowser?.mainFrame.loadHTMLString(htmlString, baseURL: url)
                } catch {
                    print(error.localizedDescription)
                }
            }
            
            guard let leftMarginString = parentWindowParams?.leftmargin else { return }
            guard let rightMarginString = parentWindowParams?.rightmargin else { return }
            guard let topMarginString = parentWindowParams?.topmargin else { return }
            guard let bottomMarginString = parentWindowParams?.bottommargin else { return }
            guard let leftMargin = Double(leftMarginString) else { return }
            guard let rightMargin = Double(rightMarginString) else { return }
            guard let topMargin = Double(topMarginString) else { return }
            guard let bottomMargin = Double(bottomMarginString) else { return }
            
            let mainBrowserWidth = Double(alertRect.width) - leftMargin - rightMargin
            let mainBrowserHeight = Double(alertRect.height) - topMargin - bottomMargin
            MainBrowser?.frame = CGRect(x: leftMargin, y: bottomMargin, width: mainBrowserWidth, height: mainBrowserHeight)
        
            HistoryTableView?.frame = CGRect(x: leftMargin, y: bottomMargin, width: mainBrowserWidth, height: mainBrowserHeight)
            
            let mainbrowserpart = Double((HistoryTableView?.frame.width)!) / 5
            var searchbuttonleft = Double(MainBrowser.frame.width) - mainbrowserpart / 5 - mainbrowserpart
            let searchbuttontop = Double(HistoryTableView.frame.height) + mainbrowserpart / 2 + mainbrowserpart / 5
            SearchButton.frame = CGRect(x: searchbuttonleft,y: searchbuttontop, width: mainbrowserpart ,height:mainbrowserpart / 4)
            
            searchbuttonleft = searchbuttonleft - mainbrowserpart / 5 - mainbrowserpart * 2
            
            SearchFiled.frame = CGRect(x: searchbuttonleft,y: searchbuttontop, width: mainbrowserpart * 2 ,height:mainbrowserpart / 4)
            
            searchbuttonleft = searchbuttonleft - mainbrowserpart / 5 - mainbrowserpart
            
            ClearHistory.frame = CGRect(x: searchbuttonleft,y: searchbuttontop, width: mainbrowserpart ,height:mainbrowserpart / 4)
            
            if windowType == .history {
                UpdateHistory(request: nil)
                MainBrowser?.isHidden = true
                CaptionBrowser.isHidden = false
                HistoryTableView?.isHidden = false
                allowLoad = allowLoad + 1
                SearchFiled.isHidden = false
                SearchButton.isHidden = false
                ClearHistory.isHidden = false
            } else {
                SearchFiled.isHidden = true
                SearchButton.isHidden = true
                ClearHistory.isHidden = true
                MainBrowser?.isHidden = false
                HistoryTableView?.isHidden = true
            }

            if let alertHref = alertHref, let isLocal = isLocal {
                if isLocal {
                    let bundleURL = URL(fileURLWithPath: Bundle.main.bundlePath)
                    MainBrowser?.mainFrame.loadHTMLString(alertHref, baseURL: bundleURL)
                } else {
                    if !FileManager.default.fileExists(atPath: alertHref), let url = URL(string: alertHref) {
                        let request = URLRequest(url: url)
                        MainBrowser.mainFrame.load(request)
                    } else {
                        let url = URL(fileURLWithPath: alertHref)
                        let request = URLRequest(url: url)
                        MainBrowser.mainFrame.load(request)
                    }
                }
            }
        }
        Config.WindowContainer?.append(self)
    }
    
    func Close() {
        guard let window = self.window else {
            DebugLogger.showAlert(with: "Error: invalid window parameter")
            return
        }
        
        if let url = URL(string: "about:blank") {
            let request = URLRequest(url: url)
            if let mainBrowserMainFrame = MainBrowser.mainFrame {
                mainBrowserMainFrame.load(request)
            }
            if let captionBrowserMainFrame = CaptionBrowser.mainFrame {
                captionBrowserMainFrame.load(request)
            }
        }
        
        window.close()
        if let windowContainer = Config.WindowContainer,
           let index = windowContainer.index(of: self) {
            Config.shared.deletefromWindowContainer(index: index)
        }
    }
    
    func webView(_ sender: WebView!, runJavaScriptAlertPanelWithMessage message: String!, initiatedBy frame: WebFrame!) {
        let myPopup:NSAlert = NSAlert()
        myPopup.addButton(withTitle: "OK")
        myPopup.informativeText = message
        
        
    }
    
    func webView(_ webView: WebView!, decidePolicyForNewWindowAction actionInformation: [AnyHashable : Any]!, request: URLRequest!, newFrameName frameName: String!, decisionListener listener: WebPolicyDecisionListener!) {
        if let url = request.url{
            if (url.absoluteString.range(of: "DeskAlerts") == nil && url.absoluteString.range(of: "deskalerts.com") == nil){
                    NSWorkspace.shared.open(request.url!)
            }
        }
    }
    
    func webView(_ webView: WebView!, didClearWindowObject windowObject: WebScriptObject!, for frame: WebFrame!) {
        webView.windowScriptObject.setValue(HtmlInteropClass(data: alert!, owner: self), forKey: "external")
    }
    
    func webView(_ sender: WebView!, didFinishLoadFor frame: WebFrame!) {
        sender.windowScriptObject.setValue(HtmlInteropClass(data: alert!, owner: self), forKey: "external")
        if (alert==nil)
        {
            self.window?.close()
            if (Config.WindowContainer != nil)
            {
                let ind = (Config.WindowContainer?.index(of: self))
                if (ind != nil)
                {
                    Config.shared.deletefromWindowContainer(index: ind!)
                }
                
            }
        }
        
        if (alert?.windows == nil)||((alert?.windows?.count)!<=0)
        {
            CoreDataManager.instance.managedObjectContext.delete(alert!)
            CoreDataManager.instance.saveContext()
            
            self.window?.close()
            if (Config.WindowContainer != nil)
            {
                let ind = (Config.WindowContainer?.index(of: self))
                if (ind != nil)
                {
                    Config.shared.deletefromWindowContainer(index: ind!)
                }
                
            }
        }
        if sender == CaptionBrowser {
            allowLoad = allowLoad + 1
            
            if isHiden {
                if allowLoad > 1 {
                    self.Close()
                }
            } else {
                if let alert = alert,
                    let windows = alert.windows,
                    let alertWindows = windows.allObjects as? [AlertWindow],
                    let alertWindow = Array(alertWindows).first {
                    if allowLoad > 1 {
                        var transparency = 255.0
                        let nottransparent = 255.0
                        
                        if (alertWindow.transparency != nil)
                        {
                            let windowtransparency = Double((alertWindow.transparency)!)
                            if (windowtransparency != nil)
                            {
                                transparency = windowtransparency!
                            }
                        }
                        
                        
                        self.window?.alphaValue = CGFloat(transparency / nottransparent)
                        CaptionBrowser?.alphaValue = CGFloat(transparency / nottransparent)
                        MainBrowser?.alphaValue = CGFloat(transparency / nottransparent)
                        
                        
                        var fetchRequest = NSFetchRequest<NSFetchRequestResult>()
                        // Create Entity Description
                        var entityDescription = NSEntityDescription.entity(forEntityName: "Alert", in: CoreDataManager.instance.managedObjectContext)
                        // Configure Fetch Request
                        fetchRequest.entity = entityDescription
                        fetchRequest.predicate = NSPredicate(format: "alert_id == %@", (Config.SessionUser?.name)!)
                        do {
                            let alert  = try CoreDataManager.instance.managedObjectContext.fetch(fetchRequest).first as? Alert
                            if (alert != nil)
                            {
                                alert?.isreaded = true
                                CoreDataManager.instance.saveContext()
                            }
                            
                        } catch {
                            let fetchError = error as NSError
                            print(fetchError)
                        }
                        
                        TrayMenuManager.shared.updateState()
                    }
                }
            }
        }
        if (sender == MainBrowser)
        {
            var Args = Array<NSObject>.init()
            Args.append(TickerIndexes as NSObject)
            let _  = MainBrowser?.windowScriptObject.callWebScriptMethod("Initial", withArguments: Args)
            allowLoad = allowLoad + 1
            
            MainBrowser.window?.makeFirstResponder(MainBrowser)
            
            if isHiden {
                if allowLoad > 1 {
                    self.Close()
                }
            } else {
                if (alert == nil)
                {
                    return
                }
                if (alert?.windows == nil)
                {
                    return
                }
                
                
                if let window = Array(alert?.windows?.allObjects as! [AlertWindow]).first, allowLoad > 1 {
                    var transparency = 255.0
                    let nottransparent = 255.0
                    
                    if (window.transparency != nil)
                    {
                        let windowtransparency = Double((window.transparency)!)
                        if (windowtransparency != nil)
                        {
                            transparency = windowtransparency!
                        }
                    }
                    
                    self.window?.alphaValue = CGFloat(transparency / nottransparent)
                    CaptionBrowser?.alphaValue = CGFloat(transparency / nottransparent)
                    MainBrowser?.alphaValue = CGFloat(transparency / nottransparent)
                    
                    
                    
                    
                    if (alert?.alert_id != nil)
                    {
                        
                        var fetchRequest = NSFetchRequest<NSFetchRequestResult>()
                        // Create Entity Description
                        var entityDescription = NSEntityDescription.entity(forEntityName: "Alert", in: CoreDataManager.instance.managedObjectContext)
                        // Configure Fetch Request
                        fetchRequest.entity = entityDescription
                        fetchRequest.predicate = NSPredicate(format: "alert_id == %@", (alert?.alert_id)!)
                        do {
                            let alert  = try CoreDataManager.instance.managedObjectContext.fetch(fetchRequest).first as? Alert
                            if (alert != nil)
                            {
                                alert?.isreaded = true
                                CoreDataManager.instance.saveContext()
                            }
                            
                        } catch {
                            let fetchError = error as NSError
                            print(fetchError)
                        }
                        
                        TrayMenuManager.shared.updateState()
                    }
                }
            }
            
        }
    }
    
    func windowWillResize(_ sender: NSWindow, to frameSize: NSSize) -> NSSize {
        
        let widthdif = frameSize.width -  sender.frame.size.width
        let heigthdif = frameSize.height - sender.frame.size.height
        
        
        let captionbrowserwidth = CaptionBrowser.frame.width + widthdif
        let captionbrowserheigth = CaptionBrowser.frame.height + heigthdif
        
        
        topView.frame = NSRect.init(x: 0, y: 0, width: Double(captionbrowserwidth), height: Double(captionbrowserheigth))
        CaptionBrowser?.frame = NSRect.init(x: 0, y: 0, width: Double(captionbrowserwidth), height: Double(captionbrowserheigth))
        
        
        let mainbrowserwidth = MainBrowser.frame.width + widthdif
        let mainbrowserheight = MainBrowser.frame.height + heigthdif
        
        let minimumAlertWidth: CGFloat = 300.0
        let minimumAlertHeight: CGFloat = 200.0
        
        // Limit the minimum size of the alert window
        self.view.window?.minSize = NSSize(width: minimumAlertWidth, height: minimumAlertHeight)
        
        MainBrowser?.frame = NSRect.init(x: Double(MainBrowser.frame.origin.x), y: Double(MainBrowser.frame.origin.y), width: Double(mainbrowserwidth), height: Double(mainbrowserheight))
        
        return frameSize
    }
}


class CustomView:WebView
{
    var canMoveMouse: Bool = false
    var Owner:WindowCommand?
    
    
    required init(coder: NSCoder)
    {
        super.init(coder: coder)!
    }
    
    
    override open var acceptsFirstResponder: Bool
    {
        return true
    }
    
    
    override func mouseDown(with event: NSEvent) {
        let window = self.window!
        let startingPoint = event.locationInWindow
    
        // highlight(true)
        
        // Track events until the mouse is up (in which we interpret as a click), or a drag starts (in which we pass off to the Window Server to perform the drag)
        var shouldCallSuper = false
        
       
        // trackEvents won't return until after the tracking all ends
        window.trackEvents(matching: [.leftMouseDragged, .leftMouseUp], timeout:NSEvent.foreverDuration, mode: RunLoop.Mode.default) { event, stop in
            switch event?.type {
            case .leftMouseUp?:
                // Stop on a mouse up; post it back into the queue and call super so it can handle it
                shouldCallSuper = true
                NSApp.postEvent(event!, atStart: false)
                stop.pointee = true
                
            case .leftMouseDragged?:
                // track mouse drags, and if more than a few points are moved we start a drag
                let currentPoint = event?.locationInWindow
                if (abs((currentPoint?.x)! - startingPoint.x) >= 5 || abs((currentPoint?.y)! - startingPoint.y) >= 5) {
                    // self.highlight(false)
                    stop.pointee = true
                    if #available(OSX 10.11, *) {
                        canMoveMouse = true
                        if (canMoveMouse)
                        {
                            window.performDrag(with: event!)
                        }
                    } else {
                        // Fallback on earlier versions
                    }
                }
                
            default:
                break
            }
        }
        
        if (shouldCallSuper) {
            super.mouseDown(with: event)
        }
    }

    override var mouseDownCanMoveWindow: Bool {
        return false
    }
    
    override var canBecomeKeyView: Bool {
        return true
    }
}

