//
//  APScreenSaverView.swift
//  PaskySaver
//
//  Created by Alberto Pasca on 31/03/16.
//  Copyright © 2016 albertopasca.it. All rights reserved.
//

import Cocoa
import ScreenSaver
import WebKit
import SQLite3

private struct Constants {
    static let databaseFileName = "/DeskAlerts.sqlite"
    static let clearWebPage = ""
    static let defaultUpdateTimerTimeInterval: TimeInterval = 60
}

private struct ScreenSaver {
    let deliveryDuration: TimeInterval
    let content: String
    let user: String
}

class DeskAlertsScreenSaverView: ScreenSaverView {
    private var screenSaversFromDatabase = [ScreenSaver]()
    private var displayedScreenSaverIndex: Int = 0

    private weak var webView: WKWebView?
    private var changeTimer: Timer?
    
    
    //MARK: - Init
    //TODO: Убрать говнокод, разобраться как сделать инициализацию правильно
    override init?(frame: NSRect, isPreview: Bool) {
        super.init(frame: frame, isPreview: isPreview)
        if !isPreview, webView == nil {
            let tmpWebView = WKWebView(frame: frame)
            webView = tmpWebView
            self.addSubview(tmpWebView)
        }
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        if !self.isPreview, webView == nil, let frame = NSScreen.main?.frame {
//            let tFrame = CGRect(x: 0, y: 0, width: 480, height: 230) for test
            let tmpWebView = WKWebView(frame: frame)
            webView = tmpWebView
            self.addSubview(tmpWebView)
        }
    }
    //MARK: -
    
    override func startAnimation() {
        super.startAnimation()
        if !self.isPreview {
           updateTimer()
        }
        
    }
    override func stopAnimation() {
        super.stopAnimation()
        changeTimer?.invalidate()
        cleanWebView()
    }
    
    override func draw(_ rect: NSRect) {
        super.draw(rect)
    }
    
    override func animateOneFrame() {
        
    }
    override var hasConfigureSheet: Bool
    {
        return false
    }
    
    override var configureSheet: NSWindow?
    {
        return nil
    }
    
    override var wantsDefaultClipping: Bool {
        return false
    }
    
    //TODO: - Переделать этот ужас на CoreData!!!
    private func upadateScreenSaversFromDatabase() {
        screenSaversFromDatabase = [ScreenSaver]()
        guard let documentsDirectoryPath = FileManager.documentsDirectoryPath,
              let databaseURL = URL(string: documentsDirectoryPath + Constants.databaseFileName) else { return }
    
        var db: OpaquePointer?
        if sqlite3_open_v2(databaseURL.path, &db, SQLITE_OPEN_READONLY, nil) != SQLITE_OK {
            print("error opening database")
        }
        
        // Initialize Fetch Request
        var statement: OpaquePointer?
        
        if sqlite3_prepare_v2(db, "select zusers, zalerttext, zautoclose from zalert where zid = 'screensaver' and zisclosed = 0", -1, &statement, nil) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("error preparing select: \(errmsg)")
        }
        
        while sqlite3_step(statement) == SQLITE_ROW {
            guard let deliveryDuration = TimeInterval(String(cString:sqlite3_column_text(statement, 2))) else { continue }
            let content = String(cString:sqlite3_column_text(statement, 1))
            let user = String(cString: sqlite3_column_text(statement, 0))
            
            let tmpScreenSaver = ScreenSaver(deliveryDuration: deliveryDuration,
                                             content: content,
                                             user: user)
            screenSaversFromDatabase.append(tmpScreenSaver)
        }
    }
    
    //MARK: - Timer
    private func updateTimer() {
        upadateScreenSaversFromDatabase()
        updateDisplayedScreenSaverIndex()
        if !screenSaversFromDatabase.isEmpty {
            let screenSaver = screenSaversFromDatabase[displayedScreenSaverIndex]
            setScreenSaverToScreen(screenSaver: screenSaver)
            resetTimer(with: screenSaver.deliveryDuration)
        } else {
            cleanWebView()
            resetTimer(with: Constants.defaultUpdateTimerTimeInterval)
        }
    }
    
    private func resetTimer(with timeInterval: TimeInterval) {
        changeTimer = Timer.scheduledTimer(withTimeInterval: timeInterval, repeats: false, block: { (timer) in
            self.updateTimer()
        })
    }
    
    private func updateDisplayedScreenSaverIndex() {
        displayedScreenSaverIndex += 1
        if displayedScreenSaverIndex > screenSaversFromDatabase.count - 1 {
            displayedScreenSaverIndex = 0
        }
    }
    
    private func setScreenSaverToScreen(screenSaver: ScreenSaver) {
        webView?.loadHTMLString(screenSaver.content, baseURL: nil)
    }
    
    private func cleanWebView() {
        webView?.loadHTMLString(Constants.clearWebPage, baseURL: nil)
        webView?.reload()
    }
}

