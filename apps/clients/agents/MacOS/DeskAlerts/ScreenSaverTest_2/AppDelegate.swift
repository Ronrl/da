//
//  AppDelegate.swift
//  ScreenSaverTest_2
//
//  Created by Ярослав Долговых on 30/01/2020.
//  Copyright © 2020 Toolbarstudio Inc. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

