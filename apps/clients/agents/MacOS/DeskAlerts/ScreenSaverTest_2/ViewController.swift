//
//  ViewController.swift
//  ScreenSaverTest_2
//
//  Created by Ярослав Долговых on 30/01/2020.
//  Copyright © 2020 Toolbarstudio Inc. All rights reserved.
//

import Cocoa

class ViewController: NSViewController {

    @IBOutlet weak var bt: NSButton!
    @IBOutlet weak var deskAlertsScreenSaverView: DeskAlertsScreenSaverView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override var representedObject: Any? {
        didSet {
            
        }
    }

    @IBAction func buttonAction_2(_ sender: Any) {
        deskAlertsScreenSaverView.stopAnimation()
    }
    @IBAction func buttonAction(_ sender: Any) {
        deskAlertsScreenSaverView.startAnimation()
    }
    
}

