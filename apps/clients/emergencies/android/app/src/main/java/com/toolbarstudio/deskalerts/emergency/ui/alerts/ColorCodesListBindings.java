package com.toolbarstudio.deskalerts.emergency.ui.alerts;

import android.databinding.BindingAdapter;
import android.widget.ListView;

import com.toolbarstudio.deskalerts.emergency.data.entities.ColorCode;

import java.util.List;

/**
 * Created by maxsh on 20.02.2018.
 */

/**
 * Contains {@link BindingAdapter}s for the {@link ColorCode} list.
 */
public class ColorCodesListBindings {
    @BindingAdapter("app:items")
    public static void setItems(ListView listView, List<ColorCode> items) {
        ColorCodesAdapter adapter = (ColorCodesAdapter) listView.getAdapter();
        if (adapter != null)
        {
            adapter.replaceData(items);
        }
    }
}
