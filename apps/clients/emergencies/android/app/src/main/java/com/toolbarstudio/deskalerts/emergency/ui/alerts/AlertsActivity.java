package com.toolbarstudio.deskalerts.emergency.ui.alerts;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.SearchView;
import android.widget.Spinner;

import com.toolbarstudio.deskalerts.emergency.R;
import com.toolbarstudio.deskalerts.emergency.core.ActivityUtils;
import com.toolbarstudio.deskalerts.emergency.core.ViewModelsFactory;
import com.toolbarstudio.deskalerts.emergency.data.sharedpreferences.SharedPreferencesHelper;
import com.toolbarstudio.deskalerts.emergency.databinding.NavigationViewHeaderBinding;
import com.toolbarstudio.deskalerts.emergency.ui.setup.SetupActivity;

import java.util.Objects;

/**
 * Created by himselfego on 09.02.2018.
 */

public class AlertsActivity extends AppCompatActivity {

    public Spinner spinner;

    private DrawerLayout drawerLayout;
    private AlertsViewModel viewModel;
    private SearchView searchView;
    private String currentFragmentName;
    private Toolbar toolbar;

    public static AlertsViewModel obtainViewModel(FragmentActivity activity) {
        ViewModelsFactory factory = ViewModelsFactory.getInstance(activity.getApplication());
        return ViewModelProviders.of(activity, factory).get(AlertsViewModel.class);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alerts);
        if (Objects.equals(SharedPreferencesHelper.getHostUrl(), "") ||
                SharedPreferencesHelper.getHostUrl() == null ||
                Objects.equals(SharedPreferencesHelper.getLogin(), "") ||
                SharedPreferencesHelper.getLogin() == null ||
                Objects.equals(SharedPreferencesHelper.getPassword(), "") ||
                SharedPreferencesHelper.getPassword() == null ||
                Objects.equals(SharedPreferencesHelper.getGuid(), "") ||
                SharedPreferencesHelper.getGuid() == null) {
            Intent intent = new Intent(getApplicationContext(), SetupActivity.class);
            startActivity(intent);
            return;
        }

        viewModel = obtainViewModel(this);

        setupToolbar();

        setupSpinner();

        setupNavigationDrawer();

        setupViewFragment();

        subscribeToLogoutEvent();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_search, menu);
        MenuItem item = menu.findItem(R.id.menuSearch);
        searchView = (SearchView)item.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                AlertsAdapter.instance.setSearchFilter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                AlertsAdapter.instance.setSearchFilter(newText);
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    private void setupViewFragment() {
        AlertsFragment alertsFragment =
                (AlertsFragment) getSupportFragmentManager().findFragmentById(R.id.content_frame);
        if (alertsFragment == null) {
            alertsFragment = AlertsFragment.newInstance();
            ActivityUtils.replaceFragmentInActivity(
                    getSupportFragmentManager(), alertsFragment, R.id.content_frame);
            currentFragmentName = "alerts";
        }
    }

    public void replaceFragments(){
        switch (currentFragmentName){
            case "alerts":
                ColorCodesFragment codesFragment =  ColorCodesFragment.getInstance();
                ActivityUtils.replaceFragmentInActivity(getSupportFragmentManager(), codesFragment, R.id.content_frame);
                searchView.setVisibility(View.GONE);
                spinner.setVisibility(View.GONE);
                currentFragmentName = "codes";
                toolbar.setTitle(getString(R.string.app_color_code));
                break;
            case "codes":
                AlertsFragment alertsFragment =  AlertsFragment.getInstance();
                ActivityUtils.replaceFragmentInActivity(getSupportFragmentManager(), alertsFragment, R.id.content_frame);
                currentFragmentName = "alerts";
                toolbar.setTitle(getString(R.string.app_name));
                searchView.setVisibility(View.VISIBLE);
                spinner.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void setupNavigationDrawer() {
        drawerLayout = findViewById(R.id.alerts_activity_drawer_layout);
        drawerLayout.setStatusBarBackground(R.color.colorPrimaryDark);
        NavigationView navigationView = findViewById(R.id.alerts_activity_navigation_view);

        NavigationViewHeaderBinding navigationViewHeaderBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.navigation_view_header,
                navigationView, false);
        navigationView.addHeaderView(navigationViewHeaderBinding.getRoot());

        navigationViewHeaderBinding.setViewmodel(viewModel);


        if (navigationView != null) {
            setupDrawerContent(navigationView);
        }
    }

    private void setupDrawerContent(final NavigationView navigationView) {
        navigationView.getMenu().getItem(0).setChecked(true);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.alerts_menu_item: {
                        onNavigationChecked(menuItem, navigationView);
                        break;
                    }
                    case R.id.color_codes_menu_item: {
                        onNavigationChecked(menuItem, navigationView);
                        break;
                    }
                    case R.id.log_out_menu_item: {
                        showDialog();
                        break;
                    }
                    default:
                        break;
                }
                drawerLayout.closeDrawers();
                return true;
            }
        });
    }

    private void onNavigationChecked(@NonNull MenuItem menuItem, NavigationView navigationView) {
        if (!menuItem.isChecked())
            replaceFragments();
        uncheckMenuItems(navigationView);
        menuItem.setChecked(true);
    }

    private void uncheckMenuItems(NavigationView navigationView){
        int size = navigationView.getMenu().size();
        for (int i = 0; i < size; i++) {
            navigationView.getMenu().getItem(i).setChecked(false);
        }
    }

    private void setupToolbar() {
        toolbar = findViewById(R.id.alerts_activity_toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    private void setupSpinner(){
        spinner = findViewById(R.id.colors_spinner);
    }

    private void showDialog() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        Intent intent = new Intent(getApplicationContext(), SetupActivity.class);
                        startActivity(intent);
                        SharedPreferencesHelper.clear();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Log out");
        builder.setMessage("Are you sure you want to log out?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (!searchView.isIconified()) {
            searchView.setIconified(true);
        } else {
            super.onBackPressed();
        }
    }

    private void subscribeToLogoutEvent() {
        viewModel.getLogoutEvent().observe(this, new Observer<Void>() {
            @Override
            public void onChanged(@Nullable Void _) {
                Intent intent = new Intent(getApplicationContext(), SetupActivity.class);
                startActivity(intent);
            }
        });
    }
}
