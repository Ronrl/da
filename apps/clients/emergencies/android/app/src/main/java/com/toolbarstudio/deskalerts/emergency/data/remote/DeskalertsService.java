package com.toolbarstudio.deskalerts.emergency.data.remote;

import com.toolbarstudio.deskalerts.emergency.data.dtos.EmergencyResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by himselfego on 11.02.2018.
 */

public interface DeskalertsService {
    @GET("register.asp")
    Call<String> pingServer();

    //@FormUrlEncoded
    //@POST("register.asp")
   // Call<String> registerOrLogin(@Field("uname") String login, @Field("upass") String password, @Field("deskbar_id") String guid, @Field("client_id") String clientId);

    @GET("emergency_app_request.asp")
    Call<EmergencyResponse> getEmergencyAlertsAndColorCodes(@Query("login") String login, @Query("password") String password);

    @FormUrlEncoded
    @POST("api_request.asp")
    Call<String> sendAlert(@Field("xml_data") String xmlData, @Field("api_signature") String apiKey);
}
