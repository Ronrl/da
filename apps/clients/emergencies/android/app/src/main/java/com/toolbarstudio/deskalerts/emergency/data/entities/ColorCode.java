package com.toolbarstudio.deskalerts.emergency.data.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.graphics.Color;
import android.support.annotation.Nullable;


/**
 * Created by himselfego on 13.02.2018.
 */

@Entity(tableName = "color_codes")
public class ColorCode {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "entity_id")
    private final int entityId;

    @Nullable
    @ColumnInfo(name = "title")
    private final String title;

    @Nullable
    @ColumnInfo(name = "hex")
    private final String hex;

    public ColorCode(int entityId, @Nullable String title, @Nullable String hex) {
        this.entityId = entityId;
        this.title = title;
        this.hex = hex;
    }

    public ColorCode(ColorCode colorCode) {
        this.entityId = 0;
        this.title = colorCode.getTitle();
        this.hex = colorCode.getHex();
    }

    public int getEntityId() {
        return entityId;
    }

    @Nullable
    public String getTitle() {
        return title;
    }

    @Nullable
    public String getHex() {
        return hex;
    }

    public int colorCodeInt() {
        return Color.parseColor(this.hex);
    }

}
