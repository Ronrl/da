package com.toolbarstudio.deskalerts.emergency.ui.setup;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;
import com.toolbarstudio.deskalerts.emergency.R;
import com.toolbarstudio.deskalerts.emergency.databinding.FragmentAuthorizationBinding;

import java.util.Objects;

/**
 * Created by himselfego on 10.02.2018.
 */

public class AuthorizationFragment extends Fragment implements BlockingStep {
    private SetupViewModel viewModel;
    private FragmentAuthorizationBinding fragmentAuthorizationBinding;
    private TextInputLayout loginTextInputLayout;
    private TextInputLayout passwordTextInputLayout;

    public AuthorizationFragment() {
        // It's ok
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View root = inflater.inflate(R.layout.fragment_authorization, container, false);
        if (fragmentAuthorizationBinding == null) {
            fragmentAuthorizationBinding = FragmentAuthorizationBinding.bind(root);
        }

        viewModel = SetupActivity.obtainViewModel(getActivity());

        fragmentAuthorizationBinding.setViewmodel(viewModel);

        setHasOptionsMenu(true);
        setRetainInstance(false);

        return fragmentAuthorizationBinding.getRoot();
    }

    @Nullable
    @Override
    public VerificationError verifyStep() {
        if (loginTextInputLayout == null) {
            loginTextInputLayout = getView().findViewById(R.id.host_fragment_login_text_input_layout);
        }
        if (passwordTextInputLayout == null) {
            passwordTextInputLayout = getView().findViewById(R.id.host_fragment_password_text_input_layout);
        }
        if (viewModel.login.get() == null || Objects.equals(viewModel.login.get(), "")) {
            return new VerificationError(getString(R.string.setup_login_enter_error_text));
        }
        if (viewModel.password.get() == null || Objects.equals(viewModel.password.get(), "")) {
            return new VerificationError(getString(R.string.setup_password_enter_error_text));
        }
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {
        if (Objects.equals(error.getErrorMessage(), getString(R.string.setup_login_enter_error_text))) {
            loginTextInputLayout.setError(error.getErrorMessage());
        } else if (Objects.equals(error.getErrorMessage(), getString(R.string.setup_password_enter_error_text))) {
            passwordTextInputLayout.setError(error.getErrorMessage());
        }
    }

    @Override
    public void onNextClicked(StepperLayout.OnNextClickedCallback callback) {
    }

    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {
        loginTextInputLayout.setError("");
        passwordTextInputLayout.setError("");
        viewModel.LoginOrRegister(callback, loginTextInputLayout, passwordTextInputLayout);
    }

    @Override
    public void onBackClicked(StepperLayout.OnBackClickedCallback callback) {
        callback.goToPrevStep();
    }
}
