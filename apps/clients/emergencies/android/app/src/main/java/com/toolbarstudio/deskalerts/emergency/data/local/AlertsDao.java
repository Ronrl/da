package com.toolbarstudio.deskalerts.emergency.data.local;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.toolbarstudio.deskalerts.emergency.data.entities.Alert;

import java.util.List;

/**
 * Created by himselfego on 13.02.2018.
 */

@Dao
public interface AlertsDao {
    @Query("SELECT * FROM alerts ORDER BY title")
    List<Alert> getAlerts();

    @Query("SELECT * FROM alerts WHERE entity_id = :alertId ORDER BY title")
    Alert getAlertById(int alertId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAlert(Alert alert);

    @Update
    int updateAlert(Alert alert);

    @Query("DELETE FROM alerts WHERE entity_id = :alertId")
    int deleteAlertById(int alertId);

    @Query("DELETE FROM alerts")
    void deleteAlerts();
}
