package com.toolbarstudio.deskalerts.emergency.ui.alerts;

import android.arch.lifecycle.Observer;
import android.content.DialogInterface;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Spinner;

import com.toolbarstudio.deskalerts.emergency.R;
import com.toolbarstudio.deskalerts.emergency.core.App;
import com.toolbarstudio.deskalerts.emergency.core.ScrollChildSwipeRefreshLayout;
import com.toolbarstudio.deskalerts.emergency.core.SnackbarMessage;
import com.toolbarstudio.deskalerts.emergency.core.SnackbarUtils;
import com.toolbarstudio.deskalerts.emergency.data.entities.Alert;
import com.toolbarstudio.deskalerts.emergency.data.entities.ColorCode;
import com.toolbarstudio.deskalerts.emergency.databinding.FragmentAlertsBinding;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by himselfego on 10.02.2018.
 */

public class AlertsFragment extends Fragment {
    private AlertsViewModel viewModel;
    private FragmentAlertsBinding fragmentAlertsBinding;
    private AlertsAdapter alertsAdapter;
    private Spinner spinner;

    private static AlertsFragment instance;

    public AlertsFragment() {
        // It's ok
    }

    public static AlertsFragment getInstance() {
        if (instance == null){
            instance = new AlertsFragment();
        }
        return instance;
    }

    public static AlertsFragment newInstance() {
        return new AlertsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        fragmentAlertsBinding = FragmentAlertsBinding.inflate(inflater, container, false);

        viewModel = AlertsActivity.obtainViewModel(getActivity());

        fragmentAlertsBinding.setViewmodel(viewModel);

        setHasOptionsMenu(false);

        return fragmentAlertsBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);
        setupSnackbar();
        setupListAdapter();
        setupRefreshLayout();
        subscribeToLoadEvent();
    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.start();
    }

    private void setupSnackbar() {
        viewModel.getSnackbarMessage().observe(this, new SnackbarMessage.SnackbarObserver() {
            @Override
            public void onNewMessage(@StringRes int snackbarMessageResourceId) {
                SnackbarUtils.showSnackbar(getView(), getString(snackbarMessageResourceId));
            }
        });
    }

    private void setupListAdapter() {
        ListView listView =  fragmentAlertsBinding.alertsList;
        alertsAdapter = new AlertsAdapter(new ArrayList<Alert>(0), viewModel, new AlertItemUserActionsListener() {
            @Override
            public void onAlertClicked(Alert alert) {
                showDialog(alert);
            }
        });
        listView.setAdapter(alertsAdapter);
    }

    private void setupFilter(){
        spinner = getActivity().findViewById(R.id.colors_spinner);
        spinner.getBackground().setColorFilter(getResources().getColor(R.color.ms_white), PorterDuff.Mode.SRC_ATOP);
        SpinnerAdapter adapter = new SpinnerAdapter(viewModel.colorCodes, viewModel);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ColorCode code = (ColorCode) parent.getItemAtPosition(position);
                AlertsAdapter.instance.setColorFilter(code.getHex());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        if (viewModel.currentColorFilter != ""){
            List<ColorCode> codes = retrieveAllItems(spinner);
            for (ColorCode colorCode: codes) {
                if (colorCode.getHex().equals(viewModel.currentColorFilter)){
                    int idx = codes.indexOf(colorCode);
                    spinner.setSelection(idx, true);
                    break;
                }
            }
        }
    }

    private List<ColorCode> retrieveAllItems(Spinner spinner) {
        Adapter adapter = spinner.getAdapter();
        int n = adapter.getCount();
        List<ColorCode> codes = new ArrayList<>(n);
        for (int i = 0; i < n; i++) {
            ColorCode user = (ColorCode) adapter.getItem(i);
            codes.add(user);
        }
        return codes;
    }

    private void setupRefreshLayout() {
        ListView listView =  fragmentAlertsBinding.alertsList;
        final ScrollChildSwipeRefreshLayout swipeRefreshLayout = fragmentAlertsBinding.refreshLayout;
        swipeRefreshLayout.setColorSchemeColors(
                ContextCompat.getColor(getActivity(), R.color.colorPrimary),
                ContextCompat.getColor(getActivity(), R.color.colorAccent),
                ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark)
        );
        // Set the scrolling view in the custom SwipeRefreshLayout.
        swipeRefreshLayout.setScrollUpChild(listView);
    }

    private void subscribeToLoadEvent() {
        viewModel.getLoadCodesEvent().observe(this, new Observer<Void>() {
            @Override
            public void onChanged(@Nullable Void _) {
                setupFilter();
            }
        });
    }

    private void showDialog(final Alert alert) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        viewModel.checkServerAvailabilityAndSendAlert(alert);
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        String body = App.getContext().getString(R.string.alerts_alert_sending_confirm);
        builder.setTitle(body);
        builder.setMessage(alert.getTitle()).setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }
}

