package com.toolbarstudio.deskalerts.emergency.ui.alerts;

import com.toolbarstudio.deskalerts.emergency.data.entities.Alert;

/**
 * Created by maxsh on 18.02.2018.
 */

public interface AlertItemUserActionsListener {

    void onAlertClicked(Alert alert);
}
