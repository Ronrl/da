package com.toolbarstudio.deskalerts.emergency.data.dtos;

import com.google.gson.annotations.SerializedName;
import com.toolbarstudio.deskalerts.emergency.data.entities.Alert;
import com.toolbarstudio.deskalerts.emergency.data.entities.ColorCode;

import java.util.List;

/**
 * Created by himselfego on 12.02.2018.
 */

public class EmergencyResponse {
    @SerializedName("alerts")
    private List<Alert> alerts;
    @SerializedName("color_codes")
    private List<ColorCode> colorCodes;
    @SerializedName("api_key")
    private String apiKey;
    @SerializedName("error")
    private String error;

    public List<Alert> getAlerts() {
        return alerts;
    }

    public void setAlerts(List<Alert> alerts) {
        this.alerts = alerts;
    }

    public List<ColorCode> getColorCodes() {
        return colorCodes;
    }

    public void setColorCodes(List<ColorCode> colorCodes) {
        this.colorCodes = colorCodes;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getError(){return error;}
}
