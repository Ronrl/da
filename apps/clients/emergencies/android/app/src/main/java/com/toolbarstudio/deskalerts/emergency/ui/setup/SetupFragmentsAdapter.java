package com.toolbarstudio.deskalerts.emergency.ui.setup;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;

import com.stepstone.stepper.Step;
import com.stepstone.stepper.adapter.AbstractFragmentStepAdapter;

/**
 * Created by himselfego on 10.02.2018.
 */

public class SetupFragmentsAdapter extends AbstractFragmentStepAdapter {
    private static String CURRENT_STEP_POSITION_KEY = "CURRENT_STEP_POSITION_KEY";

    public SetupFragmentsAdapter(@NonNull FragmentManager fm, @NonNull Context context) {
        super(fm, context);
    }

    @Override
    public Step createStep(int position) {
        switch (position) {
            case 0: {
                final HostFragment step = new HostFragment();
                Bundle bundle = new Bundle();
                bundle.putInt(CURRENT_STEP_POSITION_KEY, position);
                step.setArguments(bundle);
                return step;
            }
            case 1: {
                final AuthorizationFragment step = new AuthorizationFragment();
                Bundle bundle = new Bundle();
                bundle.putInt(CURRENT_STEP_POSITION_KEY, position);
                step.setArguments(bundle);
                return step;
            }
        }
        throw new IllegalArgumentException("Unsupported position: " + position);
    }

    @Override
    public int getCount() {
        return 2;
    }
}
