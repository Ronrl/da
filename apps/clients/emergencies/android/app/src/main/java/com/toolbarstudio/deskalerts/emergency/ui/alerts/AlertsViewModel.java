package com.toolbarstudio.deskalerts.emergency.ui.alerts;

import android.annotation.SuppressLint;
import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.content.Context;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.databinding.ObservableList;
import android.os.AsyncTask;
import android.support.annotation.StringRes;
import android.util.Log;

import com.toolbarstudio.deskalerts.emergency.R;
import com.toolbarstudio.deskalerts.emergency.core.SingleLiveEvent;
import com.toolbarstudio.deskalerts.emergency.core.SnackbarMessage;
import com.toolbarstudio.deskalerts.emergency.data.dtos.EmergencyResponse;
import com.toolbarstudio.deskalerts.emergency.data.entities.Alert;
import com.toolbarstudio.deskalerts.emergency.data.entities.ColorCode;
import com.toolbarstudio.deskalerts.emergency.data.local.EmergencyDatabase;
import com.toolbarstudio.deskalerts.emergency.data.remote.DeskalertsService;
import com.toolbarstudio.deskalerts.emergency.data.sharedpreferences.SharedPreferencesHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static com.toolbarstudio.deskalerts.emergency.core.App.TIMEOUT_VALUE;

/**
 * Created by himselfego on 09.02.2018.
 */

public class AlertsViewModel extends AndroidViewModel {

    public final ObservableList<Alert> alerts = new ObservableArrayList<>();

    public ObservableList<ColorCode> colorCodes = new ObservableArrayList<>();

    public final ObservableField<String> login = new ObservableField<>(SharedPreferencesHelper.getLogin());

    public final ObservableField<String> hostname = new ObservableField<>(SharedPreferencesHelper.getHostUrl());

    public final ObservableBoolean dataLoading = new ObservableBoolean(false);

    public final ObservableBoolean empty = new ObservableBoolean(false);

    public final ObservableBoolean emptyColors = new ObservableBoolean(false);

    public final ObservableBoolean progressVisible = new ObservableBoolean(false);

    @SuppressLint("StaticFieldLeak")
    private final Context mContext; // To avoid leaks, this must be an Application Context.

    private final SnackbarMessage mSnackbarText = new SnackbarMessage();

    private final SingleLiveEvent<Void> logoutEvent = new SingleLiveEvent<>();

    private final SingleLiveEvent<Void> loadCodesEvent = new SingleLiveEvent<>();

    private List<Alert> allAlerts = new ArrayList<>();

    private EmergencyDatabase db;

    String currentColorFilter = "";

    String currentSearchFilter = "";

    public AlertsViewModel(Application context) {
        super(context);
        mContext = context.getApplicationContext();
        createDb();
    }

    private void createDb(){
        db = EmergencyDatabase.getFileInstance(this.getApplication());
    }

    @SuppressLint("StaticFieldLeak")
    private void loadAlerts(){
        new AsyncTask<Void, Void, List<Alert>>() {
            @Override
            protected List<Alert> doInBackground(Void... params) {
                return db.alertsDao().getAlerts();
            }

            @Override
            protected void onPostExecute(List<Alert> items) {
                allAlerts = items;
            }
        }.execute();
    }

    @SuppressLint("StaticFieldLeak")
    private void loadColorCodes(){
        new AsyncTask<Void, Void, List<ColorCode>>() {
            @Override
            protected List<ColorCode> doInBackground(Void... params) {
                return db.codesDao().getColorCodes();
            }

            @Override
            protected void onPostExecute(List<ColorCode> codes) {
                colorCodes.clear();
                colorCodes.add(new ColorCode(0, "Default", "#747474"));
                colorCodes.addAll(codes);
                loadCodesEvent.call();
                setFilters();
            }
        }.execute();
    }

    @SuppressLint("StaticFieldLeak")
    private void saveAlerts(final List<Alert> alerts){
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                db.alertsDao().deleteAlerts();
                for (Alert alert :
                        alerts) {
                    db.alertsDao().insertAlert(alert);
                }
                return null;
            }
        }.execute();
    }
    @SuppressLint("StaticFieldLeak")
    private void saveColorCodes(final List<ColorCode> codes){
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                db.codesDao().deleteColorCodes();
                for (ColorCode colorCode :
                        codes) {
                    db.codesDao().insertColorCode(colorCode);
                }
                return null;
            }
        }.execute();
    }

    void start() {
        loadAlerts(false);
    }

    public void refreshAlerts(){
        loadAlerts(true);
    }

    SingleLiveEvent<Void> getLogoutEvent(){
        return logoutEvent;
    }

    SingleLiveEvent<Void> getLoadCodesEvent(){
        return loadCodesEvent;
    }

    private void logout(){

        SharedPreferencesHelper.clear();
    }

    private void loadAlerts(final boolean showLoadingUI){

        if (showLoadingUI) {
            dataLoading.set(true);
        }

        final Retrofit retrofit = new Retrofit.Builder().client(new OkHttpClient.Builder()
                .connectTimeout(TIMEOUT_VALUE, TimeUnit.SECONDS)
                .writeTimeout(TIMEOUT_VALUE, TimeUnit.SECONDS)
                .readTimeout(TIMEOUT_VALUE+10, TimeUnit.SECONDS)
                .build()).baseUrl(hostname.get()).
                addConverterFactory(ScalarsConverterFactory.create()).addConverterFactory(GsonConverterFactory.create()).build();
        final DeskalertsService deskalertsService = retrofit.create(DeskalertsService.class);

        deskalertsService.getEmergencyAlertsAndColorCodes(login.get(), SharedPreferencesHelper.getPassword()).enqueue(new Callback<EmergencyResponse>() {
            @Override
            public void onResponse(Call<EmergencyResponse> call, Response<EmergencyResponse> response) {
                if (showLoadingUI) {
                    dataLoading.set(false);
                }
                if (response.body().getError() == null) {
                    allAlerts.clear();
                    allAlerts = response.body().getAlerts();

                    colorCodes.clear();
                    colorCodes.add(new ColorCode(0, "Default", "#747474"));
                    colorCodes.addAll(response.body().getColorCodes());
                    loadCodesEvent.call();

                    setFilters();

                    saveAlerts(response.body().getAlerts());
                    saveColorCodes(response.body().getColorCodes());
                } else {
                    if (response.body().getError().contains("rights")) {
                        logoutWithError();
                    }
                }
            }
            @Override
            public void onFailure(Call<EmergencyResponse> call, Throwable t) {
                if (showLoadingUI) {
                    dataLoading.set(false);
                }
                loadAlerts();
                loadColorCodes();
                showSnackbarMessage(R.string.app_no_internet_connection);
            }
        });
    }

    void setFilters(){

        alerts.clear();
        for (Alert alert : allAlerts) {
            if (alert.getTitle().toLowerCase().contains(currentSearchFilter)
                    && alert.compareColorCode(currentColorFilter)){
                alerts.add(alert);
            }
        }

        Collections.sort(alerts, new Comparator<Alert>(){
            @Override
            public int compare(final Alert object1, final Alert object2) {
                return object1.getTitle().compareTo(object2.getTitle());
            }
        });


        Collections.sort(colorCodes, new Comparator<ColorCode>(){
            @Override
            public int compare(final ColorCode object1, final ColorCode object2) {
                return object1.getTitle().compareTo(object2.getTitle());
            }
        });

        empty.set(alerts.isEmpty());
        emptyColors.set(colorCodes.isEmpty());
    }

    void checkServerAvailabilityAndSendAlert(final Alert alert){
        progressVisible.set(true);
        final Retrofit retrofit = new Retrofit.Builder().client(new OkHttpClient.Builder()
                .connectTimeout(TIMEOUT_VALUE, TimeUnit.SECONDS)
                .writeTimeout(TIMEOUT_VALUE, TimeUnit.SECONDS)
                .readTimeout(TIMEOUT_VALUE+10, TimeUnit.SECONDS)
                .build()).baseUrl(hostname.get()).
            addConverterFactory(ScalarsConverterFactory.create()).addConverterFactory(GsonConverterFactory.create()).build();

        final DeskalertsService deskalertsService = retrofit.create(DeskalertsService.class);

        deskalertsService.getEmergencyAlertsAndColorCodes(login.get(),
                SharedPreferencesHelper.getPassword()).enqueue(new Callback<EmergencyResponse>() {
              @Override
              public void onResponse(Call<EmergencyResponse> call, Response<EmergencyResponse> response) {
                  if (response.body().getError() == null){
                      sendAlert(alert, deskalertsService);
                  } else {
                      if (response.body().getError().contains("rights")){
                          logoutWithError();
                      }
                  }
              }

              @Override
              public void onFailure(Call<EmergencyResponse> call, Throwable t) {
                  showSnackbarMessage(R.string.alerts_server_not_available);
              }
          });
    }

    SnackbarMessage getSnackbarMessage() {
        return mSnackbarText;
    }

    private void showSnackbarMessage(@StringRes Integer message) {
        progressVisible.set(false);
        mSnackbarText.setValue(message);
    }

    private void logoutWithError(){
        showSnackbarMessage(R.string.setup_publisher_right_error_text);
        logout();
        logoutEvent.call();
    }

    private void sendAlert(Alert alert, DeskalertsService deskalertsService){

        String apiKey = SharedPreferencesHelper.getApiKey();
        @SuppressLint("DefaultLocale") String xmlData = String.format("<darequest><alert><instant><alertId>%d</alertId></instant></alert></darequest>", alert.getAlertId());

        deskalertsService.sendAlert(xmlData, apiKey).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Log.d("ALERT SENT RESPONSE", response.body());
                if (response.body().contains("<result>SUCCESS</result>")){
                    showSnackbarMessage(R.string.alerts_alert_sent);
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d("ALERT RESPONSE error", t.getMessage());
                showSnackbarMessage(R.string.alerts_server_not_available);
            }
        });
    }
}
