package com.toolbarstudio.deskalerts.emergency.data.local;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.toolbarstudio.deskalerts.emergency.data.entities.ColorCode;

import java.util.List;

/**
 * Created by himselfego on 13.02.2018.
 */

@Dao
public interface ColorCodesDao {
    @Query("SELECT * FROM color_codes ORDER BY title")
    List<ColorCode> getColorCodes();

    @Query("SELECT * FROM color_codes WHERE entity_id = :colorCodeId ORDER BY title")
    ColorCode getColorCodeById(int colorCodeId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertColorCode(ColorCode colorCode);

    @Update
    int updateColorCode(ColorCode colorCode);

    @Query("DELETE FROM color_codes WHERE entity_id = :colorCodeId")
    int deleteColorCodeById(int colorCodeId);

    @Query("DELETE FROM color_codes")
    void deleteColorCodes();
}
