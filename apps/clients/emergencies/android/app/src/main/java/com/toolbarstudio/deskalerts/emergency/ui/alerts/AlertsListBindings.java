package com.toolbarstudio.deskalerts.emergency.ui.alerts;

import android.databinding.BindingAdapter;
import android.widget.ListView;

import com.toolbarstudio.deskalerts.emergency.data.entities.Alert;

import java.util.List;

/**
 * Created by maxsh on 18.02.2018.
 */

/**
 * Contains {@link BindingAdapter}s for the {@link Alert} list.
 */
public class AlertsListBindings {

    @BindingAdapter("app:items")
    public static void setItems(ListView listView, List<Alert> items) {
        AlertsAdapter adapter = (AlertsAdapter) listView.getAdapter();
        if (adapter != null)
        {
            adapter.replaceData(items);
        }
    }
}
