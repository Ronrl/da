package com.toolbarstudio.deskalerts.emergency.ui.alerts;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.toolbarstudio.deskalerts.emergency.data.entities.Alert;
import com.toolbarstudio.deskalerts.emergency.databinding.ItemAlertBinding;

import java.util.List;

/**
 * Created by himselfego on 12.02.2018.
 */

public class AlertsAdapter extends BaseAdapter  {

    private AlertsViewModel mAlertsViewModel;

    private List<Alert> mAlerts;

    private AlertItemUserActionsListener itemUserActionsListener;

    public AlertsAdapter(List<Alert> alerts, AlertsViewModel alertsViewModel, AlertItemUserActionsListener userActionsListener){

        mAlertsViewModel = alertsViewModel;
        itemUserActionsListener = userActionsListener;
        setList(alerts);
        instance = this;
    }

    public static AlertsAdapter instance;

    public void replaceData(List<Alert> alerts) {
        setList(alerts);
    }

    @Override
    public int getCount() {
        return mAlerts != null ? mAlerts.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        return mAlerts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ItemAlertBinding binding;
        if (convertView == null){
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());

            // Create the binding
            binding = ItemAlertBinding.inflate(inflater, parent, false);
        } else {
            // Recycling view
            binding = DataBindingUtil.getBinding(convertView);
        }

        binding.setAlert(mAlerts.get(position));

        binding.setListener(itemUserActionsListener);

        binding.executePendingBindings();

        return binding.getRoot();
    }

    void setSearchFilter(String searchFilterText){
        mAlertsViewModel.currentSearchFilter = searchFilterText;
        mAlertsViewModel.setFilters();
    }

    void setColorFilter(String colorText){
        if (colorText.equals("#FFFFFF")) colorText = "";
        mAlertsViewModel.currentColorFilter = colorText;
        mAlertsViewModel.setFilters();
    }

    private void setList(List<Alert> alerts) {
        mAlerts = alerts;
        notifyDataSetChanged();
    }
}

