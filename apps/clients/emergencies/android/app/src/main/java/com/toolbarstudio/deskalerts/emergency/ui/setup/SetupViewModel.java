package com.toolbarstudio.deskalerts.emergency.ui.setup;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;

import com.stepstone.stepper.StepperLayout;
import com.toolbarstudio.deskalerts.emergency.R;
import com.toolbarstudio.deskalerts.emergency.core.App;
import com.toolbarstudio.deskalerts.emergency.data.dtos.EmergencyResponse;
import com.toolbarstudio.deskalerts.emergency.data.remote.DeskalertsService;
import com.toolbarstudio.deskalerts.emergency.data.sharedpreferences.SharedPreferencesHelper;

import java.net.SocketTimeoutException;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static com.toolbarstudio.deskalerts.emergency.core.App.TIMEOUT_VALUE;

/**
 * Created by himselfego on 10.02.2018.
 */

public class SetupViewModel extends AndroidViewModel {
    public final ObservableField<String> hostname = new ObservableField<String>(getHost());
    public final ObservableField<String> login = new ObservableField<String>();
    public final ObservableField<String> password = new ObservableField<String>();
    public final ObservableBoolean progressVisible = new ObservableBoolean();

    public SetupViewModel(@NonNull Application application) {
        super(application);
    }

    private String getHost(){
        return SharedPreferencesHelper.getHostUrl().isEmpty() ? "demo.deskalerts.com" : SharedPreferencesHelper.getHostUrl();
    }

    public void PingDeskalertsServer(final StepperLayout.OnNextClickedCallback callback, final TextInputLayout textInputLayout) {
        progressVisible.set(true);

        String baseUrl;
        if (!(hostname.get().contains("http://") || hostname.get().contains("https://"))) {
            baseUrl = "http://" + hostname.get();
        } else {
            baseUrl = hostname.get();
        }
        if (!baseUrl.endsWith("/")) {
            baseUrl += "/";
        }

        final Retrofit retrofit = new Retrofit.Builder().client(new OkHttpClient.Builder()
                .connectTimeout(TIMEOUT_VALUE, TimeUnit.SECONDS)
                .writeTimeout(TIMEOUT_VALUE, TimeUnit.SECONDS)
                .readTimeout(TIMEOUT_VALUE+10, TimeUnit.SECONDS)
                .build()).baseUrl(baseUrl).addConverterFactory(ScalarsConverterFactory.create()).addConverterFactory(GsonConverterFactory.create()).build();
        final DeskalertsService deskalertsService = retrofit.create(DeskalertsService.class);

        try {
            deskalertsService.pingServer().enqueue(new Callback<String>() {
                @Override
                public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                   if (response.errorBody() == null){
                       progressVisible.set(false);
                       callback.goToNextStep();
                   } else {
                       String errorString = App.getContext().getString(R.string.setup_hostname_invalid_error_text);
                       textInputLayout.setError(errorString);
                       progressVisible.set(false);
                   }
                }

                @Override
                public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                    boolean isTimeout =  t.getClass().getName().contains("Timeout");
                    String errorString = App.getInstance().isInternetAvailable() && !isTimeout ?
                            App.getContext().getString(R.string.setup_hostname_invalid_error_text) :
                            App.getContext().getString(R.string.app_no_internet_connection);
                    textInputLayout.setError(errorString);

                    progressVisible.set(false);
                }
            });
        } catch (Exception e) {

        }

    }

    public void LoginOrRegister(final StepperLayout.OnCompleteClickedCallback callback, final TextInputLayout loginTextInputLayout, final TextInputLayout passwordTextInputLayout) {
        progressVisible.set(true);

        String baseUrl;
        if (!(hostname.get().contains("http://") || hostname.get().contains("https://"))) {
            baseUrl = "http://" + hostname.get();
        } else {
            baseUrl = hostname.get();
        }
        if (!baseUrl.endsWith("/")) {
            baseUrl += "/";
        }

        Retrofit retrofit = new Retrofit.Builder().client(new OkHttpClient.Builder()
                .connectTimeout(TIMEOUT_VALUE, TimeUnit.SECONDS)
                .writeTimeout(TIMEOUT_VALUE, TimeUnit.SECONDS)
                .readTimeout(TIMEOUT_VALUE+10, TimeUnit.SECONDS)
                .build()).baseUrl(baseUrl).addConverterFactory(ScalarsConverterFactory.create()).addConverterFactory(GsonConverterFactory.create()).build();
        DeskalertsService deskalertsService = retrofit.create(DeskalertsService.class);

        final String login = this.login.get();
        final String password = this.password.get();
        final String guid = "%7B" + UUID.randomUUID().toString().toUpperCase() + "%7D";
        final String clientId = "3";
        final String finalBaseUrl = baseUrl;

        deskalertsService.getEmergencyAlertsAndColorCodes(login, password).enqueue(new Callback<EmergencyResponse>() {
            @Override
            public void onResponse(Call<EmergencyResponse> call, Response<EmergencyResponse> response) {
                progressVisible.set(false);
                if (response.body().getError() == null) {
                    SharedPreferencesHelper.setHostUrl(finalBaseUrl);
                    SharedPreferencesHelper.setLogin(login);
                    SharedPreferencesHelper.setPassword(password);
                    SharedPreferencesHelper.setGuid(guid);
                    SharedPreferencesHelper.setApiKey(response.body().getApiKey());
                    callback.complete();
                } else {
                    if (response.body().getError().contains("rights")){
                        passwordTextInputLayout.setError(App.getContext().getString(R.string.setup_publisher_right_error_text));
                    } else if (response.body().getError().contains("superadmin")){
                        passwordTextInputLayout.setError(App.getContext().getString(R.string.setup_superadmin_error_text));
                    } else {
                        loginTextInputLayout.setError(App.getContext().getString(R.string.setup_login_invalid_error_text));
                        passwordTextInputLayout.setError(App.getContext().getString(R.string.setup_password_invalid_error_text));
                    }
                }
            }

            @Override
            public void onFailure(Call<EmergencyResponse> call, Throwable t) {
                progressVisible.set(false);
                if (App.getInstance().isInternetAvailable()){
                    loginTextInputLayout.setError(App.getContext().getString(R.string.setup_login_invalid_error_text));
                    passwordTextInputLayout.setError(App.getContext().getString(R.string.setup_password_invalid_error_text));
                }
                else{
                    loginTextInputLayout.setError(App.getContext().getString(R.string.app_no_internet_connection));
                }
            }
        });
    }
}
