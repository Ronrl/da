package com.toolbarstudio.deskalerts.emergency.ui.setup;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;
import com.toolbarstudio.deskalerts.emergency.R;
import com.toolbarstudio.deskalerts.emergency.databinding.FragmentHostBinding;

import java.util.Objects;

/**
 * Created by himselfego on 10.02.2018.
 */

public class HostFragment extends Fragment implements BlockingStep {
    private SetupViewModel viewModel;
    private FragmentHostBinding fragmentHostBinding;
    private TextInputLayout textInputLayout;

    public HostFragment() {
        // It's ok
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View root = inflater.inflate(R.layout.fragment_host, container, false);
        if (fragmentHostBinding == null) {
            fragmentHostBinding = FragmentHostBinding.bind(root);
        }

        viewModel = SetupActivity.obtainViewModel(getActivity());

        fragmentHostBinding.setViewmodel(viewModel);

        setHasOptionsMenu(true);
        setRetainInstance(false);

        return fragmentHostBinding.getRoot();
    }

    @Nullable
    @Override
    public VerificationError verifyStep() {
        if (textInputLayout == null) {
            textInputLayout = getView().findViewById(R.id.host_fragment_hostname_text_input_layout);
        }
        if (viewModel.hostname.get() == null || Objects.equals(viewModel.hostname.get(), "")) {
            return new VerificationError(getString(R.string.setup_enter_hostname));
        }
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {
        textInputLayout.setError(error.getErrorMessage());
    }

    @Override
    public void onNextClicked(StepperLayout.OnNextClickedCallback callback) {
        textInputLayout.setError("");
        viewModel.PingDeskalertsServer(callback, textInputLayout);
    }

    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {

    }

    @Override
    public void onBackClicked(StepperLayout.OnBackClickedCallback callback) {

    }
}
