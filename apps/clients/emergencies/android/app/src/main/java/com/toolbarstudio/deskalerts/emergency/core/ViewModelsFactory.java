package com.toolbarstudio.deskalerts.emergency.core;

import android.annotation.SuppressLint;
import android.app.Application;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.toolbarstudio.deskalerts.emergency.ui.alerts.AlertsViewModel;
import com.toolbarstudio.deskalerts.emergency.ui.setup.SetupViewModel;

/**
 * Created by himselfego on 11.02.2018.
 */

public class ViewModelsFactory extends ViewModelProvider.NewInstanceFactory {
    @SuppressLint("StaticFieldLeak")
    private static volatile ViewModelsFactory INSTANCE;

    private final Application application;

    private ViewModelsFactory(Application application) {
        this.application = application;
    }

    public static ViewModelsFactory getInstance(Application application) {

        if (INSTANCE == null) {
            synchronized (ViewModelsFactory.class) {
                if (INSTANCE == null) {
                    INSTANCE = new ViewModelsFactory(application);
                }
            }
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        if (modelClass.isAssignableFrom(SetupViewModel.class)) {
            //noinspection unchecked
            return (T) new SetupViewModel(application);
        } else if (modelClass.isAssignableFrom(AlertsViewModel.class)) {
            //noinspection unchecked
            return (T) new AlertsViewModel(application);
        }
        throw new IllegalArgumentException("Unknown ViewModel class: " + modelClass.getName());
    }
}
