package com.toolbarstudio.deskalerts.emergency.ui.alerts;

import com.toolbarstudio.deskalerts.emergency.data.entities.ColorCode;

/**
 * Created by maxsh on 20.02.2018.
 */

public interface ColorCodeItemUserActionsListener {

    void onCodeClicked(ColorCode code);
}
