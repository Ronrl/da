package com.toolbarstudio.deskalerts.emergency.ui.alerts;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.toolbarstudio.deskalerts.emergency.data.entities.ColorCode;
import com.toolbarstudio.deskalerts.emergency.databinding.ItemColorCodeBinding;

import java.util.List;

/**
 * Created by himselfego on 12.02.2018.
 */

public class ColorCodesAdapter extends BaseAdapter {

    private AlertsViewModel viewModel;

    private List<ColorCode> colorCodes;

    private ColorCodeItemUserActionsListener userActionsListener;

    public static ColorCodesAdapter instance;

    public ColorCodesAdapter(List<ColorCode> codes, AlertsViewModel alertsViewModel, ColorCodeItemUserActionsListener actionsListener){

        viewModel = alertsViewModel;
        setList(codes);
        instance = this;
        userActionsListener = actionsListener;
    }

    @Override
    public int getCount() {
        return colorCodes != null ? colorCodes.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        return colorCodes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ItemColorCodeBinding binding;
        if (convertView == null){
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());

            // Create the binding
            binding = ItemColorCodeBinding.inflate(inflater, parent, false);
        } else {
            // Recycling view
            binding = DataBindingUtil.getBinding(convertView);
        }

        binding.setCode(colorCodes.get(position));

        binding.setListener(userActionsListener);

        binding.executePendingBindings();

        return binding.getRoot();
    }

    public void replaceData(List<ColorCode> colorCodes) {
        setList(colorCodes);
    }

    private void setList(List<ColorCode> colorCodes1) {
        colorCodes = colorCodes1;
        notifyDataSetChanged();
    }
}
