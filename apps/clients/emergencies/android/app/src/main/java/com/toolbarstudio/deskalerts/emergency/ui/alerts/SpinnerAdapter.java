package com.toolbarstudio.deskalerts.emergency.ui.alerts;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.toolbarstudio.deskalerts.emergency.data.entities.ColorCode;
import com.toolbarstudio.deskalerts.emergency.databinding.SpinnerItemBinding;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by maxsh on 23.02.2018.
 */

public class SpinnerAdapter extends BaseAdapter {

    private final AlertsViewModel viewModel;
    private List<ColorCode> colorCodes;

    public static SpinnerAdapter instance;

    SpinnerAdapter(List<ColorCode> codes, AlertsViewModel alertsViewModel) {

        viewModel = alertsViewModel;
        setList(codes);
        instance = this;
    }

    @Override
    public int getCount() {
        return colorCodes != null ? colorCodes.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        return colorCodes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        SpinnerItemBinding binding;
        if (convertView == null){
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());

            // Create the binding
            binding = SpinnerItemBinding.inflate(inflater, parent, false);
        } else {
            // Recycling view
            binding = DataBindingUtil.getBinding(convertView);
        }

        binding.setCode(colorCodes.get(position));
        binding.executePendingBindings();

        return binding.getRoot();
    }

    public void setList(List<ColorCode> list) {
        colorCodes = new ArrayList<>();
        colorCodes.addAll(list);
        colorCodes.add(0, new ColorCode(0, "All colors", "#FFFFFF"));
    }
}
