package com.toolbarstudio.deskalerts.emergency.ui.setup;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;
import com.toolbarstudio.deskalerts.emergency.R;
import com.toolbarstudio.deskalerts.emergency.core.ViewModelsFactory;
import com.toolbarstudio.deskalerts.emergency.ui.alerts.AlertsActivity;

/**
 * Created by himselfego on 10.02.2018.
 */

public class SetupActivity extends AppCompatActivity {
    private StepperLayout stepperLayout;
    private SetupViewModel viewModel;

    public static SetupViewModel obtainViewModel(FragmentActivity activity) {
        ViewModelsFactory factory = ViewModelsFactory.getInstance(activity.getApplication());
        return ViewModelProviders.of(activity, factory).get(SetupViewModel.class);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup);
        viewModel = ViewModelProviders.of(this).get(SetupViewModel.class);
        setupStepper();
    }

    private void setupStepper() {
        stepperLayout = findViewById(R.id.setup_activity_stepper_layout);
        stepperLayout.setAdapter(new SetupFragmentsAdapter(getSupportFragmentManager(), this));
        stepperLayout.setListener(new StepperLayout.StepperListener() {
            @Override
            public void onCompleted(View completeButton) {
                Intent intent = new Intent(getApplicationContext(), AlertsActivity.class);
                startActivity(intent);
            }

            @Override
            public void onError(VerificationError verificationError) {

            }

            @Override
            public void onStepSelected(int newStepPosition) {

            }

            @Override
            public void onReturn() {

            }
        });
    }
}
