package com.toolbarstudio.deskalerts.emergency.core;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by himselfego on 12.02.2018.
 */

public class App extends Application {
    @SuppressLint("StaticFieldLeak")
    private static Context context;
    private static App instance = new App();

    public static Context getContext() {
        return context;
    }

    public static App getInstance() {
        return instance;
    }

    public static int TIMEOUT_VALUE = 15;

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
    }

    public boolean isInternetAvailable(){
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}
