package com.toolbarstudio.deskalerts.emergency.data.sharedpreferences;

import android.content.Context;
import android.content.SharedPreferences;

import com.toolbarstudio.deskalerts.emergency.core.App;

/**
 * Created by himselfego on 12.02.2018.
 */

public class SharedPreferencesHelper {
    private static String USER_PREFERENCES_NAME = "USER_PREFERENCES";
    private static String LOGIN_KEY = "LOGIN";
    private static String PASSWORD_KEY = "PASSWORD";
    private static String HOST_URL_KEY = "HOST_URL";
    private static String GUID_KEY = "GUID";
    private static String API_KEY = "API_KEY";

    public static String getLogin() {
        SharedPreferences sharedPreferences = App.getContext().getSharedPreferences(USER_PREFERENCES_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(LOGIN_KEY, "");
    }

    public static void setLogin(String login) {
        SharedPreferences sharedPreferences = App.getContext().getSharedPreferences(USER_PREFERENCES_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(LOGIN_KEY, login);
        editor.apply();
    }

    public static String getPassword() {
        SharedPreferences sharedPreferences = App.getContext().getSharedPreferences(USER_PREFERENCES_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(PASSWORD_KEY, "");
    }

    public static void setPassword(String password) {
        SharedPreferences sharedPreferences = App.getContext().getSharedPreferences(USER_PREFERENCES_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PASSWORD_KEY, password);
        editor.apply();
    }

    public static String getHostUrl() {
        SharedPreferences sharedPreferences = App.getContext().getSharedPreferences(USER_PREFERENCES_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(HOST_URL_KEY, "");
    }

    public static void setHostUrl(String hostUrl) {
        SharedPreferences sharedPreferences = App.getContext().getSharedPreferences(USER_PREFERENCES_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(HOST_URL_KEY, hostUrl);
        editor.apply();
    }

    public static String getGuid() {
        SharedPreferences sharedPreferences = App.getContext().getSharedPreferences(USER_PREFERENCES_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(GUID_KEY, "");
    }

    public static void setGuid(String guid) {
        SharedPreferences sharedPreferences = App.getContext().getSharedPreferences(USER_PREFERENCES_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(GUID_KEY, guid);
        editor.apply();
    }

    public static String getApiKey(){
        SharedPreferences sharedPreferences = App.getContext().getSharedPreferences(USER_PREFERENCES_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(API_KEY, "");
    }

    public static void setApiKey(String apiKey) {
        SharedPreferences sharedPreferences = App.getContext().getSharedPreferences(USER_PREFERENCES_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(API_KEY, apiKey);
        editor.apply();
    }

    public static void clear() {
        SharedPreferences sharedPreferences = App.getContext().getSharedPreferences(USER_PREFERENCES_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(LOGIN_KEY, "");
        editor.putString(PASSWORD_KEY, "");
        editor.putString(GUID_KEY, "");
        editor.putString(API_KEY, "");
        editor.apply();
    }
}
