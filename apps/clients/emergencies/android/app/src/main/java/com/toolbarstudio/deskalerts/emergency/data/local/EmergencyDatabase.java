package com.toolbarstudio.deskalerts.emergency.data.local;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.toolbarstudio.deskalerts.emergency.data.entities.Alert;
import com.toolbarstudio.deskalerts.emergency.data.entities.ColorCode;

/**
 * Created by himselfego on 13.02.2018.
 */

@Database(entities = {Alert.class, ColorCode.class}, version = 1, exportSchema = false)
public abstract class EmergencyDatabase extends RoomDatabase {

    private static final Object lock = new Object();

    private static EmergencyDatabase INSTANCE;

    public abstract AlertsDao alertsDao();

    public abstract ColorCodesDao codesDao();

    public static EmergencyDatabase getInMemoryInstance(Context context) {
        synchronized (lock){
            if (INSTANCE == null) {
                INSTANCE = Room.inMemoryDatabaseBuilder(context.getApplicationContext(), EmergencyDatabase.class).build();
            }
            return INSTANCE;
        }
    }

    public static EmergencyDatabase getFileInstance(Context context) {
        synchronized (lock) {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(context.getApplicationContext(), EmergencyDatabase.class, "da-emergency.db").build();
            }
            return INSTANCE;
        }
    }

    public static void destroyInstance(){
        INSTANCE = null;
    }

}
