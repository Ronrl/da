package com.toolbarstudio.deskalerts.emergency.data.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.databinding.BaseObservable;
import android.graphics.Color;
import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

/**
 * Created by himselfego on 13.02.2018.
 */

@Entity(tableName = "alerts")
public final class Alert {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "entity_id")
    private final int entityId;

    @SerializedName("id")
    @ColumnInfo(name = "alert_id")
    private final int alertId;

    @Nullable
    @ColumnInfo(name = "title")
    private final String title;

    @Nullable
    @SerializedName("color")
    @ColumnInfo(name = "color_code")
    private final String colorCode;

    public Alert(int entityId, int alertId, @Nullable String title, @Nullable String colorCode) {
        this.entityId = entityId;
        this.alertId = alertId;
        this.title = title;
        this.colorCode = colorCode;
    }

    public int getEntityId() {
        return entityId;
    }

    public int getAlertId() {
        return alertId;
    }

    @Nullable
    public String getTitle() {
        return title;
    }

    @Nullable
    public String getColorCode() {
        return colorCode;
    }

    public boolean equals(Alert alert) {
        return this.alertId == alert.alertId &&
                Objects.equals(this.title, alert.title) &&
                Objects.equals(this.colorCode, alert.colorCode);
    }

    public boolean compareColorCode(String codeHex){
        return Objects.equals(codeHex, "") || Objects.equals(this.colorCode, codeHex);
    }

    public int colorCodeInt() {
        return Color.parseColor(this.colorCode);
    }
}
