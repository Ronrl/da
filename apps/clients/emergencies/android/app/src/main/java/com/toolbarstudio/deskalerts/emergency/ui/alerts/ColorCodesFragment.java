package com.toolbarstudio.deskalerts.emergency.ui.alerts;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ListView;
import android.widget.Spinner;

import com.toolbarstudio.deskalerts.emergency.R;
import com.toolbarstudio.deskalerts.emergency.core.ScrollChildSwipeRefreshLayout;
import com.toolbarstudio.deskalerts.emergency.core.SnackbarMessage;
import com.toolbarstudio.deskalerts.emergency.core.SnackbarUtils;
import com.toolbarstudio.deskalerts.emergency.data.entities.ColorCode;
import com.toolbarstudio.deskalerts.emergency.databinding.FragmentColorCodesBinding;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by himselfego on 12.02.2018.
 */

public class ColorCodesFragment extends Fragment {

    private AlertsViewModel viewModel;
    private FragmentColorCodesBinding fragmentBinding;
    private ColorCodesAdapter adapter;

    private static ColorCodesFragment instance;

    public ColorCodesFragment() {
        // It's ok
    }

    public static ColorCodesFragment getInstance() {
        if (instance == null){
            instance = new ColorCodesFragment();
        }
        return instance;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        fragmentBinding = FragmentColorCodesBinding.inflate(inflater, container, false);
        viewModel = AlertsActivity.obtainViewModel(getActivity());
        fragmentBinding.setViewmodel(viewModel);
        setHasOptionsMenu(false);
        return fragmentBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setupSnackbar();
        setupListAdapter();
        setupRefreshLayout();
    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.start();
    }

    private void setupSnackbar() {
        viewModel.getSnackbarMessage().observe(this, new SnackbarMessage.SnackbarObserver() {
            @Override
            public void onNewMessage(@StringRes int snackbarMessageResourceId) {
                SnackbarUtils.showSnackbar(getView(), getString(snackbarMessageResourceId));
            }
        });
    }

    private void setupListAdapter() {
        ListView listView =  fragmentBinding.colorCodesList;
        adapter = new ColorCodesAdapter(new ArrayList<ColorCode>(0), viewModel, new ColorCodeItemUserActionsListener() {
            @Override
            public void onCodeClicked(ColorCode code) {
                viewModel.currentColorFilter = code.getHex();
                ((AlertsActivity)getActivity()).replaceFragments();
                NavigationView navigationView = getActivity().findViewById(R.id.alerts_activity_navigation_view);
                navigationView.getMenu().getItem(0).setChecked(true);
                navigationView.getMenu().getItem(1).setChecked(false);
            }
        });
        listView.setAdapter(adapter);
    }

    private List<ColorCode> retrieveAllItems(Spinner spinner) {
        Adapter adapter = spinner.getAdapter();
        int n = adapter.getCount();
        List<ColorCode> codes = new ArrayList<>(n);
        for (int i = 0; i < n; i++) {
            ColorCode user = (ColorCode) adapter.getItem(i);
            codes.add(user);
        }
        return codes;
    }

    private void setupRefreshLayout() {
        ListView listView =  fragmentBinding.colorCodesList;
        final ScrollChildSwipeRefreshLayout swipeRefreshLayout = fragmentBinding.refreshLayout;
        swipeRefreshLayout.setColorSchemeColors(
                ContextCompat.getColor(getActivity(), R.color.colorPrimary),
                ContextCompat.getColor(getActivity(), R.color.colorAccent),
                ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark)
        );
        // Set the scrolling view in the custom SwipeRefreshLayout.
        swipeRefreshLayout.setScrollUpChild(listView);
    }
}


