//
//  SearchAlertViewController.m
//  Deskalerts Instants
//
//  Created by Дмитрий Кайгородов on 24.09.15.
//  Copyright (c) 2015 Toolbar Inc. All rights reserved.
//

#import "SearchAlertViewController.h"
#import "AlertViewCell.h"


@interface SearchAlertViewController ()

@end

@implementation SearchAlertViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSUInteger) supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [self.result count];
}


- (UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}



- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AlertViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"AlertCell" forIndexPath:indexPath];
    NSManagedObject *record = [self.result objectAtIndex:indexPath.row];
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.title.text = [record valueForKey:@"title"];
    NSString* hexColor = [record valueForKey:@"color_code"];
    
    cell.colorCode.backgroundColor = [self colorFromHexString:hexColor];
    
    cell.alertId = [[record valueForKey:@"id"] intValue];
    
    return cell;
    
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSManagedObject *record = [self.result objectAtIndex:indexPath.row];
    
   int alertId  = [[record valueForKey:@"id"] intValue];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"sendAlert" object:[NSNumber numberWithInt:alertId]];
    
    
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
