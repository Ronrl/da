//
//  SettingsViewController.m
//  Deskalerts Instants
//
//  Created by Дмитрий Кайгородов on 12.10.15.
//  Copyright © 2015 Toolbar Inc. All rights reserved.
//

#import "SettingsViewController.h"
#import "UserSettings.h"
#import "DeskAlertServer.h"
#import "AppDelegate.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController
- (IBAction)onFieldEdited:(id)sender {
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    

    //[self.soundSwitch setOn:[UserSettings sharedInstance].useSound];
    // NSLog(@"VIEW DID LOAD");
    // Do any additional setup after loading the view.
    
    // [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onUnregisterPushNotifications) name:@"unregistered" object:nil];
    self.progressBar = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:self.progressBar];
    self.progressBar.delegate = self;
    self.progressBar.labelText = @"Processing...";

}

- (void) viewDidAppear:(BOOL)animated
{
    self.hostName.text = [UserSettings sharedInstance].hostName;
    self.login.text = [UserSettings sharedInstance].login;
    self.password.text = [UserSettings sharedInstance].password;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) saveUserSettings
{
    [UserSettings sharedInstance].hostName= self.hostName.text;
    [UserSettings sharedInstance].login = self.login.text;
    [UserSettings sharedInstance].password = self.password.text;

    [UserSettings saveUserSettings];
}


- (IBAction)saveClicked:(id)sender {
    DeskAlertServer *server = [DeskAlertServer defaultServer];
    
    if(![server checkInternetConnection:YES])
        return;
    
    NSData*  __block data;
    [self.progressBar showAnimated:YES whileExecutingBlock:^{
        
        data = [server getAllDataForUser:self.login.text andPassword:self.password.text onServer:self.hostName.text];
        
        NSError *error = nil;
        NSMutableDictionary* jsonData = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
        
        NSString* errorMsg = [jsonData objectForKey:@"error"];
        
        if(errorMsg)
        {
            //  NSString* errorMessage = [errors objectAtIndex:0];
            dispatch_sync(dispatch_get_main_queue(), ^{
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:errorMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
                
            });
            return;
        }
        NSArray* alerts =  [jsonData objectForKey:@"alerts"];
        NSArray* colorCodes = [jsonData objectForKey:@"color_codes"];
        NSString* apiKey = [jsonData objectForKey:@"api_key"];
        
        NSManagedObjectContext* context = [AppDelegate sharedInstance].managedObjectContext;
        
        NSFetchRequest * fetch = [[NSFetchRequest alloc] init];
        [fetch setEntity:[NSEntityDescription entityForName:@"Alert" inManagedObjectContext:context]];
        NSArray * result = [context executeFetchRequest:fetch error:nil];
        for (id basket in result)
            [context deleteObject:basket];
        
        
        NSFetchRequest* fetchColors = [[NSFetchRequest alloc] init];
        [fetchColors setEntity:[NSEntityDescription entityForName:@"ColorCode" inManagedObjectContext:context]];
        NSArray* resultColors = [context executeFetchRequest:fetchColors error:nil];
        for (id basket in resultColors)
            [context deleteObject:basket];
        
        
        for(NSDictionary* alertDictionary in alerts)
        {
            
            NSEntityDescription *entity = [NSEntityDescription entityForName:@"Alert" inManagedObjectContext:context];
            NSManagedObject *alertToAdd = [[NSManagedObject alloc] initWithEntity:entity insertIntoManagedObjectContext:context];
            
            [alertToAdd setValue:[alertDictionary objectForKey:@"id"] forKey:@"id"];
            [alertToAdd setValue:[alertDictionary objectForKey:@"title"] forKey:@"title"];
            [alertToAdd setValue:[alertDictionary objectForKey:@"color"] forKey:@"color_code"];
            
        }
      

        
        
        for(NSDictionary* colorCodeDictionary in colorCodes)
        {
            
            
            NSEntityDescription *entity = [NSEntityDescription entityForName:@"ColorCode" inManagedObjectContext:context];
            NSManagedObject *colorToAdd = [[NSManagedObject alloc] initWithEntity:entity insertIntoManagedObjectContext:context];
            
            [colorToAdd setValue:[colorCodeDictionary objectForKey:@"title"] forKey:@"title"];
            [colorToAdd setValue:[colorCodeDictionary objectForKey:@"hex"] forKey:@"color_code"];
        }
        
        [UserSettings sharedInstance].hostName = self.hostName.text;
        [UserSettings sharedInstance].login = self.login.text;
        [UserSettings sharedInstance].password = self.password.text;
        [UserSettings sharedInstance].hasLaunchOnce = YES;
        [UserSettings sharedInstance].apiKey = apiKey;
        [UserSettings saveUserSettings];
        
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            
            self.saveButton.enabled = NO;
                        [self.view layoutIfNeeded];
            [self.tabBarController setSelectedIndex:0];

            
        });
        

        
    }];
    


    
}

-(IBAction)fieldChanged:(id)sender
{
    
    self.saveButton.enabled = self.hostName.text.length > 0 && self.login.text.length > 0 && self.password.text.length > 0;
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
