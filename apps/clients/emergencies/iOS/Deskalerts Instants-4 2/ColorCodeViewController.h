//
//  ColorCodeViewController.h
//  Deskalerts Instants
//
//  Created by Дмитрий Кайгородов on 23.09.15.
//  Copyright (c) 2015 Toolbar Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
// UISearchControllerDelegate,UISearchResultsUpdating
@interface ColorCodeViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate, UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

@end
