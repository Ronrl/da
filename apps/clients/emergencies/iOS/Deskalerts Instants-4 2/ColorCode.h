//
//  ColorCode.h
//  Deskalerts Instants
//
//  Created by Дмитрий Кайгородов on 23.09.15.
//  Copyright (c) 2015 Toolbar Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface ColorCode : NSManagedObject

@property (nonatomic, retain) NSString * color_code;
@property (nonatomic, retain) NSString * title;

@end
