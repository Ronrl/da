//
//  AlertViewCell.m
//  Deskalerts Instants
//
//  Created by Дмитрий Кайгородов on 24.09.15.
//  Copyright (c) 2015 Toolbar Inc. All rights reserved.
//

#import "AlertViewCell.h"

@implementation AlertViewCell

- (void)awakeFromNib {
    // Initialization code
}
- (IBAction)onSendAlertClick:(id)sender {

    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"sendAlert" object:[NSNumber numberWithInt:self.alertId]];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}




@end
