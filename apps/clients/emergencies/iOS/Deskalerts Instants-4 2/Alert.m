//
//  Alert.m
//  Deskalerts Instants
//
//  Created by Дмитрий Кайгородов on 23.09.15.
//  Copyright (c) 2015 Toolbar Inc. All rights reserved.
//

#import "Alert.h"


@implementation Alert

@dynamic color_code;
@dynamic id;
@dynamic title;

@end
