//
//  ColorCodeViewCell.h
//  Deskalerts Instants
//
//  Created by Дмитрий Кайгородов on 23.09.15.
//  Copyright (c) 2015 Toolbar Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ColorCodeViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *colorCodeTitle;
@property (weak, nonatomic) IBOutlet UILabel *colorLabel;

@end
