//
//  ViewController.m
//  Deskalert Mobile
//
//  Created by Дмитрий Кайгородов on 10.03.15.
//  Copyright (c) 2015 Deskalert. All rights reserved.
//

#import "FirstWelcomeView.h"
#import "UserSettings.h"

@interface FirstWelcomeView ()

@end

@implementation FirstWelcomeView
- (IBAction)onFieldEdited:(id)sender {

}
- (IBAction)nextClick:(id)sender {
    NSLog(@"OK 1");
    
    NSString* hostName = self.hostNameField.text;
    
    if(![hostName hasPrefix:@"http://"])
        hostName = [NSString stringWithFormat:@"http://%@", hostName];
    
    
    [self.progressBar showAnimated:YES whileExecutingBlock:^{
        
       if(![[DeskAlertServer defaultServer] isServerAvailable:hostName])
       {
            dispatch_sync(dispatch_get_main_queue(), ^{
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!" message:@"Server is unavailable" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
            });
       }
        else
        {
            dispatch_sync(dispatch_get_main_queue(), ^{
                [UserSettings sharedInstance].hostName = hostName;
                [self performSegueWithIdentifier:@"1to2" sender:self];
            });
        }
    }];

}






- (void) viewDidAppear:(BOOL)animated
{
    //[self onHostNameChanged:nil];

   // [super viewWillAppear:animated];
}
- (IBAction)onHostNameChanged:(id)sender {
    
    
    UITextField *textField = (UITextField*)sender;
    self.nextButton.enabled = [textField.text length] > 0;
}

- (NSUInteger) supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)viewDidLoad {
    
    if(![UserSettings sharedInstance].hasLaunchOnce)
    {
        [super viewDidLoad];
    
        self.progressBar = [[MBProgressHUD alloc] initWithView:self.view];
        [self.view addSubview:self.progressBar];
        self.progressBar.delegate = self;
        self.progressBar.labelText = @"Processing...";
    }
    
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
