//
//  AppDelegate.m
//  Deskalerts Instants
//
//  Created by Дмитрий Кайгородов on 17.07.15.
//  Copyright (c) 2015 Toolbar Inc. All rights reserved.
//

#import "AppDelegate.h"
#import "DeskAlertServer.h"


@interface AppDelegate ()

@end

@implementation AppDelegate

+ (instancetype) sharedInstance
{
    return (AppDelegate*)[[UIApplication sharedApplication] delegate];
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
   
    
    [UserSettings loadUserSettings];
    
    if([[UserSettings sharedInstance] hasLaunchOnce])
    {
        NSData*  data;

        
        DeskAlertServer *server = [DeskAlertServer defaultServer];
        data = [server getAllDataForUser:[UserSettings sharedInstance].login andPassword:[UserSettings sharedInstance].password onServer:[UserSettings sharedInstance].hostName];
        
        NSError *error = nil;
        NSMutableDictionary* jsonData = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
        
        NSArray* alerts =  [jsonData objectForKey:@"alerts"];
        NSArray* colorCodes = [jsonData objectForKey:@"color_codes"];
        
        
        NSManagedObjectContext* context = [AppDelegate sharedInstance].managedObjectContext;
        NSFetchRequest * fetch = [[NSFetchRequest alloc] init];
        [fetch setEntity:[NSEntityDescription entityForName:@"Alert" inManagedObjectContext:context]];
        NSArray * result = [context executeFetchRequest:fetch error:nil];
        for (id basket in result)
            [context deleteObject:basket];
        
        
        NSFetchRequest* fetchColors = [[NSFetchRequest alloc] init];
        [fetchColors setEntity:[NSEntityDescription entityForName:@"ColorCode" inManagedObjectContext:context]];
        NSArray* resultColors = [context executeFetchRequest:fetchColors error:nil];
        for (id basket in resultColors)
            [context deleteObject:basket];
        for(NSDictionary* alertDictionary in alerts)
        {
            
            NSEntityDescription *entity = [NSEntityDescription entityForName:@"Alert" inManagedObjectContext:context];
            NSManagedObject *alertToAdd = [[NSManagedObject alloc] initWithEntity:entity insertIntoManagedObjectContext:context];
            
            [alertToAdd setValue:[alertDictionary objectForKey:@"id"] forKey:@"id"];
            [alertToAdd setValue:[alertDictionary objectForKey:@"title"] forKey:@"title"];
            [alertToAdd setValue:[alertDictionary objectForKey:@"color"] forKey:@"color_code"];
            
        }
        
        
        for(NSDictionary* colorCodeDictionary in colorCodes)
        {
            
            
            NSEntityDescription *entity = [NSEntityDescription entityForName:@"ColorCode" inManagedObjectContext:context];
            NSManagedObject *colorToAdd = [[NSManagedObject alloc] initWithEntity:entity insertIntoManagedObjectContext:context];
            
            [colorToAdd setValue:[colorCodeDictionary objectForKey:@"title"] forKey:@"title"];
            [colorToAdd setValue:[colorCodeDictionary objectForKey:@"hex"] forKey:@"color_code"];
        }
        
        

                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                
                UIViewController *mainView = [storyboard instantiateViewControllerWithIdentifier:@"UITabBarController-0Et-o7-3DC"];
                
                [self.window setRootViewController:mainView];



    }
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "com.toolbarstudio.Deskalerts_Instants" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Deskalerts_Instants" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Deskalerts_Instants.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] init];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

@end
