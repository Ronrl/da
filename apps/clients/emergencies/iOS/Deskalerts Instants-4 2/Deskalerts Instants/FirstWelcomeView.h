//
//  ViewController.h
//  Deskalert Mobile
//
//  Created by Дмитрий Кайгородов on 10.03.15.
//  Copyright (c) 2015 Deskalert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "DeskAlertServer.h"

@interface FirstWelcomeView : UIViewController<MBProgressHUDDelegate>

@property MBProgressHUD *progressBar;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *nextButton;
@property (weak, nonatomic) IBOutlet UITextField *hostNameField;

@end

