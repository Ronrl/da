//
//  SettingsView.m
//  Deskalert Mobile
//
//  Created by Дмитрий Кайгородов on 11.03.15.
//  Copyright (c) 2015 Deskalert. All rights reserved.
//

#import "SettingsView.h"
#import "UserSettings.h"
#import "DeskAlertServer.h"

@interface SettingsView ()

@end

@implementation SettingsView

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.hostNameField.text = [UserSettings sharedInstance].hostName;
    self.loginField.text = [UserSettings sharedInstance].login;
    self.passWordField.text = [UserSettings sharedInstance].password;
    [self.soundSwitch setOn:[UserSettings sharedInstance].useSound];
   // NSLog(@"VIEW DID LOAD");
    // Do any additional setup after loading the view.
    
   // [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onUnregisterPushNotifications) name:@"unregistered" object:nil];
    self.progressBar = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:self.progressBar];
    self.progressBar.delegate = self;
    self.progressBar.labelText = @"Processing...";
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void) onUnregisterPushNotifications
{
    
}

- (IBAction)fieldChanged:(id)sender
{
    self.saveButton.enabled = [self canSave];
}

- (void) saveUserSettings
{
    [UserSettings sharedInstance].hostName= self.hostNameField.text;
    [UserSettings sharedInstance].login = self.loginField.text;
    [UserSettings sharedInstance].password = self.passWordField.text;
    [UserSettings sharedInstance].useSound = self.soundSwitch.isOn;
    [UserSettings saveUserSettings];
}


- (NSUInteger) supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}


- (void)goToBackToMenu
{
    [self performSegueWithIdentifier:@"backToMain" sender:self];
}

- (BOOL) loginDataChanged
{
    return ![[UserSettings sharedInstance].hostName isEqualToString: self.hostNameField.text ]|| ![[UserSettings sharedInstance].login  isEqualToString: self.loginField.text] ||   ![[UserSettings sharedInstance].password isEqualToString: self.passWordField.text];
}


- (IBAction)saveSettings:(id)sender
{

    
    if([self loginDataChanged])
    {
        //[[UIApplication sharedApplication] unregisterForRemoteNotifications];
        DeskAlertServer *server = [DeskAlertServer defaultServer];
        
        if(![server checkInternetConnection:YES])
            return;
        
        BOOL __block success;
        [self.progressBar showAnimated:YES whileExecutingBlock:^{
                success = [server validateOrRegisterUserWithLogin:self.loginField.text andPassword:self.passWordField.text onServerWithHostName:self.hostNameField.text];
             [server unregisterDevice];
            if(!success)
            {
                
                [self saveUserSettings];
                [server unregisterDevice];
                [server registerDeviceWithToken:[UserSettings sharedInstance].device_token];
                [self goToBackToMenu];
                
            }            
            else
            {
                
                dispatch_sync(dispatch_get_main_queue(), ^{
                    
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Your password is incorrect!"delegate:nil cancelButtonTitle:@"Try Again" otherButtonTitles:nil];
                    
                    [alert show];
                    
                });
                
            }

        }];
           
        return;
    }
 
    [self saveUserSettings];
    [self goToBackToMenu];
    
}



- (BOOL) canSave
{
    return [self.hostNameField.text length] > 0 && [self.loginField.text length] > 0 && [self.passWordField.text length] > 0;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
