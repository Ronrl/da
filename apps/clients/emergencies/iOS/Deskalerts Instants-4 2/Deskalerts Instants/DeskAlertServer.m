//
//  DeskAlertServer.m
//  Deskalert Mobile
//
//  Created by Дмитрий Кайгородов on 12.03.15.
//  Copyright (c) 2015 Deskalert. All rights reserved.
//

#import "DeskAlertServer.h"
#import "UserSettings.h"
#import "DeskAlertResponseDefines.h"
#import "TBXML.h"
#import "Reachability.h"
#import "NSString+HTML.h"
#import "AppDelegate.h"
#import "NSString+MD5.h"
#import "NSString+HTML.h"

@implementation DeskAlertServer

- (NSArray*) badSkinsId
{
    return [NSArray arrayWithObjects:@"{04f84380-b31d-4167-8b83-c6642e3d25a8}", @"{138f9281-0c11-4c48-983e-d4ca8076748b}", @"{c28f6977-3254-418c-b96e-bda43ce94ff4}", nil];
}

- (int) validateOrRegisterUserWithLogin:(NSString *)login andPassword:(NSString *)password onServerWithHostName:(NSString *)hostName
{

    
    
    NSString* serverHostName = [NSString stringWithFormat:@"%@/register.asp", hostName];
    NSString* queryString = [NSString stringWithFormat:@"uname=%@&upass=%@&desk_id=%@&client_id=3", login, password, [UserSettings sharedInstance].guid];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:serverHostName]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[queryString length]] forHTTPHeaderField:@"Content-length"];
    
    [request setHTTPBody:[queryString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSData* recievedData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    
    NSString* responseString = [[NSString alloc] initWithBytes:[recievedData bytes] length:[recievedData length] encoding:NSUTF8StringEncoding];
    
  /*
    BOOL ok = NO;
    if(responseString)
    {
      //  NSLog(@"%@",responseString);
        
        ok = [responseString rangeOfString:responseDeskAlertIncorrectLoginData].location == NSNotFound && ([responseString rangeOfString:responseDeskAlertRegistrationSuccess].location != NSNotFound ||
               [responseString rangeOfString:responseDeskAlertLoginSuccess].location != NSNotFound);
    }*/
    

    if([responseString rangeOfString:responseDeskAlertRegistrationSuccess].location != NSNotFound ||
       [responseString rangeOfString:responseDeskAlertLoginSuccess].location != NSNotFound)
        return 0;
    else if([responseString rangeOfString:responseDeskAlertIncorrectLoginData].location != NSNotFound)
        return 1;
    
    return 2;
    
}


- (BOOL) isNetworkAvailable
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    
    NetworkStatus status = [reachability currentReachabilityStatus];
    return status != NotReachable;
}

- (BOOL) checkInternetConnection:(BOOL) showAlertIfNotAvailable
{
    BOOL avail = [self isNetworkAvailable];
    
    if(showAlertIfNotAvailable && !avail)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Network is not available "delegate:nil cancelButtonTitle:@"Try Again" otherButtonTitles:nil];
        
        [alert show];
    }
    
    return avail;
    
}

- (void) registerDeviceWithToken:(NSData*)token
{
    
    if(![self checkInternetConnection:YES])
        return;
    
    [UserSettings sharedInstance].device_token = token;
    [UserSettings saveUserSettings];
    
    
    
    //const char* data = [token bytes];
    //NSMutableString* strToken = [NSMutableString string];
    
//    for (int i = 0; i < [token length]; i++) {
//        [strToken appendFormat:@"%02.2hhX", data[i]];
//    }
    
    NSString* strToken = [[token description] stringByTrimmingCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    strToken = [strToken stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSString* serverHostName = [NSString stringWithFormat:@"%@/register_device.asp", [UserSettings sharedInstance].hostName];
    NSString* queryString = [NSString stringWithFormat:@"device_id=%@&device_type=3&user_name=%@&guid=%@", strToken, [UserSettings sharedInstance].login, [UserSettings sharedInstance].guid];
    

    NSLog(@" QSTR = %@", queryString);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:serverHostName]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[queryString length]] forHTTPHeaderField:@"Content-length"];
    
    [request setHTTPBody:[queryString dataUsingEncoding:NSUTF8StringEncoding]];
    [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
}


- (void) unregisterDevice
{
    if(![self checkInternetConnection:YES])
        return;
    
    
    NSString* strToken = [[[UserSettings sharedInstance].device_token description] stringByTrimmingCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    strToken = [strToken stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSString* serverHostName = [NSString stringWithFormat:@"%@/unregister_device.asp", [UserSettings sharedInstance].hostName];
    NSString* queryString = [NSString stringWithFormat:@"device_id=%@&device_type=3&user_name=%@&guid=%@", strToken, [UserSettings sharedInstance].login, [UserSettings sharedInstance].guid];
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:serverHostName]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[queryString length]] forHTTPHeaderField:@"Content-length"];
    
    [request setHTTPBody:[queryString dataUsingEncoding:NSUTF8StringEncoding]];
    [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    
}

+ (instancetype) defaultServer
{
    static id server;
    static dispatch_once_t token;
    dispatch_once(&token, ^{
    
        server = [[super alloc] initUniqueInstance];
        
    });
    
    return server;
}

- (instancetype) initUniqueInstance
{
    return [super init];
}

- (NSData*) getAllDataForUser:(NSString*) login andPassword: (NSString*) password onServer: (NSString*) hostName
{
     NSString* serverHostName = [NSString stringWithFormat:@"%@/emergency_app_request.asp?login=%@&password=%@", hostName, login, password];
 
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:serverHostName]];

    
    NSData* recievedData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];

    
    return recievedData;

}

- (BOOL) isServerAvailable:(NSString*)hostName
{
    
    NSString* url = [NSString stringWithFormat:@"%@/emergency_app_request.asp", hostName];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    
    
    NSData* recievedData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    
    if(recievedData == nil)
        return NO;
    
    NSString* result = [NSString stringWithUTF8String:[recievedData bytes]];
    
    return [result isEqualToString:@"{\"error\":\"Please specify a login and non-empty password\"}"];
}

- (NSString*) sendInstantAlertWithId:(int)alertId
{
    NSString* apiKey = [UserSettings sharedInstance].apiKey;
    NSString* xmlData = @"<darequest><alert><instant><alertId>%@</alertId></instant></alert></darequest>";
    
    xmlData = [NSString stringWithFormat:xmlData, [NSNumber numberWithInt:alertId]];
    
    NSString* serverHostName = [NSString stringWithFormat:@"%@/api_request.asp", [UserSettings sharedInstance].hostName];
    
   // NSString* signature = [NSString stringWithFormat:@"%@%@", [xmlData MD5String], apiKey] ;
    NSString* queryString = [NSString stringWithFormat:@"xml_data=%@&api_signature=%@", xmlData, apiKey];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:serverHostName]];

    [request setHTTPMethod:@"POST"];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[queryString length]] forHTTPHeaderField:@"Content-length"];
    
    [request setHTTPBody:[queryString dataUsingEncoding:NSUTF8StringEncoding]];
    NSData* data = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];

    
    return [NSString stringWithUTF8String:[data bytes]];
}



@end
