//
//  DeskAlertServer.h
//  Deskalert Mobile
//
//  Created by Дмитрий Кайгородов on 12.03.15.
//  Copyright (c) 2015 Deskalert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DeskAlertServer : NSObject

 

+(instancetype) alloc __attribute__((unavailable("alloc not available, call sharedInstance instead")));
-(instancetype) init __attribute__((unavailable("init not available, call sharedInstance instead")));
+(instancetype) new __attribute__((unavailable("new not available, call sharedInstance instead")));

- (int) validateOrRegisterUserWithLogin:(NSString*) login andPassword: (NSString*) password onServerWithHostName:(NSString*) hostName;

- (void) registerDeviceWithToken:(NSData*)token;
- (void) unregisterDevice;
+ (instancetype) defaultServer;
- (BOOL) checkInternetConnection:(BOOL) showAlertIfNotAvailable;
- (NSData*) getAllDataForUser:(NSString*) login andPassword: (NSString*) password onServer: (NSString*) hostName;

- (NSString*) sendInstantAlertWithId:(int)alertId;

- (BOOL) isServerAvailable:(NSString*)hostName;

@end
