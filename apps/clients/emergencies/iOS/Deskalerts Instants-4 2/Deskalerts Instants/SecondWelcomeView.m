

#import "UserSettings.h"
#import "SecondWelcomeView.h"
#import "DeskAlertServer.h"
#import "FirstWelcomeView.h"
#import "ColorCode.h"
#import "Alert.h"
#import "AppDelegate.h"

@interface SecondWelcomeView ()

@end

@implementation SecondWelcomeView

- (IBAction)onFieldEdited:(id)sender {
}


- (IBAction)loginChanged:(id)sender {
    
    self.loginButton.enabled = [self.loginField.text length ] > 0 && [self.passwordField.text length] > 0;
}
- (IBAction)loginClick:(id)sender {
    
    

    
    
    DeskAlertServer *server = [DeskAlertServer defaultServer];
    
    if(![server checkInternetConnection:YES])
        return;
    
    NSData*  __block data;
    [self.progressBar showAnimated:YES whileExecutingBlock:^{
        
        data = [server getAllDataForUser:self.loginField.text andPassword:self.passwordField.text onServer:[UserSettings sharedInstance].hostName];
        
        NSError *error = nil;
        NSMutableDictionary* jsonData = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
        
        NSString* errorMsg = [jsonData objectForKey:@"error"];
                           
        if(errorMsg)
        {
          //  NSString* errorMessage = [errors objectAtIndex:0];
            dispatch_sync(dispatch_get_main_queue(), ^{
                
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:errorMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
                
            });
            return;
        }
        NSArray* alerts =  [jsonData objectForKey:@"alerts"];
        NSArray* colorCodes = [jsonData objectForKey:@"color_codes"];
        NSString* apiKey = [jsonData objectForKey:@"api_key"];
        
        NSManagedObjectContext* context = [AppDelegate sharedInstance].managedObjectContext;
        
        NSFetchRequest * fetch = [[NSFetchRequest alloc] init];
        [fetch setEntity:[NSEntityDescription entityForName:@"Alert" inManagedObjectContext:context]];
        NSArray * result = [context executeFetchRequest:fetch error:nil];
        for (id basket in result)
            [context deleteObject:basket];
        
        NSFetchRequest * fetchColors = [[NSFetchRequest alloc] init];
        [fetchColors setEntity:[NSEntityDescription entityForName:@"ColorCode" inManagedObjectContext:context]];
        NSArray* resultColor = [context executeFetchRequest:fetchColors error:nil];
        for (id basket in resultColor)
            [context deleteObject:basket];
        
        for(NSDictionary* alertDictionary in alerts)
        {
            
            NSEntityDescription *entity = [NSEntityDescription entityForName:@"Alert" inManagedObjectContext:context];
            NSManagedObject *alertToAdd = [[NSManagedObject alloc] initWithEntity:entity insertIntoManagedObjectContext:context];
            
            [alertToAdd setValue:[alertDictionary objectForKey:@"id"] forKey:@"id"];
            [alertToAdd setValue:[alertDictionary objectForKey:@"title"] forKey:@"title"];
            [alertToAdd setValue:[alertDictionary objectForKey:@"color"] forKey:@"color_code"];
            
        }

        
        

        
        for(NSDictionary* colorCodeDictionary in colorCodes)
        {

            
            NSEntityDescription *entity = [NSEntityDescription entityForName:@"ColorCode" inManagedObjectContext:context];
            NSManagedObject *colorToAdd = [[NSManagedObject alloc] initWithEntity:entity insertIntoManagedObjectContext:context];
            
            [colorToAdd setValue:[colorCodeDictionary objectForKey:@"title"] forKey:@"title"];
            [colorToAdd setValue:[colorCodeDictionary objectForKey:@"hex"] forKey:@"color_code"];
        }
        
        [UserSettings sharedInstance].login = self.loginField.text;
        [UserSettings sharedInstance].password = self.passwordField.text;
        [UserSettings sharedInstance].hasLaunchOnce = YES;
        [UserSettings sharedInstance].apiKey = apiKey;
        [UserSettings saveUserSettings];
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            
            [self performSegueWithIdentifier:@"2to3" sender:nil];
        });
        
     /*   success = [server validateOrRegisterUserWithLogin:self.loginField.text andPassword:self.passwordField.text onServerWithHostName:[UserSettings sharedInstance].hostName];
        
        switch (success) {
            case 0:
            {
                [UserSettings sharedInstance].login = self.loginField.text;
                [UserSettings sharedInstance].password = self.passwordField.text;
                dispatch_sync(dispatch_get_main_queue(), ^{
                    [self performSegueWithIdentifier:@"2to3" sender:self];
                });
                break;
            }
                
            case 1:{
                dispatch_sync(dispatch_get_main_queue(), ^{
                    
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Your password is incorrect!"delegate:nil cancelButtonTitle:@"Try Again" otherButtonTitles:nil];
                    
                    [alert show];
                    
                });
                break;
            }
            case 2:
            {
                dispatch_sync(dispatch_get_main_queue(), ^{
                    
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Connection failed." delegate:self cancelButtonTitle:nil otherButtonTitles:@"Try again", @"Change server host", nil];
                    
                    [alert show];
                    
                });
                break;
            }
                
            default:
                break;
        }*/

        
    }];
    
    

    


//
//    BOOL __block isDataCorrect = NO;
//    
//    [self.progressBar showAnimated:YES whileExecutingBlock:^{
//        
//        isDataCorrect = [[DeskAlertServer defaultServer] validateOrRegisterUserWithLogin:self.loginField.text andPassword:self.passwordField.text onServerWithHostName:[UserSettings sharedInstance].hostName];
//    }];
//
//    
//    if(!isDataCorrect)
//    {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Your password is incorrect!"delegate:nil cancelButtonTitle:@"Try Again" otherButtonTitles:nil];
//        
//        [alert show];
//        return;
//    }
//    
//    NSLog(@"OK 2");
//    [UserSettings sharedInstance].login = self.loginField.text;
//    [UserSettings sharedInstance].password = self.passwordField.text;
    
//    UIViewController *welcome3 = [[super storyboard] instantiateViewControllerWithIdentifier:@"Welcome3"];
//    
//    [self presentViewController:welcome3 animated:YES completion:nil];
    
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if(buttonIndex == 1)
    {
        FirstWelcomeView *view = [self.storyboard instantiateViewControllerWithIdentifier:@"hostNameView"];
        [self presentViewController:view animated:YES completion:nil];
    }
}

- (IBAction)passwordChanged:(id)sender {
    
     self.loginButton.enabled = [self.loginField.text length ] > 0 && [self.passwordField.text length] > 0;
}




- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.progressBar = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:self.progressBar];
    self.progressBar.delegate = self;
    self.progressBar.labelText = @"Processing...";
    
  //  [self.progressBar showAnimated:YES whileExecutingBlock:<#^(void)block#>]
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSUInteger) supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
