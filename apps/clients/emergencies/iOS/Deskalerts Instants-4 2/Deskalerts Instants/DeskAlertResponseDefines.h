//
//  DeskAlertResponseDefines.h
//  Deskalert Mobile
//
//  Created by Дмитрий Кайгородов on 13.03.15.
//  Copyright (c) 2015 Deskalert. All rights reserved.
//

#ifndef Deskalert_Mobile_DeskAlertResponseDefines_h
#define Deskalert_Mobile_DeskAlertResponseDefines_h

#define responseDeskAlertRegistrationSuccess @"Registration was successfully completed."
#define responseDeskAlertLoginSuccess @"Thank you for signing up with existing account!"
#define responseDeskAlertLoginAlreadyExists @"Error! This user name already exists." //unused?
#define responseDeskAlertIncorrectLoginData @"Error! Username is taken, password provided is incorrect."

#endif
