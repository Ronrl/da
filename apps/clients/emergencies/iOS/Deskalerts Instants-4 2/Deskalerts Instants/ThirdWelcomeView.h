//
//  ThirdWelcomeView.h
//  Deskalert Mobile
//
//  Created by Дмитрий Кайгородов on 10.03.15.
//  Copyright (c) 2015 Deskalert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ThirdWelcomeView : UIViewController
@property (weak, nonatomic) IBOutlet UISwitch *soundSwitch;

@end
