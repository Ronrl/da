

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"


@interface SecondWelcomeView : UIViewController<MBProgressHUDDelegate, UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UIBarButtonItem *loginButton;
@property (weak, nonatomic) IBOutlet UITextField *loginField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property MBProgressHUD *progressBar;

@end
