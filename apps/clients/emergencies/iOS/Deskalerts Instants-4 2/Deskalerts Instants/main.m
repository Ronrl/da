//
//  main.m
//  Deskalerts Instants
//
//  Created by Дмитрий Кайгородов on 17.07.15.
//  Copyright (c) 2015 Toolbar Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
