//
//  NSString+MD5.h
//  Deskalerts Instants
//
//  Created by Дмитрий Кайгородов on 25.09.15.
//  Copyright (c) 2015 Toolbar Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (MD5)

- (NSString *)MD5String;

@end
