//
//  ThirdWelcomeView.m
//  Deskalert Mobile
//
//  Created by Дмитрий Кайгородов on 10.03.15.
//  Copyright (c) 2015 Deskalert. All rights reserved.
//

#import "ThirdWelcomeView.h"
#import "UserSettings.h"


@interface ThirdWelcomeView ()

@end

@implementation ThirdWelcomeView

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
- (IBAction)startButtonClick:(id)sender {

    [UserSettings sharedInstance].useSound = self.soundSwitch.isOn;
    [UserSettings sharedInstance].hasLaunchOnce = YES;
    [UserSettings saveUserSettings];
    
  //  UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeAlert| UIUserNotificationTypeBadge|UIUserNotificationTypeSound) categories:nil];
    
 //   [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
   // [[UIApplication sharedApplication] registerForRemoteNotifications];
    
    
    [self performSegueWithIdentifier:@"fromWelcomeToMain" sender:self];
}


- (NSUInteger) supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
