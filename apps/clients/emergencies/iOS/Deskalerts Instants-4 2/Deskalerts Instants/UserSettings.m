//
//  UserSettings.m
//  Deskalert Mobile
//
//  Created by Дмитрий Кайгородов on 10.03.15.
//  Copyright (c) 2015 Deskalert. All rights reserved.
//

#import "UserSettings.h"

@implementation UserSettings

+ (UserSettings*) sharedInstance
{
    
    static dispatch_once_t pred;
    static id shared = nil;
    dispatch_once(&pred, ^{
        shared = [[super alloc] initUniqueInstance];
    });
    return shared;
}

- (instancetype) initUniqueInstance
{
    return [super init];
}

+ (void) saveUserSettings
{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[UserSettings sharedInstance].hostName forKey:@"hostName"];
    [defaults setObject:[UserSettings sharedInstance].login forKey:@"login"];
    [defaults setObject:[UserSettings sharedInstance].password forKey:@"password"];
    [defaults setBool:[UserSettings sharedInstance].useSound forKey:@"useSound"];
    [defaults setBool:[UserSettings sharedInstance].hasLaunchOnce forKey:@"hasLaunchOnce"];
    [defaults setObject:[UserSettings sharedInstance].device_token forKey:@"token"];
    [defaults setObject:[UserSettings sharedInstance].apiKey forKey:@"apiKey"];
    
}

+(NSString*)GUIDString {
    CFUUIDRef theUUID = CFUUIDCreate(NULL);
    CFStringRef string = CFUUIDCreateString(NULL, theUUID);
  //  CFRelease(theUUID);
    NSString* guid = (__bridge NSString *)string;
    return guid;
}

+ (void) loadUserSettings
{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString* guid = [defaults stringForKey:@"guid"];
    
    if(guid == nil)
    {
        [UserSettings sharedInstance].guid = [UserSettings GUIDString];
        [defaults setObject:[UserSettings sharedInstance].guid forKey:@"guid" ];
        [defaults synchronize];
    }
    else
        [UserSettings sharedInstance].guid = guid;
    

    NSLog(@"GUID = %@", [UserSettings sharedInstance].guid);
    [UserSettings sharedInstance].hostName = [defaults stringForKey:@"hostName"];
    [UserSettings sharedInstance].login = [defaults stringForKey:@"login"];
    [UserSettings sharedInstance].password = [defaults stringForKey:@"password"];
    [UserSettings sharedInstance].useSound = [defaults boolForKey:@"useSound"];
    [UserSettings sharedInstance].hasLaunchOnce = [defaults boolForKey:@"hasLaunchOnce"];
    [UserSettings sharedInstance].device_token = [defaults objectForKey:@"token"];
    [UserSettings sharedInstance].apiKey = [defaults objectForKey:@"apiKey"];
}
@end
