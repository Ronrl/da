//
//  UserSettings.h
//  Deskalert Mobile
//
//  Created by Дмитрий Кайгородов on 10.03.15.
//  Copyright (c) 2015 Deskalert. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface UserSettings : NSObject

+(instancetype) alloc __attribute__((unavailable("alloc not available, call sharedInstance instead")));
-(instancetype) init __attribute__((unavailable("init not available, call sharedInstance instead")));
+(instancetype) new __attribute__((unavailable("new not available, call sharedInstance instead")));

+ (UserSettings*) sharedInstance;
+ (void) loadUserSettings;
+ (void) saveUserSettings;

@property (atomic, strong) NSString* hostName;
@property (atomic, strong) NSString* login;
@property (atomic, strong) NSString* password;
@property (atomic, strong) NSString* apiKey;
@property (atomic) BOOL useSound;
@property (atomic) BOOL hasLaunchOnce;
@property (atomic, strong) NSString* guid;
@property (atomic, strong) NSData* device_token;


@end
