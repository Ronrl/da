//
//  AlertViewController.m
//  Deskalerts Instants
//
//  Created by Дмитрий Кайгородов on 23.09.15.
//  Copyright (c) 2015 Toolbar Inc. All rights reserved.
//

#import "AlertViewController.h"
#import "AlertViewCell.h"
#import "SearchAlertViewController.h"
#import "DeskAlertServer.h"
#import "ColorCode.h"

@interface AlertViewController ()
@property (weak, nonatomic) IBOutlet UITextField *filter;
@property UISearchController* searchController;
@property int alertId;
@end

@implementation AlertViewController

- (IBAction)hidePicker:(id)sender {
    
    [self.downPicker cancelClicked:nil];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    

    self.progressBar = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:self.progressBar];
    self.progressBar.delegate = self;
    self.progressBar.labelText = @"Processing...";
    
    
    [self fillAlertTable];
    
   
    
    UINavigationController *navController = [[self storyboard] instantiateViewControllerWithIdentifier:@"ResultController"];
    
    ((SearchAlertViewController*)navController).managedObjectContext = [AppDelegate sharedInstance].managedObjectContext;
    
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:navController];
    self.searchController.searchResultsUpdater = self;
    
    self.searchController.searchBar.frame = CGRectMake(self.searchController.searchBar.frame.origin.x, self.searchController.searchBar.frame.origin.y, self.searchController.searchBar.frame.size.width, 44.0);
    
    
    [[UIBarButtonItem appearanceWhenContainedInInstancesOfClasses:[NSArray arrayWithObject:[UISearchBar class]]] setTitle:@"Cancel"];
    self.searchController.searchBar.tintColor = [UIColor colorWithRed:77/255.0 green:174/255.0 blue:74/255.0 alpha:1.0];
    
    
     self.searchController.searchBar.placeholder = @"Search alert by title";
    // self.searchController.searchBar.prompt = @"Enter alert`s title";
    self.searchController.hidesNavigationBarDuringPresentation = YES;
    
    self.tableView.tableHeaderView = self.searchController.searchBar;
    
    // NSMutableArray* bandArray = [[NSMutableArray alloc] init];
    

    
    NSFetchRequest *colorFetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"ColorCode"
                                              inManagedObjectContext:[AppDelegate sharedInstance].managedObjectContext];
    [colorFetchRequest setEntity:entity];
 
    NSError* error = nil;

    NSArray *fetchedObjects = [[AppDelegate sharedInstance].managedObjectContext executeFetchRequest:colorFetchRequest error:&error];
    
    NSMutableArray* colorCodeArray = [[NSMutableArray alloc] init];
    
    
    for(ColorCode* code in fetchedObjects)
    {
        
        UIColor* color = [self colorFromHexString:code.color_code];
        NSDictionary *attrs = @{ NSForegroundColorAttributeName : color };
        NSAttributedString *attrStr = [[NSAttributedString alloc] initWithString:code.title attributes:attrs];
        
        [colorCodeArray addObject:attrStr];
        
    }
    
    // bind yourTextField to DownPicker
    self.downPicker = [[DownPicker alloc] initWithTextField:self.filter withData:colorCodeArray  andWithCancelButton:self.cancelButton];
    
    
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(alertSend:) name:@"sendAlert" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(colorCodeChanged:) name:@"colorCodeChanged" object:nil];

    
}

- (void) fillAlertTable
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Alert"];
    
    // Add Sort Descriptors
    [fetchRequest setSortDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"title" ascending:YES]]];
    
    // Initialize Fetched Results Controller
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[AppDelegate sharedInstance].managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsController setDelegate:self];
    
    NSError *error = nil;
    [self.fetchedResultsController performFetch:&error];
}

- (void) colorCodeChanged: (NSNotification*) notification
{
    
    NSString* color = notification.object;
    
    if([color isEqual:@""])
    {
        [self fillAlertTable];
        [self.tableView reloadData];
        return;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Alert"];
    NSSortDescriptor *sd = [NSSortDescriptor sortDescriptorWithKey:@"title" ascending:YES];
    [fetchRequest setSortDescriptors:@[sd]];
    
    NSPredicate* filter = [NSPredicate predicateWithFormat:@"color_code like[c] %@", color];
    [fetchRequest setPredicate:filter];
    
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[AppDelegate sharedInstance].managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    
    
    
    [self.fetchedResultsController setDelegate:self];
    
    NSError* error = nil;
    
    
    [self.fetchedResultsController performFetch:&error];
    
    if(error)
    {
        NSLog(@"Fetch failed");
    }
    
    [self.tableView reloadData];
    
}
- (void) alertSend: (NSNotification*) notification
{
    //_alertId = [notification.object intValue];
    
    NSManagedObject* alertObject = notification.object;
    
    _alertId = [[alertObject valueForKey:@"id"] intValue];
    
    NSString* title = [alertObject valueForKey:@"title"];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:@"Send this emergency alert?" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Back", @"Send", nil];
    
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSLog(@"ALERT!!!");
    
    
    
    if(buttonIndex == 1)
    {
        if(![[DeskAlertServer defaultServer] checkInternetConnection:YES] )
        {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sending error!" message:@"Please check your internet connection." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            return;
        }
        
        
      [self.progressBar showAnimated:YES whileExecutingBlock:^{
          
          NSString* result =  [[DeskAlertServer defaultServer] sendInstantAlertWithId:_alertId];
          
          
          if([result rangeOfString:@"<result>SUCCESS</result>"].location == NSNotFound)
          {
              dispatch_sync(dispatch_get_main_queue(), ^{
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!" message:@"Can not         send alert." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert show];
              });
          }
          else
          {
              dispatch_sync(dispatch_get_main_queue(), ^{
                  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success!" message:@"Alert was sent successfully!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                  [alert show];
              });
          }
      }];
        

        
    }
}

 - (void) viewDidAppear:(BOOL)animated
{

}


- (void) updateSearchResultsForSearchController:(UISearchController *)searchController
{
    NSString* searchText = searchController.searchBar.text;
    
    
    NSArray* result = [self searchForText:searchText];
    
    if(result)
    {
        //        UINavigationController *navController = (UINavigationController *)self.searchController.searchResultsController;
        
        SearchAlertViewController *src = (SearchAlertViewController*) self.searchController.searchResultsController;
        src.result = [result mutableCopy];
        
        [src.resultTable reloadData];
        
        //  [src.resultTable scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
        
    }
}

-(NSFetchRequest*) searchRequest
{
    if (_searchRequest != nil)
    {
        return _searchRequest;
    }
    
    _searchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Alert" inManagedObjectContext:[AppDelegate sharedInstance].managedObjectContext];
    [_searchRequest setEntity:entity];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"title" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObjects:sortDescriptor, nil];
    [_searchRequest setSortDescriptors:sortDescriptors];
    
    return _searchRequest;
}


- (NSMutableArray*) searchForText:(NSString *)searchText
{
    if ([AppDelegate sharedInstance].managedObjectContext)
    {
        NSString *predicateFormat = @"title BEGINSWITH[cd] %@";
        
        
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFormat, searchText];
        [self.searchRequest setPredicate:predicate];
        
        NSError *error = nil;
        return [[[AppDelegate sharedInstance].managedObjectContext executeFetchRequest:self.searchRequest error:&error] mutableCopy];
        
        
    }
    return nil;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return  [[self.fetchedResultsController sections] count] ;
    //return  1;
}


- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    NSArray* sections = [self.fetchedResultsController sections];
    id<NSFetchedResultsSectionInfo> sectionInfo = [sections objectAtIndex:section];
    
    return [sectionInfo numberOfObjects];
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView beginUpdates];
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView endUpdates];
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath

{
    return 60;
    
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    switch (type) {
        case NSFetchedResultsChangeInsert: {
            [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        }
        case NSFetchedResultsChangeDelete: {
            [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        }
            
        case NSFetchedResultsChangeMove: {
            
            break;
        }
    }
}

- (UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}


- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AlertViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"AlertCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSManagedObject *record = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    NSString* title = [record valueForKey:@"title"];
    
//    
//    if([title length] > 32)
//    {
//        title = [[title substringToIndex:29] stringByAppendingString:@"..."];
//    }
 
    cell.title.text = title;
    [cell.title sizeToFit];
    NSString* hexColor = [record valueForKey:@"color_code"];
    
    cell.colorCode.backgroundColor = [self colorFromHexString:hexColor];

    cell.alertId = [[record valueForKey:@"id"] intValue];
    
    return cell;
    
}

-(BOOL) tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
     NSManagedObject *record = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
  //  self.alertId  = [[record valueForKey:@"id"] intValue];
   
        [[NSNotificationCenter defaultCenter] postNotificationName:@"sendAlert" object:record];
    

    
}

@end
