//
//  SettingsViewController.h
//  Deskalerts Instants
//
//  Created by Дмитрий Кайгородов on 12.10.15.
//  Copyright © 2015 Toolbar Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface SettingsViewController : UITableViewController<MBProgressHUDDelegate>
@property (weak, nonatomic) IBOutlet UITextField *hostName;
@property (weak, nonatomic) IBOutlet UITextField *login;
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *saveButton;

@property MBProgressHUD *progressBar;

@end
