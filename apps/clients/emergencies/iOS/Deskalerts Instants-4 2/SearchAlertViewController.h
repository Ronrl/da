//
//  SearchAlertViewController.h
//  Deskalerts Instants
//
//  Created by Дмитрий Кайгородов on 24.09.15.
//  Copyright (c) 2015 Toolbar Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface SearchAlertViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate>


@property (strong, nonatomic) NSMutableArray* result;
@property (weak, nonatomic) IBOutlet UITableView *resultTable;
@property (weak) NSManagedObjectContext* managedObjectContext;

@end
