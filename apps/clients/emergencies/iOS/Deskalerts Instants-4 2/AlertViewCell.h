//
//  AlertViewCell.h
//  Deskalerts Instants
//
//  Created by Evgeniy Mandrikin on 24.09.15.
//  Copyright (c) 2015 Toolbar Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DeskAlertServer.h"

@interface AlertViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *colorCode;
@property (nonatomic) int alertId;

@end
