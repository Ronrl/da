﻿namespace DeskAlerts.UIAutomationTests.Interfaces
{
    public interface IFocusableOnBody
    {
        void FocusOnBody();
    }
}
