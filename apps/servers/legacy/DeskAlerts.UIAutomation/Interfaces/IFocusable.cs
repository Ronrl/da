﻿namespace DeskAlerts.UIAutomationTests.Interfaces
{
    public interface IFocusable
    {
        void Focus();
    }
}
