﻿using DeskAlerts.UIAutomationTests.Frames.BaseFrame;
using DeskAlerts.UIAutomationTests.Interfaces;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Frames.VideoalertTitleFrame
{
    public class VideoalertTitleFrameActions : BaseFramePage, IFocusable
    {
        private readonly VideoalertTitleFrameElements _elements;
        private readonly ChromeDriver _driver;

        public VideoalertTitleFrameActions(ChromeDriver driver) : base(driver)
        {
            _elements = new VideoalertTitleFrameElements(driver);
            _driver = driver;
        }

        public void Focus()
        {
            _driver.SwitchTo().Frame(_elements.Frame);
        }

        public void SetTitle(string text)
        {
            _elements.Title.Clear();
            _elements.Title.SendKeys(text);
        }
    }
}
