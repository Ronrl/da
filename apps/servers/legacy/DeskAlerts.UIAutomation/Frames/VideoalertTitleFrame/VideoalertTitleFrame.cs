﻿using DeskAlerts.UIAutomationTests.Frames.BaseFrame;
using DeskAlerts.UIAutomationTests.Interfaces;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Frames.VideoalertTitleFrame
{
    public class VideoalertTitleFrame : BaseFramePage, IFocusable
    {
        private readonly VideoalertTitleFrameActions _actions;

        public VideoalertTitleFrame(ChromeDriver driver) : base(driver)
        {
            _actions = new VideoalertTitleFrameActions(driver);
        }

        public void SetTitle(string text)
        {
            _actions.SetTitle(text);
        }

        public void Focus()
        {
            _actions.Focus();
        }
    }
}
