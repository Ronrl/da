﻿using System.Linq;
using DeskAlerts.UIAutomationTests.Interfaces;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Frames.PolicyOusFrame
{
    public class PolicyOusFrameActions : IFocusable
    {
        private readonly PolicyOusFrameElements _elements;
        private readonly ChromeDriver _driver;

        public PolicyOusFrameActions(ChromeDriver driver)
        {
            _elements = new PolicyOusFrameElements(driver);
            _driver = driver;
        }

        public void Focus()
        {
            _driver.SwitchTo().Frame(_elements.Frame);
        }

        public void ToggleTreeElements()
        {
            foreach (var toggle in _elements.TreeToggles.Skip(1))
            {
                toggle.Click();
            }
        }

        public void ChooseTwoRandomTreeCheckboxes()
        {
            if (_elements.TreeCheckboxes.Count < 2)
            {
                throw new NotFoundException("Less than 2 elements are displayed in Organization/audience tree");
            }

            foreach (var checkbox in _elements.TreeCheckboxes.Take(2))
            {
                checkbox.Click();
            }
        }
    }
}
