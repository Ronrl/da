﻿using System.Collections.Generic;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Frames.PolicyOusFrame
{
    public class PolicyOusFrameElements
    {
        private readonly ChromeDriver _driver;

        public PolicyOusFrameElements(ChromeDriver driver)
        {
            _driver = driver;
        }

        public IWebElement Frame => _driver.FindElementById("list1");

        public ICollection<IWebElement> TreeToggles => _driver.FindElementsByCssSelector("a[id^=\"orgTreen\"]");

        public ICollection<IWebElement> TreeCheckboxes => _driver.FindElementsByCssSelector("input[id^=\"orgTreen\"]");
    }
}