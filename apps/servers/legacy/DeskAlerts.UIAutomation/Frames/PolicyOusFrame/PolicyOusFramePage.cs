﻿using DeskAlerts.UIAutomationTests.Frames.BaseFrame;
using DeskAlerts.UIAutomationTests.Interfaces;
using DeskAlerts.UIAutomationTests.Pages.BaseTable;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Frames.PolicyOusFrame
{
    public class PolicyOusFramePage : BaseTablePage, IFocusable
    {
        private readonly PolicyOusFrameActions _actions;
        private readonly IFocusableOnBody _baseFramePage;

        public PolicyOusFramePage(ChromeDriver driver) : base(driver)
        {
            _actions = new PolicyOusFrameActions(driver);
            _baseFramePage = new BaseFramePage(driver);
        }

        public void Focus()
        {
            _actions.Focus();
        }

        public void FocusOnBody()
        {
            _baseFramePage.FocusOnBody();
        }

        public void ToggleTreeElements()
        {
            _actions.ToggleTreeElements();
        }

        public void ChooseTwoRandomTreeCheckboxes()
        {
            _actions.ChooseTwoRandomTreeCheckboxes();
        }
    }
}
