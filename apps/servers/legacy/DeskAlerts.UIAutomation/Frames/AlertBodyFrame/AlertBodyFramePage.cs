﻿using DeskAlerts.UIAutomationTests.Frames.BaseFrame;
using DeskAlerts.UIAutomationTests.Interfaces;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Frames.AlertBodyFrame
{
    public class AlertBodyFramePage : BaseFramePage, IFocusable
    {
        private readonly AlertBodyFrameActions _actions;

        public AlertBodyFramePage(ChromeDriver driver) : base(driver)
        {
            _actions = new AlertBodyFrameActions(driver);
        }

        public void Focus()
        {
            _actions.Focus();
        }

        public void SetBody(string text)
        {
            _actions.SetBody(text);
        }
    }
}
