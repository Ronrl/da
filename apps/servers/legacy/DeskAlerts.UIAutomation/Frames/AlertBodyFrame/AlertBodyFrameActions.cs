﻿using DeskAlerts.UIAutomationTests.Frames.BaseFrame;
using DeskAlerts.UIAutomationTests.Interfaces;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Frames.AlertBodyFrame
{
    public class AlertBodyFrameActions : BaseFramePage, IFocusable
    {
        private readonly AlertBodyFrameElements _elements;
        private readonly ChromeDriver _driver;

        public AlertBodyFrameActions(ChromeDriver driver) : base(driver)
        {
            _elements = new AlertBodyFrameElements(driver);
            _driver = driver;
        }

        public void Focus()
        {
            _driver.SwitchTo().Frame(_elements.Frame);
        }

        public void SetBody(string text)
        {
            _elements.Body.Clear();
            _elements.Body.SendKeys(text);
        }
    }
}
