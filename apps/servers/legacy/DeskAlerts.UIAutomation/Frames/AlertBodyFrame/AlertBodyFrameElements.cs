﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Frames.AlertBodyFrame
{
    public class AlertBodyFrameElements
    {
        private readonly ChromeDriver _driver;

        public AlertBodyFrameElements(ChromeDriver driver)
        {
            _driver = driver;
        }

        public IWebElement Body => _driver.FindElementByXPath("//*[@id=\"tinymce\"]");

        public IWebElement Frame => _driver.FindElementById("alertContent_ifr");
    }
}