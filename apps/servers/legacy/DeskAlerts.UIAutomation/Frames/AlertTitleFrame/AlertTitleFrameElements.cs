﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Frames.AlertTitleFrame
{
    public class AlertTitleFrameElements
    {
        private readonly ChromeDriver _driver;

        public AlertTitleFrameElements(ChromeDriver driver)
        {
            _driver = driver;
        }

        public IWebElement Title => _driver.FindElementByXPath("//*[@id=\"tinymce\"]");

        public IWebElement Frame => _driver.FindElementById("htmlTitle_ifr");
    }
}