﻿using DeskAlerts.UIAutomationTests.Frames.BaseFrame;
using DeskAlerts.UIAutomationTests.Interfaces;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Frames.AlertTitleFrame
{
    public class AlertTitleFramePage : BaseFramePage, IFocusable
    {
        private readonly AlertTitleFrameActions _actions;

        public AlertTitleFramePage(ChromeDriver driver) : base(driver)
        {
            _actions = new AlertTitleFrameActions(driver);
        }

        public void SetTitle(string text)
        {
            _actions.SetTitle(text);
        }

        public void Focus()
        {
            _actions.Focus();
        }
    }
}
