﻿using DeskAlerts.UIAutomationTests.Frames.BaseFrame;
using DeskAlerts.UIAutomationTests.Interfaces;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Frames.AlertTitleFrame
{
    public class AlertTitleFrameActions : BaseFramePage, IFocusable
    {
        private readonly AlertTitleFrameElements _elements;
        private readonly ChromeDriver _driver;

        public AlertTitleFrameActions(ChromeDriver driver) : base(driver)
        {
            _elements = new AlertTitleFrameElements(driver);
            _driver = driver;
        }

        public void Focus()
        {
            _driver.SwitchTo().Frame(_elements.Frame);
        }

        public void SetTitle(string text)
        {
            _elements.Title.Clear();
            _elements.Title.SendKeys(text);
        }
    }
}
