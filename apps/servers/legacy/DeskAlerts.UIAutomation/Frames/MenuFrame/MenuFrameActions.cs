﻿using DeskAlerts.UIAutomationTests.Interfaces;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Frames.MenuFrame
{
    public class MenuActions : IFocusable
    {
        private readonly MenuElements _elements;
        private readonly ChromeDriver _driver;

        public MenuActions(ChromeDriver driver)
        {
            _elements = new MenuElements(driver);
            _driver = driver;
        }

        #region FocusOn

        public void Focus()
        {
            _driver.SwitchTo().DefaultContent();
            _driver.SwitchTo().Frame(_elements.Frame);
        }


        #endregion

        #region Toggle

        public void ToggleActiveDirectory()
        {
            _elements.ActiveDirectory.Click();
        }

        public void TogglePublishers()
        {
            _elements.Publishers.Click();
        }

        public void ToggleAlerts()
        {
            _elements.Alerts.Click();
        }

        public void ToggleEmergency()
        {
            _elements.Emergency.Click();
        }

        public void ToggleVideoalerts()
        {
            _elements.Videoalerts.Click();
        }

        public void ToggleDigitalSignage()
        {
            _elements.DigitalSignage.Click();
        }

        public void ToggleSurveys()
        {
            _elements.Surveys.Click();
        }

        #endregion

        #region Click

        public void ClickCreateAlert()
        {
            _elements.CreateAlert.Click();
        }

        public void ClickCreateEmergency()
        {
            _elements.CreateEmergency.Click();
        }

        public void ClickSendNow()
        {
            _elements.SendNowEmergency.Click();
        }

        public void ClickColorCodes()
        {
            _elements.ColorCodes.Click();
        }

        public void ClickCreateShortcut()
        { 
            _elements.CreateEmergencyShortcut.Click();
        }

        public void ClickAlertSent()
        {
            _elements.AlertSent.Click();
        }

        public void ClickAlertDraft()
        {
            _elements.AlertDraft.Click();
        }

        public void ClickIpGroups()
        {
            _elements.IpGroups.Click();
        }

        public void ClickSynchronizations()
        {
            _elements.Synchronizations.Click();
        }

        public void ClickOrganizationAudience()
        {
            _elements.OrganizationAudience.Click();
        }

        public void ClickPublishersList()
        {
            _elements.PublishersList.Click();
        }

        public void ClickPolicies()
        {
            _elements.Policies.Click();
        }

        public void ClickMessageTemplates()
        {
            _elements.MessageTemplates.Click();
        }

        public void ClickVideoalertsCreate()
        {
            _elements.VideoalertsCreate.Click();
        }

        public void ClickVideoalertsDraft()
        {
            _elements.VideoalertsDraft.Click();
        }

        public void ClickDigitalSignageDevices()
        {
            _elements.DigitalSignageDevices.Click();
        }

        public void ClickDigitalSignageSendContent()
        {
            _elements.DigitalSignageSendContent.Click();
        }

        public void ClickSurveysCreate()
        {
            _elements.SurveysCreate.SendKeys(Keys.Enter);
        }

        public void ClickSurveysActive()
        {
            _elements.SurveysActive.SendKeys(Keys.Enter);
        }

        public void ClickDashboard()
        {
            _elements.Dashboard.Click();
        }

        #endregion
    }
}
