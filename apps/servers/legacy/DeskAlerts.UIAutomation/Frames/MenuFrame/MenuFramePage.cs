﻿using DeskAlerts.UIAutomationTests.Frames.BaseFrame;
using DeskAlerts.UIAutomationTests.Interfaces;
using DeskAlerts.UIAutomationTests.Utilities;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Frames.MenuFrame
{
    public class MenuPage : BaseFramePage, IFocusable
    {
        private readonly MenuActions _actions;
        private readonly WebDriver _webDriver;

        public MenuPage(ChromeDriver driver, WebDriver webDriver) : base(driver)
        {
            _actions = new MenuActions(driver);
            _webDriver = webDriver;
        }

        public void OpenSynchronizations()
        {
            _actions.Focus();

            _actions.ToggleActiveDirectory();
            _webDriver.Wait();

            _actions.ClickSynchronizations();
            _actions.ToggleActiveDirectory();

            FocusOnBody();
            _webDriver.Wait();
        }

        public void OpenMessageTemplates()
        {
            _actions.Focus();

            _actions.ToggleAlerts();
            _webDriver.Wait();

            _actions.ClickMessageTemplates();
            _actions.ToggleAlerts();

            FocusOnBody();
            _webDriver.Wait();
        }

        public void OpenOrganizationAudience()
        {
            _actions.Focus();

            _actions.ToggleActiveDirectory();
            _webDriver.Wait();

            _actions.ClickOrganizationAudience();
            _actions.ToggleActiveDirectory();

            FocusOnBody();
            _webDriver.Wait();
        }

        public void OpenIpGroups()
        {
            _actions.Focus();

            _actions.ClickIpGroups();

            FocusOnBody();
            _webDriver.Wait();
        }

        public void OpenPublishersList()
        {
            _actions.Focus();

            _actions.TogglePublishers();
            _webDriver.Wait();

            _actions.ClickPublishersList();
            _actions.TogglePublishers();

            FocusOnBody();
            _webDriver.Wait();
        }

        public void OpenCreateEmergency()
        { 
            _actions.Focus();

            _actions.ToggleEmergency();
            
            _actions.ClickCreateEmergency();

            FocusOnBody();
        }

        public void OpenColorCodes()
        {
            _actions.Focus();

            _actions.ToggleEmergency();
            _webDriver.Wait();

            _actions.ClickColorCodes();

            FocusOnBody();
        }

        public void OpenSendNowEmergency()
        {
            _actions.Focus();

            _actions.ToggleEmergency();

            _actions.ClickSendNow();

            FocusOnBody();
        }

        public void OpenCreateShortcut()
        {
            _actions.Focus();

            _actions.ToggleEmergency();

            _actions.ClickCreateShortcut();

            FocusOnBody();
        }

        public void OpenPolicies()
        {
            _actions.Focus();

            _actions.TogglePublishers();
            _webDriver.Wait();

            _actions.ClickPolicies();
            _actions.TogglePublishers();

            FocusOnBody();
            _webDriver.Wait();
        }

        public void OpenCreateAlert()
        {
            _actions.Focus();

            _actions.ToggleAlerts();
            _webDriver.Wait();
            _actions.ClickCreateAlert();
            _actions.ToggleAlerts();

            FocusOnBody();
            _webDriver.Wait();
        }

        public void OpenAlertSent()
        {
            _actions.Focus();

            _actions.ToggleAlerts();
            _webDriver.Wait();
            _actions.ClickAlertSent();
            _actions.ToggleAlerts();

            FocusOnBody();
            _webDriver.Wait();
        }

        public void OpenAlertDraft()
        {
            _actions.Focus();

            _actions.ToggleAlerts();
            _webDriver.Wait();
            _actions.ClickAlertDraft();
            _actions.ToggleAlerts();

            FocusOnBody();
            _webDriver.Wait();
        }

        public void OpenVideoalertsCreate()
        {
            _actions.Focus();

            _actions.ToggleVideoalerts();
            _actions.ClickVideoalertsCreate();
            _actions.ToggleVideoalerts();

            FocusOnBody();
        }

        public void OpenVideoalertsDraft()
        {
            _actions.Focus();

            _actions.ToggleVideoalerts();
            _actions.ClickVideoalertsDraft();
            _actions.ToggleVideoalerts();

            FocusOnBody();
        }

        public void OpenDigitalSignageDevices()
        {
            _actions.Focus();

            _actions.ToggleDigitalSignage();
            _actions.ClickDigitalSignageDevices();
            _actions.ToggleDigitalSignage();

            FocusOnBody();
        }

        public void OpenDigitalSignageSendContent()
        {
            _actions.Focus();

            _actions.ToggleDigitalSignage();
            _actions.ClickDigitalSignageSendContent();
            _actions.ToggleDigitalSignage();

            FocusOnBody();
        }

        public void OpenSurveysCreate()
        {
            _actions.Focus();

            _actions.ToggleSurveys();
            _actions.ClickSurveysCreate();
            _actions.ToggleSurveys();

            FocusOnBody();
        }

        public void OpenSurveysActive()
        {
            _actions.Focus();

            _actions.ToggleSurveys();
            _actions.ClickSurveysActive();
            _actions.ToggleSurveys();

            FocusOnBody();
        }

        public void OpenDashboard()
        {
            Focus();

            _actions.ClickDashboard();

            FocusOnBody();
        }

        public void Focus()
        {
            _actions.Focus();
        }
    }
}
