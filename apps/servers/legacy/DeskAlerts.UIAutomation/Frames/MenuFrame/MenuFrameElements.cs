﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Linq;

namespace DeskAlerts.UIAutomationTests.Frames.MenuFrame
{
    public class MenuElements
    {
        private readonly ChromeDriver _driver;

        public MenuElements(ChromeDriver driver)
        {
            _driver = driver;
        }

        public IWebElement Frame => _driver.FindElementById("menu");

        public IWebElement Alerts => GetMenuParentElementByName("Alerts");

        public IWebElement Emergency => GetMenuParentElementByName("Emergency Alerts");

        public IWebElement CreateEmergency => GetMenuChildElementByHref("CreateAlert.aspx?instant=1");

        public IWebElement SendNowEmergency => GetMenuChildElementByHref("InstantMessages.aspx");

        public IWebElement ColorCodes => GetMenuChildElementByHref("ColorCodes.aspx");

        public IWebElement CreateEmergencyShortcut => GetMenuChildElementByHref("CreateEmergencyShortcut.aspx");

        public IWebElement ActiveDirectory => GetMenuParentElementByName("Active Directory");

        public IWebElement Publishers => GetMenuParentElementByName("Publishers");

        public IWebElement Synchronizations => GetMenuChildElementByHref("synchronizations.aspx");

        public IWebElement OrganizationAudience => GetMenuChildElementByHref("Organizations/Index.aspx");

        public IWebElement PublishersList => GetMenuChildElementByName("Publishers List");

        public IWebElement Policies => GetMenuChildElementByHref("PolicyList.aspx");

        public IWebElement CreateAlert => GetMenuChildElementByHref("CreateAlert.aspx");

        public IWebElement AlertSent => GetMenuChildElementByHref("PopupSent.aspx");

        public IWebElement MessageTemplates => GetMenuChildElementByHref("TextTemplates.aspx");

        public IWebElement AlertDraft => GetMenuChildElementByHref("PopupSent.aspx?draft=1");

        public IWebElement IpGroups => GetMenuParentElementByName("IP groups");

        public IWebElement Videoalerts => GetMenuParentElementByName("Video Alerts");

        public IWebElement VideoalertsCreate => GetMenuChildElementByHref("EditVideo.aspx");

        public IWebElement VideoalertsDraft => GetMenuChildElementByHref("Videos.aspx?draft=1");

        public IWebElement DigitalSignage => GetMenuParentElementByName("Digital Signage");

        public IWebElement DigitalSignageDevices => GetMenuChildElementByHref("DigitalSignageLinks.aspx");

        public IWebElement DigitalSignageSendContent => GetMenuChildElementByHref("CreateAlert.aspx?webplugin=1");

        public IWebElement Surveys => GetMenuParentElementByName("Surveys/Quizzes/Polls");

        public IWebElement SurveysCreate => GetMenuChildElementByHref("EditSurvey.aspx");

        public IWebElement SurveysActive => GetMenuChildElementByHref("surveys.aspx");

        public IWebElement Dashboard => GetMenuParentElementByHref("dashboard.aspx");

        private IWebElement GetMenuParentElementByName(string menuElementName)
        {
            var menuElements = _driver.FindElementsByClassName("menu_item");
            var result = menuElements.First(x => x.Text == menuElementName);
            return result;
        }

        private IWebElement GetMenuParentElementByHref(string href)
        {
            return _driver.FindElement(By.CssSelector($"a[href*='{href}']"));
        }

        private IWebElement GetMenuChildElementByName(string menuElementName)
        {
            var menuElements = _driver.FindElementsByCssSelector("a[name^='menu_subitem_']");
            return menuElements.First(x => x.Text == menuElementName);
        }

        private IWebElement GetMenuChildElementByHref(string href)
        {
            return _driver.FindElement(By.CssSelector($"a[href*='{href}']"));
        }
    }
}