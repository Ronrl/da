﻿using DeskAlerts.UIAutomationTests.Interfaces;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Frames.PolicyGroupsFrame
{
    public class PolicyGroupsFrameActions : IFocusable
    {
        private readonly PolicyGroupsFrameElements _elements;
        private readonly ChromeDriver _driver;

        public PolicyGroupsFrameActions(ChromeDriver driver)
        {
            _elements = new PolicyGroupsFrameElements(driver);
            _driver = driver;
        }

        public void Focus()
        {
            _driver.SwitchTo().Frame(_elements.Frame);
        }
    }
}
