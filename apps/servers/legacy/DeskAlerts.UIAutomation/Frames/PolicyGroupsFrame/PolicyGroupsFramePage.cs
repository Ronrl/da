﻿using DeskAlerts.UIAutomationTests.Frames.BaseFrame;
using DeskAlerts.UIAutomationTests.Interfaces;
using DeskAlerts.UIAutomationTests.Pages.BaseTable;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Frames.PolicyGroupsFrame
{
    public class PolicyGroupsFramePage : BaseTablePage, IFocusable
    {
        private readonly PolicyGroupsFrameActions _actions;
        private readonly IFocusableOnBody _baseFramePage;

        public PolicyGroupsFramePage(ChromeDriver driver) : base(driver)
        {
            _actions = new PolicyGroupsFrameActions(driver);
            _baseFramePage = new BaseFramePage(driver);
        }

        public void Focus()
        {
            _actions.Focus();
        }

        public void FocusOnBody()
        {
            _baseFramePage.FocusOnBody();
        }
    }
}
