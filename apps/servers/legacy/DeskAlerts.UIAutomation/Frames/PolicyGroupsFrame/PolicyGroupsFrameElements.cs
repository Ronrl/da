﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Frames.PolicyGroupsFrame
{
    public class PolicyGroupsFrameElements
    {
        private readonly ChromeDriver _driver;

        public PolicyGroupsFrameElements(ChromeDriver driver)
        {
            _driver = driver;
        }

        public IWebElement Frame => _driver.FindElementById("list1");
    }
}