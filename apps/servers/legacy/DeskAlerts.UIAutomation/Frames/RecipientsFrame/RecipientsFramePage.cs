﻿using DeskAlerts.UIAutomationTests.Frames.BaseFrame;
using DeskAlerts.UIAutomationTests.Interfaces;
using DeskAlerts.UIAutomationTests.Pages.BaseTable;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Frames.RecipientsFrame
{
    public class RecipientsFramePage : BaseTablePage, IFocusable
    {
        private readonly RecipientsFrameActions _actions;
        private readonly IFocusableOnBody _baseFramePage;

        public RecipientsFramePage(ChromeDriver driver) : base(driver)
        {
            _actions = new RecipientsFrameActions(driver);
            _baseFramePage = new BaseFramePage(driver);
        }

        public void SelectByUserName(string userName)
        {
            ClickOnCheckboxByValue(userName);
        }

        public void Focus()
        {
            _actions.Focus();
        }

        public void FocusOnBody()
        {
            _baseFramePage.FocusOnBody();
        }

        public void ClickSortByOnline()
        {
            BaseTableActions.ClickCellById("onlineCell");
        }

        public string GetFirstCellOfFirstRowValue()
        {
            var firstRowValue = BaseTableActions.GetTableRow(1).Text;
            var firstCellValue = firstRowValue.Substring(0, firstRowValue.IndexOf(' '));
            return firstCellValue;
        }
    }
}
