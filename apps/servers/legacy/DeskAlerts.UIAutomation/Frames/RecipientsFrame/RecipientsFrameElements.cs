﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Frames.RecipientsFrame
{
    public class RecipientsFrameElements
    {
        private readonly ChromeDriver _driver;

        public RecipientsFrameElements(ChromeDriver driver)
        {
            _driver = driver;
        }

        public IWebElement Frame => _driver.FindElementById("content_iframe");
    }
}