﻿using DeskAlerts.UIAutomationTests.Frames.BaseFrame;
using DeskAlerts.UIAutomationTests.Interfaces;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Frames.RecipientsFrame
{
    public class RecipientsFrameActions : BaseFramePage, IFocusable
    {
        private readonly RecipientsFrameElements _elements;
        private readonly ChromeDriver _driver;

        public RecipientsFrameActions(ChromeDriver driver) : base(driver)
        {
            _elements = new RecipientsFrameElements(driver);
            _driver = driver;
        }

        public void Focus()
        {
            _driver.SwitchTo().Frame(_elements.Frame);
        }
    }
}
