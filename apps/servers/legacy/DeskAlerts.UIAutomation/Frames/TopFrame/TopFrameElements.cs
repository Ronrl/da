﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Frames.TopFrame
{
    public class TopFrameElements
    {
        private readonly ChromeDriver _driver;

        public TopFrameElements(ChromeDriver driver)
        {
            _driver = driver;
        }

        public IWebElement Frame => _driver.FindElementById("top");

        public IWebElement SettingsButton => _driver.FindElementById("settingsButton");

        public IWebElement LogoutButton => _driver.FindElementById("logout");

        public IWebElement LoginUsername => _driver.FindElementByClassName("username");

    }
}
