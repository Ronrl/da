﻿using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Frames.TopFrame
{
    public class TopFrameActions
    {
        private readonly TopFrameElements _elements;
        private readonly ChromeDriver _driver;

        public TopFrameActions(ChromeDriver driver)
        {
            _elements = new TopFrameElements(driver);
            _driver = driver;
        }

        public void Focus()
        {
            _driver.SwitchTo().DefaultContent();
            _driver.SwitchTo().Frame(_elements.Frame);
        }

        public void ClickSettingsButton()
        {
            _elements.SettingsButton.Click();
        }

        public string GetCurrentLoggedPublisherName()
        {
            return _elements.LoginUsername.Text;
        }

        public void ClickLogoutButton()
        {
            _elements.LogoutButton.Click();
        }
    }
}
