﻿using DeskAlerts.UIAutomationTests.Frames.BaseFrame;
using DeskAlerts.UIAutomationTests.Interfaces;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Frames.TopFrame
{
    public class TopFramePage : BaseFramePage, IFocusable
    {
        private readonly TopFrameActions _actions;

        public TopFramePage(ChromeDriver driver) : base(driver)
        {
            _actions = new TopFrameActions(driver);
        }

        public void Focus()
        {
            _actions.Focus();
        }

        public string GetCurrentLoggedPublisherName()
        {
            return _actions.GetCurrentLoggedPublisherName();
        }

        public void ClickSettingsButton()
        {
            _actions.Focus();
            _actions.ClickSettingsButton();
            FocusOnBody();
        }

        public void ClickLogoutButton()
        {
            _actions.Focus();
            _actions.ClickLogoutButton();
        }
    }
}
