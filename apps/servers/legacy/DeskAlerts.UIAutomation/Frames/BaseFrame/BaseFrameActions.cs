﻿using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Frames.BaseFrame
{
    public class BaseFrameActions
    {
        private readonly BaseFrameElements _elements;
        private readonly ChromeDriver _driver;

        public BaseFrameActions(ChromeDriver driver)
        {
            _elements = new BaseFrameElements(driver);
            _driver = driver;
        }

        public void FocusOnBody()
        {
            _driver.SwitchTo().DefaultContent();
            _driver.SwitchTo().Frame(_elements.MainFrame);
            _driver.SwitchTo().Frame(_elements.WorkFrame);
        }
    }
}
