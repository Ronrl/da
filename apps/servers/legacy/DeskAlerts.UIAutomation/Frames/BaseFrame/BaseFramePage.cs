﻿using DeskAlerts.UIAutomationTests.Interfaces;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Frames.BaseFrame
{
    public class BaseFramePage : IFocusableOnBody
    {
        private readonly BaseFrameActions _actions;

        public BaseFramePage(ChromeDriver driver)
        {
            _actions = new BaseFrameActions(driver);
        }

        public void FocusOnBody()
        {
            _actions.FocusOnBody();
        }
    }
}
