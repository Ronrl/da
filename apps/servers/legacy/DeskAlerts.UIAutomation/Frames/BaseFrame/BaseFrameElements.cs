﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Frames.BaseFrame
{
    public class BaseFrameElements
    {
        private readonly ChromeDriver _driver;

        public BaseFrameElements(ChromeDriver driver)
        {
            _driver = driver;
        }

        public IWebElement MainFrame => _driver.FindElementById("main");

        public IWebElement TopFrame => _driver.FindElementById("top");

        public IWebElement WorkFrame => _driver.FindElementById("main_frame");

        
    }
}
