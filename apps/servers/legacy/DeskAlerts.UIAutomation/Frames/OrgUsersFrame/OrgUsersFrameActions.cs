﻿using DeskAlerts.UIAutomationTests.Interfaces;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Frames.OrgUsersFrame
{
    public class OrgUsersFrameActions : IFocusable
    {
        private readonly OrgUsersFrameElements _elements;
        private readonly ChromeDriver _driver;

        public OrgUsersFrameActions(ChromeDriver driver)
        {
            _elements = new OrgUsersFrameElements(driver);
            _driver = driver;
        }

        public void Focus()
        {
            _driver.SwitchTo().Frame(_elements.Frame);
        }
    }
}
