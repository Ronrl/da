﻿using DeskAlerts.UIAutomationTests.Frames.BaseFrame;
using DeskAlerts.UIAutomationTests.Interfaces;
using DeskAlerts.UIAutomationTests.Pages.BaseTable;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Frames.OrgUsersFrame
{
    public class OrgUsersFramePage : BaseTablePage, IFocusable
    {
        private readonly OrgUsersFrameActions _actions;
        private readonly IFocusableOnBody _baseFramePage;

        public OrgUsersFramePage(ChromeDriver driver) : base(driver)
        {
            _actions = new OrgUsersFrameActions(driver);
            _baseFramePage = new BaseFramePage(driver);
        }

        public void Focus()
        {
            _actions.Focus();
        }

        public void FocusOnBody()
        {
            _baseFramePage.FocusOnBody();
        }
    }
}
