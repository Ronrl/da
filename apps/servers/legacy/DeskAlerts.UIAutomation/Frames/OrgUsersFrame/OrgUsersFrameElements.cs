﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Frames.OrgUsersFrame
{
    public class OrgUsersFrameElements
    {
        private readonly ChromeDriver _driver;

        public OrgUsersFrameElements(ChromeDriver driver)
        {
            _driver = driver;
        }

        public IWebElement Frame => _driver.FindElementById("objectsFrame");

        public IWebElement MainFrame => _driver.FindElementById("main");

        public IWebElement WorkFrame => _driver.FindElementById("main_frame");
    }
}