﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Frames.PolicyUsersFrame
{
    public class PolicyUsersFrameElements
    {
        private readonly ChromeDriver _driver;

        public PolicyUsersFrameElements(ChromeDriver driver)
        {
            _driver = driver;
        }

        public IWebElement Frame => _driver.FindElementByLinkText("PolicyUsers.aspx");
    }
}