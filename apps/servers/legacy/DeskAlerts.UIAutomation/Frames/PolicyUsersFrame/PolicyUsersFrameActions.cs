﻿using DeskAlerts.UIAutomationTests.Frames.PolicyGroupsFrame;
using DeskAlerts.UIAutomationTests.Interfaces;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Frames.PolicyUsersFrame
{
    public class PolicyUsersFrameActions : IFocusable
    {
        private readonly PolicyGroupsFrameElements _elements;
        private readonly ChromeDriver _driver;

        public PolicyUsersFrameActions(ChromeDriver driver)
        {
            _elements = new PolicyGroupsFrameElements(driver);
            _driver = driver;
        }

        public void Focus()
        {
            _driver.SwitchTo().Frame(_elements.Frame);
        }
    }
}
