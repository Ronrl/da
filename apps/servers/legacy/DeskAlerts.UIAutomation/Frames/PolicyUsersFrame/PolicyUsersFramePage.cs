﻿using DeskAlerts.UIAutomationTests.Frames.BaseFrame;
using DeskAlerts.UIAutomationTests.Interfaces;
using DeskAlerts.UIAutomationTests.Pages.BaseTable;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Frames.PolicyUsersFrame
{
    public class PolicyUsersFramePage : BaseTablePage, IFocusable
    {
        private readonly PolicyUsersFrameActions _actions;
        private readonly IFocusableOnBody _baseFramePage;

        public PolicyUsersFramePage(ChromeDriver driver) : base(driver)
        {
            _actions = new PolicyUsersFrameActions(driver);
            _baseFramePage = new BaseFramePage(driver);
        }

        public void Focus()
        {
            _actions.Focus();
        }

        public void FocusOnBody()
        {
            _baseFramePage.FocusOnBody();
        }
    }
}
