﻿using DeskAlerts.UIAutomationTests.Frames.BaseFrame;
using DeskAlerts.UIAutomationTests.Interfaces;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Frames.VideoalertFilesFrame
{
    public class VideoalertFilesFrame : BaseFramePage, IFocusable
    {
        private readonly VideoalertFilesFrameActions _actions;

        public VideoalertFilesFrame(ChromeDriver driver) : base(driver)
        {
            _actions = new VideoalertFilesFrameActions(driver);
        }

        public void Focus()
        {
            _actions.Focus();
        }

        public void SelectUploadedVideo(string filename)
        {
            _actions.SelectUploadedVideo(filename);
        }
    }
}
