﻿using DeskAlerts.UIAutomationTests.Frames.BaseFrame;
using DeskAlerts.UIAutomationTests.Interfaces;
using OpenQA.Selenium.Chrome;
using System;
using System.Linq;

namespace DeskAlerts.UIAutomationTests.Frames.VideoalertFilesFrame
{
    public class VideoalertFilesFrameActions : BaseFramePage, IFocusable
    {
        private readonly VideoalertFilesFrameElements _elements;
        private readonly ChromeDriver _driver;

        public VideoalertFilesFrameActions(ChromeDriver driver) : base(driver)
        {
            _elements = new VideoalertFilesFrameElements(driver);
            _driver = driver;
        }

        public void Focus()
        {
            _driver.SwitchTo().Frame(_elements.Frame);
        }

        public void SelectUploadedVideo(string filename)
        {
            var link = _elements.UploadedVideos
                .FirstOrDefault(video => video.Text == filename);

            if (link == null)
            {
                throw new NotFoundVideoInUploadedListException("This video file not uploaded");
            }

            link.Click();
        }

        #region Exceptions

        internal class NotFoundVideoInUploadedListException : Exception
        {
            public NotFoundVideoInUploadedListException(string message) : base(message)
            {
            }
        }

        #endregion 
    }
}
