﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Collections.Generic;

namespace DeskAlerts.UIAutomationTests.Frames.VideoalertFilesFrame
{
    public class VideoalertFilesFrameElements
    {
        private readonly ChromeDriver _driver;

        public VideoalertFilesFrameElements(ChromeDriver driver)
        {
            _driver = driver;
        }

        public IWebElement Frame => _driver.FindElementById("files");

        public IReadOnlyCollection<IWebElement> UploadedVideos => _driver.FindElementsByClassName("files_uploaded");
    }
}