﻿using DeskAlerts.UIAutomationTests.Interfaces;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Frames.OrgGroupsFrame
{
    public class OrgGroupsFrameActions : IFocusable
    {
        private readonly OrgGroupsFrameElements _elements;
        private readonly ChromeDriver _driver;

        public OrgGroupsFrameActions(ChromeDriver driver)
        {
            _elements = new OrgGroupsFrameElements(driver);
            _driver = driver;
        }

        public void Focus()
        {
            _driver.SwitchTo().Frame(_elements.Frame);
        }
    }
}
