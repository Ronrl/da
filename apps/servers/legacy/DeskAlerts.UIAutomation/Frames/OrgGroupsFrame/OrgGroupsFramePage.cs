﻿using DeskAlerts.UIAutomationTests.Frames.BaseFrame;
using DeskAlerts.UIAutomationTests.Interfaces;
using DeskAlerts.UIAutomationTests.Pages.BaseTable;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Frames.OrgGroupsFrame
{
    public class OrgGroupsFramePage : BaseTablePage, IFocusable
    {
        private readonly OrgGroupsFrameActions _actions;
        private readonly IFocusableOnBody _baseFramePage;

        public OrgGroupsFramePage(ChromeDriver driver) : base(driver)
        {
            _actions = new OrgGroupsFrameActions(driver);
            _baseFramePage = new BaseFramePage(driver);
        }

        public void Focus()
        {
            _actions.Focus();
        }

        public void FocusOnBody()
        {
            _baseFramePage.FocusOnBody();
        }

        public string GetCellValueByIndexAndSearchValue(string searchValue, int index)
        {
            var policyRow = BaseTableActions.GetRowInTableByColumnValue(searchValue);
            return BaseTableActions.GetCellValueByIndexAndSearchValue(policyRow, index);
        }
    }
}
