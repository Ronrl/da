﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.SettingsSelection
{
    public class SettingsSelectionElements
    {
        private readonly ChromeDriver _driver;

        public SettingsSelectionElements(ChromeDriver driver)
        {
            _driver = driver;
        }

        public IWebElement ProfileSettings => _driver.FindElementById("profileSettings");
    }
}