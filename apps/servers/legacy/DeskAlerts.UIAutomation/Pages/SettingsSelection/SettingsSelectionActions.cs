﻿using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.SettingsSelection
{
    public class SettingsSelectionActions
    {
        private readonly SettingsSelectionElements _elements;

        public SettingsSelectionActions(ChromeDriver driver)
        {
            _elements = new SettingsSelectionElements(driver);
        }

        public void ClickProfileSettings()
        {
            _elements.ProfileSettings.Click();
        }
    }
}
