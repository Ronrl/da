﻿using DeskAlerts.UIAutomationTests.Pages.BaseTable;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.SettingsSelection
{
    public class SettingsSelectionPage : BaseTablePage
    {
        private readonly SettingsSelectionActions _actions;

        public SettingsSelectionPage(ChromeDriver driver) : base(driver)
        {
            _actions = new SettingsSelectionActions(driver);
        }

        public void ClickProfileSettings()
        {
            _actions.ClickProfileSettings();
        }
    }
}
