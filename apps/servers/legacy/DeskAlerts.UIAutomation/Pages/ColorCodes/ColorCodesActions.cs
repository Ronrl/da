﻿using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.ColorCodes
{
    public class ColorCodesActions
    {
        private readonly ColorCodesElements _elements;

        public ColorCodesActions(ChromeDriver driver)
        {
            _elements = new ColorCodesElements(driver);
        }

        public void CreateColorCode()
        {
            _elements.AddColorCode.Click();
        }

    }
}
