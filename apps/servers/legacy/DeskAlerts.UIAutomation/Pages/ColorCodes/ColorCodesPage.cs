﻿using DeskAlerts.UIAutomationTests.Pages.BaseTable;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.ColorCodes
{
    public class ColorCodesPage : BaseTablePage
    {
        private readonly ColorCodesActions _actions;

        public ColorCodesPage(ChromeDriver driver) : base(driver)
        {
            _actions = new ColorCodesActions(driver);
        }

        public void EditColorCode(string colorCodeName)
        {
            ClickOnAction(colorCodeName, 3, 0);
        }

        public void CreateColorCode()
        {
            _actions.CreateColorCode();
        }
    }
}
