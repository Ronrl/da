﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.ColorCodes
{
    public class ColorCodesElements
    {
        private readonly ChromeDriver _driver;

        public ColorCodesElements(ChromeDriver driver)
        {
            _driver = driver;
        }

        public IWebElement AddColorCode => _driver.FindElementById("addButton");

    }
}
