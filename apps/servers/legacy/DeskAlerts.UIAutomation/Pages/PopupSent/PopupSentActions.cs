﻿using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.PopupSent
{
    public class PopupSentActions
    {
        private readonly PopupSentElements _elements;

        public PopupSentActions(ChromeDriver driver)
        {
            _elements = new PopupSentElements(driver);
        }
    }
}
