﻿using DeskAlerts.UIAutomationTests.Pages.BaseTable;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.PopupSent
{
    public class PopupSentPage : BaseTablePage
    {
        private readonly PopupSentActions _actions;

        public PopupSentPage(ChromeDriver driver) : base(driver)
        {
            _actions = new PopupSentActions(driver);
        }

        public void DuplicateAlert(string alertTitle)
        {
              ClickOnAction(alertTitle, 7, 1);
        }

        public void DuplicateRsvp(string alertTitle)
        {
            ClickOnAction(alertTitle, 7, 0);
        }

        public void DuplicateSurvey(string surveyTitle)
        {
            ClickOnAction(surveyTitle, 8, 1);
        }
    }
}
