﻿using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.EditGroups
{
    public class EditGroupsActions
    {
        private readonly EditGroupsElements _elements;

        public EditGroupsActions(ChromeDriver driver)
        {
            _elements = new EditGroupsElements(driver);
        }

        public void SetGroupName(string name)
        {
            _elements.GroupNameInput.Clear();
            _elements.GroupNameInput.SendKeys(name);
        }

        public void ClickNextButton()
        {
            _elements.NextButton.Click();
        }
    }
}
