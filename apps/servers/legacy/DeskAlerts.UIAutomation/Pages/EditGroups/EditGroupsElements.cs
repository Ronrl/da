﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.EditGroups
{
    public class EditGroupsElements
    {
        private readonly ChromeDriver _driver;

        public EditGroupsElements(ChromeDriver driver)
        {
            _driver = driver;
        }

        public IWebElement GroupNameInput =>_driver.FindElementById("groupNameInputValue");

        public IWebElement NextButton => _driver.FindElementById("saveButton");

        public IWebElement CancelButton => _driver.FindElementByCssSelector("cancel_button");
    }
}
