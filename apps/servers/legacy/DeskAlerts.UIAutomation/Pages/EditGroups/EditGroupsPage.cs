﻿using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.EditGroups
{
    public class EditGroupsPage
    {
        private readonly EditGroupsActions _actions;

        public EditGroupsPage(ChromeDriver driver)
        {
            _actions = new EditGroupsActions(driver);
        }

        public void AddGroup(string groupName)
        {
            _actions.SetGroupName(groupName);
            _actions.ClickNextButton();
        }
    }
}
