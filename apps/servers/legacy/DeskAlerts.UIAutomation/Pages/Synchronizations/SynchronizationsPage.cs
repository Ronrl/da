﻿using DeskAlerts.UIAutomationTests.Pages.BaseTable;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.Synchronizations
{
    public class SynchronizationsPage : BaseTablePage
    {
        private readonly SynchronizationsActions _actions;

        public SynchronizationsPage(ChromeDriver driver) : base(driver)
        {
            _actions = new SynchronizationsActions(driver);
        }

        public void ClickAddNewSynchronizationButton()
        {
            _actions.ClickAddNewSynchronizationButton();
        }
    }
}
