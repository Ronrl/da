using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.Synchronizations
{
    public class SynchronizationsActions
    {
        private readonly SynchronizationsElements _elements;

        public SynchronizationsActions(ChromeDriver driver)
        {
            _elements = new SynchronizationsElements(driver);
        }

        public void ClickAddNewSynchronizationButton()
        {
            _elements.AddNewSynchronizationButton().Click();
        }
    }
}
