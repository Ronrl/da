﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.Synchronizations
{
    public class SynchronizationsElements
    {
        private readonly ChromeDriver _driver;

        public SynchronizationsElements(ChromeDriver driver)
        {
            _driver = driver;
        }

        public IWebElement AddNewSynchronizationButton()
        {
            return _driver.FindElementById("addButton");
        }
    }
}