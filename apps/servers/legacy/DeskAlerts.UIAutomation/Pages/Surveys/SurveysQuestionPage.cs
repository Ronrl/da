﻿using DeskAlerts.UIAutomationTests.Frames.BaseFrame;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.Surveys
{
    public class SurveysQuestionPage : BaseFramePage
    {
        private readonly SurveysQuestionActions _actions;

        public SurveysQuestionPage(ChromeDriver driver) : base(driver)
        {
            _actions = new SurveysQuestionActions(driver);
        }

        public void SetTitle(string title)
        {
            _actions.FocusOnTitle();
            _actions.SetTitle(title);
            _actions.FocusOnParent();
        }

        public void SetOption(int index, string option)
        {
            _actions.FocusOnOption(index);
            _actions.SetOption(option);
            _actions.FocusOnParent();
        }

        public void SetOptionsBySameValues(int optionsTotal, string option)
        {
            for (var i = 0; i < optionsTotal; i++)
            {
                _actions.FocusOnOption(i);
                _actions.SetOption(option);
                _actions.FocusOnParent();
            }
        }

        public void ClickOnAddOptionButton()
        {
            _actions.ClickOnAddOptionButton();
        }

        public void ClickOnSaveAndAddNewQuestionButton()
        {
            _actions.ClickOnSaveAndAddNewQuestionButton();
        }

        public void ClickOnSaveAndNextButton()
        {
            _actions.ClickOnSaveAndNextButton();
        }
    }
}
