﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.Surveys
{
    public class SurveysElements
    {
        private readonly ChromeDriver _driver;

        public SurveysElements(ChromeDriver driver)
        {
            _driver = driver;
        }

        public IWebElement TitleFrame => _driver.FindElementById("title_survey_ifr");

        public IWebElement Title => _driver.FindElementById("tinymce");

        public IWebElement NextButton => _driver.FindElementByClassName("save_and_next_button");
    }
}