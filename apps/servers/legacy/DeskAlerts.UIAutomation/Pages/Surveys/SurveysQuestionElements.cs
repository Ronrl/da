﻿using System.Collections.Generic;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.Surveys
{
    public class SurveysQuestionElements
    {
        private readonly ChromeDriver _driver;

        public SurveysQuestionElements(ChromeDriver driver)
        {
            _driver = driver;
        }

        public IWebElement TitleFrame => _driver.FindElementById("title_survey_ifr");

        public IWebElement Title => _driver.FindElementByCssSelector("body[data-id=\"title_survey\"]");

        public IEnumerable<IWebElement> OptionsFrames => _driver.FindElementsByCssSelector("iframe[id^=option]");

        public IWebElement Option => _driver.FindElementByCssSelector("body[data-id^=\"option\"]");

        public IWebElement AddOptionButton => _driver.FindElementByClassName("add_option");

        public IWebElement SaveAndAddNewQuestionButton => _driver.FindElementById("add_question");

        public IWebElement SaveAndNextButton => _driver.FindElementById("end_questions");
    }
}