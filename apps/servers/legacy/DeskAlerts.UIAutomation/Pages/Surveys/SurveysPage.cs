﻿using DeskAlerts.UIAutomationTests.Frames.BaseFrame;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.Surveys
{
    public class SurveysPage : BaseFramePage
    {
        private readonly SurveysActions _actions;

        public SurveysPage(ChromeDriver driver) : base(driver)
        {
            _actions = new SurveysActions(driver);
        }

        public void SetTitle(string text)
        {
            _actions.FocusOnTitleFrame();
            _actions.SetTitle(text);
            FocusOnBody();
        }

        public void ClickNext()
        {
            _actions.ClickNext();
        }
    }
}
