using System.Linq;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.Surveys
{
    public class SurveysQuestionActions
    {
        private readonly SurveysQuestionElements _elements;
        private readonly ChromeDriver _driver;

        public SurveysQuestionActions(ChromeDriver driver)
        {
            _elements = new SurveysQuestionElements(driver);
            _driver = driver;
        }

        public void FocusOnParent()
        {
            _driver.SwitchTo().ParentFrame();
        }

        public void FocusOnTitle()
        {
            _driver.SwitchTo().Frame(_elements.TitleFrame);
        }

        public void FocusOnOption(int index)
        {
            var frame = _elements.OptionsFrames.ElementAt(index);
            _driver.SwitchTo().Frame(frame);
        }

        public void SetTitle(string title)
        {
            _elements.Title.Clear();
            _elements.Title.SendKeys(title);
        }

        public void SetOption(string option)
        {
            _elements.Option.Clear();
            _elements.Option.SendKeys(option);
        }

        public void ClickOnAddOptionButton()
        {
            _elements.AddOptionButton.Click();
        }

        public void ClickOnSaveAndAddNewQuestionButton()
        {
            _elements.SaveAndAddNewQuestionButton.Click();
        }

        public void ClickOnSaveAndNextButton()
        {
            _elements.SaveAndNextButton.Click();
        }
    }
}
