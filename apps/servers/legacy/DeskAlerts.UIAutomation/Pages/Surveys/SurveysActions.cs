using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.Surveys
{
    public class SurveysActions
    {
        private readonly SurveysElements _elements;
        private readonly ChromeDriver _driver;

        public SurveysActions(ChromeDriver driver)
        {
            _elements = new SurveysElements(driver);
            _driver = driver;
        }

        public void FocusOnTitleFrame()
        {
            _driver.SwitchTo().Frame(_elements.TitleFrame);
        }

        public void SetTitle(string text)
        {
            _elements.Title.Clear();
            _elements.Title.SendKeys(text);
        }

        public void ClickNext()
        {
            _elements.NextButton.Click();
        }
    }
}
