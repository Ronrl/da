﻿using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.RecipientsSelection
{
    public class RecipientsSelectionActions
    {
        private readonly RecipientsSelectionElements _elements;
        private readonly ChromeDriver _driver;

        public RecipientsSelectionActions(ChromeDriver driver)
        {
            _elements = new RecipientsSelectionElements(driver);
            _driver = driver;
        }

        public void ClickSendButton()
        {
            _elements.SendButton.Click();
        }

        public void SelectBroadcast()
        {
            _elements.SelectAlertType.SelectByValue("B");
        }

        public void SelectRecipients()
        {
            _elements.SelectAlertType.SelectByValue("R");
        }
    }
}
