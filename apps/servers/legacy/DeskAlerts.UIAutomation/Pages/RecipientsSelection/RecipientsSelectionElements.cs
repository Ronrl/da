﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace DeskAlerts.UIAutomationTests.Pages.RecipientsSelection
{
    public class RecipientsSelectionElements
    {
        private readonly ChromeDriver _driver;

        public RecipientsSelectionElements(ChromeDriver driver)
        {
            _driver = driver;
        }

        public IWebElement SendButton => _driver.FindElementById("sendButton");

        public SelectElement SelectAlertType => new SelectElement(_driver.FindElementById("objectType"));
    }
}