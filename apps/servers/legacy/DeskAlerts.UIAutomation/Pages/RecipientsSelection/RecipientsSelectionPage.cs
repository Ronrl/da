﻿using DeskAlerts.UIAutomationTests.Pages.BaseTable;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.RecipientsSelection
{
    public class RecipientsSelectionPage
    {
        private readonly RecipientsSelectionActions _actions;

        public RecipientsSelectionPage(ChromeDriver driver)
        {
            _actions = new RecipientsSelectionActions(driver);
        }

        public void SendBroadcast()
        {
            _actions.SelectBroadcast();
        }

        public void SelectRecipients()
        {
            _actions.SelectRecipients();
        }

        public void ClickSendButton()
        {
            _actions.ClickSendButton();
        }
    }
}
