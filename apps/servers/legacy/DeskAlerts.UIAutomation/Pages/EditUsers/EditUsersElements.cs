﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.EditUsers
{
    public class EditUsersElements
    {
        private readonly ChromeDriver _driver;

        public EditUsersElements(ChromeDriver driver)
        {
            _driver = driver;
        }

        public IWebElement GetName()
        {
            return _driver.FindElementById("userName");
        }

        public IWebElement GetDisplayName()
        {
            return _driver.FindElementById("displayName");
        }

        public IWebElement GetEmail()
        {
            return _driver.FindElementById("email");
        }

        public IWebElement GetPassword()
        {
            return _driver.FindElementById("password");
        }

        public IWebElement GetMobilePhone()
        {
            return _driver.FindElementById("mobilePhone");
        }

        public IWebElement GetAddButton()
        {
            return _driver.FindElementById("addUserButton");
        }
    }
}
