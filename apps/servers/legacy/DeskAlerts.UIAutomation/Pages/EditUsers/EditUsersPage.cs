﻿using System;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.EditUsers
{
    public class EditUsersPage
    {
        private readonly EditUsersActions _actions;

        public EditUsersPage(ChromeDriver driver)
        {
            _actions = new EditUsersActions(driver);
        }

        public void AddUser(string userName, string userPassword)
        {
            AddUser(userName, string.Empty, userPassword, string.Empty, string.Empty);
        }

        public void AddUser(string userName, string displayName, string password, string email, string mobile)
        {
            _actions.SetName(userName);
            _actions.SetDisplayName(displayName);
            _actions.SetPassword(password);
            _actions.SetEmail(email);
            _actions.SetMobilePhone(mobile);
        }

        public void ClickAdd()
        {
            _actions.ClickAddButton();
        }
    }
}
