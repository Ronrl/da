﻿using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.EditUsers
{
    public class EditUsersActions
    {
        private readonly EditUsersElements _elements;

        public EditUsersActions(ChromeDriver driver)
        {
            _elements = new EditUsersElements(driver);
        }

        public void SetName(string name)
        {
            _elements.GetName().SendKeys(name);
        }

        public void SetDisplayName(string displayName)
        {
            _elements.GetDisplayName().SendKeys(displayName);
        }

        public void SetPassword(string password)
        {
            _elements.GetPassword().SendKeys(password);
        }

        public void SetEmail(string email)
        {
            _elements.GetEmail().SendKeys(email);
        }

        public void SetMobilePhone(string mobilePhone)
        {
            _elements.GetMobilePhone().SendKeys(mobilePhone);
        }

        public void ClickAddButton()
        {
            _elements.GetAddButton().Click();
        }
    }
}
