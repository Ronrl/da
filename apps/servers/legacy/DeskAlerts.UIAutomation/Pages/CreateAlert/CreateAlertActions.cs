﻿using DeskAlerts.ApplicationCore.Entities;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.CreateAlert
{
    public class CreateAlertActions
    {
        private readonly CreateAlertElements _elements;

        public CreateAlertActions(ChromeDriver driver)
        {
            _elements = new CreateAlertElements(driver);
        }

        public void SelectAlertType(AlertType alertType)
        {
            _elements.SelectAlertType(alertType);
        }

        public void ClickSaveAndNext()
        {
            _elements.SaveAndNextButton.Click();
        }

        public void ClickSave()
        {
            _elements.SaveButton.Click();
        }

        public void SelectColorCode(string colorCodeName)
        {
            _elements.ColorCodeSelection(colorCodeName).Click();
        }

        public string GetSelectedColorCode()
        {
            return _elements.ColorCodeDiv.GetCssValue("background-color");
        }

        public void ClickColorCode()
        {
            _elements.ColorCodeDiv.Click();
        }

        public void ClickAknowledgmentCheckbox()
        {
            _elements.AknowledgmentCheckbox.Click();
        }
    }
}
