﻿using DeskAlerts.ApplicationCore.Entities;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.CreateAlert
{
    public class CreateAlertElements
    {
        private readonly ChromeDriver _driver;

        public CreateAlertElements(ChromeDriver driver)
        {
            _driver = driver;
        }

        public IWebElement TitleFrame => _driver.FindElementById("htmlTitle_ifr");

        public IWebElement BodyFrame => _driver.FindElementById("alertContent_ifr");

        public IWebElement Body => _driver.FindElementByXPath("//*[@id=\"tinymce\"]");

        public IWebElement SaveAndNextButton => _driver.FindElementById("saveNextButton");

        public IWebElement SaveButton => _driver.FindElementById("saveButton");

        public IWebElement ColorCodeDiv => _driver.FindElementById("changeColorCode");

        public IWebElement ColorCodeSelection(string colorCodeName) => _driver.FindElementByName(colorCodeName);

        public IWebElement AknowledgmentCheckbox => _driver.FindElementById("aknown");

        public void SelectAlertType(AlertType alertType)
        {
            switch (alertType)
            {
                case AlertType.SimpleAlert:
                    _driver.FindElementById("alertTypeTab").Click();
                    break;
                case AlertType.Ticker:
                    _driver.FindElementById("tickerTypeTab").Click();
                    break;
                case AlertType.Rsvp:
                    _driver.FindElementById("rsvpTypeTab").Click();
                    break;
            }
        }
    }
}