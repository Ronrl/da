﻿using DeskAlerts.ApplicationCore.Entities;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.CreateAlert
{
    public class CreateAlertPage
    {
        private readonly CreateAlertActions _actions;

        public CreateAlertPage(ChromeDriver driver)
        {
            _actions = new CreateAlertActions(driver);
        }

        public void SelectAlertType(AlertType alertType)
        {
            _actions.SelectAlertType(alertType);
        }

        public void ClickSaveAndNext()
        {
            _actions.ClickSaveAndNext();
        }

        public void ClickColorCode()
        {
            _actions.ClickColorCode();
        }

        public bool CheckColorCode(string colorCodeValue)
        {
            return _actions.GetSelectedColorCode() == colorCodeValue ? true : false;
        }

        public void SelectColorCode(string colorCodeName)
        {
            _actions.SelectColorCode(colorCodeName);
        }

        public void ClickSave()
        {
            _actions.ClickSave();
        }

        public void ClickAknowledgmentCheckbox()
        {
            _actions.ClickAknowledgmentCheckbox();
        }
    }
}
