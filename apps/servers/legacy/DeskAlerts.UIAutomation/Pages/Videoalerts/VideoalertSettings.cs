﻿namespace DeskAlerts.UIAutomationTests.Pages.Videoalerts
{
    public struct VideoalertSettings
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Filename { get; set; }
    }
}