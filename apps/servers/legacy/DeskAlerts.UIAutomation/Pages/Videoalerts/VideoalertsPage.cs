﻿using System;
using DeskAlerts.UIAutomationTests.Frames.VideoalertFilesFrame;
using DeskAlerts.UIAutomationTests.Frames.VideoalertTitleFrame;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.Videoalerts
{
    public class VideoalertsPage
    {                                                              
        private readonly VideoalertTitleFrame _videoalertTitleFrame;
        private readonly VideoalertFilesFrame _videoalertFilesFrame;
        private readonly VideoalertsActions _actions;

        public VideoalertsPage(ChromeDriver driver)
        {
            _actions = new VideoalertsActions(driver);
            _videoalertTitleFrame = new VideoalertTitleFrame(driver);
            _videoalertFilesFrame = new VideoalertFilesFrame(driver);
        }

        public void CreateVideoalert(VideoalertSettings settings)
        {
            _videoalertTitleFrame.Focus();
            _videoalertTitleFrame.SetTitle(settings.Title);
            _videoalertTitleFrame.FocusOnBody();

            _actions.FillDescription(settings.Description);

            _videoalertFilesFrame.Focus();
            _videoalertFilesFrame.SelectUploadedVideo(settings.Filename);
            _videoalertFilesFrame.FocusOnBody();
        }

        public void SaveVideoalert()
        {
            _actions.Save();
        }

        public void SaveAndSendVideoalert()
        {
            _actions.SaveAndNext();
        }

        public void UploadVideo(string filename)
        {
            throw new NotImplementedException();
        }
    }
}