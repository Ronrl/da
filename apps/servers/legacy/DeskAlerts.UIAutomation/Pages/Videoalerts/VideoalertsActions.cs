﻿using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.Videoalerts
{
    public class VideoalertsActions
    {
        private readonly VideoalertsElements _elements;

        public VideoalertsActions(ChromeDriver driver)
        {
            _elements = new VideoalertsElements(driver);
        }

        public void FillDescription(string description)
        {
            var input = _elements.DescriptionInput;

            input.Clear();
            input.SendKeys(description);
        }    

        public void UploadVideo()
        {
            _elements.UploadForm.Click();
        }

        public void Save()
        {
            _elements.SaveButton.Click();
        }

        public void SaveAndNext()
        {
            _elements.SaveAndNextButton.Click();
        }
    }
}
