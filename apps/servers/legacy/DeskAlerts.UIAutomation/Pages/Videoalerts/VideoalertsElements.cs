﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.Videoalerts
{
    public class VideoalertsElements
    {
        private readonly ChromeDriver _driver;

        public VideoalertsElements(ChromeDriver driver)
        {
            _driver = driver;
        }

        public IWebElement DescriptionInput => _driver.FindElementById("miracleText");

        public IWebElement UploadForm => _driver.FindElementById("video_upload");

        public IWebElement SaveButton => _driver.FindElementById("saveButton");

        public IWebElement SaveAndNextButton => _driver.FindElementById("saveNextButton");
    }
}