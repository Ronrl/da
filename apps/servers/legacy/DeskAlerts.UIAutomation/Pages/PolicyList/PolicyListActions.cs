using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.PolicyList
{
    public class PolicyListActions
    {
        private readonly PolicyListElements _elements;
        private readonly ChromeDriver _driver;

        public PolicyListActions(ChromeDriver driver)
        {
            _elements = new PolicyListElements(driver);
            _driver = driver;
        }

        public void ClickAddPolicyButton()
        {
            _elements.AddPolicyButton().Click();
        }

        public void FocusOnEditorsFrame()
        {
            _driver.SwitchTo().Frame(_elements.EditorsFrame);
        }
    }
}
