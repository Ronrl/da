﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.PolicyList
{
    public class PolicyListElements
    {
        private readonly ChromeDriver _driver;

        public PolicyListElements(ChromeDriver driver)
        {
            _driver = driver;
        }

        public IWebElement AddPolicyButton()
        {
            return _driver.FindElementById("addButton");
        }

        public IWebElement EditorsFrame => _driver.FindElementById("editors_frame");
    }
}