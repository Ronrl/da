﻿using DeskAlerts.UIAutomationTests.Pages.BaseTable;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.PolicyList
{
    public class PolicyListTablePage : BaseTablePage
    {
        private readonly PolicyListActions _actions;

        public PolicyListTablePage(ChromeDriver driver) : base(driver)
        {
            _actions = new PolicyListActions(driver);
        }

        public void ClickAddPolicyButton()
        {
            _actions.ClickAddPolicyButton();
        }

        public void EditPolicy(string text)
        {
            BaseTableActions.SearchInput = text;
            BaseTableActions.ClickSearchButton();
            BaseTableActions.ClickOnAction(text, 3, 0);
        }

        public void DuplicatePolicy(string text)
        {
            BaseTableActions.SearchInput = text;
            BaseTableActions.ClickSearchButton();
            BaseTableActions.ClickOnAction(text, 3, 1);
            BaseTableActions.SearchInput = string.Empty;
        }

        public void ClickShowPublishersButtonForPolicy(string policyName)
        {
            BaseTableActions.SearchInput = policyName;
            BaseTableActions.ClickSearchButton();
            BaseTableActions.ClickOnAction(policyName, 3, 2);
        }

        public void FocusOnEditorsFrame()
        {
            _actions.FocusOnEditorsFrame();
        }

        public string GetCellValueByIndexAndSearchValue(string searchValue, int index)
        {
            var policyRow = BaseTableActions.GetRowInTableByColumnValue(searchValue);
            return BaseTableActions.GetCellValueByIndexAndSearchValue(policyRow, index);
        }
    }
}
