﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace DeskAlerts.UIAutomationTests.Pages.AdSynchronizationForm
{
    public class AdSynchronizationFormElements
    {
        private readonly ChromeDriver _driver;

        public AdSynchronizationFormElements(ChromeDriver driver)
        {
            _driver = driver;
        }

        public IWebElement SynchronizationName => _driver.FindElementById("sync_name");
        public IWebElement DomainName => _driver.FindElementById("domain");
        public IWebElement DomainPort => _driver.FindElementById("port");
        public IWebElement DomainUserName => _driver.FindElementById("adLogin");
        public IWebElement DomainUserPassword => _driver.FindElementById("adPassword");
        public IWebElement SaveButton => _driver.FindElementByClassName("save_button");
        public IWebElement CancelButton => _driver.FindElementByClassName("cancel_button");
        public IWebElement AllOrganizationUnits => _driver.FindElementById("ad_select1");
        public IWebElement SelectedOrganizationUnits => _driver.FindElementById("ad_select2");
        public IWebElement DontSynchronizeOrganizationalUnits => _driver.FindElementById("ad_select3");
    }
}