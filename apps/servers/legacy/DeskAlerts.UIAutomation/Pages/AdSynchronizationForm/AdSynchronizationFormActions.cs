﻿using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.AdSynchronizationForm
{
    public class AdSynchronizationFormActions
    {
        private readonly AdSynchronizationFormElements _elements;

        public AdSynchronizationFormActions(ChromeDriver driver)
        {
            _elements = new AdSynchronizationFormElements(driver);
        }

        public string SynchronizationName
        {
            get
            {
                return _elements.SynchronizationName.Text;
            }
            set
            {
                _elements.SynchronizationName.Clear();
                _elements.SynchronizationName.SendKeys(value);
            }
        }

        public string DomainName
        {
            get
            {
                return _elements.DomainName.Text;
            }
            set
            {
                _elements.DomainName.Clear();
                _elements.DomainName.SendKeys(value);
            }
        }

        public string DomainUserName
        {
            get { return _elements.DomainUserName.Text; }
            set
            {
                _elements.DomainUserName.Clear();
                _elements.DomainUserName.SendKeys(value);
            }
        }

        public string DomainUserPassword
        {
            get { return _elements.DomainUserPassword.Text; }
            set
            {
                _elements.DomainUserPassword.Clear();
                _elements.DomainUserPassword.SendKeys(value);
            }
        }

        public string DomainPort
        {
            get { return _elements.DomainPort.Text; }
            set
            {
                _elements.DomainPort.Clear();
                _elements.DomainPort.SendKeys(value);
            }
        }

        public void SynchronizeAllOrganizationalUnits()
        {
            _elements.AllOrganizationUnits.Click();
        }

        public void SynchronizeSelectedOrganizationalUnits()
        {
            _elements.SelectedOrganizationUnits.Click();
        }

        public void DontSynchronizeOrganizationalUnitsClick()
        {
            _elements.DontSynchronizeOrganizationalUnits.Click();
        }

        public void ClickSaveButton()
        {
            _elements.SaveButton.Click();
        }

        public void ClickCancelButton()
        {
            _elements.CancelButton.Click();
        }
    }
}
