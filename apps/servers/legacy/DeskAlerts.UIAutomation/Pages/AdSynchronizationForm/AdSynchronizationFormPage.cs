﻿using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.AdSynchronizationForm
{
    public class AdSynchronizationFormPage
    {
        private readonly AdSynchronizationFormActions _actions;

        public AdSynchronizationFormPage(ChromeDriver driver)
        {
            _actions = new AdSynchronizationFormActions(driver);
        }

        public void CreateSyncronization(string synchronizationName, string domainName, string userName, string userPassword, string port)
        {
            _actions.SynchronizationName = synchronizationName;
            _actions.DomainName = domainName;
            _actions.DomainUserName = userName;
            _actions.DomainUserPassword = userPassword;
            _actions.DomainPort = port;
            _actions.SynchronizeAllOrganizationalUnits();
        }

        public void Save()
        {
            _actions.ClickSaveButton();
        }
    }
}
