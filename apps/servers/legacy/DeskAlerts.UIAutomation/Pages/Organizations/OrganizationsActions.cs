﻿using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.Organizations
{
    public class OrganizationsActions
    {
        private readonly OrganizationsElements _elements;
        private readonly ChromeDriver _driver;

        public OrganizationsActions(ChromeDriver driver)
        {
            _elements = new OrganizationsElements(driver);
            _driver = driver;
        }

        public void ClickAddUserButton()
        {
            _elements.AddUserButton.Click();
        }

        public void ClickSaveButton()
        {
            _elements.SaveButton.Click();
        }

        public void ClickAddGroupButton()
        {
            _elements.AddGroupButton.Click();
        }
        
        public void SelectUsers()
        {
            _elements.ObjectType.SelectByText("Users");
        }

        public void SelectGroups()
        {
            _elements.ObjectType.SelectByText("Groups");
        }

        public void FocusOnOrgUsersFrame()
        {
            _driver.SwitchTo().Frame(_elements.OrgUsersFrame);
        }
    }
}
