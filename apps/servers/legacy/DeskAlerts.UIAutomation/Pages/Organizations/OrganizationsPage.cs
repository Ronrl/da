﻿using DeskAlerts.UIAutomationTests.Pages.BaseTable;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.Organizations
{
    public class OrganizationsPage : BaseTablePage
    {
        private readonly OrganizationsActions _actions;

        public OrganizationsPage(ChromeDriver driver) : base(driver)
        {
            _actions = new OrganizationsActions(driver);
        }

        public void ClickAddUser()
        {
            _actions.ClickAddUserButton();
        }

        public void ClickSaveButton()
        {
            _actions.ClickSaveButton();
        }

        public void ClickAddGroup()
        {
            _actions.ClickAddGroupButton();
        }
        
        public void SelectUsers()
        {
            _actions.SelectUsers();
        }

        public void SelectGroups()
        {
            _actions.SelectGroups();
        }

        public void FocusOnOrgUsersFrame()
        {
            _actions.FocusOnOrgUsersFrame();
        }
    }
}
