﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace DeskAlerts.UIAutomationTests.Pages.Organizations
{
    public class OrganizationsElements
    {
        private readonly ChromeDriver _driver;

        public OrganizationsElements(ChromeDriver driver)
        {
            _driver = driver;
        }

        public IWebElement AddUserButton => _driver.FindElementById("addUserButton");

        public IWebElement AddGroupButton => _driver.FindElementById("addGroupButton");

        public IWebElement SaveButton => _driver.FindElementById("saveBtn");

        public IWebElement OrgUsersFrame => _driver.FindElementById("objectsFrame");

        public SelectElement ObjectType => new SelectElement(_driver.FindElementById("objType"));
    }
}