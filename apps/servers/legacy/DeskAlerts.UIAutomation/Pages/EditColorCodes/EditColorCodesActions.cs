﻿using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.EditColorCodes
{
    public class EditColorCodesActions
    {
        private readonly EditColorCodesElements _elements;

        public EditColorCodesActions(ChromeDriver driver)
        {
            _elements = new EditColorCodesElements(driver);
        }

        public void SetColorCodeName(string colorCodeName)
        {
            _elements.ColorCodeName.Clear();
            _elements.ColorCodeName.SendKeys(colorCodeName);
        }

        public void SetColorCodeColor(string colorHex)
        {
            _elements.ColorCodeDiv.Click();
            _elements.ColorCodeSelector.Clear();
            _elements.ColorCodeSelector.SendKeys(colorHex);
        }

        public (string, string) GetColorCodeValues()
        {
            _elements.ColorCodeDiv.Click();
            var colorHex = _elements.ColorCodeSelector.GetAttribute("value");
            return (_elements.ColorCodeName.GetAttribute("value"), colorHex);
        }

        public void ClickSaveButton()
        {
            _elements.SaveButton.Click();
        }

        public void ClickCancelButton()
        {
            _elements.CancelButton.Click();
        }
    }
}
