﻿using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.EditColorCodes
{
    public class EditColorCodesPage
    {
        private readonly EditColorCodesActions _actions;

        public EditColorCodesPage(ChromeDriver driver)
        {
            _actions = new EditColorCodesActions(driver);
        }

        public void EditColorCode(string colorCodeName, string colorHex)
        {
            _actions.SetColorCodeName(colorCodeName);
            _actions.SetColorCodeColor(colorHex);
            _actions.ClickSaveButton();
        }

        public void CancelEditColorCode()
        {
            _actions.ClickCancelButton();
        }

        public bool CheckChangedColorCodeValues((string, string) colorCodeValues)
        {
            return colorCodeValues.Equals(_actions.GetColorCodeValues());
        }
    }
}
