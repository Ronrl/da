﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.EditColorCodes
{
    public class EditColorCodesElements
    {
        private readonly ChromeDriver _driver;

        public EditColorCodesElements(ChromeDriver driver)
        {
            _driver = driver;
        }

        public IWebElement ColorCodeName => _driver.FindElementById("ccName");

        public IWebElement ColorCodeDiv => _driver.FindElementById("colorPick");

        public IWebElement ColorCodeSelector => _driver.FindElementByClassName("colpick_hex_field").FindElement(By.TagName("input"));

        public IWebElement SaveButton => _driver.FindElementByClassName("save_button");

        public IWebElement CancelButton => _driver.FindElementByClassName("cancel_button");
    }
}
