﻿using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.ViewEditorsList
{
    public class ViewEditorsListActions
    {
        private readonly ViewEditorsListElements _elements;

        public ViewEditorsListActions(ChromeDriver driver)
        {
            _elements = new ViewEditorsListElements(driver);
        }
    }
}
