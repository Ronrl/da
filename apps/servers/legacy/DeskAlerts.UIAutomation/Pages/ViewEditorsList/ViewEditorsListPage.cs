﻿using DeskAlerts.UIAutomationTests.Pages.BaseTable;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.ViewEditorsList
{
    public class ViewEditorsListPage : BaseTablePage
    {
        private readonly ViewEditorsListActions _actions;

        public ViewEditorsListPage(ChromeDriver driver) : base(driver)
        {
            _actions = new ViewEditorsListActions(driver);
        }
    }
}
