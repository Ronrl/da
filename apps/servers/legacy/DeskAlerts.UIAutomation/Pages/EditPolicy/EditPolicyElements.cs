﻿using System.Collections.Generic;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.EditPolicy
{
    public class EditPolicyElements
    {
        private readonly ChromeDriver _driver;

        public EditPolicyElements(ChromeDriver driver)
        {
            _driver = driver;
        }

        public IWebElement PolicyNameInput => _driver.FindElementById("policyName");

        public IWebElement SaveButton => _driver.FindElementById("saveButton");

        public IWebElement CancelButton => _driver.FindElementByClassName("cancel_button");

        public IWebElement SystemAdministratorRadio() => _driver.FindElementById("policySys");

        public IWebElement PublisherRadio() => _driver.FindElementById("policyReg");

        public IWebElement CheckAllCheckbox() => _driver.FindElementById("checkAll");

        public IWebElement ListOfRecipientsTab => _driver.FindElementById("ui-id-2");

        public IWebElement SendingToSpecifiedRecipientsRadio => _driver.FindElementById("canSendContentToChosenUsers");

        public IWebElement OusTab => _driver.FindElementById("ui-id-6");

        public ICollection<IWebElement> OusInRecipientsList => _driver.FindElementsByCssSelector("option[value^=\"ous_\"]");

        public IWebElement AddRecipientButton => _driver.FindElementById("addRecipientButton");
    }
}