﻿using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.EditPolicy
{
    public class EditPolicyPage
    {
        private readonly EditPolicyActions _actions;

        public EditPolicyPage(ChromeDriver driver)
        {
            _actions = new EditPolicyActions(driver);
        }

        public void AddAdminPolicy(string name)
        {
            _actions.PolicyName = name;
            _actions.SelectSystemAdministratorPolicy();
            _actions.ClickSaveButton();
        }

        public void AddNonAdminPolicy(string name)
        {
            _actions.PolicyName = name;
            if (!_actions.IsPublisher)
            {
                _actions.SelectPublisherPolicy();
            }
            if (!_actions.IsGrandFullControl)
            {
                _actions.SelectGrantFullControl();
            }
            _actions.ClickSaveButton();
        }

        public bool IsPublisherWithGrantFullControl()
        {
            var isSelectGrantFullControl = _actions.IsGrandFullControl;

            var isPublisher = _actions.IsPublisher;
            return isPublisher;
        }

        public void SelectPublisherPolicy()
        {
            _actions.SelectPublisherPolicy();
        }

        public void SelectGrantFullControl()
        {
            _actions.SelectGrantFullControl();
        }

        public void SetPolicyName(string name)
        {
            _actions.PolicyName = name;
        }

        public void ChooseListOfRecipientsTab()
        {
            _actions.ChooseListOfRecipientsTab();
        }

        public void ChooseSendingToSpecifiedRecipientsRadio()
        {
            _actions.ChooseSendingToSpecifiedRecipientsRadio();
        }

        public void ChooseOusTab()
        {
            _actions.ChooseOusTab();
        }

        public int GetOusInRecipientsListTotal()
        {
            return _actions.GetOusInRecipientsListTotal();
        }

        public bool IsSelectedSendingToSpecifiedRecipientsRadio()
        {
            return _actions.IsSelectedSendingToSpecifiedRecipientsRadio();
        }

        public void ClickOnAddRecipientButton()
        {
            _actions.ClickOnAddRecipientButton();
        }

        public void Save()
        {
            _actions.ClickSaveButton();
        }

        public void Cancel()
        {
            _actions.ClickCancelButton();
        }
    }
}
