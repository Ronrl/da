﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.EditPolicy
{
    public class EditPolicyActions
    {
        private readonly EditPolicyElements _elements;

        public EditPolicyActions(ChromeDriver driver)
        {
            _elements = new EditPolicyElements(driver);
        }

        public string PolicyName
        {
            get
            {
                return _elements.PolicyNameInput.Text;
            }
            set
            {
                _elements.PolicyNameInput.Clear();
                _elements.PolicyNameInput.SendKeys(value);
            }
        }

        public bool IsPublisher => _elements.PublisherRadio().Selected;

        public bool IsGrandFullControl => _elements.CheckAllCheckbox().Selected;

        public void SelectSystemAdministratorPolicy()
        {
            _elements.SystemAdministratorRadio().Click();
        }

        public void SelectPublisherPolicy()
        {
            _elements.PublisherRadio().Click();
        }

        public void SelectGrantFullControl()
        {
            _elements.CheckAllCheckbox().Click();
        }

        public void ClickSaveButton()
        {
            _elements.SaveButton.Click();
        }

        public void ClickCancelButton()
        {
            _elements.CancelButton.Click();
        }

        public void ChooseListOfRecipientsTab()
        {
            _elements.ListOfRecipientsTab.SendKeys(Keys.Enter);
        }

        public void ChooseSendingToSpecifiedRecipientsRadio()
        {
            _elements.SendingToSpecifiedRecipientsRadio.Click();
        }

        public void ChooseOusTab()
        {
            _elements.OusTab.Click();
        }

        public bool IsSelectedSendingToSpecifiedRecipientsRadio()
        {
            return _elements.SendingToSpecifiedRecipientsRadio.Selected;
        }

        public int GetOusInRecipientsListTotal()
        {
            return _elements.OusInRecipientsList.Count;
        }

        public void ClickOnAddRecipientButton()
        {
            _elements.AddRecipientButton.Click();
        }
    }
}
