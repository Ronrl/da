﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.InstantMessages
{
    public class InstantMessagesActions
    {
        private readonly InstantMessagesElements _elements;
        public InstantMessagesActions(ChromeDriver driver)
        {
            _elements = new InstantMessagesElements(driver);
        }

        public bool IsEmergencyExist(string emergencyAlertName)
        {
            return _elements.EmergencyAlert(emergencyAlertName) != null;
        }

        public void DeleteEmergency(string emergencyAlertName)
        {
            _elements.EmergencyAlert(emergencyAlertName).FindElement(By.ClassName("im_tile_delete")).Click();
        }

        public void EmergencyEdit(string emergencyAlertName)
        {
            _elements.EmergencyAlert(emergencyAlertName).FindElement(By.ClassName("im_tile_edit")).Click();
        }

        public void SendEmergencyAlert(string emergencyAlertName)
        {
            _elements.EmergencyAlert(emergencyAlertName).FindElement(By.ClassName("im_tile_send")).Click();
        }

        public void GetEmergencyAlertPreview(string emergencyAlertName)
        {
            _elements.EmergencyAlert(emergencyAlertName).FindElement(By.ClassName("im_tile_preview")).Click();
        }

        public bool IsPreviewOpen()
        {
            return _elements.PreviewDiv().Displayed;
        }
    }
}
