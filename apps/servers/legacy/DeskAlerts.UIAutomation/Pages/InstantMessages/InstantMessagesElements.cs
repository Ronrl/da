﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.InstantMessages
{
    public class InstantMessagesElements
    {
        private readonly ChromeDriver _driver;

        public InstantMessagesElements(ChromeDriver driver)
        {
            _driver = driver;
        }

        public IWebElement NoAlertsMessage => _driver.FindElementById("noAlertsDiv");

        public IWebElement EmergencyAlert(string emergencyAlertName)
        {

            try
            { 
                return _driver.FindElement(By.Name(emergencyAlertName));
            }
            catch
            {
                return null;
            }
        }

        public IWebElement PreviewDiv() => _driver.FindElementById("alert_preview_dialog");
    }
}
