﻿using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.InstantMessages
{
    public class InstantMessagesPage
    {
        private readonly InstantMessagesActions _actions;

        public InstantMessagesPage(ChromeDriver driver)
        {
            _actions = new InstantMessagesActions(driver);
        }

        public bool IsEmergencyExist(string emergencyAlertName)
        {
            return _actions.IsEmergencyExist(emergencyAlertName);
        }

        public void SendEmergencyAlert(string emergencyAlertName)
        {
            _actions.SendEmergencyAlert(emergencyAlertName);
        }

        public void DeleteEmergency(string emergencyAlertName)
        {
            _actions.DeleteEmergency(emergencyAlertName);
        }

        public void EditEmergency(string emergencyAlertName)
        {
            _actions.EmergencyEdit(emergencyAlertName);
        }

        public void ViewPreview(string emergencyAlertName)
        {
            _actions.GetEmergencyAlertPreview(emergencyAlertName);
        }

        public bool IsPreviewOpen()
        {
            return _actions.IsPreviewOpen();
        }
    }
}
