﻿using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace DeskAlerts.UIAutomationTests.Pages.EditPublisher
{
    public class EditPublisherActions
    {
        private readonly EditPublisherElements _elements;

        public EditPublisherActions(ChromeDriver driver)
        {
            _elements = new EditPublisherElements(driver);
        }

        public string PublisherName
        {
            get
            {
                return _elements.PublisherNameInput.Text;
            }
            set
            {
                _elements.PublisherNameInput.Clear();
                _elements.PublisherNameInput.SendKeys(value);
            }
        }

        public string PublisherPassword
        {
            get
            {
                return _elements.PasswordInput.Text;
            }
            set
            {
                _elements.PasswordInput.Clear();
                _elements.PasswordInput.SendKeys(value);
            }
        }

        public string PublisherPolicy
        {
            get => _elements.PolicySelect.SelectedOption.Text;
            set => _elements.PolicySelect.SelectByText(value);
        }

        public void AddPublisher(string publisherName, string publisherPassword, string publisherPolicy)
        {
            PublisherName = publisherName;
            PublisherPassword = publisherPassword;
            PublisherPolicy = publisherPolicy;
            _elements.AddButton.Click();
        }

        public string GetSelectPolicy()
        {
            var dropdown = new SelectElement(_elements.Policy);
            return dropdown.SelectedOption.Text;
        }

        public void SelectPolicyByName(string policyName)
        {
            var dropdown = new SelectElement(_elements.Policy);
            dropdown.SelectByText(policyName);
        }

        public void ClickSaveButton()
        {
            _elements.SaveButton.Click();
        }
    }
}
