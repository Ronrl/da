﻿using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.EditPublisher
{
    public class EditPublisherPage
    {
        private readonly EditPublisherActions _actions;

        public EditPublisherPage(ChromeDriver driver)
        {
            _actions = new EditPublisherActions(driver);
        }

        public string PublisherName
        {
            get => _actions.PublisherName;
            set => _actions.PublisherName = value;
        }

        public string Password
        {
            get => _actions.PublisherPassword;
            set => _actions.PublisherPassword = value;
        }

        public void AddPublisher(string name, string password, string policyName)
        {
            _actions.AddPublisher(name, password, policyName);
        }

        public string GetSelectPolicy()
        {
            return _actions.GetSelectPolicy();
        }

        public void SelectAndSavePolicyByName(string policyName)
        {
            _actions.SelectPolicyByName(policyName);
            _actions.ClickSaveButton();
        }
    }
}
