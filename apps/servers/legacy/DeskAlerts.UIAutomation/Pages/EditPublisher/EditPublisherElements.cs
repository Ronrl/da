﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace DeskAlerts.UIAutomationTests.Pages.EditPublisher
{
    public class EditPublisherElements
    {
        private readonly ChromeDriver _driver;

        public EditPublisherElements(ChromeDriver driver)
        {
            _driver = driver;
        }

        public IWebElement PublisherNameInput => _driver.FindElementById("nameField");

        public IWebElement PasswordInput => _driver.FindElementById("passwordField");

        public IWebElement Policy => _driver.FindElementById("policySelector");

        public IWebElement SaveButton => _driver.FindElementById("acceptButton");

        public SelectElement PolicySelect => new SelectElement(_driver.FindElementById("policySelector"));

        public IWebElement AddButton => _driver.FindElementById("acceptButton");
    }
}