﻿using DeskAlerts.UIAutomationTests.Pages.BaseTable;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.IpGroups
{
    public class IpGroupsPage : BaseTablePage
    {
        private readonly IpGroupsActions _actions;

        public IpGroupsPage(ChromeDriver driver) : base(driver)
        {
            _actions = new IpGroupsActions(driver);
        }

        public void ClickCreateAudience()
        {
            _actions.ClickCreateAudience();
        }

    }
}
