﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.IpGroups
{
    public class IpGroupsElements
    {

        private readonly ChromeDriver _driver;

        public IpGroupsElements(ChromeDriver driver)
        {
            _driver = driver;
        }

        public IWebElement GetCreateAudienceButton => _driver.FindElementById("addButton");
    }
}
