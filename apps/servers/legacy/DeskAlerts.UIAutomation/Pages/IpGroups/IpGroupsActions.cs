﻿using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.IpGroups
{
    public class IpGroupsActions
    {
        private readonly IpGroupsElements _elements;

        public IpGroupsActions(ChromeDriver driver)
        {
            _elements = new IpGroupsElements(driver);
        }

        public void ClickCreateAudience()
        {
            _elements.GetCreateAudienceButton.Click();
        }
    }
}
