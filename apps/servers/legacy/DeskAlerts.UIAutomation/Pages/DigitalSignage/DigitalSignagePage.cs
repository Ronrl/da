﻿using DeskAlerts.UIAutomationTests.Pages.BaseTable;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.DigitalSignage
{
    public class DigitalSignagePage : BaseTablePage
    {
        private readonly DigitalSignageActions _actions;

        public DigitalSignagePage(ChromeDriver driver) : base(driver)
        {
            _actions = new DigitalSignageActions(driver);
        }

        // Check if this page is current
        public bool IsExistIdentifierProperty()
        {
            return _actions.IsExistIdentifierProperty();
        }

        public void ClickOnNewDeviceButton()
        {
            _actions.ClickOnNewDeviceButton();
        }

        public void SetNewDeviceName(string name)
        {
            _actions.SetNewDeviceName(name);
        }

        public void ClickOnCreateNewDeviceButton()
        {
            _actions.ClickOnCreateNewDeviceButton();
        }
    }
}
