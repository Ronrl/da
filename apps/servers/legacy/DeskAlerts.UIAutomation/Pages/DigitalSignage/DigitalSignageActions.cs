using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.DigitalSignage
{
    public class DigitalSignageActions
    {
        private readonly DigitalSignageElements _elements;

        public DigitalSignageActions(ChromeDriver driver)
        {
            _elements = new DigitalSignageElements(driver);
        }

        public bool IsExistIdentifierProperty()
        {
            try
            {
                return _elements.IdentifierForm.Displayed;
            }
            catch
            {
                return false;
            }
        }

        public void ClickOnNewDeviceButton()
        {
            _elements.NewDeviceButton.Click();
        }

        public void SetNewDeviceName(string name)
        {
            _elements.DeviceNameInput.SendKeys(name);
        }

        public void ClickOnCreateNewDeviceButton()
        {
            _elements.CreateNewDeviceButton.Click();
        }
    }
}
