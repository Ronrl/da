﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.DigitalSignage
{
    public class DigitalSignageElements
    {
        private readonly ChromeDriver _driver;

        public DigitalSignageElements(ChromeDriver driver)
        {
            _driver = driver;
        }

        public IWebElement IdentifierForm =>      
            _driver.FindElementByCssSelector("form[action$=\"DigitalSignageLinks.aspx\"]");

        public IWebElement NewDeviceButton => _driver.FindElementByClassName("add_alert_button");

        public IWebElement DeviceNameInput => _driver.FindElementById("linkValue");

        public IWebElement CreateNewDeviceButton => _driver.FindElementByXPath("//*[@id=\"linkDialog\"]/a");
    }
}