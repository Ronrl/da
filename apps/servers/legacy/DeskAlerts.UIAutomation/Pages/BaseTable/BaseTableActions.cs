﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.BaseTable
{
    public class BaseTableActions
    {
        private readonly BaseTableElements _tableElements;

        public BaseTableActions(ChromeDriver driver)
        {
            _tableElements = new BaseTableElements(driver);
        }

        public string SearchInput
        {
            get
            {
                return _tableElements.SearchInput.Text;
            }
            set
            {
                _tableElements.SearchInput.Clear();
                _tableElements.SearchInput.SendKeys(value);
            }
        }

        public void CleanSearch()
        {
            _tableElements.SearchInput.Clear();
        }

        public void ClickConfirmButton()
        {
            _tableElements.Alert.Accept();
        }

        public void ClickSearchButton()
        {
            _tableElements.SearchButton.Click();
        }

        public bool IsInTable(string value)
        {
            var element = _tableElements.FindValueInTable(value);
            return element != null;
        }

        public void ClickCheckboxByValue(string value)
        {
            _tableElements.GetCheckboxByValue(value).Click();
        }

        public void ClickDeleteButton()
        {
            _tableElements.DeleteButton.Click();
        }

        public void ClickOnAction(string value, int columnIndex, int actionIndex)
        {
            _tableElements.GetActionsByValueAndColmnIndex(value, columnIndex, actionIndex).Click();
        }

        public int GetIndexInTable(string value)
        {
            return _tableElements.GetRowIndexInTableByColumnValue(value);
        }

        public IWebElement GetRowInTableByColumnValue(string valueForSearch)
        {
            return _tableElements.GetRowInTableByColumnValue(valueForSearch);
        }

        public string GetCellValueByIndexAndSearchValue(IWebElement row, int index)
        {
            var rowTd = row.FindElements(By.TagName("td"));
            return rowTd[index].Text;
        }

        public IWebElement GetTableRow(int rowIndex)
        {
            return _tableElements.GetTableRow(rowIndex);
        }

        public void SortBy(string rowName)
        {
            _tableElements.SortBy(rowName).Click();
        }
        
        public void ClickCellById(string id)
        {
            _tableElements.GetCellById(id).Click();
        }
    }
}
