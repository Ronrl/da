using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.BaseTable
{
    public class BaseTablePage
    {
        public BaseTableActions BaseTableActions { get; set; }

        public BaseTablePage(ChromeDriver chromeDriver)
        {
            BaseTableActions = new BaseTableActions(chromeDriver);
        }

        public bool IsInTable(string text)
        {
            return BaseTableActions.IsInTable(text);
        }

        public bool Search(string text)
        {
            BaseTableActions.SearchInput = text;
            BaseTableActions.ClickSearchButton();
            return BaseTableActions.IsInTable(text);
        }

        public void CleanSearch()
        {
            BaseTableActions.CleanSearch();
        }

        public bool IsFound(string text)
        {
            BaseTableActions.SearchInput = text;
            BaseTableActions.ClickSearchButton();

            var isFound = BaseTableActions.IsInTable(text);

            BaseTableActions.SearchInput = string.Empty;
            BaseTableActions.ClickSearchButton();
            return isFound;
        }

        public int GetIndexInTable(string value)
        {
            return BaseTableActions.GetIndexInTable(value);    
        }

        //TODO: this is a crutch
        public bool IsFoundWithoutCleaning(string text)
        {
            BaseTableActions.SearchInput = text;
            BaseTableActions.ClickSearchButton();

            var isFound = BaseTableActions.IsInTable(text);
            return isFound;
        }

        public void SelectByName(string text)
        {
            BaseTableActions.ClickCheckboxByValue(text);
        }

        public void Delete(string text)
        {
            BaseTableActions.SearchInput = text;
            BaseTableActions.ClickSearchButton();

            BaseTableActions.ClickCheckboxByValue(text);
            BaseTableActions.ClickDeleteButton();

            BaseTableActions.SearchInput = string.Empty;
            BaseTableActions.ClickSearchButton();
        }

        public void DeleteWithConfirmation(string text)
        {
            BaseTableActions.SearchInput = text;
            BaseTableActions.ClickSearchButton();

            BaseTableActions.ClickCheckboxByValue(text);
            BaseTableActions.ClickDeleteButton();
            BaseTableActions.ClickConfirmButton();
        }

        public void ClickOnCheckboxByValue(string value)
        {
            BaseTableActions.SearchInput = value;
            BaseTableActions.ClickSearchButton();

            BaseTableActions.ClickCheckboxByValue(value);

            BaseTableActions.SearchInput = string.Empty;
            BaseTableActions.ClickSearchButton();
        }

        public void ClickOnAction(string value, int columnIndex, int actionIndex)
        {
            BaseTableActions.ClickOnAction(value, columnIndex, actionIndex);
        }
    }
}
