﻿using System;
using System.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace DeskAlerts.UIAutomationTests.Pages.BaseTable
{
    public class BaseTableElements
    {
        private readonly ChromeDriver _chromeDriver;

        public BaseTableElements(ChromeDriver chromeDriver)
        {
            _chromeDriver = chromeDriver;
        }

        public IAlert Alert => _chromeDriver.SwitchTo().Alert();

        public IWebElement SearchInput => _chromeDriver.FindElementById("searchTermBox");

        public IWebElement SearchButton => _chromeDriver.FindElementById("searchButton");

        public IWebElement GetRowInTableByColumnValue(string value)
        {
            try
            {
                var tableElement = _chromeDriver.FindElementById("contentTable");
                var tableRows = tableElement.FindElements(By.TagName("tr"));
                foreach (var tableRow in tableRows)
                {
                    var elements = tableRow.FindElements(By.TagName("td"));
                    if (elements.Any(x => x.Text == value))
                    {
                        return tableRow;
                    }
                }

                return null;
            }
            catch
            {
                return null;
            }
        }

        public int GetRowIndexInTableByColumnValue(string value)
        {
            try
            {
                var tableElement = _chromeDriver.FindElementById("contentTable");
                var tableRows = tableElement.FindElements(By.TagName("tr"));
                int rowIndex = 0;
                foreach (var tableRow in tableRows)
                {
                    rowIndex++;
                    var elements = tableRow.FindElements(By.TagName("td"));
                    if (elements.Any(x => x.Text == value))
                    {
                        return rowIndex - 1;
                    }
                    
                }
                return 0;
            }
            catch
            {
                return 0;
            }
        }

        public IWebElement GetTableRow(int rowIndex)
        {
            try
            {
                var tableElement = _chromeDriver.FindElementById("contentTable");
                var tableRows = tableElement.FindElements(By.TagName("tr"));

                return tableRows[rowIndex];
            }
            catch
            {
                return null;
            }
        }

        public IWebElement SortBy(string rowClass)
        {
            return GetTableRow(0).FindElement(By.Id(rowClass)).FindElement(By.TagName("a"));
        }

        public IWebElement FindValueInTable(string value)
        {
            var tableRow = GetRowInTableByColumnValue(value);
            try
            {
                var elements = tableRow.FindElements(By.TagName("td"));
                return elements.Any(x => x.Text == value) ? elements.FirstOrDefault(x => x.Text == value) : null;
            }
            catch
            {
                return null;
            }
        }

        public IWebElement GetCheckboxByValue(string value)
        {
            var tableRow = GetRowInTableByColumnValue(value);
            var elements = tableRow.FindElements(By.TagName("td"));
            return elements[0];
        }

        public IWebElement GetActionsByValueAndColmnIndex(string value, int columnIndex, int actionIndex)
        {
            var tableRow = GetRowInTableByColumnValue(value);
            var elements = tableRow.FindElements(By.TagName("td"));
            var actions = elements[columnIndex].FindElements(By.TagName("a"));
            return actions[actionIndex];
        }

        public IWebElement GetCellById(string id)
        {
            return _chromeDriver.FindElementById(id);
        }

        public IWebElement DeleteButton => _chromeDriver.FindElementById("upDelete");

        public IWebElement ContentTable => _chromeDriver.FindElementById("contentTable");
    }
}
