﻿using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.TextTemplates
{
    class TextTemplatesActions
    {
        private readonly TextTemplatesElements _elements;

        public TextTemplatesActions(ChromeDriver driver)
        {
            _elements = new TextTemplatesElements(driver);
        }
    }
}
