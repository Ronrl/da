﻿using DeskAlerts.UIAutomationTests.Pages.BaseTable;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.TextTemplates
{
    public class TextTemplatesPage : BaseTablePage
    {
        private readonly TextTemplatesActions _actions;
        public TextTemplatesPage(ChromeDriver driver) : base(driver)
        {
            _actions = new TextTemplatesActions(driver); 
        }
        
    }
}
