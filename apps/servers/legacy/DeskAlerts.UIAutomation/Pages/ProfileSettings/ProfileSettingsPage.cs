﻿using DeskAlerts.UIAutomationTests.Pages.BaseTable;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.ProfileSettings
{
    public class ProfileSettingsPage  : BaseTablePage
    {

        private readonly ProfileSettingsActions _actions;

        public ProfileSettingsPage(ChromeDriver driver) : base(driver)
        {
            _actions = new ProfileSettingsActions(driver);
        }


        public bool EditPublisherName()
        {
            return _actions.IsPublisherNameFieldEnabled();
        }

        public void OpenPasswordChangeForm()
        {

            _actions.OpenPasswordChangeForm();
        }

        public void ChangeProfilePassword(string oldPassword, string newPassword, string newPasswordConfirmation)
        {
            _actions.OpenPasswordChangeForm();

            _actions.TypeInOldPasswordField = oldPassword;
            _actions.TypeInNewPasswordField = newPassword;
            _actions.TypeInNewPasswordConfirmationField = newPasswordConfirmation;

            _actions.ClickSaveButton();
            _actions.ConfirmJavascriptAlert();
        }

        public void ClosePasswordForm()
        {
            _actions.ClickCancelPasswordChangingButton();
        }
        
        public bool IsPasswordFormVisible()
        {
            return _actions.IsPasswordFormVisible();
        }

        public string GetErrorText()
        {
            return _actions.GetErrorLabelText();
        }

        public string GetSelectedLanguage()
        {
            return _actions.GetChosenLanguage();
        }

        public void SelectProfileLanguage(string language)
        {
            _actions.ChooseLanguage(language);

            _actions.ClickSaveButton();
            _actions.ConfirmJavascriptAlert();
            
        }

        public void SelectStartPage(string startPage)
        {
            _actions.ChooseStartPage(startPage);

            _actions.ClickSaveButton();
            _actions.ConfirmJavascriptAlert();
        }

        public string GetStartPage()
        {
            return _actions.GetStartPageString();
        }
    }
}
