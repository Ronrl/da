﻿using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.ProfileSettings
{
    public class ProfileSettingsActions
    {

        private readonly ProfileSettingsElements _elements;

        public ProfileSettingsActions(ChromeDriver driver)
        {
            _elements = new ProfileSettingsElements(driver);
        }

        public bool IsPublisherNameFieldEnabled()
        {
            return _elements.ProfileNameField.Enabled;
        }
        public bool IsPasswordFormVisible()
        {
            return _elements.PasswordChangingForm.Displayed;
        }

        public void OpenPasswordChangeForm()
        {
            _elements.PasswordChangeButton.Click();
        }

        public void ClickCancelPasswordChangingButton()
        {
            _elements.CancelPasswordChangingButton.Click();
        }

        public string TypeInOldPasswordField
        {
            get
            {
                return _elements.OldPasswordField.Text;
            }
            set
            {
                _elements.OldPasswordField.Clear();
                _elements.OldPasswordField.SendKeys(value);
            }
        }

        public string TypeInNewPasswordField
        {
            get
            {
                return _elements.NewPasswordField.Text;
            }
            set
            {
                _elements.NewPasswordField.Clear();
                _elements.NewPasswordField.SendKeys(value);
            }
        }

        public string TypeInNewPasswordConfirmationField
        {
            get
            {
                return _elements.NewPasswordConfirmationField.Text;
            }
            set
            {
                _elements.NewPasswordConfirmationField.Clear();
                _elements.NewPasswordConfirmationField.SendKeys(value);
            }
        }

        public void ClickSaveButton()
        {
            _elements.SaveFormButton.Click();
        }

        public string GetErrorLabelText()
        {
            return _elements.ErrorLabel.Text;
        }

        public void ConfirmJavascriptAlert()
        {
            _elements.ConfirmAlert.Accept();
        }

        public void ChooseLanguage(string language)
        {
            _elements.LanguageList.SelectByText(language);
        }

        public string GetChosenLanguage()
        {
            return _elements.LanguageList.SelectedOption.Text;
        }

        public void ChooseStartPage(string startPage)
        {
            _elements.StartPageList.SelectByText(startPage);
        }

        public string GetStartPageString()
        {
            return _elements.StartPageList.SelectedOption.Text;
        }
    }
}
