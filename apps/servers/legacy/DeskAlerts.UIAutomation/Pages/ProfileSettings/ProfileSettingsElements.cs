﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace DeskAlerts.UIAutomationTests.Pages.ProfileSettings
{
    public class ProfileSettingsElements
    {

        private readonly ChromeDriver _driver;

        public ProfileSettingsElements(ChromeDriver driver)
        {
            _driver = driver;
        }

        public IWebElement ProfileNameField => _driver.FindElementById("userName");

        public IWebElement PasswordChangeButton => _driver.FindElementById("change_password_button");

        public IWebElement OldPasswordField => _driver.FindElementById("password_old_value_tr").FindElement(By.Id("password"));

        public IWebElement NewPasswordField => _driver.FindElementById("password_value_tr").FindElement(By.Id("newPassword"));

        public IWebElement NewPasswordConfirmationField => _driver.FindElementById("password_confirm_value_tr").FindElement(By.Id("confirmNewPassword"));

        public IWebElement CancelPasswordChangingButton => _driver.FindElementById("cancel_change_password_button");

        public IWebElement SaveFormButton => _driver.FindElementById("addUserButton");

        public IWebElement PasswordChangingForm => _driver.FindElementById("change_password_tr");

        public IWebElement ErrorLabel => _driver.FindElementById("errorLabel");

        public IAlert ConfirmAlert => _driver.SwitchTo().Alert();

        public SelectElement LanguageList => new SelectElement(_driver.FindElementById("userLangSelect"));

        public SelectElement StartPageList => new SelectElement(_driver.FindElementById("startPage"));
    }
}
