﻿using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.Auth
{
    public class AuthActions
    {
        private readonly AuthElements _authElements;

        public AuthActions(ChromeDriver driver)
        {
            _authElements = new AuthElements(driver);
        }

        public string Login
        {
            get => _authElements.LoginArea.Text;
            set => _authElements.LoginArea.SendKeys(value);
        }

        public string Password
        {
            get => _authElements.PasswordArea.Text;
            set => _authElements.PasswordArea.SendKeys(value);
        }

        public void ClickLoginButton()
        {
            _authElements.LoginButton.Click();
        }

        public bool WrongLogin => _authElements.WrongLoginSpan.Displayed;

        public bool IsDisplayedMessageAboutDisabling => _authElements.DisabledByAdminSpan.Displayed;
    }
}
