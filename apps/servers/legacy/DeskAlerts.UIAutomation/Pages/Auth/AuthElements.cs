﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.Auth
{
    public class AuthElements
    {
        private readonly ChromeDriver _chromeDriver;

        public AuthElements(ChromeDriver driver)
        {
            _chromeDriver = driver;
        }

        public IWebElement LoginArea => _chromeDriver.FindElementById("login");

        public IWebElement PasswordArea => _chromeDriver.FindElementById("password");

        public IWebElement LoginButton => _chromeDriver.FindElementById("loginButton");

        public IWebElement WrongLoginSpan => _chromeDriver.FindElementById("wrongLogin");

        public IWebElement DisabledByAdminSpan => _chromeDriver.FindElementById("PublisherAccountDisabledByAdministratorErrorText");
    }
}