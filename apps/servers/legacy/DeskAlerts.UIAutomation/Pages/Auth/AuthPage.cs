﻿using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.Auth
{
    public class AuthPage
    {
        private readonly AuthActions _authActions;

        public AuthPage(ChromeDriver driver)
        {
            _authActions = new AuthActions(driver);
        }

        public void SetUserName(string userName)
        {
            _authActions.Login = userName;
        }

        public void SetPassword(string password)
        {
            _authActions.Password = password;
        }

        public void ClickLoginButton()
        {
            _authActions.ClickLoginButton();
        }

        public bool IsAuthorized()
        {
            return !_authActions.WrongLogin;
        }

        public bool IsDisplayedMessageAboutDisabling()
        {
            return _authActions.IsDisplayedMessageAboutDisabling;
        }
    }
}
