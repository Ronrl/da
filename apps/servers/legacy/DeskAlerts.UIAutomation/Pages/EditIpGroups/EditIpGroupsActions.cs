﻿using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.EditIpGroups
{
    public class EditIpGroupsActions
    {
        private readonly EditIpGroupsElements _elements;

        public EditIpGroupsActions(ChromeDriver driver)
        {
            _elements = new EditIpGroupsElements(driver);
        }

        public void SetIpGroupName(string groupName)
        {
            _elements.IpGroupName.Clear();
            _elements.IpGroupName.SendKeys(groupName);
        }

        public void ClickCancelButton()
        {
            _elements.CancelButton.Click();
        }

        public void ClickAddButton()
        {
            _elements.AddButton.Click();
        }

        public void ClickAddIpRange()
        {
           _elements.IpRangeButton.Click();
        }

        public void ClickAddIpAddress()
        {
            _elements.IpAddressButton.Click();
        }

        public void SetIpAddress(int rowNumber, string ipAddress)
        {
            _elements.IpAddressInput(rowNumber).Clear();
            _elements.IpAddressInput(rowNumber).SendKeys(ipAddress);
        }

        public void SetIpRange(int rowNumber, string ipRangeFrom, string ipRangeTo)
        {
            _elements.IpRangeFromInput(rowNumber).Clear();
            _elements.IpRangeFromInput(rowNumber).SendKeys(ipRangeFrom);
            _elements.IpRangeToInput(rowNumber).Clear();
            _elements.IpRangeToInput(rowNumber).SendKeys(ipRangeTo);
        }

        public string GetIpAddressFromField(int rowNumber)
        {
            return _elements.IpAddressInput(rowNumber).GetAttribute("value");
        }

        public string GetIpRangeFromField(int rowNumber)
        {
            return _elements.IpRangeFromInput(rowNumber).GetAttribute("value");
        }

        public string GetIpRangeToField(int rowNumber)
        {
            return _elements.IpRangeToInput(rowNumber).GetAttribute("value");
        }

        public void DeleteIpAddress(int rowNumber)
        {
            _elements.DeleteIpAddressButton(rowNumber).Click();
        }

        public int GetIpAddressRowsCount()
        {
            return _elements.IpAddressTable;
        }

    }
}
