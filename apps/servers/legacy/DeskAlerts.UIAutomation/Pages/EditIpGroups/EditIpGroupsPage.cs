﻿using System.Collections.Generic;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.EditIpGroups
{
    public class EditIpGroupsPage
    {
        private readonly EditIpGroupsActions _actions;

        public EditIpGroupsPage(ChromeDriver driver)
        {
            _actions = new EditIpGroupsActions(driver);
        }

        public void ClickAddButton()
        {
            _actions.ClickAddButton();
        }

        public void ClickCancelButton()
        {
            _actions.ClickCancelButton();
        }

        public void AddIpGroup(string groupName, string[,] ipAddresses)
        {
            _actions.SetIpGroupName(groupName);

            for (int i = 0; i <= ipAddresses.GetUpperBound(0); i++)
            {
                if (ipAddresses[i, 1] == null)
                {
                    _actions.ClickAddIpAddress();
                    _actions.SetIpAddress(i + 1, ipAddresses[i, 0]);
                }
                else
                {
                    _actions.ClickAddIpRange();
                    _actions.SetIpRange(i + 1, ipAddresses[i, 0], ipAddresses[i, 1]);
                }
            }

            _actions.ClickAddButton();
        }

        public void SetGroupName(string groupName)
        {
            _actions.SetIpGroupName(groupName);
        }

        public void AddIpRange()
        {
            _actions.ClickAddIpRange();
        }

        public void AddIpAddress()
        {
            _actions.ClickAddIpAddress();
        }

        public void SetIpAddress(int rowNumber, string ipAddress)
        {
            _actions.SetIpAddress(rowNumber, ipAddress);
        }

        public void SetIpRange(int rowNumber, string IpRangeFrom, string ipRangeTo)
        {
            _actions.SetIpRange(rowNumber, IpRangeFrom, ipRangeTo);
        }

        public void DeleteIpAddress(int rowNumber)
        {
            _actions.DeleteIpAddress(rowNumber);
        }

        public int GetIpAddressRowsCount()
        {
           return _actions.GetIpAddressRowsCount();
        }

        public string GetIpAddress(int rowNumber)
        {
            return _actions.GetIpAddressFromField(rowNumber);
        }

        public List<string> GetIpRangeFields(int rowNumber)
        {
            List<string> namesList = new List<string>();
            namesList.Add(_actions.GetIpRangeFromField(rowNumber));
            namesList.Add(_actions.GetIpRangeToField(rowNumber));
            return namesList;


        }
    }
}
