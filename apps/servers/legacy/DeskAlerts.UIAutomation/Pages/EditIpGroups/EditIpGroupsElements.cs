﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.EditIpGroups
{
    public class EditIpGroupsElements
    {
        private readonly ChromeDriver _driver;

        public EditIpGroupsElements(ChromeDriver driver)
        {
            _driver = driver;
        }

        public IWebElement IpGroupName => _driver.FindElementById("groupName");

        public IWebElement IpAddressButton => _driver.FindElementById("ip_group_table").FindElement(By.Id("ip_address"));

        public IWebElement IpRangeButton => _driver.FindElementById("ip_group_table").FindElement(By.Id("ip_range"));

        public IWebElement CancelButton => _driver.FindElementByClassName("cancel_button");

        public IWebElement AddButton => _driver.FindElementByClassName("add_button");

        public IWebElement IpAddressInput(int rowNumber) => _driver.FindElementById("ip_address_" + rowNumber);

        public IWebElement IpRangeFromInput(int rowNumber) => _driver.FindElementById("ip_address_from_" + rowNumber);

        public IWebElement IpRangeToInput(int rowNumber) => _driver.FindElementById("ip_address_to_" + rowNumber);

        public IWebElement DeleteIpAddressButton(int rowNumber) => _driver.FindElementById("ip_group_table").FindElement(By.Id("answer_delete_" + rowNumber));

        public int IpAddressTable => _driver.FindElementById("ip_group_table").FindElements(By.Name("answer_delete")).Count;
    }
}
