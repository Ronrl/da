﻿using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.EditGroupMembers
{
    public class EditGroupMembersActions
    {
        private readonly EditGroupMembersElements _elements;

        public EditGroupMembersActions(ChromeDriver driver)
        {
            _elements = new EditGroupMembersElements(driver);
        }

        public void GroupsTabClick()
        {
            _elements.GroupsTab.Click();
        }

        public void UsersTabClick()
        {
            _elements.UsersTab.Click();
        }

        public void AddClick()
        {
            _elements.AddButton.Click();
        }
    }
}
