﻿using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.EditGroupMembers
{
    public class EditGroupMembersPage
    {
        private readonly EditGroupMembersActions _actions;

        public EditGroupMembersPage(ChromeDriver driver)
        {
            _actions = new EditGroupMembersActions(driver);
        }

        public void GroupsTabClick()
        {
            _actions.GroupsTabClick();
        }

        public void UsersTabClick()
        {
            _actions.UsersTabClick();
        }

        public void AddClick()
        {
            _actions.AddClick();
        }
    }
}
