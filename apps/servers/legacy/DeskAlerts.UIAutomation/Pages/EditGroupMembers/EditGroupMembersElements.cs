﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.EditGroupMembers
{
    public class EditGroupMembersElements
    {
        private readonly ChromeDriver _driver;

        public EditGroupMembersElements(ChromeDriver driver)
        {
            _driver = driver;
        }

        public IWebElement UsersTab => _driver.FindElementById("ui-id-1");

        public IWebElement GroupsTab => _driver.FindElementById("ui-id-2");

        public IWebElement AddButton => _driver.FindElementById("addRecipientButton");

        public IWebElement RemoveButton => _driver.FindElementById("removeaddRecipientButton");
    }
}
