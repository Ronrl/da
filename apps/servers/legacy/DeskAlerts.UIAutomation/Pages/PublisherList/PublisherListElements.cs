﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.PublisherList
{
    public class PublisherListElements
    {
        private readonly ChromeDriver _driver;

        public PublisherListElements(ChromeDriver driver)
        {
            _driver = driver;
        }

        public IWebElement AddPublisherButton => _driver.FindElementById("addPublisherButton");

        public IWebElement EnablePublisherButton => _driver.FindElementById("upEnable");

        public IWebElement DisablePublisherButton => _driver.FindElementById("upDisable");

        public IWebElement ActivePublishersLeftCounter => _driver.FindElementById("maxPublishersCount");
    }
}