﻿using System;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.PublisherList
{
    public class PublisherListActions
    {
        private readonly PublisherListElements _elements;

        public PublisherListActions(ChromeDriver driver)
        {
            _elements = new PublisherListElements(driver);
        }

        public void ClickAddPublisherButton()
        {
            _elements.AddPublisherButton.Click();
        }

        public void ClickEnablePublisherButton()
        {
            _elements.EnablePublisherButton.Click();
        }

        public void ClickDisablePublisherButton()
        {
            _elements.DisablePublisherButton.Click();
        }

        public int GetActivePublishersLeftCounterValue()
        {
            return Convert.ToInt32(_elements.ActivePublishersLeftCounter.Text);
        }
    }
}
