﻿using DeskAlerts.UIAutomationTests.Pages.BaseTable;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Pages.PublisherList
{
    public class PublisherListTablePage : BaseTablePage
    {
        private readonly PublisherListActions _actions;

        public PublisherListTablePage(ChromeDriver driver) : base(driver)
        {
            _actions = new PublisherListActions(driver);
        }

        public void ClickAddPublisherButton()
        {
            _actions.ClickAddPublisherButton();
        }

        public void ClickEnablePublisherButton()
        {
            _actions.ClickEnablePublisherButton();
        }

        public void ClickDisablePublisherButton()
        {
            _actions.ClickDisablePublisherButton();
        }

        public void EditPublisher(string value)
        {
            BaseTableActions.SearchInput = value;
            BaseTableActions.ClickSearchButton();
            BaseTableActions.ClickOnAction(value, 5, 0);
        }

        public void SortTableByName()
        {
            BaseTableActions.SortBy("pubNameCell");
        }

        public void SortByPolicyName()
        {
            BaseTableActions.SortBy("pubPolicyCell");
        }

        public int GetActivePublishersLeftCounterValue()
        {
            return _actions.GetActivePublishersLeftCounterValue();
        }
    }
}
