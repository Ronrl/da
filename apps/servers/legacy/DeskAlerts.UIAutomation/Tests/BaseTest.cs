using DeskAlerts.UIAutomationTests.Frames.AlertBodyFrame;
using DeskAlerts.UIAutomationTests.Frames.AlertTitleFrame;
using DeskAlerts.UIAutomationTests.Frames.MenuFrame;
using DeskAlerts.UIAutomationTests.Frames.OrgGroupsFrame;
using DeskAlerts.UIAutomationTests.Frames.OrgUsersFrame;
using DeskAlerts.UIAutomationTests.Frames.PolicyGroupsFrame;
using DeskAlerts.UIAutomationTests.Frames.PolicyOusFrame;
using DeskAlerts.UIAutomationTests.Frames.PolicyUsersFrame;
using DeskAlerts.UIAutomationTests.Frames.RecipientsFrame;
using DeskAlerts.UIAutomationTests.Frames.TopFrame;
using DeskAlerts.UIAutomationTests.Pages.AdSynchronizationForm;
using DeskAlerts.UIAutomationTests.Pages.Auth;
using DeskAlerts.UIAutomationTests.Pages.ColorCodes;
using DeskAlerts.UIAutomationTests.Pages.CreateAlert;
using DeskAlerts.UIAutomationTests.Pages.EditColorCodes;
using DeskAlerts.UIAutomationTests.Pages.DigitalSignage;
using DeskAlerts.UIAutomationTests.Pages.EditGroupMembers;
using DeskAlerts.UIAutomationTests.Pages.EditGroups;
using DeskAlerts.UIAutomationTests.Pages.EditIpGroups;
using DeskAlerts.UIAutomationTests.Pages.EditPolicy;
using DeskAlerts.UIAutomationTests.Pages.EditPublisher;
using DeskAlerts.UIAutomationTests.Pages.EditUsers;
using DeskAlerts.UIAutomationTests.Pages.IpGroups;
using DeskAlerts.UIAutomationTests.Pages.InstantMessages;
using DeskAlerts.UIAutomationTests.Pages.Organizations;
using DeskAlerts.UIAutomationTests.Pages.PolicyList;
using DeskAlerts.UIAutomationTests.Pages.PopupSent;
using DeskAlerts.UIAutomationTests.Pages.ProfileSettings;
using DeskAlerts.UIAutomationTests.Pages.PublisherList;
using DeskAlerts.UIAutomationTests.Pages.RecipientsSelection;
using DeskAlerts.UIAutomationTests.Pages.SettingsSelection;
using DeskAlerts.UIAutomationTests.Pages.Surveys;
using DeskAlerts.UIAutomationTests.Pages.Synchronizations;
using DeskAlerts.UIAutomationTests.Pages.TextTemplates;
using DeskAlerts.UIAutomationTests.Pages.ViewEditorsList;
using DeskAlerts.UIAutomationTests.Utilities;
using NUnit.Framework;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Tests
{
    public class BaseTest
    {
        protected ProfileSettingsPage ProfileSettingPage;
        protected TopFramePage TopFramePage;
        protected SettingsSelectionPage SettingsSelectionPage;

        protected AlertTitleFramePage AlertTitleFramePage;
        protected AlertBodyFramePage AlertBodyFramePage;
        protected RecipientsFramePage RecipientsFramePage;
        protected PolicyGroupsFramePage PolicyGroupsFramePage;
        protected PolicyUsersFramePage PolicyUsersFramePage;
        protected OrgUsersFramePage OrgUsersFramePage;
        protected OrgGroupsFramePage OrgGroupsFramePage;
        protected PolicyOusFramePage PolicyOusFramePage;

        protected AdSynchronizationFormPage AdSynchronizationFormPage;
        protected CreateAlertPage CreateAlertPage;
        protected EditGroupsPage EditGroupsPage;
        protected EditGroupMembersPage EditGroupMembersPage;
        protected EditPolicyPage EditPolicyPage;
        protected EditPublisherPage EditPublisherPage;
        protected EditUsersPage EditUsersPage;
        protected PublisherListTablePage PublisherListTablePage;
        protected PolicyListTablePage PolicyListTablePage;
        protected PopupSentPage PopupSentPage;
        protected IpGroupsPage IpGroupsPage;
        protected EditIpGroupsPage EditIpGroupsPage;
        protected OrganizationsPage OrganizationsPage;
        protected RecipientsSelectionPage RecipientsSelectionPage;
        protected SynchronizationsPage SynchronizationsPage;
        protected ViewEditorsListPage ViewEditorsListPage;
        protected EditColorCodesPage EditColorCodesPage;
        protected ColorCodesPage ColorCodesPage;
        protected InstantMessagesPage InstantMessagesPage;
        protected TextTemplatesPage TextTemplatesPage;
        protected DigitalSignagePage DigitalSignagePage;
        protected SurveysPage SurveysPage;
        protected SurveysQuestionPage SurveysQuestionPage;

        public ChromeDriver ChromeDriver { get; private set; }
        public WebDriver WebDriver { get; private set; }
        public AuthPage AuthPage { get; private set; }
        public MenuPage MenuPage { get; private set; }

        protected const string BroadcastBody = "Broadcast body";
        protected const string BroadcastTitle = "Broadcast title";
        protected const string SelectedRecipientsBody = "SelectedRecipients body";
        protected const string SelectedRecipientsTitle = "SelectedRecipients title";
        protected const string DraftAlertBody = "DraftAlert body";
        protected const string DraftAlertTitle = "DraftAlert title";
        protected const string TestUserName = "UI Test user";
        protected const string TestUserPassword = "password";
        protected const string OnlineUserName = "OnlineUser";
        protected const string OfflineUserName = "OfflineUser";
        protected const string PolicyName = "A Policy";
        protected const string PolicyName2 = "B Policy";
        protected const string ApostrophePolicyName = "UI Test Policy with apostrop'he";
        protected const string UpdatedApostrophePolicyName = "Updated UI Test Policy with apostrop'he";
        protected const string DuplicatedPolicyName = "UI Test Policy copy";
        protected const string PublisherName = "UI Test Publisher";
        protected const string PublisherPolicyName = "A";
        protected const string PublisherPolicyName2 = "Aa";
        protected const string DefaultPolicyName = "defaultuser";
        protected const string ColorCodeName = "testCode";
        protected const string ColorCodeColor = "ff00ff";
        protected const string DeviceTestName = "TestDevice";
        protected const string SurveyBroadcastTitle = "Test survey title (broadcast)";
        protected const string SurveyRecipientsTitle = "Test survey title (recipients)";
        protected const string SurveyEditedTitle = "Test survey title (edited)";
        protected const string SurveyQuestionTitle = "Test question title";
        protected const string SurveyQuestionOption = "Test answer to the question";
        protected const string DefaultPolicyName2 = "defaultuser2";

        protected const string PublisherPassword = "PublisherPassword";
        protected const string PublisherPassword2 = "PublisherPassword2";

        [SetUp]
        public void RunBeforeBaseTest()
        {
            ChromeDriver = new ChromeDriver();
            WebDriver = new WebDriver(ChromeDriver);
            AuthPage = new AuthPage(ChromeDriver);
            MenuPage = new MenuPage(ChromeDriver, WebDriver);

            WebDriver.Open();

            CreateAlertPage = new CreateAlertPage(ChromeDriver);
            AlertTitleFramePage = new AlertTitleFramePage(ChromeDriver);
            AlertBodyFramePage = new AlertBodyFramePage(ChromeDriver);
            RecipientsSelectionPage = new RecipientsSelectionPage(ChromeDriver);
            RecipientsFramePage = new RecipientsFramePage(ChromeDriver);
            PopupSentPage = new PopupSentPage(ChromeDriver);
            OrganizationsPage = new OrganizationsPage(ChromeDriver);
            OrgUsersFramePage = new OrgUsersFramePage(ChromeDriver);
            OrgGroupsFramePage = new OrgGroupsFramePage(ChromeDriver);
            PolicyOusFramePage = new PolicyOusFramePage(ChromeDriver);
            EditUsersPage = new EditUsersPage(ChromeDriver);
            PolicyGroupsFramePage = new PolicyGroupsFramePage(ChromeDriver);
            PolicyUsersFramePage = new PolicyUsersFramePage(ChromeDriver);
            EditGroupsPage = new EditGroupsPage(ChromeDriver);
            EditGroupMembersPage = new EditGroupMembersPage(ChromeDriver);
            EditIpGroupsPage = new EditIpGroupsPage(ChromeDriver);
            IpGroupsPage = new IpGroupsPage(ChromeDriver);
            PolicyListTablePage = new PolicyListTablePage(ChromeDriver);
            EditPolicyPage = new EditPolicyPage(ChromeDriver);
            ViewEditorsListPage = new ViewEditorsListPage(ChromeDriver);
            SynchronizationsPage = new SynchronizationsPage(ChromeDriver);
            AdSynchronizationFormPage = new AdSynchronizationFormPage(ChromeDriver);
            ProfileSettingPage = new ProfileSettingsPage(ChromeDriver);
            SettingsSelectionPage = new SettingsSelectionPage(ChromeDriver);
            EditColorCodesPage = new EditColorCodesPage(ChromeDriver);
            ColorCodesPage = new ColorCodesPage(ChromeDriver);
            InstantMessagesPage = new InstantMessagesPage(ChromeDriver);
            TextTemplatesPage = new TextTemplatesPage(ChromeDriver);
            DigitalSignagePage = new DigitalSignagePage(ChromeDriver);
            SurveysPage = new SurveysPage(ChromeDriver);
            SurveysQuestionPage = new SurveysQuestionPage(ChromeDriver);
            PublisherListTablePage = new PublisherListTablePage(ChromeDriver);
        }

        [TearDown]
        public void RunAfterBaseTest()
        {
            WebDriver.Close();

            ChromeDriver.Dispose();
        }

        /// <summary>
        /// Run before run all tests
        /// </summary>
        [OneTimeSetUp]
        public void RunBeforeAllTests()
        {
            
        }

        /// <summary>
        /// Run after run all tests
        /// </summary>
        [OneTimeTearDown]
        public void RunAfterAllTest()
        {
        }

        public void CreatePublisher(string publisherName, string publisherPassword, string publisherPolicy = "defaultuser")
        {
            PublisherListTablePage.ClickAddPublisherButton();
            EditPublisherPage.AddPublisher(publisherName, publisherPassword, publisherPolicy);
        }
    }
}
