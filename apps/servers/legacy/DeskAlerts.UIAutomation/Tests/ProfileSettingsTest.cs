using DeskAlerts.UIAutomationTests.Frames.TopFrame;
using DeskAlerts.UIAutomationTests.Pages.ProfileSettings;
using DeskAlerts.UIAutomationTests.Pages.SettingsSelection;
using NUnit.Framework;
using OpenQA.Selenium;

namespace DeskAlerts.UIAutomationTests.Tests
{
    public class ProfileSettingsTest : BaseAdminAuthorizationTest
    {
        [SetUp]
        public void RunBeforeTest()
        {
            ProfileSettingPage = new ProfileSettingsPage(ChromeDriver);
            TopFramePage = new TopFramePage(ChromeDriver);
            SettingsSelectionPage = new SettingsSelectionPage(ChromeDriver);

            MenuPage.OpenPublishersList();
            if (PublisherListTablePage.IsFound(PublisherName))
            {
                //Clean
                PublisherListTablePage.Delete(PublisherName);
            }

            TopFramePage.ClickSettingsButton();
            SettingsSelectionPage.ClickProfileSettings();
            ProfileSettingPage.SelectProfileLanguage("English");
            ChromeDriver.Navigate().Refresh();
            TopFramePage.ClickSettingsButton();
            SettingsSelectionPage.ClickProfileSettings();
            ProfileSettingPage.SelectStartPage("Choose start page");

        }

        [Test]
        public void Should_CheckWhenFieldDisabled_When_PublisherChangeProfileSettings()
        {
           Assert.AreEqual(false, ProfileSettingPage.EditPublisherName());
        }
        
        [Test]
        public void Should_HidePasswordEditingForm_When_ClickOnHidePasswordFormButton()
        {
            ProfileSettingPage.OpenPasswordChangeForm();
            ProfileSettingPage.ClosePasswordForm();

            Assert.AreEqual(true, ProfileSettingPage.IsPasswordFormVisible());

        }

        [Test]
        public void Should_ShowErrorMessage_When_OldPasswordFieldEmpty()
        {
            ProfileSettingPage.ChangeProfilePassword("", "admin2", "admin2");
            
            Assert.AreEqual("Old password is not valid", ProfileSettingPage.GetErrorText());
        }

        [Test]
        public void Should_ShowErrorMessage_When_NewPasswordFiledEmpty()
        {
            ProfileSettingPage.ChangeProfilePassword("admin", "", "admin2");

            Assert.AreEqual("Passwords don\'t match. Try again", ProfileSettingPage.GetErrorText());
        }

        [Test]
        public void Should_ShowErrorMessage_When_NewRepeatFieldIsEmpty()
        {
            ProfileSettingPage.ChangeProfilePassword("admin", "admin2", "");

            Assert.AreEqual("Passwords don\'t match. Try again", ProfileSettingPage.GetErrorText());
        }

        [Test]
        public void Should_ShowErrorMessage_When_PasswordFieldsEmpty()
        {
            ProfileSettingPage.ChangeProfilePassword("", "", "");

            Assert.AreEqual("Old password is not valid", ProfileSettingPage.GetErrorText());

        }

        [Test]
        public void Should_ShowErrorMessage_When_OldPublisherPasswordWrong()
        {
            ProfileSettingPage.ChangeProfilePassword("admin1", "admin2", "admin2");

            Assert.AreEqual("Old password is not valid",ProfileSettingPage.GetErrorText());

        }

        [Test]
        public void Should_ShowErrorMessage_When_NewRepeatPasswordWrong()
        {
            ProfileSettingPage.ChangeProfilePassword("admin", "admin2", "admin1");

            Assert.AreEqual("Passwords don\'t match. Try again",ProfileSettingPage.GetErrorText());
        }

        [Test]
        public void Should_ChangePublisherPassword_When_ChangeProfileSettings()
        {
            //Arrange
            ProfileSettingPage.ChangeProfilePassword("admin","admin2","admin2");

            //Act
            TopFramePage.ClickLogoutButton();
            AuthPage.SetUserName("admin");
            AuthPage.SetPassword("admin2");
            AuthPage.ClickLoginButton();

            //Assert
            Assert.Throws<NoSuchElementException>(delegate { AuthPage.IsAuthorized(); });
            
            //Clean
            TopFramePage.ClickSettingsButton();
            SettingsSelectionPage.ClickProfileSettings();
            ProfileSettingPage.ChangeProfilePassword("admin2", "admin", "admin");
        }

        [Test]
        public void Should_ChangeProfileLanguage_When_ChangeProfileSettings()
        {
            //Arrange
            MenuPage.OpenPublishersList();
            PublisherListTablePage.ClickAddPublisherButton();
            EditPublisherPage.AddPublisher(PublisherName, PublisherPassword, DefaultPolicyName);

            //Act
            TopFramePage.ClickSettingsButton();
            SettingsSelectionPage.ClickProfileSettings();
            ProfileSettingPage.SelectProfileLanguage("Spanish");
            ChromeDriver.Navigate().Refresh();
            TopFramePage.ClickSettingsButton();
            SettingsSelectionPage.ClickProfileSettings();

            //Assert
            Assert.AreEqual("Spanish", ProfileSettingPage.GetSelectedLanguage());

            //Clean
            TopFramePage.ClickSettingsButton();
            SettingsSelectionPage.ClickProfileSettings();
            ProfileSettingPage.SelectProfileLanguage("English");
            ChromeDriver.Navigate().Refresh();
            MenuPage.OpenPublishersList();
            PublisherListTablePage.Delete(PublisherName);
            
        }

        [Test]
        public void Should_FailChangeLanguage_When_ChangeAnotherPublisherLanguage()
        {
            //Arrange
            MenuPage.OpenPublishersList();
            PublisherListTablePage.ClickAddPublisherButton();
            EditPublisherPage.AddPublisher(PublisherName, PublisherPassword, DefaultPolicyName);

            //Act
            TopFramePage.ClickSettingsButton();
            SettingsSelectionPage.ClickProfileSettings();
            ProfileSettingPage.SelectProfileLanguage("Spanish");
            TopFramePage.ClickLogoutButton();
            AuthPage.SetUserName(PublisherName);
            AuthPage.SetPassword(PublisherPassword);
            AuthPage.ClickLoginButton();
            TopFramePage.ClickSettingsButton();
            SettingsSelectionPage.ClickProfileSettings();

            //Assert
            Assert.AreEqual("English", ProfileSettingPage.GetSelectedLanguage());

            //Clean
            TopFramePage.ClickLogoutButton();
            AuthPage.SetUserName("admin");
            AuthPage.SetPassword("admin");
            AuthPage.ClickLoginButton();
            TopFramePage.ClickSettingsButton();
            SettingsSelectionPage.ClickProfileSettings();
            ProfileSettingPage.SelectProfileLanguage("English");
            ChromeDriver.Navigate().Refresh();
            MenuPage.OpenPublishersList();
            PublisherListTablePage.Delete(PublisherName);
        }

        [Test]
        public void Should_ChangePublisherStartPage_When_ChangeProfileSettings()
        {
            //Arrange
            ProfileSettingPage.SelectStartPage("Rss.Create");
            ChromeDriver.Navigate().Refresh();
            TopFramePage.ClickSettingsButton();
            SettingsSelectionPage.ClickProfileSettings();

            //Assert
            Assert.AreEqual("Rss.Create", ProfileSettingPage.GetStartPage());

            //Clean
            ProfileSettingPage.SelectStartPage("Choose start page");

        }

        [TearDown]
        public void RunAfterTest()
        {
            
        }
    }
}
