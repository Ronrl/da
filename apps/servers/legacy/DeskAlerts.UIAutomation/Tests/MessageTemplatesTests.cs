﻿using DeskAlerts.UIAutomationTests.Pages.TextTemplates;
using NUnit.Framework;

namespace DeskAlerts.UIAutomationTests.Tests
{
    public class MessageTemplatesTests : BaseAdminAuthorizationTest
    {
        [SetUp]
        public void RunBeforeTest()
        {
            //Arrange
            MenuPage.OpenMessageTemplates();

        }

        [TearDown]
        public void RunAfterTest()
        {

        }

        [Test]
        public void Should_ReturnRightListOrder_When_PageLoad()
        {
            //Assert
            Assert.AreEqual(1, TextTemplatesPage.GetIndexInTable("Arrival Announcement"));
            Assert.AreEqual(2, TextTemplatesPage.GetIndexInTable("Corporate news"));
            Assert.AreEqual(3, TextTemplatesPage.GetIndexInTable("Email outage"));
            Assert.AreEqual(4, TextTemplatesPage.GetIndexInTable("Event"));
            Assert.AreEqual(5, TextTemplatesPage.GetIndexInTable("Monthly digest"));
            
        }
    } 
}
