﻿using System.Collections.Generic;
using System.Security.AccessControl;
using NUnit.Framework;
using NUnit.Framework.Internal;

namespace DeskAlerts.UIAutomationTests.Tests
{
    public class IpGroupsTests : BaseAdminAuthorizationTest
    {
        private List<string> testIpRange = new List<string> { "0.0.0.0", "1.1.1.1" };
        private string groupName = "IpGroupTest";
        
        [SetUp]
        public void RunBeforeTest()
        {
            MenuPage.OpenIpGroups();

            if (IpGroupsPage.IsFound(groupName))
            {
                IpGroupsPage.Delete(groupName);
            }

            IpGroupsPage.ClickCreateAudience();
        }


        [TearDown]
        public void RunAfterTest()
        {

        }

        [Test]
        public void Should_CreateIpGroupWithIpAddress_When_IpAddressIsCorrect()
        {
            //Act
            EditIpGroupsPage.AddIpGroup(groupName, new[,]{ { "0.0.0.0", null }, { "1.1.1.1", null }, { "22.22.22.22", null } });


            //Assert
            Assert.AreEqual(true, IpGroupsPage.IsFound(groupName));

            //Clean
            IpGroupsPage.Delete(groupName);
        }

        [Test]
        public void Should_CreateIpGroupWithIpRange_When_IpRangeCorrect()
        {
            //Act
            EditIpGroupsPage.AddIpGroup(groupName, new[,]{ { "0.0.0.0", "0.0.0.0" }, { "111.111.111.11", "111.111.111.111" } });

            //Assert
            Assert.AreEqual(true, IpGroupsPage.IsFound(groupName));

            //Clean
            IpGroupsPage.Delete(groupName);
        }

        [Test]
        public void Should_DeleteIpAddress_When_CreateIpGroup()
        {
            //Act
            EditIpGroupsPage.SetGroupName(groupName);
            EditIpGroupsPage.AddIpAddress();
            EditIpGroupsPage.SetIpAddress(1, "0.0.0.0");
            EditIpGroupsPage.AddIpAddress();
            EditIpGroupsPage.SetIpAddress(2, "0.0.0.0");
            EditIpGroupsPage.AddIpAddress();
            EditIpGroupsPage.SetIpAddress(3, "0.0.0.0");
            EditIpGroupsPage.DeleteIpAddress(1);

            //Assert
            Assert.AreEqual(2, EditIpGroupsPage.GetIpAddressRowsCount());

            //Clean
            EditIpGroupsPage.ClickCancelButton();
            if (IpGroupsPage.IsFound(groupName))
            {
                IpGroupsPage.Delete(groupName);
            }

        }

        [Test]
        public void Should_EditIpGroup_When_DeleteIpConfigurations()
        {
            //Act
            EditIpGroupsPage.AddIpGroup(groupName, new [,]{ { "0.0.0.0", null}, {"99.99.99.99", null}, {"0.0.0.0", "1.1.1.1"}, {"11.11.11.11", "111.111.111.111"}
            });
            IpGroupsPage.ClickOnAction(groupName, 2, 0);
            EditIpGroupsPage.DeleteIpAddress(2);
            EditIpGroupsPage.DeleteIpAddress(4);
            EditIpGroupsPage.ClickAddButton();
            IpGroupsPage.ClickOnAction(groupName, 2, 0);

            //Assert
            Assert.AreEqual(testIpRange, EditIpGroupsPage.GetIpRangeFields(1));
            Assert.AreEqual("0.0.0.0", EditIpGroupsPage.GetIpAddress(2));


        }

        [Test]
        public void Should_EditIpGroupName_When_ChangeIpGroup()
        {
            //Act
            EditIpGroupsPage.AddIpGroup(groupName, new [,]{ { "0.0.0.0", null}, {"99.99.99.99", null}, {"0.0.0.0", "1.1.1.1"}, {"11.11.11.11", "111.111.111.111"} });
            IpGroupsPage.ClickOnAction(groupName, 2, 0);
            EditIpGroupsPage.SetGroupName("IpGroupTest2IpGroupTest");
            EditIpGroupsPage.ClickAddButton();

            //Assert
            Assert.AreEqual(true, IpGroupsPage.IsFound("IpGroupTest2IpGroupTest"));

            //Clear
            IpGroupsPage.Delete("IpGroupTest2IpGroupTest");
        }

        [Test]
        public void Should_GoToIpGroupsPage_When_PressCancel()
        {
            //Act
            EditIpGroupsPage.SetGroupName(groupName);
            EditIpGroupsPage.AddIpAddress();
            EditIpGroupsPage.SetIpAddress(1, "0.0.0.0");
            EditIpGroupsPage.ClickCancelButton();

            //Assert
            Assert.AreEqual(false, IpGroupsPage.IsFound(groupName));

            //Clean
            if (IpGroupsPage.IsFound(groupName))
            {
                IpGroupsPage.Delete(groupName);
            }
        }

        [Test]
        public void Should_FindIpGroup_When_Search()
        {
            //Act
            EditIpGroupsPage.AddIpGroup(groupName, new [,]{ { "0.0.0.0", null}, {"11.11.11.11", "111.111.111.111"} });

            //Assert
            Assert.AreEqual(true, IpGroupsPage.IsFound(groupName));

            //Clean
            IpGroupsPage.Delete(groupName);
        }

        [Test]
        public void Should_DeleteIpGroup_When_PressDelete()
        {
            //Act
            EditIpGroupsPage.AddIpGroup(groupName, new [,]{ { "0.0.0.0", null},{"11.11.11.11", "111.111.111.111"} });
            IpGroupsPage.Delete(groupName);

            //Assert
            Assert.AreEqual(false, IpGroupsPage.IsInTable(groupName));

            //Clean
            if (IpGroupsPage.IsFound(groupName))
            {
                IpGroupsPage.Delete(groupName);
            }
        }
    }
}
