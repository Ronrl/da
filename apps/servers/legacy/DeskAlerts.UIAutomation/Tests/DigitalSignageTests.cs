﻿using NUnit.Framework;

namespace DeskAlerts.UIAutomationTests.Tests
{
    [TestFixture]
    public class DigitalSignageTests : BaseAdminAuthorizationTest
    {
        [SetUp]
        public void RunBeforeTest()
        {
            WebDriver.SetImplicitWait(3);
        }

        [TearDown]
        public void RunAfterTest()
        {
            //Delete test device
            MenuPage.OpenDigitalSignageDevices();
            if (DigitalSignagePage.IsFoundWithoutCleaning(DeviceTestName))
            {
                DigitalSignagePage.Delete(DeviceTestName);
            }
        }

        [Test]
        public void Should_foundDeviceOnPage_When_addedNewDevice()
        {
            //Arrange

            //Act
            MenuPage.OpenDigitalSignageDevices();

            DigitalSignagePage.ClickOnNewDeviceButton();
            DigitalSignagePage.SetNewDeviceName(DeviceTestName);
            DigitalSignagePage.ClickOnCreateNewDeviceButton();

            //Assert
            Assert.AreEqual(true, PopupSentPage.IsFound(DeviceTestName));
        }

        [Test]
        public void Should_returnedOnDigitalSignagePage_When_sendContentOnDevice()
        {
            //Arrange
            MenuPage.OpenDigitalSignageDevices();

            DigitalSignagePage.ClickOnNewDeviceButton();
            DigitalSignagePage.SetNewDeviceName(DeviceTestName);
            DigitalSignagePage.ClickOnCreateNewDeviceButton();

            //Act
            MenuPage.OpenDigitalSignageSendContent();

            AlertTitleFramePage.Focus();
            AlertTitleFramePage.SetTitle(SelectedRecipientsTitle);
            AlertTitleFramePage.FocusOnBody();

            AlertBodyFramePage.Focus();
            AlertBodyFramePage.SetBody(SelectedRecipientsBody);
            AlertBodyFramePage.FocusOnBody();

            CreateAlertPage.ClickSaveAndNext();

            RecipientsSelectionPage.SelectRecipients();
            RecipientsFramePage.Focus();
            RecipientsFramePage.SelectByUserName(DeviceTestName);
            RecipientsFramePage.FocusOnBody();
            RecipientsSelectionPage.ClickSendButton();

            //Assert
            Assert.True(DigitalSignagePage.IsExistIdentifierProperty());
        }
    }
}