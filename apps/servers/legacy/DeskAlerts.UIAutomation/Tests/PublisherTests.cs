using DeskAlerts.UIAutomationTests.Frames.TopFrame;
using DeskAlerts.UIAutomationTests.Pages.EditPolicy;
using DeskAlerts.UIAutomationTests.Pages.EditPublisher;
using DeskAlerts.UIAutomationTests.Pages.PolicyList;
using DeskAlerts.UIAutomationTests.Pages.PublisherList;
using NUnit.Framework;

namespace DeskAlerts.UIAutomationTests.Tests
{
    [TestFixture]
    public class PublisherTests : BaseAdminAuthorizationTest
    {
        [SetUp]
        public void RunBeforeTest()
        {
            PublisherListTablePage = new PublisherListTablePage(ChromeDriver);
            EditPublisherPage = new EditPublisherPage(ChromeDriver);
            PolicyListTablePage = new PolicyListTablePage(ChromeDriver);
            EditPolicyPage = new EditPolicyPage(ChromeDriver);
            TopFramePage = new TopFramePage(ChromeDriver);

            MenuPage.OpenPublishersList();
            if (PublisherListTablePage.IsFound(PublisherName))
            {
                //Clean
                PublisherListTablePage.Delete(PublisherName);
            }

            if (PublisherListTablePage.IsFound(PublisherPolicyName))
            {
                //Clean
                PublisherListTablePage.Delete(PublisherPolicyName);
            }

            if (PublisherListTablePage.IsFound(PublisherPolicyName2))
            {
                //Clean
                PublisherListTablePage.Delete(PublisherPolicyName2);
            }

            MenuPage.OpenPolicies();
            if (PolicyListTablePage.IsFound(PolicyName))
            {
                //Clean
                PolicyListTablePage.Delete(PolicyName);
            }

            if (PolicyListTablePage.IsFound(PolicyName2))
            {
                //Clean
                PolicyListTablePage.Delete(PolicyName2);
            }
        }

        [Test]
        public void Should_addPublisher_When_publisherIsCorrect()
        {
            //Arrange
            MenuPage.OpenPublishersList();

            //Act
            PublisherListTablePage.ClickAddPublisherButton();
            EditPublisherPage.AddPublisher(PublisherName, PublisherPassword, DefaultPolicyName);

            //Assert
            WebDriver.Wait();
            Assert.AreEqual(true, PublisherListTablePage.IsFound(PublisherName));

            //Clean
            PublisherListTablePage.Delete(PublisherName);
        }

        [Test]
        public void Should_assignPolicyToPublisher_When_adminPolicy()
        {
            //Arrange
            MenuPage.OpenPublishersList();
            PublisherListTablePage.ClickAddPublisherButton();
            EditPublisherPage.AddPublisher(PublisherName, PublisherPassword, DefaultPolicyName);
            MenuPage.OpenPolicies();
            PolicyListTablePage.ClickAddPolicyButton();
            EditPolicyPage.AddAdminPolicy(PolicyName);
            
            //Act
            MenuPage.OpenPublishersList();
            PublisherListTablePage.EditPublisher(PublisherName);
            Assert.AreEqual(DefaultPolicyName, EditPublisherPage.GetSelectPolicy());
            EditPublisherPage.SelectAndSavePolicyByName(PolicyName);
            WebDriver.Wait();
            PublisherListTablePage.EditPublisher(PublisherName);

            //Assert
            Assert.AreEqual(PolicyName, EditPublisherPage.GetSelectPolicy());

            //Clean
            MenuPage.OpenPublishersList();
            PublisherListTablePage.Delete(PublisherName);
            MenuPage.OpenPolicies();
            PolicyListTablePage.Delete(PolicyName);
        }

        [Test]
        public void Should_assignPolicyToPublisher_When_nonAdminPolicy()
        {
            //Arrange
            MenuPage.OpenPublishersList();
            PublisherListTablePage.ClickAddPublisherButton();
            EditPublisherPage.AddPublisher(PublisherName, PublisherPassword, DefaultPolicyName);
            MenuPage.OpenPolicies();
            PolicyListTablePage.ClickAddPolicyButton();
            EditPolicyPage.AddNonAdminPolicy(PolicyName);
            
            //Act
            MenuPage.OpenPublishersList();
            PublisherListTablePage.EditPublisher(PublisherName);
            Assert.AreEqual(DefaultPolicyName, EditPublisherPage.GetSelectPolicy());
            EditPublisherPage.SelectAndSavePolicyByName(PolicyName);
            WebDriver.Wait();
            PublisherListTablePage.EditPublisher(PublisherName);

            //Assert
            Assert.AreEqual(PolicyName, EditPublisherPage.GetSelectPolicy());

            //Clean
            MenuPage.OpenPublishersList();
            PublisherListTablePage.Delete(PublisherName);
            MenuPage.OpenPolicies();
            PolicyListTablePage.Delete(PolicyName);
        }

        [Test]
        public void Should_findPublisher_When_publisherCreated()
        {
            //Arrange
            MenuPage.OpenPublishersList();
            PublisherListTablePage.ClickAddPublisherButton();
            EditPublisherPage.AddPublisher(PublisherName, PublisherPassword, DefaultPolicyName);
            MenuPage.OpenPolicies();
            PolicyListTablePage.ClickAddPolicyButton();
            EditPolicyPage.AddNonAdminPolicy(PolicyName);
            
            //Act
            MenuPage.OpenPublishersList();
            PublisherListTablePage.EditPublisher(PublisherName);
            EditPublisherPage.SelectAndSavePolicyByName(PolicyName);
            MenuPage.OpenPublishersList();

            //Assert
            Assert.AreEqual(true, PublisherListTablePage.IsFound(PublisherName));

            //Clean
            PublisherListTablePage.Delete(PublisherName);
            MenuPage.OpenPolicies();
            PolicyListTablePage.Delete(PolicyName);
        }

        [Test]
        public void Should_sortPublisherWithNames_When_policyCreated()
        {
            //Arrange
            MenuPage.OpenPublishersList();
            PublisherListTablePage.ClickAddPublisherButton();
            EditPublisherPage.AddPublisher(PublisherPolicyName, PublisherPassword, DefaultPolicyName);
            MenuPage.OpenPolicies();
            PolicyListTablePage.ClickAddPolicyButton();
            EditPolicyPage.AddNonAdminPolicy(PolicyName);
            MenuPage.OpenPublishersList();
            PublisherListTablePage.EditPublisher(PublisherPolicyName);
            EditPublisherPage.SelectAndSavePolicyByName(PolicyName);
            PublisherListTablePage.ClickAddPublisherButton();
            EditPublisherPage.AddPublisher(PublisherPolicyName2, PublisherPassword2, DefaultPolicyName);
            MenuPage.OpenPolicies();
            PolicyListTablePage.ClickAddPolicyButton();
            EditPolicyPage.AddNonAdminPolicy(PolicyName2);
            MenuPage.OpenPublishersList();
            PublisherListTablePage.EditPublisher(PublisherPolicyName2);
            EditPublisherPage.SelectAndSavePolicyByName(PolicyName2);

            //Act
            MenuPage.OpenPublishersList();
            PublisherListTablePage.SortTableByName();

            //Assert
            Assert.AreEqual(1, PublisherListTablePage.GetIndexInTable(PublisherPolicyName));
            Assert.AreEqual(2, PublisherListTablePage.GetIndexInTable(PublisherPolicyName2));

            //Clean
            MenuPage.OpenPublishersList();
            PublisherListTablePage.Delete(PublisherPolicyName);
            PublisherListTablePage.Delete(PublisherPolicyName2);
            MenuPage.OpenPolicies();
            PolicyListTablePage.Delete(PolicyName);
            PolicyListTablePage.Delete(PolicyName2);
        }
        
        [Test]
        public void Should_sortPublisherWithPolicyNames_When_policyCreated()
        {
            //Arrange
            MenuPage.OpenPublishersList();
            PublisherListTablePage.ClickAddPublisherButton();
            EditPublisherPage.AddPublisher(PublisherPolicyName, PublisherPassword, DefaultPolicyName);
            PublisherListTablePage.ClickAddPublisherButton();
            EditPublisherPage.AddPublisher(PublisherPolicyName2, PublisherPassword2, DefaultPolicyName);

            //Act
            MenuPage.OpenPublishersList();
            PublisherListTablePage.SortByPolicyName();

            //Assert
            Assert.AreEqual(1, PublisherListTablePage.GetIndexInTable(PublisherPolicyName));
            Assert.AreEqual(2, PublisherListTablePage.GetIndexInTable(PublisherPolicyName2));

            //Clean
            MenuPage.OpenPublishersList();
            PublisherListTablePage.Delete(PublisherPolicyName);
            PublisherListTablePage.Delete(PublisherPolicyName2);
        }

        [Test]
        public void Should_authorizeWithPublisher_When_publisherWasCreated()
        {
            //Arrange
            MenuPage.OpenPublishersList();
            PublisherListTablePage.ClickAddPublisherButton();
            EditPublisherPage.AddPublisher(PublisherName, PublisherPassword, DefaultPolicyName);
            MenuPage.OpenPolicies();
            PolicyListTablePage.ClickAddPolicyButton();
            EditPolicyPage.AddAdminPolicy(PolicyName);
            MenuPage.OpenPublishersList();
            PublisherListTablePage.EditPublisher(PublisherName);
            EditPublisherPage.SelectAndSavePolicyByName(PolicyName);

            //Act
            TopFramePage.ClickLogoutButton();
            AuthPage.SetUserName(PublisherName);
            AuthPage.SetPassword(PublisherPassword);
            AuthPage.ClickLoginButton();
            TopFramePage.Focus();

            //Assert
            Assert.AreEqual(PublisherName, TopFramePage.GetCurrentLoggedPublisherName());

            //Clean
            TopFramePage.ClickLogoutButton();
            AuthPage.SetUserName("admin");
            AuthPage.SetPassword("admin");
            AuthPage.ClickLoginButton();
            MenuPage.OpenPublishersList();
            PublisherListTablePage.Delete(PublisherName);
            MenuPage.OpenPolicies();
            PolicyListTablePage.Delete(PolicyName);
        }

        [Test]
        public void Should_deletePublisher_When_publisherWasCreated()
        {
            //Arrange
            MenuPage.OpenPublishersList();

            //Act
            PublisherListTablePage.ClickAddPublisherButton();
            EditPublisherPage.AddPublisher(PublisherName, PublisherPassword, DefaultPolicyName);
            PublisherListTablePage.Delete(PublisherName);

            //Assert
            WebDriver.Wait();
            Assert.AreEqual(false, PublisherListTablePage.IsFound(PublisherName));

            //Clean
            if (PublisherListTablePage.IsFound(PublisherName))
            {
                PublisherListTablePage.Delete(PublisherName);
            }
        }

        [Test]
        public void Should_publisherHasDisabledStatus_When_publisherAdded()
        {
            //Arrange
            MenuPage.OpenPublishersList();

            //Act
            PublisherListTablePage.ClickAddPublisherButton();
            EditPublisherPage.AddPublisher(PublisherName, PublisherPassword, DefaultPolicyName);
            PublisherListTablePage.Search(PublisherName);

            //Assert
            Assert.AreEqual(true, PublisherListTablePage.IsInTable("Disabled"));

            //Clean
            if (PublisherListTablePage.IsFound(PublisherName))
            {
                PublisherListTablePage.Delete(PublisherName);
            }
        }

        [Test]
        public void Should_publisherHasEnabledStatus_When_enablingButtonClicked()
        {
            //Arrange
            MenuPage.OpenPublishersList();

            //Act
            PublisherListTablePage.ClickAddPublisherButton();
            EditPublisherPage.AddPublisher(PublisherName, PublisherPassword, DefaultPolicyName);
            PublisherListTablePage.SelectByName(PublisherName);
            PublisherListTablePage.ClickEnablePublisherButton();
            PublisherListTablePage.Search(PublisherName);

            //Assert
            Assert.AreEqual(true, PublisherListTablePage.IsInTable("Enabled"));

            //Clean
            if (PublisherListTablePage.IsFound(PublisherName))
            {
                PublisherListTablePage.Delete(PublisherName);
            }
        }

        [Test]
        public void Should_publisherHasDisabledByAdminStatus_When_disablingButtonClickedByAdmin()
        {
            //Arrange
            MenuPage.OpenPublishersList();

            //Act
            PublisherListTablePage.ClickAddPublisherButton();
            EditPublisherPage.AddPublisher(PublisherName, PublisherPassword, DefaultPolicyName);
            PublisherListTablePage.SelectByName(PublisherName);
            PublisherListTablePage.ClickDisablePublisherButton();
            PublisherListTablePage.Search(PublisherName);

            //Assert
            Assert.AreEqual(true, PublisherListTablePage.IsInTable("Disabled by Administrator"));

            //Clean
            if (PublisherListTablePage.IsFound(PublisherName))
            {
                PublisherListTablePage.Delete(PublisherName);
            }
        }

        [Test]
        public void Should_notAuthorize_When_publisherDisabledByAdmin()
        {
            //Arrange
            MenuPage.OpenPublishersList();
            PublisherListTablePage.ClickAddPublisherButton();
            EditPublisherPage.AddPublisher(PublisherName, PublisherPassword, DefaultPolicyName);
            PublisherListTablePage.Search(PublisherName);
            PublisherListTablePage.SelectByName(PublisherName);
            PublisherListTablePage.ClickDisablePublisherButton();

            //Act
            TopFramePage.ClickLogoutButton();
            AuthPage.SetUserName(PublisherName);
            AuthPage.SetPassword(PublisherPassword);
            AuthPage.ClickLoginButton();

            //Assert
            Assert.True(AuthPage.IsDisplayedMessageAboutDisabling());

            //Cleaning
            AuthPage.SetUserName("admin");
            AuthPage.SetPassword("admin");
            AuthPage.ClickLoginButton();
            MenuPage.OpenPublishersList();
            PublisherListTablePage.Delete(PublisherName);
        }

        [Test]
        public void Should_publishersCounterShouldDecrease_When_enablingButtonClicked()
        {
            //Arrange
            MenuPage.OpenPublishersList();
            var initialCounterValue = PublisherListTablePage.GetActivePublishersLeftCounterValue();

            //Act
            PublisherListTablePage.ClickAddPublisherButton();
            EditPublisherPage.AddPublisher(PublisherName, PublisherPassword, DefaultPolicyName);
            PublisherListTablePage.SelectByName(PublisherName);
            PublisherListTablePage.ClickEnablePublisherButton();
            var finalCounterValue = PublisherListTablePage.GetActivePublishersLeftCounterValue();

            //Assert
            Assert.AreEqual(initialCounterValue - 1, finalCounterValue);

            //Clean
            if (PublisherListTablePage.IsFound(PublisherName))
            {
                PublisherListTablePage.Delete(PublisherName);
            }
        }

        [Test]
        public void Should_publishersCounterShouldDecrease_When_publisherWasCreatedAndAutorized()
        {
            //Arrange
            MenuPage.OpenPublishersList();
            PublisherListTablePage.ClickAddPublisherButton();
            EditPublisherPage.AddPublisher(PublisherName, PublisherPassword, DefaultPolicyName);
            MenuPage.OpenPolicies();
            PolicyListTablePage.ClickAddPolicyButton();
            EditPolicyPage.AddAdminPolicy(PolicyName);
            MenuPage.OpenPublishersList();
            PublisherListTablePage.EditPublisher(PublisherName);
            EditPublisherPage.SelectAndSavePolicyByName(PolicyName);
            var initialCounterValue = PublisherListTablePage.GetActivePublishersLeftCounterValue();

            //Act
            TopFramePage.ClickLogoutButton();
            AuthPage.SetUserName(PublisherName);
            AuthPage.SetPassword(PublisherPassword);
            AuthPage.ClickLoginButton();
            MenuPage.OpenPublishersList();
            var finalCounterValue = PublisherListTablePage.GetActivePublishersLeftCounterValue();

            //Assert
            Assert.AreEqual(initialCounterValue - 1, finalCounterValue);

            //Clean
            TopFramePage.ClickLogoutButton();
            AuthPage.SetUserName("admin");
            AuthPage.SetPassword("admin");
            AuthPage.ClickLoginButton();
            MenuPage.OpenPublishersList();
            PublisherListTablePage.Delete(PublisherName);
            MenuPage.OpenPolicies();
            PolicyListTablePage.Delete(PolicyName);
        }

        [Test]
        public void Should_authorizeWithPublisher_When_publisherWasCreatedAndClientHasSameName()
        {
            //Arrange
            MenuPage.OpenPublishersList();
            PublisherListTablePage.ClickAddPublisherButton();
            EditPublisherPage.AddPublisher(PublisherName, PublisherPassword, DefaultPolicyName);

            MenuPage.OpenOrganizationAudience();
            OrganizationsPage.ClickAddUser();
            EditUsersPage.AddUser(PublisherName, PublisherPassword);
            EditUsersPage.ClickAdd();

            //Act
            TopFramePage.ClickLogoutButton();
            AuthPage.SetUserName(PublisherName);
            AuthPage.SetPassword(PublisherPassword);
            AuthPage.ClickLoginButton();
            TopFramePage.Focus();

            //Assert
            Assert.AreEqual(PublisherName, TopFramePage.GetCurrentLoggedPublisherName());

            //Clean
            TopFramePage.ClickLogoutButton();
            AuthPage.SetUserName("admin");
            AuthPage.SetPassword("admin");
            AuthPage.ClickLoginButton();

            MenuPage.OpenPublishersList();
            PublisherListTablePage.Delete(PublisherName);

            MenuPage.OpenOrganizationAudience();
            OrgUsersFramePage.Focus();
            OrganizationsPage.Delete(PublisherName);
            OrgUsersFramePage.FocusOnBody();
        }

        [TearDown]
        public void RunAfterTest()
        {
        }
    }
}
