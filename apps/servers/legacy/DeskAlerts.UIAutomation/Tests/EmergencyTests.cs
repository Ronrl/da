﻿using DeskAlerts.ApplicationCore.Entities;
using NUnit.Framework;

namespace DeskAlerts.UIAutomationTests.Tests
{
    public class EmergencyTests : BaseAdminAuthorizationTest
    {
        [SetUp]
        public void RunBeforeTest()
        {
            //Delete user
            MenuPage.OpenOrganizationAudience();
            OrgUsersFramePage.Focus();
            if (OrgUsersFramePage.IsFoundWithoutCleaning(TestUserName))
            {
                OrgUsersFramePage.Delete(TestUserName);
            }

            MenuPage.OpenAlertSent();
            if (PopupSentPage.IsFound(SelectedRecipientsTitle))
            {
                PopupSentPage.DeleteWithConfirmation(SelectedRecipientsTitle);
            }
            
            MenuPage.OpenColorCodes();
            if (ColorCodesPage.IsFound(ColorCodeName))
            {
                ColorCodesPage.Delete(ColorCodeName);
            }

            MenuPage.OpenSendNowEmergency();
            if (InstantMessagesPage.IsEmergencyExist(BroadcastTitle))
            {
                InstantMessagesPage.DeleteEmergency(BroadcastTitle);
            }
            if (InstantMessagesPage.IsEmergencyExist(SelectedRecipientsTitle))
            {
                InstantMessagesPage.DeleteEmergency(SelectedRecipientsTitle);
            }


        }

        [TearDown]
        public void RunAfterTest()
        {

        }

        [Test]
        public void Should_EditColorCode_When_ColorCodeCreate()
        {
            //Arrange
            MenuPage.OpenColorCodes();
            ColorCodesPage.CreateColorCode();

            //Act
            EditColorCodesPage.EditColorCode(ColorCodeName, ColorCodeColor);
            ColorCodesPage.EditColorCode(ColorCodeName);
            EditColorCodesPage.EditColorCode("testGroup2", "220022");
            ColorCodesPage.EditColorCode("testGroup2");

            //Assert
            Assert.AreEqual(true, EditColorCodesPage.CheckChangedColorCodeValues(("testGroup2", "220022")));


            //Clean
            EditColorCodesPage.CancelEditColorCode();
            if (ColorCodesPage.IsFound("testGroup2"))
            {
                ColorCodesPage.Delete("testGroup2");
            }

        }

        [Test]
        public void Should_FindColorCode_When_CreateColorCode()
        {
            //Arrange
            MenuPage.OpenColorCodes();
            ColorCodesPage.CreateColorCode();

            //Act
            EditColorCodesPage.EditColorCode(ColorCodeName, ColorCodeColor);

            //Assert
            Assert.AreEqual(true, ColorCodesPage.IsFound(ColorCodeName));

            //Clear
            ColorCodesPage.Delete(ColorCodeName);
        }

        [Test]
        public void Should_DeleteColorCode_When_DeleteColorCode()
        {
            //Arrange
            MenuPage.OpenColorCodes();
            ColorCodesPage.CreateColorCode();

            //Act
            EditColorCodesPage.EditColorCode(ColorCodeName, ColorCodeColor);
            ColorCodesPage.Delete(ColorCodeName);

            //Assert
            Assert.AreEqual(false, ColorCodesPage.IsFound(ColorCodeName));

            //Clear
            if (ColorCodesPage.IsFound(ColorCodeName))
            {
                ColorCodesPage.Delete(ColorCodeName);
            }
        }

        [Test]
        public void Should_FindEmergencyAlertOnSendNow_When_EmergencyWasCreated()
        {
            //Arrange
            MenuPage.OpenCreateEmergency();

            //Act
            CreateAlertPage.SelectAlertType(AlertType.SimpleAlert);

            AlertTitleFramePage.Focus();
            AlertTitleFramePage.SetTitle(BroadcastTitle);
            AlertTitleFramePage.FocusOnBody();

            AlertBodyFramePage.Focus();
            AlertBodyFramePage.SetBody(BroadcastBody);
            AlertBodyFramePage.FocusOnBody();

            CreateAlertPage.ClickSaveAndNext();
            RecipientsSelectionPage.SendBroadcast();
            RecipientsSelectionPage.ClickSendButton();
            WebDriver.Wait();

            //Assert
            Assert.AreEqual(true, InstantMessagesPage.IsEmergencyExist(BroadcastTitle));

            //Clear
            InstantMessagesPage.DeleteEmergency(BroadcastTitle);
        }

        [Test]
        public void Should_ChangeEmergencyAlertName_When_EditEmergencyAlert()
        {
            //Arrange
            MenuPage.OpenOrganizationAudience();
            WebDriver.Wait();
            OrganizationsPage.ClickAddUser();
            EditUsersPage.AddUser(TestUserName, TestUserPassword);
            EditUsersPage.ClickAdd();
            MenuPage.OpenCreateEmergency();

            //Act
            AlertTitleFramePage.Focus();
            AlertTitleFramePage.SetTitle(SelectedRecipientsTitle);
            AlertTitleFramePage.FocusOnBody();

            AlertBodyFramePage.Focus();
            AlertBodyFramePage.SetBody(SelectedRecipientsBody);
            AlertBodyFramePage.FocusOnBody();

            CreateAlertPage.ClickSaveAndNext();

            RecipientsSelectionPage.SelectRecipients();
            RecipientsFramePage.Focus();
            RecipientsFramePage.SelectByUserName(TestUserName);
            RecipientsFramePage.FocusOnBody();
            RecipientsSelectionPage.ClickSendButton();


            InstantMessagesPage.EditEmergency(SelectedRecipientsTitle);
            AlertTitleFramePage.Focus();
            AlertTitleFramePage.SetTitle(SelectedRecipientsTitle + "2");
            AlertTitleFramePage.FocusOnBody();
            CreateAlertPage.ClickSaveAndNext();

            RecipientsSelectionPage.SelectRecipients();
            RecipientsFramePage.Focus();
            RecipientsFramePage.FocusOnBody();
            RecipientsSelectionPage.ClickSendButton();

            //Assert
            Assert.AreEqual(true, InstantMessagesPage.IsEmergencyExist(SelectedRecipientsTitle + "2"));

            //Clear
            //Delete user
            MenuPage.OpenOrganizationAudience();
            OrgUsersFramePage.Focus();
            OrgUsersFramePage.Delete(TestUserName);
            OrgUsersFramePage.FocusOnBody();

            //Delete alert
            MenuPage.OpenSendNowEmergency();
            InstantMessagesPage.DeleteEmergency(SelectedRecipientsTitle + "2");
        }

        [Test]
        public void Should_AssignColorCodeToEmergencyAlert_When_CreateEmergencyAlert()
        {
            //Arrange
            MenuPage.OpenOrganizationAudience();
            WebDriver.Wait();
            OrganizationsPage.ClickAddUser();
            EditUsersPage.AddUser(TestUserName, TestUserPassword);
            EditUsersPage.ClickAdd();

            MenuPage.OpenColorCodes();
            ColorCodesPage.CreateColorCode();
            EditColorCodesPage.EditColorCode(ColorCodeName, ColorCodeColor);

            MenuPage.OpenCreateEmergency();

            //Act
            AlertTitleFramePage.Focus();
            AlertTitleFramePage.SetTitle(SelectedRecipientsTitle);
            AlertTitleFramePage.FocusOnBody();

            AlertBodyFramePage.Focus();
            AlertBodyFramePage.SetBody(SelectedRecipientsBody);
            AlertBodyFramePage.FocusOnBody();


            CreateAlertPage.ClickColorCode();
            CreateAlertPage.SelectColorCode(ColorCodeName);
            CreateAlertPage.ClickSaveAndNext();

            RecipientsSelectionPage.SelectRecipients();
            RecipientsFramePage.Focus();
            RecipientsFramePage.SelectByUserName(TestUserName);
            RecipientsFramePage.FocusOnBody();
            RecipientsSelectionPage.ClickSendButton();

            InstantMessagesPage.EditEmergency(SelectedRecipientsTitle);

            //Assert
            Assert.AreEqual(true, CreateAlertPage.CheckColorCode("rgba(255, 0, 255, 1)"));

            //Clean
            MenuPage.OpenColorCodes();
            ColorCodesPage.Delete(ColorCodeName);

            MenuPage.OpenSendNowEmergency();
            InstantMessagesPage.DeleteEmergency(SelectedRecipientsTitle);

        }

        [Test]
        public void Should_ViewEmergencyAlertPreview_When_CreateEmergencyAlert()
        {
            //Arrange
            MenuPage.OpenOrganizationAudience();
            WebDriver.Wait();
            OrganizationsPage.ClickAddUser();
            EditUsersPage.AddUser(TestUserName, TestUserPassword);
            EditUsersPage.ClickAdd();
            MenuPage.OpenCreateEmergency();

            //Act
            AlertTitleFramePage.Focus();
            AlertTitleFramePage.SetTitle(SelectedRecipientsTitle);
            AlertTitleFramePage.FocusOnBody();

            AlertBodyFramePage.Focus();
            AlertBodyFramePage.SetBody(SelectedRecipientsBody);
            AlertBodyFramePage.FocusOnBody();

            CreateAlertPage.ClickSaveAndNext();

            RecipientsSelectionPage.SelectRecipients();
            RecipientsFramePage.Focus();
            RecipientsFramePage.SelectByUserName(TestUserName);
            RecipientsFramePage.FocusOnBody();
            RecipientsSelectionPage.ClickSendButton();

            InstantMessagesPage.ViewPreview(SelectedRecipientsTitle);

            //Assert
            Assert.AreEqual(true, InstantMessagesPage.IsPreviewOpen());

            //Clear
            InstantMessagesPage.DeleteEmergency(SelectedRecipientsTitle);
        }

        [Test]
        public void Should_SendEmergencyAlert_When_EmergencyAlertExist()
        {
            //Arrange
            MenuPage.OpenOrganizationAudience();
            WebDriver.Wait();
            OrganizationsPage.ClickAddUser();
            EditUsersPage.AddUser(TestUserName, TestUserPassword);
            EditUsersPage.ClickAdd();
            MenuPage.OpenCreateEmergency();

            //Act
            AlertTitleFramePage.Focus();
            AlertTitleFramePage.SetTitle(SelectedRecipientsTitle);
            AlertTitleFramePage.FocusOnBody();

            AlertBodyFramePage.Focus();
            AlertBodyFramePage.SetBody(SelectedRecipientsBody);
            AlertBodyFramePage.FocusOnBody();

            CreateAlertPage.ClickSaveAndNext();

            RecipientsSelectionPage.SelectRecipients();
            RecipientsFramePage.Focus();
            RecipientsFramePage.SelectByUserName(TestUserName);
            RecipientsFramePage.FocusOnBody();
            RecipientsSelectionPage.ClickSendButton();

            InstantMessagesPage.SendEmergencyAlert(SelectedRecipientsTitle);

            //Assert
            Assert.AreEqual(true, PopupSentPage.IsFound(SelectedRecipientsTitle));

            //Clear
            PopupSentPage.DeleteWithConfirmation(SelectedRecipientsTitle);
        }

        [Test]
        public void Should_DeleteEmergencyAlert_When_FindOnSendNow()
        {
            //Arrange
            MenuPage.OpenOrganizationAudience();
            WebDriver.Wait();
            OrganizationsPage.ClickAddUser();
            EditUsersPage.AddUser(TestUserName, TestUserPassword);
            EditUsersPage.ClickAdd();
            MenuPage.OpenCreateEmergency();

            //Act
            AlertTitleFramePage.Focus();
            AlertTitleFramePage.SetTitle(SelectedRecipientsTitle);
            AlertTitleFramePage.FocusOnBody();

            AlertBodyFramePage.Focus();
            AlertBodyFramePage.SetBody(SelectedRecipientsBody);
            AlertBodyFramePage.FocusOnBody();

            CreateAlertPage.ClickSaveAndNext();

            RecipientsSelectionPage.SelectRecipients();
            RecipientsFramePage.Focus();
            RecipientsFramePage.SelectByUserName(TestUserName);
            RecipientsFramePage.FocusOnBody();
            RecipientsSelectionPage.ClickSendButton();

            InstantMessagesPage.DeleteEmergency(SelectedRecipientsTitle);

            //Assert
            Assert.AreEqual(false, InstantMessagesPage.IsEmergencyExist(SelectedRecipientsTitle));

            //Clear
            if (InstantMessagesPage.IsEmergencyExist(SelectedRecipientsTitle))
            {
                InstantMessagesPage.DeleteEmergency(SelectedRecipientsTitle);
            }
        }
    }
}
