﻿using NUnit.Framework;

namespace DeskAlerts.UIAutomationTests.Tests
{
    [TestFixture]
    public class BasePublisherAuthorizationTest : BaseTest
    {
        [SetUp]
        public void Authorize()
        {
            AuthPage.SetUserName("pub_1");
        }
    }
}
