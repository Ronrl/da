﻿using NUnit.Framework;

namespace DeskAlerts.UIAutomationTests.Tests
{
    [TestFixture]
    public class PolicyTests : BaseAdminAuthorizationTest
    {
        [SetUp]
        public void RunBeforeTest()
        {
            MenuPage.OpenPublishersList();
            if (PublisherListTablePage.IsFound(PublisherName))
            {
                //Clean
                PublisherListTablePage.Delete(PublisherName);
            }

            MenuPage.OpenPolicies();
            if (PolicyListTablePage.IsFound(PolicyName))
            {
                //Clean
                PolicyListTablePage.Delete(PolicyName);
            }

            if (PolicyListTablePage.IsFound(DuplicatedPolicyName))
            {
                //Clean
                PolicyListTablePage.Delete(DuplicatedPolicyName);
            }
        }

        [Test]
        public void Should_addPolicy_When_administratorPolicy()
        {
            //Arrange
            MenuPage.OpenPolicies();

            //Act
            CreateAdminPolicy(PolicyName);

            //Assert
            Assert.AreEqual(true, PolicyListTablePage.IsFound(PolicyName));

            //Clean
            PolicyListTablePage.Delete(PolicyName);
        }

        [Test]
        public void Should_addPolicy_When_apostrophePolicy()
        {
            //Arrange
            MenuPage.OpenPolicies();

            //Act
            CreateAdminPolicy(ApostrophePolicyName);

            //Assert
            Assert.AreEqual(true, PolicyListTablePage.IsFound(ApostrophePolicyName));

            //Clean
            PolicyListTablePage.Delete(ApostrophePolicyName);
        }

        [Test]
        public void Should_updatePolicy_When_administratorPolicyChangedToPublisherPolicy()
        {
            //Arrange
            MenuPage.OpenPolicies();

            //Act
            CreateAdminPolicy(PolicyName);
            PolicyListTablePage.EditPolicy(PolicyName);
            EditPolicyPage.SelectPublisherPolicy();
            WebDriver.Wait();
            EditPolicyPage.SelectGrantFullControl();
            EditPolicyPage.Save();
            WebDriver.Wait();

            //Assert
            PolicyListTablePage.EditPolicy(PolicyName);
            Assert.AreEqual(true, EditPolicyPage.IsPublisherWithGrantFullControl());
            EditPolicyPage.Cancel();
            WebDriver.Wait();

            //Clean
            PolicyListTablePage.Delete(PolicyName);
        }

        [Test]
        public void Should_updatePolicy_When_apostrophePolicy()
        {
            //Arrange
            MenuPage.OpenPolicies();

            //Act
            CreateAdminPolicy(ApostrophePolicyName);
            PolicyListTablePage.EditPolicy(ApostrophePolicyName);
            WebDriver.Wait();
            EditPolicyPage.AddAdminPolicy(UpdatedApostrophePolicyName);
            WebDriver.Wait();

            //Assert
            Assert.AreEqual(true, PolicyListTablePage.IsFound(UpdatedApostrophePolicyName));
            WebDriver.Wait();

            //Clean
            PolicyListTablePage.Delete(UpdatedApostrophePolicyName);
        }

        [Test]
        public void Should_showPublishersCountForPolicy_When_policyHasOnePublisher()
        {
            //Arrange
            MenuPage.OpenPolicies();
            CreateAdminPolicy(PolicyName);
            MenuPage.OpenPublishersList();
            CreatePublisher(PublisherName, PublisherPassword, PolicyName);

            //Act
            MenuPage.OpenPolicies();
            var publishersCount = int.Parse(PolicyListTablePage.GetCellValueByIndexAndSearchValue(PolicyName, 2));

            //Assert
            Assert.AreEqual(1, publishersCount);
            WebDriver.Wait();

            //Clean
            MenuPage.OpenPublishersList();
            PublisherListTablePage.Delete(PublisherName);
            MenuPage.OpenPolicies();
            PolicyListTablePage.Delete(PolicyName);
        }

        [Test]
        public void Should_havePublishersListButtonInPolicyActionList_When_policyCreatedRight()
        {
            //Arrange
            MenuPage.OpenPolicies();

            //Act
            CreateAdminPolicy(PolicyName);

            //Assert
            Assert.AreEqual(true, PolicyListTablePage.IsFound(PolicyName));

            //Clean
            PolicyListTablePage.Delete(PolicyName);
        }

        [Test]
        public void Should_showListOfPublishers_When_clickingAtShowPublisherListButton()
        {
            //Arrange
            MenuPage.OpenPolicies();
            CreateAdminPolicy(PolicyName);
            MenuPage.OpenPublishersList();
            CreatePublisher(PublisherName, PublisherPassword, PolicyName);
            MenuPage.OpenPolicies();

            //Act
            PolicyListTablePage.ClickShowPublishersButtonForPolicy(PolicyName);
            PolicyListTablePage.FocusOnEditorsFrame();

            //Assert
            Assert.AreEqual(true, ViewEditorsListPage.IsInTable(PublisherName));
            WebDriver.Wait();

            //Clean
            MenuPage.OpenPublishersList();
            PublisherListTablePage.Delete(PublisherName);
            MenuPage.OpenPolicies();
            PolicyListTablePage.Delete(PolicyName);
        }

        private void CreateAdminPolicy(string policyName)
        {
            PolicyListTablePage.ClickAddPolicyButton();
            EditPolicyPage.AddAdminPolicy(policyName);
            WebDriver.Wait();
        }

        [Test]
        public void Should_duplicatePolicy_When_administratorPolicy()
        {
            //Arrange
            MenuPage.OpenPolicies();
            CreateAdminPolicy(PolicyName);

            //Act
            PolicyListTablePage.DuplicatePolicy(PolicyName);
            WebDriver.Wait();

            //Assert
            Assert.AreEqual(true, PolicyListTablePage.IsFound(DuplicatedPolicyName));
            WebDriver.Wait();

            //Clean
            PolicyListTablePage.Delete(PolicyName);
            PolicyListTablePage.Delete(DuplicatedPolicyName);
        }

        [Test]
        public void Should_addedTwoOus_When_createdPublisherPolicy()
        {
            //Arrange
            WebDriver.Wait();
            MenuPage.OpenPolicies();

            //Act                      
            PolicyListTablePage.ClickAddPolicyButton();

            EditPolicyPage.SetPolicyName(PolicyName);
            EditPolicyPage.SelectPublisherPolicy();
            EditPolicyPage.SelectGrantFullControl();
            EditPolicyPage.ChooseListOfRecipientsTab();
            EditPolicyPage.ChooseSendingToSpecifiedRecipientsRadio();
            EditPolicyPage.ChooseOusTab();

            PolicyOusFramePage.Focus();
            PolicyOusFramePage.ToggleTreeElements();
            PolicyOusFramePage.ChooseTwoRandomTreeCheckboxes();
            PolicyOusFramePage.FocusOnBody();

            EditPolicyPage.ClickOnAddRecipientButton();

            //Assert
            var ousInRecipientsListTotal = EditPolicyPage.GetOusInRecipientsListTotal();
            Assert.AreEqual(2, ousInRecipientsListTotal);
        }

        [Test]
        public void Should_existedTwoOus_When_editedPublisherPolicy()
        {
            //Arrange
            WebDriver.Wait();
            MenuPage.OpenPolicies();

            //Act                      
            PolicyListTablePage.ClickAddPolicyButton();

            EditPolicyPage.SetPolicyName(PolicyName);
            EditPolicyPage.SelectPublisherPolicy();
            EditPolicyPage.SelectGrantFullControl();
            EditPolicyPage.ChooseListOfRecipientsTab();
            EditPolicyPage.ChooseSendingToSpecifiedRecipientsRadio();
            EditPolicyPage.ChooseOusTab();

            PolicyOusFramePage.Focus();
            PolicyOusFramePage.ToggleTreeElements();
            PolicyOusFramePage.ChooseTwoRandomTreeCheckboxes();
            PolicyOusFramePage.FocusOnBody();

            EditPolicyPage.ClickOnAddRecipientButton();
            EditPolicyPage.Save();

            //Act
            PolicyListTablePage.EditPolicy(PolicyName);

            EditPolicyPage.ChooseListOfRecipientsTab();
            EditPolicyPage.ChooseSendingToSpecifiedRecipientsRadio();
            EditPolicyPage.ChooseOusTab();

            //Assert
            var ousInRecipientsListTotal = EditPolicyPage.GetOusInRecipientsListTotal();
            Assert.AreEqual(2, ousInRecipientsListTotal);

            //Clean
            EditPolicyPage.Cancel();
            PolicyListTablePage.Delete(PolicyName);
        }

        [Test]
        public void Should_selectedCanSendToSpecifiedRecipientsOnlyRadio_When_savedPublisherPolicy()
        {
            //Arrange
            WebDriver.Wait();
            MenuPage.OpenPolicies();                     
            PolicyListTablePage.ClickAddPolicyButton();

            EditPolicyPage.SetPolicyName(PolicyName);
            EditPolicyPage.SelectPublisherPolicy();
            EditPolicyPage.SelectGrantFullControl();
            EditPolicyPage.ChooseListOfRecipientsTab();
            EditPolicyPage.ChooseSendingToSpecifiedRecipientsRadio();
            EditPolicyPage.ChooseOusTab();

            PolicyOusFramePage.Focus();
            PolicyOusFramePage.ToggleTreeElements();
            PolicyOusFramePage.ChooseTwoRandomTreeCheckboxes();
            PolicyOusFramePage.FocusOnBody();

            EditPolicyPage.ClickOnAddRecipientButton();
            EditPolicyPage.Save();

            //Act
            PolicyListTablePage.EditPolicy(PolicyName);
            EditPolicyPage.ChooseListOfRecipientsTab();

            //Assert
            Assert.True(EditPolicyPage.IsSelectedSendingToSpecifiedRecipientsRadio());

            //Clean
            EditPolicyPage.Cancel();
            PolicyListTablePage.Delete(PolicyName);
        }

        [TearDown]
        public void RunAfterTest()
        {
        }
    }
}
