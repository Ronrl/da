using DeskAlerts.UIAutomationTests.Pages.RecipientsSelection;
using DeskAlerts.UIAutomationTests.Pages.Videoalerts;
using DeskAlerts.UIAutomationTests.Tests;
using NUnit.Framework;

namespace DeskAlerts.UIAutomationTests.Modules.Alerts
{
    [TestFixture]
    public class VideoalertTests : BaseAdminAuthorizationTest
    {
        private VideoalertsPage _videoalertPage;
        private RecipientsSelectionPage _recipientsSelectionPage;

        [SetUp]
        public void RunBeforeTest()
        {
            _videoalertPage = new VideoalertsPage(ChromeDriver);
            _recipientsSelectionPage = new RecipientsSelectionPage(ChromeDriver);

            WebDriver.Wait();

            //Delete user
            MenuPage.OpenOrganizationAudience();
            OrgUsersFramePage.Focus();
            if (OrgUsersFramePage.IsFoundWithoutCleaning(TestUserName))
            {
                OrgUsersFramePage.Delete(TestUserName);
            }
            OrgUsersFramePage.FocusOnBody();
        }

        [TestCase("DeskAlerts360p.mp4")]
        public void Should_foundOnDraftPage_When_VideoalertSaved(string filename)
        {
            //Arrange
            var settings = new VideoalertSettings
            {
                Title = $"I'm videoalert on base {filename}",
                Description = "Simple little videoalert",
                Filename = filename
            };

            //Act
            MenuPage.OpenVideoalertsCreate();
            _videoalertPage.CreateVideoalert(settings);
            _videoalertPage.SaveVideoalert();
            MenuPage.OpenVideoalertsDraft();

            //Assert
            Assert.AreEqual(true, PopupSentPage.IsFound(settings.Title));

            //Deleting
            PopupSentPage.DeleteWithConfirmation(settings.Title);

            MenuPage.OpenDashboard();
        }

        [TestCase("DeskAlerts360p.mp4")]
        public void Should_foundOnPopupSentPage_When_VideoalertSentToSelectedRecipient(string filename)
        {
            //Arrange
            var settings = new VideoalertSettings
            {
                Title = $"I'm videoalert on base {filename} sended to a test user",
                Description = "Simple little videoalert",
                Filename = filename
            };

            MenuPage.OpenOrganizationAudience();
            OrganizationsPage.ClickAddUser();
            EditUsersPage.AddUser(TestUserName, TestUserPassword);
            EditUsersPage.ClickAdd();

            //Act
            MenuPage.OpenVideoalertsCreate();
            _videoalertPage.CreateVideoalert(settings);
            _videoalertPage.SaveAndSendVideoalert();

            _recipientsSelectionPage.SelectRecipients();
            RecipientsFramePage.Focus();
            RecipientsFramePage.SelectByUserName(TestUserName);
            RecipientsFramePage.FocusOnBody();
            _recipientsSelectionPage.ClickSendButton();

            //Assert
            Assert.AreEqual(true, PopupSentPage.IsFound(settings.Title));

            //Deleting
            PopupSentPage.DeleteWithConfirmation(settings.Title);

            MenuPage.OpenOrganizationAudience();
            OrganizationsPage.FocusOnOrgUsersFrame();
            OrganizationsPage.Delete(TestUserName);
        }

        [TestCase("DeskAlerts360p.mp4")]
        public void Should_foundOnPopupSentPage_When_VideoalertSentToBroadcast(string filename)
        {
            //Arrange
            var settings = new VideoalertSettings
            {
                Title = $"I'm videoalert on base {filename} sended by broadcast",
                Description = "Simple little videoalert",
                Filename = filename
            };

            //Act
            MenuPage.OpenVideoalertsCreate();
            _videoalertPage.CreateVideoalert(settings);
            _videoalertPage.SaveAndSendVideoalert();

            _recipientsSelectionPage.SendBroadcast();
            _recipientsSelectionPage.ClickSendButton();

            //Assert
            Assert.AreEqual(true, PopupSentPage.IsFound(settings.Title));

            //Deleting
            PopupSentPage.DeleteWithConfirmation(settings.Title);

            MenuPage.OpenDashboard();
        }
    }
}
