﻿using NUnit.Framework;

namespace DeskAlerts.UIAutomationTests.Tests
{
    [TestFixture]
    public class GroupsTests : BaseAdminAuthorizationTest
    {
        private const string ParentGroup = "Parent_Group";
        private const string NestedGroup1 = "Nested_Group_1";
        private const string NestedGroup2 = "Nested_Group_2";
        private const string User1 = "User_1";
        private const string User2 = "User_2";

        [SetUp]
        public void RunBeforeTest()
        {
            
        }

        [TearDown]
        public void RunAfterTest()
        {

        }

        [Test]
        public void Should_FoundNestedGroup_When_AddNestedGroupToParentGroup()
        {
            #region Clean

            //Delete users
            MenuPage.OpenOrganizationAudience();
            OrgUsersFramePage.Focus();
            if (OrgUsersFramePage.IsFound(User1))
            {
                OrgUsersFramePage.Delete(User1);
            }

            OrgUsersFramePage.FocusOnBody();

            //Clear groups
            OrganizationsPage.SelectGroups();
            OrgGroupsFramePage.Focus();
            if (OrgGroupsFramePage.IsFound(NestedGroup1))
            {
                OrgGroupsFramePage.Delete(NestedGroup1);
            }

            if (OrgGroupsFramePage.IsFound(ParentGroup))
            {
                OrgGroupsFramePage.Delete(ParentGroup);
            }

            OrgGroupsFramePage.FocusOnBody();

            #endregion

            //Arrange
            MenuPage.OpenOrganizationAudience();
            OrganizationsPage.ClickAddUser();
            EditUsersPage.AddUser(User1, TestUserPassword);
            EditUsersPage.ClickAdd();
            WebDriver.Wait();

            AddGroupWithUser(NestedGroup1, User1);
            AddGroupWithGroup(ParentGroup, NestedGroup1);

            //Act
            OrganizationsPage.SelectGroups();
            OrgGroupsFramePage.Focus();
            OrgGroupsFramePage.Search(ParentGroup);

            //Assert
            Assert.AreEqual("1", OrgGroupsFramePage.GetCellValueByIndexAndSearchValue(ParentGroup, 5));
        }

        [Test]
        public void Should_FoundParentGroup_When_AddNestedGroupToParentGroup()
        {
            #region Clean

            //Delete users
            MenuPage.OpenOrganizationAudience();
            OrgUsersFramePage.Focus();
            if (OrgUsersFramePage.IsFound(User1))
            {
                OrgUsersFramePage.Delete(User1);
            }

            OrgUsersFramePage.FocusOnBody();

            //Clear groups
            OrganizationsPage.SelectGroups();
            OrgGroupsFramePage.Focus();
            if (OrgGroupsFramePage.IsFound(NestedGroup1))
            {
                OrgGroupsFramePage.Delete(NestedGroup1);
            }

            if (OrgGroupsFramePage.IsFound(ParentGroup))
            {
                OrgGroupsFramePage.Delete(ParentGroup);
            }

            OrgGroupsFramePage.FocusOnBody();

            #endregion

            //Arrange
            MenuPage.OpenOrganizationAudience();
            OrganizationsPage.ClickAddUser();
            EditUsersPage.AddUser(User1, TestUserPassword);
            EditUsersPage.ClickAdd();
            WebDriver.Wait();

            AddGroupWithUser(NestedGroup1, User1);
            AddGroupWithGroup(ParentGroup, NestedGroup1);

            //Act
            OrganizationsPage.SelectGroups();
            OrgGroupsFramePage.Focus();
            OrgGroupsFramePage.Search(NestedGroup1);

            //Assert
            Assert.AreEqual("1", OrgGroupsFramePage.GetCellValueByIndexAndSearchValue(NestedGroup1, 6));
        }

        [Test]
        public void Should_DeleteParentGroup_When_AddNestedGroupToParentGroup()
        {
            #region Clean

            //Delete users
            MenuPage.OpenOrganizationAudience();
            OrgUsersFramePage.Focus();
            if (OrgUsersFramePage.IsFound(User1))
            {
                OrgUsersFramePage.Delete(User1);
            }

            OrgUsersFramePage.FocusOnBody();

            //Clear groups
            OrganizationsPage.SelectGroups();
            OrgGroupsFramePage.Focus();
            if (OrgGroupsFramePage.IsFound(NestedGroup1))
            {
                OrgGroupsFramePage.Delete(NestedGroup1);
            }

            if (OrgGroupsFramePage.IsFound(ParentGroup))
            {
                OrgGroupsFramePage.Delete(ParentGroup);
            }

            OrgGroupsFramePage.FocusOnBody();

            #endregion

            //Arrange
            MenuPage.OpenOrganizationAudience();
            OrganizationsPage.ClickAddUser();
            EditUsersPage.AddUser(User1, TestUserPassword);
            EditUsersPage.ClickAdd();
            WebDriver.Wait();

            AddGroupWithUser(NestedGroup1, User1);
            AddGroupWithGroup(ParentGroup, NestedGroup1);

            //Act
            OrganizationsPage.SelectGroups();
            OrgGroupsFramePage.Focus();
            OrgGroupsFramePage.Delete(NestedGroup1);
            OrgGroupsFramePage.CleanSearch();
            OrgGroupsFramePage.Search(ParentGroup);

            //Assert
            Assert.AreEqual("0", OrgGroupsFramePage.GetCellValueByIndexAndSearchValue(ParentGroup, 5));
        }

        [Test]
        public void Should_DeleteNestedGroup_When_AddNestedGroupToParentGroup()
        {
            #region Clean

            //Delete users
            MenuPage.OpenOrganizationAudience();
            OrgUsersFramePage.Focus();
            if (OrgUsersFramePage.IsFound(User1))
            {
                OrgUsersFramePage.Delete(User1);
            }

            OrgUsersFramePage.FocusOnBody();

            //Clear groups
            OrganizationsPage.SelectGroups();
            OrgGroupsFramePage.Focus();
            if (OrgGroupsFramePage.IsFound(NestedGroup1))
            {
                OrgGroupsFramePage.Delete(NestedGroup1);
            }

            if (OrgGroupsFramePage.IsFound(ParentGroup))
            {
                OrgGroupsFramePage.Delete(ParentGroup);
            }

            OrgGroupsFramePage.FocusOnBody();

            #endregion

            //Arrange
            MenuPage.OpenOrganizationAudience();
            OrganizationsPage.ClickAddUser();
            EditUsersPage.AddUser(User1, TestUserPassword);
            EditUsersPage.ClickAdd();
            WebDriver.Wait();

            AddGroupWithUser(NestedGroup1, User1);
            AddGroupWithGroup(ParentGroup, NestedGroup1);

            //Act
            OrganizationsPage.SelectGroups();
            OrgGroupsFramePage.Focus();
            OrgGroupsFramePage.Delete(ParentGroup);
            OrgGroupsFramePage.CleanSearch();
            OrgGroupsFramePage.Search(NestedGroup1);

            //Assert
            Assert.AreEqual("0", OrgGroupsFramePage.GetCellValueByIndexAndSearchValue(NestedGroup1, 6));
        }

        private void AddGroupWithUser(string group, string user)
        {
            MenuPage.OpenOrganizationAudience();
            OrganizationsPage.ClickAddGroup();
            EditGroupsPage.AddGroup(group);
            EditGroupMembersPage.UsersTabClick();
            PolicyUsersFramePage.Focus();
            if (PolicyUsersFramePage.Search(user))
            {
                PolicyUsersFramePage.SelectByName(user);
                PolicyUsersFramePage.FocusOnBody();
                EditGroupMembersPage.AddClick();
            }

            OrganizationsPage.ClickSaveButton();
            WebDriver.Wait();
        }

        private void AddGroupWithGroup(string group, string nestedGroup)
        {
            MenuPage.OpenOrganizationAudience();
            OrganizationsPage.ClickAddGroup();
            EditGroupsPage.AddGroup(group);
            EditGroupMembersPage.GroupsTabClick();
            PolicyGroupsFramePage.Focus();
            if (PolicyGroupsFramePage.Search(nestedGroup))
            {
                PolicyGroupsFramePage.SelectByName(nestedGroup);
                PolicyGroupsFramePage.FocusOnBody();
                EditGroupMembersPage.AddClick();
            }
            else
            {
                PolicyGroupsFramePage.FocusOnBody();
            }

            OrganizationsPage.ClickSaveButton();
            WebDriver.Wait();
        }
    }
}
