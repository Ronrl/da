﻿using DeskAlerts.ApplicationCore.Entities;
using NUnit.Framework;

namespace DeskAlerts.UIAutomationTests.Tests
{
    [TestFixture]
    public class AlertsTests : BaseAdminAuthorizationTest
    {
        [SetUp]
        public void RunBeforeTest()
        {
            MenuPage.OpenAlertSent();
            if (PopupSentPage.IsFoundWithoutCleaning(BroadcastTitle))
            {
                PopupSentPage.DeleteWithConfirmation(BroadcastTitle);
            }

            //TODO: This is a crutch
            MenuPage.OpenAlertSent();
            if (PopupSentPage.IsFoundWithoutCleaning(SelectedRecipientsTitle))
            {
                PopupSentPage.DeleteWithConfirmation(SelectedRecipientsTitle);
            }

            //Delete user
            MenuPage.OpenOrganizationAudience();
            OrgUsersFramePage.Focus();
            if (OrgUsersFramePage.IsFoundWithoutCleaning(TestUserName))
            {
                OrgUsersFramePage.Delete(TestUserName);
            }
            
            OrgUsersFramePage.FocusOnBody();
        }

        [Test]
        public void Should_foundAlertOnPopupSentPage_When_alertSendToBroadcast()
        {
            //Arrange

            //Act
            MenuPage.OpenCreateAlert();
            CreateAlertPage.SelectAlertType(AlertType.SimpleAlert);

            AlertTitleFramePage.Focus();
            AlertTitleFramePage.SetTitle(BroadcastTitle);
            AlertTitleFramePage.FocusOnBody();

            AlertBodyFramePage.Focus();
            AlertBodyFramePage.SetBody(BroadcastBody);
            AlertBodyFramePage.FocusOnBody();

            CreateAlertPage.ClickSaveAndNext();
            RecipientsSelectionPage.SendBroadcast();
            RecipientsSelectionPage.ClickSendButton();
            WebDriver.Wait();

            //Assert
            Assert.AreEqual(true, PopupSentPage.IsFound(BroadcastTitle));

            //Delete alert
            PopupSentPage.DeleteWithConfirmation(BroadcastTitle);
        }

        [Test]
        public void Should_foundAlertOnPopupSentPage_When_alertSendToSelectedRecipients()
        {
            //Arrange
            MenuPage.OpenOrganizationAudience();
            WebDriver.Wait();
            OrganizationsPage.ClickAddUser();
            EditUsersPage.AddUser(TestUserName, TestUserPassword);
            EditUsersPage.ClickAdd();

            //Act
            MenuPage.OpenCreateAlert();

            AlertTitleFramePage.Focus();
            AlertTitleFramePage.SetTitle(SelectedRecipientsTitle);
            AlertTitleFramePage.FocusOnBody();

            AlertBodyFramePage.Focus();
            AlertBodyFramePage.SetBody(SelectedRecipientsBody);
            AlertBodyFramePage.FocusOnBody();
            
            CreateAlertPage.ClickSaveAndNext();

            RecipientsSelectionPage.SelectRecipients();
            RecipientsFramePage.Focus();
            RecipientsFramePage.SelectByUserName(TestUserName);
            RecipientsFramePage.FocusOnBody();
            RecipientsSelectionPage.ClickSendButton();
            WebDriver.Wait();

            //Assert
            Assert.AreEqual(true, PopupSentPage.IsFound(SelectedRecipientsTitle));

            //Delete alert
            PopupSentPage.DeleteWithConfirmation(SelectedRecipientsTitle);

            //Delete user
            MenuPage.OpenOrganizationAudience();
            OrgUsersFramePage.Focus();
            OrgUsersFramePage.Delete(TestUserName);
            OrgUsersFramePage.FocusOnBody();
        }

        [Test]
        public void Should_foundAlertOnPopupSentPage_When_alertWithAcknowledgmentSendToBroadcast()
        {
            //Arrange

            //Act
            MenuPage.OpenCreateAlert();
            CreateAlertPage.SelectAlertType(AlertType.SimpleAlert);

            AlertTitleFramePage.Focus();
            AlertTitleFramePage.SetTitle(BroadcastTitle);
            AlertTitleFramePage.FocusOnBody();

            AlertBodyFramePage.Focus();
            AlertBodyFramePage.SetBody(BroadcastBody);
            AlertBodyFramePage.FocusOnBody();

            CreateAlertPage.ClickAknowledgmentCheckbox();
            CreateAlertPage.ClickSaveAndNext();
            RecipientsSelectionPage.SendBroadcast();
            RecipientsSelectionPage.ClickSendButton();
            WebDriver.Wait();

            //Assert
            Assert.AreEqual(true, PopupSentPage.IsFound(BroadcastTitle));

            //Delete alert
            PopupSentPage.DeleteWithConfirmation(BroadcastTitle);
        }

        [Test]
        public void Should_foundAlertOnPopupSentPage_When_alertWithAcknowledgmentSendToSelectedRecipients()
        {
            //Arrange
            WebDriver.Wait();

            MenuPage.OpenOrganizationAudience();
            OrganizationsPage.ClickAddUser();
            EditUsersPage.AddUser(TestUserName, TestUserPassword);
            EditUsersPage.ClickAdd();

            //Act
            MenuPage.OpenCreateAlert();
            CreateAlertPage.SelectAlertType(AlertType.SimpleAlert);

            AlertTitleFramePage.Focus();
            AlertTitleFramePage.SetTitle(SelectedRecipientsTitle);
            AlertTitleFramePage.FocusOnBody();

            AlertBodyFramePage.Focus();
            AlertBodyFramePage.SetBody(SelectedRecipientsBody);
            AlertBodyFramePage.FocusOnBody();
            
            CreateAlertPage.ClickAknowledgmentCheckbox();
            CreateAlertPage.ClickSaveAndNext();

            RecipientsSelectionPage.SelectRecipients();
            RecipientsFramePage.Focus();
            RecipientsFramePage.SelectByUserName(TestUserName);
            AlertBodyFramePage.FocusOnBody();
            RecipientsSelectionPage.ClickSendButton();

            //Assert
            Assert.AreEqual(true, PopupSentPage.IsFound(SelectedRecipientsTitle));

            //Delete alert
            PopupSentPage.DeleteWithConfirmation(SelectedRecipientsTitle);

            //Delete user
            MenuPage.OpenOrganizationAudience();
            OrgUsersFramePage.Focus();
            OrgUsersFramePage.Delete(TestUserName);
            OrgUsersFramePage.FocusOnBody();
        }

        [Test]
        public void Should_foundAlertOnTheDraftPage_When_clickOnSaveButton()
        {
            //Arrange

            //Act
            MenuPage.OpenCreateAlert();
            CreateAlertPage.SelectAlertType(AlertType.SimpleAlert);

            AlertTitleFramePage.Focus();
            AlertTitleFramePage.SetTitle(DraftAlertTitle);
            AlertTitleFramePage.FocusOnBody();

            AlertBodyFramePage.Focus();
            AlertBodyFramePage.SetBody(DraftAlertBody);
            AlertBodyFramePage.FocusOnBody();

            CreateAlertPage.ClickSave();
            WebDriver.Wait();
            MenuPage.OpenAlertDraft();

            //Assert
            Assert.AreEqual(true, PopupSentPage.IsFound(DraftAlertTitle));

            //Delete alert
            PopupSentPage.DeleteWithConfirmation(DraftAlertTitle);
        }

        [Test]
        public void Should_foundAlertOnPopupSentPage_When_alertDuplicatedAndSendToSelectedRecipients()
        {
            //Arrange
            WebDriver.Wait();

            //Sending origin alert
            MenuPage.OpenOrganizationAudience();
            OrganizationsPage.ClickAddUser();
            EditUsersPage.AddUser(TestUserName, TestUserPassword);
            EditUsersPage.ClickAdd();

            MenuPage.OpenCreateAlert();
            AlertTitleFramePage.Focus();
            AlertTitleFramePage.SetTitle(SelectedRecipientsTitle);
            AlertTitleFramePage.FocusOnBody();

            AlertBodyFramePage.Focus();
            AlertBodyFramePage.SetBody(SelectedRecipientsBody);
            AlertBodyFramePage.FocusOnBody();

            CreateAlertPage.ClickSaveAndNext();

            RecipientsSelectionPage.SelectRecipients();
            RecipientsFramePage.Focus();
            RecipientsFramePage.SelectByUserName(TestUserName);
            RecipientsFramePage.FocusOnBody();
            RecipientsSelectionPage.ClickSendButton();

            //Act

            //Duplication, editing and sending
            PopupSentPage.DuplicateAlert(SelectedRecipientsTitle);

            var editedAlertTitle = $"{SelectedRecipientsTitle} edited";
            var editedAlertBody = $"{SelectedRecipientsBody} edited";

            AlertTitleFramePage.Focus();
            AlertTitleFramePage.SetTitle(editedAlertTitle);
            AlertTitleFramePage.FocusOnBody();

            AlertBodyFramePage.Focus();
            AlertBodyFramePage.SetBody(editedAlertBody);
            AlertBodyFramePage.FocusOnBody();

            CreateAlertPage.ClickSaveAndNext();
            RecipientsSelectionPage.ClickSendButton();

            //Assert
            Assert.AreEqual(true, PopupSentPage.IsFound(editedAlertTitle));

            //Deleting
            PopupSentPage.DeleteWithConfirmation(editedAlertTitle);

            MenuPage.OpenOrganizationAudience();
            OrgUsersFramePage.Focus();
            OrgUsersFramePage.Delete(TestUserName);
            OrgUsersFramePage.FocusOnBody();
        }

        [Test]
        public void Should_foundTickerOnPopupSentPage_When_alertSendToSelectedRecipients()
        {
            //Arrange
            MenuPage.OpenOrganizationAudience();
            WebDriver.Wait();
            OrganizationsPage.ClickAddUser();
            EditUsersPage.AddUser(TestUserName, TestUserPassword);
            EditUsersPage.ClickAdd();

            //Act
            MenuPage.OpenCreateAlert();
            CreateAlertPage.SelectAlertType(AlertType.Ticker);

            AlertTitleFramePage.Focus();
            AlertTitleFramePage.SetTitle(SelectedRecipientsTitle);
            AlertTitleFramePage.FocusOnBody();

            AlertBodyFramePage.Focus();
            AlertBodyFramePage.SetBody(SelectedRecipientsBody);
            AlertBodyFramePage.FocusOnBody();
            
            CreateAlertPage.ClickSaveAndNext();

            RecipientsSelectionPage.SelectRecipients();
            RecipientsFramePage.Focus();
            RecipientsFramePage.SelectByUserName(TestUserName);
            AlertBodyFramePage.FocusOnBody();
            RecipientsSelectionPage.ClickSendButton();
            WebDriver.Wait();

            //Assert
            Assert.AreEqual(true, PopupSentPage.IsFound(SelectedRecipientsTitle));

            //Delete alert
            PopupSentPage.DeleteWithConfirmation(SelectedRecipientsTitle);

            //Delete user
            MenuPage.OpenOrganizationAudience();
            OrgUsersFramePage.Focus();
            OrgUsersFramePage.Delete(TestUserName);
            OrgUsersFramePage.FocusOnBody();
        }

        [Test]
        public void Should_foundTickerOnPopupSentPage_When_alertSendToBroadcast()
        {
            //Arrange

            //Act
            MenuPage.OpenCreateAlert();
            CreateAlertPage.SelectAlertType(AlertType.Ticker);

            AlertTitleFramePage.Focus();
            AlertTitleFramePage.SetTitle(BroadcastTitle);
            AlertTitleFramePage.FocusOnBody();

            AlertBodyFramePage.Focus();
            AlertBodyFramePage.SetBody(BroadcastBody);
            AlertBodyFramePage.FocusOnBody();

            CreateAlertPage.ClickSaveAndNext();
            RecipientsSelectionPage.SendBroadcast();
            RecipientsSelectionPage.ClickSendButton();
            WebDriver.Wait();

            //Assert
            Assert.AreEqual(true, PopupSentPage.IsFound(BroadcastTitle));

            //Delete alert
            PopupSentPage.DeleteWithConfirmation(BroadcastTitle);
        }

        [Test]
        public void Should_foundTickerOnPopupSentPage_When_alertWithAcknowledgmentSendToSelectedRecipients()
        {
            //Arrange
            MenuPage.OpenOrganizationAudience();
            WebDriver.Wait();
            OrganizationsPage.ClickAddUser();
            EditUsersPage.AddUser(TestUserName, TestUserPassword);
            EditUsersPage.ClickAdd();

            //Act
            MenuPage.OpenCreateAlert();
            CreateAlertPage.SelectAlertType(AlertType.Ticker);

            AlertTitleFramePage.Focus();
            AlertTitleFramePage.SetTitle(SelectedRecipientsTitle);
            AlertTitleFramePage.FocusOnBody();

            AlertBodyFramePage.Focus();
            AlertBodyFramePage.SetBody(SelectedRecipientsBody);
            AlertBodyFramePage.FocusOnBody();
            
            CreateAlertPage.ClickAknowledgmentCheckbox();
            CreateAlertPage.ClickSaveAndNext();

            RecipientsSelectionPage.SelectRecipients();
            RecipientsFramePage.Focus();
            RecipientsFramePage.SelectByUserName(TestUserName);
            AlertBodyFramePage.FocusOnBody();
            RecipientsSelectionPage.ClickSendButton();
            WebDriver.Wait();

            //Assert
            Assert.AreEqual(true, PopupSentPage.IsFound(SelectedRecipientsTitle));

            //Delete alert
            PopupSentPage.DeleteWithConfirmation(SelectedRecipientsTitle);

            //Delete user
            MenuPage.OpenOrganizationAudience();
            OrgUsersFramePage.Focus();
            OrgUsersFramePage.Delete(TestUserName);
            OrgUsersFramePage.FocusOnBody();
        }

        [Test]
        public void Should_foundTickerOnPopupSentPage_When_alertWithAcknowledgmentSendToBroadcast()
        {
            //Arrange

            //Act
            MenuPage.OpenCreateAlert();
            CreateAlertPage.SelectAlertType(AlertType.Ticker);

            AlertTitleFramePage.Focus();
            AlertTitleFramePage.SetTitle(BroadcastTitle);
            AlertTitleFramePage.FocusOnBody();

            AlertBodyFramePage.Focus();
            AlertBodyFramePage.SetBody(BroadcastBody);
            AlertBodyFramePage.FocusOnBody();

            CreateAlertPage.ClickAknowledgmentCheckbox();
            CreateAlertPage.ClickSaveAndNext();
            RecipientsSelectionPage.SendBroadcast();
            RecipientsSelectionPage.ClickSendButton();
            WebDriver.Wait();

            //Assert
            Assert.AreEqual(true, PopupSentPage.IsFound(BroadcastTitle));

            //Delete alert
            PopupSentPage.DeleteWithConfirmation(BroadcastTitle);
        }

        [Test]
        public void Should_foundTickerAtDraftPage_When_clickOnSaveButton()
        {
            //Arrange

            //Act
            MenuPage.OpenCreateAlert();
            CreateAlertPage.SelectAlertType(AlertType.Ticker);

            AlertTitleFramePage.Focus();
            AlertTitleFramePage.SetTitle(DraftAlertTitle);
            AlertTitleFramePage.FocusOnBody();

            AlertBodyFramePage.Focus();
            AlertBodyFramePage.SetBody(DraftAlertBody);
            AlertBodyFramePage.FocusOnBody();

            CreateAlertPage.ClickSave();
            WebDriver.Wait();
            MenuPage.OpenAlertDraft();

            //Assert
            Assert.AreEqual(true, PopupSentPage.IsFound(DraftAlertTitle));

            //Delete alert
            PopupSentPage.DeleteWithConfirmation(DraftAlertTitle);
        }

        [Test]
        public void Should_foundTickerOnPopupSentPage_When_tickerDuplicatedAndSendToSelectedRecipients()
        {
            //Arrange
            WebDriver.Wait();

            //Sending origin ticker
            MenuPage.OpenOrganizationAudience();         
            OrganizationsPage.ClickAddUser();
            EditUsersPage.AddUser(TestUserName, TestUserPassword);
            EditUsersPage.ClickAdd();

            MenuPage.OpenCreateAlert();
            CreateAlertPage.SelectAlertType(AlertType.Ticker);

            AlertTitleFramePage.Focus();
            AlertTitleFramePage.SetTitle(SelectedRecipientsTitle);
            AlertTitleFramePage.FocusOnBody();

            AlertBodyFramePage.Focus();
            AlertBodyFramePage.SetBody(SelectedRecipientsBody);
            AlertBodyFramePage.FocusOnBody();

            CreateAlertPage.ClickSaveAndNext();

            RecipientsSelectionPage.SelectRecipients();
            RecipientsFramePage.Focus();
            RecipientsFramePage.SelectByUserName(TestUserName);
            AlertBodyFramePage.FocusOnBody();
            RecipientsSelectionPage.ClickSendButton();

            //Act

            //Duplication, editing and sending
            PopupSentPage.DuplicateAlert(SelectedRecipientsTitle);

            var editedAlertTitle = $"{SelectedRecipientsTitle} edited";
            var editedAlertBody = $"{SelectedRecipientsBody} edited";

            AlertTitleFramePage.Focus();
            AlertTitleFramePage.SetTitle(editedAlertTitle);
            AlertTitleFramePage.FocusOnBody();

            AlertBodyFramePage.Focus();
            AlertBodyFramePage.SetBody(editedAlertBody);
            AlertBodyFramePage.FocusOnBody();

            CreateAlertPage.ClickSaveAndNext();
            RecipientsSelectionPage.ClickSendButton();

            //Assert
            Assert.AreEqual(true, PopupSentPage.IsFound(editedAlertTitle));

            //Deleting
            PopupSentPage.DeleteWithConfirmation(editedAlertTitle);

            MenuPage.OpenOrganizationAudience();
            OrgUsersFramePage.Focus();
            OrgUsersFramePage.Delete(TestUserName);
            OrgUsersFramePage.FocusOnBody();
        }

        [Test]
        public void Should_foundRsvpOnPopupSentPage_When_rsvpSendToSelectedRecipients()
        {
            //Arrange
            WebDriver.Wait();

            MenuPage.OpenOrganizationAudience(); 
            OrganizationsPage.ClickAddUser();
            EditUsersPage.AddUser(TestUserName, TestUserPassword);
            EditUsersPage.ClickAdd();

            //Act
            MenuPage.OpenCreateAlert();
            CreateAlertPage.SelectAlertType(AlertType.Rsvp);

            AlertTitleFramePage.Focus();
            AlertTitleFramePage.SetTitle(SelectedRecipientsTitle);
            AlertTitleFramePage.FocusOnBody();

            AlertBodyFramePage.Focus();
            AlertBodyFramePage.SetBody(SelectedRecipientsBody);
            AlertBodyFramePage.FocusOnBody();

            CreateAlertPage.ClickSaveAndNext();

            RecipientsSelectionPage.SelectRecipients();
            RecipientsFramePage.Focus();
            RecipientsFramePage.SelectByUserName(TestUserName);
            AlertBodyFramePage.FocusOnBody();
            RecipientsSelectionPage.ClickSendButton();

            //Assert
            Assert.AreEqual(true, PopupSentPage.IsFound(SelectedRecipientsTitle));

            //Deleting
            PopupSentPage.DeleteWithConfirmation(SelectedRecipientsTitle);

            MenuPage.OpenOrganizationAudience();
            OrgUsersFramePage.Focus();
            OrgUsersFramePage.Delete(TestUserName);
            OrgUsersFramePage.FocusOnBody();
        }

        [Test]
        public void Should_foundRsvpOnPopupSentPage_When_RsvpSendToBroadcast()
        {
            //Arrange
            WebDriver.Wait();

            //Act
            MenuPage.OpenCreateAlert();
            CreateAlertPage.SelectAlertType(AlertType.Rsvp);

            AlertTitleFramePage.Focus();
            AlertTitleFramePage.SetTitle(BroadcastTitle);
            AlertTitleFramePage.FocusOnBody();

            AlertBodyFramePage.Focus();
            AlertBodyFramePage.SetBody(BroadcastBody);
            AlertBodyFramePage.FocusOnBody();

            CreateAlertPage.ClickSaveAndNext();
            RecipientsSelectionPage.SendBroadcast();
            RecipientsSelectionPage.ClickSendButton();

            //Assert
            Assert.AreEqual(true, PopupSentPage.IsFound(BroadcastTitle));

            //Deleting
            PopupSentPage.DeleteWithConfirmation(BroadcastTitle);
        }

        [Test]
        public void Should_foundRsvpAtDraftPage_When_clickOnSaveButton()
        {
            //Arrange
            WebDriver.Wait();

            //Act
            MenuPage.OpenCreateAlert();
            CreateAlertPage.SelectAlertType(AlertType.Rsvp);

            AlertTitleFramePage.Focus();
            AlertTitleFramePage.SetTitle(DraftAlertTitle);
            AlertTitleFramePage.FocusOnBody();

            AlertBodyFramePage.Focus();
            AlertBodyFramePage.SetBody(DraftAlertBody);
            AlertBodyFramePage.FocusOnBody();

            CreateAlertPage.ClickSave();
            
            MenuPage.OpenAlertDraft();

            //Assert
            Assert.AreEqual(true, PopupSentPage.IsFound(DraftAlertTitle));

            //Delete alert
            PopupSentPage.DeleteWithConfirmation(DraftAlertTitle);
        }

        [Test]
        public void Should_foundRsvpOnPopupSentPage_When_RsvpDuplicatedAndSendToSelectedRecipients()
        {
            //Arrange
            WebDriver.Wait();

            //Sending origin ticker
            MenuPage.OpenOrganizationAudience();
            OrganizationsPage.ClickAddUser();
            EditUsersPage.AddUser(TestUserName, TestUserPassword);
            EditUsersPage.ClickAdd();

            MenuPage.OpenCreateAlert();
            CreateAlertPage.SelectAlertType(AlertType.Rsvp);

            AlertTitleFramePage.Focus();
            AlertTitleFramePage.SetTitle(SelectedRecipientsTitle);
            AlertTitleFramePage.FocusOnBody();

            AlertBodyFramePage.Focus();
            AlertBodyFramePage.SetBody(SelectedRecipientsBody);
            AlertBodyFramePage.FocusOnBody();

            CreateAlertPage.ClickSaveAndNext();

            RecipientsSelectionPage.SelectRecipients();
            RecipientsFramePage.Focus();
            RecipientsFramePage.SelectByUserName(TestUserName);
            AlertBodyFramePage.FocusOnBody();
            RecipientsSelectionPage.ClickSendButton();

            //Act

            //Duplication, editing and sending
            PopupSentPage.DuplicateRsvp(SelectedRecipientsTitle);

            var editedAlertTitle = $"{SelectedRecipientsTitle} edited";
            var editedAlertBody = $"{SelectedRecipientsBody} edited";

            AlertTitleFramePage.Focus();
            AlertTitleFramePage.SetTitle(editedAlertTitle);
            AlertTitleFramePage.FocusOnBody();

            AlertBodyFramePage.Focus();
            AlertBodyFramePage.SetBody(editedAlertBody);
            AlertBodyFramePage.FocusOnBody();

            CreateAlertPage.ClickSaveAndNext();
            RecipientsSelectionPage.ClickSendButton();

            //Assert
            Assert.AreEqual(true, PopupSentPage.IsFound(editedAlertTitle));

            //Deleting
            PopupSentPage.DeleteWithConfirmation(editedAlertTitle);

            MenuPage.OpenOrganizationAudience();
            OrgUsersFramePage.Focus();
            OrgUsersFramePage.Delete(TestUserName);
            OrgUsersFramePage.FocusOnBody();
        }
    }
}
