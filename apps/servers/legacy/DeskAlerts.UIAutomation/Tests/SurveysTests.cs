using NUnit.Framework;

namespace DeskAlerts.UIAutomationTests.Tests
{
    [TestFixture]
    public class SurveysTests : BaseAdminAuthorizationTest
    {
        [SetUp]
        public void RunBeforeTest()
        {
            WebDriver.SetImplicitWait(10);
        }

        [TearDown]
        public void RunAfterTest()
        {
            //Delete test user
            MenuPage.OpenOrganizationAudience();
            OrgUsersFramePage.Focus();
            if (OrgUsersFramePage.IsFoundWithoutCleaning(TestUserName))
            {
                OrgUsersFramePage.Delete(TestUserName);
            }
            MenuPage.OpenDashboard();
        }

        [Test]
        public void Should_foundSurveyOnPopupSentPage_When_surveySendToBroadcast()
        {
            //Arrange

            //Act
            MenuPage.OpenSurveysCreate();

            SurveysPage.SetTitle(SurveyBroadcastTitle);
            SurveysPage.ClickNext();

            SurveysQuestionPage.SetTitle(SurveyQuestionTitle);
            SurveysQuestionPage.SetOptionsBySameValues(2, SurveyQuestionOption);
            SurveysQuestionPage.ClickOnSaveAndNextButton();

            RecipientsSelectionPage.SendBroadcast();
            RecipientsSelectionPage.ClickSendButton();

            //Assert
            Assert.AreEqual(true, PopupSentPage.IsFound(SurveyBroadcastTitle));

            //Delete survey
            MenuPage.OpenSurveysActive();
            PopupSentPage.DeleteWithConfirmation(SurveyBroadcastTitle);
            MenuPage.OpenDashboard();
        }

        [Test]
        public void Should_foundSurveyOnPopupSentPage_When_surveySendToSelectedRecipients()
        {
            //Arrange
            MenuPage.OpenOrganizationAudience();
            OrganizationsPage.ClickAddUser();
            EditUsersPage.AddUser(TestUserName, TestUserPassword);
            EditUsersPage.ClickAdd();

            //Act
            MenuPage.OpenSurveysCreate();

            SurveysPage.SetTitle(SurveyRecipientsTitle);
            SurveysPage.ClickNext();

            SurveysQuestionPage.SetTitle(SurveyQuestionTitle);
            SurveysQuestionPage.SetOptionsBySameValues(2, SurveyQuestionOption);
            SurveysQuestionPage.ClickOnSaveAndNextButton();

            RecipientsSelectionPage.SelectRecipients();
            RecipientsFramePage.Focus();
            RecipientsFramePage.SelectByUserName(TestUserName);
            RecipientsFramePage.FocusOnBody();
            RecipientsSelectionPage.ClickSendButton();

            //Assert
            Assert.AreEqual(true, PopupSentPage.IsFound(SurveyRecipientsTitle));
            
            //Delete survey
            MenuPage.OpenSurveysActive();
            if (PopupSentPage.IsFoundWithoutCleaning(SurveyRecipientsTitle))
            {
                PopupSentPage.DeleteWithConfirmation(SurveyRecipientsTitle);
            }
        }

        [Test]
        public void Should_foundSurveyOnPopupSentPage_When_surveyWithManyQuestionsSendToBroadcast()
        {
            //Arrange

            //Act
            MenuPage.OpenSurveysCreate();

            SurveysPage.SetTitle(SurveyBroadcastTitle);
            SurveysPage.ClickNext();

            //Add first question
            SurveysQuestionPage.SetTitle(SurveyQuestionTitle);
            SurveysQuestionPage.SetOptionsBySameValues(2, SurveyQuestionOption);
            SurveysQuestionPage.ClickOnSaveAndAddNewQuestionButton();

            //Add second question
            SurveysQuestionPage.SetTitle($"{SurveyQuestionTitle} 2");
            SurveysQuestionPage.SetOptionsBySameValues(2, SurveyQuestionOption);
            SurveysQuestionPage.ClickOnSaveAndAddNewQuestionButton();

            //Add third question
            SurveysQuestionPage.SetTitle($"{SurveyQuestionTitle} 2");
            SurveysQuestionPage.SetOptionsBySameValues(2, SurveyQuestionOption);
            SurveysQuestionPage.ClickOnSaveAndNextButton();

            RecipientsSelectionPage.SendBroadcast();
            RecipientsSelectionPage.ClickSendButton();

            //Assert
            Assert.AreEqual(true, PopupSentPage.IsFound(SurveyBroadcastTitle));

            //Delete survey
            MenuPage.OpenSurveysActive();
            PopupSentPage.DeleteWithConfirmation(SurveyBroadcastTitle);
            MenuPage.OpenDashboard();
        }

        [Test]
        public void Should_foundSurveyOnPopupSentPage_When_surveyWithManyAnswersSendToBroadcast()
        {
            //Arrange

            //Act
            MenuPage.OpenSurveysCreate();

            SurveysPage.SetTitle(SurveyBroadcastTitle);
            SurveysPage.ClickNext();

            SurveysQuestionPage.SetTitle(SurveyQuestionTitle);
            SurveysQuestionPage.ClickOnAddOptionButton();
            SurveysQuestionPage.ClickOnAddOptionButton();
            SurveysQuestionPage.ClickOnAddOptionButton();
            SurveysQuestionPage.SetOptionsBySameValues(5, SurveyQuestionOption);
            SurveysQuestionPage.ClickOnSaveAndNextButton();

            RecipientsSelectionPage.SendBroadcast();
            RecipientsSelectionPage.ClickSendButton();

            //Assert
            Assert.AreEqual(true, PopupSentPage.IsFound(SurveyBroadcastTitle));

            //Delete survey
            MenuPage.OpenSurveysActive();
            PopupSentPage.DeleteWithConfirmation(SurveyBroadcastTitle);
            MenuPage.OpenDashboard();
        }

        [Test]
        public void Should_foundDuplicatedSurveyOnPopupSentPage_When_surveyIsDuplicatedFromActive()
        {
            //Arrange
            MenuPage.OpenSurveysCreate();

            SurveysPage.SetTitle(SurveyBroadcastTitle);
            SurveysPage.ClickNext();

            SurveysQuestionPage.SetTitle(SurveyQuestionTitle);
            SurveysQuestionPage.SetOptionsBySameValues(2, SurveyQuestionOption);
            SurveysQuestionPage.ClickOnSaveAndNextButton();

            RecipientsSelectionPage.SendBroadcast();
            RecipientsSelectionPage.ClickSendButton();

            //Act
            PopupSentPage.DuplicateSurvey(SurveyBroadcastTitle);

            SurveysPage.SetTitle(SurveyEditedTitle);
            SurveysPage.ClickNext();

            SurveysQuestionPage.SetTitle($"{SurveyQuestionTitle} edited");
            SurveysQuestionPage.SetOptionsBySameValues(2, SurveyQuestionOption);
            SurveysQuestionPage.ClickOnSaveAndNextButton();

            RecipientsSelectionPage.SendBroadcast();
            RecipientsSelectionPage.ClickSendButton();

            //Assert
            Assert.AreEqual(true, PopupSentPage.IsFound(SurveyEditedTitle));

            //Delete surveys
            MenuPage.OpenSurveysActive();
            PopupSentPage.DeleteWithConfirmation(SurveyBroadcastTitle);
            MenuPage.OpenSurveysActive();
            PopupSentPage.DeleteWithConfirmation(SurveyEditedTitle);
            MenuPage.OpenDashboard();
        }
    }
}
