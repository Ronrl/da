﻿using NUnit.Framework;

namespace DeskAlerts.UIAutomationTests.Tests
{
    [TestFixture]
    public class AdTests : BaseAdminAuthorizationTest
    {
        private const string SynchronizationName = "ActiveDirectory techdep.alert-software.com";
        private const string DomainName = "techdep.alert-software.com";
        private const string DomainUserName = "adsync";
        private const string DomainUserPassword = "ad4Sync";
        private const string DomainPort = "389";

        [SetUp]
        public void RunBeforeTest()
        {
            //Delete user
            MenuPage.OpenOrganizationAudience();
            OrgUsersFramePage.Focus();
            if (OrgUsersFramePage.IsFound(TestUserName))
            {
                OrgUsersFramePage.Delete(TestUserName);
            }

            OrgUsersFramePage.FocusOnBody();

            //Delete ActiveDirectory techdep.alert-software.com
            MenuPage.OpenSynchronizations();
            if (SynchronizationsPage.IsFound(SynchronizationName))
            {
                SynchronizationsPage.Delete(SynchronizationName);
            }
        }

        [TearDown]
        public void RunAfterTest()
        {

        }

        [Test]
        public void Should_foundUser_When_userCreated()
        {
            //Arrange

            //Act
            MenuPage.OpenOrganizationAudience();
            OrganizationsPage.ClickAddUser();
            EditUsersPage.AddUser(TestUserName, TestUserPassword);
            EditUsersPage.ClickAdd();

            //Assert
            OrgUsersFramePage.Focus();
            Assert.AreEqual(true, OrgUsersFramePage.IsFound(TestUserName));

            //Clean
            MenuPage.OpenOrganizationAudience();
            OrgUsersFramePage.Focus();
            if (OrgUsersFramePage.IsFound(TestUserName))
            {
                OrgUsersFramePage.Delete(TestUserName);
            }

            OrgUsersFramePage.FocusOnBody();
        }


        [TestCase(DomainName, DomainUserName, DomainUserPassword, DomainPort)]
        public void Should_createSynchronization_When_domainTechdep(string domainName, string userName, string password, string port)
        {
            //Arrange

            //Act
            MenuPage.OpenSynchronizations();
            SynchronizationsPage.ClickAddNewSynchronizationButton();
            AdSynchronizationFormPage.CreateSyncronization(SynchronizationName, domainName, userName, password, port);
            WebDriver.Wait();
            AdSynchronizationFormPage.Save();

            //Assert
            Assert.AreEqual(true, SynchronizationsPage.IsFound(SynchronizationName));
        }
    }
}
