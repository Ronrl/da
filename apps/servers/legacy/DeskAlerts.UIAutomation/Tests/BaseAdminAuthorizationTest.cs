﻿using NUnit.Framework;

namespace DeskAlerts.UIAutomationTests.Tests
{
    [TestFixture]
    public class BaseAdminAuthorizationTest : BaseTest
    {
        [SetUp]
        public void Authorize()
        {
            AuthPage.SetUserName("admin");
            AuthPage.SetPassword("admin");
            AuthPage.ClickLoginButton();
        }
        
    }
}
