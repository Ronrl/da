﻿using NUnit.Framework;
using OpenQA.Selenium;

namespace DeskAlerts.UIAutomationTests.Tests
{
    [TestFixture]
    public class AutorizationTests : BaseTest
    {
        [Test]
        public void Should_authorized_When_loginIsIncorrect()
        {
            //Arrange
            AuthPage.SetUserName("admin");
            AuthPage.SetPassword("incorrectPassword");

            //Act
            AuthPage.ClickLoginButton();

            //Assert
            Assert.AreEqual(false, AuthPage.IsAuthorized());
        }

        [Test]
        public void Should_unauthorized_When_loginIsCorrect()
        {
            //Arrange
            AuthPage.SetUserName("admin");
            AuthPage.SetPassword("admin");

            //Act
            AuthPage.ClickLoginButton();

            //Assert
            Assert.Throws<NoSuchElementException>(delegate { AuthPage.IsAuthorized(); });
        }
    }
}
