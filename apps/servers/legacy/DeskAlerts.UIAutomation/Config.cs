﻿using System;
using System.IO;
using NUnit.Framework;

namespace DeskAlerts.UIAutomationTests
{
    public static class Config
    {
        public static string GetUrl()
        {
            try
            {
                var configPath = Path.Combine($"{TestContext.CurrentContext.TestDirectory}\\testconfig.txt");
                return File.ReadAllText(configPath);
            }
            catch (FileNotFoundException)
            {
                Console.WriteLine("Файл конфигурации подключения к серверу testconfig.txt не найден");
                throw;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }
    }
}
