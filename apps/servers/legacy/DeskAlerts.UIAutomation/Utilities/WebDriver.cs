﻿using System;
using OpenQA.Selenium.Chrome;

namespace DeskAlerts.UIAutomationTests.Utilities
{
    public class WebDriver : IDisposable
    {
        private readonly ChromeDriver _chromeDriver;

        public WebDriver(ChromeDriver driver)
        {
            _chromeDriver = driver;
        }

        public void Open()
        {
            _chromeDriver.Manage().Window.Maximize();
            _chromeDriver.Navigate().GoToUrl(Config.GetUrl());
        }

        public void Close()
        {
            _chromeDriver.Quit();
        }

        public void Wait()
        {
            _chromeDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
        }

        public void SetImplicitWait(int seconds)
        {
            _chromeDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(seconds);
        }

        public void Dispose()
        {
            _chromeDriver.Close();
            _chromeDriver.Dispose();
        }
    }
}
