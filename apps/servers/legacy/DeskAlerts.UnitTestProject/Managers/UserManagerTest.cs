using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.UnitTestProject.FakeRepositories;
using DeskAlerts.Server.Exceptions;
using DeskAlertsDotNet.Managers;
using NUnit.Framework;
using System;

namespace DeskAlerts.UnitTestProject.Managers
{
    [TestFixture]
    public class UserManagerTest
    {
        public UserManager Target { get; private set; }

        private FakeDomainRepository _domainRepository;
        private FakeUserRepository _userRepository;
        private FakeGroupRepository _groupRepository;
        private FakePolicyRepository _policyRepository;
        private FakeUserGroupRepository _userGroupRepository;
        private FakePolicyPublisherRepository _policyPublisherRepository;
        private FakeSkinRepository _skinRepository;
        private FakeContentSettingsRepository _contentSettingsRepository;

        public DeskAlertsDotNet.Models.User User;

        [SetUp]
        public void SetUp()
        {
            _skinRepository = new FakeSkinRepository();
            var skin = new Skin
            {
                Id = Guid.Parse("26821b87-06ce-48fe-b17d-9dc3affeec95"),
                Name = "Test skin"
            };
            _skinRepository.SkinList.Add(skin);

            _contentSettingsRepository = new FakeContentSettingsRepository();

            _domainRepository = new FakeDomainRepository();
            _userGroupRepository = new FakeUserGroupRepository();
            _userRepository = new FakeUserRepository(_domainRepository);
            _groupRepository = new FakeGroupRepository
            {
                UserRepository = _userRepository,
                UserGroupRepository = _userGroupRepository
            };
            _policyRepository = new FakePolicyRepository
            {
                UserGroupRepository = _userGroupRepository,
                GroupRepository = _groupRepository
            };
            _groupRepository.PolicyRepository = _policyRepository;
            _policyPublisherRepository = new FakePolicyPublisherRepository();

            _domainRepository.Create(new Domain { Id = 1, Name = "domain" });
            _domainRepository.Create(new Domain { Id = 2, Name = "DeskAlerts.domain.local" });
            _domainRepository.Create(new Domain { Id = 3, Name = "staff.root.ehl.ch" });
            _domainRepository.Create(new Domain { Id = 4, Name = "root.ehl.ch" });

            _userRepository.Create(new User { Id = 1, Role = "U", Username = "Username" });
            _userRepository.Create(new User { Id = 2, Role = "U", Username = "User.name", DomainId = 2 });
            _userRepository.Create(new User { Id = 3, Role = "U", Username = "User.name.local", DomainId = 2 });
            _userRepository.Create(new User { Id = 4, Role = "E", Username = "jfootprints", DomainId = 3 });
            _userRepository.Create(new User { Id = 5, Role = "U", Username = "Username2" });
            _userRepository.Create(new User { Id = 6, Role = "U", Username = "User.name2" });
            _userRepository.Create(new User { Id = 7, Role = "U", Username = "User.name.local2" });

            _groupRepository.Create(new Group { Id = 9, PolicyId = 8, DomainId = 3 });

            _userGroupRepository.Create(new UserGroup { Id = 10, GroupId = 9, UserId = 4 });

            _policyRepository.Create(new Policy
            {
                Id = 8,
                Name = "jfootprints Policy",
                Type = "A",
                PolicyEditor = new PolicyPublisher { GroupId = 9, Id = 20, PolicyId = 8, PublisherId = 4 }
            });

            Target = new UserManager(_domainRepository, _userRepository, _groupRepository, _policyRepository,
                _policyPublisherRepository, null, null, _skinRepository, _contentSettingsRepository);

            User = new DeskAlertsDotNet.Models.User(1, "User_1", "user@mysite.com", "+1234567890");
        }

        [TestCase("bair@deskalerts")]
        [TestCase(@"deskalerts\bair")]
        public void Should_GetUserName_When_LoginIsCorrect(string login)
        {
            var userName = Target.CreateDomainUser(login).UserName;
            Assert.AreEqual("bair", userName);
        }

        [TestCase("bair@deskalerts")]
        [TestCase(@"deskalerts\bair")]
        public void Should_GetDomainName_When_LoginIsCorrect(string login)
        {
            var domainName = Target.CreateDomainUser(login).DomainName;
            Assert.AreEqual("deskalerts", domainName);
        }

        [TestCase("bair@deskalerts@com")]
        [TestCase(@"deskalerts\bair\com")]
        public void Should_ThrowError_When_LoginIsIncorrect(string login)
        {
            Assert.Throws<DomainUserValidationException>(delegate
            {
                Target.CreateDomainUser(login);
            });
        }

        [TestCase("")]
        public void Should_ThrowError_When_LoginIsEmpty(string login)
        {
            Assert.Throws<ArgumentNullException>(delegate
            {
                Target.CreateDomainUser(login);
            });
        }

        /// <summary>
        /// Test should create token when token is not null
        /// </summary>
        [Test]
        public void Should_CreateToken_When_TokenIsNotNull()
        {
            var token = Target.CreateToken(User);
            Assert.IsNotNull(token);
        }

        /// <summary>
        /// Test should authorize by token when token is added
        /// </summary>
        [Test]
        public void Should_AuthorizeByToken_When_TokenIsAdded()
        {
            var token = Target.CreateToken(User);
            Target.AddToken(token);
            var isAuthorized = Target.IsAuthorized(token.Guid);
            Assert.AreEqual(isAuthorized, true);
        }

        /// <summary>
        /// Test should authorize by token when token is expired
        /// </summary>
        [Test]
        public void Should_NotAuthorizeByToken_When_TokenIsExpired()
        {
            var token = Target.CreateToken(User);
            token.LastActiveDateTime = DateTime.Now.AddMinutes(-UserManager.TokenExpirationMinutes);
            Target.AddToken(token);
            var isAuthorized = Target.IsAuthorized(token.Guid);
            Assert.AreEqual(isAuthorized, false);
        }

        /// <summary>
        /// Test should authorize by user when user is registered
        /// </summary>
        [Test]
        public void Should_AuthorizeByToken_When_UserRegistered()
        {
            var guid = Target.Register(User);
            var isAuthorized = Target.IsAuthorized(guid);
            Assert.AreEqual(isAuthorized, true);
        }

        /// <summary>
        /// Test should authorize by token when token is not added
        /// </summary>
        [Test]
        public void Should_NotAuthorizeByToken_When_UserNotRegistered()
        {
            var isAuthorized = Target.IsAuthorized(Guid.NewGuid());
            Assert.AreEqual(isAuthorized, false);
        }

        [Test]
        public void Should_Authorize_When_WindowsAuthorization()
        {
            var domainUser = Target.CreateDomainUser("jfootprints@staff");
            var user = Target.LoginWithWindowsAuth(domainUser);
            Assert.AreEqual(4, user.Id);
        }
    }
}
