﻿using System;
using System.Collections;
using System.Text;
using System.Collections.Generic;
using DeskAlertsDotNet.DataBase;
using NUnit.Framework;

namespace DeskAlerts.UnitTestProject.Managers
{
    [TestFixture]
    public class DataRowTest
    {
        private DataRow _dataRow;

        [SetUp]
        public void SetUp()
        {
            //Arrange
            var dictionary = new Dictionary<string, object>
            {
                {"question_type", "M"}, {"question", "Test question"}, {"id", 17}
            };
            _dataRow = new DataRow(dictionary);
        }

        [Test]
        public void Should_DeleteProperty_When_DeleteByKeyId()
        {
            //Arrange
            var dictionaryById = new Dictionary<string, object> { { "question_type", "M" }, { "question", "Test question" } };
            var resultById = new DataRow(dictionaryById);

            //Act
            _dataRow.DeleteByKey("id");

            //Assert
            Assert.AreEqual(_dataRow.ToDictionary(), resultById.ToDictionary());
        }

        [Test]
        public void Should_DeleteProperty_When_DeleteByKeyQuestionType()
        {
            //Arrange
            var dictionaryByQuestionType = new Dictionary<string, object> { { "question", "Test question" }, { "id", 17 } };
            var resultByQuestionType = new DataRow(dictionaryByQuestionType);

            //Act
            _dataRow.DeleteByKey("question_type");

            //Assert
            Assert.AreEqual(_dataRow.ToDictionary(), resultByQuestionType.ToDictionary());
        }

        [TestCase("id", 17)]
        [TestCase("question", "Test question")]
        public void Should_GetValue_When_GetByKey(string key, object result)
        {
            //Arrange

            //Act
            var id = _dataRow.GetValue(key);

            //Assert
            Assert.AreEqual(id, result);
        }
    }
}
