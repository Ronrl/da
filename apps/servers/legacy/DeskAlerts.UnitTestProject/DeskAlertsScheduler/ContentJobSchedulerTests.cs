﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.ApplicationCore.Models.ContentTypes;
using DeskAlerts.ContentDeliveringService.Interfaces;
using DeskAlerts.Scheduler.ContentJobScheduling;
using DeskAlerts.UnitTestProject.FakeRepositories;
using DeskAlerts.UnitTestProject.FakeServices;
using NUnit.Framework;

namespace DeskAlerts.UnitTestProject.DeskAlertsScheduler
{
    [TestFixture]
    public class ContentJobSchedulerTests
    {
        private IAlertRepository _alertRepository;
        private IRecurrenceRepository _recurrenceRepository;
        private IContentDeliveringService<AlertContentType> _alertsContentDeliveringService;
        private IContentDeliveringService<TickerContentType> _tickersContentDeliveringService;
        private IContentDeliveringService<RsvpContentType> _rsvpContentDeliveringService;
        private IContentDeliveringService<SurveyContentType> _surveysContentDeliveringService;
        private IContentDeliveringService<ScreensaverContentType> _screensaverContentDeliveringService;
        private IContentDeliveringService<WallpaperContentType> _wallpaperContentDeliveringService;
        private IContentDeliveringService<VideoAlertContentType> _videoAlertsContentDeliveringService;
        private IScheduledContentService _repeatableContentService;

        public static (long, AlertType) Alert1 = (567, AlertType.Wallpaper);
        public static (long, AlertType) Alert2 = (1, AlertType.ScreenSaver);
        public static (long, AlertType) Alert3 = (2, AlertType.SimpleAlert);
        public static (long, AlertType) Alert4 = (3, AlertType.VideoAlert);
        public static (long, AlertType) Alert5 = (long.MaxValue, AlertType.SurveyPoll);

        [SetUp]
        public void SetUp()
        {
            _alertRepository = new FakeAlertRepository();
            _recurrenceRepository = new FakeRecurrenceRepository();

            _alertsContentDeliveringService = new FakeContentDeliveringService<AlertContentType>();
            _tickersContentDeliveringService = new FakeContentDeliveringService<TickerContentType>();
            _rsvpContentDeliveringService = new FakeContentDeliveringService<RsvpContentType>();
            _surveysContentDeliveringService = new FakeContentDeliveringService<SurveyContentType>();
            _screensaverContentDeliveringService = new FakeContentDeliveringService<ScreensaverContentType>();
            _wallpaperContentDeliveringService = new FakeContentDeliveringService<WallpaperContentType>();
            _videoAlertsContentDeliveringService = new FakeContentDeliveringService<VideoAlertContentType>();
        }

        public static IEnumerable<ContentJobDto> CorrectContentJobDto
        {
            get
            {
                yield return new ContentJobDto (DateTime.UtcNow,             Alert1.Item1, Alert1.Item2);
                yield return new ContentJobDto (DateTime.Now,                Alert2.Item1, Alert2.Item2);
                yield return new ContentJobDto (DateTime.UtcNow.AddHours(1), Alert3.Item1, Alert3.Item2);
                yield return new ContentJobDto (DateTime.UtcNow.AddDays(1),  Alert4.Item1, Alert4.Item2);
                yield return new ContentJobDto (DateTime.UtcNow,             Alert5.Item1, Alert5.Item2);
            }
        }

        public static IEnumerable<ContentJobDto> ContentJobDtoAsNull
        {
            get { yield return null; }
        }

        [TestCaseSource(nameof(CorrectContentJobDto))]
        public async Task Should_CreateJob_When_SetFullContentJobDto(ContentJobDto contentJobDto)
        {
            // Arrange
            _alertRepository.Create(new AlertEntity {Id = Alert1.Item1, Class = (int) Alert1.Item2});
            _alertRepository.Create(new AlertEntity {Id = Alert2.Item1, Class = (int) Alert2.Item2});
            _alertRepository.Create(new AlertEntity {Id = Alert3.Item1, Class = (int) Alert3.Item2});
            _alertRepository.Create(new AlertEntity {Id = Alert4.Item1, Class = (int) Alert4.Item2});
            _alertRepository.Create(new AlertEntity {Id = Alert5.Item1, Class = (int) Alert5.Item2});

            // Act
            var nCronContentJobScheduler = new QuartzContentJobScheduler(
                _alertRepository,
                _recurrenceRepository,
                _alertsContentDeliveringService,
                _tickersContentDeliveringService,
                _rsvpContentDeliveringService,
                _surveysContentDeliveringService,
                _screensaverContentDeliveringService,
                _wallpaperContentDeliveringService,
                _videoAlertsContentDeliveringService,
                _repeatableContentService);
            var result = await nCronContentJobScheduler.CreateJob(contentJobDto);

            // Assert
            Assert.AreEqual(true, result);
        }

        [TestCaseSource(nameof(ContentJobDtoAsNull))]
        public void Should_CreateJob_When_SetContentJobDtoAsNull(ContentJobDto contentJobDto)
        {
            // Arrange
            // Act
            var nCronContentJobScheduler = new QuartzContentJobScheduler(
                _alertRepository,
                _recurrenceRepository,
                _alertsContentDeliveringService,
                _tickersContentDeliveringService,
                _rsvpContentDeliveringService,
                _surveysContentDeliveringService,
                _screensaverContentDeliveringService,
                _wallpaperContentDeliveringService,
                _videoAlertsContentDeliveringService,
                _repeatableContentService);

            // Assert
            Assert.ThrowsAsync<ArgumentNullException>(() => nCronContentJobScheduler.CreateJob(contentJobDto));
        }

        [TestCaseSource(nameof(CorrectContentJobDto))]
        public void Should_DeleteJob_When_SetFullContentJobDto(ContentJobDto contentJobDto)
        {
            // Arrange
            // Act
            var nCronContentJobScheduler = new QuartzContentJobScheduler(
                _alertRepository,
                _recurrenceRepository,
                _alertsContentDeliveringService,
                _tickersContentDeliveringService,
                _rsvpContentDeliveringService,
                _surveysContentDeliveringService,
                _screensaverContentDeliveringService,
                _wallpaperContentDeliveringService,
                _videoAlertsContentDeliveringService,
                _repeatableContentService);
            var result = nCronContentJobScheduler.DeleteJob(contentJobDto);

            // Assert
            Assert.AreEqual(true, result);
        }
    }
}
