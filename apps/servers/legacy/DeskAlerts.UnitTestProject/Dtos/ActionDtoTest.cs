﻿using DeskAlerts.ApplicationCore.Dtos;
using NUnit.Framework;

namespace DeskAlerts.UnitTestProject.Dtos
{
    [TestFixture]
    public class ActionDtoTest
    {
        public ActionDto Target { get; private set; }
        private const string ImageReference = "ImageReference";
        private const string Reference = "Reference";
        private const string Text = "Text";

        [SetUp]
        public void SetUp()
        {
            Target = new ActionDto(Reference, ImageReference, Text);
        }

        [Test]
        public void Should_GetImageReference()
        {
            Assert.AreEqual(ImageReference, Target.ImageReference);
        }

        [Test]
        public void Should_GetReference()
        {
            Assert.AreEqual(Reference, Target.Reference);
        }

        [Test]
        public void Should_GetText()
        {
            Assert.AreEqual(Text, Target.Text);
        }
    }
}
