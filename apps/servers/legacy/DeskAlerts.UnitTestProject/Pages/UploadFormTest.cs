﻿using System;
using DeskAlertsDotNet.Pages;
using NUnit.Framework;

namespace DeskAlerts.UnitTestProject.Pages
{
    [TestFixture]
    public class UploadFormTest
    {
        public UploadForm Target { get; private set; }

        [SetUp]
        public void SetUp() => Target = new UploadForm();

        [Test]
        public void Should_ReturnFileInfo_When_CallComputeMethod()
        {
            //Given
            var logoPath = "..\\..\\DeskAlerts.Web\\sever\\STANDARD\\admin\\images\\upload\\logo.gif";
            var fileName = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, logoPath);

            //When
            var fileObject = Target.CreateFileObject(fileName, string.Empty);

            //Then
            Assert.AreEqual("logo.gif", fileObject.Value<string>("name"));
            Assert.AreEqual("images\\upload\\logo.gif", fileObject.Value<string>("relativePath"));
            Assert.AreEqual("67", fileObject.Value<string>("width"));
            Assert.AreEqual("16", fileObject.Value<string>("height"));
            Assert.AreEqual("513 B", fileObject.Value<string>("size"));
        }
        
    }
}
