﻿using DeskAlerts.ApplicationCore.Interfaces;

namespace DeskAlerts.UnitTestProject.Pages
{
    using ApplicationCore.Dtos;
    using Server;
    using NUnit.Framework;

    [TestFixture]
    public class RegisterFormTest
    {
        public RegisterForm Target { get; private set; }

        [SetUp]
        public void SetUp()
        {
            Target = new RegisterForm();
        }

        /// <summary>
        /// The password must be MD5
        /// </summary>
        [Test]
        public void Should_MakeUser_When_PasswordIsMd5()
        {
            // Given
            var userInfo = new UserInfoDto
            {
                Password = "password"                
            };

            const int domainId = 0;

            // Then
            var user = Target.GetUser(userInfo, domainId);
            Assert.AreEqual("5F4DCC3B5AA765D61D8327DEB882CF99", user.Password);
        }

        [Test]
        public void Should_ValidateUser_When_PhoneIsCorrect()
        {
            // Given
            var userInfo = new UserInfoDto
                               {
                                   Username = "username",
                                   Password = "password",
                                   Phone = "79123456987"
                               };

            // Then
            var validationResult = Target.ValidateUserInfo(userInfo, false);
            Assert.AreEqual(true, validationResult.IsSuccess);
        }

        [Test]
        public void Should_ValidateUser_When_PhoneContainsPlus()
        {
            // Given
            var userInfo = new UserInfoDto
                               {
                                   Username = "username",
                                   Password = "password",
                                   Phone = "+79123456987"
                               };

            // Then
            var validationResult = Target.ValidateUserInfo(userInfo, false);
            Assert.AreEqual(true, validationResult.IsSuccess);
        }

        [Test]
        public void Should_ReturnErrorMessage_When_PhoneIsIncorrect()
        {
            // Given
            var userInfo = new UserInfoDto
                               {
                                   Username = "username",
                                   Password = "password",
                                   Phone = "some incorrect phone"
                               };

            // Then
            var validationResult = Target.ValidateUserInfo(userInfo, false);
            Assert.AreEqual(false, validationResult.IsSuccess);
            Assert.AreEqual("Error! Phone number is invalid.", validationResult.ErrorMessage);
        }

        [Test]
        public void Should_ValidateUser_When_EmailIsCorrect()
        {
            // Given
            var userInfo = new UserInfoDto
                               {
                                   Username = "username",
                                   Password = "password",
                                   Email = "email@email.com"
                               };

            // Then
            var validationResult = Target.ValidateUserInfo(userInfo, false);
            Assert.AreEqual(true, validationResult.IsSuccess);
        }

        [Test]
        public void Should_ReturnErrorMessage_When_EmailIsIncorrect()
        {
            // Given
            var userInfo = new UserInfoDto
                               {
                                   Username = "username",
                                   Password = "password",
                                   Email = "some incorrect email"
                               };

            // Then
            var validationResult = Target.ValidateUserInfo(userInfo, false);
            Assert.AreEqual(false, validationResult.IsSuccess);
            Assert.AreEqual("Error! Email is invalid.", validationResult.ErrorMessage);
        }

        [Test]
        public void Should_ReturnErrorMessage_When_EmailDoesNotContainAt()
        {
            // Given
            var userInfo = new UserInfoDto
                               {
                                   Username = "username",
                                   Password = "password",
                                   Email = "emailemail.ru"
                               };

            // Then
            var validationResult = Target.ValidateUserInfo(userInfo, false);
            Assert.AreEqual(false, validationResult.IsSuccess);
            Assert.AreEqual("Error! Email is invalid.", validationResult.ErrorMessage);
        }

        [Test]
        public void Should_ReturnErrorMessage_When_EmailDoesNotContainDomain()
        {
            // Given
            var userInfo = new UserInfoDto
                               {
                                   Username = "username",
                                   Password = "password",
                                   Email = "email@email"
                               };

            // Then
            var validationResult = Target.ValidateUserInfo(userInfo, false);
            Assert.AreEqual(false, validationResult.IsSuccess);
            Assert.AreEqual("Error! Email is invalid.", validationResult.ErrorMessage);
        }
    }
}
