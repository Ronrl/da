﻿using FluentAssertions;
using NUnit.Framework;
using System.Collections.Generic;

namespace DeskAlerts.UnitTestProject.Pages
{
    /// <summary>
    /// The AddAlert page tests
    /// </summary>
    [TestFixture]
    public class AddAlertTest
    {
        private const string TickerText = "1";
        private bool isTickerEnabled = true;
        private bool isTickerProEnabled = true;

        /// <summary>
        /// The set up.
        /// </summary>
        [SetUp]
        public void SetUp()
        {
        }

        /// <summary>
        /// The test should add ticker speed to alert when call HandleTickerSpeed method
        /// </summary>
        [Test]
        public void Should_AddTickerSpeedTextOnAddAlertPage_When_CallHandleTickerSpeedMethod()
        {
            // Given
            string tickerSpeed = "1";
            string tickerContent = "<p>Ticker body</p>";

            var dictionary = new Dictionary<string, object> { { "", new object() } };
            var tuple = (0, tickerContent, dictionary);

            // When
            (int tTicker, string tContent, Dictionary<string, object> tAlertFields) result =
                Server.Utils.Pages.TickerHelper.HandleTickerSpeed(TickerText, tickerSpeed, this.isTickerEnabled, this.isTickerProEnabled, tuple);

            // Then
            result.tContent.Should().Be($"<p>Ticker body</p><!-- ticker_pro --><!-- tickerSpeed={tickerSpeed} -->");
        }

        /// <summary>
        /// The test should add ticker speed to duplicated alert when call HandleTickerSpeed method
        /// </summary>
        [Test]
        public void Should_AddTickerSpeedTextOnAddAlertPageWithDuplicatedAlert_When_CallHandleTickerSpeedMethod()
        {
            // Given
            string tickerSpeed = "1";
            string newTickerSpeed = "7";
            var tickerContent = $"<p>Ticker body</p><!-- ticker_pro --><!-- tickerSpeed={tickerSpeed} -->";

            var dictionary = new Dictionary<string, object> { { "", new object() } };
            var tuple = (0, tickerContent, dictionary);

            // When
            (int tTicker, string tContent, Dictionary<string, object> tAlertFields) result =
                Server.Utils.Pages.TickerHelper.HandleTickerSpeed(TickerText, newTickerSpeed, this.isTickerEnabled, this.isTickerProEnabled, tuple);

            // Then
            result.tContent.Should().Be($"<p>Ticker body</p><!-- ticker_pro --><!-- tickerSpeed={newTickerSpeed} -->");
        }
    }
}
