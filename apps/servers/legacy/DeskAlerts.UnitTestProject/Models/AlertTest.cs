﻿using DeskAlerts.ApplicationCore.Entities;

namespace DeskAlerts.UnitTestProject.Models
{
    using System;
    using System.Globalization;

    using DeskAlertsDotNet.Models;
    using DeskAlertsDotNet.Utils.Consts;
    using NUnit.Framework;

    /// <summary>
    /// The alert tests.
    /// </summary>
    /// 
    [TestFixture]
    public class AlertTest
    {
        /// <summary>
        /// Gets the target.
        /// </summary>
        public Alert Target { get; private set; }

        /// <summary>
        /// The set up.
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            Target = new Alert();
        }

        /// <summary>
        /// The alert should became expire when time has passed
        /// </summary>
        [Test]
        public void Should_AlertBecameExpire_When_TimeHasPassed()
        {
            // Given
            var futureDateTime = DateTime.Now.AddHours(-1);
            Target.ToDate = futureDateTime.ToString(DateTimeFormat.JavaScript, CultureInfo.InvariantCulture);
            var result = Target.IsExpired(DateTime.Now);

            // Then
            Assert.AreEqual(result, true);
        }

        /// <summary>
        /// The alert shouldn't became expire when time has not passed
        /// </summary>
        [Test]
        public void Should_AlertNotBecameExpire_When_TimeHasNotPassed()
        {
            // Given
            var pastDateTime = DateTime.Now.AddHours(1);
            Target.ToDate = pastDateTime.ToString(DateTimeFormat.JavaScript, CultureInfo.InvariantCulture);
            var result = Target.IsExpired(DateTime.Now);

            // Then
            Assert.AreEqual(result, false);
        }

        /// <summary>
        /// The alert should start when time hasn't passed
        /// </summary>
        [Test]
        public void Should_AlertNotStart_When_TimeHasPassed()
        {
            // Given
            var future = DateTime.Now.AddHours(1);
            Target.FromDate = future.ToString(DateTimeFormat.JavaScript, CultureInfo.InvariantCulture);
            var result = Target.IsStarted(DateTime.Now);

            // Then
            Assert.AreEqual(result, false);
        }

        /// <summary>
        /// The alert should start when time hasn't passed
        /// </summary>
        [Test]
        public void Should_AlertStart_When_TimeHasNotPassed()
        {
            // Given
            var past = DateTime.Now.AddHours(-1);
            Target.FromDate = past.ToString(DateTimeFormat.JavaScript, CultureInfo.InvariantCulture);
            var result = Target.IsStarted(DateTime.Now);

            // Then
            Assert.AreEqual(result, true);
        }

        [Test]
        public void Should_PassParameter_When_EncriptionIsEnabled()
        {
            // Arrange
            Target.Class = (int) AlertType.SimpleAlert;

            // Act
            var content = Target.GetContentUrl(string.Empty, string.Empty, string.Empty, DateTime.Now, 0, true);
            
            // Assert
            Assert.AreEqual(content.Contains("&amp;isMobile=1"), true);
        }
    }
}
