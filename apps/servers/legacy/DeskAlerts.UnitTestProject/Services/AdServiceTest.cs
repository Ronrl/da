﻿using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.Server.Utils.Services;
using DeskAlerts.UnitTestProject.FakeRepositories;
using NUnit.Framework;

namespace DeskAlerts.UnitTestProject.Services
{
    [TestFixture]
    public class AdServiceTest
    {
        public AdService Target { get; private set; }

        [SetUp]
        public void SetUp()
        {
            var domainRepository = new FakeDomainRepository();
            domainRepository.Create(new Domain() { Id = 1, Name = "corp.amex" });
            domainRepository.Create(new Domain() { Id = 2, Name = "amex" });
            domainRepository.Create(new Domain() { Id = 3, Name = "DeskAlerts.local" });
            Target = new AdService(domainRepository);
        }

        /// <summary>
        /// Test get domain
        /// </summary>
        [Test]
        public void Should_GetDomain_When_NameIsEqual()
        {
            var domain = Target.GetDomain("amex");
            Assert.AreEqual(domain.Id, 2);
        }

        /// <summary>
        /// Test get sub domain
        /// </summary>
        [Test]
        public void Should_GetDomain_When_NameIsSubdomain()
        {
            var domain = Target.GetDomain("corp");
            Assert.AreEqual(domain.Id, 1);
        }

        /// <summary>
        /// Test get sub domain
        /// </summary>
        [Test]
        public void Should_GetDomain_When_NameIsInLowerCase()
        {
            var domain = Target.GetDomain("deskalerts.local");
            Assert.AreEqual(domain.Id, 3);
        }
    }
}
