﻿using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.Server.Utils.Interfactes;
using DeskAlerts.UnitTestProject.FakeRepositories;
using DeskAlertsDotNet.Utils.Services;
using NUnit.Framework;
using System;

namespace DeskAlerts.UnitTestProject.Services
{

    [TestFixture]
    public class UserServiceTest
    {
        public IUserService Target { get; private set; }

        private const string UserNameFirst = "User1";
        private const string UserNameSecond = "User2";
        private const string UserNameThird = "User3";
        private const string DomainFirst = "Domain1";
        private const string DomainSecond = "Domain2";
        private const string DomainThird = "Domain3";
        private const string SeparateSign = "/";

        [SetUp]
        public void SetUp()
        {
            var domainRepo = new FakeDomainRepository();
            var userRepo = new FakeUserRepository(domainRepo);
            var groupRepo = new FakeGroupRepository();

            userRepo.Create(new User { Id = 1, Username = UserNameFirst, DomainId = 1, PollPeriod = "5"});
            userRepo.Create(new User { Id = 2, Username = UserNameSecond, DomainId = 2, PollPeriod = "5"});
            userRepo.Create(new User { Id = 3, Username = UserNameThird, DomainId = 3, PollPeriod = "5"});

            domainRepo.Create(new Domain() { Id=1, Name= DomainFirst });
            domainRepo.Create(new Domain() { Id=2, Name= DomainSecond });
            domainRepo.Create(new Domain() { Id=3, Name= DomainThird });

            Target = new UserService(userRepo, groupRepo);
        }

        [Test]
        public void Should_ReturnUser_When_UserNameAndDomainIsSet()
        {
            var user = Target.GetUserByUserNameAndDomainName(UserNameFirst, DomainFirst);
            Assert.AreEqual(1, user.Id);
        }

        [Test]
        public void Should_SeparateUserNameAndDomain_When_UserHaveDomain()
        {
            var userName = DomainFirst + SeparateSign + UserNameFirst;
            Assert.AreEqual(Tuple.Create(DomainFirst, UserNameFirst), Target.SeparateUserNameAndDomainName(userName));
        }

        [Test]
        public void Should_ReturnUserName_When_UserDoesntHaveDomain()
        {
            var userName = UserNameSecond;
            Assert.AreEqual(Tuple.Create(string.Empty, UserNameSecond), Target.SeparateUserNameAndDomainName(UserNameSecond));
        }

        [Test]
        public void Should_ReturnUser_When_UserDataCorrect()
        {
            var userName = DomainFirst + SeparateSign + UserNameFirst;
            Assert.AreEqual(1, Target.GetUserByName(userName).Id);
        }

        [Test]
        public void Should_JoinUserNameAndDomain_When_DomainNameExist()
        {
            Assert.AreEqual(DomainFirst + SeparateSign + UserNameFirst, Target.JoinUserNameAndDomainName(UserNameFirst, DomainFirst));
        }

        [Test]
        public void Should_ReturnRightMD5Hash_When_PasswordHasGiven()
        {
            Assert.AreEqual("21232F297A57A5A743894A0E4A801FC3", Target.ConvertToMD5Hash("admin"));
        }

        [Test]
        public void Should_ReturnTimeInSeconds_When_ClientPollPeriodHasGiven()
        {
            Assert.AreEqual("2", Target.ConvertToSeconds("2s"));
            Assert.AreEqual("3600", Target.ConvertToSeconds("1h"));
            Assert.AreEqual("1320", Target.ConvertToSeconds("22"));
            Assert.AreEqual("300", Target.ConvertToSeconds("5m"));
        }

    }
}
