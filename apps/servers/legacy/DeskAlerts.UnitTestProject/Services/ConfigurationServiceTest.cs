﻿namespace DeskAlerts.UnitTestProject.Services
{
    using DeskAlertsDotNet.Utils.Services;
    using NUnit.Framework;

    [TestFixture]
    public class ConfigurationServiceTest
    {
        public ConfigurationService Target { get; private set; }

        private const string Site = "Okta";
        private const string Application = "MyApplication";
        private const string AdminFolder = "admin";

        [SetUp]
        public void SetUp()
        {
        }

        /// <summary>
        /// Get root web site location path
        /// </summary>
        [Test]
        public void Should_GetLocationPath_When_WebSiteIsRoot()
        {
            Target = new ConfigurationService(Site, $"http://{Site}");
            Assert.AreEqual($"{Site}/{AdminFolder}", Target.LocationPath);
        }

        /// <summary>
        /// Get subdomain web site location path
        /// </summary>
        [Test]
        public void Should_GetLocationPath_When_WebSiteIsSubdomain()
        {
            Target = new ConfigurationService(Site, $"http://{Site}/{Application}");
            Assert.AreEqual($"{Site}/{Application}/{AdminFolder}", Target.LocationPath);
        }

        /// <summary>
        /// Get root web site location path with slash on end
        /// </summary>
        [Test]
        public void Should_GetLocationPath_When_WebSiteIsRootWithSlashOnEnd()
        {
            Target = new ConfigurationService(Site, $"http://{Site}/");
            Assert.AreEqual($"{Site}/{AdminFolder}", Target.LocationPath);
        }

        /// <summary>
        /// Get subdomain web site location path with slash on end
        /// </summary>
        [Test]
        public void Should_GetLocationPath_When_WebSiteIsSubdomainWithSlashOnEnd()
        {
            Target = new ConfigurationService(Site, $"http://{Site}/{Application}/");
            Assert.AreEqual($"{Site}/{Application}/{AdminFolder}", Target.LocationPath);
        }
    }
}
