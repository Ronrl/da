﻿using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.Server.Utils.Interfactes;
using DeskAlerts.Server.Utils.Services;
using DeskAlerts.UnitTestProject.FakeRepositories;
using NUnit.Framework;
using System.Globalization;
using System.Threading;

namespace DeskAlerts.UnitTestProject.Services
{
    [TestFixture]
    public class SettingsServiceTest
    {
        public ISettingsService Target { get; private set; }

        [SetUp]
        public void SetUp()
        {
            var serviceRepo = new FakeSettingsRepository();
            serviceRepo.Create(new Setting() { value = "ES" });
            serviceRepo.Create(new Setting() { value = "RU" });
            serviceRepo.Create(new Setting() { value = "EN" });
            Target = new SettingsService(serviceRepo);
        }

        /// <summary>
        /// Check APNS servers
        /// </summary>
        [Test]
        public void CheckApnsServers()
        {
            var response = Target.GetPushNotificationResponse();
            Assert.AreEqual(response.IsApnsConnected, true);
        }

        /// <summary>
        /// Check GCM servers
        /// </summary>
        [Test]
        public void CheckGcmServers()
        {
            var response = Target.GetPushNotificationResponse();
            Assert.AreEqual(response.IsGcmConnected, true);
        }


        [Test]
        public void Should_SetUserLanguage_When_UserHaveGotLanguage()
        {
            var  userLang = Target.GetLanguageByUserId(1);
            Target.SetCultureInfoForThread(userLang);
            Assert.AreEqual(Thread.CurrentThread.CurrentCulture, new CultureInfo("ru"));

        }


        [Test]
        public void Should_SetDefaultLanguage_When_ThereIsNoLanguageForUser()
        {
            var userLang = Target.GetLanguageByUserId(4);
            Target.SetCultureInfoForThread(userLang);
            Assert.AreEqual(Thread.CurrentThread.CurrentCulture, new CultureInfo("es"));
        }
    }
}
