﻿using DeskAlerts.Server.Utils.Services;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace DeskAlerts.UnitTestProject.Services
{
    [TestFixture]
    public class CacheInvalidationServiceTest
    {
        [SetUp]
        public void SetUp()
        {

        }

        [TestCase("")]
        public void Should_ArgumentNullException_When_UrlIsEmpty(string serverApiUrl)
        {
            // Arrange
            // Act
            // Assert
            Assert.Throws<ArgumentNullException>(() => CacheInvalidationService.CreateCorrectUri(serverApiUrl));
        }

        public static IEnumerable<string> UriFormatExceptionList
        {
            get
            {
                yield return "http:///deskalerts.com";
                yield return "http://deskalerts..com";
                yield return "http://deskalerts.com\\";
                yield return "http://deskalerts.com/deskalerts%%20";
                yield return "http://deskalerts.com/deskalerts?contentType=<script>";
                yield return "192.168.1.1/da";

            }
        }
        [TestCaseSource(nameof(UriFormatExceptionList))]
        public void Should_UriFormatException_When_UriAreNotCreated(string serverApiUrl)
        {
            // Arrange
            // Act
            // Assert
            Assert.Throws<UriFormatException>(() => CacheInvalidationService.CreateCorrectUri(serverApiUrl));
        }

        public static IEnumerable<string> ServerApiUrlList
        {
            get
            {
                yield return "http://deskalerts.com";
                yield return "https://deskalerts.com";
                yield return "htttp://deskalerts.com";
                yield return "http://deskalerts.com.";
                yield return "http://deskalerts.com.tr";
                yield return "http://deskalerts.com/deskalerts";
                yield return "http://deskalerts.com/deskalerts/api/v1";
                yield return "http://deskalerts.com/deskalerts/api/v1?";
                yield return "http://deskalerts.com/deskalerts?contentType=";
                yield return "http://deskalerts.com/deskalerts?contentType=Wallpapers";
            }
        }
        [TestCaseSource(nameof(ServerApiUrlList))]
        public void Should_ReturnCorrectUri_When_SetUriAsString(string serverApiUrl)
        {
            // Arrange
            Uri.TryCreate(serverApiUrl, UriKind.Absolute, out var url);

            // Act
            var result = CacheInvalidationService.CreateCorrectUri(serverApiUrl);

            // Assert
            Assert.AreEqual(url, result);
        }
    }
}
