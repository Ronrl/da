﻿namespace DeskAlerts.UnitTestProject.Services
{
    using DeskAlerts.Service.AD;
    using DeskAlerts.Service.AD.DataContracts;
    using DeskAlerts.Service.AD.ServiceContracts;
    using NUnit.Framework;
    using System;
    using System.DirectoryServices;

    [TestFixture]
    public class ADIntegrationTest
    {
        public IADService Target { get; private set; }

        [SetUp]
        public void SetUp()
        {            
            var cr = new LdapCredentials()
            {
                Host = "109.202.21.134",
                BaseDN = "DC=DeskAlerts,DC=local",
                Port = 389,
                Password = "ad4Sync",
                UserName = "adsync",
                Timeout = new TimeSpan(0, 0, 10)
            };
            Target = new ADService(cr);
        }

        /// <summary>
        /// Start the service
        /// </summary>
        [Test]
        public void CheckConnection()
        {
            var response = Target.CheckConnection();
            Assert.AreEqual(true, response);
        }

        /// <summary>
        /// Get OUs
        /// </summary>
        [Test]
        public void GetOrganizationUnits()
        {
            System.Threading.Thread.Sleep(10000);
            var response = Target.GetOrganizationUnits();
            Assert.Greater(response.Count, 0);
        }
    }
}
