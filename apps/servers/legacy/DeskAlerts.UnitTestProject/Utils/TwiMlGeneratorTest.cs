﻿using DeskAlerts.Server.Utils.Managers;
using NUnit.Framework;

namespace DeskAlerts.UnitTestProject.Utils
{
    [TestFixture]
    public class TwiMlGeneratorTest
    {
        [Test]
        public void Shoult_WrapInputWithXML_When_CallGetTWiMLFromAlertBodyMethod()
        {
            string fakeAlertBody = "Alert body";

            var result = TwiMLGenerator.GetTWiMLFromAlertBody(fakeAlertBody);

            Assert.AreEqual("<?xml version =\'1.0\' encoding=\'UTF-8\'?><Response><Say  voice=\'woman\' language=\'en-GB\' loop=\'2\'>Attention! You have new notification. Alert body</Say></Response>", result);
        }
    }
}
