﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using DeskAlerts.ApplicationCore.Utilities;
using NUnit.Framework;

namespace DeskAlerts.UnitTestProject.Utils
{
    [TestFixture]
    internal class DateTimeUtilsTest
    {
        private const int LowerTimeZone = -12;
        private const int UpperTimeZone = 14;
        private const int Inaccuracy = 1;
        
        public static IEnumerable<(int, bool)> TimeZones
        {
            get
            {
                yield return ((int)TimeZoneInfo.GetSystemTimeZones().First().BaseUtcOffset.TotalSeconds, false);
                yield return ((int)TimeZoneInfo.GetSystemTimeZones().First().BaseUtcOffset.TotalSeconds + 60, false);
                yield return (-0, false);
                yield return (0, false);
                yield return ((int)TimeZoneInfo.GetSystemTimeZones().Last().BaseUtcOffset.TotalSeconds - 60, false);
                yield return ((int)TimeZoneInfo.GetSystemTimeZones().Last().BaseUtcOffset.TotalSeconds, false);

                yield return ((int)TimeZoneInfo.GetSystemTimeZones().First().BaseUtcOffset.TotalSeconds, true);
                yield return ((int)TimeZoneInfo.GetSystemTimeZones().First().BaseUtcOffset.TotalSeconds + 60, true);
                yield return (0, true);
                yield return (-0, true);
                yield return ((int)TimeZoneInfo.GetSystemTimeZones().Last().BaseUtcOffset.TotalSeconds - 60, true);
                yield return ((int)TimeZoneInfo.GetSystemTimeZones().Last().BaseUtcOffset.TotalSeconds, true);
            }
        }

        public static IEnumerable<(string, bool)> TimeZonesAsString
        {
            get
            {
                yield return ((TimeZoneInfo.GetSystemTimeZones().First().BaseUtcOffset.TotalSeconds).ToString(CultureInfo.InvariantCulture), false);
                yield return ((TimeZoneInfo.GetSystemTimeZones().First().BaseUtcOffset.TotalSeconds + 60).ToString(CultureInfo.InvariantCulture), false);
                yield return ((-0).ToString(), false);
                yield return ((0).ToString(), false);
                yield return ((TimeZoneInfo.GetSystemTimeZones().Last().BaseUtcOffset.TotalSeconds - 60).ToString(CultureInfo.InvariantCulture), false);
                yield return ((TimeZoneInfo.GetSystemTimeZones().Last().BaseUtcOffset.TotalSeconds).ToString(CultureInfo.InvariantCulture), false);

                yield return ((TimeZoneInfo.GetSystemTimeZones().First().BaseUtcOffset.TotalSeconds).ToString(CultureInfo.InvariantCulture), true);
                yield return ((TimeZoneInfo.GetSystemTimeZones().First().BaseUtcOffset.TotalSeconds + 60).ToString(CultureInfo.InvariantCulture), true);
                yield return ((0).ToString(), true);
                yield return ((-0).ToString(), true);
                yield return ((TimeZoneInfo.GetSystemTimeZones().Last().BaseUtcOffset.TotalSeconds - 60).ToString(CultureInfo.InvariantCulture), true);
                yield return ((TimeZoneInfo.GetSystemTimeZones().Last().BaseUtcOffset.TotalSeconds).ToString(CultureInfo.InvariantCulture), true);
            }
        }

        public static IEnumerable<(int, bool)> IncorrectTimeZones
        {
            get
            {
                yield return ((int)TimeZoneInfo.GetSystemTimeZones().First().BaseUtcOffset.TotalSeconds - 1, false);
                yield return ((int)TimeZoneInfo.GetSystemTimeZones().Last().BaseUtcOffset.TotalSeconds + 1, false);

                yield return ((int)TimeZoneInfo.GetSystemTimeZones().First().BaseUtcOffset.TotalSeconds - 1, true);
                yield return ((int)TimeZoneInfo.GetSystemTimeZones().Last().BaseUtcOffset.TotalSeconds + 1, true);
            }
        }

        public static IEnumerable<double> OutOfRangeCorrectTimeZone
        {
            get
            {
                yield return (int)TimeZoneInfo.GetSystemTimeZones().First().BaseUtcOffset.TotalSeconds - 1;
                yield return (int) TimeZoneInfo.GetSystemTimeZones().Last().BaseUtcOffset.TotalSeconds + 1;
            }
        }

        public static IEnumerable<DateTime> DateTimes
        {
            get
            {
                yield return DateTime.UtcNow;
                yield return DateTime.UtcNow.AddHours(UpperTimeZone).AddMinutes(-Inaccuracy);
                yield return DateTime.UtcNow.AddHours(LowerTimeZone).AddMinutes(Inaccuracy);
                yield return DateTime.Now;
                yield return DateTime.Now.AddHours(UpperTimeZone).AddMinutes(-Inaccuracy);
                yield return DateTime.Now.AddHours(LowerTimeZone).AddMinutes(Inaccuracy);
            }
        }

        public static IEnumerable<DateTime> IncorrectDateTimes
        {
            get
            {
                yield return DateTime.UtcNow.AddHours(UpperTimeZone).AddMinutes(Inaccuracy);
                yield return DateTime.UtcNow.AddHours(LowerTimeZone).AddMinutes(-Inaccuracy);
                yield return DateTime.Now.AddHours(UpperTimeZone).AddMinutes(Inaccuracy);
                yield return DateTime.Now.AddHours(LowerTimeZone).AddMinutes(-Inaccuracy);
            }
        }


        [TestCaseSource(nameof(TimeZones))]
        public void Should_ConvertDateTimeOffsetToLocalDateTime_When_DatetimeOffsetBySeconds((int timeZone, bool isClient) tuple)
        {
            // Arrange
            var (timeZone, isClient) = tuple;
            var offset = TimeSpan.FromSeconds(timeZone);
            var dateTimeOffset = DateTime.UtcNow;
            if (isClient)
            {
                dateTimeOffset = dateTimeOffset + offset;
            }
            
            var currentTime = dateTimeOffset.ToString("u");

            // Act
            var result = DateTimeUtils.GetDateTimeByTimeZone(timeZone, isClient).ToString("u");

            // Assert
            Assert.AreEqual(result, currentTime);
        }

        [TestCaseSource(nameof(TimeZonesAsString))]
        public void Should_ConvertDateTimeOffsetToLocalDateTime_When_DatetimeOffsetBySecondsAsString((string timeZone, bool isClient) tuple)
        {
            // Arrange
            var (timeZone, isClient) = tuple;

            int.TryParse(timeZone, out var timeZoneAsInt);

            var offset = TimeSpan.FromSeconds(timeZoneAsInt);
            var dateTimeOffset = DateTime.UtcNow;
            if (isClient)
            {
                dateTimeOffset = dateTimeOffset + offset;
            }

            var currentTime = dateTimeOffset.ToString("u");

            // Act
            var result = DateTimeUtils.GetDateTimeByTimeZone(timeZone, isClient).ToString("u");

            // Assert
            Assert.AreEqual(result, currentTime);
        }

        [TestCaseSource(nameof(IncorrectTimeZones))]
        public void Should_ArgumentException_When_DateTimeOffset_incorrect((int timeZone, bool isClient) tuple)
        {
            // Arrange
            // Act

            // Assert
            Assert.Throws<ArgumentException>(() => DateTimeUtils.GetDateTimeByTimeZone(tuple.timeZone, tuple.isClient));
        }

        [TestCaseSource(nameof(DateTimes))]
        public void Should_GetTimeZone_When_SetLocalTime(DateTime dateTime)
        {
            // Arrange
            var localDateTime = TimeZoneInfo.ConvertTimeToUtc(dateTime.ToUniversalTime());
            var serverDateTime = TimeZoneInfo.ConvertTimeToUtc(DateTime.UtcNow);

            var localDateTimeWithoutSeconds = new DateTime(localDateTime.Year, localDateTime.Month, localDateTime.Day, localDateTime.Hour, localDateTime.Minute, localDateTime.Second);
            var serverDateTimeWithoutSeconds = new DateTime(serverDateTime.Year, serverDateTime.Month, serverDateTime.Day, serverDateTime.Hour, serverDateTime.Minute, serverDateTime.Second);
            
            var neededTime = (localDateTimeWithoutSeconds - serverDateTimeWithoutSeconds).TotalSeconds;
            
            // Act
            var result = DateTimeUtils.GetTimeZoneByLocalTime(dateTime);

            // Assert
            Assert.AreEqual(neededTime, result);
        }

        [TestCaseSource(nameof(IncorrectDateTimes))]
        public void Should_GetTimeZone_When_SetIncorrectLocalTime(DateTime dateTime)
        {
            // Arrange
            // Act
            
            // Assert
            Assert.Throws<ArgumentException>(()=> DateTimeUtils.GetTimeZoneByLocalTime(dateTime));
        }

        [TestCaseSource(nameof(OutOfRangeCorrectTimeZone))]
        public void Should_ReturnArgumentException_When_TimeZoneIsIncorrect(double timeZone)
        {
            // Arrange
            // Act

            // Assert
            Assert.Throws<ArgumentException>(() => DateTimeUtils.ValidateTimeZone(timeZone));
        }
    }
}
