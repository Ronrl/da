﻿using DeskAlerts.Server.Utils;
using NUnit.Framework;

namespace DeskAlerts.UnitTestProject.Utils
{
    [TestFixture]
    public class DeskAlertsUtilsTest
    {
        [TestCase("video body %video%={\"link\":\"http://localhost/admin/images/upload_video/DeskAlerts360p.mp4\",\"width\":\"100%\",\"height\":\"100%\",\"mute\":false,\"controls\":true,\"loop\":true,\"autoplay\":true}<!-- html_title = 'video title' -->", "<!-- html_title = 'video title' -->")]
        [TestCase("video body %video%={\"link\":\"http://localhost/admin/images/upload_video/DeskAlerts360p.mp4\",\"width\":\"100%\",\"height\":\"100%\",\"mute\":false,\"controls\":true,\"loop\":true,\"autoplay\":true}<!--html_title='video title'-->", "<!--html_title='video title'-->")]
        [TestCase("video body %video%={\"link\":\"http://localhost/admin/images/upload_video/DeskAlerts360p.mp4\",\"width\":\"100%\",\"height\":\"100%\",\"mute\":false,\"controls\":true,\"loop\":true,\"autoplay\":true}<!--html_title=''-->", "<!--html_title=''-->")]
        [TestCase("video body %video%={\"link\":\"http://localhost/admin/images/upload_video/DeskAlerts360p.mp4\",\"width\":\"100%\",\"height\":\"100%\",\"mute\":false,\"controls\":true,\"loop\":true,\"autoplay\":true}<!-- html_title = '' -->", "<!-- html_title = '' -->")]
        [TestCase("video body %video%={\"link\":\"http://localhost/admin/images/upload_video/DeskAlerts360p.mp4\",\"width\":\"100%\",\"height\":\"100%\",\"mute\":false,\"controls\":true,\"loop\":true,\"autoplay\":true}<!-- html_title = '@video title@' -->", "<!-- html_title = '@video title@' -->")]
        [TestCase("video body %video%={\"link\":\"http://localhost/admin/images/upload_video/DeskAlerts360p.mp4\",\"width\":\"100%\",\"height\":\"100%\",\"mute\":false,\"controls\":true,\"loop\":true,\"autoplay\":true}<!-- html_title = '\"'video title'\"' -->", "<!-- html_title = '\"'video title'\"' -->")]
        [TestCase("video body %video%={\"link\":\"http://localhost/admin/images/upload_video/DeskAlerts360p.mp4\",\"width\":\"100%\",\"height\":\"100%\",\"mute\":false,\"controls\":true,\"loop\":true,\"autoplay\":true}<!-- html_title = '\video title\' -->", "<!-- html_title = '\video title\' -->")]
        [TestCase("video body <!-- html_title = 'video title' -->%video%={\"link\":\"http://localhost/admin/images/upload_video/DeskAlerts360p.mp4\",\"width\":\"100%\",\"height\":\"100%\",\"mute\":false,\"controls\":true,\"loop\":true,\"autoplay\":true}", "<!-- html_title = 'video title' -->")]
        [TestCase("<!-- html_title = 'video title' -->video body %video%={\"link\":\"http://localhost/admin/images/upload_video/DeskAlerts360p.mp4\",\"width\":\"100%\",\"height\":\"100%\",\"mute\":false,\"controls\":true,\"loop\":true,\"autoplay\":true}", "<!-- html_title = 'video title' -->")]
        public void Should_GetHtmlTitleComment_When_AlertTextStringContainComment(string incomingLine, string result)
        {
            //Arrange

            //Act
            var tempResult = DeskAlertsUtils.ParseHtmlTitleComment(incomingLine);

            //Assert
            Assert.AreEqual(tempResult, result);
        }
    }
}
