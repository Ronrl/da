﻿using DeskAlertsDotNet.Utils;
using NUnit.Framework;

namespace DeskAlerts.UnitTestProject.Utils
{
    [TestFixture]
    public class Aes256Test
    {
        private readonly AES256 aes256 = new AES256();
        private string text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
        private string password = "^Za4L4E`;kL6%E&W";

        [Test]
        public void HandleTickerSpeed()
        {
            var encryptedText = this.aes256.EncryptText(this.text, this.password);
            var result = this.aes256.DecryptText(encryptedText, this.password);
            Assert.AreEqual(this.text, result);
        }
    }
}
