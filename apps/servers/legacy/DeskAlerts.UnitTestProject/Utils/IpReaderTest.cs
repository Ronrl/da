﻿using DeskAlerts.Server.Utils.Helpers;
using NUnit.Framework;
using System.Collections.Generic;

namespace DeskAlerts.UnitTestProject.Utils
{
    [TestFixture]
    public class IpReaderTest
    {
        private const string IpAddressString = "192.168.200.50";
        private const string IpAddressesString = "192.168.56.1,192.168.63.2,192.168.219.1,192.168.0.36,192.168.109.1";
        private const string IpAddressesStringWithText = "192.168.56.1,192.168.63.2,192.168.219.1,192.168.0.36, ip, 192.168.109.1";

        [Test]
        public void Should_ConvertToInt64IpAddress_When_StringIpAddress()
        {
            const long correctResult = (long)3232286770;

            var convertedIpAddress = IpReader.ReadIpAddress(IpAddressString);

            Assert.AreEqual(convertedIpAddress, correctResult);
        }

        [Test]
        public void Should_NotConvertToInt64IpAddress_When_StringIpAddress()
        {
            const long correctResult = (long)3232286769;

            var convertedIpAddress = IpReader.ReadIpAddress(IpAddressString);

            Assert.AreNotEqual(convertedIpAddress, correctResult);
        }

        [Test]
        public void Should_ConvertToInt64Address_When_ListIpAddress()
        {
            var correctResult = new List<long>
            {
                3232249857,
                3232251650,
                3232291585,
                3232235556,
                3232263425
            };

            var convertedIpAddresses = IpReader.GetIpAddressListAsInt64(IpAddressesString);

            Assert.AreEqual(convertedIpAddresses, correctResult);
        }

        [Test]
        public void Should_NotConvertToInt64Address_When_ListIpAddress()
        {
            var correctResult = new List<long>
            {
                3232249857,
                3232251650,
                3232291585,
                3232235556,
                3232263422
            };

            var convertedIpAddresses = IpReader.GetIpAddressListAsInt64(IpAddressesString);

            Assert.AreNotEqual(convertedIpAddresses, correctResult);
        }

        [Test]
        public void Should_ConvertToInt64Address_When_ListIpAddressContainsString()
        {
            var correctResult = new List<long>
            {
                3232249857,
                3232251650,
                3232291585,
                3232235556,
                3232263425
            };

            var convertedIpAddresses = IpReader.GetIpAddressListAsInt64(IpAddressesStringWithText);

            Assert.AreEqual(convertedIpAddresses, correctResult);
        }

        [Test]
        public void Should_NotConvertToInt64Address_When_ListIpAddressContainsString()
        {
            var correctResult = new List<long>
            {
                3232249857,
                3232251650,
                3232291585,
                3232235556,
                3232263422
            };

            var convertedIpAddresses = IpReader.GetIpAddressListAsInt64(IpAddressesStringWithText);

            Assert.AreNotEqual(convertedIpAddresses, correctResult);
        }

        // TODO: Write a test to check HttpRequest.
    }
}
