﻿using DeskAlerts.Server.Utils;
using NUnit.Framework;

namespace DeskAlerts.UnitTestProject.Utils
{
    [TestFixture]
    public class UtilsUnitTest
    {
        private const string HtmlReferenceWithoutTargetBlank = "<html><head><meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\"/></head><body style=\"margin: 5px; overflow: auto;\"><p><a href=\"https://www.google.ru\">Google</a></p><!-- html_title = 'test 1' --><!-- html_title = 'test 1' --></body></html>";
        private const string HtmlReference = "<html><head><meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" /></head><body style=\"margin: 5px; overflow: auto;\"><p><a href=\"https://www.google.ru\" target=\"_blank\">Google</a></p><!-- html_title = 'test 1' --><!-- html_title = 'test 1' --></body></html>";

        [Test]
        public void AddTargetBlankToHtml()
        {
            string result = DeskAlertsUtils.AddAttributeToTag(HtmlReferenceWithoutTargetBlank, "a", "target", "_blank");
            Assert.AreEqual(HtmlReference, result);
        }

        [Test]
        public void DontAddTargetBlankToHtml()
        {
            string result = DeskAlertsUtils.AddAttributeToTag(HtmlReference, "a", "target", "_blank");
            Assert.AreEqual(HtmlReference, result);
        }
    }
}
