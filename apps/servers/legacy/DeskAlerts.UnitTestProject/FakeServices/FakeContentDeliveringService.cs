﻿using System;
using System.Collections.Generic;
using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.ContentDeliveringService.Interfaces;
using DeskAlerts.ContentDeliveringService.Models;

namespace DeskAlerts.UnitTestProject.FakeServices
{
    public class FakeContentDeliveringService<T> : IContentDeliveringService<T> where T : IContentType
    {
        public bool DeliveryToBroadcast()
        {
            throw new NotImplementedException();
        }

        public bool DeliveryToRecipientsOfContent(long contentId)
        {
            throw new NotImplementedException();
        }

        public bool DeliveryToRecipientsOfContent(long contentId, int timeZone)
        {
            return true;
        }

        public bool DeliveryToUser(long userId)
        {
            throw new NotImplementedException();
        }

        public bool DeliveryToUsers(IEnumerable<long> usersIds)
        {
            throw new NotImplementedException();
        }

        public string Get(ContentDeliveringKey key, string ip, string computerName)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<int> GetAllTimeZones()
        {
            return new[] {1, 2};
        }
    }
}
