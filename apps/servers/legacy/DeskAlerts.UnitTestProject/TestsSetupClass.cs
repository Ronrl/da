﻿namespace DeskAlerts.UnitTestProject
{
    using Castle.Windsor;
    using DeskAlerts.Service.AD;
    using NUnit.Framework;

    [SetUpFixture]
    public class TestsSetupClass
    {
        public WindsorContainer Container { get; set; }

        [OneTimeSetUp]
        public void GlobalSetup()
        {
            AutoMapperConfiguration.Configure();
            Container = new WindsorContainer();
            //Container.Install(new ServicesInstaller());
            //Container.Register(Component.For<IWindowsService>().ImplementedBy<ADIntegrationWindowsService>());
        }

        [OneTimeTearDown]
        public void GlobalTeardown()
        {
        }
    }
}
