﻿using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DeskAlerts.UnitTestProject.FakeRepositories
{
    class FakeSettingsRepository : ISettingsRepository
    {
        private List<Setting> _settings;


        public FakeSettingsRepository()
        {
            _settings = new List<Setting>();

        }

        public Setting GetByName(string paramName)
        {
            return _settings.First();
        }

        public string GetStringByInt(int id)
        {
            if (id <= _settings.Count)
            {
                return _settings[id].value;
            }
            else
            {
                return "";
            }
        }

        public void Delete(Setting setting)
        {
            throw new NotImplementedException();
        }

        public void Update(Setting setting)
        {
            throw new NotImplementedException();
        }
        public long Create(Setting setting)
        {
           _settings.Add(setting);
            return _settings.Count;
        }
        public List<Setting> GetAll()
        {
            throw new NotImplementedException();
        }
        public Setting GetById(long id)
        {
            throw new NotImplementedException();
        }

        public bool EntityExistsByAttribute(string attributeName, string attributeValue)
        {
            throw new NotImplementedException();
        }

    }
}
