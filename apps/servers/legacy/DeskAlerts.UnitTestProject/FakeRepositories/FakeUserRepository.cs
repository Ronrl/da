using System;
using System.Collections.Generic;
using System.Linq;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Interfaces;

namespace DeskAlerts.UnitTestProject.FakeRepositories
{
    public class FakeUserRepository : IUserRepository
    {
        private List<User> _users;
        private IDomainRepository _domainRepository;

        public FakeUserRepository(IDomainRepository domainRepository)
        {
            _users = new List<User>();
            _domainRepository = domainRepository;
        }

        public long Create(User entity)
        {
            _users.Add(entity);
            return _users.Count;
        }

        public void Delete(User entity)
        {
            throw new NotImplementedException();
        }

        public void Update(User entity)
        {
            throw new NotImplementedException();
        }

        public List<User> GetAll()
        {
            return _users;
        }

        public User GetById(long entityId)
        {
            throw new NotImplementedException();
        }

        public bool EntityExistsByAttribute(string attributeName, string attributeValue)
        {
            throw new NotImplementedException();
        }
                
        public void AddUserToGroup(User child, Group parent)
        {
            throw new NotImplementedException();
        }

        public List<User> GetChildUsersByDomainName(string domainName)
        {
            throw new NotImplementedException();
        }

        public List<User> GetChildUsersByGroupName(string groupName)
        {
            throw new NotImplementedException();
        }

        public User GetUserByDomainNameAndUserName(string domainName, string userName)
        {
            if (userName == null)
            {
                throw new ArgumentNullException(nameof(userName));
            }

            var domain = _domainRepository.GetByName(domainName);
            var user = _users.Single(x => x.Username == userName && x.DomainId == domain.Id);
            return user;
        }

        public User GetUserByUserNameAndMd5Passowrd(string userName, string md5Password)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<string> GetDomainUsersUsernames(int domainId)
        {
            throw new NotImplementedException();
        }

        public string GetUserDomainNameByUserId(long userId)
        {
            // TODO: RR-1600 implement!
            throw new NotImplementedException();
        }

        public IEnumerable<User> GetPublishersByPolicyId(long policyId)
        {
            throw new NotImplementedException();
        }
        
        public void RemoveUserFromGroup(User child, Group parent)
        {
            throw new NotImplementedException();
        }

        public int GetActivePublishersAmount()
        {
            throw new NotImplementedException();
        }

        public T GetPublisherParameterByName<T>(string parameter, string name)
        {
            throw new NotImplementedException();
        }

        public void DisableLastOnlinePublishers(int amount, IEnumerable<string> allowedStatuses)
        {
            throw new NotImplementedException();
        }

        public void DisableLastOnlinePublishers(int amount)
        {
            throw new NotImplementedException();
        }

        public void EnablePublisher(string name)
        {
            throw new NotImplementedException();
        }

        public void EnablePublishers(IEnumerable<string> publishersIds)
        {
            throw new NotImplementedException();
        }

        public int GetMaxPublishersAmount()
        {
            throw new NotImplementedException();
        }

        public void DisablePublishers(IEnumerable<string> publishersIds)
        {
            throw new NotImplementedException();
        }

        public User GetPublisher(string userName)
        {
            if (userName == null)
            {
                throw new ArgumentNullException(nameof(userName));
            }

            return _users.SingleOrDefault(x =>
                (x.Role == "E" || x.Role == "A") && x.Username.ToLower() == userName.ToLower());
        }
    }
}
