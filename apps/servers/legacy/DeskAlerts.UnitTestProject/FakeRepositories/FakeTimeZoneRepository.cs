using System.Collections.Generic;
using DeskAlerts.ApplicationCore.Interfaces;

namespace DeskAlerts.UnitTestProject.FakeRepositories
{
    public class FakeTimeZoneRepository : ITimeZoneRepository
    {
        private readonly List<int> _storage;

        public FakeTimeZoneRepository()
        {
            _storage = new List<int>();
        }

        public void Create(int timeZone)
        {
            if (_storage.Contains(timeZone))
            {
                return;
            }

            _storage.Add(timeZone);
        }

        public IEnumerable<int> GetAllTimeZones()
        {
            return _storage;
        }
    }
}
