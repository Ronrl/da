﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Interfaces;

namespace DeskAlerts.UnitTestProject.FakeRepositories
{
    public class FakeSkinRepository : ISkinRepository
    {
        public List<Skin> SkinList;

        public FakeSkinRepository()
        {
            SkinList = new List<Skin>();
        }

        Guid IRepository<Skin, Guid>.Create(Skin entity)
        {
            SkinList.Add(entity);
            return entity.Id;
        }

        void ISkinRepository.CreateDependencies(long policyId)
        {
            throw new NotImplementedException();
        }

        void ISkinRepository.CreateSpecificDependencies(long policyId, IEnumerable<Guid> skinsIds)
        {
            throw new NotImplementedException();
        }

        void IRepository<Skin, Guid>.Delete(Skin entity)
        {
            throw new NotImplementedException();
        }

        void ISkinRepository.DeleteByPolicyId(long policyId)
        {
            throw new NotImplementedException();
        }

        bool IRepository<Skin, Guid>.EntityExistsByAttribute(string attributeName, string attributeValue)
        {
            throw new NotImplementedException();
        }

        List<Skin> IRepository<Skin, Guid>.GetAll()
        {
            throw new NotImplementedException();
        }

        Skin IRepository<Skin, Guid>.GetById(Guid id)
        {
            throw new NotImplementedException();
        }

        Guid ISkinRepository.GetSkinIdByAlertId(long alertId)
        {
            throw new NotImplementedException();
        }

        IEnumerable<Skin> ISkinRepository.GetSkinsByGuidList(IEnumerable<Guid> skinGuidList)
        {
            return SkinList;
        }

        IEnumerable<Skin> ISkinRepository.GetSkinsByPolicyId(long policyId)
        {
            return SkinList;
        }

        void IRepository<Skin, Guid>.Update(Skin entity)
        {
            throw new NotImplementedException();
        }
    }
}
