﻿using System;
using System.Collections.Generic;
using System.Linq;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Interfaces;

namespace DeskAlerts.UnitTestProject.FakeRepositories
{
    public class FakeGroupRepository : IGroupRepository
    {
        private readonly List<Group> _groups;
        public IUserGroupRepository UserGroupRepository { get; set; }
        public IUserRepository UserRepository { get; set; }
        public IPolicyRepository PolicyRepository { get; set; }

        public FakeGroupRepository()
        {
            _groups = new List<Group>();
        }

        public long Create(Group entity)
        {
            _groups.Add(entity);
            return _groups.Count;
        }

        public void Delete(User entity)
        {
            throw new NotImplementedException();
        }

        public void Update(User entity)
        {
            throw new NotImplementedException();
        }

        public List<Group> GetAll()
        {
            return _groups;
        }

        public Group GetById(long entityId)
        {
            throw new NotImplementedException();
        }

        public bool EntityExistsByAttribute(string attributeName, string attributeValue)
        {
            throw new NotImplementedException();
        }

        public void AddGroupToGroup(Group child, Group parent)
        {
            throw new NotImplementedException();
        }

        public List<Group> GetChildGroupsByDomainName(string domainName)
        {
            throw new NotImplementedException();
        }

        public List<Group> GetChildGroupsByGroupName(string groupName)
        {
            throw new NotImplementedException();
        }

        public List<Group> GetChildGroupsByGroupId(int groupId)
        {
            throw new NotImplementedException();
        }

        public void RemoveGroupFromGroup(Group child, Group parent)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Group> GetGroupByDomainIdMappedWithUser(long domainId, string userName)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Group> GetGroupByDomainIdAndUserIdMappedWithUser(long domainId, long userId)
        {
            var userGroups =
                from g in _groups
                join ug in UserGroupRepository.GetAll() on g.Id equals ug.GroupId
                select new { Group = g, UserGroup = ug };

            var users =
                from u in UserRepository.GetAll()
                join ug in userGroups on u.Id equals ug.UserGroup.UserId
                select new { Group = ug.Group, UserGroup = ug.UserGroup, User = u };

            var policies =
                from p in PolicyRepository.GetAll()
                join g in _groups on p.Id equals g.PolicyId
                select new {Policy = p, Group = g};

            var groups =
                from u in users
                join p in policies on u.Group.PolicyId equals p.Policy.Id
                select new Group {Id = u.Group.Id, DomainId = u.User.DomainId, Name = u.Group.Name, PolicyId = p.Policy.Id, Policy = p.Policy};

            return groups.ToList();
        }

        public IEnumerable<Group> GetGroupsPublisherId(long userId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Group> GetGroupsWithDomain()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Group> GetGroupsWithAdminPolicy()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<long> GetUserIdsByGroupNames(IEnumerable<string> groupNames)
        {
            // TODO: RR-938 add fake method repository to tests.
            throw new NotImplementedException();
        }

        public void Delete(Group entity)
        {
            throw new NotImplementedException();
        }

        public void Update(Group entity)
        {
            throw new NotImplementedException();
        }
    }
}
