﻿using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;

namespace DeskAlerts.UnitTestProject.FakeRepositories
{
    public class FakeUserGroupRepository : IUserGroupRepository
    {
        private List<UserGroup> _userGroups;

        public FakeUserGroupRepository()
        {
            _userGroups = new List<UserGroup>();
        }

        public long Create(UserGroup entity)
        {
            _userGroups.Add(entity);
            return _userGroups.Count;
        }

        public void Delete(UserGroup entity)
        {
            throw new NotImplementedException();
        }

        public void Update(UserGroup entity)
        {
            throw new NotImplementedException();
        }

        public List<UserGroup> GetAll()
        {
            return _userGroups;
        }

        public UserGroup GetById(long id)
        {
            throw new NotImplementedException();
        }

        public bool EntityExistsByAttribute(string attributeName, string attributeValue)
        {
            throw new NotImplementedException();
        }
    }
}
