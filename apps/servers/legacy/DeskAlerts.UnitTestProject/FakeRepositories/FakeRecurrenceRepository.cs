﻿using System;
using System.Collections.Generic;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Interfaces;

namespace DeskAlerts.UnitTestProject.FakeRepositories
{
    public class FakeRecurrenceRepository : IRecurrenceRepository
    {
        private readonly Dictionary<long, IEnumerable<long>> _storage;

        public FakeRecurrenceRepository()
        {
            _storage = new Dictionary<long, IEnumerable<long>>();
        }

        public long Create(Recurrence entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(Recurrence entity)
        {
            throw new NotImplementedException();
        }

        public bool EntityExistsByAttribute(string attributeName, string attributeValue)
        {
            throw new NotImplementedException();
        }

        public List<Recurrence> GetAll()
        {
            throw new NotImplementedException();
        }

        public Recurrence GetById(long id)
        {
            throw new NotImplementedException();
        }

        public bool IsActualDayRecurrenceForContent(DateTime dateTime, long contentId)
        {
            throw new NotImplementedException();
        }

        public void Update(Recurrence entity)
        {
            throw new NotImplementedException();
        }
    }
}
