﻿using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DeskAlerts.UnitTestProject.FakeRepositories
{
    public class FakeVideoAlertRepository : IVideoAlertRepository
    {
        private readonly Dictionary<long, IEnumerable<long>> _storage;

        public FakeVideoAlertRepository()
        {
            _storage = new Dictionary<long, IEnumerable<long>>();
        }

        public long Create(AlertEntity entity)
        {
            throw new NotImplementedException();
        }

        public void Create(long userId, IEnumerable<long> contentIds)
        {
            _storage.Add(userId, contentIds);
        }

        public void Delete(AlertEntity entity)
        {
            throw new NotImplementedException();
        }

        public void Update(AlertEntity entity)
        {
            throw new NotImplementedException();
        }

        public List<AlertEntity> GetAll()
        {
            throw new NotImplementedException();
        }

        public AlertEntity GetById(long id)
        {
            throw new NotImplementedException();
        }

        public bool EntityExistsByAttribute(string attributeName, string attributeValue)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<long> GetActualVideoAlertsIds(long userId, Guid deskBarId, DateTime dateTime)
        {
            var isContained = _storage.TryGetValue(userId, out var value);

            return isContained
                ? value
                : Enumerable.Empty<long>();
        }

        public IEnumerable<long> GetRecipientsUsersIds(long tickerId)
        {
            return _storage.Select(value => value.Key);
        }

        public IEnumerable<long> GetActualVideoAlertsIds(long userId, DateTime dateTime)
        {
            throw new NotImplementedException();
        }
    }
}
