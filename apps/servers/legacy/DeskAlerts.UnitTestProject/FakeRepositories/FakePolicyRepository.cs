﻿using System;
using System.Collections.Generic;
using System.Linq;
using DeskAlerts.ApplicationCore.Dtos;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Interfaces;

namespace DeskAlerts.UnitTestProject.FakeRepositories
{
    public class FakePolicyRepository : IPolicyRepository
    {
        private readonly List<Policy> _policies;
        public IUserGroupRepository UserGroupRepository { get; set; }
        public IGroupRepository GroupRepository { get; set; }

        public FakePolicyRepository()
        {
            _policies = new List<Policy>();
        }

        public long Create(Policy entity)
        {
            _policies.Add(entity);
            return _policies.Count;
        }

        public void Delete(Policy entity)
        {
            throw new NotImplementedException();
        }

        public void Update(Policy entity)
        {
            throw new NotImplementedException();
        }

        public List<Policy> GetAll()
        {
            return _policies;
        }

        public Policy GetById(long entityId)
        {
            throw new NotImplementedException();
        }

        public bool EntityExistsByAttribute(string attributeName, string attributeValue)
        {
            throw new NotImplementedException();
        }

        public List<Policy> GetPoliciesByUserIdAndDomainId(long userId, long domainId)
        {
            var ug = UserGroupRepository.GetAll().Where(x => x.UserId == userId);

            var policiesGroups =
                from g in GroupRepository.GetAll().Where(x => x.DomainId == domainId)
                join urg in ug on g.Id equals urg.GroupId
                select g;

            var policies =
                from p in _policies
                join pg in policiesGroups on p.Id equals pg.PolicyId
                select p;

            return policies.ToList();
        }

        public List<PolicyUser> GetPolicyUsersByPublisherId(long publisherId)
        {
            throw new NotImplementedException();
        }

        public List<PolicyUser> GetPolicyUsersByPolicyId(long policyId)
        {
            throw new NotImplementedException();
        }

        public List<PolicyOU> GetPolicyOUsByPublisherId(long publisherId)
        {
            throw new NotImplementedException();
        }

        public List<PolicyOU> GetPolicyOUsByPolicyId(long policyId)
        {
            throw new NotImplementedException();
        }

        public List<PolicyGroup> GetPolicyGroupsByPublisherId(long publisherId)
        {
            throw new NotImplementedException();
        }

        public List<PolicyGroup> GetPolicyGroupsByPolicyId(long policyId)
        {
            throw new NotImplementedException();
        }

        public List<Policy> GetPoliciesByPublisherId(long publisherId)
        {
            var policies =
                from p in _policies
                join ug in UserGroupRepository.GetAll() on p.PolicyEditor.PublisherId equals ug.UserId
                where ug.UserId == publisherId
                select p;

            return policies.ToList();
        }

        public List<PolicyInfoDto> GetPolicyGroups()
        {
            throw new NotImplementedException();
        }

        public int GetRecipientsTotal(long policyId)
        {
            throw new NotImplementedException();
        }

        public string GetPolicyTypeByPublisherId(long publisherId)
        {
            var policy = _policies.Single(x => x.PolicyEditor.PublisherId == publisherId);
            return policy.Type;
        }

        public IEnumerable<char> GetPoliciesTypes(long publisherId)
        {
            var policy = _policies.Single(x => x.PolicyEditor.PublisherId == publisherId);
            return policy.Type;
        }
    }
}
