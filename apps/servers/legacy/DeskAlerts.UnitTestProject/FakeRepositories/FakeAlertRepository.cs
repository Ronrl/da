﻿using System;
using System.Collections.Generic;
using System.Linq;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.ApplicationCore.Models;

namespace DeskAlerts.UnitTestProject.FakeRepositories
{
    public class FakeAlertRepository : IAlertRepository
    {
        //TODO: Transfer storage to common base
        private readonly List<AlertEntity> _alerts;
        private readonly Dictionary<long, IEnumerable<long>> _storage;

        public FakeAlertRepository()
        {
            _alerts = new List<AlertEntity>();
            _storage = new Dictionary<long, IEnumerable<long>>();
        }

        public long Create(AlertEntity entity)
        {
            _alerts.Add(entity);

            return entity.Id;
        }

        public void Create(long userId, IEnumerable<long> screensaversIds)
        {
            _storage.Add(userId, screensaversIds);
        }

        public void Delete(AlertEntity entity)
        {
            throw new NotImplementedException();
        }

        public List<AlertEntity> GetAlertsByCampaignIdMappedWithUser(long campaignId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<AlertEntity> GetAlertsMappedWithRecurrence()
        {
            throw new NotImplementedException();
        }

        public List<AlertEntity> GetAll()
        {
            throw new NotImplementedException();
        }

        public List<AlertEntity> GetByCampaignId(long campaignId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<AlertEntity> GetActualAlerts(Token userToken)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<AlertEntity> GetByCampaignIdMappedWithRecurrence(long campaignId)
        {
            throw new NotImplementedException();
        }

        public AlertEntity GetById(long id)
        {
            return _alerts.First(a => a.Id == id);
        }

        public void Update(AlertEntity entity)
        {
            throw new NotImplementedException();
        }

        public bool EntityExistsByAttribute(string attributeName, string attributeValue)
        {
            throw new NotImplementedException();
        }

        public long GetAlertsCount()
        {
            throw new NotImplementedException();
        }

        public long GetLastAlertId()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<long> GetActualAlertIds(long userId, Guid deskBarId, DateTime dateTime, string ip, string computerName)
        {
            var isContained = _storage.TryGetValue(userId, out var value);

            return isContained
                ? value
                : Enumerable.Empty<long>();
        }

        public IEnumerable<long> GetRecipientsUsersIds(long alertId)
        {
            return _storage.Select(value => value.Key);
        }

        public IEnumerable<long> GetRecipientsCompsIds(long alertId)
        {
            throw new NotImplementedException();
        }

        public AlertType GetContentTypeByContentId(long contentId)
        {
            var result = (AlertType)_alerts
                .Where(x => x.Id == contentId)
                .Select(x => x.Class)
                .FirstOrDefault();

            return result;
        }

        public bool IsActualContentByRepeatableCriteria(DateTime dateTime, long contentId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<AlertEntity> GetScheduledActiveAlerts()
        {
            //TODO: implement tests for method
            throw new NotImplementedException();
        }

        public IEnumerable<AlertEntity> GetApprovedActiveAlerts()
        {
            //todo: implement tests for method
            throw new NotImplementedException();
        }

        public IEnumerable<long> GetActualAlertIds(long userId, DateTime dateTime)
        {
            throw new NotImplementedException();
        }
    }
}
