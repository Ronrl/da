﻿using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;

namespace DeskAlerts.UnitTestProject.FakeRepositories
{
    public class FakePolicyPublisherRepository : IPolicyPublisherRepository
    {
        private List<PolicyPublisher> _policyPublishers;

        public FakePolicyPublisherRepository()
        {
            _policyPublishers = new List<PolicyPublisher>();
        }

        public long Create(PolicyPublisher entity)
        {
            _policyPublishers.Add(entity);
            return _policyPublishers.Count;
        }

        public void Delete(PolicyPublisher entity)
        {
            throw new NotImplementedException();
        }

        public void Update(PolicyPublisher entity)
        {
            throw new NotImplementedException();
        }

        public List<PolicyPublisher> GetAll()
        {
            return _policyPublishers;
        }

        public PolicyPublisher GetById(long id)
        {
            throw new NotImplementedException();
        }

        public bool EntityExistsByAttribute(string attributeName, string attributeValue)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<PolicyPublisher> GetByPublisherId(long publisherId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<PolicyPublisher> GetByGroupId(long groupId)
        {
            throw new NotImplementedException();
        }

        public PolicyPublisher GetByPolicyIdPublisherIdGroupId(long policyId, long publisherId, long groupId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<PolicyPublisher> GetByPolicyIdPublisherId(long policyId, long publisherId)
        {
            throw new NotImplementedException();
        }
    }
}
