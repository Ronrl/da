﻿using System;
using System.Collections.Generic;
using System.Linq;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Interfaces;

namespace DeskAlerts.UnitTestProject.FakeRepositories
{
    public class FakeSurveyRepository : ISurveyRepository
    {
        private readonly Dictionary<long, IEnumerable<long>> _storage;

        public FakeSurveyRepository()
        {
            _storage = new Dictionary<long, IEnumerable<long>>();
        }

        public long Create(AlertEntity entity)
        {
            throw new NotImplementedException();
        }

        public void Create(long userId, IEnumerable<long> screensaversIds)
        {
            _storage.Add(userId, screensaversIds);
        }

        public void Delete(AlertEntity entity)
        {
            throw new NotImplementedException();
        }

        public bool EntityExistsByAttribute(string attributeName, string attributeValue)
        {
            throw new NotImplementedException();
        }

        public List<AlertEntity> GetAll()
        {
            throw new NotImplementedException();
        }

        public AlertEntity GetById(long id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<long> GetActualSurveysIds(long userId, Guid deskBarId, DateTime dateTime, string ip, string computerName)
        {
            var isContained = _storage.TryGetValue(userId, out var value);

            return isContained
                ? value
                : Enumerable.Empty<long>();
        }

        public IEnumerable<long> GetRecipientsUsersIds(long surveyId)
        {
            return _storage.Select(value => value.Key);
        }

        public IEnumerable<long> GetSurveyIdBySurveyMainTableId(IEnumerable<long> surveyMainTableIds)
        {
            throw new NotImplementedException();
        }

        public void Update(AlertEntity entity)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<long> GetActualSurveysIds(long userId, DateTime dateTime)
        {
            throw new NotImplementedException();
        }
    }
}
