﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Interfaces;

namespace DeskAlerts.UnitTestProject.FakeRepositories
{
    public class FakeContentSettingsRepository : IContentSettingsRepository
    {
        public List<ContentSettings> ContentSettingsList;

        public FakeContentSettingsRepository()
        {
            ContentSettingsList = new List<ContentSettings>();
        }

        long IRepository<ContentSettings, long>.Create(ContentSettings entity)
        {
            throw new NotImplementedException();
        }

        void IContentSettingsRepository.CreateDependencies(long policyId)
        {
            throw new NotImplementedException();
        }

        void IContentSettingsRepository.CreateSpecificDependencies(long policyId, IEnumerable<long> contentOptionIdsList)
        {
            throw new NotImplementedException();
        }

        void IRepository<ContentSettings, long>.Delete(ContentSettings entity)
        {
            throw new NotImplementedException();
        }

        void IContentSettingsRepository.DeleteByPolicyId(long policyId)
        {
            throw new NotImplementedException();
        }

        bool IRepository<ContentSettings, long>.EntityExistsByAttribute(string attributeName, string attributeValue)
        {
            throw new NotImplementedException();
        }

        List<ContentSettings> IRepository<ContentSettings, long>.GetAll()
        {
            throw new NotImplementedException();
        }

        ContentSettings IRepository<ContentSettings, long>.GetById(long id)
        {
            return ContentSettingsList.FirstOrDefault();
        }

        IEnumerable<ContentSettings> IContentSettingsRepository.GetContentSettingsByPolicyId(long policyId)
        {
            return ContentSettingsList;
        }

        void IRepository<ContentSettings, long>.Update(ContentSettings entity)
        {
            throw new NotImplementedException();
        }
    }
}
