﻿using System;
using System.Collections.Generic;
using System.Linq;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Interfaces;

namespace DeskAlerts.UnitTestProject.FakeRepositories
{
    public class FakeWallpaperRepository : IWallPaperRepository
    {
        private readonly Dictionary<long, IEnumerable<long>> _storage;

        public FakeWallpaperRepository()
        {
            _storage = new Dictionary<long, IEnumerable<long>>();
        }

        public long Create(AlertEntity entity)
        {
            throw new NotImplementedException();
        }

        public void Create(long userId, IEnumerable<long> wallpapersIds)
        {
            _storage.Add(userId, wallpapersIds);
        }

        public void Delete(AlertEntity entity)
        {
            throw new NotImplementedException();
        }

        public bool EntityExistsByAttribute(string attributeName, string attributeValue)
        {
            throw new NotImplementedException();
        }

        public void Update(AlertEntity entity)
        {
            throw new NotImplementedException();
        }

        public List<AlertEntity> GetAll()
        {
            throw new NotImplementedException();
        }

        public AlertEntity GetById(long id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<long> GetActualWallPapersIds(long userId, Guid deskBarId, DateTime dateTime, string ip, string computerName)
        {
            var isContained = _storage.TryGetValue(userId, out var value);

            return isContained
                ? value
                : Enumerable.Empty<long>();
        }

        public IEnumerable<long> GetRecipientsUsersIds(long wallpaperId)
        {
            return _storage.Select(value => value.Key);
        }

        public IEnumerable<long> GetRecipientsCompsIds(long wallpaperId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<AlertEntity> GetActualSchedulingWallpapers()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<long> GetActualWallPapersIds(long userId, DateTime dateTime)
        {
            throw new NotImplementedException();
        }
    }
}
