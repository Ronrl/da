﻿using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Exceptions;
using DeskAlerts.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DeskAlerts.UnitTestProject.FakeRepositories
{
    public class FakeDomainRepository : IDomainRepository
    {
        private List<Domain> _domains;

        public FakeDomainRepository()
        {
            _domains = new List<Domain>();
        }

        public long Create(Domain entity)
        {
            _domains.Add(entity);
            return _domains.Count;
        }

        public void Delete(Domain entity)
        {
            throw new NotImplementedException();
        }

        public List<Domain> GetAll()
        {
            return _domains;
        }

        public List<Domain> GetAllDomains()
        {
            return _domains;
        }

        public Domain GetById(long id)
        {
            throw new NotImplementedException();
        }

        public Domain GetByName(string name)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            var domain = _domains.Single(x => x.Name == name);
            if (domain == null)
            {
                throw new EntityNotFoundException($"Couldn't found domain with name {name}");
            }
            else
            {

                return domain;
            }
        }

        public void Update(Domain entity)
        {
            throw new NotImplementedException();
        }

        public bool EntityExistsByAttribute(string attributeName, string attributeValue)
        {
            throw new NotImplementedException();
        }
    }
}
