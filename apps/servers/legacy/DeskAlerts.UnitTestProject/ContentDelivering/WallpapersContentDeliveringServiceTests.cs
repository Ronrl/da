﻿using System.Collections.Generic;
using DeskAlerts.ApplicationCore.Models.ContentTypes;
using DeskAlerts.ContentDeliveringService.Implementations;
using DeskAlerts.ContentDeliveringService.Interfaces;
using DeskAlerts.UnitTestProject.FakeRepositories;
using NUnit.Framework;

namespace DeskAlerts.UnitTestProject.ContentDelivering
{
    [TestFixture]
    public class WallpapersContentDeliveringServiceTests : BaseContentDeliveringServiceTests<WallpaperContentType>
    {
        private FakeWallpaperRepository _wallpaperRepository;

        protected override IContentDeliveringService<WallpaperContentType> ContentDeliveringService { get; set; }

        [SetUp]
        public void SetUp()
        {
            BaseInitialize();

            var cache = new InMemoryCache(1000);
            _wallpaperRepository = new FakeWallpaperRepository();
            ContentDeliveringService = new WallpapersContentDeliveringService(cache, ContentRepository, _wallpaperRepository, TimeZoneRepository);

            BaseSetUp();
        }

        protected override void AddContentToDb(long userId, IEnumerable<long> contentIds)
        {
            _wallpaperRepository.Create(userId, contentIds);
        }
    }
}
