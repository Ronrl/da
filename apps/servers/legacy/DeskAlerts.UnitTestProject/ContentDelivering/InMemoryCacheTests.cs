﻿using System;
using DeskAlerts.ContentDeliveringService.Implementations;
using DeskAlerts.ContentDeliveringService.Models;
using NUnit.Framework;
using System.Threading;

namespace DeskAlerts.UnitTestProject.ContentDelivering
{
    [TestFixture]
    internal class InMemoryCacheTests
    {
        private InMemoryCache _cache;
        private const string DeskBarIdFirst = "E63A2ACC-59A1-4588-81C4-E166008B0793";
        private const string DeskBarIdSecond = "58E28073-EA5B-47F2-9D3C-4D63E4DF2637";
        private const string DeskBarIdThird = "56FD67F4-717C-4BF3-BB84-3AAC3785522D";

        [SetUp]
        public void SetUp()
        {
            _cache = new InMemoryCache(1000);
        }

        [Test, Apartment(ApartmentState.MTA)]
        [TestCase(120, 1, null, DeskBarIdFirst)]
        [TestCase(120, 1, "", DeskBarIdFirst)]
        [TestCase(600, 2, "TestValue1", DeskBarIdSecond)]
        public void Should_AddValueByKey_When_CacheNotContainedValues(int timeZone, long userId, string value, string deskBarId)
        {
            // Arrange
            var cacheKey = new ContentDeliveringKey(timeZone, userId, Guid.Parse(deskBarId));

            // Act
            var isAdded = _cache.Add(cacheKey, value);

            // Assert
            Assert.True(isAdded);
        }

        [Test, Apartment(ApartmentState.MTA)]
        [TestCase(120, 1, null, DeskBarIdFirst)]
        [TestCase(120, 1, "", DeskBarIdFirst)]
        [TestCase(600, 2, "TestValue1", DeskBarIdSecond)]
        //TODO: Fix failing when it runs in series of tests
        public void Should_NotAddValueByKey_When_ValueWithThisKeyAlreadyExisting(int timeZone, long userId, string value, string deskBarId)
        {
            // Arrange
            var cacheKey = new ContentDeliveringKey(timeZone, userId, Guid.Parse(deskBarId));

            _cache.Add(cacheKey, value);

            // Act
            var isAdded = _cache.Add(cacheKey, $"{value}New");

            // Assert
            Assert.False(isAdded);
        }

        [Test, Apartment(ApartmentState.MTA)]
        [TestCase(120, 1, DeskBarIdFirst)]
        [TestCase(600, 2, DeskBarIdSecond)]
        //TODO: Fix failing when it runs in series of tests
        public void Should_GetNullValueByKey_When_CacheNotContainedValues(int timeZone, long userId, string deskBarId)
        {
            // Arrange
            var cacheKey = new ContentDeliveringKey(timeZone, userId, Guid.Parse(deskBarId));

            // Act
            var cacheValue = _cache.Get(cacheKey);

            // Assert
            Assert.IsNull(cacheValue);
        }

        [Test, Apartment(ApartmentState.MTA)]
        [TestCase(120, 1, null, DeskBarIdFirst)]
        [TestCase(120, 1, "", DeskBarIdFirst)]
        [TestCase(600, 2, "TestValue1", DeskBarIdSecond)]
        [TestCase(-60, 3, "TestValue2", DeskBarIdThird)] 
        public void Should_GetCorrectValueByKey_When_ValueWasAddedToCache(int timeZone, long userId, string value, string deskBarId)
        {
            // Arrange
            var cacheKey = new ContentDeliveringKey(timeZone, userId, Guid.Parse(deskBarId));

            _cache.Add(cacheKey, value);

            // Act
            var cacheValue = _cache.Get(cacheKey);

            // Assert
            Assert.AreEqual(value, cacheValue);
        }

        [Test, Apartment(ApartmentState.MTA)]
        [TestCase(3, "Wall1", DeskBarIdThird)]
        [TestCase(3, "", DeskBarIdThird)]
        [TestCase(3, null, DeskBarIdThird)]
        [TestCase(0, "Wall3", DeskBarIdThird)]
        [TestCase(-3, "Wall4", DeskBarIdThird)]
        public void Should_GetNullValueByKey_When_ValueHasBeenAddedAndDeleted(long userId, string value, string deskBarId)
        {
            // Arrange
            var cacheKey = new ContentDeliveringKey(10000, userId, Guid.Parse(deskBarId));

            _cache.Add(cacheKey, value);
            _cache.Delete(userId);

            // Act
            var cacheValue = _cache.Get(cacheKey);

            // Assert
            Assert.IsNull(cacheValue);
        }

        [Test, Apartment(ApartmentState.MTA)]
        [TestCase(1, 2, "", "", DeskBarIdFirst)]
        [TestCase(1, 2, null, null, DeskBarIdFirst)]
        [TestCase(1, 2, "value1", null, DeskBarIdFirst)]
        [TestCase(1, 2, null, "value2", DeskBarIdFirst)]
        [TestCase(3, 4, "value1", "value2", DeskBarIdThird)]
        public void Should_GetNullValuesByKeys_When_CacheHadValuesAndDeletedThemAll(long userId1, long userId2, string value1, string value2, string deskBarId)
        {
            // Arrange
            var cacheKey1 = new ContentDeliveringKey(10000, userId1, Guid.Parse(deskBarId));
            var cacheKey2 = new ContentDeliveringKey(10000, userId2, Guid.Parse(deskBarId));

            _cache.Add(cacheKey1, value1);
            _cache.Add(cacheKey2, value2);
            _cache.DeleteAll();

            // Act
            var cacheValue1 = _cache.Get(cacheKey1);
            var cacheValue2 = _cache.Get(cacheKey2);
            
            // Assert
            Assert.IsNull(cacheValue1);
            Assert.IsNull(cacheValue2);
        }
    }
}