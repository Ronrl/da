﻿using System.Collections.Generic;
using DeskAlerts.ApplicationCore.Models.ContentTypes;
using DeskAlerts.ContentDeliveringService.Implementations;
using DeskAlerts.ContentDeliveringService.Interfaces;
using DeskAlerts.UnitTestProject.FakeRepositories;
using NUnit.Framework;

namespace DeskAlerts.UnitTestProject.ContentDelivering
{
    [TestFixture]
    public class ScreensaversContentDeliveringServiceTests : BaseContentDeliveringServiceTests<ScreensaverContentType>
    {
        private FakeScreensaverRepository _screensaverRepository;

        protected override IContentDeliveringService<ScreensaverContentType> ContentDeliveringService { get; set; }

        [SetUp]
        public void SetUp()
        {
            BaseInitialize();

            var cache = new InMemoryCache(1000);
            _screensaverRepository = new FakeScreensaverRepository();
            ContentDeliveringService = new ScreensaversContentDeliveringService(cache, ContentRepository, _screensaverRepository, TimeZoneRepository);

            BaseSetUp();
        }

        protected override void AddContentToDb(long userId, IEnumerable<long> contentIds)
        {
            _screensaverRepository.Create(userId, contentIds);
        }
    }
}