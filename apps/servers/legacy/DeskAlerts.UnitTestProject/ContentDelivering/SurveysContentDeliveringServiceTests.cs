﻿using System.Collections.Generic;
using DeskAlerts.ApplicationCore.Models.ContentTypes;
using DeskAlerts.ContentDeliveringService.Implementations;
using DeskAlerts.ContentDeliveringService.Interfaces;
using DeskAlerts.UnitTestProject.FakeRepositories;
using NUnit.Framework;

namespace DeskAlerts.UnitTestProject.ContentDelivering
{
    [TestFixture]
    public class SurveysContentDeliveringServiceTests : BaseContentDeliveringServiceTests<SurveyContentType>
    {
        private FakeSurveyRepository _surveyRepository;

        protected override IContentDeliveringService<SurveyContentType> ContentDeliveringService { get; set; }

        [SetUp]
        public void SetUp()
        {
            BaseInitialize();

            var cache = new InMemoryCache(1000);
            _surveyRepository = new FakeSurveyRepository();
            ContentDeliveringService = new SurveysContentDeliveringService(cache, ContentRepository, _surveyRepository, TimeZoneRepository);

            BaseSetUp();
        }

        protected override void AddContentToDb(long userId, IEnumerable<long> contentIds)
        {
            _surveyRepository.Create(userId, contentIds);
        }
    }
}