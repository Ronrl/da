﻿using System.Collections.Generic;
using DeskAlerts.ApplicationCore.Models.ContentTypes;
using DeskAlerts.ContentDeliveringService.Implementations;
using DeskAlerts.ContentDeliveringService.Interfaces;
using DeskAlerts.UnitTestProject.FakeRepositories;
using NUnit.Framework;

namespace DeskAlerts.UnitTestProject.ContentDelivering
{
    [TestFixture]
    public class TickersContentDeliveringServiceTests : BaseContentDeliveringServiceTests<TickerContentType>
    {
        private FakeTickerRepository _tickerRepository;

        protected override IContentDeliveringService<TickerContentType> ContentDeliveringService { get; set; }

        [SetUp]
        public void SetUp()
        {
            BaseInitialize();

            var cache = new InMemoryCache(1000);
            _tickerRepository = new FakeTickerRepository();
            ContentDeliveringService = new TickersContentDeliveringService(cache, ContentRepository, _tickerRepository, TimeZoneRepository);

            BaseSetUp();
        }

        protected override void AddContentToDb(long userId, IEnumerable<long> contentIds)
        {
            _tickerRepository.Create(userId, contentIds);
        }
    }
}