﻿using System.Collections.Generic;
using DeskAlerts.ApplicationCore.Models.ContentTypes;
using DeskAlerts.ContentDeliveringService.Implementations;
using DeskAlerts.ContentDeliveringService.Interfaces;
using DeskAlerts.UnitTestProject.FakeRepositories;
using NUnit.Framework;

namespace DeskAlerts.UnitTestProject.ContentDelivering
{
    [TestFixture]
    public class AlertContentDeliveringServiceTests : BaseContentDeliveringServiceTests<AlertContentType>
    {
        private FakeAlertRepository _alertRepository;

        protected override IContentDeliveringService<AlertContentType> ContentDeliveringService { get; set; }

        [SetUp]
        public void SetUp()
        {
            BaseInitialize();

            var cache = new InMemoryCache(1000);
            _alertRepository = new FakeAlertRepository();
            ContentDeliveringService = new AlertContentDeliveringService(cache, ContentRepository, _alertRepository, TimeZoneRepository);

            BaseSetUp();
        }

        protected override void AddContentToDb(long userId, IEnumerable<long> contentIds)
        {
            _alertRepository.Create(userId, contentIds);
        }
    }
}