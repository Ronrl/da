﻿using System.Collections.Generic;
using DeskAlerts.ApplicationCore.Models.ContentTypes;
using DeskAlerts.ContentDeliveringService.Implementations;
using DeskAlerts.ContentDeliveringService.Interfaces;
using DeskAlerts.UnitTestProject.FakeRepositories;
using NUnit.Framework;

namespace DeskAlerts.UnitTestProject.ContentDelivering
{
    [TestFixture]
    public class TerminatedContentDeliveringServiceTests : BaseContentDeliveringServiceTests<TerminatedContentType>
    {
        private FakeTerminatedRepository _terminatedRepository;

        protected override IContentDeliveringService<TerminatedContentType> ContentDeliveringService { get; set; }

        [SetUp]
        public void SetUp()
        {
            BaseInitialize();

            var cache = new InMemoryCache(1000);
            _terminatedRepository = new FakeTerminatedRepository();
            ContentDeliveringService = new TerminatedContentDeliveringService(cache, ContentRepository, _terminatedRepository, TimeZoneRepository);

            BaseSetUp();
        }

        protected override void AddContentToDb(long userId, IEnumerable<long> contentIds)
        {
            _terminatedRepository.Create(userId, contentIds);
        }
    }
}