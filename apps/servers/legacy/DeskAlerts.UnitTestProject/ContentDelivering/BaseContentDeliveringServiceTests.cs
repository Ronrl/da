﻿using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Enums;
using DeskAlerts.ApplicationCore.Extensions.Enums;
using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.ContentDeliveringService.Interfaces;
using DeskAlerts.ContentDeliveringService.Models;
using DeskAlerts.UnitTestProject.FakeRepositories;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DeskAlerts.UnitTestProject.ContentDelivering
{
    [TestFixture]
    public abstract class BaseContentDeliveringServiceTests<TContent> where TContent : IContentType
    {
        private const int TimeZoneFirst = 600;
        private const int TimeZoneSecond = -240;
        private const int TimeZoneThird = 120;
        // TODO: TS
        // IP string example: "ip,127.0.0.1,192.168.0.10, 192.200.200.255, 2001:0db8:11a3:09d7:1f34:8a2e:07a0:765d, ::ffff:192.0.2.1, 240.170.53.47"
        private const string DefaultIp = "127.0.0.1";
        private const string DefaultComputerName = "ComputerName";
        private const long UserIdFirst = 1;
        private const long UserIdSecond = 2;
        private const long UserIdThird = 3;
        private const string DeskBarIdFirst = "E63A2ACC-59A1-4588-81C4-E166008B0793";
        private const string DeskBarIdSecond = "58E28073-EA5B-47F2-9D3C-4D63E4DF2637";
        private const string DeskBarIdThird = "56FD67F4-717C-4BF3-BB84-3AAC3785522D";
        private const long ContentIdFirst = 11;
        private const long ContentIdSecond = 12;

        protected FakeAlertRepository ContentRepository;
        protected FakeTimeZoneRepository TimeZoneRepository;


        protected abstract IContentDeliveringService<TContent> ContentDeliveringService { get; set; }

        protected abstract void AddContentToDb(long userId, IEnumerable<long> contentIds);

        protected void BaseInitialize()
        {
            ContentRepository = new FakeAlertRepository();
            TimeZoneRepository = new FakeTimeZoneRepository();
        }

        public void BaseSetUp()
        {
            AddContentToDb(UserIdFirst, new[] {ContentIdFirst, ContentIdSecond});
            AddContentToDb(UserIdSecond, new[] { ContentIdFirst });
        }

        [Test]
        [TestCase(0)]
        [TestCase(-10)]
        public void Should_ThrowArgumentException_When_DeliveryToUserWithWrongParameters(long userid)
        {
            // Arrange
            // Act
            // Assert
            Assert.Throws<ArgumentException>(() => ContentDeliveringService.DeliveryToUser(userid));
        }

        [Test]
        public void Should_ThrowArgumentException_When_DeliveryToUsersWithWrongParameters()
        {
            // Arrange
            var users = new long[] { 1, 2, 0, -1 };

            // Act
            // Assert
            Assert.Throws<ArgumentException>(() => ContentDeliveringService.DeliveryToUsers(users));
            Assert.Throws<ArgumentException>(() => ContentDeliveringService.DeliveryToUsers(null));
        }

        [Test]
        [TestCase(0)]
        [TestCase(-10)]
        public void Should_ThrowArgumentException_When_DeliveryToRecipientsOfContentWithWrongParameters(long contentId)
        {
            // Arrange
            // Act
            // Assert
            Assert.Throws<ArgumentException>(() => ContentDeliveringService.DeliveryToRecipientsOfContent(contentId));
        }

        [Test]
        [TestCase(0, 600)]
        [TestCase(-10, -240)]
        public void Should_ThrowArgumentException_When_DeliveryToRecipientsOfContentWithWrongParameters(long contentId, int timeZone)
        {
            // Arrange
            // Act
            // Assert
            Assert.Throws<ArgumentException>(() => ContentDeliveringService.DeliveryToRecipientsOfContent(contentId, timeZone));
        }

        [Test]
        public void Should_ThrowArgumentException_When_GetWithWrongParameters()
        {
            // Arrange
            // Act
            // Assert
            Assert.Throws<ArgumentException>(() => ContentDeliveringService.Get(null, DefaultIp, DefaultComputerName));
        }

        [Test]
        public void Should_GetAllTimeZones_When_CacheContainsEntriesForManyZones()
        {
            // Arrange
            var keyForFirstUser = new ContentDeliveringKey(TimeZoneFirst, UserIdFirst, Guid.Parse(DeskBarIdFirst));
            var keyForSecondUser = new ContentDeliveringKey(TimeZoneSecond, UserIdSecond, Guid.Parse(DeskBarIdSecond)); 
            
            ContentDeliveringService.Get(keyForFirstUser, DefaultIp, DefaultComputerName);
            ContentDeliveringService.Get(keyForSecondUser, DefaultIp, DefaultComputerName);

            TimeZoneRepository.Create(TimeZoneFirst);
            TimeZoneRepository.Create(TimeZoneSecond);
            TimeZoneRepository.Create(TimeZoneThird);

            // Act
            var timeZones = ContentDeliveringService
                .GetAllTimeZones()
                .ToArray();

            // Assert
            Assert.AreEqual(3, timeZones.Length);
            Assert.Contains(TimeZoneFirst, timeZones);
            Assert.Contains(TimeZoneSecond, timeZones);
        }

        [Test]
        public void Should_GetOnlyOneTimeZone_When_CacheContainsEntriesForOneZones()
        {
            // Arrange
            var keyForFirstUser = new ContentDeliveringKey(TimeZoneFirst, UserIdFirst, Guid.Parse(DeskBarIdFirst));
            var keyForSecondUser = new ContentDeliveringKey(TimeZoneFirst, UserIdSecond, Guid.Parse(DeskBarIdSecond));

            ContentDeliveringService.Get(keyForFirstUser, DefaultIp, DefaultComputerName);
            ContentDeliveringService.Get(keyForSecondUser, DefaultIp, DefaultComputerName);

            TimeZoneRepository.Create(keyForFirstUser.TimeZone);
            TimeZoneRepository.Create(keyForSecondUser.TimeZone);

            // Act
            var timeZones = ContentDeliveringService
                .GetAllTimeZones()
                .ToArray();

            // Assert
            Assert.AreEqual(1, timeZones.Length);
            Assert.Contains(TimeZoneFirst, timeZones);
        }

        [Test]
        public void Should_GetEmptyCollection_When_CacheNotContainsEntries()
        {
            // Arrange
            // Act
            var timeZones = ContentDeliveringService.GetAllTimeZones();

            // Assert
            Assert.True(!timeZones.Any());
        }

        [Test]
        public void Should_SuccessfullyDelivered_When_DeliveryToBroadcast()
        {
            // Arrange
            // Act
            var isDelivered = ContentDeliveringService.DeliveryToBroadcast();

            // Assert
            Assert.True(isDelivered);
        }

        [Test]
        public void Should_SuccessfullyDelivered_When_DeliveryToAllUsers()
        {
            // Arrange
            // Act
            var isDelivered = ContentDeliveringService.DeliveryToUsers(new [] {UserIdFirst, UserIdSecond, UserIdThird});

            // Assert
            Assert.True(isDelivered);
        }

        [Test]
        public void Should_SuccessfullyDelivered_When_DeliveryToEveryUser()
        {
            // Arrange
            // Act
            var isDeliveredToFirstUser = ContentDeliveringService.DeliveryToUser(UserIdFirst);
            var isDeliveredToSecondUser = ContentDeliveringService.DeliveryToUser(UserIdSecond);
            var isDeliveredToThirdUser = ContentDeliveringService.DeliveryToUser(UserIdThird);

            // Assert
            Assert.True(isDeliveredToSecondUser);
            Assert.True(isDeliveredToFirstUser);
            Assert.True(isDeliveredToThirdUser);
        }

        [Test]
        public void Should_SuccessfullyDelivered_When_DeliveryToRecipientsOfContent()
        {
            // Arrange
            ContentRepository.Create(new AlertEntity { Id = ContentIdFirst });
            ContentRepository.Create(new AlertEntity { Id = ContentIdSecond });

            // Act
            var isDeliveredToRecipientsOfFirstContent = ContentDeliveringService.DeliveryToRecipientsOfContent(ContentIdFirst);
            var isDeliveredToRecipientsOfSecondContent = ContentDeliveringService.DeliveryToRecipientsOfContent(ContentIdSecond);

            // Assert
            Assert.True(isDeliveredToRecipientsOfFirstContent);
            Assert.True(isDeliveredToRecipientsOfSecondContent);
        }

        [Test]
        public void Should_SuccessfullyDelivered_When_DeliveryToRecipientsOfContentInTimeZone()
        {
            // Arrange
            // Act
            var isDeliveredToRecipientsOfFirstContent = ContentDeliveringService.DeliveryToRecipientsOfContent(ContentIdFirst, TimeZoneFirst);
            var isDeliveredToRecipientsOfSecondContent = ContentDeliveringService.DeliveryToRecipientsOfContent(ContentIdSecond, TimeZoneSecond);

            // Assert
            Assert.True(isDeliveredToRecipientsOfFirstContent);
            Assert.True(isDeliveredToRecipientsOfSecondContent);
        }

        [Test]
        public void Should_GetCorrectValue_When_GetForUserWhoHasManyContent()
        {
            // Arrange
            var expectedValue = $"{ContentIdFirst},{ContentIdSecond}";

            // Act
            var keyForFirstUser = new ContentDeliveringKey(TimeZoneFirst, UserIdFirst, Guid.Parse(DeskBarIdFirst));
            var valueForFirstUser = ContentDeliveringService.Get(keyForFirstUser, DefaultIp, DefaultComputerName);

            // Assert
            Assert.AreEqual(expectedValue, valueForFirstUser);
        }

        [Test]
        public void Should_GetCorrectValue_When_GetForUserWhoHasOneContent()
        {
            // Arrange
            var expectedValue = ContentIdFirst.ToString();

            // Act
            var keyForSecondUser = new ContentDeliveringKey(TimeZoneSecond, UserIdSecond, Guid.Parse(DeskBarIdSecond));
            var valueForSecondUser = ContentDeliveringService.Get(keyForSecondUser, DefaultIp, DefaultComputerName);

            // Assert
            Assert.AreEqual(expectedValue, valueForSecondUser);
        }

        [Test]
        public void Should_GetEmptyValue_When_GetForUserWhoHasNotContent()
        {
            // Arrange
            var expectedValue = string.Empty;

            // Act
            var keyForThirdUser = new ContentDeliveringKey(TimeZoneThird, UserIdThird, Guid.Parse(DeskBarIdThird));
            var valueForThirdUser = ContentDeliveringService.Get(keyForThirdUser, DefaultIp, DefaultComputerName);

            // Assert
            Assert.AreEqual(expectedValue, valueForThirdUser);
        }

        [Test]
        //TODO: Fix failing when it runs in series of tests
        public void Should_GetOldValue_When_RepositoryWasChangedButChangesWereNotDelivered()
        {
            // Arrange
            var keyForThirdUser = new ContentDeliveringKey(TimeZoneThird, UserIdThird, Guid.Parse(DeskBarIdThird));
            ContentDeliveringService.Get(keyForThirdUser, DefaultIp, DefaultComputerName);

            AddContentToDb(UserIdThird, new[] { ContentIdFirst, ContentIdSecond });

            // Act
            var valueForThirdUser = ContentDeliveringService.Get(keyForThirdUser, DefaultIp, DefaultComputerName);

            // Assert
            Assert.AreEqual(string.Empty, valueForThirdUser);
        }

        [Test]
        public void Should_GetNewValue_When_RepositoryWasChangedAndChangesWereNotDeliveredButRecipientInOtherTimeZone()
        {
            // Arrange
            var keyForThirdUser = new ContentDeliveringKey(TimeZoneThird, UserIdThird, Guid.Parse(DeskBarIdThird));
            ContentDeliveringService.Get(keyForThirdUser, DefaultIp, DefaultComputerName);

            AddContentToDb(UserIdThird, new[] { ContentIdFirst, ContentIdSecond });
            ContentRepository.Create(new AlertEntity { Type2 = ContentReceivingType.Recipients.GetStringValue(), Id = ContentIdFirst });
            ContentRepository.Create(new AlertEntity { Type2 = ContentReceivingType.Recipients.GetStringValue(), Id = ContentIdSecond });

            var expectedValue = $"{ContentIdFirst},{ContentIdSecond}";

            // Act
            var keyForThirdUserInOtherTimeZone = new ContentDeliveringKey(TimeZoneFirst, UserIdThird, Guid.Parse(DeskBarIdThird));
            var valueForThirdUser = ContentDeliveringService.Get(keyForThirdUserInOtherTimeZone, DefaultIp, DefaultComputerName);

            // Assert
            Assert.AreEqual(expectedValue, valueForThirdUser);
        }

        [Test]
        public void Should_GetNewValue_When_RepositoryWasChangedAndChangesWereDeliveredToBroadcast()
        {
            // Arrange
            var keyForThirdUser = new ContentDeliveringKey(TimeZoneThird, UserIdThird, Guid.Parse(DeskBarIdThird));
            ContentDeliveringService.Get(keyForThirdUser, DefaultIp, DefaultComputerName);

            AddContentToDb(UserIdThird, new[] { ContentIdFirst, ContentIdSecond });
            ContentDeliveringService.DeliveryToBroadcast();

            var expectedValue = $"{ContentIdFirst},{ContentIdSecond}";

            // Act
            var valueForThirdUser = ContentDeliveringService.Get(keyForThirdUser, DefaultIp, DefaultComputerName);

            // Assert
            Assert.AreEqual(expectedValue, valueForThirdUser);
        }

        [Test]
        public void Should_GetNewValue_When_RepositoryWasChangedAndChangesWereDeliveredToUsers()
        {
            // Arrange
            var keyForThirdUser = new ContentDeliveringKey(TimeZoneThird, UserIdThird, Guid.Parse(DeskBarIdThird));
            ContentDeliveringService.Get(keyForThirdUser, DefaultIp, DefaultComputerName);

            AddContentToDb(UserIdThird, new[] { ContentIdFirst, ContentIdSecond });
            ContentDeliveringService.DeliveryToUsers(new[] { UserIdFirst, UserIdThird });

            var expectedValue = $"{ContentIdFirst},{ContentIdSecond}";

            // Act
            var valueForThirdUser = ContentDeliveringService.Get(keyForThirdUser, DefaultIp, DefaultComputerName);

            // Assert
            Assert.AreEqual(expectedValue, valueForThirdUser);
        }

        [Test]
        public void Should_GetNewValue_When_RepositoryWasChangedAndChangesWereDeliveredToUser()
        {
            // Arrange
            var keyForThirdUser = new ContentDeliveringKey(TimeZoneThird, UserIdThird, Guid.Parse(DeskBarIdThird));
            ContentDeliveringService.Get(keyForThirdUser, DefaultIp, DefaultComputerName);

            AddContentToDb(UserIdThird, new[] { ContentIdFirst, ContentIdSecond });
            ContentDeliveringService.DeliveryToUser(UserIdThird);

            var expectedValue = $"{ContentIdFirst},{ContentIdSecond}";

            // Act
            var valueForThirdUser = ContentDeliveringService.Get(keyForThirdUser, DefaultIp, DefaultComputerName);

            // Assert
            Assert.AreEqual(expectedValue, valueForThirdUser);
        }

        [Test]
        public void Should_GetNewValue_When_RepositoryWasChangedAndChangesWereDeliveredToRecipientsOfContent()
        {
            // Arrange
            var keyForThirdUser = new ContentDeliveringKey(TimeZoneThird, UserIdThird, Guid.Parse(DeskBarIdThird));
            ContentDeliveringService.Get(keyForThirdUser, DefaultIp, DefaultComputerName);

            AddContentToDb(UserIdThird, new[] { ContentIdFirst, ContentIdSecond });
            ContentRepository.Create(new AlertEntity { Type2 = ContentReceivingType.Recipients.GetStringValue(), Id = ContentIdFirst });
            ContentRepository.Create(new AlertEntity { Type2 = ContentReceivingType.Recipients.GetStringValue(), Id = ContentIdSecond });
            ContentDeliveringService.DeliveryToRecipientsOfContent(ContentIdFirst);

            var expectedValue = $"{ContentIdFirst},{ContentIdSecond}";

            // Act
            var valueForThirdUser = ContentDeliveringService.Get(keyForThirdUser, DefaultIp, DefaultComputerName);

            // Assert
            Assert.AreEqual(expectedValue, valueForThirdUser);
        }

        [Test]
        public void Should_GetNewValue_When_RepositoryWasChangedAndChangesWereDeliveredToRecipientsOfContentInTimeZone()
        {
            // Arrange
            var keyForThirdUser = new ContentDeliveringKey(TimeZoneThird, UserIdThird, Guid.Parse(DeskBarIdThird));
            TimeZoneRepository.Create(TimeZoneThird);
            ContentDeliveringService.Get(keyForThirdUser, DefaultIp, DefaultComputerName);

            ContentRepository.Create(new AlertEntity { Type2 = ContentReceivingType.Recipients.GetStringValue(), Id = ContentIdFirst });
            ContentRepository.Create(new AlertEntity { Type2 = ContentReceivingType.Recipients.GetStringValue(), Id = ContentIdSecond });

            AddContentToDb(UserIdThird, new[] { ContentIdFirst, ContentIdSecond });

            ContentDeliveringService.DeliveryToRecipientsOfContent(ContentIdFirst, TimeZoneThird);

            var expectedValue = $"{ContentIdFirst},{ContentIdSecond}";

            // Act
            var valueForThirdUser = ContentDeliveringService.Get(keyForThirdUser, DefaultIp, DefaultComputerName);

            // Assert
            Assert.AreEqual(expectedValue, valueForThirdUser);
        }
    }
}
