﻿using DeskAlerts.ApplicationCore.Models.ContentTypes;
using DeskAlerts.ContentDeliveringService.Implementations;
using DeskAlerts.ContentDeliveringService.Interfaces;
using DeskAlerts.UnitTestProject.FakeRepositories;
using NUnit.Framework;
using System.Collections.Generic;

namespace DeskAlerts.UnitTestProject.ContentDelivering
{
    [TestFixture]
    public class VideoAlertContentDeliveringServiceTests : BaseContentDeliveringServiceTests<VideoAlertContentType>
    {
        private FakeVideoAlertRepository _videoAlertRepository;

        protected override IContentDeliveringService<VideoAlertContentType> ContentDeliveringService { get; set; }

        [SetUp]
        public void SetUp()
        {
            BaseInitialize();

            var cache = new InMemoryCache(1000);
            _videoAlertRepository = new FakeVideoAlertRepository();
            ContentDeliveringService = new VideoAlertsContentDeliveringService(cache, ContentRepository, _videoAlertRepository, TimeZoneRepository);

            BaseSetUp();
        }

        protected override void AddContentToDb(long userId, IEnumerable<long> contentIds)
        {
            _videoAlertRepository.Create(userId, contentIds);
        }
    }
}
