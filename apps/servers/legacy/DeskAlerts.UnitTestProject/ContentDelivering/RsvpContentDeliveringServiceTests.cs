﻿using System.Collections.Generic;
using DeskAlerts.ApplicationCore.Models.ContentTypes;
using DeskAlerts.ContentDeliveringService.Implementations;
using DeskAlerts.ContentDeliveringService.Interfaces;
using DeskAlerts.UnitTestProject.FakeRepositories;
using NUnit.Framework;

namespace DeskAlerts.UnitTestProject.ContentDelivering
{
    [TestFixture]
    public class RsvpContentDeliveringServiceTests : BaseContentDeliveringServiceTests<RsvpContentType>
    {
        private FakeRsvpRepository _rsvpRepository;

        protected override IContentDeliveringService<RsvpContentType> ContentDeliveringService { get; set; }

        [SetUp]
        public void SetUp()
        {
            BaseInitialize();

            var cache = new InMemoryCache(1000);
            _rsvpRepository = new FakeRsvpRepository();
            ContentDeliveringService = new RsvpContentDeliveringService(cache, ContentRepository, _rsvpRepository, TimeZoneRepository);

            BaseSetUp();
        }

        protected override void AddContentToDb(long userId, IEnumerable<long> contentIds)
        {
            _rsvpRepository.Create(userId, contentIds);
        }
    }
}