﻿namespace DeskAlerts.UnitTestProject.Survey
{
    using DeskAlertsDotNet.Pages;

    using NUnit.Framework;

    [TestFixture]
    public class GetSurveyTest
    {
        public GetSurvey Target { get; private set; }

        [SetUp]
        public void SetUp()
        {
            Target = new GetSurvey();
        }

        /// <summary>
        /// Test formating html comment in responce for clients application.
        /// </summary>
        [Test]
        public void FormatTitleTest()
        {
            string inputString = " s jd jkh jhvgb <!-- html_title=\"fd jdsgdhf bkdsj >< dgds >< fg dsg\" --></body>";
            string outputString = " s jd jkh jhvgb <!-- html_title = \'fd jdsgdhf bkdsj >< dgds >< fg dsg\' --></body>";

            string result = Target.FormatTitle(inputString);

            Assert.AreEqual(outputString, result);
        }
    }
}
