@ECHO OFF
cls
if %~d0 == \\ (
	echo.
	echo This script can't be run from UNC path
	echo.
	pause
	goto :eof
)
cd /d "%ProgramFiles(x86)%"||cd /d "%ProgramFiles%"
cd "Microsoft Visual Studio 12.0"
call "Common7\Tools\vsvars32.bat"
cd /d "%~dp0"
echo.
echo.Building project with Visual Studio...
echo.
echo.^>^>Release x86
devenv  "DeskAlertsAsp.sln" /build "Release 9|Any CPU"
echo.
echo.^>^>Release x64
devenv  "DeskAlertsAsp.sln" /build "Release 9|Any CPU"
echo.
echo.
if exist installer\include\custom.sql (
	echo. ATTENTION!!! FOUND CUSTOM.SQL FILE
	echo.
)
copy "obj\Release 9\DeskAlertsDotNet.dll" sever\STANDARD\bin\DeskAlertsDotNet.dll >nul
rem set revision=0
rem for /f "tokens=5" %%i in ('SubWCRev .^|find "Last committed at revision"') do set revision=%%i
rem echo Number of sources revision: %revision% [will be add to version]
rem echo Version format: major.minor.build.revision
rem set "style=0.0.0"
rem set "format=^[0-9][0-9]*\.[0-9][0-9]*\.[0-9][0-9]*$"
echo.
set "contract_style=YYYYMMDD"
set "contract_format=^[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$"
echo Contract style: %contract_style%
set /P contract_date="Please enter contract date:  "


echo %contract_date%|findstr /i /r "%contract_format%" > nul

echo update version set contract_date=cast( > installer\include\Custom_contract_date.sql
echo '%contract_date%'>> installer\include\Custom_contract_date.sql
echo  as datetime) >> installer\include\Custom_contract_date.sql

rem set CONTRACT_DATETIME=%contract_date%
set attr=/DCONTRACT_DATETIME=%contract_date%
rem set "CONTRACT_DATETIME=%contract_date%"

echo.
set "style=0.0.0.0abrc"
set "format=^[0-9][0-9]*\.[0-9][0-9]*\.[0-9][0-9]*\.[0-9a-z][0-9a-z]*$"
if not exist lastversion.log goto again
set /p version=<lastversion.log
echo %version%|findstr /i /r "%format%" > nul
if errorlevel 1 goto again
echo Please push Return key for keep version %version%
set /p version=or enter other version in style %style%  
goto check
:again
set /p version=Please enter version in style %style%  
:check
echo %version%|findstr /i /r "%format%" > nul
if errorlevel 1 echo Wrong version string. & goto again
echo %version%>lastversion.log
rem set attr=/DPRODUCT_VERSION=%version%.%revision%

set attr=/DPRODUCT_VERSION=%version%

for /f "delims=." %%a in ("%version%") do set major=%%a
if %major% gtr 4 set attr=%attr% /DfiveServer
if %major% gtr 5 set attr=%attr% /DsixServer
if %major% gtr 6 set attr=%attr% /DsevenServer
if %major% gtr 7 set attr=%attr% /DeightServer
if %major% gtr 8 set attr=%attr% /DnineServer

ECHO.
set choices=
set lastchoices=________________________________
if not exist lastchoices.log goto no_lastchoices
set /p lastchoices=<lastchoices.log
set lastchoices=%lastchoices%________________________________
:no_lastchoices

:again_tbserv
set tbserv=%lastchoices:~0,1%
set text=
if not '%tbserv%' == '_' set text=(last value: %tbserv%) 
set /p tbserv=Do you want to build DEMO Server?" %text%[y/n] 
if /i '%tbserv:~0,1%'=='n' set tbserv=n& goto no_tbserv
if /i not '%tbserv:~0,1%'=='y' goto again_tbserv
set attr=%attr% /DdemoServer
set tbserv=y
:no_tbserv
set choices=%choices%%tbserv%

ECHO.

:again_ad
set choice=%lastchoices:~1,1%
set text=
if not '%choice%' == '_' set text=(last value: %choice%) 
set /p choice=Do you want to add "AD/LDAP add-on"? %text%[y/n] 
if /i '%choice:~0,1%'=='n' set choice=n& goto no_ad
if /i not '%choice:~0,1%'=='y' goto again_ad
set attr=%attr% /DisADAddon
set choice=y
:no_ad
set choices=%choices%%choice%

:again_ed
set choice=%lastchoices:~2,1%
set text=
if not '%choice%' == '_' set text=(last value: %choice%) 
set /p choice=Do you want to add "eDirectory add-on"? %text%[y/n] 
if /i '%choice:~0,1%'=='n' set choice=n& goto no_ed
if /i not '%choice:~0,1%'=='y' goto again_ed
set attr=%attr% /DisEDAddon
set choice=y
:no_ed
set choices=%choices%%choice%

:again_sms
set choice=%lastchoices:~3,1%
set text=
if not '%choice%' == '_' set text=(last value: %choice%) 
set /p choice=Do you want to add "SMS add-on"? %text%[y/n] 
if /i '%choice:~0,1%'=='n' set choice=n& goto no_sms
if /i not '%choice:~0,1%'=='y' goto again_sms
set attr=%attr% /DisSMSAddon
set choice=y
:no_sms
set choices=%choices%%choice%

:again_email
set choice=%lastchoices:~4,1%
set text=
if not '%choice%' == '_' set text=(last value: %choice%) 
set /p choice=Do you want to add "E-Mail add-on"? %text%[y/n] 
if /i '%choice:~0,1%'=='n' set choice=n& goto no_email
if /i not '%choice:~0,1%'=='y' goto again_email
set attr=%attr% /DisEMAILAddon
set choice=y
:no_email
set choices=%choices%%choice%

:again_enc
set choice=%lastchoices:~5,1%
set text=
if not '%choice%' == '_' set text=(last value: %choice%) 
set /p choice=Do you want to add "Encryption add-on"? %text%[y/n] 
if /i '%choice:~0,1%'=='n' set choice=n& goto no_enc
if /i not '%choice:~0,1%'=='y' goto again_enc
set attr=%attr% /DisENCRYPTAddon
set choice=y
:no_enc
set choices=%choices%%choice%

:againg_surv
set choice=%lastchoices:~6,1%
set text=
if not '%choice%' == '_' set text=(last value: %choice%) 
set /p choice=Do you want to add "Survey manager add-on"? %text%[y/n] 
if /i '%choice:~0,1%'=='n' set choice=n& goto no_surv
if /i not '%choice:~0,1%'=='y' goto againg_surv
set attr=%attr% /DisSURVEYSAddon
set choice=y
:no_surv
set choices=%choices%%choice%

:again_stat
set choice=%lastchoices:~7,1%
set text=
if not '%choice%' == '_' set text=(last value: %choice%) 
set /p choice=Do you want to add "Extended statistics add-on"? %text%[y/n] 
if /i '%choice:~0,1%'=='n' set choice=n& goto no_stat
if /i not '%choice:~0,1%'=='y' goto again_stat
set attr=%attr% /DisSTATISTICSAddon
set choice=y
:no_stat
set choices=%choices%%choice%

:again_ss
set choice=%lastchoices:~8,1%
set text=
if not '%choice%' == '_' set text=(last value: %choice%) 
set /p choice=Do you want to add "Screeensaver add-on"? %text%[y/n] 
if /i '%choice:~0,1%'=='n' set choice=n& goto no_ss
if /i not '%choice:~0,1%'=='y' goto again_ss
set attr=%attr% /DisSCREENSAVERAddon
set choice=y
:no_ss
set choices=%choices%%choice%

:again_rss
set choice=%lastchoices:~9,1%
set text=
if not '%choice%' == '_' set text=(last value: %choice%) 
set /p choice=Do you want to add "RSS add-on"? %text%[y/n] 
if /i '%choice:~0,1%'=='n' set choice=n& goto no_rss
if /i not '%choice:~0,1%'=='y' goto again_rss
set attr=%attr% /DisRSSAddon
set choice=y
:no_rss
set choices=%choices%%choice%

:again_wp
set choice=%lastchoices:~10,1%
set text=
if not '%choice%' == '_' set text=(last value: %choice%) 
set /p choice=Do you want to add "Wallpaper add-on"? %text%[y/n] 
if /i '%choice:~0,1%'=='n' set choice=n& goto no_wp
if /i not '%choice:~0,1%'=='y' goto again_wp
set attr=%attr% /DisWALLPAPERAddon
set choice=y
:no_wp
set choices=%choices%%choice%

:again_blg
set choice=%lastchoices:~11,1%
set text=
if not '%choice%' == '_' set text=(last value: %choice%) 
set /p choice=Do you want to add "Blog add-on"? %text%[y/n] 
if /i '%choice:~0,1%'=='n' set choice=n& goto no_blg
if /i not '%choice:~0,1%'=='y' goto again_blg
set attr=%attr% /DisBLOGAddon
set choice=y
:no_blg
set choices=%choices%%choice%

:again_twtr
set choice=%lastchoices:~12,1%
set text=
if not '%choice%' == '_' set text=(last value: %choice%) 
set /p choice=Do you want to add "Social add-on for Twitter"? %text%[y/n] 
if /i '%choice:~0,1%'=='n' set choice=n& goto no_twtr
if /i not '%choice:~0,1%'=='y' goto again_twtr
set attr=%attr% /DisTWITTERAddon
set choice=y
:no_twtr
set choices=%choices%%choice%

:again_lnkdn
set choice=%lastchoices:~13,1%
set text=
if not '%choice%' == '_' set text=(last value: %choice%) 
set /p choice=Do you want to add "Social add-on for LinkedIn"? %text%[y/n] 
if /i '%choice:~0,1%'=='n' set choice=n& goto no_lnkdn
if /i not '%choice:~0,1%'=='y' goto again_lnkdn
set attr=%attr% /DisLINKEDINAddon
set choice=y
:no_lnkdn
set choices=%choices%%choice%

:again_fsa
set choice=%lastchoices:~14,1%
set text=
if not '%choice%' == '_' set text=(last value: %choice%) 
set /p choice=Do you want to add "Fullscreen alert add-on"? %text%[y/n] 
if /i '%choice:~0,1%'=='n' set choice=n& goto no_fsa
if /i not '%choice:~0,1%'=='y' goto again_fsa
set attr=%attr% /DisFULLSCREENAddon
set choice=y
:no_fsa
set choices=%choices%%choice%

:again_tick
set choice=%lastchoices:~15,1%
set text=
if not '%choice%' == '_' set text=(last value: %choice%) 
set /p choice=Do you want to add "Scrolling ticker alert add-on"? %text%[y/n] 
if /i '%choice:~0,1%'=='n' set choice=n& goto no_tick
if /i not '%choice:~0,1%'=='y' goto again_tick
set attr=%attr% /DisTICKERAddon
set choice=y
:no_tick
set choices=%choices%%choice%

:again_tickpro
set choice=%lastchoices:~16,1%
set text=
if not '%choice%' == '_' set text=(last value: %choice%) 
set /p choice=Do you want to add "Scrolling ticker PRO add-on"? %text%[y/n] 
if /i '%choice:~0,1%'=='n' set choice=n& goto no_tickpro
if /i not '%choice:~0,1%'=='y' goto again_tickpro
set attr=%attr% /DisTICKERPROAddon
set choice=y
:no_tickpro
set choices=%choices%%choice%

:again_im
set choice=%lastchoices:~17,1%
set text=
if not '%choice%' == '_' set text=(last value: %choice%) 
set /p choice=Do you want to add "Instant Messages add-on"? %text%[y/n] 
if /i '%choice:~0,1%'=='n' set choice=n& goto no_im
if /i not '%choice:~0,1%'=='y' goto again_im
set attr=%attr% /DisIMAddon
set choice=y
:no_im
set choices=%choices%%choice%

:again_widgetstat
set attr=%attr% /DisWIDGETSTATAddon
set choice=y
:no_widgetstat
set choices=%choices%%choice%

:again_textcall
set choice=%lastchoices:~19,1%
set text=
if not '%choice%' == '_' set text=(last value: %choice%) 
set /p choice=Do you want to add "Text to call module"? %text%[y/n] 
if /i '%choice:~0,1%'=='n' set choice=n& goto no_textcall
if /i not '%choice:~0,1%'=='y' goto again_textcall
set attr=%attr% /DisTEXTCALLAddon
set choice=y
:no_textcall
set choices=%choices%%choice%

:again_mobile
set choice=%lastchoices:~20,1%
set text=
if not '%choice%' == '_' set text=(last value: %choice%) 
set /p choice=Do you want to add "Mobile sending module"? %text%[y/n] 
if /i '%choice:~0,1%'=='n' set choice=n& goto no_mobile
if /i not '%choice:~0,1%'=='y' goto again_mobile
set attr=%attr% /DisMOBILEAddon
set choice=y
:no_mobile
set choices=%choices%%choice%

:again_video
set choice=%lastchoices:~21,1%
set text=
if not '%choice%' == '_' set text=(last value: %choice%) 
set /p choice=Do you want to add "Videoalert add-on"? %text%[y/n] 
if /i '%choice:~0,1%'=='n' set choice=n& goto no_video
if /i not '%choice:~0,1%'=='y' goto again_video
set attr=%attr% /DisVIDEOAddon
set choice=y
:no_video

set choices=%choices%%choice%

:again_webplugin
set choice=%lastchoices:~22,1%
set text=
if not '%choice%' == '_' set text=(last value: %choice%) 
set /p choice=Do you want to add "WebPlugin add-on"? %text%[y/n] 
if /i '%choice:~0,1%'=='n' set choice=n& goto no_webplugin
if /i not '%choice:~0,1%'=='y' goto again_webplugin
set attr=%attr% /DisWEBPLUGINAddon
set choice=y
:no_webplugin

set choices=%choices%%choice%

:again_campaign
set attr=%attr% /DisCAMPAIGNAddon
set choice=y
:no_campaign

set choices=%choices%%choice%

:approve_panel_again
set attr=%attr% /DisAPPROVEAddon
set choice=y
:no_approve_panel

set choices=%choices%%choice%

:again_digsign
set choice=%lastchoices:~25,1%
set text=
if not '%choice%' == '_' set text=(last value: %choice%) 
set /p choice=Do you want to add "Digital signage add-on"? %text%[y/n] 
if /i '%choice:~0,1%'=='n' set choice=n& goto no_digsign
if /i not '%choice:~0,1%'=='y' goto again_digsign
set attr=%attr% /DisDIGSIGNAddon
set choice=y
:no_digsign

set choices=%choices%%choice%

:again_yammer
set choice=%lastchoices:~26,1%
set text=
if not '%choice%' == '_' set text=(last value: %choice%) 
set /p choice=Do you want to add "Social module for Yammer"? %text%[y/n] 
if /i '%choice:~0,1%'=='n' set choice=n& goto no_yammer
if /i not '%choice:~0,1%'=='y' goto again_yammer
set attr=%attr% /DisYAMMERAddon
set choice=y
:no_yammer

set choices=%choices%%choice%

ECHO %choices%>lastchoices.log

ECHO.
if not exist lastlicenses.log goto again_l
set /p licenses=<lastlicenses.log
echo %licenses%|findstr /r "^[0-9][0-9]*$" > nul
if errorlevel 1 goto again_l
echo Please push Return key for keep number of licenses: %licenses%
set /p licenses=or enter new number of licenses  
goto check_l
:again_l
set /p licenses=Please enter number of licenses  
:check_l
echo %licenses%|findstr /r "^[0-9][0-9]*$" > nul
if errorlevel 1 echo Wrong number of licenses. & goto again_l
>lastlicenses.log echo.%licenses%
if "%licenses%" == "0" (
set attr=%attr% /Dlicenses=unlimited
) else (
set attr=%attr% /Dlicenses=%licenses%
)

ECHO.
if not exist lasttrial.log goto again_t
set /p trial=<lasttrial.log
echo %trial%|findstr /r "^[0-9][0-9]*$" > nul
if errorlevel 1 goto again_t
echo Please push Return key for keep number of trial days (0 - not trial): %trial%
set /p trial=or enter new number of trial days (0 - not trial)  
goto check_t
:again_t
set /p trial=Please enter number of trial days (0 - not trial)  
:check_t
echo %trial%|findstr /r "^[0-9][0-9]*$" > nul
if errorlevel 1 echo Wrong number of trial days. & goto again_t
>lasttrial.log echo.%trial%
if not "%trial%" == "0" (
	set attr=%attr% /Dtrial=%trial%
	set version=%version%.trial
)

ECHO.
ECHO Patching...

rmdir /q /s tmp 2>nul
mkdir tmp
copy checker\Release\Win32\DeskAlertsServer32.dll tmp\DeskAlertsServer32.dll >nul
copy checker\Release\x64\DeskAlertsServer64.dll tmp\DeskAlertsServer64.dll >nul

ECHO.
checker\Release\patcher.exe tmp\DeskAlertsServer32.dll %licenses% %trial%
checker\Release\patcher.exe tmp\DeskAlertsServer64.dll %licenses% %trial%

ECHO.
ECHO Signing...
ECHO.

installer\signtool.exe sign /f installer\ToolbarStudioInc.pfx /p softomate54Pas /t http://timestamp.verisign.com/scripts/timstamp.dll /v tmp\DeskAlertsServer32.dll tmp\DeskAlertsServer64.dll

ECHO.
ECHO Building...
ECHO.

NSIS\makensis.exe /V4 %attr% installer\ServerInstallation.nsi > build.log


if "%ERRORLEVEL%" == "0" (
ECHO Done. View build.log
ECHO.
installer\signtool.exe sign /f installer\ToolbarStudioInc.pfx /p softomate54Pas /t http://timestamp.verisign.com/scripts/timstamp.dll /v ServerInstallation.exe
move ServerInstallation.exe DeskAlerts.Server.v%version%.exe
) else (
ECHO Failed. View build.log
)
ECHO.
rmdir /q /s tmp 2>nul
pause