// Patcher.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <string.h>
#include <stdlib.h>
#include <objbase.h>
#include "../constants.h"

char * strnstrn(const char * str1, size_t str1_n, const char * str2, size_t str2_n)
{
	char *cp = (char *) str1;
	char *s1, *s2;

	while(cp <= str1 + str1_n)
	{
		s1 = cp;
		s2 = (char *) str2;

		while ( ( s1 <= (str1 + str1_n) ) && ( s2 <= (str2 + str2_n) ) && !(*s1-*s2) )
			s1++, s2++;

		if (s2 == str2 + str2_n + 1)
			return(cp);

		cp++;
	}

	return(NULL);
}

int _tmain(int argc, _TCHAR* argv[])
{
	if(argc >= 3)
	{
		char *str = NULL;
		FILE* fp = NULL;
		errno_t err;
		if(!(err = _tfopen_s(&fp, argv[1], _T("r+b"))) && fp)
		{
			fseek(fp, 0, SEEK_END);
			long sz = ftell(fp);
			if(sz > 0)
			{
				fseek(fp, 0, SEEK_SET);
				str = new char[sz];
				int numread = fread(str, 1, sz, fp);
				char *pch = strnstrn(str, sz, g_LicenseCountStr, strlen(g_LicenseCountStr));
				if(pch)
				{
					//////////////////////////////////////////////////////////////////////////
					// Licenses count
					long number = _tcstol(argv[2], NULL, 0);

					GUID g;
					unsigned char *gs1, *gs2, *gs3;
					char *newstr = new char[strlen(g_LicenseCountStr)+1];
					CoCreateGuid(&g);
					UuidToStringA(&g,&gs1);
					CoCreateGuid(&g);
					UuidToStringA(&g,&gs2);
					CoCreateGuid(&g);
					UuidToStringA(&g,&gs3);
					sprintf_s(newstr, strlen(g_LicenseCountStr)+1, "{%s}{%s}{%s}", gs1, gs2, gs3);

					char c[9];
					sprintf_s(c, 9, "%08x", number ^ g_LicenseCountXor);
					newstr[LicenseStep0] = c[0];
					newstr[LicenseStep1] = c[1];
					newstr[LicenseStep2] = c[2];
					newstr[LicenseStep3] = c[3];
					newstr[LicenseStep4] = c[4];
					newstr[LicenseStep5] = c[5];
					newstr[LicenseStep6] = c[6];
					newstr[LicenseStep7] = c[7];

					fseek(fp, pch-str, SEEK_SET);
					fwrite(newstr, 1, strlen(newstr), fp);
					delete newstr;

					//////////////////////////////////////////////////////////////////////////
					// Trial time
					pch = strnstrn(str, sz, g_TrialTimeStr, strlen(g_TrialTimeStr));
					if(pch)
					{
						if(argc >= 4)
							number = _tcstol(argv[3], NULL, 0)*60*60*24; //in seconds
						else
							number = 0;

						newstr = new char[strlen(g_TrialTimeStr)+1];
						CoCreateGuid(&g);
						UuidToStringA(&g,&gs1);
						CoCreateGuid(&g);
						UuidToStringA(&g,&gs2);
						CoCreateGuid(&g);
						UuidToStringA(&g,&gs3);
						sprintf_s(newstr, strlen(g_TrialTimeStr)+1, "{%s}{%s}{%s}", gs1, gs2, gs3);

						sprintf_s(c, 9, "%08x", number ^ g_TrialTimeXor);
						newstr[TrialTimeStep0] = c[0];
						newstr[TrialTimeStep1] = c[1];
						newstr[TrialTimeStep2] = c[2];
						newstr[TrialTimeStep3] = c[3];
						newstr[TrialTimeStep4] = c[4];
						newstr[TrialTimeStep5] = c[5];
						newstr[TrialTimeStep6] = c[6];
						newstr[TrialTimeStep7] = c[7];

						fseek(fp, pch-str, SEEK_SET);
						fwrite(newstr, 1, strlen(newstr), fp);
						delete newstr;

						_tprintf(_T("patching is done for: %s\n"), argv[1]);
					}
					else
						_tprintf(_T("can't find trial string in: %s\n"), argv[1]);
				}
				else
					_tprintf(_T("can't find license string in: %s\n"), argv[1]);
				delete str;
			}
			fclose(fp);
		}
		else
		{
			TCHAR buff[1024];
			_tcserror_s(buff, size_t(buff)/sizeof(TCHAR), err);
			_tprintf(_T("can't open for read/write: %s\nerror: %s\n"), argv[1], buff);
		}
	}
	else
	{
		printf("Use: patcher.exe DeskAlertsServer.dll licenses [trial days]\n");
	}
	return 0;
}

