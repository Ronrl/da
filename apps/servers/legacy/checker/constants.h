const char *g_LicenseCountStr = "{9C53B961-1610-4ce0-A860-E2044D621FA9}{160B091A-42FD-4f15-B8AF-D073AFBAE9F5}{5F5B7F45-20AE-49c2-AF2F-76292422A783}";
const long g_LicenseCountXor = 0x7fa7b16a;
const char *g_TrialTimeStr = "{7005C0A1-7542-430c-B24C-AABC2A01D32A}{4626F487-F003-4a74-9727-547DC8F76E27}{80D0C3CA-3BFC-449f-8B32-80FD71E43828}";
const long g_TrialTimeXor = 0x73ab318e;

enum {
	LicenseStep0 = 41,
	LicenseStep1 = 64,
	LicenseStep2 = 17,
	LicenseStep3 = 106,
	LicenseStep4 = 78,
	LicenseStep5 = 60,
	LicenseStep6 = 58,
	LicenseStep7 = 68
} LicenseSteps;

enum {
	TrialTimeStep0 = 53,
	TrialTimeStep1 = 23,
	TrialTimeStep2 = 73,
	TrialTimeStep3 = 112,
	TrialTimeStep4 = 32,
	TrialTimeStep5 = 43,
	TrialTimeStep6 = 26,
	TrialTimeStep7 = 84
} TrialTimeSteps;