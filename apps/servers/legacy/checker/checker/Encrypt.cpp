// Encrypt.cpp : Implementation of CEncrypt

#include "stdafx.h"
#include "Encrypt.h"
#include "Rijndael.h"
#include "sha1.h"
#include "MD5Hash.h"

#include <atlsafe.h>
#include <atlstr.h>
#include <atlenc.h>

//CEncrypt
STDMETHODIMP CEncrypt::RC4Encrypt(char *data, const char *password, SAFEARRAY **result)
{
	RELEASE_TRY
	{
		size_t sz = strlen(data);
		RC4_Session rec;

		rec.data = data;
		rec.data_len = sz;

		RC4_Start(rec, password, strlen(password));

		CComSafeArray<BYTE, VT_UI1> resArray;
		for (size_t i = 0; i<sz; i++)
		{
			data[i] ^= RC4_Gen(rec);
			resArray.Add(sizeof(BYTE), (BYTE*)&data[i], false);
		}
		*result = resArray.Detach();
	}
	RELEASE_CATCH_ALL;
	return S_OK;
}

STDMETHODIMP CEncrypt::AESEncrypt(char *data, const char *password, SAFEARRAY **result)
{
	RELEASE_TRY
	{
		CRijndael oRijndael;
		oRijndael.MakeKey(password, CRijndael::sm_chain0, 16, 16);

		char *szDataIn = data;
		char *szDataOut = NULL;

		oRijndael.EncryptBlock(szDataIn, szDataOut);

		CComSafeArray<BYTE, VT_UI1> resArray;
		resArray.Add(static_cast<int>(strlen(szDataOut)), (BYTE*)szDataOut, TRUE);
		*result = resArray.Detach();
	}
	RELEASE_CATCH_ALL;
	return S_OK;
}

STDMETHODIMP CEncrypt::encrypt(
	/* [in] */ BSTR data,
	/* [in] */ BSTR password,
	/* [retval][out] */ SAFEARRAY **result)
{
	return RC4Encrypt(CW2A(data), CW2A(password, CP_UTF8), result);
}

STDMETHODIMP CEncrypt::encryptUTF8(
	/* [in] */ BSTR data,
	/* [in] */ BSTR password,
	/* [retval][out] */ SAFEARRAY **result)
{
	return RC4Encrypt(CW2A(data, CP_UTF8), CW2A(password, CP_UTF8), result);
}

STDMETHODIMP CEncrypt::newEncrypt(
	/* [in] */ BSTR data,
	/* [in] */ BSTR password,
	/* [retval][out] */ SAFEARRAY **result)
{
	return AESEncrypt(CW2A(data), CW2A(password, CP_UTF8), result);
}

STDMETHODIMP CEncrypt::newEncryptUTF8(
	/* [in] */ BSTR data,
	/* [in] */ BSTR password,
	/* [retval][out] */ SAFEARRAY **result)
{
	return AESEncrypt(CW2A(data, CP_UTF8), CW2A(password, CP_UTF8), result);
}

//CEncrypt2

STDMETHODIMP CEncrypt::md5FromUtf8(
	/* [in] */ BSTR str,
	/* [retval][out] */ BSTR *result)
{
	RELEASE_TRY
	{
		CW2A utf8str(str, CP_UTF8);
		*result = CComBSTR(CMD5Hash::GetMD5((const BYTE*)(const CHAR*)utf8str, strlen(utf8str))).Detach();
	}
	RELEASE_CATCH_ALL;
	return S_OK;
}

STDMETHODIMP CEncrypt::sha1FromUtf8(
	/* [in] */ BSTR str,
	/* [retval][out] */ BSTR *result)
{
	RELEASE_TRY
	{
		CW2A utf8str(str, CP_UTF8);
		SHA1 sha1;
		sha1.Input(utf8str, static_cast<int>(strlen(utf8str)));
		unsigned iRes[5];
		if(sha1.Result((unsigned *)&iRes))
		{
			CString csRes;
			csRes.Format(_T("%08x%08x%08x%08x%08x"), iRes[0], iRes[1], iRes[2], iRes[3], iRes[4]);
			*result = CComBSTR(csRes).Detach();
		}
		else
		{
			*result = CComBSTR().Detach();
		}
	}
	RELEASE_CATCH_ALL;
	return S_OK;
}

STDMETHODIMP CEncrypt::base64FromUtf8(
	/* [in] */ BSTR str,
	/* [retval][out] */ BSTR *result)
{
	RELEASE_TRY
	{
		CW2A utf8str(str, CP_UTF8);
		CStringA str64;
		int nSourceLen = static_cast<int>(strlen(utf8str));
		int nDestLen = Base64EncodeGetRequiredLength(nSourceLen);
		Base64Encode((const BYTE*)(const CHAR*)utf8str, nSourceLen, str64.GetBuffer(nDestLen), &nDestLen);
		str64.ReleaseBuffer(nDestLen);
		*result = CComBSTR(str64).Detach();
	}
	RELEASE_CATCH_ALL;
	return S_OK;
}


STDMETHODIMP CEncrypt::strFromUtf8Base64(
	/* [in] */ BSTR str,
	/* [retval][out] */ BSTR *result)
{
	RELEASE_TRY
	{
		CStringA csResult;
		CW2A str64(str);
		int nSourceLen = static_cast<int>(strlen(str64));
		int nDestLen = Base64DecodeGetRequiredLength(nSourceLen);
		Base64Decode(str64, nSourceLen, (BYTE*)csResult.GetBuffer(nDestLen), &nDestLen);
		csResult.ReleaseBuffer(nDestLen);
		*result = CComBSTR(CA2W(csResult, CP_UTF8)).Detach();
	}
	RELEASE_CATCH_ALL;
	return S_OK;
}

STDMETHODIMP CEncrypt::utf8FromBase64(
	/* [in] */ BSTR str,
	/* [retval][out] */ SAFEARRAY **result)
{
	RELEASE_TRY
	{
		CStringA csResult;
		CW2A str64(str);
		int nSourceLen = static_cast<int>(strlen(str64));
		int nDestLen = Base64DecodeGetRequiredLength(nSourceLen);
		Base64Decode(str64, nSourceLen, (BYTE*)csResult.GetBuffer(nDestLen), &nDestLen);
		csResult.ReleaseBuffer(nDestLen);

		CComSafeArray<BYTE, VT_UI1> resArray;
		resArray.Add(csResult.GetLength(),(BYTE*)csResult.GetString(), TRUE);
		*result = resArray.Detach();
	}
	RELEASE_CATCH_ALL;
	return S_OK;
}

STDMETHODIMP CEncrypt::shortIdFromGuid(
	/* [in] */ BSTR str,
	/* [retval][out] */ BSTR *result)
{
	RELEASE_TRY
	{
		UUID uuid = {0};
		CStringW csStr = str;
		csStr.Remove(L'{');
		csStr.Remove(L'}');
		/*const HRESULT hr = */UuidFromString((RPC_WSTR)csStr.GetString(), &uuid);

		CStringA str64;
		int nSourceLen = sizeof(UUID);
		int nDestLen = Base64EncodeGetRequiredLength(nSourceLen);
		Base64Encode((const BYTE*)&uuid, nSourceLen, str64.GetBuffer(nDestLen), &nDestLen);
		str64.ReleaseBuffer(nDestLen);
		str64.Replace("/", "_");
		str64.Replace("+", "-");
		*result = CComBSTR(str64.Mid(0, 22)).Detach();
	}
	RELEASE_CATCH_ALL;
	return S_OK;
}

STDMETHODIMP CEncrypt::guidFromShortId(
	/* [in] */ BSTR str,
	/* [retval][out] */ BSTR *result)
{
	RELEASE_TRY
	{
		CStringW csStr = str;
		csStr.Replace(L"_", L"/");
		csStr.Replace(L"-", L"+");
		csStr += L"==";

		CStringA csResult;
		CW2A str64(csStr);
		csStr = L"00000000-0000-0000-0000-000000000000";
		int nSourceLen = static_cast<int>(strlen(str64));
		int nDestLen = Base64DecodeGetRequiredLength(nSourceLen);
		if(Base64Decode(str64, nSourceLen, (BYTE*)csResult.GetBuffer(nDestLen), &nDestLen))
		{
			csResult.ReleaseBuffer(nDestLen);

			RPC_WSTR buff = NULL;
			const HRESULT hr = UuidToString((UUID*)csResult.GetString(), &buff);
			if(hr == S_OK)
			{
				csStr = (WCHAR*)buff;
				RpcStringFree(&buff);
			}
		}

		*result = CComBSTR(csStr).Detach();
	}
	RELEASE_CATCH_ALL;
	return S_OK;
}
