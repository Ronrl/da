// LDAPQuery.cpp : Implementation of CEDirectoryQuery

#include "stdafx.h"
#include "LDAPQuery.h"
#include "AssocObject.h"
#include <chrono>
#include <iostream>
#include <fstream>
#include <atlcoll.h>
#include <comutil.h >
#include <ctime>
#include <iomanip>
double TotalLogDuration = 0.0;
std::string bstr_to_str(BSTR source) {
    //source = L"lol2inside";
    _bstr_t wrapped_bstr = _bstr_t(source);
    int length = wrapped_bstr.length();
    char* char_array = new char[length];
    strcpy_s(char_array, length + 1, wrapped_bstr);
    return char_array;
}
void WriteADSyncLog(int rowNubmer, std::chrono::system_clock::time_point startOperationTimePoint, BSTR logFileName)
{
    if ( NULL == logFileName || logFileName == BSTR("") )
        return; 
    std::chrono::system_clock::time_point startLogTimePoint = std::chrono::system_clock::now();
    std::time_t startLogTime_t = std::chrono::system_clock::to_time_t(startLogTimePoint);
    double operationDuration = /*std::chrono::duration<double>(startOperationTimePoint.time_since_epoch()).count();*/(double)(std::chrono::duration_cast<std::chrono::milliseconds> ((startLogTimePoint - startOperationTimePoint)).count()) / (double)1000;

    std::ofstream osLogFile;
    osLogFile.open(bstr_to_str(logFileName).c_str(), std::ofstream::out | std::ofstream::app);
    if (0 == rowNubmer)
    {
        osLogFile << std::put_time(std::localtime(&startLogTime_t), "%F %T") << "; " << "WriteLoggingTime in LDAPQuery.cpp" << "; " << 0 << "; " << TotalLogDuration /1000 << std::endl;
    }
    else 
    {
        osLogFile << std::put_time(std::localtime(&startLogTime_t), "%F %T") << "; " << "LDAPQuery.cpp" << "; " << rowNubmer << "; " << operationDuration << std::endl;
    }
    osLogFile.close(); TotalLogDuration += /*std::chrono::duration<double>(startLogTimePoint.time_since_epoch()).count();*/(double)(std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - startLogTimePoint).count());
    return;
}
// CLDAPQuery
CLDAPQuery::CLDAPQuery() : m_maxConnectionId(0)
{
}

bool CertRoutine(PLDAP /*Connection*/, PCCERT_CONTEXT /*pServerCert*/)
{
	/*if (pServerCert)
		CertFreeCertificateContext(pServerCert);*/
	return true; //just skip all certificate errors
}

CComVariant getAttributeVariant(PBERVAL attr, const DataType type)
{
	CComVariant result;
	if(type == OctetString)
	{
		CComSafeArray<BYTE, VT_UI1> resArray(attr->bv_len);
		for(ULONG i=0; i < attr->bv_len; i++)
		{
			const BYTE val = (attr->bv_val[i]&0xFF);
			resArray.Add(sizeof(BYTE), &val, false);
		}
		result = resArray;
	}
	return result;
}

DataType getAttributeType(CString &type)
{
	DataType result;
	if(type == L"1.3.6.1.4.1.1466.115.121.1.40")
		result = OctetString;
	else
		result = DirectoryString;
	return result;
}

inline bool isWordChar(const WCHAR ch)
{
	return ((ch >= L'a' && ch <= L'z') || (ch >= L'A' && ch <= L'Z') || (ch >= L'0' && ch <= L'9'));
}

//function for getting a name pointer
PWCHAR wcs_schema_name(const PWCHAR str, const PWCHAR name)
{
	PWCHAR cp = const_cast<PWCHAR>(str);
	PWCHAR s1, s2;

	if (!*name)
		return const_cast<PWCHAR>(str);

	while (*cp)
	{
		if(*cp == L'\'' || *cp == L'"') //skip values
		{
			cp = wcschr(cp+1, *cp);
			if(!cp) break;
			cp++;
			continue;
		}

		s1 = cp;
		s2 = const_cast<PWCHAR>(name);

		while ( *s1 && *s2 && !(*s1-*s2) ) //compare with the name
			s1++, s2++;

		if (!*s2 && (!*s1 || !isWordChar(*s1)) && (str == cp || !isWordChar(*(cp-1)))) //checking if found and it's isolated
			return(cp);

		cp++;
	}
	return(NULL);
}

//function for getting a value pointer with moving start pointer
PWCHAR wcs_schema_value(PWCHAR &start)
{
	while(*start == L' ') start++;
	return (*start == L'\'' || *start == L'"') ? wcschr(++start, *(start-1)) : wcspbrk(start, L" {}()");
}

CAtlMap<CString, AttrSchema> *getAttributesSchema(LDAP *ld, PWCHAR schema)
{
	CAtlMap<CString, AttrSchema> *result = new CAtlMap<CString, AttrSchema>;

	CSimpleArray<PWCHAR> a;
	a.Add(L"attributeTypes");
	a.Add(NULL);
	LDAPMessage *pMsg = NULL;
	if(ldap_search_s(ld, schema, LDAP_SCOPE_BASE, L"(objectClass=*)", a.GetData(), 0, &pMsg) == LDAP_SUCCESS)
	{
		LDAPMessage* entry = ldap_first_entry(ld, pMsg);
		if(entry)
		{
			PWCHAR attr;
			BerElement* berElem = NULL;
			for(attr = ldap_first_attribute(ld, entry, &berElem); attr != NULL; attr=ldap_next_attribute(ld, entry, berElem))
			{
				PWCHAR *vals;
				if((vals = ldap_get_values(ld, entry, attr)) != NULL)
				{
					for(ULONG i=0; vals[i]!=NULL; i++)
					{
						PWCHAR start = wcs_schema_name(vals[i], L"SYNTAX"), end;
						if(start != NULL)
						{
							start += 6;
							end = wcs_schema_value(start);
							const size_t len = end?end-start:wcslen(start);
							CString type(start, static_cast<int>(len));
							AttrSchema attr;
							attr.type = getAttributeType(type);
							attr.single = (wcs_schema_name(start, L"SINGLE-VALUE") != NULL);

							start = wcs_schema_name(vals[i], L"NAME");
							if(start != NULL)
							{
								start += 4;
								while(*start == L' ') start++;
								PWCHAR close = (*start == L'(')?wcschr(++start, L')'):NULL; //if it is collection of names - find the end of it
								do
								{
									end = wcs_schema_value(start);
									const size_t len = end?end-start:close?close-start:wcslen(start);
									if(!len) break;
									CString name(start, static_cast<int>(len));
									result->SetAt(name, attr);
									start += len;
									if(*start == L'\'' || *start == L'"') start++;
									while(*start == L' ') start++;
								}
								while(start < close); //if it was collection - repeat to the end of collection
							}
						}
					}
					ldap_value_free(vals);
				}
				ldap_memfree(attr);
			}
		}
		ldap_msgfree(pMsg);
	}
	return result;
}

HRESULT STDMETHODCALLTYPE CLDAPQuery::execQuery(
	/* [in] */ BSTR username,
	/* [in] */ BSTR password,
	/* [in] */ BSTR host,
	/* [in] */ BSTR basedn,
	/* [in] */ BSTR scope,
	/* [in] */ BSTR filter,
	/* [in] */ BSTR attributes,
	/* [in] */ VARIANT_BOOL usessl,
    /* [in] */ BSTR logFileName,
	/* [retval][out] */ SAFEARRAY **result)
{
	LONG connectId = -1;
	connect(username, password, host, usessl, &connectId);
	if(connectId > -1)
	{
		query(connectId, basedn, scope, filter, attributes, logFileName, result);
		close(connectId);
	}
	return S_OK;
}

HRESULT STDMETHODCALLTYPE CLDAPQuery::getLastError(LONG* result)
{
	*result = m_errorCode;
	return S_OK;
}

STDMETHODIMP CLDAPQuery::connect(
	/* [in] */ BSTR username,
	/* [in] */ BSTR password,
	/* [in] */ BSTR host,
	/* [in] */ VARIANT_BOOL usessl,
	/* [retval][out] */ LONG *connect_id)
{
	m_errorCode = 0L;
	const ULONG no_limit = LDAP_NO_LIMIT;

	PLDAP ld = NULL;
	bool useSSL = (usessl==VARIANT_TRUE)? true : false;
	bool canFindFromRoot = true;
	CAtlMap<CString, AttrSchema> *attrsSchema = NULL;
	ULONG ulPort = useSSL?LDAP_SSL_PORT:LDAP_PORT;
	
	PWCHAR port = NULL;
	CString defaultNamingContext;
	CString csHost = host;
	CString hostname = wcstok_s(csHost.GetBuffer(), L":", &port);
	if(port && wcslen(port) > 0)
		ulPort = wcstol(port, NULL, 10);
	csHost.ReleaseBuffer();

	try
	{
		if (useSSL)
		{
			if((ld = ldap_sslinit(hostname.GetBuffer(), ulPort, 1))==NULL)
			{
				m_errorCode = LdapGetLastError();
				return S_FALSE;
			}

			m_errorCode = ldap_set_option(ld, LDAP_OPT_SERVER_CERTIFICATE, &CertRoutine);
			if (m_errorCode != LDAP_SUCCESS)
				throw L"error LDAP_OPT_SERVER_CERTIFICATE";
		}
		else
		{
			if((ld = ldap_init(hostname.GetBuffer(), ulPort))==NULL)
			{
				m_errorCode = LdapGetLastError();
				return S_FALSE;
			}
		}

		const ULONG version = LDAP_VERSION3;
		m_errorCode = ldap_set_option(ld, LDAP_OPT_PROTOCOL_VERSION, (void*)&version);
		if (m_errorCode != LDAP_SUCCESS)
			throw L"error LDAP_OPT_PROTOCOL_VERSION";

		m_errorCode = ldap_set_option(ld, LDAP_OPT_SIZELIMIT, (void*)&no_limit);
		if (m_errorCode != LDAP_SUCCESS)
			throw L"error LDAP_OPT_SIZELIMIT";
		ld->ld_sizelimit = no_limit;

		m_errorCode = ldap_set_option(ld, LDAP_OPT_TIMELIMIT, (void*)&no_limit);
		if (m_errorCode != LDAP_SUCCESS)
			throw L"error LDAP_OPT_TIMELIMIT";
		ld->ld_timelimit = no_limit;

		m_errorCode = ldap_connect(ld, 0);
		if (m_errorCode != LDAP_SUCCESS )
			throw L"error ldap_connect";

		m_errorCode = ldap_bind_s(ld, CString(username).GetBuffer(), CString(password).GetBuffer(), LDAP_AUTH_SIMPLE);
		if (m_errorCode != LDAP_SUCCESS)
			throw L"error LDAP_AUTH_SIMPLE";

		/* Get the RootDSE and BaseDN attribute (add checks on use this code).*/
		CSimpleArray<PWCHAR> a;
		a.Add(L"defaultNamingContext");
		a.Add(L"subschemaSubentry");
		a.Add(NULL);
		PLDAPMessage pBaseMsg = NULL;
		if(ldap_search_s(ld, L"", LDAP_SCOPE_BASE, L"(objectClass=*)", a.GetData(), 0, &pBaseMsg) == LDAP_SUCCESS)
		{
			PLDAPMessage entry = ldap_first_entry(ld, pBaseMsg);
			if(entry)
			{
				PWCHAR *pschema = ldap_get_values(ld, entry, L"subschemaSubentry");
				if(pschema)
				{
					attrsSchema = getAttributesSchema(ld, *pschema);
					ldap_value_free(pschema);
				}
				//try to find one item in subtree from root, if found it is Global Catalog search
				PLDAPSearch pPages = ldap_search_init_page(ld, NULL, LDAP_SCOPE_SUBTREE, L"(objectClass=*)", NULL, 0, NULL, NULL, no_limit, 1, NULL);
				if(pPages)
				{
					PLDAPMessage pMsg = NULL;
					canFindFromRoot = (ldap_get_next_page_s(ld, pPages, NULL, 1, NULL, &pMsg) == LDAP_SUCCESS);
					if(pMsg)
						ldap_msgfree(pMsg);
					ldap_search_abandon_page(ld, pPages);
				}
				PWCHAR *pDefaultNamingContext = ldap_get_values(ld, entry, L"defaultNamingContext");
				if(pDefaultNamingContext)
				{
					defaultNamingContext = *pDefaultNamingContext;
					ldap_value_free(pDefaultNamingContext);
				}
			}
			ldap_msgfree(pBaseMsg);
		}

		ConnectInfo *cinfo = new ConnectInfo;
		cinfo->ld = ld;
		cinfo->attrsSchema = attrsSchema;
		cinfo->defaultNamingContext = defaultNamingContext;
		cinfo->canFindFromRoot = canFindFromRoot;

		*connect_id = ++m_maxConnectionId;
		m_connections.Add(*connect_id, cinfo);
	}
	catch (PWCHAR /*e*/)
	{
		if (ld)
			ldap_unbind_s(ld);
		*connect_id = -1;
		return S_FALSE;
	}
	
	return S_OK;
}

STDMETHODIMP CLDAPQuery::close(
	/* [in] */ LONG connect_id)
{
	m_errorCode = 0L;
	return m_connections.Remove(connect_id) ? S_OK : S_FALSE;
}

STDMETHODIMP CLDAPQuery::query(
	/* [in] */ LONG connect_id,
	/* [in] */ BSTR basedn,
	/* [in] */ BSTR scope,
	/* [in] */ BSTR filter,
	/* [in] */ BSTR attributes,
    /* [in] */ BSTR logFileName,
	/* [retval][out] */ SAFEARRAY **result)
{
    
	LONG results_id = -1L;
	pageQuery(connect_id, basedn, scope, filter, attributes, LDAP_NO_LIMIT, &results_id);
    //std::ofstream myfile;
    //myfile.open("C:\\tmp\\Sync Timer.txt", std::ofstream::out | std::ofstream::app);
    //myfile << " StartSync Iteration: \r\n";
    //myfile << " filters: " << bstr_to_str(filter).c_str() << "\r\n";
    //myfile << " attributes: " << bstr_to_str(attributes).c_str() << "\r\n";
    //
    //myfile.close();
	if(results_id > -1L)
	{
		nextResults(results_id, LDAP_NO_LIMIT, logFileName, result);
	}
	return S_OK;
}

STDMETHODIMP CLDAPQuery::pageQuery(
	/* [in] */ LONG connect_id,
	/* [in] */ BSTR basedn,
	/* [in] */ BSTR scope,
	/* [in] */ BSTR filter,
	/* [in] */ BSTR attributes,
	/* [in] */ ULONG total_results,
	/* [retval][out] */ LONG* results_id)
{
	m_errorCode = 0L;
	const int id = m_connections.FindKey(connect_id);
	if(id > -1)
	{
		ConnectInfo *cinfo = m_connections.GetValueAt(id);

		const PLDAP ld = cinfo->ld;

		const ULONG no_limit = LDAP_NO_LIMIT;
		ULONG ulScope = LDAP_SCOPE_BASE;
		CString csScope = scope;
		csScope.MakeLower();
		if(csScope == _T("subtree"))
			ulScope = LDAP_SCOPE_SUBTREE;
		else if(csScope == _T("onelevel"))
			ulScope = LDAP_SCOPE_ONELEVEL;
		else if(csScope == _T("base"))
			ulScope = LDAP_SCOPE_BASE;

		CString csBaseDN = basedn;

		if(csBaseDN.IsEmpty() && (ulScope == LDAP_SCOPE_ONELEVEL || (!cinfo->canFindFromRoot && ulScope == LDAP_SCOPE_SUBTREE)))
		{
			csBaseDN = cinfo->defaultNamingContext;
		}

		CSimpleArray<PWCHAR> attributesArr;
		PWCHAR nextAttributes = NULL;
		CString csArrs = attributes;
		PWCHAR attribute = wcstok_s(csArrs.GetBuffer(), L",", &nextAttributes);
		while(attribute != NULL)
		{
			attributesArr.Add(attribute);
			attribute = wcstok_s(NULL, L",", &nextAttributes);
		}
		csArrs.ReleaseBuffer();
		attributesArr.Add(NULL); // NULL-terminated array

		PLDAPSearch pPages = ldap_search_init_page(ld, csBaseDN.GetBuffer(), ulScope, CString(filter).GetBuffer(), attributesArr.GetData(), 0, NULL, NULL, no_limit, total_results, NULL);
		m_errorCode = LdapGetLastError();
		if(pPages)
		{
			*results_id = cinfo->addResult(pPages);
		}
	}
	return S_OK;
}

STDMETHODIMP CLDAPQuery::nextResults(
	/* [in] */ LONG results_id,
	/* [in] */ ULONG num_results,
    /* [in] */ BSTR logFileName,
	/* [retval][out] */ SAFEARRAY** result)
{
	CComSafeArray<VARIANT> dnList(0UL);

	ConnectInfo *cinfo = NULL;
	ResultsInfo *presinfo = NULL;
	m_errorCode = 0L;
	for(int i = 0; i < m_connections.GetSize(); i++)
	{
		ConnectInfo* currentConnectInfo = m_connections.GetValueAt(i);
		if(currentConnectInfo)
		{
			const int id = currentConnectInfo->results.FindKey(results_id);
			if(id > -1)
			{
				cinfo = currentConnectInfo;
				presinfo = currentConnectInfo->results.GetValueAt(id);
				break;
			}
		}
	}
	if(cinfo && presinfo && presinfo->pages)
	{
		const PLDAP ld = cinfo->ld;
		const PLDAPSearch pPages = presinfo->pages;
		PLDAPMessage pMsg = NULL;
		const BOOL allResults = (num_results == 0UL);
        std::chrono::system_clock::time_point logTimePoint_1 = std::chrono::system_clock::now();
		while ((allResults || num_results > 0UL) &&
			(m_errorCode = ldap_get_next_page_s(ld, pPages, NULL, (num_results < 100UL && num_results > 0UL ? num_results : 100UL), NULL, &pMsg)) == LDAP_SUCCESS) //not more the 1k results per query
		{
            std::chrono::system_clock::time_point logTimePoint_2 = std::chrono::system_clock::now();
			if(pMsg)
			{
				//myfile << chrono::duration_cast<chrono::milliseconds> (chrono::system_clock::now() - start).count() << "ms ldap_get_next_page_s \r\n";
				//start = chrono::system_clock::now();
				int entryIt = 0;
                std::chrono::system_clock::time_point logTimePoint_3 = std::chrono::system_clock::now();
				for (PLDAPMessage entry = ldap_first_entry(ld, pMsg); entry != NULL && (allResults || num_results-- > 0UL); entry = ldap_next_entry(ld, entry))
				{
                    std::chrono::system_clock::time_point logTimePoint_4 = std::chrono::system_clock::now();
					CComObject<CAssocObject> *obj;
					CComObject<CAssocObject>::CreateInstance(&obj);

					const PWCHAR dn = ldap_get_dn(ld, entry);
					obj->AddValue(L"dn", CComVariant(dn));

					PWCHAR attr;
					BerElement* berElem = NULL;
					for(attr = ldap_first_attribute(ld, entry, &berElem); attr != NULL; attr = ldap_next_attribute(ld, entry, berElem))
					{
                        std::chrono::system_clock::time_point logTimePoint_5 = std::chrono::system_clock::now();
						bool single = false;
						const CAtlMap<CString, AttrSchema>::CPair *pPair = cinfo->attrsSchema->Lookup(attr);
                        
						if(pPair)
						{
                            std::chrono::system_clock::time_point logTimePoint_6 = std::chrono::system_clock::now();
							const AttrSchema &schema = pPair->m_value;
							single = schema.single;
							if(schema.type == OctetString)
							{
								PBERVAL *vals;
                                std::chrono::system_clock::time_point logTimePoint_7 = std::chrono::system_clock::now();
								if((vals=ldap_get_values_len(ld, entry, attr)) != NULL)
								{
									if(!single)
									{
                                        std::chrono::system_clock::time_point logTimePoint_8 = std::chrono::system_clock::now();
										CComSafeArray<VARIANT> attrValues(ldap_count_values_len(vals));
										for(LONG i=0L; vals[i]!=NULL; i++)
										{
                                            std::chrono::system_clock::time_point logTimePoint_9 = std::chrono::system_clock::now();
											attrValues[i] = getAttributeVariant(vals[i], schema.type);
                                            WriteADSyncLog(511, logTimePoint_9, logFileName);
										}
										obj->AddValue(attr, CComVariant(attrValues));
                                        WriteADSyncLog(507, logTimePoint_8, logFileName);
									}
									else
									{
                                        std::chrono::system_clock::time_point logTimePoint_8 = std::chrono::system_clock::now();
										obj->AddValue(attr, getAttributeVariant(vals[0], schema.type));
                                        WriteADSyncLog(520, logTimePoint_8, logFileName);
									}
									ldap_value_free_len(vals);
								}
								ldap_memfree(attr);
                                WriteADSyncLog(503, logTimePoint_7, logFileName);
								continue;
							}
                            WriteADSyncLog(496, logTimePoint_6, logFileName);
						}
						
                        ////////////////////////////////////////////////////
                        // loading attribute's values by "sampling_step" count
                        PWCHAR *vals = 0;
                        BOOL lastPage = FALSE;
                        LONG start = 0L;
                        LONG sampling_step = 999L;
                        ULONG ldaplastErr = 0UL;
                        CString ldaplastErrStr = ldap_err2string(ldaplastErr);
                        ULONG countValues;
                        CComSafeArray<VARIANT> attrValues(0UL);
                        //do
                        {
                            ////////////////////////////////////////////
                            // add range to filter
                            CStringW attrWithRange;
                            //if (lastPage)
                            //    attrWithRange.Format(L"%s;range=%ld-*", attr, start);
                            //else
                            //    attrWithRange.Format(L"%s;range=%ld-%ld", attr, start, start + sampling_step);

                            //////////////////////////////////////////////////////
                            // accumulate current range to attrValues safe array
                            vals = ldap_get_values(ld, entry, attr/* attrWithRange.GetBuffer()*/);
                            countValues = ldap_count_values(vals);

                            if (countValues > 0UL)
                            {
                                if (!single)
                                {
                                    CComSafeArray<VARIANT> attrValues_currentRange(countValues);
                                    for (LONG i = 0L; i < countValues; i++)
                                    {
                                        if (NULL != vals[i])
                                        {
                                            attrValues_currentRange[i] = CComVariant(vals[i]);
                                        }
                                    }
                                    ///////////////////////////////////////////////
                                    // dont load, if only one empty value of attr
                                    if ( ! (1 == countValues && NULL == vals[0]) )
                                    {
                                        attrValues.Add(attrValues_currentRange);
                                    }
                                    start += sampling_step;
                                }
                                else
                                {
                                    attrValues.Add(CComVariant(vals[0]));
                                    start = -1L; //just leave. "single" attribute have only one value
                                }
                            }
                            else
                            {
                                if (lastPage) start = -1L; //leave loop if already last
                                lastPage = TRUE;
                            }
                            ldap_value_free(vals);
                        } //while (start >= 0L);
                        std::chrono::system_clock::time_point logTimePoint_6 = std::chrono::system_clock::now();
                        if (0 != attrValues.GetCount())
                        {
                            obj->AddValue(attr, CComVariant(attrValues));
                        }
						ldap_memfree(attr);
                        WriteADSyncLog(591, logTimePoint_6, logFileName);
                        WriteADSyncLog(490, logTimePoint_5, logFileName);
					}
					dnList.Add(CComVariant(obj));
					ldap_memfree(dn);
                    WriteADSyncLog(479, logTimePoint_4, logFileName);
				}
                WriteADSyncLog(476, logTimePoint_3, logFileName);
				ldap_msgfree(pMsg);
				//myfile << chrono::duration_cast<chrono::milliseconds> (chrono::system_clock::now() - start).count() << "ms body cycle \r\n";
				//start = chrono::system_clock::now();
			}
            WriteADSyncLog(471, logTimePoint_2, logFileName);
		}
        WriteADSyncLog(467, logTimePoint_1, logFileName);
		if(m_errorCode != LDAP_SUCCESS && m_errorCode != LDAP_SIZELIMIT_EXCEEDED)
		{
            logTimePoint_1 = std::chrono::system_clock::now();
			ldap_search_abandon_page(ld, pPages);
            WriteADSyncLog(615, logTimePoint_1, logFileName);
			presinfo->pages = NULL;
		}
		if(m_errorCode == LDAP_NO_RESULTS_RETURNED || m_errorCode == LDAP_MORE_RESULTS_TO_RETURN || m_errorCode == LDAP_SIZELIMIT_EXCEEDED)
			m_errorCode = LDAP_SUCCESS;
        //std::chrono::system_clock::time_point endSyncTimePoint = std::chrono::system_clock::now();
        //std::ofstream myfile;
        //myfile.open("C:\\tmp\\Sync Timer.txt",std::ofstream::out | std::ofstream::app);
        //myfile << std::chrono::duration_cast<std::chrono::milliseconds> (endSyncTimePoint - startSyncTimePoint).count() << " ms for SyncAD \r\n";
        //myfile.close();
	}
	*result = dnList.Detach();
    WriteADSyncLog(0, std::chrono::system_clock::now(), logFileName);
	return S_OK;
}

STDMETHODIMP CLDAPQuery::dn2ufn(
	/* [in] */ BSTR dn,
	/* [retval][out] */ BSTR *ufn)
{
	const PTCHAR pUfn = ldap_dn2ufn(CComBSTR(dn));
	m_errorCode = LdapGetLastError();
	if(pUfn)
	{
		*ufn = CComBSTR(pUfn).Detach();
		ldap_memfree(pUfn);
	}
	return S_OK;
}

STDMETHODIMP CLDAPQuery::ufn2dn(
	/* [in] */ BSTR ufn,
	/* [retval][out] */ BSTR *dn)
{
	PTCHAR pDn = NULL;
	m_errorCode = ldap_ufn2dn(CComBSTR(ufn), &pDn);
	if(pDn)
	{
		*dn = CComBSTR(pDn).Detach();
		ldap_memfree(pDn);
	}
	return S_OK;
}

STDMETHODIMP CLDAPQuery::err2string(
	/* [in] */ ULONG ufn,
	/* [retval][out] */ BSTR *string)
{
	const PTCHAR pStr = ldap_err2string(ufn);
	m_errorCode = LdapGetLastError();
	if(pStr)
	{
		*string = CComBSTR(pStr).Detach();
		ldap_memfree(pStr);
	}
	return S_OK;
}

STDMETHODIMP CLDAPQuery::explodeDn(
	/* [in] */ BSTR str,
	/* [in] */ ULONG notypes,
	/* [retval][out] */ SAFEARRAY **result)
{
	PTCHAR *vals = ldap_explode_dn(CComBSTR(str), notypes);
	m_errorCode = LdapGetLastError();
	if(vals)
	{
		CComSafeArray<VARIANT> dnValues;
		for(LONG i=0L; vals[i]!=NULL; i++)
		{
			dnValues.Add(CComVariant(vals[i]));
		}
		ldap_value_free(vals);
		*result = dnValues.Detach();
	}
	return S_OK;
}

STDMETHODIMP CLDAPQuery::rename(
	/* [in] */ LONG connect_id,
	/* [in] */ BSTR dn,
	/* [in] */ BSTR newRDN,
	/* [in] */ BSTR newParent,
	/* [in] */ BOOL deleteOldRdn)
{
	m_errorCode = 0L;
	const int id = m_connections.FindKey(connect_id);
	if(id > -1)
	{
		const ConnectInfo *cinfo = m_connections.GetValueAt(id);

		ldap_rename(cinfo->ld, CComBSTR(dn), CComBSTR(newRDN), CComBSTR(newParent), deleteOldRdn ? 1 : 0, NULL, NULL, NULL);
		m_errorCode = LdapGetLastError();
	}
	return S_OK;
}
