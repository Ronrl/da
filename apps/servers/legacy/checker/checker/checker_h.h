

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 8.00.0603 */
/* at Fri May 13 03:21:40 2016
 */
/* Compiler settings for checker.idl:
    Oicf, W1, Zp8, env=Win64 (32b run), target_arch=AMD64 8.00.0603 
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __checker_h_h__
#define __checker_h_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __IAlertXML_FWD_DEFINED__
#define __IAlertXML_FWD_DEFINED__
typedef interface IAlertXML IAlertXML;

#endif 	/* __IAlertXML_FWD_DEFINED__ */


#ifndef __IAlertXML2_FWD_DEFINED__
#define __IAlertXML2_FWD_DEFINED__
typedef interface IAlertXML2 IAlertXML2;

#endif 	/* __IAlertXML2_FWD_DEFINED__ */


#ifndef __IAlertXML3_FWD_DEFINED__
#define __IAlertXML3_FWD_DEFINED__
typedef interface IAlertXML3 IAlertXML3;

#endif 	/* __IAlertXML3_FWD_DEFINED__ */


#ifndef __IAlertXML4_FWD_DEFINED__
#define __IAlertXML4_FWD_DEFINED__
typedef interface IAlertXML4 IAlertXML4;

#endif 	/* __IAlertXML4_FWD_DEFINED__ */


#ifndef __IEncrypt_FWD_DEFINED__
#define __IEncrypt_FWD_DEFINED__
typedef interface IEncrypt IEncrypt;

#endif 	/* __IEncrypt_FWD_DEFINED__ */


#ifndef __IEncrypt2_FWD_DEFINED__
#define __IEncrypt2_FWD_DEFINED__
typedef interface IEncrypt2 IEncrypt2;

#endif 	/* __IEncrypt2_FWD_DEFINED__ */


#ifndef __IEncrypt3_FWD_DEFINED__
#define __IEncrypt3_FWD_DEFINED__
typedef interface IEncrypt3 IEncrypt3;

#endif 	/* __IEncrypt3_FWD_DEFINED__ */


#ifndef __ILicensing_FWD_DEFINED__
#define __ILicensing_FWD_DEFINED__
typedef interface ILicensing ILicensing;

#endif 	/* __ILicensing_FWD_DEFINED__ */


#ifndef __ILicensing2_FWD_DEFINED__
#define __ILicensing2_FWD_DEFINED__
typedef interface ILicensing2 ILicensing2;

#endif 	/* __ILicensing2_FWD_DEFINED__ */


#ifndef __ILDAPQuery_FWD_DEFINED__
#define __ILDAPQuery_FWD_DEFINED__
typedef interface ILDAPQuery ILDAPQuery;

#endif 	/* __ILDAPQuery_FWD_DEFINED__ */


#ifndef __ILDAPQuery2_FWD_DEFINED__
#define __ILDAPQuery2_FWD_DEFINED__
typedef interface ILDAPQuery2 ILDAPQuery2;

#endif 	/* __ILDAPQuery2_FWD_DEFINED__ */


#ifndef __AlertXML_FWD_DEFINED__
#define __AlertXML_FWD_DEFINED__

#ifdef __cplusplus
typedef class AlertXML AlertXML;
#else
typedef struct AlertXML AlertXML;
#endif /* __cplusplus */

#endif 	/* __AlertXML_FWD_DEFINED__ */


#ifndef __Encrypt_FWD_DEFINED__
#define __Encrypt_FWD_DEFINED__

#ifdef __cplusplus
typedef class Encrypt Encrypt;
#else
typedef struct Encrypt Encrypt;
#endif /* __cplusplus */

#endif 	/* __Encrypt_FWD_DEFINED__ */


#ifndef __Licensing_FWD_DEFINED__
#define __Licensing_FWD_DEFINED__

#ifdef __cplusplus
typedef class Licensing Licensing;
#else
typedef struct Licensing Licensing;
#endif /* __cplusplus */

#endif 	/* __Licensing_FWD_DEFINED__ */


#ifndef __LDAPQuery_FWD_DEFINED__
#define __LDAPQuery_FWD_DEFINED__

#ifdef __cplusplus
typedef class LDAPQuery LDAPQuery;
#else
typedef struct LDAPQuery LDAPQuery;
#endif /* __cplusplus */

#endif 	/* __LDAPQuery_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"

#ifdef __cplusplus
extern "C"{
#endif 


#ifndef __IAlertXML_INTERFACE_DEFINED__
#define __IAlertXML_INTERFACE_DEFINED__

/* interface IAlertXML */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IAlertXML;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("3E624124-FDF6-4B11-AA32-08EC7EC48AC7")
    IAlertXML : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE setUserId( 
            /* [in] */ IDispatch *connection,
            /* [in] */ LONG id) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE setTimeout( 
            /* [in] */ LONG timeout) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE setPlugin( 
            /* [in] */ BSTR plugin) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE setAlertURL( 
            /* [in] */ BSTR url) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE setSurveyURL( 
            /* [in] */ BSTR url) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE setDeskbarId( 
            /* [in] */ BSTR id) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE addAlert( 
            /* [in] */ BOOL urgent,
            /* [in] */ LONG id,
            /* [in] */ LONG aknown,
            /* [in] */ LONG autoclose,
            /* [in] */ LONG create_timestamp,
            /* [in] */ BSTR title,
            /* [in] */ BSTR position,
            /* [in] */ BSTR width,
            /* [in] */ BSTR height,
            /* [in] */ LONG ticker,
            /* [in] */ LONG schedule,
            /* [in] */ BOOL history) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE addCustomAlert( 
            /* [in] */ BOOL urgent,
            /* [in] */ BSTR href,
            /* [in] */ LONG id,
            /* [in] */ LONG aknown,
            /* [in] */ LONG autoclose,
            /* [in] */ LONG create_timestamp,
            /* [in] */ BSTR title,
            /* [in] */ BSTR position,
            /* [in] */ BSTR width,
            /* [in] */ BSTR height,
            /* [in] */ LONG ticker,
            /* [in] */ LONG schedule,
            /* [in] */ BOOL history) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE addSurvey( 
            /* [in] */ BOOL urgent,
            /* [in] */ LONG id,
            /* [in] */ LONG create_timestamp,
            /* [in] */ BSTR title,
            /* [in] */ LONG schedule,
            /* [in] */ BOOL history) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE getResult( 
            /* [retval][out] */ BSTR *result) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE getUTF8Result( 
            /* [retval][out] */ SAFEARRAY * *result) = 0;
        
    };
    
    
#else 	/* C style interface */

    typedef struct IAlertXMLVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IAlertXML * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IAlertXML * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IAlertXML * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IAlertXML * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IAlertXML * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IAlertXML * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IAlertXML * This,
            /* [annotation][in] */ 
            _In_  DISPID dispIdMember,
            /* [annotation][in] */ 
            _In_  REFIID riid,
            /* [annotation][in] */ 
            _In_  LCID lcid,
            /* [annotation][in] */ 
            _In_  WORD wFlags,
            /* [annotation][out][in] */ 
            _In_  DISPPARAMS *pDispParams,
            /* [annotation][out] */ 
            _Out_opt_  VARIANT *pVarResult,
            /* [annotation][out] */ 
            _Out_opt_  EXCEPINFO *pExcepInfo,
            /* [annotation][out] */ 
            _Out_opt_  UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *setUserId )( 
            IAlertXML * This,
            /* [in] */ IDispatch *connection,
            /* [in] */ LONG id);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *setTimeout )( 
            IAlertXML * This,
            /* [in] */ LONG timeout);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *setPlugin )( 
            IAlertXML * This,
            /* [in] */ BSTR plugin);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *setAlertURL )( 
            IAlertXML * This,
            /* [in] */ BSTR url);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *setSurveyURL )( 
            IAlertXML * This,
            /* [in] */ BSTR url);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *setDeskbarId )( 
            IAlertXML * This,
            /* [in] */ BSTR id);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *addAlert )( 
            IAlertXML * This,
            /* [in] */ BOOL urgent,
            /* [in] */ LONG id,
            /* [in] */ LONG aknown,
            /* [in] */ LONG autoclose,
            /* [in] */ LONG create_timestamp,
            /* [in] */ BSTR title,
            /* [in] */ BSTR position,
            /* [in] */ BSTR width,
            /* [in] */ BSTR height,
            /* [in] */ LONG ticker,
            /* [in] */ LONG schedule,
            /* [in] */ BOOL history);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *addCustomAlert )( 
            IAlertXML * This,
            /* [in] */ BOOL urgent,
            /* [in] */ BSTR href,
            /* [in] */ LONG id,
            /* [in] */ LONG aknown,
            /* [in] */ LONG autoclose,
            /* [in] */ LONG create_timestamp,
            /* [in] */ BSTR title,
            /* [in] */ BSTR position,
            /* [in] */ BSTR width,
            /* [in] */ BSTR height,
            /* [in] */ LONG ticker,
            /* [in] */ LONG schedule,
            /* [in] */ BOOL history);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *addSurvey )( 
            IAlertXML * This,
            /* [in] */ BOOL urgent,
            /* [in] */ LONG id,
            /* [in] */ LONG create_timestamp,
            /* [in] */ BSTR title,
            /* [in] */ LONG schedule,
            /* [in] */ BOOL history);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *getResult )( 
            IAlertXML * This,
            /* [retval][out] */ BSTR *result);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *getUTF8Result )( 
            IAlertXML * This,
            /* [retval][out] */ SAFEARRAY * *result);
        
        END_INTERFACE
    } IAlertXMLVtbl;

    interface IAlertXML
    {
        CONST_VTBL struct IAlertXMLVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IAlertXML_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IAlertXML_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IAlertXML_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IAlertXML_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IAlertXML_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IAlertXML_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IAlertXML_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IAlertXML_setUserId(This,connection,id)	\
    ( (This)->lpVtbl -> setUserId(This,connection,id) ) 

#define IAlertXML_setTimeout(This,timeout)	\
    ( (This)->lpVtbl -> setTimeout(This,timeout) ) 

#define IAlertXML_setPlugin(This,plugin)	\
    ( (This)->lpVtbl -> setPlugin(This,plugin) ) 

#define IAlertXML_setAlertURL(This,url)	\
    ( (This)->lpVtbl -> setAlertURL(This,url) ) 

#define IAlertXML_setSurveyURL(This,url)	\
    ( (This)->lpVtbl -> setSurveyURL(This,url) ) 

#define IAlertXML_setDeskbarId(This,id)	\
    ( (This)->lpVtbl -> setDeskbarId(This,id) ) 

#define IAlertXML_addAlert(This,urgent,id,aknown,autoclose,create_timestamp,title,position,width,height,ticker,schedule,history)	\
    ( (This)->lpVtbl -> addAlert(This,urgent,id,aknown,autoclose,create_timestamp,title,position,width,height,ticker,schedule,history) ) 

#define IAlertXML_addCustomAlert(This,urgent,href,id,aknown,autoclose,create_timestamp,title,position,width,height,ticker,schedule,history)	\
    ( (This)->lpVtbl -> addCustomAlert(This,urgent,href,id,aknown,autoclose,create_timestamp,title,position,width,height,ticker,schedule,history) ) 

#define IAlertXML_addSurvey(This,urgent,id,create_timestamp,title,schedule,history)	\
    ( (This)->lpVtbl -> addSurvey(This,urgent,id,create_timestamp,title,schedule,history) ) 

#define IAlertXML_getResult(This,result)	\
    ( (This)->lpVtbl -> getResult(This,result) ) 

#define IAlertXML_getUTF8Result(This,result)	\
    ( (This)->lpVtbl -> getUTF8Result(This,result) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IAlertXML_INTERFACE_DEFINED__ */


#ifndef __IAlertXML2_INTERFACE_DEFINED__
#define __IAlertXML2_INTERFACE_DEFINED__

/* interface IAlertXML2 */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IAlertXML2;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("8DD9C28B-DD6B-4805-8B35-0731BC457DBD")
    IAlertXML2 : public IAlertXML
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE setUserInfo( 
            /* [in] */ IDispatch *connection,
            /* [in] */ LONG id,
            /* [in] */ BSTR name,
            /* [in] */ BSTR hash,
            /* [retval][out] */ BOOL *result) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE addAlert2( 
            /* [in] */ BOOL urgent,
            /* [in] */ LONG id,
            /* [in] */ LONG aknown,
            /* [in] */ LONG autoclose,
            /* [in] */ LONG create_timestamp,
            /* [in] */ BSTR title,
            /* [in] */ BSTR position,
            /* [in] */ BSTR visible,
            /* [in] */ BSTR width,
            /* [in] */ BSTR height,
            /* [in] */ LONG ticker,
            /* [in] */ LONG schedule,
            /* [in] */ LONG recurrence,
            /* [in] */ BOOL history,
            /* [in] */ BSTR innner,
            /* [retval][out] */ BOOL *result) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE addCustomAlert2( 
            /* [in] */ BOOL urgent,
            /* [in] */ BSTR href,
            /* [in] */ LONG id,
            /* [in] */ LONG aknown,
            /* [in] */ LONG autoclose,
            /* [in] */ LONG create_timestamp,
            /* [in] */ BSTR title,
            /* [in] */ BSTR position,
            /* [in] */ BSTR visible,
            /* [in] */ BSTR width,
            /* [in] */ BSTR height,
            /* [in] */ LONG ticker,
            /* [in] */ LONG schedule,
            /* [in] */ LONG recurrence,
            /* [in] */ BOOL history,
            /* [in] */ BSTR innner,
            /* [retval][out] */ BOOL *result) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE addSurvey2( 
            /* [in] */ BOOL urgent,
            /* [in] */ LONG id,
            /* [in] */ LONG create_timestamp,
            /* [in] */ BSTR title,
            /* [in] */ BSTR position,
            /* [in] */ BSTR visible,
            /* [in] */ BSTR width,
            /* [in] */ BSTR height,
            /* [in] */ LONG schedule,
            /* [in] */ LONG recurrence,
            /* [in] */ BOOL history,
            /* [in] */ BSTR innner,
            /* [retval][out] */ BOOL *result) = 0;
        
    };
    
    
#else 	/* C style interface */

    typedef struct IAlertXML2Vtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IAlertXML2 * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IAlertXML2 * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IAlertXML2 * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IAlertXML2 * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IAlertXML2 * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IAlertXML2 * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IAlertXML2 * This,
            /* [annotation][in] */ 
            _In_  DISPID dispIdMember,
            /* [annotation][in] */ 
            _In_  REFIID riid,
            /* [annotation][in] */ 
            _In_  LCID lcid,
            /* [annotation][in] */ 
            _In_  WORD wFlags,
            /* [annotation][out][in] */ 
            _In_  DISPPARAMS *pDispParams,
            /* [annotation][out] */ 
            _Out_opt_  VARIANT *pVarResult,
            /* [annotation][out] */ 
            _Out_opt_  EXCEPINFO *pExcepInfo,
            /* [annotation][out] */ 
            _Out_opt_  UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *setUserId )( 
            IAlertXML2 * This,
            /* [in] */ IDispatch *connection,
            /* [in] */ LONG id);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *setTimeout )( 
            IAlertXML2 * This,
            /* [in] */ LONG timeout);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *setPlugin )( 
            IAlertXML2 * This,
            /* [in] */ BSTR plugin);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *setAlertURL )( 
            IAlertXML2 * This,
            /* [in] */ BSTR url);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *setSurveyURL )( 
            IAlertXML2 * This,
            /* [in] */ BSTR url);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *setDeskbarId )( 
            IAlertXML2 * This,
            /* [in] */ BSTR id);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *addAlert )( 
            IAlertXML2 * This,
            /* [in] */ BOOL urgent,
            /* [in] */ LONG id,
            /* [in] */ LONG aknown,
            /* [in] */ LONG autoclose,
            /* [in] */ LONG create_timestamp,
            /* [in] */ BSTR title,
            /* [in] */ BSTR position,
            /* [in] */ BSTR width,
            /* [in] */ BSTR height,
            /* [in] */ LONG ticker,
            /* [in] */ LONG schedule,
            /* [in] */ BOOL history);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *addCustomAlert )( 
            IAlertXML2 * This,
            /* [in] */ BOOL urgent,
            /* [in] */ BSTR href,
            /* [in] */ LONG id,
            /* [in] */ LONG aknown,
            /* [in] */ LONG autoclose,
            /* [in] */ LONG create_timestamp,
            /* [in] */ BSTR title,
            /* [in] */ BSTR position,
            /* [in] */ BSTR width,
            /* [in] */ BSTR height,
            /* [in] */ LONG ticker,
            /* [in] */ LONG schedule,
            /* [in] */ BOOL history);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *addSurvey )( 
            IAlertXML2 * This,
            /* [in] */ BOOL urgent,
            /* [in] */ LONG id,
            /* [in] */ LONG create_timestamp,
            /* [in] */ BSTR title,
            /* [in] */ LONG schedule,
            /* [in] */ BOOL history);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *getResult )( 
            IAlertXML2 * This,
            /* [retval][out] */ BSTR *result);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *getUTF8Result )( 
            IAlertXML2 * This,
            /* [retval][out] */ SAFEARRAY * *result);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *setUserInfo )( 
            IAlertXML2 * This,
            /* [in] */ IDispatch *connection,
            /* [in] */ LONG id,
            /* [in] */ BSTR name,
            /* [in] */ BSTR hash,
            /* [retval][out] */ BOOL *result);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *addAlert2 )( 
            IAlertXML2 * This,
            /* [in] */ BOOL urgent,
            /* [in] */ LONG id,
            /* [in] */ LONG aknown,
            /* [in] */ LONG autoclose,
            /* [in] */ LONG create_timestamp,
            /* [in] */ BSTR title,
            /* [in] */ BSTR position,
            /* [in] */ BSTR visible,
            /* [in] */ BSTR width,
            /* [in] */ BSTR height,
            /* [in] */ LONG ticker,
            /* [in] */ LONG schedule,
            /* [in] */ LONG recurrence,
            /* [in] */ BOOL history,
            /* [in] */ BSTR innner,
            /* [retval][out] */ BOOL *result);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *addCustomAlert2 )( 
            IAlertXML2 * This,
            /* [in] */ BOOL urgent,
            /* [in] */ BSTR href,
            /* [in] */ LONG id,
            /* [in] */ LONG aknown,
            /* [in] */ LONG autoclose,
            /* [in] */ LONG create_timestamp,
            /* [in] */ BSTR title,
            /* [in] */ BSTR position,
            /* [in] */ BSTR visible,
            /* [in] */ BSTR width,
            /* [in] */ BSTR height,
            /* [in] */ LONG ticker,
            /* [in] */ LONG schedule,
            /* [in] */ LONG recurrence,
            /* [in] */ BOOL history,
            /* [in] */ BSTR innner,
            /* [retval][out] */ BOOL *result);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *addSurvey2 )( 
            IAlertXML2 * This,
            /* [in] */ BOOL urgent,
            /* [in] */ LONG id,
            /* [in] */ LONG create_timestamp,
            /* [in] */ BSTR title,
            /* [in] */ BSTR position,
            /* [in] */ BSTR visible,
            /* [in] */ BSTR width,
            /* [in] */ BSTR height,
            /* [in] */ LONG schedule,
            /* [in] */ LONG recurrence,
            /* [in] */ BOOL history,
            /* [in] */ BSTR innner,
            /* [retval][out] */ BOOL *result);
        
        END_INTERFACE
    } IAlertXML2Vtbl;

    interface IAlertXML2
    {
        CONST_VTBL struct IAlertXML2Vtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IAlertXML2_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IAlertXML2_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IAlertXML2_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IAlertXML2_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IAlertXML2_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IAlertXML2_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IAlertXML2_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IAlertXML2_setUserId(This,connection,id)	\
    ( (This)->lpVtbl -> setUserId(This,connection,id) ) 

#define IAlertXML2_setTimeout(This,timeout)	\
    ( (This)->lpVtbl -> setTimeout(This,timeout) ) 

#define IAlertXML2_setPlugin(This,plugin)	\
    ( (This)->lpVtbl -> setPlugin(This,plugin) ) 

#define IAlertXML2_setAlertURL(This,url)	\
    ( (This)->lpVtbl -> setAlertURL(This,url) ) 

#define IAlertXML2_setSurveyURL(This,url)	\
    ( (This)->lpVtbl -> setSurveyURL(This,url) ) 

#define IAlertXML2_setDeskbarId(This,id)	\
    ( (This)->lpVtbl -> setDeskbarId(This,id) ) 

#define IAlertXML2_addAlert(This,urgent,id,aknown,autoclose,create_timestamp,title,position,width,height,ticker,schedule,history)	\
    ( (This)->lpVtbl -> addAlert(This,urgent,id,aknown,autoclose,create_timestamp,title,position,width,height,ticker,schedule,history) ) 

#define IAlertXML2_addCustomAlert(This,urgent,href,id,aknown,autoclose,create_timestamp,title,position,width,height,ticker,schedule,history)	\
    ( (This)->lpVtbl -> addCustomAlert(This,urgent,href,id,aknown,autoclose,create_timestamp,title,position,width,height,ticker,schedule,history) ) 

#define IAlertXML2_addSurvey(This,urgent,id,create_timestamp,title,schedule,history)	\
    ( (This)->lpVtbl -> addSurvey(This,urgent,id,create_timestamp,title,schedule,history) ) 

#define IAlertXML2_getResult(This,result)	\
    ( (This)->lpVtbl -> getResult(This,result) ) 

#define IAlertXML2_getUTF8Result(This,result)	\
    ( (This)->lpVtbl -> getUTF8Result(This,result) ) 


#define IAlertXML2_setUserInfo(This,connection,id,name,hash,result)	\
    ( (This)->lpVtbl -> setUserInfo(This,connection,id,name,hash,result) ) 

#define IAlertXML2_addAlert2(This,urgent,id,aknown,autoclose,create_timestamp,title,position,visible,width,height,ticker,schedule,recurrence,history,innner,result)	\
    ( (This)->lpVtbl -> addAlert2(This,urgent,id,aknown,autoclose,create_timestamp,title,position,visible,width,height,ticker,schedule,recurrence,history,innner,result) ) 

#define IAlertXML2_addCustomAlert2(This,urgent,href,id,aknown,autoclose,create_timestamp,title,position,visible,width,height,ticker,schedule,recurrence,history,innner,result)	\
    ( (This)->lpVtbl -> addCustomAlert2(This,urgent,href,id,aknown,autoclose,create_timestamp,title,position,visible,width,height,ticker,schedule,recurrence,history,innner,result) ) 

#define IAlertXML2_addSurvey2(This,urgent,id,create_timestamp,title,position,visible,width,height,schedule,recurrence,history,innner,result)	\
    ( (This)->lpVtbl -> addSurvey2(This,urgent,id,create_timestamp,title,position,visible,width,height,schedule,recurrence,history,innner,result) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IAlertXML2_INTERFACE_DEFINED__ */


#ifndef __IAlertXML3_INTERFACE_DEFINED__
#define __IAlertXML3_INTERFACE_DEFINED__

/* interface IAlertXML3 */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IAlertXML3;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("70789E45-7607-4e45-809C-72A7E4D6815B")
    IAlertXML3 : public IAlertXML2
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE addAlert3( 
            /* [in] */ BOOL urgent,
            /* [in] */ LONG id,
            /* [in] */ LONG aknown,
            /* [in] */ LONG autoclose,
            /* [in] */ LONG create_timestamp,
            /* [in] */ BSTR title,
            /* [in] */ BSTR position,
            /* [in] */ BSTR visible,
            /* [in] */ BSTR width,
            /* [in] */ BSTR height,
            /* [in] */ LONG ticker,
            /* [in] */ LONG schedule,
            /* [in] */ LONG recurrence,
            /* [in] */ BOOL history,
            /* [in] */ LONG classId,
            /* [in] */ BSTR innner,
            /* [retval][out] */ BOOL *result) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE addCustomAlert3( 
            /* [in] */ BOOL urgent,
            /* [in] */ BSTR href,
            /* [in] */ LONG id,
            /* [in] */ LONG aknown,
            /* [in] */ LONG autoclose,
            /* [in] */ LONG create_timestamp,
            /* [in] */ BSTR title,
            /* [in] */ BSTR position,
            /* [in] */ BSTR visible,
            /* [in] */ BSTR width,
            /* [in] */ BSTR height,
            /* [in] */ LONG ticker,
            /* [in] */ LONG schedule,
            /* [in] */ LONG recurrence,
            /* [in] */ BOOL history,
            /* [in] */ LONG classId,
            /* [in] */ BSTR innner,
            /* [retval][out] */ BOOL *result) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE addSurvey3( 
            /* [in] */ BOOL urgent,
            /* [in] */ LONG id,
            /* [in] */ LONG create_timestamp,
            /* [in] */ BSTR title,
            /* [in] */ LONG autoclose,
            /* [in] */ BSTR position,
            /* [in] */ BSTR visible,
            /* [in] */ BSTR width,
            /* [in] */ BSTR height,
            /* [in] */ LONG schedule,
            /* [in] */ LONG recurrence,
            /* [in] */ BOOL history,
            /* [in] */ LONG classId,
            /* [in] */ BSTR innner,
            /* [retval][out] */ BOOL *result) = 0;
        
    };
    
    
#else 	/* C style interface */

    typedef struct IAlertXML3Vtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IAlertXML3 * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IAlertXML3 * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IAlertXML3 * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IAlertXML3 * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IAlertXML3 * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IAlertXML3 * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IAlertXML3 * This,
            /* [annotation][in] */ 
            _In_  DISPID dispIdMember,
            /* [annotation][in] */ 
            _In_  REFIID riid,
            /* [annotation][in] */ 
            _In_  LCID lcid,
            /* [annotation][in] */ 
            _In_  WORD wFlags,
            /* [annotation][out][in] */ 
            _In_  DISPPARAMS *pDispParams,
            /* [annotation][out] */ 
            _Out_opt_  VARIANT *pVarResult,
            /* [annotation][out] */ 
            _Out_opt_  EXCEPINFO *pExcepInfo,
            /* [annotation][out] */ 
            _Out_opt_  UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *setUserId )( 
            IAlertXML3 * This,
            /* [in] */ IDispatch *connection,
            /* [in] */ LONG id);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *setTimeout )( 
            IAlertXML3 * This,
            /* [in] */ LONG timeout);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *setPlugin )( 
            IAlertXML3 * This,
            /* [in] */ BSTR plugin);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *setAlertURL )( 
            IAlertXML3 * This,
            /* [in] */ BSTR url);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *setSurveyURL )( 
            IAlertXML3 * This,
            /* [in] */ BSTR url);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *setDeskbarId )( 
            IAlertXML3 * This,
            /* [in] */ BSTR id);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *addAlert )( 
            IAlertXML3 * This,
            /* [in] */ BOOL urgent,
            /* [in] */ LONG id,
            /* [in] */ LONG aknown,
            /* [in] */ LONG autoclose,
            /* [in] */ LONG create_timestamp,
            /* [in] */ BSTR title,
            /* [in] */ BSTR position,
            /* [in] */ BSTR width,
            /* [in] */ BSTR height,
            /* [in] */ LONG ticker,
            /* [in] */ LONG schedule,
            /* [in] */ BOOL history);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *addCustomAlert )( 
            IAlertXML3 * This,
            /* [in] */ BOOL urgent,
            /* [in] */ BSTR href,
            /* [in] */ LONG id,
            /* [in] */ LONG aknown,
            /* [in] */ LONG autoclose,
            /* [in] */ LONG create_timestamp,
            /* [in] */ BSTR title,
            /* [in] */ BSTR position,
            /* [in] */ BSTR width,
            /* [in] */ BSTR height,
            /* [in] */ LONG ticker,
            /* [in] */ LONG schedule,
            /* [in] */ BOOL history);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *addSurvey )( 
            IAlertXML3 * This,
            /* [in] */ BOOL urgent,
            /* [in] */ LONG id,
            /* [in] */ LONG create_timestamp,
            /* [in] */ BSTR title,
            /* [in] */ LONG schedule,
            /* [in] */ BOOL history);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *getResult )( 
            IAlertXML3 * This,
            /* [retval][out] */ BSTR *result);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *getUTF8Result )( 
            IAlertXML3 * This,
            /* [retval][out] */ SAFEARRAY * *result);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *setUserInfo )( 
            IAlertXML3 * This,
            /* [in] */ IDispatch *connection,
            /* [in] */ LONG id,
            /* [in] */ BSTR name,
            /* [in] */ BSTR hash,
            /* [retval][out] */ BOOL *result);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *addAlert2 )( 
            IAlertXML3 * This,
            /* [in] */ BOOL urgent,
            /* [in] */ LONG id,
            /* [in] */ LONG aknown,
            /* [in] */ LONG autoclose,
            /* [in] */ LONG create_timestamp,
            /* [in] */ BSTR title,
            /* [in] */ BSTR position,
            /* [in] */ BSTR visible,
            /* [in] */ BSTR width,
            /* [in] */ BSTR height,
            /* [in] */ LONG ticker,
            /* [in] */ LONG schedule,
            /* [in] */ LONG recurrence,
            /* [in] */ BOOL history,
            /* [in] */ BSTR innner,
            /* [retval][out] */ BOOL *result);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *addCustomAlert2 )( 
            IAlertXML3 * This,
            /* [in] */ BOOL urgent,
            /* [in] */ BSTR href,
            /* [in] */ LONG id,
            /* [in] */ LONG aknown,
            /* [in] */ LONG autoclose,
            /* [in] */ LONG create_timestamp,
            /* [in] */ BSTR title,
            /* [in] */ BSTR position,
            /* [in] */ BSTR visible,
            /* [in] */ BSTR width,
            /* [in] */ BSTR height,
            /* [in] */ LONG ticker,
            /* [in] */ LONG schedule,
            /* [in] */ LONG recurrence,
            /* [in] */ BOOL history,
            /* [in] */ BSTR innner,
            /* [retval][out] */ BOOL *result);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *addSurvey2 )( 
            IAlertXML3 * This,
            /* [in] */ BOOL urgent,
            /* [in] */ LONG id,
            /* [in] */ LONG create_timestamp,
            /* [in] */ BSTR title,
            /* [in] */ BSTR position,
            /* [in] */ BSTR visible,
            /* [in] */ BSTR width,
            /* [in] */ BSTR height,
            /* [in] */ LONG schedule,
            /* [in] */ LONG recurrence,
            /* [in] */ BOOL history,
            /* [in] */ BSTR innner,
            /* [retval][out] */ BOOL *result);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *addAlert3 )( 
            IAlertXML3 * This,
            /* [in] */ BOOL urgent,
            /* [in] */ LONG id,
            /* [in] */ LONG aknown,
            /* [in] */ LONG autoclose,
            /* [in] */ LONG create_timestamp,
            /* [in] */ BSTR title,
            /* [in] */ BSTR position,
            /* [in] */ BSTR visible,
            /* [in] */ BSTR width,
            /* [in] */ BSTR height,
            /* [in] */ LONG ticker,
            /* [in] */ LONG schedule,
            /* [in] */ LONG recurrence,
            /* [in] */ BOOL history,
            /* [in] */ LONG classId,
            /* [in] */ BSTR innner,
            /* [retval][out] */ BOOL *result);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *addCustomAlert3 )( 
            IAlertXML3 * This,
            /* [in] */ BOOL urgent,
            /* [in] */ BSTR href,
            /* [in] */ LONG id,
            /* [in] */ LONG aknown,
            /* [in] */ LONG autoclose,
            /* [in] */ LONG create_timestamp,
            /* [in] */ BSTR title,
            /* [in] */ BSTR position,
            /* [in] */ BSTR visible,
            /* [in] */ BSTR width,
            /* [in] */ BSTR height,
            /* [in] */ LONG ticker,
            /* [in] */ LONG schedule,
            /* [in] */ LONG recurrence,
            /* [in] */ BOOL history,
            /* [in] */ LONG classId,
            /* [in] */ BSTR innner,
            /* [retval][out] */ BOOL *result);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *addSurvey3 )( 
            IAlertXML3 * This,
            /* [in] */ BOOL urgent,
            /* [in] */ LONG id,
            /* [in] */ LONG create_timestamp,
            /* [in] */ BSTR title,
            /* [in] */ LONG autoclose,
            /* [in] */ BSTR position,
            /* [in] */ BSTR visible,
            /* [in] */ BSTR width,
            /* [in] */ BSTR height,
            /* [in] */ LONG schedule,
            /* [in] */ LONG recurrence,
            /* [in] */ BOOL history,
            /* [in] */ LONG classId,
            /* [in] */ BSTR innner,
            /* [retval][out] */ BOOL *result);
        
        END_INTERFACE
    } IAlertXML3Vtbl;

    interface IAlertXML3
    {
        CONST_VTBL struct IAlertXML3Vtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IAlertXML3_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IAlertXML3_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IAlertXML3_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IAlertXML3_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IAlertXML3_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IAlertXML3_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IAlertXML3_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IAlertXML3_setUserId(This,connection,id)	\
    ( (This)->lpVtbl -> setUserId(This,connection,id) ) 

#define IAlertXML3_setTimeout(This,timeout)	\
    ( (This)->lpVtbl -> setTimeout(This,timeout) ) 

#define IAlertXML3_setPlugin(This,plugin)	\
    ( (This)->lpVtbl -> setPlugin(This,plugin) ) 

#define IAlertXML3_setAlertURL(This,url)	\
    ( (This)->lpVtbl -> setAlertURL(This,url) ) 

#define IAlertXML3_setSurveyURL(This,url)	\
    ( (This)->lpVtbl -> setSurveyURL(This,url) ) 

#define IAlertXML3_setDeskbarId(This,id)	\
    ( (This)->lpVtbl -> setDeskbarId(This,id) ) 

#define IAlertXML3_addAlert(This,urgent,id,aknown,autoclose,create_timestamp,title,position,width,height,ticker,schedule,history)	\
    ( (This)->lpVtbl -> addAlert(This,urgent,id,aknown,autoclose,create_timestamp,title,position,width,height,ticker,schedule,history) ) 

#define IAlertXML3_addCustomAlert(This,urgent,href,id,aknown,autoclose,create_timestamp,title,position,width,height,ticker,schedule,history)	\
    ( (This)->lpVtbl -> addCustomAlert(This,urgent,href,id,aknown,autoclose,create_timestamp,title,position,width,height,ticker,schedule,history) ) 

#define IAlertXML3_addSurvey(This,urgent,id,create_timestamp,title,schedule,history)	\
    ( (This)->lpVtbl -> addSurvey(This,urgent,id,create_timestamp,title,schedule,history) ) 

#define IAlertXML3_getResult(This,result)	\
    ( (This)->lpVtbl -> getResult(This,result) ) 

#define IAlertXML3_getUTF8Result(This,result)	\
    ( (This)->lpVtbl -> getUTF8Result(This,result) ) 


#define IAlertXML3_setUserInfo(This,connection,id,name,hash,result)	\
    ( (This)->lpVtbl -> setUserInfo(This,connection,id,name,hash,result) ) 

#define IAlertXML3_addAlert2(This,urgent,id,aknown,autoclose,create_timestamp,title,position,visible,width,height,ticker,schedule,recurrence,history,innner,result)	\
    ( (This)->lpVtbl -> addAlert2(This,urgent,id,aknown,autoclose,create_timestamp,title,position,visible,width,height,ticker,schedule,recurrence,history,innner,result) ) 

#define IAlertXML3_addCustomAlert2(This,urgent,href,id,aknown,autoclose,create_timestamp,title,position,visible,width,height,ticker,schedule,recurrence,history,innner,result)	\
    ( (This)->lpVtbl -> addCustomAlert2(This,urgent,href,id,aknown,autoclose,create_timestamp,title,position,visible,width,height,ticker,schedule,recurrence,history,innner,result) ) 

#define IAlertXML3_addSurvey2(This,urgent,id,create_timestamp,title,position,visible,width,height,schedule,recurrence,history,innner,result)	\
    ( (This)->lpVtbl -> addSurvey2(This,urgent,id,create_timestamp,title,position,visible,width,height,schedule,recurrence,history,innner,result) ) 


#define IAlertXML3_addAlert3(This,urgent,id,aknown,autoclose,create_timestamp,title,position,visible,width,height,ticker,schedule,recurrence,history,classId,innner,result)	\
    ( (This)->lpVtbl -> addAlert3(This,urgent,id,aknown,autoclose,create_timestamp,title,position,visible,width,height,ticker,schedule,recurrence,history,classId,innner,result) ) 

#define IAlertXML3_addCustomAlert3(This,urgent,href,id,aknown,autoclose,create_timestamp,title,position,visible,width,height,ticker,schedule,recurrence,history,classId,innner,result)	\
    ( (This)->lpVtbl -> addCustomAlert3(This,urgent,href,id,aknown,autoclose,create_timestamp,title,position,visible,width,height,ticker,schedule,recurrence,history,classId,innner,result) ) 

#define IAlertXML3_addSurvey3(This,urgent,id,create_timestamp,title,autoclose,position,visible,width,height,schedule,recurrence,history,classId,innner,result)	\
    ( (This)->lpVtbl -> addSurvey3(This,urgent,id,create_timestamp,title,autoclose,position,visible,width,height,schedule,recurrence,history,classId,innner,result) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IAlertXML3_INTERFACE_DEFINED__ */


#ifndef __IAlertXML4_INTERFACE_DEFINED__
#define __IAlertXML4_INTERFACE_DEFINED__

/* interface IAlertXML4 */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IAlertXML4;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("9755E4B0-16FE-445E-8367-6A5A70257C0D")
    IAlertXML4 : public IAlertXML3
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE setAllUserInfo( 
            /* [in] */ IDispatch *connection,
            /* [in] */ LONG id,
            /* [in] */ BSTR name,
            /* [in] */ BSTR context,
            /* [in] */ BSTR domain,
            /* [in] */ BSTR hash,
            /* [retval][out] */ BOOL *result) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE addAlert4( 
            /* [in] */ IDispatch *params,
            /* [in] */ BSTR innner,
            /* [retval][out] */ BOOL *result) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE addSurvey4( 
            /* [in] */ BOOL urgent,
            /* [in] */ LONG id,
            /* [in] */ LONG create_timestamp,
            /* [in] */ BSTR title,
            /* [in] */ LONG autoclose,
            /* [in] */ BSTR position,
            /* [in] */ BSTR visible,
            /* [in] */ BSTR width,
            /* [in] */ BSTR height,
            /* [in] */ LONG schedule,
            /* [in] */ LONG recurrence,
            /* [in] */ BOOL history,
            /* [in] */ LONG classId,
            /* [in] */ LONG resizable,
            /* [in] */ LONG to_date,
            /* [in] */ BSTR innner,
            /* [retval][out] */ BOOL *result) = 0;
        
    };
    
    
#else 	/* C style interface */

    typedef struct IAlertXML4Vtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IAlertXML4 * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IAlertXML4 * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IAlertXML4 * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IAlertXML4 * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IAlertXML4 * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IAlertXML4 * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IAlertXML4 * This,
            /* [annotation][in] */ 
            _In_  DISPID dispIdMember,
            /* [annotation][in] */ 
            _In_  REFIID riid,
            /* [annotation][in] */ 
            _In_  LCID lcid,
            /* [annotation][in] */ 
            _In_  WORD wFlags,
            /* [annotation][out][in] */ 
            _In_  DISPPARAMS *pDispParams,
            /* [annotation][out] */ 
            _Out_opt_  VARIANT *pVarResult,
            /* [annotation][out] */ 
            _Out_opt_  EXCEPINFO *pExcepInfo,
            /* [annotation][out] */ 
            _Out_opt_  UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *setUserId )( 
            IAlertXML4 * This,
            /* [in] */ IDispatch *connection,
            /* [in] */ LONG id);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *setTimeout )( 
            IAlertXML4 * This,
            /* [in] */ LONG timeout);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *setPlugin )( 
            IAlertXML4 * This,
            /* [in] */ BSTR plugin);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *setAlertURL )( 
            IAlertXML4 * This,
            /* [in] */ BSTR url);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *setSurveyURL )( 
            IAlertXML4 * This,
            /* [in] */ BSTR url);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *setDeskbarId )( 
            IAlertXML4 * This,
            /* [in] */ BSTR id);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *addAlert )( 
            IAlertXML4 * This,
            /* [in] */ BOOL urgent,
            /* [in] */ LONG id,
            /* [in] */ LONG aknown,
            /* [in] */ LONG autoclose,
            /* [in] */ LONG create_timestamp,
            /* [in] */ BSTR title,
            /* [in] */ BSTR position,
            /* [in] */ BSTR width,
            /* [in] */ BSTR height,
            /* [in] */ LONG ticker,
            /* [in] */ LONG schedule,
            /* [in] */ BOOL history);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *addCustomAlert )( 
            IAlertXML4 * This,
            /* [in] */ BOOL urgent,
            /* [in] */ BSTR href,
            /* [in] */ LONG id,
            /* [in] */ LONG aknown,
            /* [in] */ LONG autoclose,
            /* [in] */ LONG create_timestamp,
            /* [in] */ BSTR title,
            /* [in] */ BSTR position,
            /* [in] */ BSTR width,
            /* [in] */ BSTR height,
            /* [in] */ LONG ticker,
            /* [in] */ LONG schedule,
            /* [in] */ BOOL history);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *addSurvey )( 
            IAlertXML4 * This,
            /* [in] */ BOOL urgent,
            /* [in] */ LONG id,
            /* [in] */ LONG create_timestamp,
            /* [in] */ BSTR title,
            /* [in] */ LONG schedule,
            /* [in] */ BOOL history);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *getResult )( 
            IAlertXML4 * This,
            /* [retval][out] */ BSTR *result);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *getUTF8Result )( 
            IAlertXML4 * This,
            /* [retval][out] */ SAFEARRAY * *result);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *setUserInfo )( 
            IAlertXML4 * This,
            /* [in] */ IDispatch *connection,
            /* [in] */ LONG id,
            /* [in] */ BSTR name,
            /* [in] */ BSTR hash,
            /* [retval][out] */ BOOL *result);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *addAlert2 )( 
            IAlertXML4 * This,
            /* [in] */ BOOL urgent,
            /* [in] */ LONG id,
            /* [in] */ LONG aknown,
            /* [in] */ LONG autoclose,
            /* [in] */ LONG create_timestamp,
            /* [in] */ BSTR title,
            /* [in] */ BSTR position,
            /* [in] */ BSTR visible,
            /* [in] */ BSTR width,
            /* [in] */ BSTR height,
            /* [in] */ LONG ticker,
            /* [in] */ LONG schedule,
            /* [in] */ LONG recurrence,
            /* [in] */ BOOL history,
            /* [in] */ BSTR innner,
            /* [retval][out] */ BOOL *result);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *addCustomAlert2 )( 
            IAlertXML4 * This,
            /* [in] */ BOOL urgent,
            /* [in] */ BSTR href,
            /* [in] */ LONG id,
            /* [in] */ LONG aknown,
            /* [in] */ LONG autoclose,
            /* [in] */ LONG create_timestamp,
            /* [in] */ BSTR title,
            /* [in] */ BSTR position,
            /* [in] */ BSTR visible,
            /* [in] */ BSTR width,
            /* [in] */ BSTR height,
            /* [in] */ LONG ticker,
            /* [in] */ LONG schedule,
            /* [in] */ LONG recurrence,
            /* [in] */ BOOL history,
            /* [in] */ BSTR innner,
            /* [retval][out] */ BOOL *result);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *addSurvey2 )( 
            IAlertXML4 * This,
            /* [in] */ BOOL urgent,
            /* [in] */ LONG id,
            /* [in] */ LONG create_timestamp,
            /* [in] */ BSTR title,
            /* [in] */ BSTR position,
            /* [in] */ BSTR visible,
            /* [in] */ BSTR width,
            /* [in] */ BSTR height,
            /* [in] */ LONG schedule,
            /* [in] */ LONG recurrence,
            /* [in] */ BOOL history,
            /* [in] */ BSTR innner,
            /* [retval][out] */ BOOL *result);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *addAlert3 )( 
            IAlertXML4 * This,
            /* [in] */ BOOL urgent,
            /* [in] */ LONG id,
            /* [in] */ LONG aknown,
            /* [in] */ LONG autoclose,
            /* [in] */ LONG create_timestamp,
            /* [in] */ BSTR title,
            /* [in] */ BSTR position,
            /* [in] */ BSTR visible,
            /* [in] */ BSTR width,
            /* [in] */ BSTR height,
            /* [in] */ LONG ticker,
            /* [in] */ LONG schedule,
            /* [in] */ LONG recurrence,
            /* [in] */ BOOL history,
            /* [in] */ LONG classId,
            /* [in] */ BSTR innner,
            /* [retval][out] */ BOOL *result);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *addCustomAlert3 )( 
            IAlertXML4 * This,
            /* [in] */ BOOL urgent,
            /* [in] */ BSTR href,
            /* [in] */ LONG id,
            /* [in] */ LONG aknown,
            /* [in] */ LONG autoclose,
            /* [in] */ LONG create_timestamp,
            /* [in] */ BSTR title,
            /* [in] */ BSTR position,
            /* [in] */ BSTR visible,
            /* [in] */ BSTR width,
            /* [in] */ BSTR height,
            /* [in] */ LONG ticker,
            /* [in] */ LONG schedule,
            /* [in] */ LONG recurrence,
            /* [in] */ BOOL history,
            /* [in] */ LONG classId,
            /* [in] */ BSTR innner,
            /* [retval][out] */ BOOL *result);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *addSurvey3 )( 
            IAlertXML4 * This,
            /* [in] */ BOOL urgent,
            /* [in] */ LONG id,
            /* [in] */ LONG create_timestamp,
            /* [in] */ BSTR title,
            /* [in] */ LONG autoclose,
            /* [in] */ BSTR position,
            /* [in] */ BSTR visible,
            /* [in] */ BSTR width,
            /* [in] */ BSTR height,
            /* [in] */ LONG schedule,
            /* [in] */ LONG recurrence,
            /* [in] */ BOOL history,
            /* [in] */ LONG classId,
            /* [in] */ BSTR innner,
            /* [retval][out] */ BOOL *result);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *setAllUserInfo )( 
            IAlertXML4 * This,
            /* [in] */ IDispatch *connection,
            /* [in] */ LONG id,
            /* [in] */ BSTR name,
            /* [in] */ BSTR context,
            /* [in] */ BSTR domain,
            /* [in] */ BSTR hash,
            /* [retval][out] */ BOOL *result);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *addAlert4 )( 
            IAlertXML4 * This,
            /* [in] */ IDispatch *params,
            /* [in] */ BSTR innner,
            /* [retval][out] */ BOOL *result);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *addSurvey4 )( 
            IAlertXML4 * This,
            /* [in] */ BOOL urgent,
            /* [in] */ LONG id,
            /* [in] */ LONG create_timestamp,
            /* [in] */ BSTR title,
            /* [in] */ LONG autoclose,
            /* [in] */ BSTR position,
            /* [in] */ BSTR visible,
            /* [in] */ BSTR width,
            /* [in] */ BSTR height,
            /* [in] */ LONG schedule,
            /* [in] */ LONG recurrence,
            /* [in] */ BOOL history,
            /* [in] */ LONG classId,
            /* [in] */ LONG resizable,
            /* [in] */ LONG to_date,
            /* [in] */ BSTR innner,
            /* [retval][out] */ BOOL *result);
        
        END_INTERFACE
    } IAlertXML4Vtbl;

    interface IAlertXML4
    {
        CONST_VTBL struct IAlertXML4Vtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IAlertXML4_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IAlertXML4_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IAlertXML4_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IAlertXML4_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IAlertXML4_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IAlertXML4_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IAlertXML4_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IAlertXML4_setUserId(This,connection,id)	\
    ( (This)->lpVtbl -> setUserId(This,connection,id) ) 

#define IAlertXML4_setTimeout(This,timeout)	\
    ( (This)->lpVtbl -> setTimeout(This,timeout) ) 

#define IAlertXML4_setPlugin(This,plugin)	\
    ( (This)->lpVtbl -> setPlugin(This,plugin) ) 

#define IAlertXML4_setAlertURL(This,url)	\
    ( (This)->lpVtbl -> setAlertURL(This,url) ) 

#define IAlertXML4_setSurveyURL(This,url)	\
    ( (This)->lpVtbl -> setSurveyURL(This,url) ) 

#define IAlertXML4_setDeskbarId(This,id)	\
    ( (This)->lpVtbl -> setDeskbarId(This,id) ) 

#define IAlertXML4_addAlert(This,urgent,id,aknown,autoclose,create_timestamp,title,position,width,height,ticker,schedule,history)	\
    ( (This)->lpVtbl -> addAlert(This,urgent,id,aknown,autoclose,create_timestamp,title,position,width,height,ticker,schedule,history) ) 

#define IAlertXML4_addCustomAlert(This,urgent,href,id,aknown,autoclose,create_timestamp,title,position,width,height,ticker,schedule,history)	\
    ( (This)->lpVtbl -> addCustomAlert(This,urgent,href,id,aknown,autoclose,create_timestamp,title,position,width,height,ticker,schedule,history) ) 

#define IAlertXML4_addSurvey(This,urgent,id,create_timestamp,title,schedule,history)	\
    ( (This)->lpVtbl -> addSurvey(This,urgent,id,create_timestamp,title,schedule,history) ) 

#define IAlertXML4_getResult(This,result)	\
    ( (This)->lpVtbl -> getResult(This,result) ) 

#define IAlertXML4_getUTF8Result(This,result)	\
    ( (This)->lpVtbl -> getUTF8Result(This,result) ) 


#define IAlertXML4_setUserInfo(This,connection,id,name,hash,result)	\
    ( (This)->lpVtbl -> setUserInfo(This,connection,id,name,hash,result) ) 

#define IAlertXML4_addAlert2(This,urgent,id,aknown,autoclose,create_timestamp,title,position,visible,width,height,ticker,schedule,recurrence,history,innner,result)	\
    ( (This)->lpVtbl -> addAlert2(This,urgent,id,aknown,autoclose,create_timestamp,title,position,visible,width,height,ticker,schedule,recurrence,history,innner,result) ) 

#define IAlertXML4_addCustomAlert2(This,urgent,href,id,aknown,autoclose,create_timestamp,title,position,visible,width,height,ticker,schedule,recurrence,history,innner,result)	\
    ( (This)->lpVtbl -> addCustomAlert2(This,urgent,href,id,aknown,autoclose,create_timestamp,title,position,visible,width,height,ticker,schedule,recurrence,history,innner,result) ) 

#define IAlertXML4_addSurvey2(This,urgent,id,create_timestamp,title,position,visible,width,height,schedule,recurrence,history,innner,result)	\
    ( (This)->lpVtbl -> addSurvey2(This,urgent,id,create_timestamp,title,position,visible,width,height,schedule,recurrence,history,innner,result) ) 


#define IAlertXML4_addAlert3(This,urgent,id,aknown,autoclose,create_timestamp,title,position,visible,width,height,ticker,schedule,recurrence,history,classId,innner,result)	\
    ( (This)->lpVtbl -> addAlert3(This,urgent,id,aknown,autoclose,create_timestamp,title,position,visible,width,height,ticker,schedule,recurrence,history,classId,innner,result) ) 

#define IAlertXML4_addCustomAlert3(This,urgent,href,id,aknown,autoclose,create_timestamp,title,position,visible,width,height,ticker,schedule,recurrence,history,classId,innner,result)	\
    ( (This)->lpVtbl -> addCustomAlert3(This,urgent,href,id,aknown,autoclose,create_timestamp,title,position,visible,width,height,ticker,schedule,recurrence,history,classId,innner,result) ) 

#define IAlertXML4_addSurvey3(This,urgent,id,create_timestamp,title,autoclose,position,visible,width,height,schedule,recurrence,history,classId,innner,result)	\
    ( (This)->lpVtbl -> addSurvey3(This,urgent,id,create_timestamp,title,autoclose,position,visible,width,height,schedule,recurrence,history,classId,innner,result) ) 


#define IAlertXML4_setAllUserInfo(This,connection,id,name,context,domain,hash,result)	\
    ( (This)->lpVtbl -> setAllUserInfo(This,connection,id,name,context,domain,hash,result) ) 

#define IAlertXML4_addAlert4(This,params,innner,result)	\
    ( (This)->lpVtbl -> addAlert4(This,params,innner,result) ) 

#define IAlertXML4_addSurvey4(This,urgent,id,create_timestamp,title,autoclose,position,visible,width,height,schedule,recurrence,history,classId,resizable,to_date,innner,result)	\
    ( (This)->lpVtbl -> addSurvey4(This,urgent,id,create_timestamp,title,autoclose,position,visible,width,height,schedule,recurrence,history,classId,resizable,to_date,innner,result) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IAlertXML4_INTERFACE_DEFINED__ */


#ifndef __IEncrypt_INTERFACE_DEFINED__
#define __IEncrypt_INTERFACE_DEFINED__

/* interface IEncrypt */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IEncrypt;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("08BBC941-030D-4053-A305-095C3BEB4035")
    IEncrypt : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE encrypt( 
            /* [in] */ BSTR data,
            /* [in] */ BSTR password,
            /* [retval][out] */ SAFEARRAY * *result) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE encryptUTF8( 
            /* [in] */ BSTR data,
            /* [in] */ BSTR password,
            /* [retval][out] */ SAFEARRAY * *result) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE newEncrypt( 
            /* [in] */ BSTR data,
            /* [in] */ BSTR password,
            /* [retval][out] */ SAFEARRAY * *result) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE newEncryptUTF8( 
            /* [in] */ BSTR data,
            /* [in] */ BSTR password,
            /* [retval][out] */ SAFEARRAY * *result) = 0;
        
    };
    
    
#else 	/* C style interface */

    typedef struct IEncryptVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IEncrypt * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IEncrypt * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IEncrypt * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IEncrypt * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IEncrypt * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IEncrypt * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IEncrypt * This,
            /* [annotation][in] */ 
            _In_  DISPID dispIdMember,
            /* [annotation][in] */ 
            _In_  REFIID riid,
            /* [annotation][in] */ 
            _In_  LCID lcid,
            /* [annotation][in] */ 
            _In_  WORD wFlags,
            /* [annotation][out][in] */ 
            _In_  DISPPARAMS *pDispParams,
            /* [annotation][out] */ 
            _Out_opt_  VARIANT *pVarResult,
            /* [annotation][out] */ 
            _Out_opt_  EXCEPINFO *pExcepInfo,
            /* [annotation][out] */ 
            _Out_opt_  UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *encrypt )( 
            IEncrypt * This,
            /* [in] */ BSTR data,
            /* [in] */ BSTR password,
            /* [retval][out] */ SAFEARRAY * *result);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *encryptUTF8 )( 
            IEncrypt * This,
            /* [in] */ BSTR data,
            /* [in] */ BSTR password,
            /* [retval][out] */ SAFEARRAY * *result);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *newEncrypt )( 
            IEncrypt * This,
            /* [in] */ BSTR data,
            /* [in] */ BSTR password,
            /* [retval][out] */ SAFEARRAY * *result);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *newEncryptUTF8 )( 
            IEncrypt * This,
            /* [in] */ BSTR data,
            /* [in] */ BSTR password,
            /* [retval][out] */ SAFEARRAY * *result);
        
        END_INTERFACE
    } IEncryptVtbl;

    interface IEncrypt
    {
        CONST_VTBL struct IEncryptVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEncrypt_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IEncrypt_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IEncrypt_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IEncrypt_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IEncrypt_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IEncrypt_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IEncrypt_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IEncrypt_encrypt(This,data,password,result)	\
    ( (This)->lpVtbl -> encrypt(This,data,password,result) ) 

#define IEncrypt_encryptUTF8(This,data,password,result)	\
    ( (This)->lpVtbl -> encryptUTF8(This,data,password,result) ) 

#define IEncrypt_newEncrypt(This,data,password,result)	\
    ( (This)->lpVtbl -> newEncrypt(This,data,password,result) ) 

#define IEncrypt_newEncryptUTF8(This,data,password,result)	\
    ( (This)->lpVtbl -> newEncryptUTF8(This,data,password,result) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IEncrypt_INTERFACE_DEFINED__ */


#ifndef __IEncrypt2_INTERFACE_DEFINED__
#define __IEncrypt2_INTERFACE_DEFINED__

/* interface IEncrypt2 */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IEncrypt2;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("D534EE24-30BF-4725-94AE-F72D3EA52648")
    IEncrypt2 : public IEncrypt
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE md5FromUtf8( 
            /* [in] */ BSTR str,
            /* [retval][out] */ BSTR *result) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE sha1FromUtf8( 
            /* [in] */ BSTR str,
            /* [retval][out] */ BSTR *result) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE base64FromUtf8( 
            /* [in] */ BSTR str,
            /* [retval][out] */ BSTR *result) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE strFromUtf8Base64( 
            /* [in] */ BSTR str,
            /* [retval][out] */ BSTR *result) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE utf8FromBase64( 
            /* [in] */ BSTR str,
            /* [retval][out] */ SAFEARRAY * *result) = 0;
        
    };
    
    
#else 	/* C style interface */

    typedef struct IEncrypt2Vtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IEncrypt2 * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IEncrypt2 * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IEncrypt2 * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IEncrypt2 * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IEncrypt2 * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IEncrypt2 * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IEncrypt2 * This,
            /* [annotation][in] */ 
            _In_  DISPID dispIdMember,
            /* [annotation][in] */ 
            _In_  REFIID riid,
            /* [annotation][in] */ 
            _In_  LCID lcid,
            /* [annotation][in] */ 
            _In_  WORD wFlags,
            /* [annotation][out][in] */ 
            _In_  DISPPARAMS *pDispParams,
            /* [annotation][out] */ 
            _Out_opt_  VARIANT *pVarResult,
            /* [annotation][out] */ 
            _Out_opt_  EXCEPINFO *pExcepInfo,
            /* [annotation][out] */ 
            _Out_opt_  UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *encrypt )( 
            IEncrypt2 * This,
            /* [in] */ BSTR data,
            /* [in] */ BSTR password,
            /* [retval][out] */ SAFEARRAY * *result);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *encryptUTF8 )( 
            IEncrypt2 * This,
            /* [in] */ BSTR data,
            /* [in] */ BSTR password,
            /* [retval][out] */ SAFEARRAY * *result);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *newEncrypt )( 
            IEncrypt2 * This,
            /* [in] */ BSTR data,
            /* [in] */ BSTR password,
            /* [retval][out] */ SAFEARRAY * *result);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *newEncryptUTF8 )( 
            IEncrypt2 * This,
            /* [in] */ BSTR data,
            /* [in] */ BSTR password,
            /* [retval][out] */ SAFEARRAY * *result);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *md5FromUtf8 )( 
            IEncrypt2 * This,
            /* [in] */ BSTR str,
            /* [retval][out] */ BSTR *result);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *sha1FromUtf8 )( 
            IEncrypt2 * This,
            /* [in] */ BSTR str,
            /* [retval][out] */ BSTR *result);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *base64FromUtf8 )( 
            IEncrypt2 * This,
            /* [in] */ BSTR str,
            /* [retval][out] */ BSTR *result);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *strFromUtf8Base64 )( 
            IEncrypt2 * This,
            /* [in] */ BSTR str,
            /* [retval][out] */ BSTR *result);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *utf8FromBase64 )( 
            IEncrypt2 * This,
            /* [in] */ BSTR str,
            /* [retval][out] */ SAFEARRAY * *result);
        
        END_INTERFACE
    } IEncrypt2Vtbl;

    interface IEncrypt2
    {
        CONST_VTBL struct IEncrypt2Vtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEncrypt2_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IEncrypt2_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IEncrypt2_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IEncrypt2_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IEncrypt2_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IEncrypt2_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IEncrypt2_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IEncrypt2_encrypt(This,data,password,result)	\
    ( (This)->lpVtbl -> encrypt(This,data,password,result) ) 

#define IEncrypt2_encryptUTF8(This,data,password,result)	\
    ( (This)->lpVtbl -> encryptUTF8(This,data,password,result) ) 

#define IEncrypt2_newEncrypt(This,data,password,result)	\
    ( (This)->lpVtbl -> newEncrypt(This,data,password,result) ) 

#define IEncrypt2_newEncryptUTF8(This,data,password,result)	\
    ( (This)->lpVtbl -> newEncryptUTF8(This,data,password,result) ) 


#define IEncrypt2_md5FromUtf8(This,str,result)	\
    ( (This)->lpVtbl -> md5FromUtf8(This,str,result) ) 

#define IEncrypt2_sha1FromUtf8(This,str,result)	\
    ( (This)->lpVtbl -> sha1FromUtf8(This,str,result) ) 

#define IEncrypt2_base64FromUtf8(This,str,result)	\
    ( (This)->lpVtbl -> base64FromUtf8(This,str,result) ) 

#define IEncrypt2_strFromUtf8Base64(This,str,result)	\
    ( (This)->lpVtbl -> strFromUtf8Base64(This,str,result) ) 

#define IEncrypt2_utf8FromBase64(This,str,result)	\
    ( (This)->lpVtbl -> utf8FromBase64(This,str,result) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IEncrypt2_INTERFACE_DEFINED__ */


#ifndef __IEncrypt3_INTERFACE_DEFINED__
#define __IEncrypt3_INTERFACE_DEFINED__

/* interface IEncrypt3 */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IEncrypt3;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("F10FB315-9DA9-4e8e-BB66-B14047E8C6D8")
    IEncrypt3 : public IEncrypt2
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE shortIdFromGuid( 
            /* [in] */ BSTR str,
            /* [retval][out] */ BSTR *result) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE guidFromShortId( 
            /* [in] */ BSTR str,
            /* [retval][out] */ BSTR *result) = 0;
        
    };
    
    
#else 	/* C style interface */

    typedef struct IEncrypt3Vtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IEncrypt3 * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IEncrypt3 * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IEncrypt3 * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IEncrypt3 * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IEncrypt3 * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IEncrypt3 * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IEncrypt3 * This,
            /* [annotation][in] */ 
            _In_  DISPID dispIdMember,
            /* [annotation][in] */ 
            _In_  REFIID riid,
            /* [annotation][in] */ 
            _In_  LCID lcid,
            /* [annotation][in] */ 
            _In_  WORD wFlags,
            /* [annotation][out][in] */ 
            _In_  DISPPARAMS *pDispParams,
            /* [annotation][out] */ 
            _Out_opt_  VARIANT *pVarResult,
            /* [annotation][out] */ 
            _Out_opt_  EXCEPINFO *pExcepInfo,
            /* [annotation][out] */ 
            _Out_opt_  UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *encrypt )( 
            IEncrypt3 * This,
            /* [in] */ BSTR data,
            /* [in] */ BSTR password,
            /* [retval][out] */ SAFEARRAY * *result);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *encryptUTF8 )( 
            IEncrypt3 * This,
            /* [in] */ BSTR data,
            /* [in] */ BSTR password,
            /* [retval][out] */ SAFEARRAY * *result);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *newEncrypt )( 
            IEncrypt3 * This,
            /* [in] */ BSTR data,
            /* [in] */ BSTR password,
            /* [retval][out] */ SAFEARRAY * *result);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *newEncryptUTF8 )( 
            IEncrypt3 * This,
            /* [in] */ BSTR data,
            /* [in] */ BSTR password,
            /* [retval][out] */ SAFEARRAY * *result);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *md5FromUtf8 )( 
            IEncrypt3 * This,
            /* [in] */ BSTR str,
            /* [retval][out] */ BSTR *result);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *sha1FromUtf8 )( 
            IEncrypt3 * This,
            /* [in] */ BSTR str,
            /* [retval][out] */ BSTR *result);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *base64FromUtf8 )( 
            IEncrypt3 * This,
            /* [in] */ BSTR str,
            /* [retval][out] */ BSTR *result);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *strFromUtf8Base64 )( 
            IEncrypt3 * This,
            /* [in] */ BSTR str,
            /* [retval][out] */ BSTR *result);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *utf8FromBase64 )( 
            IEncrypt3 * This,
            /* [in] */ BSTR str,
            /* [retval][out] */ SAFEARRAY * *result);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *shortIdFromGuid )( 
            IEncrypt3 * This,
            /* [in] */ BSTR str,
            /* [retval][out] */ BSTR *result);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *guidFromShortId )( 
            IEncrypt3 * This,
            /* [in] */ BSTR str,
            /* [retval][out] */ BSTR *result);
        
        END_INTERFACE
    } IEncrypt3Vtbl;

    interface IEncrypt3
    {
        CONST_VTBL struct IEncrypt3Vtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEncrypt3_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IEncrypt3_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IEncrypt3_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IEncrypt3_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IEncrypt3_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IEncrypt3_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IEncrypt3_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IEncrypt3_encrypt(This,data,password,result)	\
    ( (This)->lpVtbl -> encrypt(This,data,password,result) ) 

#define IEncrypt3_encryptUTF8(This,data,password,result)	\
    ( (This)->lpVtbl -> encryptUTF8(This,data,password,result) ) 

#define IEncrypt3_newEncrypt(This,data,password,result)	\
    ( (This)->lpVtbl -> newEncrypt(This,data,password,result) ) 

#define IEncrypt3_newEncryptUTF8(This,data,password,result)	\
    ( (This)->lpVtbl -> newEncryptUTF8(This,data,password,result) ) 


#define IEncrypt3_md5FromUtf8(This,str,result)	\
    ( (This)->lpVtbl -> md5FromUtf8(This,str,result) ) 

#define IEncrypt3_sha1FromUtf8(This,str,result)	\
    ( (This)->lpVtbl -> sha1FromUtf8(This,str,result) ) 

#define IEncrypt3_base64FromUtf8(This,str,result)	\
    ( (This)->lpVtbl -> base64FromUtf8(This,str,result) ) 

#define IEncrypt3_strFromUtf8Base64(This,str,result)	\
    ( (This)->lpVtbl -> strFromUtf8Base64(This,str,result) ) 

#define IEncrypt3_utf8FromBase64(This,str,result)	\
    ( (This)->lpVtbl -> utf8FromBase64(This,str,result) ) 


#define IEncrypt3_shortIdFromGuid(This,str,result)	\
    ( (This)->lpVtbl -> shortIdFromGuid(This,str,result) ) 

#define IEncrypt3_guidFromShortId(This,str,result)	\
    ( (This)->lpVtbl -> guidFromShortId(This,str,result) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IEncrypt3_INTERFACE_DEFINED__ */


#ifndef __ILicensing_INTERFACE_DEFINED__
#define __ILicensing_INTERFACE_DEFINED__

/* interface ILicensing */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_ILicensing;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("16A674A3-E8BD-4455-BC29-153D53CAD9F6")
    ILicensing : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE getLicenseCount( 
            /* [in] */ IDispatch *connection,
            /* [retval][out] */ LONG *result) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE getCurrentUsersCount( 
            /* [in] */ IDispatch *connection,
            /* [retval][out] */ LONG *result) = 0;
        
    };
    
    
#else 	/* C style interface */

    typedef struct ILicensingVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ILicensing * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ILicensing * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ILicensing * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ILicensing * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ILicensing * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ILicensing * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ILicensing * This,
            /* [annotation][in] */ 
            _In_  DISPID dispIdMember,
            /* [annotation][in] */ 
            _In_  REFIID riid,
            /* [annotation][in] */ 
            _In_  LCID lcid,
            /* [annotation][in] */ 
            _In_  WORD wFlags,
            /* [annotation][out][in] */ 
            _In_  DISPPARAMS *pDispParams,
            /* [annotation][out] */ 
            _Out_opt_  VARIANT *pVarResult,
            /* [annotation][out] */ 
            _Out_opt_  EXCEPINFO *pExcepInfo,
            /* [annotation][out] */ 
            _Out_opt_  UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *getLicenseCount )( 
            ILicensing * This,
            /* [in] */ IDispatch *connection,
            /* [retval][out] */ LONG *result);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *getCurrentUsersCount )( 
            ILicensing * This,
            /* [in] */ IDispatch *connection,
            /* [retval][out] */ LONG *result);
        
        END_INTERFACE
    } ILicensingVtbl;

    interface ILicensing
    {
        CONST_VTBL struct ILicensingVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ILicensing_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define ILicensing_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define ILicensing_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define ILicensing_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define ILicensing_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define ILicensing_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define ILicensing_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define ILicensing_getLicenseCount(This,connection,result)	\
    ( (This)->lpVtbl -> getLicenseCount(This,connection,result) ) 

#define ILicensing_getCurrentUsersCount(This,connection,result)	\
    ( (This)->lpVtbl -> getCurrentUsersCount(This,connection,result) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __ILicensing_INTERFACE_DEFINED__ */


#ifndef __ILicensing2_INTERFACE_DEFINED__
#define __ILicensing2_INTERFACE_DEFINED__

/* interface ILicensing2 */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_ILicensing2;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("119542DB-0469-4305-9E06-AB5E6459064F")
    ILicensing2 : public ILicensing
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE getTrialTime( 
            /* [in] */ IDispatch *connection,
            /* [retval][out] */ LONG *result) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE getTrialExpire( 
            /* [in] */ IDispatch *connection,
            /* [retval][out] */ LONG *result) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE getTrialState( 
            /* [in] */ IDispatch *connection,
            /* [retval][out] */ BOOL *result) = 0;
        
    };
    
    
#else 	/* C style interface */

    typedef struct ILicensing2Vtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ILicensing2 * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ILicensing2 * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ILicensing2 * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ILicensing2 * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ILicensing2 * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ILicensing2 * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ILicensing2 * This,
            /* [annotation][in] */ 
            _In_  DISPID dispIdMember,
            /* [annotation][in] */ 
            _In_  REFIID riid,
            /* [annotation][in] */ 
            _In_  LCID lcid,
            /* [annotation][in] */ 
            _In_  WORD wFlags,
            /* [annotation][out][in] */ 
            _In_  DISPPARAMS *pDispParams,
            /* [annotation][out] */ 
            _Out_opt_  VARIANT *pVarResult,
            /* [annotation][out] */ 
            _Out_opt_  EXCEPINFO *pExcepInfo,
            /* [annotation][out] */ 
            _Out_opt_  UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *getLicenseCount )( 
            ILicensing2 * This,
            /* [in] */ IDispatch *connection,
            /* [retval][out] */ LONG *result);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *getCurrentUsersCount )( 
            ILicensing2 * This,
            /* [in] */ IDispatch *connection,
            /* [retval][out] */ LONG *result);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *getTrialTime )( 
            ILicensing2 * This,
            /* [in] */ IDispatch *connection,
            /* [retval][out] */ LONG *result);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *getTrialExpire )( 
            ILicensing2 * This,
            /* [in] */ IDispatch *connection,
            /* [retval][out] */ LONG *result);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *getTrialState )( 
            ILicensing2 * This,
            /* [in] */ IDispatch *connection,
            /* [retval][out] */ BOOL *result);
        
        END_INTERFACE
    } ILicensing2Vtbl;

    interface ILicensing2
    {
        CONST_VTBL struct ILicensing2Vtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ILicensing2_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define ILicensing2_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define ILicensing2_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define ILicensing2_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define ILicensing2_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define ILicensing2_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define ILicensing2_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define ILicensing2_getLicenseCount(This,connection,result)	\
    ( (This)->lpVtbl -> getLicenseCount(This,connection,result) ) 

#define ILicensing2_getCurrentUsersCount(This,connection,result)	\
    ( (This)->lpVtbl -> getCurrentUsersCount(This,connection,result) ) 


#define ILicensing2_getTrialTime(This,connection,result)	\
    ( (This)->lpVtbl -> getTrialTime(This,connection,result) ) 

#define ILicensing2_getTrialExpire(This,connection,result)	\
    ( (This)->lpVtbl -> getTrialExpire(This,connection,result) ) 

#define ILicensing2_getTrialState(This,connection,result)	\
    ( (This)->lpVtbl -> getTrialState(This,connection,result) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __ILicensing2_INTERFACE_DEFINED__ */


#ifndef __ILDAPQuery_INTERFACE_DEFINED__
#define __ILDAPQuery_INTERFACE_DEFINED__

/* interface ILDAPQuery */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_ILDAPQuery;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("9D2ACEA7-F523-4DC5-8DA6-8A1160749A69")
    ILDAPQuery : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE execQuery( 
            /* [in] */ BSTR username,
            /* [in] */ BSTR password,
            /* [in] */ BSTR host,
            /* [in] */ BSTR basedn,
            /* [in] */ BSTR scope,
            /* [in] */ BSTR filter,
            /* [in] */ BSTR attributes,
            /* [in] */ VARIANT_BOOL usessl,
            /* [retval][out] */ SAFEARRAY * *result) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE getLastError( 
            /* [retval][out] */ LONG *result) = 0;
        
    };
    
    
#else 	/* C style interface */

    typedef struct ILDAPQueryVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ILDAPQuery * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ILDAPQuery * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ILDAPQuery * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ILDAPQuery * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ILDAPQuery * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ILDAPQuery * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ILDAPQuery * This,
            /* [annotation][in] */ 
            _In_  DISPID dispIdMember,
            /* [annotation][in] */ 
            _In_  REFIID riid,
            /* [annotation][in] */ 
            _In_  LCID lcid,
            /* [annotation][in] */ 
            _In_  WORD wFlags,
            /* [annotation][out][in] */ 
            _In_  DISPPARAMS *pDispParams,
            /* [annotation][out] */ 
            _Out_opt_  VARIANT *pVarResult,
            /* [annotation][out] */ 
            _Out_opt_  EXCEPINFO *pExcepInfo,
            /* [annotation][out] */ 
            _Out_opt_  UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *execQuery )( 
            ILDAPQuery * This,
            /* [in] */ BSTR username,
            /* [in] */ BSTR password,
            /* [in] */ BSTR host,
            /* [in] */ BSTR basedn,
            /* [in] */ BSTR scope,
            /* [in] */ BSTR filter,
            /* [in] */ BSTR attributes,
            /* [in] */ VARIANT_BOOL usessl,
            /* [retval][out] */ SAFEARRAY * *result);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *getLastError )( 
            ILDAPQuery * This,
            /* [retval][out] */ LONG *result);
        
        END_INTERFACE
    } ILDAPQueryVtbl;

    interface ILDAPQuery
    {
        CONST_VTBL struct ILDAPQueryVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ILDAPQuery_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define ILDAPQuery_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define ILDAPQuery_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define ILDAPQuery_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define ILDAPQuery_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define ILDAPQuery_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define ILDAPQuery_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define ILDAPQuery_execQuery(This,username,password,host,basedn,scope,filter,attributes,usessl,result)	\
    ( (This)->lpVtbl -> execQuery(This,username,password,host,basedn,scope,filter,attributes,usessl,result) ) 

#define ILDAPQuery_getLastError(This,result)	\
    ( (This)->lpVtbl -> getLastError(This,result) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __ILDAPQuery_INTERFACE_DEFINED__ */


#ifndef __ILDAPQuery2_INTERFACE_DEFINED__
#define __ILDAPQuery2_INTERFACE_DEFINED__

/* interface ILDAPQuery2 */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_ILDAPQuery2;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("3FC3A94F-1F13-4a0a-8EC3-1F7997355EBE")
    ILDAPQuery2 : public ILDAPQuery
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE connect( 
            /* [in] */ BSTR username,
            /* [in] */ BSTR password,
            /* [in] */ BSTR host,
            /* [in] */ VARIANT_BOOL usessl,
            /* [retval][out] */ LONG *connect_id) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE close( 
            /* [in] */ LONG connect_id) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE query( 
            /* [in] */ LONG connect_id,
            /* [in] */ BSTR basedn,
            /* [in] */ BSTR scope,
            /* [in] */ BSTR filter,
            /* [in] */ BSTR attributes,
            /* [retval][out] */ SAFEARRAY * *result) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE pageQuery( 
            /* [in] */ LONG connect_id,
            /* [in] */ BSTR basedn,
            /* [in] */ BSTR scope,
            /* [in] */ BSTR filter,
            /* [in] */ BSTR attributes,
            /* [in] */ ULONG total_results,
            /* [retval][out] */ LONG *results_id) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE nextResults( 
            /* [in] */ LONG results_id,
            /* [in] */ ULONG num_results,
            /* [retval][out] */ SAFEARRAY * *result) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE rename( 
            /* [in] */ LONG connect_id,
            /* [in] */ BSTR dn,
            /* [in] */ BSTR newRDN,
            /* [in] */ BSTR newParent,
            /* [in] */ BOOL deleteOldRdn) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE dn2ufn( 
            /* [in] */ BSTR dn,
            /* [retval][out] */ BSTR *ufn) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ufn2dn( 
            /* [in] */ BSTR ufn,
            /* [retval][out] */ BSTR *dn) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE err2string( 
            /* [in] */ ULONG ufn,
            /* [retval][out] */ BSTR *string) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE explodeDn( 
            /* [in] */ BSTR str,
            /* [in] */ ULONG notypes,
            /* [retval][out] */ SAFEARRAY * *result) = 0;
        
    };
    
    
#else 	/* C style interface */

    typedef struct ILDAPQuery2Vtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ILDAPQuery2 * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ILDAPQuery2 * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ILDAPQuery2 * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ILDAPQuery2 * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ILDAPQuery2 * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ILDAPQuery2 * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ILDAPQuery2 * This,
            /* [annotation][in] */ 
            _In_  DISPID dispIdMember,
            /* [annotation][in] */ 
            _In_  REFIID riid,
            /* [annotation][in] */ 
            _In_  LCID lcid,
            /* [annotation][in] */ 
            _In_  WORD wFlags,
            /* [annotation][out][in] */ 
            _In_  DISPPARAMS *pDispParams,
            /* [annotation][out] */ 
            _Out_opt_  VARIANT *pVarResult,
            /* [annotation][out] */ 
            _Out_opt_  EXCEPINFO *pExcepInfo,
            /* [annotation][out] */ 
            _Out_opt_  UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *execQuery )( 
            ILDAPQuery2 * This,
            /* [in] */ BSTR username,
            /* [in] */ BSTR password,
            /* [in] */ BSTR host,
            /* [in] */ BSTR basedn,
            /* [in] */ BSTR scope,
            /* [in] */ BSTR filter,
            /* [in] */ BSTR attributes,
            /* [in] */ VARIANT_BOOL usessl,
            /* [retval][out] */ SAFEARRAY * *result);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *getLastError )( 
            ILDAPQuery2 * This,
            /* [retval][out] */ LONG *result);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *connect )( 
            ILDAPQuery2 * This,
            /* [in] */ BSTR username,
            /* [in] */ BSTR password,
            /* [in] */ BSTR host,
            /* [in] */ VARIANT_BOOL usessl,
            /* [retval][out] */ LONG *connect_id);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *close )( 
            ILDAPQuery2 * This,
            /* [in] */ LONG connect_id);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *query )( 
            ILDAPQuery2 * This,
            /* [in] */ LONG connect_id,
            /* [in] */ BSTR basedn,
            /* [in] */ BSTR scope,
            /* [in] */ BSTR filter,
            /* [in] */ BSTR attributes,
            /* [retval][out] */ SAFEARRAY * *result);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *pageQuery )( 
            ILDAPQuery2 * This,
            /* [in] */ LONG connect_id,
            /* [in] */ BSTR basedn,
            /* [in] */ BSTR scope,
            /* [in] */ BSTR filter,
            /* [in] */ BSTR attributes,
            /* [in] */ ULONG total_results,
            /* [retval][out] */ LONG *results_id);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *nextResults )( 
            ILDAPQuery2 * This,
            /* [in] */ LONG results_id,
            /* [in] */ ULONG num_results,
            /* [retval][out] */ SAFEARRAY * *result);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *rename )( 
            ILDAPQuery2 * This,
            /* [in] */ LONG connect_id,
            /* [in] */ BSTR dn,
            /* [in] */ BSTR newRDN,
            /* [in] */ BSTR newParent,
            /* [in] */ BOOL deleteOldRdn);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *dn2ufn )( 
            ILDAPQuery2 * This,
            /* [in] */ BSTR dn,
            /* [retval][out] */ BSTR *ufn);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ufn2dn )( 
            ILDAPQuery2 * This,
            /* [in] */ BSTR ufn,
            /* [retval][out] */ BSTR *dn);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *err2string )( 
            ILDAPQuery2 * This,
            /* [in] */ ULONG ufn,
            /* [retval][out] */ BSTR *string);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *explodeDn )( 
            ILDAPQuery2 * This,
            /* [in] */ BSTR str,
            /* [in] */ ULONG notypes,
            /* [retval][out] */ SAFEARRAY * *result);
        
        END_INTERFACE
    } ILDAPQuery2Vtbl;

    interface ILDAPQuery2
    {
        CONST_VTBL struct ILDAPQuery2Vtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ILDAPQuery2_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define ILDAPQuery2_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define ILDAPQuery2_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define ILDAPQuery2_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define ILDAPQuery2_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define ILDAPQuery2_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define ILDAPQuery2_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define ILDAPQuery2_execQuery(This,username,password,host,basedn,scope,filter,attributes,usessl,result)	\
    ( (This)->lpVtbl -> execQuery(This,username,password,host,basedn,scope,filter,attributes,usessl,result) ) 

#define ILDAPQuery2_getLastError(This,result)	\
    ( (This)->lpVtbl -> getLastError(This,result) ) 


#define ILDAPQuery2_connect(This,username,password,host,usessl,connect_id)	\
    ( (This)->lpVtbl -> connect(This,username,password,host,usessl,connect_id) ) 

#define ILDAPQuery2_close(This,connect_id)	\
    ( (This)->lpVtbl -> close(This,connect_id) ) 

#define ILDAPQuery2_query(This,connect_id,basedn,scope,filter,attributes,result)	\
    ( (This)->lpVtbl -> query(This,connect_id,basedn,scope,filter,attributes,result) ) 

#define ILDAPQuery2_pageQuery(This,connect_id,basedn,scope,filter,attributes,total_results,results_id)	\
    ( (This)->lpVtbl -> pageQuery(This,connect_id,basedn,scope,filter,attributes,total_results,results_id) ) 

#define ILDAPQuery2_nextResults(This,results_id,num_results,result)	\
    ( (This)->lpVtbl -> nextResults(This,results_id,num_results,result) ) 

#define ILDAPQuery2_rename(This,connect_id,dn,newRDN,newParent,deleteOldRdn)	\
    ( (This)->lpVtbl -> rename(This,connect_id,dn,newRDN,newParent,deleteOldRdn) ) 

#define ILDAPQuery2_dn2ufn(This,dn,ufn)	\
    ( (This)->lpVtbl -> dn2ufn(This,dn,ufn) ) 

#define ILDAPQuery2_ufn2dn(This,ufn,dn)	\
    ( (This)->lpVtbl -> ufn2dn(This,ufn,dn) ) 

#define ILDAPQuery2_err2string(This,ufn,string)	\
    ( (This)->lpVtbl -> err2string(This,ufn,string) ) 

#define ILDAPQuery2_explodeDn(This,str,notypes,result)	\
    ( (This)->lpVtbl -> explodeDn(This,str,notypes,result) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __ILDAPQuery2_INTERFACE_DEFINED__ */



#ifndef __DeskAlertsServerLib_LIBRARY_DEFINED__
#define __DeskAlertsServerLib_LIBRARY_DEFINED__

/* library DeskAlertsServerLib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_DeskAlertsServerLib;

EXTERN_C const CLSID CLSID_AlertXML;

#ifdef __cplusplus

class DECLSPEC_UUID("FCA25450-E1AD-4FC2-9CC3-C3A8B8BD6F8C")
AlertXML;
#endif

EXTERN_C const CLSID CLSID_Encrypt;

#ifdef __cplusplus

class DECLSPEC_UUID("C464F969-00EC-4972-8628-E7C1A0F79901")
Encrypt;
#endif

EXTERN_C const CLSID CLSID_Licensing;

#ifdef __cplusplus

class DECLSPEC_UUID("08D1FC3E-FC8C-4626-9074-5CF8770DE036")
Licensing;
#endif

EXTERN_C const CLSID CLSID_LDAPQuery;

#ifdef __cplusplus

class DECLSPEC_UUID("C5942E1C-63E3-46A8-826D-2CF283031762")
LDAPQuery;
#endif
#endif /* __DeskAlertsServerLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long *, unsigned long            , BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserMarshal(  unsigned long *, unsigned char *, BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserUnmarshal(unsigned long *, unsigned char *, BSTR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long *, BSTR * ); 

unsigned long             __RPC_USER  LPSAFEARRAY_UserSize(     unsigned long *, unsigned long            , LPSAFEARRAY * ); 
unsigned char * __RPC_USER  LPSAFEARRAY_UserMarshal(  unsigned long *, unsigned char *, LPSAFEARRAY * ); 
unsigned char * __RPC_USER  LPSAFEARRAY_UserUnmarshal(unsigned long *, unsigned char *, LPSAFEARRAY * ); 
void                      __RPC_USER  LPSAFEARRAY_UserFree(     unsigned long *, LPSAFEARRAY * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


