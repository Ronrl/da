// AlertXML.cpp : Implementation of CAlertXML

#include "stdafx.h"
#include "AlertXML.h"
#include "Licensing.h"
#include "sha1.h"
#include "MD5Hash.h"

#include <atlsafe.h>
// CAlertXML

CAlertXML::CAlertXML() : m_userId(-2), m_timeout(0), m_plugin(L"alert:ID3")
{
}

CAlertXML::~CAlertXML()
{
}

//IAlertXML
STDMETHODIMP CAlertXML::setUserId( 
	/* [in] */ IDispatch *connection,
	/* [in] */ LONG id)
{
	RELEASE_TRY
	{
		if(CLicensing::checkLicenseAndTrialState(connection))
		{
			m_userId = id;
		}
	}
	RELEASE_CATCH_ALL;
	return S_OK;
}

STDMETHODIMP CAlertXML::setTimeout( 
	/* [in] */ LONG timeout)
{
	m_timeout = timeout;
	return S_OK;
}

STDMETHODIMP CAlertXML::setPlugin( 
	/* [in] */ BSTR plugin)
{
	m_plugin = plugin;
	return S_OK;
}

STDMETHODIMP CAlertXML::setAlertURL( 
	/* [in] */ BSTR url)
{
	m_alertURL = url;
	return S_OK;
}

STDMETHODIMP CAlertXML::setSurveyURL( 
	/* [in] */ BSTR url)
{
	m_surveyURL = url;
	return S_OK;
}

STDMETHODIMP CAlertXML::setDeskbarId( 
	/* [in] */ BSTR id)
{
	m_deskbarId = id;
	return S_OK;
}

STDMETHODIMP CAlertXML::addAlert( 
	/* [in] */ BOOL urgent,
	/* [in] */ LONG id,
	/* [in] */ LONG aknown,
	/* [in] */ LONG autoclose,
	/* [in] */ LONG create_timestamp,
	/* [in] */ BSTR title,
	/* [in] */ BSTR position,
	/* [in] */ BSTR width,
	/* [in] */ BSTR height,
	/* [in] */ LONG ticker,
	/* [in] */ LONG schedule,
	/* [in] */ BOOL history)
{
	HRESULT res = S_OK;
	RELEASE_TRY
	{
		CStringW href;
		href.Format(L"%ws?id=%ld&user_id=%ld&deskbar_id=%ws", m_alertURL, id, m_userId, m_deskbarId);
		res = addCustomAlert(urgent, CComBSTR(href), id, aknown, autoclose, create_timestamp, title, position, width, height, ticker, schedule, history);
	}
	RELEASE_CATCH_ALL;
	return res;
}

STDMETHODIMP CAlertXML::addCustomAlert( 
	/* [in] */ BOOL urgent,
	/* [in] */ BSTR href,
	/* [in] */ LONG id,
	/* [in] */ LONG aknown,
	/* [in] */ LONG autoclose,
	/* [in] */ LONG create_timestamp,
	/* [in] */ BSTR title,
	/* [in] */ BSTR position,
	/* [in] */ BSTR width,
	/* [in] */ BSTR height,
	/* [in] */ LONG ticker,
	/* [in] */ LONG schedule,
	/* [in] */ BOOL history)
{
	RELEASE_TRY
	{
		if(m_userId > -2)
		{
			CStringW alert;
			alert.Format(L"<ALERT id=\"alert\" href=\"%ws\" timeout=\"%ld\" expire=\"\" plugin=\"%ws\" priority=\"1\" acknown=\"%ld\" autoclose=\"%ld\" alert_id=\"%ld\" user_id=\"%ld\" create_date=\"%ld\" utc=\"1\" title=\"%ws\" position=\"%ws\" width=\"%ws\" height=\"%ws\" ticker=\"%ld\" survey=\"0\" schedule=\"%ld\" urgent=\"%ld\" history=\"%ld\"/>",
				escape(href),
				m_timeout,
				escape(m_plugin),
				aknown,
				autoclose,
				id,
				m_userId,
				create_timestamp,
				CStringW(title), //escaped by script
				escape(position),
				escape(width),
				escape(height),
				ticker,
				schedule,
				(LONG)urgent?1L:0L,
				(LONG)history?1L:0L);
			if(urgent)
				m_urgentXML += alert;
			else
				m_usualXML += alert;
		}
	}
	RELEASE_CATCH_ALL;
	return S_OK;
}

STDMETHODIMP CAlertXML::addSurvey( 
	/* [in] */ BOOL urgent,
	/* [in] */ LONG id,
	/* [in] */ LONG create_timestamp,
	/* [in] */ BSTR title,
	/* [in] */ LONG schedule,
	/* [in] */ BOOL history)
{
	RELEASE_TRY
	{
		if(m_userId > -2)
		{
			CStringW alert;
			alert.Format(L"<ALERT id=\"alert\" href=\"%ws?id=%ld&amp;user_id=%ld&amp;deskbar_id=%ws\" timeout=\"%ld\" expire=\"\"  plugin=\"%ws\" priority=\"1\" create_date=\"%ld\" utc=\"1\" title=\"%ws\" ticker=\"0\" survey=\"1\" schedule=\"%ld\" urgent=\"%ld\" history=\"%ld\"/>",
				escape(m_surveyURL),
				id,
				m_userId,
				escape(m_deskbarId),
				m_timeout,
				escape(m_plugin),
				create_timestamp,
				CStringW(title), //escaped by script
				schedule,
				(LONG)urgent?1L:0L,
				(LONG)history?1L:0L);
			if(urgent)
				m_urgentXML += alert;
			else
				m_usualXML += alert;
		}
	}
	RELEASE_CATCH_ALL;
	return S_OK;
}

STDMETHODIMP CAlertXML::getResult( 
	/* [retval][out] */ BSTR *result)
{
	RELEASE_TRY
	{
		CComBSTR res = m_urgentXML;
		res += m_usualXML;
		*result = res.Detach();
	}
	RELEASE_CATCH_ALL;
	return S_OK;
}

STDMETHODIMP CAlertXML::getUTF8Result( 
	/* [retval][out] */ SAFEARRAY **result)
{
	RELEASE_TRY
	{
		CComBSTR res = m_urgentXML;
		res += m_usualXML;
		CComSafeArray<BYTE, VT_UI1> resArray;
		if(res.Length() > 0)
		{
			CW2A resChars(res, CP_UTF8);
			resArray.Add((ULONG)strlen(resChars), (const BYTE*)(const CHAR*)resChars, false);
		}
		*result = resArray.Detach();
	}
	RELEASE_CATCH_ALL;
	return S_OK;
}

//IAlertXML2
STDMETHODIMP CAlertXML::setUserInfo( 
									  /* [in] */ IDispatch *connection,
									  /* [in] */ LONG id,
									  /* [in] */ BSTR name,
									  /* [in] */ BSTR hash,
									  /* [retval][out] */ BOOL *result)
{
	RELEASE_TRY
	{
		*result = FALSE;
		if(checkUserHash(name, hash) && CLicensing::checkLicenseAndTrialState(connection))
		{
			m_userId = id;
			*result = TRUE;
		}
	}
	RELEASE_CATCH_ALL;
	return S_OK;
}

STDMETHODIMP CAlertXML::setAllUserInfo(
	/* [in] */ IDispatch *connection,
	/* [in] */ LONG id,
	/* [in] */ BSTR name,
	/* [in] */ BSTR context,
	/* [in] */ BSTR domain,
	/* [in] */ BSTR hash,
	/* [retval][out] */ BOOL *result)
{
	RELEASE_TRY
	{
		*result = FALSE;
		CW2A utf8name(name, CP_UTF8);
		CW2A utf8context(context, CP_UTF8);
		CW2A utf8domain(domain, CP_UTF8);
		CString str = CMD5Hash::GetMD5((const BYTE*)(const CHAR*)utf8name, strlen(utf8name));
		str += CMD5Hash::GetMD5((const BYTE*)(const CHAR*)utf8context, strlen(utf8context));
		str += CMD5Hash::GetMD5((const BYTE*)(const CHAR*)utf8domain, strlen(utf8domain));
		if(checkUserHash(str, hash) && CLicensing::checkLicenseAndTrialState(connection))
		{
			m_userId = id;
			*result = TRUE;
		}
	}
	RELEASE_CATCH_ALL;
	return S_OK;
}

STDMETHODIMP CAlertXML::addAlert2( 
	/* [in] */ BOOL urgent,
	/* [in] */ LONG id,
	/* [in] */ LONG aknown,
	/* [in] */ LONG autoclose,
	/* [in] */ LONG create_timestamp,
	/* [in] */ BSTR title,
	/* [in] */ BSTR position,
	/* [in] */ BSTR visible,
	/* [in] */ BSTR width,
	/* [in] */ BSTR height,
	/* [in] */ LONG ticker,
	/* [in] */ LONG schedule,
	/* [in] */ LONG recurrence,
	/* [in] */ BOOL history,
	/* [in] */ BSTR innner,
	/* [retval][out] */ BOOL *result)
{
	HRESULT res = S_OK;
	RELEASE_TRY
	{
		*result = FALSE;
		CStringW href;
		href.Format(L"%ws?id=%ld&user_id=%ld&deskbar_id=%ws", m_alertURL, id, m_userId, m_deskbarId);
		res = addCustomAlert2(urgent, CComBSTR(href), id, aknown, autoclose, create_timestamp, title, position, visible, width, height, ticker, schedule, recurrence, history, innner, result);
	}
	RELEASE_CATCH_ALL;
	return res;
}

STDMETHODIMP CAlertXML::addCustomAlert2( 
	/* [in] */ BOOL urgent,
	/* [in] */ BSTR href,
	/* [in] */ LONG id,
	/* [in] */ LONG aknown,
	/* [in] */ LONG autoclose,
	/* [in] */ LONG create_timestamp,
	/* [in] */ BSTR title,
	/* [in] */ BSTR position,
	/* [in] */ BSTR visible,
	/* [in] */ BSTR width,
	/* [in] */ BSTR height,
	/* [in] */ LONG ticker,
	/* [in] */ LONG schedule,
	/* [in] */ LONG recurrence,
	/* [in] */ BOOL history,
	/* [in] */ BSTR innner,
	/* [retval][out] */ BOOL *result)
{
	RELEASE_TRY
	{
		*result = FALSE;
		if(m_userId > -2)
		{
			CStringW alert, inn(innner);
			alert.Format(L"<ALERT id=\"alert\" href=\"%ws\" timeout=\"%ld\" expire=\"\" plugin=\"%ws\" priority=\"1\" acknown=\"%ld\" autoclose=\"%ld\" alert_id=\"%ld\" user_id=\"%ld\" create_date=\"%ld\" utc=\"1\" title=\"%ws\" position=\"%ws\" visible=\"%ws\" width=\"%ws\" height=\"%ws\" ticker=\"%ld\" survey=\"0\" schedule=\"%ld\" recurrence=\"%ld\" urgent=\"%ld\" history=\"%ld\"%ws" ,
				escape(href),
				m_timeout,
				escape(m_plugin),
				aknown,
				autoclose,
				id,
				m_userId,
				create_timestamp,
				escape(title),
				escape(position),
				escape(visible),
				escape(width),
				escape(height),
				ticker,
				schedule,
				recurrence,
				(LONG)urgent?1L:0L,
				(LONG)history?1L:0L,
				inn.IsEmpty()?L"/>":L">"+inn+L"</ALERT>");
			if(urgent)
				m_urgentXML += alert;
			else
				m_usualXML += alert;
			*result = TRUE;
		}
	}
	RELEASE_CATCH_ALL;
	return S_OK;
}

STDMETHODIMP CAlertXML::addSurvey2( 
	/* [in] */ BOOL urgent,
	/* [in] */ LONG id,
	/* [in] */ LONG create_timestamp,
	/* [in] */ BSTR title,
	/* [in] */ BSTR position,
	/* [in] */ BSTR visible,
	/* [in] */ BSTR width,
	/* [in] */ BSTR height,
	/* [in] */ LONG schedule,
	/* [in] */ LONG recurrence,
	/* [in] */ BOOL history,
	/* [in] */ BSTR innner,
	/* [retval][out] */ BOOL *result)
{
	RELEASE_TRY
	{
		*result = FALSE;
		if(m_userId > -2)
		{
			CStringW alert, inn(innner);
			alert.Format(L"<ALERT id=\"alert\" href=\"%ws?id=%ld&amp;user_id=%ld&amp;deskbar_id=%ws\" timeout=\"%ld\" expire=\"\"  plugin=\"%ws\" priority=\"1\" create_date=\"%ld\" utc=\"1\" title=\"%ws\" position=\"%ws\" visible=\"%ws\" width=\"%ws\" height=\"%ws\" ticker=\"0\" survey=\"1\" schedule=\"%ld\" recurrence=\"%ld\" urgent=\"%ld\" history=\"%ld\"%ws",
				escape(m_surveyURL),
				id,
				m_userId,
				escape(m_deskbarId),
				m_timeout,
				escape(m_plugin),
				create_timestamp,
				escape(title),
				escape(position),
				escape(visible),
				escape(width),
				escape(height),
				schedule,
				recurrence,
				(LONG)urgent?1L:0L,
				(LONG)history?1L:0L,
				inn.IsEmpty()?L"/>":L">"+inn+L"</ALERT>");
			if(urgent)
				m_urgentXML += alert;
			else
				m_usualXML += alert;
			*result = TRUE;
		}
	}
	RELEASE_CATCH_ALL;
	return S_OK;
}

STDMETHODIMP CAlertXML::addSurvey3( 
								   /* [in] */ BOOL urgent,
								   /* [in] */ LONG id,
								   /* [in] */ LONG create_timestamp,
								   /* [in] */ BSTR title,
                                   /* [in] */ LONG autoclose,
								   /* [in] */ BSTR position,
								   /* [in] */ BSTR visible,
								   /* [in] */ BSTR width,
								   /* [in] */ BSTR height,
								   /* [in] */ LONG schedule,
								   /* [in] */ LONG recurrence,
								   /* [in] */ BOOL history,
								   /* [in] */ LONG classId,
								   /* [in] */ BSTR innner,
								   /* [retval][out] */ BOOL *result)
{
	RELEASE_TRY
	{
		*result = FALSE;
		if(m_userId > -2)
		{
			CStringW alert, inn(innner);
		alert.Format(L"<ALERT id=\"alert\" href=\"%ws?id=%ld&amp;user_id=%ld&amp;deskbar_id=%ws\" autoclose=\"%ld\" timeout=\"%ld\" expire=\"\"  plugin=\"%ws\" priority=\"1\" create_date=\"%ld\" utc=\"1\" title=\"%ws\" position=\"%ws\" visible=\"%ws\" width=\"%ws\" height=\"%ws\" ticker=\"0\" survey=\"1\" schedule=\"%ld\" recurrence=\"%ld\" urgent=\"%ld\" history=\"%ld\" class_id=\"%ld\"%ws",
				escape(m_surveyURL),
				id,
				m_userId,
				escape(m_deskbarId),
				autoclose,
				m_timeout,
				escape(m_plugin),
				create_timestamp,
				escape(title),
				escape(position),
				escape(visible),
				escape(width),
				escape(height),
				schedule,
				recurrence,
				(LONG)urgent?1L:0L,
				(LONG)history?1L:0L,
				classId,
				inn.IsEmpty()?L"/>":L">"+inn+L"</ALERT>");
			if(urgent)
				m_urgentXML += alert;
			else
				m_usualXML += alert;
			*result = TRUE;
		}
	}
	RELEASE_CATCH_ALL;
	return S_OK;
}

STDMETHODIMP CAlertXML::addSurvey4( 
								   /* [in] */ BOOL urgent,
								   /* [in] */ LONG id,
								   /* [in] */ LONG create_timestamp,
								   /* [in] */ BSTR title,
								   /* [in] */ LONG autoclose,
								   /* [in] */ BSTR position,
								   /* [in] */ BSTR visible,
								   /* [in] */ BSTR width,
								   /* [in] */ BSTR height,
								   /* [in] */ LONG schedule,
								   /* [in] */ LONG recurrence,
								   /* [in] */ BOOL history,
								   /* [in] */ LONG classId,
                                   /* [in] */ LONG resizable,
                                   /* [in] */ LONG to_date,
								   /* [in] */ BSTR innner,
								   /* [retval][out] */ BOOL *result)
{
	RELEASE_TRY
	{
		*result = FALSE;
		if(m_userId > -2)
		{
			CStringW alert, inn(innner);
			alert.Format(L"<ALERT id=\"alert\" href=\"%ws?id=%ld&amp;user_id=%ld&amp;deskbar_id=%ws\" autoclose=\"%ld\" timeout=\"%ld\" expire=\"\"  plugin=\"%ws\" priority=\"1\" create_date=\"%ld\" utc=\"1\" title=\"%ws\" position=\"%ws\" visible=\"%ws\" width=\"%ws\" height=\"%ws\" ticker=\"0\" survey=\"1\" schedule=\"%ld\" recurrence=\"%ld\" urgent=\"%ld\" history=\"%ld\" class_id=\"%ld\" resizable=\"%ld\" to_date=\"%ld\"%ws",
				escape(m_surveyURL),
				id,
				m_userId,
				escape(m_deskbarId),
				autoclose,
				m_timeout,
				escape(m_plugin),
				create_timestamp,
				escape(title),
				escape(position),
				escape(visible),
				escape(width),
				escape(height),
				schedule,
				recurrence,
				(LONG)urgent?1L:0L,
				(LONG)history?1L:0L,
				classId,
                resizable,
                to_date,
				inn.IsEmpty()?L"/>":L">"+inn+L"</ALERT>");
			if(urgent)
				m_urgentXML += alert;
			else
				m_usualXML += alert;
			*result = TRUE;
		}
	}
	RELEASE_CATCH_ALL;
	return S_OK;
}

/*static*/ BOOL CAlertXML::checkUserHash(CStringW name, CStringW hash)
{
	BOOL result = FALSE;
	RELEASE_TRY
	{
		if(hash.GetLength() == 48)
		{
			SHA1 sha1;
			CStringW rnd = hash.Mid(27,2) + hash.Mid(8,3) + hash.Mid(37,3);
			CT2A calc(rnd.Mid(3,3) + name + rnd.Mid(6,2) + _T("dR5Hg") + rnd.Left(3), CP_UTF8);
			sha1.Input(calc, static_cast<int>(strlen(calc)));
			unsigned res[5];
			if(sha1.Result((unsigned *)&res))
			{
				if(_tcstoul(hash.Left(8),NULL,16) == res[2] &&
					_tcstoul(hash.Mid(11,8),NULL,16) == res[1] &&
					_tcstoul(hash.Mid(19,8),NULL,16) == res[4] &&
					_tcstoul(hash.Mid(29,8),NULL,16) == res[3] &&
					_tcstoul(hash.Mid(40,8),NULL,16) == res[0])
				{
					result = TRUE;
				}
			}
		}
	}
	RELEASE_CATCH_ALL;
	return result;
}

/*static*/ CStringW CAlertXML::escape(CStringW str)
{
	str.Replace(_T("&"), _T("&amp;"));
	str.Replace(_T("\""), _T("&quot;"));
	str.Replace(_T("'"), _T("&apos;"));
	str.Replace(_T("<"), _T("&lt;"));
	str.Replace(_T(">"), _T("&gt;"));
	return str;
}

//IAlertXML3
STDMETHODIMP CAlertXML::addAlert3( 
	/* [in] */ BOOL urgent,
	/* [in] */ LONG id,
	/* [in] */ LONG aknown,
	/* [in] */ LONG autoclose,
	/* [in] */ LONG create_timestamp,
	/* [in] */ BSTR title,
	/* [in] */ BSTR position,
	/* [in] */ BSTR visible,
	/* [in] */ BSTR width,
	/* [in] */ BSTR height,
	/* [in] */ LONG ticker,
	/* [in] */ LONG schedule,
	/* [in] */ LONG recurrence,
	/* [in] */ BOOL history,
	/* [in] */ LONG classId,
	/* [in] */ BSTR innner,
	/* [retval][out] */ BOOL *result)
{
	HRESULT res = S_OK;
	RELEASE_TRY
	{
		*result = FALSE;
		CStringW href;
		href.Format(L"%ws?id=%ld&user_id=%ld&deskbar_id=%ws", m_alertURL, id, m_userId, m_deskbarId);
		res = addCustomAlert3(urgent, CComBSTR(href), id, aknown, autoclose, create_timestamp, title, position, visible, width, height, ticker, schedule, recurrence, history, classId, innner, result);
	}
	RELEASE_CATCH_ALL;
	return res;
}

STDMETHODIMP CAlertXML::addCustomAlert3( 
	/* [in] */ BOOL urgent,
	/* [in] */ BSTR href,
	/* [in] */ LONG id,
	/* [in] */ LONG aknown,
	/* [in] */ LONG autoclose,
	/* [in] */ LONG create_timestamp,
	/* [in] */ BSTR title,
	/* [in] */ BSTR position,
	/* [in] */ BSTR visible,
	/* [in] */ BSTR width,
	/* [in] */ BSTR height,
	/* [in] */ LONG ticker,
	/* [in] */ LONG schedule,
	/* [in] */ LONG recurrence,
	/* [in] */ BOOL history,
	/* [in] */ LONG classId,
	/* [in] */ BSTR innner,
	/* [retval][out] */ BOOL *result)
{
	RELEASE_TRY
	{
		*result = FALSE;
		if(m_userId > -2)
		{
			CStringW alert, inn(innner);
			alert.Format(L"<ALERT id=\"alert\" href=\"%ws\" timeout=\"%ld\" expire=\"\" plugin=\"%ws\" priority=\"1\" acknown=\"%ld\" autoclose=\"%ld\" alert_id=\"%ld\" user_id=\"%ld\" create_date=\"%ld\" utc=\"1\" title=\"%ws\" position=\"%ws\" visible=\"%ws\" width=\"%ws\" height=\"%ws\" ticker=\"%ld\" survey=\"0\" schedule=\"%ld\" recurrence=\"%ld\" urgent=\"%ld\" history=\"%ld\" class_id =\"%ld\"%ws" ,
				escape(href),
				m_timeout,
				escape(m_plugin),
				aknown,
				autoclose,
				id,
				m_userId,
				create_timestamp,
				escape(title),
				escape(position),
				escape(visible),
				escape(width),
				escape(height),
				ticker,
				schedule,
				recurrence,
				(LONG)urgent?1L:0L,
				(LONG)history?1L:0L,
				classId,
				inn.IsEmpty()?L"/>":L">"+inn+L"</ALERT>");
			if(urgent)
				m_urgentXML += alert;
			else
				m_usualXML += alert;
			*result = TRUE;
		}
	}
	RELEASE_CATCH_ALL;
	return S_OK;
}

STDMETHODIMP CAlertXML::addAlert4(
	/* [in] */ IDispatch* params,
	/* [in] */ BSTR innner,
	/* [retval][out] */ BOOL *result)
{
	RELEASE_TRY
	{
		*result = FALSE;
		if(m_userId > -2)
		{
			LONG urgent = 0L;
			CStringW alert = L"<ALERT ", inn(innner);
			CComQIPtr<Scripting::IDictionary> paramsDict = params;
			if(paramsDict)
			{
				LONG alertId = 0L;
				CComVariant vAlertId, vUrgent;

				CComVariant alertIdName(L"alert_id");
				if(paramsDict->get_Item(&alertIdName, &vAlertId) == S_OK)
				{	
					if(vAlertId.vt == VT_DECIMAL)
					{
						vAlertId.ChangeType(VT_I8);
					}
					if(vAlertId.vt == VT_I4 || vAlertId.vt == VT_I8)
					{
						alertId = vAlertId.lVal;
					}
				}

				CComVariant urgentName(L"urgent");
				if(paramsDict->get_Item(&urgentName, &vUrgent) == S_OK)
				{
					if(vUrgent.vt == VT_DECIMAL)
					{
						vUrgent.ChangeType(VT_I8);
					}
					if(vUrgent.vt == VT_I4 || vUrgent.vt == VT_I8)
					{
						urgent = vUrgent.lVal;
					}
					else if(vUrgent.vt == VT_BOOL)
					{
						urgent = vUrgent.boolVal ? 1L : 0L;
					}
				}

				CComVariant idName(L"id");
				if(paramsDict->Exists(&idName) == VARIANT_FALSE)
				{
					CComVariant value(L"alert");
					paramsDict->Add(&idName, &value);
				}

				CComVariant hrefName(L"href");
				if(paramsDict->Exists(&hrefName) == VARIANT_FALSE)
				{
					CStringW href;
					href.Format(L"%ws?id=%ld&user_id=%ld&deskbar_id=%ws", m_alertURL, alertId, m_userId, m_deskbarId);
					CComVariant value(href);
					paramsDict->Add(&hrefName, &value);
				}

				CComVariant timeoutName(L"timeout");
				if(paramsDict->Exists(&timeoutName) == VARIANT_FALSE)
				{
					CComVariant value(m_timeout);
					paramsDict->Add(&timeoutName, &value);
				}

				CComVariant expireName(L"expire");
				if(paramsDict->Exists(&expireName) == VARIANT_FALSE)
				{
					CComVariant value(L"");
					paramsDict->Add(&expireName, &value);
				}

				CComVariant priorityName(L"priority");
				if(paramsDict->Exists(&priorityName) == VARIANT_FALSE)
				{
					CComVariant value(1L);
					paramsDict->Add(&priorityName, &value);
				}

				CComVariant pluginName(L"plugin");
				if(paramsDict->Exists(&pluginName) == VARIANT_FALSE)
				{
					CComVariant value(escape(m_plugin));
					paramsDict->Add(&pluginName, &value);
				}

				CComVariant userIdName(L"user_id");
				if(paramsDict->Exists(&userIdName) == VARIANT_FALSE)
				{
					CComVariant value(m_userId);
					paramsDict->Add(&userIdName, &value);
				}

				CComVariant surveyName(L"survey");
				if(paramsDict->Exists(&surveyName) == VARIANT_FALSE)
				{
					CComVariant value(0L);
					paramsDict->Add(&surveyName, &value);
				}

				CComVariant utcName(L"utc");
				if(paramsDict->Exists(&utcName) == VARIANT_FALSE)
				{
					CComVariant value(1L);
					paramsDict->Add(&utcName, &value);
				}

				CComQIPtr<IEnumVARIANT> pNewEnum = paramsDict->_NewEnum();
				if(pNewEnum)
				{
					CComVariant vKey;
					while(pNewEnum->Next(1, &vKey, NULL) == S_OK)
					{
						if(vKey.vt == VT_BSTR)
						{
							CStringW key = vKey.bstrVal;
							CComVariant vValue;
							if(paramsDict->get_Item(&vKey, &vValue) == S_OK)
							{
								CStringW prop;
								if (vValue.vt == VT_DECIMAL)
								{
									vValue.ChangeType(VT_I8);
								}
								if (vValue.vt == VT_I8)
								{
									prop.Format(L"%s=\"%ld\" ", key, vValue.lVal);
								}
								else if(vValue.vt == VT_BSTR)
								{
									CStringW val = vValue.bstrVal;
									prop.Format(L"%s=\"%s\" ", key, escape(val));
								}
								else if (vValue.vt == VT_BOOL)
								{
									prop.Format(L"%s=\"%d\" ", key, vValue.boolVal?1:0);
								}
								else if (vValue.vt == VT_I4)
								{
									prop.Format(L"%s=\"%d\" ", key, vValue.lVal);
								}
								else if (vValue.vt == VT_EMPTY)
								{
									prop.Format(L"%s=\"\" ", key);
								}
								alert += prop;
							}
						}
					}
				}	
				alert += inn.IsEmpty()?L"/>":L">"+inn+L"</ALERT>";
			}
			if(urgent)
				m_urgentXML += alert;
			else
				m_usualXML += alert;
			*result = TRUE;
		}
	}
	RELEASE_CATCH_ALL;
	return S_OK;
}
