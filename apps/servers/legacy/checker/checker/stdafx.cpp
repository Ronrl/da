// stdafx.cpp : source file that includes just the standard includes
// checker.pch will be the pre-compiled header
// stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"

#pragma data_seg(".SHARED")

volatile time_t g_LastCheck = 0;
volatile BOOL g_FirstLoad = TRUE;
volatile BOOL g_LicenseLockedState = FALSE;
volatile BOOL g_TrialLockedState = FALSE;
volatile LONG g_CurrentUsersCount = -1;
volatile LONG g_ElipseSinseServerWasInstalled = -1;
volatile BOOL g_FailedUsersCountSelect = FALSE;

#pragma data_seg()
#pragma comment(linker, "/SECTION:.SHARED,RWS")
