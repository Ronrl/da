// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently,
// but are changed infrequently

#pragma once

#ifndef STRICT
#define STRICT
#endif

#include "targetver.h"

#define _ATL_APARTMENT_THREADED
#define _ATL_NO_AUTOMATIC_NAMESPACE

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// some CString constructors will be explicit
#pragma comment(lib, "Wldap32")

#include "resource.h"
#include <atlbase.h>
#include <atlcom.h>
#include <atlsafe.h>
#include <atlctl.h>

#include <windows.h>
#include <winldap.h>

using namespace ATL;

#import "MSADO15.DLL" rename_namespace("ADOCust") rename("EOF","EndOfFile")

#undef GetFreeSpace
#import "scrrun.dll" rename_namespace("Scripting") \
					 rename("CopyFile","ScriptingCopyFile") \
					 rename("DeleteFile","ScriptingDeleteFile") \
					 rename("MoveFile","ScriptingMoveFile")

using namespace ADOCust;

#ifndef chASSERT
#define chASSERT(expr) ATLASSERT(expr)
#endif

#ifdef _DEBUG
#define RELEASE_TRY
#define RELEASE_CATCH_ALL
#else
#define RELEASE_TRY try {
#define RELEASE_CATCH_ALL } catch(...) {}
#endif
