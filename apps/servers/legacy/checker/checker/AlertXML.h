// AlertXML.h : Declaration of the CAlertXML

#pragma once
#include "resource.h"       // main symbols

#include "checker_i.h"
#include <atlstr.h>

#if defined(_WIN32_WCE) && !defined(_CE_DCOM) && !defined(_CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA)
#error "Single-threaded COM objects are not properly supported on Windows CE platform, such as the Windows Mobile platforms that do not include full DCOM support. Define _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA to force ATL to support creating single-thread COM object's and allow use of it's single-threaded COM object implementations. The threading model in your rgs file was set to 'Free' as that is the only threading model supported in non DCOM Windows CE platforms."
#endif

// CAlertXML

class ATL_NO_VTABLE CAlertXML :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CAlertXML, &CLSID_AlertXML>,
	public IDispatchImpl<IAlertXML4, &IID_IAlertXML4, &LIBID_DeskAlertsServerLib, /*wMajor =*/ 1, /*wMinor =*/ 0>
{
public:
	CAlertXML();
	virtual ~CAlertXML();

DECLARE_REGISTRY_RESOURCEID(IDR_ALERTXML)

BEGIN_COM_MAP(CAlertXML)
	COM_INTERFACE_ENTRY(IAlertXML)
	COM_INTERFACE_ENTRY(IAlertXML2)
	COM_INTERFACE_ENTRY(IAlertXML3)
	COM_INTERFACE_ENTRY(IAlertXML4)
//	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY2(IDispatch, IAlertXML)
	COM_INTERFACE_ENTRY2(IDispatch, IAlertXML2)
	COM_INTERFACE_ENTRY2(IDispatch, IAlertXML3)
	COM_INTERFACE_ENTRY2(IDispatch, IAlertXML4)
END_COM_MAP()

	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

private:
	CComBSTR m_urgentXML;
	CComBSTR m_usualXML;

	LONG m_userId;
	LONG m_timeout;
	CStringW m_plugin;
	CStringW m_alertURL;
	CStringW m_surveyURL;
	CStringW m_deskbarId;

	static BOOL checkUserHash(CStringW name, CStringW hash);
	static CStringW escape(CStringW str);

public:
//IAlertXML
	STDMETHOD(setUserId)( 
		/* [in] */ IDispatch *connection,
		/* [in] */ LONG id);

	STDMETHOD(setTimeout)( 
		/* [in] */ LONG timeout);

	STDMETHOD(setPlugin)( 
		/* [in] */ BSTR plugin);

	STDMETHOD(setAlertURL)( 
		/* [in] */ BSTR url);

	STDMETHOD(setSurveyURL)( 
		/* [in] */ BSTR url);

	STDMETHOD(setDeskbarId)( 
		/* [in] */ BSTR id);

	STDMETHOD(addAlert)( 
		/* [in] */ BOOL urgent,
		/* [in] */ LONG id,
		/* [in] */ LONG aknown,
		/* [in] */ LONG autoclose,
		/* [in] */ LONG create_timestamp,
		/* [in] */ BSTR title,
		/* [in] */ BSTR position,
		/* [in] */ BSTR width,
		/* [in] */ BSTR height,
		/* [in] */ LONG ticker,
		/* [in] */ LONG schedule,
		/* [in] */ BOOL history);

	STDMETHOD(addCustomAlert)( 
		/* [in] */ BOOL urgent,
		/* [in] */ BSTR href,
		/* [in] */ LONG id,
		/* [in] */ LONG aknown,
		/* [in] */ LONG autoclose,
		/* [in] */ LONG create_timestamp,
		/* [in] */ BSTR title,
		/* [in] */ BSTR position,
		/* [in] */ BSTR width,
		/* [in] */ BSTR height,
		/* [in] */ LONG ticker,
		/* [in] */ LONG schedule,
		/* [in] */ BOOL history);

	STDMETHOD(addSurvey)( 
		/* [in] */ BOOL urgent,
		/* [in] */ LONG id,
		/* [in] */ LONG create_timestamp,
		/* [in] */ BSTR title,
		/* [in] */ LONG schedule,
		/* [in] */ BOOL history);

	STDMETHOD(getResult)( 
		/* [retval][out] */ BSTR *result);

	STDMETHOD(getUTF8Result)( 
		/* [retval][out] */ SAFEARRAY **result);

public:
//IAlertXML2
	STDMETHOD(setUserInfo)(
		/* [in] */ IDispatch *connection,
		/* [in] */ LONG id,
		/* [in] */ BSTR name,
		/* [in] */ BSTR hash,
		/* [retval][out] */ BOOL *result);

	STDMETHOD(addAlert2)(
		/* [in] */ BOOL urgent,
		/* [in] */ LONG id,
		/* [in] */ LONG aknown,
		/* [in] */ LONG autoclose,
		/* [in] */ LONG create_timestamp,
		/* [in] */ BSTR title,
		/* [in] */ BSTR position,
		/* [in] */ BSTR visible,
		/* [in] */ BSTR width,
		/* [in] */ BSTR height,
		/* [in] */ LONG ticker,
		/* [in] */ LONG schedule,
		/* [in] */ LONG recurrence,
		/* [in] */ BOOL history,
		/* [in] */ BSTR innner,
		/* [retval][out] */ BOOL *result);

	STDMETHOD(addCustomAlert2)(
		/* [in] */ BOOL urgent,
		/* [in] */ BSTR href,
		/* [in] */ LONG id,
		/* [in] */ LONG aknown,
		/* [in] */ LONG autoclose,
		/* [in] */ LONG create_timestamp,
		/* [in] */ BSTR title,
		/* [in] */ BSTR position,
		/* [in] */ BSTR visible,
		/* [in] */ BSTR width,
		/* [in] */ BSTR height,
		/* [in] */ LONG ticker,
		/* [in] */ LONG schedule,
		/* [in] */ LONG recurrence,
		/* [in] */ BOOL history,
		/* [in] */ BSTR innner,
		/* [retval][out] */ BOOL *result);

	STDMETHOD(addSurvey2)(
		/* [in] */ BOOL urgent,
		/* [in] */ LONG id,
		/* [in] */ LONG create_timestamp,
		/* [in] */ BSTR title,
		/* [in] */ BSTR position,
		/* [in] */ BSTR visible,
		/* [in] */ BSTR width,
		/* [in] */ BSTR height,
		/* [in] */ LONG schedule,
		/* [in] */ LONG recurrence,
		/* [in] */ BOOL history,
		/* [in] */ BSTR innner,
		/* [retval][out] */ BOOL *result);

//IAlertXML3
	STDMETHOD(addAlert3)(
		/* [in] */ BOOL urgent,
		/* [in] */ LONG id,
		/* [in] */ LONG aknown,
		/* [in] */ LONG autoclose,
		/* [in] */ LONG create_timestamp,
		/* [in] */ BSTR title,
		/* [in] */ BSTR position,
		/* [in] */ BSTR visible,
		/* [in] */ BSTR width,
		/* [in] */ BSTR height,
		/* [in] */ LONG ticker,
		/* [in] */ LONG schedule,
		/* [in] */ LONG recurrence,
		/* [in] */ BOOL history,
		/* [in] */ LONG classId,
		/* [in] */ BSTR innner,
		/* [retval][out] */ BOOL *result);

	STDMETHOD(addCustomAlert3)(
		/* [in] */ BOOL urgent,
		/* [in] */ BSTR href,
		/* [in] */ LONG id,
		/* [in] */ LONG aknown,
		/* [in] */ LONG autoclose,
		/* [in] */ LONG create_timestamp,
		/* [in] */ BSTR title,
		/* [in] */ BSTR position,
		/* [in] */ BSTR visible,
		/* [in] */ BSTR width,
		/* [in] */ BSTR height,
		/* [in] */ LONG ticker,
		/* [in] */ LONG schedule,
		/* [in] */ LONG recurrence,
		/* [in] */ BOOL history,
		/* [in] */ LONG classId,
		/* [in] */ BSTR innner,
		/* [retval][out] */ BOOL *result);

	STDMETHOD(addSurvey3)(
		/* [in] */ BOOL urgent,
		/* [in] */ LONG id,
		/* [in] */ LONG create_timestamp,
		/* [in] */ BSTR title,
		/* [in] */ LONG autoclose,
		/* [in] */ BSTR position,
		/* [in] */ BSTR visible,
		/* [in] */ BSTR width,
		/* [in] */ BSTR height,
		/* [in] */ LONG schedule,
		/* [in] */ LONG recurrence,
		/* [in] */ BOOL history,
		/* [in] */ LONG classId,
		/* [in] */ BSTR innner,
		/* [retval][out] */ BOOL *result);



//IAlertXML4
	STDMETHOD(setAllUserInfo)(
		/* [in] */ IDispatch *connection,
		/* [in] */ LONG id,
		/* [in] */ BSTR name,
		/* [in] */ BSTR context,
		/* [in] */ BSTR domain,
		/* [in] */ BSTR hash,
		/* [retval][out] */ BOOL *result);

	STDMETHOD(addAlert4)(
		/* [in] */ IDispatch* params,
		/* [in] */ BSTR innner,
		/* [retval][out] */ BOOL *result);

    STDMETHOD(addSurvey4)(
		/* [in] */ BOOL urgent,
		/* [in] */ LONG id,
		/* [in] */ LONG create_timestamp,
		/* [in] */ BSTR title,
		/* [in] */ LONG autoclose,
		/* [in] */ BSTR position,
		/* [in] */ BSTR visible,
		/* [in] */ BSTR width,
		/* [in] */ BSTR height,
		/* [in] */ LONG schedule,
		/* [in] */ LONG recurrence,
		/* [in] */ BOOL history,
		/* [in] */ LONG classId,
        /* [in] */ LONG resizable,
        /* [in] */ LONG to_date,
		/* [in] */ BSTR innner,
		/* [retval][out] */ BOOL *result);
};

OBJECT_ENTRY_AUTO(__uuidof(AlertXML), CAlertXML)
