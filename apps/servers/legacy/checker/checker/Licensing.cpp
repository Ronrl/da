// Licensing.cpp : Implementation of CLicensing

#include "stdafx.h"
#include "Licensing.h"
#include "../constants.h"

#include <atlstr.h>
#include <time.h>

// CLicensing
extern volatile time_t g_LastCheck;
extern volatile BOOL g_FirstLoad;
extern volatile BOOL g_LicenseLockedState;
extern volatile BOOL g_TrialLockedState;
extern volatile LONG g_CurrentUsersCount;
extern volatile LONG g_ElipseSinseServerWasInstalled;
extern volatile BOOL g_FailedUsersCountSelect;

COptex CLicensing::g_cs(_T("DeskAlertsServerCS"));
BOOL CLicensing::checkLicenseAndTrialState(IDispatch *connection)
{
   
	BOOL result = FALSE;
	RELEASE_TRY
	{
		g_cs.Enter();
		RELEASE_TRY
		{
			if(g_FirstLoad)
			{
				restoreLockedStates(connection);
				g_FirstLoad = FALSE;
			}
			const time_t tickcount = time(NULL);//seconds
			if(g_FailedUsersCountSelect ||
				(!g_LicenseLockedState && !g_TrialLockedState && //locking is irreversible
					(tickcount - g_LastCheck > 60*60/**24*/ || tickcount < g_LastCheck))) //check every hour
			{
				g_LicenseLockedState = TRUE;
				g_CurrentUsersCount = getCurrentUsersCount(connection);
				if(g_CurrentUsersCount != -1L)
				{
					g_FailedUsersCountSelect = FALSE;
					const LONG licenseCount = getLicenseCount(connection);
					if(licenseCount > -1L && (licenseCount == 0L || g_CurrentUsersCount <= licenseCount))
					{
						g_LicenseLockedState = FALSE;
						g_TrialLockedState = TRUE;
						const LONG trialTime = getTrialTime(connection); //in seconds
						if(trialTime > 0L)
						{
							g_ElipseSinseServerWasInstalled = getElipseSinseServerWasInstalled(connection); //in seconds
							if(trialTime >= g_ElipseSinseServerWasInstalled)
							{
								result = TRUE;
								g_TrialLockedState = FALSE;
							}
						}
						else if(trialTime == 0L)
						{
							result = TRUE;
							g_TrialLockedState = FALSE;
						}
					}
					g_LastCheck = tickcount;
					if(!result)
					{
						saveLockedStates(connection);
					}
				}
				else
				{
					g_FailedUsersCountSelect = TRUE;
				}
			}
			else if(!g_LicenseLockedState && !g_TrialLockedState)
			{
				result = TRUE;
			}
		}
		RELEASE_CATCH_ALL; //for avoid locking on crash
		g_cs.Leave();
	}
	RELEASE_CATCH_ALL;
	return result;
}


void CLicensing::saveLockedStates(IDispatch *connection)
{
	RELEASE_TRY
	{
		CComQIPtr<_Connection> conn = connection;
		if(conn)
		{
			BOOL insert = TRUE;
			CComVariant var(VT_I4);
			_RecordsetPtr recordset = conn->Execute(_bstr_t(L"SELECT 1 FROM [users] WITH (READPAST) WHERE [name] = N'admin' AND [role] = 'A'"), &var, adOptionUnspecified);
			if(recordset)
			{
				FieldsPtr fields = recordset->GetFields();
				if(fields && fields->GetCount() > 0L)
				{
					insert = FALSE;
				}
			}
			if(insert)
			{
				conn->Execute(_bstr_t(L"INSERT INTO [users] ([name], [role]) VALUES (N'admin', 'A')"), &var, adOptionUnspecified);
			}
			_bstr_t sql = L"UPDATE [users] SET ";
			if(g_LicenseLockedState)
			{
				WCHAR buff[33];
				sql += L"[last_date] = DATEADD(n, ";
				if(_ltow_s(g_CurrentUsersCount, buff, 10) == 0) //save active users count
					sql += buff;
				else sql += L"-1";
				sql += L", 0), [last_request] = DATEADD(n, ";
				if(_ltow_s(getLicenseCount(connection), buff, 10) == 0) //save number of licenses
					sql += buff;
				else sql += L"-1";
				sql += L", 0), [next_request] = 60";
			}
			else
				sql += L"[last_date] = NULL, [last_request] = NULL, [next_request] = NULL";

			sql += L", ";

			if(g_TrialLockedState)
			{
				WCHAR buff[33];
				sql += L"[last_standby_request] = DATEADD(n, ";
				if(_ltow_s(getTrialTime(connection), buff, 10) == 0) //save trial time
					sql += buff;
				else sql += L"-1";
				sql += L", 0), [next_standby_request] = 60";
			}
			else
				sql += L"[last_standby_request] = NULL, [next_standby_request] = NULL";

			sql += L" WHERE [name] = N'admin' AND [role] = 'A'";
			conn->Execute(sql, &var, adOptionUnspecified);
		}
	}
	RELEASE_CATCH_ALL;
}

void CLicensing::restoreLockedStates(IDispatch *connection)
{
	RELEASE_TRY
	{
		CComQIPtr<_Connection> conn = connection;
		if(conn)
		{
			BOOL needToSaveStates = FALSE;
			CComVariant var(VT_I4);
			_RecordsetPtr recordset = conn->Execute(_bstr_t(L"SELECT DATEDIFF(n, 0, [last_date]), DATEDIFF(n, 0, [last_request]), CAST([next_request] AS INT), DATEDIFF(n, 0, [last_standby_request]), CAST([next_standby_request] AS INT) FROM [users] WITH (READPAST) WHERE [name] = N'admin' AND [role] = 'A'"), &var, adOptionUnspecified);
			if(recordset)
			{	
				FieldsPtr fields = recordset->GetFields();
				if(fields && fields->GetCount() == 5L)
				{
					FieldPtr lastRequestField = fields->GetItem(_variant_t(1L, VT_I4)); //last_request -> number of licenses
					if(lastRequestField)
					{ 
						_variant_t lastRequestVal = lastRequestField->GetValue();
						if(lastRequestVal.vt == VT_I4) //last request are set
						{
							FieldPtr nextRequestField = fields->GetItem(_variant_t(2L, VT_I4)); //next_request -> g_LicenseLockedState
							if(nextRequestField)
							{
								_variant_t nextRequestVal = nextRequestField->GetValue();
								if(nextRequestVal.vt == VT_I4 && nextRequestVal.lVal == 60L) //g_LicenseLockedState = TRUE
								{
									if(getLicenseCount(connection) == lastRequestVal.lVal) //number of licenses not changed
									{
										g_LicenseLockedState = TRUE;
										FieldPtr lastDateField = fields->GetItem(_variant_t(0L, VT_I4)); //last_date -> g_CurrentUsersCount
										if(lastDateField)
										{
											_variant_t lastDateVal = lastDateField->GetValue();
											if(lastDateVal.vt == VT_I4)
											{
												g_CurrentUsersCount = lastDateVal.lVal; //restore active users count
											}
										}
									}
									else
									{
										needToSaveStates = TRUE;
									}
								}
							}
						}
					}
					FieldPtr lastStandbyField = fields->GetItem(_variant_t(3L, VT_I4)); //last_standby_request -> number of trial days
					if(lastStandbyField)
					{
						_variant_t lastStandbyVal = lastStandbyField->GetValue();
						if(lastStandbyVal.vt == VT_I4) //last standby request are set
						{
							FieldPtr nextStandbyField = fields->GetItem(_variant_t(4L, VT_I4)); //next_standby_request -> g_TrialLockedState
							if(nextStandbyField)
							{
								_variant_t nextStandbyVal = nextStandbyField->GetValue();
								if(nextStandbyVal.vt == VT_I4 && nextStandbyVal.lVal == 60L) //g_LicenseLockedState = TRUE
								{
									if(getTrialTime(connection) == lastStandbyVal.lVal) //number of trial days not changed
									{
										g_TrialLockedState = TRUE;
									}
									else
									{
										needToSaveStates = TRUE;
									}
								}
							}
						}
					}
				}
			}
			if(needToSaveStates)
				saveLockedStates(connection);
		}
	}
	RELEASE_CATCH_ALL;
}

LONG CLicensing::getLicenseCount(IDispatch* connection)
{
	LONG result = -1L;
	RELEASE_TRY
	{
		CComQIPtr<_Connection> conn = connection;
		if(conn)
		{
			//BOOL insert = TRUE;
			CComVariant var(VT_I4);
			_RecordsetPtr recordset = conn->Execute(_bstr_t(L"SELECT val as count FROM [settings] WHERE name='licenses' "), &var, adOptionUnspecified);
			if(recordset)
			{
				FieldsPtr fields = recordset->GetFields();
				if(fields && fields->GetCount() > 0L)
				{
					CString str = fields->Item["count"]->Value;
					size_t outputSize = str.GetLength()+1;
					char * outputString = new char[outputSize];
					size_t charsConverted = 0;
					const wchar_t * inputW = str;
					wcstombs_s(&charsConverted, outputString, outputSize, inputW, str.GetLength());

					result = atol(outputString);
				}
			}

		}
		/*CStringA num("0x00000000");
		// 41,64,17,106,78,60,58,68
		num.Format("0x%c%c%c%c%c%c%c%c",
			g_LicenseCountStr[LicenseStep0],
			g_LicenseCountStr[LicenseStep1],
			g_LicenseCountStr[LicenseStep2],
			g_LicenseCountStr[LicenseStep3],
			g_LicenseCountStr[LicenseStep4],
			g_LicenseCountStr[LicenseStep5],
			g_LicenseCountStr[LicenseStep6],
			g_LicenseCountStr[LicenseStep7]);
		result = strtol(num, NULL, 16);
		if(result != 0L || num == "0x00000000")
			result ^= g_LicenseCountXor;
		else
			result = -1L;*/
	}
	RELEASE_CATCH_ALL;
	return result;
}

LONG CLicensing::getTrialTime(IDispatch* connection)
{
	LONG result = -1L;
	RELEASE_TRY
	{
		CComQIPtr<_Connection> conn = connection;
		if(conn)
		{
			//BOOL insert = TRUE;
			CComVariant var(VT_I4);
			_RecordsetPtr recordset = conn->Execute(_bstr_t(L"SELECT val as count FROM [settings] WHERE name='supermario' "), &var, adOptionUnspecified);
			if(recordset)
			{
				FieldsPtr fields = recordset->GetFields();
				if(fields && fields->GetCount() > 0L)
				{
					CString str = fields->Item["count"]->Value;
					size_t outputSize = str.GetLength()+1;
					char * outputString = new char[outputSize];
					size_t charsConverted = 0;
					const wchar_t * inputW = str;
					wcstombs_s(&charsConverted, outputString, outputSize, inputW, str.GetLength());

					result = atol(outputString)*24L*60L*60L;
				}
			}

		}
		/*CStringA num("0x00000000");
		// 53,23,73,112,32,43,26,84
		num.Format("0x%c%c%c%c%c%c%c%c",
			g_TrialTimeStr[TrialTimeStep0],
			g_TrialTimeStr[TrialTimeStep1],
			g_TrialTimeStr[TrialTimeStep2],
			g_TrialTimeStr[TrialTimeStep3],
			g_TrialTimeStr[TrialTimeStep4],
			g_TrialTimeStr[TrialTimeStep5],
			g_TrialTimeStr[TrialTimeStep6],
			g_TrialTimeStr[TrialTimeStep7]);
		result = strtol(num, NULL, 16);
		if(result != 0L || num == "0x00000000")
			result ^= g_TrialTimeXor;
		else
			result = -1L;*/
	}
	RELEASE_CATCH_ALL;
	return result;
}

LONG CLicensing::getCurrentUsersCount(IDispatch *connection)
{
	LONG result = -1L;
	RELEASE_TRY
	{
		CComQIPtr<_Connection> conn = connection;
		if(conn)
		{
			CComVariant var(VT_I4);
			_RecordsetPtr recordset = conn->Execute(_bstr_t(L" \
				DECLARE @from_date DATETIME \
				SET @from_date = DATEADD(day, -1 , GETDATE()) \
				SELECT COUNT(1) FROM ( \
					SELECT u.id FROM alerts a WITH (READPAST) \
						INNER JOIN alerts_sent s WITH (READPAST) \
						ON s.alert_id = a.id \
						INNER JOIN users u WITH (READPAST) \
						ON s.user_id = u.id AND NOT u.mobile_phone = '' \
						WHERE a.sent_date > @from_date AND a.schedule = '0' AND a.sms = '1' \
					UNION \
					SELECT u.id FROM alerts a WITH (READPAST) \
						INNER JOIN alerts_sent s WITH (READPAST) \
						ON s.alert_id = a.id \
						INNER JOIN users u WITH (READPAST) \
						ON s.user_id = u.id AND NOT u.email = N'' \
						WHERE a.sent_date > @from_date AND a.schedule = '0' AND a.email = '1' \
					UNION \
					SELECT u.id FROM alerts a WITH (READPAST) \
						INNER JOIN alerts_received_recur r WITH (READPAST) \
						ON r.alert_id = a.id \
						INNER JOIN users u WITH (READPAST) \
						ON r.user_id = u.id AND NOT u.mobile_phone = '' \
						WHERE r.date > @from_date AND a.schedule = '1' AND a.sms = '1' \
					UNION \
					SELECT u.id FROM alerts a WITH (READPAST) \
						INNER JOIN alerts_received_recur r WITH (READPAST) \
						ON r.alert_id = a.id \
						INNER JOIN users u WITH (READPAST) \
						ON r.user_id = u.id AND NOT u.email = N'' \
						WHERE r.date > @from_date AND a.schedule = '1' AND a.email = '1' \
					UNION \
					SELECT id FROM users WITH (READPAST) \
						WHERE role = 'U' and last_request > @from_date \
				) t"), &var, adOptionUnspecified);
			if(recordset)
			{
				FieldsPtr fields = recordset->GetFields();
				if(fields && fields->GetCount() > 0L)
				{
					FieldPtr field = fields->GetItem(_variant_t(0L, VT_I4));
					if(field)
					{
						_variant_t val = field->GetValue();
						if(val.vt == VT_I4)
						{
							result = val.lVal;
						}
					}
				}
			}
		}
	}
	RELEASE_CATCH_ALL;
	return result;
}

LONG CLicensing::getElipseSinseServerWasInstalled(IDispatch *connection)
{
	LONG result = -1L;
	RELEASE_TRY
	{
		CComQIPtr<_Connection> conn = connection;
		if(conn)
		{
			CComVariant var(VT_I4);
			_RecordsetPtr recordset = conn->Execute(_bstr_t(L"SELECT DATEDIFF(s, [reg_date], GETDATE()) FROM [users] WITH (READPAST) WHERE [name] = N'admin' AND [role] = 'A'"), &var, adOptionUnspecified);
			if(recordset)
			{
				FieldsPtr fields = recordset->GetFields();
				if(fields && fields->GetCount() > 0L)
				{
					FieldPtr field = fields->GetItem(_variant_t(0L, VT_I4));
					if(field)
					{
						_variant_t val = field->GetValue();
						if(val.vt == VT_NULL)
						{
							conn->Execute(_bstr_t(L"UPDATE [users] SET [reg_date] = GETDATE() WHERE [name] = N'admin' AND [role] = 'A'"), &var, adOptionUnspecified);
							result = 0L;
						}
						else if(val.vt == VT_I4)
						{
							result = val.lVal;
						}
					}
				}
				else
				{
					conn->Execute(_bstr_t(L"INSERT INTO [users] ([name], [role], [reg_date]) VALUES (N'admin', 'A', GETDATE())"), &var, adOptionUnspecified);
					result = 0L;
				}
			}
		}
	}
	RELEASE_CATCH_ALL;
	return result;
}

STDMETHODIMP CLicensing::getLicenseCount( 
	/* [in] */ IDispatch *connection,
	/* [retval][out] */ LONG *result)
{
	RELEASE_TRY
	{
		CComQIPtr<_Connection> conn = connection;
		if(conn) //check for valid connection
		{
			*result = CLicensing::getLicenseCount(connection);
		}
	}
	RELEASE_CATCH_ALL;
	return S_OK;
}

STDMETHODIMP CLicensing::getCurrentUsersCount( 
	/* [in] */ IDispatch *connection,
	/* [retval][out] */ LONG *result)
{
	RELEASE_TRY
	{
		CLicensing::checkLicenseAndTrialState(connection); //renew g_CurrentUsersCount if need
		*result = g_CurrentUsersCount;
	}
	RELEASE_CATCH_ALL;
	return S_OK;
}

STDMETHODIMP CLicensing::getTrialTime(
	/* [in] */ IDispatch *connection,
	/* [retval][out] */ LONG *result)
{
	RELEASE_TRY
	{
		CComQIPtr<_Connection> conn = connection;
		if(conn) //check for valid connection
		{
			*result = CLicensing::getTrialTime(connection);
		}
	}
	RELEASE_CATCH_ALL;
	return S_OK;
}

STDMETHODIMP CLicensing::getTrialExpire(
	/* [in] */ IDispatch *connection,
	/* [retval][out] */ LONG *result)
{
	RELEASE_TRY
	{
		CLicensing::checkLicenseAndTrialState(connection); //renew g_ElipseSinseServerWasInstalled if need
		*result = g_ElipseSinseServerWasInstalled;
	}
	RELEASE_CATCH_ALL;
	return S_OK;
}

STDMETHODIMP CLicensing::getTrialState( //trial prolongation
	/* [in] */ IDispatch *connection,
	/* [retval][out] */ BOOL *result)
{
	*result = FALSE;
	RELEASE_TRY
	{
		CComQIPtr<_Connection> conn = connection;
		if(conn)
		{
			const LONG trialTime = CLicensing::getTrialTime(connection); //in seconds
			if(trialTime > 0L) //only if trial
			{
				g_cs.Enter();
				RELEASE_TRY
				{
					CComVariant var(VT_I4);
					_RecordsetPtr recordset = conn->Execute(_bstr_t("SELECT [was_checked] FROM [users] WITH (READPAST) WHERE [name] = N'admin' AND [role] = 'A'"), &var, adOptionUnspecified);
					if(recordset)
					{
						FieldsPtr fields = recordset->GetFields();
						if(fields && fields->GetCount() > 0L)
						{
							FieldPtr field = fields->GetItem(_variant_t(0L, VT_I4));
							if(field)
							{
								_variant_t val = field->GetValue();
								if(val.vt == VT_NULL) //first prolongation
								{
									conn->Execute(_bstr_t("UPDATE [users] SET [reg_date] = GETDATE(), [was_checked] = 0 WHERE [name] = N'admin' AND [role] = 'A'"), &var, adOptionUnspecified);
									g_TrialLockedState = FALSE; //unlock
									g_LastCheck = 0; //recheck
									*result = TRUE;
								}
								else if(val.vt == VT_I4 && var.lVal == 0) //second prolongation
								{
									conn->Execute(_bstr_t("UPDATE [users] SET [reg_date] = GETDATE(), [was_checked] = 1 WHERE [name] = N'admin' AND [role] = 'A'"), &var, adOptionUnspecified);
									g_TrialLockedState = FALSE; //unlock
									g_LastCheck = 0; //recheck
									*result = TRUE;
								}
							}
						}
					}
				}
				RELEASE_CATCH_ALL; //for avoid locking on crash
				g_cs.Leave();
			}
		}
	}
	RELEASE_CATCH_ALL;
	return S_OK;
}