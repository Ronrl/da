// dllmain.h : Declaration of module class.

class CCheckerModule : public CAtlDllModuleT< CCheckerModule >
{
public :
	DECLARE_LIBID(LIBID_DeskAlertsServerLib)
	DECLARE_REGISTRY_APPID_RESOURCEID(IDR_CHECKER, "{15F90EFE-A210-4889-BDC5-01F7209CA4AF}")
};

extern class CCheckerModule _AtlModule;
