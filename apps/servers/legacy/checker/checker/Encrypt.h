// Encrypt.h : Declaration of the CEncrypt

#pragma once
#include "resource.h"       // main symbols

#include "checker_i.h"

#if defined(_WIN32_WCE) && !defined(_CE_DCOM) && !defined(_CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA)
#error "Single-threaded COM objects are not properly supported on Windows CE platform, such as the Windows Mobile platforms that do not include full DCOM support. Define _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA to force ATL to support creating single-thread COM object's and allow use of it's single-threaded COM object implementations. The threading model in your rgs file was set to 'Free' as that is the only threading model supported in non DCOM Windows CE platforms."
#endif

// CEncrypt

class ATL_NO_VTABLE CEncrypt :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CEncrypt, &CLSID_Encrypt>,
	public IDispatchImpl<IEncrypt3, &IID_IEncrypt3, &LIBID_DeskAlertsServerLib, /*wMajor =*/ 1, /*wMinor =*/ 0>
{
public:
	CEncrypt()
	{
	}

DECLARE_REGISTRY_RESOURCEID(IDR_ENCRYPT)

BEGIN_COM_MAP(CEncrypt)
	COM_INTERFACE_ENTRY(IEncrypt)
	COM_INTERFACE_ENTRY(IEncrypt2)
	COM_INTERFACE_ENTRY(IEncrypt3)
//	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY2(IDispatch, IEncrypt)
	COM_INTERFACE_ENTRY2(IDispatch, IEncrypt2)
	COM_INTERFACE_ENTRY2(IDispatch, IEncrypt3)
END_COM_MAP()

	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

public:
//IEncrypt
	STDMETHOD(encrypt)(
		/* [in] */ BSTR data,
		/* [in] */ BSTR password,
		/* [retval][out] */ SAFEARRAY **result);

	STDMETHOD(encryptUTF8)(
		/* [in] */ BSTR data,
		/* [in] */ BSTR password,
		/* [retval][out] */ SAFEARRAY **result);

	STDMETHOD(newEncrypt)(
		/* [in] */ BSTR data,
		/* [in] */ BSTR password,
		/* [retval][out] */ SAFEARRAY **result);

	STDMETHOD(newEncryptUTF8)(
		/* [in] */ BSTR data,
		/* [in] */ BSTR password,
		/* [retval][out] */ SAFEARRAY **result);

private:
#define SBOX_SIZE 256          
#define SBOX_SIZE_DIV_8 32     

	typedef struct
	{
		unsigned char i;
		unsigned char j;
		unsigned char sbox[SBOX_SIZE];
		char *data;
		size_t data_len;
	} RC4_Session;

	STDMETHOD(RC4Encrypt)(char *data, const char *password, SAFEARRAY **result);
	STDMETHOD(AESEncrypt)(char *data, const char *password, SAFEARRAY **result);

	static void swap_char(unsigned char *a, unsigned char *b);
	static void RC4_Start(RC4_Session &session, const char *key, size_t key_len);
	static void RC4_F(RC4_Session &session);
	static unsigned char RC4_Gen(RC4_Session &session);

public:
//IEncrypt2
	STDMETHOD(md5FromUtf8)(
		/* [in] */ BSTR str,
		/* [retval][out] */ BSTR *result);

	STDMETHOD(sha1FromUtf8)(
		/* [in] */ BSTR str,
		/* [retval][out] */ BSTR *result);

	STDMETHOD(base64FromUtf8)(
		/* [in] */ BSTR str,
		/* [retval][out] */ BSTR *result);

	STDMETHOD(strFromUtf8Base64)(
		/* [in] */ BSTR str,
		/* [retval][out] */ BSTR *result);

	STDMETHOD(utf8FromBase64)(
		/* [in] */ BSTR str,
		/* [retval][out] */ SAFEARRAY **result);

public:
	//IEncrypt3
	STDMETHOD(shortIdFromGuid)(
		/* [in] */ BSTR str,
		/* [retval][out] */ BSTR *result);

	STDMETHOD(guidFromShortId)(
		/* [in] */ BSTR str,
		/* [retval][out] */ BSTR *result);

};

OBJECT_ENTRY_AUTO(__uuidof(Encrypt), CEncrypt)
