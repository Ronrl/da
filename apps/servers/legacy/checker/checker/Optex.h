/******************************************************************************
Module name: Optex.h
Written by:  Jeffrey Richter
******************************************************************************/


#pragma once


///////////////////////////////////////////////////////////////////////////////


class COptex {
public:
	COptex(DWORD dwSpinCount = 4000);
	COptex(PCWSTR pszName, DWORD dwSpinCount = 4000);
	~COptex();

	void SetSpinCount(DWORD dwSpinCount);
	void Enter();
	BOOL TryEnter();
	void Leave();
	BOOL IsSingleProcessOptex() const;

private:
	typedef struct {
		DWORD m_dwSpinCount;
		long  m_lLockCount;
		DWORD m_dwThreadId;
		long  m_lRecurseCount;
	} SHAREDINFO, *PSHAREDINFO;

	HANDLE      m_hevt;
	HANDLE      m_hfm;
	PSHAREDINFO m_psi;

private:
	static BOOL sm_fUniprocessorHost;

private:
	void CommonConstructor(DWORD dwSpinCount, BOOL fUnicode, PVOID pszName);
	PTSTR ConstructObjectName(PTSTR pszResult, 
		PCTSTR pszPrefix, BOOL fUnicode, PVOID pszName);
};


///////////////////////////////////////////////////////////////////////////////

inline COptex::COptex(DWORD dwSpinCount) {

	CommonConstructor(dwSpinCount, FALSE, NULL);
}

///////////////////////////////////////////////////////////////////////////////



inline COptex::COptex(PCTSTR pszName, DWORD dwSpinCount) {

	BOOL isUnicode=TRUE;
#ifdef _UNICODE
	isUnicode=TRUE;
#else
	isUnicode=FALSE;
#endif
	CommonConstructor(dwSpinCount, isUnicode, (PVOID) pszName);
}


///////////////////////////////////////////////////////////////////////////////


inline BOOL COptex::IsSingleProcessOptex() const {

	return(m_hfm == NULL);
}


///////////////////////////////// End of File /////////////////////////////////
