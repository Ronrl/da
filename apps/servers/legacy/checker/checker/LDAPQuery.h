// LDAPQuery.h : Declaration of the CLDAPQuery

#pragma once
#include "resource.h"       // main symbols

#include "checker_i.h"

#include <atlstr.h>
#include <atlcoll.h>

#if defined(_WIN32_WCE) && !defined(_CE_DCOM) && !defined(_CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA)
#error "Single-threaded COM objects are not properly supported on Windows CE platform, such as the Windows Mobile platforms that do not include full DCOM support. Define _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA to force ATL to support creating single-thread COM object's and allow use of it's single-threaded COM object implementations. The threading model in your rgs file was set to 'Free' as that is the only threading model supported in non DCOM Windows CE platforms."
#endif

#ifndef MAXULONG
#define MAXULONG ((ULONG)~((ULONG)0))
#endif

enum DataType {
	IA5String,
	DirectoryString,
	PrintableString,
	OctetString,
	PostalAddress,
	CountryString,
	NumericString,
	Integer,
	GeneralizedTime,
	TelephoneNumber,
	Boolean,
	Binary,
	DistinguishedName,
	BitsStrings
};
/*
	1.3.6.1.4.1.1466.115.121.1.3 - Attribute Type Description 
	1.3.6.1.4.1.1466.115.121.1.5 - Binary syntax 
	1.3.6.1.4.1.1466.115.121.1.6 - Bit string syntax 
	1.3.6.1.4.1.1466.115.121.1.7 - Boolean syntax 
	1.3.6.1.4.1.1466.115.121.1.8 - Certificate syntax 
	1.3.6.1.4.1.1466.115.121.1.9 - Certificate List syntax 
	1.3.6.1.4.1.1466.115.121.1.10 - Certificate Pair syntax 
	1.3.6.1.4.1.1466.115.121.1.11 - Country String syntax 
	1.3.6.1.4.1.1466.115.121.1.12 - Distinguished Name syntax 
	1.3.6.1.4.1.1466.115.121.1.14 - Delivery Method 
	1.3.6.1.4.1.1466.115.121.1.15 - Directory String syntax 
	1.3.6.1.4.1.1466.115.121.1.16 - DIT Content Rule syntax 
	1.3.6.1.4.1.1466.115.121.1.17 - DIT Structure Rule Description syntax 
	1.3.6.1.4.1.1466.115.121.1.21 - Enhanced Guide 
	1.3.6.1.4.1.1466.115.121.1.22 - Facsimile Telephone Number syntax 
	1.3.6.1.4.1.1466.115.121.1.23 - Fax image syntax 
	1.3.6.1.4.1.1466.115.121.1.24 - Generalized Time syntax 
	1.3.6.1.4.1.1466.115.121.1.26 - IA5 String syntax 
	1.3.6.1.4.1.1466.115.121.1.27 - Integer syntax 
	1.3.6.1.4.1.1466.115.121.1.28 - JPeg Image syntax 
	1.3.6.1.4.1.1466.115.121.1.30 - Matching Rule Description syntax 
	1.3.6.1.4.1.1466.115.121.1.31 - Matching Rule Use Description syntax 
	1.3.6.1.4.1.1466.115.121.1.33 - MHS OR Address syntax 
	1.3.6.1.4.1.1466.115.121.1.34 - Name and Optional UID syntax 
	1.3.6.1.4.1.1466.115.121.1.35 - Name Form syntax 
	1.3.6.1.4.1.1466.115.121.1.36 - Numeric String syntax 
	1.3.6.1.4.1.1466.115.121.1.37 - Object Class Description syntax 
	1.3.6.1.4.1.1466.115.121.1.38 - OID syntax 
	1.3.6.1.4.1.1466.115.121.1.39 - Other Mailbox syntax 
	1.3.6.1.4.1.1466.115.121.1.40 - Octet String 
	1.3.6.1.4.1.1466.115.121.1.41 - Postal Address syntax 
	1.3.6.1.4.1.1466.115.121.1.43 - Presentation Address syntax 
	1.3.6.1.4.1.1466.115.121.1.44 - Printable string syntax 
	1.3.6.1.4.1.1466.115.121.1.49 - Supported Algorithm 
	1.3.6.1.4.1.1466.115.121.1.50 - Telephone number syntax 
	1.3.6.1.4.1.1466.115.121.1.51 - Teletex Terminal Identifier 
	1.3.6.1.4.1.1466.115.121.1.52 - Telex Number 
	1.3.6.1.4.1.1466.115.121.1.53 - UTCTime syntax 
	1.3.6.1.4.1.1466.115.121.1.54 - LDAP Syntax Description syntax
*/

template <class TKey, class TVal, class TEqual = CSimpleMapEqualHelper< TKey, TVal > >
class CAutoDeleteSimpleMap : public CSimpleMap<TKey, TVal, TEqual>
{
public:
	virtual ~CAutoDeleteSimpleMap()
	{
		RemoveAll();
	}
	BOOL Remove(const TKey& key)
	{
		int nIndex = CSimpleMap<TKey, TVal, TEqual>::FindKey(key);
		if(nIndex == -1)
			return FALSE;
		return RemoveAt(nIndex);
	}
	void RemoveAll()
	{
		if(m_aVal != NULL)
		{
			for(int i = 0; i < m_nSize; i++)
			{
				if(m_aVal[i])
					delete m_aVal[i];
			}
		}
		CSimpleMap<TKey, TVal, TEqual>::RemoveAll();
	}
	BOOL RemoveAt(int nIndex)
	{
		ATLASSERT(nIndex >= 0 && nIndex < m_nSize);
		if (nIndex < 0 || nIndex >= m_nSize)
			return FALSE;
		if(m_aVal[nIndex])
			delete m_aVal[nIndex];
		return CSimpleMap<TKey, TVal, TEqual>::RemoveAt(nIndex);
	}
};

struct AttrSchema {
	DataType type;
	bool single;
};

struct ResultsInfo {
	ResultsInfo() : ld(NULL), pages(NULL)
	{
	}
	virtual ~ResultsInfo()
	{
		if(ld && pages)
			ldap_search_abandon_page(ld, pages);
	}
	PLDAP ld;
	PLDAPSearch pages;
};

class ConnectInfo {
public:
	ConnectInfo() : ld(NULL), attrsSchema(NULL)
	{
	}
	virtual ~ConnectInfo()
	{
		if(attrsSchema)
			delete attrsSchema;
		results.RemoveAll();
		if(ld)
			ldap_unbind_s(ld);
	}
	PLDAP ld;
	CAtlMap<CString, AttrSchema> *attrsSchema;
	CAutoDeleteSimpleMap<LONG, ResultsInfo*> results;
	CString defaultNamingContext;
	bool canFindFromRoot;
	LONG addResult(PLDAPSearch pages)
	{
		static LONG maxResultId = 0L;
		ResultsInfo *info = new ResultsInfo;
		info->ld = ld;
		info->pages = pages;
		results.Add(maxResultId, info);
		return maxResultId++;
	}
	BOOL removeResult(LONG id)
	{
		results.Remove(id);
	}
};

// CLDAPQuery
class ATL_NO_VTABLE CLDAPQuery :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CLDAPQuery, &CLSID_LDAPQuery>,
	public IDispatchImpl<ILDAPQuery2, &IID_ILDAPQuery2, &LIBID_DeskAlertsServerLib, /*wMajor =*/ 1, /*wMinor =*/ 0>
{
public:
	CLDAPQuery();

DECLARE_REGISTRY_RESOURCEID(IDR_LDAPQUERY)

BEGIN_COM_MAP(CLDAPQuery)
	COM_INTERFACE_ENTRY(ILDAPQuery)
	COM_INTERFACE_ENTRY(ILDAPQuery2)
	//COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY2(IDispatch, ILDAPQuery)
	COM_INTERFACE_ENTRY2(IDispatch, ILDAPQuery2)
END_COM_MAP()

DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

public:
//ILDAPQuery
	HRESULT STDMETHODCALLTYPE execQuery( 
		/* [in] */ BSTR username,
		/* [in] */ BSTR password,
		/* [in] */ BSTR host,
		/* [in] */ BSTR basedn,
		/* [in] */ BSTR scope,
		/* [in] */ BSTR filter,
		/* [in] */ BSTR attributes,
		/* [in] */ VARIANT_BOOL usessl,
        /* [in] */ BSTR logFileName,
		/* [retval][out] */ SAFEARRAY **result);

	HRESULT STDMETHODCALLTYPE getLastError( 
		/* [retval][out] */ LONG *result);

public:
//ILDAPQuery2
	STDMETHOD(connect)(
		/* [in] */ BSTR username,
		/* [in] */ BSTR password,
		/* [in] */ BSTR host,
		/* [in] */ VARIANT_BOOL usessl,
		/* [retval][out] */ LONG *connect_id);

	STDMETHOD(close)(
		/* [in] */ LONG connect_id);

	STDMETHOD(query)(
		/* [in] */ LONG connect_id,
		/* [in] */ BSTR basedn,
		/* [in] */ BSTR scope,
		/* [in] */ BSTR filter,
		/* [in] */ BSTR attributes,
        /* [in] */ BSTR logFileName,
		/* [retval][out] */ SAFEARRAY **result);

	STDMETHOD(pageQuery)(
		/* [in] */ LONG connect_id,
		/* [in] */ BSTR basedn,
		/* [in] */ BSTR scope,
		/* [in] */ BSTR filter,
		/* [in] */ BSTR attributes,
		/* [in] */ ULONG total_results,
		/* [retval][out] */ LONG *results_id);

	STDMETHOD(nextResults)(
		/* [in] */ LONG results_id,
		/* [in] */ ULONG num_results,
        /* [in] */ BSTR logFileName,
		/* [retval][out] */ SAFEARRAY **result);

	STDMETHOD(dn2ufn)(
		/* [in] */ BSTR dn,
		/* [retval][out] */ BSTR *ufn);

	STDMETHOD(ufn2dn)(
		/* [in] */ BSTR ufn,
		/* [retval][out] */ BSTR *dn);

	STDMETHOD(err2string)(
		/* [in] */ ULONG ufn,
		/* [retval][out] */ BSTR *string);

	STDMETHOD(explodeDn)(
		/* [in] */ BSTR str,
		/* [in] */ ULONG notypes,
		/* [retval][out] */ SAFEARRAY **result);

	STDMETHOD(rename)(
		/* [in] */ LONG connect_id,
		/* [in] */ BSTR dn,
		/* [in] */ BSTR newRDN,
		/* [in] */ BSTR newParent,
		/* [in] */ BOOL deleteOldRdn);

private:
	LONG m_errorCode;
	LONG m_maxConnectionId;
	CAutoDeleteSimpleMap<LONG, ConnectInfo*> m_connections;
};

OBJECT_ENTRY_AUTO(__uuidof(LDAPQuery), CLDAPQuery)
