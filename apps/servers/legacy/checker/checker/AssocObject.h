#pragma once

#include <map>
#include <vector>
#include <atlstr.h>
#include "checker_i.h"

class CAssocObject :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CAssocObject>,
	public IDispatchImpl<IDispatch, &IID_IDispatch, &LIBID_DeskAlertsServerLib, /*wMajor =*/ 1, /*wMinor =*/ 0>
{
public:
	CAssocObject(void);
	~CAssocObject(void);

	DECLARE_PROTECT_FINAL_CONSTRUCT()
	DECLARE_NO_REGISTRY()

	BEGIN_COM_MAP(CAssocObject)
		COM_INTERFACE_ENTRY(IDispatch)
	END_COM_MAP()

private:
	std::map<DISPID,CComVariant> idValue;
	std::map<CString,DISPID> nameId;
	DISPID m_id;

public:
	void AddValue(CString name, CComVariant value);

	//IDispatch
	STDMETHOD(GetIDsOfNames)(
		/* [in] */ REFIID riid,
		/* [size_is][in] */ LPOLESTR *rgszNames,
		/* [in] */ UINT cNames,
		/* [in] */ LCID lcid,
		/* [size_is][out] */ DISPID *rgDispId);

	STDMETHOD(Invoke)(
		/* [in] */ DISPID dispIdMember,
		/* [in] */ REFIID riid,
		/* [in] */ LCID lcid,
		/* [in] */ WORD wFlags,
		/* [out][in] */ DISPPARAMS *pDispParams,
		/* [out] */ VARIANT *pVarResult,
		/* [out] */ EXCEPINFO *pExcepInfo,
		/* [out] */ UINT *puArgErr);

};
