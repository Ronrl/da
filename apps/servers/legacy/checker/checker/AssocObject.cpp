#include "StdAfx.h"
#include "AssocObject.h"

CAssocObject::CAssocObject(void): m_id(DISPID_VALUE+1) //start from non zero
{
}

CAssocObject::~CAssocObject(void)
{
}

void CAssocObject::AddValue(CString name, CComVariant value)
{
	idValue.insert(std::pair<int,CComVariant>(m_id, value));
	nameId.insert(std::pair<CString,int>(name,m_id));
	++m_id;
}

STDMETHODIMP CAssocObject::GetIDsOfNames( 
	/* [in] */ REFIID /*riid*/, 
	/* [size_is][in] */ LPOLESTR *rgszNames, 
	/* [in] */ UINT /*cNames*/, 
	/* [in] */ LCID /*lcid*/, 
	/* [size_is][out] */ DISPID *rgDispId )
{
	std::map<CString,DISPID>::const_iterator it = nameId.find(CString(*rgszNames));
	if (it!=nameId.end())
	{
		*rgDispId = (*it).second;
	}
	else
	{
		*rgDispId = m_id++;
		nameId.insert(std::pair<CString,int>(*rgszNames,*rgDispId));
	}
	return S_OK;
}

STDMETHODIMP CAssocObject::Invoke(
	/* [in] */ DISPID dispIdMember,
	/* [in] */ REFIID /*riid*/,
	/* [in] */ LCID /*lcid*/,
	/* [in] */ WORD wFlags,
	/* [out][in] */ DISPPARAMS *pDispParams,
	/* [out] */ VARIANT *pVarResult,
	/* [out] */ EXCEPINFO* /*pExcepInfo*/,
	/* [out] */ UINT* /*puArgErr*/)
{
	if(pDispParams && pDispParams->cArgs > 0)
	{
		CString name;
		CComVariant var0 = pDispParams->rgvarg[pDispParams->cArgs-1];
		if(var0.vt == (VT_VARIANT | VT_BYREF))
			var0 = *var0.pvarVal;
		if(var0.vt == VT_BSTR)
		{
			name = var0.bstrVal;
		}
		else if(var0.vt == (VT_BSTR | VT_BYREF))
		{
			name = *var0.pbstrVal;
		}
		if(!name.IsEmpty())
		{
			std::map<CString,DISPID>::const_iterator it = nameId.find(name);
			if (it!=nameId.end())
			{
				dispIdMember = (*it).second;
			}
			else
			{
				dispIdMember = m_id++;
				nameId.insert(std::pair<CString,int>(name,dispIdMember));
			}
		}
	}
	if(wFlags & DISPATCH_PROPERTYGET || wFlags == DISPATCH_METHOD)
	{
		CComVariant result;
		std::map<DISPID,CComVariant>::const_iterator itm = idValue.find(dispIdMember);
		if (itm!=idValue.end())
		{
			result = (*itm).second;
		}
		result.Detach(pVarResult);
	}
	else if(wFlags & DISPATCH_PROPERTYPUT || wFlags & DISPATCH_PROPERTYPUTREF)
	{
		std::map<DISPID,CComVariant>::iterator itm = idValue.find(dispIdMember);
		if (itm!=idValue.end())
		{
			(*itm).second = pDispParams->rgvarg[0];
		}
		else if(dispIdMember > DISPID_VALUE)
		{
			idValue.insert(std::pair<int,CComVariant>(dispIdMember, pDispParams->rgvarg[0]));
		}
	}
	return S_OK;
}
