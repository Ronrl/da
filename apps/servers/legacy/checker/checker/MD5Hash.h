#pragma once

#include "MD5Defines.h"
#include <atlstr.h>

class CMD5Hash  
{
public:
	static CString GetMD5(const BYTE* pBuf, size_t nLength);

protected:
	//constructor/destructor
	CMD5Hash();
	virtual ~CMD5Hash() {};

	//RSA MD5 implementation
	void Transform(const BYTE Block[64]);
	void Update(const BYTE* Input, ULONG nInputLen);
	CString Final();
	inline DWORD RotateLeft(DWORD x, int n);
	inline void FF( DWORD& A, DWORD B, DWORD C, DWORD D, DWORD X, DWORD S, DWORD T);
	inline void GG( DWORD& A, DWORD B, DWORD C, DWORD D, DWORD X, DWORD S, DWORD T);
	inline void HH( DWORD& A, DWORD B, DWORD C, DWORD D, DWORD X, DWORD S, DWORD T);
	inline void II( DWORD& A, DWORD B, DWORD C, DWORD D, DWORD X, DWORD S, DWORD T);

	//utility functions
	inline void DWordToByte(BYTE* Output, DWORD* Input, UINT nLength);
	inline void ByteToDWord(DWORD* Output, const BYTE* Input, UINT nLength);

private:
	BYTE  m_lpszBuffer[64];		//input buffer
	ULONG m_nCount[2];			//number of bits, modulo 2^64 (lsb first)
	ULONG m_lMD5[4];			//MD5 checksum

};

