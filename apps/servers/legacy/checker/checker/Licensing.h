// Licensing.h : Declaration of the CLicensing

#pragma once
#include "resource.h"       // main symbols
#include "checker_i.h"
#include "optex.h"

#if defined(_WIN32_WCE) && !defined(_CE_DCOM) && !defined(_CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA)
#error "Single-threaded COM objects are not properly supported on Windows CE platform, such as the Windows Mobile platforms that do not include full DCOM support. Define _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA to force ATL to support creating single-thread COM object's and allow use of it's single-threaded COM object implementations. The threading model in your rgs file was set to 'Free' as that is the only threading model supported in non DCOM Windows CE platforms."
#endif

// CLicensing

class ATL_NO_VTABLE CLicensing :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CLicensing, &CLSID_Licensing>,
	public IDispatchImpl<ILicensing2, &IID_ILicensing2, &LIBID_DeskAlertsServerLib, /*wMajor =*/ 1, /*wMinor =*/ 0>
{
public:
	CLicensing()
	{
	}

DECLARE_REGISTRY_RESOURCEID(IDR_LICENSING)

BEGIN_COM_MAP(CLicensing)
	COM_INTERFACE_ENTRY(ILicensing)
	COM_INTERFACE_ENTRY(ILicensing2)
	//COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY2(IDispatch, ILicensing)
	COM_INTERFACE_ENTRY2(IDispatch, ILicensing2)
END_COM_MAP()

	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

public:
	static BOOL checkLicenseAndTrialState(IDispatch *connection);

private:
	static COptex g_cs;
	static void saveLockedStates(IDispatch *connection);
	static void restoreLockedStates(IDispatch *connection);
	static LONG getLicenseCount(IDispatch *connection);
	static LONG getTrialTime(IDispatch *connection);
	static LONG getCurrentUsersCount(IDispatch *connection);
	static LONG getElipseSinseServerWasInstalled(IDispatch *connection);

public:
//ILicensing
	STDMETHOD(getLicenseCount)(
		/* [in] */ IDispatch *connection,
		/* [retval][out] */ LONG *result);

	STDMETHOD(getCurrentUsersCount)(
		/* [in] */ IDispatch *connection,
		/* [retval][out] */ LONG *result);

public:
//ILicensing2
	STDMETHOD(getTrialTime)(
		/* [in] */ IDispatch *connection,
		/* [retval][out] */ LONG *result);

	STDMETHOD(getTrialExpire)(
		/* [in] */ IDispatch *connection,
		/* [retval][out] */ LONG *result);

	STDMETHOD(getTrialState)(
		/* [in] */ IDispatch *connection,
		/* [retval][out] */ BOOL *result);
};

OBJECT_ENTRY_AUTO(__uuidof(Licensing), CLicensing)
