SetCompressor lzma

!define DA_PROD_NAME "DeskAlerts Server"
!define PRODUCT_NAME "DeskAlerts Server Update"
!define PRODUCT_PUBLISHER "Softomate, LLC"
!define PRODUCT_WEB_SITE "http://www.softomate.com"
!define PRODUCT_UNINST_KEY "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_NAME}"
!define DA_PROD_UNINST_KEY "Software\Microsoft\Windows\CurrentVersion\Uninstall\${DA_PROD_NAME}"
!define PRODUCT_UNINST_ROOT_KEY "HKLM"
!define PRODUCT_UNINST_FILEDIR "$WINDIR\$$UninstallDeskalerts$$"
!define PRODUCT_UNINST_FILENAME "UninstallUpdate"
!define BACKUPDIR "${PRODUCT_UNINST_FILEDIR}\backup"

!define INSTALLED_conf "$INSTDIR\admin\config.inc"
!define REGPATH "Software\Softomate\ServerInstallation"
;--------------------------------
!define nameLicenseCount "Number of licenses: "
!define nameTrialCount "Number of trial days: "
;--------------------------------
;General
  
	;Name and file
	Name "${PRODUCT_NAME}"
	OutFile "deskalerts_server_update.exe"

	;Default installation folder
	InstallDir "C:\Inetpub\wwwroot\Deskalerts"

	;Get installation folder from registry if available
	InstallDirRegKey ${PRODUCT_UNINST_ROOT_KEY} "${DA_PROD_UNINST_KEY}" "InstallLocation"

;---------------------
;Header files

	!addincludedir "..\installer\include"
	!include "MUI.nsh"
	!include "x64.nsh"
	!include "StrFunc.nsh"
	!include "TextFunc.nsh"
	${StrTrimNewLines}
	!include "..\installer\include\StrRep.nsh" ;only custom version
	${StrReplace}
	${UnStrReplace}
	!include "..\installer\include\ReplaceInFile.nsh" ;only custom version
	${ReplaceInFile}
	!include "..\installer\include\ReadConfig.nsh"
	${ReadFromConfig}
	${UnReadFromConfig}
	!include "..\installer\include\CustomFunction.nsh"
	;${StrTok} defined in Trim.nsh

;--------------------------------
;Defines

	!define MUI_LANGDLL_REGISTRY_ROOT "${PRODUCT_UNINST_ROOT_KEY}"
	!define MUI_LANGDLL_REGISTRY_KEY "${PRODUCT_UNINST_KEY}"
	!define MUI_LANGDLL_REGISTRY_VALUENAME "NSIS:Language"

;--------------------------------
;Interface Settings

	!define MUI_HEADERIMAGE
	!define MUI_HEADERIMAGE_BITMAP "..\installer\deskalerts.bmp" ; optional
	!define MUI_HEADERIMAGE_UNBITMAP "..\installer\deskalerts.bmp" ; optional
	!define MUI_ABORTWARNING

;--------------------------------
;Pages

	!define MUI_PAGE_HEADER_TEXT "Choose Deskalerts Server Location"
	!insertmacro MUI_PAGE_DIRECTORY

!ifdef withDB
	Page custom SetDB ValidateDB
!endif

	!insertmacro MUI_PAGE_INSTFILES

	!insertmacro MUI_UNPAGE_CONFIRM
	!insertmacro MUI_UNPAGE_INSTFILES

;--------------------------------
;Languages

	!insertmacro MUI_LANGUAGE "English"

;--------------------------------
;Add additional information
VIProductVersion "${PRODUCT_VERSION}"
VIAddVersionKey /LANG=${LANG_ENGLISH} "ProductName" "${PRODUCT_NAME}"
VIAddVersionKey /LANG=${LANG_ENGLISH} "CompanyName" "${PRODUCT_PUBLISHER}"
VIAddVersionKey /LANG=${LANG_ENGLISH} "LegalCopyright" "Copyright (c) 2006-2011 Softomate"
VIAddVersionKey /LANG=${LANG_ENGLISH} "FileVersion" "${PRODUCT_VERSION}"
VIAddVersionKey /LANG=${LANG_ENGLISH} "OriginalFilename" "deskalerts_server_update.exe"
;--------------------------------
!define delim ""
!define COMMENTS ""

!ifdef licenses
	!define TMP "${COMMENTS}${delim}${nameLicenseCount}${licenses}"
	!undef COMMENTS
	!define COMMENTS "${TMP}"
!endif
!ifdef trial
	!ifdef TMP
		!undef COMMENTS
		!define COMMENTS "${TMP},$\r$\n${nameTrialCount}${trial}"
	!else
		!define TMP "${COMMENTS}${delim}${nameTrialCount}${trial}"
		!undef COMMENTS
		!define COMMENTS "${TMP}"
	!endif
!endif
VIAddVersionKey /LANG=${LANG_ENGLISH} "Comments" "${COMMENTS}"
;VIAddVersionKey /LANG=${LANG_ENGLISH} "FileDescription" "${PRODUCT_NAME} Installer.$\r$\n${COMMENTS}"
VIAddVersionKey /LANG=${LANG_ENGLISH} "FileDescription" "${PRODUCT_NAME} Installer"

;--------------------------------
;Reserve Files
  
  ;If you are using solid compression, files that are required before
  ;the actual installation should be stored first in the data block,
  ;because this will make your installer start faster.
  
  ReserveFile "${NSISDIR}\Plugins\InstallOptions.dll"
  ReserveFile "..\installer\include\db.ini"

  Icon "${NSISDIR}\Contrib\Graphics\Icons\orange-install.ico"
  UninstallIcon "${NSISDIR}\Contrib\Graphics\Icons\orange-uninstall.ico"

;--------------------------------
;Variables

Var "NeedToReloadIIS"
Var "is_update"

!ifdef withDB
Var "host"
Var "dbname"
Var "user_name"
Var "password"
Var "win_auth"
!endif

;--------------------------------
;Installer Sections
!ifdef includeDLLs
Function backupDlls

	ReadRegStr $0 HKCR "DeskAlertsServer.AlertXML\CLSID" ""
	ReadRegStr $0 HKCR "CLSID\$0\TypeLib" "" ;Path of copy x64 dll from

	ReadRegStr $1 HKCR "TypeLib\$0\1.0\0\win64" "" ;Path of copy x32 dll from
	ReadRegStr $2 HKCR "TypeLib\$0\1.0\0\win32" "" ;Path of copy x32 dll from
	
	StrCpy $3 "${BACKUPDIR}\DeskAlertsServer64.dll" ;Path of copy x64 dll to
	StrCpy $4 "${BACKUPDIR}\DeskAlertsServer32.dll" ;Path of copy x32 dll to
	StrCpy $5 0 ; only 0 or 1, set 0 to overwrite file if it already exists
	
	CopyFiles "$1" "$3"
	CopyFiles "$2" "$4"	
	
FunctionEnd

!macro GetDllsPaths 
	FindFirst $0 $1 "$APPDATA\DeskAlertsServer\DeskAlertsServer32*.dll"
	files_loop32:
		StrCmp $1 "" files_done32
		nsExec::Exec 'regsvr32.exe /u /s "$APPDATA\DeskAlertsServer\$1"'
		pop $R0
		FindNext $0 $1
		Goto files_loop32
	files_done32:
	FindClose $0

	StrCpy $R1 "$APPDATA\DeskAlertsServer\DeskAlertsServer32.dll"
	ClearErrors
	Delete "$APPDATA\DeskAlertsServer\DeskAlertsServer32*"
	IfErrors 0 flOk32
		StrCpy $R0 0 ; Set 0
		StrCpy $NeedToReloadIIS 1 ; Need IIS Reload
		flEx32:
		IntOp $R0 $R0 + 1 ; Inc in loop
		IfFileExists "$APPDATA\DeskAlertsServer\DeskAlertsServer32.dl$R0" flEx32
		ClearErrors
		Rename "$APPDATA\DeskAlertsServer\DeskAlertsServer32.dll" "$APPDATA\DeskAlertsServer\DeskAlertsServer32.dl$R0"
		IfErrors flCntMove32
		Delete /REBOOTOK "$APPDATA\DeskAlertsServer\DeskAlertsServer32.dl$R0"
		Goto flOk32
	flCntMove32:	
		StrCpy $R0 0 ; Set 0
		flDEx32:
		IntOp $R0 $R0 + 1 ; Inc in loop
		StrCpy $R1 "$APPDATA\DeskAlertsServer\DeskAlertsServer32[$R0].dll"
		IfFileExists "$R1" flDEx32
		Delete /REBOOTOK "$APPDATA\DeskAlertsServer\DeskAlertsServer32.dll"
	flOk32:

	; Delete/Rename DeskAlertsServer64.dll if it exsists
	FindFirst $0 $1 "$APPDATA\DeskAlertsServer\DeskAlertsServer64*.dll"
	files_loop64:
		StrCmp $1 "" files_done64
		nsExec::Exec 'cmd /c cd /d "%WinDir%\SysNative"&cmd.exe /c start /w regsvr32.exe /u /s "$APPDATA\DeskAlertsServer\$1"'
		pop $R0
		FindNext $0 $1
		Goto files_loop64
	files_done64:
	FindClose $0

	StrCpy $R2 "$APPDATA\DeskAlertsServer\DeskAlertsServer64.dll"
	ClearErrors
	Delete "$APPDATA\DeskAlertsServer\DeskAlertsServer64*"
	IfErrors 0 flOk64
		StrCpy $R0 0 ; Set 0
		StrCpy $NeedToReloadIIS 1 ; Need IIS Reload
		flEx64:
		IntOp $R0 $R0 + 1 ; Inc in loop
		IfFileExists "$APPDATA\DeskAlertsServer\DeskAlertsServer64.dl$R0" flEx64
		ClearErrors
		Rename "$APPDATA\DeskAlertsServer\DeskAlertsServer64.dll" "$APPDATA\DeskAlertsServer\DeskAlertsServer64.dl$R0"
		IfErrors flCntMove64
		Delete /REBOOTOK "$APPDATA\DeskAlertsServer\DeskAlertsServer64.dl$R0"
		Goto flOk64
	flCntMove64:	
		StrCpy $R0 0 ; Set 0
		flDEx64:
		IntOp $R0 $R0 + 1 ; Inc in loop
		StrCpy $R2 "$APPDATA\DeskAlertsServer\DeskAlertsServer64[$R0].dll"
		IfFileExists "$R2" flDEx64
		Delete /REBOOTOK "$APPDATA\DeskAlertsServer\DeskAlertsServer64.dll"
	flOk64:

	Push $R2
	Push $R1
!macroend
!endif

Section "Dummy Section" SecDummy
	SetOutPath "$INSTDIR"

	DetailPrint "Backuping..."
	RMDir /r "${BACKUPDIR}"
	CreateDirectory ${BACKUPDIR}
	!include backup.nsh

!ifdef includeDLLs
	Call backupDlls
!endif

!ifdef withFiles
	DetailPrint "Installing..."	
!include files.nsh
	!endif

	detailprint "Set permissions..."
	Call SetRights

!ifdef withDB	
	Call ExecuteSQL
!endif	

SectionEnd

;--------------------------------
;Installer Functions

Function .onInit
	InitPluginsDir
	File /oname=$PLUGINSDIR\db.ini "..\installer\include\db.ini"
	StrCpy $NeedToReloadIIS 0
	StrCpy $is_update 1
	ReadRegStr $0 ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "GUID"
	StrCmp "$0" "${UPDATE_GUID}" 0 no_installed
		MessageBox MB_ICONSTOP|MB_OK "This update already installed."
		Abort
	no_installed:
FunctionEnd

;-------------------------------------------------------------------------------- Uninstall -----------

Section -Post
	WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayName"      "${PRODUCT_NAME}"
	WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "UninstallString"  "${PRODUCT_UNINST_FILEDIR}\${PRODUCT_UNINST_FILENAME}.exe"
	WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayIcon"      "${PRODUCT_UNINST_FILEDIR}\${PRODUCT_UNINST_FILENAME}.exe"
	;WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayVersion"   "${PRODUCT_VERSION}"
	WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "URLInfoAbout"     "${PRODUCT_WEB_SITE}"
	WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "URLUpdateInfo"    "${PRODUCT_WEB_SITE}"
	WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "HelpLink"         "${PRODUCT_WEB_SITE}"
	WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "Publisher"        "${PRODUCT_PUBLISHER}"
	WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "InstallLocation"  "$INSTDIR"
	WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "GUID"             "${UPDATE_GUID}"

	DetailPrint "Creating uninstaller..."
	SetDetailsPrint none
	CreateDirectory "${PRODUCT_UNINST_FILEDIR}"
	SetFileAttributes "${PRODUCT_UNINST_FILEDIR}" ARCHIVE|READONLY|HIDDEN|SYSTEM
	WriteUninstaller "${PRODUCT_UNINST_FILEDIR}\${PRODUCT_UNINST_FILENAME}.exe"
	SetDetailsPrint both

	StrCmp $NeedToReloadIIS 0 DoNothing
		MessageBox MB_ICONINFORMATION|MB_OK "Please restart IIS to complete the install process."
	DoNothing:
SectionEnd

Function un.onInit
	ReadRegStr $INSTDIR ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "InstallLocation"
FunctionEnd

Section "Uninstall"
	SetShellVarContext all
	SetOutPath "$INSTDIR"
	DetailPrint "Restoring from backup..."
	!include restore.nsh

!ifdef includeDLLs
	!insertmacro GetDllsPaths
	Pop $R1
	Pop $R2
	StrCpy $3 "${BACKUPDIR}\DeskAlertsServer64.dll" ;Path of copy x64 dll to
	StrCpy $4 "${BACKUPDIR}\DeskAlertsServer32.dll" ;Path of copy x32 dll to
	StrCpy $5 0 ; only 0 or 1, set 0 to overwrite file if it already exists
	
	CopyFiles "${BACKUPDIR}\DeskAlertsServer32.dll" "$R1"
	CopyFiles "${BACKUPDIR}\DeskAlertsServer64.dll" "$R2"

	nsExec::Exec 'regsvr32.exe /s "$R1"'
	pop $R0
	DetailPrint "registering '$R1'"
!endif

	${If} ${RunningX64}
		DetailPrint "registering '$R2'"
		nsExec::Exec 'cmd /c cd /d "%WinDir%\SysNative"&cmd.exe /c start /w regsvr32.exe /s "$R2"'
		pop $R0
	${EndIf}
	
	DetailPrint "Clearing..."
	SetDetailsPrint none
	DeleteRegKey ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}"
	RMDir /r "${BACKUPDIR}"
	Delete /REBOOTOK "${PRODUCT_UNINST_FILEDIR}\${PRODUCT_UNINST_FILENAME}.exe"
	SetDetailsPrint both
SectionEnd

Function SetRights
	;ExpandEnvStrings $1 "IUSR_%COMPUTERNAME%"
	StrCpy $1 "(S-1-1-0)" ; Everyone

	${SetRightsWithPrint} "$INSTDIR\admin\images\upload" "$1" "FullAccess"

	${SetRightsWithPrint} "$INSTDIR\admin\images\upload_document" "$1" "FullAccess"

	${SetRightsWithPrint} "$INSTDIR\admin\images\upload_flash" "$1" "FullAccess"

	${SetRightsWithPrint} "$INSTDIR\admin\images\upload_video" "$1" "FullAccess"

	${SetRightsWithPrint} "$INSTDIR\admin\example_image_list.js" "$1" "FullAccess"

	${SetRightsWithPrint} "$INSTDIR\admin\csv" "$1" "FullAccess"

	${SetRightsWithPrint} "$INSTDIR\admin\scripts" "$1" "FullAccess"

	${SetRightsWithPrint} "$INSTDIR\encrypt" "$1" "FullAccess"

	${SetRightsWithPrint} "$INSTDIR\enc" "$1" "FullAccess"
FunctionEnd

!ifdef withDB
Var "notfirstrun_db"
Function SetDB

	!insertmacro MUI_HEADER_TEXT "Setting up the DeskAlerts Database" "Configure database settings"	

	StrCmp "$notfirstrun_db" "1" not_first_run_db
		${ReadFromASPorRegistryAndWrite} "$PLUGINSDIR\db.ini" "Field 1" "host" "host" $0
		;${ReadFromASPorRegistry} "host" "host" $0
		;StrCmp "$0" "" DoNothing
		;	${StrTok} $1 "$0" "\" 1 0 ;instance
		;	${StrTok} $2 "$0" "\" 0 0
		;	${StrTok} $3 "$2" "," 0 0 ;host
		;	${StrTok} $4 "$2" "," 1 0 ;port
		;	StrCmp "$3" "" +2
		;		WriteINIStr "$PLUGINSDIR\db.ini" "Field 1" "State" $3
		;	StrCmp "$1" "" +2
		;		WriteINIStr "$PLUGINSDIR\db.ini" "Field 13" "State" $1
		;	StrCmp "$4" "" +2
		;		WriteINIStr "$PLUGINSDIR\db.ini" "Field 15" "State" $4
		;DoNothing:

		${ReadFromASPorRegistryAndWrite} "$PLUGINSDIR\db.ini" "Field 2" "dbname" "dbname" $0
		${ReadFromASPorRegistryAndWrite} "$PLUGINSDIR\db.ini" "Field 3" "user_name" "user_name" $0
		${ReadFromASPorRegistryAndWrite} "$PLUGINSDIR\db.ini" "Field 4" "password" "password" $0
		${ReadFromASPorRegistryAndWrite} "$PLUGINSDIR\db.ini" "Field 11" "win_auth" "win_auth" $0

		StrCpy $1 ""
		StrCpy $2 "DISABLED"
		StrCmp $0 "1" 0 +3
		StrCpy $1 "DISABLED"
		StrCpy $2 ""

		WriteINIStr "$PLUGINSDIR\db.ini" "Field 3" "Flags" "$1"
		WriteINIStr "$PLUGINSDIR\db.ini" "Field 4" "Flags" "$1"
		WriteINIStr "$PLUGINSDIR\db.ini" "Field 13" "Flags" "$2"
		WriteINIStr "$PLUGINSDIR\db.ini" "Field 15" "Flags" "$2"

		${ReadFromASPorRegistryAndWrite} "$PLUGINSDIR\db.ini" "Field 13" "IIS_user" "IIS_User" $0

		StrCpy $notfirstrun_db "1"
	not_first_run_db:

	Push $R0
	InstallOptions::dialog "$PLUGINSDIR\db.ini"
	Pop $R0
	Pop $R0
FunctionEnd

Function ValidateDB
	ReadINIStr $0 "$PLUGINSDIR\db.ini" "Settings" "State"
	ReadINIStr $win_auth "$PLUGINSDIR\db.ini" "Field 11" "State"	

	IntCmp $0 0 if_next ;if $0 = 0 was clicked next button otherwise it was notify
		ReadINIStr $1 "$PLUGINSDIR\db.ini" "Field 3" "HWND"
		ReadINIStr $2 "$PLUGINSDIR\db.ini" "Field 4" "HWND"
		ReadINIStr $8 "$PLUGINSDIR\db.ini" "Field 13" "HWND"
		ReadINIStr $9 "$PLUGINSDIR\db.ini" "Field 15" "HWND"

		IntOp $3 $win_auth !
		EnableWindow $1 $3
		EnableWindow $2 $3

		IntOp $3 $3 !
		EnableWindow $8 $3
		EnableWindow $9 $3

		StrCpy $4 ""
		StrCpy $5 "DISABLED"
		IntCmp $win_auth 0 +3
		StrCpy $4 "DISABLED"
		StrCpy $5 ""

		WriteINIStr "$PLUGINSDIR\db.ini" "Field 3" "Flags" "$4"
		WriteINIStr "$PLUGINSDIR\db.ini" "Field 4" "Flags" "$4"
		WriteINIStr "$PLUGINSDIR\db.ini" "Field 13" "Flags" "$5"
		WriteINIStr "$PLUGINSDIR\db.ini" "Field 15" "Flags" "$5"

		IntCmp $win_auth 0 +2
		StrCpy $4 ""		

		WriteINIStr "$PLUGINSDIR\db.ini" "Field 13" "Flags" "$4"
		WriteINIStr "$PLUGINSDIR\db.ini" "Field 15" "Flags" "$4"

		Abort
	if_next:

	ReadINIStr $0 "$PLUGINSDIR\db.ini" "Field 1" "State" ;host
	StrCpy $host $0

	;ReadINIStr $1 "$PLUGINSDIR\db.ini" "Field 15" "State" ;port
	;StrCmp "$1" "(default)" +3
	;StrCmp "$1" "" +2
	;	StrCpy $host "$host,$1"

	;ReadINIStr $2 "$PLUGINSDIR\db.ini" "Field 13" "State" ;instance
	;StrCmp "$2" "(default)" +3
	;StrCmp "$2" "" +2
	;	StrCpy $host "$host\$2"

	ReadINIStr $dbname "$PLUGINSDIR\db.ini" "Field 2" "State"

	StrCpy $user_name ""
	StrCpy $password ""

	IntCmp $win_auth 1 if_win_auth
		ReadINIStr $user_name "$PLUGINSDIR\db.ini" "Field 3" "State"
		ReadINIStr $password "$PLUGINSDIR\db.ini" "Field 4" "State"

		StrCmp "$user_name" "" 0 finish_user_check
			MessageBox MB_OK "Username cannot be empty."
			Abort
		finish_user_check:
	if_win_auth:

	ReadINIStr $win_auth "$PLUGINSDIR\db.ini" "Field 11" "State"

begin_db:
	detailprint "Loggin on to SQL server $host"
	MSSQL_OLEDB::SQL_Logon /NOUNLOAD "$host" "$user_name" "$password"
	pop $1
	pop $0

	StrCmp "$1" "0" if_not_equal_err
		MSSQL_OLEDB::SQL_GetError /NOUNLOAD
		Pop $1
		Pop $0
		MessageBox MB_OK "$0$\r$\nPlease enter another username/password or host"
		Abort
	if_not_equal_err:
		MSSQL_OLEDB::SQL_Execute /NOUNLOAD "USE master"
		pop $1
		pop $0

		${StrReplace} $0 "'" "''" "$dbname"
		MSSQL_OLEDB::SQL_Execute /NOUNLOAD "SELECT 1 FROM sysdatabases WHERE name='$0'"
		pop $1
		pop $0
		
		StrCmp "$1" "0" 0 db_error ;sql request error
			MSSQL_OLEDB::SQL_GetRow /NOUNLOAD
			pop $1
			pop $0

			StrCmp "$1" "1" db_error 0  ;get row error
			StrCmp "$0" "1" update_end ;get 1
				MessageBox MB_OK "There is no data base with name $dbname."
				Abort
		update_end:
		
		${StrReplace} $0 "]" "]]" "$dbname"
		MSSQL_OLEDB::SQL_Execute /NOUNLOAD "USE [$0]"
		pop $1
		pop $0

			MSSQL_OLEDB::SQL_Execute /NOUNLOAD "SELECT PERMISSIONS()&2"
			pop $1
			pop $0

		StrCmp "$1" "0" 0 db_error ;can't check
			MSSQL_OLEDB::SQL_GetRow /NOUNLOAD
			pop $1
			pop $0
			
			StrCmp "$1" "0" 0 db_error ;can't check
			StrCmp "$0" "0" 0 db_end
				MessageBox MB_OK "Current user have no rights to alter tables in database $dbname.$\r$\nPlease enter another username/password or give more rights to this user."
				Abort
		
		db_error:
			MSSQL_OLEDB::SQL_GetError /NOUNLOAD
			Pop $1
			Pop $0
			MessageBox MB_ABORTRETRYIGNORE "$0$\r$\n" IDRETRY begin_db IDIGNORE db_end
			Abort
		db_end:
FunctionEnd

Function ExecuteSQL
	SetDetailsPrint none
	SetOutPath $PLUGINSDIR
	File /r "patches\*.sql"
	SetDetailsPrint both

	DetailPrint "Executing SQL scripts"
	FindFirst $0 $2 "$PLUGINSDIR\*.sql"
	files_loop:
		StrCmp $2 "" files_done
			DetailPrint "Script: $2"
			SetDetailsPrint none
			${ReplaceParamInSQLFile} "$PLUGINSDIR\$2" "dbname" "$dbname"
			SetDetailsPrint both
			${ExecuteSQLScript} "$PLUGINSDIR\$2"
		FindNext $0 $2
		Goto files_loop
	files_done:
	FindClose $0	
FunctionEnd

!endif

