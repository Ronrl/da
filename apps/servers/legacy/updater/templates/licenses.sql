USE [%dbname_hook%];
SET LANGUAGE British
UPDATE [settings] SET 
[val] =  CASE  
			WHEN [val]='unlimited' THEN %VAL% 
			ELSE [val]+%VAL% 
		END
WHERE [name] = 'licenses'
