@ECHO OFF
cls
call "C:\Program Files (x86)\Microsoft Visual Studio\2017\Enterprise\Common7\Tools\VsDevCmd.bat"
cd /d "%~dp0"
echo.Building project with Visual Studio 2017 Enterprise
.nuget\nuget.exe restore DeskAlertsDotNet.csproj -PackagesDirectory packages
.nuget\nuget.exe restore NewInstaller\NewInstaller.sln -PackagesDirectory NewInstaller\packages
echo.
echo.Building project with Visual Studio...
echo.
echo.^>^>Release x86
rem devenv  "..\DeskAlertsAsp.sln" /build "Release 9|Any CPU"
echo.
:start_build
echo Welcome to the New DeskAlerts Patcher!
echo Please, follow the instruction:
rmdir /q /s patches 2>nul
mkdir patches 
set checkS=n
::trial extention patch
:trial_patch
set /p choice=Do you want patch current trial status?" %text%[y/n] 
if /i '%choice:~0,1%'=='n' set choice=n& goto system_unlock_patch
if /i not '%choice:~0,1%'=='y' goto trial_patch
set /p choice=Do you want just add few license days?" %text%[y/n] 
if /i '%choice:~0,1%'=='n' set choice=n& goto trial_patch_no
if /i not '%choice:~0,1%'=='y' goto trial_patch
:days_validate
set /p days=Please enter how many days you want to add: 
echo.%days%|findstr /r "^[0-9][0-9]*$" > nul
if errorlevel 1 echo Wrong number string. & goto days_validate
copy templates\increment_l.sql patches\increment_l.sql > nul
cscript replace.vbs patches\increment_l.sql "%%VAL%%" %days% > nul
set attr=%attr% /DwithDB
set checkS=y
goto set_version

::New trial date
:trial_patch_no
set /p trialdate=Please enter new trial startdate in SYSTEM format style "dd/mm/yyyy" or "mm/dd/yyyy": 
echo %trialdate%|findstr /r "^[0-9][0-9]/[0-9][0-9]/[0-9][0-9][0-9][0-9]$" > nul
if errorlevel 1 echo Wrong date string. & goto trial_patch_no
for /f "delims=" %%a in ('cscript //nologo date.vbs %trialdate%') do (set ScriptOut=%%a) > nul
copy templates\change_m.sql patches\change_m.sql > nul
cscript replace.vbs patches\change_m.sql "%%DATE%%" %trialdate% > nul
if /i '%ScriptOut%'=='0' echo Wrong date. & goto trial_patch_no
:trial_length
set /p triallength=Please enter number of trial days (0 - not trial)  
echo %triallength%|findstr /r "^[0-9][0-9]*$" > nul
if errorlevel 1 echo Wrong number. & goto trial_length
if %triallength% lss 0 echo Trial days is less than zero & goto trial_length
for /f "delims=" %%a in ('cscript //nologo datediff.vbs %date% %trialdate%') do (set ScriptOut=%%a) > nul
set /a ndays=%ScriptOut%+%triallength%
if %ndays% LSS 0 echo Trial period since selected date is already expired. Please, choose a new trial date & goto trial_patch_no
echo Trial will expire in %ndays% days
copy templates\reset_t.sql patches\reset_t.sql > nul
cscript replace.vbs patches\reset_t.sql "%%DATE%%" %trialdate% > nul
cscript replace.vbs patches\reset_t.sql "%%VAL%%" %triallength% > nul
set attr=%attr% /DwithDB
set checkS=y
goto set_version

::system unlock patch
:system_unlock_patch
set /p choice=Do you want to unlock system?" %text%[y/n] 
if /i '%choice:~0,1%'=='n' set choice=n& goto reset_patch
if /i not '%choice:~0,1%'=='y' goto system_unlock_patch
copy templates\unlock.sql patches\unlock.sql > nul
set attr=%attr% /DwithDB
set checkS=y
goto set_version

::admin password reset patch
:reset_patch
set /p choice=Do you want to reset admin password?" %text%[y/n] 
if /i '%choice:~0,1%'=='n' set choice=n& goto add_licenses
if /i not '%choice:~0,1%'=='y' goto reset_patch
copy templates\reset_a.sql patches\reset_a.sql > nul
set attr=%attr% /DwithDB
set checkS=y
goto set_version

::add some licenses patch
:add_licenses
set /p choice=Do you want to add licenses?" %text%[y/n] 
if /i '%choice:~0,1%'=='n' set choice=n& goto update_main
if /i not '%choice:~0,1%'=='y' goto add_licenses
:checkS_l
set /p licenses=Please enter number of licenses  
echo.%licenses%|findstr /r "^[0-9][0-9]*$" > nul
if errorlevel 1 echo Wrong number of licenses. & goto checkS_l
copy templates\licenses.sql patches\licenses.sql > nul
cscript replace.vbs patches\licenses.sql "%%VAL%%" %licenses% > nul
set attr=%attr% /DwithDB
set checkS=y
goto set_version

::update maintenance patch
:update_main
set /p choice=Do you want to update maintenance date?" %text%[y/n] 
if /i '%choice:~0,1%'=='n' set choice=n& goto add_files
if /i not '%choice:~0,1%'=='y' goto update_main
:main_date
set /p newdate=Please enter new maintenance date in style dd/mm/yyyy: 
echo %newdate%|findstr /r "^[0-9][0-9]/[0-9][0-9]/[0-9][0-9][0-9][0-9]$" > nul
if errorlevel 1 echo Wrong date. & goto main_date
for /f "delims=" %%a in ('cscript //nologo date.vbs %newdate%') do (set ScriptOut=%%a) > nul
if /i '%ScriptOut%'=='0' echo Wrong date. & goto main_date
copy templates\change_m.sql patches\change_m.sql > nul
cscript replace.vbs patches\change_m.sql "%%DATE%%" %newdate% > nul
set attr=%attr% /DwithDB
set checkS=y
goto set_version

::add custom files patch
:add_files
set /p choice=It seems you're building custom patch, do you need to add the files?" %text%[y/n] 
if /i '%choice:~0,1%'=='n' set choice=n& goto add_sql
if /i not '%choice:~0,1%'=='y' goto add_files
set _TMP=
for /f "delims=" %%a in ('dir /b files') do set _TMP=%%a

set _TMP=
for /f "delims=" %%a in ('dir /b files') do set _TMP=%%a

IF {%_TMP%}=={} (
	echo Files folder is empty. Please, put some files in files/ folder then restart builder
ECHO.
goto set_version
) ELSE (
set filesAttr=%filesAttr% /DwithFiles
set checkS=y
goto add_sql
)		

::add custom sql files
:add_sql
set /p choice=Do you need to add some custom SQL files?" %text%[y/n] 
if /i '%choice:~0,1%'=='n' set choice=n& goto set_version
if /i not '%choice:~0,1%'=='y' goto add_sql
set _TMP=
for /f "delims=" %%a in ('dir /b sql') do set _TMP=%%a

IF {%_TMP%}=={} (
	echo Sql folder is empty. Please, put sql files in sql/ folder then restart builder
ECHO.
goto set_version
) ELSE (
	xcopy /s sql\*.sql patches\ > nul
set attr=%attr% /DwithDB
set checkS=y
goto set_version
)		

::finalizing builder
:set_version
set verAttr=/DPRODUCT_VERSION=0.0.0.0
set /p choice=Do you need to supply a new version number? "%text%[y/n] 
if /i '%choice:~0,1%'=='n' set choice=n& goto go_next
if /i not '%choice:~0,1%'=='y' goto set_version
goto ver_input

:ver_input
set /p version=Please enter version in style 0.0.0.0  
:check
echo %version%|findstr /r "^[0-9][0-9]*\.[0-9][0-9]*\.[0-9][0-9]*\.[0-9][0-9]*$" > nul
if errorlevel 1 echo Wrong version string. & goto ver_input
copy templates\change_v.sql patches\change_v.sql > nul
cscript replace.vbs patches\change_v.sql "%%VAL%%" %version% > nul
set verAttr=/DPRODUCT_VERSION=%version%
set checkS=y
:go_next
if /i '%checkS%'=='n' goto ask_to_repeat		
ECHO.
ECHO Building...
ECHO.
cd files
echo.>..\files.nsh
echo.>..\backup.nsh
echo.>..\restore.nsh
for %%i in (*) do (
	call :write "%%i"
)
for /d %%i in (*) do (
	call :browse "%%~i"
)
cd %~p0
goto build

:browse
echo CreateDirectory "$INSTDIR\%~1">>..\files.nsh
echo CreateDirectory "${BACKUPDIR}\%~1">>..\backup.nsh
for %%i in ("%~1\*") do (
	call :write "%%i"
)
for /d %%i in ("%~1\*") do (
	call :browse "%%~i"
)
goto :eof

:write
echo File "/oname=%~1" "files\%~1">>..\files.nsh
echo Rename "$INSTDIR\%~1" "${BACKUPDIR}\%~1">>..\backup.nsh
echo Delete "$INSTDIR\%~1">>..\restore.nsh
echo Rename "${BACKUPDIR}\%~1" "%~1">>..\restore.nsh
goto :eof

:ask_to_repeat
set /p choice=It seems you didn't select anything. Do you want to repeat build process? "%text%[y/n] 
if /i '%choice:~0,1%'=='n' set choice=n& goto end_build
if /i not '%choice:~0,1%'=='y' goto go_next
goto start_build

:build
echo Set TypeLib = CreateObject("Scriptlet.TypeLib"):WScript.echo TypeLib.Guid>guid.vbs
for /f %%i in ('cscript guid.vbs') do set GUID=%%i
del guid.vbs
ECHO. 
..\NSIS\makensis.exe "/DUPDATE_GUID=%GUID%" %attr% %filesAttr% %verAttr% installer.nsi > build.log
if "%ERRORLEVEL%" == "0" (
ECHO Done. View build.log
ECHO.
..\installer\signtool.exe sign /f ..\installer\ToolbarStudioInc.pfx /p softomate54Pas /t http://tsa.starfieldtech.com/ /v deskalerts_server_update.exe
ECHO .
7z.exe a -tzip deskalerts_server_update.zip deskalerts_server_update.exe
) else (
ECHO Failed. View build.log
)
ECHO.
:end_build
rmdir /q /s tmp 2>nul
rmdir /q /s patches 2>nul
ECHO.
pause	