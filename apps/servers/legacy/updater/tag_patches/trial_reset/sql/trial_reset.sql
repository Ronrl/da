
ALTER TABLE [users]
ALTER COLUMN [last_request] [datetime] NULL

UPDATE [users] SET [reg_date] = GETDATE(), [last_date] = NULL, [last_request] = NULL, [next_request] = NULL, [last_standby_request] = NULL, [next_standby_request] = NULL WHERE [name] = N'admin' AND [role] = 'A'
