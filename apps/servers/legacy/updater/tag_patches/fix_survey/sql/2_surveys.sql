USE [%dbname_hook%]

GO

IF EXISTS (SELECT *
		   FROM   sysobjects
		   WHERE  id = object_id('regSurveys')
				  AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  BEGIN
	  DROP PROCEDURE regSurveys
  END

GO

IF EXISTS (SELECT *
		   FROM   sysobjects
		   WHERE  id = object_id('adSurveys')
				  AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  BEGIN
	  DROP PROCEDURE adSurveys
  END

GO

IF EXISTS (SELECT *
		   FROM   sysobjects
		   WHERE  id = object_id('getSurveys')
				  AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  BEGIN
	  DROP PROCEDURE getSurveys
  END

GO

CREATE PROCEDURE [dbo].[getSurveys] (@user_id   BIGINT,
							@user_date DATETIME,
							@comp_id   BIGINT,
							@clsid       VARCHAR (255),
							@ip BIGINT)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @timezone INT
	SET @timezone = DATEDIFF(Minute,GETDATE(),GETUTCDATE())

	IF(@user_id > 1)
	BEGIN
		WITH ssent
		   AS (SELECT su.id,
					  su.closed,
					  su.create_date,
					  su.schedule,
					  su.name,
					  su.from_date,
					  su.to_date
			   FROM   surveys_sent_group ssg WITH (READPAST)
					  JOIN users_groups ug WITH (READPAST)
						ON ug.group_id = ssg.group_id
					  JOIN surveys_main su WITH (READPAST)
						ON su.id = ssg.survey_id
						   AND su.type = 'R'
			   WHERE  ug.user_id = @user_id
			   UNION ALL
			   SELECT su.id,
					  su.closed,
					  su.create_date,
					  su.schedule,
					  su.name,
					  su.from_date,
					  su.to_date
			   FROM   surveys_sent_group ssg WITH (READPAST)
					  JOIN computers_groups cg WITH (READPAST)
						ON cg.group_id = ssg.group_id
					  JOIN surveys_main su WITH (READPAST)
						ON su.id = ssg.survey_id
						   AND su.type = 'R'
			   WHERE  cg.comp_id = @comp_id
			   UNION ALL
			   SELECT su.id,
					  su.closed,
					  su.create_date,
					  su.schedule,
					  su.name,
					  su.from_date,
					  su.to_date
			   FROM   surveys_sent ss WITH (READPAST)
					  JOIN surveys_main su WITH (READPAST)
						ON su.id = ss.survey_id
						   AND su.type = 'R'
			   WHERE  ss.user_id = @user_id
			   UNION ALL
			   SELECT su.id,
					  su.closed,
					  su.create_date,
					  su.schedule,
					  su.name,
					  su.from_date,
					  su.to_date
			   FROM   surveys_sent_iprange srg WITH (READPAST)
					  JOIN surveys_main su WITH (READPAST)
						ON su.id = srg.survey_id
						   AND su.type = 'I'
			   WHERE  srg.ip_from <= @ip
					  AND srg.ip_to >= @ip
			   UNION ALL
			   SELECT su.id,
					  su.closed,
					  su.create_date,
					  su.schedule,
					  su.name,
					  su.from_date,
					  su.to_date
			   FROM   surveys_sent_comp c WITH (READPAST)
					  JOIN surveys_main su WITH (READPAST)
						ON su.id = c.survey_id
						   AND su.type = 'R'
			   WHERE  c.comp_id = @comp_id)
		SELECT DISTINCT res.id,
					  res.closed,
					  DATEADD(Minute, @timezone, res.create_date) AS create_date,
					  res.schedule,
					  res.name,
					  res.from_date,
					  res.to_date
		FROM   (SELECT ssent.id,
					 ssent.closed,
					 ssent.create_date,
					 ssent.schedule,
					 ssent.name,
					 ssent.from_date,
					 ssent.to_date
			  FROM   ssent
					 LEFT JOIN surveys_received_stat r WITH (READPAST)
					   ON r.survey_id = ssent.id
						  AND r.user_id = @user_id
						  AND ( r.clsid = @clsid
								 OR r.clsid = ''
								 OR @clsid = '' )
			  WHERE  r.survey_id IS NULL
					 AND ssent.create_date > @user_date
			  UNION ALL
			  SELECT su.id,
					 su.closed,
					 su.create_date,
					 su.schedule,
					 su.name,
					 su.from_date,
					 su.to_date
			  FROM   surveys_main su WITH (READPAST)
					 LEFT JOIN surveys_received_stat r WITH (READPAST)
					   ON r.survey_id = su.id
						  AND r.user_id = @user_id
						  AND ( r.clsid = @clsid
								 OR r.clsid = ''
								 OR @clsid = '' )
			  WHERE  r.survey_id IS NULL
					 AND su.type = 'B'
					 AND su.create_date > @user_date) res
		ORDER  BY res.id
	END
END
