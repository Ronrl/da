USE [%dbname_hook%]

GO

IF EXISTS (SELECT 1
		   FROM   sysobjects
		   WHERE  id = object_id('isActualDayRecurrence')
				  AND OBJECTPROPERTY(id, N'IsScalarFunction') = 1)
BEGIN
	DROP FUNCTION isActualDayRecurrence
END

GO

CREATE FUNCTION [dbo].[isActualDayRecurrence](
	@pattern [varchar] (1),
	@number_days [int],
	@number_week [int],
	@week_days [int],
	@month_day [int],
	@number_month [int],
	@weekday_place [int],
	@weekday_day [int],
	@month_val [int],
	@dayly_selector [varchar] (50),
	@monthly_selector [varchar] (50),
	@yearly_selector [varchar] (50),
	@end_type [varchar] (50),
	@from_date [DATETIME],
	@to_date [DATETIME],
	@check_date [DATETIME])
RETURNS BIT
AS
BEGIN
	DECLARE @tmp_first DATETIME
	DECLARE @tmp_next_month DATETIME

	IF @end_type = 'no_date' OR
			(@end_type = 'end_by' AND DATEDIFF(d, @to_date, @check_date) <= 0) OR
			(@end_type = 'end_after')
	BEGIN
		IF @pattern = 'd' --Daily
		BEGIN
			IF @dayly_selector = 'dayly_1'
			BEGIN
				IF DATEDIFF(d, @from_date, @check_date) % @number_days = 0
					RETURN 1
			END
			ELSE --dayly_2
				RETURN 1
		END
		ELSE IF @pattern = 'w' --Weekly
		BEGIN
			-- Sun = 1, Mon = 2, Tue = 4, Wed = 8, Thu = 16, Fri = 32, Sat = 64
			IF DATEDIFF(ww, @from_date, @check_date) % @number_week = 0 AND
				NOT @week_days & POWER(2, CASE WHEN DATEPART(dw, @check_date) - DATEPART(dw, 0) + 1 = 7 THEN 0 ELSE DATEPART(dw, @check_date) - DATEPART(dw, 0) + 1 END) = 0
					RETURN 1
		END
		ELSE IF @pattern = 'm' --Monthly
		BEGIN
			IF @monthly_selector = 'monthly_1'
			BEGIN
				IF DATEDIFF(m, @from_date, @check_date) % @number_month = 0
				BEGIN
					DECLARE @tmp_day_now int
					SET @tmp_day_now = DATEPART(d, @check_date)
					IF @tmp_day_now = @month_day
						RETURN 1
					ELSE
					BEGIN
						DECLARE @tmp_last_day int
						SET @tmp_next_month = DATEADD(m, 1, @check_date)
						SET @tmp_last_day = DATEPART(d, DATEADD(d, -DATEPART(d, @tmp_next_month), @tmp_next_month))
						IF @month_day > @tmp_last_day AND @tmp_day_now = @tmp_last_day -- is last day of month
							RETURN 1
					END
				END
			END
			ELSE --monthly_2
			BEGIN
				IF DATEDIFF(m, @from_date, @check_date) % @number_month = 0
				BEGIN
					IF CASE WHEN DATEPART(dw, @check_date) - DATEPART(dw, 0) + 1 = 7 THEN 0 ELSE DATEPART(dw, @check_date) - DATEPART(dw, 0) + 1 END = @weekday_day
					BEGIN
						SET @tmp_first = DATEADD(d, -DATEPART(d, @check_date)+1, @check_date)
						IF @weekday_place = 0
						BEGIN
							SET @tmp_next_month = DATEADD(m, 1, @check_date)
							IF DATEDIFF(ww, @tmp_first, @check_date) = DATEDIFF(ww, @tmp_first, DATEADD(d, -DATEPART(d, @tmp_next_month), @tmp_next_month)) --is last week
								RETURN 1
						END
						ELSE IF DATEDIFF(ww, @tmp_first, @check_date)+1 = @weekday_place
							RETURN 1
					END
				END
			END
		END
		ELSE IF @pattern = 'y' --Yearly
		BEGIN
			IF @yearly_selector = 'yearly_1'
			BEGIN
				IF DATEPART(d, @check_date) = @month_day AND DATEPART(m, @check_date) = @month_val
					RETURN 1
			END
			ELSE --yearly_2
			BEGIN
				IF DATEPART(m, @check_date) = @month_val
				BEGIN
					IF CASE WHEN DATEPART(dw, @check_date) - DATEPART(dw, 0) + 1 = 7 THEN 0 ELSE DATEPART(dw, @check_date) - DATEPART(dw, 0) + 1 END = @weekday_day
					BEGIN
						SET @tmp_first = DATEADD(d, -DATEPART(d, @check_date)+1, @check_date)
						IF @weekday_place = 0
						BEGIN
							SET @tmp_next_month = DATEADD(m, 1, @check_date)
							IF DATEDIFF(ww, @tmp_first, @check_date) = DATEDIFF(ww, @tmp_first, DATEADD(d, -DATEPART(d, @tmp_next_month), @tmp_next_month)) --is last week
								RETURN 1
						END
						ELSE IF DATEDIFF(ww, @tmp_first, @check_date)+1 = @weekday_place
							RETURN 1
					END
				END
			END
		END
	END
	RETURN 0
END

GO

IF EXISTS (SELECT *
		   FROM   sysobjects
		   WHERE  id = object_id('isActualRecurrence')
				  AND OBJECTPROPERTY(id, N'IsScalarFunction') = 1)
  BEGIN
	  DROP FUNCTION isActualRecurrence
  END

GO

CREATE FUNCTION [dbo].[isActualRecurrence](
	@pattern [varchar] (1),
	@number_days [int],
	@number_week [int],
	@week_days [int],
	@month_day [int],
	@number_month [int],
	@weekday_place [int],
	@weekday_day [int],
	@month_val [int],
	@dayly_selector [varchar] (50),
	@monthly_selector [varchar] (50),
	@yearly_selector [varchar] (50),
	@end_type [varchar] (50),
	@occurrences [int],
	@from_date [DATETIME],
	@to_date [DATETIME],
	@occur_cnt [int],
	@max_recieved_date [DATETIME])
RETURNS BIT
AS
BEGIN
	DECLARE @now DATETIME
	DECLARE @tmp_first DATETIME
	DECLARE @tmp_next_month DATETIME

	SET @now = GETDATE()
	
	
	IF @pattern = 'o'
	BEGIN
		IF (@now >= @from_date) 
			AND (@now <= @to_date)
			AND (@max_recieved_date IS NULL OR @max_recieved_date<@from_date) 
			RETURN 1
		RETURN 0
	END
	
	IF @pattern = 'c' --Countdown
	BEGIN
		SET @tmp_first = DATEADD(n,-@number_days,@from_date)
		SET @tmp_next_month = DATEADD(n,-@number_days+@number_week,@from_date)
		
		IF @tmp_first <= @now -- @number_days as minutes before
		AND @tmp_next_month > @now -- @number_weeks as minutes to expire
		AND (@max_recieved_date IS NULL OR @tmp_first > @max_recieved_date)
			RETURN 1
		RETURN 0
	END
	ELSE IF NOT @max_recieved_date IS NULL AND DATEDIFF(d, @max_recieved_date, @now) = 0 -- already recieved
		RETURN 0
	
	--check alert time (compare only times)
	IF DATEDIFF(s, DATEADD(d,DATEDIFF(d, @from_date, @now), @from_date), @now) >= 0 AND DATEDIFF(s, DATEADD(d,DATEDIFF(d, @to_date, @now), @to_date), @now) <= 0
	BEGIN
		IF @end_type = 'no_date' OR
			(@end_type = 'end_by' AND DATEDIFF(d, @to_date, @now) <= 0) OR
			(@end_type = 'end_after' AND @occurrences > @occur_cnt)
		BEGIN
			RETURN dbo.isActualDayRecurrence(@pattern, @number_days, @number_week, @week_days, @month_day, @number_month, @weekday_place, @weekday_day, @month_val, @dayly_selector, @monthly_selector, @yearly_selector, @end_type, @from_date, @to_date, @now)
		END
	END
	RETURN 0
END

GO

IF EXISTS (SELECT *
		   FROM   sysobjects
		   WHERE  id = object_id('regAlerts')
				  AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  BEGIN
	  DROP PROCEDURE regAlerts
  END
 
GO

IF EXISTS (SELECT *
		   FROM   sysobjects
		   WHERE  id = object_id('edirAlerts')
				  AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  BEGIN
	  DROP PROCEDURE edirAlerts
  END

GO

IF EXISTS (SELECT *
		   FROM   sysobjects
		   WHERE  id = object_id('adAlerts')
				  AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  BEGIN
	  DROP PROCEDURE adAlerts
  END

GO

IF EXISTS (SELECT *
		   FROM   sysobjects
		   WHERE  id = object_id('getAlerts')
				  AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  BEGIN
	  DROP PROCEDURE getAlerts
  END

GO

CREATE PROCEDURE  [dbo].[getAlerts] (@user_id  BIGINT,
							@user_date DATETIME,
							@comp_id BIGINT,
							@clsid       VARCHAR (255),
							@ip          BIGINT)
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @cur_date DATETIME
	DECLARE @timezone INT
	SET @cur_date = GETDATE()
	SET @timezone = DATEDIFF(Minute,@cur_date,GETUTCDATE())
	
	SELECT @user_date = reg_date FROM users WHERE id = @user_id 
	
	;WITH asent AS
		(
			SELECT a.id,
				a.aknown,
				a.autoclose,
				a.create_date,
				a.sent_date,
				a.recurrence,
				a.ticker,
				a.urgent,
				a.schedule,
				a.schedule_type,
				a.fullscreen,
				a.alert_width,
				a.alert_height,
				a.title,
				a.from_date,
				a.to_date,
				a.class,
				a.desktop
			FROM alerts a
			INNER JOIN
			(
				SELECT sg.alert_id
				FROM alerts_sent_group sg WITH (READPAST)
				INNER JOIN users_groups ug WITH (READPAST)
				ON ug.group_id = sg.group_id
				WHERE ug.user_id = @user_id
			UNION
				SELECT sg.alert_id
				FROM alerts_sent_group sg WITH (READPAST)
				INNER JOIN computers_groups cg WITH (READPAST)
				ON cg.group_id = sg.group_id
				WHERE cg.comp_id = @comp_id
			UNION
				SELECT als.alert_id
				FROM alerts_sent als WITH (READPAST)
				WHERE  als.user_id = @user_id
			UNION
				SELECT c.alert_id
				FROM alerts_sent_comp c WITH (READPAST)
				WHERE c.comp_id = @comp_id
			UNION
				SELECT rg.alert_id
				FROM alerts_sent_iprange rg WITH (READPAST)
				WHERE rg.ip_from <= @ip
					AND rg.ip_to >= @ip
			UNION
				SELECT aou.alert_id
				FROM alerts_sent_ou aou WITH (READPAST)
				JOIN OU_User u WITH (READPAST)
				ON aou.ou_id = u.Id_OU
				WHERE u.Id_user = @user_id
			UNION
				SELECT aou.alert_id
				FROM alerts_sent_ou aou WITH (READPAST)
				JOIN OU_comp c WITH (READPAST)
				ON aou.ou_id = c.Id_OU
				WHERE c.Id_comp = @comp_id
			) s
			ON a.id = s.alert_id
			WHERE desktop = '1'
		UNION ALL
			SELECT a.id,
				a.aknown,
				a.autoclose,
				a.create_date,
				a.sent_date,
				a.recurrence,
				a.ticker,
				a.urgent,
				a.schedule,
				a.schedule_type,
				a.fullscreen,
				a.alert_width,
				a.alert_height,
				a.title,
				a.from_date,
				a.to_date,
				a.class,
				a.desktop
			FROM alerts a WITH (READPAST)
			WHERE a.type2 = 'B' AND desktop = '1'
		)
		SELECT res.id,
			res.aknown,
			res.autoclose,
			DATEADD(Minute, @timezone, res.create_date) AS create_date,
			DATEADD(Minute, @timezone, res.sent_date) AS sent_date,
			res.recurrence,
			res.ticker,
			res.urgent,
			res.schedule,
			res.schedule_type,
			res.fullscreen,
			res.alert_width,
			res.alert_height,
			res.title,
			res.from_date,
			res.to_date,
			res.rec_pattern,
			res.class,
			res.desktop,
			'' AS caption_file_name
		FROM
		(
			SELECT a.id,
				a.aknown,
				a.autoclose,
				a.create_date,
				a.sent_date,
				a.recurrence,
				a.ticker,
				a.urgent,
				a.schedule,
				a.schedule_type,
				a.fullscreen,
				a.alert_width,
				a.alert_height,
				a.title,
				a.from_date,
				a.to_date,
				'' as rec_pattern,
				a.class,
				a.desktop
			FROM asent a
			LEFT JOIN alerts_received r WITH (READPAST)
			ON r.alert_id = a.id AND
				r.user_id = @user_id AND
				(r.clsid = @clsid OR r.clsid = '' OR @clsid = '')
			WHERE r.alert_id IS NULL AND
				NOT a.recurrence = '1' AND
				a.sent_date > @user_date AND
				(
					NOT schedule = '1'
					OR
					(
						a.schedule_type = '1' AND
						a.from_date <= @cur_date AND
						(to_date = 0 OR to_date >= @cur_date)
					)
				)
		UNION ALL
			SELECT a.id,
				a.aknown,
				a.autoclose,
				a.create_date,
				a.sent_date,
				a.recurrence,
				a.ticker,
				a.urgent,
				a.schedule,
				a.schedule_type,
				a.fullscreen,
				a.alert_width,
				a.alert_height,
				a.title,
				a.from_date,
				a.to_date,
				r.pattern as rec_pattern,
				a.class,
				a.desktop
			FROM alerts a
			LEFT JOIN recurrence r WITH (READPAST)
			ON a.id = r.alert_id
			LEFT JOIN alerts_received s WITH (READPAST)
			ON s.alert_id = a.id AND s.user_id = @user_id AND
				(s.clsid = @clsid OR s.clsid = '' OR @clsid = '')
			WHERE  a.recurrence = '1' AND a.schedule_type = '1'
				AND a.sent_date > @user_date
			GROUP BY
				a.id,
				a.aknown,
				a.autoclose,
				a.create_date,
				a.sent_date,
				a.recurrence,
				a.ticker,
				a.urgent,
				a.schedule,
				a.schedule_type,
				a.fullscreen,
				a.alert_width,
				a.alert_height,
				a.title,
				a.from_date,
				a.to_date,
				a.class,
				a.desktop,
				r.pattern,
				r.number_days,
				r.number_week,
				r.week_days,
				r.month_day,
				r.number_month,
				r.weekday_place,
				r.weekday_day,
				r.month_val,
				r.dayly_selector,
				r.monthly_selector,
				r.yearly_selector,
				r.end_type,
				r.occurences
			HAVING dbo.isActualRecurrence(
				r.pattern,
				r.number_days,
				r.number_week,
				r.week_days,
				r.month_day,
				r.number_month,
				r.weekday_place,
				r.weekday_day,
				r.month_val,
				r.dayly_selector,
				r.monthly_selector,
				r.yearly_selector,
				r.end_type,
				r.occurences,
				a.from_date,
				a.to_date,
				COUNT(s.alert_id),
				MAX(s.date)
			) = 1
		) res
		ORDER BY res.urgent DESC,
				res.id
END
