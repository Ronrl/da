USE [%dbname_hook%]

GO

IF NOT EXISTS (SELECT *
		   FROM   sysobjects
		   WHERE  id = object_id('getAlertsFromCache')
				  AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	EXEC(N'USE [%dbname_both%]
		CREATE PROCEDURE [getAlertsFromCache] (
								@user_id BIGINT,
								@user_date DATETIME,
								@comp_id BIGINT,
								@clsid   VARCHAR (255),
								@ip      BIGINT)
	AS
	BEGIN
		EXEC dbo.getAlerts @user_id, @user_date, @comp_id, @clsid, @ip
	END')
END

GO

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE column_name = 'occurrence' AND table_name = 'alerts_received' )
BEGIN
	ALTER TABLE [dbo].[alerts_received] ADD [occurrence] [bigint]
	EXEC(N'USE [%dbname_both%]
		UPDATE alerts_received
		SET occurrence = r.cnt
		FROM
		(
			SELECT alert_id, user_id, clsid, COUNT(1) AS cnt
			FROM alerts_received
		    GROUP BY alert_id, user_id, clsid
		) r
		WHERE alerts_received.alert_id = r.alert_id
			AND alerts_received.user_id = r.user_id
			AND alerts_received.clsid = r.clsid')
END