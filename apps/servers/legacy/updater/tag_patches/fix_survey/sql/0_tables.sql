USE [%dbname_hook%]

GO

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='class'
		AND table_name = 'alerts' )
BEGIN
	ALTER TABLE [alerts] ADD [class] [bigint] NOT NULL DEFAULT 1
END

GO

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='class'
		AND table_name = 'archive_alerts' )
BEGIN
	ALTER TABLE [archive_alerts] ADD [class] [bigint] NOT NULL DEFAULT 1
END

GO

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='date'
		AND table_name = 'alerts_received' )
BEGIN
	ALTER TABLE [alerts_received] ADD [date] [datetime]
END

GO

IF EXISTS ( SELECT TOP 1 1 FROM [alerts_received] WHERE [date] IS NULL )
BEGIN
	UPDATE [alerts_received] SET [date] = [alerts_received_stat].[date]
	FROM [alerts_received_stat]
	WHERE [alerts_received_stat].[id] = [alerts_received].[id]
		AND [alerts_received].[date] IS NULL
END

GO

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='caption_id'
		AND table_name = 'alerts' )
BEGIN
	ALTER TABLE [alerts] ADD [caption_id] [uniqueidentifier] NULL
END

GO

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='caption_id'
		AND table_name = 'archive_alerts' )
BEGIN
	ALTER TABLE [archive_alerts] ADD [caption_id] [uniqueidentifier] NULL
END

GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[alert_captions]'))
	CREATE TABLE [dbo].[alert_captions](
	[id] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
	[name] [nvarchar](max) NULL,
	[file_name] [nvarchar](max) NULL,
	CONSTRAINT [PK_alert_captions] PRIMARY KEY CLUSTERED ([id] ASC)
) ON [PRIMARY]

GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_alert_captions_id]') AND type = 'D')
	ALTER TABLE [dbo].[alert_captions] ADD  CONSTRAINT [DF_alert_captions_id]  DEFAULT (newid()) FOR [id]
	
GO

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='client_version'
		AND table_name = 'users' )
BEGIN
	ALTER TABLE [users] ADD [client_version] [varchar] (50)
END