
<TOOLBAR>
<% db_backup_no_redirect = 1 %>
<!-- #include file="admin/config.inc" -->
<!-- #include file="admin/db_conn.asp" -->
<!-- #include file="admin/libs/templateObj.asp" -->
<!-- #include file="admin/md5.asp" -->
<%

templateGuid = "23d8575a-9eb8-41ee-9f0a-e3a11dce3fc6"

uname = Replace(Request("uname"),"'","''")
cname = Replace(Request("computer"),"'","''")
desk_id = Replace(Request("deskbar_id"),"'","''")
nextrequest = Request("nextrequest")

if ConfEnableMultiAlert = 0 then
	desk_id = ""
end if

Dim XML
user_id = 0
standby = Request("standby")
disable = Request("disable")
clientVersion = Request("client_version")

if disable <> "1" then
	disable = "0"
end if

if standby <> "1" then
	standby = "0"
end if

toolbar=Request("toolbar")

strUserDeskbarId = Replace(Request("deskbar_id"),"'","''")
intUserDeskbarId = 0

if toolbar <> 1 then toolbar = 0 end if

if Request("scheduled") = "" then
	g_scheduled_not_set = true
	g_scheduled = -1
else
	g_scheduled_not_set = false
	g_scheduled = Request("scheduled")
end if

if Request("ticker") = "" then
	g_ticker = -1
else
	g_ticker = Request("ticker")
end if
totalAlertCount = 0

if AD = 3 then
	if EDIR=1 then
		if ConfDisableContextSupport <> 1 then
			context=Request("edir")
			context_id=-1
			if len(context) > 0 then
				Set RS3 = Conn.Execute("SELECT c.id, c.name FROM users u INNER JOIN edir_context c ON u.context_id=c.id WHERE u.name=N'"& uname&"' and u.role='U'")
				do while not RS3.EOF
					strContext=RS3("name")
					strContext=replace(strContext,",ou=",".")
					strContext=replace(strContext,",o=",".")
					strContext=replace(strContext,",dc=",".")
					strContext=replace(strContext,",c=",".")

					strContext=replace(strContext,"ou=","")
					strContext=replace(strContext,"o=","")
					strContext=replace(strContext,"dc=","")
					strContext=replace(strContext,"c=","")
					if LCase(strContext) = LCase(context) then
						context_id=RS3("id")
						exit do
					end if
					RS3.MoveNext
				loop
			elseif ConfEnableADEDCheck = 1 and ConfEnableRegAndADEDMode <> 1 then
				'send alert that client is not in EDIR mode and server is in EDIR mode
				user_id=-1
				setXML
				XML.addCustomAlert false, alerts_folder & "not_edir.asp", 0, 0, 0, 0, "", "", 0, 0, 0, 0, false
			end if
		else
			context_id = -2
		end if
	else
		domain = replace(Request("fulldomain"),"'","''")
		compdomain = replace(Request("compdomain"),"'","''")
		if compdomain = "" then
			compdomain = domain
		end if
		if domain = "" and ConfEnableADEDCheck = 1 and ConfEnableRegAndADEDMode <> 1 then
			'send alert that client is not in AD mode and server is in AD mode
			user_id = -1
			setXML
			XML.addCustomAlert false, alerts_folder & "not_ad.asp", 0, 0, 0, 0, "", "", 0, 0, 0, 0, false
		end if
	end if
end if

'alerts
if Request("cnt") = "cont" then
	cnt = 1
else
	cnt=clng(Request("cnt"))
end if
index = 0

Dim ip : ip = Request.ServerVariables("HTTP_X_FORWARDED_FOR")
if (ip = "") then
	ip = Request.ServerVariables("REMOTE_ADDR")
end if
Dim longip : longip = -1
if ip <> "" then
	longip = 0
	Dim iprange : iprange = Split(ip, ".")
	if UBound(iprange) = 3 then
		For j = 0 to 3
			if isNumeric(iprange(j)) then
				if iprange(j) < 0 or iprange(j) > 255 then
					longip = -1
					exit for
				end if
				longip = longip + iprange(j)*(256^(3-j))
			else
				longip = -1
				exit for
			end if
		Next
	end if
end if

'--- ALERTS ---
if Request("survey") = "" or Request("survey") = "0" then
	alertsSQL = " SET NOCOUNT ON" & vbCrLf &_
				" DECLARE @user_id BIGINT" & vbCrLf &_
				" DECLARE @comp_id BIGINT" & vbCrLf &_
				" DECLARE @context_id BIGINT" & vbCrLf &_
				" DECLARE @comp_domain NVARCHAR (255)" & vbCrLf &_
				" DECLARE @cur_date DATETIME" & vbCrLf &_
				" DECLARE @user_date DATETIME" & vbCrLf &_
				" DECLARE @user_name NVARCHAR (255)" & vbCrLf &_
				" DECLARE @user_domain NVARCHAR (255)" & vbCrLf &_
				" DECLARE @comp_name NVARCHAR (255)" & vbCrLf &_
				" DECLARE @next_request VARCHAR (255)" & vbCrLf &_
				" DECLARE @clsid VARCHAR (255)" & vbCrLf &_
				" DECLARE @ip BIGINT" & vbCrLf &_
				" DECLARE @client_version VARCHAR (50)" & vbCrLf &_
				" SET @cur_date = GETDATE()" & vbCrLf &_
				" SET @context_id = '"& Replace(context_id,"'","''") &"'"& vbCrLf &_
				" SET @user_domain = '"& domain &"'"& vbCrLf &_
				" SET @comp_domain = '"& compdomain &"'"& vbCrLf &_
				" SET @user_name = '"& Replace(uname,"'","''") &"'"& vbCrLf &_
				" SET @comp_name = '"& Replace(cname,"'","''") &"'"& vbCrLf &_
				" SET @next_request = '"& Replace(nextrequest,"'","''") &"'"& vbCrLf &_
				" SET @clsid = '"& Replace(desk_id,"'","''") &"'"& vbCrLf &_
				" SET @ip = "& longip & vbCrLf &_
				" " & vbCrLf &_
				" IF(@context_id > 0)" & vbCrLf &_
				" 	SELECT @user_id = u.id, @user_date = u.reg_date" & vbCrLf &_
				" 	FROM   users u WITH (READPAST)" & vbCrLf &_
				" 	WHERE u.name = @user_name AND role = 'U'" & vbCrLf &_
				" 		 AND u.context_id = @context_id" & vbCrLf &_
				" ELSE IF(@user_domain IS NULL OR @user_domain = N'')" & vbCrLf &_
				" 	SELECT @user_id = u.id, @user_date = u.reg_date" & vbCrLf &_
				" 	FROM   users u WITH (READPAST)" & vbCrLf &_
				" 		 LEFT JOIN domains d WITH (READPAST)" & vbCrLf &_
				" 		   ON d.id = u.domain_id" & vbCrLf &_
				" 	WHERE  (d.name = N'' OR u.domain_id IS NULL)" & vbCrLf &_
				" 		 AND u.name = @user_name AND role = 'U'" & vbCrLf &_
				" ELSE" & vbCrLf &_
				" 	SELECT @user_id = u.id, @user_date = u.reg_date" & vbCrLf &_
				" 	FROM   users u WITH (READPAST)" & vbCrLf &_
				" 		 JOIN domains d WITH (READPAST)" & vbCrLf &_
				" 		   ON d.id = u.domain_id" & vbCrLf &_
				" 	WHERE  d.name = @user_domain" & vbCrLf &_
				" 		 AND u.name = @user_name AND role = 'U'" & vbCrLf &_
				" " & vbCrLf &_
				" IF(@user_id > 1)" & vbCrLf &_
				" 	UPDATE users" & vbCrLf &_
				" 	SET  last_request = @cur_date," & vbCrLf &_
				" 		 next_request = @next_request," & vbCrLf &_
				" 		 standby = " & standby & "," & vbCrLf &_
				" 		 disabled = " & disable & "," & vbCrLf &_
				"		 client_version = '"& Replace(clientVersion,"'","''") &"'" & vbCrLf &_
				" 	WHERE  id = @user_id" & vbCrLf &_
				" " & vbCrLf &_
				" SELECT @user_id as user_id" & vbCrLf &_
				" " & vbCrLf &_
				" IF(@user_id > 1)" & vbCrLf &_
				" BEGIN" & vbCrLf &_
				" 	IF(@comp_domain IS NULL OR @comp_domain = N'')" & vbCrLf &_
				" 		SELECT @comp_id = comp.id" & vbCrLf &_
				" 		FROM   computers comp WITH (READPAST)" & vbCrLf &_
				" 		INNER JOIN domains d WITH (READPAST)" & vbCrLf &_
				" 			   ON d.id = comp.domain_id" & vbCrLf &_
				" 		WHERE  (d.name = N'' OR comp.domain_id IS NULL)" & vbCrLf &_
				" 			 AND comp.name = @comp_name;" & vbCrLf &_
				" 	ELSE" & vbCrLf &_
				" 		SELECT @comp_id = comp.id" & vbCrLf &_
				" 		FROM computers comp WITH (READPAST)" & vbCrLf &_
				" 		INNER JOIN domains d WITH (READPAST)" & vbCrLf &_
				" 		ON d.id = comp.domain_id" & vbCrLf &_
				" 		WHERE d.name = @comp_domain" & vbCrLf &_
				" 			AND comp.name = @comp_name;" & vbCrLf &_
				" " & vbCrLf &_
				" 	IF(@comp_id > 1)" & vbCrLf &_
				" 		UPDATE computers" & vbCrLf &_
				" 		SET last_request = @cur_date," & vbCrLf &_
				" 			 next_request = @next_request" & vbCrLf &_
				" 		WHERE  id = @comp_id;" & vbCrLf &_
				" " & vbCrLf &_
				" 	EXEC getAlertsFromCache @user_id, @user_date, @comp_id, @clsid, @ip" & vbCrLf &_
				" END" & vbCrLf &_
				" ELSE" & vbCrLf &_
				" BEGIN" & vbCrLf &_
				" 	SELECT -1 as id," & vbCrLf &_
				" 			'0' as aknown," & vbCrLf &_
				" 			'0' as autoclose," & vbCrLf &_
				" 			GETUTCDATE() as create_date," & vbCrLf &_
				" 			GETUTCDATE() as sent_date," & vbCrLf &_
				" 			'0' as recurrence," & vbCrLf &_
				" 			'0' as ticker," & vbCrLf &_
				" 			'0' as urgent," & vbCrLf &_
				" 			'0' as schedule," & vbCrLf &_
				" 			'0'  as schedule_type," & vbCrLf &_
				" 			'2'  as fullscreen," & vbCrLf &_
				" 			'500'  as alert_width," & vbCrLf &_
				" 			'400'  as alert_height," & vbCrLf &_
				" 			'Registration'  as title," & vbCrLf &_
				" 			''  as from_date," & vbCrLf &_
				" 			''  as to_date," & vbCrLf &_
				"			'1' as class" & vbCrLf &_ 
				" END "
	set RSalerts = Conn.Execute(alertsSQL)
	
	user_id=-1
	if Not RSalerts.EOF then
		if VarType(RSalerts("user_id")) > 1 then
			user_id = RSalerts("user_id")
		end if
		Set RSalerts = RSalerts.NextRecordset()
	end if

	if (not RSalerts is Nothing) then
	
		if Not RSalerts.EOF then
			setXML
		end if

		wallpapersHash=""
		screensaversHash=""
		do while not RSalerts.EOF
			recurrence=RSalerts("recurrence")
			alert_id=RSalerts("id")
			if ColumnExists(RSalerts, "class") then
				classid = RSalerts("class")&""
			else
				classid = "1"
			end if
			if classid = "2" then
				if screensaversHash<>"" then
					screensaversHash = screensaversHash & ","
				end if
				screensaversHash = screensaversHash & alert_id
			elseif classid = "8" then
				if wallpapersHash<>"" then
					wallpapersHash = wallpapersHash & ","
				end if
				wallpapersHash = wallpapersHash & alert_id
			end if
			if clng(alert_id) = -1 then
				if ConfEnableRegAndADEDMode=1 and g_ticker<>1 then
					'XML.addCustomAlert false, alerts_folder & "after_install.asp?deskbar_id="&strUserDeskbarId, 0, 0, 0, 0, "Registration", "center", "", "", 0, 0, false
				end if
				Exit Do
			end if
			schedule = RSalerts("schedule")
			'for not stopped schedule alerts (checking schedule_type)
			if ((recurrence="1" or schedule="1") and rsalerts("schedule_type")="1" and (g_scheduled=1 or g_scheduled_not_set)) or (recurrence<>"1" and schedule<>"1" and (g_scheduled=0 or g_scheduled_not_set)) then
				if(urgent<>"1") then totalAlertCount=totalAlertCount+1 end if
				urgent = RSalerts("urgent")
				ticker = RSalerts("ticker")
				if ColumnExists(RSalerts, "desktop") then
					desktop = RSalerts("desktop")
				else
					desktop = "1"
				end if
				if (g_ticker=-1 or ticker=g_ticker) and ((urgent="1") or ((disable <> "1") and (index < cnt))) then
					aknown=RSalerts("aknown")
					autoclose=0
					if ColumnExists(RSalerts, "autoclose") then
						if not IsNull(RSalerts("autoclose")) then
							autoclose=Clng(RSalerts("autoclose"))
						end if
					end if
					create_date=RSalerts("sent_date")
					create_timestamp=GetUnixDate(create_date)
					alert_title=RSalerts("title")
					
					LoadTemplateFromString "title"&templateGuid, alert_title
					
					SetVar "time_till", ""
					if ColumnExists(RSalerts, "rec_pattern") then
						if RSalerts("rec_pattern") = "c" then
							time_till = DateDiff("n",now_db,RSalerts("from_date"))
							SetVar "time_till", time_till& " minutes till "
						end if
					end if
					
					Parse "title"&templateGuid, False
					alert_title = PrintVar("title"&templateGuid)
					
					alert_width = RSalerts("alert_width")
					alert_height = RSalerts("alert_height")
					
					position = ""
					if ticker = 1 then
						alert_width = ""
						alert_height = ""
						position = ""
					elseif ColumnExists(RSalerts, "fullscreen") then
						if RSalerts("fullscreen") = 1 then
							position = "fullscreen"
						end if
					end if
					
					innerXML = ""
					if ColumnExists(RSalerts, "caption_file_name") then
						if RSalerts("caption_file_name") <> "" then
							innerXML = "<WINDOW captionhref="""+alerts_folder+"admin/preview/skin/version/"+RSalerts("caption_file_name")+""" />"
						end if
					end if
					if classid <> "2" and classid <> "4" and classid <> "8" and index < cnt and (desktop = "" or desktop = "1") then
						if jsCheckProperty(XML, "addAlert3") then
							added = XML.addAlert3(urgent, alert_id, aknown, autoclose, create_timestamp, alert_title, position, "", alert_width, alert_height, ticker, schedule, recurrence, true, clng(classId), innerXML)
						elseif jsCheckProperty(XML, "addAlert2") then
							added = XML.addAlert2(urgent, alert_id, aknown, autoclose, create_timestamp, alert_title, position, "", alert_width, alert_height, ticker, schedule, recurrence, true, innerXML)
						else
							XML.addAlert urgent, alert_id, aknown, autoclose, create_timestamp, alert_title, position, alert_width, alert_height, ticker, schedule, true
							added = true
						end if
						if added and clng(user_id) > 0 then
							Set RSreceived = Conn.Execute("SELECT COUNT(1) as cnt FROM alerts_received WHERE user_id = " & CStr(user_id) & " AND clsid = '" & desk_id & "' AND alert_id = " & alert_id)
							if not RSreceived.EOF then
								occurrence = RSreceived("cnt") + 1
							else
								occurrence = 1
							end if
							Conn.Execute("INSERT INTO alerts_received (user_id, clsid, alert_id, [date], occurrence) VALUES (" & CStr(user_id) & ", '"& desk_id &"', " & alert_id & ", '"&now_db&"', " & occurrence & ")")
							Conn.Execute("INSERT INTO alerts_received_stat (user_id, alert_id, [date]) VALUES (" & CStr(user_id) & ", " & alert_id & ", '"&now_db&"')")
						end if
						index=index+1
					end if
				end if
			end if
			RSalerts.MoveNext
		loop
	end if
end if
'--- ALERT END ---

'--- SURVEYS BEGIN ---
if (Request("survey") = "" or Request("survey") = "1") and (g_ticker=-1 OR g_ticker=0) and (disable <> "1") then
	if(index < cnt) then
	surveySQL = " SET NOCOUNT ON" & vbCrLf &_
				" DECLARE @user_id BIGINT" & vbCrLf &_
				" DECLARE @comp_id BIGINT" & vbCrLf &_
				" DECLARE @context_id BIGINT" & vbCrLf &_
				" DECLARE @comp_domain NVARCHAR (255)" & vbCrLf &_
				" DECLARE @cur_date DATETIME" & vbCrLf &_
				" DECLARE @user_date DATETIME" & vbCrLf &_
				" DECLARE @user_name NVARCHAR (255)" & vbCrLf &_
				" DECLARE @user_domain NVARCHAR (255)" & vbCrLf &_
				" DECLARE @comp_name NVARCHAR (255)" & vbCrLf &_
				" DECLARE @next_request VARCHAR (255)" & vbCrLf &_
				" DECLARE @clsid VARCHAR (255)" & vbCrLf &_
				" DECLARE @ip BIGINT" & vbCrLf &_
				" DECLARE @client_version VARCHAR (50)" & vbCrLf &_
				" SET @cur_date = GETDATE()" & vbCrLf &_
				" SET @context_id = '"& Replace(context_id,"'","''") &"'"& vbCrLf &_
				" SET @user_domain = '"& domain &"'"& vbCrLf &_
				" SET @comp_domain = '"& compdomain &"'"& vbCrLf &_
				" SET @user_name = '"& Replace(uname,"'","''") &"'"& vbCrLf &_
				" SET @comp_name = '"& Replace(cname,"'","''") &"'"& vbCrLf &_
				" SET @next_request = '"& Replace(nextrequest,"'","''") &"'"& vbCrLf &_
				" SET @clsid = '"& Replace(desk_id,"'","''") &"'"& vbCrLf &_
				" SET @ip = "& longip & vbCrLf &_
				" " & vbCrLf &_
				" IF(@context_id > 0)" & vbCrLf &_
				" 	SELECT @user_id = u.id, @user_date = u.reg_date" & vbCrLf &_
				" 	FROM   users u WITH (READPAST)" & vbCrLf &_
				" 	WHERE u.name = @user_name AND role = 'U'" & vbCrLf &_
				" 		 AND u.context_id = @context_id" & vbCrLf &_
				" ELSE IF(@user_domain IS NULL OR @user_domain = N'')" & vbCrLf &_
				" 	SELECT @user_id = u.id, @user_date = u.reg_date" & vbCrLf &_
				" 	FROM   users u WITH (READPAST)" & vbCrLf &_
				" 		 LEFT JOIN domains d WITH (READPAST)" & vbCrLf &_
				" 		   ON d.id = u.domain_id" & vbCrLf &_
				" 	WHERE  (d.name = N'' OR u.domain_id IS NULL)" & vbCrLf &_
				" 		 AND u.name = @user_name AND role = 'U'" & vbCrLf &_
				" ELSE" & vbCrLf &_
				" 	SELECT @user_id = u.id, @user_date = u.reg_date" & vbCrLf &_
				" 	FROM   users u WITH (READPAST)" & vbCrLf &_
				" 		 JOIN domains d WITH (READPAST)" & vbCrLf &_
				" 		   ON d.id = u.domain_id" & vbCrLf &_
				" 	WHERE  d.name = @user_domain" & vbCrLf &_
				" 		 AND u.name = @user_name AND role = 'U'" & vbCrLf &_
				" " & vbCrLf &_
				" SELECT @user_id as user_id" & vbCrLf &_
				" " & vbCrLf &_
				" IF(@user_id > 1)" & vbCrLf &_
				" BEGIN" & vbCrLf &_
				" 	IF(@comp_domain IS NULL OR @comp_domain = N'')" & vbCrLf &_
				" 		SELECT @comp_id = comp.id" & vbCrLf &_
				" 		FROM   computers comp WITH (READPAST)" & vbCrLf &_
				" 		INNER JOIN domains d WITH (READPAST)" & vbCrLf &_
				" 			   ON d.id = comp.domain_id" & vbCrLf &_
				" 		WHERE  (d.name = N'' OR comp.domain_id IS NULL)" & vbCrLf &_
				" 			 AND comp.name = @comp_name;" & vbCrLf &_
				" 	ELSE" & vbCrLf &_
				" 		SELECT @comp_id = comp.id" & vbCrLf &_
				" 		FROM computers comp WITH (READPAST)" & vbCrLf &_
				" 		INNER JOIN domains d WITH (READPAST)" & vbCrLf &_
				" 		ON d.id = comp.domain_id" & vbCrLf &_
				" 		WHERE d.name = @comp_domain" & vbCrLf &_
				" 			AND comp.name = @comp_name;" & vbCrLf &_
				" " & vbCrLf &_
				" 	EXEC getSurveys @user_id, @user_date, @comp_id, @clsid, @ip" & vbCrLf &_
				" END"
		Set RSsurveys= Conn.Execute(surveySQL)
		if Not RSsurveys.EOF then
			if VarType(RSsurveys("user_id")) > 1 then
				user_id = RSsurveys("user_id")
			else
				user_id=-1
			end if
			Set RSsurveys = RSsurveys.NextRecordset()
		end if
		
		if (not RSsurveys is Nothing) then
		
			if Not RSsurveys.EOF then
				setXML
			end if
			
			Do While (Not RSsurveys.EOF)
				if RSsurveys("closed") = "N" then
					sur_id=RSsurveys("id")
					create_date=RSsurveys("create_date")
					create_timestamp=GetUnixDate(create_date)
					survey_title=RSsurveys("name")
					schedule=RSsurveys("schedule")

					send_flag=0
					if(schedule=1) then
						if(DateDiff("n", RSsurveys("from_date"), now_db) >= 0 and (DateDiff("n", RSsurveys("to_date"), now_db) <= 0 or Year(RSsurveys("to_date")) = 1900)) Then send_flag=1
					else
						send_flag=1
					end if
					if(send_flag=1) then
						if(XML.addSurvey2(false, sur_id, create_timestamp, survey_title, "", "", "", "", schedule, 0, true, "")) then
							SQL = "INSERT INTO surveys_received_stat (user_id, survey_id, [date], clsid) VALUES (" & CStr(user_id) & ", " & sur_id & ", '"&now_db&"', '"&desk_id&"')"
							Conn.Execute(SQL)
						end if
						index=index+1
					end if
				end if
				RSsurveys.MoveNext
				if(index=cnt) then Exit Do end if
			loop
		end if
	end if
end if
'--- SURVEYS END ---

'end if

if(disable="1") then
	Response.Write "<ALERTCOUNT count="""& totalAlertCount &"""/>"
end if

'end if
if not isEmpty(XML) then
	Response.Write XML.getResult()
end if

if (screensaversHash<>"") then
	Response.Write "<SCREENSAVERS hash="""&md5(screensaversHash)&"""/>"
end if

if (wallpapersHash<>"") then
	Response.Write "<WALLPAPERS hash="""&md5(wallpapersHash)&"""/>"
end if

XML = null
%>
<!-- #include file="admin/db_conn_close.asp" -->
</TOOLBAR>