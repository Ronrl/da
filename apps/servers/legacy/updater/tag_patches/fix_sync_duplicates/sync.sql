
IF EXISTS (SELECT *
		   FROM   sysobjects
		   WHERE  id = object_id('addUser')
				  AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	DROP PROCEDURE addUser
END

GO

CREATE PROCEDURE addUser(@user_hash VARCHAR(128),
						 @user_name NVARCHAR(255),
						 @display_name NVARCHAR(255),
						 @mail NVARCHAR (255),
						 @phone VARCHAR (255),
						 @ou_id BIGINT,
						 @group_id BIGINT,
						 @domain_id INT)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @updated BIT
	SET @updated = 1
	DECLARE @user_id BIGINT
	SELECT @user_id=u.id FROM users u WHERE u.name=@user_name AND u.domain_id=@domain_id
	IF NOT @user_id IS NULL
	BEGIN
		IF NOT @ou_id IS NULL
			DELETE OU_User WHERE Id_user = @user_id AND NOT Id_OU = @ou_id
		ELSE
			DELETE OU_User WHERE Id_user = @user_id
	END
	IF NOT @user_id IS NULL
		UPDATE users SET was_checked=1, mobile_phone=@phone, email=@mail, display_name=@display_name WHERE id=@user_id
	ELSE
	BEGIN
		INSERT INTO users (name, domain_id, role, was_checked, mobile_phone, email, display_name, reg_date) VALUES (@user_name, @domain_id, 'U', 1, @phone, @mail, @display_name, GETDATE())
		SET @user_id=SCOPE_IDENTITY()
		INSERT INTO User_synch (Id_user, UID) VALUES (@user_id, @user_hash)
		SET @updated = 0
	END
	IF NOT @ou_id IS NULL
	BEGIN
		IF NOT EXISTS (SELECT 1 FROM OU_User WHERE Id_user = @user_id)
			INSERT INTO OU_User (Id_user, Id_OU) VALUES (@user_id, @ou_id)
		ELSE
			UPDATE OU_User SET Id_OU = @ou_id WHERE Id_user = @user_id
	END
	IF NOT @group_id IS NULL
		EXEC addUserToGroup @group_id, @user_id
	SELECT @user_id AS id, @updated as updated
END

GO

IF EXISTS (SELECT *
		   FROM   sysobjects
		   WHERE  id = object_id('addComp')
				  AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	DROP PROCEDURE addComp
END

GO

CREATE PROCEDURE addComp(@comp_hash VARCHAR(128),
						 @comp_name NVARCHAR(255),
						 @ou_id BIGINT,
						 @group_id BIGINT,
						 @domain_id INT)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @updated BIT
	SET @updated = 1
	DECLARE @comp_id BIGINT
	SELECT @comp_id=c.id FROM computers c WHERE c.name=@comp_name AND c.domain_id=@domain_id
	IF NOT @comp_id IS NULL
	BEGIN
		IF NOT @ou_id IS NULL
			DELETE OU_comp WHERE Id_comp = @comp_id AND NOT Id_OU = @ou_id
		ELSE
			DELETE OU_comp WHERE Id_comp = @comp_id
	END
	IF NOT @comp_id IS NULL
		UPDATE computers SET was_checked=1 WHERE id=@comp_id
	ELSE
	BEGIN
		INSERT INTO computers (name, domain_id, was_checked) VALUES (@comp_name, @domain_id, 1)
		SET @comp_id=SCOPE_IDENTITY()
		INSERT INTO Comp_synch (Id_comp, UID) VALUES (@comp_id, @comp_hash)
		SET @updated = 0
	END
	IF NOT @ou_id IS NULL
	BEGIN
		IF NOT EXISTS (SELECT 1 FROM OU_comp WHERE Id_comp = @comp_id)
			INSERT INTO OU_comp (Id_comp, Id_OU) VALUES (@comp_id, @ou_id)
		ELSE
			UPDATE OU_comp SET Id_OU = @ou_id WHERE Id_comp = @comp_id
	END
	IF NOT @group_id IS NULL
		EXEC addCompToGroup @group_id, @comp_id
	SELECT @comp_id AS id, @updated as updated
END

GO

IF EXISTS (SELECT *
		   FROM   sysobjects
		   WHERE  id = object_id('addGroup')
				  AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	DROP PROCEDURE addGroup
END

GO

CREATE PROCEDURE addGroup(@group_hash VARCHAR(128),
						  @group_name NVARCHAR(255),
						  @ou_id BIGINT,
						  @container_group_id BIGINT,
						  @domain_id INT)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @updated BIT
	SET @updated = 1
	DECLARE @group_id BIGINT
	SELECT @group_id=g.id FROM groups g WHERE g.name=@group_name AND g.domain_id=@domain_id
	IF NOT @group_id IS NULL
	BEGIN
		IF NOT @ou_id IS NULL
			DELETE OU_group WHERE Id_group = @group_id AND NOT Id_OU = @ou_id
		ELSE
			DELETE OU_group WHERE Id_group = @group_id
	END
	IF NOT @group_id IS NULL
		UPDATE groups SET was_checked=1 WHERE id=@group_id
	ELSE
	BEGIN
		INSERT INTO groups (name, domain_id, was_checked) VALUES (@group_name, @domain_id, 1)
		SET @group_id=SCOPE_IDENTITY()
		INSERT INTO Group_synch (Id_group, UID) VALUES (@group_id, @group_hash)
		SET @updated = 0
	END
	IF NOT @ou_id IS NULL
	BEGIN
		IF NOT EXISTS (SELECT 1 FROM OU_group WHERE Id_group = @group_id)
			INSERT INTO OU_group (Id_group, Id_OU) VALUES (@group_id, @ou_id)
		ELSE
			UPDATE OU_group SET Id_OU = @ou_id WHERE Id_group = @group_id
	END
	IF NOT @container_group_id IS NULL
		EXEC addGroupToGroup @container_group_id, @group_id
	SELECT @group_id AS id, @updated as updated
END

GO

IF EXISTS (SELECT *
		   FROM   sysobjects
		   WHERE  id = object_id('addOU')
				  AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	DROP PROCEDURE addOU
END

GO

CREATE PROCEDURE addOU(@ou_hash VARCHAR(128),
					   @ou_name NVARCHAR(255),
					   @ou_path NVARCHAR(255),
					   @parent_ou_id BIGINT,
					   @domain_id BIGINT,
					   @was_checked BIT)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @updated BIT
	SET @updated = 1
	DECLARE @ou_id BIGINT
	SELECT @ou_id=s.Id_OU FROM OU_synch s INNER JOIN OU o ON o.Id=s.Id_OU AND o.Id_domain = @domain_id WHERE s.UID=@ou_hash
	IF @ou_id IS NULL
	BEGIN
		DELETE OU_synch WHERE UID=@ou_hash
		SELECT @ou_id=o.Id FROM OU o LEFT JOIN OU_synch s ON o.Id=s.Id_OU WHERE o.Name=@ou_name AND o.Id_domain = @domain_id AND s.Id_OU IS NULL
		IF NOT @ou_id IS NULL
		BEGIN
			IF NOT @parent_ou_id IS NULL
			BEGIN
				DELETE OU_hierarchy WHERE Id_child = @ou_id AND NOT Id_parent = @parent_ou_id
				DELETE OU_DOMAIN WHERE Id_ou = @ou_id
			END
			ELSE
				DELETE OU_hierarchy WHERE Id_child = @ou_id AND NOT Id_parent = -1
			INSERT INTO OU_synch (Id_OU, UID) VALUES (@ou_id, @ou_hash)
		END
	END
	IF NOT @ou_id IS NULL
		UPDATE OU SET Was_checked=@was_checked, OU_PATH=@ou_path WHERE id=@ou_id
	ELSE
	BEGIN
		INSERT INTO OU (Name, OU_PATH, Id_domain, Was_checked) VALUES (@ou_name, @ou_path, @domain_id, @was_checked)
		SET @ou_id=SCOPE_IDENTITY()
		INSERT INTO OU_synch (Id_OU, UID) VALUES (@ou_id, @ou_hash)
		SET @updated = 0
	END
	IF @parent_ou_id IS NULL
	BEGIN
		DECLARE @cur_domain_id BIGINT
		SET @cur_domain_id = (SELECT Id_domain FROM OU_DOMAIN WHERE Id_ou = @ou_id)
		IF @cur_domain_id IS NULL
			INSERT INTO OU_DOMAIN (Id_domain, Id_ou) VALUES (@domain_id, @ou_id)
		ELSE IF NOT @cur_domain_id = @domain_id
			UPDATE OU_DOMAIN SET Id_domain = @domain_id WHERE Id_ou = @ou_id
		SET @parent_ou_id = -1
	END
	ELSE
		DELETE OU_DOMAIN WHERE Id_ou = @ou_id
	IF NOT EXISTS (SELECT 1 FROM OU_hierarchy WHERE Id_parent = @parent_ou_id AND Id_child = @ou_id)
		INSERT INTO OU_hierarchy (Id_parent, Id_child) VALUES (@parent_ou_id, @ou_id)
	DELETE OU_hierarchy WHERE NOT Id_parent = @parent_ou_id AND Id_child = @ou_id
	SELECT @ou_id AS id, @updated as updated
END