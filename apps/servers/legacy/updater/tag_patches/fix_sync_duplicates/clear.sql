;with u as (
  select id, 
     row_number() over (partition by name, domain_id order by id desc) as rn
  from users
)
delete u where rn > 1

;with g as (
  select id, 
     row_number() over (partition by name, domain_id order by id desc) as rn
  from groups
)
delete g where rn > 1

;with c as (
  select id, 
     row_number() over (partition by name, domain_id order by id desc) as rn
  from computers
)
delete c where rn > 1