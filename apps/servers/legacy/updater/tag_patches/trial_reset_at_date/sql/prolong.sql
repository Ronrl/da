USE [%dbname_hook%];
SET LANGUAGE British

ALTER TABLE [users]
ALTER COLUMN [last_request] [datetime] NULL

UPDATE [users] SET [reg_date] = '10/01/2017 00:00:00.000', [last_date] = NULL, [last_request] = NULL, [next_request] = NULL, [last_standby_request] = NULL, [next_standby_request] = NULL WHERE [name] = N'admin' AND [role] = 'A'
UPDATE [settings] SET [val]='30' WHERE name= 'supermario'