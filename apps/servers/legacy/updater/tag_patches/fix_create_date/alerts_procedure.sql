
IF EXISTS (SELECT *
		   FROM   sysobjects
		   WHERE  id = object_id('getAlertsFromCache')
				  AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	DROP PROCEDURE [getAlertsFromCache]
END

GO

CREATE PROCEDURE [getAlertsFromCache] (
							@user_id BIGINT,
							@user_date DATETIME,
							@comp_id BIGINT,
							@clsid   VARCHAR (255),
							@ip      BIGINT)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @now DATETIME
	SET @now = GETDATE()
	DECLARE @timezone INT
	SET @timezone = DATEDIFF(Minute,@now,GETUTCDATE())
	SELECT a.id,
			a.aknown,
			a.autoclose,
			DATEADD(Minute, @timezone, a.create_date) AS create_date,
			DATEADD(Minute, @timezone, a.sent_date) AS sent_date,
			a.recurrence,
			a.ticker,
			a.urgent,
			a.schedule,
			a.schedule_type,
			a.fullscreen,
			a.alert_width,
			a.alert_height,
			a.title,
			a.from_date,
			a.to_date,
			a.class,
			a.desktop,
			ac.file_name as caption_file_name,
			rc.pattern as rec_pattern
	FROM
	(
		SELECT DISTINCT alert_id, from_date, recur_id
		FROM
		(
			SELECT alert_id, from_date, recur_id
			FROM alerts_cache_users WITH (READPAST)
			WHERE user_id = @user_id
				AND from_date <= @now AND (to_date IS NULL OR @now <= to_date)
		UNION ALL
			SELECT alert_id, from_date, recur_id
			FROM alerts_cache_comps WITH (READPAST)
			WHERE comp_id = @comp_id
				AND from_date <= @now AND (to_date IS NULL OR @now <= to_date)
		UNION ALL
			SELECT alert_id, from_date, recur_id
			FROM alerts_cache_ips WITH (READPAST)
			WHERE ip_from <= @ip
				AND ip_to >= @ip
				AND from_date <= @now AND (to_date IS NULL OR @now <= to_date)
		) sent
	) s
	INNER JOIN alerts a WITH (READPAST)
		ON a.id = s.alert_id
	LEFT JOIN recurrence rc WITH (READPAST)
		ON rc.id = s.recur_id
	LEFT JOIN alerts_received r WITH (READPAST)
		ON a.id = r.alert_id
			AND r.user_id = @user_id
			AND (r.clsid = @clsid OR r.clsid = '' OR @clsid = '')
			AND (r.date >= s.from_date OR (rc.end_type = 'end_after' AND rc.occurences <= r.occurrence))
	LEFT JOIN alert_captions ac
		ON ac.id = a.caption_id
	WHERE a.sent_date >= @user_date /*for comps and ips*/
		AND r.alert_id IS NULL
	ORDER BY a.urgent DESC, a.id
END
