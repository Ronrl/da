﻿using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Enums;
using System;
using System.ComponentModel;
using System.Timers;

namespace DeskAlerts.ApplicationCore.Models
{
    public class Token : ISynchronizeInvoke
    {
        public Token(int lifeTimeMinutes, ElapsedEventHandler callback)
        {
            var lifetimeMilliseconds = lifeTimeMinutes * 60000;
            var timer = new Timer()
            {
                AutoReset = false,
                Enabled = true,
                Interval = lifetimeMilliseconds,
                SynchronizingObject = this
            };

            timer.Elapsed += callback;
            TokenExpirationTimer = timer;
        }

        public Timer TokenExpirationTimer { get; set; }
        public string UserName { get; set; }
        public string ComputerName { get; set; }
        public string ComputerDomainName { get; set; }
        public string DomainName { get; set; }
        public string Ip { get; set; }
        public Guid Guid { get; set; }
        public string Version { get; set; }
        public string DeskbarId { get; set; }
        public User User { get; set; }
        public string DistinguishedName { get; set; }
        public ClientDeviceType DeviceType { get; set; }
        public bool IsMobile => DeviceType == ClientDeviceType.AndroidClient || DeviceType == ClientDeviceType.IosClient;

        public IAsyncResult BeginInvoke(Delegate method, object[] args)
        {
            throw new NotImplementedException();
        }

        public object EndInvoke(IAsyncResult result)
        {
            throw new NotImplementedException();
        }

        public object Invoke(Delegate method, object[] args)
        {
            throw new NotImplementedException();
        }

        public bool InvokeRequired { get; }

        public override string ToString()
        {
            return $@"Username: {UserName}, ComputerName: {ComputerName}, ComputerDomainName: {ComputerDomainName}, 
                        DomainName: {DomainName}, Ip: {Ip}, Guid: {Guid.ToString()}, Version: {Version}, DeskbarId: {DeskbarId}
                        DistinguishedName: {DistinguishedName}";
        }
    }
}