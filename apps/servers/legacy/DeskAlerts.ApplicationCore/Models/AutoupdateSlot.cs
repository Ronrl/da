﻿namespace DeskAlerts.ApplicationCore.Models
{
    using System;
    using System.Threading;

    public class AutoupdateSlot
    {
        public AutoupdateSlot(int lifeTimeMinutes, TimerCallback callback)
        {
            var lifetimeMilliseconds = lifeTimeMinutes * 60000;
            SlotExpirationTimer = new Timer(callback, this, lifetimeMilliseconds, Timeout.Infinite);
        }

        public Guid Id { get; set; }

        public ClientApplicationInfo ClientApplicationInfo { get; set; }

        public Timer SlotExpirationTimer;
    }
}