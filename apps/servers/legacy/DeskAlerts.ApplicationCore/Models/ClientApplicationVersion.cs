﻿namespace DeskAlerts.ApplicationCore.Models
{
    public class ClientApplicationInfo
    {
        public ClientApplicationInfo(string version, string deskbarId)
        {
            var arr = version.Split('.');
            Minor = int.Parse(arr[0]);
            Major = int.Parse(arr[1]);
            Build = int.Parse(arr[2]);
            Hotfix = int.Parse(arr[3]);
            DeskbarId = deskbarId;
        }

        public string DeskbarId { get; set; }

        public int Minor { get; set; }

        public int Major { get; set; }

        public int Build { get; set; }

        public int Hotfix { get; set; }
    }
}