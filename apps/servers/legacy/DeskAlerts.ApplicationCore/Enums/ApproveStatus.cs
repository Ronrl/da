﻿using DeskAlerts.ApplicationCore.Extensions.Enums;

namespace DeskAlerts.ApplicationCore.Enums
{
    public enum ApproveStatus
    {
        [StringValue("LNG_PENDING_APPROVAL")]
        NotModerated = 0,
        [StringValue("LNG_APPROVED")]
        ApprovedByDefault = 1,
        [StringValue("LNG_REJECTED")]
        Rejected = 2,
        [StringValue("LNG_APPROVED")]
        ApprovedByModerator = 3
    }
}