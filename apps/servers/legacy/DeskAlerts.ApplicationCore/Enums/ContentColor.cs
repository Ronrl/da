﻿using DeskAlerts.ApplicationCore.Extensions.Enums;

namespace DeskAlerts.ApplicationCore.Enums
{
    public enum ContentCustomClass
    {
        [StringValue("trialNotifications")]
        TrialNotification = 1,
        [StringValue("additionalDescriptions")]
        AdditionalDescription
    } 
}