﻿namespace DeskAlerts.ApplicationCore.Enums
{
    public enum TerminateType
    {
        NotTerminated = 0,
        Terminated
    }
}
