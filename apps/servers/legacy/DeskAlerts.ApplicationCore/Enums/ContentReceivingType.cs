﻿using DeskAlerts.ApplicationCore.Extensions.Enums;

namespace DeskAlerts.ApplicationCore.Enums
{
    /// <summary>
    /// Its a "Type2" in alerts table.
    /// </summary>
    public enum ContentReceivingType
    {
        [StringValue("")]
        Undefined = 0,
        [StringValue("B")]
        BroadCast,
        [StringValue("R")]
        Recipients,
        [StringValue("I")]
        IpRange
    }
}
