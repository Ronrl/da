﻿namespace DeskAlerts.ApplicationCore.Enums
{
    /// <summary>
    /// This is the field "device" in alert table in database.
    /// Desktop, mobile or all target to send on device in dashboard.
    /// </summary>
    public enum TargetDeviceType
    {
        Desktop = 1,
        Mobile = 2,
        All = 3
    }
}
