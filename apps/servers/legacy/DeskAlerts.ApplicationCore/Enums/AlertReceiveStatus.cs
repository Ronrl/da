﻿namespace DeskAlerts.ApplicationCore.Enums
{
    public enum AlertReceiveStatus
    {
        NotReceived = 0,
        Received = 1,
        Readed = 2
    }
}
