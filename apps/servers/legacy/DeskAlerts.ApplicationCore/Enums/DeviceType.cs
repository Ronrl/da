﻿namespace DeskAlerts.ApplicationCore.Enums
{
    public enum DeviceType
    {
        WindowsDesktop = 1,
        Android = 2,
        IOs = 3,
        WindowsPhone = 4,
        MacDesktop = 5
    }
}
