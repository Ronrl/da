﻿namespace DeskAlerts.ApplicationCore.Enums
{
    public enum ClientDeviceType
    {
        WindowsClient = 1,
        MacClient = 2,
        AndroidClient = 3,
        IosClient = 4
    }
}
