﻿using DeskAlerts.ApplicationCore.Extensions.Enums;

namespace DeskAlerts.ApplicationCore.Enums
{
    /// <summary>
    /// Its a "Type" in alerts table.
    /// </summary>
    public enum ContentState
    {
        [StringValue("")]
        Undefined = 0,
        [StringValue("S")]
        Send,
        [StringValue("D")]
        Draft
    }
}
