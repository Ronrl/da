﻿namespace DeskAlerts.ApplicationCore.Enums
{
    public enum AlertPosition
    {
        Empty = 0,
        FullScreen = 1,
        LeftTop = 2,
        RightTop = 3,
        Center = 4,
        LeftBottom = 5,
        RightBottom = 6,
        Top = 7,
        Middle = 8,
        Bottom = 9
    }
}
