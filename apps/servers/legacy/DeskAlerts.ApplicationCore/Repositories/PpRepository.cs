﻿namespace DeskAlerts.ApplicationCore.Repositories
{
    using System;
    using System.Collections.Generic;

    using Entities;
    using Exceptions;
    using Interfaces;

    using PetaPoco;

    public class PpRepository<T, K> : IRepository<T, K>
        where T : EntityBase<K>
    {
        public PpRepository(IConfigurationManager configurationManager)
        {
            if (configurationManager == null)
            {
                throw new ArgumentNullException(nameof(configurationManager));
            }

            var connectionString = configurationManager.GetConnectionString();

            Database = new Database(connectionString, string.Empty);
        }

        protected Database Database { get; }

        public K Create(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            var id = Database.Insert(entity);
            if (id == null)
            {
                throw new EntityNotCreatedException($"Couldn't create {typeof(T).Name} entity");
            }

            return (K)id;
        }

        public void Delete(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            var affected = Database.Delete(entity);
            if (affected == 0)
            {
                throw new EntityNotDeletedException($"Couldn't delete {typeof(T).Name} entity with id {entity.Id}");
            }
        }

        public List<T> GetAll()
        {
            return Database.Fetch<T>(string.Empty);
        }

        public T GetById(K id)
        {
            if (id == null)
            {
                throw new ArgumentNullException(nameof(id));
            }
            
            var entity = Database.SingleOrDefault<T>(id);
            if (entity == default(T))
            {
                throw new EntityNotFoundException($"Couldn't found {typeof(T).Name} entity with id {id}");
            }

            return entity;
        }

        public void Update(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            var affected = Database.Update(entity);
            if (affected == 0)
            {
                throw new EntityNotUpdatedException($"Couldn't update {typeof(T).Name} entity with id {entity.Id}");
            }
        }

        public bool EntityExistsByAttribute(string attributeName, string attributeValue)
        {
            if (attributeName == null)
            {
                throw new ArgumentNullException(nameof(attributeName));
            }

            return Database.Exists<T>(" WHERE @0 = @1", attributeName, attributeValue);
        }
    }
}