﻿using System;
using System.Collections.Generic;
using System.Linq;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Interfaces;

namespace DeskAlerts.ApplicationCore.Repositories
{
    public class PpUserInstancesIpRepository : PpRepository<UserInstanceIps, Guid>, IUserInstanceIpRepository
    {
        public PpUserInstancesIpRepository(IConfigurationManager connectionStringBuilder) : base(connectionStringBuilder)
        {
        }

        public void DeleteByUserInstanceId(Guid userInstanceId)
        {
            if (userInstanceId == Guid.Empty)
            {
                throw new ArgumentException("Id of entity must be not empty", nameof(userInstanceId));
            }

            Database.Delete<UserInstanceIps>("WHERE [user_instance_guid] = @0", userInstanceId);
        }

        public List<UserInstanceIps> GetByUserId(long userid)
        {
            return Database.Query<UserInstanceIps>($@"
                SELECT DISTINCT *
                FROM user_instance_ip WHERE user_instance_guid IN
                (SELECT clsid FROM user_instances WHERE user_id = {userid})
            ").ToList();
        }
    }
}