using System;
using System.Collections.Generic;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Exceptions;
using DeskAlerts.ApplicationCore.Interfaces;

namespace DeskAlerts.ApplicationCore.Repositories
{
    public class PpUserRepository : PpRepository<User, long>, IUserRepository
    {
        public PpUserRepository(IConfigurationManager connectionStringBuilder) : base(connectionStringBuilder)
        {
        }

        public void AddUserToGroup(User child, Group parent)
        {
            if (child == null)
            {
                throw new ArgumentNullException(nameof(child));
            }

            if (parent == null)
            {
                throw new ArgumentNullException(nameof(parent));
            }

            var id = Database.Insert("users_groups", "id", new { user_id = child.Id, group_id = parent.Id });
            if (id == null)
            {
                throw new EntityNotCreatedException("Couldn't create relationship user to group entity");
            }
        }

        public new void Delete(User entity)
        {
            base.Delete(entity);

            Database.Execute("DELETE [users_groups] WHERE [users_groups].[user_id] = @0", entity.Id);
        }

        public List<User> GetChildUsersByDomainName(string domainName)
        {
            if (domainName == null)
            {
                throw new ArgumentNullException(nameof(domainName));
            }

            return Database.Fetch<User>(
                "INNER JOIN [domains] ON [users].[domain_id] = [domains].[id] WHERE [domains].[name] = @0 AND [users].[next_request] != 'NULL'",
                domainName);
        }

        public List<User> GetChildUsersByGroupName(string groupName)
        {
            if (groupName == null)
            {
                throw new ArgumentNullException(nameof(groupName));
            }

            return Database.Fetch<User>(
                "INNER JOIN [users_groups] ON [users].[id] = [users_groups].[user_id] "
                + "INNER JOIN [groups] ON [groups].[id] = [users_groups].[group_id] WHERE [groups].[name] = @0 AND [users].[next_request] != 'NULL'",
                groupName);
        }

        public string GetUserDomainNameByUserId(long userId)
        {
            const string query = @"
                SELECT DISTINCT 
                       domains.name
                FROM domains
                     JOIN users ON domains.id = users.domain_id
                WHERE users.id = @userId;";

            var result = Database.FirstOrDefault<string>(query, new
            {
                userId = userId
            });

            return result;
        }

        public IEnumerable<User> GetPublishersByPolicyId(long policyId)
        {
            if (policyId == 0)
            {
                throw new ArgumentNullException(nameof(policyId));
            }

            return Database.Fetch<User>(
                @"SELECT u.*
                FROM policy_editor AS pe
                    LEFT JOIN users AS u ON u.id = @0
                WHERE u.id IS NOT NULL; ",
                policyId);
        }

        public User GetUserByDomainNameAndUserName(string domainName, string userName)
        {
            if (userName == null)
            {
                throw new ArgumentNullException(nameof(userName));
            }
            var user =
            Database.FirstOrDefault<User>(
                @"SELECT *
                FROM users AS u
                WHERE(u.name = @0)
                   AND (u.role = 'U'
                       OR u.role = 'A')
                   AND ((u.domain_id IS NULL)
                       OR ((u.domain_id =
                (
                  SELECT id
                  FROM domains
                  WHERE name = @1
               ))));", userName, domainName);
            return user;
        }

        public User GetPublisher(string userName)
        {
            if (userName == null)
            {
                throw new ArgumentNullException(nameof(userName));
            }

            return Database.SingleOrDefault<User>(@"
                SELECT *
                FROM users AS u
                WHERE u.name = @0
                    AND (u.role = 'E' OR u.role = 'A')", 
                userName);
        }

        public User GetUserByUserNameAndMd5Passowrd(string userName, string md5Password)
        {
            if (string.IsNullOrEmpty(userName))
            {
                throw new ArgumentNullException(nameof(userName));
            }

            if (string.IsNullOrEmpty(md5Password))
            {
                throw new ArgumentNullException(nameof(md5Password));
            }
            
            return Database.FirstOrDefault<User>(
            @"SELECT u.*
            FROM users AS u
            WHERE u.name = @0
                AND u.role = 'U' 
                AND u.pass = @1;", userName
            , md5Password
            );
        }

        public IEnumerable<string> GetDomainUsersUsernames(int domainId)
        {
            return Database.Fetch<string>($"SELECT users.name FROM users WHERE users.domain_id = {domainId}");
        }

        public void RemoveUserFromGroup(User child, Group parent)
        {
            if (child == null)
            {
                throw new ArgumentNullException(nameof(child));
            }

            if (parent == null)
            {
                throw new ArgumentNullException(nameof(parent));
            }

            var affected = Database.Execute(
                "DELETE [users_groups] WHERE [users_groups].[user_id] = @0 AND [users_groups].[group_id] = @1",
                child.Id,
                parent.Id);
            if (affected == 0)
            {
                throw new EntityNotDeletedException(@"Couldn't delete relationship user to group entity");
            }
        }

        /// <summary>
        /// Calculating how many active publishers you can have
        /// </summary>
        /// <returns></returns>
        public int GetActivePublishersAmount()
        {
            var activePublishersCount = Database.SingleOrDefault<int>(@"
                SELECT COUNT(id)
                FROM users 
                WHERE publisher_status = 'Enabled' 
                    AND role = 'E'"
            );

            return activePublishersCount;                                                                              
        }

        public int GetMaxPublishersAmount()
        {
            var maxPublishersCount = Database.SingleOrDefault<int>(@"
                SELECT val 
                FROM settings 
                WHERE name = 'ConfMaxPublishersCount'"
            );

            return maxPublishersCount;
        }

        public T GetPublisherParameterByName<T>(string parameter, string name)
        {
            var result = Database.ExecuteScalar<T>($@"
                SELECT {parameter} 
                FROM users 
                WHERE name = @0
                    AND (role = 'E' OR  role = 'A')", name
            );

            return result;
        }

        public void DisableLastOnlinePublishers(int amount)
        {
            Database.Execute($@"
                UPDATE users 
                SET publisher_status = 'Disabled' 
                WHERE name IN (
                    SELECT TOP {amount} name 
                    FROM users 
                    WHERE role = 'E'
                        AND publisher_status = 'Enabled'
                    ORDER BY publisher_last_online_datetime, reg_date ASC)"
            );
        }

        public void DisableLastOnlinePublishers(int amount, IEnumerable<string> allowedStatuses)
        {
            var allowedStatusesString = string.Join(",", allowedStatuses);
            if (string.IsNullOrEmpty(allowedStatusesString))
            {
                throw new ArgumentNullException(nameof(allowedStatuses));
            }

            Database.Execute($@"
                UPDATE users 
                SET publisher_status = 'Disabled' 
                WHERE name IN (
                    SELECT TOP {amount} name 
                    FROM users 
                    WHERE role = 'E'
                        AND publisher_status IN ({allowedStatusesString})
                    ORDER BY publisher_last_online_datetime, reg_date ASC)"
            );
        }

        public void DisablePublishers(IEnumerable<string> publishersIds)
        {
            var publishersIdsString = string.Join(",", publishersIds);
            if (string.IsNullOrEmpty(publishersIdsString))
            {
                throw new ArgumentNullException(nameof(publishersIds));
            }

            Database.Execute($@"
                UPDATE users 
                SET publisher_status = 'Disabled by Administrator' 
                WHERE id IN ({publishersIdsString})"
            );
        }

        public void EnablePublisher(string name)
        {
            Database.Execute($@"
                UPDATE users
                SET publisher_last_online_datetime = GETDATE(), publisher_status = 'Enabled' 
                WHERE name = @0", name
            );
        }

        public void EnablePublishers(IEnumerable<string> publishersIds)
        {
            var publishersIdsString = string.Join(",", publishersIds);
            if (string.IsNullOrEmpty(publishersIdsString))
            {
                throw new ArgumentNullException(nameof(publishersIds));
            }

            Database.Execute($@"
                UPDATE users 
                SET publisher_status = 'Enabled' 
                WHERE id IN ({publishersIdsString})"
            );
        }
    }
}