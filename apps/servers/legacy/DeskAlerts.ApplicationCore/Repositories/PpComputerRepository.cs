﻿using System;
using System.Collections.Generic;
using System.Linq;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Interfaces;

namespace DeskAlerts.ApplicationCore.Repositories
{
    public class PpComputerRepository : PpRepository<Computer, long>, IComputerRepository
    {
        public PpComputerRepository(IConfigurationManager connectionStringBuilder) : base(connectionStringBuilder) { }

        public Computer GetByNameAndDomainId(string name, long domainId)
        {
            return Database.SingleOrDefault<Computer>("WHERE [name] = @0 AND [domain_id] = @1", name, domainId);
        }

        public IEnumerable<long> GetUserIdsByComputerNames(IEnumerable<string> computerNames)
        {
            if (computerNames == null)
            {
                throw new ArgumentNullException(nameof(computerNames));
            }

            var result = new List<long>();

            computerNames = computerNames.ToList();
            if (!computerNames.Any())
            {
                return result;
            }

            const string query = @"
                SELECT DISTINCT 
                       ui.user_id
                FROM user_instances AS ui
                     JOIN user_instance_computers AS uic ON uic.user_instance_guid = ui.clsid
                     JOIN computers AS c ON c.id = uic.user_instance_computer_id
                WHERE c.name IN(@computerNamesList);";

            result = Database.Fetch<long>(
                query,
                new
                {
                    computerNamesList = computerNames
                });

            return result;
        }
    }
}
