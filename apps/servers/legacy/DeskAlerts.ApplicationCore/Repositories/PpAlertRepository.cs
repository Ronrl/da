﻿using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Enums;
using DeskAlerts.ApplicationCore.Extensions.Enums;
using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.ApplicationCore.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using AlertType = DeskAlerts.ApplicationCore.Entities.AlertType;

namespace DeskAlerts.ApplicationCore.Repositories
{
    public class PpAlertRepository : PpRepository<AlertEntity, long>, IAlertRepository
    {
        public PpAlertRepository(IConfigurationManager connectionStringBuilder) : base(connectionStringBuilder)
        {
        }

        public List<AlertEntity> GetAlertsByCampaignIdMappedWithUser(long campaignId)
        {
            return Database.Query<AlertEntity, User, AlertEntity>(
                (a, u) => { a.Sender = u; return a; },
                @"SELECT a.id, a.class, a.campaign_id, a.title, a.schedule, a.from_date, a.to_date, a.ticker, u.name
                        FROM alerts AS a
                        INNER JOIN users AS u ON u.id = a.sender_id
                        WHERE a.campaign_id=@0 AND a.class <> 5", campaignId).ToList();
        }

        public List<AlertEntity> GetByCampaignId(long campaignId)
        {
            return Database.Fetch<AlertEntity>("WHERE [alerts].[campaign_id] = @0", campaignId);
        }

        public long GetAlertsCount()
        {
            return Database.Fetch<long>($"SELECT COUNT(id) as count FROM alerts;").FirstOrDefault();
        }

        public long GetLastAlertId()
        {
            return Database.Fetch<long>($"SELECT TOP (1) id FROM alerts ORDER BY id DESC;").FirstOrDefault();
        }

        public IEnumerable<long> GetActualAlertIds(long userId, Guid deskBarId, DateTime dateTime, string ip, string computerName)
        {
            var localDateTime = dateTime.ToLocalTime();

            return GetActualAlertIdsByUser(userId, deskBarId, localDateTime)
                .Concat(GetActualAlertIdsByBroadCast(deskBarId, localDateTime))
                .Distinct();
        }

        public IEnumerable<long> GetActualAlertIds(long userId, DateTime dateTime)
        {
            var localDateTime = dateTime.ToLocalTime();

            return GetActualAlertIdsByUser(userId, localDateTime)
                .Concat(GetActualAlertIdsByBroadCast(localDateTime))
                .Distinct();
        }

        public IEnumerable<long> GetActualAlertIdsByUser(long userId, Guid deskBarId, DateTime dateTime)
        {
            const string query = @"
                SELECT DISTINCT 
                       a.id
                FROM alerts AS a
                     JOIN alerts_received AS ar ON ar.alert_id = a.id
                WHERE(a.to_date >= @datetime
                      AND a.from_date <= @datetime)
                     AND a.schedule_type = 1
                     AND a.type = @isSend
                     AND a.class = @contentType
                     AND a.approve_status != @approveStatusRejected
                     AND a.approve_status != @approveStatusNotModerated
                     AND a.type2 != @isBroadcast
                     AND a.ticker = 0
                     AND ar.status = @isNotReceived
                     AND ar.user_id = @userIdentifier
                     AND ar.clsid = @userDeskBarId 
                     AND a.is_repeatable_template != 1
                     AND a.id NOT IN
                (
                    SELECT alerts.id
                    FROM alerts
                         JOIN surveys_main ON surveys_main.sender_id = alerts.id
                );";

            var result = Database.Fetch<long>(
                query,
                new
                {
                    datetime = dateTime.ToString(DateTimeFormat.MsSqlFormat),
                    isSend = ContentState.Send.GetStringValue(),
                    contentType = (int)AlertType.SimpleAlert,
                    approveStatusRejected = (int)ApproveStatus.Rejected,
                    approveStatusNotModerated = (int)ApproveStatus.NotModerated,
                    isBroadcast = ContentReceivingType.BroadCast.GetStringValue(),
                    isNotReceived = (int)AlertReceiveStatus.NotReceived,
                    userIdentifier = userId,
                    userDeskBarId = deskBarId
                }
            );

            return result;
        }

        public IEnumerable<long> GetActualAlertIdsByBroadCast(Guid deskBarId, DateTime dateTime)
        {
            const string query = @"
                SELECT DISTINCT 
                       a.id
                FROM alerts AS a
                     JOIN alerts_received AS ar ON ar.alert_id = a.id
                WHERE(a.to_date >= @datetime
                      AND a.from_date <= @datetime)
                     AND a.schedule_type = 1
                     AND a.type = @isSend
                     AND a.class = @contentType
                     AND a.approve_status != @approveStatusRejected
                     AND a.approve_status != @approveStatusNotModerated
                     AND a.type2 = @isBroadcast
                     AND a.ticker = 0
                     AND ar.status = @isNotReceived
                     AND ar.clsid = @userDeskBarId
                     AND a.is_repeatable_template != 1
                     AND a.id NOT IN
                (
                    SELECT alerts.id
                    FROM alerts
                         JOIN surveys_main ON surveys_main.sender_id = alerts.id
                );";

            var result = Database.Fetch<long>(
                query,
                new
                {
                    datetime = dateTime.ToString(DateTimeFormat.MsSqlFormat),
                    isSend = ContentState.Send.GetStringValue(),
                    contentType = (int)AlertType.SimpleAlert,
                    approveStatusRejected = (int)ApproveStatus.Rejected,
                    approveStatusNotModerated = (int)ApproveStatus.NotModerated,
                    isBroadcast = ContentReceivingType.BroadCast.GetStringValue(),
                    isNotReceived = (int)AlertReceiveStatus.NotReceived,
                    userDeskBarId = deskBarId
                }
            );

            return result;
        }

        public IEnumerable<long> GetActualAlertIdsByUser(long userId, DateTime dateTime)
        {
            const string query = @"
                SELECT DISTINCT 
                       a.id
                FROM alerts AS a
                     JOIN alerts_received AS ar ON ar.alert_id = a.id
                WHERE(a.to_date >= @datetime
                      AND a.from_date <= @datetime)
                     AND a.schedule_type = 1
                     AND a.type = @isSend
                     AND a.class = @contentType
                     AND a.approve_status != @approveStatusRejected
                     AND a.approve_status != @approveStatusNotModerated
                     AND a.type2 != @isBroadcast
                     AND a.ticker = 0
                     AND ar.user_id = @userIdentifier
                     AND a.is_repeatable_template != 1
                     AND a.id NOT IN
                (
                    SELECT alerts.id
                    FROM alerts
                         JOIN surveys_main ON surveys_main.sender_id = alerts.id
                );";

            var result = Database.Fetch<long>(
                query,
                new
                {
                    datetime = dateTime.ToString(DateTimeFormat.MsSqlFormat),
                    isSend = ContentState.Send.GetStringValue(),
                    contentType = (int)AlertType.SimpleAlert,
                    approveStatusRejected = (int)ApproveStatus.Rejected,
                    approveStatusNotModerated = (int)ApproveStatus.NotModerated,
                    isBroadcast = ContentReceivingType.BroadCast.GetStringValue(),
                    userIdentifier = userId
                }
            );

            return result;
        }

        public IEnumerable<long> GetActualAlertIdsByBroadCast(DateTime dateTime)
        {
            const string query = @"
                SELECT DISTINCT 
                       a.id
                FROM alerts AS a
                WHERE(a.to_date >= @datetime
                      AND a.from_date <= @datetime)
                     AND a.schedule_type = 1
                     AND a.type = @isSend
                     AND a.class = @contentType
                     AND a.approve_status != @approveStatusRejected
                     AND a.approve_status != @approveStatusNotModerated
                     AND a.type2 = @isBroadcast
                     AND a.ticker = 0
                     AND a.is_repeatable_template != 1
                     AND a.id NOT IN
                (
                    SELECT alerts.id
                    FROM alerts
                         JOIN surveys_main ON surveys_main.sender_id = alerts.id
                );";

            var result = Database.Fetch<long>(
                query,
                new
                {
                    datetime = dateTime.ToString(DateTimeFormat.MsSqlFormat),
                    isSend = ContentState.Send.GetStringValue(),
                    contentType = (int)AlertType.SimpleAlert,
                    approveStatusRejected = (int)ApproveStatus.Rejected,
                    approveStatusNotModerated = (int)ApproveStatus.NotModerated,
                    isBroadcast = ContentReceivingType.BroadCast.GetStringValue()
                }
            );

            return result;
        }

        public IEnumerable<long> GetRecipientsUsersIds(long alertId)
        {
            const string query = @"
                SELECT DISTINCT 
                       u.id
                FROM alerts AS a
                     JOIN alerts_received ON a.id = alerts_received.alert_id
                     JOIN users AS u ON u.id = alerts_received.user_id
                WHERE class = @0
                      AND a.ticker = 0
                      AND a.id = @1
                      AND a.type2 != @2 
                      AND a.id NOT IN (SELECT sender_id FROM surveys_main)";

            var result = Database.Fetch<long>(
                query,
                (int)AlertType.SimpleAlert,
                alertId,
                ContentReceivingType.BroadCast.GetStringValue());

            return result;
        }

        public AlertType GetContentTypeByContentId(long contentId)
        {
            if (contentId <= default(long))
            {
                return AlertType.Undefined;
            }

            const string query = @"
                SELECT a.class
                FROM alerts AS a
                WHERE a.id = @0";

            var result = Database.Fetch<long>(query, contentId).FirstOrDefault();

            if ((AlertType)result != AlertType.SimpleAlert)
            {
                return (AlertType)result;

            }

            if (IsTickerContent(contentId))
            {
                return AlertType.Ticker;
            }
            else if (IsRsvpContent(contentId))
            {
                return AlertType.Rsvp;
            }
            else
            {
                return AlertType.SimpleAlert;
            }
        }

        private bool IsTickerContent(long contentId)
        {
            const string query = @"
                SELECT a.ticker
                FROM alerts AS a
                WHERE a.id = @0;";

            var result = Database.Fetch<char>(query, contentId).FirstOrDefault();

            if (result == '1')
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool IsRsvpContent(long contentId)
        {
            const string query = @"
                SELECT a.id
                FROM alerts AS a
                     JOIN surveys_main AS sm ON sm.sender_id = a.id
                WHERE a.class = @0
                      AND a.id = @1;";

            var result = Database.Fetch<long>(query, AlertType.SimpleAlert, contentId).FirstOrDefault();

            if (result != default(long))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public IEnumerable<AlertEntity> GetScheduledActiveAlerts()
        {
            var validAlertTypesFotPushes = new List<int>
            { 
                (int)AlertType.SimpleAlert,
                (int)AlertType.RssAlert,
                (int)AlertType.EmergencyAlert,
                (int)AlertType.SimpleSurvey,
                (int)AlertType.SurveyPoll,
                (int)AlertType.SurveyQuiz
            };

            return GetAll()
                .Where(x => x.Schedule != 0 &&
                            x.SendType != 'D' &&
                            x.Device != (int) TargetDeviceType.Desktop &&
                            validAlertTypesFotPushes.Contains(x.Class) &&
                            x.ApproveStatus == (int)ApproveStatus.ApprovedByDefault &&
                            x.IsStarted &&
                            !x.IsExpired);
        }

        public IEnumerable<AlertEntity> GetApprovedActiveAlerts()
        {
            var invalidAlertTypesFotPushes = new List<int>
            {
                (int)AlertType.Wallpaper,
                (int)AlertType.ScreenSaver,
            };

            return GetAll()
                .Where(x => x.Device != (int)TargetDeviceType.Desktop &&
                            x.SendType != 'D' &&
                            !invalidAlertTypesFotPushes.Contains(x.Class) &&
                            x.ApproveStatus == (int)ApproveStatus.ApprovedByModerator &&
                            x.IsStarted &&
                            !x.IsExpired);
        }
    }
}