﻿using System.Linq;

namespace DeskAlerts.ApplicationCore.Repositories
{
    using System;
    using System.Collections.Generic;
    using DeskAlerts.ApplicationCore.Entities;
    using DeskAlerts.ApplicationCore.Exceptions;
    using DeskAlerts.ApplicationCore.Interfaces;
    using DeskAlerts.ApplicationCore.Replicators;

    public class PpGroupRepository : PpRepository<Group, long>, IGroupRepository
    {
        public PpGroupRepository(IConfigurationManager connectionStringBuilder) : base(connectionStringBuilder)
        {
        }

        public void AddGroupToGroup(Group child, Group parent)
        {
            if (child == null)
            {
                throw new ArgumentNullException(nameof(child));
            }

            if (parent == null)
            {
                throw new ArgumentNullException(nameof(parent));
            }

            var id = Database.Insert(
                "groups_groups",
                "id",
                new { group_id = child.Id, container_group_id = parent.Id });
            if (id == null)
            {
                throw new EntityNotCreatedException("Couldn't create relationship group to group entity");
            }
        }

        public new void Delete(Group entity)
        {
            base.Delete(entity);
            Database.Execute("DELETE [groups_groups] WHERE [groups_groups].[group_id] = @0", entity.Id);
        }

        public List<Group> GetChildGroupsByDomainName(string domainName)
        {
            if (domainName == null)
            {
                throw new ArgumentNullException(nameof(domainName));
            }

            return Database.Fetch<Group, Policy>(
                @"SELECT g.*, p.*
                FROM groups AS g
                INNER JOIN [domains] ON g.[domain_id] = [domains].[id] 
                LEFT JOIN [policy] AS p ON g.policy_id = p.id
                WHERE [domains].[name] = @0",
                domainName);
        }

        public List<Group> GetChildGroupsByGroupName(string groupName)
        {
            if (groupName == null)
            {
                throw new ArgumentNullException(nameof(groupName));
            }

            return Database.Fetch<Group>(
                "INNER JOIN [groups_groups] ON [groups].[id] = [groups_groups].[group_id] "
                + "INNER JOIN [groups] as [container_group] ON [container_group].[id] = [groups_groups].[container_group_id]  "
                + "WHERE [container_group].[name] = @0",
                groupName);
        }

        public List<Group> GetChildGroupsByGroupId(int groupId)
        {
            if (groupId == 0)
            {
                throw new ArgumentNullException(nameof(groupId));
            }

            return Database.Fetch<Group>(
                @"
            SELECT g.*, p.* 
            FROM groups AS g 
            INNER JOIN[groups_groups] AS gg ON g.[id] = gg.[group_id]
            INNER JOIN[groups] AS cg ON cg.[id] = gg.[container_group_id]
            LEFT JOIN policy AS p ON p.id = g.policy_id
            WHERE cg.[id] = @0",
                groupId);
        }

        public void RemoveGroupFromGroup(Group child, Group parent)
        {
            if (child == null)
            {
                throw new ArgumentNullException(nameof(child));
            }

            if (parent == null)
            {
                throw new ArgumentNullException(nameof(parent));
            }

            var affected = Database.Execute(
                "DELETE [groups_groups] WHERE [groups_groups].[group_id] = @0 AND [groups_groups].[container_group_id] = @1",
                child.Id,
                parent.Id);
            if (affected == 0)
            {
                throw new EntityNotDeletedException(@"Couldn't delete relationship group to group entity");
            }
        }

        public IEnumerable<Group> GetGroupsWithAdminPolicy()
        {
            return Database.Query<Group, Policy>(
                @"SELECT * 
                FROM groups 
                INNER JOIN policy ON policy.id = groups.policy_id 
                WHERE groups.domain_id IS NOT NULL 
                AND policy.type = 'A'");
        }

        public IEnumerable<long> GetUserIdsByGroupNames(IEnumerable<string> groupNames)
        {
            if (groupNames == null)
            {
                throw new ArgumentNullException(nameof(groupNames));
            }

            var result = new List<long>();

            groupNames = groupNames.ToList();
            if (!groupNames.Any())
            {
                return result;
            }

            const string query = @"
                SELECT DISTINCT 
                       users_groups.user_id
                FROM groups
                     JOIN users_groups ON users_groups.group_id = groups.id
                WHERE groups.name IN(@groupNamesList);";

            result = Database.Fetch<long>(
                query,
                new
                {
                    groupNamesList = groupNames
                });

            return result;
        }

        public IEnumerable<Group> GetGroupByDomainIdMappedWithUser(long domainId, string userName)
        {
            return Database.Query<Group, User, Group>(
                new GroupToUserRelator().MapIt,
                @"SELECT g.*,
                        u.*
                    FROM [groups] AS g
                        LEFT JOIN [users_groups] AS ug ON ug.group_id = g.id
                        LEFT JOIN [users] AS u ON u.id = ug.[user_id]
                    WHERE g.domain_id = @0
                        AND g.policy_id IS NOT NULL
                        AND u.name = @1;",
                domainId,
                userName);
        }

        public IEnumerable<Group> GetGroupsWithDomain()
        {
            return Database.Fetch<Group, Domain>(@"SELECT g.*, d.* FROM [groups] AS g RIGHT JOIN domains AS d ON d.id = g.domain_id WHERE [policy_id] IS NOT NULL AND [domain_id] <> 1;");
        }

        public IEnumerable<Group> GetGroupByDomainIdAndUserIdMappedWithUser(long domainId, long userId)
        {
            return Database.Fetch<Group, Policy>(
                @"SELECT g.*, p.*
                    FROM [groups] AS g
                        LEFT JOIN [users_groups] AS ug ON ug.group_id = g.id
                        LEFT JOIN [users] AS u ON u.id = ug.[user_id]
                        LEFT JOIN [policy] AS p ON p.id = g.policy_id
                    WHERE g.domain_id = @0
                        AND g.policy_id IS NOT NULL
                        AND u.id = @1;",
                domainId,
                userId);
        }

        public IEnumerable<Group> GetGroupsPublisherId(long userId)
        {
            return Database.Fetch<Group, Policy>(
                @"SELECT g.*, p.*
                    FROM [groups] AS g
                        LEFT JOIN [users_groups] AS ug ON ug.group_id = g.id
                        LEFT JOIN [users] AS u ON u.id = ug.[user_id]
                        LEFT JOIN [policy] AS p ON p.id = g.policy_id
                    WHERE AND g.policy_id IS NOT NULL
                        AND u.id = @0;",
                userId);
        }
    }
}