﻿using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Enums;
using DeskAlerts.ApplicationCore.Extensions.Enums;
using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.ApplicationCore.Structs;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DeskAlerts.ApplicationCore.Repositories
{
    public class PpVideoAlertRepository : PpRepository<AlertEntity, long>, IVideoAlertRepository
    {
        private readonly IAlertRepository _alertRepository;

        public PpVideoAlertRepository(IConfigurationManager configurationManager, IAlertRepository alertRepository) : base(configurationManager)
        {
            _alertRepository = alertRepository;
        }

        public IEnumerable<long> GetActualVideoAlertsIds(long userId, Guid deskBarId, DateTime dateTime)
        {
            var localDateTime = dateTime.ToLocalTime();

            return GetVideoAlertsIdsByUser(userId, deskBarId, localDateTime)
                .Concat(GetVideoAlertsIdsByBroadCast(deskBarId, localDateTime))
                .Distinct();
        }

        public IEnumerable<long> GetActualVideoAlertsIds(long userId, DateTime dateTime)
        {
            var localDateTime = dateTime.ToLocalTime();

            return GetVideoAlertsIdsByUser(userId, localDateTime)
                .Concat(GetVideoAlertsIdsByBroadCast(localDateTime))
                .Distinct();
        }

        public IEnumerable<long> GetVideoAlertsIdsByUser(long userId, Guid deskBarId, DateTime dateTime)
        {
            const string query = @"
                SELECT DISTINCT 
                       a.id
                FROM alerts AS a
                     JOIN alerts_received AS ar ON a.id = ar.alert_id
                WHERE(a.to_date > @datetime
                      AND a.from_date < @datetime)
                     AND a.schedule_type = 1
                     AND a.type = @isSend
                     AND a.class = @contentType
                     AND a.approve_status != @approveStatusRejected
                     AND a.approve_status != @approveStatusNotModerated
                     AND ar.status = @isNotReceived
                     AND a.type2 != @isBroadcast
                     AND ar.user_id = @userIdentifier
                     AND ar.clsid = @userDeskBarId 
                     AND a.is_repeatable_template != 1;";

            var result = Database.Fetch<long>(
                query,
                new
                {
                    datetime = dateTime.ToString(DateTimeFormat.MsSqlFormat),
                    isSend = ContentState.Send.GetStringValue(),
                    contentType = AlertType.VideoAlert,
                    approveStatusRejected = (int)ApproveStatus.Rejected,
                    approveStatusNotModerated = (int)ApproveStatus.NotModerated,
                    isNotReceived = (int)AlertReceiveStatus.NotReceived,
                    isBroadcast = ContentReceivingType.BroadCast.GetStringValue(),
                    userIdentifier = userId,
                    userDeskBarId = deskBarId
                });

            return result;
        }

        public IEnumerable<long> GetVideoAlertsIdsByBroadCast(Guid deskBarId, DateTime dateTime)
        {
            const string query = @"
                SELECT DISTINCT 
                       a.id
                FROM alerts AS a
                     JOIN alerts_received AS ar ON a.id = ar.alert_id
                WHERE(a.to_date > @datetime
                      AND a.from_date < @datetime)
                     AND a.schedule_type = 1
                     AND a.type = @isSend
                     AND a.class = @contentType
                     AND a.approve_status != @approveStatusRejected
                     AND a.approve_status != @approveStatusNotModerated
                     AND ar.status = @isNotReceived
                     AND a.type2 = @isBroadcast
                     AND ar.clsid = @userDeskBarId 
                     AND a.is_repeatable_template != 1;";

            var result = Database.Fetch<long>(
                query,
                new
                {
                    datetime = dateTime.ToString(DateTimeFormat.MsSqlFormat),
                    isSend = ContentState.Send.GetStringValue(),
                    contentType = AlertType.VideoAlert,
                    approveStatusRejected = (int)ApproveStatus.Rejected,
                    approveStatusNotModerated = (int)ApproveStatus.NotModerated,
                    isNotReceived = (int)AlertReceiveStatus.NotReceived,
                    isBroadcast = ContentReceivingType.BroadCast.GetStringValue(),
                    userDeskBarId = deskBarId
                });

            return result;
        }

        public IEnumerable<long> GetVideoAlertsIdsByUser(long userId, DateTime dateTime)
        {
            const string query = @"
                SELECT DISTINCT 
                       a.id
                FROM alerts AS a
                     JOIN alerts_received AS ar ON a.id = ar.alert_id
                WHERE(a.to_date > @datetime
                      AND a.from_date < @datetime)
                     AND a.schedule_type = 1
                     AND a.type = @isSend
                     AND a.class = @contentType
                     AND a.approve_status != @approveStatusRejected
                     AND a.approve_status != @approveStatusNotModerated
                     AND a.type2 != @isBroadcast
                     AND ar.user_id = @userIdentifier
                     AND a.is_repeatable_template != 1;";

            var result = Database.Fetch<long>(
                query,
                new
                {
                    datetime = dateTime.ToString(DateTimeFormat.MsSqlFormat),
                    isSend = ContentState.Send.GetStringValue(),
                    contentType = AlertType.VideoAlert,
                    approveStatusRejected = (int)ApproveStatus.Rejected,
                    approveStatusNotModerated = (int)ApproveStatus.NotModerated,
                    isBroadcast = ContentReceivingType.BroadCast.GetStringValue(),
                    userIdentifier = userId
                });

            return result;
        }

        public IEnumerable<long> GetVideoAlertsIdsByBroadCast(DateTime dateTime)
        {
            const string query = @"
                SELECT DISTINCT 
                       a.id
                FROM alerts AS a
                WHERE(a.to_date > @datetime
                      AND a.from_date < @datetime)
                     AND a.schedule_type = 1
                     AND a.type = @isSend
                     AND a.class = @contentType
                     AND a.approve_status != @approveStatusRejected
                     AND a.approve_status != @approveStatusNotModerated
                     AND a.type2 = @isBroadcast
                     AND a.is_repeatable_template != 1;";

            var result = Database.Fetch<long>(
                query,
                new
                {
                    datetime = dateTime.ToString(DateTimeFormat.MsSqlFormat),
                    isSend = ContentState.Send.GetStringValue(),
                    contentType = AlertType.VideoAlert,
                    approveStatusRejected = (int)ApproveStatus.Rejected,
                    approveStatusNotModerated = (int)ApproveStatus.NotModerated,
                    isBroadcast = ContentReceivingType.BroadCast.GetStringValue()
                });

            return result;
        }

        public IEnumerable<long> GetRecipientsUsersIds(long tickerId)
        {
            const string query = @"
                SELECT DISTINCT 
                       u.id
                FROM alerts AS a
                     JOIN alerts_received ON a.id = alerts_received.alert_id
                     JOIN users AS u ON u.id = alerts_received.user_id
                WHERE class = @0
                      AND a.type2 != @1
                      AND a.id = @2;";

            var result = Database.Fetch<long>(
                query,
                AlertType.VideoAlert,
                ContentReceivingType.BroadCast.GetStringValue(),
                tickerId);

            return result;
        }
    }
}
