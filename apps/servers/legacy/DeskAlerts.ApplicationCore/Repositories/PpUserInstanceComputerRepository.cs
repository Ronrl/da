﻿using System;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Interfaces;

namespace DeskAlerts.ApplicationCore.Repositories
{
    public class PpUserInstanceComputerRepository : PpRepository<UserInstanceComputer ,Guid>, IUserInstanceComputerRepository
    {
        public PpUserInstanceComputerRepository(IConfigurationManager connectionStringBuilder) : base(connectionStringBuilder)
        {
        }
    }
}