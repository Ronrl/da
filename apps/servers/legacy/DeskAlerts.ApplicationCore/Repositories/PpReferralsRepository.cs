﻿namespace DeskAlerts.ApplicationCore.Repositories
{
    using System.Collections.Generic;

    using DeskAlerts.ApplicationCore.Entities;
    using DeskAlerts.ApplicationCore.Interfaces;

    public class PpReferralsRepository : PpRepository<Referral, long>, IReferralsRepository
    {
        public PpReferralsRepository(IConfigurationManager configurationManager) : base(configurationManager)
        {
        }
    }
}
