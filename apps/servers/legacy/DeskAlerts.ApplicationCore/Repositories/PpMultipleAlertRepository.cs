﻿using System.Collections.Generic;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Interfaces;

namespace DeskAlerts.ApplicationCore.Repositories
{
    public class PpMultipleAlertRepository : PpRepository<MultipleAlert, int>, IMultipleAlertRepository
    {
        public PpMultipleAlertRepository(IConfigurationManager connectionStringBuilder) : base(connectionStringBuilder)
        {
        }

        public IEnumerable<string> GetConfirmedUsernames(long alertId)
        {
            return Database.Fetch<string>(@"
                SELECT DISTINCT u.name 
                FROM alerts_received AS ar
                LEFT JOIN users AS u ON ar.user_id = u.id
                WHERE ar.alert_id = @0
                    AND (SELECT count(1) 
                         FROM multiple_alerts 
                         WHERE alert_id = @1 AND confirmed = 1) > 0 ", alertId, alertId);
        }

        public bool CheckIfMultiple(long alertId)
        {
            // Check records existing and group sending
            var existingRecordsAmount = Database.ExecuteScalar<int>(@"
                SELECT count(1)
                FROM multiple_alerts
                WHERE alert_id = @0
                    AND part_id != -1", alertId);

            return existingRecordsAmount > 0;
        }
    }
}