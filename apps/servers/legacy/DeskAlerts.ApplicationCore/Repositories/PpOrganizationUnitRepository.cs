﻿using System;
using System.Collections.Generic;
using System.Linq;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Interfaces;

namespace DeskAlerts.ApplicationCore.Repositories
{
    public class PpOrganizationUnitRepository : PpRepository<OrganizationUnit, long>, IOrganizationUnitRepository
    {
        public PpOrganizationUnitRepository(IConfigurationManager configurationManager) : base(configurationManager)
        {
        }

        public IEnumerable<long> GetUserIdsByOrganizationUnitNames(IEnumerable<string> organizationUnitNames)
        {
            if (organizationUnitNames == null)
            {
                throw new ArgumentNullException(nameof(organizationUnitNames));
            }

            var result = new List<long>();

            organizationUnitNames = organizationUnitNames.ToList();
            if (!organizationUnitNames.Any())
            {
                return result;
            }

            const string query = @"
                SELECT DISTINCT 
                       ouu.Id_user
                FROM OU_User AS ouu
                     LEFT JOIN OU ON OU.Id = ouu.Id_OU
                WHERE OU.Name IN(@ouNamesList);";

            result = Database.Fetch<long>(
                query,
                new
                {
                    ouNamesList = organizationUnitNames
                });

            return result;
        }
    }
}
