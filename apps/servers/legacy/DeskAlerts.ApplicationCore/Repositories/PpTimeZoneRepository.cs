﻿using System.Collections.Generic;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Interfaces;

namespace DeskAlerts.ApplicationCore.Repositories
{
    public class PpTimeZoneRepository : PpRepository<User, long>, ITimeZoneRepository
    {
        public PpTimeZoneRepository(IConfigurationManager connectionStringBuilder) : base(connectionStringBuilder)
        {
        }

        public IEnumerable<int> GetAllTimeZones()
        {
            return Database.Fetch<int>(@"
                SELECT DISTINCT u.time_zone
                FROM users AS u
                WHERE u.time_zone IS NOT NULL");
        }
    }
}
