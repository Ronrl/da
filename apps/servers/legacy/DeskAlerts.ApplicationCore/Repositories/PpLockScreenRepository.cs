﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Enums;
using DeskAlerts.ApplicationCore.Extensions.Enums;
using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.ApplicationCore.Structs;


namespace DeskAlerts.ApplicationCore.Repositories
{
    public class PpLockScreenRepository : PpRepository<AlertEntity, long>, ILockScreenRepository
    {
        public PpLockScreenRepository(IConfigurationManager configurationManager) : base(configurationManager)
        {
        }

        public IEnumerable<long> GetActualLockScreensIds(long userId, Guid deskBarId, DateTime dateTime, string ip, string computerName)
        {
            var localDateTime = dateTime.ToLocalTime();

            return GetLockScreensIdsByUser(userId, deskBarId, localDateTime)
                .Concat(GetLockScreensIdsByBroadCast(deskBarId, localDateTime))
                .Distinct();
        }

        public IEnumerable<long> GetActualLockScreensIds(long userId, DateTime dateTime)
        {
            var localDateTime = dateTime.ToLocalTime();

            return GetLockScreensIdsByUser(userId, localDateTime)
                .Concat(GetLockScreensIdsByBroadCast(localDateTime))
                .Distinct();
        }

        public IEnumerable<long> GetLockScreensIdsByUser(long userId, Guid deskBarId, DateTime dateTime)
        {
            const string query = @"
                SELECT DISTINCT 
                       a.id
                FROM alerts AS a
                     JOIN alerts_received AS ar ON a.id = ar.alert_id
                WHERE(a.to_date > @datetime
                      AND a.from_date < @datetime)
                     AND a.schedule_type = 1
                     AND a.type = @isSend
                     AND a.class = @contentType
                     AND a.approve_status != @approveStatusRejected
                     AND a.approve_status != @approveStatusNotModerated
                     AND a.type2 != @isBroadcast
                     AND ar.user_id = @userIdentifier
                     AND ar.clsid = @userDeskBarId;";

            return Database.Fetch<long>(
                query,
                new
                {
                    datetime = dateTime.ToString(DateTimeFormat.MsSqlFormat),
                    isSend = ContentState.Send.GetStringValue(),
                    contentType = AlertType.LockScreen,
                    approveStatusRejected = (int)ApproveStatus.Rejected,
                    approveStatusNotModerated = (int)ApproveStatus.NotModerated,
                    isBroadcast = ContentReceivingType.BroadCast.GetStringValue(),
                    userIdentifier = userId,
                    userDeskBarId = deskBarId
                });
        }

        public IEnumerable<long> GetLockScreensIdsByBroadCast(Guid deskBarId, DateTime dateTime)
        {
            const string query = @"
                SELECT DISTINCT 
                       a.id
                FROM alerts AS a
                     JOIN alerts_received AS ar ON a.id = ar.alert_id
                WHERE(a.to_date > @datetime
                      AND a.from_date < @datetime)
                     AND a.schedule_type = 1
                     AND a.type = @isSend
                     AND a.class = @contentType
                     AND a.approve_status != @approveStatusRejected
                     AND a.approve_status != @approveStatusNotModerated
                     AND a.type2 = @isBroadcast
                     AND ar.clsid = @userDeskBarId;";

            var result = Database.Fetch<long>(
                query,
                new
                {
                    datetime = dateTime.ToString(DateTimeFormat.MsSqlFormat),
                    isSend = ContentState.Send.GetStringValue(),
                    contentType = AlertType.LockScreen,
                    approveStatusRejected = (int)ApproveStatus.Rejected,
                    approveStatusNotModerated = (int)ApproveStatus.NotModerated,
                    isBroadcast = ContentReceivingType.BroadCast.GetStringValue(),
                    userDeskBarId = deskBarId
                });

            return result;
        }

        public IEnumerable<long> GetLockScreensIdsByUser(long userId, DateTime dateTime)
        {
            const string query = @"
                SELECT DISTINCT 
                       a.id
                FROM alerts AS a
                     JOIN alerts_received AS ar ON a.id = ar.alert_id
                WHERE(a.to_date > @datetime
                      AND a.from_date < @datetime)
                     AND a.schedule_type = 1
                     AND a.type = @isSend
                     AND a.class = @contentType
                     AND a.approve_status != @approveStatusRejected
                     AND a.approve_status != @approveStatusNotModerated
                     AND a.type2 != @isBroadcast
                     AND ar.user_id = @userIdentifier;";

            return Database.Fetch<long>(
                query,
                new
                {
                    datetime = dateTime.ToString(DateTimeFormat.MsSqlFormat),
                    isSend = ContentState.Send.GetStringValue(),
                    contentType = AlertType.LockScreen,
                    approveStatusRejected = (int)ApproveStatus.Rejected,
                    approveStatusNotModerated = (int)ApproveStatus.NotModerated,
                    isBroadcast = ContentReceivingType.BroadCast.GetStringValue(),
                    userIdentifier = userId
                });
        }

        public IEnumerable<long> GetLockScreensIdsByBroadCast(DateTime dateTime)
        {
            const string query = @"
                SELECT DISTINCT 
                       a.id
                FROM alerts AS a
                     JOIN alerts_received AS ar ON a.id = ar.alert_id
                WHERE(a.to_date > @datetime
                      AND a.from_date < @datetime)
                     AND a.schedule_type = 1
                     AND a.type = @isSend
                     AND a.class = @contentType
                     AND a.approve_status != @approveStatusRejected
                     AND a.approve_status != @approveStatusNotModerated
                     AND a.type2 = @isBroadcast;";

            var result = Database.Fetch<long>(
                query,
                new
                {
                    datetime = dateTime.ToString(DateTimeFormat.MsSqlFormat),
                    isSend = ContentState.Send.GetStringValue(),
                    contentType = AlertType.LockScreen,
                    approveStatusRejected = (int)ApproveStatus.Rejected,
                    approveStatusNotModerated = (int)ApproveStatus.NotModerated,
                    isBroadcast = ContentReceivingType.BroadCast.GetStringValue()
                });

            return result;
        }

        public IEnumerable<long> GetRecipientsUsersIds(long lockscreenId)
        {
            //TODO: Add enum for 'type2' property
            const string query = @"
                SELECT DISTINCT 
                    u.id
                FROM alerts AS a
                    JOIN alerts_received ON a.id = alerts_received.alert_id
                    JOIN users AS u ON u.id = alerts_received.user_id
                WHERE class = @0
                    AND a.type2 != @1 
                    AND a.id = @2;";

            var result = Database.Fetch<long>(
                query,
                AlertType.LockScreen,
                ContentReceivingType.BroadCast.GetStringValue(),
                lockscreenId);

            return result;
        }

        public IEnumerable<long> GetRecipientsCompsIds(long lockscreenId)
        {
            const string query = @"
                SELECT c.id
                FROM alerts AS a
	                JOIN alerts_sent_comp AS sc ON sc.alert_id = a.id
	                JOIN computers AS c ON c.id = sc.comp_id
                WHERE a.schedule_type = 1
                    AND a.class = @0
                    AND a.type2 != @1
	                AND a.id = @2;";

            var result = Database.Fetch<long>(
                query,
                AlertType.LockScreen,
                ContentReceivingType.BroadCast.GetStringValue(),
                lockscreenId);

            return result;
        }

        public IEnumerable<AlertEntity> GetActualSchedulingLockscreens()
        {
            const string query = @"
                SELECT *
                FROM alerts
                WHERE((from_date > @0)
                      OR (from_date < @0
                          AND @0 < to_date))
                     AND schedule = 1
                     AND class = @1;";

            var result = Database.Fetch<AlertEntity>(
                query,
                DateTime.Now.ToString(DateTimeFormat.MsSqlFormat, CultureInfo.InvariantCulture),
                AlertType.LockScreen);

            return result;
        }
    }
}
    