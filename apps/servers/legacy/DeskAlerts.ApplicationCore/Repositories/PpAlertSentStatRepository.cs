﻿using System.Collections.Generic;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Interfaces;

namespace DeskAlerts.ApplicationCore.Repositories
{
    public class PpAlertSentStatRepository : PpRepository<AlertSentStat, long>, IAlertSentStatRepository
    {
        public PpAlertSentStatRepository(IConfigurationManager configurationManager) : base(configurationManager)
        {
        }

        public IEnumerable<AlertSentStat> GetAllByContentId(long contentId)
        {
            const string query = @"
                SELECT DISTINCT als.* 
                FROM alerts_sent_stat AS als 
                WHERE alert_id = @0";

            return Database.Fetch<AlertSentStat>(query, contentId);
        }
    }
}
