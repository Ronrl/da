﻿using System;
using System.Collections.Generic;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Interfaces;

namespace DeskAlerts.ApplicationCore.Repositories
{
    

    public class PpUserInstancesRepository : PpRepository<UserInstances, Guid>, IUserInstancesRepository
    {
        public PpUserInstancesRepository(IConfigurationManager connectionStringBuilder) : base(connectionStringBuilder)
        {
        }

        public UserInstances GetInstance(Guid instanceGuid)
        {
            return Database.SingleOrDefault<UserInstances>("WHERE [user_instances].[clsid] = @0", instanceGuid);
        }

        public UserInstances GetInstanceById(Guid instanceGuid)
        {
            return Database.SingleOrDefault<UserInstances>("WHERE [user_instances].[id] = @0", instanceGuid);
        }


        public IEnumerable<UserInstances> GetInstances(Guid instanceId)
        {
            if (instanceId == Guid.Empty)
            {
                throw new ArgumentException("Id of entity must be not empty", nameof(instanceId));
            }

            return Database.Fetch<UserInstances>("WHERE [user_instances].[id] = @0", instanceId);
        }

        public IEnumerable<UserInstances> GetInstances(long userId)
        {
            if (userId <= 0)
            {
                throw new ArgumentException("Id of entity must be not more than zero", nameof(userId));
            }

            return Database.Fetch<UserInstances>("WHERE [user_instances].[user_id] = @0", userId);
        }

        public IEnumerable<UserInstances> GetMobileInstances()
        {
            return Database.Fetch<UserInstances>("WHERE device_type IN (2,3) AND device_id <> \'(null)\' AND device_id IS NOT NULL");
        }

        public void DeleteByUserId(long userId)
        {
            if (userId <= 0)
            {
                throw new ArgumentException("Id of entity must be not more than zero", nameof(userId));
            }

            Database.Delete<UserInstances>("WHERE [user_id] = @0", userId);
        }
    }
}