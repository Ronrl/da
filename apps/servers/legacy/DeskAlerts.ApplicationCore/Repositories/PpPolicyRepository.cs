﻿using System;
using System.Collections.Generic;
using System.Linq;
using DeskAlerts.ApplicationCore.Dtos;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Interfaces;

namespace DeskAlerts.ApplicationCore.Repositories
{
    public class PpPolicyRepository : PpRepository<Policy, long>, IPolicyRepository
    {
        public PpPolicyRepository(IConfigurationManager connectionStringBuilder) : base(connectionStringBuilder)
        {
        }

        public List<Policy> GetPoliciesByPublisherId(long userId)
        {
            if (userId == 0)
            {
                throw new ArgumentNullException(nameof(userId));
            }

            return Database.Fetch<Policy, PolicyList, PolicyViewer, PolicyPublisher>(
                @"SELECT p.*, pl.*, pv.*, pe.*
                FROM [policy] AS p
                    LEFT JOIN policy_editor AS pe ON pe.policy_id = p.id
                    INNER JOIN policy_list AS pl ON pl.policy_id = pe.policy_id
                    LEFT JOIN policy_viewer AS pv ON pv.policy_id = p.id
                WHERE pe.editor_id = @0;",
                userId);
        }

        public string GetPolicyTypeByPublisherId(long userId)
        {
            if (userId == 0)
            {
                throw new ArgumentNullException(nameof(userId));
            }

            return Database.SingleOrDefault<string>(@"SELECT type
                FROM [policy] AS p
                    LEFT JOIN policy_editor AS pe ON pe.policy_id = p.id
                WHERE pe.editor_id = @0",
                userId
            );
        }

        public IEnumerable<char> GetPoliciesTypes(long publisherId)
        {
            if (publisherId == 0)
            {
                throw new ArgumentNullException(nameof(publisherId));
            }

            return Database.Fetch<char>(@"SELECT DISTINCT type
                FROM [policy] AS p
                    LEFT JOIN policy_editor AS pe ON pe.policy_id = p.id
                WHERE pe.editor_id = @0",
                publisherId
            );
        }

        public List<Policy> GetPoliciesByUserIdAndDomainId(long userId, long domainId)
        {
            if (userId == 0)
            {
                throw new ArgumentNullException(nameof(userId));
            }

            return Database.Fetch<Policy>(
                @"SELECT p.*
                FROM [policy] AS p
                    LEFT JOIN groups AS g ON p.id = g.policy_id
                    LEFT JOIN users_groups AS ug ON ug.group_id = g.id
                WHERE g.policy_id IS NOT NULL
                    AND g.domain_id = @0
                    AND ug.user_id = @1;",
                domainId,
                userId);
        }

        public List<PolicyInfoDto> GetPolicyGroups()
        {
            var result = Database.Fetch<PolicyInfoDto>("select distinct(policy_id) as id, policy.name, policy.type from groups inner join policy on policy.type = \'A\' and groups.policy_id = policy.id ");
            if (!result.Any())
            {
                result = null;
            }

            return result;
        }

        public List<PolicyGroup> GetPolicyGroupsByPolicyId(long policyId)
        {
            if (policyId == 0)
            {
                throw new ArgumentNullException(nameof(policyId));
            }

            return Database.Fetch<PolicyGroup>(
                @"SELECT pg.*
                FROM policy_editor AS pe
                    INNER JOIN policy AS p ON p.id = pe.policy_id
                    INNER JOIN policy_group AS pg ON pg.policy_id = p.id
                WHERE p.id =  @0",
                policyId);
        }

        public List<PolicyGroup> GetPolicyGroupsByPublisherId(long publisherId)
        {
            if (publisherId == 0)
            {
                throw new ArgumentNullException(nameof(publisherId));
            }

            return Database.Fetch<PolicyGroup>(
                @"SELECT pg.*
                FROM policy_editor AS pe
                    INNER JOIN policy AS p ON p.id = pe.policy_id
                    INNER JOIN policy_group AS pg ON pg.policy_id = p.id
                WHERE pe.editor_id =  @0",
                publisherId);
        }

        public List<PolicyOU> GetPolicyOUsByPolicyId(long policyId)
        {
            if (policyId == 0)
            {
                throw new ArgumentNullException(nameof(policyId));
            }

            return Database.Fetch<PolicyOU>(
                @"SELECT pou.*
                FROM policy_editor AS pe
                    INNER JOIN policy AS p ON p.id = pe.policy_id
                    INNER JOIN policy_ou AS pou ON pou.policy_id = p.id
                WHERE p.id =  @0",
                policyId);
        }

        public List<PolicyOU> GetPolicyOUsByPublisherId(long publisherId)
        {
            if (publisherId == 0)
            {
                throw new ArgumentNullException(nameof(publisherId));
            }

            return Database.Fetch<PolicyOU>(
                @"SELECT pou.*
                FROM policy_editor AS pe
                    INNER JOIN policy AS p ON p.id = pe.policy_id
                    INNER JOIN policy_ou AS pou ON pou.policy_id = p.id
                WHERE pe.editor_id =   @0",
                publisherId);
        }

        public List<PolicyUser> GetPolicyUsersByPolicyId(long policyId)
        {
            if (policyId == 0)
            {
                throw new ArgumentNullException(nameof(policyId));
            }

            return Database.Fetch<PolicyUser>(
                @"SELECT pu.*
                FROM policy_editor AS pe
                    INNER JOIN policy AS p ON p.id = pe.policy_id
                    INNER JOIN policy_user AS pu ON pu.policy_id = p.id
                WHERE p.id = @0",
                policyId);
        }

        public List<PolicyUser> GetPolicyUsersByPublisherId(long publisherId)
        {
            if (publisherId == 0)
            {
                throw new ArgumentNullException(nameof(publisherId));
            }

            return Database.Fetch<PolicyUser>(
                @"SELECT pu.*
                FROM policy_editor AS pe
                    INNER JOIN policy AS p ON p.id = pe.policy_id
                    INNER JOIN policy_user AS pu ON pu.policy_id = p.id
                WHERE pe.editor_id =  @0",
                publisherId);
        }

        public int GetRecipientsTotal(long policyId)
        {
            if (policyId == 0)
            {
                throw new ArgumentNullException(nameof(policyId));
            }

            var usersTotal = Database.ExecuteScalar<int>(@"
                SELECT count(1)
                FROM policy_user 
                WHERE policy_id = @0",
                policyId
            );

            var groupsTotal = Database.ExecuteScalar<int>(@"
                SELECT count(1)
                FROM policy_group
                WHERE policy_id = @0",
                policyId
            );

            var ousTotal = Database.ExecuteScalar<int>(@"
                SELECT count(1)
                FROM policy_OU 
                WHERE policy_id = @0",
                policyId
            );

            var computersTotal = Database.ExecuteScalar<int>(@"
                SELECT count(1)
                FROM policy_computer 
                WHERE policy_id = @0",
                policyId
            );

            return usersTotal + groupsTotal + ousTotal + computersTotal;
        }
    }
}