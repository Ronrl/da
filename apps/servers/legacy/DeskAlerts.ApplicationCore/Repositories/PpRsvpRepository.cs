﻿using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Enums;
using DeskAlerts.ApplicationCore.Extensions.Enums;
using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.ApplicationCore.Structs;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DeskAlerts.ApplicationCore.Repositories
{
    public class PpRsvpRepository : PpRepository<AlertEntity, long>, IRsvpRepository
    {
        private readonly IAlertRepository _alertRepository;

        public PpRsvpRepository(IConfigurationManager configurationManager, IAlertRepository alertRepository) : base(configurationManager)
        {
            _alertRepository = alertRepository;
        }

        public IEnumerable<long> GetActualRsvpIds(long userId, Guid deskBarId, DateTime dateTime, string ip, string computerName)
        {
            var localDateTime = dateTime.ToLocalTime();

            return GetActualRsvpIdsByUser(userId, deskBarId, localDateTime)
                .Concat(GetActualRsvpIdsByBroadCast(deskBarId, localDateTime))
                .Distinct();
        }

        public IEnumerable<long> GetActualRsvpIds(long userId, DateTime dateTime)
        {
            var localDateTime = dateTime.ToLocalTime();

            return GetActualRsvpIdsByUser(userId, localDateTime)
                .Concat(GetActualRsvpIdsByBroadCast(localDateTime))
                .Distinct();
        }

        public IEnumerable<long> GetActualRsvpIdsByUser(long userId, Guid deskBarId, DateTime dateTime)
        {
            const string query = @"
                SELECT DISTINCT 
                       a.id
                FROM alerts AS a
                     JOIN alerts_received AS ar ON a.id = ar.alert_id
                     JOIN surveys_main AS s ON a.id = s.sender_id
                WHERE(a.to_date > @datetime
                      AND a.from_date < @datetime)
                     AND a.schedule_type = 1
                     AND a.type = @isSend
                     AND a.class = @contentType
                     AND a.approve_status != @approveStatusRejected
                     AND a.approve_status != @approveStatusNotModerated
                     AND ar.status = @isNotReceived                     
                     AND a.type2 != @isBroadcast
                     AND ar.user_id = @userIdentifier
                     AND ar.clsid = @userDeskBarId 
                     AND a.is_repeatable_template != 1;";

            var result = Database.Fetch<long>(
                query,
                new
                {
                    datetime = dateTime.ToString(DateTimeFormat.MsSqlFormat),
                    isSend = ContentState.Send.GetStringValue(),
                    contentType = AlertType.SimpleAlert,
                    approveStatusRejected = (int)ApproveStatus.Rejected,
                    approveStatusNotModerated = (int)ApproveStatus.NotModerated,
                    isNotReceived = (int)AlertReceiveStatus.NotReceived,
                    isBroadcast = ContentReceivingType.BroadCast.GetStringValue(),
                    userIdentifier = userId,
                    userDeskBarId = deskBarId
                });

            return result;
        }

        public IEnumerable<long> GetActualRsvpIdsByBroadCast(Guid deskBarId, DateTime dateTime)
        {
            const string query = @"
                SELECT DISTINCT 
                       a.id
                FROM alerts AS a
                     JOIN alerts_received AS ar ON a.id = ar.alert_id
                     JOIN surveys_main AS s ON a.id = s.sender_id
                WHERE(a.to_date > @datetime
                      AND a.from_date < @datetime)
                     AND a.schedule_type = 1
                     AND a.type = @isSend
                     AND a.class = @contentType
                     AND a.approve_status != @approveStatusRejected
                     AND a.approve_status != @approveStatusNotModerated
                     AND ar.status = @isNotReceived
                     AND a.type2 = @isBroadcast
                     AND ar.clsid = @userDeskBarId 
                     AND a.is_repeatable_template != 1;";

            var result = Database.Fetch<long>(
                query,
                new
                {
                    datetime = dateTime.ToString(DateTimeFormat.MsSqlFormat),
                    isSend = ContentState.Send.GetStringValue(),
                    contentType = AlertType.SimpleAlert,
                    approveStatusRejected = (int)ApproveStatus.Rejected,
                    approveStatusNotModerated = (int)ApproveStatus.NotModerated,
                    isNotReceived = (int)AlertReceiveStatus.NotReceived,
                    isBroadcast = ContentReceivingType.BroadCast.GetStringValue(),
                    userDeskBarId = deskBarId
                });

            return result;
        }

        public IEnumerable<long> GetActualRsvpIdsByUser(long userId, DateTime dateTime)
        {
            const string query = @"
                SELECT DISTINCT 
                       a.id
                FROM alerts AS a
                     JOIN alerts_received AS ar ON a.id = ar.alert_id
                     JOIN surveys_main AS s ON a.id = s.sender_id
                WHERE(a.to_date > @datetime
                      AND a.from_date < @datetime)
                     AND a.schedule_type = 1
                     AND a.type = @isSend
                     AND a.class = @contentType
                     AND a.approve_status != @approveStatusRejected
                     AND a.approve_status != @approveStatusNotModerated
                     AND a.type2 != @isBroadcast
                     AND ar.user_id = @userIdentifier
                     AND a.is_repeatable_template != 1;";

            var result = Database.Fetch<long>(
                query,
                new
                {
                    datetime = dateTime.ToString(DateTimeFormat.MsSqlFormat),
                    isSend = ContentState.Send.GetStringValue(),
                    contentType = AlertType.SimpleAlert,
                    approveStatusRejected = (int)ApproveStatus.Rejected,
                    approveStatusNotModerated = (int)ApproveStatus.NotModerated,
                    isBroadcast = ContentReceivingType.BroadCast.GetStringValue(),
                    userIdentifier = userId
                });

            return result;
        }

        public IEnumerable<long> GetActualRsvpIdsByBroadCast(DateTime dateTime)
        {
            const string query = @"
                SELECT DISTINCT 
                       a.id
                FROM alerts AS a
                     JOIN surveys_main AS s ON a.id = s.sender_id
                WHERE(a.to_date > @datetime
                      AND a.from_date < @datetime)
                     AND a.schedule_type = 1
                     AND a.type = @isSend
                     AND a.class = @contentType
                     AND a.approve_status != @approveStatusRejected
                     AND a.approve_status != @approveStatusNotModerated
                     AND a.type2 = @isBroadcast
                     AND a.is_repeatable_template != 1;";

            var result = Database.Fetch<long>(
                query,
                new
                {
                    datetime = dateTime.ToString(DateTimeFormat.MsSqlFormat),
                    isSend = ContentState.Send.GetStringValue(),
                    contentType = AlertType.SimpleAlert,
                    approveStatusRejected = (int)ApproveStatus.Rejected,
                    approveStatusNotModerated = (int)ApproveStatus.NotModerated,
                    isBroadcast = ContentReceivingType.BroadCast.GetStringValue()
                });

            return result;
        }

        public IEnumerable<long> GetRecipientsUsersIds(long rsvpId)
        {
            const string query = @"
                SELECT DISTINCT 
                       u.id
                FROM alerts AS a
                     JOIN alerts_received ON a.id = alerts_received.alert_id
                     JOIN users AS u ON u.id = alerts_received.user_id
                WHERE class = @0
                      AND a.type2 != @1
                      AND a.id = @2;";

            var result = Database.Fetch<long>(
                query,
                AlertType.SimpleAlert,
                ContentReceivingType.BroadCast.GetStringValue(),
                rsvpId);

            return result;
        }

        public IEnumerable<long> GetRecipientsCompsIds(long rsvpId)
        {
            const string query = @"
                SELECT c.id
                FROM alerts AS a
                     JOIN alerts_sent_comp AS sc ON sc.alert_id = a.id
                     JOIN computers AS c ON c.id = sc.comp_id
                WHERE a.schedule_type = 1
                      AND a.class = @0
                      AND a.type2 != @1
                      AND a.id = @2;";

            var result = Database.Fetch<long>(
                query,
                AlertType.SimpleAlert,
                ContentReceivingType.BroadCast.GetStringValue(),
                rsvpId);

            return result;
        }
    }
}
