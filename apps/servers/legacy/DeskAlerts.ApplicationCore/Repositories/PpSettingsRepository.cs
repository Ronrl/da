﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeskAlerts.ApplicationCore.Repositories
{
    using DeskAlerts.ApplicationCore.Entities;
    using DeskAlerts.ApplicationCore.Interfaces;

    public class PpSettingsRepository : PpRepository<Setting, long>, ISettingsRepository
    {

        public PpSettingsRepository(IConfigurationManager connectionStringBuilder) : base(connectionStringBuilder) { }

        public Setting GetByName(string paramName)
        {
            return Database.Single<Setting>("WHERE name = @0;", paramName);
        }

        public string GetStringByInt(int id)
        {
            const string query = @"
                SELECT ISNULL(
                (
                    SELECT code
                    FROM languages_list
                         JOIN users ON users.language_id = languages_list.id
                                       AND users.id = @userId
                ),
                (
                    SELECT val
                    FROM settings
                    WHERE name LIKE 'ConfDefaultLanguage'
                ));";

            var result = Database.FirstOrDefault<string>(
                query,
                new
                {
                    userId = id
                });

            return result;
        }
    }
}
