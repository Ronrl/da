﻿using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DeskAlerts.ApplicationCore.Repositories
{
    public class PpPushSentRepository : PpRepository<PushSent, int>, IPushSentRepository
    {
        public PpPushSentRepository(IConfigurationManager connectionStringBuilder) : base(connectionStringBuilder) { }

        public List<PushSent> GetBrokenPushes(int maxAtttemptCount)
        {
            return Database.Fetch<PushSent>("WHERE [status] > @0 AND [try_count] < @1", PushSendingStatus.Success, maxAtttemptCount);
        }

        public PushSent GetPushByDeviceIdAlertId(Guid deviceId, long alertId)
        {
            return Database.FirstOrDefault<PushSent>("WHERE [alert_id] = @0  AND [user_instance_id] = @1", alertId, deviceId);
        }

        public PushSent GetPushById(Guid id)
        {
            return Database.FirstOrDefault<PushSent>("WHERE [id] = @0 ", id);
        }

        public void DeleteByUserInstanceId(Guid userInstanceId)
        {
            if (userInstanceId == Guid.Empty)
            {
                throw new ArgumentException("Id of entity must be not empty", nameof(userInstanceId));
            }

            Database.Delete<PushSent>("WHERE [user_instance_id] = @0", userInstanceId);
        }

        public void InsertByAlertIdAndUserId(long contentId, IEnumerable<long> userIds)
        {
            if (contentId <= default(long))
            {
                return;
            }

            var userIdsCollection = userIds.Distinct().ToList();
            if (!userIdsCollection.Any())
            {
                return;
            }

            const int amountEntriesOnPage = 500;
            var pageCount = Math.Ceiling(userIdsCollection.Count / (double) amountEntriesOnPage);

            const string query = @"
                INSERT INTO push_sent(alert_id, sent_date, status, try_count, user_instance_id) 
	                SELECT @contentId, GETDATE(), @status, @tryCount, id 
	                FROM user_instances 
	                WHERE user_id IN (@userIdsString) AND device_id != ''";

            for (var i = 0; i < pageCount; i++)
            {
                var partOfUsers = userIdsCollection.Skip(i * amountEntriesOnPage).Take(amountEntriesOnPage);

                Database.Execute(query, new
                {
                    contentId = contentId,
                    status = (int)PushSendingStatus.NotSent,
                    tryCount = 1,
                    userIdsString = partOfUsers
                });
            }
        }

        public IEnumerable<long> GetAlertIdForSentPushes()
        {
            return GetAll().Select(x => x.AlertId).Distinct();
        }
    }
}
