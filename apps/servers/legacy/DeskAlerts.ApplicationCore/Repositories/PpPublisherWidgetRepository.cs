﻿using System;
using System.Collections.Generic;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Interfaces;

namespace DeskAlerts.ApplicationCore.Repositories
{
    public class PpPublisherWidgetRepository : PpRepository<PublisherWidget, Guid>, IPublisherWidgetRepository
    {
        public PpPublisherWidgetRepository(IConfigurationManager connectionStringBuilder) : base(connectionStringBuilder)
        {
        }

        public IEnumerable<PublisherWidget> GetByPublisherId(long publisherId)
        {
            return Database.Fetch<PublisherWidget, Widget>(
                @"SELECT we.*, w.*
                FROM [widgets_editors] AS we
                LEFT JOIN [widgets] AS w ON we.widget_id = w.id
                WHERE [editor_id] = @0",
                publisherId);
        }
    }
}