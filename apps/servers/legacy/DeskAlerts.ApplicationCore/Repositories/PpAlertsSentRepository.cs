﻿using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Interfaces;
using NLog;
using System;
using System.Collections.Generic;

namespace DeskAlerts.ApplicationCore.Repositories
{
    public class PpAlertsSentRepository : PpRepository<AlertSent, long>, IAlertsSentRepository
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public PpAlertsSentRepository(IConfigurationManager configurationManager) : base(configurationManager)
        {
        }

        public IEnumerable<AlertSent> GetAlertSentsForContent(long contentId)
        {
            try
            {
                return Database.Query<AlertSent>("WHERE [alert_id] = @0", contentId);
            }
            catch (Exception e)
            {
                _logger.Error($"Exception: {e.Message} with parameters: contentId - {contentId}");
                throw;
            }
        }
    }
}
