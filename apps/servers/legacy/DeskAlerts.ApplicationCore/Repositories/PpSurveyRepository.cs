﻿using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Enums;
using DeskAlerts.ApplicationCore.Extensions.Enums;
using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.ApplicationCore.Structs;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DeskAlerts.ApplicationCore.Repositories
{
    public class PpSurveyRepository : PpRepository<AlertEntity, long>, ISurveyRepository
    {
        private readonly IAlertRepository _alertRepository;

        public PpSurveyRepository(IConfigurationManager configurationManager, IAlertRepository alertRepository) : base(configurationManager)
        {
            _alertRepository = alertRepository;
        }

        public IEnumerable<long> GetActualSurveysIds(long userId, Guid deskBarId, DateTime dateTime, string ip, string computerName)
        {
            var localDateTime = dateTime.ToLocalTime();

            return GetActualSurveysIdsByUser(userId, deskBarId, localDateTime)
                .Concat(GetActualSurveysIdsByBroadCast(deskBarId, localDateTime))
                .Distinct();
        }

        public IEnumerable<long> GetActualSurveysIds(long userId, DateTime dateTime)
        {
            var localDateTime = dateTime.ToLocalTime();

            return GetActualSurveysIdsByUser(userId, localDateTime)
                .Concat(GetActualSurveysIdsByBroadCast(localDateTime))
                .Distinct();
        }

        public IEnumerable<long> GetActualSurveysIdsByUser(long userId, Guid deskBarId, DateTime dateTime)
        {
            const string query = @"
                SELECT DISTINCT 
                       a.id
                FROM alerts AS a
                     JOIN alerts_received AS ar ON a.id = ar.alert_id
                     JOIN surveys_main AS s ON a.id = s.sender_id
                WHERE(a.to_date >= @datetime
                      AND a.from_date <= @datetime)
                     AND a.schedule_type = 1
                     AND a.type = @isSend
                     AND (a.class = @contentTypeSurveyClassic
                          OR a.class = @contentTypeSurveyQuiz
                          OR a.class = @contentTypeSurveyPool)
                     AND a.approve_status != @approveStatusRejected
                     AND a.approve_status != @approveStatusNotModerated
                     AND ar.status = @isNotReceived
                     AND a.type2 != @isBroadcast
                     AND ar.user_id = @userIdentifier
                     AND ar.clsid = @userDeskBarId;";

            var result = Database.Fetch<long>(
                query,
                new
                {
                    datetime = dateTime.ToString(DateTimeFormat.MsSqlFormat),
                    isSend = ContentState.Send.GetStringValue(),
                    contentTypeSurveyClassic = (long)AlertType.SimpleSurvey,
                    contentTypeSurveyQuiz = (long)AlertType.SurveyQuiz,
                    contentTypeSurveyPool = (long)AlertType.SurveyPoll,
                    approveStatusRejected = (int)ApproveStatus.Rejected,
                    approveStatusNotModerated = (int)ApproveStatus.NotModerated,
                    isNotReceived = (int)AlertReceiveStatus.NotReceived,
                    isBroadcast = ContentReceivingType.BroadCast.GetStringValue(),
                    userIdentifier = userId,
                    userDeskBarId = deskBarId
                });

            return result;
        }

        public IEnumerable<long> GetActualSurveysIdsByBroadCast(Guid deskBarId, DateTime dateTime)
        {
            const string query = @"
                SELECT DISTINCT 
                       a.id
                FROM alerts AS a
                     JOIN alerts_received AS ar ON a.id = ar.alert_id
                     JOIN surveys_main AS s ON a.id = s.sender_id
                WHERE(a.to_date >= @datetime
                      AND a.from_date <= @datetime)
                     AND a.schedule_type = 1
                     AND a.type = @isSend
                     AND (a.class = @contentTypeSurveyClassic
                          OR a.class = @contentTypeSurveyQuiz
                          OR a.class = @contentTypeSurveyPool)
                     AND a.approve_status != @approveStatusRejected
                     AND a.approve_status != @approveStatusNotModerated
                     AND ar.status = @isNotReceived
                     AND a.type2 = @isBroadcast
                     AND ar.clsid = @userDeskBarId;";

            var result = Database.Fetch<long>(
                query,
                new
                {
                    datetime = dateTime.ToString(DateTimeFormat.MsSqlFormat),
                    isSend = ContentState.Send.GetStringValue(),
                    contentTypeSurveyClassic = (long)AlertType.SimpleSurvey,
                    contentTypeSurveyQuiz = (long)AlertType.SurveyQuiz,
                    contentTypeSurveyPool = (long)AlertType.SurveyPoll,
                    approveStatusRejected = (int)ApproveStatus.Rejected,
                    approveStatusNotModerated = (int)ApproveStatus.NotModerated,
                    isNotReceived = (int)AlertReceiveStatus.NotReceived,
                    isBroadcast = ContentReceivingType.BroadCast.GetStringValue(),
                    userDeskBarId = deskBarId
                });

            return result;
        }

        public IEnumerable<long> GetActualSurveysIdsByUser(long userId, DateTime dateTime)
        {
            const string query = @"
                SELECT DISTINCT 
                       a.id
                FROM alerts AS a
                     JOIN alerts_received AS ar ON a.id = ar.alert_id
                     JOIN surveys_main AS s ON a.id = s.sender_id
                WHERE(a.to_date >= @datetime
                      AND a.from_date <= @datetime)
                     AND a.schedule_type = 1
                     AND a.type = @isSend
                     AND (a.class = @contentTypeSurveyClassic
                          OR a.class = @contentTypeSurveyQuiz
                          OR a.class = @contentTypeSurveyPool)
                     AND a.approve_status != @approveStatusRejected
                     AND a.approve_status != @approveStatusNotModerated
                     AND a.type2 != @isBroadcast
                     AND ar.user_id = @userIdentifier;";

            var result = Database.Fetch<long>(
                query,
                new
                {
                    datetime = dateTime.ToString(DateTimeFormat.MsSqlFormat),
                    isSend = ContentState.Send.GetStringValue(),
                    contentTypeSurveyClassic = (long)AlertType.SimpleSurvey,
                    contentTypeSurveyQuiz = (long)AlertType.SurveyQuiz,
                    contentTypeSurveyPool = (long)AlertType.SurveyPoll,
                    approveStatusRejected = (int)ApproveStatus.Rejected,
                    approveStatusNotModerated = (int)ApproveStatus.NotModerated,
                    isBroadcast = ContentReceivingType.BroadCast.GetStringValue(),
                    userIdentifier = userId
                });

            return result;
        }

        public IEnumerable<long> GetActualSurveysIdsByBroadCast(DateTime dateTime)
        {
            const string query = @"
                SELECT DISTINCT 
                       a.id
                FROM alerts AS a
                     JOIN surveys_main AS s ON a.id = s.sender_id
                WHERE(a.to_date >= @datetime
                      AND a.from_date <= @datetime)
                     AND a.schedule_type = 1
                     AND a.type = @isSend
                     AND (a.class = @contentTypeSurveyClassic
                          OR a.class = @contentTypeSurveyQuiz
                          OR a.class = @contentTypeSurveyPool)
                     AND a.approve_status != @approveStatusRejected
                     AND a.approve_status != @approveStatusNotModerated
                     AND a.type2 = @isBroadcast;";

            var result = Database.Fetch<long>(
                query,
                new
                {
                    datetime = dateTime.ToString(DateTimeFormat.MsSqlFormat),
                    isSend = ContentState.Send.GetStringValue(),
                    contentTypeSurveyClassic = (long)AlertType.SimpleSurvey,
                    contentTypeSurveyQuiz = (long)AlertType.SurveyQuiz,
                    contentTypeSurveyPool = (long)AlertType.SurveyPoll,
                    approveStatusRejected = (int)ApproveStatus.Rejected,
                    approveStatusNotModerated = (int)ApproveStatus.NotModerated,
                    isBroadcast = ContentReceivingType.BroadCast.GetStringValue()
                });

            return result;
        }

        public IEnumerable<long> GetRecipientsUsersIds(long surveyId)
        {
            const string query = @"
                SELECT DISTINCT 
                       u.id
                FROM alerts AS a
                     JOIN alerts_received ON a.id = alerts_received.alert_id
                     JOIN users AS u ON u.id = alerts_received.user_id
                     JOIN surveys_main AS s ON s.sender_id = a.id
                WHERE(a.class = @0
                      OR a.class = @1
                      OR a.class = @2)
                     AND a.type2 != @3
                     AND a.id = @4;";

            var result = Database.Fetch<long>(
                query,
                (long)AlertType.SimpleSurvey,
                (long)AlertType.SurveyQuiz,
                (long)AlertType.SurveyPoll,
                ContentReceivingType.BroadCast.GetStringValue(),
                surveyId);

            return result;
        }

        public IEnumerable<long> GetSurveyIdBySurveyMainTableId(IEnumerable<long> surveyMainTableIds)
        {
            const string query = @"
                SELECT a.id
                FROM alerts AS a
                     JOIN surveys_main AS s ON s.sender_id = a.id
                WHERE s.id IN(@ids);";

            var result = Database.Fetch<long>(
                query,
                new
                {
                    ids = surveyMainTableIds
                });

            return result;
        }
    }
}
