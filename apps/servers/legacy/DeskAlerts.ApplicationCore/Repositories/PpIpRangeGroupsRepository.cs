﻿using System.Collections.Generic;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Interfaces;

namespace DeskAlerts.ApplicationCore.Repositories
{
    public class PpIpRangeGroupsRepository : PpRepository<IpRangeGroup, long>, IIpRangeGroupsRepository
    {
        public PpIpRangeGroupsRepository(IConfigurationManager connectionStringBuilder) : base(connectionStringBuilder)
        {
        }

        public IEnumerable<IpRangeGroup> GetByGroupId(long groupId)
        {
            return Database.Fetch<IpRangeGroup>($" WHERE group_id = {groupId}");
        }
    }
}