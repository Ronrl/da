﻿using System;
using System.Collections.Generic;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Interfaces;

namespace DeskAlerts.ApplicationCore.Repositories
{
    public class PpSkinRepository : PpRepository<Skin, Guid>, ISkinRepository
    {
        public PpSkinRepository(IConfigurationManager connectionStringBuilder) : base(connectionStringBuilder)
        {
        }

        public IEnumerable<Skin> GetSkinsByGuidList(IEnumerable<Guid> skinGuidList)
        {
            return Database.Fetch<Skin>(
                @"SELECT [alert_captions].*
                  FROM [alert_captions]
                  WHERE id IN (@0);",
                skinGuidList);
        }

        public IEnumerable<Skin> GetSkinsByPolicyId(long policyId)
        {
            return Database.Fetch<Skin>(
                @"SELECT [alert_captions].*
                  FROM [alert_captions]
                       JOIN [policy_skins] ON [policy_skins].skin_id = [alert_captions].id
                  WHERE [policy_skins].policy_id = @0;",
                policyId);
        }

        public void DeleteByPolicyId(long policyId)
        {
            Database.Execute(
                @"DELETE
                  FROM policy_skins
                  WHERE policy_id = @0;",
                policyId);
        }

        public void CreateDependencies(long policyId)
        {
            Database.Execute(
                @"INSERT INTO [policy_skins] ([policy_id], [skin_id])
                  (
                      SELECT [policy].[id] AS [policy_id],
                              [alert_captions].[id] AS [skin_id]
                      FROM [alert_captions]
                      CROSS JOIN [policy]
                      Where [policy].id = @0
                  );",
                policyId);
        }

        public void CreateSpecificDependencies(long policyId, IEnumerable<Guid> skinsIds)
        {
            try
            {
                Database.BeginTransaction();

                try
                {
                    foreach (var skinsId in skinsIds)
                    {
                        Database.Execute(
                            @"INSERT INTO [policy_skins]
                              (policy_id, skin_id )
                              VALUES
                              (@0, @1);",
                            policyId, skinsId);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }

                Database.CompleteTransaction();
            }
            catch
            {
                Database.AbortTransaction();
            }
        }

        public Guid GetSkinIdByAlertId(long alertId)
        {
            return Database.FirstOrDefault<Guid>(
                @"SELECT alerts.caption_id
                  FROM alerts
                  WHERE id = @0;",
                alertId);
        }

    }
}
