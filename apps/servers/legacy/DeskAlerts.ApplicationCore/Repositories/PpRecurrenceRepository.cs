﻿using System;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.ApplicationCore.Structs;

namespace DeskAlerts.ApplicationCore.Repositories
{
    public class PpRecurrenceRepository : PpRepository<Recurrence, long>, IRecurrenceRepository
    {
        private readonly object _recurrenceRepositoryLock = new object();

        public PpRecurrenceRepository(IConfigurationManager configurationManager) : base(configurationManager)
        {
        }

        public bool IsActualDayRecurrenceForContent(DateTime dateTime, long contentId)
        {
            const string query = @"
                DECLARE @@pattern VARCHAR(1)
                DECLARE @@number_days INT
                DECLARE @@number_week INT
                DECLARE @@week_days INT
                DECLARE @@month_day INT
                DECLARE @@number_month INT
                DECLARE @@weekday_place INT
                DECLARE @@weekday_day INT
                DECLARE @@month_val INT
                DECLARE @@dayly_selector VARCHAR(50)
                DECLARE @@monthly_selector VARCHAR(50)
                DECLARE @@yearly_selector VARCHAR(50)
                DECLARE @@end_type VARCHAR(50)
                DECLARE @@from_date DATETIME
                DECLARE @@to_date DATETIME
                DECLARE @@tmp_start_date DATETIME

                SET @@tmp_start_date = @start_datetime

                SELECT @@pattern = r.pattern,
	                @@number_days = r.number_days,
	                @@number_week = r.number_week,
	                @@week_days = r.week_days,
	                @@month_day = r.month_day,
	                @@number_month = r.number_month,
	                @@weekday_place = r.weekday_place,
	                @@weekday_day = r.weekday_day,
	                @@month_val = r.month_val,
	                @@dayly_selector = r.dayly_selector,
	                @@monthly_selector = r.monthly_selector,
	                @@yearly_selector = r.yearly_selector,
	                @@end_type = r.end_type,
	                @@from_date = a.from_date,
	                @@to_date = a.to_date
                FROM alerts a
                JOIN recurrence r
	                ON a.id = r.alert_id
                WHERE a.id = @content_id

                SELECT dbo.isActualDayRecurrence(
	                @@pattern,
	                @@number_days,
	                @@number_week,
	                @@week_days,
	                @@month_day,
	                @@number_month,
	                @@weekday_place,
	                @@weekday_day,
	                @@month_val,
	                @@dayly_selector,
	                @@monthly_selector,
	                @@yearly_selector,
	                @@end_type,
	                @@from_date,
	                @@to_date,
	                @@tmp_start_date
                )";

            lock (_recurrenceRepositoryLock)
            {
                var result = Database.ExecuteScalar<int>(
                    query,
                    new
                    {
                        start_datetime = dateTime.ToString(DateTimeFormat.MsSqlFormat),
                        content_id = contentId
                    }
                );

                return result == 1;
            }
        }
    }
}