﻿using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Interfaces;

namespace DeskAlerts.ApplicationCore.Repositories
{
    public class PpAlertReadRepository : PpRepository<AlertRead, long>, IAlertReadRepository
    {
        public PpAlertReadRepository(IConfigurationManager configurationManager) : base(configurationManager)
        {
        }
    }
}
