﻿using System;
using System.Collections.Generic;
using System.Globalization;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.ApplicationCore.Structs;
using DeskAlerts.ApplicationCore.Utilities;

namespace DeskAlerts.ApplicationCore.Repositories
{
    public class PpScheduledContentRepository : PpRepository<AlertEntity, long>, IScheduledContentRepository
    {
        public PpScheduledContentRepository(IConfigurationManager connectionStringBuilder) : base(connectionStringBuilder)
        {
        }

        public IEnumerable<AlertEntity> GetActualScheduledContent<TContent>() where TContent : IContentType
        {
            const string query = @"
                SELECT *
                FROM alerts AS a
                WHERE ((a.from_date > @0) OR (a.from_date < @0 AND @0 < a.to_date)) 
                    AND a.schedule = 1 
                    AND (a.recurrence = 0 OR a.recurrence IS NULL) 
                    AND a.class = @1;";

            var contentType = ContentTypeUtils.GetContentTypeEnum<TContent>();
            var result = Database.Fetch<AlertEntity>(
                query,
                DateTime.Now.ToString(DateTimeFormat.MsSqlFormat, CultureInfo.InvariantCulture),
                contentType);

            return result;
        }

        public IEnumerable<long> GetActualScheduledContentIdsForUser<TContent>(long userId) where TContent : IContentType
        {
            const string query = @"
                SELECT DISTINCT a.id
                FROM alerts AS a 
                    JOIN alerts_received AS ar ON a.id = ar.alert_id
                WHERE ((a.from_date > @0) OR (a.from_date < @0 AND @0 < a.to_date)) 
                    AND a.schedule = 1 
                    AND (a.recurrence = 0 OR a.recurrence IS NULL) 
                    AND a.class = @1 
                    AND ar.user_id = @2;";

            var contentType = ContentTypeUtils.GetContentTypeEnum<TContent>();
            var result = Database.Fetch<long>(
                query,
                DateTime.Now.ToString(DateTimeFormat.MsSqlFormat, CultureInfo.InvariantCulture),
                contentType,
                userId);

            return result;
        }

        public IEnumerable<long> GetActualScheduledContentIdsForUserInstance<TContent>(Guid userInstanceId) where TContent : IContentType
        {
            const string query = @"
                SELECT DISTINCT a.id
                FROM alerts AS a 
                    JOIN alerts_received AS ar ON a.id = ar.alert_id
                WHERE ((a.from_date > @0) OR (a.from_date < @0 AND @0 < a.to_date)) 
                    AND a.schedule = 1 
                    AND (a.recurrence = 0 OR a.recurrence IS NULL) 
                    AND a.class = @1 
                    AND ar.clsid = @2;";

            var contentType = ContentTypeUtils.GetContentTypeEnum<TContent>();
            var result = Database.Fetch<long>(
                query,
                DateTime.Now.ToString(DateTimeFormat.MsSqlFormat, CultureInfo.InvariantCulture),
                contentType,
                userInstanceId);

            return result;
        }

        public IEnumerable<AlertEntity> GetActualRepeatableTemplates<TContent>() where TContent : IContentType
        {
            const string query = @"
                SELECT *
                FROM alerts AS a
                WHERE ((a.from_date > @0) OR (a.from_date < @0 AND @0 < a.to_date))
                    AND a.schedule = 1
                    AND a.recurrence = 1 
                    AND a.is_repeatable_template = 1 
                    AND a.class = @1;";

            var contentType = ContentTypeUtils.GetContentTypeEnum<TContent>();
            var result = Database.Fetch<AlertEntity>(
                query,
                DateTime.Now.ToString(DateTimeFormat.MsSqlFormat, CultureInfo.InvariantCulture),
                contentType);

            return result;
        }

        public IEnumerable<long> GetActualRepeatableTemplatesIdsForUser<TContent>(long userId) where TContent : IContentType
        {
            const string query = @"
                SELECT DISTINCT a.id
                FROM alerts AS a 
                    JOIN alerts_received AS ar ON a.id = ar.alert_id
                WHERE ((a.from_date > @0) OR (a.from_date < @0 AND @0 < a.to_date))
                    AND a.schedule = 1
                    AND a.recurrence = 1 
                    AND a.is_repeatable_template = 1 
                    AND a.class = @1 
                    AND ar.user_id = @2;";

            var contentType = ContentTypeUtils.GetContentTypeEnum<TContent>();
            var result = Database.Fetch<long>(
                query,
                DateTime.Now.ToString(DateTimeFormat.MsSqlFormat, CultureInfo.InvariantCulture),
                contentType,
                userId);

            return result;
        }

        public IEnumerable<long> GetActualRepeatableTemplatesIdsForUserInstance<TContent>(Guid userInstanceId) where TContent : IContentType
        {
            const string query = @"
                SELECT DISTINCT a.id
                FROM alerts AS a 
                    JOIN alerts_received AS ar ON a.id = ar.alert_id
                WHERE ((a.from_date > @0) OR (a.from_date < @0 AND @0 < a.to_date))
                    AND a.schedule = 1
                    AND a.recurrence = 1 
                    AND a.is_repeatable_template = 1 
                    AND a.class = @1 
                    AND ar.clsid = @2;";

            var contentType = ContentTypeUtils.GetContentTypeEnum<TContent>();
            var result = Database.Fetch<long>(
                query,
                DateTime.Now.ToString(DateTimeFormat.MsSqlFormat, CultureInfo.InvariantCulture),
                contentType,
                userInstanceId);

            return result;
        }

        public void DeleteInstancesOfTemplate(long templateId)
        {
            if (templateId <= 0)
            {
                throw new ArgumentException("Id of template must be more than zero", nameof(templateId));
            }

            Database.Delete<AlertEntity>("WHERE [parent_template_id] = @0", templateId);
        }

        public void UpdateSentDateForTemplate(long templateId, DateTime sentDate)
        {
            if (templateId <= 0)
            {
                throw new ArgumentException("Id of content must be more than zero", nameof(templateId));
            }

            const string query = @"
                UPDATE alerts 
                SET sent_date = @0 
                WHERE id = @1";

            Database.Execute(
                query,
                sentDate.ToString(DateTimeFormat.MsSqlFormat, CultureInfo.InvariantCulture),
                templateId);
        }
    }
}