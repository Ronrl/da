﻿using DeskAlerts.ApplicationCore.Exceptions;

namespace DeskAlerts.ApplicationCore.Repositories
{
    using System;
    using System.Collections.Generic;
    using Entities;
    using Interfaces;

    public class PpDomainRepository : PpRepository<Domain, long>, IDomainRepository
    {
        public PpDomainRepository(IConfigurationManager connectionStringBuilder) : base(connectionStringBuilder)
        {
        }

        public List<Domain> GetAllDomains()
        {
            var domains = Database.Fetch<Domain>("WHERE name != ''");
            return domains;
        }

        public Domain GetByName(string name)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            var domain = Database.SingleOrDefault<Domain>("WHERE name = @0", name);

            //TODO: Delete this exception and check all references of 'GetByName' method
            if (domain == default(Domain))
            {
                throw new EntityNotFoundException($"Couldn't found domain with name {name}");
            }

            return domain;
        }
        
    }
}