﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Interfaces;

namespace DeskAlerts.ApplicationCore.Repositories
{
    public class PpContentSettingsRepository : PpRepository<ContentSettings, long>, IContentSettingsRepository
    {
        public PpContentSettingsRepository(IConfigurationManager configurationManager) : base(configurationManager)
        {
        }

        public IEnumerable<ContentSettings> GetContentSettingsByPolicyId(long policyId)
        {
            return Database.Fetch<ContentSettings>(
                @"SELECT [ContentSettings].*
                  FROM [Policy_ContentSettings]
                       JOIN [ContentSettings] ON [ContentSettings].[Id] = [Policy_ContentSettings].[ContentSetting_Id]
                  WHERE [Policy_ContentSettings].[Policy_Id] = @0;",
                policyId);
        }

        public void DeleteByPolicyId(long policyId)
        {
            Database.Execute(
                @"DELETE FROM [Policy_ContentSettings]
                  WHERE [Policy_ContentSettings].[Policy_Id] = @0;",
                policyId);
        }

        public void CreateDependencies(long policyId)
        {
            Database.Execute(
                @"INSERT INTO [Policy_ContentSettings] ([Policy_Id], [ContentSetting_Id])
                  (
                      SELECT [Policy].[Id] AS [Policy_Id],
                             [ContentSettings].[Id] AS [ContentSetting_Id]
                      FROM [ContentSettings]
                      CROSS JOIN [policy]
                      Where [policy].id = @0
                  );",
                policyId);
        }

        public void CreateSpecificDependencies(long policyId, IEnumerable<long> contentOptionIdsList)
        {
            try
            {
                Database.BeginTransaction();

                try
                {
                    foreach (var contentOptionId in contentOptionIdsList)
                    {
                        Database.Execute(
                            @"INSERT INTO [Policy_ContentSettings]
                                  ([Policy_Id], [ContentSetting_Id])
                              VALUES
                                  (@0, @1);",
                            policyId, contentOptionId);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }

                Database.CompleteTransaction();
            }
            catch
            {
                Database.AbortTransaction();
            }
        }
    }
}
