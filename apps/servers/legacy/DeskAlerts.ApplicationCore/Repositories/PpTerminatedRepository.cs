﻿using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Enums;
using DeskAlerts.ApplicationCore.Extensions.Enums;
using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.ApplicationCore.Structs;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DeskAlerts.ApplicationCore.Repositories
{
    public class PpTerminatedRepository : PpRepository<AlertEntity, long>, ITerminatedRepository
    {
        public PpTerminatedRepository(IConfigurationManager configurationManager) : base(configurationManager)
        {
        }

        public IEnumerable<long> GetActualTerminatedContentIds(long userId, Guid deskBarId, DateTime dateTime, string ip, string computerName)
        {
            var localDateTime = dateTime.ToLocalTime();

            return GetTerminatedContentIdsByUser(userId, deskBarId, localDateTime)
                .Concat(GetTerminatedContentIdsByBroadCast(deskBarId, localDateTime))
                .Distinct();
        }

        public IEnumerable<long> GetTerminatedContentIdsByUser(long userId, Guid deskBarId, DateTime dateTime)
        {
            const string query = @"
                SELECT DISTINCT 
                       a.id
                FROM alerts AS a
                     JOIN alerts_received AS ar ON a.id = ar.alert_id
                WHERE(a.to_date > @0
                      AND a.from_date < @0)
                     AND a.schedule_type = 0
                     AND a.type = @1
                     AND (a.class != @2
                          OR a.class != @3)
                     AND a.approve_status != @4
                     AND a.approve_status != @5
                     AND a.type2 != @6
                     AND ar.terminated = @7
                     AND ar.user_id = @8
                     AND ar.clsid = @9;";

            var result = Database.Fetch<long>(
                query,
                dateTime.ToString(DateTimeFormat.MsSqlFormat),
                ContentState.Send.GetStringValue(),
                (long)AlertType.Wallpaper,
                (long)AlertType.ScreenSaver,
                (int)ApproveStatus.Rejected,
                (int)ApproveStatus.NotModerated,
                ContentReceivingType.BroadCast.GetStringValue(),
                (int)TerminateType.NotTerminated,
                userId,
                deskBarId
                );

            return result;
        }

        public IEnumerable<long> GetTerminatedContentIdsByBroadCast(Guid deskBarId, DateTime dateTime)
        {
            const string query = @"
                SELECT DISTINCT 
                       a.id
                FROM alerts AS a
                     JOIN alerts_received AS ar ON a.id = ar.alert_id
                WHERE(a.to_date > @0
                      AND a.from_date < @0)
                     AND a.schedule_type = 0
                     AND a.type = @1
                     AND (a.class != @2
                          OR a.class != @3)
                     AND a.approve_status != @4
                     AND a.approve_status != @5
                     AND a.type2 = @6
                     AND ar.terminated = @7
                     AND ar.clsid = @8;";

            var result = Database.Fetch<long>(
                query,
                dateTime.ToString(DateTimeFormat.MsSqlFormat),
                ContentState.Send.GetStringValue(),
                (long)AlertType.Wallpaper,
                (long)AlertType.ScreenSaver,
                (int)ApproveStatus.Rejected,
                (int)ApproveStatus.NotModerated,
                ContentReceivingType.BroadCast.GetStringValue(),
                (int)TerminateType.NotTerminated,
                deskBarId
                );

            return result;
        }

        public IEnumerable<long> GetRecipientsUsersIds(long terminatedContent)
        {
            const string query = @"
                SELECT DISTINCT 
                       u.id
                FROM alerts AS a
                     JOIN alerts_received ON a.id = alerts_received.alert_id
                     JOIN users AS u ON u.id = alerts_received.user_id
                WHERE a.schedule_type = 0
                      AND (a.class != @0
                           OR a.class != @1)
                      AND a.type2 != @2
                      AND a.id = @3;";

            var result = Database.Fetch<long>(
                query,
                (long)AlertType.Wallpaper,
                (long)AlertType.ScreenSaver,
                ContentReceivingType.BroadCast.GetStringValue(),
                terminatedContent);

            return result;
        }
    }
}
