﻿namespace DeskAlerts.ApplicationCore.Repositories
{
    using DeskAlerts.ApplicationCore.Entities;
    using DeskAlerts.ApplicationCore.Interfaces;
    using System;
    using System.Collections.Generic;

    public class PpPolicyPublisherRepository : PpRepository<PolicyPublisher, long>, IPolicyPublisherRepository
    {
        public PpPolicyPublisherRepository(IConfigurationManager connectionStringBuilder) : base(connectionStringBuilder)
        {
        }

        public IEnumerable<PolicyPublisher> GetByGroupId(long groupId)
        {
            if (groupId == 0)
            {
                throw new ArgumentNullException(nameof(groupId));
            }

            return Database.Fetch<PolicyPublisher>(
                @"SELECT pe.*
                FROM policy_editor AS pe
                WHERE pe.group_id = @0;",
                groupId);
        }

        public IEnumerable<PolicyPublisher> GetByPolicyIdPublisherId(long policyId, long publisherId)
        {
            if (policyId == 0)
            {
                throw new ArgumentNullException(nameof(policyId));
            }

            if (publisherId == 0)
            {
                throw new ArgumentNullException(nameof(publisherId));
            }

            return Database.Fetch<PolicyPublisher>(
                @"SELECT pe.*
                FROM policy_editor AS pe
                WHERE pe.editor_id = @0
                    AND pe.policy_id = @1;",
                publisherId, policyId);
        }

        public PolicyPublisher GetByPolicyIdPublisherIdGroupId(long policyId, long publisherId, long groupId)
        {
            if (policyId == 0)
            {
                throw new ArgumentNullException(nameof(policyId));
            }

            if (publisherId == 0)
            {
                throw new ArgumentNullException(nameof(publisherId));
            }

            if (groupId == 0)
            {
                throw new ArgumentNullException(nameof(groupId));
            }

            return Database.Single<PolicyPublisher>(
                @"SELECT pe.*
                FROM policy_editor AS pe
                WHERE pe.group_id = @0
                    AND pe.editor_id = @1
                    AND pe.policy_id = @2;",
                groupId, publisherId, policyId);
        }

        public IEnumerable<PolicyPublisher> GetByPublisherId(long publisherId)
        {
            if (publisherId == 0)
            {
                throw new ArgumentNullException(nameof(publisherId));
            }

            return Database.Fetch<PolicyPublisher>(
                @"SELECT pe.*
                FROM policy_editor AS pe
                WHERE pe.editor_id = @0;",
                publisherId);
        }
    }
}