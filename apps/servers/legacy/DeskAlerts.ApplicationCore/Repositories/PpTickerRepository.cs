﻿using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Enums;
using DeskAlerts.ApplicationCore.Extensions.Enums;
using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.ApplicationCore.Structs;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DeskAlerts.ApplicationCore.Repositories
{
    public class PpTickerRepository : PpRepository<AlertEntity, long>, ITickerRepository
    {
        public PpTickerRepository(IConfigurationManager configurationManager) : base(configurationManager)
        {
        }

        public IEnumerable<long> GetActualTickersIds(long userId, Guid deskBarId, DateTime dateTime, string ip, string computerName)
        {
            var localDateTime = dateTime.ToLocalTime();

            return GetTickersIdsByUser(userId, deskBarId, localDateTime)
                .Concat(GetTickersIdsByBroadCast(deskBarId, localDateTime))
                .Distinct();
        }

        public IEnumerable<long> GetActualTickersIds(long userId, DateTime dateTime)
        {
            var localDateTime = dateTime.ToLocalTime();

            return GetTickersIdsByUser(userId, localDateTime)
                .Concat(GetTickersIdsByBroadCast(localDateTime))
                .Distinct();
        }

        public IEnumerable<long> GetTickersIdsByUser(long userId, Guid deskBarId, DateTime dateTime)
        {
            const string query = @"
                SELECT DISTINCT 
                       a.id
                FROM alerts AS a
                     JOIN alerts_received AS ar ON a.id = ar.alert_id
                WHERE(a.to_date > @datetime
                      AND a.from_date < @datetime)
                     AND a.schedule_type = 1
                     AND a.type = @isSend
                     AND a.class = @contentType
                     AND a.approve_status != @approveStatusRejected
                     AND a.approve_status != @approveStatusNotModerated
                     AND a.ticker = @isTicker
                     AND a.type2 != @isBroadcast
                     AND ar.status = @isNotReceived
                     AND ar.user_id = @userIdentifier
                     AND ar.clsid = @userDeskBarId 
                     AND a.is_repeatable_template != 1;";

            var result = Database.Fetch<long>(
                query,
                new
                {
                    datetime = dateTime.ToString(DateTimeFormat.MsSqlFormat),
                    isSend = ContentState.Send.GetStringValue(),
                    contentType = AlertType.SimpleAlert,
                    approveStatusRejected = (int)ApproveStatus.Rejected,
                    approveStatusNotModerated = (int)ApproveStatus.NotModerated,
                    isTicker = 1,
                    isBroadcast = ContentReceivingType.BroadCast.ToString(),
                    isNotReceived = (int)AlertReceiveStatus.NotReceived,
                    userIdentifier = userId,
                    userDeskBarId = deskBarId
                });

            return result;
        }

        public IEnumerable<long> GetTickersIdsByBroadCast(Guid deskBarId, DateTime dateTime)
        {
            const string query = @"
                SELECT DISTINCT 
                       a.id
                FROM alerts AS a
                     JOIN alerts_received AS ar ON a.id = ar.alert_id
                WHERE(a.to_date > @datetime
                      AND a.from_date < @datetime)
                     AND a.schedule_type = 1
                     AND a.type = @isSend
                     AND a.class = @contentType
                     AND a.approve_status != @approveStatusRejected
                     AND a.approve_status != @approveStatusNotModerated
                     AND a.ticker = @isTicker
                     AND a.type2 = @isBroadcast
                     AND ar.status = @isNotReceived
                     AND ar.clsid = @userDeskBarId 
                     AND a.is_repeatable_template != 1;";

            var result = Database.Fetch<long>(
                query,
                new
                {
                    datetime = dateTime.ToString(DateTimeFormat.MsSqlFormat),
                    isSend = ContentState.Send.GetStringValue(),
                    contentType = AlertType.SimpleAlert,
                    approveStatusRejected = (int)ApproveStatus.Rejected,
                    approveStatusNotModerated = (int)ApproveStatus.NotModerated,
                    isTicker = 1,
                    isBroadcast = ContentReceivingType.BroadCast.ToString(),
                    isNotReceived = (int)AlertReceiveStatus.NotReceived,
                    userDeskBarId = deskBarId
                });

            return result;
        }

        public IEnumerable<long> GetTickersIdsByUser(long userId, DateTime dateTime)
        {
            const string query = @"
                SELECT DISTINCT 
                       a.id
                FROM alerts AS a
                     JOIN alerts_received AS ar ON a.id = ar.alert_id
                WHERE(a.to_date > @datetime
                      AND a.from_date < @datetime)
                     AND a.schedule_type = 1
                     AND a.type = @isSend
                     AND a.class = @contentType
                     AND a.approve_status != @approveStatusRejected
                     AND a.approve_status != @approveStatusNotModerated
                     AND a.ticker = @isTicker
                     AND a.type2 != @isBroadcast
                     AND ar.user_id = @userIdentifier
                     AND a.is_repeatable_template != 1;";

            var result = Database.Fetch<long>(
                query,
                new
                {
                    datetime = dateTime.ToString(DateTimeFormat.MsSqlFormat),
                    isSend = ContentState.Send.GetStringValue(),
                    contentType = AlertType.SimpleAlert,
                    approveStatusRejected = (int)ApproveStatus.Rejected,
                    approveStatusNotModerated = (int)ApproveStatus.NotModerated,
                    isTicker = 1,
                    isBroadcast = ContentReceivingType.BroadCast.ToString(),
                    userIdentifier = userId
                });

            return result;
        }

        public IEnumerable<long> GetTickersIdsByBroadCast(DateTime dateTime)
        {
            const string query = @"
                SELECT DISTINCT 
                       a.id
                FROM alerts AS a
                WHERE(a.to_date > @datetime
                      AND a.from_date < @datetime)
                     AND a.schedule_type = 1
                     AND a.type = @isSend
                     AND a.class = @contentType
                     AND a.approve_status != @approveStatusRejected
                     AND a.approve_status != @approveStatusNotModerated
                     AND a.ticker = @isTicker
                     AND a.type2 = @isBroadcast
                     AND a.is_repeatable_template != 1;";

            var result = Database.Fetch<long>(
                query,
                new
                {
                    datetime = dateTime.ToString(DateTimeFormat.MsSqlFormat),
                    isSend = ContentState.Send.GetStringValue(),
                    contentType = AlertType.SimpleAlert,
                    approveStatusRejected = (int)ApproveStatus.Rejected,
                    approveStatusNotModerated = (int)ApproveStatus.NotModerated,
                    isTicker = 1,
                    isBroadcast = ContentReceivingType.BroadCast.ToString()
                });

            return result;
        }

        public IEnumerable<long> GetRecipientsUsersIds(long tickerId)
        {
            const string query = @"
                SELECT DISTINCT 
                    u.id
                FROM alerts AS a
                    JOIN alerts_received ON a.id = alerts_received.alert_id
                    JOIN users AS u ON u.id = alerts_received.user_id
                WHERE class = @0
                    AND a.type2 != @1 
                    AND a.id = @2;";

            var result = Database.Fetch<long>(
                query,
                AlertType.SimpleAlert,
                ContentReceivingType.BroadCast.GetStringValue(),
                tickerId);

            return result;
        }
    }
}
