﻿using System;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Interfaces;

namespace DeskAlerts.ApplicationCore.Repositories
{
    public class PpWidgetRepository : PpRepository<Widget, Guid>, IWidgetRepository
    {
        public PpWidgetRepository(IConfigurationManager connectionStringBuilder) : base(connectionStringBuilder)
        {
        }
    }
}