﻿using System.Linq;

namespace DeskAlerts.ApplicationCore.Repositories
{
    using System.Collections.Generic;
    using DeskAlerts.ApplicationCore.Entities;
    using DeskAlerts.ApplicationCore.Interfaces;

    public class PpCampaignRepository : PpRepository<Campaign, long>, ICampaignRepository
    {
        public PpCampaignRepository(IConfigurationManager connectionStringBuilder) : base(connectionStringBuilder) { }

        public IEnumerable<Campaign> GetNearestCampaigns()
        {
            return Database.Fetch<Campaign>(
                $"WHERE DATEADD(HOUR, 1, [start_date]) > GETDATE()" +
                $"      AND is_active = 1" +
                $"      AND [start_date] IS NOT NULL" +
                $"           AND(end_date > GETDATE()" +
                $"           OR end_date IS NULL)");
        }

        public IEnumerable<Campaign> GetActiveCampaigns()
        {
            return GetAll().Where(x => x.Status == CampaignStatus.Active);
        }
    }
}