﻿using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Enums;
using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.ApplicationCore.Structs;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DeskAlerts.ApplicationCore.Repositories
{
    public class PpAlertReceivedRepository : PpRepository<AlertReceived, long>, IAlertReceivedRepository
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public PpAlertReceivedRepository(IConfigurationManager connectionStringBuilder) : base(connectionStringBuilder) { }

        public AlertReceived GetByAlertIdAndUserId(long alertId, long userId, string deskbarId)
        {
            try
            {
                return Database.Single<AlertReceived>("WHERE [alert_id] = @0 AND [user_id] = @1 AND [clsid] = @2", alertId, userId, deskbarId);
            }
            catch (Exception e)
            {
                _logger.Error($"Exception: {e.Message} with parameters: alertId - {alertId}, userId - {userId}, deskbarId - {deskbarId}");
                return null;
            }
        }

        public bool IsAlertReceived(long alertId, long userId, string deskbarId)
        {
            return Database.Exists<AlertReceived>("WHERE [alert_id] = @0 AND [user_id] = @1 AND [clsid] = @2", alertId, userId, deskbarId);
        }

        public bool IsTerminateCommandReceived(long alertId, long userId, string deskbarId)
        {
            var entity = Database
                .SingleOrDefault<AlertReceived>(@"WHERE [alert_id] = @0 AND [user_id] = @1 AND [clsid] = @2", alertId, userId, deskbarId);

            if (entity == null)
            {
                return false;
            }

            return entity.TerminateStatus == 1;
        }

        public int GetVotesCount(long alertId)
        {
            return Database.Query<AlertReceived>($@"
                SELECT alert_id, user_id
                FROM alerts_received
                WHERE vote = 1  AND alert_id = {alertId}
                GROUP BY alert_id, user_id").Count();
        }

        public List<AlertReceived> GetSendedForUser(long userId)
        {
            return Database.Query<AlertReceived>($@"
                SELECT DISTINCT alert_id 
                FROM alerts_received
                WHERE user_id = {userId}").ToList();
        }

        public List<AlertReceived> GetReceivedForUser(long userId)
        {
            return Database.Query<AlertReceived>($@"
                SELECT DISTINCT alert_id 
                FROM alerts_received
                WHERE user_id = {userId} AND status = 1").ToList();
        }

        public IEnumerable<AlertReceived> GetAlertsReceivedForContent(long contentId)
        {
            return Database.Fetch<AlertReceived>($@"
                SELECT DISTINCT alerts_received.user_id
                FROM alerts_received
                WHERE alert_id = {contentId}");
        }

        public IEnumerable<AlertReceived> GetAlertsReceivedForContentExtended(long contentId)
        {
            return Database.Fetch<AlertReceived>($@"
                SELECT DISTINCT alerts_received.*
                FROM alerts_received
                WHERE alert_id = {contentId}");
        }

        public IEnumerable<AlertReceived> GetReceivedForContent(long contentId)
        {
            return Database.Fetch<AlertReceived>($@"
                SELECT DISTINCT alerts_received.user_id
                FROM alerts_received
                WHERE alert_id = {contentId}
                AND status = {(int)AlertReceiveStatus.Received}"
            );
        }

        public void CreateForRecipientsOfBroadcastContent(long alertId, string deviceTypeScript)
        {
            const string query = @"
                INSERT INTO alerts_received (user_id, clsid, alert_id, date, status)
                SELECT u.id, ui.clsid, @0, @1, @2
                FROM users AS u
                INNER JOIN user_instances AS ui ON u.id = ui.user_id
                WHERE NOT EXISTS ( 
	                SELECT *
	                FROM alerts_received AS ar
	                WHERE ar.user_id = u.id AND ar.clsid = ui.id AND ar.alert_id = @0)";

            Database.Execute(query + deviceTypeScript,
                alertId,
                DateTime.Now.ToString(DateTimeFormat.MsSqlFormat),
                AlertReceiveStatus.NotReceived);
        }

        public IEnumerable<AlertReceived> GetUserAndAlertIdsWithSmsToSend()
        {
            const string query = @"
                SELECT DISTINCT 
                       user_id, 
                       alert_id
                FROM alerts_sent
                WHERE alert_id IN
                (
                    SELECT id
                    FROM alerts
                    WHERE sms = 1
                          AND alert_id NOT IN
                    (
                        SELECT alert_id
                        FROM sms_sent
                    )
                          AND from_date < @dateTimeNow
                          AND to_date > @dateTimeNow
                          AND approve_status IN(@approveStatusApprovedByDefault, @approveStatusApprovedByModerator)
                    AND schedule_type <> 0
                );";
            
            var result = Database.Fetch<AlertReceived>(query,
                new
                {
                    dateTimeNow = DateTime.Now,
                    approveStatusApprovedByDefault = (int)ApproveStatus.ApprovedByDefault,
                    approveStatusApprovedByModerator = (int)ApproveStatus.ApprovedByModerator
                }
            );
            return result;
        }

        public IEnumerable<AlertReceived> GetUserAndAlertIdToEmailSend()
        {
            const string query = @"
                SELECT DISTINCT 
                       als.user_id, 
                       als.alert_id
                FROM alerts_sent AS als
                     JOIN alerts AS a ON a.id = als.alert_id
                     LEFT JOIN email_sent AS es ON es.alert_id = als.alert_id
                WHERE a.email = '1'
                      AND a.from_date <= @dateTimeNow
                      AND a.to_date >= @dateTimeNow
                      AND a.schedule_type != 0
                      AND (a.terminated != @terminated
                           OR a.terminated IS NULL)
                      AND (a.approve_status = @approveStatusApprovedByDefault
                           OR a.approve_status = @approveStatusApprovedByModerator)
                      AND es.alert_id IS NULL;";

            var result = Database.Fetch<AlertReceived>(
                query,
                new
                {
                    dateTimeNow = DateTime.Now,
                    terminated = (int)TerminateType.Terminated,
                    approveStatusApprovedByDefault = (int)ApproveStatus.ApprovedByDefault,
                    approveStatusApprovedByModerator = (int)ApproveStatus.ApprovedByModerator
                });

            return result;
        }

        public IEnumerable<string> GetUsernamesForPushesByAlertId(long alertId)
        {
            return Database.Fetch<string>($@"SELECT name
                FROM users
                     INNER JOIN user_instances AS ui ON ui.user_id = users.id
                     INNER JOIN
                (
                    SELECT DISTINCT 
                           user_id, 
                           alert_id
                    FROM alerts_received
                ) AS ar ON ar.user_id = users.id
                WHERE(ui.device_type = 2
                      OR ui.device_type = 3)
                     AND ar.alert_id = {alertId};");
        }
    }
}
