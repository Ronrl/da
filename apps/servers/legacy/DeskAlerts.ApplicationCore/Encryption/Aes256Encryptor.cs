﻿namespace DeskAlerts.ApplicationCore.Encryption
{
    using System;
    using System.IO;
    using System.Security.Cryptography;
    using System.Text;

    public class Aes256Encryptor
    {
        private const string Iv = "4276235117979925";

        public static string EncryptData(string data, string key)
        {
            var dataBytes = Encoding.UTF8.GetBytes(data);
            using (var encryptor = Rijndael.Create())
            {
                var keyBytes = Encoding.ASCII.GetBytes(key);
                var preparedKey = new byte[32];
                encryptor.KeySize = 128;
                encryptor.Mode = CipherMode.CFB;
                encryptor.IV = Encoding.ASCII.GetBytes(Iv);
                encryptor.Key = new byte[32];
                Array.Copy(keyBytes, preparedKey, keyBytes.Length < 32 ? keyBytes.Length : preparedKey.Length);
                encryptor.Key = preparedKey;
                using (var ms = new MemoryStream())
                {
                    using (var cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(dataBytes, 0, dataBytes.Length);
                        cs.Close();
                    }

                    Array.Copy(ms.ToArray(), dataBytes, dataBytes.Length);
                    return Convert.ToBase64String(dataBytes);
                }
            }
        }
    }
}