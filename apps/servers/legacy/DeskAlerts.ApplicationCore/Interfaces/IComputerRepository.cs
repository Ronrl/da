﻿using System.Collections.Generic;
using DeskAlerts.ApplicationCore.Entities;

namespace DeskAlerts.ApplicationCore.Interfaces
{
    public interface IComputerRepository : IRepository<Computer, long>
    {
        Computer GetByNameAndDomainId(string name, long domainId);

        IEnumerable<long> GetUserIdsByComputerNames(IEnumerable<string> computerNames);
    }
}
