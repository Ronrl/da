﻿namespace DeskAlerts.ApplicationCore.Interfaces
{
    using System.Collections.Generic;
    using DeskAlerts.ApplicationCore.Dtos;

    public interface IAzureAdClient
    {
        List<AzureAdGroupMember> GetGroupMembers(AzureAdGroup group);
        List<AzureAdGroup> GetGroups();
        List<AzureAdUser> GetUsers();
    }
}