﻿using System.Collections.Generic;
using DeskAlerts.ApplicationCore.Entities;

namespace DeskAlerts.ApplicationCore.Interfaces
{
    public interface IUserRepository : IRepository<User, long>
    {
        void AddUserToGroup(User child, Group parent);
        List<User> GetChildUsersByDomainName(string domainName);
        List<User> GetChildUsersByGroupName(string groupName);
        User GetUserByDomainNameAndUserName(string domainName, string userName);
        User GetUserByUserNameAndMd5Passowrd(string userName, string md5Password);
        IEnumerable<string> GetDomainUsersUsernames(int domainId);
        string GetUserDomainNameByUserId(long userId);
        IEnumerable<User> GetPublishersByPolicyId(long policyId);
        User GetPublisher(string userName);
        int GetActivePublishersAmount();
        int GetMaxPublishersAmount();
        T GetPublisherParameterByName<T>(string parameter, string name);
        void RemoveUserFromGroup(User child, Group parent);
        void DisableLastOnlinePublishers(int amount);
        void DisableLastOnlinePublishers(int amount, IEnumerable<string> allowedStatuses);
        void DisablePublishers(IEnumerable<string> publishersIds);
        void EnablePublisher(string name);
        void EnablePublishers(IEnumerable<string> publishersIds);
    }
}