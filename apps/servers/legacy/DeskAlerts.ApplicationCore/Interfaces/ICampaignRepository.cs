﻿namespace DeskAlerts.ApplicationCore.Interfaces
{
    using System.Collections.Generic;
    using DeskAlerts.ApplicationCore.Entities;

    public interface ICampaignRepository : IRepository<Campaign, long>
    {
        IEnumerable<Campaign> GetNearestCampaigns();
        IEnumerable<Campaign> GetActiveCampaigns();
    }
}