﻿using System.Collections.Generic;
using DeskAlerts.ApplicationCore.Entities;

namespace DeskAlerts.ApplicationCore.Interfaces
{
    public interface IIpRangeGroupsRepository : IRepository<IpRangeGroup, long>
    {
        IEnumerable<IpRangeGroup> GetByGroupId(long groupId);
    }
}
