﻿namespace DeskAlerts.ApplicationCore.Interfaces
{
    using DeskAlerts.ApplicationCore.Entities;

    public interface IReferralsRepository : IRepository<Referral, long>
    {

    }
}