﻿namespace DeskAlerts.ApplicationCore.Interfaces
{
    using System.Collections.Generic;
    using Entities;

    public interface IDomainRepository : IRepository<Domain, long>
    {
        Domain GetByName(string name);
        List<Domain> GetAllDomains();
    }
}