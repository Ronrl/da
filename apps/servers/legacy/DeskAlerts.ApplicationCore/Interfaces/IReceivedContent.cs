﻿namespace DeskAlerts.ApplicationCore.Interfaces
{
    using Enums;

    public interface IReceivedContent
    {
        long Id { get; set; }
        AlertReceiveStatus ReceivedStatus { get; set; }
    }
}
