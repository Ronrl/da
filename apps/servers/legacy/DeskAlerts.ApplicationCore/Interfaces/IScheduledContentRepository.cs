﻿using System;
using System.Collections.Generic;
using DeskAlerts.ApplicationCore.Entities;

namespace DeskAlerts.ApplicationCore.Interfaces
{
    public interface IScheduledContentRepository : IRepository<AlertEntity, long>
    {
        IEnumerable<AlertEntity> GetActualScheduledContent<TContent>() where TContent : IContentType;
        IEnumerable<long> GetActualScheduledContentIdsForUser<TContent>(long userId) where TContent : IContentType;
        IEnumerable<long> GetActualScheduledContentIdsForUserInstance<TContent>(Guid userInstanceId) where TContent : IContentType;
        IEnumerable<AlertEntity> GetActualRepeatableTemplates<TContent>() where TContent : IContentType;
        IEnumerable<long> GetActualRepeatableTemplatesIdsForUser<TContent>(long userId) where TContent : IContentType;
        IEnumerable<long> GetActualRepeatableTemplatesIdsForUserInstance<TContent>(Guid userInstanceId) where TContent : IContentType;
        void DeleteInstancesOfTemplate(long templateId);
        void UpdateSentDateForTemplate(long templateId, DateTime sentDate);
    }
}