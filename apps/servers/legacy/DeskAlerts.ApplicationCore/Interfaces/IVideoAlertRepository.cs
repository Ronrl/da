﻿using DeskAlerts.ApplicationCore.Entities;
using System;
using System.Collections.Generic;

namespace DeskAlerts.ApplicationCore.Interfaces
{
    public interface IVideoAlertRepository : IRepository<AlertEntity, long>
    {
        IEnumerable<long> GetActualVideoAlertsIds(long userId, Guid deskBarId, DateTime dateTime);
        IEnumerable<long> GetActualVideoAlertsIds(long userId, DateTime dateTime);
        IEnumerable<long> GetRecipientsUsersIds(long tickerId);
    }
}
