﻿using DeskAlerts.ApplicationCore.Entities;
using System;
using System.Collections.Generic;

namespace DeskAlerts.ApplicationCore.Interfaces
{
    public interface IAlertRepository : IRepository<AlertEntity, long>
    {
        List<AlertEntity> GetAlertsByCampaignIdMappedWithUser(long campaignId);
        List<AlertEntity> GetByCampaignId(long campaignId);
        long GetAlertsCount();
        long GetLastAlertId();

        #region Content delivering system

        IEnumerable<long> GetActualAlertIds(long userId, Guid deskBarId, DateTime dateTime, string ip, string computerName);
        IEnumerable<long> GetRecipientsUsersIds(long alertId);

        AlertType GetContentTypeByContentId(long contentId);

        IEnumerable<AlertEntity> GetScheduledActiveAlerts();
        IEnumerable<AlertEntity> GetApprovedActiveAlerts();

        #endregion

        IEnumerable<long> GetActualAlertIds(long userId, DateTime dateTime);
    }
}