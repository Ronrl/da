﻿using System;
using System.Collections.Generic;
using DeskAlerts.ApplicationCore.Entities;

namespace DeskAlerts.ApplicationCore.Interfaces
{
    public interface IScreenSaverRepository : IRepository<AlertEntity, long>
    {
        IEnumerable<long> GetActualScreenSaversIds(long userId, Guid deskBarId, DateTime dateTime, string ip, string computerName);
        IEnumerable<long> GetActualScreenSaversIds(long userId, DateTime dateTime);
        IEnumerable<long> GetRecipientsUsersIds(long screensaverId);
        IEnumerable<long> GetRecipientsCompsIds(long screensaverId);
        IEnumerable<AlertEntity> GetActualSchedulingScreensavers();
    }
}
