﻿using System;
using System.Collections.Generic;
using DeskAlerts.ApplicationCore.Entities;

namespace DeskAlerts.ApplicationCore.Interfaces
{
    public interface ITickerRepository : IRepository<AlertEntity, long>
    {
        IEnumerable<long> GetActualTickersIds(long userId, Guid deskBarId, DateTime dateTime, string ip, string computerName);
        IEnumerable<long> GetActualTickersIds(long userId, DateTime dateTime);
        IEnumerable<long> GetRecipientsUsersIds(long tickerId);
    }
}