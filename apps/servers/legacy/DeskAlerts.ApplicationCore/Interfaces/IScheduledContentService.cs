﻿namespace DeskAlerts.ApplicationCore.Interfaces
{
    public interface IScheduledContentService
    {
        /// <summary>
        /// Create instance of repeatable content from template
        /// </summary>
        /// <param name="templateId"></param>
        /// <returns>Id of new instance of repeatable content</returns>
        long CreateInstanceFromTemplate(long templateId);

        /// <summary>
        /// Delete all instances of template
        /// </summary>
        /// <param name="templateId"></param>
        /// <returns></returns>
        bool DeleteInstancesOfTemplate(long templateId);
    }
}