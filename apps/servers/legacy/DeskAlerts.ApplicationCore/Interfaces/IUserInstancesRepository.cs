﻿using System;
using System.Collections.Generic;
using DeskAlerts.ApplicationCore.Entities;

namespace DeskAlerts.ApplicationCore.Interfaces
{
    public interface IUserInstancesRepository : IRepository<UserInstances, Guid>
    {
        IEnumerable<UserInstances> GetInstances(Guid instanceId);

        IEnumerable<UserInstances> GetInstances(long userId);

        UserInstances GetInstance(Guid instanceGuid);

        IEnumerable<UserInstances> GetMobileInstances();

        UserInstances GetInstanceById(Guid instanceGuid);
        void DeleteByUserId(long userId);
    }
}