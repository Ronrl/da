﻿namespace DeskAlerts.ApplicationCore.Interfaces
{
    using DeskAlerts.ApplicationCore.Entities;
    using System.Collections.Generic;

    public interface IPolicyPublisherRepository : IRepository<PolicyPublisher, long>
    {
        IEnumerable<PolicyPublisher> GetByPublisherId(long publisherId);
        IEnumerable<PolicyPublisher> GetByGroupId(long groupId);
        PolicyPublisher GetByPolicyIdPublisherIdGroupId(long policyId, long publisherId, long groupId);
        IEnumerable<PolicyPublisher> GetByPolicyIdPublisherId(long policyId, long publisherId);
    }
}