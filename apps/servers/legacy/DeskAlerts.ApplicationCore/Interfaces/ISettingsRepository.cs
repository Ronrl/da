﻿namespace DeskAlerts.ApplicationCore.Interfaces
{
    using Entities;
    public interface ISettingsRepository : IRepository<Setting, long>
    {
        Setting GetByName(string paramName);

        string GetStringByInt(int id);

    }
}
