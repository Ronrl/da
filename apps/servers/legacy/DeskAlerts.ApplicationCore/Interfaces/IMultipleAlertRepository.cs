﻿using System.Collections.Generic;
using DeskAlerts.ApplicationCore.Entities;

namespace DeskAlerts.ApplicationCore.Interfaces
{
    public interface IMultipleAlertRepository : IRepository<MultipleAlert, int>
    {
        // Allows empty values if the user has been deleted (need for correct statistic)
        IEnumerable<string> GetConfirmedUsernames(long alertId);

        bool CheckIfMultiple(long alertId);
    }
}