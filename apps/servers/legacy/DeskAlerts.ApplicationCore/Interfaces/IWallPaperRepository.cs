﻿using System;
using System.Collections.Generic;
using DeskAlerts.ApplicationCore.Entities;

namespace DeskAlerts.ApplicationCore.Interfaces
{
    public interface IWallPaperRepository : IRepository<AlertEntity, long>
    {
        IEnumerable<long> GetActualWallPapersIds(long userId, Guid deskBarId, DateTime dateTime, string ip, string computerName);
        IEnumerable<long> GetActualWallPapersIds(long userId, DateTime dateTime);
        IEnumerable<long> GetRecipientsUsersIds(long wallpaperId);
        IEnumerable<long> GetRecipientsCompsIds(long wallpaperId);
        IEnumerable<AlertEntity> GetActualSchedulingWallpapers();
    }
}
