﻿using System.Collections.Generic;

namespace DeskAlerts.ApplicationCore.Interfaces
{
    public interface ITimeZoneRepository
    {
        IEnumerable<int> GetAllTimeZones();
    }
}