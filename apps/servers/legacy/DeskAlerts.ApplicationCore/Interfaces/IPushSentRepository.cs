﻿using System;
using System.Collections.Generic;
using DeskAlerts.ApplicationCore.Entities;

namespace DeskAlerts.ApplicationCore.Interfaces
{
    public interface IPushSentRepository : IRepository<PushSent, int>
    {
        List<PushSent> GetBrokenPushes( int maxAtttemptCount );
        PushSent GetPushByDeviceIdAlertId( Guid deviceId, long alertid );
        void DeleteByUserInstanceId(Guid userInstanceId);
        void InsertByAlertIdAndUserId(long contentId, IEnumerable<long> userId);
        IEnumerable<long> GetAlertIdForSentPushes();
    }
}
