﻿namespace DeskAlerts.ApplicationCore.Interfaces
{
    public interface IConfigurationManager
    {
        string GetAppSetting(string key);
        string DatabaseName { get; }
        string DataSource { get; }
        bool IsWindowsAuthentication { get; }
        string Password { get; }
        string User { get; }
        string GetConnectionString();
    }
}
