﻿using System.Collections.Generic;
using DeskAlerts.ApplicationCore.Entities;

namespace DeskAlerts.ApplicationCore.Interfaces
{
    public interface IContentSettingsRepository : IRepository<ContentSettings, long>
    {
        IEnumerable<ContentSettings> GetContentSettingsByPolicyId(long policyId);

        void DeleteByPolicyId(long policyId);

        void CreateDependencies(long policyId);

        void CreateSpecificDependencies(long policyId, IEnumerable<long> contentOptionIdsList);
    }
}
