﻿using System.Collections.Generic;
using DeskAlerts.ApplicationCore.Entities;

namespace DeskAlerts.ApplicationCore.Interfaces
{
    public interface IAlertSentStatRepository : IRepository<AlertSentStat, long>
    {
        IEnumerable<AlertSentStat> GetAllByContentId(long contentId);
    }
}
