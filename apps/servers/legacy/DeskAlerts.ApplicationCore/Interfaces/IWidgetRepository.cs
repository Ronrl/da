﻿using System;
using DeskAlerts.ApplicationCore.Entities;

namespace DeskAlerts.ApplicationCore.Interfaces
{
    public interface IWidgetRepository : IRepository<Widget, Guid>
    {
    }
}