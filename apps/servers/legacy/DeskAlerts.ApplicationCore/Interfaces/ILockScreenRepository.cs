﻿using System;
using System.Collections.Generic;
using DeskAlerts.ApplicationCore.Entities;

namespace DeskAlerts.ApplicationCore.Interfaces
{
    public interface ILockScreenRepository : IRepository<AlertEntity, long>
    {
        IEnumerable<long> GetActualLockScreensIds(long userId, Guid deskBarId, DateTime dateTime, string ip, string computerName);
        IEnumerable<long> GetActualLockScreensIds(long userId, DateTime dateTime);
        IEnumerable<long> GetRecipientsUsersIds(long lockscreenId);
        IEnumerable<long> GetRecipientsCompsIds(long lockscreenId);
        IEnumerable<AlertEntity> GetActualSchedulingLockscreens();
    }
}
