﻿namespace DeskAlerts.ApplicationCore.Interfaces
{
    using DeskAlerts.ApplicationCore.Entities;
    using System.Collections.Generic;

    public interface IConfigurationService
    {
        void UpdateAuthentication(bool isWindowsAuthEnabled);

        void SetGroups(IEnumerable<Group> groups);

        //void RestartSite();
    }
}
