﻿namespace DeskAlerts.ApplicationCore.Interfaces
{
    using System.Collections.Generic;
    using Entities;

    public interface IRepository<T, K>
        where T : EntityBase<K>
    {
        K Create(T entity);
        void Delete(T entity);
        void Update(T entity);
        List<T> GetAll();
        T GetById(K id);
        bool EntityExistsByAttribute(string attributeName, string attributeValue);
    }
}