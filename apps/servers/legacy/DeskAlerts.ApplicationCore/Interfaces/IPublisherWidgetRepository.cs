﻿using System;
using System.Collections.Generic;
using DeskAlerts.ApplicationCore.Entities;

namespace DeskAlerts.ApplicationCore.Interfaces
{
    public interface IPublisherWidgetRepository : IRepository<PublisherWidget, Guid>
    {
        IEnumerable<PublisherWidget> GetByPublisherId(long publisherId);
    }
}