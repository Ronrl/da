﻿using System.Collections.Generic;
using DeskAlerts.ApplicationCore.Entities;

namespace DeskAlerts.ApplicationCore.Interfaces
{
    public interface IUserGroupRepository : IRepository<UserGroup, long>
    {
    }
}