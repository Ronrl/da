﻿using System;
using DeskAlerts.ApplicationCore.Entities;

namespace DeskAlerts.ApplicationCore.Interfaces
{
    public interface IUserInstanceComputerRepository : IRepository<UserInstanceComputer, Guid>
    {
        
    }
}