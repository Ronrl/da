﻿using System;
using DeskAlerts.ApplicationCore.Entities;

namespace DeskAlerts.ApplicationCore.Interfaces
{
    public interface IRecurrenceRepository : IRepository<Recurrence, long>
    {
        bool IsActualDayRecurrenceForContent(DateTime dateTime, long contentId);
    }
}