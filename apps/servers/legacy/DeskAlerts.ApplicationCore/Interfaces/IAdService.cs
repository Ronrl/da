﻿using DeskAlerts.ApplicationCore.Entities;

namespace DeskAlerts.ApplicationCore.Interfaces
{
    public interface IAdService
    {
        Domain GetDomain(string domainName);

        long GetUserIdByDomainNameAndUserName(string domainName, string userName);
    }
}
