﻿using DeskAlerts.ApplicationCore.Dtos;

namespace DeskAlerts.ApplicationCore.Interfaces
{
    public interface IUserInstanceService
    {
        void HandleNewUserInstance(UserInfoDto userInfo, string instanceIp, string computerName, string computerDomainName);
    }
}