﻿using System;
using System.Collections.Generic;
using DeskAlerts.ApplicationCore.Entities;

namespace DeskAlerts.ApplicationCore.Interfaces
{
    public interface ISurveyRepository : IRepository<AlertEntity, long>
    {
        IEnumerable<long> GetActualSurveysIds(long userId, Guid deskBarId, DateTime dateTime, string ip, string computerName);
        IEnumerable<long> GetActualSurveysIds(long userId, DateTime dateTime);
        IEnumerable<long> GetRecipientsUsersIds(long surveyId);

        IEnumerable<long> GetSurveyIdBySurveyMainTableId(IEnumerable<long> surveyMainTableIds);
    }
}