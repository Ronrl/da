﻿namespace DeskAlerts.ApplicationCore.Interfaces
{
    using Dtos;
    using Models;
    using System;
    using System.Web;

    public interface IAutoupdateService
    {
        AutoUpdateSlotDto GetSlot(ClientApplicationInfo version);
        string PathToFile { get; }
        void SaveFile(HttpPostedFile file);
        void DeleteFile();
        string UrlToDownload { get; }
        void DeleteSlotById(Guid slotId);
        void UpdateConfiguration(int maxSlotCacheLength, int slotLifeTime);
        void ClearSlots();
        string Version { get; }
    }
}
