﻿using System;
using System.Collections.Generic;

namespace DeskAlerts.ApplicationCore.Interfaces
{
    using Entities;
    public interface ISkinRepository : IRepository<Skin, Guid>
    {
        IEnumerable<Skin> GetSkinsByGuidList(IEnumerable<Guid> skinGuidList);

        IEnumerable<Skin> GetSkinsByPolicyId(long policyId);

        void DeleteByPolicyId(long policyId);

        void CreateDependencies(long policyId);

        void CreateSpecificDependencies(long policyId, IEnumerable<Guid> skinsIds);

        Guid GetSkinIdByAlertId(long alertId);
    }
}
