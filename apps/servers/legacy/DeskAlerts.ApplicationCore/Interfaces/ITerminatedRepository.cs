﻿using System;
using System.Collections.Generic;
using DeskAlerts.ApplicationCore.Entities;

namespace DeskAlerts.ApplicationCore.Interfaces
{
    public interface ITerminatedRepository : IRepository<AlertEntity, long>
    {
        IEnumerable<long> GetActualTerminatedContentIds(long userId, Guid deskBarId, DateTime dateTime, string ip, string computerName);
        IEnumerable<long> GetRecipientsUsersIds(long terminatedContentId);
    }
}