﻿using DeskAlerts.ApplicationCore.Entities;
using System.Collections.Generic;

namespace DeskAlerts.ApplicationCore.Interfaces
{
    public interface IAlertsSentRepository : IRepository<AlertSent, long>
    {
        IEnumerable<AlertSent> GetAlertSentsForContent(long contentId);
    }
}
