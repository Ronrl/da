﻿namespace DeskAlerts.ApplicationCore.Interfaces
{
    using DeskAlerts.ApplicationCore.Dtos;

    public interface IPage
    {
        Pagination Pagination { get; set; }
        bool CanCreate { get; set; }
        bool CanView { get; set; }
        bool CanEdit { get; set; }
        bool CanDelete { get; set; }
        bool CanSend { get; set; }
        bool CanStop { get; set; }
    }
}
