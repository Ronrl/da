﻿using System.Collections.Generic;

namespace DeskAlerts.ApplicationCore.Interfaces
{
    using Entities;

    public interface IAlertReceivedRepository : IRepository<AlertReceived, long>
    {
        AlertReceived GetByAlertIdAndUserId(long alertid, long userId, string deskbarId);

        bool IsAlertReceived(long alertid, long userId, string deskbarId);

        bool IsTerminateCommandReceived(long alertId, long userId, string deskbarId);

        int GetVotesCount(long alertId);

        List<AlertReceived> GetSendedForUser(long userId);

        List<AlertReceived> GetReceivedForUser(long userId);

        IEnumerable<AlertReceived> GetAlertsReceivedForContent(long contentId);

        IEnumerable<AlertReceived> GetAlertsReceivedForContentExtended(long contentId);

        IEnumerable<AlertReceived> GetReceivedForContent(long contentId);

        void CreateForRecipientsOfBroadcastContent(long alertId, string deviceTypeScript);

        IEnumerable<string> GetUsernamesForPushesByAlertId(long alertId);

        IEnumerable<AlertReceived> GetUserAndAlertIdsWithSmsToSend();

        IEnumerable<AlertReceived> GetUserAndAlertIdToEmailSend();
    }
}
