﻿using System;
using System.Collections.Generic;
using DeskAlerts.ApplicationCore.Entities;

namespace DeskAlerts.ApplicationCore.Interfaces
{
    public interface IUserInstanceIpRepository : IRepository<UserInstanceIps, Guid>
    {
        void DeleteByUserInstanceId(Guid userInstanceId);
        List<UserInstanceIps> GetByUserId(long userid);
    }
}