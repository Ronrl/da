﻿using System;
using System.Collections.Generic;
using DeskAlerts.ApplicationCore.Entities;

namespace DeskAlerts.ApplicationCore.Interfaces
{
    public interface IRsvpRepository : IRepository<AlertEntity, long>
    {
        IEnumerable<long> GetActualRsvpIds(long userId, Guid deskBarId, DateTime dateTime, string ip, string computerName);
        IEnumerable<long> GetActualRsvpIds(long userId, DateTime dateTime);
        IEnumerable<long> GetRecipientsUsersIds(long rsvpId);
        IEnumerable<long> GetRecipientsCompsIds(long rsvpId);
    }
}
