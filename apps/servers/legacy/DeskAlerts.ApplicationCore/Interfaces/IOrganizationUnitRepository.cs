﻿using System.Collections.Generic;
using DeskAlerts.ApplicationCore.Entities;

namespace DeskAlerts.ApplicationCore.Interfaces
{
    public interface IOrganizationUnitRepository : IRepository<OrganizationUnit, long>
    {
        IEnumerable<long> GetUserIdsByOrganizationUnitNames(IEnumerable<string> organizationUnitNames);
    }
}
