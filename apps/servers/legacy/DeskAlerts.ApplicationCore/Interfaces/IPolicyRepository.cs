﻿using System;
using System.Collections.Generic;
using DeskAlerts.ApplicationCore.Dtos;
using DeskAlerts.ApplicationCore.Entities;

namespace DeskAlerts.ApplicationCore.Interfaces
{
    public interface IPolicyRepository : IRepository<Policy, long>
    {
        List<Policy> GetPoliciesByUserIdAndDomainId(long userId, long domainId);
        List<PolicyUser> GetPolicyUsersByPublisherId(long publisherId);
        List<PolicyUser> GetPolicyUsersByPolicyId(long policyId);
        List<PolicyOU> GetPolicyOUsByPublisherId(long publisherId);
        List<PolicyOU> GetPolicyOUsByPolicyId(long policyId);
        List<PolicyGroup> GetPolicyGroupsByPublisherId(long publisherId);
        List<PolicyGroup> GetPolicyGroupsByPolicyId(long policyId);
        List<Policy> GetPoliciesByPublisherId(long publisherId);
        string GetPolicyTypeByPublisherId(long publisherId);
        List<PolicyInfoDto> GetPolicyGroups();
        IEnumerable<char> GetPoliciesTypes(long publisherId);
        int GetRecipientsTotal(long policyId);
    }
}