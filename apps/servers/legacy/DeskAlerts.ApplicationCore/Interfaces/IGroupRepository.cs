﻿namespace DeskAlerts.ApplicationCore.Interfaces
{
    using System.Collections.Generic;
    using DeskAlerts.ApplicationCore.Entities;

    public interface IGroupRepository : IRepository<Group, long>
    {
        void AddGroupToGroup(Group child, Group parent);
        List<Group> GetChildGroupsByDomainName(string domainName);
        List<Group> GetChildGroupsByGroupName(string groupName);
        List<Group> GetChildGroupsByGroupId(int groupId);
        void RemoveGroupFromGroup(Group child, Group parent);
        IEnumerable<Group> GetGroupByDomainIdMappedWithUser(long domainId, string userName);
        IEnumerable<Group> GetGroupByDomainIdAndUserIdMappedWithUser(long domainId, long userId);
        IEnumerable<Group> GetGroupsPublisherId(long userId);
        IEnumerable<Group> GetGroupsWithDomain();
        IEnumerable<Group> GetGroupsWithAdminPolicy();
        IEnumerable<long> GetUserIdsByGroupNames(IEnumerable<string> groupNames);
    }
}