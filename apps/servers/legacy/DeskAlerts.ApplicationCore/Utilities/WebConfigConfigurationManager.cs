﻿using DeskAlerts.ApplicationCore.Interfaces;
using System;
using System.Configuration;
using System.Text;
using System.Web.Configuration;

namespace DeskAlerts.ApplicationCore.Utilities
{
    public class WebConfigConfigurationManager : IConfigurationManager
    {
        private readonly Configuration _configuration;

        public WebConfigConfigurationManager(Configuration configuration)
        {
            _configuration = configuration;
        }

        public string GetAppSetting(string key)
        {
            return WebConfigurationManager.AppSettings[key];
        }

        public string DatabaseName { get { return _configuration.AppSettings.Settings["dbname"].Value; } }

        public string DataSource { get { return _configuration.AppSettings.Settings["source"].Value; } }

        public bool IsWindowsAuthentication { get { return _configuration.AppSettings.Settings["winauth"].Value == "1"; } }

        public string Password
        {
            get
            {
                return Encoding.UTF8.GetString(Convert.FromBase64String(
                    _configuration.AppSettings.Settings["password"].Value));
            }
        }

        public string User { get { return _configuration.AppSettings.Settings["user"].Value; } }

        public string GetConnectionString()
        {
            return IsWindowsAuthentication
                       ? $"Data Source={DataSource};Initial Catalog={DatabaseName};TrustServerCertificate=True;Integrated Security=SSPI;Max Pool Size=1000000;{AppConfiguration.MultipleActiveResultSets}"
                       : $"Data Source={DataSource};Initial Catalog={DatabaseName};User ID={User};Password={Password};TrustServerCertificate=True;Max Pool Size=1000000;{AppConfiguration.MultipleActiveResultSets}";
        }
    }
}
