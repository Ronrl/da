﻿using System;
using System.Collections.Generic;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.ApplicationCore.Models.ContentTypes;

namespace DeskAlerts.ApplicationCore.Utilities
{
    public static class ContentTypeUtils
    {
        private static readonly Dictionary<Type, AlertType> ContentTypeDictionary = new Dictionary<Type, AlertType>
        {
            { typeof(AlertContentType), AlertType.SimpleAlert },
            { typeof(TickerContentType), AlertType.Ticker },
            { typeof(RsvpContentType), AlertType.Rsvp },
            { typeof(ScreensaverContentType), AlertType.ScreenSaver },
            { typeof(WallpaperContentType), AlertType.Wallpaper },
            { typeof(LockscreenContentType), AlertType.LockScreen },
            { typeof(SurveyContentType), AlertType.SimpleSurvey },
            { typeof(VideoAlertContentType), AlertType.VideoAlert },
            { typeof(TerminatedContentType), AlertType.Terminated }
        };

        public static AlertType GetContentTypeEnum<TContent>() where TContent : IContentType
        {
            var typeOfContent = typeof(TContent);
            var isContains = ContentTypeDictionary.TryGetValue(typeOfContent, out var alertType);

            return isContains
                ? alertType
                : AlertType.SimpleAlert;
        }
    }
}
