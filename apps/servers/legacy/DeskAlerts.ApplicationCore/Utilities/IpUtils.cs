﻿using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace DeskAlerts.ApplicationCore.Utilities
{
    public class IpUtils
    {
        /// <summary>
        /// Convert string with ip addresses to list of <see cref="IPAddress"/>.
        /// </summary>
        /// <param name="ipAddresses">String with ips.</param>
        /// <returns><see cref="IEnumerable{T}"/> of <see cref="IPAddress"/>.</returns>
        public static IEnumerable<IPAddress> ParseIpAddresses(string ipAddresses)
        {
            if (string.IsNullOrEmpty(ipAddresses))
            {
                return new List<IPAddress>();
            }

            var ipAddressList = ipAddresses
                .Split(',')
                .ToList();

            ipAddressList
                .Remove(ipAddressList.FirstOrDefault(item => item == "ip"));

            var result = ipAddressList
                .Select(ip => IPAddress.Parse(ip.Trim()))
                .ToList();

            return result;
        }
    }
}
