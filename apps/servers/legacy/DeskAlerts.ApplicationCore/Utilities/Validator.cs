﻿using System;

namespace DeskAlerts.ApplicationCore.Utilities
{
    public static class Validator
    {
        public static void ValidateId(long id)
        {
            if (id <= 0)
            {
                throw new ArgumentException("Id should be greater than zero");
            }
        }
    }
}
