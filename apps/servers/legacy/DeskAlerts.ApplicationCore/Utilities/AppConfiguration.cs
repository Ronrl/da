﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace DeskAlerts.ApplicationCore.Utilities
{
    public class AppConfiguration
    {
        /// <summary>
        /// The number of entries that can be stored in the server cache.
        /// </summary>
        public static int CacheCapacity
        {
            get
            {
                var result = 10000;
                try
                {
                    var value = ConfigurationManager.AppSettings["CacheCapacity"];
                    if (string.IsNullOrEmpty(value))
                    {
                        return result;
                    }

                    result = int.Parse(value);
                }
                catch
                {
                    return result;
                }

                return result;
            }
        }

        /// <summary>
        /// Flag indicating that it is necessary to use the new cds system.
        /// </summary>
        public static bool IsNewContentDeliveringScheme
        {
            get
            {
                var result = true;

                try
                {
                    var value = ConfigurationManager.AppSettings["IsNewContentDeliveringScheme"];
                    if (string.IsNullOrEmpty(value))
                    {
                        return true;
                    }

                    result = bool.Parse(value);
                }
                catch
                {
                    return result;
                }

                return result;
            }
        }

        /// <summary>
        /// Get list of siblings servers.
        /// </summary>
        public static IEnumerable<string> GetServerSiblings
        {
            get
            {
                IEnumerable<string> result;

                try
                {
                    var servers = ConfigurationManager.AppSettings["Siblings"];
                    result = servers
                        .Split(';')
                        .Where(server => !string.IsNullOrEmpty(server))
                        .ToList();
                }
                catch
                {
                    result = new List<string>();
                }

                return result;
            }
        }

        /// <summary>
        /// A flag indicating that sms, email, push, text to call should be sent from this server.
        /// </summary>
        public static bool IsPrimaryServer
        {
            get
            {
                var result = true;

                try
                {
                    var value = ConfigurationManager.AppSettings["IsPrimaryServer"];
                    if (string.IsNullOrEmpty(value))
                    {
                        return true;
                    }

                    result = bool.Parse(value);
                }
                catch
                {
                    return result;
                }

                return result;
            }
        }

        /// <summary>
        /// Cache regenerate time in seconds. Default value 3600 seconds.
        /// </summary>
        public static double CacheRegenerateTime
        {
            get
            {
                double result = 3600;

                try
                {
                    var value = ConfigurationManager.AppSettings["CacheRegenerateTime"];
                    if (string.IsNullOrEmpty(value))
                    {
                        return result;
                    }

                    result = double.Parse(value);
                }
                catch
                {
                    return result;
                }

                return result;
            }
        }

        /// <summary>
        /// Returns the number of seconds in HTTP Headers Cache-control. 
        /// </summary>
        public static int MaxAgeOfHttpHeaderCacheControl
        {
            get
            {
                const int defaultSeconds = 5;

                try
                {
                    var value = ConfigurationManager.AppSettings["HttpHeaderCacheControlMaxAge"];
                    if (value == null || string.IsNullOrEmpty(value))
                    {
                        return defaultSeconds;
                    }

                    var result = int.Parse(value);

                    if (result < default(int) || result > new TimeSpan(0, 1, 0, 0).TotalSeconds)
                    {
                        result = defaultSeconds;
                    }

                    return result;
                }
                catch
                {
                    return defaultSeconds;
                }
            }
        }

        /// <summary>
        /// Enables http status 304 not modified in server response for clients applications.
        /// </summary>
        public static bool IsHttpStatusNotModified
        {
            get
            {
                var result = true;

                try
                {
                    var value = ConfigurationManager.AppSettings["IsHttpStatusNotModified"];
                    if (string.IsNullOrEmpty(value))
                    {
                        return true;
                    }

                    result = bool.Parse(value);
                }
                catch
                {
                    return result;
                }

                return result;
            }
        }

        /// <summary>
        /// SMS and Email recurrent task timer in seconds.
        /// </summary>
        public static double SmsAndEmailSendingTaskTimer
        {
            get
            {
                const double defaultSeconds = 60;

                try
                {
                    var value = ConfigurationManager.AppSettings["SmsAndEmailSendingTaskTimer"];
                    if (value == null || string.IsNullOrEmpty(value))
                    {
                        return defaultSeconds;
                    }

                    var result = double.Parse(value);

                    if (result <= default(double) || result > new TimeSpan(0, 1, 0, 0).TotalSeconds)
                    {
                        result = defaultSeconds;
                    }

                    return result;
                }
                catch
                {
                    return defaultSeconds;
                }
            }
        }

        /// <summary>
        /// Maximum entries (contents) in response for client.
        /// Example: 17 new entries for client. Server return 10 entries in first response and 7 entries in second response.
        /// </summary>
        public static int MaximumNumberOfResponseEntries
        {
            get
            {
                const int defaultEntries = 10;

                try
                {
                    var value = ConfigurationManager.AppSettings["MaximumNumberOfResponseEntries"];
                    if (value == null || string.IsNullOrEmpty(value))
                    {
                        return defaultEntries;
                    }

                    var result = int.Parse(value);

                    if (result <= default(int))
                    {
                        result = 1;
                    }

                    if (result > 100)
                    {
                        result = 100;
                    }

                    return result;
                }
                catch
                {
                    return defaultEntries;
                }
            }
        }

        /// <summary>
        /// Enable MultipleActiveResultSets (MARS) in sql connection string.
        /// Default value "true".
        /// <see cref="https://docs.microsoft.com/en-us/dotnet/framework/data/adonet/sql/multiple-active-result-sets-mars"/>
        /// </summary>
        public static string MultipleActiveResultSets
        {
            get
            {
                bool configStringValue;

                try
                {
                    var value = ConfigurationManager.AppSettings["MultipleActiveResultSets"];
                    
                    configStringValue = string.IsNullOrEmpty(value) || bool.Parse(value);
                }
                catch
                {
                    configStringValue = true;
                }

                var mars = configStringValue 
                    ? "MultipleActiveResultSets=True;" 
                    : "MultipleActiveResultSets=False;";

                return mars;
            }
        }

        /// <summary>
        /// Connection string to db
        /// </summary>
        public static string ConnectionString
        {
            get
            {
                try
                {
                    var isWindowsAuth = ConfigurationManager.AppSettings["winauth"] == "1";
                    var isAzureSql = ConfigurationManager.AppSettings["azuresql"] == "1";

                    var dataSource = ConfigurationManager.AppSettings["source"];
                    var dbName = ConfigurationManager.AppSettings["dbname"];
                    
                    if (isWindowsAuth)
                    {
                        return $"Data Source={dataSource};Initial Catalog={dbName};TrustServerCertificate=True;Integrated Security=SSPI;Max Pool Size=1000000;";
                    }

                    var user = ConfigurationManager.AppSettings["user"];
                    var password = Encoding.Default.GetString(Convert.FromBase64String(ConfigurationManager.AppSettings["password"]));

                    if (isAzureSql)
                    {
                        return $"Server=tcp:{dataSource}.database.windows.net,1433;Initial Catalog={dbName};Persist Security Info=False;User ID={user};Password={password};Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
                    }

                    // Sql authentication
                    return  $"Data Source={dataSource};Initial Catalog={dbName};User ID={user};Password={password};TrustServerCertificate=True;Max Pool Size=1000000;";
                }
                catch
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// LockScreen enable status.
        /// </summary>
        public static bool IsLockScreen
        {
            get
            {
                var result = false;

                try
                {
                    var value = ConfigurationManager.AppSettings["LOCKSCREEN"];
                    if (string.IsNullOrEmpty(value))
                    {
                        return false;
                    }

                    var configValue = int.Parse(value);

                    result = configValue == 1;
                }
                catch
                {
                    return result;
                }

                return result;
            }
        }
    }
}