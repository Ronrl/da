﻿namespace DeskAlerts.ApplicationCore.Exceptions
{
    using System;
    using System.Runtime.Serialization;

    public class EntityNotFoundException : EntityExceptionBase
    {
        public EntityNotFoundException(string message)
            : base(message)
        {
        }

        public EntityNotFoundException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected EntityNotFoundException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}