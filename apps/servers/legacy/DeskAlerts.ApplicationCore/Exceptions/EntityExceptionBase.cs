﻿namespace DeskAlerts.ApplicationCore.Exceptions
{
    using System;
    using System.Runtime.Serialization;

    public class EntityExceptionBase : Exception
    {
        public EntityExceptionBase(string message)
            : base(message)
        {
        }

        public EntityExceptionBase(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected EntityExceptionBase(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}