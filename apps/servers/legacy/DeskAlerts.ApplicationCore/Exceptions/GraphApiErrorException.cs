﻿namespace DeskAlerts.ApplicationCore.Exceptions
{
    using System;
    using System.Runtime.Serialization;

    public class GraphApiErrorException : Exception
    {
        public GraphApiErrorException(string message)
            : base(message)
        {
        }

        public GraphApiErrorException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected GraphApiErrorException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}