﻿namespace DeskAlerts.ApplicationCore.Exceptions
{
    using System;
    using System.Runtime.Serialization;

    public class EntityNotDeletedException : EntityExceptionBase
    {
        public EntityNotDeletedException(string message)
            : base(message)
        {
        }

        public EntityNotDeletedException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected EntityNotDeletedException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}