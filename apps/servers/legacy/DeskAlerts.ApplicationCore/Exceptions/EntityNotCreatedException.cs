﻿namespace DeskAlerts.ApplicationCore.Exceptions
{
    using System;
    using System.Runtime.Serialization;

    public class EntityNotCreatedException : EntityExceptionBase
    {
        public EntityNotCreatedException(string message)
            : base(message)
        {
        }

        public EntityNotCreatedException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected EntityNotCreatedException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}