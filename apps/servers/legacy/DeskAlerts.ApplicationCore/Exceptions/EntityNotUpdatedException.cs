﻿namespace DeskAlerts.ApplicationCore.Exceptions
{
    using System;
    using System.Runtime.Serialization;

    public class EntityNotUpdatedException : EntityExceptionBase
    {
        public EntityNotUpdatedException(string message)
            : base(message)
        {
        }

        public EntityNotUpdatedException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected EntityNotUpdatedException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}