﻿namespace DeskAlerts.ApplicationCore.Replicators
{
    using DeskAlerts.ApplicationCore.Entities;
    using System.Collections.Generic;

    public class GroupToUserRelator
    {
        public Group current;

        public Group MapIt(Group g, User u)
        {
            if (g == null)
                return current;

            if (g != null && current != null && g.Id == current.Id)
            {
                current.UsersMembers.Add(u);
                return null;
            }

            var prev = current;
            current = g;
            current.UsersMembers = new List<User>() { u };

            return prev;
        }
    }
}
