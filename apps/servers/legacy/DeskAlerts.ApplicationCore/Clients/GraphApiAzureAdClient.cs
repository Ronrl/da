﻿namespace DeskAlerts.ApplicationCore.Clients
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Net.Http;
    using System.Net.Http.Headers;

    using DeskAlerts.ApplicationCore.Dtos;
    using DeskAlerts.ApplicationCore.Exceptions;
    using DeskAlerts.ApplicationCore.Interfaces;

    using Microsoft.IdentityModel.Clients.ActiveDirectory;

    using Newtonsoft.Json;

    public class GraphApiAzureAdClient : IAzureAdClient
    {
        private const string GraphApiUrl = "https://graph.microsoft.com/";

        private const string GraphApiVersion = "v1.0";

        private string _domainTenant;

        private string _pollingApplicationId;

        private string _pollingApplicationSecretKey;

        public GraphApiAzureAdClient(
            string domainTenant,
            string pollingApplicationId,
            string pollingApplicationSecretKey)
        {
            if (domainTenant == null)
            {
                throw new ArgumentNullException(nameof(domainTenant));
            }

            if (pollingApplicationId == null)
            {
                throw new ArgumentNullException(nameof(pollingApplicationId));
            }

            if (pollingApplicationSecretKey == null)
            {
                throw new ArgumentNullException(nameof(pollingApplicationSecretKey));
            }

            _domainTenant = domainTenant;
            _pollingApplicationId = pollingApplicationId;
            _pollingApplicationSecretKey = pollingApplicationSecretKey;
        }

        public List<AzureAdGroupMember> GetGroupMembers(AzureAdGroup group)
        {
            if (group == null)
            {
                throw new ArgumentNullException(nameof(group));
            }

            var resource = $"groups/{group.Id}/members";
            var membersCollection = GetAzureAdCollection<AzureAdGroupMember>(resource);

            return membersCollection;
        }

        public List<AzureAdGroup> GetGroups()
        {
            var groupsCollection = GetAzureAdCollection<AzureAdGroup>("groups");

            return groupsCollection;
        }

        public List<AzureAdUser> GetUsers()
        {
            var userCollection = GetAzureAdCollection<AzureAdUser>("users");
            return userCollection;
        }

        private List<T> GetAzureAdCollection<T>(string resource)
        {
            if (resource == null)
            {
                throw new ArgumentNullException(nameof(resource));
            }

            try
            {
                var token = GetGraphApiAccessToken();

                var client = new HttpClient();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

                var url = $"{GraphApiUrl}{GraphApiVersion}/{resource}?$top=100";
                var collection = new List<T>();
                while (!string.IsNullOrEmpty(url))
                {
                    var content = client.GetStringAsync(url).Result;
                    var response = JsonConvert.DeserializeObject<GraphApiCollectionResponse<T>>(content);
                    url = response.NextPageLink;
                    collection.AddRange(response.Collection);
                }

                return collection;
            }
            catch (Exception exception)
            {
                Debug.WriteLine(exception.Message);
                throw new GraphApiErrorException(exception.Message, exception.InnerException);
            }
        }

        private string GetGraphApiAccessToken()
        {
            var authority = $"https://login.microsoftonline.com/{_domainTenant}";
            var authenticationContext = new AuthenticationContext(authority);

            var clientCredential = new ClientCredential(_pollingApplicationId, _pollingApplicationSecretKey);

            var authenticationResult = authenticationContext.AcquireTokenAsync(GraphApiUrl, clientCredential).Result;
            return authenticationResult.AccessToken;
        }
    }
}