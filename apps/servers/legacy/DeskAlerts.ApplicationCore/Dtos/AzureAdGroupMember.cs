﻿namespace DeskAlerts.ApplicationCore.Dtos
{
    using Newtonsoft.Json;

    public class AzureAdGroupMember
    {
        [JsonProperty("id")]
        public string Id { get; set; } = string.Empty;
    }
}