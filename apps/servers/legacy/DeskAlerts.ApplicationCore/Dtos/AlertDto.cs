﻿using System;

namespace DeskAlerts.ApplicationCore.Dtos
{
    public class AlertDto
    {
        public long Id { get; set; }

        public long CampaignId { get; set; }

        public string Title { get; set; }

        public string Scheduled { get; set; }

        public AlertTypeDto Type { get; set; }

        public string SenderName { get; set; }

        public DateTime? FromDate { get; set; }

        public DateTime? ToDate { get; set; }
    }
}