﻿namespace DeskAlerts.ApplicationCore.Dtos
{
    using System.Collections.Generic;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public enum AzureAdAutoSynchronizationMode
    {
        Daily,

        Weekly,

        Monthly
    }

    public class AzureAdAutoSynchronizationSettings
    {
        public bool Enabled { get; set; } = false;

        [JsonConverter(typeof(StringEnumConverter))]
        public AzureAdAutoSynchronizationMode Mode { get; set; } = AzureAdAutoSynchronizationMode.Daily;

        public string MonthlyDay { get; set; } = string.Empty;

        public string StartTime { get; set; } = string.Empty;

        public List<string> WeeklyDays { get; set; } = new List<string>();
    }
}