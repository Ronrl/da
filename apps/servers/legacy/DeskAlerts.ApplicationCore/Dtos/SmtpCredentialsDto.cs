﻿using Newtonsoft.Json;

namespace DeskAlerts.ApplicationCore.Dtos
{
    public class SmtpCredentialsDto
    {
        public string SmtpServer { get; set; }

        public int SmtpPort { get; set; }

        public bool SmtpAuthentication { get; set; }

        public bool SmtpSsl { get; set; }

        public string SmtpUserName { get; set; }

        public string SmtpUserNamePassword { get; set; }

        public string SmtpFrom { get; set; }

        public string SmtpConnectionTimeout { get; set; }
        
    }
}
