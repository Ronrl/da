﻿namespace DeskAlerts.ApplicationCore.Dtos.Content
{
    using Interfaces;

    public class LockscreenDto : IContent
    {
        public long AlertId { get; set; }
        public string Href { get; set; }
        public string UserId { get; set; }
        public string Height { get; set; }
        public string Width { get; set; }
        public string Title { get; set; }
        public string Acknowledgment { get; set; }
        public string ToDate { get; set; }
        public string Utc { get; set; }
        public string IsUtf8 { get; set; }
        public string Autoclose { get; set; }
    }
}
