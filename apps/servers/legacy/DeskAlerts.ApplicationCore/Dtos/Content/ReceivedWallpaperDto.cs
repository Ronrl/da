﻿namespace DeskAlerts.ApplicationCore.Dtos.Content
{
    using Enums;
    using Interfaces;

    public class ReceivedWallpaperDto : IReceivedContent
    {
        public long Id { get; set; }
        public AlertReceiveStatus ReceivedStatus { get; set; }
    }
}
