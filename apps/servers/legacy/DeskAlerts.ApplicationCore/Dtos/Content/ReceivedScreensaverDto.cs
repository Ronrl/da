﻿namespace DeskAlerts.ApplicationCore.Dtos.Content
{
    using Enums;
    using Interfaces;

    public class ReceivedScreensaverDto : IReceivedContent
    {
        public long Id { get; set; }
        public AlertReceiveStatus ReceivedStatus { get; set; }
    }
}
