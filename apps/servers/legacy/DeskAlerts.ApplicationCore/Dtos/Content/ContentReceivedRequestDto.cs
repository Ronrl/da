﻿namespace DeskAlerts.ApplicationCore.Dtos.Content
{
    using System.Collections.Generic;

    public class ContentReceivedRequestDto
    {
        public IEnumerable<ReceivedAlertDto> Alerts { get; set; }
        public IEnumerable<ReceivedWallpaperDto> Wallpapers { get; set; }
        public IEnumerable<ReceivedLockscreenDto> Lockscreens { get; set; }
        public IEnumerable<ReceivedScreensaverDto> Screensavers { get; set; }
    }
}
