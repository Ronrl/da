﻿namespace DeskAlerts.ApplicationCore.Dtos.Content
{
    using Interfaces;

    public class ScreensaverDto : IContent
    {
        public long Id { get; set; }
        public string Xml { get; set; }
        public string Url { get; set; }
    }
}
