﻿namespace DeskAlerts.ApplicationCore.Dtos.Content
{
    public class ContentRequestDto
    {
        public string LocalTime { get; set; }
    }
}
