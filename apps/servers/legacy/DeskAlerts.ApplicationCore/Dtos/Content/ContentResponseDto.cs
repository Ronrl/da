﻿namespace DeskAlerts.ApplicationCore.Dtos.Content
{
    using System.Collections.Generic;

    public class ContentResponseDto
    {
        public IEnumerable<ContentDto> Alerts { get; set; }
        public string WallpapersHash { get; set; }
        public string LockscreensHash { get; set; }
        public string ScreensaversHash { get; set; }
        public IEnumerable<long> TerminatedAlerts { get; set; }

        //TODO: delete after testing
        public IEnumerable<ContentDto> AlertsNew { get; set; }
        public string WallPapersHashNew { get; set; }
        public string LockscreensHashNew { get; set; }
        public string ScreenSaversHashNew { get; set; }
        public IEnumerable<long> TerminatedAlertsNew { get; set; }
    }
}
