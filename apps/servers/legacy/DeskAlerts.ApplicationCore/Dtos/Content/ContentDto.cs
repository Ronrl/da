﻿namespace DeskAlerts.ApplicationCore.Dtos.Content
{
    using Interfaces;

    public class ContentDto: IContent
    {
        public long Id { get; set; }
        public string Body { get; set; }
        public string Url { get; set; }
    }
}
