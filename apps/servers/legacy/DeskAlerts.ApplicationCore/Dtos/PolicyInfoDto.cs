﻿namespace DeskAlerts.ApplicationCore.Dtos
{
    using DeskAlerts.ApplicationCore.Entities;

    public class PolicyInfoDto
    {
        public long? Id { get; set; } = 0; 
        
        public string Name { get; set; } = string.Empty;

        public string Policy_type { get; set; } = string.Empty;

        public PolicyInfoDto(Policy policy)
        {
            Id = policy.Id;
            Name = policy.Name;
            Policy_type = policy.Type;
        }
    }
}
