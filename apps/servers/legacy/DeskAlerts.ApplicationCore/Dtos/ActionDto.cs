﻿namespace DeskAlerts.ApplicationCore.Dtos
{
    public class ActionDto
    {
        public ActionDto(string reference, string imageReference, string text)
        {
            Reference = reference;
            ImageReference = imageReference;
            Text = text;
        }

        public string Reference { get; }

        public string ImageReference { get; }

        public string Text { get; }
    }
}