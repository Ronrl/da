﻿namespace DeskAlerts.ApplicationCore.Dtos
{
    public class UserInfoDto
    {
        public string ClientId { get; set; } = string.Empty;

        public string DeskbarId { get; set; } = string.Empty;

        public string DomainName { get; set; } = string.Empty;

        public string Email { get; set; } = string.Empty;

        public string Password { get; set; } = string.Empty;

        public string Phone { get; set; } = string.Empty;

        public string Username { get; set; } = string.Empty;
    }
}