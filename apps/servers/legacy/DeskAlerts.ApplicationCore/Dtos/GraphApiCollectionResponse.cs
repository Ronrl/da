﻿namespace DeskAlerts.ApplicationCore.Dtos
{
    using System.Collections.Generic;

    using Newtonsoft.Json;

    public class GraphApiCollectionResponse<T>
    {
        [JsonProperty("value")]
        public List<T> Collection { get; set; } = new List<T>();

        [JsonProperty("@odata.nextLink")]
        public string NextPageLink { get; set; } = string.Empty;
    }
}