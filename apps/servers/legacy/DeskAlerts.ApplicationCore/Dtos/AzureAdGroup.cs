﻿namespace DeskAlerts.ApplicationCore.Dtos
{
    using Newtonsoft.Json;
    using System.Collections.Generic;

    public class AzureAdGroup
    {
        [JsonProperty("id")]
        public string Id { get; set; } = string.Empty;

        [JsonProperty("displayName")]
        public string Name { get; set; } = string.Empty;

        [JsonProperty("groupTypes")]
        public HashSet<string> GroupTypes { get; set; } = new HashSet<string>();

        [JsonProperty("mailEnabled")]
        public bool MailEnabled { get; set; } = false;

        [JsonProperty("securityEnabled")]
        public bool SecurityEnabled { get; set; } = false;

        public bool IsDistributionList()
        {
            return GroupTypes.Count == 0 && MailEnabled && !SecurityEnabled;
        }
    }
}