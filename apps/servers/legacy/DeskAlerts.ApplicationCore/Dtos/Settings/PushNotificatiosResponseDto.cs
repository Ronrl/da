﻿namespace DeskAlerts.ApplicationCore.Dtos.Settings
{
    public class PushNotificatiosResponseDto
    {
        /// <summary>
        /// Apple Push Notification Service
        /// </summary>
        public string ApnsResponse { get; set; }
        /// <summary>
        /// Google Cloud Messaging
        /// </summary>
        public string GcmResponse { get; set; }
        public bool IsApnsConnected { get; set; }
        public bool IsGcmConnected { get; set; }
    }
}
