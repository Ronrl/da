﻿namespace DeskAlerts.ApplicationCore.Dtos.Settings
{
    public class GroupSearchDto
    {
        public string Search { get; set; }
    }
}
