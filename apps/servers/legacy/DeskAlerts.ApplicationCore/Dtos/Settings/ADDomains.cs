﻿namespace DeskAlerts.ApplicationCore.Dtos.Settings
{
    using System.Collections.Generic;

    public class AdDomains
    {
        public List<AdGroupDto> children { get; set; } = new List<AdGroupDto>();
        
        public string text { get; set; } = string.Empty;

        public SsoGroupStateDto state { get; set; }
    }
}
