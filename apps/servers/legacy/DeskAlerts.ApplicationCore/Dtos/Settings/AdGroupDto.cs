﻿namespace DeskAlerts.ApplicationCore.Dtos.Settings
{
    public class AdGroupDto
    {
        public string text { get; set; } = string.Empty;
        public PolicyDto data { get; set; }
        public SsoGroupStateDto state { get; set; }
        public long id { get; set; }       
    }
}
