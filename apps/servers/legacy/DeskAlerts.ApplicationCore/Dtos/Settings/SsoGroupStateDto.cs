﻿namespace DeskAlerts.ApplicationCore.Dtos.Settings
{
    public class SsoGroupStateDto
    {
        public bool disabled { get; set; }

        public bool opened { get; set; }

        public bool @checked { get; set; }
    }
}