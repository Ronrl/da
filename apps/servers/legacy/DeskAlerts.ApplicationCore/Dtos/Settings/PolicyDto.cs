﻿namespace DeskAlerts.ApplicationCore.Dtos.Settings
{
    public class PolicyDto
    {
        public long? policy_id { get; set; } = null;
        public string policy_name { get; set; }
        public string policy_type { get; set; }
    }
}
