﻿using Newtonsoft.Json;

namespace DeskAlerts.ApplicationCore.Dtos.Settings
{
    public class DigitalSignageDeviceSettingsDto
    {
        [JsonProperty(PropertyName = "deviceName")]
        public string DeviceName { get; set; }

        [JsonProperty(PropertyName = "deskbarId")]
        public string DeskbarId { get; set; }

        [JsonProperty(PropertyName = "refreshTime")]
        public int RefreshTime { get; set; }
    }
}
