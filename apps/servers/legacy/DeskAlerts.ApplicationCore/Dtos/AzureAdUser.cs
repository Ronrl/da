﻿namespace DeskAlerts.ApplicationCore.Dtos
{
    using Newtonsoft.Json;

    public class AzureAdUser
    {
        [JsonProperty("displayName")]
        public string DisplayName { get; set; } = string.Empty;

        [JsonProperty("mail")]
        public string Email { get; set; } = string.Empty;

        [JsonProperty("id")]
        public string Id { get; set; } = string.Empty;

        [JsonProperty("mobilePhone")]
        public string Phone { get; set; } = string.Empty;

        [JsonProperty("userPrincipalName")]
        public string Username { get; set; } = string.Empty;
    }
}