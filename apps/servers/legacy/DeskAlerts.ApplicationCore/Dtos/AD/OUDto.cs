﻿namespace DeskAlerts.ApplicationCore.Dtos.AD
{
    public class OUDto
    {
        public int Id { get; set; }
        public int DomainId { get; set; }
        public int OuId { get; set; }
        public int ParentId { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public int NestingLevel { get; set; }
    }
}
