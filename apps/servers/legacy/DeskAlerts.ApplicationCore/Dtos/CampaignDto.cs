﻿using DeskAlerts.ApplicationCore.Entities;
using System;

namespace DeskAlerts.ApplicationCore.Dtos
{
    public class CampaignDto
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string StartDate { get; set; }

        public string EndDate { get; set; }

        public string StatusText { get; set; }

        public bool StopButtonVisible { get; set; }

        public bool StartButtonVisible { get; set; }

        public bool IsStarted { get; set; }

        public CampaignStatus Status { get; set; }

        public long SenderId { get; set; }

        public CampaignDto(CampaignStatus status, long id)
        {
            Status = status;
            StartButtonVisible = 
                status == CampaignStatus.Invative ||
                status == CampaignStatus.Pending ||
                status == CampaignStatus.Stopped;
            StopButtonVisible = status == CampaignStatus.Active;
            Id = id;
        }
    }
}