﻿namespace DeskAlerts.ApplicationCore.Dtos
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public enum ResponseStatus
    {
        Success,
        Fail,
        Error,
        NoContent,
        NA
    }

    public class DeskAlertsApiResponse<T>
    {
        public T Data { get; set; } = default(T);

        public string ErrorMessage { get; set; } = null;

        [JsonConverter(typeof(StringEnumConverter))]
        public ResponseStatus Status { get; set; } = ResponseStatus.Success;
    }
}