﻿using System.Collections.Generic;
using DeskAlerts.ApplicationCore.Entities;

namespace DeskAlerts.ApplicationCore.Dtos
{
    public class AlertTypeDto
    {
        public AlertTypeDto(AlertType alertType, long campaignId, string duplicateText, int alertClass, long alertId)
        {
            Actions = new List<ActionDto>();
            ActionDto duplicateAction = null;
            switch (alertType)
            {
                case AlertType.SimpleAlert:
                case AlertType.Ticker:
                case AlertType.Rsvp:
                    duplicateAction = new ActionDto($"CreateAlert.aspx?id={alertId}&class={alertClass}&alert_campaign={campaignId}", "images/action_icons/duplicate.png", duplicateText);
                    break;
                case AlertType.SimpleSurvey:
                case AlertType.SurveyQuiz:
                case AlertType.SurveyPoll:
                    duplicateAction = new ActionDto($"EditSurvey.aspx?id={alertId}&alert_campaign={campaignId}", "images/action_icons/duplicate.png", duplicateText);
                    break;
            }
            switch (alertType)
            {
                case AlertType.SimpleAlert:
                    Icon = "images/alert_types/alert.png";
                    Url = $"CreateAlert.aspx?skin_id=&alert_campaign={campaignId}";
                    Label = "Alert";
                    Value = 1;
                    break;
                case AlertType.Ticker:
                    Icon = "images/alert_types/ticker.png";
                    Url = $"CreateAlert.aspx?ticker=1&skin_id=&alert_campaign={campaignId}";
                    Label = "Ticker";
                    Value = 2;
                    break;
                case AlertType.Rsvp:
                    Icon = "images/alert_types/rsvp.png";
                    Url = $"CreateAlert.aspx?rsvp=1&skin_id=&alert_campaign={campaignId}";
                    Label = "RSVP";
                    Value = 3;
                    break;
                case AlertType.SimpleSurvey:
                    Icon = "images/alert_types/new/128.png";
                    Url = $"EditSurvey.aspx?alert_campaign={campaignId}";
                    Label = "Survey";
                    Value = 4;
                    break;
                case AlertType.SurveyQuiz:
                    Icon = "images/alert_types/new/128.png";
                    Url = $"EditSurvey.aspx?alert_campaign={campaignId}";
                    Label = "Quiz survey";
                    Value = 5;
                    break;
                case AlertType.SurveyPoll:
                    Icon = "images/alert_types/new/256.png";
                    Url = $"EditSurvey.aspx?alert_campaign={campaignId}";
                    Label = "Poll survey";
                    Value = 6;
                    break;
                case AlertType.Wallpaper:
                    Icon = "images/alert_types/fullscreen.png";
                    Url = $"EditWallpaper.aspx?alert_campaign={campaignId}";
                    Label = "Wallpapers";
                    Value = 8;
                    duplicateAction = new ActionDto($"EditWallpaper.aspx?id={alertId}&alert_campaign={campaignId}", "images/action_icons/duplicate.png", duplicateText);
                    break;
                case AlertType.ScreenSaver:
                    Icon = "images/alert_types/new/2.png";
                    Url = $"EditScreensavers.aspx?alert_campaign={campaignId}";
                    Label = "Screensavers";
                    Value = 7;
                    duplicateAction = new ActionDto($"EditScreensaversHtml.aspx?id={alertId}&alert_campaign={campaignId}", "images/action_icons/duplicate.png", duplicateText);
                    break;
                case AlertType.VideoAlert:
                    Icon = "images/alert_types/video_big.png";
                    Url = $"EditVideo.aspx?alert_campaign={campaignId}";
                    Label = "Video alerts";
                    Value = 9;
                    duplicateAction = new ActionDto($"EditVideo.aspx?id={alertId} &alert_campaign={campaignId}", "images/action_icons/duplicate.png", duplicateText);
                    break;
                case AlertType.RssFeed:
                    Icon = "images/alert_types/new/4.png";
                    Url = $"EditRss.aspx?alert_campaign={campaignId}";
                    Label = "RSS/news feed";
                    Value = 12;
                    duplicateAction = new ActionDto($"EditRss.aspx?id={alertId}&alert_campaign={campaignId}", "images/action_icons/duplicate.png", duplicateText);
                    break;
                case AlertType.RssAlert:
                    Value = 11;
                    break;
                case AlertType.EmergencyAlert:
                    Value = 10;
                    break;
                case AlertType.DigitalSignature:
                    Value = 13;
                    break;
                default:
                    break;
            }

            Actions.Add(duplicateAction);
        }

        public int Value { get; set; }

        public string Label { get; set; }

        public string Icon { get; set; }

        public string Url { get; set; }

        public List<ActionDto> Actions { get; set; }
    }
}