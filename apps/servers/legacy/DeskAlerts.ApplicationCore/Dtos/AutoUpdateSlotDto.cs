﻿using System;

namespace DeskAlerts.ApplicationCore.Dtos
{
    public class AutoUpdateSlotDto
    {
        public string SlotId { get; set; }

        public string Url { get; set; }
    }
}
