﻿namespace DeskAlerts.ApplicationCore.Dtos
{
    public class DateTimeDto
    {
        public string DateFormat { get; set; }

        public string TimeFormat { get; set; }
    }
}
