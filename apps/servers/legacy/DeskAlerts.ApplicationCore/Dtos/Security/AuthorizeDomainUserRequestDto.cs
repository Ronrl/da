﻿namespace DeskAlerts.ApplicationCore.Dtos.Security
{
    using Enums;

    public class AuthorizeDomainUserRequestDto
    {
        public string DeskbarId { get; set; }
        public string UserName { get; set; }
        public string ComputerName { get; set; }
        public string ComputerDomainName { get; set; }
        public string Ip { get; set; }
        public ClientDeviceType DeviceType { get; set; }
        public string ClientVersion { get; set; }
        public string DomainName { get; set; }
        public string PollPeriod { get; set; } = "5";
    }
}
