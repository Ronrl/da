﻿namespace DeskAlerts.ApplicationCore.Dtos.Security
{
    public class TokenResponseDto
    {
        public string Authorization { get; set; }
    }
}
