﻿namespace DeskAlerts.ApplicationCore.Dtos
{
    public class AzureAdSynchronizationSettings
    {
        public string AuthorizationApplicationId { get; set; } = string.Empty;

        public AzureAdAutoSynchronizationSettings AutoSynchronization { get; set; } = new AzureAdAutoSynchronizationSettings();

        public string DomainTennant { get; set; } = string.Empty;

        public string PollingApplicationId { get; set; } = string.Empty;

        public string PollingApplicationSecretKey { get; set; } = string.Empty;
    }
}