﻿namespace DeskAlerts.ApplicationCore.Dtos
{
    public class AzureAdSynchronizationResult
    {
        public int CreatedGroups { get; set; } = 0;

        public int CreatedUsers { get; set; } = 0;

        public int DeletedGroups { get; set; } = 0;

        public int DeletedUsers { get; set; } = 0;

        public int UpdatedGroups { get; set; } = 0;

        public int UpdatedUsers { get; set; } = 0;
    }
}