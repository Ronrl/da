﻿namespace DeskAlerts.ApplicationCore.Dtos
{
    public class ClientApplicationDto
    {
        public string DeskbarId { get; set; }

        public string Version { get; set; }
    }
}
