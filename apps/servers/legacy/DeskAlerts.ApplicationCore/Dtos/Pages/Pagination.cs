﻿namespace DeskAlerts.ApplicationCore.Dtos
{
    public class Pagination
    {
        public bool Descending { get; set; }

        public int Page { get; set; }

        public int RowsNumber { get; set; }

        public int RowsPerPage { get; set; }

        public string SortBy { get; set; }

        public string Search { get; set; }
    }
}