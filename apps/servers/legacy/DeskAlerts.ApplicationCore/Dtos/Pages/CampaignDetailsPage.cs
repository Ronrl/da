﻿using DeskAlerts.ApplicationCore.Interfaces;
using System.Collections.Generic;

namespace DeskAlerts.ApplicationCore.Dtos.Pages
{
    public class CampaignDetailsPage : IPage
    {
        public Pagination Pagination { get; set; }

        public long CampaignId { get; set; }

        public List<AlertDto> Alerts { get; set; }

        public bool CanCreate { get; set; }

        public bool CanView { get; set; }

        public bool CanEdit { get; set; }

        public bool CanDelete { get; set; }

        public bool CanSend { get; set; }

        public bool CanStop { get; set; }
    }
}