﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeskAlerts.ApplicationCore.Entities
{
    using PetaPoco;

    public enum PushSendingStatus
    {
        Success = 1,
        RetryAfterException = 2,
        DeviceSubscriptionExpiredException = 3,
        GcmNotificationException = 4,
        GcmMulticastResultException = 5,
        NotSent = 6
    }

    [TableName("push_sent")]
    [PrimaryKey("id")]
    [ExplicitColumns]

    public class PushSent : EntityBase<int>
    {
        [Column("alert_id")]
        public long AlertId { get; set; }

        [Column("sent_date")]
        public DateTime Date { get; set; }

        [Column("status")]
        public int Status { get; set; }

        [Column("try_count")]
        public int TryCount { get; set; } = 1;

        [Column("user_instance_id")]
        public Guid UserInstanceId { get; set; }
    }
}
