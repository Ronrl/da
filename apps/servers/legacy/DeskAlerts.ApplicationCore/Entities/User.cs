﻿namespace DeskAlerts.ApplicationCore.Entities
{
    using System;

    using PetaPoco;

    [TableName("users")]
    [PrimaryKey("id")]
    [ExplicitColumns]
    public class User : EntityBase<long>
    {
        public const string AdminRole = "A";

        public const string PublisherRole = "E";

        public const string UserRole = "U";

        [Column("client_version")]
        public string Version { get; set; } = string.Empty;

        [Column("display_name")]
        public string DisplayName { get; set; } = string.Empty;

        [Column("domain_id")]
        public long DomainId { get; set; } = 0;

        [Column("email")]
        public string Email { get; set; } = string.Empty;

        [Column("last_request")]
        public DateTime LastRequestDateTime { get; set; } = new DateTime(1970, 1, 1);

        [Column("time_zone")]
        public int TimeZone { get; set; }

        [Column("pass")]
        public string Password { get; set; } = string.Empty;

        [Column("mobile_phone")]
        public string Phone { get; set; } = string.Empty;

        [Column("next_request")]
        public string PollPeriod { get; set; } = "300";

        [Column("reg_date")]
        public DateTime RegistrationDateTime { get; set; } = DateTime.Now;

        [Column("role")]
        public string Role { get; set; } = string.Empty;

        [Column("name")]
        public string Username { get; set; } = string.Empty;

        [Column("start_page")]
        public string StartPage { get; set; } = string.Empty;

        [Column("language_id")]
        public int LanguageId { get; set; }
    }
}