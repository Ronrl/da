﻿using System;
using PetaPoco;

namespace DeskAlerts.ApplicationCore.Entities
{
    [TableName("user_instance_ip")]
    [PrimaryKey("id")]
    [ExplicitColumns]
    public class UserInstanceIps : EntityBase<Guid>
    {
        [Column("user_instance_guid")]
        public Guid UserInstancesGuid { get; set; } = Guid.NewGuid();

        [Column("user_instance_ip")]
        public string UserInstanceIpField { get; set; } = string.Empty;
    }
}