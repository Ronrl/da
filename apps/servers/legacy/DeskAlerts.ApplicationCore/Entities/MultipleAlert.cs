﻿using PetaPoco;

namespace DeskAlerts.ApplicationCore.Entities
{
    [TableName("multiple_alerts")]
    [PrimaryKey("id")]
    [ExplicitColumns]
    public class MultipleAlert : EntityBase<int>
    {
        [Column("alert_id")]
        public int AlertId { get; set; }

        [Column("part_id")]
        public int PartId { get; set; }

        [Column("part_content")]
        public string PartContent { get; set; }

        [Column("confirmed")]
        public int IsConfirmed { get; set; }

        [Column("stat_alert_id")]
        public string StatAlertId { get; set; }
    }
}

