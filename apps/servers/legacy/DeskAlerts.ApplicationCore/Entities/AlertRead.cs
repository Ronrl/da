﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using PetaPoco;

namespace DeskAlerts.ApplicationCore.Entities
{
    [TableName("alerts_read")]
    [PrimaryKey("id")]
    [ExplicitColumns]
    public class AlertRead : EntityBase<long>
    {
        [Column("alert_id")]
        public long AlertId { get; set; }

        [Column("user_id")]
        public long UserId { get; set; }

        [Ignore]
        [Column("closetime")]
        public DateTime CloseTime { get; set; }
        
        [Ignore]
        [Column("read")]
        public bool Read { get; set; }

        [Column("date")]
        public DateTime Date { get; set; }
    }
}
