﻿namespace DeskAlerts.ApplicationCore.Entities
{
    using PetaPoco;

    [TableName("policy_editor")]
    [PrimaryKey("id")]
    [ExplicitColumns]
    public class PolicyPublisher : EntityBase<long>
    {
        [Column("policy_id")]
        public long PolicyId { get; set; }

        [Column("editor_id")]
        public long PublisherId { get; set; }

        [Column("group_id")]
        public long GroupId { get; set; }
    }
}