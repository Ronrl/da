﻿namespace DeskAlerts.ApplicationCore.Entities
{
    using System.Collections.Generic;

    using PetaPoco;

    [TableName("groups")]
    [PrimaryKey("id")]
    [ExplicitColumns]
    public class Group : EntityBase<long>
    {
        [Column("domain_id")]
        public long DomainId { get; set; } = 0;

        [Ignore]
        public List<Group> GroupsMembers { get; set; } = new List<Group>();

        [Column("name")]
        public string Name { get; set; } = string.Empty;

        [Ignore]
        public List<User> UsersMembers { get; set; } = new List<User>();

        [Column("policy_id")]
        public long? PolicyId { get; set; }

        [Ignore]
        public Policy Policy { get; set; }

        [Ignore]
        public Domain Domain { get; set; }
    }
}