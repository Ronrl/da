﻿namespace DeskAlerts.ApplicationCore.Entities
{
    using PetaPoco;
    using System;

    [TableName("alerts")]
    [PrimaryKey("id")]
    [ExplicitColumns]
    public class AlertEntity : EntityBase<long>
    {
        [Column("campaign_id")]
        public long CampaignId { get; set; }

        [Column("title")]
        public string Title { get; set; } = string.Empty;

        [Column("alert_text")]
        public string Body { get; set; }

        [Column("create_date")]
        public DateTime CreateDate { get; set; }

        [Column("schedule_type")]
        public int ScheduleType { get; set; }

        [Column("autoclose")]
        public string Autoclose { get; set; }

        [Column("schedule")]
        public int Schedule { get; set; }

        [Column("terminated")]
        public int Terminated { get; set; }

        [Column("sender_id")]
        public long SenderId { get; set; }

        [Ignore]
        public User Sender { get; set; }

        [Ignore]
        public bool IsScheduled => Schedule == 1;

        [Column("from_date")]
        public DateTime? FromDate { get; set; } = null;

        [Ignore]
        public bool IsExpired => ToDate != null && ToDate < DateTime.Now;

        [Column("to_date")]
        public DateTime? ToDate { get; set; } = null;

        [Ignore]
        public bool IsStarted => FromDate != null && FromDate < DateTime.Now;

        [Ignore]
        public bool IsActive => IsStarted && !IsExpired && !IsScheduled;

        [Column("recurrence")]
        public int Recurrence { get; set; } = 0;

        [Column("is_repeatable_template")]
        public bool IsPepeatableTemplate { get; set; } = false;

        [Column("parent_template_id")]
        public long ParentTemplateId { get; set; }

        [Column("urgent")]
        public int Urgent { get; set; } = 0;

        [Column("class")]
        public int Class { get; set; }

        [Column("device")]
        public int Device { get; set; }

        [Column("aknown")]
        public int Aknown { get; set; }

        [Column("ticker")]
        public int Ticker { get; set; }

        [Column("fullscreen")]
        public string Fullscreen { get; set; }

        [Column("ticker_speed")]
        public int TickerSpeed { get; set; }

        [Column("alert_width")]
        public int Width { get; set; }

        [Column("alert_height")]
        public int Height { get; set; }

        [Column("email")]
        public int Email { get; set; }

        [Column("sms")]
        public int Sms { get; set; }

        [Column("caption_id")]
        public Guid? CaptionId { get; set; }

        [Column("resizable")]
        public bool Resizable { get; set; }

        [Column("self_deletable")]
        public bool SelfDeletable { get; set; }

        [Column("text_to_call")]
        public int TextToCall { get; set; }

        [Column("type")]
        public char SendType { get; set; }

        [Column("approve_status")]
        public int ApproveStatus { get; set; }

        [Column("lifetime")]
        public int Lifetime { get; set; }
        /*
         * B - broadcast content
         * R - contetn for recipients
         * I - content for Ip Groups
         */
        [Column("type2")]
        public string Type2 { get; set; }

        [Ignore]
        public AlertType Type
        {
            get
            {
                switch (Class)
                {
                    case 1:
                        return AlertType.SimpleAlert;
                    case 2:
                        return AlertType.ScreenSaver;
                    case 3:
                        return AlertType.Ticker;
                    case 4:
                        return AlertType.RssFeed;
                    case 5:
                        return AlertType.RssAlert;
                    case 6:
                        return AlertType.Rsvp;
                    case 8:
                        return AlertType.Wallpaper;
                    case 16:
                        return AlertType.EmergencyAlert;
                    case 32:
                        return AlertType.VideoAlert;
                    case 64:
                        return AlertType.SimpleSurvey;
                    case 128:
                        return AlertType.SurveyQuiz;
                    case 256:
                        return AlertType.SurveyPoll;
                    case 2048:
                        return AlertType.DigitalSignature;
                    case 4096:
                        return AlertType.LockScreen;
                    default:
                        return AlertType.Undefined;
                }
            }
        }
    }

    public enum AlertType
    {
        Undefined = 0,
        SimpleAlert = 1,
        ScreenSaver = 2,
        Ticker = 3,
        RssFeed = 4,
        RssAlert = 5,
        Rsvp = 6,
        Wallpaper = 8,
        EmergencyAlert = 16,
        VideoAlert = 32,
        SimpleSurvey = 64,
        SurveyQuiz = 128,
        SurveyPoll = 256,
        DigitalSignature = 2048,
        LockScreen = 4096,
        Terminated
    }
}

