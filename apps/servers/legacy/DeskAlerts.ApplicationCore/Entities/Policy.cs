﻿namespace DeskAlerts.ApplicationCore.Entities
{
    using PetaPoco;
    
    [TableName("policy")]
    [PrimaryKey("id")]
    [ExplicitColumns]
    public class Policy : EntityBase<long>
    {
        [Column("name")]
        public string Name { get; set; }

        [Column("type")]
        public string Type { get; set; }

        [Column("full_skin_access")]
        public bool FullSkinAccess { get; set; }

        [Column("full_contentSettings_access")]
        public bool FullContentSettingsAccess { get; set; }

        [Ignore]
        public PolicyList PolicyList { get; set; }

        [Ignore]
        public PolicyViewer PolicyViewer { get; set; }

        [Ignore]
        public PolicyPublisher PolicyEditor { get; set; }
    }
}