﻿namespace DeskAlerts.ApplicationCore.Entities
{
    using System.Collections.Generic;

    using PetaPoco;

    [TableName("policy_list")]
    [PrimaryKey("id")]
    [ExplicitColumns]
    public class PolicyList : EntityBase<long>
    {
        [Column("name")]
        public string Name { get; set; }

        [Column("policy_id")]
        public long PolicyId { get; set; }

        [Column("alerts_val")]
        public string Alerts { get; set; }

        [Column("emails_val")]
        public string Emails { get; set; }

        [Column("sms_val")]
        public string Sms { get; set; }

        [Column("surveys_val")]
        public string Surveys { get; set; }

        [Column("users_val")]
        public string Users { get; set; }

        [Column("groups_val")]
        public string Groups { get; set; }

        [Column("templates_val")]
        public string Templates { get; set; }

        [Column("text_templates_val")]
        public string TextTemplates { get; set; }

        [Column("statistics_val")]
        public string Statistics { get; set; }

        [Column("ip_groups_val")]
        public string IpGroups { get; set; }

        [Column("rss_val")]
        public string Rss { get; set; }

        [Column("screensavers_val")]
        public string Screensavers { get; set; }

        [Column("wallpapers_val")]
        public string Wallpapers { get; set; }

        [Column("lockscreens_val")]
        public string Lockscreens { get; set; }

        [Column("im_val")]
        public string Im { get; set; }

        [Column("text_to_call_val")]
        public string TextToCall { get; set; }

        [Column("feedback_val")]
        public string Feedback { get; set; }

        [Column("cc_val")]
        public string Cc { get; set; }

        [Column("webplugin_val")]
        public string Webplugin { get; set; }

        [Column("video_val")]
        public string Video { get; set; }

        [Column("campaign_val")]
        public string Campaign { get; set; }
    }
}