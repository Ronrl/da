﻿namespace DeskAlerts.ApplicationCore.Entities
{
    using PetaPoco;

    [TableName("policy_user")]
    [PrimaryKey("id")]
    [ExplicitColumns]
    public class PolicyUser : EntityBase<long>
    {
        [Column("policy_id")]
        public long PolicyId { get; set; }

        [Column("user_id")]
        public long UserId { get; set; }
    }
}