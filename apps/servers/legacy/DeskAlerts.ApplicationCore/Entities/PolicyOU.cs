﻿namespace DeskAlerts.ApplicationCore.Entities
{
    using PetaPoco;

    [TableName("policy_ou")]
    [PrimaryKey("id")]
    [ExplicitColumns]
    public class PolicyOU : EntityBase<long>
    {
        [Column("policy_id")]
        public long PolicyId { get; set; }

        [Column("ou_id")]
        public long OUId { get; set; }
    }
}