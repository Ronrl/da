﻿using PetaPoco;

namespace DeskAlerts.ApplicationCore.Entities
{
    [TableName("ContentSettings")]
    [PrimaryKey("Id")]
    [ExplicitColumns]
    public class ContentSettings : EntityBase<long>
    {
        [Column("Name")]
        public string Name { get; set; }
    }
}
