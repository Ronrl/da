﻿namespace DeskAlerts.ApplicationCore.Entities
{
    using System.Collections.Generic;

    using PetaPoco;

    [TableName("domains")]
    [PrimaryKey("id")]
    [ExplicitColumns]
    public class Domain : EntityBase<long>
    {
        [Ignore]
        public List<Group> GroupsMembers { get; set; } = new List<Group>();

        [Column("name")]
        public string Name { get; set; } = string.Empty;

        [Ignore]
        public List<User> UsersMembers { get; set; } = new List<User>();
    }
}