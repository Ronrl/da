﻿using PetaPoco;

namespace DeskAlerts.ApplicationCore.Entities
{
    [TableName("OU")]
    [PrimaryKey("Id")]
    [ExplicitColumns]
    public class OrganizationUnit : EntityBase<long>
    {
        [Column("Id_domain")]
        public long DomainId { get; set; }

        [Column("Name")]
        public string Name { get; set; }

        [Column("OU_PATH")]
        public string OuPath { get; set; }

        [Column("Was_checked")]
        public string WasChecked { get; set; }

        [Column("type")]
        public string Type { get; set; }
    }
}
