﻿namespace DeskAlerts.ApplicationCore.Entities
{
    public class EntityBase<T>
    {
        [PetaPoco.Column("id")]
        public T Id { get; set; } = default(T);
    }
}