﻿namespace DeskAlerts.ApplicationCore.Entities.AD
{
    using PetaPoco;

    [TableName("policy")]
    [PrimaryKey("id")]
    [ExplicitColumns]
    public class OU : EntityBase<long>
    {
        [Column("name")]
        public string Name { get; set; }
        [Column("OU_PATH")]
        public string Path { get; set; }
        [Column("Id_domain")]
        public long DomainId { get; set; }
    }
}
