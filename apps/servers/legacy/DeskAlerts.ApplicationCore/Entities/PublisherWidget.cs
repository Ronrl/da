﻿using System;

namespace DeskAlerts.ApplicationCore.Entities
{
    using PetaPoco;

    [TableName("widgets_editors")]
    [PrimaryKey("id")]
    [ExplicitColumns]
    public class PublisherWidget : EntityBase<Guid>
    {
        [Column("editor_id")]
        public long PublisherId { get; set; }

        [Column("widget_id")]
        public Guid WidgetId { get; set; }

        [Column("parameters")]
        public int Priority { get; set; }

        [Column("page")]
        public string Page { get; set; }

        [Ignore]
        public Widget Widget { get; set; }
    }
}