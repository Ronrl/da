﻿namespace DeskAlerts.ApplicationCore.Entities
{
    using PetaPoco;

    [TableName("recurrence")]
    [PrimaryKey("id")]
    [ExplicitColumns]
    public class Recurrence : EntityBase<long>
    {
        [Column("alert_id")]
        public long AlertId { get; set; }

        [Column("pattern")]
        public string Pattern { get; set; }

        [Column("number_days")]
        public int NumberDays { get; set; }

        [Column("number_month")]
        public int NumberMonth { get; set; }

        [Column("week_days")]
        public int WeekDays { get; set; }
    }
}