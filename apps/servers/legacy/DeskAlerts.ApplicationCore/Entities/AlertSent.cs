﻿using PetaPoco;

namespace DeskAlerts.ApplicationCore.Entities
{
    [TableName("alerts_sent")]
    [PrimaryKey("id")]
    [ExplicitColumns]
    public class AlertSent : EntityBase<long>
    {
        [Column("alert_id")]
        public long AlertId { get; set; }

        [Column("user_id")]
        public long UserId { get; set; }
    }
}
