﻿using PetaPoco;

namespace DeskAlerts.ApplicationCore.Entities
{
    [TableName("ip_range_groups")]
    [PrimaryKey("id")]
    [ExplicitColumns]
    public class IpRangeGroup : EntityBase<long>
    {
        [Column("group_id")]
        public int GroupId { get; set; }

        [Column("from_ip")]
        public string FromIp { get; set; }

        [Column("to_ip")]
        public string ToIp { get; set; }
    }
}