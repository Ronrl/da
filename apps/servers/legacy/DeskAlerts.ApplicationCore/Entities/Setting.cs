﻿namespace DeskAlerts.ApplicationCore.Entities
{
    using System;

    using PetaPoco;

    [TableName("settings")]
    [PrimaryKey("id")]
    [ExplicitColumns]
    public class Setting : EntityBase<long>
    {
        [Column("val")]
        public string value { get; set; } = string.Empty;
    }
}