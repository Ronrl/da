﻿using System;
using PetaPoco;

namespace DeskAlerts.ApplicationCore.Entities
{
    [TableName("alert_captions")]
    [PrimaryKey("id")]
    [ExplicitColumns]
    public class Skin : EntityBase<Guid>
    {
        [Column("name")]
        public string Name { get; set; }

        [Column("file_name")]
        public string SkinFile { get; set; }
    }
}