﻿namespace DeskAlerts.ApplicationCore.Entities
{
    using PetaPoco;

    [TableName("policy_group")]
    [PrimaryKey("id")]
    [ExplicitColumns]
    public class PolicyGroup : EntityBase<long>
    {
        [Column("policy_id")]
        public long PolicyId { get; set; }

        [Column("group_id")]
        public long GroupId { get; set; }
    }
}