﻿using System;
using PetaPoco;

namespace DeskAlerts.ApplicationCore.Entities
{
    [TableName("user_instance_computers")]
    [PrimaryKey("id")]
    [ExplicitColumns]
    public class UserInstanceComputer : EntityBase<Guid>
    {
        [Column("user_instance_guid")]
        public Guid UserInstanceGuid { get; set; } = Guid.NewGuid();

        [Column("user_instance_computer_id")]
        public long UserInstanceComputerId { get; set; }
    }
}