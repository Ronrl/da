﻿namespace DeskAlerts.ApplicationCore.Entities
{
    using PetaPoco;

    [TableName("policy_viewer")]
    [PrimaryKey("id")]
    [ExplicitColumns]
    public class PolicyViewer : EntityBase<long>
    {
        [Column("policy_id")]
        public long PolicyId { get; set; }

        [Column("editor_id")]
        public long PublisherId { get; set; }
    }
}