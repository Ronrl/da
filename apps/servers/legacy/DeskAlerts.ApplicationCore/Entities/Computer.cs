﻿using System;

namespace DeskAlerts.ApplicationCore.Entities
{
    using PetaPoco;

    [TableName("computers")]
    [PrimaryKey("id")]
    [ExplicitColumns]

    public class Computer : EntityBase<long>
    {
        [Column("name")]
        public string Name { get; set; }

        [Column("domain_id")]
        public long DomainId { get; set; }

        [Column("was_checked")]
        public string Enabled { get; set; }

        [Column("last_request")]
        public DateTime? LastRequest { get; set; }
    }
}
