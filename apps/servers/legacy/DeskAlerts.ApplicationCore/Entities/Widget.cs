﻿using System;

namespace DeskAlerts.ApplicationCore.Entities
{
    using PetaPoco;

    [TableName("widgets")]
    [PrimaryKey("id")]
    [ExplicitColumns]
    public class Widget : EntityBase<Guid>
    {
        [Column("title")]
        public string Title { get; set; } = string.Empty;

        [Column("href")]
        public string Href { get; set; } = string.Empty;

        [Column("priority")]
        public int Priority { get; set; }
    }
}