﻿using DeskAlerts.ApplicationCore.Enums;

namespace DeskAlerts.ApplicationCore.Entities
{
    using System;
    using PetaPoco;

    [TableName("user_instances")]
    [PrimaryKey("id")]
    [ExplicitColumns]
    public class UserInstances : EntityBase<Guid>
    {
        [Column("user_id")]
        public long UserId { get; set; } = 0;

        [Column("clsid")]
        public Guid Clsid { get; set; }

        [Column("device_type")]
        public DeviceType Type { get; set; }

        [Column("device_id")]
        public string PushToken { get; set; } = string.Empty;

        [Column("android_sdk_int")]
        public int AndroidSdkInt { get; set; } = 0;
    }
}