﻿using DeskAlerts.ApplicationCore.Enums;

namespace DeskAlerts.ApplicationCore.Entities
{
    using PetaPoco;
    using System;

    [TableName("alerts_received")]
    [PrimaryKey("id")]
    [ExplicitColumns]

    public class AlertReceived : EntityBase<long>
    {
        [Column("alert_id")]
        public long AlertId { get; set; }

        [Column("user_id")]
        public long UserId { get; set; }

        [Column("clsid")]
        public string DeskbarId { get; set; }

        [Column("date")]
        public DateTime Date { get; set; }

        [Column("vote")]
        public int? Vote { get; set; }

        [Column("occurrence")]
        public long Occurrence { get; set; }

        [Column("status")]
        public AlertReceiveStatus Status { get; set; }

        [Column("computer_id")]
        public long? ComputerId { get; set; }

        [Column("terminated")]
        public int TerminateStatus { get; set; }

        [Column("ip_address")]
        public string IpAddress { get; set; }
    }
}
