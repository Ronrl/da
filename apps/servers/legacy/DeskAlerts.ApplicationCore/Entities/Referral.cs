﻿namespace DeskAlerts.ApplicationCore.Entities
{
    using System;

    using PetaPoco;

    [TableName("referrals")]
    [PrimaryKey("id")]
    [ExplicitColumns]
    public class Referral : EntityBase<long>
    {
        [Column("email")]
        public string Email { get; set; } = string.Empty;

        [Column("visit_date")]
        public DateTime VisitDate { get; set; } = new DateTime(1970, 1, 1);
    }
}