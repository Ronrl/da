﻿using PetaPoco;

namespace DeskAlerts.ApplicationCore.Entities
{
    [TableName("policy_skins")]
    [PrimaryKey("id")]
    [ExplicitColumns]
    public class PolicySkins : EntityBase<long>
    {
        [Column("policy_id")]
        public string PolicyId { get; set; }

        [Column("skin_id")]
        public string SkinId { get; set; }
    }
}