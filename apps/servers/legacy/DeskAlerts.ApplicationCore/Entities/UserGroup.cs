﻿namespace DeskAlerts.ApplicationCore.Entities
{
    using PetaPoco;

    [TableName("users_groups")]
    [PrimaryKey("id")]
    [ExplicitColumns]
    public class UserGroup : EntityBase<long>
    {
        [Column("group_id")]
        public long GroupId { get; set; }

        [Column("user_id")]
        public long UserId { get; set; }
    }
}