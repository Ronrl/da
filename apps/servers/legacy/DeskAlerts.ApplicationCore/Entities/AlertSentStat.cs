﻿using System;
using PetaPoco;

namespace DeskAlerts.ApplicationCore.Entities
{
    [TableName("alerts_sent_stat")]
    [PrimaryKey("id")]
    [ExplicitColumns]
    public class AlertSentStat : EntityBase<long>
    {
        [Column("alert_id")]
        public long AlertId { get; set; }

        [Column("user_id")]
        public long UserId { get; set; }

        [Column("date")]
        public DateTime Date { get; set; }
    }
}
