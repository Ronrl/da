﻿namespace DeskAlerts.ApplicationCore.Entities
{
    using System;

    using PetaPoco;

    [TableName("campaigns")]
    [PrimaryKey("id")]
    [ExplicitColumns]
    public class Campaign : EntityBase<long>
    {
        [Column("name")]
        public string Name { get; set; } = string.Empty;

        [Column("create_date")]
        public DateTime? CreateDate { get; set; } = null;

        [Column("start_date")]
        public DateTime? StartDate { get; set; } = null;

        [Column("end_date")]
        public DateTime? EndDate { get; set; } = null;

        [Column("sender_id")]
        public long SenderId { get; set; }

        [Column("is_active")]
        public bool IsActive { get; set; }

        [Ignore]
        public CampaignStatus Status
        {
            get
            {
                if (IsPending)
                {
                    return CampaignStatus.Pending;
                }
                else if (IsActive && IsStarted && !IsFinished)
                {
                    return CampaignStatus.Active;
                }
                else if (!IsActive && IsStarted)
                {
                    return CampaignStatus.Stopped;
                }
                else if (IsFinished)
                {
                    return CampaignStatus.Finished;
                }
                else
                {
                    return CampaignStatus.Invative;
                }
            }
        }

        [Ignore]
        public bool IsStarted
        {
            get
            {
                return StartDate < DateTime.Now && StartDate != null;
            }
        }

        [Ignore]
        public bool IsPending
        {
            get
            {
                return StartDate > DateTime.Now && StartDate != null;
            }
        }

        [Ignore]
        public bool IsFinished
        {
            get
            {
                return EndDate < DateTime.Now && EndDate != null;
            }
        }
    }

    public enum CampaignStatus
    {
        Invative = 1,
        Pending = 2,
        Active = 3,
        Stopped = 4,
        Finished = 5
    }
}