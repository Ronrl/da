﻿using System;

namespace DeskAlerts.ApplicationCore.Structs
{
    public struct ContentSettings
    {
        public enum ContentSettingsEnum
        {
            Acknowledgement = 1,
            AutoClose = 2,
            FullScreen = 3,
            HighPriority = 4,
            PrintButton = 5,
            SelfDestructing = 6,
            Unobtrusive = 7
        }

        public static string GetName(ContentSettingsEnum contentOptions)
        {
            string result;
            switch (contentOptions)
            {
                case ContentSettingsEnum.Acknowledgement:
                    result = "Acknowledgement";
                    break;

                case ContentSettingsEnum.AutoClose:
                    result = "Auto-close";
                    break;

                case ContentSettingsEnum.FullScreen:
                    result = "Full screen";
                    break;

                case ContentSettingsEnum.HighPriority:
                    result = "High-priority";
                    break;

                case ContentSettingsEnum.PrintButton:
                    result = "Print button";
                    break;

                case ContentSettingsEnum.SelfDestructing:
                    result = "Self-destructing";
                    break;

                case ContentSettingsEnum.Unobtrusive:
                    result = "Unobtrusive";
                    break;

                default:
                    throw new ArgumentOutOfRangeException(nameof(contentOptions), contentOptions, null);
            }

            return result;
        }
    }
}
