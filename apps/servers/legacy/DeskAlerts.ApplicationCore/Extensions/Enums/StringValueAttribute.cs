﻿using System;

namespace DeskAlerts.ApplicationCore.Extensions.Enums
{
    public class StringValueAttribute : Attribute
    {
        public string StringValue { get; protected set; }

        public StringValueAttribute(string value)
        {
            StringValue = value;
        }
    }
}