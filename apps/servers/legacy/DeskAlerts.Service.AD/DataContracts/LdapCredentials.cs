﻿namespace DeskAlerts.Service.AD.DataContracts
{
    using System;
    using System.Runtime.Serialization;

    [DataContract]
    public class LdapCredentials
    {
        [DataMember]
        public string Host { get; set; }

        [DataMember]
        public int Port { get; set; }

        [DataMember]
        public string BaseDN { get; set; }

        [DataMember]
        public string UserName { get; set; }

        [DataMember]
        public string Password { get; set; }

        [DataMember]
        public TimeSpan Timeout { get; set; }
    }
}
