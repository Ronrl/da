﻿namespace DeskAlerts.Service.AD.DataContracts
{
    using System.Runtime.Serialization;

    [DataContract]
    public class Group
    {
        [DataMember]
        public string Path { get; set; }
    }
}
