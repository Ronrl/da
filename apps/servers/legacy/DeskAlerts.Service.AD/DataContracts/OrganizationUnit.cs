﻿namespace DeskAlerts.Service.AD.DataContracts
{
    using System.Runtime.Serialization;

    [DataContract]
    public class OrganizationUnit
    {
        [DataMember]
        public string Path { get; set; }
    }
}
