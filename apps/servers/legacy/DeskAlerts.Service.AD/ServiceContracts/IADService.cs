﻿namespace DeskAlerts.Service.AD.ServiceContracts
{
    using DeskAlerts.Service.AD.DataContracts;
    using System.Collections.Generic;
    using System.ServiceModel;

    [ServiceContract]
    public interface IADService
    {
        [OperationContract]
        bool CheckConnection();

        [OperationContract]
        List<OrganizationUnit> GetOrganizationUnits();

        [OperationContract]
        List<Group> GetGroupsByName(string groupName);
    }
}
