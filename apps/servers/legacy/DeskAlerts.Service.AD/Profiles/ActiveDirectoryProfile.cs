﻿namespace DeskAlerts.Service.AD.Profiles
{
    using AutoMapper;
    using DeskAlerts.Service.AD.DataContracts;
    using System.DirectoryServices;

    public class ActiveDirectoryProfile : Profile
    {
        public ActiveDirectoryProfile()
        {
            CreateMap<SearchResult, OrganizationUnit>()
                .ForMember(destination => destination.Path,
                opts => opts.MapFrom(source => source.Path));

            CreateMap<SearchResult, Group>()
                .ForMember(destination => destination.Path,
                opts => opts.MapFrom(source => source.Path));
        }
    }
}
