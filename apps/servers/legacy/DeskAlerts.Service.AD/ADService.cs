﻿namespace DeskAlerts.Service.AD
{
    using AutoMapper;
    using DeskAlerts.Service.AD.DataContracts;
    using DeskAlerts.Service.AD.DirectorySearchers;
    using DeskAlerts.Service.AD.ServiceContracts;
    using System;
    using System.Collections.Generic;
    using System.DirectoryServices;

    public class ADService : IADService, IDisposable
    {
        private DirectoryEntry _directoryEntry;

        public ADService() { }

        public ADService(LdapCredentials cr)
        {
            _directoryEntry = new DirectoryEntry($"LDAP://{cr.Host}:{cr.Port}/{cr.BaseDN}", cr.UserName, cr.Password)
            {
                AuthenticationType = AuthenticationTypes.Secure
            };
        }

        /// <summary>
        /// Check connection with Active Directory
        /// </summary>
        /// <returns></returns>
        public bool CheckConnection()
        {
            using (var ds = new DirectorySearcher(_directoryEntry))
            {
                ds.SearchScope = SearchScope.OneLevel;
                ds.FindOne();
                return true;
            }
        }

        /// <summary>
        /// Get Urginization Units
        /// </summary>
        /// <returns></returns>
        public List<OrganizationUnit> GetOrganizationUnits()
        {
            using (var ds = new OuFilter(_directoryEntry))
            {
                var orgUnits = new List<OrganizationUnit>();
                foreach (SearchResult sr in ds.FindAll())
                {
                    orgUnits.Add(Mapper.Map<SearchResult, OrganizationUnit>(sr));
                }
                return orgUnits;
            };
        }

        /// <summary>
        /// Get Groups by name
        /// </summary>
        /// <param name="groupName">Name of the group</param>
        /// <returns></returns>
        public List<Group> GetGroupsByName(string groupName)
        {
            using (var ds = new GroupByNameFilter(_directoryEntry, groupName))
            {
                var orgUnits = new List<Group>();
                foreach (SearchResult sr in ds.FindAll())
                {
                    orgUnits.Add(Mapper.Map<SearchResult, Group>(sr));
                }
                return orgUnits;
            };
        }

        public void Dispose()
        {
            _directoryEntry.Close();
        }
    }
}
