﻿namespace DeskAlerts.Service.AD.DirectorySearchers
{
    using System.DirectoryServices;

    public class OuFilter : DirectorySearcher
    {
        private DirectoryEntry _directoryEntry;

        public OuFilter(DirectoryEntry directoryEntry)
        {
            _directoryEntry = directoryEntry;
            Filter = "(objectCategory=organizationalUnit)";
            PageSize = 1; // Get more than 1000 records
        }
    }
}
