﻿namespace DeskAlerts.Service.AD.DirectorySearchers
{
    using System.DirectoryServices;

    public class GroupByNameFilter : DirectorySearcher
    {
        private DirectoryEntry _directoryEntry;

        public GroupByNameFilter(DirectoryEntry directoryEntry, string groupName)
        {
            _directoryEntry = directoryEntry;
            Filter = $"(&(objectCategory=group)(cn=*{groupName}*))";
            PageSize = 1; // Get more than 1000 records
        }
    }
}
