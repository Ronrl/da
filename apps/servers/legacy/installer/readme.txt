STEP BY STEP INSTALLING DESKALERTS
1. Before DeskAlerts installation, make sure MS SQL and IIS had 
   already been installed. Technical requirements for DeskAlerts:
   http://www.alert-software.com/support/faq/
2. Run DeskAlert server setup using DeskAlerts.Server.v[NUMBERS].exe 
   (i.e. DeskAlerts.Server.v6.0.10.29.exe). Follow setup wizard installation 
   steps. Server will be setup.
3. Run ClientInstallation.exe to build a client application according to your 
   needs. Follow step by step wizard in there.
4. As a result you will get deskalert_setup.exe and deskalert_setup.msi installers 
   for Windows (and deskalerts_setup.zip for Mac and DeskAlerts_i386.deb/DeskAlerts_amd64.deb for Linux)
5. Install the client application on one or several PC's. If your client application version is less than 5.1.3.x,
   you should perform a clean installation, uninstalling the old client first.
6. Use your browser to navigate to DeskAlert Control Panel URL and use admin/admin to login. Make sure to 
   change the password as you log into the Control Panel

For detailed information we suggest you to check:
Documentation section: http://www.alert-software.com/support/documentation/
FAQ: http://www.alert-software.com/support/faq/

