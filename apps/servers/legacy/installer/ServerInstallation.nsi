 SetCompressor lzma

!define PRODUCT_NAME "DeskAlerts Server"
!define PRODUCT_UPDATE_NAME "DeskAlerts Server Update"
!define PRODUCT_PUBLISHER "Softomate, LLC"
!define PRODUCT_WEB_SITE "http://www.softomate.com"
!define BRANDING_TEXT "DeskAlerts http://www.deskalerts.com"
!define PRODUCT_UNINST_KEY "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_NAME}"
!define PRODUCT_UPDATE_UNINST_KEY "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_UPDATE_NAME}"
!define PRODUCT_UNINST_ROOT_KEY "HKLM"
!define PRODUCT_UNINST_FILEDIR "$WINDIR\$$UninstallDeskAlerts$$"
!define PRODUCT_UNINST_FILENAME "Uninstall"
!define PRODUCT_UPDATE_UNINST_FILENAME "UninstallUpdate"
!define UPDATE_BACKUPDIR "${PRODUCT_UNINST_FILEDIR}\backup"


!define MUI_UI "${NSISDIR}\Contrib\UIs\modern_352_264.exe"
!define MUI_UI_HEADERIMAGE "${NSISDIR}\Contrib\UIs\modern_headerbmp_352_264.exe"

;-------------------------------- Addons Names
!define nameADAddon "AD/LDAP module"
!define nameEDAddon "eDirectory module"
!define nameSMSAddon "SMS module"
!define nameEMAILAddon "E-Mail module"
!define nameENCRYPTAddon "Encryption module"
!define nameSURVEYSAddon "Survey manager module"
!define nameSTATISTICSAddon "Reports module"
!define nameSCREENSAVERAddon "Screensaver module"
!define nameRSSAddon "News feed module"
!define nameWALLPAPERAddon "Wallpaper module"
!define nameBLOGAddon "Blog module"
!define nameTWITTERAddon "Social module for Twitter"
!define nameLINKEDINAddon "Social module for LinkedIn"
!define nameFULLSCREENAddon "Fullscreen alert module"
!define nameVIDEOAddon "Videoalert module"
!define nameTICKERAddon "Scrolling news ticker alert module"
!define nameTICKERPROAddon "Scrolling news ticker PRO module"
!define nameIMAddon "Instant Messages module"
!define nameWIDGETSTATAddon "Widgets Statistic module"
!define nameTEXTCALLAddon "Text to call module"
!define nameMOBILEAddon "Mobile alerts module"
!define nameLicenseCount "Number of licenses: "
!define nameTrialCount "Number of trial days: "
!define nameWEBPLUGINAddon "WebPlugin module"
!define nameCAMPAIGNAddon "Campaign alert module"
!define nameAPPROVEAddon "Approval panel module"
!define nameDIGSIGNAddon "Digital signage module"
!define nameYAMMERAddon "Social module for Yammer"
;--------------------------------
!addplugindir "include"
!addincludedir "include"
!include "include\controls.nsh"
;Include Modern UI
!include "MUI.nsh"
!include "x64.nsh"
!include "StrFunc.nsh"
!include "TextFunc.nsh"
${StrTrimNewLines}
!include "include\StrRep.nsh" ;only custom version
${StrReplace}
${UnStrReplace}
!include "include\ReplaceInFile.nsh" ;only custom version
${ReplaceInFile}
!include "include\ReadConfig.nsh"
${ReadFromConfig}
${UnReadFromConfig}
!include "include\UpdateConfig.nsh"
!include "include\CustomFunction.nsh"
;${StrTok} defined in Trim.nsh
!include "include\CreateTask.nsh"
!include "include\Archive.nsh"
!include "include\Schedule.nsh"
!include "include\RSS.nsh"
!include "include\Sync.nsh"
!include "include\Random.nsh"
!include "include\ClearCache.nsh"
;--------------------------------
;General
;Name and file
Name "${PRODUCT_NAME}"
BrandingText "${BRANDING_TEXT}"
OutFile "..\ServerInstallation.exe"
XPStyle on
RequestExecutionLevel admin

;-------------------------------- ReserveFile ---------
ReserveFile "include\check_for_requirements.ini"
ReserveFile "include\iis_set.ini"
ReserveFile "include\db.ini"
ReserveFile "include\AD_addon.ini"
ReserveFile "include\BLOG_addon.ini"
ReserveFile "include\EMAIL_addon.ini"
ReserveFile "include\ENCRYPT_addon.ini"
ReserveFile "include\FULLSCREEN_addon.ini"
ReserveFile "include\VIDEO_addon.ini"
ReserveFile "include\LINKEDIN_addon.ini"
ReserveFile "include\RSS_addon.ini"
ReserveFile "include\SMS_addon.ini"
ReserveFile "include\SS_addon.ini"
ReserveFile "include\SURVEYS_addon.ini"
ReserveFile "include\TICKER_addon.ini"
ReserveFile "include\TICKERPRO_addon.ini"
ReserveFile "include\IM_addon.ini"
ReserveFile "include\WIDGETSTAT_addon.ini"
ReserveFile "include\TWITTER_addon.ini"
ReserveFile "include\WP_addon.ini"
ReserveFile "include\thanks.ini"
ReserveFile "include\iis.vbs"
ReserveFile "include\ExtStat.ini"
ReserveFile "include\Trial.ini"
ReserveFile "include\TEXTCALL_addon.ini"
ReserveFile "include\MOBILE_addon.ini"
ReserveFile "include\WEBPLUGIN_addon.ini"
ReserveFile "include\CAMPAIGN_addon.ini"
ReserveFile "include\APPROVE_addon.ini"
ReserveFile "include\DIGSIGN_addon.ini"
ReserveFile "include\YAMMER_addon.ini"
;--------------------------------
;Interface Configuration
!define MUI_LANGDLL_REGISTRY_ROOT "${PRODUCT_UNINST_ROOT_KEY}"
!define MUI_LANGDLL_REGISTRY_KEY "${PRODUCT_UNINST_KEY}"
!define MUI_LANGDLL_REGISTRY_VALUENAME "NSIS:Language"

!define MUI_HEADERIMAGE

!ifdef nineServer
	!define MUI_HEADERIMAGE_BITMAP "deskalerts9.bmp" ; optional
	!define MUI_HEADERIMAGE_UNBITMAP "deskalerts9.bmp" ; optional
!else
	!ifdef eightServer
		!define MUI_HEADERIMAGE_BITMAP "deskalerts8.bmp" ; optional
		!define MUI_HEADERIMAGE_UNBITMAP "deskalerts8.bmp" ; optional
	!else
		!ifdef sevenServer
			!define MUI_HEADERIMAGE_BITMAP "deskalerts7.bmp" ; optional
			!define MUI_HEADERIMAGE_UNBITMAP "deskalerts7.bmp" ; optional
		!else
			!ifdef sixServer
				!define MUI_HEADERIMAGE_BITMAP "deskalerts6.bmp" ; optional
				!define MUI_HEADERIMAGE_UNBITMAP "deskalerts6.bmp" ; optional
			!else
				!ifdef fiveServer
					!define MUI_HEADERIMAGE_BITMAP "deskalerts5.bmp" ; optional
					!define MUI_HEADERIMAGE_UNBITMAP "deskalerts5.bmp" ; optional
				!else
					!define MUI_HEADERIMAGE_BITMAP "deskalerts4.bmp" ; optional
					!define MUI_HEADERIMAGE_UNBITMAP "deskalerts4.bmp" ; optional
				!endif
			!endif
		!endif
	!endif
!endif

!define MUI_ABORTWARNING
!define MUI_HEADERIMAGE_BITMAP_NOSTRETCH
!define MUI_HEADER_TRANSPARENT_TEXT
;--------------------------------
;Interface Init
!insertmacro MUI_LANGUAGE "English"
;--------------------------------

;--------------------------------
;Add additional information
VIProductVersion "${PRODUCT_VERSION}"
VIAddVersionKey /LANG=${LANG_ENGLISH} "ProductName" "${PRODUCT_NAME}"
VIAddVersionKey /LANG=${LANG_ENGLISH} "CompanyName" "${PRODUCT_PUBLISHER}"
VIAddVersionKey /LANG=${LANG_ENGLISH} "LegalCopyright" "Copyright (c) 2006-2014 Softomate"
VIAddVersionKey /LANG=${LANG_ENGLISH} "FileVersion" "${PRODUCT_VERSION}"
VIAddVersionKey /LANG=${LANG_ENGLISH} "ProductVersion" "${PRODUCT_VERSION}"
VIAddVersionKey /LANG=${LANG_ENGLISH} "OriginalFilename" "ServerInstallation.exe"
;--------------------------------

;--------------------------------
;Add additional information
!define delim ""
!define lastPage "$PLUGINSDIR\db.ini"
!define COMMENTS "Included standard addons:$\r$\n"
!ifdef isADAddon
	!define TMP1 "${COMMENTS}${delim}${nameADAddon}"
	!undef COMMENTS
	!define COMMENTS "${TMP1}"
	!undef lastPage
	!define lastPage "$PLUGINSDIR\AD_addon.ini"
	!undef delim
	!define delim ",$\r$\n"
!endif
!ifdef isEDAddon
	!define TMP2 "${COMMENTS}${delim}${nameEDAddon}"
	!undef COMMENTS
	!define COMMENTS "${TMP2}"
	!undef delim
	!define delim ",$\r$\n"
!endif
!ifdef isSMSAddon
	!define TMP3 "${COMMENTS}${delim}${nameSMSAddon}"
	!undef COMMENTS
	!define COMMENTS "${TMP3}"
	!undef lastPage
	!define lastPage "$PLUGINSDIR\SMS_addon.ini"
	!undef delim
	!define delim ",$\r$\n"
!endif
!ifdef isEMAILAddon
	!define TMP4 "${COMMENTS}${delim}${nameEMAILAddon}"
	!undef COMMENTS
	!define COMMENTS "${TMP4}"
	!undef lastPage
	!define lastPage "$PLUGINSDIR\EMAIL_addon.ini"
	!undef delim
	!define delim ",$\r$\n"
!endif
!ifdef isENCRYPTAddon
	!define TMP5 "${COMMENTS}${delim}${nameENCRYPTAddon}"
	!undef COMMENTS
	!define COMMENTS "${TMP5}"
	!undef lastPage
	!define lastPage "$PLUGINSDIR\ENCRYPT_addon.ini"
	!undef delim
	!define delim ",$\r$\n"
!endif
!ifdef isSURVEYSAddon
	!define TMP6 "${COMMENTS}${delim}${nameSURVEYSAddon}"
	!undef COMMENTS
	!define COMMENTS "${TMP6}"
	!undef lastPage
	!define lastPage "$PLUGINSDIR\SURVEYS_addon.ini"
	!undef delim
	!define delim ",$\r$\n"
!endif
!ifdef isSTATISTICSAddon
	!define TMP7 "${COMMENTS}${delim}${nameSTATISTICSAddon}"
	!undef COMMENTS
	!define COMMENTS "${TMP7}"
	!undef lastPage
	!define lastPage "$PLUGINSDIR\ExtStat.ini"
	!undef delim
	!define delim ",$\r$\n"
!endif
!ifdef isSCREENSAVERAddon
	!define TMP8 "${COMMENTS}${delim}${nameSCREENSAVERAddon}"
	!undef COMMENTS
	!define COMMENTS "${TMP8}"
	!undef lastPage
	!define lastPage "$PLUGINSDIR\SS_addon.ini"
	!undef delim
	!define delim ",$\r$\n"
!endif
!ifdef isRSSAddon
	!define TMP9 "${COMMENTS}${delim}${nameRSSAddon}"
	!undef COMMENTS
	!define COMMENTS "${TMP9}"
	!undef lastPage
	!define lastPage "$PLUGINSDIR\RSS_addon.ini"
	!undef delim
	!define delim ",$\r$\n"
!endif
!ifdef isWALLPAPERAddon
	!define TMP10 "${COMMENTS}${delim}${nameWALLPAPERAddon}"
	!undef COMMENTS
	!define COMMENTS "${TMP10}"
	!undef lastPage
	!define lastPage "$PLUGINSDIR\WP_addon.ini"
	!undef delim
	!define delim ",$\r$\n"
!endif
!ifdef isBLOGAddon
	!define TMP11 "${COMMENTS}${delim}${nameBLOGAddon}"
	!undef COMMENTS
	!define COMMENTS "${TMP11}"
	!undef lastPage
	!define lastPage "$PLUGINSDIR\BLOG_addon.ini"
	!undef delim
	!define delim ",$\r$\n"
!endif
!ifdef isTWITTERAddon
	!define TMP12 "${COMMENTS}${delim}${nameTWITTERAddon}"
	!undef COMMENTS
	!define COMMENTS "${TMP12}"
	!undef lastPage
	!define lastPage "$PLUGINSDIR\TWITTER_addon.ini"
	!undef delim
	!define delim ",$\r$\n"
!endif
!ifdef isLINKEDINAddon
	!define TMP13 "${COMMENTS}${delim}${nameLINKEDINAddon}"
	!undef COMMENTS
	!define COMMENTS "${TMP16}"
	!undef lastPage
	!define lastPage "$PLUGINSDIR\LINKEDIN_addon.ini"
	!undef delim
	!define delim ",$\r$\n"
!endif
!ifdef isFULLSCREENAddon
	!define TMP14 "${COMMENTS}${delim}${nameFULLSCREENAddon}"
	!undef COMMENTS
	!define COMMENTS "${TMP13}"
	!undef lastPage
	!define lastPage "$PLUGINSDIR\FULLSCREEN_addon.ini"
	!undef delim
	!define delim ",$\r$\n"
!endif
!ifdef isTICKERAddon
	!define TMP15 "${COMMENTS}${delim}${nameTICKERAddon}"
	!undef COMMENTS
	!define COMMENTS "${TMP14}"
	!undef lastPage
	!define lastPage "$PLUGINSDIR\TICKER_addon.ini"
	!undef delim
	!define delim ",$\r$\n"
!endif
!ifdef isTICKERPROAddon
	!define TMP16 "${COMMENTS}${delim}${nameTICKERPROAddon}"
	!undef COMMENTS
	!define COMMENTS "${TMP15}"
	!undef lastPage
	!define lastPage "$PLUGINSDIR\TICKERPRO_addon.ini"
	!undef delim
	!define delim ",$\r$\n"
!endif
!ifdef isIMAddon
	!define TMP17 "${COMMENTS}${delim}${nameIMAddon}"
	!undef COMMENTS
	!define COMMENTS "${TMP17}"
	!undef lastPage
	!define lastPage "$PLUGINSDIR\IM_addon.ini"
	!undef delim
	!define delim ",$\r$\n"
!endif
;!ifdef isWIDGETSTATAddon
;	!define TMP18 "${COMMENTS}${delim}${nameWIDGETSTATAddon}"
;	!undef COMMENTS
;	!define COMMENTS "${TMP18}"
;	!undef lastPage
;	!define lastPage "$PLUGINSDIR\WIDGETSTAT_addon.ini"
;	!undef delim
;	!define delim ",$\r$\n"
;!endif
!ifdef isTEXTCALLAddon
	!define TMP18 "${COMMENTS}${delim}${nameTEXTCALLAddon}"
	!undef COMMENTS
	!define COMMENTS "${TMP18}"
	!undef lastPage
	!define lastPage "$PLUGINSDIR\TEXTCALL_addon.ini"
	!undef delim
	!define delim ",$\r$\n"
!endif
!ifdef isMOBILEAddon
	!define TMP19 "${COMMENTS}${delim}${nameMOBILEAddon}"
	!undef COMMENTS
	!define COMMENTS "${TMP19}"
	!undef lastPage
	!define lastPage "$PLUGINSDIR\MOBILE_addon.ini"
	!undef delim
	!define delim ",$\r$\n"
!endif
!ifdef isVIDEOAddon
	!define TMP20 "${COMMENTS}${delim}${nameVIDEOAddon}"
	!undef COMMENTS
	!define COMMENTS "${TMP20}"
	!undef lastPage
	!define lastPage "$PLUGINSDIR\VIDEO_addon.ini"
	!undef delim
	!define delim ",$\r$\n"
!endif
!ifdef isWEBPLUGINAddon
	!define TMP21 "${COMMENTS}${delim}${nameWEBPLUGINAddon}"
	!undef COMMENTS
	!define COMMENTS "${TMP21}"
	!undef lastPage
	!define lastPage "$PLUGINSDIR\WEBPLUGIN_addon.ini"
	!undef delim
	!define delim ",$\r$\n"
!endif
!ifdef isCAMPAIGNAddon
	!define TMP22 "${COMMENTS}${delim}${nameCAMPAIGNAddon}"
	!undef COMMENTS
	!define COMMENTS "${TMP22}"
	!undef lastPage
	!define lastPage "$PLUGINSDIR\CAMPAIGN_addon.ini"
	!undef delim
	!define delim ",$\r$\n"
!endif
!ifdef isAPPROVEAddon
	!define TMP23 "${COMMENTS}${delim}${nameAPPROVEAddon}"
	!undef COMMENTS
	!define COMMENTS "${TMP23}"
	!undef lastPage
	!define lastPage "$PLUGINSDIR\APPROVE_addon.ini"
	!undef delim
	!define delim ",$\r$\n"
!endif
!ifdef isDIGSIGNAddon
	!define TMP24 "${COMMENTS}${delim}${nameDIGSIGNAddon}"
	!undef COMMENTS
	!define COMMENTS "${TMP24}"
	!undef lastPage
	!define lastPage "$PLUGINSDIR\DIGSIGN_addon.ini"
	!undef delim
	!define delim ",$\r$\n"
!endif
!ifdef isYAMMERAddon
	!define TMP25 "${COMMENTS}${delim}${nameYAMMERAddon}"
	!undef COMMENTS
	!define COMMENTS "${TMP25}"
	!undef lastPage
	!define lastPage "$PLUGINSDIR\YAMMER_addon.ini"
	!undef delim
	!define delim ",$\r$\n"
!endif
!ifdef TMP1 | TMP2 | TMP3 | TMP4 | TMP5 | TMP6 | TMP7 | TMP8 | TMP9 | TMP10 | TMP11 | TMP12 | TMP13 | TMP14 | TMP15 | TMP16 | TMP17 | TMP18 | TMP19 | TMP20 | TMP21 | TMP22 | TMP23 | TMP24 | TMP25
!else
	!undef COMMENTS
	!define COMMENTS "Without standard modules"
	!undef delim
	!define delim ",$\r$\n"
!endif
!ifdef licenses
	!define TMP "${COMMENTS}${delim}${nameLicenseCount}${licenses}"
	!undef COMMENTS
	!define COMMENTS "${TMP}"
!endif
!ifdef trial
	!define trialpage 1
	!ifdef TMP
		!undef COMMENTS
		!define COMMENTS "${TMP},$\r$\n${nameTrialCount}${trial}"
	!else
		!define TMP "${COMMENTS}${delim}${nameTrialCount}${trial}"
		!undef COMMENTS
		!define COMMENTS "${TMP}"
	!endif
!else
	!define trial "0"
!endif
VIAddVersionKey /LANG=${LANG_ENGLISH} "Comments" "${COMMENTS}"
;VIAddVersionKey /LANG=${LANG_ENGLISH} "FileDescription" "${PRODUCT_NAME} Installer.$\r$\n${COMMENTS}"
VIAddVersionKey /LANG=${LANG_ENGLISH} "FileDescription" "${PRODUCT_NAME} Installer"
;--------------------------------

;------------------------------------------------------ Requirements var
Var "WindowsVersion"
Var "WindowsVersionText"
Var "IISVersion"
Var "MSSQLVersion"
Var "WindowsVersionOk"
Var "IISVersionOk"
Var "MSSQLVersionOk"
Var "RequirementsOk"

Var "isLocalDB"
Var "NeedToReloadIIS"

;------------------------------------------------------ Standart set var
Var "is_update"
Var "dbupdate"
Var "alerts_folder"
Var "alerts_dir"
Var "alerts_timeout"
Var "host"
Var "dbname"
Var "user_name"
Var "password"
Var "win_auth"

!define ZIP_NAME               "server.zip"
!define INSTALLED_conf         "$alerts_folder\admin\config.inc"
!define MAIN_conf              "AlertServer\AlertServerUnzip\STANDARD\admin\config.inc"
!define MAIN_BLANK_conf        "AlertServer\AlertServerUnzip\STANDARD\admin\config.blank.inc"
!define MAIN_DOTNET_conf 	   "AlertServer\AlertServerUnzip\STANDARD\web.config"
!define MAIN_BLANK_DOTNET_conf "AlertServer\AlertServerUnzip\STANDARD\web.blank.config"
!define AD_MODULE_conf         "AlertServer\AlertServerUnzip\MODULES\AD MODULE\admin\add_to_config.inc"
!define ED_MODULE_conf         "AlertServer\AlertServerUnzip\MODULES\EDIR MODULE\admin\add_to_config.inc"
!define SMS_MODULE_conf        "AlertServer\AlertServerUnzip\MODULES\SMS MODULE\admin\add_to_config.inc"
!define EMAIL_MODULE_conf      "AlertServer\AlertServerUnzip\MODULES\EMAIL MODULE\admin\add_to_config.inc"
!define ENCRYPT_MODULE_conf    "AlertServer\AlertServerUnzip\MODULES\ENCRYPT MODULE\admin\add_to_config.inc"
!define SURVEYS_MODULE_conf    "AlertServer\AlertServerUnzip\MODULES\SURVEYS MODULE\admin\add_to_config.inc"
!define SS_MODULE_conf         "AlertServer\AlertServerUnzip\MODULES\SCREENSAVER\admin\add_to_config.inc"
!define RSS_MODULE_conf        "AlertServer\AlertServerUnzip\MODULES\RSS\admin\add_to_config.inc"
!define WP_MODULE_conf         "AlertServer\AlertServerUnzip\MODULES\WALLPAPER\admin\add_to_config.inc"
!define BLG_MODULE_conf        "AlertServer\AlertServerUnzip\MODULES\BLOG\admin\add_to_config.inc"
!define TWTR_MODULE_conf       "AlertServer\AlertServerUnzip\MODULES\SOCIAL_MEDIA\admin\add_to_config.inc"
!define LNKDN_MODULE_conf      "AlertServer\AlertServerUnzip\MODULES\SOCIAL_LINKEDIN\admin\add_to_config.inc"
!define FSA_MODULE_conf        "AlertServer\AlertServerUnzip\MODULES\FULLSCREEN\admin\add_to_config.inc"
!define VIDEO_MODULE_conf      "AlertServer\AlertServerUnzip\MODULES\VIDEOALERT\admin\add_to_config.inc"
!define TICK_MODULE_conf       "AlertServer\AlertServerUnzip\MODULES\TICKER\admin\add_to_config.inc"
!define TICKPRO_MODULE_conf    "AlertServer\AlertServerUnzip\MODULES\TICKER_PRO\admin\add_to_config.inc"
!define IM_MODULE_conf         "AlertServer\AlertServerUnzip\MODULES\INSTANT_MESSAGES\admin\add_to_config.inc"
!define WIDGETSTAT_MODULE_conf "AlertServer\AlertServerUnzip\MODULES\WIDGETSTAT\admin\add_to_config.inc"
!define TEXTCALL_MODULE_conf   "AlertServer\AlertServerUnzip\MODULES\TEXTCALL MODULE\admin\add_to_config.inc"
!define MOBILE_MODULE_conf     "AlertServer\AlertServerUnzip\MODULES\MOBILE\admin\add_to_config.inc"
!define WEBPLUGIN_MODULE_conf  "AlertServer\AlertServerUnzip\MODULES\WEBPLUGIN\admin\add_to_config.inc"
!define CAMPAIGN_MODULE_conf  "AlertServer\AlertServerUnzip\MODULES\CAMPAIGN MODULE\admin\add_to_config.inc"
!define APPROVE_MODULE_conf	  "AlertServer\AlertServerUnzip\MODULES\APPROVAL\admin\add_to_config.inc"
!define DIGSIGN_MODULE_conf	  "AlertServer\AlertServerUnzip\MODULES\DIGSIGN\admin\add_to_config.inc"
!define YAMMER_MODULE_conf	  "AlertServer\AlertServerUnzip\MODULES\YAMMER\admin\add_to_config.inc"

!define REGPATH "Software\Softomate\ServerInstallation"

;---------------------------------------------------------------------------------------------------
;Pages


Page custom SetRequirements ValidateRequirements

!ifdef trialpage
	Page custom SetTrial    ValidateTrial
!endif

Page custom SetIIS          ValidateIIS
;Page Custom LockedListShow

Page custom SetDB           ValidateDB
!ifdef isADAddon
	Page custom SetADAddon      ValidateADAddon
!endif
!ifdef isSMSAddon
	Page custom SetSMSAddon     ValidateSMSAddon
!endif
!ifdef isEMAILAddon
	Page custom SetEMAILAddon   ValidateEMAILAddon
!endif
!ifdef isENCRYPTAddon
	Page custom SetENCRYPTAddon ValidateENCRYPTAddon
!endif
!ifdef isSURVEYSAddon
	Page custom SetSURVEYSAddon ValidateSURVEYSAddon
!endif
!ifdef isSTATISTICSAddon
	Page custom SetEXTSTATAddon ValidateEXTSTATAddon
!endif
!ifdef isSCREENSAVERAddon
	Page custom SetSCREENSAVERAddon   ValidateSCREENSAVERAddon
!endif
!ifdef isRSSAddon
	Page custom SetRSSAddon     ValidateRSSAddon
!endif
!ifdef isWALLPAPERAddon
	Page custom SetWALLPAPERAddon     ValidateWALLPAPERAddon
!endif
!ifdef isBLOGAddon
	Page custom SetBLOGAddon    ValidateBLOGAddon
!endif
!ifdef isTWITTERAddon
	Page custom SetTWITTERAddon ValidateTWITTERAddon
!endif
!ifdef isLINKEDINAddon
	Page custom SetLINKEDINAddon      ValidateLINKEDINAddon
!endif
!ifdef isFULLSCREENAddon
	Page custom SetFULLSCREENAddon    ValidateFULLSCREENAddon
!endif
!ifdef isTICKERAddon
	Page custom SetTICKERAddon  ValidateTICKERAddon
!endif
!ifdef isTICKERPROAddon
	Page custom SetTICKERPROAddon     ValidateTICKERPROAddon
!endif
!ifdef isIMAddon
	Page custom SetIMAddon     ValidateIMAddon
!endif
;!ifdef isWIDGETSTATAddon
;	Page custom SetWIDGETSTATAddon     ValidateWIDGETSTATAddon
;!endif
!ifdef isTEXTCALLAddon
	Page custom SetTEXTCALLAddon     ValidateTEXTCALLAddon
!endif
!ifdef isMOBILEAddon
	Page custom SetMOBILEAddon    ValidateMOBILEAddon
!endif

!ifdef isVIDEOAddon
	Page custom SetVIDEOAddon    ValidateVIDEOAddon
!endif

!ifdef isWEBPLUGINAddon
	Page custom SetWEBPLUGINAddon    ValidateWEBPLUGINAddon
!endif

!ifdef isCAMPAIGNAddon
	Page custom SetCAMPAIGNAddon    ValidateCAMPAIGNAddon
!endif

!ifdef isAPPROVEAddon
	Page custom SetAPPROVEAddon ValidateAPPROVEAddon
!endif

!ifdef isDIGSIGNAddon
	Page custom SetDIGSIGNAddon ValidateDIGSIGNAddon
!endif

!ifdef isYAMMERAddon
	Page custom SetYAMMERAddon ValidateYAMMERAddon
!endif

!define MUI_PAGE_HEADER_TEXT ""
!define MUI_PAGE_HEADER_SUBTEXT ""
!insertmacro MUI_PAGE_INSTFILES

Page custom SetThanks       ValidateThanks

Function MoveFiles
	Exch $0
	Exch
	Exch $3
	Push $1
	Push $2
	;ClearErrors
	FindFirst $1 $2 "$0\*"
	loop:
		StrCmp $2 "" end
		;IfErrors end
		StrCmp $2 "." next
		StrCmp $2 ".." next
		IfFileExists "$0\$2\*" dir
		Delete "$3\$2"
		CopyFiles /SILENT /FILESONLY "$0\$2" "$3\$2"
		Goto next
	dir:
		CreateDirectory "$3\$2"
		Push "$3\$2"
		Push "$0\$2"
		call ${__FUNCTION__}
	next:
		;ClearErrors
		FindNext $1 $2
		Goto loop
	end:
	FindClose $1
	Pop $2
	Pop $1
	Pop $3
	Pop $0
FunctionEnd

Function LockedListShow
	StrCmp "$is_update" "1" update
		Abort
	update:
	!insertmacro MUI_HEADER_TEXT " Validate files locking" "Validate DeskAlerts server files locking"
	Push "succesed"
	LockedList::AddFolder /NOUNLOAD "$alerts_folder"
	Pop $3
	StrCmp $3 "succesed" dialog
	Pop $3 ;error
dialog:
	Push "succesed"
	LockedList::Dialog /NOUNLOAD /ignore Ignore
	Pop $0
	StrCmp $0 "succesed" succesed
	Pop $0 ;error
succesed:
FunctionEnd
Var encrypted_pass
Section "Components"
;----------------------------------------------------------------------------- Delete previous update patch
	DetailPrint "Clearing..."
	SetDetailsPrint none
	DeleteRegKey ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UPDATE_UNINST_KEY}"
	RMDir /r "${UPDATE_BACKUPDIR}"
	Delete /REBOOTOK "${PRODUCT_UNINST_FILEDIR}\${PRODUCT_UPDATE_UNINST_FILENAME}.exe"
	SetDetailsPrint both
;----------------------------------------------------------------------------- Apply server settings
	detailprint "Apply server settings ... "
	SetDetailsPrint none

	IfFileExists "$TEMP\${MAIN_BLANK_conf}" 0 no_blank
		Rename "$TEMP\${MAIN_BLANK_conf}" "$TEMP\${MAIN_conf}"
	no_blank:
	
	IfFileExists "$TEMP\${MAIN_BLANK_DOTNET_conf}" 0 no_dotnet_blank
		Rename "$TEMP\${MAIN_BLANK_DOTNET_conf}" "$TEMP\${MAIN_DOTNET_conf}"
	no_dotnet_blank:
	
	NsisCrypt::Base64Encode $password
	Pop $1
	STRCPY $encrypted_pass $1
	
	
	${ReplaceInASPConfig} "$TEMP\${MAIN_conf}" "%alerts_folder%"  "$alerts_folder"
	${ReplaceInASPConfig} "$TEMP\${MAIN_conf}" "%alerts_dir%"     "$alerts_dir"
	${ReplaceInASPConfig} "$TEMP\${MAIN_conf}" "%alerts_timeout%" "$alerts_timeout"
	${ReplaceInASPConfig} "$TEMP\${MAIN_conf}" "%host%"           "$host"
	${ReplaceInASPConfig} "$TEMP\${MAIN_conf}" "%dbname%"         "$dbname"
	${ReplaceInASPConfig} "$TEMP\${MAIN_conf}" "%user_name%"      "$user_name"
	${ReplaceInASPConfig} "$TEMP\${MAIN_conf}" "%password%"       "$encrypted_pass"
	${ReplaceInASPConfig} "$TEMP\${MAIN_conf}" "%win_auth%"       "$win_auth"
	
	${ReplaceInASPConfig} "$TEMP\${MAIN_DOTNET_conf}" "%alerts_folder%"  "$alerts_folder"
	${ReplaceInASPConfig} "$TEMP\${MAIN_DOTNET_conf}" "%alerts_dir%"     "$alerts_dir"
	${ReplaceInASPConfig} "$TEMP\${MAIN_DOTNET_conf}" "%alerts_timeout%" "$alerts_timeout"
	${ReplaceInASPConfig} "$TEMP\${MAIN_DOTNET_conf}" "%DB_SOURCE%"           "$host"
	${ReplaceInASPConfig} "$TEMP\${MAIN_DOTNET_conf}" "%DB_NAME%"         "$dbname"
	${ReplaceInASPConfig} "$TEMP\${MAIN_DOTNET_conf}" "%DB_USER%"      "$user_name"
	${ReplaceInASPConfig} "$TEMP\${MAIN_DOTNET_conf}" "%DB_PASS%"       "$encrypted_pass"
	${ReplaceInASPConfig} "$TEMP\${MAIN_DOTNET_conf}" "%WIN_AUTH%"       "$win_auth"	
	
	StrCmp $is_update 0 no_config 0
	
	move_conf_settings:
	Push "$TEMP\${MAIN_conf}"
	Push "${INSTALLED_conf}"
	Call UpdateConfig
	Pop $0
	StrCmp $0 "error" 0 no_config
		MessageBox MB_YESNO "Error while updating existing copy: cannot open old configuration file. Retry?" IDYES move_conf_settings IDNO no_config
	no_config:
	
	SetDetailsPrint both

	WriteRegStr HKCU "${REGPATH}" "alerts_folder" "$alerts_folder"
	WriteRegStr HKCU "${REGPATH}" "alerts_dir"    "$alerts_dir"
	WriteRegStr HKCU "${REGPATH}" "host"          "$host"
	WriteRegStr HKCU "${REGPATH}" "dbname"        "$dbname"
	WriteRegStr HKCU "${REGPATH}" "user_name"     "$user_name"
;	WriteRegStr HKCU "${REGPATH}" "password"      "$password"
	WriteRegStr HKCU "${REGPATH}" "win_auth"      "$win_auth"

;------------------------------------------------------------------------------ Apply addon settings
	detailprint "Apply addon settings ... "
	!ifdef demoServer
	${ReplaceInASPConfig} "$TEMP\${MAIN_DOTNET_conf}" "<!--IS_DEMO-->"  "<add key='DEMO' value='1'/>"
	!else
	${ReplaceInASPConfig} "$TEMP\${MAIN_DOTNET_conf}" "<!--IS_DEMO-->"  ""
	!endif	
	
	!ifdef isADAddon
		detailprint "${nameADAddon}"
		Call ApplyADAddon
	!else
		${ReplaceInASPConfig} "$TEMP\${MAIN_DOTNET_conf}" "<!--MODULE_AD-->"  ""
	!endif
	!ifdef isEDAddon
		detailprint "${nameEDAddon}"
		Call ApplyEDAddon
	!else
		${ReplaceInASPConfig} "$TEMP\${MAIN_DOTNET_conf}" "<!--MODULE_ED-->"  ""
	!endif
	!ifdef isSMSAddon
		detailprint "${nameSMSAddon}"
		Call ApplySMSAddon
	!else
		${ReplaceInASPConfig} "$TEMP\${MAIN_DOTNET_conf}" "<!--MODULE_SMS-->"  ""
	!endif
	!ifdef isEMAILAddon
		detailprint "${nameEMAILAddon}"
		Call ApplyEMAILAddon
	!else
		${ReplaceInASPConfig} "$TEMP\${MAIN_DOTNET_conf}" "<!--MODULE_EMAIL-->"  ""
	!endif
	!ifdef isENCRYPTAddon
		detailprint "${nameENCRYPTAddon}"
		Call ApplyENCRYPTAddon
	!else
		${ReplaceInASPConfig} "$TEMP\${MAIN_DOTNET_conf}" "<!--MODULE_ENCRYPT-->"  ""
	!endif
	!ifdef isSURVEYSAddon
		detailprint "${nameSURVEYSAddon}"
		Call ApplySURVEYSAddon
	!else
		${ReplaceInASPConfig} "$TEMP\${MAIN_DOTNET_conf}" "<!--MODULE_SURVEYS-->"  ""
	!endif
	!ifdef isSTATISTICSAddon
		detailprint "${nameSTATISTICSAddon}"
		Call ApplyEXTSTATAddon
	!else
		${ReplaceInASPConfig} "$TEMP\${MAIN_DOTNET_conf}" "<!--MODULE_STATISTICS-->"  ""
	!endif
	!ifdef isSCREENSAVERAddon
		detailprint "${nameSCREENSAVERAddon}"
		Call ApplySCREENSAVERAddon
	!else
		${ReplaceInASPConfig} "$TEMP\${MAIN_DOTNET_conf}" "<!--MODULE_SCREENSAVER-->"  ""
	!endif
	!ifdef isRSSAddon
		detailprint "${nameRSSAddon}"
		Call ApplyRSSAddon
	!else
		${ReplaceInASPConfig} "$TEMP\${MAIN_DOTNET_conf}" "<!--MODULE_RSS-->"  ""
	!endif
	!ifdef isWALLPAPERAddon
		detailprint "${nameWALLPAPERAddon}"
		Call ApplyWALLPAPERAddon
	!else
		${ReplaceInASPConfig} "$TEMP\${MAIN_DOTNET_conf}" "<!--MODULE_WALLPAPER-->"  ""
	!endif
	!ifdef isBLOGAddon
		detailprint "${nameBLOGAddon}"
		Call ApplyBLOGAddon
	!else
		${ReplaceInASPConfig} "$TEMP\${MAIN_DOTNET_conf}" "<!--MODULE_BLOG-->"  ""
	!endif
	!ifdef isTWITTERAddon
		detailprint "${nameTWITTERAddon}"
		Call ApplyTWITTERAddon
	!else
		${ReplaceInASPConfig} "$TEMP\${MAIN_DOTNET_conf}" "<!--MODULE_TWITTER-->"  ""
	!endif
	!ifdef isLINKEDINAddon
		detailprint "${nameLINKEDINAddon}"
		Call ApplyLINKEDINAddon
	!else
		${ReplaceInASPConfig} "$TEMP\${MAIN_DOTNET_conf}" "<!--MODULE_LINKEDIN-->"  ""
	!endif
	!ifdef isFULLSCREENAddon
		detailprint "${nameFULLSCREENAddon}"
		Call ApplyFULLSCREENAddon
	!else
		${ReplaceInASPConfig} "$TEMP\${MAIN_DOTNET_conf}" "<!--MODULE_FULLSCREEN-->"  " "
	!endif
	!ifdef isTICKERAddon
		detailprint "${nameTICKERAddon}"
		Call ApplyTICKERAddon
	!else
		${ReplaceInASPConfig} "$TEMP\${MAIN_DOTNET_conf}" "<!--MODULE_TICKER-->"  " "
	!endif
	!ifdef isTICKERPROAddon
		detailprint "${nameTICKERPROAddon}"
		Call ApplyTICKERPROAddon
	!else
		${ReplaceInASPConfig} "$TEMP\${MAIN_DOTNET_conf}" "<!--MODULE_TICKERPRO-->"  " "
	!endif
	!ifdef isIMAddon
		detailprint "${nameIMAddon}"
		Call ApplyIMAddon
	!else
		${ReplaceInASPConfig} "$TEMP\${MAIN_DOTNET_conf}" "<!--MODULE_IM-->"  " "
	!endif
	!ifdef isWIDGETSTATAddon
		;detailprint "${nameWIDGETSTATAddon}"
		Call ApplyWIDGETSTATAddon
	!endif
	!ifdef isTEXTCALLAddon
		detailprint "${nameTEXTCALLAddon}"
		Call ApplyTEXTCALLAddon
	!else
		${ReplaceInASPConfig} "$TEMP\${MAIN_DOTNET_conf}" "<!--MODULE_TEXTTOCALL-->"  " "
	!endif
	!ifdef isMOBILEAddon
		detailprint "${nameMOBILEAddon}"
		Call ApplyMOBILEAddon
	!else
		${ReplaceInASPConfig} "$TEMP\${MAIN_DOTNET_conf}" "<!--MODULE_MOBILE-->"  " "
	!endif

	!ifdef isVIDEOAddon
		detailprint "${nameVIDEOAddon}"
		Call ApplyVIDEOAddon
	!else
		${ReplaceInASPConfig} "$TEMP\${MAIN_DOTNET_conf}" "<!--MODULE_VIDEO-->"  " "
	!endif
	
	!ifdef isWEBPLUGINAddon
		detailprint "${nameWEBPLUGINAddon}"
		Call ApplyWEBPLUGINAddon
	!endif
	!ifdef isCAMPAIGNAddon
		detailprint "${nameCAMPAIGNAddon}"
		Call ApplyCAMPAIGNAddon
	!else
		${ReplaceInASPConfig} "$TEMP\${MAIN_DOTNET_conf}" "<!--MODULE_CAMPAIGN-->"  " "
	!endif
	
	!ifdef isAPPROVEAddon
		detailprint "${nameAPPROVEAddon}"
		Call ApplyAPPROVEAddon
	!else
		${ReplaceInASPConfig} "$TEMP\${MAIN_DOTNET_conf}" "<!--MODULE_APPROVE-->"  " "
	!endif
	
	!ifdef isDIGSIGNAddon
		detailprint "${nameDIGSIGNAddon}"
		Call ApplyDIGSIGNAddon
	!else
		${ReplaceInASPConfig} "$TEMP\${MAIN_DOTNET_conf}" "<!--MODULE_DIGSIGN-->"  " "
	!endif
	
	!ifdef isYAMMERAddon
		detailprint "${nameYAMMERAddon}"
		Call ApplyYAMMERAddon
	!endif
	
	SetDetailsPrint none
	Delete "$TEMP\AlertServer\AlertServerUnzip\STANDARD\admin\add_to_config.inc"
	Delete "$TEMP\AlertServer\AlertServerUnzip\STANDARD\admin\add_to_config.inc.old"

	SetShellVarContext all
	SetDetailsPrint both
	detailprint "Installing DLL files ... "
	CreateDirectory "$APPDATA\DeskAlertsServer"
	SetDetailsPrint none

	; Delete/Rename DeskAlertsServer32.dll if it exsists
	FindFirst $0 $1 "$APPDATA\DeskAlertsServer\DeskAlertsServer32*.dll"
	files_loop32:
		StrCmp $1 "" files_done32
		nsExec::Exec 'regsvr32.exe /u /s "$APPDATA\DeskAlertsServer\$1"'
		pop $R0
		FindNext $0 $1
		Goto files_loop32
	files_done32:
	FindClose $0

	StrCpy $R1 "$APPDATA\DeskAlertsServer\DeskAlertsServer32.dll"
	ClearErrors
	Delete "$APPDATA\DeskAlertsServer\DeskAlertsServer32*"
	IfErrors 0 flOk32
		StrCpy $R0 0 ; Set 0
		StrCpy $NeedToReloadIIS 1 ; Need IIS Reload
		flEx32:
		IntOp $R0 $R0 + 1 ; Inc in loop
		IfFileExists "$APPDATA\DeskAlertsServer\DeskAlertsServer32.dl$R0" flEx32
		ClearErrors
		Rename "$APPDATA\DeskAlertsServer\DeskAlertsServer32.dll" "$APPDATA\DeskAlertsServer\DeskAlertsServer32.dl$R0"
		IfErrors flCntMove32
		Delete /REBOOTOK "$APPDATA\DeskAlertsServer\DeskAlertsServer32.dl$R0"
		Goto flOk32
	flCntMove32:	
		StrCpy $R0 0 ; Set 0
		flDEx32:
		IntOp $R0 $R0 + 1 ; Inc in loop
		StrCpy $R1 "$APPDATA\DeskAlertsServer\DeskAlertsServer32[$R0].dll"
		IfFileExists "$R1" flDEx32
		Delete /REBOOTOK "$APPDATA\DeskAlertsServer\DeskAlertsServer32.dll"
	flOk32:

	; Delete/Rename DeskAlertsServer64.dll if it exsists
	FindFirst $0 $1 "$APPDATA\DeskAlertsServer\DeskAlertsServer64*.dll"
	files_loop64:
		StrCmp $1 "" files_done64
		nsExec::Exec 'cmd /c cd /d "%WinDir%\SysNative"&cmd.exe /c start /w regsvr32.exe /u /s "$APPDATA\DeskAlertsServer\$1"'
		pop $R0
		FindNext $0 $1
		Goto files_loop64
	files_done64:
	FindClose $0

	StrCpy $R2 "$APPDATA\DeskAlertsServer\DeskAlertsServer64.dll"
	ClearErrors
	Delete "$APPDATA\DeskAlertsServer\DeskAlertsServer64*"
	IfErrors 0 flOk64
		StrCpy $R0 0 ; Set 0
		StrCpy $NeedToReloadIIS 1 ; Need IIS Reload
		flEx64:
		IntOp $R0 $R0 + 1 ; Inc in loop
		IfFileExists "$APPDATA\DeskAlertsServer\DeskAlertsServer64.dl$R0" flEx64
		ClearErrors
		Rename "$APPDATA\DeskAlertsServer\DeskAlertsServer64.dll" "$APPDATA\DeskAlertsServer\DeskAlertsServer64.dl$R0"
		IfErrors flCntMove64
		Delete /REBOOTOK "$APPDATA\DeskAlertsServer\DeskAlertsServer64.dl$R0"
		Goto flOk64
	flCntMove64:	
		StrCpy $R0 0 ; Set 0
		flDEx64:
		IntOp $R0 $R0 + 1 ; Inc in loop
		StrCpy $R2 "$APPDATA\DeskAlertsServer\DeskAlertsServer64[$R0].dll"
		IfFileExists "$R2" flDEx64
		Delete /REBOOTOK "$APPDATA\DeskAlertsServer\DeskAlertsServer64.dll"
	flOk64:

	SetDetailsPrint both
	File "/oname=$R1" "..\tmp\DeskAlertsServer32.dll"
	nsExec::Exec 'regsvr32.exe /s "$R1"'
	pop $R0
	DetailPrint "registering '$R1' - $R0"
	nsExec::Exec `cmd /c cd %windir%\system32\inetsrv & appcmd set config /section:staticContent /+"[fileExtension='.bat',mimeType='application/bat']"`
	${If} ${RunningX64}
		File "/oname=$R2" "..\tmp\DeskAlertsServer64.dll"
		nsExec::Exec 'cmd /c cd /d "%WinDir%\SysNative"&cmd.exe /c start /w regsvr32.exe /s "$R2"'
		pop $R0
		DetailPrint "registering '$R2' - $R0"
	${EndIf}

;------------------------------------------------------------------------------- Copy files --------

	StrCmp "$is_update" "1" update
		detailprint "Clearing ... "
		SetDetailsPrint none
		${RemoveFilesAndSubDirs} "$alerts_folder"
		SetDetailsPrint both
		Goto no_update
	update:
		SetDetailsPrint none
		Delete "$alerts_folder\*.key"
		Delete "$alerts_folder\admin\*.key"
		SetDetailsPrint both
	no_update:

	detailprint "Copy files ... "
	SetDetailsPrint none
	CreateDirectory $alerts_folder
	Push $alerts_folder
	Push "$TEMP\AlertServer\AlertServerUnzip\STANDARD"
	call MoveFiles 
	Push "$alerts_folder\bin\DeskAlertsDotNet.dll"
	call RegisterDotNet
	SetDetailsPrint both

	DetailPrint "Installing MSXML6.0 ... "
	SetDetailsPrint none
	SetOutPath $PLUGINSDIR
	SetDetailsPrint both
	File msxml6.msi
	File msxml6_x64.msi
	File msxml6_ia64.msi
	nsExec::Exec 'msiexec.exe /q /i "$PLUGINSDIR\msxml6.msi"'
	pop $R0
	nsExec::Exec 'msiexec.exe /q /i "$PLUGINSDIR\msxml6_x64.msi"'
	pop $R0
	nsExec::Exec 'msiexec.exe /q /i "$PLUGINSDIR\msxml6_ia64.msi"'
	pop $R0

;------------------------------------------------------------------------------- Set Rights --------
	detailprint "Set permissions ... "
	Call SetRights

;-------------------------------------------------------------------------------- Remove temp files
	DetailPrint "Remove temp files ... "
	SetDetailsPrint none
	RMDir /r "$TEMP\AlertServer"
	SetDetailsPrint both
	
;-------------------------------------------------------------------------------- Create scripts
	DetailPrint "Create scripts ... "
	Call ProcessArchive
	Call ProcessSchedule
	Call ProcessRSS
	Call ProcessSync
	Call ClearCache
	
SectionEnd

Function SetRights
	Push $1
	StrCpy $1 "(S-1-1-0)" ; Everyone

	${SetRightsWithPrint} "$alerts_folder\ds" "$1" "FullAccess"
	
	${SetRightsWithPrint} "$alerts_folder\admin\images\upload" "$1" "FullAccess"

	${SetRightsWithPrint} "$alerts_folder\admin\images\upload_document" "$1" "FullAccess"

	${SetRightsWithPrint} "$alerts_folder\admin\images\upload_flash" "$1" "FullAccess"

	${SetRightsWithPrint} "$alerts_folder\admin\images\upload_video" "$1" "FullAccess"

	${SetRightsWithPrint} "$alerts_folder\admin\example_image_list.js" "$1" "FullAccess"
	
	${SetRightsWithPrint} "$alerts_folder\admin\images\langs" "$1" "FullAccess"	

	${SetRightsWithPrint} "$alerts_folder\admin\csv" "$1" "FullAccess"
	
	${SetRightsWithPrint} "$alerts_folder\admin\ad_import\files" "$1" "FullAccess"
	
	${SetRightsWithPrint} "$alerts_folder\admin\ad_import\logs" "$1" "FullAccess"

	${SetRightsWithPrint} "$alerts_folder\admin\scripts" "$1" "FullAccess"

	${SetRightsWithPrint} "$alerts_folder\admin\logs" "$1" "FullAccess"

	${SetRightsWithPrint} "$alerts_folder\encrypt" "$1" "FullAccess"

	${SetRightsWithPrint} "$alerts_folder\enc" "$1" "FullAccess"
	
	${SetRightsWithPrint} "$alerts_folder\admin\preview\skin\version" "$1" "FullAccess"

	${SetRightsWithPrint} "$alerts_folder\documentation" "$1" "FullAccess"
	Pop $1
FunctionEnd


Section "SQL"
	SetDetailsPrint none
	setoutpath $PLUGINSDIR

	FILE include\BackupDB.sql
	FILE include\CreateDB.sql
	FILE include\ClearDB.sql
	FILE include\CreateStored.sql
	FILE include\CreateIndexes.sql
	FILE include\CreateUser.sql
	FILE include\CreateUser2.sql
	FILE include\UpdateTables.sql
	FILE include\UpdateTables1.sql
	FILE include\UpdateTables2.sql
	FILE /nonfatal include\Custom.sql
	FILE /nonfatal include\Custom_contract_date.sql
	${ReplaceParamInSQLFile} "$PLUGINSDIR\BackupDB.sql" "dbname" "$dbname"
	${ReplaceParamInSQLFile} "$PLUGINSDIR\CreateDB.sql" "dbname" "$dbname"
	${ReplaceParamInSQLFile} "$PLUGINSDIR\ClearDB.sql" "dbname" "$dbname"
	${ReplaceParamInSQLFile} "$PLUGINSDIR\CreateStored.sql" "dbname" "$dbname"
	${ReplaceParamInSQLFile} "$PLUGINSDIR\CreateIndexes.sql" "dbname" "$dbname"

	${ReplaceParamInSQLFile} "$PLUGINSDIR\UpdateTables.sql" "dbname" "$dbname"
	${RandomStr} $R0 10
	${ReplaceParamInSQLFile} "$PLUGINSDIR\UpdateTables.sql" "api_secret" "$R0"

	${ReplaceParamInSQLFile} "$PLUGINSDIR\UpdateTables1.sql" "dbname" "$dbname"
	${ReplaceParamInSQLFile} "$PLUGINSDIR\UpdateTables2.sql" "dbname" "$dbname"

	${ReplaceParamInSQLFile} "$PLUGINSDIR\UpdateTables2.sql" "deskalerts_version" "${PRODUCT_VERSION}"
	${ReplaceParamInSQLFile} "$PLUGINSDIR\UpdateTables2.sql" "licenses_count" "${licenses}"
	${ReplaceParamInSQLFile} "$PLUGINSDIR\UpdateTables2.sql" "trialseconds_count" "${trial}"
	;${ReplaceParamInSQLFile} "$PLUGINSDIR\UpdateTables2.sql" "contract_date" "${CONTRACT_DATETIME}"
	${ReplaceParamInSQLFile} "$PLUGINSDIR\Custom.sql" "dbname" "$dbname"
	SetDetailsPrint both

	detailprint "Loggin on to SQL server $host"
	MSSQL_OLEDB::SQL_Logon /NOUNLOAD "$host" "$user_name" "$password"
	pop $0
	pop $1
	detailprint $1

	StrCmp "$0" "0" no_cn_err 0
	MSSQL_OLEDB::SQL_GetError /NOUNLOAD
	Pop $0
	Pop $0
	DetailPrint $0
	no_cn_err:

	StrCmp "$dbupdate" "1" db_work_end
	StrCmp "$dbupdate" "2" db_clear

		DetailPrint "Create database"
		${ExecuteSQLScript} "$PLUGINSDIR\CreateDB.sql"

		Goto db_work_end
	db_clear:
		DetailPrint "Clear database"
		${ExecuteSQLScript} "$PLUGINSDIR\ClearDB.sql"
	db_work_end:

	DetailPrint "Update tables"
	${ExecuteSQLScript} "$PLUGINSDIR\UpdateTables.sql"

	DetailPrint "Update translates"
	${ExecuteSQLScript} "$PLUGINSDIR\UpdateTables1.sql"

	DetailPrint "Update types"
	${ExecuteSQLScript} "$PLUGINSDIR\UpdateTables2.sql"

	DetailPrint "Creating stored"
	${ExecuteSQLScript} "$PLUGINSDIR\CreateStored.sql"

	DetailPrint "Creating indexes"
	${ExecuteSQLScript} "$PLUGINSDIR\CreateIndexes.sql"
	
	IfFileExists "$PLUGINSDIR\Custom.sql" 0 no_custom_sql
		DetailPrint "Inserting the data"
		${ExecuteSQLScript} "$PLUGINSDIR\Custom.sql"
	no_custom_sql:
	
	IfFileExists "$PLUGINSDIR\Custom_contract_date.sql" 0 no_custom_cd_sql
	DetailPrint "Inserting the data contract date"
	${ExecuteSQLScript} "$PLUGINSDIR\Custom_contract_date.sql"
	no_custom_cd_sql:

	inetc::get /caption "Validating alert server" /SILENT /nocancel "$alerts_dir/editors_update.asp"  $2 /end
	pop $0 ; get result		
		
	 StrCmp $user_name "" +2
	 IntCmp $win_auth 0 if_not_win_auth
		 ExpandEnvStrings $1 "%COMPUTERNAME%"
		 ExpandEnvStrings $2 "%USERDOMAIN%"

		 SetDetailsPrint none
		 ${ReplaceParamInSQLFile} "$PLUGINSDIR\CreateUser.sql" "dbname" "$dbname"
		 ${ReplaceParamInSQLFile} "$PLUGINSDIR\CreateUser.sql" "user_name" "$2\IUSR_$1"
		 SetDetailsPrint both
		 DetailPrint "create user"

		 MSSQL_OLEDB::SQL_ExecuteScript /NOUNLOAD "$PLUGINSDIR\CreateUser.sql"
		 Pop $0
		 Pop $1
		 ;DetailPrint $1

		 StrCmp "$0" "0" no_ur_err 0
		 MSSQL_OLEDB::SQL_GetError /NOUNLOAD
		 Pop $0
		 Pop $0
		 ;DetailPrint $0
		 no_ur_err:

		 SetDetailsPrint none
		 ${ReplaceParamInSQLFile} "$PLUGINSDIR\CreateUser2.sql" "dbname" "$dbname"
		 ${ReplaceParamInSQLFile} "$PLUGINSDIR\CreateUser2.sql" "user_name" "NT AUTHORITY\IUSR"
		 SetDetailsPrint both
		 ;DetailPrint "create user"

		 MSSQL_OLEDB::SQL_ExecuteScript /NOUNLOAD "$PLUGINSDIR\CreateUser2.sql"
		 Pop $0
		 Pop $1
		 ;DetailPrint $1

		 StrCmp "$0" "0" no_ur_err2 0
		 MSSQL_OLEDB::SQL_GetError /NOUNLOAD
		 Pop $0
		 Pop $0
		 ;DetailPrint $0
		 no_ur_err2:
	 if_not_win_auth:

	MSSQL_OLEDB::SQL_Logout
SectionEnd

;-------------------------------------------------------------------------------- PAGE 1 -----------
Function SetRequirements
	!insertmacro MUI_HEADER_TEXT " Validate requirements" "Validate environment settings needed to successfully install DeskAlerts server"
	Push $R0
	InstallOptions::dialog "$PLUGINSDIR\check_for_requirements.ini"
	Pop $R0
	Pop $R0
FunctionEnd

Function ValidateRequirements
	Push $R1
	StrCmp "$RequirementsOk" "Ok" equal_finish
		ReadINIStr $R1 "$PLUGINSDIR\check_for_requirements.ini" "Field 8" "State"
		StrCmp "$R1" "1" equal_finish
			MessageBox MB_OK "Requirements not met. DeskAlerts server cannot be installed on this machine."
			Pop $R1
			Abort
	equal_finish:
	Pop $R1
	Call CheckASP
	Call SetStandart
FunctionEnd

;-------------------------------------------------------------------------------- PAGE 2 -----------
Var "notfirstrun_iis"
Var "notfirstrun_db"
Var "notfirstrun_ad"
Var "notfirstrun_blog"
Var "notfirstrun_fullscreen"
Var "notfirstrun_video"
Var "notfirstrun_rss"
Var "notfirstrun_ss"
Var "notfirstrun_ticker"
Var "notfirstrun_tickerpro"
Var "notfirstrun_im"
Var "notfirstrun_widgetstat"
Var "notfirstrun_twitter"
Var "notfirstrun_linkedin"
Var "notfirstrun_wallpaper"
Var "notfirstrun_sms"
Var "notfirstrun_textcall"
Var "notfirstrun_encrypt"
Var "notfirstrun_surveys"
Var "notfirstrun_extstat"
Var "notfirstrun_email"
Var "notfirstrun_mobile"
Var "notfirstrun_webplugin"
Var "notfirstrun_campaign"
Var "notfirstrun_approve"
Var "notfirstrun_digsign"
Var "notfirstrun_yammer"

Var "IIS_folder_path"
Var "IIS_URL_path"
Var "IIS_list_items"
Var "ComputerName"

Function getSiteUrl
	Exch $0
	Push $1
	Push $2
	Push $3
	
	; WebSites FORMAT:  name|physical_path|protocol,ip,port,host>
	
	${StrTok} $0 $0 "|" "2" "0" ;get binding
	${StrTok} $1 $0 "," "0" "0" ; get protocol
	${StrTok} $2 $0 "," "2" "0" ; get port
	${StrTok} $3 $0 "," "3" "0" ; get host
					
	StrCmp $3 "" 0 +2
		StrCpy $3 $ComputerName
	
	StrCpy $2 ":$2"	
	StrCmp $1 "http" 0 end_http
	StrCmp $2 ":80" 0 end_http
		StrCpy $2 ""	
end_http:
	StrCmp $1 "https" 0 end_https
	StrCmp $2 ":443" 0 end_https
		StrCpy $2 ""	
end_https:	

	StrCpy $0 "$1://$3$2/DeskAlerts"

	Pop $3
	Pop $2
	Pop $1
	Exch $0
FunctionEnd


Function getSitePath
	Exch $0
	
	; WebSites FORMAT:  name|physical_path|protocol,ip,port,host>
	
	${StrTok} $0 $0 "|" "1" "0" ; wwwrootpath
					
	StrCpy $0 "$0\DeskAlerts"

	Exch $0
FunctionEnd

var sitesInfo
Function SetIIS

	!insertmacro MUI_HEADER_TEXT " Internet Information Services" "Validate Internet Information Services settings"	
	
	StrCpy "$4" "$IIS_list_items"

	StrCmpS "$IIS_folder_path" "" common_init 
	StrCpy "$1" "$IIS_folder_path"

common_init:
	StrCmpS "$IIS_URL_path" "" common_init2 
	StrCpy "$2" "$IIS_URL_path"
	Goto use_saved_values

common_init2:		
	StrCmp "$notfirstrun_iis" "1" not_first_run_iis
	
	System::Call 'kernel32.dll::GetComputerNameA(t .r0,*i ${NSIS_MAX_STRLEN} r1)i.r2'
	StrCpy $ComputerName $0
	ClearErrors
	
	IISWork::InitializeWebSitesInfo /NOUNLOAD
	Pop $0
	
	IISWork::GetWebSitesCount /NOUNLOAD
	Pop $0
	
	StrCpy $1 "0"
	StrCpy $4 ""
	StrCpy $sitesInfo ""
	
	get_site:
	
	StrCmp $1 $0 get_sites_end 0
	
	
		Push $1 
		IISWork::GetWebSiteById /NOUNLOAD
		Pop $5
		
		${StrTok} $3 $5 "|" "2" "0"
		${StrTok} $3 $3 "," "0" "0" 
		
		StrCmp $3 "http" +2 0
			StrCmp $3 "https" 0 next_site
		
		Push $5 
		Call getSiteUrl
		Pop $2
		
		StrCpy $sitesInfo "$sitesInfo<$5>"
		
		${StrTok} $5 $5 "|" "0" "0"
		
		StrCpy $2 "$5  URL: $\"$2$\""
	
		StrCmp $4 "" 0 +3
			StrCpy $4 $2
			Goto +2
		StrCpy $4 "$4|$2"
		
	next_site:
		IntOp $1 $1 + 1
	
		Goto get_site
	
	get_sites_end:
	
	StrCpy $1 ""
	StrCpy $2 ""
	
	ExpandEnvStrings $1 $1

	ReadRegStr $0 HKCU "${REGPATH}" "alerts_folder"
	StrCmp $0 "" +2
	StrCpy $1 $0

	ReadRegStr $0 HKCU "${REGPATH}" "alerts_dir"
	StrCmp $0 "" +2
	StrCpy $2 $0

use_saved_values:

	WriteINIStr "$PLUGINSDIR\iis_set.ini" "Field 1" "State" "$1"
	WriteINIStr "$PLUGINSDIR\iis_set.ini" "Field 2" "State" "$2"
	WriteINIStr "$PLUGINSDIR\iis_set.ini" "Field 9" "ListItems" "$4"

	StrCpy $notfirstrun_iis "1"
not_first_run_iis:

	Push $R0
	InstallOptions::dialog "$PLUGINSDIR\iis_set.ini"
	Pop $R0
	Pop $R0
FunctionEnd

Var "AlertsFolderCopy"
Var "AlertsFolder"
Var "IISBasePathLen"
Var "IISWholePathLen"
Var "IISAlertDirNameLen"

Function ValidateIIS
	ReadINIStr $0 "$PLUGINSDIR\iis_set.ini" "Settings" "State"
	IntCmp $0 0 if_next ;if $0 = 0 was clicked next button otherwise it was notify
		StrCmp $0 9 0 not_drop_down
			ReadINIStr $1 "$PLUGINSDIR\iis_set.ini" "Field 9" "HWND"
			ReadINIStr $2 "$PLUGINSDIR\iis_set.ini" "Field 1" "HWND"
			ReadINIStr $3 "$PLUGINSDIR\iis_set.ini" "Field 2" "HWND"
			
			SendMessage $1 ${CB_GETCURSEL} 0 0 $R1
			
			;SendMessage $1 ${CB_SETCURSEL} ${CB_ERR} 0
				
			${StrTok} $R1 $sitesInfo "<>" $R1 "1"

			Push $R1
			Call getSiteUrl
			Pop $R2
			
			SendMessage $3 ${WM_SETTEXT} 0 "STR:$R2"
			
			Push $R1
			Call getSitePath
			Pop $R3
			
			SendMessage $2 ${WM_SETTEXT} 0 "STR:$R3"
			
		GoTo if_abort
	not_drop_down:
		ReadINIStr $1 "$PLUGINSDIR\iis_set.ini" "Field 7" "State"
		ReadINIStr $2 "$PLUGINSDIR\iis_set.ini" "Field 8" "HWND"

		EnableWindow $2 $1

		StrCpy $4 ""
		IntCmp $1 1 +2
		StrCpy $4 "DISABLED"

		WriteINIStr "$PLUGINSDIR\iis_set.ini" "Field 8" "Flags" "$4"
	if_abort:
		Abort
	if_next:

	StrCpy $R9 $alerts_folder ;last value
	ReadINIStr $alerts_folder "$PLUGINSDIR\iis_set.ini" "Field 1" "State"

	StrCpy "$AlertsFolderCopy" "$alerts_folder"	; saving this value to use it during IIS anonymous user validation.

	;StrLen "$IISBasePathLen" "%SystemDrive%:\Inetpub\wwwroot\"
	StrCpy $3 "%SystemDrive%\Inetpub\wwwroot\"

	ExpandEnvStrings $4 $3

	StrLen "$IISBasePathLen" $4
	StrLen "$IISWholePathLen" "$AlertsFolderCopy"

	IntOp $IISAlertDirNameLen $IISWholePathLen - $IISBasePathLen

	StrCpy $AlertsFolder $AlertsFolderCopy "" -$IISAlertDirNameLen

	Push "$AlertsFolder"
	;MessageBox MB_OK "$AlertsFolder"
	IISWork::SetVirtualDirectoryName /NOUNLOAD
	Pop "$AlertsFolder"
	;MessageBox MB_OK "$AlertsFolder"


	ReadINIStr $alerts_dir "$PLUGINSDIR\iis_set.ini" "Field 2" "State"

check_folder:
	StrCpy $0 $alerts_folder "" -1
	StrCmp $0 '\' 0 +3
	StrCpy $alerts_folder $alerts_folder -1
	Goto check_folder

check_dir:
	StrCpy $0 $alerts_dir "" -1
	StrCmp $0 '/' 0 +3
	StrCpy $alerts_dir $alerts_dir -1
	Goto check_dir

	ReadINIStr $0 "$PLUGINSDIR\iis_set.ini" "Field 7" "State"
	StrCmp $0 0 finish

	Banner::show /NOUNLOAD /set 76 "Server installation" "Validating ... "
	Banner::getWindow /NOUNLOAD
	pop $0 ; get result

	ClearErrors
	CreateDirectory $alerts_folder
	FileOpen $0 "$alerts_folder\check.txt" w
	IfErrors error
	FileWrite $0 "check"
	FileClose $0

	ClearErrors
	GetTempFileName $2
	inetc::get /caption "Validating alert server" /SILENT /nocancel "$alerts_dir/check.txt"  $2 /end
	pop $0 ; get result
	FileOpen $0 $2 r
	IfErrors error
	FileRead $0 $1
	FileClose $0
	goto done
	error:
		Banner::destroy
		MessageBox MB_OK "URL is wrong, please try again."
		SetDetailsPrint none
		Delete "$alerts_folder\check.txt"
		RMDir $alerts_folder
		SetDetailsPrint both
		Abort
	done:
		ReadINIStr $0 "$PLUGINSDIR\iis_set.ini" "Field 8" "State"
		IntCmp $0 0 asp_done
			FileOpen $0 "$alerts_folder\check.asp" w
			IfErrors error
			FileWrite $0 "<% response.write $\"check$\"%>"
			FileClose $0

			ClearErrors
			GetTempFileName $2
			inetc::get /caption "Validating alert server" /SILENT /nocancel "$alerts_dir/check.asp"  $2 /end
			pop $0 ; get result
			FileOpen $0 $2 r
			IfErrors asp_error
			FileRead $0 $1
			FileClose $0
			
			StrCmp $1 "check" asp_done
			
			asp_error:
				Banner::destroy
				MessageBox MB_OK "ASP settings are wrong, please try again."
				SetDetailsPrint none
				Delete "$alerts_folder\check.asp"
				Delete "$alerts_folder\check.txt"
				RMDir $alerts_folder
				SetDetailsPrint both
				Abort
			asp_done:

			Banner::destroy
			SetDetailsPrint none
			Delete "$alerts_folder\check.asp"
			Delete "$alerts_folder\check.txt"
			RMDir $alerts_folder
			SetDetailsPrint both
	finish:

	StrCpy $R8 $is_update
	StrCpy $is_update "0"
	IfFileExists "${INSTALLED_conf}" 0 not_exists
		MessageBox MB_ICONQUESTION|MB_YESNOCANCEL "Another version of DeskAlerts is already installed in the specified folder.$\r$\nWould you like to upgrade it?$\r$\n$\r$\nYes - install a new version over the existing one (will keep your uploads and settings)$\r$\nNo - remove the existing version and install a new version of DeskAlerts into the same folder$\r$\nCancel - choose a different folder  to install a new version of DeskAlerts" IDYES update IDNO noupdate
			Abort
		update:
			StrCpy $is_update "1"
			Goto not_exists
		noupdate:
			MessageBox MB_ICONQUESTION|MB_YESNO "Are you sure you want to remove the existing version?" IDYES not_exists
			Abort
	not_exists:

	StrCmp "$is_update" "$R8" 0 reset_update
	StrCmp "$is_update" "0" no_update
	StrCmp $R9 "" no_update
	StrCmp $R9 $alerts_folder no_update
	reset_update:
		StrCpy $notfirstrun_ad "0"
		StrCpy $notfirstrun_db "0"
		StrCpy $notfirstrun_email "0"
		StrCpy $notfirstrun_encrypt "0"
		StrCpy $notfirstrun_extstat "0"
		StrCpy $notfirstrun_iis "0"
		StrCpy $notfirstrun_sms "0"
		StrCpy $notfirstrun_surveys "0"
		StrCpy $notfirstrun_blog "0"
		StrCpy $notfirstrun_fullscreen "0"
		StrCpy $notfirstrun_video "0"
		StrCpy $notfirstrun_rss "0"
		StrCpy $notfirstrun_ss "0"
		StrCpy $notfirstrun_ticker "0"
		StrCpy $notfirstrun_tickerpro "0"
		StrCpy $notfirstrun_im "0"
		StrCpy $notfirstrun_widgetstat "0"
		StrCpy $notfirstrun_twitter "0"
		StrCpy $notfirstrun_linkedin "0"
		StrCpy $notfirstrun_wallpaper "0"
		StrCpy $notfirstrun_textcall "0"
		StrCpy $notfirstrun_mobile "0"
		StrCpy $notfirstrun_webplugin "0"
		StrCpy $notfirstrun_campaign "0"
		StrCpy $notfirstrun_approve "0"
		StrCpy $notfirstrun_digsign "0"
		StrCpy $notfirstrun_yammer "0"
	no_update:

	StrCpy "$IIS_folder_path" "$alerts_folder"
	StrCpy "$IIS_URL_path" "$alerts_dir"
	ReadINIStr $0 "$PLUGINSDIR\iis_set.ini" "Field 9" "ListItems"
	StrCpy "$IIS_list_items" $0
FunctionEnd

;-------------------------------------------------------------------------------- PAGE 3 -----------
Function SetDB

	!insertmacro MUI_HEADER_TEXT " Setting up the DeskAlerts Database" "Configure database settings"	

	StrCmp "$notfirstrun_db" "1" not_first_run_db
		${ReadFromASPorRegistryAndWrite} "$PLUGINSDIR\db.ini" "Field 1" "host" "host" $0
		;${ReadFromASPorRegistry} "host" "host" $0
		;StrCmp "$0" "" DoNothing
		;	${StrTok} $1 "$0" "\" 1 0 ;instance
		;	${StrTok} $2 "$0" "\" 0 0
		;	${StrTok} $3 "$2" "," 0 0 ;host
		;	${StrTok} $4 "$2" "," 1 0 ;port
		;	StrCmp "$3" "" +2
		;		WriteINIStr "$PLUGINSDIR\db.ini" "Field 1" "State" $3
		;	StrCmp "$1" "" +2
		;		WriteINIStr "$PLUGINSDIR\db.ini" "Field 13" "State" $1
		;	StrCmp "$4" "" +2
		;		WriteINIStr "$PLUGINSDIR\db.ini" "Field 15" "State" $4
		;DoNothing:

		${ReadFromASPorRegistryAndWrite} "$PLUGINSDIR\db.ini" "Field 2" "dbname" "dbname" $0
		${ReadFromASPorRegistryAndWrite} "$PLUGINSDIR\db.ini" "Field 3" "user_name" "user_name" $0
		${ReadFromASPorRegistryAndWrite} "$PLUGINSDIR\db.ini" "Field 4" "password" "password" $0
		${ReadFromASPorRegistryAndWrite} "$PLUGINSDIR\db.ini" "Field 11" "win_auth" "win_auth" $0

		StrCpy $1 ""
		StrCpy $2 "DISABLED"
		StrCmp $0 "1" 0 +3
		StrCpy $1 "DISABLED"
		StrCpy $2 ""

		WriteINIStr "$PLUGINSDIR\db.ini" "Field 3" "Flags" "$1"
		WriteINIStr "$PLUGINSDIR\db.ini" "Field 4" "Flags" "$1"
		WriteINIStr "$PLUGINSDIR\db.ini" "Field 13" "Flags" "$2"
		WriteINIStr "$PLUGINSDIR\db.ini" "Field 15" "Flags" "$2"

		${ReadFromASPorRegistryAndWrite} "$PLUGINSDIR\db.ini" "Field 13" "IIS_user" "IIS_User" $0

		StrCpy $notfirstrun_db "1"
	not_first_run_db:

	Push $R0
	InstallOptions::dialog "$PLUGINSDIR\db.ini"
	Pop $R0
	Pop $R0
FunctionEnd

Function ValidateDB		
	ReadINIStr $0 "$PLUGINSDIR\db.ini" "Settings" "State"
	ReadINIStr $win_auth "$PLUGINSDIR\db.ini" "Field 11" "State"	

	IntCmp $0 0 if_next ;if $0 = 0 was clicked next button otherwise it was notify
		ReadINIStr $1 "$PLUGINSDIR\db.ini" "Field 3" "HWND"
		ReadINIStr $2 "$PLUGINSDIR\db.ini" "Field 4" "HWND"
		ReadINIStr $8 "$PLUGINSDIR\db.ini" "Field 13" "HWND"
		ReadINIStr $9 "$PLUGINSDIR\db.ini" "Field 15" "HWND"

		IntOp $3 $win_auth !
		EnableWindow $1 $3
		EnableWindow $2 $3

		IntOp $3 $3 !
		EnableWindow $8 $3
		EnableWindow $9 $3

		StrCpy $4 ""
		StrCpy $5 "DISABLED"
		IntCmp $win_auth 0 +3
		StrCpy $4 "DISABLED"
		StrCpy $5 ""

		WriteINIStr "$PLUGINSDIR\db.ini" "Field 3" "Flags" "$4"
		WriteINIStr "$PLUGINSDIR\db.ini" "Field 4" "Flags" "$4"
		WriteINIStr "$PLUGINSDIR\db.ini" "Field 13" "Flags" "$5"
		WriteINIStr "$PLUGINSDIR\db.ini" "Field 15" "Flags" "$5"

		IntCmp $win_auth 0 +2
		StrCpy $4 ""		

		WriteINIStr "$PLUGINSDIR\db.ini" "Field 13" "Flags" "$4"
		WriteINIStr "$PLUGINSDIR\db.ini" "Field 15" "Flags" "$4"

		Abort
	if_next:

	ReadINIStr $0 "$PLUGINSDIR\db.ini" "Field 1" "State" ;host
	StrCpy $host $0

	;ReadINIStr $1 "$PLUGINSDIR\db.ini" "Field 15" "State" ;port
	;StrCmp "$1" "(default)" +3
	;StrCmp "$1" "" +2
	;	StrCpy $host "$host,$1"

	;ReadINIStr $2 "$PLUGINSDIR\db.ini" "Field 13" "State" ;instance
	;StrCmp "$2" "(default)" +3
	;StrCmp "$2" "" +2
	;	StrCpy $host "$host\$2"

	ReadINIStr $dbname "$PLUGINSDIR\db.ini" "Field 2" "State"

	StrCpy $user_name ""
	StrCpy $password ""

	IntCmp $win_auth 1 if_win_auth
		ReadINIStr $user_name "$PLUGINSDIR\db.ini" "Field 3" "State"
		ReadINIStr $password "$PLUGINSDIR\db.ini" "Field 4" "State"

		StrCmp "$user_name" "" 0 finish_user_check
			MessageBox MB_OK "Username cannot be empty."
			Abort
		finish_user_check:
	if_win_auth:

	ReadINIStr $win_auth "$PLUGINSDIR\db.ini" "Field 11" "State"
	IntCmp "$win_auth" 0 skip_au_credentials_validation
		Call ValidateIISAU

skip_au_credentials_validation:

	StrCmp "$isLocalDB" "false" 0 finish_host_check
	StrCmp "$0" "(local)" 0 finish_host_check
		MessageBox MB_OK "You cannot install Alert data base on this computer. Change Host"
		Abort
	finish_host_check:

begin_db:
	detailprint "Loggin on to SQL server $host"
	MSSQL_OLEDB::SQL_Logon /NOUNLOAD "$host" "$user_name" "$password"
	pop $1
	pop $0

	StrCmp "$1" "0" if_not_equal_err
		MSSQL_OLEDB::SQL_GetError /NOUNLOAD
		Pop $1
		Pop $0
		MessageBox MB_OK "$0$\r$\nPlease enter another username/password or host"
		Abort
	if_not_equal_err:
		StrCpy $dbupdate "0"
		MSSQL_OLEDB::SQL_Execute /NOUNLOAD "USE master"
		pop $1
		pop $0

		${StrReplace} $0 "'" "''" "$dbname"
		MSSQL_OLEDB::SQL_Execute /NOUNLOAD "SELECT 1 FROM sysdatabases WHERE name='$0'"
		pop $1
		pop $0
		
		
		StrCmp "$1" "0" 0 db_error ;sql request error
			MSSQL_OLEDB::SQL_GetRow /NOUNLOAD
			pop $1
			pop $0

			StrCmp "$1" "1" db_error 0  ;get row error
			StrCmp "$0" "1" 0 check_create_db ;get 1
				MessageBox MB_ICONQUESTION|MB_YESNOCANCEL "Another version of DeskAlerts database was created in the specified location.$\r$\nWould you like to upgrade it?$\r$\n$\r$\nYes - upgrade the existing database and keep all data$\r$\nNo - remove the existing database and create a new database$\r$\nCancel - choose a different database location" IDYES update IDNO noupdate
					Abort
				update:
					StrCpy $dbupdate "1"
					Goto update_end
				noupdate:
					StrCpy $dbupdate "2"
					MessageBox MB_ICONQUESTION|MB_YESNO "Are you sure you want to remove the existing database with all the data?" IDYES update_end
					Abort
		update_end:
		
		${StrReplace} $0 "]" "]]" "$dbname"
		MSSQL_OLEDB::SQL_Execute /NOUNLOAD "USE [$0]"
		pop $1
		pop $0

			MSSQL_OLEDB::SQL_Execute /NOUNLOAD "SELECT PERMISSIONS()&2"
			pop $1
			pop $0

		StrCmp "$1" "0" 0 db_error ;can't check
			MSSQL_OLEDB::SQL_GetRow /NOUNLOAD
			pop $1
			pop $0
			
			StrCmp "$1" "0" 0 db_error ;can't check
			StrCmp "$0" "0" 0 db_end
				MessageBox MB_OK "Current user have no rights to create tables in database $dbname.$\r$\nPlease enter another username/password or host"
				Abort						
		
	check_create_db:
		MSSQL_OLEDB::SQL_Execute /NOUNLOAD "SELECT PERMISSIONS()&1"
		pop $1
		pop $0

		StrCmp "$1" "0" 0 db_error ;can't check
			MSSQL_OLEDB::SQL_GetRow /NOUNLOAD
			pop $1
			pop $0

			StrCmp "$1" "0" 0 db_error ;can't check
			StrCmp "$0" "0" 0 db_end
				MessageBox MB_OK "Current user have no rights to create database.$\r$\nPlease enter another username/password or host"
				Abort
		
		db_error:
			MSSQL_OLEDB::SQL_GetError /NOUNLOAD
			Pop $1
			Pop $0
			MessageBox MB_ABORTRETRYIGNORE "$0$\r$\n" IDRETRY begin_db IDIGNORE db_end
			Abort
		db_end:
FunctionEnd

;Var "IISBasePathLen"
;Var "IISWholePathLen"
;Var "IISAlertDirNameLen"
Var "IIS_AU_Name"
Var "IIS_AU_Pwd"
Var "INST_AU_Name"
Var "INST_AU_Pwd"

Function ValidateIISAU  ; During validation we're going to check IIS AU name & password along with all the needed 
			; permissions to work with MSSQL on behalf of this amonymous user.
Goto AU_ignore

AU_usr_check:
	IISWork::GetAnonymousUserName /NOUNLOAD
	Pop "$IIS_AU_Name"
	Pop "$0"

	IntCmp "$0" 0 AU_pwd_check
	MessageBox MB_ABORTRETRYIGNORE "Error during retrieval of anonymous user credentials. Check the settings of your IIS." IDRETRY AU_usr_check IDIGNORE AU_ignore
	Abort

	;MessageBox MB_OK "$IIS_AU_Name"
        ;MessageBox MB_OK "$0"

AU_pwd_check:
	IISWork::GetAnonymousUserPwd /NOUNLOAD
	Pop "$IIS_AU_Pwd"
	Pop "$0"

	IntCmp "$0" 0 AU_keep_check
	MessageBox MB_ABORTRETRYIGNORE "Error during retrieval of anonymous user credentials. Check the settings of your IIS." IDRETRY AU_pwd_check IDIGNORE AU_ignore
	Abort

	;MessageBox MB_OK "$IIS_AU_Pwd"
        ;MessageBox MB_OK "$0"

AU_keep_check:
	ReadINIStr "$INST_AU_Name" "$PLUGINSDIR\db.ini" "Field 13" "State"
	ReadINIStr "$INST_AU_Pwd" "$PLUGINSDIR\db.ini" "Field 15" "State"

	;MessageBox MB_OK "$INST_AU_Name"

	StrCmp "$INST_AU_Name" "$IIS_AU_Name" if_equal_names
	MessageBox MB_ABORTRETRYIGNORE "IIS anonymous user name is not correct. Check the name at IIS or specify it correctly." IDRETRY AU_usr_check IDIGNORE AU_ignore
	Abort

if_equal_names:
	StrCmpS "$INST_AU_Pwd" "$IIS_AU_Pwd" if_equal_pwds
	MessageBox MB_ABORTRETRYIGNORE "IIS anonymous user password is not correct. Check the password at IIS or specify it correctly." IDRETRY AU_usr_check IDIGNORE AU_ignore
	Abort

if_equal_pwds:
	; In this place we need to be sure that IIS user has enough rights to work with database.

	detailprint "Loggin on to MS SQL server $host as IIS Anonymous User"
	MSSQL_OLEDB::SQL_Logon /NOUNLOAD "$host" "" ""	
	pop $0
	pop $1
	detailprint $1

	IntCmp "$0" 0 check_iis_au_db_login_exists 0
	MSSQL_OLEDB::SQL_GetError /NOUNLOAD
	Pop $0
	Pop $0
	DetailPrint $0
	MessageBox MB_ABORTRETRYIGNORE "Failed to login to database. Check your batabase settings." IDRETRY if_equal_pwds IDIGNORE AU_ignore
	Abort

check_iis_au_db_login_exists:
	MSSQL_OLEDB::SQL_Execute /NOUNLOAD "USE master"
	pop $0
	pop $1

	MSSQL_OLEDB::SQL_Execute /NOUNLOAD "SELECT * FROM syslogins WHERE name='$INST_AU_Name'"
	pop $0
	pop $1

	IntCmp "$0" 0 check_iis_au_db_rights 0
	MSSQL_OLEDB::SQL_GetError /NOUNLOAD
	Pop $0
	Pop $1
	DetailPrint $1
	MessageBox MB_ABORTRETRYIGNORE "Failed to read IIS anonymous user MSSQL login. Check your batabase settings." IDRETRY check_iis_au_db_login_exists IDIGNORE AU_SQL_validation_finish
	Abort
	

check_iis_au_db_rights:

	MSSQL_OLEDB::SQL_Execute /NOUNLOAD "EXECUTE AS USER = '$INST_AU_Name'"
	pop $0
	pop $1

	MSSQL_OLEDB::SQL_Execute /NOUNLOAD "select PERMISSIONS()"
	pop $0
	pop $1

	MSSQL_OLEDB::SQL_GetRow /NOUNLOAD
	pop $0
	pop $1	

	;check the create db right
	IntOp $3 $1 & 1 
	IntCmp $3 1 check_db_update_right 0
	MessageBox MB_ABORTRETRYIGNORE "Failed to check database creation permissions. Check your batabase settings." IDRETRY check_iis_au_db_rights IDIGNORE AU_SQL_validation_finish
	Abort


check_db_update_right:	
	;IntOp $3 $1 & 1 
	;IntCmp $3 1 AU_SQL_validation_finish 0
	;MessageBox MB_ABORTRETRYIGNORE "Failed to check database creation permissions. Check your batabase settings." IDRETRY check_iis_au_db_rights IDIGNORE AU_SQL_validation_finish
	;Abort

AU_SQL_validation_finish:
	MSSQL_OLEDB::SQL_Logout 

AU_ignore:

	WriteRegStr HKCU "${REGPATH}" "IIS_user" "$INST_AU_Name"

FunctionEnd
;-------------------------------------------------------------------------------- PAGE 4 -----------


Function SetStandart
;-------------------------------------------------------------------------------- Clear prev install
	RMDir /r "$TEMP\AlertServer"
;-------------------------------------------------------------------------------- Unpack files -----
	Banner::show /NOUNLOAD /set 76 "Server Installation" "Unpacking files ... "
	Banner::getWindow /NOUNLOAD
	pop $0 ; get result

	SetDetailsPrint none

!ifdef isADAddon
	SetOutPath "$TEMP\AlertServer\AlertServerUnzip\MODULES\AD MODULE"
	File /r /x ".svn" "..\sever\MODULES\AD MODULE\*.*"
!endif

!ifdef isEDAddon
	SetOutPath "$TEMP\AlertServer\AlertServerUnzip\MODULES\EDIR MODULE"
	File /r /x ".svn" "..\sever\MODULES\EDIR MODULE\*.*"
!endif

!ifdef isSMSAddon
	SetOutPath "$TEMP\AlertServer\AlertServerUnzip\MODULES\SMS MODULE"
	File /r /x ".svn" "..\sever\MODULES\SMS MODULE\*.*"
!endif

!ifdef isEMAILAddon
	SetOutPath "$TEMP\AlertServer\AlertServerUnzip\MODULES\EMAIL MODULE"
	File /r /x ".svn" "..\sever\MODULES\EMAIL MODULE\*.*"
!endif

!ifdef isENCRYPTAddon
	SetOutPath "$TEMP\AlertServer\AlertServerUnzip\MODULES\ENCRYPT MODULE"
	File /r /x ".svn" "..\sever\MODULES\ENCRYPT MODULE\*.*"
!endif

!ifdef isSURVEYSAddon
	SetOutPath "$TEMP\AlertServer\AlertServerUnzip\MODULES\SURVEYS MODULE"
	File /r /x ".svn" "..\sever\MODULES\SURVEYS MODULE\*.*"
!endif

!ifdef isSTATISTICSAddon
	SetOutPath "$TEMP\AlertServer\AlertServerUnzip\MODULES\EXTENDED STATISTICS"
	File /r /x ".svn" "..\sever\MODULES\EXTENDED STATISTICS\*.*"
!endif

!ifdef isSCREENSAVERAddon
	SetOutPath "$TEMP\AlertServer\AlertServerUnzip\MODULES\SCREENSAVER"
	File /r /x ".svn" "..\sever\MODULES\SCREENSAVER\*.*"
!endif

!ifdef isRSSAddon
	SetOutPath "$TEMP\AlertServer\AlertServerUnzip\MODULES\RSS"
	File /r /x ".svn" "..\sever\MODULES\RSS\*.*"
!endif

!ifdef isWALLPAPERAddon
	SetOutPath "$TEMP\AlertServer\AlertServerUnzip\MODULES\WALLPAPER"
	File /r /x ".svn" "..\sever\MODULES\WALLPAPER\*.*"
!endif

!ifdef isBLOGAddon
	SetOutPath "$TEMP\AlertServer\AlertServerUnzip\MODULES\BLOG"
	File /r /x ".svn" "..\sever\MODULES\BLOG\*.*"
!endif

!ifdef isTWITTERAddon
	SetOutPath "$TEMP\AlertServer\AlertServerUnzip\MODULES\SOCIAL_MEDIA"
	File /r /x ".svn" "..\sever\MODULES\SOCIAL_MEDIA\*.*"
!endif

!ifdef isLINKEDINAddon
	SetOutPath "$TEMP\AlertServer\AlertServerUnzip\MODULES\SOCIAL_LINKEDIN"
	File /r /x ".svn" "..\sever\MODULES\SOCIAL_LINKEDIN\*.*"
!endif

!ifdef isFULLSCREENAddon
	SetOutPath "$TEMP\AlertServer\AlertServerUnzip\MODULES\FULLSCREEN"
	File /r /x ".svn" "..\sever\MODULES\FULLSCREEN\*.*"
!endif

!ifdef isVIDEOAddon
	SetOutPath "$TEMP\AlertServer\AlertServerUnzip\MODULES\VIDEOALERT"
	File /r /x ".svn" "..\sever\MODULES\VIDEOALERT\*.*"
!endif

!ifdef isTICKERAddon
	SetOutPath "$TEMP\AlertServer\AlertServerUnzip\MODULES\TICKER"
	File /r /x ".svn" "..\sever\MODULES\TICKER\*.*"
!endif

!ifdef isTICKERPROAddon
	SetOutPath "$TEMP\AlertServer\AlertServerUnzip\MODULES\TICKER_PRO"
	File /r /x ".svn" "..\sever\MODULES\TICKER_PRO\*.*"
!endif
!ifdef isIMAddon
	SetOutPath "$TEMP\AlertServer\AlertServerUnzip\MODULES\INSTANT_MESSAGES"
	File /r /x ".svn" "..\sever\MODULES\INSTANT_MESSAGES\*.*"
!endif

!ifdef isWIDGETSTATAddon
	SetOutPath "$TEMP\AlertServer\AlertServerUnzip\MODULES\WIDGETSTAT"
	File /r /x ".svn" "..\sever\MODULES\WIDGETSTAT\*.*"
!endif

!ifdef isTEXTCALLAddon
	SetOutPath "$TEMP\AlertServer\AlertServerUnzip\MODULES\TEXTCALL MODULE"
	File /r /x ".svn" "..\sever\MODULES\TEXTCALL MODULE\*.*"
!endif

!ifdef isMOBILEAddon
	SetOutPath "$TEMP\AlertServer\AlertServerUnzip\MODULES\MOBILE"
	File /r /x ".svn" "..\sever\MODULES\MOBILE\*.*"
!endif

!ifdef isWEBPLUGINAddon
	SetOutPath "$TEMP\AlertServer\AlertServerUnzip\MODULES\WEBPLUGIN"
	File /r /x ".svn" "..\sever\MODULES\WEBPLUGIN\*.*"
!endif

!ifdef isCAMPAIGNAddon
	SetOutPath "$TEMP\AlertServer\AlertServerUnzip\MODULES\CAMPAIGN MODULE"
	File /r /x ".svn" "..\sever\MODULES\CAMPAIGN MODULE\*.*"
!endif

!ifdef isAPPROVEAddon
	SetOutPath "$TEMP\AlertServer\AlertServerUnzip\MODULES\APPROVAL"
	File /r /x ".svn" "..\sever\MODULES\APPROVAL\*.*"
!endif

!ifdef isDIGSIGNAddon
	SetOutPath "$TEMP\AlertServer\AlertServerUnzip\MODULES\DIGSIGN"
	File /r /x ".svn" "..\sever\MODULES\DIGSIGN\*.*"
!endif

	SetOutPath "$TEMP\AlertServer\AlertServerUnzip\STANDARD"

;!ifdef withASPNETServer
;	File /r /x ".svn" /x "*.user" /x "*.csproj" "..\..\branch\new\DeskAlertsServer\*.*"
;!endif

	File /r /x ".svn" "..\sever\STANDARD\*.*"

!ifdef nineServer
	File /r /x ".svn" "..\sever\NINE\*.*"
!else
	!ifdef eightServer
		File /r /x ".svn" "..\sever\EIGHT\*.*"
	!else
		!ifdef sevenServer
			File /r /x ".svn" "..\sever\SEVEN\*.*"
		!else
			!ifdef sixServer
				File /r /x ".svn" "..\sever\SIX\*.*"
			!else
				!ifdef fiveServer
					File /r /x ".svn" "..\sever\FIVE\*.*"
				!endif
			!endif
		!endif
	!endif
!endif

!ifdef demoServer
	File /r /x ".svn" "..\sever\DEMO\*.*"
!endif

	;SetOutPath "$TEMP\AlertServer"
	;File "${ZIP_NAME}"
	;ZipDLL::extractall "$TEMP\AlertServer\${ZIP_NAME}" "$TEMP\AlertServer\AlertServerUnzip"

	SetDetailsPrint both

	Banner::destroy
FunctionEnd

;-------------------------------------------------------------------------------- AD -----------
Function SetADAddon
	
	!insertmacro MUI_HEADER_TEXT " Active Directory module" "Configure Active Directory module settings"	

	StrCmp "$notfirstrun_ad" "1" not_first_run_ad
		${CheckByExistsFileOrRegistryAndWrite} "$PLUGINSDIR\AD_addon.ini" "Field 1" "$alerts_folder\admin\nj6kjvk58s8wb2d73.key" "adDisabled" $0

		StrCpy $notfirstrun_ad "1"
	not_first_run_ad:

	Push $R0
	InstallOptions::dialog "$PLUGINSDIR\AD_addon.ini"
	Pop $R0
	Pop $R0
FunctionEnd

Function ValidateADAddon
FunctionEnd

Function ApplyADAddon
	ReadINIStr $1 "$PLUGINSDIR\AD_addon.ini" "Field 1" "State"
	WriteRegStr HKCU "${REGPATH}" "adDisabled" "$1"
	StrCmp "$1" "1" if_not_install
		SetDetailsPrint none
		${ReplaceInASPConfig} "$TEMP\${AD_MODULE_conf}" "%AD%" "3"
		${FileJoin} "$TEMP\${MAIN_conf}" "$TEMP\${AD_MODULE_conf}" "$TEMP\${MAIN_conf}"
		${ReplaceInASPConfig} "$TEMP\${MAIN_DOTNET_conf}" "<!--MODULE_AD-->"  "<add key='AD' value='1' />"
		CopyFiles /SILENT "$TEMP\AlertServer\AlertServerUnzip\MODULES\AD MODULE\*" "$TEMP\AlertServer\AlertServerUnzip\STANDARD\"
		SetDetailsPrint both
	if_not_install:	
FunctionEnd

Function ApplyEDAddon
	;ReadINIStr $1 "$PLUGINSDIR\ED_addon.ini" "Field 1" "State"
	;WriteRegStr HKCU "${REGPATH}" "edDisabled" "$1"
	;StrCmp "$1" "1" if_not_install
		SetDetailsPrint none
		${FileJoin} "$TEMP\${MAIN_conf}" "$TEMP\${ED_MODULE_conf}" "$TEMP\${MAIN_conf}"
		${ReplaceInASPConfig} "$TEMP\${MAIN_DOTNET_conf}" "<!--MODULE_ED-->"  "<add key='ED' value='1' />"
		CopyFiles /SILENT "$TEMP\AlertServer\AlertServerUnzip\MODULES\EDIR MODULE\*" "$TEMP\AlertServer\AlertServerUnzip\STANDARD\"
		SetDetailsPrint both
	;if_not_install:
FunctionEnd

;-------------------------------------------------------------------------------- SMS -----------
Var "smsUrl"
Var "smsPostData"
Var "smsAPI_ID"
Var "smsProxy"
Var "smsProxyServer"
Var "smsProxyUseAuth"
Var "smsProxyUser"
Var "smsProxyPass"
Function SetSMSAddon
	
	!insertmacro MUI_HEADER_TEXT " SMS module" "Configure SMS module settings"	

	StrCmp "$notfirstrun_sms" "1" not_first_run_sms

		ReadRegStr $0 HKCU "${REGPATH}" "smsUsername"
		StrCmp $0 "" +2
			WriteINIStr "$PLUGINSDIR\SMS_addon.ini" "${smsUsernameField}" "State" $0

		ReadRegStr $0 HKCU "${REGPATH}" "smsPassword"
		StrCmp $0 "" +2
			WriteINIStr "$PLUGINSDIR\SMS_addon.ini" "${smsPasswordField}" "State" $0

		ReadRegStr $0 HKCU "${REGPATH}" "smsAPI_ID"
		StrCmp $0 "" +2
			StrCpy $smsAPI_ID $0

		StrCpy $smsUrl "http://api.clickatell.com/http/sendmsg"
		StrCpy $smsPostData "api_id=&user=&password=&to=%mobilePhone%&text=%smsText%"

		ReadRegStr $0 HKCU "${REGPATH}" "smsUrl"
		StrCmp $0 "" +2
			StrCpy $smsUrl $0

		ReadRegStr $0 HKCU "${REGPATH}" "smsPostData"
		StrCmp $0 "" +2
			StrCpy $smsPostData $0

		ReadRegStr $R0 HKCU "${REGPATH}" "smsCustom"

		StrCpy $0 ""
		${ReadFromASPConfigIfUpdate} "smsUrl" $0
		StrCmp $0 "" sms_not_from_asp
			StrCpy $smsUrl $0

			${ReadFromASPConfigIfUpdate} "smsPostData" $0
			StrCpy $smsPostData $0

			StrCpy $R0 "1"
		sms_not_from_asp:

		StrCmp $R0 "" +4
			WriteINIStr "$PLUGINSDIR\SMS_addon.ini" "${smsUseCustomField}" "State" $R0
			IntOp $1 $R0 !
			WriteINIStr "$PLUGINSDIR\SMS_addon.ini" "${smsUseClickatellField}" "State" $1

		StrCmp "$R0" "1" if_custom
			WriteINIStr "$PLUGINSDIR\SMS_addon.ini" "${smsUrlField}" "State" "http://api.clickatell.com/http/sendmsg"
			WriteINIStr "$PLUGINSDIR\SMS_addon.ini" "${smsUrlField}" "Flags" "READONLY"
			WriteINIStr "$PLUGINSDIR\SMS_addon.ini" "${smsAPIIDField}" "State" "$smsAPI_ID"
			WriteINIStr "$PLUGINSDIR\SMS_addon.ini" "${smsAPIIDPOSTLabel}" "Text" "API ID:"
			WriteINIStr "$PLUGINSDIR\SMS_addon.ini" "${smsURLLabel}" "Text" "URL:"
			Goto end_custom
		if_custom:
			WriteINIStr "$PLUGINSDIR\SMS_addon.ini" "${smsUrlField}" "State" "$smsUrl"
			WriteINIStr "$PLUGINSDIR\SMS_addon.ini" "${smsUrlField}" "Flags" ""
			WriteINIStr "$PLUGINSDIR\SMS_addon.ini" "${smsPostDataField}" "State" "$smsPostData"
			WriteINIStr "$PLUGINSDIR\SMS_addon.ini" "${smsAPIIDPOSTLabel}" "Text" "POST:"
			WriteINIStr "$PLUGINSDIR\SMS_addon.ini" "${smsURLLabel}" "Text" "URL/GET:"
		end_custom:

		${CheckByExistsFileOrRegistryAndWrite} "$PLUGINSDIR\SMS_addon.ini" "${smsDontInstField}" "$alerts_folder\admin\3sdfm98a3nz73h825.key" "smsDisabled" $0

		StrCpy $notfirstrun_sms "1"
	not_first_run_sms:

	Push $R0
	Push $R1
	Push $R2
		InstallOptions::initDialog /NOUNLOAD "$PLUGINSDIR\SMS_addon.ini"
		Pop $R0 ;HWND of dialog
		
		ReadINIStr $1 "$PLUGINSDIR\SMS_addon.ini" "${smsUseCustomField}" "State"
		ReadINIStr $2 "$PLUGINSDIR\SMS_addon.ini" "${smsParamsLabel}" "HWND"
		ShowWindow $2 $1

		IntOp $1 $1 !
		ReadINIStr $2 "$PLUGINSDIR\SMS_addon.ini" "${smsUsernameField}" "HWND"
		ReadINIStr $3 "$PLUGINSDIR\SMS_addon.ini" "${smsPasswordField}" "HWND"
		ReadINIStr $4 "$PLUGINSDIR\SMS_addon.ini" "${smsUsernameLabel}" "HWND"
		ReadINIStr $5 "$PLUGINSDIR\SMS_addon.ini" "${smsPasswordLabel}" "HWND"
		ShowWindow $2 $1
		ShowWindow $3 $1
		ShowWindow $4 $1
		ShowWindow $5 $1

		InstallOptions::show
		Pop $R0
	Pop $R2
	Pop $R1
	Pop $R0
FunctionEnd

Function ValidateSMSAddon
	ReadINIStr $0 "$PLUGINSDIR\SMS_addon.ini" "Settings" "State"
	IntCmp $0 0 if_next ;if $0 = 0 was clicked next button otherwise it was notify
		ReadINIStr $1 "$PLUGINSDIR\SMS_addon.ini" "${smsUseCustomField}" "State"
		ReadINIStr $2 "$PLUGINSDIR\SMS_addon.ini" "${smsUrlField}" "HWND"
		ReadINIStr $6 "$PLUGINSDIR\SMS_addon.ini" "${smsAPIIDPOSTLabel}" "HWND"
		ReadINIStr $7 "$PLUGINSDIR\SMS_addon.ini" "${smsURLLabel}" "HWND"

		IntCmp $1 1 if_custom
			ReadINIStr $smsUrl "$PLUGINSDIR\SMS_addon.ini" "${smsUrlField}" "State"
			SendMessage $2 ${WM_SETTEXT} 0 "STR:http://api.clickatell.com/http/sendmsg"
			WriteINIStr "$PLUGINSDIR\SMS_addon.ini" "${smsUrlField}" "State" "http://api.clickatell.com/http/sendmsg"
			WriteINIStr "$PLUGINSDIR\SMS_addon.ini" "${smsUrlField}" "Flags" "READONLY"
			
			ReadINIStr $9 "$PLUGINSDIR\SMS_addon.ini" "${smsPostDataField}" "State"
			StrCpy $smsPostData $9
			
			ReadINIStr $3 "$PLUGINSDIR\SMS_addon.ini" "${smsAPIIDField}" "HWND"
			SendMessage $3 ${WM_SETTEXT} 0 "STR:$smsAPI_ID"
			WriteINIStr "$PLUGINSDIR\SMS_addon.ini" "${smsAPIIDField}" "State" "$smsAPI_ID"
			
			SendMessage $6 ${WM_SETTEXT} 0 "STR:API ID:"
			WriteINIStr "$PLUGINSDIR\SMS_addon.ini" "${smsAPIIDPOSTLabel}" "Text" "API ID:"
			
			SendMessage $7 ${WM_SETTEXT} 0 "STR:URL:"
			WriteINIStr "$PLUGINSDIR\SMS_addon.ini" "${smsURLLabel}" "Text" "URL:"
			Goto end_custom
		if_custom:
			SendMessage $2 ${WM_SETTEXT} 0 "STR:$smsUrl"
			WriteINIStr "$PLUGINSDIR\SMS_addon.ini" "${smsUrlField}" "State" "$smsUrl"
			WriteINIStr "$PLUGINSDIR\SMS_addon.ini" "${smsUrlField}" "Flags" ""
			
			ReadINIStr $9 "$PLUGINSDIR\SMS_addon.ini" "${smsAPIIDField}" "State"
			StrCpy $smsAPI_ID $9
			
			ReadINIStr $3 "$PLUGINSDIR\SMS_addon.ini" "${smsPostDataField}" "HWND"
			SendMessage $3 ${WM_SETTEXT} 0 "STR:$smsPostData"
			WriteINIStr "$PLUGINSDIR\SMS_addon.ini" "${smsPostDataField}" "State" "$smsPostData"
			
			SendMessage $6 ${WM_SETTEXT} 0 "STR:POST:"
			WriteINIStr "$PLUGINSDIR\SMS_addon.ini" "${smsAPIIDPOSTLabel}" "Text" "POST:"
			
			SendMessage $7 ${WM_SETTEXT} 0 "STR:URL/GET:"
			WriteINIStr "$PLUGINSDIR\SMS_addon.ini" "${smsURLLabel}" "Text" "URL/GET:"
		end_custom:

		ReadINIStr $6 "$PLUGINSDIR\SMS_addon.ini" "${smsParamsLabel}" "HWND"
		ShowWindow $6 $1

		IntOp $1 $1 !
		SendMessage $2 ${EM_SETREADONLY} $1 0
		
		ReadINIStr $2 "$PLUGINSDIR\SMS_addon.ini" "${smsUsernameField}" "HWND"
		ReadINIStr $3 "$PLUGINSDIR\SMS_addon.ini" "${smsPasswordField}" "HWND"
		ReadINIStr $4 "$PLUGINSDIR\SMS_addon.ini" "${smsUsernameLabel}" "HWND"
		ReadINIStr $5 "$PLUGINSDIR\SMS_addon.ini" "${smsPasswordLabel}" "HWND"
		ShowWindow $2 $1
		ShowWindow $3 $1
		ShowWindow $4 $1
		ShowWindow $5 $1

		Abort
	if_next:

	ReadINIStr $1 "$PLUGINSDIR\SMS_addon.ini" "${smsDontInstField}" "State"
	ReadINIStr $0 "$PLUGINSDIR\SMS_addon.ini" "${smsUseCustomField}" "State"
	StrCmp "$1" "1" dont_validate
	StrCmp "$0" "1" dont_validate ;custom gateway
		ReadINIStr $2 "$PLUGINSDIR\SMS_addon.ini" "${smsAPIIDField}" "State"
		ReadINIStr $3 "$PLUGINSDIR\SMS_addon.ini" "${smsUsernameField}" "State"
		ReadINIStr $4 "$PLUGINSDIR\SMS_addon.ini" "${smsPasswordField}" "State"

		GetTempFileName $R1

		Banner::show /NOUNLOAD /set 76 "Server installation" "Validating ... "
		Banner::getWindow /NOUNLOAD
		pop $0 ; get result

		inetc::get /caption "Validating server" /SILENT /nocancel "http://api.clickatell.com/http/sendmsg?user=$3&password=$4&api_id=$2"  $R1 /end
		pop $0 ; get result
		FileOpen $4 $R1 r
		FileRead $4 $1 8
		FileClose $4 ; and close the file
			 
		StrCmp "$1" "ERR: 001" if_validate_err
		StrCmp "$1" "ERR: 108" if_validate_err
		Goto if_no_err
		if_validate_err:
			MessageBox MB_OK "$1. Sorry, this user has no rights to access SMS Gateway. Please enter another username/password."
			Banner::destroy
			Abort
		if_no_err:
		Banner::destroy
	dont_validate:	
FunctionEnd

Function ApplySMSAddon

	
	ReadINIStr $0 "$PLUGINSDIR\SMS_addon.ini" "${smsDontInstField}" "State"
	ReadINIStr $9 "$PLUGINSDIR\SMS_addon.ini" "${smsUseCustomField}" "State"

	ReadINIStr $1 "$PLUGINSDIR\SMS_addon.ini" "${smsUrlField}" "State"
	ReadINIStr $3 "$PLUGINSDIR\SMS_addon.ini" "${smsUsernameField}" "State"
	ReadINIStr $4 "$PLUGINSDIR\SMS_addon.ini" "${smsPasswordField}" "State"

	ReadINIStr $2 "$PLUGINSDIR\SMS_addon.ini" "${smsAPIIDField}" "State"
	ReadINIStr $5 "$PLUGINSDIR\SMS_addon.ini" "${smsPostDataField}" "State"
	
	NsisCrypt::Base64Encode $5
	Pop $5
	STRCPY $encrypted_pass $5

	StrCmp "$0" "1" if_not_install
		SetDetailsPrint none
		StrCmp "$9" "1" 0 if_not_custom
			${ReplaceInASPConfig} "$TEMP\${SMS_MODULE_conf}" "%smsUrl%"      "$1"
			${ReplaceInASPConfig} "$TEMP\${SMS_MODULE_conf}" "%smsPostData%" "$encrypted_pass"
			${FileJoin} "$TEMP\${MAIN_conf}" "$TEMP\${SMS_MODULE_conf}" "$TEMP\${MAIN_conf}"
			${ReplaceInASPConfig} "$TEMP\${MAIN_DOTNET_conf}" "<!--MODULE_SMS-->"  "<add key='SMS' value='1' />"
			Goto end_custom
		if_not_custom:
			${ReplaceInASPConfig} "$TEMP\${SMS_MODULE_conf}" "%smsUrl%"      "$1"
			${ReplaceInASPConfig} "$TEMP\${SMS_MODULE_conf}" "%smsPostData%" "$encrypted_pass"
			${FileJoin} "$TEMP\${MAIN_conf}" "$TEMP\${SMS_MODULE_conf}" "$TEMP\${MAIN_conf}"
		end_custom:

		${ReplaceInASPConfig} "$TEMP\${SMS_MODULE_conf}" "%smsProxy%"        "$smsProxy"
		${ReplaceInASPConfig} "$TEMP\${SMS_MODULE_conf}" "%smsProxyServer%"  "$smsProxyServer"
		${ReplaceInASPConfig} "$TEMP\${SMS_MODULE_conf}" "%smsProxyUseAuth%" "$smsProxyUseAuth"
		${ReplaceInASPConfig} "$TEMP\${SMS_MODULE_conf}" "%smsProxyUser%"    "$smsProxyUser"
		${ReplaceInASPConfig} "$TEMP\${SMS_MODULE_conf}" "%smsProxyPass%"    "$smsProxyPass"

		CopyFiles /SILENT "$TEMP\AlertServer\AlertServerUnzip\MODULES\SMS MODULE\*" "$TEMP\AlertServer\AlertServerUnzip\STANDARD\"
		SetDetailsPrint both
		
	if_not_install:	
	
	StrCmp "$9" "1" 0 if_not_custom2
		StrCpy $smsUrl $1
		StrCpy $smsPostData $2
		Goto end_custom2
	if_not_custom2:
		StrCpy $smsAPI_ID $5
	end_custom2:

	WriteRegStr HKCU "${REGPATH}" "smsDisabled" "$0"
	WriteRegStr HKCU "${REGPATH}" "smsCustom"   "$9"

	WriteRegStr HKCU "${REGPATH}" "smsUrl"      "$smsUrl"
	WriteRegStr HKCU "${REGPATH}" "smsPostData" "$smsPostData"
	WriteRegStr HKCU "${REGPATH}" "smsAPI_ID"   "$smsAPI_ID"

	WriteRegStr HKCU "${REGPATH}" "smsUsername" "$3"
	WriteRegStr HKCU "${REGPATH}" "smsPassword" "$4"
FunctionEnd


;-------------------------------------------------------------------------------- TEXTCALL -----------
Var "textcallAuthUser"
Var "textcallAuthPass"
Var "phoneUser"

Function SetTEXTCALLAddon
	
	!insertmacro MUI_HEADER_TEXT " Text to call module" "Configure Text to Call module settings"	

	StrCmp "$notfirstrun_textcall" "1" not_first_run_textcall

		ReadRegStr $0 HKCU "${REGPATH}" "textcallAuthUser"
		StrCmp $0 "" +2
			WriteINIStr "$PLUGINSDIR\TEXTCALL_addon.ini" "${textcallAuthUser}" "State" $0

		ReadRegStr $0 HKCU "${REGPATH}" "textcallAuthPass"
		StrCmp $0 "" +2
			WriteINIStr "$PLUGINSDIR\TEXTCALL_addon.ini" "${textcallAuthPass}" "State" $0

			ReadRegStr $0 HKCU "${REGPATH}" "phoneUser"
		StrCmp $0 "" +2
			WriteINIStr "$PLUGINSDIR\TEXTCALL_addon.ini" "${phoneUser}" "State" $0
			
		${CheckByExistsFileOrRegistryAndWrite} "$PLUGINSDIR\TEXTCALL_addon.ini" "${textcallDontInstField}" "$alerts_folder\admin\8gs6anv5e43eyuq48.key" "textcallDisabled" $0

		StrCpy $notfirstrun_textcall "1"
	not_first_run_textcall:

	Push $R0
	Push $R1
	Push $R2
		InstallOptions::initDialog /NOUNLOAD "$PLUGINSDIR\TEXTCALL_addon.ini"
		Pop $R0 ;HWND of dialog

		ReadINIStr $1 "$PLUGINSDIR\TEXTCALL_addon.ini" "${textcallAuthUser}" "HWND"
		ReadINIStr $2 "$PLUGINSDIR\TEXTCALL_addon.ini" "${textcallAuthPass}" "HWND"
		ReadINIStr $3 "$PLUGINSDIR\TEXTCALL_addon.ini" "${phoneUser}" "HWND"

		InstallOptions::show
		Pop $R0
	Pop $R2
	Pop $R1
	Pop $R0
FunctionEnd

Function ValidateTEXTCALLAddon
	ReadINIStr $0 "$PLUGINSDIR\TEXTCALL_addon.ini" "Settings" "State"
	IntCmp $0 0 if_next ;if $0 = 0 was clicked next button otherwise it was notify
		
			SendMessage $2 ${WM_SETTEXT} 0 "STR:$textcallAuthUser"
			WriteINIStr "$PLUGINSDIR\TEXTCALL_addon.ini" "${textcallAuthUser}" "State" "$textcallAuthUser"
			SendMessage $3 ${WM_SETTEXT} 0 "STR:$textcallAuthPass"
			WriteINIStr "$PLUGINSDIR\TEXTCALL_addon.ini" "${textcallAuthPass}" "State" "$textcallAuthPass"
			SendMessage $4 ${WM_SETTEXT} 0 "STR:phoneUser"
			WriteINIStr "$PLUGINSDIR\TEXTCALL_addon.ini" "${phoneUser}" "State" "$phoneUser"		

	if_next:

	ReadINIStr $1 "$PLUGINSDIR\TEXTCALL_addon.ini" "${textcallDontInstField}" "State"

	StrCmp "$1" "1" dont_validate

		ReadINIStr $2 "$PLUGINSDIR\TEXTCALL_addon.ini" "${textcallAuthUser}" "State"
		ReadINIStr $3 "$PLUGINSDIR\TEXTCALL_addon.ini" "${textcallAuthPass}" "State"
		ReadINIStr $4 "$PLUGINSDIR\TEXTCALL_addon.ini" "${phoneUser}" "State"
		GetTempFileName $R1

		Banner::show /NOUNLOAD /set 76 "Server installation" "Validating ... "
		Banner::getWindow /NOUNLOAD
		pop $0 ; get result
		inetc::get /caption "Validating server" /SILENT /nocancel "https://api.twilio.com/2010-04-01/Accounts/$2/Calls"  $R1 /end
		pop $0 ; get result
		FileOpen $4 $R1 r
		FileRead $4 $1 8
		FileClose $4 ; and close the file
		StrCmp "$1" "ERR: 001" if_validate_err
		StrCmp "$1" "ERR: 108" if_validate_err
		Goto if_no_err
		if_validate_err:
			MessageBox MB_OK "$1. Sorry, this user has no rights to access TEXT CALL Gateway. Please enter another username/password."
			Banner::destroy
			Abort
		if_no_err:
		Banner::destroy
	dont_validate:	
FunctionEnd

Function ApplyTEXTCALLAddon
	ReadINIStr $0 "$PLUGINSDIR\TEXTCALL_addon.ini" "Field 4" "State"
	ReadINIStr $1 "$PLUGINSDIR\TEXTCALL_addon.ini" "Field 1" "State"
	ReadINIStr $2 "$PLUGINSDIR\TEXTCALL_addon.ini" "Field 2" "State"
	ReadINIStr $3 "$PLUGINSDIR\TEXTCALL_addon.ini" "Field 3" "State"

	WriteRegStr HKCU "${REGPATH}" "textcallDisabled" $0
	WriteRegStr HKCU "${REGPATH}" "textcallAuthUser" $1
	WriteRegStr HKCU "${REGPATH}" "textcallAuthPass" $2
	WriteRegStr HKCU "${REGPATH}" "phoneUser"   $3
	
	StrCmp "$0" "1" if_not_install
		SetDetailsPrint none
		
		${ReplaceInASPConfig} "$TEMP\${TEXTCALL_MODULE_conf}" "%textcallAuthUser%"  "$1"
		${ReplaceInASPConfig} "$TEMP\${TEXTCALL_MODULE_conf}" "%textcallAuthPass%"  "$2"
		${ReplaceInASPConfig} "$TEMP\${TEXTCALL_MODULE_conf}" "%phoneUser%" "$3"
		${FileJoin} "$TEMP\${MAIN_conf}" "$TEMP\${TEXTCALL_MODULE_conf}" "$TEMP\${MAIN_conf}"
		${ReplaceInASPConfig} "$TEMP\${MAIN_DOTNET_conf}" "<!--MODULE_TEXTTOCALL-->"  "<add key='TEXTTOCALL' value='1' />"
		CopyFiles /SILENT "$TEMP\AlertServer\AlertServerUnzip\MODULES\TEXTCALL MODULE\*" "$TEMP\AlertServer\AlertServerUnzip\STANDARD\"
		SetDetailsPrint both
		
	if_not_install:	
FunctionEnd
;-------------------------------------------------------------------------------- PAGE 7 -----------
Var "ENC"
Var "encrypt_key"
Function SetENCRYPTAddon
	
	!insertmacro MUI_HEADER_TEXT " Encrypt module" "Configure Encrypt module settings"	

	StrCmp "$notfirstrun_encrypt" "1" not_first_run_encrypt
		;allways enabled
		${SetRadiobox} "$PLUGINSDIR\ENCRYPT_addon.ini" "Field 2" "1" "1" "0"
		${SetRadiobox} "$PLUGINSDIR\ENCRYPT_addon.ini" "Field 3" "1" "1" "1"

		${ReadFromASPorRegistryAndWrite} "$PLUGINSDIR\ENCRYPT_addon.ini" "Field 1" "encrypt_key" "encrypt_key" $0
		StrCmp $0 "" 0 +3
			${RandomStr} $0 10
			WriteINIStr "$PLUGINSDIR\ENCRYPT_addon.ini" "Field 1" "State" $0

		${CheckByExistsFileOrRegistryAndWrite} "$PLUGINSDIR\ENCRYPT_addon.ini" "Field 4" "$alerts_folder\a3nz73h8253sdfm98.key" "encryptDisabled" $0

		${ReadFromASPorRegistry} "ENC" "ENC" $0
		StrCmp $0 "0" 0 +2
			WriteINIStr "$PLUGINSDIR\ENCRYPT_addon.ini" "Field 4" "State" "1"

		StrCpy $notfirstrun_encrypt "1"
	not_first_run_encrypt:

	Push $R0
	InstallOptions::dialog "$PLUGINSDIR\ENCRYPT_addon.ini"
	Pop $R0
	Pop $R0
FunctionEnd

Function ValidateENCRYPTAddon
	ReadINIStr $encrypt_key "$PLUGINSDIR\ENCRYPT_addon.ini" "Field 1" "State"
	ReadINIStr $1           "$PLUGINSDIR\ENCRYPT_addon.ini" "Field 2" "State"
	ReadINIStr $2           "$PLUGINSDIR\ENCRYPT_addon.ini" "Field 3" "State"

	StrCmp "$1" "1" 0 if_not_dis
		StrCpy $ENC "0"
	if_not_dis:

	StrCmp "$2" "1" 0 if_not_enb
		StrCpy $ENC "1"
	if_not_enb:
FunctionEnd

Function ApplyENCRYPTAddon
	ReadINIStr $1 "$PLUGINSDIR\ENCRYPT_addon.ini" "Field 4" "State"

	WriteRegStr HKCU "${REGPATH}" "encryptDisabled" $1
	WriteRegStr HKCU "${REGPATH}" "ENC"             "$ENC"
	WriteRegStr HKCU "${REGPATH}" "encrypt_key"     "$encrypt_key"

	StrCmp "$1" "1" if_not_install
		SetDetailsPrint none
		${ReplaceInASPConfig} "$TEMP\${ENCRYPT_MODULE_conf}" "%ENC%"         "$ENC"
		${ReplaceInASPConfig} "$TEMP\${ENCRYPT_MODULE_conf}" "%encrypt_key%" "$encrypt_key"
		${FileJoin} "$TEMP\${MAIN_conf}" "$TEMP\${ENCRYPT_MODULE_conf}" "$TEMP\${MAIN_conf}"
		${ReplaceInASPConfig} "$TEMP\${MAIN_DOTNET_conf}" "<!--MODULE_ENCRYPT-->"  "<add key='ENCRYPT' value='1' />"
		CopyFiles /SILENT "$TEMP\AlertServer\AlertServerUnzip\MODULES\ENCRYPT MODULE\*" "$TEMP\AlertServer\AlertServerUnzip\STANDARD\"
		SetDetailsPrint both
	if_not_install:	
FunctionEnd

;-------------------------------------------------------------------------------- PAGE 8 -----------
Function SetSURVEYSAddon

	!insertmacro MUI_HEADER_TEXT " Surveys module" "Install Surveys module"	

	StrCmp "$notfirstrun_surveys" "1" not_first_run_surveys
		${CheckByExistsFileOrRegistryAndWrite} "$PLUGINSDIR\SURVEYS_addon.ini" "Field 1" "$alerts_folder\admin\h8djke5m3p90cn1ud.key" "surveysDisabled" $0

		StrCpy $notfirstrun_surveys "1"
	not_first_run_surveys:

	Push $R0
	InstallOptions::dialog "$PLUGINSDIR\SURVEYS_addon.ini"
	Pop $R0
	Pop $R0
FunctionEnd

Function ValidateSURVEYSAddon	
FunctionEnd

Function ApplySURVEYSAddon
	ReadINIStr $1 "$PLUGINSDIR\SURVEYS_addon.ini" "Field 1" "State"
	WriteRegStr HKCU "${REGPATH}" "surveysDisabled" "$1"
	StrCmp "$1" "1" if_not_install
		SetDetailsPrint none
		CopyFiles /SILENT "$TEMP\AlertServer\AlertServerUnzip\MODULES\SURVEYS MODULE\*" "$TEMP\AlertServer\AlertServerUnzip\STANDARD\"
		${ReplaceInASPConfig} "$TEMP\${MAIN_DOTNET_conf}" "<!--MODULE_SURVEYS-->"  "<add key='SURVEYS' value='1' />"
		SetDetailsPrint both
	if_not_install:	
FunctionEnd

;-------------------------------------------------------------------------------- PAGE 9 -----------
Function SetThanks
	Push $R0
	InstallOptions::dialog "$PLUGINSDIR\thanks.ini"
	Pop $R0
	Pop $R0
FunctionEnd

Function ValidateThanks
FunctionEnd

;-------------------------------------------------------------------------------- PAGE 10 -----------
Function SetEXTSTATAddon

	!insertmacro MUI_HEADER_TEXT " Extended reports module" "Install reports module"		

	StrCmp "$notfirstrun_extstat" "1" not_first_run_extstat
		${CheckByExistsFileOrRegistryAndWrite} "$PLUGINSDIR\ExtStat.ini" "Field 1" "$alerts_folder\admin\k58s6kjv28wbnjd73.key" "extstatDisabled" $0

		StrCpy $notfirstrun_extstat "1"
	not_first_run_extstat:

	Push $R0
	InstallOptions::dialog "$PLUGINSDIR\ExtStat.ini"
	Pop $R0
	Pop $R0
FunctionEnd

Function ValidateEXTSTATAddon	
FunctionEnd

Function ApplyEXTSTATAddon
	ReadINIStr $1 "$PLUGINSDIR\ExtStat.ini" "Field 1" "State"
	WriteRegStr HKCU "${REGPATH}" "extstatDisabled" "$1"
	StrCmp "$1" "1" if_not_install
		SetDetailsPrint none
		CopyFiles /SILENT "$TEMP\AlertServer\AlertServerUnzip\MODULES\EXTENDED STATISTICS\*" "$TEMP\AlertServer\AlertServerUnzip\STANDARD\"
		${ReplaceInASPConfig} "$TEMP\${MAIN_DOTNET_conf}" "<!--MODULE_STATISTICS-->"  "<add key='STATISTICS' value='1' />"
		SetDetailsPrint both
	if_not_install:
FunctionEnd

;-------------------------------------------------------------------------------- INIT -----------
Function .onInit
	;Extract InstallOptions files
	;$PLUGINSDIR will automatically be removed when the installer closes
	InitPluginsDir
	StrCpy $NeedToReloadIIS 0
	SetDetailsPrint none
	File /oname=$PLUGINSDIR\check_for_requirements.ini "include\check_for_requirements.ini"
	File /oname=$PLUGINSDIR\iis_set.ini "include\iis_set.ini"
	File /oname=$PLUGINSDIR\db.ini "include\db.ini"
	File /oname=$PLUGINSDIR\AD_addon.ini "include\AD_addon.ini"
	File /oname=$PLUGINSDIR\BlOG_addon.ini "include\BLOG_addon.ini"
	File /oname=$PLUGINSDIR\FULLSCREEN_addon.ini "include\FULLSCREEN_addon.ini"
	File /oname=$PLUGINSDIR\VIDEO_addon.ini "include\VIDEO_addon.ini"
	File /oname=$PLUGINSDIR\RSS_addon.ini "include\RSS_addon.ini"
	File /oname=$PLUGINSDIR\SS_addon.ini "include\SS_addon.ini"
	File /oname=$PLUGINSDIR\TICKER_addon.ini "include\TICKER_addon.ini"
	File /oname=$PLUGINSDIR\TICKERPRO_addon.ini "include\TICKERPRO_addon.ini"
	File /oname=$PLUGINSDIR\IM_addon.ini "include\IM_addon.ini"
	File /oname=$PLUGINSDIR\WIDGETSTAT_addon.ini "include\WIDGETSTAT_addon.ini"
	File /oname=$PLUGINSDIR\TWITTER_addon.ini "include\TWITTER_addon.ini"
	File /oname=$PLUGINSDIR\LINKEDIN_addon.ini "include\LINKEDIN_addon.ini"
	File /oname=$PLUGINSDIR\WP_addon.ini "include\WP_addon.ini"
	File /oname=$PLUGINSDIR\SMS_addon.ini "include\SMS_addon.ini"
	File /oname=$PLUGINSDIR\EMAIL_addon.ini "include\EMAIL_addon.ini"
	File /oname=$PLUGINSDIR\ENCRYPT_addon.ini "include\ENCRYPT_addon.ini"
	File /oname=$PLUGINSDIR\SURVEYS_addon.ini "include\SURVEYS_addon.ini"
	File /oname=$PLUGINSDIR\TEXTCALL_addon.ini "include\TEXTCALL_addon.ini"
	File /oname=$PLUGINSDIR\MOBILE_addon.ini "include\MOBILE_addon.ini"
	File /oname=$PLUGINSDIR\WEBPLUGIN_addon.ini "include\WEBPLUGIN_addon.ini"
	File /oname=$PLUGINSDIR\CAMPAIGN_addon.ini "include\CAMPAIGN_addon.ini"
	File /oname=$PLUGINSDIR\APPROVE_addon.ini "include\APPROVE_addon.ini"
	File /oname=$PLUGINSDIR\DIGSIGN_addon.ini "include\DIGSIGN_addon.ini"
	File /oname=$PLUGINSDIR\YAMMER_addon.ini "include\YAMMER_addon.ini"
	File /oname=$PLUGINSDIR\thanks.ini "include\thanks.ini"
	File /oname=$PLUGINSDIR\iis.vbs "include\iis.vbs"
	File /oname=$PLUGINSDIR\iis_asp.vbs "include\iis_asp.vbs"
	File /oname=$PLUGINSDIR\iis_limit.vbs "include\iis_limit.vbs"
	File /oname=$PLUGINSDIR\ExtStat.ini "include\ExtStat.ini"
	File /oname=$PLUGINSDIR\Trial.ini "include\Trial.ini"
	SetDetailsPrint both
	
	ClearErrors
	ReadRegStr $R0 HKLM \
	"SOFTWARE\Microsoft\Windows NT\CurrentVersion" CurrentVersion
	IfErrors 0 lbl_winnt
	; we are not NT
	ReadRegStr $R0 HKLM \
	"SOFTWARE\Microsoft\Windows\CurrentVersion" VersionNumber
	StrCpy $R1 $R0 1
	StrCmp $R1 '4' 0 lbl_error
	StrCpy $R1 $R0 3
	StrCmp $R1 '4.0' lbl_win32_95
	StrCmp $R1 '4.9' lbl_win32_ME lbl_win32_98
	lbl_win32_95:
		StrCpy $R0 '95'
		Goto lbl_done
	lbl_win32_98:
		StrCpy $R0 '98'
		Goto lbl_done
	lbl_win32_ME:
		StrCpy $R0 'ME'
		Goto lbl_done
	lbl_winnt:
	StrCpy $R1 $R0 1
	StrCmp $R1 '3' lbl_winnt_x
	StrCmp $R1 '4' lbl_winnt_x
	StrCpy $R1 $R0 3
	StrCmp $R1 '5.0' lbl_winnt_2000
	StrCmp $R1 '5.1' lbl_winnt_XP
	StrCmp $R1 '5.2' lbl_winnt_2003
	StrCmp $R1 '6.0' lbl_winnt_vista
	StrCmp $R1 '7.0' lbl_winnt_seven lbl_error
	lbl_winnt_x:
		StrCpy $R0 "NT $R0" 6
	Goto lbl_done
	lbl_winnt_2000:
		Strcpy $R0 '2000'
		Goto lbl_done
	lbl_winnt_XP:
		Strcpy $R0 'XP'
		Goto lbl_done
	lbl_winnt_2003:
		Strcpy $R0 '2003'
		Goto lbl_done
	lbl_winnt_vista:
		Strcpy $R0 'Vista'
		Goto lbl_done
	lbl_winnt_seven:
		Strcpy $R0 'Seven'
		Goto lbl_done
	lbl_error:
		Strcpy $R0 ''
	lbl_done:
	StrCpy "$WindowsVersionText" "$R0 ($R1)"
	StrCpy "$WindowsVersion" "$R1"

	ClearErrors
	ReadRegDWORD $R0 HKLM "SOFTWARE\Microsoft\INetStp" "MajorVersion"
	ReadRegDWORD $R1 HKLM "SOFTWARE\Microsoft\INetStp" "MinorVersion"
	IfErrors 0 lbl_iis_nerr
		StrCpy "$IISVersion" "NoN"
	Goto lbl_iis_fin
	lbl_iis_nerr:
		StrCpy "$IISVersion" "$R0.$R1"
	lbl_iis_fin:


	StrCpy "$MSSQLVersion" "NoN"
	ReadRegStr $R1 HKLM "SOFTWARE\Microsoft\Microsoft SQL Server\120\Tools\ClientSetup\CurrentVersion" "CurrentVersion"
	IfErrors 0 sql_found
	ClearErrors
	ReadRegStr $R1 HKLM "SOFTWARE\Microsoft\Microsoft SQL Server\110\Tools\ClientSetup\CurrentVersion" "CurrentVersion"
	IfErrors 0 sql_found
	ClearErrors
	ReadRegStr $R1 HKLM "SOFTWARE\Microsoft\Microsoft SQL Server\100\Tools\ClientSetup\CurrentVersion" "CurrentVersion"
	IfErrors 0 sql_found
	ClearErrors
	ReadRegStr $R1 HKLM "SOFTWARE\Microsoft\Microsoft SQL Server\90\Tools\ClientSetup\CurrentVersion" "CurrentVersion"
	IfErrors 0 sql_found
	ClearErrors
	ReadRegStr $R1 HKLM "SOFTWARE\Microsoft\Microsoft SQL Server\80\Tools\ClientSetup\CurrentVersion" "CurrentVersion"
	IfErrors sql_not_found sql_found
	sql_found:
	StrCpy "$MSSQLVersion" "$R1"
	sql_not_found:


  ;--------------------------------------------------------------------------------------

	WriteINIStr "$PLUGINSDIR\check_for_requirements.ini" "Field 1" "Text" "$WindowsVersionText"
	WriteINIStr "$PLUGINSDIR\check_for_requirements.ini" "Field 2" "Text" "$IISVersion"
	WriteINIStr "$PLUGINSDIR\check_for_requirements.ini" "Field 3" "Text" "$MSSQLVersion"


	StrCpy "$RequirementsOk" "Ok"

	${StrTok} $R0 "$WindowsVersion" "." 0 1

	IntCmp $R0 5 is5Win lessthan5Win morethan5Win
	is5Win:
		StrCpy "$WindowsVersionOk" "Ok"
		Goto doneWin
	lessthan5Win:
		StrCpy "$WindowsVersionOk" "Fail"
		StrCpy "$RequirementsOk" "Fail"
	Goto doneWin
	morethan5Win:
		StrCpy "$WindowsVersionOk" "Ok"
		Goto doneWin
	doneWin:

	${StrTok} $R0 "$IISVersion" "." 0 1
	${StrTok} $R1 "$IISVersion" "." 1 1

	IntCmp $R0 5 is5_IIS_1 lessthan5_IIS_1 morethan5_IIS_1
	is5_IIS_1:
		IntCmp $R1 1 is1_IIS_2 lessthan1_IIS_2 morethan1_IIS_2
		is1_IIS_2:
			StrCpy "$IISVersionOk" "Ok"
			Goto done_IIS_2
		lessthan1_IIS_2:
			StrCpy "$IISVersionOk" "Fail"
			StrCpy "$RequirementsOk" "Fail"
			Goto done_IIS_2
		morethan1_IIS_2:
			StrCpy "$IISVersionOk" "Ok"
			Goto done_IIS_2
		done_IIS_2:
		Goto done_IIS_1
	lessthan5_IIS_1:
		StrCpy "$IISVersionOk" "Fail"
		StrCpy "$RequirementsOk" "Fail"
		Goto done_IIS_1
	morethan5_IIS_1:
		StrCpy "$IISVersionOk" "Ok"
		StrCpy "$RequirementsOk" "Ok"
		Goto done_IIS_1
	done_IIS_1:

	${StrTok} $R0 "$MSSQLVersion" "." 0 1

	IntCmp $R0 8 is8_MSSQL lessthan8_MSSQL morethan8_MSSQL
	is8_MSSQL:
		StrCpy "$MSSQLVersionOk" "Ok"
		WriteINIStr "$PLUGINSDIR\check_for_requirements.ini" "Field 7" "State" "1"
		Goto done8_MSSQL
	lessthan8_MSSQL:
		StrCpy "$MSSQLVersionOk" "N/A"
		WriteINIStr "$PLUGINSDIR\check_for_requirements.ini" "Field 10" "State" "1"
		WriteINIStr "$PLUGINSDIR\check_for_requirements.ini" "Field 14" "Text" "You cannot install DeskAlerts Database on this computer, but you can use MSSQL on a remote machine."
		StrCpy $isLocalDB "false"
		Goto done8_MSSQL
	morethan8_MSSQL:
		StrCpy "$MSSQLVersionOk" "Ok"
		WriteINIStr "$PLUGINSDIR\check_for_requirements.ini" "Field 12" "State" "1"
		Goto done8_MSSQL
	done8_MSSQL:

	WriteINIStr "$PLUGINSDIR\check_for_requirements.ini" "Field 4" "Text" "$WindowsVersionOk"
	WriteINIStr "$PLUGINSDIR\check_for_requirements.ini" "Field 5" "Text" "$IISVersionOk"
	WriteINIStr "$PLUGINSDIR\check_for_requirements.ini" "Field 7" "Text" "$MSSQLVersionOk"
	
	StrCmp "$WindowsVersionOk" "Ok" if_equal_IIS if_not_equal
if_equal_IIS:
	StrCmp "$IISVersionOk" "Ok" if_equal_WinVer_IIS if_not_equal
if_equal_WinVer_IIS:	
	#WriteINIStr "$PLUGINSDIR\check_for_requirements.ini" "Field 8" "Flags" "DISABLED"
	WriteINIStr "$PLUGINSDIR\check_for_requirements.ini" "Field 8" "Left" "0"
	WriteINIStr "$PLUGINSDIR\check_for_requirements.ini" "Field 8" "Right" "0"
	WriteINIStr "$PLUGINSDIR\check_for_requirements.ini" "Field 6" "Left" "0"
	WriteINIStr "$PLUGINSDIR\check_for_requirements.ini" "Field 6" "Right" "0"
	Goto LastPage_label
if_not_equal:	
	WriteINIStr "$PLUGINSDIR\check_for_requirements.ini" "Field 6" "Text" "$RequirementsOk"
  
LastPage_label:
	!ifdef lastPage
		WriteINIStr ${lastPage} "Settings" "NextButtonText" "Install"
	!endif

	${ReadFromOurASPConfig} "alerts_timeout" $alerts_timeout
	StrCmp $alerts_timeout "" 0 +2
		StrCpy $alerts_timeout "-1"
FunctionEnd

;-------------------------------------------------------------------------------- EMAIL    -----------
Var "smtpServer"
Var "smtpSSL"
Var "smtpPort"
Var "smtpAuth"
Var "smtpUsername"
Var "smtpPassword"
Var "smtpFrom"
Function SetEMAILAddon
	
	!insertmacro MUI_HEADER_TEXT " E-Mail module" "Configure E-Mail module settings"	

	StrCmp "$notfirstrun_email" "1" not_first_run_email
		Push $0
		${ReadFromASPorRegistryAndWrite} "$PLUGINSDIR\EMAIL_addon.ini" "Field 1" "smtpServer" "smtpServer" $0
							 StrCmp $0 "" 0 +2
								StrCpy $0 "localhost"
							 WriteINIStr "$PLUGINSDIR\EMAIL_addon.ini" "Field 17" "State" "noreply@$0"
		${ReadFromASPorRegistryAndWrite} "$PLUGINSDIR\EMAIL_addon.ini" "Field 17" "smtpFrom" "smtpFrom" $0
		${ReadFromASPorRegistryAndWrite} "$PLUGINSDIR\EMAIL_addon.ini" "Field 2" "smtpPort" "smtpPort" $0
		${ReadFromASPorRegistryAndWrite} "$PLUGINSDIR\EMAIL_addon.ini" "Field 3" "smtpSSL" "smtpSSL" $0

		${ReadFromASPorRegistry} "smtpAuth" "smtpAuth" $0

		${SetRadiobox} "$PLUGINSDIR\EMAIL_addon.ini" "Field 4" "$0" "0" "0"
		${SetRadiobox} "$PLUGINSDIR\EMAIL_addon.ini" "Field 5" "$0" "0" "1"
		${SetRadiobox} "$PLUGINSDIR\EMAIL_addon.ini" "Field 6" "$0" "0" "2"

		StrCmp $0 "" +4
		StrCmp $0 "0" +3
		WriteINIStr "$PLUGINSDIR\EMAIL_addon.ini" "Field 7" "Flags" ""
		WriteINIStr "$PLUGINSDIR\EMAIL_addon.ini" "Field 8" "Flags" ""
		
		${ReadFromASPorRegistryAndWrite} "$PLUGINSDIR\EMAIL_addon.ini" "Field 7" "smtpUsername" "smtpUsername" $0
		${ReadFromASPorRegistryAndWrite} "$PLUGINSDIR\EMAIL_addon.ini" "Field 8" "smtpPassword" "smtpPassword" $0

		${CheckByExistsFileOrRegistryAndWrite} "$PLUGINSDIR\EMAIL_addon.ini" "Field 9" "$alerts_folder\admin\m93sndf8a3z723h85.key" "emailDisabled" $0

		StrCpy $notfirstrun_email "1"
		Pop $0
	not_first_run_email:

	Push $R0
	InstallOptions::dialog "$PLUGINSDIR\EMAIL_addon.ini"
	Pop $R0
	Pop $R0
FunctionEnd

Function ValidateEMAILAddon
	ReadINIStr $0 "$PLUGINSDIR\EMAIL_addon.ini" "Settings" "State"
	IntCmp $0 0 if_next ;if $0 = 0 was clicked next button otherwise it was notify
		StrCmp $0 "3" 0 if_not_ssl
			ReadINIStr $1 "$PLUGINSDIR\EMAIL_addon.ini" "Field 3" "State"
			ReadINIStr $2 "$PLUGINSDIR\EMAIL_addon.ini" "Field 2" "HWND"
			StrCmp $1 "0" 0 +2
				SendMessage $2 ${WM_SETTEXT} 0 "STR:25"
			StrCmp $1 "1" 0 +2
				SendMessage $2 ${WM_SETTEXT} 0 "STR:465"
			Abort
		if_not_ssl:

		ReadINIStr $1 "$PLUGINSDIR\EMAIL_addon.ini" "Field 4" "State"

		ReadINIStr $2 "$PLUGINSDIR\EMAIL_addon.ini" "Field 7" "HWND"
		ReadINIStr $3 "$PLUGINSDIR\EMAIL_addon.ini" "Field 8" "HWND"

		IntOp $4 $1 !
		EnableWindow $2 $4
		EnableWindow $3 $4

		StrCpy $5 ""
		IntCmp $1 0 +2 0 0
		StrCpy $5 "DISABLED"

		WriteINIStr "$PLUGINSDIR\EMAIL_addon.ini" "Field 7" "Flags" "$5"
		WriteINIStr "$PLUGINSDIR\EMAIL_addon.ini" "Field 8" "Flags" "$5"

		Abort
	if_next:

	ReadINIStr $smtpServer     "$PLUGINSDIR\EMAIL_addon.ini" "Field 1" "State"
	ReadINIStr $smtpPort       "$PLUGINSDIR\EMAIL_addon.ini" "Field 2" "State"
	ReadINIStr $smtpSSL        "$PLUGINSDIR\EMAIL_addon.ini" "Field 3" "State"
	ReadINIStr $smtpFrom       "$PLUGINSDIR\EMAIL_addon.ini" "Field 17" "State"

	ReadINIStr $0     "$PLUGINSDIR\EMAIL_addon.ini" "Field 4" "State"
	StrCmp $0 "1" 0 +2
	StrCpy $smtpAuth "0"

	ReadINIStr $0     "$PLUGINSDIR\EMAIL_addon.ini" "Field 5" "State"
	StrCmp $0 "1" 0 +2
	StrCpy $smtpAuth "1"

	ReadINIStr $0     "$PLUGINSDIR\EMAIL_addon.ini" "Field 6" "State"
	StrCmp $0 "1" 0 +2
	StrCpy $smtpAuth "2"

	ReadINIStr $smtpUsername   "$PLUGINSDIR\EMAIL_addon.ini" "Field 7" "State"
	ReadINIStr $smtpPassword   "$PLUGINSDIR\EMAIL_addon.ini" "Field 8" "State"	
FunctionEnd

Function ApplyEMAILAddon
	ReadINIStr $1 "$PLUGINSDIR\EMAIL_addon.ini" "Field 9" "State"
	NsisCrypt::Base64Encode $smtpPassword
	Pop $1
	STRCPY $encrypted_pass $1
	
	
	WriteRegStr HKCU "${REGPATH}" "emailDisabled" "$1"
	WriteRegStr HKCU "${REGPATH}" "smtpServer"    "$smtpServer"
	WriteRegStr HKCU "${REGPATH}" "smtpPort"      "$smtpPort"
	WriteRegStr HKCU "${REGPATH}" "smtpSSL"       "$smtpSSL"
	WriteRegStr HKCU "${REGPATH}" "smtpAuth"      "$smtpAuth"
	WriteRegStr HKCU "${REGPATH}" "smtpUsername"  "$smtpUsername"
	WriteRegStr HKCU "${REGPATH}" "smtpPassword"  "$encrypted_pass"
	WriteRegStr HKCU "${REGPATH}" "smtpFrom"      "$smtpFrom"

	StrCmp "$1" "1" if_not_install
		SetDetailsPrint none
		${ReplaceInASPConfig} "$TEMP\${EMAIL_MODULE_conf}" "%smtpServer%"     "$smtpServer"
		${ReplaceInASPConfig} "$TEMP\${EMAIL_MODULE_conf}" "%smtpPort%"       "$smtpPort"
		${ReplaceInASPConfig} "$TEMP\${EMAIL_MODULE_conf}" "%smtpSSL%"        "$smtpSSL"
		${ReplaceInASPConfig} "$TEMP\${EMAIL_MODULE_conf}" "%smtpAuth%"       "$smtpAuth"
		${ReplaceInASPConfig} "$TEMP\${EMAIL_MODULE_conf}" "%smtpUsername%"   "$smtpUsername"
		${ReplaceInASPConfig} "$TEMP\${EMAIL_MODULE_conf}" "%smtpPassword%"   "$encrypted_pass"
		${ReplaceInASPConfig} "$TEMP\${EMAIL_MODULE_conf}" "%smtpFrom%"       "$smtpFrom"
		${FileJoin} "$TEMP\${MAIN_conf}" "$TEMP\${EMAIL_MODULE_conf}" "$TEMP\${MAIN_conf}"
		${ReplaceInASPConfig} "$TEMP\${MAIN_DOTNET_conf}" "<!--MODULE_EMAIL-->"  "<add key='EMAIL' value='1' />"
		CopyFiles /SILENT "$TEMP\AlertServer\AlertServerUnzip\MODULES\EMAIL MODULE\*" "$TEMP\AlertServer\AlertServerUnzip\STANDARD\"
		SetDetailsPrint both
	if_not_install:	
FunctionEnd

;-------------------------------------------------------------------------------- Screensaver -----------
Function SetSCREENSAVERAddon
	
	!insertmacro MUI_HEADER_TEXT " Corporate Screensaver module" "Configure Screensaver module settings"	

	StrCmp "$notfirstrun_ss" "1" not_first_run_ss
		${CheckByExistsFileOrRegistryAndWrite} "$PLUGINSDIR\SS_addon.ini" "Field 1" "$alerts_folder\admin\d9q7tdkjxcv0zm30fd.key" "ssDisabled" $0

		StrCpy $notfirstrun_ss "1"
	not_first_run_ss:

	Push $R0
	InstallOptions::dialog "$PLUGINSDIR\SS_addon.ini"
	Pop $R0
	Pop $R0
FunctionEnd

Function ValidateSCREENSAVERAddon
FunctionEnd

Function ApplySCREENSAVERAddon
	ReadINIStr $1 "$PLUGINSDIR\SS_addon.ini" "Field 1" "State"
	WriteRegStr HKCU "${REGPATH}" "ssDisabled" "$1"
	StrCmp "$1" "1" if_not_install
		SetDetailsPrint none
		${FileJoin} "$TEMP\${MAIN_conf}" "$TEMP\${SS_MODULE_conf}" "$TEMP\${MAIN_conf}"
		${ReplaceInASPConfig} "$TEMP\${MAIN_DOTNET_conf}" "<!--MODULE_SCREENSAVER-->"  "<add key='SCREENSAVER' value='1' />"
		CopyFiles /SILENT "$TEMP\AlertServer\AlertServerUnzip\MODULES\SCREENSAVER\*" "$TEMP\AlertServer\AlertServerUnzip\STANDARD\"
		SetDetailsPrint both
	if_not_install:
FunctionEnd

;-------------------------------------------------------------------------------- Rss -----------
Function SetRSSAddon
	
	!insertmacro MUI_HEADER_TEXT " News feed module" "Configure news feed module settings"	

	StrCmp "$notfirstrun_rss" "1" not_first_run_rss
		${CheckByExistsFileOrRegistryAndWrite} "$PLUGINSDIR\RSS_addon.ini" "Field 1" "$alerts_folder\admin\sfj10g4kg6aslhpbc7x.key" "rssDisabled" $0

		StrCpy $notfirstrun_rss "1"
	not_first_run_rss:

	Push $R0
	InstallOptions::dialog "$PLUGINSDIR\RSS_addon.ini"
	Pop $R0
	Pop $R0
FunctionEnd

Function ValidateRSSAddon
FunctionEnd

Function ApplyRSSAddon
	ReadINIStr $1 "$PLUGINSDIR\RSS_addon.ini" "Field 1" "State"
	WriteRegStr HKCU "${REGPATH}" "rssDisabled" "$1"
	StrCmp "$1" "1" if_not_install
		SetDetailsPrint none
		${FileJoin} "$TEMP\${MAIN_conf}" "$TEMP\${RSS_MODULE_conf}" "$TEMP\${MAIN_conf}"
		${ReplaceInASPConfig} "$TEMP\${MAIN_DOTNET_conf}" "<!--MODULE_RSS-->"  "<add key='RSS' value='1' />"
		CopyFiles /SILENT "$TEMP\AlertServer\AlertServerUnzip\MODULES\RSS\*" "$TEMP\AlertServer\AlertServerUnzip\STANDARD\"
		SetDetailsPrint both
	if_not_install:
FunctionEnd

;-------------------------------------------------------------------------------- Wallpaper -----------
Function SetWALLPAPERAddon
	
	!insertmacro MUI_HEADER_TEXT " Corporate Wallpaper module" "Configure Wallpaper module settings"	

	StrCmp "$notfirstrun_wallpaper" "1" not_first_run_wp
		${CheckByExistsFileOrRegistryAndWrite} "$PLUGINSDIR\WP_addon.ini" "Field 1" "$alerts_folder\admin\19gjqqcbuhfkn30djfm4.key" "wpDisabled" $0

		StrCpy $notfirstrun_wallpaper "1"
	not_first_run_wp:

	Push $R0
	InstallOptions::dialog "$PLUGINSDIR\WP_addon.ini"
	Pop $R0
	Pop $R0
FunctionEnd

Function ValidateWALLPAPERAddon
FunctionEnd

Function ApplyWALLPAPERAddon
	ReadINIStr $1 "$PLUGINSDIR\WP_addon.ini" "Field 1" "State"
	WriteRegStr HKCU "${REGPATH}" "wpDisabled" "$1"
	StrCmp "$1" "1" if_not_install
		SetDetailsPrint none
		${FileJoin} "$TEMP\${MAIN_conf}" "$TEMP\${WP_MODULE_conf}" "$TEMP\${MAIN_conf}"
		${ReplaceInASPConfig} "$TEMP\${MAIN_DOTNET_conf}" "<!--MODULE_WALLPAPER-->"  "<add key='WALLPAPER' value='1' />"
		CopyFiles /SILENT "$TEMP\AlertServer\AlertServerUnzip\MODULES\WALLPAPER\*" "$TEMP\AlertServer\AlertServerUnzip\STANDARD\"
		SetDetailsPrint both
	if_not_install:
FunctionEnd

;-------------------------------------------------------------------------------- Blog -----------
Function SetBLOGAddon
	
	!insertmacro MUI_HEADER_TEXT " Blog module for WordPress" "Configure Blog module settings"	

	StrCmp "$notfirstrun_blog" "1" not_first_run_blg
		${CheckByExistsFileOrRegistryAndWrite} "$PLUGINSDIR\BLOG_addon.ini" "Field 1" "$alerts_folder\admin\slk13jkds649hba8d2.key" "blgDisabled" $0

		StrCpy $notfirstrun_blog "1"
	not_first_run_blg:

	Push $R0
	InstallOptions::dialog "$PLUGINSDIR\BLOG_addon.ini"
	Pop $R0
	Pop $R0
FunctionEnd

Function ValidateBLOGAddon
FunctionEnd

Function ApplyBLOGAddon
	ReadINIStr $1 "$PLUGINSDIR\BLOG_addon.ini" "Field 1" "State"
	WriteRegStr HKCU "${REGPATH}" "blgDisabled" "$1"
	StrCmp "$1" "1" if_not_install
		SetDetailsPrint none
		${FileJoin} "$TEMP\${MAIN_conf}" "$TEMP\${BLG_MODULE_conf}" "$TEMP\${MAIN_conf}"
		${ReplaceInASPConfig} "$TEMP\${MAIN_DOTNET_conf}" "<!--MODULE_BLOG-->"  "<add key='BLOG' value='1' />"
		CopyFiles /SILENT "$TEMP\AlertServer\AlertServerUnzip\MODULES\BLOG\*" "$TEMP\AlertServer\AlertServerUnzip\STANDARD\"
		SetDetailsPrint both
	if_not_install:
FunctionEnd

;-------------------------------------------------------------------------------- Twitter -----------
Function SetTWITTERAddon
	
	!insertmacro MUI_HEADER_TEXT " Social module for Twitter" "Configure Social module settings"	

	StrCmp "$notfirstrun_twitter" "1" not_first_run_twitter
		${CheckByExistsFileOrRegistryAndWrite} "$PLUGINSDIR\TWITTER_addon.ini" "Field 1" "$alerts_folder\admin\q9d8dfk2cvns7ejm3.key" "twtrDisabled" $0

		StrCpy $notfirstrun_twitter "1"
	not_first_run_twitter:

	Push $R0
	InstallOptions::dialog "$PLUGINSDIR\TWITTER_addon.ini"
	Pop $R0
	Pop $R0
FunctionEnd

Function ValidateTWITTERAddon
FunctionEnd

Function ApplyTWITTERAddon
	ReadINIStr $1 "$PLUGINSDIR\TWITTER_addon.ini" "Field 1" "State"
	WriteRegStr HKCU "${REGPATH}" "twtrDisabled" "$1"
	StrCmp "$1" "1" if_not_install
		SetDetailsPrint none
		${FileJoin} "$TEMP\${MAIN_conf}" "$TEMP\${TWTR_MODULE_conf}" "$TEMP\${MAIN_conf}"
		${ReplaceInASPConfig} "$TEMP\${MAIN_DOTNET_conf}" "<!--MODULE_TWITTER-->"  "<add key='TWITTER' value='1' />"
		CopyFiles /SILENT "$TEMP\AlertServer\AlertServerUnzip\MODULES\SOCIAL_MEDIA\*" "$TEMP\AlertServer\AlertServerUnzip\STANDARD\"
		SetDetailsPrint both
	if_not_install:
FunctionEnd

;-------------------------------------------------------------------------------- LinkedIn -----------
Function SetLINKEDINAddon
	
	!insertmacro MUI_HEADER_TEXT " Social module for LinkedIn" "Configure Social module settings"	

	StrCmp "$notfirstrun_linkedin" "1" not_first_run_linkedin
		${CheckByExistsFileOrRegistryAndWrite} "$PLUGINSDIR\LINKEDIN_addon.ini" "Field 1" "$alerts_folder\admin\qvmxo0r8cv13xnw8odzv.key" "lnkdnDisabled" $0

		StrCpy $notfirstrun_linkedin "1"
	not_first_run_linkedin:

	Push $R0
	InstallOptions::dialog "$PLUGINSDIR\LINKEDIN_addon.ini"
	Pop $R0
	Pop $R0
FunctionEnd

Function ValidateLINKEDINAddon
FunctionEnd

Function ApplyLINKEDINAddon
	ReadINIStr $1 "$PLUGINSDIR\LINKEDIN_addon.ini" "Field 1" "State"
	WriteRegStr HKCU "${REGPATH}" "lnkdnDisabled" "$1"
	StrCmp "$1" "1" if_not_install
		SetDetailsPrint none
		${FileJoin} "$TEMP\${MAIN_conf}" "$TEMP\${LNKDN_MODULE_conf}" "$TEMP\${MAIN_conf}"
		${ReplaceInASPConfig} "$TEMP\${MAIN_DOTNET_conf}" "<!--MODULE_LINKEDIN-->"  "<add key='LINKEDIN' value='1' />"
		CopyFiles /SILENT "$TEMP\AlertServer\AlertServerUnzip\MODULES\SOCIAL_LINKEDIN\*" "$TEMP\AlertServer\AlertServerUnzip\STANDARD\"
		SetDetailsPrint both
	if_not_install:
FunctionEnd

;-------------------------------------------------------------------------------- Fullscreen -----------
Function SetFULLSCREENAddon
	
	!insertmacro MUI_HEADER_TEXT " Fullscreen Alert module" "Configure Fullscreen module settings"	

	StrCmp "$notfirstrun_fullscreen" "1" not_first_run_fs
		${CheckByExistsFileOrRegistryAndWrite} "$PLUGINSDIR\FULLSCREEN_addon.ini" "Field 1" "$alerts_folder\admin\dpf93jg28ffiq.key" "fsaDisabled" $0

		StrCpy $notfirstrun_fullscreen "1"
	not_first_run_fs:

	Push $R0
	InstallOptions::dialog "$PLUGINSDIR\FULLSCREEN_addon.ini"
	Pop $R0
	Pop $R0
FunctionEnd

Function ValidateFULLSCREENAddon
FunctionEnd

Function ApplyFULLSCREENAddon
	ReadINIStr $1 "$PLUGINSDIR\FULLSCREEN_addon.ini" "Field 1" "State"
	WriteRegStr HKCU "${REGPATH}" "fsaDisabled" "$1"
	StrCmp "$1" "1" if_not_install
		SetDetailsPrint none
		${FileJoin} "$TEMP\${MAIN_conf}" "$TEMP\${FSA_MODULE_conf}" "$TEMP\${MAIN_conf}"
		${ReplaceInASPConfig} "$TEMP\${MAIN_DOTNET_conf}" "<!--MODULE_FULLSCREEN-->"  "<add key='FULLSCREEN' value='1' />"
		CopyFiles /SILENT "$TEMP\AlertServer\AlertServerUnzip\MODULES\FULLSCREEN\*" "$TEMP\AlertServer\AlertServerUnzip\STANDARD\"
		SetDetailsPrint both
	if_not_install:
FunctionEnd
;-------------------------------------------------------------------------------- Approve  -----------
Function SetAPPROVEAddon
	!insertmacro MUI_HEADER_TEXT " Approve panel module" "Configure Approve panel module settings"
	
	StrCmp "$notfirstrun_approve" "1" not_first_run_fs
		${CheckByExistsFileOrRegistryAndWrite} "$PLUGINSDIR\APPROVE_addon.ini" "Field 1" "$alerts_folder\admin\99w92al30pgrxww6hn8r.key" "fsaDisabled" $0
		StrCpy $notfirstrun_approve "1"
	not_first_run_fs:
	
	Push $R0
	InstallOptions::dialog "$PLUGINSDIR\APPROVE_addon.ini"
	Pop $R0
	Pop $R0
FunctionEnd

Function ValidateAPPROVEAddon
	
FunctionEnd

Function ApplyAPPROVEAddon
	ReadINIStr $1 "$PLUGINSDIR\APPROVE_addon.ini" "Field 1" "State"
	WriteRegStr HKCU "${REGPATH}" "fsaDisabled" "$1"
	StrCmp "$1" "1" if_not_install
		SetDetailsPrint none
		${FileJoin} "$TEMP\${MAIN_conf}" "$TEMP\${APPROVE_MODULE_conf}" "$TEMP\${MAIN_conf}"
				${ReplaceInASPConfig} "$TEMP\${MAIN_DOTNET_conf}" "<!--MODULE_APPROVE-->"  "<add key='APPROVE' value='1' />"
		CopyFiles /SILENT "$TEMP\AlertServer\AlertServerUnzip\MODULES\APPROVAL\*" "$TEMP\AlertServer\AlertServerUnzip\STANDARD\"
		SetDetailsPrint both
	if_not_install:	
FunctionEnd

;-------------------------------------------------------------------------------- Digital signage  -----------
Function SetDIGSIGNAddon
	!insertmacro MUI_HEADER_TEXT "Digital signage module" "Configure Digital signage module settings"
	
	StrCmp "$notfirstrun_digsign" "1" not_first_run_fs
		${CheckByExistsFileOrRegistryAndWrite} "$PLUGINSDIR\DIGSIGN_addon.ini" "Field 1" "$alerts_folder\admin\99w92al30pgrxww6hn8r.key" "fsaDisabled" $0
		StrCpy $notfirstrun_digsign "1"
	not_first_run_fs:
	
	Push $R0
	InstallOptions::dialog "$PLUGINSDIR\DIGSIGN_addon.ini"
	Pop $R0
	Pop $R0
FunctionEnd

Function ValidateDIGSIGNAddon
	
FunctionEnd

Function ApplyDIGSIGNAddon
	ReadINIStr $1 "$PLUGINSDIR\DIGSIGN_addon.ini" "Field 1" "State"
	WriteRegStr HKCU "${REGPATH}" "fsaDisabled" "$1"
	StrCmp "$1" "1" if_not_install
		SetDetailsPrint none
		${FileJoin} "$TEMP\${MAIN_conf}" "$TEMP\${DIGSIGN_MODULE_conf}" "$TEMP\${MAIN_conf}"
		${ReplaceInASPConfig} "$TEMP\${MAIN_DOTNET_conf}" "<!--MODULE_DIGSIGN-->"  "<add key='DIGITAL_SIGNAGE' value='1' />"
		CopyFiles /SILENT "$TEMP\AlertServer\AlertServerUnzip\MODULES\DIGSIGN\*" "$TEMP\AlertServer\AlertServerUnzip\STANDARD\"
		SetDetailsPrint both
	if_not_install:	
FunctionEnd

;-------------------------------------------------------------------------------- Yammer  -----------
Function SetYAMMERAddon
	!insertmacro MUI_HEADER_TEXT "Social module for Yammer" "Configure Social module for Yammer settings"
	
	StrCmp "$notfirstrun_yammer" "1" not_first_run_yammer
		${CheckByExistsFileOrRegistryAndWrite} "$PLUGINSDIR\YAMMER_addon.ini" "Field 1" "$alerts_folder\admin\8v1jv27kkhs7v91ksa.key" "yammerDisabled" $0
		StrCpy $notfirstrun_yammer "1"
	not_first_run_yammer:
	
	Push $R0
	InstallOptions::dialog "$PLUGINSDIR\YAMMER_addon.ini"
	Pop $R0
	Pop $R0
FunctionEnd

Function ValidateYAMMERAddon
	
FunctionEnd

Function ApplyYAMMERAddon
	ReadINIStr $1 "$PLUGINSDIR\YAMMER_addon.ini" "Field 1" "State"
	WriteRegStr HKCU "${REGPATH}" "yammerDisabled" "$1"
	StrCmp "$1" "1" if_not_install
		SetDetailsPrint none
		${FileJoin} "$TEMP\${MAIN_conf}" "$TEMP\${YAMMER_MODULE_conf}" "$TEMP\${MAIN_conf}"
		CopyFiles /SILENT "$TEMP\AlertServer\AlertServerUnzip\MODULES\YAMMER\*" "$TEMP\AlertServer\AlertServerUnzip\STANDARD\"
		SetDetailsPrint both
	if_not_install:	
FunctionEnd


;-------------------------------------------------------------------------------- Campaign -----------
Function SetCAMPAIGNAddon
	
	!insertmacro MUI_HEADER_TEXT " Campaign module" "Configure Campaign module settings"	

	StrCmp "$notfirstrun_campaign" "1" not_first_run_fs
		${CheckByExistsFileOrRegistryAndWrite} "$PLUGINSDIR\CAMPAIGN_addon.ini" "Field 1" "$alerts_folder\admin\99w92al30pgrxww6hn8r.key" "fsaDisabled" $0

		StrCpy $notfirstrun_campaign "1"
	not_first_run_fs:

	Push $R0
	InstallOptions::dialog "$PLUGINSDIR\CAMPAIGN_addon.ini"
	Pop $R0
	Pop $R0
FunctionEnd

Function ValidateCAMPAIGNAddon
FunctionEnd

Function ApplyCAMPAIGNAddon
	ReadINIStr $1 "$PLUGINSDIR\CAMPAIGN_addon.ini" "Field 1" "State"
	WriteRegStr HKCU "${REGPATH}" "fsaDisabled" "$1"
	StrCmp "$1" "1" if_not_install
		SetDetailsPrint none
		${FileJoin} "$TEMP\${MAIN_conf}" "$TEMP\${CAMPAIGN_MODULE_conf}" "$TEMP\${MAIN_conf}"
		${ReplaceInASPConfig} "$TEMP\${MAIN_DOTNET_conf}" "<!--MODULE_CAMPAIGN-->"  "<add key='CAMPAIGN' value='1' />"
		CopyFiles /SILENT "$TEMP\AlertServer\AlertServerUnzip\MODULES\CAMPAIGN MODULE\*" "$TEMP\AlertServer\AlertServerUnzip\STANDARD\"
		SetDetailsPrint both
	if_not_install:
FunctionEnd
;-------------------------------------------------------------------------------- Video -----------
Function SetVIDEOAddon
	
	!insertmacro MUI_HEADER_TEXT " Videoalert module" "Configure Videoalert module settings"	

	StrCmp "$notfirstrun_video" "1" not_first_run_video
		${CheckByExistsFileOrRegistryAndWrite} "$PLUGINSDIR\VIDEO_addon.ini" "Field 1" "$alerts_folder\admin\d9q7tdkjxcv0df76fd.key" "videoDisabled" $0

		StrCpy $notfirstrun_video "1"
	not_first_run_video:

	Push $R0
	InstallOptions::dialog "$PLUGINSDIR\VIDEO_addon.ini"
	Pop $R0
	Pop $R0
FunctionEnd

Function ValidateVIDEOAddon
FunctionEnd

Function ApplyVIDEOAddon
	ReadINIStr $1 "$PLUGINSDIR\VIDEO_addon.ini" "Field 1" "State"
	WriteRegStr HKCU "${REGPATH}" "videoDisabled" "$1"
	StrCmp "$1" "1" if_not_install
		SetDetailsPrint none
		${FileJoin} "$TEMP\${MAIN_conf}" "$TEMP\${VIDEO_MODULE_conf}" "$TEMP\${MAIN_conf}"
		${ReplaceInASPConfig} "$TEMP\${MAIN_DOTNET_conf}" "<!--MODULE_VIDEO-->"  "<add key='VIDEO' value='1' />"
		CopyFiles /SILENT "$TEMP\AlertServer\AlertServerUnzip\MODULES\VIDEOALERT\*" "$TEMP\AlertServer\AlertServerUnzip\STANDARD\"
		SetDetailsPrint both
	if_not_install:
FunctionEnd
;-------------------------------------------------------------------------------- Web Plugin -----------
Function SetWEBPLUGINAddon
	
	!insertmacro MUI_HEADER_TEXT " WebPlugin module" "Configure WebPlugin module settings"	

	StrCmp "$notfirstrun_video" "1" not_first_run_webplugin
		${CheckByExistsFileOrRegistryAndWrite} "$PLUGINSDIR\WEBPLUGIN_addon.ini" "Field 1" "$alerts_folder\admin\d9q7tdkjxcv0df7de6fd.key" "webpluginDisabled" $0

		StrCpy $notfirstrun_webplugin "1"
	not_first_run_webplugin:

	Push $R0
	InstallOptions::dialog "$PLUGINSDIR\WEBPLUGIN_addon.ini"
	Pop $R0
	Pop $R0
FunctionEnd

Function ValidateWEBPLUGINAddon
FunctionEnd

Function ApplyWEBPLUGINAddon
	ReadINIStr $1 "$PLUGINSDIR\WEBPLUGIN_addon.ini" "Field 1" "State"
	WriteRegStr HKCU "${REGPATH}" "webpluginDisabled" "$1"
	StrCmp "$1" "1" if_not_install
		SetDetailsPrint none
		${FileJoin} "$TEMP\${MAIN_conf}" "$TEMP\${WEBPLUGIN_MODULE_conf}" "$TEMP\${MAIN_conf}"
		CopyFiles /SILENT "$TEMP\AlertServer\AlertServerUnzip\MODULES\WEBPLUGIN\*" "$TEMP\AlertServer\AlertServerUnzip\STANDARD\"
		SetDetailsPrint both
	if_not_install:
FunctionEnd

;-------------------------------------------------------------------------------- Ticker -----------
Function SetTICKERAddon
	
	!insertmacro MUI_HEADER_TEXT " Scrolling news ticker Alert module" "Configure news ticker module settings"	

	StrCmp "$notfirstrun_ticker" "1" not_first_run_tick
		${CheckByExistsFileOrRegistryAndWrite} "$PLUGINSDIR\TICKER_addon.ini" "Field 1" "$alerts_folder\admin\1gu3e30ccvnqigkc3z.key" "tickDisabled" $0

		StrCpy $notfirstrun_ticker "1"
	not_first_run_tick:

	Push $R0
	InstallOptions::dialog "$PLUGINSDIR\TICKER_addon.ini"
	Pop $R0
	Pop $R0
FunctionEnd

Function ValidateTICKERAddon
FunctionEnd

Function ApplyTICKERAddon
	ReadINIStr $1 "$PLUGINSDIR\TICKER_addon.ini" "Field 1" "State"
	WriteRegStr HKCU "${REGPATH}" "tickDisabled" "$1"
	StrCmp "$1" "1" if_not_install
		SetDetailsPrint none
		${FileJoin} "$TEMP\${MAIN_conf}" "$TEMP\${TICK_MODULE_conf}" "$TEMP\${MAIN_conf}"
				${ReplaceInASPConfig} "$TEMP\${MAIN_DOTNET_conf}" "<!--MODULE_TICKER-->"  "<add key='TICKER' value='1' />"
		CopyFiles /SILENT "$TEMP\AlertServer\AlertServerUnzip\MODULES\TICKER\*" "$TEMP\AlertServer\AlertServerUnzip\STANDARD\"
		SetDetailsPrint both
	if_not_install:
FunctionEnd

;-------------------------------------------------------------------------------- Ticker PRO -----------
Function SetTICKERPROAddon
	
	!insertmacro MUI_HEADER_TEXT " Scrolling news ticker Alert PRO module" "Configure news ticker PRO module settings"	

	StrCmp "$notfirstrun_tickerpro" "1" not_first_run_tickpro
		${CheckByExistsFileOrRegistryAndWrite} "$PLUGINSDIR\TICKERPRO_addon.ini" "Field 1" "$alerts_folder\admin\apqogc03mcj43xzy.key" "tickProDisabled" $0

		StrCpy $notfirstrun_tickerpro "1"
	not_first_run_tickpro:

	Push $R0
	InstallOptions::dialog "$PLUGINSDIR\TICKERPRO_addon.ini"
	Pop $R0
	Pop $R0
FunctionEnd

Function ValidateTICKERPROAddon
FunctionEnd

Function ApplyTICKERPROAddon
	ReadINIStr $1 "$PLUGINSDIR\TICKERPRO_addon.ini" "Field 1" "State"
	WriteRegStr HKCU "${REGPATH}" "tickProDisabled" "$1"
	StrCmp "$1" "1" if_not_install
		SetDetailsPrint none
		${FileJoin} "$TEMP\${MAIN_conf}" "$TEMP\${TICKPRO_MODULE_conf}" "$TEMP\${MAIN_conf}"
		${ReplaceInASPConfig} "$TEMP\${MAIN_DOTNET_conf}" "<!--MODULE_TICKERPRO-->"  "<add key='TICKER_PRO' value='1' />"
		CopyFiles /SILENT "$TEMP\AlertServer\AlertServerUnzip\MODULES\TICKER_PRO\*" "$TEMP\AlertServer\AlertServerUnzip\STANDARD\"
		SetDetailsPrint both
	if_not_install:
FunctionEnd

;-------------------------------------------------------------------------------- Instant Messages -----------
Function SetIMAddon
	
	!insertmacro MUI_HEADER_TEXT " Instant Messages module" "Configure instant messages module settings"	

	StrCmp "$notfirstrun_im" "1" not_first_run_im
		${CheckByExistsFileOrRegistryAndWrite} "$PLUGINSDIR\IM_addon.ini" "Field 1" "$alerts_folder\admin\dwpcv82mc7apzijq3avp.key" "imDisabled" $0

		StrCpy $notfirstrun_im "1"
	not_first_run_im:

	Push $R0
	InstallOptions::dialog "$PLUGINSDIR\IM_addon.ini"
	Pop $R0
	Pop $R0
FunctionEnd

Function ValidateIMAddon
FunctionEnd

Function ApplyIMAddon
	ReadINIStr $1 "$PLUGINSDIR\IM_addon.ini" "Field 1" "State"
	WriteRegStr HKCU "${REGPATH}" "imDisabled" "$1"
	StrCmp "$1" "1" if_not_install
		SetDetailsPrint none
		${FileJoin} "$TEMP\${MAIN_conf}" "$TEMP\${IM_MODULE_conf}" "$TEMP\${MAIN_conf}"
				${ReplaceInASPConfig} "$TEMP\${MAIN_DOTNET_conf}" "<!--MODULE_IM-->"  "<add key='EMERGENCY' value='1' />"
		CopyFiles /SILENT "$TEMP\AlertServer\AlertServerUnzip\MODULES\INSTANT_MESSAGES\*" "$TEMP\AlertServer\AlertServerUnzip\STANDARD\"
		SetDetailsPrint both
	if_not_install:
FunctionEnd

;-------------------------------------------------------------------------------- Widget statistic -----------
Function SetWIDGETSTATAddon
	
	!insertmacro MUI_HEADER_TEXT "Widget Statistic module" "Configure Widget Statistic module module settings"	

	StrCmp "$notfirstrun_im" "1" not_first_run_im
		${CheckByExistsFileOrRegistryAndWrite} "$PLUGINSDIR\WIDGETSTAT_addon.ini" "Field 1" "$alerts_folder\admin\lsde428sf3qfusgr5siu.key" "imDisabled" $0

		StrCpy $notfirstrun_im "1"
	not_first_run_im:

	Push $R0
	InstallOptions::dialog "$PLUGINSDIR\WIDGETSTAT_addon.ini"
	Pop $R0
	Pop $R0
FunctionEnd

Function ValidateWIDGETSTATAddon
FunctionEnd

Function ApplyWIDGETSTATAddon
	;ReadINIStr $1 "$PLUGINSDIR\WIDGETSTAT_addon.ini" "Field 1" "State"
	;WriteRegStr HKCU "${REGPATH}" "imDisabled" "0"
	SetDetailsPrint none
	${FileJoin} "$TEMP\${MAIN_conf}" "$TEMP\${WIDGETSTAT_MODULE_conf}" "$TEMP\${MAIN_conf}"
	CopyFiles /SILENT "$TEMP\AlertServer\AlertServerUnzip\MODULES\WIDGETSTAT\*" "$TEMP\AlertServer\AlertServerUnzip\STANDARD\"
	SetDetailsPrint both
FunctionEnd

;-------------------------------------------------------------------------------- MOBILE -----------
Function SetMOBILEAddon
	
	!insertmacro MUI_HEADER_TEXT " Mobile Alerts module" "Configure Mobile Alerts module settings"	

	StrCmp "$notfirstrun_mobile" "1" not_first_run_mobile
		${CheckByExistsFileOrRegistryAndWrite} "$PLUGINSDIR\MOBILE_addon.ini" "Field 1" "$alerts_folder\admin\lvppics03jcvkw38pa3.key" "mobileDisabled" $0

		StrCpy $notfirstrun_mobile "1"
	not_first_run_mobile:

	Push $R0
	InstallOptions::dialog "$PLUGINSDIR\MOBILE_addon.ini"
	Pop $R0
	Pop $R0
FunctionEnd

Function ValidateMOBILEAddon
FunctionEnd

Function ApplyMOBILEAddon
	ReadINIStr $1 "$PLUGINSDIR\MOBILE_addon.ini" "Field 1" "State"
	WriteRegStr HKCU "${REGPATH}" "mobileDisabled" "$1"
	StrCmp "$1" "1" if_not_install
		SetDetailsPrint none
		${FileJoin} "$TEMP\${MAIN_conf}" "$TEMP\${MOBILE_MODULE_conf}" "$TEMP\${MAIN_conf}"
				${ReplaceInASPConfig} "$TEMP\${MAIN_DOTNET_conf}" "<!--MODULE_MOBILE-->"  "<add key='MOBILE' value='1' />"
		CopyFiles /SILENT "$TEMP\AlertServer\AlertServerUnzip\MODULES\MOBILE\*" "$TEMP\AlertServer\AlertServerUnzip\STANDARD\"
		SetDetailsPrint both
	if_not_install:
FunctionEnd

;---------------------------------------------------------------------------------- ASP ---------------

Function CheckASP
	nsExec::ExecToStack /TIMEOUT=20000 '"$SYSDIR\cscript.exe" /nologo "$PLUGINSDIR\iis_asp.vbs"'
	Pop $0
	Pop $1
	${StrTrimNewLines} $1 $1
	StrCmp "$1" "disabled" 0 DoNothing
	MessageBox MB_ICONQUESTION|MB_YESNO "The ASP is currently disabled, and for DeskAlerts it needs to be enabled.$\r$\nDo you want to enable ASP?" IDCANCEL DoNothing
		Banner::show /NOUNLOAD /set 76 "Server installation" "Processing ... "
		Banner::getWindow /NOUNLOAD
		pop $0 ; get result
		nsExec::Exec 'cmd /c cd /d "%WinDir%\SysNative"&cmd.exe /c if exist "%WinDir%\System32\pkgmgr.exe" (start /w "" "%WinDir%\System32\pkgmgr.exe" /iu:IIS-WebServerRole;IIS-WebServer;IIS-CommonHttpFeatures;IIS-StaticContent;IIS-DefaultDocument;IIS-DirectoryBrowsing;IIS-HttpErrors;IIS-ApplicationDevelopment;IIS-ASP;IIS-ISAPIExtensions;IIS-HealthAndDiagnostics;IIS-HttpLogging;IIS-LoggingLibraries;IIS-RequestMonitor;IIS-Security;IIS-RequestFiltering;IIS-HttpCompressionStatic;IIS-WebServerManagementTools;IIS-ManagementConsole;WAS-WindowsActivationService;WAS-ProcessModel;WAS-NetFxEnvironment;WAS-ConfigurationAPI)'
		pop $R0
		nsExec::Exec /TIMEOUT=20000 '"$SYSDIR\cscript.exe" "$PLUGINSDIR\iis_asp.vbs" enable'
		pop $R0
		Banner::destroy
	DoNothing:
FunctionEnd

;-------------------------------------------------------------------------------- Uninstall -----------

Section -Post
	WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayName"      "${PRODUCT_NAME}"
	WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "UninstallString"  "${PRODUCT_UNINST_FILEDIR}\${PRODUCT_UNINST_FILENAME}.exe"
	WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayIcon"      "${PRODUCT_UNINST_FILEDIR}\${PRODUCT_UNINST_FILENAME}.exe"
	WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayVersion"   "${PRODUCT_VERSION}"
	WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "URLInfoAbout"     "${PRODUCT_WEB_SITE}"
	WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "URLUpdateInfo"    "${PRODUCT_WEB_SITE}"
	WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "HelpLink"         "${PRODUCT_WEB_SITE}"
	WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "Publisher"        "${PRODUCT_PUBLISHER}"
	WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "InstallLocation"  "$alerts_folder"

	DetailPrint "Creating uninstaller..."
	SetDetailsPrint none
	CreateDirectory "${PRODUCT_UNINST_FILEDIR}"
	SetFileAttributes "${PRODUCT_UNINST_FILEDIR}" ARCHIVE|READONLY|HIDDEN|SYSTEM
	WriteUninstaller "${PRODUCT_UNINST_FILEDIR}\${PRODUCT_UNINST_FILENAME}.exe"
	SetDetailsPrint both

	StrCmp $NeedToReloadIIS 0 DoNothing
		MessageBox MB_ICONINFORMATION|MB_OK "Please restart IIS to complete the install process."
	DoNothing:
SectionEnd

Function un.onInit
	ReadRegStr $alerts_folder ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "InstallLocation"
	
	StrCmp "$alerts_folder" "" ErrorChecking
		IfFileExists "$alerts_folder" EndChecking
		ErrorChecking:
			MessageBox MB_ICONEXCLAMATION|MB_OK "DeskAlerts already uninstalled."
			StrCpy $alerts_folder ""
			GoTo DoUninstall
	EndChecking:
	
	MessageBox MB_ICONQUESTION|MB_YESNO "Are you sure you want to delete DeskAlerts in path $alerts_folder?" IDYES DoUninstall
		Abort
	DoUninstall:
FunctionEnd

Section "Uninstall"
	StrCmp "$alerts_folder" "" AlreadyUninstalled
		${UnReadFromASPConfig} "${INSTALLED_conf}" "host"      $host
		${UnReadFromASPConfig} "${INSTALLED_conf}" "dbname"    $dbname
		${UnReadFromASPConfig} "${INSTALLED_conf}" "user_name" $user_name
		${UnReadFromASPConfig} "${INSTALLED_conf}" "password"  $password
		${UnReadFromASPConfig} "${INSTALLED_conf}" "win_auth"  $win_auth

		StrCmp "$win_auth" "0" not_win_auth
			StrCpy $user_name ""
			StrCpy $password ""
		not_win_auth:

		DetailPrint "Deleting files..."
		;SetDetailsPrint none
		RMDir /r /REBOOTOK $alerts_folder
		;SetDetailsPrint both

		; Delete DeskAlertsServer32.dll if it exsists
		FindFirst $0 $1 "$APPDATA\DeskAlertsServer\DeskAlertsServer32*.dll"
		files_loop32:
			StrCmp $1 "" files_done32
			nsExec::Exec 'regsvr32.exe /u /s "$APPDATA\DeskAlertsServer\$1"'
			pop $R0
			FindNext $0 $1
			Goto files_loop32
		files_done32:
		FindClose $0

		; Delete DeskAlertsServer64.dll if it exsists
		FindFirst $0 $1 "$APPDATA\DeskAlertsServer\DeskAlertsServer64*.dll"
		files_loop64:
			StrCmp $1 "" files_done64
			nsExec::Exec 'cmd /c cd /d "%WinDir%\SysNative"&cmd.exe /c start /w regsvr32.exe /u /s "$APPDATA\DeskAlertsServer\$1"'
			pop $R0
			FindNext $0 $1
			Goto files_loop64
		files_done64:
		FindClose $0

		RMDir /r /REBOOTOK "$APPDATA\DeskAlertsServer"

		StrCmp "$host" "" DoNothing
		StrCmp "$dbname" "" DoNothing
		MessageBox MB_ICONQUESTION|MB_YESNO "Do you want to drop database $dbname from $host?" IDNO DoNothing
			detailprint "Loggin on to SQL server $host"
			MSSQL_OLEDB::SQL_Logon /NOUNLOAD "$host" "$user_name" "$password"
			pop $0
			pop $1
			detailprint $1
		
			StrCmp "$0" "0" no_cn_err 0
			MSSQL_OLEDB::SQL_GetError /NOUNLOAD
			Pop $0
			Pop $0
			DetailPrint $0
			no_cn_err:
		
			DetailPrint "Removing data base"
			${UnStrReplace} $0 "]" "]]" "$dbname"
			MSSQL_OLEDB::SQL_Execute /NOUNLOAD "DROP DATABASE [$0]"
			Pop $0
			Pop $1
			DetailPrint $1
		
			StrCmp "$0" "0" no_db_err 0
			MSSQL_OLEDB::SQL_GetError /NOUNLOAD
			Pop $0
			Pop $0
			DetailPrint $0
			no_db_err:
		DoNothing:
	DetailPrint ""
	AlreadyUninstalled:

	DetailPrint "Clearing..."
	SetDetailsPrint none
	DeleteRegKey ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}"
	DeleteRegKey ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UPDATE_UNINST_KEY}"
	Delete /REBOOTOK "$INSTDIR\${PRODUCT_UNINST_FILENAME}.exe"
	Delete /REBOOTOK "$INSTDIR\${PRODUCT_UPDATE_UNINST_FILENAME}.exe"
	RMDir /r /REBOOTOK "${PRODUCT_UNINST_FILEDIR}"
	SetDetailsPrint both
SectionEnd

Function SetTrial
	!insertmacro MUI_HEADER_TEXT " Trial" "Enter trial key"

	Push $R0
	InstallOptions::dialog "$PLUGINSDIR\Trial.ini"
	Pop $R0
	Pop $R0
FunctionEnd

Function RegisterDotNet

Exch $R0
Push $R1

ReadRegStr $R1 HKEY_LOCAL_MACHINE \
"Software\Microsoft\.NETFramework" "InstallRoot"

IfFileExists $R1\v4.0.30319\regasm.exe FileExists
  MessageBox MB_ICONSTOP|MB_OK "Microsoft .NET Framework 4.0 was not detected!"
Abort

FileExists:

ExecWait '"$R1\v4.0.30319\regasm.exe" "$R0" /codebase'

Pop $R1
Pop $R0

FunctionEnd

Function ValidateTrial
	ReadINIStr $0 "$PLUGINSDIR\Trial.ini" "Field 2" "State"

	StrCmp $0 "" 0 next
		MessageBox MB_OK "Please enter your trial key."
		Abort
	next:

	${ValidTrialKey} $0 $0

	StrCmp $0 1 end
		MessageBox MB_OK "This trial key is not valid. Try to re-enter or get it again."
		Abort
	end:
FunctionEnd
