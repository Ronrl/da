// IISAnonimUserWork.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "IISAnonymUserWork.h"



#include <stddef.h>
#include <string>
#include <sstream>

#include <iostream>
#include <fstream>


#define INITGUID

namespace
{
    static std::wstring swzVirtDir;

    char* narrow( const std::wstring& str )
    {
        std::ostringstream stm ;
        const std::ctype<char>& ctfacet = std::use_facet< std::ctype<char> >( stm.getloc() ) ;
        
        for( size_t i=0 ; i < str.size() ; ++i )
            stm << ctfacet.narrow( str[i], 0 ) ;
        
        std::string strTemp = stm.str();
        char* c = new char [ strTemp.size()+1];
        strcpy(c, strTemp.c_str());
        return c;
    }

    std::wstring GetIISPath()
    {
        std::wstring root( L"LM/W3SVC/1/ROOT/");        
        root += swzVirtDir;

        return root;
    }    
}

// The one and only application object


int CIISAnonymUserWork::SetName( const std::wstring& newName)
{
    CComPtr <IMSAdminBase> pIMeta;
    HRESULT hRes = 0;
    METADATA_HANDLE handle;
    METADATA_RECORD record = {0};

    CoInitialize (NULL);

    hRes = CoCreateInstance( CLSID_MSAdminBase, NULL, CLSCTX_ALL, IID_IMSAdminBase, (void **) &pIMeta);

	if( FAILED( hRes))
		return static_cast<int>( hRes);

    std::wstring path = GetIISPath();
	//get a handle to the web service
    hRes = pIMeta->OpenKey( METADATA_MASTER_ROOT_HANDLE, path.c_str(),
							/*METADATA_PERMISSION_READ | */METADATA_PERMISSION_WRITE, 20, &handle);

    record.dwMDIdentifier =  MD_ANONYMOUS_USER_NAME;
	record.dwMDAttributes = METADATA_INHERIT;
	record.dwMDUserType = IIS_MD_UT_FILE;
	record.dwMDDataType = STRING_METADATA;
    record.pbMDData = (unsigned char *)newName.c_str();
	record.dwMDDataLen = ( wcslen( newName.c_str()) + 1) * sizeof( wchar_t);
	hRes = pIMeta->SetData( handle,NULL, &record);

	if( FAILED( hRes))
	{
		pIMeta->SaveData();
		return static_cast<int>( hRes);
	}

    pIMeta->CloseKey( handle);
	pIMeta->SaveData();

    return 0;
}

int CIISAnonymUserWork::SetPwd( const std::wstring& newPwd)
{
    CComPtr <IMSAdminBase> pIMeta;
    HRESULT hRes = 0;
    METADATA_HANDLE handle;
    METADATA_RECORD record = {0};

    CoInitialize (NULL);
    hRes = CoCreateInstance( CLSID_MSAdminBase, NULL, CLSCTX_ALL, IID_IMSAdminBase, (void **) &pIMeta);

	if( FAILED( hRes))
		return static_cast<int>( hRes);

    std::wstring path = GetIISPath();
	//get a handle to the web service
	hRes = pIMeta->OpenKey( METADATA_MASTER_ROOT_HANDLE, path.c_str(),
							/*METADATA_PERMISSION_READ |*/ METADATA_PERMISSION_WRITE, 20, &handle);

    record.dwMDIdentifier =  MD_ANONYMOUS_PWD;
	record.dwMDAttributes = METADATA_INHERIT;
	record.dwMDUserType = IIS_MD_UT_FILE;
	record.dwMDDataType = STRING_METADATA;
    record.pbMDData = (unsigned char *)newPwd.c_str();
	record.dwMDDataLen = ( wcslen( newPwd.c_str()) + 1) * sizeof( wchar_t);
	hRes = pIMeta->SetData( handle, NULL, &record);

	if( FAILED( hRes))
	{
		pIMeta->SaveData();
		return static_cast<int>( hRes);
	}

    pIMeta->CloseKey( handle);
	pIMeta->SaveData();

    return 0;
}

int CIISAnonymUserWork::GetName( std::wstring& name)
{
    CComPtr <IMSAdminBase> pIMeta;
    HRESULT hRes = 0;
    METADATA_HANDLE handle;
    METADATA_RECORD record = {0};

    CoInitialize (NULL);
    hRes = CoCreateInstance( CLSID_MSAdminBase, NULL, CLSCTX_ALL, IID_IMSAdminBase, (void **) &pIMeta);

	if( FAILED( hRes))
		return static_cast<int>( hRes);

    std::wstring path = GetIISPath();
	//get a handle to the web service
	hRes = pIMeta->OpenKey( METADATA_MASTER_ROOT_HANDLE, path.c_str() /*L"/LM/W3SVC/1/Root/DeskAlerts"*/,
							METADATA_PERMISSION_READ /*| METADATA_PERMISSION_WRITE*/, 20, &handle);

    // Need to query the length of returned AU name.
    record.dwMDIdentifier =  MD_ANONYMOUS_USER_NAME;
	record.dwMDAttributes = METADATA_INHERIT;
	record.dwMDUserType = IIS_MD_UT_FILE;
	record.dwMDDataType = STRING_METADATA;
    record.pbMDData = NULL;
	record.dwMDDataLen = 0;
    DWORD dwDesiredLen = 0;
	hRes = pIMeta->GetData( handle, NULL, &record, &dwDesiredLen);

    record.pbMDData = new unsigned char[ dwDesiredLen];
	record.dwMDDataLen = dwDesiredLen;

    // Get the actual name of AU
    hRes = pIMeta->GetData( handle, NULL, &record, &dwDesiredLen);

    if( FAILED( hRes))
	{
        delete [] record.pbMDData;
		return static_cast<int>( hRes);
	}
    
    // nice converting & assigning stuff
    char* asciiName = narrow( std::wstring( (wchar_t*)record.pbMDData));
    std::string tempNameA( asciiName);
    std::wstring tempNameW( tempNameA.begin(), tempNameA.end());
    name.assign( tempNameW);

    delete [] asciiName;    
    delete [] record.pbMDData;
    
	pIMeta->CloseKey( handle);	   
    return 0;
}

int CIISAnonymUserWork::GetPwd( std::wstring& pwd)
{
    CComPtr <IMSAdminBase> pIMeta;
    HRESULT hRes = 0;
    METADATA_HANDLE handle;
    METADATA_RECORD record = {0};

    CoInitialize (NULL);
    hRes = CoCreateInstance( CLSID_MSAdminBase, NULL, CLSCTX_ALL, IID_IMSAdminBase, (void **) &pIMeta);

	if( FAILED( hRes))
		return static_cast<int>( hRes);

    std::wstring path = GetIISPath();
	//get a handle to the web service
	hRes = pIMeta->OpenKey( METADATA_MASTER_ROOT_HANDLE, path.c_str() /*L"/LM/W3SVC/1/Root/DeskAlerts"*/,
							METADATA_PERMISSION_READ /*| METADATA_PERMISSION_WRITE*/, 20, &handle);

    // Need to query the length of returned AU name.
    record.dwMDIdentifier =  MD_ANONYMOUS_PWD;
	record.dwMDAttributes = METADATA_INHERIT;
	record.dwMDUserType = IIS_MD_UT_FILE;
	record.dwMDDataType = STRING_METADATA;
    record.pbMDData = NULL;
	record.dwMDDataLen = 0;
    DWORD dwDesiredLen = 0;
	hRes = pIMeta->GetData( handle, NULL, &record, &dwDesiredLen);

    record.pbMDData = new unsigned char[ dwDesiredLen];
	record.dwMDDataLen = dwDesiredLen;

    // Get the actual password of AU
    hRes = pIMeta->GetData( handle, NULL, &record, &dwDesiredLen);

    if( FAILED( hRes))
	{
        delete [] record.pbMDData;
		return static_cast<int>( hRes);
	}
    
    // nice converting & assigning stuff
    char* asciiPwd = narrow( std::wstring( (wchar_t*)record.pbMDData));
    std::string tempPwdA( asciiPwd);
    std::wstring tempPwdW( tempPwdA.begin(), tempPwdA.end());
    pwd.assign( tempPwdW);

    delete [] asciiPwd;    
    delete [] record.pbMDData;

	pIMeta->CloseKey( handle);	
    return 0;
}

int CIISAnonymUserWork::SerVD( const std::wstring& virt_dir)
{
    swzVirtDir.assign( virt_dir);
    return 0;
}

extern "C" IISWORK_API void SetAnonymousUserName( HWND hwndParent, int string_size, 
                                                            char *variables, stack_t **stacktop)
{
    EXDLL_INIT();
    char cNewAUName[256] = {0};	
	popstring( &cNewAUName[0]); // here we have AU name to update

    std::string tempA( cNewAUName);
    std::wstring tempW( tempA.begin(), tempA.end());
    int res = CIISAnonymUserWork::SetName( tempW);    

    std::stringstream szRes;
    szRes << res;

    pushstring( szRes.str().c_str());
}

extern "C" IISWORK_API void SetAnonymousUserPwd( HWND hwndParent, int string_size, 
                                                           char *variables, stack_t **stacktop)
{
    EXDLL_INIT();
    char cNewAUPwd[256] = {0};	
	popstring( &cNewAUPwd[0]); // here we have AU password to update

    std::string tempA( cNewAUPwd);
    std::wstring tempW( tempA.begin(), tempA.end());
    int res = CIISAnonymUserWork::SetPwd( tempW);   

    std::stringstream szRes;
    szRes << res;

    pushstring( szRes.str().c_str());
}

extern "C" IISWORK_API void GetAnonymousUserName( HWND hwndParent, int string_size, 
                                                            char *variables, stack_t **stacktop)
{
    EXDLL_INIT();

    std::wstring tempNameW;
    int res = CIISAnonymUserWork::GetName( tempNameW);

    std::stringstream szRes;
    szRes << res;

    pushstring( szRes.str().c_str());

    char* asciiName = narrow( tempNameW);
    pushstring( asciiName);
    delete [] asciiName;
}

extern "C" IISWORK_API void GetAnonymousUserPwd( HWND hwndParent, int string_size, 
                                                           char *variables, stack_t **stacktop)
{
    EXDLL_INIT();

    std::wstring tempPwdW;
    int res = CIISAnonymUserWork::GetPwd( tempPwdW);

    std::stringstream szRes;
    szRes << res;

    pushstring( szRes.str().c_str());

    char* asciiPwd = narrow( tempPwdW);
    pushstring( asciiPwd);
    delete [] asciiPwd;
}

extern "C" IISWORK_API void SetVirtualDirectoryName( HWND hwndParent, int string_size, 
                                                               char *variables, stack_t **stacktop)
{
    EXDLL_INIT();

    char cVirtDir[256] = {0};	
	popstring( &cVirtDir[0]); // here we have Virtual Directory sub-path

    std::string tempA( cVirtDir);
    std::wstring tempW( tempA.begin(), tempA.end());

    swzVirtDir.assign( tempW);

    char* asciiPath = narrow( GetIISPath());
    pushstring( asciiPath);
    delete [] asciiPath;
}


