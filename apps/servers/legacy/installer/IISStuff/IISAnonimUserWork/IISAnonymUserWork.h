#include <initguid.h>
#include <windows.h>
#include <iostream>
#include <winnt.h>

#include "iadmw.h" // COM Interface header
#include "iiscnfg.h" // MD_ & IIS_MD_ #defines


#include <string>

#include "pluginapi.h"

// Here we assume that client of this class runs on the same computer, not remotelly.
class IISWORK_API CIISAnonymUserWork {

public:	

    static int SetName( const std::wstring& newName);    
    static int SetPwd( const std::wstring& newPwd);

    static int GetName( std::wstring& name);
    static int GetPwd( std::wstring& pwd);

    static int SerVD( const std::wstring& virt_dir);
};

extern "C" IISWORK_API void SetAnonymousUserName( HWND hwndParent, int string_size, 
                                                            char *variables, stack_t **stacktop);

extern "C" IISWORK_API void SetAnonymousUserPwd( HWND hwndParent, int string_size, 
                                                           char *variables, stack_t **stacktop);

extern "C" IISWORK_API void GetAnonymousUserName( HWND hwndParent, int string_size, 
                                                            char *variables, stack_t **stacktop);

extern "C" IISWORK_API void GetAnonymousUserPwd( HWND hwndParent, int string_size, 
                                                           char *variables, stack_t **stacktop);

extern "C" IISWORK_API void SetVirtualDirectoryName( HWND hwndParent, int string_size, 
                                                               char *variables, stack_t **stacktop);
