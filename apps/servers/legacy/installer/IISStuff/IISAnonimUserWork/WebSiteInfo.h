#include <initguid.h>
#include <windows.h>
#include <iostream>
#include <winnt.h>

#include "iadmw.h" // COM Interface header
#include "iiscnfg.h" // MD_ & IIS_MD_ #defines


#include <string>

#include "pluginapi.h"
#include <vector>

static std::vector<std::wstring> webSites;

extern "C" IISWORK_API void InitializeWebSitesInfo( HWND hwndParent, int string_size, 
												   char *variables, stack_t **stacktop);

extern "C" IISWORK_API void GetWebSitesCount( HWND hwndParent, int string_size, 
									   char *variables, stack_t **stacktop);

extern "C" IISWORK_API void GetWebSiteById( HWND hwndParent, int string_size, 
											 char *variables, stack_t **stacktop);