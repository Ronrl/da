
!ifndef STRREP_INCLUDED
!define STRREP_INCLUDED

!macro StrReplaceCall _PREFIX OUT NEEDLE NEEDLE2 HAYSTACK
	Push "${HAYSTACK}"
	Push "${NEEDLE}"
	Push "${NEEDLE2}"
	Call ${_PREFIX}StrReplace
	Pop "${OUT}"
!macroend

!macro StrReplace un un_
	Function ${un_}StrReplace
		Exch $2
		Exch
		Exch $1
		Exch
		Exch 2
		Exch $0

		Push $3
		Push $4
		Push $5
		Push $6
		Push $7
		Push $8

		StrCpy $3 -1
		StrLen $4 $1
		StrLen $6 $0
		loop:
			IntOp $3 $3 + 1
			StrCpy $5 $0 $4 $3
			StrCmp $5 $1 found
			StrCmp $3 $6 done
			Goto loop
		found:
			StrCpy $5 $0 $3
			IntOp $8 $3 + $4
			StrCpy $7 $0 "" $8
			StrCpy $0 $5$2$7
			StrLen $3 $5$2
			IntOp $3 $3 - 1
			StrLen $6 $0
			Goto loop
		done:
		StrCpy $2 $0

		Pop $8
		Pop $7
		Pop $6
		Pop $5
		Pop $4
		Pop $3
		Pop $0
		Pop $1
		Exch $2
	FunctionEnd
	!undef ${un}StrReplace
	!define ${un}StrReplace "!insertmacro StrReplaceCall '${un_}'"
!macroend

!define StrReplace "!insertmacro StrReplace '' ''"
!define UnStrReplace "!insertmacro StrReplace 'Un' 'un.'"

!endif