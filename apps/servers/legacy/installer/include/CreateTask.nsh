
Function CreateTask
	System::Store /NOUNLOAD "S r5r4r3r2r1r0"

	nsExec::Exec 'schtasks /delete /tn "$0" /f'
	Pop $R0

	${StrReplace} $0 '$\"' '$\"$\"' '$0'
	${StrReplace} $1 '$\"' '$\"$\"' '$1'
	${StrReplace} $2 '$\"' '$\"$\"' '$2'
	${StrReplace} $3 '$\"' '$\"$\"' '$3'
	${StrReplace} $4 '$\"' '$\"$\"' '$4'
	${StrReplace} $5 '$\"' '$\"$\"' '$5'

	nsExec::Exec 'schtasks /create /tn "$0" /tr "$1" /sc "$2" /mo "$3" /ru "$4" /rp "$5" /st "00:00:00"'
	Pop $R0
	StrCmp $R0 "0" 0 error
		StrCpy $R0 "ok"
		Goto done
error:
		StrCpy $R0 "error"
done:
	; restore registers and push result
	System::Store /NOUNLOAD "P0 L"
	System::Free 0
FunctionEnd
