
!ifndef CUSTOMFUNCTION_INCLUDED
!define CUSTOMFUNCTION_INCLUDED

!macro ReplaceInASPConfigCall _FILE _SEARCHTEXT _REPLACEMENT
	Push $0
		${StrReplace} $0 "$\"" "$\"$\"" "${_REPLACEMENT}"
		${ReplaceInFile} "${_FILE}" "${_SEARCHTEXT}" "$0"
	Pop $0
!macroend
!define ReplaceInASPConfig "!insertmacro ReplaceInASPConfigCall"

!macro ReplaceParamInSQLFileCall _FILE _PARAM _VALUE
	Push $0
	Push $1
		StrCpy $0 "%${_PARAM}%"
		${ReplaceInFile} "${_FILE}" "$0" "${_VALUE}"

		StrCpy $0 "%${_PARAM}_string%"
		${StrReplace} $1 "'" "''" "${_VALUE}"
		${ReplaceInFile} "${_FILE}" "$0" "$1"

		StrCpy $0 "%${_PARAM}_hook%"
		${StrReplace} $1 "]" "]]" "${_VALUE}"
		${ReplaceInFile} "${_FILE}" "$0" "$1"

		StrCpy $0 "%${_PARAM}_both%"
		${StrReplace} $1 "'" "''" "${_VALUE}"
		${StrReplace} $1 "]" "]]" "$1"
		${ReplaceInFile} "${_FILE}" "$0" "$1"
	Pop $1
	Pop $0
!macroend
!define ReplaceParamInSQLFile "!insertmacro ReplaceParamInSQLFileCall"

!macro ReadFromASPConfigCall _PREFIX _FILE _ENTRY _RESULT
	StrCpy ${_RESULT} ""
	${${_PREFIX}ReadFromConfig} "${_FILE}" "${_ENTRY}" ${_RESULT}
	${${_PREFIX}StrReplace} ${_RESULT} "$\"$\"" "$\"" ${_RESULT}
!macroend
!define ReadFromASPConfig "!insertmacro ReadFromASPConfigCall ''"
!define UnReadFromASPConfig "!insertmacro ReadFromASPConfigCall 'Un'"
!define ReadFromOurASPConfig `${ReadFromASPConfig} "${INSTALLED_conf}"`

!macro ReadFromASPConfigIfUpdateCall _ENTRY _RESULT
	!define Macro1ID ${__LINE__}
	StrCmp "$is_update" "1" 0 noupdate_${Macro1ID}
		${ReadFromOurASPConfig} ${_ENTRY} ${_RESULT}
	noupdate_${Macro1ID}:
	!undef Macro1ID
!macroend
!define ReadFromASPConfigIfUpdate "!insertmacro ReadFromASPConfigIfUpdateCall"

!macro ReadFromASPorRegistryCall _CONFNAME _REGNAME _RESULT
	!define Macro2ID ${__LINE__}
	StrCpy ${_RESULT} ""
	${ReadFromASPConfigIfUpdate} "${_CONFNAME}" ${_RESULT}
	StrCmp ${_RESULT} "" 0 exists_${Macro2ID}
		ReadRegStr ${_RESULT} HKCU "${REGPATH}" "${_REGNAME}"
	exists_${Macro2ID}:
	!undef Macro2ID
!macroend
!define ReadFromASPorRegistry "!insertmacro ReadFromASPorRegistryCall"

!macro ReadFromASPorRegistryAndWriteCall _FILE _FIELDNAME _CONFNAME _REGNAME _RESULT
	!define Macro3ID ${__LINE__}
	${ReadFromASPorRegistry} "${_CONFNAME}" "${_REGNAME}" ${_RESULT}
	StrCmp ${_RESULT} "" empty_${Macro3ID}
		WriteINIStr "${_FILE}" "${_FIELDNAME}" "State" ${_RESULT}
	empty_${Macro3ID}:
	!undef Macro3ID
!macroend
!define ReadFromASPorRegistryAndWrite "!insertmacro ReadFromASPorRegistryAndWriteCall"

!macro SetRadioboxCall _FILE _FIELDNAME _VALUE _DEFVALUE _CHECKBOXVALUE
	!define Macro4ID ${__LINE__}
	Push $0
		StrCpy $0 ${_VALUE}
		StrCmp "$0" "" 0 set_${Macro4ID}
			StrCpy $0 "${_DEFVALUE}"
		set_${Macro4ID}:
		StrCmp "$0" "${_CHECKBOXVALUE}" checked_${Macro4ID}
			WriteINIStr "${_FILE}" "${_FIELDNAME}" "State" "0"
			Goto end_${Macro4ID}
		checked_${Macro4ID}:
			WriteINIStr "${_FILE}" "${_FIELDNAME}" "State" "1"
		end_${Macro4ID}:
	Pop $0
	!undef Macro4ID
!macroend
!define SetRadiobox "!insertmacro SetRadioboxCall"

!macro CheckByExistsFileOrRegistryAndWriteCall _FILE _FIELDNAME _CHECKFILE _REGNAME _RESULT
	!define Macro5ID ${__LINE__}
	StrCmp "$is_update" "1" 0 read_${Macro5ID}
		StrCpy ${_RESULT} "1"
		IfFileExists "${_CHECKFILE}" 0 write_${Macro5ID}
			StrCpy ${_RESULT} "0"
			Goto write_${Macro5ID}
	read_${Macro5ID}:
		ReadRegStr ${_RESULT} HKCU "${REGPATH}" "${_REGNAME}"
	StrCmp ${_RESULT} "" empty_${Macro5ID}
	write_${Macro5ID}:
		WriteINIStr "${_FILE}" "${_FIELDNAME}" "State" ${_RESULT}
	empty_${Macro5ID}:
	!undef Macro5ID
!macroend
!define CheckByExistsFileOrRegistryAndWrite "!insertmacro CheckByExistsFileOrRegistryAndWriteCall"

!macro SetRightsWithPrintCall _FILE _USER _RIGHTS
	!define Macro6ID ${__LINE__}
	IfFileExists ${_FILE} 0 notexists_${Macro6ID}
		Push $0
		Push $1
			Push "successful"
			AccessControl::DisableFileInheritance /NOUNLOAD ${_FILE}
			Pop $0
			StrCmp $0 "successful" clear_${Macro6ID}
				Pop $1
				Goto print_${Macro6ID}
			clear_${Macro6ID}:
				Push "successful"
				AccessControl::ClearOnFile /NOUNLOAD ${_FILE} ${_USER} ${_RIGHTS}
				Pop $0
				StrCmp $0 "successful" print_${Macro6ID}
				Pop $1
			print_${Macro6ID}:
			detailprint "${_FILE} - $0"
		Pop $1
		Pop $0
	notexists_${Macro6ID}:
	!undef Macro6ID
!macroend
!define SetRightsWithPrint "!insertmacro SetRightsWithPrintCall"

;--------------------------------
!macro EXECUTE_SQL_SCRIPT SQLFILE
	!define MacroSSQLGOID ${__LINE__}
	ClearErrors
	Push $0
	Push $1
	Push $R0
	Push $R1
	Push $R2
	Push $R3

	FileOpen $R0 "${SQLFILE}" r
	IfErrors noFile_${MacroSSQLGOID}

	FileOpen $R2 "$PLUGINSDIR\tmp.sql" w
	IfErrors cantOpenWriteFile_${MacroSSQLGOID}
	loop_${MacroSSQLGOID}:
		FileRead $R0 $R1
		IfErrors done_${MacroSSQLGOID}
		${Trim} $R1 $R3
		StrCmp $R3 "GO" execute_${MacroSSQLGOID}
			FileWrite $R2 "$R1"
			IfErrors cantWriteFile_${MacroSSQLGOID}
		Goto loop_${MacroSSQLGOID}
		execute_${MacroSSQLGOID}:
			FileClose $R2
			MSSQL_OLEDB::SQL_ExecuteScript /NOUNLOAD "$PLUGINSDIR\tmp.sql"
			Pop $0
			Pop $1
			StrCmp "$0" "0" no_executesql_err_${MacroSSQLGOID}
				MSSQL_OLEDB::SQL_GetError /NOUNLOAD
				Pop $0
				Pop $0
				DetailPrint $0
			no_executesql_err_${MacroSSQLGOID}:
			FileOpen $R2 "$PLUGINSDIR\tmp.sql" w
			IfErrors cantOpenWriteFile_${MacroSSQLGOID}
		Goto loop_${MacroSSQLGOID}
	done_${MacroSSQLGOID}:
		FileClose $R2
		MSSQL_OLEDB::SQL_ExecuteScript /NOUNLOAD "$PLUGINSDIR\tmp.sql"
		Pop $0
		Pop $1
		StrCmp "$0" "0" end_${MacroSSQLGOID}
			MSSQL_OLEDB::SQL_GetError /NOUNLOAD
			Pop $0
			Pop $0
			DetailPrint $0
	Goto end_${MacroSSQLGOID}
cantWriteFile_${MacroSSQLGOID}:
		FileClose $R2
		DetailPrint "Unable to write SQL File - $PLUGINSDIR\tmp.sql"
	Goto end_${MacroSSQLGOID}
cantOpenWriteFile_${MacroSSQLGOID}:
		DetailPrint "Unable to open for write SQL File - ${SQLFILE}"
	Goto end_${MacroSSQLGOID}
noFile_${MacroSSQLGOID}:
		DetailPrint "Unable to open SQL File - ${SQLFILE}"
end_${MacroSSQLGOID}:
	FileClose $R0
	IfFileExists "$PLUGINSDIR\tmp.sql" 0 noTemp_${MacroSSQLGOID}
		SetDetailsPrint none
		Delete "$PLUGINSDIR\tmp.sql"
		SetDetailsPrint both
noTemp_${MacroSSQLGOID}:
	Pop $R3
	Pop $R2
	Pop $R1
	Pop $R0
	Pop $1
	Pop $0
	!undef MacroSSQLGOID
!macroend
!define ExecuteSQLScript "!insertmacro EXECUTE_SQL_SCRIPT"

; ################################################################
; appends \ to the path if missing
; example: !insertmacro GetCleanDir "c:\blabla"
; Pop $0 => "c:\blabla\"
!macro GetCleanDir INPUTDIR
  ; ATTENTION: USE ON YOUR OWN RISK!
  ; Please report bugs here: http://stefan.bertels.org/
  !define Index_GetCleanDir 'GetCleanDir_Line${__LINE__}'
  Push $R0
  Push $R1
  StrCpy $R0 "${INPUTDIR}"
  StrCmp $R0 "" ${Index_GetCleanDir}-finish
  StrCpy $R1 "$R0" "" -1
  StrCmp "$R1" "\" ${Index_GetCleanDir}-finish
  StrCpy $R0 "$R0\"
${Index_GetCleanDir}-finish:
  Pop $R1
  Exch $R0
  !undef Index_GetCleanDir
!macroend
 
; ################################################################
; similar to "RMDIR /r DIRECTORY", but does not remove DIRECTORY itself
; example: !insertmacro RemoveFilesAndSubDirs "$INSTDIR"
!macro RemoveFilesAndSubDirs DIRECTORY
  ; ATTENTION: USE ON YOUR OWN RISK!
  ; Please report bugs here: http://stefan.bertels.org/
  !define Index_RemoveFilesAndSubDirs 'RemoveFilesAndSubDirs_${__LINE__}'
 
  Push $R0
  Push $R1
  Push $R2
 
  !insertmacro GetCleanDir "${DIRECTORY}"
  Pop $R2
  FindFirst $R0 $R1 "$R2*.*"
${Index_RemoveFilesAndSubDirs}-loop:
  StrCmp $R1 "" ${Index_RemoveFilesAndSubDirs}-done
  StrCmp $R1 "." ${Index_RemoveFilesAndSubDirs}-next
  StrCmp $R1 ".." ${Index_RemoveFilesAndSubDirs}-next
  IfFileExists "$R2$R1\*.*" ${Index_RemoveFilesAndSubDirs}-directory
  ; file
  Delete "$R2$R1"
  goto ${Index_RemoveFilesAndSubDirs}-next
${Index_RemoveFilesAndSubDirs}-directory:
  ; directory
  RMDir /r "$R2$R1"
${Index_RemoveFilesAndSubDirs}-next:
  FindNext $R0 $R1
  Goto ${Index_RemoveFilesAndSubDirs}-loop
${Index_RemoveFilesAndSubDirs}-done:
  FindClose $R0
 
  Pop $R2
  Pop $R1
  Pop $R0
  !undef Index_RemoveFilesAndSubDirs
!macroend
!define RemoveFilesAndSubDirs "!insertmacro RemoveFilesAndSubDirs"

!macro RelGotoPageCall _PREFIX _PAGE
	Push "${_PAGE}"
	Call ${_PREFIX}RelGotoPage
!macroend

!macro RelGotoPage un un_
	Function ${un_}RelGotoPage
		Exch $0
		IntCmp $0 0 0 Move Move
			StrCmp $0 "X" 0 Move
				StrCpy $0 "120"
		Move:
		SendMessage $HWNDPARENT "0x408" "$0" ""
		Pop $0
	FunctionEnd
	!undef ${un}RelGotoPage
	!define ${un}RelGotoPage "!insertmacro RelGotoPageCall '${un_}'"
!macroend

!define RelGotoPage "!insertmacro RelGotoPage '' ''"
!define UnRelGotoPage "!insertmacro RelGotoPage 'Un' 'un.'"

!macro CreateGUIDCall _RESULT
	Call ${_PREFIX}CreateGUID
	Pop ${_RESULT}
!macroend

!macro CreateGUID un un_
	Function ${un_}CreateGUID
		Push $0
		Push $1
		Push $2
		System::Alloc 80
		System::Alloc 16
		System::Call 'ole32::CoCreateGuid(i sr1) i'
		System::Call 'ole32::StringFromGUID2(i r1, i sr2, i 80) i'
		System::Call 'kernel32::WideCharToMultiByte(i 0, i 0, i r2, i 80, t .r0, i ${NSIS_MAX_STRLEN}, i 0, i 0) i'
		System::Free $1
		System::Free $2
		Pop $2
		Pop $1
		Exch $0
	FunctionEnd
	!undef ${un}CreateGUID
	!define ${un}CreateGUID "!insertmacro CreateGUIDCall '${un_}'"
!macroend

!define CreateGUID "!insertmacro RelGotoPage '' ''"
!define UnCreateGUID "!insertmacro RelGotoPage 'Un' 'un.'"

!define Asc "!insertmacro Asc"
!macro Asc _STRING _RETURN
	Push ${_STRING}
	Call Asc
	Pop ${_RETURN}
!macroend
Function Asc
	Exch $0 ; CHAR
	Push $1
	System::Call "*(&t1 r0)i.r1"
	System::Call "*$1(&i1 .r0)"
	System::Free $1
	Pop $1
	Exch $0
FunctionEnd

!define ValidTrialKey "!insertmacro ValidTrialKey"
!macro ValidTrialKey _STRING _RETURN
	Push ${_STRING}
	Call ValidTrialKey
	Pop ${_RETURN}
!macroend
; xxx...xxSx - where S is sign char
Function ValidTrialKey
	Exch $0 ; $res
	Push $1 ; $i
	Push $2 ; $char
	Push $3 ; $int
	Push $4 ; $sign
	Push $5 ; $sign_char
	Push $R0 ; $str
	Push $R1 ; $str_len
	
	System::Call "User32::CharUpper(t r0 r0)i"
	StrLen $1 $0			; $i = strlen($res)
	IntCmp $1 6 0 not_valid 0 ; validate length to "6"

	StrCpy $2 $0 1 -1		; $char = $res[$i]
	StrCpy $5 $0 1 -2		; $sign_char = $res[$i]

	IntOp $1 $1 - 2			; $i-=2
	StrCpy $0 $0 $1			; $res = mid($res, $i)
	StrCpy $0 "$0$2"

	StrCpy $R0 "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ" ; $str
	StrLen $R1 $R0			; $str_len = strlen($str)

	StrCpy $1 0				; $i = 0
	StrCpy $4 0				; $sign = 0
loop:
		StrCpy $2 $0 1 $1		; $char = $res[$i]
		StrCmp $2 "" end		; end of string
		${Asc} $2 $2			; $char = ord($char)
		IntOp $3 $R1 - $1		; $int = $str_len - $i
		IntCmp $3 0 +2 0 +2		; if $int < 0
			IntOp $3 $3 * -1	; $int = Abs($int)
		IntOp $3 $3 + 1			; $int++
		IntOp $3 $2 % $3		; $int = $char % $int
		IntOp $4 $4 + $3		; $sign += $int
		IntOp $1 $1 + 1			; $i++
	Goto loop
end:
	IntOp $4 $4 % $R1		; $sign %= $str_len
	StrCpy $2 $R0 1 $4		; $char = $str[$sign]

	StrCpy $0 1			; set valid
	StrCmp $5 $2 +3		; if $sign_char != $char
not_valid:
		StrCpy $0 0		; set not valid

	Pop $R1
	Pop $R0
	Pop $5
	Pop $4
	Pop $3
	Pop $2
	Pop $1
	Exch $0
FunctionEnd

!endif