Dim Cmd
If WScript.Arguments.Count > 0 Then
	Cmd = WScript.Arguments.Item(0)
End If

On Error Resume Next
Set adminManager = CreateObject("Microsoft.ApplicationHost.WritableAdminManager")
If adminManager Then
	adminManager.CommitPath = "MACHINE/WEBROOT/APPHOST"
	Set isapiCgiRestrictionSection = adminManager.GetAdminSection("system.webServer/security/isapiCgiRestriction", "MACHINE/WEBROOT/APPHOST")
	Set isapiCgiRestrictionCollection = isapiCgiRestrictionSection.Collection

	Found = False
	Count = CLng(isapiCgiRestrictionCollection.Count) - 1
	For i = 0 to Count
		If InStr(isapiCgiRestrictionCollection.Item(i).Properties.Item("path").Value, "asp.dll") > 0 Then
			Found = True
			If Cmd <> "" Then
				If Cmd = "enable" Then
					isapiCgiRestrictionCollection.Item(i).Properties.Item("allowed").Value = True
					adminManager.CommitChanges()
				ElseIf Cmd = "disable" Then
					isapiCgiRestrictionCollection.Item(i).Properties.Item("allowed").Value = False
					adminManager.CommitChanges()
				End If
			Else
				If isapiCgiRestrictionCollection.Item(i).Properties.Item("allowed").Value = True Then
					WScript.Echo "enabled"
				Else
					WScript.Echo "disabled"
				End If
			End If
			Exit For
		End If
	Next

	if Found = False then
		If Cmd <> "" Then
			If Cmd = "enable" Then
				Set addElement = isapiCgiRestrictionCollection.CreateNewElement("add")
				addElement.Properties.Item("path").Value = "%windir%\system32\inetsrv\asp.dll"
				addElement.Properties.Item("description").Value = "Active Server Pages"
				addElement.Properties.Item("groupId").Value = "ASP"
				addElement.Properties.Item("allowed").Value = True
				isapiCgiRestrictionCollection.AddElement(addElement)

				adminManager.CommitChanges()
			End If
		Else
			WScript.Echo "disabled"
		End If
	End If
Else
	Dim aWebSvcExtRestrictionList
	Set providerObj = GetObject("winmgmts://localhost/root/MicrosoftIISv2")
	Set IIsWebServiceObj = providerObj.Get("IIsWebService='W3SVC'")
	Set IIsWebServiceSettingObj = providerObj.Get("IIsWebServiceSetting='W3SVC'")

	aWebSvcExtRestrictionList = IIsWebServiceSettingObj.WebSvcExtRestrictionList

	For Each i in aWebSvcExtRestrictionList
		If InStr(i.FilePath, "asp.dll") > 0 Then
			If Cmd <> "" Then
				If Cmd = "enable" Then
					IIsWebServiceObj.EnableWebServiceExtension i.ServerExtension
				ElseIf Cmd = "disable" Then
					IIsWebServiceObj.DisableWebServiceExtension i.ServerExtension
				End If
			Else
				If i.Access = "1" Then
					WScript.Echo "enabled"
				ElseIf i.Access = "0" Then
					WScript.Echo "disabled"
				End If
			End If
			Exit For
		End If
	Next
End If
