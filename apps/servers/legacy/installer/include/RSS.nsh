
Function ProcessRSS
	System::Store /NOUNLOAD "S"

!ifdef isRSSAddon
	ReadINIStr $1 "$PLUGINSDIR\iis_set.ini" "Field 2" "State"
;----------------------------------------------------------------------path
	ClearErrors
	ReadRegStr $R1 HKLM "SOFTWARE\Microsoft\INetStp" "PathWWWRoot"
	IfErrors 0 lbl_iis_nerr
		StrCpy $0 "%SystemDrive%\Inetpub\AdminScripts"
	Goto lbl_iis_fin
	lbl_iis_nerr:
		${StrReplace} $0 "wwwroot" "AdminScripts" $R1
	lbl_iis_fin:
	ExpandEnvStrings $0 $0

	StrCpy $2 "$0" "" -1 ; this gets the last char
	StrCmp $2 "\" 0 +2 ; check if last char is '\'
	StrCpy $0 "$2" -1 ; last char was '\', remove it

	IfFileExists "$0\*" exists ; directory exists
	IfFileExists "$0" 0 create ; if is a file then delete it
		Delete "$0"
	create:
		CreateDirectory $0
	exists:

	
	SetDetailsPrint none
	setoutpath $PLUGINSDIR
	StrCpy $3 $1
	${StrReplace} $3 "http://" "" $1
	${StrReplace} $3 "/" "_" $3
	StrCpy $2 "DeskAlerts_rss.vbs" 
	!define path_rss "include\DeskAlerts_rss.vbs"
	;StrCpy $3 include\DeskAlerts_schedule_$3\.vbs
	FILE ${path_rss}
	${ReplaceInFile} "$PLUGINSDIR\$2" "%URL%" "$1"
	CopyFiles /SILENT "$PLUGINSDIR\$2" "$0"
	StrCpy $2 "DeskAlerts_rss_$3.vbs" 
	Rename "$0\DeskAlerts_rss.vbs" "$0\$2"
	${StrReplace} $3 ".vbs" "" $2
	${StrReplace} $3 "_" " " $3
	StrCpy $3 "$3 Task"
	SetDetailsPrint both

;-------------------------------------------------------------------------------- RSSd task
	push '$3'
	push 'wscript.exe /b "$0\$2\"'
	push 'minute' ;minute, hourly, daily, weekly, monthly, once, onstart, onlogon, onidle, onevent
	push '5' ;minute:1-1439, hourly:1-23, daily:1-365, weekly:1-52, monthly:1-12 or first, second, third, fourth, last, lastday
	push '' ; '$5\$3'
	push '' ; '$4'
	Call CreateTask
	Pop $1
	DetailPrint "DeskAlerts RSS Send Task creation result: $1"
	StrCmp $1 "ok" is_ok
		DetailPrint "Can't create task for: $0\DeskAlerts_rss.vbs"
		DetailPrint "Please add it manually to system scheduler"
	is_ok:

!else
	nsExec::Exec 'schtasks /delete /tn "DeskAlerts RSS Send Task" /f'
	Pop $R0
!endif

	System::Store /NOUNLOAD "L"
	System::Free 0
FunctionEnd