!include "StrFunc.nsh"

Function UpdateConfig
	Exch $1
	Exch
	Exch $0
	Exch
	Push $2		;handler for old config file
	Push $3		;handler for new config file
	Push $4		;string taken from new config
	Push $5		;setting name taken from new config
	Push $6		;length of setting name
	Push $7		;string taken from old config
	Push $8		;fragment of $7, if equal to $5, string shold be written to result config
	Push $R0	;name of temporary file for storing result config
	Push $R1	;handler for temporary file
	
	FileOpen $2 $1 r
		IfErrors error_opening_old
	FileOpen $3 $0 r

	GetTempFileName $R0 
	FileOpen $R1 $R0 w

	new_config_loop:
		FileRead $3 $4
		IfErrors new_eof
		
		StrCpy $5 $4 4 0
		StrCmp $5 "Conf" 0 write_new
		
		${StrTok} $5 $4 "=" 0 1
		StrLen $6 $5
		
		old_config_loop:
			FileRead $2 $7
			IfErrors old_eof
			
			StrCpy $8 $7 $6 0
			StrCmp $8 $5 0 old_config_loop
			
			FileWrite $R1 $7
			FileSeek $2 0 SET
			Goto new_config_loop
			
		old_eof:
			FileSeek $2 0 SET
			
	write_new:
		FileWrite $R1 $4
		Goto new_config_loop
		
	new_eof:
		FileClose $2
		FileClose $3
		FileClose $R1

		Delete "$0"
		Rename "$R0" "$0"
		StrCpy $0 "noerror"
		Goto end
	
	error_opening_old:
		StrCpy $0 "error"
		
	end:
	ClearErrors
	Pop $R1
	Pop $R0
	Pop $8
	Pop $7
	Pop $6
	Pop $5
	Pop $4
	Pop $3
	Pop $2
	Pop $1
	Exch $0
	
FunctionEnd 