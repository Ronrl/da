;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Generate a random number using the RtlGenRandom api
;; P1 :out: Random number
;; P2 :in:  Minimum value
;; P3 :in:  Maximum value
;; min/max P2 and P3 values = -2 147 483 647 / 2 147 483 647
;; max range = 2 147 483 647 (31-bit)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

!define Rnd "!insertmacro _Rnd"

!macro _Rnd _RetVal_ _Min_ _Max_
	Push "${_Max_}"
	Push "${_Min_}"
	Call Rnd
	Pop ${_RetVal_}
!macroend

Function Rnd
	Exch $0  ;; Min / return value
	Exch
	Exch $1  ;; Max / random value
	Push "$3"  ;; Max - Min range
	Push "$4"  ;; random value buffer

	IntOp $3 $1 - $0 ;; calculate range
	IntOp $3 $3 + 1
	System::Call '*(l) i .r4'
	System::Call 'advapi32::SystemFunction036(i r4, i 4)'  ;; RtlGenRandom
	System::Call '*$4(l .r1)'
	System::Free $4
	;; fit value within range
	System::Int64Op $1 * $3
	Pop $3
	System::Int64Op $3 / 0xFFFFFFFF
	Pop $3
	IntOp $0 $3 + $0  ;; index with minimum value

	Pop $4
	Pop $3
	Pop $1
	Exch $0
FunctionEnd


!define RandomStr `!insertmacro RandomStrCall`

!macro RandomStrCall _OUT _LEN
	Push `${_LEN}`
	Call RandomStr
	Pop `${_OUT}`
!macroend

Function RandomStr
	Exch $0 ; _LEN
	Push $1
	Push $2
	Push $3
	Push $4
	Push $5

	StrCpy $5 $0
	StrCpy $0 ""
	StrCpy $1 "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
	StrLen $2 "$1"
loop:
	IntCmp $5 0 end_loop end_loop
	${Rnd} $4 0 $2

	StrCpy $3 $1 1 $4
	StrCpy $0 "$0$3"
	IntOp $5 $5 - 1
	Goto loop
end_loop:

	Pop $5
	Pop $4
	Pop $3
	Pop $2
	Pop $1
	Exch $0
FunctionEnd