
!ifndef READCONFIG_INCLUDED
!define READCONFIG_INCLUDED

!include "StrFunc.nsh"
${StrStr}
${UnStrStr}
${StrTok}
${UnStrTok}

!include "Trim.nsh"
${Trim}
${UnTrim}

!macro ReadFromConfigCall _PREFIX _FILE _ENTRY _RESULT
	Push "${_FILE}"
	Push "${_ENTRY}"
	Call ${_PREFIX}ReadFromConfig
	Pop "${_RESULT}"
!macroend

!macro ReadFromConfig un un_
	Function ${un_}ReadFromConfig
		Exch $1
		Exch
		Exch $0
		Exch

		Push $2
		Push $3
		Push $4
		Push $5
		Push $R1
		Push $R2
		Push $R3
		Push $R4

		ClearErrors

		FileOpen $2 $0 r
		IfErrors error
		StrLen $0 $1
		StrCmp $0 0 error

		readnext:
		FileRead $2 $3
		IfErrors error

		${${un}StrTok} $4 "$3" "=" 0 1
		${${un}Trim} $4 $4
		StrCmp $4 $1 0 readnext
		${${un}StrStr} $0 "$3" "="
		StrCpy $0 $0 "" 1 ;remove equal char
		${${un}Trim} $0 $0

		StrCpy $3 $0 1
		StrCpy $4 $0 "" -1

		StrCmp "$3" "$\"" trimquote 0
			StrCpy $R0 -1
			IntOp $R0 $R0 + 1
				StrCpy $R2 $0 1 $R0
				StrCmp $R2 "" +2
				StrCmp $R2 "'" 0 -3
			StrCpy $0 $0 $R0
			${${un}Trim} $0 $0
			Goto close
		trimquote:
			StrCpy $0 $0 "" 1 ;remove first double quote char
			StrCpy $R0 -1 ;set first pos
		next_char:
				IntOp $R0 $R0 + 1 ;set to next pos
				StrCpy $R2 $0 1 $R0 ;get one char
				StrCmp $R2 "" copy_string ;str is over
				StrCmp $R2 "$\"" 0 next_char
					IntOp $R1 $R0 + 1 ;set to next pos
					StrCpy $R3 $0 1 $R1 ;get one char
					StrCmp $R3 "$\"" 0 copy_string ;char was another double quote - it was escaping, otherwise - it was the end of value
						IntOp $R0 $R0 + 1 ;skip this second quote
						Goto next_char
		copy_string:
			StrCpy $0 $0 $R0
			Goto close
			
		error:
		SetErrors
		StrCpy $0 ''

		close:
		FileClose $2

		Pop $R4
		Pop $R3
		Pop $R2
		Pop $R1
		Pop $5
		Pop $4
		Pop $3
		Pop $2
		Pop $1
		Exch $0
	FunctionEnd
	!undef ${un}ReadFromConfig
	!define ${un}ReadFromConfig "!insertmacro ReadFromConfigCall '${un_}'"
!macroend

!define ReadFromConfig "!insertmacro ReadFromConfig '' ''"
!define UnReadFromConfig "!insertmacro ReadFromConfig 'Un' 'un.'"

!endif
