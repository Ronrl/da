
!ifndef TRIM_INCLUDED
!define TRIM_INCLUDED

!macro TrimCall _PREFIX _STRING _RESULT
	Push "${_STRING}"
	Call ${_PREFIX}Trim
	Pop "${_RESULT}"
!macroend

!macro Trim un un_
	Function ${un_}Trim
		Exch $R1 ; Original string
		Push $R2

	Loop:
		StrCpy $R2 "$R1" 1
		StrCmp "$R2" " " TrimLeft
		StrCmp "$R2" "$\r" TrimLeft
		StrCmp "$R2" "$\n" TrimLeft
		StrCmp "$R2" "$\t" TrimLeft
		GoTo Loop2
	TrimLeft:
		StrCpy $R1 "$R1" "" 1
		Goto Loop

	Loop2:
		StrCpy $R2 "$R1" 1 -1
		StrCmp "$R2" " " TrimRight
		StrCmp "$R2" "$\r" TrimRight
		StrCmp "$R2" "$\n" TrimRight
		StrCmp "$R2" "$\t" TrimRight
		GoTo Done
	TrimRight:
		StrCpy $R1 "$R1" -1
		Goto Loop2

	Done:
		Pop $R2
		Exch $R1
	FunctionEnd
	!undef ${un}Trim
	!define ${un}Trim "!insertmacro TrimCall '${un_}'"
!macroend

!define Trim "!insertmacro Trim '' ''"
!define UnTrim "!insertmacro Trim 'Un' 'un.'"

!endif