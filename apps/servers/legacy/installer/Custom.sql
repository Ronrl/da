IF EXISTS ( SELECT 1 FROM settings WHERE name = 'ConfAddonsTrialDate' AND val='')
UPDATE settings SET val=CONVERT(nvarchar, GETDATE(), 9) WHERE name='ConfAddonsTrialDate';
UPDATE settings SET val='Active Directory;SMS;E-mail;Screensaver;RSS;Wallpaper;Blog;Twitter;LinkedIn;Fullscreen;Ticker;Ticker Pro;Survey;Statistics;InstantAlerts;TextToCall;Mobile' WHERE name='ConfAddonsTrialList';