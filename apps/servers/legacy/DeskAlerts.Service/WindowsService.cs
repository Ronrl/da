﻿namespace DeskAlerts.ADIntegration
{
    using DeskAlerts.Service.AD;
    using System.ServiceModel;
    using System.ServiceProcess;

    public partial class WindowsService : ServiceBase
    {
        private const string EventLogSource = "DeskAlertsADI";

        private const string EventLogLog = "DeskAlerts ActiveDirectoryIntegration Service";

        public ServiceHost serviceHost = null;

        public WindowsService()
        {
            InitializeComponent();
            if (!System.Diagnostics.EventLog.SourceExists(EventLogSource))
            {
                System.Diagnostics.EventLog.CreateEventSource(EventLogSource, EventLogLog);
            }
            eventLogger.Source = EventLogSource;
            eventLogger.Log = EventLogLog;
        }

        protected override void OnStart(string[] args)
        {
            if (serviceHost != null)
            {
                serviceHost.Close();
            }
            serviceHost = new ServiceHost(typeof(ADService));
            serviceHost.Open();
            eventLogger.WriteEntry("The service has been started");
        }

        protected override void OnStop()
        {
            if (serviceHost != null)
            {
                serviceHost.Close();
                serviceHost = null;
            }
            eventLogger.WriteEntry("The service has been stopped");
        }
    }
}
