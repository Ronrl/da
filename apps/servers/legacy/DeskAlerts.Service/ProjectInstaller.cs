﻿using System.ComponentModel;
using System.Configuration.Install;

namespace DeskAlerts.ADIntegration
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : Installer
    {
        public ProjectInstaller()
        {
            InitializeComponent();
            serviceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            serviceInstaller.ServiceName = "DAADISvc";
            serviceInstaller.Description = "The service handle requests from DeskAlerts server and him responses via LDAP protocol";
            serviceInstaller.DisplayName = "DeskAlerts Active Directory integration service";
            serviceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
        }
    }
}
