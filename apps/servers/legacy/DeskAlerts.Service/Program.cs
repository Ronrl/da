﻿using System.ServiceProcess;

namespace DeskAlerts.ADIntegration
{
    static class Program
    {
        static void Main()
        {
            var services = new ServiceBase[]
            {
                new WindowsService()
            };
            ServiceBase.Run(services);
        }
    }
}
