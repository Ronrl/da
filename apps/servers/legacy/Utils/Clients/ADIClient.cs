﻿namespace DeskAlertsDotNet.Utils.Clients
{
    using DeskAlerts.ApplicationCore.Interfaces;
    using DeskAlertsDotNet.Utils.Interfaces;
    using System.ServiceModel;

    public class ADIClient : ClientBase<IADIService>, IADIClient
    {
        public string DoWork(string message)
        {
            return Channel.DoWork(message);
        }
    }
}