﻿namespace DeskAlertsDotNet.Utils.Tasks
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using DeskAlerts.ApplicationCore.Interfaces;
    using DeskAlerts.ApplicationCore.Repositories;
    using DeskAlerts.ApplicationCore.Utilities;
    using DeskAlertsDotNet.Cache;
    using DeskAlertsDotNet.DataBase;
    using DeskAlertsDotNet.Models;
    using DeskAlertsDotNet.Parameters;
    using DeskAlertsDotNet.Utils.Factories;

    using NLog;

    using DataRow = DataBase.DataRow;
    using DataSet = DataBase.DataSet;

    public class ScheduleTask : IDisposable
    {
        public ScheduleTask()
        {
            _configurationManager = new WebConfigConfigurationManager(Config.Configuration);
            _campaignRepository = new PpCampaignRepository(_configurationManager);
            _cacheManager = new CacheManager();
        }

        private const string SelectIdFromSmsSentByAlertIdAndUserId =
            "SELECT id FROM sms_sent AS ss WHERE alert_id = @alertId AND user_id = @userId AND ss.sent_date > DATEADD(d, 0, DATEDIFF(d, 0, GETDATE()))";

        private const string SelectIdFromEmailSentByAlertIdAndUserId =
            "SELECT id FROM email_sent AS es WHERE alert_id = @alertId AND user_id = @userId AND es.sent_date > DATEADD(d, 0, DATEDIFF(d, 0, GETDATE()))";

        private const string SelectIdFromSmsSentByAlertId =
            "SELECT user_id AS id FROM sms_sent AS ss WHERE alert_id = @alert_id AND ss.sent_date > DATEADD(d, 0, DATEDIFF(d, 0, GETDATE()))";

        private const string SelectIdFromEmailSentByAlertId =
            "SELECT user_id AS id FROM email_sent AS es WHERE alert_id = @alert_id AND es.sent_date > DATEADD(d, 0, DATEDIFF(d, 0, GETDATE()))";

        private const string InsertSmsSentByAlertIdAndUserId =
            "INSERT INTO sms_sent (alert_id,user_id,sent_date) VALUES  (@alert_id, @user_id, @date)";

        private const string InsertEmailSentByAlertIdAndUserId =
            "INSERT INTO email_sent (alert_id,user_id,sent_date) VALUES  (@alert_id, @user_id, @date)";

        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        private DBManager _manager;

        private readonly IConfigurationManager _configurationManager;

        private readonly ICampaignRepository _campaignRepository;

        private readonly CacheManager _cacheManager;

        public void Start()
        {
            _manager = new DBManager();
            _cacheManager.UpdateActiveCampaigns();
            var scheduledSmsAlerts = _cacheManager.GetScheduledSmsAlerts();
            SendSms(scheduledSmsAlerts);
            var scheduledEmailAlerts = _cacheManager.GetScheduledEmailAlerts();
            SendEmail(scheduledEmailAlerts);
        }

        private void SendSms(IEnumerable<UserAlertsDTO> scheduledSmsAlerts)
        {
            foreach (var userAlerts in scheduledSmsAlerts)
            {
                if (userAlerts.Alerts.Any())
                {
                    if (userAlerts.UserName == "broadcast")
                    {
                        SendBroadcastSms(userAlerts);
                    }
                    else
                    {
                        SendToUserSms(userAlerts);
                    }
                }
            }
        }

        private void SendEmail(IEnumerable<UserAlertsDTO> scheduledEmailAlerts)
        {
            foreach (var userAlerts in scheduledEmailAlerts)
            {
                if (userAlerts.Alerts.Any())
                {
                    if (userAlerts.UserName == "broadcast")
                    {
                        SendBroadcastEmail(userAlerts);
                    }
                    else
                    {
                        SendToUserEmail(userAlerts);
                    }
                }
            }
        }

        private void SendBroadcastSms(UserAlertsDTO userAlerts)
        {
            foreach (var alert in userAlerts.Alerts)
            {
                if (_cacheManager.IsActiveAlert(alert))
                {
                    var usersIdToSend = GetUsersIdToSmsSend(alert.Id);

                    foreach (var userIdDataRow in usersIdToSend)
                    {
                        var user = UserManager.Default.GetUserById(userIdDataRow.GetInt("id"));
                        var alertText = alert.GetClearAlertText();
                        if (!string.IsNullOrEmpty(user.Phone) && !string.IsNullOrEmpty(alertText))
                        {
                            var smsResult = SmsManager.Default.Send(alertText, user.Phone);
                            if (smsResult == SmsResult.Ok)
                            {
                                WriteToSmsSend(user.Id, alert.Id);
                            }
                        }
                    }
                }
            }
        }

        private void SendBroadcastEmail(UserAlertsDTO userAlerts)
        {
            foreach (var alert in userAlerts.Alerts)
            {
                if (_cacheManager.IsActiveAlert(alert))
                {
                    var usersIdToSend = GetUsersIdToEmailSend(alert.Id);

                    foreach (var userIdDataRow in usersIdToSend)
                    {
                        var user = UserManager.Default.GetUserById(userIdDataRow.GetInt("id"));
                        var alertText = alert.GetAlertText();
                        if (!string.IsNullOrEmpty(user.Email))
                        {
                            var emailResult = EmailManager.Default.Send(alert.Title, alertText, user.Email);
                            if (emailResult == EmailResult.Ok)
                            {
                                WriteToEmailSend(user.Id, alert.Id);
                            }
                        }
                    }
                }
            }
        }

        private IEnumerable<DataRow> GetUsersIdToSmsSend(int alertId)
        {
            var alertIdParameter = SqlParameterFactory.Create(DbType.Int32, alertId, "alert_id");
            var sendedSmsUserId = _manager.GetDataByQuery(SelectIdFromSmsSentByAlertId, alertIdParameter).ToList();
            var allUsersId = UserManager.Default.GetUsersId().ToList();
            var usersIdToSend = allUsersId.Where(x => sendedSmsUserId.All(y => x.GetInt("id") != y.GetInt("id")));
            return usersIdToSend;
        }

        private IEnumerable<DataRow> GetUsersIdToEmailSend(int alertId)
        {
            var alertIdParameter = SqlParameterFactory.Create(DbType.Int32, alertId, "alert_id");
            var sendedSmsUserId = _manager.GetDataByQuery(SelectIdFromEmailSentByAlertId, alertIdParameter).ToList();
            var allUsersId = UserManager.Default.GetUsersId().ToList();
            var usersIdToSend = allUsersId.Where(x => sendedSmsUserId.All(y => x.GetInt("id") != y.GetInt("id")));
            return usersIdToSend;
        }

        private void SendToUserSms(UserAlertsDTO userAlerts)
        {
            var userId = GetUserId(userAlerts.UserName);

            foreach (var alert in userAlerts.Alerts)
            {
                if (_cacheManager.IsActiveAlert(alert))
                {
                    var user = UserManager.Default.GetUserById(userId);
                    var alertText = alert.GetClearAlertText();
                    var recievedUsers = GetRecievedSmsUsers(alert.Id, user.Id);
                    if (!recievedUsers.IsEmpty)
                    {
                        var dataRow = recievedUsers.GetRow(0);
                        var smsSentId = dataRow.GetInt("id");
                        if (smsSentId <= 0)
                        {
                            SendSms(alertText, user.Phone, userId, alert.Id);
                        }
                    }
                    else
                    {
                        SendSms(alertText, user.Phone, userId, alert.Id);
                    }
                }
            }
        }

        private void SendToUserEmail(UserAlertsDTO userAlerts)
        {
            var userId = GetUserId(userAlerts.UserName);

            foreach (var alert in userAlerts.Alerts)
            {
                if (_cacheManager.IsActiveAlert(alert))
                {
                    var user = UserManager.Default.GetUserById(userId);
                    var alertText = alert.GetAlertText();
                    var recievedUsers = GetRecievedEmailUsers(alert.Id, user.Id);
                    if (!recievedUsers.IsEmpty)
                    {
                        var dataRow = recievedUsers.GetRow(0);
                        var smsSentId = dataRow.GetInt("id");
                        if (smsSentId <= 0)
                        {
                            SendEmail(alert.Title, alertText, user.Email, userId, alert.Id);
                        }
                    }
                    else
                    {
                        SendEmail(alert.Title, alertText, user.Email, userId, alert.Id);
                    }
                }
            }
        }

        private void SendSms(string text, string phone, int userId, int alertId)
        {
            var smsResult = SmsManager.Default.Send(text, phone);
            if (smsResult == SmsResult.Ok)
            {
                WriteToSmsSend(userId, alertId);
            }
        }

        private void SendEmail(string title, string text, string to, int userId, int alertId)
        {
            var emailResult = EmailManager.Default.Send(title, text, to);
            if (emailResult == EmailResult.Ok)
            {
                WriteToEmailSend(userId, alertId);
            }
        }

        private int GetUserId(string user)
        {
            var userNamePisces = user.Split('.');
            var userName = userNamePisces.Last();

            var domainName = string.Empty;
            if (userNamePisces.Length > 1)
            {
                // 1 - it's length of delimeter betwen user name and domain
                domainName = user.Substring(0, user.Length - userName.Length - 1);
            }

            var userId = string.IsNullOrEmpty(domainName)
                             ? UserManager.Default.GetUserIdByName(userName)
                             : UserManager.Default.GetUserIdByNameAndDomain(userName, domainName);
            return userId;
        }

        private DataSet GetRecievedSmsUsers(int alertId, int userid)
        {
            var alertIdParam = SqlParameterFactory.Create(
                DbType.Int32,
                userid,
                "userId");
            var userIdParam = SqlParameterFactory.Create(
                DbType.Int32,
                alertId,
                "alertId");
            var dataSet = _manager.GetDataByQuery(
                SelectIdFromSmsSentByAlertIdAndUserId,
                alertIdParam,
                userIdParam);
            return dataSet;
        }

        private DataSet GetRecievedEmailUsers(int alertId, int userid)
        {
            var alertIdParam = SqlParameterFactory.Create(
                DbType.Int32,
                userid,
                "userId");
            var userIdParam = SqlParameterFactory.Create(
                DbType.Int32,
                alertId,
                "alertId");
            var dataSet = _manager.GetDataByQuery(
                SelectIdFromEmailSentByAlertIdAndUserId,
                alertIdParam,
                userIdParam);
            return dataSet;
        }

        private void WriteToSmsSend(int userId, int alertid)
        {
            var userIdParameter = SqlParameterFactory.Create(DbType.Int32, userId, "user_id");
            var alertIdParameter = SqlParameterFactory.Create(DbType.Int32, alertid, "alert_id");
            var dateParameter = SqlParameterFactory.Create(DbType.DateTime, DateTime.Now, "date");
            _manager.ExecuteQuery(InsertSmsSentByAlertIdAndUserId, userIdParameter, alertIdParameter, dateParameter);
        }

        private void WriteToEmailSend(int userId, int alertid)
        {
            var userIdParameter = SqlParameterFactory.Create(DbType.Int32, userId, "user_id");
            var alertIdParameter = SqlParameterFactory.Create(DbType.Int32, alertid, "alert_id");
            var dateParameter = SqlParameterFactory.Create(DbType.DateTime, DateTime.Now, "date");
            _manager.ExecuteQuery(InsertEmailSentByAlertIdAndUserId, userIdParameter, alertIdParameter, dateParameter);
        }

        public void Dispose()
        {
            _manager.Dispose();
        }
    }
}