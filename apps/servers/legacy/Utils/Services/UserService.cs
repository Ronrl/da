﻿namespace DeskAlertsDotNet.Utils.Services
{
    using DeskAlerts.ApplicationCore.Entities;
    using DeskAlerts.ApplicationCore.Interfaces;
    using DeskAlerts.ApplicationCore.Repositories;
    using DeskAlerts.ApplicationCore.Utilities;
    using DeskAlertsDotNet.Parameters;
    using DeskAlertsDotNet.Utils.Interfaces;
    using NLog;
    using System;
    using System.Text;

    public class UserService : IUserService
    {
        private IUserRepository _userRepository;

        private IGroupRepository _gruopRepository;

        private IPolicyRepository _policyRepository;

        private Logger _logger = LogManager.GetCurrentClassLogger();

        public UserService()
        {
            var configurationManager = new WebConfigConfigurationManager(Config.Configuration);
            _userRepository = new PpUserRepository(configurationManager);            
            _policyRepository = new PpPolicyRepository(configurationManager);
            _gruopRepository = new PpGroupRepository(configurationManager);
        }

        /// <summary>
        /// Register a new publisher
        /// </summary>
        /// <param name="userName">Name of the user</param>
        /// <param name="domainId">Id of the domain</param>
        /// <param name="password">Password of the user</param>
        /// <returns></returns>
        public long RegisterPublisher(string userName, long domainId, string password)
        {
            var passwordBytes = Encoding.UTF8.GetBytes(password);
            var passwordEncoded = Convert.ToBase64String(passwordBytes);

            var user = new User()
            {
                Username = userName,
                Role = "E",
                RegistrationDateTime = DateTime.Now,
                DomainId = domainId,
                Password = passwordEncoded
            };

            var userId = _userRepository.Create(user);
            _logger.Info($"Publisher {userName} was registred");

            return userId;
        }

        public Group GetGroupById(long groupId)
        {
            return _gruopRepository.GetById(groupId);
        }

        public void UpdateGroup(Group group)
        {
            _gruopRepository.Update(group);
        }

        public void RemovePublisher(User user)
        {
            _userRepository.Delete(user);
        }

        public User GetUserById(long userId)
        {
            return _userRepository.GetById(userId);
        }
    }
}