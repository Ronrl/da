﻿namespace DeskAlertsDotNet.Utils.Interfaces
{
    using System.ServiceModel;

    [ServiceContract]
    public interface IADIService
    {
        [OperationContract]
        string DoWork(string message);
    }
}
