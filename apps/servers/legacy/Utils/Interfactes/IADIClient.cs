﻿namespace DeskAlertsDotNet.Utils.Interfaces
{
    public interface IADIClient
    {
        string DoWork(string message);
    }
}
