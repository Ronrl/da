#include "stdafx.h"
#include "WebSiteInfo.h"
#include <Ahadmin.h>

using namespace std;



std::wstring createBindingString(std::wstring binding, std::wstring type)
{
	wstring ip;
	wstring port;
	wstring host;
	wstring result;
	wstring::size_type pos = binding.find(L"]:");
	if(pos != wstring::npos)
	{
		ip = binding.substr(1,pos-1);
		pos++;
	}
	else
	{
		pos = binding.find(L':');
		ip = binding.substr(0,pos);
	}
	wstring::size_type pos2 = binding.find(L':', pos + 1);
	port = binding.substr(pos+1,pos2-pos-1);
	host = binding.substr(pos2+1);

	result += type;
	result += L',';
	result += ip;
	result += L',';
	result += port;
	result += L',';
	result += host;

	return result;
}

wstring createBindingsString(METADATA_RECORD MyRecord, wstring type)
{
	wstring result;
	wchar_t* data = (wchar_t*)MyRecord.pbMDData;
	wchar_t* token = data;

	for (DWORD i=0; i < MyRecord.dwMDDataLen/sizeof(wchar_t) - 1; i++)
	{
		if (data[i]==L'\0')
		{
			if(!result.empty()) result += L';';
			result += createBindingString(token, type);
			token += i+1;
		}
	}
	return result;
}

wstring GetWebSitesWithDetails()
{
	HRESULT hRes = 0;
	HRESULT hRes1 = 0;
	HRESULT hRes2 = 0;
	DWORD indx = 0;
	METADATA_HANDLE MyHandle;
	METADATA_RECORD MyRecord; 
	WCHAR SubKeyName[METADATA_MAX_NAME_LEN];
	CComPtr <IMSAdminBase> pIMeta;
	BOOL bMatched = FALSE;
	TCHAR CurrKey[METADATA_MAX_NAME_LEN];
	const DWORD dwBufLen = 1024;
	DWORD dwReqBufLen = 0;
	PBYTE pbBuffer = new BYTE[dwBufLen];
	TCHAR szMetabasepath[MAX_PATH];

	wstring result;

	hRes = CoCreateInstance(CLSID_MSAdminBase, NULL, CLSCTX_ALL,
		IID_IMSAdminBase, (void **) &pIMeta);

	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	if (FAILED(hRes)) // IF IIS 7 AND HIGHER
	{
		CComPtr<IAppHostAdminManager> spAppHostAdminManager;
		HRESULT hRes = CoCreateInstance(CLSID_AppHostAdminManager, NULL, CLSCTX_ALL, IID_IAppHostAdminManager, (void **) &spAppHostAdminManager);

		if (FAILED(hRes)) return result;

		IAppHostElement * pParentElem = NULL;
		IAppHostElementCollection *pHostCollection = NULL;
		BSTR bstrSectionName = SysAllocString(L"system.applicationHost/sites");
		BSTR bstrPath = SysAllocString(L"MACHINE/WEBROOT/APPHOST");

		hRes = spAppHostAdminManager->GetAdminSection( bstrSectionName, bstrPath, &pParentElem );

		if (SUCCEEDED(hRes))
		{
			//get websites collection
			hRes = pParentElem->get_Collection(&pHostCollection);
			if (SUCCEEDED(hRes))
			{
				DWORD count;
				pHostCollection->get_Count(&count);

				//enumerate web site collection
				for (DWORD i = 0; i < count; i++)
				{
					wstring siteName;
					wstring bindingsStr;
					wstring physicalPath;

					IAppHostElement * pElem = NULL;
					VARIANT vi;
					VariantInit(&vi);
					vi.intVal=i;
					vi.vt = VT_I2;

					//get web site at index i
					hRes = pHostCollection->get_Item(vi,&pElem);

					if (SUCCEEDED(hRes))
					{
						IAppHostElementCollection *pElemCollection = NULL;

						//get web site at index i elements collection
						hRes = pElem->get_Collection(&pElemCollection);

						if (SUCCEEDED(hRes))
						{
							DWORD elemCount;
							hRes = pElemCollection->get_Count(&elemCount);

							if (SUCCEEDED(hRes))
							{
								//enumerate web site at index i element collection and find element with property "path" == "/"
								for (DWORD elemIndex = 0; elemIndex < elemCount; elemIndex++)
								{
									IAppHostElement * pAppElement;
									VARIANT elemVi;
									VariantInit(&elemVi);
									elemVi.intVal=elemIndex;
									elemVi.vt = VT_I2;

									//get element of web site element collection at index elemVi
									hRes = pElemCollection->get_Item(elemVi,&pAppElement);
									if (SUCCEEDED(hRes))
									{
										IAppHostProperty *appPathProp = NULL;

										//get property "path" of selected element
										hRes = pAppElement->GetPropertyByName(L"path",&appPathProp);
										if (SUCCEEDED(hRes))
										{
											BSTR bstrAppPath;
											//get string value of property "path" of selected element
											hRes = appPathProp->get_StringValue(&bstrAppPath);
											if (SUCCEEDED(hRes))
											{
												wstring appPath = bstrAppPath;
												//if property "path" of selected element == "/"
												if (appPath.compare(L"/") == 0)
												{
													IAppHostElementCollection *pAppCollection;
													// then get application collection of this element
													hRes = pAppElement->get_Collection(&pAppCollection);
													if (SUCCEEDED(hRes))
													{
														DWORD appCount;
														hRes = pAppCollection->get_Count(&appCount);
														if (SUCCEEDED(hRes))
														{
															// enumerate application collection of this element and find application element with property "path" == "/"
															for (DWORD appIndex = 0; appIndex < appCount; appIndex++)
															{
																IAppHostElement * pVirtualDirElement;
																VARIANT appVi;
																VariantInit(&appVi);
																appVi.intVal=appIndex;
																appVi.vt = VT_I2;
																//get element of application collection at index appVi (this is virtual directory element)
																hRes = pAppCollection->get_Item(appVi,&pVirtualDirElement);
																if (SUCCEEDED(hRes))
																{
																	IAppHostProperty *pVirtualDirPathProp = NULL;
																	//get property "path" of virtual directory element
																	hRes = pVirtualDirElement->GetPropertyByName(L"path",&pVirtualDirPathProp);
																	if (SUCCEEDED(hRes))
																	{
																		BSTR bstrVirtualDirPath;
																		//get string value of property "path" of virtual directory element
																		hRes = pVirtualDirPathProp->get_StringValue(&bstrVirtualDirPath);
																		if (SUCCEEDED(hRes))
																		{
																			wstring virtualDirPath = bstrVirtualDirPath;
																			//if property "path" of virtual directory element == "/"
																			if (virtualDirPath.compare(L"/") == 0)
																			{
																				IAppHostProperty *pPhysicalPathProp;
																				// then get property "physicalPath" of virtual directory element
																				hRes = pVirtualDirElement->GetPropertyByName(L"physicalPath",&pPhysicalPathProp);
																				if (SUCCEEDED(hRes))
																				{
																					BSTR bstrPhysicalPath;
																					//get string value of property "physicalPath" of virtual directory element
																					hRes = pPhysicalPathProp->get_StringValue(&bstrPhysicalPath);
																					if (SUCCEEDED(hRes))
																					{
																						physicalPath = bstrPhysicalPath;
																					}
																				}
																				break;
																			}
																		}
																	}
																}
															}
														}
													}
													break;
												}
											}
										}
									}
								}
							}
						}
						// get property "name" of website
						IAppHostProperty* pPropertySiteName = NULL;
						hRes = pElem->GetPropertyByName(L"name",&pPropertySiteName);

						if (SUCCEEDED(hRes))
						{
							BSTR bstrSiteName;
							// get string value of property "name" of website
							pPropertySiteName->get_StringValue(&bstrSiteName);
							siteName = bstrSiteName;
						}

						IAppHostElement * pBindings = NULL;
						// get bindings element of web site
						hRes = pElem->GetElementByName(L"bindings",&pBindings);

						if (SUCCEEDED(hRes))
						{
							IAppHostElementCollection *pBindingsCollection = NULL;
							// get bindings collection
							hRes = pBindings->get_Collection(&pBindingsCollection);
							if (SUCCEEDED(hRes))
							{
								DWORD bindingsCount;
								hRes = pBindingsCollection->get_Count(&bindingsCount);
								if (SUCCEEDED(hRes))
								{
									//enumerate bindings collection
									for (DWORD bindingIndex = 0; bindingIndex < bindingsCount; bindingIndex++)
									{
										IAppHostElement * pBinding = NULL;
										VARIANT bindingVi;
										VariantInit(&bindingVi);
										bindingVi.intVal=bindingIndex;
										bindingVi.vt = VT_I2;
										// get binding element of bindings collection at index bindingIndex
										hRes = pBindingsCollection->get_Item(bindingVi,&pBinding);
										if (SUCCEEDED(hRes))
										{
											IAppHostProperty* pPropertyBindingInformation = NULL;
											// get property "bindingInformation" of binding element
											hRes = pBinding->GetPropertyByName(L"bindingInformation",&pPropertyBindingInformation);

											if (SUCCEEDED(hRes))
											{
												BSTR bstrBindingInformation;
												// get string value of property "bindingInformation" of binding element
												hRes = pPropertyBindingInformation->get_StringValue(&bstrBindingInformation);
												if (SUCCEEDED(hRes))
												{
													IAppHostProperty* pPropertyBindingProtocol = NULL;
													// get property "protocol" of binding element
													hRes = pBinding->GetPropertyByName(L"protocol",&pPropertyBindingProtocol);
													if (SUCCEEDED(hRes))
													{
														BSTR bstrBindingProtocol;
														// get string value of property "protocol" of binding element
														hRes = pPropertyBindingProtocol->get_StringValue(&bstrBindingProtocol);
														if (SUCCEEDED(hRes))
														{
															//add to bindings string current binding information
															if(!bindingsStr.empty()) bindingsStr += L';';
															bindingsStr += createBindingString(bstrBindingInformation,bstrBindingProtocol);
														}
													}
												}
											}	
										}
									}
								}
							}
						}
					}
					result += L"<";
					result += siteName;
					result += L"|";
					result += physicalPath;
					result += L"|";
					result += bindingsStr;
					result += L">";
				}
			}
		}
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////
	else // IF IIS 6 AND LOWER
	{
		//get a handle to the web service
		hRes = pIMeta->OpenKey(METADATA_MASTER_ROOT_HANDLE, TEXT("/LM"),
			METADATA_PERMISSION_READ, 20, &MyHandle);

		if (FAILED(hRes))
		{
			wprintf(TEXT("OpenKey failed: %d (0x%08x)"),
				hRes,
				hRes
				);
			return FALSE;
		}

		//loop until there are no more subkeys
		while (SUCCEEDED(hRes))
		{
			//enumerate the subkeys of the World Wide Web service
			hRes = pIMeta->EnumKeys(MyHandle, TEXT("/W3SVC"), SubKeyName, indx);
			if (SUCCEEDED(hRes))
			{
				//initialize the input structure,
				//the values specify what kind of data to retrieve
				MyRecord.dwMDAttributes = METADATA_NO_ATTRIBUTES;
				MyRecord.dwMDUserType = IIS_MD_UT_SERVER;
				MyRecord.dwMDDataType = ALL_METADATA;
				MyRecord.dwMDDataLen = 0;
				MyRecord.pbMDData = NULL; 
				MyRecord.dwMDIdentifier = MD_KEY_TYPE; // KeyType 
				wsprintf(CurrKey, TEXT("/W3SVC/%s"), SubKeyName);

				hRes1 = pIMeta->GetData(MyHandle, CurrKey, &MyRecord, &dwReqBufLen);

				MyRecord.dwMDDataLen = dwReqBufLen;
				MyRecord.pbMDData = new unsigned char[dwReqBufLen]; 

				hRes1 = pIMeta->GetData(MyHandle, CurrKey, &MyRecord, &dwReqBufLen);
				if (!hRes1 && (!wcscmp((TCHAR *)MyRecord.pbMDData, TEXT("IIsWebServer")))) {

					wstring siteName;
					wstring bindingsStr;
					wstring physicalPath;

					MyRecord.dwMDAttributes = METADATA_NO_ATTRIBUTES;
					MyRecord.dwMDUserType = IIS_MD_UT_SERVER;
					MyRecord.dwMDDataType = ALL_METADATA;
					MyRecord.dwMDDataLen = 0;
					MyRecord.pbMDData = NULL; 
					MyRecord.dwMDIdentifier = MD_SERVER_COMMENT;

					hRes1 = pIMeta->GetData(MyHandle, CurrKey, &MyRecord, &dwReqBufLen);

					MyRecord.dwMDDataLen = dwReqBufLen;
					MyRecord.pbMDData = new unsigned char[dwReqBufLen]; 

					hRes1 = pIMeta->GetData(MyHandle, CurrKey, &MyRecord, &dwReqBufLen);

					if (!hRes1)
					{
						wsprintf(szMetabasepath, TEXT("/LM%s %s"), CurrKey, (TCHAR*)MyRecord.pbMDData);
						siteName = (wchar_t*)MyRecord.pbMDData;
					}

					MyRecord.dwMDAttributes = METADATA_NO_ATTRIBUTES;
					MyRecord.dwMDUserType = IIS_MD_UT_SERVER;
					MyRecord.dwMDDataType = ALL_METADATA;
					MyRecord.dwMDDataLen = 0;
					MyRecord.pbMDData = NULL; 
					MyRecord.dwMDIdentifier = MD_SERVER_BINDINGS;

					hRes1 = pIMeta->GetData(MyHandle, CurrKey, &MyRecord, &dwReqBufLen);

					MyRecord.dwMDDataLen = dwReqBufLen;
					MyRecord.pbMDData = new unsigned char[dwReqBufLen]; 
					hRes1 = pIMeta->GetData(MyHandle, CurrKey, &MyRecord, &dwReqBufLen);
					if (!hRes1)
					{
						bindingsStr = createBindingsString(MyRecord,L"http");

					}

					MyRecord.dwMDAttributes = METADATA_NO_ATTRIBUTES;
					MyRecord.dwMDUserType = IIS_MD_UT_SERVER;
					MyRecord.dwMDDataType = ALL_METADATA;
					MyRecord.dwMDDataLen = 0;
					MyRecord.pbMDData = NULL; 
					MyRecord.dwMDIdentifier = MD_SECURE_BINDINGS;

					hRes1 = pIMeta->GetData(MyHandle, CurrKey, &MyRecord, &dwReqBufLen);

					MyRecord.dwMDDataLen = dwReqBufLen;
					MyRecord.pbMDData = new unsigned char[dwReqBufLen]; 

					hRes1 = pIMeta->GetData(MyHandle, CurrKey, &MyRecord, &dwReqBufLen);
					if (!hRes1)
					{
						wstring secureBindingsStr = createBindingsString(MyRecord,L"https");
						if (!secureBindingsStr.empty())
						{
							bindingsStr += L';';
							bindingsStr += secureBindingsStr;
						}
					}


					MyRecord.dwMDAttributes = METADATA_INHERIT;
					MyRecord.dwMDUserType = IIS_MD_UT_FILE;
					MyRecord.dwMDDataType = STRING_METADATA;
					MyRecord.dwMDDataLen = 0;
					MyRecord.pbMDData = NULL; 
					MyRecord.dwMDIdentifier = MD_VR_PATH;
					MyRecord.dwMDDataTag = 0;

					wsprintf(CurrKey, L"%s/%s",CurrKey, L"ROOT");

					hRes1 = pIMeta->GetData(MyHandle, CurrKey, &MyRecord, &dwReqBufLen);

					MyRecord.dwMDDataLen = dwReqBufLen;
					MyRecord.pbMDData = new unsigned char[dwReqBufLen]; 

					hRes1 = pIMeta->GetData(MyHandle, CurrKey, &MyRecord, &dwReqBufLen);
					if (!hRes1)
					{
						physicalPath = (wchar_t*)MyRecord.pbMDData;							
					}
					result += L"<";
					result += siteName;
					result += L"|";
					result += physicalPath;
					result += L"|";
					result += bindingsStr;
					result += L">";
				}
			}
			//increment the index
			indx++;
		}
		pIMeta->CloseKey(MyHandle);
		delete pbBuffer;
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////

	return result;
}

#define STATUS_STRING_SIZE 2048

extern "C" ENCRYPTPLUGIN_API void GetWebSitesWithDetails( HWND hwndParent, int string_size, 
												   char *variables, stack_t **stacktop)
{

	EXDLL_INIT();

	std::wstring tempNameW;
	
	wstring res;

	TCHAR szStatus[STATUS_STRING_SIZE];
	HRESULT hcores = S_OK;


	// initialize COM
	hcores = CoInitialize(NULL);

	if (SUCCEEDED(hcores))
	{
		res = GetWebSitesWithDetails();
	}

	CoUninitialize();

	pushstring( CW2A(res.c_str()));
	

}