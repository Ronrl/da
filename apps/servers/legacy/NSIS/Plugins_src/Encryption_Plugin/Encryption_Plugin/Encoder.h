#pragma once
#include <atlstr.h>


class Encoder
{
public:
	Encoder(void);
	~Encoder(void);
	static CString encodeURIComponent(CString sIn, const int encode = CP_UTF8);
	static CString URLEncode(CString sIn, LPCTSTR exclude, const int encode = CP_UTF8);
};
