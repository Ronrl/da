#pragma once

#include "stdafx.h"

class IProgressObserver
{
public:
	virtual ~IProgressObserver() {}
	virtual void OnProgress(unsigned char) = 0;
	virtual void Finished(HRESULT) = 0;
	virtual DWORD Send(DWORD dwMessage, LPVOID pData, DWORD dwDataLength) = 0;
};