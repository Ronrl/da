#pragma once

#include "stdafx.h"

#define MMIO_SIZE           65536

#define MMIO_V40            0
#define MMIO_V45            1

#define MMIO_MESSAGE(version, defaultResponse, messageCode) \
    ((((DWORD)version & 0xFF) << 24) | (((DWORD)defaultResponse & 0xFF) << 16) | ((DWORD)messageCode & 0xFFFF))
#define MMIO_MESSAGE_CODE(messageId) \
    (messageId & 0xFFFF)
#define MMIO_MESSAGE_DEFAULT_RESONSE(messageId) \
    ((messageId >> 16) & 0xFF)
#define MMIO_MESSAGE_VERSION(messageId) \
    ((messageId >>24) & 0xFF)

#define MMIO_NO_MESSAGE    0

#define MMIO_CLOSE_APPS    MMIO_MESSAGE(MMIO_V45, IDNO, 1)

struct MmioApplication
{
	WCHAR m_szName[MAX_PATH];
	DWORD m_dwPid;
};

struct MmioCloseApplications
{
	DWORD m_dwApplicationsSize;
	MmioApplication m_applications[1];
};
