#include "stdafx.h"
#include "resource.h"
#include "Server.h"
#include "dismapi.h"

#ifndef SM_TABLETPC
	#define SM_TABLETPC		86
#endif

#ifndef SM_MEDIACENTER
	#define SM_MEDIACENTER	87
#endif

#define CountOf(x) sizeof(x)/sizeof(*x)

#define BUFSIZE 8192

HANDLE g_hChildStd_OUT_Rd = nullptr;
HANDLE g_hChildStd_OUT_Wr = nullptr;

PROCESS_INFORMATION piProcInfo;
STARTUPINFO siStartInfo;

const TCHAR* g_szNetfx10RegKeyName = _T("Software\\Microsoft\\.NETFramework\\Policy\\v1.0");
const TCHAR* g_szNetfx10RegKeyValue = _T("3705");
const TCHAR* g_szNetfx10SPxMSIRegKeyName = _T("Software\\Microsoft\\Active Setup\\Installed Components\\{78705f0d-e8db-4b2d-8193-982bdda15ecd}");
const TCHAR* g_szNetfx10SPxOCMRegKeyName = _T("Software\\Microsoft\\Active Setup\\Installed Components\\{FDC11A6F-17D1-48f9-9EA3-9051954BAA24}");
const TCHAR* g_szNetfx11RegKeyName = _T("Software\\Microsoft\\NET Framework Setup\\NDP\\v1.1.4322");
const TCHAR* g_szNetfx20RegKeyName = _T("Software\\Microsoft\\NET Framework Setup\\NDP\\v2.0.50727");
const TCHAR* g_szNetfx30RegKeyName = _T("Software\\Microsoft\\NET Framework Setup\\NDP\\v3.0\\Setup");
const TCHAR* g_szNetfx30SpRegKeyName = _T("Software\\Microsoft\\NET Framework Setup\\NDP\\v3.0");
const TCHAR* g_szNetfx30RegValueName = _T("InstallSuccess");
const TCHAR* g_szNetfx35RegKeyName = _T("Software\\Microsoft\\NET Framework Setup\\NDP\\v3.5");
const TCHAR* g_szNetfx40ClientRegKeyName = _T("Software\\Microsoft\\NET Framework Setup\\NDP\\v4\\Client");
const TCHAR* g_szNetfx40FullRegKeyName = _T("Software\\Microsoft\\NET Framework Setup\\NDP\\v4\\Full");
const TCHAR* g_szNetfx40SPxRegValueName = _T("Servicing");
const TCHAR* g_szNetfx45RegKeyName = _T("Software\\Microsoft\\NET Framework Setup\\NDP\\v4\\Full");
const TCHAR* g_szNetfx45RegValueName = _T("Release");
const TCHAR* g_szNetfxStandardRegValueName = _T("Install");
const TCHAR* g_szNetfxStandardSPxRegValueName = _T("SP");
const TCHAR* g_szNetfxStandardVersionRegValueName = _T("Version");

const TCHAR* g_szNetfx10VersionString = _T("v1.0.3705");
const TCHAR* g_szNetfx11VersionString = _T("v1.1.4322");
const TCHAR* g_szNetfx20VersionString = _T("v2.0.50727");
const TCHAR* g_szNetfx40VersionString = _T("v4.0.30319");

const int g_iNetfx30VersionMajor = 3;
const int g_iNetfx30VersionMinor = 0;
const int g_iNetfx30VersionBuild = 4506;
const int g_iNetfx30VersionRevision = 26;
const int g_iNetfx35VersionMajor = 3;
const int g_iNetfx35VersionMinor = 5;
const int g_iNetfx35VersionBuild = 21022;
const int g_iNetfx35VersionRevision = 8;
const int g_iNetfx40VersionMajor = 4;
const int g_iNetfx40VersionMinor = 0;
const int g_iNetfx40VersionBuild = 30319;
const int g_iNetfx40VersionRevision = 0;
const int g_dwNetfx45ReleaseVersion = 378389;
const int g_dwNetfx451ReleaseVersion = 378675;
const int g_dwNetfx452ReleaseVersion = 379893;
const int g_dwNetfx46ReleaseVersion = 393295;
const int g_dwNetfx461ReleaseVersion = 394254;
const int g_dwNetfx462ReleaseVersion = 394802;

std::vector<std::wstring> g_vecFeatures;

bool CheckNetfxBuildNumber(const TCHAR*, const TCHAR*, const int, const int, const int, const int);
bool CheckNetfxVersionUsingMscoree(const TCHAR*);
void FillFeaturesList();
int GetNetfx10SPLevel();
int GetNetfxSPLevel(const TCHAR*, const TCHAR*);
DWORD GetProcessorArchitectureFlag();
bool IsCurrentOSTabletMedCenter();
bool IsNetfx10Installed();
bool IsNetfx11Installed();
bool IsNetfx20Installed();
bool IsNetfx30Installed();
bool IsNetfx35Installed();
bool IsNetfx40ClientInstalled();
bool IsNetfx40FullInstalled();
bool IsNetfx45Installed();
bool IsNetfx451Installed();
bool IsNetfx452Installed();
bool IsNetfx46Installed();
bool IsNetfx461Installed();
bool IsNetfx462Installed();
bool RegistryGetValue(HKEY, const TCHAR*, const TCHAR*, DWORD, LPBYTE, DWORD);
void WriteMessageToScreenAndLog(char*, bool, bool);
bool UnpackExeFromResource(int, LPWSTR);
bool RunProcess(LPCWSTR, LPCWSTR, BOOL);
BOOL ErrorHappened(CHAR*);
BOOL WINAPI ConsoleHandler(DWORD);
bool InstallFramework();
bool SetupPipes();
bool InstallWs2008SpecificFeatures();
bool InstallIisFeaturesOnWs2008();
bool InstallAspNetFeaturesOnWs2008();
bool RegisterAspNet();
void DismCleanup(DismString*, DismSession&);
bool InstallIisFeatures(DismSession&);
bool InstallAspNetFeatures(DismSession&);

int main()
{
	HWND hwnd = GetConsoleWindow();
	HMENU hmenu = GetSystemMenu(hwnd, FALSE);
	EnableMenuItem(hmenu, SC_CLOSE, MF_GRAYED);

	PROCESSENTRY32 entry;
	entry.dwSize = sizeof(PROCESSENTRY32);
	WCHAR szFileName[MAX_PATH + 1];

	GetModuleFileName(nullptr, szFileName, MAX_PATH + 1);
	
	HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);

	if (Process32First(snapshot, &entry) == TRUE)
	{
		int processCount = 0;
		while (Process32Next(snapshot, &entry) == TRUE)
		{			
			if (wcsstr(szFileName, entry.szExeFile) != 0 && wcsstr(entry.szExeFile, L".exe") != 0)
			{
				processCount++;
			}
			if (processCount > 1)
			{
				CloseHandle(snapshot);
				return 0;
			}
		}
	}	
	
	ULARGE_INTEGER dwFreeBytesAvailable;
	ULARGE_INTEGER dwTotalNumberOfBytes;
	ULARGE_INTEGER dwTotalNumberOfFreeBytes;

	SetConsoleCtrlHandler(ConsoleHandler, TRUE);

	GetDiskFreeSpaceEx(L"c:\\", &dwFreeBytesAvailable, &dwTotalNumberOfBytes, &dwTotalNumberOfFreeBytes);
	auto freeSpace = double(dwTotalNumberOfFreeBytes.QuadPart) / 1024 / 1024;
	if (freeSpace < 1500.0)
	{
		MessageBox(nullptr, L"Not enought free disk space. It is required around 1.5Gb of free disk space", L"Error", MB_OK);		
		return 0;
	}

	WriteMessageToScreenAndLog("DeskAlerts is installing", false, false);

	FillFeaturesList();

	if (!IsNetfx45Installed() && !IsNetfx451Installed() && !IsNetfx452Installed() &&
		!IsNetfx46Installed() && !IsNetfx461Installed() && !IsNetfx462Installed())
	{
		if (!InstallFramework())
		{
			return 0;
		}
	}

	if (!SetupPipes()) 
	{
		return 0;
	}

	std::wstring strLaunchString;

	if (!IsWindows8OrGreater())
	{
		WriteMessageToScreenAndLog("Starting IIS features installation process.", false, false);

		if (!InstallWs2008SpecificFeatures()) 
		{
			return 0;
		}

		if (!InstallIisFeaturesOnWs2008())
		{
			return 0;
		}

		WriteMessageToScreenAndLog("Starting ASP.NET features installation process.", false, false);

		if (!InstallAspNetFeaturesOnWs2008())
		{
			return 0;
		}

		WriteMessageToScreenAndLog("Old IIS detected. Starting ASP.NET registration process.", false, false);

		if (!RegisterAspNet())
		{
			return 0;
		}
	}
	else
	{
		WriteMessageToScreenAndLog("Starting IIS features installation process.", false, false);

		DismSession session = DISM_SESSION_DEFAULT;
		DismString *pErrorString = nullptr;
		HRESULT hr;

		hr = DismInitialize(DismLogErrorsWarningsInfo, nullptr, nullptr);
		if (FAILED(hr))
		{
			DismCleanup(pErrorString, session);
			WriteMessageToScreenAndLog("Failed to launch DISM API", true, true);
			return 0;
		}

		hr = DismOpenSession(DISM_ONLINE_IMAGE, nullptr, nullptr, &session);
		if (FAILED(hr))
		{
			WriteMessageToScreenAndLog("Failed to launch DISM API", true, true);
			DismCleanup(pErrorString, session);
			return 0;
		}
		
		if (!InstallIisFeatures(session))
		{
			return 0;
		}

		WriteMessageToScreenAndLog("Starting ASP.NET features installation process.", false, false);

		if (!InstallAspNetFeatures(session)) 
		{
			return 0;
		}

		DismCleanup(pErrorString, session);
	}

	WriteMessageToScreenAndLog("Unpacking DeskAlerts installer", false, false);

	TCHAR lpTempPathBuffer[MAX_PATH];
	GetTempPath(MAX_PATH, lpTempPathBuffer);

	// ReSharper disable once CppDeprecatedEntity
	_stprintf(lpTempPathBuffer, _T("%s%s"), lpTempPathBuffer, _T("DeskAlerts.Server.exe"));

	if (!UnpackExeFromResource(IDR_EXE_SERVER, lpTempPathBuffer))
	{
		return 0;
	}

	WriteMessageToScreenAndLog("Starting DeskAlerts installer", false, false);

	if (!RunProcess(lpTempPathBuffer, nullptr, FALSE))
	{
		return 0;
	}

	return 0;
}


void WriteMessageToScreenAndLog(char* pMsg, bool bIsError, bool bOpenLog = false)
{
	auto errorMessageID = GetLastError();
	LPSTR messageBuffer = nullptr;
	FormatMessageA(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
		nullptr, errorMessageID, MAKELANGID(LANG_ENGLISH, SUBLANG_DEFAULT), LPSTR(&messageBuffer), 0, nullptr);

	auto t = std::time(nullptr);
	// ReSharper disable CppDeprecatedEntity
	auto tm = *std::localtime(&t);
	// ReSharper restore CppDeprecatedEntity
	std::ofstream ofs("install.log", std::ofstream::app);
	if (bIsError)
	{
		ofs << "[" << std::put_time(&tm, "%d-%m-%Y %H-%M-%S") << "]" << " [E] " << pMsg << ". " << messageBuffer << std::endl;
		std::cout << "[" << std::put_time(&tm, "%d-%m-%Y %H-%M-%S") << "]" << " [E] " << pMsg << ". " << messageBuffer << std::endl;
	}
	else
	{
		ofs << "[" << std::put_time(&tm, "%d-%m-%Y %H-%M-%S") << "]" << " [I] " << pMsg << std::endl;
		std::cout << "[" << std::put_time(&tm, "%d-%m-%Y %H-%M-%S") << "]" << " [I] " << pMsg << std::endl;
	}
	ofs.close();
	LocalFree(messageBuffer);
	if (bOpenLog)
	{
		STARTUPINFO si = { 0 };
		si.cb = sizeof(si);
		PROCESS_INFORMATION pi = { nullptr };

		CreateProcess(L"c:\\windows\\notepad.exe", L" install.log", nullptr, nullptr, FALSE, 0, nullptr, nullptr, &si, &pi);

		CloseHandle(pi.hProcess);
		CloseHandle(pi.hThread);
	}
}

BOOL WINAPI ConsoleHandler(DWORD dwCtrlEvent)
{
	switch (dwCtrlEvent)
	{
	case CTRL_SHUTDOWN_EVENT:
	case CTRL_LOGOFF_EVENT:
	case CTRL_CLOSE_EVENT:
	{
		HANDLE hProcess = OpenProcess(PROCESS_TERMINATE, 0, piProcInfo.dwProcessId);
		if (hProcess != nullptr)
		{
			TerminateProcess(hProcess, 9);
			CloseHandle(hProcess);
		}
		return TRUE;
	}
	default:
		return FALSE;
	}
}

BOOL ErrorHappened(CHAR *chBuf)
{
	DWORD dwRead;
	BOOL bSuccess;

	for (;;)
	{
		bSuccess = ReadFile(g_hChildStd_OUT_Rd, chBuf, BUFSIZE, &dwRead, nullptr);
		chBuf[dwRead] = 0;
		if (!bSuccess || dwRead == 0)
		{
			return TRUE;
		}
		if (bSuccess && dwRead <= BUFSIZE)
		{
			if (strstr(chBuf, "Error") == nullptr)
			{
				return FALSE;
			}
			return TRUE;
		}
	}
}

bool UnpackExeFromResource(int resourceId, LPWSTR lpFileName)
{
	HRSRC hrSrc;
	HGLOBAL hGlobal;
	BYTE *pExeResource;
	HANDLE hFile;
	DWORD dSize;

	hrSrc = FindResource(nullptr, LPCWSTR(resourceId), RT_RCDATA);
	if (hrSrc == nullptr)
	{
		WriteMessageToScreenAndLog("Couldn't unpack (Resource search)", true, true);
		return false;
	}

	dSize = SizeofResource(nullptr, hrSrc);
	if (dSize == 0)
	{
		WriteMessageToScreenAndLog("Couldn't unpack (Size calculation)", true, true);
		return false;
	}

	hGlobal = LoadResource(nullptr, hrSrc);
	if (hGlobal == nullptr)
	{
		WriteMessageToScreenAndLog("Couldn't unpack (Resource loading)", true, true);
		return false;
	}

	pExeResource = static_cast<BYTE*>(LockResource(hGlobal));
	if (pExeResource == nullptr)
	{
		WriteMessageToScreenAndLog("Couldn't unpack (Casting to bytes)", true, true);
		return false;
	}

	hFile = CreateFile(lpFileName, GENERIC_WRITE | GENERIC_READ, 0, nullptr, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, nullptr);
	if (hFile != INVALID_HANDLE_VALUE)
	{
		DWORD bytesWritten = 0;
		WriteFile(hFile, pExeResource, dSize, &bytesWritten, nullptr);
		CloseHandle(hFile);
		return true;
	}

	WriteMessageToScreenAndLog("Couldn't unpack (File saving)", true, true);
	return false;
}

bool InstallFramework()
{
	WriteMessageToScreenAndLog(".NET Framework was not detected. Unpacking installer", false, false);

	TCHAR lpTempPathBuffer[MAX_PATH];
	GetTempPath(MAX_PATH, lpTempPathBuffer);

	// ReSharper disable once CppDeprecatedEntity
	_stprintf(lpTempPathBuffer, _T("%s%s"), lpTempPathBuffer, _T("NDP452.exe"));

	if (!UnpackExeFromResource(IDR_EXE_FRAMEWORK, lpTempPathBuffer))
	{
		return false;
	}

	// ReSharper disable once CppDeprecatedEntity
	_stprintf(lpTempPathBuffer, _T("%s%s"), lpTempPathBuffer, _T(" /q /norestart /pipe DeskAlertsSection"));

	WriteMessageToScreenAndLog("Starting .NET Framework installation process", false, false);

	auto server = Server();
	server.Launch(lpTempPathBuffer);

	if (!IsNetfx45Installed() && !IsNetfx451Installed() && !IsNetfx452Installed() &&
		!IsNetfx46Installed() && !IsNetfx461Installed() && !IsNetfx462Installed())
	{
		WriteMessageToScreenAndLog(".NET Framework was not installed", true, true);
		return false;
	}

	if (server.m_hrInternalResult != 0)
	{
		WriteMessageToScreenAndLog(".NET Framework was not installed. There was an error in installation process", true, true);
		return false;
	}
	return true;
}

bool SetupPipes()
{
	SECURITY_ATTRIBUTES saAttr;
	saAttr.nLength = sizeof(SECURITY_ATTRIBUTES);
	saAttr.bInheritHandle = TRUE;
	saAttr.lpSecurityDescriptor = nullptr;

	if (!CreatePipe(&g_hChildStd_OUT_Rd, &g_hChildStd_OUT_Wr, &saAttr, 0))
	{
		WriteMessageToScreenAndLog("Failed to launch DISM (Creating pipe)", true, true);
		return false;
	}

	if (!SetHandleInformation(g_hChildStd_OUT_Rd, HANDLE_FLAG_INHERIT, 0))
	{
		WriteMessageToScreenAndLog("Failed to launch DISM (Handle information)", true, true);
		return false;
	}

	ZeroMemory(&piProcInfo, sizeof(PROCESS_INFORMATION));

	ZeroMemory(&siStartInfo, sizeof(STARTUPINFO));
	siStartInfo.cb = sizeof(STARTUPINFO);
	siStartInfo.hStdError = g_hChildStd_OUT_Wr;
	siStartInfo.hStdOutput = g_hChildStd_OUT_Wr;
	siStartInfo.dwFlags |= STARTF_USESTDHANDLES;
	return true;
}

bool InstallWs2008SpecificFeatures()
{
	std::wstring strLaunchString;
	LPWSTR arg;
	CHAR chBuf[BUFSIZE];

	strLaunchString = _T("DISM /English /NoRestart /Online /Enable-Feature /FeatureName:NetFx3");
	arg = new wchar_t[strLaunchString.size() + 1];
	copy(strLaunchString.begin(), strLaunchString.end(), arg);
	arg[strLaunchString.size()] = 0;

	if (!CreateProcess(nullptr, arg, nullptr, nullptr, TRUE, 0, nullptr, nullptr, &siStartInfo, &piProcInfo))
	{
		WriteMessageToScreenAndLog("Failed to launch DISM (Creating process)", true, true);
		return false;
	}
	WaitForSingleObject(piProcInfo.hProcess, INFINITE);
	CloseHandle(piProcInfo.hProcess);
	CloseHandle(piProcInfo.hThread);

	ErrorHappened(chBuf);


	strLaunchString = _T("DISM /English /NoRestart /Online /Enable-Feature /FeatureName:NetFx3ServerFeatures");
	arg = new wchar_t[strLaunchString.size() + 1];
	copy(strLaunchString.begin(), strLaunchString.end(), arg);
	arg[strLaunchString.size()] = 0;

	if (!CreateProcess(nullptr, arg, nullptr, nullptr, TRUE, 0, nullptr, nullptr, &siStartInfo, &piProcInfo))
	{
		WriteMessageToScreenAndLog("Failed to launch DISM (Creating process)", true, true);
		return false;
	}
	WaitForSingleObject(piProcInfo.hProcess, INFINITE);
	CloseHandle(piProcInfo.hProcess);
	CloseHandle(piProcInfo.hThread);

	ErrorHappened(chBuf);


	strLaunchString = _T("DISM /English /NoRestart /Online /Enable-Feature /FeatureName:NetFx4Extended-ASPNET45");
	arg = new wchar_t[strLaunchString.size() + 1];
	copy(strLaunchString.begin(), strLaunchString.end(), arg);
	arg[strLaunchString.size()] = 0;

	if (!CreateProcess(nullptr, arg, nullptr, nullptr, TRUE, 0, nullptr, nullptr, &siStartInfo, &piProcInfo))
	{
		WriteMessageToScreenAndLog("Failed to launch DISM (Creating process)", true, true);
		return false;
	}
	WaitForSingleObject(piProcInfo.hProcess, INFINITE);
	CloseHandle(piProcInfo.hProcess);
	CloseHandle(piProcInfo.hThread);

	ErrorHappened(chBuf);


	strLaunchString = _T("DISM /English /NoRestart /Online /Enable-Feature /FeatureName:IIS-NetFxExtensibility45");
	arg = new wchar_t[strLaunchString.size() + 1];
	copy(strLaunchString.begin(), strLaunchString.end(), arg);
	arg[strLaunchString.size()] = 0;

	if (!CreateProcess(nullptr, arg, nullptr, nullptr, TRUE, 0, nullptr, nullptr, &siStartInfo, &piProcInfo))
	{
		WriteMessageToScreenAndLog("Failed to launch DISM (Creating process)", true, true);
		return false;
	}
	WaitForSingleObject(piProcInfo.hProcess, INFINITE);
	CloseHandle(piProcInfo.hProcess);
	CloseHandle(piProcInfo.hThread);

	ErrorHappened(chBuf);


	return true;
}

bool InstallIisFeaturesOnWs2008()
{
	std::wstring strLaunchString;
	LPWSTR arg;
	CHAR chBuf[BUFSIZE];

	strLaunchString = _T("DISM /English /NoRestart /Online /Enable-Feature ");
	for (auto feature : g_vecFeatures)
	{
		strLaunchString += _T("/FeatureName:") + feature + _T(" ");
	}
	arg = new wchar_t[strLaunchString.size() + 1];
	copy(strLaunchString.begin(), strLaunchString.end(), arg);
	arg[strLaunchString.size()] = 0;

	if (!CreateProcess(nullptr, arg, nullptr, nullptr, TRUE, 0, nullptr, nullptr, &siStartInfo, &piProcInfo))
	{
		WriteMessageToScreenAndLog("Failed to launch DISM (Creating process)", true, true);
		return false;
	}
	WaitForSingleObject(piProcInfo.hProcess, INFINITE);
	CloseHandle(piProcInfo.hProcess);
	CloseHandle(piProcInfo.hThread);

	if (ErrorHappened(chBuf))
	{
		WriteMessageToScreenAndLog("Couldn't install IIS-Feautures", true, false);
		WriteMessageToScreenAndLog(chBuf, true, true);
		return false;
	}
	return true;
}

bool InstallAspNetFeaturesOnWs2008()
{
	std::wstring strLaunchString;
	LPWSTR arg;
	CHAR chBuf[BUFSIZE];

	strLaunchString = _T("DISM /English /NoRestart /Online /Enable-Feature /FeatureName:IIS-ASPNET");
	arg = new wchar_t[strLaunchString.size() + 1];
	copy(strLaunchString.begin(), strLaunchString.end(), arg);
	arg[strLaunchString.size()] = 0;

	if (!CreateProcess(nullptr, arg, nullptr, nullptr, TRUE, 0, nullptr, nullptr, &siStartInfo, &piProcInfo))
	{
		WriteMessageToScreenAndLog("Failed to launch DISM (Creating process)", true, true);
		return false;
	}
	WaitForSingleObject(piProcInfo.hProcess, INFINITE);
	CloseHandle(piProcInfo.hProcess);
	CloseHandle(piProcInfo.hThread);

	BOOL bAspNetNotInstalled = ErrorHappened(chBuf);

	strLaunchString = _T("DISM /English /NoRestart /Online /Enable-Feature /FeatureName:IIS-ASPNET45");
	arg = new wchar_t[strLaunchString.size() + 1];
	copy(strLaunchString.begin(), strLaunchString.end(), arg);
	arg[strLaunchString.size()] = 0;

	if (!CreateProcess(nullptr, arg, nullptr, nullptr, TRUE, 0, nullptr, nullptr, &siStartInfo, &piProcInfo))
	{
		WriteMessageToScreenAndLog("Failed to launch DISM (Creating process)", true, true);
		return false;
	}
	WaitForSingleObject(piProcInfo.hProcess, INFINITE);
	CloseHandle(piProcInfo.hProcess);
	CloseHandle(piProcInfo.hThread);

	BOOL bAspNet45NotInstalled = ErrorHappened(chBuf);

	strLaunchString = _T("DISM /English /NoRestart /Online /Enable-Feature /FeatureName:IIS-ASPNET46");
	arg = new wchar_t[strLaunchString.size() + 1];
	copy(strLaunchString.begin(), strLaunchString.end(), arg);
	arg[strLaunchString.size()] = 0;

	if (!CreateProcess(nullptr, arg, nullptr, nullptr, TRUE, 0, nullptr, nullptr, &siStartInfo, &piProcInfo))
	{
		WriteMessageToScreenAndLog("Failed to launch DISM (Creating process)", true, true);
		return false;
	}
	WaitForSingleObject(piProcInfo.hProcess, INFINITE);
	CloseHandle(piProcInfo.hProcess);
	CloseHandle(piProcInfo.hThread);

	BOOL bAspNet46NotInstalled = ErrorHappened(chBuf);

	if (bAspNetNotInstalled && bAspNet45NotInstalled && bAspNet46NotInstalled)
	{
		WriteMessageToScreenAndLog("Failed to install ASP.NET features", true, true);
		return false;
	}
	return true;
}

bool RegisterAspNet()
{
	std::wstring strLaunchString;
	LPWSTR arg;
	strLaunchString = _T("C:\\Windows\\Microsoft.NET\\Framework\\v4.0.30319\\aspnet_regiis.exe -i");
	arg = new wchar_t[strLaunchString.size() + 1];
	copy(strLaunchString.begin(), strLaunchString.end(), arg);
	arg[strLaunchString.size()] = 0;

	if (!CreateProcess(nullptr, arg, nullptr, nullptr, TRUE, 0, nullptr, nullptr, &siStartInfo, &piProcInfo))
	{
		WriteMessageToScreenAndLog("Couldn't start ASP.NET registration process", true, true);
		return false;
	}

	WaitForSingleObject(piProcInfo.hProcess, INFINITE);

	CloseHandle(piProcInfo.hProcess);
	CloseHandle(piProcInfo.hThread);
	return true;
}

void DismCleanup(DismString *pErrorString, DismSession &session)
{
	HRESULT hr;
	hr = DismDelete(pErrorString);
	if (FAILED(hr))
	{
	}

	hr = DismCloseSession(session);
	if (FAILED(hr))
	{
	}

	hr = DismShutdown();
	if (FAILED(hr))
	{
	}

}

bool InstallIisFeatures(DismSession &session)
{
	HRESULT hr;
	DismString* pErrorString;

	for (auto feature : g_vecFeatures)
	{
		auto featureName = new wchar_t[feature.size() + 1];
		copy(feature.begin(), feature.end(), featureName);
		featureName[feature.size()] = 0;
		hr = DismEnableFeature(session, featureName, nullptr, DismPackageNone, TRUE, nullptr, 0, TRUE, nullptr, nullptr, nullptr);
		if (FAILED(hr) && feature != _T("IIS-NetFxExtensibility") && feature != _T("WAS-NetFxEnvironment"))
		{
			CHAR chBuf[BUFSIZE];
			DismGetLastErrorMessage(&pErrorString);
			WriteMessageToScreenAndLog("Couldn't install IIS-Feautures", true, false);
			WideCharToMultiByte(CP_ACP, 0, featureName, -1, chBuf, BUFSIZE, nullptr, nullptr);
			WriteMessageToScreenAndLog(chBuf, true, false);
			WideCharToMultiByte(CP_ACP, 0, pErrorString->Value, -1, chBuf, BUFSIZE, nullptr, nullptr);
			WriteMessageToScreenAndLog(chBuf, true, true);
			DismCleanup(pErrorString, session);
			return false;
		}
	}

	return true;
}

bool InstallAspNetFeatures(DismSession &session)
{
	HRESULT hr;
	DismString* pErrorString = nullptr;

	BOOL bAspNetNotInstalled = TRUE, bAspNet45NotInstalled = TRUE, bAspNet46NotInstalled = TRUE;

	hr = DismEnableFeature(session, _T("IIS-ASPNET"), nullptr, DismPackageNone, TRUE, nullptr, 0, TRUE, nullptr, nullptr, nullptr);
	if (!FAILED(hr))
	{
		bAspNetNotInstalled = FALSE;
	}

	hr = DismEnableFeature(session, _T("IIS-ASPNET45"), nullptr, DismPackageNone, TRUE, nullptr, 0, TRUE, nullptr, nullptr, nullptr);
	if (!FAILED(hr))
	{
		bAspNet45NotInstalled = FALSE;
	}

	hr = DismEnableFeature(session, _T("IIS-ASPNET46"), nullptr, DismPackageNone, TRUE, nullptr, 0, TRUE, nullptr, nullptr, nullptr);
	if (!FAILED(hr))
	{
		bAspNet46NotInstalled = FALSE;
	}

	if (bAspNetNotInstalled && bAspNet45NotInstalled && bAspNet46NotInstalled)
	{
		WriteMessageToScreenAndLog("Failed to install ASP.NET features", true, true);
		DismCleanup(pErrorString, session);
		return false;
	}
	return true;
}

bool RunProcess(LPCWSTR lpFile, LPCWSTR lpParameters, BOOL bIsChild)
{
	SHELLEXECUTEINFO ShellExecuteInfo = { 0 };
	ShellExecuteInfo.cbSize = sizeof(SHELLEXECUTEINFO);
	ShellExecuteInfo.fMask = SEE_MASK_NOCLOSEPROCESS;
	ShellExecuteInfo.hwnd = nullptr;
	ShellExecuteInfo.lpVerb = _T("runas");
	ShellExecuteInfo.lpFile = lpFile;
	ShellExecuteInfo.lpParameters = lpParameters;
	ShellExecuteInfo.lpDirectory = nullptr;
	ShellExecuteInfo.nShow = SW_SHOW;
	ShellExecuteInfo.hInstApp = nullptr;

	auto hChild = ShellExecuteEx(&ShellExecuteInfo);
	if (!hChild)
	{
		WriteMessageToScreenAndLog("Couldn't start installation process", true, true);
		return false;
	}

	if (bIsChild) 
	{
		WaitForSingleObject(ShellExecuteInfo.hProcess, INFINITE);
		CloseHandle(ShellExecuteInfo.hProcess);		
	}

	return true;
}

bool CheckNetfxVersionUsingMscoree(const TCHAR* pszNetfxVersionToCheck)
{
	bool bFoundRequestedNetfxVersion = false;
	HRESULT hr;

	if (NULL == pszNetfxVersionToCheck)
		return false;

	HMODULE hmodMscoree = LoadLibraryEx(_T("mscoree.dll"), nullptr, 0);
	if (NULL != hmodMscoree)
	{
		typedef HRESULT (STDAPICALLTYPE *GETCORVERSION)(LPWSTR szBuffer, DWORD cchBuffer, DWORD* dwLength);
		GETCORVERSION pfnGETCORVERSION = GETCORVERSION(GetProcAddress(hmodMscoree, "GetCORVersion"));

		if (NULL == pfnGETCORVERSION)
			goto Finish;

		typedef HRESULT (STDAPICALLTYPE *CORBINDTORUNTIME)(LPCWSTR pwszVersion, LPCWSTR pwszBuildFlavor, REFCLSID rclsid, REFIID riid, LPVOID FAR * ppv);
		CORBINDTORUNTIME pfnCORBINDTORUNTIME = CORBINDTORUNTIME(GetProcAddress(hmodMscoree, "CorBindToRuntime"));

		typedef HRESULT (STDAPICALLTYPE *GETREQUESTEDRUNTIMEINFO)(LPCWSTR pExe, LPCWSTR pwszVersion, LPCWSTR pConfigurationFile, DWORD startupFlags, DWORD runtimeInfoFlags, LPWSTR pDirectory, DWORD dwDirectory, DWORD* dwDirectoryLength, LPWSTR pVersion, DWORD cchBuffer, DWORD* dwlength);
		GETREQUESTEDRUNTIMEINFO pfnGETREQUESTEDRUNTIMEINFO = GETREQUESTEDRUNTIMEINFO(GetProcAddress(hmodMscoree, "GetRequestedRuntimeInfo"));

		if (NULL != pfnCORBINDTORUNTIME)
		{
			TCHAR szRetrievedVersion[50];
			DWORD dwLength = CountOf(szRetrievedVersion);

			if (NULL == pfnGETREQUESTEDRUNTIMEINFO)
			{
				if (0 == _tcscmp(pszNetfxVersionToCheck, g_szNetfx10VersionString))
				{
					hr = pfnGETCORVERSION(szRetrievedVersion, dwLength, &dwLength);

					if (SUCCEEDED(hr))
					{
						if (0 == _tcscmp(szRetrievedVersion, g_szNetfx10VersionString))
							bFoundRequestedNetfxVersion = true;
					}

					goto Finish;
				}
			}

			UINT uOldErrorMode = SetErrorMode(SEM_FAILCRITICALERRORS);

			TCHAR szDirectory[MAX_PATH];
			DWORD dwDirectoryLength = 0;
			//DWORD dwRuntimeInfoFlags = RUNTIME_INFO_DONT_RETURN_DIRECTORY | GetProcessorArchitectureFlag();

			hr = pfnGETREQUESTEDRUNTIMEINFO(nullptr, pszNetfxVersionToCheck, nullptr, STARTUP_LOADER_OPTIMIZATION_MULTI_DOMAIN_HOST, 0, szDirectory, CountOf(szDirectory), &dwDirectoryLength, szRetrievedVersion, CountOf(szRetrievedVersion), &dwLength);

			if (SUCCEEDED(hr))
				bFoundRequestedNetfxVersion = true;

			SetErrorMode(uOldErrorMode);
		}
	}

Finish:
	if (hmodMscoree)
	{
		FreeLibrary(hmodMscoree);
	}

	return bFoundRequestedNetfxVersion;
}

int GetNetfx10SPLevel()
{
	TCHAR szRegValue[MAX_PATH];
	TCHAR* pszSPLevel;
	int iRetValue = -1;
	bool bRegistryRetVal;

	if (IsCurrentOSTabletMedCenter())
		bRegistryRetVal = RegistryGetValue(HKEY_LOCAL_MACHINE, g_szNetfx10SPxOCMRegKeyName, g_szNetfxStandardVersionRegValueName, NULL, LPBYTE(szRegValue), MAX_PATH);
	else
		bRegistryRetVal = RegistryGetValue(HKEY_LOCAL_MACHINE, g_szNetfx10SPxMSIRegKeyName, g_szNetfxStandardVersionRegValueName, NULL, LPBYTE(szRegValue), MAX_PATH);

	if (bRegistryRetVal)
	{
		pszSPLevel = _tcsrchr(szRegValue, _T(','));
		if (NULL != pszSPLevel)
		{
			pszSPLevel++;

			iRetValue = _tstoi(pszSPLevel);
		}
	}

	return iRetValue;
}

int GetNetfxSPLevel(const TCHAR* pszNetfxRegKeyName, const TCHAR* pszNetfxRegValueName)
{
	DWORD dwRegValue = 0;

	if (RegistryGetValue(HKEY_LOCAL_MACHINE, pszNetfxRegKeyName, pszNetfxRegValueName, NULL, LPBYTE(&dwRegValue), sizeof(DWORD)))
	{
		return int(dwRegValue);
	}

	return -1;
}

DWORD GetProcessorArchitectureFlag()
{
	HMODULE hmodKernel32;
	typedef void (WINAPI *PFnGetNativeSystemInfo)(LPSYSTEM_INFO);
	PFnGetNativeSystemInfo pfnGetNativeSystemInfo;

	SYSTEM_INFO sSystemInfo;
	memset(&sSystemInfo, 0, sizeof(sSystemInfo));

	bool bRetrievedSystemInfo = false;

	hmodKernel32 = LoadLibrary(_T("Kernel32.dll"));
	if (NULL != hmodKernel32)
	{
		pfnGetNativeSystemInfo = PFnGetNativeSystemInfo(GetProcAddress(hmodKernel32, "GetNativeSystemInfo"));
		if (NULL != pfnGetNativeSystemInfo)
		{
			(*pfnGetNativeSystemInfo)(&sSystemInfo);
			bRetrievedSystemInfo = true;
		}
		FreeLibrary(hmodKernel32);
	}

	if (!bRetrievedSystemInfo)
	{
		GetSystemInfo(&sSystemInfo);
		bRetrievedSystemInfo = true;
	}

	if (bRetrievedSystemInfo)
	{
		switch (sSystemInfo.wProcessorArchitecture)
		{
		case PROCESSOR_ARCHITECTURE_INTEL:
			return RUNTIME_INFO_REQUEST_X86;
		case PROCESSOR_ARCHITECTURE_IA64:
			return RUNTIME_INFO_REQUEST_IA64;
		case PROCESSOR_ARCHITECTURE_AMD64:
			return RUNTIME_INFO_REQUEST_AMD64;
		default:
			return 0;
		}
	}

	return 0;
}

bool CheckNetfxBuildNumber(const TCHAR* pszNetfxRegKeyName, const TCHAR* pszNetfxRegKeyValue, const int iRequestedVersionMajor, const int iRequestedVersionMinor, const int iRequestedVersionBuild, const int iRequestedVersionRevision)
{
	TCHAR szRegValue[MAX_PATH];
	TCHAR* pszToken;
	TCHAR* pszNextToken = nullptr;
	int iVersionPartCounter = 0;
	int iRegistryVersionMajor = 0;
	int iRegistryVersionMinor = 0;
	int iRegistryVersionBuild = 0;
	int iRegistryVersionRevision = 0;
	bool bRegistryRetVal;

	bRegistryRetVal = RegistryGetValue(HKEY_LOCAL_MACHINE, pszNetfxRegKeyName, pszNetfxRegKeyValue, NULL, LPBYTE(szRegValue), MAX_PATH);

	if (bRegistryRetVal)
	{
		pszToken = _tcstok_s(szRegValue, _T("."), &pszNextToken);
		while (NULL != pszToken)
		{
			iVersionPartCounter++;

			switch (iVersionPartCounter)
			{
			case 1:
				iRegistryVersionMajor = _tstoi(pszToken);
				break;
			case 2:
				iRegistryVersionMinor = _tstoi(pszToken);
				break;
			case 3:
				iRegistryVersionBuild = _tstoi(pszToken);
				break;
			case 4:
				iRegistryVersionRevision = _tstoi(pszToken);
				break;
			default:
				break;
			}

			pszToken = _tcstok_s(nullptr, _T("."), &pszNextToken);
		}
	}

	if (iRegistryVersionMajor > iRequestedVersionMajor)
	{
		return true;
	}
	else if (iRegistryVersionMajor == iRequestedVersionMajor)
	{
		if (iRegistryVersionMinor > iRequestedVersionMinor)
		{
			return true;
		}
		else if (iRegistryVersionMinor == iRequestedVersionMinor)
		{
			if (iRegistryVersionBuild > iRequestedVersionBuild)
			{
				return true;
			}
			else if (iRegistryVersionBuild == iRequestedVersionBuild)
			{
				if (iRegistryVersionRevision >= iRequestedVersionRevision)
				{
					return true;
				}
			}
		}
	}

	return false;
}

bool IsCurrentOSTabletMedCenter()
{
	return ((GetSystemMetrics(SM_TABLETPC) != 0) || (GetSystemMetrics(SM_MEDIACENTER) != 0));
}

bool IsNetfx10Installed()
{
	TCHAR szRegValue[MAX_PATH];
	return (RegistryGetValue(HKEY_LOCAL_MACHINE, g_szNetfx10RegKeyName, g_szNetfx10RegKeyValue, NULL, LPBYTE(szRegValue), MAX_PATH));
}

bool IsNetfx11Installed()
{
	bool bRetValue = false;
	DWORD dwRegValue = 0;

	if (RegistryGetValue(HKEY_LOCAL_MACHINE, g_szNetfx11RegKeyName, g_szNetfxStandardRegValueName, NULL, LPBYTE(&dwRegValue), sizeof(DWORD)))
	{
		if (1 == dwRegValue)
			bRetValue = true;
	}

	return bRetValue;
}

bool IsNetfx20Installed()
{
	bool bRetValue = false;
	DWORD dwRegValue = 0;

	if (RegistryGetValue(HKEY_LOCAL_MACHINE, g_szNetfx20RegKeyName, g_szNetfxStandardRegValueName, NULL, LPBYTE(&dwRegValue), sizeof(DWORD)))
	{
		if (1 == dwRegValue)
			bRetValue = true;
	}

	return bRetValue;
}

bool IsNetfx30Installed()
{
	bool bRetValue = false;
	DWORD dwRegValue = 0;

	if (RegistryGetValue(HKEY_LOCAL_MACHINE, g_szNetfx30RegKeyName, g_szNetfx30RegValueName, NULL, LPBYTE(&dwRegValue), sizeof(DWORD)))
	{
		if (1 == dwRegValue)
			bRetValue = true;
	}

	return (bRetValue && CheckNetfxBuildNumber(g_szNetfx30RegKeyName, g_szNetfxStandardVersionRegValueName, g_iNetfx30VersionMajor, g_iNetfx30VersionMinor, g_iNetfx30VersionBuild, g_iNetfx30VersionRevision));
}

bool IsNetfx35Installed()
{
	bool bRetValue = false;
	DWORD dwRegValue = 0;

	if (RegistryGetValue(HKEY_LOCAL_MACHINE, g_szNetfx35RegKeyName, g_szNetfxStandardRegValueName, NULL, LPBYTE(&dwRegValue), sizeof(DWORD)))
	{
		if (1 == dwRegValue)
			bRetValue = true;
	}

	return (bRetValue && CheckNetfxBuildNumber(g_szNetfx35RegKeyName, g_szNetfxStandardVersionRegValueName, g_iNetfx35VersionMajor, g_iNetfx35VersionMinor, g_iNetfx35VersionBuild, g_iNetfx35VersionRevision));
}

bool IsNetfx40ClientInstalled()
{
	bool bRetValue = false;
	DWORD dwRegValue = 0;

	if (RegistryGetValue(HKEY_LOCAL_MACHINE, g_szNetfx40ClientRegKeyName, g_szNetfxStandardRegValueName, NULL, LPBYTE(&dwRegValue), sizeof(DWORD)))
	{
		if (1 == dwRegValue)
			bRetValue = true;
	}

	return (bRetValue && CheckNetfxBuildNumber(g_szNetfx40ClientRegKeyName, g_szNetfxStandardVersionRegValueName, g_iNetfx40VersionMajor, g_iNetfx40VersionMinor, g_iNetfx40VersionBuild, g_iNetfx40VersionRevision));
}

bool IsNetfx40FullInstalled()
{
	bool bRetValue = false;
	DWORD dwRegValue = 0;

	if (RegistryGetValue(HKEY_LOCAL_MACHINE, g_szNetfx40FullRegKeyName, g_szNetfxStandardRegValueName, NULL, LPBYTE(&dwRegValue), sizeof(DWORD)))
	{
		if (1 == dwRegValue)
			bRetValue = true;
	}

	return (bRetValue && CheckNetfxBuildNumber(g_szNetfx40FullRegKeyName, g_szNetfxStandardVersionRegValueName, g_iNetfx40VersionMajor, g_iNetfx40VersionMinor, g_iNetfx40VersionBuild, g_iNetfx40VersionRevision));
}

bool IsNetfx45Installed()
{
	bool bRetValue = false;
	DWORD dwRegValue = 0;

	if (RegistryGetValue(HKEY_LOCAL_MACHINE, g_szNetfx45RegKeyName, g_szNetfx45RegValueName, NULL, LPBYTE(&dwRegValue), sizeof(DWORD)))
	{
		if (g_dwNetfx45ReleaseVersion <= dwRegValue)
			bRetValue = true;
	}

	return bRetValue;
}

bool IsNetfx451Installed()
{
	bool bRetValue = false;
	DWORD dwRegValue = 0;

	if (RegistryGetValue(HKEY_LOCAL_MACHINE, g_szNetfx45RegKeyName, g_szNetfx45RegValueName, NULL, LPBYTE(&dwRegValue), sizeof(DWORD)))
	{
		if (g_dwNetfx451ReleaseVersion <= dwRegValue)
			bRetValue = true;
	}

	return bRetValue;
}

bool IsNetfx452Installed()
{
	bool bRetValue = false;
	DWORD dwRegValue = 0;

	if (RegistryGetValue(HKEY_LOCAL_MACHINE, g_szNetfx45RegKeyName, g_szNetfx45RegValueName, NULL, LPBYTE(&dwRegValue), sizeof(DWORD)))
	{
		if (g_dwNetfx452ReleaseVersion <= dwRegValue)
			bRetValue = true;
	}

	return bRetValue;
}

bool IsNetfx46Installed()
{
	bool bRetValue = false;
	DWORD dwRegValue = 0;

	if (RegistryGetValue(HKEY_LOCAL_MACHINE, g_szNetfx45RegKeyName, g_szNetfx45RegValueName, NULL, LPBYTE(&dwRegValue), sizeof(DWORD)))
	{
		if (g_dwNetfx46ReleaseVersion <= dwRegValue)
			bRetValue = true;
	}

	return bRetValue;
}

bool IsNetfx461Installed()
{
	bool bRetValue = false;
	DWORD dwRegValue = 0;

	if (RegistryGetValue(HKEY_LOCAL_MACHINE, g_szNetfx45RegKeyName, g_szNetfx45RegValueName, NULL, LPBYTE(&dwRegValue), sizeof(DWORD)))
	{
		if (g_dwNetfx461ReleaseVersion <= dwRegValue)
			bRetValue = true;
	}

	return bRetValue;
}

bool IsNetfx462Installed()
{
	bool bRetValue = false;
	DWORD dwRegValue = 0;

	if (RegistryGetValue(HKEY_LOCAL_MACHINE, g_szNetfx45RegKeyName, g_szNetfx45RegValueName, NULL, LPBYTE(&dwRegValue), sizeof(DWORD)))
	{
		if (g_dwNetfx462ReleaseVersion <= dwRegValue)
			bRetValue = true;
	}

	return bRetValue;
}

bool RegistryGetValue(HKEY hk, const TCHAR* pszKey, const TCHAR* pszValue, DWORD dwType, LPBYTE data, DWORD dwSize)
{
	HKEY hkOpened;

	if (RegOpenKeyEx(hk, pszKey, 0, KEY_READ, &hkOpened) != ERROR_SUCCESS)
	{
		return false;
	}

	if (RegQueryValueEx(hkOpened, pszValue, nullptr, &dwType, LPBYTE(data), &dwSize) != ERROR_SUCCESS)
	{
		RegCloseKey(hkOpened);
		return false;
	}

	RegCloseKey(hkOpened);

	return true;
}

void FillFeaturesList()
{
	g_vecFeatures.push_back(_T("IIS-WebServerRole"));
	g_vecFeatures.push_back(_T("IIS-WebServer"));
	g_vecFeatures.push_back(_T("IIS-CommonHttpFeatures"));
	g_vecFeatures.push_back(_T("IIS-StaticContent"));
	g_vecFeatures.push_back(_T("IIS-DefaultDocument"));
	g_vecFeatures.push_back(_T("IIS-DirectoryBrowsing"));
	g_vecFeatures.push_back(_T("IIS-HttpErrors"));
	g_vecFeatures.push_back(_T("IIS-HttpRedirect"));
	g_vecFeatures.push_back(_T("IIS-ApplicationDevelopment"));
	g_vecFeatures.push_back(_T("IIS-ASP"));
	g_vecFeatures.push_back(_T("IIS-CGI"));
	g_vecFeatures.push_back(_T("IIS-ISAPIExtensions"));
	g_vecFeatures.push_back(_T("IIS-ISAPIFilter"));
	g_vecFeatures.push_back(_T("IIS-ServerSideIncludes"));
	g_vecFeatures.push_back(_T("IIS-HealthAndDiagnostics"));
	g_vecFeatures.push_back(_T("IIS-HttpLogging"));
	g_vecFeatures.push_back(_T("IIS-LoggingLibraries"));
	g_vecFeatures.push_back(_T("IIS-RequestMonitor"));
	g_vecFeatures.push_back(_T("IIS-HttpTracing"));
	g_vecFeatures.push_back(_T("IIS-CustomLogging"));
	g_vecFeatures.push_back(_T("IIS-ODBCLogging"));
	g_vecFeatures.push_back(_T("IIS-Security"));
	g_vecFeatures.push_back(_T("IIS-BasicAuthentication"));
	g_vecFeatures.push_back(_T("IIS-WindowsAuthentication"));
	g_vecFeatures.push_back(_T("IIS-DigestAuthentication"));
	g_vecFeatures.push_back(_T("IIS-ClientCertificateMappingAuthentication"));
	g_vecFeatures.push_back(_T("IIS-IISCertificateMappingAuthentication"));
	g_vecFeatures.push_back(_T("IIS-URLAuthorization"));
	g_vecFeatures.push_back(_T("IIS-RequestFiltering"));
	g_vecFeatures.push_back(_T("IIS-IPSecurity"));
	g_vecFeatures.push_back(_T("IIS-Performance"));
	g_vecFeatures.push_back(_T("IIS-HttpCompressionStatic"));
	g_vecFeatures.push_back(_T("IIS-HttpCompressionDynamic"));
	g_vecFeatures.push_back(_T("IIS-WebServerManagementTools"));
	g_vecFeatures.push_back(_T("IIS-ManagementScriptingTools"));
	g_vecFeatures.push_back(_T("IIS-IIS6ManagementCompatibility"));
	g_vecFeatures.push_back(_T("IIS-Metabase"));
	g_vecFeatures.push_back(_T("IIS-WMICompatibility"));
	g_vecFeatures.push_back(_T("IIS-LegacyScripts"));
	g_vecFeatures.push_back(_T("WAS-WindowsActivationService"));
	g_vecFeatures.push_back(_T("WAS-ProcessModel"));
	g_vecFeatures.push_back(_T("IIS-FTPServer"));
	g_vecFeatures.push_back(_T("IIS-FTPSvc"));
	g_vecFeatures.push_back(_T("IIS-FTPExtensibility"));
	g_vecFeatures.push_back(_T("IIS-WebDAV"));
	g_vecFeatures.push_back(_T("IIS-NetFxExtensibility"));
	g_vecFeatures.push_back(_T("WAS-NetFxEnvironment"));
	g_vecFeatures.push_back(_T("WAS-ConfigurationAPI"));
	g_vecFeatures.push_back(_T("IIS-ManagementService"));
}
