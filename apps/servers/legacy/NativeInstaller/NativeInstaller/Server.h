#pragma once

#include "stdafx.h"
#include "IProgressObserver.h"
#include "MmIoChainer.h"

class Server : public MmioChainer, public IProgressObserver
{
public:
	BOOL m_bFirtsTime = true;
	HRESULT m_hrInternalResult = 0;

	Server() : MmioChainer(L"DeskAlertsSection", L"DeskAlertsEvent") {}

	bool Launch(const LPWSTR lpPath)
	{
		STARTUPINFO si = { 0 };
		si.cb = sizeof(si);
		PROCESS_INFORMATION pi = { nullptr };

		BOOL bLaunchedSetup = ::CreateProcess(nullptr, lpPath, nullptr, nullptr, FALSE, 0, nullptr, nullptr, &si, &pi);

		if (bLaunchedSetup != 0)
		{
			IProgressObserver& observer = dynamic_cast<IProgressObserver&>(*this);
			Run(pi.hProcess, observer);

			DWORD dwResult = GetResult();
			if (E_PENDING == dwResult)
			{
				::GetExitCodeProcess(pi.hProcess, &dwResult);
			}

			m_hrInternalResult = GetInternalResult();
			
			::CloseHandle(pi.hThread);
			::CloseHandle(pi.hProcess);
		}

		return bLaunchedSetup != 0;
	}

private:
	void OnProgress(unsigned char ubProgressSoFar) override
	{
		//printf("Progress: %i\n  ", ubProgressSoFar);
	}

	void Finished(HRESULT hr) override
	{
		//printf("\r\nFinished HRESULT: 0x%08X\r\n", hr);
	}

	DWORD Send(DWORD dwMessage, LPVOID pData, DWORD dwDataLength) override
	{
		DWORD dwResult = 0;
		//printf("recieved message: %d\n", dwMessage);
		switch (dwMessage)
		{
		case MMIO_CLOSE_APPS:
		{
			if (m_bFirtsTime)
			{
				dwResult = IDYES;
				m_bFirtsTime = false;
			}
			else
			{
				MmioCloseApplications* applications = reinterpret_cast<MmioCloseApplications*>(pData);
				for (DWORD i = 0; i < applications->m_dwApplicationsSize; i++)
				{
					HANDLE hProcess = OpenProcess(PROCESS_TERMINATE, 0, DWORD(applications->m_applications[i].m_dwPid));
					if (hProcess != nullptr)
					{
						TerminateProcess(hProcess, 9);
						CloseHandle(hProcess);
					}
				}
				dwResult = IDYES;
			}
			break;
		}
		default:
			break;
		}
		return dwResult;
	}

	static void ReportLastError()
	{
		DWORD dwLastError;
		DWORD dwMessageLength;
		LPWSTR lpstrMsgBuf = nullptr;
		
		dwLastError = GetLastError();
		dwMessageLength = FormatMessageW(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM, nullptr, dwLastError, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), LPTSTR(&lpstrMsgBuf), 0, nullptr);

		if (dwMessageLength)
		{
			printf("Last error: %ls", lpstrMsgBuf);
			LocalFree(lpstrMsgBuf);
		}
	}
};