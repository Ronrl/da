#pragma once

#include "IProgressObserver.h"
#include "MmioMessage.h"

#define MMIO_SUFFICIENT_SIZE_FOR_FIELD(size, field) \
    (m_dwDataSize >= (offsetof(MmioDataStructure, field) + sizeof(MmioDataStructure().field)))
#define MMIO_SUFFICIENT_SIZE_FOR_ARRAYFIELD(size, field, fieldCount) \
    (m_dwDataSize >= (offsetof(MmioDataStructure, field) + sizeof(MmioDataStructure().field) * fieldCount))

struct MmioDataStructure
{
	bool m_downloadFinished;
	bool m_installFinished;
	bool m_downloadAbort;
	bool m_installAbort;
	HRESULT m_hrDownloadFinished;
	HRESULT m_hrInstallFinished;
	HRESULT m_hrInternalError;
	WCHAR m_szCurrentItemStep[MAX_PATH];
	unsigned char m_downloadSoFar;
	unsigned char m_installSoFar;
	WCHAR m_szEventName[MAX_PATH];

	BYTE m_version;

	DWORD m_messageCode;
	DWORD m_messageResponse;
	DWORD m_messageDataLength;
	BYTE m_messageData[1];
};

class MmioChainerBase
{
	HANDLE m_hSection;

	HANDLE m_hEventChaineeSend;
	HANDLE m_hEventChainerSend;
	HANDLE m_hMutex;

	MmioDataStructure* m_pData;
	DWORD m_dwDataSize;

	class LockMutex
	{
		HANDLE m_hMutex;

	public:
		LockMutex(HANDLE hMutex) : m_hMutex(nullptr)
		{
			Lock(hMutex);
		}

		~LockMutex()
		{
			Release();
		}

		void Lock(HANDLE hMutex)
		{
			if (NULL != hMutex)
			{
				DWORD dwResult = WaitForSingleObject(m_hMutex, INFINITE);
				if (WAIT_OBJECT_0 == dwResult || WAIT_ABANDONED == dwResult)
				{
					m_hMutex = hMutex;
				}
			}
		}

		void Release()
		{
			if (NULL != m_hMutex)
			{
				ReleaseMutex(m_hMutex);
				m_hMutex = nullptr;
			}
		}
	};

protected:
	MmioChainerBase(HANDLE hSection) : m_hSection(hSection), m_hEventChaineeSend(nullptr) , m_hEventChainerSend(nullptr), m_hMutex(nullptr), m_pData(MapView(hSection)), m_dwDataSize(DWORD(GetRegionSize(m_pData)))
	{
	}

	virtual ~MmioChainerBase()
	{
		if (m_pData)
		{
			::UnmapViewOfFile(m_pData);
		}

		if (m_hEventChaineeSend)
		{
			::CloseHandle(m_hEventChaineeSend);
		}

		if (m_hEventChainerSend)
		{
			::CloseHandle(m_hEventChainerSend);
		}

		if (m_hMutex)
		{
			::CloseHandle(m_hMutex);
		}
	}

public:
	HANDLE GetChaineeEventHandle() const { return m_hEventChaineeSend; }
	HANDLE GetChainerEventHandle() const { return m_hEventChainerSend; }
	HANDLE GetMmioHandle() const { return m_hSection; }
	MmioDataStructure* GetData() const { return m_pData; }

	void InitializeChainer(CString eventName)
	{
		if (NULL == m_pData)
		{
			return;
		}

		m_hEventChaineeSend = CreateEvent(eventName);
		m_hEventChainerSend = CreateEvent(eventName + L"_send");
		m_hMutex = CreateMutex(eventName + L"_mutex");

		LockMutex lock(m_hMutex);

		wcscpy_s(m_pData->m_szEventName, MAX_PATH, eventName);

		m_pData->m_downloadFinished = false;
		m_pData->m_downloadSoFar = 0;
		m_pData->m_hrDownloadFinished = E_PENDING;
		m_pData->m_downloadAbort = false;

		m_pData->m_installFinished = false;
		m_pData->m_installSoFar = 0;
		m_pData->m_hrInstallFinished = E_PENDING;
		m_pData->m_installAbort = false;

		m_pData->m_hrInternalError = S_OK;

		m_pData->m_version = MMIO_V45;
		m_pData->m_messageCode = 0;
		m_pData->m_messageResponse = 0;
		m_pData->m_messageDataLength = 0;
	}

	void SignalChainee() const
	{
		if (m_hEventChainerSend)
		{
			::SetEvent(m_hEventChainerSend);
		}
	}

	bool WaitForChainee() const
	{
		if (m_hEventChaineeSend && WAIT_OBJECT_0 == ::WaitForSingleObject(m_hEventChaineeSend, INFINITE))
		{
			return true;
		}
		return false;
	}

	void Abort() const
	{
		if (NULL == m_pData	|| !MMIO_SUFFICIENT_SIZE_FOR_FIELD(m_dwDataSize, m_downloadAbort) || !MMIO_SUFFICIENT_SIZE_FOR_FIELD(m_dwDataSize, m_installAbort))
		{
			return;
		}

		LockMutex lock(m_hMutex);
		m_pData->m_downloadAbort = true;
		m_pData->m_installAbort = true;

		lock.Release();

		SignalChainee();
	}

	bool IsDone() const
	{
		if (NULL == m_pData	|| !MMIO_SUFFICIENT_SIZE_FOR_FIELD(m_dwDataSize, m_downloadFinished) || !MMIO_SUFFICIENT_SIZE_FOR_FIELD(m_dwDataSize, m_installFinished))
		{
			return true;
		}

		LockMutex lock(m_hMutex);
		return m_pData->m_downloadFinished && m_pData->m_installFinished;
	}

	unsigned char GetProgress() const
	{
		if (NULL == m_pData || !MMIO_SUFFICIENT_SIZE_FOR_FIELD(m_dwDataSize, m_downloadSoFar) || !MMIO_SUFFICIENT_SIZE_FOR_FIELD(m_dwDataSize, m_installSoFar))
		{
			return 255;
		}

		LockMutex lock(m_hMutex);
		return (m_pData->m_downloadSoFar + m_pData->m_installSoFar) / 2;
	}

	unsigned char GetDownloadProgress() const
	{
		if (NULL == m_pData	|| !MMIO_SUFFICIENT_SIZE_FOR_FIELD(m_dwDataSize, m_downloadSoFar))
		{
			return 255;
		}

		LockMutex lock(m_hMutex);
		return m_pData->m_downloadSoFar;
	}

	unsigned char GetInstallProgress() const
	{
		if (NULL == m_pData	|| !MMIO_SUFFICIENT_SIZE_FOR_FIELD(m_dwDataSize, m_installSoFar))
		{
			return 255;
		}

		LockMutex lock(m_hMutex);
		return m_pData->m_installSoFar;
	}

	HRESULT GetResult() const
	{
		if (NULL == m_pData	|| !MMIO_SUFFICIENT_SIZE_FOR_FIELD(m_dwDataSize, m_hrInstallFinished) || !MMIO_SUFFICIENT_SIZE_FOR_FIELD(m_dwDataSize, m_hrDownloadFinished))
		{
			return S_FALSE;
		}

		LockMutex lock(m_hMutex);
		if (m_pData->m_hrInstallFinished != S_OK)
		{
			return m_pData->m_hrInstallFinished;
		}
		return m_pData->m_hrDownloadFinished;
	}

	HRESULT GetDownloadResult() const
	{
		if (NULL == m_pData || !MMIO_SUFFICIENT_SIZE_FOR_FIELD(m_dwDataSize, m_hrDownloadFinished))	
		{
			return S_FALSE;
		}

		LockMutex lock(m_hMutex);
		return m_pData->m_hrDownloadFinished;
	}

	HRESULT GetInstallResult() const
	{
		if (NULL == m_pData || !MMIO_SUFFICIENT_SIZE_FOR_FIELD(m_dwDataSize, m_hrInstallFinished))
		{
			return S_FALSE;
		}

		LockMutex lock(m_hMutex);
		return m_pData->m_hrInstallFinished;
	}

	HRESULT GetInternalResult() const
	{
		if (NULL == m_pData || !MMIO_SUFFICIENT_SIZE_FOR_FIELD(m_dwDataSize, m_hrInternalError))
		{
			return S_FALSE;
		}

		LockMutex lock(m_hMutex);
		return m_pData->m_hrInternalError;
	}

	CString GetCurrentItemStep() const
	{
		if (NULL == m_pData	|| !MMIO_SUFFICIENT_SIZE_FOR_FIELD(m_dwDataSize, m_szCurrentItemStep))
		{
			return L"";
		}

		LockMutex lock(m_hMutex);
		return CString(m_pData->m_szCurrentItemStep);
	}

	void Respond(DWORD dwResponse) const
	{
		if (NULL == m_pData || !MMIO_SUFFICIENT_SIZE_FOR_FIELD(m_dwDataSize, m_messageResponse))
		{
			return;
		}

		LockMutex lock(m_hMutex);
		m_pData->m_messageCode = MMIO_NO_MESSAGE;
		m_pData->m_messageResponse = dwResponse;
		lock.Release();

		SignalChainee();
	}

	void InitializeChainee()
	{
		if (NULL == m_pData || !MMIO_SUFFICIENT_SIZE_FOR_FIELD(m_dwDataSize, m_szEventName))
		{
			return;
		}

		CString eventName(m_pData->m_szEventName);
		m_hEventChaineeSend = OpenEvent(eventName);
		m_hEventChainerSend = OpenEventReadOnly(eventName + L"_send");
		m_hMutex = OpenMutex(eventName + L"_mutex");
	}

	void SignalChainer() const
	{
		if (m_hEventChaineeSend)
		{
			::SetEvent(m_hEventChaineeSend);
		}
	}

	bool WaitForChainer() const
	{
		if (m_hEventChainerSend && WAIT_OBJECT_0 == ::WaitForSingleObject(m_hEventChainerSend, INFINITE))
		{
			return true;
		}
		return false;
	}

	BYTE GetChainerVersion() const
	{
		if (NULL == m_pData	|| !MMIO_SUFFICIENT_SIZE_FOR_FIELD(m_dwDataSize, m_version))
		{
			return MMIO_V40;
		}

		LockMutex lock(m_hMutex);
		return m_pData->m_version;
	}

	void GetMessage(DWORD* pdwMessage, LPVOID* ppBuffer, DWORD* pdwBufferSize) const
	{
		if (NULL == m_pData	|| !MMIO_SUFFICIENT_SIZE_FOR_FIELD(m_dwDataSize, m_messageCode)	|| !MMIO_SUFFICIENT_SIZE_FOR_FIELD(m_dwDataSize, m_messageDataLength))
		{
			return;
		}

		if (!pdwMessage || !ppBuffer || !pdwBufferSize)
		{
			return;
		}

		LockMutex lock(m_hMutex);

		*pdwMessage = m_pData->m_messageCode;
		*ppBuffer = nullptr;
		*pdwBufferSize = 0;

		if (*pdwMessage != MMIO_NO_MESSAGE)
		{
			*ppBuffer = new BYTE[m_pData->m_messageDataLength];
			if (*ppBuffer && MMIO_SUFFICIENT_SIZE_FOR_ARRAYFIELD(m_dwDataSize, m_messageData, m_pData->m_messageDataLength))
			{
				memcpy(*ppBuffer, m_pData->m_messageData, m_pData->m_messageDataLength);
			}
			*pdwBufferSize = m_pData->m_messageDataLength;
		}
	}

	bool IsAborted() const
	{
		if (NULL == m_pData	|| !MMIO_SUFFICIENT_SIZE_FOR_FIELD(m_dwDataSize, m_installAbort) || !MMIO_SUFFICIENT_SIZE_FOR_FIELD(m_dwDataSize, m_installAbort))
		{
			return false;
		}

		LockMutex lock(m_hMutex);
		return m_pData->m_installAbort || m_pData->m_downloadAbort;
	}

	void Finished(HRESULT hr, HRESULT hrInternalResult, CString strCurrentItemStep, bool bDownloader) const
	{
		if (NULL == m_pData)
		{
			SignalChainer();
			return;
		}

		LockMutex lock(m_hMutex);
		if (bDownloader)
		{
			m_pData->m_hrDownloadFinished = hr;
			m_pData->m_downloadFinished = true;
		}
		else
		{
			m_pData->m_hrInstallFinished = hr;
			m_pData->m_installFinished = true;
		}
		m_pData->m_hrInternalError = hrInternalResult;
		wcscpy_s(m_pData->m_szCurrentItemStep, MAX_PATH, strCurrentItemStep);
		lock.Release();

		SignalChainer();
	}

	void SoFar(unsigned char soFar, bool bDownloader) const
	{
		if (NULL == m_pData)
		{
			SignalChainer();
			return;
		}

		LockMutex lock(m_hMutex);
		if (bDownloader)
		{
			m_pData->m_downloadSoFar = soFar;
		}
		else
		{
			m_pData->m_installSoFar = soFar;
		}
		lock.Release();

		SignalChainer();
	}

	DWORD Send(DWORD dwMessage, LPVOID pData, DWORD dwDataLength) const
	{
		if (NULL == m_pData || !MMIO_SUFFICIENT_SIZE_FOR_FIELD(m_dwDataSize, m_messageCode) || !MMIO_SUFFICIENT_SIZE_FOR_FIELD(m_dwDataSize, m_messageResponse) || !MMIO_SUFFICIENT_SIZE_FOR_FIELD(m_dwDataSize, m_messageDataLength) || !MMIO_SUFFICIENT_SIZE_FOR_ARRAYFIELD(m_dwDataSize, m_messageData, dwDataLength))
		{
			return MMIO_MESSAGE_DEFAULT_RESONSE(dwMessage);
		}

		if (MMIO_MESSAGE_VERSION(dwMessage) > GetChainerVersion())
		{
			return MMIO_MESSAGE_DEFAULT_RESONSE(dwMessage);
		}

		LockMutex lock(m_hMutex);
		m_pData->m_messageCode = dwMessage;
		m_pData->m_messageResponse = MMIO_MESSAGE_DEFAULT_RESONSE(dwMessage);
		m_pData->m_messageDataLength = dwDataLength;
		memcpy(m_pData->m_messageData, pData, m_pData->m_messageDataLength);
		lock.Release();

		SignalChainer();

		if (!WaitForChainer())
		{
			return MMIO_MESSAGE_DEFAULT_RESONSE(dwMessage);
		}

		lock.Lock(m_hMutex);
		return m_pData->m_messageResponse;
	}

protected:
	static MmioDataStructure* MapView(HANDLE section)
	{
		if (NULL == section)
		{
			return reinterpret_cast<MmioDataStructure*>(NULL);
		}

		return reinterpret_cast<MmioDataStructure*>(::MapViewOfFile(section, FILE_MAP_WRITE, 0, 0, 0));
	}

	static SIZE_T GetRegionSize(LPCVOID lpAddress)
	{
		MEMORY_BASIC_INFORMATION info;
		if (::VirtualQuery(lpAddress, &info, sizeof(info)))
		{
			return info.RegionSize;
		}
		return 0;
	}

	static HANDLE CreateEvent(LPCWSTR eventName)
	{
		return ::CreateEvent(nullptr, FALSE, FALSE, eventName);
	}

	static HANDLE OpenEvent(LPCWSTR eventName)
	{
		return ::OpenEvent(EVENT_MODIFY_STATE | SYNCHRONIZE, FALSE, eventName);
	}

	static HANDLE OpenEventReadOnly(LPCWSTR eventName)
	{
		return ::OpenEvent(SYNCHRONIZE, FALSE, eventName);
	}

	static HANDLE CreateMutex(LPCWSTR mutexName)
	{
		return ::CreateMutex(nullptr, FALSE, mutexName);
	}

	static HANDLE OpenMutex(LPCWSTR mutexName)
	{
		return ::OpenMutex(SYNCHRONIZE, FALSE, mutexName);
	}
};

class MmioChainer : protected MmioChainerBase
{
public:
	MmioChainer(LPCWSTR sectionName, LPCWSTR eventName) : MmioChainerBase(CreateSection(sectionName))
	{
		InitializeChainer(eventName);
	}

	virtual ~MmioChainer()
	{
		HANDLE hSection = GetMmioHandle();
		if (hSection)
		{
			::CloseHandle(hSection);
		}
	}

public:
	using MmioChainerBase::Abort;
	using MmioChainerBase::GetInstallResult;
	using MmioChainerBase::GetInstallProgress;
	using MmioChainerBase::GetDownloadResult;
	using MmioChainerBase::GetDownloadProgress;
	using MmioChainerBase::GetCurrentItemStep;

	HRESULT GetInternalErrorCode() const
	{
		return GetInternalResult();
	}

	void Run(HANDLE process, IProgressObserver& observer) const
	{
		HANDLE handles[2] = { process, GetChaineeEventHandle() };

		while (!IsDone())
		{
			DWORD ret = ::WaitForMultipleObjects(2, handles, FALSE, 100);
			switch (ret)
			{
			case WAIT_OBJECT_0:
			{
				if (IsDone() == false)
				{
					HRESULT hr = GetResult();
					if (hr == E_PENDING)
					{
						observer.Finished(E_FAIL);
					}
					else
					{
						observer.Finished(hr);
					}

					return;
				}
				break;
			}
			case WAIT_OBJECT_0 + 1:
			{
				observer.OnProgress(GetProgress());

				DWORD dwMessage = MMIO_NO_MESSAGE;
				DWORD dwBufferSize = 0;
				LPVOID pBuffer = nullptr;

				GetMessage(&dwMessage, &pBuffer, &dwBufferSize);

				if (MMIO_NO_MESSAGE != dwMessage)
				{
					Respond(observer.Send(dwMessage, pBuffer, dwBufferSize));
				}

				if (pBuffer)
				{
					delete[] pBuffer;
				}
				break;
			}
			default:
				break;
			}
		}
		observer.Finished(GetResult());
	}

private:
	static HANDLE CreateSection(LPCWSTR sectionName)
	{
		return ::CreateFileMapping(INVALID_HANDLE_VALUE, nullptr, PAGE_READWRITE, 0, MMIO_SIZE, sectionName);
	}
};

class MmioChainee : protected MmioChainerBase
{
public:
	MmioChainee(LPCWSTR sectionName) : MmioChainerBase(OpenSection(sectionName))
	{
		InitializeChainee();
	}
	virtual ~MmioChainee()
	{
		HANDLE hSection = GetMmioHandle();
		if (hSection)
		{
			::CloseHandle(hSection);
		}
	}

private:
	static HANDLE OpenSection(LPCWSTR sectionName)
	{
		return ::OpenFileMapping(FILE_MAP_WRITE, FALSE, sectionName);
	}
};