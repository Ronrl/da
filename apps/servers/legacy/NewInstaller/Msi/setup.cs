//css_ref ..\..\..\WixSharp\WixSharp.dll;
//css_ref ..\..\..\WixSharp\Wix_bin\SDK\Microsoft.Deployment.WindowsInstaller.dll;
//css_ref System.Core.dll;

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Microsoft.Deployment.WindowsInstaller;
using Microsoft.Web.Administration;
using WixSharp;
using System.Windows.Forms;
using System.Xml;
using WixSharp.CommonTasks;
using Action = WixSharp.Action;
using Application = Microsoft.Web.Administration.Application;
using System.Threading;

// ReSharper disable ObjectCreationAsStatement

namespace DeskAlertsMsi
{
    class Script
    {
        private static string isDemo;
        private static string isAdAddon;
        private static string isSmsAddon;
        private static string isEmailAddon;
        private static string isEncryptAddon;
        private static string isSurveysAddon;
        private static string isStatisticsAddon;
        private static string isScreensaverAddon;
        private static string isRssAddon;
        private static string isWallpaperAddon;
        private static string isLockscreenAddon;
        private static string isTwitterAddon;
        private static string isLinkedinAddon;
        private static string isFullscreenAddon;
        private static string isTickerAddon;
        private static string isImAddon;
        private static string isWidgetstatAddon;
        private static string isTextcallAddon;
        private static string isMobileAddon;
        private static string isVideoAddon;
        private static string isCampaignAddon;
        private static string isRESTAPIAddon;
        private static string isMultiLanguageAddon;
        private static string isApproveAddon;
        private static string isMultipleAlerts;
        private static string isDigsignAddon;
        private static string isYammerAddon;
        private static bool isSingleInstance;
        private static bool isBulkUploadOfRecipients;
        private static string vers;
        private static string licenses;
        private static string maxPublishersCount;
        private static string trialcount;
        private const string ProductName = "DeskAlerts";

        public static void Main(string[] args)
        {
            if (System.IO.File.Exists("Attributes.txt"))
            {
                using (var sr = new System.IO.StreamReader("Attributes.txt"))
                {
                    var attributes = sr.ReadToEnd().Split(' ');
                    isDemo = ReadBoolAttribute(attributes, "isDemo") ? "IS_DEMO" : string.Empty;
                    isAdAddon = ReadBoolAttribute(attributes, "isADAddon") ? "MODULEACTIVEDIRECTORY" : string.Empty;
                    isSmsAddon = ReadBoolAttribute(attributes, "isSMSAddon") ? "MODULESMS" : string.Empty;
                    isEmailAddon = ReadBoolAttribute(attributes, "isEMAILAddon") ? "ModuleEmail" : string.Empty;
                    isEncryptAddon = ReadBoolAttribute(attributes, "isENCRYPTAddon") ? "ModuleEncrypt" : string.Empty;
                    isSurveysAddon = ReadBoolAttribute(attributes, "isSURVEYSAddon") ? "ModuleSurvey" : string.Empty;
                    isStatisticsAddon = ReadBoolAttribute(attributes, "isSTATISTICSAddon") ? "ModuleStatistics" : string.Empty;
                    isScreensaverAddon = ReadBoolAttribute(attributes, "isSCREENSAVERAddon") ? "ScreenSaver" : string.Empty;
                    isRssAddon = ReadBoolAttribute(attributes, "isRSSAddon") ? "ModuleRSS" : string.Empty;
                    isWallpaperAddon = ReadBoolAttribute(attributes, "isWALLPAPERAddon") ? "ModuleWalpaper" : string.Empty;
                    isLockscreenAddon = ReadBoolAttribute(attributes, "isLOCKSCREENAddon") ? "ModuleLockscreen" : string.Empty;
                    isTwitterAddon = ReadBoolAttribute(attributes, "isTWITTERAddon") ? "ModuleTwitter" : string.Empty;
                    isLinkedinAddon = ReadBoolAttribute(attributes, "isLINKEDINAddon") ? "ModuleLinkedin" : string.Empty;
                    isFullscreenAddon = ReadBoolAttribute(attributes, "isFULLSCREENAddon") ? "ModuleFullScreen" : string.Empty;
                    isTickerAddon = ReadBoolAttribute(attributes, "isTICKERAddon") ? "ModuleTicker" : string.Empty;
                    isImAddon = ReadBoolAttribute(attributes, "isIMAddon") ? "ModuleIM" : string.Empty;
                    isWidgetstatAddon = ReadBoolAttribute(attributes, "isWIDGETSTATAddon") ? "ModuleWidgets" : string.Empty;
                    isTextcallAddon = ReadBoolAttribute(attributes, "isTEXTCALLAddon") ? "ModuleTextToCall" : string.Empty;
                    isMobileAddon = ReadBoolAttribute(attributes, "isMOBILEAddon") ? "ModuleMobile" : string.Empty;
                    isVideoAddon = ReadBoolAttribute(attributes, "isVIDEOAddon") ? "ModuleVideo" : string.Empty;
                    isCampaignAddon = ReadBoolAttribute(attributes, "isCAMPAIGNAddon") ? "ModuleCampaign" : string.Empty;
                    isRESTAPIAddon = ReadBoolAttribute(attributes, "isRESTAPIAddon") ? "ModuleRESTAPI" : string.Empty;
                    isMultiLanguageAddon = ReadBoolAttribute(attributes, "isMULTILANGUAGEAddon") ? "ModuleMultiLanguage" : string.Empty;
                    isApproveAddon = ReadBoolAttribute(attributes, "isAPPROVEAddon") ? "ModuleApprove" : string.Empty;
                    isDigsignAddon = ReadBoolAttribute(attributes, "isDIGSIGNAddon") ? "ModuleDesign" : string.Empty;
                    isYammerAddon = ReadBoolAttribute(attributes, "isYAMMERAddon") ? "ModuleYammer" : string.Empty;
                    isMultipleAlerts = ReadBoolAttribute(attributes, "isMultipleAlerts") ? "ModuleMultipleAlerts" : string.Empty;
                    isSingleInstance = ReadBoolAttribute(attributes, "isSingleInstance");
                    isBulkUploadOfRecipients = ReadBoolAttribute(attributes, "isBulkUploadOfRecipients");
                    vers = ReadStringAttribute(attributes, "PRODUCT_VERSION");
                    licenses = ReadStringAttribute(attributes, "licenses");
                    maxPublishersCount = ReadStringAttribute(attributes, "maxpublisherscount");
                    trialcount = ReadStringAttribute(attributes, "trial");
                    var binaries = new Feature("Binaries");
                    var project =
                        new Project
                        {
                            Name = ProductName,
                            Actions = isSingleInstance ? SingleInstanceManagedActions : MulpitpleInstanceManagedActions,
                            Properties = new[]
                            {
                                new Property("IsDemo", isDemo),
                                new Property("INSTALLPATH", string.Empty),
                                new Property("DROPDATABASE", string.Empty),
                                new Property("SQLSERVER", string.Empty),
                                new Property("AZURESQL", string.Empty),
                                new Property("SQLLOGIN", string.Empty),
                                new Property("SQLPASSWORD", string.Empty),
                                new Property("SQLDATABASE", string.Empty),
                                new Property("NETINSTALL", "False"),
                                new Property("SITE", "\\"),
                                new Property("LICENSES", licenses),
                                new Property("MAXPUBLISHERSCOUNT", maxPublishersCount),
                                new Property("TRIALCOUNT", trialcount),
                                new Property("VERSION", vers),
                                new Property("IISURL", string.Empty),
                                new Property("SMSURL", string.Empty),
                                new Property("SMSPOSTDATA", string.Empty),
                                new Property("SMSAUTHUSER", string.Empty),
                                new Property("SMTPSERVER", string.Empty),
                                new Property("SMTPPORT", string.Empty),
                                new Property("SMTPSSL", string.Empty),
                                new Property("SMTPAUTH", string.Empty),
                                new Property("SMTPUSER", string.Empty),
                                new Property("SMTPPASSWORD", string.Empty),
                                new Property("ENCKEY", string.Empty),
                                new Property("TTCALLUSER", string.Empty),
                                new Property("TTCALLPASS", string.Empty),
                                new Property("TTCALLPHONE", string.Empty),
                                new Property("INSTANCEID", "Default"),
                                new Property("DYNGUID", Guid.NewGuid().ToString()),
                                new Property("MODULEVIDEO", "Default"),
                                new Property("MODULERESTAPI", "Default"),
                                new Property("NODULEMULTILANGUAGE", "Default"),
                                new Property("MODULETEXTTOCALL", "Default"),
                                new Property("MODULEACTIVEDIRECTORY", "Default"),
                                new Property("MODULESSO", "Default"),
                                new Property("MODULESMS", "Default"),
                                new Property("MODULEEMAIL", "Default"),
                                new Property("MODULEENCRYPT", "Default"),
                                new Property("MODULESURVEY", "Default"),
                                new Property("MODULESTATISTICS", "Default"),
                                new Property("SCREENSAVER", "Default"),
                                new Property("MODULERSS", "Default"),
                                new Property("MODULEMULTIPLEALERTS", "Default"),
                                new Property("MODULESMPP", "Default"),
                                new Property("ModuleWalpaper", "Default"),
                                new Property("MODULEWALPAPER", "Default"),
                                new Property("MODULETWITTER", "Default"),
                                new Property("MODULELINKEDIN", "Default"),
                                new Property("MODULEYAMMER", "Default"),
                                new Property("MODULEFULLSCREEN", "Default"),
                                new Property("MODULETICKER", "Default"),
                                new Property("MODULEIM", "Default"),
                                new Property("MODULEMOBILE", "Default"),
                                new Property("MODULECAMPAIGN", "Default"),
                                new Property("MODULEAPPROVE", "Default"),
                                new Property("MODULEDESIGN", "Default"),
                                new Property("MODULEBULKUPLOADOFRECIPIENTS", "Default")
                            },
                            LaunchConditions = new List<LaunchCondition>
                            {
                                new LaunchCondition("CUSTOM_UI=\"true\" OR REMOVE=\"ALL\"", "Please run .exe instead."),
                                new LaunchCondition("NOT WIX_IS_NETFRAMEWORK_45_OR_LATER_INSTALLED",
                                    "Please install .Net Framework 4.5"),
                            },

                            ProductId = isSingleInstance
                                ? new Guid("420779b2-216f-4003-b215-a13fde010f3f")
                                : Guid.NewGuid(),
                            GUID = isSingleInstance ? new Guid("a596749f-2026-408d-9f9a-da1807275351") : Guid.NewGuid(),
                            Version = new Version(vers),
                            Language = "en-US",
                            Description = "DeskAlerts Server",
                            UI = WUI.WixUI_FeatureTree
                        };

                    var releaseDirectory = new Dir(@".", new Files(binaries, @"..\..\DeskAlerts.Web\bin\Release\*.*"));
                    var sqlDirectory = new Dir("sqlfiles", new Files(binaries, @"sqlScripts\*.*"), new DirPermission("Everyone", GenericPermission.All));
                    var dllDirectory = new Dir(binaries, "dll", new Files(binaries, @"..\additionalfiles\DLL\*.*"), new DirPermission("Everyone", GenericPermission.All));
                    var modulesDirectory = new Dir("Modules", GetModules(binaries));
                    var dirs = new List<Dir> { releaseDirectory, sqlDirectory, dllDirectory, modulesDirectory }; //, servicesDirectory };
                    if (isSingleInstance)
                    {
                        var servicesDirectory = new Dir("Service", ServiceFiles);
                        dirs.Add(servicesDirectory);
                    }

                    project.Dirs = dirs.ToArray();
                    project.EmitConsistentPackageId = true;
                    project.PreserveTempFiles = true;
                    project.ControlPanelInfo.Comments = "DeskAlerts Server Installation";
                    project.ControlPanelInfo.Readme = "http://alert-software.com/tech-info/system-requierements/";
                    project.ControlPanelInfo.HelpLink = "http://alert-software.com/support/send-request/";
                    project.ControlPanelInfo.HelpTelephone = "1-703-763-2247";
                    project.ControlPanelInfo.UrlInfoAbout = "http://alert-software.com/";
                    project.ControlPanelInfo.UrlUpdateInfo = "http://alert-software.com/";
                    project.ControlPanelInfo.ProductIcon = "icon.ico";
                    project.ControlPanelInfo.Contact = "Softomate, LLC";
                    project.ControlPanelInfo.Manufacturer = "Softomate, LLC";
                    project.ControlPanelInfo.InstallLocation = "[INSTALLDIR]";
                    project.ControlPanelInfo.NoModify = true;

                    Compiler.BuildMsi(project);
                }
            }
        }

        private static WixEntity[] ServiceFiles => new WixEntity[]
        {
            new Files(@"..\..\DeskAlerts.Service\bin\Release\*.exe"),
            new Files(@"..\..\DeskAlerts.Service\bin\Release\*.dll"),
            new Files(@"..\..\DeskAlerts.Service\bin\Release\*.config")
        };

        private static Action[] SingleInstanceManagedActions => new Action[]
        {
            new ManagedAction(Actions.StopAppPools, Return.check, When.Before, Step.InstallInitialize,
                Condition.NOT_Installed),
            new ManagedAction(Actions.PreInstall, Return.check, When.Before, Step.LaunchConditions,
                Condition.NOT_Installed, Sequence.InstallUISequence),
            new ManagedAction(Actions.PostInstall, Return.check, When.After, Step.InstallFinalize,
                Condition.NOT_Installed),
            new ManagedAction(Actions.InstallService, Return.check, When.After, Step.InstallFinalize,
                Condition.NOT_Installed),
            new ManagedAction(Actions.Uninstall, Return.check, When.Before, Step.InstallFinalize,
                Condition.Installed),
            new ManagedAction(Actions.UninstallService, Return.check, When.Before, Step.RemoveFiles,
                Condition.BeingRemoved),
            new ManagedAction(Actions.StartAppPools, Return.check, When.After, Step.InstallFinalize,
                Condition.NOT_Installed)
        };

        private static Action[] MulpitpleInstanceManagedActions => new Action[]
        {
            new ManagedAction(Actions.PreInstall, Return.check, When.Before, Step.LaunchConditions,
                Condition.NOT_Installed, Sequence.InstallUISequence),
            new ManagedAction(Actions.PostInstall, Return.check, When.After, Step.InstallFinalize,
                Condition.NOT_Installed),
            new ManagedAction(Actions.Uninstall, Return.check, When.Before, Step.InstallFinalize,
                Condition.Installed),
            new ManagedAction(Actions.StartAppPools, Return.check, When.After, Step.InstallFinalize,
                Condition.NOT_Installed),
            new ManagedAction(Actions.StopAppPools, Return.check, When.Before, Step.InstallInitialize,
                Condition.NOT_Installed)
        };

        private const string PropertyPrefix = "/D";

        private static bool ReadBoolAttribute(IEnumerable<string> attributes, string attribute)
        {
            return attributes.Any(x => x.StartsWith(PropertyPrefix + attribute));
        }

        private static string ReadStringAttribute(IEnumerable<string> attributes, string attribute)
        {
            var attributeText = attributes.FirstOrDefault(x => x.StartsWith(PropertyPrefix + attribute));
            if (string.IsNullOrEmpty(attributeText))
            {
                return string.Empty;
            }
            else
            {
                return attributeText.Substring((PropertyPrefix + attribute).Length + 1);// + 1 - it's length of equal sign
            }
        }

        private static void AddModuleIfEnabled(string module, string moduleName, string path, List<WixEntity> modules, Feature feature)
        {
            if (!string.IsNullOrEmpty(module))
            {
                modules.Add(new Dir(feature, moduleName, new Files(feature, path),
                    new DirPermission("Everyone", GenericPermission.All)));
            }
        }

        private static WixEntity[] GetModules(Feature feature)
        {
            var modules = new List<WixEntity>();

            AddModuleIfEnabled(isDemo, "demoServer", @"..\..\DeskAlerts.Web\sever\DEMO\*.*", modules, feature);
            AddModuleIfEnabled(isAdAddon, "isAdAddon", @"..\..\DeskAlerts.Web\sever\MODULES\AD MODULE\*.*", modules, feature);
            AddModuleIfEnabled(isSmsAddon, "isSmsAddon", @"..\..\DeskAlerts.Web\sever\MODULES\SMS MODULE\*.*", modules, feature);
            AddModuleIfEnabled(isEmailAddon, "isEmailAddon", @"..\..\DeskAlerts.Web\sever\MODULES\EMAIL MODULE\*.*", modules, feature);
            AddModuleIfEnabled(isEncryptAddon, "isEncryptAddon", @"..\..\DeskAlerts.Web\sever\MODULES\ENCRYPT MODULE\*.*", modules, feature);
            AddModuleIfEnabled(isSurveysAddon, "isSurveysAddon", @"..\..\DeskAlerts.Web\sever\MODULES\SURVEYS MODULE\*.*", modules, feature);
            AddModuleIfEnabled(isStatisticsAddon, "isStatisticsAddon", @"..\..\DeskAlerts.Web\sever\MODULES\EXTENDED STATISTICS\*.*", modules, feature);
            AddModuleIfEnabled(isScreensaverAddon, "isScreensaverAddon", @"..\..\DeskAlerts.Web\sever\MODULES\SCREENSAVER\*.*", modules, feature);
            AddModuleIfEnabled(isRssAddon, "isRssAddon", @"..\..\DeskAlerts.Web\sever\MODULES\RSS\*.*", modules, feature);
            AddModuleIfEnabled(isWallpaperAddon, "isWallpaperAddon", @"..\..\DeskAlerts.Web\sever\MODULES\WALLPAPER\*.*", modules, feature);
            AddModuleIfEnabled(isLockscreenAddon, "isLockscreenAddon", @"..\..\DeskAlerts.Web\sever\MODULES\LOCKSCREEN\*.*", modules, feature);
            AddModuleIfEnabled(isTwitterAddon, "isTwitterAddon", @"..\..\DeskAlerts.Web\sever\MODULES\SOCIAL_MEDIA\*.*", modules, feature);
            AddModuleIfEnabled(isLinkedinAddon, "isLinkedinAddon", @"..\..\DeskAlerts.Web\sever\MODULES\SOCIAL_LINKEDIN\*.*", modules, feature);
            AddModuleIfEnabled(isFullscreenAddon, "isFullscreenAddon", @"..\..\DeskAlerts.Web\sever\MODULES\FULLSCREEN\*.*", modules, feature);
            AddModuleIfEnabled(isTickerAddon, "isTickerAddon", @"..\..\DeskAlerts.Web\sever\MODULES\TICKER\*.*", modules, feature);
            AddModuleIfEnabled(isImAddon, "isImAddon", @"..\..\DeskAlerts.Web\sever\MODULES\INSTANT_MESSAGES\*.*", modules, feature);
            AddModuleIfEnabled(isWidgetstatAddon, "isWidgetstatAddon", @"..\..\DeskAlerts.Web\sever\MODULES\WIDGETSTAT\*.*", modules, feature);
            AddModuleIfEnabled(isTextcallAddon, "isTextcallAddon", @"..\..\DeskAlerts.Web\sever\MODULES\TEXTCALL MODULE\*.*", modules, feature);
            AddModuleIfEnabled(isMobileAddon, "isMobileAddon", @"..\..\DeskAlerts.Web\sever\MODULES\MOBILE\*.*", modules, feature);
            AddModuleIfEnabled(isVideoAddon, "isVideoAddon", @"..\..\DeskAlerts.Web\sever\MODULES\VIDEOALERT\*.*", modules, feature);
            AddModuleIfEnabled(isCampaignAddon, "isCampaignAddon", @"..\..\DeskAlerts.Web\sever\MODULES\CAMPAIGN MODULE\*.*", modules, feature);
            AddModuleIfEnabled(isApproveAddon, "isApproveAddon", @"..\..\DeskAlerts.Web\sever\MODULES\APPROVAL\*.*", modules, feature);
            AddModuleIfEnabled(isYammerAddon, "isYammerAddon", @"..\..\DeskAlerts.Web\sever\MODULES\YAMMER\*.*", modules, feature);

            return modules.ToArray();
        }
    }

    public class Actions
    {
        private static int MinutesToWaitStopApplicationPool = 0;
        private static void GarantAccess(string fullPath)
        {
            var process = new Process();
            var startInfo = new ProcessStartInfo
            {
                WindowStyle = ProcessWindowStyle.Hidden,
                FileName = "cmd.exe",
                Arguments = "/c icacls " + fullPath + " /grant IIS_IUSRS:F",
                UseShellExecute = true,
                Verb = "runas"
            };

            process.StartInfo = startInfo;
            process.Start();
            process.WaitForExit();

        }

        private static TimeSpan getPeriodUpdateInstallLogFor_StartStopAppPools() 
        {
            return new TimeSpan(0, 0, 15);
        }
        public static void AddHttpsRedirect(string iisSiteName, string applicationName, string siteName)
        {
            using (ServerManager serverManager = new ServerManager())
            {
                Configuration appConfig = serverManager.GetWebConfiguration(siteName, applicationName);

                ConfigurationSection rulesSection = appConfig.GetSection("system.webServer/rewrite/rules");

                ConfigurationElementCollection rulesCollection = rulesSection.GetCollection();

                foreach (ConfigurationElement rule in rulesCollection)
                {
                    if (rule["name"] == @"Redirect to https")
                    {
                        rulesCollection.Remove(rule);
                    }
                }

                if (iisSiteName.StartsWith("https"))
                {
                    ConfigurationElement ruleElement1 = rulesCollection.CreateElement("rule");

                    ruleElement1["name"] = @"Redirect to https";
                    ruleElement1["stopProcessing"] = true;
                    ConfigurationElement matchElement = ruleElement1.GetChildElement("match");
                    matchElement["url"] = @".*";

                    ConfigurationElement conditionsElement = ruleElement1.GetChildElement("conditions");

                    ConfigurationElementCollection conditionsCollection = conditionsElement.GetCollection();

                    ConfigurationElement addElement = conditionsCollection.CreateElement("add");
                    addElement["input"] = @"{HTTPS}";
                    addElement["pattern"] = @"off";
                    conditionsCollection.Add(addElement);

                    ConfigurationElement actionElement = ruleElement1.GetChildElement("action");
                    actionElement["type"] = @"Redirect";
                    actionElement["url"] = @"https://{HTTP_HOST}{REQUEST_URI}";
                    actionElement["appendQueryString"] = false;
                    rulesCollection.Add(ruleElement1);

                    serverManager.CommitChanges();
                }
            }

        }

        #region Create Application

        private static string GenerateAppPoolName()
        {
            return $"DeskAlertsAppPool_{Guid.NewGuid().ToString().Substring(0, 5)}";
        }

        private static string GetAppPoolName(ServerManager serverManager)
        {
            string applicationPoolName;
            ApplicationPool applicationPool;
            do
            {
                applicationPoolName = GenerateAppPoolName();
                applicationPool = serverManager.ApplicationPools[applicationPoolName];
            } while (applicationPool != null);

            return applicationPoolName;
        }

        private static string GenerateAppPoolName(ServerManager serverManager)
        {
            string applicationPoolName;
            ApplicationPool applicationPool;
            do
            {
                applicationPoolName = GenerateAppPoolName();
                applicationPool = serverManager.ApplicationPools[applicationPoolName];
            } while (applicationPool != null);

            return applicationPoolName;
        }

        public static void CreateApplication(string siteName, string applicationName, string path, string oldPath, bool isNewAppPool)
        {
            using (var serverManager = new ServerManager())
            {
                var config = serverManager.GetApplicationHostConfiguration();
                var site = GetSite(siteName, serverManager);
                EnableAnonymousAuthentication(config, siteName);
                var application = GetApplication(site, applicationName);
                if (application == null)
                {
                    application = site.Applications.Add("/" + applicationName, path);
                }
                else if (path != oldPath) //Path was updated
                {
                    application.VirtualDirectories[0].PhysicalPath = path;
                }

                if (isNewAppPool)
                {
                    var applicationPoolName = GetAppPoolName(serverManager);
                    serverManager.ApplicationPools.Add(applicationPoolName);
                    application.ApplicationPoolName = applicationPoolName;
                }
                serverManager.CommitChanges();
            }
        }

        private static Site GetSite(string siteName, ServerManager serverManager)
        {
            return serverManager.Sites.FirstOrDefault(a => a.Name == siteName);
        }
 
        private static ApplicationPool GetApplicationPool(string siteName, ServerManager serverManager)
        {
            return serverManager.ApplicationPools[GetSite(siteName, serverManager).Applications["/"].ApplicationPoolName];
        }

        private static bool StopApplicationPoolsOfSite(string siteName, ServerManager serverManager, Session session)
        {
            TimeSpan maxTimeSpan = new TimeSpan(0, Actions.MinutesToWaitStopApplicationPool, 0);
            TimeSpan currentTimeSpan;
            TimeSpan nextTimeUpdateLog = new TimeSpan(0, 0, 0);
            List<String> resAppPoolNames;
            bool isStoped = true;
            foreach (var application in GetSite(siteName, serverManager).Applications)
            {
                bool isStopCurrentApplPoolSuccess = false;
                session.Log(string.Format("DeskAlerts site use '%s' ApplicationPool. Try to stop it"), application.ApplicationPoolName);
                DateTime startTime = DateTime.Now;
                currentTimeSpan = DateTime.Now - startTime;

                var state = serverManager.ApplicationPools[application.ApplicationPoolName].State;
                if (state == ObjectState.Starting)
                {
                    session.Log("Application pool '" + application.ApplicationPoolName + "' is starting now, wait for start done..");
                }
                ////////////////////////////////////////////
                /// Wait if ApplicationPool is starting now
                while (state == ObjectState.Starting)
                {
                    currentTimeSpan = DateTime.Now - startTime;
                    if (currentTimeSpan > nextTimeUpdateLog)
                    {
                        nextTimeUpdateLog += getPeriodUpdateInstallLogFor_StartStopAppPools();
                        session.Log(string.Format("Installer wait %d sec for '%s' ApplicationPool finish the start", (DateTime.Now - startTime).TotalSeconds, application.ApplicationPoolName));
                    }
                    if (currentTimeSpan > maxTimeSpan) 
                    {
                        session.Log(string.Format("Installer cant stop '%s' ApplicationPool for %d minutes, try skip it", application.ApplicationPoolName, Actions.MinutesToWaitStopApplicationPool));
                        break;
                    };
                    state = serverManager.ApplicationPools[application.ApplicationPoolName].State;
                    Thread.Sleep(100);
                }
                if (currentTimeSpan > maxTimeSpan)
                {
                    session.Log(string.Format("Stopping '%s' ApplicationPool is " + (isStopCurrentApplPoolSuccess?"":"not ") + "success"));
                    continue;
                }
                ////////////////////////////////////////////
                /// check ApplicationPool state
                state = serverManager.ApplicationPools[application.ApplicationPoolName].State;
                switch (state)
                {
                    case ObjectState.Stopped:
                        session.Log(string.Format("ApplicationPool %d already stopped", application.ApplicationPoolName));
                        break;
                    case ObjectState.Stopping: 
                        {
                            while (state == ObjectState.Stopping)
                            {
                                currentTimeSpan = DateTime.Now - startTime;
                                if (currentTimeSpan > nextTimeUpdateLog)
                                {
                                    nextTimeUpdateLog += getPeriodUpdateInstallLogFor_StartStopAppPools();
                                    session.Log(string.Format("Installer wait %d sec for '%s' ApplicationPool finish the stop", (DateTime.Now - startTime).TotalSeconds, application.ApplicationPoolName));
                                }
                                if (currentTimeSpan > maxTimeSpan)
                                {
                                    session.Log(string.Format("Installer cant stop '%s' ApplicationPool for %d minutes, try skip it", application.ApplicationPoolName, Actions.MinutesToWaitStopApplicationPool));
                                    break;
                                };
                                state = serverManager.ApplicationPools[application.ApplicationPoolName].State;
                                Thread.Sleep(100);
                            }
                        }
                        break;
                    case ObjectState.Started:
                    case ObjectState.Unknown:
                        try
                        {
                            serverManager.ApplicationPools[application.ApplicationPoolName].Stop();
                            session.Log(string.Format("Call command for stop '%s' ApplicationPool", application.ApplicationPoolName));
                            Thread.Sleep(100);
                            while (ObjectState.Stopped != serverManager.ApplicationPools[application.ApplicationPoolName].State)
                            {
                                currentTimeSpan = DateTime.Now - startTime;
                                if (currentTimeSpan > nextTimeUpdateLog)
                                {
                                    nextTimeUpdateLog += getPeriodUpdateInstallLogFor_StartStopAppPools();
                                    session.Log(string.Format("Installer wait %d sec for '%s' ApplicationPool finish the stop", (DateTime.Now - startTime).TotalSeconds, application.ApplicationPoolName));
                                }
                                if (currentTimeSpan > maxTimeSpan)
                                {
                                    session.Log(string.Format("Installer cant stop '%s' ApplicationPool for %d minutes, try skip it", application.ApplicationPoolName, Actions.MinutesToWaitStopApplicationPool));
                                    break;
                                };
                                state = serverManager.ApplicationPools[application.ApplicationPoolName].State;
                                Thread.Sleep(100);
                            }
                        }
                        catch (Exception ex)
                        {
                            session.Log(ex.Message);
                            session.Log(ex.StackTrace);
                        }
                        break;
                }

                foreach (WorkerProcess proc in serverManager.WorkerProcesses)
                {
                    foreach (var daApplication in GetSite(siteName, serverManager).Applications)
                    {
                        proc.Delete();
                    }
                }

                session.Log(string.Format("Stopping '%s' ApplicationPool is " + (isStopCurrentApplPoolSuccess ? "" : "not ") + "success"));
            }
            return isStoped;
        }

        private static bool StartApplicationPoolsOfSite(string siteName, ServerManager serverManager, Session session)
        {
            //todo chage code as "todo" on stoppools
            bool isStarted = true;

            foreach (var application in GetSite(siteName, serverManager).Applications)
            {
                var state = serverManager.ApplicationPools[application.ApplicationPoolName].State;
                try
                {
                    serverManager.ApplicationPools[application.ApplicationPoolName].Start();
                }
                catch (Exception ex)
                {
                    session.Log(ex.Message);
                    session.Log(ex.StackTrace);
                }
            }

            return isStarted;
        }

        private static Site CreateDefaultWebSite(ServerManager serverManager, string siteName)
        {
            serverManager.Sites.Add(siteName, @"C:\inetpub\wwwroot", 80);
            return serverManager.Sites.First(a => a.Name == siteName);
        }

        private static void EnableAnonymousAuthentication(Configuration config, string siteName)
        {
            var anonymouseAuthenticationSetion =
                    config.GetSection("system.webServer/security/authentication/anonymousAuthentication", siteName);
            anonymouseAuthenticationSetion["enabled"] = true;
        }

        private static Application GetApplication(Site site, string applicationName)
        {
            return site.Applications["/" + applicationName];
        }

        #endregion

        public static void MoveModules(Session session)
        {
            DeleteKeyFileByName(session, "nj6kjvk58s8wb2d73.key");
            DeleteKeyFileByName(session, "3sdfm98a3nz73h825.key");
            DeleteKeyFileByName(session, "m93sndf8a3z723h85.key");
            DeleteKeyFileByName(session, "a3nz73h8253sdfm98.key");
            DeleteKeyFileByName(session, "sfj10g4kg6aslhpbc7x.key");
            DeleteKeyFileByName(session, "d9q7tdkjxcv0zm30fd.key");
            DeleteKeyFileByName(session, "19gjqqcbuhfkn30djfm4.key");
            DeleteKeyFileByName(session, "slk13jkds649hba8d2.key");
            DeleteKeyFileByName(session, "q9d8dfk2cvns7ejm3.key");
            DeleteKeyFileByName(session, "qvmxo0r8cv13xnw8odzv.key");
            DeleteKeyFileByName(session, "dpf93jg28ffiq.key");
            DeleteKeyFileByName(session, "1gu3e30ccvnqigkc3z.key");
            DeleteKeyFileByName(session, "apqogc03mcj43xzy.key");
            DeleteKeyFileByName(session, "lsde428sf3qfusgr5siu.key");
            DeleteKeyFileByName(session, "8gs6anv5e43eyuq48.key");
            DeleteKeyFileByName(session, "lvppics03jcvkw38pa3.key");
            DeleteKeyFileByName(session, "d9q7tdkjxcv0df76fd.key");
            DeleteKeyFileByName(session, "99w92al30pgrxww6hn8r.key");
            DeleteKeyFileByName(session, "sdud08l1mc1obw70sa.key");
            DeleteKeyFileByName(session, "081cljjw7khcxqjv02.key");
            DeleteKeyFileByName(session, "h8djke5m3p90cn1ud.key");

            var dirs = System.IO.Directory.GetDirectories(session["INSTALLDIR"] + "\\Modules");
            foreach (var dir in dirs)
            {
                DirectoryCopy(dir.PathGetFullPath(), session["INSTALLDIR"], true);
            }

        }

        private static void DeleteKeyFileByName(Session session, string keyName)
        {
            if (string.IsNullOrEmpty(keyName)) return;

            string filePath = session["INSTALLDIR"] + "admin\\" + keyName;

            try
            {
                if (System.IO.File.Exists(filePath))
                {
                    System.IO.File.Delete(filePath);
                }
            }
            catch (Exception ex)
            {
                session.Log(ex.ToString());
            }
        }

        private static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
        {
            // Get the subdirectories for the specified directory.
            var dir = new System.IO.DirectoryInfo(sourceDirName);

            if (!dir.Exists)
            {
                throw new System.IO.DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }

            var dirs = dir.GetDirectories();
            // If the destination directory doesn't exist, create it.
            if (!System.IO.Directory.Exists(destDirName))
            {
                System.IO.Directory.CreateDirectory(destDirName);
            }

            // Get the files in the directory and copy them to the new location.
            var files = dir.GetFiles();
            foreach (var file in files)
            {
                string temppath = System.IO.Path.Combine(destDirName, file.Name);
                file.CopyTo(temppath, true);
            }

            // If copying subdirectories, copy them and their contents to new location.
            if (copySubDirs)
            {
                foreach (var subdir in dirs)
                {
                    string temppath = System.IO.Path.Combine(destDirName, subdir.Name);
                    DirectoryCopy(subdir.FullName, temppath, true);
                }
            }
        }


        [CustomAction]
        public static ActionResult PreInstall(Session session)
        {
            try
            {
                session.Log("Site restarting");
                using (var serverManager = new ServerManager())
                {
                    var siteName = session["SITE"];
                    var site = GetSite(siteName, serverManager);

                    site?.Stop();
                    site?.Start();
                }
            }
            catch (Exception ex)
            {
                session.Log("Error site restarting");
                session.Log(ex.Message);
                session.Log(ex.StackTrace);
                return ActionResult.Failure;
            }

            try
            {
                FileStream fs = System.IO.File.Create(@"D:\temp\PreInstall.txt");
                fs.WriteByte(55);
                fs.Close();
                session["INSTALLDIR"] = session["INSTALLPATH"];
                session["GUID"] = session["DYNGUID"];
                session["ID"] = session["DYNGUID"];
                try
                {
                    if (System.IO.Directory.Exists(System.IO.Path.GetPathRoot(Environment.SystemDirectory) + "ProgramData\\DeskAlertsServer"))
                        System.IO.Directory.Delete(System.IO.Path.GetPathRoot(Environment.SystemDirectory) + "ProgramData\\DeskAlertsServer");
                }
                catch (Exception)
                {

                    //ignore
                }
            }
            catch (Exception ex)
            {
                session.Log(ex.Message);
                if (!string.IsNullOrEmpty(ex.StackTrace))
                    session.Log(ex.StackTrace);

                return ActionResult.Failure;
            }
            return ActionResult.Success;
        }

        public static string GetConnectionString(Session session)
        {
            string connectionString;
            var sqlServer = session["SQLSERVER"];
            var dataBase = session["SQLDATABASE"];
            if (session["AZURESQL"] == "True")
            {
                var userId = session["SQLLOGIN"];
                var password = Encoding.Default.GetString(Convert.FromBase64String(session["SQLPASSWORD"]));
                connectionString =
                    $"Server=tcp:{sqlServer}.database.windows.net,1433;Initial Catalog={dataBase};Persist Security Info=False;User ID={userId};Password={password};MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
            }
            else if (session["SQLUSEAUTH"] == "True")
            {
                connectionString = $"Server={sqlServer};Integrated Security=True;";
            }
            else
            {
                var userId = session["SQLLOGIN"];
                var password = Encoding.Default.GetString(Convert.FromBase64String(session["SQLPASSWORD"]));
                connectionString =
                    $"Server={sqlServer};Integrated Security=false; user id={userId};Password={password};";
            }

            return connectionString;
        }

        private static IEnumerable<string> SplitSqlStatements(string sqlScript)
        {
            // Split by "GO" statements
            var statements = Regex.Split(
                sqlScript,
                @"^\s*GO\s*\d*\s*($|\-\-.*$)",
                RegexOptions.Multiline |
                RegexOptions.IgnorePatternWhitespace |
                RegexOptions.IgnoreCase);

            // Remove empties, trim, and return
            return statements
                .Where(x => !string.IsNullOrEmpty(x))
                .Select(x => x.Trim(' ', '\r', '\n'));
        }

        public static void ExecuteNonQueryBatch(string sqlserver, string file, Session session)
        {

            var fileContent = System.IO.File.ReadAllText(file);
            var sqlqueries = SplitSqlStatements(fileContent);

            var con = new SqlConnection(GetConnectionString(session));
            var cmd = new SqlCommand("query", con);
            //Set time in seconds to wait for the command to execute. 0 - no limit
            cmd.CommandTimeout = 0;
            con.Open();
            foreach (var query in sqlqueries)
            {
                cmd.CommandText = query;
                cmd.ExecuteNonQuery();
            }
            con.Close();
        }

        private static string ReplaceDatabaseArgs(string text, Session session, bool aleternative = false, bool sqlaut = false, bool createus2 = false)
        {
            if (isAzure)
            {
                text = text.Replace("USE master", string.Empty);
                text = text.Replace("USE [%dbname_hook%];", string.Empty);
                text = text.Replace("USE [%dbname_string%];", string.Empty);
            }

            text = text.Replace("%licenses_count%", session["LICENSES"]);
            text = text.Replace("%MaxPublishersCount%", session["MAXPUBLISHERSCOUNT"]);
            text = text.Replace("%dbname_string%", session["SQLDATABASE"]);
            text = text.Replace("%dbname_hook%", session["SQLDATABASE"]);
            text = text.Replace("%trialseconds_count%",
                string.IsNullOrEmpty(session["TRIALCOUNT"]) ? "0" : session["TRIALCOUNT"]);
            text = text.Replace("%dbname_both%", session["SQLDATABASE"]);
            text = text.Replace("%api_secret_string%", Guid.NewGuid().ToString());
            text = text.Replace("%deskalerts_version%", session["VERSION"]);

            text = text.Replace("%smtpServer%", session["SMTPSERVER"]);
            text = text.Replace("%smtpPort%", session["SMTPPORT"]);
            text = text.Replace("%smtpSsl%", Convert.ToInt32(bool.Parse(session["SMTPSSL"])).ToString());
            text = text.Replace("%smtpAuth%", Convert.ToInt32(bool.Parse(session["SMTPAUTH"])).ToString());
            text = text.Replace("%smtpUsername%", session["SMTPUSER"]);
            text = text.Replace("%smtpPassword%", session["SMTPPASSWORD"]);
            text = text.Replace("%smtpFrom%", session["SMTPUSER"]);

            text = text.Replace("%smsUrl%", session["SMSURL"]);
            text = text.Replace("%smsPostData%", session["SMSPOSTDATA"]);
            text = text.Replace("%smsContentType%", "application/x-www-form-urlencoded");
            text = text.Replace("%smsUseAuth%", "0");
            text = text.Replace("%smsAuthUser%", session["SMSAUTHUSER"]);
            text = text.Replace("%smsAuthPass%", session["SMSAUTHPASS"]);
            text = text.Replace("%smsProxy%", "0");
            text = text.Replace("%smsProxyServer%", string.Empty);
            text = text.Replace("%smsProxyBypassList%", string.Empty);
            text = text.Replace("%smsProxyUserAuth%", string.Empty);
            text = text.Replace("%smsProxyUser%", string.Empty);
            text = text.Replace("%smsProxyPass%", string.Empty);
            text = text.Replace("%smsPhoneForma%", "1");
            text = text.Replace("%smsCountryCode%", "US");
            text = text.Replace("%smsAuthKey%", "");

            text = text.Replace("%textToCallSID%", session["TTCALLUSER"]);
            text = text.Replace("%textToCallAuthToken%", session["TTCALLPASS"]);
            text = text.Replace("%textToCallNumber%", session["TTCALLPHONE"]);

            if (!aleternative)
            {
                text = text.Replace("%user_name_string%", session["SQLLOGIN"]);
                text = text.Replace("%user_name_hook%", session["SQLLOGIN"]);
            }
            else
            {
                if (createus2)
                {

                    text = text.Replace("%user_name_string%", "NT AUTHORITY\\IUSR");
                    text = text.Replace("%user_name_hook%", "NT AUTHORITY\\IUSR");
                }
                if (sqlaut)
                {
                    text = text.Replace("%user_name_string%",
                            Environment.UserDomainName + "\\" + Environment.UserName);
                    text = text.Replace("%user_name_hook%",
                        Environment.UserDomainName + "\\" + Environment.UserName);
                }
            }

            return text;
        }

        private static bool isAzure;

        static bool CreateDb(Session session)
        {
            try
            {
                isAzure = session["AZURESQL"] == "True";
                ExecuteScript("CreateDB.sql", session);
                if (session["DROPDATABASE"] == "True" && !isAzure)
                {
                    ExecuteScript("ClearDB.sql", session);
                }

                ExecuteScript("UpdateTables.sql", session);
                if (session["IsDemo"] == "IS_DEMO")
                {
                    ExecuteScript("CreateDemoUser.sql", session);
                }

                ExecuteScript("UpdateTables2.sql", session);
                ExecuteScript("CreateStored.sql", session);
                ExecuteScript("CreateIndexes.sql", session);
                ExecuteScript("AlertsReceivedTable.sql", session);
                ExecuteScript("UpdatePolicies.sql", session);
                ExecuteScript("Custom_contract_date.sql", session);
                ExecuteScript("Set_license_expire_date.sql", session);
                if (System.IO.File.Exists(session["INSTALLDIR"] + "\\sqlfiles\\" + "Custom.sql"))
                {
                    ExecuteScript("Custom.sql", session);
                }

                ExecuteScript("PolicySkins.sql", session);
                ExecuteScript("ContentSettings.sql", session);
                ExecuteScript("PolicyContentSettings.sql", session);
                ExecuteScript("users.sql", session);
                ExecuteScript("domains.sql", session);
                ExecuteScript("user_instances.sql", session);

                var di = new System.IO.DirectoryInfo(session["INSTALLDIR"] + "\\sqlfiles\\");
                foreach (var file in di.GetFiles())
                {
                    file.Delete();
                }
                foreach (var dir in di.GetDirectories())
                {
                    dir.Delete(true);
                }
                return true;
            }
            catch (Exception ex)
            {
                session.Log(ex.Message);
                session.Log(ex.StackTrace);
                return false;
            }
        }

        private static bool ExecuteScript(string scriptName, Session session)
        {
            try
            {
                string text = ReplaceDatabaseArgs(System.IO.File.ReadAllText(session["INSTALLDIR"] + "\\sqlfiles\\" + scriptName), session);
                System.IO.File.WriteAllText(session["INSTALLDIR"] + "\\sqlfiles\\" + scriptName, text);
                ExecuteNonQueryBatch(session["SQLSERVER"], session["INSTALLDIR"] + "\\sqlfiles\\" + scriptName, session);
                return true;
            }
            catch (Exception e)
            {
                session.Log(scriptName);
                session.Log(e.Message);
                session.Log(e.StackTrace);
                throw e;
            }
        }

        private static bool BuildConfig(Session session)
        {



            if (!System.IO.File.Exists(System.IO.Path.GetFullPath(session["INSTALLDIR"] + "admin\\config.blank.inc")))
                return false;
            var conentConfasp =
                System.IO.File.ReadAllText(System.IO.Path.GetFullPath(session["INSTALLDIR"] + "admin\\config.blank.inc"),
                    Encoding.Default);
            conentConfasp = conentConfasp.Replace("%alerts_dir%", session["IISURL"]);
            conentConfasp = conentConfasp.Replace("%alerts_folder%", session["INSTALLDIR"]);
            conentConfasp = conentConfasp.Replace("%host%", session["SQLSERVER"]);
            conentConfasp = conentConfasp.Replace("%dbname%", session["SQLDATABASE"]);
            conentConfasp = conentConfasp.Replace("%user_name%", session["SQLLOGIN"]);
            conentConfasp = conentConfasp.Replace("%password%", session["SQLPASSWORD"]);
            conentConfasp = conentConfasp.Replace("%win_auth%",
                Convert.ToInt32(bool.Parse(session["SQLUSEAUTH"])).ToString());
            conentConfasp = conentConfasp.Replace("%azure_sql%",
                Convert.ToInt32(bool.Parse(session["AZURESQL"])).ToString());
            conentConfasp = conentConfasp.Replace("%alerts_timeout%", "-1");

            if (System.IO.File.Exists(session["INSTALLDIR"] + "admin\\" + "nj6kjvk58s8wb2d73.key") &&
                (session["MODULEACTIVEDIRECTORY"] == "True"))
            {
                conentConfasp = conentConfasp + "\n<%\nAD=3\nConfDisableContextSupport=0\n%>";

            }
            if (System.IO.File.Exists(session["INSTALLDIR"] + "admin\\" + "3sdfm98a3nz73h825.key") &&
                (session["MODULESMS"] == "True"))
            {
                conentConfasp = conentConfasp + "<%" +
                                "\n\'---------------ADD TO CONFIG----------------" +
                                "\nDim smsUrl, smsPostData" +
                                "\nsmsUrl=\"" + session["SMSURL"] + "\"" +
                                "\nsmsPostData=\"" + session["SMSPOSTDATA"] + "\"" +
                                "\nsmsContentType = \"application/x-www-form-urlencoded\"" +
                                "\nsmsUseAuth=\"0\"" +
                                "\nsmsAuthUser = \"" + session["SMSAUTHUSER"] + "\"" +
                                "\nsmsAuthPass = \"" + session["SMSAUTHPASS"] + "\"" +
                                "\nsmsProxy=\"%smsProxy%\" \' \"0\" - System predefined, \"1\" - Direct (without proxy), \"2\" - Use proxy" +
                                "\nsmsProxyServer=\"%smsProxyServer%\" \' The name of a proxy server or a list of proxy server names. For example: localhost:8080" +
                                "\nsmsProxyBypassList=\"%smsProxyBypassList%\" \' The list of locally known host names or IP addresses for which you want to permit bypass of the proxy server." +
                                "\nsmsProxyUserAuth=\"%smsProxyUseAuth%\" \' \"0\" - Don\'t use, \"1\" - Use proxy authorization" +
                                "\nsmsProxyUser=\"%smsProxyUser%\"" +
                                "\nsmsProxyPass=\"%smsProxyPass%\"" +
                                "\nsmsPhoneFormat = \"1\" \'\"0\" - Don\'t convert phone numbers," +
                                "\n\t \' \"1\" - Just remove all special chars but not grouping symbols," +
                                "\n\t \' \"2\" - Just remove all special chars but not grouping symbols and not lead plus \"+\" symbol," +
                                "\n\t \' \"3\" - Just remove all special chars," +
                                "\n\t \' \"4\" - Just remove all special chars but not lead plus \"+\" symbol," +
                                "\n\t \' \"5\" - Convert letters to numeric," +
                                "\n\t \' \"6\" - Convert letters to numeric and remove all special chars," +
                                "\n\t \' \"7\" - Convert letters to numeric and remove all special chars but not lead plus \"+\" symbol," +
                                "\n\t \' \"8\" - Use international E.164 phone format," +
                                "\n\t \' \"9\" - Use international E.164 phone format but without lead plus \"+\" symbol." +
                                "\nsmsDefaultCountryCode = \"US\" \' ISO 3166-1 two-letter country code. Used for E.164 phone format on numbers without country prefix." +

                                "\nurlShortEnabled = \"1\"" +
                                "\nurlShortUrl = \"https://www.googleapis.com/urlshortener/v1/url\"" +
                                "\nurlShortPostData = \"{\"\"longUrl\"\": \"\"%url%\"\"}\"" +
                                "\nurlShortContentType = \"application/json\"" +
                                "\nurlShortResponseType = \"1\" \' \"1\" - JSON" +
                                "\nurlShortResponsePath = \"id\"" +
                                "\nSMS=1" +
                                "\n\'---------------ADD TO CONFIG----------------\r\n%>";


            }
            if (System.IO.File.Exists(session["INSTALLDIR"] + "admin\\" + "m93sndf8a3z723h85.key") &&
                (session["MODULEEMAIL"] == "True"))
            {
                conentConfasp = conentConfasp +
                                "<%" +
                                "\n\'---------------ADD TO CONFIG----------------" +
                                "\nDim smtpServer, smtpPort, smtpSSL, smtpAuth, smtpUsername, smtpPassword" +
                                "\nsmtpServer=\"" + session["SMTPSERVER"] + "\"" +
                                "\nsmtpPort=\"" + session["SMTPPORT"] + "\"" +
                                "\nsmtpSSL=\"" + Convert.ToInt32(bool.Parse(session["SMTPSSL"])).ToString() + "\"" +
                                "\nsmtpAuth=\"" + Convert.ToInt32(bool.Parse(session["SMTPAUTH"])).ToString() + "\"" +
                                "\nsmtpUsername=\"" + session["SMTPUSER"] + "\"" +
                                "\nsmtpPassword=\"" + session["SMTPPASSWORD"] + "\"" +
                                "\nsmtpFrom=\"" + session["SMTPUSER"] + "\"" +
                                "\nsmtpConnectTimeout=\"60\"" +
                                "\nEM=1" +
                                "\n\'---------------ADD TO CONFIG----------------\r\n%>";

            }
            if (System.IO.File.Exists(session["INSTALLDIR"] + "admin\\" + "a3nz73h8253sdfm98.key") &&
                (session["MODULEENCRYPT"] == "True"))
            {
                conentConfasp = conentConfasp +
                                "<%" +
                                "\n\'---------------ADD TO CONFIG----------------" +
                                "\nDim ENC, encrypt_key" +
                                "\n\'ENC=0 - disable" +
                                "\n\'ENC=1 - enable" +
                                "\nENC=1" +
                                "\nencrypt_key=\"" + session["ENCKEY"] + "\"" +

                                "\n\'---------------ADD TO CONFIG----------------\r\n%>";

            }
            if (System.IO.File.Exists(session["INSTALLDIR"] + "admin\\" + "sfj10g4kg6aslhpbc7x.key") &&
                (session["MODULERSS"] == "True"))
            {
                conentConfasp = conentConfasp +
                                "<%" +
                                "\n\'---------------ADD TO CONFIG----------------" +
                                "\nRSS = 1" +
                                "\nrssProxy=\"%rssProxy%\" \' \"0\" - System predefined, \"1\" - Direct (without proxy), \"2\" - Use proxy" +
                                "\nrssProxyServer=\"%rssProxyServer%\" \' The name of a proxy server or a list of proxy server names. For example: localhost:8080" +
                                "\nrssProxyBypassList=\"%rssProxyBypassList%\" \' The list of locally known host names or IP addresses for which you want to permit bypass of the proxy server." +
                                "\nrssProxyUseAuth=\"%rssProxyUseAuth%\" \' \"0\" - Don\'t use, \"1\" - Use proxy authorization" +
                                "\nrssProxyUser=\"%rssProxyUser%\"" +
                                "\nrssProxyPass=\"%rssProxyPass%\"" +
                                "\n\'---------------ADD TO CONFIG----------------\r\n%>";

            }
            if (System.IO.File.Exists(session["INSTALLDIR"] + "admin\\" + "d9q7tdkjxcv0zm30fd.key") &&
                (session["SCREENSAVER"] == "True"))
            {
                conentConfasp = conentConfasp +
                                "<%" +
                                "\n\'---------------ADD TO CONFIG----------------" +
                                "\nSS = 1" +
                                "\n\'---------------ADD TO CONFIG----------------\r\n%>";

            }
            if (System.IO.File.Exists(session["INSTALLDIR"] + "admin\\" + "19gjqqcbuhfkn30djfm4.key") &&
                (session["MODULEWALPAPER"] == "True"))
            {
                conentConfasp = conentConfasp +
                                "<%" +
                                "\n\'---------------ADD TO CONFIG----------------" +
                                "\nWP = 1" +
                                "\n\'---------------ADD TO CONFIG----------------\r\n%>";


            }
            /*if (System.IO.File.Exists(session["INSTALLDIR"] + "admin\\" + "slk13jkds649hba8d2.key") &&
                (session["MODULEBLOG"] == "True"))
            {
                conentConfasp = conentConfasp +
                                "<%" +
                                "\n\'---------------ADD TO CONFIG----------------" +
                                "\nBLOG = 1" +
                                "\n\'---------------ADD TO CONFIG----------------\r\n%>";

            }*/
            if (System.IO.File.Exists(session["INSTALLDIR"] + "admin\\" + "q9d8dfk2cvns7ejm3.key") &&
                (session["MODULETWITTER"] == "True"))
            {
                conentConfasp = conentConfasp +
                                "<%" +
                                "\n\'---------------ADD TO CONFIG----------------" +
                                "\nSOCIAL_MEDIA = 1" +
                                "\n\'---------------ADD TO CONFIG----------------\r\n%>";

            }
            if (System.IO.File.Exists(session["INSTALLDIR"] + "admin\\" + "qvmxo0r8cv13xnw8odzv.key") &&
                (session["MODULELINKEDIN"] == "True"))
            {
                conentConfasp = conentConfasp +
                                "<%" +
                                "\n\'---------------ADD TO CONFIG----------------" +
                                "\nSOCIAL_LINKEDIN = 1" +
                                "\n\'---------------ADD TO CONFIG----------------\r\n%>";

            }
            if (System.IO.File.Exists(session["INSTALLDIR"] + "admin\\" + "dpf93jg28ffiq.key") &&
                (session["MODULEFULLSCREEN"] == "True"))
            {
                conentConfasp = conentConfasp +
                                "<%" +
                                "\n\'---------------ADD TO CONFIG----------------" +
                                "\nFULLSCREEN_CFG = 1" +
                                "\n\'---------------ADD TO CONFIG----------------\r\n%>";

            }
            if (System.IO.File.Exists(session["INSTALLDIR"] + "admin\\" + "1gu3e30ccvnqigkc3z.key") &&
                (session["MODULETICKER"] == "True"))
            {
                conentConfasp = conentConfasp +
                                "<%" +
                                "\n\'---------------ADD TO CONFIG----------------" +
                                "\nTICKER_CFG = 1" +
                                "\n\'---------------ADD TO CONFIG----------------\r\n%>";

            }
            if (System.IO.File.Exists(session["INSTALLDIR"] + "admin\\" + "dwpcv82mc7apzijq3avp.key") &&
                (session["MODULEIM"] == "True"))
            {
                conentConfasp = conentConfasp +
                                "<%" +
                                "\n\'---------------ADD TO CONFIG----------------" +
                                "\nIM = 1" +
                                "\n\'---------------ADD TO CONFIG----------------\r\n%>";
            }
            if (System.IO.File.Exists(session["INSTALLDIR"] + "admin\\" + "lsde428sf3qfusgr5siu.key"))
            //it is in standart package and installer not controls it)
            {
                conentConfasp = conentConfasp +
                                "<%" +
                                "\n\'---------------ADD TO CONFIG----------------" +
                                "\nWIDGETSTAT = 1" +
                                "\n\'---------------ADD TO CONFIG----------------\r\n%>";

            }
            if (System.IO.File.Exists(session["INSTALLDIR"] + "admin\\" + "8gs6anv5e43eyuq48.key") &&
                (session["MODULETEXTTOCALL"] == "True"))
            {
                conentConfasp = conentConfasp +
                                "<%" +
                                "\n\'---------------ADD TO CONFIG----------------" +
                                "\nDim textcallUrl, textcallPostData" +
                                "\n\' Use the following variables: %mobilePhone% as a mobile phone number, %textcallText% as a textcall text" +
                                "\ntextcallUseAuth=\"1\" \' \"0\" - Don\'t use, \"1\" - Use www-authorization" +
                                "\ntextcallAuthUser=\"" + session["TTCALLUSER"] + "\"" +
                                "\ntextcallAuthPass=\"" + session["TTCALLPASS"] + "\"" +
                                "\nphoneUser=\"" + session["TTCALLPHONE"] + "\"" +
                                "\ntextcallUrl = \"https://api.twilio.com/2010-04-01/Accounts/" + session["TTCALLUSER"] +
                                "/Calls\"" +
                                "\ntextcallPostData = \"From=%userPhone%&To=%mobilePhone%&Url=%callText%\"" +
                                "\ntextcallContentType = \"application/x-www-form-urlencoded\"" +
                                "\ntextcallPhoneFormat = \"1\"   \' \"0\" - Don\'t convert phone numbers," +
                                "\n\t\' \"1\" - Just remove all special chars but not grouping symbols," +
                                "\n\t\' \"2\" - Just remove all special chars but not grouping symbols and not lead plus \"+\" symbol," +
                                "\n\t\' \"3\" - Just remove all special chars," +
                                "\n\t\' \"4\" - Just remove all special chars but not lead plus \"+\" symbol,Phone " +
                                "\n\t\' \"5\" - Convert letters to numeric," +
                                "\n\t\' \"6\" - Convert letters to numeric and remove all special chars," +
                                "\n\t\' \"7\" - Convert letters to numeric and remove all special chars but not lead plus \"+\" symbol," +
                                "\n\t\' \"8\" - Use international E.164 phone format," +
                                "\n\t\' \"9\" - Use international E.164 phone format but without lead plus \"+\" symbol." +
                                "\ntextcallDefaultCountryCode = \"US\" \' ISO 3166-1 two-letter country code. Used for E.164 phone format on numbers without country prefix." +
                                "\n" +
                                "\n" +
                                "\n\'---------------ADD TO CONFIG----------------\r\n%>";

            }

            if (System.IO.File.Exists(session["INSTALLDIR"] + "admin\\" + "lvppics03jcvkw38pa3.key") &&
                (session["MODULEMOBILE"] == "True"))
            {
                conentConfasp = conentConfasp +
                                "<%" +
                                "\n\'---------------ADD TO CONFIG----------------" +
                                "\nMOBILE_SEND = 1" +
                                "\n\'---------------ADD TO CONFIG----------------\r\n%>";

            }
            if ((System.IO.File.Exists(session["INSTALLDIR"] + "admin\\" + "d9q7tdkjxcv0df76fd.key")) &&
                (session["MODULEVIDEO"] == "True"))
            {
                conentConfasp = conentConfasp +
                                "<%" +
                                "\n\'---------------ADD TO CONFIG----------------" +
                                "\nVIDEO = 1" +
                                "\n\'---------------ADD TO CONFIG----------------\r\n%>";

            }

            if (session["MODULECAMPAIGN"] == "True")
            {
                conentConfasp = conentConfasp +
                                "<%" +
                                "\n\'---------------ADD TO CONFIG----------------" +
                                "\nCAMPAIGN = 1" +
                                "\n\'---------------ADD TO CONFIG----------------\r\n%>";
            }

            if (session["MODULERESTAPI"] == "True")
            {
                conentConfasp = conentConfasp +
                                "<%" +
                                "\n\'---------------ADD TO CONFIG----------------" +
                                "\nRESTAPI = 1" +
                                "\n\'---------------ADD TO CONFIG----------------\r\n%>";
            }

            if (session["MODULEMULTILANGUAGE"] == "True")
            {
                conentConfasp = conentConfasp +
                                "<%" +
                                "\n\'---------------ADD TO CONFIG----------------" +
                                "\nMULTILANGUAGE = 1" +
                                "\n\'---------------ADD TO CONFIG----------------\r\n%>";
            }

            if (session["MODULESMPP"] == "True")
            {
                conentConfasp = conentConfasp +
                                "<%" +
                                "\n\'---------------ADD TO CONFIG----------------" +
                                "\nSMPP = 1" +
                                "\n\'---------------ADD TO CONFIG----------------\r\n%>";
            }

            if (session["MODULEMULTIPLEALERTS"] == "True")
            {
                conentConfasp = conentConfasp +
                                "<%" +
                                "\n\'---------------ADD TO CONFIG----------------" +
                                "\nMULTIPLE = 1" +
                                "\n\'---------------ADD TO CONFIG----------------\r\n%>";
            }

            if (System.IO.File.Exists(session["INSTALLDIR"] + "admin\\" + "sdud08l1mc1obw70sa.key") &&
                (session["MODULEAPPROVE"] == "True"))
            {
                conentConfasp = conentConfasp +
                                "<%" +
                                "\n\'---------------ADD TO CONFIG----------------" +
                                "\nAPPROVE = 1" +
                                "\n\'---------------ADD TO CONFIG----------------\r\n%>";

            }
            if (System.IO.File.Exists(session["INSTALLDIR"] + "admin\\" + "h8djke5m3p90cn1ud.key"))
            {

                //ignore. Surveys are in include file
            }

            if (System.IO.File.Exists(session["INSTALLDIR"] + "admin\\config.inc"))
            {
                System.IO.File.Delete(session["INSTALLDIR"] + "admin\\config.backup.inc");
            }
            System.IO.File.WriteAllText(session["INSTALLDIR"] + "admin\\config.inc", conentConfasp);
            return true;
        }


        public static bool WriteToAxpxConfig(Session session)
        {
            try
            {
                if (!System.IO.File.Exists(session["INSTALLDIR"] + "\\web.blank.config"))
                {
                    return false;
                }

                string conentConfaspx = System.IO.File.ReadAllText(session["INSTALLDIR"] + "\\web.blank.config");
                conentConfaspx = conentConfaspx.Replace("%alerts_dir%", session["IISURL"]);
                conentConfaspx = conentConfaspx.Replace("%alerts_folder%", session["INSTALLDIR"]);
                conentConfaspx = conentConfaspx.Replace("%DB_SOURCE%", session["SQLSERVER"]);
                conentConfaspx = conentConfaspx.Replace("%DB_NAME%", session["SQLDATABASE"]);
                conentConfaspx = conentConfaspx.Replace("%DB_USER%", session["SQLLOGIN"]);
                conentConfaspx = conentConfaspx.Replace("%DB_PASS%", session["SQLPASSWORD"]);
                conentConfaspx = conentConfaspx.Replace("%WIN_AUTH%",
                    Convert.ToInt32(bool.Parse(session["SQLUSEAUTH"])).ToString());
                conentConfaspx = conentConfaspx.Replace("%AZURE_SQL%",
                    Convert.ToInt32(bool.Parse(session["AZURESQL"])).ToString());
                conentConfaspx = conentConfaspx.Replace("%alerts_timeout%", "-1");
                conentConfaspx = conentConfaspx.Replace("%SITE%", session["SITE"]);
                var newConentConfx = string.Empty;

                if (session["IS_DEMO"] == "True")
                {
                    newConentConfx = AddParameterToConfigFileData(newConentConfx, "DEMO", "1");
                }

                if (System.IO.File.Exists(session["INSTALLDIR"] + "admin\\" + "nj6kjvk58s8wb2d73.key") && (session["MODULEACTIVEDIRECTORY"] == "True"))
                {
                    newConentConfx = AddParameterToConfigFileData(newConentConfx, "AD", "1");
                }

                if (session["MODULESSO"] == "True")
                {
                    newConentConfx = AddParameterToConfigFileData(newConentConfx, "SSO", "1");
                }

                if (System.IO.File.Exists(session["INSTALLDIR"] + "admin\\" + "3sdfm98a3nz73h825.key") && (session["MODULESMS"] == "True"))
                {
                    newConentConfx = AddParameterToConfigFileData(newConentConfx, "SMS", "1");
                }

                if (System.IO.File.Exists(session["INSTALLDIR"] + "admin\\" + "m93sndf8a3z723h85.key") && (session["MODULEEMAIL"] == "True"))
                {
                    newConentConfx = AddParameterToConfigFileData(newConentConfx, "EMAIL", "1");
                }

                if (System.IO.File.Exists(session["INSTALLDIR"] + "admin\\" + "a3nz73h8253sdfm98.key") && (session["MODULEENCRYPT"] == "True"))
                {
                    newConentConfx = AddParameterToConfigFileData(newConentConfx, "ENCRYPT", "1");
                    newConentConfx = AddParameterToConfigFileData(newConentConfx, "ENCRYPT_KEY", $"{session["ENCKEY"]}");
                }

                if (System.IO.File.Exists(session["INSTALLDIR"] + "admin\\" + "sfj10g4kg6aslhpbc7x.key") && (session["MODULERSS"] == "True"))
                {
                    newConentConfx = AddParameterToConfigFileData(newConentConfx, "RSS", "1");
                }

                if (System.IO.File.Exists(session["INSTALLDIR"] + "admin\\" + "d9q7tdkjxcv0zm30fd.key") && (session["SCREENSAVER"] == "True"))
                {
                    newConentConfx = AddParameterToConfigFileData(newConentConfx, "SCREENSAVER", "1");
                }

                if (System.IO.File.Exists(session["INSTALLDIR"] + "admin\\" + "19gjqqcbuhfkn30djfm4.key") && (session["MODULEWALPAPER"] == "True"))
                {
                    newConentConfx = AddParameterToConfigFileData(newConentConfx, "WALLPAPER", "1");
                }

                // TODO ��������� ����!!!
                if (System.IO.File.Exists(session["INSTALLDIR"] + "admin\\" + "cxhgqcatr2fajvsx99mk.key") && (session["MODULELOCKSCREEN"] == "True"))
                {
                    newConentConfx = AddParameterToConfigFileData(newConentConfx, "LOCKSCREEN", "1");
                }

                if (System.IO.File.Exists(session["INSTALLDIR"] + "admin\\" + "slk13jkds649hba8d2.key") && (session["MODULEBLOG"] == "True"))
                {
                    newConentConfx = AddParameterToConfigFileData(newConentConfx, "BLOG", "1");
                }

                if (System.IO.File.Exists(session["INSTALLDIR"] + "admin\\" + "q9d8dfk2cvns7ejm3.key") && (session["MODULETWITTER"] == "True"))
                {
                    newConentConfx = AddParameterToConfigFileData(newConentConfx, "TWITTER", "1");
                }

                if (System.IO.File.Exists(session["INSTALLDIR"] + "admin\\" + "qvmxo0r8cv13xnw8odzv.key") && (session["MODULELINKEDIN"] == "True"))
                {
                    newConentConfx = AddParameterToConfigFileData(newConentConfx, "LINKEDIN", "1");
                }

                if (System.IO.File.Exists(session["INSTALLDIR"] + "admin\\" + "dpf93jg28ffiq.key") && (session["MODULEFULLSCREEN"] == "True"))
                {
                    newConentConfx = AddParameterToConfigFileData(newConentConfx, "FULLSCREEN", "1");
                }

                if (System.IO.File.Exists(session["INSTALLDIR"] + "admin\\" + "1gu3e30ccvnqigkc3z.key") && (session["MODULETICKER"] == "True"))
                {
                    newConentConfx = AddParameterToConfigFileData(newConentConfx, "TICKER", "1");
                    newConentConfx = AddParameterToConfigFileData(newConentConfx, "TICKER_PRO", "1");
                }

                if (System.IO.File.Exists(session["INSTALLDIR"] + "admin\\" + "dwpcv82mc7apzijq3avp.key") && (session["MODULEIM"] == "True"))
                {
                    newConentConfx = AddParameterToConfigFileData(newConentConfx, "EMERGENCY", "1");
                }

                if (System.IO.File.Exists(session["INSTALLDIR"] + "admin\\" + "lsde428sf3qfusgr5siu.key") && (session["MODULESTATISTICS"] == "True"))
                {
                    newConentConfx = AddParameterToConfigFileData(newConentConfx, "STATISTICS", "1");
                }

                if (System.IO.File.Exists(session["INSTALLDIR"] + "admin\\" + "8gs6anv5e43eyuq48.key") && (session["MODULETEXTTOCALL"] == "True"))
                {
                    newConentConfx = AddParameterToConfigFileData(newConentConfx, "TEXTTOCALL", "1");
                }

                if (System.IO.File.Exists(session["INSTALLDIR"] + "admin\\" + "lvppics03jcvkw38pa3.key") && (session["MODULEMOBILE"] == "True"))
                {
                    newConentConfx = AddParameterToConfigFileData(newConentConfx, "MOBILE", "1");
                }

                if (System.IO.File.Exists(session["INSTALLDIR"] + "admin\\" + "d9q7tdkjxcv0df76fd.key") && (session["MODULEVIDEO"] == "True"))
                {
                    newConentConfx = AddParameterToConfigFileData(newConentConfx, "VIDEO", "1");
                }

                if (session["MODULESMPP"] == "True")
                {
                    newConentConfx = AddParameterToConfigFileData(newConentConfx, "SMPP", "1");
                }

                if (session["MODULEMULTIPLEALERTS"] == "True")
                {
                    newConentConfx = AddParameterToConfigFileData(newConentConfx, "MULTIPLE", "1");
                }

                if (session["MODULECAMPAIGN"] == "True")
                {
                    newConentConfx = AddParameterToConfigFileData(newConentConfx, "CAMPAIGN", "1");
                }
                if (session["MODULERESTAPI"] == "True")
                {
                    newConentConfx = AddParameterToConfigFileData(newConentConfx, "RESTAPI", "1");
                }

                if (System.IO.File.Exists(session["INSTALLDIR"] + "admin\\" + "sdud08l1mc1obw70sa.key") && (session["MODULEAPPROVE"] == "True"))
                {
                    newConentConfx = AddParameterToConfigFileData(newConentConfx, "APPROVE", "1");
                }
                if (session["MODULEMULTILANGUAGE"] == "True")
                {
                    newConentConfx = AddParameterToConfigFileData(newConentConfx, "MULTILANGUAGE", "1");
                }

                if (session["MODULEDESIGN"] == "True")
                {
                    newConentConfx = AddParameterToConfigFileData(newConentConfx, "DIGITAL_SIGNAGE", "1");
                }

                if (System.IO.File.Exists(session["INSTALLDIR"] + "admin\\" + "h8djke5m3p90cn1ud.key") && (session["MODULESURVEY"] == "True"))
                {
                    newConentConfx = AddParameterToConfigFileData(newConentConfx, "SURVEYS", "1");
                }

                if (session["MODULEBULKUPLOADOFRECIPIENTS"] == "True")
                {
                    newConentConfx = AddParameterToConfigFileData(newConentConfx, "BULK_UPLOAD_OF_RECIPIENTS", "1");
                }

                newConentConfx = AddParameterToConfigFileData(newConentConfx, "IMPORT_PHONES", "0");

                newConentConfx = $"{newConentConfx}\r\n";

                conentConfaspx = conentConfaspx.Replace("<appSettings>", "<appSettings>\r\n\t" + newConentConfx);
                if (System.IO.File.Exists(session["INSTALLDIR"] + "\\web.config"))
                {
                    System.IO.File.Delete(session["INSTALLDIR"] + "\\web.backup.config");
                }
                System.IO.File.WriteAllText(session["INSTALLDIR"] + "\\web.config", conentConfaspx);

                return true;
            }
            catch (Exception ex)
            {
                session.Log(ex.ToString());
                return false;
            }

        }

        private static string AddParameterToConfigFileData(string configFileData, string module, string value)
        {
            if (string.IsNullOrEmpty(module) || string.IsNullOrEmpty(value))
            {
                return configFileData;
            }

            configFileData = $"{configFileData}<add key=\'{module}\' value=\'{value}\' />\r\n\t";

            return configFileData;
        }

        private static void UnregisterDll(string installDirectory, string fileName)
        {
            var path = installDirectory + fileName;
            if (System.IO.File.Exists(path))
            {
                var argFileinfo = $"/s /u \"{path}\"";
                var reg = new Process
                {
                    StartInfo =
                    {
                        FileName = "regsvr32.exe",
                        Arguments = argFileinfo,
                        UseShellExecute = false,
                        CreateNoWindow = true,
                        RedirectStandardOutput = true
                    }
                };
                reg.Start();
                reg.WaitForExit();
                reg.Close();
            }
        }
        

        private static void RegisterDll(string installDirectory, string fileName)
        {
            var path = installDirectory + fileName;
            if (System.IO.File.Exists(path))
            {
                var argFileinfo = $"/s \"{path}\"";
                var reg = new Process
                {
                    StartInfo =
                    {
                        FileName = "regsvr32.exe",
                        Arguments = argFileinfo,
                        UseShellExecute = false,
                        CreateNoWindow = true,
                        RedirectStandardOutput = true
                    }
                };
                reg.Start();
                reg.WaitForExit();
                reg.Close();
            }
        }

        private static bool RegisterDll(Session session)
        {
            RegisterDll(session["INSTALLDIR"], @"\DLL\DeskAlertsServer32.dll");
            UnregisterDll(session["INSTALLDIR"], @"\DLL\DeskAlertsServer64.dll");
            RegisterDll(session["INSTALLDIR"], @"\DLL\DeskAlertsServer64.dll");

            return true;
        }

        [CustomAction]
        public static ActionResult InstallService(Session session)
        {
            return session.HandleErrors(() =>
            {
                try
                {
                    Tasks.InstallService(session.Property("INSTALLDIR") + @"\Service\daadi.exe", true);
                }
                catch
                {
                    MessageBox.Show("Error is occurent when install Daadi service");
                }

                try
                {
                    Tasks.StartService("DAADISvc", false);
                }
                catch
                {
                    MessageBox.Show("Error is occurent when start Daadi service");
                }
            });
        }

        [CustomAction]
        public static ActionResult PostInstall(Session session)
        {
            try
            {
                try
                {
                    MoveModules(session);
                }
                catch (Exception ex)
                {
                    session.Log("Error moving modules");
                    session.Log(ex.Message);
                    session.Log(ex.StackTrace);
                    return ActionResult.Failure;
                }

                try
                {
                    System.IO.Directory.Delete(session["INSTALLDIR"] + "\\Modules", true);
                }
                catch (Exception ex)
                {
                    session.Log("Error deleting modules directory");
                    session.Log(ex.Message);
                    session.Log(ex.StackTrace);
                }


                try
                {
                    GarantAccess(session["%WindowsFolder%"] + "System32\\inetsrv\\config\\redirection.config");
                }
                catch (Exception ex)
                {
                    session.Log(ex.Message);
                    session.Log(ex.StackTrace);
                }

                try
                {
                    session.Log("CreateDB");
                    if (!CreateDb(session)) return ActionResult.Failure;
                }
                catch (Exception ex)
                {
                    session.Log("Error creating db");
                    session.Log(ex.Message);
                    session.Log(ex.StackTrace);
                    return ActionResult.Failure;
                }

                try
                {
                    session.Log("BuildConfig");
                    if (!BuildConfig(session))
                    {
                        session.Log("asp config.blank.inc is not exists");
                        return ActionResult.Failure;
                    }
                }
                catch (Exception ex)
                {
                    session.Log("Error creating asp config file");
                    session.Log(ex.Message);
                    session.Log(ex.StackTrace);
                    return ActionResult.Failure;
                }

                try
                {
                    session.Log("WriteToAxpxConfig");
                    if (!WriteToAxpxConfig(session)) return ActionResult.Failure;
                }
                catch (Exception ex)
                {
                    session.Log("Error creating aspx config file");
                    session.Log(ex.Message);
                    session.Log(ex.StackTrace);
                    return ActionResult.Failure;
                }

                try
                {
                    if (!UpdateNLogConfig(session, "\\NLog.blank.config"))
                    {
                        return ActionResult.Failure;
                    }
                }
                catch (Exception ex)
                {
                    session.Log("NLog.config file not updated or not found.");
                    session.Log(ex.Message);
                    session.Log(ex.StackTrace);
                    return ActionResult.Failure;
                }

                try
                {
                    session.Log("RegisterDll");
                    if (!RegisterDll(session))
                        return ActionResult.Failure;
                }
                catch (Exception ex)
                {
                    session.Log("Error registering dll");
                    session.Log(ex.Message);
                    session.Log(ex.StackTrace);
                    return ActionResult.Failure;
                }

                try
                {
                    session.Log("CreateApplication");
                    var siteName = session["SITE"];
                    var uri = new Uri(session["IISURL"]);
                    var applicationName = uri.LocalPath.TrimStart('/');
                    var physicalPath = Path.GetFullPath(session["INSTALLDIR"]);
                    CreateApplication(siteName, applicationName, physicalPath, session["OLDINSTALLDIR"], session["NEWDATABASE"] == "True");
                }
                catch (Exception ex)
                {
                    session.Log("Error creating web application");
                    session.Log(ex.Message);
                    session.Log(ex.StackTrace);
                    return ActionResult.Failure;
                }

                try
                {
                    session.Log("Site restarting");
                    using (var serverManager = new ServerManager())
                    {
                        var siteName = session["SITE"];
                        var site = GetSite(siteName, serverManager);
                        site?.Stop();
                        site?.Start();
                    }
                }
                catch (Exception ex)
                {
                    session.Log("Error site restarting");
                    session.Log(ex.Message);
                    session.Log(ex.StackTrace);
                    return ActionResult.Failure;
                }

                try
                {
                    session.Log("AddMime");
                    //add MIME type for files with *.bat extention to make emergency shortcuts work
                    string mimeTypeAddCommand = "/C cd %windir%\\system32\\inetsrv & appcmd set config /section:staticContent /+\"[fileExtension='.bat',mimeType='application/bat']\"";
                    Process.Start("cmd.exe", mimeTypeAddCommand);
                }
                catch (Exception ex)
                {
                    session.Log("Error adding mime types");
                    session.Log(ex.Message);
                    session.Log(ex.StackTrace);
                }
            }
            catch (Exception ex)
            {
                session.Log(ex.Message);
                session.Log(ex.StackTrace);
                return ActionResult.Failure;
            }
            return ActionResult.Success;
        }

        [CustomAction]
        public static ActionResult Uninstall(Session session)
        {
            try
            {
                using (var serverManager = new ServerManager())
                {
                    var path = session["INSTALLDIR"] + "/web.config";
                    var doc = new XmlDocument();
                    doc.Load(path);
                    var xnList = doc.SelectNodes("/configuration/appSettings")?[0];
                    string alertsDir = string.Empty, siteName = string.Empty;
                    if (xnList != null)
                    {
                        foreach (XmlNode node in xnList)
                        {
                            if (node.Attributes != null && node.Attributes["key"].Value == "site")
                            {
                                siteName = node.Attributes["value"].Value;
                                session.Log($"siteName is {siteName}");
                            }

                            if (node.Attributes != null && node.Attributes["key"].Value == "alertsDir")
                            {
                                session.Log($"alertsDir is {alertsDir}");
                                alertsDir = node.Attributes["value"].Value;
                            }
                        }
                    }

                    var site = serverManager.Sites[siteName];
                    session.Log($"Site {siteName} is found");
                    var uri = new Uri(alertsDir);
                    if (!string.IsNullOrEmpty(uri.LocalPath))
                    {
                        var applicationName = uri.LocalPath;
                        if (site != null)
                        {
                            var application = site.Applications[applicationName];
                            session.Log($"Application {applicationName} is found");
                            if (application.Path != "/")
                            {
                                site.Applications.Remove(application);
                            }
                        }

                        serverManager.CommitChanges();
                    }
                }
            }
            catch
            {
                MessageBox.Show("Error reading web.config file");
            }

            return ActionResult.Success;
        }

        [CustomAction]
        public static ActionResult UninstallService(Session session)
        {
            return session.HandleErrors(() =>
            {
                try
                {
                    Tasks.InstallService(session.Property("INSTALLDIR") + @"\Service\daadi.exe", false);
                }
                catch
                {
                    MessageBox.Show("Error is occurent when uninstall Daadi service");
                }
            });
        }

        [CustomAction]
        public static ActionResult StartAppPools(Session session) 
        {
            using (var serverManager = new ServerManager())
            {
                var siteName = session["SITE"];
                StartApplicationPoolsOfSite(siteName, serverManager, session);
            }
            return ActionResult.Success;
        }

        [CustomAction]
        public static ActionResult StopAppPools(Session session)
        {
            using (var serverManager = new ServerManager())
            {
                var siteName = session["SITE"];
                StopApplicationPoolsOfSite(siteName, serverManager, session);
            }
            return ActionResult.Success;
        }

        /// <summary>
        /// Update Nlog.config file for update SQL server connection string.
        /// </summary>
        /// <param name="session">WIX install session.</param>
        /// <param name="nlogConfigFile">Nlog.config file.</param>
        /// <returns></returns>
        private static bool UpdateNLogConfig(Session session, string nlogConfigFile)
        {
            bool res = false;

            if (nlogConfigFile.Length > 0)
            {
                nlogConfigFile = $"{session["INSTALLDIR"]}\\{nlogConfigFile}";

                try
                {
                    if (System.IO.File.Exists(nlogConfigFile))
                    {
                        var nlogConfig = System.IO.File.ReadAllText(nlogConfigFile);

                        try
                        {
                            System.IO.File.WriteAllText(session["INSTALLDIR"] + "\\NLog.config", nlogConfig);
                            System.IO.File.Delete(session["INSTALLDIR"] + "\\NLog.blank.config");
                        }
                        catch (Exception ex)
                        {
                            session.Log("NLog.config doesn't exist.");
                            session.Log(ex.Message);
                            session.Log(ex.StackTrace);
                        }
                    }
                    else
                    {
                        session.Log("NLog.config not found.");
                    }
                    res = true;
                }
                catch (Exception ex)
                {
                    session.Log("NLog.config is not updated.");
                    session.Log(ex.Message);
                    session.Log(ex.StackTrace);
                }
            }
            else
            {
                session.Log("NLog.config is not updated.");
            }

            return res;
        }
    }
}