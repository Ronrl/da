USE [%dbname_hook%];
	
	UPDATE [policy_list]
	SET alerts_val =
	CASE
		WHEN LEN(alerts_val) >= 8 THEN alerts_val
		WHEN LEN(alerts_val) < 8 THEN alerts_val + '0'
		WHEN [alerts_val] IS NULL THEN '00000000' ELSE [alerts_val]
	END

	UPDATE [policy_list]
	SET [emails_val] =
	CASE
		WHEN LEN([emails_val]) >= 8 THEN [emails_val]
		WHEN LEN([emails_val]) < 8 THEN [emails_val] + '0'
		WHEN [emails_val] IS NULL THEN '00000000' ELSE [emails_val]
	END
	
	UPDATE [policy_list]
	SET [sms_val] =
	CASE
		WHEN LEN([sms_val]) >= 8 THEN [sms_val]
		WHEN LEN([sms_val]) < 8 THEN [sms_val] + '0'
		WHEN [sms_val] IS NULL THEN '00000000' ELSE [sms_val]
	END
	
	UPDATE [policy_list]
	SET [surveys_val] =
	CASE
		WHEN LEN([surveys_val]) >= 8 THEN [surveys_val]
		WHEN LEN([surveys_val]) < 8 THEN [surveys_val] + '0'
		WHEN [surveys_val] IS NULL THEN '00000000' ELSE [surveys_val]
	END
	
	UPDATE [policy_list]
	SET [users_val] =
	CASE
		WHEN LEN([users_val]) >= 8 THEN [users_val] 
		WHEN LEN([users_val]) < 8 THEN [users_val] + '0'
		WHEN [surveys_val] IS NULL THEN '00000000' ELSE [users_val]
	END
	
	UPDATE [policy_list]
	SET [groups_val] =
	CASE
		WHEN LEN([groups_val]) >= 8 THEN [groups_val]
		WHEN LEN([groups_val]) < 8 THEN [groups_val] + '0'
		WHEN [groups_val] IS NULL THEN '00000000' ELSE [groups_val]
	END
	
	UPDATE [policy_list]
	SET [templates_val] =
	CASE
		WHEN LEN([templates_val]) >= 8 THEN [templates_val]
		WHEN LEN([templates_val]) < 8 THEN [templates_val] + '0'
		WHEN [templates_val] IS NULL THEN '00000000' ELSE [templates_val]
	END
	
	UPDATE [policy_list]
	SET [text_templates_val] =
	CASE
		WHEN LEN([text_templates_val]) >= 8 THEN [text_templates_val]
		WHEN LEN([text_templates_val]) < 8 THEN [text_templates_val] + '0'
		WHEN [templates_val] IS NULL THEN '00000000' ELSE [templates_val]
	END
	
	UPDATE [policy_list]
	SET [statistics_val] =
	CASE
		WHEN LEN([statistics_val]) >= 8 THEN [statistics_val]
		WHEN LEN([statistics_val]) < 8 THEN [statistics_val] + '0'
		WHEN [statistics_val] IS NULL THEN '00000000' ELSE [statistics_val]
	END
	
	UPDATE [policy_list]
	SET [ip_groups_val] =
	CASE
		WHEN LEN([ip_groups_val]) >= 8 THEN [ip_groups_val]
		WHEN LEN([ip_groups_val]) < 8 THEN [ip_groups_val] + '0'
		WHEN [ip_groups_val] IS NULL THEN '00000000' ELSE [ip_groups_val]
	END
	
	UPDATE [policy_list]
	SET [rss_val] =
	CASE
		WHEN LEN([rss_val]) >= 8 THEN [rss_val]
		WHEN LEN([rss_val]) < 8 THEN [rss_val] + '0'
		WHEN [rss_val] IS NULL THEN '00000000' ELSE [rss_val]
	END
	
	UPDATE [policy_list]
	SET [screensavers_val] =
	CASE
		WHEN LEN([screensavers_val]) >= 8 THEN [screensavers_val]
		WHEN LEN([screensavers_val]) < 8 THEN [screensavers_val] + '0'
		WHEN [screensavers_val] IS NULL THEN '00000000' ELSE [screensavers_val]
	END
	
	UPDATE [policy_list]
	SET [wallpapers_val] =
	CASE
		WHEN LEN([wallpapers_val]) >= 8 THEN [wallpapers_val]
		WHEN LEN([wallpapers_val]) < 8 THEN [wallpapers_val] + '0'
		WHEN [wallpapers_val] IS NULL THEN '00000000' ELSE [wallpapers_val]
	END
	
	UPDATE [policy_list]
	SET [lockscreens_val] =
	CASE
		WHEN LEN([lockscreens_val]) >= 8 THEN [lockscreens_val]
		WHEN LEN([lockscreens_val]) < 8 THEN [lockscreens_val] + '0'
		WHEN [lockscreens_val] IS NULL THEN '00000000' ELSE [lockscreens_val]
	END

	UPDATE [policy_list]
	SET [im_val] =
	CASE
		WHEN LEN([im_val]) >= 8 THEN [im_val]
		WHEN LEN([im_val]) < 8 THEN [im_val] + '0'
		WHEN [im_val] IS NULL THEN '00000000' ELSE [im_val]
	END
	
	UPDATE [policy_list]
	SET [text_to_call_val] =
	CASE
		WHEN LEN([text_to_call_val]) >= 8 THEN [text_to_call_val]
		WHEN LEN([text_to_call_val]) < 8 THEN [text_to_call_val] + '0'
		WHEN [text_to_call_val] IS NULL THEN '00000000' ELSE [text_to_call_val]
	END
	
	UPDATE [policy_list]
	SET [feedback_val] =
	CASE
		WHEN LEN([feedback_val]) >= 8 THEN [feedback_val]
		WHEN LEN([feedback_val]) < 8 THEN [feedback_val] + '0'
		WHEN [feedback_val] IS NULL THEN '00000000' ELSE [feedback_val]
	END
	
	UPDATE [policy_list]
	SET [cc_val] =
	CASE
		WHEN LEN([cc_val]) >= 8 THEN [cc_val]
		WHEN LEN([cc_val]) < 8 THEN [cc_val] + '0'
		WHEN [cc_val] IS NULL THEN '00000000' ELSE [cc_val]
	END
	
	UPDATE [policy_list]
	SET [webplugin_val] =
	CASE
		WHEN LEN([webplugin_val]) >= 8 THEN [webplugin_val]
		WHEN LEN([webplugin_val]) < 8 THEN [webplugin_val] + '0'
		WHEN [webplugin_val] IS NULL THEN '00000000' ELSE [webplugin_val]
	END
	GO
	
	UPDATE [policy_list]
	SET [video_val] =
	CASE
		WHEN LEN([video_val]) >= 8 THEN [video_val]
		WHEN LEN([video_val]) < 8 THEN [video_val] + '0'
		WHEN [video_val] IS NULL THEN '00000000' ELSE [video_val]
	END
	
	UPDATE [policy_list]
	SET [campaign_val] =
	CASE
		WHEN LEN([campaign_val]) >= 8 THEN [campaign_val]
		WHEN LEN([campaign_val]) < 8 THEN [campaign_val] + '0'
		WHEN [campaign_val] IS NULL THEN '00000000' ELSE [campaign_val]
	END
GO
