﻿USE [%dbname_hook%];

IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE table_name = 'Policy_ContentSettings'
)
BEGIN
    CREATE TABLE [dbo].[Policy_ContentSettings]
    (
        [Id]		        [BIGINT] IDENTITY NOT NULL, 
        [Policy_Id]	        [BIGINT] NOT NULL, 
        [ContentSetting_Id] [BIGINT] NOT NULL, 
        CONSTRAINT	        [PK_Policy_ContentSettings] PRIMARY KEY CLUSTERED([Id] ASC), 
        CONSTRAINT	        [FK_Policy_ContentSettings_to_Policy] FOREIGN KEY(Policy_Id) REFERENCES [Policy](Id) ON DELETE CASCADE, 
        CONSTRAINT	        [FK_Policy_ContentSettings_to_ContentSettings] FOREIGN KEY(ContentSetting_Id) REFERENCES [ContentSettings](Id) ON DELETE CASCADE
    )
    ON [PRIMARY];
END;

IF
(
    SELECT COUNT(*)
    FROM [Policy_ContentSettings]
) <= 0 
BEGIN
    INSERT INTO [Policy_ContentSettings] ([Policy_Id], [ContentSetting_Id])
        (
            SELECT [Policy].[Id] AS [Policy_Id],
                   [ContentSettings].[Id] AS [ContentSetting_Id]
            FROM [ContentSettings]
            CROSS JOIN [Policy]
        );
END;