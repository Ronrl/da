USE [%dbname_hook%];

--indexes
GO
--widget tables
IF EXISTS(SELECT 1 from INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'widgets' AND column_name = 'name')
BEGIN
DROP TABLE widgets
END

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'widgets')
BEGIN
CREATE TABLE [dbo].[widgets](
	[id] [uniqueidentifier] NOT NULL,
	[title] [varchar](50) NULL,
	[href] [varchar](50) NULL,
 CONSTRAINT [PK_widgets] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)
) ON [PRIMARY];

END

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_widgets_id]') AND type = 'D')
	ALTER TABLE [dbo].[widgets] ADD  CONSTRAINT [DF_widgets_id]  DEFAULT (newid()) FOR [id]
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'widgets_editors')
BEGIN
CREATE TABLE [dbo].[widgets_editors](
	[id] [uniqueidentifier] NOT NULL,
	[editor_id] [bigint] NOT NULL,
	[widget_id] [uniqueidentifier] NOT NULL,
	[parameters] [varchar](500) NULL,
	[page] [varchar](1) NULL,
 CONSTRAINT [PK_widgets_editors] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)
) ON [PRIMARY]
END

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_widgets_editors_id]') AND type = 'D')
	ALTER TABLE [dbo].[widgets_editors] ADD  CONSTRAINT [DF_widgets_editors_id]  DEFAULT (newid()) FOR [id]
GO

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'unobtrusives' )
CREATE TABLE [dbo].[unobtrusives](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[from] [nvarchar](50) NOT NULL,
	[to] [nvarchar](50) NOT NULL,
	[days] [nvarchar](50) NULL,
	[enabled] [int] NOT NULL,
	[user_id] [int] NULL
) ON [PRIMARY]
GO
--banners here
IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'banners' )
CREATE TABLE  [dbo].[banners](
	[id] [int] IDENTITY NOT NULL,
	[name] [varchar](50) NULL,
	[date] [datetime] NULL,
	[width] [int] NULL,
	[width_unit] [varchar](3) NULL,
	[max_width] [int] NULL,
	[max_width_unit] [varchar](3) NULL,
	[min_width] [int] NULL,
	[min_width_unit] [varchar](3) NULL,
	[height] [int] NULL,
	[height_unit] [varchar](3) NULL,
	[max_height] [int] NULL,
	[max_height_unit] [varchar](3) NULL,
	[min_height] [int] NULL,
	[min_height_unit] [varchar](3) NULL,
	[background_color] [varchar](7) NULL,
	[transparent] [bit] NULL,
	[font_color] [varchar](7) NULL,
	[font_size] [int] NULL,
	[font_size_unit] [varchar](3) NULL,
	[font_weight] [int] NULL,
	[font_weight_unit] [varchar](3) NULL,
	[text_align] [varchar](10) NULL,
	[direction] [varchar](20) NULL,
	[unicode_bidi] [varchar](20) NULL,
	[border_weight] [int] NULL,
	[border_weight_unit] [varchar](3) NULL,
	[border_style] [varchar](30) NULL,
	[border_color] [varchar](7) NULL,
	[padding_top] [int] NULL,
	[padding_top_unit] [varchar](30) NULL,
	[padding_right] [int] NULL,
	[padding_right_unit] [varchar](30) NULL,
	[padding_bottom] [int] NULL,
	[padding_bottom_unit] [varchar](30) NULL,
	[padding_left] [int] NULL,
	[padding_left_unit] [varchar](30) NULL,
	[margin_top] [int] NULL,
	[margin_top_unit] [varchar](30) NULL,
	[margin_right] [int] NULL,
	[margin_right_unit] [varchar](30) NULL,
	[margin_bottom] [int] NULL,
	[margin_bottom_unit] [varchar](30) NULL,
	[margin_left] [int] NULL,
	[margin_left_unit] [varchar](30) NULL,
	[display] [varchar](30) NULL,
	[position] [varchar](30) NULL,
	[top] [int] NULL,
	[top_unit] [varchar](3) NULL,
	[right] [int] NULL,
	[right_unit] [varchar](3) NULL,
	[bottom] [int] NULL,
	[bottom_unit] [varchar](3) NULL,
	[left] [int] NULL,
	[left_unit] [varchar](3) NULL,
	CONSTRAINT [PK_banner] PRIMARY KEY CLUSTERED ([id] ASC)
) ON [PRIMARY]


GO

--full tables check for empty database
IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'alerts' )
	CREATE TABLE [dbo].[alerts](
		[id] [bigint] IDENTITY(1,1) NOT NULL,
		[alert_text] [nvarchar](max) NULL,
		[title] [nvarchar](max) NULL,
		[type] [varchar](1) NULL,
		[group_type] [varchar](1) NULL,
		[create_date] [datetime] NULL,
		[sent_date] [datetime] NULL,
		[type2] [varchar](1) NULL,
		[from_date] [datetime] NULL,
		[to_date] [datetime] NULL,
		[schedule] [varchar](1) NULL,
		[schedule_type] [varchar](1) NULL,
		[recurrence] [varchar](1) NULL,
		[urgent] [varchar](1) NULL,
		[toolbarmode] [varchar](1) NULL,
		[deskalertsmode] [varchar](1) NULL,
		[sender_id] [int] NULL,
		[template_id] [bigint] NULL,
		[group_id] [bigint] NULL,
		[aknown] [varchar](1) NULL,
		[ticker] [varchar](1) NULL,
		[fullscreen] [varchar](1) NULL,
		[ticker_speed] [varchar](1) NULL,
		[anonymous_survey] [varchar](1) NULL,
		[alert_width] [int] NULL,
		[alert_height] [int] NULL,
		[email_sender] [nvarchar](255) NULL,
		[email] [varchar](1) NULL,
		[sms] [varchar](1) NULL,
		[desktop] [varchar](1) NULL,
		[escalate] [varchar](1) NULL,
		[escalate_hours] [int] NULL,
		[escalate_count] [int] NULL,
		[escalate_step] [int] NULL,
		[escalate_hours_initial] [int] NULL,
		[autoclose] [bigint] NULL,
		[param_temp_id] [uniqueidentifier] NULL,
		[caption_id] [uniqueidentifier] NULL,
		[class] [bigint] NOT NULL DEFAULT 1,
		[parent_id] [bigint] NULL,
		[resizable] [bit] NULL,
		[post_to_blog] [bit] NULL,
		[social_media] [bit] NULL,
		[self_deletable] [bit] NULL,
		[text_to_call] [varchar] (1),
		[campaign_id] [bigint] NOT NULL DEFAULT -1
		CONSTRAINT [PK_alerts] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY]
ELSE
BEGIN
	IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_alerts]') AND type = 'K')
		ALTER TABLE [dbo].[alerts] ADD CONSTRAINT [PK_alerts] PRIMARY KEY CLUSTERED ([id] ASC)    
END

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'multiple_alerts' )
BEGIN
CREATE TABLE [dbo].[multiple_alerts](
    [id] [int] IDENTITY NOT NULL,
    [alert_id] [int] NOT NULL,
    [part_id] [int] NOT NULL,
    [part_content] [nvarchar](max) NOT NULL,
    [confirmed] [int] NOT NULL,
    [stat_alert_id] [nvarchar](200) NOT NULL,
    CONSTRAINT [PK_multiple_alerts] PRIMARY KEY CLUSTERED 
    (
        [id] ASC
    )
) ON [PRIMARY]
END

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE column_name = 'campaign_id' AND table_name = 'alerts')
	ALTER TABLE [dbo].[alerts] ADD [campaign_id] [bigint] NOT NULL DEFAULT -1	
	
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE column_name = 'video' AND table_name = 'alerts')
	ALTER TABLE [dbo].[alerts] ADD [video] [bit] NOT NULL DEFAULT 0
	
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE column_name = 'approve_status' AND table_name = 'alerts')
	ALTER TABLE [dbo].[alerts] ADD [approve_status] int NULL
	
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE column_name = 'reject_reason' AND table_name = 'alerts')
	ALTER TABLE [dbo].[alerts] ADD [reject_reason] nvarchar(1000) NULL
	
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE column_name = 'lifetime' AND table_name = 'alerts')
	ALTER TABLE [dbo].[alerts] ADD [lifetime] int NULL
	
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE column_name = 'terminated' AND table_name = 'alerts')
	ALTER TABLE [dbo].[alerts] ADD [terminated] int NULL

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE column_name = 'ticker_speed' AND table_name = 'alerts')
	ALTER TABLE [dbo].[alerts] ADD [ticker_speed] varchar(1) NULL	
	
IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='color_code'
		AND table_name = 'alerts' )
BEGIN
	ALTER TABLE [alerts] ADD [color_code] [uniqueidentifier]
END

GO	
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE column_name = 'rss_allow_content' AND table_name = 'alerts')
	ALTER TABLE [dbo].[alerts] ADD [rss_allow_content] int NULL
	
IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'alerts_escalate' )
	CREATE TABLE [alerts_escalate](
		[id] [bigint] IDENTITY (1,1) NOT NULL,
		[id_initial] [bigint] NOT NULL,
		[id_escalate] [bigint] NOT NULL,
		CONSTRAINT [PK_alerts_escalate] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY];
ELSE IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE column_name ='id' AND table_name = 'alerts_escalate' )
	ALTER TABLE [dbo].[alerts_escalate] ADD [id] [bigint] IDENTITY (1,1) NOT NULL
GO
IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_alerts_escalate]') AND type = 'K')
	ALTER TABLE [dbo].[alerts_escalate] ADD CONSTRAINT [PK_alerts_escalate] PRIMARY KEY CLUSTERED ([id] ASC)

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'recurrence' )
	CREATE TABLE [recurrence] (
		[id] [bigint] IDENTITY (1,1) NOT NULL, 
		[alert_id] [bigint], 
		[pattern] [varchar] (1), 
		[number_days] [int], 
		[number_week] [int], 
		[week_days] [int], 
		[month_day] [int], 
		[number_month] [int], 
		[weekday_place] [int], 
		[weekday_day] [int], 
		[month_val] [int], 
		[dayly_selector] [varchar] (50), 
		[monthly_selector] [varchar] (50), 
		[yearly_selector] [varchar] (50), 
		[end_type] [varchar] (50), 
		[occurences] [int],
		CONSTRAINT [PK_recurrence] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY]
ELSE IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_recurrence]') AND type = 'K')
	ALTER TABLE [dbo].[recurrence] ADD CONSTRAINT [PK_recurrence] PRIMARY KEY CLUSTERED ([id] ASC)
	
IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'feedbacks' )
	CREATE TABLE [feedbacks](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[client] [nvarchar](255) NOT NULL,
	[text] [nvarchar](max) NULL DEFAULT NULL,
	[date] [datetime] NULL DEFAULT NULL,
	[readed] [bit] NOT NULL DEFAULT 0,
    PRIMARY KEY CLUSTERED 
    (
	    [id] ASC
    )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
    ) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'alerts_sent' )
	CREATE TABLE [alerts_sent] (
		[id] [bigint] IDENTITY (1,1) NOT NULL, 
		[alert_id] [bigint], 
		[user_id] [bigint],
		CONSTRAINT [PK_alerts_sent] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY]
ELSE IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_alerts_sent]') AND type = 'K')
	ALTER TABLE [dbo].[alerts_sent] ADD CONSTRAINT [PK_alerts_sent] PRIMARY KEY CLUSTERED ([id] ASC)

--- BEGIN Alerts_received
IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'alerts_received' )
	CREATE TABLE [alerts_received] (
		[id] [bigint] IDENTITY (1,1) NOT NULL, 
		[alert_id] [bigint], 
		[user_id] [bigint], 
		[clsid] [varchar] (255),
		[date] [datetime],
		[computer_id] [bigint],
		CONSTRAINT [PK_alerts_received] PRIMARY KEY CLUSTERED ([id] ASC),
		CONSTRAINT [KEY_alerts_received] UNIQUE (clsid, alert_id, user_id)
	) ON [PRIMARY]
ELSE IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_alerts_received]') AND type = 'K')
	ALTER TABLE [dbo].[alerts_received] ADD CONSTRAINT [PK_alerts_received] PRIMARY KEY CLUSTERED ([id] ASC)
GO

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='date'
		AND table_name = 'alerts_received' )
BEGIN
	ALTER TABLE [alerts_received] ADD [date] [datetime]
END

GO
--------- END alerts_received
IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'groups' )
	CREATE TABLE [groups] (
		[id] [bigint] IDENTITY (1,1) NOT NULL, 
		[name] [varchar] (255),
		[display_name] [varchar] (255),
		[alert_id] [int], 
		[type] [varchar] (1), 
		[special_id] [varchar] (50), 
		[domain_id] [int], 
		[context_id] [int], 
		[rss_url] [varchar] (255), 
		[was_checked] [int], 
		[root_group] [int], 
		[custom_group] [int],
		[policy_id] [bigint] NULL,
		[dn] [varchar] (255),
		CONSTRAINT [PK_groups] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY]
ELSE IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_groups]') AND type = 'K')
	ALTER TABLE [dbo].[groups] ADD CONSTRAINT [PK_groups] PRIMARY KEY CLUSTERED ([id] ASC)

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'groups_policy' )
	CREATE TABLE [groups_policy](
		[id] [bigint] IDENTITY(1,1) NOT NULL,
		[policy_id] [bigint] NULL,
		[group_id] [bigint] NULL,
		[was_checked] [int] NULL,
		CONSTRAINT [PK_groups_policy]  PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY]
ELSE IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_groups_policy]') AND type = 'K')
	ALTER TABLE [dbo].[groups_policy] ADD CONSTRAINT [PK_groups_policy] PRIMARY KEY CLUSTERED ([id] ASC)

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'rss' )
BEGIN
	CREATE TABLE [rss] (
		[id] [bigint] IDENTITY (1,1) NOT NULL, 
		[broad_url] [varchar] (255),
		CONSTRAINT [PK_rss] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY];
	INSERT INTO rss (broad_url) VALUES ('');
END
ELSE IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_rss]') AND type = 'K')
	ALTER TABLE [dbo].[rss] ADD CONSTRAINT [PK_rss] PRIMARY KEY CLUSTERED ([id] ASC)

if EXISTS(SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[tri_user_delete]') AND type = 'TR')
drop trigger  dbo.tri_user_delete
go

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'users' )
BEGIN
	CREATE TABLE [users] (
		[id] [bigint] IDENTITY (1,1) NOT NULL, 
		[name] [varchar] (255), 
		[display_name] [varchar] (255), 
		[pass] [varchar] (255), 
		[deskbar_id] [varchar] (255), 
		[alert_id] [text], 
		[last_date] [datetime], 
		[role] [varchar] (1), 
		[group_id] [int], 
		[last_rss] [text], 
		[rss_url] [varchar] (255), 
		[last_broad] [text], 
		[reg_date] [datetime] DEFAULT getdate() NULL,
		[last_request] [datetime] DEFAULT '1970-01-01 00:00:00.000' NULL,
		[next_request] [varchar] (255), 
		[last_standby_request] [datetime], 
		[next_standby_request] [varchar] (255), 
		[mobile_phone] [varchar] (50), 
		[email] [varchar] (255), 
		[domain_id] [int], 
		[context_id] [int], 
		[standby] [int], 
		[disabled] [int], 
		[was_checked] [int], 
		[rss_modify] [varchar] (255), 
		[rss_broad_modify] [varchar] (255), 
		[uhash] [varchar] (255), 
		[dis] [int], 
		[update_date] [datetime], 
		[expire_date] [datetime], 
		[fname] [varchar] (255), 
		[sname] [varchar] (255), 
		[only_personal] [int], 
		[only_templates] [int],
		[only_alerts] [int],
		[client_version] [varchar] (255),
		[start_page] [varchar] (255),
		[guide] [varchar] (5),
		[dis_guide] [varchar] (100),
		[send_logs] [int],
		[publisher_status] [varchar] (255),
		[publisher_last_online_datetime] [datetime],
		CONSTRAINT [PK_users] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY];
	INSERT INTO users (name, pass, role, reg_date) VALUES ('admin', '21232f297a57a5a743894a0e4a801fc3', 'A', GETDATE());
	INSERT INTO users (name, pass, role, reg_date) VALUES ('da_admin', '21232f297a57a5a743894a0e4a801fc3', 'A', GETDATE());
	INSERT INTO widgets_editors (editor_id, widget_id, parameters, page) VALUES(1, '071954af-ad48-4630-bd1e-8193691fc9a1', '', 'D');
	INSERT INTO widgets_editors (editor_id, widget_id, parameters, page) VALUES(1, 'e745170b-4243-4bec-a8b4-56def37dd7ae', '', 'D');
	--INSERT INTO widgets_editors (editor_id, widget_id, parameters, page) VALUES(1, '7d513b11-bbe8-45ee-937c-922adcfb107b', '', 'D');
END
ELSE IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_users]') AND type = 'K')
	ALTER TABLE [dbo].[users] ADD CONSTRAINT [PK_users] PRIMARY KEY CLUSTERED ([id] ASC)
ELSE	
BEGIN

IF EXISTS (SELECT * FROM users)
BEGIN
    --Protection from multiple admin's account
    delete 
    FROM users
    WHERE name = 'admin' and
         users.id <> (select min(id) 
                        from users
                       WHERE name = 'admin')


		--
		UPDATE users SET last_request = '1970-01-01 00:00:00.000' WHERE last_request IS NULL
		UPDATE users SET reg_date = getdate() where reg_date IS NULL
	END

	IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_users]') AND type = 'K')
	BEGIN
		ALTER TABLE [dbo].[users] ADD CONSTRAINT [PK_users] PRIMARY KEY CLUSTERED ([id] ASC)
	END

	IF NOT EXISTS ( SELECT default_constraints.name
	FROM sys.all_columns 
		INNER JOIN sys.tables ON all_columns.object_id = tables.object_id
		INNER JOIN sys.schemas ON tables.schema_id = schemas.schema_id
		INNER JOIN sys.default_constraints ON all_columns.default_object_id = default_constraints.object_id
	WHERE schemas.name = 'dbo'
		AND tables.name = 'users'
		AND all_columns.name = 'last_request')
	BEGIN
		ALTER TABLE [dbo].[users] ADD DEFAULT '1970-01-01 00:00:00.000' FOR last_request
	END

	
	IF NOT EXISTS ( SELECT default_constraints.name
	FROM sys.all_columns 
		INNER JOIN sys.tables ON all_columns.object_id = tables.object_id
		INNER JOIN sys.schemas ON tables.schema_id = schemas.schema_id
		INNER JOIN sys.default_constraints ON all_columns.default_object_id = default_constraints.object_id
	WHERE schemas.name = 'dbo'
		AND tables.name = 'users'
		AND all_columns.name = 'reg_date')
	BEGIN
		ALTER TABLE [dbo].[users] ADD DEFAULT getdate() FOR reg_date
	END
END

go

CREATE TRIGGER [dbo].[tri_user_delete]    ON  [dbo].[users] 
   AFTER DELETE
AS 
BEGIN
	SET NOCOUNT ON;
	--Delete stat for deleted users
	DELETE FROM surveys_answers_stat 
	WHERE EXISTS (SELECT sas.*	
				FROM surveys_answers_stat AS sas
					LEFT JOIN users AS u ON u.id = sas.user_id
				WHERE u.id IS NULL)

	DELETE FROM surveys_custom_answers_stat 
	WHERE EXISTS (SELECT scass.*
				FROM surveys_custom_answers_stat AS scass 
					LEFT JOIN users AS u ON scass.user_id = u.id
				WHERE u.id IS NULL)
	--Prevent admin deletion
	declare @userName nvarchar(255)
	if exists( select * from deleted where name in ('admin','da_admin') and role = 'A')
	begin
		ROLLBACK TRAN
		RAISERROR ('Attemp to delete admin accounts.', 16, 10) 
	end
END
go
ALTER TABLE [dbo].[users] ENABLE TRIGGER [tri_user_delete]

GO

if EXISTS(SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[tri_user_update]') AND type = 'TR')
drop trigger  dbo.tri_user_update
go

CREATE TRIGGER [dbo].[tri_user_update]
   ON  [dbo].[users]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;
	IF UPDATE(id) OR UPDATE(name)
	BEGIN
		if exists( select * from deleted where name in ('admin','da_admin') and role = 'A')
		begin
			ROLLBACK TRAN
			RAISERROR ('Attemp to modify <id> or <name> field for admin accounts.', 16, 10) 
		end
	END
END
GO
ALTER TABLE [dbo].[users] ENABLE TRIGGER [tri_user_update]
GO


IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'templates' )
BEGIN
	CREATE TABLE [templates] (
		[id] [bigint] IDENTITY (1,1) NOT NULL, 
		[top] [varchar] (max), 
		[bottom] [varchar] (max), 
		[name] [varchar] (255), 
		[sender_id] [bigint],
		CONSTRAINT [PK_templates] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY];
	INSERT INTO templates (name, [top], bottom) VALUES ('default','', '' );
	INSERT INTO templates (name, [top], bottom) VALUES ('sample','<body style="margin: 0px, 0px, 0px, 0px;font-family: arial, helvetica, sans-serif;font-size:12px;"><table cellpadding=0 cellspacing=0 border=0 width=100% ><tr><td background="admin/images/upload/back.gif" align="left"><div style="padding-top: 5px; padding-bottom: 5px;padding-left: 5px;"><img src="admin/images/upload/logo.gif" alt="Your Logo" width="67" height="16" border="0"></div></td></tr><tr><td><div style="padding: 5px, 0px, 10px, 10px;font-size:14px;">', '</div></td></tr></table></body>' );
END
ELSE IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_templates]') AND type = 'K')
	ALTER TABLE [dbo].[templates] ADD CONSTRAINT [PK_templates] PRIMARY KEY CLUSTERED ([id] ASC)


IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'statistics' )
	CREATE TABLE [statistics] (
		[id] [bigint] IDENTITY (1,1) NOT NULL, 
		[send] [int], 
		[view] [int], 
		[clicks] [int], 
		[date] [datetime], 
		[alert_id] [int], 
		[user_id] [text],
		CONSTRAINT [PK_statistics] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY]
ELSE IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_statistics]') AND type = 'K')
	ALTER TABLE [dbo].[statistics] ADD CONSTRAINT [PK_statistics] PRIMARY KEY CLUSTERED ([id] ASC)

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'version' )
BEGIN
	CREATE TABLE [version] (
		[id] [bigint] IDENTITY (1,1) NOT NULL, 
		[version] [varchar] (50), 
		[ul] [varchar] (max), 
		[ul_date] [datetime], 
		[ul_logins] [int],
		[contract_date] [datetime],
		[license_expire_date] [datetime],
		[used_prolong_keys] [varchar] (max),
		CONSTRAINT [PK_version] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY];
	INSERT INTO [version] ([version]) VALUES ('%deskalerts_version%');
END
ELSE IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_version]') AND type = 'K')
	ALTER TABLE [dbo].[version] ADD CONSTRAINT [PK_version] PRIMARY KEY CLUSTERED ([id] ASC)

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'users_groups' )
	CREATE TABLE [users_groups] (
		[id] [bigint] IDENTITY (1,1) NOT NULL, 
		[group_id] [bigint], 
		[user_id] [bigint], 
		[last_rss] [text], 
		[was_checked] [int], 
		[rss_modify] [varchar] (255),
		CONSTRAINT [PK_users_groups] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY]
ELSE IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_users_groups]') AND type = 'K')
	ALTER TABLE [dbo].[users_groups] ADD CONSTRAINT [PK_users_groups] PRIMARY KEY CLUSTERED ([id] ASC)

GO

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'surveys_main' )
	CREATE TABLE [surveys_main] (
		[id] [bigint] IDENTITY (1,1) NOT NULL, 
		[name] [varchar] (255), 
		[question] [varchar] (max), 
		[type] [varchar] (1), 
		[group_type] [varchar] (1), 
		[create_date] [datetime], 
		[closed] [varchar] (1), 
		[sender_id] [int], 
		[template_id] [bigint], 
		[from_date] [datetime], 
		[to_date] [datetime], 
		[schedule] [varchar] (1),
		[anonymous_survey] [varchar](1) NULL,
		[stype] [smallint] NOT NULL DEFAULT ((1)),
		CONSTRAINT [PK_surveys_main] PRIMARY KEY CLUSTERED ([id] ASC)
) ON [PRIMARY]
ELSE IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_surveys_main]') AND type = 'K')
	ALTER TABLE [dbo].[surveys_main] ADD CONSTRAINT [PK_surveys_main] PRIMARY KEY CLUSTERED ([id] ASC)

GO

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'surveys_answers' )
	CREATE TABLE [surveys_answers] (
		[id] [bigint] IDENTITY (1,1) NOT NULL, 
		[answer] [varchar] (max), 
		[survey_id] [bigint], 
		[votes] [bigint], 
		[question_id] [bigint],
		[correct] [bit] NULL,
		CONSTRAINT [PK_surveys_answers] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY]
ELSE IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_surveys_answers]') AND type = 'K')
	ALTER TABLE [dbo].[surveys_answers] ADD CONSTRAINT [PK_surveys_answers] PRIMARY KEY CLUSTERED ([id] ASC)	

	
IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'domains' )
BEGIN
	CREATE TABLE [domains] (
		[id] [bigint] IDENTITY (1,1) NOT NULL, 
		[name] [varchar] (255), 
		[all_groups] [int], 
		[all_ous] [int], 
		[was_checked] [int],
		CONSTRAINT [PK_domains] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY];
	INSERT INTO domains (name) VALUES ('');
END
ELSE IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_domains]') AND type = 'K')
	ALTER TABLE [dbo].[domains] ADD CONSTRAINT [PK_domains] PRIMARY KEY CLUSTERED ([id] ASC)	

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'edir_context' )
	CREATE TABLE [edir_context] (
		[id] [bigint] IDENTITY (1,1) NOT NULL, 
		[name] [varchar] (max), 
		[domain_id] [int], 
		[was_checked] [int],
		CONSTRAINT [PK_edir_context] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY]
ELSE IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_edir_context]') AND type = 'K')
	ALTER TABLE [dbo].[edir_context] ADD CONSTRAINT [PK_edir_context] PRIMARY KEY CLUSTERED ([id] ASC)	

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'alerts_read' )
	CREATE TABLE [alerts_read] (
		[id] [bigint] IDENTITY (1,1) NOT NULL, 
		[alert_id] [bigint], 
		[user_id] [bigint], 
		[closetime] [varchar] (55), 
		[read] [char] (10), 
		[date] [datetime],
		CONSTRAINT [PK_alerts_read] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY]
ELSE IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_alerts_read]') AND type = 'K')
	ALTER TABLE [dbo].[alerts_read] ADD CONSTRAINT [PK_alerts_read] PRIMARY KEY CLUSTERED ([id] ASC)	

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'alerts_sent_stat' )
	CREATE TABLE [alerts_sent_stat] (
		[id] [bigint] IDENTITY (1,1) NOT NULL, 
		[alert_id] [bigint],
		[user_id] [bigint], 
		[date] [datetime],
		CONSTRAINT [PK_alerts_sent_stat] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY]
ELSE IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_alerts_sent_stat]') AND type = 'K')
	ALTER TABLE [dbo].[alerts_sent_stat] ADD CONSTRAINT [PK_alerts_sent_stat] PRIMARY KEY CLUSTERED ([id] ASC)	

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'alerts_group_stat' )
	CREATE TABLE [alerts_group_stat] (
		[id] [bigint] IDENTITY (1,1) NOT NULL,
		[alert_id] [bigint], 
		[group_id] [bigint], 
		[date] [datetime],
		CONSTRAINT [PK_alerts_group_stat] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY]
ELSE IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_alerts_group_stat]') AND type = 'K')
	ALTER TABLE [dbo].[alerts_group_stat] ADD CONSTRAINT [PK_alerts_group_stat] PRIMARY KEY CLUSTERED ([id] ASC)	

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'alerts_ou_stat' )
	CREATE TABLE [alerts_ou_stat] (
		[id] [bigint] IDENTITY (1,1) NOT NULL, 
		[alert_id] [bigint], 
		[ou_id] [bigint], 
		[date] [datetime],
		CONSTRAINT [PK_alerts_ou_stat] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY]
ELSE IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_alerts_ou_stat]') AND type = 'K')
	ALTER TABLE [dbo].[alerts_ou_stat] ADD CONSTRAINT [PK_alerts_ou_stat] PRIMARY KEY CLUSTERED ([id] ASC)	

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'client_versions' )
	CREATE TABLE [client_versions] (
		[id] [bigint] IDENTITY (1,1) NOT NULL, 
		[user_id] [bigint], 
		[winver] [varchar] (255), 
		[iever] [varchar] (255), 
		[spver] [varchar] (255), 
		[date] [datetime],
		CONSTRAINT [PK_client_versions] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY]
ELSE IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_client_versions]') AND type = 'K')
	ALTER TABLE [dbo].[client_versions] ADD CONSTRAINT [PK_client_versions] PRIMARY KEY CLUSTERED ([id] ASC)

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'text_templates' )
BEGIN
	CREATE TABLE [text_templates] (
		[id] [bigint] IDENTITY (1,1) NOT NULL, 
		[name] [varchar] (255), 
		[template_text] [varchar] (max),
		CONSTRAINT [PK_text_templates] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY]
	
	INSERT INTO [text_templates] ([name], [template_text]) VALUES (N'Event [pop-up only]', N'<p><span style="font-size: 18pt;">It Department is aware of the email outage. Some of our users have not been able to access their email since 10 am EST. The issues has been harder to fix than we originally expected.</span></p>
<p>&nbsp;</p>
<p><span style="font-size: 18pt;">We have dozens of people working around the clock to bring it to a resolution. We believe our current efforts will restore our users'' access to their inboxes by 3 pm today. We will keep you posted.</span></p>
<p>&nbsp;</p>')

INSERT [text_templates] ([name], [template_text]) VALUES ( N'Arrival Announcement [pop-up & ticker]', N'<p align="center"><span style="font-size: 10pt;"><span style="font-size: 10pt;"><span style="font-size: 10pt;"><span style="font-size: 10pt;"><span style="font-size: 10pt;"><strong>Emplo</strong></span></span></span></span></span><span style="font-size: 10pt;"><span style="font-size: 10pt;"><span style="font-size: 10pt;"><span style="font-size: 10pt;"><span style="font-size: 10pt;"><strong>yee</strong></span></span></span></span></span><span style="font-size: 10pt;"><span style="font-size: 10pt;"><span style="font-size: 10pt;"><span style="font-size: 10pt;"><span style="font-size: 10pt;"><strong>, Arrival Announcement* </strong></span></span></span></span></span></p>
<p align="center"><span style="font-size: 10pt;"><span style="font-size: 10pt;"><span style="font-size: 10pt;"><span style="font-size: 10pt;"><span style="font-size: 10pt;"><strong>(To </strong></span></span></span></span></span><span style="font-size: 10pt;"><span style="font-size: 10pt;"><span style="font-size: 10pt;"><span style="font-size: 10pt;"><span style="font-size: 10pt;"><strong>co-workers from Supervisor)</strong></span></span></span></span></span></p>
<p align="left"><span style="font-size: 10pt;"><span style="font-size: 10pt;"><span style="font-size: 10pt;"><span style="font-size: 10pt;">Dear </span></span></span><span style="font-size: 10pt;"><span style="font-size: 10pt;"><span style="font-size: 10pt;">[</span></span></span><span style="font-size: 10pt;"><span style="font-size: 10pt;"><span style="font-size: 10pt;">Department Name] Team:</span></span></span></span></p>
<p align="left"><span style="font-size: 10pt;"><span style="font-size: 10pt;"><span style="font-size: 10pt;"><span style="font-size: 10pt;">I am delighted to announce that </span></span></span><span style="font-size: 10pt;"><span style="font-size: 10pt;"><span style="font-size: 10pt;">[</span></span></span><span style="font-size: 10pt;"><span style="font-size: 10pt;"><span style="font-size: 10pt;">employee''s name] is joining our department as [job title of employee] on [start date]. Please do everything you can to make </span></span></span><span style="font-size: 10pt;"><span style="font-size: 10pt;"><span style="font-size: 10pt;">[</span></span></span><span style="font-size: 10pt;"><span style="font-size: 10pt;"><span style="font-size: 10pt;">him/her] feel welcome and a part of our team.</span></span></span></span><span style="font-size: 10pt;"><span style="font-size: 10pt;"><span style="font-size: 10pt;">During the orientation process [name of "buddy&rdquo;</span></span></span><span style="font-size: 10pt;"><span style="font-size: 10pt;"><span style="font-size: 10pt;">]</span></span></span><span style="font-size: 10pt;"><span style="font-size: 10pt;"><span style="font-size: 10pt;"> will be assigned as a "buddy" to [employee''s name]. Many of you will be involved in assisting [name of "buddy&rdquo;l in the orientation and training our employee to ensure they become a productive member of our work team.</span></span></span><span style="font-size: 10pt;"><span style="font-size: 10pt;"><span style="font-size: 10pt;">Please make a point of welcoming [employee''s name] t</span></span></span><span style="font-size: 10pt;"><span style="font-size: 10pt;"><span style="font-size: 10pt;">o</span></span></span><span style="font-size: 10pt;"><span style="font-size: 10pt;"><span style="font-size: 10pt;">ou</span></span></span><span style="font-size: 10pt;"><span style="font-size: 10pt;"><span style="font-size: 10pt;">r department on [his/her] first day.</span></span></span></p>
<p align="left"><span style="font-size: 10pt;"><span style="font-size: 10pt;"><span style="font-size: 10pt;"><span style="font-size: 10pt;">Sincerely.</span></span></span></span></p>')



END
ELSE IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_text_templates]') AND type = 'K')
	ALTER TABLE [dbo].[text_templates] ADD CONSTRAINT [PK_text_templates] PRIMARY KEY CLUSTERED ([id] ASC)

IF NOT EXISTS (SELECT 1 FROM [text_templates] WHERE name = 'Corporate news [pop-up only]')
BEGIN
INSERT [text_templates] ([name], [template_text]) VALUES ( N'Corporate news [pop-up only]', N'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <title>DeskAlerts</title>
            <style type="text/css">
                #outlook a{
                padding:0;
                }
                body{
                width:100% !important;
                }
                .ReadMsgBody{
                width:100%;
                }
                .ExternalClass{
                width:100%;
                }
                body{
                -webkit-text-size-adjust:none;
                }
                body{
                margin:0;
                padding:0;
                }
                img{
                border:0;
                height:auto;
                line-height:100%;
                outline:none;
                text-decoration:none;
                }
                table td{
                border-collapse:collapse;
                }
                #bodyTable{
                height:100% !important;
                margin:0;
                padding:0;
                width:100% !important;
                }
                body,#bodyTable{
                background-color:#ABDEF3;
                }
                #templateContainer{
                border:0;
                }
                h1{
                color:#FFFFFF;
                display:block;
                font-family:Georgia;
                font-size:30px;
                font-style:normal;
                font-weight:Normal;
                line-height:100%;
                letter-spacing:normal;
                margin-top:0;
                margin-right:0;
                margin-bottom:10px;
                margin-left:0;
                text-align:left;
                }
                h2{
                color:#0E76BC;
                display:block;
                font-family:Georgia;
                font-size:26px;
                font-style:normal;
                font-weight:normal;
                line-height:100%;
                letter-spacing:normal;
                margin-top:0;
                margin-right:0;
                margin-bottom:10px;
                margin-left:0;
                text-align:left;
                }
                h3{
                color:#231F20;
                display:block;
                font-family:Georgia;
                font-size:18px;
                font-style:normal;
                font-weight:normal;
                line-height:100%;
                letter-spacing:normal;
                margin-top:0;
                margin-right:0;
                margin-bottom:10px;
                margin-left:0;
                text-align:left;
                }
                h4{
                color:#0E76BC;
                display:block;
                font-family:Georgia;
                font-size:13px;
                font-style:normal;
                font-weight:normal;
                line-height:100%;
                letter-spacing:normal;
                margin-top:0;
                margin-right:0;
                margin-bottom:10px;
                margin-left:0;
                text-align:left;
                }
                #templatePreheader{
                background-color:#0E76BC;
                border-bottom:1px solid #0D5B90;
                }
                .preheaderContent{
                color:#FFFFFF;
                font-family:Helvetica;
                font-size:10px;
                line-height:125%;
                text-align:left;
                }
                .preheaderContent a:link,.preheaderContent a:visited,.preheaderContent a .yshortcuts {
                color:#FFFFFF;
                font-weight:normal;
                text-decoration:underline;
                }
                #templateHeader{
                background-color:#F4F4F4;
                border-top:1px solid #FFFFFF;
                border-bottom:1px solid #CCCCCC;
                }
                .headerBannerContent,.headerBannerContent a:link,.headerBannerContent a:visited,.headerBannerContent a .yshortcuts {
                background-color:#0E76BC;
                color:#FFFFFF;
                font-family:Georgia;
                font-size:20px;
                line-height:150%;
                text-align:center;
                text-decoration:none;
                }
                .headerContent{
                color:#505050;
                font-family:Helvetica;
                font-size:20px;
                font-weight:bold;
                line-height:100%;
                padding-top:20px;
                padding-right:0;
                padding-bottom:0;
                padding-left:0;
                text-align:center;
                vertical-align:middle;
                }
                .headerContent a:link,.headerContent a:visited,.headerContent a .yshortcuts {
                color:#0E76BC;
                font-weight:normal;
                text-decoration:underline;
                }
                #headerImage{
                height:auto;
                max-width:600px !important;
                }
                #templateBody{
                background-color:#F4F4F4;
                border-top:1px solid #FFFFFF;
                border-bottom:1px solid #CCCCCC;
                }
                .bodyContent{
                color:#505050;
                font-family:Helvetica;
                font-size:15px;
                line-height:150%;
                text-align:left;
                }
                .bodyContent a:link,.bodyContent a:visited,.bodyContent a .yshortcuts {
                color:#0E76BC;
                font-weight:normal;
                text-decoration:underline;
                }
                .bodyContent img{
                display:inline;
                height:auto;
                max-width:325px !important;
                }
                #templateSidebar{
                background-color:#F4F4F4;
                border-left:1px solid #FFFFFF;
                }
                #mainBodyContent{
                border-right:1px solid #DDDDDD;
                }
                .sidebarBannerContent,.sidebarBannerContent a:link,.sidebarBannerContent a:visited,.sidebarBannerContent a .yshortcuts {
                background-color:#0E76BC;
                color:#FFFFFF;
                font-family:Georgia;
                font-size:13px;
                line-height:150%;
                text-align:center;
                text-decoration:none;
                }
                .sidebarContent{
                color:#707070;
                font-family:Helvetica;
                font-size:13px;
                line-height:150%;
                text-align:left;
                }
                .sidebarContent a:link,.sidebarContent a:visited,.sidebarContent a .yshortcuts {
                color:#0E76BC;
                font-weight:normal;
                text-decoration:underline;
                }
                .sidebarContent img{
                display:inline;
                height:auto;
                max-width:160px !important;
                }
                #templateFooter{
                background-color:#231F20;
                border-top:1px solid #FFFFFF;
                }
                .footerContent{
                color:#AAAAAA;
                font-family:Helvetica;
                font-size:10px;
                line-height:150%;
                text-align:center;
                }
                .footerContent a:link,.footerContent a:visited,.footerContent a .yshortcuts {
                color:#CCCCCC;
                font-weight:normal;
                text-decoration:underline;
                }
                .footerContent img{
                display:inline;
                }
                #monkeyRewards img{
                max-width:180px;
                }
            </style>
    </head>
    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0"
          style="-webkit-text-size-adjust: none;margin: 0;padding: 0;background-color: #ABDEF3;width: 100% !important;">
		<center>
            <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable"
                   style="margin: 0;padding: 0;background-color: #ABDEF3;height: 100% !important;width: 100% !important;">
                <tr>
                    <td align="center" valign="top"
                        style="padding-top: 20px;padding-bottom: 40px;border-collapse: collapse;">
                        <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateContainer"
                               style="border: 0;">
                            <tr>
                                <td align="center" valign="top" style="border-collapse: collapse;">
                                    <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateHeader"
                                           style="background-color: #F4F4F4;border-top: 1px solid #FFFFFF;border-bottom: 1px solid #CCCCCC;">
                                        <tr>
                                            <td align="center" valign="top" style="border-collapse: collapse;">
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td class="headerContent"
                                                            style="border-collapse: collapse;color: #505050;font-family: Helvetica;font-size: 20px;font-weight: bold;line-height: 100%;padding-top: 20px;padding-right: 0;padding-bottom: 0;padding-left: 0;text-align: center;vertical-align: middle;">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" valign="top"
                                                            style="padding: 20px;border-collapse: collapse;">
                                                            <table border="0" cellpadding="0" cellspacing="0"
                                                                   width="560" class="templateBanner">
                                                                <tr>
                                                                    <td colspan="3" valign="bottom"
                                                                        class="headerContent"
                                                                        style="padding-left: 20px;padding-right: 20px;border-collapse: collapse;color: #505050;font-family: Helvetica;font-size: 20px;font-weight: bold;line-height: 100%;padding-top: 20px;padding-bottom: 0;text-align: center;vertical-align: middle;">
                                                                       
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="10" width="10"
                                                                        style="border-collapse: collapse;">
                                                                        <img
                                                                            src="spacer.gif"
                                                                            width="10" height="10"
                                                                            style="display: block;margin: 0;padding: 0;height: 10px !important;border: 0;line-height: 100%;outline: none;text-decoration: none;">
                                                                    </td>
                                                                    <td align="center" rowspan="2" width="540"
                                                                        class="headerBannerContent"
                                                                        style="padding: 20px;border-collapse: collapse;background-color: #0E76BC;color: #FFFFFF;font-family: Georgia;font-size: 20px;line-height: 150%;text-align: center;text-decoration: none;">
                                                                        <h1
                                                                            style="color: #FFFFFF;display: block;font-family: Georgia;font-size: 30px;font-style: normal;font-weight: Normal;line-height: 100%;letter-spacing: normal;margin-top: 0;margin-right: 0;margin-bottom: 10px;margin-left: 0;text-align: left;">
                                                                            The Charming Times
                                                                        </h1>

                                                                        <h3
                                                                            style="color: #231F20;display: block;font-family: Georgia;font-size: 18px;font-style: normal;font-weight: normal;line-height: 100%;letter-spacing: normal;margin-top: 0;margin-right: 0;margin-bottom: 10px;margin-left: 0;text-align: left;">
                                                                            School News for April, 2012
                                                                        </h3>
                                                                    </td>
                                                                    <td height="10" width="10"
                                                                        style="border-collapse: collapse;">
                                                                        <img
                                                                            src="spacer.gif"
                                                                            width="10" height="10"
                                                                            style="display: block;margin: 0;padding: 0;height: 10px !important;border: 0;line-height: 100%;outline: none;text-decoration: none;">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="10"
                                                                        style="background-color: #505050;border-collapse: collapse;">
                                                                        <br>
                                                                    </td>
                                                                    <td width="10"
                                                                        style="background-color: #505050;border-collapse: collapse;">
                                                                        <br>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="3" style="border-collapse: collapse;">
                                                                        <table border="0" cellpadding="0"
                                                                               cellspacing="0" width="560">
                                                                            <tr>
                                                                                <td height="10" width="20"
                                                                                    style="background-color: #505050;border-collapse: collapse;">
                                                                                    <img
                                                                                        src="spacer.gif"
                                                                                        width="14" height="6"
                                                                                        style="display: block;margin: 0;padding: 0;height: 6px !important;border: 0;line-height: 100%;outline: none;text-decoration: none;">
                                                                                </td>
                                                                                <td height="10" width="500"
                                                                                    style="border-collapse: collapse;">
                                                                                    <img
                                                                                        src="spacer.gif"
                                                                                        width="132" height="6"
                                                                                        style="display: block;margin: 0;padding: 0;height: 6px !important;border: 0;line-height: 100%;outline: none;text-decoration: none;">
                                                                                </td>
                                                                                <td height="10" width="20"
                                                                                    style="background-color: #505050;border-collapse: collapse;">
                                                                                    <img
                                                                                        src="spacer.gif"
                                                                                        width="14" height="6"
                                                                                        style="display: block;margin: 0;padding: 0;height: 6px !important;border: 0;line-height: 100%;outline: none;text-decoration: none;">
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top" style="border-collapse: collapse;">
                                    <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateBody"
                                           style="background-color: #F4F4F4;border-top: 1px solid #FFFFFF;border-bottom: 1px solid #CCCCCC;">
                                        <tr>
                                            <td align="center" valign="top" style="border-collapse: collapse;">
                                                <table border="0" cellpadding="20" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td align="center" valign="top" id="mainBodyContent"
                                                            style="border-collapse: collapse;border-right: 1px solid #DDDDDD;">
                                                            <table border="0" cellpadding="0" cellspacing="0"
                                                                   width="100%">
                                                                <tr>
                                                                    <td valign="top" class="bodyContent"
                                                                        style="border-collapse: collapse;color: #505050;font-family: Helvetica;font-size: 15px;line-height: 150%;text-align: left;">
                                                                        <h2
                                                                            style="color: #0E76BC;display: block;font-family: Georgia;font-size: 26px;font-style: normal;font-weight: normal;line-height: 100%;letter-spacing: normal;margin-top: 0;margin-right: 0;margin-bottom: 10px;margin-left: 0;text-align: left;">
                                                                            Tutoring Program
                                                                        </h2>
                                                                        <strong>In an effort to raise Charming`s test scores</strong>
                                                                        , we`re happy to announce our inaugural tutoring program. We`re aware that students face challenges every day in the classroom, and it`s tough being a high schooler. These challenges are real, and can`t be overcome without action. If our students don`t succeed, then Charming doesn`t succeed either. It`s that simple.
                                                                        <br>
                                                                            <br>
                                                                                It is with this in mind that we have recruited Mr. Unser and Ms. Knowles, two of the San Joaquin Valley finest tutoring specialists, to kick off this program. They have 15 years of experience between them, their specialties residing in English and Math, as well as SAT and college-admissions prep. We`ll announce a schedule soon, but in the meantime, interested students should sign up in Principal Teller`s office.
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td valign="top" width="160" id="templateSidebar"
                                                            style="border-collapse: collapse;background-color: #F4F4F4;border-left: 1px solid #FFFFFF;">
                                                            <table border="0" cellpadding="0" cellspacing="0"
                                                                   width="160" class="templateBanner">
                                                                <tr>
                                                                    <td height="6" width="6"
                                                                        style="border-collapse: collapse;">
                                                                        <img
                                                                            src="spacer.gif"
                                                                            width="6" height="6"
                                                                            style="display: block;margin: 0;padding: 0;height: 6px !important;border: 0;line-height: 100%;outline: none;text-decoration: none;">
                                                                    </td>
                                                                    <td align="center" rowspan="2" width="126"
                                                                        class="sidebarBannerContent"
                                                                        style="padding: 10px;border-collapse: collapse;background-color: #0E76BC;color: #FFFFFF;font-family: Georgia;font-size: 13px;line-height: 150%;text-align: center;text-decoration: none;">
                                                                        UPCOMING EVENTS
                                                                    </td>
                                                                    <td height="6" width="6"
                                                                        style="border-collapse: collapse;">
                                                                        <img
                                                                            src="spacer.gif"
                                                                            width="6" height="6"
                                                                            style="display: block;margin: 0;padding: 0;height: 6px !important;border: 0;line-height: 100%;outline: none;text-decoration: none;">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="6"
                                                                        style="background-color: #505050;border-collapse: collapse;">
                                                                        <br>
                                                                    </td>
                                                                    <td width="6"
                                                                        style="background-color: #505050;border-collapse: collapse;">
                                                                        <br>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="3"
                                                                        style="padding-bottom: 15px;border-collapse: collapse;">
                                                                        <table border="0" cellpadding="0"
                                                                               cellspacing="0" width="160">
                                                                            <tr>
                                                                                <td height="6" width="14"
                                                                                    style="background-color: #505050;border-collapse: collapse;">
                                                                                    <img
                                                                                        src="spacer.gif"
                                                                                        width="14" height="6"
                                                                                        style="display: block;margin: 0;padding: 0;height: 6px !important;border: 0;line-height: 100%;outline: none;text-decoration: none;">
                                                                                </td>
                                                                                <td height="6" width="132"
                                                                                    style="border-collapse: collapse;">
                                                                                    <img
                                                                                        src="spacer.gif"
                                                                                        width="132" height="6"
                                                                                        style="display: block;margin: 0;padding: 0;height: 6px !important;border: 0;line-height: 100%;outline: none;text-decoration: none;">
                                                                                </td>
                                                                                <td height="6" width="14"
                                                                                    style="background-color: #505050;border-collapse: collapse;">
                                                                                    <img
                                                                                        src="spacer.gif"
                                                                                        width="14" height="6"
                                                                                        style="display: block;margin: 0;padding: 0;height: 6px !important;border: 0;line-height: 100%;outline: none;text-decoration: none;">
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table border="0" cellpadding="0" cellspacing="0"
                                                                   width="160" mc:repeatable="repeat_1"
                                                                   mc:repeatindex="0" mc:hideable="hideable_repeat_1_1"
                                                                   mchideable="hideable_repeat_1_1">
                                                                <tr>
                                                                    <td valign="top" class="sidebarContent"
                                                                        style="padding-bottom: 20px;border-collapse: collapse;color: #707070;font-family: Helvetica;font-size: 13px;line-height: 150%;text-align: left;">
                                                                        <h4
                                                                            style="color: #0E76BC;display: block;font-family: Georgia;font-size: 13px;font-style: normal;font-weight: normal;line-height: 100%;letter-spacing: normal;margin-top: 0;margin-right: 0;margin-bottom: 10px;margin-left: 0;text-align: left;">
                                                                            April 14, 7p
                                                                        </h4>
                                                                        Track Meet
                                                                        <br>
                                                                            at Delano High
                                                                            <br>
                                                                                <br>
                                                                                    <h4
                                                                                        style="color: #0E76BC;display: block;font-family: Georgia;font-size: 13px;font-style: normal;font-weight: normal;line-height: 100%;letter-spacing: normal;margin-top: 0;margin-right: 0;margin-bottom: 10px;margin-left: 0;text-align: left;">
                                                                                        April 15, 3p
                                                                                    </h4>
                                                                                    Swim Meet
                                                                                    <br>
                                                                                        at Charming Natatorium
                                                                                        <br>
                                                                                            <br>
                                                                                                <h4
                                                                                                    style="color: #0E76BC;display: block;font-family: Georgia;font-size: 13px;font-style: normal;font-weight: normal;line-height: 100%;letter-spacing: normal;margin-top: 0;margin-right: 0;margin-bottom: 10px;margin-left: 0;text-align: left;">
                                                                                                    April 21, 10a
                                                                                                </h4>
                                                                                                Honor Society Brunch
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top" style="border-collapse: collapse;">
                                    <table border="0" cellpadding="20" cellspacing="0" width="600" id="templateFooter"
                                           style="background-color: #231F20;border-top: 1px solid #FFFFFF;">
                                        <tr>
                                            <td align="center" valign="top" style="border-collapse: collapse;">
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td valign="top" class="footerContent"
                                                            style="padding-bottom: 20px;border-collapse: collapse;color: #AAAAAA;font-family: Helvetica;font-size: 10px;line-height: 150%;text-align: center;">
                                                            <a href="#"
                                                               style="color: #CCCCCC;font-weight: normal;text-decoration: underline;">
                                                                Follow on Twitter
                                                            </a>
                                                            |
                                                            <a href="#"
                                                               style="color: #CCCCCC;font-weight: normal;text-decoration: underline;">
                                                                Friend on Facebook
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top" class="footerContent"
                                                            style="border-collapse: collapse;color: #AAAAAA;font-family: Helvetica;font-size: 10px;line-height: 150%;text-align: center;">
                                                            <em>Copyright В©<span id="year"></span>, All rights reserved.</em>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </center>
		<script type="text/javascript">
			document.getElementById("year").innerHTML = new Date().getFullYear();
		</script>
    </body>
</html>')	
END

IF NOT EXISTS (SELECT 1 FROM [text_templates] WHERE name = 'Event [pop-up only]')
BEGIN
INSERT [text_templates] ([name], [template_text]) VALUES ( N'Event [pop-up only]', N'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>DeskAlerts</title>
		
    <style type="text/css">
		#outlook a{
			padding:0;
		}
		body{
			width:100% !important;
		}
		.ReadMsgBody{
			width:100%;
		}
		.ExternalClass{
			width:100%;
		}
		body{
			-webkit-text-size-adjust:none;
		}
		body{
			margin:0;
			padding:0;
		}
		img{
			border:0;
			height:auto;
			line-height:100%;
			outline:none;
			text-decoration:none;
		}
		table td{
			border-collapse:collapse;
		}
		#bodyTable{
			height:100% !important;
			margin:0;
			padding:0;
			width:100% !important;
		}
		h1{
			color:#202020;
			display:block;
			font-family:Helvetica;
			font-size:26px;
			font-style:normal;
			font-weight:bold;
			line-height:100%;
			letter-spacing:normal;
			margin-top:0;
			margin-right:0;
			margin-bottom:10px;
			margin-left:0;
			text-align:left;
		}
		h2{
			color:#404040;
			display:block;
			font-family:Helvetica;
			font-size:20px;
			font-style:normal;
			font-weight:bold;
			line-height:100%;
			letter-spacing:normal;
			margin-top:0;
			margin-right:0;
			margin-bottom:10px;
			margin-left:0;
			text-align:left;
		}
		h3{
			color:#606060;
			display:block;
			font-family:Helvetica;
			font-size:16px;
			font-style:normal;
			font-weight:bold;
			line-height:100%;
			letter-spacing:normal;
			margin-top:0;
			margin-right:0;
			margin-bottom:10px;
			margin-left:0;
			text-align:left;
		}
		h4{
			color:#808080;
			display:block;
			font-family:Helvetica;
			font-size:12px;
			font-style:normal;
			font-weight:bold;
			line-height:100%;
			letter-spacing:normal;
			margin-top:0;
			margin-right:0;
			margin-bottom:10px;
			margin-left:0;
			text-align:left;
		}
		#templatePreheader{
			background-color:#000000;
			border-bottom:0;
		}
		.preheaderContent{
			color:#FFFFFF;
			font-family:Helvetica;
			font-size:10px;
			line-height:125%;
			text-align:left;
		}
		.preheaderContent a:link,.preheaderContent a:visited,.preheaderContent a .yshortcuts {
			color:#E60101;
			font-weight:normal;
			text-decoration:underline;
		}
		#templateHeader{
			background-color:#F4F4F4;
			border-top:0;
			border-bottom:0;
		}
		.headerContent{
			color:#505050;
			font-family:Helvetica;
			font-size:20px;
			font-weight:bold;
			line-height:100%;
			padding-top:0;
			padding-right:0;
			padding-bottom:0;
			padding-left:0;
			text-align:left;
			vertical-align:middle;
		}
		.headerContent a:link,.headerContent a:visited,.headerContent a .yshortcuts {
			color:#EB4102;
			font-weight:normal;
			text-decoration:underline;
		}
		#headerImage{
			height:auto;
			max-width:600px !important;
		}
		.templateBody{
			background-color:#F4F4F4;
		}
		.calendarTitleBar{
			background-color:#D00000;
			border-bottom:1px solid #B00000;
		}
		.calendarTitleBarContent,.calendarTitleBarContent a:link,.calendarTitleBarContent a:visited,.calendarTitleBarContent a .yshortcuts{
			color:#FFFFFF;
			font-family:Helvetica;
			font-size:45px;
			font-style:normal;
			font-weight:bold;
			line-height:100%;
			letter-spacing:-1px;
			text-align:center;
			text-decoration:none;
		}
		#calendarContentBlock{
			-moz-border-radius:0 0 10px 10px;
			-webkit-border-radius:0 0 10px 10px;
			background-color:#FFFFFF;
			border:1px solid #E8E8E8;
			border-radius:0 0 10px 10px;
		}
		.calendarContent,.calendarContent a:link,.calendarContent a:visited,.calendarContent a .yshortcuts{
			color:#303030;
			font-family:Helvetica;
			font-size:175px;
			font-style:normal;
			font-weight:bold;
			line-height:100%;
			letter-spacing:-5px;
			text-align:center;
			text-decoration:none;
		}
		.calendarTitleBar h1,.calendarTitleBar h2,.calendarTitleBar h3,.calendarTitleBar h4,.calendarContent h1,.calendarContent h2,.calendarContent h3,.calendarContent h4{
			line-height:100% !important;
			margin:0 !important;
		}
		.contentTitleBar{
			background-color:#909090;
			border-bottom:1px solid #707070;
		}
		.contentTitleBarContent{
			color:#FFFFFF;
			font-family:Helvetica;
			font-size:20px;
			font-style:normal;
			font-weight:bold;
			line-height:100%;
			letter-spacing:normal;
			text-align:left;
		}
		#bodyContentBlock{
			-moz-border-radius:0 0 10px 10px;
			-webkit-border-radius:0 0 10px 10px;
			background-color:#FFFFFF;
			border:1px solid #E8E8E8;
			border-radius:0 0 10px 10px;
		}
		.bodyContent{
			color:#505050;
			font-family:Helvetica;
			font-size:13px;
			line-height:150%;
			text-align:left;
		}
		.bodyContent a:link,.bodyContent a:visited,.bodyContent a .yshortcuts {
			color:#D00000;
			font-weight:normal;
			text-decoration:underline;
		}
		.contentTitleBar h1,.contentTitleBar h2,.contentTitleBar h3,.contentTitleBar h4{
			line-height:100% !important;
			margin:0 !important;
		}
		.bodyContent img{
			display:inline;
			height:auto;
		}
		#templateFooter{
			border-top:0;
		}
		body,#bodyTable{
			background-color:#F4F4F4;
		}
		.footerContent{
			color:#808080;
			font-family:Helvetica;
			font-size:10px;
			line-height:150%;
			text-align:left;
		}
		.footerContent a:link,.footerContent a:visited,.footerContent a .yshortcuts {
			color:#606060;
			font-weight:normal;
			text-decoration:underline;
		}
		.footerContent img{
			display:inline;
		}
		#monkeyRewards img{
			max-width:190px !important;
		}
</style></head>
    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0" style="-webkit-text-size-adjust: none;margin: 0;padding: 0;background-color: #F4F4F4;width: 100% !important;">
    	<center>
        	<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="margin: 0;padding: 0;background-color: #F4F4F4;height: 100% !important;width: 100% !important;">
            	<tr>
                	<td align="center" valign="top" style="border-collapse: collapse;">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateContainer">
                        	<tr>
                            	<td align="center" valign="top" style="border-collapse: collapse;">
                                	<table border="0" cellpadding="20" cellspacing="0" width="100%" id="templateHeader" style="background-color: #F4F4F4;border-top: 0;border-bottom: 0;">
                                    	<tr>
                                        	<td align="center" valign="top" style="border-collapse: collapse;">
                                            	<table border="0" cellpadding="0" cellspacing="0" width="600">
                                                	<tr>
                                                        <td class="headerContent" style="border-collapse: collapse;color: #505050;font-family: Helvetica;font-size: 20px;font-weight: bold;line-height: 100%;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;text-align: left;vertical-align: middle;">
                                                            
															          
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        	<tr>
                            	<td align="center" valign="top" style="border-collapse: collapse;">
                                	<table border="0" cellpadding="40" cellspacing="0" width="100%">
                                    	<tr>
                                        	<td align="center" valign="top" class="templateBody" style="border-collapse: collapse;background-color: #F4F4F4;">
                                            	<table border="0" cellpadding="30" cellspacing="0" width="500" id="calendarContentBlock" style="-moz-border-radius: 0 0 10px 10px;-webkit-border-radius: 0 0 10px 10px;background-color: #FFFFFF;border: 1px solid #E8E8E8;border-radius: 0 0 10px 10px;">
                                                	<tr>
                                                        <td align="center" valign="top" class="calendarTitleBar calendarTitleBarContent" style="border-collapse: collapse;background-color: #D00000;border-bottom: 1px solid #B00000;color: #FFFFFF;font-family: Helvetica;font-size: 45px;font-style: normal;font-weight: bold;line-height: 100%;letter-spacing: -1px;text-align: center;text-decoration: none;">
                                                        	SEPTEMBER
                                                        </td>
                                                    </tr>
                                                	<tr>
                                                        <td align="center" valign="top" class="calendarContent" style="border-collapse: collapse;color: #303030;font-family: Helvetica;font-size: 175px;font-style: normal;font-weight: bold;line-height: 100%;letter-spacing: -5px;text-align: center;text-decoration: none;">
                                                            25
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    	<tr>
                                        	<td align="center" valign="top" class="templateBody" style="padding-top: 0;border-collapse: collapse;background-color: #F4F4F4;">
                                            	<table border="0" cellpadding="20" cellspacing="0" width="500" id="bodyContentBlock" style="-moz-border-radius: 0 0 10px 10px;-webkit-border-radius: 0 0 10px 10px;background-color: #FFFFFF;border: 1px solid #E8E8E8;border-radius: 0 0 10px 10px;">
                                                	<tr>
                                                        <td valign="top" class="contentTitleBar contentTitleBarContent" style="padding-top: 10px;padding-bottom: 10px;border-collapse: collapse;background-color: #909090;border-bottom: 1px solid #707070;color: #FFFFFF;font-family: Helvetica;font-size: 20px;font-style: normal;font-weight: bold;line-height: 100%;letter-spacing: normal;text-align: left;">
                                                        	Save the Date
                                                        </td>
                                                    </tr>
                                                	<tr>
                                                        <td align="center" valign="top" style="border-collapse: collapse;">
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td valign="top" class="bodyContent" style="padding-bottom: 20px;border-collapse: collapse;color: #505050;font-family: Helvetica;font-size: 13px;line-height: 150%;text-align: left;">
                                                                        <h1 style="color: #202020;display: block;font-family: Helvetica;font-size: 26px;font-style: normal;font-weight: bold;line-height: 100%;letter-spacing: normal;margin-top: 0;margin-right: 0;margin-bottom: 10px;margin-left: 0;text-align: left;">Octane Happy Hour</h1>
                                                                        Join us after work for some iced coffee and $2 beers at Octane. Folks start arriving around 4pm and stay until the sun goes down. It is the best possible way to beat rush-hour traffic!
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        	<tr>
                            	<td align="center" valign="top" style="border-collapse: collapse;">
                                	<table border="0" cellpadding="40" cellspacing="0" width="100%" id="templateFooter" style="border-top: 0;">
                                    	<tr>
                                        	<td align="center" valign="top" style="padding-bottom: 40px;border-collapse: collapse;">
                                            	<table border="0" cellpadding="0" cellspacing="0" width="600">
                                                    <tr>
                                                    	<td colspan="3" valign="top" class="footerContent" style="padding-bottom: 20px;border-collapse: collapse;color: #808080;font-family: Helvetica;font-size: 10px;line-height: 150%;text-align: left;">
                                                        	<a href="#" style="color: #606060;font-weight: normal;text-decoration: underline;">Follow on Twitter</a> | <a href="#" style="color: #606060;font-weight: normal;text-decoration: underline;">Friend on Facebook</a>
                                                        </td>
                                                    </tr>
                                                	<tr>
                                                        <td valign="top" class="footerContent" style="border-collapse: collapse;color: #808080;font-family: Helvetica;font-size: 10px;line-height: 150%;text-align: left;">
                                                            <em>Copyright В© <span id="year"></span>, All rights reserved.</em>
                                                        </td>
                                                        <td width="20" style="border-collapse: collapse;">
                                                        	<br>
                                                        </td>
                                                        <td valign="top" width="200" id="monkeyRewards" class="footerContent" style="border-collapse: collapse;color: #808080;font-family: Helvetica;font-size: 10px;line-height: 150%;text-align: left;">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                    	<td colspan="3" valign="top" class="footerContent" style="padding-top: 20px;border-collapse: collapse;color: #808080;font-family: Helvetica;font-size: 10px;line-height: 150%;text-align: left;">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </center>
		<script type="text/javascript">
			document.getElementById("year").innerHTML = new Date().getFullYear();
		</script>
    </body>
</html>')
END

IF NOT EXISTS (SELECT 1 FROM [text_templates] WHERE name = 'Monthly digest [pop-up only]')
BEGIN
INSERT [text_templates] ([name], [template_text]) VALUES ( N'Monthly digest [pop-up only]', N'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>DeskAlerts</title>
		
    <style type="text/css">
		#outlook a{
			padding:0;
		}
		body{
			width:100% !important;
		}
		.ReadMsgBody{
			width:100%;
		}
		.ExternalClass{
			width:100%;
		}
		body{
			-webkit-text-size-adjust:none;
		}
		body{
			margin:0;
			padding:0;
		}
		img{
			border:0;
			height:auto;
			line-height:100%;
			outline:none;
			text-decoration:none;
		}
		table td{
			border-collapse:collapse;
		}
		#bodyTable{
			height:100% !important;
			margin:0;
			padding:0;
			width:100% !important;
		}
		h1{
			color:#202020;
			display:block;
			font-family:Helvetica;
			font-size:36px;
			font-style:normal;
			font-weight:bold;
			line-height:100%;
			letter-spacing:normal;
			margin-top:0;
			margin-right:0;
			margin-bottom:10px;
			margin-left:0;
			text-align:left;
		}
		h2{
			color:#404040;
			display:block;
			font-family:Helvetica;
			font-size:20px;
			font-style:normal;
			font-weight:bold;
			line-height:100%;
			letter-spacing:normal;
			margin-top:0;
			margin-right:0;
			margin-bottom:10px;
			margin-left:0;
			text-align:left;
		}
		h3{
			color:#606060;
			display:block;
			font-family:Helvetica;
			font-size:16px;
			font-style:normal;
			font-weight:bold;
			line-height:100%;
			letter-spacing:normal;
			margin-top:0;
			margin-right:0;
			margin-bottom:10px;
			margin-left:0;
			text-align:left;
		}
		h4{
			color:#808080;
			display:block;
			font-family:Helvetica;
			font-size:12px;
			font-style:normal;
			font-weight:bold;
			line-height:100%;
			letter-spacing:normal;
			margin-top:0;
			margin-right:0;
			margin-bottom:10px;
			margin-left:0;
			text-align:left;
		}
		#templatePreheader{
			background-color:#000000;
			border-bottom:0;
		}
		.preheaderContent{
			color:#FFFFFF;
			font-family:Helvetica;
			font-size:10px;
			line-height:125%;
			text-align:left;
		}
		.preheaderContent a:link,.preheaderContent a:visited,.preheaderContent a .yshortcuts {
			color:#E60101;
			font-weight:normal;
			text-decoration:underline;
		}
		#templateHeader{
			background-color:#F4F4F4;
			border-top:0;
			border-bottom:1px solid #E5E5E5;
		}
		.headerContent{
			color:#505050;
			font-family:Helvetica;
			font-size:20px;
			font-weight:bold;
			line-height:100%;
			padding-top:0;
			padding-right:0;
			padding-bottom:0;
			padding-left:0;
			text-align:left;
			vertical-align:middle;
		}
		.headerContent a:link,.headerContent a:visited,.headerContent a .yshortcuts {
			color:#EB4102;
			font-weight:normal;
			text-decoration:underline;
		}
		#headerImage{
			height:auto;
			max-width:600px !important;
		}
		.templateBody{
			background-color:#F4F4F4;
			border-top:1px solid #FFFFFF;
			border-bottom:1px solid #CCCCCC;
		}
		.calendarTitleBar{
			background-color:#D00000;
			border-bottom:1px solid #B00000;
		}
		.calendarTitleBarContent,.calendarTitleBarContent a:link,.calendarTitleBarContent a:visited,.calendarTitleBarContent a .yshortcuts{
			color:#FFFFFF;
			font-family:Helvetica;
			font-size:20px;
			font-style:normal;
			font-weight:bold;
			line-height:100%;
			letter-spacing:normal;
			text-align:center;
			text-decoration:none;
		}
		#calendarContentBlock{
			-moz-border-radius:0 0 10px 10px;
			-webkit-border-radius:0 0 10px 10px;
			background-color:#FFFFFF;
			border:1px solid #E8E8E8;
			border-radius:0 0 10px 10px;
		}
		.calendarContent,.calendarContent a:link,.calendarContent a:visited,.calendarContent a .yshortcuts{
			color:#303030;
			font-family:Helvetica;
			font-size:80px;
			font-style:normal;
			font-weight:bold;
			line-height:100%;
			letter-spacing:normal;
			text-align:center;
			text-decoration:none;
		}
		.calendarTitleBar h1,.calendarTitleBar h2,.calendarTitleBar h3,.calendarTitleBar h4,.calendarContent h1,.calendarContent h2,.calendarContent h3,.calendarContent h4{
			line-height:100% !important;
			margin:0 !important;
		}
		.bodyContentBlock{
			border-top:1px solid #CCCCCC;
		}
		.bodyContent{
			color:#505050;
			font-family:Helvetica;
			font-size:13px;
			line-height:150%;
			text-align:left;
		}
		.bodyContent a:link,.bodyContent a:visited,.bodyContent a .yshortcuts {
			color:#D00000;
			font-weight:normal;
			text-decoration:underline;
		}
		.contentTitleBar h1,.contentTitleBar h2,.contentTitleBar h3,.contentTitleBar h4{
			line-height:100% !important;
			margin:0 !important;
		}
		.bodyContent img{
			display:inline;
			height:auto;
		}
		#templateFooter{
			border-top:1px solid #FFFFFF;
		}
		body,#bodyTable{
			background-color:#F4F4F4;
		}
		.footerContent{
			color:#808080;
			font-family:Helvetica;
			font-size:10px;
			line-height:150%;
			text-align:left;
		}
		.footerContent a:link,.footerContent a:visited,.footerContent a .yshortcuts {
			color:#606060;
			font-weight:normal;
			text-decoration:underline;
		}
		.footerContent img{
			display:inline;
		}
		#monkeyRewards img{
			max-width:190px !important;
		}
</style></head>
    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0" style="-webkit-text-size-adjust: none;margin: 0;padding: 0;background-color: #F4F4F4;width: 100% !important;">
    	<center>
        	<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="margin: 0;padding: 0;background-color: #F4F4F4;height: 100% !important;width: 100% !important;">
            	<tr>
                	<td align="center" valign="top" style="border-collapse: collapse;">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateContainer">
                        	<tr>
                            	<td align="center" valign="top" style="border-collapse: collapse;">
                                	<table border="0" cellpadding="20" cellspacing="0" width="100%" id="templateHeader" style="background-color: #F4F4F4;border-top: 0;border-bottom: 1px solid #E5E5E5;">
                                    	<tr>
                                        	<td align="center" valign="top" style="border-collapse: collapse;">
                                            	<table border="0" cellpadding="0" cellspacing="0" width="600">
                                                	<tr>
                                                        <td class="headerContent" style="border-collapse: collapse;color: #505050;font-family: Helvetica;font-size: 20px;font-weight: bold;line-height: 100%;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;text-align: left;vertical-align: middle;">
                                                            		  
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        	<tr>
                            	<td align="center" valign="top" style="border-collapse: collapse;">
                                	<table border="0" cellpadding="40" cellspacing="0" width="100%" class="templateBody" style="background-color: #F4F4F4;border-top: 1px solid #FFFFFF;border-bottom: 1px solid #CCCCCC;">
                                    	<tr>
                                        	<td align="center" valign="top" style="padding-bottom: 20px;border-collapse: collapse;">
                                            	<table border="0" cellpadding="0" cellspacing="0" width="600">
                                                	<tr>
                                                        <td colspan="3" valign="top" class="bodyContent" style="padding-bottom: 20px;border-collapse: collapse;color: #505050;font-family: Helvetica;font-size: 13px;line-height: 150%;text-align: left;">
                                                        	<h1 style="color: #202020;display: block;font-family: Helvetica;font-size: 36px;font-style: normal;font-weight: bold;line-height: 100%;letter-spacing: normal;margin-top: 0;margin-right: 0;margin-bottom: 10px;margin-left: 0;text-align: left;">Save the Date</h1>
                                                            We have got a couple of great events at Octane and Ormsby coming up in September that should be tons of fun for everyone involved. We have got all the details laid out for you. Check them out:
                                                        </td>
                                                    </tr>
                                                	<tr>
                                                        <td align="center" valign="top" width="180" style="border-collapse: collapse;">
                                                            <table border="0" cellpadding="0" cellspacing="0" width="180" class="bodyContentBlock" mc:repeatable="repeat_1" mc:repeatindex="0" mc:hideable="hideable_repeat_1_1" mchideable="hideable_repeat_1_1" style="border-top: 1px solid #CCCCCC;">
                                                                <tr>
                                                                	<td align="center" valign="top" style="padding-top: 20px;border-collapse: collapse;">
                                                                        <table border="0" cellpadding="10" cellspacing="0" width="180" id="calendarContentBlock" style="-moz-border-radius: 0 0 10px 10px;-webkit-border-radius: 0 0 10px 10px;background-color: #FFFFFF;border: 1px solid #E8E8E8;border-radius: 0 0 10px 10px;">
                                                                            <tr>
                                                                                <td align="center" valign="top" class="calendarTitleBar calendarTitleBarContent" style="border-collapse: collapse;background-color: #D00000;border-bottom: 1px solid #B00000;color: #FFFFFF;font-family: Helvetica;font-size: 20px;font-style: normal;font-weight: bold;line-height: 100%;letter-spacing: normal;text-align: center;text-decoration: none;">
                                                                                    SEPTEMBER
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="center" valign="top" class="calendarContent" style="padding-top: 20px;padding-bottom: 15px;border-collapse: collapse;color: #303030;font-family: Helvetica;font-size: 80px;font-style: normal;font-weight: bold;line-height: 100%;letter-spacing: normal;text-align: center;text-decoration: none;">
                                                                                    27
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="3" valign="top" class="bodyContent" style="padding-top: 20px;padding-bottom: 20px;border-collapse: collapse;color: #505050;font-family: Helvetica;font-size: 13px;line-height: 150%;text-align: left;">
                                                                    	<h2 style="color: #404040;display: block;font-family: Helvetica;font-size: 20px;font-style: normal;font-weight: bold;line-height: 100%;letter-spacing: normal;margin-top: 0;margin-right: 0;margin-bottom: 10px;margin-left: 0;text-align: left;">Bocce League At Ormsby</h2>
                                                                        Join us after work for some iced coffee and $2 beers at Octane. Folks start arriving around 4pm and stay until the sun goes down. It is the best possible way to beat rush-hour traffic!
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td width="40" style="border-collapse: collapse;">
                                                        	<br>
                                                        </td>
                                                        <td align="center" valign="top" width="380" style="border-collapse: collapse;">
                                                        	<table border="0" cellpadding="0" cellspacing="0" width="380">
                                                            	<tr>
                                                                	<td valign="top" class="bodyContentBlock bodyContent" style="padding-top: 20px;padding-bottom: 20px;border-collapse: collapse;border-top: 1px solid #CCCCCC;color: #505050;font-family: Helvetica;font-size: 13px;line-height: 150%;text-align: left;">
                                                                    	<h2 style="color: #404040;display: block;font-family: Helvetica;font-size: 20px;font-style: normal;font-weight: bold;line-height: 100%;letter-spacing: normal;margin-top: 0;margin-right: 0;margin-bottom: 10px;margin-left: 0;text-align: left;">Thanks, from Generitech</h2>
                                                                        As the year comes to a close, I had like to take the opportunity to thank you, Generitech loyal supporters, for your continued commitment to experiencing the difference with our powerful products and solutions.
                                                                        <br>
                                                                        <br>
                                                                        I have learned a lot from your feedback this year, and from the countless surveys and polls Generitech has conducted. I have learned that you are leaders in your industries. Your innovation is both groundbreaking and wave-making. I have learned that streamlining your company communication efforts is your #1 priority.
                                                                        <br>
                                                                        <br>
                                                                        Follow us on Twitter at mailchimp.com, and let us know how we are doing.
                                                                        <br>
                                                                        <br>
                                                                        Thanks for a great year,
                                                                        <br>
                                                                        <br>
                                                                        <em>Gary Terry</em>
                                                                        <br>
                                                                        President, Generitech Solutions
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        	<tr>
                            	<td align="center" valign="top" style="border-collapse: collapse;">
                                	<table border="0" cellpadding="40" cellspacing="0" width="100%" id="templateFooter" style="border-top: 1px solid #FFFFFF;">
                                    	<tr>
                                        	<td align="center" valign="top" style="padding-bottom: 40px;border-collapse: collapse;">
                                            	<table border="0" cellpadding="0" cellspacing="0" width="600">
                                                    <tr>
                                                    	<td colspan="3" valign="top" class="footerContent" style="padding-bottom: 20px;border-collapse: collapse;color: #808080;font-family: Helvetica;font-size: 10px;line-height: 150%;text-align: left;">
                                                        	<a href="#" style="color: #606060;font-weight: normal;text-decoration: underline;">Follow on Twitter</a> | <a href="#" style="color: #606060;font-weight: normal;text-decoration: underline;">Friend on Facebook</a>
                                                        </td>
                                                    </tr>
                                                	<tr>
                                                        <td valign="top" class="footerContent" style="border-collapse: collapse;color: #808080;font-family: Helvetica;font-size: 10px;line-height: 150%;text-align: left;">
                                                            <em>Copyright В© <span id="year"></span>, All rights reserved.</em>
                                                        </td>
                                                        <td width="20" style="border-collapse: collapse;">
                                                        	<br>
                                                        </td>
                                                        <td valign="top" width="200" id="monkeyRewards" class="footerContent" style="border-collapse: collapse;color: #808080;font-family: Helvetica;font-size: 10px;line-height: 150%;text-align: left;">  
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                    	<td colspan="3" valign="top" class="footerContent" style="padding-top: 20px;border-collapse: collapse;color: #808080;font-family: Helvetica;font-size: 10px;line-height: 150%;text-align: left;">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </center>
        <script type="text/javascript">
			document.getElementById("year").innerHTML = new Date().getFullYear();
		</script>
    </body>
</html>')
END

	

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'toolbars' )
	CREATE TABLE [toolbars] (
		[id] [bigint] 
		IDENTITY (1,1), 
		[tbid] [varchar] (100), 
		[user_id] [bigint], 
		[after_install_page] [varchar] (255), 
		[after_uninstall_page] [varchar] (255), 
		[name] [varchar] (255),
		CONSTRAINT [PK_toolbars] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY]
ELSE IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_toolbars]') AND type = 'K')
	ALTER TABLE [dbo].[toolbars] ADD CONSTRAINT [PK_toolbars] PRIMARY KEY CLUSTERED ([id] ASC)
	
IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'urls' )
	CREATE TABLE [urls] (
		[id] [bigint] IDENTITY (1,1) NOT NULL, 
		[url] [varchar] (255),
		[toolbar_id] [bigint], 
		[search] [int], 
		[keywords] [varchar] (255),
		CONSTRAINT [PK_urls] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY];
ELSE IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_urls]') AND type = 'K')
	ALTER TABLE [dbo].[urls] ADD CONSTRAINT [PK_urls] PRIMARY KEY CLUSTERED ([id] ASC)
	
IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'tb_install_stat' )
	CREATE TABLE [tb_install_stat] (
		[id] [bigint] IDENTITY (1,1), 
		[toolbar_id] [bigint], 
		[date] [datetime],
		CONSTRAINT [PK_tb_install_stat] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY]
ELSE IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_tb_install_stat]') AND type = 'K')
	ALTER TABLE [dbo].[tb_install_stat] ADD CONSTRAINT [PK_tb_install_stat] PRIMARY KEY CLUSTERED ([id] ASC)

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'tb_uninstall_stat' )
	CREATE TABLE [tb_uninstall_stat] (
		[id] [bigint] IDENTITY (1,1), 
		[toolbar_id] [bigint], 
		[date] [datetime],
		CONSTRAINT [PK_tb_uninstall_stat] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY]
ELSE IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_tb_uninstall_stat]') AND type = 'K')
	ALTER TABLE [dbo].[tb_uninstall_stat] ADD CONSTRAINT [PK_tb_uninstall_stat] PRIMARY KEY CLUSTERED ([id] ASC)

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'url_stat' )
	CREATE TABLE [url_stat] (
		[id] [bigint] IDENTITY (1,1), 
		[toolbar_id] [bigint], 
		[url_id] [bigint], 
		[date] [datetime],
		CONSTRAINT [PK_url_stat] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY]
ELSE IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_url_stat]') AND type = 'K')
	ALTER TABLE [dbo].[url_stat] ADD CONSTRAINT [PK_url_stat] PRIMARY KEY CLUSTERED ([id] ASC)

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'users_deskbar' )
	CREATE TABLE [users_deskbar] (
		[id] [bigint] IDENTITY (1,1) NOT NULL, 
		[user_id] [int], 
		[deskbar_id] [varchar] (255),
		CONSTRAINT [PK_users_deskbar] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY];
ELSE IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_users_deskbar]') AND type = 'K')
	ALTER TABLE [dbo].[users_deskbar] ADD CONSTRAINT [PK_users_deskbar] PRIMARY KEY CLUSTERED ([id] ASC)
	
IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'alerts_sent_deskbar' )
	CREATE TABLE [alerts_sent_deskbar] (
		[id] [bigint] IDENTITY (1,1) NOT NULL, 
		[alert_id] [int], 
		[desk_id] [int],
		CONSTRAINT [PK_alerts_sent_deskbar] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY]
ELSE IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_alerts_sent_deskbar]') AND type = 'K')
	ALTER TABLE [dbo].[alerts_sent_deskbar] ADD CONSTRAINT [PK_alerts_sent_deskbar] PRIMARY KEY CLUSTERED ([id] ASC)	

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'alerts_sent_group' )
	CREATE TABLE [alerts_sent_group] (
		[id] [bigint] IDENTITY (1,1) NOT NULL, 
		[alert_id] [bigint], 
		[group_id] [bigint],
		CONSTRAINT [PK_alerts_sent_group] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY]
ELSE IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_alerts_sent_group]') AND type = 'K')
	ALTER TABLE [dbo].[alerts_sent_group] ADD CONSTRAINT [PK_alerts_sent_group] PRIMARY KEY CLUSTERED ([id] ASC)	

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'alerts_sent_ou' )
	CREATE TABLE [alerts_sent_ou] (
		[id] [bigint] IDENTITY (1,1) NOT NULL, 
		[alert_id] [bigint], 
		[ou_id] [bigint],
		CONSTRAINT [PK_alerts_sent_ou] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY]
ELSE IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_alerts_sent_ou]') AND type = 'K')
	ALTER TABLE [dbo].[alerts_sent_ou] ADD CONSTRAINT [PK_alerts_sent_ou] PRIMARY KEY CLUSTERED ([id] ASC)	

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'surveys_questions' )
	CREATE TABLE [surveys_questions] (
		[id] [bigint] IDENTITY (1,1) NOT NULL, 
		[question] [varchar] (max), 
		[survey_id] [bigint], 
		[question_number] [bigint],
		question_type [varchar] (1),
		CONSTRAINT [PK_surveys_questions] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY]
ELSE IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_surveys_questions]') AND type = 'K')
	ALTER TABLE [dbo].[surveys_questions] ADD CONSTRAINT [PK_surveys_questions] PRIMARY KEY CLUSTERED ([id] ASC)	

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'surveys_answers_stat' )
	CREATE TABLE [surveys_answers_stat] (
		[id] [bigint] IDENTITY (1,1) NOT NULL, 
		[answer_id] [bigint], 
		[user_id] [bigint], 
		[date] [datetime], 
		[clsid] [varchar] (255),
		CONSTRAINT [PK_surveys_answers_stat] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY]
ELSE IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_surveys_answers_stat]') AND type = 'K')
	ALTER TABLE [dbo].[surveys_answers_stat] ADD CONSTRAINT [PK_surveys_answers_stat] PRIMARY KEY CLUSTERED ([id] ASC)	
	
IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'surveys_custom_answers_stat' )
	CREATE TABLE [surveys_custom_answers_stat] (
		[id] [bigint] IDENTITY (1,1) NOT NULL, 
		[question_id] [bigint], 
		[user_id] [bigint], 
		[date] [datetime], 
		[clsid] [varchar] (255), 
		[answer] [varchar] (max),
		CONSTRAINT [PK_surveys_custom_answers_stat] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY]
ELSE IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_surveys_custom_answers_stat]') AND type = 'K')
	ALTER TABLE [dbo].[surveys_custom_answers_stat] ADD CONSTRAINT [PK_surveys_custom_answers_stat] PRIMARY KEY CLUSTERED ([id] ASC)	

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'OU' )
	CREATE TABLE [OU] (
		[Id] [BIGINT] IDENTITY (1,1) NOT NULL, 
		[Id_domain] [BIGINT], 
		[Name] [VARCHAR](255), 
		[OU_PATH] [varchar] (max),
		[Was_checked] [VARCHAR](1), 
		[type] [varchar](155),
		CONSTRAINT [PK_OU] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY]
ELSE IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_OU]') AND type = 'K')
	ALTER TABLE [dbo].[OU] ADD CONSTRAINT [PK_OU] PRIMARY KEY CLUSTERED ([Id] ASC)
	
IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'OU_hierarchy' )
	CREATE TABLE [OU_hierarchy] (
		[PId] [BIGINT] IDENTITY (1,1) NOT NULL,
		[Id_parent] [BIGINT] NOT NULL, 
		[Id_child] [BIGINT] NOT NULL,
		CONSTRAINT [PK_OU_hierarchy] PRIMARY KEY CLUSTERED ([PId] ASC)
	) ON [PRIMARY]
ELSE IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE column_name = 'PId' AND table_name = 'OU_hierarchy' )
	ALTER TABLE [dbo].[OU_hierarchy] ADD [PId] [BIGINT] IDENTITY (1,1) NOT NULL
GO
IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_OU_hierarchy]') AND type = 'K')
	ALTER TABLE [dbo].[OU_hierarchy] ADD CONSTRAINT [PK_OU_hierarchy] PRIMARY KEY CLUSTERED ([PId] ASC)
GO

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'OU_synch' )
	CREATE TABLE [OU_synch] (
		[SId] [BIGINT] IDENTITY (1,1) NOT NULL,
		[Id_OU] [BIGINT] NOT NULL,
		[UID] [VARCHAR](128),
		CONSTRAINT [PK_OU_synch] PRIMARY KEY CLUSTERED ([SId] ASC)
	) ON [PRIMARY];
ELSE IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE column_name = 'SId' AND table_name = 'OU_synch' )
	ALTER TABLE [dbo].[OU_synch] ADD [SId] [BIGINT] IDENTITY (1,1) NOT NULL
GO
IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_OU_synch]') AND type = 'K')
	ALTER TABLE [dbo].[OU_synch] ADD CONSTRAINT [PK_OU_synch] PRIMARY KEY CLUSTERED ([SId] ASC)
GO

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'OU_DOMAIN' )
	CREATE TABLE [OU_DOMAIN] (
		[Id_domain] [BIGINT], 
		[Id_ou] [BIGINT] NOT NULL,
		CONSTRAINT [PK_OU_DOMAIN] PRIMARY KEY CLUSTERED ([Id_ou] ASC)
	) ON [PRIMARY]
IF NOT EXISTS ( SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_OU_DOMAIN]') AND type = 'K' )
	ALTER TABLE [dbo].[OU_DOMAIN] ALTER COLUMN [Id_ou] [BIGINT] NOT NULL
GO
IF NOT EXISTS ( SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_OU_DOMAIN]') AND type = 'K' )
	ALTER TABLE [dbo].[OU_DOMAIN] ADD CONSTRAINT [PK_OU_DOMAIN] PRIMARY KEY CLUSTERED ([Id_ou] ASC)	
GO

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'User_synch' )
	CREATE TABLE [User_synch] (
		[SId] [BIGINT] IDENTITY (1,1) NOT NULL,
		[Id_user] [BIGINT] NOT NULL,
		[UID] [VARCHAR](128),
		CONSTRAINT [PK_User_synch] PRIMARY KEY CLUSTERED ([Id_user] ASC)
	) ON [PRIMARY]
ELSE IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE column_name = 'SId' AND table_name = 'User_synch' )
	ALTER TABLE [dbo].[User_synch] ADD [SId] [BIGINT] IDENTITY (1,1) NOT NULL
GO
IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_User_synch]') AND type = 'K')
	ALTER TABLE [dbo].[User_synch] ADD CONSTRAINT [PK_User_synch] PRIMARY KEY CLUSTERED ([SId] ASC)
GO

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'OU_User' )
	CREATE TABLE [OU_User] (
		[PId] [BIGINT] IDENTITY (1,1) NOT NULL,
		[Id_user] [BIGINT] NOT NULL,
		[Id_OU] [BIGINT] NOT NULL,
		CONSTRAINT [PK_OU_User] PRIMARY KEY CLUSTERED ([PId] ASC)
	) ON [PRIMARY]
IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE column_name = 'PId' AND table_name = 'OU_User' )
	ALTER TABLE [dbo].[OU_User] ADD [PId] [BIGINT] IDENTITY (1,1) NOT NULL
GO
IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_OU_User]') AND type = 'K')
	ALTER TABLE [dbo].[OU_User] ADD CONSTRAINT [PK_OU_User] PRIMARY KEY CLUSTERED ([PId] ASC)

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'policy')
	CREATE TABLE [policy] (
		[id] [bigint] IDENTITY (1,1) NOT NULL, 
		[name] [varchar] (255), 
		[type] [varchar] (1),
        [full_skin_access] [bit] DEFAULT 1 NOT NULL,
        [full_contentSettings_access] [bit] DEFAULT 1 NOT NULL,
		CONSTRAINT [PK_policy] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY]
ELSE
BEGIN
    IF NOT EXISTS (
        SELECT 1 FROM SYS.COLUMNS 
        WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[policy]') AND name = 'full_skin_access'
    )
    BEGIN
	    ALTER TABLE [dbo].[policy] ADD [full_skin_access] [bit] DEFAULT 1 NOT NULL
    END

    IF NOT EXISTS (
        SELECT 1 FROM SYS.COLUMNS 
        WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[policy]') AND name = 'full_contentSettings_access'
    )
    BEGIN
	    ALTER TABLE [dbo].[policy] ADD [full_contentSettings_access] [bit] DEFAULT 1 NOT NULL
    END

    IF NOT EXISTS (
        SELECT 1 FROM dbo.sysobjects 
        WHERE id = OBJECT_ID(N'[PK_policy]') AND type = 'K'
    )
    BEGIN
        ALTER TABLE [dbo].[policy] ADD CONSTRAINT [PK_policy] PRIMARY KEY CLUSTERED ([id] ASC)
    END
END
	


GO

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'policy_user')
	CREATE TABLE [policy_user] (
		[id] [bigint] IDENTITY (1,1) NOT NULL, 
		[policy_id] [bigint], 
		[user_id] [bigint], 
		[was_checked] [int],
		CONSTRAINT [PK_policy_user] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY]
ELSE IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_policy_user]') AND type = 'K')
	ALTER TABLE [dbo].[policy_user] ADD CONSTRAINT [PK_policy_user] PRIMARY KEY CLUSTERED ([id] ASC)	

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'policy_group' )
	CREATE TABLE [policy_group] (
		[id] [bigint] IDENTITY (1,1) NOT NULL, 
		[policy_id] [bigint], 
		[group_id] [bigint], 
		[was_checked] [int],
		CONSTRAINT [PK_policy_group] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY]
ELSE IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_policy_group]') AND type = 'K')
	ALTER TABLE [dbo].[policy_group] ADD CONSTRAINT [PK_policy_group] PRIMARY KEY CLUSTERED ([id] ASC)

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'policy_ou' )
	CREATE TABLE [policy_ou] (
		[id] [bigint] IDENTITY (1,1) NOT NULL, 
		[policy_id] [bigint], 
		[ou_id] [bigint], 
		[was_checked] [int],
		CONSTRAINT [PK_policy_ou] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY]
ELSE IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_policy_ou]') AND type = 'K')
	ALTER TABLE [dbo].[policy_ou] ADD CONSTRAINT [PK_policy_ou] PRIMARY KEY CLUSTERED ([id] ASC)

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'policy_editor' )
	CREATE TABLE [policy_editor] (
		[id] [bigint] IDENTITY (1,1) NOT NULL, 
		[policy_id] [bigint],
		[editor_id] [bigint],
		[group_id] [bigint],
		[was_checked] [int],
		CONSTRAINT [PK_policy_editor] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY]
ELSE IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_policy_editor]') AND type = 'K')
	BEGIN
		ALTER TABLE [dbo].[policy_editor] ADD CONSTRAINT [PK_policy_editor] PRIMARY KEY CLUSTERED ([id] ASC)		
	END 

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'policy_viewer' )
	CREATE TABLE [policy_viewer] (
		[id] [bigint] IDENTITY (1,1) NOT NULL,
		[policy_id] [bigint], 
		[editor_id] [bigint],
		CONSTRAINT [PK_policy_viewer] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY]
ELSE IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_policy_viewer]') AND type = 'K')
	ALTER TABLE [dbo].[policy_viewer] ADD CONSTRAINT [PK_policy_viewer] PRIMARY KEY CLUSTERED ([id] ASC)

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'templates_preview' )
	CREATE TABLE [templates_preview] (
		[id] [bigint] IDENTITY (1,1) NOT NULL, 
		[top] [varchar] (max), [bottom] [varchar] (max), 
		[text_template] [varchar] (max), 
		[name] [varchar] (255),
		CONSTRAINT [PK_templates_preview] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY]
ELSE IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_templates_preview]') AND type = 'K')
	ALTER TABLE [dbo].[templates_preview] ADD CONSTRAINT [PK_templates_preview] PRIMARY KEY CLUSTERED ([id] ASC)	

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'computers' )
	CREATE TABLE [computers] (
		[id] [bigint] IDENTITY (1,1) NOT NULL, 
		[name] [varchar] (255), 
		[last_date] [datetime], 
		[reg_date] [datetime], 
		[last_request] [datetime], 
		[next_request] [varchar] (255),
		[domain_id] [int], 
		[context_id] [int], 
		[standby] [int], 
		[disabled] [int], 
		[was_checked] [int],
		CONSTRAINT [PK_computers] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY]
ELSE IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_computers]') AND type = 'K')
	ALTER TABLE [dbo].[computers] ADD CONSTRAINT [PK_computers] PRIMARY KEY CLUSTERED ([id] ASC)	

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'computers_groups' )
	CREATE TABLE [computers_groups] (
		[id] [bigint] IDENTITY (1,1) NOT NULL, 
		[group_id] [bigint], 
		[comp_id] [bigint], 
		[was_checked] [int],
		CONSTRAINT [PK_computers_groups] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY]
ELSE IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_computers_groups]') AND type = 'K')
	ALTER TABLE [dbo].[computers_groups] ADD CONSTRAINT [PK_computers_groups] PRIMARY KEY CLUSTERED ([id] ASC)	

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'alerts_sent_comp' )
	CREATE TABLE [alerts_sent_comp] (
		[id] [bigint] IDENTITY (1,1) NOT NULL,
		[alert_id] [bigint], 
		[comp_id] [bigint],
		CONSTRAINT [PK_alerts_sent_comp] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY]
ELSE IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_alerts_sent_comp]') AND type = 'K')
	ALTER TABLE [dbo].[alerts_sent_comp] ADD CONSTRAINT [PK_alerts_sent_comp] PRIMARY KEY CLUSTERED ([id] ASC)	
	
IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'alerts_comp_stat' )
	CREATE TABLE [alerts_comp_stat] (
		[id] [bigint] IDENTITY (1,1) NOT NULL, 
		[alert_id] [bigint], 
		[comp_id] [bigint], 
		[date] [datetime],
		CONSTRAINT [PK_alerts_comp_stat] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY]
ELSE IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_alerts_comp_stat]') AND type = 'K')
	ALTER TABLE [dbo].[alerts_comp_stat] ADD CONSTRAINT [PK_alerts_comp_stat] PRIMARY KEY CLUSTERED ([id] ASC)	
	
IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'OU_comp' )
	CREATE TABLE [OU_comp] (
		[PId] [BIGINT] IDENTITY (1,1) NOT NULL,
		[Id_comp] [BIGINT] NOT NULL,
		[Id_OU] [BIGINT] NOT NULL,
		CONSTRAINT [PK_OU_comp] PRIMARY KEY CLUSTERED ([PId] ASC)
	) ON [PRIMARY]
IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE column_name = 'PId' AND table_name = 'OU_comp' )
	ALTER TABLE [dbo].[OU_comp] ADD [PId] [BIGINT] IDENTITY (1,1) NOT NULL
GO
IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_OU_comp]') AND type = 'K')
	ALTER TABLE [dbo].[OU_comp] ADD CONSTRAINT [PK_OU_comp] PRIMARY KEY CLUSTERED ([PId] ASC)
GO

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'Comp_synch' )
	CREATE TABLE [Comp_synch] (
		[SId] [BIGINT] IDENTITY (1,1) NOT NULL,
		[Id_comp] [BIGINT] NOT NULL,
		[UID] [VARCHAR](128),
		CONSTRAINT [PK_Comp_synch] PRIMARY KEY CLUSTERED ([Id_comp] ASC)
	) ON [PRIMARY]
ELSE IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE column_name = 'SId' AND table_name = 'Comp_synch' )
	ALTER TABLE [dbo].[Comp_synch] ADD [SId] [BIGINT] IDENTITY (1,1) NOT NULL
GO
IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_Comp_synch]') AND type = 'K')
	ALTER TABLE [dbo].[Comp_synch] ADD CONSTRAINT [PK_Comp_synch] PRIMARY KEY CLUSTERED ([SId] ASC)
GO

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'alerts_sent_iprange' )
	CREATE TABLE [alerts_sent_iprange] (
		[id] [bigint] IDENTITY (1,1) NOT NULL, 
		[alert_id] [bigint],
		[ip_range_group_id] [bigint],
		[ip_from] [bigint], 
		[ip_to] [bigint],
		CONSTRAINT [PK_alerts_sent_iprange] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY]
ELSE IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_alerts_sent_iprange]') AND type = 'K')
	ALTER TABLE [dbo].[alerts_sent_iprange] ADD CONSTRAINT [PK_alerts_sent_iprange] PRIMARY KEY CLUSTERED ([id] ASC)	

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'policy_list' )
	CREATE TABLE [policy_list] (
		[id] [bigint] IDENTITY (1,1) NOT NULL,
		[policy_id] [bigint],
		[alerts_val] [varchar] (7),
		[emails_val] [varchar] (7),
		[sms_val] [varchar] (7),
		[surveys_val] [varchar] (7),
		[users_val] [varchar](7),
		[groups_val] [varchar] (7),
		[templates_val] [varchar] (7),
		[text_templates_val] [varchar] (7),
		[statistics_val] [varchar] (7),
		[ip_groups_val] [varchar] (7),
		[rss_val] [varchar] (7),
		[screensavers_val] [varchar] (7),
		[wallpapers_val] [varchar] (7),
		[lockscreens_val] [varchar] (7),
		[im_val] [varchar] (7),
		[text_to_call_val] [varchar] (7),
		CONSTRAINT [PK_policy_list] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY]
ELSE IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE column_name = 'id' AND table_name = 'policy_list' )
	ALTER TABLE [dbo].[policy_list] ADD [id] [bigint] IDENTITY (1,1) NOT NULL
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'policy_list' AND column_name = 'feedback_val')
    ALTER TABLE [dbo].[policy_list] ADD [feedback_val] [varchar](7) NULL
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'policy_list' AND column_name = 'cc_val')
    ALTER TABLE [dbo].[policy_list] ADD [cc_val] [varchar](7) NULL
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'policy_list' AND column_name = 'webplugin_val')
    ALTER TABLE [dbo].[policy_list] ADD [webplugin_val] [varchar](7) NULL
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'policy_list' AND column_name = 'video_val')
    ALTER TABLE [dbo].[policy_list] ADD [video_val] [varchar](7) NULL
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'policy_list' AND column_name = 'campaign_val')
    ALTER TABLE [dbo].[policy_list] ADD [campaign_val] [varchar](7) NULL
GO

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_policy_list]') AND type = 'K')
	ALTER TABLE [dbo].[policy_list] ADD CONSTRAINT [PK_policy_list] PRIMARY KEY CLUSTERED ([id] ASC)

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'policy_computer' )
	CREATE TABLE [policy_computer] (
		[id] [bigint] IDENTITY (1,1) NOT NULL, 
		[policy_id] [bigint], 
		[comp_id] [bigint], 
		[was_checked] [int],
		CONSTRAINT [PK_policy_computer] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY]
ELSE IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_policy_computer]') AND type = 'K')
	ALTER TABLE [dbo].[policy_computer] ADD CONSTRAINT [PK_policy_computer] PRIMARY KEY CLUSTERED ([id] ASC)	

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'policy_ip_group' )
	CREATE TABLE [policy_ip_group] (
		[id] [bigint] IDENTITY (1,1) NOT NULL, 
		[policy_id] [bigint], 
		[group_id] [bigint],
		[was_checked] [int],
		CONSTRAINT [PK_policy_ip_group] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY]
ELSE IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_policy_ip_group]') AND type = 'K')
	ALTER TABLE [dbo].[policy_ip_group] ADD CONSTRAINT [PK_policy_ip_group] PRIMARY KEY CLUSTERED ([id] ASC)	


IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'OU_Group' )
	CREATE TABLE [OU_Group] (
		[PId] [BIGINT] IDENTITY (1,1) NOT NULL,
		[Id_group] [BIGINT] NOT NULL,
		[Id_OU] [BIGINT] NOT NULL,
		CONSTRAINT [PK_OU_Group] PRIMARY KEY CLUSTERED ([PId] ASC)
	) ON [PRIMARY]
IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE column_name = 'PId' AND table_name = 'OU_Group' )
	ALTER TABLE [dbo].[OU_Group] ADD [PId] [BIGINT] IDENTITY (1,1) NOT NULL
GO
IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_OU_Group]') AND type = 'K')
	ALTER TABLE [dbo].[OU_Group] ADD CONSTRAINT [PK_OU_Group] PRIMARY KEY CLUSTERED ([PId] ASC)

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'groups_groups' )
	CREATE TABLE [groups_groups] (
		[id] [bigint] IDENTITY (1,1) NOT NULL, 
		[container_group_id] [BIGINT] NOT NULL, 
		[group_id] [BIGINT] NOT NULL, 
		[was_checked] [int],
		CONSTRAINT [PK_groups_groups] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY]
ELSE IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_groups_groups]') AND type = 'K')
	ALTER TABLE [dbo].[groups_groups] ADD CONSTRAINT [PK_groups_groups] PRIMARY KEY CLUSTERED ([id] ASC)	
GO

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'Group_synch' )
	CREATE TABLE [Group_synch] (
		[SId] [BIGINT] IDENTITY (1,1) NOT NULL,
		[Id_group] [BIGINT] NOT NULL,
		[UID] [VARCHAR](128),
		CONSTRAINT [PK_Group_synch] PRIMARY KEY CLUSTERED ([Id_group] ASC)
	) ON [PRIMARY]
ELSE IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE column_name = 'SId' AND table_name = 'Group_synch' )
	ALTER TABLE [dbo].[Group_synch] ADD [SId] [BIGINT] IDENTITY (1,1) NOT NULL
GO
IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_Group_synch]') AND type = 'K')
	ALTER TABLE [dbo].[Group_synch] ADD CONSTRAINT [PK_Group_synch] PRIMARY KEY CLUSTERED ([SId] ASC)
GO

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'archive_settings' )
BEGIN
	CREATE TABLE [archive_settings] (
		[id] [bigint] IDENTITY (1,1) NOT NULL, 
		[move_type] [varchar] (1), 
		[move_number] [int], 
		[move_date_type] [varchar] (1), 
		[clear_type] [varchar] (1), 
		[clear_number] [int], 
		[clear_date_type] [varchar] (1),
		CONSTRAINT [PK_archive_settings] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY]
	INSERT INTO [archive_settings] (move_type, move_number, move_date_type, clear_type, clear_number, clear_date_type) VALUES ('N', 1, 'Y', 'N', 1, 'Y');
END
ELSE IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_archive_settings]') AND type = 'K')
	ALTER TABLE [dbo].[archive_settings] ADD CONSTRAINT [PK_archive_settings] PRIMARY KEY CLUSTERED ([id] ASC)	


IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'archive_alerts' )
	CREATE TABLE [archive_alerts] (
		[id] [BIGINT] NOT NULL, 
		[alert_text] [varchar] (max), 
		[title] [varchar] (255), 
		[type] [varchar] (1), 
		[group_type] [varchar] (1), 
		[create_date] [datetime], 
		[sent_date] [datetime], 
		[type2] [varchar] (1), 
		[from_date] [datetime], 
		[to_date] [datetime], 
		[schedule] [varchar] (1), 
		[schedule_type] [varchar] (1), 
		[recurrence] [varchar] (1), 
		[urgent] [varchar] (1), 
		[toolbarmode] [varchar] (1), 
		[deskalertsmode] [varchar] (1), 
		[sender_id] [int],
		[template_id] [bigint], 
		[group_id] [bigint], 
		[aknown] [varchar] (1), 
		[ticker] [varchar] (1), 
		[ticker_speed] [varchar] (1),
		[anonymous_survey] [varchar] (1),
		[fullscreen] [varchar] (1), 
		[alert_width] [int], 
		alert_height [int], 
		[email_sender] [varchar] (255), 
		[email] [varchar] (1), 
		[sms] [varchar] (1), 
		[desktop] [varchar] (1), 
		[escalate] varchar (1), 
		[escalate_hours] [int], 
		[escalate_count] [int], 
		[escalate_step] [int], 
		[escalate_hours_initial] [int], 
		[autoclose] [bigint],
		[param_temp_id] [uniqueidentifier] NULL,
		[caption_id] [uniqueidentifier] NULL,
		[class] [bigint] NOT NULL DEFAULT 1,
		[parent_id] [bigint] NULL,
		[resizable] [bit] NULL,
		[post_to_blog] [bit] NULL,
		[social_media] [bit] NULL,
		[self_deletable] [bit] NULL,
		[text_to_call] [varchar] (1),
		[campaign_id] [bigint] NOT NULL DEFAULT -1
		CONSTRAINT [PK_archive_alerts] PRIMARY KEY CLUSTERED ([id] ASC)

		
	) ON [PRIMARY]
ELSE IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_archive_alerts]') AND type = 'K')
	ALTER TABLE [dbo].[archive_alerts] ALTER COLUMN [id] [BIGINT] NOT NULL
GO
IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_archive_alerts]') AND type = 'K')
	ALTER TABLE [dbo].[archive_alerts] ADD CONSTRAINT [PK_archive_alerts] PRIMARY KEY CLUSTERED ([id] ASC)	
	
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE column_name = 'video' AND table_name = 'archive_alerts')
	ALTER TABLE [dbo].[archive_alerts] ADD [video] [bit] NOT NULL DEFAULT 0
	
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE column_name = 'campaign_id' AND table_name = 'archive_alerts')
	ALTER TABLE [dbo].[archive_alerts] ADD [campaign_id] [bigint] NOT NULL DEFAULT -1	
	
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE column_name = 'approve_status' AND table_name = 'archive_alerts')
	ALTER TABLE [dbo].[archive_alerts] ADD [approve_status] int NULL
	
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE column_name = 'reject_reason' AND table_name = 'archive_alerts')
	ALTER TABLE [dbo].[archive_alerts] ADD [reject_reason] nvarchar(1000) NULL
	
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE column_name = 'lifetime' AND table_name = 'archive_alerts')
	ALTER TABLE [dbo].[archive_alerts] ADD [lifetime] int NULL	
	
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE column_name = 'terminated' AND table_name = 'archive_alerts')
	ALTER TABLE [dbo].[archive_alerts] ADD [terminated] int NULL
	
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE column_name = 'ticker_speed' AND table_name = 'archive_alerts')
	ALTER TABLE [dbo].[archive_alerts] ADD [ticker_speed] int NULL
	
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE column_name = 'anonymous_survey' AND table_name = 'archive_alerts') 
	ALTER TABLE [dbo].[archive_alerts] ADD [anonymous_survey] int NULL

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'archive_alerts_sent_group' )
	CREATE TABLE [archive_alerts_sent_group] (
		[id] [bigint] NOT NULL, 
		[alert_id] [bigint], 
		[group_id] [bigint],
		CONSTRAINT [PK_archive_alerts_sent_group] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY]
ELSE IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_archive_alerts_sent_group]') AND type = 'K')
	ALTER TABLE [dbo].[archive_alerts_sent_group] ALTER COLUMN [id] [bigint] NOT NULL	
GO
IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_archive_alerts_sent_group]') AND type = 'K')
	ALTER TABLE [dbo].[archive_alerts_sent_group] ADD CONSTRAINT [PK_archive_alerts_sent_group] PRIMARY KEY CLUSTERED ([id] ASC)	

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'archive_alerts_group_stat' )
	CREATE TABLE [archive_alerts_group_stat] (
		[id] [bigint] NOT NULL, 
		[alert_id] [bigint], 
		[group_id] [bigint], 
		[date] [datetime],
		CONSTRAINT [PK_archive_alerts_group_stat] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY]
ELSE IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_archive_alerts_group_stat]') AND type = 'K')
	ALTER TABLE [dbo].[archive_alerts_group_stat] ALTER COLUMN [id] [bigint] NOT NULL
GO
IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_archive_alerts_group_stat]') AND type = 'K')
	ALTER TABLE [dbo].[archive_alerts_group_stat] ADD CONSTRAINT [PK_archive_alerts_group_stat] PRIMARY KEY CLUSTERED ([id] ASC)		

GO
IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'archive_alerts_sent' )
	CREATE TABLE [archive_alerts_sent] (
		[id] [bigint] NOT NULL,		
		[alert_id] [bigint], 
		[user_id] [bigint],
		CONSTRAINT [PK_archive_alerts_sent] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY]
ELSE IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_archive_alerts_sent]') AND type = 'K')
	ALTER TABLE [dbo].[archive_alerts_sent] ALTER COLUMN [id] [bigint] NOT NULL
GO	
IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_archive_alerts_sent]') AND type = 'K')
	ALTER TABLE [dbo].[archive_alerts_sent] ADD CONSTRAINT [PK_archive_alerts_sent] PRIMARY KEY CLUSTERED ([id] ASC)

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'archive_alerts_sent_stat' )
	CREATE TABLE [archive_alerts_sent_stat] (
		[id] [bigint] NOT NULL, 
		[alert_id] [bigint], 
		[user_id] [bigint], 
		[date] [datetime],
		CONSTRAINT [PK_archive_alerts_sent_stat] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY]
ELSE IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_archive_alerts_sent_stat]') AND type = 'K')
	ALTER TABLE [dbo].[archive_alerts_sent_stat] ALTER COLUMN [id] [bigint] NOT NULL	
GO
IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_archive_alerts_sent_stat]') AND type = 'K')
	ALTER TABLE [dbo].[archive_alerts_sent_stat] ADD CONSTRAINT [PK_archive_alerts_sent_stat] PRIMARY KEY CLUSTERED ([id] ASC)	

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'archive_alerts_sent_comp' )
	CREATE TABLE [archive_alerts_sent_comp] (
		[id] [bigint] NOT NULL, 
		[alert_id] [bigint], 
		[comp_id] [bigint],
		CONSTRAINT [PK_archive_alerts_sent_comp] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY]
ELSE IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_archive_alerts_sent_comp]') AND type = 'K')
	ALTER TABLE [dbo].[archive_alerts_sent_comp] ALTER COLUMN [id] [bigint] NOT NULL
GO
IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_archive_alerts_sent_comp]') AND type = 'K')
	ALTER TABLE [dbo].[archive_alerts_sent_comp] ADD CONSTRAINT [PK_archive_alerts_sent_comp] PRIMARY KEY CLUSTERED ([id] ASC)	


IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'archive_alerts_comp_stat' )
	CREATE TABLE [archive_alerts_comp_stat] (
		[id] [bigint] NOT NULL, 
		[alert_id] [bigint], 
		[comp_id] [bigint], 
		[date] [datetime],
		CONSTRAINT [PK_archive_alerts_comp_stat] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY]
ELSE IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_archive_alerts_comp_stat]') AND type = 'K')
	ALTER TABLE [dbo].[archive_alerts_comp_stat] ALTER COLUMN [id] [bigint] NOT NULL
GO
IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_archive_alerts_comp_stat]') AND type = 'K')
	ALTER TABLE [dbo].[archive_alerts_comp_stat] ADD CONSTRAINT [PK_archive_alerts_comp_stat] PRIMARY KEY CLUSTERED ([id] ASC)	

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'archive_alerts_received' )
	CREATE TABLE [archive_alerts_received] (
		[id] [bigint] NOT NULL, 
		[alert_id] [bigint],
		[user_id] [bigint], 
		[clsid] [varchar] (255),
		[date] [datetime],
		CONSTRAINT [PK_archive_alerts_received] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY]
ELSE IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_archive_alerts_received]') AND type = 'K')
	ALTER TABLE [dbo].[archive_alerts_received] ALTER COLUMN [id] [bigint] NOT NULL
GO
IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_archive_alerts_received]') AND type = 'K')
	ALTER TABLE [dbo].[archive_alerts_received] ADD CONSTRAINT [PK_archive_alerts_received] PRIMARY KEY CLUSTERED ([id] ASC)	

GO

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='date'
		AND table_name = 'archive_alerts_received' )
BEGIN
	ALTER TABLE [archive_alerts_received] ADD [date] [datetime]
END

GO


IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'archive_alerts_read' )
	CREATE TABLE [archive_alerts_read] (
		[id] [bigint] NOT NULL, 
		[alert_id] [bigint], 
		[user_id] [bigint], 
		[closetime] [varchar] (55), 
		[read] [char] (10), 
		[date] [datetime],
		CONSTRAINT [PK_archive_alerts_read] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY]
ELSE IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_archive_alerts_read]') AND type = 'K')
	ALTER TABLE [dbo].[archive_alerts_read] ALTER COLUMN [id] [bigint] NOT NULL
GO
IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_archive_alerts_read]') AND type = 'K')
	ALTER TABLE [dbo].[archive_alerts_read] ADD CONSTRAINT [PK_archive_alerts_read] PRIMARY KEY CLUSTERED ([id] ASC)	


IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'ip_groups' )
	CREATE TABLE [ip_groups] (
		[id] [bigint] IDENTITY (1,1) NOT NULL, 
		[name] [varchar] (255),
		CONSTRAINT [PK_ip_groups] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY]
ELSE IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_ip_groups]') AND type = 'K')
	ALTER TABLE [dbo].[ip_groups] ADD CONSTRAINT [PK_ip_groups] PRIMARY KEY CLUSTERED ([id] ASC)	


IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'ip_range_groups' )
	CREATE TABLE [ip_range_groups] (
		[id] [bigint] IDENTITY (1,1) NOT NULL, 
		[group_id] [bigint], 
		[from_ip] [varchar] (255), 
		[to_ip] [varchar] (255),
		CONSTRAINT [PK_ip_range_groups] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY]
ELSE IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_ip_range_groups]') AND type = 'K')
	ALTER TABLE [dbo].[ip_range_groups] ADD CONSTRAINT [PK_ip_range_groups] PRIMARY KEY CLUSTERED ([id] ASC)	

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'campaigns' )
    CREATE TABLE [campaigns] (
        [id] [bigint] IDENTITY (1,1) NOT NULL, 
        [name] [nvarchar] (255),
        [create_date] [datetime] NULL,
        [start_date] [datetime] NULL,
        CONSTRAINT [PK_campaigns] PRIMARY KEY CLUSTERED ([id] ASC)
        ) ON [PRIMARY]
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE column_name = 'start_date' AND table_name = 'campaigns')
	ALTER TABLE [dbo].[campaigns] ADD [start_date] [datetime] NULL
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE column_name = 'end_date' AND table_name = 'campaigns')
	ALTER TABLE [dbo].[campaigns] ADD [end_date] [datetime] NULL
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE column_name = 'is_active' AND table_name = 'campaigns')
	ALTER TABLE [dbo].[campaigns] ADD [is_active] [bit] NULL
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE column_name = 'sender_id' AND table_name = 'campaigns')
	ALTER TABLE [dbo].[campaigns] ADD [sender_id] [bigint] NULL
	
ALTER TABLE [campaigns] ALTER COLUMN [name] nvarchar (255) 	     
GO

ALTER TABLE campaigns ALTER COLUMN name nvarchar(255) NULL

SET LANGUAGE British

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE table_name = 'settings' )
BEGIN
	CREATE TABLE [settings](
		[id] [bigint] IDENTITY (1,1) NOT NULL,
		[name] [varchar] (255),
		[val] [nvarchar] (MAX),
		[s_type] [varchar] (1),
		[s_part] [varchar] (1),
		CONSTRAINT [PK_settings] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY]
	--default
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfDesktopAlertByDefault', '1', 'C', 'D');
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfDesktopAlertType', 'P', 'R', 'D');
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfEmailByDefault', '0', 'C', 'D');
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfSMSByDefault', '0', 'C', 'D');
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfPopupType', 'W', 'R', 'D');
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfEnableSmtpLogging', '0', 'C', 'D');
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfAlertWidth', '500', 'I', 'D');
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfAlertHeight', '400', 'I', 'D');
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfUrgentByDefault', '0', 'C', 'D');
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfAcknowledgementByDefault', '0', 'C', 'D');
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfAutocloseByDefault', '0', 'C', 'D');
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfAutocloseValue', '1', 'I', 'D');
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfAllowManualCloseByDefault', '0', 'C', 'D');
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfScheduleByDefault', '0', 'C', 'D');
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfMessageExpire', '1440', 'I', 'D');
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfRSSMessageExpire', '1440', 'I', 'D');
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfAlertPosition', '6', 'R', 'D');
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfPostToBlogByDefault', '0', 'C', 'D');
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfSocialMediaByDefault', '0', 'C', 'D');
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfUnobtrusiveByDefault', '0', 'C', 'D');
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfTickerPosition', '9', 'R', 'D');
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfSelfDeletableByDefault', '0', 'C', 'D');
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfTextCallByDefault', '0', 'C', 'D');
	--system
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfAllowClientDeleteTerminatedAlerts', '0', 'C', 'S');
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfEnableMultiAlert', '0', 'C', 'S');
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfEnableMultiVote', '0', 'C', 'S');
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfDefaultEmail', '', 'I', 'S');
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfDefaultEmailAddress', '', 'I', 'S');
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfEnableADEDCheck', '1', 'C', 'S');
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfEnableRegAndADEDMode', '1', 'C', 'S');
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfAPISecret', '%api_secret_string%', 'I', 'S');
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfEnableHtmlAlertTitle', '1', 'C', 'S');
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfEnablePostponeInPreview', '1', 'R', 'S');
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfEnableHelp', '0', 'C', 'S');
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfEnableADSyncLogDetails', '0', 'C', 'S');
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfShowSkinsDialog', '0', 'C', 'S');
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfShowTypesDialog', '0', 'C', 'S');
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfShowNewsBanner', '1', 'C', 'S');
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfAllowApprove', '0', 'C', 'S');
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfForceSelfRegWithAD', '1', 'C', 'S');
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('SimpleGroupSending', '0', 'C', 'S');
	--ed
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfEDMobile', 'mobile', 'I', 'E');
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfEDMail', 'mail', 'I', 'E');
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfEDMember', 'member', 'I', 'E');
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfEDUser', 'user', 'I', 'E');
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfEDGroup', 'group', 'I', 'E');
	--task_scheduler
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfRSSReaderTaskTimeStamp', CONVERT(nvarchar, GETUTCDATE(), 9), 'I', 'T');
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfArchiveTaskTimeStamp', CONVERT(nvarchar, GETUTCDATE(), 9), 'I', 'T');
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfSendReccurenceTaskTimeStamp', CONVERT(nvarchar, GETUTCDATE(), 9), 'I', 'T');
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfSyncTaskTimeStamp', CONVERT(nvarchar, GETUTCDATE(), 9), 'I', 'T');
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfSessionTimeout', '480', 'I', 'S');
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfSelfDeletableByDefault', '0', 'C', 'D');
	--trial add-ons
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfAddonsTrialDate', '', 'I', 'T');
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfAddonsTrialList', '', 'I', 'T');
	--azure-ad
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfAzureAdTenant', '', 'I', 'T');
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfAzureAdPollingAppId', '', 'I', 'T');
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfAzureAdPollingAppSecret', '', 'I', 'T');
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfAzureAdAuthorizationAppId', '', 'I', 'T');
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfAzureAdAutoSynchronization', '', 'I', 'T');
	--date-time-formats
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfDateFormat', 'dd/MM/yyyy', 'I', 'S');
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfTimeFormat', 'HH:mm:ss', 'I', 'S');
END
ELSE
BEGIN
	UPDATE [settings] SET [s_part] = 'D' WHERE name = 'ConfMessageExpire' AND [s_part] = 'S'	
	IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE column_name = 'id' AND table_name = 'settings' )
		ALTER TABLE [settings] ADD [id] [bigint] IDENTITY (1,1) NOT NULL
END
GO
IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_settings]') AND type = 'K')
	ALTER TABLE [dbo].[settings] ADD CONSTRAINT [PK_settings] PRIMARY KEY CLUSTERED ([id] ASC)

IF NOT EXISTS ( SELECT 1 FROM settings WHERE name = 'ConfEDMobile' )
BEGIN
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfEDMobile', 'mobile', 'I', 'E');
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfEDMail', 'mail', 'I', 'E');
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfEDMember', 'member', 'I', 'E');
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfEDUser', 'user', 'I', 'E');
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfEDGroup', 'group', 'I', 'E');
END

IF NOT EXISTS ( SELECT 1 FROM settings WHERE name = 'ConfDefaultLanguage' )
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfDefaultLanguage', 'EN', 'I', 'S');
	
IF NOT EXISTS ( SELECT 1 FROM settings WHERE name = 'SimpleGroupSending' )
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('SimpleGroupSending', '0', 'C', 'S');
	
IF NOT EXISTS ( SELECT 1 FROM settings WHERE name = 'ConfRSSReaderTaskTimeStamp' )
BEGIN
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfRSSReaderTaskTimeStamp', CONVERT(nvarchar, GETUTCDATE(), 9), 'I', 'T');
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfArchiveTaskTimeStamp', CONVERT(nvarchar, GETUTCDATE(), 9), 'I', 'T');
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfSendReccurenceTaskTimeStamp', CONVERT(nvarchar, GETUTCDATE(), 9), 'I', 'T');
END

IF NOT EXISTS ( SELECT 1 FROM settings WHERE name = 'ConfSyncTaskTimeStamp' )
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfSyncTaskTimeStamp', CONVERT(nvarchar, GETUTCDATE(), 9), 'I', 'T');

IF NOT EXISTS ( SELECT 1 FROM settings WHERE name = 'ConfRSSMessageExpire' )
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfRSSMessageExpire', '1440', 'I', 'D');
	
IF NOT EXISTS ( SELECT 1 FROM settings WHERE name = 'ConfDateFormat' )
BEGIN
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfDateFormat', 'dd/MM/yyyy', 'I', 'S');
END 

IF NOT EXISTS ( SELECT 1 FROM settings WHERE name = 'ConfTimeFormat' )
BEGIN
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfTimeFormat', 'HH:mm:ss', 'I', 'S');
END 

IF EXISTS ( SELECT 1 FROM settings WHERE name = 'ConfTimeFormat' AND (val = '1' OR val = '2') )
BEGIN
	UPDATE [settings] SET val = 'HH:mm:ss', s_type = 'I', s_part = 'S' WHERE [name] = 'ConfTimeFormat'
END


IF NOT EXISTS ( SELECT 1 FROM settings WHERE name = 'ConfFirstDayOfWeek' )
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfFirstDayOfWeek', '0', 'I', 'S');

IF NOT EXISTS ( SELECT 1 FROM settings WHERE name = 'ConfSessionTimeout' )
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfSessionTimeout', '480', 'I', 'S');

IF NOT EXISTS ( SELECT 1 FROM settings WHERE name = 'ConfEnableHtmlAlertTitle' )
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfEnableHtmlAlertTitle', '1', 'C', 'S');

IF NOT EXISTS ( SELECT 1 FROM settings WHERE name = 'ConfEnablePostponeInPreview' )
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfEnablePostponeInPreview', '1', 'R', 'S');

IF NOT EXISTS ( SELECT 1 FROM settings WHERE name = 'ConfEnableHelp' )
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfEnableHelp', '0', 'C', 'S');

IF NOT EXISTS ( SELECT 1 FROM settings WHERE name = 'ConfEnableADSyncLogDetails' )
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfEnableADSyncLogDetails', '0', 'C', 'S');
	
IF NOT EXISTS ( SELECT 1 FROM settings WHERE name = 'ConfOldSurvey' )
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfOldSurvey', '0', 'C', 'S');
	
IF NOT EXISTS ( SELECT 1 FROM settings WHERE name = 'ConfShowSkinsDialog' )
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfShowSkinsDialog', '0', 'C', 'S');
	
IF NOT EXISTS ( SELECT 1 FROM settings WHERE name = 'ConfShowTypesDialog' )
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfShowTypesDialog', '0', 'C', 'S');

IF NOT EXISTS ( SELECT 1 FROM settings WHERE name = 'ConfShowNewsBanner' )
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfShowNewsBanner', '1', 'C', 'S');

IF NOT EXISTS ( SELECT 1 FROM settings WHERE name = 'ConfAlertPosition' )
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfAlertPosition', '6', 'R', 'D');

IF NOT EXISTS ( SELECT 1 FROM settings WHERE name = 'ConfPostToBlogByDefault' )
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfPostToBlogByDefault', '0', 'C', 'D');
	
IF NOT EXISTS ( SELECT 1 FROM settings WHERE name = 'ConfEnableSmtpLogging' )
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfEnableSmtpLogging', '0', 'C', 'D');
	
IF NOT EXISTS ( SELECT 1 FROM settings WHERE name = 'ConfSocialMediaByDefault' )
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfSocialMediaByDefault', '0', 'C', 'D');
	
IF NOT EXISTS ( SELECT 1 FROM settings WHERE name = 'ConfUnobtrusiveByDefault' )
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfUnobtrusiveByDefault', '0', 'C', 'D');
	
IF NOT EXISTS ( SELECT 1 FROM settings WHERE name = 'ConfTickerPosition' )
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfTickerPosition', '9', 'R', 'D');
	
IF NOT EXISTS ( SELECT 1 FROM settings WHERE name = 'ConfSelfDeletableByDefault' )	
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfSelfDeletableByDefault', '0', 'C', 'D');
	
IF NOT EXISTS ( SELECT 1 FROM settings WHERE name = 'ConfAllowApprove' )
    INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfAllowApprove', '0', 'C', 'S');

IF NOT EXISTS ( SELECT 1 FROM settings WHERE name = 'ConfForceSelfRegWithAD' )
    INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfForceSelfRegWithAD', '1', 'C', 'S');
	
	
IF NOT EXISTS ( SELECT 1 FROM settings WHERE name = 'ConfAddonsTrialDate' )
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfAddonsTrialDate', '', 'I', 'T');
	
IF NOT EXISTS ( SELECT 1 FROM settings WHERE name = 'ConfAddonsTrialList' )
	INSERT INTO [settings] (name, val, s_type, s_part) VALUES ('ConfAddonsTrialList', '', 'I', 'T');
ELSE
	UPDATE [settings] SET val='' WHERE name='ConfAddonsTrialList'
--policy altering

IF NOT (SELECT CHARACTER_MAXIMUM_LENGTH FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='alerts_val'
		AND table_name = 'policy_list') != 7
BEGIN
	ALTER TABLE policy_list
		ALTER COLUMN [alerts_val] [varchar] (7) 
	ALTER TABLE policy_list
		ALTER COLUMN [emails_val] [varchar] (7)
	ALTER TABLE policy_list
		ALTER COLUMN [sms_val] [varchar] (7)
	ALTER TABLE policy_list
		ALTER COLUMN [surveys_val] [varchar] (7)
	ALTER TABLE policy_list
		ALTER COLUMN [users_val] [varchar](7)
	ALTER TABLE policy_list
		ALTER COLUMN [groups_val] [varchar] (7)
	ALTER TABLE policy_list
		ALTER COLUMN [templates_val] [varchar] (7)
	ALTER TABLE policy_list
		ALTER COLUMN [statistics_val] [varchar] (7)
	ALTER TABLE policy_list
		ALTER COLUMN [text_to_call_val] [varchar] (7)	
END

-- password encryption 3.1.6
IF EXISTS (SELECT 1 FROM version WHERE version ='3.1.6')
	UPDATE users SET pass=SUBSTRING(master.dbo.fn_varbintohexstr(HashBytes('MD5', pass)), 3, 32)

-- password encryption 2.1.0.40
IF EXISTS (SELECT 1 FROM version WHERE version ='2.1.0.40')
	UPDATE users SET pass=SUBSTRING(master.dbo.fn_varbintohexstr(HashBytes('MD5', pass)), 3, 32)

-- aknown(3.1.8)
IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='aknown'
		AND table_name = 'alerts' )
BEGIN
	ALTER TABLE [alerts] ADD [aknown] [varchar] (1)
END


-- edir(3.2.0.1)
IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='context_id'
		AND table_name = 'groups' )
BEGIN
	ALTER TABLE [groups] ADD [context_id] [int]
END

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='context_id'
		AND table_name = 'users' )
BEGIN
	ALTER TABLE [users] ADD [context_id] [int]
END


-- email(3.3.0.1)
IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='email'
		AND table_name = 'users' )
BEGIN
	ALTER TABLE [users] ADD [email] [varchar] (255)
END

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='client_version'
		AND table_name = 'users' )
BEGIN
	ALTER TABLE [users] ADD [client_version] [varchar] (50)
END

-- start page (6.1.5)

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='start_page'
		AND table_name = 'users' )
BEGIN
	ALTER TABLE [users] ADD [start_page] [varchar] (255)
END

--contract_date (6.1.6)

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='contract_date'
		AND table_name = 'version' )
BEGIN
	ALTER TABLE [version] ADD [contract_date] [datetime]
END

-- guide setting (7.0.12)

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='guide'
		AND table_name = 'users' )
BEGIN
	ALTER TABLE [users] ADD [guide] [varchar] (5)
END

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='dis_guide'
		AND table_name = 'users' )
BEGIN
	ALTER TABLE [users] ADD [dis_guide] [varchar] (100)
END

--send logs of errors to admin email

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='send_logs'
		AND table_name = 'users' )
BEGIN
	ALTER TABLE [users] ADD [send_logs] [int]
END

GO

-- multiple users
IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='clsid'
		AND table_name = 'alerts_received' )
BEGIN
	ALTER TABLE [alerts_received] ADD [clsid] [varchar] (255) DEFAULT('') WITH VALUES
END

-- color codes(7.0.0)


IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='color_code'
		AND table_name = 'archive_alerts' )
BEGIN
	ALTER TABLE [archive_alerts] ADD [color_code] [uniqueidentifier]
END

GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='color_code'
		AND table_name = 'alerts' 
		AND data_type = 'varchar' )
BEGIN
	UPDATE [alerts] SET color_code=NULL
	ALTER TABLE [alerts] ALTER COLUMN [color_code] [uniqueidentifier]
END

GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='color_code'
		AND table_name = 'archive_alerts' 
		AND data_type = 'varchar' )
BEGIN
	UPDATE [archive_alerts] SET color_code=NULL
	ALTER TABLE [archive_alerts] ALTER COLUMN [color_code] [uniqueidentifier]
END

GO
-- widgets order(7.0.0)
IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='priority'
		AND table_name = 'widgets' )
BEGIN
	ALTER TABLE [widgets] ADD [priority] [int]
END

GO

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='vote'
		AND table_name = 'alerts_received' )
BEGIN
	ALTER TABLE [alerts_received] ADD [vote] [int]
END

GO

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='vote'
		AND table_name = 'archive_alerts_received' )
BEGIN
	ALTER TABLE [archive_alerts_received] ADD [vote] [int]
END

-- multiple surveys
IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='question_id'
		AND table_name = 'surveys_answers' )
BEGIN
	ALTER TABLE [surveys_answers] ADD [question_id] [bigint]
END

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='clsid'
		AND table_name = 'surveys_answers_stat' )
BEGIN
	ALTER TABLE [surveys_answers_stat] ADD [clsid] [varchar] (255) DEFAULT('') WITH VALUES
END

-- recurrence

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='recurrence'
		AND table_name = 'alerts' )
BEGIN
	ALTER TABLE [alerts] ADD [recurrence] [varchar] (1)
END

-- schedule for survey
IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='from_date'
		AND table_name = 'surveys_main' )
BEGIN
	ALTER TABLE [surveys_main] ADD [from_date] [datetime]
END

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='to_date'
		AND table_name = 'surveys_main' )
BEGIN
	ALTER TABLE [surveys_main] ADD [to_date] [datetime]
END

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='schedule'
		AND table_name = 'surveys_main' )
BEGIN
	ALTER TABLE [surveys_main] ADD [schedule] [varchar] (1)
END

-- policy
IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='only_personal'
		AND table_name = 'users' )
BEGIN
	ALTER TABLE [users] ADD [only_personal] [int]
END

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='only_templates'
		AND table_name = 'users' )
BEGIN
	ALTER TABLE [users] ADD [only_templates] [int]
END

-- ticker (4.0.0.2)
IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='ticker'
		AND table_name = 'alerts' )
BEGIN
	ALTER TABLE [alerts] ADD [ticker] [varchar] (1)
END

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='ticker_speed'
		AND table_name = 'alerts' )
BEGIN
	ALTER TABLE [alerts] ADD [ticker_speed] [varchar] (1)
END

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS 
    WHERE column_name ='anonymous_survey' 
    AND table_name = 'alerts' ) 
BEGIN 
	ALTER TABLE [alerts] ADD [anonymous_survey] [varchar] (1) 
END

-- title (4.0.0.7)
IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='title'
		AND table_name = 'alerts' )
BEGIN
	ALTER TABLE [alerts] ADD [title] [varchar] (255)
END

-- templates_preview (4.0.0.13)

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='group_type'
		AND table_name = 'alerts' )
BEGIN
	ALTER TABLE [alerts] ADD [group_type] [varchar] (1)
END

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='group_type'
		AND table_name = 'surveys_main' )
BEGIN
	ALTER TABLE [surveys_main] ADD [group_type] [varchar] (1)
END

-- only alerts 4.0.2.1
IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='only_alerts'
		AND table_name = 'users' )
BEGIN
	ALTER TABLE [users] ADD [only_alerts] [int]
END

-- alerts save info 4.0.2.4
IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='email_sender'
		AND table_name = 'alerts' )
BEGIN
	ALTER TABLE [alerts] ADD [email_sender] [varchar] (250)
END
IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='email'
		AND table_name = 'alerts' )
BEGIN
	ALTER TABLE [alerts] ADD [email] [varchar] (1)
END
IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='sms'
		AND table_name = 'alerts' )
BEGIN
	ALTER TABLE [alerts] ADD [sms] [varchar] (1)
END
IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='desktop'
		AND table_name = 'alerts' )
BEGIN
	ALTER TABLE [alerts] ADD [desktop] [varchar] (1)
END

IF NOT EXISTS ( SELECT 1 FROM domains WHERE name = '' )
BEGIN
	INSERT INTO domains (name) VALUES ('')
END

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='desktop'
		AND table_name = 'alerts' )
BEGIN
	ALTER TABLE [alerts] ADD [desktop] [varchar] (1)
END

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='all_ous'
		AND table_name = 'domains' )
BEGIN
	ALTER TABLE [domains] ADD [all_ous] [int]
END


IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='schedule_type'
		AND table_name = 'alerts' )
BEGIN
	ALTER TABLE [alerts] ADD [schedule_type] [varchar] (1)
END

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='fullscreen'
		AND table_name = 'alerts' )
BEGIN
	ALTER TABLE [alerts] ADD [fullscreen] [varchar] (1)
END

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='alert_width'
		AND table_name = 'alerts' )
BEGIN
	ALTER TABLE [alerts] ADD [alert_width] [int]
END

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='alert_height'
		AND table_name = 'alerts' )
BEGIN
	ALTER TABLE [alerts] ADD [alert_height] [int]
END

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='type'
		AND table_name = 'policy' )
BEGIN
	ALTER TABLE [policy] ADD [type] [varchar] (1)
END

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='escalate'
		AND table_name = 'alerts' )
BEGIN
	ALTER TABLE [alerts] ADD [escalate] varchar (1)
END

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='escalate_hours'
		AND table_name = 'alerts' )
BEGIN
	ALTER TABLE [alerts] ADD [escalate_hours] [int]
END
IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='escalate_count'
		AND table_name = 'alerts' )
BEGIN
	ALTER TABLE [alerts] ADD [escalate_count] [int]
END
IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='escalate_step'
		AND table_name = 'alerts' )
BEGIN
	ALTER TABLE [alerts] ADD [escalate_step] [int]
END
IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='escalate_hours_initial'
		AND table_name = 'alerts' )
BEGIN
	ALTER TABLE [alerts] ADD [escalate_hours_initial] [int]
END
IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='question_type'
		AND table_name = 'surveys_questions' )
BEGIN
	ALTER TABLE [surveys_questions] ADD [question_type] [varchar] (1)
END

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='root_group'
		AND table_name = 'groups' )
BEGIN
	ALTER TABLE [groups] ADD [root_group] [int]
END

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='autoclose'
		AND table_name = 'alerts' )
BEGIN
	ALTER TABLE [alerts] ADD [autoclose] [bigint]
END

GO

IF EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name IN (N'to_users', N'to_groups', N'to_comps')
		AND table_name = 'alerts' )
BEGIN
	DECLARE @Command  nvarchar(MAX)
	SET @Command = N'USE [%dbname_both%]'
	SELECT @Command = @Command + N';ALTER TABLE [alerts] DROP CONSTRAINT [' + d.name + ']'
	FROM sys.tables t
	JOIN sys.default_constraints d
	ON d.parent_object_id = t.object_id
	JOIN sys.columns c
	ON c.object_id = t.object_id
	AND c.column_id = d.parent_column_id
	WHERE t.name = N'alerts'
	AND c.name IN (N'to_users', N'to_groups', N'to_comps')
	EXECUTE(@Command)
END

GO

IF EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='to_users'
		AND table_name = 'alerts' )
BEGIN
	IF EXISTS (SELECT name FROM sys.indexes WHERE name = N'IX_alerts_recur')
		DROP INDEX IX_alerts_recur ON [dbo].[alerts]
END

GO

IF EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='to_users'
		AND table_name = 'alerts' )
BEGIN
	ALTER TABLE [alerts] DROP COLUMN [to_users]
END

GO

IF EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='to_groups'
		AND table_name = 'alerts' )
BEGIN
	ALTER TABLE [alerts] DROP COLUMN [to_groups]
END

GO

IF EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='to_comps'
		AND table_name = 'alerts' )
BEGIN
	ALTER TABLE [alerts] DROP COLUMN [to_comps]
END

GO

IF EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name IN (N'to_users', N'to_groups', N'to_comps')
		AND table_name = 'archive_alerts' )
BEGIN
	DECLARE @Command  nvarchar(MAX)
	SET @Command = N'USE [%dbname_both%]'
	SELECT @Command = @Command + N';ALTER TABLE [archive_alerts] DROP CONSTRAINT [' + d.name + ']'
	FROM sys.tables t
	JOIN sys.default_constraints d
	ON d.parent_object_id = t.object_id
	JOIN sys.columns c
	ON c.object_id = t.object_id
	AND c.column_id = d.parent_column_id
	WHERE t.name = N'archive_alerts'
	AND c.name IN (N'to_users', N'to_groups', N'to_comps')
	EXECUTE(@Command)
END

GO

IF EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='to_users'
		AND table_name = 'archive_alerts' )
BEGIN
	ALTER TABLE [archive_alerts] DROP COLUMN [to_users]
END

GO

IF EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='to_groups'
		AND table_name = 'archive_alerts' )
BEGIN
	ALTER TABLE [archive_alerts] DROP COLUMN [to_groups]
END

GO

IF EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='to_comps'
		AND table_name = 'archive_alerts' )
BEGIN
	ALTER TABLE [archive_alerts] DROP COLUMN [to_comps]
END

GO

IF EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name IN (N'to_users', N'to_groups', N'to_comps')
		AND table_name = 'surveys_main' )
BEGIN
	DECLARE @Command  nvarchar(MAX)
	SET @Command = N'USE [%dbname_both%]'
	SELECT @Command = @Command + N';ALTER TABLE [surveys_main] DROP CONSTRAINT [' + d.name + ']'
	FROM sys.tables t
	JOIN sys.default_constraints d
	ON d.parent_object_id = t.object_id
	JOIN sys.columns c
	ON c.object_id = t.object_id
	AND c.column_id = d.parent_column_id
	WHERE t.name = N'surveys_main'
	AND c.name IN (N'to_users', N'to_groups', N'to_comps')
	EXECUTE(@Command)
END

GO

IF EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='to_users'
		AND table_name = 'surveys_main' )
BEGIN	
	ALTER TABLE [surveys_main] DROP COLUMN [to_users]
END

GO

IF EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='to_groups'
		AND table_name = 'surveys_main' )
BEGIN
	ALTER TABLE [surveys_main] DROP COLUMN [to_groups]
END

GO

IF EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='to_comps'
		AND table_name = 'surveys_main' )
BEGIN
	ALTER TABLE [surveys_main] DROP COLUMN [to_comps]
END

GO

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='stype'
		AND table_name = 'surveys_main' )
BEGIN
	ALTER TABLE [surveys_main] ADD [stype] [SMALLINT] NOT NULL DEFAULT 1
END

GO

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS  
    WHERE column_name ='anonymous_survey'  
    AND table_name = 'surveys_main' )  
BEGIN  
  ALTER TABLE [surveys_main] ADD [anonymous_survey] [varchar] (1)  
END 
 
GO
IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='next_request'
		AND table_name = 'users' )
BEGIN
	ALTER TABLE [users] ADD [next_request] [varchar] (255)
END

GO

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='last_standby_request'
		AND table_name = 'users' )
BEGIN
	ALTER TABLE [users] ADD [last_standby_request] [varchar] (255)
END

GO

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='next_standby_request'
		AND table_name = 'users' )
BEGIN
	ALTER TABLE [users] ADD [next_standby_request] [varchar] (255)
END

GO

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='next_request'
		AND table_name = 'computers' )
BEGIN
	ALTER TABLE [computers] ADD [next_request] [varchar] (255)
END

GO

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='ip_groups_val'
		AND table_name = 'policy_list' )
BEGIN
	ALTER TABLE [policy_list] ADD [ip_groups_val] [varchar] (7)
END

GO

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='rss_val'
		AND table_name = 'policy_list' )
BEGIN
	ALTER TABLE [policy_list] ADD [rss_val] [varchar] (7)
END

GO

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='screensavers_val'
		AND table_name = 'policy_list' )
BEGIN
	ALTER TABLE [policy_list] ADD [screensavers_val] [varchar] (7)
END

GO

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='text_to_call_val'
		AND table_name = 'policy_list' )
BEGIN
	ALTER TABLE [policy_list] ADD [text_to_call_val] [varchar] (7)
END

GO

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='wallpapers_val'
		AND table_name = 'policy_list' )
BEGIN
	ALTER TABLE [policy_list] ADD [wallpapers_val] [varchar] (7)
END

GO

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='lockscreens_val'
		AND table_name = 'policy_list' )
BEGIN
	ALTER TABLE [policy_list] ADD [lockscreens_val] [varchar] (7)
END

GO

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='text_templates_val'
		AND table_name = 'policy_list' )
BEGIN
	ALTER TABLE [policy_list] ADD [text_templates_val] [varchar] (7)
END

GO

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='im_val'
		AND table_name = 'policy_list' )
BEGIN
	ALTER TABLE [policy_list] ADD [im_val] [varchar] (7)
END

GO

IF NOT (SELECT CHARACTER_MAXIMUM_LENGTH FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='alerts_val'
		AND table_name = 'policy_list') != 7
BEGIN
	ALTER TABLE policy_list
		ALTER COLUMN [alerts_val] [varchar] (7) 
	ALTER TABLE policy_list
		ALTER COLUMN [emails_val] [varchar] (7)
	ALTER TABLE policy_list
		ALTER COLUMN [sms_val] [varchar] (7)
	ALTER TABLE policy_list
		ALTER COLUMN [surveys_val] [varchar] (7)
	ALTER TABLE policy_list
		ALTER COLUMN [users_val] [varchar](7)
	ALTER TABLE policy_list
		ALTER COLUMN [groups_val] [varchar] (7)
	ALTER TABLE policy_list
		ALTER COLUMN [templates_val] [varchar] (7)
	ALTER TABLE policy_list
		ALTER COLUMN [statistics_val] [varchar] (7)
	ALTER TABLE policy_list
		ALTER COLUMN [text_to_call_val] [varchar] (7)	
END

GO

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'languages_list' )
	CREATE TABLE [languages_list] (
		[id] [bigint] IDENTITY (1,1) NOT NULL,
		[name] [varchar] (255) NOT NULL, 
		[code] [varchar] (10)
		CONSTRAINT [PK_languages_list] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY]
ELSE IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE column_name = 'id' AND table_name = 'languages_list' )
	ALTER TABLE [dbo].[languages_list] ADD [id] [bigint] IDENTITY (1,1) NOT NULL
GO
IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_languages_list]') AND type = 'K')
	ALTER TABLE [dbo].[languages_list] ADD CONSTRAINT [PK_languages_list] PRIMARY KEY CLUSTERED ([id] ASC)
GO

IF NOT EXISTS ( SELECT 1 FROM [languages_list] WHERE name = N'English' )
BEGIN
    INSERT [dbo].[languages_list] ([name], [code]) VALUES (N'English', N'EN')
END

IF NOT EXISTS ( SELECT 1 FROM [languages_list] WHERE name = N'Russian' )
BEGIN
    INSERT [dbo].[languages_list] ([name], [code]) VALUES (N'Russian', N'RU')
END
 
IF NOT EXISTS ( SELECT 1 FROM [languages_list] WHERE name = N'French' )
BEGIN
    INSERT [dbo].[languages_list] ([name], [code]) VALUES (N'French',N'FR')
END
 
IF NOT EXISTS ( SELECT 1 FROM [languages_list] WHERE name = N'Spanish' )
BEGIN
    INSERT [dbo].[languages_list] ([name], [code]) VALUES (N'Spanish',N'ES')
END
 
IF NOT EXISTS ( SELECT 1 FROM [languages_list] WHERE name = N'Portuguese' )
BEGIN
    INSERT [dbo].[languages_list] ([name], [code]) VALUES (N'Portuguese',N'PT')
END
 
IF NOT EXISTS ( SELECT 1 FROM [languages_list] WHERE name = N'Korean' )
BEGIN
    INSERT [dbo].[languages_list] ([name], [code]) VALUES (N'Korean',N'KO')
END

IF NOT EXISTS ( SELECT 1 FROM [languages_list] WHERE name = N'Arabic' )
BEGIN
    INSERT [dbo].[languages_list] ([name], [code]) VALUES (N'Arabic',N'AR')
END

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'alerts_received_recur' )
	CREATE TABLE [alerts_received_recur] (
		[id] [bigint] IDENTITY (1,1) NOT NULL,
		[alert_id] [bigint] NOT NULL,
		[user_id] [bigint]  NOT NULL,
		[date] [datetime] NOT NULL,
		CONSTRAINT [PK_alerts_received_recur] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY];
ELSE IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE column_name = 'id' AND table_name = 'alerts_received_recur' )
	ALTER TABLE [dbo].[alerts_received_recur] ADD [id] [bigint] IDENTITY (1,1) NOT NULL
GO
IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_alerts_received_recur]') AND type = 'K')
	ALTER TABLE [dbo].[alerts_received_recur] ADD CONSTRAINT [PK_alerts_received_recur] PRIMARY KEY CLUSTERED ([id] ASC)


/* parametrized alerts */

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='param_temp_id'
		AND table_name = 'alerts' )
BEGIN
	ALTER TABLE [alerts] ADD [param_temp_id] [uniqueidentifier] NULL
END

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='param_temp_id'
		AND table_name = 'archive_alerts' )
BEGIN
	ALTER TABLE [archive_alerts] ADD [param_temp_id] [uniqueidentifier] NULL
END

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE column_name = 'rss_allow_content' AND table_name = 'archive_alerts')
	ALTER TABLE [dbo].[archive_alerts] ADD [rss_allow_content] int NULL


IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[alert_param_templates]') AND type = N'U')
	CREATE TABLE [dbo].[alert_param_templates](
		[id] [uniqueidentifier] NOT NULL,
		[name] [nvarchar](255) NOT NULL,
		[html] [nvarchar](max) NOT NULL,
		CONSTRAINT [PK_alert_param_templates] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY]
GO

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_alert_param_templates_id]') AND type = 'D')
	ALTER TABLE [dbo].[alert_param_templates] ADD CONSTRAINT [DF_alert_param_templates_id] DEFAULT (newid()) FOR [id]
GO

IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[alert_param_template_forms]') AND type = N'U')
	CREATE TABLE [dbo].alert_param_template_forms(
		[id] [uniqueidentifier] NOT NULL,
		[name] [nvarchar](255) NOT NULL,
		[html] [nvarchar](max) NOT NULL,
		[order] [bigint] NOT NULL,
		[show_condition] [nvarchar](max) NULL,
		[next_button_name] [nvarchar](max) NULL,
		[previous_button_name] [nvarchar](max) NULL,
		[close_button_name] [nvarchar](max) NULL,
		[param_temp_id] [uniqueidentifier] NOT NULL,
		CONSTRAINT [PK_alert_param_template_forms] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY]
GO

IF EXISTS(SELECT * FROM sys.columns WHERE Name = N'form' AND object_id = OBJECT_ID(N'alert_param_templates'))    
BEGIN
	EXEC ('ALTER TABLE alert_param_templates DROP COLUMN form')
END

GO 

IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[alert_parameters]') AND type = N'U')
	CREATE TABLE [dbo].[alert_parameters](
		[id] [uniqueidentifier] NOT NULL,
		[temp_id] [uniqueidentifier] NOT NULL,
		[short_name] [varchar](255) NOT NULL,
		[name] [nvarchar](255) NOT NULL,
		[type] [tinyint] NOT NULL,
		[order] [smallint] NOT NULL,
		[default] [nvarchar](max) NULL,
		[visible] [bit] NOT NULL,
		[action] [smallint] NULL,
		[required] [bit] NOT NULL DEFAULT ((0)),
		CONSTRAINT [PK_alert_parameters] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY]
GO

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_alert_parameters_id]') AND type = 'D')
	ALTER TABLE [dbo].[alert_parameters] ADD CONSTRAINT [DF_alert_parameters_id] DEFAULT (newid()) FOR [id]
GO

IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[alert_param_values]') AND type = N'U')
	CREATE TABLE [dbo].[alert_param_values](
		[id] [bigint] IDENTITY (1,1) NOT NULL,
		[alert_id] [bigint] NOT NULL,
		[param_id] [uniqueidentifier] NOT NULL,
		[value] [nvarchar](max) NOT NULL,
		CONSTRAINT [PK_alert_param_values] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY]
ELSE IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE column_name = 'id' AND table_name = 'alert_param_values' )
	ALTER TABLE [dbo].[alert_param_values] ADD [id] [bigint] IDENTITY (1,1) NOT NULL
GO
IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_alert_param_values]') AND type = 'K')
	ALTER TABLE [dbo].[alert_param_values] ADD CONSTRAINT [PK_alert_param_values] PRIMARY KEY CLUSTERED ([id] ASC)
GO

IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[alert_param_selects]') AND type = N'U')
	CREATE TABLE [dbo].[alert_param_selects](
		[id] [uniqueidentifier] NOT NULL,
		[param_id] [uniqueidentifier] NOT NULL,
		[name] [nvarchar](255) NOT NULL,
		[value] [nvarchar](max) NOT NULL,
		[type] [tinyint] NOT NULL,
		[order] [bigint] NULL,
		CONSTRAINT [PK_alert_param_selects] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY]
GO

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE column_name = 'order' AND table_name = 'alert_param_selects' )
	ALTER TABLE [dbo].[alert_param_selects] ADD [order] [bigint] NULL
GO

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_alert_param_selects_id]') AND type = 'D')
	ALTER TABLE [dbo].[alert_param_selects] ADD CONSTRAINT [DF_alert_param_selects_id] DEFAULT (newid()) FOR [id]

GO

IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ou_groups]') AND type = N'U')
	CREATE TABLE [dbo].[ou_groups](
		[id] [bigint] IDENTITY(1,1) NOT NULL,
		[group_id] [bigint] NOT NULL,
		[ou_id] [bigint] NOT NULL,
		[was_checked] [int] NOT NULL,
		CONSTRAINT [PK_ou_groups] PRIMARY KEY CLUSTERED ([id] ASC)
	) ON [PRIMARY]

GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[alerts_langs]') AND type in (N'U'))
    CREATE TABLE [dbo].[alerts_langs](
        [id] [bigint] IDENTITY(1,1) NOT NULL,
        [name] [nvarchar](50) NOT NULL,
        [abbreviatures] [nvarchar](50) NOT NULL,
        [is_default] [bit] NOT NULL,
        CONSTRAINT [PK_alerts_langs] PRIMARY KEY CLUSTERED ([id] ASC)
    ) ON [PRIMARY]
ELSE IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE column_name = 'id' AND table_name = 'alerts_langs' )
    ALTER TABLE [dbo].[alerts_langs] ADD [id] [bigint] IDENTITY (1,1) NOT NULL
GO
IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_alerts_langs]') AND type = 'K')
    ALTER TABLE [dbo].[alerts_langs] ADD CONSTRAINT [PK_alerts_langs] PRIMARY KEY CLUSTERED ([id] ASC)
	

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[languages]'))
	DROP TABLE languages
	
GO

IF NOT EXISTS ( SELECT 1 FROM [alerts_langs])
BEGIN
    INSERT INTO [alerts_langs] (name, abbreviatures, is_default) VALUES (N'English', N'EN', 1);
END

IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[synchronizations]') AND type = N'U')
BEGIN
	CREATE TABLE [dbo].[synchronizations](
		[id] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
		[name] [nvarchar](max) NULL,
		[sync_data] [nvarchar](max) NULL,
		[last_sync_time] [datetime] NULL,
		[last_time_sync_started] [datetime] NULL,
		[start_time] [datetime] NULL,
		[recur_id] [bigint] NULL,
		CONSTRAINT [PK_synchronizations] PRIMARY KEY CLUSTERED  ([id] ASC) ON [PRIMARY]
	) ON [PRIMARY]
END
ELSE IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE column_name = 'last_time_sync_started' AND table_name = 'synchronizations' )
BEGIN
    ALTER TABLE [dbo].[synchronizations] ADD [last_time_sync_started] [datetime] NULL
END

GO

------------------------- Cache --------------------------

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE column_name = 'occurrence' AND table_name = 'alerts_received' )
BEGIN
	ALTER TABLE [dbo].[alerts_received] ADD [occurrence] [bigint]
	EXECUTE(N'USE [%dbname_both%]
		UPDATE alerts_received
		SET occurrence = r.cnt
		FROM
		(
			SELECT alert_id, user_id, clsid, COUNT(1) AS cnt
			FROM alerts_received
			GROUP BY alert_id, user_id, clsid
		) r
		WHERE alerts_received.alert_id = r.alert_id
			AND alerts_received.user_id = r.user_id
			AND alerts_received.clsid = r.clsid')
END

GO

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE column_name = 'occurrence' AND table_name = 'alerts_received_recur' )
BEGIN
	ALTER TABLE [dbo].[alerts_received_recur] ADD [occurrence] [bigint]
	EXECUTE(N'USE [%dbname_both%]
		UPDATE alerts_received_recur
		SET occurrence = r.cnt
		FROM
		(
			SELECT alert_id, user_id, COUNT(1) AS cnt
			FROM alerts_received_recur
			GROUP BY alert_id, user_id
		) r
		WHERE alerts_received_recur.alert_id = r.alert_id
			AND alerts_received_recur.user_id = r.user_id')
END

GO

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE column_name = 'occurrence' AND table_name = 'archive_alerts_received' )
	ALTER TABLE [dbo].[archive_alerts_received] ADD [occurrence] [bigint]
GO

-- alerts_cache_users --

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[alerts_cache_users]') AND type in (N'U'))
	CREATE TABLE [dbo].[alerts_cache_users](
		[id] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
		[alert_id] [bigint] NOT NULL,
		[recur_id] [bigint] NOT NULL,
		[user_id] [bigint] NOT NULL,
		[from_date] [datetime] NOT NULL,
		[to_date] [datetime] NOT NULL,
		CONSTRAINT [PK_alerts_cache_users] PRIMARY KEY CLUSTERED ([id] ASC) ON [PRIMARY]
	) ON [PRIMARY]

GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_alerts_cache_users_id]') AND type = 'D')
	ALTER TABLE [dbo].[alerts_cache_users] ADD CONSTRAINT [DF_alerts_cache_users_id] DEFAULT (newid()) FOR [id]
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[alerts_cache_users]') AND name = N'IX_alerts_cache_users_all')
	DROP INDEX [IX_alerts_cache_users_all] ON [dbo].[alerts_cache_users]
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[alerts_cache_users]') AND name = N'IX_alerts_cache_users_updated')
	DROP INDEX [IX_alerts_cache_users_updated] ON [dbo].[alerts_cache_users]
GO

IF (SELECT IS_NULLABLE FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name = 'recur_id'
		AND table_name = 'alerts_cache_users') = 'YES'
BEGIN
	UPDATE [alerts_cache_users] SET [recur_id] = 0 WHERE recur_id IS NULL
	ALTER TABLE [alerts_cache_users]
		ALTER COLUMN [recur_id] BIGINT NOT NULL
END

GO

IF (SELECT IS_NULLABLE FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name = 'to_date'
		AND table_name = 'alerts_cache_users') = 'YES'
BEGIN
	UPDATE [alerts_cache_users] SET [to_date] = '9999-01-01 00:00:00.000' WHERE to_date IS NULL
	ALTER TABLE [alerts_cache_users]
		ALTER COLUMN [to_date] [datetime] NOT NULL
END

GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name = 'updated'
		AND table_name = 'alerts_cache_users')
	ALTER TABLE [alerts_cache_users] DROP COLUMN [updated]

GO

CREATE NONCLUSTERED INDEX [IX_alerts_cache_users_all] ON [dbo].[alerts_cache_users] 
(
	[user_id] ASC,
	[from_date] ASC,
	[to_date] ASC,
	[alert_id] ASC,
	[recur_id] ASC
)
INCLUDE
(
	[id]
) ON [PRIMARY]

GO

CREATE NONCLUSTERED INDEX [IX_alerts_cache_users_updated] ON [dbo].[alerts_cache_users] 
(
	[alert_id] ASC,
	[recur_id] ASC,
	[from_date] ASC,
	[to_date] ASC
)
INCLUDE
(
	[id]
) ON [PRIMARY]

GO

-- alerts_cache_comps --

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[alerts_cache_comps]') AND type in (N'U'))
	CREATE TABLE [dbo].[alerts_cache_comps](
		[id] [uniqueidentifier] ROWGUIDCOL NOT NULL,
		[alert_id] [bigint] NOT NULL,
		[recur_id] [bigint] NOT NULL,
		[comp_id] [bigint] NOT NULL,
		[from_date] [datetime] NOT NULL,
		[to_date] [datetime] NOT NULL,
		CONSTRAINT [PK_alerts_cache_comps] PRIMARY KEY CLUSTERED ([id] ASC) ON [PRIMARY]
	) ON [PRIMARY]

GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_alerts_cache_comps_id]') AND type = 'D')
	ALTER TABLE [dbo].[alerts_cache_comps] ADD  CONSTRAINT [DF_alerts_cache_comps_id]  DEFAULT (newid()) FOR [id]
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[alerts_cache_comps]') AND name = N'IX_alerts_cache_comps_all')
	DROP INDEX [IX_alerts_cache_comps_all] ON [dbo].[alerts_cache_comps]
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[alerts_cache_comps]') AND name = N'IX_alerts_cache_comps_updated')
	DROP INDEX [IX_alerts_cache_comps_updated] ON [dbo].[alerts_cache_comps]
GO

IF (SELECT IS_NULLABLE FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name = 'recur_id'
		AND table_name = 'alerts_cache_comps') = 'YES'
BEGIN
	UPDATE [alerts_cache_comps] SET [recur_id] = 0 WHERE recur_id IS NULL
	ALTER TABLE [alerts_cache_comps]
		ALTER COLUMN [recur_id] BIGINT NOT NULL
END

GO

IF (SELECT IS_NULLABLE FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name = 'to_date'
		AND table_name = 'alerts_cache_comps') = 'YES'
BEGIN
	UPDATE [alerts_cache_comps] SET [to_date] = '9999-01-01 00:00:00.000' WHERE to_date IS NULL
	ALTER TABLE [alerts_cache_comps]
		ALTER COLUMN [to_date] [datetime] NOT NULL
END

GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name = 'updated'
		AND table_name = 'alerts_cache_comps')
	ALTER TABLE [alerts_cache_comps] DROP COLUMN [updated]

GO

CREATE NONCLUSTERED INDEX [IX_alerts_cache_comps_all] ON [dbo].[alerts_cache_comps] 
(
	[comp_id] ASC,
	[from_date] ASC,
	[to_date] ASC,
	[alert_id] ASC,
	[recur_id] ASC
)
INCLUDE
(
	[id]
) ON [PRIMARY]

GO

CREATE NONCLUSTERED INDEX [IX_alerts_cache_comps_updated] ON [dbo].[alerts_cache_comps] 
(
	[alert_id] ASC,
	[recur_id] ASC,
	[from_date] ASC,
	[to_date] ASC
)
INCLUDE
(
	[id]
) ON [PRIMARY]

GO

-- alerts_cache_ips --

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[alerts_cache_ips]') AND type in (N'U'))
	CREATE TABLE [dbo].[alerts_cache_ips](
		[id] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
		[alert_id] [bigint] NOT NULL,
		[recur_id] [bigint] NOT NULL,
		[ip_from] [bigint] NOT NULL,
		[ip_to] [bigint] NOT NULL,
		[from_date] [datetime] NOT NULL,
		[to_date] [datetime] NOT NULL,
		CONSTRAINT [PK_alerts_cache_ips] PRIMARY KEY CLUSTERED ([id] ASC) ON [PRIMARY]
	) ON [PRIMARY]

GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_alerts_cache_ips_id]') AND type = 'D')
	ALTER TABLE [dbo].[alerts_cache_ips] ADD  CONSTRAINT [DF_alerts_cache_ips_id]  DEFAULT (newid()) FOR [id]
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[alerts_cache_ips]') AND name = N'IX_alerts_cache_ips_all')
	DROP INDEX [IX_alerts_cache_ips_all] ON [dbo].[alerts_cache_ips]
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[alerts_cache_ips]') AND name = N'IX_alerts_cache_ips_updated')
	DROP INDEX [IX_alerts_cache_ips_updated] ON [dbo].[alerts_cache_ips] WITH ( ONLINE = OFF )
GO

IF (SELECT IS_NULLABLE FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name = 'recur_id'
		AND table_name = 'alerts_cache_ips') = 'YES'
BEGIN
	UPDATE [alerts_cache_ips] SET [recur_id] = 0 WHERE recur_id IS NULL
	ALTER TABLE [alerts_cache_ips]
		ALTER COLUMN [recur_id] BIGINT NOT NULL
END

GO

IF (SELECT IS_NULLABLE FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name = 'to_date'
		AND table_name = 'alerts_cache_ips') = 'YES'
BEGIN
	UPDATE [alerts_cache_ips] SET [to_date] = '9999-01-01 00:00:00.000' WHERE to_date IS NULL
	ALTER TABLE [alerts_cache_ips]
		ALTER COLUMN [to_date] [datetime] NOT NULL
END

GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name = 'updated'
		AND table_name = 'alerts_cache_ips')
	ALTER TABLE [alerts_cache_ips] DROP COLUMN [updated]

GO

CREATE NONCLUSTERED INDEX [IX_alerts_cache_ips_all] ON [dbo].[alerts_cache_ips] 
(
	[ip_from] ASC,
	[ip_to] ASC,
	[from_date] ASC,
	[to_date] ASC,
	[alert_id] ASC,
	[recur_id] ASC
)
INCLUDE
(
	[id]
) ON [PRIMARY]

GO

CREATE NONCLUSTERED INDEX [IX_alerts_cache_ips_updated] ON [dbo].[alerts_cache_ips] 
(
	[alert_id] ASC,
	[recur_id] ASC,
	[from_date] ASC,
	[to_date] ASC
)
INCLUDE
(
	[id]
) ON [PRIMARY]

GO
-- alert_captions --

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='caption_id'
		AND table_name = 'alerts' )
BEGIN
	ALTER TABLE [alerts] ADD [caption_id] [uniqueidentifier] NULL
END

GO

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='caption_id'
		AND table_name = 'archive_alerts' )
BEGIN
	ALTER TABLE [archive_alerts] ADD [caption_id] [uniqueidentifier] NULL
END

GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[alert_captions]'))
	CREATE TABLE [dbo].[alert_captions](
	[id] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
	[name] [nvarchar](max) NULL,
	[file_name] [nvarchar](max) NULL,
	CONSTRAINT [PK_alert_captions] PRIMARY KEY CLUSTERED ([id] ASC)
) ON [PRIMARY]

GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_alert_captions_id]') AND type = 'D')
	ALTER TABLE [dbo].[alert_captions] ADD  CONSTRAINT [DF_alert_captions_id]  DEFAULT (newid()) FOR [id]
	
GO

-- screensavers

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='class'
		AND table_name = 'alerts' )
BEGIN
	ALTER TABLE [alerts] ADD [class] [bigint] NOT NULL DEFAULT 1
END

GO

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='class'
		AND table_name = 'archive_alerts' )
BEGIN
	ALTER TABLE [archive_alerts] ADD [class] [bigint] NOT NULL DEFAULT 1
END


-- group changes of RSS alerts

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='parent_id'
		AND table_name = 'alerts' )
BEGIN
	ALTER TABLE [alerts] ADD [parent_id] [bigint] NULL
END

GO

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='parent_id'
		AND table_name = 'archive_alerts' )
BEGIN
	ALTER TABLE [archive_alerts] ADD [parent_id] [bigint] NULL
END

GO

-- resizable alerts

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name = 'resizable'
		AND table_name = 'alerts' )
BEGIN
	ALTER TABLE [alerts] ADD [resizable] [bit] NULL
END

GO

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name = 'resizable'
		AND table_name = 'archive_alerts' )
BEGIN
	ALTER TABLE [archive_alerts] ADD [resizable] [bit] NULL
END

GO

-- alerts blogs

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name = 'post_to_blog'
		AND table_name = 'alerts' )
BEGIN
	ALTER TABLE [alerts] ADD [post_to_blog] [bit] NULL
END

GO

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name = 'post_to_blog'
		AND table_name = 'archive_alerts' )
BEGIN
	ALTER TABLE [archive_alerts] ADD [post_to_blog] [bit] NULL
END

GO

-- alerts social_media

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name = 'social_media'
		AND table_name = 'alerts' )
BEGIN
	ALTER TABLE [alerts] ADD [social_media] [bit] NULL
END

GO

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name = 'social_media'
		AND table_name = 'archive_alerts' )
BEGIN
	ALTER TABLE [archive_alerts] ADD [social_media] [bit] NULL
END

GO

-- alerts self_deletable

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name = 'self_deletable'
		AND table_name = 'alerts' )
BEGIN
	ALTER TABLE [alerts] ADD [self_deletable] [bit] NULL
END

GO

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name = 'self_deletable'
		AND table_name = 'archive_alerts' )
BEGIN
	ALTER TABLE [archive_alerts] ADD [self_deletable] [bit] NULL
END

GO

-- alerts text to call

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name = 'text_to_call'
		AND table_name = 'alerts' )
BEGIN
	ALTER TABLE [alerts] ADD [text_to_call] [varchar] (1)
END

GO

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name = 'text_to_call'
		AND table_name = 'archive_alerts' )
BEGIN
	ALTER TABLE [archive_alerts] ADD [text_to_call] [varchar] (1)
END

GO

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='correct'
		AND table_name = 'surveys_answers' )
BEGIN
	ALTER TABLE [surveys_answers] ADD [correct] [bit] NULL
END

GO


-- iCalendars
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[icalendars]'))
	CREATE TABLE [dbo].[icalendars] (
		[id] uniqueidentifier NOT NULL ROWGUIDCOL
			CONSTRAINT PK_icalendars PRIMARY KEY CLUSTERED (id)
			CONSTRAINT DF_icalendars_id DEFAULT newid(),
		[calendar_id] uniqueidentifier NOT NULL
			CONSTRAINT DF_icalendars_calendar_id DEFAULT newid(),
		[summary] nvarchar(MAX) NOT NULL,
		[location] nvarchar(MAX) NOT NULL,
		[description] nvarchar(MAX) NOT NULL,
		[sender_id] bigint NOT NULL,
		[organizer] nvarchar(255) NULL,
		[organizer_email] nvarchar(255) NULL,
		[create_date] datetime NOT NULL
			CONSTRAINT DF_icalendars_create_date DEFAULT getutcdate(),
		[modify_date] datetime NOT NULL
			CONSTRAINT DF_icalendars_modify_date DEFAULT getutcdate(),
		[class] tinyint NOT NULL
			CONSTRAINT DF_icalendars_class DEFAULT 0,
		[priority] tinyint NOT NULL
			CONSTRAINT DF_icalendars_priority DEFAULT 0,
		[sequence] int NOT NULL
			CONSTRAINT DF_icalendars_sequence DEFAULT 0,
		[status] tinyint NOT NULL
			CONSTRAINT DF_icalendars_status DEFAULT 0,
		[start_date] datetime NULL,
		[end_date] datetime NULL,
		[alarm] datetime NULL
	) ON [PRIMARY]

GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[icalendars]') AND name = N'IX_icalendars_calendar_id')
	DROP INDEX [IX_icalendars_calendar_id] ON [dbo].[icalendars]
GO

CREATE NONCLUSTERED INDEX [IX_icalendars_calendar_id] ON [dbo].[icalendars] 
(
	[calendar_id] ASC
)
INCLUDE
(
	[id],
	[summary],
	[location],
	[description],
	[sender_id],
	[organizer],
	[organizer_email],
	[create_date],
	[modify_date],
	[class],
	[priority],
	[sequence],
	[status],
	[start_date],
	[end_date],
	[alarm]
) ON [PRIMARY]

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[user_instances]'))
	CREATE TABLE [dbo].[user_instances]
	(
		[id] [uniqueidentifier] NOT NULL ROWGUIDCOL
			CONSTRAINT PK_user_instances PRIMARY KEY CLUSTERED (id)
			CONSTRAINT [DF_user_instances_id] DEFAULT (newid()),
		[user_id] bigint NOT NULL,
		[clsid] uniqueidentifier NOT NULL,
		[device_type] int NOT NULL,
		[device_id] varchar(512) NOT NULL,
		[android_sdk_int] int NULL
	)  ON [PRIMARY]
ELSE
BEGIN
	-- Deletion user_instances for deleted users.
	DELETE FROM user_instances
	WHERE user_id NOT IN (
		SELECT id
		FROM users
	);

	-- Constrain for automated deletion user_instances when user deleted.
	IF NOT EXISTS (
		SELECT *
		FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS
		WHERE CONSTRAINT_NAME = 'FK_users_user_instances'
	) BEGIN
		ALTER TABLE [user_instances]
			WITH CHECK
			ADD CONSTRAINT [FK_users_user_instances] FOREIGN KEY([user_id]) REFERENCES [users]([id]) ON DELETE CASCADE;
		ALTER TABLE [user_instances] CHECK CONSTRAINT [FK_users_user_instances];
	  END;
END;
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[user_instance_ip]'))
	CREATE TABLE [dbo].[user_instance_ip]
	(
		[id] [uniqueidentifier] NOT NULL ROWGUIDCOL
			CONSTRAINT PK_user_instance_ip PRIMARY KEY CLUSTERED (id)
			CONSTRAINT [DF_user_instance_guid] DEFAULT (newid()),
		[user_instance_guid] [uniqueidentifier] NOT NULL,
		[user_instance_ip] [nvarchar](255) NULL
	)  ON [PRIMARY]

GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[user_instance_computers]'))
	CREATE TABLE [dbo].[user_instance_computers]
	(
		[id] [uniqueidentifier] NOT NULL ROWGUIDCOL
			CONSTRAINT PK_user_instance_computers PRIMARY KEY CLUSTERED (id)
			CONSTRAINT [DF_user_instance_computers] DEFAULT (newid()),
		[user_instance_guid] [uniqueidentifier] NOT NULL,
		[user_instance_computer_id] bigint NULL
	)  ON [PRIMARY]

GO

IF NOT EXISTS (SELECT 1  FROM SYS.COLUMNS WHERE  
	OBJECT_ID = OBJECT_ID(N'[dbo].[user_instances]') AND name = 'android_sdk_int')
BEGIN
	ALTER TABLE [dbo].[user_instances] ADD android_sdk_int integer NULL
END

GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[user_instances]') AND name = N'IX_user_instances_user_id_clsid')
	DROP INDEX [IX_user_instances_user_id_clsid] ON [dbo].[user_instances]

GO
IF EXISTS (SELECT 1  FROM SYS.COLUMNS WHERE  
	OBJECT_ID = OBJECT_ID(N'[dbo].[user_instances]') AND name = 'clsid')
BEGIN
	ALTER TABLE [dbo].[user_instances] ALTER COLUMN [clsid] uniqueidentifier NULL
END

GO
CREATE NONCLUSTERED INDEX [IX_user_instances_user_id_clsid] ON [dbo].[user_instances] 
(
	[user_id] ASC,
	[clsid] ASC
)
INCLUDE
(
	[device_type],
	[device_id]
) ON [PRIMARY]

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[blogs]'))
	CREATE TABLE [dbo].[blogs]
	(
		[id] [uniqueidentifier] NOT NULL ROWGUIDCOL
			CONSTRAINT [PK_blogs] PRIMARY KEY CLUSTERED (id)
			CONSTRAINT [DF_blogs_id] DEFAULT (newid()),
		[name] [nvarchar](50) NOT NULL,
		[url] [nvarchar](500) NOT NULL,
		[username] [nvarchar](50) NOT NULL,
		[password] [nvarchar](50) NOT NULL,
		[blogID] [int] NOT NULL,
		[draft] [bit] NULL,
		[comments] [bit] NULL,
		[postAsPage] [bit] NULL,
		[author] [nvarchar](50) NULL
	) ON [PRIMARY]
	
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[color_codes]'))
BEGIN
	CREATE TABLE [dbo].[color_codes](
	[id] [uniqueidentifier] NULL,
	[name] [nvarchar](50) NULL,
	[color] [varchar](6) NULL
) ON [PRIMARY]
	ALTER TABLE [dbo].[color_codes] ADD  CONSTRAINT [DF_color_codes_id]  DEFAULT (newid()) FOR [id]
END

ALTER TABLE [dbo].[color_codes] ALTER COLUMN [name] nvarchar(50) NULL


IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[social_media]'))
	CREATE TABLE [dbo].[social_media]
	(
		[id] [uniqueidentifier] NOT NULL ROWGUIDCOL
			CONSTRAINT [PK_social_media] PRIMARY KEY CLUSTERED (id)
			CONSTRAINT [DF_social_media_id] DEFAULT (newid()),
		[type] [tinyint] NOT NULL
			CONSTRAINT [DF_social_media_type] DEFAULT ((0)),
		[name] [nvarchar](250) NULL,
		[field1] [nvarchar](500) NULL,
		[field2] [nvarchar](500) NULL,
		[field3] [nvarchar](500) NULL,
		[field4] [nvarchar](500) NULL
	) ON [PRIMARY]
	
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[instant_messages]'))
	CREATE TABLE [dbo].[instant_messages]
	(
		[id] [uniqueidentifier] NOT NULL ROWGUIDCOL
			CONSTRAINT [PK_instant_messages] PRIMARY KEY CLUSTERED (id)
			CONSTRAINT [DF_instant_messages_id] DEFAULT (newid()),
		[alert_id] [bigint] NOT NULL
	) ON [PRIMARY]

GO
IF NOT EXISTS (SELECT 1 FROM [dbo].[social_media] WHERE [id] = 'ee70dc87-7c4f-4953-ab20-ea1bf16b5bb0')
	INSERT [dbo].[social_media] ([id], [type], [name], [field1], [field2], [field3], [field4]) VALUES ('ee70dc87-7c4f-4953-ab20-ea1bf16b5bb0', 1, N'DeskAlertsDemo', N'eGJly1ZgQ8Dc3Se2YlT4CQ', N'OHReXS1RaamDB96M4jORpEgxpiZg1A7xYznhj5KTcfE', N'2152556454-ItKuQcvmpJZXxvNMrlgY7NMQbcAUvfdv5Cqpzos', N'wMepibtCss3OU6XPB4KzXf9lQVhnSTLTOtoTCgNpPKiBq')
GO

-- TGROUPS TABLES
IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'message_templates_groups' )
CREATE TABLE [dbo].[message_templates_groups](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NULL
) ON [PRIMARY]
GO

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'group_message_templates' )
CREATE TABLE [dbo].[group_message_templates](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[group_id] [int] NOT NULL,
	[template_id] [int] NOT NULL
) ON [PRIMARY]
GO

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'policy_tgroups' )
CREATE TABLE [dbo].[policy_tgroups](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[tgroup_id] [bigint] NOT NULL,
	[policy_id] [bigint] NOT NULL,
	[was_checked] [int] NOT NULL
) ON [PRIMARY]
GO



IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'digsign_links' )
CREATE TABLE [dbo].[digsign_links](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](255) NULL,
	[link] [nvarchar](255) NULL,
	[creation_date] [datetime] NULL,
	[slide_time_value] [int] NULL,
	[slide_time_factor] [int] NULL
) ON [PRIMARY]
GO


IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'banners' )
CREATE TABLE  [dbo].[banners](
	[id] [int] IDENTITY NOT NULL,
	[name] [varchar](50) NULL,
	[date] [datetime] NULL,
	[width] [int] NULL,
	[width_unit] [varchar](3) NULL,
	[max_width] [int] NULL,
	[max_width_unit] [varchar](3) NULL,
	[min_width] [int] NULL,
	[min_width_unit] [varchar](3) NULL,
	[height] [int] NULL,
	[height_unit] [varchar](3) NULL,
	[max_height] [int] NULL,
	[max_height_unit] [varchar](3) NULL,
	[min_height] [int] NULL,
	[min_height_unit] [varchar](3) NULL,
	[background_color] [varchar](7) NULL,
	[transparent] [bit] NULL,
	[font_color] [varchar](7) NULL,
	[font_size] [int] NULL,
	[font_size_unit] [varchar](3) NULL,
	[font_weight] [int] NULL,
	[font_weight_unit] [varchar](3) NULL,
	[text_align] [varchar](10) NULL,
	[direction] [varchar](20) NULL,
	[unicode_bidi] [varchar](20) NULL,
	[border_weight] [int] NULL,
	[border_weight_unit] [varchar](3) NULL,
	[border_style] [varchar](30) NULL,
	[border_color] [varchar](7) NULL,
	[padding_top] [int] NULL,
	[padding_top_unit] [varchar](30) NULL,
	[padding_right] [int] NULL,
	[padding_right_unit] [varchar](30) NULL,
	[padding_bottom] [int] NULL,
	[padding_bottom_unit] [varchar](30) NULL,
	[padding_left] [int] NULL,
	[padding_left_unit] [varchar](30) NULL,
	[margin_top] [int] NULL,
	[margin_top_unit] [varchar](30) NULL,
	[margin_right] [int] NULL,
	[margin_right_unit] [varchar](30) NULL,
	[margin_bottom] [int] NULL,
	[margin_bottom_unit] [varchar](30) NULL,
	[margin_left] [int] NULL,
	[margin_left_unit] [varchar](30) NULL,
	[display] [varchar](30) NULL,
	[position] [varchar](30) NULL,
	[top] [int] NULL,
	[top_unit] [varchar](3) NULL,
	[right] [int] NULL,
	[right_unit] [varchar](3) NULL,
	[bottom] [int] NULL,
	[bottom_unit] [varchar](3) NULL,
	[left] [int] NULL,
	[left_unit] [varchar](3) NULL,
	CONSTRAINT [PK_banner] PRIMARY KEY CLUSTERED ([id] ASC)
) ON [PRIMARY]

GO 

IF NOT EXISTS (SELECT 1 FROM dbo.policy WHERE name = 'defaultuser' AND type = 'A')
	INSERT INTO dbo.policy VALUES('defaultuser','A', 1, 1)
	INSERT INTO [dbo].[policy_list]
           ([policy_id]
           ,[alerts_val]
           ,[emails_val]
           ,[sms_val]
           ,[surveys_val]
           ,[users_val]
           ,[groups_val]
           ,[templates_val]
           ,[text_templates_val]
           ,[statistics_val]
           ,[ip_groups_val]
           ,[rss_val]
           ,[screensavers_val]
           ,[wallpapers_val]
           ,[lockscreens_val]
           ,[im_val]
           ,[text_to_call_val]
           ,[feedback_val]
           ,[cc_val]
           ,[video_val]
           ,[webplugin_val]
           ,[campaign_val])
SELECT id,'0000000','0000000','0000000','0000000','0000000','0000000','0000000','0000000','0000000','0000000','0000000','0000000','0000000','0000000','0000000','0000000','0000000','0000000','0000000','0000000','0000000' FROM dbo.policy WHERE name = 'defaultuser' AND type = 'A'


IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='device'
		AND table_name = 'alerts' )
BEGIN
	ALTER TABLE [alerts] ADD [device] int NULL
END

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='device'
		AND table_name = 'archive_alerts' )
BEGIN
	ALTER TABLE [archive_alerts] ADD [device] int NULL
END

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'menu_items' )
BEGIN
CREATE TABLE [dbo].[menu_items](
	[id] [int] IDENTITY(0,1) NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[loc_key] [nvarchar](200) NOT NULL,
	[image] [nvarchar](max) NOT NULL,
	[link] [nvarchar](200) NOT NULL,
	[enabled] [int] NOT NULL,
	[parent_id] [int] NOT NULL,
	[default_index] [int] NOT NULL,
	[index_in_parent] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_menu_items_enabled]') AND type = 'D')
ALTER TABLE [dbo].[menu_items] ADD  CONSTRAINT [DF_menu_items_enabled]  DEFAULT ((1)) FOR [enabled]
GO

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_menu_items_child_id]') AND type = 'D')
ALTER TABLE [dbo].[menu_items] ADD  CONSTRAINT [DF_menu_items_child_id]  DEFAULT ((-1)) FOR [parent_id]
GO

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_menu_items_default_index]') AND type = 'D')
ALTER TABLE [dbo].[menu_items] ADD  CONSTRAINT [DF_menu_items_default_index]  DEFAULT ((-1)) FOR [default_index]
GO

DELETE FROM menu_items

SET IDENTITY_INSERT [dbo].[menu_items] ON 

INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (1, N'DashBoard', N'LNG_DASHBOARD', N'images/menu_icons/contentschedule_20.png', N'dashboard.aspx', 1, -1, 0, -1)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (2, N'Alerts', N'LNG_ALERT_MENU_ITEM', N'images/menu_icons/alert_20.png', N' ', 1, -1, 1, -1)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (3, N'alerts.create.popup', N'LNG_CREATE_ALERT', N'images/menu_icons/alert_create.png', N'CreateAlert.aspx', 1, 2, -1, 1)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (4, N'alerts.create.ticker', N'LNG_CREATE_TICKER', N'images/menu_icons/ticker.png', N'CreateAlert.aspx?ticker=1', 1, 2, -1, 2)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (5, N'alerts.sent', N'LNG_SENT', N'images/menu_icons/alert_sent.png', N'PopupSent.aspx', 1, 2, -1, 3)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (7, N'alerts.draft', N'LNG_DRAFT', N'images/menu_icons/alert_draft.png', N'PopupSent.aspx?draft=1', 1, 2, -1, 4)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (8, N'alerts.templates', N'LNG_TEXT_TEMPLATES', N'images/menu_icons/templates_20.png', N'TextTemplates.aspx', 1, 2, -1, 5)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (9, N'campaigns', N'LNG_CAMPAIGN_TITLE', N'images/menu_icons/campaign.png', N'Campaign.aspx', 1, -1, 2, -1)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (10, N'digsign', N'LNG_DIGSIGN_TITLE', N'images/menu_icons/ic_digsign.png', N' ', 1, -1, 3, -1)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (74, N'multiple_alerts.create', N'LNG_MULTIPLE', N'images/menu_icons/alert_create.png', N'CreateMultipleAlert.aspx', 1, 2, -1, 1)

INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (11, N'digsign.links', N'LNG_DIGSIGN_LINKS', N'images/menu_icons/ic_link.png', N'DigitalSignage\DigitalSignageLinks.aspx', 1, 10, -1, 1)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (13, N'digsign.send', N'LNG_DIGSIGN_SEND_CONTENT', N'images/menu_icons/alert_sent.png', N'CreateAlert.aspx?webplugin=1', 1, 10, -1, 2)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (15, N'surveys', N'LNG_SURVEY_QUIZZES_POLLS', N'images/menu_icons/survey_20.png', N' ', 1, -1, 4, -1)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (16, N'surveys.create', N'LNG_CREATE', N'images/menu_icons/survey_create.png', N'EditSurvey.aspx', 1, 15, -1, 1)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (17, N'surveys.active', N'LNG_ACTIVE', N'images/menu_icons/survey_active.png', N'surveys.aspx', 1, 15, -1, 2)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (20, N'surveys.archived', N'LNG_ARCHIVED', N'images/menu_icons/survey_archived.png', N'surveys.aspx?draft=1', 1, 15, -1, 3)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (21, N'surveys.opt-in', N'LNG_CREATE_OPT_IN_LIST', N'images/menu_icons/survey_20.png', N'OptInHelp.aspx', 0, -1, 18, -1)

INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (22, N'rss', N'LNG_RSS_NEWSFEEDS', N'images/menu_icons/rss_20.png', N' ', 1, -1, 5, -1)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (23, N'rss.create', N'LNG_CREATE', N'images/menu_icons/rss_create.png', N'EditRss.aspx', 1, 22, -1, 1)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (25, N'rss.current', N'LNG_CURRENT', N'images/menu_icons/rss_current.png', N'RssList.aspx', 1, 22, -1, 2)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (26, N'rss.draft', N'LNG_DRAFT', N'images/menu_icons/rss_draft.png', N'RssList.aspx?draft=1', 1, 22, -1, 3)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (27, N'ss', N'LNG_SCREENSAVERS', N'images/menu_icons/screensaver_20.png', N' ', 1, -1, 6, -1)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (28, N'ss.create', N'LNG_CREATE', N'images/menu_icons/screensaver_create.png', N'EditScreensavers.aspx', 1, 27, -1, 1)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (29, N'ss.current', N'LNG_CURRENT', N'images/menu_icons/screensaver_current.png', N'screensavers.aspx', 1, 27, -1, 2)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (30, N'ss.draft', N'LNG_DRAFT', N'images/menu_icons/screensaver_draft.png', N'screensavers.aspx?draft=1', 1, 27, -1, 3)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (31, N'wp', N'LNG_WALLPAPERS', N'images/menu_icons/wallpaper_20.png', N' ', 1, -1, 7, -1)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (32, N'wp.create', N'LNG_CREATE', N'images/menu_icons/wallpaper_create.png', N'EditWallpaper.aspx', 1, 31, -1, 1)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (33, N'wp.current', N'LNG_CURRENT', N'images/menu_icons/wallpaper_current.png', N'wallpapers.aspx', 1, 31, -1, 2)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (34, N'wp.draft', N'LNG_DRAFT', N'images/menu_icons/wallpaper_draft.png', N'wallpapers.aspx?draft=1', 1, 31, -1, 3)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (35, N'im', N'LNG_INSTANT_SEND', N'images/menu_icons/im_20.png', N' ', 1, -1, 8, -1)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (37, N'im.create', N'LNG_CREATE', N'images/menu_icons/im_create.png', N'CreateAlert.aspx?instant=1', 1, 35, -1, 1)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (38, N'im.colorcodes', N'LNG_COLOR_CODES', N'images/menu_icons/im_codes.png', N'ColorCodes.aspx', 1, 35, -1, 3)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (40, N'im.send', N'LNG_SEND_NOW', N'images/menu_icons/im_send.png', N'InstantMessages.aspx', 1, 35, -1, 2)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (41, N'im.shortcuts', N'LNG_CREATE_SHORTCUT', N'images/menu_icons/create_shortcut.png', N'CreateShortcut.aspx', 1, 35, -1, 4)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (42, N'ad', N'LNG_ACTIVE_DIRECTORY', N'images/menu_icons/domains_20.png', N' ', 1, -1, 9, -1)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (43, N'ad.domains', N'LNG_DOMAINS', N'images/menu_icons/domains_20.png', N'domains.aspx', 1, 42, -1, 1)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (45, N'ad.sync', N'LNG_SYNCHRONIZATIONS', N'images/menu_icons/synchronizations.png', N'synchronizations.aspx', 1, 42, -1, 2)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (46, N'ad.orgs', N'LNG_ORGANIZATION', N'images/menu_icons/organization_20.png', N'Organizations/Index.aspx', 1, 42, -1, 3)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (74, N'ad.orgs.nonadmin', N'LNG_ORGANIZATION', N'images/menu_icons/organization_20.png', N'Organizations/Index.aspx', 1, -1, 21, -1)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (48, N'users.groups', N'LNG_USERS_GROUPS', N'images/menu_icons/users_20.png', N' ', 1, -1, 10, -1)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (50, N'users', N'LNG_USERS', N'images/menu_icons/users_20.png', N'Organizations/OrgUsers.aspx', 1, 48, -1, 1)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (51, N'groups', N'LNG_GROUPS', N'images/menu_icons/ipgroups_20.png', N'Organizations/OrgGroups.aspx', 1, 48, -1, 2)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (52, N'ip_groups', N'LNG_IP_GROUPS', N'images/menu_icons/ipgroups_20.png', N'IpGroups.aspx', 1, 48, -1, 3)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (53, N'pubs', N'LNG_EDITORS', N'images/menu_icons/editors_20.png', N' ', 1, -1, 11, -1)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (54, N'pub.groups_policy', N'LNG_GROUPS_POLICY_MENU_TITLE', N'images/menu_icons/editors_20.png', N'GroupList.aspx', 1, 53, -1, 2)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (55, N'pubs.list', N'LNG_EDITORS_PUBLISHERS_LIST', N'images/menu_icons/editors_20.png', N'PublisherList.aspx', 1, 53, -1, 2)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (56, N'pubs.policies', N'LNG_POLICIES', N'images/menu_icons/policies_20.png', N'PolicyList.aspx', 1, 53, -1, 3)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (57, N'ad.ip_groups', N'LNG_IP_GROUPS', N'images/menu_icons/ipgroups_20.png', N'IpGroups.aspx', 1, -1, 12, -1)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (58, N'feedback', N'LNG_FEEDBACK', N'images/menu_icons/feedback_20.png', N'feedback_show.asp', 0, -1, 13, -1)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (59, N'ext_stat', N'LNG_EXTENDED_STAT', N'images/menu_icons/statistics_20.png', N'Statistics.aspx', 1, -1, 14, -1)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (60, N'stat', N'LNG_STATISTICS_REPORTS', N'images/menu_icons/statistics_20.png', N'Statistics.aspx', 1, -1, 15, -1)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (61, N'settings', N'LNG_SETTINGS', N'images/menu_icons/settings_20.png', N' ', 0, -1, 18, -1)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (62, N'settings.profile', N'LNG_PROFILE_SETTINGS', N' ', N'settings_edit_profile.asp', 1, 62, -1, 1)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (63, N'settings.social', N'LNG_SOCIAL_SETTINGS', N' ', N'settings_social_settings.asp', 1, 62, -1, 2)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (64, N'settings.common', N'LNG_COMMON_SETTINGS', N' ', N'settings_common.asp', 1, 62, -1, 3)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (65, N'approve.panel', N'LNG_APPROVAL_PANEL', N'images/menu_icons/approve.png', N'ApprovePanel.aspx?uid={uid}', 1, -1, 16, -1)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (66, N'approve.reject', N'LNG_REJECTED_ALERTS', N'images/menu_icons/reject.png', N'RejectedAlerts.aspx?uid={uid}', 1, -1, 17, -1)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (67, N'alerts.tgroups', N'LNG_TGROUPS_MENUITEM', N'images/menu_icons/templates_20.png', N'tgroups.asp', 1, 2, -1, 6)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (68, N'video', N'LNG_VIDEO_MODULE', N'images/menu_icons/videomodule_20.png', N' ', 1, -1, 20, -1)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (69, N'video.create', N'LNG_CREATE', N'images/menu_icons/videomodule_create.png', N'EditVideo.aspx', 1, 68, -1, 1)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (70, N'video.current', N'LNG_CURRENT', N'images/menu_icons/videomodule_current.png', N'Videos.aspx', 1, 68, -1, 2)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (71, N'video.draft', N'LNG_DRAFT', N'images/menu_icons/videomodule_draft.png', N'Videos.aspx?draft=1', 1, 68, -1, 3)

INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (72, N'ls', N'LNG_LOCKSCREENS', N'images/menu_icons/lockscreen_20.png', N' ', 1, -1, 7, -1)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (73, N'ls.create', N'LNG_CREATE', N'images/menu_icons/lockscreen_create.png', N'EditLockscreen.aspx', 1, 72, -1, 1)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (74, N'ls.current', N'LNG_CURRENT', N'images/menu_icons/lockscreen_current.png', N'lockscreens.aspx', 1, 72, -1, 2)
INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (75, N'ls.draft', N'LNG_DRAFT', N'images/menu_icons/lockscreen_draft.png', N'lockscreens.aspx?draft=1', 1, 72, -1, 3)


--INSERT [dbo].[menu_items] ([id], [name], [loc_key], [image], [link], [enabled], [parent_id], [default_index], [index_in_parent]) VALUES (72, N'azure_ad', N'LNG_AZURE_AD', N'images/menu_icons/azure_ad.png', N'quasarfront', 1, -1, 21, -1)
SET IDENTITY_INSERT [dbo].[menu_items] OFF

update settings set val = '480' where name='ConfSessionTimeout'

GO
IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'parameters' )
BEGIN
CREATE TABLE [dbo].[parameters](
	[paramName] [varchar](255) NULL,
	[paramValue] [varchar](255) NULL
) ON [PRIMARY]
END

INSERT [dbo].[parameters] ([paramName], [paramValue]) VALUES ('helpStatus', '')
INSERT [dbo].[parameters] ([paramName], [paramValue]) VALUES ('dontShowTrialAddonsDialog', '')
INSERT [dbo].[parameters] ([paramName], [paramValue]) VALUES ('dontShowTour', '')
INSERT [dbo].[parameters] ([paramName], [paramValue]) VALUES ('dontShowTrialDialogToday', '')
INSERT [dbo].[parameters] ([paramName], [paramValue]) VALUES ('maintenanceCheckbox', '')
INSERT [dbo].[parameters] ([paramName], [paramValue]) VALUES ('dashboardListOrder', '')
INSERT [dbo].[parameters] ([paramName], [paramValue]) VALUES ('menuOrder', '')
INSERT [dbo].[parameters] ([paramName], [paramValue]) VALUES ('closeHintStatus', '')
INSERT [dbo].[parameters] ([paramName], [paramValue]) VALUES ('multiple', '')
INSERT [dbo].[parameters] ([paramName], [paramValue]) VALUES ('licensesReminderCriticalPoint', '40')

--license_expire_date (9.1.12.29)

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='license_expire_date'
		AND table_name = 'version' )
BEGIN
	ALTER TABLE [version] ADD [license_expire_date] [datetime]
END

--used_prolong_keys (9.1.12.29)

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='used_prolong_keys'
		AND table_name = 'version' )
BEGIN
	ALTER TABLE [version] ADD [used_prolong_keys] [varchar] (max)
END

GO
IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'jwt_sessions' )
BEGIN
CREATE TABLE [dbo].[jwt_sessions](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[user_id] [nvarchar](255) NULL,
	[creation_time] [nvarchar](255) NULL,	
	[token_value] [nvarchar](255) NULL,	
) ON [PRIMARY]
END
GO

--AD fix and anonymous survey (9.2.2.15)

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'groups' AND column_name ='dn' )
	ALTER TABLE [dbo].[groups] ADD [dn] varchar(255) NULL

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE column_name = 'anonymous_survey' AND table_name = 'alerts') 
	ALTER TABLE [dbo].[alerts] ADD [anonymous_survey] varchar(1) NULL
	
--Fix bug in licensing mechanism (9.2.3.12)

ALTER TABLE [users]
ALTER COLUMN [last_request] [datetime] NULL
GO

--ADD Logs table for logging in project. 2018-03-26
IF EXISTS (SELECT 1 FROM sys.Tables WHERE object_id = OBJECT_ID('Logs'))
	DROP TABLE [dbo].[Logs]
GO
create table [dbo].[Logs](
	[LogId] [int] IDENTITY(1,1) not null,
	[DateTimeUTC] [datetime] not null constraint [df_logs_DateTimeUTC]  default (getutcdate()),
	[IPAddress] [varchar](max) not null,
	[Level] [nvarchar](max) not null,
	[CallSite] [nvarchar](max) not null,
	[Message] [nvarchar](max) not null,
	[ExType] [nvarchar](max) not null,
	[ExMessage] [nvarchar](max) not null,
	[ExStackTrace] [nvarchar](max) not null,
	[ExInnerException] [nvarchar](max) not null,

	constraint [pk_logs] primary key clustered 
	(
		[LogId]
	)
)
GO
IF EXISTS (SELECT 1 FROM sysobjects WHERE id = OBJECT_ID('InsertLog'))
    DROP PROCEDURE [dbo].[InsertLog]
GO
CREATE PROCEDURE [dbo].[InsertLog]
(
	@ipAddress nvarchar(max),
	@level nvarchar(max),
	@callSite nvarchar(max),
	@message nvarchar(max),
	@exType nvarchar(max),
	@exMessage nvarchar(max),
	@exStackTrace nvarchar(max),
	@exInnerException nvarchar(max)
)
AS
INSERT INTO dbo.Logs
(
	[IPAddress],
	[Level],
	[CallSite],
	[Message],
	[ExType],
	[ExMessage],
	[ExStackTrace],
	[ExInnerException]
)
VALUES
(
	@ipAddress,
	@level,
	@callSite,
	@message,
	@exType,
	@exMessage,
	@exStackTrace,
	@exInnerException
)
GO

--ADD SMS sent table for count Users that receive sms. 2018-05-03
IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'sms_sent' )
BEGIN
CREATE TABLE [dbo].[sms_sent](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[user_id] [int] NOT NULL,
	[alert_id] [int] NOT NULL,
	[sent_date] [datetime] NOT NULL
) ON [PRIMARY]
END
GO

--ADD Email sent table for count Users that receive Email. 2018-05-10
IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'email_sent' )
BEGIN
CREATE TABLE [dbo].[email_sent](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[user_id] [int] NOT NULL,
	[alert_id] [int] NOT NULL,
	[sent_date] [datetime] NOT NULL
) ON [PRIMARY]
END
GO

--ADD Text_to_call sent table for count Users that receive text_to_call. 2018-11-15
IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'text_to_call_sent' )
BEGIN
CREATE TABLE [dbo].[text_to_call_sent](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[user_id] [int] NOT NULL,
	[alert_id] [int] NOT NULL,
	[sent_date] [datetime] NOT NULL
) ON [PRIMARY]
END
GO

--ADD Push sent table for count Users-devices that receive pushes. 2018-06-04
IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'push_sent' )
BEGIN
CREATE TABLE [dbo].[push_sent](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[alert_id] [int] NOT NULL,
	[sent_date] [datetime] NOT NULL,
	[status] [int] NOT NULL DEFAULT 1,
	[try_count] [int] NOT NULL DEFAULT 0,
	[user_instance_id] uniqueidentifier NOT NULL,
	CONSTRAINT [PK_push_sent] PRIMARY KEY CLUSTERED ([id] ASC)
) ON [PRIMARY]
END
go
IF NOT EXISTS (SELECT 1  FROM SYS.COLUMNS WHERE  
	OBJECT_ID = OBJECT_ID(N'[dbo].[push_sent]') AND name = 'user_id')
BEGIN
	ALTER TABLE [dbo].[push_sent] ADD user_id integer 
	EXEC(N'update push_sent  set user_id = ui.user_id 	from push_sent ps join user_instances ui on ui.id = ps.user_instance_id')
END

IF NOT EXISTS (SELECT 1  FROM SYS.COLUMNS WHERE  
	OBJECT_ID = OBJECT_ID(N'[dbo].[push_sent]') AND name = 'status')
BEGIN
	ALTER TABLE [dbo].[push_sent] ADD status integer default 0
END
go

IF NOT EXISTS (SELECT 1  FROM SYS.COLUMNS WHERE  
	OBJECT_ID = OBJECT_ID(N'[dbo].[push_sent]') AND name = 'try_count')
BEGIN
	ALTER TABLE [dbo].[push_sent] ADD try_count integer default 1
END
go
IF NOT EXISTS (SELECT 1  FROM SYS.COLUMNS WHERE  
	OBJECT_ID = OBJECT_ID(N'[dbo].[push_sent]') AND name = 'user_instance_id')
BEGIN
	ALTER TABLE [dbo].[push_sent] ADD user_instance_id uniqueidentifier
END
go
IF EXISTS (SELECT 1  FROM SYS.COLUMNS WHERE  
	OBJECT_ID = OBJECT_ID(N'[dbo].[push_sent]') AND name = 'user_id')
BEGIN
	ALTER TABLE [dbo].[push_sent] ALTER COLUMN user_id int NULL
END
go
IF NOT EXISTS (SELECT 1  FROM dbo.sysobjects WHERE  
	id = OBJECT_ID(N'[PK_push_sent]') AND type = 'k')
BEGIN
	ALTER TABLE [dbo].[push_sent] ADD CONSTRAINT [PK_push_sent] PRIMARY KEY CLUSTERED ([id] ASC)
END
go

--ADD referrals information
IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'referrals' )
BEGIN
CREATE TABLE [dbo].[referrals](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[visit_date] [datetime] NOT NULL,
	[email] [nvarchar](255) NULL	
	CONSTRAINT [PK_referrals] PRIMARY KEY CLUSTERED ([id] ASC)
) ON [PRIMARY]
END
GO

-- SSO additional columns
IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'groups' AND column_name ='policy_id' )
	ALTER TABLE [dbo].[groups] ADD [policy_id] [bigint] NULL

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'policy_editor' AND column_name ='group_id' )
	ALTER TABLE [dbo].[policy_editor] ADD [group_id] [bigint] NULL
GO

IF NOT EXISTS(SELECT 1
FROM INFORMATION_SCHEMA.COLUMNS
WHERE column_name = 'language_id'
AND table_name = 'users')
BEGIN
ALTER TABLE [dbo].[users]
ADD [language_id] [INT] NULL
END

-- Add Computer ID to alerts_received
IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='computer_id'
		AND table_name = 'alerts_received' )
BEGIN
	ALTER TABLE [alerts_received] ADD [computer_id] bigint NULL
END
GO

-- Add IP group ID to alerts_received
IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='ip_address'
		AND table_name = 'alerts_received' )
BEGIN
	ALTER TABLE [alerts_received] ADD [ip_address] [nvarchar](255) NULL
END
GO

-- Add status (AlertReceiveStatus) to alerts_received
IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='status'
		AND table_name = 'alerts_received' )
BEGIN
	ALTER TABLE [alerts_received] ADD [status] tinyint NULL
END

GO
IF EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='status'
		AND table_name = 'alerts_received' )
BEGIN
UPDATE [alerts_received] set [alerts_received].[status] = 1
	WHERE [alerts_received].[status] IS NULL OR [alerts_received].[status] = 2
END

GO
	
IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__alerts_received_status]') AND type = 'D')
	BEGIN
		ALTER TABLE [dbo].[alerts_received] ADD CONSTRAINT [DF__alerts_received_status] DEFAULT NULL FOR [status]
	END
GO

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='terminated'
		AND table_name = 'alerts_received' )
BEGIN
	ALTER TABLE [alerts_received] ADD [terminated] tinyint NULL
END

IF EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'users' )
	BEGIN
		UPDATE users
		  SET 
			  domain_id = 1
		WHERE name = 'admin'
			  AND role = 'A';
	END
GO

-- 9.3 Disable Publisher
IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'users' AND column_name ='publisher_last_online_datetime' )
BEGIN
	ALTER TABLE [dbo].[users] ADD [publisher_last_online_datetime] [datetime] NULL
END

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'users' AND column_name ='publisher_status' )
BEGIN
	ALTER TABLE [dbo].[users] ADD [publisher_status] [varchar] (255) NULL
END

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'users' AND column_name ='time_zone' )
BEGIN
	ALTER TABLE [dbo].[users] ADD [time_zone] [int] NULL
END

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'alerts_sent_iprange' AND column_name ='ip_range_group_id' )
BEGIN
	ALTER TABLE [dbo].[alerts_sent_iprange] ADD [ip_range_group_id] [bigint] NULL
END

-- Display Name field form AD
IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'groups' AND column_name ='display_name' )
	ALTER TABLE [dbo].[groups] ADD [display_name] [varchar] (255)

GO

-- Add fields for repeatable content
IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'alerts' AND column_name ='is_repeatable_template' )
BEGIN
	ALTER TABLE [dbo].[alerts] ADD [is_repeatable_template] [bit] NOT NULL DEFAULT 0
END

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'alerts' AND column_name ='parent_template_id' )
BEGIN
	ALTER TABLE [dbo].[alerts] ADD [parent_template_id] [bigint] NULL
END

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'archive_alerts' AND column_name ='is_repeatable_template' )
BEGIN
	ALTER TABLE [dbo].[archive_alerts] ADD [is_repeatable_template] [bit] NOT NULL DEFAULT 0
END

IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'archive_alerts' AND column_name ='parent_template_id' )
BEGIN
	ALTER TABLE [dbo].[archive_alerts] ADD [parent_template_id] [bigint] NULL
END

GO
