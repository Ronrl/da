USE master

GO

IF EXISTS (SELECT * FROM sysdatabases WHERE name='%dbname_string%')
BEGIN
	EXECUTE('USE [%dbname_both%] ' +
    'EXEC sp_MSforeachtable "DECLARE @name nvarchar(max); SET @name = parsename(''?'', 1); EXEC sp_MSdropconstraints @name";' +
	'EXEC sp_MSforeachtable "DROP TABLE ?" ' +
	'DECLARE @sqlstring nvarchar(MAX) ' +
	'SET @sqlstring = N'''' ' +
	'SELECT @sqlstring = @sqlstring + N''drop procedure ['' + sysobjects.name + N''];'' ' +
	'FROM sysobjects ' +
	'WHERE OBJECTPROPERTY(sysobjects.id, N''IsProcedure'') = 1 ' +
	'SELECT @sqlstring = @sqlstring + N''drop function ['' + sysobjects.name + N''];'' ' +
	'FROM sysobjects ' +
	'WHERE OBJECTPROPERTY(sysobjects.id, N''IsScalarFunction'') = 1 OR OBJECTPROPERTY(sysobjects.id, N''IsTableFunction'') = 1 ' +

	'EXECUTE(@sqlstring) ')
END