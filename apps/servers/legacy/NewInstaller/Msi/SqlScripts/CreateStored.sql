USE [%dbname_hook%];

GO

UPDATE [alerts] SET type2='R' WHERE type2='P';
UPDATE [alerts] SET type2='R' WHERE type2='G';
UPDATE [alerts] SET type2='R' WHERE type2='C';

UPDATE [alerts] SET desktop='1' WHERE type2<>'N' AND desktop IS NULL;

UPDATE [archive_alerts] SET type2='R' WHERE type2='P';
UPDATE [archive_alerts] SET type2='R' WHERE type2='G';
UPDATE [archive_alerts] SET type2='R' WHERE type2='C';

GO

IF EXISTS (SELECT *
		   FROM   sysobjects
		   WHERE  id = object_id('recAlerts')
				  AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	DROP PROCEDURE recAlerts;
END

GO

IF EXISTS (SELECT *
		   FROM   sysobjects
		   WHERE  id = object_id('recSurveys')
				  AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	DROP PROCEDURE recSurveys
END

GO

IF EXISTS (SELECT *
		   FROM   sysobjects
		   WHERE  id = object_id('ArchiveData')
				  AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	DROP PROCEDURE ArchiveData;
END

GO

IF EXISTS (SELECT *
		   FROM   sysobjects
		   WHERE  id = object_id('ClearArchive')
				  AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	DROP PROCEDURE ClearArchive;
END
 
GO

CREATE PROC ArchiveData
(
	@CutOffDate datetime = NULL,
	@date_type varchar(50),
	@date_number int
)
AS
BEGIN
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	BEGIN TRANSACTION

	IF @date_type = 'day'
	BEGIN
		SET @CutOffDate = DATEADD(day, -@date_number, CURRENT_TIMESTAMP)
	END
	IF @date_type = 'month'
	BEGIN
		SET @CutOffDate = DATEADD(month, -@date_number, CURRENT_TIMESTAMP)
	END
	IF @date_type = 'year'
	BEGIN
		SET @CutOffDate = DATEADD(year, -@date_number, CURRENT_TIMESTAMP)
	END
	
	
	IF @CutOffDate IS NULL 
	BEGIN
		SET @CutOffDate = DATEADD(mm, -6, CURRENT_TIMESTAMP)
	END

	INSERT INTO archive_alerts
	SELECT * 
	FROM alerts
	WHERE [sent_date] < @CutOffDate

	INSERT INTO archive_alerts_read
	SELECT * 
	FROM alerts_read
	WHERE alert_id IN
	(
		SELECT id
		FROM alerts
		WHERE [sent_date] < @CutOffDate
	)

	INSERT INTO archive_alerts_sent_group
	SELECT * 
	FROM alerts_sent_group
	WHERE alert_id IN
	(
		SELECT id
		FROM alerts
		WHERE [sent_date] < @CutOffDate
	)

	INSERT INTO archive_alerts_group_stat
	SELECT * 
	FROM alerts_group_stat
	WHERE alert_id IN
	(
		SELECT id
		FROM alerts
		WHERE [sent_date] < @CutOffDate
	)

	INSERT INTO archive_alerts_sent
	SELECT * 
	FROM alerts_sent
	WHERE alert_id IN
	(
		SELECT id
		FROM alerts
		WHERE [sent_date] < @CutOffDate
	)

	INSERT INTO archive_alerts_sent_stat
	SELECT * 
	FROM alerts_sent_stat
	WHERE alert_id IN
	(
		SELECT id
		FROM alerts
		WHERE [sent_date] < @CutOffDate
	)

	INSERT INTO archive_alerts_sent_comp
	SELECT * 
	FROM alerts_sent_comp
	WHERE alert_id IN
	(
		SELECT id
		FROM alerts
		WHERE [sent_date] < @CutOffDate
	)

	INSERT INTO archive_alerts_comp_stat
	SELECT * 
	FROM alerts_comp_stat
	WHERE alert_id IN
	(
		SELECT id
		FROM alerts
		WHERE [sent_date] < @CutOffDate
	)

	DELETE alerts_received
	WHERE alert_id IN
	(
		SELECT id
		FROM alerts
		WHERE [sent_date] < @CutOffDate
	)

	DELETE alerts_read
	WHERE alert_id IN
	(
		SELECT id
		FROM alerts
		WHERE [sent_date] < @CutOffDate
	)

	DELETE alerts_sent_group
	WHERE alert_id IN
	(
		SELECT id
		FROM alerts
		WHERE [sent_date] < @CutOffDate
	)

	DELETE alerts_group_stat
	WHERE alert_id IN
	(
		SELECT id
		FROM alerts
		WHERE [sent_date] < @CutOffDate
	)

	DELETE alerts_sent
	WHERE alert_id IN
	(
		SELECT id
		FROM alerts
		WHERE [sent_date] < @CutOffDate
	)

	DELETE alerts_sent_stat
	WHERE alert_id IN
	(
		SELECT id
		FROM alerts
		WHERE [sent_date] < @CutOffDate
	)

	DELETE alerts_sent_comp
	WHERE alert_id IN
	(
		SELECT id
		FROM alerts
		WHERE [sent_date] < @CutOffDate
	)

	DELETE alerts_comp_stat
	WHERE alert_id IN
	(
		SELECT id
		FROM alerts
		WHERE [sent_date] < @CutOffDate
	)

	DELETE alerts
	WHERE [sent_date] < @CutOffDate

	COMMIT TRANSACTION
END

GO

CREATE PROC dbo.ClearArchive
(
	@CutOffDate datetime = NULL,
	@date_type varchar(50),
	@date_number int
	
)
AS
BEGIN
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	BEGIN TRANSACTION

	IF @date_type = 'day'
	BEGIN
		SET @CutOffDate = DATEADD(day, -@date_number, CURRENT_TIMESTAMP)
	END
	IF @date_type = 'month'
	BEGIN
		SET @CutOffDate = DATEADD(month, -@date_number, CURRENT_TIMESTAMP)
	END
	IF @date_type = 'year'
	BEGIN
		SET @CutOffDate = DATEADD(year, -@date_number, CURRENT_TIMESTAMP)
	END
	
	IF @CutOffDate IS NULL 
	BEGIN
		SET @CutOffDate = DATEADD(mm, -6, CURRENT_TIMESTAMP)
	END

	DELETE archive_alerts_read
	WHERE alert_id IN
	(
		SELECT id
		FROM archive_alerts
		WHERE sent_date < @CutOffDate
	)

	DELETE archive_alerts_sent_group
	WHERE alert_id IN
	(
		SELECT id
		FROM archive_alerts
		WHERE sent_date < @CutOffDate
	)

	DELETE archive_alerts_group_stat
	WHERE alert_id IN
	(
		SELECT id
		FROM archive_alerts
		WHERE sent_date < @CutOffDate
	)

	DELETE archive_alerts_sent
	WHERE alert_id IN
	(
		SELECT id
		FROM archive_alerts
		WHERE sent_date < @CutOffDate
	)

	DELETE archive_alerts_sent_stat
	WHERE alert_id IN
	(
		SELECT id
		FROM archive_alerts
		WHERE sent_date < @CutOffDate
	)

	DELETE archive_alerts_sent_comp
	WHERE alert_id IN
	(
		SELECT id
		FROM archive_alerts
		WHERE sent_date < @CutOffDate
	)

	DELETE archive_alerts_comp_stat
	WHERE alert_id IN
	(
		SELECT id
		FROM archive_alerts
		WHERE sent_date < @CutOffDate
	)

	DELETE archive_alerts
	WHERE [sent_date] < @CutOffDate

	COMMIT TRANSACTION
END

GO

IF EXISTS (SELECT 1
		   FROM   sysobjects
		   WHERE  id = object_id('fnGetNthWeekdayOfMonth')
				  AND OBJECTPROPERTY(id, N'IsScalarFunction') = 1)
BEGIN
	DROP FUNCTION fnGetNthWeekdayOfMonth
END

GO

CREATE FUNCTION [dbo].[fnGetNthWeekdayOfMonth]
(
    @theDate [DATETIME],
    @theWeekday [TINYINT],
    @theNth [SMALLINT]
)
RETURNS DATETIME

BEGIN
	IF (@theWeekday < 1)
		SET @theWeekday = 7
    RETURN  (
                SELECT  theDate
                FROM    (
                            SELECT  DATEADD(DAY, 7 * @theNth - 7 * SIGN(SIGN(@theNth) + 1) +(@theWeekday + 6 - DATEDIFF(DAY, '17530101', DATEADD(MONTH, DATEDIFF(MONTH, @theNth, @theDate), '19000101')) % 7) % 7, DATEADD(MONTH, DATEDIFF(MONTH, @theNth, @theDate), '19000101')) AS theDate
                            WHERE   @theWeekday BETWEEN 1 AND 7
                                    AND @theNth IN (-5, -4, -3, -2, -1, 1, 2, 3, 4, 5)
                        ) AS d
                WHERE   DATEDIFF(MONTH, theDate, @theDate) = 0
            )
END

GO

USE [master];

IF OBJECT_ID(N'dbo.isActualRecurrence') IS NOT NULL
BEGIN
	DROP FUNCTION isActualRecurrence
END

GO

USE [%dbname_hook%];

IF OBJECT_ID(N'dbo.isActualRecurrence') IS NOT NULL
BEGIN
	DROP FUNCTION isActualRecurrence
END

GO

CREATE FUNCTION [dbo].[isActualRecurrence](
	@pattern [varchar] (1),
	@number_days [int],
	@number_week [int],
	@week_days [int],
	@month_day [int],
	@number_month [int],
	@weekday_place [int],
	@weekday_day [int],
	@month_val [int],
	@dayly_selector [varchar] (50),
	@monthly_selector [varchar] (50),
	@yearly_selector [varchar] (50),
	@end_type [varchar] (50),
	@occurrences [int],
	@from_date [DATETIME],
	@to_date [DATETIME],
	@occur_cnt [int],
	@max_recieved_date [DATETIME])
RETURNS BIT
AS
BEGIN
	DECLARE @now DATETIME
	DECLARE @tmp_first DATETIME
	DECLARE @tmp_next_month DATETIME

	SET @now = GETDATE()
	
	IF @pattern = 'o'
	BEGIN
		IF (@now >= @from_date) 
			AND (@now <= @to_date)
			AND (@max_recieved_date IS NULL OR @max_recieved_date<@from_date) 
			RETURN 1
		RETURN 0
	END
	
	IF @pattern = 'c' --Countdown
	BEGIN
		SET @tmp_first = DATEADD(n,-@number_days,@from_date)
		SET @tmp_next_month = DATEADD(n,-@number_days+@number_week,@from_date)
		
		IF @tmp_first <= @now -- @number_days as minutes before
		AND @tmp_next_month > @now -- @number_weeks as minutes to expire
		AND (@max_recieved_date IS NULL OR @tmp_first > @max_recieved_date)
			RETURN 1
		RETURN 0
	END
	ELSE IF NOT @max_recieved_date IS NULL AND DATEDIFF(d, @max_recieved_date, @now) = 0 -- already recieved/synced
		RETURN 0

	DECLARE @minutes_from_start_of_sync INT, @days_count INT, @sync_start_datetime datetime;
	SET @days_count = DATEDIFF(d, 0, @now);
	SET @sync_start_datetime = DATEADD(hour, DATEPART(hour, @from_date), DATEADD(minute, DATEPART(minute, @from_date), @days_count));
	SET @minutes_from_start_of_sync = DATEDIFF(minute, @sync_start_datetime, @now);

	--check alert time (compare only times)
	IF @minutes_from_start_of_sync >= 0
	BEGIN
		IF @end_type = 'no_date' OR
			(@end_type = 'end_by' AND DATEDIFF(d, @to_date, @now) <= 0) OR
			(@end_type = 'end_after' AND @occurrences > @occur_cnt)
		BEGIN
			RETURN dbo.isActualDayRecurrence(@pattern, @number_days, @number_week, @week_days, @month_day, @number_month, @weekday_place, @weekday_day, @month_val, @dayly_selector, @monthly_selector, @yearly_selector, @end_type, @from_date, @to_date, @now)
		END
	END
	RETURN 0
END

GO

IF EXISTS (SELECT *
		   FROM   sysobjects
		   WHERE  id = object_id('regAlerts')
				  AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	DROP PROCEDURE regAlerts
END
 
GO

IF EXISTS (SELECT *
		   FROM   sysobjects
		   WHERE  id = object_id('edirAlerts')
				  AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	DROP PROCEDURE edirAlerts
END

GO

IF EXISTS (SELECT *
		   FROM   sysobjects
		   WHERE  id = object_id('adAlerts')
				  AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	DROP PROCEDURE adAlerts
END

GO

IF EXISTS (SELECT *
		   FROM   sysobjects
		   WHERE  id = object_id('getAlerts')
				  AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	DROP PROCEDURE getAlerts
END

GO

IF EXISTS (SELECT *
		   FROM   sysobjects
		   WHERE  id = object_id('regSurveys')
				  AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  BEGIN
	  DROP PROCEDURE regSurveys
  END

GO

IF EXISTS (SELECT *
		   FROM   sysobjects
		   WHERE  id = object_id('adSurveys')
				  AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  BEGIN
	  DROP PROCEDURE adSurveys
  END

GO

IF EXISTS (SELECT *
		   FROM   sysobjects
		   WHERE  id = object_id('getSurveys')
				  AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  BEGIN
	  DROP PROCEDURE getSurveys
  END

GO

IF EXISTS (SELECT *
		   FROM   sysobjects
		   WHERE  id = object_id('FindRecurAlert')
				  AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	DROP PROCEDURE FindRecurAlert
END

GO

CREATE PROCEDURE FindRecurAlert
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @now DATETIME
	SET @now = GETDATE()
	IF OBJECT_ID('tempdb..#result') IS NOT NULL DROP TABLE #result
	CREATE TABLE #result (
		id BIGINT NOT NULL,
		[user_id] BIGINT NULL,
		[title] [nvarchar](255) NULL,
		[alert_text] [nvarchar](max) NULL,
		[is_email] [varchar](1) NULL,
		[is_sms] [varchar](1) NULL,
		[name] [nvarchar](255) NULL,
		[email] [nvarchar](255) NULL,
		[email_sender] nvarchar(255) NULL,
		[mobile_phone] [varchar](50) NULL,
		[template_id] int NULL,
		[occurrence] int NULL
	)
	INSERT INTO #result (id, user_id, title, alert_text, is_email, is_sms, name, email, email_sender, mobile_phone, template_id, occurrence)
	(
		SELECT a.id,
				u.id as user_id,
				a.title,
				a.alert_text,
				a.email as is_email,
				a.sms as is_sms,
				u.name,
				u.email,
				a.email_sender,
				u.mobile_phone,
				a.template_id,
				(SELECT COUNT(1) FROM alerts_received_recur cr WITH (READPAST) WHERE a.id = cr.alert_id AND cr.user_id = u.id) as occurrence
		FROM users u WITH (READPAST)
		INNER JOIN alerts_cache_users s WITH (READPAST)
			ON (u.id = s.user_id OR s.user_id = 0)
				AND from_date <= @now AND @now <= to_date
		INNER JOIN alerts a WITH (READPAST)
			ON a.id = s.alert_id
			AND u.reg_date < a.sent_date
			AND a.class IN (1,5,16)
			AND (a.schedule = '1' OR a.recurrence = '1')
			AND (a.sms = '1' OR a.email = '1')
			AND a.type = 'S'
		LEFT JOIN recurrence rc WITH (READPAST)
			ON rc.id = s.recur_id AND rc.alert_id = a.id
		LEFT JOIN alerts_received_recur r WITH (READPAST)
			ON a.id = r.alert_id
				AND r.user_id = u.id
				AND (r.date >= s.from_date OR (rc.end_type = 'end_after' AND rc.occurences <= r.occurrence))
		LEFT JOIN templates t WITH (READPAST)
			ON t.id=a.template_id OR (a.template_id IS NULL AND t.id=1)
		WHERE u.role = 'U' AND ((a.sms = '1' AND u.mobile_phone <> '') OR (a.email = '1' AND u.email <> ''))
			AND r.alert_id IS NULL
	)
	INSERT INTO alerts_received_recur (alert_id, user_id, date, occurrence)
		(SELECT DISTINCT id, user_id, @now, occurrence+1 FROM #result)
	SELECT DISTINCT r.id,
		r.title,
		r.alert_text,
		r.is_email,
		r.is_sms,
		r.name,
		r.email,
		r.email_sender,
		r.mobile_phone,
		t.[top],
		t.bottom
	FROM #result r
	LEFT JOIN templates t WITH (READPAST)
		ON t.id=r.template_id OR (r.template_id IS NULL AND t.id=1)
	DROP TABLE #result
END

--functions
GO

IF EXISTS (SELECT *
		   FROM   sysobjects
		   WHERE  id = object_id('udf_StripHTML')
				  AND OBJECTPROPERTY(id, N'IsScalarFunction') = 1)
BEGIN
	DROP FUNCTION udf_StripHTML
END

GO

CREATE FUNCTION [dbo].[udf_StripHTML](@HTMLText VARCHAR(MAX))
RETURNS VARCHAR(MAX)
AS
  BEGIN
	  DECLARE @Start INT
	  DECLARE @End INT
	  DECLARE @Length INT

	  SET @Start = CHARINDEX('<', @HTMLText)
	  SET @End = CHARINDEX('>', @HTMLText, CHARINDEX('<', @HTMLText))
	  SET @Length = ( @End - @Start ) + 1

	  WHILE @Start > 0
			AND @End > 0
			AND @Length > 0
		BEGIN
			SET @HTMLText = STUFF(@HTMLText, @Start, @Length, '')
			SET @Start = CHARINDEX('<', @HTMLText)
			SET @End = CHARINDEX('>', @HTMLText, CHARINDEX('<', @HTMLText))
			SET @Length = ( @End - @Start ) + 1
		END

	  RETURN LTRIM(RTRIM(@HTMLText))
  END

GO

IF EXISTS (SELECT *
		   FROM   sysobjects
		   WHERE  id = object_id('getChildOUs')
				  AND OBJECTPROPERTY(id, N'IsTableFunction') = 1)
  BEGIN
	  DROP FUNCTION getChildOUs
  END

GO

CREATE FUNCTION [dbo].[getChildOUs](@ou_id BIGINT)
RETURNS @rtnTable TABLE(id BIGINT NOT NULL)
AS  
BEGIN
	WITH OUs AS(
		SELECT @ou_id AS id
		UNION ALL
		SELECT Id_child
		FROM OU_hierarchy h
		INNER JOIN OUs ON OUs.id = h.Id_parent
	)
	INSERT INTO @rtnTable (id) (SELECT id FROM OUs) Option (MaxRecursion 10000)
	RETURN
END

--update titles
GO

UPDATE alerts
SET    title = SUBSTRING(dbo.udf_StripHTML(alert_text), 0, 50)
WHERE  title = ''
		OR title IS NULL

-- new directory import

GO

IF EXISTS (SELECT *
		   FROM   sysobjects
		   WHERE  id = object_id('addUserToGroup')
				  AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	DROP PROCEDURE addUserToGroup
END

GO

CREATE PROCEDURE addUserToGroup(@group_id BIGINT,
								@user_id BIGINT)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @id BIGINT
	SELECT @id=id FROM users_groups WHERE group_id=@group_id AND user_id=@user_id
	IF NOT @id IS NULL
		UPDATE users_groups SET was_checked=1 WHERE id=@id
	ELSE
		INSERT INTO users_groups (group_id, user_id, was_checked) VALUES (@group_id, @user_id, 1)
END

GO

IF EXISTS (SELECT *
		   FROM   sysobjects
		   WHERE  id = object_id('addCompToGroup')
				  AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	DROP PROCEDURE addCompToGroup
END

GO

CREATE PROCEDURE addCompToGroup(@group_id BIGINT,
								@comp_id BIGINT)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @id BIGINT
	SELECT @id=id FROM computers_groups WHERE group_id=@group_id AND comp_id=@comp_id
	IF NOT @id IS NULL
		UPDATE computers_groups SET was_checked=1 WHERE id=@id
	ELSE
		INSERT INTO computers_groups (group_id, comp_id, was_checked) VALUES (@group_id, @comp_id, 1)
END

GO

IF EXISTS (SELECT *
		   FROM   sysobjects
		   WHERE  id = object_id('addGroupToGroup')
				  AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	DROP PROCEDURE addGroupToGroup
END

GO

CREATE PROCEDURE addGroupToGroup(@container_group_id BIGINT,
								@group_id BIGINT)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @id BIGINT
	SELECT @id=id FROM groups_groups WHERE container_group_id=@container_group_id AND group_id=@group_id
	IF NOT @id IS NULL
		UPDATE groups_groups SET was_checked=1 WHERE id=@id
	ELSE
		INSERT INTO groups_groups (container_group_id, group_id, was_checked) VALUES (@container_group_id, @group_id, 1)
END

GO

IF EXISTS (SELECT *
		   FROM   sysobjects
		   WHERE  id = object_id('addUser')
				  AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	DROP PROCEDURE addUser
END

GO

CREATE PROCEDURE addUser(@user_hash VARCHAR(128),
						 @user_name NVARCHAR(255),
						 @display_name NVARCHAR(255),
						 @mail NVARCHAR (255),
						 @phone VARCHAR (255),
						 @ou_id BIGINT,
						 @group_id BIGINT,
						 @domain_id INT)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @updated BIT
	SET @updated = 1
	DECLARE @user_id BIGINT
	SELECT @user_id=u.id FROM users u WHERE u.name=@user_name AND u.domain_id=@domain_id
	IF NOT @user_id IS NULL
	BEGIN
		IF NOT @ou_id IS NULL
			DELETE OU_User WHERE Id_user = @user_id AND NOT Id_OU = @ou_id
		ELSE
			DELETE OU_User WHERE Id_user = @user_id
	END
	IF NOT @user_id IS NULL
		UPDATE users SET was_checked=1, mobile_phone=@phone, email=@mail, display_name=@display_name WHERE id=@user_id
	ELSE
	BEGIN
		INSERT INTO users (name, domain_id, role, was_checked, mobile_phone, email, display_name, reg_date) VALUES (@user_name, @domain_id, 'U', 1, @phone, @mail, @display_name, GETDATE())
		SET @user_id=SCOPE_IDENTITY()
		INSERT INTO User_synch (Id_user, UID) VALUES (@user_id, @user_hash)
		SET @updated = 0
	END
	IF NOT @ou_id IS NULL
	BEGIN
		IF NOT EXISTS (SELECT 1 FROM OU_User WHERE Id_user = @user_id)
			INSERT INTO OU_User (Id_user, Id_OU) VALUES (@user_id, @ou_id)
		ELSE
			UPDATE OU_User SET Id_OU = @ou_id WHERE Id_user = @user_id
	END
	IF NOT @group_id IS NULL
		EXEC addUserToGroup @group_id, @user_id
	SELECT @user_id AS id, @updated as updated
END

GO

IF EXISTS (SELECT *
		   FROM   sysobjects
		   WHERE  id = object_id('addComp')
				  AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	DROP PROCEDURE addComp
END

GO

CREATE PROCEDURE addComp(@comp_hash VARCHAR(128),
						 @comp_name NVARCHAR(255),
						 @ou_id BIGINT,
						 @group_id BIGINT,
						 @domain_id INT)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @updated BIT
	SET @updated = 1
	DECLARE @comp_id BIGINT
	SELECT @comp_id=c.id FROM computers c WHERE c.name=@comp_name AND c.domain_id=@domain_id
	IF NOT @comp_id IS NULL
	BEGIN
		IF NOT @ou_id IS NULL
			DELETE OU_comp WHERE Id_comp = @comp_id AND NOT Id_OU = @ou_id
		ELSE
			DELETE OU_comp WHERE Id_comp = @comp_id
	END
	IF NOT @comp_id IS NULL
		UPDATE computers SET was_checked=1 WHERE id=@comp_id
	ELSE
	BEGIN
		INSERT INTO computers (name, domain_id, was_checked) VALUES (@comp_name, @domain_id, 1)
		SET @comp_id=SCOPE_IDENTITY()
		INSERT INTO Comp_synch (Id_comp, UID) VALUES (@comp_id, @comp_hash)
		SET @updated = 0
	END
	IF NOT @ou_id IS NULL
	BEGIN
		IF NOT EXISTS (SELECT 1 FROM OU_comp WHERE Id_comp = @comp_id)
			INSERT INTO OU_comp (Id_comp, Id_OU) VALUES (@comp_id, @ou_id)
		ELSE
			UPDATE OU_comp SET Id_OU = @ou_id WHERE Id_comp = @comp_id
	END
	IF NOT @group_id IS NULL
		EXEC addCompToGroup @group_id, @comp_id
	SELECT @comp_id AS id, @updated as updated
END

GO

IF EXISTS (SELECT *
		   FROM   sysobjects
		   WHERE  id = object_id('addGroup')
				  AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	DROP PROCEDURE addGroup
END

GO

CREATE PROCEDURE addGroup(@group_hash VARCHAR(128),
						  @group_name NVARCHAR(255),
						  @ou_id BIGINT,
						  @container_group_id BIGINT,
						  @domain_id INT,
						  @group_dn NVARCHAR(255) = '',
						  @group_display_name NVARCHAR(255) = '')
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @updated BIT
	SET @updated = 1
	DECLARE @group_id BIGINT
	SELECT @group_id=g.id FROM groups g WHERE g.name=@group_name AND g.domain_id=@domain_id
	IF NOT @group_id IS NULL
	BEGIN
		IF NOT @ou_id IS NULL
			DELETE OU_group WHERE Id_group = @group_id AND NOT Id_OU = @ou_id
		ELSE
			DELETE OU_group WHERE Id_group = @group_id
	END
	IF NOT @group_id IS NULL
		UPDATE groups SET 
			was_checked = 1, 
			dn = @group_dn ,
			display_name = @group_display_name
			WHERE id=@group_id
	ELSE
	BEGIN
		INSERT INTO groups (name, domain_id, was_checked, dn, display_name) VALUES (@group_name, @domain_id, 1, @group_dn, @group_display_name)
		SET @group_id=SCOPE_IDENTITY()
		INSERT INTO Group_synch (Id_group, UID) VALUES (@group_id, @group_hash)
		SET @updated = 0
	END
	IF NOT @ou_id IS NULL
	BEGIN
		IF NOT EXISTS (SELECT 1 FROM OU_group WHERE Id_group = @group_id)
			INSERT INTO OU_group (Id_group, Id_OU) VALUES (@group_id, @ou_id)
		ELSE
			UPDATE OU_group SET Id_OU = @ou_id WHERE Id_group = @group_id
	END
	IF NOT @container_group_id IS NULL
		EXEC addGroupToGroup @container_group_id, @group_id
	SELECT @group_id AS id, @updated as updated
END

GO

IF EXISTS (SELECT *
		   FROM   sysobjects
		   WHERE  id = object_id('addOU')
				  AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	DROP PROCEDURE addOU
END

GO

CREATE PROCEDURE addOU(@ou_hash VARCHAR(128),
					   @ou_name NVARCHAR(255),
					   @ou_path NVARCHAR(255),
					   @parent_ou_id BIGINT,
					   @domain_id BIGINT,
					   @was_checked BIT)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @updated BIT
	SET @updated = 1
	DECLARE @ou_id BIGINT
	SELECT @ou_id=s.Id_OU FROM OU_synch s INNER JOIN OU o ON o.Id=s.Id_OU AND o.Id_domain = @domain_id WHERE s.UID=@ou_hash
	IF @ou_id IS NULL
	BEGIN
		DELETE OU_synch WHERE UID=@ou_hash
		SELECT @ou_id=o.Id FROM OU o LEFT JOIN OU_synch s ON o.Id=s.Id_OU WHERE o.Name=@ou_name AND o.Id_domain = @domain_id AND s.Id_OU IS NULL
		IF NOT @ou_id IS NULL
		BEGIN
			DELETE FROM OU_synch WHERE Id_OU = @ou_id
			INSERT INTO OU_synch (Id_OU, UID) VALUES (@ou_id, @ou_hash)
		END
	END
	IF NOT @ou_id IS NULL
		UPDATE OU SET Was_checked=@was_checked, OU_PATH=@ou_path WHERE id=@ou_id
	ELSE
	BEGIN
		INSERT INTO OU (Name, OU_PATH, Id_domain, Was_checked) VALUES (@ou_name, @ou_path, @domain_id, @was_checked)
		SET @ou_id=SCOPE_IDENTITY()
		INSERT INTO OU_synch (Id_OU, UID) VALUES (@ou_id, @ou_hash)
		SET @updated = 0
	END
	IF @parent_ou_id IS NULL
	BEGIN
		DECLARE @cur_domain_id BIGINT
		SET @cur_domain_id = (SELECT Id_domain FROM OU_DOMAIN WHERE Id_ou = @ou_id)
		IF @cur_domain_id IS NULL
			INSERT INTO OU_DOMAIN (Id_domain, Id_ou) VALUES (@domain_id, @ou_id)
		ELSE IF NOT @cur_domain_id = @domain_id
			UPDATE OU_DOMAIN SET Id_domain = @domain_id WHERE Id_ou = @ou_id
		SET @parent_ou_id = -1
	END
	ELSE
		DELETE OU_DOMAIN WHERE Id_ou = @ou_id
	IF NOT EXISTS (SELECT 1 FROM OU_hierarchy WHERE Id_parent = @parent_ou_id AND Id_child = @ou_id)
		INSERT INTO OU_hierarchy (Id_parent, Id_child) VALUES (@parent_ou_id, @ou_id)
	DELETE OU_hierarchy WHERE NOT Id_parent = @parent_ou_id AND Id_child = @ou_id
	SELECT @ou_id AS id, @updated as updated
END

GO

IF EXISTS (SELECT *
		   FROM   sysobjects
		   WHERE  id = object_id('getAlertsFromCache')
				  AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	DROP PROCEDURE [getAlertsFromCache]
END

GO

CREATE PROCEDURE [getAlertsFromCache] (
							@user_id BIGINT,
							@user_date DATETIME,
							@comp_id BIGINT,
							@clsid   VARCHAR (255),
							@ip      BIGINT,
							@local DATETIME,
							@comp_name VARCHAR (255))
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @now DATETIME
	SET @now = @local
	DECLARE @timezone INT
	SET @timezone = DATEDIFF(Minute,@now,GETUTCDATE())
	DECLARE @device_type INT
	IF @comp_name='mobile_client' 
	SET @device_type=2
	ELSE
	SET @device_type=1
	SELECT DISTINCT a.id,
			a.aknown,
			a.autoclose,
			DATEADD(Minute, @timezone, a.create_date) AS create_date,
			DATEADD(Minute, @timezone, a.sent_date) AS sent_date,
			a.recurrence,
			a.ticker,
			a.urgent,
			a.schedule,
			a.schedule_type,
			a.fullscreen,
			a.alert_width,
			a.alert_height,
			a.title,
			a.from_date,
			a.to_date,
			a.class,
			a.desktop,
			a.resizable,
			a.self_deletable,
			ac.file_name as caption_file_name,
			ac.id as skin_id,
			rc.pattern as rec_pattern,
			sv.id as survey_id
	FROM
	(
		SELECT DISTINCT alert_id, from_date, recur_id
		FROM
		(
			SELECT ach.alert_id, ach.from_date, ach.recur_id, al.lifetime
			FROM alerts_cache_users as ach WITH (READPAST)
			INNER JOIN alerts as al ON al.id = ach.alert_id
			WHERE (user_id = @user_id OR user_id = 0)
			 AND ((al.lifetime = 1 AND ach.from_date <= GETDATE() AND GETDATE() <= ach.to_date ) 
			 OR ( (al.lifetime= 0 OR ISNULL(al.lifetime, 0) = 0) AND  ach.from_date <= @now AND @now <= ach.to_date) )
		UNION ALL
			SELECT acc.alert_id, acc.from_date, acc.recur_id, al.lifetime
			FROM alerts_cache_comps as acc WITH (READPAST)
			INNER JOIN alerts as al ON al.id = acc.alert_id
			WHERE acc.comp_id = @comp_id
			 AND ((al.lifetime = 1 AND acc.from_date <= GETDATE() AND GETDATE() <= acc.to_date ) 
			 OR ( (al.lifetime= 0 OR ISNULL(al.lifetime, 0) = 0) AND  acc.from_date <= @now AND @now <= acc.to_date) )
		UNION ALL
			SELECT aci.alert_id, aci.from_date, aci.recur_id, al.lifetime
			FROM alerts_cache_ips as aci WITH (READPAST)
			INNER JOIN alerts as al ON al.id = aci.alert_id
			WHERE aci.ip_from <= @ip
				AND aci.ip_to >= @ip
				AND ((al.lifetime = 1 AND aci.from_date <= GETDATE() AND GETDATE() <= aci.to_date ) 
				OR ( (al.lifetime= 0 OR ISNULL(al.lifetime, 0) = 0) AND  aci.from_date <= @now AND @now <= aci.to_date) )
		) sent
	) s
	INNER JOIN alerts a WITH (READPAST)
		ON a.id = s.alert_id
	LEFT JOIN recurrence rc WITH (READPAST)
		ON rc.id = s.recur_id AND rc.alert_id = a.id
	LEFT JOIN alerts_received r WITH (READPAST)
		ON a.id = r.alert_id
			AND r.user_id = @user_id
			AND (r.clsid = @clsid OR r.clsid = '' OR @clsid = '')
			AND (r.date >= s.from_date OR (rc.end_type = 'end_after' AND rc.occurences <= r.occurrence))
	LEFT JOIN alert_captions ac WITH (READPAST)
		ON ac.id = a.caption_id
	LEFT JOIN surveys_main sv WITH (READPAST)
		ON sv.sender_id = a.id
	WHERE a.sent_date >= @user_date
	AND (a.device=@device_type or a.device=3 or a.device IS Null)
		AND r.alert_id IS NULL
	ORDER BY a.urgent DESC, a.id
END

GO

IF EXISTS (SELECT 1
		   FROM   sysobjects
		   WHERE  id = object_id('isActualDayRecurrence')
				  AND OBJECTPROPERTY(id, N'IsScalarFunction') = 1)
BEGIN
	DROP FUNCTION isActualDayRecurrence
END

GO

CREATE FUNCTION [dbo].[isActualDayRecurrence](
	@pattern [varchar] (1),
	@number_days [int],
	@number_week [int],
	@week_days [int],
	@month_day [int],
	@number_month [int],
	@weekday_place [int],
	@weekday_day [int],
	@month_val [int],
	@dayly_selector [varchar] (50),
	@monthly_selector [varchar] (50),
	@yearly_selector [varchar] (50),
	@end_type [varchar] (50),
	@from_date [DATETIME],
	@to_date [DATETIME],
	@check_date [DATETIME])
RETURNS BIT
AS
BEGIN
	DECLARE @tmp_first DATETIME
	DECLARE @tmp_next_month DATETIME

	IF @end_type = 'no_date' OR
			(@end_type = 'end_by' AND DATEDIFF(d, @to_date, @check_date) <= 0) OR
			(@end_type = 'end_after')
	BEGIN
		IF @pattern = 'd' --Daily
		BEGIN
			IF @dayly_selector = 'dayly_1'
			BEGIN
				IF DATEDIFF(d, @from_date, @check_date) % @number_days = 0
					RETURN 1
			END
			ELSE --dayly_2
				RETURN 1
		END
		ELSE IF @pattern = 'w' --Weekly
		BEGIN
			-- Sun = 1, Mon = 2, Tue = 4, Wed = 8, Thu = 16, Fri = 32, Sat = 64
			IF DATEDIFF(ww, @from_date, @check_date) % @number_week = 0 AND
				NOT @week_days & POWER(2, CASE WHEN DATEPART(dw, @check_date) - DATEPART(dw, 0) + 1 = 7 THEN 0 ELSE DATEPART(dw, @check_date) - DATEPART(dw, 0) + 1 END) = 0
					RETURN 1
		END
		ELSE IF @pattern = 'm' --Monthly
		BEGIN
			IF @monthly_selector = 'monthly_1'
			BEGIN
				IF DATEDIFF(m, @from_date, @check_date) % @number_month = 0
				BEGIN
					DECLARE @tmp_day_now int
					SET @tmp_day_now = DATEPART(d, @check_date)
					IF @tmp_day_now = @month_day
						RETURN 1
					ELSE
					BEGIN
						DECLARE @tmp_last_day int
						SET @tmp_next_month = DATEADD(m, 1, @check_date)
						SET @tmp_last_day = DATEPART(d, DATEADD(d, -DATEPART(d, @tmp_next_month), @tmp_next_month))
						IF @month_day > @tmp_last_day AND @tmp_day_now = @tmp_last_day -- is last day of month
							RETURN 1
					END
				END
			END
			ELSE --monthly_2
			BEGIN
				IF DATEDIFF(m, @from_date, @check_date) % @number_month = 0
				BEGIN
					IF CASE WHEN DATEPART(dw, @check_date) - DATEPART(dw, 0) + 1 = 7 THEN 0 ELSE DATEPART(dw, @check_date) - DATEPART(dw, 0) + 1 END = @weekday_day
					BEGIN
						SET @tmp_first = DATEADD(d, -DATEPART(d, @check_date)+1, @check_date)
						IF @weekday_place = 0
						BEGIN
							SET @tmp_next_month = DATEADD(m, 1, @check_date)
							IF DATEDIFF(ww, @tmp_first, @check_date) = DATEDIFF(ww, @tmp_first, DATEADD(d, -DATEPART(d, @tmp_next_month), @tmp_next_month)) --is last week
								RETURN 1
						END
						ELSE IF DATEDIFF(ww, @tmp_first, @check_date)+1 = @weekday_place
							RETURN 1
					END
				END
			END
		END
		ELSE IF @pattern = 'y' --Yearly
		BEGIN
			IF @yearly_selector = 'yearly_1'
			BEGIN
				IF DATEPART(d, @check_date) = @month_day AND DATEPART(m, @check_date) = @month_val
					RETURN 1
			END
			ELSE --yearly_2
			BEGIN
				IF DATEPART(m, @check_date) = @month_val
				BEGIN
					IF CASE WHEN DATEPART(dw, @check_date) - DATEPART(dw, 0) + 1 = 7 THEN 0 ELSE DATEPART(dw, @check_date) - DATEPART(dw, 0) + 1 END = @weekday_day
					BEGIN
						SET @tmp_first = DATEADD(d, -DATEPART(d, @check_date)+1, @check_date)
						IF @weekday_place = 0
						BEGIN
							SET @tmp_next_month = DATEADD(m, 1, @check_date)
							IF DATEDIFF(ww, @tmp_first, @check_date) = DATEDIFF(ww, @tmp_first, DATEADD(d, -DATEPART(d, @tmp_next_month), @tmp_next_month)) --is last week
								RETURN 1
						END
						ELSE IF DATEDIFF(ww, @tmp_first, @check_date)+1 = @weekday_place
							RETURN 1
					END
				END
			END
		END
	END
	RETURN 0
END

GO

IF EXISTS (SELECT *
		   FROM   sysobjects
		   WHERE  id = object_id('insertAlertToCache')
				  AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	DROP PROCEDURE [insertAlertToCache]
END

GO

CREATE PROCEDURE [dbo].[insertAlertToCache](
	@alert_id [bigint],
	@recur_id [bigint],
	@start_date [datetime],
	@end_date [datetime],
	@sent_date [datetime],
	@type2 VARCHAR(1))
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @user_id BIGINT
	DECLARE @comp_id BIGINT
	DECLARE @ip_from BIGINT
	DECLARE @ip_to BIGINT
	/*using #tempObjectsIds cache table which is created in parent procedure*/

	TRUNCATE TABLE #tempObjectsIds
	IF @type2 = 'B'
		INSERT INTO #tempObjectsIds (id, id2) VALUES(0, 0)
	ELSE
		INSERT INTO #tempObjectsIds (id, id2)
		SELECT user_id, 0 FROM
		(
			SELECT ug.user_id
				FROM alerts_sent_group sg WITH (READPAST)
				INNER JOIN users_groups ug WITH (READPAST)
				ON ug.group_id = sg.group_id
				WHERE sg.alert_id = @alert_id
			UNION
			SELECT als.user_id
				FROM alerts_sent als WITH (READPAST)
				WHERE als.alert_id = @alert_id
			UNION
			SELECT u.Id_user
				FROM alerts_sent_ou aou WITH (READPAST)
				JOIN OU_User u WITH (READPAST)
				ON aou.ou_id = u.Id_OU
				WHERE aou.alert_id = @alert_id
		) s
		INNER JOIN users u
		ON s.user_id = u.id
		WHERE role = 'U' AND u.reg_date < @sent_date
	
	DELETE alerts_cache_users
		FROM alerts_cache_users cu
		LEFT JOIN #tempObjectsIds u
			ON cu.user_id = u.id
		WHERE cu.alert_id = @alert_id AND cu.recur_id = @recur_id AND cu.from_date = @start_date AND cu.to_date = @end_date
			AND cu.user_id IS NULL

	INSERT INTO alerts_cache_users (id, alert_id, recur_id, user_id, from_date, to_date)
		SELECT newid(), @alert_id, @recur_id, u.id, @start_date, @end_date
		FROM #tempObjectsIds u
		LEFT JOIN alerts_cache_users cu
			ON cu.user_id = u.id AND cu.from_date = @start_date AND cu.to_date = @end_date AND cu.alert_id = @alert_id AND cu.recur_id = @recur_id
		WHERE cu.user_id IS NULL

	IF NOT @type2 = 'B'
	BEGIN
		TRUNCATE TABLE #tempObjectsIds
		INSERT INTO #tempObjectsIds (id, id2)
		SELECT comp_id, 0 FROM
		(
			SELECT cg.comp_id
				FROM alerts_sent_group sg WITH (READPAST)
				INNER JOIN computers_groups cg WITH (READPAST)
				ON cg.group_id = sg.group_id
				WHERE sg.alert_id = @alert_id
			UNION 
			SELECT als.comp_id
				FROM alerts_sent_comp als WITH (READPAST)
				WHERE als.alert_id = @alert_id
			UNION 
			SELECT c.Id_comp
				FROM alerts_sent_ou aou WITH (READPAST)
				JOIN OU_comp c WITH (READPAST)
				ON aou.ou_id = c.Id_OU
				WHERE aou.alert_id = @alert_id
		) s
	
		DELETE alerts_cache_comps
			FROM alerts_cache_comps cc
			LEFT JOIN #tempObjectsIds c
				ON cc.comp_id = c.id
			WHERE cc.alert_id = @alert_id AND cc.recur_id = @recur_id AND cc.from_date = @start_date AND cc.to_date = @end_date
				AND cc.comp_id IS NULL

		INSERT INTO alerts_cache_comps (id, alert_id, recur_id, comp_id, from_date, to_date)
			SELECT newid(), @alert_id, @recur_id, c.id, @start_date, @end_date
			FROM #tempObjectsIds c
			LEFT JOIN alerts_cache_comps cc
				ON cc.comp_id = c.id AND cc.from_date = @start_date AND cc.to_date = @end_date AND cc.alert_id = @alert_id AND cc.recur_id = @recur_id
			WHERE cc.comp_id IS NULL

		TRUNCATE TABLE #tempObjectsIds
		INSERT INTO #tempObjectsIds (id, id2)
			SELECT DISTINCT rg.ip_from, rg.ip_to
			FROM alerts_sent_iprange rg WITH (READPAST)
			WHERE rg.alert_id = @alert_id

		DELETE alerts_cache_ips
			FROM alerts_cache_ips ci
			LEFT JOIN #tempObjectsIds i
				ON ci.ip_from = i.id AND ci.ip_to = i.id2
			WHERE ci.alert_id = @alert_id AND ci.recur_id = @recur_id AND ci.from_date = @start_date AND ci.to_date = @end_date
				AND i.id IS NULL

		INSERT INTO alerts_cache_ips (id, alert_id, recur_id, ip_from, ip_to, from_date, to_date)
			SELECT newid(), @alert_id, @recur_id, i.id, i.id2, @start_date, @end_date
			FROM #tempObjectsIds i
			LEFT JOIN alerts_cache_ips ci
				ON ci.ip_from = i.id AND ci.ip_to = i.id2 AND ci.from_date = @start_date AND ci.to_date = @end_date AND ci.alert_id = @alert_id AND ci.recur_id = @recur_id
			WHERE ci.ip_from IS NULL
	END
END

GO

IF EXISTS (SELECT *
		   FROM   sysobjects
		   WHERE  id = object_id('processAlerts')
				  AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	DROP PROCEDURE [processAlerts]
END

GO

CREATE PROCEDURE [dbo].[processAlerts](
	@alert_id BIGINT = NULL,
	@start_date DATETIME = NULL,
	@end_date DATETIME = NULL,
	@build_cache BIT = 0,
	@select_active BIT = 0
)
AS
BEGIN
	SET NOCOUNT ON

	IF @start_date IS NULL
		SET @start_date = GETDATE()

	IF @end_date IS NULL
		SET @end_date = DATEADD(d, 7, @start_date)

	DECLARE @build_alert_id BIGINT
	SET @build_alert_id = @alert_id

	DECLARE @recur_id BIGINT
	DECLARE @pattern VARCHAR(1)
	DECLARE @number_days INT
	DECLARE @number_week INT
	DECLARE @week_days INT
	DECLARE @month_day INT
	DECLARE @number_month INT
	DECLARE @weekday_place INT
	DECLARE @weekday_day INT
	DECLARE @month_val INT
	DECLARE @dayly_selector VARCHAR(50)
	DECLARE @monthly_selector VARCHAR(50)
	DECLARE @yearly_selector VARCHAR(50)
	DECLARE @end_type VARCHAR(50)
	DECLARE @occurences INT
	DECLARE @result BIT

	DECLARE @from_date DATETIME
	DECLARE @to_date DATETIME
	DECLARE @sent_date DATETIME
	DECLARE @type2 VARCHAR(1)
	DECLARE @schedule_type VARCHAR(1)

	DECLARE @tmp_start_date DATETIME
	DECLARE @tmp_end_date DATETIME

	DECLARE @i INT
	DECLARE @count_days INT

	DECLARE @from_time_diff INT
	DECLARE @to_time_diff INT

	IF @build_cache = 1
	BEGIN
		IF OBJECT_ID('tempdb..#tempObjectsIds') IS NOT NULL DROP TABLE #tempObjectsIds
		CREATE TABLE #tempObjectsIds (
			id BIGINT NOT NULL,
			id2 BIGINT NOT NULL,
			PRIMARY KEY CLUSTERED (id, id2)
		)
		IF OBJECT_ID('tempdb..#tempAlertsAndRecIds') IS NOT NULL DROP TABLE #tempAlertsAndRecIds
		CREATE TABLE #tempAlertsAndRecIds (
			id BIGINT NOT NULL,
			recur_id BIGINT NOT NULL,
			PRIMARY KEY CLUSTERED (id, recur_id)
		)
	END

	IF @select_active = 1
	BEGIN
		IF OBJECT_ID('tempdb..#tempAlertsIdsDates') IS NOT NULL DROP TABLE #tempAlertsIdsDates
		CREATE TABLE #tempAlertsIdsDates (
			id BIGINT NOT NULL,
			from_date DATETIME NOT NULL,
			to_date DATETIME NOT NULL
		)
	END

	IF @build_alert_id IS NULL
	BEGIN
		DECLARE alerts_cursor CURSOR FAST_FORWARD FOR
		SELECT a.id,
			r.id AS recur_id,
			a.sent_date,
			r.pattern,
			r.number_days,
			r.number_week,
			r.week_days,
			r.month_day,
			r.number_month,
			r.weekday_place,
			r.weekday_day,
			r.month_val,
			r.dayly_selector,
			r.monthly_selector,
			r.yearly_selector,
			r.end_type,
			r.occurences,
			a.from_date,
			a.to_date,
			a.type2,
			a.schedule_type
		FROM alerts a
		JOIN recurrence r
			ON a.id = r.alert_id
		WHERE type = 'S'
	END
	ELSE
	BEGIN
		DECLARE alerts_cursor CURSOR FAST_FORWARD FOR
		SELECT a.id,
			r.id AS recur_id,
			a.sent_date,
			r.pattern,
			r.number_days,
			r.number_week,
			r.week_days,
			r.month_day,
			r.number_month,
			r.weekday_place,
			r.weekday_day,
			r.month_val,
			r.dayly_selector,
			r.monthly_selector,
			r.yearly_selector,
			r.end_type,
			r.occurences,
			a.from_date,
			a.to_date,
			a.type2,
			a.schedule_type
		FROM alerts a
		LEFT JOIN recurrence r
			ON a.id = r.alert_id
		WHERE a.id = @build_alert_id
	END

	OPEN alerts_cursor
	FETCH NEXT FROM alerts_cursor INTO @alert_id, @recur_id, @sent_date, @pattern, @number_days, @number_week, @week_days, @month_day, @number_month, @weekday_place, @weekday_day, @month_val, @dayly_selector, @monthly_selector, @yearly_selector, @end_type, @occurences, @from_date, @to_date, @type2, @schedule_type

	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF @recur_id IS NULL
			SET @recur_id = 0

		SET @result = 0
		IF @schedule_type = '1'
		BEGIN
			IF @pattern = 'o' OR @pattern IS NULL --not recurrence and scheduled alert
			BEGIN
				IF @from_date IS NULL OR DATEPART(yy, @from_date) = 1900
					SET @from_date = 0

				IF @to_date IS NULL OR DATEPART(yy, @to_date) = 1900
					SET @to_date = '9999-01-01 00:00:00.000'

				IF NOT (@to_date < @start_date OR @from_date > @end_date)
					SET @result = 1

				IF @result = 1 AND @build_cache = 1
					EXEC dbo.insertAlertToCache @alert_id, @recur_id, @from_date, @to_date, @sent_date, @type2
				IF @result = 1 AND @select_active = 1
					INSERT INTO #tempAlertsIdsDates (id, from_date, to_date) VALUES (@alert_id, @from_date, @to_date)
			END
			ELSE IF @pattern = 'c' --countdown
			BEGIN
				SET @to_date = DATEADD(n, -@number_days+@number_week, @from_date)
				SET @from_date = DATEADD(n, -@number_days, @from_date)
				
				IF NOT (@to_date < @start_date OR @from_date > @end_date)
					SET @result = 1
				
				IF @result = 1 AND @build_cache = 1
					EXEC dbo.insertAlertToCache @alert_id, @recur_id, @from_date, @to_date, @sent_date, @type2
				IF @result = 1 AND @select_active = 1
					INSERT INTO #tempAlertsIdsDates (id, from_date, to_date) VALUES (@alert_id, @from_date, @to_date)
			END
			ELSE
			BEGIN
				IF @start_date < @from_date
					SET @tmp_start_date = @from_date
				ELSE
					SET @tmp_start_date = @start_date

				SET @i = 0
				SET @count_days = DATEDIFF(d, @tmp_start_date, @end_date) --calc num of days in checking period

				WHILE (@i <= @count_days)
				BEGIN
					IF (@i = @count_days)
						SET @tmp_end_date = @end_date
					ELSE
						SET @tmp_end_date = DATEADD(dd, 0, DATEDIFF(dd, 0, @tmp_start_date)+0.99999999) --set end of date
					
					SET @from_time_diff = DATEDIFF(s, DATEADD(d,DATEDIFF(d, @tmp_start_date, @from_date), @tmp_start_date), @from_date) --get alert start time of day
					SET @to_time_diff = DATEDIFF(s, DATEADD(d,DATEDIFF(d, @tmp_start_date, @to_date), @tmp_start_date), @to_date) --get alert end time of day
					
					SET @tmp_end_date = DATEADD(s,@to_time_diff, @tmp_start_date) --add end time of day
					SET @tmp_start_date = DATEADD(s,@from_time_diff, @tmp_start_date) --add start time of day

					IF NOT DATEDIFF(s, DATEADD(d, DATEDIFF(d, @tmp_start_date, @to_date), @tmp_start_date), @to_date) < 0 AND
						NOT DATEDIFF(s, DATEADD(d, DATEDIFF(d, @tmp_end_date, @from_date), @tmp_end_date), @from_date) > 0
					BEGIN
						IF 1 = dbo.isActualDayRecurrence(@pattern,@number_days,@number_week,@week_days,@month_day,@number_month,@weekday_place,@weekday_day,@month_val,@dayly_selector,@monthly_selector,@yearly_selector ,@end_type,@from_date,@to_date,@tmp_start_date)
						BEGIN
							IF @build_cache = 1
								EXEC dbo.insertAlertToCache @alert_id, @recur_id, @tmp_start_date, @tmp_end_date, @sent_date, @type2
							IF @select_active = 1
								INSERT INTO #tempAlertsIdsDates (id, from_date, to_date) VALUES (@alert_id, @tmp_start_date, @tmp_end_date)
							SET @result = 1
						END
					END 

					SET @tmp_start_date = DATEADD(dd, 0, DATEDIFF(dd, 0, @tmp_start_date)+1) --next day
					SET @i = @i+1
				END
			END
		END
		IF @result = 0 AND @build_cache = 1
			INSERT INTO #tempAlertsAndRecIds (id, recur_id) VALUES (@alert_id, @recur_id) --alert id to delete from cache
		FETCH NEXT FROM alerts_cursor INTO @alert_id, @recur_id, @sent_date, @pattern, @number_days, @number_week, @week_days, @month_day, @number_month, @weekday_place, @weekday_day, @month_val, @dayly_selector, @monthly_selector, @yearly_selector, @end_type, @occurences, @from_date, @to_date, @type2, @schedule_type
	END

	CLOSE alerts_cursor
	DEALLOCATE alerts_cursor
	
	IF @build_cache = 1
	BEGIN
		DROP TABLE #tempObjectsIds

		DELETE alerts_cache_users
		FROM alerts_cache_users cu
			INNER JOIN #tempAlertsAndRecIds a
				ON a.id = cu.alert_id AND a.recur_id = cu.recur_id

		DELETE alerts_cache_comps
		FROM alerts_cache_comps cc
			INNER JOIN #tempAlertsAndRecIds a
				ON a.id = cc.alert_id AND a.recur_id = cc.recur_id

		DELETE alerts_cache_ips
		FROM alerts_cache_ips ci
			INNER JOIN #tempAlertsAndRecIds a
				ON a.id = ci.alert_id AND a.recur_id = ci.recur_id

		DROP TABLE #tempAlertsAndRecIds
	END

	IF @select_active = 1
	BEGIN
		SELECT DISTINCT t.id,
			a.title,
			a.class,
			a.urgent,
			a.ticker,
			a.fullscreen,
			a.sent_date,
			a.sender_id,
			t.from_date,
			t.to_date,
			s.id AS survey_id
		FROM #tempAlertsIdsDates t
		INNER JOIN alerts a
		ON a.id = t.id
		LEFT JOIN surveys_main s
		ON s.closed = 'A' and s.sender_id = a.id
		ORDER BY t.from_date ASC, t.id ASC
		DROP TABLE #tempAlertsIdsDates
	END
END

GO

IF EXISTS (SELECT *
		   FROM   sysobjects
		   WHERE  id = object_id('buildAlertsCacheForAlert')
				  AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	DROP PROCEDURE [buildAlertsCacheForAlert]
END

GO

CREATE PROCEDURE [dbo].[buildAlertsCacheForAlert](@alert_id BIGINT = NULL)
AS
BEGIN
	DECLARE @approved INT;

	SELECT  @approved = (SELECT alerts.approve_status FROM alerts WHERE id = @alert_id );

	IF @approved = 1  OR @approved IS NULL
	BEGIN
		EXEC dbo.processAlerts @alert_id, @build_cache = 1
	END
END

GO

IF EXISTS (SELECT *
		   FROM   sysobjects
		   WHERE  id = object_id('buildAlertsCache')
				  AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	DROP PROCEDURE [buildAlertsCache]
END

GO

CREATE PROCEDURE [dbo].[buildAlertsCache]
AS
BEGIN
	EXEC dbo.buildAlertsCacheForAlert @alert_id = NULL
END

GO

IF EXISTS (SELECT *
		   FROM   sysobjects
		   WHERE  id = object_id('buildAlertsCacheForUser')
				  AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	DROP PROCEDURE [buildAlertsCacheForUser]
END

GO

CREATE PROCEDURE [dbo].[buildAlertsCacheForUser](@user_id BIGINT)
AS
BEGIN
	EXEC dbo.buildAlertsCache
END

GO

IF EXISTS (SELECT *
		   FROM   sysobjects
		   WHERE  id = object_id('buildAlertsCacheForComp')
				  AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	DROP PROCEDURE [buildAlertsCacheForComp]
END

GO

CREATE PROCEDURE [dbo].[buildAlertsCacheForComp](@comp_id BIGINT)
AS
BEGIN
	EXEC dbo.buildAlertsCache
END

GO

IF EXISTS (SELECT *
		   FROM   sysobjects
		   WHERE  id = object_id('buildAlertsCacheForIp')
				  AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	DROP PROCEDURE [buildAlertsCacheForIp]
END

GO

CREATE PROCEDURE [dbo].[buildAlertsCacheForIp](@ip_from BIGINT, @ip_to BIGINT)
AS
BEGIN
	EXEC dbo.buildAlertsCache
END

GO

IF EXISTS (SELECT *
		   FROM   sysobjects
		   WHERE  id = object_id('buildAlertsCacheForGroup')
				  AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	DROP PROCEDURE [buildAlertsCacheForGroup]
END

GO

CREATE PROCEDURE [dbo].[buildAlertsCacheForGroup](@group_id BIGINT)
AS
BEGIN
	EXEC dbo.buildAlertsCache
END

GO

IF EXISTS (SELECT *
		   FROM   sysobjects
		   WHERE  id = object_id('buildAlertsCacheForOU')
				  AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	DROP PROCEDURE [buildAlertsCacheForOU]
END

GO

CREATE PROCEDURE [dbo].[buildAlertsCacheForOU](@ou_id BIGINT)
AS
BEGIN
	EXEC dbo.buildAlertsCache
END

GO 
IF EXISTS (SELECT *
		   FROM   sysobjects
		   WHERE  id = object_id('getNearestHourAlerts')
				  AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
DROP PROCEDURE [dbo].[getNearestHourAlerts]
END
GO


CREATE PROCEDURE [dbo].[getNearestHourAlerts](
	@start_date DATETIME,
	@recipients VARCHAR(200),
	@terminated INT
)
AS
BEGIN	
	SET NOCOUNT ON

	DECLARE @select_active BIT
	DECLARE @end_date DATETIME
	SET @select_active = 1

	--IF @start_date IS NULL
		SET @start_date = DATEADD(day, DATEDIFF(day, 0, GETDATE()), 0)

	--IF @end_date IS NULL
		SET @end_date =  DATEADD(second,  -1, DATEADD(day, 1, @start_date))



	DECLARE @recur_id BIGINT
	DECLARE @pattern VARCHAR(1)
	DECLARE @number_days INT
	DECLARE @number_week INT
	DECLARE @week_days INT
	DECLARE @month_day INT
	DECLARE @number_month INT
	DECLARE @weekday_place INT
	DECLARE @weekday_day INT
	DECLARE @month_val INT
	DECLARE @dayly_selector VARCHAR(50)
	DECLARE @monthly_selector VARCHAR(50)
	DECLARE @yearly_selector VARCHAR(50)
	DECLARE @end_type VARCHAR(50)
	DECLARE @occurences INT
	DECLARE @result BIT

	DECLARE @from_date DATETIME
	DECLARE @to_date DATETIME
	DECLARE @sent_date DATETIME
	DECLARE @type2 VARCHAR(1)
	DECLARE @schedule_type VARCHAR(1)

	DECLARE @tmp_start_date DATETIME
	DECLARE @tmp_end_date DATETIME

	DECLARE @i INT
	DECLARE @count_days INT

	DECLARE @from_time_diff INT
	DECLARE @to_time_diff INT



	IF @select_active = 1
	BEGIN
		IF OBJECT_ID('tempdb..#tempAlertsIdsDates') IS NOT NULL DROP TABLE #tempAlertsIdsDates
		CREATE TABLE #tempAlertsIdsDates (
			id BIGINT NOT NULL,
			from_date DATETIME NOT NULL,
			to_date DATETIME NOT NULL,
			recur_id INT NOT NULL
		)
	END


		DECLARE alerts_cursor CURSOR FAST_FORWARD FOR
		SELECT a.id,
			r.id AS recur_id,
			a.sent_date,
			r.pattern,
			r.number_days,
			r.number_week,
			r.week_days,
			r.month_day,
			r.number_month,
			r.weekday_place,
			r.weekday_day,
			r.month_val,
			r.dayly_selector,
			r.monthly_selector,
			r.yearly_selector,
			r.end_type,
			r.occurences,
			a.from_date,
			a.to_date,
			a.type2,
			a.schedule_type
		FROM alerts a
		JOIN recurrence r
			ON a.id = r.alert_id
		WHERE type = 'S'


	DECLARE @alert_id BIGINT
	OPEN alerts_cursor
	FETCH NEXT FROM alerts_cursor INTO @alert_id, @recur_id, @sent_date, @pattern, @number_days, @number_week, @week_days, @month_day, @number_month, @weekday_place, @weekday_day, @month_val, @dayly_selector, @monthly_selector, @yearly_selector, @end_type, @occurences, @from_date, @to_date, @type2, @schedule_type

	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF @recur_id IS NULL
			SET @recur_id = 0

		SET @result = 0
		IF @schedule_type = '1'
		BEGIN
			IF @pattern = 'o' OR @pattern IS NULL --not recurrence and scheduled alert
			BEGIN
				IF @from_date IS NULL OR DATEPART(yy, @from_date) = 1900
					SET @from_date = 0

				IF @to_date IS NULL OR DATEPART(yy, @to_date) = 1900
					SET @to_date = '9999-01-01 00:00:00.000'

				IF NOT (@to_date < @start_date OR @from_date > @end_date)
					SET @result = 1

				IF @result = 1 AND @select_active = 1
					INSERT INTO #tempAlertsIdsDates (id, from_date, to_date, recur_id) VALUES (@alert_id, @from_date, @to_date, @recur_id)
			END
			ELSE IF @pattern = 'c' --countdown
			BEGIN
				SET @to_date = DATEADD(n, -@number_days+@number_week, @from_date)
				SET @from_date = DATEADD(n, -@number_days, @from_date)
				
				IF NOT (@to_date < @start_date OR @from_date > @end_date)
					SET @result = 1
				
			
				IF @result = 1 AND @select_active = 1
					INSERT INTO #tempAlertsIdsDates (id, from_date, to_date, recur_id) VALUES (@alert_id, @from_date, @to_date, @recur_id)
			END
			ELSE
			BEGIN
				IF @start_date < @from_date
					SET @tmp_start_date = @from_date
				ELSE
					SET @tmp_start_date = @start_date

				SET @i = 0
				SET @count_days = DATEDIFF(d, @tmp_start_date, @end_date) --calc num of days in checking period

				WHILE (@i <= @count_days)
				BEGIN
					IF (@i = @count_days)
						SET @tmp_end_date = @end_date
					ELSE
						SET @tmp_end_date = DATEADD(dd, 0, DATEDIFF(dd, 0, @tmp_start_date)+0.99999999) --set end of date
					
					SET @from_time_diff = DATEDIFF(s, DATEADD(d,DATEDIFF(d, @tmp_start_date, @from_date), @tmp_start_date), @from_date) --get alert start time of day
					SET @to_time_diff = DATEDIFF(s, DATEADD(d,DATEDIFF(d, @tmp_start_date, @to_date), @tmp_start_date), @to_date) --get alert end time of day
					
					SET @tmp_end_date = DATEADD(s,@to_time_diff, @tmp_start_date) --add end time of day
					SET @tmp_start_date = DATEADD(s,@from_time_diff, @tmp_start_date) --add start time of day

					IF NOT DATEDIFF(s, DATEADD(d, DATEDIFF(d, @tmp_start_date, @to_date), @tmp_start_date), @to_date) < 0 AND
						NOT DATEDIFF(s, DATEADD(d, DATEDIFF(d, @tmp_end_date, @from_date), @tmp_end_date), @from_date) > 0
					BEGIN
						IF 1 = dbo.isActualDayRecurrence(@pattern,@number_days,@number_week,@week_days,@month_day,@number_month,@weekday_place,@weekday_day,@month_val,@dayly_selector,@monthly_selector,@yearly_selector ,@end_type,@from_date,@to_date,@tmp_start_date)
						BEGIN
							IF @select_active = 1
								INSERT INTO #tempAlertsIdsDates (id, from_date, to_date, recur_id) VALUES (@alert_id, @tmp_start_date, @tmp_end_date, @recur_id)
							SET @result = 1
						END
					END 

					SET @tmp_start_date = DATEADD(dd, 0, DATEDIFF(dd, 0, @tmp_start_date)+1) --next day
					SET @i = @i+1
				END
			END
		END
		
		FETCH NEXT FROM alerts_cursor INTO @alert_id, @recur_id, @sent_date, @pattern, @number_days, @number_week, @week_days, @month_day, @number_month, @weekday_place, @weekday_day, @month_val, @dayly_selector, @monthly_selector, @yearly_selector, @end_type, @occurences, @from_date, @to_date, @type2, @schedule_type
	END

	CLOSE alerts_cursor
	DEALLOCATE alerts_cursor

	IF @select_active = 1
	BEGIN
		IF @terminated = 1
		BEGIN
			IF @recipients = 'users'
			BEGIN
				SELECT  t.id,
					a.title,
					a.type2,
					a.class,
					a.urgent,
					a.ticker,
					a.device,
					a.fullscreen,
					a.ticker_speed as tickerspeed,
					a.sent_date as sentdate,
					a.sender_id as senderid,
					a.aknown,
					a.lifetime,
					case  when cast(a.caption_id as VARCHAR(36)) IS NULL THEN 'default'
					else cast(a.caption_id as VARCHAR(36))  end as captionid,
					a.alert_width as width,
					a.alert_height as height,
					a.resizable,
					a.autoclose,
					a.self_deletable as selfdeletable,
					a.create_date as createdate,
					a.schedule,
					a.schedule_type,
					a.email,
					a.sms,
					a.text_to_call AS texttocall,
					a.approve_status,
					t.from_date as fromdate,
					t.to_date as todate,
					s.id AS surveyid,
					--hack
					case  when a.type2 = 'B' THEN 'broadcast'
					else u.name  end as username,
					--u.name as username,
					d.name as user_domain,
					ui.clsid as deskbar,
					a.campaign_id AS campaignid
				FROM #tempAlertsIdsDates t
				INNER JOIN alerts a
				ON a.id = t.id
				AND a.terminated = 1
				LEFT JOIN surveys_main s
				ON  s.sender_id = a.id
				--hack
				left JOIN
				(alerts_sent INNER JOIN 
					((users as u INNER JOIN [domains] as d ON u.domain_id = d.id OR u.domain_id IS NULL) 
						JOIN user_instances as ui ON ui.user_id = u.id) 
					ON u.id = alerts_sent.user_id) 
				ON alerts_sent.alert_id = a.id
				LEFT JOIN recurrence rc WITH (READPAST)
					ON rc.id = t.recur_id AND rc.alert_id = a.id
				LEFT JOIN alerts_received r WITH (READPAST)
					ON a.id = r.alert_id
					AND r.user_id = u.id
					AND (r.clsid = u.deskbar_id OR r.clsid = '' OR u.deskbar_id = '')
					AND (r.date >= t.from_date OR (rc.end_type = 'end_after' AND (rc.occurences <= r.occurrence OR rc.occurences IS NULL)))
				WHERE  
				(a.approve_status = 1 OR a.approve_status IS NULL)
				AND a.schedule = 1 
				AND a.class != 4					
				OR (a.class = 2 AND a.schedule_type = 1)
				OR (a.class = 8 AND a.schedule_type = 1)
		  
				ORDER BY t.from_date ASC, t.id ASC
				DROP TABLE #tempAlertsIdsDates
			END

			IF @recipients = 'computers'
			BEGIN
				SELECT  t.id,
					a.title,
					a.type2,
					a.class,
					a.urgent,
					a.ticker,
					a.device,
					a.fullscreen,
					a.ticker_speed as tickerspeed,
					a.sent_date as sentdate,
					a.sender_id as senderid,
					a.aknown,
					a.lifetime,
					case  when cast(a.caption_id as VARCHAR(36)) IS NULL THEN 'default'
					else cast(a.caption_id as VARCHAR(36))  end as captionid,
					a.alert_width as width,
					a.alert_height as height,
					a.resizable,
					a.self_deletable as selfdeletable,
					a.autoclose,
					a.create_date as createdate,
					a.schedule,
					a.schedule_type,
					a.email,
					a.sms,
					a.text_to_call AS texttocall,
					a.approve_status,
					t.from_date as fromdate,
					t.to_date as todate,
					s.id AS surveyid,
					p.name as comp_name,
					d.name as comp_domain,
					a.campaign_id AS campaignid
				FROM #tempAlertsIdsDates t
				INNER JOIN alerts a
				ON a.id = t.id
				AND a.terminated = 1
				LEFT JOIN surveys_main s
				ON  s.sender_id = a.id
				INNER JOIN alerts_sent_comp ON alerts_sent_comp.alert_id = a.id
				INNER JOIN computers as p ON p.id = alerts_sent_comp.comp_id
				LEFT JOIN [domains] as d ON p.domain_id = d.id -- OR p.domain_id IS NULL
				WHERE a.class != 4 AND ( a.approve_status = 1 OR a.approve_status IS NULL)
				ORDER BY t.from_date ASC, t.id ASC
				DROP TABLE #tempAlertsIdsDates
			END

			IF @recipients = 'ips'
			BEGIN
				SELECT  t.id,
					a.title,
					a.type2,
					a.class,
					a.urgent,
					a.device,
					a.ticker,
					a.fullscreen,
					a.lifetime,
					a.ticker_speed as tickerspeed,
					case  when cast(a.caption_id as VARCHAR(36)) IS NULL THEN 'default'
					else cast(a.caption_id as VARCHAR(36))  end as captionid,
					a.sent_date as sentdate,
					a.sender_id as senderid,
					a.aknown,
					a.self_deletable as selfdeletable,
					a.alert_width as width,
					a.alert_height as height,
					a.resizable,
					a.autoclose,
					a.create_date as createdate,
					a.schedule,
					a.schedule_type,
					a.email,
					a.sms,
					a.text_to_call AS texttocall,
					a.approve_status,
					t.from_date as fromdate,
					t.to_date as todate,
					s.id AS surveyid,
					ip.ip_from,
					ip.ip_to,
					a.campaign_id AS campaignid
				FROM #tempAlertsIdsDates t
				INNER JOIN alerts a
				ON a.id = t.id
				AND a.terminated = 1
				LEFT JOIN surveys_main s
				ON s.closed = 'A' and s.sender_id = a.id
				INNER JOIN alerts_sent_iprange  as ip ON ip.alert_id = a.id
				WHERE a.class != 4 AND ( a.approve_status = 1 OR a.approve_status IS NULL)
				ORDER BY t.from_date ASC, t.id ASC
				DROP TABLE #tempAlertsIdsDates
			END
		END
		ELSE
		BEGIN
			IF @recipients = 'users'
			BEGIN
				SELECT  t.id,
					a.title,
					a.type2,
					a.class,
					a.urgent,
					a.ticker,
					a.ticker_speed as tickerspeed,
					a.device,
					a.fullscreen,
					a.sent_date as sentdate,
					a.sender_id as senderid,
					a.aknown,
					a.lifetime,
					case  when cast(a.caption_id as VARCHAR(36)) IS NULL THEN 'default'
					else cast(a.caption_id as VARCHAR(36))  end as captionid,
					a.alert_width as width,
					a.alert_height as height,
					a.resizable,
					a.autoclose,
					a.self_deletable as selfdeletable,
					a.create_date as createdate,
					a.schedule,
					a.schedule_type,
					a.email,
					a.sms,
					a.text_to_call AS texttocall,
					a.approve_status,
					t.from_date as fromdate,
					t.to_date as todate,
					s.id AS surveyid,
					--hack
					case  when a.type2 = 'B' THEN 'broadcast'
					else u.name  end as username,
					--u.name as username,
					d.name as user_domain,
					ui.clsid as deskbar,
					a.campaign_id AS campaignid
				FROM #tempAlertsIdsDates t
				INNER JOIN alerts a
				ON a.id = t.id
				LEFT JOIN surveys_main s
				ON  s.sender_id = a.id
				--hack
				left JOIN
				(alerts_sent INNER JOIN 
					((users as u INNER JOIN [domains] as d ON u.domain_id = d.id OR u.domain_id IS NULL) 
						JOIN user_instances as ui ON ui.user_id = u.id) 
					ON u.id = alerts_sent.user_id) 
				ON alerts_sent.alert_id = a.id
				LEFT JOIN recurrence rc WITH (READPAST)
					ON rc.id = t.recur_id AND rc.alert_id = a.id
				LEFT JOIN alerts_received r WITH (READPAST)
					ON a.id = r.alert_id
					AND r.user_id = u.id
					AND (r.clsid = u.deskbar_id OR r.clsid = '' OR u.deskbar_id = '')
					AND (r.date >= t.from_date OR (rc.end_type = 'end_after' AND (rc.occurences <= r.occurrence OR rc.occurences IS NULL)))
				WHERE  
				(a.approve_status = 1 OR a.approve_status IS NULL)
				AND a.schedule=1
				AND a.class != 4				
				OR (a.class = 2 AND a.schedule_type=1)
				OR (a.class = 8 AND a.schedule_type=1)

				ORDER BY t.from_date ASC, t.id ASC
				DROP TABLE #tempAlertsIdsDates
			END

			IF @recipients = 'computers'
			BEGIN
				SELECT  t.id,
					a.title,
					a.type2,
					a.class,
					a.urgent,
					a.ticker,
					a.ticker_speed AS tickerspeed,
					a.device,
					a.fullscreen,
					a.sent_date as sentdate,
					a.sender_id as senderid,
					a.aknown,
					a.lifetime,
					case  when cast(a.caption_id as VARCHAR(36)) IS NULL THEN 'default'
					else cast(a.caption_id as VARCHAR(36))  end as captionid,
					a.alert_width as width,
					a.alert_height as height,
					a.resizable,
					a.self_deletable as selfdeletable,
					a.autoclose,
					a.create_date as createdate,
					a.schedule,
					a.schedule_type,
					a.email,
					a.sms,
					a.text_to_call AS texttocall,
					a.approve_status,
					t.from_date as fromdate,
					t.to_date as todate,
					s.id AS surveyid,
					p.name as comp_name,
					d.name as comp_domain,
					a.campaign_id AS campaignid
				FROM #tempAlertsIdsDates t
				INNER JOIN alerts a
				ON a.id = t.id
				LEFT JOIN surveys_main s
				ON  s.sender_id = a.id
				INNER JOIN alerts_sent_comp ON alerts_sent_comp.alert_id = a.id
				INNER JOIN computers as p ON p.id = alerts_sent_comp.comp_id
				LEFT JOIN [domains] as d ON p.domain_id = d.id -- OR p.domain_id IS NULL
				WHERE a.class != 4 AND ( a.approve_status = 1 OR a.approve_status IS NULL)
				ORDER BY t.from_date ASC, t.id ASC
				DROP TABLE #tempAlertsIdsDates
			END

			IF @recipients = 'ips'
			BEGIN
				SELECT  t.id,
					a.title,
					a.type2,
					a.class,
					a.urgent,
					a.device,
					a.ticker,
					a.ticker_speed AS tickerspeed,
					a.fullscreen,
					a.lifetime,
					case  when cast(a.caption_id as VARCHAR(36)) IS NULL THEN 'default'
					else cast(a.caption_id as VARCHAR(36))  end as captionid,
					a.sent_date as sentdate,
					a.sender_id as senderid,
					a.aknown,
					a.self_deletable as selfdeletable,
					a.alert_width as width,
					a.alert_height as height,
					a.resizable,
					a.autoclose,
					a.create_date as createdate,
					a.schedule,
					a.schedule_type,
					a.email,
					a.sms,
					a.text_to_call AS texttocall,
					a.approve_status,
					t.from_date as fromdate,
					t.to_date as todate,
					s.id AS surveyid,
					ip.ip_from,
					ip.ip_to,
					a.campaign_id AS campaignid
				FROM #tempAlertsIdsDates t
				INNER JOIN alerts a
				ON a.id = t.id
				LEFT JOIN surveys_main s
				ON s.closed = 'A' and s.sender_id = a.id
				INNER JOIN alerts_sent_iprange  as ip ON ip.alert_id = a.id
				WHERE a.class != 4 AND ( a.approve_status = 1 OR a.approve_status IS NULL)
				ORDER BY t.from_date ASC, t.id ASC
				DROP TABLE #tempAlertsIdsDates
			END
		END
	END
END

GO

IF EXISTS (SELECT *
		   FROM   sysobjects
		   WHERE  id = object_id('GetAlertData')
				  AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
DROP PROCEDURE [dbo].[GetAlertData]
END
GO

CREATE PROC [dbo].[GetAlertData]
(
	@start_date DATETIME,
	@alert_id INT,
	@recipients VARCHAR(200)
)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @select_active BIT
	DECLARE @end_date DATETIME
	SET @select_active = 1

	--IF @start_date IS NULL
		SET @start_date = DATEADD(day, DATEDIFF(day, 0, GETDATE()), 0)

	--IF @end_date IS NULL
		SET @end_date =  DATEADD(second,  -1, DATEADD(day, 1, @start_date))



	DECLARE @recur_id BIGINT
	DECLARE @pattern VARCHAR(1)
	DECLARE @number_days INT
	DECLARE @number_week INT
	DECLARE @week_days INT
	DECLARE @month_day INT
	DECLARE @number_month INT
	DECLARE @weekday_place INT
	DECLARE @weekday_day INT
	DECLARE @month_val INT
	DECLARE @dayly_selector VARCHAR(50)
	DECLARE @monthly_selector VARCHAR(50)
	DECLARE @yearly_selector VARCHAR(50)
	DECLARE @end_type VARCHAR(50)
	DECLARE @occurences INT
	DECLARE @result BIT

	DECLARE @from_date DATETIME
	DECLARE @to_date DATETIME
	DECLARE @sent_date DATETIME
	DECLARE @type2 VARCHAR(1)
	DECLARE @schedule_type VARCHAR(1)

	DECLARE @tmp_start_date DATETIME
	DECLARE @tmp_end_date DATETIME

	DECLARE @i INT
	DECLARE @count_days INT

	DECLARE @from_time_diff INT
	DECLARE @to_time_diff INT



	IF @select_active = 1
	BEGIN
		IF OBJECT_ID('tempdb..#tempAlertsIdsDates') IS NOT NULL DROP TABLE #tempAlertsIdsDates
		CREATE TABLE #tempAlertsIdsDates (
			id BIGINT NOT NULL,
			from_date DATETIME NOT NULL,
			to_date DATETIME NOT NULL,
			recur_id INT NOT NULL
		)
	END


		DECLARE alerts_cursor CURSOR FAST_FORWARD FOR
		SELECT a.id,
			r.id AS recur_id,
			a.sent_date,
			r.pattern,
			r.number_days,
			r.number_week,
			r.week_days,
			r.month_day,
			r.number_month,
			r.weekday_place,
			r.weekday_day,
			r.month_val,
			r.dayly_selector,
			r.monthly_selector,
			r.yearly_selector,
			r.end_type,
			r.occurences,
			a.from_date,
			a.to_date,
			a.type2,
			a.schedule_type
		FROM alerts a
		LEFT JOIN recurrence r
			ON a.id = r.alert_id
		WHERE a.id = @alert_id 


	--DECLARE @alert_id BIGINT
	OPEN alerts_cursor
	FETCH NEXT FROM alerts_cursor INTO @alert_id, @recur_id, @sent_date, @pattern, @number_days, @number_week, @week_days, @month_day, @number_month, @weekday_place, @weekday_day, @month_val, @dayly_selector, @monthly_selector, @yearly_selector, @end_type, @occurences, @from_date, @to_date, @type2, @schedule_type

	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF @recur_id IS NULL
			SET @recur_id = 0

		SET @result = 0
		IF @schedule_type = '1'
		BEGIN
			IF @pattern = 'o' OR @pattern IS NULL --not recurrence and scheduled alert
			BEGIN
				IF @from_date IS NULL OR DATEPART(yy, @from_date) = 1900
					SET @from_date = 0

				IF @to_date IS NULL OR DATEPART(yy, @to_date) = 1900
					SET @to_date = '9999-01-01 00:00:00.000'

				IF NOT (@to_date < @start_date OR @from_date > @end_date)
					SET @result = 1

				IF @result = 1 AND @select_active = 1
					INSERT INTO #tempAlertsIdsDates (id, from_date, to_date, recur_id) VALUES (@alert_id, @from_date, @to_date, @recur_id)
			END
			ELSE IF @pattern = 'c' --countdown
			BEGIN
				SET @to_date = DATEADD(n, -@number_days+@number_week, @from_date)
				SET @from_date = DATEADD(n, -@number_days, @from_date)
				
				IF NOT (@to_date < @start_date OR @from_date > @end_date)
					SET @result = 1
				
			
				IF @result = 1 AND @select_active = 1
					INSERT INTO #tempAlertsIdsDates (id, from_date, to_date, recur_id) VALUES (@alert_id, @from_date, @to_date, @recur_id)
			END
			ELSE
			BEGIN
				IF @start_date < @from_date
					SET @tmp_start_date = @from_date
				ELSE
					SET @tmp_start_date = @start_date

				SET @i = 0
				SET @count_days = DATEDIFF(d, @tmp_start_date, @end_date) --calc num of days in checking period

				WHILE (@i <= @count_days)
				BEGIN
					IF (@i = @count_days)
						SET @tmp_end_date = @end_date
					ELSE
						SET @tmp_end_date = DATEADD(dd, 0, DATEDIFF(dd, 0, @tmp_start_date)+0.99999999) --set end of date
					
					SET @from_time_diff = DATEDIFF(s, DATEADD(d,DATEDIFF(d, @tmp_start_date, @from_date), @tmp_start_date), @from_date) --get alert start time of day
					SET @to_time_diff = DATEDIFF(s, DATEADD(d,DATEDIFF(d, @tmp_start_date, @to_date), @tmp_start_date), @to_date) --get alert end time of day
					
					SET @tmp_end_date = DATEADD(s,@to_time_diff, @tmp_start_date) --add end time of day
					SET @tmp_start_date = DATEADD(s,@from_time_diff, @tmp_start_date) --add start time of day

					IF NOT DATEDIFF(s, DATEADD(d, DATEDIFF(d, @tmp_start_date, @to_date), @tmp_start_date), @to_date) < 0 AND
						NOT DATEDIFF(s, DATEADD(d, DATEDIFF(d, @tmp_end_date, @from_date), @tmp_end_date), @from_date) > 0
					BEGIN
						IF 1 = dbo.isActualDayRecurrence(@pattern,@number_days,@number_week,@week_days,@month_day,@number_month,@weekday_place,@weekday_day,@month_val,@dayly_selector,@monthly_selector,@yearly_selector ,@end_type,@from_date,@to_date,@tmp_start_date)
						BEGIN
							IF @select_active = 1
								INSERT INTO #tempAlertsIdsDates (id, from_date, to_date, recur_id) VALUES (@alert_id, @from_date, @to_date, @recur_id)
							SET @result = 1
						END
					END 

					SET @tmp_start_date = DATEADD(dd, 0, DATEDIFF(dd, 0, @tmp_start_date)+1) --next day
					SET @i = @i+1
				END
			END
		END
		
		FETCH NEXT FROM alerts_cursor INTO @alert_id, @recur_id, @sent_date, @pattern, @number_days, @number_week, @week_days, @month_day, @number_month, @weekday_place, @weekday_day, @month_val, @dayly_selector, @monthly_selector, @yearly_selector, @end_type, @occurences, @from_date, @to_date, @type2, @schedule_type
	END

	CLOSE alerts_cursor
	DEALLOCATE alerts_cursor
	

	IF @select_active = 1
	BEGIN
		IF @recipients = 'users'
		BEGIN
		SELECT  t.id,
			a.title,
			a.type2,
			a.class,
			a.urgent,
			a.ticker,
			a.device,
			a.fullscreen,
			a.ticker_speed as tickerspeed,
			a.sent_date as sentdate,
			a.sender_id as senderid,
			a.aknown,
			a.lifetime,
			a.alert_width as width,
			a.alert_height as height,
			a.resizable,
			a.autoclose,
			a.approve_status,
			case  when cast(a.caption_id as VARCHAR(36)) IS NULL THEN 'default'
			else cast(a.caption_id as VARCHAR(36))  end as captionid,
			a.self_deletable as selfdeletable,
			a.create_date as createdate,
			a.schedule,
			a.schedule_type,
			a.sms,
			a.email,
			a.text_to_call AS texttocall,
			t.from_date as fromdate,
			t.to_date as todate,
			s.id AS surveyid,
			case a.type2 WHEN 'B' THEN 'broadcast'
			else u.name end as username,
			d.name as user_domain,
			a.campaign_id AS campaignid
		FROM #tempAlertsIdsDates t
		INNER JOIN alerts a
		ON a.id = t.id
		LEFT JOIN surveys_main s
		ON  s.sender_id = a.id
		LEFT JOIN alerts_sent ON alerts_sent.alert_id = a.id
		LEFT JOIN users as u ON (u.id = alerts_sent.user_id)
		LEFT JOIN [domains] as d ON u.domain_id = d.id --OR u.domain_id IS NULL
		LEFT JOIN recurrence rc WITH (READPAST)
			ON rc.id = t.recur_id AND rc.alert_id = a.id
		LEFT JOIN alerts_received r WITH (READPAST)
			ON a.id = r.alert_id
			AND r.user_id = u.id
			AND (r.clsid = u.deskbar_id OR r.clsid = '' OR u.deskbar_id = '')
			AND (r.date >= t.from_date OR (rc.end_type = 'end_after' AND (rc.occurences <= r.occurrence OR rc.occurences IS NULL))) 

		WHERE  
			a.id NOT IN 
			(SELECT ar.alert_id FROM alerts_received as ar 
			WHERE ar.user_id =  a.id  
			AND  (rc.occurences >= ar.occurrence OR rc.occurences IS NULL)) 
			AND a.class != 4 
			AND ( a.approve_status = 1 OR a.approve_status IS NULL)
			
		ORDER BY t.from_date ASC, t.id ASC
		DROP TABLE #tempAlertsIdsDates
		END

		IF @recipients = 'computers'
		BEGIN
		SELECT  t.id,
			a.title,
			a.type2,
			a.class,
			a.urgent,
			a.ticker,
			a.fullscreen,
			a.ticker_speed as tickerspeed,
			a.sent_date as sentdate,
			a.sender_id as senderid,
			a.aknown,
			a.lifetime,
			a.alert_width as width,
			a.alert_height as height,
			a.resizable,
			a.self_deletable as selfdeletable,
			a.autoclose,
			a.device,
			a.approve_status,
			case  when cast(a.caption_id as VARCHAR(36)) IS NULL THEN 'default'
			else cast(a.caption_id as VARCHAR(36))  end as captionid,
			a.create_date as createdate,
			a.schedule,
			a.schedule_type,
			a.sms,
			a.email,
			a.text_to_call AS texttocall,
			t.from_date as fromdate,
			t.to_date as todate,
			s.id AS surveyid,
			p.name as comp_name,
			d.name as comp_domain,
			a.campaign_id AS campaignid
		FROM #tempAlertsIdsDates t
		INNER JOIN alerts a
		ON a.id = t.id
		LEFT JOIN surveys_main s
		ON  s.sender_id = a.id
		INNER JOIN alerts_sent_comp ON alerts_sent_comp.alert_id = a.id
		INNER JOIN computers as p ON p.id = alerts_sent_comp.comp_id
		LEFT JOIN [domains] as d ON p.domain_id = d.id OR p.domain_id IS NULL
		WHERE a.class != 4 AND ( a.approve_status = 1 OR a.approve_status IS NULL)
		ORDER BY t.from_date ASC, t.id ASC
		DROP TABLE #tempAlertsIdsDates
		
		END

		IF @recipients = 'ips'
		BEGIN
		SELECT  t.id,
			a.title,
			a.type2,
			a.class,
			a.urgent,
			a.ticker,
			a.fullscreen,
			a.device,
			a.ticker_speed as tickerspeed,
			a.sent_date as sentdate,
			a.sender_id as senderid,
			a.lifetime,
			a.aknown,
			a.approve_status,
			case  when cast(a.caption_id as VARCHAR(36)) IS NULL THEN 'default'
			else cast(a.caption_id as VARCHAR(36))  end as captionid,
			a.self_deletable as selfdeletable,
			a.alert_width as width,
			a.alert_height as height,
			a.resizable,
			a.autoclose,
			a.create_date as createdate,
			a.schedule,
			a.schedule_type,
			a.sms,
			a.email,
			a.text_to_call AS texttocall,
			t.from_date as fromdate,
			t.to_date as todate,
			s.id AS surveyid,
			ip.ip_from,
			ip.ip_to,
			a.campaign_id AS campaignid
		FROM #tempAlertsIdsDates t
		INNER JOIN alerts a
		ON a.id = t.id
		LEFT JOIN surveys_main s
		ON  s.sender_id = a.id
		INNER JOIN alerts_sent_iprange  as ip ON ip.alert_id = a.id
		WHERE a.class != 4 AND ( a.approve_status = 1 OR a.approve_status IS NULL)
		ORDER BY t.from_date ASC, t.id ASC
		DROP TABLE #tempAlertsIdsDates
		END
	END
END

GO

IF EXISTS (SELECT *
		   FROM   sysobjects
		   WHERE  id = object_id('GetTerminatedAlert')
				  AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
DROP PROCEDURE [dbo].[GetTerminatedAlert]
END
GO

CREATE PROC [dbo].[GetTerminatedAlert ]
(
	@alert_id INT,
	@recipients VARCHAR(200)
)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @recur_id BIGINT
	DECLARE @pattern VARCHAR(1)
	DECLARE @number_days INT
	DECLARE @number_week INT
	DECLARE @week_days INT
	DECLARE @month_day INT
	DECLARE @number_month INT
	DECLARE @weekday_place INT
	DECLARE @weekday_day INT
	DECLARE @month_val INT
	DECLARE @dayly_selector VARCHAR(50)
	DECLARE @monthly_selector VARCHAR(50)
	DECLARE @yearly_selector VARCHAR(50)
	DECLARE @end_type VARCHAR(50)
	DECLARE @occurences INT
	DECLARE @result BIT

	DECLARE @from_date DATETIME
	DECLARE @to_date DATETIME
	DECLARE @sent_date DATETIME
	DECLARE @type2 VARCHAR(1)
	DECLARE @schedule_type VARCHAR(1)

	DECLARE @tmp_start_date DATETIME
	DECLARE @tmp_end_date DATETIME

	DECLARE @i INT
	DECLARE @count_days INT

	DECLARE @from_time_diff INT
	DECLARE @to_time_diff INT



	IF OBJECT_ID('tempdb..#tempAlertsIdsDates') IS NOT NULL DROP TABLE #tempAlertsIdsDates
	CREATE TABLE #tempAlertsIdsDates (
		id BIGINT NOT NULL,
		from_date DATETIME NOT NULL,
		to_date DATETIME NOT NULL,
		recur_id INT NOT NULL
	)


    DECLARE alerts_cursor CURSOR FAST_FORWARD FOR
    SELECT a.id,
        r.id AS recur_id,
        a.sent_date,
        r.pattern,
        r.number_days,
        r.number_week,
        r.week_days,
        r.month_day,
        r.number_month,
        r.weekday_place,
        r.weekday_day,
        r.month_val,
        r.dayly_selector,
        r.monthly_selector,
        r.yearly_selector,
        r.end_type,
        r.occurences,
        a.from_date,
        a.to_date,
        a.type2,
        a.schedule_type
    FROM alerts a
    LEFT JOIN recurrence r
        ON a.id = r.alert_id
    WHERE a.id = @alert_id 

	OPEN alerts_cursor
	FETCH NEXT FROM alerts_cursor INTO @alert_id, @recur_id, @sent_date, @pattern, @number_days, @number_week, @week_days, @month_day, @number_month, @weekday_place, @weekday_day, @month_val, @dayly_selector, @monthly_selector, @yearly_selector, @end_type, @occurences, @from_date, @to_date, @type2, @schedule_type

	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF @recur_id IS NULL
				SET @recur_id = 0
		IF @from_date IS NULL OR DATEPART(yy, @from_date) = 1900
					SET @from_date = 0
		IF @to_date IS NULL OR DATEPART(yy, @to_date) = 1900
			SET @to_date = '9999-01-01 00:00:00.000'
		INSERT INTO #tempAlertsIdsDates (id, from_date, to_date, recur_id) VALUES (@alert_id, @from_date, @to_date, @recur_id)
		FETCH NEXT FROM alerts_cursor INTO @alert_id, @recur_id, @sent_date, @pattern, @number_days, @number_week, @week_days, @month_day, @number_month, @weekday_place, @weekday_day, @month_val, @dayly_selector, @monthly_selector, @yearly_selector, @end_type, @occurences, @from_date, @to_date, @type2, @schedule_type
	END

	CLOSE alerts_cursor
	DEALLOCATE alerts_cursor
	

	IF @recipients = 'users'
	BEGIN
	SELECT  t.id,
		a.title,
		a.type2,
		a.class,
		a.urgent,
		a.ticker,
		a.device,
		a.fullscreen,
		a.ticker_speed as tickerspeed,
		a.sent_date as sentdate,
		a.sender_id as senderid,
		a.aknown,
		a.lifetime,
		a.alert_width as width,
		a.alert_height as height,
		a.resizable,
		a.autoclose,
		case  when cast(a.caption_id as VARCHAR(36)) IS NULL THEN 'default'
		else cast(a.caption_id as VARCHAR(36))  end as captionid,
		a.self_deletable as selfdeletable,
		a.create_date as createdate,
		a.schedule,
		a.schedule_type,
		t.from_date as fromdate,
		t.to_date as todate,
		s.id AS surveyid,
		case a.type2 WHEN 'B' THEN 'broadcast'
		else u.name end as username,
		d.name as user_domain
	FROM #tempAlertsIdsDates t
	INNER JOIN alerts a
	ON a.id = t.id
	AND a.terminated = 1
	LEFT JOIN surveys_main s
	ON  s.sender_id = a.id
	LEFT JOIN alerts_sent ON alerts_sent.alert_id = a.id
	LEFT JOIN users as u ON (u.id = alerts_sent.user_id)
	LEFT JOIN [domains] as d ON u.domain_id = d.id
	LEFT JOIN recurrence rc WITH (READPAST)
		ON rc.id = t.recur_id AND rc.alert_id = a.id
	LEFT JOIN alerts_received r WITH (READPAST)
		ON a.id = r.alert_id
		AND r.user_id = u.id
		AND (r.clsid = u.deskbar_id OR r.clsid = '' OR u.deskbar_id = '')
		AND (r.date >= t.from_date OR (rc.end_type = 'end_after' AND (rc.occurences <= r.occurrence OR rc.occurences IS NULL))) 
	WHERE  a.class != 4 AND ( a.approve_status = 1 OR a.approve_status IS NULL)
	ORDER BY t.from_date ASC, t.id ASC
	DROP TABLE #tempAlertsIdsDates
	END

	IF @recipients = 'computers'
	BEGIN
	SELECT  t.id,
		a.title,
		a.type2,
		a.class,
		a.urgent,
		a.ticker,
		a.fullscreen,
		a.ticker_speed as tickerspeed,
		a.sent_date as sentdate,
		a.sender_id as senderid,
		a.aknown,
		a.alert_width as width,
		a.alert_height as height,
		a.resizable,
		a.lifetime,
		a.self_deletable as selfdeletable,
		a.autoclose,
		a.device,
		case  when cast(a.caption_id as VARCHAR(36)) IS NULL THEN 'default'
		else cast(a.caption_id as VARCHAR(36))  end as captionid,
		a.create_date as createdate,
		a.schedule,
		a.schedule_type,
		t.from_date as fromdate,
		t.to_date as todate,
		s.id AS surveyid,
		p.name as comp_name,
		d.name as comp_domain
	FROM #tempAlertsIdsDates t
	INNER JOIN alerts a
	ON a.id = t.id
	AND a.terminated = 1
	LEFT JOIN surveys_main s
	ON  s.sender_id = a.id
	INNER JOIN alerts_sent_comp ON alerts_sent_comp.alert_id = a.id
	INNER JOIN computers as p ON p.id = alerts_sent_comp.comp_id
	LEFT JOIN [domains] as d ON p.domain_id = d.id OR p.domain_id IS NULL
	WHERE a.class != 4 AND ( a.approve_status = 1 OR a.approve_status IS NULL)
	ORDER BY t.from_date ASC, t.id ASC
	DROP TABLE #tempAlertsIdsDates
		
	END

	IF @recipients = 'ips'
	BEGIN
	SELECT  t.id,
		a.title,
		a.class,
		a.type2,
		a.urgent,
		a.ticker,
		a.fullscreen,
		a.device,
		a.lifetime,
		a.ticker_speed as tickerspeed,
		a.sent_date as sentdate,
		a.sender_id as senderid,
		a.aknown,
		case  when cast(a.caption_id as VARCHAR(36)) IS NULL THEN 'default'
		else cast(a.caption_id as VARCHAR(36))  end as captionid,
		a.self_deletable as selfdeletable,
		a.alert_width as width,
		a.alert_height as height,
		a.resizable,
		a.autoclose,
		a.create_date as createdate,
		a.schedule,
		a.schedule_type,
		t.from_date as fromdate,
		t.to_date as todate,
		s.id AS surveyid,
		ip.ip_from,
		ip.ip_to
	FROM #tempAlertsIdsDates t
	INNER JOIN alerts a
	ON a.id = t.id
	AND a.terminated = 1
	LEFT JOIN surveys_main s
	ON  s.sender_id = a.id
	INNER JOIN alerts_sent_iprange  as ip ON ip.alert_id = a.id
	WHERE a.class != 4 AND ( a.approve_status = 1 OR a.approve_status IS NULL)
	ORDER BY t.from_date ASC, t.id ASC
	DROP TABLE #tempAlertsIdsDates
	END
END

GO


