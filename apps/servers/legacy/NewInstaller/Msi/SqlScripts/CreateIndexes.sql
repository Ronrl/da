USE [%dbname_hook%];

--indexes
GO

IF EXISTS (SELECT name
		   FROM   sys.indexes
		   WHERE  name = N'IX3_alerts_sent_all')
  DROP INDEX IX3_alerts_sent_all ON alerts_sent_group

GO

CREATE NONCLUSTERED INDEX [IX3_alerts_sent_all] ON [alerts_sent_group]
(
 [group_id] ASC,
 [alert_id] ASC
) ON [PRIMARY]

GO

IF EXISTS (SELECT name
		   FROM   sys.indexes
		   WHERE  name = N'IX_alerts_covering')
  DROP INDEX IX_alerts_covering ON alerts

GO

IF EXISTS (SELECT name
		   FROM   sys.indexes
		   WHERE  name = N'IX_alerts_sent_all')
  DROP INDEX IX_alerts_sent_all ON alerts_sent

GO

CREATE NONCLUSTERED INDEX [IX_alerts_sent_all] ON [alerts_sent]
(
 [user_id] ASC,
 [alert_id] ASC
) ON [PRIMARY]

GO

IF EXISTS (SELECT name
		   FROM   sys.indexes
		   WHERE  name = N'IX_alerts')
  DROP INDEX IX_alerts ON alerts

GO

IF EXISTS (SELECT name
		   FROM   sys.indexes
		   WHERE  name = N'IX_alerts_received')
  DROP INDEX IX_alerts_received ON alerts_received

GO

CREATE NONCLUSTERED INDEX [IX_alerts_received] ON [dbo].[alerts_received] 
(
	[alert_id] ASC,
	[user_id] ASC,
	[clsid] ASC,
	[date] ASC,
	[occurrence] ASC
) ON [PRIMARY]


GO

IF EXISTS (SELECT name
		   FROM   sys.indexes
		   WHERE  name = N'IX1_users_groups')
  DROP INDEX IX1_users_groups ON users_groups

GO

IF EXISTS (SELECT name
		   FROM   sys.indexes
		   WHERE  name = N'IX2_users_groups')
  DROP INDEX IX2_users_groups ON users_groups

GO

IF EXISTS (SELECT name
		   FROM   sys.indexes
		   WHERE  name = N'IX_users_groups_user_id')
  DROP INDEX IX_users_groups_user_id ON users_groups

GO

CREATE NONCLUSTERED INDEX [IX_users_groups_user_id] ON [dbo].[users_groups] 
(
	[user_id] ASC
)
INCLUDE ([group_id])

GO

IF EXISTS (SELECT name
		   FROM   sys.indexes
		   WHERE  name = N'IX1_computers_groups')
  DROP INDEX IX1_computers_groups ON computers_groups

GO

IF EXISTS (SELECT name
		   FROM   sys.indexes
		   WHERE  name = N'IX2_computers_groups')
  DROP INDEX IX2_computers_groups ON computers_groups

GO

IF EXISTS (SELECT name
		   FROM   sys.indexes
		   WHERE  name = N'IX_computers_groups_comp_id')
  DROP INDEX IX_computers_groups_comp_id ON computers_groups

GO

CREATE NONCLUSTERED INDEX [IX_computers_groups_comp_id] ON [dbo].[computers_groups] 
(
	[comp_id] ASC
)
INCLUDE ( [group_id])

GO

IF EXISTS (SELECT name
		   FROM   sys.indexes
		   WHERE  name = N'IX1_OU_User')
  DROP INDEX IX1_OU_User ON OU_User

GO

CREATE NONCLUSTERED INDEX IX1_OU_User ON OU_User
(
	[Id_user] ASC
)
INCLUDE
(
	[Id_OU]
)
GO

IF EXISTS (SELECT name
		   FROM   sys.indexes
		   WHERE  name = N'IX2_OU_User')
  DROP INDEX IX2_OU_User ON OU_User

GO

CREATE NONCLUSTERED INDEX IX2_OU_User ON OU_User
(
	[Id_OU] ASC
)
INCLUDE 
(
	[Id_user]
)

GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[alerts_sent]') AND name = N'IX1_alerts_sent')
	DROP INDEX [IX1_alerts_sent] ON [dbo].[alerts_sent]
GO

CREATE NONCLUSTERED INDEX [IX1_alerts_sent] ON [dbo].[alerts_sent]
(
	[alert_id] ASC
)
INCLUDE
(
	[user_id]
)

GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[alerts_sent]') AND name = N'IX2_alerts_sent')
	DROP INDEX [IX2_alerts_sent] ON [dbo].[alerts_sent]
GO

CREATE NONCLUSTERED INDEX [IX2_alerts_sent] ON [dbo].[alerts_sent]
(
	[user_id] ASC
)
INCLUDE
(
	[alert_id]
)

GO

IF EXISTS (SELECT name
		   FROM   sys.indexes
		   WHERE  name = N'IX1_alerts_sent_group')
  DROP INDEX IX1_alerts_sent_group ON alerts_sent_group

GO

CREATE NONCLUSTERED INDEX IX1_alerts_sent_group
ON alerts_sent_group (alert_id)
INCLUDE ([group_id])

GO

IF EXISTS (SELECT name
		   FROM   sys.indexes
		   WHERE  name = N'IX1_archive_alerts_sent_group')
  DROP INDEX IX1_archive_alerts_sent_group ON archive_alerts_sent_group

GO

CREATE NONCLUSTERED INDEX IX1_archive_alerts_sent_group
ON archive_alerts_sent_group (alert_id)
INCLUDE ([group_id])

GO

IF EXISTS (SELECT name
		   FROM   sys.indexes
		   WHERE  name = N'IX2_alerts_sent_group')
  DROP INDEX IX2_alerts_sent_group ON alerts_sent_group

GO

CREATE NONCLUSTERED INDEX IX2_alerts_sent_group
  ON alerts_sent_group (group_id)
INCLUDE ([alert_id])

GO

IF EXISTS (SELECT name
		   FROM   sys.indexes
		   WHERE  name = N'IX1_alerts_sent_comp')
  DROP INDEX IX1_alerts_sent_comp ON alerts_sent_comp

GO

CREATE NONCLUSTERED INDEX IX1_alerts_sent_comp
  ON alerts_sent_comp (alert_id)
INCLUDE ([comp_id])

GO

IF EXISTS (SELECT name
		   FROM   sys.indexes
		   WHERE  name = N'IX2_alerts_sent_comp')
  DROP INDEX IX2_alerts_sent_comp ON alerts_sent_comp

GO

IF EXISTS (SELECT name
		   FROM   sys.indexes
		   WHERE  name = N'IX_alerts_sent_comp_comp_id')
  DROP INDEX IX_alerts_sent_comp_comp_id ON alerts_sent_comp

GO

CREATE NONCLUSTERED INDEX [IX_alerts_sent_comp_comp_id] ON [dbo].[alerts_sent_comp] 
(
	[comp_id] ASC
)
INCLUDE ( [alert_id])

GO

IF EXISTS (SELECT name
		   FROM   sys.indexes
		   WHERE  name = N'IX1_alerts_sent_ou')
  DROP INDEX IX1_alerts_sent_ou ON alerts_sent_ou

GO

CREATE NONCLUSTERED INDEX IX1_alerts_sent_ou
  ON alerts_sent_ou (alert_id)
INCLUDE ( [ou_id])

GO

IF EXISTS (SELECT name
		   FROM   sys.indexes
		   WHERE  name = N'IX2_alerts_sent_ou')
  DROP INDEX IX2_alerts_sent_ou ON alerts_sent_ou

GO

IF EXISTS (SELECT name
		   FROM   sys.indexes
		   WHERE  name = N'IX_alerts_sent_ou_ou_id')
  DROP INDEX IX_alerts_sent_ou_ou_id ON alerts_sent_ou

GO

CREATE NONCLUSTERED INDEX [IX_alerts_sent_ou_ou_id] ON [dbo].[alerts_sent_ou] 
(
	[ou_id] ASC
)
INCLUDE ( [alert_id])

GO

IF EXISTS (SELECT name
		   FROM   sys.indexes
		   WHERE  name = N'IX_surveys_main')
  DROP INDEX IX_surveys_main ON surveys_main

GO

CREATE NONCLUSTERED INDEX IX_surveys_main
  ON surveys_main ([create_date],[type])
  INCLUDE([id])

GO

IF EXISTS (SELECT name
		   FROM   sys.indexes
		   WHERE  name = N'IX_surveys_main_closed_sender_id')
  DROP INDEX IX_surveys_main_closed_sender_id ON surveys_main

GO

CREATE NONCLUSTERED INDEX IX_surveys_main_closed_sender_id
  ON surveys_main ([closed],[sender_id])
  INCLUDE([id], [name], [type], [stype], [create_date], [schedule], [from_date], [to_date])

GO

IF EXISTS (SELECT name
		   FROM   sys.indexes
		   WHERE  name = N'IX1_recurrence')
  DROP INDEX IX1_recurrence ON recurrence

GO

CREATE NONCLUSTERED INDEX [IX1_recurrence] ON [dbo].[recurrence] 
(
	[alert_id] ASC,
	[id] ASC
)
INCLUDE
(
	[pattern],
	[number_days],
	[number_week],
	[week_days],
	[month_day],
	[number_month],
	[weekday_place],
	[weekday_day],
	[month_val],
	[dayly_selector],
	[monthly_selector],
	[yearly_selector],
	[end_type],
	[occurences]
) ON [PRIMARY]

GO

IF EXISTS (SELECT name
		   FROM   sys.indexes
		   WHERE  name = N'IX1_users')
  DROP INDEX IX1_users ON users

GO

IF EXISTS (SELECT name
		   FROM   sys.indexes
		   WHERE  name = N'IX2_users')
  DROP INDEX IX2_users ON users

GO

IF EXISTS (SELECT name
		   FROM   sys.indexes
		   WHERE  name = N'IX_computers_name')
  DROP INDEX IX_computers_name ON [dbo].[computers]

GO

CREATE NONCLUSTERED INDEX IX_computers_name
ON [dbo].[computers] ([name])
INCLUDE ([id],[domain_id])

GO

IF EXISTS (SELECT name
		   FROM   sys.indexes
		   WHERE  name = N'IX1_computers')
  DROP INDEX IX1_computers ON computers

GO

IF EXISTS (SELECT name
		   FROM   sys.indexes
		   WHERE  name = N'IX1_alerts_read')
  DROP INDEX IX1_alerts_read ON alerts_read

GO

CREATE NONCLUSTERED INDEX IX1_alerts_read
  ON alerts_read (user_id, alert_id)
  INCLUDE (id)
GO

IF EXISTS (SELECT name
		   FROM   sys.indexes
		   WHERE  name = N'IX1_archive_alerts_read')
  DROP INDEX IX1_archive_alerts_read ON archive_alerts_read

GO

CREATE NONCLUSTERED INDEX IX1_archive_alerts_read
  ON archive_alerts_read (user_id, alert_id)
  INCLUDE (id)
GO

IF EXISTS (SELECT name
		   FROM   sys.indexes
		   WHERE  name = N'IX2_alerts_read')
  DROP INDEX IX2_alerts_read ON alerts_read

GO

CREATE NONCLUSTERED INDEX IX2_alerts_read
  ON alerts_read (alert_id, [date])
  INCLUDE (user_id, id)

GO

IF EXISTS (SELECT name
		   FROM   sys.indexes
		   WHERE  name = N'IX2_archive_alerts_read')
  DROP INDEX IX2_archive_alerts_read ON archive_alerts_read

GO

CREATE NONCLUSTERED INDEX IX2_archive_alerts_read
  ON archive_alerts_read (alert_id, [date])
  INCLUDE (user_id, id)

GO

IF EXISTS (SELECT name
		   FROM   sys.indexes
		   WHERE  name = N'IX1_alerts_sent_iprange')
  DROP INDEX IX1_alerts_sent_iprange ON alerts_sent_iprange

GO

CREATE NONCLUSTERED INDEX IX1_alerts_sent_iprange
  ON alerts_sent_iprange (alert_id)
INCLUDE (
	[ip_from],
	[ip_to]
)

GO

IF EXISTS (SELECT name
		   FROM   sys.indexes
		   WHERE  name = N'IX2_alerts_sent_iprange')
  DROP INDEX IX2_alerts_sent_iprange ON alerts_sent_iprange

GO

IF EXISTS (SELECT name
		   FROM   sys.indexes
		   WHERE  name = N'IX_alerts_sent_iprange_ip_from_ip_to')
  DROP INDEX IX_alerts_sent_iprange_ip_from_ip_to ON alerts_sent_iprange

GO

CREATE NONCLUSTERED INDEX [IX_alerts_sent_iprange_ip_from_ip_to] ON [dbo].[alerts_sent_iprange] 
(
	[ip_from] ASC,
	[ip_to] ASC
)
INCLUDE ( [alert_id])

GO 

IF EXISTS (SELECT name
		   FROM   sys.indexes
		   WHERE  name = N'IX_groups_domain_id')
  DROP INDEX IX_groups_domain_id ON groups

GO

CREATE NONCLUSTERED INDEX [IX_groups_domain_id]
ON [groups] ([domain_id])
INCLUDE ([id])

GO 

IF EXISTS (SELECT name
		   FROM   sys.indexes
		   WHERE  name = N'IX_OU_group_id_ou')
  DROP INDEX IX_OU_group_id_ou ON OU_Group

GO

CREATE NONCLUSTERED INDEX [IX_OU_group_id_ou] ON [OU_Group]
(
	[Id_OU] ASC
)
INCLUDE
(
	[Id_group]
)


IF EXISTS (SELECT name
		   FROM   sys.indexes
		   WHERE  name = N'IX_OU_group_id_group')
  DROP INDEX IX_OU_group_id_group ON OU_Group

GO

CREATE NONCLUSTERED INDEX [IX_OU_group_id_group] ON [OU_Group]
(
	[Id_group] ASC
)
INCLUDE
(
	[Id_OU]
)

GO 

IF EXISTS (SELECT name
		   FROM   sys.indexes
		   WHERE  name = N'IX_users_groups_group_id_include_user_id')
  DROP INDEX IX_users_groups_group_id_include_user_id ON users_groups

GO

CREATE NONCLUSTERED INDEX [IX_users_groups_group_id_include_user_id]
ON [users_groups]([group_id])
INCLUDE ([user_id])

GO 

IF EXISTS (SELECT name
		   FROM   sys.indexes
		   WHERE  name = N'IX_computers_groups_group_id_include_comp_id')
  DROP INDEX IX_computers_groups_group_id_include_comp_id ON computers_groups

GO

CREATE NONCLUSTERED INDEX [IX_computers_groups_group_id_include_comp_id]
ON [computers_groups]([group_id])
INCLUDE ([comp_id])

GO 

IF EXISTS (SELECT name
		   FROM   sys.indexes
		   WHERE  name = N'IX_groups_groups_container_group_id_include_group_id')
  DROP INDEX IX_groups_groups_container_group_id_include_group_id ON groups_groups

GO

CREATE NONCLUSTERED INDEX [IX_groups_groups_container_group_id_include_group_id]
ON [groups_groups]([container_group_id])
INCLUDE ([group_id])

GO

IF EXISTS (SELECT name FROM   sys.indexes WHERE name = N'IX_alerts_recur')
	DROP INDEX IX_alerts_recur ON [dbo].[alerts]

GO

CREATE NONCLUSTERED INDEX [IX_alerts_recur] ON [dbo].[alerts]
(
	[sms] ASC,
	[email] ASC,
	[type2] ASC,
	[schedule_type] ASC,
	[recurrence] ASC,
	[sent_date] ASC,
	[from_date] ASC,
	[to_date] ASC
)
INCLUDE
(
	[id],
	[title],
	[alert_text],
	[template_id],
	[email_sender],
	[schedule],
	[urgent],
	[aknown],
	[ticker],
	[autoclose],
	[create_date],
	[fullscreen],
	[alert_width],
	[alert_height]
)

GO


IF EXISTS (SELECT name FROM   sys.indexes WHERE name = N'IX_alerts_recur_sms')
	DROP INDEX [IX_alerts_recur_sms] ON [dbo].[alerts]

GO

CREATE NONCLUSTERED INDEX [IX_alerts_recur_sms] ON [dbo].[alerts]
(
	[sms] ASC,
	[schedule] ASC,
	[sent_date] ASC
)
INCLUDE
(
	[id]
)
GO

IF EXISTS (SELECT name FROM   sys.indexes WHERE name = N'IX_alerts_recur_email')
	DROP INDEX [IX_alerts_recur_email] ON [dbo].[alerts]

GO

CREATE NONCLUSTERED INDEX [IX_alerts_recur_email] ON [dbo].[alerts]
(
	[email] ASC,
	[schedule] ASC,
	[sent_date] ASC
)
INCLUDE
(
	[id]
)

GO

IF EXISTS (SELECT name FROM   sys.indexes WHERE name = N'IX_alerts_sched')
	DROP INDEX IX_alerts_sched ON [dbo].[alerts]

GO

IF EXISTS (SELECT name FROM   sys.indexes WHERE name = N'IX_alerts_received_recur_id_id')
	DROP INDEX IX_alerts_received_recur_id_id ON [dbo].[alerts_received_recur]

GO

CREATE NONCLUSTERED INDEX [IX_alerts_received_recur_id_id] ON [dbo].[alerts_received_recur] 
(
	[user_id] ASC,
	[alert_id] ASC,
	[date] ASC,
	[occurrence] ASC
)

GO

IF EXISTS (SELECT name FROM   sys.indexes WHERE name = N'IX_alerts_received_recur_id_date')
	DROP INDEX [IX_alerts_received_recur_id_date] ON [dbo].[alerts_received_recur]

GO

CREATE NONCLUSTERED INDEX [IX_alerts_received_recur_id_date] ON [dbo].[alerts_received_recur] 
(
	[alert_id] ASC,
	[date] ASC,
	[occurrence] ASC
)
INCLUDE
(
	[user_id]
)

GO

IF EXISTS (SELECT name FROM sys.indexes WHERE name = N'IX_alerts_received_recur_id_id_date')
	DROP INDEX IX_alerts_received_recur_id_id_date ON [dbo].[alerts_received_recur]

GO

IF EXISTS (SELECT name FROM   sys.indexes WHERE  name = N'IX_users_id')
	DROP INDEX IX_users_id ON [dbo].[users]

GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[OU_hierarchy]') AND name = N'IX_OU_hierarchy_Id_parent')
	DROP INDEX [IX_OU_hierarchy_Id_parent] ON [dbo].[OU_hierarchy]

GO

CREATE NONCLUSTERED INDEX [IX_OU_hierarchy_Id_parent] ON [dbo].[OU_hierarchy]
(
	[Id_parent] ASC
)
INCLUDE
(
	[Id_child]
)
GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[OU_comp]') AND name = N'IX_OU_comp_Id_OU')
	DROP INDEX [IX_OU_comp_Id_OU] ON [dbo].[OU_comp]
GO

CREATE NONCLUSTERED INDEX [IX_OU_comp_Id_OU] ON [dbo].[OU_comp]
(
	[Id_OU] ASC
)
INCLUDE
(
	[Id_comp]
)
GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[OU_comp]') AND name = N'IX_OU_comp_Id_comp')
	DROP INDEX [IX_OU_comp_Id_comp] ON [dbo].[OU_comp]
GO

CREATE NONCLUSTERED INDEX [IX_OU_comp_Id_comp] ON [dbo].[OU_comp]
(
	[Id_comp] ASC
)
INCLUDE
(
	[Id_OU]
)
GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[policy_computer]') AND name = N'IX_policy_computer_comp_id')
	DROP INDEX [IX_policy_computer_comp_id] ON [dbo].[policy_computer]
GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[computers]') AND name = N'IX_computer_domain_id')
	DROP INDEX [IX_computer_domain_id] ON [dbo].[computers]
GO

CREATE NONCLUSTERED INDEX [IX_computer_domain_id] ON [dbo].[computers] 
(
	[domain_id] ASC
)
INCLUDE ([id])

GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[users]') AND name = N'IX_users_domain_id')
	DROP INDEX [IX_users_domain_id] ON [dbo].[users]

GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[policy_user]') AND name = N'IX_policy_user_user_id')
	DROP INDEX [IX_policy_user_user_id] ON [dbo].[policy_user]
GO

IF EXISTS (SELECT name
		   FROM   sys.indexes
		   WHERE  name = N'IX_users_name_role')
  DROP INDEX IX_users_name_role ON [dbo].[users]

GO

IF EXISTS (SELECT name
		   FROM   sys.indexes
		   WHERE  name = N'IX_users_role_reg_date_id')
  DROP INDEX IX_users_role_reg_date_id ON [dbo].[users]

GO

CREATE NONCLUSTERED INDEX IX_users_role_reg_date_id
ON [dbo].[users] ([role],[reg_date],[id])
INCLUDE ([name],[mobile_phone],[email],[standby],[disabled],[last_request])

GO

IF EXISTS (SELECT name
		   FROM   sys.indexes
		   WHERE  name = N'IX_users_name_role_domain_id')
  DROP INDEX IX_users_name_role_domain_id ON [dbo].[users]

GO

CREATE NONCLUSTERED INDEX IX_users_name_role_domain_id
ON [dbo].[users] ([role],[name],[domain_id])
INCLUDE ([id],[reg_date],[context_id])

GO

IF EXISTS (SELECT name
		   FROM   sys.indexes
		   WHERE  name = N'IX_alerts_type2_sent_date_recurrence')
  DROP INDEX IX_alerts_type2_sent_date_recurrence ON [dbo].[alerts]

GO

CREATE NONCLUSTERED INDEX [IX_alerts_type2_sent_date_recurrence] ON [dbo].[alerts]
(
	[type2] ASC,
	[sent_date] ASC,
	[recurrence] ASC
)
INCLUDE
(
	[id],
	[title],
	[create_date],
	[from_date],
	[to_date],
	[schedule],
	[schedule_type],
	[urgent],
	[aknown],
	[ticker],
	[fullscreen],
	[alert_width],
	[alert_height],
	[autoclose],
	[class],
	[desktop],
	[caption_id]
) ON [PRIMARY]

GO

IF EXISTS (SELECT name
		   FROM   sys.indexes
		   WHERE  name = N'IX_alerts_type2_sent_date_from_date_to_date')
  DROP INDEX IX_alerts_type2_sent_date_from_date_to_date ON [dbo].[alerts]

GO

CREATE NONCLUSTERED INDEX [IX_alerts_type2_sent_date_from_date_to_date] ON [dbo].[alerts]
(
	[type2] ASC,
	[sent_date] ASC,
	[from_date] ASC,
	[to_date] ASC
)
INCLUDE
(
	[id],
	[title],
	[schedule],
	[schedule_type],
	[recurrence],
	[urgent],
	[aknown],
	[ticker],
	[fullscreen],
	[alert_width],
	[alert_height],
	[autoclose],
	[class],
	[desktop],
	[caption_id]
) ON [PRIMARY]

GO

IF EXISTS (SELECT name
		   FROM   sys.indexes
		   WHERE  name = N'IX_archive_alerts_type2_sent_date_recurrence')
  DROP INDEX IX_archive_alerts_type2_sent_date_recurrence ON [dbo].[archive_alerts]

GO

CREATE NONCLUSTERED INDEX [IX_archive_alerts_type2_sent_date_recurrence] ON [dbo].[archive_alerts]
(
	[type2] ASC,
	[sent_date] ASC,
	[recurrence] ASC
)
INCLUDE
(
	[id],
	[title],
	[create_date],
	[from_date],
	[to_date],
	[schedule],
	[schedule_type],
	[urgent],
	[aknown],
	[ticker],
	[fullscreen],
	[alert_width],
	[alert_height],
	[autoclose],
	[class],
	[desktop],
	[caption_id]
) ON [PRIMARY]

GO

IF EXISTS (SELECT name
		   FROM   sys.indexes
		   WHERE  name = N'IX_alerts_received_user_id')
  DROP INDEX IX_alerts_received_user_id ON [dbo].[alerts_received]

GO

CREATE NONCLUSTERED INDEX [IX_alerts_received_user_id] ON [dbo].[alerts_received] 
(
	[user_id] ASC
)
INCLUDE
(
	[alert_id],
	[clsid],
	[date],
	[occurrence]
) ON [PRIMARY]

GO

IF EXISTS (SELECT name
		   FROM   sys.indexes
		   WHERE  name = N'IX_domains_name')
  DROP INDEX IX_domains_name ON [dbo].[domains]

GO

CREATE NONCLUSTERED INDEX IX_domains_name
ON [dbo].[domains] ([name])
INCLUDE ([id])

GO

IF EXISTS (SELECT name
		   FROM   sys.indexes
		   WHERE  name = N'IX_User_synch_uid')
  DROP INDEX IX_User_synch_uid ON User_synch

GO

CREATE NONCLUSTERED INDEX IX_User_synch_uid ON User_synch
(
	[UID] ASC
)
INCLUDE 
(
	[Id_user]
)

GO

IF EXISTS (SELECT name
		   FROM   sys.indexes
		   WHERE  name = N'IX_Comp_synch_uid')
  DROP INDEX IX_Comp_synch_uid ON Comp_synch

GO

CREATE NONCLUSTERED INDEX IX_Comp_synch_uid ON Comp_synch
(
	[UID] ASC
)
INCLUDE 
(
	[Id_comp]
)

GO

IF EXISTS (SELECT name
		   FROM   sys.indexes
		   WHERE  name = N'IX_Group_synch_uid')
  DROP INDEX IX_Group_synch_uid ON Group_synch

GO

CREATE NONCLUSTERED INDEX IX_Group_synch_uid ON Group_synch
(
	[UID] ASC
)
INCLUDE 
(
	[Id_group]
)

GO

IF EXISTS (SELECT name
		   FROM   sys.indexes
		   WHERE  name = N'IX_OU_synch_uid')
  DROP INDEX IX_OU_synch_uid ON OU_synch

GO

CREATE NONCLUSTERED INDEX IX_OU_synch_uid ON OU_synch
(
	[UID] ASC
)
INCLUDE 
(
	[Id_OU]
)

GO

IF EXISTS (SELECT name
		   FROM   sys.indexes
		   WHERE  name = N'IX_groups_groups_id_id_id')
  DROP INDEX IX_groups_groups_id_id_id ON groups_groups

GO

CREATE NONCLUSTERED INDEX IX_groups_groups_id_id_id ON groups_groups
(
	[container_group_id] ASC,
	[group_id] ASC
)
INCLUDE 
(
	[id]
)

GO

IF EXISTS (SELECT name
		   FROM   sys.indexes
		   WHERE  name = N'IX_computers_groups_id_id_id')
  DROP INDEX IX_computers_groups_id_id_id ON computers_groups

GO

CREATE NONCLUSTERED INDEX IX_computers_groups_id_id_id ON computers_groups
(
	[group_id] ASC,
	[comp_id] ASC
)
INCLUDE 
(
	[id]
)

GO

IF EXISTS (SELECT name
		   FROM   sys.indexes
		   WHERE  name = N'IX_users_groups_id_id_id')
  DROP INDEX IX_users_groups_id_id_id ON users_groups

GO

CREATE NONCLUSTERED INDEX IX_users_groups_id_id_id ON users_groups
(
	[group_id] ASC,
	[user_id] ASC
)
INCLUDE 
(
	[id]
)

GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[users]') AND name = N'IX_role_last_request')
	DROP INDEX [IX_role_last_request] ON [dbo].[users]

GO

CREATE NONCLUSTERED INDEX [IX_role_last_request] ON [dbo].[users]
(
	[role] ASC,
	[last_request] ASC
)
INCLUDE
(
	[id]
)
GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[users]') AND name = N'IX_users_id_email')
	DROP INDEX [IX_users_id_email] ON [dbo].[users]
GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[users]') AND name = N'IX_users_id_phone')
	DROP INDEX [IX_users_id_phone] ON [dbo].[users]
GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[groups_groups]') AND name = N'IX_groups_groups_group_id_include_container_group_id')
	DROP INDEX [IX_groups_groups_group_id_include_container_group_id] ON [dbo].[groups_groups]
GO

CREATE NONCLUSTERED INDEX [IX_groups_groups_group_id_include_container_group_id] ON [dbo].[groups_groups] 
(
	[group_id] ASC
)
INCLUDE
(
	[container_group_id]
)

GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ou_groups]') AND name = N'IX_ou_groups_group_id_include_ou_id')
	DROP INDEX [IX_ou_groups_group_id_include_ou_id] ON [dbo].[ou_groups]
GO

CREATE NONCLUSTERED INDEX [IX_ou_groups_group_id_include_ou_id] ON [dbo].[ou_groups] 
(
	[group_id] ASC
)
INCLUDE
(
	[ou_id]
)

GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ou_groups]') AND name = N'IX_ou_groups_ou_id_include_group_id')
	DROP INDEX [IX_ou_groups_ou_id_include_group_id] ON [dbo].[ou_groups]
GO

CREATE NONCLUSTERED INDEX [IX_ou_groups_ou_id_include_group_id] ON [dbo].[ou_groups] 
(
	[ou_id] ASC
)
INCLUDE
(
	[group_id]
)

GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ou_groups]') AND name = N'IX_ou_groups_id_id_id')
	DROP INDEX [IX_ou_groups_id_id_id] ON [dbo].[ou_groups]
GO

CREATE NONCLUSTERED INDEX [IX_ou_groups_id_id_id] ON [dbo].[ou_groups] 
(
	[group_id] ASC,
	[ou_id] ASC
)
INCLUDE
(
	[id]
)

GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[alerts]') AND name = N'IX_alerts_type_class_sent_date_sender_id')
	DROP INDEX [IX_alerts_type_class_sent_date_sender_id] ON [dbo].[alerts]
GO

CREATE NONCLUSTERED INDEX [IX_alerts_type_class_sent_date_sender_id] ON [dbo].[alerts]
(
	[type] ASC,
	[class] ASC,
	[sent_date] ASC,
	[sender_id] ASC
)
INCLUDE
(
	[id],
	[alert_text],
	[aknown],
	[type2],
	[title],
	[create_date],
	[from_date],
	[to_date],
	[schedule_type],
	[urgent],
	[ticker],
	[fullscreen]
)

GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[archive_alerts]') AND name = N'IX_archive_alerts_type_class_sent_date_sender_id')
	DROP INDEX [IX_archive_alerts_type_class_sent_date_sender_id] ON [dbo].[archive_alerts]
GO

CREATE NONCLUSTERED INDEX [IX_archive_alerts_type_class_sent_date_sender_id] ON [dbo].[archive_alerts]
(
	[type] ASC,
	[class] ASC,
	[sent_date] ASC,
	[sender_id] ASC
)
INCLUDE
(
	[id],
	[alert_text],
	[aknown],
	[type2],
	[title],
	[create_date],
	[urgent],
	[ticker],
	[fullscreen]
)

GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[alerts]') AND name = N'IX_alerts_type_class_param_temp_id_sender_id')
	DROP INDEX [IX_alerts_type_class_param_temp_id_sender_id] ON [dbo].[alerts]
GO

CREATE NONCLUSTERED INDEX [IX_alerts_type_class_param_temp_id_sender_id] ON [dbo].[alerts]
(
	[type] ASC,
	[class] ASC,
	[param_temp_id] ASC,
	[sender_id] ASC
)
INCLUDE
(
	[id],
	[schedule],
	[schedule_type],
	[recurrence],
	[alert_text],
	[title],
	[sent_date],
	[create_date],
	[from_date],
	[to_date]
)

GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[archive_alerts]') AND name = N'IX_archive_alerts_type_class_param_temp_id_sender_id')
	DROP INDEX [IX_archive_alerts_type_class_param_temp_id_sender_id] ON [dbo].[archive_alerts]
GO

CREATE NONCLUSTERED INDEX [IX_archive_alerts_type_class_param_temp_id_sender_id] ON [dbo].[archive_alerts]
(
	[type] ASC,
	[class] ASC,
	[param_temp_id] ASC,
	[sender_id] ASC
)
INCLUDE
(
	[id],
	[schedule],
	[schedule_type],
	[recurrence],
	[alert_text],
	[title],
	[sent_date],
	[create_date],
	[from_date],
	[to_date]
)

GO

IF EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[alerts_sent_stat]') AND name = N'IX_alerts_sent_stat_date')
	DROP INDEX [IX_alerts_sent_stat_date] ON [dbo].[alerts_sent_stat]
GO

CREATE NONCLUSTERED INDEX [IX_alerts_sent_stat_date]
ON [dbo].[alerts_sent_stat] ([date])
INCLUDE ([alert_id],[user_id])

GO

IF EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[archive_alerts_sent_stat]') AND name = N'IX_archive_alerts_sent_stat_date')
	DROP INDEX [IX_archive_alerts_sent_stat_date] ON [dbo].[archive_alerts_sent_stat]
GO

CREATE NONCLUSTERED INDEX [IX_archive_alerts_sent_stat_date]
ON [dbo].[archive_alerts_sent_stat] ([date])
INCLUDE ([alert_id],[user_id])

GO

IF EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[alerts]') AND name = N'IX_alerts_create_date')
	DROP INDEX [IX_alerts_create_date] ON [dbo].[alerts]
GO

CREATE NONCLUSTERED INDEX [IX_alerts_create_date]
ON [dbo].[alerts] ([create_date])

GO

IF EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[archive_alerts]') AND name = N'IX_archive_alerts_create_date')
	DROP INDEX [IX_archive_alerts_create_date] ON [dbo].[archive_alerts]
GO

CREATE NONCLUSTERED INDEX [IX_archive_alerts_create_date]
ON [dbo].[archive_alerts] ([create_date])

GO
------- parametrized -------------

IF EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[alert_parameters]') AND name = N'IX_alert_parameters_temp_id')
	DROP INDEX [IX_alert_parameters_temp_id] ON [dbo].[alert_parameters]
GO

CREATE NONCLUSTERED INDEX [IX_alert_parameters_temp_id]
ON [dbo].[alert_parameters] ([temp_id], [visible])
INCLUDE ([id], [name], [short_name], [type], [default], [action])

GO

IF EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[alert_parameters]') AND name = N'IX_alert_parameters_type')
	DROP INDEX [IX_alert_parameters_type] ON [dbo].[alert_parameters]
GO

CREATE NONCLUSTERED INDEX [IX_alert_parameters_type]
ON [dbo].[alert_parameters] ([type])
INCLUDE ([id], [name], [order])

GO

IF EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[alert_param_selects]') AND name = N'IX_alert_param_selects_param_id')
	DROP INDEX [IX_alert_param_selects_param_id] ON [dbo].[alert_param_selects]
GO

CREATE NONCLUSTERED INDEX [IX_alert_param_selects_param_id]
ON [dbo].[alert_param_selects] ([param_id])
INCLUDE ([id], [name], [value])

GO
IF EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[alert_param_values]') AND name = N'IX_alert_param_values_alert_id')
	DROP INDEX [IX_alert_param_values_alert_id] ON [dbo].[alert_param_values]

GO

CREATE NONCLUSTERED INDEX [IX_alert_param_values_alert_id]
ON [dbo].[alert_param_values] ([alert_id],[param_id])
INCLUDE ([id], [value])

GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[policy_computer]') AND name = N'IX_policy_computer_policy_id')
	DROP INDEX [IX_policy_computer_policy_id] ON [dbo].[policy_computer]
GO

CREATE NONCLUSTERED INDEX [IX_policy_computer_policy_id] ON [dbo].[policy_computer]
(
	[policy_id] ASC
)
INCLUDE
(
	[comp_id]
)

GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[policy_editor]') AND name = N'IX_policy_editor_policy_id')
	DROP INDEX [IX_policy_editor_policy_id] ON [dbo].[policy_editor]
GO

CREATE NONCLUSTERED INDEX [IX_policy_editor_policy_id] ON [dbo].[policy_editor]
(
	[policy_id] ASC
)
INCLUDE
(
	[editor_id]
)

GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[policy_group]') AND name = N'IX_policy_group_policy_id')
	DROP INDEX [IX_policy_group_policy_id] ON [dbo].[policy_group]
GO

CREATE NONCLUSTERED INDEX [IX_policy_group_policy_id] ON [dbo].[policy_group]
(
	[policy_id] ASC
)
INCLUDE
(
	[group_id]
)

GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[policy_ip_group]') AND name = N'IX_policy_ip_group_policy_id')
	DROP INDEX [IX_policy_ip_group_policy_id] ON [dbo].[policy_ip_group]
GO

CREATE NONCLUSTERED INDEX [IX_policy_ip_group_policy_id] ON [dbo].[policy_ip_group]
(
	[policy_id] ASC
)
INCLUDE
(
	[group_id]
)

GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[policy_ou]') AND name = N'IX_policy_ou_policy_id')
	DROP INDEX [IX_policy_ou_policy_id] ON [dbo].[policy_ou]
GO

CREATE NONCLUSTERED INDEX [IX_policy_ou_policy_id] ON [dbo].[policy_ou]
(
	[policy_id] ASC
)
INCLUDE
(
	[ou_id]
) 

GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[policy_user]') AND name = N'IX_policy_user_policy_id')
	DROP INDEX [IX_policy_user_policy_id] ON [dbo].[policy_user]
GO

CREATE NONCLUSTERED INDEX [IX_policy_user_policy_id] ON [dbo].[policy_user]
(
	[policy_id] ASC
)
INCLUDE
(
	[user_id]
)

GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[policy_viewer]') AND name = N'IX_policy_viewer_policy_id')
	DROP INDEX [IX_policy_viewer_policy_id] ON [dbo].[policy_viewer]
GO

CREATE NONCLUSTERED INDEX [IX_policy_viewer_policy_id] ON [dbo].[policy_viewer]
(
	[policy_id] ASC
)
INCLUDE
(
	[editor_id]
)
