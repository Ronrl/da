USE [%dbname_hook%];

--indexes
GO

UPDATE policy_list SET ip_groups_val = '0000000' WHERE ip_groups_val IS NULL
UPDATE policy_list SET rss_val = '0000000' WHERE rss_val IS NULL
UPDATE policy_list SET screensavers_val = '0000000' WHERE screensavers_val IS NULL
UPDATE policy_list SET wallpapers_val = '0000000' WHERE wallpapers_val IS NULL
UPDATE policy_list SET lockscreens_val = '0000000' WHERE lockscreens_val IS NULL
UPDATE policy_list SET im_val = '0000000' WHERE im_val IS NULL
UPDATE policy_list SET text_templates_val = templates_val WHERE text_templates_val IS NULL
UPDATE policy_list SET text_to_call_val = '0000000' WHERE text_to_call_val IS NULL

IF EXISTS (SELECT name
           FROM   sys.indexes
           WHERE  name = N'IX_alerts_covering')
  DROP INDEX IX_alerts_covering ON alerts

IF EXISTS (SELECT name
           FROM   sys.indexes
           WHERE  name = N'IX1_users')
  DROP INDEX IX1_users ON users

IF EXISTS (SELECT name
           FROM   sys.indexes
           WHERE  name = N'IX_users_id')
  DROP INDEX IX_users_id ON users

IF EXISTS (SELECT name
           FROM   sys.indexes
           WHERE  name = N'IX_users_name_role')
  DROP INDEX IX_users_name_role ON users

IF EXISTS (SELECT name
           FROM   sys.indexes
           WHERE  name = N'IX_users_name_role_domain_id')
  DROP INDEX IX_users_name_role_domain_id ON users

IF EXISTS (SELECT name
           FROM   sys.indexes
           WHERE  name = N'IX1_computers')
  DROP INDEX IX1_computers ON computers

IF EXISTS (SELECT name
           FROM   sys.indexes
           WHERE  name = N'IX_domains_name')
  DROP INDEX IX_domains_name ON domains

IF EXISTS (SELECT name
           FROM   sys.indexes
           WHERE  name = N'IX_alerts_type_class_param_temp_id_sender_id')
	DROP INDEX IX_alerts_type_class_param_temp_id_sender_id ON alerts

IF EXISTS (SELECT name
           FROM   sys.indexes
           WHERE  name = N'IX_archive_alerts_type_class_param_temp_id_sender_id')
	DROP INDEX IX_archive_alerts_type_class_param_temp_id_sender_id ON archive_alerts
GO

DECLARE @policy_len int

SELECT @policy_len = (select distinct  character_maximum_length    from information_schema.columns  
 where table_name = 'policy_list' and data_type = 'varchar' and character_maximum_length = 7)
 
if @policy_len = 7
begin
ALTER TABLE policy_list ALTER COLUMN 
	[alerts_val] VARCHAR (8)

ALTER TABLE policy_list ALTER COLUMN 
	[emails_val] VARCHAR (8)
ALTER TABLE policy_list ALTER COLUMN 
	[sms_val] VARCHAR (8)
ALTER TABLE policy_list ALTER COLUMN 
	[surveys_val] VARCHAR (8)
ALTER TABLE policy_list ALTER COLUMN 
	[users_val] VARCHAR (8)
ALTER TABLE policy_list ALTER COLUMN 
	[groups_val] VARCHAR (8)
ALTER TABLE policy_list ALTER COLUMN 
	[templates_val] VARCHAR (8)
ALTER TABLE policy_list ALTER COLUMN 
	[text_templates_val] VARCHAR (8)


ALTER TABLE policy_list ALTER COLUMN 
	[statistics_val] VARCHAR (8)
ALTER TABLE policy_list ALTER COLUMN 
	[ip_groups_val] VARCHAR (8)
ALTER TABLE policy_list ALTER COLUMN 
	[rss_val] VARCHAR (8)
ALTER TABLE policy_list ALTER COLUMN 
	[screensavers_val] VARCHAR (8)
ALTER TABLE policy_list ALTER COLUMN 
	[wallpapers_val] VARCHAR (8)
ALTER TABLE policy_list ALTER COLUMN 
	[lockscreens_val] VARCHAR (8)
ALTER TABLE policy_list ALTER COLUMN 
	[im_val] VARCHAR (8)
ALTER TABLE policy_list ALTER COLUMN 
	[text_to_call_val] VARCHAR (8)

ALTER TABLE policy_list ALTER COLUMN 
	[feedback_val] VARCHAR (8)
ALTER TABLE policy_list ALTER COLUMN 
	[cc_val] VARCHAR (8)
ALTER TABLE policy_list ALTER COLUMN 
	[webplugin_val] VARCHAR (8)
ALTER TABLE policy_list ALTER COLUMN 
	[video_val] VARCHAR (8)
ALTER TABLE policy_list ALTER COLUMN 
	[campaign_val] VARCHAR (8)
end


--utf8
alter table [alerts]
	alter column [alert_text] nvarchar(max)
alter table [alerts]
	alter column [title] nvarchar(255)
alter table [alerts]
	alter column email_sender nvarchar(255)
alter table [alerts]
	alter column [sender_id] bigint

alter table [templates] 
	alter column [top] nvarchar(max) 
alter table [templates] 
	alter column [bottom] nvarchar(max)

alter table [surveys_main]
	alter column [question] nvarchar(max)

alter table [surveys_answers]
	alter column [answer] nvarchar(max)

alter table [edir_context]
	alter column [name] nvarchar(max)

alter table [text_templates]
	alter column [template_text] nvarchar(max)

alter table [surveys_questions]
	alter column [question] nvarchar(max)

alter table [surveys_custom_answers_stat]
	alter column [answer] nvarchar(max)

alter table [OU]
	alter column [OU_PATH] nvarchar(max)

alter table [templates_preview]
	alter column [top] nvarchar(max)
alter table [templates_preview]
	alter column [bottom] nvarchar(max)
alter table [templates_preview]
	alter column [text_template] nvarchar(max)

alter table [archive_alerts]
	alter column [alert_text] nvarchar(max)


alter table [groups]
	alter column [name] nvarchar(255)

alter table [users]
	alter column [name] nvarchar(255)
alter table [users]
	alter column [display_name] nvarchar(255)
alter table [users]
	alter column [email] nvarchar(255)

alter table [templates]
	alter column [name] nvarchar(255)

alter table [surveys_main]
	alter column [name] nvarchar(255)

alter table [domains]
	alter column [name] nvarchar(255)

alter table [text_templates]
	alter column [name] nvarchar(255)

alter table [OU]
	alter column [Name] nvarchar(255)

alter table [computers]
	alter column [name] nvarchar(255)

alter table [archive_alerts]
	alter column [title] nvarchar(255)
alter table [archive_alerts]
	alter column [email_sender] nvarchar(255)
alter table [archive_alerts]
	alter column [sender_id] bigint

alter table [ip_groups]
	alter column [name] nvarchar(255)

alter table [settings]
	alter column [val] nvarchar(MAX)

alter table [policy]
	alter column [name] nvarchar(255)


GO

IF EXISTS (SELECT name
		   FROM   sys.indexes
		   WHERE  name = N'IX_surveys_main_closed_sender_id')
  DROP INDEX IX_surveys_main_closed_sender_id ON surveys_main

GO

alter table [surveys_main]
	alter column [sender_id] bigint

GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[custom_groups]') AND type in (N'U'))
BEGIN
	DECLARE @id bigint
	DECLARE @new_id bigint
	DECLARE @name nvarchar(255)
	DECLARE @alert_id int
	DECLARE @type varchar(1)
	DECLARE @special_id varchar(50)
	DECLARE @domain_id int
	DECLARE @context_id int
	DECLARE @rss_url varchar(255)
	DECLARE @was_checked int
	DECLARE @root_group int
	DECLARE @custom_group int
	DECLARE @policy_id bigint

	DECLARE custom_groups_cursor CURSOR READ_ONLY FOR
		SELECT [id], [name], [alert_id], [type], [special_id], [domain_id], [context_id], [rss_url], [was_checked], [root_group], [custom_group]
		FROM [dbo].[custom_groups]

	OPEN custom_groups_cursor
	FETCH NEXT FROM custom_groups_cursor INTO @id, @name, @alert_id, @type, @special_id, @domain_id, @context_id, @rss_url, @was_checked, @root_group, @custom_group
	WHILE @@FETCH_STATUS = 0
	BEGIN
		INSERT INTO [dbo].[groups] ([name] ,[alert_id] ,[type] ,[special_id] ,[domain_id] ,[context_id] ,[rss_url] ,[was_checked] ,[root_group] ,[custom_group], [policy_id])
			VALUES (@name, @alert_id, @type, @special_id, @domain_id, @context_id, @rss_url, @was_checked, @root_group, @custom_group, @policy_id)
		SET @new_id = SCOPE_IDENTITY()

		UPDATE [dbo].[policy_group] SET [group_id] = @new_id WHERE [group_id] = @id

		INSERT INTO [dbo].[users_groups] ([group_id], [user_id], [was_checked])
			(SELECT @new_id, [user_id], [was_checked] FROM [dbo].[custom_group_user] WHERE [custom_group_id] = @id)

		INSERT INTO [dbo].[groups_groups] ([container_group_id], [group_id], [was_checked])
			(SELECT @new_id, [group_id], [was_checked] FROM [dbo].[custom_group_group] WHERE [custom_group_id] = @id)

		INSERT INTO [dbo].[computers_groups] ([group_id], [comp_id], [was_checked])
			(SELECT @new_id, [comp_id], [was_checked] FROM [dbo].[custom_group_computer] WHERE [custom_group_id] = @id)

		INSERT INTO [dbo].[ou_groups] ([group_id], [ou_id], [was_checked])
			(SELECT @new_id, [ou_id], [was_checked] FROM [dbo].[custom_group_ou] WHERE [custom_group_id] = @id)

		FETCH NEXT FROM custom_groups_cursor INTO @id, @name, @alert_id, @type, @special_id, @domain_id, @context_id, @rss_url, @was_checked, @root_group, @custom_group
	END
	CLOSE custom_groups_cursor
	DEALLOCATE custom_groups_cursor

	DROP TABLE [dbo].[custom_groups]
	DROP TABLE [dbo].[custom_group_user]
	DROP TABLE [dbo].[custom_group_group]
	DROP TABLE [dbo].[custom_group_computer]
	DROP TABLE [dbo].[custom_group_ou]
END

-- update version
IF EXISTS (SELECT 1 FROM version)
BEGIN
	UPDATE version SET version = '%deskalerts_version%'
END
ELSE
BEGIN
	INSERT INTO version (version) VALUES ('%deskalerts_version%')
END

--update version set contract_date=cast('%contract_date%' as datetime)

IF EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfMaxPublishersCount')
BEGIN
	UPDATE [settings] SET [val] = '%MaxPublishersCount%' WHERE [name] = 'ConfMaxPublishersCount'
END
ELSE
BEGIN
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfMaxPublishersCount','%MaxPublishersCount%','I','T')
END

IF EXISTS (SELECT 1 FROM [settings] WHERE [name]='licenses')
BEGIN
	UPDATE [settings] SET [val] = '%licenses_count%' WHERE [name] = 'licenses'
END
ELSE
BEGIN
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('licenses','%licenses_count%','I','T')
END


IF EXISTS (SELECT 1 FROM [settings] WHERE [name]='supermario')
BEGIN
	UPDATE [settings] SET [val] = '%trialseconds_count%' WHERE [name] = 'supermario'
END
ELSE
BEGIN
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('supermario','%trialseconds_count%','I','T')
END


--update not scheduled alerts to be possible to stop them
IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name] = 'ConfCanStopUsualAlert')
BEGIN
	UPDATE alerts SET schedule_type = '1' WHERE schedule = '0'
	INSERT INTO [settings] ([name], [val], [s_type], [s_part]) VALUES ('ConfCanStopUsualAlert', '1', 'I', '')
END


IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfDSRefreshRateValue')
BEGIN
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfDSRefreshRateValue','1','I','S')
END

IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfDSRefreshRateFactor')
BEGIN
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfDSRefreshRateFactor','1','I','S')
END

IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfCSVRefreshRateValue')
BEGIN
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfCSVRefreshRateValue','10','I','S')
END

IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfCSVRefreshRateFactor')
BEGIN
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfCSVRefreshRateFactor','60','I','S')
END

IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfActiveDirectoryAssociateField')
BEGIN
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfActiveDirectoryAssociateField','email','I','S')
END

IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfCsvDirectoryAssociateField')
BEGIN
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfCsvDirectoryAssociateField','E-mail','I','S')
END

IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfSmtpServer')
BEGIN
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfSmtpServer','%smtpServer%','I','S')
END


IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfSmtpUser')
BEGIN
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfSmtpUser','%smsAuthUser%','I','S')
END

IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfSmtpPort')
BEGIN
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfSmtpPort','%smtpPort%','C','S')
END

IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfSmtpSsl')
BEGIN
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfSmtpSsl','%smtpSsl%','C','S')
END

IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='usnChanged')
BEGIN
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('usnChanged','0','I','D')
END

IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfSmtpAuth')
BEGIN
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfSmtpAuth','%smtpAuth%','C','S')
END
ELSE
BEGIN
	UPDATE [settings] SET [s_type] = 'C' WHERE [name] = 'ConfSmtpAuth'
END

IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfSmtpPassword')
BEGIN
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfSmtpPassword','%smtpPassword%','I','S')
END

IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfSmtpFrom')
BEGIN
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfSmtpFrom','%smtpFrom%','I','S')
END

IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfSmtpTimeout')
BEGIN
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfSmtpTimeout','2000','C','S')
END

IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfSmsUrl')
BEGIN
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfSmsUrl','%smsUrl%','I','S')
END

IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfSmsPostData')
BEGIN
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfSmsPostData','%smsPostData%','I','S')
END

IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfSmsContentType')
BEGIN
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfSmsContentType','%smsContentType%','I','S')
END

IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfSmsContentType')
BEGIN
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfSmsContentType','%smsContentType%','I','S')
END

IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfSmsUseAuth')
BEGIN
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfSmsUseAuth','%smsUseAuth%','C','S')
END

IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfSmsAuthUser')
BEGIN
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfSmsAuthUser','%smsAuthUser%','I','S')
END

IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfSmsAuthPass')
BEGIN
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfSmsAuthPass','%smsAuthPass%','I','S')
END

IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfSmsProxyEnable')
BEGIN
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfSmsProxyEnable','%smsProxy%','C','S')
END

IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfSmsProxyServer')
BEGIN
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfSmsProxyServer','%smsProxyServer%','I','S')
END

IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfSmsProxyUser')
BEGIN
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfSmsProxyUser','%smsProxyUserAuth%','I','S')
END

IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfSmsProxyPass')
BEGIN
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfSmsProxyPass','%smsProxyPass%','I','S')
END

IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfSmsAuthType')
BEGIN
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfSmsAuthType','0','I','S')
END

IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfSmsAuthType')
BEGIN
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfSmsAuthType','','I','S')
END

IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfSmsAuthKey')
BEGIN
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfSmsAuthKey','','I','S')
END

IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConsSmsPhoneFormat')
BEGIN
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConsSmsPhoneFormat','%smsPhoneForma%','I','S')
END

IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfTextToCallSID')
BEGIN
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfTextToCallSID','%textToCallSID%','I','S')
END

IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfTextToCallAuthToken')
BEGIN
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfTextToCallAuthToken','%textToCallAuthToken%','I','S')
END

IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfTextToCallNumber')
BEGIN
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfTextToCallNumber','%textToCallNumber%','I','S')
END


IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfTraceLogLevelEnabled')
BEGIN
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfTraceLogLevelEnabled','0','C','S')
END

IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfInfoLogLevelEnabled')
BEGIN
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfInfoLogLevelEnabled','0','C','S')
END

IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfDebugLogLevelEnabled')
BEGIN
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfDebugLogLevelEnabled','0','C','S')
END

IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfErrorLogLevelEnabled')
BEGIN
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfErrorLogLevelEnabled','1','C','S')
END

IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfFatalLogLevelEnabled')
BEGIN
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfFatalLogLevelEnabled','1','C','S')
END

IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfUseGoogleTranslate')
BEGIN
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfUseGoogleTranslate','','I','S')
END

IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfGoogleTranslateKey')
BEGIN
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfGoogleTranslateKey','','I','S')
END

IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfTokenAuthEnable')
BEGIN
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfTokenAuthEnable','1','C','S')
END

IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfTokenMaxCountForUserEnable')
BEGIN
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfTokenMaxCountForUserEnable','0','C','S')
END

IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfTokenMaxCountForServerEnable')
BEGIN
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfTokenMaxCountForServerEnable','0','C','S')
END

IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfSsoEnable')
BEGIN
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfSsoEnable','0','C','S')
END

IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfTokenExpirationTimeEnable')
BEGIN
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfTokenExpirationTimeEnable','0','C','S')
END

IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfTokenMaxCountForUser')
BEGIN
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfTokenMaxCountForUser','5','I','S')
END

IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfTokenMaxCountForServer')
BEGIN
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfTokenMaxCountForServer','50','I','S')
END

IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfTokenExpirationTime')
BEGIN
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfTokenExpirationTime','30','I','S')
END

IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfLogDeleteDays')
BEGIN
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfLogDeleteDays','30','I','S')
END

IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfEnableManualImportPhones')
BEGIN
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfEnableManualImportPhones','0','C','S')
END

DELETE FROM alert_captions
WHERE id = '6b7e05b0-4e1a-41f8-a051-97c1091d4e42'

DELETE FROM alert_captions
WHERE id = '63e4a40b-e197-485e-869c-d1d9fe5318de'

DELETE FROM alert_captions
WHERE id = '97933766-7da0-4748-b855-3addc50b9343'

DELETE FROM alert_captions
WHERE id = 'e54e9b65-e03c-4ef1-981f-77180e9cffad'

DELETE FROM alert_captions
WHERE id = '6731f5a8-b7ac-448a-91fd-6a2eea1e76fd'

DELETE FROM alert_captions
WHERE id = 'f94042de-4f15-482b-9fc7-70e7b90e7e13'


delete from alert_captions
where id='20c90232-a246-4e41-90eb-55011217dbfe'

delete from alert_captions
where id='203baee6-3633-4cc6-843b-88257984c21f'

delete from alert_captions
where id='84d2289f-400b-4ab4-bca7-ca74f5165984'

delete from alert_captions
where id='e69129ae-0fe6-49f2-95ed-e295106e3e0c'

delete from alert_captions
where id='138F9281-0C11-4C48-983E-D4CA8076748B'

delete from alert_captions
where id='d09df1f1-19f5-4655-9d53-26eb25f4c656'

delete from alert_captions
where id='c28f6977-3254-418c-b96e-bda43ce94ff4'

delete from alert_captions
where id='04F84380-B31D-4167-8B83-C6642E3D25A8'

IF NOT EXISTS (SELECT id
           FROM   alert_captions
           WHERE  id = '87ebf16f-2561-4d39-8e12-00aaab3c4aa9')
	INSERT INTO alert_captions (id, name) VALUES('87ebf16f-2561-4d39-8e12-00aaab3c4aa9', N'Red')

--IF NOT EXISTS (SELECT id
--           FROM   alert_captions
--           WHERE  id = 'd09df1f1-19f5-4655-9d53-26eb25f4c656')
--	INSERT INTO alert_captions (id, name) VALUES('d09df1f1-19f5-4655-9d53-26eb25f4c656', N'Green')

IF NOT EXISTS (SELECT id
           FROM   alert_captions
           WHERE  id = 'be097872-6326-423f-bc8b-2e2daccd851b')
	INSERT INTO alert_captions (id, name) VALUES('be097872-6326-423f-bc8b-2e2daccd851b', N'Blue')
	
--IF NOT EXISTS (SELECT id
--           FROM   alert_captions
--           WHERE  id = 'c28f6977-3254-418c-b96e-bda43ce94ff4')
--	INSERT INTO alert_captions (id, name) VALUES('c28f6977-3254-418c-b96e-bda43ce94ff4', N'Bubble')
	
IF NOT EXISTS (SELECT id
           FROM   alert_captions
           WHERE  id = 'C139AFEB-3546-49D2-BD23-F4F46A3176AF')
	INSERT INTO alert_captions (id, name) VALUES('C139AFEB-3546-49D2-BD23-F4F46A3176AF', N'Yellow')
	
--IF NOT EXISTS (SELECT id
--           FROM   alert_captions
--           WHERE  id = '04F84380-B31D-4167-8B83-C6642E3D25A8')
--	INSERT INTO alert_captions (id, name) VALUES('04F84380-B31D-4167-8B83-C6642E3D25A8', N'Rounded')
	
--IF NOT EXISTS (SELECT id
--           FROM   alert_captions
--           WHERE  id = '138F9281-0C11-4C48-983E-D4CA8076748B')
--	INSERT INTO alert_captions (id, name) VALUES('138F9281-0C11-4C48-983E-D4CA8076748B', N'Note')

--IF NOT EXISTS (SELECT id
--           FROM   alert_captions
--           WHERE  id = '84d2289f-400b-4ab4-bca7-ca74f5165984')
--	INSERT INTO alert_captions (id, name) VALUES('84d2289f-400b-4ab4-bca7-ca74f5165984', N'Squares')

--IF NOT EXISTS (SELECT id
--           FROM   alert_captions
--           WHERE  id = '20c90232-a246-4e41-90eb-55011217dbfe')
--	INSERT INTO alert_captions (id, name) VALUES('20c90232-a246-4e41-90eb-55011217dbfe',N'Waves')


--DECLARE @version varchar(255)
--SET @version = '%deskalerts_version%'
--SET @version = SUBSTRING(@version, 0, CHARINDEX('.', REPLACE(@version, ',', '.'), 0))

--IF ISNUMERIC(@version) = 1 AND @version = 6
--BEGIN
--	IF NOT EXISTS (SELECT id
--	           FROM   alert_captions
--	           WHERE  id = '203baee6-3633-4cc6-843b-88257984c21f')
--		INSERT INTO alert_captions (id, name) VALUES('203baee6-3633-4cc6-843b-88257984c21f', N'Bars')
--
--	IF NOT EXISTS (SELECT id
--	           FROM   alert_captions
--	           WHERE  id = 'e69129ae-0fe6-49f2-95ed-e295106e3e0c')
--		INSERT INTO alert_captions (id, name) VALUES('e69129ae-0fe6-49f2-95ed-e295106e3e0c', N'Rounds')
--END
--ELSE
--BEGIN
--	DELETE FROM alert_captions WHERE id = '203baee6-3633-4cc6-843b-88257984c21f'
--	DELETE FROM alert_captions WHERE id = 'e69129ae-0fe6-49f2-95ed-e295106e3e0c'
--END

-- new surveys
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[alerts_received_stat]') AND type in (N'U'))
	DROP TABLE [alerts_received_stat]

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[archive_alerts_received_stat]') AND type in (N'U'))
	DROP TABLE [archive_alerts_received_stat]

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[surveys_sent]') AND type in (N'U'))
BEGIN
	DECLARE @alert_id [bigint]
	DECLARE @class [bigint]
	DECLARE @id [bigint]
	DECLARE @name [nvarchar](255)
	DECLARE @question [nvarchar](max)
	DECLARE @type [varchar](1)
	DECLARE @group_type [varchar](1)
	DECLARE @create_date [datetime]
	DECLARE @closed [varchar](1)
	DECLARE @sender_id [bigint]
	DECLARE @template_id [bigint]
	DECLARE @from_date [datetime]
	DECLARE @to_date [datetime]
	DECLARE @schedule [varchar](1)
	DECLARE @stype [smallint]
	DECLARE @schedule_type [varchar](1)

	DECLARE surveys_main_cursor CURSOR FORWARD_ONLY FOR
		SELECT [id], [name], [question], [type], [group_type], [create_date], [closed], [sender_id], [template_id], [from_date], [to_date], [schedule], [stype]
		FROM [surveys_main]
		WHERE [closed] IN ('Y', 'N')

	OPEN surveys_main_cursor
	FETCH NEXT FROM surveys_main_cursor INTO @id, @name, @question, @type, @group_type, @create_date, @closed, @sender_id, @template_id, @from_date, @to_date, @schedule, @stype
	BEGIN
		IF @closed = 'N'
			SET @schedule_type = '1'
		ELSE
			SET @schedule_type = '0'
			
		SET @class = 64
		IF @stype = 2
			SET @class = 128
		IF @stype = 3
			SET @class = 256

		INSERT INTO [alerts] ([alert_text], [title], [type], [group_type], [create_date], [sent_date], [type2], [from_date], [to_date], [schedule], [schedule_type], [recurrence], [urgent], [toolbarmode], [deskalertsmode], [sender_id], [template_id], [aknown], [ticker], [fullscreen], [email_sender], [email], [sms], [desktop], [autoclose], [class], [resizable], [post_to_blog], [social_media], [self_deletable], [text_to_call])
			VALUES (@question, @name, 'S', 'B', @create_date, @create_date, @type, @from_date, @to_date, @schedule, @schedule_type, '0', '0', '0', '1', @sender_id, @template_id, '0', '0', '0', '', '0', '0', '1', '0', @class, 0, 0, 0, 0, 0)
		SET @alert_id = SCOPE_IDENTITY()
		
		UPDATE [surveys_main] SET [sender_id] = @id WHERE CURRENT OF surveys_main_cursor

		INSERT INTO [alerts_sent] ([alert_id], [user_id])
			(SELECT @alert_id, [user_id] FROM [surveys_sent] WHERE [survey_id] = @id)

		INSERT INTO [alerts_sent_comp] ([alert_id], [comp_id])
			(SELECT @alert_id, [comp_id] FROM [surveys_sent_comp] WHERE [survey_id] = @id)

		INSERT INTO [alerts_sent_group] ([alert_id], [group_id])
			(SELECT @alert_id, [group_id] FROM [surveys_sent_group] WHERE [survey_id] = @id)

		INSERT INTO [alerts_sent_iprange] ([alert_id], [ip_from], [ip_to])
			(SELECT @alert_id, [ip_from], [ip_to] FROM [surveys_sent_iprange] WHERE [survey_id] = @id)

		INSERT INTO [alerts_sent_stat] ([alert_id], [user_id], [date])
			(SELECT @alert_id, [user_id], [date] FROM [surveys_sent_stat] WHERE [survey_id] = @id)

		INSERT INTO [alerts_received] ([alert_id], [user_id], [clsid], [date], [occurrence], [vote])
			(SELECT @alert_id, [user_id], [clsid], [date], 1, [vote] FROM [surveys_received_stat] WHERE [survey_id] = @id)

		INSERT INTO [alerts_comp_stat] ([alert_id], [comp_id], [date])
			(SELECT @alert_id, [comp_id], [date] FROM [surveys_comp_stat] WHERE [survey_id] = @id)

		INSERT INTO [alerts_group_stat] ([alert_id], [group_id], [date])
			(SELECT @alert_id, [group_id], [date] FROM [surveys_group_stat] WHERE [survey_id] = @id)

		FETCH NEXT FROM surveys_main_cursor INTO @id, @name, @question, @type, @group_type, @create_date, @closed, @sender_id, @template_id, @from_date, @to_date, @schedule, @stype
	END
	CLOSE surveys_main_cursor
	DEALLOCATE surveys_main_cursor
	
	DROP TABLE [surveys_sent]
	DROP TABLE [surveys_sent_comp]
	DROP TABLE [surveys_sent_group]
	DROP TABLE [surveys_sent_iprange]
	DROP TABLE [surveys_sent_stat]
	DROP TABLE [surveys_received]
	DROP TABLE [surveys_received_stat]
	DROP TABLE [surveys_comp_stat]
	DROP TABLE [surveys_group_stat]
END

-- widgets
-- delete discontinued widgets from "Add Widget" list
DELETE FROM widgets WHERE href='inst_mes'
DELETE FROM widgets WHERE href='one_button_solution'
DELETE FROM widgets WHERE href='stats'
DELETE FROM widgets WHERE href='stats1'
DELETE FROM widgets WHERE href='WidgetRealtime.aspx'

DELETE FROM widgets;


IF NOT EXISTS (SELECT id
           FROM   widgets
           WHERE  id = 'a6c3bd7f-733c-420a-a709-670f024704eb')
	INSERT INTO widgets (id, title, href) VALUES('a6c3bd7f-733c-420a-a709-670f024704eb', N'Users Statistics', N'WidgetUsersStats.aspx')
	
IF NOT EXISTS (SELECT id
           FROM   widgets
           WHERE  id = 'd24bbf25-1fa8-43c9-8996-1dfd76b84387')
	INSERT INTO widgets (id, title, href) VALUES('d24bbf25-1fa8-43c9-8996-1dfd76b84387', N'Last Alerts', N'WidgetLastContent.aspx')
	
IF NOT EXISTS (SELECT id
           FROM   widgets
           WHERE  id = '8faeb4ce-542e-40d4-97da-ac15cdcd145d')
	INSERT INTO widgets (id, title, href) VALUES('8faeb4ce-542e-40d4-97da-ac15cdcd145d', N'Alert Details', N'WidgetAlertsDetails.aspx')

IF NOT EXISTS (SELECT id
           FROM   widgets
           WHERE  id = 'e870e444-3a4f-4b39-b34d-0f54a4b997fc')
	INSERT INTO widgets (id, title, href) VALUES('e870e444-3a4f-4b39-b34d-0f54a4b997fc', N'Survey Answers', N'WidgetSurveysStat.aspx')
	
IF NOT EXISTS (SELECT id
           FROM   widgets
           WHERE  id = 'f76c8abc-c20a-42a0-ad0a-f2a356889b37')
	INSERT INTO widgets (id, title, href) VALUES('f76c8abc-c20a-42a0-ad0a-f2a356889b37', N'Survey Details', N'WidgetSurveysDetails.aspx')
	
IF NOT EXISTS (SELECT id
           FROM   widgets
           WHERE  id = '55a513f6-48ab-490a-b13d-7d0c31cff63c')
	INSERT INTO widgets (id, title, href) VALUES('55a513f6-48ab-490a-b13d-7d0c31cff63c', N'User Details', N'WidgetUsersDetails.aspx')
	

	
IF NOT EXISTS (SELECT id
           FROM   widgets
           WHERE  id = 'b9b41165-7bda-4b4a-9626-d346957bddaf')
	INSERT INTO widgets (id, title, href) VALUES('b9b41165-7bda-4b4a-9626-d346957bddaf', N'Instant Send', N'WidgetInstants.aspx')
	
IF NOT EXISTS (SELECT id
           FROM   widgets
           WHERE  id = '071954af-ad48-4630-bd1e-8193691fc9a1')
	INSERT INTO widgets (id, title, href) VALUES('071954af-ad48-4630-bd1e-8193691fc9a1', N'Create Alert', N'widget_create_alert.html')

-- less the priority, earlier the widget is in the list. Intervals left for later widgets.
-- delete discontinued widgets from dashboard
DELETE FROM widgets_editors WHERE widget_id = '7d513b11-bbe8-45ee-937c-922adcfb107b'
DELETE FROM widgets_editors WHERE widget_id = 'e745170b-4243-4bec-a8b4-56def37dd7ae'

IF EXISTS (SELECT id FROM widgets WHERE priority IS NULL)
BEGIN
	--UPDATE widgets SET priority=1 WHERE id='7d513b11-bbe8-45ee-937c-922adcfb107b'
	UPDATE widgets SET priority=5 WHERE id='d24bbf25-1fa8-43c9-8996-1dfd76b84387'
	UPDATE widgets SET priority=7 WHERE id='b9b41165-7bda-4b4a-9626-d346957bddaf'
	UPDATE widgets SET priority=8 WHERE id='071954af-ad48-4630-bd1e-8193691fc9a1'
	UPDATE widgets SET priority=9 WHERE id='e9f8b3d3-b508-4288-a04e-2ebe49712e50'
	UPDATE widgets SET priority=13 WHERE id='55a513f6-48ab-490a-b13d-7d0c31cff63c'
	UPDATE widgets SET priority=17 WHERE id='a6c3bd7f-733c-420a-a709-670f024704eb'
	UPDATE widgets SET priority=21 WHERE id='8faeb4ce-542e-40d4-97da-ac15cdcd145d'
	UPDATE widgets SET priority=25 WHERE id='f76c8abc-c20a-42a0-ad0a-f2a356889b37'
	UPDATE widgets SET priority=29 WHERE id='e870e444-3a4f-4b39-b34d-0f54a4b997fc'
END

UPDATE users SET start_page = 'dashboard.aspx' WHERE start_page = 'dashboard.asp'
UPDATE users SET start_page = 'dashboard.aspx' WHERE start_page = 'feedback_show.asp'
UPDATE users SET start_page = 'CreateAlert.aspx' WHERE start_page = 'edit_alert_db.asp'
UPDATE users SET start_page = 'PopupSent.aspx' WHERE start_page = 'sent.asp'
UPDATE users SET start_page = 'PopupSent.aspx?draft=1' WHERE start_page = 'draft.asp'
UPDATE users SET start_page = 'TextTemplates.aspx' WHERE start_page = 'text_templates.asp'
UPDATE users SET start_page = 'Campaign.aspx' WHERE start_page = 'campaign.asp'
UPDATE users SET start_page = 'DigitalSignageList.aspx' WHERE start_page = 'DigSignLinks.asp'
UPDATE users SET start_page = 'CreateAlert.aspx?webplugin=1' WHERE start_page = 'edit_alert_db.asp?webplugin=1'
UPDATE users SET start_page = 'wallpaper.aspx' WHERE start_page = 'wallpaper.asp'
UPDATE users SET start_page = 'wallpaper.aspx?draft=1' WHERE start_page = 'wallpaper.asp?draft=1'
UPDATE users SET start_page = 'CreateAlert.aspx?instant=1' WHERE start_page = 'edit_alert_db.asp?instant=1'
UPDATE users SET start_page = 'ColorCodes.aspx' WHERE start_page = 'color_codes.asp'
UPDATE users SET start_page = 'PolicyList.aspx' WHERE start_page = 'policy_list.asp'
UPDATE users SET start_page = 'screensavers.aspx' WHERE start_page = 'screensavers.asp'
UPDATE users SET start_page = 'screensavers.aspx?draft=1' WHERE start_page = 'screensavers.asp?draft=1'
UPDATE users SET start_page = 'synchronizations.aspx' WHERE start_page = 'synchronizations.asp'
UPDATE users SET start_page = 'Organizations/Index.aspx' WHERE start_page = 'ou_new_interface.asp'
UPDATE users SET start_page = 'domains.aspx' WHERE start_page = 'admin_domains.asp'
UPDATE users SET start_page = 'PublisherList.aspx' WHERE start_page = 'admin_admin.asp'

-- Add SMPP configuration
IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfSmppEnabled')
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfSmppEnabled','0','C','S')
	
IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfSmppAutoReconnectDelay')
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfSmppAutoReconnectDelay','5000','C','S')
	
IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfSmppDefaultServiceType')
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfSmppDefaultServiceType','','I','S')

IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfSmppEncoding')
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfSmppEncoding','8','I','S')
	
IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfSmppHost')
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfSmppHost','','I','S')
	
IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfSmppPassword')
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfSmppPassword','','I','S')
	
IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfSmppPort')
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfSmppPort','0','C','S')
	
IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfSmppReconnectInteval')
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfSmppReconnectInteval','10000','C','S')
	
IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfSmppSourceAddress')
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfSmppSourceAddress','','I','S')
	
IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfSmppSystemID')
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfSmppSystemID','','I','S')
	
IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfSmppSystemType')
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfSmppSystemType','','I','S')
	
IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfSmppTimeOut')
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfSmppTimeOut','60000','C','S')
	
IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfSmppRegisteredDeliveryValue')
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfSmppRegisteredDeliveryValue','16','C','S')
	
IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfSmppKeepAliveInterval')
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfSmppKeepAliveInterval','0','C','S')
	
-- Add autoupdate configuration
IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfAutoupdateSlotCacheLength')
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfAutoupdateSlotCacheLength','100','I','S')
	IF NOT EXISTS (SELECT 1 FROM [settings] WHERE [name]='ConfAutoupdateSlotLifeTime')
	INSERT INTO [settings] (name,val,s_type,s_part) VALUES ('ConfAutoupdateSlotLifeTime','30','I','S')
	



	