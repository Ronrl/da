﻿USE [%dbname_hook%];

DECLARE @numberOfYears INT;
SET @numberOfYears = 10;
SET LANGUAGE british;
UPDATE [version]
  SET 
      [contract_date] = DATEADD(year, @numberOfYears, GETDATE()), 
      [license_expire_date] = DATEADD(year, @numberOfYears, GETDATE());
