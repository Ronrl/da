﻿USE [%dbname_hook%];

IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE table_name = 'ContentSettings'
)
BEGIN
    CREATE TABLE [dbo].[ContentSettings]
    (
        [Id]		[BIGINT] IDENTITY NOT NULL, 
        [Name]	    NVARCHAR(100) NOT NULL, 
        CONSTRAINT	[PK_ContentSettings] PRIMARY KEY CLUSTERED([Id] ASC), 
    )
    ON [PRIMARY];
END;

BEGIN
    DECLARE @value NVARCHAR(MAX);
    
    SET @value = 'Acknowledgement';
    IF ( SELECT COUNT([Id]) FROM [ContentSettings] WHERE [ContentSettings].[Name] = @value ) < 1
    BEGIN
        INSERT INTO [ContentSettings]([Name]) VALUES (@value);
    END;
    SET @value = 'Auto-close';
    IF ( SELECT COUNT([Id]) FROM [ContentSettings] WHERE [ContentSettings].[Name] = @value ) < 1
    BEGIN
        INSERT INTO [ContentSettings]([Name]) VALUES (@value);
    END;
    SET @value = 'Full screen';
    IF ( SELECT COUNT([Id]) FROM [ContentSettings] WHERE [ContentSettings].[Name] = @value ) < 1
    BEGIN
        INSERT INTO [ContentSettings]([Name]) VALUES (@value);
    END;
    SET @value = 'High-priority';
    IF ( SELECT COUNT([Id]) FROM [ContentSettings] WHERE [ContentSettings].[Name] = @value ) < 1
    BEGIN
        INSERT INTO [ContentSettings]([Name]) VALUES (@value);
    END;
    SET @value = 'Print button';
    IF ( SELECT COUNT([Id]) FROM [ContentSettings] WHERE [ContentSettings].[Name] = @value ) < 1
    BEGIN
        INSERT INTO [ContentSettings]([Name]) VALUES (@value);
    END;
    SET @value = 'Self-destructing';
    IF ( SELECT COUNT([Id]) FROM [ContentSettings] WHERE [ContentSettings].[Name] = @value ) < 1
    BEGIN
        INSERT INTO [ContentSettings]([Name]) VALUES (@value);
    END;
    SET @value = 'Unobtrusive';
    IF ( SELECT COUNT([Id]) FROM [ContentSettings] WHERE [ContentSettings].[Name] = @value ) < 1
    BEGIN
        INSERT INTO [ContentSettings]([Name]) VALUES (@value);
    END;
END;