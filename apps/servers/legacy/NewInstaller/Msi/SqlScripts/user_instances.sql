USE [%dbname_hook%];

IF EXISTS
(
    SELECT 1
    FROM dbo.sysobjects
    WHERE id = OBJECT_ID(N'[UQ__userInstances_userId_clsId]')
          AND type = 'K'
)
    BEGIN
        ALTER TABLE [dbo].[user_instances] DROP CONSTRAINT [UQ__userInstances_userId_clsId];
    END;

BEGIN
    WITH CTE
     AS (SELECT [user_id], 
                [clsid], 
                rn = ROW_NUMBER() OVER(PARTITION BY [user_id], 
                                                    [clsid]
                ORDER BY [user_id])
         FROM [dbo].[user_instances])
     DELETE FROM CTE
     WHERE rn > 1;
END;

IF NOT EXISTS
(
    SELECT 1
    FROM dbo.sysobjects
    WHERE id = OBJECT_ID(N'[UQ__userInstances_userId_clsId]')
          AND type = 'K'
)
    BEGIN
        ALTER TABLE [dbo].[user_instances]
        ADD CONSTRAINT [UQ__userInstances_userId_clsId] UNIQUE([user_id], [clsid]);
    END;