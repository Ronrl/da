USE [%dbname_hook%];

IF EXISTS
(
    SELECT 1
    FROM dbo.sysobjects
    WHERE id = OBJECT_ID(N'[UQ__domains_name]')
          AND type = 'K'
)
    BEGIN
        ALTER TABLE [dbo].[domains] DROP CONSTRAINT [UQ__domains_name];
    END;

WITH Cte
     AS (SELECT [name], 
                rn = ROW_NUMBER() OVER(PARTITION BY [name]
                ORDER BY [name])
         FROM [dbo].[domains])
     DELETE FROM Cte
     WHERE rn > 1;

IF NOT EXISTS
(
    SELECT 1
    FROM dbo.sysobjects
    WHERE id = OBJECT_ID(N'[UQ__domains_name]')
          AND type = 'K'
)
    BEGIN
        ALTER TABLE [dbo].[domains]
        ADD CONSTRAINT [UQ__domains_name] UNIQUE([name]);
    END;