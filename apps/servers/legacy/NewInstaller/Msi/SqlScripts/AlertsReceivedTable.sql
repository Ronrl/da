USE [%dbname_hook%];

--- Clear duplicates in alerts_received
GO
IF EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[KEY_alerts_received]') AND type = 'K')
BEGIN
	ALTER TABLE [alerts_received]
	DROP constraint [KEY_alerts_received];
END
GO
IF EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
		WHERE column_name ='clsid'
		AND table_name = 'alerts_received' )
BEGIN
UPDATE [alerts_received] set [alerts_received].[clsid] = REPLACE(REPLACE(clsid, '{', ''), '}', '')
END
GO

WITH CTE AS(
   SELECT [alert_id], [user_id], [clsid],
       RN = ROW_NUMBER()OVER(PARTITION BY [alert_id], [user_id], clsid ORDER BY [alert_id], [user_id])
   FROM [dbo].[alerts_received]
)
DELETE FROM CTE WHERE RN > 1
GO
IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[KEY_alerts_received]') AND type = 'K')
BEGIN
	ALTER TABLE [dbo].[alerts_received]  ADD CONSTRAINT [KEY_alerts_received] UNIQUE (clsid, alert_id, user_id)
END
