USE [%dbname_hook%];

SET LANGUAGE British

-- Add demo user
IF NOT EXISTS ( SELECT 1 FROM users WHERE [name] = 'demo' )
BEGIN
	INSERT INTO users ([name], [pass], [role], [reg_date]) VALUES ('demo', 'C378674CE8C697E8CE75EBC8F1B94351', 'E', GETDATE());
	INSERT INTO policy_editor ([policy_id], [editor_id], [was_checked]) VALUES (1, SCOPE_IDENTITY(), 1);
END
GO

-- Add sales managers
IF NOT EXISTS ( SELECT 1 FROM users WHERE [name] = 'evgen' )
BEGIN
	INSERT INTO users ([name], [pass], [role], [reg_date]) VALUES ('evgen', '79138D3403B0DC98555428F15E68CDA4', 'E', GETDATE());
	INSERT INTO policy_editor ([policy_id], [editor_id], [was_checked]) VALUES (1, SCOPE_IDENTITY(), 1);
END
GO

IF NOT EXISTS ( SELECT 1 FROM users WHERE [name] = 'mila' )
BEGIN
	INSERT INTO users ([name], [pass], [role], [reg_date]) VALUES ('mila', 'ABF0EC7E667380F7976189FAD249A178', 'E', GETDATE());
	INSERT INTO policy_editor ([policy_id], [editor_id], [was_checked]) VALUES (1, SCOPE_IDENTITY(), 1);
END
GO

IF NOT EXISTS ( SELECT 1 FROM users WHERE [name] = 'yuri' )
BEGIN
	INSERT INTO users ([name], [pass], [role], [reg_date]) VALUES ('yuri', 'A65FC797FDC94C90488B11E05D5D10D4', 'E', GETDATE());
	INSERT INTO policy_editor ([policy_id], [editor_id], [was_checked]) VALUES (1, SCOPE_IDENTITY(), 1);
END
GO

IF NOT EXISTS ( SELECT 1 FROM users WHERE [name] = 'alex' )
BEGIN
	INSERT INTO users ([name], [pass], [role], [reg_date]) VALUES ('alex', '0DB0D7521ABE07E6B0D297FF24E72D9D', 'E', GETDATE());
	INSERT INTO policy_editor ([policy_id], [editor_id], [was_checked]) VALUES (1, SCOPE_IDENTITY(), 1);
END
GO

-- Enable non AD/ED members to register and receive alerts  
UPDATE [dbo].[settings]
   SET [val] = 1
 WHERE [name] = 'ConfEnableRegAndADEDMode'
GO

-- Update password for admin user
UPDATE [dbo].[users]
   SET [pass] = '58BDF2ADF8A731A0EB8760582B46DD3A'
 WHERE [name] = 'admin'
GO