﻿USE [%dbname_hook%];

IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE table_name = 'policy_skins'
)
BEGIN
    CREATE TABLE [dbo].[policy_skins]
    (
        [id]		[BIGINT] IDENTITY NOT NULL, 
        [policy_id]	[BIGINT] NOT NULL, 
        [skin_id]	[UNIQUEIDENTIFIER] NOT NULL, 
        CONSTRAINT	[PK_policy_skins] PRIMARY KEY CLUSTERED([id] ASC), 
        CONSTRAINT	[FK_policy_skins_to_policy] FOREIGN KEY(policy_id) REFERENCES [policy](id) ON DELETE CASCADE, 
        CONSTRAINT	[FK_policy_skins_to_skins] FOREIGN KEY(skin_id) REFERENCES [alert_captions](id) ON DELETE CASCADE
    )
    ON [PRIMARY];
END;

IF
(
    SELECT COUNT(*)
    FROM [policy_skins]
) <= 0 
BEGIN
    INSERT INTO [policy_skins] ([policy_id], [skin_id])
        (
            SELECT [policy].[id] AS [policy_id],
                   [alert_captions].[id] AS [skin_id]
            FROM [alert_captions]
            CROSS JOIN [policy]
        );
END;