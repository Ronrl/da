CREATE LOGIN [%user_name_hook%] FROM WINDOWS;
USE [%dbname_hook%];
GO
CREATE USER [%user_name_hook%] FOR LOGIN [%user_name_hook%];
EXEC sp_addrolemember 'db_datareader', '%user_name_hook%';
EXEC sp_addrolemember 'db_datawriter', '%user_name_hook%';
EXEC sp_addrolemember 'db_owner', '%user_name_hook%';
