USE [%dbname_hook%];

IF EXISTS
(
    SELECT 1
    FROM dbo.sysobjects
    WHERE id = OBJECT_ID(N'[UQ__users_name_domainid_role]')
          AND type = 'K'
)
    BEGIN
        ALTER TABLE [dbo].[users] DROP CONSTRAINT [UQ__users_name_domainid_role];
    END;

WITH Cte
     AS (SELECT [name], 
                [domain_id], 
                [role], 
                rn = ROW_NUMBER() OVER(PARTITION BY [name], 
                                                    [domain_id], 
                                                    [role]
                ORDER BY [name])
         FROM [dbo].[users])
     DELETE FROM Cte
     WHERE rn > 1;

IF NOT EXISTS
(
    SELECT 1
    FROM dbo.sysobjects
    WHERE id = OBJECT_ID(N'[UQ__users_name_domainid_role]')
          AND type = 'K'
)
    BEGIN
        ALTER TABLE [dbo].[users]
        ADD CONSTRAINT [UQ__users_name_domainid_role] UNIQUE([name], [domain_id], [role]);
    END;