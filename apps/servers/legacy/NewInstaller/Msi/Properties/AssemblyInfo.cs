﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("DeskAlerts")]
[assembly: AssemblyDescription("DeskAlerts Server")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Softomate")]
[assembly: AssemblyProduct("DeskAlerts")]
[assembly: AssemblyCopyright("Copyright ©Softomate LLC  2017")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("c667963f-599f-4181-a422-bd938aee1a8a")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("9.0.2.28")]
[assembly: AssemblyFileVersion("9.0.2.28")]
