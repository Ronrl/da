﻿using System;
using System.Windows;
using System.Linq;
using Microsoft.Win32;
using System.Windows.Media.Imaging;
using Microsoft.Web.Administration;
using Application = System.Windows.Application;

namespace WpfSetup
{
    public partial class MainWindow
    {
        public string WindowsVersionString { get; set; }
        public string IisVersionString { get; set; }
        public string SqlVersionString { get; set; }
        public string NetFrameworkVersionString { get; set; }
        // Checking the version using >= will enable forward compatibility, 
        // however you should always compile your code on newer versions of
        // the framework to ensure your app works the same.
        private bool Get45Or451FromRegistry()
        {
            try
            {
                RegistryKey ndpKey =
                    Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\NET Framework Setup\\NDP\\v4\\Full\\",
                        RegistryKeyPermissionCheck.ReadSubTree);
                if (ndpKey != null && ndpKey.GetValue("Release") != null)
                {
                    return CheckFor45DotVersion((int) ndpKey.GetValue("Release"));
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }

        private bool CheckFor45DotVersion(int releaseKey)
        {
            try
            {

                if ((releaseKey >= 378389))
                {
                    NetFrameworkVersionString = "4.5+";
                    return true;
                }
                else
                {
                    NetFrameworkVersionString = "4.5 will be installed";
                    // This line should never execute. A non-null release key should mean
                    // that 4.5 or later is installed.
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }


        public static BitmapSource LoadBitmap(System.Drawing.Bitmap source)
        {
            try
            {
                IntPtr ip = source.GetHbitmap();
                var bs = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(ip,
                    IntPtr.Zero, Int32Rect.Empty,
                    BitmapSizeOptions.FromEmptyOptions());
                return bs;
            }
            catch
            {
                return null;
            }
        }



        void WriteToSystemRequirementsTab()
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(() =>
            {
                if (!string.IsNullOrEmpty(WindowsVersionString))
                {
                    WindowsVersion.Text = WindowsVersionString;
                }
                if (!string.IsNullOrEmpty(IisVersionString))
                {
                    IisVersion.Text = IisVersionString;
                }
                if (!string.IsNullOrEmpty(NetFrameworkVersionString))
                {
                    NetFrameworkVersion.Text = NetFrameworkVersionString;
                }
            }));
        }

        bool CheckIisVersion()
        {
            try
            {
                RegistryKey iisKey =
                    Registry.LocalMachine.OpenSubKey(@"Software\Microsoft\InetStp",
                        RegistryKeyPermissionCheck.ReadSubTree);
                if (iisKey != null)
                {
                    IisVersionString = iisKey.GetValue("MajorVersion").ToString();
                    if ((int) iisKey.GetValue("MajorVersion") < 5)
                    {
                        return false;
                    }
                    ServerManager serverManager = ServerManager.OpenRemote("localhost");
                    if (serverManager == null)
                        return false;
                }
            }
            catch
            {
                return false;
            }
            return false;
        }

        bool CheckAsPinstalled()
        {
            try
            {
                RegistryKey ndpKey =
                    Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\InetStp\\Components\\",
                        RegistryKeyPermissionCheck.ReadSubTree);
                if (ndpKey != null && ndpKey.GetValue("ASP") != null)
                {
                    return (int) ndpKey.GetValue("ASP") == 1;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }


        bool CheckAspNetRegistered()
        {
            if (Environment.OSVersion.Version.Minor < 2)
            {
                return true;
            }
            else
            {
                
                using (var mgr = new ServerManager())
                {
                    return mgr.ApplicationPools.Any(pool => pool.ManagedRuntimeVersion == "v4.0");
                }
            }
        }

        bool CheckAspNetInstalled()
        {

            try
            {

           
                if (Environment.OSVersion.Version.Minor<2)
                {
                   /* var feature = DismApi.GetFeatures(session).ToList().FirstOrDefault(a => a.FeatureName=="IIS-ASPNET");
                    if (feature != null && feature.State == DismPackageFeatureState.Staged)
                    {
                    
                        return true;
                    }
                    else
                    {
                        return false;
                    }*/
                    return true;
                }
                else
                {
                    Microsoft.Dism.DismApi.Initialize(Microsoft.Dism.DismLogLevel.LogErrors);
                    using (Microsoft.Dism.DismSession session = Microsoft.Dism.DismApi.OpenOnlineSession())
                    {
                        var features =
                            Microsoft.Dism.DismApi.GetFeatures(session)
                                .ToList()
                                .Where(a => a.FeatureName.Contains("IIS-ASPNET45"))
                                .ToList();
                        if (features.Count > 0)
                        {
                            foreach (var feature in features)
                            {
                                return feature != null && feature.State == Microsoft.Dism.DismPackageFeatureState.Staged;
                            }
                        }
                        else
                        {
                            return false;
                        }
                        Microsoft.Dism.DismApi.Shutdown();
                    }
                    
                }
            return true;
            }
            catch (Exception)
            {
                return true;
            }
        }

        bool CheckSystemRequirements()
        {
            //check os version
            int k = 0;
            WindowsVersionString = Environment.OSVersion.Version.Major.ToString();
            if (int.Parse(WindowsVersionString) < 6)
            {
                Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                {
                    WindowsVersionImage.Source = LoadBitmap(Properties.Resources.error_icon);
                }));
                k++;
            }
            else
            {
                Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                {
                    WindowsVersionImage.Source = LoadBitmap(Properties.Resources.sucsess_icon);
                }));
            }

            //check IIS version
            if (CheckIisVersion())
            {
                Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                {
                    IisVersionImage.Source = LoadBitmap(Properties.Resources.sucsess_icon);
                }));
            }
            else
            {
                k++;
                Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                {
                    IisVersionImage.Source = LoadBitmap(Properties.Resources.error_icon);
                }));
            }
            if (CheckAspNetInstalled())
            {
                if (CheckAspNetRegistered())
                {
                    Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        ASPNETFeaturesImage.Source = LoadBitmap(Properties.Resources.sucsess_icon);
                    }));
                }
                else
                {
                    k++;
                    Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        AspNetText.Text = "Not Registered.";
                        ASPNETFeaturesImage.Source = LoadBitmap(Properties.Resources.error_icon);
                    }));
                }
            }
            else
            {
                k++;
                Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                {
                    AspNetText.Text = "Not Registered.";
                    ASPNETFeaturesImage.Source = LoadBitmap(Properties.Resources.error_icon);
                }));
            }
            if (CheckAsPinstalled())
            {
                Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                {
                    ASPFeaturesImage.Source = LoadBitmap(Properties.Resources.sucsess_icon);
                }));
            }
            else
            {
                k++;
                Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                {
                    ASPFeaturesImage.Source = LoadBitmap(Properties.Resources.error_icon);
                }));
            }

            
            //check framework version

            if (Get45Or451FromRegistry())
            {
                Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                {
                    NetFrameworkVersionImage.Source = LoadBitmap(Properties.Resources.sucsess_icon);
                }));
            }
            else
            {
                try
                {
                    k++;
                    Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                    {

                        NetFrameworkVersionImage.Source = LoadBitmap(Properties.Resources.error_icon);
                    }));

                }
                catch (Exception)
                {

                    k++;
                }

               
            }
            //write to SystemRequiremets tab
            WriteToSystemRequirementsTab();
            return k <= 0;
        }


    }
}
