﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows;
using System.Windows.Media;
using System.Windows.Controls;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;

namespace WpfSetup
{
    public partial class MainWindow
    {
        public bool SqlUseWindowsAuthVar;
        public bool UseAzureAuth;
        public string SqlHostVar;
        public string SqlLoginVar;
        public string SqlPassworVar;
        public SqlConnection SqlConn;
        public string SqlDatabaseVar;
        public bool SqlDropDatabase;
        public bool NewDataBase { get; set; }
        public string GetConnectionString()
        {
            string connectionStrig;
            if (UseAzureAuth)
            {
                connectionStrig =
                    $"Server=tcp:{SqlHostVar}.database.windows.net,1433;Initial Catalog={SqlDatabaseVar};Persist Security Info=False;User ID='{SqlLoginVar}';Password='{SqlPassworVar}';MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
            }
            else if (SqlUseWindowsAuthVar)
            {
                connectionStrig = $"Server={SqlHostVar};Integrated Security=True;";
            }
            else
            {
                connectionStrig =
                    $"Server={SqlHostVar};Integrated Security=false; user id='{SqlLoginVar}';Password='{SqlPassworVar}';";
            }

            return connectionStrig;
        }

        bool CheckSqlConnection()
        {
            SqlConn = new SqlConnection(GetConnectionString());
            try
            {
                SqlConn.Open();
                return SqlConn.State == ConnectionState.Open;
            }
            catch
            {
                return false;
            }
            finally
            {
                if (SqlConn.State == ConnectionState.Open)
                {
                    SqlConn.Close();
                }
            }
        }

        private bool CheckUserPrevilegies()
        {
            if (UseAzureAuth)
            {
                return true;
            }

            SqlConn = new SqlConnection(GetConnectionString());
            var randomdbname = new Random((int)DateTime.Now.Ticks).Next().ToString();
            var createstr = "CREATE DATABASE \"" + randomdbname + "\";";
            var deletestr = "DROP DATABASE \"" + randomdbname + "\";";
            var createCommand = new SqlCommand(createstr, SqlConn);
            var deleteCommand = new SqlCommand(deletestr, SqlConn);
            try
            {
                SqlConn.Open();
                createCommand.ExecuteNonQuery();
                deleteCommand.ExecuteNonQuery();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                if (SqlConn.State == ConnectionState.Open)
                {
                    SqlConn.Close();
                }
            }
        }

        public static bool CheckDatabaseExists(string connectionString, string databaseName)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand(string.Format(
                       "SELECT db_id('{0}')", databaseName), connection))
                {
                    connection.Open();
                    return (command.ExecuteScalar() != DBNull.Value);
                }
            }
        }

        private void SQLDatabase_TextChanged(object sender, TextChangedEventArgs e)
        {
            SqlDatabaseVar = SqlDatabase.Text;

        }




        private void SQlUseWindowsAuth_Checked(object sender, RoutedEventArgs e)
        {
            UseAzureAuth = false;
            SqlLogin.IsEnabled = false;
            SqlPassword.IsEnabled = false;
            SqlLogin.Visibility = Visibility.Collapsed;
            SqlLoginText.Visibility = Visibility.Collapsed;
            SqlPassword.Visibility = Visibility.Collapsed;
            SqlPasswordText.Visibility = Visibility.Collapsed;
            SqlUseWindowsAuthVar = true;
            SQlNotUseWindowsAuth.IsChecked = false;
        }
        private void SQlNotUseWindowsAuth_Checked(object sender, RoutedEventArgs e)
        {
            UseAzureAuth = false;
            SqlLogin.IsEnabled = true;
            SqlPassword.IsEnabled = true;
            SqlLogin.Visibility = Visibility.Visible;
            SqlLoginText.Visibility = Visibility.Visible;
            SqlPassword.Visibility = Visibility.Visible;
            SqlPasswordText.Visibility = Visibility.Visible;
            SqlUseWindowsAuthVar = false;
            SQlUseWindowsAuth.IsChecked = false;
        }

        private void SQlUseAzureAuth_Checked(object sender, RoutedEventArgs e)
        {
            UseAzureAuth = true;
            SqlLogin.IsEnabled = true;
            SqlPassword.IsEnabled = true;
            SqlLogin.Visibility = Visibility.Visible;
            SqlLoginText.Visibility = Visibility.Visible;
            SqlPassword.Visibility = Visibility.Visible;
            SqlPasswordText.Visibility = Visibility.Visible;
            SqlUseWindowsAuthVar = false;
            SQlUseWindowsAuth.IsChecked = false;
        }

        private void SQLHost_TextChanged(object sender, TextChangedEventArgs e)
        {
            SqlHostVar = SqlHost.Text;
            SqlHost.Background = Brushes.White;
        }
        private void SQLLogin_TextChanged(object sender, TextChangedEventArgs e)
        {
            SqlLoginVar = SqlLogin.Text;
            SqlLogin.Background = Brushes.White;
        }


        private void SqlPassword_PasswordChanged(object sender, RoutedEventArgs e)
        {
            SqlPassworVar = SqlPassword.Password;
            SqlPassword.Background = Brushes.White;
        }

    }
}
