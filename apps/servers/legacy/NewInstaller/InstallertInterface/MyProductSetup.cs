using System;
using WixSharp.UI;

namespace WpfSetup
{
    public class ActionChangedEventArgs : EventArgs
    {
        private readonly string data;

        public ActionChangedEventArgs(string data)
        {
            this.data = data;
        }

        public string Data
        {
            get { return this.data; }
        }
    }

    public class MyProductSetup : GenericSetup
    {
        public MyProductSetup(string msiFile, bool enableLoging = true)
            : base(msiFile, enableLoging)
        {
            SetupStarted += MyProductSetup_SetupStarted;

            //Uncomment if you want to see current action name changes. Otherwise it is too quick.
        }
        public delegate void ActionChangedEventHandler(
        object sender,
        ActionChangedEventArgs args);

        public event ActionChangedEventHandler ActionChanged;

        public override void OnActionData(string data)
        {
            if (ActionChanged != null)
            {
                ActionChanged(this, new ActionChangedEventArgs(data));
            }
        }
        private void MyProductSetup_SetupStarted()
        {
            LogFileCreated = true;
        }

    }
}