﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Windows.Forms;
using Microsoft.Web.Administration;
namespace WpfSetup
{
    public partial class MainWindow
    {
        private readonly List<string> _webSites = new List<string>();
        public Site IiSite { get; set; }

        private List<string> GetWebSites()
        {
            using (ServerManager serverManager = new ServerManager())
            {
                foreach (var site in serverManager.Sites)
                {
                    _webSites.Add(site.Name);
                }

                return _webSites;
            }

        }

        private void IisWebSite_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            using (ServerManager serverManager = new ServerManager())
            {
                if (serverManager.Sites.Count > IisWebSite.SelectedIndex)
                {
                    IiSite = serverManager.Sites[IisWebSite.SelectedIndex];
                }
            }

        }

        private void IisINSTALLDIR_TextChanged(object sender, TextChangedEventArgs e)
        {
            IisINSTALLDIR.Text = IisINSTALLDIR.Text.Replace("%SystemDrive%",
                Path.GetPathRoot(Environment.SystemDirectory)).Replace("\\\\", "\\");
        }

        private bool CheckUrl()
        {
            try
            {
                if (!Directory.Exists(IisDirVar))
                {
                    Directory.CreateDirectory(IisDirVar);
                }

                File.WriteAllText(IisDirVar + "/validateurl.html", @"Done");
                using (var client = new WebClient())
                {
                    return client.DownloadString(new Uri(IisUrlVar + "/validateurl.html")).Contains("Done");
                }
            }
            catch
            {
                return false;
            }

        }

        private void IISINSTALLDIRButton_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new FolderBrowserDialog();
            dialog.ShowDialog();
            if (!string.IsNullOrEmpty(dialog.SelectedPath))
            {
                IisINSTALLDIR.Text = dialog.SelectedPath;
            }
        }
    }
}
