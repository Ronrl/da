﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Microsoft.Win32;

namespace WpfSetup
{
    public static class Config
    {
        public static string AlertsFolder { get; set; }
        public static string AlertsDir { get; set; }
        public static string Host { get; set; }
        public static string Dbname { get; set; }
        public static string UserName { get; set; }
        public static string Password { get; set; }
        public static string WinAuth { get; set; }
        public static string Site { get; set; }
        public static string SmsUseClickatel { get; set; }
        public static string SmsUrl { get; set; }
        public static string SmsPostData { get; set; }
        public static string SmsAuthUser { get; set; }
        public static string SmsAuthPass { get; set; }
        public static string SmtpServer { get; set; }
        public static string SmtpPort { get; set; }
        public static string SmtpSsl { get; set; }
        public static string SmtpAuth { get; set; }
        public static string SmtpUsername { get; set; }
        public static string SmtpPassword { get; set; }
        public static string EncryptKey { get; set; }

        public static string TextCallAuthUser { get; set; }
        public static string TextCallAuthPass { get; set; }

        public static string TextToCallPhoneText { get; set; }

        public static List<MouduleItem> Modules { get; set; }

        public static void WriteToRegistery()
        {
            RegistryKey key = Registry.CurrentUser.OpenSubKey("Software\\Softomate\\ServerInstallation", true);
            if (key == null)
            {
                Registry.CurrentUser.CreateSubKey("Software\\Softomate\\ServerInstallation");

            }

            if (key == null)
            {
                return;
            }
            var admodule = Modules.FirstOrDefault(a => a.ModuleName == "ModuleActiveDirectory");
            key.SetValue("adDisabled", admodule != null ? "0" : "1");

            var moduleEncrypt = Modules.FirstOrDefault(a => a.ModuleName == "ModuleEncrypt");
            key.SetValue("encryptDisabled", moduleEncrypt != null ? moduleEncrypt.ModuleInstall ? "1" : "0" : "0");
            key.SetValue("ENC", moduleEncrypt != null ? moduleEncrypt.ModuleInstall ? "1" : "0" : "0");

            var moduleStatistics = Modules.FirstOrDefault(a => a.ModuleName == "ModuleStatistics");
            key.SetValue("extstatDisabled", moduleStatistics != null ? moduleStatistics.ModuleInstall ? "1" : "0" : "0");


            var moduleFullScreen = Modules.FirstOrDefault(a => a.ModuleName == "ModuleFullScreen");
            key.SetValue("fsaDisabled", moduleFullScreen != null ? moduleFullScreen.ModuleInstall ? "1" : "0" : "0");


            var moduleIm = Modules.FirstOrDefault(a => a.ModuleName == "ModuleIM");
            key.SetValue("imDisabled", moduleIm != null ? moduleIm.ModuleInstall ? "1" : "0" : "0");

            var moduleLinkedin = Modules.FirstOrDefault(a => a.ModuleName == "ModuleLinkedin");
            key.SetValue("lnkdnDisabled", moduleLinkedin != null ? moduleLinkedin.ModuleInstall ? "1" : "0" : "0");

            var moduleMobile = Modules.FirstOrDefault(a => a.ModuleName == "ModuleMobile");
            key.SetValue("mobileDisabled", moduleMobile != null ? moduleMobile.ModuleInstall ? "1" : "0" : "0");


            var moduleRss = Modules.FirstOrDefault(a => a.ModuleName == "ModuleRSS");
            key.SetValue("rssDisabled", moduleRss != null ? moduleRss.ModuleInstall ? "1" : "0" : "0");

            var moduleSms = Modules.FirstOrDefault(a => a.ModuleName == "ModuleSMS");
            key.SetValue("smsDisabled", moduleSms != null ? moduleSms.ModuleInstall ? "1" : "0" : "0");

            var screenSaver = Modules.FirstOrDefault(a => a.ModuleName == "ScreenSaver");
            key.SetValue("ssDisabled", screenSaver != null ? screenSaver.ModuleInstall ? "1" : "0" : "0");

            var moduleSurvey = Modules.FirstOrDefault(a => a.ModuleName == "ModuleSurvey");
            key.SetValue("surveysDisabled", moduleSurvey != null ? moduleSurvey.ModuleInstall ? "1" : "0" : "0");

            var moduleTextToCall = Modules.FirstOrDefault(a => a.ModuleName == "ModuleTextToCall");
            key.SetValue("textcallDisabled", moduleTextToCall != null ? moduleTextToCall.ModuleInstall ? "1" : "0" : "0");

            var moduleTicker = Modules.FirstOrDefault(a => a.ModuleName == "ModuleTicker");
            key.SetValue("tickDisabled", moduleTicker != null ? moduleTicker.ModuleInstall ? "1" : "0" : "0");

            var moduleTickerPro = Modules.FirstOrDefault(a => a.ModuleName == "ModuleTicker");
            key.SetValue("tickProDisabled", moduleTickerPro != null ? moduleTickerPro.ModuleInstall ? "1" : "0" : "0");

            var moduleSmpp = Modules.FirstOrDefault(a => a.ModuleName == "ModuleSmpp");
            key.SetValue("smppDisabled", moduleSmpp != null ? moduleSmpp.ModuleInstall ? "1" : "0" : "0");

            var moduleTwitter = Modules.FirstOrDefault(a => a.ModuleName == "ModuleTwitter");
            key.SetValue("twtrDisabled", moduleTwitter != null ? moduleTwitter.ModuleInstall ? "1" : "0" : "0");

            var moduleVideo = Modules.FirstOrDefault(a => a.ModuleName == "ModuleVideo");
            key.SetValue("videoDisabled", moduleVideo != null ? moduleVideo.ModuleInstall ? "1" : "0" : "0");

            var moduleRESTAPI = Modules.FirstOrDefault(a => a.ModuleName == "ModuleRESTAPI");
            key.SetValue("restapiDisabled", moduleRESTAPI != null ? moduleRESTAPI.ModuleInstall ? "1" : "0" : "0");

            var moduleMultiLanguage = Modules.FirstOrDefault(a => a.ModuleName == "ModuleMultiLanguage");
            key.SetValue("multilanguageDisabled", moduleMultiLanguage != null ? moduleMultiLanguage.ModuleInstall ? "1" : "0" : "0");

            var moduleMultipleAlerts = Modules.FirstOrDefault(a => a.ModuleName == "ModuleMultipleAlerts");
            key.SetValue("multiplealertsDisabled", moduleMultipleAlerts != null ? moduleMultipleAlerts.ModuleInstall ? "1" : "0" : "0");

            var moduleWalpaper = Modules.FirstOrDefault(a => a.ModuleName == "ModuleWalpaper");
            key.SetValue("wpDisabled", moduleWalpaper != null ? moduleWalpaper.ModuleInstall ? "1" : "0" : "0");

            var moduleYammer = Modules.FirstOrDefault(a => a.ModuleName == "ModuleYammer");
            key.SetValue("yammerDisabled", moduleYammer != null ? moduleYammer.ModuleInstall ? "1" : "0" : "0");

            key.SetValue("alerts_dir", AlertsDir);

            key.SetValue("alerts_folder", AlertsFolder);

            key.SetValue("dbname", Dbname);

            key.SetValue("site", Site);

            key.SetValue("encrypt_key", EncryptKey);

            key.SetValue("host", Host);

            key.SetValue("smsAPI_ID", SmsPostData);

            key.SetValue("SmsUseClickatel", SmsUseClickatel);

            var smscustom = !string.IsNullOrEmpty(SmsAuthUser) ? "1" : "0";
            key.SetValue("smsCustom", smscustom);

            key.SetValue("smsPassword", SmsAuthPass);

            key.SetValue("smsPostData", SmsPostData);

            key.SetValue("smsUrl", SmsUrl);

            key.SetValue("smsUsername", SmsAuthUser);

            var smtpauth = !string.IsNullOrEmpty(SmtpUsername) ? "1" : "0";
            key.SetValue("smtpAuth", smtpauth);

            key.SetValue("smtpFrom", SmtpUsername);

            key.SetValue("smtpPassword", SmtpPassword);

            key.SetValue("smtpPort", SmtpPort);

            key.SetValue("smtpServer", SmtpServer);

            key.SetValue("smtpSSL", SmtpSsl);

            key.SetValue("smtpUsername", SmtpUsername);

            key.SetValue("user_name", UserName);
            key.SetValue("user_password", Password);
            key.SetValue("win_auth", WinAuth);

            key.SetValue("textcallAuthUser", TextCallAuthUser);
            key.SetValue("textcallAuthPass", TextCallAuthPass);
            key.SetValue("textToCallPhoneText", TextToCallPhoneText);

            key.Close();


        }

        public static void LoadFromRegistery()
        {
            RegistryKey key = Registry.CurrentUser.OpenSubKey("Software\\Softomate\\ServerInstallation");
            if (key == null) return;

            AlertsDir = key.GetValue("alerts_dir") != null ? key.GetValue("alerts_dir").ToString() : string.Empty;
            WinAuth = key.GetValue("win_auth") != null ? key.GetValue("win_auth").ToString() : string.Empty;
            UserName = key.GetValue("user_name") != null ? key.GetValue("user_name").ToString() : string.Empty;
            Password = key.GetValue("user_password") != null ? key.GetValue("user_password").ToString() : string.Empty;
            SmtpUsername = key.GetValue("smtpUsername") != null ? key.GetValue("smtpUsername").ToString() : string.Empty;
            SmtpSsl = key.GetValue("smtpSSL") != null ? key.GetValue("smtpSSL").ToString() : string.Empty;
            SmtpServer = key.GetValue("smtpServer") != null ? key.GetValue("smtpServer").ToString() : string.Empty;
            SmtpPort = key.GetValue("smtpPort") != null ? key.GetValue("smtpPort").ToString() : string.Empty;
            SmtpPassword = key.GetValue("smtpPassword") != null ? key.GetValue("smtpPassword").ToString() : string.Empty;
            SmtpAuth = key.GetValue("smtpAuth") != null ? key.GetValue("smtpAuth").ToString() : string.Empty;

            SmsUseClickatel = key.GetValue("SmsUseClickatel") != null ? key.GetValue("SmsUseClickatel").ToString() : string.Empty;
            SmsAuthUser = key.GetValue("smsUsername") != null ? key.GetValue("smsUsername").ToString() : string.Empty;
            SmsUrl = key.GetValue("smsUrl") != null ? key.GetValue("smsUrl").ToString() : string.Empty;
            SmsPostData = key.GetValue("smsPostData") != null ? key.GetValue("smsPostData").ToString() : string.Empty;
            SmsAuthPass = key.GetValue("smsPassword") != null ? key.GetValue("smsPassword").ToString() : string.Empty;
            SmsPostData = key.GetValue("smsAPI_ID") != null ? key.GetValue("smsAPI_ID").ToString() : string.Empty;
            Host = key.GetValue("host") != null ? key.GetValue("host").ToString() : string.Empty;
            EncryptKey = key.GetValue("encrypt_key") != null ? key.GetValue("encrypt_key").ToString() : string.Empty;
            Dbname = key.GetValue("dbname") != null ? key.GetValue("dbname").ToString() : string.Empty;
            AlertsFolder = key.GetValue("alerts_folder") != null ? key.GetValue("alerts_folder").ToString() : string.Empty;
            TextCallAuthUser = key.GetValue("textcallAuthUser") != null ? key.GetValue("textcallAuthUser").ToString() : string.Empty;
            TextCallAuthPass = key.GetValue("textcallAuthPass") != null ? key.GetValue("textcallAuthPass").ToString() : string.Empty;
            TextToCallPhoneText = key.GetValue("textToCallPhoneText") != null ? key.GetValue("textToCallPhoneText").ToString() : string.Empty;
            Site = key.GetValue("site") != null ? key.GetValue("site").ToString() : string.Empty;
        }


    }
}
