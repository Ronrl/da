﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Controls;
using System.Collections.Generic;
using System.Linq;

namespace WpfSetup
{
    public class MouduleItem
    {
        public string ModuleName { get; set; }
        public string ModuleToolTip { get; set; }
        public string ModuleText { get; set; }
        public bool ModuleInstall { get; set; }


        public MouduleItem(string name, string tooltip, bool mouduleinstall, string text)
        {
            ModuleName = name;
            ModuleToolTip = tooltip;
            ModuleInstall = mouduleinstall;
            ModuleText = text;


        }
    }
    public partial class MainWindow
    {
        void LoadModulesList()
        {
            try
            {
                ModuleItemsList = new List<MouduleItem>();

                if (Properties.Resources.Attributes != string.Empty)
                {
                    String[] attribs = Properties.Resources.Attributes.Split(' ');
                    foreach (string atrib in attribs)
                    {
                        if (atrib.Contains("/DisDemo"))
                        {
                            ModuleItemsList.Add(new MouduleItem("Is_Demo", Properties.Resources.ModuleDemoServerToolTip, true, Properties.Resources.ModuleDemoServerTitle));
                        }
                        if (atrib.Contains("/DisVIDEOAddon"))
                        {
                            ModuleItemsList.Add(new MouduleItem("ModuleVideo", Properties.Resources.ModuleVideoToolTip, true, Properties.Resources.ModuleVideoTitle));
                        }

                        if (atrib.Contains("/DisRESTAPIAddon"))
                        {
                            ModuleItemsList.Add(new MouduleItem("ModuleRESTAPI", Properties.Resources.ModuleRESTAPIToolTip, true, Properties.Resources.ModuleRESTAPITitle));
                        }

                        if (atrib.Contains("/DisMULTILANGUAGEAddon"))
                        {
                            ModuleItemsList.Add(new MouduleItem("ModuleMultiLanguage", Properties.Resources.ModuleMultiLanguageToolTip, true, Properties.Resources.ModuleMultiLanguageTitle));
                        }

                        if (atrib.Contains("/DisTEXTCALLAddon"))
                        {
                            ModuleItemsList.Add(new MouduleItem("ModuleTextToCall", Properties.Resources.ModuleTextToCallToolTip, false, Properties.Resources.ModuleTextToCallTitle));

                        }
                        if (atrib.Contains("/DisADAddon"))
                        {
                            ModuleItemsList.Add(new MouduleItem("ModuleActiveDirectory", Properties.Resources.ModuleActiveDirectoryToolTip, true, Properties.Resources.ModuleActiveTitle));
                        }

                        if (atrib.Contains("/DisSSOAddon"))
                        {
                            ModuleItemsList.Add(new MouduleItem("ModuleSSO", Properties.Resources.ModuleSSOToolTip, true, Properties.Resources.ModuleSSOTitle));
                        }

                        if (atrib.Contains("/DisSMPPAddon"))
                        {
                            ModuleItemsList.Add(new MouduleItem("ModuleSmpp", Properties.Resources.ModuleSMPPToolTip, true, Properties.Resources.ModuleSMPPTitle));
                        }

                        if (atrib.Contains("/DisSMSAddon"))
                        {
                            ModuleItemsList.Add(new MouduleItem("ModuleSMS", Properties.Resources.ModuleSMSToolTip, true, Properties.Resources.ModuleSMSTitle));
                        }
                        if (atrib.Contains("/DisEMAILAddon"))
                        {
                            ModuleItemsList.Add(new MouduleItem("ModuleEmail", Properties.Resources.ModuleEmailToolTip, true, Properties.Resources.ModuleEmailTitle));
                        }
                        if (atrib.Contains("/DisENCRYPTAddon"))
                        {
                            ModuleItemsList.Add(new MouduleItem("ModuleEncrypt", Properties.Resources.ModuleEncryptToolTip, false, Properties.Resources.ModuleEncryptTitle));
                        }
                        if (atrib.Contains("/DisSURVEYSAddon"))
                        {
                            ModuleItemsList.Add(new MouduleItem("ModuleSurvey", Properties.Resources.ModuleSurveyToolTip, true, Properties.Resources.ModuleSurveyTitle));
                        }
                        if (atrib.Contains("/DisSTATISTICSAddon"))
                        {
                            ModuleItemsList.Add(new MouduleItem("ModuleStatistics", Properties.Resources.ModuleStatisticsToolTip, true, Properties.Resources.ModuleStatisticsTitle));
                        }
                        if (atrib.Contains("/DisSCREENSAVERAddon"))
                        {
                            ModuleItemsList.Add(new MouduleItem("ScreenSaver", Properties.Resources.ScreenSaverToolTip, true, Properties.Resources.ScreenSaverTitle));
                        }
                        if (atrib.Contains("/DisRSSAddon"))
                        {
                            ModuleItemsList.Add(new MouduleItem("ModuleRSS", Properties.Resources.ModuleRSSToolTip, true, Properties.Resources.ModuleRSSTitle));
                        }
                        if (atrib.Contains("/DisWALLPAPERAddon"))
                        {
                            ModuleItemsList.Add(new MouduleItem("ModuleWalpaper", Properties.Resources.ModuleWalpaperToolTip, true, Properties.Resources.ModuleWalpaperTitle));
                        }
                        if (atrib.Contains("/DisLOCKSCREENAddon"))
                        {
                            ModuleItemsList.Add(new MouduleItem("ModuleLockscreen", Properties.Resources.ModuleLockscreenToolTip, true, Properties.Resources.ModuleLockscreenTitle));
                        }

                        /*if (atrib.Contains("/DisBLOGAddon"))
                        {
                            ModuleItemsList.Add(new MouduleItem("ModuleBlog", Properties.Resources.ModuleBlogToolTip, false, Properties.Resources.ModuleBlogTitle));
                        }*/
                        if (atrib.Contains("/DisTWITTERAddon"))
                        {
                            ModuleItemsList.Add(new MouduleItem("ModuleTwitter", Properties.Resources.ModuleTwitterToolTip, false, Properties.Resources.ModuleTwitterTitle));
                        }
                        if (atrib.Contains("/DisMultipleAlerts"))
                        {
                            ModuleItemsList.Add(new MouduleItem("ModuleMultipleAlerts", Properties.Resources.ModuleMultipleAlertsTooltip, true, Properties.Resources.ModuleMultipleAlerts));
                        }
                        if (atrib.Contains("/DisLINKEDINAddon"))
                        {
                            ModuleItemsList.Add(new MouduleItem("ModuleLinkedin", Properties.Resources.ModuleLinkedinToolTip, false, Properties.Resources.ModuleLinkedinTitle));
                        }
                        if (atrib.Contains("/DisYAMMERAddon"))
                        {
                            ModuleItemsList.Add(new MouduleItem("ModuleYammer", Properties.Resources.ModuleDigsignToolTip, false, Properties.Resources.ModuleYammerTitle));
                        }
                        if (atrib.Contains("/DisFULLSCREENAddon"))
                        {
                            ModuleItemsList.Add(new MouduleItem("ModuleFullScreen", Properties.Resources.ModuleFullScreenToolTip, true, Properties.Resources.ModuleFullScreenTitle));
                        }
                        if (atrib.Contains("/DisTICKERAddon"))
                        {
                            ModuleItemsList.Add(new MouduleItem("ModuleTicker", Properties.Resources.ModuleTickerToolTip, true, Properties.Resources.ModuleTickerTitle));
                        }
                        if (atrib.Contains("/DisIMAddon"))
                        {
                            ModuleItemsList.Add(new MouduleItem("ModuleIM", Properties.Resources.ModuleIMToolTip, true, Properties.Resources.ModuleIMToolTitle));
                        }
                        if (atrib.Contains("/DisWIDGETSTATAddon"))
                        {
                            // ModuleItemsList.Add(new MouduleItem("ModuleWidgets", Properties.Resources.ModuleWidgetsToolTip, true, Properties.Resources.ModuleWidgetsTitle));
                        }
                        if (atrib.Contains("/DisMOBILEAddon"))
                        {
                            ModuleItemsList.Add(new MouduleItem("ModuleMobile", Properties.Resources.ModuleMobileToolTip, true, Properties.Resources.ModuleMobileTitle));
                        }
                        if (atrib.Contains("/DisCAMPAIGNAddon"))
                        {
                            ModuleItemsList.Add(new MouduleItem("ModuleCampaign", Properties.Resources.ModuleCampaignToolTip, true, Properties.Resources.ModuleCampaignTitle));
                        }
                        if (atrib.Contains("/DisAPPROVEAddon"))
                        {
                            ModuleItemsList.Add(new MouduleItem("ModuleApprove", Properties.Resources.ModuleApproveToolTip, true, Properties.Resources.ModuleApproveTitle));
                        }
                        if (atrib.Contains("/DisDIGSIGNAddon"))
                        {
                            ModuleItemsList.Add(new MouduleItem("ModuleDesign", Properties.Resources.ModuleDigsignToolTip, true, Properties.Resources.ModuleDigsignTitle));
                        }
                        if (atrib.Contains("/DisBulkUploadOfRecipients"))
                        {
                            ModuleItemsList.Add(new MouduleItem("ModuleBulkUploadOfRecipients", Properties.Resources.ModuleBulkUploadOfRecipientsToolTip, true, Properties.Resources.ModuleBulkUploadOfRecipientsTitle));
                        }
                        if (atrib.Contains("/Dtrial"))
                        {
                            TrialAttrib = true;
                        }
                        if (atrib.Contains("/Dlicenses"))
                        {
                            Licenses = atrib.Remove(0, 10);
                        }
                    }

                    foreach (var moduleitem in ModuleItemsList)
                    {
                        ModulesListBox.Items.Add(moduleitem);
                    }
                }
                else
                {
                    MessageBox.Show("File Attibutes not found. Please Rebuild Installer with install.bat", "Error");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }
        private void ModulesCustom_Checked(object sender, RoutedEventArgs e)
        {
            ModulesRecomended.IsChecked = false;

        }
        private void ModuleItemCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            ModulesRecomended.IsChecked = false;
            ModulesCustom.IsChecked = true;
        }

        private void ModuleItemCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            ModulesRecomended.IsChecked = false;
            ModulesCustom.IsChecked = true;
        }
        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {

            foreach (var item in ModuleItemsList)
            {
                if (item.ModuleName.Contains("Encr"))
                {
                    item.ModuleInstall = false;
                }
               /* if (item.ModuleName.Contains("ModuleBlog"))
                {
                    item.ModuleInstall = false;
                }*/
                if (item.ModuleName.Contains("ModuleTwitter"))
                {
                    item.ModuleInstall = false;
                }
                if (item.ModuleName.Contains("ModuleTwitter"))
                {
                    item.ModuleInstall = false;
                }
                if (item.ModuleName.Contains("ModuleLinkedin"))
                {
                    item.ModuleInstall = false;
                }
                if (item.ModuleName.Contains("ModuleYammer"))
                {
                    item.ModuleInstall = false;
                }
                if (item.ModuleName.Contains("ModuleTwitter"))
                {
                    item.ModuleInstall = false;
                }
            }
            ModulesListBox.Items.Clear();
            foreach (var moduleitem in ModuleItemsList.OrderByDescending(x => x.ModuleInstall))
            {
                ModulesListBox.Items.Add(moduleitem);
            }

            ModulesCustom.IsChecked = false;
        }
        private void ModuleInfo_Click(object sender, RoutedEventArgs e)
        {
            Button b = (Button)sender;
            Grid grid = VisualTreeHelper.GetParent(b) as Grid;
            if (grid != null)
            {
                TextBlock text = grid.Children.Cast<UIElement>().First(a => Grid.GetColumn(a) == 1) as TextBlock;
                var moudleitem = ModuleItemsList.FirstOrDefault(a => text != null && a.ModuleText == text.Text);
                if (moudleitem != null)
                    MessageBox.Show(moudleitem.ModuleToolTip, "Information", MessageBoxButton.OK);
            }
        }

    }
}
