﻿using System;
using System.IO;
using System.Windows;
using System.Linq;

namespace WpfSetup
{
    public partial class MainWindow
    {
        private void ShowProgressPage()
        {
            Next.Visibility = Visibility.Collapsed;
            SkipButton.Visibility = Visibility.Collapsed;
            Progress.Focus();
            Install.IsEnabled = true;
            FocusedPage = Progress;
            CurrentPages.Add(Progress);
            Next.IsEnabled = false;
            Previous.IsEnabled = true;
            StatusTitle.Content = Properties.Resources.ProgressTitle;
            ProgressText.Text = ""; ProgressText.AppendText("\n\r" + "READY TO INSTALL!");
            ProgressText.AppendText("\n\r" + "PLEASE CLICK \"INSTALL\" BUTTON TO START INSTALLATION!");
        }

        private void Next_Click(object sender, RoutedEventArgs e)
        {
            var installedModules = ModuleItemsList.Where(x => x.ModuleInstall).ToArray();

            var sms = installedModules.Any(x => x.ModuleName == "ModuleSMS");
            var smtp = installedModules.Any(x => x.ModuleName == "ModuleEmail");
            var encrypt = installedModules.Any(x => x.ModuleName == "ModuleEncrypt");
            var textToCall = installedModules.Any(x => x.ModuleName == "ModuleTextToCall");
            var sso = installedModules.Any(x => x.ModuleName == "ModuleSSO");

            if (Equals(FocusedPage, WelcomePage))
            {
                CurrentPages.Clear();
                if (!TrialAttrib)
                {
                    IisLoading.Visibility = Visibility.Visible;
                    IisGrid.Visibility = Visibility.Collapsed;
                    //loading iis window
                    CurrentPages.Add(WelcomePage);
                    Previous.IsEnabled = false;
                    Next.IsEnabled = false;
                    if (!_backgroundWorker.IsBusy)
                    {
                        _backgroundWorker.RunWorkerAsync("GetWebsites");
                    }
                    StatusTitle.Content = Properties.Resources.IISTitle;

                }
                else
                {
                    Trial.Focus();
                    Previous.IsEnabled = true;
                    FocusedPage = Trial;
                    CurrentPages.Add(WelcomePage);
                    CurrentPages.Add(Trial);
                    StatusTitle.Content = Properties.Resources.TrialTitle;
                }
            }
            else if (Equals(FocusedPage, Trial))
            {
                TrialHeadGrid.Visibility = Visibility.Collapsed;
                TrialHelpGrid.Visibility = Visibility.Collapsed;
                TrialLoading.Visibility = Visibility.Visible;
                string key = TrialKeys.FirstOrDefault(a => a == TrialKey.Text);
                if (!string.IsNullOrEmpty(key))
                {
                    TrialHeadGrid.Visibility = Visibility.Visible;
                    TrialHelpGrid.Visibility = Visibility.Visible;
                    TrialLoading.Visibility = Visibility.Collapsed;
                    IisLoading.Visibility = Visibility.Visible;
                    IisGrid.Visibility = Visibility.Collapsed;
                    //loading iis window
                    Previous.IsEnabled = false;
                    Next.IsEnabled = false;
                    if (!_backgroundWorker.IsBusy)
                    {
                        _backgroundWorker.RunWorkerAsync("GetWebsites");
                    }
                    StatusTitle.Content = Properties.Resources.IISTitle;

                }
                else
                {
                    TrialHeadGrid.Visibility = Visibility.Visible;
                    TrialHelpGrid.Visibility = Visibility.Visible;
                    TrialLoading.Visibility = Visibility.Collapsed;
                    MessageBoxResult res = MessageBox.Show(Properties.Resources.TrialMessage, "Warning",
                        MessageBoxButton.YesNo);
                    if (res == MessageBoxResult.Yes)
                    {
                        System.Diagnostics.Process.Start(Properties.Resources.TrialLink);
                    }

                }
            }
            else if (Equals(FocusedPage, SystemRequirements))
            {
                IisLoading.Visibility = Visibility.Visible;
                IisGrid.Visibility = Visibility.Collapsed;
                //loading iis window
                Previous.IsEnabled = false;
                Next.IsEnabled = false;
                if (!_backgroundWorker.IsBusy)
                {
                    _backgroundWorker.RunWorkerAsync("GetWebsites");
                }

                StatusTitle.Content = Properties.Resources.IISTitle;
            }
            else if (Equals(FocusedPage, Iis))
            {

                Uri uriResult;
                bool result = Uri.TryCreate(Iisurl.Text, UriKind.Absolute, out uriResult)
                              && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);
                if (result)
                {
                    IisDirVar = IisINSTALLDIR.Text;
                    IisUrlVar = Iisurl.Text;

                    string driveName = Path.GetPathRoot(IisDirVar);

                    if (!string.IsNullOrEmpty(driveName))
                    {
                        var driveInfo = DriveInfo.GetDrives().First(d => d.Name == driveName);

                        long availableSpaceInMegabytes = driveInfo.AvailableFreeSpace / 1024L / 1024L;

                        if (availableSpaceInMegabytes < App.InstalledPackageSizeInMegabytes)
                        {
                            MessageBox.Show(this,
                                string.Format(
                                    "Not enought space on drive {0}. You must have at least {1} MB of free disk space. Please free disk space and try again.",
                                    driveName, App.InstalledPackageSizeInMegabytes), "DeskAlerts installer error", MessageBoxButton.OK, MessageBoxImage.Error);

                            return;
                        }
                    }


                    IisGrid.Visibility = Visibility.Collapsed;
                    IisLoading.Visibility = Visibility.Visible;
                    if (!_backgroundWorker.IsBusy)
                    {
                        _backgroundWorker.RunWorkerAsync("CheckUrl");
                    }
                }
                else
                {
                    MessageBox.Show(Properties.Resources.IISUrlerror, "Error", MessageBoxButton.OK);
                }
            }
            else if (Equals(FocusedPage, Sql))
            {
                if (SQlNotUseWindowsAuth.IsChecked == true)
                {
                    if (string.IsNullOrEmpty(SqlLogin.Text))
                    {
                        MessageBox.Show(Properties.Resources.SqlEmptyLoginText, "Error", MessageBoxButton.OK);
                        return;
                    }
                }
                Previous.IsEnabled = false;
                Next.IsEnabled = false;
                SqlLoading.Visibility = Visibility.Visible;
                SqlGrid.Visibility = Visibility.Collapsed;
                if (!_backgroundWorker.IsBusy)
                {
                    _backgroundWorker.RunWorkerAsync("CheckSqlConnection");
                }
            }
            else if (Equals(FocusedPage, Modules))
            {

                if (SQlUseWindowsAuth.IsChecked == true && sso)
                {
                    MessageBox.Show(
                        Properties.Resources
                            .In_case_you_want_to_setup_SSO_you_ony_can_t_chose_Windows_authentication_for_SQL_database,
                        "Error", MessageBoxButton.OK);
                    return;
                }

                if (sms)
                {
                    SmsModule.Focus();
                    SkipButton.Visibility = Visibility.Visible;
                    FocusedPage = SmsModule;
                    CurrentPages.Add(SmsModule);
                    StatusTitle.Content = Properties.Resources.ModuleSMSTitle;
                }
                else
                {
                    if (smtp)
                    {
                        SmtpModule.Focus();
                        FocusedPage = SmtpModule;
                        SkipButton.Visibility = Visibility.Visible;
                        CurrentPages.Add(SmtpModule);
                        StatusTitle.Content = Properties.Resources.ModuleEmailTitle;
                    }
                    else
                    {
                        if (encrypt)
                        {
                            EncryptModule.Focus();
                            FocusedPage = EncryptModule;
                            SkipButton.Visibility = Visibility.Visible;
                            CurrentPages.Add(EncryptModule);
                            StatusTitle.Content = Properties.Resources.ModuleEncryptTitle;
                        }
                        else
                        {
                            if (textToCall)
                            {
                                TextToCallModule.Focus();
                                FocusedPage = TextToCallModule;
                                SkipButton.Visibility = Visibility.Visible;
                                CurrentPages.Add(TextToCallModule);
                                StatusTitle.Content = Properties.Resources.ModuleTextToCallTitle;
                            }
                            else
                            {
                                ShowProgressPage();
                            }
                        }
                    }
                }
            }
            else if (Equals(FocusedPage, SmsModule))
            {
                if (smtp)
                {
                    SkipButton.Visibility = Visibility.Visible;
                    SmtpModule.Focus();
                    FocusedPage = SmtpModule;
                    CurrentPages.Add(SmtpModule);
                    StatusTitle.Content = Properties.Resources.ModuleEmailTitle;
                }
                else if (encrypt)
                {
                    SkipButton.Visibility = Visibility.Visible;
                    EncryptModule.Focus();
                    FocusedPage = EncryptModule;
                    CurrentPages.Add(EncryptModule);
                    StatusTitle.Content = Properties.Resources.ModuleEncryptTitle;
                }
                else if (textToCall)
                {
                    TextToCallModule.Focus();
                    FocusedPage = TextToCallModule;
                    SkipButton.Visibility = Visibility.Visible;
                    CurrentPages.Add(TextToCallModule);
                    StatusTitle.Content = Properties.Resources.ModuleTextToCallTitle;
                }
                else
                {
                    ShowProgressPage();
                }
            }
            else if (Equals(FocusedPage, SmtpModule))
            {
                if (encrypt)
                {
                    EncryptModule.Focus();
                    FocusedPage = EncryptModule;
                    CurrentPages.Add(EncryptModule);
                    StatusTitle.Content = Properties.Resources.ModuleEncryptTitle;
                }
                else
                {
                    if (textToCall)
                    {
                        TextToCallModule.Focus();
                        FocusedPage = TextToCallModule;
                        SkipButton.Visibility = Visibility.Visible;
                        CurrentPages.Add(TextToCallModule);
                        StatusTitle.Content = Properties.Resources.ModuleTextToCallTitle;
                    }
                    else
                    {
                        ShowProgressPage();
                    }
                }
            }
            else if (Equals(FocusedPage, EncryptModule))
            {
                if (textToCall)
                {
                    TextToCallModule.Focus();
                    FocusedPage = TextToCallModule;
                    SkipButton.Visibility = Visibility.Visible;
                    CurrentPages.Add(TextToCallModule);
                    StatusTitle.Content = Properties.Resources.ModuleTextToCallTitle;
                }
                else
                {
                    ShowProgressPage();
                }
            }
            else if (Equals(FocusedPage, TextToCallModule))
            {
                ShowProgressPage();
            }
        }
    }
}
