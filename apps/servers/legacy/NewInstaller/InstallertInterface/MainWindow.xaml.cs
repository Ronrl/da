﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

using Microsoft.Web.Administration;

using Application = System.Windows.Application;
using Button = System.Windows.Controls.Button;
using DataFormats = System.Windows.DataFormats;
using MessageBox = System.Windows.MessageBox;
using RichTextBox = System.Windows.Controls.RichTextBox;

namespace WpfSetup
{
    //Result Items List Class

    public partial class MainWindow
    {
        private readonly BackgroundWorker _backgroundWorker;
        private static TabItem FocusedPage { get; set; }
        //souce of items in resultwindow

        //souce of items in modulewindow
        private List<MouduleItem> ModuleItemsList { get; set; }

        private static readonly List<TabItem> CurrentPages = new List<TabItem>();
        public MyProductSetup Setup { get; set; }

        public bool TrialAttrib { get; set; }

        public string Licenses { get; set; }
        public List<string> TrialKeys { get; set; }

        public bool InstallNetFx { get; set; }

        public static string LinkVar { get; set; }

        public static string IisUrlVar { get; set; }

        public static string IisDirVar { get; set; }

        public MainWindow()
        {
            InitializeComponent();
            _backgroundWorker = ((BackgroundWorker)FindResource("BackgroundWorker"));
        }

        // load all modules like in msi. File "attributes" is created from .bat in ../Msi and loaded in resources
        public static void SetRtf(RichTextBox rtb)
        {
            using (var reader = new MemoryStream(Properties.Resources.License))
            {
                reader.Position = 0;
                rtb.SelectAll();
                rtb.Selection.Load(reader, DataFormats.Rtf);
            }
        }

        private Button SkipButton { get; set; }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                App.HideSplashScreen();
                //MessageBox.Show(App.MsiFile);
                if (string.IsNullOrEmpty(App.MsiFile)) Application.Current.Shutdown();
                Setup = new MyProductSetup(App.MsiFile) { InUiThread = InUiThread };
                DataContext = Setup;
                Config.LoadFromRegistery();
                Install.Content = "Install";

                Install.IsEnabled = false;
                Previous.IsEnabled = false;

                WelcomePage.Focus();
                TopLogo.Source = LoadBitmap(Properties.Resources.top_logo);
                LicenseAgreement.IsReadOnly = true;

                string[] keys = Properties.Resources.TrialKeys.Split(new[] { "\r\n" }, StringSplitOptions.None);
                TrialKeys = new List<string>();
                foreach (string t in keys)
                {
                    TrialKeys.Add(t);
                }

                SetRtf(LicenseAgreement);

                LicenseDisAgree.IsChecked = true;
                LicenseAgree.IsChecked = false;
                Next.IsEnabled = false;

                FocusedPage = WelcomePage;
                CurrentPages.Add(WelcomePage);
                if (Equals(FocusedPage, WelcomePage))
                {
                    StatusTitle.Content = "End User License Agreement (EULA)";
                }

                TrialHelpText.Text = Properties.Resources.TrialHelp;

                //IISFeaturesImage.Source = LoadBitmap(Properties.Resources.sucsess_icon);
                //ASPNETFeaturesImage.Source = LoadBitmap(Properties.Resources.sucsess_icon);

                IisWebSite.ItemsSource = _webSites;

                string selectedSitename = _webSites.FirstOrDefault(s => s == Config.Site);

                if (!string.IsNullOrEmpty(selectedSitename))
                {
                    int siteIndex = _webSites.IndexOf(selectedSitename);

                    IisWebSite.SelectedIndex = siteIndex;
                }

                Iisurl.Text = !string.IsNullOrEmpty(Config.AlertsFolder)
                    ? Config.AlertsFolder
                    : "http://" + Environment.MachineName + "/DeskAlerts";

                IisHelpText.Text = Properties.Resources.IISHelp;
                try
                {
                    using (ServerManager serverManager = new ServerManager())
                    {
                        var site = serverManager.Sites.FirstOrDefault();
                        if (site != null)
                        {
                            var applicationRoot =
                                site.Applications.Single(a => a.Path == "/");
                            var virtualRoot =
                                applicationRoot.VirtualDirectories.Single(v => v.Path == "/");
                            string firstsitepath = virtualRoot.PhysicalPath;

                            IisINSTALLDIR.Text = !string.IsNullOrEmpty(Config.AlertsDir)
                                                     ? Config.AlertsDir
                                                     : firstsitepath.Replace(
                                                               "%SystemDrive%",
                                                               Path.GetPathRoot(Environment.SystemDirectory))
                                                           .Replace("\\\\", "\\") + "\\DeskAlerts";
                        }
                    }
                }
                catch (Exception)
                {
                    //ignore
                }

                SqlPassworVar = !string.IsNullOrEmpty(Config.Password) ? Config.Password : string.Empty;
                SqlPassword.Password = SqlPassworVar;
                SqlUseWindowsAuthVar = !string.IsNullOrEmpty(Config.WinAuth) &&
                                       Convert.ToBoolean(Convert.ToInt32(Config.WinAuth));
                SQlUseWindowsAuth.IsChecked = SqlUseWindowsAuthVar;
                SQlNotUseWindowsAuth.IsChecked = !SqlUseWindowsAuthVar;
                SqlHostVar = !string.IsNullOrEmpty(Config.Host) ? Config.Host : "(local)";
                SqlHost.Text = SqlHostVar;
                SqlDatabaseVar = !string.IsNullOrEmpty(Config.Dbname) ? Config.Dbname : "DeskAlerts";
                SqlDatabase.Text = SqlDatabaseVar;
                SqlLoginVar = !string.IsNullOrEmpty(Config.UserName) ? Config.UserName : "";
                SqlLogin.Text = SqlLoginVar;
                SqlPassworVar = !string.IsNullOrEmpty(Config.Password) ? Config.Password : "";
                SqlPassword.Password = SqlPassworVar;
                SqlHelpText.Text = Properties.Resources.SQLHelp;

                SmsHelpText.Text = Properties.Resources.ModuleSMSToolTip;

                SmtpPassword.Password = !string.IsNullOrEmpty(Config.SmtpPassword) ? Config.SmtpPassword : string.Empty;
                SmtpAuth.IsChecked = !string.IsNullOrEmpty(Config.SmtpAuth) && Convert.ToBoolean(Convert.ToInt32(Config.SmtpAuth));
                SmtpNotAuth.IsChecked = string.IsNullOrEmpty(Config.SmtpAuth) || !Convert.ToBoolean(Convert.ToInt32(Config.SmtpAuth));
                SmtpUsername.Text = !string.IsNullOrEmpty(Config.SmtpUsername) ? Config.SmtpUsername : string.Empty;
                SmtpPort.Text = !string.IsNullOrEmpty(Config.SmtpPort) ? Config.SmtpPort : Config.SmtpSsl == "0" ? "443" : "25";
                Smtpurl.Text = !string.IsNullOrEmpty(Config.SmtpServer) ? Config.SmtpServer : string.Empty;
                Smtpssl.IsChecked = !string.IsNullOrEmpty(Config.SmtpSsl) && Convert.ToBoolean(Convert.ToInt32(Config.SmtpSsl));
                SmtpHelpText.Text = Properties.Resources.ModuleEmailToolTip;

                EncryptionKey.Text = !string.IsNullOrEmpty(Config.EncryptKey) ? Config.EncryptKey : string.Empty;
                EncryptionHelpText.Text = Properties.Resources.ModuleEncryptToolTip;

                ModulesListBox.ItemsSource = ModuleItemsList;

                TextToCallurl.Text = !string.IsNullOrEmpty(Config.TextCallAuthUser)
                    ? Config.TextCallAuthUser
                    : string.Empty;
                TextToCallPhone.Text = !string.IsNullOrEmpty(Config.TextToCallPhoneText)
                    ? Config.TextToCallPhoneText
                    : string.Empty;
                TextToCallPassword.Password = !string.IsNullOrEmpty(Config.TextCallAuthPass)
                    ? Config.TextCallAuthPass
                    : string.Empty;
                TextToCallHelpText.Text = Properties.Resources.ModuleTextToCallToolTip;

                SkipButton = new Button
                {
                    DataContext = Next.DataContext,
                    Content = "Configure later",
                    Visibility = Visibility.Collapsed
                };
                SkipButton.Click += Next_Click;
                ButtonsPanel.Children.Add(SkipButton);
                LoadModulesList();
                ModulesRecomended.IsChecked = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message +
                                (!string.IsNullOrEmpty(ex.StackTrace) ? " StackTrace: " + ex.StackTrace : string.Empty));
            }
        }

        public void InUiThread(Action action)
        {
            if (Dispatcher.CheckAccess())
            {
                action();
            }
            else
            {
                Dispatcher.BeginInvoke(DispatcherPriority.Normal, action);
            }
        }

        private void Minimize_Click(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }

        private void Close_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Install_Click(object sender, RoutedEventArgs e)
        {
            var url = GetUrl(Iisurl.Text);

            var oldInstallDir = Config.AlertsDir;
            Config.AlertsDir = IisINSTALLDIR.Text;
            Config.AlertsFolder = url;
            Config.Site = IiSite.Name;
            Config.Dbname = SqlDatabase.Text;
            Config.UserName = SqlLogin.Text;
            Config.Password = SqlPassword.Password;
            Config.WinAuth = SQlUseWindowsAuth.IsChecked == true ? "1" : "0";
            Config.EncryptKey = EncryptionKey.Text;

            Config.SmtpAuth = SmtpAuth.IsChecked == true ? "1" : "0";
            Config.SmtpUsername = SmtpUsername.Text;
            Config.SmtpPassword = SmtpPassword.Password;
            Config.SmtpPort = SmtpPort.Text;
            Config.SmtpSsl = Smtpssl.IsChecked == true ? "1" : "0";
            Config.SmtpServer = Smtpurl.Text;
            Config.Host = SqlHost.Text;
            Config.Modules = ModuleItemsList;
            Config.TextCallAuthUser = TextToCallurl.Text;
            Config.TextToCallPhoneText = TextToCallPhone.Text;
            Config.TextCallAuthPass = TextToCallPassword.Password;
            Config.WriteToRegistery();

            if (SQlUseWindowsAuth.IsChecked == true)
            {
                SqlPassworVar = "";
                SqlLoginVar = "";
            }
            else
            {
                SqlPassworVar = Convert.ToBase64String(Encoding.Default.GetBytes(SqlPassworVar));
            }
            LinkVar = url;
            SmtpPassword.Password = Convert.ToBase64String(Encoding.Default.GetBytes(SmtpPassword.Password));

            string drop = SqlDropDatabase ? "True" : "False";

            string moduleargs = string.Empty;
            foreach (var module in ModuleItemsList)
            {
                if (module.ModuleInstall)
                {
                    moduleargs = moduleargs + " " + module.ModuleName + "=\"True\"";
                }
            }

            if (Directory.Exists(Path.GetFullPath(IisINSTALLDIR.Text) + "/bin"))
            {
                Directory.Delete(Path.GetFullPath(IisINSTALLDIR.Text) + "/bin", true);
            }

            Setupargs = "CUSTOM_UI=true SITE=\"" + IiSite.Name +
                        "\" IISURL=\"" + url +
                        "\" INSTALLPATH=\"" + Path.GetFullPath(IisINSTALLDIR.Text) +
                        "\" TARGETDIR=\"" + Path.GetFullPath(IisINSTALLDIR.Text) +
                        "\" INSTALLDIR=\"" + Path.GetFullPath(IisINSTALLDIR.Text) +
                        "\" OLDINSTALLDIR =\"" + oldInstallDir +
                        "\" SQLDATABASE=\"" + SqlDatabaseVar +
                        "\" DROPDATABASE=\"" + drop +
                        "\" NEWDATABASE=\"" + NewDataBase +
                        "\" SQLSERVER=\"" + SqlHostVar +
                        "\" AZURESQL=\"" + UseAzureAuth +
                        "\" SQLUSEAUTH=\"" + SqlUseWindowsAuthVar +
                        "\" SQLLOGIN=\"" + SqlLoginVar +
                        "\" SQLPASSWORD=\"" + SqlPassworVar +
                        "\" DYNGUID=\"" + Guid.NewGuid() +
                        "\" NETINSTALL=\"" + InstallNetFx +
                        "\" SMTPSERVER=\"" + Smtpurl.Text +
                        "\" SMTPPORT=\"25\" SMTPSSL=\"" + Smtpssl.IsChecked +
                        "\" SMTPAUTH =\"" + SmtpAuth.IsChecked +
                        "\" SMTPUSER =\"" + SmtpUsername.Text +
                        "\" SMTPPASSWORD=\"" + SmtpPassword.Password +
                        "\" ENCKEY =\"" + EncryptionKey.Text +
                        "\" TTCALLUSER =\"" + TextToCallurl.Text +
                        "\" TTCALLPASS =\"" + TextToCallPassword.Password +
                        "\" TTCALLPHONE=\"" + TextToCallPhone.Text + "\"" + moduleargs;

            MainProgressBar.Visibility = Visibility.Visible;
            ProgressText.SelectAll();
            ProgressText.Text = string.Empty;
            ProgressText.AppendText("\n\r" + "Installation started. \n Please wait.....");
            Setup.ActionChanged += ActionChanged;
            if (Setup.CanUnInstall)
            {
                Setup.ProgressChanged += UninstallProgress;
                MainProgressBar.Value = Setup.ProgressCurrentPosition;
                MainProgressBar.Maximum = Setup.ProgressTotal;
                Setup.SetupComplete += Setupcompleted2;
                Setup.StartUninstall();
            }

            if (Setup.CanInstall)
            {
                Setup.ProgressChanged += InstallProgress;
                MainProgressBar.Value = Setup.ProgressCurrentPosition;
                MainProgressBar.Maximum = Setup.ProgressTotal;
                Setup.SetupComplete += Setupcompleted;
                Setup.StartInstall(Setupargs);
            }
        }

        private string GetUrl(string text)
        {
            var uri = new Uri(text);
            var applicationName = EscapeUnsupportedCaracters(uri.LocalPath);
            return $"{uri.Scheme}://{uri.Host}{applicationName}".TrimEnd('/');
        }

        private string EscapeUnsupportedCaracters(string siteName)
        {
            string[] unsupportedCharacters =
                {@"\", "?", ";", ":", "@", "&", "=", "+", "$", ",", "|", "\"", "<", ">", "*", " ", ":", ".", "_"};
            return unsupportedCharacters.Aggregate(siteName,
                (currentSymbol, character) => currentSymbol.Replace(character, string.Empty));
        }

        private void ActionChanged(object sender, ActionChangedEventArgs args)
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(() =>
            {
                ProgressText.AppendText("\n\r" + args.Data);
                ProgressText.ScrollToEnd();
            }));
        }

        private void UninstallProgress(object sender, EventArgs e)
        {
            MainProgressBar.Value = Setup.ProgressCurrentPosition;
            MainProgressBar.Maximum = Setup.ProgressTotal;
        }

        private void InstallProgress(object sender, EventArgs e)
        {
            MainProgressBar.Value = Setup.ProgressCurrentPosition;
            MainProgressBar.Maximum = Setup.ProgressTotal;
        }

        private static string Setupargs { get; set; }

        public void Setupcompleted2()
        {
            if (Setup.ErrorStatus.Contains("Success"))
            {
                Setup = new MyProductSetup(App.MsiFile) { InUiThread = InUiThread };
                if (Setup.CanInstall)
                {
                    Setup.ActionChanged += ActionChanged;
                    Setup.SetupComplete += Setupcompleted;
                    Setup.ProgressChanged += InstallProgress;
                    MainProgressBar.Value = Setup.ProgressCurrentPosition;
                    MainProgressBar.Maximum = Setup.ProgressTotal;
                    Setup.StartInstall(Setupargs);
                }
            }
            else
            {
                MainProgressBar.Visibility = Visibility.Collapsed;
                MessageBox.Show(Setup.ErrorStatus, "Error");
                Process.Start(Setup.LogFile);
                Application.Current.Shutdown();
            }
        }

        public void Setupcompleted()
        {
            if (Setup.ErrorStatus.Contains("Success"))
            {
                MainProgressBar.Visibility = Visibility.Collapsed;
                var res = MessageBox.Show(
                    NewDataBase ? Properties.Resources.InstallationSuccessWithAdminPassword : Properties.Resources.InstallationSuccess,
                    "Installation Completed", MessageBoxButton.OK);
                if (res == MessageBoxResult.OK)
                {
                    Process.Start(LinkVar);
                }

                Application.Current.Shutdown();
            }
            else
            {
                MainProgressBar.Visibility = Visibility.Collapsed;
                MessageBox.Show(Setup.ErrorStatus, "Error");
                Process.Start(Setup.LogFile);
                Application.Current.Shutdown();
            }
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            e.Cancel = Setup.IsRunning;
        }

        private void LicenseAgree_Checked(object sender, RoutedEventArgs e)
        {
            Next.IsEnabled = true;
            LicenseDisAgree.IsChecked = false;
        }

        private void LicenseDisAgree_Checked(object sender, RoutedEventArgs e)
        {
            Next.IsEnabled = false;
            LicenseAgree.IsChecked = false;
        }

        private void Window_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            DragMove();
        }
    }
}