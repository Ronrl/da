﻿using System.Windows.Controls;

namespace WpfSetup.LoadingControl.Control
{
    /// <summary>
    /// Interaction logic for LoadingAnimation.xaml
    /// </summary>
    public partial class LoadingAnimation : UserControl
    {
        public LoadingAnimation()
        {
            InitializeComponent();
        }
    }
}
