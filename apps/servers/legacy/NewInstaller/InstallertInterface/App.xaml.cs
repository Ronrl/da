﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Threading;

namespace WpfSetup
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        static Window _splashscreen;

        public static readonly int InstalledPackageSizeInMegabytes = 500;
        private void Application_Startup(object sender, StartupEventArgs e)
        {
           
            string systemDrive = Path.GetPathRoot(Environment.SystemDirectory);

            var driveInfo = DriveInfo.GetDrives().First(d => d.Name == systemDrive);

            if (driveInfo != null)
            {
                long availableSpaceInMegabytes = driveInfo.AvailableFreeSpace / 1024L / 1024L;

                if (availableSpaceInMegabytes < InstalledPackageSizeInMegabytes)
                {
                    MessageBox.Show(string.Format(
                            "Not enought space on system drive {0}. You must have at least {1} MB of free disk space. Please free disk space and try again.",
                            systemDrive, InstalledPackageSizeInMegabytes), "DeskAlerts installer error",
                        MessageBoxButton.OK, MessageBoxImage.Error);
                    Current.Shutdown();
                    return;

                }
            }


            _splashscreen = new SplashScreen();
            _splashscreen.Show();
            DoEvents();
            byte[] msiData = WpfSetup.Properties.Resources.setup;
            try
            {
                MsiFile = Path.Combine(Path.GetTempPath(), "DeskAlerts.msi");
                AttributesFile = "Attributes.txt";
                if (File.Exists(MsiFile))
                {
                    File.Delete(MsiFile);
                }

                try
                {

                    //MessageBox.Show(MsiFile);
                    if (!File.Exists(MsiFile) || new FileInfo(MsiFile).Length != msiData.Length)
                        File.WriteAllBytes(MsiFile, msiData);
                }
                catch
                {

                    //ignore
                }

            }


            catch (Exception)
            {

                Current.Shutdown();
            }


            AppDomain.CurrentDomain.AssemblyResolve += CurrentDomain_AssemblyResolve;
        }

        Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            return Assembly.Load(File.ReadAllBytes("..\\..\\..\\WixSharp\\WixSharp.Msi.dll"));
        }

        public static void HideSplashScreen()
        {
            _splashscreen.Close();
        }

        
        public static string MsiFile { get; set; }
        public static string AttributesFile { get; set; }
        public static void DoEvents()
        {
            Current.Dispatcher.Invoke(DispatcherPriority.Background, new Action(delegate { })); 
        }

    }
}