﻿using System.Windows;


namespace WpfSetup
{
    public partial class MainWindow
    {
        private void Previous_Click(object sender, RoutedEventArgs e)
        {
            if (Equals(FocusedPage, WelcomePage)) return;
            if (Equals(FocusedPage, SystemRequirements))
            {
                if (!TrialAttrib)
                {
                    Previous.IsEnabled = false;
                    Next.IsEnabled = false;
                    LicenseDisAgree.IsChecked = true;
                    LicenseAgree.IsChecked = false;
                }

            }
            if (Equals(FocusedPage, Trial))
            {
                Previous.IsEnabled = false;
                Next.IsEnabled = false;
                LicenseDisAgree.IsChecked = true;
                LicenseAgree.IsChecked = false;
            }
            if (Equals(FocusedPage, Progress))
            {
                Next.Visibility = Visibility.Visible;
                Next.IsEnabled = true;
                Install.IsEnabled = false;

            }
               

            CurrentPages.Remove(CurrentPages[CurrentPages.Count - 1]);
            CurrentPages[CurrentPages.Count - 1].Focus();
            FocusedPage = CurrentPages[CurrentPages.Count - 1];
            int visibilitycount = 0;
            if (Equals(FocusedPage, SmsModule))
            {
                SkipButton.Visibility = Visibility.Visible;
                visibilitycount++;
            }
            if (Equals(FocusedPage, SmtpModule))
            {
                SkipButton.Visibility = Visibility.Visible;
                visibilitycount++;
            }
            if (Equals(FocusedPage, EncryptModule))
            {
                SkipButton.Visibility = Visibility.Visible;
                visibilitycount++;
            }
            if (Equals(FocusedPage, TextToCallModule))
            {
                SkipButton.Visibility = Visibility.Visible;
                visibilitycount++;
            }

            SkipButton.Visibility = visibilitycount > 0 ? Visibility.Visible : Visibility.Collapsed;

            if (Equals(FocusedPage, WelcomePage))
            {
                StatusTitle.Content = "End User License Agreement (EULA)";
                AspNetText.Text = "";
            }
            if (Equals(FocusedPage, Trial))
            {
                StatusTitle.Content = StatusTitle.Content = Properties.Resources.TrialTitle;
                AspNetText.Text = "";
            }
            if (Equals(FocusedPage, SystemRequirements))
            {
                StatusTitle.Content = Properties.Resources.SystemRequirementsTitle;
            }
            if (Equals(FocusedPage, Iis))
            {
                StatusTitle.Content = Properties.Resources.IISTitle;
            }
            if (Equals(FocusedPage, Sql))
            {
                StatusTitle.Content = Properties.Resources.SQLTitle;
            }
            if (Equals(FocusedPage, Modules))
            {
                StatusTitle.Content = Properties.Resources.ModulesTitle;
            }
            if (Equals(FocusedPage, SmsModule))
            {
                StatusTitle.Content = Properties.Resources.ModuleSMSTitle;
            }
            if (Equals(FocusedPage, SmtpModule))
            {
                StatusTitle.Content = Properties.Resources.ModuleEmailTitle;
            }
            if (Equals(FocusedPage, EncryptModule))
            {
                StatusTitle.Content = Properties.Resources.ModuleEncryptTitle;
            }
            if (Equals(FocusedPage, TextToCallModule))
            {
                StatusTitle.Content = Properties.Resources.ModuleTextToCallTitle;
            }
            if (Equals(FocusedPage, Progress))
            {
                StatusTitle.Content = Properties.Resources.ProgressTitle;
            }
        }
    }
}
