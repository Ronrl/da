﻿using System.Windows;

namespace WpfSetup
{
    public partial class MainWindow
    {
        private void Smtpssl_Checked(object sender, RoutedEventArgs e)
        {
            SmtpPort.Text = "443";
        }

        private void Smtpssl_Unchecked(object sender, RoutedEventArgs e)
        {
            SmtpPort.Text = "25";
        }
    }
}
