﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Threading;
using System.Windows.Media;
using System.Windows.Controls;
using System.Collections.Generic;
using System.Linq;

namespace WpfSetup
{
    public class ResultItem
    {

        public string ResultText { get; set; }
        public string ResultBackground { get; set; }

        public ResultItem(string background, string text)
        {

            ResultBackground = background;
            ResultText = text;
        }
    }
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Messages To Result Window
        /// </summary>
        /// <param name="type">0 - info (no color), 1 - Warning (yellow color), 2 - Error (Red Color) </param>
        /// <param name="message"></param>
        void ResultMessage(int type, string message)
        {
            if ((message != null) && (message != string.Empty))
            {
                string color = "";
                switch (type)
                {
                    case 0:
                        {
                            color = "Black";
                            _resultItemsList.Add(new ResultItem(color, message));

                            break;
                        }
                    case 1:
                        {
                            color = "Yellow";
                            _resultItemsList.Add(new ResultItem(color, message));
                            break;
                        }
                    case 2:
                        {
                            Result.Focus();
                            Install.IsEnabled = false;
                            Next.IsEnabled = false;
                            Previous.IsEnabled = false;
                           
                            color = "Red";
                            _resultItemsList.Add(new ResultItem(color, message));
                            break;
                        }
                }
            }
        }
        
    }
}
