﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;


namespace WpfSetup
{
    public class BackgroundAction
    {
        public string Action { get; set; }
        public List<string> Data { get; set; }

        public bool ForResult { get; set; }

        public BackgroundAction(string action, List<string> data, bool res)
        {
            Action = action;
            Data = data;
            ForResult = res;

        }
    }
    public partial class MainWindow
    {


        private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {

            string func = (string)e.Argument;
            if (func == "GetWebsites")
            {
                e.Result = new BackgroundAction("GetWebsites", GetWebSites(), false);
            }
            if (func == "CheckSqlConnection")
            {
                e.Result = new BackgroundAction("CheckSqlConnection", null, CheckSqlConnection());
            }
            if (func == "CheckUserPrevilegies")
            {
                e.Result = new BackgroundAction("CheckUserPrevilegies", null, CheckUserPrevilegies());
            }
            if (func == "CheckSqlDatabase")
            {
                bool isDataBaseExists = false;
                if (!UseAzureAuth)
                {
                    isDataBaseExists = CheckDatabaseExists(GetConnectionString(), SqlDatabaseVar);
                }

                NewDataBase = !isDataBaseExists;
                e.Result = new BackgroundAction("CheckSqlDatabase", null, isDataBaseExists);
            }
            if (func == "CheckSystemRequirements")
            {
                e.Result = new BackgroundAction("CheckSystemRequirements", null, CheckSystemRequirements());
            }
            if (func == "CheckUrl")
            {
                e.Result = new BackgroundAction("CheckUrl", null, CheckUrl());
            }
        }
        private void backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                // error on dowork
                MessageBox.Show(e.Error.Message, "Error");

            }
            else
            {
                BackgroundAction workerresult = (BackgroundAction)e.Result;
                if (workerresult == null) throw new ArgumentNullException("sender");
                if (workerresult.Action == "CheckSystemRequirements")
                {
                    if (workerresult.ForResult)
                    {
                        SystemRequirementsLoading.Visibility = Visibility.Collapsed;
                        SystemRequirementsGrid.Visibility = Visibility.Visible;
                        Previous.IsEnabled = true;
                        Next.IsEnabled = true;
                        FocusedPage = SystemRequirements;
                        CurrentPages.Add(WelcomePage);
                        CurrentPages.Add(SystemRequirements);
                        StatusTitle.Content = WpfSetup.Properties.Resources.SystemRequirementsTitle;

                    }
                    else
                    {
                        SystemRequirementsLoading.Visibility = Visibility.Collapsed;
                        SystemRequirementsGrid.Visibility = Visibility.Visible;
                        Previous.IsEnabled = true;
                        Next.IsEnabled = false;
                        FocusedPage = SystemRequirements;
                        CurrentPages.Add(WelcomePage);
                        CurrentPages.Add(SystemRequirements);
                        StatusTitle.Content = WpfSetup.Properties.Resources.SystemRequirementsTitle;
                    }

                }
                else if (workerresult.Action == "GetWebsites")
                {
                    if (!string.IsNullOrEmpty(Config.Site))
                    {
                        var index = _webSites.IndexOf(_webSites.FirstOrDefault(a => a == Config.Site));
                        IisWebSite.SelectedIndex = index >= 0 ? index : 0;
                    }
                    else
                    {
                        IisWebSite.SelectedIndex = 0;
                    }

                    Previous.IsEnabled = true;
                    Next.IsEnabled = true;
                    IisLoading.Visibility = Visibility.Collapsed;
                    IisGrid.Visibility = Visibility.Visible;
                    Iis.Focus();
                    FocusedPage = Iis;
                    CurrentPages.Add(Iis);
                }
                else if (workerresult.Action == "CheckSqlConnection")
                {
                    Previous.IsEnabled = true;
                    Next.IsEnabled = true;
                    if (workerresult.ForResult)
                    {
                        SqlLoading.Visibility = Visibility.Collapsed;
                        SqlGrid.Visibility = Visibility.Visible;
                        if (!_backgroundWorker.IsBusy)
                        {
                            _backgroundWorker.RunWorkerAsync("CheckSqlDatabase");
                        }
                    }
                    else
                    {
                        SqlLoading.Visibility = Visibility.Collapsed;
                        SqlGrid.Visibility = Visibility.Visible;
                        SqlHost.Background = Brushes.Red;
                        if (!SQlUseWindowsAuth.IsChecked == true)
                        {
                            SqlLogin.Background = Brushes.Red;
                            SqlPassword.Background = Brushes.Red;
                        }
                        MessageBox.Show(Properties.Resources.SQLConnectionError + " " + SqlHost.Text);
                    }
                }
                else if (workerresult.Action == "CheckUserPrevilegies")
                {
                    Previous.IsEnabled = true;
                    Next.IsEnabled = true;
                    if (workerresult.ForResult)
                    {
                        SqlLoading.Visibility = Visibility.Collapsed;
                        SqlGrid.Visibility = Visibility.Visible;
                        SqlDropDatabase = true;
                        Modules.Focus();
                        FocusedPage = Modules;
                        CurrentPages.Add(Modules);
                        StatusTitle.Content = Properties.Resources.ModulesTitle;
                    }
                    else
                    {
                        SqlLoading.Visibility = Visibility.Collapsed;
                        SqlGrid.Visibility = Visibility.Visible;
                        SqlHost.Background = Brushes.Red;
                        if (!SQlUseWindowsAuth.IsChecked == true)
                        {
                            SqlLogin.Background = Brushes.Red;
                            SqlPassword.Background = Brushes.Red;
                        }
                        MessageBox.Show(Properties.Resources.SQLDatabasePremissionsMessage + " " + SqlHost.Text);
                    }
                }
                else if (workerresult.Action == "CheckUrl")
                {
                    IisGrid.Visibility = Visibility.Visible;
                    IisLoading.Visibility = Visibility.Collapsed;
                    if (workerresult.ForResult)
                    {

                        Sql.Focus();
                        FocusedPage = Sql;
                        CurrentPages.Add(Sql);
                        StatusTitle.Content = Properties.Resources.SQLTitle;
                    }
                    else
                    {
                        MessageBoxResult res = MessageBox.Show(Properties.Resources.IISValidateErrorText, "Warning",
                           MessageBoxButton.YesNo);
                        if (res == MessageBoxResult.Yes)
                        {
                            Sql.Focus();
                            FocusedPage = Sql;
                            CurrentPages.Add(Sql);
                            StatusTitle.Content = Properties.Resources.SQLTitle;
                        }
                    }

                }
                else if (workerresult.Action == "CheckSqlDatabase")
                {
                    SqlHost.Background = Brushes.White;
                    SqlLogin.Background = Brushes.White;
                    SqlPassword.Background = Brushes.White;
                    Previous.IsEnabled = true;
                    Next.IsEnabled = true;
                    if (workerresult.ForResult)
                    {
                        var res = MessageBox.Show(Properties.Resources.SQLDatabaseExistsMessage, "Warning",
                            MessageBoxButton.YesNoCancel);
                        if (res == MessageBoxResult.Yes)
                        {
                            SqlDropDatabase = false;
                            SqlLoading.Visibility = Visibility.Collapsed;
                            SqlGrid.Visibility = Visibility.Visible;
                            Modules.Focus();
                            FocusedPage = Modules;
                            CurrentPages.Add(Modules);
                            StatusTitle.Content = Properties.Resources.ModulesTitle;
                        }
                        else if (res == MessageBoxResult.No)
                        {
                            var res2 = MessageBox.Show(Properties.Resources.SQLDatabaseDeleteMessage,
                                "Attention", MessageBoxButton.YesNo);
                            if (res2 == MessageBoxResult.Yes)
                            {
                                if (!_backgroundWorker.IsBusy)
                                {
                                    _backgroundWorker.RunWorkerAsync("CheckUserPrevilegies");
                                }
                            }
                        }
                    }
                    else
                    {
                        if (!_backgroundWorker.IsBusy)
                        {
                            _backgroundWorker.RunWorkerAsync("CheckUserPrevilegies");
                        }
                    }
                }
            }
        }
        private void backgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {

        }

    }
}
