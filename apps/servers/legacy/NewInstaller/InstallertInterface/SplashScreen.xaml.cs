﻿using System;
using System.Windows;
using System.Windows.Media.Imaging;

namespace WpfSetup
{
    /// <summary>
    /// Interaction logic for SplashScreen.xaml
    /// </summary>
    public partial class SplashScreen
    {
        public SplashScreen()
        {
            InitializeComponent();
            Image.Source = LoadBitmap(WpfSetup.Properties.Resources.top_logo);
        }

        public static BitmapSource LoadBitmap(System.Drawing.Bitmap source)
        {
            try
            {
                IntPtr ip = source.GetHbitmap();

                var bs = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(ip,
                    IntPtr.Zero, Int32Rect.Empty,
                    BitmapSizeOptions.FromEmptyOptions());


                return bs;
            }
            catch
            {
                return null;
            }
        }
    }
}
