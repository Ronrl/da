@ECHO OFF
cls
call "C:\Program Files (x86)\Microsoft Visual Studio\2017\Enterprise\Common7\Tools\VsDevCmd.bat"
cd /d "%~dp0"
echo.Building project with Visual Studio 2017 Enterprise
.nuget\nuget.exe restore DeskAlerts.Web\DeskAlerts.Web.csproj -PackagesDirectory packages
.nuget\nuget.exe restore NewInstaller\NewInstaller.sln -PackagesDirectory NewInstaller\packages
echo.
echo.Building project with Visual Studio...
echo.
echo.^>^>Release x86
devenv  "DeskAlerts.Server.sln" /rebuild "Release 9|AnyCPU"
echo.
if exist installer\include\custom.sql (
	echo. ATTENTION!!! FOUND CUSTOM.SQL FILE
	echo.
)
copy "obj\Release 9\DeskAlertsDotNet.dll" sever\STANDARD\bin\DeskAlertsDotNet.dll >nul
echo.

set "contract_style=YYYYMMDD"
set "contract_format=^[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$"
echo Contract style: %contract_style%
set /P contract_date="Please enter contract date:  "
echo %contract_date%|findstr /i /r "%contract_format%" > nul
if Exist NewInstaller\Msi\SqlScripts\Custom_contract_date.sql del /F /Q NewInstaller\Msi\SqlScripts\Custom_contract_date.sql
echo USE [%%dbname_hook%%];>> NewInstaller\Msi\SqlScripts\Custom_contract_date.sql
echo SET LANGUAGE british;>> NewInstaller\Msi\SqlScripts\Custom_contract_date.sql
echo UPDATE version SET contract_date = cast( >> NewInstaller\Msi\SqlScripts\Custom_contract_date.sql
echo '%contract_date%'>> NewInstaller\Msi\SqlScripts\Custom_contract_date.sql
echo AS datetime) >> NewInstaller\Msi\SqlScripts\Custom_contract_date.sql

set "license_expire_date_style=YYYYMMDD"
set "license_expire_date_format=^[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$"
echo License expire date style: %license_expire_date_style%
set /P license_expire_date="Please enter license expire date:  "
echo %license_expire_date%|findstr /i /r "%license_expire_date_format%" > nul
if Exist NewInstaller\Msi\SqlScripts\Set_license_expire_date.sql del /F /Q NewInstaller\Msi\SqlScripts\Set_license_expire_date.sql
echo USE [%%dbname_hook%%];>> NewInstaller\Msi\SqlScripts\Set_license_expire_date.sql
echo SET LANGUAGE british;>> NewInstaller\Msi\SqlScripts\Set_license_expire_date.sql
echo UPDATE version SET license_expire_date = cast( >> NewInstaller\Msi\SqlScripts\Set_license_expire_date.sql
echo '%license_expire_date%'>> NewInstaller\Msi\SqlScripts\Set_license_expire_date.sql
echo AS datetime) >> NewInstaller\Msi\SqlScripts\Set_license_expire_date.sql

set "style=0.0.0.0abrc"
set "format=^[0-9][0-9]*\.[0-9][0-9]*\.[0-9][0-9]*\.[0-9a-z][0-9a-z]*$"
if not exist lastversion.log goto again
set /p version=<lastversion.log
echo %version%|findstr /i /r "%format%" > nul
if errorlevel 1 goto again
echo Please push Return key for keep version %version%
set /p version=or enter other version in style %style%  
goto check
:again
set /p version=Please enter version in style %style%  
:check
echo %version%|findstr /i /r "%format%" > nul
if errorlevel 1 echo Wrong version string. & goto again
echo %version%>lastversion.log

set attr=/DPRODUCT_VERSION=%version%

for /f "delims=." %%a in ("%version%") do set major=%%a
if %major% gtr 4 set attr=%attr% /DfiveServer
if %major% gtr 5 set attr=%attr% /DsixServer
if %major% gtr 6 set attr=%attr% /DsevenServer
if %major% gtr 7 set attr=%attr% /DeightServer
if %major% gtr 8 set attr=%attr% /DnineServer

set choices=
set lastchoices=______________________________
if not exist lastchoices.log goto no_lastchoices
set /p lastchoices=<lastchoices.log
set lastchoices=%lastchoices%______________________________
:no_lastchoices

:again_tbserv
set tbserv=%lastchoices:~0,1%
set text=
if not '%tbserv%' == '_' set text=(last value: %tbserv%) 
set /p tbserv=DEMO Server:                  %text%[y/n] 
if /i '%tbserv:~0,1%'=='n' set tbserv=n& goto no_tbserv
if /i not '%tbserv:~0,1%'=='y' goto again_tbserv
set attr=%attr% /DisDemo
set tbserv=y
:no_tbserv
set choices=%choices%%tbserv%

:again_enc
set choice=%lastchoices:~1,1%
set text=
if not '%choice%' == '_' set text=(last value: %choice%) 
set /p choice=Encryption:                   %text%[y/n] 
if /i '%choice:~0,1%'=='n' set choice=n& goto no_enc
if /i not '%choice:~0,1%'=='y' goto again_enc
set attr=%attr% /DisENCRYPTAddon
set choice=y
:no_enc
set choices=%choices%%choice%

:again_ad
set choice=%lastchoices:~2,1%
set text=
if not '%choice%' == '_' set text=(last value: %choice%) 
set /p choice=Active Directory:             %text%[y/n] 
if /i '%choice:~0,1%'=='n' set choice=n& goto no_ad
if /i not '%choice:~0,1%'=='y' goto again_ad
set attr=%attr% /DisADAddon
set choice=y
:no_ad
set choices=%choices%%choice%

:again_sso
set choice=%lastchoices:~3,1%
set text=
if not '%choice%' == '_' set text=(last value: %choice%) 
set /p choice=SSO (Single Sign-On):         %text%[y/n] 
if /i '%choice:~0,1%'=='n' set choice=n& goto no_sso
if /i not '%choice:~0,1%'=='y' goto again_sso
set attr=%attr% /DisSSOAddon
set choice=y
:no_sso
set choices=%choices%%choice%

:again_video
set choice=%lastchoices:~4,1%
set text=
if not '%choice%' == '_' set text=(last value: %choice%) 
set /p choice=Videoalert:                   %text%[y/n] 
if /i '%choice:~0,1%'=='n' set choice=n& goto no_video
if /i not '%choice:~0,1%'=='y' goto again_video
set attr=%attr% /DisVIDEOAddon
set choice=y
:no_video
set choices=%choices%%choice%

:again_email
set choice=%lastchoices:~5,1%
set text=
if not '%choice%' == '_' set text=(last value: %choice%) 
set /p choice=E-Mail:                       %text%[y/n] 
if /i '%choice:~0,1%'=='n' set choice=n& goto no_email
if /i not '%choice:~0,1%'=='y' goto again_email
set attr=%attr% /DisEMAILAddon
set choice=y
:no_email
set choices=%choices%%choice%

:again_twtr
set choice=%lastchoices:~6,1%
set text=
if not '%choice%' == '_' set text=(last value: %choice%) 
set /p choice=Social add-on for Twitter:    %text%[y/n] 
if /i '%choice:~0,1%'=='n' set choice=n& goto no_twtr
if /i not '%choice:~0,1%'=='y' goto again_twtr
set attr=%attr% /DisTWITTERAddon
set choice=y
:no_twtr
set choices=%choices%%choice%

:again_lnkdn
set choice=%lastchoices:~7,1%
set text=
if not '%choice%' == '_' set text=(last value: %choice%) 
set /p choice=Social add-on for LinkedIn:   %text%[y/n] 
if /i '%choice:~0,1%'=='n' set choice=n& goto no_lnkdn
if /i not '%choice:~0,1%'=='y' goto again_lnkdn
set attr=%attr% /DisLINKEDINAddon
set choice=y
:no_lnkdn
set choices=%choices%%choice%

:again_yammer
set choice=%lastchoices:~8,1%
set text=
if not '%choice%' == '_' set text=(last value: %choice%) 
set /p choice=Social module for Yammer:     %text%[y/n] 
if /i '%choice:~0,1%'=='n' set choice=n& goto no_yammer
if /i not '%choice:~0,1%'=='y' goto again_yammer
set attr=%attr% /DisYAMMERAddon
set choice=y
:no_yammer
set choices=%choices%%choice%

:again_textcall
set choice=%lastchoices:~9,1%
set text=
if not '%choice%' == '_' set text=(last value: %choice%) 
set /p choice=Text to call module:          %text%[y/n] 
if /i '%choice:~0,1%'=='n' set choice=n& goto no_textcall
if /i not '%choice:~0,1%'=='y' goto again_textcall
set attr=%attr% /DisTEXTCALLAddon
set choice=y
:no_textcall
set choices=%choices%%choice%

:again_tick
set choice=%lastchoices:~10,1%
set text=
if not '%choice%' == '_' set text=(last value: %choice%) 
set /p choice=Ticker alert:                 %text%[y/n] 
if /i '%choice:~0,1%'=='n' set choice=n& goto no_tick
if /i not '%choice:~0,1%'=='y' goto again_tick
set attr=%attr% /DisTICKERAddon
set choice=y
:no_tick
set choices=%choices%%choice%

:again_ss
set choice=%lastchoices:~11,1%
set text=
if not '%choice%' == '_' set text=(last value: %choice%) 
set /p choice=Screensaver:                  %text%[y/n] 
if /i '%choice:~0,1%'=='n' set choice=n& goto no_ss
if /i not '%choice:~0,1%'=='y' goto again_ss
set attr=%attr% /DisSCREENSAVERAddon
set choice=y
:no_ss
set choices=%choices%%choice%

:again_wp
set choice=%lastchoices:~12,1%
set text=
if not '%choice%' == '_' set text=(last value: %choice%) 
set /p choice=Wallpaper:                    %text%[y/n] 
if /i '%choice:~0,1%'=='n' set choice=n& goto no_wp
if /i not '%choice:~0,1%'=='y' goto again_wp
set attr=%attr% /DisWALLPAPERAddon
set choice=y
:no_wp
set choices=%choices%%choice%

:again_rss
set choice=%lastchoices:~13,1%
set text=
if not '%choice%' == '_' set text=(last value: %choice%) 
set /p choice=RSS:                          %text%[y/n] 
if /i '%choice:~0,1%'=='n' set choice=n& goto no_rss
if /i not '%choice:~0,1%'=='y' goto again_rss
set attr=%attr% /DisRSSAddon
set choice=y
:no_rss
set choices=%choices%%choice%

:again_im
set choice=%lastchoices:~14,1%
set text=
if not '%choice%' == '_' set text=(last value: %choice%) 
set /p choice=Emergency notifications:      %text%[y/n] 
if /i '%choice:~0,1%'=='n' set choice=n& goto no_im
if /i not '%choice:~0,1%'=='y' goto again_im
set attr=%attr% /DisIMAddon
set choice=y
:no_im
set choices=%choices%%choice%

:again_fsa
set choice=%lastchoices:~15,1%
set text=
if not '%choice%' == '_' set text=(last value: %choice%) 
set /p choice=Fullscreen alert:             %text%[y/n] 
if /i '%choice:~0,1%'=='n' set choice=n& goto no_fsa
if /i not '%choice:~0,1%'=='y' goto again_fsa
set attr=%attr% /DisFULLSCREENAddon
set choice=y
:no_fsa
set choices=%choices%%choice%

:again_mobile
set choice=%lastchoices:~16,1%
set text=
if not '%choice%' == '_' set text=(last value: %choice%) 
set /p choice=Mobile sending module:        %text%[y/n] 
if /i '%choice:~0,1%'=='n' set choice=n& goto no_mobile
if /i not '%choice:~0,1%'=='y' goto again_mobile
set attr=%attr% /DisMOBILEAddon
set choice=y
:no_mobile
set choices=%choices%%choice%

:again_digsign
set choice=%lastchoices:~17,1%
set text=
if not '%choice%' == '_' set text=(last value: %choice%) 
set /p choice=Digital signage:              %text%[y/n] 
if /i '%choice:~0,1%'=='n' set choice=n& goto no_digsign
if /i not '%choice:~0,1%'=='y' goto again_digsign
set attr=%attr% /DisDIGSIGNAddon
set choice=y
:no_digsign
set choices=%choices%%choice%

:again_stat
set choice=%lastchoices:~18,1%
set text=
if not '%choice%' == '_' set text=(last value: %choice%) 
set /p choice=Extended statistics:          %text%[y/n] 
if /i '%choice:~0,1%'=='n' set choice=n& goto no_stat
if /i not '%choice:~0,1%'=='y' goto again_stat
set attr=%attr% /DisSTATISTICSAddon
set choice=y
:no_stat
set choices=%choices%%choice%

:againg_surv
set choice=%lastchoices:~19,1%
set text=
if not '%choice%' == '_' set text=(last value: %choice%) 
set /p choice=Survey manager:               %text%[y/n] 
if /i '%choice:~0,1%'=='n' set choice=n& goto no_surv
if /i not '%choice:~0,1%'=='y' goto againg_surv
set attr=%attr% /DisSURVEYSAddon
set choice=y
:no_surv
set choices=%choices%%choice%

:again_smpp
set choice=%lastchoices:~20,1%
set text=
if not '%choice%' == '_' set text=(last value: %choice%) 
set /p choice=SMPP SMS:                     %text%[y/n] 
if /i '%choice:~0,1%'=='n' set choice=n& goto no_smpp
if /i not '%choice:~0,1%'=='y' goto again_smpp
set attr=%attr% /DisSMPPAddon
set attr=%attr% /DisSMSAddon
set sms_module=y
set choice=y%smsmodule%
echo SMS module has been added
:no_smpp
set choices=%choices%%choice%

if '%sms_module%'=='y' (
	set choices=%choices%y
	goto again_campaigns
)

:again_sms
set choice=%lastchoices:~21,1%
if not '%choice%' == '_' set text=(last value: %choice%) 
set /p choice=SMS:                          %text%[y/n]
if /i '%choice:~0,1%'=='n' set choice=n& goto no_sms
if /i not '%choice:~0,1%'=='y' goto again_sms
set attr=%attr% /DisSMSAddon
set choice=y
:no_sms
set choices=%choices%%choice%

:again_campaigns
set choice=%lastchoices:~22,1%
set text=
if not '%choice%' == '_' set text=(last value: %choice%) 
set /p choice=Campaigns:                    %text%[y/n] 
if /i '%choice:~0,1%'=='n' set choice=n& goto no_campaigns
if /i not '%choice:~0,1%'=='y' goto again_campaigns
set attr=%attr% /DisCAMPAIGNAddon
set choice=y
:no_campaigns
set choices=%choices%%choice%

:again_multiplealerts
set choice=%lastchoices:~23,1%
set text=
if not '%choice%' == '_' set text=(last value: %choice%) 
set /p choice=Multiple Alerts:       	      %text%[y/n] 
if /i '%choice:~0,1%'=='n' set choice=n& goto no_multiplealerts
if /i not '%choice:~0,1%'=='y' goto again_multiplealerts
set attr=%attr% /DisMultipleAlerts
set choice=y
:no_multiplealerts
set choices=%choices%%choice%

:again_multilanguage
set choice=%lastchoices:~24,1%
set text=
if not '%choice%' == '_' set text=(last value: %choice%) 
set /p choice=MultiLanguage alerts:         %text%[y/n] 
if /i '%choice:~0,1%'=='n' set choice=n& goto no_multilanguage
if /i not '%choice:~0,1%'=='y' goto again_multilanguage
set attr=%attr% /DisMULTILANGUAGEAddon
set choice=y
:no_multilanguage
set choices=%choices%%choice%

:again_restapi
set choice=%lastchoices:~25,1%
set text=
if not '%choice%' == '_' set text=(last value: %choice%) 
set /p choice=REST API:                     %text%[y/n] 
if /i '%choice:~0,1%'=='n' set choice=n& goto no_restapi
if /i not '%choice:~0,1%'=='y' goto again_restapi
set attr=%attr% /DisRESTAPIAddon
set choice=y
:no_restapi
set choices=%choices%%choice%

:again_approve
set choice=%lastchoices:~26,1%
set text=
if not '%choice%' == '_' set text=(last value: %choice%) 
set /p choice=Approval:                     %text%[y/n] 
if /i '%choice:~0,1%'=='n' set choice=n& goto no_approve
if /i not '%choice:~0,1%'=='y' goto again_approve
set attr=%attr% /DisAPPROVEAddon
set choice=y
:no_approve
set choices=%choices%%choice%

:again_singleinstance
set choice=%lastchoices:~27,1%
set text=
if not '%choice%' == '_' set text=(last value: %choice%) 
set /p choice=single instance server:       %text%[y/n] 
if /i '%choice:~0,1%'=='n' (
	set choice=n
	set isSingle=n
	goto no_singleinstance
)
if /i not '%choice:~0,1%'=='y' (
	goto again_singleinstance
) ELSE (
	set isSingle=y
)
set attr=%attr% /DisSingleInstance
set choice=y
:no_singleinstance
set choices=%choices%%choice%

:again_bulkuploadofrecipients
set choice=%lastchoices:~28,1%
set text=
if not '%choice%' == '_' set text=(last value: %choice%) 
set /p choice=Bulk upload of recipients:    %text%[y/n] 
if /i '%choice:~0,1%'=='n' set choice=n& goto no_bulkuploadofrecipients
if /i not '%choice:~0,1%'=='y' goto again_bulkuploadofrecipients
set attr=%attr% /DisBulkUploadOfRecipients
set choice=y
:no_bulkuploadofrecipients
set choices=%choices%%choice%


:again_ls
set choice=%lastchoices:~29,1%
set text=
if not '%choice%' == '_' set text=(last value: %choice%) 
set /p choice=RR-563 Lockscreen:            %text%[y/n] 
if /i '%choice:~0,1%'=='n' set choice=n& goto no_ls
if /i not '%choice:~0,1%'=='y' goto again_ls
set attr=%attr% /DisLOCKSCREENAddon
set choice=y
:no_ls
set choices=%choices%%choice%


set attr=%attr% /DisBLOGAddon

set attr=%attr% /DisWIDGETSTATAddon

set attr=%attr% /DisWEBPLUGINAddon

ECHO %choices%>lastchoices.log

ECHO.
if not exist lastlicenses.log goto again_l
set /p licenses=<lastlicenses.log
echo %licenses%|findstr /r "^[0-9][0-9]*$" > nul
if errorlevel 1 goto again_l
echo Please push Return key for keep number of licenses: %licenses%
set /p licenses=or enter new number of licenses 
goto check_l
:again_l
set /p licenses=Please enter number of licenses  
:check_l
echo %licenses%|findstr /r "^[0-9][0-9]*$" > nul
if errorlevel 1 echo Wrong number of licenses. & goto again_l
>lastlicenses.log echo.%licenses%
if "%licenses%" == "0" (
set attr=%attr% /Dlicenses=unlimited
) else (
set attr=%attr% /Dlicenses=%licenses%
)

ECHO.
if not exist lastmaxpublisherscount.log goto again_l
set /p maxpublisherscount=<lastmaxpublisherscount.log
echo %maxpublisherscount%|findstr /r "^[0-9][0-9]*$" > nul
if errorlevel 1 goto again_l
echo Please push Return key for keep number of max publishers count: %maxpublisherscount%
set /p maxpublisherscount=or enter new number of max publishers count 
goto check_l
:again_l
set /p maxpublisherscount=Please enter number of max publishers count  
:check_l
echo %maxpublisherscount%|findstr /r "^[0-9][0-9]*$" > nul
if errorlevel 1 echo Wrong number of max publishers count. & goto again_l
>lastmaxpublisherscount.log echo.%maxpublisherscount%
set attr=%attr% /Dmaxpublisherscount=%maxpublisherscount%

ECHO.
if not exist lasttrial.log goto again_t
set /p trial=<lasttrial.log
echo %trial%|findstr /r "^[0-9][0-9]*$" > nul
if errorlevel 1 goto again_t
echo Please push Return key for keep number of trial days (0 - not trial): %trial%
set /p trial=or enter new number of trial days (0 - not trial)  
goto check_t
:again_t
set /p trial=Please enter number of trial days (0 - not trial)  
:check_t
echo %trial%|findstr /r "^[0-9][0-9]*$" > nul
if errorlevel 1 echo Wrong number of trial days. & goto again_t
>lasttrial.log echo.%trial%
if not "%trial%" == "0" (
	set attr=%attr% /Dtrial=%trial%
	set version=%version%.trial
)

rem ECHO.
rem ECHO Patching...

rem rmdir /q /s tmp 2>nul
rem mkdir tmp
rem copy checker\Release\Win32\DeskAlertsServer32.dll tmp\DeskAlertsServer32.dll >nul
rem copy checker\Release\x64\DeskAlertsServer64.dll tmp\DeskAlertsServer64.dll >nul

rem ECHO.
rem ECHO Signing...
rem ECHO.

rem installer\signtool.exe sign /tr http://timestamp.comodoca.com/?td=sha256 /td sha256 /fd sha256 /f installer\CodeSignCertificate.pfx /p DeskAlerts /v NewInstaller\additionalfiles\DLL\DeskAlertsServer32.dll NewInstaller\additionalfiles\DLL\DeskAlertsServer64.dll

rem ECHO.
rem ECHO Building...
rem ECHO.

if Exist NewInstaller\InstallertInterface\Resources\Attributes.txt del /F /Q NewInstaller\InstallertInterface\Resources\Attributes.txt
if Exist NewInstaller\Msi\Attributes.txt del /F /Q NewInstaller\Msi\Attributes.txt
if Exist NewInstaller\Msi\setup.msi del /F /Q NewInstaller\Msi\setup.msi
if Exist NewInstaller\InstallertInterface\bin\Release\WpfSetup.exe del /F /Q NewInstaller\InstallertInterface\bin\Release\WpfSetup.exe

echo %attr% > NewInstaller\Msi\Attributes.txt
echo %attr% > NewInstaller\InstallertInterface\Resources\Attributes.txt

call "C:\Program Files (x86)\Microsoft Visual Studio\2017\Enterprise\Common7\Tools\VsDevCmd.bat"
cd /d "%~dp0"
echo.
echo.Building project with Visual Studio...
echo.
echo.^>^>Release
devenv  "NewInstaller\NewInstaller.sln" /rebuild "Release|Any CPU"
echo.

copy "NewInstaller\InstallertInterface\bin\Release\WpfSetup.exe" "ServerInstallation.exe"

if "%ERRORLEVEL%" == "0" (
	ECHO Done. View build.log
	ECHO.
	installer\signtool.exe sign /tr http://timestamp.comodoca.com/?td=sha256 /td sha256 /fd sha256 /f installer\CodeSignCertificate.pfx /p DeskAlerts /v ServerInstallation.exe
	
	IF "%isSingle%" == "y" (
		move ServerInstallation.exe DeskAlerts.Server.v%version%.exe
	) ELSE (
		move ServerInstallation.exe DeskAlerts.Server.v%version%_MultiInstance.exe
	)
) else (
	ECHO Failed. View build.log
)
ECHO.
rmdir /q /s tmp 2>nul
pause