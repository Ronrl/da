using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.ApplicationCore.Models.ContentTypes;
using DeskAlerts.ApplicationCore.Utilities;
using DeskAlerts.ContentDeliveringService.Interfaces;
using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DeskAlerts.Scheduler.ContentJobScheduling
{
    public class QuartzContentJobScheduler : IContentJobScheduler
    {
        private readonly IAlertRepository _alertRepository;
        private readonly IRecurrenceRepository _recurrenceRepository;

        private readonly IContentDeliveringService<AlertContentType> _alertsContentDeliveringService;
        private readonly IContentDeliveringService<TickerContentType> _tickersContentDeliveringService;
        private readonly IContentDeliveringService<RsvpContentType> _rsvpContentDeliveringService;
        private readonly IContentDeliveringService<SurveyContentType> _surveysContentDeliveringService;
        private readonly IContentDeliveringService<ScreensaverContentType> _screensaverContentDeliveringService;
        private readonly IContentDeliveringService<WallpaperContentType> _wallpapersContentDeliveringService;
        private readonly IContentDeliveringService<VideoAlertContentType> _videoAlertContentDeliveringService;

        private readonly IScheduledContentService _repeatableContentService;

        public QuartzContentJobScheduler(
            IAlertRepository alertRepository,
            IRecurrenceRepository recurrenceRepository,
            IContentDeliveringService<AlertContentType> alertsContentDeliveringService,
            IContentDeliveringService<TickerContentType> tickersContentDeliveringService,
            IContentDeliveringService<RsvpContentType> rsvpContentDeliveringService,
            IContentDeliveringService<SurveyContentType> surveysContentDeliveringService,
            IContentDeliveringService<ScreensaverContentType> screensaverContentDeliveringService,
            IContentDeliveringService<WallpaperContentType> wallpapersContentDeliveringService,
            IContentDeliveringService<VideoAlertContentType> videoAlertContentDeliveringService,
            IScheduledContentService repeatableContentService
            )
        {
            _alertRepository = alertRepository;
            _recurrenceRepository = recurrenceRepository;
            _alertsContentDeliveringService = alertsContentDeliveringService;
            _tickersContentDeliveringService = tickersContentDeliveringService;
            _rsvpContentDeliveringService = rsvpContentDeliveringService;
            _surveysContentDeliveringService = surveysContentDeliveringService;
            _screensaverContentDeliveringService = screensaverContentDeliveringService;
            _wallpapersContentDeliveringService = wallpapersContentDeliveringService;
            _videoAlertContentDeliveringService = videoAlertContentDeliveringService;
            _repeatableContentService = repeatableContentService;
        }

        public async Task<bool> CreateJob(ContentJobDto contentJobDto)
        {
            if (contentJobDto == null)
            {
                throw new ArgumentNullException();
            }

            try
            {
                var scheduler = await StdSchedulerFactory.GetDefaultScheduler();
                await scheduler.Start();

                var contentId = contentJobDto.ContentId;
                var startDateTime = contentJobDto.StartDateTime.ToUniversalTime();

                IEnumerable<int> timeZones;
                object contentDeliveringService;
                JobBuilder jobBuilder;

                var contentType = _alertRepository.GetContentTypeByContentId(contentId);
                //TODO: kick off this crutch
                switch (contentType)
                {
                    case AlertType.SimpleAlert:
                        timeZones = _alertsContentDeliveringService.GetAllTimeZones();
                        contentDeliveringService = _alertsContentDeliveringService;
                        jobBuilder = JobBuilder.Create<QuartzContentJob<AlertContentType>>();
                        break;
                    case AlertType.Ticker:
                        timeZones = _tickersContentDeliveringService.GetAllTimeZones();
                        contentDeliveringService = _tickersContentDeliveringService;
                        jobBuilder = JobBuilder.Create<QuartzContentJob<TickerContentType>>();
                        break;
                    case AlertType.Rsvp:
                        timeZones = _rsvpContentDeliveringService.GetAllTimeZones();
                        contentDeliveringService = _rsvpContentDeliveringService;
                        jobBuilder = JobBuilder.Create<QuartzContentJob<RsvpContentType>>();
                        break;
                    case AlertType.SimpleSurvey:
                    case AlertType.SurveyPoll:
                    case AlertType.SurveyQuiz:
                        timeZones = _surveysContentDeliveringService.GetAllTimeZones();
                        contentDeliveringService = _surveysContentDeliveringService;
                        jobBuilder = JobBuilder.Create<QuartzContentJob<SurveyContentType>>();
                        break;
                    case AlertType.ScreenSaver:
                        timeZones = _screensaverContentDeliveringService.GetAllTimeZones();
                        contentDeliveringService = _screensaverContentDeliveringService;
                        jobBuilder = JobBuilder.Create<QuartzContentJob<ScreensaverContentType>>();
                        break;
                    case AlertType.Wallpaper:
                        timeZones = _wallpapersContentDeliveringService.GetAllTimeZones();
                        contentDeliveringService = _wallpapersContentDeliveringService;
                        jobBuilder = JobBuilder.Create<QuartzContentJob<WallpaperContentType>>();
                        break;
                    case AlertType.VideoAlert:
                        timeZones = _videoAlertContentDeliveringService.GetAllTimeZones();
                        contentDeliveringService = _videoAlertContentDeliveringService;
                        jobBuilder = JobBuilder.Create<QuartzContentJob<VideoAlertContentType>>();
                        break;
                    default:
                        throw new ArgumentException();
                }

                foreach (var timezone in timeZones)
                {
                    var job = jobBuilder
                        .WithIdentity(new JobKey(Guid.NewGuid().ToString()))
                        .UsingJobData("contentId", contentId)
                        .UsingJobData("timeZone", timezone)
                        .Build();
                    job.JobDataMap["contentDeliveringService"] = contentDeliveringService;

                    var actualDateTime = DateTimeUtils.GetServerDateTimeByTimeZoneAndLocalDateTime(timezone, startDateTime);
                    var trigger = TriggerBuilder.Create()
                        .StartAt(DateTime.SpecifyKind(actualDateTime, DateTimeKind.Local))
                        .Build();

                    await scheduler.ScheduleJob(job, trigger);
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> CreateRepeatableJob(ContentJobDto contentJobDto)
        {
            if (contentJobDto == null)
            {
                throw new ArgumentNullException();
            }

            try
            {
                var scheduler = await StdSchedulerFactory.GetDefaultScheduler();
                await scheduler.Start();

                var contentId = contentJobDto.ContentId;
                var startDateTime = contentJobDto.StartDateTime.ToUniversalTime();

                IEnumerable<int> timeZones;
                object contentDeliveringService;
                JobBuilder jobBuilder;

                var contentType = _alertRepository.GetContentTypeByContentId(contentId);
                //TODO: kick off this crutch
                switch (contentType)
                {
                    case AlertType.SimpleAlert:
                        timeZones = _alertsContentDeliveringService.GetAllTimeZones();
                        contentDeliveringService = _alertsContentDeliveringService;
                        jobBuilder = JobBuilder.Create<QuartzRepeatableContentJob<AlertContentType>>();
                        break;
                    case AlertType.Ticker:
                        timeZones = _tickersContentDeliveringService.GetAllTimeZones();
                        contentDeliveringService = _tickersContentDeliveringService;
                        jobBuilder = JobBuilder.Create<QuartzRepeatableContentJob<TickerContentType>>();
                        break;
                    case AlertType.Rsvp:
                        timeZones = _rsvpContentDeliveringService.GetAllTimeZones();
                        contentDeliveringService = _rsvpContentDeliveringService;
                        jobBuilder = JobBuilder.Create<QuartzRepeatableContentJob<RsvpContentType>>();
                        break;
                    case AlertType.VideoAlert:
                        timeZones = _videoAlertContentDeliveringService.GetAllTimeZones();
                        contentDeliveringService = _videoAlertContentDeliveringService;
                        jobBuilder = JobBuilder.Create<QuartzRepeatableContentJob<VideoAlertContentType>>();
                        break;
                    default:
                        throw new ArgumentException();
                }

                foreach (var timezone in timeZones)
                {
                    var job = jobBuilder
                        .WithIdentity(new JobKey(Guid.NewGuid().ToString()))
                        .UsingJobData("contentId", contentId)
                        .UsingJobData("timeZone", timezone)
                        .Build();

                    job.JobDataMap["recurrenceRepository"] = _recurrenceRepository;
                    job.JobDataMap["contentDeliveringService"] = contentDeliveringService;
                    job.JobDataMap["repeatableContentService"] = _repeatableContentService;

                    var actualDateTime = DateTimeUtils.GetServerDateTimeByTimeZoneAndLocalDateTime(timezone, startDateTime);
                    var trigger = TriggerBuilder.Create()
                        .StartAt(DateTime.SpecifyKind(actualDateTime, DateTimeKind.Local))
                        .WithSimpleSchedule(scheduling => scheduling.WithIntervalInHours(24).RepeatForever())
                        .Build();

                    await scheduler.ScheduleJob(job, trigger);
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteJob(ContentJobDto contentJobDto)
        {
            return true;
        }
    }
}
