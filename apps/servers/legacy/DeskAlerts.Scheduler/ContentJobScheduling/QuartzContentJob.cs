﻿using System;
using System.Threading.Tasks;
using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.ContentDeliveringService.Interfaces;
using Quartz;

namespace DeskAlerts.Scheduler.ContentJobScheduling
{
    public class QuartzContentJob<T> : IJob where T : IContentType
    {
        private static object _lockObject = new object();

        public Task Execute(IJobExecutionContext context)
        {
            lock (_lockObject)
            {
                var dataMap = context.JobDetail.JobDataMap;

                var contentId = dataMap.GetLongValue("contentId");
                var timeZone = dataMap.GetIntValue("timeZone");
                if (!(context.JobDetail.JobDataMap["contentDeliveringService"] is IContentDeliveringService<T> contentDeliveringService))
                {
                    throw new InvalidCastException();
                }

                contentDeliveringService.DeliveryToRecipientsOfContent(contentId, timeZone);
            }
            
            return Task.CompletedTask;
        }
    }
}
