﻿using System;
using DeskAlerts.ContentDeliveringService.Interfaces;
using DeskAlerts.ApplicationCore.Models.ContentTypes;
using Quartz;
using System.Threading.Tasks;

namespace DeskAlerts.Scheduler.ContentJobScheduling
{
    class QuartzLockscreenJob : IJob
    {
        public Task Execute(IJobExecutionContext context)
        {
            var dataMap = context.JobDetail.JobDataMap;

            var contentId = dataMap.GetLongValue("contentId");
            var timeZone = dataMap.GetIntValue("timeZone");
            if (!(context.JobDetail.JobDataMap["lockscreensContentDeliveringService"] is IContentDeliveringService<LockscreenContentType> contentDeliveringService))
            {
                throw new InvalidCastException();
            }

            contentDeliveringService.DeliveryToRecipientsOfContent(contentId, timeZone);
            return Task.CompletedTask;
        }
    }
}
