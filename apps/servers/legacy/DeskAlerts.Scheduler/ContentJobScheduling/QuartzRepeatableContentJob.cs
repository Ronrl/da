﻿using System;
using System.Threading.Tasks;
using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.ContentDeliveringService.Interfaces;
using Quartz;

namespace DeskAlerts.Scheduler.ContentJobScheduling
{
    public class QuartzRepeatableContentJob<T> : IJob where T : IContentType
    {
        private static object _lockObject = new object();

        public Task Execute(IJobExecutionContext context)
        {
            lock (_lockObject)
            {
                var dataMap = context.JobDetail.JobDataMap;
                var contentId = dataMap.GetLongValue("contentId");
                var timeZone = dataMap.GetIntValue("timeZone");

                if (!(dataMap["recurrenceRepository"] is IRecurrenceRepository recurrenceRepository) ||
                    !(dataMap["contentDeliveringService"] is IContentDeliveringService<T> contentDeliveringService) ||
                    !(dataMap["repeatableContentService"] is IScheduledContentService repeatableContentService))
                {
                    throw new InvalidCastException();
                }

                if (recurrenceRepository.IsActualDayRecurrenceForContent(DateTime.Now, contentId))
                {
                    var instanceId = repeatableContentService.CreateInstanceFromTemplate(contentId);
                    contentDeliveringService.DeliveryToRecipientsOfContent(instanceId, timeZone);
                }
            }
        
            return Task.CompletedTask;
        }
    }
}
