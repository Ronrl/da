﻿using System.Threading.Tasks;

namespace DeskAlerts.Scheduler.ContentJobScheduling
{
    public interface IContentJobScheduler
    {
        Task<bool> CreateJob(ContentJobDto contentJobDto);
        Task<bool> CreateRepeatableJob(ContentJobDto contentJobDto);
        bool DeleteJob(ContentJobDto contentJobDto);
    }
}
