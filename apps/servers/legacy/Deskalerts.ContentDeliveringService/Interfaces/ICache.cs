﻿using DeskAlerts.ContentDeliveringService.Models;
using System.Collections.Generic;

namespace DeskAlerts.ContentDeliveringService.Interfaces
{
    public interface ICache
    {
        bool Add(ContentDeliveringKey key, string value);
        string Get(ContentDeliveringKey key);
        bool Delete(long userId);
        bool Delete(IEnumerable<long> usersIds);
        bool Delete(IEnumerable<long> usersIds, int timeZone);
        bool DeleteAll();
    }
}