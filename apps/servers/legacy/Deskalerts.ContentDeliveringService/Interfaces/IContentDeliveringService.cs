﻿using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.ContentDeliveringService.Models;
using System.Collections.Generic;

namespace DeskAlerts.ContentDeliveringService.Interfaces
{
    public interface IContentDeliveringService<TContent> where TContent : IContentType
    {
        IEnumerable<int> GetAllTimeZones();
        bool DeliveryToBroadcast();
        bool DeliveryToUser(long userId);
        bool DeliveryToUsers(IEnumerable<long> usersIds);
        bool DeliveryToRecipientsOfContent(long contentId);
        bool DeliveryToRecipientsOfContent(long contentId, int timeZone);
        string Get(ContentDeliveringKey key, string ip, string computerName);
    }
}