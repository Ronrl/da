﻿using System;

namespace DeskAlerts.ContentDeliveringService.Models
{
    public class ContentDeliveringKey
    {
        public const char Separator = '@';

        public int TimeZone { get; }
        public long UserId { get; }
        public Guid DeskBarId { get; }

        public ContentDeliveringKey(int timeZone, long userId, Guid deskBarId)
        {
            TimeZone = timeZone;
            UserId = userId;
            DeskBarId = deskBarId;
        }

        public string ToStringForm()
        {
            return $"{TimeZone}{Separator}{UserId}{Separator}{DeskBarId}";
        }

        public static ContentDeliveringKey CreateFromString(string key)
        {
            if (string.IsNullOrEmpty(key))
            {
                throw new ArgumentNullException(nameof(key));
            }

            var wordStrings = key.Split(Separator);
            if (wordStrings.Length != 3)
            {
                throw new ArgumentException($"String \"{key}\" doesn't contained separator \"{Separator}\".");
            }

            var timeZoneParse = int.TryParse(wordStrings[0], out var timeZone);
            var userIdParse = int.TryParse(wordStrings[1], out var userId);
            var deskBarIdParse = Guid.TryParse(wordStrings[2], out var deskBarId);

            if (!timeZoneParse || !userIdParse || !deskBarIdParse)
            {
                throw new ArgumentException("Parsing error.");
            }

            return new ContentDeliveringKey(timeZone, userId, deskBarId);
        }
    }
}
