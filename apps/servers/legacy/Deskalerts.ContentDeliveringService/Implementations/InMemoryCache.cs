﻿using DeskAlerts.ContentDeliveringService.Interfaces;
using DeskAlerts.ContentDeliveringService.Models;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace DeskAlerts.ContentDeliveringService.Implementations
{
    public class InMemoryCache : ICache
    {
        private int capacity;
        private Dictionary<string, LinkedListNode<LruCacheItem<string, string>>> cacheMap = new Dictionary<string, LinkedListNode<LruCacheItem<string, string>>>();
        private LinkedList<LruCacheItem<string, string>> lruList = new LinkedList<LruCacheItem<string, string>>();

        public InMemoryCache(int capacity)
        {
            this.capacity = capacity;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public bool Add(ContentDeliveringKey key, string val)
        {
            try
            {
                lock (cacheMap)
                {
                    if (cacheMap.Count >= capacity)
                    {
                        DeleteFirst();
                    }
                }

                var cacheItem = new LruCacheItem<string, string>(key.ToStringForm(), val);
                var node = new LinkedListNode<LruCacheItem<string, string>>(cacheItem);
                lock (lruList)
                {
                    lruList.AddLast(node);
                }

                lock (cacheMap)
                {
                    cacheMap.Add(key.ToStringForm(), node);
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public string Get(ContentDeliveringKey key)
        {
            LinkedListNode<LruCacheItem<string, string>> node;
            lock (cacheMap)
            {
                if (!cacheMap.TryGetValue(key.ToStringForm(), out node))
                {
                    return null;
                }
            }

            var value = node.Value.Value;
            lock (lruList)
            {
                lruList.Remove(node);
                lruList.AddLast(node);
            }

            return value;
        }

        private void DeleteFirst()
        {
            // Remove from LRUPriority
            LinkedListNode<LruCacheItem<string, string>> node;

            lock (lruList)
            {
                node = lruList.First;
                lruList.RemoveFirst();
            }

            // Remove from cache
            lock (cacheMap)
            {
                cacheMap.Remove(node.Value.Key);
            }
        }

        public bool Delete(long userId)
        {
            LruCacheItem<string, string> node;
            lock (lruList)
            {
                node = lruList.FirstOrDefault(item => ContentDeliveringKey.CreateFromString(item.Key).UserId == userId);
            }

            if (node == null)
            {
                return true;
            }

            lock (lruList)
            {
                lruList.Remove(node);
            }

            lock (cacheMap)
            {
                cacheMap.Remove(node.Key);
            }

            return true;
        }

        public bool Delete(IEnumerable<long> usersIds)
        {
            LruCacheItem<string, string>[] nodes;
            lock (lruList)
            {
                nodes = lruList
                    .Where(item => usersIds.Contains(ContentDeliveringKey.CreateFromString(item.Key).UserId))
                    .ToArray();
            }

            foreach (var node in nodes)
            {
                lock (lruList)
                {
                    lruList.Remove(node);
                }

                lock (cacheMap)
                {
                    cacheMap.Remove(node.Key);
                }
            }

            return true;
        }

        public bool Delete(IEnumerable<long> usersIds, int timeZone)
        {
            LruCacheItem<string, string>[] nodes;
            lock (lruList)
            {
                nodes = lruList
                    .Where(item =>
                    {
                        var cdKey = ContentDeliveringKey.CreateFromString(item.Key);
                        return cdKey.TimeZone == timeZone && usersIds.Contains(cdKey.UserId);
                    })
                    .ToArray();
            }


            foreach (var node in nodes)
            {
                lock (lruList)
                {
                    lruList.Remove(node);
                }

                lock (cacheMap)
                {
                    cacheMap.Remove(node.Key);
                }
            }

            return true;
        }

        public bool DeleteAll()
        {
            lock (lruList)
            {
                lruList.Clear();
            }

            lock (cacheMap)
            {
                cacheMap.Clear();
            }

            return true;
        }
    }

    class LruCacheItem<TK, TV>
    {
        public LruCacheItem(TK k, TV v)
        {
            Key = k;
            Value = v;
        }
        public TK Key;
        public TV Value;
    }
}
