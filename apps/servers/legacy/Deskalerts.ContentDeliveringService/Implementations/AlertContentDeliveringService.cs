﻿using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.ApplicationCore.Models.ContentTypes;
using DeskAlerts.ApplicationCore.Utilities;
using DeskAlerts.ContentDeliveringService.Interfaces;
using DeskAlerts.ContentDeliveringService.Models;
using System.Collections.Generic;
using System.Linq;

namespace DeskAlerts.ContentDeliveringService.Implementations
{
    public class AlertContentDeliveringService : ContentDeliveringService<AlertContentType>
    {
        private readonly IAlertRepository _repository;

        public AlertContentDeliveringService(
            ICache cache,
            IAlertRepository alertRepository,
            IAlertRepository repository,
            ITimeZoneRepository timeZoneRepository
            ) : base(cache, alertRepository, timeZoneRepository)
        {
            _repository = repository;
        }

        protected override string GetValueFromDb(ContentDeliveringKey key, string ip, string computerName)
        {
            var dateTime = DateTimeUtils.GetDateTimeByTimeZone(key.TimeZone);

            var ids = _repository
                .GetActualAlertIds(key.UserId, key.DeskBarId, dateTime, ip, computerName)
                .ToArray();

            return ids.Any()
                ? string.Join(",", ids)
                : string.Empty;
        }

        protected override IEnumerable<long> GetRecipientsUsersIds(long contentId)
        {
            var userIds = _repository.GetRecipientsUsersIds(contentId);
            return userIds;
        }
    }
}
