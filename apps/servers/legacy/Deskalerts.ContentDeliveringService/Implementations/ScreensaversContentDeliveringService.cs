﻿using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.ApplicationCore.Models.ContentTypes;
using DeskAlerts.ApplicationCore.Utilities;
using DeskAlerts.ContentDeliveringService.Interfaces;
using DeskAlerts.ContentDeliveringService.Models;
using System.Collections.Generic;
using System.Linq;

namespace DeskAlerts.ContentDeliveringService.Implementations
{
    public class ScreensaversContentDeliveringService : ContentDeliveringService<ScreensaverContentType>
    {
        private readonly IScreenSaverRepository _repository;

        public ScreensaversContentDeliveringService(
            ICache cache,
            IAlertRepository alertRepository,
            IScreenSaverRepository repository,
            ITimeZoneRepository timeZoneRepository
            ) : base(cache, alertRepository, timeZoneRepository)
        {
            _repository = repository;
        }

        protected override string GetValueFromDb(ContentDeliveringKey key, string ip, string computerName)
        {
            var ids = _repository
                .GetActualScreenSaversIds(key.UserId, key.DeskBarId, DateTimeUtils.GetDateTimeByTimeZone(key.TimeZone), ip, computerName)
                .ToArray();

            return ids.Any()
                ? string.Join(",", ids)
                : string.Empty;
        }

        protected override IEnumerable<long> GetRecipientsUsersIds(long contentId)
        {
            var usersIds = _repository.GetRecipientsUsersIds(contentId);
            return usersIds;
        }
    }
}