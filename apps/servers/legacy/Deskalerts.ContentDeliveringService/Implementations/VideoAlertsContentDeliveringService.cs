﻿using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.ApplicationCore.Models.ContentTypes;
using DeskAlerts.ApplicationCore.Utilities;
using DeskAlerts.ContentDeliveringService.Interfaces;
using DeskAlerts.ContentDeliveringService.Models;
using System.Collections.Generic;
using System.Linq;

namespace DeskAlerts.ContentDeliveringService.Implementations
{
    public class VideoAlertsContentDeliveringService : ContentDeliveringService<VideoAlertContentType>
    {
        private readonly IVideoAlertRepository _repository;

        public VideoAlertsContentDeliveringService(
            ICache cache,
            IAlertRepository alertRepository,
            IVideoAlertRepository repository,
            ITimeZoneRepository timeZoneRepository
            ) : base(cache, alertRepository, timeZoneRepository)
        {
            _repository = repository;
        }

        protected override string GetValueFromDb(ContentDeliveringKey key, string ip, string computerName)
        {
            var dateTime = DateTimeUtils.GetDateTimeByTimeZone(key.TimeZone);

            var ids = _repository
                .GetActualVideoAlertsIds(key.UserId, key.DeskBarId, dateTime)
                .ToArray();

            return string.Join(",", ids);
        }

        protected override IEnumerable<long> GetRecipientsUsersIds(long contentId)
        {
            var userIds = _repository.GetRecipientsUsersIds(contentId);
            return userIds;
        }
    }
}
