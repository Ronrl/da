﻿using DeskAlerts.ApplicationCore.Enums;
using DeskAlerts.ApplicationCore.Extensions.Enums;
using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.ApplicationCore.Utilities;
using DeskAlerts.ContentDeliveringService.Interfaces;
using DeskAlerts.ContentDeliveringService.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DeskAlerts.ContentDeliveringService.Implementations
{
    public abstract class ContentDeliveringService<TContent> : IContentDeliveringService<TContent> where TContent : IContentType
    {
        private readonly ICache _cache;
        private readonly IAlertRepository _alertRepository;
        private readonly ITimeZoneRepository _timeZoneRepository;

        private static object _lockObject = new object();

        protected ContentDeliveringService(ICache cache, IAlertRepository alertRepository, ITimeZoneRepository timeZoneRepository)
        {
            _cache = cache;
            _alertRepository = alertRepository;
            _timeZoneRepository = timeZoneRepository;
        }

        protected abstract string GetValueFromDb(ContentDeliveringKey key, string ip, string computerName);

        protected abstract IEnumerable<long> GetRecipientsUsersIds(long contentId);

        public IEnumerable<int> GetAllTimeZones()
        {
            return _timeZoneRepository.GetAllTimeZones();
        }

        public bool DeliveryToBroadcast()
        {
            return _cache.DeleteAll();
        }

        public bool DeliveryToUser(long userId)
        {
            Validator.ValidateId(userId);

            return _cache.Delete(userId);
        }

        public bool DeliveryToUsers(IEnumerable<long> usersIds)
        {
            if (usersIds == null)
            {
                throw new ArgumentException();
            }

            var ids = usersIds.ToArray();
            foreach (var id in ids)
            {
                Validator.ValidateId(id);
            }

            return _cache.Delete(ids);
        }

        public bool DeliveryToRecipientsOfContent(long contentId)
        {
            Validator.ValidateId(contentId);

            var contentEntity = _alertRepository.GetById(contentId);
            var contentReceivingType = contentEntity.Type2;

            if (contentReceivingType == ContentReceivingType.BroadCast.GetStringValue())
            {
                return DeliveryToBroadcast();
            }

            var userRecipientsIds = GetRecipientsUsersIds(contentId);
            return DeliveryToUsers(userRecipientsIds);
        }

        public bool DeliveryToRecipientsOfContent(long contentId, int timeZone)
        {
            Validator.ValidateId(contentId);

            var actualTimeZones = GetAllTimeZones();
            if (!actualTimeZones.Contains(timeZone))
            {
                return true;
            }

            var contentEntity = _alertRepository.GetById(contentId);
            var contentReceivingType = contentEntity.Type2;

            if (contentReceivingType == ContentReceivingType.BroadCast.GetStringValue())
            {
                return DeliveryToBroadcast();
            }

            var userRecipientsIds = GetRecipientsUsersIds(contentId);
            return _cache.Delete(userRecipientsIds, timeZone);
        }

        public string Get(ContentDeliveringKey key, string ip, string computerName)
        {
            if (key == null)
            {
                throw new ArgumentException();
            }

            var cacheValue = _cache.Get(key);
            if (cacheValue != null)
            {
                return cacheValue;
            }

            var dbValue = string.Empty;

            lock (_lockObject)
            {
                dbValue = GetValueFromDb(key, ip, computerName);
            }

            var isAdded = _cache.Add(key, dbValue);

            if (!isAdded)
            {
                throw new Exception("Failed to add the value to the cache");
            }

            return dbValue;
        }
    }
}