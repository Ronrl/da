﻿using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.ApplicationCore.Models.ContentTypes;
using DeskAlerts.ApplicationCore.Utilities;
using DeskAlerts.ContentDeliveringService.Interfaces;
using DeskAlerts.ContentDeliveringService.Models;
using System.Collections.Generic;
using System.Linq;

namespace DeskAlerts.ContentDeliveringService.Implementations
{
    public class SurveysContentDeliveringService : ContentDeliveringService<SurveyContentType>
    {
        private readonly ISurveyRepository _repository;

        public SurveysContentDeliveringService(
            ICache cache,
            IAlertRepository alertRepository,
            ISurveyRepository repository,
            ITimeZoneRepository timeZoneRepository
            ) : base(cache, alertRepository, timeZoneRepository)
        {
            _repository = repository;
        }

        protected override string GetValueFromDb(ContentDeliveringKey key, string ip, string computerName)
        {
            var dateTime = DateTimeUtils.GetDateTimeByTimeZone(key.TimeZone);

            var ids = _repository
                .GetActualSurveysIds(key.UserId, key.DeskBarId, dateTime, ip, computerName)
                .ToArray();

            return string.Join(",", ids);
        }

        protected override IEnumerable<long> GetRecipientsUsersIds(long contentId)
        {
            var usersIds = _repository.GetRecipientsUsersIds(contentId);
            return usersIds;
        }
    }
}