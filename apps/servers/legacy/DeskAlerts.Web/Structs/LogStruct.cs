﻿namespace DeskAlertsDotNet.Structs
{
    using System;
    using System.ComponentModel;

    /// <summary>
    /// Structures for logs text.
    /// </summary>
    public struct LogsText
    {
        /// <summary>
        /// Log text enum.
        /// </summary>
        public enum LogTextList
        {
            DefaultLogMessage,
            DataBaseExecuteError,
            ConfigParametrError,
            UserSignUp,
            UserLogOut,
            UserSaveSettings,
            UserChangeSettings,
            
            ConfTokenAuthEnable,
            ConfTokenMaxCountForServerEnable,
            ConfTokenMaxCountForUserEnable,
            ConfTokenExpirationTimeEnable,
            ConfApiSecret,
        }

        /// <summary>
        /// Get text string for Log enum <see cref="LogTextList"/>.
        /// </summary>
        /// <param name="logTextList">Type of log <see cref="LogTextList"/>.</param>
        /// <returns>Message text for the log.</returns>
        public static string GetLogText(LogTextList logTextList)
        {
            if (!Enum.IsDefined(typeof(LogTextList), logTextList))
            {
                throw new InvalidEnumArgumentException(nameof(logTextList), (int)logTextList, typeof(LogTextList));
            }

            string result;
            switch (logTextList)
            {
                case LogTextList.DefaultLogMessage:
                    result = "Unhandled error.";
                    break;

                case LogTextList.DataBaseExecuteError:
                    result = "An error occurred while executing the database query.";
                    break;

                case LogTextList.ConfigParametrError:
                    result = "The configuration parameter could not be obtained.";
                    break;

                case LogTextList.UserSignUp:
                    result = " is signup in DeskAlert server.";
                    break;

                case LogTextList.UserLogOut:
                    result = " is logout in DeskAlert server.";
                    break;

                case LogTextList.UserSaveSettings:
                    result = " is save settings in DeskAlert server in ";
                    break;

                case LogTextList.UserChangeSettings:
                    result = " is change settings in DeskAlert server in ";
                    break;

                case LogTextList.ConfTokenAuthEnable:
                    result = " enable token authorization ";
                    break;

                case LogTextList.ConfTokenMaxCountForServerEnable:
                    result = " maximum sessions on server: ";
                    break;

                case LogTextList.ConfTokenMaxCountForUserEnable:
                    result = " maximum sessions for publisher: ";
                    break;

                case LogTextList.ConfTokenExpirationTimeEnable:
                    result = " session expiration time (minutes): ";
                    break;
                case LogTextList.ConfApiSecret:
                    result = " API secret ";
                    break;

                default:
                    result = "Unhandled error.";
                    break;
            }

            return result;
        }
    }
}