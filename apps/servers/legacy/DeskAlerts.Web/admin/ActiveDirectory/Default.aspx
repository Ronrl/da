﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="DeskAlerts.Server.admin.ActiveDirectory.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../css/style9.css" rel="stylesheet" type="text/css" />
    <link href="../css/jquery/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../jscripts/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="../jscripts/json2.js"></script>
    <script type="text/javascript" src="../jscripts/jquery/jquery-ui.min.js"></script>
    <script>
        $(function () {
            $(".button").button()
        })
    </script>
</head>
<body>
    <table class="main_table" cellspacing="0" cellpadding="0">
        <tr class="main_table_title" style="height: 31px;">
            <td>
                <img src="../images/menu_icons/synchronizations.png" class="header_icon" />
                <a href="#" class="header_title" style='position: absolute; top: 15px'>AD Synchronizing</a>
            </td>
        </tr>
        <tr class="row">
            <td>
                <form id="form1" runat="server">
                    <asp:ScriptManager ID="ScriptManager" runat="server"></asp:ScriptManager>
                    <div class="row">
                        <label for="synchronizationName" class="label">Name</label><asp:TextBox runat="server" ID="synchronizationName"></asp:TextBox>
                    </div>
                    <div class="row">
                        <asp:Label runat="server" Text='You should enter full name of domain with dots (for example: your.domain.net).
Also you should provide login/password of user who is a member of "Domain Users" group in your Active Directory.'></asp:Label>
                    </div>
                    <div class="row">
                        <label for="host" class="label">Host:</label>
                        <asp:TextBox runat="server" ID="host" Text="192.168.0.222"></asp:TextBox>
                        <asp:Label runat="server" Text='e.g. "your.domain.net"'></asp:Label>
                    </div>
                    <div class="row">
                        <label for="port" class="label">Port</label>
                        <asp:TextBox runat="server" ID="port" Text="389"></asp:TextBox>
                        <asp:Label runat="server" Text="the TCP/IP Protocol port number"></asp:Label>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1"
                            ControlToValidate="port" runat="server"
                            ErrorMessage="Only Numbers allowed"
                            ValidationExpression="\d+">
                        </asp:RegularExpressionValidator>
                    </div>
                    <div class="row">
                        <label for="username" class="label">Login</label>
                        <asp:TextBox runat="server" ID="username" Text="Administrator"></asp:TextBox><br />
                    </div>
                    <div class="row">
                        <label for="password" class="label">Password</label>
                        <asp:TextBox TextMode="Password" runat="server" ID="password" Text="nD2mrc!@"></asp:TextBox><br />
                    </div>
                    <hr />

                    <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="row">
                                <asp:RadioButtonList ID="OuRadioButtonList" runat="server" OnSelectedIndexChanged="OuRadioButton_SelectedIndexChanged" AutoPostBack="true">
                                    <asp:ListItem Selected="True" Value="all" Text="Synchronize all Organizational Units (users and groups will be selected from whole Domain tree)"></asp:ListItem>
                                    <asp:ListItem Value="selected" Text="Synchronize selected Organizational Units (only users and groups from selected OU's will be synchronized)"></asp:ListItem>
                                    <asp:ListItem Value="disabled" Text="Don't synchronize Organizational Units (users and groups will be synchronized from the domain without OU's)"></asp:ListItem>
                                </asp:RadioButtonList>
                                <asp:TreeView ID="tree" OnTreeNodeCheckChanged="tree_TreeNodeCheckChanged" ShowCheckBoxes="All" runat="server" ></asp:TreeView>
                            </div>
                            <hr />
                            <div class="row">
                                <asp:RadioButtonList ID="groupRadioButtonList" runat="server" OnSelectedIndexChanged="GroupRadioButtonList_SelectedIndexChanged" AutoPostBack="true">
                                    <asp:ListItem Selected="True" Value="all" Text="Synchronize all groups"></asp:ListItem>
                                    <asp:ListItem Value="selected" Text="Synchronize only selected groups and users from them"></asp:ListItem>
                                </asp:RadioButtonList>
                                <asp:Panel ID="GroupPanel" Visible="false" runat="server">
                                    <asp:Panel ID="SearchGroupPanel" Visible="false" runat="server">
                                        Groups:<br />
                                        <asp:TextBox ID="GroupSearch" runat="server"></asp:TextBox>
                                        <asp:Button ID="GroupSearchButton" Text="Search" OnClick="GroupSearchButton_Click" CssClass="ui-button ui-widget ui-corner-all ui-state-default" runat="server" />
                                        <br />
                                        <asp:CheckBoxList ID="SearchGroupList" runat="server"></asp:CheckBoxList>
                                        <asp:Button ID="GroupSearchButtonAdd" Text="Add" OnClick="GroupSearchButtonAdd_Click" CssClass="ui-button ui-widget ui-corner-all ui-state-default" runat="server" />
                                    </asp:Panel>
                                    <asp:Panel ID="SelectedGroupPanel" Visible="false" runat="server">
                                        Groups for synchronizing:<br />
                                        <asp:CheckBoxList ID="GroupList" runat="server"></asp:CheckBoxList>
                                        <asp:Button ID="GroupDeleteButton" Text="Delete" OnClick="GroupDeleteButton_Click" CssClass="ui-button ui-widget ui-corner-all ui-state-default" runat="server" />
                                    </asp:Panel>
                                </asp:Panel>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <hr />
                    <div class="row">
                        <asp:CheckBox runat="server" Text="Remove objects that were removed from AD" Checked="true" /><br />
                    </div>
                    <div class="row">
                        <asp:CheckBox runat="server" Text="Import users with disabled accounts" /><br />
                    </div>
                    <div class="row">
                        <asp:CheckBox runat="server" Text="Import Computers" /><br />
                    </div>
                    <div class="row">
                        <asp:CheckBox runat="server" Text="Enable auto synchronization" /><br />
                    </div>
                    Please note that the synchronizing process may take some time (up to severalhours) depending on the number of groups selected and number of users in domains.
                    <hr />
                    <div class="row">
                        <asp:HyperLink CssClass="button" Text="Cancel" runat="server" NavigateUrl="../synchronizations.aspx"></asp:HyperLink>
                        <asp:Button CssClass="ui-button ui-widget ui-corner-all ui-state-default" Text="Save" runat="server" UseSubmitBehavior="true" />
                    </div>
                </form>
            </td>
        </tr>
    </table>
</body>
</html>
