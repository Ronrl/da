﻿namespace DeskAlerts.Server.admin.ActiveDirectory
{
    using DeskAlerts.ApplicationCore.Dtos.AD;
    using DeskAlerts.Service.AD;
    using DeskAlerts.Service.AD.DataContracts;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI.WebControls;

    public partial class Default : System.Web.UI.Page
    {
        ADService ADService { get; set; }
        List<OUDto> OUDtos { get; set; }

        private const string ouIcon = @"..\images\ad_icons\ou.png";
        private const string domainIcon = @"..\images\ad_icons\domain.png";
        private const string organizationIcon = @"..\images\ad_icons\organization.png";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                var cr = new LdapCredentials()
                {
                    Host = host.Text,
                    UserName = username.Text,
                    Password = password.Text,
                    Port = int.Parse(port.Text),
                    Timeout = new TimeSpan(0, 0, 10),
                };
                ADService = new ADService(cr);
            }
        }


        private void DrawOUs()
        {
            var ous = ADService.GetOrganizationUnits();
            if (ous.Count > 0)
            {
                tree.Nodes.Clear();
                var node = GetOrganization(ous);
                tree.Nodes.Add(node);
            }
        }

        private TreeNode GetOrganization(List<OrganizationUnit> ous)
        {
            var organizationNode = new TreeNode()
            {
                ImageUrl = organizationIcon,
                Text = "Organization",
                SelectAction = TreeNodeSelectAction.None,
                Expanded = true
            };
            organizationNode.ChildNodes.Add(GetTree(ous));
            return organizationNode;
        }

        private TreeNode GetTree(List<OrganizationUnit> ous)
        {
            var ouDtos = new List<OUDto>();
            foreach (var ou in ous)
            {
                var ouPath = ou.Path.Substring(ou.Path.LastIndexOf('/') + 1);
                var index = ouPath.IndexOf("DC=");
                ouPath = ReadOU(ouPath, index);
                var ouDto = new OUDto()
                {
                    Path = ouPath,
                    NestingLevel = ouPath.Split('/').Length
                };
                ouDtos.Add(ouDto);
            }
            OUDtos = ouDtos.OrderBy(x => x.NestingLevel).ToList();
            var rootNodeName = OUDtos.First().Path.Split('/').First();
            var rootNode = GetNode(rootNodeName);
            rootNode.ImageUrl = domainIcon;
            rootNode.SelectAction = TreeNodeSelectAction.None;
            rootNode.Expanded = true;
            return rootNode;
        }

        private TreeNode GetNode(string parentNodeName)
        {
            var node = new TreeNode()
            {
                Text = parentNodeName.Split('/').Last()
            };
            var nodeNames = OUDtos
                .Where(x => x.Path.StartsWith(parentNodeName))
                .Select(x => x.Path.Substring(parentNodeName.Length));
            foreach (var nodeName in nodeNames)
            {
                var nodes = nodeName.Split('/');
                if (nodes.Length == 2)
                {
                    var childNode = GetNode(parentNodeName + nodeName);
                    childNode.ImageUrl = ouIcon;
                    childNode.SelectAction = TreeNodeSelectAction.None;
                    childNode.Expanded = false;
                    node.ChildNodes.Add(childNode);
                }
            }

            return node;
        }

        private string ReadOU(string source, int index)
        {
            var domain = string.Join(".", source.Substring(index).Replace("DC=", string.Empty).Split(','));
            var ous = string.Join("/", source.Substring(0, index - 1).Replace("OU=", string.Empty).Split(',').Reverse());
            return domain + "/" + ous;
        }

        protected override void OnPreRenderComplete(EventArgs e)
        {
            base.OnPreRenderComplete(e);
            if (IsPostBack)
            {
                ADService.Dispose();
            }
        }

        public void OuRadioButton_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender is RadioButtonList radioButtonList)
            {
                var isSelected = radioButtonList.SelectedValue == "selected";
                tree.Visible = isSelected;
                if (isSelected)
                {
                    try
                    {
                        if (ADService.CheckConnection())
                        {
                            DrawOUs();
                        }
                    }
                    catch (Exception ex)
                    {
                        ScriptManager.AsyncPostBackErrorMessage = ex.Message;
                    }
                }
            }
        }

        protected void GroupRadioButtonList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender is RadioButtonList radioButtonList)
            {
                var isSelected = radioButtonList.SelectedValue == "selected";
                SearchGroupPanel.Visible = GroupPanel.Visible =
                    isSelected;
                if (!isSelected)
                {
                    SearchGroupList.Items.Clear();
                    GroupSearch.Text = string.Empty;
                }
            }
        }

        protected void GroupSearchButton_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(GroupSearch.Text))
            {
                if (ADService.CheckConnection())
                {
                    DrawGroups(GroupSearch.Text);
                }
            }
        }

        private void DrawGroups(string groupName)
        {
            var groups = ADService.GetGroupsByName(groupName);
            if (groups.Count > 0)
            {
                SearchGroupList.Items.Clear();
                var listItems = GetListItems(groups);
                foreach (var listItem in listItems)
                {
                    SearchGroupList.Items.Add(listItem);
                }
            }
        }

        private List<ListItem> GetListItems(List<Group> groups)
        {
            var listItems = new List<ListItem>();
            var prefix = "CN=";
            foreach (var group in groups)
            {
                var name = group.Path.Substring(group.Path.IndexOf(prefix) + prefix.Length);
                name = name.Substring(0, name.IndexOf(','));
                var listItem = new ListItem(name, group.Path);
                listItems.Add(listItem);
            }
            return listItems;
        }

        protected void GroupSearchButtonAdd_Click(object sender, EventArgs e)
        {
            if (SearchGroupList.Items.Count > 0)
            {
                foreach (ListItem item in SearchGroupList.Items)
                {
                    if (item.Selected)
                    {
                        item.Selected = false;
                        GroupList.Items.Add(item);
                    }
                    
                }
                SelectedGroupPanel.Visible = true;
            }
        }

        protected void GroupDeleteButton_Click(object sender, EventArgs e)
        {
            var selectedValues = GroupList.Items.Cast<ListItem>()
                .Where(li => li.Selected)
                .ToList();
            if (selectedValues.Count > 0)
            {
                foreach (ListItem item in selectedValues)
                {
                    GroupList.Items.Remove(item);
                }
            }
        }

        protected void tree_TreeNodeCheckChanged(object sender, TreeNodeEventArgs e)
        {

        }
    }
}