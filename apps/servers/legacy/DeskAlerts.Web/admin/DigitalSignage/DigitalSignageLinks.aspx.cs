﻿using DeskAlerts.Server.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Web.Services;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using DeskAlerts.ApplicationCore.Dtos.Settings;
using DeskAlerts.Server.Utils.Helpers;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Parameters;
using DeskAlertsDotNet.Pages;
using NLog;
using Resources;
using DeskAlertsDotNet.Utils.Factories;
using Newtonsoft.Json;
using DataRow = DeskAlertsDotNet.DataBase.DataRow;
using DataSet = DeskAlertsDotNet.DataBase.DataSet;

namespace DeskAlerts.Server.admin.DigitalSignage
{
    public partial class DigitalSignageLinks : DeskAlertsBaseListPage
    {

        private const int RefreshFactorSeconds = 0;
        private const int RefreshFactorMinutes = 1;
        private const int RefreshFactorHours = 2;
        protected override HtmlInputText SearchControl => searchTermBox;
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        protected override void OnLoad(EventArgs e)
        {
            try
            {
                base.OnLoad(e);
                Table = contentTable;
                searchButton.Text = searchButton.Text.Replace("{Search}", resources.LNG_SEARCH);

                nameCell.Text = GetSortLink(resources.LNG_NAME, "digsign_links.name", Request["sortBy"]);
                linkCell.Text = GetSortLink(resources.LNG_LINK, "digsign_links.link", Request["sortBy"]);
                creationDateCell.Text = GetSortLink(resources.LNG_CREATION_DATE, "creation_date", Request["sortBy"]);

                if (!IsPostBack)
                {
                    searchTermBox.Value = Request["searchTermBox"];
                }

                digitalSignageTrial.Visible = Settings.IsTrial;
                parametersCell.Text = resources.LNG_DIGSIGN_MSG_DISP_TIME;
                actionsCell.Text = resources.LNG_ACTIONS;

                if (!IsPostBack)
                {
                    mdtFactor.Items.Add(new ListItem(resources.LNG_SECONDS, "0"));
                    mdtFactor.Items.Add(new ListItem(resources.LNG_MINUTES, "1"));
                    mdtFactor.Items.Add(new ListItem(resources.LNG_HOURS, "2"));
                    editMdtFactor.Items.Add(new ListItem(resources.LNG_SECONDS, "0"));
                    editMdtFactor.Items.Add(new ListItem(resources.LNG_MINUTES, "1"));
                    editMdtFactor.Items.Add(new ListItem(resources.LNG_HOURS, "2"));
                }

                int cnt = this.Content.Count < (Offset + Limit) 
                    ? this.Content.Count 
                    : Offset + Limit;

                for (int i = Offset; i < cnt; i++)
                {
                    DataRow dbRow = this.Content.GetRow(i);
                    TableRow tableRow = new TableRow();

                    CheckBox chb = GetCheckBoxForDelete(dbRow);

                    TableCell checkTableRow = new TableCell();

                    checkTableRow.Controls.Add(chb);

                    tableRow.Cells.Add(checkTableRow);

                    const string sqlQuery = "SELECT id FROM users WHERE name = @name AND ( role='U' OR role IS NULL ) AND client_version = 'web client'";
                    DataSet userDataSet = dbMgr.GetDataByQuery(sqlQuery, SqlParameterFactory.Create(DbType.String, dbRow.GetString("name"), "@name"));

                    if (userDataSet.IsEmpty)
                    {
                        continue;
                    }

                    int userId = userDataSet.GetInt(0, "id");

                    var nameLink = $"<a href='../PopupSent.aspx?user_id={userId}'>{dbRow.GetString("name")}</a>";

                    TableCell nameContentCell = new TableCell { Text = nameLink };
                    tableRow.Cells.Add(nameContentCell);

                    var linkHtml = $"<a target='_blank' href='{dbRow.GetString("link")}'>{dbRow.GetString("link")}</a>";

                    TableCell linkContentCell = new TableCell { Text = linkHtml };
                    tableRow.Cells.Add(linkContentCell);

                    var creationDate = DateTimeConverter.ToUiDateTime(dbRow.GetDateTime("creation_date"));

                    tableRow.Cells.Add(new TableCell
                    {
                        Text = creationDate
                    });

                    int slideTimeValue = dbRow.GetInt("slide_time_value");
                    int slideTimeFactor = dbRow.GetInt("slide_time_factor");

                    string[] factorNames = { "seconds", "minutes", "hours" };

                    string factorName = factorNames[slideTimeFactor];

                    if (slideTimeValue == 1)
                    {
                        factorName = factorName.Substring(0, factorName.Length - 1);
                    }
                    var refreshRateText = $"{slideTimeValue} {factorName}";

                    TableCell parametersContentCell = new TableCell { Text = refreshRateText };
                    tableRow.Cells.Add(parametersContentCell);

                    LinkButton editButton = GetActionButton(dbRow.GetString("id"), 
                        "../images/action_icons/edit.png",
                        "LNG_EDIT",
                        $"editInstance({dbRow.GetString("id")}, '{dbRow.GetString("name")}', {slideTimeValue}, {slideTimeFactor})",
                        0);

                    TableCell actionRow = new TableCell();
                    actionRow.HorizontalAlign = HorizontalAlign.Center;
                    actionRow.Controls.Add(editButton);

                    tableRow.Cells.Add(actionRow);
                    contentTable.Rows.Add(tableRow);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                throw;
            }
        }

        [WebMethod]
        public static bool IsExists(string name)
        {
            try
            {
                const string sqlQuery = "SELECT COUNT(1) FROM digsign_links WHERE name = @name";

                int count;
                using (var dbMgr = new DBManager())
                {
                    count = dbMgr.GetScalarQuery<int>(sqlQuery, SqlParameterFactory.Create(DbType.String, name, "@name"));
                }

                return count > 0;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                throw;
            }
        }

        [WebMethod]
        public static string Change(string name, int rateValue, int rateFactor, bool update)
        {
            try
            {
                using (var dbMgr = new DBManager())
                {
                    string res = "";
                    int[] multipier = { 1, 60, 1440 };
                    var resultDict = DeployInstance(name, rateValue * multipier[rateFactor], update);

                    Dictionary<string, object> digSignDictionary = new Dictionary<string, object>();

                    digSignDictionary.Add("slide_time_value", rateValue);
                    digSignDictionary.Add("slide_time_factor", rateFactor);

                    if (!update)
                    {
                        digSignDictionary.Add("name", name);
                        digSignDictionary.Add("link", resultDict["link"]);
                        digSignDictionary.Add("creation_date", DateTimeConverter.ToDbDateTime(DateTime.Now));
                        dbMgr.Insert("digsign_links", digSignDictionary);
                    }
                    else
                    {
                        Dictionary<string, object> conditionDictionary = new Dictionary<string, object> {
                            { "name", name}
                        };
                        res = dbMgr.Update("digsign_links", digSignDictionary, conditionDictionary);
                    }

                    Dictionary<string, object> userDictionary = new Dictionary<string, object>();
                    userDictionary.Add("name", name);
                    userDictionary.Add("deskbar_id", resultDict["deskbar"]);
                    userDictionary.Add("reg_date", DateTimeConverter.ToDbDateTime(DateTime.Now));
                    userDictionary.Add("pass", "f49c0321a68ac5311234aa469ceb15bc");
                    userDictionary.Add("client_version", "web client");
                    userDictionary.Add("role", "U");
                    userDictionary.Add("group_id", 0);
                    userDictionary.Add("domain_id", 1);

                    if (!update)
                    {
                        dbMgr.Insert("users", userDictionary);
                    }

                    return res;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                throw;
            }
        }

        private static Dictionary<string, string> DeployInstance(string deviceName, int slideTime, bool update = false)
        {
            try
            {
                var destDir = Config.Data.GetString("alertsFolder") + "\\ds\\" +
                                 deviceName.Replace("*", "")
                                     .Replace(":", "")
                                     .Replace("|", "")
                                     .Replace("?", "")
                                     .Replace("<", "")
                                     .Replace(">", "")
                                     .Replace("\"", "")
                                     .Replace("'", "");

                if (!update || !Directory.Exists(destDir))
                {
                    var templateDir = Config.Data.GetString("alertsFolder") + @"\admin\DigitalSignage\templates\digsign";
                    DeskAlertsUtils.DirectoryCopy(templateDir, destDir, true);
                }

                var deskBarGuid = Guid.NewGuid();
                var deviceSettings = new DigitalSignageDeviceSettingsDto
                {
                    DeviceName = deviceName,
                    DeskbarId = deskBarGuid.ToString(),
                    RefreshTime = slideTime
                };
                File.WriteAllText($"{destDir}\\config.json", JsonConvert.SerializeObject(deviceSettings));

                var link = $"{Config.Data.GetString("alertsDir")}/ds/{deviceName}";
                var result = new Dictionary<string, string>
                {
                    { "deskbar", deskBarGuid.ToString()},
                    { "link", link}
                };

                return result;
            }
            catch (IOException ex)
            {
                Logger.Error(ex, $"Problem with IO: {ex.Message}");
                throw;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                throw;
            }
        }

        protected override string CustomQuery
        {
            get
            {
                string query = "";

                query = "SELECT id, name, link, creation_date, slide_time_value, slide_time_factor FROM digsign_links";

                if (SearchTerm.Length > 0)
                {
                    query += " WHERE name LIKE '%" + SearchTerm + "%'";
                }

                if (!String.IsNullOrEmpty(Request["sortBy"]))
                {
                    query += " ORDER BY " + Request["sortBy"];
                }

                return query;
            }
        }

        protected override string CustomCountQuery
        {
            get
            {
                var query = string.Empty;
                query = "SELECT count(1) FROM digsign_links";
                if (SearchTerm.Length > 0)
                    query += " WHERE name LIKE '%" + SearchTerm + "%'";
                return query;
            }
        }

        #region DataProperties
        protected override string DataTable
        {
            get
            {

                //return your table name from this property
                return "digsign_links";
            }
        }

        protected override void OnDeleteRows(string[] ids)
        {

            dbMgr.ExecuteQuery(
                string.Format(
                    "DELETE u FROM users as u INNER JOIN digsign_links as d ON d.name = u.name WHERE d.id IN ({0}) AND u.client_version = 'web client'", string.Join(",", ids)));

        }

        protected override string[] Collumns
        {
            get
            {

                //return array of collumn`s names of table from data table
                return new[] { "name", "link", "creation_date", "slide_time_value", "slide_time_factor" };
            }
        }

        protected override string DefaultOrderCollumn
        {
            //return collumn for default order
            get
            {
                return "id";

            }
        }

        /*
         * 
         * If you want to add condition for data selection override property ConditionSqlString 
         * Also you can implement your custom query by overriding properties CustomQuery and CustomCountQuery
         *  
         */
        #endregion

        #region Interface properties

        protected override IButtonControl[] DeleteButtons
        {
            get
            {
                return new IButtonControl[] { upDelete, bottomDelete };
            }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl NoRowsDiv
        {
            get { return NoRows; }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl TableDiv
        {
            get { return mainTableDiv; }
        }

        protected override HtmlGenericControl TopPaging
        {
            get { return topPaging; }
        }

        protected override HtmlGenericControl BottomPaging
        {
            get { return bottomPaging; }
        }

        protected override string ContentName
        {
            get
            {
                //return content name here
                return "Devices";
            }
        }

        #endregion
    }
}