﻿namespace DeskAlerts.Server.admin.DigitalSignage
{
    using System;
    using System.Collections.Generic;
    using System.Web;
    using System.Net;
    using System.IO;

    public partial class DeleteDSignFolders : System.Web.UI.Page
    {


       protected void Page_PreInit(object sender, EventArgs e)
       {

           
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            string[] folders = Request["folders"].Split(',');

            foreach (string folder in folders)
            {
                if (folder.Length <= 0)
                    continue;

                string fullFolderPath = Server.MapPath("ds\\"+folder).Replace("\\admin", "");

                Response.Write(fullFolderPath);
                DirectoryInfo dirInfo = new DirectoryInfo(fullFolderPath);

                if (dirInfo.Exists)
                {
                    dirInfo.Delete(true);
                }

            }

           
        }
        private static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
        {
            // Get the subdirectories for the specified directory.
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);
            DirectoryInfo[] dirs = dir.GetDirectories();
            
            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }

            // If the destination directory doesn't exist, create it. 
            if (!Directory.Exists(destDirName))
            {
                Directory.CreateDirectory(destDirName);
            }

            // Get the files in the directory and copy them to the new location.
            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                string temppath = Path.Combine(destDirName, file.Name);
                file.CopyTo(temppath, true);
            }

            // If copying subdirectories, copy them and their contents to new location. 
            if (copySubDirs)
            {
                foreach (DirectoryInfo subdir in dirs)
                {
                    string temppath = Path.Combine(destDirName, subdir.Name);
                    DirectoryCopy(subdir.FullName, temppath, copySubDirs);
                }
            }
        }
        public Dictionary<string, string> GetAspVariables(params string[] keys)
        {
            string queryString = String.Join(":", keys);

            string url = HttpContext.Current.Request.Url.AbsoluteUri;
            string path = url.Substring(0, url.IndexOf("DigSignLinks"));
            string url_bridge = path + "/dotnetbridge.asp?var=" + queryString;
            WebRequest request = WebRequest.Create(url_bridge);
            WebResponse response = request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
           // Response.Write(reader.ReadToEnd());
            string[] variablesStrings = reader.ReadToEnd().Split('&');
            

            Dictionary<string, string> result = new Dictionary<string, string>();
            foreach (string keyVal in variablesStrings)
            {

                string[] keyValArray = keyVal.Split(':');

                if (keyValArray.Length != 2)
                    continue;

                string key = keyValArray[0];
                string value = keyValArray[1];

                result.Add(key, value);
            }

            return result;

            
        }
        public string GetAspVariable(string variableName)
        {
            string url = HttpContext.Current.Request.Url.AbsoluteUri;
            string path = url.Substring(0, url.IndexOf("DigSignLinks"));
            string url_bridge = path + "/dotnetbridge.asp?var="+variableName;
            WebRequest request = WebRequest.Create(url_bridge);
            WebResponse response = request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            string variable = reader.ReadToEnd();
            return variable;
        }

   




    }
}
