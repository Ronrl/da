﻿namespace DeskAlerts.Server.admin.DigitalSignage
{
    using System;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using DeskAlertsDotNet.Parameters;

    using Resources;

    public partial class LifetimeDropdown : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
         //   if (Page.IsPostBack) return;

           lifetimeDropDown = new HtmlSelect();
            lifetimeDropDown.Items.Add(new ListItem(resources.LNG_MINUTES, "0"));
            lifetimeDropDown.Items.Add(new ListItem(resources.LNG_HOURS, "1"));
            lifetimeDropDown.Items.Add(new ListItem(resources.LNG_DAYS, "2"));
        }
    }
}