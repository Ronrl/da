﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DigitalSignageLinks.aspx.cs" Inherits="DeskAlerts.Server.admin.DigitalSignage.DigitalSignageLinks" %>

<%@ Import Namespace="DeskAlertsDotNet.Parameters" %>
<%@ Import Namespace="Resources" %>
<%@ Register Src="LifetimeDropdown.ascx" TagName="ReccurencePanel"
    TagPrefix="da" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>DeskAlerts</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <link href="../css/style9.css" rel="stylesheet" type="text/css" />
    <link href="../css/jquery/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="../css/jquery/jquery_timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css" />

    <style>
        .linkDialog {
            margin-top: 200px;
        }
    </style>

    <script type="text/javascript" src="../functions.js"></script>
    <script type="text/javascript" src="../jscripts/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="../jscripts/jquery/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../jscripts/date.js"></script>
    <script type="text/javascript" src="../jscripts/jquery/jquery_timepicker/jquery-ui-timepicker-addon.js"></script>
    <script type="text/javascript" src="../jscripts/jquery/datepicker_fix.js"></script>
    <script type="text/javascript">

        function openDialog(mode) {
            var dialogId = "";
            var textBoxId = "";

            switch (mode) {
                case 1:
                    {
                        dialogId = "#duplicateDialog";
                        textBoxId = "#dupName";
                        break;
                    }
                case 2: //add
                    {
                        dialogId = "#linkDialog";
                        textBoxId = "#linkValue";
                        $("#schedule").prop("checked", false);
                        $("#start_date_and_time").prop('disabled', true);
                        $
                        break;
                    }
            }

            $(textBoxId).focus(function () {
                $(textBoxId).select().mouseup(function (e) {
                    e.preventDefault();
                    $(this).unbind("mouseup");
                });
            });

            $(dialogId).dialog('open');
        }

        function addLink(edit) {
            var name, timeVal, timeFactor;

            if (!edit) {
                name = $("#linkValue").val();
                timeVal = $("#mdt_value").val();
                timeFactor = $("#mdtFactor").val();

                var regTamplate = "([A-Za-z0-9-_А-Яа-я]+)";
                var found = name.match(regTamplate);
                var error = "You can use only letters, digits or - and _ in digitals names";

                if (!name.length) {
                    alert("Digital link name is empty!");
                    return false;
                }

                if (found == null) {
                    alert(error);
                    return false;
                } else if (found[0] != name) {
                    alert(error);
                    return false;
                }


            } else {
                name = $("#edit_linkValue").val();
                timeVal = $("#edit_mdt_value").val();
                timeFactor = $("#editMdtFactor").val();
            }

            $.ajax({
                type: "POST",
                async: false,
                url: "DigitalSignageLinks.aspx/IsExists",
                data: "{name: '" + name + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response.d && !edit) {
                        alert("Sorry, this link is already exists.");
                        return;
                    }

                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "DigitalSignageLinks.aspx/Change",
                        data: '{name: "' + name + '", rateValue: ' + timeVal + ', rateFactor: ' + timeFactor + ', update: ' + edit + '}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            location.reload();
                        },
                        failure: function (response) {
                        }
                    });
                },
                failure: function (response) {
                }
            });
        }


        function editInstance(id, name, timeVal, timefactor) {
            $("#edit_id").val(id);
            $("#edit_linkValue").val(name);
            $("#edit_mdt_value").val(timeVal);
            $("#editMdtFactor").val(timefactor);
            $("#updateDialog").dialog('open');
        }

        $(document)
            .ready(function () {
                $(".delete_button")
                    .button({

                    });

                $("#selectAll")
                    .click(function () {
                        var checked = $(this).prop('checked');
                        $(".content_object").prop('checked', checked);
                    });



                $(".add_alert_button")
                    .button({
                        icons: {
                            primary: "ui-icon-plusthick"
                        }
                    });

                $("#linkDialog")
                    .dialog({
                        position: { my: "center top", at: "center top", of: window },
                        dialogClass: "linkDialog",
                        autoOpen: false,
                        height: 200,
                        width: 360,
                        modal: true,
                        resizable: false
                    });

                $("#updateDialog")
                    .dialog({
                        autoOpen: false,
                        height: 140,
                        width: 360,
                        modal: true,
                        resizable: false
                    });


            });

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
            <tr>
                <td>
                    <table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
                        <tr>
                            <td width="100%" height="31" class="main_table_title">
                                <img src="../images/menu_icons/ic_link.png" alt="" width="20" height="20" border="0" style="padding-left: 7px">
                                <a id="headerTitle" class="header_title" style="position: absolute; top: 22px"><%=resources.LNG_DIGSIGN_LINK_TITLE%></a>
                            </td>
                        </tr>
                        <tr>
                            <td class="main_table_body" height="100%">
                                <br />
                                <br />

                                <span style="padding-left: 10px;" class="work_header"><span id="dsDescription"><% =resources.LNG_DIGSIGN_DESCRIPTION%></span> </span>
                                <table style="padding-left: 10px;" width="100%" border="0">
                                    <tbody>
                                        <tr valign="middle">
                                            <td width="240">
                                                <% =resources.LNG_SEARCH_USERS%>
                                                <input type="text" id="searchTermBox" runat="server" name="uname" value="" /></td>
                                            <td width="1%">
                                                <br />

                                                <asp:LinkButton runat="server" class="search_button ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" ID="searchButton" Style="margin-right: 0px" role="button" aria-disabled="false" Text="<span class='ui-button-icon-primary ui-icon ui-icon-search'></span><span class='ui-button-text'>{Search}</span>" />
                                                <input type="hidden" name="search" value="1">
                                            </td>

                                            <td>
                                                <br />
                                                <a class="add_alert_button" href="javascript:openDialog(2)" style="text-align: center; float: right;  margin-right: 10px">
                                                    <% =resources.LNG_DIGSIGN_NEW_LINK%>
                                                </a>

                                                <a class="add_alert_button" href="../CreateAlert.aspx?webplugin=1"
                                                    style="text-align: right; float: right; margin-right: 10px">
                                                    <% =resources.LNG_DIGSIGN_SEND_CONTENT%>
                                                </a>

                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div id="digitalSignageTrial" runat="server" visible="false" class="trialNotifications"><%=Resources.resources.DIGITAL_SIGNAGE_TRIAL %></div>
                                <div style="margin-left: 10px" runat="server" id="mainTableDiv">

                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td>
                                                <div style="margin-left: 10px" id="topPaging" runat="server"></div>
                                            </td>
                                        </tr>
                                    </table>
                                    <br>
                                    <asp:LinkButton Style="margin-left: 10px" runat="server" ID="upDelete" class='delete_button' />
                                    <br>
                                    <br>
                                    <asp:Table Style="padding-left: 0px; margin-left: 10px; margin-right: 10px;" runat="server" ID="contentTable" Width="98.5%" Height="100%" CssClass="data_table" CellSpacing="0" CellPadding="3">
                                        <asp:TableRow CssClass="data_table_title">
                                            <asp:TableCell runat="server" ID="headerSelectAll" CssClass="table_title" Width="2%">
                                                <asp:CheckBox ID="selectAll" runat="server" />
                                            </asp:TableCell>
                                            <asp:TableCell runat="server" ID="nameCell" CssClass="table_title"></asp:TableCell>
                                            <asp:TableCell runat="server" ID="linkCell" CssClass="table_title"></asp:TableCell>
                                            <asp:TableCell runat="server" ID="creationDateCell" CssClass="table_title"></asp:TableCell>
                                            <asp:TableCell runat="server" ID="parametersCell" CssClass="table_title"></asp:TableCell>
                                            <asp:TableCell runat="server" ID="actionsCell" CssClass="table_title"></asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                    <br>
                                    <asp:LinkButton Style="margin-left: 10px" runat="server" ID="bottomDelete" class='delete_button' />
                                    <br>
                                    <br>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">

                                        <tr>
                                            <td>
                                                <div style="margin-left: 10px" id="bottomPaging" runat="server"></div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div runat="server" id="NoRows">
                                    <br />
                                    <br />
                                    <center><b><!-- Add here your message сcontent withou--></b></center>
                                </div>
                                <br>
                                <br>
                            </td>
                        </tr>
                    </table>
        </table>

        <div id="updateDialog">

            <input runat="server" id="edit_id" type="hidden" style="width: 277px" />
            <input runat="server" id="edit_linkValue" type="hidden" style="width: 277px" />
            <br />
            <label id="Label2" for="edit_mdt_value"><% = resources.LNG_DIGSIGN_MSG_DISP_TIME + ":"  %></label>
            <input type="number" id="edit_mdt_value" name="mdt_value" min="1" max="60" value="30">
            <asp:DropDownList runat="server" ID="editMdtFactor" name="factor" Style="height: 21px">
            </asp:DropDownList>
            <br />
            <br />
            <a href='#' class="add_alert_button" style="float: right;" onclick="javascript:addLink(true);">
                <% =resources.LNG_UPDATE%>                 </a>
        </div>

        <div runat="server" id="linkDialog" style="display: none;">
            <label id="campaignNameLabel" for="linkValue"><% =resources.LNG_LINK_NAME + ":"  %></label>
            <input runat="server" id="linkValue" type="text" style="width: 277px" /><br />
            <br />
            <label id="mdt" for="mdt_value"><% =resources.LNG_DIGSIGN_MSG_DISP_TIME + ":"  %></label>
            <input type="number" id="mdt_value" name="mdt_value" min="1" max="60" value="30">
            <asp:DropDownList runat="server" ID="mdtFactor" name="factor" Style="height: 21px">
            </asp:DropDownList>
            <br />
            <br />
            <a href='#' class="add_alert_button" style="float: right;" onclick="javascript:addLink(false);">
                <% =resources.LNG_CREATE%>                 </a>
        </div>
    </form>
</body>
</html>
