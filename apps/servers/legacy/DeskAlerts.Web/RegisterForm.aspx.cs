﻿using DeskAlerts.Server.Utils;
using System;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using DeskAlerts.ApplicationCore.Dtos;
using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.ApplicationCore.Repositories;
using DeskAlerts.ApplicationCore.Utilities;
using DeskAlertsDotNet.Pages;
using DeskAlertsDotNet.Parameters;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Resources;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.Server.Utils.Services;
using NLog;

namespace DeskAlerts.Server
{
    public partial class RegisterForm : Page
    {
        private const string MobilePhoneValidationRegexp = "^[0-9*#+]+$";
        private const string EmailValidationRegexp = @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$";

        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        private IDomainRepository _domainRepository;
        private IUserRepository _userRepository;

        protected override void OnLoad(EventArgs e)
        {
            IConfigurationManager configurationManager = new WebConfigConfigurationManager(Config.Configuration);

            _userRepository = new PpUserRepository(configurationManager);
            _domainRepository = new PpDomainRepository(configurationManager);
            new UserInstanceService();

            base.OnLoad(e);
            OnLoad();
        }

        public void OnLoad()
        {
            SetupPage();

            var canLoginWithAdAccount = Config.Data.HasModule(Modules.AD) && Settings.Content["ConfEnableRegAndADEDMode"] == "1"
                                        || !Config.Data.HasModule(Modules.AD);
            var canRegister = Config.Data.HasModule(Modules.AD) && Settings.Content["ConfEnableRegAndADEDMode"] == "1"
                                           && Settings.Content["ConfForceSelfRegWithAD"] == "1" || !Config.Data.HasModule(Modules.AD);

            var isSyncFree = Request.GetParam("syncFree", string.Empty) == "1";

            var domainName = Request.GetParam("domain_name", string.Empty);

            if (string.IsNullOrEmpty(domainName))
            {
                domainName = Request.GetParam("domain", string.Empty);
            }
            
            var userInfo = new UserInfoDto
            {
                Username = Request.GetParam("uname", string.Empty),
                Password = Request.GetParam("upass", string.Empty),
                Phone = Request.GetParam("uphone", string.Empty),
                Email = Request.GetParam("uemail", string.Empty),
                DomainName = domainName,
                DeskbarId = Request.GetParam("deskbar_id", Guid.NewGuid().ToString()),
                ClientId = Request.GetParam("client_id", string.Empty)
            };

            deskbar_id.Value = userInfo.DeskbarId;

            if (!canLoginWithAdAccount && !canRegister && !isSyncFree)
            {
                Response.Clear();
                var activeDirectoryError = File.ReadAllText(Server.MapPath("not_ad.html"));
                Response.Write(activeDirectoryError);
                Response.End();
                return;
            }

            if (domain.Items.Count == 0 && !canRegister && !isSyncFree)
            {
                Response.Clear();
                Response.Write(resources.LNG_NO_SYNC_DOMAINS_ERROR);
                Response.End();
                return;
            }

            // Don't use IsPostBack here because of redirect from register.asp 
            if (string.IsNullOrEmpty(userInfo.Username))
            {
                return;
            }

            if (userInfo.Password.Length <= 1 && !isSyncFree)
            {
                ShowError("Error! Password should be more than 1 symbol.");
                return;
            }

            if (Regex.IsMatch(userInfo.Username, "[<>\\/%:;=,+*?>{}\"]"))
            {
                ShowError("Error! You can not registrate the Username with symbol: <>\\/%:;=,+*?>{}\"");
                return;
            }

            if (string.IsNullOrEmpty(userInfo.DomainName) && userInfo.Username.Contains("\\"))
            {
                userInfo.DomainName = userInfo.Username.Split("\\".ToCharArray())[0];
                userInfo.Username = userInfo.Username.Split("\\".ToCharArray())[1];
            }

            var userValidationResult = ValidateUserInfo(userInfo, isSyncFree);

            if (userValidationResult.IsSuccess)
            {
                //Has synced domains or canRegister or syncFree
                var syncedDomain = _domainRepository.GetAll().SingleOrDefault(d => string.Equals(d.Name, userInfo.DomainName, StringComparison.OrdinalIgnoreCase));

                if (syncedDomain == default(Domain) && isSyncFree)
                {
                    var id = _domainRepository.Create(new Domain
                    {
                        Name = userInfo.DomainName
                    });

                    syncedDomain = _domainRepository.GetById(id);
                }

                //First try auth in Azure AD
                if (IsAzureCredentials(userInfo))
                {
                    var tenant = Settings.Content["ConfAzureAdTenant"];
                    var authorizationApplicationId = Settings.Content["ConfAzureAdAuthorizationAppId"];
                    const string resource = "https://graph.microsoft.com/";
                    var authority = $"https://login.microsoftonline.com/{tenant}";
                    var authenticationContext = new AuthenticationContext(authority);
                    var userNameCredentials = new UserPasswordCredential(userInfo.Username, userInfo.Password);
                    var accessToken = string.Empty;

                    try
                    {
                        accessToken = authenticationContext
                            .AcquireTokenAsync(resource, authorizationApplicationId, userNameCredentials).GetAwaiter()
                            .GetResult().AccessToken;
                    }
                    catch (Exception e)
                    {
                        _logger.Error(e);
                        ShowError(e.Message);
                    }
                    finally
                    {
                        if (!string.IsNullOrEmpty(accessToken))
                        {
                            WriteJsForWinClientToResponse(userInfo);
                            ShowSuccess("Thank you for signing up with existing account!");
                        }
                        else
                        {
                            ShowError("Error! Provided password is incorrect");
                        }
                    }
                }
                else if (syncedDomain != null && string.IsNullOrEmpty(syncedDomain.Name)) //canRegister or empty domain field
                {
                    var user = new User();

                    try
                    {
                        user = _userRepository.GetUserByDomainNameAndUserName(syncedDomain.Name, userInfo.Username);
                    }
                    catch (InvalidOperationException)
                    {
                        user = null;
                    }
                    catch (Exception ex)
                    {
                        _logger.Error(ex.Message);
                    }

                    if (user != null) //User found
                    {
                        if (string.Equals(HashingUtils.MD5(userInfo.Password).ToLower(), user.Password.ToLower()))
                        {
                            if (!string.IsNullOrEmpty(userInfo.Email))
                            {
                                user.Email = userInfo.Email;
                            }

                            if (!string.IsNullOrEmpty(userInfo.Phone))
                            {
                                user.Phone = userInfo.Phone;
                            }

                            // CR killall5: WTF?
                            _userRepository.Update(user);

                            WriteJsForWinClientToResponse(userInfo);
                            ShowSuccess("Thank you for signing up with existing account!");
                        }
                        else
                        {
                            ShowError("Error! Provided password is incorrect");
                        }
                    }
                    else //User not found
                    {
                        if (canRegister || isSyncFree)
                        {
                            _userRepository.Create(GetUser(userInfo, syncedDomain.Id));

                            WriteJsForWinClientToResponse(userInfo);
                            ShowSuccess("Registration was successfully completed.");
                        }
                        else
                        {
                            ShowError("Your server configured to allow only AD credentials. Please enter domain name.");
                        }
                    }
                }
                else if (syncedDomain != null && isSyncFree)
                {
                    var user = new User();
                    try
                    {
                        user = _userRepository.GetUserByDomainNameAndUserName(syncedDomain.Name, userInfo.Username);
                    }
                    catch (InvalidOperationException ex)
                    {
                        user = null;
                    }
                    catch (Exception ex)
                    {
                        _logger.Error(ex.Message);
                    }

                    if (user == default(User))
                    {
                        _userRepository.Create(GetUser(userInfo, syncedDomain.Id));                   
                    }

                    WriteJsForWinClientToResponse(userInfo);
                    ShowSuccess("Registration was successfully completed.");
                }
                else //Can't register, try login under AD credentials
                {
                    try
                    {
                        if (syncedDomain == null)
                        {
                            throw new NullReferenceException(nameof(syncedDomain) + " is null!");
                        }

                        using (var pc = new PrincipalContext(ContextType.Domain, syncedDomain.Name))
                        {
                            if (pc.ValidateCredentials(userInfo.Username, userInfo.Password))
                            {
                                WriteJsForWinClientToResponse(userInfo);
                                ShowSuccess("Thank you for signing up with existing account!");
                            }
                            else
                            {
                                ShowError("Error! Provided login/password pair is incorrect");
                            }   
                        }
                    }
                    catch (Exception ex)
                    {
                        ShowError("Error! Provided login/password pair is incorrect");
                    }
                }
            }
            else
            {
                ShowError(userValidationResult.ErrorMessage);
            }
        }

        private bool IsAzureCredentials(UserInfoDto userInfo)
        {
            var userFromAzureAd = _userRepository.GetChildUsersByDomainName("Azure Active Directory").SingleOrDefault(u => string.Equals(userInfo.Username, u.Username, StringComparison.CurrentCultureIgnoreCase));

            return userFromAzureAd != default(User);
        }

        public UserInfoValidationResult ValidateUserInfo(UserInfoDto userInfo, bool isSyncFree)
        {
            if (userInfo.Username.Length <= 1)
            {
                return new UserInfoValidationResult
                {
                    IsSuccess = false,
                    ErrorMessage = "Error! User name should be more than 1 symbol."
                };
            }

            if (userInfo.Password.Length <= 1 && !isSyncFree)
            {
                return new UserInfoValidationResult
                {
                    IsSuccess = false,
                    ErrorMessage = "Error! Password should be more than 1 symbol."
                };
            }

            if (Regex.IsMatch(userInfo.Username, "[<>\\/%:;=,+*?>{}\"]"))
            {
                return new UserInfoValidationResult
                {
                    IsSuccess = false,
                    ErrorMessage = "Error! You can not registrate the Username with symbol: <>\\/%:;=,+*?>{}\""
                };
            }

            if (!string.IsNullOrEmpty(userInfo.Phone) && !Regex.IsMatch(userInfo.Phone, MobilePhoneValidationRegexp))
            {
                return new UserInfoValidationResult
                {
                    IsSuccess = false,
                    ErrorMessage = "Error! Phone number is invalid."
                };
            }

            if (!string.IsNullOrEmpty(userInfo.Email) && !Regex.IsMatch(userInfo.Email, EmailValidationRegexp))
            {
                return new UserInfoValidationResult
                {
                    IsSuccess = false,
                    ErrorMessage = "Error! Email is invalid."
                };
            }

            return new UserInfoValidationResult
            {
                IsSuccess = true
            };
        }

        public User GetUser(UserInfoDto userInfo, long domainId)
        {
            return new User()
            {
                Username = userInfo.Username,
                Password = HashingUtils.MD5(userInfo.Password),
                Phone = userInfo.Phone,
                Email = userInfo.Email,
                RegistrationDateTime = DateTime.Now,
                LastRequestDateTime = DateTime.Now,
                Role = ApplicationCore.Entities.User.UserRole,
                DomainId = domainId
            };
        }

        private void ShowError(string errorMessage)
        {
            messageDiv.Visible = true;
            closeButton.Visible = false;
            SuccessMessage.Visible = true;
            SuccessMessage.Text = errorMessage;
        }

        private void ShowSuccess(string successMessage)
        {
            mainForm.Visible = false;
            messageDiv.Visible = true;
            closeButton.Visible = true;
            SuccessMessage.Visible = true;
            SuccessMessage.Text = successMessage;
        }

        private void SetupPage()
        {
            closeButton.Attributes["alt"] = resources.LNG_CLOSE;
            closeButton.Attributes["title"] = resources.LNG_CLOSE;
            closeButton.Attributes["src"] = Config.Data.GetString("alertsDir") + "/admin/images/langs/EN/close_button.gif";

            var isAdEnabledOnClient = Request.GetParam("ad_enabled", string.Empty) == "1";

            domainRow.Visible = Config.Data.HasModule(Modules.AD) && isAdEnabledOnClient;
            mobilePhoneRow.Visible = Config.Data.HasModule(Modules.SMS) || Config.Data.HasModule(Modules.TEXTTOCALL);
            emailRow.Visible = Config.Data.HasModule(Modules.EMAIL);

            if (!string.IsNullOrEmpty(Request["disableUnameChange"]))
            {
                disableUnameChange.Value = Request["disableUnameChange"];
            }

            if (domainRow.Visible)
            {
                var synchronizedDomains = _domainRepository.GetAll();

                domain.Items.Clear();

                var canReg = Settings.Content["ConfEnableRegAndADEDMode"] == "1" && Settings.Content["ConfForceSelfRegWithAD"] == "1";

                foreach (var synchronizedDomain in synchronizedDomains)
                {
                    if (canReg)
                    {
                        domain.Items.Add(new ListItem(synchronizedDomain.Name, synchronizedDomain.Name));
                    }
                    else if (!string.IsNullOrEmpty(synchronizedDomain.Name))
                    {
                        domain.Items.Add(new ListItem(synchronizedDomain.Name, synchronizedDomain.Name));
                    }
                }
            }
        }

        private void WriteJsForWinClientToResponse(UserInfoDto userInfo)
        {
            var scriptTemplate = @"<script type='text/javascript'>
                                        try {
							                if(window.external) {
								                window.external.setProperty('user_name',""%userName%"");
                                                window.external.setProperty('domain_name',""%domainName%"");
								                window.external.setProperty('user_id',""%userId%"");
                                                window.external.setProperty('md5_password',""%md5Password%"");
								                window.external.setProperty('email',""%email%"");
								                window.external.setProperty('phone',""%phone%"");
								                window.external.setProperty('modmail',""%emailModule%"");
								                window.external.setProperty('modsms',""%smsModule%"");
								                window.external.setProperty('isSilent',""%isSilent%"");
								                window.external.reloadAlert();
							                }
						                } catch(e) {}
                                     </script>";

            var finalScript = scriptTemplate
                .Replace("%userName%", userInfo.Username)
                .Replace("%domainName%", userInfo.DomainName)
                .Replace("%md5Password%", HashingUtils.MD5(userInfo.Password))
                .Replace("%email%", userInfo.Email)
                .Replace("%phone%", userInfo.Phone)
                .Replace("%emailModule%", Config.Data.HasModule(Modules.EMAIL) ? "1" : "-1")
                .Replace("%smsModule%", Config.Data.HasModule(Modules.SMS) ? "1" : "-1")
                .Replace("%isSilent%", string.IsNullOrEmpty(Request["disableUnameChange"]) ? "0" : "1");

            Response.Write(finalScript);
        }
    }

    public class UserInfoValidationResult
    {
        public bool IsSuccess { get; set; }

        public string ErrorMessage { get; set; }
    }
}