﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.ApplicationCore.Models.ContentTypes;
using DeskAlerts.ApplicationCore.Utilities;
using DeskAlerts.ContentDeliveringService.Implementations;
using DeskAlerts.ContentDeliveringService.Interfaces;
using DeskAlerts.Scheduler.ContentJobScheduling;
using DeskAlerts.Server.Utils.Interfaces;
using DeskAlerts.Server.Utils.Interfaces.Services;
using DeskAlerts.Server.Utils.Interfactes;
using DeskAlerts.Server.Utils.Services;
using DeskAlertsDotNet.Utils.Services;

namespace DeskAlerts.Server.IoC
{
    public class ServiceInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<ISkinService>().ImplementedBy<SkinService>().LifestyleScoped());
            container.Register(Component.For<ITokenService>().ImplementedBy<TokenService>().LifestyleScoped());
            container.Register(Component.For<IUserService>().ImplementedBy<UserService>().LifestyleScoped());

            container.Register(Component.For<IContentService>().ImplementedBy<ContentService>().LifestyleSingleton());

            container.Register(Component.For<ISettingsService>().ImplementedBy<SettingsService>().LifestyleScoped());

            container.Register(Component.For<ICache>().ImplementedBy<InMemoryCache>()
                .DependsOn(Dependency.OnValue("capacity", AppConfiguration.CacheCapacity)).LifestyleTransient());

            container.Register(Component.For<IContentDeliveringService<AlertContentType>>().ImplementedBy<AlertContentDeliveringService>().LifestyleSingleton());
            container.Register(Component.For<IContentDeliveringService<TickerContentType>>().ImplementedBy<TickersContentDeliveringService>().LifestyleSingleton());
            container.Register(Component.For<IContentDeliveringService<RsvpContentType>>().ImplementedBy<RsvpContentDeliveringService>().LifestyleSingleton());
            container.Register(Component.For<IContentDeliveringService<SurveyContentType>>().ImplementedBy<SurveysContentDeliveringService>().LifestyleSingleton());
            container.Register(Component.For<IContentDeliveringService<ScreensaverContentType>>().ImplementedBy<ScreensaversContentDeliveringService>().LifestyleSingleton());
            container.Register(Component.For<IContentDeliveringService<WallpaperContentType>>().ImplementedBy<WallpapersContentDeliveringService>().LifestyleSingleton());
            container.Register(Component.For<IContentDeliveringService<VideoAlertContentType>>().ImplementedBy<VideoAlertsContentDeliveringService>().LifestyleSingleton());
            container.Register(Component.For<IContentDeliveringService<TerminatedContentType>>().ImplementedBy<TerminatedContentDeliveringService>().LifestyleSingleton());
            container.Register(Component.For<ICacheInvalidationService>().ImplementedBy<CacheInvalidationService>().LifestyleSingleton());

            container.Register(Component.For<IContentJobScheduler>().ImplementedBy<QuartzContentJobScheduler>().LifestyleSingleton());

            container.Register(Component.For<IContentDeliveringService<LockscreenContentType>>()
                .ImplementedBy<LockscreensContentDeliveringService>().LifestyleSingleton());

            
            container.Register(Component.For<IAutoupdateService>().ImplementedBy<AutoupdateService>().LifestyleSingleton());

            container.Register(Component.For<IETagService>().ImplementedBy<ETagService>().LifestyleSingleton());

            container.Register(Component.For<IContentReceivedService>().ImplementedBy<ContentReceivedService>().LifestyleSingleton());

            container.Register(Component.For<IEmailService>().ImplementedBy<EmailService>().LifestyleScoped());

            container.Register(Component.For<IScheduledContentService>().ImplementedBy<ScheduledContentService>().LifestyleSingleton());
        }
    }
}