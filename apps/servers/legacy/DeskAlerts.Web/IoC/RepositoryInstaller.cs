﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.ApplicationCore.Repositories;

namespace DeskAlerts.Server.IoC
{
    public class RepositoryInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<IPushSentRepository>().ImplementedBy<PpPushSentRepository>().LifestyleScoped());
            container.Register(Component.For<IUserInstancesRepository>().ImplementedBy<PpUserInstancesRepository>().LifestyleScoped());
            container.Register(Component.For<IUserInstanceIpRepository>().ImplementedBy<PpUserInstancesIpRepository>().LifestyleScoped());
            container.Register(Component.For<IUserRepository>().ImplementedBy<PpUserRepository>().LifestyleScoped());
            container.Register(Component.For<IPolicyRepository>().ImplementedBy<PpPolicyRepository>().LifestyleSingleton());
            container.Register(Component.For<IGroupRepository>().ImplementedBy<PpGroupRepository>().LifestyleSingleton());
            container.Register(Component.For<IDomainRepository>().ImplementedBy<PpDomainRepository>().LifestyleSingleton());
            container.Register(Component.For<IOrganizationUnitRepository>().ImplementedBy<PpOrganizationUnitRepository>().LifestyleSingleton());
            container.Register(Component.For<IAlertReadRepository>().ImplementedBy<PpAlertReadRepository>().LifestyleScoped());
            container.Register(Component.For<IAlertsSentRepository>().ImplementedBy<PpAlertsSentRepository>().LifestyleScoped());
            container.Register(Component.For<IAlertSentStatRepository>().ImplementedBy<PpAlertSentStatRepository>().LifestyleScoped());
            container.Register(Component.For<IAlertReceivedRepository>().ImplementedBy<PpAlertReceivedRepository>().LifestyleScoped());
            container.Register(Component.For<IComputerRepository>().ImplementedBy<PpComputerRepository>().LifestyleSingleton());
            container.Register(Component.For<ISettingsRepository>().ImplementedBy<PpSettingsRepository>().LifestyleSingleton());
            container.Register(Component.For<IAlertRepository>().ImplementedBy<PpAlertRepository>().LifestyleTransient());
            container.Register(Component.For<ICampaignRepository>().ImplementedBy<PpCampaignRepository>().LifestyleSingleton());
            container.Register(Component.For<ISkinRepository>().ImplementedBy<PpSkinRepository>().LifestyleSingleton());
            container.Register(Component.For<IMultipleAlertRepository>().ImplementedBy<PpMultipleAlertRepository>().LifestyleSingleton());
            container.Register(Component.For<IWallPaperRepository>().ImplementedBy<PpWallPaperRepository>().LifestyleSingleton());
            container.Register(Component.For<ILockScreenRepository>().ImplementedBy<PpLockScreenRepository>().LifestyleTransient());
            container.Register(Component.For<IScreenSaverRepository>().ImplementedBy<PpScreenSaverRepository>().LifestyleSingleton());
            container.Register(Component.For<ITickerRepository>().ImplementedBy<PpTickerRepository>().LifestyleSingleton());
            container.Register(Component.For<IRsvpRepository>().ImplementedBy<PpRsvpRepository>().LifestyleSingleton());
            container.Register(Component.For<ISurveyRepository>().ImplementedBy<PpSurveyRepository>().LifestyleSingleton());
            container.Register(Component.For<IVideoAlertRepository>().ImplementedBy<PpVideoAlertRepository>().LifestyleSingleton());
            container.Register(Component.For<ITerminatedRepository>().ImplementedBy<PpTerminatedRepository>().LifestyleSingleton());
            container.Register(Component.For<ITimeZoneRepository>().ImplementedBy<PpTimeZoneRepository>().LifestyleSingleton());
            container.Register(Component.For<IRecurrenceRepository>().ImplementedBy<PpRecurrenceRepository>().LifestyleSingleton());
            container.Register(Component.For<IScheduledContentRepository>().ImplementedBy<PpScheduledContentRepository>().LifestyleSingleton());
        }
    }
}