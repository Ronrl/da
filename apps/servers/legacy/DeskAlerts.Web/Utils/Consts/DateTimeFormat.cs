﻿namespace DeskAlertsDotNet.Utils.Consts
{
    public class DateTimeFormat
    {
        public static string Client => "dd.MM.yyyy|HH:mm:ss";
        public static string MsSqlFormat => "yyyyMMdd HH:mm:ss";
        public static string JavaScript => "dd/MM/yyyy HH:mm:ss";
        public static string Iso8601 => "yyyy-MM-ddTHH:mm:sszzz";
    }
}