﻿using DeskAlerts.Server.Utils.Consts;
using DeskAlertsDotNet.Application;
using DeskAlertsDotNet.Parameters;
using NLog;
using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;

namespace DeskAlerts.Server.Utils.Timers
{
    public static class DeleteClientCrashLogs
    {
        private static readonly Timer timer = new Timer(ExecuteWork);
        private static readonly JobHost jobHost = new JobHost();

        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private static readonly int deleteClientCrashLogsPeriod = Config.Data.GetInt("clearClientDumps");
        /// <summary>
        /// Start of Delete Logs Timer
        /// </summary>
        public static void Start()
        {
            if (deleteClientCrashLogsPeriod == -1) { return; }
            timer.Change(TimeSpan.Zero, TimeSpan.FromDays(1));
        }

        private static void ExecuteWork(object sender)
        {
            jobHost.DoWork(DeleteCrashesFromFolder);
        }

        private static void DeleteCrashesFromFolder()
        {
            var deleteClientCrashDateTime = DateTime.Now.AddDays(-Convert.ToDouble(deleteClientCrashLogsPeriod));
            var clientCrashPath = AppDomain.CurrentDomain.BaseDirectory + FolderVariables.ClientCrashFolder.Replace("/", "").Replace("~", "");
            try
            {
                if (Directory.Exists(clientCrashPath))
                {
                    var clientLogsDirectories = Directory.GetFiles(clientCrashPath);
                    foreach (var path in clientLogsDirectories)
                    {
                        if (File.GetCreationTime(path) <= deleteClientCrashDateTime)
                        {
                            File.Delete(path);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }
    }
}