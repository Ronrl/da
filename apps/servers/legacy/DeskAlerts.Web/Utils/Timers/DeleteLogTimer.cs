﻿namespace DeskAlertsDotNet.Utils
{
    using System;
    using System.Threading;

    using DeskAlertsDotNet.Application;
    using DeskAlertsDotNet.DAL;
    using DeskAlertsDotNet.Pages;

    /// <summary>
    /// Timer for delete logs.
    /// </summary>
    public static class DeleteLogTimer
    {
        private static readonly Timer Timer = new Timer(OnTimerElapsed);
        private static readonly JobHost JobHost = new JobHost();

        /// <summary>
        /// Start log delete timer.
        /// </summary>
        public static void Start()
        {
            Timer.Change(TimeSpan.Zero, TimeSpan.FromDays(1));
        }

        /// <summary>
        /// Run log deleting at the end of the timer.
        /// </summary>
        private static void OnTimerElapsed(object sender)
        {
            JobHost.DoWork(DeleteOldLogEntries);
        }

        /// <summary>
        /// Deleting logs.
        /// </summary>
        private static void DeleteOldLogEntries()
        {
            LogsRepository logs = new LogsRepository();
            logs.DeleteOldLogsEntries(GetConfigLogDeleteDays());
        }

        /// <summary>
        /// Getting the number of days through which logs are deleted.
        /// </summary>
        /// <returns>Number of days.</returns>
        private static int GetConfigLogDeleteDays()
        {
            // Default value of delete log days.
            int daysToKeepLogs = 30;

            string s = Parameters.Settings.Content["ConfLogDeleteDays"];
            if (!string.IsNullOrEmpty(s))
            {
                if (!int.TryParse(s, out daysToKeepLogs))
                {
                    DeskAlertsBasePage.Logger.Info(Structs.LogsText.GetLogText(Structs.LogsText.LogTextList.ConfigParametrError));
                }
            }

            return daysToKeepLogs;
        }
    }
}