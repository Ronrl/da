﻿using DeskAlerts.ApplicationCore.Utilities;
using DeskAlertsDotNet.Utils.Tasks;
using NLog;
using System;
using System.Threading;

namespace DeskAlertsDotNet.Utils.Timers
{
    public static class ScheduleTimer
    {
        private static readonly Timer Timer = new Timer(OnTimerElapsed);

        private static object _lock = new object();

        public static void Start()
        {
            Timer.Change(TimeSpan.Zero, TimeSpan.FromMinutes(1));
        }

        private static void OnTimerElapsed(object sender)
        {
            if (!AppConfiguration.IsPrimaryServer)
            {
                return;
            }

            Logger logger = LogManager.GetCurrentClassLogger();
            var lockTaken = false;
            try
            {
                Monitor.TryEnter(_lock, TimeSpan.FromMilliseconds(100), ref lockTaken);
                if (lockTaken)
                {
                    using (var task = new ScheduleTask())
                    {
                        task.Start();
                    }

                    using (var task = new PushScheduleTask())
                    {
                        task.Start();
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            finally
            {
                if (lockTaken)
                {
                    Monitor.Exit(_lock);
                }
            }
        }
    }
}