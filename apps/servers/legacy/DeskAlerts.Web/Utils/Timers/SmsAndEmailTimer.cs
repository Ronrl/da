﻿using DeskAlerts.ApplicationCore.Utilities;
using DeskAlertsDotNet.Utils.Tasks;
using System;
using System.Threading;

namespace DeskAlertsDotNet.Utils.Timers
{
    public static class SmsAndEmailTimer
    {
        private static readonly Timer Timer = new Timer(OnTimerElapsed);

        public static void Start()
        {
            Timer.Change(TimeSpan.Zero, TimeSpan.FromSeconds(AppConfiguration.SmsAndEmailSendingTaskTimer));
        }

        private static void OnTimerElapsed(object sender)
        {
            if (!AppConfiguration.IsPrimaryServer)
            {
                return;
            }

            var task = new SmsAndEmailTask();
            task.Start();
        }
    }
}