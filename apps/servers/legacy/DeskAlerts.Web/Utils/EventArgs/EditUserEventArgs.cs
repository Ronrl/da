﻿namespace DeskAlertsDotNet.Utils.EventArgs
{
    using EventArgs = System.EventArgs;

    public class EditUserEventArgs : EventArgs
    {
        public EditUserEventArgs(string userId)
        {
            UserId = userId;
        }

        public string UserId { get; set; }
    }
}