﻿using System.Collections.Generic;
using System.Linq;
using Castle.MicroKernel.Lifestyle;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.ApplicationCore.Models.ContentTypes;
using DeskAlerts.ApplicationCore.Utilities;
using DeskAlerts.Scheduler.ContentJobScheduling;
using DeskAlertsDotNet.Application;

namespace DeskAlerts.Server.Utils.Tasks
{
    public class ActualScheduleContent
    {
        private readonly IContentJobScheduler _contentJobScheduler;
        private readonly IWallPaperRepository _wallPaperRepository;
        private readonly IScreenSaverRepository _screenSaverRepository;
        private readonly ILockScreenRepository _lockScreenRepository;
        private readonly IScheduledContentRepository _repeatableContentRepository;

        public ActualScheduleContent()
        {
            using (Global.Container.BeginScope())
            {
                _contentJobScheduler = Global.Container.Resolve<IContentJobScheduler>();
                _wallPaperRepository = Global.Container.Resolve<IWallPaperRepository>();
                _screenSaverRepository = Global.Container.Resolve<IScreenSaverRepository>();
                _lockScreenRepository = Global.Container.Resolve<ILockScreenRepository>();
                _repeatableContentRepository = Global.Container.Resolve<IScheduledContentRepository>();
            }
        }

        public void CreateJobsForScheduleContent()
        {
            var actualScheduleJobs = GetAllActualScheduleContent();
            foreach (var actualScheduleJob in actualScheduleJobs)
            {
                _contentJobScheduler.CreateJob(actualScheduleJob);
            }

            var actualRepeatableJobs = GetAllActualRepeatableContent();
            foreach (var job in actualRepeatableJobs)
            {
                _contentJobScheduler.CreateRepeatableJob(job);
            }
        }

        public IEnumerable<ContentJobDto> GetAllActualScheduleContent()
        {
            return GetScheduleScreensavers()
                .Concat(GetScheduleWallpapers())
                .Concat(GetScheduleLockscreens())
                .Concat(GetScheduledAlerts())
                .Concat(GetScheduledTickers())
                .Concat(GetScheduledRsvpAlerts())
                .Concat(GetScheduledSurveys())
                .Concat(GetScheduledVideoAlerts());
        }

        public IEnumerable<ContentJobDto> GetAllActualRepeatableContent()
        {
            return GetRepeatableAlerts()
                .Concat(GetRepeatableTickers())
                .Concat(GetRepeatableRsvpAlerts())
                .Concat(GetRepeatableVideoAlerts());
        }

        #region GetScheduledContent

        public IEnumerable<ContentJobDto> GetScheduleWallpapers()
        {
            var scheduleWallpapers = _wallPaperRepository.GetActualSchedulingWallpapers();
            
            var result = new List<ContentJobDto>();

            foreach (var scheduleWallpaper in scheduleWallpapers)
            {
                var startDateTime = scheduleWallpaper.FromDate ?? DateTimeUtils.MinSupportedDateTime;
                var contentId = scheduleWallpaper.Id;
                var contentType = scheduleWallpaper.Type;
                
                result.Add(new ContentJobDto(startDateTime, contentId, contentType));
            }

            return result;
        }

        public IEnumerable<ContentJobDto> GetScheduleLockscreens()
        {
            var scheduleLockscreens = _lockScreenRepository.GetActualSchedulingLockscreens();

            var result = new List<ContentJobDto>();

            foreach (var scheduleLockscreen in scheduleLockscreens)
            {
                var startDateTime = scheduleLockscreen.FromDate ?? DateTimeUtils.MinSupportedDateTime;
                var contentId = scheduleLockscreen.Id;
                var contentType = scheduleLockscreen.Type;

                result.Add(new ContentJobDto(startDateTime, contentId, contentType));
            }

            return result;
        }

        public IEnumerable<ContentJobDto> GetScheduleScreensavers()
        {
            var scheduleScreensavers = _screenSaverRepository.GetActualSchedulingScreensavers();

            var result = new List<ContentJobDto>();

            foreach (var scheduleScreensaver in scheduleScreensavers)
            {
                var startDateTime = scheduleScreensaver.FromDate ?? DateTimeUtils.MinSupportedDateTime;
                var contentId = scheduleScreensaver.Id;
                var contentType = scheduleScreensaver.Type;

                result.Add(new ContentJobDto(startDateTime, contentId, contentType));
            }

            return result;
        }

        public IEnumerable<ContentJobDto> GetScheduledAlerts()
        {
            var scheduledAlerts = _repeatableContentRepository.GetActualScheduledContent<AlertContentType>();
            var result = scheduledAlerts
                .Select(entity =>
                {
                    var startDateTime = entity.FromDate ?? DateTimeUtils.MinSupportedDateTime;
                    return new ContentJobDto(startDateTime, entity.Id, AlertType.SimpleAlert);
                });

            return result;
        }
        public IEnumerable<ContentJobDto> GetScheduledTickers()
        {
            var scheduledTickers = _repeatableContentRepository.GetActualScheduledContent<TickerContentType>();
            var result = scheduledTickers.Select(entity =>
            {
                var startDateTime = entity.FromDate ?? DateTimeUtils.MinSupportedDateTime;
                return new ContentJobDto(startDateTime, entity.Id, AlertType.Ticker);
            });

            return result;
        }
        public IEnumerable<ContentJobDto> GetScheduledRsvpAlerts()
        {
            var scheduledRsvpAlerts = _repeatableContentRepository.GetActualScheduledContent<RsvpContentType>();
            var result = scheduledRsvpAlerts.Select(entity =>
            {
                var startDateTime = entity.FromDate ?? DateTimeUtils.MinSupportedDateTime;
                return new ContentJobDto(startDateTime, entity.Id, AlertType.Rsvp);
            });

            return result;
        }
        public IEnumerable<ContentJobDto> GetScheduledSurveys()
        {
            var scheduledSurveys = _repeatableContentRepository.GetActualScheduledContent<SurveyContentType>();
            var result = scheduledSurveys
                .Select(entity =>
                {
                    var startDateTime = entity.FromDate ?? DateTimeUtils.MinSupportedDateTime;
                    return new ContentJobDto(startDateTime, entity.Id, AlertType.SimpleSurvey);
                });

            return result;
        }

        public IEnumerable<ContentJobDto> GetScheduledVideoAlerts()
        {
            var scheduledVideoAlerts = _repeatableContentRepository.GetActualScheduledContent<VideoAlertContentType>();
            var result = scheduledVideoAlerts.Select(entity =>
            {
                var startDateTime = entity.FromDate ?? DateTimeUtils.MinSupportedDateTime;
                return new ContentJobDto(startDateTime, entity.Id, AlertType.VideoAlert);
            });

            return result;
        }

        #endregion

        #region GetRepeatableContent

        public IEnumerable<ContentJobDto> GetRepeatableAlerts()
        {
            var scheduledAlerts = _repeatableContentRepository.GetActualRepeatableTemplates<AlertContentType>();
            var result = scheduledAlerts
                .Select(entity =>
                {
                    var startDateTime = entity.FromDate ?? DateTimeUtils.MinSupportedDateTime;
                    return new ContentJobDto(startDateTime, entity.Id, AlertType.SimpleAlert);
                });

            return result;
        }

        public IEnumerable<ContentJobDto> GetRepeatableTickers()
        {
            var scheduledTickers = _repeatableContentRepository.GetActualRepeatableTemplates<TickerContentType>();
            var result = scheduledTickers.Select(entity =>
                {
                    var startDateTime = entity.FromDate ?? DateTimeUtils.MinSupportedDateTime;
                    return new ContentJobDto(startDateTime, entity.Id, AlertType.Ticker);
                });

            return result;
        }

        public IEnumerable<ContentJobDto> GetRepeatableRsvpAlerts()
        {
            var scheduledRsvpAlerts = _repeatableContentRepository.GetActualRepeatableTemplates<RsvpContentType>();
            var result = scheduledRsvpAlerts.Select(entity =>
                {
                    var startDateTime = entity.FromDate ?? DateTimeUtils.MinSupportedDateTime;
                    return new ContentJobDto(startDateTime, entity.Id, AlertType.Rsvp);
                });

            return result;
        }

        public IEnumerable<ContentJobDto> GetRepeatableVideoAlerts()
        {
            var scheduledVideoAlerts = _repeatableContentRepository.GetActualRepeatableTemplates<VideoAlertContentType>();
            var result = scheduledVideoAlerts.Select(entity =>
                {
                    var startDateTime = entity.FromDate ?? DateTimeUtils.MinSupportedDateTime;
                    return new ContentJobDto(startDateTime, entity.Id, AlertType.VideoAlert);
                });

            return result;
        }

        #endregion
    }
}