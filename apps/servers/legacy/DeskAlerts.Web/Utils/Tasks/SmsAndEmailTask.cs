﻿using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.ApplicationCore.Utilities;
using DeskAlerts.Server.Utils.Interfaces.Services;
using DeskAlerts.Server.Utils.Interfactes;
using DeskAlertsDotNet.Application;
using DeskAlertsDotNet.Models;
using DeskAlertsDotNet.Utils.Helpers;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DeskAlertsDotNet.Utils.Tasks
{
    public class SmsAndEmailTask
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private IEmailService _emailService;
        private IContentReceivedService _contentReceivedService;
        private IUserService _userService;
        private IAlertRepository _alertRepository;

        private readonly EmailSender _emailSender;

        public SmsAndEmailTask()
        {
            _emailSender = new EmailSender();
        }

        public void Start()
        {
            try
            {
                if (AppConfiguration.IsNewContentDeliveringScheme)
                {
                    _emailService = Global.Container.Resolve<IEmailService>();
                    _contentReceivedService = Global.Container.Resolve<IContentReceivedService>();
                    _userService = Global.Container.Resolve<IUserService>();
                    _alertRepository = Global.Container.Resolve<IAlertRepository>();

                    var emails = _emailService.GetApprovedEmailAlerts();
                    _emailSender.SendEmail(emails);

                    var sms = new HashSet<UserAlertsDTO>();
                    var userAndAlertIdsWithSmsToSend = _contentReceivedService.GetUserAndAlertIdsWithSmsToSend();

                    foreach (var userAndAlert in userAndAlertIdsWithSmsToSend)
                    {
                        var userId = userAndAlert.UserId;
                        var user = _userService.GetUserById(userId);
                        var domainName = _userService.GetUserDomainNameByUserId(userId);
                        var userName = _userService.JoinUserNameAndDomainName(user.Username, domainName);

                        var alert = _alertRepository.GetById(userAndAlert.AlertId);

                        if (sms.Any(x => x.UserName.Equals(userName)))
                        {
                            var alerts = sms.First(x => x.UserName.Equals(userName)).Alerts.ToList();

                            alerts.Add(new Alert(alert));

                            sms.First(x => x.UserName.Equals(userName)).Alerts = alerts;
                        }
                        else
                        {
                            sms.Add(new UserAlertsDTO
                            {
                                UserName = userName,
                                Alerts = new HashSet<Alert>
                                {
                                    new Alert(alert)
                                }
                            });
                        }
                    }

                    SmsSender.SendSms(sms);
                }
                else
                {
                    if (Global.IsCacheManagerLoaded)
                    {
                        var cache = Global.CacheManager;
                        var scheduledSmsAlerts = cache.GetApprovedSmsAlerts();
                        SmsSender.SendSms(scheduledSmsAlerts);
                        var scheduledEmailAlerts = cache.GetApprovedEmailAlerts();
                        _emailSender.SendEmail(scheduledEmailAlerts);
                    }
                }
            }
            catch (Exception ex)
            {
                this._logger.Log(LogLevel.Error, ex);
            }
        }
    }
}