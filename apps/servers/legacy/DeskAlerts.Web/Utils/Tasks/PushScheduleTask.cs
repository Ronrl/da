﻿using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.ApplicationCore.Repositories;
using DeskAlerts.ApplicationCore.Utilities;
using DeskAlerts.Server.Utils.Interfactes;
using DeskAlerts.Server.Utils.Managers;
using DeskAlertsDotNet.Application;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Managers;
using DeskAlertsDotNet.Models;
using DeskAlertsDotNet.Parameters;
using DeskAlertsDotNet.Utils.Services;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DeskAlertsDotNet.Utils.Tasks
{


    public class PushScheduleTask : IDisposable
    {
        private readonly Logger logger = LogManager.GetCurrentClassLogger();

        private DBManager _manager;

        private IUserInstancesRepository _userInstancesRepository;
        private IAlertRepository _alertRepository;
        private IUserService _userService;
        private readonly IConfigurationManager _configurationManager;
        private IGroupRepository _groupRepository;
        private IUserRepository _userRepository;
        private IAlertReceivedRepository _alertReceivedRepository;
        private IPushSentRepository _pushSentRepository;
        private ICampaignRepository _campaignRepository;
        private CacheManager _cacheManager;

        public void Start()
        {
            _manager = new DBManager();
            IConfigurationManager _configurationManager = new WebConfigConfigurationManager(Config.Configuration);
            _userInstancesRepository = new PpUserInstancesRepository(_configurationManager);
            _userRepository = new PpUserRepository(_configurationManager);
            _groupRepository = new PpGroupRepository(_configurationManager);
            _userService = new UserService(_userRepository, _groupRepository);
            _cacheManager = Global.CacheManager;
            _pushSentRepository = new PpPushSentRepository(_configurationManager);
            _campaignRepository = new PpCampaignRepository(_configurationManager);
            _alertReceivedRepository = new PpAlertReceivedRepository(_configurationManager);
            _alertRepository = new PpAlertRepository(_configurationManager);
            
            if (Global.IsCacheManagerLoaded && !AppConfiguration.IsNewContentDeliveringScheme)
            {
                var scheduledPushAlerts = _cacheManager.GetScheduledPushAlerts();
                var approvedPushAlerts = _cacheManager.GetApprovedPushAlerts();
                SendPush(scheduledPushAlerts);
                SendPush(approvedPushAlerts);

            }
            else
            {
                List<UserAlertsDTO> resultedPushAlerts = new List<UserAlertsDTO>();
                var scheduledAlerts = _alertRepository
                                      .GetScheduledActiveAlerts()
                                      .Where(x => !_pushSentRepository.GetAlertIdForSentPushes().Contains(x.Id));
                var approvedAlerts = _alertRepository.GetApprovedActiveAlerts()
                                                     .Where(x => !_pushSentRepository.GetAlertIdForSentPushes().Contains(x.Id));
                var resultedAlerts = scheduledAlerts.Concat(approvedAlerts);

                foreach (var alertEntity in resultedAlerts)
                {
                    var alertModel = new Alert(alertEntity);

                    foreach (var userName in _alertReceivedRepository.GetUsernamesForPushesByAlertId(alertEntity.Id))
                    {
                        if (resultedPushAlerts != null && resultedPushAlerts.Any(x => x.UserName == userName))
                        {
                           if (resultedPushAlerts.First(x => x.UserName == userName).Alerts.Any(x => x != alertModel))
                            {
                                resultedPushAlerts.First(x => x.UserName == userName).Alerts.ToList().Add(alertModel);
                            }
                        }
                        else
                        {
                            resultedPushAlerts.Add(new UserAlertsDTO()
                            {
                                UserName = userName,
                                Alerts = new List<Alert>
                                {
                                    alertModel
                                }
                            });
                        }
                    }
                }

                SendPush(resultedPushAlerts);

            }

           
            PushNotificationsManager.Default.SendBrokenPushes();
        }

        private IEnumerable<long> GetUsersIdToPushSend()
        {
            var allUsersId = UserManager.Default.GetUsersId().ToList().Select(x => (long)x.GetInt("u.id "));
            return allUsersId;
        }

        private void SendBroadcastPush(UserAlertsDTO userAlerts)
        {
            var usersIdToSend = GetUsersIdToPushSend();
            foreach (var alert in userAlerts.Alerts)
            {
                if (CheckAlertStatusWithinCampaign(alert))
                {
                    PushNotificationsManager.Default.AddPushNotificationToDb(alert.Id, usersIdToSend);

                }
            }
        }

        private bool CheckAlertStatusWithinCampaign(Alert alert)
        {
            return alert.CampaignId < 0 || _campaignRepository.GetActiveCampaigns().Any(x => x.Id == alert.CampaignId);
        }

        private void SendPush(IEnumerable<UserAlertsDTO> scheduledPushAlerts)
        {
            foreach (var userAlerts in scheduledPushAlerts)
            {
                if (userAlerts.Alerts.Any())
                {
                    if (userAlerts.UserName == "broadcast")
                    {
                        SendBroadcastPush(userAlerts);
                    }
                    else
                    {
                        SendToUserPush(userAlerts);
                    }
                }
            }
        }

        private void SendToUserPush(UserAlertsDTO userAlerts)
        {
            if (userAlerts == null || string.IsNullOrEmpty(userAlerts.UserName) || !userAlerts.Alerts.Any())
            {
                throw new ArgumentNullException();
            }

            foreach (var alert in userAlerts.Alerts)
            {
                if (CheckAlertStatusWithinCampaign(alert))
                {
                    var userIdList = new List<long> { Convert.ToInt32(_userService.GetUserByName(userAlerts.UserName).Id) };
                    PushNotificationsManager.Default.AddPushNotificationToDb(alert.Id, userIdList);
                }
            }
        }

        public void Dispose()
        {
            _manager.Dispose();
        }
    }
}