﻿using DeskAlerts.ApplicationCore.Dtos;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Enums;
using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.ApplicationCore.Repositories;
using DeskAlerts.ApplicationCore.Utilities;
using DeskAlerts.Server.Utils.Helpers;
using DeskAlerts.Server.Utils.Interfactes;
using DeskAlerts.Server.Utils.Managers;
using DeskAlertsDotNet.Application;
using DeskAlertsDotNet.Models;
using DeskAlertsDotNet.Parameters;
using DeskAlertsDotNet.Utils.Consts;
using NLog;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using Domain = DeskAlerts.ApplicationCore.Entities.Domain;

namespace DeskAlerts.Server.Utils.Services
{
    public class UserInstanceService
    {
        private readonly IUserRepository _userRepository;
        private readonly IComputerRepository _computerRepository;
        private readonly IDomainRepository _domainRepository;
        private readonly IUserInstancesRepository _userInstancesRepository;       
        private readonly IUserInstanceIpRepository _userInstanceIpRepository;
        private readonly IUserInstanceComputerRepository _userInstanceComputerRepository;
        private readonly IContentService _contentService;

        private readonly object _userInstanceRepositoryLock = new object();

        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        private const string MobileFlag = "mobile";
       
        private static CacheManager CacheManager => Global.CacheManager;

        public UserInstanceService()
        {
            var configurationManager = new WebConfigConfigurationManager(Config.Configuration);

            var alertReceivedRepository = new PpAlertReceivedRepository(configurationManager);
            var alertReadRepository = new PpAlertReadRepository(configurationManager);
            var alertRepository = new PpAlertRepository(configurationManager);
            var multipleAlertsRepository = new PpMultipleAlertRepository(configurationManager);
            var tickerRepository = new PpTickerRepository(configurationManager);
            var rsvpRepository = new PpRsvpRepository(configurationManager, alertRepository);
            var surveyRepository = new PpSurveyRepository(configurationManager, alertRepository);
            var videoAlertRepository = new PpVideoAlertRepository(configurationManager, alertRepository);
            var wallPaperRepository = new PpWallPaperRepository(configurationManager);
            var screenSaverRepository = new PpScreenSaverRepository(configurationManager);
            var lockScreenRepository = new PpLockScreenRepository(configurationManager);

            _userRepository = new PpUserRepository(configurationManager);
            _computerRepository = new PpComputerRepository(configurationManager);
            _domainRepository = new PpDomainRepository(configurationManager);
            _userInstancesRepository = new PpUserInstancesRepository(configurationManager);
            _userInstanceIpRepository = new PpUserInstancesIpRepository(configurationManager);
            _userInstanceComputerRepository = new PpUserInstanceComputerRepository(configurationManager);
            var scheduledContentRepository = new PpScheduledContentRepository(configurationManager);

            _contentService = new ContentService(
                alertRepository,
                alertReceivedRepository, 
                alertReadRepository, 
                multipleAlertsRepository,
                tickerRepository,
                rsvpRepository,
                surveyRepository,
                videoAlertRepository,
                wallPaperRepository,
                screenSaverRepository,
                lockScreenRepository,
                scheduledContentRepository);
        }

        public void HandleNewUserInstance(UserInfoDto userInfo, string instanceIpString, string computerName, string computerDomainName)
        {
            if (string.IsNullOrEmpty(userInfo.Username))
            {
                _logger.Warn("Calling HandleNewUserInstance with empty username");
                return;
            }

            _logger.Info($"Trying to create instance for user  with username {userInfo.Username}, deskbarId {userInfo.DeskbarId} and domainName {userInfo.DomainName}");
            lock (_userInstanceRepositoryLock)
            {
                var userDeskbarId = Guid.Parse(userInfo.DeskbarId);
                var userInstance = _userInstancesRepository.GetInstance(userDeskbarId);
                var isNewInstance = false;

                //First element is redundant
                //request.Ip example: "ip,xxx.xxx.xxx.x,yyy.yyy.y.yy,zzz.zzz.zzz.z"
                var instanceIps = instanceIpString.Split(',').Skip(1);

                IEnumerable<long> contentIdsForUser;

                if (userInstance == null && computerName != MobileFlag)
                {
                    _logger.Info($"There is no any instance for user with username {userInfo.Username} and deskbarId {userDeskbarId}. Trying to create a new one.");
                    var newUserInstance = new UserInstances
                    {
                        UserId = _userRepository
                            .GetUserByDomainNameAndUserName(userInfo.DomainName, userInfo.Username).Id,
                        Clsid = userDeskbarId
                    };

                    _userInstancesRepository.Create(newUserInstance);
                    _logger.Debug($"The User Instance for user with name {userInfo.Username} and deskbarId {newUserInstance.Clsid} and userId {newUserInstance.UserId} has been successfully created.");

                    foreach (var ip in instanceIps)
                    {
                        _userInstanceIpRepository.Create(new UserInstanceIps
                        {
                            UserInstancesGuid = userDeskbarId,
                            UserInstanceIpField = ip
                        });

                        _logger.Debug($"The IP User Instance for user with name {userInfo.Username}, deskbarId {userDeskbarId}, userId {newUserInstance.UserId} and ip {ip} has been successfully created.");
                    }

                    if (!string.IsNullOrEmpty(computerDomainName))
                    {
                        _logger.Info($"Trying to create domain computer instance with computer name {computerDomainName} for user {userInfo.Username} with deskbarId {userDeskbarId}");
                        var computerDomainEntity = default(Domain);
                        try
                        {
                            computerDomainEntity = _domainRepository.GetByName(computerDomainName);
                        }
                        catch (Exception e)
                        {
                            _logger.Info($"Couldn't get domain for computer with name {computerDomainName}");
                        }

                        if (computerDomainEntity != default(Domain))
                        {
                            var computer = _computerRepository.GetByNameAndDomainId(computerName, computerDomainEntity.Id);

                            if (computer != null)
                            {
                                _userInstanceComputerRepository.Create(new UserInstanceComputer
                                {
                                    UserInstanceGuid = userDeskbarId,
                                    UserInstanceComputerId = computer.Id
                                });
                            }
                            else
                            {
                                _logger.Info($"Couldn't find computer with computer name {computerName} and computer domain name {computerDomainEntity.Name}");
                            }
                        }
                        _logger.Info($"Creation of domain computer instance with computer name {computerDomainName} for user {userInfo.Username} with deskbarId {userDeskbarId} has been made.");
                    }

                    userInstance = _userInstancesRepository.GetInstance(userDeskbarId);

                    if (AppConfiguration.IsNewContentDeliveringScheme)
                    {
                        contentIdsForUser = _contentService.GetContentForUser(DateTime.Now, userInstance.UserId);
                    }
                    else
                    {
                        contentIdsForUser = CacheManager
                            .GetAlertsForUserDistinctByInstance(userInfo.Username, userInfo.DomainName)
                            .Select(entity => (long) entity.Id);
                    }
                    
                    _logger.Debug($"The new instance has been successfully created for user  with username {userInfo.Username}, deskbarId {userInfo.DeskbarId} and domainName {userInfo.DomainName}");
                    isNewInstance = true;
                }
                else
                {
                    if (AppConfiguration.IsNewContentDeliveringScheme && userInstance != null)
                    {
                        contentIdsForUser = _contentService.GetContentForUserInstance(DateTime.Now, userInstance.UserId, userInstance.Clsid);
                    }
                    else
                    {
                        contentIdsForUser = CacheManager
                            .GetAlertsForInstance(userInfo.DeskbarId, userInfo.Username, userInfo.DomainName)
                            .Select(entity => (long)entity.Id);
                    }

                    _logger.Debug($"The new instance has been successfully created for user  with username {userInfo.Username}, deskbarId {userInfo.DeskbarId} and domainName {userInfo.DomainName}");

                }

                if (instanceIps != null)
                {
                    foreach (var ip in instanceIps)
                    {
                        contentIdsForUser = contentIdsForUser
                            .Concat(CacheManager
                                .GetAlertsForIp(IpReader.ConvertIpAddressToLong(IPAddress.Parse(ip)))
                                .Select(entity => (long)entity.Id)
                        );

                        _logger.Debug($"The content for ip {ip},  user with username {userInfo.Username}, deskbarId {userInfo.DeskbarId} and domainName {userInfo.DomainName} has been sent");
                    }
                }
                else
                {
                    throw new Exception("instanceIps is null");
                }

                if (isNewInstance)
                {
                    ProceedContentForInstance(contentIdsForUser, userInstance);

                    if (!AppConfiguration.IsNewContentDeliveringScheme)
                    {
                        var alertsForComputer = CacheManager.GetAlertsForComputer(computerName, computerDomainName);

                        foreach (var alertForComputer in alertsForComputer)
                        {
                            _contentService.AddContentRecipientConnectionToDb(userInstance.UserId, userInstance.Clsid, alertForComputer.Id, 0);
                        }
                    }
                    
                    _logger.Debug($"The content for new instance user {userInfo.Username}, deskbarId {userInfo.DeskbarId} and domainName {userInfo.DomainName} has been sent");
                }
                else if (IsMobileInstance(userInstance))
                {
                    ProceedContentForInstance(contentIdsForUser, userInstance);
                    _logger.Debug($"The content for mobile user {userInfo.Username}, deskbarId {userInfo.DeskbarId} and domainName {userInfo.DomainName} has been sent");
                }
            }
            _logger.Info($"The instance has been successfully handled for user  with username {userInfo.Username}, deskbarId {userInfo.DeskbarId} and domainName {userInfo.DomainName}");
        }

        private void ProceedContentForInstance(IEnumerable<long> contentIdsForUser, UserInstances userInstance)
        {
            foreach (var contentId in contentIdsForUser)
            {
                _contentService.AddContentRecipientConnectionToDb(userInstance.UserId, userInstance.Clsid, contentId, 0);

                if (!AppConfiguration.IsNewContentDeliveringScheme)
                {
                    CacheManager.Add(contentId, DateTime.Now.ToString(DateTimeFormat.MsSqlFormat, CultureInfo.InvariantCulture));
                }
            }
        }

        private static bool IsMobileInstance(UserInstances userInstance)
        {
            return (userInstance.Type == DeviceType.Android || userInstance.Type == DeviceType.IOs ||
                   userInstance.Type == DeviceType.WindowsPhone);
        }
    }
}