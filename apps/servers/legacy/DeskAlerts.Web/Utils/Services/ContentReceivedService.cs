﻿using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Enums;
using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.ApplicationCore.Repositories;
using DeskAlerts.ApplicationCore.Utilities;
using DeskAlerts.Server.Utils.Interfactes;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Models;
using DeskAlertsDotNet.Parameters;
using DeskAlertsDotNet.Utils.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using IpRangeGroup = DeskAlerts.Server.sever.STANDARD.Models.IpRangeGroup;

namespace DeskAlerts.Server.Utils.Services
{
    public class ContentReceivedService : IContentReceivedService
    {
        private readonly IUserService _userService;
        private readonly IUserInstancesRepository _userInstancesRepository;
        private readonly IIpRangeGroupsRepository _ipRangeGroupsRepository;
        private readonly IUserInstanceIpRepository _userInstanceIpRepository;
        private readonly IAlertReceivedRepository _alertReceivedRepository;
        private readonly IContentService _contentService;
        private readonly IAlertRepository _alertRepository;
        private readonly IComputerRepository _computerRepository;
        private readonly IGroupRepository _groupRepository;
        private readonly IOrganizationUnitRepository _organizationUnitRepository;

        public ContentReceivedService()
        {
            var configurationManager = new WebConfigConfigurationManager(Config.Configuration);

            IUserRepository userRepository = new PpUserRepository(configurationManager);
            IGroupRepository groupRepository = new PpGroupRepository(configurationManager);
            _userService = new UserService(userRepository, groupRepository);

            _userInstancesRepository = new PpUserInstancesRepository(configurationManager);
            _userInstanceIpRepository = new PpUserInstancesIpRepository(configurationManager);
            _ipRangeGroupsRepository = new PpIpRangeGroupsRepository(configurationManager);
            _alertReceivedRepository = new PpAlertReceivedRepository(configurationManager);
            _alertRepository = new PpAlertRepository(configurationManager);
            _computerRepository = new PpComputerRepository(configurationManager);
            _groupRepository = new PpGroupRepository(configurationManager);
            _organizationUnitRepository = new PpOrganizationUnitRepository(configurationManager);

            var alertReadRepository = new PpAlertReadRepository(configurationManager);
            var multipleAlertsRepository = new PpMultipleAlertRepository(configurationManager);
            var tickerRepository = new PpTickerRepository(configurationManager);
            var rsvpRepository = new PpRsvpRepository(configurationManager, _alertRepository);
            var surveyRepository = new PpSurveyRepository(configurationManager, _alertRepository);
            var videoAlertRepository = new PpVideoAlertRepository(configurationManager, _alertRepository);
            var wallPaperRepository = new PpWallPaperRepository(configurationManager);
            var screenSaverRepository = new PpScreenSaverRepository(configurationManager);
            var lockScreenRepository = new PpLockScreenRepository(configurationManager);
            var scheduledContentRepository = new PpScheduledContentRepository(configurationManager);

            _contentService = new ContentService(
                _alertRepository,
                _alertReceivedRepository,
                alertReadRepository,
                multipleAlertsRepository,
                tickerRepository,
                rsvpRepository,
                surveyRepository,
                videoAlertRepository,
                wallPaperRepository,
                screenSaverRepository,
                lockScreenRepository,
                scheduledContentRepository);
        }

        public string GetDeviceTypeScript(int alertId )
        {
            var device_type = _alertRepository.GetById(alertId).Device;//alertSet.GetInt(0, "device");
            var device_type_script = string.Empty;
            if (device_type == (int)Device.Mobile)
            {
                device_type_script = " and ui.device_type != 0";
            }
            else if (device_type == (int)Device.Desktop)
            {
                device_type_script = " and ui.device_type = 0";
            }
            return device_type_script;
        }
        public long Create(long alertId, long userId, Guid deskbarId)
        {
            try
            {
                var alertsReceivedEntity = _alertReceivedRepository.GetByAlertIdAndUserId(alertId, userId, deskbarId.ToString());
                if (alertsReceivedEntity != null && alertsReceivedEntity.Id > 0)
                {
                    return alertsReceivedEntity.Id;
                }
            }
            catch
            {
                // ignored
            }

            var alertReceived = new AlertReceived
            {
                AlertId = alertId,
                UserId = userId,
                DeskbarId = deskbarId.ToString(),
                Date = DateTime.Now,
                ComputerId = null,
                Vote = null,
                Occurrence = 0,
                IpAddress = null,
                Status = AlertReceiveStatus.NotReceived,
                TerminateStatus = (int)TerminateType.NotTerminated
            };
            var result = _alertReceivedRepository.Create(alertReceived);

            return result;
        }

        public void CreateRowsForUsers(long alertId, IEnumerable<long> userIds)
        {
            foreach (var userId in userIds)
            {
                var userInstances = _userInstancesRepository.GetInstances(userId);

                foreach (var userInstance in userInstances)
                {
                    Create(alertId, userId, userInstance.Clsid);
                }
            }
        }

        public void CreateRowsForUsersByUserNames(long alertId, IEnumerable<string> userNames)
        {
            var userIdsList = new List<long>();
            foreach (var userName in userNames)
            {
                userIdsList.Add(_userService.GetUserByName(userName).Id);
            }

            CreateRowsForUsers(alertId, userIdsList);
        }

        public void CreateRowsForUsersByComputerNames(long alertId, IEnumerable<string> computerNames)
        {
            var userIdsList = new List<long>();
            
            userIdsList.AddRange(_computerRepository.GetUserIdsByComputerNames(computerNames));

            CreateRowsForUsers(alertId, userIdsList);
        }

        public void CreateRowsForUsersByGroupNames(long alertId, IEnumerable<string> groupNames)
        {
            var userIdsList = new List<long>();

            userIdsList.AddRange(_groupRepository.GetUserIdsByGroupNames(groupNames));

            CreateRowsForUsers(alertId, userIdsList);
        }

        public void CreateRowsForUsersByOrganizationUnitNames(long alertId, IEnumerable<string> ouNames)
        {
            var userIdsList = new List<long>();

            userIdsList.AddRange(_organizationUnitRepository.GetUserIdsByOrganizationUnitNames(ouNames));

            CreateRowsForUsers(alertId, userIdsList);
        }

        //TODO: Transfer it to repositories
        public void CreateRowsInAlertsReceived(string sendType, int alertId, IEnumerable<long> recipientIds = null, IEnumerable<string> ipRangedIds = null)
        {
            switch (sendType)
            {
                case "broadcast":
                    _alertReceivedRepository.CreateForRecipientsOfBroadcastContent(alertId, GetDeviceTypeScript(alertId));
                    break;
                case "recipients":
                    if (recipientIds != null)
                    {
                        foreach (var recipientId in recipientIds)
                        {
                            var userInstanceses =
                                _userInstancesRepository.GetInstances(recipientId);

                            foreach (var userInstance in userInstanceses)
                            {
                                _contentService.AddContentRecipientConnectionToDb(userInstance.UserId, userInstance.Clsid, alertId, 0);
                            }
                        }
                    }
                    else
                    {
                        throw new Exception("Error while trying add data in CreateRowsInAlertsReceived method. SendType was recipients but alertsSent is null.");
                    }

                    break;
                case "iprange":
                    if (ipRangedIds != null)
                    {
                        var allUsersInstancesIps = _userInstanceIpRepository.GetAll();
                        foreach (var ipRangedId in ipRangedIds)
                        {
                            var ipRangeGroupEntities = _ipRangeGroupsRepository.GetByGroupId(int.Parse(ipRangedId));
                            var ipAddressesRanges = ipRangeGroupEntities
                                .Select(
                                    group => new IpRangeGroup
                                    {
                                        FromIp = group.FromIp,
                                        ToIp = group.ToIp,
                                        GroupId = group.GroupId
                                    }.GetIpAddressRange()
                                )
                                .ToArray();

                            if (!ipAddressesRanges.Any())
                            {
                                continue;
                            }

                            foreach (var usersInstanceIp in allUsersInstancesIps)
                            {
                                var ipAddress = IPAddress.Parse(usersInstanceIp.UserInstanceIpField);
                                if (ipAddressesRanges.All(range => !range.IsInRange(ipAddress)))
                                {
                                    continue;
                                }

                                var userInstance = _userInstancesRepository.GetInstance(usersInstanceIp.UserInstancesGuid);
                                if (userInstance != null)
                                {
                                    _contentService.AddContentRecipientConnectionToDb(userInstance.UserId, userInstance.Clsid, alertId, 0);
                                }
                            }
                        }
                    }
                    else
                    {
                        throw new Exception("Error while trying add data in CreateRowsInAlertsReceived method. SendType was iprange but ipRangedIds is null.");
                    }

                    break;
                default:
                    throw new Exception($"Unexpected sendType {sendType} in CreateRowsInAlertsReceived method at AddRecipients page.");
            }
        }

        public void CreateRowsInAlertsReceivedForUser(long userId, int alertId)
        {
            var instances = _userInstancesRepository.GetInstances(userId);
            foreach (var instance in instances)
            {
                _contentService.AddContentRecipientConnectionToDb(instance.UserId, instance.Clsid, alertId, 0);
            }
        }

        public void CreateRowsInAlertsReceivedForIp(DataSet ipRanges, int alertId)
        {
            var allUsersInstancesIps = _userInstanceIpRepository.GetAll();

            foreach (var ipRange in ipRanges)
            {
                var ipFrom = LongToIp(long.Parse(ipRange.GetValue("ip_from").ToString()));
                var ipTo = LongToIp(long.Parse(ipRange.GetValue("ip_to").ToString()));
                var ipAddressRange = new IpAddressRange(IPAddress.Parse(ipFrom), IPAddress.Parse(ipTo));

                foreach (var usersInstanceIp in allUsersInstancesIps)
                {
                    if (ipAddressRange.IsInRange(IPAddress.Parse(usersInstanceIp.UserInstanceIpField)))
                    {
                        var userInstance =
                            _userInstancesRepository.GetInstance(usersInstanceIp.UserInstancesGuid);

                        if (userInstance != null && userInstance != default(UserInstances))
                        {
                            _contentService.AddContentRecipientConnectionToDb(userInstance.UserId, userInstance.Clsid, alertId, 0);
                        }
                    }
                }
            }
        }

        public static string LongToIp(long longIp)
        {
            var ip = string.Empty;

            for (var i = 0; i < 4; i++)
            {
                var num = (int)(longIp / Math.Pow(256, (3 - i)));

                longIp = longIp - (long)(num * Math.Pow(256, (3 - i)));

                if (i == 0)
                {
                    ip = num.ToString();
                }
                else
                {
                    ip = ip + "." + num;
                }
            }

            return ip;
        }

        public HashSet<int> GetSentIpGroupIdsForUser(long userId)
        {
            var userIps = _userInstanceIpRepository.GetByUserId(userId);
            var allIpGroups = _ipRangeGroupsRepository.GetAll().Select(x => new IpRangeGroup
            {
                FromIp = x.FromIp,
                GroupId = x.GroupId,
                ToIp = x.ToIp
            });

            var sentIpGroupIds = (
                from ipRangeGroup in allIpGroups
                let ipRange = ipRangeGroup.GetIpAddressRange()
                from userIp in userIps
                let userIpAdress = IPAddress.Parse(userIp.UserInstanceIpField)
                where ipRange.IsInRange(userIpAdress)
                select ipRangeGroup.GroupId).ToList();

            return new HashSet<int>(sentIpGroupIds);
        }

        public HashSet<AlertReceived> GetUserAndAlertIdsWithSmsToSend()
        {
            return new HashSet<AlertReceived>(_alertReceivedRepository.GetUserAndAlertIdsWithSmsToSend());
        }
    }
}