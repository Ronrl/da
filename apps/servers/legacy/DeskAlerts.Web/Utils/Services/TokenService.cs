﻿using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Enums;
using DeskAlerts.ApplicationCore.Models;
using DeskAlerts.Server.Models;
using DeskAlerts.Server.Utils.Consts;
using DeskAlerts.Server.Utils.Interfactes;
using DeskAlertsDotNet.Application;
using NLog;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using Castle.Core.Internal;

namespace DeskAlerts.Server.Utils.Services
{
    public class TokenService : ITokenService
    {
        private readonly IUserService _userService = Global.Container.Resolve<IUserService>();
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public const string BearerPrefix = "Bearer ";

        public Token CreateTokenForDomainUser(string userName, string computerName, string computerDomainName, string domainName, string ip, string deskbarId, ClientDeviceType deviceType)
        {
            var user = _userService.GetUserByUserNameAndDomainName(userName, domainName);
            if (user == null)
            {
                _logger.Info($"The user {userName} with deskbarId {deskbarId} with domain name {domainName} hasn't found in Database");
            }
            //ToDo: Remove the crutch
            if (user == null && userName == computerName)
            {
                _logger.Info($"There is a trying to find a computer name user with username {userName} with deskbarId {deskbarId} and domainName {domainName} and computer name {computerName}");
                user = _userService.GetUserByUserNameAndDomainName(userName, string.Empty);
                if (user == null) { _logger.Info($"The computer name user with username {userName} with deskbarId {deskbarId} and domainName {domainName}  and computer name {computerName} couldn't be found"); }
                domainName = SecurityVariables.EmbeddedDomain;
            }
            var token = CreateToken(computerName, computerDomainName, domainName, Guid.NewGuid(), ip, userName, user, deskbarId, deviceType);

            return token;
        }

        public Token CreateTokenForUser(string userName, string computerName, string computerDomainName, string ip, string md5Password, string deskbarId, ClientDeviceType deviceType)
        {
            var user = _userService.GetUser(userName, md5Password);

            if (user == default(User))
            {
                return null;
            }

            var token = CreateToken(computerName, computerDomainName, SecurityVariables.EmbeddedDomain, Guid.NewGuid(), ip, userName, user, deskbarId, deviceType);
            return token;
        }

        public bool ValidateToken(string bearerToken)
        {
            Token token;

            try
            {
                token = TokenStorage.GetToken(GetGuid(bearerToken));
            }
            catch
            {
                _logger.Info($"Token {bearerToken} isn't valid");

                return false;
            }

            token.TokenExpirationTimer.Stop();
            token.TokenExpirationTimer.Start();

            return true;
        }

        private Token CreateToken(string computerName, string computerDomainName, string domainName, Guid guid, string ip, string userName, User user, string deskbarId, ClientDeviceType deviceType)
        {
            var connectSign = _userService.ConnectSign;
            var distinguishedName = (domainName == SecurityVariables.EmbeddedDomain ?
                $"{deskbarId}.{userName}" :
                $"{deskbarId}.{domainName}{connectSign}{userName}").ToLower();

            _logger.Info($"Create token userName:{userName} distinguishedName:{distinguishedName} computerName:{computerName} computerDomainName: {computerDomainName} domainName:{domainName} guid:{guid} deskbarId:{deskbarId}");

            return new Token(60, OnTimedEvent)
            {
                ComputerName = computerName.ToLower(),
                ComputerDomainName = string.IsNullOrEmpty(computerDomainName) ? string.Empty : computerDomainName.ToLower(),
                DomainName = domainName,
                Guid = guid,
                Ip = ip,
                UserName = userName.ToLower(),
                User = user,
                DistinguishedName = distinguishedName,
                DeviceType = deviceType,
                DeskbarId = deskbarId
            };
        }

        private void OnTimedEvent(object sender, ElapsedEventArgs e)
        {
            try
            {
                var token = (Token)((Timer)sender).SynchronizingObject;

                RemoveToken(token.Guid);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public Guid GetGuid(string bearerToken)
        {
            var guidText = bearerToken.Substring(BearerPrefix.Length);
            return Guid.Parse(guidText);
        }

        public void AddToken(Token token)
        {
            TokenStorage.AddToken(token);
        }

        public void RemoveToken(Guid tokenGuid)
        {
            try
            {
                TokenStorage.RemoveToken(tokenGuid);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public void RemoveToken(string username)
        {
            try
            {
                TokenStorage.RemoveToken(username);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public void RemoveAllTokens()
        {
            TokenStorage.RemoveAllTokens();
        }

        public Token GetTokenByGuid(Guid tokenGuid)
        {
            return TokenStorage.GetToken(tokenGuid);
        }

        public IEnumerable<ShowTokenDto> GetAllTokens()
        {
            var result = new List<ShowTokenDto>();

            var tokens = TokenStorage.GetAll();
            foreach (var token in tokens)
            {
                var password = token.Value.User.Password.Length == 32
                    ? $"{token.Value.User.Password.Substring(0, 3)}***{token.Value.User.Password.Substring(token.Value.User.Password.Length - 4, 4)}"
                    : string.Empty;

                var dto = new ShowTokenDto
                {
                    TokenGuid = $"{BearerPrefix}{token.Key}",
                    UserName = token.Value.UserName,
                    Password = password,
                    DeskbarId = token.Value.DeskbarId,
                    DomainName = token.Value.DomainName,
                    DistinguishedName = token.Value.DistinguishedName,
                    ComputerName = token.Value.ComputerName,
                    ComputerDomainName = token.Value.ComputerDomainName,
                    Ip = token.Value.Ip
                };
                result.Add(dto);
            }

            return result.OrderBy(x => x.UserName).ToList();
        }

        private static class TokenStorage
        {
            private static ConcurrentDictionary<Guid, Token> GuidTokenStorage { get; } = new ConcurrentDictionary<Guid, Token>();

            private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

            public static ConcurrentDictionary<Guid, Token> GetAll()
            {
                return GuidTokenStorage.IsNullOrEmpty() ? new ConcurrentDictionary<Guid, Token>() : GuidTokenStorage;
            }

            public static void RemoveToken(Guid tokenGuid)
            {
                if (GuidTokenStorage.IsNullOrEmpty())
                {
                    return;
                }

                if (!GuidTokenStorage.TryRemove(tokenGuid, out var unused))
                {
                    Logger.Error($"TryRemove fail to remove {tokenGuid.ToString()} from {nameof(GuidTokenStorage)}");
                }
            }

            public static void RemoveToken(string username)
            {
                if (GuidTokenStorage.IsNullOrEmpty())
                {
                    return;
                }

                foreach (var token in GuidTokenStorage.Where(kvp => kvp.Value.UserName.Equals(username)).ToList())
                {
                    if (!GuidTokenStorage.TryRemove(token.Key, out var unused))
                    {
                        Logger.Error($"TryRemove fail to remove tokens for user {username} from {nameof(GuidTokenStorage)}");
                    }
                }
            }

            public static void RemoveAllTokens()
            {
                if (GuidTokenStorage.IsNullOrEmpty())
                {
                    return;
                }

                GuidTokenStorage.Clear();
            }

            public static void AddToken(Token token)
            {
                if (!GuidTokenStorage.TryAdd(token.Guid, token))
                {
                    Logger.Error($"TryAdd fail to add token {token.ToString()} for guid {token.Guid} at {nameof(GuidTokenStorage)}");
                }
            }

            public static Token GetToken(Guid tokenGuid)
            {
                return GuidTokenStorage[tokenGuid];
            }
        }
    }
}