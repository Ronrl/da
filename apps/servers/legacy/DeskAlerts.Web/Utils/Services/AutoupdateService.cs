﻿using System;
using System.IO;
using System.Web;
using DeskAlerts.ApplicationCore.Dtos;
using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.ApplicationCore.Models;
using DeskAlertsDotNet.Parameters;
using DeskAlertsDotNet.Utils.Caches;
using Microsoft.Deployment.WindowsInstaller;

namespace DeskAlerts.Server.Utils.Services
{
    public class AutoupdateService : IAutoupdateService
    {
        public static string FileDoesNotExist = Resources.resources.AUTOUPDATE_FILE_DOESNT_EXIST;

        public string Version
        {
            get
            {
                try
                {
                    _version = ReadMsiVersion();
                }
                catch (InstallerException)
                {
                    _version = FileDoesNotExist;
                }

                return _version;
            }
        }

        public string PathToFile => Config.Data.GetString("alertsFolder") + $"clientApplicationUpdates\\{_clientFileName}";

        public string UrlToDownload => Config.Data.GetString("alertsDir") + $"/clientApplicationUpdates/{_clientFileName}";

        private static string _clientFileName = "deskalerts_setup.msi";

        private static string _version;

        private static int _maxSlotCacheLength = int.Parse(Settings.Content["ConfAutoupdateSlotCacheLength"]);

        private static int _slotLifeTime = int.Parse(Settings.Content["ConfAutoupdateSlotLifeTime"]);

        private string ReadMsiVersion()
        {
            using (var database = new Database(PathToFile, DatabaseOpenMode.ReadOnly))
            {
                return database.ExecuteScalar("SELECT `Value` FROM `Property` WHERE `Property` = '{0}'", "ProductVersion") as string;
            }
        }

        public void DeleteSlotById(Guid slotId)
        {
            AutoupdateSlotCache.Items.Remove(slotId);
        }

        private static void DeleteSlotCallback(object sender)
        {
            var slot = (AutoupdateSlot)sender;
            AutoupdateSlotCache.Items.Remove(slot.Id);
        }

        public AutoUpdateSlotDto GetSlot(ClientApplicationInfo clientApplicationInfo)
        {
            if (AutoupdateSlotCache.Items.Count <= _maxSlotCacheLength)
            {
                var slot = new AutoupdateSlot(_slotLifeTime, DeleteSlotCallback)
                {
                    Id = Guid.NewGuid(),
                    ClientApplicationInfo = clientApplicationInfo,
                };

                AutoupdateSlotCache.Items.Add(slot.Id, slot);
                return new AutoUpdateSlotDto() { SlotId = slot.Id.ToString(), Url = UrlToDownload };
            }
            else
            {
                return null;
            }
        }

        public void UpdateConfiguration(int maxSlotCacheLength, int slotLifeTime)
        {
            _maxSlotCacheLength = maxSlotCacheLength;
            _slotLifeTime = slotLifeTime;
        }

        public void SaveFile(HttpPostedFile file)
        {
            if (File.Exists(PathToFile))
            {
                File.Delete(PathToFile);
            }

            file.SaveAs(PathToFile);
            _version = ReadMsiVersion();
        }

        public void DeleteFile()
        {
            if (File.Exists(PathToFile))
            {
                File.Delete(PathToFile);
            }

            _version = FileDoesNotExist;
        }

        public void ClearSlots()
        {
            AutoupdateSlotCache.Items.Clear();
        }
    }
}