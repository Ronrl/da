﻿using System;
using System.Collections.Generic;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Interfaces;
using NLog;

namespace DeskAlerts.Server.Utils.Services
{
    public class AdService : IAdService
    {
        private readonly IDomainRepository _domainRepository;

        private readonly IUserRepository _userRepository;

        private Logger _logger = LogManager.GetCurrentClassLogger();

        public AdService(IDomainRepository domainRepository, IUserRepository userRepository)
        {
            _domainRepository = domainRepository;
            _userRepository = userRepository;
        }

        public AdService(IDomainRepository domainRepository)
        {
            _domainRepository = domainRepository;
        }

        public Domain GetDomain(string domainName)
        {
            domainName = domainName.ToUpper();
            var domains = _domainRepository.GetAll();
            var result = new Dictionary<int, Domain>();
            foreach (var domain in domains)
            {
                var subdomains = domain.Name.ToUpper().Split('.');
                var level = GetLevelOfSubdomains(subdomains, -1, domainName);
                if (level != -1)
                {
                    result.Add(level, domain);
                }
                else if(domain.Name.ToUpper() == domainName)
                {
                    result.Add(0, domain);
                }
            }
            var min = result.Count;
            foreach (var domain in result)
            {
                if (domain.Key < min)
                {
                    min = domain.Key;
                }
            }
            return result[min];
        }

        private int GetLevelOfSubdomains(string[] subdomains, int level, string domainName)
        {
            level++;
            if (subdomains[level].ToUpper() == domainName)
            {
                return level;
            }

            if (subdomains.Length > level + 1)
            {
                return GetLevelOfSubdomains(subdomains, level, domainName);
            }

            return -1;
        }

        public long GetUserIdByDomainNameAndUserName(string domainName, string userName)
        {
            var user = _userRepository.GetUserByDomainNameAndUserName(domainName, userName);
            return user.Id;
        }
    }
}