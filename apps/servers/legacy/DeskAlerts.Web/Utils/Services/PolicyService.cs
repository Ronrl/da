using System;
using System.Collections.Generic;
using System.Linq;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.Server.Utils.Interfactes;
using DeskAlertsDotNet.Models.Permission;
using NLog;

using PolicyEntity = DeskAlerts.ApplicationCore.Entities.Policy;
using PolicyPermissions = DeskAlertsDotNet.Models.Permission.Policy;

namespace DeskAlertsDotNet.Utils.Services
{
    public class PolicyService : IPolicyService
    {
        private readonly IPolicyRepository _policyRepository;

        private readonly IGroupRepository _groupRepository;

        private readonly IDomainRepository _domainRepository;

        private readonly IPolicyPublisherRepository _policyPublisherRepository;

        private readonly IUserRepository _userRepository;

        private readonly ISkinRepository _skinRepository;

        private readonly IContentSettingsRepository _contentSettingsRepository;

        private Logger _logger = LogManager.GetCurrentClassLogger();

        public PolicyService(IPolicyRepository policyRepository, IGroupRepository groupRepository, 
            IDomainRepository domainRepository, IPolicyPublisherRepository policyPublisherRepository,
            IUserRepository userRepository, ISkinRepository skinRepository, IContentSettingsRepository contentSettingsRepository)
        {
            _policyRepository = policyRepository;
            _groupRepository = groupRepository;
            _domainRepository = domainRepository;
            _policyPublisherRepository = policyPublisherRepository;
            _userRepository = userRepository;
            _skinRepository = skinRepository;
            _contentSettingsRepository = contentSettingsRepository;
        }

        public PolicyEntity GetPolicyById(long policyId)
        {
            return _policyRepository.GetById(policyId);
        }

        public long AddPolicy(PolicyEntity policy)
        {
            return _policyRepository.Create(policy);
        }

        public void UpdatePolicy(PolicyEntity policy)
        {
            _policyRepository.Update(policy);
        }

        public PolicyPermissions GetDefaultUserPolicy(long userId, bool isAdmin)
        {
            var policy = new PolicyPermissions
            {
                CanViewAll = isAdmin,
                SendersViewList = new List<long> { userId },
                CanSendAllUsers = isAdmin,
                CanSendAllTemplates = isAdmin,
                CanSendAllOUs = isAdmin,
                CanSendAllGroups = isAdmin,
                IsBroadcastEnabled = isAdmin
            };

            foreach (var permName in Enum.GetValues(typeof(Policies)).Cast<Policies>())
            {
                policy.Permissions.Add(permName, new Permission(new string(isAdmin ? '1' : '0', Permission.DefaultMaskBitsCount)));
            }

            return policy;
        }

        private void SetPermissionsForPolicy(PolicyPermissions policyPermissions, Policies policy, string permissions)
        {
            var permission = new Permission(permissions);
            if (permission.CanSend)
            {
                policyPermissions.Permissions.Add(policy, permission);
            }
        }

        public List<PolicyPermissions> GetPoliciesByPublisherIdAndAlertType(long publisherId, AlertType alertType)
        {
            var policiesPermissions = new List<PolicyPermissions>();
            var policies = _policyRepository.GetPoliciesByPublisherId(publisherId);
            foreach (var policyEntity in policies)
            {
                Policies policy = Policies.Undefined;
                var policyPermissions = new PolicyPermissions();
                switch (alertType)
                {
                    case AlertType.SimpleSurvey:
                    case AlertType.SurveyPoll:
                    case AlertType.SurveyQuiz:
                        policy = Policies.SURVEYS;
                        SetPermissionsForPolicy(policyPermissions, policy, policyEntity.PolicyList.Surveys);
                        break;
                    case AlertType.SimpleAlert:
                    case AlertType.Ticker:
                    case AlertType.Rsvp:
                    case AlertType.DigitalSignature:
                    case AlertType.VideoAlert:
                        policy = Policies.ALERTS;
                        SetPermissionsForPolicy(policyPermissions, policy, policyEntity.PolicyList.Alerts);
                        break;
                    case AlertType.EmergencyAlert:
                        policy = Policies.IM;
                        SetPermissionsForPolicy(policyPermissions, policy, policyEntity.PolicyList.Im);
                        break;
                    case AlertType.RssFeed:
                    case AlertType.RssAlert:
                        policy = Policies.RSS;
                        SetPermissionsForPolicy(policyPermissions, policy, policyEntity.PolicyList.Rss);
                        break;
                    case AlertType.ScreenSaver:
                        policy = Policies.SCREENSAVERS;
                        SetPermissionsForPolicy(policyPermissions, policy, policyEntity.PolicyList.Screensavers);
                        break;
                    case AlertType.Wallpaper:
                        policy = Policies.WALLPAPERS;
                        SetPermissionsForPolicy(policyPermissions, policy, policyEntity.PolicyList.Wallpapers);
                        break;
                    case AlertType.LockScreen:
                        policy = Policies.LOCKSCREENS;
                        SetPermissionsForPolicy(policyPermissions, policy, policyEntity.PolicyList.Lockscreens);
                        break;
                }

                policyPermissions.Id = (int)policyEntity.Id;
                if (policyEntity.Type == "A")
                {
                    policyPermissions.CanSendAllUsers = true;
                    policyPermissions.CanSendAllOUs = true;
                    policyPermissions.CanSendAllGroups = true;
                    policyPermissions.CanSendAllTemplates = true;
                    policyPermissions.IsBroadcastEnabled = true;
                    policyPermissions.IsAdminPolicy = true;
                }
                else if (policyPermissions.HasPermissions && policyPermissions.Permissions[policy].CanSend)
                {
                    policyPermissions.RecipientsList = _policyRepository.GetPolicyUsersByPolicyId(policyPermissions.Id).Select(x => x.UserId).ToList();
                    if (policyPermissions.RecipientsList.Count == 0)
                    {
                        policyPermissions.CanSendAllUsers = true;
                    }

                    policyPermissions.OrganizationUnits = _policyRepository.GetPolicyOUsByPolicyId(policyPermissions.Id).Select(x => x.OUId).ToList();
                    if (policyPermissions.OrganizationUnits.Count == 0)
                    {
                        policyPermissions.CanSendAllOUs = true;
                    }

                    policyPermissions.Groups = _policyRepository.GetPolicyGroupsByPolicyId(policyPermissions.Id).Select(x => x.GroupId).ToList();
                    if (policyPermissions.Groups.Count == 0)
                    {
                        policyPermissions.CanSendAllGroups = true;
                    }

                    policyPermissions.IsBroadcastEnabled = policyPermissions.Groups.Count == 0 && policyPermissions.OrganizationUnits.Count == 0 && policyPermissions.RecipientsList.Count == 0;
                }

                if (policyPermissions.HasPermissions)
                {
                    policiesPermissions.Add(policyPermissions);
                }
            }

            return policiesPermissions;
        }

        public PolicyPermissions GetMergedPolicyByPublisherId(long publisherId)
        {
            var policy = new PolicyPermissions();
            var policies = _policyRepository.GetPoliciesByPublisherId(publisherId);
            foreach (var policyEntity in policies)
            {
                policy.CanViewAll |= policyEntity.PolicyViewer.PolicyId != 0;
                if (policyEntity.PolicyViewer.PolicyId == 0 && !policy.SendersViewList.Any(x => x == publisherId))
                {
                    policy.SendersViewList.Add(publisherId);
                }

                policy.IsAdminPolicy |= policyEntity.Type == "A";

                Merge(Policies.ALERTS, policyEntity.PolicyList.Alerts, policy);
                Merge(Policies.CAMPAIGN, policyEntity.PolicyList.Campaign, policy);
                Merge(Policies.EMAILS, policyEntity.PolicyList.Emails, policy);
                Merge(Policies.SMS, policyEntity.PolicyList.Sms, policy);
                Merge(Policies.USERS, policyEntity.PolicyList.Users, policy);
                Merge(Policies.GROUPS, policyEntity.PolicyList.Groups, policy);
                Merge(Policies.TEMPLATES, policyEntity.PolicyList.Templates, policy);
                Merge(Policies.SURVEYS, policyEntity.PolicyList.Surveys, policy);
                Merge(Policies.TEXT_TEMPLATES, policyEntity.PolicyList.TextTemplates, policy);
                Merge(Policies.STATISTICS, policyEntity.PolicyList.Statistics, policy);
                Merge(Policies.IP_GROUPS, policyEntity.PolicyList.IpGroups, policy);
                Merge(Policies.RSS, policyEntity.PolicyList.Rss, policy);
                Merge(Policies.SCREENSAVERS, policyEntity.PolicyList.Screensavers, policy);
                Merge(Policies.WALLPAPERS, policyEntity.PolicyList.Wallpapers, policy);
                Merge(Policies.LOCKSCREENS, policyEntity.PolicyList.Lockscreens, policy);
                Merge(Policies.IM, policyEntity.PolicyList.Im, policy);
                Merge(Policies.TEXT_TO_CALL, policyEntity.PolicyList.TextToCall, policy);
                Merge(Policies.FEEDBACK, policyEntity.PolicyList.Feedback, policy);
                Merge(Policies.CC, policyEntity.PolicyList.Cc, policy);
            }

            policy.RecipientsList = _policyRepository.GetPolicyUsersByPublisherId(publisherId).Select(x => x.UserId).ToList();
            if (policy.RecipientsList.Count == 0)
            {
                policy.CanSendAllUsers = true;
            }

            policy.OrganizationUnits = _policyRepository.GetPolicyOUsByPublisherId(publisherId).Select(x => x.OUId).ToList();
            if (policy.OrganizationUnits.Count == 0)
            {
                policy.CanSendAllOUs = true;
            }

            policy.Groups = _policyRepository.GetPolicyGroupsByPublisherId(publisherId).Select(x => x.GroupId).ToList();
            if (policy.Groups.Count == 0)
            {
                policy.CanSendAllGroups = true;
            }

            policy.IsBroadcastEnabled = policy.Groups.Count == 0 && policy.OrganizationUnits.Count == 0 && policy.RecipientsList.Count == 0;
            return policy;
        }

        public IEnumerable<PolicyEntity> GetAdministratorPoliciesForDomainsGroups()
        {
            IEnumerable<long> domainsList = _domainRepository.GetAll().Select(x => x.Id);
            var policiesIdForDomainsGroups = _groupRepository.GetAll().Where(x => domainsList.Contains(x.DomainId))
                .Where(u => u.PolicyId != null).Select(w => w.PolicyId).ToList();
            IEnumerable<PolicyEntity> adminsPolicyList = _policyRepository.GetAll().Where(t => policiesIdForDomainsGroups.Contains(t.Id)).Where(p => p.Type == "A");

            return adminsPolicyList;
        }

        public List<PolicyEntity> GetPoliciesByPublisherId(long publisherId)
        {
            return _policyRepository.GetPoliciesByPublisherId(publisherId);
        }

        public List<Guid> GetMergedPolicySkinsByPublisherId(long publisherId)
        {
            var result = new List<Guid>();

            var policies = _policyRepository.GetPoliciesByPublisherId(publisherId);
            if (policies == null || !policies.Any())
            {
                result = _skinRepository.GetAll()
                    .Select(x => x.Id)
                    .ToList();
            }
            else
            {
                foreach (var policy in policies)
                {
                    var policiesSkins = _skinRepository.GetSkinsByPolicyId(policy.Id)
                        .Select(x => x.Id)
                        .ToList();

                    foreach (var policySkin in policiesSkins)
                    {
                        if (!result.Contains(policySkin))
                        {
                            result.Add(policySkin);
                        }
                    }
                }
            }
            
            return result;
        }

        public List<string> GetMergedPolicyContentSettingsByPublisherId(long publisherId)
        {
            var result = new List<string>();

            var policies = _policyRepository.GetPoliciesByPublisherId(publisherId);
            if (policies == null || !policies.Any())
            {
                result = _contentSettingsRepository.GetAll()
                    .Select(x => x.Name)
                    .ToList();
            }
            else
            {
                foreach (var policy in policies)
                {
                    var contentOptionByPolicy = _contentSettingsRepository.GetContentSettingsByPolicyId(policy.Id)
                        .Select(x => x.Name);

                    foreach (var contentOption in contentOptionByPolicy)
                    {
                        if (!result.Contains(contentOption))
                        {
                            result.Add(contentOption);
                        }
                    }
                }
            }

            return result;
        }

        private void Merge(Policies policyEnum, string value, PolicyPermissions policy)
        {
            if (!string.IsNullOrEmpty(value))
            {
                if (policy.Permissions.ContainsKey(policyEnum))
                {
                    policy.Permissions[policyEnum] =
                        Permission.Merge(policy.Permissions[policyEnum], new Permission(value));
                }
                else
                {
                    policy.Permissions.Add(policyEnum, new Permission(value));
                }
            }
        }

        public IEnumerable<PolicyPublisher> GetPublisherPolicies(long publisherId)
        {
            return _policyPublisherRepository.GetByPublisherId(publisherId);
        }

        /// <summary>
        /// Add a new publisher policy
        /// </summary>
        /// <param name="userId">Id of the user</param>
        /// <param name="policyId">Id of the policy</param>
        /// <param name="groupId">Id of the group</param>
        public PolicyPublisher AddPolicyPublisher(long userId, long policyId, long groupId)
        {
            var policyPublisher = new PolicyPublisher()
            {
                GroupId = groupId,
                PolicyId = policyId,
                PublisherId = userId
            };

            _policyPublisherRepository.Create(policyPublisher);
            _logger.Info($"Policy with Id {policyId} was added to user with Id {userId} from group with Id {groupId}");
            return policyPublisher;
        }

        /// <summary>
        /// Remove publisher policy and if it last one then delete publisher
        /// </summary>
        /// <param name="policyId">Id of the policy</param>
        /// <param name="groupId">Id of the group</param>
        public void RemovePolicyFromPublisher(long policyId, long groupId)
        {
            var publishersPolicy = _policyPublisherRepository.GetByGroupId(groupId);
            foreach (var publisherPolicy in publishersPolicy)
            {
                _policyPublisherRepository.Delete(publisherPolicy);
                _logger.Info($"publisher policy with Id {publisherPolicy.Id} was deleted");
                var remainedPublishersPolicy = _policyPublisherRepository.GetByPublisherId(publisherPolicy.PublisherId);
                if (!remainedPublishersPolicy.Any())
                {
                    var user = _userRepository.GetById(publisherPolicy.PublisherId);
                    _userRepository.Delete(user);
                    _logger.Info($"publisher {user.Username} was deleted");
                }
            }
        }

        public void RemovePolicyPublisherByPublisherIdByPolicyIdByGroupId(long userId, long policyId, long groupId)
        {
            var publisherPolicy = _policyPublisherRepository.GetByPolicyIdPublisherIdGroupId(policyId, userId, groupId);
            _policyPublisherRepository.Delete(publisherPolicy);
        }

        public void RemovePolicyPublisherByPolicyIdPublisherId(long policyId, long publisherId)
        {
            var publisherPolicies = _policyPublisherRepository.GetByPolicyIdPublisherId(policyId, publisherId);
            foreach (var publisherPolicy in publisherPolicies)
            {
                _policyPublisherRepository.Delete(publisherPolicy);
            }
        }

        public void RemovePolicy(PolicyEntity policy)
        {
            _policyRepository.Delete(policy);
        }

        public IEnumerable<PolicyPublisher> GetPolicyPublisherByPublisherId(long publisherId)
        {
            return _policyPublisherRepository.GetByPublisherId(publisherId);
        }

        public void RemovePolicyPublisher(PolicyPublisher policyPublisher)
        {
            _logger.Info($"Policy ID:{policyPublisher.Id},GroupID:{policyPublisher.GroupId},UserID:{policyPublisher.PublisherId} was deleted");
            _policyPublisherRepository.Delete(policyPublisher);
        }

        public bool PolicyExists(string policyName)
        {
            return _policyRepository.EntityExistsByAttribute("name", policyName);
        }

        public bool IsCanSendToSpecificRecipientsOnly(long policyId)
        {
            var recipientsTotal = _policyRepository.GetRecipientsTotal(policyId);
            return recipientsTotal > 0;
        }
    }
}