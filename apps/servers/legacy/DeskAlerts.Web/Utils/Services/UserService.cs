﻿using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.Server.Utils.Interfactes;
using NLog;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace DeskAlertsDotNet.Utils.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IGroupRepository _groupRepository;

        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public char ConnectSign => '/';

        public char DeskbarConnectSign => '.';

        public UserService(IUserRepository userRepository, IGroupRepository groupRepository)
        {
            _userRepository = userRepository;
            _groupRepository = groupRepository;
        }

        /// <summary>
        /// Register a new publisher
        /// </summary>
        /// <param name="userName">Name of the user</param>
        /// <param name="domainId">Id of the domain</param>
        /// <param name="password">Password of the user</param>
        /// <returns></returns>
        public long RegisterPublisher(string userName, long domainId, string password)
        {
            var passwordBytes = Encoding.UTF8.GetBytes(password);
            var passwordEncoded = Convert.ToBase64String(passwordBytes);

            var user = new User()
            {
                Username = userName,
                Role = "E",
                RegistrationDateTime = DateTime.Now,
                DomainId = domainId,
                Password = passwordEncoded
            };

            var userId = _userRepository.Create(user);
            _logger.Info($"Publisher {userName} was registred");

            return userId;
        }

        public Group GetGroupById(long groupId)
        {
            return _groupRepository.GetById(groupId);
        }

        public void UpdateGroup(Group group)
        {
            _groupRepository.Update(group);
        }

        public void RemovePublisher(User user)
        {
            _userRepository.Delete(user);
        }

        public User GetUserById(long userId)
        {
            return _userRepository.GetById(userId);
        }

        public User GetUserByUserNameAndDomainName(string userName, string domainName)
        {
            return _userRepository.GetUserByDomainNameAndUserName(domainName, userName);
        }

        public void UpdateUserLastRequest(long userId)
        {
            var user = _userRepository.GetById(userId);
            user.LastRequestDateTime = DateTime.Now;
            _userRepository.Update(user);
        }

        public void UpdateUserTimeZone(long userId, int timeZone)
        {
            var user = _userRepository.GetById(userId);
            if (user.TimeZone != timeZone)
            {
                user.TimeZone = timeZone;
                _userRepository.Update(user);
            }
        }

        public User GetUser(string userName, string md5password)
        {
            return _userRepository.GetUserByUserNameAndMd5Passowrd(userName, md5password);
        }

        public string GetUserDomainNameByUserId(long userId)
        {
            var result = string.Empty;

            if (userId <= default(long))
            {
                return result;
            }

            result = _userRepository.GetUserDomainNameByUserId(userId);

            return result;
        }

        public string JoinUserNameAndDomainName(string userName, string domainName)
        {
            return string.IsNullOrEmpty(domainName)
                ? userName
                : domainName + ConnectSign + userName;
        }

        public string JoinDeskbarAndUserNameAndDomainName(string deskbar, string userName, string domainName)
        {
            var fullname = string.IsNullOrEmpty(domainName)
                ? userName
                : domainName + ConnectSign + userName;

            return (deskbar + DeskbarConnectSign + fullname).ToLower();
        }

        public Tuple<string, string> SeparateUserNameAndDomainName(string fullUserName)
        {
            string domainName = string.Empty, userName = string.Empty;
            
            if (fullUserName.LastIndexOf(ConnectSign) != -1)
            {
                var strings = fullUserName.Split(ConnectSign);
                domainName = strings[0];
                userName = strings[1];
            }
            else
            {
                domainName = string.Empty;
                userName = fullUserName;
            }
            return Tuple.Create(domainName, userName);
        }

        public string ConvertToMD5Hash(string password)
        {
            StringBuilder hash = new StringBuilder();
            MD5CryptoServiceProvider md5provider = new MD5CryptoServiceProvider();
            byte[] bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(password));

            for (int i = 0; i < bytes.Length; i++)
            {
                hash.Append(bytes[i].ToString("x2"));
            }

            return hash.ToString().ToUpper();

        }

        public User GetUserByName(string userName)
        {
            if (string.IsNullOrEmpty(userName))
            {
                throw new ArgumentNullException(nameof(userName));
            }

            try
            {
                var userNameAndDomainName = SeparateUserNameAndDomainName(userName);
                var user = _userRepository.GetUserByDomainNameAndUserName(domainName: userNameAndDomainName.Item1, userName: userNameAndDomainName.Item2);

                return user;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IEnumerable<string> GetDomainUsersUsernames(int domainId)
        {
            return _userRepository.GetDomainUsersUsernames(domainId);
        }

        public string ConvertToSeconds(string pollPeriod)
        {
            pollPeriod = string.IsNullOrEmpty(pollPeriod) ? "5" : pollPeriod;
            var timeUnit = pollPeriod.Substring(pollPeriod.Length - 1);
            switch (timeUnit)
            {
                case "s":
                    pollPeriod = new TimeSpan(0, 0, Convert.ToInt32(pollPeriod.Remove(pollPeriod.Length - 1))).TotalSeconds.ToString();
                    break;
                case "h":
                    pollPeriod = new TimeSpan(Convert.ToInt32(pollPeriod.Remove(pollPeriod.Length - 1)), 0, 0).TotalSeconds.ToString();
                    break;
                default:
                case "m":
                    {
                        pollPeriod = timeUnit == "m" ? pollPeriod.Remove(pollPeriod.Length - 1) : pollPeriod;
                        pollPeriod = new TimeSpan(0, Convert.ToInt32(pollPeriod), 0).TotalSeconds.ToString();
                        break;
                    }
            }
            return pollPeriod;
        }

        public void UpdatePollPeriod(long userId, string pollPeriod)
        {
            _logger.Debug($"Trying to update version for user with id {userId} and poll period {pollPeriod}");
            var user = _userRepository.GetById(userId);
            if (user == null) { _logger.Debug($"The user with id {userId} couldn't be found for poll period updating"); }
            user.PollPeriod = ConvertToSeconds(pollPeriod);
            _userRepository.Update(user);
            _logger.Info($"The poll period has been successfully updated for user with id {userId} to poll period {pollPeriod}");
        }

        public void UpdateVersion(long userId, string version)
        {
            _logger.Debug($"Trying to update version for user with id {userId} and version {version}");
            var user = _userRepository.GetById(userId);
            if (user == null) { _logger.Debug($"The user with id {userId} couldn't be found for version updating"); }
            user.Version = version;
            _userRepository.Update(user);
            _logger.Info($"The version has been successfully updated for user with id {userId} to version {version}");
        }
    }
}