using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Enums;
using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.ApplicationCore.Models;
using DeskAlerts.Server.Utils.Consts;
using DeskAlerts.Server.Utils.Helpers;
using DeskAlerts.Server.Utils.Interfactes;
using DeskAlerts.Server.Utils.Managers;
using DeskAlertsDotNet.Application;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Models;
using DeskAlertsDotNet.Utils.Consts;
using DeskAlertsDotNet.Utils.Factories;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using DeskAlerts.ApplicationCore.Models.ContentTypes;

namespace DeskAlerts.Server.Utils.Services
{
    public class ContentService : IContentService
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private readonly int[] _badClasses = { 2, 4, 8, (int)AlertType.LockScreen };

        private readonly IAlertRepository _alertRepository;
        private readonly IAlertReceivedRepository _alertReceivedRepository;
        private readonly IAlertReadRepository _alertReadRepository;
        private readonly IMultipleAlertRepository _multipleAlertRepository;
        private readonly ITickerRepository _tickerRepository;
        private readonly IRsvpRepository _rsvpRepository;
        private readonly ISurveyRepository _surveyRepository;
        private readonly IVideoAlertRepository _videoAlertRepository;
        private readonly IWallPaperRepository _wallPaperRepository;
        private readonly IScreenSaverRepository _screenSaverRepository;
        private readonly ILockScreenRepository _lockScreenRepository;
        private readonly IScheduledContentRepository _scheduledContentRepository;

        private readonly object _alertReceivedRepositoryLock = new object();

        public ContentService(
            IAlertRepository alertRepository, 
            IAlertReceivedRepository alertReceivedRepository, 
            IAlertReadRepository alertReadRepository, 
            IMultipleAlertRepository multipleAlertRepository, 
            ITickerRepository tickerRepository, 
            IRsvpRepository rsvpRepository, 
            ISurveyRepository surveyRepository, 
            IVideoAlertRepository videoAlertRepository, 
            IWallPaperRepository wallPaperRepository, 
            IScreenSaverRepository screenSaverRepository, 
            ILockScreenRepository lockScreenRepository,
            IScheduledContentRepository scheduledContentRepository
            )
        {
            _alertRepository = alertRepository;
            _alertReceivedRepository = alertReceivedRepository;
            _alertReadRepository = alertReadRepository;
            _multipleAlertRepository = multipleAlertRepository;
            _tickerRepository = tickerRepository;
            _rsvpRepository = rsvpRepository;
            _surveyRepository = surveyRepository;
            _videoAlertRepository = videoAlertRepository;
            _wallPaperRepository = wallPaperRepository;
            _screenSaverRepository = screenSaverRepository;
            _lockScreenRepository = lockScreenRepository;
            _scheduledContentRepository = scheduledContentRepository;
        }

        #region Implementation

        [ObsoleteAttribute("This method is obsolete. Use new content delivery system instead.", false)]
        private IEnumerable<Alert> GetContent(DateTime localDateTime, Token token)
        {
            return CollectAllContent(token, localDateTime).Where(x => IsValidAlert(x, token, localDateTime));
        }

        public CacheManager CacheManager => Global.CacheManager;

        public AlertEntity GetContentById(long id)
        {
            return _alertRepository.GetById(id);
        }

        public IEnumerable<long> GetTerminatedAlertsForInstance(DateTime localDateTime, Token token)
        {
            var userId = token.User.Id;
            var deskbarId = token.DeskbarId;

            var terminatedAlertsIds = Enumerable.Empty<long>();
            lock (_alertReceivedRepositoryLock)
            {
                terminatedAlertsIds = CollectAllContent(token, localDateTime)
                    .Where(x => IsActiveAlert(x, token, localDateTime))
                    .Select(x => _alertRepository.GetById(x.Id))
                    .Where(x => x.Type != AlertType.Wallpaper && 
                                x.Type != AlertType.ScreenSaver && 
                                x.Type != AlertType.LockScreen &&
                                x.Terminated == 1 && 
                                !_alertReceivedRepository.IsTerminateCommandReceived(x.Id, userId, deskbarId))
                    .Select(x => Convert.ToInt64(x.Id));
            }

            return terminatedAlertsIds;
        }

        public IEnumerable<Alert> GetContentByUser(DateTime localDateTime, Token token, string userName, string domain)
        {
            var userAlerts = CacheManager.GetAlertsForInstance(
                token.DeskbarId,
                token.UserName,
                token.DomainName == SecurityVariables.EmbeddedDomain ? string.Empty : token.DomainName);
            return userAlerts;
        }

        public IEnumerable<Alert> GetContentByComputerName(DateTime localDateTime, Token token, string computerName, string domain)
        {
            var computerAlerts = CacheManager.GetAlertsForComputer(token.ComputerName, token.ComputerDomainName);
            return computerAlerts;
        }

        public IEnumerable<Alert> GetContentByIpGroup(DateTime localDateTime, Token token, string ipAddress)
        {
            var ip = IpReader.GetIpAddressListAsInt64(ipAddress);
            var ipAlerts = CacheManager.GetAlertsForIp(ip);
            return ipAlerts;
        }

        public IEnumerable<Alert> GetWallpapers(DateTime localDateTime, Token token)
        {
            return GetContent(localDateTime, token).Where(x => x.Class == (int)AlertType.Wallpaper);
        }
        
        public IEnumerable<Alert> GetLockScreens(DateTime localDateTime, Token token)
        {
            return GetContent(localDateTime, token).Where(x => x.Class == (int)AlertType.LockScreen);
        }

        public IEnumerable<Alert> GetScreensaver(DateTime localDateTime, Token token)
        {
            return GetContent(localDateTime, token).Where(x => x.Class == (int)AlertType.ScreenSaver);
        }

        public IEnumerable<Alert> GetAlerts(DateTime localDateTime, Token token)
        {
            return GetContent(localDateTime, token).Where(IsSimpleAlert);
        }

        public IEnumerable<long> GetContentForUser(DateTime localDateTime, long userId)
        {
            var alertsIds = _alertRepository.GetActualAlertIds(userId, localDateTime);
            var tickersIds = _tickerRepository.GetActualTickersIds(userId, localDateTime);
            var rsvpIds = _rsvpRepository.GetActualRsvpIds(userId, localDateTime);
            var surveysIds = _surveyRepository.GetActualSurveysIds(userId, localDateTime);
            var videoAlertsIds = _videoAlertRepository.GetActualVideoAlertsIds(userId, localDateTime);
            var wallpapersIds = _wallPaperRepository.GetActualWallPapersIds(userId, localDateTime);
            var screensaversIds = _screenSaverRepository.GetActualScreenSaversIds(userId, localDateTime);
            var lockscreensIds = _lockScreenRepository.GetActualLockScreensIds(userId, localDateTime);

            var scheduledAlertsIds = _scheduledContentRepository.GetActualScheduledContentIdsForUser<AlertContentType>(userId);
            var scheduledTickersIds = _scheduledContentRepository.GetActualScheduledContentIdsForUser<TickerContentType>(userId);
            var scheduledRsvpIds = _scheduledContentRepository.GetActualScheduledContentIdsForUser<RsvpContentType>(userId);
            var scheduledSurveysIds = _scheduledContentRepository.GetActualScheduledContentIdsForUser<SurveyContentType>(userId);
            var scheduledVideoAlertsIds = _scheduledContentRepository.GetActualScheduledContentIdsForUser<VideoAlertContentType>(userId);
            var scheduledWallpapersIds = _scheduledContentRepository.GetActualScheduledContentIdsForUser<WallpaperContentType>(userId);
            var scheduledScreensaversIds = _scheduledContentRepository.GetActualScheduledContentIdsForUser<ScreensaverContentType>(userId);

            var repeatableAlertsIds = _scheduledContentRepository.GetActualRepeatableTemplatesIdsForUser<AlertContentType>(userId);
            var repeatableTickersIds = _scheduledContentRepository.GetActualRepeatableTemplatesIdsForUser<TickerContentType>(userId);
            var repeatableRsvpIds = _scheduledContentRepository.GetActualRepeatableTemplatesIdsForUser<RsvpContentType>(userId);
            var repeatableVideoAlertsIds = _scheduledContentRepository.GetActualRepeatableTemplatesIdsForUser<VideoAlertContentType>(userId);

            var contentIds = alertsIds
                .Concat(tickersIds)
                .Concat(rsvpIds)
                .Concat(surveysIds)
                .Concat(videoAlertsIds)
                .Concat(wallpapersIds)
                .Concat(screensaversIds)
                .Concat(lockscreensIds)
                .Concat(scheduledAlertsIds)
                .Concat(scheduledTickersIds)
                .Concat(scheduledRsvpIds)
                .Concat(scheduledSurveysIds)
                .Concat(scheduledVideoAlertsIds)
                .Concat(scheduledWallpapersIds)
                .Concat(scheduledScreensaversIds)
                .Concat(repeatableAlertsIds)
                .Concat(repeatableTickersIds)
                .Concat(repeatableRsvpIds)
                .Concat(repeatableVideoAlertsIds)
                .Distinct();

            return contentIds;
        }

        public IEnumerable<long> GetContentForUserInstance(DateTime localDateTime, long userId, Guid userInstanceId)
        {
            var alertsIds = _alertRepository.GetActualAlertIds(userId, userInstanceId, localDateTime, string.Empty, string.Empty);
            var tickersIds = _tickerRepository.GetActualTickersIds(userId, userInstanceId, localDateTime, string.Empty, string.Empty);
            var rsvpIds = _rsvpRepository.GetActualRsvpIds(userId, userInstanceId, localDateTime, string.Empty, string.Empty);
            var surveysIds = _surveyRepository.GetActualSurveysIds(userId, userInstanceId, localDateTime, string.Empty, string.Empty);
            var videoAlertsIds = _videoAlertRepository.GetActualVideoAlertsIds(userId, userInstanceId, localDateTime);
            var wallpapersIds = _wallPaperRepository.GetActualWallPapersIds(userId, userInstanceId, localDateTime, string.Empty, string.Empty);
            var screensaversIds = _screenSaverRepository.GetActualScreenSaversIds(userId, userInstanceId, localDateTime, string.Empty, string.Empty);
            var lockscreensIds = _lockScreenRepository.GetActualLockScreensIds(userId, userInstanceId, localDateTime, string.Empty, string.Empty);

            var scheduledAlertsIds = _scheduledContentRepository.GetActualScheduledContentIdsForUserInstance<AlertContentType>(userInstanceId);
            var scheduledTickersIds = _scheduledContentRepository.GetActualScheduledContentIdsForUserInstance<TickerContentType>(userInstanceId);
            var scheduledRsvpIds = _scheduledContentRepository.GetActualScheduledContentIdsForUserInstance<RsvpContentType>(userInstanceId);
            var scheduledSurveysIds = _scheduledContentRepository.GetActualScheduledContentIdsForUserInstance<SurveyContentType>(userInstanceId);
            var scheduledVideoAlertsIds = _scheduledContentRepository.GetActualScheduledContentIdsForUserInstance<VideoAlertContentType>(userInstanceId);
            var scheduledWallpapersIds = _scheduledContentRepository.GetActualScheduledContentIdsForUserInstance<WallpaperContentType>(userInstanceId);
            var scheduledScreensaversIds = _scheduledContentRepository.GetActualScheduledContentIdsForUserInstance<ScreensaverContentType>(userInstanceId);

            var repeatableAlertsIds = _scheduledContentRepository.GetActualRepeatableTemplatesIdsForUserInstance<AlertContentType>(userInstanceId);
            var repeatableTickersIds = _scheduledContentRepository.GetActualRepeatableTemplatesIdsForUserInstance<TickerContentType>(userInstanceId);
            var repeatableRsvpIds = _scheduledContentRepository.GetActualRepeatableTemplatesIdsForUserInstance<RsvpContentType>(userInstanceId);
            var repeatableVideoAlertsIds = _scheduledContentRepository.GetActualRepeatableTemplatesIdsForUserInstance<VideoAlertContentType>(userInstanceId);

            var contentIds = alertsIds
                .Concat(tickersIds)
                .Concat(rsvpIds)
                .Concat(surveysIds)
                .Concat(videoAlertsIds)
                .Concat(wallpapersIds)
                .Concat(screensaversIds)
                .Concat(lockscreensIds)
                .Concat(scheduledAlertsIds)
                .Concat(scheduledTickersIds)
                .Concat(scheduledRsvpIds)
                .Concat(scheduledSurveysIds)
                .Concat(scheduledVideoAlertsIds)
                .Concat(scheduledWallpapersIds)
                .Concat(scheduledScreensaversIds)
                .Concat(repeatableAlertsIds)
                .Concat(repeatableTickersIds)
                .Concat(repeatableRsvpIds)
                .Concat(repeatableVideoAlertsIds)
                .Distinct();

            return contentIds;
        }

        private bool IstReceivedByCacheManager(IReceivedContent content, Token token)
        {
            return content.ReceivedStatus == AlertReceiveStatus.Received && !CacheManager.IsReceivedAlertForUser(token.DistinguishedName, (int)content.Id);
        }

        public void UpdateAlertEntity(long id, List<KeyValuePair<string, string>> parametersDictionary)
        {
            var alertEntity = _alertRepository.GetById(id);
            foreach (var alertParameter in parametersDictionary)
            {
                PropertyInfo parameterType = null;
                try
                {
                    parameterType = alertEntity.GetType().GetProperty(alertParameter.Key);

                }

                catch (Exception ex)
                {
                    _logger.Debug($"There is no AlertEntity value with name {alertParameter.Key}. The value of this element is {alertParameter.Value}");
                }
                parameterType.SetValue(alertEntity,
                                       Convert.ChangeType(alertParameter.Value, parameterType.PropertyType));
                _alertRepository.Update(alertEntity);

            }
        }

        public void ReceiveContent(IEnumerable<IReceivedContent> contents, Token token)
        {
            var receivedContent = contents.Where(x => IstReceivedByCacheManager(x, token)).ToList();

            ChangeStatus(receivedContent, token, AlertReceiveStatus.Received);

            foreach (var content in receivedContent)
            {
                _logger.Debug($"CacheManager.AddAlertsToRecieved token.DistinguishedName:{token.DistinguishedName}content.Id:{content.Id}");

                CacheManager.AddAlertsToReceivedCache(token.DistinguishedName, (int)content.Id);
            }
        }

        public void ChangeStatus(IEnumerable<IReceivedContent> receivedContents, Token token, AlertReceiveStatus status)
        {
            foreach (var receivedContent in receivedContents)
            {
                _logger.Debug($"UpdateAlertReceived receivedContent.Id:{receivedContent.Id} token.User.Id:{token.User.Id}");
                var alertsReceived = GetAlertsReceived(receivedContent.Id, token.User.Id, token.DeskbarId);
                if (alertsReceived == null)
                {
                    continue;
                }
                UpdateAlertReceived(alertsReceived, status);
            }
        }

        public void ReadContent(long alertId, Token token)
        {
            CreateReadAlert(alertId, token.User.Id, DateTime.Now);
        }

        public void TerminateAlert(long contentId, Token token)
        {
            var content = _alertReceivedRepository.GetByAlertIdAndUserId(contentId, token.User.Id, token.DeskbarId);
            content.TerminateStatus = 1;
            lock (_alertReceivedRepositoryLock)
            {
                _alertReceivedRepository.Update(content);
            }
        }

        public long GetContentsCount()
        {
            return _alertRepository.GetAlertsCount();
        }

        public long GetLastContentId()
        {
            return _alertRepository.GetLastAlertId();
        }

        #endregion

        #region Methods

        private AlertReceived GetAlertsReceived(long alertid, long userId, string deskbarId)
        {
            lock (_alertReceivedRepositoryLock)
            {
                return _alertReceivedRepository.GetByAlertIdAndUserId(alertid, userId, deskbarId);
            }
        }

        public IEnumerable<string> GetConfirmedUsernamesForMultipleAlert(long alertId)
        {
            try
            {
                return _multipleAlertRepository
                    .GetConfirmedUsernames(alertId)
                    .Where(name => !string.IsNullOrEmpty(name));
            }
            catch (Exception ex)
            {
                _logger.Error($"Getting of confirmed users for multiple alert {alertId} failed. Exception {ex}");
                return Enumerable.Empty<string>();
            }
        }

        public int GetConfirmedUsersForMultipleAlertAmount(long alertId)
        {
            try
            {
                return _multipleAlertRepository
                    .GetConfirmedUsernames(alertId)
                    .Count();
            }
            catch (Exception ex)
            {
                _logger.Error($"Getting of confirmed users amount for multiple alert {alertId} failed. Exception {ex}");
                return 0;
            }
        }

        public bool CheckForMultipleAlert(long alertId)
        {
            return _multipleAlertRepository.CheckIfMultiple(alertId);
        }

        public void UpdateAlertReceived(AlertReceived alertsReceived, AlertReceiveStatus status)
        {
            lock (_alertReceivedRepositoryLock)
            {
                alertsReceived.Status = status;
                _alertReceivedRepository.Update(alertsReceived);
            }
        }

        private void CreateReadAlert(long alertId, long userId, DateTime date)
        {
            var alertRead = new AlertRead
            {
                AlertId = alertId,
                UserId = userId,
                Date = date
            };
            _alertReadRepository.Create(alertRead);
        }
        private IEnumerable<Alert> CollectAllContent(Token token, DateTime localDateTime)
        {
            var userAlerts = GetContentByUser(localDateTime, token, token.UserName, token.DomainName);
            var computerAlerts = GetContentByComputerName(localDateTime, token, token.ComputerName, token.DomainName);
            var ipAlerts = GetContentByIpGroup(localDateTime, token, token.Ip);

            return ipAlerts.Concat(userAlerts.Concat(computerAlerts));
        }

        private bool IsActiveAlert(Alert alert, Token token, DateTime localDateTime)
        {
            var localtimeDateTime = alert.Lifetime == 1 ? DateTime.Now : localDateTime;
            var isActive = !alert.IsExpired(localtimeDateTime) && alert.IsStarted(localtimeDateTime);
            if (!isActive)
            {
                return false;
            }

            var isActiveCampaign = CacheManager.IsActiveAlert(alert);
            if (!isActiveCampaign)
            {
                return false;
            }


            if (IsDesktopClientAndMobileAlert(alert, token) || IsMobileClientAndDesktopAlert(alert, token))
            {
                return false;
            }

            return true;
        }

        private bool IsValidAlert(Alert alert, Token token, DateTime localDateTime)
        {
            if (alert.Class != (int)AlertType.ScreenSaver && 
                alert.Class != (int)AlertType.Wallpaper &&
                alert.Class != (int)AlertType.LockScreen &&
                (CacheManager.IsReceivedAlertForUser(token.DistinguishedName, alert.Id) || CacheManager.IsReceivedAlertForUser("." + token.UserName, alert.Id)))
            {
                return false;
            }

            if (GetContentById(alert.Id).Terminated == 1)
            {
                return false;
            }

            return IsActiveAlert(alert, token, localDateTime);
        }

        private bool IsDesktopClientAndMobileAlert(Alert alert, Token token)
        {
            return (token.DeviceType == ClientDeviceType.MacClient || token.DeviceType == ClientDeviceType.WindowsClient) && alert.Device == Device.Mobile;
        }

        private bool IsMobileClientAndDesktopAlert(Alert alert, Token token)
        {
            return (token.DeviceType == ClientDeviceType.AndroidClient || token.DeviceType == ClientDeviceType.IosClient) && alert.Device == Device.Desktop;
        }

        private bool IsSimpleAlert(Alert alert)
        {
            return !Array.Exists(_badClasses, c => c == alert.Class);
        }

        public void AddContentRecipientConnectionToDb(long userId, Guid deskbarId, long alertId, int alertRecievedStatus)
        {
            var logger = LogManager.GetCurrentClassLogger();
            logger.Debug($"Start executing function DeskAlertsUtils.MarkAlertAsReceived with params userId={userId} deskbarId={deskbarId} alertId={alertId} requestLocalTime={DateTime.Now.ToString(DateTimeFormat.MsSqlFormat)}");
            using (var dbManager = new DBManager())
            {
                var alertParameter1 = SqlParameterFactory.Create(DbType.Int64, alertId, "alertId1");
                var userParameter1 = SqlParameterFactory.Create(DbType.Int32, userId, "userId1");
                var deskbarParameter1 = SqlParameterFactory.Create(DbType.String, deskbarId.ToString(), "deskbarId1");
                var alertParameter2 = SqlParameterFactory.Create(DbType.Int64, alertId, "alertId2");
                var userParameter2 = SqlParameterFactory.Create(DbType.Int32, userId, "userId2");
                var deskbarParameter2 = SqlParameterFactory.Create(DbType.String, deskbarId.ToString(), "deskbarId2");
                var alertReceiveStatusParameter = SqlParameterFactory.Create(DbType.Int32, alertRecievedStatus, "alertReceiveStatus");

                var insertAlertToReceivedQuery =
                    $@"SET LANGUAGE BRITISH;
                    IF NOT EXISTS (SELECT id FROM alerts_received WHERE user_id = @userId1 AND clsid = @deskbarId1 AND alert_id = @alertId1)
                    INSERT INTO alerts_received (user_id, clsid, alert_id, [date], status) 
                    VALUES (@userId2, @deskbarId2, @alertId2, '{DateTime.Now.ToString(DateTimeFormat.MsSqlFormat)}', @alertReceiveStatus)";

                lock (_alertReceivedRepositoryLock)
                {
                    dbManager.ExecuteQuery(insertAlertToReceivedQuery, alertParameter1, userParameter1,
                        deskbarParameter1, alertParameter2, userParameter2,
                        deskbarParameter2, alertReceiveStatusParameter);
                }
            }
        }
        #endregion
    }
}