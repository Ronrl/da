﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.ApplicationCore.Repositories;
using DeskAlerts.ApplicationCore.Utilities;
using DeskAlerts.Server.Utils.Interfaces;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Extentions;
using DeskAlertsDotNet.Parameters;
using Newtonsoft.Json;
using NLog;


namespace DeskAlertsDotNet.Utils.Services
{
    public class SkinService : ISkinService
    {
        private readonly ISkinRepository _skinRepository;

        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public SkinService()
        {
            var configurationManager = new WebConfigConfigurationManager(Config.Configuration);

            _skinRepository = new PpSkinRepository(configurationManager);
        }

        public IEnumerable<Skin> GetAllSkins()
        {
            return _skinRepository.GetAll();
        }

        public IEnumerable<Skin> GetSkinsByIdsList(IEnumerable<Guid> skinGuidList)
        {
            var guidList = skinGuidList.ToList();
            if (!guidList.Any())
            {
                _logger.Error("Skins GUIDs not found.");
                return new List<Skin>();
            }
            else
            {
                return _skinRepository.GetSkinsByGuidList(guidList);
            }
        }

        public IEnumerable<Skin> GetSkinsByPolicyId(long policyId)
        {
            if (policyId == default(long))
            {
                _logger.Error("Policy Id cannot be equal to default.");
                return new List<Skin>();
            }
            else
            {
                return _skinRepository.GetSkinsByPolicyId(policyId);
            }
        }

        public void DeleteByPolicyId(long policyId)
        {
            if (policyId <= default(long))
            {
                _logger.Error("Policy Id cannot be less or equal to default.");
                return;
            }

            _skinRepository.DeleteByPolicyId(policyId);
        }

        public void CreateDependencies(long policyId)
        {
            if (policyId <= default(long))
            {
                _logger.Error("Policy Id cannot be less or equal to default.");
                return;
            }

            _skinRepository.CreateDependencies(policyId);
        }

        public void CreateSpecificDependencies(long policyId, IEnumerable<Guid> skinsIds)
        {
            var enumerable = skinsIds.ToList();
            if (policyId <= default(long) || !enumerable.Any())
            {
                _logger.Error("Policy Id cannot be less or equal to default.");
                return;
            }

            _skinRepository.CreateSpecificDependencies(policyId, enumerable);
        }

        public Skin GetByAlertId(long alertId)
        {
            if (alertId <= default(long))
            {
                _logger.Error("Alert Id cannot be less or equal to default");
                return null;
            }

            Guid skinByAlertId;
            try
            {
                skinByAlertId = _skinRepository.GetSkinIdByAlertId(alertId);
            }
            catch (NullReferenceException ex)
            {
                return null;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

            return _skinRepository.GetById(skinByAlertId);
        }

        public string GetSkinsPreviewTemplate(IEnumerable<Guid> skinGuids)
        {
            var skinsByPolicy = GetSkinsByIdsList(skinGuids);

            const string defaultSkin = "Default";
            var result = GenerateSkinPreviewTemplate(defaultSkin, defaultSkin);

            foreach (var skin in skinsByPolicy)
            {
                result += GenerateSkinPreviewTemplate(skin.Name, skin.Id.ToString());
            }

            return result;
        }

        private static string GenerateSkinPreviewTemplate(string skinName, string skinGuid)
        {
            if (string.IsNullOrEmpty(skinName) || string.IsNullOrEmpty(skinGuid))
            {
                return string.Empty;
            }

            skinName = Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(skinName);
            skinGuid = skinGuid.TrimStart('{').TrimEnd('}');

            if (!string.Equals(skinGuid, "default", StringComparison.CurrentCultureIgnoreCase))
            {
                skinGuid = $"{{{skinGuid}}}";
            }

            return $"<div class=\"tile\" id=\"{skinName}\">" +
                   $"<div style=\"padding-bottom:5px;\"><span autofocus=\"\" style=\"margin:0px\" id=\"header_title\" class=\"header_title\">{skinName}</span></div>" +
                   $"<div style=\"font-size: 1px;\"><img id=\"skin_preview_img\" src=\"skins/{skinGuid}/thumbnail.png\"></div>" +
                   $"</div>";
        }

        public string GetSkinsFromDbByPolicyGuidList(IEnumerable<Guid> skinGuids)
        {
            var skinsByPolicy = GetSkinsByIdsList(skinGuids);

            var skinSet = ConvertSkinsToDataSet(skinsByPolicy);

            var settings = new JsonSerializerSettings
            {
                Converters = new JsonConverter[] { new DeskAlertsDataBaseJsonWorker() }
            };

            var skinsJson = JsonConvert.SerializeObject(skinSet, settings);

            return skinsJson;
        }

        private static DataSet ConvertSkinsToDataSet(IEnumerable<Skin> skinsByPolicy)
        {
            var dataSet = new DataSet();
            const string defaultSkinID = "default";
            var defaultSkinName = "Default";

            if (!string.IsNullOrEmpty(Settings.Content["DefaultSkinDisplayName"]))
            {
                defaultSkinName = Settings.Content["DefaultSkinDisplayName"];
            }

            var row = new Dictionary<string, object>
            {
                {"id", defaultSkinID},
                { "name", defaultSkinName}
            };

            var dataRow = new DataRow(row);
            dataSet.Add(dataRow);

            if (skinsByPolicy == null)
            {
                return dataSet;
            }

            foreach (var skin in skinsByPolicy)
            {
                row = new Dictionary<string, object>
                {
                    {"id", skin.Id},
                    { "name", skin.Name}
                };

                dataRow = new DataRow(row);
                dataSet.Add(dataRow);
            }

            return dataSet;
        }

        public string CheckSkinIdInPolicy(string currentSkinId, IEnumerable<Guid> curUserPolicySkinList)
        {
            const string skinDefaultId = "default";

            if (string.IsNullOrEmpty(currentSkinId) || curUserPolicySkinList == null)
            {
                return skinDefaultId;
            }
            
            var skinsByPolicy = GetSkinsByIdsList(curUserPolicySkinList);

            var regex = new Regex(@"([\w\d]{8})-([\w\d]{4})-([\w\d]{4})-([\w\d]{4})-([\w\d]{12})");

            var skinId = regex.IsMatch(currentSkinId) 
                ? Guid.Parse(currentSkinId).ToString() 
                : skinDefaultId;

            var loadedSkinsAllowed = skinsByPolicy.Any(x => x.Id.ToString().Equals(skinId, StringComparison.CurrentCultureIgnoreCase));
            if (loadedSkinsAllowed == false)
            {
                // If the current policy doesn't contain the skin ID used while duplication, the default skin ID is used. The default skin ID is defined in the code of the project.
                // TODO: Fix it with the default skin ID defined in the code of the project. For example:
                // skinId = skinsByPolicy.FirstOrDefault()?.Id.ToString();
                skinId = skinDefaultId;
            }

            if (!string.Equals(skinId, skinDefaultId, StringComparison.CurrentCultureIgnoreCase))
            {
                skinId = $"{{{skinId}}}";
            }

            return skinId;
        }
    }
}