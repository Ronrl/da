﻿using System;
using System.Globalization;
using System.Net;
using System.Net.Mail;
using System.Net.Sockets;
using System.Threading;
using DeskAlerts.ApplicationCore.Dtos;
using DeskAlerts.ApplicationCore.Dtos.Settings;
using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.Server.Utils.Helpers;
using DeskAlerts.Server.Utils.Interfactes;
using DeskAlerts.Server.Utils.Managers;
using NLog;
using Resources;

namespace DeskAlerts.Server.Utils.Services
{
    public class SettingsService : ISettingsService
    {
        public const string ApnsHost = "gateway.push.apple.com";
        public const int ApnsPort = 2195;

        public const string GcmHost = "android.googleapis.com";
        public const int GcmPort = 443;

        private ISettingsRepository _settingsRepository;
        private Logger _logger = LogManager.GetCurrentClassLogger();
        private EmailManager emailManager = new EmailManager();

        public SettingsService(ISettingsRepository settingsRepository)
        {
            _settingsRepository = settingsRepository;
        }

        public PushNotificatiosResponseDto GetPushNotificationResponse()
        {
            var isApnsConnected = CheckConnection(ApnsHost, ApnsPort);
            var isGcmConnected = CheckConnection(GcmHost, GcmPort);
            var response = new PushNotificatiosResponseDto()
            {
                GcmResponse = isGcmConnected ?
                "Ok: android.googleapis.com:443 connected" :
                "While sending alert at Android application, DA server have to send request at android.googleapis.com using 443 port so Google push server send push notification at user's device.",
                ApnsResponse = isApnsConnected ?
                "Ok: gateway.push.apple.com:2195 connected" :
                "While sending alert at iOS application, DA server have to send request at gateway.push.apple.com using 2195 port so Apple push server send push notification at user's device.",
                IsApnsConnected = isApnsConnected,
                IsGcmConnected = isGcmConnected
            };

            return response;
        }

        public DeskAlertsApiResponse<EmailResult> TestSmtpServerConnection(SmtpCredentialsDto credentialsDto)
        {
            emailManager.WriteSmtpLog("Trying to connect to server " + credentialsDto.SmtpServer + resources.SMTP_WITH_PORT + credentialsDto.SmtpPort);

            try
            {
                using (MailMessage message = new MailMessage())
                {
                    MailAddress fromAddress = new MailAddress(credentialsDto.SmtpFrom);

                    message.From = fromAddress;
                    message.Subject = "test";
                    message.IsBodyHtml = true;
                    message.Body = "test";
                    message.To.Add(credentialsDto.SmtpFrom);
                    var result = SendEmail(credentialsDto, message);
                    emailManager.WriteSmtpLog(result.Message);
                    emailManager.WriteSmtpLog(resources.SMTP_CONNECTION_ENDED);
                    return new DeskAlertsApiResponse<EmailResult>
                    {
                        Data = result
                    };
                }
            }
            catch (Exception e)
            {
               _logger.Error(e);

                return new DeskAlertsApiResponse<EmailResult>
                {
                    Data = EmailResult.Failed
                };
            }
        } 

        public EmailResult SendEmail(SmtpCredentialsDto credentialsDto, MailMessage message)
        {
            using (SmtpClient smtpClient = new SmtpClient())
            {
                smtpClient.Host = credentialsDto.SmtpServer;
                if (credentialsDto.SmtpAuthentication)
                {
                    smtpClient.UseDefaultCredentials = false;
                    smtpClient.Credentials = new NetworkCredential(credentialsDto.SmtpUserName,
                        credentialsDto.SmtpUserNamePassword);
                }
                else
                {
                    smtpClient.UseDefaultCredentials = true;
                }

                smtpClient.EnableSsl = credentialsDto.SmtpSsl;

                try
                {
                    smtpClient.Send(message);
                    
                    return new EmailResult()
                    {
                        StatusCode = SmtpStatusCode.Ok,
                        Message = resources.SMTP_SERVER_AVAILABLE
                    };
                }
                catch (Exception ex)
                {
                    _logger.Debug(ex);
                    string innerExceptionLog = (ex.InnerException != null)
                        ? ex.InnerException.Message
                        : "";
                    return new EmailResult()
                    {
                        StatusCode = SmtpStatusCode.TransactionFailed,
                        Message = ex.Message + " " + innerExceptionLog,
                    };
                }
            }
        }

        public string DateTimeExample(string dateFormat, string timeFormat)
        {
            if (string.IsNullOrEmpty(dateFormat))
            {
                throw new ArgumentNullException(nameof(dateFormat));
            }

            if (string.IsNullOrEmpty(timeFormat))
            {
                throw new ArgumentNullException(nameof(timeFormat));
            }

            var format = DateTimeConverter.GetDateTimeFormat(dateFormat, timeFormat);
            return DateTime.Now.ToString(format, CultureInfo.InvariantCulture);
        }

        public string GetLanguageByUserId(int userId)
        {
            var userLang = _settingsRepository.GetStringByInt(userId);
            if (string.IsNullOrEmpty(userLang))
            {
                userLang = _settingsRepository.GetByName("ConfDefaultLanguage").value;
            }

            return userLang;
        }

        public void SetCultureInfoForThread(string cultureInfo)
        {
            var lang = new CultureInfo(cultureInfo);
            Thread.CurrentThread.CurrentUICulture = lang;
            Thread.CurrentThread.CurrentCulture = lang;
        }

        public bool isTrial() {
            return !_settingsRepository.GetByName("supermario").value.Equals("0");
        }

        private bool CheckConnection(string host, int port)
        {
            using (var socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp))
            {
                try
                {
                    socket.Connect(host, port);
                }
                catch (SocketException ex)
                {
                    if (ex.SocketErrorCode == SocketError.ConnectionRefused ||
                        ex.SocketErrorCode == SocketError.HostNotFound ||
                        ex.SocketErrorCode == SocketError.AccessDenied)
                    {
                        return false;
                    }
                }
                return true;
            }
        }
    }
}