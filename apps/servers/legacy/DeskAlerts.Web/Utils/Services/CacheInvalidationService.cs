﻿using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Models.ContentTypes;
using DeskAlerts.ApplicationCore.Utilities;
using DeskAlerts.ContentDeliveringService.Interfaces;
using DeskAlerts.Server.Utils.Interfaces.Services;
using NLog;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;

namespace DeskAlerts.Server.Utils.Services
{
    /// <summary>
    /// Cache invalidation class on all servers. 
    /// </summary>
    public class CacheInvalidationService : ICacheInvalidationService
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly IContentDeliveringService<AlertContentType> _alertContentDeliveringService;
        private readonly IContentDeliveringService<TickerContentType> _tickerContentDeliveringService;
        private readonly IContentDeliveringService<RsvpContentType> _rsvpContentDeliveringService;
        private readonly IContentDeliveringService<SurveyContentType> _surveyContentDeliveringService;
        private readonly IContentDeliveringService<ScreensaverContentType> _screensaversContentDeliveringService;
        private readonly IContentDeliveringService<WallpaperContentType> _wallpapersContentDeliveringService;
        private readonly IContentDeliveringService<VideoAlertContentType> _videoContentDeliveringService;
        private readonly IContentDeliveringService<TerminatedContentType> _terminatedContentDeliveringService;
        private readonly IContentDeliveringService<LockscreenContentType> _lockscreensContentDeliveringService;

        /// <summary>
        /// The class constructor.
        /// </summary>
        /// <param name="alertContentDeliveringService">Used IoC.</param>
        /// <param name="tickerContentDeliveringService">Used IoC.</param>
        /// <param name="rsvpContentDeliveringService">Used IoC.</param>
        /// <param name="surveyContentDeliveringService">Used IoC.</param>
        /// <param name="screensaversContentDeliveringService">Used IoC.</param>
        /// <param name="wallpapersContentDeliveringService">Used IoC.</param>
        /// <param name="videoContentDeliveringService">Used IoC.</param>
        /// <param name="terminatedContentDeliveringService">Used IoC.</param>
        public CacheInvalidationService(
            IContentDeliveringService<AlertContentType> alertContentDeliveringService,
            IContentDeliveringService<TickerContentType> tickerContentDeliveringService,
            IContentDeliveringService<RsvpContentType> rsvpContentDeliveringService,
            IContentDeliveringService<SurveyContentType> surveyContentDeliveringService,
            IContentDeliveringService<ScreensaverContentType> screensaversContentDeliveringService,
            IContentDeliveringService<WallpaperContentType> wallpapersContentDeliveringService,
            IContentDeliveringService<VideoAlertContentType> videoContentDeliveringService,
            IContentDeliveringService<TerminatedContentType> terminatedContentDeliveringService,
            IContentDeliveringService<LockscreenContentType> lockscreensContentDeliveringService)
        {
            _alertContentDeliveringService = alertContentDeliveringService;
            _tickerContentDeliveringService = tickerContentDeliveringService;
            _rsvpContentDeliveringService = rsvpContentDeliveringService;
            _surveyContentDeliveringService = surveyContentDeliveringService;
            _screensaversContentDeliveringService = screensaversContentDeliveringService;
            _wallpapersContentDeliveringService = wallpapersContentDeliveringService;
            _videoContentDeliveringService = videoContentDeliveringService;
            _terminatedContentDeliveringService = terminatedContentDeliveringService;
            _lockscreensContentDeliveringService = lockscreensContentDeliveringService;
        }

        /// <summary>
        /// Call <see cref="DeskAlerts.ContentDeliveringService"/> at all servers.
        /// </summary>
        /// <param name="contentType">DeskAlerts Content type <see cref="AlertType"/>.</param>
        /// <param name="contentId">Content identifier.</param>
        public void CacheInvalidation(AlertType contentType, long contentId)
        {
            if (contentId <= 0)
            {
                throw new ArgumentException(nameof(contentId));
            }

            LocalCacheInvalidation(contentType, contentId);

            SiblingsCacheInvalidation(contentType, contentId);
        }

        /// <summary>
        /// Call <see cref="DeskAlerts.ContentDeliveringService"/> at current server.
        /// </summary>
        /// <param name="contentType">DeskAlerts Content type <see cref="AlertType"/>.</param>
        /// <param name="contentId">Content identifier.</param>
        public void LocalCacheInvalidation(AlertType contentType, long contentId)
        {
            switch (contentType)
            {
                case AlertType.Undefined:
                    break;
                case AlertType.SimpleAlert:
                    _alertContentDeliveringService.DeliveryToRecipientsOfContent(contentId);
                    _tickerContentDeliveringService.DeliveryToRecipientsOfContent(contentId);
                    _rsvpContentDeliveringService.DeliveryToRecipientsOfContent(contentId);
                    break;
                case AlertType.Ticker:
                    _tickerContentDeliveringService.DeliveryToRecipientsOfContent(contentId);
                    break;
                case AlertType.Rsvp:
                    _rsvpContentDeliveringService.DeliveryToRecipientsOfContent(contentId);
                    break;
                case AlertType.SimpleSurvey:
                case AlertType.SurveyQuiz:
                case AlertType.SurveyPoll:
                    _surveyContentDeliveringService.DeliveryToRecipientsOfContent(contentId);
                    break;
                case AlertType.ScreenSaver:
                    _screensaversContentDeliveringService.DeliveryToRecipientsOfContent(contentId);
                    break;
                case AlertType.Wallpaper:
                    _wallpapersContentDeliveringService.DeliveryToRecipientsOfContent(contentId);
                    break;
                case AlertType.VideoAlert:
                    _videoContentDeliveringService.DeliveryToRecipientsOfContent(contentId);
                    break;
                case AlertType.Terminated:
                    _terminatedContentDeliveringService.DeliveryToRecipientsOfContent(contentId);
                    break;
                case AlertType.RssAlert:
                    break;
                case AlertType.LockScreen:
                    _lockscreensContentDeliveringService.DeliveryToRecipientsOfContent(contentId);
                    break;
                case AlertType.RssFeed:
                    break;
                case AlertType.DigitalSignature:
                    break;
                case AlertType.EmergencyAlert:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(contentType), contentType, null);
            }
        }

        /// <summary>
        /// Call <see cref="DeskAlerts.ContentDeliveringService"/> at siblings.
        /// </summary>
        /// <param name="contentType">DeskAlerts Content type <see cref="AlertType"/>.</param>
        /// <param name="contentId">Content identifier.</param>
        public void SiblingsCacheInvalidation(AlertType contentType, long contentId)
        {
            var servers = AppConfiguration.GetServerSiblings.ToList();
            if (!servers.Any())
            {
                return;
            }

            const string invalidateCacheUrl = "api/v1/cache/invalidation";
            var uriParams = $"contentType={contentType}&contentId={contentId}";

            using (var client = CreateHttpClient())
            {
                foreach (var server in servers)
                {
                    var httpRequestMessage = new HttpRequestMessage
                    {
                        RequestUri = CreateCorrectUri($"{server}/{invalidateCacheUrl}?{uriParams}"),
                        Method = HttpMethod.Get
                    };

                    var response = client.SendAsync(httpRequestMessage).Result;

                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        Logger.Error(response.Content);
                    }
                }
            }
        }

        /// <summary>
        /// Create <see cref="HttpClient"/> instance for call <see cref="DeskAlerts.ContentDeliveringService"/>.
        /// </summary>
        /// <returns><see cref="HttpClient"/> instance.</returns>
        private static HttpClient CreateHttpClient()
        {
            var client = new HttpClient
            {
                Timeout = new TimeSpan(0, 0, 30)
            };

            return client;
        }

        /// <summary>
        /// Creating valid http url for sibling servers.
        /// </summary>
        /// <param name="uri">Sibling servers url.</param>
        /// <returns>Siblings url as <see cref="Uri"/></returns>
        public static Uri CreateCorrectUri(string uri)
        {
            try
            {
                if (string.IsNullOrEmpty(uri))
                {
                    throw new ArgumentNullException(nameof(uri));
                }

                var decodedUri = HttpUtility.HtmlDecode(uri);

                var isWallUri = Uri.IsWellFormedUriString(decodedUri, UriKind.Absolute);
                if (!isWallUri)
                {
                    throw new UriFormatException(nameof(decodedUri));
                }

                var isCreate = Uri.TryCreate(decodedUri, UriKind.Absolute, out var result);

                if (!isCreate)
                {
                    throw new UriFormatException(nameof(uri));
                }

                return result;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                throw;
            }
        }
    }
}