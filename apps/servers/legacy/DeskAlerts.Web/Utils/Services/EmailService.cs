﻿using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.Server.Utils.Interfaces.Services;
using DeskAlerts.Server.Utils.Interfactes;
using DeskAlertsDotNet.Models;
using System.Collections.Generic;
using System.Linq;

namespace DeskAlerts.Server.Utils.Services
{
    public class EmailService : IEmailService
    {
        private readonly IAlertReceivedRepository _alertReceivedRepository;
        private readonly IAlertRepository _alertRepository;
        private readonly IUserService _userService;

        public EmailService(IAlertReceivedRepository alertReceivedRepository, IAlertRepository alertRepository, IUserService userService)
        {
            _alertReceivedRepository = alertReceivedRepository;
            _alertRepository = alertRepository;
            _userService = userService;
        }

        public IEnumerable<UserAlertsDTO> GetApprovedEmailAlerts()
        {
            var userAlertsDtoList = new List<UserAlertsDTO>();

            var alertReceivedList = _alertReceivedRepository
                .GetUserAndAlertIdToEmailSend()
                .ToList();

            var usersList = alertReceivedList
                .Select(x => x.UserId)
                .Distinct();

            foreach (var userId in usersList)
            {
                var userDomainName = _userService.GetUserDomainNameByUserId(userId);
                var userName = _userService.JoinUserNameAndDomainName(_userService.GetUserById(userId).Username, userDomainName);

                var alertsIds = alertReceivedList
                    .Where(x => x.UserId == userId)
                    .Select(x => x.AlertId);

                var alerts = new List<Alert>();

                foreach (var alertsId in alertsIds)
                {
                    var alert = new Alert(_alertRepository.GetById(alertsId));
                    alerts.Add(alert);
                }

                var userAlertsDto = new UserAlertsDTO
                {
                    UserName = userName,
                    Alerts = alerts
                };

                userAlertsDtoList.Add(userAlertsDto);
            }

            return userAlertsDtoList;
        }
    }
}