﻿namespace DeskAlertsDotNet.Utils.Services
{
    using DeskAlerts.ApplicationCore.Entities;
    using DeskAlerts.ApplicationCore.Interfaces;
    using DeskAlerts.ApplicationCore.Repositories;
    using DeskAlerts.ApplicationCore.Utilities;
    using DeskAlerts.Server.Utils.Interfactes;
    using Parameters;
    using NLog;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class SsoService : ISsoService
    {
        private IGroupRepository _groupRepository;

        private IDomainRepository _domainRepository;

        private IPolicyRepository _policyRepository;

        private Logger _logger = LogManager.GetCurrentClassLogger();

        private string _locationPath;

        private string _siteName;

        public SsoService(IGroupRepository groupRepository, IDomainRepository domainRepository, IPolicyRepository policyRepository)
        {
            _groupRepository = groupRepository;
            _domainRepository = domainRepository;
            _policyRepository = policyRepository;
            _siteName = Config.Data.GetString("site");
            _locationPath = _siteName + "/" + Config.Data.GetString("alertsDir").Split('/').Last();
        }

        public List<Policy> GetPoliciesByPublisherIdAndDomainId(long userId, long domainId)
        {
            return _policyRepository.GetPoliciesByUserIdAndDomainId(userId, domainId);
        }

        public List<Group> GetGroupsWithPolicyAndDomain()
        {
            try
            {
                return _groupRepository.GetGroupsWithDomain().ToList();
            }
            catch (Exception e)
            {
                _logger.Log(LogLevel.Error, e);
                throw;
            }
        }

        public bool IsAuthorized(string userName, string domainName)
        {
            try
            {
                var domain = _domainRepository.GetByName(domainName);

                if (domain == default(Domain))
                {
                    throw new Exception($"Can't get '{domainName}' domain!");
                }

                _logger.Info($"Finded domain {domain.Name}");
                var groups = _groupRepository.GetGroupByDomainIdMappedWithUser(domain.Id, userName).ToList();

                if (groups.Count > 0)
                {
                    _logger.Info($"User {domainName}\\{userName} was authorized with AD SSO");
                    return true;
                }

                return false;
            }
            catch (Exception e)
            {
                _logger.Info($"User {domainName}\\{userName} was not authorized with AD SSO");
                return false;
            }
        }

        public IEnumerable<Group> GetGroupsByPublisherIdAndDomainId(long userId, long domainId)
        {
            return _groupRepository.GetGroupByDomainIdAndUserIdMappedWithUser(domainId, userId);
        }

        public IEnumerable<Group> GetGroupsByPublisherId(long userId)
        {
            return _groupRepository.GetGroupsPublisherId(userId);
        }
    }
}