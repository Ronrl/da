﻿using System;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.ApplicationCore.Repositories;
using DeskAlerts.ApplicationCore.Utilities;
using DeskAlerts.Server.Utils.Interfactes;
using DeskAlertsDotNet.Parameters;

namespace DeskAlerts.Server.Utils.Services
{
    public class ComputerService : IComputerService
    {
        private readonly IComputerRepository _computerRepository;
        private readonly IDomainRepository _domainRepository;
        
        private static readonly object ComputerRepositoryLock = new object();

        public ComputerService()
        {
            var configurationManager = new WebConfigConfigurationManager(Config.Configuration);

            _computerRepository = new PpComputerRepository(configurationManager);
            _domainRepository = new PpDomainRepository(configurationManager);
        }

        public void UpdateComputerLastRequest(string name, string domainName)
        {
            if (string.IsNullOrEmpty(domainName))
            {
                return;
            }

            long computerDomainId;
            try
            {
                computerDomainId = _domainRepository.GetByName(domainName).Id;
            }
            catch (Exception e)
            {
                return;
            }

            lock (ComputerRepositoryLock)
            {
                var computerEntity = _computerRepository.GetByNameAndDomainId(name, computerDomainId);
                if (computerEntity == null)
                {
                    return;
                }

                computerEntity.LastRequest = DateTime.Now;
                _computerRepository.Update(computerEntity);
            }
        } 
    }
}