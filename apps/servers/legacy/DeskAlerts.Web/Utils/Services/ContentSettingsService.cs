﻿using System.Collections.Generic;
using System.Linq;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.ApplicationCore.Repositories;
using DeskAlerts.ApplicationCore.Utilities;
using DeskAlerts.Server.Utils.Interfaces;
using DeskAlertsDotNet.Parameters;
using NLog;

namespace DeskAlerts.Server.Utils.Services
{
    public class ContentSettingsService : IContentSettingsService
    {
        private readonly IContentSettingsRepository _contentSettingsRepository;

        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public ContentSettingsService()
        {
            var configurationManager = new WebConfigConfigurationManager(Config.Configuration);

            _contentSettingsRepository = new PpContentSettingsRepository(configurationManager);
        }

        public IEnumerable<ContentSettings> GetAllContentSettings()
        {
            var result = _contentSettingsRepository.GetAll();

            return result;
        }

        public IEnumerable<ContentSettings> GetContentSettingsByPublisherId(long policyId)
        {
            if (policyId <= default(long))
            {
                _logger.Error("Policy Id cannot be equal to default.");
                return new List<ContentSettings>();
            }
            else
            {
                return _contentSettingsRepository.GetContentSettingsByPolicyId(policyId);
            }
        }

        public void DeleteByPolicyId(long policyId)
        {
            if (policyId <= default(long))
            {
                _logger.Error("Policy Id cannot be less or equal to default.");
                return;
            }

            _contentSettingsRepository.DeleteByPolicyId(policyId);
        }

        public void CreateDependencies(long policyId)
        {
            if (policyId <= default(long))
            {
                _logger.Error("Policy Id cannot be less or equal to default.");
                return;
            }

            _contentSettingsRepository.CreateDependencies(policyId);
        }

        public void CreateSpecificDependencies(long policyId, IEnumerable<long> contentOptionIdsList)
        {
            var optionIdsList = contentOptionIdsList.ToList();
            if (policyId <= default(long) || !optionIdsList.Any())
            {
                _logger.Error("Policy Id cannot be less or equal to default.");
                return;
            }

            _contentSettingsRepository.CreateSpecificDependencies(policyId, optionIdsList);
        }
    }
}