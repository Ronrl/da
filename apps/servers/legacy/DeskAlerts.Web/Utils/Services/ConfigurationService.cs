﻿namespace DeskAlertsDotNet.Utils.Services
{
    using DeskAlerts.ApplicationCore.Entities;
    using DeskAlerts.ApplicationCore.Interfaces;
    using Microsoft.Web.Administration;
    using NLog;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class ConfigurationService : IConfigurationService
    {
        public string LocationPath { get; private set; }

        private Logger _logger = LogManager.GetCurrentClassLogger();
        private readonly string _siteName;
        private readonly string _alertsDir;
        private const string _pathToAnonymousAuthentication = "system.webServer/security/authentication/anonymousAuthentication";
        private const string _pathToWindowsAuthentication = "system.webServer/security/authentication/windowsAuthentication";
        private const string _pathToAuthorization = "system.webServer/security/authorization";
        private const string Https = "https://";
        private const string Http = "http://";

        public ConfigurationService(string site, string alertsDir)
        {
            _siteName = site;
            _alertsDir = alertsDir;
            LocationPath = GetLocationPath();
        }

        public string GetLocationPath()
        {
            var subsiteName = _alertsDir.StartsWith(Https) ? _alertsDir.Substring(Https.Length) : _alertsDir.Substring(Http.Length);
            var subsites = subsiteName.Split('/');
            var isSubsite = subsites.Count() > 1 && !string.IsNullOrEmpty(subsites[1]);
            return isSubsite ? $"{_siteName }/{subsites[1]}/admin" : $"{_siteName }/admin";
        }

        public void UpdateAuthentication(bool isWindowsAuthEnabled)
        {
            try
            {
                using (ServerManager serverManager = new ServerManager())
                {
                    ChangeWindowsAuthentication(serverManager.GetApplicationHostConfiguration(), isWindowsAuthEnabled);
                    _logger.Info($"Windows authorization was {(isWindowsAuthEnabled ? "enabled" : "disabled")}");
                    serverManager.CommitChanges();
                    _logger.Info($"Configuration of site {LocationPath} was updated");
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }

        public void SetGroups(IEnumerable<Group> groups)
        {
            try
            {
                using (ServerManager serverManager = new ServerManager())
                {
                    var config = serverManager.GetApplicationHostConfiguration();
                    var authorizationSection = config.GetSection("system.webServer/security/authorization", LocationPath);
                    var authorizationCollection = authorizationSection.GetCollection();
                    authorizationCollection.Clear();

                    foreach (var group in groups)
                    {
                        var domain = group.Domain.Name.ToUpper();
                        var userName = $"{domain}\\{group.Name}";
                        if (!authorizationCollection.Any(x => x["roles"].ToString() == userName))
                        {
                            var addElement = authorizationCollection.CreateElement("add");
                            addElement["accessType"] = @"Allow";
                            addElement["roles"] = userName;
                            authorizationCollection.Add(addElement);
                            _logger.Info($"Group was {group.Name} added to configuration");
                        }
                    }

                    serverManager.CommitChanges();
                    _logger.Info($"Configuration of site {LocationPath} was updated");
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }

        private void ChangeWindowsAuthentication(Configuration config, bool isWinAuthEnabled)
        {
            config.GetSection(_pathToAnonymousAuthentication, LocationPath)["enabled"] = !isWinAuthEnabled;
            config.GetSection(_pathToWindowsAuthentication, LocationPath)["enabled"] = isWinAuthEnabled;
            _logger.Info($"Authentication has been changed from {(isWinAuthEnabled ? "Windows" : "Anonymous")} to {(isWinAuthEnabled ? "Anonymous" : "Windows")} in section {LocationPath + _pathToWindowsAuthentication}");
            if (isWinAuthEnabled)
            {
                SetConfigurationSection(config, "images");
                SetConfigurationSection(config, "skins");
                SetConfigurationSection(config, "jscripts");
                SetConfigurationSection(config, "preview");
                SetConfigurationSection(config, "css");
                _logger.Info($"Anonymous authorization was added to admin folder");
            }
            else
            {
                AddAnonymousAuth(config, LocationPath);
                _logger.Info($"Anonymous users authorization was added to {LocationPath}");
            }
        }

        private void SetConfigurationSection(Configuration config, string path)
        {
            var locationPath = $"{LocationPath}/{path}";
            config.GetSection(_pathToAnonymousAuthentication, locationPath)["enabled"] = true;
            config.GetSection(_pathToWindowsAuthentication, locationPath)["enabled"] = false;
            AddAnonymousAuth(config, locationPath);
        }

        private void AddAnonymousAuth(Configuration config, string locationPath)
        {
            var authorizationSection = config.GetSection(_pathToAuthorization, locationPath);
            var authorizationCollection = authorizationSection.GetCollection();
            authorizationCollection.Clear();
            var addElement = authorizationCollection.CreateElement("add");
            addElement["accessType"] = @"Allow";
            addElement["users"] = "?";
            authorizationCollection.Add(addElement);
        }

        public void RestartSite()
        {
            try
            {
                using (ServerManager serverManager = new ServerManager())
                {
                    var config = serverManager.GetApplicationHostConfiguration();
                    var site = serverManager.Sites[_siteName];
                    site.Stop();
                    site.Start();
                    _logger.Info($"Site {LocationPath} was restarted");
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }
    }
}