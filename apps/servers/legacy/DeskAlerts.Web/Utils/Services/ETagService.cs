﻿using DeskAlerts.ApplicationCore.Models.ContentTypes;
using DeskAlerts.ApplicationCore.Utilities;
using DeskAlerts.ContentDeliveringService.Interfaces;
using DeskAlerts.ContentDeliveringService.Models;
using DeskAlerts.Server.Utils.Interfaces.Services;
using System.Collections.Generic;

namespace DeskAlerts.Server.Utils.Services
{
    public class ETagService : IETagService
    {
        private readonly IContentDeliveringService<AlertContentType> _alertContentDeliveringService;
        private readonly IContentDeliveringService<TickerContentType> _tickerContentDeliveringService;
        private readonly IContentDeliveringService<RsvpContentType> _rsvpContentDeliveringService;
        private readonly IContentDeliveringService<SurveyContentType> _surveyContentDeliveringService;
        private readonly IContentDeliveringService<ScreensaverContentType> _screensaverContentDeliveringService;
        private readonly IContentDeliveringService<WallpaperContentType> _wallpaperContentDeliveringService;
        private readonly IContentDeliveringService<LockscreenContentType> _lockscreenContentDeliveringService;
        private readonly IContentDeliveringService<VideoAlertContentType> _videoAlertContentDeliveringService;
        private readonly IContentDeliveringService<TerminatedContentType> _terminatedContentDeliveringService;

        public ETagService(
            IContentDeliveringService<AlertContentType> alertContentDeliveringService,
            IContentDeliveringService<TickerContentType> tickerContentDeliveringService,
            IContentDeliveringService<RsvpContentType> rsvpContentDeliveringService,
            IContentDeliveringService<SurveyContentType> surveyContentDeliveringService,
            IContentDeliveringService<ScreensaverContentType> screensaverContentDeliveringService,
            IContentDeliveringService<WallpaperContentType> wallpaperContentDeliveringService,
            IContentDeliveringService<LockscreenContentType> lockscreenContentDeliveringService,
            IContentDeliveringService<VideoAlertContentType> videoAlertContentDeliveringService,
            IContentDeliveringService<TerminatedContentType> terminatedContentDeliveringService
            )
        {
            _alertContentDeliveringService = alertContentDeliveringService;
            _tickerContentDeliveringService = tickerContentDeliveringService;
            _rsvpContentDeliveringService = rsvpContentDeliveringService;
            _surveyContentDeliveringService = surveyContentDeliveringService;
            _screensaverContentDeliveringService = screensaverContentDeliveringService;
            _wallpaperContentDeliveringService = wallpaperContentDeliveringService;
            _lockscreenContentDeliveringService = lockscreenContentDeliveringService;
            _videoAlertContentDeliveringService = videoAlertContentDeliveringService;
            _terminatedContentDeliveringService = terminatedContentDeliveringService;
        }

        public string GetETag(ContentDeliveringKey key)
        {
            var ip = string.Empty;
            var compName = string.Empty;

            var list = new List<string>
            {
                _alertContentDeliveringService.Get(key, ip, compName),
                _tickerContentDeliveringService.Get(key, ip, compName),
                _rsvpContentDeliveringService.Get(key, ip, compName),
                _surveyContentDeliveringService.Get(key, ip, compName),
                _screensaverContentDeliveringService.Get(key, ip, compName),
                _wallpaperContentDeliveringService.Get(key, ip, compName),
                _lockscreenContentDeliveringService.Get(key, ip, compName),
                _videoAlertContentDeliveringService.Get(key, ip, compName),
                _terminatedContentDeliveringService.Get(key, ip, compName)
            };

            var contentsString = string.Join(",", list);

            var result = HashingUtils.MD5(contentsString);

            return result;
        }
    }
}