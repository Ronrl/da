﻿using System;
using DeskAlerts.ApplicationCore.Interfaces;

namespace DeskAlerts.Server.Utils.Services
{
    public class ScheduledContentService : IScheduledContentService
    {
        private readonly IScheduledContentRepository _repeatableContentRepository;
        private readonly IAlertReceivedRepository _alertReceivedRepository;
        private readonly IAlertSentStatRepository _alertSentRepository;

        public ScheduledContentService(
            IScheduledContentRepository repeatableContentRepository,
            IAlertReceivedRepository alertReceivedRepository, 
            IAlertSentStatRepository alertSentRepository
            )
        {
            _repeatableContentRepository = repeatableContentRepository;
            _alertReceivedRepository = alertReceivedRepository;
            _alertSentRepository = alertSentRepository;
        }

        public long CreateInstanceFromTemplate(long templateId)
        {
            if (templateId <= 0)
            {
                throw new ArgumentException();
            }

            var currentDateTime = DateTime.Now;

            // Get template from repository 
            var template = _repeatableContentRepository.GetById(templateId);
            if (template == null || !template.IsPepeatableTemplate)
            {
                throw new ArgumentException($"Template with id {templateId} not found"); 
            }

            // Create new instance by template with actual parameters
            var instance = template;
            instance.IsPepeatableTemplate = false;
            instance.ParentTemplateId = templateId;
            instance.FromDate = template.FromDate.HasValue
                ? DateTime.Today.Add(template.FromDate.Value.TimeOfDay)
                : currentDateTime;
            instance.ToDate = template.ToDate.HasValue
                ? DateTime.Today.Add(template.ToDate.Value.TimeOfDay)
                : instance.FromDate.Value.AddDays(1);

            // Save instance to db
            var instanceId = _repeatableContentRepository.Create(template);

            // Save relations with recipients to 'alerts_received' table
            var alertReceivedEntities = _alertReceivedRepository.GetAlertsReceivedForContentExtended(templateId);
            foreach (var entity in alertReceivedEntities)
            {
                entity.AlertId = instanceId;
                _alertReceivedRepository.Create(entity);
            }

            // Save relations with recipients to 'alerts_sent' table (for correct statistic)
            var alertSentEntities = _alertSentRepository.GetAllByContentId(templateId);
            foreach (var entity in alertSentEntities)
            {
                entity.AlertId = instanceId;
                _alertSentRepository.Create(entity);
            }

            // Update 'CreateDate' property of template (crutch for convenient display in the list of sent content)
            _repeatableContentRepository.UpdateSentDateForTemplate(templateId, currentDateTime);

            return instanceId;
        }

        public bool DeleteInstancesOfTemplate(long templateId)
        {
            try
            {
                _repeatableContentRepository.DeleteInstancesOfTemplate(templateId);
            }
            catch
            {
                return false;
            }

            return true;
        }
    }
}