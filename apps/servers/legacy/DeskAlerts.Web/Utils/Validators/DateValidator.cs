﻿using DeskAlertsDotNet.Parameters;
using System;

namespace DeskAlertsDotNet.Utils
{
    using Resources;

    public static class DateValidator
    {
        /// <summary>
        /// Validation Statuses.
        /// The "undefined" is introduced for cases when it will be impossible say the result.
        /// Default 0: undefined result.
        /// Element 1: Correct dates.
        /// Element 2: Stop date less then current date time.
        /// Element 3: Stop date less then start date time.
        /// </summary>
        public enum ValidateDateStatus
        {
            Undefined = 0,
            CorrectDates = 1,
            IncorrectDateTime = 2,
            DateStopLessThenCurrentDate = 3,
            DateStopLessThenStartDate = 4
        }

        /// <summary>
        /// Validation of dates in sheduller.
        /// Return validation status: <see cref="ValidateDateStatus"/>.
        /// </summary>
        /// <param name="dateTimeStart">Start date of the period.</param>
        /// <param name="dateTimeStop">Stop date of the period.</param>
        /// <returns>Validation status.</returns>
        public static ValidateDateStatus ValidateDate(DateTime dateTimeStart, DateTime dateTimeStop)
        {
            var currentDateTimeWidthSeconds = DateTime.Now.AddHours(-1); // AddHour -1: to give time for publisher to fill fields.
            var currentDateTime = new DateTime(currentDateTimeWidthSeconds.Year,
                                                    currentDateTimeWidthSeconds.Month,
                                                    currentDateTimeWidthSeconds.Day,
                                                    currentDateTimeWidthSeconds.Hour,
                                                    currentDateTimeWidthSeconds.Minute,
                                                    0);
            var result = ValidateDateStatus.CorrectDates;

            if (dateTimeStart > DateTime.MinValue && dateTimeStart < DateTime.MaxValue &&
                dateTimeStop > DateTime.MinValue && dateTimeStop < DateTime.MaxValue)
            {
                if (dateTimeStop > currentDateTime)
                {
                    if (dateTimeStop < dateTimeStart)
                    {
                        result = ValidateDateStatus.DateStopLessThenStartDate;
                    }
                }
                else
                {
                    result = ValidateDateStatus.DateStopLessThenCurrentDate;
                }
            }
            else
            {
                result = ValidateDateStatus.IncorrectDateTime;
            }

            return result;
        }

        /// <summary>
        /// Validation of dates (past time intervals are considered as valid)
        /// Return validation status: <see cref="ValidateDateStatus"/>.
        /// </summary>
        /// <param name="dateTimeStart">Start date of the period.</param>
        /// <param name="dateTimeStop">Stop date of the period.</param>
        /// <returns>Validation status.</returns>
        public static ValidateDateStatus ValidateDateWithPast(DateTime dateTimeStart, DateTime dateTimeStop)
        {
            if (dateTimeStart <= DateTime.MinValue || 
                dateTimeStart >= DateTime.MaxValue ||
                dateTimeStop <= DateTime.MinValue || 
                dateTimeStop >= DateTime.MaxValue)
            {
                return ValidateDateStatus.IncorrectDateTime;
            }

            return dateTimeStop < dateTimeStart
                ? ValidateDateStatus.DateStopLessThenStartDate
                : ValidateDateStatus.CorrectDates;
        }

        /// <summary>
        /// Parse validate status to return error string or emptry string for correct date.
        /// </summary>
        /// <param name="validateDateStatus">Validatation Status <see cref="ValidateDateStatus"/></param>
        /// <returns>String for show message.</returns>
        public static string ParseValidateStatus(ValidateDateStatus validateDateStatus)
        {
            string result = string.Empty;

            if (validateDateStatus != ValidateDateStatus.CorrectDates)
            {
                switch (validateDateStatus)
                {
                    case ValidateDateStatus.DateStopLessThenCurrentDate:
                        result = resources.LNG_TO_DATE_LESS_THAN_CURRENT_DATE;
                        break;

                    case ValidateDateStatus.DateStopLessThenStartDate:
                        result = resources.LNG_REC_DATE_ERROR;
                        break;

                    default:
                        result = resources.LNG_DATE_VALUE_IS_INCORRECT;
                        break;
                }
            }

            return result;
        }
    }
}