﻿using System;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.ApplicationCore.Repositories;
using DeskAlerts.ApplicationCore.Utilities;
using DeskAlerts.Server.Utils.Interfactes;
using DeskAlerts.Server.Utils.Services;
using DeskAlertsDotNet.Managers;
using DeskAlertsDotNet.Parameters;
using DeskAlertsDotNet.sever.STANDARD.Utils;

namespace DeskAlerts.Server.Utils.HttpModules
{
    public class AuthCheckModule : IHttpModule
    {
        private IConfigurationManager _configurationManager;

        private IReferralsRepository _referralsRepository;

        public String ModuleName
        {
            get { return "AuthCheckModule"; }
        }

        private const string AuthPagePath = "~/admin/Auth.aspx";

        private readonly string[] _pages =
        {
            "Auth.aspx", "GetAlerts.aspx", "GetAlert.aspx", "GetWallpaper.aspx", "Vote.aspx", "GetSurvey.aspx",
            "PreviewCaption.aspx", "RegenerateCache.aspx", "ShowCache.aspx", "functions_inc.aspx", "ApplePush.aspx",
            "PoolIdentiryError.aspx", "DomainHelper.aspx", "RegisterForm.aspx", "UploadFileAction.aspx",
            "UploadFile.aspx", "SurveyAnswersStatistic.aspx", "Preview.aspx", "RssSender.aspx", "StandBy.aspx",
            "Modules.aspx", "CreateCustomGroup.aspx", "AlertLangs.aspx", "TerminateAlert.aspx", "PreviewBody.aspx",
            "GetAlertData.aspx", "api", "MultipleUpdate.aspx"
        };

        public void Init(HttpApplication application)
        {
            application.PostMapRequestHandler += application_PostMapRequestHandler;
            application.PostAcquireRequestState += application_PostAcquireRequestState;
            application.PreRequestHandlerExecute += application_PreRequestHandlerExecute;
        }

        private bool IsServiсePage(string uri)
        {
            if (uri.Contains("da_errors"))
            {
                return true;
            }

            if (IsDigitalSignage(uri))
            {
                return true;
            }

            return _pages.Count(x => uri.ToLower().IndexOf(x.ToLower(), StringComparison.Ordinal) > -1) > 0;
        }

        private void SetUserCulture() {

            _configurationManager = new WebConfigConfigurationManager(Config.Configuration);
            ISettingsRepository settingsRepo= new PpSettingsRepository(_configurationManager);
            ISettingsService  settingsService= new SettingsService(settingsRepo);
             var userLang = settingsService.GetLanguageByUserId(UserManager.Default.CurrentUser.Id);
            settingsService.SetCultureInfoForThread(userLang);
        }

        private bool IsDigitalSignage(string uri)
        {
            var chunks = uri.Split('/');
            var digSignIndex = uri.EndsWith("/") ? 3 : 2;
            var digSign = chunks[chunks.Length - digSignIndex];
            var isDigitalSignage = digSign == "ds";
            return isDigitalSignage;
        }

        private void application_PreRequestHandlerExecute(object sender, EventArgs e)
        {
            HttpApplication app = (HttpApplication)sender;

            if (app.Context.Session == null || IsServiсePage(app.Context.Request.Url.AbsoluteUri))
            {
                return;
            }

            var token = app.Context.Session["token"];
            if (token != null && UserManager.Default.CurrentUser != null)
            {
                var tokenGuid = (Guid)token;
                bool userAuthorized = UserManager.Default.IsAuthorized(tokenGuid);
                bool tokenAuthEnabled = Settings.Content["ConfTokenAuthEnable"] == "1";
                bool currentUserIsAdmin = UserManager.Default.CurrentUser.IsAdmin;
                SetUserCulture();
                if (tokenAuthEnabled && !currentUserIsAdmin)
                {

                    if (!userAuthorized)
                    {
                        var cookie = app.Context.Request.Cookies["desk_token"];
                        if (cookie != null)
                        {
                            var tokenTools = new TokenTools();
                            tokenTools.DeleteToken(cookie.Value);
                        }
                        app.Context.Response.Redirect(AuthPagePath);
                    }

                    if (userAuthorized)
                    {
                        var cookie = app.Context.Request.Cookies["desk_token"];
                        if (cookie == null)
                        {
                            Logout(app, tokenGuid);
                        }
                        else
                        {
                            var tokenjwt = cookie.Value;
                            if (!IsJwtTokenValid(tokenjwt))
                            {
                                Logout(app, tokenGuid);
                            }
                        }
                    }
                }
            }
            else
            {
                var referralEmail = app.Context.Request["ref"];

                if (!string.IsNullOrEmpty(referralEmail))
                {
                    _configurationManager = new WebConfigConfigurationManager(Config.Configuration);
                    _referralsRepository = new PpReferralsRepository(_configurationManager);

                    var exist = _referralsRepository.GetAll().Any(r => r.Email == referralEmail);

                    if (!exist)
                    {
                        _referralsRepository.Create(new Referral { Email = referralEmail, VisitDate = DateTime.Now });
                    }
                }

                app.Context.Response.Redirect(AuthPagePath);
            }
        }

        private static void Logout(HttpApplication app, Guid guid)
        {
            UserManager.Default.Unregister(guid);
            app.Context.Session.Clear();
            app.Context.Session.Abandon();
            app.Context.Response.Redirect(AuthPagePath);
        }

        private bool IsJwtTokenValid(string token)
        {
            bool result = false;
            var tokenTools = new TokenTools();

            int? userId = UserManager.Default.CurrentUser?.Id;

            if (userId != null)
            {
                tokenTools.CheckTokensCountForUser(userId.Value);
            }
            else
            {
                throw new UnauthorizedAccessException();
            }

            if (!tokenTools.IsTokenExists(token))
            {
                return false;
            }

            if (!tokenTools.IsTokenExpired(token))
            {
                result = true;
            }

            if (result == false)
            {
                tokenTools.DeleteToken(token);
            }

            return result;
        }

        private void application_PostAcquireRequestState(object sender, EventArgs e)
        {
            HttpContext context = ((HttpApplication)sender).Context;
            MyHttpHandler resourceHttpHandler = context.Handler as MyHttpHandler;
            if (resourceHttpHandler != null)
                context.Handler = resourceHttpHandler.OriginalHandler;
        }

        private void application_PostMapRequestHandler(object sender, EventArgs e)
        {
            HttpContext context = ((HttpApplication)sender).Context;

            if (context.Handler is IReadOnlySessionState || context.Handler is IRequiresSessionState)
                return;

            context.Handler = new MyHttpHandler(context.Handler);
        }

        public void Dispose()
        {
        }
    }

    public class MyHttpHandler : IHttpHandler, IReadOnlySessionState
    {
        internal readonly IHttpHandler OriginalHandler;

        public MyHttpHandler(IHttpHandler originalHandler)
        {
            OriginalHandler = originalHandler;
        }

        public void ProcessRequest(HttpContext context)
        {
            throw new InvalidOperationException("MyHttpHandler cannot process requests.");
        }

        public bool IsReusable
        {
            get { return false; }
        }
    }
}