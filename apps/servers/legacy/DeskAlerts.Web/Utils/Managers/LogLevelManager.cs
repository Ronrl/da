﻿using DeskAlerts.ApplicationCore.Utilities;
using DeskAlertsDotNet.Parameters;
using NLog;
using NLog.Config;

namespace DeskAlerts.Server.Utils.Managers
{
    public class LogLevelManager
    {
        private const string TraceLogLevelConfigName = "ConfTraceLogLevelEnabled";
        private const string InfoLogLevelConfigName = "ConfInfoLogLevelEnabled";
        private const string DebugLogLevelConfigName = "ConfDebugLogLevelEnabled";
        private const string ErrorLogLevelConfigName = "ConfErrorLogLevelEnabled";
        private const string FatalLogLevelConfigName = "ConfFatalLogLevelEnabled";

        public static void ConfigureLogingRulesFromSettings()
        {
            foreach (var rule in LogManager.Configuration.LoggingRules)
            {
                ConfigureLoggingRule(rule, TraceLogLevelConfigName, LogLevel.Trace);
                ConfigureLoggingRule(rule, InfoLogLevelConfigName, LogLevel.Info);
                ConfigureLoggingRule(rule, DebugLogLevelConfigName, LogLevel.Debug);
                ConfigureLoggingRule(rule, ErrorLogLevelConfigName, LogLevel.Error);
                ConfigureLoggingRule(rule, FatalLogLevelConfigName, LogLevel.Fatal);
            }

            LogManager.ReconfigExistingLoggers();
        }

        public static void ConfigureLoggingDbConnectionSettings()
        {
            LogManager.Configuration.Variables["daConnectionString"] = AppConfiguration.ConnectionString;
            LogManager.ReconfigExistingLoggers();
        }

        private static void ConfigureLoggingRule(LoggingRule rule, string logLevelConfigName, LogLevel logLevel)
        {
            if (Settings.Content[logLevelConfigName] == "1")
            {
                rule.EnableLoggingForLevel(logLevel);
            }
            else
            {
                rule.DisableLoggingForLevel(logLevel);
            }
        }
    }
}