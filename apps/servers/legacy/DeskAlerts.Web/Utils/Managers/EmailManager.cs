﻿using System;
using System.IO;
using System.Web;
using System.Net.Mail;
using DeskAlerts.ApplicationCore.Dtos;
using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.ApplicationCore.Repositories;
using DeskAlerts.ApplicationCore.Utilities;
using DeskAlerts.Server.Utils.Interfactes;
using DeskAlerts.Server.Utils.Services;
using DeskAlertsDotNet.Pages;
using DeskAlertsDotNet.Parameters;
using Resources;

namespace DeskAlerts.Server.Utils.Managers
{
    public class EmailResult
    {
        public static EmailResult Ok = new EmailResult()
        {
            StatusCode = SmtpStatusCode.Ok,
            Message = string.Empty
        };

        public static EmailResult Failed = new EmailResult()
        {
            StatusCode = SmtpStatusCode.TransactionFailed,
            Message = resources.SMTP_SENDING_FAILED
        };

        public SmtpStatusCode StatusCode { get; set; }
        public string Message { get; set; }
    }

    public class EmailManager
    {
        private static EmailManager _instance;
        

        public static EmailManager Default
        {
            get
            {
                if (_instance == null)
                    return _instance = new EmailManager();

                return _instance;
            }
        }
        
        public EmailResult Send(string title, string text, string to)
        {
            var emailSettings = EmailModuleSettings.GetSettingsFromDataBase();

            var mail = new MailMessage
            {
                From = new MailAddress(emailSettings.EmailLogin, emailSettings.EmailFrom),
                Subject = DeskAlertsUtils.ExtractTextFromHtml(title),
                IsBodyHtml = true,
                Body = HttpUtility.HtmlDecode(text)
            };

            var emails = to.Split(',');

            foreach (var email in emails)
            {
                if (!string.IsNullOrEmpty(email))
                {
                    mail.To.Add(email);
                }
            }
            
            var emailSettingsFromDataBase = EmailModuleSettings.GetSettingsFromDataBase();
            SmtpCredentialsDto smtpCredentialsDto = new SmtpCredentialsDto
            {
                SmtpServer = emailSettingsFromDataBase.EmailServer,
                SmtpPort = emailSettingsFromDataBase.EmailPort,
                SmtpSsl = emailSettingsFromDataBase.EnableSsl,
                SmtpAuthentication = emailSettingsFromDataBase.EnableAuth,
                SmtpFrom = emailSettingsFromDataBase.EmailFrom,
                SmtpUserNamePassword = emailSettingsFromDataBase.EmailPassword,
                SmtpUserName = emailSettingsFromDataBase.EmailLogin,
                SmtpConnectionTimeout = emailSettingsFromDataBase.TimeOut.ToString()

            };
            IConfigurationManager _configurationManager = new WebConfigConfigurationManager(Config.Configuration);
            ISettingsRepository _settingsRepository = new PpSettingsRepository(_configurationManager);
            ISettingsService _settingsService = new SettingsService(_settingsRepository);

            var result = _settingsService.SendEmail(smtpCredentialsDto, mail);
            
            if (result.StatusCode == SmtpStatusCode.Ok)
            {
                WriteSmtpLog(resources.SMTP_SUCCESSFUL_SENDING + mail.To + resources.SMTP_WITH_PORT + smtpCredentialsDto.SmtpPort);
            }
            else
            {
                WriteSmtpLog(result.Message);
            }

            WriteSmtpLog(resources.SMTP_CONNECTION_ENDED);
            return result;
        }
        
        public void WriteSmtpLog(string errorLog)
        {
            var enableSmtpLogging = Settings.Content["ConfEnableSmtpLogging"];
            try
            {
                string logString = DateTime.Now + resources.SMTP_SEND + errorLog;
                string path = Config.Data.GetString("alertsFolder") + "\\SMTPlogs\\";
                if (enableSmtpLogging == "1")
                {
                    Directory.CreateDirectory(path);
                    File.AppendAllLines(path + "\\SmtpLogs.txt", new[] { logString });
                }
            }
            catch (Exception ex)
            {
                DeskAlertsBasePage.Logger.Error(ex.Message);
                DeskAlertsBasePage.Logger.Debug(ex);
            }
        }
    }
    

    public class EmailModuleSettings
    {

        public string EmailFrom { get; set; }
        public string EmailServer { get; set; }
        public int EmailPort { get; set; }
        public string EmailLogin { get; set; }
        public string EmailPassword { get; set; }
        public bool EnableSsl { get; set; }
        public bool EnableAuth { get; set; }
        public int TimeOut { get; set; }


        public static EmailModuleSettings GetSettingsFromDataBase()
        {
            var emailSettings = new EmailModuleSettings
            {
                EmailFrom = Settings.Content["ConfSmtpFrom"].Trim(),
                EmailServer = Settings.Content["ConfSmtpServer"].Trim(),
                EnableSsl = Settings.Content.GetInt("ConfSmtpSsl") == 1,
                EnableAuth = Settings.Content.GetInt("ConfSmtpAuth") == 1,
            };
            emailSettings.EmailPort = Settings.Content.GetInt("ConfSmtpPort");
            emailSettings.EmailLogin = Settings.Content["ConfSmtpUser"].Trim();
            emailSettings.EmailPassword = Settings.Content["ConfSmtpPassword"].Trim();
            emailSettings.TimeOut = Settings.Content.GetInt("ConfSmtpTimeout");

            return emailSettings;
        }

        public bool IsValid => (!string.IsNullOrEmpty(EmailFrom) && !string.IsNullOrEmpty(EmailServer) && EmailPort > 0) ||
                               (!string.IsNullOrEmpty(EmailServer) && EmailPort > 0 && EnableAuth == false);



    }
}