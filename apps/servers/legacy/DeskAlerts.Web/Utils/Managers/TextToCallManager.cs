﻿namespace DeskAlerts.Server.Utils.Managers
{
    using System;

    using DeskAlertsDotNet.Parameters;

    using NLog;

    using Twilio;
    using Twilio.Http;
    using Twilio.Rest.Api.V2010.Account;
    using Twilio.Types;

    public class TextToCallManager
    {
        private readonly string _accountSid = Settings.Content["ConfTextToCallSID"];
        private readonly string _authToken = Settings.Content["ConfTextToCallAuthToken"];
        private readonly string _fromNumber = Settings.Content["ConfTextToCallNumber"];

        private static string _twiMLBaseUrl = Config.Data.GetString("alertsDir") + "/api/v1/TextToCall/xml/";

        private static TextToCallManager _instance;

        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public static TextToCallManager Default => _instance ?? (_instance = new TextToCallManager());

        public CallResource Send(int alertId, string recipientMobileNumber)
        {
            try
            {
                TwilioClient.Init(_accountSid, _authToken);

                var to = new PhoneNumber(recipientMobileNumber);
                var from = new PhoneNumber(_fromNumber);
                var twiMLUrl = _twiMLBaseUrl + alertId;

                return CallResource.Create(to, from, url: new Uri(twiMLUrl), method: HttpMethod.Get);
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }
    }

    public static class TwiMLGenerator
    {   
        public static string GetTWiMLFromAlertBody(string alertBody)
        {
            string result = $"<?xml version =\'1.0\' encoding=\'UTF-8\'?><Response><Say  voice=\'woman\' language=\'en-GB\' loop=\'2\'>Attention! You have new notification. {alertBody}</Say></Response>";
            return result;
        }
    }
}