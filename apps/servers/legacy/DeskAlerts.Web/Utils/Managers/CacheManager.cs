﻿using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Enums;
using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.ApplicationCore.Repositories;
using DeskAlerts.ApplicationCore.Utilities;
using DeskAlerts.Server.Models.Cache;
using DeskAlerts.Server.Utils.Interfactes;
using DeskAlertsDotNet.Application;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Models;
using DeskAlertsDotNet.Models.Cache;
using DeskAlertsDotNet.Parameters;
using DeskAlertsDotNet.Utils.Factories;
using DeskAlertsDotNet.Utils.Services;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Caching;
using System.Runtime.InteropServices;
using System.Text;
using Alert = DeskAlertsDotNet.Models.Alert;

namespace DeskAlerts.Server.Utils.Managers
{
    public enum CacheTypes
    {
        CacheUsers = 0,
        CacheComps = 1,
        CacheIps = 2,
        CacheRecievedAlerts = 6,
    }

    public class AlertInstancePair
    {
        public Alert Alert { get; set; }

        public String InstanceId { get; set; }

        public AlertInstancePair(Alert alert, String deskbar)
        {
            Alert = alert;
            InstanceId = deskbar.TrimStart('{').TrimEnd('}').ToLower();
        }
    }

    [ComVisible(true)]
    [ClassInterface(ClassInterfaceType.AutoDual)]
    [ProgId("DeskAlerts.CacheManager")]
    [Guid("9CC99E14-8A5B-46A6-9B0E-D779094350D8")]
    public class CacheManager
    {
        private const int ApprovedAlertStatusIndex = 1;
        private const string UsersCacheName = "usersCache";
        private const string CompsCacheName = "compNamesCache";
        private const string IpCacheName = "ipsCache";
        private const string RecievedCacheName = "recievedCache";
        private const string DomainCacheName = "domainsCache";

        private readonly string _selectReceivedAlertsQuery = $@"
        SELECT ar.alert_id,
            user_id,
            from_date,
            to_date,
            clsid AS deskbar,
            u.name AS username,
            d.name AS domain
        FROM alerts_received AS ar
            JOIN alerts AS a ON ar.alert_id = a.id
            JOIN users AS u ON ar.user_id = u.id
            LEFT JOIN domains AS d ON d.id = u.domain_id
            LEFT JOIN recurrence AS r ON a.id = r.alert_id
        WHERE(to_date > @Now
            OR YEAR(to_date) = 1900)
            AND (ar.date > DATEADD(d, 0, DATEDIFF(d, 0, @Now))
                OR r.id IS NULL)
            AND [status] =  {(int)AlertReceiveStatus.Received}";

        // Format this veeeery carefully (look to the method 'Add')
        private string SelectActualAlertsForRecipientsQuery = @"
        SELECT a.id,
            a.title,
            a.type2,
            a.class,
            a.urgent,
            a.ticker,
            a.ticker_speed AS tickerspeed,
            a.device,
            a.fullscreen,
            a.sent_date AS sentdate,
            a.sender_id AS senderid,
            a.aknown,
            a.lifetime,
            CASE
               WHEN CAST(a.caption_id AS VARCHAR(36)) IS NULL
               THEN 'default'
               ELSE CAST(a.caption_id AS VARCHAR(36))
            END AS captionid,
            a.alert_width AS width,
            a.alert_height AS height,
            a.resizable,
            a.autoclose,
            a.self_deletable AS selfdeletable,
            a.create_date AS createdate,
            a.schedule,
            a.schedule_type,
            a.email,
            a.sms,
            a.text_to_call AS texttocall,
            a.approve_status,
            a.from_date AS fromdate,
            a.to_date AS todate,
            d.name as user_domain,  
            u.name as username,
            sm.id AS surveyid,
			u.name as username,
			d.name as user_domain,
			ui.clsid as deskbar,
            a.campaign_id AS campaignid
        FROM alerts AS a
            LEFT JOIN surveys_main AS sm ON a.id = sm.sender_id
			JOIN alerts_sent AS als ON als.alert_id = a.id
		    JOIN users AS u ON u.id = als.user_id
			LEFT JOIN domains AS d ON u.domain_id = d.id
			JOIN user_instances as ui ON ui.user_id = u.id
        WHERE a.to_date > @Now
            AND a.from_date < @Now
            AND (a.schedule = 0 OR a.schedule = 1 AND a.[recurrence] = 0)
            AND a.schedule_type = 1
            AND a.class <> 4
			AND a.type2 != 'B'";

        // Format this veeeery carefully (look to the method 'Add')
        private const string SelectActualAlertsForBroadcastQuery =
            @"SELECT a.id,
                a.title,
                a.type2,
                a.class,
                a.urgent,
                a.ticker,
                a.ticker_speed AS tickerspeed,
                a.device,
                a.fullscreen,
                a.sent_date AS sentdate,
                a.sender_id AS senderid,
                a.aknown,
                a.approve_status,
                a.lifetime,
                CASE
                    WHEN CAST(a.caption_id AS VARCHAR(36)) IS NULL
                    THEN 'default'
                    ELSE CAST(a.caption_id AS VARCHAR(36))
                END AS captionid,
                a.alert_width AS width,
                a.alert_height AS height,
                a.resizable,
                a.autoclose,
                a.self_deletable AS selfdeletable,
                a.create_date AS createdate,
                a.schedule,
                a.schedule_type,
                a.email,
                a.sms,
                a.text_to_call as texttocall,
                a.from_date AS fromdate,
                a.to_date AS todate,                
                'broadcast' AS username,
	            s.id as surveyid,
                a.campaign_id AS campaignid
          FROM alerts AS a
                LEFT JOIN surveys_main AS s ON a.id = s.sender_id
          WHERE a.to_date > @Now
                AND a.from_date < @Now
                AND (a.schedule = 0 OR a.schedule = 1 AND a.[recurrence] = 0)
                AND a.schedule_type = 1
                AND a.type2 = 'B'
                AND a.class <> 4";
        
        // TODO LIPCHANSKIY Проверить корректность работы этого запроса
        private const string SelectActualScreensaversAndWallpapersForIpsQuery =
            @"SELECT a.id,
            a.title,
            a.type2,
            a.class,
            a.urgent,
            a.ticker,
            a.ticker_speed AS tickerspeed,
            a.device,
            a.fullscreen,
            a.sent_date AS sentdate,
            a.sender_id AS senderid,
            a.aknown,
            a.lifetime,
            CASE
               WHEN CAST(a.caption_id AS VARCHAR(36)) IS NULL
               THEN 'default'
               ELSE CAST(a.caption_id AS VARCHAR(36))
            END AS captionid,
            a.alert_width AS width,
            a.alert_height AS height,
            a.resizable,
            a.autoclose,
            a.self_deletable AS selfdeletable,
            a.create_date AS createdate,
            a.schedule,
            a.schedule_type,
            a.email,
            a.sms,
            a.text_to_call AS texttocall,
            a.approve_status,
            a.from_date AS fromdate,
            a.to_date AS todate,
            sm.id as surveyid,
            a.campaign_id AS campaignid,
            ip.ip_from,
			ip.ip_to
        FROM alerts AS a
            LEFT JOIN surveys_main AS sm ON a.id = sm.sender_id
            INNER JOIN alerts_sent_iprange  as ip ON ip.alert_id = a.id
        WHERE a.from_date < @Now
            AND a.to_date > @Now
            AND (a.schedule = 0 OR a.schedule = 1 AND a.[recurrence] = 0)
            AND a.schedule_type = 1
            AND a.class <> 4
			AND a.type2 = 'I'";

        private static MemoryCache _usersCache;
        private static MemoryCache _ipsCache;
        private static MemoryCache _compNamesCache;
        private static MemoryCache _domainsCache;
        private static MemoryCache _receivedAlertsCache;

        private static IEnumerable<Campaign> _activeCampaigns;
        private static IEnumerable<Campaign> _nearestCampaigns;

        private readonly ICampaignRepository _campaignRepository;
        private readonly IUserService _userService;

        private readonly CacheItemPolicy _policy;
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        private static readonly object RebuildCacheLock = new object();

        public CacheManager()
        {
            IConfigurationManager configurationManager = new WebConfigConfigurationManager(Config.Configuration);
            _campaignRepository = new PpCampaignRepository(configurationManager);
            IGroupRepository groupRepository = new PpGroupRepository(configurationManager);
            IUserRepository userRepository = new PpUserRepository(configurationManager);
            _userService = new UserService(userRepository, groupRepository);
            _usersCache = _usersCache ?? new MemoryCache(UsersCacheName);
            _ipsCache = _ipsCache ?? new MemoryCache(IpCacheName);
            _compNamesCache = _compNamesCache ?? new MemoryCache(CompsCacheName);
            _domainsCache = _domainsCache ?? new MemoryCache(DomainCacheName);
            _receivedAlertsCache = _receivedAlertsCache ?? new MemoryCache(RecievedCacheName);
            _policy = new CacheItemPolicy();
        }

        public void RebuildCache(string time)
        {
            if (AppConfiguration.IsNewContentDeliveringScheme)
            {
                return;
            }

            lock (RebuildCacheLock)
            {
                _logger.Debug($"RebuildCache: {time}. Global.IsCacheManagerLoaded: {Global.IsCacheManagerLoaded}.");
                Global.IsCacheManagerLoaded = false;
                try
                {
                    _usersCache.Dispose();
                    _ipsCache.Dispose();
                    _compNamesCache.Dispose();
                    _domainsCache.Dispose();
                    _receivedAlertsCache.Dispose();

                    _usersCache = new MemoryCache(UsersCacheName);
                    _ipsCache = new MemoryCache(IpCacheName);
                    _compNamesCache = new MemoryCache(CompsCacheName);
                    _domainsCache = new MemoryCache(DomainCacheName);
                    _receivedAlertsCache = new MemoryCache(RecievedCacheName);

                    using (var manager = new DBManager())
                    {
                        manager.ExecuteQuery("SET LANGUAGE BRITISH");
                        FillReceivedAlertsCache();

                        var actualAlertsForRecipients = GetDataSetOfAlerts(SelectActualAlertsForRecipientsQuery);
                        var actualAlertsForBroadcast = GetDataSetOfAlerts(SelectActualAlertsForBroadcastQuery);
                        var actualScreensaversAndWallpapersForIps =
                            GetDataSetOfAlerts(SelectActualScreensaversAndWallpapersForIpsQuery);

                        var scheduledUsersAlertsDataSet = manager.GetDataByQuery(
                            $"EXEC getNearestHourAlerts '{time}', @recipients = 'users', @terminated = 0");
                        var scheduledComputersAlertsDataSet = manager.GetDataByQuery(
                            $"EXEC getNearestHourAlerts '{time}', @recipients = 'computers', @terminated = 0");
                        var scheduledIpsAlertsDataSet = manager.GetDataByQuery(
                            $"EXEC getNearestHourAlerts '{time}', @recipients = 'ips', @terminated = 0");

                        UpdateNearestCampaigns();
                        ExcludeInactiveCampaigns(actualAlertsForRecipients);
                        ExcludeInactiveCampaigns(actualAlertsForBroadcast);
                        ExcludeInactiveCampaigns(scheduledUsersAlertsDataSet);
                        ExcludeInactiveCampaigns(scheduledComputersAlertsDataSet);
                        ExcludeInactiveCampaigns(scheduledIpsAlertsDataSet);
                        ExcludeInactiveCampaigns(actualScreensaversAndWallpapersForIps);

                        BuildCacheFromDataSet(actualAlertsForRecipients, ref _usersCache, CacheTypes.CacheUsers);
                        BuildCacheFromDataSet(actualAlertsForBroadcast, ref _usersCache, CacheTypes.CacheUsers);
                        BuildCacheFromDataSet(scheduledUsersAlertsDataSet, ref _usersCache, CacheTypes.CacheUsers);
                        BuildCacheFromDataSet(scheduledComputersAlertsDataSet, ref _compNamesCache, CacheTypes.CacheComps);
                        BuildCacheFromDataSet(scheduledIpsAlertsDataSet, ref _ipsCache, CacheTypes.CacheIps);
                        BuildCacheFromDataSet(actualScreensaversAndWallpapersForIps, ref _ipsCache, CacheTypes.CacheIps);
                    }
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                }
                finally
                {
                    Global.IsCacheManagerLoaded = true;
                    var dateTimeNow = DateTime.Now.ToString(DeskAlertsDotNet.Utils.Consts.DateTimeFormat.MsSqlFormat, CultureInfo.InvariantCulture);
                    _logger.Debug($"RebuildCache are finished: {dateTimeNow}. Global.IsCacheManagerLoaded: {Global.IsCacheManagerLoaded}.");
                }
            }
        }

        public void AddAlertsToReceivedCache(string key, int alertId)
        {
            _logger.Info($"AddAlertsToReceived key:{key} alertId:{alertId}");

            try
            {
                key = key.ToLower();
                if (!(_receivedAlertsCache.Get(key) is List<int> receivedIds))
                {
                    receivedIds = new List<int>();
                }

                if (!receivedIds.Contains(alertId))
                {
                    receivedIds.Add(alertId);
                }

                _receivedAlertsCache.Set(key, receivedIds, _policy);

                // Send to siblings
                var siblingsList = Config.Data.GetServerCompanions();
                var alertCache = new ReceivedAlertCache
                {
                    AlertId = alertId,
                    Key = key
                };
                SendReceivedCacheToSiblings(siblingsList, alertCache);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }

        /// <summary>
        /// Check if specified alert is cached.
        /// </summary>
        /// <param name="alertId">Alert id.</param>
        /// <returns></returns>
        public bool IsAlertCached(long alertId)
        {
            _logger.Debug($"Start checking if alert {alertId} is cached.");

            var receivedAlertsCacheInstance = _receivedAlertsCache.FirstOrDefault();

            var cachedAlertsList = (List<long>)receivedAlertsCacheInstance.Value;

            bool result = cachedAlertsList.Contains(alertId);
            return result;
        }

        public bool IsReceivedAlertForUser(string key, int alertId)
        {
            return _receivedAlertsCache.Get(key.ToLower()) is List<int> receivedIds && receivedIds.Contains(alertId);
        }

        // This method is smells
        public bool IsRecieved(int alertId)
        {
            var recievedLists = _receivedAlertsCache.Select(kvp => kvp.Value).ToList();

            foreach (List<int> list in recievedLists)
            {
                if (list.Where(id => id == alertId).Count() > 0)
                {
                    return true;
                }
            }

            return false;
        }

        public void RemoveAlertForKey(string key, long alertId, CacheTypes type)
        {
            key = key.ToLower();
            MemoryCache cache = null;

            switch (type)
            {
                case CacheTypes.CacheUsers:
                    {
                        cache = _usersCache;
                        break;
                    }

                case CacheTypes.CacheIps:
                    {
                        cache = _ipsCache;
                        break;
                    }

                case CacheTypes.CacheComps:
                    {
                        cache = _compNamesCache;
                        break;
                    }

                case CacheTypes.CacheRecievedAlerts:
                    {
                        cache = _receivedAlertsCache;
                        break;
                    }

            }


            if (cache != null)
            {
                var cacheValueObject = cache.Get(key);
                var alertCacheList = (List<AlertInstancePair>)cacheValueObject;

                if (alertCacheList != null)
                {
                    alertCacheList.RemoveAll(a => a.Alert.Id == alertId);

                    if (alertCacheList.Count == 0)
                    {
                        cache.Remove(key);
                    }
                    else
                    {
                        cache.Set(key, alertCacheList, _policy);
                    }
                }
            }
            else
            {
                throw new Exception("Cahce is null!");
            }
        }

        [ComVisible(true)]
        public void Update(int alertId, string date)
        {
            Delete(alertId, date);
            Add(alertId, date);
        }

        public void UpdateActiveCampaigns()
        {
            _activeCampaigns = _campaignRepository.GetAll().Where(x => x.Status == CampaignStatus.Active);
        }

        public void UpdateNearestCampaigns()
        {
            _nearestCampaigns = _campaignRepository.GetNearestCampaigns();
        }

        public bool IsActiveAlert(DataSet users, DataSet computers, DataSet IPs)
        {
            var isUsersAlert = false;
            var isComputersAlert = false;
            var isIPsAlert = false;

            if (!users.IsEmpty)
            {
                var alert = new Alert(users.First());
                isUsersAlert = alert.CampaignId > 0 ? _nearestCampaigns.Any(x => x.Id == alert.CampaignId) : true;
            }

            if (!computers.IsEmpty)
            {
                var alert = new Alert(computers.First());
                isComputersAlert = alert.CampaignId > 0 ? _nearestCampaigns.Any(x => x.Id == alert.CampaignId) : true;
            }

            if (!IPs.IsEmpty)
            {
                var alert = new Alert(IPs.First());
                isIPsAlert = alert.CampaignId > 0 ? _nearestCampaigns.Any(x => x.Id == alert.CampaignId) : true;
            }

            return isUsersAlert || isComputersAlert || isIPsAlert;
        }

        public bool IsActiveAlert(Alert alert)
        {
            return alert.CampaignId < 0 || _activeCampaigns.Any(x => x.Id == alert.CampaignId);
        }

        public void ExcludeInactiveCampaigns(DataSet dataSet)
        {
            foreach (var dataRow in dataSet)
            {
                var alert = new Alert(dataRow);
                if (alert.CampaignId > 0 && !_nearestCampaigns.Any(x => x.Id == alert.CampaignId))
                {
                    dataSet.Remove(dataRow);
                }
            }
        }

        [ComVisible(true)]
        public void Delete(long alertId, string date)
        {
            RemoveAlertsFromCache(alertId, CacheTypes.CacheUsers);
            RemoveAlertsFromCache(alertId, CacheTypes.CacheComps);
            RemoveAlertsFromCache(alertId, CacheTypes.CacheIps);

            var siblingsList = Config.Data.GetServerCompanions();
            if (siblingsList.Length > 0)
            {
                var alertCache = new AlertCache { AlertId = alertId, Date = date };
                DeleteCacheFromSiblings(siblingsList, alertCache);
            }
        }

        public List<Alert> GetAlertsForIp(List<long> ipList)
        {
            var alerts = new List<Alert>();
            var cacheKeys = _ipsCache.Select(kvp => kvp.Key).ToList();

            foreach (var key in cacheKeys)
            {
                var fromIp = Convert.ToInt64(key.Split('-')[0]);
                var toIp = Convert.ToInt64(key.Split('-')[1]);

                foreach (var ip in ipList)
                {
                    if (fromIp <= ip && ip <= toIp)
                    {
                        var alertsPart = (List<AlertInstancePair>)_ipsCache.Get(key);

                        if (alertsPart != null)
                        {
                            foreach (var alertInstancePair in alertsPart)
                            {
                                alerts.Add(alertInstancePair.Alert);
                            }
                        }
                    }
                }
            }

            alerts.RemoveAll(a => a.Approve_Status != (int)ApproveStatus.ApprovedByModerator && a.Approve_Status != (int)ApproveStatus.ApprovedByDefault);
            return alerts;
        }

        public static List<Alert> GetAlertsForIp(long ipList)
        {
            var alerts = new List<Alert>();
            var cacheKeys = _ipsCache.Select(kvp => kvp.Key).ToList();

            foreach (var key in cacheKeys)
            {
                var fromIp = Convert.ToInt64(key.Split('-')[0]);
                var toIp = Convert.ToInt64(key.Split('-')[1]);

                if (fromIp <= ipList && ipList <= toIp)
                {
                    var alertsPart = (List<AlertInstancePair>)_ipsCache.Get(key);

                    if (alertsPart != null)
                    {
                        foreach (var alertInstancePair in alertsPart)
                        {
                            alerts.Add(alertInstancePair.Alert);
                        }
                    }
                }
            }

            alerts.RemoveAll(a => a.Approve_Status != (int)ApproveStatus.ApprovedByModerator && a.Approve_Status != (int)ApproveStatus.ApprovedByDefault);
            return alerts;
        }

        public List<Alert> GetAlertsByCacheType(CacheTypes cacheType)
        {
            var alerts = new List<Alert>();
            List<string> cacheKeys;

            switch (cacheType)
            {
                case CacheTypes.CacheUsers:
                    cacheKeys = _usersCache.Select(kvp => kvp.Key).ToList();
                    foreach (var key in cacheKeys)
                    {
                        var alertsPart = _usersCache.Get(key) as List<Alert>;
                        alerts.AddRange(alertsPart.AsEnumerable());
                    }

                    break;
                case CacheTypes.CacheComps:
                    cacheKeys = _compNamesCache.Select(kvp => kvp.Key).ToList();
                    foreach (var key in cacheKeys)
                    {
                        var alertsPart = _compNamesCache.Get(key) as List<Alert>;
                        alerts.AddRange(alertsPart.AsEnumerable());
                    }

                    break;
                case CacheTypes.CacheIps:
                    cacheKeys = _ipsCache.Select(kvp => kvp.Key).ToList();
                    foreach (var key in cacheKeys)
                    {
                        var alertsPart = _ipsCache.Get(key) as List<Alert>;
                        alerts.AddRange(alertsPart.AsEnumerable());
                    }

                    break;
                case CacheTypes.CacheRecievedAlerts:
                    cacheKeys = _receivedAlertsCache.Select(kvp => kvp.Key).ToList();
                    foreach (var key in cacheKeys)
                    {
                        var alertsPart = _receivedAlertsCache.Get(key) as List<Alert>;
                        alerts.AddRange(alertsPart.AsEnumerable());
                    }

                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(cacheType), cacheType, null);
            }

            return alerts;
        }

        public List<Alert> GetAlertsForComputer(string name, string domain = null)
        {
            var key = name.ToLower();

            if (domain != null)
            {
                key = domain.ToLower() + "." + name.ToLower();
            }

            var vals = _compNamesCache.Get(key);
            var result = vals == null ? new List<AlertInstancePair>() : (List<AlertInstancePair>)vals;
            var alerts = result.Select(x => x.Alert).ToList();

            alerts.RemoveAll(a => a.Approve_Status != (int)ApproveStatus.ApprovedByModerator && a.Approve_Status != (int)ApproveStatus.ApprovedByDefault);

            return alerts;
        }

        private class AlertComparer : IEqualityComparer<Alert>
        {
            public bool Equals(Alert g1, Alert g2)
            {
                return g1.Id == g2.Id;
            }

            public int GetHashCode(Alert obj)
            {
                return obj.Id;
            }
        }

        public List<Alert> GetAlertsForUser(string name, string domain = null)
        {
            //TODO: parsing through a mask
            var fullUserName = _userService.JoinUserNameAndDomainName(name, domain).ToLower();
            var alertsPairForUser = _usersCache.Get(fullUserName) as List<AlertInstancePair> ?? new List<AlertInstancePair>();

            var alertsBroadcast = _usersCache.Get("broadcast") as List<AlertInstancePair> ?? new List<AlertInstancePair>();
            alertsBroadcast.RemoveAll(x => alertsPairForUser.Count(y => y.Alert.Id == x.Alert.Id) > 0);

            var result = alertsPairForUser.Select(afu => afu.Alert).Concat(alertsBroadcast.Select(alp => alp.Alert)).ToList();
            result.RemoveAll(a =>
                a.Approve_Status == (int)ApproveStatus.NotModerated ||
                a.Approve_Status == (int)ApproveStatus.Rejected);

            return result;
        }

        public List<Alert> GetAlertsForUserDistinctByInstance(string name, string domain = null)
        {
            return GetAlertsForUser(name, domain).Distinct(new AlertComparer()).ToList();
        }

        public List<Alert> GetAlertsForInstance(string deskbar, string name, string domain = null)
        {
            var key = _userService.JoinUserNameAndDomainName(name, domain).ToLower();

            var alertsForInstance = (_usersCache.Get(key) as List<AlertInstancePair> ?? new List<AlertInstancePair>())
                .Where(aip => aip.InstanceId.ToLower()
                    .Equals(deskbar.TrimStart('{').TrimEnd('}').ToLower()))
                .Select(aip1 => aip1.Alert)
                .Distinct(new AlertComparer()).ToList();

            var alertsBroadcast = _usersCache.Get("broadcast") as List<AlertInstancePair> ?? new List<AlertInstancePair>();
            alertsBroadcast.RemoveAll(x => alertsForInstance.Count(y => y.Id == x.Alert.Id) > 0);

            var result = alertsForInstance.Concat(alertsBroadcast.Select(aip1 => aip1.Alert)).ToList();
            result.RemoveAll(a =>
                a.Approve_Status == (int)ApproveStatus.NotModerated ||
                a.Approve_Status == (int)ApproveStatus.Rejected);

            return result.ToList();
        }

        /// <summary>
        /// Add alert to cache.
        /// </summary>
        /// <param name="alertId">Alert ID.</param>
        /// <param name="date">Alert date.</param>
        public void Add(long alertId, string date)
        {
            _logger.Debug($"Start adding alert {alertId} to cache");
            using (DBManager manager = new DBManager())
            {
                manager.ExecuteQuery("SET LANGUAGE BRITISH");

                var usersAlertSet = GetDataSetOfAlerts(
                    SelectActualAlertsForRecipientsQuery
                        .Replace("AND (a.schedule = 0 OR a.schedule = 1 AND a.[recurrence] = 0)", string.Empty)
                        .Replace("AND a.from_date < @Now", string.Empty) +
                    $"AND a.id = {alertId}"
                );

                var usersBroadcastAlertSet = GetDataSetOfAlerts(
                    SelectActualAlertsForBroadcastQuery
                        .Replace("AND (a.schedule = 0 OR a.schedule = 1 AND a.[recurrence] = 0)", string.Empty)
                        .Replace("AND a.from_date < @Now", string.Empty) +
                    $"AND a.id = {alertId}"
                );

                foreach (var alertRow in usersBroadcastAlertSet)
                {
                    usersAlertSet.Add(alertRow);
                }

                var compsAlertSet = manager.GetDataByQuery(
                    $"EXEC GetAlertData '{date}' ,@alert_id = {alertId}, @recipients = 'computers'");
                var ipsAlertSet = manager.GetDataByQuery(
                    $"EXEC GetAlertData '{date}' , @alert_id = {alertId}, @recipients = 'ips'");

                if (IsActiveAlert(usersAlertSet, compsAlertSet, ipsAlertSet))
                {
                    BuildCacheFromDataSet(usersAlertSet, ref _usersCache, CacheTypes.CacheUsers);
                    BuildCacheFromDataSet(compsAlertSet, ref _compNamesCache, CacheTypes.CacheComps);
                    BuildCacheFromDataSet(ipsAlertSet, ref _ipsCache, CacheTypes.CacheIps);

                    var siblingsList = Config.Data.GetServerCompanions();
                    if (siblingsList.Length > 0)
                    {
                        _logger.Debug($"Start sending alert {alertId} to siblings` cache");
                        var alertCache = new AlertCache { AlertId = alertId, Date = date };
                        SendCacheToSiblings(siblingsList, alertCache);
                    }
                }
            }
        }

        private IEnumerable<Alert> GetAlertsListFromCachedUser(KeyValuePair<String, object> user)
        {
            return ((List<AlertInstancePair>)user.Value).Select(p => p.Alert).ToList();
        }


        public IEnumerable<UserAlertsDTO> GetScheduledSmsAlerts()
        {
            var users = new List<UserAlertsDTO>();
            foreach (var user in _usersCache)
            {
                var userAlerts = GetAlertsListFromCachedUser(user);
                var alerts = userAlerts.Where(
                    x => x.IsStarted(DateTime.Now)
                         && !x.IsExpired(DateTime.Now)
                         && x.Sms
                         && x.IsScheduledAndStarted()
                         && x.Approve_Status == ApprovedAlertStatusIndex);
                users.Add(new UserAlertsDTO { UserName = user.Key, Alerts = alerts });
            }

            return users;
        }

        public IEnumerable<UserAlertsDTO> GetApprovedPushAlerts()
        {
            var users = new List<UserAlertsDTO>();

            foreach (var user in _usersCache)
            {
                var userAlerts = GetAlertsListFromCachedUser(user);
                var alerts = userAlerts.Where(
                    x => x.IsStarted(DateTime.Now)
                         && !x.IsExpired(DateTime.Now)
                         && x.Approve_Status == (int)ApproveStatus.ApprovedByModerator
                         && x.Class != (int)AlertType.Wallpaper
                         && x.Class != (int)AlertType.ScreenSaver);
                if (alerts != null && alerts.Any())
                {
                    users.Add(new UserAlertsDTO { UserName = user.Key, Alerts = alerts });
                }
            }
            return users;
        }

        public IEnumerable<UserAlertsDTO> GetApprovedSmsAlerts()
        {
            var users = new List<UserAlertsDTO>();
            foreach (var user in _usersCache)
            {
                var userAlerts = GetAlertsListFromCachedUser(user);
                var alerts = userAlerts.Where(
                    x => x.IsStarted(DateTime.Now)
                         && !x.IsExpired(DateTime.Now)
                         && !x.IsScheduled()
                         && x.Sms
                         && x.Approve_Status == ApprovedAlertStatusIndex);
                users.Add(new UserAlertsDTO { UserName = user.Key, Alerts = alerts });
            }

            return users;
        }

        public IEnumerable<UserAlertsDTO> GetApprovedEmailAlerts()
        {
            var users = new List<UserAlertsDTO>();
            foreach (var user in _usersCache)
            {
                var userAlerts = GetAlertsListFromCachedUser(user);
                var alerts = userAlerts.Where(
                    x => x.IsStarted(DateTime.Now)
                         && !x.IsExpired(DateTime.Now)
                         && !x.IsScheduled()
                         && x.Email
                         && x.Approve_Status == ApprovedAlertStatusIndex);
                users.Add(new UserAlertsDTO { UserName = user.Key, Alerts = alerts });
            }

            return users;
        }

        public IEnumerable<UserAlertsDTO> GetScheduledEmailAlerts()
        {
            var users = new List<UserAlertsDTO>();
            foreach (var user in _usersCache)
            {
                var userAlerts = GetAlertsListFromCachedUser(user);
                var alerts = userAlerts.Where(
                    x => x.IsStarted(DateTime.Now) && !x.IsExpired(DateTime.Now) && x.Email
                         && x.IsScheduledAndStarted()
                         && x.Approve_Status == 1);
                users.Add(new UserAlertsDTO { UserName = user.Key, Alerts = alerts });
            }

            return users;
        }

        public IEnumerable<UserAlertsDTO> GetScheduledTextToCallAlerts()
        {
            var users = new List<UserAlertsDTO>();
            foreach (var user in _usersCache)
            {
                var userAlerts = GetAlertsListFromCachedUser(user);
                var alerts = userAlerts.Where(
                    x => x.IsStarted(DateTime.Now) && !x.IsExpired(DateTime.Now) && x.TextToCall
                         && x.IsScheduledAndStarted());
                users.Add(new UserAlertsDTO { UserName = user.Key, Alerts = alerts });
            }

            return users;
        }

        public IEnumerable<UserAlertsDTO> GetPushAlertsForRSS()
        {
            var users = new List<UserAlertsDTO>();
            foreach (var user in _usersCache)
            {
                var userAlerts = GetAlertsListFromCachedUser(user);
                var alerts = userAlerts.Where(
                    x => x.IsStarted(DateTime.Now) && !x.IsExpired(DateTime.Now) && !x.IsScheduled()
                         && x.Device != Device.Desktop && x.Class == (int)AlertType.RssAlert && x.Approve_Status == ApprovedAlertStatusIndex);
                users.Add(new UserAlertsDTO { UserName = user.Key, Alerts = alerts });
            }

            return users;
        }

        public IEnumerable<UserAlertsDTO> GetScheduledPushAlerts()
        {
            var users = new List<UserAlertsDTO>();
            using (DBManager manager = new DBManager())
            {
                var alertSentIds = manager.GetDataByQuery("SELECT DISTINCT(alert_id) as userId FROM push_sent;").ToList().Select(x => (long)x.GetInt("userId"));

                var alreadySent = new HashSet<long>(alertSentIds);

                foreach (var user in _usersCache)
                {
                    var userAlerts = GetAlertsListFromCachedUser(user);
                    var alerts = userAlerts.Where(
                        x => x.IsStarted(DateTime.Now) && !x.IsExpired(DateTime.Now) && x.IsScheduledAndStarted()
                             && x.Device != Device.Desktop
                             && x.Approve_Status == ApprovedAlertStatusIndex
                             && (x.Class == (int)AlertType.SimpleAlert
                                || x.Class == (int)AlertType.RssAlert
                                || x.Class == (int)AlertType.EmergencyAlert
                                || x.Class == (int)AlertType.SimpleSurvey
                                || x.Class == (int)AlertType.SurveyPoll
                                || x.Class == (int)AlertType.SurveyQuiz)
                            && !alreadySent.Contains(x.Id));
                    alerts = alerts.Distinct();
                    if (alerts.Any())
                    {
                        users.Add(new UserAlertsDTO { UserName = user.Key, Alerts = alerts });
                    }

                }
            }

            return users;
        }

        private string GenerateKey(DataRow row, CacheTypes type)
        {
            string key = string.Empty;

            switch (type)
            {
                case CacheTypes.CacheUsers:
                    {
                        //var deskbar = row.GetString("deskbar").ToLower();
                        var domain = row.GetString("user_domain").ToLower();
                        var username = row.GetString("username").ToLower();

                        key = username;
                        if (!username.Equals("broadcast"))
                        {
                            //key = _userService.JoinDeskbarAndUserNameAndDomainName(deskbar, username, domain);
                            key = _userService.JoinUserNameAndDomainName(username, domain);
                        }
                        break;
                    }

                case CacheTypes.CacheComps:
                    {
                        string domain = row.GetString("comp_domain").ToLower();
                        string compname = row.GetString("comp_name").ToLower();
                        key = compname;
                        if (!string.IsNullOrEmpty(domain))
                        {
                            key = domain + "." + compname;
                        }

                        break;
                    }

                case CacheTypes.CacheIps:
                    {
                        string ipFrom = row.GetString("ip_from");
                        string ipTo = row.GetString("ip_to");

                        key = ipFrom + "-" + ipTo;
                        break;
                    }
            }

            return key;
        }

        private void FillReceivedAlertsCache()
        {
            var receivedAlertsDataSet = GetDataSetOfAlerts(_selectReceivedAlertsQuery);

            foreach (var row in receivedAlertsDataSet)
            {
                var user = row.GetString("username");
                var domain = row.GetString("domain");
                var deskbar = row.GetString("deskbar");
                var alertId = int.Parse(row.GetString("alert_id"));

                var fullUserName = _userService
                    .JoinDeskbarAndUserNameAndDomainName(deskbar, user, domain)
                    .ToLower();

                AddAlertsToReceivedCache(fullUserName, alertId);
            }
        }

        private void BuildCacheFromDataSet(DataSet usersAlertsSet, ref MemoryCache cache, CacheTypes type, bool clear = false)
        {
            if (clear)
            {
                var cacheKeys = cache.Select(kvp => kvp.Key);
                foreach (var key in cacheKeys)
                {
                    cache.Remove(key);
                }
            }

            foreach (var row in usersAlertsSet)
            {
                var key = GenerateKey(row, type);
                var deskbar = row.GetString("deskbar").ToLower();
                if (cache.Contains(key))
                {
                    //var alerts = cache.Get(key) as List<Alert>;
                    var alertPairs = cache.Get(key) as List<AlertInstancePair>;
                    var alert = new Alert(row);
                    var alertInstancePair = new AlertInstancePair(alert, deskbar);
                    alertPairs?.Add(alertInstancePair);
                    cache.Set(key, alertPairs ?? throw new InvalidOperationException(), _policy);
                }
                else
                {
                    var list = new List<AlertInstancePair> {new AlertInstancePair(new Alert(row), deskbar)};
                    cache.Add(key, list, _policy);
                }
            }
        }

        private void RemoveAlertsFromCache(long alertId, CacheTypes type)
        {
            MemoryCache cache = null;

            switch (type)
            {
                case CacheTypes.CacheUsers:
                    {
                        cache = _usersCache;
                        break;
                    }
                case CacheTypes.CacheIps:
                    {
                        cache = _ipsCache;
                        break;
                    }
                case CacheTypes.CacheComps:
                    {
                        cache = _compNamesCache;
                        break;
                    }
                case CacheTypes.CacheRecievedAlerts:
                    {
                        cache = _receivedAlertsCache;
                        break;
                    }
            }

            var cacheKeys = (cache ?? throw new InvalidOperationException()).Select(kvp => kvp.Key).ToList();

            foreach (var key in cacheKeys)
            {
                RemoveAlertForKey(key.ToLower(), alertId, type);
            }
        }

        /// <summary>
        /// Send alert to server siblings to provide cache replication.
        /// </summary>
        /// <param name="siblingsUrlsList">Replication servers list.</param>
        /// <param name="alertCacheCache">Alert to sync with siblings.</param>
        private void SendCacheToSiblings(IReadOnlyCollection<string> siblingsUrlsList, AlertCache alertCacheCache)
        {
            try
            {
                if (alertCacheCache == null)
                {
                    throw new ArgumentException("Alert cache model is empty.");
                }

                if (siblingsUrlsList.Count > 0)
                {
                    _logger.Debug($"Start sending cached alert {alertCacheCache.AlertId} to all siblings.");

                    using (var client = new HttpClient())
                    {
                        foreach (var siblingUrl in siblingsUrlsList)
                        {
                            _logger.Debug($"Start sending cached alert {alertCacheCache.AlertId} to {siblingUrl}.");

                            var jsonData = JsonConvert.SerializeObject(alertCacheCache);
                            var content = new StringContent(jsonData, Encoding.UTF8);

                            var request = new HttpRequestMessage
                            {
                                Content = content,
                                Method = HttpMethod.Post,
                                RequestUri = new Uri($"{siblingUrl}/api/Cache/AddCache")
                            };

                            request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                            var unused = client.SendAsync(request).Result;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }

        /// <summary>
        /// Send alert to server siblings to provide cache replication.
        /// </summary>
        /// <param name="siblingsUrlsList">Replication servers list.</param>
        /// <param name="cachedAlertCache">Alert to sync with siblings.</param>
        private void SendReceivedCacheToSiblings(string[] siblingsUrlsList, ReceivedAlertCache receivedAlertCache)
        {
            try
            {
                if (receivedAlertCache == null)
                {
                    throw new ArgumentException("Alert cache model is empty.");
                }

                if (siblingsUrlsList.Length > 0)
                {
                    _logger.Debug($"Start sending cached received alert {receivedAlertCache.AlertId} to all siblings.");

                    using (var client = new HttpClient())
                    {
                        foreach (var siblingUrl in siblingsUrlsList)
                        {
                            _logger.Debug($"Start sending cached alert {receivedAlertCache.AlertId} to {siblingUrl}.");

                            var jsonData = JsonConvert.SerializeObject(receivedAlertCache);
                            var content = new StringContent(jsonData, Encoding.UTF8);

                            var request = new HttpRequestMessage
                            {
                                Content = content,
                                Method = HttpMethod.Post,
                                RequestUri = new Uri($"{siblingUrl}/api/Cache/AddReceivedCache")
                            };

                            request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                            var unused = client.SendAsync(request).Result;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }

        // TODO Remove copy-paste from SendCacheToSiblings method.
        /// <summary>
        /// Send alert to server siblings to provide cache replication.
        /// </summary>
        /// <param name="siblingsUrlsList">Replication servers list.</param>
        /// <param name="cachedAlertCache">Alert to sync with siblings.</param>
        private void DeleteCacheFromSiblings(string[] siblingsUrlsList, AlertCache cachedAlertCache)
        {
            try
            {
                if (cachedAlertCache == null)
                {
                    throw new ArgumentException("Alert cache model is empty.");
                }

                if (siblingsUrlsList.Length > 0)
                {
                    _logger.Debug($"Start deleting cached alert {cachedAlertCache.AlertId} from all siblings.");

                    using (var client = new HttpClient())
                    {
                        foreach (var siblingUrl in siblingsUrlsList)
                        {
                            _logger.Debug($"Start deleting cached alert {cachedAlertCache.AlertId} from {siblingUrl}.");

                            var jsonData = JsonConvert.SerializeObject(cachedAlertCache);
                            var content = new StringContent(jsonData, Encoding.UTF8);

                            var request = new HttpRequestMessage
                            {
                                Content = content,
                                Method = HttpMethod.Post,
                                RequestUri = new Uri($"{siblingUrl}/api/Cache/DeleteCache")
                            };

                            request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                            var unused = client.SendAsync(request).Result;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }

        private DataSet GetDataSetOfAlerts(string query)
        {
            using (var manager = new DBManager())
            {
                var dateTimeNowParam = SqlParameterFactory.Create(System.Data.DbType.DateTime, DateTime.Now, "Now");
                var actualAlerts = manager.GetDataByQuery(query, dateTimeNowParam);
                return actualAlerts;
            }
        }

        public List<ShowCacheDto> ShowUserCache()
        {
            var cache = _usersCache;

            var cacheDto = new List<ShowCacheDto>();
            if (cache == null)
            {
                return cacheDto;
            }

            foreach (var userCache in cache)
            {
                foreach (var alertInstancePair in (List<AlertInstancePair>)userCache.Value)
                {
                    cacheDto.Add(new ShowCacheDto(userCache.Key, alertInstancePair.InstanceId, alertInstancePair.Alert.Id, alertInstancePair.Alert.Title));
                }
            }

            return cacheDto;
        }

        public List<ShowCacheDto> ShowAlertReceivedCache()
        {
            var cache = _receivedAlertsCache;

            var cacheDto = new List<ShowCacheDto>();
            if (cache == null)
            {
                return cacheDto;
            }

            foreach (var receivedCache in cache)
            {
                foreach (var receivedAlertId in (List<int>)receivedCache.Value)
                {
                    cacheDto.Add(new ShowCacheDto(receivedCache.Key, string.Empty, receivedAlertId, string.Empty));
                }
            }

            return cacheDto;
        }
    }
}