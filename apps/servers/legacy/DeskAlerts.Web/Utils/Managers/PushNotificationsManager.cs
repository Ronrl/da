﻿using DeskAlerts.ApplicationCore.Enums;

namespace DeskAlerts.Server.Utils.Managers
{
    using System;
    using System.Collections.Generic;
    using System.Xml.Linq;

    using ApplicationCore.Entities;
    using ApplicationCore.Interfaces;
    using ApplicationCore.Utilities;
    using DeskAlertsDotNet.Pages;
    using DeskAlertsDotNet.Parameters;
    using ApplicationCore.Repositories;

    using Newtonsoft.Json.Linq;

    using PushSharp.Google;
    using PushSharp.Windows;
    using NLog;
    using System.Threading;

    public class PushNotificationsManager
    {
        private const string AndroidAuthLegacyToken = "AIzaSyDB_eUk9o_slxovyZ7S2u8Q34PcHDS2A-M";
        private const string AndroidAuthToken = "AAAACBrYytg:APA91bHy3sxYxRxyOhoVAUcYxyjmIgpOpzY8CzDOr0O3CizCodLeB0WccbZ5kK1FlScfNBZc44RtP-103IUutzVk8mx4GDuCzLgpLlNiXE2jVo5rjE_DUhYB6QgltFxSErzHjfIxGOAz";
        private const string FCMAppId = "637817683655";
        private const string FCMUrl = "https://fcm.googleapis.com/fcm/send";

        private readonly Logger logger = LogManager.GetCurrentClassLogger();

        private const int MaxAttemptCount = 6;

        IConfigurationManager _config = null;
        IPushSentRepository _pushRepository = null;
        IAlertRepository _alertRepository = null;
        private readonly IUserInstancesRepository _userInstancesRepository;
        private static readonly object PushUpdateLocker = new object();
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        public const string ApnsCertificatePassword = "Softomate1K";

        public const string AppleCertPath = @"\admin\APNS_distribution_PK.p12";

        private static PushNotificationsManager instance;

        private PushNotificationsManager()
        {
            _config = new WebConfigConfigurationManager(Config.Configuration);
            _pushRepository = new PpPushSentRepository(_config);
            _alertRepository = new PpAlertRepository(_config);
            _userInstancesRepository = new PpUserInstancesRepository(_config);
        }

        public static PushNotificationsManager Default
        {
            get
            {
                if (instance == null)
                {
                    return instance = new PushNotificationsManager();
                }

                return instance;
            }
        }

        public void SendPushNotification(AlertEntity alert, UserInstances instance)
        {
            var pushSent = _pushRepository.GetPushByDeviceIdAlertId(instance.Id, alert.Id);
            switch (instance.Type)
            {
                case DeviceType.Android:
                    {
                        SendAndroidNotification(alert, instance, pushSent);
                        break;
                    }

                case DeviceType.IOs:
                    {
                        SendIOsNotification(alert, instance, pushSent);
                        break;
                    }

                case DeviceType.WindowsPhone:
                    {
                        SendWindowsPhoneNotification(alert, instance, pushSent);
                        break;
                    }
            }
        }

        public void AddPushNotificationToDb(long alertId, IEnumerable<long> userIds)
        {
            try
            {
                _pushRepository.InsertByAlertIdAndUserId(alertId, userIds);
            }
            catch (Exception ex)
            {
                var userIdsString = string.Join(",", userIds);
                _logger.Error($"Push couldn't be sent for alertId {alertId} and userIds {userIdsString} with an Exception {ex.Message}: {ex.StackTrace}");
            }
        }

        private void SendAndroidNotification(AlertEntity alert, UserInstances instance, PushSent pushSent )
        {
            var config = new GcmConfiguration( FCMAppId , AndroidAuthToken, null);
            config.GcmUrl = FCMUrl;
            
            var provider = "FCM";

            var gcmBroker = new GcmServiceBroker(config);

            string gcmJsonText = @"{
                                    ""title"":""%title%"",
                                    ""sound"" : ""notify.wav"",
                                    ""icon"": ""myicon"",
                                    ""alertId"": ""%alertId"",
                                    ""unobtrusive"": ""%unobtrusive%""                                    
                                }";

            alert.Title = alert.Title.Replace("\"", "\\\"");

            gcmJsonText = gcmJsonText.Replace("%title%", alert.Title.EscapeQuotes()).Replace(
                "%alertId%",
                alert.Id.ToString().Replace("%unobtrusive%", (alert.Class == (int)AlertType.EmergencyAlert).ToString()));
            
            gcmBroker.OnNotificationFailed += (notification, aggregateEx) => {

                aggregateEx.Handle(ex => {
                    if (ex is GcmNotificationException notificationException)
                    {
                        logger.Log(LogLevel.Error, notificationException);
                        var gcmNotification = notificationException.Notification;
                        var description = notificationException.Description;
                        int pushSentId = (int)gcmNotification.Tag;
                        UpdatePushStatus(pushSent.Id, PushSendingStatus.GcmNotificationException);
                    }
                    else
                    {
                        UpdatePushStatus(pushSent.Id, PushSendingStatus.NotSent);
                        logger.Log(LogLevel.Error, $"Some push notification exception. See http traffic logs. Exception: \"{ex}\".");
                    }
                    return true;
                });
            };

            gcmBroker.OnNotificationSucceeded += (notification) => {
                int pushSentId = (int)notification.Tag;
                UpdatePushStatus(pushSent.Id, PushSendingStatus.Success);
            };
            
            gcmBroker.Start();
            gcmBroker.QueueNotification(
                new GcmNotification
                {
                    RegistrationIds = new List<string> { instance.PushToken },
                    Data = JObject.Parse(gcmJsonText),
                    Tag = pushSent.Id
                    });
            gcmBroker.Stop();
        }

        internal void SendBrokenPushes()
        {
            var brokenPushes = _pushRepository.GetBrokenPushes( MaxAttemptCount );

            foreach ( var pushSent in brokenPushes )
            {
                try
                {
                    var alert = _alertRepository.GetById(pushSent.AlertId);
                    var instanceToSend = _userInstancesRepository.GetInstanceById(pushSent.UserInstanceId);
                    if (alert == null || instanceToSend == null)
                    {
                        logger.Debug($"Variable \"{nameof(alert)}\" or \"{nameof(instanceToSend)}\" is null in send broken pushes.");
                        continue;
                    }
                    SendPushNotification(alert, instanceToSend);
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                }
                
                Thread.Sleep(100);
            }
        }

        private void SendIOsNotification(AlertEntity alert, UserInstances instance, PushSent pushSent)
        {
            var expirationDate = alert.ToDate ?? DateTime.Now.AddYears(100);
            var payload = new NotificationPayload(instance.PushToken, alert.Title, 0, "notify.wav", expirationDate);
            payload.AddCustom("RegionID", "IDQ10150");
            payload.AddCustom("alert_id", alert.Id);

            var payloads = new List<NotificationPayload> { payload };

            var pushNotification = new PushNotification(
                false,
                Config.Data.GetString("alertsFolder") + AppleCertPath,
                ApnsCertificatePassword);

            bool result = pushNotification.SendToApple(payloads);
            if (result)
            {
                UpdatePushStatus(pushSent.Id, PushSendingStatus.Success);
            }
            else
            {
                UpdatePushStatus(pushSent.Id, PushSendingStatus.NotSent);
            }

        }

        private void SendWindowsPhoneNotification(AlertEntity alert, UserInstances instance, PushSent pushSent)
        {
            var config = new WnsConfiguration(string.Empty, string.Empty, string.Empty);
            var wnsBroker = new WnsServiceBroker(config);

            wnsBroker.Start();
            wnsBroker.QueueNotification(
                new WnsToastNotification
                    {
                        ChannelUri = instance.PushToken,
                        Payload = XElement.Parse(
                            @"<?xml version=""1.0"" encoding=""utf-8""?>"
                            + @"<wp:Notification xmlns:wp=""WPNotification"">" + @"<wp:Toast>"
                            + @"<wp:Text1>" + alert.Title + "</wp:Text1>" + @"<wp:Text2>" + alert.Body
                            + "</wp:Text2>" + @"<wp:Param>/EntryPage.xaml?alertId=" + alert.Id
                            + "</wp:Param>" + @"</wp:Toast> " + @"</wp:Notification>")
                    });
            wnsBroker.Stop();
            UpdatePushStatus(pushSent.Id, PushSendingStatus.Success);
        }
        
        private void UpdatePushStatus(int pushId, PushSendingStatus status)
        {
            lock (PushUpdateLocker)
            {
                var pushSent = _pushRepository.GetById(pushId);
                pushSent.Status = (int)status;
                pushSent.TryCount += 1;
                _pushRepository.Update(pushSent);
            }
        }
        
    }
}