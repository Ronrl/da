﻿namespace DeskAlertsDotNet.Utils.Pages
{
    using System;
    using System.Collections.Specialized;
    using System.Text;
    using System.Web;
    using NLog;
    using Resources;

    public class UrlHelper
    {
        private const string Ascending = "ASC";

        private const string Descending = "DESC";

        public string SortBy { get; set; }

        public int Offset { get; set; }
        public string Webalert { get; set; }
        public int Limit { get; set; }

        public string Search;

        private Logger _logger = LogManager.GetCurrentClassLogger();

        public string Url { get; set; }

        private string _pageParams;

        private string _contentName;

        private int _count;

        public string AlertType { get; set; }

        public UrlHelper(string url, string search, string pageParams, string defaultOrder, string defaultColumn, string sortBy, string contentName, string offset, string limit, int count, string webalert, string alertType)
        {
            Url = url;
            Search = search;
            _pageParams = pageParams;
            _contentName = contentName;
            Limit = string.IsNullOrEmpty(limit) ? 25 : Convert.ToInt32(limit);
            Offset = string.IsNullOrEmpty(offset) ? 0 : Convert.ToInt32(offset);
            SortBy = string.IsNullOrEmpty(sortBy) ? $"{defaultColumn} {defaultOrder}" : sortBy;
            _count = count;
            Webalert = webalert;
            AlertType = alertType;
        }

        public string GetUrl(string sortBy, int offset, int limit)
        {
            try
            {
                var uriBuilder = new UriBuilder(Url);
                var query = HttpUtility.ParseQueryString(uriBuilder.Query);
                query = AddPageParameters(query);
                query["searchTermBox"] = Search;
                query["sortBy"] = sortBy;
                query["offset"] = offset.ToString();
                query["limit"] = limit.ToString();
                query["webalert"] = Webalert;
                query["alertType"] = AlertType;
                uriBuilder.Query = query.ToString();
                var result = uriBuilder.ToString();
                return result;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                _logger.Debug(ex);
                throw;
            }
        }

        private NameValueCollection AddPageParameters(NameValueCollection urlParameters)
        {
            try
            {
                if (urlParameters == null)
                {
                    throw new ArgumentNullException(nameof(urlParameters));
                }

                var parameters = HttpUtility.ParseQueryString(_pageParams);
                foreach (var parameter in parameters.AllKeys)
                {
                    var value = parameters[parameter];
                    urlParameters.Add(parameter, value);
                }

                return urlParameters;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                _logger.Debug(ex);
                throw;
            }
        }

        private const string makePagesTableHeader =
            "<table class=\"paginate\" style =\"margin-top:10px\" width=\"95%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td> ";

        private const string makePagesTagTrOpen =
            "</td><td align=right>";

        private const string makePagesOtherTableHeader =
            "<table width='95%' height='100%' cellspacing='0' cellpadding='3'><tr><td>";

        private const string makePagesTableHeaderClose =
            "</td></tr></table>";

        /// <summary>
        /// Pagination table constructor.
        /// </summary>
        /// <returns>Pagination table (max records, pages, record per page).</returns>
        public string MakePages()
        {
            var result = new StringBuilder();
            result.Append(makePagesTableHeader);
            int sum = Offset + 1;
            result.Append(sum + "-");

            if (_count - Offset < Limit)
            {
                result.Append(_count.ToString());
            }
            else
            {
                sum = Offset + Limit;
                result.Append(sum.ToString());
            }

            result.Append(" " + resources.LNG_OF + " " + _count + " " + _contentName + makePagesTagTrOpen);

            int n = Limit * 10; // TODO: rename variables, n,k etc. to names with sense
            while (true)
            {
                if (Offset >= n - (Limit * 10) && Offset < n)
                {
                    break;
                }
                else
                {
                    n += Limit * 10;
                }
            }

            int k = 0, i = 0;

            if (k + Limit < _count)
                result.Append(resources.LNG_PAGES + ":");

            while (k + Limit < _count)
            {
                k = i * Limit;
                int l = k + Limit;
                int startPeriodPageNumber = i + 1;
                int stopPeriodPageNumber = startPeriodPageNumber + 9;

                if (k != Offset)
                {
                    var url = GetUrl(SortBy, k, Limit);
                    if (k < n && k >= n - (Limit * 10))
                    {
                        result.Append(" | " + "<a href='" + url + "'>" + startPeriodPageNumber + "</a>");
                    }
                    else if (k - n == 0 || n - k == (Limit * 20))
                    {
                        result.Append(" | " + "<a href='" + url + "'>" + "[" + startPeriodPageNumber + "-" + stopPeriodPageNumber + "]" + "</a>");
                    }
                }
                else
                {
                    result.Append(" | <strong>" + startPeriodPageNumber + "</strong>");
                }
                i++;
            }

            result.Append(makePagesTableHeaderClose);
            result.Append(makePagesOtherTableHeader + resources.LNG_RECORDS_PER_PAGE + " ");
            int[] recordsPerPage = { 25, 50, 100, 500, 1000 };
            foreach (int record in recordsPerPage)
            {
                if (Limit == record)
                {
                    result.Append(" | " + record);
                }
                else
                {
                    var url = GetUrl(SortBy, 0, record);
                    result.Append(" | <a href='" + url + "'>" + record + "</a>");
                }
            }

            result.Append(makePagesTableHeaderClose);
            return result.ToString();
        }
    }
}