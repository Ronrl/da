﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeskAlerts.Server.Utils.Pages
{
    public class TickerHelper
    {
        public static (int, string, Dictionary<string, object>) HandleTickerSpeed(string tickerText, string tickerSpeedValue, bool isTickerEnabled, bool isTickerProEnabled,
            (int tTicker, string tContent, Dictionary<string, object> tAlertFields) tuple)
        {
            if ((!string.IsNullOrEmpty(tickerText) && tickerText.Equals("1")) &&
                (isTickerEnabled || isTickerProEnabled))
            {
                tuple.tTicker = 1;
                if (isTickerProEnabled && tuple.tContent.IndexOf("<!-- ticker_pro -->", StringComparison.Ordinal) < 0)
                {
                    tuple.tContent += "<!-- ticker_pro -->";
                }

                var middleTickerSpeed = "4";

                tickerSpeedValue = tickerSpeedValue == null ? middleTickerSpeed : tickerSpeedValue;

                var tickerSpeed = int.Parse(tickerSpeedValue);

                var tickerSpeedString = "<!-- tickerSpeed=";
                var tickerSpeedEntranceIndex = tuple.tContent.IndexOf(tickerSpeedString, StringComparison.Ordinal);
                if (tickerSpeedEntranceIndex > 0)
                {
                    var sb = new StringBuilder(tuple.tContent);
                    sb[tickerSpeedEntranceIndex + tickerSpeedString.Length] = char.Parse(tickerSpeed.ToString());
                    tuple.tContent = sb.ToString();
                }
                else
                {
                    tuple.tContent += "<!-- tickerSpeed=" + tickerSpeed + " -->";
                }

                tuple.tAlertFields.Add("ticker_speed", tickerSpeed);
                tuple.tAlertFields.Add("ticker", tuple.tTicker);
            }
            else
            {
                tuple.tAlertFields.Add("ticker", 0);
            }

            var result = (tuple.tTicker, tuple.tContent, tuple.tAlertFields);

            return result;
        }
    }
}