﻿namespace DeskAlertsDotNet.Utils.Caches
{
    using DeskAlerts.ApplicationCore.Models;
    using System;
    using System.Collections.Generic;

    public static class AutoupdateSlotCache
    {
        public static Dictionary<Guid, AutoupdateSlot> Items { get; } = new Dictionary<Guid, AutoupdateSlot>();
    }
}