﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Web;
using NLog;

namespace DeskAlerts.Server.Utils.Helpers
{
    public static class IpReader
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Get IP client application as long type.
        /// </summary>
        /// <param name="ipAddressString">Ip address.</param>
        /// <returns>Ip address as Int64 type.</returns>
        public static long ReadIpAddress(string ipAddressString, HttpRequest httpRequest = null)
        {
            long longIp = 0;
            try
            {
                IPAddress ipAddress = null;
                if (ipAddressString != null)
                {
                    ipAddress = IPAddress.Parse(ipAddressString.Trim());
                }
                else if (httpRequest?.UserHostAddress != null)
                {
                    ipAddress = Dns.GetHostAddresses(httpRequest.UserHostAddress)[0];
                }

                if (ipAddress != null)
                {
                    longIp = ConvertIpAddressToLong(ipAddress);
                }
            }
            catch (Exception ex)
            {
                Logger.Debug(ex);
            }

            return longIp;
        }

        /// <summary>
        /// Get IP client application list from token as long type.
        /// </summary>
        /// <param name="ipAddressString">Token.Ip.</param>
        /// <param name="ipVersion">Return IP version (<see cref="AddressFamily"/> for IP version or null for both version).</param>
        /// <returns>List of IP client addresses in <see cref="long"/> type.</returns>
        public static List<long> GetIpAddressListAsInt64(string ipAddressString, HttpRequest httpRequest = null, AddressFamily? ipVersion = null)
        {
            var result = new List<long>();
            try
            {
                var ipAddressList = new List<IPAddress>();
                var resultIps = new List<IPAddress>();
                if (ipAddressString != null)
                {
                    var ipAddressStringArray = ipAddressString.Split(',');

                    foreach (var ipString in ipAddressStringArray)
                    {
                        if (ipString != "ip")
                        {
                            try
                            {
                                ipAddressList.Add(IPAddress.Parse(ipString.Trim()));
                            }
                            catch
                            {
                                Logger.Trace($"Value \"{ipString}\" is not IP Address.");
                            }
                        }
                    }
                }
                else if (httpRequest?.UserHostAddress != null)
                {
                    ipAddressList.Add(Dns.GetHostAddresses(httpRequest.UserHostAddress)[0]);
                }

                switch (ipVersion)
                {
                    case AddressFamily.InterNetwork:
                        resultIps.AddRange(ipAddressList.Where(x => x.AddressFamily == AddressFamily.InterNetwork));
                        break;
                    case AddressFamily.InterNetworkV6:
                        // TODO: IPv6 not working in this moment:
                        // resultIps.AddRange(ipAddressList.Where(x => x.AddressFamily == AddressFamily.InterNetworkV6));
                        resultIps.AddRange(ipAddressList.Where(x => x.AddressFamily == AddressFamily.InterNetwork));
                        break;
                    default:
                        resultIps.AddRange(ipAddressList);
                        break;
                }

                foreach (var ipAddress in resultIps)
                {
                    result.Add(ConvertIpAddressToLong(ipAddress));
                }
            }
            catch (Exception ex)
            {
                Logger.Trace(ex);
            }

            return result;
        }

        /// <summary>
        /// Convert <see cref="IPAddress"/> type to <see cref="long"/> type.
        /// </summary>
        /// <param name="ipAddress">Ip address.</param>
        /// <returns>Ip address as long.</returns>
        public static long ConvertIpAddressToLong(IPAddress ipAddress)
        {
            if (ipAddress == null)
            {
                Logger.Debug($"Argument {nameof(ipAddress)} is null.");
                return default(long);
            }

            var bytes = ipAddress.GetAddressBytes();
            Array.Reverse(bytes);
            long result = BitConverter.ToUInt32(bytes, 0);

            return result;
        }
    }
}