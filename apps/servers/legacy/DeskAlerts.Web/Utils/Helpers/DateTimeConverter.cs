﻿using System;
using System.Collections.Generic;
using System.Globalization;
using DeskAlertsDotNet.DataBase;

namespace DeskAlerts.Server.Utils.Helpers
{
    public class DateTimeConverter
    {
        public static readonly IEnumerable<string> DateTimeFormats =
            new List<string>
            {
                "dd/MM/yyyy",
                "yyyy/MM/dd",
                "MM/dd/yyyy",
                "yyyy/dd/MM",
                "MM/yyyy/dd",
                "dd/yyyy/MM",
                "dd-MM-yyyy",
                "yyyy-MM-dd",
                "MM-dd-yyyy",
                "yyyy-dd-MM",
                "MM-yyyy-dd",
                "dd-yyyy-MM"
            };

        public static string DateTimeFormat
        {
            get
            {
                if (string.IsNullOrEmpty(_dateTimeFormat))
                {
                    _dateTimeFormat = GetDateTimeFormat(DateFormat, TimeFormat);
                }

                return _dateTimeFormat;
            }
        }

        public static string DateFormat
        {
            get
            {
                if (string.IsNullOrEmpty(_dateFormat))
                {
                    using (var dbMgr = new DBManager())
                    {
                        _dateFormat = dbMgr.GetScalarQuery<string>("SELECT val FROM settings WHERE name = 'ConfDateFormat'");
                    }
                }

                return _dateFormat;
            }
        }

        public static string JsTimeFormat => TimeFormat.Contains("tt") ? TimeFormat.Replace("tt", "TT") : TimeFormat;

        public static string MomentJsDateTimeFormat
        {
            get
            {
                var jsDateFormat = DateTimeFormat.ToUpperInvariant();
                if (jsDateFormat.Contains("TT"))
                {
                    jsDateFormat = jsDateFormat.Replace("TT", "A");
                }

                if (jsDateFormat.Contains("HH:MM"))
                {
                    jsDateFormat = jsDateFormat.Replace("HH:MM", "hh:mm");
                }

                if (jsDateFormat.Contains("H:MM"))
                {
                    jsDateFormat = jsDateFormat.Replace("H:MM", "h:mm");
                }

                return jsDateFormat;
            }
        }

        public static string QuasarDateTimeFormat
        {
            get
            {
                var jsTimeFormat = TimeFormat;
                if (jsTimeFormat.Contains("tt"))
                {
                    jsTimeFormat = jsTimeFormat.Replace("tt", "A");
                }

                return $"{DateFormat.ToUpperInvariant()} {jsTimeFormat}";
            }
        }

        public static string JsDateTimeFormat
        {
            get
            {
                var jsTimeFormat = TimeFormat;
                if (jsTimeFormat.Contains("tt"))
                {
                    jsTimeFormat = jsTimeFormat.Replace("tt", "a");
                }

                return $"{DateFormat} {jsTimeFormat}";
            }
        }

        public static string JsDateFormat
        {
            get
            {
                var jsDateFormat = DateFormat;
                if (jsDateFormat.Contains("MM"))
                {
                    jsDateFormat = jsDateFormat.Replace("MM", "mm");
                }

                if (jsDateFormat.Contains("yyyy"))
                {
                    jsDateFormat = jsDateFormat.Replace("yyyy", "yy");
                }

                return jsDateFormat;
            }
        }

        public static string TimeFormat
        {
            get
            {
                if (string.IsNullOrEmpty(_timeFormat))
                {
                    using (var dbMgr = new DBManager())
                    {
                        _timeFormat = dbMgr.GetScalarQuery<string>("SELECT val FROM settings WHERE name = 'ConfTimeFormat'");
                    }
                }

                return _timeFormat;
            }
        }

        private static string _dateTimeFormat;
        private static string _dateFormat;
        private static string _timeFormat;

        private static string MsSqlFormat => "M/d/yyyy h:mm:ss tt";
        private static string BritishFormat => "d/M/yyyy h:mm:ss tt";

        public static string GetDateTimeFormat(string dateFormat, string timeFormat)
        {
            return $"{dateFormat} {timeFormat}";
        }

        public static void UpdateDateTimeFormat(string dateFormat, string timeFormat)
        {
            _dateFormat = dateFormat;
            _timeFormat = timeFormat;
            _dateTimeFormat = GetDateTimeFormat(dateFormat, timeFormat);
        }

        public static string ToUiDateTime(DateTime dateTime)
        {
            return dateTime.ToString(DateTimeFormat, CultureInfo.InvariantCulture);
        }

        public static string ToDbDateTime(DateTime dateTime)
        {
            return dateTime.ToString(BritishFormat, CultureInfo.InvariantCulture);
        }

        public static string ToUiDate(DateTime dateTime)
        {
            return dateTime.ToString(DateFormat, CultureInfo.InvariantCulture);
        }

        public static string ToUiTime(DateTime dateTime)
        {
            return dateTime.ToString(TimeFormat, CultureInfo.InvariantCulture);
        }

        public static string ConvertFromUiToDb(string s)
        {
            var dateTime = DateTime.ParseExact(s, DateTimeFormat, CultureInfo.InvariantCulture);
            return dateTime.ToString(BritishFormat, CultureInfo.InvariantCulture);
        }

        public static DateTime ParseDateTimeFromUi(string s)
        {
            return DateTime.ParseExact(s, DateTimeFormat, CultureInfo.InvariantCulture);
        }

        public static DateTime ParseTimeFromUi(string s)
        {
            return DateTime.ParseExact(s, TimeFormat, CultureInfo.InvariantCulture);
        }

        public static DateTime ParseDateTimeFromDb(string s, bool isBritishFormat = false)
        {
            var format = isBritishFormat
                ? BritishFormat
                : MsSqlFormat;

            return DateTime.ParseExact(s, format, CultureInfo.InvariantCulture);
        }
    }
}