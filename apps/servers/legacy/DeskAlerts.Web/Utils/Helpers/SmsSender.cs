﻿using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.ApplicationCore.Repositories;
using DeskAlerts.ApplicationCore.Utilities;
using DeskAlerts.Server.Utils.Interfactes;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Managers;
using DeskAlertsDotNet.Models;
using DeskAlertsDotNet.Parameters;
using DeskAlertsDotNet.Utils.Factories;
using DeskAlertsDotNet.Utils.Services;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using DataRow = DeskAlertsDotNet.DataBase.DataRow;

namespace DeskAlertsDotNet.Utils.Helpers
{
    public class SmsSender
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private const string InsertSmsSentByAlertIdAndUserId =
            "INSERT INTO sms_sent (alert_id,user_id,sent_date) VALUES  (@alert_id, @user_id, @date)";

        private const string SelectIdFromSmsSentByAlertId =
            "SELECT user_id AS id FROM sms_sent AS ss WHERE alert_id = @alert_id";

        private const string SelectIdFromSmsSentByAlertIdAndUserId =
            "SELECT id FROM sms_sent AS ss WHERE alert_id = @alertId AND user_id = @userId";

        private readonly IUserService _userService;

        public SmsSender()
        {
            IConfigurationManager configurationManager = new WebConfigConfigurationManager(Config.Configuration);
            IUserRepository userRepository = new PpUserRepository(configurationManager);
            IGroupRepository groupRepository = new PpGroupRepository(configurationManager);
            _userService = new UserService(userRepository, groupRepository);
        }

        public static void SendSms(IEnumerable<UserAlertsDTO> scheduledSmsAlerts)
        {
            foreach (var userAlerts in scheduledSmsAlerts)
            {
                if (userAlerts.Alerts.Any())
                {
                    if (userAlerts.UserName == "broadcast")
                    {
                        SendBroadcastSms(userAlerts);
                    }
                    else
                    {
                        SendToUserSms(userAlerts);
                    }
                }
            }
        }

        private static void SendBroadcastSms(UserAlertsDTO userAlerts)
        {
            foreach (var alert in userAlerts.Alerts)
            {
                var usersIdToSend = GetUsersIdToSmsSend(alert.Id, alert.IsScheduled());

                foreach (var userIdDataRow in usersIdToSend)
                {
                    var user = UserManager.Default.GetUserById(userIdDataRow.GetInt("id"));
                    var alertText = alert.GetClearAlertText();
                    if (!string.IsNullOrEmpty(user.Phone) && !string.IsNullOrEmpty(alertText))
                    {
                        var smsResult = SmsManager.Default.Send(alertText, user.Phone);
                        if (smsResult == SmsResult.Ok)
                        {
                            WriteToSmsSend(user.Id, alert.Id);
                        }
                    }
                }
            }
        }

        private static IEnumerable<DataRow> GetUsersIdToSmsSend(int alertId, bool isAlertScheduled)
        {
            using (DBManager dbMgr = new DBManager())
            {
                var alertIdParameter = SqlParameterFactory.Create(DbType.Int32, alertId, "alert_id");
                string isBroadcastSmsSendedQuery = SelectIdFromSmsSentByAlertId;
                if (isAlertScheduled)
                {
                    isBroadcastSmsSendedQuery += " AND ss.sent_date > DATEADD(d, 0, DATEDIFF(d, 0, GETDATE()))";
                }

                var sendedSmsUserId = dbMgr.GetDataByQuery(isBroadcastSmsSendedQuery, alertIdParameter).ToList();
                var allUsersId = UserManager.Default.GetUsersId().ToList();
                var usersIdToSend = allUsersId.Where(x => sendedSmsUserId.All(y => x.GetInt("id") != y.GetInt("id")));
                return usersIdToSend;
            }
        }

        private static void SendToUserSms(UserAlertsDTO userAlerts)
        {
            var userIdByRepository = new SmsSender()._userService.GetUserByName(userAlerts.UserName)?.Id;
            if (userIdByRepository == null)
            {
                Logger.Debug($"UserId not found in repository by user name \"{userAlerts.UserName}\".");
                return;
            }

            var userId = Convert.ToInt32(userIdByRepository);

            foreach (var alert in userAlerts.Alerts)
            {
                var user = UserManager.Default.GetUserById(userId);
                var alertText = alert.GetClearAlertText();
                var recievedUsers = GetRecievedSmsUsers(alert.Id, user.Id, alert.IsScheduled());
                if (!recievedUsers.IsEmpty)
                {
                    var dataRow = recievedUsers.GetRow(0);
                    var smsSentId = dataRow.GetInt("id");
                    if (smsSentId <= 0)
                    {
                        SendSms(alertText, user.Phone, userId, alert.Id);
                    }
                }
                else
                {
                    SendSms(alertText, user.Phone, userId, alert.Id);
                }
            }
        }

        private static void SendSms(string text, string phone, int userId, int alertId)
        {
            var smsResult = SmsManager.Default.Send(text, phone);
            if (smsResult == SmsResult.Ok)
            {
                WriteToSmsSend(userId, alertId);
            }
        }

        private static DataBase.DataSet GetRecievedSmsUsers(int alertId, int userid, bool isScheduled)
        {
            using (DBManager dbMgr = new DBManager())
            {
                var alertIdParam = SqlParameterFactory.Create(DbType.Int32, userid, "userId");
                var userIdParam = SqlParameterFactory.Create(DbType.Int32, alertId, "alertId");
                string isSmsSendedToUserQuery = SelectIdFromSmsSentByAlertIdAndUserId;
                if (isScheduled)
                {
                    isSmsSendedToUserQuery += " AND ss.sent_date > DATEADD(d, 0, DATEDIFF(d, 0, GETDATE()))";
                }

                var dataSet = dbMgr.GetDataByQuery(isSmsSendedToUserQuery, alertIdParam, userIdParam);
                return dataSet;
            }
        }

        private static void WriteToSmsSend(int userId, int alertid)
        {
            using (DBManager dbMgr = new DBManager())
            {
                var userIdParameter = SqlParameterFactory.Create(DbType.Int32, userId, "user_id");
                var alertIdParameter = SqlParameterFactory.Create(DbType.Int32, alertid, "alert_id");
                var dateParameter = SqlParameterFactory.Create(DbType.DateTime, DateTime.Now, "date");
                dbMgr.ExecuteQuery(
                    InsertSmsSentByAlertIdAndUserId,
                    userIdParameter,
                    alertIdParameter,
                    dateParameter);
            }
        }
    }
}