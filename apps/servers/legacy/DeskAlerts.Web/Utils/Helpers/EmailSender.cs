﻿using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.ApplicationCore.Repositories;
using DeskAlerts.ApplicationCore.Utilities;
using DeskAlerts.Server.Utils.Interfactes;
using DeskAlerts.Server.Utils.Managers;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Managers;
using DeskAlertsDotNet.Models;
using DeskAlertsDotNet.Parameters;
using DeskAlertsDotNet.Utils.Factories;
using DeskAlertsDotNet.Utils.Services;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Mail;
using DataRow = DeskAlertsDotNet.DataBase.DataRow;
using DataSet = DeskAlertsDotNet.DataBase.DataSet;

namespace DeskAlertsDotNet.Utils.Helpers
{
    public class EmailSender
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private const string SelectIdFromEmailSentByAlertIdAndUserId =
            "SELECT id FROM email_sent AS es WHERE alert_id = @alertId AND user_id = @userId";

        private const string SelectIdFromEmailSentByAlertId =
            "SELECT user_id AS id FROM email_sent AS es WHERE alert_id = @alert_id";

        private const string InsertEmailSentByAlertIdAndUserId =
            "INSERT INTO email_sent (alert_id,user_id,sent_date) VALUES  (@alert_id, @user_id, @date)";

        private readonly IUserService _userService;

        public EmailSender()
        {
            IConfigurationManager configurationManager = new WebConfigConfigurationManager(Config.Configuration);
            IUserRepository userRepository = new PpUserRepository(configurationManager);
            IGroupRepository groupRepository = new PpGroupRepository(configurationManager);
            _userService = new UserService(userRepository, groupRepository);
        }

        public void SendEmail(IEnumerable<UserAlertsDTO> scheduledEmailAlerts)
        {
            foreach (var userAlerts in scheduledEmailAlerts)
            {
                if (userAlerts.Alerts.Any())
                {
                    if (userAlerts.UserName == "broadcast")
                    {
                        SendBroadcastEmail(userAlerts);
                    }
                    else
                    {
                        SendToUserEmail(userAlerts);
                    }
                }
            }
        }

        private static void SendBroadcastEmail(UserAlertsDTO userAlerts)
        {
            foreach (var alert in userAlerts.Alerts)
            {
                var usersIdToSend = GetUsersIdToEmailSend(alert.Id, alert.IsScheduled());

                foreach (var userIdDataRow in usersIdToSend)
                {
                    var user = UserManager.Default.GetUserById(userIdDataRow.GetInt("id"));
                    var alertText = alert.GetAlertText();
                    if (!string.IsNullOrEmpty(user.Email))
                    {
                        var emailResult = EmailManager.Default.Send(alert.Title, alertText, user.Email);
                        if (emailResult.StatusCode == SmtpStatusCode.Ok)
                        {
                            WriteToEmailSend(user.Id, alert.Id);
                        }
                    }
                }
            }
        }

        private static IEnumerable<DataRow> GetUsersIdToEmailSend(int alertId, bool isAlertScheduled)
        {
            using (DBManager dbMgr = new DBManager())
            {
                var alertIdParameter = SqlParameterFactory.Create(DbType.Int32, alertId, "alert_id");
                string isEmailSentToUserQuery = SelectIdFromEmailSentByAlertId;
                if (isAlertScheduled)
                {
                    isEmailSentToUserQuery += " AND es.sent_date > DATEADD(d, 0, DATEDIFF(d, 0, GETDATE()))";
                }

                var sendedEmaiUserIds =
                    dbMgr.GetDataByQuery(isEmailSentToUserQuery, alertIdParameter).ToList();
                var allUsersId = UserManager.Default.GetUsersId().ToList();
                var usersIdToSend = allUsersId.Where(x => sendedEmaiUserIds.All(y => x.GetInt("id") != y.GetInt("id")));
                return usersIdToSend;
            }
        }

        private void SendToUserEmail(UserAlertsDTO userAlerts)
        {
            var userByRepositoryId = _userService.GetUserByName(userAlerts.UserName)?.Id;
            if (userByRepositoryId == null)
            {
                Logger.Debug($"UserId not found in repository by user name \"{userAlerts.UserName}\".");
                return;
            }

            var userId = Convert.ToInt32(userByRepositoryId);

            foreach (var alert in userAlerts.Alerts)
            {
                var user = UserManager.Default.GetUserById(userId);
                var alertText = alert.GetAlertText();
                var recievedUsers = GetRecievedEmailUsers(alert.Id, user.Id, alert.IsScheduled());
                if (!recievedUsers.IsEmpty)
                {
                    var dataRow = recievedUsers.GetRow(0);
                    var smsSentId = dataRow.GetInt("id");
                    if (smsSentId <= 0)
                    {
                        SendEmail(alert.Title, alertText, user.Email, userId, alert.Id);
                    }
                }
                else
                {
                    SendEmail(alert.Title, alertText, user.Email, userId, alert.Id);
                }
            }
        }

        private static void SendEmail(string title, string text, string to, int userId, int alertId)
        {
            var emailResult = EmailManager.Default.Send(title, text, to);
            if (emailResult.StatusCode == EmailResult.Ok.StatusCode)
            {
                WriteToEmailSend(userId, alertId);
            }
        }

        private static DataSet GetRecievedEmailUsers(int alertId, int userid, bool isAlertScheduled)
        {
            using (DBManager dbMgr = new DBManager())
            {
                var alertIdParam = SqlParameterFactory.Create(DbType.Int32, userid, "userId");
                var userIdParam = SqlParameterFactory.Create(DbType.Int32, alertId, "alertId");
                string isEmailSentToUserQuery = SelectIdFromEmailSentByAlertIdAndUserId;
                if (isAlertScheduled)
                {
                    isEmailSentToUserQuery += " AND es.sent_date > DATEADD(d, 0, DATEDIFF(d, 0, GETDATE()))";
                }

                var dataSet = dbMgr.GetDataByQuery(
                    isEmailSentToUserQuery,
                    alertIdParam,
                    userIdParam);
                return dataSet;
            }
        }

        private static void WriteToEmailSend(int userId, int alertid)
        {
            using (DBManager dbMgr = new DBManager())
            {
                var userIdParameter = SqlParameterFactory.Create(DbType.Int32, userId, "user_id");
                var alertIdParameter = SqlParameterFactory.Create(DbType.Int32, alertid, "alert_id");
                var dateParameter = SqlParameterFactory.Create(DbType.DateTime, DateTime.Now, "date");
                dbMgr.ExecuteQuery(
                    InsertEmailSentByAlertIdAndUserId,
                    userIdParameter,
                    alertIdParameter,
                    dateParameter);
            }
        }
    }
}