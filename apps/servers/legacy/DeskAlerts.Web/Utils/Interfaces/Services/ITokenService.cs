﻿using DeskAlerts.Server.Models;
using System.Collections.Generic;

namespace DeskAlerts.Server.Utils.Interfactes
{
    using ApplicationCore.Enums;
    using ApplicationCore.Models;
    using System;

    public interface ITokenService
    {
        IEnumerable<ShowTokenDto> GetAllTokens();
        void AddToken(Token token);
        void RemoveToken(Guid tokenGuid);
        void RemoveToken(string username);
        void RemoveAllTokens();
        Guid GetGuid(string bearerToken);
        Token CreateTokenForDomainUser(string userName, string computerName, string computerDomainName, string domainName, string ip, string deskbarId, ClientDeviceType deviceType);
        Token CreateTokenForUser(string userName, string computerName, string computerDomainName, string ip, string md5Password, string deskbarId, ClientDeviceType deviceType);
        Token GetTokenByGuid(Guid tokenGuid);
        bool ValidateToken(string bearerToken);
    }
}
