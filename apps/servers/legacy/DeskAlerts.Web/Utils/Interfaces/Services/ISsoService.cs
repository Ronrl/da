﻿namespace DeskAlerts.Server.Utils.Interfactes
{
    using ApplicationCore.Entities;
    using System.Collections.Generic;

    interface ISsoService
    {
        bool IsAuthorized(string userName, string domain);

        List<Policy> GetPoliciesByPublisherIdAndDomainId(long userId, long domainId);

        List<Group> GetGroupsWithPolicyAndDomain();

        IEnumerable<Group> GetGroupsByPublisherIdAndDomainId(long userId, long domainId);

        IEnumerable<Group> GetGroupsByPublisherId(long userId);
    }
}
