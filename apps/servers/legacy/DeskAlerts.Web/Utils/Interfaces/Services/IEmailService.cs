﻿using DeskAlertsDotNet.Models;
using System.Collections.Generic;

namespace DeskAlerts.Server.Utils.Interfaces.Services
{
    public interface IEmailService
    {
        IEnumerable<UserAlertsDTO> GetApprovedEmailAlerts();
    }
}
