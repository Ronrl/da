﻿using DeskAlertsDotNet.DataBase;
using System;
using System.Collections.Generic;
using DeskAlerts.ApplicationCore.Entities;

namespace DeskAlerts.Server.Utils.Interfactes
{
    public interface IContentReceivedService
    {
        long Create(long alertId, long userId, Guid deskbarId);
        
        void CreateRowsForUsers(long alertId, IEnumerable<long> userIds);
        
        void CreateRowsForUsersByUserNames(long alertId, IEnumerable<string> userNames);

        void CreateRowsForUsersByComputerNames(long alertId, IEnumerable<string> computersNames);

        void CreateRowsForUsersByGroupNames(long alertId, IEnumerable<string> groupNames);

        void CreateRowsForUsersByOrganizationUnitNames(long alertId, IEnumerable<string> ouNames);

        void CreateRowsInAlertsReceived(string sendType, int alertId, IEnumerable<long> recipientIds, IEnumerable<string> ipRangedIds);

        void CreateRowsInAlertsReceivedForUser(long usersId, int alertId);

        void CreateRowsInAlertsReceivedForIp(DataSet ipRanges, int alertId);

        HashSet<int> GetSentIpGroupIdsForUser(long userId);

        HashSet<AlertReceived> GetUserAndAlertIdsWithSmsToSend();
    }
}
