﻿namespace DeskAlerts.Server.Utils.Interfactes
{
    using System.Net.Mail;
    using ApplicationCore.Dtos;
    using ApplicationCore.Dtos.Settings;
    using Managers;

    public interface ISettingsService
    {
        PushNotificatiosResponseDto GetPushNotificationResponse();

        string GetLanguageByUserId(int user_id);

        void SetCultureInfoForThread(string cultureInfo);

        DeskAlertsApiResponse<EmailResult> TestSmtpServerConnection(SmtpCredentialsDto credentialsDto);

        EmailResult SendEmail(SmtpCredentialsDto credentialsDto, MailMessage messaget);

        string DateTimeExample(string dateFormat, string timeFormat);

        bool isTrial();
    }
}
