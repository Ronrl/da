﻿namespace DeskAlerts.Server.Utils.Interfactes
{
    public interface IComputerService
    {
        void UpdateComputerLastRequest(string computerName, string computerDomainName);
    }
}
