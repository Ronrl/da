﻿using System;
using System.Collections.Generic;
using DeskAlerts.ApplicationCore.Entities;

namespace DeskAlerts.Server.Utils.Interfaces
{
    public interface ISkinService
    {
        IEnumerable<Skin> GetAllSkins();

        IEnumerable<Skin> GetSkinsByIdsList(IEnumerable<Guid> guidList);

        IEnumerable<Skin> GetSkinsByPolicyId(long policyId);

        void DeleteByPolicyId(long policyId);

        void CreateDependencies(long policyId);

        void CreateSpecificDependencies(long policyId, IEnumerable<Guid> skinsIds);

        string GetSkinsPreviewTemplate(IEnumerable<Guid> skinGuids);

        string GetSkinsFromDbByPolicyGuidList(IEnumerable<Guid> skinGuids);

        string CheckSkinIdInPolicy(string currentSkinId, IEnumerable<Guid> curUserPolicySkinList);

        Skin GetByAlertId(long alertId);
    }
}