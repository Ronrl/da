﻿using DeskAlerts.ContentDeliveringService.Models;

namespace DeskAlerts.Server.Utils.Interfaces.Services
{
    public interface IETagService
    {
        string GetETag(ContentDeliveringKey key);
    }
}