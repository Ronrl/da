﻿using System;

namespace DeskAlerts.Server.Utils.Interfactes
{
    using System.Collections.Generic;
    using ApplicationCore.Entities;
    using PolicyEntity = ApplicationCore.Entities.Policy;
    using PolicyPermission = DeskAlertsDotNet.Models.Permission.Policy;

    interface IPolicyService
    {
        PolicyEntity GetPolicyById(long policyId);

        PolicyPermission GetMergedPolicyByPublisherId(long publisherId);

        PolicyPermission GetDefaultUserPolicy(long userId, bool isAdmin);

        List<PolicyPermission> GetPoliciesByPublisherIdAndAlertType(long publisherId, AlertType alertType);

        List<PolicyEntity> GetPoliciesByPublisherId(long publisherId);

        List<Guid> GetMergedPolicySkinsByPublisherId(long publisherId);

        List<string> GetMergedPolicyContentSettingsByPublisherId(long publisherId);

        IEnumerable<PolicyPublisher> GetPolicyPublisherByPublisherId(long publisherId);

        IEnumerable<PolicyEntity> GetAdministratorPoliciesForDomainsGroups();

        PolicyPublisher AddPolicyPublisher(long userId, long policyId, long groupId);

        void RemovePolicyPublisherByPublisherIdByPolicyIdByGroupId(long publisherId, long policyId, long groupId);

        void RemovePolicyPublisher(PolicyPublisher policy);

        void RemovePolicyFromPublisher(long policyId, long groupId);

        void RemovePolicyPublisherByPolicyIdPublisherId(long policyId, long publisherId);
        bool PolicyExists(string policyName);
    }
}
