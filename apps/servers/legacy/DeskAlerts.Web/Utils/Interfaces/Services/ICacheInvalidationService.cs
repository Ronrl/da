﻿using DeskAlerts.ApplicationCore.Entities;

namespace DeskAlerts.Server.Utils.Interfaces.Services
{
    public interface ICacheInvalidationService
    {
        void CacheInvalidation(AlertType contentType, long contentId);

        void LocalCacheInvalidation(AlertType contentType, long contentId);

        void SiblingsCacheInvalidation(AlertType contentType, long contentId);
    }
}
