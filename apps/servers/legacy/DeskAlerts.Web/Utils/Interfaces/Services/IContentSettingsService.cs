﻿using System.Collections.Generic;
using DeskAlerts.ApplicationCore.Entities;

namespace DeskAlerts.Server.Utils.Interfaces
{
    public interface IContentSettingsService
    {
        IEnumerable<ContentSettings> GetAllContentSettings();

        IEnumerable<ContentSettings> GetContentSettingsByPublisherId(long policyId);

        void DeleteByPolicyId(long policyId);

        void CreateDependencies(long policyId);

        void CreateSpecificDependencies(long policyId, IEnumerable<long> contentOptionIdsList);
    }
}
