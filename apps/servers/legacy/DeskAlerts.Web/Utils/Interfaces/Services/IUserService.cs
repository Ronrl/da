﻿using System.Collections.Generic;

namespace DeskAlerts.Server.Utils.Interfactes
{
    using ApplicationCore.Entities;
    using System;

    public interface IUserService
    {
        char ConnectSign { get; }
        User GetUser(string userName, string md5Password);
        User GetUserById(long userId);
        User GetUserByUserNameAndDomainName(string userName, string domainName);
        Group GetGroupById(long groupId);
        long RegisterPublisher(string userName, long domainId, string password);
        User GetUserByName(string userName);
        IEnumerable<string> GetDomainUsersUsernames(int domainId);
        string ConvertToMD5Hash(string password);
        Tuple<string, string> SeparateUserNameAndDomainName(string fullUserName);
        string GetUserDomainNameByUserId(long userId);
        string JoinUserNameAndDomainName(string userName, string domainName);
        string JoinDeskbarAndUserNameAndDomainName(string deskbar, string userName, string domainName);
        void RemovePublisher(User user);
        void UpdateGroup(Group group);
        void UpdateUserLastRequest(long userId);
        void UpdateUserTimeZone(long userId, int timeZone);
        void UpdatePollPeriod(long userId, string pollPeriod);
        void UpdateVersion(long userId, string version);
        string ConvertToSeconds(string pollPeriod);
    }
}
