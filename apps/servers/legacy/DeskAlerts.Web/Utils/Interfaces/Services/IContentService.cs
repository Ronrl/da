using System.Web.UI.MobileControls;

namespace DeskAlerts.Server.Utils.Interfactes
{
    using ApplicationCore.Interfaces;
    using ApplicationCore.Models;
    using ApplicationCore.Entities;
    using DeskAlertsDotNet.Models;
    using Managers;
    using ApplicationCore.Enums;
    using System;
    using System.Collections.Generic;

    public interface IContentService
    {
        CacheManager CacheManager { get; }
        AlertEntity GetContentById(long id);
        IEnumerable<Alert> GetContentByUser(DateTime localDateTime, Token token, string userName, string domain);
        IEnumerable<Alert> GetContentByComputerName(DateTime localDateTime, Token token, string computerName, string domain);
        IEnumerable<Alert> GetContentByIpGroup(DateTime localDateTime, Token token, string ipAddress);
        IEnumerable<Alert> GetWallpapers(DateTime localDateTime, Token token);
        IEnumerable<Alert> GetLockScreens(DateTime localDateTime, Token token);
        IEnumerable<Alert> GetScreensaver(DateTime localDateTime, Token token);
        IEnumerable<Alert> GetAlerts(DateTime localDateTime, Token token);
        IEnumerable<string> GetConfirmedUsernamesForMultipleAlert(long alertId);
        int GetConfirmedUsersForMultipleAlertAmount(long alertId);
        bool CheckForMultipleAlert(long alertId);
        IEnumerable<long> GetTerminatedAlertsForInstance(DateTime localDateTime, Token token);
        void ReceiveContent(IEnumerable<IReceivedContent> contents, Token token);
        void UpdateAlertEntity(long id, List<KeyValuePair<string, string>> parametersDictionary);
        void UpdateAlertReceived(AlertReceived alertsReceived, AlertReceiveStatus status);
        void TerminateAlert(long contentId, Token token);
        void ChangeStatus(IEnumerable<IReceivedContent> contents, Token token, AlertReceiveStatus status);
        void ReadContent(long alertId, Token token);
        void AddContentRecipientConnectionToDb(long userId, Guid deskbarId, long alertId, int alertRecievedStatus);
        long GetContentsCount();
        long GetLastContentId();

        /// <summary>
        /// Get content for user through new content delivering scheme
        /// </summary>
        /// <param name="localDateTime"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        IEnumerable<long> GetContentForUser(DateTime localDateTime, long userId);

        /// <summary>
        /// Get content for user instance through new content delivering scheme
        /// </summary>
        /// <param name="localDateTime"></param>
        /// <param name="userId"></param>
        /// <param name="userInstanceId"></param>
        /// <returns></returns>
        IEnumerable<long> GetContentForUserInstance(DateTime localDateTime, long userId, Guid userInstanceId);
    }
}
