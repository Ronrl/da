﻿namespace DeskAlertsDotNet.Utils.Factories
{
    using System.Data;
    using System.Data.SqlClient;

    /// <summary>
    /// Class for generation SqlParameters
    /// </summary>
    public class SqlParameterFactory
    {
        /// <summary>
        /// Create instance of class <see cref="SqlParameter"/>
        /// </summary>
        /// <param name="type">Used to indicate <see cref="DbType"/></param>
        /// <param name="value">Used to set value</param>
        /// <param name="parameterName">Used to specify name</param>
        /// <returns>instance of <see cref="SqlParameter"/></returns>
        public static SqlParameter Create(DbType type, object value, string parameterName)
        {
            SqlParameter parameter = new SqlParameter()
            {
                DbType = type,
                Value = value,
                ParameterName = parameterName
            };

            return parameter;
        }
    }
}