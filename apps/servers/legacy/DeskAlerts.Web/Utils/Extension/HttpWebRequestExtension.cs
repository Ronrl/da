﻿using System;
using System.Net;
using System.Text;

namespace DeskAlerts.Server.Utils.Extension
{
    public static class HttpWebRequestExtension
    {
        public static void AddAuthHeaders(this HttpWebRequest request, string login, string password)
        {
            var byteEncodedAuthString = Encoding.UTF8.GetBytes(login + ":" + password);
            request.Headers.Add(HttpRequestHeader.Authorization, "Basic " + Convert.ToBase64String(byteEncodedAuthString));
        }
    }
}