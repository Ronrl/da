﻿namespace DeskAlertsDotNet.Utils.Buttons
{
    using System;
    using System.Web.UI.WebControls;

    using EventArgs;

    public class EditUserButton : LinkButton
    {
        public event EventHandler<EditUserEventArgs> ClickEvent;

        public string UserId { get; set; }

        protected override void OnClick(EventArgs e)
        {
            ClickEvent?.Invoke(this, new EditUserEventArgs(UserId));
            base.OnClick(e);
        }

        protected virtual void OnClickEvent(EditUserEventArgs e)
        {
            ClickEvent?.Invoke(this, e);
        }
    }
}