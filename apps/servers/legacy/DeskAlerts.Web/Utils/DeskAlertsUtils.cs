﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Hosting;
using System.Xml;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Pages;
using DeskAlertsDotNet.Parameters;
using DeskAlertsDotNet.Utils.Consts;
using HtmlAgilityPack;
using NLog;
using Resources;
using DeskAlerts.Server.sever.MODULES.VIDEOALERT;
using Newtonsoft.Json;

namespace DeskAlerts.Server.Utils
{
    public class DeskAlertsUtils
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public static string ServerMapPath = Config.Data.GetString("alertsDir");

        public const int DefaultCompNextRequest = 5;

        private static Dictionary<int, string> classTooptips = new Dictionary<int, string>
                                                                   {
                                                                       { 1, "LNG_ALERT" },
                                                                       { 2, "LNG_SCREENSAVER" },
                                                                       { 4, "LNG_RSS" },
                                                                       { 5, "LNG_RSS" },
                                                                       { 8, "LNG_WALLPAPER" },
                                                                       { 16, "LNG_INSTANT_MESSAGE" },
                                                                       { 64, "LNG_SURVEY" },
                                                                       { 128, "LNG_QUIZ" },
                                                                       { 256, "LNG_POLL" },
                                                                       { 2048, "LNG_ALERT" }
                                                                   };

        public static string GetTooltipForClass(int alertClass)
        {
            try
            {
                string key = classTooptips[alertClass];
                return resources.ResourceManager.GetString(key);
            }
            catch (KeyNotFoundException ex)
            {
                DeskAlertsBasePage.Logger.Debug(ex);
                return string.Empty;
            }
        }

        public static long DateTimeToTimeStamp(DateTime dt)
        {
            var timeSpan = dt.ToUniversalTime().Subtract(new DateTime(1970, 1, 1));// (dt - new DateTime(1970, 1, 1, 0, 0, 0));
            return (long)timeSpan.TotalSeconds;
        }

        public static DateTime DateTimeFromString(string dateTimeText)
        {
            DateTime dateTime = DateTime.ParseExact(dateTimeText, DateTimeFormat.JavaScript, CultureInfo.InvariantCulture);
            return dateTime;
        }

        public static void ShowJavaScriptAlert(HttpResponse response, string text)
        {
            response.Write(string.Format("<script>alert('{0}');</script>", text));
        }

        public static string GetRandomHexColor()
        {
            var result = Guid.NewGuid().ToString().Substring(0, 6);
            return result;
        }

        public static string BeautifyString(string str)
        {
            TextInfo tInfo = CultureInfo.CurrentCulture.TextInfo;
            return tInfo.ToTitleCase(str);
        }

        public static string GetFullDateTimeFormat(DBManager dbMgr)
        {
            return GetDateFormat(dbMgr) + " " + GetTimeFormat(dbMgr);
        }

        public static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
        {
            try
            {
                // Get the subdirectories for the specified directory.
                DirectoryInfo dir = new DirectoryInfo(sourceDirName);
                DirectoryInfo[] dirs = dir.GetDirectories();

                if (!dir.Exists)
                {
                    throw new DirectoryNotFoundException(
                        "Source directory does not exist or could not be found: "
                        + sourceDirName);
                }

                // If the destination directory doesn't exist, create it.
                if (!Directory.Exists(destDirName))
                {
                    Directory.CreateDirectory(destDirName);
                }

                // Get the files in the directory and copy them to the new location.
                FileInfo[] files = dir.GetFiles();
                foreach (FileInfo file in files)
                {
                    string temppath = Path.Combine(destDirName, file.Name);
                    file.CopyTo(temppath, true);
                }

                // If copying subdirectories, copy them and their contents to new location.
                if (copySubDirs)
                {
                    foreach (DirectoryInfo subdir in dirs)
                    {
                        string temppath = Path.Combine(destDirName, subdir.Name);
                        DirectoryCopy(subdir.FullName, temppath, copySubDirs);
                    }
                }
            }
            catch (IOException ex)
            {
                Logger.Error(ex, ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                throw;
            }
        }

        public static string AppendParameterToUrl(string url, string paramKeyValue, object paramValue)
        {
            if (url.Contains("?"))
            {
                url += "&";
            }
            else
            {
                url += "?";
            }

            url += paramKeyValue + "=" + paramValue;

            return url;
        }

        public static string AppendParameterToUrl(string url, string paramKeyValue)
        {
            string result;

            if (url.Contains(paramKeyValue))
            {
                return url;
            }

            if (url.Contains("?"))
            {
                result = url + "&";
            }
            else
            {
                result = url + "?";
            }

            result += paramKeyValue;

            return result;
        }

        public static string GetDateFormat(DBManager dbMgr)
        {
            object val = dbMgr.GetScalarByQuery("SELECT val FROM settings WHERE name = 'ConfDateFormat'");

            return val.GetType() == typeof(DBNull) ? string.Empty : val.ToString();
        }

        public static string GetTimeFormat(DBManager dbMgr, bool convert = true)
        {
            object val = dbMgr.GetScalarByQuery("SELECT val FROM settings WHERE name = 'ConfTimeFormat'");

            if (val.GetType() != typeof(DBNull))
            {
                if (!convert)
                    return val.ToString();

                if (val.ToString().Equals("2"))
                {
                    return "hh:mm tt";
                }
            }

            return "HH:mm";
        }

        public static string GetLocaltime(DateTime requestLocaltime)
        {
            return requestLocaltime.ToString(DateTimeFormat.Client); 
        }

        

        public static string HTMLToText(string HTMLCode)
        {
            // Remove new lines since they are not visible in HTML
            HTMLCode = HTMLCode.Replace("\n", " ");

            // Remove tab spaces
            HTMLCode = HTMLCode.Replace("\t", " ");

            // Replace double quores with single
            HTMLCode = HTMLCode.Replace("\"", "'");

            // Remove HTML space(&nbsp) to empty space
            HTMLCode = HTMLCode.Replace("&nbsp;", " ");

            // Remove multiple white spaces from HTML
            HTMLCode = Regex.Replace(HTMLCode, "\\s+", " ");

            // Remove HEAD tag
            HTMLCode = Regex.Replace(
                HTMLCode,
                "<head.*?</head>",
                string.Empty,
                RegexOptions.IgnoreCase | RegexOptions.Singleline);

            // Remove any JavaScript
            HTMLCode = Regex.Replace(
                HTMLCode,
                "<script.*?</script>",
                string.Empty,
                RegexOptions.IgnoreCase | RegexOptions.Singleline);

            // Finally, remove all HTML tags and return plain text
            HTMLCode = Regex.Replace(HTMLCode, "<[^>]*>", string.Empty);

            // Replace special characters like &, <, >, " etc.
            StringBuilder sbHTML = new StringBuilder(HTMLCode);

            // Note: There are many more special characters, these are just
            // most common. You can add new characters in this arrays if needed
            sbHTML = ReplaceSpecialCharactersInMarkup(sbHTML);
            sbHTML = AddLineBreaksToMarkup(sbHTML);

            return sbHTML.ToString();

        }

        /// <summary>
        /// Remove special  characters to their html values
        /// </summary>
        /// <param name="HTMLmarkup">get html markup that would be changed by this method</param>
        /// <returns></returns>
        public static StringBuilder ReplaceSpecialCharactersInMarkup(StringBuilder HTMLmarkup)
        {
            return HTMLmarkup.Replace("<", "&lt;").Replace("&", "&amp;");
        }

        /// <summary>
        /// Remove HTML <p> tag
        /// </summary>
        /// <param name="html">input HTML string</param>
        /// <returns>HTML string without <p> tag</returns>
        public static string EscapeHtmlTagParagraph(string html)
        {
            string result = html.Replace("<p>", string.Empty).Replace("</p>", string.Empty);
            return result;
        }

        /// <summary>
        /// Remove XML comments
        /// </summary>
        /// <param name="html">input HTML string</param>
        /// <returns>string wthiout XML comments</returns>
        public static string EscapeXmlComments(string html)
        {
            string result = Regex.Replace(html, "<!--.*?-->", string.Empty);
            return result;
        }

        /// <summary>
        /// The method extract text from HTML string
        /// </summary>
        /// <param name="html">HTML string</param>
        /// <returns></returns>
        public static string ExtractTextFromHtml(string html)
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(html);
            return doc.DocumentNode.InnerText;
        }

        /// <summary>
        /// This method adds line breaks after special html tags
        /// </summary>
        /// <param name="HTMLcode">Html markup for styling</param>
        /// <returns></returns>
        public static StringBuilder AddLineBreaksToMarkup(StringBuilder HTMLmarkup)
        {
            return HTMLmarkup.Replace("<br>", "\n<br>").Replace("<br ", "\n<br ").Replace("<p ", "\n<p ");
        }

        #region AddAttributeToTag

        public static string AddAttributeToTag(string source, string tag, string attr, string attrValue)
        {
            try
            {
                var xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(source);
                RecurenceXmlNode(xmlDoc, tag, attr, attrValue, xmlDoc);
                return xmlDoc.OuterXml;
            }
            catch (Exception ex)
            {
                DeskAlertsBasePage.Logger.Debug(ex);
                return source;
            }
        }

        private static void RecurenceXmlNode(XmlNode xnl, string tag, string attr, string attrValue, XmlDocument xmlDoc)
        {
            foreach (XmlNode node in xnl.ChildNodes)
            {
                if (node.ChildNodes != null)
                {
                    foreach (XmlNode childNode in node.ChildNodes)
                    {
                        RecurenceXmlNode(node, tag, attr, attrValue, xmlDoc);
                    }
                }

                if (node.Name == "a")
                {
                    if (node.Attributes[tag] != null)
                    {
                        node.Attributes[tag].Value = attr;
                    }
                    else
                    {
                        var newAttr = xmlDoc.CreateAttribute(attr);
                        newAttr.Value = attrValue;
                        node.Attributes.Append(newAttr);
                    }
                }
            }
        }

        #endregion AddAttributeToTag

        private static string _path;
        private static bool _logFileExist = File.Exists(PathToLogs);

        private static string PathToLogs
        {
            get
            {
                if (string.IsNullOrEmpty(_path))
                {
                    _path = HostingEnvironment.MapPath("~") + @"\admin\logs\log.txt";
                }

                return _path;
            }
        }

        public static string EscapeUnderscore(string searchTerm)
        {
            if (searchTerm.Contains("_") && !searchTerm.Contains("[_]"))
            {
                searchTerm = searchTerm.Replace(@"_", @"[_]");
            }

            return searchTerm;
        }

        /// <summary>
        /// Detect positive or negative value
        /// </summary>
        /// <param name="notRecievedCount">positive or negative value</param>
        /// <returns>int value</returns>
        public static int DetectPositiveOrNegativeValue(int notRecievedCount)
        {
            // Detect positive or negative value
            int isPositiveOrNegativeNumber = Math.Sign(notRecievedCount);

            // assign a value to 0 if the number is negative
            if (isPositiveOrNegativeNumber == -1)
            {
                return 0;
            }
            else
            {
                return notRecievedCount;
            }
        }

        /// <summary>
        /// Encrypt RC4 the message with encrypt key
        /// </summary>
        /// <param name="text">The text message</param>
        /// <param name="encryptKey">The key to encrypt</param>
        /// <param name="isUriEscape">Need escape Uri symbols</param>
        /// <returns></returns>
        public static byte[] GetEncryptedBytes(string text, string encryptKey)
        {
            string aspEncryptorBaseUrl = ServerMapPath + "/Utils/Encryption/rc4_encryptor.asp";
            using (var webClient = new HttpClient())
            {
                text = Uri.EscapeDataString(text);
                var content = new StringContent($"text={text}&encrypt_key={encryptKey}", Encoding.UTF8, "application/x-www-form-urlencoded");
                var result = webClient.PostAsync(aspEncryptorBaseUrl, content).Result;
                var bytes = result.Content.ReadAsByteArrayAsync().Result;
                return bytes;
            }
        }

        private static readonly DateTime EmptyDateTime = new DateTime(1970, 1, 1);

        public static bool IsNotEmptyDateTime(DateTime lastRequest)
        {
            return lastRequest != EmptyDateTime && lastRequest != DateTime.MinValue;
        }

        public static bool IsUserOnline(DateTime lastRequest, int nextRequest)
        {
            if (lastRequest == DateTime.MinValue)
            {
                return false;
            }

            var additionalTime = nextRequest < 60
                ? 5
                : nextRequest < 600
                    ? 30
                    : 60;

            var lastRequestSeconds = (int)(DateTime.Now - lastRequest).TotalSeconds;
            return lastRequestSeconds < nextRequest + additionalTime;
        }

        public static bool IsCompOnline(DateTime lastRequest, int nextRequest)
        {
            if (lastRequest == DateTime.MinValue)
            {
                return false;
            }

            if (nextRequest == 0)
            {
                nextRequest = DefaultCompNextRequest;
            }

            var additionalTime = nextRequest < 60
                ? 5
                : nextRequest < 600
                    ? 30
                    : 60;

            var lastRequestMinutes = (int)(DateTime.Now - lastRequest).TotalMinutes;
            return lastRequestMinutes < nextRequest + additionalTime;
        }

        private const string VideoAlertPrefix = "%video%=";

        public static VideoAlertSettingsDto ParseVideoAlertSettings(string alertText)
        {
            if (!alertText.Contains(VideoAlertPrefix) && !alertText.StartsWith(VideoAlertPrefix))
            {
                throw new ArgumentException(nameof(alertText));
            }

            const int bracketSize = 1;
            VideoAlertSettingsDto videAlertSettings;
            var description = alertText.Substring(0,
                alertText.IndexOf(VideoAlertPrefix, StringComparison.Ordinal) - bracketSize);
            alertText = alertText.Substring(description.Length + VideoAlertPrefix.Length + bracketSize);

            if (alertText.StartsWith("|"))
            {
                var videoParts = alertText.Split('|');
                videAlertSettings = new VideoAlertSettingsDto
                {
                    Autoplay = true,
                    Controls = true,
                    Loop = false,
                    Mute = false,
                    Link = videoParts[1],
                    Width = videoParts[2],
                    Height = videoParts[3],
                    Description = description
                };
            }
            else
            {
                var leftBracketIndex = alertText.LastIndexOf('{');
                var rightBracketIndex = alertText.LastIndexOf('}');
                var jsonText = alertText.Substring(leftBracketIndex, rightBracketIndex - leftBracketIndex + bracketSize);
                videAlertSettings = JsonConvert.DeserializeObject<VideoAlertSettingsDto>(jsonText);
                videAlertSettings.Description = description;
            }

            return videAlertSettings;
        }

        public static string GenerateVideoAlertHtml(string alertText)
        {
            var videoAlertSettings = ParseVideoAlertSettings(alertText);

            var mute = videoAlertSettings.Mute ? " muted" : string.Empty;
            var controls = videoAlertSettings.Controls ? " controls" : string.Empty;
            var loop = videoAlertSettings.Loop ? " loop" : string.Empty;
            var autoplay = videoAlertSettings.Autoplay ? " autoplay" : string.Empty;
            var htmlTitleComment = ParseHtmlTitleComment(alertText);

            var result = $"<video src='{videoAlertSettings.Link}' width='{videoAlertSettings.Width}' height='{videoAlertSettings.Height}' type='video/mp4' preload{mute}{controls}{loop}{autoplay}></video>{videoAlertSettings.Description}{htmlTitleComment}";

            return result;
        }

        public static string ParseHtmlTitleComment(string alertText)
        {
            if (string.IsNullOrEmpty(alertText))
            {
                return string.Empty;
            }

            var regExp = new Regex(@"<!--(\s)?html_title(\s)?=(\s)?'(.*)'(\s)?-->");
            var result = regExp.Match(alertText)?.Groups[0]?.Value;

            return result;
        }
    }
}