using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DeskAlerts.ContentDeliveringService.Interfaces;
using DeskAlerts.ApplicationCore.Dtos;
using DeskAlerts.ApplicationCore.Dtos.Security;
using DeskAlerts.ApplicationCore.Models.ContentTypes;
using DeskAlerts.Server.Utils.Consts;
using DeskAlerts.Server.Utils.Interfactes;
using DeskAlerts.Server.Utils.Services;
using DeskAlertsDotNet.Application;
using NLog;

namespace DeskAlerts.Server.Api.v1.Security
{
    [RoutePrefix("api/v1")]
    public class SecurityController : ApiController
    {
        private readonly IUserService _userService;
        private readonly ITokenService _tokenService = Global.Container.Resolve<ITokenService>();
        private readonly UserInstanceService _userInstanceService;
        private readonly IContentDeliveringService<WallpaperContentType> _wallpaperContentDeliveringService;
        private readonly IContentDeliveringService<LockscreenContentType> _lockscreenContentDeliveringService;
        private readonly IContentDeliveringService<ScreensaverContentType> _screensaversContentDeliveringService;
        private readonly IContentDeliveringService<TickerContentType> _tickerContentDeliveringService;
        private readonly IContentDeliveringService<RsvpContentType> _rsvpContentDeliveringService;
        private readonly IContentDeliveringService<AlertContentType> _alertContentDeliveringService;
        private readonly IContentDeliveringService<SurveyContentType> _surveyContentDeliveringService;
        private readonly IContentDeliveringService<TerminatedContentType> _terminatedContentDeliveringService;

        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public const string BearerPrefix = "Bearer ";

        public SecurityController(
            IUserService userService,
            IContentDeliveringService<WallpaperContentType> wallpaperContentDeliveringService,
            IContentDeliveringService<LockscreenContentType> lockscreenContentDeliveringService,
            IContentDeliveringService<ScreensaverContentType> screensaversContentDeliveringService,
            IContentDeliveringService<TickerContentType> tickerContentDeliveringService,
            IContentDeliveringService<RsvpContentType> rsvpContentDeliveringService,
            IContentDeliveringService<AlertContentType> alertContentDeliveringService,
            IContentDeliveringService<SurveyContentType> surveyContentDeliveringService,
            IContentDeliveringService<TerminatedContentType> terminatedContentDeliveringService)
        {
            _userService = userService;
            _userInstanceService = new UserInstanceService();          
            _wallpaperContentDeliveringService = wallpaperContentDeliveringService;
            _lockscreenContentDeliveringService = lockscreenContentDeliveringService;
            _screensaversContentDeliveringService = screensaversContentDeliveringService;
            _tickerContentDeliveringService = tickerContentDeliveringService;
            _rsvpContentDeliveringService = rsvpContentDeliveringService;
            _alertContentDeliveringService = alertContentDeliveringService;
            _surveyContentDeliveringService = surveyContentDeliveringService;
            _terminatedContentDeliveringService = terminatedContentDeliveringService;
        }

        #region api/v1/security/token/aduser
        [HttpPost, Route("security/token/aduser")]
        public HttpResponseMessage GetTokenForAdUser(AuthorizeDomainUserRequestDto request)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            try
            {
                var trimmedDeskbarId = request.DeskbarId.TrimStart('{').TrimEnd('}');

                if (string.IsNullOrEmpty(trimmedDeskbarId))
                {
                    _logger.Info($"The DeskbarId for user {request.UserName} requested token is invalid");
                }

                var token = _tokenService.CreateTokenForDomainUser(request.UserName, request.ComputerName, request.ComputerDomainName, request.DomainName, request.Ip, trimmedDeskbarId, request.DeviceType);
                var userId = token.User.Id;

                _tokenService.AddToken(token);
                _logger.Info($"The security token for user {request.UserName} with domain name {request.DomainName} has been successfully added.");
                var responseObject = new DeskAlertsApiResponse<TokenResponseDto>
                {
                    Data = new TokenResponseDto
                    {
                        Authorization = BearerPrefix + token.Guid
                    }
                };

                _userService.UpdateVersion(userId, request.ClientVersion);
                _userService.UpdatePollPeriod(userId, request.PollPeriod);
                var userInfo = new UserInfoDto
                {
                    Username = token.UserName,
                    Password = token.User.Password,
                    Phone = token.User.Phone,
                    Email = token.User.Email,
                    DomainName = token.DomainName,
                    DeskbarId = token.DeskbarId
                };

                _userInstanceService.HandleNewUserInstance(userInfo, request.Ip, token.ComputerName, token.ComputerDomainName);

                _wallpaperContentDeliveringService.DeliveryToUser(userId);
                _lockscreenContentDeliveringService.DeliveryToUser(userId);
                _screensaversContentDeliveringService.DeliveryToUser(userId);
                _tickerContentDeliveringService.DeliveryToUser(userId);
                _rsvpContentDeliveringService.DeliveryToUser(userId);
                _alertContentDeliveringService.DeliveryToUser(userId);
                _surveyContentDeliveringService.DeliveryToUser(userId);
                _terminatedContentDeliveringService.DeliveryToUser(userId);

                _logger.Info($"Authorization token has been received by user {userInfo.Username} with deskbarId { trimmedDeskbarId } and id {userId}");
                return Request.CreateResponse(HttpStatusCode.OK, responseObject);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.OK, new DeskAlertsApiResponse<object>
                {
                    ErrorMessage = $"An error has occured while getting token. Error: {ex}.",
                    Status = ResponseStatus.Error
                });
            }
        }
        #endregion

        #region api/v1/security/token/user
        [HttpPost, Route("security/token/user")]
        public HttpResponseMessage GetTokenForUser(AuthorizeUserRequestDto request)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            try
            {
                var trimmedDeskbarId = request.DeskbarId.TrimStart('{').TrimEnd('}');

                if (string.IsNullOrEmpty(trimmedDeskbarId))
                {
                    _logger.Debug($"The DeskbarId for user {request.UserName} requested token is invalid");
                }

                var token = _tokenService.CreateTokenForUser(
                    request.UserName, request.ComputerName, request.ComputerDomainName, request.Ip, request.Md5Password, trimmedDeskbarId, request.DeviceType);

                if (token == null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new DeskAlertsApiResponse<object>
                    {
                        ErrorMessage = $"Can't create token for username: \"{request.UserName}\". Probably user is not in database.",
                        Status = ResponseStatus.Error
                    });
                }

                _tokenService.AddToken(token);

                var userId = token.User.Id;
                _logger.Debug($"The security token for user {request.UserName} and deskbarId { trimmedDeskbarId } and id {userId} has been successfully added.");

                var responseObject = new DeskAlertsApiResponse<TokenResponseDto>
                {
                    Data = new TokenResponseDto
                    {
                        Authorization = BearerPrefix + token.Guid
                    }
                };

                _userService.UpdateVersion(userId, request.ClientVersion);
                _userService.UpdatePollPeriod(userId, request.PollPeriod);
                var userInfo = new UserInfoDto
                {
                    Username = token.UserName,
                    Password = token.User.Password,
                    Phone = token.User.Phone,
                    Email = token.User.Email,
                    DomainName = token.DomainName == SecurityVariables.EmbeddedDomain ? string.Empty : token.DomainName,
                    DeskbarId = token.DeskbarId
                };

                _userInstanceService.HandleNewUserInstance(userInfo, request.Ip, token.ComputerName, token.ComputerDomainName);

                _wallpaperContentDeliveringService.DeliveryToUser(userId);
                _lockscreenContentDeliveringService.DeliveryToUser(userId);
                _screensaversContentDeliveringService.DeliveryToUser(userId);
                _tickerContentDeliveringService.DeliveryToUser(userId);
                _rsvpContentDeliveringService.DeliveryToUser(userId);
                _alertContentDeliveringService.DeliveryToUser(userId);
                _surveyContentDeliveringService.DeliveryToUser(userId);
                _terminatedContentDeliveringService.DeliveryToUser(userId);

                _logger.Info($"Authorization token has been received by user {userInfo.Username} with deskbarId {userInfo.DeskbarId} and id {userId}");
                return Request.CreateResponse(HttpStatusCode.OK, responseObject);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.OK, new DeskAlertsApiResponse<object>
                {
                    ErrorMessage = $"An error has occured while getting token. Error: {ex}.",
                    Status = ResponseStatus.Error
                });
            }
        }
        #endregion

        #region Methods
        
        #endregion
    }
}
