﻿using DeskAlertsDotNet.Application;
using DeskAlerts.ApplicationCore.Dtos;
using DeskAlerts.Server.Utils.Interfactes;
using System;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace DeskAlertsDotNet.Api.v1.Security
{
    public class TokenAuthorizeAttribute : AuthorizeAttribute
    {
        private readonly ITokenService _tokenService = Global.Container.Resolve<ITokenService>();
        private const string AuthHeaderAttr = "Authorization";

        public override void OnAuthorization(HttpActionContext actionContext)
        {
            try
            {
                var tokenGuid = actionContext.Request.Headers.GetValues(AuthHeaderAttr).First();

                if (!_tokenService.ValidateToken(tokenGuid))
                {
                    //HandleUnauthorizedRequest(actionContext);
                    actionContext.Response = actionContext.ControllerContext.Request.CreateResponse(System.Net.HttpStatusCode.OK, 
                        new DeskAlertsApiResponse<object>
                    {
                        ErrorMessage = "Not authorized.",
                        Status = ResponseStatus.NA
                    });
                }
            }
            catch
            {
                throw new Exception("Authorization error has occured.");
            }
        }
    }
}