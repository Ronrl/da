﻿using DeskAlerts.ApplicationCore.Dtos;
using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.ApplicationCore.Models;
using DeskAlertsDotNet.Utils.Services;
using NLog;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DeskAlerts.Server.Utils.Services;

namespace DeskAlerts.Server.Api.v1
{
    [RoutePrefix("api/v1")]
    public class AutoupdateController : ApiController
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        private readonly IAutoupdateService _autoupdateService;

        public AutoupdateController()
        {
            _autoupdateService = new AutoupdateService();
        }

        [HttpGet, Route("autoupdate/deleteFile")]
        public string DeleteAutoUpdateFile()
        {
            try
            {
                _autoupdateService.DeleteFile();
                return "success";
            }
            catch (Exception ex)
            {
                _logger.Debug(ex);
                return null;
            }
        }

        [HttpGet, Route("client/version")]
        public HttpResponseMessage GetClientVersion()
        {
            try
            {
                var responseObject =
                    _autoupdateService.Version == AutoupdateService.FileDoesNotExist ?
                    new DeskAlertsApiResponse<object> { Data = "File does not exist on server", Status = ResponseStatus.Error } :
                    new DeskAlertsApiResponse<object> { Data = new ClientApplicationDto() { Version = _autoupdateService.Version } };
                return Request.CreateResponse(HttpStatusCode.OK, responseObject);
            }
            catch (Exception ex)
            {
                _logger.Debug(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    new DeskAlertsApiResponse<object>
                    {
                        Data = null,
                        ErrorMessage = "Error is occured when try to get version of the client",
                        Status = ResponseStatus.Error
                    });
            }
        }

        [HttpPost, Route("client/slot")]
        public HttpResponseMessage GetSlot(ClientApplicationDto client)
        {
            try
            {
                var version = new ClientApplicationInfo(client.Version, client.DeskbarId);
                var slot = _autoupdateService.GetSlot(version);
                if (slot != null)
                {
                    var responseObject = new DeskAlertsApiResponse<AutoUpdateSlotDto> { Data = slot };
                    return Request.CreateResponse(HttpStatusCode.OK, responseObject);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
            }
            catch (Exception ex)
            {
                _logger.Debug(ex);
                var responseObject = new DeskAlertsApiResponse<object>
                {
                    Data = null,
                    ErrorMessage = "Error is occured when try to get slot for the client",
                    Status = ResponseStatus.Error
                };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, responseObject);
            }
        }

        [HttpPost, Route("client/slot/delete")]
        public HttpResponseMessage DeleteSlot(AutoUpdateSlotDto slotDto)
        {
            try
            {
                var guid = Guid.Parse(slotDto.SlotId);
                _autoupdateService.DeleteSlotById(guid);
                var responseObject = new DeskAlertsApiResponse<string> { Data = "Ok" };
                return Request.CreateResponse(HttpStatusCode.OK, responseObject);
            }
            catch (Exception ex)
            {
                _logger.Debug(ex);
                var responseObject = new DeskAlertsApiResponse<object>
                {
                    Data = null,
                    ErrorMessage = "Error is occured when try to delete slot for the client",
                    Status = ResponseStatus.Error
                };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, responseObject);
            }
        }
    }
}
