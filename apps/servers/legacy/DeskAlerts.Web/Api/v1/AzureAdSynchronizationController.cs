﻿using DeskAlerts.Server.Utils.Interfactes;
using DeskAlertsDotNet.Application;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using DeskAlerts.ApplicationCore.Clients;
using DeskAlerts.ApplicationCore.Dtos;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Exceptions;
using DeskAlerts.ApplicationCore.Interfaces;

using DeskAlertsDotNet.Parameters;

using NLog;
using System.Collections.Generic;

namespace DeskAlertsDotNet.Api.V1
{
    [RoutePrefix("api/v1/synchronization/azure")]
    public class AzureAdSynchronizationController : ApiController
    {
        private readonly IDomainRepository _domainRepository = Global.Container.Resolve<IDomainRepository>();
        private readonly IGroupRepository _groupRepository = Global.Container.Resolve<IGroupRepository>();
        private readonly IUserRepository _userRepository = Global.Container.Resolve<IUserRepository>();
        private readonly ITokenService _tokenService = Global.Container.Resolve<ITokenService>();

        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        [HttpPost]
        [Route("perform")]
        public HttpResponseMessage PerformSynchronization()
        {
            try
            {
                var synchronizationResult = new AzureAdSynchronizationResult();

                var domainTenant = Settings.Content["ConfAzureAdTenant"];
                var pollingApplicationId = Settings.Content["ConfAzureAdPollingAppId"];
                var pollingApplicationSecretKey = Settings.Content["ConfAzureAdPollingAppSecret"];
                var azureAdClient = new GraphApiAzureAdClient(domainTenant, pollingApplicationId, pollingApplicationSecretKey);

                var usersFromAzureAd = azureAdClient.GetUsers();
                var groupsFromAzureAd = azureAdClient.GetGroups();

                groupsFromAzureAd = RemoveDistribustionLists(groupsFromAzureAd);

                Domain azureAdDomain;

                try
                {
                    azureAdDomain = _domainRepository.GetByName("Azure Active Directory");
                }
                catch (EntityNotFoundException)
                {
                    _domainRepository.Create(new Domain { Name = "Azure Active Directory" });
                    azureAdDomain = _domainRepository.GetByName("Azure Active Directory");
                }

                var usersFromDb = _userRepository.GetChildUsersByDomainName("Azure Active Directory");
                var groupsFromDb = _groupRepository.GetChildGroupsByDomainName("Azure Active Directory");

                var usersToDelete = (from dbUser in usersFromDb
                                     let usernames = from adUser in usersFromAzureAd
                                                     select adUser.Username
                                     where usernames.Contains(dbUser.Username) == false
                                     select dbUser).ToList();

                foreach (var user in usersToDelete)
                {
                    _userRepository.Delete(user);
                    _tokenService.RemoveToken(user.Username);
                    synchronizationResult.DeletedUsers++;
                }

                var groupsToDelete = (from dbGroup in groupsFromDb
                                      let names = from adGroup in groupsFromAzureAd
                                                  select adGroup.Name
                                      where names.Contains(dbGroup.Name) == false
                                      select dbGroup).ToList();

                foreach (var group in groupsToDelete)
                {
                    _groupRepository.Delete(group);
                    synchronizationResult.DeletedGroups++;
                }

                var usersToUpdate = (from dbUser in usersFromDb
                                     let usernames = from adUser in usersFromAzureAd
                                                     select adUser.Username
                                     where usernames.Contains(dbUser.Username)
                                     select dbUser).ToList();

                foreach (var user in usersToUpdate)
                {
                    var azureAdUser = usersFromAzureAd.Find(u => u.Username == user.Username);
                    user.DisplayName = azureAdUser.DisplayName;
                    user.Email = azureAdUser.Email;
                    user.Phone = azureAdUser.Phone;
                    user.Username = azureAdUser.Username;
                    _userRepository.Update(user);
                    synchronizationResult.UpdatedUsers++;
                }

                var usersToCreate = (from adUser in usersFromAzureAd
                                     let usernames = from dbUser in usersFromDb
                                                     select dbUser.Username
                                     where usernames.Contains(adUser.Username) == false
                                     select adUser).ToList();

                foreach (var azureAdUser in usersToCreate)
                {
                    var newUser = new User
                                      {
                                          Username = azureAdUser.Username,
                                          DisplayName = azureAdUser.DisplayName,
                                          Email = azureAdUser.Email,
                                          Phone = azureAdUser.Phone,
                                          Role = DeskAlerts.ApplicationCore.Entities.User.UserRole,
                                          DomainId = azureAdDomain.Id
                                      };
                    _userRepository.Create(newUser);
                    synchronizationResult.CreatedUsers++;
                }

                var groupsToCreate = (from adGroup in groupsFromAzureAd
                                      let names = from dbGroup in groupsFromDb
                                                  select dbGroup.Name
                                      where names.Contains(adGroup.Name) == false
                                      select adGroup).ToList();

                foreach (var azureAdGroup in groupsToCreate)
                {
                    var newGroup = new Group { Name = azureAdGroup.Name, DomainId = azureAdDomain.Id };
                    _groupRepository.Create(newGroup);
                    synchronizationResult.CreatedGroups++;
                }

                usersFromDb = _userRepository.GetChildUsersByDomainName("Azure Active Directory");
                groupsFromDb = _groupRepository.GetChildGroupsByDomainName("Azure Active Directory");

                foreach (var groupFromAzureAd in groupsFromAzureAd)
                {
                    try
                    {
                        var members = azureAdClient.GetGroupMembers(groupFromAzureAd);

                        var parent = groupsFromDb.Find(g => g.Name == groupFromAzureAd.Name);

                        var childUsersFromDb = _userRepository.GetChildUsersByGroupName(parent.Name);
                        var childGroupsFromDb = _groupRepository.GetChildGroupsByGroupName(parent.Name);

                        var childUsersFromAzureAd = (from azureAdUser in usersFromAzureAd
                                                     join member in members on azureAdUser.Id equals member.Id
                                                     select azureAdUser).ToList();
                        var childGroupsFromAzureAd = (from azureAdGroup in groupsFromAzureAd
                                                      join member in members on azureAdGroup.Id equals member.Id
                                                      select azureAdGroup).ToList();

                        var usersToDeleteFromGroup = (from dbUser in childUsersFromDb
                                                      let usernames = from adUser in childUsersFromAzureAd
                                                                      select adUser.Username
                                                      where usernames.Contains(dbUser.Username) == false
                                                      select dbUser).ToList();

                        foreach (var user in usersToDeleteFromGroup)
                        {
                            _userRepository.RemoveUserFromGroup(user, parent);
                        }

                        var groupsToDeleteFromGroup = (from dbGroup in childGroupsFromDb
                                                       let names = from adGroup in childGroupsFromAzureAd
                                                                   select adGroup.Name
                                                       where names.Contains(dbGroup.Name) == false
                                                       select dbGroup).ToList();

                        foreach (var group in groupsToDeleteFromGroup)
                        {
                            _groupRepository.RemoveGroupFromGroup(group, parent);
                        }

                        var usersToAddToGroup = (from adUser in childUsersFromAzureAd
                                                 let usernames = from dbUser in childUsersFromDb
                                                                 select dbUser.Username
                                                 where usernames.Contains(adUser.Username) == false
                                                 select adUser).ToList();

                        foreach (var azureAdUser in usersToAddToGroup)
                        {
                            var user = usersFromDb.Find(u => u.Username == azureAdUser.Username);
                            _userRepository.AddUserToGroup(user, parent);
                        }

                        var groupsToAddToGroup = (from adGroup in childGroupsFromAzureAd
                                                  let names = from dbGroup in childGroupsFromDb
                                                              select dbGroup.Name
                                                  where names.Contains(adGroup.Name) == false
                                                  select adGroup).ToList();

                        foreach (var azureAdGroup in groupsToAddToGroup)
                        {
                            var group = groupsFromDb.Find(u => u.Name == azureAdGroup.Name);
                            _groupRepository.AddGroupToGroup(group, parent);
                        }
                    }
                    catch (GraphApiErrorException)
                    {
                        _logger.Debug($"Couldn't get members of {groupFromAzureAd.Name} group");
                    }
                }

                var responseObject = new DeskAlertsApiResponse<AzureAdSynchronizationResult> { Data = synchronizationResult };
                return Request.CreateResponse(HttpStatusCode.OK, responseObject);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                _logger.Debug(ex);
                var responseObject = new DeskAlertsApiResponse<object>
                                         {
                                             Data = null,
                                             ErrorMessage =
                                                 "Synchronization was not perfomed. Check your settings and try again later",
                                             Status = ResponseStatus.Error
                                         };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, responseObject);
            }
        }

        private List<AzureAdGroup> RemoveDistribustionLists(List<AzureAdGroup> groupsFromAzureAd)
        {
            var result = new List<AzureAdGroup>(groupsFromAzureAd.Count);

            foreach (var group in groupsFromAzureAd)
            {
                if (!group.IsDistributionList())
                {
                    result.Add(group);
                }
            }

            return result;
        }
    }
}