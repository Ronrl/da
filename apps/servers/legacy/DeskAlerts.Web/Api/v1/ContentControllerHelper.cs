﻿using DeskAlerts.ApplicationCore.Dtos.Content;
using DeskAlerts.ApplicationCore.Models;
using DeskAlerts.ApplicationCore.Utilities;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Models;
using DeskAlertsDotNet.Utils.Factories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using DataSet = DeskAlertsDotNet.DataBase.DataSet;


namespace DeskAlerts.Server.Api.v1
{
    public class ContentControllerHelper
    {
        /// <summary>
        /// Get <see cref="Alert"/> class by content id.
        /// </summary>
        /// <param name="contentId">Content id.</param>
        /// <returns><see cref="Alert"/> entity.</returns>
        public static Alert GetAlertById(long contentId)
        {
            // TODO: feature/AddContentCds : Delete after all implementation.
            // Copy from cache manager and add alertId.
            const string selectActualAlertsForRecipientsQuery = @"
                SELECT DISTINCT TOP (@maximumNumberOfEntries) 
                    a.id, 
                    a.title, 
                    a.type2, 
                    a.class, 
                    a.urgent, 
                    a.ticker, 
                    a.ticker_speed AS tickerspeed, 
                    a.device, 
                    a.fullscreen, 
                    a.sent_date AS sentdate, 
                    a.sender_id AS senderid, 
                    a.aknown, 
                    a.lifetime,
                    CASE
                        WHEN CAST(a.caption_id AS VARCHAR(36)) IS NULL
                        THEN 'default'
                        ELSE CAST(a.caption_id AS VARCHAR(36))
                    END AS captionid, 
                    a.alert_width AS width, 
                    a.alert_height AS height, 
                    a.resizable, 
                    a.autoclose, 
                    a.self_deletable AS selfdeletable, 
                    a.create_date AS createdate, 
                    a.schedule, 
                    a.schedule_type, 
                    a.email, 
                    a.sms, 
                    a.text_to_call AS texttocall, 
                    a.approve_status, 
                    a.from_date AS fromdate, 
                    a.to_date AS todate, 
                    d.name AS user_domain, 
                    u.name AS username, 
                    sm.id AS surveyid, 
                    u.name AS username, 
                    d.name AS user_domain, 
                    ui.clsid AS deskbar, 
                    a.campaign_id AS campaignid
                FROM alerts AS a
                     LEFT JOIN surveys_main AS sm ON a.id = sm.sender_id
                     JOIN alerts_received AS als ON als.alert_id = a.id
                     JOIN users AS u ON u.id = als.user_id
                     LEFT JOIN domains AS d ON u.domain_id = d.id
                     JOIN user_instances AS ui ON ui.user_id = u.id
                WHERE a.to_date > @Now
                      AND a.from_date < @Now
                      AND a.schedule_type = 1
                      AND a.class <> 4
                      AND a.id = @alertId;";

            var maximumNumberOfEntries = SqlParameterFactory.Create(DbType.Int32, AppConfiguration.MaximumNumberOfResponseEntries, "maximumNumberOfEntries");
            var alertIdParameter = SqlParameterFactory.Create(DbType.Int64, contentId, "alertId");
            var dateTimeNow = SqlParameterFactory.Create(DbType.DateTime, DateTime.Now, "Now");
            DataSet dataSet;

            using (var dbManager = new DBManager())
            {
                dataSet = dbManager.GetDataByQuery(selectActualAlertsForRecipientsQuery, maximumNumberOfEntries, alertIdParameter, dateTimeNow);
            }

            if (dataSet == null || dataSet.Count <= 0)
            {
                return null;
            }

            var dataRow = dataSet.First();
            var result = new Alert(dataRow);

            return result;
        }

        /// <summary>
        /// Get list of <see cref="Alert"/> by list of content id.
        /// </summary>
        /// <param name="contentIds">Content id.</param>
        /// <returns>List of <see cref="Alert"/> entity.</returns>
        public static IEnumerable<Alert> GetAlertListByIds(IEnumerable<long> contentIds)
        {
            var result = new List<Alert>();

            foreach (var contentId in contentIds)
            {
                var alert = GetAlertById(contentId);
                if (alert == null)
                {
                    continue;
                }

                result.Add(alert);
            }

            return result;
        }

        /// <summary>
        /// Convert string of content ids from CDS to ids list of content.
        /// </summary>
        /// <param name="stringOfContentIds">String of content ids.</param>
        /// <returns>List of content ids as long.</returns>
        public static IEnumerable<long> ConvertContentIdsStringToLong(string stringOfContentIds)
        {
            if (string.IsNullOrEmpty(stringOfContentIds))
            {
                return new List<long>();
            }

            var contentIds = stringOfContentIds
                .Split(',')
                .Select(long.Parse)
                .ToList();

            return contentIds;
        }

        /// <summary>
        /// Create <see cref="ContentDto"/> list by <see cref="Alert"/> entity.
        /// </summary>
        /// <param name="token">DeskAlerts <see cref="Token"/>.</param>
        /// <param name="localDateTime">Client local date time.</param>
        /// <param name="stringOfContentIds">Contents Ids string with ',' delimiter.</param>
        /// <returns>List of <see cref="ContentDto"/>.</returns>
        public static IEnumerable<ContentDto> GetContentByContentId(Token token, DateTime localDateTime, string stringOfContentIds)
        {
            var contentIds = ConvertContentIdsStringToLong(stringOfContentIds);

            var contentList = GetAlertListByIds(contentIds);

            var result = new List<ContentDto>();

            foreach (var alert in contentList)
            {
                var contentDto = GetContentDto(alert, token, localDateTime);
                result.Add(contentDto);
            }
            
            return result;
        }

        public static ContentDto GetContentDto(Alert alert, Token token, DateTime localDateTime)
        {
            if (alert == null)
            {
                throw new ArgumentNullException(nameof(alert));
            }

            if (token == null)
            {
                throw new ArgumentNullException(nameof(token));
            }

            var alertXml = alert.ToXmlString(token.Version, token.DeskbarId, token.UserName, token.User.Id, localDateTime, token.IsMobile);

            var result = new ContentDto
            {
                Id = alert.Id,
                Body = alertXml
            };

            return result;
        }
    }
}