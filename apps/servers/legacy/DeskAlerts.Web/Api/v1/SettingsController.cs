﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DeskAlerts.ApplicationCore.Dtos;
using DeskAlerts.ApplicationCore.Dtos.Settings;
using DeskAlerts.Server.Utils.Interfactes;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Parameters;
using Newtonsoft.Json;
using NLog;

namespace DeskAlerts.Server.Api.v1
{
    [RoutePrefix("api/v1/settings")]
    public class SettingsController : ApiController
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private readonly ISettingsService _settingsService;

        public SettingsController(ISettingsService settingsService)
        {
            _settingsService = settingsService;
        }

        [HttpPost]
        [Route("synchronization/azure")]
        public HttpResponseMessage EditAzureAdSynchronizationSettings(AzureAdSynchronizationSettings synchronizationSettings)
        {
            try
            {
                using (var databaseManager = new DBManager())
                {
                    databaseManager.ExecuteQuery(
                        $"UPDATE settings SET val = '{synchronizationSettings.DomainTennant}' WHERE name = 'ConfAzureAdTenant'");
                    databaseManager.ExecuteQuery(
                        $"UPDATE settings SET val = '{synchronizationSettings.PollingApplicationId}' WHERE name = 'ConfAzureAdPollingAppId'");
                    databaseManager.ExecuteQuery(
                        $"UPDATE settings SET val = '{synchronizationSettings.PollingApplicationSecretKey}' WHERE name = 'ConfAzureAdPollingAppSecret'");
                    databaseManager.ExecuteQuery(
                        $"UPDATE settings SET val = '{synchronizationSettings.AuthorizationApplicationId}' WHERE name = 'ConfAzureAdAuthorizationAppId'");
                    databaseManager.ExecuteQuery(
                        $"UPDATE settings SET val = '{JsonConvert.SerializeObject(synchronizationSettings.AutoSynchronization)}' WHERE name = 'ConfAzureAdAutoSynchronization'");
                }

                Settings.Content.Reload();

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                _logger.Debug(ex);
                var responseObject = new DeskAlertsApiResponse<object>
                {
                    Data = null,
                    ErrorMessage =
                                                 "Couldn't save Azure Active Directory synchronization settings",
                    Status = ResponseStatus.Error
                };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, responseObject);
            }
        }

        [HttpGet]
        [Route("synchronization/azure")]
        public HttpResponseMessage GetAzureAdSynchronizationSettings()
        {
            try
            {
                var domainTennant = Settings.Content["ConfAzureAdTenant"];
                var pollingApplicationId = Settings.Content["ConfAzureAdPollingAppId"];
                var pollingApplicationSecretKey = Settings.Content["ConfAzureAdPollingAppSecret"];
                var authorizationApplicationId = Settings.Content["ConfAzureAdAuthorizationAppId"];
                var autoSynchronizationString = Settings.Content["ConfAzureAdAutoSynchronization"];

                var azureSettings = new AzureAdSynchronizationSettings()
                {
                    DomainTennant = domainTennant,
                    PollingApplicationId = pollingApplicationId,
                    PollingApplicationSecretKey = pollingApplicationSecretKey,
                    AuthorizationApplicationId = authorizationApplicationId
                };

                if (!string.IsNullOrEmpty(autoSynchronizationString))
                {
                    azureSettings.AutoSynchronization =
                        JsonConvert.DeserializeObject<AzureAdAutoSynchronizationSettings>(autoSynchronizationString);
                }

                var responseObject = new DeskAlertsApiResponse<AzureAdSynchronizationSettings> { Data = azureSettings };

                return Request.CreateResponse(HttpStatusCode.OK, responseObject);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                _logger.Debug(ex);
                var responseObject = new DeskAlertsApiResponse<object>
                {
                    Data = null,
                    ErrorMessage =
                                                 "Couldn't get Azure Active Directory synchronization settings",
                    Status = ResponseStatus.Error
                };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, responseObject);
            }
        }

        [HttpGet]
        [Route("pushnotificationservers/check")]
        public HttpResponseMessage CheckPushNotificationServers()
        {
            try
            {
                var response = _settingsService.GetPushNotificationResponse();
                var responseObject = new DeskAlertsApiResponse<PushNotificatiosResponseDto>
                {
                    Data = response
                };
                return Request.CreateResponse(HttpStatusCode.OK, responseObject);
            }
            catch (Exception ex)
            {
                _logger.Debug(ex);
                var responseObject = new DeskAlertsApiResponse<object>
                {
                    Data = null,
                    ErrorMessage = "Couldn't check connection with Apple Push Notification Service and Google Cloud Messaging",
                    Status = ResponseStatus.Error
                };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, responseObject);
            }
        }

        [HttpPost]
        [Route("smtpserver/test")]
        public HttpResponseMessage TestSmtpServer(SmtpCredentialsDto credentialsDto)
        {
            return Request.CreateResponse(HttpStatusCode.Accepted, _settingsService.TestSmtpServerConnection(credentialsDto));
        }

        [HttpPost]
        [Route("dateTimeFormat")]
        public HttpResponseMessage DateTimeFormat(DateTimeDto dateTime)
        {
            try
            {
                var response = _settingsService.DateTimeExample(dateTime.DateFormat, dateTime.TimeFormat);
                var responseObject = new DeskAlertsApiResponse<string>
                {
                    Data = response
                };

                return Request.CreateResponse(HttpStatusCode.OK, responseObject);
            }
            catch (Exception ex)
            {
                _logger.Debug(ex);
                var responseObject = new DeskAlertsApiResponse<object>
                {
                    ErrorMessage = ex.Message,
                    Status = ResponseStatus.Error
                };

                return Request.CreateResponse(HttpStatusCode.InternalServerError, responseObject);
            }

        }
    }
}