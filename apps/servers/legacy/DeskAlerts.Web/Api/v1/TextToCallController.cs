﻿using DeskAlerts.Server.Utils;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using DeskAlerts.ApplicationCore.Dtos;
using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.ApplicationCore.Repositories;
using DeskAlerts.ApplicationCore.Utilities;
using DeskAlerts.Server.Utils.Managers;
using DeskAlertsDotNet.Parameters;
using NLog;

namespace DeskAlerts.Server.Api.v1
{
    [RoutePrefix("api/v1/TextToCall")]
    public class TextToCallController : ApiController
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private readonly IConfigurationManager _configurationManager;
        private readonly IAlertRepository _alertRepository;

        public TextToCallController()
        {
            _configurationManager = new WebConfigConfigurationManager(Config.Configuration);
            _alertRepository = new PpAlertRepository(_configurationManager);
        }

        public TextToCallController(IAlertRepository alertRepository)
        {
            _alertRepository = alertRepository;
        }

        [HttpGet, Route("xml/{alertId}")]
        public HttpResponseMessage GetAlertXMLById(long alertId)
        {
            try
            {
                var alert = _alertRepository.GetById(alertId);
                var alertText = DeskAlertsUtils.ExtractTextFromHtml(alert.Body);
                var result = TwiMLGenerator.GetTWiMLFromAlertBody(alertText);

                return new HttpResponseMessage()
                           {
                               Content = new StringContent(result, Encoding.UTF8, "application/xml"),
                               StatusCode = HttpStatusCode.OK
                           };
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                _logger.Debug(ex);
                return Request.CreateResponse(
                    HttpStatusCode.InternalServerError,
                    new DeskAlertsApiResponse<object>
                    {
                        Data = null,
                        ErrorMessage = ex.Message,
                        Status = ResponseStatus.Error
                    });
            }
        }
    }
}