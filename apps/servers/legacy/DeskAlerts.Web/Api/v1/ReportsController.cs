﻿namespace DeskAlertsDotNet.Api.v1
{
    using System;
    using System.Net;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Text;
    using System.Web.Http;

    using DeskAlerts.ApplicationCore.Dtos;
    using DeskAlerts.ApplicationCore.Interfaces;
    using DeskAlerts.ApplicationCore.Repositories;
    using DeskAlerts.ApplicationCore.Utilities;

    using DeskAlertsDotNet.Parameters;

    using NLog;

    [RoutePrefix("api/v1")]
    public class ReportsController : ApiController
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        private readonly IConfigurationManager _configurationManager;

        private readonly IReferralsRepository _referralsRepository;

        public ReportsController()
        {
            _configurationManager = new WebConfigConfigurationManager(Config.Configuration);
            _referralsRepository = new PpReferralsRepository(_configurationManager);
        }

        [HttpGet, Route("reports/referrals")]
        public HttpResponseMessage GetAllReferrals()
        {
            try
            {
                var response = new HttpResponseMessage();
                
                var stringBuilder = new StringBuilder();
                var referrals = _referralsRepository.GetAll();

                stringBuilder.AppendLine("<caption>Referrals</caption>");
                stringBuilder.AppendLine("<tr><th>Referral</th><th>Visit date</th></tr>");

                foreach (var referral in referrals)
                {
                    stringBuilder.AppendLine($"<tr><td>{referral.Email}</td><td>{referral.VisitDate}</td></tr>");
                }

                response.Content = new StringContent(
                    "<html>"
                    + "<style>\r\n"
                    + "table, th, td {\r\n    "
                    + "border: 1px solid black;\r\n   "
                    + " border-collapse: collapse;\r\n}\r\n"
                    + "th, td {\r\n    " 
                    + "padding: 5px;\r\n    " 
                    + "text-align: left;\r\n}\r\n" 
                    + "</style>" 
                    + "<body><table>" 
                    + $"{stringBuilder}" 
                    + "</table></body>" 
                    + "</html>");
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");

                return response;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                _logger.Debug(ex);
                var responseObject = new DeskAlertsApiResponse<object>
                                         {
                                             Data = null,
                                             ErrorMessage = "Couldn't get referrals",
                                             Status = ResponseStatus.Error
                                         };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, responseObject);
            }
        }
    }
}