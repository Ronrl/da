﻿using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.Server.Utils.Helpers;
using DeskAlertsDotNet.Application;

namespace DeskAlertsDotNet.Api.v1
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;
    using DeskAlerts.ApplicationCore.Dtos;
    using DeskAlerts.ApplicationCore.Dtos.Pages;
    using DeskAlerts.ApplicationCore.Interfaces;
    using DeskAlerts.ApplicationCore.Repositories;
    using DeskAlerts.ApplicationCore.Utilities;
    using DeskAlerts.Server.Utils.Managers;
    using DeskAlertsDotNet.Managers;
    using DeskAlertsDotNet.Parameters;
    using DeskAlertsDotNet.Utils.Consts;
    using NLog;

    using Resources;

    using static DeskAlerts.ApplicationCore.Entities.AlertEntity;

    [RoutePrefix("api/v1")]
    public class AlertController : ApiController
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        private readonly IConfigurationManager _configurationManager;

        private readonly IAlertRepository _alertRepository;

        private readonly Models.User _currentUser;
        

        public AlertController(IAlertRepository alertRepository)
        {
            _alertRepository = alertRepository;
            _currentUser = UserManager.Default.CurrentUser;
        }

        [HttpPost, Route("campaigns/{campaignId}/alerts")]
        public HttpResponseMessage GetAlertsByCampaignId(long campaignId, Pagination pagination)
        {
            try
            {
                var alerts = _alertRepository.GetAlertsByCampaignIdMappedWithUser(campaignId);
                var startPageIndex = (pagination.Page - 1) * pagination.RowsPerPage;
                var rowsPerPage = pagination.RowsPerPage == 0 ? alerts.Count : pagination.RowsPerPage;
                var alertsDto = alerts.
                    Select(x => new AlertDto()
                    {
                        CampaignId = x.CampaignId,
                        Title = x.Title,
                        Scheduled = x.IsScheduled ? "Yes" : "No",
                        Type = new AlertTypeDto(x.Type, x.CampaignId, resources.LNG_RESEND_ALERT, x.Class, x.Id),
                        SenderName = x.Sender.Username,
                        FromDate = x.FromDate.Value.Year == 1900 ? null : x.FromDate,
                        ToDate = x.ToDate.Value.Year == 1900 ? null : x.ToDate,
                        Id = x.Id
                    }).
                    Skip(startPageIndex).
                    Take(rowsPerPage);
                pagination.RowsNumber = alerts.Count;
                if (!string.IsNullOrEmpty(pagination.Search))
                {
                    alertsDto = alertsDto.Where(x => x.Title.Contains(pagination.Search));
                }

                if (!string.IsNullOrEmpty(pagination.SortBy))
                {
                    switch (pagination.SortBy)
                    {
                        case "type":
                            alertsDto = alertsDto.OrderBy(x => x.Type);
                            break;
                        case "scheduled":
                            alertsDto = alertsDto.OrderBy(x => x.Scheduled);
                            break;
                        case "sentBy":
                            alertsDto = alertsDto.OrderBy(x => x.SenderName);
                            break;
                        case "fromDate":
                            alertsDto = alertsDto.OrderBy(x => x.FromDate);
                            break;
                        case "toDate":
                            alertsDto = alertsDto.OrderBy(x => x.ToDate);
                            break;
                        default:
                            alertsDto = alertsDto.OrderBy(x => x.Title);
                            break;
                    }
                }

                var acceptedTypes = GetAcceptedTypes();
                var page = new CampaignDetailsPage()
                {
                    Alerts = alertsDto.ToList(),
                    CampaignId = campaignId,
                    Pagination = pagination,
                    CanCreate = _currentUser.IsAdmin || _currentUser.Policy[Models.Permission.Policies.CAMPAIGN].CanCreate,
                    CanView = _currentUser.IsAdmin || _currentUser.Policy[Models.Permission.Policies.CAMPAIGN].CanView,
                    CanEdit = _currentUser.IsAdmin || _currentUser.Policy[Models.Permission.Policies.CAMPAIGN].CanEdit,
                    CanDelete = _currentUser.IsAdmin || _currentUser.Policy[Models.Permission.Policies.CAMPAIGN].CanDelete,
                    CanSend = _currentUser.IsAdmin || (_currentUser.Policy[Models.Permission.Policies.CAMPAIGN].CanSend && acceptedTypes.Count > 0),
                    CanStop = _currentUser.IsAdmin || _currentUser.Policy[Models.Permission.Policies.CAMPAIGN].CanStop,
                };

                var responseObject = new DeskAlertsApiResponse<CampaignDetailsPage> { Data = page };
                return Request.CreateResponse(HttpStatusCode.OK, responseObject);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                _logger.Debug(ex);
                var responseObject = new DeskAlertsApiResponse<object>
                {
                    Data = null,
                    ErrorMessage = "Couldn't get Alerts by Campaign id",
                    Status = ResponseStatus.Error
                };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, responseObject);
            }
        }

        [HttpGet, Route("alert/{campaignId}/types")]
        public HttpResponseMessage GetAlertTypesByCampaignId(long campaignId)
        {
            try
            {
                var acceptedTypes = GetAcceptedTypes();
                var types = Enum.GetValues(typeof(AlertType)).
                    Cast<AlertType>().
                    Where(y => acceptedTypes.Contains(y)).
                    Select(x =>
                    new AlertTypeDto(x, campaignId, resources.LNG_RESEND_ALERT, 0, 0)).
                    OrderBy(x => x.Value);

                var responseObject = new DeskAlertsApiResponse<List<AlertTypeDto>> { Data = types.ToList() };
                return Request.CreateResponse(HttpStatusCode.OK, responseObject);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                _logger.Debug(ex);
                var responseObject = new DeskAlertsApiResponse<object>
                {
                    Data = null,
                    ErrorMessage = "Couldn't get Alerts by Campaign id",
                    Status = ResponseStatus.Error
                };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, responseObject);
            }
        }

        [HttpPost, Route("alerts/delete")]
        public HttpResponseMessage DeleteAlerts(List<AlertDto> alerts)
        {
            try
            {
                var cacheManager = Global.CacheManager;
                foreach (var alertDto in alerts)
                {
                    var alert = _alertRepository.GetById(alertDto.Id);
                    _alertRepository.Delete(alert);
                    cacheManager.Delete(alert.Id, DateTimeConverter.ToDbDateTime(DateTime.Now));
                }

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                _logger.Debug(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new DeskAlertsApiResponse<object>
                {
                    Data = null,
                    ErrorMessage = "Couldn't delete the campaigns",
                    Status = ResponseStatus.Error
                });
            }
        }

        private List<AlertType> GetAcceptedTypes()
        {
            var acceptedTypes = new List<AlertType>();
               bool approveModuleEnabled = Config.Data.HasModule(Modules.APPROVE) && Settings.Content["ConfAllowApprove"].Equals("1");

            if ((_currentUser.IsPublisher && _currentUser.Policy[AlertType.SimpleAlert].CanCreate && (_currentUser.Policy[AlertType.SimpleAlert].CanApprove || !approveModuleEnabled)) || _currentUser.IsAdmin)
            {
                acceptedTypes.Add(AlertType.SimpleAlert);
                acceptedTypes.Add(AlertType.Ticker);
                acceptedTypes.Add(AlertType.Rsvp);
                acceptedTypes.Add(AlertType.VideoAlert);
            }

            if ((_currentUser.IsPublisher && _currentUser.Policy[AlertType.RssFeed].CanCreate) || _currentUser.IsAdmin)
            {
                acceptedTypes.Add(AlertType.RssFeed);
            }

            if ((_currentUser.IsPublisher && _currentUser.Policy[AlertType.SimpleSurvey].CanCreate && (_currentUser.Policy[AlertType.SimpleSurvey].CanApprove || !approveModuleEnabled)) || _currentUser.IsAdmin)
            {
                acceptedTypes.Add(AlertType.SimpleSurvey);
            }

            if ((_currentUser.IsPublisher && _currentUser.Policy[AlertType.ScreenSaver].CanCreate && (_currentUser.Policy[AlertType.ScreenSaver].CanApprove || !approveModuleEnabled)) || _currentUser.IsAdmin)
            {
                acceptedTypes.Add(AlertType.ScreenSaver);
            }

            if ((_currentUser.IsPublisher && _currentUser.Policy[AlertType.Wallpaper].CanCreate && (_currentUser.Policy[AlertType.Wallpaper].CanApprove || !approveModuleEnabled)) || _currentUser.IsAdmin)
            {
                acceptedTypes.Add(AlertType.Wallpaper);
            }

            return acceptedTypes;
        }
    }
}