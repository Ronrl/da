﻿using DeskAlertsDotNet.Application;

namespace DeskAlertsDotNet.Api.v1
{
    using System;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;

    using DeskAlerts.ApplicationCore.Dtos;
    using DeskAlerts.Server.Utils.Managers;
    using NLog;

    [RoutePrefix("api/v1")]
    public class CachesController : ApiController
    {
        private readonly CacheManager _cacheManager = Global.CacheManager;

        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        [HttpGet, Route("caches")]
        public HttpResponseMessage GetAllCaches()
        {
            try
            {
                var responseObject = new DeskAlertsApiResponse<object>
                                         {
                                             Data = new
                                                        {
                                                            UsersCache = _cacheManager.GetAlertsByCacheType(CacheTypes.CacheUsers),
                                                            IpsCache = _cacheManager.GetAlertsByCacheType(CacheTypes.CacheIps),
                                                            ComputersCache = _cacheManager.GetAlertsByCacheType(CacheTypes.CacheComps),
                                                            ReceivedCache = _cacheManager.GetAlertsByCacheType(CacheTypes.CacheRecievedAlerts)
                                                        }
                                         };

                return Request.CreateResponse(HttpStatusCode.OK, responseObject);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                _logger.Debug(ex);
                var responseObject = new DeskAlertsApiResponse<object>
                                         {
                                             Data = null,
                                             ErrorMessage = "Couldn't get caches",
                                             Status = ResponseStatus.Error
                                         };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, responseObject);
            }
        }

        [HttpGet, Route("caches/users")]
        public HttpResponseMessage GetUserCache()
        {
            try
            {
                var responseObject = new DeskAlertsApiResponse<object>
                                         {
                                             Data = _cacheManager.GetAlertsByCacheType(CacheTypes.CacheUsers)
                                         };

                return Request.CreateResponse(HttpStatusCode.OK, responseObject);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                _logger.Debug(ex);
                var responseObject = new DeskAlertsApiResponse<object>
                                         {
                                             Data = null,
                                             ErrorMessage = "Couldn't get users cache",
                                             Status = ResponseStatus.Error
                                         };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, responseObject);
            }
        }

        [HttpGet, Route("caches/computers")]
        public HttpResponseMessage GetComputersCache()
        {
            try
            {
                var responseObject = new DeskAlertsApiResponse<object>
                                         {
                                             Data = _cacheManager.GetAlertsByCacheType(CacheTypes.CacheComps)
                                         };

                return Request.CreateResponse(HttpStatusCode.OK, responseObject);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                _logger.Debug(ex);
                var responseObject = new DeskAlertsApiResponse<object>
                                         {
                                             Data = null,
                                             ErrorMessage = "Couldn't get computers cache",
                                             Status = ResponseStatus.Error
                                         };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, responseObject);
            }
        }

        [HttpGet, Route("caches/ips")]
        public HttpResponseMessage GetIpsCaches()
        {
            try
            {
                var responseObject = new DeskAlertsApiResponse<object>
                                         {
                                             Data = _cacheManager.GetAlertsByCacheType(CacheTypes.CacheIps)
                                         };

                return Request.CreateResponse(HttpStatusCode.OK, responseObject);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                _logger.Debug(ex);
                var responseObject = new DeskAlertsApiResponse<object>
                                         {
                                             Data = null,
                                             ErrorMessage = "Couldn't get ips cache",
                                             Status = ResponseStatus.Error
                                         };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, responseObject);
            }
        }

        [HttpGet, Route("caches/received")]
        public HttpResponseMessage GetReceivedCache()
        {
            try
            {
                var responseObject = new DeskAlertsApiResponse<object>
                                         {
                                             Data = _cacheManager.GetAlertsByCacheType(CacheTypes.CacheRecievedAlerts)
                                         };

                return Request.CreateResponse(HttpStatusCode.OK, responseObject);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                _logger.Debug(ex);
                var responseObject = new DeskAlertsApiResponse<object>
                                         {
                                             Data = null,
                                             ErrorMessage = "Couldn't get received cache",
                                             Status = ResponseStatus.Error
                                         };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, responseObject);
            }
        }
    }
}