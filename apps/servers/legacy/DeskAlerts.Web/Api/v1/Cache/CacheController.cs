﻿using DeskAlertsDotNet.Application;

namespace DeskAlertsDotNet.API
{
    using System;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;
    using DeskAlerts.Server.Utils.Managers;
    using DeskAlertsDotNet.Models.Cache;

    using NLog;

    public class CacheController : ApiController
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        [HttpPost]
        public HttpResponseMessage AddCache(AlertCache model)
        {
            _logger.Debug("Cache sync started");
            _logger.Debug($"Start adding alert {model.AlertId} to cache.");
            var cacheManager = Global.CacheManager;
            try
            {
                bool alertCached = cacheManager.IsAlertCached(model.AlertId);
                if (!alertCached)
                {
                    _logger.Debug($"Alert {model.AlertId} is absent in cache, adding.");
                    cacheManager.Add(model.AlertId, model.Date);
                    _logger.Debug($"Alert {model.AlertId} successfully added to cache.");
                    return new HttpResponseMessage(HttpStatusCode.OK);
                }
                else
                {
                    _logger.Debug($"Alert {model.AlertId} is already cached.");
                    return new HttpResponseMessage(HttpStatusCode.NotModified);
                }
            }
            catch (Exception e)
            {
                _logger.Debug(e);
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }

        [HttpPost]
        public HttpResponseMessage AddReceivedCache(ReceivedAlertCache model)
        {
            _logger.Debug("Cache sync started");
            _logger.Debug($"Start adding alert {model.AlertId} to received cache.");
            var cacheManager = Global.CacheManager;
            try
            {
                bool alertCached = cacheManager.IsRecieved(model.AlertId);
                if (!alertCached)
                {
                    _logger.Debug($"Alert {model.AlertId} is absent in received cache, adding.");
                    cacheManager.AddAlertsToReceivedCache(model.Key, model.AlertId);
                    _logger.Debug($"Alert {model.AlertId} successfully added to received cache.");
                    return new HttpResponseMessage(HttpStatusCode.OK);
                }
                else
                {
                    _logger.Debug($"Alert {model.AlertId} is already cached.");
                    return new HttpResponseMessage(HttpStatusCode.NotModified);
                }
            }
            catch (Exception e)
            {
                _logger.Debug(e);
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }

        // TODO Remove copy-paste from AddCache
        // TODO Replace HttpPost with HttpDelete
        [HttpPost]
        public HttpResponseMessage DeleteCache(AlertCache model)
        {
            _logger.Debug("Cache sync started");
            _logger.Debug($"Start deleting alert {model.AlertId} from cache.");
            var cacheManager = Global.CacheManager;
            try
            {
                bool alertCached = cacheManager.IsAlertCached(model.AlertId);
                if (alertCached)
                {
                    _logger.Debug($"Alert {model.AlertId} presents in cache, removing.");
                    cacheManager.Delete(model.AlertId, model.Date);
                    _logger.Debug($"Alert {model.AlertId} successfully removed to cache.");
                    return new HttpResponseMessage(HttpStatusCode.OK);
                }
                else
                {
                    _logger.Debug($"Alert {model.AlertId} is absent in cache.");
                    return new HttpResponseMessage(HttpStatusCode.NotModified);
                }
            }
            catch (Exception e)
            {
                _logger.Debug(e);
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }
    }
}