﻿namespace DeskAlertsDotNet.Api.v1
{
    using System;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;

    using DeskAlerts.ApplicationCore.Dtos;
    using DeskAlerts.ApplicationCore.Interfaces;
    using DeskAlerts.ApplicationCore.Repositories;
    using DeskAlerts.ApplicationCore.Utilities;

    using DeskAlertsDotNet.Parameters;

    using NLog;

    [RoutePrefix("api/v1/groups")]
    public class GroupsController : ApiController
    {
        private readonly IGroupRepository _groupRepository;

        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public GroupsController()
        {
            var configurationManager = new WebConfigConfigurationManager(Config.Configuration);
            _groupRepository = new PpGroupRepository(configurationManager);
        }

        [HttpGet, Route("getGroupsWithAdminPolicy")]
        public HttpResponseMessage GetDomainGroupsWithAdminPolicy()
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, _groupRepository.GetGroupsWithAdminPolicy());
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                var responseObject =
                    new DeskAlertsApiResponse<object>
                        {
                            Data = null,
                            ErrorMessage = "There is no Groups with Admins Policy"
                        };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, responseObject);
            }
        }
    }
}
