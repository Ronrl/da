﻿using DeskAlerts.ApplicationCore.Dtos;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.Server.Utils.Interfaces.Services;
using NLog;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DeskAlerts.Server.Api.v1
{
    /// <summary>
    /// Class contains implementation of cache invalidation.
    /// This class is used when there is a need to invalidate the cache with a trigger from another source.
    /// </summary>
    [RoutePrefix("api/v1")]
    public class CacheInvalidationController : ApiController
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly ICacheInvalidationService _cacheInvalidationService;

        /// <summary>
        /// The class constructor.
        /// </summary>
        public CacheInvalidationController(ICacheInvalidationService cacheInvalidationService)
        {
            _cacheInvalidationService = cacheInvalidationService;
        }

        /// <summary>
        /// Call cache invalidation on current server.
        /// </summary>
        /// <param name="contentType">Type of DeskAlerts content <see cref="AlertType"/>.</param>
        /// <param name="contentId">Content identifier.</param>
        /// <returns><see cref="HttpResponseMessage"/> with success or failed CDS result.</returns>
        [HttpGet, Route("cache/invalidation")]
        public HttpResponseMessage Invalidation(AlertType contentType, long contentId)
        {
            if (contentId <= 0)
            {
                throw new ArgumentException(nameof(contentId));
            }

            try
            {
                _cacheInvalidationService.LocalCacheInvalidation(contentType, contentId);

                return Request.CreateResponse(HttpStatusCode.OK, new DeskAlertsApiResponse<object>
                {
                    Status = ResponseStatus.Success
                });
            }
            catch (Exception ex)
            {
                Logger.Error(ex);

                return Request.CreateResponse(HttpStatusCode.OK, new DeskAlertsApiResponse<object>
                {
                    ErrorMessage = $"An internal error has occurred in the Content Delivering Service. Error: {ex.Message}.",
                    Status = ResponseStatus.Error
                });
            }
        }
    }
}