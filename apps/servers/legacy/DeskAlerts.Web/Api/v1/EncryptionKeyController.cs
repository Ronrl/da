﻿using System;
using System.Web.Http;
using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.Server.Models;
using DeskAlerts.Server.Utils.Interfactes;
using DeskAlerts.Server.Utils.Managers;
using DeskAlertsDotNet.Application;
using DeskAlertsDotNet.Models;
using DeskAlertsDotNet.Parameters;

namespace DeskAlertsDotNet.Api.v1
{
    public class EncryptionKeyController : ApiController
    {
        private readonly IUserService _userService;

        public EncryptionKeyController(IUserRepository userRepository, IUserService userService)
        {
            _userService = userService;
        }

        /// <summary>
        /// <example><code>
        /// JSON header format:
        /// Content-Type: application/json
        /// JSON request format:
        /// {
        ///"Username": "test",
        ///"Password":  "admin"
        ///}
        ///</code></example>
        /// </summary>
        /// <param name="credentials"> Object have Username and Password fields</param>
        /// <returns></returns>
        [HttpPost, Route("api/EncryptionKey/GetEncryptionKey")]
        public string GetEncryptionKey(UserCredentialsDto request)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            try
            {
                var userName = request.Username;
                var password = _userService.ConvertToMD5Hash(request.Password);

                var user = _userService.GetUser(userName, password);

                return user != null && user.Password == request.Password.ToUpper() ? Config.Data.GetString("ENCRYPT_KEY") : string.Empty;
            }
            catch
            {
                return string.Empty;
            }
        }
    }
}