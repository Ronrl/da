using Castle.Core.Internal;
using DeskAlerts.ApplicationCore.Dtos;
using DeskAlerts.ApplicationCore.Dtos.Content;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.ApplicationCore.Models;
using DeskAlerts.ApplicationCore.Models.ContentTypes;
using DeskAlerts.ApplicationCore.Utilities;
using DeskAlerts.ContentDeliveringService.Interfaces;
using DeskAlerts.ContentDeliveringService.Models;
using DeskAlerts.Server.Utils.Interfaces.Services;
using DeskAlerts.Server.Utils.Interfactes;
using DeskAlerts.Server.Utils.Services;
using DeskAlertsDotNet.Api.v1.Security;
using DeskAlertsDotNet.Application;
using DeskAlertsDotNet.Models;
using DeskAlertsDotNet.Utils.Consts;
using NLog;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace DeskAlerts.Server.Api.v1
{
    [RoutePrefix("api/v1")]
    public class ContentController : ApiController
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly IUserService _userService;
        private readonly ITokenService _tokenService = Global.Container.Resolve<ITokenService>();
        private readonly IContentService _contentService;
        private readonly IComputerService _computerService;
        private readonly IContentDeliveringService<WallpaperContentType> _wallpapersContentDeliveringService;
        private readonly IContentDeliveringService<LockscreenContentType> _lockscreensContentDeliveringService;
        private readonly IContentDeliveringService<ScreensaverContentType> _screensaversContentDeliveringService;
        private readonly IContentDeliveringService<TickerContentType> _tickerContentDeliveringService;
        private readonly IContentDeliveringService<RsvpContentType> _rsvpContentDeliveringService;
        private readonly IContentDeliveringService<AlertContentType> _alertContentDeliveringService;
        private readonly IContentDeliveringService<SurveyContentType> _surveyContentDeliveringService;
        private readonly IContentDeliveringService<VideoAlertContentType> _videoAlertContentDeliveringService;
        private readonly IContentDeliveringService<TerminatedContentType> _terminatedContentDeliveringService;
        private readonly IAlertRepository _alertRepository;
        private readonly ICacheInvalidationService _cacheInvalidationService;
        private readonly IETagService _eTagService;

        private static readonly object LockGetUserAlerts = new object();
        private static readonly object LockUpdateComputer = new object();

        public ContentController(
            IUserService userService,
            IContentService contentService,
            IContentDeliveringService<WallpaperContentType> wallpapersContentDeliveringService,
            IContentDeliveringService<LockscreenContentType> lockscreensContentDeliveringService,
            IContentDeliveringService<ScreensaverContentType> screensaversContentDeliveringService,
            IContentDeliveringService<TickerContentType> tickerContentDeliveringService,
            IContentDeliveringService<RsvpContentType> rsvpContentDeliveringService,
            IContentDeliveringService<AlertContentType> alertContentDeliveringService,
            IContentDeliveringService<SurveyContentType> surveyContentDeliveringService,
            IContentDeliveringService<VideoAlertContentType> videoAlertContentDeliveringService,
            IContentDeliveringService<TerminatedContentType> terminatedContentDeliveringService,
            IAlertRepository alertRepository,
            ICacheInvalidationService cacheInvalidationService,
            IETagService eTagService
        )
        {
            _contentService = contentService;
            _userService = userService;
            _computerService = new ComputerService();
            _wallpapersContentDeliveringService = wallpapersContentDeliveringService;
            _lockscreensContentDeliveringService = lockscreensContentDeliveringService;
            _screensaversContentDeliveringService = screensaversContentDeliveringService;
            _tickerContentDeliveringService = tickerContentDeliveringService;
            _rsvpContentDeliveringService = rsvpContentDeliveringService;
            _alertContentDeliveringService = alertContentDeliveringService;
            _surveyContentDeliveringService = surveyContentDeliveringService;
            _videoAlertContentDeliveringService = videoAlertContentDeliveringService;
            _terminatedContentDeliveringService = terminatedContentDeliveringService;
            _alertRepository = alertRepository;
            _cacheInvalidationService = cacheInvalidationService;
            _eTagService = eTagService;
        }

        #region user/contents

        /// <summary>
        /// The method return alerts, wallpapers and ScreenSavers hash.
        /// </summary>
        /// <param name="timezone">Time zone offset in seconds (example: 3600 or -18000). Can be between -50400 and 50400.</param>
        /// <returns></returns>
        [TokenAuthorize]
        [HttpGet, Route("user/contents")]
        public HttpResponseMessage GetUserContents(string timezone)
        {
            if (timezone == null)
            {
                throw new ArgumentNullException(nameof(timezone));
            }

            try
            {
                var localDateTime = DateTimeUtils.GetDateTimeByTimeZone(timezone);
                var timezoneParam = int.Parse(timezone);

                return ApiResponse(localDateTime, timezoneParam);
            }
            catch (Exception ex)
            {
                return ApiErrorResponse(ex);
            }
        }

        /// <summary>
        /// ToDo: Rename to "user/contents"
        /// The method return alerts, wallpapers and screensavers hash
        /// </summary>
        /// <param name="request">Conantin LocalTime of current client</param>
        /// <returns></returns>
        [TokenAuthorize]
        [HttpPost, Route("user/alerts")]
        public HttpResponseMessage GetUserAlerts(ContentRequestDto request)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            try
            {
                var localDateTime = DateTime.ParseExact(request.LocalTime, DateTimeFormat.Client, CultureInfo.InvariantCulture);
                var timezone = DateTimeUtils.GetTimeZoneByLocalTime(localDateTime);

                return ApiResponse(localDateTime, timezone);
            }
            catch (Exception ex)
            {
                return ApiErrorResponse(ex);
            }
        }

        private HttpResponseMessage ApiErrorResponse(Exception ex)
        {
            Logger.Error(ex);
            return Request.CreateResponse(HttpStatusCode.OK, new DeskAlertsApiResponse<object>
            {
                ErrorMessage = $"An error has occured while getting content. Error: {ex}.",
                Status = ResponseStatus.Error
            });
        }

        private HttpResponseMessage ApiResponse(DateTime localDateTime, int timezone)
        {
            if (!AppConfiguration.IsNewContentDeliveringScheme)
            {
                if (!Global.IsCacheManagerLoaded)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new DeskAlertsApiResponse<object>
                    {
                        ErrorMessage = "CacheManager is loading",
                        Status = ResponseStatus.Fail
                    });
                }
            }

            var token = GetTokenByGuidFromRequest();

            lock (LockGetUserAlerts)
            {
                // CR killall5: schedule it!
                _userService.UpdateUserLastRequest(token.User.Id);
                _userService.UpdateUserTimeZone(token.User.Id, timezone);
            }

            if (!string.IsNullOrEmpty(token.ComputerDomainName))
            {
                lock (LockUpdateComputer)
                {
                    _computerService.UpdateComputerLastRequest(token.ComputerName, token.ComputerDomainName);
                }
            }

            var cacheKey = new ContentDeliveringKey(timezone, token.User.Id, Guid.Parse(token.DeskbarId));

            string eTag;

            lock (LockGetUserAlerts)
            {
                eTag = $"\"{_eTagService.GetETag(cacheKey)}\"";
            }

            if (AppConfiguration.IsNewContentDeliveringScheme && AppConfiguration.IsHttpStatusNotModified)
            {
                var ifNoneMatchHeader = Request.Headers.IfNoneMatch.ToString();
                if (ifNoneMatchHeader == eTag)
                {
                    return Request.CreateResponse(HttpStatusCode.NotModified);
                }
            }

            string dtoScreenSavers;
            string dtoWallPapers;
            string dtoLockScreens;
            IEnumerable<ContentDto> dtoAlerts;
            IEnumerable<long> dtoTerminated;

            if (AppConfiguration.IsNewContentDeliveringScheme)
            {
                var screensaversIds = _screensaversContentDeliveringService.Get(cacheKey, token.Ip, token.ComputerName);
                var wallpapersIds = _wallpapersContentDeliveringService.Get(cacheKey, token.Ip, token.ComputerName);
                var lockscreensIds = _lockscreensContentDeliveringService.Get(cacheKey, token.Ip, token.ComputerName);

                dtoScreenSavers = !string.IsNullOrEmpty(screensaversIds)
                    ? HashingUtils.MD5(screensaversIds)
                    : string.Empty;

                dtoWallPapers = !string.IsNullOrEmpty(wallpapersIds)
                    ? HashingUtils.MD5(wallpapersIds)
                    : string.Empty;

                dtoLockScreens = !string.IsNullOrEmpty(lockscreensIds)
                    ? HashingUtils.MD5(lockscreensIds)
                    : string.Empty;

                dtoAlerts = GetContentDtoListFromContentDeliveringService(cacheKey, token, localDateTime);

                var terminatedContents = _terminatedContentDeliveringService.Get(cacheKey, token.Ip, token.ComputerName);

                dtoTerminated = terminatedContents.Any()
                    ? terminatedContents
                        .Split(',')
                        .Select(long.Parse)
                        .ToList()
                    : new List<long>();
            }
            else
            {
                var screenSavers = _contentService.GetScreensaver(localDateTime, token).ToArray();
                var wallpapers = _contentService.GetWallpapers(localDateTime, token).ToArray();
                var lockscreens = _contentService.GetLockScreens(localDateTime, token).ToArray();

                dtoScreenSavers = screenSavers.Any()
                    ? HashingUtils.MD5(string.Join(",", screenSavers.Select(x => x.Id)))
                    : string.Empty;

                dtoWallPapers = wallpapers.Any()
                    ? HashingUtils.MD5(string.Join(",", wallpapers.Select(x => x.Id)))
                    : string.Empty;

                dtoLockScreens = lockscreens.Any()
                    ? HashingUtils.MD5(string.Join(",", lockscreens.Select(x => x.Id)))
                    : string.Empty;

                dtoAlerts = _contentService.GetAlerts(localDateTime, token)
                    .Where(a => !Global.CacheManager.IsReceivedAlertForUser(token.DistinguishedName, a.Id))
                    .Select(x => ContentControllerHelper.GetContentDto(x, token, localDateTime));

                dtoTerminated = _contentService
                    .GetTerminatedAlertsForInstance(localDateTime, token);
            }

            var data = new ContentResponseDto
            {
                ScreensaversHash = dtoScreenSavers,
                WallpapersHash = dtoWallPapers,
                LockscreensHash = dtoLockScreens,
                Alerts = dtoAlerts,
                TerminatedAlerts = dtoTerminated,
            };

            var response = new DeskAlertsApiResponse<ContentResponseDto> { Data = data };
            var result = Request.CreateResponse(HttpStatusCode.OK, response);

            if (AppConfiguration.MaxAgeOfHttpHeaderCacheControl != 0)
            {
                result.Headers.CacheControl = new CacheControlHeaderValue()
                {
                    Public = true,
                    MaxAge = TimeSpan.FromSeconds(AppConfiguration.MaxAgeOfHttpHeaderCacheControl)
                };
            }
            else
            {
                result.Headers.CacheControl = new CacheControlHeaderValue()
                {
                    NoCache = true
                };
            }

            result.Headers.ETag = AppConfiguration.IsNewContentDeliveringScheme
                ? new EntityTagHeaderValue(eTag)
                : new EntityTagHeaderValue($"\"{GenerateETagMd5Value()}\"");

            return result;
        }

        #endregion

        #region Get content from Content Delivering Service

        /// <summary>
        /// Create <see cref="ContentDto"/> list with Alerts, Tickers, RSVPs, Surveys, VideoAlerts.
        /// </summary>
        /// <param name="key"><see cref="ContentDeliveringKey"/> from cache.</param>
        /// <param name="token">DeskAlerts user <see cref="Token"/>.</param>
        /// <param name="localDateTime">Client local date time.</param>
        /// <returns>List of <see cref="ContentDto"/> with Alerts, Tickers, RSVPs, Surveys, VideoAlerts.</returns>
        public IEnumerable<ContentDto> GetContentDtoListFromContentDeliveringService(ContentDeliveringKey key, Token token, DateTime localDateTime)
        {
            var alertIds = _alertContentDeliveringService.Get(key, token.Ip, token.ComputerName);
            var tickerIds = _tickerContentDeliveringService.Get(key, token.Ip, token.ComputerName);
            var rsvpIds = _rsvpContentDeliveringService.Get(key, token.Ip, token.ComputerName);
            var surveyIds = _surveyContentDeliveringService.Get(key, token.Ip, token.ComputerName);
            var videoIds = _videoAlertContentDeliveringService.Get(key, token.Ip, token.ComputerName);

            var allIds = new List<string>
            {
                alertIds,
                tickerIds,
                rsvpIds,
                surveyIds,
                videoIds
            };

            var stringOfContentIds = string.Join(",", allIds.Where(x => !string.IsNullOrEmpty(x)));

            var result = ContentControllerHelper.GetContentByContentId(token, localDateTime, stringOfContentIds);

            return result;
        }

        #endregion

        #region user/content/wallpapers

        [TokenAuthorize]
        [HttpPost, Route("user/content/wallpapers")]
        public HttpResponseMessage GetUserWallpapers(ContentRequestDto request)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }
            try
            {
                var token = GetTokenByGuidFromRequest();
                var localDateTime = DateTime.ParseExact(request.LocalTime, DateTimeFormat.Client, CultureInfo.InvariantCulture);
                var response = new DeskAlertsApiResponse<IEnumerable<WallpaperDto>>
                {
                    Data = _contentService.GetWallpapers(localDateTime, token).Select(x => GetWallpaperDto(x, token))
                };

                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                Logger.Debug(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new DeskAlertsApiResponse<object>
                {
                    ErrorMessage = $"Couldn't get content. Error: {ex}.",
                    Status = ResponseStatus.Error
                });
            }
        }

        #endregion

        #region user/content/lockscreens

        [TokenAuthorize]
        [HttpPost, Route("user/content/lockscreens")]
        public HttpResponseMessage GetUserLockscreens(ContentRequestDto request)
        {
            if (!AppConfiguration.IsLockScreen)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new DeskAlertsApiResponse<object>
                {
                    ErrorMessage = "LockScreen module is disabled.",
                    Status = ResponseStatus.Fail
                });
            }

            if (!AppConfiguration.IsNewContentDeliveringScheme)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new DeskAlertsApiResponse<object> 
                {
                    ErrorMessage = "Old content delivery scheme not supported",
                    Status = ResponseStatus.Fail
                });
            }
            
            
            if (request == null || request.LocalTime.IsNullOrEmpty())
            {
                throw new ArgumentNullException(nameof(request));
            }

            try
            {
                var intTimezone = 0;
                var localDateTime = DateTimeUtils.GetDateTimeByTimeZone(intTimezone, true);
                
                
                var token = GetTokenByGuidFromRequest();

                lock (LockGetUserAlerts)
                {
                    // CR killall5: schedule it!
                    _userService.UpdateUserLastRequest(token.User.Id);
                    _userService.UpdateUserTimeZone(token.User.Id, intTimezone);
                }

                if (!string.IsNullOrEmpty(token.ComputerDomainName))
                {
                    lock (LockUpdateComputer)
                    {
                        _computerService.UpdateComputerLastRequest(token.ComputerName, token.ComputerDomainName);
                    }
                }

                var cacheKey = new ContentDeliveringKey(intTimezone, token.User.Id, Guid.Parse(token.DeskbarId));
                //
                // string eTag;
                //
                // lock (LockGetUserAlerts)
                // {
                //     eTag = $"\"{_eTagService.GetETag(cacheKey)}\"";
                // }
                //
                // if (AppConfiguration.IsHttpStatusNotModified)
                // {
                //     var ifNoneMatchHeader = Request.Headers.IfNoneMatch.ToString();
                //     if (ifNoneMatchHeader == eTag)
                //     {
                //         return Request.CreateResponse(HttpStatusCode.NotModified);
                //     }
                // } 
                //
                var lockscreens = _lockscreensContentDeliveringService.Get(cacheKey, token.Ip, token.ComputerName);

                var lockascreenIds = lockscreens.IsNullOrEmpty() ? 
                    new List<long>() : lockscreens
                        .Split(',')
                        .Select(long.Parse)
                        .ToList();
                
                    
                var response = new DeskAlertsApiResponse<IEnumerable<LockscreenDto>>
                 {
                     Data = lockascreenIds.Select(x=>GetLockscreenDto(x, token))
                 };
                
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                Logger.Debug(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new DeskAlertsApiResponse<object>
                {
                    ErrorMessage = $"Couldn't get content. Error: {ex}.",
                    Status = ResponseStatus.Error
                });
            }
        }


        [TokenAuthorize]
        [HttpGet, Route("computer/content/lockscreens")]
        public HttpResponseMessage GetComputerLockscreens(string timezone)
        {
            if (!AppConfiguration.IsLockScreen)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new DeskAlertsApiResponse<object>
                {
                    ErrorMessage = "LockScreen module is disabled.",
                    Status = ResponseStatus.Fail
                });
            }

            if (!AppConfiguration.IsNewContentDeliveringScheme)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new DeskAlertsApiResponse<object> 
                {
                    ErrorMessage = "Old content delivery scheme not supported",
                    Status = ResponseStatus.Fail
                });
            }
            
            
            if (timezone.IsNullOrEmpty())
            {
                throw new ArgumentNullException(nameof(timezone));
            }

            try
            {
                var intTimezone = int.Parse(timezone);
                var localDateTime = DateTimeUtils.GetDateTimeByTimeZone(intTimezone, true);
                
                
                var token = GetTokenByGuidFromRequest();

                lock (LockGetUserAlerts)
                {
                    // CR killall5: schedule it!
                    _userService.UpdateUserLastRequest(token.User.Id);
                    _userService.UpdateUserTimeZone(token.User.Id, intTimezone);
                }

                if (!string.IsNullOrEmpty(token.ComputerDomainName))
                {
                    lock (LockUpdateComputer)
                    {
                        _computerService.UpdateComputerLastRequest(token.ComputerName, token.ComputerDomainName);
                    }
                }

                var cacheKey = new ContentDeliveringKey(intTimezone, token.User.Id, Guid.Parse(token.DeskbarId));

                var lockscreens = _lockscreensContentDeliveringService.Get(cacheKey, token.Ip, token.ComputerName);

                var lockascreenIds = lockscreens.IsNullOrEmpty() ? 
                    new List<long>() : lockscreens
                    .Split(',')
                    .Select(long.Parse)
                    .ToList();
                
                    
                var response = new DeskAlertsApiResponse<IEnumerable<LockscreenDto>>
                 {
                     Data = lockascreenIds.Select(x=>GetLockscreenDto(x, token))
                 };
                
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                Logger.Debug(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new DeskAlertsApiResponse<object>
                {
                    ErrorMessage = $"Couldn't get content. Error: {ex}.",
                    Status = ResponseStatus.Error
                });
            }
        }

        #endregion

        #region user/content/screensavers

        [TokenAuthorize]
        [HttpPost, Route("user/content/screensavers")]
        public HttpResponseMessage GetUserScreensavers(ContentRequestDto request)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }
            try
            {
                var token = GetTokenByGuidFromRequest();
                var localDateTime = DateTime.ParseExact(request.LocalTime, DateTimeFormat.Client, CultureInfo.InvariantCulture);
                var response = new DeskAlertsApiResponse<IEnumerable<ScreensaverDto>>
                {
                    Data = _contentService.GetScreensaver(localDateTime, token).Select(x => GetScreensaverDto(x, token, localDateTime))
                };

                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                Logger.Debug(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new DeskAlertsApiResponse<object>
                {
                    ErrorMessage = $"Couldn't get content. Error: {ex}.",
                    Status = ResponseStatus.Error
                });
            }
        }

        #endregion

        #region user/content/received

        /// <summary>
        /// ToDo: Rename to "user/content/received"
        /// </summary>
        /// <param name="request">Contains alerts, wallpapers and screensavers</param>
        /// <returns></returns>
        [TokenAuthorize]
        [HttpPost, Route("user/alerts/received")]
        public HttpResponseMessage PostReceivedAlerts(ContentReceivedRequestDto request)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            if (request.Alerts == null)
            {
                throw new ArgumentNullException(nameof(request.Alerts));
            }

            if (request.Wallpapers == null)
            {
                throw new ArgumentNullException(nameof(request.Wallpapers));
            }

//            if (request.Lockscreens == null)
//            {
//                throw new ArgumentNullException(nameof(request.Lockscreens));
//            }

            if (request.Screensavers == null)
            {
                throw new ArgumentNullException(nameof(request.Screensavers));
            }

            try
            {
                var token = GetTokenByGuidFromRequest();
                _contentService.ReceiveContent(request.Alerts, token);
                _contentService.ReceiveContent(request.Screensavers, token);
                _contentService.ReceiveContent(request.Wallpapers, token);

                if (request.Lockscreens != null)
                    _contentService.ReceiveContent(request.Lockscreens, token);

                foreach (var contentEntity in request.Alerts)
                {
                    var contentType = _alertRepository.GetContentTypeByContentId(contentEntity.Id);
                    _cacheInvalidationService.CacheInvalidation(contentType, contentEntity.Id);
                }

                var response = new DeskAlertsApiResponse<ContentResponseDto>();
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                // ToDo: rewrite after release to Debug
                Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new DeskAlertsApiResponse<object>
                {
                    Data = null,
                    ErrorMessage = $"Couldn't post received alerts. Error: {ex}.",
                    Status = ResponseStatus.Error
                });
            }
        }

        #endregion

        #region user/content/read

        /// <summary>
        /// ToDo: Rename to "user/content/read"
        /// </summary>
        /// <param name="readAlert">The request conatins AlertId identifier</param>
        /// <returns></returns>
        [TokenAuthorize]
        [HttpPost, Route("user/alerts/read")]
        public HttpResponseMessage ReadAlerts(AlertReadDto readAlert)
        {
            try
            {
                var token = GetTokenByGuidFromRequest();
                _contentService.ReadContent(readAlert.AlertId, token);

                var contentType = _alertRepository.GetContentTypeByContentId(readAlert.AlertId);
                _cacheInvalidationService.CacheInvalidation(contentType, readAlert.AlertId);

                var response = new DeskAlertsApiResponse<AlertReadDto>();
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new DeskAlertsApiResponse<object>()
                {
                    Data = null,
                    ErrorMessage = $"Couldn't add information about read alerts with id {readAlert.AlertId}. Error: {ex}.",
                    Status = ResponseStatus.Error
                });
            }
        }

        #endregion


        #region user/content/terminate/{id}

        [TokenAuthorize]
        [HttpPost, Route("user/alerts/terminate/{contentId}")]
        public HttpResponseMessage ReceiveTerminatedContent(long contentId)
        {
            try
            {
                var token = GetTokenByGuidFromRequest();
                _contentService.TerminateAlert(contentId, token);

                _cacheInvalidationService.CacheInvalidation(AlertType.Terminated, contentId);

                return Request.CreateResponse(HttpStatusCode.OK, new DeskAlertsApiResponse<object>());
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new DeskAlertsApiResponse<object>()
                {
                    Data = null,
                    ErrorMessage = $"Couldn't terminate alert {contentId}. Error: {ex}.",
                    Status = ResponseStatus.Error
                });
            }


        }

        #endregion

        #region Methods

        private Token GetTokenByGuidFromRequest()
        {
            var guid = _tokenService.GetGuid(Request.Headers.GetValues("Authorization").First());
            return _tokenService.GetTokenByGuid(guid);
        }

        private WallpaperDto GetWallpaperDto(Alert alert, Token token)
        {
            var content = _contentService.GetContentById(alert.Id);
            return new WallpaperDto
            {
                AlertId = alert.Id,
                Href = content.Body,
                Height = "0",
                Width = "0",
                UserId = token.User.Id.ToString(),
                Title = content.Title,
                Acknowledgment = "0",
                ToDate = content.ToDate?.Millisecond.ToString(),
                Utc = "1",
                IsUtf8 = "1",
                Autoclose = content.Autoclose
            };
        }

        private LockscreenDto GetLockscreenDto(long alertId, Token token)
        {
            var content = _contentService.GetContentById(alertId);
            return new LockscreenDto
            {
                AlertId = alertId,
                Href = content.Body,
                Height = "0",
                Width = "0",
                UserId = token.User?.Id.ToString(),
                Title = content.Title,
                Acknowledgment = "0",
                ToDate = content.ToDate?.Millisecond.ToString(),
                Utc = "1",
                IsUtf8 = "1",
                Autoclose = content.Autoclose
            };
        }

        private ScreensaverDto GetScreensaverDto(Alert alert, Token token, DateTime localDateTime)
        {
            var alertXml = alert.ToXmlString(token.Version, token.DeskbarId, token.UserName, token.User.Id, localDateTime, token.IsMobile);
            return new ScreensaverDto { Id = alert.Id, Xml = alertXml };
        }

        /// <summary>
        /// Create ETag hash to add into response headers.
        /// </summary>
        /// <returns>ETag hash.</returns>
        public string GenerateETagMd5Value()
        {
            try
            {
                var alertsCount = _contentService.GetContentsCount();
                var lastAlertId = _contentService.GetLastContentId();

                var result = HashingUtils.MD5($"{alertsCount},{lastAlertId}");

                return result;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                throw;
            }
        }

        #endregion

    }
}

