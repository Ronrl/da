﻿namespace DeskAlertsDotNet.Api.v1
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;
    using DeskAlerts.ApplicationCore.Dtos;
    using DeskAlerts.ApplicationCore.Dtos.Settings;
    using DeskAlerts.ApplicationCore.Entities;
    using DeskAlerts.ApplicationCore.Interfaces;
    using DeskAlerts.ApplicationCore.Repositories;
    using DeskAlerts.ApplicationCore.Utilities;
    using DeskAlerts.Server.Utils.Interfactes;
    using Parameters;
    using Utils.Services;
    using NLog;

    [RoutePrefix("api/v1/ad")]
    public class AdDomainsController : ApiController
    {
        private readonly IGroupRepository _groupRepository;
        private readonly IDomainRepository _domainRepository;
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private readonly IUserService _userService;
        private readonly IPolicyService _policyService;

        public AdDomainsController()
        {
            var configurationManager = new WebConfigConfigurationManager(Config.Configuration);
            IPolicyRepository policyRepository = new PpPolicyRepository(configurationManager);
            _groupRepository = new PpGroupRepository(configurationManager);
            _domainRepository = new PpDomainRepository(configurationManager);
            var userRepository = new PpUserRepository(configurationManager);
            var policyPublisherRepository = new PpPolicyPublisherRepository(configurationManager);
            var skinRepository = new PpSkinRepository(configurationManager);
            var contentSettingsRepository = new PpContentSettingsRepository(configurationManager);

            _userService = new UserService(userRepository, _groupRepository);
            _policyService = new PolicyService(policyRepository, _groupRepository, _domainRepository, policyPublisherRepository,
                userRepository, skinRepository, contentSettingsRepository);
        }

        public List<AdGroupDto> MappingAdGroups(List<Group> groups)
        {
            List<AdGroupDto> groupDto = new List<AdGroupDto>();
            foreach (Group group in groups)
            {
                groupDto.Add(
                    new AdGroupDto
                    {
                        id = group.Id,
                        text = group.Name,
                        state =
                                new SsoGroupStateDto()
                                {
                                    disabled = false,
                                    opened = false,
                                    @checked = group.PolicyId != null
                                },
                        data = new PolicyDto()
                        {
                            policy_id = group.Policy.Id == 0 ? null : (long?)group.Policy.Id,
                            policy_name = group.Policy.Name,
                            policy_type = group.Policy.Type,
                        }
                    });
            }

            return groupDto;
        }

        public AdDomains MappingAdDomains(Domain domain, bool isExpanded)
        {
            return new AdDomains { children = new List<AdGroupDto>(), text = domain.Name, state = new SsoGroupStateDto() { disabled = false, opened = isExpanded, @checked = false } };
        }


        [HttpPost]
        [Route("getAllDomains")]
        public HttpResponseMessage GetAllAdGroups(GroupSearchDto groupSearch)
        {
            try
            {
                var domainsEntity = _domainRepository.GetAllDomains();
                var domainsDto = new List<AdDomains>();

                if (domainsEntity.Count == 0)
                {
                    return Request.CreateResponse(
                        HttpStatusCode.OK,
                        new DeskAlertsApiResponse<object> { Data = null });
                }
                else
                {
                    foreach (var domain in domainsEntity)
                    {
                        var groups = MappingAdGroups(_groupRepository.GetChildGroupsByDomainName(domain.Name));
                        bool isExpanded = false;
                        if (!string.IsNullOrEmpty(groupSearch.Search))
                        {
                            isExpanded = true;
                            groups = groups.Where(x => x.text.ToUpper().Contains(groupSearch.Search.ToUpper())).ToList();
                        }
                        var domainDto = MappingAdDomains(domain, isExpanded);
                        domainDto.children.AddRange(groups);
                        domainsDto.Add(domainDto);
                    }

                    var responseObject = new DeskAlertsApiResponse<List<AdDomains>> { Data = domainsDto };
                    return Request.CreateResponse(HttpStatusCode.OK, responseObject);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);

                var responseObject =
                    new DeskAlertsApiResponse<object>
                    {
                        Data = null,
                        ErrorMessage = "There is no any Active Directory Domains"
                    };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, responseObject);
            }
        }

        [HttpPost]
        [Route("updateGroup")]
        public HttpResponseMessage UpdateGroup(AdGroupDto adGroup)
        {
            try
            {
                var group = _userService.GetGroupById(adGroup.id);
                if (adGroup.state.@checked)
                {
                    group.PolicyId = adGroup.data.policy_id;
                }
                else
                {
                    _policyService.RemovePolicyFromPublisher((long)adGroup.data.policy_id, group.Id);
                    group.PolicyId = null;
                }

                _userService.UpdateGroup(group);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                _logger.Debug(ex);

                var responseObject = new DeskAlertsApiResponse<object>
                {
                    Data = null,
                    ErrorMessage = $"Can't update group"
                };

                return Request.CreateResponse(HttpStatusCode.InternalServerError, responseObject);
            }
        }
    }
}
