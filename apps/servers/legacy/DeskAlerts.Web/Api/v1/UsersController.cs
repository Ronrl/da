﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DeskAlerts.ApplicationCore.Dtos;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.ApplicationCore.Repositories;
using DeskAlerts.ApplicationCore.Utilities;
using DeskAlerts.Server.Utils;
using DeskAlertsDotNet.Parameters;
using NLog;

namespace DeskAlerts.Server.Api.v1
{
    [RoutePrefix("api/v1/users")]
    public class UsersController : ApiController
    {
        private readonly IConfigurationManager _configurationManager;

        private readonly IDomainRepository _domainRepository;

        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        private readonly IUserRepository _userRepository;

        public UsersController()
        {
            _configurationManager = new WebConfigConfigurationManager(Config.Configuration);
            _domainRepository = new PpDomainRepository(_configurationManager);
            _userRepository = new PpUserRepository(_configurationManager);
        }

        [HttpPost]
        [Route("")]
        public HttpResponseMessage CreateUsers(List<UserInfoDto> users)
        {
            try
            {
                foreach (var user in users)
                {
                    var isExist = _userRepository.GetChildUsersByDomainName(user.DomainName)
                        .Any(u => u.Username == user.Username);

                    if (isExist)
                    {
                        continue;
                    }

                    var domain = _domainRepository.GetAll().SingleOrDefault(d => d.Name == user.DomainName);

                    if (domain == default(Domain))
                    {
                        var id = _domainRepository.Create(new Domain() { Name = user.DomainName });
                        domain = _domainRepository.GetById(id);
                    }

                    var newUser = new User()
                                      {
                                          Username = user.Username,
                                          Password = HashingUtils.MD5(user.Password),
                                          Phone = user.Phone,
                                          Email = user.Email,
                                          RegistrationDateTime = DateTime.Now,
                                          Role = DeskAlerts.ApplicationCore.Entities.User.UserRole,
                                          DomainId = domain.Id
                                      };

                    _userRepository.Create(newUser);
                }

                var responseObject = new DeskAlertsApiResponse<string> { Data = null, Status = ResponseStatus.Success };
                return Request.CreateResponse(HttpStatusCode.OK, responseObject);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                _logger.Debug(ex);
                var responseObject =
                    new DeskAlertsApiResponse<object>
                        {
                            Data = null,
                            ErrorMessage = "Couldn't create users",
                            Status = ResponseStatus.Error
                        };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, responseObject);
            }
        }
    }
}