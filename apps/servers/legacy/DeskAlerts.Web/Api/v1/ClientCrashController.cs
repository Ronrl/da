﻿using DeskAlerts.ApplicationCore.Dtos;
using DeskAlerts.Server.sever.STANDARD.Models;
using DeskAlerts.Server.Utils.Consts;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace DeskAlerts.Server.Api.v1
{
    [RoutePrefix("api/v1/error")]
    public class ClientCrashController : ApiController
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        [HttpPost, Route("client/{os}"), Route("client/")]
        public HttpResponseMessage ReceiveClientDump(string os = null)
        {
            try
            {
                var request = HttpContext.Current.Request;
                var osParams = GetClientCrashesConnector(os);
                var fileExtension = osParams.FileExtension;
                var folderPlatformPrefix = osParams.FolderPlatformPrefix;
                var folder = Directory.CreateDirectory(HttpContext.Current.Server.MapPath($"{FolderVariables.ClientCrashFolder}" + folderPlatformPrefix));
                var filePath = folder.FullName + request.UserHostAddress + "_" + Guid.NewGuid() + fileExtension;

                using (var file = File.Create(filePath))
                {
                    request.InputStream.CopyTo(file);
                }

                return Request.CreateResponse(new DeskAlertsApiResponse<object>());
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new DeskAlertsApiResponse<object>
                {
                    Data = null,
                    ErrorMessage = "The file isn't aproppritate for this action.",
                });
            }
        }

        #region Methods

        private ClientCrashesConnector GetClientCrashesConnector(string os) {
            switch (os)
            {
                case "mac":
                    return new MacOsConnector();
                    break;
                case "win":
                    return new WindowsConnector();
                    break;
                default:
                    return new DefaultConnector();
                    break;
            }
        }

        #endregion

    }
}