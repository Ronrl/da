﻿using DeskAlerts.Server.Utils.Helpers;
using DeskAlertsDotNet.Application;

namespace DeskAlertsDotNet.Api.v1
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;
    using DeskAlerts.ApplicationCore.Dtos;
    using DeskAlerts.ApplicationCore.Dtos.Pages;
    using DeskAlerts.ApplicationCore.Entities;
    using DeskAlerts.ApplicationCore.Interfaces;
    using DeskAlerts.ApplicationCore.Repositories;
    using DeskAlerts.ApplicationCore.Utilities;
    using DeskAlerts.Server.Utils.Managers;
    using DeskAlertsDotNet.Managers;
    using DeskAlertsDotNet.Parameters;
    using DeskAlertsDotNet.Utils.Consts;
    using NLog;

    using Resources;

    [RoutePrefix("api/v1")]
    public class CampaignController : ApiController
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        private readonly IConfigurationManager _configurationManager;

        private readonly ICampaignRepository _campaignRepository;

        private readonly IAlertRepository _alertRepository;

        private readonly Models.User _currentUser;

        public CampaignController(IAlertRepository alertRepository, ICampaignRepository campaignRepository)
        {
            _campaignRepository = campaignRepository;
            _alertRepository = alertRepository;
            _currentUser = UserManager.Default.CurrentUser;
        }

        [HttpPost, Route("campaigns/add")]
        public HttpResponseMessage AddCampaign(CampaignDto campaignDto)
        {
            try
            {
                var campaign = new Campaign()
                {
                    CreateDate = DateTime.Now,
                    EndDate = ParseDateTime(campaignDto.EndDate),
                    StartDate = ParseDateTime(campaignDto.StartDate),
                    Name = campaignDto.Name,
                    IsActive = true,
                    SenderId = _currentUser.Id
                };

                var campaignId = _campaignRepository.Create(campaign);
                campaign = _campaignRepository.GetById(campaignId);
                campaignDto = new CampaignDto(campaign.Status, campaignId);
                var cacheManager = Global.CacheManager;
                cacheManager.RebuildCache(DateTime.Now.ToString(DateTimeFormat.MsSqlFormat, CultureInfo.InvariantCulture));
                return Request.CreateResponse(HttpStatusCode.OK, new DeskAlertsApiResponse<CampaignDto> { Data = campaignDto });
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                _logger.Debug(ex);
                var responseObject = new DeskAlertsApiResponse<object>
                {
                    Data = null,
                    ErrorMessage = "Couldn't add the campaign",
                    Status = ResponseStatus.Error
                };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, responseObject);
            }
        }

        [HttpPost, Route("campaigns/update")]
        public HttpResponseMessage UpdateCampaign(CampaignDto campaignDto)
        {
            try
            {
                var existCampaign = _campaignRepository.GetById(campaignDto.Id);
                var campaign = new Campaign()
                {
                    CreateDate = DateTime.Now,
                    EndDate = ParseDateTime(campaignDto.EndDate),
                    StartDate = ParseDateTime(campaignDto.StartDate),
                    Name = campaignDto.Name,
                    Id = campaignDto.Id,
                    IsActive = existCampaign.IsActive
                };

                _campaignRepository.Update(campaign);
                var cacheManager = Global.CacheManager;
                cacheManager.RebuildCache(DateTime.Now.ToString(DateTimeFormat.MsSqlFormat, CultureInfo.InvariantCulture));
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                _logger.Debug(ex);
                var responseObject = new DeskAlertsApiResponse<object>
                {
                    Data = null,
                    ErrorMessage = "Couldn't update the Campaign",
                    Status = ResponseStatus.Error
                };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, responseObject);
            }
        }

        [HttpPost, Route("campaigns")]
        public HttpResponseMessage GetCampaigns(Pagination pagination)
        {
            try
            {
                var campaigns = _campaignRepository.GetAll();
                var startPageIndex = (pagination.Page - 1) * pagination.RowsPerPage;
                var rowsPerPage = pagination.RowsPerPage == 0 ? campaigns.Count : pagination.RowsPerPage;
                var campaignsDto = campaigns.
                    Select(x => new CampaignDto(x.Status, x.Id)
                    {
                        Id = x.Id,
                        EndDate = DateTimeToString(x.EndDate),
                        StartDate = DateTimeToString(x.StartDate),
                        Name = x.Name,
                        StatusText = GetCampaignStatusText(x.Status),
                        SenderId = x.SenderId
                    }).
                    Skip(startPageIndex).
                    Take(rowsPerPage);
                pagination.RowsNumber = campaigns.Count;
                if (!string.IsNullOrEmpty(pagination.Search))
                {
                    campaignsDto = campaignsDto.Where(x => x.Name.Contains(pagination.Search));
                }

                if (!string.IsNullOrEmpty(pagination.SortBy))
                {
                    switch (pagination.SortBy)
                    {
                        case "st_d":
                            campaignsDto = campaignsDto.OrderBy(x => x.StartDate);
                            break;
                        case "en_d":
                            campaignsDto = campaignsDto.OrderBy(x => x.EndDate);
                            break;
                        case "stat":
                            campaignsDto = campaignsDto.OrderBy(x => x.StatusText);
                            break;
                        default:
                            campaignsDto = campaignsDto.OrderBy(x => x.Name);
                            break;
                    }
                }

                if (!_currentUser.IsAdmin && !_currentUser.Policy.CanViewAll)
                {
                    campaignsDto = campaignsDto.Where(x => x.SenderId == _currentUser.Id);
                }

                var page = new CampaignPage()
                {
                    Campaigns = campaignsDto.ToList(),
                    Pagination = pagination,
                    CanCreate = _currentUser.IsAdmin || _currentUser.Policy[Models.Permission.Policies.CAMPAIGN].CanCreate,
                    CanView = _currentUser.IsAdmin || _currentUser.Policy[Models.Permission.Policies.CAMPAIGN].CanView,
                    CanEdit = _currentUser.IsAdmin || _currentUser.Policy[Models.Permission.Policies.CAMPAIGN].CanEdit,
                    CanDelete = _currentUser.IsAdmin || _currentUser.Policy[Models.Permission.Policies.CAMPAIGN].CanDelete,
                    CanSend = _currentUser.IsAdmin || _currentUser.Policy[Models.Permission.Policies.CAMPAIGN].CanSend,
                    CanStop = _currentUser.IsAdmin || _currentUser.Policy[Models.Permission.Policies.CAMPAIGN].CanStop,
                };

                return Request.CreateResponse(HttpStatusCode.OK, new DeskAlertsApiResponse<CampaignPage> { Data = page });
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                _logger.Debug(ex);
                var responseObject = new DeskAlertsApiResponse<object>
                {
                    Data = null,
                    ErrorMessage = "Couldn't get Campaigns",
                    Status = ResponseStatus.Error
                };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, responseObject);
            }
        }

        [HttpGet, Route("campaigns/{campaignId}")]
        public HttpResponseMessage GetById(long campaignId)
        {
            try
            {
                var campaign = _campaignRepository.GetById(campaignId);

                return Request.CreateResponse(HttpStatusCode.OK, new DeskAlertsApiResponse<CampaignDto>
                {
                    Data = new CampaignDto(campaign.Status, campaign.Id)
                    {
                        Id = campaign.Id,
                        EndDate = DateTimeToString(campaign.EndDate),
                        StartDate = DateTimeToString(campaign.StartDate),
                        Name = campaign.Name,
                        StatusText = GetCampaignStatusText(campaign.Status)
                    }
                });
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                _logger.Debug(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new DeskAlertsApiResponse<object>
                {
                    Data = null,
                    ErrorMessage = "Couldn't get Campaign",
                    Status = ResponseStatus.Error
                });
            }
        }

        [HttpPost, Route("campaigns/{campaignId}/stop")]
        public HttpResponseMessage StopCampaignById(long campaignId)
        {
            try
            {
                var campaign = _campaignRepository.GetById(campaignId);
                campaign.IsActive = false;
                _campaignRepository.Update(campaign);
                var cacheManager = Global.CacheManager;
                cacheManager.RebuildCache(DateTime.Now.ToString(DateTimeFormat.MsSqlFormat, CultureInfo.InvariantCulture));
                foreach (var alert in GetActiveAlertsByCampaignId(campaignId))
                {
                    cacheManager.Delete((int)alert.Id, DateTime.Now.ToString());
                }

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                _logger.Debug(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new DeskAlertsApiResponse<object>
                {
                    Data = null,
                    ErrorMessage = "Couldn't stop the campaign",
                    Status = ResponseStatus.Error
                });
            }
        }

        [HttpPost, Route("campaigns/{campaignId}/start")]
        public HttpResponseMessage StartCampaignById(long campaignId)
        {
            try
            {
                var campaign = _campaignRepository.GetById(campaignId);
                campaign.IsActive = true;
                if (campaign.Status == CampaignStatus.Pending || campaign.Status == CampaignStatus.Invative)
                {
                    campaign.StartDate = DateTime.Now;
                }

                _campaignRepository.Update(campaign);
                var cacheManager = Global.CacheManager;
                cacheManager.RebuildCache(DateTime.Now.ToString(DateTimeFormat.MsSqlFormat, CultureInfo.InvariantCulture));
                foreach (var alert in GetActiveAlertsByCampaignId(campaignId))
                {
                    cacheManager.Add(alert.Id, DateTime.Now.ToString(DateTimeFormat.MsSqlFormat, CultureInfo.InvariantCulture));
                }

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                _logger.Debug(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new DeskAlertsApiResponse<object>
                {
                    Data = null,
                    ErrorMessage = "Couldn't start the campaign",
                    Status = ResponseStatus.Error
                });
            }
        }

        [HttpPost, Route("campaigns/delete")]
        public HttpResponseMessage DeleteCampaigns(List<CampaignDto> campaigns)
        {
            try
            {
                var cacheManager = Global.CacheManager;
                foreach (var campaignDto in campaigns)
                {
                    var campaign = _campaignRepository.GetById(campaignDto.Id);
                    _campaignRepository.Delete(campaign);
                    cacheManager.RebuildCache(DateTime.Now.ToString(DateTimeFormat.MsSqlFormat, CultureInfo.InvariantCulture));
                    foreach (var alert in GetActiveAlertsByCampaignId(campaign.Id))
                    {
                        cacheManager.Delete(alert.Id, DateTimeConverter.ToDbDateTime(DateTime.Now));
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                _logger.Debug(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new DeskAlertsApiResponse<object>
                {
                    Data = null,
                    ErrorMessage = "Couldn't delete the campaigns",
                    Status = ResponseStatus.Error
                });
            }
        }

        private string GetCampaignStatusText(CampaignStatus campaignStatus)
        {
            switch (campaignStatus)
            {
                case CampaignStatus.Invative:
                    return resources.LNG_CAMPAIGN_INACTIVE;
                case CampaignStatus.Pending:
                    return resources.LNG_CAMPAIGN_PENDING;
                case CampaignStatus.Active:
                    return resources.LNG_CAMPAIGN_ACTIVE;
                case CampaignStatus.Stopped:
                    return resources.LNG_CAMPAIGN_STOPPED;
                default:
                    return resources.LNG_CAMPAIGN_FINISHED;
            }
        }

        private DateTime? ParseDateTime(string dateTimeText)
        {
            return string.IsNullOrEmpty(dateTimeText) ?
                null :
                (DateTime?)DateTime.Parse(dateTimeText, CultureInfo.InvariantCulture);
        }

        private string DateTimeToString(DateTime? dateTime)
        {
            return dateTime?.ToString(DateTimeFormat.Iso8601, CultureInfo.InvariantCulture);
        }

        private List<AlertEntity> GetActiveAlertsByCampaignId(long campaignId)
        {
            return _alertRepository.GetByCampaignId(campaignId).Where(x => x.IsActive).ToList();
        }
    }
}