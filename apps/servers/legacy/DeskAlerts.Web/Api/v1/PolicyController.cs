﻿namespace DeskAlertsDotNet.Api.v1
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;

    using DeskAlerts.ApplicationCore.Dtos;
    using DeskAlerts.ApplicationCore.Interfaces;
    using DeskAlerts.ApplicationCore.Repositories;
    using DeskAlerts.ApplicationCore.Utilities;
    using DeskAlerts.Server.Utils.Interfactes;
    using DeskAlertsDotNet.Parameters;
    using DeskAlertsDotNet.Utils.Services;

    using NLog;

    [RoutePrefix("api/v1/policy")]
    public class PolicyController : ApiController
    {
        private readonly IPolicyRepository _policyRepository;

        private readonly IConfigurationManager _configurationManager;

        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public PolicyController()
        {
            _configurationManager = new WebConfigConfigurationManager(Config.Configuration);
            _policyRepository = new PpPolicyRepository(_configurationManager);
        }

        [HttpGet]
        [Route("")]
        public HttpResponseMessage GetAllPolicies()
        {
            try
            {
                var policiesListEntity = _policyRepository.GetAll();
                var policiesListDto = new List<PolicyInfoDto>();

                foreach (var policy in policiesListEntity)
                {
                   policiesListDto.Add(new PolicyInfoDto(policy));
                }

                var responseObject = new DeskAlertsApiResponse<List<PolicyInfoDto>> { Data = policiesListDto };
                return Request.CreateResponse(HttpStatusCode.OK, responseObject);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                _logger.Debug(ex);
                var responseObject = new DeskAlertsApiResponse<object>
                                         {
                                             Data = null,
                                             ErrorMessage = "There is no any policies",
                                             Status = ResponseStatus.Error
                                         };

                return Request.CreateResponse(HttpStatusCode.InternalServerError, responseObject);
            }
        }

        [HttpGet]
        [Route("getAdminPoliciesForGroups")]
        public HttpResponseMessage GetCountOfAdministratorsPolicies()
        {
            try
            {
                var configurationManager = new WebConfigConfigurationManager(Config.Configuration);
                var domainRepository = new PpDomainRepository(configurationManager);
                var groupRepository = new PpGroupRepository(configurationManager);
                var userRepository = new PpUserRepository(configurationManager);
                var policyPublisherRepository = new PpPolicyPublisherRepository(configurationManager);
                var skinRepository = new PpSkinRepository(configurationManager);
                var contentSettingsRepository = new PpContentSettingsRepository(configurationManager);

                IPolicyService policyService =
                    new PolicyService(_policyRepository, groupRepository, domainRepository, policyPublisherRepository, userRepository, skinRepository, contentSettingsRepository);
                return Request.CreateResponse(HttpStatusCode.OK, policyService.GetAdministratorPoliciesForDomainsGroups());
            }
            catch (Exception ex)
            {
                this._logger.Error(ex.Message);
                this._logger.Debug(ex);

                var responseObject = new DeskAlertsApiResponse<object>
                                         {
                                             Data = null,
                                             ErrorMessage =
                                                 "This API request doesn't work as was supposed",
                                             Status = ResponseStatus.Error
                                         };
                return Request.CreateResponse(HttpStatusCode.OK, responseObject);
            }
        }
    }
}
