using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Utilities;
using DeskAlerts.Server.Utils.Interfaces.Services;
using DeskAlerts.Server.Utils.Interfactes;
using DeskAlertsDotNet.Application;
using DeskAlertsDotNet.Attributes;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Parameters;
using DeskAlertsDotNet.Server.Domain;
using DeskAlertsDotNet.Utils.Consts;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DeskAlertsDotNet.API
{
    [DeskAlertsAuthorize]
    public class AlertsController : ApiController
    {
        private readonly ICacheInvalidationService _cacheInvalidationService;
        private readonly IContentReceivedService _contentReceivedService;

        public AlertsController(
            ICacheInvalidationService cacheInvalidationService, 
            IContentReceivedService contentReceivedService)
        {
            _cacheInvalidationService = cacheInvalidationService;
            _contentReceivedService = contentReceivedService;
        }

        public string Get(int id)
        {
            return id.ToString();
        }

        public string Get(string userName, string computerName, string domain)
        {
            var url = Config.Data.GetString("alertsFolder") + "/GetAlerts.aspx" +
                      $"?uname={userName}&computer={computerName}&domain={domain}";

            WebRequest webRequest = WebRequest.Create(url);
            webRequest.Method = "GET";

            var stream = webRequest.GetResponse().GetResponseStream();

            StreamReader st = new StreamReader(stream);

            string responseText = st.ReadToEnd();
            st.Close();
            stream.Close();
            return responseText;
        }

        /// <summary>
        /// <example>
        /// <code>
        /// api url: domain_name/application_name/api/alerts
        /// json headers: apiKey="xxx", Content-Type="application/json"
        /// json query:
        /// {
        /// "version": "v1",
        /// "alert_text":  "json alert text",
        /// "title": "json alert title",
        /// "users": ["ADMINRG-7GVFN91","user22","user2"],
        /// "groups": ["test_group1","test_group2"],
        /// "computers": ["test_computer1","test_computer2"]
        /// }
        /// </code></example>
        /// </summary> 
        public void Post(AlertDto value)
        {
            if (string.IsNullOrEmpty(value.version) || !value.version.Equals("v1"))
            {
                throw new HttpResponseException(
                    Request.CreateErrorResponse(
                        HttpStatusCode.NotFound,
                        "Error using API version. Do: in json request set field: version, available values: v1"));
            }

            if (string.IsNullOrEmpty(value.title))
            {
                throw new HttpResponseException(
                    Request.CreateErrorResponse(HttpStatusCode.NotFound, "Error Title of alert"));
            }
            if (value.title.Length < 1)
            {
                throw new HttpResponseException(
                    Request.CreateErrorResponse(HttpStatusCode.NotFound, "Error Title of alert"));
            }

            if (string.IsNullOrEmpty(value.alert_text))
            {
                throw new HttpResponseException(
                    Request.CreateErrorResponse(HttpStatusCode.NotFound, "Error text of alert"));
            }
            if (value.alert_text.Length < 1)
            {
                throw new HttpResponseException(
                    Request.CreateErrorResponse(HttpStatusCode.NotFound, "Error text of alert"));
            }

            var alertText = string.Empty;

            using (var dbMgr = new DBManager())
            {
                var alertSettingsDto = new AlertSettingsDto();
                string insertQuery;

                AlertType alertType;
                if (!value.IsTicker)
                {
                    alertType = AlertType.SimpleAlert;
                    alertText = $"<p>{value.alert_text} </p><!-- html_title = ''{value.title}'' -->";

                    insertQuery = $@"INSERT INTO alerts(alert_text, title
                                  , type, group_type, type2
                                  , create_date, sent_date, from_date, to_date
                                  , schedule, schedule_type, recurrence
                                  , urgent, toolbarmode, deskalertsmode 
                                  , sender_id, aknown, ticker 
                                  , fullscreen, alert_width, alert_height
                                  , email_sender, email, sms
                                  , desktop, autoclose 
                                  , class, resizable, post_to_blog, social_media
                                  , self_deletable, text_to_call, campaign_id, video 
                                  , approve_status, lifetime, color_code 
                                  , device 
                                  )  
                                  OUTPUT inserted.id  
                                  VALUES(N'{alertText}', N'{value.title}'
                                  , 'S', 'B', 'R' 
                                  , GETDATE(), GETDATE(), GETDATE(), DATEADD(DAY, 1, GETDATE()) 
                                  , '0', '1', '0'
                                  , '0', '0', '0' 
                                  , '1', '0', '0' 
                                  , '{alertSettingsDto.ConfAlertPosition}', '{alertSettingsDto.ConfAlertWidth}', '{alertSettingsDto.ConfAlertHeight}'
                                  , '', '{alertSettingsDto.ConfEmailByDefault}', '{alertSettingsDto.ConfSmsByDefault}'
                                  , '{alertSettingsDto.ConfDesktopAlertByDefault}', '{alertSettingsDto.ConfAutocloseValue}' 
                                  , '1', '0', '0', '0' 
                                  , '0', '0', '-1', '0' 
                                  , '1', '1', '00000000-0000-0000-0000-000000000000' 
                                  , '3' 
                                  )";
                }
                else
                {
                    alertType = AlertType.Ticker;
                    alertText = $"<p>{value.alert_text} </p><!-- ticker_pro --><!-- tickerSpeed=4 --><!-- html_title = ''{value.title}'' -->";

                    insertQuery = $@"INSERT INTO alerts(alert_text, title
                                  , type, group_type, type2
                                  , create_date, sent_date, from_date, to_date
                                  , schedule, schedule_type, recurrence
                                  , urgent, toolbarmode, deskalertsmode 
                                  , sender_id, aknown, ticker, ticker_speed
                                  , fullscreen
                                  , email_sender, email, sms
                                  , desktop, autoclose 
                                  , class, resizable, post_to_blog, social_media
                                  , self_deletable, text_to_call, campaign_id, video 
                                  , approve_status, lifetime, color_code 
                                  , device 
                                  )  
                                  OUTPUT inserted.id  
                                  VALUES(N'{alertText}', N'{value.title}'
                                  , 'S', 'B', 'R' 
                                  , GETDATE(), GETDATE(), GETDATE(), DATEADD(DAY, 1, GETDATE()) 
                                  , '0', '1', '0'
                                  , '0', '0', '0' 
                                  , '1', '0', '1', '4'
                                  , '{alertSettingsDto.ConfTickerPosition}'
                                  , '', '{alertSettingsDto.ConfEmailByDefault}', '{alertSettingsDto.ConfSmsByDefault}'
                                  , '{alertSettingsDto.ConfDesktopAlertByDefault}', '{alertSettingsDto.ConfAutocloseValue}' 
                                  , '1', '0', '0', '0' 
                                  , '0', '0', '-1', '0' 
                                  , '1', '1', '00000000-0000-0000-0000-000000000000' 
                                  , '3' 
                                  )";
                }

                var alertId = dbMgr.GetDataByQuery(insertQuery)
                    .First()
                    .GetInt("id");


                if (value.users != null)
                {
                    foreach (string user in value.users)
                    {
                        new ContentSender().SendByUserName(user, alertId);
                    }

                    _contentReceivedService.CreateRowsForUsersByUserNames(alertId, value.users);
                }

                if (value.groups != null)
                {
                    foreach (string group in value.groups)
                    {
                        new ContentSender().SendToGroupName(group, alertId);
                    }

                    _contentReceivedService.CreateRowsForUsersByGroupNames(alertId, value.groups);
                }

                if (value.computers != null)
                {
                    foreach (string computer in value.computers)
                    {
                        var count = dbMgr.GetDataByQuery("SELECT id FROM computers WHERE name='" + computer + "'")
                            .Count;
                        if (count > 0)
                        {
                            var computerId = dbMgr.GetScalarByQuery<int>($"SELECT id FROM computers WHERE name='{computer}'");
                            dbMgr.ExecuteQuery($"INSERT INTO alerts_sent_comp (alert_id, comp_id) VALUES( '{alertId}' , '{computerId}')");
                        }
                        else
                        {
                            // this version API is ignoring no matched computernames
                            // TODO: add error handle
                        }
                    }

                    _contentReceivedService.CreateRowsForUsersByComputerNames(alertId, value.computers);
                }

                if (value.OUs != null)
                {
                    foreach (string ou in value.OUs)
                    {
                        new ContentSender().SendToOrganizationUnitsName(ou, alertId);
                    }

                    _contentReceivedService.CreateRowsForUsersByOrganizationUnitNames(alertId, value.OUs);
                }

                if (AppConfiguration.IsNewContentDeliveringScheme)
                {
                    _cacheInvalidationService.CacheInvalidation(alertType, alertId);
                }
                else
                {
                    // должен быть после всех алёртов
                    Global.CacheManager.Add(alertId, DateTime.Now.ToString(DateTimeFormat.MsSqlFormat, CultureInfo.InvariantCulture));
                }
            }
        }

        public class AlertSettingsDto
        {
            public string ConfTickerPosition => Settings.Content["ConfTickerPosition"];
            public bool ConfUrgentByDefault => Settings.Content["ConfUrgentByDefault"] == "1";
            public bool ConfAcknowledgementByDefault => Settings.Content["ConfAcknowledgementByDefault"] == "1";
            public bool ConfUnobtrusiveByDefault => Settings.Content["ConfUnobtrusiveByDefault"] == "1";
            public bool ConfSelfDeletableByDefault => Settings.Content["ConfSelfDeletableByDefault"] == "1";
            public string ConfDesktopAlertByDefault => Settings.Content["ConfDesktopAlertByDefault"];
            public string ConfEmailByDefault => Settings.Content["ConfEmailByDefault"];
            public string ConfSmsByDefault => Settings.Content["ConfSMSByDefault"];
            public string ConfAlertPosition
            {
                get
                {
                    var result = Settings.Content["ConfPopupType"] == "F"
                        ? "1"
                        : Settings.Content["ConfAlertPosition"];

                    return result;
                }
            }
            public string ConfAlertWidth => Settings.Content["ConfAlertWidth"];
            public string ConfAlertHeight => Settings.Content["ConfAlertHeight"];
            public string ConfAutocloseValue
            {
                get
                {
                    var result = "0";

                    if (Settings.Content["ConfAutocloseByDefault"] == "1")
                    {
                        result = (int.Parse(Settings.Content["ConfAutocloseValue"]) * 60).ToString();
                    }

                    return result;
                }
            }
        }

        /// <summary>
        /// Alert model.
        /// </summary>
        public class AlertDto
        {
            private bool _isTicker = false;

            public string version { get; set; }

            public string alert_text { get; set; }

            public string title { get; set; }

            public bool IsTicker
            {
                get => _isTicker;
                set => _isTicker = (bool)value;
            }

            public List<string> users { get; set; }

            public List<string> computers { get; set; }

            public List<string> groups { get; set; }

            public List<string> OUs { get; set; }
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}