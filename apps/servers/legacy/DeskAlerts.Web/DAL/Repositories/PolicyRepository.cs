﻿namespace DeskAlertsDotNet.DAL.Repositories
{
    using DeskAlertsDotNet.DataBase;

    public class PolicyRepository
    {
        /// <summary>
        /// Method that returns id of policy for current publisher
        /// </summary>
        /// <param name="userId">Id of current publisher</param>
        /// <returns>Current publisher's policy id (string type because of default empty value for this parameter)</returns>
        public static int GetPolicyIdFromPublishersByUserId(int userId)
        {
            if (userId > 0)
            {
                using (DBManager dbMgr = new DBManager())
                {
                    return dbMgr.GetScalarQuery<int>(
                        $"select policy_id from policy_editor where editor_id ={userId}");
                }
            }
            else
            {
                Pages.DeskAlertsBasePage.Logger.Error("Current publisher ID is equal " + userId);
            }

            return 0;
        }

        /// <summary>
        /// This method counts the number of recipients in users for current publisher's policy
        /// </summary>
        /// <param name="curUserId">Id of current publisher's policy</param>
        /// <returns>Number of recipients users for current publisher's policy</returns>
        public static int CountUsersForCurrentPublisher(int curUserId)
        {
            int result = 0;

            if (curUserId > 0)
            {
                using (DBManager dbMgr = new DBManager())
                {
                    result = dbMgr.GetScalarQuery<int>($"SELECT count(user_id) FROM policy_user WHERE policy_id = {curUserId}");
                }
            }
            else
            {
                Pages.DeskAlertsBasePage.Logger.Error("Current publisher ID is wrong and equal " + curUserId);
            }

            return result;
        }

        /// <summary>
        /// This method counts the number of recipients in groupsfor current publisher's policy
        /// </summary>
        /// <param name="curUserId">Id of current publisher's policy</param>
        /// <returns>Number of recipients groups for current publisher's policy</returns>
        public static int CountGroupsForCurrentPublisher(int curUserId)
        {
            int result = 0;

            if (curUserId > 0)
            {
                using (DBManager dbMgr = new DBManager())
                {
                    result = dbMgr.GetScalarQuery<int>($"SELECT count([policy_id]) FROM [policy_group] WHERE [policy_id] = {curUserId}");
                }
            }
            else
            {
                Pages.DeskAlertsBasePage.Logger.Error("Current publisher ID is wrong and equal " + curUserId);
            }

            return result;
        }
    }
}
