﻿namespace DeskAlertsDotNet.DAL
{
    using System;

    using DeskAlertsDotNet.DataBase;
    using DeskAlertsDotNet.Structs;

    /// <summary>
    /// Repository for Logs.
    /// </summary>
    public class LogsRepository
    {
        /// <summary>
        /// Deleting old logs.
        /// </summary>
        /// <param name="daysToKeepLogs">Number of days after which logs are deleted.</param>
        public void DeleteOldLogsEntries(int daysToKeepLogs)
        {
            daysToKeepLogs = Math.Abs(daysToKeepLogs);

            DateTime lastLogRemoveDate = DateTime.Now.AddDays(-daysToKeepLogs);

            if (lastLogRemoveDate >= DateTime.Now.AddYears(-100) &&
                lastLogRemoveDate <= DateTime.Now.AddYears(100))
            {
                string queryString = $"DELETE FROM Logs WHERE DateTimeUTC < CONVERT(datetime, '{lastLogRemoveDate}')";

                try
                {
                    using (DBManager dbMgr = new DBManager())
                    {
                        dbMgr.ExecuteQuery(queryString);
                    }
                }
                catch (Exception ex)
                {
                    Pages.DeskAlertsBasePage.Logger.Debug(ex, LogsText.GetLogText(LogsText.LogTextList.DataBaseExecuteError));
                }
            }
        }
    }
}