﻿namespace DeskAlerts.Server
{
    using System;
    using ApplicationCore.Encryption;
    using DeskAlertsDotNet.DataBase;
    using DeskAlertsDotNet.Managers;
    using DeskAlertsDotNet.Pages;
    using DeskAlertsDotNet.Parameters;

    public partial class GetWallpaper : DeskAlertsBasePage
    {
        private static readonly string EncryptKey = Config.Data.GetString("ENCRYPT_KEY");
        
        // TODO Add user_id and deskbar_id to GetWallpaper request
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (string.IsNullOrEmpty(Request["id"]))
            {
                return;
            }

            string wallpaperText = dbMgr.GetScalarQuery<string>($"SELECT alert_text FROM alerts WHERE id={Request["id"]}");

            Response.Write(Config.Data.HasModule(Modules.ENCRYPT) ? Aes256Encryptor.EncryptData(wallpaperText, EncryptKey) : wallpaperText);
        }
    }
}