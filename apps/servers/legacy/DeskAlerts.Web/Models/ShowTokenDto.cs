﻿using System;

namespace DeskAlerts.Server.Models
{
    public class ShowTokenDto
    {
        public string TokenGuid { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string DeskbarId { get; set; }
        public string DomainName { get; set; }
        public string DistinguishedName { get; set; }
        public string ComputerName { get; set; }
        public string ComputerDomainName { get; set; }
        public string Ip { get; set; }
    }
}