﻿namespace DeskAlertsDotNet.Models.Cache
{
    public class ReceivedAlertCache
    {
        public int AlertId { get; set; }

        public string Key { get; set; }
    }
}