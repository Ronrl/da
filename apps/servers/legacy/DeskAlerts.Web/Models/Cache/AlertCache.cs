﻿namespace DeskAlertsDotNet.Models.Cache
{
    /// <summary>
    /// Alert cache view model
    /// </summary>
    public class AlertCache
    {
        public long AlertId { get; set; }

        public string Date { get; set; }
    }
}