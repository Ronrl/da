﻿namespace DeskAlerts.Server.Models.Cache
{
    public class ShowCacheDto
    {
        public string UserName { get; set; }
        public string ClsId { get; set; }
        public long AlertId { get; set; }
        public string AlertTitle { get; set; }

        public ShowCacheDto(string userName, string clsId, long alertId, string alertTitle)
        {
            UserName = userName;
            ClsId = clsId;
            AlertId = alertId;
            AlertTitle = alertTitle;
        }
    }
}