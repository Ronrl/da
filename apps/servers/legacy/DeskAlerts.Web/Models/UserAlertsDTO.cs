﻿namespace DeskAlertsDotNet.Models
{
    using System.Collections.Generic;

    public class UserAlertsDTO
    {
        public string UserName { get; set; }

        public IEnumerable<Alert> Alerts { get; set; }
    }
}