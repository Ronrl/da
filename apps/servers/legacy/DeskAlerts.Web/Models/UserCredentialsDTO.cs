﻿namespace DeskAlerts.Server.Models
{
    /// <summary>
    /// Model for user credentials to receive encryption key.
    /// User credentials consists of Username and Password
    /// </summary>
    public class UserCredentialsDto
    {
        /// <summary>
        /// Username for client credentials
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// Password for client credentials
        /// </summary>
        public string Password { get; set; }
    }
}