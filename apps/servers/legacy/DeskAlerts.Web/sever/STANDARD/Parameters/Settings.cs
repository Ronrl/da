﻿using System.Collections.Generic;
using DeskAlertsDotNet.DataBase;

namespace DeskAlertsDotNet.Parameters
{
    public class Settings : ParametersLoader<Settings>
    {
        protected override string DataTable => "settings";

        protected override string KeyCollumn => "name";

        protected override string ValuesCollumn => "val";

        public static bool IsTrial => !Content["supermario"].Equals("0");

        public override void OnDataLoaded(ref Dictionary<string, string> p)
        {
            base.OnDataLoaded(ref p);
            var dbMgr = new DBManager();
            var defaultLanguage = p.ContainsKey("ConfDefaultLanguage") ? p["ConfDefaultLanguage"] : "EN";
            var checker = dbMgr.GetScalarByQuery("SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE column_name = '" + defaultLanguage + "' and table_name = 'languages'");

            if (checker == null)
            {
                p["ConfDefaultLanguage"] = "EN";
            }
                
            dbMgr.Dispose();
        }
    }
}