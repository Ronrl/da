﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DeskAlertsDotNet.DataBase;

namespace DeskAlertsDotNet.Parameters
{
    using NLog;

    public abstract class ParametersLoader<T> where T : ParametersLoader<T>, new()
    {
        protected abstract string DataTable { get; }

        protected abstract string KeyCollumn { get; }

        protected abstract string ValuesCollumn { get; }

        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        static T _content;

        public static T Content
        {
            get
            {
                if (_content == null)
                {
                    return _content = new T();
                }

                return _content;
            }
        }

        public virtual void OnDataLoaded(ref Dictionary<string, string> p) { }

        protected ParametersLoader()
        {
            LoadData();
            OnDataLoaded(ref parameters);
        }

        public string[] Keys
        {
            get
            {
                return parameters.Keys.ToArray();

            }
        }

        public string this[string key]
        {
            get
            {
                if (!parameters.ContainsKey(key))
                    return string.Empty;

                return parameters[key];
            }
        }

        public int GetInt(string key)
        {

            try
            {
                if (!parameters.ContainsKey(key))
                    return 0;

                return Convert.ToInt32(parameters[key]);
            }
            catch (Exception ex)
            {
                Pages.DeskAlertsBasePage.Logger.Debug(ex);
                return 0;
            }
        }

        Dictionary<string, string> parameters = new Dictionary<string, string>();

        public virtual void Reload()
        {
            try
            {
                parameters.Clear();
                LoadData();
                OnDataLoaded(ref parameters);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                _logger.Debug(ex);
            }
        }

        private void LoadData()
        {
            try
            {
                DBManager dbMgr = new DBManager();
                T child = this as T;

                DataSet dSet = dbMgr.GetDataByQuery(
                    "SELECT DISTINCT " + child.KeyCollumn + " as [key] ," + child.ValuesCollumn + " as [value] FROM "
                    + DataTable);

                foreach (DataRow dRow in dSet)
                {
                    parameters.Add(dRow.GetString("key"), dRow.GetString("value"));
                }
            }
            catch (Exception ex)
            {
                _logger.Error("There is a problem with loading settings for application", ex.Message);
                _logger.Debug(ex);
            }
        }
    }
}