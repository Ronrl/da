using System;
using System.Web;
using System.Web.Configuration;
using System.Configuration;
using NLog;

namespace DeskAlertsDotNet.Parameters
{
    public enum Modules
    {
        AD,
        SSO,
        /// <summary>
        /// Unused module
        /// </summary>
        ED,
        SMS,
        SMPP,
        /// <summary>
        /// Unused module
        /// </summary>
        TEXTTOCALL,
        ENCRYPT,
        SURVEYS,
        STATISTICS,
        EMAIL,
        SCREENSAVER,
        RSS,
        WALLPAPER,
        LOCKSCREEN,
        BLOG,
        TWITTER,
        LINKEDIN,
        FULLSCREEN,
        /// <summary>
        /// Module included in standard DeskAlerts package
        /// </summary>
        APPROVE,
        DIGITAL_SIGNAGE,
        /// <summary>
        /// Module included in standard DeskAlerts package
        /// </summary>
        CAMPAIGN,
        VIDEO,
        TICKER,
        TICKER_PRO,
        MULTIPLE,
        EMERGENCY,
        MOBILE,
        YAMMER,
        BULK_UPLOAD_OF_RECIPIENTS,
        IMPORT_PHONES
    }
    public class Config
    {
        private static Config _conf;
        public static Configuration Configuration { get{ return conf; } }
        private static Configuration conf;
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public static Config Data
        {
            get
            {
                
                if(_conf == null)
                {
                    return _conf = new Config();
                }

                return _conf;
            }
            
        }

        public static KeyValueConfigurationCollection AppSettings
        {
            get
            {
                if (_conf == null)
                {
                    _conf = new Config();
                    return conf.AppSettings.Settings;
                }

                return conf.AppSettings.Settings;
            }
        }


        private Config()
        {
            try
            {
                conf = WebConfigurationManager.OpenWebConfiguration("~");
            }
            catch (ArgumentException ex)
            {
                Pages.DeskAlertsBasePage.Logger.Debug(ex);
                conf = WebConfigurationManager.OpenWebConfiguration(HttpRuntime.AppDomainAppVirtualPath);
            }
        }

        public bool HasModule(Modules module)
        {
            string moduleName = module.ToString("F");

            return conf.AppSettings.Settings[moduleName] != null;
        }

        public bool HasModuleAndValue(Modules module)
        {
            var moduleName = module.ToString("F");

            if (conf.AppSettings.Settings[moduleName] == null)
            {
                return false;
            }

            return GetString(moduleName) == "1";
        }

        public bool IsOptionEnabled(string opt)
        {
            if (conf.AppSettings.Settings[opt] == null)
                return false;

            return GetInt(opt) == 1;
        }
        public  int GetInt(string key)
        {
            if (conf.AppSettings.Settings[key] == null)
                return -1;

            return Convert.ToInt32(conf.AppSettings.Settings[key].Value);
        }
        public  string GetString(string key)
        {
            if (conf.AppSettings.Settings[key] == null)
                return "";
            return conf.AppSettings.Settings[key].Value;
        }

        public bool GetBool(string key)
        {
            var setting = conf.AppSettings.Settings[key];
            var result = setting != null && Convert.ToBoolean(setting.Value);
            return result;
        }

        public  double GetDouble(string key)
        {
            return Convert.ToDouble(conf.AppSettings.Settings[key].Value);        
        }

        public string GetLogsDirectory()
        {
            return Data.GetString("alertsFolder") + "\\admin\\logs";
        }

        /// <summary>
        /// Get Cache manager replication instances
        /// </summary>
        public string[] GetServerCompanions()
        {
            return new string[] { };
        }

        public bool IsDemo
        {
            get{
               return conf.AppSettings.Settings["DEMO"] != null;
            }
        }
        
        public static Uri GetAlertsDir()
        {
            try
            {
                var result = Data.GetString("alertsDir");
                if (string.IsNullOrEmpty(result))
                {
                    throw new Exception("Server url (alertsDir) is empty.");
                }

                return new Uri(result);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Server url (alertsDir) cann't be read.");
                throw;
            }
        }
    }
}