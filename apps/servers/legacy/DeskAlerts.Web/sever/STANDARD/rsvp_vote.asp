<% Response.Expires = 0 %>
<% db_backup_no_redirect = 1 %>
<!-- #include file="admin/config.inc" -->
<!-- #include file="admin/db_conn.asp" -->
<!-- #include file="admin/lang.asp" -->
<%
surveyId = Clng(Request("survey_id"))
answerId = Clng(Request("answer"))
userId = Clng(Request("user_id"))
if ConfForceSelfRegWithAD=1 then
	SQL = "SELECT u.id FROM users as u WHERE u.id ="&userId
	Set RSMimic = Conn.Execute(SQL)
	if not RSMimic.EOF then
		userId = RSMimic("id")
	end if
end if
answer = Replace(Request("custom_answer"), "'", "''")
desk_id = Replace(Request("deskbar_id"), "'", "''")

if ConfEnableMultiVote = 1 then
	desk_query = " AND (clsid='" & desk_id & "' OR clsid='')"
else
	desk_query = ""
end if

if surveyId <> "" and userId <> "" then
	set RS = Conn.Execute("SELECT sender_id FROM surveys_main WHERE id=" & surveyId)
	if not RS.EOF then
		alertId = RS("sender_id")
		Conn.Execute("UPDATE alerts_received SET vote=1 WHERE alert_id=" & alertId & " AND user_id=" & userId & desk_query)

		'select answer ids for the question
		answer_ids=""
		set RSAnswer = Conn.Execute("SELECT a.id as answer_id FROM surveys_answers a INNER JOIN surveys_questions q ON q.id=a.question_id WHERE q.survey_id=" & surveyId & " AND q.question_number = 1")
		do While not RSAnswer.EOF
			answer_id=RSAnswer("answer_id")
			if answer_ids <> "" then
				answer_ids = answer_ids & " OR " & " answer_id=" & answer_id
			else
				answer_ids = answer_ids & " answer_id=" & answer_id
			end if
			RSAnswer.MoveNext
		loop
		
		'text = "<img src='admin/images/langs/"& default_lng &"/close_button.gif' alt='Close' border='0' onclick='if(window.external) window.external.Close();'/>"
        text = "<img src='admin/images/langs/"& default_lng &"/close_button.gif' alt='Close' border='0' onclick='window.external.Close();'/>"

		set RS1 = Conn.Execute("SELECT id FROM surveys_answers_stat WHERE ("& answer_ids &") AND user_id=" & userId & desk_query)
        set notTerminated = Conn.Execute("SELECT id FROM alerts WHERE id = " & alertId & " AND terminated = 1")
       
		if (RS1.EOF AND notTerminated.EOF) then
			Conn.Execute("INSERT INTO surveys_answers_stat (user_id, answer_id, date, clsid) VALUES (" & userId & ", " & answerId & ", GETDATE(), '" & desk_id & "')")
    
			set RS1 = Conn.Execute("SELECT q.id, a.question_id FROM surveys_questions q LEFT JOIN surveys_custom_answers_stat a ON q.id=a.question_id AND a.user_id=" & userId & desk_query &" WHERE q.survey_id=" & surveyId & " AND q.question_number = 2")
			if not RS1.EOF then
				if isnull(RS1("question_id")) then
					Conn.Execute("INSERT INTO surveys_custom_answers_stat (user_id, question_id, date, clsid, answer) VALUES (" & userId & ", " & RS1("id") & ", GETDATE(), '" & desk_id & "', N'" & answer & "')")
				end if
			end if
    
			Response.Write "<html><body onload="" window.external.Close();"">"& text &"</body></html>"
    
		else'already voted
			text = "<br/><b><center>Sorry! You can't respond to this message anymore.</b><br/><br/><br/>" & text
			Response.Write "<html>" + text + "</html>"
		end if
	end if
end if

%><!-- #include file="admin/db_conn_close.asp" -->