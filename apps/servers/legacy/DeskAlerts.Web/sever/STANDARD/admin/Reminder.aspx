﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Reminder.aspx.cs" Inherits="DeskAlertsDotNet.Pages.Reminder" %>
<%@ Import NameSpace="DeskAlertsDotNet.Parameters" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta charset="utf-8">
<title>Maintenance Expiration Reminder</title>
<link href="css/style.css" rel="stylesheet" type="text/css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<link rel="stylesheet" href="/css/style9.css">
<script>
    function prepare_link() {
        messageSubject = "DeskAlerts Annual Maintenance";
        messageBody = "I want to talk about prolongation of my license";

        document.getElementById('email_us').href='mailto:sales@deskalerts.com?Subject=' + messageSubject + '&body=' + messageBody;
    }

    $(document).ready(function () {
        $("#prolongButton").click(function () {
            var prolongKeyValue = $("#prolongKeyInput").val();

            if (prolongKeyValue != "") {
                jQuery.ajax({
                    url: 'Reminder.aspx/ProlongLicense',
                    type: "POST",
                    data: "{'prolongKeyValue' : '" + prolongKeyValue + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var result = data["d"];

                        if (result === "OK") {
                            alert("Your license successfully extended!");
                            location.reload();
                        } else {
                            alert(result);
                        }
                    },
                    failure: function (msg) { alert("Wild Error is occured!"); }
                });
            } else {
                alert("Please enter prolong key.");
            }
        });
    });
</script>
</head>
<body>
<form id="form1" runat="server">
    <div  style=''>
        <br/>
        <div runat="server" id="currentDAVersionDiv">
            <h1>Your DeskAlerts version is: <b><%=version%></b></h1>
        </div>
        <div runat="server" id="licenseExpiredDiv">
            <h1>License expired (<b><font style='color:red;'><%=licenseExpireDate%></font></b>)</h1>
            <div>
                <h2>To prolong your DeskAlerts licence, enter key to the next field:</h2>
                <input id="prolongKeyInput"/>
                <a id="prolongButton" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary">
                    <span class="ui-button-icon-primary ui-icon ui-icon-unlocked"></span>
                    <span class="ui-button-text">Prolong</span>
                </a>
            </div>
        </div>
        <div runat="server" id="expireInDaysDiv">
            <div style='width:100%;height:auto;clear:both;'><h1>Annual maintenance expires in <font style='color:red;'><%=days%></font> days (<%=contractDate%>)</h1></div>
            <div id="message_div" style='clear:both;display:none;margin-top:5px;'>
                <textarea id="comments_message" placeholder="Please, write your comments here..." style='width:100%;height:100px !important;max-height:100px !important;width:770px !important;max-width:770px !important;'></textarea>
            </div>
        </div>
        <div runat="server" id="expiredDiv">
            <div style='width:100%;'><h1>Annual maintenance expired (<b><font style='color:red;'><%=contractDate%></font></b>)<h1></div>
            <div style='width:100%;text-align:center;'>
                To prolong annual maintenance please email us <a href='mailto:sales@deskalerts.com?Subject=MaintenanceExpirationReminder' target='_top'><b><i>sales@deskalerts.com</i></b></a>
            </div>
        </div>
        <p id='cookie'></p>
        <div>
            <div runat="server" id="nonButtonDiv" style="position:absolute;bottom:0;right:0;margin-right:10px;margin-bottom:5px;">
                    <label for='maintenance_reminder_checkbox'>Do not show again</label>
	                <input type='checkbox' name='maintenance_reminder_checkbox' id='maintenance_checkbox' checked/>
            </div>
            <div runat="server" id="contactDiv">
                <div style='position:absolute;bottom:0;left:0;margin-bottom:15px;margin-left:10px;'>
	                <div style='clear:both;'><a href='#' onClick='prepare_link();' style='text-decoration:none;' id='email_us'><div style='background-color:#f0f0f1;padding-left:10px;padding-right:10px;padding-top:5px;padding-bottom:5px;border:1px solid gray;width:110px;text-align:center;float:left;'><b>Submit & Email</b></div></a><div style='float:left;padding-left:5px;padding-top:5px;'>In order to prolong annual maintenance please contact sales@deskalerts.com</div></div>
                    <div style='clear:both;'><div style='float:left;padding-left:5px;padding-top:5px;'><b>What is included?</b></div></div>
                    <div style='clear:both;'><div style='float:left;padding-left:5px;padding-top:5px;'>
                        <ul>
                            <li>Product version upgrades</li>
                            <li>Product minor updates, critical hotfix maintenance (bug fixes)</li>
                            <li>Product specific information that is available in the online DeskAlerts Knowledge Center</li>
                            <li>Software updates and bug fixes during 1 year</li>
                            <li>Dedicated customer service manager</li>
                            <li>Customer service provides assistance for several types of requests -  general questions, account maintenance, licensing and renewals.</li>
                            <li>Modification Request and Problem Report Help Desk: Standard support plan is included</li>   
                        </ul>                
                    </div>
                    </div>
	                <div style='clear:both;'><div style='float:left;padding-left:5px;padding-top:5px;'>You can change the current support plan at any time. To request more information please visit <a href="https://www.alert-software.com/support">Support page</a></div></div>
                    <div style='clear:both;'><div style='float:left;padding-left:5px;padding-top:5px;'><a href="https://www.alert-software.com/pro-services">Learn more</a> about our services </div></div>
                </div>
            </div>
        </div>
    </div>
</form>
</body>
</html>
