﻿using DeskAlertsDotNet.Parameters;
using System;
using System.IO;
using System.Web;

namespace DeskAlertsDotNet.Pages
{
    public partial class PreviewBody : DeskAlertsBasePage
    {
        protected string jsSource = "";
        protected string skinPath;
        protected string SkinCss = string.Empty;

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            var skinId = Request.GetParam("skin_id", "default");
            skinPath = $"{Config.Data.GetString("alertsDir")}/admin/skins/{skinId}/version/";
            SkinCss = $"href=\"{Path.Combine(skinPath, "skin.min.css")}\"";

            var customJs = Request.GetParam("custom_js", "");
            if (customJs.Length > 0)
            {
                var customJsPath = $"skins/{skinId}/version/{customJs}";
                jsSource = File.ReadAllText(Server.MapPath(customJsPath)).Replace("file://", ".");
            }

            mainDiv.InnerHtml = HttpUtility.HtmlDecode(Request.GetParam("alert_html", ""));
        }
    }
}