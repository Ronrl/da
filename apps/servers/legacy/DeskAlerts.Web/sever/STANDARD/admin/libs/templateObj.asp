<%
'
'    Filename: TemplateObj.asp
'    Generated with CodeCharge 2.0.3
'    ASP 2.0 & Templates.ccp build 10/17/2001
'         
'    Usage:
'     LoadTemplate server.mappath("/templates/new.html"), "main"
'     SetVar "ID", 2
'     SetVar "Value", "Name"
'     Parse "DynBlock", False 'or True if you want to create a list
'     Parse "main", False
'     PrintVar "main"
'

Dim objFSO
Dim objStream

Dim DBlocks
Dim ParsedBlocks

Sub SetBlock(sTplName, sBlockName)
	Dim nName
	if not DBlocks.Exists(sBlockName) then
		DBlocks.Add sBlockName, getBlock(DBlocks(sTplName), sBlockName)
	end if
	DBlocks(sTplName) = replaceBlock(DBlocks(sTplName), sBlockName)
	nName = NextDBlockName(sBlockName)
	while not (nName = "")
		SetBlock sBlockName, nName
		nName = NextDBlockName(sBlockName)
	wend
End Sub

Sub LoadTemplateFromString(sName, sValue)
	Dim nName
	if not isObject(DBlocks) then 
		Set DBlocks = Server.CreateObject("Scripting.Dictionary")
		Set ParsedBlocks = Server.CreateObject("Scripting.Dictionary")
	end if
	if not DBlocks.Exists(sName) then
		DBlocks.Add sName, sValue
	else
		DBlocks(sName) = sValue
	end if
	nName = NextDBlockName(sName)
	while not (nName = "")
		SetBlock sName, nName
		nName = NextDBlockName(sName)
	wend
End Sub

Sub LoadTemplate(sPath, sName)
	Dim nName
	if not isObject(objFSO) then 
		Set objFSO = Server.CreateObject("Scripting.FileSystemObject")
		Set objStream = Server.CreateObject("ADODB.Stream")
		objStream.Open
		objStream.CharSet = "UTF-8"
	end if
	if not isObject(DBlocks) then 
		Set DBlocks = Server.CreateObject("Scripting.Dictionary")
		Set ParsedBlocks = Server.CreateObject("Scripting.Dictionary")
	end if
	if objFSO.FileExists(sPath) then
		objStream.LoadFromFile sPath
		if not DBlocks.Exists(sName) then
			DBlocks.Add sName, objStream.ReadText
		else
			DBlocks(sName) = objStream.ReadText
		end if
		nName = NextDBlockName(sName)
		while not (nName = "")
			SetBlock sName, nName
			nName = NextDBlockName(sName)
		wend
	end if
End Sub

Sub UnloadTemplate()
	if isObject(objFSO) then 
		Set objFSO = nothing
		Set objStream = nothing
	end if
	if isObject(DBlocks) then 
		Set DBlocks = nothing
		Set ParsedBlocks = nothing
	end if
End Sub

Function GetVar(sName)
	if isObject(DBlocks) then
		if DBlocks.Exists(sName) then
			GetVar = DBlocks(sName)
		else
			GetVar = Null
		end if
	else
		GetVar = Null
	end if
End Function

Function SetHtmlVar(sName, sValue)
	SetHtmlVar = SetVar(sName, HtmlEncode(sValue))
End Function

Function SetVar(sName, sValue)
	if not isObject(DBlocks) then
		Set DBlocks = Server.CreateObject("Scripting.Dictionary")
		Set ParsedBlocks = Server.CreateObject("Scripting.Dictionary")
	end if
	if ParsedBlocks.Exists(sName) then
		ParsedBlocks(sName) = replace(replace(sValue&"", "{", "&#123;"), "}", "&#125;")
	else
		ParsedBlocks.add sName, replace(replace(sValue&"", "{", "&#123;"), "}", "&#125;")
	end if
End Function

Function Parse(sTplName, bRepeat)
	if isObject(ParsedBlocks) and isObject(DBlocks) then
		if ParsedBlocks.Exists(sTplName) then
			if bRepeat then
				ParsedBlocks(sTplName) = ParsedBlocks(sTplName) & ProceedTpl(DBlocks(sTplName))
			else
				ParsedBlocks(sTplName) = ProceedTpl(DBlocks(sTplName))
			end if
		else 
			ParsedBlocks.add sTplName, ProceedTpl(DBlocks(sTplName))
		end if
	end if
End Function

Function PrintVar(sName)
	if isObject(ParsedBlocks) then
		PrintVar = ParsedBlocks(sName)
	else
		PrintVar = ""
	end if
End function

Function ProceedTpl(sTpl)
	if isObject(ParsedBlocks) and isObject(DBlocks) then
		Dim begin_pos, end_pos, sTmp
		ProceedTpl = sTpl
		begin_pos = instr(1, ProceedTpl, "{")
		do while begin_pos > 0
			end_pos = instr(begin_pos + 1, ProceedTpl, "}")
			if end_pos = 0 then exit do
			sName = mid(ProceedTpl, begin_pos + 1, end_pos - begin_pos - 1)
			if ParsedBlocks.Exists(sName) then
				ProceedTpl = left(ProceedTpl, begin_pos - 1) & ParsedBlocks(sName) & mid(ProceedTpl, end_pos + 1)
				begin_pos = begin_pos + len(ParsedBlocks(sName))
			elseif DBlocks.Exists(sName) then
				sTmp = ProceedTpl(DBlocks(sName))
				ProceedTpl = left(ProceedTpl, begin_pos - 1) & sTmp & mid(ProceedTpl, end_pos + 1)
				begin_pos = begin_pos + len(sTmp)
			else
				begin_pos = begin_pos + 1
			end if
			begin_pos = instr(begin_pos, ProceedTpl, "{")
		loop
	end if
End Function

Function getBlock(sTemplate, sName)
	Dim BBlock, EBlock, alpha
	alpha = len(sName) + 12
	BBlock = instr(sTemplate, "<!--Begin" & sName & "-->")
	EBlock = instr(sTemplate, "<!--End" & sName & "-->")
	if not (BBlock = 0 or EBlock = 0) then
		getBlock = mid(sTemplate, BBlock + alpha, EBlock - BBlock - alpha)
	else
		getBlock = ""
	end if
End Function

Function replaceBlock(sTemplate, sName)
	Dim BBlock, EBlock
	BBlock = instr(sTemplate, "<!--Begin" & sName & "-->")
	EBlock = instr(sTemplate, "<!--End" & sName & "-->")
	if not (BBlock = 0 or EBlock = 0) then
		replaceBlock = left(sTemplate, BBlock - 1) & "{" & sName & "}" & right(sTemplate, len(sTemplate) - EBlock - len("<!--End" & sName & "-->") + 1)
	else
		replaceBlock = sTemplate
	end if
end function

Function NextDBlockName(sTemplateName)
	if isObject(DBlocks) then
		dim BTag, ETag, sName, sTemplate
		sTemplate = DBlocks(sTemplateName)
		BTag = instr(sTemplate, "<!--Begin")
		if BTag > 0 then
			ETag = instr(BTag, sTemplate, "-->")
			sName = Mid(sTemplate, BTag + 9, ETag - (BTag + 9))
			if instr(sTemplate, "<!--End" & sName & "-->") > 0 then
				NextDBlockName = sName
			else
				NextDBlockName = ""
			end if
		else
			NextDBlockName = ""
		end if
	else
		NextDBlockName = ""
	end if
End function

'Print all Dynamic Variables
Function PrintAll()
	if isObject(ParsedBlocks) and isObject(DBlocks) then
		dim aPBlocks
		dim aDBlocks
		dim i, res

		aPBlocks = ParsedBlocks.Items
		aDBlocks = DBlocks.Items
		res = "<table border=1>"
		for i = 1 to UBound(aDBlocks)
			res = res & "<tr><td><pre>" & ToHTML(aDBlocks(i)) & "</pre></td></tr>"
		next
		for i = 1 to UBound(aPBlocks)
			res = res & "<tr><td><pre>" & ToHTML(aPBlocks(i)) & "</pre></td></tr>"
		next
		res = res & "</table>"
		PrintAll = res
	else
		PrintAll = ""
	end if
End Function
%>