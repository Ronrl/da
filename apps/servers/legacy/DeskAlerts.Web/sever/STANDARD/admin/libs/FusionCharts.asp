<%
'Page: FusionCharts.asp
'Author: InfoSoft Global (P) Ltd.

'This page contains functions that can be used to render FusionCharts.

'encodeDataURL function encodes the dataURL before it's served to FusionCharts.
'If you've parameters in your dataURL, you necessarily need to encode it.
'Param: strDataURL - dataURL to be fed to chart
'Param: addNoCacheStr - Whether to add aditional string to URL to disable caching of data
Function encodeDataURL(strDataURL, addNoCacheStr)
	'Add the no-cache string if required
	if addNoCacheStr=true then
		'We add ?FCCurrTime=xxyyzz
		'If the dataURL already contains a ?, we add &FCCurrTime=xxyyzz
		'We replace : with _, as FusionCharts cannot handle : in URLs
		if Instr(strDataURL,"?")<>0 then
			strDataURL = strDataURL & "&FCCurrTime=" & Replace(Now(),":","_")
		else
			strDataURL = strDataURL & "?FCCurrTime=" & Replace(Now(),":","_")
		end if
	end if	
	'URL Encode it
	encodeDataURL = Server.URLEncode(strDataURL)
End Function

'renderChart renders the JavaScript + HTML code required to embed a chart.
'This function assumes that you've already included the FusionCharts JavaScript class
'in your page.

' chartSWF - SWF File Name (and Path) of the chart which you intend to plot
' strURL - If you intend to use dataURL method for this chart, pass the URL as this parameter. Else, set it to "" (in case of dataXML method)
' strXML - If you intend to use dataXML method for this chart, pass the XML data as this parameter. Else, set it to "" (in case of dataURL method)
' chartId - Id for the chart, using which it will be recognized in the HTML page. Each chart on the page needs to have a unique Id.
' chartWidth - Intended width for the chart (in pixels)
' chartHeight - Intended height for the chart (in pixels)
Function renderChart(chartSWF, strURL, strXML, chartId, chartWidth, chartHeight)
	'First we create a new DIV for each chart. We specify the name of DIV as "chartId"Div.			
	'DIV names are case-sensitive.

renderChart = "<div id='" & chartId & "Div' align='center'>Chart.</div>"
renderChart = renderChart & "<script type=""text/javascript"">"
renderChart = renderChart & "var chart_" & chartId & " = new FusionCharts(""" & chartSWF & """, """ & chartId & """, """ & chartWidth & """, """ & chartHeight & """);"

		if strXML="" then 
			renderChart = renderChart & "chart_" & chartId & ".setDataURL(""" & strURL & """);"
		else 
			renderChart = renderChart & "chart_" & chartId & ".setDataXML(""" & Replace(strXML, """", "\""")  & """);"
		end if 
			renderChart = renderChart & "chart_" & chartId & ".render(""" & chartId & "Div"");"
			renderChart = renderChart & "</script>"
End Function

'renderChartHTML function renders the HTML code for the JavaScript. This
'method does NOT embed the chart using JavaScript class. Instead, it uses
'direct HTML embedding. So, if you see the charts on IE 6 (or above), you'll
'see the "Click to activate..." message on the chart.
' chartSWF - SWF File Name (and Path) of the chart which you intend to plot
' strURL - If you intend to use dataURL method for this chart, pass the URL as this parameter. Else, set it to "" (in case of dataXML method)
' strXML - If you intend to use dataXML method for this chart, pass the XML data as this parameter. Else, set it to "" (in case of dataURL method)
' chartId - Id for the chart, using which it will be recognized in the HTML page. Each chart on the page needs to have a unique Id.
' chartWidth - Intended width for the chart (in pixels)
' chartHeight - Intended height for the chart (in pixels)
Function renderChartHTML(chartSWF, strURL, strXML, chartId, chartWidth, chartHeight)
	'Generate the FlashVars string based on whether dataURL has been provided
	'or dataXML.
	Dim strFlashVars
	if strXML="" then
		'DataURL Mode
		strFlashVars = "&chartWidth=" & chartWidth & "&chartHeight=" & chartHeight & "&dataURL=" & strURL
	else
		'DataXML Mode
		strFlashVars = "&chartWidth=" & chartWidth & "&chartHeight=" & chartHeight & "&dataXML=" & strXML 		
	end if
	%>
	<!-- START Code Block for Chart <%=chartId%> -->
	<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0" width="<%=chartWidth%>" height="<%=chartHeight%>" id="<%=chartId%>">
		<param name="allowScriptAccess" value="always" />
		<param name="movie" value="<%=chartSWF%>"/>		
		<param name="FlashVars" value="<%=strFlashVars%>" />
		<param name="quality" value="high" />
		<embed src="<%=chartSWF%>" FlashVars="<%=strFlashVars%>" quality="high" width="<%=chartWidth%>" height="<%=chartHeight%>" name="<%=chartId%>" allowScriptAccess="always" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" />
	</object>
	<!-- END Code Block for Chart <%=chartId%> -->
	<%
End Function

'boolToNum function converts boolean values to numeric (1/0)
Function boolToNum(bVal)
	Dim intNum
	if bVal=true then
		intNum = 1
	else
		intNum = 0
	end if
	boolToNum = intNum
End Function
%>