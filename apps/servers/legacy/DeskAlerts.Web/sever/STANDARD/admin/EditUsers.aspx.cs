﻿using DeskAlerts.Server.Utils;
using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.ApplicationCore.Utilities;
using DeskAlerts.ApplicationCore.Repositories;
using DeskAlerts.ApplicationCore.Entities;
using Resources;
using DeskAlertsDotNet.Parameters;
using DeskAlertsDotNet.DataBase;

namespace DeskAlertsDotNet.Pages
{
    public partial class EditUsers : DeskAlertsBasePage
    {
        string id;

        string editingUserName;

        private IUserRepository _userRepository;

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            var configurationManager = new WebConfigConfigurationManager(Config.Configuration);
            _userRepository = new PpUserRepository(configurationManager);

            id = Request["user_id"];
            bool isNew = string.IsNullOrEmpty(id);

            if (!isNew)
            {
                editingUserName = dbMgr.GetDataByQuery("SELECT name FROM users WHERE id =" + id).First().GetString("name");
            }

            if (isNew)
            {
                editingUserName = "";
            }

            ad_label_tr.Visible = ad_value_tr.Visible = Config.Data.HasModule(Modules.AD);
            sms_label_tr.Visible = sms_value_tr.Visible = Config.Data.HasModule(Modules.SMS) || Config.Data.HasModule(Modules.TEXTTOCALL);
            email_label_tr.Visible = email_value_tr.Visible = Config.Data.HasModule(Modules.EMAIL);
            addUserButton.Click += addUserButton_Click;
            domainId.Items.Add(new ListItem( resources.LNG_REGISTERED, "1"));
            DataSet domainSet = dbMgr.GetDataByQuery("SELECT id, name FROM domains WHERE id > 1");

            foreach (DataRow row in domainSet)
            {
                domainId.Items.Add(new ListItem(row.GetString("name"), row.GetString("id")));
            }
            if (string.IsNullOrEmpty(id))
            {
                headerTitle.InnerText =  resources.LNG_ADD_USER;
                addUserButton.Text =  resources.LNG_ADD;
                changePasswordButton.Visible = false;
            }
            else
            {
                headerTitle.InnerText =  resources.LNG_EDIT_USER;
                addUserButton.Text =  resources.LNG_SAVE;
                password.Style.Add(HtmlTextWriterStyle.Display, "none");
                passwordLabel.Style.Add(HtmlTextWriterStyle.Display, "none");
                DataSet userSet = dbMgr.GetDataByQuery("SELECT u.name, u.display_name, u.email, u.mobile_phone, u.domain_id FROM users as u WHERE u.id = " + id + " AND u.role='U'");

                userName.Value = userSet.GetString(0, "name");
                displayName.Value = userSet.GetString(0, "display_name");
                domainId.Value = userSet.GetString(0, "domain_id");
                email.Value = userSet.GetString(0, "email");
                mobilePhone.Value = userSet.GetString(0, "mobile_phone");
            }
        }

        protected void addUserButton_Click(object sender, EventArgs e)
        {
            var domainId = string.IsNullOrEmpty(Request["domainId"]) ? "1" : Request["domainId"];
            var userNameInputValue = Request["userName"];
            bool isNew = string.IsNullOrEmpty(id);

            var usersWithSameNameTotal = dbMgr.GetScalarQuery<int>($@"
                SELECT COUNT(name) 
                FROM users 
                WHERE name = '{userNameInputValue.Replace("'", "''")}' 
                    AND role = 'U'"
            );
            var isNameAlreadyExist = usersWithSameNameTotal > 0;

            if (isNew
                && isNameAlreadyExist)
            {
                Response.ShowJavaScriptAlert( resources.LNG_USER_WITH_THIS_NAME_ALREADY_EXISTS);
                return;
            }

            if (!isNew
                && editingUserName != userNameInputValue
                && isNameAlreadyExist)
            {
                Response.ShowJavaScriptAlert( resources.LNG_USER_WITH_THIS_NAME_ALREADY_EXISTS);
                return;
            }

            bool isEmptyPasswordForUserInDb = (dbMgr.GetScalarByQuery<int>(
                                                   string.Format(
                                                       "select count(pass) from users where id='{0}' and domain_id=\'1\'", id)) == 0);
            if ((string.IsNullOrEmpty(Request["password"])) && (domainId == "1") && isEmptyPasswordForUserInDb)
            {
                Response.ShowJavaScriptAlert( resources.LNG_ENTER_PASSWORD_PLEASE);
                return;
            }

            bool isExistGroupName = string.IsNullOrEmpty(Request["userName"]);
            if (userNameInputValue.Length == 0)
            {
                Response.ShowJavaScriptAlert( resources.LNG_PLEASE_ENTER_USER_NAME);
                return;
            }

            if (string.IsNullOrEmpty(id))
            {
                var user = new User()
                {
                    Username = Request["userName"],
                    Role = DeskAlerts.ApplicationCore.Entities.User.UserRole,
                    DisplayName = Request["displayName"],
                    Email = Request["email"],
                    Phone = Request["mobilePhone"],
                    DomainId = Int32.Parse(domainId),
                    Password = HashingUtils.MD5(Request["password"])
                };

                _userRepository.Create(user);
            }
            else
            {
                int userId = Convert.ToInt32(id);
                var user = _userRepository.GetById(userId);
                user.Role = DeskAlerts.ApplicationCore.Entities.User.UserRole;
                user.Username = Request["userName"];
                user.DisplayName = Request["displayName"];
                user.Email = Request["email"];
                user.Phone = Request["mobilePhone"];
                user.DomainId = Int32.Parse(domainId);
                FillInPassword(domainId, user);
                _userRepository.Update(user);
            }

            Response.Redirect(Request["return_page"], true);
        }

        private void FillInPassword(string domainId, User user)
        {
            if (!string.IsNullOrEmpty(Request["password"]) && (domainId == "1"))
            {
                user.Password = HashingUtils.MD5(Request["password"]);
            }
        }
    }
}