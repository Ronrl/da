﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Parameters;
using Resources;


namespace DeskAlertsDotNet.Pages
{
    public partial class EditColorCodes : DeskAlertsBasePage
    {
        //use curUser to access to current user of DA control panel      
        //use dbMgr to access to database

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            headerTitle.Text = resources.LNG_COLOR_CODES;

            workHeader.InnerText = string.IsNullOrEmpty(Request["edit"])
                ? resources.LNG_ADD_COLOR_CODE
                : resources.LNG_EDIT_COLOR_CODE;


            if (!string.IsNullOrEmpty(Request["id"]))
            {
                ccId.Value = Request["id"];
               var colorCodeRow = dbMgr.GetDataByQuery("SELECT * FROM color_codes WHERE id = " + Request["id"].Quotes());

                if (!colorCodeRow.IsEmpty)
                {
                    colorPick.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#" + colorCodeRow.GetString(0, "color"));
                    ccName.Value = colorCodeRow.GetString(0, "name");
                    color.Value = "#" + colorCodeRow.GetString(0, "color");
                }
               
            }                
            //Add your main code here
        }


        [WebMethod]
        public static bool AddColorCode(string name, string color, string ccId)
        {
            using (var dbManager = new DBManager())
            {
                color = color.Replace("#", "").ToLowerInvariant();

                Dictionary<string, object> colorCodeData = new Dictionary<string, object>()
                {
                    {"name", name},
                    {"color", color}
                };


                if (ccId.Length == 0)
                {
                    dbManager.Insert("color_codes", colorCodeData);
                }                 
                else
                {
                    Dictionary<string, object> conditionObject = new Dictionary<string, object>();
                    conditionObject.Add("id", ccId);
                    dbManager.Update("color_codes", colorCodeData, conditionObject);
                }

                return true;
            }
        }
    }
}