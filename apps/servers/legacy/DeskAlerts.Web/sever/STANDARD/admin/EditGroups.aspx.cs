﻿using System;
using System.Web.Services;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Parameters;
using Resources;

namespace DeskAlertsDotNet.Pages
{
    public partial class EditGroups : DeskAlertsBasePage
    {
        string editingGroupId;
        string editingGroupName;
        protected override void OnLoad(EventArgs e)
        {
            saveButton.Click += SaveButton_Click;
            saveButton.Text = resources.LNG_NEXT;

            editingGroupId = Request["group_id"];
            editingGroupName = Request["groupNameInputValue"];

            if (!string.IsNullOrEmpty(editingGroupId))
            {
                workHeader.InnerText = resources.LNG_EDIT_GROUP;
                DataSet groupSet = dbMgr.GetDataByQuery("SELECT name, domain_id FROM groups WHERE id = " + editingGroupId);
                groupNameInputValue.Value = oldGroupName.Value = groupSet.GetString(0, "name");
                domainId.Value = groupSet.GetString(0, "domain_id");
            }
            else
            {
                workHeader.InnerText = resources.LNG_ADD_GROUP;
                domainId.Value = "1";
            }
        }

        protected void SaveButton_Click(object sender, EventArgs e)
        {
            bool groupWithThisNameAlreadyExist = dbMgr.GetScalarQuery<int>($"SELECT Count(name) FROM Groups WHERE name = '{editingGroupName.Replace("'", "''")}'") > 0;
            
            if (editingGroupName.Length == 0)
            {
                Response.ShowJavaScriptAlert(resources.LNG_PLEASE_ENTER_GROUP_NAME);
                return;
            }

            if (!string.IsNullOrEmpty(oldGroupName.Value))
            {
                if (editingGroupName != oldGroupName.Value)
                {
                    string updateGroupNameQuery = $"UPDATE groups SET [name] = N'{Request["groupNameInputValue"]}' WHERE id = {editingGroupId}";
                    dbMgr.ExecuteQuery(updateGroupNameQuery);
                }
            }
            else
            {
                if (groupWithThisNameAlreadyExist)
                {
                    Response.ShowJavaScriptAlert(resources.LNG_GROUP_WITH_THIS_NAME_ALREADY_EXISTS);
                    return;
                }
                else
                {
                    editingGroupId = dbMgr.GetScalarByQuery("INSERT INTO groups (name, domain_id) OUTPUT INSERTED.id VALUES (N'" + editingGroupName.EscapeQuotes() + "', " + domainId.Value + ")").ToString();
                }
            }

            Response.Redirect("EditGroupMembers.aspx?group_id=" + editingGroupId + "&return_page=" + Request["return_page"], true);
        }

        [WebMethod]
        public static void AddGroup(string name, string ids)
        {
            DBManager dbMgr = new DBManager();
            int id =
                dbMgr.GetScalarQuery<int>("INSERT INTO groups (name, domain_id) OUTPUT INSERTED.id VALUES (N'" + name +
                                            "', 1)");

            string[] idsArray = ids.Split(',');

            foreach (var userId in idsArray)
            {
                dbMgr.ExecuteQuery(string.Format("INSERT INTO users_groups (group_id, user_id) VALUES ({0}, {1})", id, userId));
            }

        }
    }
}