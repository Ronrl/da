﻿<!-- #include file="config.inc" -->
<!-- #include file="demo.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%
if (Request("success")<>"") then
	Response.Write Request("success")
	Response.End
end if

check_session()

uid = Session("uid")

function CopyImages(FromCode, ToCode)
	'create folder
	dim fs,f
	set fs=Server.CreateObject("Scripting.FileSystemObject")
	set f=fs.CreateFolder(alerts_dir & "admin\images\langs\"&ToCode)
	set f=nothing
	set fs=nothing
	
	'copy files
	set fs=Server.CreateObject("Scripting.FileSystemObject")
	fs.CopyFile alerts_dir & "admin\images\langs\"&FromCode&"\*.*",alerts_dir & "admin\images\langs\"&ToCode
	set fs=nothing	
	
end function

function RenameImages(FromCode, ToCode)
	'move folder
	dim fs
	set fs=Server.CreateObject("Scripting.FileSystemObject")
	fs.MoveFolder alerts_dir & "admin\images\langs\"&FromCode ,alerts_dir & "admin\images\langs\"&ToCode
	set fs=nothing
	'delete old folder?
end function

if (uid <> "") then

'update group name in database
old_code = request("old_code")

if(Request("sub1") = "Lang") then
	    name=replace(Request("name"),"'","''")
		code=replace(Request("code"),"'","''")
		old_code=replace(Request("old_code"),"'","''")
		if(old_code<>"") then
			if(old_code<>code)then
				SQL = "ALTER TABLE [languages] ADD ["&code&"] [nvarchar] (max)"
				Conn.Execute(SQL)			
				SQL = "ALTER TABLE [languages] DROP COLUMN ["&old_code&"]"
				Conn.Execute(SQL)	
				SQL = "UPDATE settings SET val=N'"&code&"' WHERE name='ConfDefaultLanguage'"
				Conn.Execute(SQL)	
				RenameImages old_code, code
			end if
			SQL = "UPDATE languages_list SET name=N'"&name&"', code=N'"&code&"' WHERE code='"&old_code&"'"
			Conn.Execute(SQL)				    
			For Each item In request.form
				'use prefix Conf to identify settings
				if(InStr(item, "LNG_")>0) then
					SQL = "UPDATE languages SET "&code&"=N'" & replace(request(item),"'","''") & "' WHERE name='" & item & "'"
					Conn.Execute(SQL)
				end if
			Next
			Response.Redirect "settings_system_configuration.asp"			
		else
			Set rsCheck = Conn.Execute("SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE column_name ='"&code&"' AND table_name = 'languages'")
			if(rsCheck.EOF) then
				SQL = "ALTER TABLE [languages] ADD ["&code&"] [nvarchar] (max)"
				Conn.Execute(SQL)			
				SQL = "INSERT INTO languages_list (name, code) VALUES(N'"&name&"', N'"&code&"' )"
				Conn.Execute(SQL)							
				For Each item In request.form
					'use prefix Conf to identify settings
					if(InStr(item, "LNG_")>0) then
						SQL = "UPDATE languages SET "&code&"=N'" & replace(request(item),"'","''") & "' WHERE name='" & item & "'"
						Conn.Execute(SQL)
					end if
				Next
				CopyImages "EN", code
				Response.Redirect "settings_system_configuration.asp"
			end if
		end if
		error_descr = "'"&name&"' " & LNG_LANGUAGE_ALREADY_EXISTS & "."
end if

'show editing form

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>DeskAlerts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/style9.css" rel="stylesheet" type="text/css">
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script language="javascript" type="text/javascript" src="jscripts/json2.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script language="javascript" type="text/javascript">
function showDialog(html)
{
	$("#dialog-modal > p").html(html)
	$("#dialog-modal").dialog({
		modal: true,
		resizable: false,
		draggable: false,
		width:'auto',
		buttons : {
			"<%=LNG_CLOSE%>" : function(){$(this).dialog("close");}
		}
	});
}

$(document).ready(function() {
	$(".save_button").button();
	$(".add_button").button();
	$(".cancel_button").button();
	$(".send_translations_button").button().click(function(){
		var translations = {};
		translations.langName = $("#l_name").val();
		translations.langCode = $("#l_code").val();
		translations.data = [];
		var inputs = $(".data_table input");

		for (var i = 0; i < inputs.length; i++ )
		{
			var origValue = $($(inputs[i]).closest("tr").find("td")[1]).text();
			var value = $(inputs[i]).val();
			if (value != origValue)
			{
				translations.data.push({
					name : $(inputs[i]).attr("name"),
					value : value
				});
			}
			
		}
			
		if (translations.data.length > 0)
		{
			$("#translations_frame").remove();
			$("#translations_form").remove();
			
			var translationsFrame = $("<iframe id='translations_frame' style='display:none' name='translations_frame'/>").appendTo(document.body);
			var translationsForm = $("<form method='post' id='translations_form' style='display:none' action='http://www.alert-software.com/control_panel/translations/' target='translations_frame'/>").appendTo(document.body);
			var translationsInput = $("<input name='translations' type='hidden'/>").val(JSON.stringify(translations)).appendTo(translationsForm);
			var translationsUrlInput = $("<input name='local_url' type='hidden'/>").val(window.location).appendTo(translationsForm);
			
			function showError()
			{
				var commonDiv = $("<div style='width:500px; height: 300px;'/>");
				var textArea = $("<textarea style='width:100%; height:250px;'/>").val(JSON.stringify(translations));
				$(commonDiv).append(textArea);
				$(commonDiv).append($("<div style='margin-top:20px'>"+"<%=jsEncode(LNG_TRANSLATIONS_NOT_SENT)%>"+"</div>"));
				
				showDialog(commonDiv);
			}
			
			$(translationsFrame).load(function(){
				window.parent.hideLoader();
				try{
					if ($(translationsFrame).contents().text() == "1")
					{
						showDialog("<%=LNG_TRANSLATIONS_SENT_SUCCESSFULY%>");
					}
					else
					{
						showError();
					}
				}
				catch(e)
				{
					showError();
				}
				$(translationsFrame).remove();
				$(translationsForm).remove();
			})
			
			window.parent.showLoader();
			$(translationsForm).submit();
			
		}
		else //do nothing
		{
			showDialog("<%=LNG_TRANSLATIONS_SENT_SUCCESSFULY%>");
		}
	});
});

</script>
<script language="javascript">
function my_sub()
{
	var letters = /^[A-Za-z]+$/;
	//if(checkValues()==false) return false;
	if(document.getElementById('l_name').value==""){
		alert("<%=LNG_PLEASE_ENTER_LANGUAGE_NAME %>");
		return false;
	}
	else{
		if(document.getElementById('l_code').value==""){
			alert("<%=LNG_PLEASE_ENTER_LANGUAGE_CODE %>");
			return false;
		}
		else{
			//var onlyLetters = /^[a-zA-Z]$/.test(document.getElementById('l_code').value);
			if(!document.getElementById('l_code').value.match(letters)){
				alert("The code should contain only english letters");
				return false;
			}		
			else{
				if(document.getElementById('l_code').value.length>10){
					alert("The code should less then 10 symbols");
					return false;
				}
				else{
					return true;
				}
			}
		}
	}
}
</script>

</head>

<%
val="Add"
lang=Replace(Request("lang"),"'", "''")
if(lang <> "") then
	Set rs = Conn.Execute("SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE column_name = '"&lang&"' AND table_name = 'languages'")
	if(Not rs.EOF) then
		Set rsLang = Conn.Execute("SELECT name FROM languages_list WHERE code = '"&lang&"'")
		if(Not rsLang.EOF) then
			langName = rsLang("name")
			val="Save"
		end if
	end if
end if
%>
<body style="margin:0px" class="body">

<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	<tr>
	<td>
	<table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
		<tr>
		<td width=100% height=31 class="main_table_title"><a href="languages_group.asp" class="header_title"><%=LNG_ADD_EDIT_LANGUAGE %></a></td>
		</tr>
		<tr>
		<td class="main_table_body" height="100%">
		<div style="margin:10px;">

<form method="post" action="">
<input type="hidden" name="sub1" value="Lang">
<font color="red"><%=error_descr %></font>
<table cellpadding=4 cellspacing=4 border=0 id="ip_group_table">
<tr><td><label for="l_name"><%=LNG_LANGUAGE_NAME %>:</label></td><td><input name="name" id="l_name" type="text" value="<%=HtmlEncode(langName) %>"></td></tr>
<tr><td><label for="l_code"><%=LNG_LANGUAGE_CODE %>:</label></td><td>
<% if lang <> "EN" then %>
	<input type="text" id="l_code" name="code" value="<%=HtmlEncode(lang) %>"/>
<% else %>
	<input type="text" id="l_code" value="<%=HtmlEncode(lang) %>" disabled="disabled"/>
	<input type="hidden" name="code" value="<%=HtmlEncode(lang) %>"/>
<%end if %>
</td></tr>
</table>
<table style="border:0px; width:100%">
	<tr>
		<td style="text-align:left; vertical-align:center">
		<% if DEMO <> 1 then %>
			<a class="send_translations_button"><%=LNG_SEND_TRANSLATIONS %></a><img style="margin-left:5px; width:7px; height:9px" title="<%=HtmlEncode(LNG_SEND_TRANSLATIONS_TITLE)%>" alt="<%=HtmlEncode(LNG_SEND_TRANSLATIONS_TITLE)%>" src="images/help.png"></img>
		<% end if %>
		</td>
		<td style="text-align:right;">
			<a class="cancel_button" onclick="javascript:history.go(-1); return false;"><%=LNG_CANCEL %></a>
<%
if(val="Add") then
%>
			<a class="add_button" onclick="if(my_sub()){document.forms[0].submit()}"><%=LNG_ADD %></a>
<%
else
%>
			<a class="save_button" onclick="if(my_sub()){document.forms[0].submit()}"><%=LNG_SAVE %></a>
<%
end if
%>
		</td>
	</tr>
</table>
<table width="100%" height="100%" style="margin-top:10px; margin-bottom:10px" cellspacing="0" cellpadding="3" class="data_table">
<tr class="data_table_title">
<td><%=LNG_VALUE %></td><td><%=LNG_CURRENT_ENGLISH_TEXT %></td></tr>
<%

if(val="Add") then
	Set rsFields = Conn.Execute("SELECT name, EN FROM languages")
else
	Set rsFields = Conn.Execute("SELECT name, EN, "&lang&" FROM languages")
end if
do while not rsFields.EOF
	fname = rsFields("name")
	engText = rsFields("EN")
	if(val="Add") then
		fval = engText
	else
		fval = rsFields(lang)
	end if
	%>
	<tr><td><input type="text" name="<%=fname%>" size="40" value="<%=HtmlEncode(fval) %>" /></td><td><%=HtmlEncode(engText)%></td>
	<%
	rsFields.moveNext
loop

%>

</table>

<table style="border:0px; width:100%">
	<tr>
		<td style="text-align:left; vertical-align:center">
		<% if DEMO <> 1 then %>
			<a class="send_translations_button"><%=LNG_SEND_TRANSLATIONS %></a><img style="margin-left:5px; width:7px; height:9px" title="<%=HtmlEncode(LNG_SEND_TRANSLATIONS_TITLE)%>" alt="<%=HtmlEncode(LNG_SEND_TRANSLATIONS_TITLE)%>" src="images/help.png"></img>
		<% end if %>
		</td>
		<td style="text-align:right;">
			<a class="cancel_button" onclick="javascript:history.go(-1); return false;"><%=LNG_CANCEL %></a>
<%
if(val="Add") then
%>
			<a class="add_button" onclick="if(my_sub()){document.forms[0].submit()}"><%=LNG_ADD %></a>
<%
else
%>
			<a class="save_button" onclick="if(my_sub()){document.forms[0].submit()}"><%=LNG_SAVE %></a>
<%
end if
%>
		</td>
	</tr>
</table>
<%
if(val="Save")  then
	Response.Write "<input type='hidden' name='edit' value='1'/>"
	Response.Write "<input type='hidden' name='old_code' value='" & lang & "'/>"
end if
	
%>
<br>
</form>

		 <img src="images/inv.gif" alt="" width="1" height="100%" border="0">
		</div>
		</td>
		</tr>
	</table>
	</td>
	</tr>
</table>

<%

else

  Response.Redirect "index.asp"

end if
%>
<div id="dialog-modal" title="" style='display:none'>
	<p></p>
</div>
</body>
</html>
<!-- #include file="db_conn_close.asp" -->