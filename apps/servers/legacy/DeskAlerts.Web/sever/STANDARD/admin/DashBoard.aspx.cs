﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using DeskAlertsDotNet.Managers;
using DeskAlertsDotNet.Models;
using DeskAlertsDotNet.Models.Permission;
using DeskAlertsDotNet.Models.WidgetExtentions;

namespace DeskAlertsDotNet.Pages
{
    public partial class DashBoard : DeskAlertsBasePage
    {
        public string widgetsList = "[]";

        protected static bool IsStatistic { get; private set; }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            IsStatistic = !string.IsNullOrEmpty(Request["stat"]);
            widgetsList = FilterWidgetsDependsOnUserPolicy(curUser.Widgets).ToJsonArray(IsStatistic ? WidgetPage.STATISTIC : WidgetPage.DASHBOARD);
        }

        [WebMethod]
        public static string GetWidgetsJson()
        {
            var curUser = UserManager.Default.CurrentUser;
            return curUser.Widgets.ToJsonArray(IsStatistic ? WidgetPage.STATISTIC : WidgetPage.DASHBOARD);
        }

        private List<Widget> FilterWidgetsDependsOnUserPolicy(List<Widget> widgets)
        {
            if (!curUser.HasPrivilege && !curUser.Policy[Policies.ALERTS].CanCreate)
                widgets.RemoveAll(w => w.Title.ToLowerInvariant() == "create alert");

            return widgets;
        }
    }
}