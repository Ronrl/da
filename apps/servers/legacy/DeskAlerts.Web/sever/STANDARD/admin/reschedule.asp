﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="recurrence_form.asp" -->
<!-- #include file="recurrence_process.asp" -->
<!-- #include file="lang.asp" -->
<!-- #include file="functions_inc.asp" -->

<%

check_session()

%>

<%

  uid = Session("uid")
'default recurrence values----
pattern = "o" 'd,w,m,y
dayly_selector = "dayly_1"
monthly_selector = "monthly_1"
yearly_selector = "yearly_1"

number_days = 1 '(every 3 days), 0=every weekday
number_weeks = 1 '(every 5 week)
week_days = 0 ' mon, wed, fri
month_day = 1 '27e chislo)
number_month = 1 '(every 4 month)
weekday_place = 1 '(1,2,3,4), 0=last
weekday_day = 1 'mon, tue, wed, thu, fri, sat, sun 
month_val = 1'(1-12)

end_type = "no_date" '(no_date, end_after, end_by)
occurences = 1 '(number)
'-------------------

%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Reschedule alert</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/style9.css" rel="stylesheet" type="text/css">
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script language="javascript" type="text/javascript" src="functions.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$(".save_button").button();
		$(".close_button").button();
		$(".save_and_next_button").button({
			icons: {
				secondary: "ui-icon-carat-1-e"
			}
		});
	<% if Request("ro") = 1 then %>
		$("input,select").prop("disabled", true);
	<% end if %>
	});
</script>
<link href="css/form_style.css" rel="stylesheet" type="text/css" />
<!--[if IE]>
<style type="text/css" media="all">
@import "css/ie.css";
</style>    
<![endif]-->
<style>
	html, body
	{
		margin:0px;
		border:0px;
	}
</style>
</head>
<body style="margin:0" class="body">
<% if Request("wh") <> "1" then %>
<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	<tr>
	<td valign="top">
	<table width="100%" height="100%" cellspacing="0" cellpadding="0" class="">
		<tr>

		</tr>
		<tr>
		<td style="padding:5px; width:100%" height="100%"   valign="top" class="main_table_body">
<% end if %>
<%
if (uid <> "")  then
	id=Request("id")
	
	if id = "" then
	
	    campaign_id = Request("campaign_id")
	    if campaign_id <> "" then
	        Set ids = Conn.Execute("SELECT id FROM alerts WHERE campaign_id = " & campaign_id)
	    
	     id = ""
	     do while not ids.EOF
	        id = id & ids("id") & ","
	        ids.movenext
	     loop
	        
	        id = left(id,len(id) - 1)
	        'response.write "ID = " & id
	     end if
	end if
'save alert and go to "send" page
	if(Request("sub1")<>"") then
	
	    'Response.Write "Sub1"
		resend=request("resend")
		'response.write "resend=" & resend
		'response.end
	
		countdown_enable = Clng(Request("countdown_enable"))
		if (countdown_enable = 1) then
			Set countdown_array = Request("countdowns")
			recurrence = 1
		end if
		
		if Request("pattern") = "o" then
			recurrence = 0
			schedule = 1
			
			from_date=Request("from_date")
			to_date=Request("to_date")
			
		else
			recurrence = 1
			schedule = 0

			from_date=Request("from_date_rec")
			to_date=Request("to_date_rec")

			from_date = from_date & " " & Request("from_time")

			if(to_date<>"") then 
				to_date = to_date & " " & Request("to_time")
			else
				to_date = Request("to_time")
			end if
		end if
			if (countdown_enable = 1) then recurrence = 1 end if

           
			
			Conn.Execute("UPDATE alerts SET schedule='"&schedule&"', recurrence='"&recurrence&"', from_date='"&from_date&"', to_date='"&to_date&"' WHERE id="&Replace(id, ",", "OR id = "))
			if(resend="1") then
				Conn.Execute("DELETE FROM alerts_received WHERE alert_id="&Replace(id, ",", "OR alert_id = "))
				Conn.Execute("UPDATE alerts_received" &_
							"	SET occurrence = r.cnt" &_
							"	FROM" &_
							"	(" &_
							"		SELECT alert_id, user_id, clsid, COUNT(1) AS cnt" &_
							"		FROM alerts_received" &_
							" 		WHERE alert_id = " & Replace(id, ",", "OR alert_id = ") &_
							" 		GROUP BY alert_id, user_id, clsid" &_
							" 	) r" &_
							"	WHERE alerts_received.alert_id = r.alert_id" &_
							"		AND alerts_received.user_id = r.user_id" &_
							"		AND alerts_received.clsid = r.clsid")
			end if

			recurrenceProcess id,countdown_array

          ids = Split(id, ",")
            MemCacheUpdate "Update", id
            for each x in ids
               'MEMCACHE
	           ' Set cacheMgr = CreateObject("DeskAlerts.CacheManager")
             ' cacheMgr.Update(x, now())
                 
			next    

			if Request("return_page") <> "" then
				Response.Redirect Request("return_page")
			elseif Request("san") = "1" then
				Response.Redirect "alert_users.asp?id="&id
			end if
			
			response.write "<center><b>Alert successfully rescheduled.</b> </center>"
			'<A class='close_button' href='#' onclick='javascript: window.close();'> - old code of close button
			response.end
	end if



'save alert and continue editing

start_date = now_db
end_date=""

myhour1 = hour(start_date)
myhalf1="am"
if(myhour1 = 0) then
	myhour1 = 12
elseif(myhour1 > 11) then
	if(myhour1 > 12) then myhour1 = myhour1 - 12 end if
	myhalf1="pm"
end if
myminute1 = minute(start_date)

if ConfEndDateByDefault then
	end_date=DateAdd("n", ConfEndDateByDefault, now_db)

	myhour2 = hour(end_date)
	myhalf2="am"
	if(myhour2 = 0) then
		myhour2 = 12
	elseif(myhour2 > 11) then
		if(myhour2 > 12) then myhour2 = myhour2 - 12 end if
		myhalf2="pm"
	end if
	myminute2 = minute(end_date)
end if

if ConfScheduleByDefault then
	recurrence = "checked"
else
	recurrence = ""
end if
toolbarmode = "checked"
deskalertsmode = "checked"

userName=""
Set RSUser = Conn.Execute("SELECT name FROM users WHERE id="&uid)
if(Not RSUser.EOF) then
	userName=RSUser("name")
end if
email_sender = userName
desktop="checked"


		SQL = "SELECT alert_text, title, recurrence, schedule, urgent, email, desktop, sms, email_sender, ticker, aknown, from_date, to_date, template_id, toolbarmode, deskalertsmode FROM alerts WHERE id=" & Replace(id, ",", "OR id = ")
		Set rs = Conn.Execute(SQL)
		do while Not rs.EOF 
		if(Not IsNull(rs("from_date"))) then 
			myhour1 = hour(rs("from_date"))
			myhalf1="am"
			if(myhour1 = 0) then
				myhour1 = 12
			elseif(myhour1 > 11) then
				if(myhour1 > 12) then myhour1 = myhour1 - 12 end if
				myhalf1="pm"
			end if
			myminute1 = minute(rs("from_date"))

			start_date = rs("from_date")
		end if
		if(Not IsNull(rs("to_date"))) then 
			myhour2 = hour(rs("to_date"))
			myhalf2="am"
			if(myhour2 = 0) then
				myhour2 = 12
			elseif(myhour2 > 11) then
				if(myhour2 > 12) then myhour2 = myhour2 - 12 end if
				myhalf2="pm"
			end if
			myminute2 = minute(rs("to_date"))

			'if(year(rs("to_date")) & "-" & month(rs("to_date")) & "-" & day(rs("to_date")) <>"1900-1-1") then
				end_date = rs("to_date")
			'end if
		end if

		recurrence = ""

		if(Not IsNull (rs("schedule"))) then
			if(rs("schedule")=1) then
				recurrence = "checked"
				pattern = "o"
			end if
		end if

		if(Not IsNull (rs("recurrence"))) then
			if(rs("recurrence")="1") then
				recurrence = "checked"
			end if
		end if
		
		rs.movenext

	loop


'start_date = GetENGDate(start_date)
'end_date = GetENGDate(end_date)

%>
<form name="my_form" method="post" action="reschedule.asp">
	<input type="hidden" name="sub1" value="sub1">
<%
if(id <> "") then
	Response.Write "<input type='hidden' name='id' value='" & id & "'/>"
end if
	Response.Write "<input type='hidden' name='san' value='" & Request("san") & "'/>"
	Response.Write "<input type='hidden' name='return_page' value='" & Request("return_page")  & "'/>"
%>
<div id="recurrence_div">
<%

recurrenceLoadData(id)

recurrenceForm()

%>
</div>
<script language="javascript">
	patternChange('<% response.write pattern %>');
</script>

<br/>
<table width="100%">
	<tr>
		<td>
			<% if Request("ro") <> 1 then %>
				<input type="checkbox" id="resend" name="resend" value="1" > <label for="resend">resend alert to users who had already received this alert</label></td><td align="right"> 
			<% end if %>
			<% if Request("san") = "1" then %>
				<a class="save_and_next_button" onclick="if (check_valid_dates_rec()) {document.my_form.submit()}"><%=LNG_SAVE_AND_NEXT %></a>
			<% elseif Request("ro") = 1 then %>
				<a class="close_button" onclick="window.close()"><%="" %></a>
			<% else %>
				<a class="save_button" onclick="if (check_valid_dates_rec()) {document.my_form.submit()}"><%=LNG_SAVE %></a>
			<% end if %>
		</td>
	</tr>
</table>
</form>
<%
else
	Response.write "You are not authorised to view this page!"
end if

%>
<!-- #include file="db_conn_close.asp" -->
<% if Request("wh") <> "1" then %>
		</td>
	</tr>
</table>
<% end if %>
</body>
</html>