<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%
check_session()
%>
<!-- #include file="Common.asp" -->
<!-- #include file="header.asp" -->
<!-- #include file="footer.asp" -->
<!-- #include file="libs/FusionCharts.asp" -->
<%
'On Error Resume Next
' index CustomIncludes End
'-------------------------------

'===============================
' Save Page and File Name available into variables
'-------------------------------
Dim sFileName
Dim sTemplateFileName
Dim sPaginateFileName
Dim intSessionUserId

sFileName = "statistics_da_users_view_details.asp"
if Request("widget")=1 then
	sTemplateFileName = "statistics_da_users_view_details_widget.html"
else
	sTemplateFileName = "statistics_da_users_view_details.html"
end if
sPaginateFileName = "paginate.html"
'===============================
  

intSessionUserId = Session("uid")
uid=intSessionUserId


if (intSessionUserId <> "") then


' index Show begin

'===============================
' Display page
'-------------------------------
' Load HTML template for this page
'-------------------------------
LoadTemplate alerts_dir & "\" & templateFolder & "\" & sTemplateFileName, "main"

'-------------------------------
' Load HTML template of Header and Footer
'-------------------------------
LoadTemplate sHeaderFileName, "Header"
LoadTemplate sFooterFileName, "Footer"
'-------------------------------
if(AD=3) then
	LoadTemplate sOU_menuFileName, "ou_menu"
end if

SetVar "LNG_GROUPS", LNG_GROUPS
SetVar "LNG_OUS", LNG_OUS
SetVar "LNG_TOTAL", LNG_TOTAL
SetVar "LNG_EXPORT_TO_CSV", LNG_EXPORT_TO_CSV
SetVar "default_lng", default_lng

PageName(LNG_USER_DETAILS)
'editor ---
	Set RS = Conn.Execute("SELECT id FROM users WHERE id="&uid&" AND role='U'")
	if(Not RS.EOF) then
		gid = 1
		Set rs_policy = Conn.Execute("SELECT type FROM policy INNER JOIN policy_editor ON policy.id=policy_editor.policy_id WHERE editor_id=" & uid)
		if(Not rs_policy.EOF) then
			if(rs_policy("type")="A") then
				gid = 0
			end if
		end if
	else
		gid = 0
	end if
	RS.Close
reg_date=strUserDate
'--------


	 user_id=Clng(request("id"))
    from_date=request("from_date")
	to_date=request("to_date")

	Set RS = Conn.Execute("SELECT name, display_name, reg_date FROM users WHERE id =" & user_id)
	if(Not RS.EOF) then
	

	strUserName=RS("name")
	strUserDate=RS("reg_date")

	mysent=0
	Set RS7 = Conn.Execute("SELECT COUNT(DISTINCT alert_id) as mycnt FROM [alerts_sent_stat] WHERE [date] >= '" & from_date & "'  AND alert_id NOT IN (SELECT alert_id FROM instant_messages) AND [date] <= '" & to_date & "' AND user_id="& user_id)
	if(not RS7.EOF) then
		mysent=RS7("mycnt")
	end if

	'broad
	Set RSBroad = Conn.Execute("SELECT COUNT(id) as mycnt FROM alerts WHERE create_date >= '"&strUserDate&"' AND create_date >= '" & from_date & "'  AND id NOT IN (SELECT alert_id FROM instant_messages) AND create_date <= '" & to_date & "' AND type2='B'")
	if(not RSBroad.EOF) then 
		broad_cnt=RSBroad("mycnt") 
	end if


'			do while not RS6.EOF

	'group
	  Set RSGroup = Conn.Execute("SELECT alerts_sent_group.group_id as group_id FROM alerts_sent_group INNER JOIN alerts ON alerts.id=alerts_sent_group.alert_id WHERE create_date >= '" & from_date & "'  AND alert_id NOT IN (SELECT alert_id FROM instant_messages) AND create_date <= '" & to_date & "'")
	  Do While not RSGroup.EOF
		 group_id=RSGroup("group_id")
		  Set RSUsers = Conn.Execute("SELECT users.id FROM users INNER JOIN users_groups ON users.id=users_groups.user_id WHERE users_groups.group_id="&group_id&" AND users_groups.user_id="&user_id)
		  if(not RSUsers.EOF) then 
			group_cnt=group_cnt+1
		end if
	  RSGroup.MoveNext	
	  loop


	mygroups=0
        usersGroupsFlag=0
	Set RS6 = Conn.Execute("SELECT DISTINCT group_id FROM [users_groups] WHERE user_id="& user_id)
	do while not RS6.EOF
		if(usersGroupsFlag=0) then
			usersGroups = usersGroups & " group_id=" & RS6("group_id")
		else
			usersGroups = usersGroups & " OR " & " group_id=" & RS6("group_id")
		end if
		usersGroupsFlag=1
		mygroups=mygroups+1	
		RS6.MoveNext
	loop





'user_id=RS("id")
intReceivedAlertsNum=0
'response.write "<br>uname=" & name & "<br>"
'response.write "users_groups=" & usersGroups & "<br>"
broad_cnt=0
group_cnt=0
'---------------
'broadcast alerts
if(gid<>0) then
else

  broadAlertsId=""
  broadFlag=0
  Set RSBroad = Conn.Execute("SELECT id, create_date FROM alerts WHERE create_date >= '" & from_date & "'  AND id NOT IN (SELECT alert_id FROM instant_messages) AND create_date <= '" & to_date & "' AND create_date > '" & strUserDate & "' AND type2='B'")
  Do While not RSBroad.EOF
	broadAlertId=RSBroad("id")
	if(broadFlag=1) then
		broadAlertsId=broadAlertsId& " OR " & " alert_id=" & broadAlertId
	else
		broadAlertsId=broadAlertsId& " alert_id=" & broadAlertId
	end if
	broadFlag=1
	broad_cnt=broad_cnt+1
   RSBroad.MoveNext
  loop

  'received
if(broadAlertsId<>"") then
  Set RS7 = Conn.Execute("SELECT COUNT(DISTINCT alert_id) as mycnt FROM [alerts_received] WHERE (" & broadAlertsId & ") AND user_id=" & user_id)
    
  if(not RS7.EOF) then 
    broadRecCnt=RS7("mycnt") 
    intReceivedAlertsNum=intReceivedAlertsNum+broadRecCnt
  end if
end if

'response.write "broad_sent_cnt=" & broad_cnt & "<br>"
'response.write "broad_ids=" & broadAlertsId & "<br>"
'response.write "broad_rec_cnt=" & broadRecCnt & "<br>"

end if

group_ids=""
groupFlag=0

groupAlertsId=""
groupAlertsFlag=0

usersGroups=replace(usersGroups,"group_id", "alerts_sent_group.group_id")
if(usersGroups<>"") then
'if(gid<>0) then
'  Set RSGroup = Conn.Execute("SELECT DISTINCT alerts_sent_group.alert_id as id, create_date FROM alerts INNER JOIN alerts_sent_group ON alerts_sent_group.alert_id=alerts.id WHERE create_date >= '" & from_date & "' AND create_date <= '" & to_date & "' AND create_date > '"&reg_date&"' AND ("& usersGroups&") AND sender_id=" & uid)
'else
  Set RSGroup = Conn.Execute("SELECT DISTINCT alerts_sent_group.alert_id as id, create_date FROM alerts INNER JOIN alerts_sent_group ON alerts_sent_group.alert_id=alerts.id WHERE  create_date >= '" & from_date & "' AND id NOT IN (SELECT alert_id FROM instant_messages) AND create_date <= '" & to_date & "' AND create_date > '"&reg_date&"' AND ("& usersGroups&")")
'end if

  Do While not RSGroup.EOF
'	 alertDate=RSGroup("create_date")
	 groupAlertId=RSGroup("id")

	if(groupAlertsFlag=1) then
		groupAlertsId=groupAlertsId& " OR " & " alert_id=" & groupAlertId
	else
		groupAlertsId=groupAlertsId& " alert_id=" & groupAlertId
	end if
	groupAlertsFlag=1
	group_cnt=group_cnt+1

  RSGroup.MoveNext	
  loop

if(groupAlertsId<>"") then
  'received
  Set RS7 = Conn.Execute("SELECT COUNT(DISTINCT alert_id) as mycnt FROM [alerts_received] WHERE (" & groupAlertsId & ") AND user_id=" & user_id)
  if(not RS7.EOF) then 
    groupRecCnt=RS7("mycnt") 
    intReceivedAlertsNum=intReceivedAlertsNum+groupRecCnt
  end if
end if

'response.write "group_sent_cnt=" & group_cnt & "<br>"
'response.write "group_ids=" & groupAlertsId & "<br>"
'response.write "group_rec_cnt=" & groupRecCnt & "<br>"
end if

intSentAlertsNum=0
'if(gid<>0) then
'  Set RS7 = Conn.Execute("SELECT COUNT(DISTINCT alerts_sent_stat.alert_id) as mycnt FROM [alerts_sent_stat] INNER JOIN alerts ON alerts.id=alerts_sent_stat.alert_id WHERE [date] >= '" & from_date & "' AND [date] <= '" & to_date & "' AND sender_id="&uid&" AND alerts_sent_stat.user_id=" & user_id)
'else
  Set RS7 = Conn.Execute("SELECT COUNT(DISTINCT alerts_sent_stat.alert_id) as mycnt FROM [alerts_sent_stat] INNER JOIN alerts ON alerts.id=alerts_sent_stat.alert_id WHERE [date] >= '" & from_date & "' AND [date] <= '" & to_date & "' AND alert_id NOT IN (SELECT alert_id FROM instant_messages) AND alerts_sent_stat.user_id=" & user_id)
'end if

'if(gid<>0) then
'  Set RS8 = Conn.Execute("SELECT COUNT(DISTINCT alerts_sent_stat.alert_id) as mycnt FROM [alerts_sent_stat] INNER JOIN alerts ON alerts.id=alerts_sent_stat.alert_id INNER JOIN alerts_received ON alerts_sent_stat.alert_id=alerts_received.alert_id WHERE alerts_sent_stat.user_id=alerts_received.user_id AND alerts_sent_stat.date >= '" & from_date & "' AND alerts_sent_stat.date <= '" & to_date & "' AND sender_id="&uid&" AND alerts_sent_stat.user_id=" & user_id)
'else
  Set RS8 = Conn.Execute("SELECT COUNT(DISTINCT alerts_sent_stat.alert_id) as mycnt FROM [alerts_sent_stat] INNER JOIN alerts ON alerts.id=alerts_sent_stat.alert_id INNER JOIN alerts_received ON alerts_sent_stat.alert_id=alerts_received.alert_id WHERE alerts_sent_stat.user_id=alerts_received.user_id AND alerts_sent_stat.alert_id NOT IN (SELECT alert_id FROM instant_messages) AND alerts_sent_stat.date >= '" & from_date & "' AND alerts_sent_stat.date <= '" & to_date & "' AND alerts_sent_stat.user_id=" & user_id)
'end if


  if(not RS7.EOF) then	intSentAlertsNum=RS7("mycnt") end if
  if(not RS8.EOF) then	intReceivedAlertsNum=intReceivedAlertsNum+RS8("mycnt") end if

  intSentAlertsNum=intSentAlertsNum+broad_cnt+group_cnt

'response.write "personal_sent_cnt=" & RS7("mycnt") & "<br>"
'response.write "personal_rec_cnt=" & RS8("mycnt") & "<br>"

'---------------




	mysent=intSentAlertsNum
	myreceived=intReceivedAlertsNum

	myread=0
'if(gid<>0) then
'	Set RS7 = Conn.Execute("SELECT COUNT(DISTINCT alert_id) as mycnt FROM [alerts_read] INNER JOIN alerts ON alerts.id=alerts_read.alert_id WHERE create_date >= '" & from_date & "' AND create_date <= '" & to_date & "' AND user_id="& user_id & "AND sender_id="&uid)
'else
	Set RS7 = Conn.Execute("SELECT COUNT(DISTINCT alert_id) as mycnt FROM [alerts_read] INNER JOIN alerts ON alerts.id=alerts_read.alert_id WHERE create_date >= '" & from_date & "' AND create_date <= '" & to_date & "' AND user_id="& user_id)
'end if
	if(not RS7.EOF) then
		myread=RS7("mycnt")
	end if

	intOUsCnt=0
	Set RS7 = Conn.Execute("SELECT COUNT(DISTINCT Id_OU) as mycnt FROM [OU_User] WHERE Id_User="& user_id)
	if(not RS7.EOF) then
		intOUsCnt=RS7("mycnt")
	end if


	intReceivedAlertsNum=myreceived
	intAcknowNum=myread
	intSentAlertsNum=mysent
	intGroupsCnt=mygroups
	


        alertsVal1Color="3B6392"
	alertsVal2Color="A44441"
	alertsVal3Color="9ABA59"

        alertsVal1Name=ToHTML(LNG_RECEIVED)
	alertsVal2Name=ToHTML(LNG_ACKNOWLEDGED)
	alertsVal3Name=ToHTML(LNG_NOT_RECEIVED)

        alertsVal1Value=CStr(intReceivedAlertsNum)
	alertsVal2Value=CStr(intAcknowNum)
	alertsVal3Value=CStr(intSentAlertsNum-intReceivedAlertsNum)
	totalAlertsValue=intSentAlertsNum

	if(alertsVal1Value<>"0" OR alertsVal2Value<>"0" OR alertsVal3Value <> "0") then
	SetVar "showAlerts","true"
	alertsData = "["&alertsVal1Value&"],["&alertsVal2Value&"],["&alertsVal3Value&"]"
	alertsColors = "'#"&alertsVal1Color&"','#"&alertsVal2Color&"','#"&alertsVal3Color&"'"
	SetVar "alertsData",alertsData
	SetVar "alertsColors",alertsColors
	else
	SetVar "showAlerts","false"
	SetVar "alertsData",""
	SetVar "alertsColors",""
	end if
	
	if Len(RS("display_name")) > 0 then
		SetVar "strDisplayName", "<br/>(" & RS("display_name") & ")"
	end if
	SetVar "strUserName", strUserName
	SetVar "intUserId", user_id
	SetVar "intGroupsCnt", intGroupsCnt
	SetVar "intOUsCnt", intOUsCnt
	SetVar "strFromDate", from_date
	SetVar "strToDate", to_date


	SetVar "alertsVal1Name", alertsVal1Name
	SetVar "alertsVal1Color", alertsVal1Color
	SetVar "alertsVal1Value", alertsVal1Value

	SetVar "alertsVal2Name", alertsVal2Name
	SetVar "alertsVal2Color", alertsVal2Color
	SetVar "alertsVal2Value", alertsVal2Value

	SetVar "alertsVal3Name", alertsVal3Name
	SetVar "alertsVal3Color", alertsVal3Color
	SetVar "alertsVal3Value", alertsVal3Value

	SetVar "totalAlertsValue", totalAlertsValue


	SetVar "alertChart", alertChart

	strUserName=replace(strUserName, """", """""")
        csvData=""
	csvData=csvData & """User name"",""Groups"",""OUs"",""Total alerts""," & """" & alertsVal1Name & """" & "," & """" & alertsVal2Name & """" & "," & """" & alertsVal3Name & """" 
	csvData=csvData & vbcrlf & """" & strUserName & """" & "," & """" & intGroupsCnt & """" & "," & """" & intOUsCnt & """" & "," & """" & totalAlertsValue & """" & "," & """" & alertsVal1Value & """" & "," & """" & alertsVal2Value & """" & "," & """" & alertsVal3Value & """"
	WriteToFile alerts_dir & "admin\csv\user" & user_id & ".csv", "", False
	WriteToFile alerts_dir & "admin\csv\user" & user_id & ".csv", csvData, True

 	SetVar "csvUrl", "csv/user"&user_id&".csv"


end if


'--------------------


'-------------------------------
' Step through each form
'-------------------------------
Header_Show
Footer_Show
'-------------------------------
' Process page templates
'-------------------------------
Parse "Header", False
Parse "Footer", False
Parse "main", False
'-------------------------------
' Output the page to the browser
'-------------------------------
Response.write PrintVar("main")

' index Show End

'-------------------------------
' Destroy all object variables
'-------------------------------

' index Close Event begin
' index Close Event End

UnloadTemplate
'===============================

else

Login(Request("intErrorCode"))
'response.write "test111222"

end if

'===============================
' Display Grid Form
'-------------------------------

%><!-- #include file="db_conn_close.asp" -->