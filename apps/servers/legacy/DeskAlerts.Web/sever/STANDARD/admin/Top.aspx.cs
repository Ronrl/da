﻿using DeskAlertsDotNet.Managers;
using DeskAlertsDotNet.Models;
using DeskAlertsDotNet.Parameters;
using System;

namespace DeskAlertsDotNet.Pages
{
    using Resources;

    public partial class Top : System.Web.UI.Page
    {
        protected User _user;

        protected override void OnLoad(EventArgs e)
        {
            _user = UserManager.Default.CurrentUser;

            if (!IsPostBack && Config.Data.IsDemo)
            {
                demo.Visible = true;
                windows.Text = resources.LNG_WINDOWS;
                macOSX.Text = resources.LNG_MAC_OS_X;
                android.Text = resources.LNG_ANDROID;
                iOS.Text = resources.LNG_IOS;
            }

            if (!Config.Data.IsDemo && Config.Data.IsOptionEnabled("ConfHideBuyAddonsLink"))
            {
                buyButton.Visible = true;
            }

            if (Settings.Content["ConfEnableHelp"] == "0")
            {
                helpOpener.Visible = false;
            }
        }
    }
}