﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%
uid = Session("uid")
try_to_auth()
%>
<!DOCTYPE html>
<html style="height:100%">
<head>
	<title>DeskAlerts</title>
	<meta http-equiv="X-UA-Compatible" content="IE=Edge" /> 
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link href="css/style9.css" rel="stylesheet" type="text/css">
	<link href="images/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
	<script type="text/javascript">
		if(window.top !== window.self)
		{
			window.top.location = window.self.location;
		}
	</script>
	<script>
	    $(document).ready(function() {
	        $.post($('#sharepoint').attr('action'), $('#sharepoint').serialize())
            .done(function(data, textStatus, jqXHR) {
                $('#results').html(data);
            })
            .fail(function(jqXHR, textStatus, errorThrown) {
                $('#results').html("<h3>Request has been failed. Try again</h3>");
            });
	    });
</script>
</head>
<%
if uid <> "" then
	SQL = "SELECT start_page FROM users WHERE id = "&uid
	SET RS =  Conn.Execute(SQL)
		
	if (NOT RS.EOF) then
			userStartPage = RS("start_page")
	end if
	if(IsNull(RS("start_page"))) then 
		userStartPage = "dashboard.aspx"
	end if
%>
<frameset rows="*" cols="240, 100%" framespacing="0" frameborder="0">
    <frame src="Menu.aspx" class="menu_frame" id="menu" name="menu" scrolling="auto" noresize>
	<frameset rows="63,100%" cols="*" framespacing="0" frameborder="0">
		<frame src="top.asp" id="top" scrolling="no" noresize>
		<frame src="main.asp?url=<%=userStartPage%>" scrolling="auto" id="main" name="main" noresize>
	</frameset>
</frameset>
<noframes>
    <body></body>
</noframes>
<%
else
sharepoint = Request("sharepoint")
%>
<body class="login_body" style="height:100%;margin:0px" onload="var login = document.getElementById('login'); if(login) login.focus();">
<div style="display:table; position:absolute;height:100%;width:100%;">
<div style="display:table-cell; vertical-align:middle;">
<div style="margin-left:auto;margin-right:auto;width:500px;">
<!-- #include file="comm_login.asp" -->
<%if DEMO = 1 and sharepoint <> "" then %>
<form id="sharepoint" action="<%=alerts_folder&"register.asp" %>"> 
<input type="hidden" value="1" name="sharepoint" />
<input type="hidden" value="<%=Request("username") %>" name="uname" />
<input type="hidden" value="<%=Request("username") %>" name="upass" />
<input type="hidden" value="<%=Request("guid") %>" name="deskbar_id" />
</form>
<div id="results" style="text-align:center;"></div>
<%
   end if
	'Call login()

    Response.Redirect "Auth.aspx"
%>
</div>
</div>
</div>

</body>
<%
end if
%>
</html>
<!-- #include file="db_conn_close.asp" -->