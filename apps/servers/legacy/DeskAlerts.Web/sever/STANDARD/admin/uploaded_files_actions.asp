<!-- #include file="config.inc" -->

<%

 uid = Session("uid")
  
  '--------------------------------------------
'Deletes the file given the full file name strFileName
'INPUT : the file name to be deleted, less the path
Sub delete(strFilePath)
	Dim oFS, a
	if InStr(1, strFilePath, "images/upload", 1) = 1 then
		Set oFS = Server.CreateObject("Scripting.FileSystemObject")
		a = oFS.DeleteFile(Server.MapPath(strFilePath))
		Set oFs = nothing
		Set a = nothing	
	end if
End sub

function generateConfirmDeleteMessage(strFilePath)
	Dim message
	message="Are you sure you wish to delete this file? "

	Set RS = Conn.Execute("SELECT id FROM alerts WHERE alert_text LIKE N'%"& strFilePath &"%'")
	if(Not RS.EOF) then
	 message = message & "This file is used in alerts.<br>"
	end if

	Set RS2 = Conn.Execute("SELECT id FROM text_templates WHERE template_text LIKE N'%"& strFilePath &"%'")
	if(Not RS2.EOF) then
 	 message = message & "This file is used in text templates.<br>"
	end if

	Set RS1 = Conn.Execute("SELECT id FROM templates WHERE [top] LIKE N'%"& strFilePath &"%' OR [bottom] LIKE N'%"& strFilePath &"%'")
	if(Not RS1.EOF) then
 	 message = message & "This file is used in templates.<br>"
	end if

	generateConfirmDeleteMessage = message
end function

if(uid<>"") then
	action = Request("action")
	filePath = Request("file_path")
	if (action = "delete") then
		delete filePath
	elseif (action = "confirm_delete") then
		Response.Write generateConfirmDeleteMessage(filePath)
	end if
end if

%>