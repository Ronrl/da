﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Parameters;


namespace DeskAlertsDotNet.Pages
{
    public partial class AddTemplate : DeskAlertsBasePage
    {
        //use curUser to access to current user of DA control panel      
        //use dbMgr to access to database

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            string templateName = Request["templateName"];
            string templateContent = Request["templateContent"];

            bool isUpdate = !string.IsNullOrEmpty(Request["edit"]) && Request["edit"] == "1";

            if(!string.IsNullOrEmpty(templateName) && !string.IsNullOrEmpty(templateContent))
            {
                if (!isUpdate)
                {
                    dbMgr.Insert("text_templates",
                        new Dictionary<string, object>() {{"name", templateName}, {"template_text", templateContent}});
                }
                else
                {
                    dbMgr.Update(                        
                        "text_templates",                        
                        new Dictionary<string, object>() {{"name", templateName},{"template_text", templateContent}},
                        new Dictionary<string, object>() { { "id", Request["templateId"] } }
                   );
                }

            }

            Response.Redirect("TextTemplates.aspx");
            //Add your main code here
        }

        [WebMethod]
        public static bool IsTemplateExists(string templateName)
        {
            using (DBManager dbManager = new DBManager())
            {
               int templatesCount =  dbManager.GetScalarByQuery<int>("SELECT COUNT(1) FROM text_templates WHERE name = " +
                                                templateName.Quotes());

                return templatesCount > 0;
            }

      }
    }
}