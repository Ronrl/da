﻿<!-- #include file="config.inc" -->
<%

check_session()

%>
<!-- #include file="ad.inc" -->
<!-- #include file="db_conn.asp" -->
<%
check_session()
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>View computer details</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/style9.css" rel="stylesheet" type="text/css">
</head>
<script language="javascript">
function LINKCLICK() {
  alert ("This action is disabled in demo");
  return false;
}
</script>

<body style="margin:0" class="body">
<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	<tr>
	<td>
	<table width="100%" height="100%" border="1" bordercolor="#000000" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
		<tr>
		<td width=100% height=31 class="theme-colored-back"><a href="" class="header_title">Computer details</a></td>
		</tr>
		<tr>
		<td bgcolor="#ffffff" height="100%">
		<div style="margin:10px;"><br><br>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
<%
	if Request("id") <> "" then
	Set RS = Conn.Execute("SELECT id, context_id, name, domain_id, reg_date, last_date, next_request, convert(varchar,last_request) as last_request1, last_request FROM computers WHERE id=" & Request("id"))
	num=0
	if(Not RS.EOF) then
		 Response.Write "<tr><td> <b>Name: </b></td><td>"& RS("name")&"</td></tr>"
  	        if (AD = 3) then 	
	       	Set RS3 = Conn.Execute("SELECT name FROM domains WHERE id=" & RS("domain_id"))
		if(Not RS3.EOF) then
			  domain=RS3("name")
		else
			domain="&nbsp;"
		end if
		RS3.close
			Response.Write "<tr><td> <b>Domain: </b></td><td>"& domain &"</td></tr>" 
		end if
	        	mygroups=""
			Set RS1 = Conn.Execute("SELECT name FROM groups INNER JOIN computers_groups ON groups.id=computers_groups.group_id WHERE comp_id=" & Request("id"))
			Do While Not RS1.EOF
					mygroups=mygroups & RS1("name")

				RS1.MoveNext
				if (Not RS1.EOF) then
					mygroups=mygroups & ", "
				end if
			loop
			RS1.Close
			 Response.Write "<tr><td valign='top'> <b>Groups: </b></td><td>"&mygroups&"</td></tr>"

			if(Not IsNull(RS("last_request"))) then
				last_activ = CStr(RS("last_request1"))
		
			        mydiff=DateDiff ("n", RS("last_request"), now_db)
					if(RS("next_request")<>"") then
						online_counter = Int(CLng(RS("next_request"))/60) * 2
					else
						online_counter = 2
					end if
				if(mydiff > online_counter) then
					online="<img src='images/offline.gif' alt='offline' width='11' height='11' border='0'>"
				else
					online="<img src='images/online.gif' alt='online' width='11' height='11' border='0'>"
				end if
			else
				last_activ = "&nbsp;"
				online="<img src='images/offline.gif' alt='offline' width='11' height='11' border='0'>"
			end if


			 Response.Write "<tr><td valign='top'> <b>Last activity: </b></td><td>" & last_activ & "</td></tr>"
		 
		 if (AD = 0) then 	
			if(Not IsNull(RS("reg_date"))) then
			 Response.Write "<tr><td valign='top'> <b>Registration Date: </b></td><td>" &  RS("reg_date") & "</td></tr>"
			else
			 Response.Write "<tr><td valign='top'> <b>Registration Date: </b></td><td>&nbsp;</td></tr>"
			end if
		end if

		if (EDIR = 1) then 	
			Set RS3 = Conn.Execute("SELECT name FROM edir_context WHERE id=" & RS("context_id"))
			if(Not RS3.EOF) then
				  strContext=RS3("name")
				RS3.close
			else
				strContext="&nbsp;"
			end if

			strContext=replace (strContext,",ou=",".")
			strContext=replace (strContext,",o=",".")
			strContext=replace (strContext,",dc=",".")
			strContext=replace (strContext,",c=",".")

			strContext=replace (strContext,"ou=","")
			strContext=replace (strContext,"o=","")
			strContext=replace (strContext,"dc=","")
			strContext=replace (strContext,"c=","")

			Response.Write "<tr><td valign='top'> <b>Context: </b></td><td>" & strContext & "</td></tr>"
		end if

		Response.Write "<tr><td valign='top'> <b>Online: </b></td><td>" & online & "</td></tr>"
	else
	Response.Write "<tr><td> Error: No such computer in database! </td></tr>"
	end if
	RS.Close
	end if
%>
		</table>
		 <img src="images/inv.gif" alt="" width="1" height="100%" border="0">
		</div>
		</td>
		</tr>
	</table>
	</td>
	</tr>
</table>
</body>
</html>

<!-- #include file="db_conn_close.asp" -->