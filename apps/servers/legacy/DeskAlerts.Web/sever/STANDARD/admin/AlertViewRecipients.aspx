﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AlertViewRecipients.aspx.cs" Inherits="DeskAlerts.Server.Pages.AlertViewRecipients" %>

<%@ Import Namespace="DeskAlertsDotNet.Parameters" %>
<%@ Import Namespace="Resources" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>DeskAlerts</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link href="css/style9.css" rel="stylesheet" type="text/css">
    <link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="css/jquery/jquery_timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css">
    <script language="javascript" type="text/javascript" src="functions.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/date.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery_timepicker/jquery-ui-timepicker-addon.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/jquery/datepicker_fix.js"></script>
    <script language="javascript" type="text/javascript">


        $(document).ready(function () {
            $(".delete_button").button({
            });

            $("#selectAll").click(function () {
                var checked = $(this).prop('checked');
                $(".content_object").prop('checked', checked);
            });


        });


        function showPreview(id) {
            window.open('../UserDetails.aspx?id=' + id, '', 'status=0, toolbar=0, width=350, height=350, scrollbars=1');
        }
        function Return() {
            var getPathFromOpenPage = document.referrer;
            var isEmpty = (getPathFromOpenPage === undefined || getPathFromOpenPage == null || getPathFromOpenPage.length <= 0) ? true : false;

            if (~getPathFromOpenPage.indexOf("SurveyAnswersStatistic.aspx") || isEmpty) {
                window.close();
            }
            else {
                history.back();
            }
        }

        function editUser(id, queryString) {
            location.replace('../EditUsers.aspx?id=' + id + '&return_page=users_new.asp?' + queryString);
        }

        var usersIds = $("input[name=recipientId]")
            .map(function () {
                return $(this).val();
            })
            .get()
            .join();

        if (usersIds.length == 0) {

            $("#newGroupButton").css('display', 'none');
        }

        $("#newGroupDialog").dialog({
            autoOpen: false,
            height: 125,
            width: 200,
            modal: true,
            resizable: false
        });


        function openDialog() {

            $("#newGroupDialog")
                .dialog({
                    autoOpen: false,
                    height: 125,
                    width: 250,
                    modal: true,
                    resizable: false
                })
                .dialog("open");

        }

        function addGroup() {

            var name = $("#groupName").val();

            var usersIds = $("input[prop=recipientId]")
                .map(function () {
                    return $(this).val();
                })
                .get()
                .join();


            if (usersIds.length == 0) {

                alert("There are no users chosen this answer");
                return;
            }
            $.ajax({
                type: "POST",
                url: "EditGroups.aspx/AddGroup",
                data: '{name: "' + name + '", ids: "' + usersIds + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {

                    $("#newGroupDialog").dialog("close");
                    alert("Recipients were successfully added to group " + name);

                    //window.parent.parent.document.getElementById('menu').src = window.parent.parent.document.getElementById('menu').src;
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
            <tr>
                <td>
                    <table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
                        <tr>
                        </tr>
                        <tr>
                            <td class="main_table_body" height="100%">
                                <br />
                                <br />
                                <table style="padding-left: 10px;" width="100%" border="0">
                                    <tbody>
                                        <tr valign="middle">
                                            <td width="240">
                                                <% =resources.LNG_SEARCH + ":" %>
                                                <input type="text" id="searchTermBox" runat="server" name="uname" value="" /></td>
                                            <td width="1%">
                                                <br />
                                                <asp:LinkButton runat="server" class="search_button ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" ID="searchButton" Style="margin-right: 0px" role="button" aria-disabled="false" Text="<span class='ui-button-icon-primary ui-icon ui-icon-search'></span><span class='ui-button-text'>{Search}</span>" />
                                                <input type="hidden" name="search" value="1">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div style="margin-left: 10px" runat="server" id="mainTableDiv">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td>
                                                <div style="margin-left: 10px" id="topPaging" runat="server"></div>
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                    <br />
                                    <asp:Table Style="margin-right: 10px;" runat="server" ID="contentTable" Width="98.5%" Height="100%" CssClass="data_table" CellSpacing="0" CellPadding="3">
                                        <asp:TableRow CssClass="data_table_title">
                                            <asp:TableCell runat="server" ID="userCell" CssClass="table_title"></asp:TableCell>
                                            <asp:TableCell runat="server" ID="typeCell" CssClass="table_title" HorizontalAlign="Center"></asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                    <br />
                                    <br />
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td>
                                                <div style="margin-left: 10px" id="bottomPaging" runat="server"></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <br />
                                                <center>
                                                    <a class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" onclick = "Return()">
                                                        <span class="ui-button-text">Return</span>
                                                    </a>
                                                </center>
                                                <br />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <br />
                                <div runat="server" id="NoRows" style="float: left">
                                </div>
                                <br />
                                <br />
                            </td>
                        </tr>
                    </table>
                    <div runat="server" id="optInDiv" visible="False">
                        <br />
                        <a href="#" class="delete_button" id="newGroupButton" onclick="openDialog()"><% =resources.LNG_ADD_USERS_TO_GROUP %></a>
                        <div id="newGroupDialog" style="display: none">
                            <label id="groupNameLabel" for="linkValue"><% =resources.LNG_GROUP_NAME %> : </label>
                            <input id="groupName" type="text" style="width: 200px" /><br />
                            <br />

                            <a href='#' class="delete_button" style="float: right;" onclick="javascript:addGroup();">
                                <% =resources.LNG_CREATE %>
                            </a>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
