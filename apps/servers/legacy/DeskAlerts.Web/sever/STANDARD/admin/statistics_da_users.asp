<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%
check_session()
%>
<!-- #include file="Common.asp" -->
<!-- #include file="header.asp" -->
<!-- #include file="footer.asp" -->
<!-- #include file="libs/FusionCharts.asp" -->

<%
Server.ScriptTimeout = 600
Conn.CommandTimeout = 600

On Error Resume Next
' index CustomIncludes End
'-------------------------------

'===============================
' Save Page and File Name available into variables
'-------------------------------
Dim sFileName
Dim sTemplateFileName
Dim sPaginateFileName
Dim intSessionUserId

sFileName = "statistics_da_users.asp"
sTemplateFileName = "statistics_da_users.html"
sPaginateFileName = "paginate.html"
'===============================
  

intSessionUserId = Session("uid")
uid=intSessionUserId 
if (intSessionUserId <> "") then


' index Show begin

'===============================
' Display page
'-------------------------------
' Load HTML template for this page
'-------------------------------
LoadTemplate alerts_dir & "\" & templateFolder & "\" & sTemplateFileName, "main"
SetVar "DListUsersStat", ""
'-------------------------------
' Load HTML template of Header and Footer
'-------------------------------
sHeaderFileName=Replace(sHeaderFileName,"header.html","header-old.html")
LoadTemplate sHeaderFileName, "Header"
LoadTemplate sFooterFileName, "Footer"

'-------------------------------
LoadTemplate sDateFormFileName, "DateForm"
LoadTemplate sSearchFormFileName, "SearchForm"
LoadTemplate sMenuFileName, "Menu"

'SetVar "myvalue", "test"


Dim intInstallNum
Dim intUninstallNum
Dim rsToolbar
Dim rsInstall
Dim rsUninstall

Dim i
Dim days

Dim from_day
Dim from_month
Dim from_year

Dim to_day
Dim to_month
Dim to_year


SetVar "SearchTitle", LNG_SEARCH_USER_BY_NAME
SetVar "Search", LNG_SEARCH


' Get dates
start_date = Request("start_date")
end_date = Request("end_date")


my_day = "01"
my_month = "01"
my_year = "2001"

my_day1 = day(date())
my_month1 = month(date())
my_year1 = year(date())

test_date=make_date_by(my_day, my_month, my_year)
test_date1=make_date_by(my_day1, my_month1, my_year1)

from_date = test_date & " 00:00:00"
to_date = test_date1 & " 23:59:59"

days = array ("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec")

if(Request("select_date_radio")="1" OR Request("select_date_radio")="") then setVar "strCheckedOption1", "checked" end if 

if(Request("select_date_radio")="2") then setVar "strEnabledOption1", "disabled" end if 

if(Request("range")="1") then setVar "strSelectedOption1", "selected" end if 
if(Request("range")="2") then setVar "strSelectedOption2", "selected" end if 
if(Request("range")="3") then setVar "strSelectedOption3", "selected" end if 
if(Request("range")="4") then setVar "strSelectedOption4", "selected" end if 
if(Request("range")="5") then setVar "strSelectedOption5", "selected" end if 
if(Request("range")="6") then setVar "strSelectedOption6", "selected" end if 

if(Request("select_date_radio")="2") then 
setVar "strCheckedOption2", "checked" 
end if 




if(Request("select_date_radio")="1" OR Request("select_date_radio")="") then setVar "strEnabledOption2", "disabled" end if 


SetVar "startDateValue", start_date
SetVar "endDateValue", end_date

   if(Request("offset") <> "") then 
	offset = clng(Request("offset"))
   else 
	offset = 0
   end if	
   
   if(Request("limit") <> "") then 
	limit = clng(Request("limit"))
   else 
	limit=50
   end if
   
   myuname = request("uname")
	if(myuname <> "") then 
		searchSQL = " (users.name LIKE N'%"&Replace(myuname, "'", "''")&"%' OR users.display_name LIKE N'%"&Replace(myuname, "'", "''")&"%')"
	else 
		searchSQL = ""
	end if	


SetVar "type", Request("type")
SetVar "limit", limit

'editor ---
	Set RS = Conn.Execute("SELECT id, group_id FROM users WHERE id="&intSessionUserId&" AND role='U'")
	if(Not RS.EOF) then
		gid = 1
		Set rs_policy = Conn.Execute("SELECT p.id, p.type, pu.user_id FROM policy p INNER JOIN policy_editor pe ON p.id=pe.policy_id LEFT JOIN policy_user pu ON pu.policy_id = p.id AND pu.user_id = 0 WHERE pe.editor_id=" & uid)
		if Not rs_policy.EOF then
			if rs_policy("type")="A" or not IsNull(rs_policy("user_id")) then 'can send to all
				gid = 0
			end if
		end if
		if(gid=1) then
			editor_id=RS("id")
			policy_ids=""
			policy_ids=editorGetPolicyIds(editor_id)
			policy_group_ids=""
			policy_group_ids=editorGetGroupIds(policy_ids)
			policy_group_ids=replace(policy_group_ids, "group_id", "users_groups.group_id")
			if(policy_group_ids <> "") then
				policy_group_ids = "OR " & policy_group_ids
			end if
		end if
	else
		gid = 0
	end if

	RS.Close
'--------
'---------------------------------

	if(Request("sortby") <> "") then 
		sortby = Request("sortby")
	else
		sortby = "name"
	end if


    if sortby = "{sortby}" then
        sortby = "name"
    end if
	myuname = Request("uname")
	if myuname<>"" then
		SetVar "searchUserName", HTMLEncode(myuname)
	else
		SetVar "searchUserName", ""
	end if


	search_type="organization"
	search_id=0
	cnt=0
	if (search_type <>"") then
	
		showSQL = 	" SET NOCOUNT ON" & vbCrLf &_
					" IF OBJECT_ID('tempdb..#tempOUs') IS NOT NULL DROP TABLE #tempOUs" & vbCrLf &_
					" CREATE TABLE #tempOUs  (Id_child BIGINT NOT NULL, Rights BIT NOT NULL);" & vbCrLf &_
					" IF OBJECT_ID('tempdb..#tempUsers') IS NOT NULL DROP TABLE #tempUsers" & vbCrLf &_
					" CREATE TABLE #tempUsers  (user_id BIGINT NOT NULL);" & vbCrLf &_
					" IF OBJECT_ID('tempdb..#tempGroups') IS NOT NULL DROP TABLE #tempGroups" & vbCrLf &_
					" CREATE TABLE #tempGroups  (group_id BIGINT NOT NULL);" & vbCrLf &_
					" DECLARE @now DATETIME;" & vbCrLf &_
					" SET @now = GETDATE(); "
					
		OUsSQL=""

		if(search_type = "ou") then
			if Request("search") = "1" then
				OUsSQL = OUsSQL & "WITH OUs (id) AS("
				OUsSQL = OUsSQL &" SELECT CAST("&search_id&" AS BIGINT) "
				OUsSQL = OUsSQL &_
						"	UNION ALL" & vbCrLf &_
						"	SELECT Id_child" & vbCrLf &_
						"	FROM OU_hierarchy h" & vbCrLf &_
						"	INNER JOIN OUs ON OUs.id = h.Id_parent" & vbCrLf &_
						") " & vbCrLf &_
						"INSERT INTO #tempOUs (Id_child, Rights) (SELECT DISTINCT id, 0 FROM OUs) Option (MaxRecursion 10000); "
			else
				OUsSQL = OUsSQL &" INSERT INTO #tempOUs (Id_child, Rights) VALUES ("&search_id&", 0)"
			end if
		elseif(search_type = "DOMAIN") then
			OUsSQL = OUsSQL &" INSERT INTO #tempOUs (Id_child, Rights) (SELECT id, 0 FROM OU WHERE Id_domain = "&search_id&")"
		else
			OUsSQL = OUsSQL & " INSERT INTO #tempOUs (Id_child, Rights) (SELECT id, 0 FROM OU) "
		end if

		if (gid = 1) then 
			OUsSQL = OUsSQL & vbCrLf &_
				" ;WITH OUsList (Id_parent, Id_child) AS (" & vbCrLf &_
				" 	SELECT Id_child AS Id_parent, Id_child FROM #tempOUs" & vbCrLf &_
				"	UNION ALL" & vbCrLf &_
				"	SELECT h.Id_parent, l.Id_child" & vbCrLf &_
				"	FROM OU_hierarchy as h" & vbCrLf &_
				"	INNER JOIN OUsList as l" & vbCrLf &_
				"	ON h.Id_child = l.Id_parent)" & vbCrLf &_ 
				" UPDATE #tempOUs" & vbCrLf &_
				" SET Rights = 1" & vbCrLf &_
				" FROM #tempOUs t" & vbCrLf &_
				" INNER JOIN OUsList l" & vbCrLf &_
				" ON l.Id_child = t.Id_child" & vbCrLf &_
				" LEFT JOIN policy_ou p ON p.ou_id=l.Id_parent AND " & Replace(policy_ids, "policy_id", "p.policy_id") & vbCrLf &_
				" LEFT JOIN ou_groups g ON g.ou_id=l.Id_parent" & vbCrLf &_
				" LEFT JOIN policy_group pg ON pg.group_id=g.group_id AND " & Replace(policy_ids, "policy_id", "pg.policy_id") & vbCrLf &_
				" WHERE NOT p.id IS NULL OR NOT pg.id IS NULL"
				
		end if

		groupsSQL = " INSERT INTO #tempGroups (group_id)" & vbCrLf &_ 
					" (" & vbCrLf &_
					" SELECT DISTINCT groups.id" & vbCrLf &_
					" FROM groups" & vbCrLf &_
					" INNER JOIN OU_Group og" & vbCrLf &_
					" ON groups.id=og.Id_group" & vbCrLf &_
					" INNER JOIN #tempOUs t" & vbCrLf &_
					" ON og.Id_OU=t.Id_child"
		if (gid = 1) then 
			groupsSQL = groupsSQL &_
					" LEFT JOIN policy_group p ON p.group_id=og.Id_group AND "&policy_ids &_
					" WHERE (t.Rights = 1 OR NOT p.group_id IS NULL) " 
		end if
		groupsSQL = groupsSQL & " )"
						
'		if (search_type <> "ou") then
'			groupsSQL = groupsSQL &_
'						" INSERT INTO #tempGroups (group_id)" & vbCrLf &_
'						" (" & vbCrLf &_
'						" SELECT DISTINCT groups.id" & vbCrLf &_
'						" FROM groups"
'			if (gid = 1) then 
'				groupsSQL = groupsSQL & " INNER JOIN policy_group p ON p.group_id=groups.id AND "&policy_ids
'			end if
'				groupsSQL = groupsSQL &_	
'						" LEFT JOIN #tempGroups t" & vbCrLf &_
'						" ON t.group_id=groups.id" & vbCrLf &_
'						" WHERE t.group_id IS NULL "
'			if (search_type = "DOMAIN") then
'				groupsSQL = groupsSQL & " AND groups.domain_id = "&search_id
'			end if 
'			groupsSQL = groupsSQL & " ) "
'		end if
		
		groupsSQL = groupsSQL &_
						" IF OBJECT_ID('tempdb..#tmp1') IS NOT NULL DROP TABLE #tmp1" & vbCrLf &_
						" CREATE TABLE #tmp1  (group_id BIGINT NOT NULL);" & vbCrLf &_
						" IF OBJECT_ID('tempdb..#tmp2') IS NOT NULL DROP TABLE #tmp2" & vbCrLf &_
						" CREATE TABLE #tmp2  (group_id BIGINT NOT NULL);" & vbCrLf &_
						" INSERT INTO #tmp1 (group_id)(SELECT group_id FROM #tempGroups)" & vbCrLf &_
						" WHILE 1=1" & vbCrLf &_
						" BEGIN" & vbCrLf &_
						" 	INSERT INTO #tmp2 (group_id)" & vbCrLf &_
						" 	(" & vbCrLf &_
						" 			SELECT DISTINCT g.group_id FROM groups_groups as g" & vbCrLf &_
						" 			INNER JOIN #tmp1 as t1" & vbCrLf &_
						" 			ON g.container_group_id = t1.group_id" & vbCrLf &_
						" 			LEFT JOIN #tempGroups as t2" & vbCrLf &_
						" 			ON g.group_id = t2.group_id" & vbCrLf &_
						" 			WHERE t2.group_id IS NULL" & vbCrLf &_
						" 	)" & vbCrLf &_
						" 	IF @@ROWCOUNT = 0 BREAK;" & vbCrLf &_
						" 	DELETE #tmp1" & vbCrLf &_
						" 	INSERT INTO #tmp1 (group_id)(SELECT group_id FROM #tmp2)" & vbCrLf &_
						" 	INSERT INTO #tempGroups (group_id)(SELECT group_id FROM #tmp2)" & vbCrLf &_
						" 	DELETE #tmp2" & vbCrLf &_
						" END; "& vbCrLf &_
						" DROP TABLE #tmp1" & vbCrLf &_
						" DROP TABLE #tmp2 "
		

		usersSQL = " INSERT INTO #tempUsers (user_id)(" & vbCrLf &_
					" SELECT DISTINCT Id_user" & vbCrLf &_
					" FROM OU_User u" & vbCrLf &_
					" INNER JOIN #tempOUs t" & vbCrLf &_
					" ON u.Id_OU=t.Id_child" & vbCrLf &_ 
					" LEFT JOIN users ON u.Id_user = users.id"
		
		whereSQL = ""
		
		if (gid = 1) then 
			usersSQL = usersSQL & vbCrLf &_
					" LEFT JOIN policy_user p ON p.user_id=u.Id_user AND "&policy_ids
			whereSQL = " AND (t.Rights = 1 OR NOT p.id IS NULL) "
		end if
		
		usersSQL = usersSQL & " WHERE role='U' " & whereSQL
			
		if (searchSQL<>"") then 
			usersSQL = usersSQL & " AND " & searchSQL
		end if
		usersSQL = usersSQL & ") "
		
		whereSQL = ""
		usersSQL = usersSQL &_
					" INSERT INTO #tempUsers (user_id)(" & vbCrLf &_
					" 	SELECT DISTINCT g.user_id" & vbCrLf &_
					" 	FROM users_groups g" & vbCrLf &_
					" 	INNER JOIN #tempGroups t" & vbCrLf &_
					" 	ON t.group_id = g.group_id" & vbCrLf &_
					" 	LEFT JOIN #tempUsers tu" & vbCrLf &_
					" 	ON tu.user_id = g.user_id" 
			
		if (searchSQL<>"") then
			usersSQL = usersSQL & " LEFT JOIN users ON users.id = g.user_id" 
			whereSQL = " AND " & searchSQL
		end if 
		usersSQL = usersSQL & " WHERE tu.user_id IS NULL" & whereSQL
		usersSQL = usersSQL & " )"
		
		if (search_type <> "ou") then
			usersSQL = usersSQL & vbCrLf &_
						" INSERT INTO #tempUsers(user_id)(" & vbCrLf &_
						" SELECT DISTINCT users.id FROM users "
			
			if (gid = 1) then 
				usersSQL = usersSQL & " INNER JOIN policy_user p ON p.user_id=users.id AND "&policy_ids
			end if
			
			usersSQL = usersSQL & vbCrLf &_	
					" LEFT JOIN #tempUsers t" & vbCrLf &_
					" ON t.user_id=users.id" & vbCrLf &_
					" WHERE t.user_id IS NULL "
			
			if (search_type = "DOMAIN") then
				usersSQL = usersSQL & " AND users.domain_id = "&search_id
			end if 
				if (searchSQL<>"") then 
					usersSQL = usersSQL & " AND " & searchSQL
				end if
				usersSQL = usersSQL & ") "	
		end if
		
		usersSQL = usersSQL & vbCrLf &_
					" SELECT COUNT(1) as cnt FROM #tempUsers t" & vbCrLf &_
					" INNER JOIN users ON t.user_id = users.id" & vbCrLf &_
					" WHERE role='U' AND (users.last_request >= '" & from_date & "' AND users.last_request <= '" & to_date & "') AND (users.name LIKE N'%" & Replace(myuname, "'", "''") & "%' OR users.display_name LIKE N'%" & Replace(myuname, "'", "''") & "%')" & vbCrLf &_
					" SELECT user_id as id, users.next_request, users.last_standby_request, users.next_standby_request, users.context_id, users.mobile_phone, users.email, users.name as name, users.display_name, users.standby, users.domain_id, domains.name as domain_name, users.deskbar_id, users.reg_date, users.last_date, CONVERT(varchar, users.last_request) as last_request1, users.last_request, users.client_version, " & vbCrLf &_
					" edir_context.name as context_name, users.disabled" & vbCrLf &_
					" FROM #tempUsers t" & vbCrLf &_
					" INNER JOIN users ON t.user_id = users.id" & vbCrLf &_
					" LEFT JOIN domains ON domains.id = users.domain_id" & vbCrLf &_
					" LEFT JOIN edir_context ON edir_context.id = users.context_id" & vbCrLf &_
					" WHERE role='U' AND (users.last_request >= '" & from_date & "' AND users.last_request <= '" & to_date & "') AND (users.name LIKE N'%" & Replace(myuname, "'", "''") & "%' OR users.display_name LIKE N'%" & Replace(myuname, "'", "''") & "%')" & vbCrLf &_
					" ORDER BY " & sortby & vbCrLf &_
					" DROP TABLE #tempOUs" & vbCrLf &_
					" DROP TABLE #tempUsers" & vbCrLf &_ 
					" DROP TABLE #tempGroups"
		
		showSQL = showSQL & OUsSQL & groupsSQL & usersSQL

        'Response.Write(showSQL)
		Set RS = Conn.Execute(showSQL)
		

		if(Not RS.EOF) then
			cnt=RS("cnt")
			SET RS = RS.NextRecordSet
		end if
		
		j=cnt/limit
		
	end if


	if(DA<>1) then
		SetVar "strLogout", "Logout"
	end if

	PageName(LNG_STATISTICS_USERS_STATISTICS)
	GenerateMenu(2)

	SetVar "LNG_SEARCH_USERS", LNG_SEARCH_USERS
	SetVar "LNG_USERNAME", LNG_USERNAME
	SetVar "LNG_STATUS", LNG_STATUS
	SetVar "LNG_REGISTERED", LNG_REGISTERED
	SetVar "LNG_LAST_ACTIVITY", LNG_LAST_ACTIVITY
	
	
  '  Response.Write "COUNT = " & cnt
if(cnt>0) then



	page=sFileName & "?start_date=" & start_date & "&end_date=" & end_date & "&range=" & Request("range") & "&select_date_radio=" & Request("select_date_radio") & "&uname=" & Server.URLEncode(myuname) & "&"
	name=LNG_USERS
	

	
	SetVar "LNG_USERNAME", sorting_post(LNG_USERNAME,"name", sortby, offset, limit)
	SetVar "LNG_REGISTERED", sorting_post(LNG_REGISTERED,"reg_date", sortby, offset, limit)
	SetVar "LNG_LAST_ACTIVITY", sorting_post(LNG_LAST_ACTIVITY,"last_request", sortby, offset, limit)

	SetVar "paging", make_pages_post(offset, cnt, limit,  name, sortby) 
		
'show main table
	num=offset
	if(Not RS.EOF) then RS.Move(offset) end if
	Do While Not RS.EOF
		num=num+1
		if((offset+limit) < num) then Exit Do end if

		if(num > offset AND offset+limit >= num) then
			reg_date=RS("reg_date")
			last_date=RS("last_request")
			standby=RS("standby")
			disabled=RS("disabled")
			
				standby_diff=DateDiff ("n", RS("last_standby_request"), now_db)
				if(RS("next_standby_request")<>"" AND RS("next_standby_request")<>"0") then
					standby_counter = Int(CLng(RS("next_standby_request"))/60) * 2
				else
					standby_counter = 10
				end if
			
			if(standby=1 AND (standby_diff < standby_counter)) then
				online="<img src='images/standby.gif' alt='stand by' width='11' height='11' border='0'>"
			else
				if(disabled=1) then
			     		online="<img src='images/disabled.gif' alt='disabled' width='11' height='11' border='0'>"
				else
					if(Not IsNull(RS("last_request"))) then
						mydiff=DateDiff ("n", RS("last_request"), now_db)
						if(RS("next_request")<>"") then
							online_counter = Int(CLng(RS("next_request"))/60) * 2
						else
							online_counter = 2
						end if							
						if(mydiff > online_counter) then
							online="<img src='images/offline.gif' alt='offline' width='11' height='11' border='0'>"
						else
							online="<img src='images/online.gif' alt='online' width='11' height='11' border='0'>"
						end if
					else
						online="<img src='images/offline.gif' alt='offline' width='11' height='11' border='0'>"
					end if
				end if
			end if

			if(IsNull(RS("name"))) then
				name="Unregistered"
			else
				name = RS("name")
			end if
			if (AD = 3) then 	
				domain="&nbsp;"
'				if(Not IsNull(RS("domain_id"))) then
					if(RS("domain_name")<>"") then
'					       	Set RS3 = Conn.Execute("SELECT name FROM domains WHERE id=" & RS("domain_id"))
'						if(Not RS3.EOF) then
							  domain=RS("domain_name")
						else
							domain="&nbsp;"
'						end if
'						RS3.close
'					end if 
					end if
			end if
			strUserName = "<A href='#' onclick=""javascript: window.open('statistics_da_users_view_details.asp?id=" & CStr(RS("id")) & "&from_date=" & from_date & "&to_date=" & to_date & "&range=" & Request("range") & "&select_date_radio=" & Request("select_date_radio") & "','', 'status=0, toolbar=0, width=350, height=450, scrollbars=1')"">" & name & "</a>"
			if Len(RS("display_name")) > 0 then
				strUserName = strUserName & " (" & RS("display_name") & ")"
			end if
			SetVar "strUserName", strUserName

			if(Not IsNull(online)) then
				SetVar "strStatus", online
			else
				SetVar "strStatus", ""
			end if

			if(Not IsNull(reg_date)) then
				SetVar "strRegDate", reg_date
			else
				SetVar "strRegDate", ""
			end if

			if(Not IsNull(last_date)) then
				SetVar "strLastDate", last_date
			else
				SetVar "strLastDate", ""
			end if

			if(Not IsNull(myopen)) then
				SetVar "intOpenRatio", myopen
			else
				SetVar "intOpenRatio", ""
			end if

			Parse "DListUsersStat", True


		end if
		RS.MoveNext

	Loop
	RS.Close

else

'Response.Write "<center><b>There are no alerts.</b></center>"

end if


'-------------


'-------------------------------
' Step through each form
'-------------------------------
Header_Show
Footer_Show
'-------------------------------
' Process page templates
'-------------------------------
Parse "Header", False
Parse "Footer", False
Parse "main", False
'-------------------------------
' Output the page to the browser
'-------------------------------
Response.write PrintVar("main")

' index Show End

'-------------------------------
' Destroy all object variables
'-------------------------------

' index Close Event begin
' index Close Event End

UnloadTemplate
'===============================

else

Login(Request("intErrorCode"))
'response.write "test111222"

end if

'===============================
' Display Grid Form
'-------------------------------

%><!-- #include file="db_conn_close.asp" -->