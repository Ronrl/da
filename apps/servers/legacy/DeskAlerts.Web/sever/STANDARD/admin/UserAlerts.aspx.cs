﻿using System;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using DeskAlerts.ApplicationCore.Enums;
using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.ApplicationCore.Repositories;
using DeskAlerts.ApplicationCore.Utilities;
using DeskAlerts.Server.Utils.Interfactes;
using DeskAlerts.Server.Utils.Services;
using DeskAlertsDotNet.Parameters;
using DeskAlerts.Server.Utils.Helpers;

namespace DeskAlertsDotNet.Pages
{
    using DeskAlerts.ApplicationCore.Entities;
    using Resources;

    public partial class UserAlerts : DeskAlertsBaseListPage
    {
        private string _userId;
        private string _recipientsType;

        private readonly IContentReceivedService _contentReceivedService = new ContentReceivedService();
        private static readonly IConfigurationManager ConfigurationManager = new WebConfigConfigurationManager(Config.Configuration);
        private readonly IAlertReceivedRepository _alertReceivedRepository = new PpAlertReceivedRepository(ConfigurationManager);

        protected override void OnLoad(EventArgs e)
        {
            if (string.IsNullOrEmpty(Request["id"]) && string.IsNullOrEmpty(Request["answer_id"]))
            {
                Response.End();
                return;
            }

            _userId = Request["id"];
            _recipientsType = Request["type"];

            searchButton.Text = searchButton.Text.Replace("{Search}", resources.LNG_SEARCH);

            base.OnLoad(e);

            userNameCell.Text = resources.LNG_OBJECTS;
            dateCell.Text = resources.LNG_DATE;

            var cnt = Content.Count < (Offset + Limit) ? Content.Count : Offset + Limit;
            int countOfReceivedContentByUser;

            switch (_recipientsType)
            {
                case "drec":
                case "ack":
                    deletedObjectsMessage.InnerText = string.Empty;
                    break;
                case "rec":
                    countOfReceivedContentByUser = _alertReceivedRepository.GetReceivedForUser(long.Parse(_userId)).Count;

                    if (Content.Count < countOfReceivedContentByUser)
                    {
                        deletedObjectsMessage.InnerText = $"{countOfReceivedContentByUser - Content.Count} {resources.X_OBJECTS_WAS_DELETED}";
                    }
                    break;
                default:
                    countOfReceivedContentByUser = _alertReceivedRepository.GetSendedForUser(long.Parse(_userId)).Count;

                    if (Content.Count < countOfReceivedContentByUser)
                    {
                        deletedObjectsMessage.InnerText = $"{countOfReceivedContentByUser - Content.Count} {resources.X_OBJECTS_WAS_DELETED}";
                    }
                    break;
            }

            for (var i = Offset; i < cnt; i++)
            {
                var row = new TableRow();
                var dbRow = Content.GetRow(i);

                var usernameContentCell = new TableCell
                {
                    Text = dbRow.GetString("title")
                };

                row.Cells.Add(usernameContentCell);

                var dateContentCell = new TableCell
                {
                    Text = DateTimeConverter.ToUiDateTime(dbRow.GetDateTime("create_date"))
                };

                row.Cells.Add(dateContentCell);
                contentTable.Rows.Add(row);
            }
        }

        protected override string CustomQuery
        {
            get
            {
                string query;
                switch (_recipientsType)
                {
                    case "drec":
                        {
                            query = $@"
                                SELECT a.title, a.create_date
                                FROM (
                                    SELECT alert_id FROM (SELECT alert_id, user_id, SUM(CASE WHEN status = {(int) AlertReceiveStatus.NotReceived} THEN 0 ELSE 1 END) recievedTotal
                                    FROM alerts_received 
                                        GROUP BY alert_id, user_id
                                        HAVING user_id = {_userId}) rt
                                        WHERE rt.recievedtotal = 0) AS rt
                                JOIN (
                                SELECT a.id, a.title, a.create_date
                                FROM alerts AS a
                                WHERE a.class != {(int) AlertType.RssFeed}";

                            if (SearchTerm.Length > 0)
                            {
                                query += $" AND (a.title LIKE \'%{SearchTerm}%\')";
                            }

                            query += @") AS a
                                ON a.id = rt.alert_id";

                            break;
                        }

                    case "rec":
                        {
                            query = $@"
                                SELECT DISTINCT b.title, b.create_date 
                                FROM alerts_received AS a 
                                INNER JOIN alerts AS b ON a.alert_id = b.id 
                                WHERE user_id = {Request["id"]}
                                    AND a.status = {(int) AlertReceiveStatus.Received}";

                            if (SearchTerm.Length > 0)
                            {
                                query += $" AND (b.title LIKE \'%{SearchTerm}%\')";
                            }

                            break;
                        }

                    case "ack":
                        {
                            query = $@"
                                SELECT DISTINCT alerts.title, alerts_read.date as create_date 
                                FROM [alerts_read] 
                                INNER JOIN alerts ON alerts.id=alerts_read.alert_id 
                                WHERE user_id={_userId}";

                            if (SearchTerm.Length > 0)
                            {
                                query += $" AND (alerts.title LIKE \'%{SearchTerm}%\')";
                            }

                            break;
                        }

                    default:
                        {
                            query = $@" 
                                SELECT DISTINCT b.title, b.create_date 
                                FROM alerts_received AS a 
                                INNER JOIN alerts AS b ON a.alert_id = b.id 
                                WHERE user_id = {_userId} ";

                            if (SearchTerm.Length > 0)
                            {
                                query += $" AND (b.title LIKE \'%{SearchTerm}%\')";
                            }

                            break;
                        }
                }

                return query;
            }
        }

        protected override string CustomCountQuery
        {
            get
            {
                string query;
                switch (_recipientsType)
                {
                    case "drec":
                        {
                            query = $@"SELECT SUM(mycnt)
                                    FROM (
                                        SELECT COUNT(1) as mycnt 
                                        FROM (SELECT a.title, a.create_date
                                            FROM (
                                                SELECT alert_id FROM (SELECT alert_id, user_id, SUM(CASE WHEN status = {(int)AlertReceiveStatus.NotReceived} THEN 0 ELSE 1 END) recievedTotal
                                                FROM alerts_received 
                                                group by alert_id, user_id
                                                HAVING user_id = {_userId}) rt
                                                WHERE rt.recievedtotal = 0) AS rt
                                    JOIN (
                                    SELECT a.id, a.title, a.create_date
                                    FROM alerts AS a
                                    WHERE a.class != {(int)AlertType.RssFeed}";

                            if (SearchTerm.Length > 0)
                            {
                                query += $" AND (a.title LIKE \'%{SearchTerm}%\')";
                            }

                            query += @") AS a
                                ON a.id = rt.alert_id) t) t";

                            break;
                        }

                    case "rec":
                        {
                            query = $@"
                                SELECT COUNT(DISTINCT b.id) as mycnt 
                                FROM alerts_received AS a 
                                INNER JOIN alerts AS b ON a.alert_id = b.id 
                                WHERE user_id = {_userId}
                                    AND a.status = {(int) AlertReceiveStatus.Received}";

                            if (SearchTerm.Length > 0)
                            {
                                query += $" AND (b.title LIKE \'%{SearchTerm}%\')";
                            }

                            break;
                        }

                    case "ack":
                        {
                            query = $@"
                                SELECT COUNT(DISTINCT alert_id) as mycnt 
                                FROM [alerts_read] 
                                INNER JOIN alerts ON alerts.id=alerts_read.alert_id 
                                WHERE user_id={_userId}";

                            if (SearchTerm.Length > 0)
                            {
                                query += $" AND (alerts.title LIKE \'%{SearchTerm}%\')";
                            }

                            break;
                        }

                    default:
                        {
                            query = $@"
                                SELECT COUNT(DISTINCT b.id)
                                FROM alerts_received AS a
                                INNER JOIN alerts AS b ON a.alert_id = b.id
                                WHERE user_id = { _userId } ";

                            if (SearchTerm.Length > 0)
                            {
                                query += $" AND (b.title LIKE \'%{SearchTerm}%\')";
                            }

                            break;
                        }
                }

                return query;
            }
        }

        protected override string PageParams => "&id=" + Request["id"] + "&type=" + _recipientsType + "&";

        #region DataProperties
        protected override string DataTable => "alerts";

        protected override string[] Collumns => new string[] { };

        protected override string DefaultOrderCollumn => "id";

        #endregion

        #region Interface properties

        protected override IButtonControl[] DeleteButtons => new IButtonControl[] { };

        protected override HtmlGenericControl TopPaging => topPaging;

        protected override HtmlGenericControl BottomPaging => bottomPaging;

        protected override HtmlGenericControl NoRowsDiv => NoRows;

        protected override HtmlGenericControl TableDiv => mainTableDiv;

        protected override HtmlInputText SearchControl => searchTermBox;

        protected override string ContentName => resources.LNG_OBJECTS;

        #endregion
    }
}