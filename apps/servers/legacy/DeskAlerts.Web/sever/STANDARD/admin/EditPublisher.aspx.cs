﻿using DeskAlerts.Server.Utils;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using DeskAlerts.ApplicationCore.Utilities;
using DeskAlertsDotNet.Parameters;
using Resources;
using DeskAlertsDotNet.sever.STANDARD.Utils;
using DeskAlertsDotNet.Utils.Factories;

namespace DeskAlertsDotNet.Pages
{
    public partial class EditPublisher : DeskAlertsBasePage
    {
        // use curUser to access to current user of DA control panel
        // use dbMgr to access to database
        private string _id;
        private string _selectedPolicyId;

        private const string  adminName = "admin";

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            _id = Request["id"];

            var policySet = dbMgr.GetDataByQuery("SELECT id, name FROM policy");

            foreach (var row in policySet)
            {
                policySelector.Items.Add(new ListItem(row.GetString("name"), row.GetString("id")));
            }

            if (!string.IsNullOrEmpty(_id))
            {
                workHeader.InnerText = resources.LNG_EDIT_EDITOR;

                var userSet = dbMgr.GetDataByQuery("SELECT u.name  FROM users as u  WHERE u.id = " + _id + " AND u.role !='U'");
                nameField.Value = userSet.GetString(0, "name");

                var usersPolicySet = dbMgr.GetDataByQuery("SELECT policy_id FROM policy_editor WHERE  editor_id=" + _id);

                if (!usersPolicySet.IsEmpty)
                {
                    policySelector.Items.FindByValue(usersPolicySet.GetString(0, "policy_id")).Selected = true;
                    _selectedPolicyId = usersPolicySet.GetString(0, "policy_id");
                }

                acceptButton.Text = resources.LNG_SAVE;
                passwordFieldRow.Style.Add(HtmlTextWriterStyle.Display, "none");
            }
            else
            {
                notChangeLink.Style.Add(HtmlTextWriterStyle.Display, "none");
                workHeader.InnerText = resources.LNG_ADD_EDITOR;
                changePasswordField.Style.Add(HtmlTextWriterStyle.Display, "none");
                acceptButton.Text = resources.LNG_ADD;
            }

            acceptButton.Click += acceptButton_Click;
            cancelButton.Click += cancelButton_Click;
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("PublisherList.aspx", true);
        }

        private void acceptButton_Click(object sender, EventArgs e)
        {
            var requerstPublisherName = Request["nameField"];

            if (string.IsNullOrEmpty(_id))
            {
                if (IsUserExist(requerstPublisherName))
                {
                    DeskAlertsUtils.ShowJavaScriptAlert(Response, "User with that name already exists");
                    return;
                }

                if (string.IsNullOrEmpty(passwordField.Value))
                {
                    DeskAlertsUtils.ShowJavaScriptAlert(Response, "Password must not be empty");
                    return;
                }

                Logger.Info($"User \"{curUser.Name}\" save new publisher with name \"{requerstPublisherName} \"");
            }
            else
            {
                // Get publisher name from database.
                var publisherNameQuery = $"SELECT name FROM users WHERE id={_id}";
                var publisherName = dbMgr.GetScalarByQuery<string>(publisherNameQuery);

                if (IsUserExist(requerstPublisherName) && requerstPublisherName != publisherName)
                {
                    DeskAlertsUtils.ShowJavaScriptAlert(Response, "User with that name already exists");
                    return;
                }

                if (requerstPublisherName != publisherName)
                {
                    Logger.Info($"User \"{curUser.Name}\" changed the field \"Publisher name\" from  \"{publisherName}\" to  \"{requerstPublisherName} \" ");
                }
            }

            if (!PublisherNameIsValid(requerstPublisherName))
            {
                publisherEditValidationErrors.InnerText = "Publisher name can not be 'admin'";
                return;
            }

            var publisherPolicyIdQuery = $"SELECT policy_id FROM policy_editor WHERE editor_id ='{_id}'";
            var policyId = Request.GetParam("policySelector", 0);
            var requestPolicyId = dbMgr.GetScalarByQuery(publisherPolicyIdQuery) != null
                                         ? dbMgr.GetScalarByQuery(publisherPolicyIdQuery).ToString()
                                         : policyId.ToString();

            if (int.Parse(requestPolicyId) != policyId)
            {
                var oldPolicyQuery = $"SELECT name FROM policy WHERE id='{requestPolicyId}'";
                var oldPolicy = dbMgr.GetScalarByQuery(oldPolicyQuery).ToString();

                var newPolicyQuery = $"SELECT name FROM policy WHERE id='{policyId}'";
                var newPolicy = dbMgr.GetScalarByQuery(newPolicyQuery).ToString();

                Logger.Info($"User \"{curUser.Name}\" changed the field \"Policy\" from \"{oldPolicy}\" to \"{newPolicy}\" for publisher \"{requerstPublisherName}\"");
            }

            if (policyId.ToString() != _selectedPolicyId && Settings.Content["ConfTokenAuthEnable"] == "1")
            {
                var tokensTools = new TokenTools();
                tokensTools.DeleteUserTokens(Convert.ToInt32(_id));
            }

            int publisherId;
            SqlParameter publisherParam;

            if (!string.IsNullOrEmpty(_id))
            {
                var userParam = SqlParameterFactory.Create(DbType.String, requerstPublisherName, "user");

                publisherId = Convert.ToInt32(_id);
                publisherParam = SqlParameterFactory.Create(DbType.Int32, publisherId, "publisherid");

                if (Request["keepPassword"] == "1")
                {
                    var updateQuery = $"UPDATE users SET name = @user , role=\'E\' WHERE id={publisherId}";
                    dbMgr.ExecuteQuery(updateQuery, userParam);
                }
                else
                {
                    if (string.IsNullOrEmpty(passwordField.Value))
                    {
                        DeskAlertsUtils.ShowJavaScriptAlert(Response, "Password must not be empty");
                        return;
                    }

                    var passwordParam = SqlParameterFactory.Create(DbType.String, HashingUtils.MD5(passwordField.Value), "password");
                    var updateQuery = $"UPDATE users SET name = @user , pass= @password , role=\'E\' WHERE id={publisherId}";
                    dbMgr.ExecuteQuery(updateQuery, userParam, passwordParam);
                    Logger.Info($"User \"{curUser.Name}\" changed the field \"Password\" for publisher  \"{requerstPublisherName}\" ");
                }
            }
            else
            {
                var userParam = SqlParameterFactory.Create(DbType.String, requerstPublisherName, "user");
                var dateParam = SqlParameterFactory.Create(DbType.DateTime, DateTime.Now, "dateTimeNow");
                var changePasswprdQuery = $"INSERT INTO users(name, pass, role, reg_date) OUTPUT INSERTED.ID VALUES(@user, \'{HashingUtils.MD5(passwordField.Value)}\', \'E\', @dateTimeNow)";
                var pubIdObj = dbMgr.GetScalarByQuery(changePasswprdQuery, userParam, dateParam);

                publisherId = Convert.ToInt32(pubIdObj);
                publisherParam = SqlParameterFactory.Create(DbType.Int32, publisherId, "publisherid");

                const string widgetQuery = @"INSERT INTO widgets_editors (editor_id, widget_id, parameters, page) VALUES(@publisherid, '071954af-ad48-4630-bd1e-8193691fc9a1', '', 'D')
                                        INSERT INTO widgets_editors (editor_id, widget_id, parameters, page) VALUES(@publisherid, '8FAEB4CE-542E-40D4-97DA-AC15CDCD145D', '', 'S')";
                dbMgr.ExecuteQuery(widgetQuery, publisherParam);
            }

            var editorIdParam = SqlParameterFactory.Create(DbType.Int32, publisherId, "editorId");
            const string deleteEditorQuery = "DELETE FROM policy_editor WHERE editor_id = @editorId";
            dbMgr.ExecuteQuery(deleteEditorQuery, editorIdParam);

            var policyIdParam = SqlParameterFactory.Create(DbType.Int32, policyId, "policyId");
            var editorIdParam2 = SqlParameterFactory.Create(DbType.Int32, publisherId, "editorId2");
            const string addEditorQuery = "INSERT INTO policy_editor (policy_id, editor_id, was_checked) VALUES (@policyId, @editorId2, 1)";
            dbMgr.ExecuteQuery(addEditorQuery, policyIdParam, editorIdParam2);

            Response.Redirect("PublisherList.aspx", true);
        }

        private bool IsUserExist(string user)
        {
            var userParam = SqlParameterFactory.Create(DbType.String, user, "user");
            const string query = "SELECT id FROM users WHERE name = @user AND role ='E';";
            var publisherId = dbMgr.GetDataByQuery(query, userParam);
            return !publisherId.IsEmpty;
        }

        private static bool PublisherNameIsValid(string publisherName)
        {
            return !publisherName.ToLower(CultureInfo.InvariantCulture).Equals(adminName);
        }
    }
}