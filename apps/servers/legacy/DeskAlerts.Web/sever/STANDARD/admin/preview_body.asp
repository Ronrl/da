<!-- #include file="config.inc" -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<% 
Response.Expires = 0 

customJs = Request("custom_js")
customJsSource = ""
skinId = Request("skin_id")

if skinId <> "" then
	basePath = alerts_folder&"/admin/skins/"&skinId&"/version/"
else
	basePath = alerts_folder&"/admin/skins/default/version/"
end if

if customJs <> "" then
	Set fs=Server.CreateObject("Scripting.FileSystemObject")

	if skinId <> "" then
		customJsPath = "skins/"&skinId&"/version/"&customJs
	else
		customJsPath = "skins/default/version/"&customJs
	end if
	
	Set f=fs.OpenTextFile(Server.MapPath(customJsPath), 1)
	customJsSource = f.ReadAll
	f.Close
	Set f=Nothing
	Set fs=Nothing
end if


customJsSource = Replace(customJsSource,"file://",".",1,-1,1)

%>
<html>
<head>
<style>
body {background-color:#FFFFFF;overflow:auto}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<script type="text/JavaScript" language="JavaScript" src="jscripts/jquery/jquery.min.js"></script>
<script type='text/JavaScript' language='JavaScript' src='jscripts/json2.js'></script>
<script type='text/JavaScript' language='JavaScript'>var ext_props=JSON.parse($("<div/>").html("<%=jsEncode(Request("ext_props"))%>".replace(/&/g, '&amp;').replace(/"/g, '&quot;').replace(/'/g, '&#39;').replace(/</g, '&lt;').replace(/>/g, '&gt;')).text());</script>
<script type='text/JavaScript' language='JavaScript' src='external.asp'></script>
<script type='text/JavaScript' language='JavaScript'>
<%=customJsSource%>
</script>
<base href="<%=basePath%>"/>
<script type='text/JavaScript' language='JavaScript'>
$(document).ready(function()
{
	var Initial_Timeout_old = window.Initial_Timeout;
	if (typeof window.Initial_Timeout == 'function')
	{
		window.Initial_Timeout = function(indexes)
		{
			Initial_Timeout_old(indexes);
			var old_display = document.body.style.display;
			document.body.style.display = "none";
			setTimeout(function(){document.body.style.display = old_display;},0);
		}
		
		Initial("0");
	}
	else if (typeof Initial == 'function')
	{
		Initial("0");
		var old_display = document.body.style.display;
		document.body.style.display = "none";
		setTimeout(function(){document.body.style.display = old_display;},0);
	}
});
</script>
</head>
<body>
<%
Response.Write HtmlDecode(Request("alert_html"))
%>
</body>
</html>