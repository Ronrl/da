﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%
response.AddHeader "content-type", "text/html; charset=utf-8"
Response.CharSet = "UTF-8"
check_session()

uid = Session("uid")
if uid <>"" then
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>DeskAlerts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/style9.css" rel="stylesheet" type="text/css">
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script type="text/JavaScript" language="JavaScript" src="jscripts/json2.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/preview.js?v=9.2.1.31"></script>
<script language="javascript" type="text/javascript" src="functions.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/date.js"></script>
</head>
<script language="javascript" type="text/javascript">

    var settingsDateFormat = "<%=GetDateFormat()%>";
    var settingsTimeFormat = "<%=GetTimeFormat()%>";

    var responseDbFormat = "dd/MM/yyyy HH:mm:ss";
    var uiFormat = getUiDateTimeFormat(settingsDateFormat, settingsTimeFormat);

    $(function() {
    	$(".delete_button").button({
    });

    function openFeedback(text) {
    	$("#feedback_dialog").dialog({
    		width: 300,
    		height: 300,
    		modal: true,
    		autoOpen: false,
    		close: function() {
    			$("#feedback_p").remove();
    		}
    	});
    	$("#feedback_dialog").append("<p id='feedback_p'>" + text + "</p>");
    	$("#feedback_dialog").dialog('open');
    }

    $(document).ready(function() {
    	var width = $(".data_table").width();
    	var pwidth = $(".paginate").width();
    	if (width != pwidth) {
    		$(".paginate").width("100%");
    	}

    	
    });
   })
</script>
<%
    
   if(Request("offset") <> "") then 
	offset = clng(Request("offset"))
   else 
	offset = 0
   end if	

   if(Request("sortby") <> "") then 
	sortby = Request("sortby")
   else 
	sortby = "date DESC"
   end if	

'counting pages to display
   if(Request("limit") <> "") then 
	limit = clng(Request("limit"))
   else 
	limit=25
   end if

'  limit=10

	'--- rights ---
	set rights = getRights(uid, true, false, true, "feedback_val")
	gid = rights("gid")
	gviewall = rights("gviewall")
	gviewlist = rights("gviewlist")
	fb_arr = rights("feedback_val")
	'--------------

	if(fb_arr(2)="") then
		linkclick_str = "not"
	else
		linkclick_str = LNG_FEEDBACK
	end if

	SQL = "SELECT COUNT(id) as mycnt FROM feedbacks"
    Set RS = Conn.Execute(SQL)

	cnt=0

	do while Not RS.EOF
		if(Not IsNull(RS("mycnt"))) then cnt=cnt+clng(RS("mycnt"))
		RS.MoveNext
	loop

	RS.Close

  j=cnt/limit
  
  	SQL = "SELECT COUNT(id) as count FROM feedbacks WHERE readed=0"
	Set RS = Conn.Execute(SQL)
	count=0
	do while Not RS.EOF
		if(Not IsNull(RS("count"))) then count=count+clng(RS("count"))
		RS.MoveNext
	loop
	RS.Close
	
   

%><body style="margin:0" class="body">
<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	<tr>
	    <td>
	        <table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
		        <tr>
		            <td width="100%" height="31" class="main_table_title"><img src="images/menu_icons/feedback_20.png" alt="" width="20" height="20" border="0" style="padding-left:7px">
					<a href="feedback_show.asp" class="header_title" style="position:absolute;top:15px"><%=LNG_FEEDBACK %></a></td>
		        </tr>
		        <tr>
		            <td height="100%" valign="top" class="main_table_body">
		            <div style="margin:10px">
		            <span class="work_header"><%=LNG_VIEW_FEEDBACK %></span>
		            <span class="work_header" style="color: #3CB371; margin-left:20px; ">
		            <%
		            if (count > 0) then
		                  Response.Write CStr(count) & " " & LNG_NEW_FEEDBACK
                    end if 
                    %>
                    </span>
<%

if(cnt>0) then
    SQL = "UPDATE feedbacks SET readed=1 WHERE readed=0"
    Conn.Execute(SQL)
    
	page="feedback_show.asp?"
	name=LNG_FEEDBACK 
	Response.Write make_pages(offset, cnt, limit, page, name, sortby) 

	response.write "<br><A href='#' class='delete_button' onclick=""javascript: return LINKCLICK('"&linkclick_str&"');"">"&LNG_DELETE&"</a><br/><br/>"
	response.write "<form action='delete_objects.asp' name='delete_objs' id='delete_objs' method='post'><input type='hidden' name='objType' value='feedback'>"
%>
	<script type="text/javascript">
		function openFeedback(text) {
			$("#feedback_"+text).dialog({
				title: 'Full feedback text',
				width: 450,
				height: 300,
				modal: true/*,
				autoOpen: false,
				close: function() {
					$("#feedback_p").remove();
				}*/
			});
			//$("#feedback_dialog").append("<p id='feedback_p'>" + text + "</p>");
		}
	</script>
		<table width="100%" height="100%" cellspacing="0" cellpadding="3" class="data_table">
		    <tr class="data_table_title">
		    	<td width="10"><input type='checkbox' name='select_all' id='select_all' onclick='javascript: select_all_objects();'></td>
		        <td class="table_title" style="width:auto;"><%= sorting(LNG_CLIENT_FEEDBACK,"client", sortby, offset, limit, page) %></td>
		        <td class="table_title" style="width:60%;"><%=LNG_TEXT_FEEDBACK %></td>
		        <td class="table_title" style="width:auto;"><%=sorting(LNG_DATE_FEEDBACK,"date", sortby, offset, limit, page) %></td>
		    </tr>
<%

'show main table

	SQL = "SELECT * FROM feedbacks ORDER BY " & sortby
	Set RS = Conn.Execute(SQL)

	num=offset
	
	if(Not RS.EOF) then RS.Move(offset) end if	
	Do While Not RS.EOF
		num=num+1
		if((offset+limit) < num) then Exit Do end if
		if(num > offset AND offset+limit >= num) then
		strObjectID=RS("id")
		reqText = RS("text")
		truncText = Left(reqText,70)
		if truncText <> reqText then
			truncText = truncText & "..."
		end if
		%>
		<tr>
			<td><input type="checkbox" name="objects" value="<%=strObjectID%>"></td>
		    <td style="text-align: center;">
			    <% 
			        Response.Write CStr(RS("client"))
			    %>
		    </td>
		    <td style="overflow:hidden">
			    <% if reqText<>truncText then
					reqText = Replace(reqText, """", "\""")%>
			        <a href='#' onclick='openFeedback("<%=strObjectID%>")'><%=truncText%></a>
			        <div id="feedback_<%=strObjectID%>" style="display:none"><%=reqText %></div>
			        <%else%>
			        <%= reqText%>
			        <%end if%>
		    </td>
		    <td style="text-align: center;">
			    <script language="javascript">
			        document.write(getUiDateTime('<%=RS("date")%>'));
			    </script>
		    </td>
		</tr>
		<%
		end if
	RS.MoveNext
	Loop
	RS.Close
	
	%>

			</table>
<%
	response.write "<input type='hidden' name='back' value='feedback_show.asp'></form><br/><A href='#' class='delete_button' onclick=""javascript: return LINKCLICK('"&linkclick_str&"');"">"&LNG_DELETE&"</a><br><br>"
	Response.Write make_pages(offset, cnt, limit, page, name, sortby) 

else

Response.Write "<center><b>"&LNG_NO_FEEDBACK&"</b><br><br></center>"

end if
%>
		</div>
		</td>
		</tr>
	</table>
	</td>
	</tr>
</table>
<%
else
	Response.Redirect "index.asp"

end if
%>
<div id="feedback_dialog"></div>
</body>
</html>
<!-- #include file="db_conn_close.asp" -->