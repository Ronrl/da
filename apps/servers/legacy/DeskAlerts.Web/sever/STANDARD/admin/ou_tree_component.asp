<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%
check_session()

' Variables
mifTreeFolder = "mif_tree"
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	
	
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/mootools.js"></script>	
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Node.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Hover.js"></script>	
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Selection.js"></script>	
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Load.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Draw.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.KeyNav.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Sort.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Transform.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Drag.js"></script>	
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Drag.Element.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Checkbox.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Rename.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Row.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.mootools-patch.js"></script>
	
	<link rel="stylesheet" href="<%Response.write mifTreeFolder%>/style.css" type="text/css" />
	<script type="text/javascript">
		var tree;
		
		function getCheckedOus ()
		{			
			if( tree && tree != 'undefined' )
			{
				nodes = new Array();			

				tree.root.recursive
				(
					function()
					{
						if( this.state.checked == "checked" )
						{
							temp = new Object();
							nodes.push(temp);
							nodes[nodes.length-1].id = this.property.id;
							nodes[nodes.length-1].type = this.property.type;
							nodes[nodes.length-1].path = this.property.path;
						}
					}
				);
				return nodes;
			}
			return null;
		}
		
		window.addEvent('domready',function()
		{		
				tree = new Mif.Tree({
					container: $('tree_container'),
					forest: false,
					initialize: function(){
						this.initCheckbox('deps');
						new Mif.Tree.KeyNav(this);
					},					
					types: {
						organization:{
							openIcon: 'mif-tree-organization-icon',
							closeIcon: 'mif-tree-organization-icon'
						},
						domain:{
							openIcon: 'mif-tree-domain-icon',
							closeIcon: 'mif-tree-domain-icon'
						},
						ou:{
							openIcon: 'mif-tree-ou-icon',
							closeIcon: 'mif-tree-ou-icon'							
						}	
					},
					dfltType:'organization'
					
				});
				
				tree.initSortable();
				try
				{
				/*
					var sLocation = window.location;
					thePath = unescape(sLocation)					
					thePath=thePath.substr(0,thePath.lastIndexOf('/')+1);
					alert(thePath+"ou_get_tree.asp");
					loadXMLDoc(thePath+"get_ou_tree.asp");*/
					//loadXMLDoc("")
					
					tree.load({
						url: 'ou_get_tree.asp'
					//	url: '<%Response.write mifTreeFolder%>/mootools/forest.json'
					});
					
				}
				catch(e)
				{
					alert('Fail');
				}
				
				tree.loadOptions=function(node){
					// if node name 'empty' load from url 'empty.json'
					if(node.name=='empty'){
						return {
							url: '<%Response.write mifTreeFolder%>/mootools/empty.json'
						}
					}
					return {
						url: '<%Response.write mifTreeFolder%>/mootools/mediumTree.json'
					};
				}
		});
		//
		function send_to_users()
		{
			res = getCheckedOus();
			idSequence = "";			
			if( res )
			{
				for(i=0;i<res.length;i++)
				{
					if(res[i].type == "OU" )
						idSequence += res[i].id.toString() + ",";
				}
			}	
			alert(idSequence);
			popupWin = openDialogUI('form','frame',"ou_send_to_users.asp?ous_id_sequence="+idSequence,600,600,'');
        }

        function openDialogUI(_form, _frame, _href, _width, _height, _title) {
            $("#dialog-" + _form).dialog('option', 'height', _height);
            $("#dialog-" + _form).dialog('option', 'width', _width);
            $("#dialog-" + _form).dialog('option', 'title', _title);
            $("#dialog-" + _frame).attr("src", _href);
            $("#dialog-" + _frame).css("height", _height);
            $("#dialog-" + _frame).css("display", 'block');
            if ($("#dialog-" + _frame).attr("src") != undefined) {
                $("#dialog-" + _form).dialog('open');
                $("#dialog-" + _form).contents().find('#table_border').css('border', 'none');
            }
        }



        $(document).ready(function() {

            $("#dialog-form").dialog({
                autoOpen: false,
                height: 100,
                width: 100,
                modal: true
            });


            $('#dialog-frame').on('load', function() {
                $("#dialog-frame").contents().find('.prelink_stats').on('click', function(e) {
                    e.preventDefault();
                    var link = $(this).attr('href');
                    var title = $(this).attr('title');
                    var last_id = $('.dialog_forms').size();
                    var elem = '<div id="dialog-form-' + last_id + '" class="dialog_forms" style="overflow:hidden;display:none;"><iframe id="dialog-frame-' + last_id + '" style="display:none;width:100%;height:auto;overflow:hidden;border : none;"></iframe></div>';
                    $('#secondary').append(elem);
                    $("#dialog-form-" + last_id).dialog({
                        autoOpen: false,
                        height: 100,
                        width: 100,
                        modal: true
                    });
                    openDialogUI("form-" + last_id, "frame-" + last_id, link, 350, 350, title);
                });
            });
        });
		
		//
	</script>
</head>
<body>
<div id="dialog-form" style="overflow: hidden; display: none;">
        <iframe id='dialog-frame' style="width: 100%; height: 100%; overflow: hidden; border: none;
            display: none"></iframe>
    </div>
    <div id="secondary" style="overflow: hidden; display: none;">
    </div>
	<div id="tree_view_page_header">
		<div align="center"><a href="ad_sync_ou.asp" target="_self"><img src="images/sync_ous.png" alt="synchronize" border="0"></a></div>
	</div>	
	
	<table border="0px" width="100%" height="550">
	<tr>
		<td width="90%">			
			<div id="tree_container"></div>
		</td>
		<td width="10%">
			<div id="reciver_selector">
			
				<div align="center">				
						<img src="images/ou_send_to_users.png" alt="send to ous" border="0" onclick="send_to_users()">
				</div>
				
				<div align="center">
					<a href="ou_send_to_ous.asp" target="_self">
						<img src="images/ou_send_to_ous.png" alt="send to users" border="0">
					</a>
				</div>
			
			</div>
		</td>
	</tr>
	</table>
	
	
	<div id="tree_view_page_footer"></div>
</body>
</html>
<!-- #include file="db_conn_close.asp" -->