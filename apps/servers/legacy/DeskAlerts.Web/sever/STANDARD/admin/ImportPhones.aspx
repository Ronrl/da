﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImportPhones.aspx.cs" Inherits="DeskAlertsDotNet.Pages.ImportPhones" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title><%=Resources.resources.LNG_IMPORT_PHONES %></title>
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
    <style>
        .uploadCsvForm {
            padding: 10px;
            width: 100%;
            height: 100%;
            vertical-align: top;
        }

            .uploadCsvForm > div {
                margin-bottom: 1rem;
            }

            .uploadCsvForm .info {
                font-style: italic;
            }

                .uploadCsvForm .info input[type="submit"], .uploadCsvForm .info > p {
                    display: inline-block;
                }

        #loaderDiv {
            display: none;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <table style="width: 100%; height: 100%; border: 0; border-spacing: 0; padding: 6px;">
            <tr>
                <td>
                    <table style="width: 100%; height: 100%; border-spacing: 0; padding: 0;" class="main_table">
                        <tr>
                            <td style="height: 32px;" class="main_table_title">
                                <a runat="server" id="headerTitle" href="#" class="header_title"></a>
                            </td>
                        </tr>
                        <tr>
                            <td class="main_table_body uploadCsvForm">
                                <div class="info">
                                    <p><%=Resources.resources.LNG_IMPORT_PHONE_TEMPLATE_DOWNLOAD_INFO %></p>
                                    <asp:Button runat="server" ID="importPhoneTemplate" OnClick="ImportPhoneTemplate_Download" />
                                </div>
                                <div>
                                    <p>Select File &nbsp;<asp:FileUpload ID="FileUploader" runat="server" accept=".csv" /></p>
                                    <asp:Button ID="UploadButton" runat="server" OnClick="UploadButton_Click" />
                                </div>
                                <div>
                                    <asp:Label ID="Label1" runat="server"></asp:Label>
                                </div>
                                <div id="loaderDiv" runat="server">
                                    <asp:Label runat="server" ID="ImportingLabel" />
                                    <br />
                                    <img alt="progress" src="images/loader.gif" />
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
