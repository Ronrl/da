﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<!-- #include file="libs\templateObj.asp" -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>DeskAlerts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/style9.css" rel="stylesheet" type="text/css">
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css">
<link href="css/jquery/jquery_timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css">
<style>

	.ui-button-icon-only {font-size: 8px;}
	.ui-icon-pencil { float: left; margin: 0.4em 0.2em 0 0; cursor: pointer; }
</style>
<script language="javascript" type="text/javascript" src="jscripts/json2.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/date.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery_timepicker/jquery-ui-timepicker-addon.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/datepicker_fix.js"></script>
<script language="javascript" type="text/javascript" src="functions.js"></script>
<script language="javascript" type="text/javascript">
	var serverDate;
	var settingsDateFormat = "<%=GetDateFormat()%>";
	var settingsTimeFormat = "<%=GetTimeFormat()%>";

	var responseDbFormat = "dd/MM/yyyy HH:mm:ss";
	var saveDbFormat = "dd/MM/yyyy HH:mm";
	var saveDbDateFormat = "dd/MM/yyyy";
	var saveDbTimeFormat = "HH:mm";
	var uiFormat = getUiDateTimeFormat(settingsDateFormat,settingsTimeFormat);
	
	var tableParams = [];
	
	function fillTableParamDialog(id,data)
	{
		var tableParam = $.grep(tableParams, function(element,index){
			return element.id == id;
		})[0];
		
		var headers = tableParam.headers;
		var paramDialog = $("#param_tables_dialog");
		$(paramDialog).empty();
		var table = $("<table/>").appendTo(paramDialog);
		
		for (var i=0; i < headers.length; i++)
		{
			var tr = $("<tr/>").appendTo(table);
			var labelTd = $("<td/>").appendTo(tr);
			var inputTd = $("<td/>").appendTo(tr);
			var label = $("<label for='"+id+"_"+headers[i].name+"'>"+headers[i].name+"</label>").appendTo(labelTd);
			var input = $("<input id='"+id+"_"+headers[i].name+"' type='text'/>").appendTo(inputTd);
			if (data)
			{
				input.val(data[i]);
			}
		}
	}
	
	
	function makeTableParamField(param)
	{
		var tableDiv = $("<div id=\""+param.id+"_div\"/>");
		return tableDiv;
	}
	
	function createTableRow(param,data)
	{
		var row = $("<tr/>");
		//var dataTable = $("#"+id+" .data_table");
		var checkboxTd =$("<td/>").appendTo(row);
		var checkbox = $("<input class='table_param_checkbox' type='checkbox'/>").appendTo(checkboxTd);
		
		for (var i in data)
		{
			var td = $("<td class='data_cell'/>").appendTo(row);
			td.text(data[i]);
		}
		
		var actionTd =$("<td/>").appendTo(row);
		
		var editButton = $("<a><%=LNG_EDIT%></a>").appendTo(actionTd);
		
		editButton.button({
			icons: {
				primary: "ui-icon-pencil"
			},
			text: false
		}).click(function(){
		
			fillTableParamDialog(param.id,data);
			
			$("#param_tables_dialog").dialog({
				buttons : {
					"<%=LNG_SAVE%>" : function(){
					
						data = getParamTableDialogValues();
						var tds = $(row).find("td").filter('.data_cell');
						for (var i in data)
						{
							$(tds.get(i)).text(data[i]);
						}
						
						$(this).dialog("close");
					}
				}
			});
			$("#param_tables_dialog").dialog("open");
		});
		
		return row;
		
	}
	
	
	function getParamTableValues(paramId)
	{
		var values = [];
		$("* [paramId="+paramId+"]").find(".data_table tr").each(function(index, element){
			if (index > 0)
			{
				var value = {};
				$(element).find("td").filter(".data_cell").each(function(index, element){
					value[index] = $(element).text();
				});
				values.push(value);
			}
		});
		return values;
	}
	
	function getParamTableDialogValues()
	{
		var result = {};
		$("#param_tables_dialog input").each(function(index, element){
			result[index] = $(element).val();
		});
		return result;
	}
	
	function makeParamTable(param)
	{
		var table = $("<table paramId='"+param.id+"' style='border-collapse:collapse; margin:0px; padding:0px;'></table>");
		var dataRow = $("<tr/>").appendTo(table);
		var dataCell = $("<td colspan="+param.headers.length+" style='border:0px; margin:0px; padding:0px;'></td>").appendTo(dataRow);
		
		var dataTable = $("<table class='data_table'></table>").appendTo(dataCell);
		var head = $("<tr class='data_table_title' />").appendTo(dataTable);
		
		$("<td/>").appendTo(head);
		
		for (var i = 0; i < param.headers.length; i++)
		{
			var th = $("<td class='table_title'/>").appendTo(head);
			th.text(param.headers[i].name);	
		}
		
		$("<td/>").appendTo(head);
		
		var buttonRow = $("<tr></tr>").appendTo(table);
		var buttonPanel =$("<td colspan="+(param.headers.length+2)+" style='border:0px; margin:0px; padding:0px;  padding-top:5px; text-align:right'></td>").appendTo(buttonRow);
		var deleteButton = $("<a><%=LNG_DELETE%></a>").appendTo(buttonPanel);
		var addButton = $("<a><%=LNG_ADD%></a>").appendTo(buttonPanel);
		
		$(deleteButton).button().click(function(){
			var paramDialog = $("#param_tables_dialog");
			$(paramDialog).empty();
			var dialogText = $("<p></p>").appendTo(paramDialog);
			dialogText.text("<%=LNG_ARE_YOU_SURE_YOU_WANT_TO_DELETE_SELECTED%>?");
			$(paramDialog).dialog({
				buttons : {
					"<%=LNG_YES%>" : function(){
						$(dataTable).find(".table_param_checkbox").filter(":checked").closest("tr").remove();
						$(this).dialog("close");
					},
					"<%=LNG_NO%>" : function(){
						$(this).dialog("close");
					}
				}
			});
			$(paramDialog).dialog("open");
		});
		
		$(addButton).button().click(function(){
			fillTableParamDialog(param.id);
			$("#param_tables_dialog").dialog({
				buttons : {
					"<%=LNG_ADD%>" : function(){
						dataTable.append(createTableRow(param,getParamTableDialogValues()));
						$(this).dialog("close");
					}
				}
			});
			$("#param_tables_dialog").dialog("open");
		});
		
		
		return table;	
	}
	
	function drawTableParam(param)
	{
		var tableDiv = makeTableParamField(param);
		$(tableDiv).append(makeParamTable(param));
		
		var tableParamInput = $("#"+param.id)
		
		if (tableParamInput.val())
		{
			var data = JSON.parse(tableParamInput.val()) ;
			
			for (var i=0; i <data.length; i++)
			{
				$(tableDiv).find(".data_table").append(createTableRow(param,data[i]));
			}
		}
		
		$(tableDiv).insertBefore(tableParamInput);
	}
	
	function drawTableParams()
	{
		for (var i=0; i < tableParams.length; i++)
		{
			drawTableParam(tableParams[i]);
		}
	}

	$(function() {

	    serverDate = new Date(getDateFromAspFormat("<%=now_db%>", responseDbFormat));
	    setInterval(function() { serverDate.setSeconds(serverDate.getSeconds() + 1); }, 1000);

	    drawTableParams();

	    function checkFromCondition(form) {

	        var result = true;

	        var condition = $(form).attr("showCondition");

	        if (condition) {
	            try {
	                var params = getFormsParameters($(".param_form"));
	                var paramsString = "";
	                for (var i = 0; i < params.length; i++) {
	                    paramsString += "var " + params[i].paramName + " = \"" + escape(params[i].value) + "\"; "
	                }
	                eval(paramsString);
	                result = eval($(form).attr("showCondition"));
	            }
	            catch (e) {
	                result = false;
	            }

	        }

	        return result;
	    }

	    $(".next_button").button().click(function() {

	        var curOrder = parseInt($(this).attr("order"));

	        while (curOrder <= maxFormOrder) {
	            curOrder++;
	            var form = $("form[order='" + curOrder + "']");
	            if (form.length != 0) {
	                if (checkFromCondition($(form[0]))) {
	                    $("form").hide();
	                    $(form[0]).show();
	                    return;
	                }
	            }
	        }

	        if (validateParams()) {
	            saveParamAlert();
	        }
	    });

	    $(".close_button").button();

	    $("#param_tables_dialog").dialog({
	        title: '',
	        autoOpen: false,
	        modal: true,
	        resizable: false
	    });

	    $("#dialog").dialog({
	        title: 'Error',
	        autoOpen: false,
	        modal: true
	    });

	    $.each($(".date_time_param_input"), function(index, element) {

	        var dateValue = $.trim($(element).val());
	        if (dateValue != "" && isDate(dateValue, responseDbFormat)) {
	            $(element).val(formatDate(new Date(getDateFromFormat(dateValue, responseDbFormat)), uiFormat));
	        }

	    });

	    $.each($(".time_in_minutes_param_input"), function(index, element) {

	        var minutesValue = $.trim($(element).val());
	        if (minutesValue != "") {
	            var hours = Math.floor(minutesValue / 60);
	            var minutes = minutesValue % 60;

	            var date = new Date();

	            date.setHours(hours);
	            date.setMinutes(minutes);


	            $(element).val(formatDate(date, "HH:mm"));
	        }


	    });


	    initDateTimePicker($(".date_time_param_input"), settingsDateFormat, settingsTimeFormat);
	    $(".time_in_minutes_param_input").timepicker({
	        showButtonPanel: false
	    });
	});
	
	
	function getFormsParameters(originForm)
	{
		var params = [];
		$.each($(originForm).find("input,select"),function(index,element){
		
			var inputValue = $(element).val();
			
			if ($(element).attr("type") == "radio" && !($(element).is(":checked")))
			{
				return;
			}
			
			if ($(element).attr("type") == "checkbox" && !($(element).is(":checked")))
			{
				inputValue = $(element).attr("unchekedvalue");
			}
			
		
			
			//var $inputElement = $("<input type='hidden'/>").appendTo($tmpPostForm);
			
			//$inputElement.attr("name",$(element).attr("name"));
			
			
			if ($(element).hasClass("date_time_param_input"))
			{
				var inputDate = new Date(getDateFromFormat(inputValue,uiFormat));
				inputValue = formatDate(inputDate,saveDbFormat);
			}
			else if ($(element).hasClass("time_in_minutes_param_input"))
			{
				var inputTime = new Date(getDateFromFormat(inputValue,"HH:mm"));
				inputValue = inputTime.getHours() * 60 + inputTime.getMinutes();
			}
			else if ($(element).hasClass("table_param_input"))
			{
				inputValue = JSON.stringify(getParamTableValues($(element).attr("id")));
			}
			
			params.push({value: inputValue, name: $(element).attr("name"), paramName: $(element).attr("paramName") })
			
			//$inputElement.val(inputValue);
		});
		return params;
	}
	
	function createTempForm(originForm)
	{
		$("tmp_post_form").remove();
		
		var $tmpPostForm = $("<form method='POST'/>").appendTo(document.body);
			
		var params = getFormsParameters(originForm);
		
		for (var i = 0; i < params.length; i++)
		{
			var $inputElement = $("<input type='hidden'/>").appendTo($tmpPostForm);
			$inputElement.attr("name",params[i].name);
			$inputElement.val(params[i].value);
		}
		
		return $tmpPostForm; 
	}
	
	function validateParams()
	{
		var $minuteTimes = $(".time_in_minutes_param_input");
		var result = true;
		$minuteTimes.each(function(index,element){
			
			var value = $.trim($(element).val());
			
			if (!isDate(value,"HH:mm"))
			{
				$("#dialog_text").text("Time is incorrect!");
				$("#dialog").dialog("open");
				result = false;
			}
		});

		
		if ($(".date_time_param_input").val()=="")
		{
			result = false;
			$("#dialog_text").text("Date must not be empty");
			$("#dialog").dialog("open");
		}
		
		return result;
	}
	
	function saveParamAlert()
	{
		var $submitForm = createTempForm($(".param_form"));
		$submitForm.submit();	
	}
</script>
</head>
<body style="margin:0" class="body">
<script language="JScript" src="jscripts/json2.js" runat="server"></script>
<script language="JScript" runat="server">


	function createTableParam(values,headers)
	{
		
		var headers = JSON.parse(headers);
		var values = JSON.parse(values);
		
		var style = "<style>.table_param {border: 1px solid black; border-collapse:collapse} .table_param_header_cell{background: #cccccc; border: 1px solid black} .table_param_data_cell{border: 1px solid black}</style>"
		var tableHTML = style + "<table class='table_param'><tr>";

		for (var i = 0; i < headers.length; i++)
		{
			tableHTML += "<td class='table_param_header_cell'>"
			tableHTML += headers[i].name;
			tableHTML += "</td>";
		}	
		
		tableHTML += "</tr>";
		
		for (var i = 0; i < values.length; i++)
		{
			tableHTML += "<tr>";
			for (var j = 0; j < headers.length; j++)
			{
				tableHTML += "<td class='table_param_data_cell'>"
				tableHTML += values[i][j];
				tableHTML += "</td>";
			}
			tableHTML += "</tr>";
		}	
		
		tableHTML += "</table>";
		
		return tableHTML;
	}
</script>

<%

Function Val( myString )
' Val Function for VBScript (aka ParseInt Function in VBScript).
' By Denis St-Pierre.
' Natively VBScript has no function to extract numbers from a string.
' Based shamelessly on MS' Helpfile example on RegExp object.
' CAVEAT: Returns only the *last* match found
'         (or, with objRE.Global = False, only the *first* match)

	Dim colMatches, objMatch, objRE, strPattern

	' Default if no numbers are found
	Val = 0

	strPattern = "[-+0-9]+"       ' Numbers positive and negative; use
	                              ' "ˆ[-+0-9]+" to emulate Rexx' Value()
	                              ' function, which returns 0 unless the
	                              ' string starts with a number or sign.
	Set objRE = New RegExp        ' Create regular expression object.
	objRE.Pattern    = strPattern ' Set pattern.
	objRE.IgnoreCase = True       ' Set case insensitivity.
	objRE.Global     = True       ' Set global applicability:
	                              '   True  => return last match only,
	                              '   False => return first match only.
	Set colMatches = objRE.Execute( myString )  ' Execute search.
	For Each objMatch In colMatches             ' Iterate Matches collection.
	    Val = objMatch.Value
	Next
	Set objRE= Nothing
End Function

function floor(x)
	dim temp

	temp = Round(x)

	if temp > x then
		temp = temp - 1
	end if

	floor = temp
end function

function convertTimeToMinutes(curValue)
	minutesTime =  Split(curValue,":")
	hours = minutesTime(0)
	minutes = minutesTime(1)
	minutesTime = CLng(Val(hours)) * 60 + CLng(Val(minutes))
	convertTimeToMinutes = minutesTime
end function

function updateAlert(id,alertText, alertTitle, fromDate, toDate, schedule, scheduleType, recurrence, urgent, email, desktop, sms, emailSender, ticker, fullscreen, alertWidth, alertHeight, aknown, autoclose, toolbarmode, deskalertsmode, template, uid, paramTemplateId)
	sqlRequest = "UPDATE alerts SET alert_text=N'" + alertText + "', title=N'"&alertTitle&"', type='D', from_date='"&fromDate&"', to_date='"&toDate&"', schedule='"&schedule&"', schedule_type='"&scheduleType&"', recurrence='"&recurrence&"', urgent='"&urgent&"', email='"&email&"', desktop='"&desktop&"', sms='"&sms&"', email_sender='"&emailSender&"', ticker='"&ticker&"', fullscreen='"&fullscreen&"', alert_width="&alertWidth&",  alert_height="&alertHeight&", aknown='"&aknown&"',  autoclose="&autoclose&",  toolbarmode='"&toolbarmode&"', deskalertsmode='"&deskalertsmode&"', template_id="&template&", sender_id="&uid&", param_temp_id="&paramTemplateId&" WHERE id = " & Clng(id)
	Set RS_alert_update = Conn.Execute(sqlRequest)
	updateAlert = id
end function

function addAlertToDataBase(alertText, alertTitle, fromDate, toDate, schedule, scheduleType, recurrence, urgent, email, desktop, sms, emailSender, ticker, fullscreen, alertWidth, alertHeight, aknown, autoclose, toolbarmode, deskalertsmode, template, uid, paramTemplateId)
	sqlRequest = "INSERT INTO alerts (alert_text, title, create_date, type, from_date, to_date, schedule, schedule_type, recurrence, urgent, email, desktop, sms, email_sender, ticker, fullscreen, alert_width,  alert_height, aknown,  autoclose,  toolbarmode, deskalertsmode, template_id, sender_id, param_temp_id, class) VALUES (N'" + alertText + "', N'"&alertTitle&"', GETDATE(), 'D', '"&fromDate&"','"&toDate&"','"&schedule&"', '"&scheduleType&"', '"&recurrence&"', '"&urgent&"', '"&email&"', '"&desktop&"', '"&sms&"', '"&emailSender&"',  '"&ticker&"', '"&fullscreen&"', "&alertWidth&",  "&alertHeight&", '"&aknown&"', "&autoclose&", '"&toolbarmode&"', '"&deskalertsmode&"',"&template&", "&uid&","&paramTemplateId&",1); select @@IDENTITY as 'newID'"
	Set RS_alert = Conn.Execute(sqlRequest)
	Set RS_alert  = RS_alert.NextRecordset
	addAlertToDataBase=RS_alert("newID")
	RS_alert.Close
end function

templateGuid = "23d8575a-9eb8-41ee-9f0a-e3a11dce3fc6"

uid = Session("uid")
templateId = Request("temp_id")


if (uid <>"") then

		alertId = Request("alert_id")
		SQL = ""
		
		set params = param("values")

		if (params.count > 0) then
	
			Set RS = Conn.Execute("select p.id, p.short_name, p.type, p.action, s.value as select_value from alert_parameters p LEFT JOIN alert_param_selects s ON s.param_id = p.id WHERE p.temp_id ='"&templateId&"' AND (s.[order] is NULL or s.[order]=0)")
			LoadTemplateFromString  "alert_template"&templateGuid, Request("alert_content")
			
			Set actionsDictionary = Server.CreateObject("Scripting.Dictionary")
			while not RS.EOF
				paramValue = params(""&RS("id")&"")
				'if (RS("action") = "9") then
				'	if (RS("type") = "12") then
				'		paramValue = convertTimeToMinutes(paramValue)
				'	end if
				'end if
				
				if (VarType(RS("action"))>1) then
					if not actionsDictionary.Exists(RS("action")) then
						actionsDictionary.Add RS("action")&"", paramValue&""
					end if
				end if
				
				if (RS("type") = "5") then
					paramValue = createTableParam(paramValue,RS("select_value").Value)
				end if
				
				LoadTemplateFromString RS("short_name")&"", paramValue&""
				RS.MoveNext
			wend
			
			if (actionsDictionary.exists("0")) then 
				alertTitle = actionsDictionary("0")
			end if 
			if (actionsDictionary.exists("7")) then
				startDate = actionsDictionary("7")
				if (actionsDictionary.exists("9")) then
					startDate = DateAdd("n",actionsDictionary("9"),startDate)
				end if
			elseif (actionsDictionary.exists("9")) then
				startDate = DateAdd("n",actionsDictionary("9"),now_db)
			end if
			
			if (actionsDictionary.exists("11")) then 
				stepsCount = actionsDictionary("11")
			end if 
			
			Parse "alert_template"&templateGuid, False
			
			alertContent = PrintVar("alert_template"&templateGuid)
			
			UnloadTemplate
			
			alertContent = Replace(alertContent, "'", "''")
			
			if (alertId = "") then
				alertId = addAlertToDataBase(alertContent,alertTitle,startDate,"",1,1,0,0,0,1,1,"",0,0,500,400,0,0,0,1,1,uid,"'"&templateId&"'")
			else
				alertId = updateAlert(alertId,alertContent,alertTitle,startDate,"",1,1,0,0,0,1,1,"",0,0,500,400,0,0,0,1,1,uid,"'"&templateId&"'")
				Conn.Execute("DELETE FROM alerts_received WHERE alert_id="&alertId)
			end if 
			RS.MoveFirst
			while not RS.EOF
						
				if (alertId <> "") then
					SQL = SQL & " DELETE FROM alert_param_values WHERE (param_id='"&RS("id")&"' AND alert_Id = "&alertId&")"
				end if
				
				SQL = SQL & " INSERT INTO alert_param_values (value, param_id, alert_id) VALUES ('" & Replace(params(""&RS("id")&""), "'", "''") & "', '"&RS("id")&"', "&alertId&")"

				RS.MoveNext
			wend
			SQL = "BEGIN TRAN T1; "&SQL&"; COMMIT TRAN T1"
			Conn.Execute(SQL)
			RS.Close
			
			redir="alert_users.asp?desktop=1&sms=1&id="& CStr(alertId)&"&return_page=alert_parametrized.asp?temp_id="&templateId&"&cur_step="&stepsCount&"&steps_count="&stepsCount
			'Response.end
			Response.Redirect redir
			
		end if
		Set RS_temp = Conn.Execute("SELECT id, name, html FROM alert_param_templates WHERE id ='"&templateId&"'")
		%>
		<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
			<tr>
			<td>
			<table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
				<tr>
				<td  height=31 class="main_table_title"><a href="alert_parametrized_edit.asp?temp_id=<%=templateId %>" class="header_title"><%=RS_temp("name")%></a></td>
				</tr>
				<tr>
				<td style="padding:5px; width:100%" height="100%"   valign="top" class="main_table_body">
					<div style="margin:10px;">
		<%
		
		templateName = RS_temp("name")
		alertContent = RS_temp("html")
		
		SQL = "SET NOCOUNT ON" &_
		" SELECT p.id, p.type, p.name as param_name, p.short_name, s.name, s.value, p.[default], s.[order]"
		
		if alertId<>"" then
			SQL = SQL & ", v.value as cur_value"
		else
			SQL = SQL & ", NULL as cur_value"
		end if
			SQL = SQL &_
				" FROM alert_parameters p" &_
				" LEFT JOIN alert_param_selects s" &_
				" ON p.id=s.param_id"
		if alertId<>"" then
			SQL = SQL &_
				" LEFT JOIN alert_param_values v" &_
				" ON v.param_id = p.id AND v.alert_id ="&Clng(alertId)
		end if
		
		SQL =SQL & " ORDER BY [order] "
		
		Set RS = Conn.Execute(SQL)
			
		RS_temp.Close
			
		formsSql = "SELECT id, name, html, [order], show_condition, next_button_name, previous_button_name, close_button_name, param_temp_id" &_
					" FROM alert_param_template_forms" &_
					" WHERE param_temp_id = '"& Replace(templateId, "'", "''") &"'" &_
					" ORDER BY [order]"
					
		Set RS_forms = Conn.Execute(formsSql)
		
		i = 0
		
		while not RS_forms.EOF
		
			
			LoadTemplateFromString  templateName & RS_forms("name") & templateGuid, RS_forms("html")
			
			while not RS.EOF
				shortName = RS("short_name")
				
				curValue = ""
				if (RS("cur_value")<>"") then
					curValue = HtmlEncode(RS("cur_value")&"")
				else 
					curValue = HtmlEncode(RS("default")&"")
				end if
				
				
				
				if (RS("type")=2) then
				
					if VarType(GetVar(shortName)) <= 1 then			
						LoadTemplateFromString shortName, "<select paramName="""&shortName&""" name=""values['{"&shortName&templateGuid&"_id}']""><!--Begin"&shortName&templateGuid&"list--><option value=""{"&shortName&templateGuid&"_value}""{"&shortName&templateGuid&"_selected}>{"&shortName&templateGuid&"_name}</option><!--End"&shortName&templateGuid&"list--></select>"
						SetVar shortName&templateGuid&"_id", RS("id")
					end if
					if RS("value") = curValue then
						SetVar shortName&templateGuid&"_selected", " selected=""selected"""
					else
						SetVar shortName&templateGuid&"_selected", ""
					end if
					SetVar shortName&templateGuid&"_name", HtmlEncode(RS("name")&"")
					SetVar shortName&templateGuid&"_value", HtmlEncode(RS("value")&"")
					Parse shortName&templateGuid&"list", True
					'Parse shortName, True
					
				elseif (RS("type") = 3) then
				
					if VarType(GetVar(shortName)) <= 1 then
						LoadTemplateFromString shortName, "<!--Begin"&shortName&templateGuid&"list--><input id="""&shortName&"_{"&shortName&templateGuid&"_name}"&""" paramName="""&shortName&""" type=""radio"" value=""{"&shortName&templateGuid&"_value}"" name=""values['"&RS("id")&"']"" {"&shortName&templateGuid&"_selected}></input><label for="""&shortName&"_{"&shortName&templateGuid&"_name}"&""">{"&shortName&templateGuid&"_name}</label><!--End"&shortName&templateGuid&"list-->"
						SetVar shortName&templateGuid&"_id", RS("id")
					end if
					if RS("value") = curValue then
						SetVar shortName&templateGuid&"_selected", "checked"
					else
						SetVar shortName&templateGuid&"_selected", ""
					end if
					SetVar shortName&templateGuid&"_name", HtmlEncode(RS("name")&"")
					SetVar shortName&templateGuid&"_value", HtmlEncode(RS("value")&"")
					Parse shortName&templateGuid&"list", True					
				else
				
					labelHtml = ""
					valueInputHtml = "<input id="""&shortName&""" paramName="""&shortName&""" name=""values['"&RS("id")&"']"""
					if  (RS("type") = 6) then 'date_time type
						convertRS = Conn.Execute("if ('"&curValue&"'<>'') SELECT CONVERT(DATETIME,'"&curValue&"') AS value ELSE SELECT '"&curValue&"' as value")
						curValue = HtmlEncode(convertRS("value"))
						valueInputHtml = valueInputHtml&" class='date_time_param_input'"
					end if
					
					if  (RS("type") = 12) then 'date_time type
						valueInputHtml = valueInputHtml&" class='time_in_minutes_param_input'"
					end if
					
					if (RS("type") = 4) then
						if (RS("order") = "1") then
							if RS("value") = curValue then
								valueInputHtml = valueInputHtml& " checked "
							end if 
							curValue = HtmlEncode(RS("value"))
						elseif (RS("order") = "2") then
							valueInputHtml = valueInputHtml&" unchekedvalue='"&HtmlEncode(RS("value"))&"'"
						end if
						labelHtml = "<label for="""&shortName&""">"&HtmlEncode(RS("param_name")&"")&"</label>"
						valueInputHtml = valueInputHtml&" type=""checkbox"""
					end if
					
					if (RS("type") = 5) then
						headers = RS("value")
						valueInputHtml = valueInputHtml&" type=""hidden"""
						valueInputHtml = valueInputHtml&" class='table_param_input'"
						scriptHtml = scriptHtml & "<scr"&"ipt language=""javascript"" type=""text/javascript"">" &_
										"var headers = JSON.parse(""" & JsEncode(headers) & """);"&_
										"tableParams.push({" &_
											"""id"" : """&JsEncode(shortName)&"""," &_
											"""headers"" : headers" &_
										"})"&_
									"</scr"&"ipt>"
					end if
					
					if (RS("type") = 11) then 'hidden type
						valueInputHtml = valueInputHtml&" type=""hidden"""
					else
						valueInputHtml = valueInputHtml&" type=""text"""
					end if
					
					valueInputHtml = valueInputHtml&" value="""&curValue&"""/>"
					valueInputHtml = valueInputHtml & labelHtml
					
					SetVar shortName, valueInputHtml
					
				end if
				RS.MoveNext
			Wend
			
			formOrder = RS_forms("order")
			
			SetVar "next-button", "<a class='next_button' order='" & formOrder & "' >" & LNG_NEXT & "</a>"
			SetVar "close-button", "<a class='close_button' href='alert_parametrized.asp?temp_id=" & templateId & "' order='" & formOrder & "' >" & LNG_CLOSE & "</a>"

			Parse templateName & RS_forms("name") & templateGuid, False

			%>
				<form class="param_form" name="<%=RS_forms("name")%>" order="<%=formOrder%>" showCondition="<%=HtmlEncode(RS_forms("show_condition"))%>" <%if i<>0 then Response.Write "style=""display:none""" end if%>>
			<%
			Response.Write PrintVar(templateName & RS_forms("name") & templateGuid)
			
			%>
				</form id="">
			<%
			
			i = i + 1
			
			
			
			RS_forms.MoveNext
		Wend
		Response.write scriptHtml
		%>
			<script language="javascript" type="text/javascript">
				var maxFormOrder = <%=formOrder%>;
			</script>
		<%
		RS_forms.Close
		UnloadTemplate
		
		%>
			<form class="param_form">
				<input name="temp_id" type="hidden" value="<%=templateId%>"/>
				<input name="alert_content" type="hidden" value="<% Response.Write HtmlEncode(alertContent) %>"/>
			</form>
		<% 
		'if (alertId<>"") then saveButton = "save.gif" else saveButton = "add.gif" end if
		'Response.Write "<A style='margin:10px' href='#' onclick=""javascript: saveParamAlert()""><img src='images/langs/"&default_lng &"/"&saveButton&"' border='0'/></a>" 
		RS.Close
		
		%>
			</div>
		</td>
	</tr>
</table>
<div class="ui-state-error" id="dialog" style="display:none;"> 
	<p id="dialog_text" ></p> 
</div>

<div id="param_tables_dialog" style="display:none;"> 
</div>
<%
else
  Response.Redirect "index.asp"
end if
		
%>


</body>

</html>