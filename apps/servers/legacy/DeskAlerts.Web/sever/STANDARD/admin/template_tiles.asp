<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%
check_session()
%>
<!-- #include file="Common.asp" -->
<!-- #include file="header.asp" -->
<!-- #include file="footer.asp" -->
<!-- #include file="libs/FusionCharts.asp" -->

<%
'On Error Resume Next
' index CustomIncludes End
'-------------------------------

'===============================
' Save Page and File Name available into variables
'-------------------------------
Dim sFileName
Dim sTemplateFileName
Dim sPaginateFileName
Dim intSessionUserId

sFileName = "template_tiles.asp"
sTemplateFileName = "template_tiles.html"
sPaginateFileName = "paginate.html"
'===============================
  

intSessionUserId = Session("uid")
uid=intSessionUserId

if (intSessionUserId <> "") then


' index Show begin

'===============================
' Display page
'-------------------------------
' Load HTML template for this page
'-------------------------------
LoadTemplate alerts_dir & "\" & templateFolder & "\" & sTemplateFileName, "main"
SetVar "DListTiles", ""
'-------------------------------
' Load HTML template of Header and Footer
'-------------------------------
LoadTemplate sHeaderFileName, "Header"
LoadTemplate sFooterFileName, "Footer"

'-------------------------------

LoadTemplate sMenuFileName, "Menu"

'---------------------------------


if(DA=1) then
else
	SetVar "strLogout", "Logout"
end if

SetVar "strPageName", LNG_SELECT_SKIN
SetVar "strMenu", ""
SetVar "curTime", now_db
SetVar "LNG_TEMPLATES", LNG_TEMPLATES
SetVar "LNG_DEFAULT", LNG_DEFAULT
SetVar "editAlertPage", "edit_alert_db.asp"

page = sFileName
name = LNG_TEMPLATES

Set RS = Conn.Execute("SELECT id, name FROM alert_captions")
If RS.EOF then
	Response.Redirect "edit_alert_db.asp"
Else
	dim fs,f,path
	set fs = Server.CreateObject("Scripting.FileSystemObject")
	
	path = Server.MapPath("skins\default\thumbnail.png")
	if fs.FileExists(path) then
		set f = fs.GetFile(path)
		SetVar "fileTimestamp", CStr(CDbl(f.DateLastModified))
	else
		SetVar "fileTimestamp", ""
	end if
	
	SetVar "folderName", "default"
	SetVar "tileName", LNG_DEFAULT
	SetVar "tileId", ""
	Parse "DListTiles", (Not RS.EOF)

	Do While Not RS.EOF
		path = Server.MapPath("skins\"& RS("id") &"\thumbnail.png")
		if fs.FileExists(path) then
			set f = fs.GetFile(path)
			SetVar "fileTimestamp", CStr(CDbl(f.DateLastModified))
		else
			SetVar "fileTimestamp", ""
		end if

		SetVar "folderName", RS("id")
		SetVar "tileName",  RS("name")
		SetVar "tileId",  RS("id")
		Parse "DListTiles", True
		RS.MoveNext
	Loop
End If
RS.Close

'-------------


'-------------------------------
' Step through each form
'-------------------------------
Header_Show
Footer_Show
'-------------------------------
' Process page templates
'-------------------------------
Parse "Header", False
Parse "Footer", False
Parse "main", False
'-------------------------------
' Output the page to the browser
'-------------------------------
Response.write PrintVar("main")

' index Show End

'-------------------------------
' Destroy all object variables
'-------------------------------

' index Close Event begin
' index Close Event End

UnloadTemplate
'===============================

else

Login(Request("intErrorCode"))

end if

'===============================
' Display Grid Form
'-------------------------------
%>
<!-- #include file="db_conn_close.asp" -->

