using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using DeskAlerts.ApplicationCore.Enums;
using DeskAlerts.Server.Utils;
using DeskAlerts.Server.Utils.Helpers;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Parameters;
using Resources;

namespace DeskAlertsDotNet.Pages
{
    public partial class SurveyStatisticDetails : DeskAlertsBasePage
    {
        //use curUser to access to current user of DA control panel      
        //use dbMgr to access to database
        protected string alertId;
        protected string sendAlertId;
        protected string alertsColors = "";
        protected string alertsValues = "";

        protected string answersColors = "";
        protected string answersValues = "";
        private bool isAnonymousSurvey;
        protected bool isSentToIpGroups;
        private const string AnonymousUserName = "Anonymous";

        protected override void OnLoad(EventArgs e)
        {
            try
            {
                alertId = Request["id"];
                isAnonymousSurvey = IsAnonymousSurvey(int.Parse(alertId));
                if (string.IsNullOrEmpty(Request["id"]))
                {
                    Response.End();
                    return;
                }

                base.OnLoad(e);



                if (string.IsNullOrEmpty(Request["sendAlertId"]))
                {

                    sendAlertId = dbMgr.GetScalarByQuery<string>("SELECT sender_id FROM surveys_main WHERE id=" + alertId);
                }
                else
                {
                    sendAlertId = Request["sendAlertId"];
                }

                DataRow alertRow = dbMgr.GetDataByQuery("SELECT * FROM alerts WHERE id IN (SELECT sender_id FROM surveys_main WHERE id =  " + alertId + ")").First();
                int totalRecipientsCount = 0;
                int recievedRecipientsCount = 0;
                int notRecievedRecipientsCount = 0;
                int votedCnt = 0;
                int notVotedCnt = 0;
                isSentToIpGroups = alertRow.GetString("type2") == "I";
                if (alertRow.GetString("type2") == "B")
                {
                    totalRecipientsCount =
                        dbMgr.GetScalarQuery<int>("SELECT count(1) as mycnt FROM users where email is not NULL");
                }
                else if (alertRow.GetString("type2") == "R")
                {
                    string query = "SELECT SUM(mycnt) FROM (";
                    query +=
                        "SELECT COUNT(DISTINCT user_id) as mycnt, 1 as mycnt1 FROM [alerts_sent_stat] WHERE alert_id= " +
                        sendAlertId;
                    query += " UNION ALL ";
                    query +=
                        "SELECT COUNT(DISTINCT user_id) as mycnt, 2 as mycnt1 FROM [archive_alerts_sent_stat] WHERE alert_id=" +
                        sendAlertId;
                    query += " UNION ALL ";
                    query +=
                        " SELECT COUNT(DISTINCT comp_id) as mycnt, 1 as mycnt1 FROM [alerts_sent_comp] WHERE alert_id= " +
                        sendAlertId;
                    query += " UNION ALL ";
                    query +=
                        "SELECT COUNT(DISTINCT comp_id) as mycnt, 2 as mycnt1 FROM [archive_alerts_sent_comp] WHERE alert_id=" +
                        sendAlertId;
                    query += " ) [count]";
                    totalRecipientsCount =
                     dbMgr.GetScalarQuery<int>(query);
                }

                var receivedUsersCountQuery = $@"SELECT COUNT(DISTINCT user_id) as total
                    FROM [alerts_received] 
                    WHERE alert_id = {sendAlertId}
                        AND status = {(int) AlertReceiveStatus.Received}";

                recievedRecipientsCount = dbMgr.GetScalarQuery<int>(receivedUsersCountQuery);

                DataSet surveySet = dbMgr.GetDataByQuery("SELECT id FROM surveys_main WHERE id = " + alertId);
                if (surveySet.IsEmpty)
                    throw new ArgumentException("Message with id = " + alertId + " is not survey");

                var surveyId = surveySet.GetString(0, "id");
                string votedQuery =
                    "SELECT COUNT(u.[user_id]) " +
                    "FROM " +
                        "(SELECT[as].[user_id] " +
                        "FROM surveys_questions AS q " +
                        "LEFT JOIN[surveys_answers] AS a ON q.id = a.question_id " +
                        "LEFT JOIN[surveys_answers_stat] AS[as] ON[as].answer_id = a.id " +
                        "WHERE q.survey_id = {0} AND[user_id] IS NOT NULL " +
                        "UNION " +
                        "SELECT cas.[user_id] " +
                        "FROM surveys_questions AS q " +
                        "LEFT JOIN[surveys_answers] AS a ON q.id = a.question_id " +
                        "RIGHT JOIN[surveys_custom_answers_stat] as cas ON cas.question_id = q.id " +
                        "WHERE q.survey_id = {0} AND[user_id] IS NOT NULL" +
                     ") AS u";
                votedQuery = string.Format(votedQuery, surveyId);
                votedCnt = dbMgr.GetScalarQuery<int>(votedQuery);

                if (totalRecipientsCount > 0)
                {
                    notRecievedRecipientsCount = totalRecipientsCount - recievedRecipientsCount;
                }
                notVotedCnt = recievedRecipientsCount - votedCnt;
                if (notVotedCnt < 0)
                    notVotedCnt = 0;

                string recievedColor = "'#3B6392'";
                string notRecievedColor = "'#A44441'";
                string votedColor = "'#9ABA59'";
                string notVotedColor = "'#9ABA59'";
                string valuesJsonTemplate = "[{0}]";
                string recievedKey = resources.LNG_RECEIVED;
                string notRecievedKey = resources.LNG_NOT_RECEIVED;
                string votedKey = resources.LNG_VOTED;
                string notVotedKey = resources.LNG_NOT_VOTED;
                firstLegend.Style.Add(HtmlTextWriterStyle.BackgroundColor, recievedColor.Replace("'", ""));
                firstLegendLabel.InnerText = recievedKey;
                if (recievedRecipientsCount == 0)
                    firstLegendValue.InnerHtml = "0";
                else
                {
                    string link = string.Format("<a href='AlertViewRecipients.aspx?type={0}&id={1}' title=''  class='prelink_stats'>{2}</a>",
                       "rec", sendAlertId, recievedRecipientsCount);
                    firstLegendValue.InnerHtml = Config.Data.HasModule(Modules.STATISTICS) ? link : Convert.ToString(recievedRecipientsCount);
                }

                alertsValues += string.Format(valuesJsonTemplate, recievedRecipientsCount);
                alertsColors = recievedColor;
                secondLegend.Style.Add(HtmlTextWriterStyle.BackgroundColor, notRecievedColor.Replace("'", ""));
                secondLegendLabel.InnerText = notRecievedKey;
                if (notRecievedRecipientsCount == 0)
                    secondLegendValue.InnerHtml = "0";
                else
                {
                    string link = string.Format("<a href='AlertViewRecipients.aspx?type={0}&id={1}' title='' class='prelink_stats'>{2}</a>",
                       "drec", sendAlertId, notRecievedRecipientsCount);
                    secondLegendValue.InnerHtml = Config.Data.HasModule(Modules.STATISTICS) ? link : Convert.ToString(notRecievedRecipientsCount);
                }

                if (alertsValues.Length > 0)
                {
                    alertsValues += ",";
                }
                alertsValues += string.Format(valuesJsonTemplate, notRecievedRecipientsCount);
                alertsColors += ",";
                alertsColors += notRecievedColor;
                thirdLegend.Style.Add(HtmlTextWriterStyle.BackgroundColor, votedColor.Replace("'", ""));
                thirdLegendLabel.InnerText = votedKey;
                if (votedCnt == 0)
                    thirdLegendValue.InnerHtml = "0";
                else
                {
                    if (isAnonymousSurvey)
                    {
                        thirdLegendValue.InnerHtml = $"<span class='prelink_stats'>{votedCnt}</span>";
                    }
                    else
                    {
                        thirdLegendValue.InnerHtml = Config.Data.HasModule(Modules.STATISTICS)
                                                         ? $"<a href='AlertViewRecipients.aspx?type=vot&id={sendAlertId}' title='' class='prelink_stats'>{votedCnt}</a>"
                                                         : Convert.ToString(votedCnt);
                    }
                }
                if (alertsValues.Length > 0)
                    alertsValues += ",";
                alertsValues += string.Format(valuesJsonTemplate, votedCnt);
                if (alertsColors.Length > 0)
                    alertsColors += ",";
                alertsColors += votedColor;
                //FOURTH
                if (alertsValues.Length > 0)
                    alertsValues += ",";
                alertsValues += string.Format(valuesJsonTemplate, notVotedCnt);
                if (alertsColors.Length > 0)
                    alertsColors += ",";

                alertsColors += notVotedColor;

                fourhLegend.Style.Add(HtmlTextWriterStyle.BackgroundColor, notVotedColor.Replace("'", ""));
                fourhLegendLabel.InnerText = notVotedKey;
                if (notVotedCnt == 0)
                    fourhLegendValue.InnerHtml = "0";
                else
                {
                    if (isAnonymousSurvey)
                    {
                        fourhLegendValue.InnerHtml = $"<span class='prelink_stats'>{notVotedCnt}</span>";
                    }
                    else
                    {
                        fourhLegendValue.InnerHtml = Config.Data.HasModule(Modules.STATISTICS)
                                                         ?
                            $"<a href='AlertViewRecipients.aspx?type=dvot&id={sendAlertId}' title='' class='prelink_stats'>{notVotedCnt}</a>" : Convert.ToString(notVotedCnt);
                    }
                }

                var totalNumberLink = Config.Data.HasModule(Modules.STATISTICS)
                               ? string.Format(
                                   "<a class='prelink_stats' title='' href='AlertRecipientsList.aspx?id={0}&sendAlertId={1}&widget={2}'>{3}</a>",
                                   Request["id"],
                                   sendAlertId,
                                   Request["widget"],
                                   totalRecipientsCount)
                               : Convert.ToString(totalRecipientsCount);
                totalRecipients.Text = isSentToIpGroups ? string.Empty : totalNumberLink;

                alertTitleLiteral.Text = alertRow.GetString("title");
                createDateLiteral.Text = DateTimeConverter.ToUiDateTime(alertRow.GetDateTime("create_date"));
                //Add your main code here

                int totalQuestions =
                    dbMgr.GetScalarQuery<int>("SELECT COUNT(id) as max_number FROM surveys_questions WHERE survey_id=" + surveyId);

                int currentQuestions = string.IsNullOrWhiteSpace(Request["question"]) ? 1 : Convert.ToInt32(Request["question"]);

                if (currentQuestions - 1 > 0)
                {
                    prevQuestionButton.Attributes["href"] = "SurveyStatisticDetails.aspx?id=" + alertId + "&sendAlertId=" + sendAlertId + "&question=" +
                                                            (currentQuestions - 1);
                }
                else
                {
                    prevQuestionButton.Visible = false;
                }

                if (currentQuestions < totalQuestions)
                {
                    nextQuestionButton.Attributes["href"] = "SurveyStatisticDetails.aspx?id=" + alertId + "&sendAlertId=" + sendAlertId + "&question=" +
                                        (currentQuestions + 1);
                }
                else
                {
                    nextQuestionButton.Visible = false;
                }

                DataRow questionRow =
                    dbMgr.GetDataByQuery("SELECT question, id, question_type FROM surveys_questions WHERE question_number = " +
                                         currentQuestions + " AND survey_id = " + surveySet.GetString(0, "id")).First();
                questionText.Text = questionRow.GetString("question");
                var questionId = questionRow.GetString("id");
                var questionType = questionRow.GetString("question_type");
                var isIntermediaryStep = questionType == "S";

                DataSet FreeAnswersSet = dbMgr.GetDataByQuery("SELECT COUNT(1) as number_of_free_answer_questions FROM surveys_custom_answers_stat WHERE question_id = " + questionId);
                DataRow FreeAnswersRow = FreeAnswersSet.GetRow(0);
                bool isFreeAnswerQuestion = FreeAnswersRow.GetInt("number_of_free_answer_questions") > 0;

                string[] headers = new string[1] { string.Empty };
                string[] fullHeaders = { "Question", "Answer", "Votes", "Username" };
                bool showStat = false;
                List<string[]> singleCsvRows = new List<string[]>();

                if (isFreeAnswerQuestion && Config.Data.HasModule(Modules.STATISTICS))
                {
                    var answersQuery = "SELECT answer, user_id FROM surveys_custom_answers_stat WHERE question_id = " + questionId;
                    DataSet answersSet = dbMgr.GetDataByQuery(answersQuery);
                    headers = new string[] { "Answer", "Username" };
                    foreach (DataRow answer in answersSet)
                    {
                        var userId = answer.GetInt("user_id");
                        var answerString = HttpUtility.UrlEncode(answer.GetString("answer"));
                        this.totalQuestions.Text = totalQuestions.ToString();
                        string randomColor = "#" + DeskAlertsUtils.GetRandomHexColor();
                        var tableRow = new HtmlTableRow();
                        tableRow.Cells.Add(new HtmlTableCell() { InnerHtml = string.Format("<div class=\"legend\" style='background-color: {0};'></div>", randomColor) });
                        tableRow.Cells.Add(new HtmlTableCell() { InnerHtml = answer.GetString("answer") });
                        if (isAnonymousSurvey)
                        {
                            tableRow.Cells.Add(new HtmlTableCell() { InnerHtml = "<span class='prelink_stats'>1</span>" });
                        }
                        else
                        {
                            var tableCell = new HtmlTableCell
                            {
                                InnerHtml = Config.Data.HasModule(Modules.STATISTICS) ? $"<a href='AlertViewRecipients.aspx?type=ans&answer_id={questionId}&freeAnswer=1&answerString={answerString}&questionType={questionType}','', 'status=0, toolbar=0, width=350, height=450, scrollbars=1>1</a>" : answerString
                            };

                            tableRow.Cells.Add(tableCell);
                        }

                        questionsTable.Rows.Add(tableRow);

                        if (answersColors.Length > 0)
                            answersColors += ",";

                        answersColors += randomColor.Quotes();
                        if (answersValues.Length > 0)
                            answersValues += ",";
                        if (!showStat && Config.Data.HasModule(Modules.STATISTICS))
                            showStat = true;

                        var userName = GetUserByQuestionId(questionId, userId);
                        answersValues += ($"['{DeskAlertsUtils.HTMLToText(answer.GetString("answer").EscapeJSQuotes())}', 1]");
                        string[] result = { DeskAlertsUtils.HTMLToText(answer.GetString("answer")), userName };
                        singleCsvRows.Add(result);
                    }
                }
                else if (isFreeAnswerQuestion && !Config.Data.HasModule(Modules.STATISTICS))
                {
                    this.totalQuestions.Text = totalQuestions.ToString();
                    var buyPanelTitleLabel = new Label()
                    {
                        Text = "Note: <br />",
                        CssClass = "buy-panel-title"
                    };

                    var buyPanelBodyLabel = new Label()
                    {
                        Text = resources.LNG_BUY_EXTENDED_REPORTS,
                        CssClass = "buy-panel-body"
                    };
                    answersChart.Visible = false;

                    BuyExtendedReports.Controls.Add(buyPanelTitleLabel);
                    BuyExtendedReports.Controls.Add(buyPanelBodyLabel);
                    BuyExtendedReports.Visible = true;
                }
                else
                {
                    var answersQuery = "SELECT answer, id FROM surveys_answers WHERE question_id =  " + questionId;
                    answersQuery += " UNION SELECT answer, id FROM surveys_custom_answers_stat WHERE question_id = " + questionId;
                    DataSet answersSet = dbMgr.GetDataByQuery(answersQuery);
                    headers = new string[] { "Answer", "Votes", "Users" };
                    foreach (DataRow answer in answersSet)
                    {
                        var answerId = answer.GetInt("id");
                        int totalUsers =
                            dbMgr.GetScalarQuery<int>("SELECT count(id) as votes_cnt FROM surveys_answers_stat WHERE answer_id=" + answerId);

                        this.totalQuestions.Text = totalQuestions.ToString();
                        string randomColor = "#" + DeskAlertsUtils.GetRandomHexColor();
                        var tableRow = new HtmlTableRow();
                        tableRow.Cells.Add(new HtmlTableCell() { InnerHtml = string.Format("<div class=\"legend\" style='background-color: {0};'></div>", randomColor) });
                        tableRow.Cells.Add(new HtmlTableCell() { InnerHtml = answer.GetString("answer") });

                        if (isAnonymousSurvey)
                        {
                            tableRow.Cells.Add(new HtmlTableCell() { InnerHtml = Config.Data.HasModule(Modules.STATISTICS) ? string.Format("<div href='#' onclick=\"javascript: ('AlertViewRecipients.aspx?type=ans&answer_id={0}','', 'status=0, toolbar=0, width=350, height=450, scrollbars=1')\">{1}</div>", answer.GetInt("id"), totalUsers) : Convert.ToString(totalUsers) });
                        }
                        else
                        {
                            tableRow.Cells.Add(new HtmlTableCell() { InnerHtml = Config.Data.HasModule(Modules.STATISTICS) ? string.Format("<a href='#' onclick=\"javascript: window.open('AlertViewRecipients.aspx?type=ans&answer_id={0}','', 'status=0, toolbar=0, width=350, height=450, scrollbars=1')\">{1}</a>", answer.GetInt("id"), totalUsers) : Convert.ToString(totalUsers) });
                        }
                        questionsTable.Rows.Add(tableRow);

                        if (answersColors.Length > 0)
                            answersColors += ",";

                        answersColors += randomColor.Quotes();
                        if (answersValues.Length > 0)
                            answersValues += ",";
                        if (!showStat && totalUsers > 0)
                            showStat = true;
                        var userNames = GetUsers(answerId, totalUsers);
                        answersValues += string.Format("['{0}', {1}]", DeskAlertsUtils.HTMLToText(answer.GetString("answer").EscapeJSQuotes()), totalUsers);
                        string[] result = { DeskAlertsUtils.HTMLToText(answer.GetString("answer")), totalUsers.ToString(), userNames };
                        singleCsvRows.Add(result);
                    }
                }

                if (!showStat)
                {
                    answersValues = "['',1]";
                    answersColors = "'#f0f0f1'";
                }

                //Generate link for all questions CSV
                var questionSet = dbMgr.GetDataByQuery("SELECT * FROM surveys_questions WHERE survey_id = " + surveyId);
                var fullQuestionsList = new List<string[]>();
                foreach (DataRow question in questionSet)
                {
                    var questionIdCSV = question.GetString("id");
                    string questionTextValue = DeskAlertsUtils.HTMLToText(question.GetString("question"));
                    DataSet answerSet =
                        dbMgr.GetDataByQuery(
                            "SELECT [id] ,[answer] ,[survey_id] ,[votes] ,[question_id] ,[correct], NULL AS user_id " +
                            "FROM surveys_answers WHERE question_id = " + questionIdCSV + " " +
                            "UNION " +
                            "SELECT NULL as [id] ,[answer] ,NULL as [survey_id] ,NULL as [votes] ,[question_id] ,0 as [correct], [user_id] " +
                            "FROM [surveys_custom_answers_stat] WHERE question_id =" + questionIdCSV);

                    foreach (DataRow answer in answerSet)
                    {
                        var userId = answer.GetString("user_id");
                        var answerId = answer.GetString("id");
                        int totalUsers;
                        if (string.IsNullOrEmpty(answerId))
                        {
                            totalUsers = dbMgr.GetScalarQuery<int>(
                                  "SELECT count(question_id) as votes_cnt " +
                                  "FROM surveys_custom_answers_stat " +
                                  "WHERE user_id = " + userId +
                                  " AND question_id = " + questionIdCSV);
                        }
                        else
                        {
                            totalUsers = dbMgr.GetScalarQuery<int>(
                                  "SELECT count(id) as votes_cnt " +
                                  "FROM surveys_answers_stat " +
                                  "WHERE answer_id = " + answerId);
                        }

                        string answerText = DeskAlertsUtils.HTMLToText(answer.GetString("answer"));
                        var userNames = string.Empty;
                        if (string.IsNullOrEmpty(answerId))
                        {
                            userNames = GetUsersForFreeAnswer(userId, questionIdCSV);
                        }
                        else
                        {
                            userNames = GetUsers(int.Parse(answerId), totalUsers);
                        }

                        string[] questionValues = { questionTextValue, answerText, totalUsers.ToString(), userNames };
                        fullQuestionsList.Add(questionValues);
                    }
                }

                var excelHeaders = new[]
                {
                    "Alert title",
                    "Sent date",
                    "Total",
                    "Received",
                    "Not received",
                    "Voted",
                    "Not voted"
                };

                var excelValues = new[]
                {
                    alertRow.GetString("title"),
                    alertRow.GetString("sent_date"),
                    totalRecipientsCount.ToString(),
                    recievedRecipientsCount.ToString(),
                    notRecievedRecipientsCount.ToString(),
                    votedCnt.ToString(),
                    notVotedCnt.ToString()
                };

                var csvDownloadLinkValue = SpreedsheetManager.WriteCsvToUploadsFromDataRow("alert" + alertId, excelHeaders, new List<string[]> { excelValues });
                var xlsxDownloadLinkValue = SpreedsheetManager.WriteXlsxToUploads("alert" + alertId, new[] { excelHeaders, excelValues });

                var fullCsvLinkValue = SpreedsheetManager.WriteCsvToUploadsFromDataRow("survey" + surveyId, fullHeaders, fullQuestionsList);
                fullQuestionsList.Insert(0, fullHeaders);
                var fullXlsxLinkValue = SpreedsheetManager.WriteXlsxToUploads("survey" + surveyId, fullQuestionsList);

                var singleCsvLinkValue = SpreedsheetManager.WriteCsvToUploadsFromDataRow("question" + questionRow.GetString("id"), headers, singleCsvRows);
                singleCsvRows.Insert(0, headers);
                var singleXlsxLinkValue = SpreedsheetManager.WriteXlsxToUploads("question" + questionRow.GetString("id"), singleCsvRows);

                csvDownloadLink.Attributes["href"] = csvDownloadLinkValue;
                xlsxDownloadLink.Attributes["href"] = xlsxDownloadLinkValue;
                fullCsvLink.Attributes["href"] = fullCsvLinkValue;
                fullXlsxLink.Attributes["href"] = fullXlsxLinkValue;
                singleCsvLink.Attributes["href"] = singleCsvLinkValue;
                singleXlsxLink.Attributes["href"] = singleXlsxLinkValue;

                if (Config.Data.HasModule(Modules.STATISTICS))
                {
                    csvDownloadLink.Visible = true;
                    xlsxDownloadLink.Visible = true;
                    fullCsvLink.Visible = true;
                    singleCsvLink.Visible = true;
                }
            }
            catch (Exception ex)
            {
                Logger.Debug(ex);
                throw ex;
            }
        }

        private bool IsAnonymousSurvey(int alertId)
        {
            return dbMgr.GetScalarByQuery<string>(string.Format("SELECT anonymous_survey FROM surveys_main WHERE id ='{0}'", alertId)) == "1";
        }

        private string GetUsersForFreeAnswer(string userId, string questionId)
        {
            var usersNamesSet = dbMgr.GetDataByQuery(
                                "SELECT DISTINCT u.name " +
                                "FROM users AS u " +
                                "INNER JOIN surveys_custom_answers_stat AS scas " +
                                "ON scas.user_id = u.id " +
                                "WHERE scas.user_id = " + userId + " AND scas.question_id = " + questionId);
            if (usersNamesSet.Count == 0)
            {
                throw new Exception("The answer is not contains user");
            }

            var userName = usersNamesSet.First().GetString("name");
            userName = isAnonymousSurvey ? AnonymousUserName : userName;
            return userName;
        }

        private string GetUsers(int answerId, int totalUsers)
        {
            var userNames = string.Empty;
            var usersNamesSet = dbMgr.GetDataByQuery(
                                "SELECT u.name " +
                                "FROM users as u " +
                                "INNER JOIN surveys_answers_stat as sas " +
                                "ON sas.user_id = u.id " +
                                "WHERE sas.answer_id = " + answerId);

            if (!usersNamesSet.IsEmpty)
            {
                bool isFirst = true;
                foreach (var usersNames in usersNamesSet)
                {
                    string userName = usersNames.GetString("name");
                    if (isFirst)
                    {
                        userNames += userName;
                    }
                    else
                    {
                        userNames += "," + userName;
                    }
                    isFirst = false;
                }
            }
            userNames = isAnonymousSurvey ? AnonymousUserName : userNames;
            userNames = totalUsers == 0 ? string.Empty : userNames;
            return userNames;
        }

        private string GetUserByQuestionId(string questionId, int userId)
        {
            if (isAnonymousSurvey)
            {
                return AnonymousUserName;
            }
            else
            {
                return dbMgr.GetScalarByQuery<string>(
               "SELECT u.name FROM surveys_custom_answers_stat AS cas " +
               "RIGHT JOIN users AS u ON cas.[user_id] = u.id " +
               "WHERE cas.question_id = " + questionId + " AND cas.user_id = " + userId);
            }
        }
    }
}