﻿<% Response.CacheControl = "no-cache" %>
<% Response.AddHeader "Pragma", "no-cache" %>
<% Response.Expires = -1 %>
<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<!-- #include file="ad.inc" -->
<%
Server.ScriptTimeout = 300
Conn.CommandTimeout = 300

check_session()

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>DeskAlerts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/style9.css" rel="stylesheet" type="text/css">
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script language="javascript" type="text/javascript">

$(document).ready(function() {
		var width = $(".data_table").width();
		var pwidth = $(".paginate").width();
		if (width != pwidth) {
			$(".paginate").width("100%");
		}
	$(".search_button").button({
		icons : {
			primary : "ui-icon-search"
		}
	});
});

function filterType(e) {
	if (filterCheckboxClicked(e)) {
		$("#search").val("<%= Request("search") %>");
		$("#search_users").submit();
	}
}

</script>
</head>
<script language="javascript" type="text/javascript" src="functions.js"></script>



<%

  uid = Session("uid")
  flag=1

%>

<body style="background-color: #ffffff; margin:0px" class="body">
<%

if uid <> "" then
	users_arr = Array("checked","checked","checked","checked","checked","checked","checked")
	gid = 0
	gsendtoall = 0
	set RS = Conn.Execute("SELECT id, only_personal FROM users WHERE id="&uid&" AND role='E'")
	if not RS.EOF then
		gid = 1
		set rs_policy = Conn.Execute("SELECT p.id, p.type, pu.user_id FROM policy p INNER JOIN policy_editor pe ON p.id=pe.policy_id LEFT JOIN policy_user pu ON pu.policy_id = p.id AND pu.user_id = 0 WHERE pe.editor_id=" & uid)
		if not rs_policy.EOF then
			if rs_policy("type")="A" then 'can send to all
				gid = 0
			elseif not IsNull(rs_policy("user_id")) then
				gid = 0
				gsendtoall = 1
			end if
		end if
		if gid = 1 or gsendtoall = 1 then
			editor_id = RS("id")
			policy_ids = editorGetPolicyIds(editor_id)
			users_arr = editorGetPolicyList(policy_ids, "users_val")
		end if
	end if
	RS.Close

	'showCheckbox = request("sc")
	send_sms = Request("sms")
	send_email = Request("email")
	send_desktop = Request("desktop")
	send_text_to_call = Request("text_to_call")
	device=Request("device")
	showCheckbox = 1
	search_id = clng(request("id"))
	search_type = request("type")
	if search_type = "" then search_type = "organization"

	filter_users = request("fu")
	filter_groups = request("fg")
	filter_computers = request("fc")
	
	if((filter_users="" AND filter_groups="" AND filter_computers="") OR (filter_users="0" AND filter_groups="0" AND filter_computers="0")) then
		if ConfShowGroupsByDefault = 1 then
			filter_groups = "1"
		else
			filter_users = "1"
		end if
	end if
	
	if(Request("offset") <> "") then 
		offset = clng(Request("offset"))
	else 
		offset = 0
	end if	

	if(Request("sortby") <> "") then 
		sortby = Request("sortby")
	else 
		sortby = "object_name"
	end if	


	myuname = request("uname")
	if(myuname <> "") then 
		searchSQLusers = " (users.name LIKE N'%"&Replace(myuname, "'", "''")&"%' OR users.display_name LIKE N'%"&Replace(myuname, "'", "''")&"%'"
		searchSQLgroups = " (groups.name LIKE N'%"&Replace(myuname, "'", "''")&"%' ) "
		searchSQLcomps = " (computers.name LIKE N'%"&Replace(myuname, "'", "''")&"%') "
		
		if Request("webalert") = "1" then
		    searchSQLusers = searchSQLusers & " AND ( users.client_version = 'web client' )"	
		else 
		    searchSQLusers = searchSQLusers & " AND ( users.client_version != 'web client' OR users.client_version IS NULL ) "	
		end if 
		
		searchSQLusers = searchSQLusers & " )"
	else 
		searchSQLusers = ""
		searchSQLgroups = ""
		searchSQLcomps = ""
	end if	
	
	
	cnt = 0
		showSQL = 	" SET NOCOUNT ON" & vbCrLf &_
					" IF OBJECT_ID('tempdb..#tempOUs') IS NOT NULL DROP TABLE #tempOUs" & vbCrLf &_
					" CREATE TABLE #tempOUs  (Id_child BIGINT NOT NULL, Rights BIT NOT NULL);" & vbCrLf &_
					" IF OBJECT_ID('tempdb..#tempUsers') IS NOT NULL DROP TABLE #tempUsers" & vbCrLf &_
					" CREATE TABLE #tempUsers  (user_id BIGINT NOT NULL);" & vbCrLf &_
					" IF OBJECT_ID('tempdb..#tempComputers') IS NOT NULL DROP TABLE #tempComputers" & vbCrLf &_
					" CREATE TABLE #tempComputers  (comp_id BIGINT NOT NULL);" & vbCrLf &_
					" IF OBJECT_ID('tempdb..#tempGroups') IS NOT NULL DROP TABLE #tempGroups" & vbCrLf &_
					" CREATE TABLE #tempGroups  (group_id BIGINT NOT NULL);" & vbCrLf &_
					" DECLARE @now DATETIME;" & vbCrLf &_
					" SET @now = GETDATE(); "
						
		OUsSQL=""

		if(search_type = "ou") then
			if Request("search") = "1" then
				OUsSQL = OUsSQL & "WITH OUs (id) AS("
				OUsSQL = OUsSQL &" SELECT CAST("&search_id&" AS BIGINT) "
				OUsSQL = OUsSQL &_
						"	UNION ALL" & vbCrLf &_
						"	SELECT Id_child" & vbCrLf &_
						"	FROM OU_hierarchy h" & vbCrLf &_
						"	INNER JOIN OUs ON OUs.id = h.Id_parent" & vbCrLf &_
						") " & vbCrLf &_
						"INSERT INTO #tempOUs (Id_child, Rights) (SELECT DISTINCT id, 0 FROM OUs) Option (MaxRecursion 10000); "
			else
				OUsSQL = OUsSQL &" INSERT INTO #tempOUs (Id_child, Rights) VALUES ("&search_id&", 0)"
			end if
		elseif(search_type = "DOMAIN") then
			OUsSQL = OUsSQL &" INSERT INTO #tempOUs (Id_child, Rights) (SELECT id, 0 FROM OU WHERE Id_domain = "&search_id&")"
		else
			OUsSQL = OUsSQL & " INSERT INTO #tempOUs (Id_child, Rights) (SELECT id, 0 FROM OU) "
		end if

		if (gid = 1) then 
		
			OUsSQL = OUsSQL &_
				" ;WITH OUsList (Id_parent, Id_child) AS (" & vbCrLf &_
				" 	SELECT Id_child AS Id_parent, Id_child FROM #tempOUs" & vbCrLf &_
				"	UNION ALL" & vbCrLf &_
				"	SELECT h.Id_parent, l.Id_child" & vbCrLf &_
				"	FROM OU_hierarchy as h" & vbCrLf &_
				"	INNER JOIN OUsList as l" & vbCrLf &_
				"	ON h.Id_child = l.Id_parent)" & vbCrLf &_ 
				" UPDATE #tempOUs" & vbCrLf &_
				" SET Rights = 1" & vbCrLf &_
				" FROM #tempOUs t" & vbCrLf &_
				" INNER JOIN OUsList l" & vbCrLf &_
				" ON l.Id_child = t.Id_child" & vbCrLf &_
				" LEFT JOIN policy_ou p ON p.ou_id=l.Id_parent AND " & Replace(policy_ids, "policy_id", "p.policy_id") & vbCrLf &_
				" LEFT JOIN ou_groups g ON g.ou_id=l.Id_parent" & vbCrLf &_
				" LEFT JOIN policy_group pg ON pg.group_id=g.group_id AND " & Replace(policy_ids, "policy_id", "pg.policy_id") & vbCrLf &_
				" WHERE NOT p.id IS NULL OR NOT pg.id IS NULL"
		end if
		
		groupsSQL = " INSERT INTO #tempGroups (group_id)" & vbCrLf &_ 
					" (" & vbCrLf &_
					" SELECT DISTINCT groups.id" & vbCrLf &_
					" FROM groups" & vbCrLf &_
					" INNER JOIN OU_Group og" & vbCrLf &_
					" ON groups.id=og.Id_group" & vbCrLf &_
					" INNER JOIN #tempOUs t" & vbCrLf &_
					" ON og.Id_OU=t.Id_child"
		if (gid = 1) then 
			groupsSQL = groupsSQL &_
					" LEFT JOIN policy_group p ON p.group_id=og.Id_group AND "&policy_ids &_
					" WHERE (t.Rights = 1 OR NOT p.group_id IS NULL) " 
			if (searchSQLgroups<>"") then 
				groupsSQL = groupsSQL & " AND " & searchSQLgroups
			end if
		else	
			if (searchSQLgroups<>"") then 
				groupsSQL = groupsSQL & " WHERE " &searchSQLgroups
			end if
		end if
		
		groupsSQL = groupsSQL & " )"
		
'		if Request("search") = "1" then
'			groupsSQL = groupsSQL &_
'						" IF OBJECT_ID('tempdb..#tmp1') IS NOT NULL DROP TABLE #tmp1" & vbCrLf &_
'						" CREATE TABLE #tmp1  (group_id BIGINT NOT NULL);" & vbCrLf &_
'						" IF OBJECT_ID('tempdb..#tmp2') IS NOT NULL DROP TABLE #tmp2" & vbCrLf &_
'						" CREATE TABLE #tmp2  (group_id BIGINT NOT NULL);" & vbCrLf &_
'						" INSERT INTO #tmp1 (group_id)(SELECT group_id FROM #tempGroups)" & vbCrLf &_
'						" WHILE 1=1" & vbCrLf &_
'						" BEGIN" & vbCrLf &_
'						" 	INSERT INTO #tmp2 (group_id)" & vbCrLf &_
'						" 	(" & vbCrLf &_
'						" 			SELECT DISTINCT g.group_id FROM groups_groups as g" & vbCrLf &_
'						" 			INNER JOIN #tmp1 as t1" & vbCrLf &_
'						" 			ON g.container_group_id = t1.group_id" & vbCrLf &_
'						" 			LEFT JOIN #tempGroups as t2" & vbCrLf &_
'						" 			ON g.group_id = t2.group_id" & vbCrLf &_
'						" 			WHERE t2.group_id IS NULL" & vbCrLf &_
'						" 	)" & vbCrLf &_
'						" 	IF @@ROWCOUNT = 0 BREAK;" & vbCrLf &_
'						" 	DELETE #tmp1" & vbCrLf &_
'						" 	INSERT INTO #tmp1 (group_id)(SELECT group_id FROM #tmp2)" & vbCrLf &_
'						" 	INSERT INTO #tempGroups (group_id)(SELECT group_id FROM #tmp2)" & vbCrLf &_
'						" 	DELETE #tmp2" & vbCrLf &_
'						" END; "& vbCrLf &_
'						" DROP TABLE #tmp1" & vbCrLf &_
'						" DROP TABLE #tmp2 "
'		end if

		if (search_type <> "ou") then
			groupsSQL = groupsSQL &_
						" INSERT INTO #tempGroups (group_id)" & vbCrLf &_
						" (" & vbCrLf &_
						" SELECT DISTINCT groups.id" & vbCrLf &_
						" FROM groups"
						if (gid = 1) then 
				groupsSQL = groupsSQL & " INNER JOIN policy_group p ON p.group_id=groups.id AND "&policy_ids
			end if
				groupsSQL = groupsSQL &_	
						" LEFT JOIN #tempGroups t" & vbCrLf &_
						" ON t.group_id=groups.id" & vbCrLf &_
						" WHERE t.group_id IS NULL "
			if (search_type = "DOMAIN") then
				groupsSQL = groupsSQL & " AND groups.domain_id = "&search_id
			end if 
				if (searchSQLgroups<>"") then 
					groupsSQL = groupsSQL & " AND " & searchSQLgroups
				end if
				groupsSQL = groupsSQL & " ) "
		end if



		checkedOUs=request("ous")
		checkedObject = ""
		if(InStr(checkedOUs, search_id)>0) then
			'checkedObject = "checked"
		end if

	'counting pages to display
		if(Request("limit") <> "") then 
			limit = clng(Request("limit"))
		else 
			limit=25
		end if

		linkclick_str = LNG_USERS

		SQL = 	""
		usersSQL = ""
		computersSQL = ""
		countSQL = ""
		selectSQL = ""
			if(filter_users="1") then
			
				usersSQL = " INSERT INTO #tempUsers (user_id)(" & vbCrLf &_
						" SELECT DISTINCT Id_user" & vbCrLf &_
						" FROM OU_User u" & vbCrLf &_
						" INNER JOIN #tempOUs t" & vbCrLf &_
						" ON u.Id_OU=t.Id_child" & vbCrLf &_ 
						" LEFT JOIN users ON u.Id_user = users.id"
						
				whereSQL = ""
				
				if (gid = 1) then 
					usersSQL = usersSQL &_
							" LEFT JOIN policy_user p ON p.user_id=u.Id_user AND "&policy_ids
							
					whereSQL = " AND (t.Rights = 1 OR NOT p.id IS NULL) "
					

				end if
				

				
				usersSQL = usersSQL & " WHERE role='U' " & whereSQL
					
				if (searchSQLusers<>"") then 
					usersSQL = usersSQL & " AND " & searchSQLusers
				end if
				
				if(Request("webalert") = "1") then 
					    usersSQL = usersSQL & "  AND ( client_version = 'web client' )"
				else 
				    usersSQL = usersSQL & "  AND ( client_version != 'web client'  OR client_version IS NULL ) "
				end if
				usersSQL = usersSQL & ") "
				
			
				
				if (search_type <> "ou") then
					usersSQL = usersSQL &_
								" INSERT INTO #tempUsers(user_id)(" & vbCrLf &_
								" SELECT DISTINCT users.id FROM users"
								
					if (gid = 1) then 
						usersSQL = usersSQL & " INNER JOIN policy_user p ON p.user_id=users.id AND "&policy_ids
					end if
					if(device="1") then
					'Response.Write("desktop")
					usersSQL = usersSQL &_	
								" LEFT JOIN #tempUsers t" & vbCrLf &_
								" ON t.user_id=users.id" & vbCrLf &_
								" WHERE t.user_id IS NULL AND users.role='U' AND  (users.id not in (select user_id from user_instances) OR users.id in (select id from users where (next_request is NOT NULL AND next_request<>''))) "
					elseIf(device="2") then
					'Response.Write("mobile")
					usersSQL = usersSQL &_	
								" LEFT JOIN #tempUsers t" & vbCrLf &_
								" ON t.user_id=users.id" & vbCrLf &_
								" WHERE t.user_id IS NULL AND users.role='U' AND  users.id in(select user_id from user_instances) "
					else
					'Response.Write("all devices")
					usersSQL = usersSQL &_	
								" LEFT JOIN #tempUsers t" & vbCrLf &_
								" ON t.user_id=users.id" & vbCrLf &_
								" WHERE t.user_id IS NULL AND users.role='U' "
					end if
														
					if( Request("webalert") = "1") then
					    usersSQL = usersSQL & " AND ( users.client_version = 'web client'  ) "
					else
					    usersSQL = usersSQL & "AND ( client_version != 'web client'  OR client_version IS NULL ) "
					end if 
					
					if (search_type = "DOMAIN") then
						usersSQL = usersSQL & " AND users.domain_id = "&search_id
					end if 
						if (searchSQLusers<>"") then 
							usersSQL = usersSQL & " AND " & searchSQLusers
						end if
						usersSQL = usersSQL & ") "	
				end if
				
				countSQL = "SELECT COUNT(1) as mycnt FROM #tempUsers"
				selectSQL = " SELECT users.id, 'user' as object_type, context_id, users.name as object_name, display_name, domains.name as domain_name, users.next_request, users.last_standby_request, users.next_standby_request, users.mobile_phone, users.email, users.standby, CONVERT(varchar, users.last_request) as last_request1, users.last_request," & vbCrLf &_
					" edir_context.name as context_name" & vbCrLf &_
					" FROM #tempUsers" & vbCrLf &_ 
					" INNER JOIN users ON user_id = users.id" & vbCrLf &_
					" LEFT JOIN domains ON users.domain_id = domains.id " & vbCrLf &_
					" LEFT JOIN edir_context ON edir_context.id = users.context_id" & vbCrLf
			
			end if
			if(filter_groups="1") then
				
				if(countSQL <> "") then
					countSQL = countSQL & " UNION ALL "
				end if
				
				countSQL = countSQL & " SELECT COUNT(1) as mycnt FROM #tempGroups"
			
				if(selectSQL <> "") then
					selectSQL = selectSQL & " UNION ALL "
				end if

				selectSQL = selectSQL &_
				" SELECT groups.id, 'group' as object_type, context_id, groups.name as object_name, '' as display_name, domains.name as domain_name, NULL as next_request, NULL as last_standby_request, NULL as next_standby_request, '' as mobile_phone, '' as email, 0 as standby, '' as last_request1, NULL as last_request," & vbCrLf &_
				" edir_context.name as context_name" & vbCrLf &_
				" FROM #tempGroups" & vbCrLf &_
				" INNER JOIN groups ON group_id = groups.id" & vbCrLf &_
				" LEFT JOIN domains ON domains.id = groups.domain_id " & vbCrLf &_
				" LEFT JOIN edir_context ON edir_context.id = groups.context_id" & vbCrLf
			
			end if
			
			if(filter_computers="1") then
			
				computersSQL = " INSERT INTO #tempComputers(comp_id)(" & vbCrLf &_
						" 	SELECT DISTINCT Id_comp" & vbCrLf &_
						" 	FROM OU_comp c" & vbCrLf &_
						" 	INNER JOIN #tempOUs t" & vbCrLf &_
						" 	ON c.Id_OU=t.Id_child "
						
				whereSQL=""
				if (searchSQLcomps<> "") then
					computersSQL = computersSQL & " LEFT JOIN computers ON c.Id_comp = computers.id"
					whereSQL = " WHERE " & searchSQLcomps		
				end if
							
											
				if (gid = 1) then 
					computersSQL = computersSQL &_
							" LEFT JOIN policy_computer p ON p.comp_id=c.Id_comp AND "&policy_ids&_
							" WHERE t.Rights = 1 OR NOT p.id IS NULL "
					if (searchSQLcomps<>"") then 
						computersSQL = computersSQL & " AND " & searchSQLcomps
					end if
				else
					computersSQL = computersSQL & whereSQL
				end if
				
				
				computersSQL = computersSQL & ") "
				
			
				
				if (search_type <> "ou") then
					computersSQL = computersSQL &_
								" INSERT INTO #tempComputers(comp_id)(" & vbCrLf &_
								" SELECT DISTINCT computers.id FROM computers"
					if (gid = 1) then 
						computersSQL = computersSQL & " INNER JOIN policy_computer p ON p.comp_id=computers.id AND "&policy_ids
					end if
						computersSQL = computersSQL &_	
								" LEFT JOIN #tempComputers t" & vbCrLf &_
								" ON t.comp_id=computers.id" & vbCrLf &_
								" WHERE t.comp_id IS NULL "
					if (search_type = "DOMAIN") then
						computersSQL = computersSQL & " AND computers.domain_id = "&search_id
					end if 
						if (searchSQLcomps<> "") then
							computersSQL = computersSQL & " AND " & searchSQLcomps
						end if
						computersSQL = computersSQL & ") "	
				end if
				
				if(countSQL <> "") then
					countSQL = countSQL & " UNION ALL "
				end if
				
				countSQL = countSQL & " SELECT COUNT(1) as mycnt FROM #tempComputers"
			
				if(selectSQL <> "") then
					selectSQL = selectSQL & " UNION ALL "
				end if

				selectSQL = selectSQL &_
				" SELECT computers.id, 'computer' as object_type, context_id, computers.name as object_name, '' as display_name, domains.name as domain_name, NULL as next_request, NULL as last_standby_request, NULL as next_standby_request, '' as mobile_phone, '' as email, 0 as standby, '' as last_request1, NULL as last_request," & vbCrLf &_
				" edir_context.name as context_name" & vbCrLf &_
				" FROM #tempComputers" & vbCrLf &_
				" INNER JOIN computers ON comp_id = computers.id" & vbCrLf &_
				" LEFT JOIN domains ON domains.id = computers.domain_id " & vbCrLf &_
				" LEFT JOIN edir_context ON edir_context.id = computers.context_id"
			end if
			
			selectSQL = " SELECT * FROM ( " & selectSQL & " ) as tmp ORDER BY " & sortby & vbCrLf &_
				" DROP TABLE #tempOUs" & vbCrLf &_
				" DROP TABLE #tempUsers" & vbCrLf &_
				" DROP TABLE #tempComputers" & vbCrLf &_ 
				" DROP TABLE #tempGroups"
	
			showSQL = showSQL & OUsSQL & groupsSQL & usersSQL & computersSQL & countSQL & selectSQL
			
			
           ' Response.Write(showSQL)
			

		Set RS = Conn.Execute(showSQL)
		
		if Not RS.EOF then
			if(Not IsNull(RS("mycnt"))) then cnt=cnt+RS("mycnt") end if
			RS.MoveNext
		end if
		if Not RS.EOF then
			if(Not IsNull(RS("mycnt"))) then cnt=cnt+RS("mycnt") end if
			RS.MoveNext
		end if
		if Not RS.EOF then
			if(Not IsNull(RS("mycnt"))) then cnt=cnt+RS("mycnt") end if
			RS.MoveNext
		end if

		SET RS = RS.NextRecordSet
		j=cnt/limit
	

	if(filter_users="1") then
		filter_users_checked = "checked"
	end if
	if(filter_groups="1") then
		filter_groups_checked = "checked"
	end if
	if(filter_computers="1") then
		filter_computers_checked = "checked"
	end if
  
%>

<form name="search_users" id="search_users" action="objects.asp?sc=<%=showCheckbox%>&id=<%=search_id%>&type=<%=search_type%>&ous=<%=checkedOUs%>&offset=0&limit=<%=limit%>&sms=<%=send_sms%>&email=<%=send_email%>&desktop=<%=send_desktop%>&text_to_call=<%=send_text_to_call%>&webalert=<%=Request("webalert")%>&device=<%=device%>>" method="POST">
<table border="0">
<tr valign="middle">
	<td valign="middle"><%=LNG_SEARCH_OBJECTS %>: <input type="text" name="uname" value="<%= HTMLEncode(myuname) %>"/></td>
	<td valign="middle">
		<a name="sub" class="search_button" onclick="javascript:document.forms[0].submit()"><%=LNG_SEARCH %></a>
		<input type="hidden" id="search" name="search" value="1" />
	</td>
	<% if Request("webalert") <> "1" then %>
	<td valign="middle">&nbsp;</td>
	<td valign="middle"><%=LNG_SHOW %>:</td>
	<td valign="middle"><input type="checkbox" <%=filter_users_checked %> name="fu" onclick="filterType(event);" id="filter_users" value="1"></td>
	<td valign="middle"><label for="filter_users"><%=LNG_USERS %></label></td>
	<td valign="middle"><input type="checkbox" <%=filter_groups_checked %> name="fg" onclick="filterType(event);" id="filter_groups" value="1"></td>
	<td valign="middle"><label for="filter_groups"><%=LNG_GROUPS %></label></td>
	
	<%if(AD=3) then%>
	<td valign="middle"><input type="checkbox" <%=filter_computers_checked %> name="fc" onclick="filterType(event);" id="filter_computers" value="1"></td>
	<td valign="middle"><label for="filter_computers"><%=LNG_COMPUTERS %></label></td>
	<%end if%>
	<% end if %>
</tr>
<% if myuname <> "" or Request("search") = "1" then %>
<tr valign="middle">
	<td valign="middle" colspan="10">
		<%=LNG_YOUR_SEARCH_BY %> "<%=HTMLEncode(myuname)%>" <%=LNG_KEYWORD%>:
	</td>
</tr>
<% end if %>
</table>
</form>
<%
if(cnt>0) then
    webalert = ""
    if (request("webalert") = "1") then
        webalert = "1"
    end if

	page="objects.asp?webalert="&webalert&"&sc="&showCheckbox&"&id="&search_id&"&type="&search_type&"&ous="&checkedOUs&"&uname=" & Server.URLEncode(myuname) & "&fu="&filter_users&"&fg="&filter_groups&"&fc="&filter_computers&"&sms="&send_sms&"&email="&send_email&""&"&desktop="&send_desktop&"&text_to_call="&send_text_to_call&"&search="&Request("search")&"&"
	name=LNG_OBJECTS
	Response.Write make_pages(offset, cnt, limit, page, name, sortby) 
%>
		<table height="100%" cellspacing="0" cellpadding="3" class="data_table">
		<tr class="data_table_title">
		<%if(showCheckbox=1) then%>
<td width="10"><input type='checkbox' name='select_all' id='select_all' onclick='javascript: select_all_objects_ou();' ></td>
<%end if%>

<td class="table_title"><% response.write sorting(LNG_NAME,"object_name", sortby, offset, limit, page) %></td>
<td class="table_title"><% response.write sorting(LNG_TYPE,"object_type", sortby, offset, limit, page) %></td>

<% if (AD = 3) then %>		
<td class="table_title"><% response.write sorting(LNG_DISPLAY_NAME,"display_name", sortby, offset, limit, page) %></td> 
<td class="table_title"><% response.write sorting(LNG_DOMAIN,"domain_name", sortby, offset, limit, page) %></td> 
<%end if%>

<% if (EDIR = 1) then %>		<td class="table_title"><%=LNG_CONTEXT %></td> <% end if %>

<% if ((AD = 3 OR SMS = 1 OR TEXT_TO_CALL = 1) AND (send_sms="1" OR send_text_to_call="1")) then%>
<td class="table_title"><% response.write sorting(LNG_MOBILE_PHONE,"mobile_phone", sortby, offset, limit, page) %></td>
<%end if%>

<% if ((AD = 3 OR EM = 1) AND send_email="1") then%>
<td class="table_title"><% response.write sorting(LNG_EMAIL,"email", sortby, offset, limit, page) %></td>
<% end if %>

<% if (send_desktop="1") then%>
<td class="table_title"><% response.write sorting(LNG_ONLINE,"last_request1", sortby, offset, limit, page) %></td>
<% end if %>
</tr>


<%

'show main table
	num=offset
	if(Not RS.EOF) then RS.Move(offset) end if
	Do While Not RS.EOF
		num=num+1
		if((offset+limit) < num) then Exit Do end if
		if(num > offset AND offset+limit >= num) then

			if(Not IsNull(RS("last_request"))) then
			last_activ = CStr(RS("last_request1"))

				mydiff=DateDiff ("n", RS("last_request"), now_db)
				if(RS("next_request")<>"") then
					online_counter = Int(CLng(RS("next_request"))/60) * 2
				else
					online_counter = 2
				end if
				
				if(mydiff > online_counter) then
					online="<img src='images/offline.gif' alt='"& LNG_OFFLINE &"' title='"& LNG_OFFLINE &"' width='11' height='11' border='0'>"
				else
					online="<img src='images/online.gif' alt='"&LNG_ONLINE &"' title='"&LNG_ONLINE &"' width='11' height='11' border='0'>"
				end if
			else
				last_activ = "&nbsp;"
				online="<img src='images/offline.gif' alt='"& LNG_OFFLINE &"' title='"& LNG_OFFLINE &"' width='11' height='11' border='0'>"
			end if

			standby_diff=DateDiff ("n", RS("last_standby_request"), now_db)
			if(RS("next_standby_request")<>"" AND RS("next_standby_request")<>"0") then
				standby_counter = Int(CLng(RS("next_standby_request"))/60) * 2
			else
				standby_counter = 10
			end if							
	
			if(RS("standby")=1 AND (standby_diff < standby_counter)) then
				online="<img src='images/standby.gif' alt='"&LNG_STAND_BY &"' title='"&LNG_STAND_BY &"' width='11' height='11' border='0'>"
			end if

			if (AD = 3 OR SMS = 1 OR TEXT_TO_CALL = 1) then
				mobile="&nbsp;"
				if(RS("mobile_phone")<>"") then
					mobile=HtmlEncode(RS("mobile_phone"))
				end if
			end if

			if (AD = 3 OR EM = 1) then
				email="&nbsp;"
				if(RS("email")<>"") then
					email=HtmlEncode(RS("email"))
				end if
			end if

			if (EDIR = 1) then 
				strContext="&nbsp;"
				if(Not IsNull(RS("context_name"))) then
					strContext=RS("context_name")
				end if
			end if 
	
	end if
	strObjectID=RS("id")
        Response.Write "<tr>"
if(showCheckbox=1) then
%>
	<td><input type="checkbox" name="objects" id="object<% response.write Left(RS("object_type"),1) & strObjectID%>" value="<%response.write Left(RS("object_type"),1) & strObjectID%>" <%=checkedObject %> onclick="checkboxClicked(event);">
		<input type="hidden" name="objects_name" id="object_name<% response.write Left(RS("object_type"),1) & strObjectID%>"  value="<%=HtmlEncode(RS("object_name")) %>"/>
	</td>
<%
end if
response.write "<td>" & HtmlEncode(RS("object_name")) & "</td>"
response.write "<td>" & RS("object_type") & "</td>"
 if (AD = 3) then 	
	response.write "<td align=center>"
	if(RS("display_name")<>"") then
		response.write HtmlEncode(RS("display_name"))
	else
		response.write "&nbsp;"
	end if
	Response.Write "</td><td align=center>"
	
	Response.Write HtmlEncode(RS("domain_name"))
	Response.Write "</td>"
	
 end if
 
 if (EDIR = 1) then 	
	strContext=replace (strContext,",ou=",".")
	strContext=replace (strContext,",o=",".")
	strContext=replace (strContext,",dc=",".")
	strContext=replace (strContext,",c=",".")

	strContext=replace (strContext,"ou=","")
	strContext=replace (strContext,"o=","")
	strContext=replace (strContext,"dc=","")
	strContext=replace (strContext,"c=","")
	Response.Write "<td align=center>"
	Response.Write HtmlEncode(strContext)
	Response.Write "</td>"
 end if
 
 if ((AD = 3 OR SMS=1) AND (send_sms="1" OR send_text_to_call="1")) then
	Response.Write "<td align=center>"
	Response.Write mobile
	Response.Write "</td>"
 end if

 if ((AD = 3 OR EM=1) AND send_email="1") then
	Response.Write "<td align=center>"
	Response.Write email
	Response.Write "</td>"
 end if
 
 if (send_desktop="1") then
	Response.Write "<td align=center>"
	Response.Write online & "</td>"
 end if

 Response.Write "</tr>"
 RS.MoveNext
 Loop
 RS.Close


%>         
			</table>
<%

	Response.Write make_pages(offset, cnt, limit, page, name, sortby) 


else

Response.Write "<center><b>" & LNG_THERE_ARE_NO_OBJECTS & ".</b><br><br></center>"

end if

%>

<%
else

  Response.Redirect "index.asp"

end if

%> 
</body>
</html>
<!-- #include file="db_conn_close.asp" -->