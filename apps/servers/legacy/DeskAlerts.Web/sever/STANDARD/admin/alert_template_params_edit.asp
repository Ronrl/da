﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>DeskAlerts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/style9.css" rel="stylesheet" type="text/css">
<%
	preview_value_id = Request("preview_value_id")
	if preview_value_id <> "" then
%>
</head>
<body style="margin:0" class="iframe_body">
<%
		Set RS = Conn.Execute("SELECT value FROM alert_param_selects WHERE id = '"&Replace(preview_value_id, "'", "''")&"'")
		Response.Write RS("value")
		RS.Close
	else
%>
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css">
<script language="javascript" type="text/javascript" src="functions.js"></script>
<!-- TinyMCE -->
<script language="javascript" type="text/javascript" src="jscripts/tiny_mce/tinymce.min.js"></script>
<script language="javascript" type="text/javascript">
	var newTinySettings = {<%= newTinySettings()%>};
	initTinyMCE("<%=lcase(default_lng) %>", "textareas", null, null, null, newTinySettings);
</script>
<!-- /TinyMCE -->
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script language="javascript" type="text/javascript">
	
	function insertAtCaret (elementId, myValue)
	{
		tinyMCE.execInstanceCommand(elementId,"mceInsertContent",false,myValue);
	}

	function insertParamInText(id)
	{
		var shortName = $("#params_select").val();
		var paramStr = "{" + shortName + "}";
		insertAtCaret('elm1',paramStr)
	}

	$(function() {
		$( "#tabs" ).tabs({
		});
		
		$("#dialog").dialog({ 
			title: 'Error',
			autoOpen: false,
			modal : true
		});
		
		$(".insert_in_text_param_button").button({
			icons: {
				primary: "ui-icon-document"
			},
			text: false
		});

		$('iframe').each(function(){
			var src = this.src;
			this.src = '';
			this.src = src;
		});
		$('iframe').load(function(){
			var iframe = $(this);
			iframe.height(iframe.contents().height());
			setTimeout(function(){
				iframe.height(iframe.contents().height());
			}, 50);
		});
		
		$(".add_button").button();
		$(".delete_button").button();
	});
	
	function addParamValue()
	{
		var selectedIndex=$( "#tabs" ).tabs('option', 'selected');
		var name =  $("#value_name").val();
		var value = "";
		switch (selectedIndex)
		{
			case 0:
				value = $("#row_value").val();
			break;
			
			case 1:
				value = tinyMCE.get('elm1').getContent();
			break;
			
			default: break;
		}
		if (name == "")
		{
			$("#dialog_text").text("Name must not be empty");
			$("#dialog").dialog("open");
			return;
		}
		else if (value == "")
		{
			$("#dialog_text").text("Value must not be empty");
			$("#dialog").dialog("open");
			return;
		}
		$("#add_name").val(name);
		$("#add_value").val(value);
		$("#add_type").val(selectedIndex);
		$("#add_param_value").submit();
	}
</script>
</head>
<body style="margin:0" class="body">
<%
		paramId = Request("id")
		valueId = Request("value_id")
		addValue = Request("add_value")
		addName = Request("add_name")
		addType = Request("add_type")
%>
<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	<tr>
	<td>
	<table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
		<tr>
		<td width=100% height=31 class="main_table_title"><a href="alert_template_params_edit.asp?id=<%=paramId %>" class="header_title">Edit parameters</a></td>
		</tr>
		<tr>
		<td height="100%" valign="top" class="main_table_body" style="padding:10px">

<%
		if (addName<>"" AND addValue<>"" AND addType<>"") then

				addName = Replace(addName, "'", "''") 
				addValue = Replace(addValue, "'", "''") 
				
				if (valueId<>"") then
					Set RS = Conn.Execute("UPDATE alert_param_selects set name = '"&addName&"', value = '"&addValue&"', type = "&addType&" WHERE id='"&valueId&"'")
				else
					Set RS = Conn.Execute("INSERT INTO alert_param_selects (param_id, name, value, type) VALUES ('"&paramId&"','"&addName&"','"&addValue&"',"&addType&")")
				end if

				
				Response.Redirect "alert_template_params_edit.asp?id="&paramId
		end if

		Dim paramType

		if (valueId="") then

			Set RS = Conn.Execute("select type from alert_parameters where id = '"&Replace(paramId, "'", "''")&"'")
			paramType = CLng(RS("type"))
			RS.Close
			
			if (paramType=2 OR paramType=3) then
				Set RS = Conn.Execute("select id, name, value, type from alert_param_selects where param_id = '"&Replace(paramId, "'", "''")&"'")		
				Response.write "<form action='delete_objects.asp' name='delete_objs' id='delete_objs' method='post'><input type='hidden' name='objType' value='alert_template_param_values'><input type='hidden' name='param_id' value='"&paramId&"'>"
				%>
					<table height="100%" cellspacing="0" cellpadding="3" class="data_table">
						<tr class="data_table_title">
							<td width="10"><input type='checkbox' name='select_all' id='select_all' onclick='javascript: select_all_objects();'></td>
							<td width="300" class="table_title"><%=LNG_NAME %></td>
							<td width="300" class="table_title"><%=LNG_VALUE %></td>
							<td width="100" class="table_title" width="135"><%=LNG_ACTIONS %></td>
						</tr>
				<%
				While Not RS.EOF
					strObjectID=RS("id")
					%>
						<tr>
							<td width="20" ><input type="checkbox" name="objects" value="'<%=strObjectID%>'"></td>
							<td width="300"><%=HTMLEncode(RS("name")) %></td>
							<td width="300"><%
							if RS("type") = "1" then
							%>
								<iframe security="restricted" src="alert_template_params_edit.asp?preview_value_id=<%=RS("id")%>" frameborder="0"></iframe>
							<%
							else
								Response.Write HTMLEncode(RS("value"))
							end if
							%></td>
							<td width="100">
								<a href="alert_template_params_edit.asp?id=<%=paramId %>&value_id=<%=RS("id") %>">
									<img src="images/action_icons/edit.png" alt="<%=LNG_EDIT %>" title="<%=LNG_EDIT %>" width="16" height="16" border="0" hspace=5>
								</a>
							</td>
						</tr>
					<%
					RS.MoveNext
				Wend
				%>
					</table>
					<br>
					<table border="0">
						<tr>
							<td>
							<% 
								Response.write "<br><A href='#' class='delete_button' onclick=""javascript: return LINKCLICK('"&linkclick_str&"');"">"&LNG_DELETE&"</a><br/><br/>"
								Response.write "</form>"
							%>
							</td>
						</tr>
					</table>
					<br>
					<br>
				<%
			end if
		else
			Set RS = Conn.Execute("select id, name, value, type from alert_param_selects where id = '"&Replace(valueId, "'", "''")&"'")
			addName = RS("name")
			addValue = RS("value")
			addType = RS("type")
		end if
%>
								<%=LNG_NAME %>: <input name="value_name" id="value_name" value="<%=HTMLEncode(addName)%>"></input><br><br>
								<%=LNG_VALUE %>:
								<div id="tabs">
									<ul>
										<li><a href="#tabs-1">Row</a></li>
										<li><a href="#tabs-2">HTML</a></li>
									</ul>
									<div id="tabs-1">
										<input id="row_value" value="<% if (addType = 0) then Response.Write HTMLEncode(addValue) end if%>"></input>
									</div>
									<div id="tabs-2">
										<textarea id="elm1" name="elm1" rows="15" cols="80" style="width: 100%">
										<% if (addType = 1) then Response.Write HTMLEncode(addValue) end if%>
										</textarea>
										<%		
											Set RS_params = Conn.Execute("SELECT name, short_name FROM alert_parameters WHERE temp_id = (SELECT temp_id FROM alert_parameters WHERE id = '"&paramId&"') ORDER by [id]")
											if (Not RS_params.EOF) then
										%>	
											<div style="margin-top:10px;">
												<select id="params_select">
												<%
													While Not RS_params.EOF
												%>		
													<option value="<%=RS_params("short_name")%>"><%=RS_params("short_name")%></option>	
												<%
														RS_params.MoveNext	
													Wend
												%>
												</select>
												<a class="insert_in_text_param_button" href="javascript: insertParamInText()"><%=LNG_INSERT_INTO_TEXT%></a>
											</div>
										<%	
											end if	
											RS_params.Close
										%>
									</div>
								</div>
								<div class="ui-state-error" id="dialog" style="display:none;"> 
									<p id="dialog_text" ></p> 
								</div> 
							<br>
							<table border="0">
								<tr>
									<form method="post" action="alert_template_params_edit.asp" id="add_param_value">
										<input type="hidden" id="add_name" value="<%=HTMLEncode(addName)%>" name="add_name"/>
										<input type="hidden" id="add_value" value="" name="add_value"/>
										<input type="hidden" name="id" value="<%=paramId%>"/>
										<input type="hidden" name="add_type" id="add_type" value="<%=paramId%>"/>
										<input type="hidden" name="value_id" id="add_value_id" value="<%=valueId%>"/>
									</form>
									<td style="padding:0px; margin:0px">
									<% 
										if (valueId<>"") then saveButton = LNG_SAVE else saveButton = LNG_ADD end if 
										Response.Write "<A class='add_button' onclick=""javascript: addParamValue()"">"&saveButton&"</a>" 
									%>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<script>
	$(function() {
		$( "#tabs" ).tabs({
			selected: <%if (addType<>"") then Response.Write addType else Response.Write 0 end if%>
		});
		
	});
	</script>
<%
end if
%>
</body>

</html>
