<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="ad.inc" -->
<%

check_session()

%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>View survey details</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
<link href="style.css" rel="stylesheet" type="text/css">
</head>
<script language="javascript">
function LINKCLICK() {
  alert ("This action is disabled in demo");
  return false;
}
</script>

<body style="margin:0" class="body">
<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	<tr>
	<td>
	<table width="100%" height="100%" border="1" bordercolor="#000000" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
		<tr>
		<td width=100% height=31 class="theme-colored-back"><a href="" class="header_title">Survey details</a></td>
		</tr>
		<tr>
		<td bgcolor="#ffffff" height="100%">
		<br>
		<div style="margin:10px;">

		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr valign="top"><td>&nbsp;</td><td><b>Preview:</td><td><iframe  STYLE="border: 1px solid black;" src="<% Response.Write alerts_folder & "survey_preview.asp?id=" & Request("id") %>" width="200" height="200" frameborder="0"></iframe><br><br></td></tr>

<%
	Set RS = Conn.Execute("SELECT id, name, type, convert(varchar,create_date) as create_date, sender_id FROM surveys_main WHERE closed='Y' AND id="&request("id"))
	num=0
	if(Not RS.EOF) then
		alertId = RS("sender_id")
		if(RS("type")="B") then
			mytype="Broadcast"
			myrecipients="All"
		end if
		if(RS("type")="G") then
			mytype="Group"
			Set RS6 = Conn.Execute("SELECT name FROM groups INNER JOIN alerts_group_stat ON groups.id=alerts_group_stat.group_id WHERE alerts_group_stat.alert_id="&alertId)
			do while not RS6.EOF
				myrecipients=myrecipients & RS6("name")
				RS6.MoveNext
				if (Not RS6.EOF) then
					myrecipients=myrecipients & ", "
				end if
			loop
		end if
		mysentby=""
		if(Not IsNull(RS("sender_id"))) then
			Set RS7 = Conn.Execute("SELECT name FROM users WHERE id="&RS("sender_id"))	
			if(Not RS7.EOF) then
				mysentby=RS7("name")
			end if
		end if
		mysent=0
		  Set RS7 = Conn.Execute("SELECT COUNT(id) as mycnt FROM [alerts_sent_stat] WHERE [date] >= '" + from_date + "' AND [date] <= '" + to_date + "' AND alert_id="&alertId)
		  if(not RS7.EOF) then
			mysent=RS7("mycnt")
		  end if

		  myreceived=0
		  myvotes=0
		  Set RS7 = Conn.Execute("SELECT COUNT(id) as mycnt, SUM(vote) as vote_cnt FROM [alerts_received] WHERE [date] >= '" + from_date + "' AND [date] <= '" + to_date + "' AND alert_id="&alertId)
		  if(not RS7.EOF) then
			myreceived=RS7("mycnt")
			myvotes=RS7("vote_cnt")
		  end if
		  if(IsNull(myvotes)) then myvotes=0 end if

		  if(mysent<>0) then
		    myopen=Round(clng(myreceived)/clng(mysent)*100,2)
		  else
		    myopen="0"
		  end if
		 if(myreceived<>0) then
		    myctr=Round(clng(myvotes)/clng(myreceived)*100,2)
		  else
		    myctr="0"
		  end if
		 Response.Write "<tr><td>&nbsp;</td><td> <b>Date: </b></td><td>"& RS("create_date") &"</td></tr>"
		 Response.Write "<tr><td>&nbsp;</td><td> <b>Type: </b></td><td>"& mytype &"</td></tr>"
		 Response.Write "<tr><td>&nbsp;</td><td> <b>Recipients: </b></td><td>"& myrecipients &"</td></tr>"
		 Response.Write "<tr><td>&nbsp;</td><td> <b>Sent by: </b></td><td>"& mysentby &"</td></tr>"
		 Response.Write "<tr><td>&nbsp;</td><td> <b>Sent: </b></td><td>"& mysent &"</td></tr>"
		 Response.Write "<tr><td></td><td> <b>Received: </b></td><td>"& myreceived &" </td></tr>"
		 Response.Write "<tr><td></td><td> <b>Read: </b></td><td>"& myvotes &"</td></tr>"
		 Response.Write "<tr><td>&nbsp;</td><td> <b>CTR: </b></td><td>"& myctr &"%</td></tr>"
	else
	Response.Write "<tr><td> Error: No such survey in database! </td></tr>"
	end if
	RS.Close
%>

		
		</table>
		 <img src="images/inv.gif" alt="" width="1" height="100%" border="0">
		</div>
		</td>
		</tr>
	</table>
	</td>
	</tr>
</table>
</body>
</html>
<!-- #include file="db_conn_close.asp" -->