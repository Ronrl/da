﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Parameters;


namespace DeskAlertsDotNet.Pages
{
    public partial class UploadFileAction : DeskAlertsBasePage
    {
        //use curUser to access to current user of DA control panel      
        //use dbMgr to access to database

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            string action = Request.GetParam("action", "");
            string path = Request.GetParam("file_path", "");
            if (action.Length == 0 || path.Length == 0)
                return;

            if (action == "confirm_delete")
            {
                Response.Write(GenerateConfirmMessage(path));
            }
            else if (action == "delete")
            {
                DeleteFile(path);
            }
            //Add your main code here
        }

        private void DeleteFile(string path)
        {
            string fullPath = Config.Data.GetString("alertsFolder") + "\\admin\\" + path;

            if(File.Exists(fullPath))
                File.Delete(fullPath);
        }

        private string GenerateConfirmMessage(string path)
        {

            string message = "Are you sure you wish to delete this file? ";
            if (!dbMgr.GetDataByQuery("SELECT id FROM alerts WHERE alert_text LIKE N'%" + path + "%'").IsEmpty)
            {
                message += "This file is used in alerts.<br>";
            }

            if (!dbMgr.GetDataByQuery("SELECT id FROM text_templates WHERE template_text LIKE N'%" + path + "%'").IsEmpty)
            {
                message += "This file is used in templates.<br>";
            }



            return message;
        }
    }
}