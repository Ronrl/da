﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DeskAlertsDotNet.DataBase;
using WordPressSharp;
using WordPressSharp.Models;

namespace DeskAlertsDotNet
{
    public class WordPressManager
    {
        private static WordPressManager _instance;
        public static WordPressManager Default
        {
            get
            {
                if (_instance == null)
                    return _instance = new WordPressManager();

                return _instance;
            }
        }

        private WordPressManager()
        {
            
        }
        public void NewPost(DBManager dbMgr,
             string title, string text)
        {
            DataSet blogSet = dbMgr.GetDataByQuery("SELECT * FROM blogs");

            foreach (DataRow dataRow in blogSet)
            {
                var post = new Post
                {
                    PostType = "post",
                    Title = title,
                    Content = text,
                    PublishDateTime = DateTime.Now
                };
                using (var client = new WordPressClient(new WordPressSiteConfig
                {
                    BaseUrl = dataRow.GetString("url"),
                    Username = dataRow.GetString("username"),
                    Password = dataRow.GetString("password"),
                    BlogId = dataRow.GetInt("blogID")
                }))
                {
                    client.NewPost(post);
                }


            }
        }


    }
}