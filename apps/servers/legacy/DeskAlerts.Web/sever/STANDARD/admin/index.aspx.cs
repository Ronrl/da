﻿using System;
using System.Data;
using DeskAlertsDotNet.Managers;
using DeskAlertsDotNet.Models;
using DeskAlertsDotNet.Utils.Factories;


namespace DeskAlertsDotNet.Pages
{
    public partial class Index : DeskAlertsBasePage
    {
        protected User _user;

        protected string userStartPage;
        private const string DefaultStartPage = "dashboard.aspx";
        private const string SqlQuery = "SELECT start_page FROM users WHERE id = @userId";

        protected override void OnLoad(EventArgs e)
        {
            _user = UserManager.Default.CurrentUser;

            base.OnLoad(e);

            var currentUserId = curUser?.Id;

            if (currentUserId != null && currentUserId != default(long))
            {
                var sqlQueryParameter = SqlParameterFactory.Create(DbType.Int64, currentUserId, "@userId");
                userStartPage = dbMgr.GetDataByQuery(SqlQuery, sqlQueryParameter)?.First().GetString("start_page");

                if (string.IsNullOrEmpty(userStartPage))
                {
                    userStartPage = DefaultStartPage;
                }
            }
            else
            {
                userStartPage = DefaultStartPage;
            }
        }
    }
}