<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%

check_session()
Response.Expires = 0
Response.Expiresabsolute = Now() - 1 
Response.AddHeader "pragma","no-cache" 
Response.AddHeader "cache-control","private" 
Response.CacheControl = "no-cache" 
Response.ContentType = "text/html; charset=utf-8"

  uid = Session("uid")

if (uid <>"") then

   if(Request("offset") <> "") then 
	offset = clng(Request("offset"))
   else 
	offset = 0
   end if	

   if(Request("uname") <> "") then 
	myuname = Request("uname")
   else 
	myuname = ""
   end if	

   if(Request("limit") <> "") then 
	limit = clng(Request("limit"))
   else 
	limit=50
   end if
  

	Set RS = Conn.Execute("SELECT id FROM users WHERE id="&uid&" AND role='E'")
	if(Not RS.EOF) then
		gid = 1
		'Set rs_policy = Conn.Execute("SELECT type FROM policy INNER JOIN policy_editor ON policy.id=policy_editor.policy_id WHERE editor_id=" & uid)
		Set rs_policy = Conn.Execute("SELECT p.id, p.type, pu.comp_id FROM policy p INNER JOIN policy_editor pe ON p.id=pe.policy_id LEFT JOIN policy_computer pu ON pu.policy_id = p.id AND pu.comp_id = 0 WHERE pe.editor_id=" & uid)
		if(Not rs_policy.EOF) then
			'if(rs_policy("type")="A") then
			if rs_policy("type")="A" or not IsNull(rs_policy("comp_id")) then 'can send to all
				gid = 0
			end if
		end if
		if(gid=1) then
			editor_id=RS("id")
			policy_ids=""
			policy_ids=editorGetPolicyIds(editor_id)
			group_ids=""
			group_ids=editorGetGroupIds(policy_ids)
			group_ids=replace(group_ids, "group_id", "computers_groups.group_id")
			if(group_ids <> "") then
				group_ids = "OR " & group_ids
			end if
			ou_ids=""
			ou_ids=editorGetOUIds(policy_ids)
			ou_ids=replace(ou_ids, "ou_id", "OU_comp.Id_OU")
			if(ou_ids <> "") then
				ou_ids = "OR " & ou_ids
			end if
		end if
	else
		gid = 0
	end if
	RS.Close

	if(myuname<>"") then
		if(gid<>0) then
			Set RS = Conn.Execute("SELECT COUNT(DISTINCT computers.id) as cnt FROM computers LEFT JOIN policy_computer ON computers.id=policy_computer.comp_id LEFT JOIN computers_groups ON computers.id=computers_groups.comp_id LEFT JOIN OU_comp ON computers.id=OU_comp.Id_comp WHERE name LIKE N'%"&Replace(myuname, "'", "''")&"%' AND ("&policy_ids&" "&group_ids&" "&ou_ids&")")
		else
			Set RS = Conn.Execute("SELECT COUNT(id) as cnt FROM computers WHERE name LIKE N'%"&Replace(myuname, "'", "''")&"%'")
		end if
	else
		if(gid<>0) then
			Set RS = Conn.Execute("SELECT COUNT(DISTINCT computers.id) as cnt FROM computers LEFT JOIN policy_computer ON computers.id=policy_computer.comp_id LEFT JOIN computers_groups ON computers.id=computers_groups.comp_id LEFT JOIN OU_comp ON computers.id=OU_comp.Id_comp WHERE ("&policy_ids&" "&group_ids&" "&ou_ids&")")
		else
			Set RS = Conn.Execute("SELECT COUNT(id) as cnt FROM computers")
		end if
	end if
	cnt=0

	if(Not RS.EOF) then
		cnt=RS("cnt")
        end if
	RS.Close
  j=cnt/limit


if(cnt>0) then

	page="get_computers.asp?uname=" & myuname & "&"
	name= LNG_COMPUTERS &"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
	
	if(limit=50) then response.write LNG_YOU_HAVE & " " & cnt & " "&name&" | 50 | <A href=""#"" onclick=""javascript: page('get_computers.asp?offset=0&limit=100&uname="&myuname&"');"">100</a> | <A href=""#"" onclick=""javascript: page('get_computers.asp?offset=0&limit=500&uname="&myuname&"');"">500</a> | <A href=""#"" onclick=""javascript: page('get_computers.asp?offset=0&limit=1000&uname="&myuname&"');"">1000</a></td></tr>" end if
	if(limit=100) then response.write LNG_YOU_HAVE & " " & cnt & " "&name&" | <A href=""#"" onclick=""javascript: page('get_computers.asp?offset=0&limit=50&uname="&myuname&"');"">50</a> | 100 | <A href=""#"" onclick=""javascript: page('get_computers.asp?offset=0&limit=500&uname="&myuname&"');"">500</a> | <A href=""#"" onclick=""javascript: page('get_computers.asp?offset=0&limit=1000&uname="&myuname&"');"">1000</a></td></tr>" end if
	if(limit=500) then response.write LNG_YOU_HAVE & " " & cnt & " "&name&" | <A href=""#"" onclick=""javascript: page('get_computers.asp?offset=0&limit=50&uname="&myuname&"');"">50</a> | <A href=""#"" onclick=""javascript: page('get_computers.asp?offset=0&limit=100&uname="&myuname&"');"">100</a> | 500 | <A href=""#"" onclick=""javascript: page('get_computers.asp?offset=0&limit=1000&uname="&myuname&"');"">1000</a></td></tr>" end if
	if(limit=1000) then response.write LNG_YOU_HAVE & " " & cnt & " "&name&" | <A href=""#"" onclick=""javascript: page('get_computers.asp?offset=0&limit=50&uname="&myuname&"');"">50</a> | <A href=""#"" onclick=""javascript: page('get_computers.asp?offset=0&limit=100&uname="&myuname&"');"">100</a> | <A href=""#"" onclick=""javascript: page('get_computers.asp?offset=0&limit=500&uname="&myuname&"');"">500</a> | 1000</td></tr>" end if
	response.write "<br/>"
	Response.Write make_pages_ajax(offset, cnt, limit, page, name) 
	%>
			<table class='data_table' cellspacing='0' cellpadding='3' class='table_get'>
			<tr class='data_table_title'><td class='table_title'>
			<input type='checkbox' name='select_all' id='select_all' onclick='javascript: select_all_users();'></td>
			<td class='table_title'><%=LNG_COMPUTER_NAME %></td>
			<% if(AD=3) then %>
			<td class='table_title'><%=LNG_DOMAIN %></td>
			<%	end if 	%>
			</tr>
	<%
	
	
	if(myuname<>"") then
		if(gid<>0) then
			Set RS = Conn.Execute("SELECT * FROM (SELECT *, ROW_NUMBER() OVER (order by name) as rnum FROM (SELECT DISTINCT computers.id as id, name,  domain_id, context_id FROM computers LEFT JOIN policy_computer ON computers.id=policy_computer.comp_id LEFT JOIN computers_groups ON computers.id=computers_groups.comp_id LEFT JOIN OU_comp ON computers.id=OU_comp.Id_comp WHERE name LIKE N'%"&Replace(myuname, "'", "''")&"%' AND ("&policy_ids&" "&group_ids&" "&ou_ids&")) computers) computers WHERE rnum Between "&(offset+1)&" and "&(offset+limit)&" ")	
		else
			Set RS = Conn.Execute("SELECT * FROM (SELECT id, name, domain_id, context_id, ROW_NUMBER() OVER (order by name) as rnum FROM computers WHERE name LIKE N'%"&Replace(myuname, "'", "''")&"%') computers WHERE rnum Between "&(offset+1)&" and "&(offset+limit))
		end if
	else
		if(gid<>0) then
			Set RS = Conn.Execute("SELECT * FROM (SELECT *, ROW_NUMBER() OVER (order by name) as rnum FROM (SELECT DISTINCT computers.id as id, name, domain_id, context_id FROM computers LEFT JOIN policy_computer ON computers.id=policy_computer.comp_id LEFT JOIN computers_groups ON computers.id=computers_groups.comp_id LEFT JOIN OU_comp ON computers.id=OU_comp.Id_comp WHERE ("&policy_ids&" "&group_ids&" "&ou_ids&")) computers) computers WHERE rnum Between "&(offset+1)&" and "&(offset+limit)&" ")	
		else
			Set RS = Conn.Execute("SELECT * FROM (SELECT id, name, domain_id, context_id, ROW_NUMBER() OVER (order by name) as rnum FROM computers) computers WHERE rnum Between "&(offset+1)&" and "&(offset+limit)&" ")	
		end if
	end if
	num=0
	Do While Not RS.EOF
		strUserName = RS("name")
		strUserID = RS("id")
		strDomainName=""
		if(AD=3) then
			if not IsNull(RS("domain_id")) then
				domain_id = RS("domain_id")
				Set RSDomain = Conn.Execute("SELECT name FROM domains WHERE id="&domain_id)
				if(Not RSDomain.EOF) then
					strDomainName = RSDomain("name") 
				end if
			end if

			if (EDIR = 1) then
				if not IsNull(RS("context_id")) then
					context_id = RS("context_id")
					Set RS3 = Conn.Execute("SELECT name FROM edir_context WHERE id=" & context_id)
					if(Not RS3.EOF) then
						strContext=RS3("name")
						RS3.close
					else
						strContext="&nbsp;"
					end if
					strContext=replace (strContext,",ou=",".")
					strContext=replace (strContext,",o=",".")
					strContext=replace (strContext,",dc=",".")
					strContext=replace (strContext,",c=",".")

					strContext=replace (strContext,"ou=","")
					strContext=replace (strContext,"o=","")
					strContext=replace (strContext,"dc=","")
					strContext=replace (strContext,"c=","")
					strDomainName = strDomainName & " / " & strContext
				end if
			end if
		end if
%>
		<tr><td><input type="checkbox" id="users_<%=strUserID%>" name="users" value="<%=strUserID%>"></td><td><input type="hidden" name="users_name" value="<%=strUserName%>"><label for="users_<%=strUserID%>"><%=strUserName%></label></td>
	<% if(AD=3) then%>
		<td><%=strDomainName%></td>
	<% end if %>
		</tr>
<%
	RS.MoveNext
	Loop
	RS.Close
	response.write "</table>"

	Response.Write make_pages_ajax(offset, cnt, limit, page, name) 

else

Response.Write "<center><b>"&LNG_THERE_ARE_NO_COMPUTERS &".</b><br><br></center>"

end if
else

Response.Write "<center><b>"&LNG_THERE_ARE_NO_COMPUTERS &".</b><br><br></center>"

end if
%>
<!-- #include file="db_conn_close.asp" -->