﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Parameters;

namespace DeskAlertsDotNet.Pages
{
    using Resources;

    public partial class GroupEdit : DeskAlertsBasePage
    {
        private DBManager _dbManager;
        private bool _edit;
        private string _id;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["PreviousPage"] = Request.UrlReferrer;
            }

            _edit = Request.QueryString.AllKeys.Contains("id");
            _id = _edit ? Request.QueryString["id"] : null;

            lbTitle.Text = resources.LNG_GROUPS;
            lbHeader.Text = _edit
                ? resources.LNG_EDIT_GROUPS_POLICY
                : resources.LNG_ADD_GROUP_POLICY;
            lbSelectGroup.Text = resources.LNG_SELECT_GROUP;
            lbSelectPolicy.Text = resources.LNG_SELECT_POLICY;
            btnCancel.Text = resources.LNG_CANCEL;
            btnSave.Text = resources.LNG_SAVE;

            _dbManager = new DBManager();

            ddlGroups.DataSource = _dbManager.GetDataTableByQuery("SELECT id, name FROM groups");
            ddlGroups.Items.Insert(0, new ListItem(resources.LNG_SELECT_GROUP));
            ddlGroups.DataTextField = "name";
            ddlGroups.DataValueField = "id";
            ddlGroups.DataBind();

            ddlPolicy.DataSource = _dbManager.GetDataTableByQuery("SELECT id, name FROM policy");
            ddlPolicy.Items.Insert(0, new ListItem(resources.LNG_SELECT_POLICY));
            ddlPolicy.DataTextField = "name";
            ddlPolicy.DataValueField = "id";
            ddlPolicy.DataBind();

            if (!IsPostBack && _edit)
            {
                var data =
                    _dbManager.GetDataByQuery(
                        string.Format("SELECT id, group_id, policy_id FROM groups_policy WHERE id = {0}", _id));
                var groupId = data.First().GetString("group_id");
                var policyId = data.First().GetString("policy_id");

                ddlGroups.SelectedValue = ddlGroups.Items.FindByValue(groupId).Value;
                ddlPolicy.SelectedValue = ddlPolicy.Items.FindByValue(policyId).Value;
            }
        }

        protected void btnSave_OnServerClick(object sender, EventArgs e)
        {
            var tableName = "groups_policy";
            var groupId = ddlGroups.SelectedItem.Value;
            var policyId = ddlPolicy.SelectedItem.Value;

            var row = new Dictionary<string, object>
            {
                {"group_id", groupId},
                {"policy_id", policyId},
                {"was_checked", 1}
            };

            var rowExists =
                _dbManager.GetScalarByQuery<int>(
                    string.Format("SELECT COUNT(1) FROM groups_policy WHERE group_id = {0} AND policy_id = {1}", groupId,
                        policyId)) > 0;

            if (rowExists)
            {
                ShowPopup(string.Format("Policy {0} for group {1} already added.", ddlPolicy.SelectedItem.Text,
                    ddlGroups.SelectedItem.Text));
                return;
            }

            if (_edit)
            {
                var condition = new Dictionary<string, object>
                {
                    {"id", _id}
                };
                _dbManager.Update(tableName, row, condition);
            }
            else
            {
                _dbManager.Insert(tableName, row, "id");
            }

            if (ViewState["PreviousPage"] != null)
            {
                Response.Redirect(ViewState["PreviousPage"].ToString());
            }
        }

        private void ShowPopup(string message)
        {
            string script = string.Format("alert('{0}');", message);
            if (Page != null && !Page.ClientScript.IsClientScriptBlockRegistered("alert"))
            {
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "alert", script, true);
            }
        }

        protected void btnCancel_OnClick(object sender, EventArgs e)
        {
            if (ViewState["PreviousPage"] != null)
            {
                Response.Redirect(ViewState["PreviousPage"].ToString());
            }
        }
    }
}