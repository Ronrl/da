﻿using System;
using DeskAlertsDotNet.Parameters;

namespace DeskAlertsDotNet.Pages
{
    public partial class Statistics : DeskAlertsBasePage
    {
        //use curUser to access to current user of DA control panel      
        //use dbMgr to access to database

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            alertsTab.Visible = Config.Data.HasModule(Modules.STATISTICS);
            alerts.Visible = Config.Data.HasModule(Modules.STATISTICS);

            usersTab.Visible = Config.Data.HasModule(Modules.STATISTICS);
            users.Visible = Config.Data.HasModule(Modules.STATISTICS);

            surveysTab.Visible = Config.Data.HasModule(Modules.STATISTICS) && Config.Data.HasModule(Modules.SURVEYS);
            surveys.Visible = Config.Data.HasModule(Modules.STATISTICS) && Config.Data.HasModule(Modules.SURVEYS);

            rssTab.Visible = Config.Data.HasModule(Modules.STATISTICS) && Config.Data.HasModule(Modules.RSS);
            rss.Visible = Config.Data.HasModule(Modules.STATISTICS) && Config.Data.HasModule(Modules.RSS);

            wpTab.Visible= Config.Data.HasModule(Modules.STATISTICS) && Config.Data.HasModule(Modules.WALLPAPER);
            wp.Visible = Config.Data.HasModule(Modules.STATISTICS) && Config.Data.HasModule(Modules.WALLPAPER);

            ssTab.Visible = Config.Data.HasModule(Modules.STATISTICS) && Config.Data.HasModule(Modules.SCREENSAVER);
            ss.Visible = Config.Data.HasModule(Modules.STATISTICS) && Config.Data.HasModule(Modules.SCREENSAVER);
        }
    }
}