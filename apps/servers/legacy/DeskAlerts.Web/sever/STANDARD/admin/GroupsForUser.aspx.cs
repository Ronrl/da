﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Parameters;

namespace DeskAlertsDotNet.Pages
{
    using Resources;

    public partial class GroupsForUser : DeskAlertsBaseListPage
    {

        public string UserId { get; set; }
        protected override void OnLoad(EventArgs e)
        {
            UserId = Request.GetParam("id", "");

            if(string.IsNullOrEmpty(UserId))
                return;
            
            base.OnLoad(e);

            groupNameHeaderCell.Text = resources.LNG_GROUP_NAME;


            int cnt = this.Content.Count < (Offset + Limit) ? this.Content.Count : Offset + Limit;

            for (int i = Offset; i < cnt; i++)
            {
                DataRow row = this.Content.GetRow(i);
                TableRow tableRow = new TableRow();

                TableCell nameCell = new TableCell()
                {
                    Text = row.GetString("name")
                };

                tableRow.Cells.Add(nameCell);
                contentTable.Rows.Add(tableRow);
            }

        }

        #region DataProperties 
        protected override string DataTable
        {
            get
            {

                //return your table name from this property
                return String.Empty;
                
            }
        }

        protected override string[] Collumns
        {
            get { return new string[] { }; }
        }

        protected override string DefaultOrderCollumn
        {
            //return collumn for default order
            get { return "id"; }
        }

        /*
         * 
         * If you want to add condition for data selection override property ConditionSqlString 
         * Also you can implement your custom query by overriding properties CustomQuery and CustomCountQuery
         *  
         */
        #endregion

        #region Interface properties

        protected override IButtonControl[] DeleteButtons
        {
            get
            {
                return new IButtonControl[] { };
            }
        }


        protected override string CustomQuery
        {
            get
            {
                return
                    "SELECT groups.name as name FROM groups INNER JOIN users_groups ON groups.id=users_groups.group_id WHERE users_groups.user_id=" +
                    UserId;
            }
        }

        protected override string CustomCountQuery
        {
            get { return "SELECT COUNT(group_id) as mycnt FROM groups INNER JOIN users_groups ON groups.id=users_groups.group_id WHERE users_groups.user_id=" + UserId; }
        }


        protected override System.Web.UI.HtmlControls.HtmlGenericControl NoRowsDiv
        {
            get { return NoRows; }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl TableDiv
        {
            get { return mainTableDiv; }
        }

        protected override string ContentName
        {
            get
            {
                //return content name here
                return resources.LNG_GROUP_NAME;
            }
        }


        protected override HtmlGenericControl BottomPaging
        {
            get { return bottonPaging; }
        }

        protected override HtmlGenericControl TopPaging
        {
            get { return topPaging; }
        }

        #endregion
    }
}