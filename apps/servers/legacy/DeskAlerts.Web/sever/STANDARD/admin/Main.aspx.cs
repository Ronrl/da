﻿using System;
using DeskAlertsDotNet.Licensing;


namespace DeskAlertsDotNet.Pages
{
    public partial class Main : DeskAlertsBasePage
    {
        //use curUser to access to current user of DA control panel      
        //use dbMgr to access to database
        protected int notApporvedAlertsCount;
        protected int rejectedAlertsCount;
        protected long ellapse;
        protected long trial;
        

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            notApporvedAlertsCount = MenuManager.Default.GetNotApprovedAlertsCount(dbMgr, curUser);
            rejectedAlertsCount = MenuManager.Default.GetRejectedAlertsCount(dbMgr, curUser);
            ellapse = LicenseManager.Default.GetServerLifeTime()/60/60/24;
            trial = LicenseManager.Default.GetTrialTime(dbMgr);
            //Add your main code here
        }
    }
}