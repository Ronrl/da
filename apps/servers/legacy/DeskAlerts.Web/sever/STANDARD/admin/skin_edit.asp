﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%

check_session()

'script for change alerts templates

uid = Session("uid")
captionName=""
captionHref=""

if (uid <> "")  then
	id = Replace(Request("id"),"'","''")
	if (id <> "") then
		SQL = "SELECT name, file_name FROM alert_captions WHERE id='"&id&"'"
		Set RS = Conn.Execute(SQL)
		if (not RS.EOF) then
			captionName = RS("name")
			captionHref = RS("file_name")		
		end if
		RS.Close
	end if

'show edit form
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>DeskAlerts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/style9.css" rel="stylesheet" type="text/css">
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
<style>
.ui-button-icon-only {font-size: 8px;}
input.text { margin-bottom:12px; width:95%; padding: .4em; }
fieldset { padding:0; border:0; margin-top:25px; }
h1 { font-size: 1.2em; margin: .6em 0; }
div#users-contain { width: 350px; margin: 20px 0; }
div#users-contain table { margin: 1em 0; border-collapse: collapse; width: 100%; }
div#users-contain table td, div#users-contain table th { border: 1px solid #eee; padding: .6em 10px; text-align: left; }
.ui-dialog .ui-state-error { padding: .3em; }
.validateTips { border: 1px solid transparent; padding: 0.3em; }
</style>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/preview.js?v=9.2.1.31"></script>
<script language="javascript" type="text/javascript" src="jscripts/skin_edit.js"></script>

<script language="javascript" type="text/javascript">

var caption_href = '<%=jsEncode(captionHref)%>';
var skin_id = '<%=jsEncode(id)%>'
var caption_name = '<%=jsEncode(captionName)%>';

var PREVIEW_JS_START = "<!--server_preview_section_start--"+">";
var PREVIEW_JS_END =  "<!--server_preview_section_end--"+">";

function showPrompt(text)
{
	$("#dialog-modal > p").text(text)
	$("#dialog-modal").dialog({
		height: 140,
		modal: true
	});
}


$(document).ready(function(){

	var captionName = $( "#caption_name" ),
	allFields = $( [] ).add( captionName ),
	tips = $( ".validateTips" );
	
	
	function updateTips( t ) {
		tips
			.text( t )
			.addClass( "ui-state-highlight" );
		setTimeout(function() {
			tips.removeClass( "ui-state-highlight", 1500 );
		}, 500 );
	}

	function checkLength( o, n, min, max ) {
		if ( o.val().length > max || o.val().length < min ) {
			o.addClass( "ui-state-error" );
			updateTips( "Length of " + n + " must be between " +
				min + " and " + max + "." );
			return false;
		} else {
			return true;
		}
	}

	function checkRegexp( o, regexp, n ) {
		if ( !( regexp.test( o.val() ) ) ) {
			o.addClass( "ui-state-error" );
			updateTips( n );
			return false;
		} else {
			return true;
		}
	}
		
	$( "#dialog-form" ).dialog({
		autoOpen: false,
		height: 300,
		width: 350,
		modal: true,
		buttons: {
			"Save": function() {
				var bValid = true;
				allFields.removeClass( "ui-state-error" );

				bValid = bValid && checkLength( captionName, "name", 3, 16 );
			
				bValid = bValid && checkRegexp( captionName, /^[a-z]([0-9a-z_])+$/i, "Name may consist of a-z, 0-9, underscores, begin with a letter." );
	
				if ( bValid ) {
					var dataString = "caption_name=" + encodeURIComponent(captionName.val())	+
					"&caption_html=" +  encodeURIComponent(getCaptionHtml()) +
					"&skin_id="+ skin_id; 
					$.ajax({
						type: 'POST',
						url: 'save_alert_caption.asp',
						data: dataString,
						success: function(data) {
							skin_id = data;
							caption_name = captionName.val();
							showPrompt('Saved');
						},
						async: true
					});
				}
			},
			Cancel: function() {
				$( this ).dialog( "close" );
			}
		},
		close: function() {
			allFields.val( "" ).removeClass( "ui-state-error" );
		}
	});
	
	bindLayersEvents();
	
	$("#param_tabs").tabs({
		fx: { opacity: 'toggle' }
	});
	
	$("ul.param_tabs a").css('height', $("ul.param_tabs").height());
	
	showEditPreview();
		
	$("#toggle_snap").button({
		icons: {
			primary: "ui-icon-arrowthickstop-1-e"
		},
		text: false
	}).click(function(){
		toggleSnap(getSelectedControlItems()[0]);
	});
	
	
	$("#save_button").button();
	$("#preview_button").button();
	$("#preview_button").click(function(){
		showAlertPreview();
	})
	
	
	$("#save_button").click(function(){
		$("#caption_name").val(caption_name);
		$( "#dialog-form" ).dialog( "open" );
	});
	

	clearControlPositions();
	$(".control_layer_inputs").prop('disabled','disabled');
	
});


function getCaptionHtml()
{
	var $frame = $("#skin_edit_frame").contents().find("#caption_frame");
	var frameWindow = ($frame[0].contentWindow) ? $frame[0].contentWindow : $frame[0].defaultView;


	for (var i = 0; i < controlItems.length; i++)
	{

		var controlItem = controlItems[i];
		controlItem.originalElement.css({
			position: controlItem.item.css('position'),
			top: controlItem.item.position().top,
			left : controlItem.item.css("left"),
			right : controlItem.item.css("right"),
			'z-index': controlItem.item.css('z-index')
		}).appendTo(controlItem.item.parent());
		
		if (controlItem.item.css('position')=="absolute")
		{
			controlItem.originalElement.width(controlItem.item.width());
			controlItem.originalElement.height(controlItem.item.height());
		}
		
		if (controlItem.item.attr('wordwrap')=='true')
		{
			controlItem.originalElement.css({
				"white-space":"normal"
			}).attr('wordwrap',true);
		}
		else
		{
			controlItem.originalElement.css({
				"white-space":"nowrap"
			}).attr('wordwrap',false);
		}
		
		if (controlItem.autowidth)
		{
			controlItem.originalElement.attr('autowidth',true);
		}
		else
		{
			controlItem.originalElement.attr('autowidth',false);
		}
		
		if (controlItem.autoleft)
		{
			controlItem.originalElement.attr('autoleft',true);
		}
		else
		{
			controlItem.originalElement.attr('autoleft',false);
		}
		
		if (controlItem.autoright)
		{
			controlItem.originalElement.attr('autoright',true);
		}
		else
		{
			controlItem.originalElement.attr('autoright',false);
		}
		
		controlItem.parent = controlItem.item.parent();

	
		controlItem.item = controlItem.item.appendTo($("#tmp_div"));
		controlItem.originalElement.show();
	}
	
	$frame.contents().find("#caption").text(frameWindow.external.getProperty("alert_header",""))

	var html = $frame.contents().find('html').html();
	
	var previewJsStarPos = html.indexOf(PREVIEW_JS_START);
	var previewJsEndPos = html.indexOf(PREVIEW_JS_END);
	html = html.substring(0,previewJsStarPos) + html.substring(previewJsEndPos + PREVIEW_JS_END.length);
	
	html = html.replace('div.alerttitle {width:100%!important;}','div.alerttitle {width:100%}')
	
	html = html.replace(/<\/head>/gi,"<sc"+"ript id='autosizeJs' type='text/JavaScript' language='JavaScript' src='../../../jscripts/skin/autosize.js'></sc"+"ript></head>")
	
	//html = html.replace(/function *?OnPageLoad\( *?\)(\t| |\n|\r)*?{/g,"function OnPageLoad() { try{ autoresize(document.getElementById('title'); autoresize(document.getElementById('create');)")

	html = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\"><!-- saved from url=(0026)http://www.softomate.com --><html>"
			+ html +
			"</html>";
	
	for (var i = 0; i < controlItems.length; i++)
	{
		var controlItem = controlItems[i];
		controlItem.originalElement.hide();
		controlItem.item.appendTo(controlItem.parent);
	}
	
	return html;
}

function showAlertPreview()
{
	var data = new Object();
	
	var caption_html = getCaptionHtml();

	
	data.create_date = "<%=al_create_date%>";

	var fullscreen =  false;
	var ticker =  0;
	var acknowledgement =  0; 
	
	var alert_width = 500;
	var alert_height = 400;
	var alert_title = "<%=LNG_TITLE%>";
	var alert_html = "";
	
	var top_template = "";
	var bottom_template = "";


	data.top_template = top_template;
	data.bottom_template = bottom_template;
	data.alert_width = alert_width;
	data.alert_height = alert_height;
	data.ticker = ticker;
	data.fullscreen = fullscreen;
	data.acknowledgement = acknowledgement;
	data.alert_title = alert_title;
	data.alert_html = alert_html;
	data.caption_html = caption_html;
	
	initAlertPreview(data);
	
	return false;
}



function showEditPreview()
{
	var data = new Object();
	
	data.create_date = "<%=al_create_date%>";


	var alert_width = 500
	var alert_height = 400
	var alert_title = "";
	var alert_html = "";
	
	var top_template = "";
	var bottom_template = "";

	var $editContainer= $("<div/>");
	$editContainer.attr({
		id: "skin_edit_container"
	}).css({
		margin : "0",
		padding : "0",
		border : "0",
		width: data.alert_width,
		height: data.alert_height,
		overflow: "hidden"
	});
	$editContainer.appendTo($("#skin_edit_preview_div"));

	data.top_template = top_template;
	data.bottom_template = bottom_template;
	data.alert_width = alert_width;
	data.alert_height = alert_height;
	data.ticker = 0;
	data.fullscreen = 0;
	data.acknowledgement = 0;
	data.alert_title = alert_title;
	data.alert_html = alert_html;
	data.caption_href = caption_href
	
	initEditPreview(data,$editContainer,$("#control_item_params_div"));
	
	return false;
}



</script>
</head>

<body style="margin:0px" class="body">
<div id="dialog-modal" title="" style='display:none'>
	<p></p>
</div>

<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	<tr>
	<td>
		<table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
			<tr>
				<td width=100% height=31 class="main_table_title"><a href="" class="header_title"><%=LNG_SKIN%></a></td>
			</tr>
			<tr>
				<td class="main_table_body" height="100%">
					<div style="margin:10px;">
						<span class="work_header"><%=LNG_EDIT_SKIN%></span>
						<br>
						<br>
						<table style='width:100%; height:100%; text-align:center; vertical-align:middle; border-collapse:collapse; padding:0px; margin:0px;'>
							<tr>
								<td>
									<div style="height:100%" id="skin_edit_preview_div"></div>
								</td>
								<td style="width:100%; height:100%; vertical-align:top;">
									<div id="skin_edit_params_div" style="width:350px;">
										<div id="param_tabs" style="width:350px">
											<ul>
												<li><a href="#actions_tab"><%=LNG_ACTIONS%></a></li>
												<li><a href="#layout_tab"><%=LNG_LAYOUT%></a></li>
											</ul>
											<div id="actions_tab" style="height:250px; text-align:left">
												<a id="toggle_snap"><%=LNG_TOGGLE_SNAP%></a>
												<div id="control_item_params_div"></div>
											</div>
											<div id="layout_tab" style="height:250px">
													<table style="height:100%; width:100%; background-color:red padding:0px; margin:0px; border-collapse:collapse">
														<tr>
															<td style="text-align:center; vertical-align:middle;">
																<div>
																<table style="border:1px solid #375f91; height:240px; background-color:#b9cde6; width:280px; padding:0px; margin:0px; border-collapse:collapse;  margin-left:auto; margin-right:auto;">
																	<tr style="height:60px">
																		<td>
																		</td>
																		<td>
																		<%=LNG_TOP%>:&nbsp;<input class="control_layer_inputs" id="top_control_position_input" style="width:30px"></input>&nbsp;px
																		</td>
																		<td>
																		</td>
																	</tr>
																	<tr style="height:120px">
																		<td style="width:85px">
																		<%=LNG_LEFT%>:&nbsp;<input class="control_layer_inputs" id="left_control_position_input" style="width:30px"></input>&nbsp;px
																		<br>
																		<input class="control_layer_inputs control_layer_checkboxes" id="auto_left_control_checkbox" type="checkbox"><%=LNG_AUTO%></input>&nbsp;<img src="images/help.png" title="Setting this to auto means that the value will be calculated to fill unused space.">
																		</td>
																		<td style="border:1px solid #375f91; background-color:#dce6f0">
																		<%=LNG_WIDTH%>:&nbsp;<input class="control_layer_inputs" id="width_control_input" style="width:30px"></input>&nbsp;px
																		<br>
																		<input class="control_layer_inputs control_layer_checkboxes" id="auto_width_control_checkbox" type="checkbox"><%=LNG_AUTO%></input>&nbsp;<img src="images/help.png" title="Setting this to auto means that the value will be calculated to fill unused space.">
																		<br>
																		<%=LNG_HEIGHT%>:&nbsp;<input class="control_layer_inputs" id="height_control_input" style="width:30px"></input>&nbsp;px
																		</td>
																		<td style="width:85px">
																		<%=LNG_RIGHT%>:&nbsp;<input class="control_layer_inputs" id="right_control_position_input" style="width:30px"></input>&nbsp;px
																		<br>
																		<input class="control_layer_inputs control_layer_checkboxes" id="auto_right_control_checkbox" type="checkbox"><%=LNG_AUTO%></input>&nbsp;<img src="images/help.png" title="Setting this to auto means that the value will be calculated to fill unused space.">
																		</td>
																	</tr>
																	<tr style="height:60px">
																		<td>
																		</td>
																		<td>
																		<%=LNG_BOTTOM%>:&nbsp;<input class="control_layer_inputs" id="bottom_control_position_input" style="width:30px"></input>&nbsp;px
																		</td>
																		<td>
																		</td>
																	</tr>
																</table>
																</div>
															</td>
														</tr>
													</table>
											</div>
										</div>
									</div>
								</td>
							</tr>
							<tr>
								<td style="vertical-align:top;">
									<a id='save_button'><%=LNG_SAVE%></a>
									<a id='preview_button'><%=LNG_PREVIEW%></a>
								</td>
							</tr>
						</table>
						
						<img src="images/inv.gif" alt="" width="1" height="100%" border="0">
					</div>
				</td>
			</tr>
		</table>
	</td>
	</tr>
</table>
<div id="tmp_div" style="display:none"></div>
<div id="dialog-form" title="<%=LNG_SAVE%>">
	<p class="validateTips"></p>
	<form>
	<fieldset>
		<label for="caption_name"><%=LNG_NAME%></label>
		<input type="text" name="caption_name" id="caption_name" class="text ui-widget-content ui-corner-all" value="<%=HtmlEncode(captionName)%>"/>
	</fieldset>
	</form>
</div>

<%

else
  Response.Redirect "index.asp"

end if
%> 
</body>
</html>
<!-- #include file="db_conn_close.asp" -->