<%
Response.CodePage = 65001
Response.CharSet = "utf-8"
%>
<!-- #include file="upload.asp" -->
<%

Dim strFolder, bolUpload, strMessage
Dim httpref, lngFileSize
Dim strIncludes, strExcludes

%>
<!-- #include file = "config.inc" -->
<%
Response.CodePage = 65001
Server.ScriptTimeout = 3600

 On Error Resume Next

if not isEmpty(foldersufix) then
	strFolder=strFolder & foldersufix
	httpRef=httpRef & foldersufix
end if

shorthttpref = Replace(httpref, alerts_folder & "admin/", "")

' Create the FileUploader
Dim Uploader, File
Set Uploader = New FileUploader

' This starts the upload process
Uploader.Upload()

'******************************************
' Use [FileUploader object].Form to access 
' additional form variables submitted with
' the file upload(s). (used below)
'******************************************

' Check if any files were uploaded
If Uploader.Files.Count = 0 Then
	strMessage = "No file entered."
Else
	' Loop through the uploaded files
	For Each File In Uploader.Files.Items

		bolUpload = false

		if lngFileSize = 0 then
			bolUpload = true
		else		
			if File.FileSize > lngFileSize then
				bolUpload = false
				strMessage = "File too large"
			else
				bolUpload = true
			end if
		end if

		if bolUpload = true then
			'Check to see if file extensions are excluded
			If strExcludes <> "" Then
				If ValidFileExtension(File.FileName, strExcludes) Then
					strMessage = "It is not allowed to upload a file containing a [." & GetFileExtension(File.FileName) & "] extension"
					bolUpload = false
				End If
			End If
			'Check to see if file extensions are included
			If strIncludes <> "" Then
				If InValidFileExtension(File.FileName, strIncludes) Then
					strMessage = "It is not allowed to upload a file containing a [." & GetFileExtension(File.FileName) & "] extension"
					bolUpload = false
				End If
			End If			
		end if

   
		 if bolUpload = true then
			File.FileName = GetFileName(strFolder, File.FileName)
			File.SaveToDisk strFolder ' Save the file
			
			if InStr(strFolder,"upload_video") > 0 then
			 'convert video to mp4
			    Dim oHTTP
			    Dim HTTPPost
                Dim sRequest

			    sRequest=Server.URLEncode(Base64Encode("{"&Chr(34)&"Function"&Chr(34)&":"&Chr(34)&"ConvertVideo"&Chr(34)&","&Chr(34)&"JsonString"&Chr(34)&":{"&Chr(34)&"InputFilePath"&Chr(34)&":"&Chr(34)&strFolder&File.FileName&Chr(34)&"}}"))

	    		set oHTTP = CreateObject("Microsoft.XMLHTTP")

			    oHTTP.open "POST", alerts_folder&"/admin/functions_inc.aspx",false
			    oHTTP.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
			    oHTTP.setRequestHeader "Content-Length", Len(sRequest)
			    oHTTP.send sRequest

			    HTTPPost = HtmlDecode(oHTTP.responseText)

			    Dim fso
			    if IsEmpty(HTTPPost) then
				    Set fso = CreateObject("Scripting.FileSystemObject")

				    if fso.FileExists(strFolder&File.FileName) then
    					fso.DeleteFile strFolder&File.FileName
				    end if

				    strMessage =  "File Uploaded and Converted to .mp4<br>"
				    shortfilename = ShortCaptionWithTitle(File.FileName&".mp4", 30)

				    if Uploader.Form("can_select") = "1" then
    					strMessage = strMessage & "<a href=""#"" onclick=""selectFile('" &HtmlEncode(jsEncode(File.FileName&".mp4"))& "', '" &HtmlEncode(jsEncode(httpref &"/"& File.FileName&".mp4"))& "')"">" &shortfilename& "</a>"
				    else
    				    strMessage = strMessage & shortfilename
				    end if
			    else
			        response.write HTTPPost

			        strMessage = "File Not Uploaded. mp4 Conversion Error: "&HTTPPost
				    Set fso = CreateObject("Scripting.FileSystemObject")

				    if fso.FileExists(strFolder&File.FileName) then
					    fso.DeleteFile strFolder&File.FileName
				    end if
			    end if

			 Set fso = Nothing
			 Set oHTTP = Nothing
			 Set HTTPPost = Nothing
			 Set sRequest = Nothing

			 else
			    strMessage = "File Uploaded: "
				shortfilename = ShortCaptionWithTitle(File.FileName, 30)
				if Uploader.Form("can_select") = "1" then
					strMessage = strMessage & "<a href=""#"" onclick=""selectFile('" &HtmlEncode(jsEncode(File.FileName))& "', '" &HtmlEncode(jsEncode(httpref &"/"& File.FileName))& "')"">" &shortfilename& "</a>"
				else
					strMessage = strMessage & shortfilename
				end if
		     end if
		end if
	Next
End If

return_page = Uploader.Form("return")
if return_page = "" then
	return_page = "uploadform.asp"
end if

if Err.number <> 0 then
    Response.Write "File convertion error #"&Err.number
    Response.End
end if
On Error GoTo 0
Response.Redirect(return_page & "?msg=" & Server.UrlEncode(HtmlEncode(strMessage)) & "&id=" & Uploader.Form("id"))

'--------------------------------------------
' ValidFileExtension()
' You give a list of file extensions that are allowed to be uploaded.
' Purpose:  Checks if the file extension is allowed
' Inputs:   strFileName -- the filename
'           strFileExtension -- the fileextensions not allowed
' Returns:  boolean
' Gives False if the file extension is NOT allowed
'--------------------------------------------
Function ValidFileExtension(strFileName, strFileExtensions)

	Dim arrExtension
	Dim strFileExtension
	Dim i
	
	strFileExtension = UCase(GetFileExtension(strFileName))
	
	arrExtension = Split(UCase(strFileExtensions), ";")
	
	For i = 0 To UBound(arrExtension)
		
		'Check to see if a "dot" exists
		If Left(arrExtension(i), 1) = "." Then
			arrExtension(i) = Replace(arrExtension(i), ".", vbNullString)
		End If
		
		'Check to see if FileExtension is allowed
		If arrExtension(i) = strFileExtension Then
			ValidFileExtension = True
			Exit Function
		End If
		
	Next
	
	ValidFileExtension = False

End Function

'--------------------------------------------
' InValidFileExtension()
' You give a list of file extensions that are not allowed.
' Purpose:  Checks if the file extension is not allowed
' Inputs:   strFileName -- the filename
'           strFileExtension -- the fileextensions that are allowed
' Returns:  boolean
' Gives False if the file extension is NOT allowed
'--------------------------------------------
Function InValidFileExtension(strFileName, strFileExtensions)

	Dim arrExtension
	Dim strFileExtension
	Dim i
		
	strFileExtension = UCase(GetFileExtension(strFileName))
	
	arrExtension = Split(UCase(strFileExtensions), ";")
	
	For i = 0 To UBound(arrExtension)
		
		'Check to see if a "dot" exists
		If Left(arrExtension(i), 1) = "." Then
			arrExtension(i) = Replace(arrExtension(i), ".", vbNullString)
		End If
		
		'Check to see if FileExtension is not allowed
		If arrExtension(i) = strFileExtension Then
			InValidFileExtension = False
			Exit Function
		End If
		
	Next
	
	InValidFileExtension = True

End Function

'--------------------------------------------
' GetFileExtension()
' Purpose:  Returns the extension of a filename
' Inputs:   strFileName     -- string containing the filename
'           varContent      -- variant containing the filedata
' Outputs:  a string containing the fileextension
'--------------------------------------------
Function GetFileExtension(strFileName)

	GetFileExtension = Mid(strFileName, InStrRev(strFileName, ".") + 1)
	
End Function

%>

