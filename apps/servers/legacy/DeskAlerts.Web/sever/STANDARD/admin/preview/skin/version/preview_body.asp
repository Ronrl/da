<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<% 
Response.Expires = 0 

Response.CharSet = "utf-8"
Response.CodePage = 65001

Function HtmlDecode(sText)
	if sText <> "" then
		HtmlDecode = Replace(sText, "&#34;", """")
		HtmlDecode = Replace(HtmlDecode, "&quot;", """")
		
		HtmlDecode = Replace(HtmlDecode, "&#39;", "'")
		HtmlDecode = Replace(HtmlDecode, "&apos;", "'")
		
		HtmlDecode = Replace(HtmlDecode, "&#60;", "<")
		HtmlDecode = Replace(HtmlDecode, "&lt;", "<")
		
		HtmlDecode = Replace(HtmlDecode, "&#62;", ">")
		HtmlDecode = Replace(HtmlDecode, "&gt;", ">")
		
		HtmlDecode = Replace(HtmlDecode, "&#38;", "&")
		HtmlDecode = Replace(HtmlDecode, "&amp;", "&")
	end if
End Function

Function jsEncode(str)
	Dim charmap(127), haystack()
	charmap(8)  = "\b"
	charmap(9)  = "\t"
	charmap(10) = "\n"
	charmap(12) = "\f"
	charmap(13) = "\r"
	charmap(34) = "\"""
	charmap(47) = "\/"
	charmap(92) = "\\"

	Dim strlen : strlen = Len(str) - 1
	ReDim haystack(strlen)

	Dim i, charcode
	For i = 0 To strlen
		haystack(i) = Mid(str, i + 1, 1)

		charcode = AscW(haystack(i)) And 65535
		If charcode < 127 Then
			If Not IsEmpty(charmap(charcode)) Then
				haystack(i) = charmap(charcode)
			ElseIf charcode < 32 Then
				haystack(i) = "\u" & Right("000" & Hex(charcode), 4)
			End If
		Else
			haystack(i) = "\u" & Right("000" & Hex(charcode), 4)
		End If
	Next

	jsEncode = Join(haystack, "")
End Function

customJs = Request("custom_js")
customJsSource = ""

if (customJs <> "") then
	Set fs=Server.CreateObject("Scripting.FileSystemObject")
	Set f=fs.OpenTextFile(Server.MapPath(customJs), 1)
	customJsSource = f.ReadAll
	f.Close
	Set f=Nothing
	Set fs=Nothing
end if


customJsSource = Replace(customJsSource,"file://",".",1,-1,1)

%>
<html>
<head>
<style>
body {background-color:#FFFFFF;}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<script type="text/JavaScript" language="JavaScript" src="../../../jscripts/jquery/jquery.min.js"></script>
<script type='text/JavaScript' language='JavaScript' src='../../../jscripts/json2.js'></script>
<script type='text/JavaScript' language='JavaScript'>var ext_props=JSON.parse($("<div/>").html("<%=jsEncode(Request("ext_props"))%>".replace(/&/g, '&amp;').replace(/"/g, '&quot;').replace(/'/g, '&#39;').replace(/</g, '&lt;').replace(/>/g, '&gt;')).text());</script>
<script type='text/JavaScript' language='JavaScript' src='external.asp'></script>
<script type='text/JavaScript' language='JavaScript'>
<%=customJsSource%>
</script>
<script type='text/JavaScript' language='JavaScript'>
$(document).ready(function()
{
	var Initial_Timeout_old = window.Initial_Timeout;
	if (typeof window.Initial_Timeout == 'function')
	{
		window.Initial_Timeout = function(indexes)
		{
			Initial_Timeout_old(indexes);
			var old_display = document.body.style.display;
			document.body.style.display = "none";
			setTimeout(function(){document.body.style.display = old_display;},0);
		}
		
		Initial("0");
	}
	else if (typeof Initial == 'function')
	{
		Initial("0");
		var old_display = document.body.style.display;
		document.body.style.display = "none";
		setTimeout(function(){document.body.style.display = old_display;},0);
	}
});
</script>
</head>
<body>
<%
Response.Write HtmlDecode(Request("alert_html"))
%>
</body>
</html>