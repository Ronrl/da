function getBody() {
	return window.parent.document.getElementById("body_frame")
}
function getCaption() {
	return window.parent.document.getElementById("caption_frame")
}
function getDialog() {
	return window.parent.parent.document.getElementById("alert_preview_dialog")
}

var external = {
	parseAllVars: function (propStr){
		var propertyVal = propStr
		var st = 0 
		while(1)
		{
			st = propertyVal.indexOf("%",0)
			if (st < 0) { break; }
			fn = propertyVal.indexOf("%",st+1)
			if (fn < 0) { break; }
			innerProp = propertyVal.substring(st+1, fn-st)
			newPropVal = external.getProperty(innerProp,"")
			if (newPropVal != innerProp)
			{
				propertyVal = propertyVal.substring(0,st-1) + newPropVal + propertyVal.substring(fn+1)		
			}
			st = fn +1
		}
		return propertyVal
	},
	getProperty: function(name, def){
		var ret = typeof(def)!='undefined'?def:name;
		if(ext_props[name])
			ret = ext_props[name].value;
		return external.parseAllVars(ret);
	},
	setProperty: function(name, val){
		if (ext_props[name] && ext_props[name].cnst)
		{
			return val;
		}
		if (!ext_props[name]) ext_props[name] = {};
		ext_props[name].value = val;
		ext_props[name].cnst = 0;
	
		return val;
	},
	isResizible: function(){
		var position = external.getProperty('position', 'right-bottom');
		return position != 'maximized' && position != 'maximize' && position != 'fullscreen';
	},
	Close: function(){
		window.parent.close();
	},
	close: function(){
		window.parent.close();
	},
	Hide: function(){
		window.parent.close();
	},
	hide: function(){
		window.parent.close();
	},
	capturemouse: function(){
	},
	format: function(date){
		return date;
	},
	getBodyLeft: function(){
		return $(getBody()).position().left;
	},
	getBodyTop: function(){
		return $(getBody()).position().top;
	},
	getBodyWidth: function(){
		return $(getBody()).width();
	},
	getBodyHeight: function(){
		return $(getBody()).height();
	},
	setBodyLeft: function(bodyLeft){
		return $(getBody()).css("left",bodyLeft).position().left;
	},
	setBodyTop: function(bodyTop){
		return $(getBody()).css("top",bodyTop).position().top;
	},
	setBodyWidth: function(bodyWidth){
		return $(getBody()).width(bodyWidth).width();
	},
	setBodyHeight: function(bodyHeight){
		return $(getBody()).height(bodyHeight).height();
	},
	setCaptionWidthByBodyWidth: function(bodyWidth){
		var body = $(getBody());
		var delta = body.width() - bodyWidth;
		var dialog = $(getDialog());
		var frame = $(getCaption());
		frame.width(frame.width() - delta);
		body.width(bodyWidth);
		window.parent.parent.setPreviewSize({width: dialog.width() - delta, height: dialog.height()})
	},
	setCaptionHeightByBodyHeight: function(bodyHeight){
		var body = $(getBody());
		var delta = body.height() - bodyHeight;
		var dialog = $(getDialog());
		var frame = $(getCaption());
		frame.height(frame.height() - delta);
		body.height(bodyHeight);
		window.parent.parent.setPreviewSize({width: dialog.width(), height: dialog.height() - delta})
	},
	callInCaption: function(){
		window.parent.callInCaption.apply(window.parent.callInCaption, arguments);
	},
	callInBody: function(){
		window.parent.callInBody(arguments)
	},
	Search: function(searchTerm){
	}
}
