<%
Response.Expires = 0 

Response.CharSet = "utf-8"
Response.CodePage = 65001

Function HtmlDecode(sText)
	if sText <> "" then
		HtmlDecode = Replace(sText, "&#34;", """")
		HtmlDecode = Replace(HtmlDecode, "&quot;", """")
		
		HtmlDecode = Replace(HtmlDecode, "&#39;", "'")
		HtmlDecode = Replace(HtmlDecode, "&apos;", "'")
		
		HtmlDecode = Replace(HtmlDecode, "&#60;", "<")
		HtmlDecode = Replace(HtmlDecode, "&lt;", "<")
		
		HtmlDecode = Replace(HtmlDecode, "&#62;", ">")
		HtmlDecode = Replace(HtmlDecode, "&gt;", ">")
		
		HtmlDecode = Replace(HtmlDecode, "&#38;", "&")
		HtmlDecode = Replace(HtmlDecode, "&amp;", "&")
	end if
End Function

Function jsEncode(str)
	Dim charmap(127), haystack()
	charmap(8)  = "\b"
	charmap(9)  = "\t"
	charmap(10) = "\n"
	charmap(12) = "\f"
	charmap(13) = "\r"
	charmap(34) = "\"""
	charmap(47) = "\/"
	charmap(92) = "\\"

	Dim strlen : strlen = Len(str) - 1
	ReDim haystack(strlen)

	Dim i, charcode
	For i = 0 To strlen
		haystack(i) = Mid(str, i + 1, 1)

		charcode = AscW(haystack(i)) And 65535
		If charcode < 127 Then
			If Not IsEmpty(charmap(charcode)) Then
				haystack(i) = charmap(charcode)
			ElseIf charcode < 32 Then
				haystack(i) = "\u" & Right("000" & Hex(charcode), 4)
			End If
		Else
			haystack(i) = "\u" & Right("000" & Hex(charcode), 4)
		End If
	Next

	jsEncode = Join(haystack, "")
End Function


alertSource = HtmlDecode(Request("caption_html"))

if (alertSource="") then
	Set fs=Server.CreateObject("Scripting.FileSystemObject")

	Set f=fs.OpenTextFile(Server.MapPath(Request("caption_href")), 1)
	alertSource = f.ReadAll
	f.Close
	Set f=Nothing
	Set fs=Nothing
end if

Set rHead = New RegExp
With rHead
	.Pattern = "<head[^>]*>"
	.IgnoreCase = True
	.Global = False
End With
alertSource = rHead.Replace(alertSource, "$&"& vbNewline & _
	"<!--server_preview_section_start-->"& vbNewline & _
	"<link href='../../../css/jquery/jquery-ui.css' rel='stylesheet' type='text/css'/>"& vbNewline & _
	"<script type='text/javascript' language='JavaScript' src='../../../jscripts/jquery/jquery.min.js'></script>"& vbNewline & _
	"<script type='text/javascript' language='JavaScript' src='../../../jscripts/jquery/jquery-ui.min.js'></script>"& vbNewline & _
	"<script type='text/javascript' language='JavaScript'>"& vbNewline & _
	"	var ext_props=$.parseJSON($('<div/>').html("& vbNewline & _
	"	"""& jsEncode(Request("ext_props")) &""""& vbNewline & _
	"		.replace(/&/g, '&amp;').replace(/""/g, '&quot;').replace(/'/g, '&#39;').replace(/</g, '&lt;').replace(/>/g, '&gt;')"& vbNewline & _
	"	).text());"& vbNewline & _
	"</script>"& vbNewline & _
	"<script type='text/javascript' language='JavaScript' src='external.asp'></script>"& vbNewline & _
	"<!--server_preview_section_end-->"& vbNewline)
alertSource = Replace(alertSource,".parentElement",".parentNode",1,-1,1)

Response.Write alertSource

%>

