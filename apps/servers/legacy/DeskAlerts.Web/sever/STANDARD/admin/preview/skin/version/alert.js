var searchTerm;
var max_timer_value = 60 * 1000;

function setSearchTerm(_searchTerm)
{
	searchTerm = _searchTerm;
}
function checkLang(current, langs)
{
	var currents = current.toLowerCase().replace(" ", "").split(",");
	var languages = langs.toLowerCase().replace(" ", "").split(",");
	for(var i=0; i<currents.length; i++)
	{
		for(var j=0; j<languages.length; j++)
		{
			if(currents[i].indexOf(languages[j])==0 ||
				languages[j].indexOf(currents[i])==0)
			{
				return true;
			}
		}
	}
	return false;
}

function pauseVideo()
{
	
	var videoElement = document.getElementsByTagName("video");
	
	if(videoElement)
	{
		for(i = 0; i < videoElement.length; i++)
			videoElement[i].pause();
	}
		
}
function replaceWithInput(elem, name)
{
	var inp = document.createElement("input");
	inp.type = "submit";
	if(elem.tagName.toLowerCase() == "img")
	{
		var func = elem.onclick||'';
		if(typeof(func) == 'function')
			inp.onclick = function() {
				func();
				return false;
			};
		else
			inp.onclick = func+";return false";
	}
	else
	{
		inp.onclick = elem.onclick;
	}
	inp.name = elem.name;
	inp.value = name;
	elem.parentNode.replaceChild(inp, elem);
}
function onContextMenuMain(e)
{
	if(!e) e = window.event;
	e.returnValue = false;
	e.cancelBubble = true;
	return false;
}
function getDocumentSize(doc)
{
	var size ={
		height: 0,
		width: 0
	};
	var body = doc.body;
	if (!doc.compatMode || doc.compatMode=="CSS1Compat")
	{
		var topMargin = parseInt(body.currentStyle.marginTop, 10) || 0;
		var bottomMargin = parseInt(body.currentStyle.marginBottom, 10) || 0;
		var leftMargin = parseInt(body.currentStyle.marginLeft, 10) || 0;
		var rightMargin = parseInt(body.currentStyle.marginRight, 10) || 0;

		size.width=Math.max(body.offsetWidth + leftMargin + rightMargin, doc.documentElement.clientWidth, doc.documentElement.scrollWidth);
		size.height=Math.max(body.offsetHeight + topMargin + bottomMargin, doc.documentElement.clientHeight, doc.documentElement.scrollHeight);
	}
	else
	{
		size.width = body.scrollWidth;
		size.height = body.scrollHeight;
	}
	return size;
}
function resizeByBody()
{
	try{
		var isResizible = window.external.isResizible();
		if (isResizible)
		{
			var readit = document.getElementById('readitButton');
			if(readit)
			{
				readit.style.bottom = 'auto';
				readit.style.position = 'relative';
				document.body.style.overflow = '';
			}
			var size = getDocumentSize(document);
			var curBodyWidth = window.external.getBodyWidth();
			var curBodyHeight = window.external.getBodyHeight();
			if (curBodyWidth > size.width) size.width=curBodyWidth;
			if (curBodyHeight > size.height) size.height=curBodyHeight;
			window.external.setCaptionWidthByBodyWidth(size.width);
			window.external.setCaptionHeightByBodyHeight(size.height);
			moveReaditButton();
		}
	}catch(e){}
}
function moveReaditButton()
{
	try{
		if(window.external.getProperty('readit_at_bottom', '0') == '1')
		{
			var readit = document.getElementById('readitButton');
			if(readit && ((readit.offsetTop + readit.clientHeight) < document.body.clientHeight))
			{
				readit.style.position = 'relative';
				readit.style.bottom = '2px';
				readit.style.right = '23px'; 
				//document.body.style.overflow = 'hidden';
			}
			else readit.style.right = '23px'; 
		}
	}catch(e){}
}
var initialized = false;
function Initial(a)
{
	setTimeout(Initial_Timeout, 1);
}

function CloseAtDate( date, func ) {
    var now = (new Date()).getTime()/1000;
    var diff = Math.max((date - now), 0) * 1000;
	
    if (diff > max_timer_value){ 
		setTimeout(function() {CloseAtDate(date, func);}, max_timer_value);
	} else {
		setTimeout(func, diff);
	}
}

function Initial_Timeout()
{		
	if(initialized) return;
	else initialized = true;
	try
	{
		var html = document.body.innerHTML;
		var top_template = window.external.getProperty('top_template', '')||'';
		var bottom_template = window.external.getProperty('bottom_template', '')||'';
		var match = html.match(/<!-- *begin_lang[^>]*-->([\w\W]*?)<!-- *end_lang *-->/ig);
		if(match)
		{
			var locale = window.external.getProperty("customlocale", "");
			if(!locale) locale = window.external.getProperty("locale", "");
			var default_lang, use_dafault = true;
			for(var j=0; j<match.length; j++)
			{
				var lang = /<!-- *begin_lang[^>]*lang *= *(['"])([^"']*)\1[^>]*-->/i.exec(match[j]);
				if(lang && checkLang(locale, lang[2]))
				{
					html = html.replace(/<!-- *begin_lang[^>]*-->([\w\W]*)<!-- *end_lang *-->/i, match[j]);
					use_dafault = false;
					var title = /<!-- *begin_lang[^>]*title *= *(['"])([^"']*)\1[^>]*-->/i.exec(match[j]);
					if(title && title[2])
						window.external.callInCaption('setTitle', title[2]);
					break;
				}
				var is_default = /<!-- *begin_lang[^>]*is_default *= *(['"])([^"']*)\1[^>]*-->/i.exec(match[j]);
				if(is_default && (is_default[2]!=0 || is_default[2].toLowerCase()=="true"))
					default_lang = match[j];
			}
			if(use_dafault && default_lang)
				html = html.replace(/<!-- *begin_lang[^>]*-->([\w\W]*)<!-- *end_lang *-->/i, default_lang);
		}
		
		var root_path, close_button, save_button, submit_button, readIt_button, alert_auto_size = false;
		var skinId = null;
		try {
			skinId = window.external.getProperty("skin_id","");
			alert_auto_size = window.external.getProperty('alert_auto_size', '') == '1';
		} catch(e) {}
		if(skinId)
		{
			try {
				var server = window.external.getProperty("server");
				close_button = server + "/admin/skins/" + skinId + "/version/close_button.gif";
				save_button = server + "/admin/skins/" + skinId + "/version/save.gif";
				submit_button = server + "/admin/skins/" + skinId + "/version/submit_button.gif";
				readIt_button = server + "/admin/skins/" + skinId + "/version/read_it.gif";
			} catch(e) {
				root_path = null;
				close_button = null;
				save_button = null;
				submit_button = null;
			}
		}
		else
		{
			try {
				root_path = window.external.getProperty("root_path").replace(/\\/g, "/");
				close_button = window.external.getProperty("closeButton").replace(/\\/g, "/");
				save_button = window.external.getProperty("saveButton").replace(/\\/g, "/");
				submit_button = window.external.getProperty("submitButton").replace(/\\/g, "/");
				readIt_button = window.external.getProperty("readitButton").replace(/\\/g, "/");
			} catch(e) {
				root_path = null;
				close_button = null;
				save_button = null;
				submit_button = null;
			}
		}
		var alert_id = window.external.getProperty('alertid');
		match = /<!-- readit *= *'(.*?)', '([-\d]+)', '([-\d]+)' -->/i.exec(html);
		if(match)
		{
			if(readIt_button.indexOf("/") >= 0)
			{
				html += "<img";
			}
			else
				html += "<input type=\"button\"";
			html +=
					" id=\"readitButton\" class=\"readitButton\" style=\"position: relative; float:right; right: 50px;\"" + 
					" src=\"" + readIt_button + "\" value=\"" + readIt_button + "\" alt=\"OK\" title=\"OK\" border=\"0\"";	
			if(html.match(/<!-- self-deletable -->/i))	
				html +=	" onclick=\"window.external.DeleteHistoryByAlertID('"+alert_id+"');window.external.readIt('" + match[1] + "','" + match[2] + "','" + match[3] + "'); return false\"/>" ; 
			else 		
				html +=	" onclick=\"window.external.readIt('" + match[1] + "','" + match[2] + "','" + match[3] + "');return false\"/>" ; 
		}
		
		var readItDiv = document.getElementById("readItDiv");
		
		if(readItDiv)
		{
			var fileName = readItDiv.getAttribute("file");
			var alertId =  readItDiv.getAttribute("alertId");
			var userId = readItDiv.getAttribute("userId");
			
			if(readIt_button.indexOf("/") >= 0)
			{
				html += "<img";
			}
			else
				html += "<input type=\"button\"";
			html +=
					" id=\"readitButton\" class=\"readitButton\" style=\"position: relative; float:right; right: 50px;\"" + 
					" src=\"" + readIt_button + "\" value=\"" + readIt_button + "\" alt=\"OK\" title=\"OK\" border=\"0\"";	
			if(html.match(/<!-- self-deletable -->/i))	
				html +=	" onclick=\"window.external.DeleteHistoryByAlertID('"+alertId+"');window.external.readIt('" + fileName + "','" + alertId + "','" + userId + "'); return false\"/>" ; 
			else 		
				html +=	" onclick=\"window.external.readIt('" + fileName + "','" + alertId + "','" + userId + "');return false\"/>" ; 			
		}
		match = html.match("<!-- html_title = '(.*?)' -->", 'i');
		if(match && match[1])
		{
			var div = document.createElement("div");
			div.innerHTML = match[1];
			window.external.callInCaption('setHtmlTitle', div.innerHTML||div.textContent);
		}

		document.body.innerHTML = top_template + html + bottom_template;

		if(close_button || save_button || submit_button)
		{
		
			var images = document.getElementsByTagName("img");
			for(var j = 0; j < 2; j++)
			{
				if(j==1) images = document.getElementsByTagName("input");
				for(var i = 0; i < images.length; i++)
				{
					if(images[i].tagName.toLowerCase() == "img" || images[i].getAttribute("type").toLowerCase() == "image")
					{
                        var src = images[i].getAttribute("src");

                        if (src.startsWith("data:")) {
                            continue;;
                        }

						if(close_button && /.*admin\/images\/(langs\/\w+\/?|)close_button\.gif/i.test(src))
						{
							if(/^[\w\s]+$/.test(close_button))
								replaceWithInput(images[i], close_button);
							else
							{
								images[i].src = (/https?:\/\//i.test(close_button) ? "" : "file:///") + close_button;
								images[i].alt = "Close";
								images[i].title = "Close";
							}

						}
						else if(save_button && /.*admin\/images\/(langs\/\w+\/?|)save\.gif/i.test(src))
						{
							if(/^[\w\s]+$/.test(save_button))
								replaceWithInput(images[i], save_button);
							else
							{
								images[i].src = (/https?:\/\//i.test(save_button) ? "" : "file:///") + save_button;
								images[i].alt = "Save";
								images[i].title = "Save";
							}
						}
						else if(submit_button && /.*admin\/images\/(langs\/\w+\/?|)submit_button\.gif/i.test(src))
						{	
							if(/^[\w\s]+$/.test(submit_button))
								replaceWithInput(images[i], submit_button);
							else
							{
								images[i].src = (/https?:\/\//i.test(submit_button) ? "" : "file:///") + submit_button;
								images[i].alt = "Submit";
								images[i].title = "Submit";
							}
						}
					}
				}
			}
		}
		match = html.match(/<!-- self-deletable -->/i);
		if (match) {
		    window.external.setProperty('self_deletable', '1');
		}
		else window.external.setProperty('self_deletable', '0');
		
		document.body.scroll="auto";
		document.oncontextmenu = onContextMenuMain;

        document.body.className = "alertcontent";

        var cssProperty = GetCssProperty(skinId, ".alertcontent");
	    var styleElemenet = document.createElement("style");
	    styleElemenet.innerHTML = cssProperty;
	    document.head.appendChild(styleElemenet);
        
		var readit = document.getElementById('readitButton');
		if(readit)
		{
			document.body.appendChild(readit.parentNode.removeChild(readit));
		}

		// turn on autoresizing alert by body of document
		if(alert_auto_size)
		{
			resizeByBody();
			setTimeout(resizeByBody, 50);
			setTimeout(resizeByBody, 100);
		}
		else
			moveReaditButton();
		
		/*try {
			window.external.Search(searchTerm);
		} catch(e) {}*/
        if (html.indexOf('<!-- printable_alert -->') > -1) {
            var printable_text = html;
            if (html.indexOf('<!-- cannot close -->') > -1)
            {				
                var foundedString = html.match(/<img id=\"readitButton\"+(\s|\S)+?(?=>)>/img) ;
                if (null !== foundedString) {
                    printable_text = foundedString;
                    if (printable_text) {
                        printable_text = html.replace(/<img id=\"readitButton\"+(\s|\S)+?(?=>)>/, "");
                    }
                }
			}
			window.external.callInCaption('showPrintButton', true, printable_text);
		}
		else
		{
			try {
				window.external.callInCaption('showPrintButton', false);
			} catch(e) {}
		}
		if (window.external.getProperty('acknowledgement', '0') == '1' && html.indexOf('<!-- cannot close -->') > -1 || window.external.getProperty('hide_close', '0') == '1' ) {
		    try {
		        window.external.callInCaption('showCloseButton', false);
		    } catch (e) { }
		    window.onunload = function() {
		        try {
					if(window.external.getProperty('hide_close', '0') == '0')
					{
						
						window.external.callInCaption('showCloseButton', true);
					}
		        } catch (e) { }
		    };
		}
		else if (html.indexOf('<!-- cannot close -->') > -1) {
		    window.external.callInCaption('showCloseButton', false);
		}
		else
		{
			try {
				if(window.external.getProperty('hide_close', '0') == '0')
				{
					window.external.callInCaption('showCloseButton', true);
				}
			} catch(e) {}
		}
		
		var to_date_match = html.match(/<!-- todate = '(.*?)', '([\d]+)', '([\d]+)', '(.*?)' -->/i);
			
		if(to_date_match && to_date_match[2] > 0 && to_date_match[3] > 0 && to_date_match[4])
		{
			var lifetimedate = to_date_match[1];

			if(!isNaN(parseInt(lifetimedate) - Math.round(new Date().getTime()/1000.0))){
				CloseAtDate( lifetimedate, function(){ window.external.Close(); } );
			}
		} else {
			var toDateDiv = document.getElementById('todate');
			
			if(toDateDiv)
			{
				var now = Math.round(new Date().getTime()/1000.0);
				var lifetimedate =  toDateDiv.getAttribute('date');//to_date_match[1];
				var lifetimedate = to_date_match[1];
				
				while (Math.round(new Date().getTime()/1000.0) < parseInt(lifetimedate))
				{
					continue;
				}
				
				window.external.Close();               
			}
		}
	}
	catch(e)
	{
	}
}

function GetCssProperty(skinId, cssElement) {
    try {
        if (!skinId || !cssElement) {
            return;
        }

        let url = GetCssPath(skinId);
        let cssContent = LoadCss(url);

        let regexp = new RegExp("(\\" + cssElement.toLowerCase() + "(\\n?\\t?\\s?)\\{){1}[\\n\\t\\w\\s\\:\\;\\#\\-]+(\\n?\\}){1}", "g");
        let matches = cssContent.toLowerCase().match(regexp);

        let result = matches.join('\n\r');
        return result;
    }
    catch (e) {
        console.log(e);
    }
}

function LoadCss(url) {
    try {
        let skinsCss = new Object();
        let css = skinsCss[url] || window.external.QueryURL(url);

        return css;
    }
    catch (e) {
        console.log(e);
    }
}

function GetCssPath(skinGuid) {
    try {
        let url;

        let isNotOffline = (skinGuid && window.external.getProperty('AlertMode') != 'offline');
        if (isNotOffline) {
            url = window.external.getProperty("server") + "/admin/skins/" + skinGuid + "/version/skin.min.css";
        } else {
            url = "file:///" + window.external.getProperty("root_path").replace(/\\/g, "/") + "/skins/" + skinGuid + "/version/skin.min.css";
        }

        return url;
    }
    catch (e) {
        console.log(e);
    }
}