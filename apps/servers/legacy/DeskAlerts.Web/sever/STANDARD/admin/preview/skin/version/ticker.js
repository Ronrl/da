function createDomDocument(source)
{
	var xmlDoc = null;

	if (window.ActiveXObject){
		xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
		xmlDoc.async = "false";
		xmlDoc.loadXML(source);
	}
	else
	{
		var parser = new DOMParser();
		xmlDoc = parser.parseFromString(source,"text/xml");
	}
	return xmlDoc;
}

function unescapeXML(str)
{
	str = str.replace(/&amp;/g, "&");
	str = str.replace(/&quot;/g, "\"");
	
	str = str.replace(/&apos;/g, "'");
	str = str.replace(/&lt;/g, "<");
	str = str.replace(/&gt;/g, ">");
	str = str.replace(/&#x9;/g, "\t");
	str = str.replace(/&#xA;/g, "\n");
	str = str.replace(/&#xD;/g, "\r");
	
	return str;
}

function checkLang(current, langs)
{
	var currents = current.toLowerCase().replace(" ", "").split(",");
	var languages = langs.toLowerCase().replace(" ", "").split(",");
	for(var i=0; i<currents.length; i++)
	{
		for(var j=0; j<languages.length; j++)
		{
			if(currents[i].indexOf(languages[j])==0 ||
				languages[j].indexOf(currents[i])==0)
			{
				return true;
			}
		}
	}
	return false;
}
function replaceWithInput(elem, name)
{
	var inp = document.createElement("input");
	inp.type = "submit";
	if(elem.tagName.toLowerCase() == "img")
	{
		var func = elem.onclick||'';
		if(typeof(func) == 'function')
			inp.onclick = function() {
				func();
				return false;
			};
		else
			inp.onclick = func+";return false";
	}
	else
	{
		inp.onclick = elem.onclick;
	}
	inp.name = elem.name;
	inp.value = name;
	elem.parentNode.replaceChild(inp, elem);
}
function onContextMenuMain(event)
{
	if (!event) event = window.event;
	event.returnValue = false;  
	event.cancelBubble = true;
	return false;
}
function getDocumentSize(doc)
{
	var size ={
		height: 0,
		width: 0
	};
	var body = doc.body;
	if (!doc.compatMode || doc.compatMode=="CSS1Compat")
	{
		var topMargin = parseInt(body.currentStyle.marginTop, 10) || 0;
		var bottomMargin = parseInt(body.currentStyle.marginBottom, 10) || 0;
		var leftMargin = parseInt(body.currentStyle.marginLeft, 10) || 0;
		var rightMargin = parseInt(body.currentStyle.marginRight, 10) || 0;

		size.width=Math.max(body.offsetWidth + leftMargin + rightMargin, doc.documentElement.clientWidth, doc.documentElement.scrollWidth);
		size.height=Math.max(body.offsetHeight + topMargin + bottomMargin, doc.documentElement.clientHeight, doc.documentElement.scrollHeight);
	}
	else
	{
		size.width = body.scrollWidth;
		size.height = body.scrollHeight;
	}
	return size;
}
function resizeByBody(docSize)
{
	try
	{
		var isResizible = window.external.isResizible();
		if (isResizible)
		{
			var docHeight = docSize.height;
			var curBodyHeight = window.external.getBodyHeight();
			if (curBodyHeight>docHeight) docHeight=curBodyHeight;
			window.external.setCaptionHeightByBodyHeight(docHeight);
		}
	} catch (e) {}
}
function replace_tags(html)
{
	return html.replace(/<(\/?)(\w+)(.*?)(\sstyle\s*=\s*(('|")(.*?)\6|(\w+)))?(.*?)>/gi, function(all, c, tag, a1, s, style, q, s1, s2, a2) {
		var before = '';
		var after = '';
		switch(tag.toLowerCase())
		{
			case 'hr':
			case 'br':
			{
				return ' ';
			}
			case 'html':
			case 'body':
			case 'head':
			case 'table':
			case 'thead':
			case 'tbody':
			case 'tr':
			case 'th':
			case 'td':
			case 'pre':
			case 'ul':
			case 'ol':
			case 'li':
			case 'dl':
			case 'dt':
			case 'dd':
			case 'blockquote':
			case 'q':
			case 'p':
			{
				tag = 'span';
				after = ' ';
				break;
			}
			case 'h1':
			case 'h2':
			case 'h3':
			case 'h4':
			case 'h5':
			case 'h6':
			{
				tag = 'b';
				after = ' ';
				break;
			}
			case 'img':
			{
				before = "</td><td valign='middle'>";
				after = "</td><td valign='middle' class='text'>";
				break;
			}
		}
		return before+'<'+c+tag+a1+(s?' style='+q+(s1||s2||'').replace(/!\s*important/,'')+q:'')+a2+'>'+after;
	});
}

var cannotclose = new Array();
function CloseTicker(id)
{
	try {
		var elem = document.getElementById('ticker_'+id);
		if(elem)
		{
			var width = elem.offsetWidth;
			elem.className = 'hidden';
			alert_body_left += width;
			alert_body_width -= width;
			scrollticker(0, true);
			var child = alert_body.firstChild;
			while(child)
			{
				if(child.className != 'hidden')
				{
					var sep = child.firstChild;
					if(sep && sep.className == 'ticker_separator')
					{
						sep.className = 'hidden';
					}
					break;
				}
				child = child.nextSibling;
			}
		}
		for(var i=0; i<cannotclose.length; i++)
		{
			if(cannotclose[i] == id)
			{
				cannotclose.splice(i, 1);
				try {
					if(cannotclose.length == 0 && window.external)
					{
						window.external.callInCaption('showCloseButton', true);
						window.external.setProperty('manualclose', '1');
					}
				} catch(e) {}
				break;
			}
		}
	} catch(e) {}
}

var body_width;
var alert_body;

var alert_body_left = 0;
var alert_body_width;

var tickerstop=false;
var initialized = false;

var remainingMessages = [];
var interval;
var skinId;
var alertsContent = "";
var gIndexes;

function Initial(indexes)
{
	setTimeout("Initial_Timeout('"+indexes+"')", 1);
}
function Initial_Timeout(indexes)
{
	if(initialized) return;
	else initialized = true;
	skinId = null;
	var server = window.external.getProperty("server");
	try
	{
		document.body.style.overflow = 'hidden';
		var el = document.getElementById('alert_body');
		if(null == el)
		{
			var html = replace_tags(document.body.innerHTML);
            alertsContent = html;
			var unread = indexes.split(',');
            if (!gIndexes) { gIndexes = unread.slice(0)};
			remainingMessages = unread.slice(0);
			var aknow = false;
			var alerts = html.split('<!-- separator -->');
			html = '';
			var main_title = '';
			try {
				main_title = window.external.getProperty("title");
			} catch(e) {}

			for(var i=unread.length-1; i>=0; i--)
			{
				var root_path, close_button, save_button, submit_button, separator_image;
				var newSkinId = "";
				
				var alert_id = unread[i];
		
				var cur = alerts[alerts.length-unread[i]-1];
				
				var match = cur.match(/<!-- cdata = '(.*?)' -->/i);
				if(match)
				{
					var doc = createDomDocument("<ALERT>"+unescapeXML(match[1])+"</ALERT>");
					var alertWindows = doc.getElementsByTagName("WINDOW");
					if (alertWindows.length > 0)
					{
						newSkinId = alertWindows[0].getAttribute("skin_id");
					}
				}
				
				if (!skinId)
				{
					skinId = "default";
					if (newSkinId)
					{
						skinId = newSkinId;
					}
					window.external.callInCaption("refreshSkin",skinId);
				}

				if ((skinId != newSkinId) && (skinId!="default" || newSkinId))
				{
					break;
				}

				remainingMessages.splice(remainingMessages.length - 1,1);

				if (skinId && skinId!="default")
				{
					close_button = server + "/admin/skins/" + skinId + "/version/close_button.gif";
					save_button = server + "/admin/skins/" + skinId + "/version/save.gif";
					submit_button = server + "/admin/skins/" + skinId + "/version/submit_button.gif";
					separator_image = server + "/admin/skins/" + skinId + "/version/separator.gif";
					root_path = server + "/admin/skins/" + skinId + "/version/";
					readIt_button = server + "/admin/skins/" + skinId + "/version/read_it.gif";
				}
				else
				{
					try {
						root_path = window.external.getProperty("root_path").replace(/\\/g, "/");
						close_button = window.external.getProperty("closeButton").replace(/\\/g, "/");
						save_button = window.external.getProperty("saveButton").replace(/\\/g, "/");
						submit_button = window.external.getProperty("submitButton").replace(/\\/g, "/");
						separator_image = "file:///" + root_path + "/separator.gif";
						readIt_button = "file:///" + root_path + "/read_it.gif";
						
					} catch(e) {
						root_path = null;
						close_button = null;
						save_button = null;
						submit_button = null;
						separator_image = null;
					}
				}
				if(close_button || save_button || submit_button)
				{
					var images = document.getElementsByTagName("img");
					for(var j = 0; j < 2; j++)
					{
						if(j==1) images = document.getElementsByTagName("input");
						for(var imgIndex = 0; imgIndex < images.length; imgIndex++)
						{
							if(images[imgIndex].tagName.toLowerCase() == "img" || images[imgIndex].getAttribute("type").toLowerCase() == "image")
							{
								var src = images[imgIndex].getAttribute("src");
								if(close_button && /.*admin\/images\/(langs\/\w+\/?|)close_button\.gif/i.test(src))
								{
									if(/^[\w\s]+$/.test(close_button))
										replaceWithInput(images[imgIndex], close_button);
									else
									{
										images[imgIndex].src = (/https?:\/\//i.test(close_button) ? "" : "file:///") + close_button;
									}

								}
								else if(save_button && /.*admin\/images\/(langs\/\w+\/?|)save\.gif/i.test(src))
								{
									if(/^[\w\s]+$/.test(save_button))
										replaceWithInput(images[imgIndex], save_button);
									else
									{
										images[imgIndex].src = (/https?:\/\//i.test(save_button) ? "" : "file:///") + save_button;
									}
								}
								else if(submit_button && /.*admin\/images\/(langs\/\w+\/?|)submit_button\.gif/i.test(src))
								{	
									if(/^[\w\s]+$/.test(submit_button))
										replaceWithInput(images[imgIndex], submit_button);
									else
									{
										images[imgIndex].src = (/https?:\/\//i.test(submit_button) ? "" : "file:///") + submit_button;
									}
								}
							}
						}
					}
				}

				if(cur.indexOf('<!-- cannot close -->') > -1)
				{
					cannotclose.push(alert_id);
					aknow = true;
				}

				match = cur.match(/<!-- autoclose = '-?([\d]+)', '(.*?)' -->/i);
				
				if(match && match[1] > 0)
				{
					try {
						if(window.external)
						{
							window.external.callInCaption('setAutoclose', alert_id, match[1], match[2]);
						}
					} catch(e) {}
					cur = cur.replace('/*close_func*/', ';CloseTicker('+alert_id+');')
				}
				
				match = cur.match(/<!-- readit = '(.*?)', '([\d]+)', '([\d]+)' -->/i);
				
				
				if(match && match[2] > 0 && match[3] > 0)
				{
		
					if(readIt_button.indexOf("/") >= 0)
					{
						cur += "<img";
					}
					else
						cur += "<input type=\"button\"";
						
					cur +=
						" id=\"readitButton\" class=\"readitButton\" style=\"position: relative; float:right; right: 50px;\"" + 
						" onclick=\"window.external.readIt('" + match[1] + "','" + match[2] + "','" + match[3] + "');return false\"" + 
						" src=\"" + readIt_button + "\" value=\"" + readIt_button + "\" border=\"0\"/>";
				}
				
				var title = main_title;
				match = cur.match(/<!-- title = '(.*?)' -->/i);
				if(match)
					title = match[1];
				match = cur.match(/<!-- *begin_lang[^>]*-->([\w\W]*?)<!-- *end_lang *-->/ig);
				if(match)
				{
					var locale;
					try
					{
						locale = window.external.getProperty("customlocale", "");
						if(!locale) locale = window.external.getProperty("locale", "");
					}
					catch(err)
					{
						locale = "EN";
					}
					var default_lang, use_dafault = true;
					for(var j=0; j<match.length; j++)
					{
						var lang = match[j].match(/<!-- *begin_lang[^>]*lang *= *(['"])([^"']*)\1[^>]*-->/i);
						if(lang && checkLang(locale, lang[2]))
						{
							cur = cur.replace(/<!-- *begin_lang[^>]*-->([\w\W]*)<!-- *end_lang *-->/i, match[j]);
							use_dafault = false;
							var rtitle = match[j].match(/<!-- *begin_lang[^>]*title *= *(['"])([^"']*)\1[^>]*-->/i);
							if(rtitle && rtitle[2])
								title = rtitle[2];
							break;
						}
						var is_default = match[j].match(/<!-- *begin_lang[^>]*is_default *= *(['"])([^"']*)\1[^>]*-->/i);
						if(is_default && (is_default[2]!=0 || is_default[2].toLowerCase()=="true"))
							default_lang = match[j];
					}
					if(use_dafault && default_lang)
						cur = cur.replace(/<!-- *begin_lang[^>]*-->([\w\W]*)<!-- *end_lang *-->/i, default_lang);
				}
				if(html) html += "<td id='ticker_"+alert_id+"' valign='middle' class='text'><span class='ticker_separator'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td valign='middle'><img class='ticker_image' src='" + separator_image + "'/></td><td valign='middle' class='text'><span class='ticker_dot'>&#9679;</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>";
				else html += "<td id='ticker_"+alert_id+"' valign='middle' class='text'>";

				var topTemplate = "", bottomTemplate = "";
				try
				{
					topTemplate = replace_tags(window.external.getProperty("top_template",""));
					bottomTemplate = replace_tags(window.external.getProperty("bottom_template",""));
				}
				catch(e)
				{
				}
				if(title)
					html += "</td><td valign='middle' class='ticker_title_container'><div class='ticker_title' valign='middle'>"+title+"</div></td><td class='ticker_after_title_td'>&nbsp;</td><td valign='middle' class='text'>";

				html += topTemplate + cur + bottomTemplate + "</td>";
			}
			html = "<table cellpadding='0' cellspacing='0'><tr>" + html + "</tr></table>";

			//hide close button
			try {
				if(window.external)
				{
					window.external.callInCaption('showCloseButton', !aknow);
					window.external.setProperty('manualclose', aknow?'0':'1');
				}
			} catch(e) {}

			document.oncontextmenu = onContextMenuMain; 

			//insert base tag	
			var headTag = document.getElementsByTagName('head')[0]; 
			var base = document.createElement('base'); 
			//base.setAttribute("href", "file:///" + root_path);
			base.setAttribute("target", "_blank");
			headTag.appendChild(base);

			//insert style
			var css = "\
			* {margin:0px!important; \
				padding:0px!important; \
				orphans:0!important; \
				widows:0!important; \
				vertical-align:middle; \
				white-space:nowrap!important; \
				overflow:show!important; \
			} \
			.alert_body td * {border:0px none!important; \
				outline:0px none!important; \
				position:static!important; \
				z-index:auto!important; \
				clip:auto!important; \
				width:auto!important; \
				clear:none!important; \
				float:none!important; \
				line-height:normal!important; \
				page-break-after:auto!important; \
				page-break-before:auto!important; \
				page-break-inside:auto!important; \
				text-indent:0px!important; \
				text-shadow:none!important; \
				visibility:visible!important; \
			} \
			.alert_body td.text * {display:inline!important; \
				top:auto!important; \
				right:auto!important; \
				bottom:auto!important; \
				left:auto!important; \
			} \
			.alert_body td img {display:block!important; \
			} \
			.alert_body, table, tr, tbody, td {height:100%!important} \
			.hidden {display:none!important} \
			body {-moz-user-select:none;-khtml-user-select:none;user-select:none;}";
			var imprt = document.createElement('style');
			imprt.setAttribute("type", "text/css");
			if (imprt.styleSheet) imprt.styleSheet.cssText = css; //IE
			else imprt.appendChild(document.createTextNode(css)); // Other browsers
			headTag.appendChild(imprt);

			var imp = document.createElement('link');
			imp.setAttribute("type", "text/css");
			imp.setAttribute("rel", "stylesheet");
			imp.setAttribute("id", "ticker_css");
			

			if (skinId)
			{
                if (typeof root_path == "undefined") {
                    var skinBase = server + "/admin/skins/" + skinId + "/version/";
                }
                else {
                    var skinBase = "file:///" + root_path + "/skins/" + skinId + "/version/";
                }
                var baseElement = document.createElement("BASE");
                baseElement.setAttribute("href", skinBase);
                headTag.appendChild(baseElement);
                imp.setAttribute("href", skinBase + "/skin.css");
			}
			else
			{
				imp.setAttribute("href", "file:///" + root_path + "/skin.css");
			}
			headTag.appendChild(imp);

			document.body.innerHTML = "<div id='alert_body' class='alert_body' onmouseover='tickerstop=true' onmouseout='tickerstop=false' style='position:relative'>"+html+"</div>";
			document.body.className = "tickercontent";

			//resize images
			var imgs = document.getElementsByTagName('img');
			var height = document.body.clientHeight - 4;
			for(var i=0; i < imgs.length; i++)
			{
				//imgs[i].align = 'absmiddle';
				if(imgs[i].clientHeight > height)
				{
					var h = imgs[i].clientHeight;
					imgs[i].height = height;
					imgs[i].width = h > 0 ? imgs[i].clientWidth * height/h : height;
				}
			}
			alert_body = document.getElementById('alert_body');
			alert_body_left = body_width = document.body.clientWidth + 10;
			alert_body_width = alert_body.offsetWidth;
			alert_body.style.left = body_width+"px"

			if (remainingMessages.length==0)
			{
				remainingMessages = gIndexes.slice(0);
			}

			interval = setInterval('scrollticker(1, false)',15);
			try {
				if(window.external)
				{
					window.external.callInCaption('checkForClosed');
					resizeByBody(getDocumentSize(document));
				}
			} catch(e) {}
				
		}
	}
	catch(e)
	{
	}
}
function scrollticker(delta, force)
{
	try {
		if((tickerstop && !force) || !alert_body) return;
		
		//alert_body_left = (alert_body_left > (-10 - alert_body_width)) ? (alert_body_left-delta > body_width ? -alert_body_width : alert_body_left-delta) : body_width;
		
		if (alert_body_left <= (-10 - alert_body_width)) //has been scrolled to left out of screen
		{
			document.body.innerHTML = alertsContent;
			var cssEl = document.getElementById("ticker_css");
			cssEl.parentNode.removeChild(cssEl);
			initialized = false;
			if (interval) clearInterval(interval);
			Initial(remainingMessages.join());
			alert_body_left = body_width;
		}
		else if (alert_body_left-delta > body_width) //has been scrolled to right out of screen
		{
			alert_body_left = -alert_body_width;
		}
		else
		{
			alert_body_left -= delta;
		}
		
		alert_body.style.left = alert_body_left+"px";
	} catch(e) {}
}

function wheel(event)
{
	try {
		var delta = 0;
		if (!event) event = window.event;
		if (event.wheelDelta) {
			delta = event.wheelDelta/120;
			if (window.opera)
				delta = -delta;
		} else if (event.detail) {
			delta = -event.detail/3;
		}
		if (delta)
				scrollticker(Math.round(delta)*20, true);
		if (event.preventDefault)
				event.preventDefault();
		event.returnValue = false;
	} catch(e) {}
}

if (window.addEventListener) window.addEventListener('DOMMouseScroll', wheel, false);
window.onmousewheel = document.onmousewheel = wheel;
