<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<script language="JScript" src="jscripts/json2.js" runat="server"></script>

<script language="JScript" runat="server">

	function changeFormsOrder(Conn,formsJson)
	{

		var forms = JSON.parse(formsJson);
		var formsSQL = "";
		
		for (var i=0; i<forms.length; i++)
		{
			formsSQL += " UPDATE alert_param_template_forms SET [order]="+i+" WHERE id='"+forms[i].id.replace("'","''")+"'";
		}

		var result = true;
		if (formsSQL)
		{
			Conn.Execute(formsSQL);
		}
	
		return result;
	}
</script>

<%
	tempId = Request("id")
	formId = Request("form_id")
	formHtml = Request("form_html")
	formName = Request("form_name")
	paramId = Request("param_id")
	paramName = Request("param_name")
	paramShortName= Request("param_short_name")
	paramType = Request("param_type")
	paramDefaultValue = Request("param_default_value")
	paramVisible = Request("param_visible")
	paramAction = Request("param_action")
	saveFlag = Request("save")
	editParamFlag = Request("edit_param")
	addParamFlag = Request("add_param")
	deleteParamFlag = Request("delete_param")
	getFormFlag = Request("get_form")
	addFormFlag = Request("add_form")
	deleteFormFlag = Request("delete_form")
	orderFormsFlag = Request("order_forms")
	returnPage = Request("return_page")
	formsJson = Request("forms_json")
	
	paramSQL = ""

	
	if (tempId = "") then
		Response.End
	end if
	if (tempId <> "") then
		if (formId <> "" OR formsJson <> "" OR formName <> "") then
		
			formSQL = ""
			
			errorCode = "0"
			errorMessage = ""
			
			result = "{"
			if (getFormFlag = "1") then
				
				formSQL = "SELECT id, name, html, show_condition, next_button_name, previous_button_name, close_button_name, [order] FROM alert_param_template_forms WHERE id='"+Replace(formId,"'", "''")+"'"
				Set formsRS = Conn.Execute(formSQL)
				
				formObject = "{"
				
				if (not formsRS.EOF) then
					formObject = formObject &_
						"""id"": """& formsRS("id") &"""," &_
						"""name"": """& jsEncode(formsRS("name")) &"""," &_
						"""html"": """& jsEncode(formsRS("html")) &"""," &_
						"""show_condition"": """& jsEncode(formsRS("show_condition")) &"""," &_
						"""next_button_name"": """& jsEncode(formsRS("next_button_name")) &"""," &_
						"""previous_button_name"": """& jsEncode(formsRS("previous_button_name")) &"""," &_
						"""close_button_name"": """& jsEncode(formsRS("close_button_name")) &"""," &_
						"""order"": """& formsRS("order") &""""
				else
					errorCode = "1"
					errorMessage = "Can not get form from database. Maybe it is not exists (form id: "&jsEncode(formId)&")"
				end if
			
				formObject = formObject & "}"
				
				formsRS.Close
			
				result = result &_
						"""errorCode"": """& errorCode &"""," &_
						"""errorMessage"": """& errorMessage &"""," &_
						"""form"": "& formObject
						
			elseif (orderFormsFlag = "1") then
			
				if (changeFormsOrder (Conn, formsJson)) then
					result = result &_
					"""errorCode"": """& errorCode &"""," &_
					"""errorMessage"": """& errorMessage &""""
				end if
				
			elseif (addFormFlag = "1") then

				formSQL = "SET NOCOUNT ON" &_
						" DECLARE @formId [uniqueidentifier]" &_
						" DECLARE @formOrder [bigint]" &_
						" DECLARE @formHTML [nvarchar](max)" &_
						" SET @formId = NEWID()" &_
						" SET @formHTML = '{next-button}'" &_
						" SELECT @formOrder = MAX([order])+1 FROM alert_param_template_forms" &_
						" IF NOT EXISTS (SELECT 1 FROM alert_param_template_forms WHERE name = '"&formName&"')" &_
						" BEGIN" &_
							" INSERT INTO alert_param_template_forms (id, name, html, param_temp_id, [order]) VALUES (@formId,'"&formName&"',@formHTML,'"&Replace(tempId, "'", "''")&"', @formOrder )" &_
						" SELECT @formId as id, @formOrder as [order], @formHTML as html, 0 as name_exists" &_
						" END" &_
						" ELSE SELECT 1 AS name_exists"
				Set formsRS = Conn.Execute(formSQL)
				
				formObject = "{"
				
				if (formsRS.State<>0) then
				
					if (not formsRS.EOF) then
						if (formsRS("name_exists") = "1") then
							errorCode = "2"
							errorMessage = "Name already exists"
						else
							formObject = formObject &_
							"""id"": """& formsRS("id") &"""," &_
							"""name"": """& jsEncode(formName) &"""," &_
							"""html"": """& jsEncode(formsRS("html")) &"""," &_
							"""show_condition"": """"," &_
							"""next_button_name"": """"," &_
							"""previous_button_name"": """"," &_
							"""close_button_name"":	""""," &_
							"""order"": """& formsRS("order") &""""
						end if			
					else
						errorCode = "3"
						errorMessage = "Can not add form to database. Maybe it is already exists (form name: "&jsEncode(formName)&")"
					end if
					formsRS.Close
				else
					errorCode = "3"
					errorMessage = "Can not add form to database. Maybe it is already exists (form name: "&jsEncode(formName)&")"
				end if
			
				formObject = formObject & "}"
				
				
			
				result = result &_
						"""errorCode"": """& errorCode &"""," &_
						"""errorMessage"": """& errorMessage &"""," &_
						"""form"": "& formObject	
			
			elseif (deleteFormFlag = "1") then

				formSQL = "DELETE FROM alert_param_template_forms WHERE id = '"&Replace(formId, "'", "''")&"'"

				Set formsRS = Conn.Execute(formSQL)

				result = result &_
						"""errorCode"": """& errorCode &"""," &_
						"""errorMessage"": """& errorMessage &"""" 
			
			elseif (saveFlag = "1") then
				formSQL = "SET NOCOUNT ON" &_
						" IF NOT EXISTS (SELECT 1 FROM alert_param_template_forms WHERE name = '"&Replace(formName, "'", "''")&"' AND id <> '"&Replace(formId, "'", "''")&"')" &_
						" BEGIN" &_
							" UPDATE alert_param_template_forms SET name = '"&Replace(formName, "'", "''")&"', html = '"&Replace(formHtml, "'", "''")&"' WHERE id='"&Replace(formId, "'", "''")&"'" &_
						" SELECT 0 as name_exists" &_
						" END" &_
						" ELSE SELECT 1 AS name_exists"
				'response.write formSQL
				Set formsRS = Conn.Execute(formSQL)
				
				
				formObject = "{"
				
				if (formsRS.State<>0) then
				
					if (not formsRS.EOF) then
						if (formsRS("name_exists") = "1") then
							errorCode = "2"
							errorMessage = "Name already exists"
						else
							formObject = formObject &_
							"""id"": """& formId &"""," &_
							"""name"": """& jsEncode(formName) &"""," &_
							"""html"": """& jsEncode(formHtml) &""""
						end if			
					else
						errorCode = "4"
						errorMessage = "Can not update form in database. (form id: "&jsEncode(formId)&")"
					end if
					formsRS.Close
				else
					errorCode = "4"
					errorMessage = "Can not update form in database. (form id: "&jsEncode(formId)&")"
				end if
			
				formObject = formObject & "}"
				
			
				result = result &_
						"""errorCode"": """& errorCode &"""," &_
						"""errorMessage"": """& errorMessage &"""," &_
						"""form"": "& formObject	
				
			end if
		
			result = result & "}"
			
			Response.Write result
			Response.End
		end if

		actionSQL = "NULL"
		if (paramAction <> "") then
			actionSQL = "'"&Replace(paramAction, "'", "''")&"'"
		end if
		
		defaultValueSQL = "NULL"
		if (paramDefaultValue <> "") then
			defaultValueSQL = "'"&Replace(paramDefaultValue, "'", "''")&"'"
		end if
		
		if (paramId <> "") then
			if (editParamFlag = "1") then
				paramSQL = "UPDATE alert_parameters SET name = '"&Replace(paramName, "'", "''")&"', temp_id = '"&Replace(tempId, "'", "''")&"', short_name = '"&Replace(paramShortName, "'", "''")&"', type = '"&Replace(paramType, "'", "''")&"', [default] = " & defaultValueSQL & ", action =" & actionSQL & ",  visible = '"&Replace(paramVisible, "'", "''")&"', required = 0 WHERE id = '"&Replace(paramId, "'", "''")&"'"
			else
				if (deleteParamFlag = "1") then
					paramSQL = "DELETE FROM alert_parameters WHERE id = '"&Replace(paramId, "'", "''")&"'"
				end if
			end if
		else 
			if (addParamFlag = "1") then
				paramSQL = "INSERT INTO alert_parameters (name,temp_id,short_name,type,visible,[default],action,[order],required) VALUES ('"&Replace(paramName, "'", "''")&"', '"&Replace(tempId, "'", "''")&"', '"&Replace(paramShortName, "'", "''")&"', '"&Replace(paramType, "'", "''")&"', '"&Replace(paramVisible, "'", "''")&"', " & defaultValueSQL & ", " & actionSQL & ", 0, 0 )"
			end if
		end if

		
		if (paramSQL <> "") then
			Conn.Execute(paramSQL)
		end if
		

		
	
	end if
	
%>