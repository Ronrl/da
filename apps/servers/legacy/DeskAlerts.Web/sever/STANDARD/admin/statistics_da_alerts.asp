<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%
check_session()
%>
<!-- #include file="Common.asp" -->
<!-- #include file="header.asp" -->
<!-- #include file="footer.asp" -->
<!-- #include file="libs/FusionCharts.asp" -->
<%
Server.ScriptTimeout = 600
Conn.CommandTimeout = 600

'On Error Resume Next
' index CustomIncludes End
'-------------------------------

'===============================
' Save Page and File Name available into variables
'-------------------------------
Dim sFileName
Dim sTemplateFileName
Dim sPaginateFileName
Dim intSessionUserId

sFileName = "statistics_da_alerts.asp"
sTemplateFileName = "statistics_da_alerts.html"
sPaginateFileName = "paginate.html"
'===============================
  

intSessionUserId = Session("uid")
uid=intSessionUserId

if (intSessionUserId <> "") then


' index Show begin

'===============================
' Display page
'-------------------------------
' Load HTML template for this page
'-------------------------------
LoadTemplate alerts_dir & "\" & templateFolder & "\" & sTemplateFileName, "main"
SetVar "DListAlertsStat", ""
'-------------------------------
' Load HTML template of Header and Footer
'-------------------------------
sHeaderFileName=Replace(sHeaderFileName,"header.html","header-old.html")
LoadTemplate sHeaderFileName, "Header"
LoadTemplate sFooterFileName, "Footer"

'-------------------------------
LoadTemplate sDateFormFileName, "DateForm"
LoadTemplate sMenuFileName, "Menu"

'SetVar "myvalue", "test"


Dim intInstallNum
Dim intUninstallNum
Dim rsToolbar
Dim rsInstall
Dim rsUninstall

Dim i
Dim days

Dim from_day
Dim from_month
Dim from_year

Dim to_day
Dim to_month
Dim to_year



' Get dates
start_date = Request("start_date")

end_date = Request("end_date")


if(Request("select_date_radio")="1" OR Request("select_date_radio")="") then
	if(Request("range")="1" OR Request("range")="") then
	        i=date()
		my_day = day(i)
		my_month = month(i)
		my_year = year (i)
	        i=date()
		my_day1 = day(i)
		my_month1 = month(i)
		my_year1 = year (i)

		test_date=make_date_by(my_day, my_month, my_year)
		test_date1=make_date_by(my_day1, my_month1, my_year1)

	end if
	if(Request("range")="2") then
	        i=date()-1
		my_day = day(i)
		my_month = month(i)
		my_year = year (i)

	        i=date()-1
		my_day1 = day(i)
		my_month1 = month(i)
		my_year1 = year (i)

		test_date=make_date_by(my_day, my_month, my_year)
		test_date1=make_date_by(my_day1, my_month1, my_year1)

	end if
	if(Request("range")="3") then

	        i=date()-7
		my_day = day(i)
		my_month = month(i)
		my_year = year (i)

	        i=date()
		my_day1 = day(i)
		my_month1 = month(i)
		my_year1 = year (i)

		test_date=make_date_by(my_day, my_month, my_year)
		test_date1=make_date_by(my_day1, my_month1, my_year1)

	end if
	if(Request("range")="4") then
		my_day = "01"
		my_month = month(date ())
		my_year = year (date())

		my_day1 = day(date())
		my_month1 = month(date())
		my_year1 = year(date())

		'date from config---	
		test_date=make_date_by(my_day, my_month, my_year)
		test_date1=make_date_by(my_day1, my_month1, my_year1)
		'-------------------

	end if
	if(Request("range")="5") then
		my_day = "01"
		my_month = month(date ())-1
		my_year = year (date())
		
		my_month1 = month(date ())-1
		my_year1 = year (date())
		my_day1 = "31"
		if(IsDate(my_month1 & "/" & my_day1 & "/" & my_year1)<>True) then
			my_day1="30"
			if(IsDate(my_month1 & "/" & my_day1 & "/" & my_year1)<>True) then
				my_day1="29"
				if(IsDate(my_month1 & "/" & my_day1 & "/" & my_year1)<>True) then
					my_day1="28"
					if(IsDate(my_month1 & "/" & my_day1 & "/" & my_year1)<>True) then
						my_day1="27"
					end if

				end if
	        	end if
	        end if
		'date from config---

		test_date=make_date_by(my_day, my_month, my_year)
		test_date1=make_date_by(my_day1, my_month1, my_year1)

		'-------------------
	end if
	if(Request("range")="6") then
		my_day = "01"
		my_month = "01"
		my_year = "2001"

		my_day1 = day(date())
		my_month1 = month(date())
		my_year1 = year(date())

		'date from config---
		test_date=make_date_by(my_day, my_month, my_year)
		test_date1=make_date_by(my_day1, my_month1, my_year1)

	end if
	from_date = test_date & " 00:00:00"
	to_date = test_date1 & " 23:59:59"
else


	'date from config---	

	from_date=start_date & " 00:00:00"

	to_date=end_date & " 23:59:59"

	'-------------------
end if



days = array ("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec")

if(Request("select_date_radio")="1" OR Request("select_date_radio")="") then setVar "strCheckedOption1", "checked" end if 

if(Request("select_date_radio")="2") then setVar "strEnabledOption1", "disabled" end if 

if(Request("range")="1") then setVar "strSelectedOption1", "selected" end if 
if(Request("range")="2") then setVar "strSelectedOption2", "selected" end if 
if(Request("range")="3") then setVar "strSelectedOption3", "selected" end if 
if(Request("range")="4") then setVar "strSelectedOption4", "selected" end if 
if(Request("range")="5") then setVar "strSelectedOption5", "selected" end if 
if(Request("range")="6") then setVar "strSelectedOption6", "selected" end if 

if(Request("select_date_radio")="2") then setVar "strCheckedOption2", "checked" end if 

if(Request("select_date_radio")="1" OR Request("select_date_radio")="") then setVar "strEnabledOption2", "disabled" end if 

SetVar "startDateValue", start_date
SetVar "endDateValue", end_date

   if(Request("offset") <> "" AND Request("offset") <> "{offset}") then 
	offset = clng(Request("offset"))
   else 
	offset = 0
   end if	

   if(Request("limit") <> "") then 
	limit = clng(Request("limit"))
   else 
	limit=25
   end if


SetVar "type", Request("type")
SetVar "limit", limit
SetVar "offset", offset
SetVar "sortby", Request("sortby")
SetVar "SearchTitle", LNG_SEARCH_ALERT_BY_TITLE
SetVar "Search", LNG_SEARCH

classIds = Request("class")
classIdsStr = ""
classVarStr = ""
classRequestStr = ""


if classIds = "" then 
	classIdsStr = "1" 
	classRequestStr = "class=1"
elseif (varType(classIds) = 8) then
	classIds = Split(classIds,",")
	For Each classId In classIds
		if (classIdsStr <> "") then 
			classIdsStr = classIdsStr & "," 
			classRequestStr = classRequestStr &"&"
		end if
		classIdsStr = classIdsStr & classId
		classVarStr = classVarStr & "<input type='hidden' name='class' value='"&Clng(classId)&"'/>"
		classRequestStr = classRequestStr &"class="&classId
	Next
else
	classIdsStr = Clng(classIds) & ""
end if
classIdsArr = Split(classIdsStr, ",")

SetVar "class", classVarStr

'--- rights ---
set rights = getRights(uid, true, true, true, null)
gid = rights("gid")
gsendtoall = rights("gsendtoall")
gviewall = rights("gviewall")
gviewlist = rights("gviewlist")
'--------------

'---------------------------------

if(Request("sortby") <> "" AND Request("sortby") <> "{sortby}") then 
	sortby = Request("sortby")
else 
	sortby = "sent_date desc"
end if	

    searchSql = ""
    if(Request("uname") <> "") then 
     searchSql = " AND alerts.title LIKE '%" & Request("uname") & "%'"
    end if

    archiveSql = ""

    if(Request("uname") <> "") then 
     archiveSql = " AND archive_alerts.title LIKE '%" & Request("uname") & "%'"
    end if

    
if gviewall = 1 or gid=0 then
	
	SQL = "SELECT COUNT(1) as mycnt, 1 as mycnt1 FROM alerts WHERE alerts.sent_date >= '" & from_date & "' AND alerts.sent_date <= '" & to_date & "' AND type='S' AND class IN ("& classIdsStr &")  AND alerts.id NOT IN (SELECT alert_id FROM instant_messages) " & searchSql
	SQL = SQL & " UNION ALL "
	SQL = SQL & "SELECT COUNT(1) as mycnt, 2 as mycnt1 FROM archive_alerts WHERE archive_alerts.sent_date >= '" & from_date & "' AND archive_alerts.sent_date <= '" & to_date & "' AND type='S' AND class IN ("& classIdsStr &")  AND archive_alerts.id NOT IN (SELECT alert_id FROM instant_messages) " & archiveSql
	'Response.Write SQL
    Set RS = Conn.Execute(SQL)
else
	
	SQL = "SELECT COUNT(1) as mycnt, 1 as mycnt1 FROM alerts WHERE alerts.sent_date >= '" & from_date & "' AND alerts.sent_date <= '" & to_date & "' AND type='S' AND class IN ("& classIdsStr &") AND sender_id IN ("& Join(gviewlist,",") &")  AND alerts.id NOT IN (SELECT alert_id FROM instant_messages) " & searchSql
	SQL = SQL & " UNION ALL "
	SQL = SQL & "SELECT COUNT(1) as mycnt, 2 as mycnt1 FROM archive_alerts WHERE archive_alerts.sent_date >= '" & from_date & "' AND archive_alerts.sent_date <= '" & to_date & "' AND type='S' AND class IN ("& classIdsStr &") AND sender_id IN ("& Join(gviewlist,",") &")  AND archive_alerts.id NOT IN (SELECT alert_id FROM instant_messages) " & archiveSql
    Set RS = Conn.Execute(SQL)
end if

cnt=0
do while Not RS.EOF
	cnt = cnt + Clng(RS("mycnt"))
	RS.MoveNext
Loop
RS.Close
j=cnt/limit


if InArray(5, classIdsArr) then
	PageName(LNG_STATISTICS & ": " & LNG_RSS_STATISTICS)
	GenerateMenu(4)
elseif InArray(2, classIdsArr) then
	PageName(LNG_STATISTICS & ": " & LNG_SS_STATISTICS)
	GenerateMenu(5)
elseif InArray(8, classIdsArr) then
	PageName(LNG_STATISTICS & ": " & LNG_WP_STATISTICS)
	GenerateMenu(6)
else
	PageName(LNG_STATISTICS_ALERTS_STATISTICS)
	GenerateMenu(1)
end if

if(DA=1) then
else
	SetVar "strLogout", "Logout"
end if

req_class = Request("class")
if (req_class="2" OR req_class="5" OR req_class ="8") then
	strAlertTypeStyle="display:none"
else
	strAlertTypeStyle=""
end if
SetVar "strAlertTypeStyle", strAlertTypeStyle

SetVar "LNG_ALERTS", LNG_ALERTS
SetVar "LNG_TYPE", LNG_TYPE
SetVar "LNG_CREATION_DATE", LNG_CREATION_DATE
SetVar "LNG_SENT_DATE", LNG_SENT_DATE
SetVar "LNG_DATE", LNG_DATE
SetVar "LNG_SENT_BY", LNG_SENT_BY
SetVar "LNG_RECIPIENTS", LNG_RECIPIENTS
SetVar "LNG_OPEN_RATIO", LNG_OPEN_RATIO

if(cnt>0) then

	page=sFileName &"?from_day="& Request("from_day") &"&from_month="& Request("from_month") &"&from_year="& Request("from_year") &"&to_day="& Request("to_day") &"&to_month="& Request("to_month") &"&to_year="& Request("to_year") &"&range="& Request("range") &"&select_date_radio="& Request("select_date_radio") &"&"& classRequestStr &"&"
	name=LNG_ALERTS
	
	SetVar "LNG_ALERTS", sorting_post(LNG_ALERTS,"title", sortby, offset, limit)
	SetVar "LNG_DATE", sorting_post(LNG_DATE,"create_date", sortby, offset, limit)
	SetVar "LNG_SENT_BY", sorting_post(LNG_SENT_BY,"uname", sortby, offset, limit)
	
	' page,
	SetVar "paging", make_pages_post(offset, cnt, limit, name, sortby) 

'show main table
	if gviewall = 1 or gid=0 then
		SQL = "SELECT * FROM (SELECT users.name as uname, alerts.id, alerts.alert_text, alerts.title, alerts.type, alerts.aknown, alerts.sent_date, alerts.type2, alerts.sender_id, alerts.create_date, alerts.class, alerts.urgent, alerts.ticker, alerts.fullscreen, s.id as is_rsvp FROM alerts LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = alerts.id LEFT JOIN users ON users.id = alerts.sender_id WHERE alerts.sent_date >= '" + from_date + "' AND alerts.sent_date <= '" + to_date + "' AND alerts.type='S' AND alerts.class IN ("& classIdsStr &")  AND alerts.id NOT IN (SELECT alert_id FROM instant_messages) " & searchSql
		SQL = SQL & " UNION ALL "
		SQL = SQL & "SELECT users.name as uname, archive_alerts.id, archive_alerts.alert_text, archive_alerts.title, archive_alerts.type, archive_alerts.aknown, archive_alerts.sent_date, archive_alerts.type2, archive_alerts.sender_id, archive_alerts.create_date, archive_alerts.class, archive_alerts.urgent, archive_alerts.ticker, archive_alerts.fullscreen, s.id as is_rsvp FROM archive_alerts LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = archive_alerts.id LEFT JOIN users ON users.id = archive_alerts.sender_id WHERE archive_alerts.sent_date >= '" + from_date + "' AND archive_alerts.sent_date <= '" + to_date + "' AND archive_alerts.type='S' AND archive_alerts.class IN ("& classIdsStr &")  AND archive_alerts.id NOT IN (SELECT alert_id FROM instant_messages)"  & archiveSql &" ) res ORDER BY res."& sortby
		Set RS = Conn.Execute(SQL)
	else
		
		SQL = "SELECT * FROM (SELECT users.name as uname, alerts.id, alerts.alert_text, alerts.title, alerts.type, alerts.aknown, alerts.sent_date, alerts.type2, alerts.sender_id, alerts.create_date, alerts.class, alerts.urgent, alerts.ticker, alerts.fullscreen, s.id as is_rsvp FROM alerts LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = alerts.id LEFT JOIN users ON users.id = alerts.sender_id WHERE alerts.sent_date >= '" + from_date + "' AND alerts.sent_date <= '" + to_date + "' AND alerts.type='S' AND alerts.class IN ("& classIdsStr &") AND alerts.sender_id IN ("& Join(gviewlist,",") &") AND alerts.id NOT IN (SELECT alert_id FROM instant_messages) " & searchSql
		SQL = SQL & " UNION ALL "
		SQL = SQL & "SELECT users.name as uname, archive_alerts.id, archive_alerts.alert_text, archive_alerts.title, archive_alerts.type, archive_alerts.aknown, archive_alerts.sent_date, archive_alerts.type2, archive_alerts.sender_id, archive_alerts.create_date, archive_alerts.class, archive_alerts.urgent, archive_alerts.ticker, archive_alerts.fullscreen, s.id as is_rsvp FROM archive_alerts LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = archive_alerts.id LEFT JOIN users ON users.id = archive_alerts.sender_id WHERE archive_alerts.sent_date >= '" + from_date + "' AND archive_alerts.sent_date <= '" + to_date + "' AND archive_alerts.type='S' AND archive_alerts.class IN ("& classIdsStr &") AND archive_alerts.sender_id IN ("& Join(gviewlist,",") &")  AND archive_alerts.id NOT IN (SELECT alert_id FROM instant_messages) "  & archiveSql &" ) res ORDER BY res."& sortby
		Set RS = Conn.Execute(SQL)
	end if
	num=offset
	if(Not RS.EOF) then RS.Move(offset) end if
	Do While Not RS.EOF
		num=num+1
		if((offset+limit) < num) then Exit Do end if
		if(num > offset AND offset+limit >= num) then
                        myrecipients=""
			mysentby=""
			mydate=RS("sent_date")
			text=RS("title")
			strAlertTitle = "<A href='#' onclick=""javascript: window.open('statistics_da_alerts_view_details.asp?id=" & CStr(RS("id")) & "','', 'status=0, toolbar=0, width=600, height=550, scrollbars=1')"">" & text & "</a>"
			mysent=0
			
			dim isUrgent
			if RS("urgent") = "1" then
				isUrgent = true
			else
				isUrgent = false
			end if
			if RS("class") = "16" then
				if isUrgent = true then
					strAlertType = "<img src=""images/alert_types/urgent_unobtrusive.gif"" title=""Urgent Unobtrusive"">"
				else
					strAlertType = "<img src=""images/alert_types/unobtrusive.gif"" title=""Unobtrusive"">"
				end if
			elseif not isNull(RS("is_rsvp")) then
				if isUrgent = true then
					strAlertType = "<img src=""images/alert_types/urgent_rsvp.png"" title=""Urgent RSVP"">"
				else
					strAlertType = "<img src=""images/alert_types/rsvp.png"" title=""RSVP"">"
				end if
			elseif RS("ticker")="1" then
				if isUrgent = true then
					strAlertType = "<img src=""images/alert_types/urgent_ticker.png"" title=""Urgent Ticker"">"
				else
					strAlertType = "<img src=""images/alert_types/ticker.png"" title=""Ticker"">"
				end if
			elseif RS("fullscreen")="1" then
				if isUrgent = true then
					strAlertType = "<img src=""images/alert_types/urgent_fullscreen.png"" title=""Urgent Fullscreen"">"
				else
					strAlertType = "<img src=""images/alert_types/fullscreen.png"" title=""Fullscreen"">"
				end if
			elseif isUrgent = true then
				strAlertType = "<img src=""images/alert_types/urgent_alert.gif"" title=""Urgent Alert"">"
			elseif  RS("type")="L" then
			        strAlertType = "<img src=""images/alert_types/linkedin.png"" title=""LinkedIn Alert"">"
			else
				strAlertType = "<img src=""images/alert_types/alert.png"" title=""Usual Alert"">"
			end if


			if(RS("type2")="R") then 

				SQL = "SELECT COUNT(DISTINCT user_id) as mycnt, 1 as mycnt1 FROM [alerts_sent_stat] WHERE alert_id="&RS("id")
				SQL = SQL & " UNION ALL "
				SQL = SQL & "SELECT COUNT(DISTINCT user_id) as mycnt, 2 as mycnt1 FROM [archive_alerts_sent_stat] WHERE alert_id="&RS("id")
				Set RS7 = Conn.Execute(SQL)
				do while not RS7.EOF
					if(Not IsNull(RS7("mycnt"))) then mysent=mysent+clng(RS7("mycnt")) end if
					RS7.MoveNext
				loop
                        
				SQL = "SELECT name FROM users INNER JOIN alerts_sent_stat ON users.id=alerts_sent_stat.user_id WHERE alerts_sent_stat.alert_id="&RS("id")
				SQL = SQL & " UNION ALL "
				SQL = SQL & "SELECT name FROM users INNER JOIN archive_alerts_sent_stat ON users.id=archive_alerts_sent_stat.user_id WHERE archive_alerts_sent_stat.alert_id="&RS("id")
				Set RS6 = Conn.Execute(SQL)	
				if(Not RS6.EOF) then
					myrecipients=RS6("name")
				end if
				mmm=0
				do while not RS6.EOF
					mmm=mmm+1
					RS6.MoveNext
				loop
				if(mmm>1) then myrecipients=myrecipients&", ..." end if
			end if
			if(RS("type2")="B") then 
				users_cnt=0
				'broad_cnt=0
				Set RSUsers = Conn.Execute("SELECT COUNT(id) as mycnt FROM users WHERE reg_date <= '" & RS("sent_date") & "'")
		  		if(not RSUsers.EOF) then 
					users_cnt=RSUsers("mycnt") 
		  		end if
		  		'SQL = "SELECT COUNT(id) as mycnt, 1 as mycnt1 FROM alerts WHERE create_date >= '" & from_date & "' AND create_date <= '" & to_date & "' AND type2='B'"
				'SQL = SQL & " UNION ALL "
				'SQL = SQL & "SELECT COUNT(id) as mycnt, 2 as mycnt1 FROM archive_alerts WHERE create_date >= '" & from_date & "' AND create_date <= '" & to_date & "' AND type2='B'"
				'Set RSBroad = Conn.Execute(SQL)
		  		'do while not RSBroad.EOF
				'	if(Not IsNull(RSBroad("mycnt"))) then broad_cnt=broad_cnt + clng(RSBroad("mycnt") ) end if
				'	RSBroad.MoveNext
				'loop

				mysent=users_cnt
				mytype="Broadcast" 
				myrecipients=LNG_BROADCAST
			end if

			if RS("class") = "2" or RS("class") = "8" then
				received_from = "alerts_read"
				archive_received_from = "archive_alerts_read"
			else
				received_from = "alerts_received"
				archive_received_from = "archive_alerts_received"
			end if
			myreceived=0
			SQL = "SELECT COUNT(DISTINCT user_id) as mycnt, 1 as mycnt1 FROM ["& received_from &"] WHERE alert_id="&RS("id")
			SQL = SQL & " UNION ALL "
			SQL = SQL & "SELECT COUNT(DISTINCT user_id) as mycnt, 2 as mycnt1 FROM ["& archive_received_from &"] WHERE alert_id="&RS("id")
			Set RS7 = Conn.Execute(SQL)
			do while not RS7.EOF
				if(Not IsNull(RS7("mycnt"))) then myreceived=myreceived+clng(RS7("mycnt")) end if
				RS7.MoveNext
			loop

			mysentby=RS("uname")

			if(mysent<>0) then
				myopen=Round(clng(myreceived)*100/clng(mysent),2)
			else
				myopen="0"
			end if
			myrecipients = "<A href='#' onclick=""javascript: window.open('view_recipients_list.asp?id=" & CStr(RS("id")) & "','', 'status=0, toolbar=0, width=555, height=450, scrollbars=1')"">" & myrecipients & "</a>"

			SetVar "strAlertTitle", strAlertTitle
			SetVar "strAlertType", strAlertType 
			SetVar "strAlertDate", RS("create_date")
			SetVar "strAlertDate2", mydate
			SetVar "strAlertSentby", mysentby
			SetVar "strRecipients", myrecipients
			SetVar "intOpenRatio", myopen
	
			Parse "DListAlertsStat", True
	end if

	RS.MoveNext
	Loop
	RS.Close

else

'Response.Write "<center><b>There are no alerts.</b></center>"

end if


'-------------


'-------------------------------
' Step through each form
'-------------------------------
Header_Show
Footer_Show
'-------------------------------
' Process page templates
'-------------------------------
Parse "Header", False
Parse "Footer", False
Parse "main", False
'-------------------------------
' Output the page to the browser
'-------------------------------
Response.write PrintVar("main")

' index Show End

'-------------------------------
' Destroy all object variables
'-------------------------------

' index Close Event begin
' index Close Event End

UnloadTemplate
'===============================

else

Login(Request("intErrorCode"))
'response.write "test111222"

end if

'===============================
' Display Grid Form
'-------------------------------
%>
<!-- #include file="db_conn_close.asp" -->
