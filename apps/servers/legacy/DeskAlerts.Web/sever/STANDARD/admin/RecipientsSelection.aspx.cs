﻿using AutoMapper;
using DeskAlerts.ApplicationCore.Dtos.AD;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Repositories;
using DeskAlerts.ApplicationCore.Utilities;
using System.Collections.Generic;

namespace DeskAlertsDotNet.Pages
{
    using DataBase;
    using DeskAlerts.Server.Utils.Interfactes;
    using Models.Permission;
    using Parameters;
    using Resources;
    using System;
    using System.Linq;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Utils.Services;

    public partial class RecipientsSelection : DeskAlertsBasePage
    {
        protected string Sms;

        protected string Email;

        protected string TextToCallAlert;

        protected string Desktop;

        protected string Device;

        protected string title;

        protected string AlertToLoad;

        protected string SelectedOUs;

        protected string SelectedUsers;

        protected string SelectedGroups;

        protected string SelectedComps;

        protected string SelectedIps;

        protected string SendingType;

        protected string SelectedOUsNames;

        protected string SelectedUsersNames;

        protected string SelectedGroupsNames;

        protected string SelectedCompsNames;

        protected string SelectedIpsNames;

        protected bool MoreThanOneAlertLangsUsed;

        private string ipRangeGroup = @"SELECT DISTINCT 
        *
        FROM
        (
            SELECT i.id, 
                   i.name
            FROM alerts_sent_iprange s
                 INNER JOIN ip_range_groups g ON(CAST(s.ip_from / 16777216 AS VARCHAR) 
		         + '.' + CAST((s.ip_from % 16777216) / 65536 AS VARCHAR) 
		         + '.' + CAST(((s.ip_from % 16777216) % 65536) / 256 AS VARCHAR) 
		         + '.' + CAST(((s.ip_from % 16777216) % 65536) % 256 AS VARCHAR)) = g.from_ip
                 AND (CAST(s.ip_to / 16777216 AS VARCHAR) 
		         + '.' + CAST((s.ip_to % 16777216) / 65536 AS VARCHAR) 
		         + '.' + CAST(((s.ip_to % 16777216) % 65536) / 256 AS VARCHAR) 
		         + '.' + CAST(((s.ip_to % 16777216) % 65536) % 256 AS VARCHAR)) = g.to_ip
                 AND s.ip_range_group_id = g.group_id
                 INNER JOIN ip_groups i ON i.id = g.group_id
            WHERE s.alert_id = {0}
        ) tmp;";

        private IPolicyService _policyService;
        private List<OUDto> _ouSet = new List<OUDto>();

        // use curUser to access to current user of DA control panel
        // use dbMgr to access to database
        protected override void OnLoad(EventArgs e)
        {
            var configurationManager = new WebConfigConfigurationManager(Config.Configuration);
            var policyRepository = new PpPolicyRepository(configurationManager);
            var groupRepository = new PpGroupRepository(configurationManager);
            var domainRepository = new PpDomainRepository(configurationManager);
            var userRepository = new PpUserRepository(configurationManager);
            var policyPublisherRepository = new PpPolicyPublisherRepository(configurationManager);
            var skinRepository = new PpSkinRepository(configurationManager);
            var contentSettingsRepository = new PpContentSettingsRepository(configurationManager);

            _policyService = new PolicyService(policyRepository, groupRepository, domainRepository, policyPublisherRepository, userRepository, skinRepository, contentSettingsRepository);

            base.OnLoad(e);

            orgTree.SelectedNodeChanged += orgTree_SelectedNodeChanged;
            orgTree.ShowLines = true;

            BuildOrganizationTree();

            if (!string.IsNullOrEmpty(Request["desktop"]))
            {
                Desktop = Request["desktop"];
            }

            if (!string.IsNullOrEmpty(Request["email"]))
            {
                Email = Request["email"];
            }

            if (!string.IsNullOrEmpty(Request["sms"]))
            {
                Sms = Request["sms"];
            }

            if (!string.IsNullOrEmpty(Request["textToCall"]))
            {
                TextToCallAlert = Request["textToCall"];
            }

            if (!string.IsNullOrEmpty(Request["device"]))
            {
                Device = Request["device"];
            }

            if (!string.IsNullOrEmpty(Request["id"]))
            {
                alertId.Value = Request["id"];
                alertType.Value = dbMgr.GetScalarByQuery<string>($"SELECT class FROM alerts WHERE id = {alertId.Value}"); ;
            }

            if (!string.IsNullOrEmpty(Request["survey"]))
            {
                survey.Value = Request["survey"];
            }

            if (!string.IsNullOrEmpty(Request["instant"]))
            {
                instant.Value = Request["instant"];
            }

            if (!string.IsNullOrEmpty(Request["shouldApprove"]))
            {
                shouldApprove.Value = Request["shouldApprove"];
            }

            if (!string.IsNullOrEmpty(Request["rejectedEdit"]))
            {
                rejectedEdit.Value = Request["rejectedEdit"];
            }

            if (!string.IsNullOrEmpty(Request["return_page"]))
            {
                returnPage.Value = Request["return_page"];
            }

            Logger.Trace($"Variable: {nameof(Desktop)}. Value: {Desktop}.");
            Logger.Trace($"Variable: {nameof(Email)}. Value: {Email}.");
            Logger.Trace($"Variable: {nameof(Sms)}. Value: {Sms}.");
            Logger.Trace($"Variable: {nameof(Device)}. Value: {Device}.");
            Logger.Trace($"Variable: {nameof(alertId.Value)}. Value: {alertId.Value}.");
            Logger.Trace($"Variable: {nameof(survey.Value)}. Value: {survey.Value}.");
            Logger.Trace($"Variable: {nameof(instant.Value)}. Value: {instant.Value}.");
            Logger.Trace($"Variable: {nameof(shouldApprove.Value)}. Value: {shouldApprove.Value}.");
            Logger.Trace($"Variable: {nameof(rejectedEdit.Value)}. Value: {rejectedEdit.Value}.");
            Logger.Trace($"Variable: {nameof(returnPage.Value)}. Value: {returnPage.Value}.");
            Logger.Trace($"Variable: {nameof(title)}. Value: {title}.");

            sendButton.Text = resources.LNG_SEND;
            sendButtonBottom.Text = resources.LNG_SEND;
            sendButton.Click += sendButton_Click;
            sendButtonBottom.Click += sendButton_Click;
            headerTitle.InnerText = resources.LNG_ALERTS;
            currentAlertCreationStepNumberSpan.InnerText = totalAlertCreationStepNumberSpan.InnerText = IsMultilanguage() && !IsVideoAlert() ? "3" : "2";
            title = !string.IsNullOrEmpty(Request["title"]) ? Request["title"] : resources.LNG_ADD_ALERT;

            var choiseListItem = new ListItem("-- " + resources.LNG_CHOOSE + " --", "0");

            objectType.Items.Add(choiseListItem);

            if (!Config.Data.IsDemo && Request.GetParam("webalert", "0") != "1" && IsBroadcastEnabled())
            {
                var broadcastItem = new ListItem(resources.LNG_BROADCAST, "B");
                objectType.Items.Add(broadcastItem);
            }

            var recipientsItem = new ListItem(resources.LNG_SELECT_RECIPIENTS, "R");

            objectType.Items.Add(recipientsItem);

            if (Request.GetParam("edit", string.Empty) == "1")
            {
                edit.Value = "1";
                sendButton.Text = resources.LNG_SAVE;
                sendButtonBottom.Text = resources.LNG_SAVE;
            }

            if (Config.Data.IsOptionEnabled("ConfEnableIPRanges") &&
                (curUser.HasPrivilege || curUser.Policy[Policies.IP_GROUPS].CanSend) &&
                Request["webalert"] != "1")
            {
                var ipGroupsItem = new ListItem(resources.LNG_IP_GROUPS, "I");
                
                objectType.Items.Add(ipGroupsItem);
            }

            if (Request.GetParam("isModerated", string.Empty) == "1")
            {
                isModerated.Value = "1";
                sendButton.Text = resources.LNG_SEND;
                sendButtonBottom.Text = resources.LNG_SEND;
            }

            if (Request.GetParam("instant", string.Empty) == "1")
            {
                sendButton.Text = resources.LNG_SAVE;
                sendButtonBottom.Text = resources.LNG_SAVE;
            }

            var campaignId = Request.GetParam("campaign_id", "-1");

            if (campaignId != "-1")
            {
                sendButton.Text = resources.LNG_SAVE;
                sendButtonBottom.Text = resources.LNG_SAVE;
                this.campaignId.Value = campaignId;
            }

            if (!curUser.HasPrivilege && Config.Data.HasModule(Modules.APPROVE) && Settings.Content["ConfAllowApprove"] == "1")
            {
                var alertClass = (AlertType)int.Parse(alertType.Value);
                if (!curUser.Policy[alertClass].CanApprove)
                {
                    sendButton.Text = resources.LNG_APPROVE_SUBMIT;
                    sendButtonBottom.Text = resources.LNG_APPROVE_SUBMIT;
                }
            }

            if (!string.IsNullOrEmpty(Request["dup_id"]))
            {
                AlertToLoad = Request["dup_id"];

                // Check OUs
                SendingType = dbMgr.GetScalarByQuery<string>("SELECT type2 FROM alerts WHERE id = " + AlertToLoad);

                DataSet ouSet = dbMgr.GetDataByQuery("SELECT a.ou_id, b.name FROM alerts_sent_ou as a INNER JOIN OU as b ON b.id = a.ou_id WHERE a.alert_id = " + AlertToLoad);

                SelectedOUs = string.Join(",", ouSet.ToArray().Select(row => row.GetInt("ou_id")).ToArray());
                SelectedOUsNames = string.Join(",", ouSet.ToArray().Select(row => row.GetString("name").DoubleQuotes()).ToArray());

                DataSet usersSet = dbMgr.GetDataByQuery("SELECT a.user_id, b.name FROM alerts_sent as a INNER JOIN users as b ON a.user_id = b.id WHERE a.alert_id = " + AlertToLoad);

                SelectedUsers = string.Join(",", usersSet.ToArray().Select(row => row.GetInt("user_id")).ToArray());
                SelectedUsersNames = string.Join(",", usersSet.ToArray().Select(row => row.GetString("name").DoubleQuotes()).ToArray());

                DataSet groupsSet = dbMgr.GetDataByQuery("SELECT a.group_id, b.name FROM alerts_sent_group as a INNER JOIN groups as b ON a.group_id = b.id WHERE a.alert_id = " + AlertToLoad);
                SelectedGroupsNames = string.Join(",", groupsSet.ToArray().Select(row => row.GetString("name").DoubleQuotes()).ToArray());

                SelectedGroups = string.Join(",", groupsSet.ToArray().Select(row => row.GetInt("group_id")).ToArray());

                DataSet compsSet = dbMgr.GetDataByQuery("SELECT a.comp_id, b.name FROM alerts_sent_comp as a INNER JOIN computers as b ON a.comp_id = b.id WHERE a.alert_id = " + AlertToLoad);

                SelectedComps = string.Join(",", compsSet.ToArray().Select(row => row.GetInt("comp_id")).ToArray());
                SelectedCompsNames = string.Join(",", compsSet.ToArray().Select(row => row.GetString("name").DoubleQuotes()).ToArray());

                DataSet ipsSet
                        = dbMgr.GetDataByQuery(string.Format(ipRangeGroup, AlertToLoad));

                SelectedIps = string.Join(",", ipsSet.ToArray().Select(row => row.GetInt("id")).ToArray());
                SelectedIpsNames = string.Join(
                    ",",
                    ipsSet.ToArray().Select(row => row.GetString("name").DoubleQuotes()).ToArray());
            }
            else
            {
                AlertToLoad = string.Empty;
            }

            var templatesSet = dbMgr.GetDataByQuery("SELECT * FROM groups WHERE custom_group = 1");

            foreach (var template in templatesSet)
            {
                var groupName = template.GetString("name");
                var groupId = template.GetString("id");

            }
        }

        /// <summary>
        /// This method checks if there is any user for the publisher with some policy
        /// </summary>
        /// <returns> True if any user for this publisher with this policy exists</returns>
        private bool IsBroadcastEnabled()
        {
            if (curUser.Policy.IsBroadcastEnabled)
            {
                return true;
            }
            else
            {
                var type = (AlertType)int.Parse(alertType.Value);

                var configurationManager = new WebConfigConfigurationManager(Config.Configuration);
                var policyRepository = new PpPolicyRepository(configurationManager);
                var domainRepository = new PpDomainRepository(configurationManager);
                var groupRepository = new PpGroupRepository(configurationManager);
                var userRepository = new PpUserRepository(configurationManager);
                var policyPublisherRepository = new PpPolicyPublisherRepository(configurationManager);
                var skinRepository = new PpSkinRepository(configurationManager);
                var contentSettingsRepository = new PpContentSettingsRepository(configurationManager);

                var policyService = new PolicyService(policyRepository, groupRepository, domainRepository, policyPublisherRepository,
                    userRepository, skinRepository, contentSettingsRepository);

                var policies = policyService.GetPoliciesByPublisherIdAndAlertType(curUser.Id, type);
                return policies.Any(x => x.IsBroadcastEnabled);
            }
        }

        private void sendButton_Click(object sender, EventArgs e)
        {
            DeskAlertsBaseListPage.CleanUploadBulkOfRecipientsDirectory();
            SubmitForm();
        }

        private void SubmitForm()
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "SUBMIT", "submitForm()", true);
        }

        private void orgTree_SelectedNodeChanged(object sender, EventArgs e)
        {
        }

        private void BuildOrganizationTree()
        {
            TreeNode root = new TreeNode(resources.LNG_ORGANIZATION);
            root.NavigateUrl = "javascript:changeUsersUnit(\'DOMAIN\', -1)";
            root.Selected = true;
            root.Expanded = true;
            root.ImageUrl = "images/ad_icons/organization.png";
            root.SelectAction = TreeNodeSelectAction.Select;
            BuildOrganizationTree(root);
            orgTree.Nodes.Add(root);
        }

        private void BuildOrganizationTree(TreeNode root)
        {
            Logger.Trace($"Variable: {root}. Value: {root}.");

            orgTree.Nodes.Clear();
            var domainsSet = dbMgr.GetDataByQuery("SELECT id, name FROM domains ORDER BY name ASC");

            foreach (var domainRow in domainsSet)
            {
                var domainId = domainRow.GetInt("id");
                var domainName = domainRow.GetString("name");

                if (domainName.Trim().Length == 0)
                {
                    domainName = resources.LNG_REGISTERED;
                }

                var domainNode = new TreeNode(domainName)
                {
                    Expanded = false,
                    ImageUrl = "images/ad_icons/domain.png",
                    NavigateUrl = $"javascript:changeUsersUnit('DOMAIN', {domainId})",
                    SelectAction = TreeNodeSelectAction.Select,
                    Value = @"domain_" + domainId
                };

                root.ChildNodes.Add(domainNode);

                var getOUsByDomainIdQuery = $"SELECT ou.id, ou.Name, h.Id_parent FROM OU INNER JOIN OU_hierarchy as h ON h.Id_child = ou.Id WHERE ou.Id_domain = {domainId} ORDER BY h.id_parent ASC, ou.Name ASC";
                dbMgr.GetDataByQuery(getOUsByDomainIdQuery).ToList().ForEach(el =>
                {
                    _ouSet.Add(Mapper.Map<DataRow, OUDto>(el));
                });

                AddOUToTree(domainNode, -1);
            }
        }

        void AddOUToTree(TreeNode parentNode, int parentId)
        {
            List<OUDto> ouSet = _ouSet?.Where(ou => ou.ParentId == parentId).ToList();
            parentNode.SelectAction = TreeNodeSelectAction.Select;

            if (ouSet != null)
                foreach (var ouRow in ouSet)
                {
                    string ouName = ouRow.Name;
                    int ouId = ouRow.Id;

                    TreeNode ouNode = new TreeNode(ouName);
                    ouNode.Expanded = false;
                    ouNode.Value = "ou_" + ouId;
                    ouNode.ImageUrl = "images/ad_icons/ou.png";
                    ouNode.ShowCheckBox = true;
                    ouNode.NavigateUrl =
                        $"javascript:changeUsersUnit('ou', {ouId})";
                    parentNode.ChildNodes.Add(ouNode);
                    AddOUToTree(ouNode, ouId);
                }
        }

        private bool IsVideoAlert()
        {
            return Request["return_page"] == "Videos.aspx";
        }

        private bool IsMultilanguage()
        {
            var languageCount = dbMgr.GetScalarQuery<int>("SELECT count(id) FROM alerts_langs");
            return languageCount > 1 && Config.Data.IsOptionEnabled("ConfEnableAlertLangs");
        }
    }
}