<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<%
check_session()
%>
<!-- #include file="Common.asp" -->
<!-- #include file="header.asp" -->
<!-- #include file="footer.asp" -->

<%
' index CustomIncludes End
'-------------------------------

'===============================
' Save Page and File Name available into variables
'-------------------------------
Dim sFileName
Dim sTemplateFileName
Dim sPaginateFileName
Dim intSessionUserId

if(DA=1) then
	intSessionUserId = Session("uid")
else
	intSessionUserId = Session("intUserId")
end if


sFileName = "view_tb_stat_detailed.asp"
sTemplateFileName = "view_tb_stat_detailed.html"
sPaginateFileName = "paginate.html"
'===============================


if (intSessionUserId <> "") then


' index Show begin

'===============================
' Display page
'-------------------------------
' Load HTML template for this page
'-------------------------------
LoadTemplate sAppPath & sTemplateFileName, "main"

'-------------------------------
' Load HTML template of Header and Footer
'-------------------------------
LoadTemplate sHeaderFileName, "Header"
LoadTemplate sFooterFileName, "Footer"
'-------------------------------
intToolbarId=request("toolbar_id")
strDate=request("date")

SetVar "strDate", strDate

	Set rsToolbar = Conn.Execute("SELECT name FROM [toolbars] WHERE id=" & intToolbarId)
	if not rsToolbar.EOF then
		strToolbarName=rsToolbar("name")
	end if

PageName("Detailed statistics: " & strToolbarName & " Date: " &strDate)



	Set rsUrl = Conn.Execute("SELECT * FROM [urls] WHERE toolbar_id=" & intToolbarId & " AND search<>1")
	do while not rsUrl.EOF
	       	intClickNum=0
	  	Set rsUrlStat = Conn.Execute("SELECT * FROM [url_stat] WHERE url_id=" & rsUrl("id") & " AND toolbar_id=" & intToolbarId & " AND date= '" & strDate & "'")
		  do while not rsUrlStat.EOF
			intClickNum=intClickNum+1
			rsUrlStat.MoveNext	
		  loop
		  if(intClickNum<>0) then
			SetVar "strClickCount", intClickNum
			SetVar "strURL", rsUrl("url")
			Parse "DListClicksStat", True
		  end if
		rsUrl.MoveNext	
	loop
	rsUrl.Close

	Set rsUrl = Conn.Execute("SELECT url FROM [urls] WHERE toolbar_id=" & intToolbarId & " AND search=1 GROUP BY url")
	do while not rsUrl.EOF
	       	intClickNum=0
	  	Set rsUrlStat = Conn.Execute("SELECT url_stat.id as id FROM [url_stat] INNER JOIN [urls] ON url_stat.url_id=urls.id WHERE url='" & rsUrl("url") & "' AND search=1 AND url_stat.toolbar_id=" & intToolbarId & " AND date= '" & strDate & "'")
		  do while not rsUrlStat.EOF
			intClickNum=intClickNum+1
			rsUrlStat.MoveNext	
		  loop
		  if(intClickNum<>0) then
			SetVar "strSearchCount", intClickNum
			Set rsEngine = Conn.Execute("SELECT id FROM [urls] WHERE toolbar_id=" & intToolbarId & " AND search=1 AND url='"&rsUrl("url")&"'")
			if(not rsEngine.EOF) then
				eng_id=rsEngine("id")
			end if 
			SetVar "strEngine", "<A href=view_tb_stat_detailed_engine.asp?eng_id="&eng_id&"&date="&strDate&"&toolbar_id="&intToolbarId&">"& rsUrl("url") & "</a>"
'			SetVar "strEngine", rsUrl("url")
			Parse "DListSearchesStat", True
		  end if
		rsUrl.MoveNext	
	loop
	rsUrl.Close





'-------------------------------
' Step through each form
'-------------------------------
Header_Show
Footer_Show
'-------------------------------
' Process page templates
'-------------------------------
Parse "Header", False
Parse "Footer", False
Parse "main", False
'-------------------------------
' Output the page to the browser
'-------------------------------
Response.write PrintVar("main")

' index Show End

'-------------------------------
' Destroy all object variables
'-------------------------------

' index Close Event begin
' index Close Event End

UnloadTemplate
'===============================

else

Login(Request("intErrorCode"))
'response.write "test111222"

end if

'===============================
' Display Grid Form
'-------------------------------
%>
<!-- #include file="db_conn_close.asp" -->