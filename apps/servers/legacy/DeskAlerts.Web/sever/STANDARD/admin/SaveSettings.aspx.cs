﻿using System;
using System.Data;
using System.Linq;
using System.Web;
using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.ApplicationCore.Repositories;
using DeskAlerts.ApplicationCore.Utilities;
using DeskAlerts.Server.Utils.Helpers;
using DeskAlerts.Server.Utils.Managers;
using DeskAlerts.Server.Utils.Services;
using DeskAlertsDotNet.Parameters;
using DeskAlertsDotNet.sever.STANDARD.Utils;
using DeskAlertsDotNet.Structs;
using DeskAlertsDotNet.Utils.Factories;
using DeskAlertsDotNet.Utils.Services;
using Resources;

namespace DeskAlertsDotNet.Pages
{
    public partial class SaveSettings : DeskAlertsBasePage
    {
        private const string TokenAuthEnableConfigKey = "ConfTokenAuthEnable";

        private string _logReason = string.Empty;

        private IConfigurationService _configurationService = new ConfigurationService(Config.Data.GetString("site"), Config.Data.GetString("alertsDir"));

        private IAutoupdateService _autoupdateService = new AutoupdateService();

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            foreach (var settingsKey in Settings.Content.Keys)
            {
                var requestKey = Request.Form.AllKeys.FirstOrDefault(sk => sk.ToLowerInvariant() == settingsKey.ToLowerInvariant());

                if (string.IsNullOrEmpty(requestKey))
                {
                    var settingsKeySqlParameter = SqlParameterFactory.Create(DbType.String, settingsKey, "settingsKey");
                    dbMgr.ExecuteQuery("UPDATE settings SET val = '0' WHERE name = @settingsKey AND s_type = 'C'", settingsKeySqlParameter);
                }
                else
                {
                    var settingValue = Request[requestKey];
                    if (!string.IsNullOrEmpty(settingValue))
                    {
                        var settingValueSqlParameter = SqlParameterFactory.Create(DbType.String, settingValue, "settingValue");
                        var settingsKeySqlParameter = SqlParameterFactory.Create(DbType.String, settingsKey, "settingsKey");
                        dbMgr.ExecuteQuery($"UPDATE settings SET val = @settingValue WHERE name = @settingsKey", settingValueSqlParameter, settingsKeySqlParameter);
                    }
                }
            }

            CheckTokenAuthToggle();

            LogSessionSettingsChanges("ConfTokenMaxCountForServerEnable", "ConfTokenMaxCountForServer", LogsText.GetLogText(LogsText.LogTextList.ConfTokenMaxCountForServerEnable));
            LogSessionSettingsChanges("ConfTokenMaxCountForUserEnable", "ConfTokenMaxCountForUser", LogsText.GetLogText(LogsText.LogTextList.ConfTokenMaxCountForUserEnable));
            LogSessionSettingsChanges("ConfTokenExpirationTimeEnable", "ConfTokenExpirationTime", LogsText.GetLogText(LogsText.LogTextList.ConfTokenExpirationTimeEnable));

            CheckIfApiKeyChanged();

            var dateFormat = Settings.Content["ConfDateFormat"];
            var timeFormat = Settings.Content["ConfTimeFormat"];

            Settings.Content.Reload();

            if (dateFormat != Settings.Content["ConfDateFormat"] || timeFormat != Settings.Content["ConfTimeFormat"])
            {
                DateTimeConverter.UpdateDateTimeFormat(Settings.Content["ConfDateFormat"],
                    Settings.Content["ConfTimeFormat"]);
            }

            LogLevelManager.ConfigureLogingRulesFromSettings();

            if (Settings.Content["ConfTokenAuthEnable"] != "1")
            {
                var tokenTools = new TokenTools();
                tokenTools.ClearTokens();
            }

            // Verify using authentication
            if (Settings.Content["ConfSmtpAuth"] == "1")
            {
                // assign a string to the variable "SMTP user name"
                string confSmtpUser = Request["ConfSmtpUser"];

                // assign a string to the variable "SMTP password"
                string confSmtpPassword = Request["ConfSmtpPassword"];

                // Output a message if the fields are empty and refresh the page
                if (string.IsNullOrEmpty(confSmtpUser) || string.IsNullOrEmpty(confSmtpPassword))
                {
                    Response.ShowJavaScriptAlert(resources.LNG_USING_AUTHENTICATION);
                    Response.RedirectParent("CommonSettings.aspx");
                }

                // Loging Entered SMTP user name and SMTP password
                _logReason = LogsText.GetLogText(LogsText.LogTextList.UserChangeSettings);
                Logger.Info($"Name: \"{curUser.Name}\" {_logReason} the \"Using authentication is full\" Entered SMTP user name: " +
                    $" {confSmtpUser} SMTP password: {confSmtpPassword}");
            }

            // Desplay a message about data saving
            Response.ShowJavaScriptAlert(resources.LNG_NOTIFICATION_OF_SAVING_SETTINGS);

            string activeTab = Request.GetParam("activeTab", string.Empty);
            var isOktaEnabled = Config.Data.IsOptionEnabled("OKTA_INTEGRATION");
            if (Config.Data.HasModule(Modules.SSO) && Settings.Content["ConfSsoEnable"] == "1" && !isOktaEnabled)
            {
                var configurationManager = new WebConfigConfigurationManager(Config.Configuration);
                var groupRepository = new PpGroupRepository(configurationManager);
                var groups = groupRepository.GetGroupsWithDomain();
                _configurationService.SetGroups(groups);
                _configurationService.UpdateAuthentication(true);
            }
            else if (Settings.Content["ConfSsoEnable"] == "0" && !isOktaEnabled)
            {
                _configurationService.UpdateAuthentication(false);
            }
            else if (Settings.Content["ConfSsoEnable"] == "1" && isOktaEnabled)
            {
                _configurationService.UpdateAuthentication(false);
            }

            UpdateAutoupdateConfiguration();
            UploadAutoupdateFile();

            Response.RedirectParent("CommonSettings.aspx?activeTab=" + activeTab);
        }

        /// <summary>
        /// Log changes if session settings sction
        /// </summary>
        /// <param name="changedCheckboxRequestKey">Changed checkbox index in Request object</param>
        /// <param name="changedInputRequestKey">Changed input index in Request object</param>
        /// <param name="itemDisplayName"></param>
        private void LogSessionSettingsChanges(string changedCheckboxRequestKey, string changedInputRequestKey, string itemDisplayName)
        {
            string tokenAuthEnabled = GetRequestValueByName("ConfTokenAuthEnable");

            // Item enabled in the Request
            string requestItemEnabled = GetRequestValueByName(changedCheckboxRequestKey);

            // Item enabled in database
            string dbItemEnabled = Settings.Content[changedCheckboxRequestKey];
            string requestItemValue = CheckTextFildToNull(changedInputRequestKey);
            string dbItemValue = Settings.Content[changedInputRequestKey];

            if (requestItemEnabled != dbItemEnabled)
            {
                Logger.Info(
                    $"User \"{curUser.Name}\" in \"Session Settings\" change the checkbox \"{itemDisplayName}\" from \"{dbItemEnabled}\" to \"{requestItemEnabled}\"");
            }

            if (tokenAuthEnabled == "1")
            {
                LogSessionSettingsItemChanges(requestItemEnabled, requestItemValue, dbItemValue, itemDisplayName);
            }
        }

        /// <summary>
        /// Log settings items changes for "Session settings" section
        /// </summary>
        /// <param name="itemEnabled">Is selected checkbox enabled</param>
        /// <param name="requestItemValue">Settings item value from request</param>
        /// <param name="dbItemValue">Settings item value from database</param>
        /// <param name="itemDisplayName"></param>
        private void LogSessionSettingsItemChanges(string itemEnabled, string requestItemValue, string dbItemValue, string itemDisplayName)
        {
            if (itemEnabled == "1")
            {
                if (requestItemValue != dbItemValue)
                {
                    Logger.Info(
                        $"User \"{curUser.Name}\" in \"Session Settings\" changed the text field \"{itemDisplayName}\" from \"{dbItemValue}\" to \"{requestItemValue}\" ");
                }
            }
        }

        /// <summary>
        /// Check if "Token auth enable" field is changed
        /// </summary>
        /// <returns></returns>
        private void CheckTokenAuthToggle()
        {
            string dbTokenAuthEnabled = Settings.Content[TokenAuthEnableConfigKey];
            string requestTokenAuthEnabled = GetRequestValueByName(TokenAuthEnableConfigKey);
            string toketAuthDisplayName = LogsText.GetLogText(LogsText.LogTextList.ConfTokenAuthEnable);

            if (requestTokenAuthEnabled != dbTokenAuthEnabled)
            {
                Logger.Info(
                    $"User \"{curUser.Name}\" in \"Session Settings\" change the checkbox \"{toketAuthDisplayName}\" from \"{dbTokenAuthEnabled}\" to \"{requestTokenAuthEnabled}\"");
            }
        }

        private string CheckTextFildToNull(string nameTextFild)
        {
            if (string.IsNullOrEmpty(Request[nameTextFild]))
            {
                return "";
            }

            return Request[nameTextFild];
        }

        private string GetRequestValueByName(string fieldName)
        {
            if (string.IsNullOrEmpty(Request[fieldName]))
            {
                return "0";
            }

            return Request[fieldName];
        }

        /// <summary>
        /// Compate API key from request with API key from database
        /// </summary>
        private void CheckIfApiKeyChanged()
        {
            // API Key field index in Request object
            string requestApiKeyIndex = "ConfAPISecret";
            string requestApiKey = Request[requestApiKeyIndex];
            string dbApiKey = Settings.Content[requestApiKeyIndex];

            // API Key field display name
            string apiKeyDisplayName = LogsText.GetLogText(LogsText.LogTextList.ConfApiSecret);

            if (requestApiKey != dbApiKey)
            {
                Logger.Info(
                    $"User \"{curUser.Name}\" changed the field in {apiKeyDisplayName}");
            }
        }

        private void UpdateAutoupdateConfiguration()
        {
            var maxSlotCacheLength = int.Parse(Settings.Content["ConfAutoupdateSlotCacheLength"]);
            var slotLifeTime = int.Parse(Settings.Content["ConfAutoupdateSlotLifeTime"]);
            _autoupdateService.UpdateConfiguration(maxSlotCacheLength, slotLifeTime);
        }

        private void UploadAutoupdateFile()
        {
            HttpPostedFile file = Request.Files["clientApplicationUpload"];
            if (file != null && file.ContentLength > 0)
            {
                _autoupdateService.SaveFile(file);
                _autoupdateService.ClearSlots();
            }
        }
    }
}