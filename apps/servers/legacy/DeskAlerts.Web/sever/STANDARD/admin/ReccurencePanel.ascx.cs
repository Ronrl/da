﻿using System;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using DeskAlerts.Server.Utils;
using DeskAlerts.Server.Utils.Helpers;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Pages;
using Resources;

namespace DeskAlertsDotNet.Controls
{
    public enum ReccurencePanelError
    {
        TO_DATE_LESS_THAN_FROM_DATE = 0,
        TIME_VALUE_IS_EMPTY = 1,
        DATE_TIME_IS_EMPTY = 2,
        OK = 3,
        TO_DATE_LESS_THAN_CURRENT_DATETIME = 4
    }

    public partial class ReccurencePanel : UserControl
    {
        public int AlertId { get; set; }
        protected DBManager dbMgr = new DBManager();
        public string Pattern { get; private set; }
        protected string countdowns;

        protected void Page_Load(object sender, EventArgs e)
        {
            patternOnce.Text = resources.LNG_ONCE;
            patternDayly.Text = resources.LNG_DAILY;
            patternWeekly.Text = resources.LNG_WEEKLY;
            patternMonthly.Text = resources.LNG_MONTHLY;
            patternYearly.Text = resources.LNG_YEARLY;

            weekdaySunday.Text = resources.LNG_SUNDAY;
            weekdayMonday.Text = resources.LNG_MONDAY;
            weekdayTuesday.Text = resources.LNG_TUESDAY;
            weekdayWednesday.Text = resources.LNG_WEDNESDAY;
            weekdayThursday.Text = resources.LNG_THURSDAY;
            weekdayFriday.Text = resources.LNG_FRIDAY;
            weekdaySaturday.Text = resources.LNG_SATURDAY;


            if (!IsPostBack)
            {
                oncePatternDiv.Style.Add(HtmlTextWriterStyle.Display, "block");
                dailyPatternDiv.Style.Add(HtmlTextWriterStyle.Display, "none");
                weeklyPatternDiv.Style.Add(HtmlTextWriterStyle.Display, "none");
                monthlyPatternDiv.Style.Add(HtmlTextWriterStyle.Display, "none");
                yearlyParrentDiv.Style.Add(HtmlTextWriterStyle.Display, "none");
                alertRange.Style.Add(HtmlTextWriterStyle.Display, "none");
                alertTime.Style.Add(HtmlTextWriterStyle.Display, "none");


                FillWeekdayPlaceSelector(ref m_weekday_place);
                FillWeekdayPlaceSelector(ref y_weekday_day);

                FillNumbersSelector(ref number_days);
                FillNumbersSelector(ref number_week);
                FillNumbersSelector(ref number_month_1);
                FillNumbersSelector(ref number_month_2);

                FillDayPlaceSelector(ref y_weekday_place);
                FillDayPlaceSelector(ref m_weekday_day);

                FillMonthSelector(ref month_val_1);
                FillMonthSelector(ref month_val_2);

                AddLastToDaySelector(ref m_month_day);
                AddLastToDaySelector(ref y_month_day);
            }

            if (AlertId > 0)
            {
                var reccurenceSet = dbMgr.GetDataByQuery($@"SELECT pattern, dayly_selector, monthly_selector, yearly_selector, number_days, number_week, week_days, month_day, 
                                                            number_month, weekday_place, weekday_day, month_val, end_type, occurences 
                                                            FROM recurrence 
                                                            WHERE  pattern != 'c' AND alert_id = {AlertId}");

                if (reccurenceSet.IsEmpty)
                {
                    Pattern = "o";

                    var dateSet = dbMgr.GetDataByQuery("SELECT from_date, to_date FROM alerts WHERE id = " + AlertId);

                    start_date_and_time.Disabled = false;
                    end_date_and_time.Disabled = false;

                    start_date_and_time.Value = DateTimeConverter.ToUiDateTime(dateSet.GetDateTime(0, "from_date"));
                    end_date_and_time.Value = DateTimeConverter.ToUiDateTime(dateSet.GetDateTime(0, "to_date"));

                    return;
                }

                Pattern = reccurenceSet.GetString(0, "pattern");

                if (!string.IsNullOrWhiteSpace(Pattern))
                {
                    pattern.SelectedValue = Pattern;

                    if (!Pattern.Equals("o"))
                    {
                        var dateSet = dbMgr.GetDataByQuery("SELECT from_date, to_date FROM alerts WHERE id = " + AlertId);
                        var fromDate = DateTimeConverter.ParseDateTimeFromDb(dateSet.GetString(0, "from_date"));

                        if (fromDate.Year > 1900)
                        {
                            start_date_rec.Value = DateTimeConverter.ToUiDate(fromDate);
                            start_time.Value = DateTimeConverter.ToUiTime(fromDate);
                        }

                        var toDate = DateTimeConverter.ParseDateTimeFromDb(dateSet.GetString(0, "to_date"));

                        if (toDate.Year > 1900)
                        {
                            end_date_rec.Value = DateTimeConverter.ToUiDate(toDate);
                        }

                        end_time.Value = DateTimeConverter.ToUiTime(toDate);

                        switch (reccurenceSet.GetString(0, "end_type"))
                        {
                            case "no_date":
                                {
                                    radio1.Checked = true;
                                    break;

                                }
                            case "end_after":
                                {
                                    radio2.Checked = true;
                                    end_after.Value = reccurenceSet.GetString(0, "occurences");
                                    break;
                                }
                            case "end_by":
                                {
                                    radio3.Checked = true;
                                    break;
                                }
                            default:
                                {
                                    var exception = new Exception($"{Pattern} is not correct end_type value");

                                    DeskAlertsBasePage.Logger.Error(exception);

                                    throw exception;
                                }
                        }
                    }
                }

                switch (Pattern)
                {
                    case "o":
                        {
                            var dateSet = dbMgr.GetDataByQuery("SELECT from_date, to_date FROM alerts WHERE id = " + AlertId);

                            start_date_and_time.Disabled = true;
                            end_date_and_time.Disabled = true;

                            start_date_and_time.Value = DateTimeConverter.ToUiDateTime(dateSet.GetDateTime(0, "from_date"));
                            end_date_and_time.Value = DateTimeConverter.ToUiDateTime(dateSet.GetDateTime(0, "to_date"));

                            var countDownSet =
                                dbMgr.GetDataByQuery("SELECT number_days FROM recurrence WHERE alert_id = " + AlertId +
                                                     " AND pattern = 'c'");

                            countdowns = string.Join(",",
                                countDownSet.ToList().Select(x => x.GetInt("number_days")).ToArray());
                            break;
                        }
                    case "d":
                        {

                            number_days.Value = reccurenceSet.GetString(0, "number_days");
                            break;

                        }
                    case "w":
                        {
                            number_week.Value = reccurenceSet.GetString(0, "number_week");
                            weekDayPatternCheckBoxList.Enabled = false;
                            int weekInt = reccurenceSet.GetInt(0, "week_days");

                            for (int weekday = 0; weekday < 7; weekday++) // перебираются дни недели начиная с Sunday
                            {
                                if ((weekInt & (1 << weekday)) != 0) // здесь принимается решение что текущий день недели в битовом поле БД отмечен
                                {
                                    // если мы сдесь и bit равен 0 то Sunday отмечен в этом расписании
                                    // если мы сдесь и bit равен 1 то Monday отмечен в этом расписании и так далее
                                    setViewDayOfWeeks(weekday);
                                }
                            }
                            break;
                        }
                    case "m":
                        {
                            string monthlySelector = reccurenceSet.GetString(0, "monthly_selector");

                            if (monthlySelector.Equals("monthly_1"))
                            {
                                monthly1.Checked = true;
                                m_month_day.Value = reccurenceSet.GetString(0, "month_day");
                                number_month_1.Value = reccurenceSet.GetString(0, "number_month");
                            }
                            else if (monthlySelector.Equals("monthly_2"))
                            {
                                monthly2.Checked = true;
                                m_weekday_place.Value = reccurenceSet.GetString(0, "weekday_place");
                                m_weekday_day.Value = reccurenceSet.GetString(0, "weekday_day");
                                number_month_2.Value = reccurenceSet.GetString(0, "number_month");
                            }
                            break;
                        }
                    case "y":
                        {
                            string monthlySelector = reccurenceSet.GetString(0, "yearly_selector");

                            if (monthlySelector.Equals("yearly_1"))
                            {
                                yearly1.Checked = true;
                                y_month_day.Value = reccurenceSet.GetString(0, "month_day");
                                month_val_1.Value = reccurenceSet.GetString(0, "month_val");
                            }
                            else if (monthlySelector.Equals("yearly_2"))
                            {
                                yearly2.Checked = true;
                                month_val_2.Value = reccurenceSet.GetString(0, "month_val");
                                y_weekday_place.Value = reccurenceSet.GetString(0, "weekday_place");
                                y_weekday_day.Value = reccurenceSet.GetString(0, "weekday_day");
                            }
                            break;
                        }
                    default:
                        {
                            var exception = new Exception($"{Pattern} is not correct end_type value");

                            DeskAlertsBasePage.Logger.Error(exception);

                            throw exception;
                        }
                }
            }
            else if (!IsPostBack)
            {
                start_date_and_time.Value = DateTimeConverter.ToUiDateTime(DateTime.Now);
                end_date_and_time.Value = DateTimeConverter.ToUiDateTime(DateTime.Now.AddHours(1));
                start_date_rec.Value = DateTimeConverter.ToUiDate(DateTime.Now);
                end_date_rec.Value = DateTimeConverter.ToUiDate(DateTime.Now.AddYears(10));
                start_time.Value = DateTimeConverter.ToUiTime(DateTime.Now);
                end_time.Value = DateTimeConverter.ToUiTime(DateTime.Now.Date.AddMinutes(-1));
            }
        }

        private void setViewDayOfWeeks(int bit)
        {
            int bitMask = ((int)Math.Pow(2, bit));
            ListItem weekDay =
                weekDayPatternCheckBoxList.Items.Cast<ListItem>()
                    .First(x => x.Value.Equals(bitMask.ToString()));
            weekDay.Selected = true;
        }

        /// <summary>
        /// Validate date in sheduller.
        /// "patternValue" is sheduller pattern: "once", "Daily", "Weekly", "Monthly", "Yearly".
        /// </summary>
        /// <returns>Return sheduller error status.</returns>
        public ReccurencePanelError ValidateInputs()
        {
            string patternValue = pattern.SelectedValue;

            string startDate;
            string endDate;

            if (patternValue == "o")
            {
                startDate = start_date_and_time.Value;
                endDate = end_date_and_time.Value;

                if (startDate.Length == 0 || endDate.Length == 0)
                {
                    return ReccurencePanelError.DATE_TIME_IS_EMPTY;
                }

                var startDateTime = DateTimeConverter.ParseDateTimeFromUi(startDate);
                var endDateTime = DateTimeConverter.ParseDateTimeFromUi(endDate);

                if (endDateTime.CompareTo(startDateTime) < 0)
                {
                    return ReccurencePanelError.TO_DATE_LESS_THAN_FROM_DATE;
                }

                if (!ValidateShedullerEndDateTime(endDateTime))
                {
                    return ReccurencePanelError.TO_DATE_LESS_THAN_CURRENT_DATETIME;
                }
            }
            else
            {
                if (start_time.Value.Length == 0 || end_time.Value.Length == 0 || start_date_rec.Value.Length == 0)
                {
                    return ReccurencePanelError.TIME_VALUE_IS_EMPTY;
                }

                startDate = start_date_rec.Value + " " + start_time.Value;
                endDate = end_date_rec.Value + " " + end_time.Value;

                try
                {
                    if (end_date_rec.Value.Length != 0)
                    {
                        var startDateTime = DateTimeConverter.ParseDateTimeFromUi(startDate);
                        var endDateTime = DateTimeConverter.ParseDateTimeFromUi(endDate);

                        if (endDateTime.CompareTo(startDateTime) < 0)
                        {
                            return ReccurencePanelError.TO_DATE_LESS_THAN_FROM_DATE;
                        }

                        if (!ValidateShedullerEndDateTime(endDateTime))
                        {
                            return ReccurencePanelError.TO_DATE_LESS_THAN_CURRENT_DATETIME;
                        }
                    }
                    else
                    {
                        TimeSpan startSpan = DateTimeConverter.ParseTimeFromUi(start_time.Value).TimeOfDay;
                        TimeSpan endSpan = DateTimeConverter.ParseTimeFromUi(end_time.Value).TimeOfDay;

                        if (endSpan.CompareTo(startSpan) < 0)
                        {
                            return ReccurencePanelError.TO_DATE_LESS_THAN_FROM_DATE;
                        }
                    }
                }
                catch (Exception ex)
                {
                    DeskAlertsBasePage.Logger.Debug(ex);

                    throw new ArgumentException("BAD DATETIME start = " + start_time.Value + " end = " + end_time.Value + "format = " + DeskAlertsUtils.GetTimeFormat(dbMgr));
                }
            }

            return ReccurencePanelError.OK;
        }

        /// <summary>
        /// Checking the end datetime with the current datetime.
        /// </summary>
        /// <param name="endDateTime">Sheduller end datetime.</param>
        /// <returns>Correct datetime.</returns>
        private bool ValidateShedullerEndDateTime(DateTime endDateTime)
        {
            DateTime currentDateTime = DateTime.Now;
            DateTime currentDateTimeWithoutSeconds = new DateTime(currentDateTime.Year, currentDateTime.Month, currentDateTime.Day, currentDateTime.Hour, currentDateTime.Minute, 0);
            if (endDateTime.CompareTo(currentDateTimeWithoutSeconds) < 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }


        protected void OnReccurencePatternChanged(object sender, EventArgs e)
        {
            RadioButtonList rbList = sender as RadioButtonList;

            if (rbList != null)
            {
                int selectedIndex = rbList.SelectedIndex;

                oncePatternDiv.Visible = selectedIndex == 0;
                dailyPatternDiv.Visible = selectedIndex == 1;
                weeklyPatternDiv.Visible = selectedIndex == 2;
                monthlyPatternDiv.Visible = selectedIndex == 3;
                yearlyParrentDiv.Visible = selectedIndex == 4;
            }
        }

        void FillMonthSelector(ref HtmlSelect select)
        {
            select.Items.Add(new ListItem(resources.LNG_JANUARY, "1"));
            select.Items.Add(new ListItem(resources.LNG_FEBRUARY, "2"));
            select.Items.Add(new ListItem(resources.LNG_MARCH, "3"));
            select.Items.Add(new ListItem(resources.LNG_APRIL, "4"));
            select.Items.Add(new ListItem(resources.LNG_MAY, "5"));
            select.Items.Add(new ListItem(resources.LNG_JUNE, "6"));
            select.Items.Add(new ListItem(resources.LNG_JULY, "7"));
            select.Items.Add(new ListItem(resources.LNG_AUGUST, "8"));
            select.Items.Add(new ListItem(resources.LNG_SEPTEMBER, "9"));
            select.Items.Add(new ListItem(resources.LNG_OCTOBER, "10"));
            select.Items.Add(new ListItem(resources.LNG_NOVEMBER, "11"));
            select.Items.Add(new ListItem(resources.LNG_DECEMBER, "12"));
        }
        void FillDayPlaceSelector(ref HtmlSelect select)
        {
            select.Items.Add(new ListItem(resources.LNG_SUNDAY, "1"));
            select.Items.Add(new ListItem(resources.LNG_MONDAY, "2"));
            select.Items.Add(new ListItem(resources.LNG_TUESDAY, "3"));
            select.Items.Add(new ListItem(resources.LNG_WEDNESDAY, "4"));
            select.Items.Add(new ListItem(resources.LNG_THURSDAY, "5"));
            select.Items.Add(new ListItem(resources.LNG_FRIDAY, "6"));
            select.Items.Add(new ListItem(resources.LNG_SATURDAY, "7"));

        }
        void FillWeekdayPlaceSelector(ref HtmlSelect select)
        {
            select.Items.Add(new ListItem(resources.LNG_1ST, "1"));
            select.Items.Add(new ListItem(resources.LNG_2ND, "2"));
            select.Items.Add(new ListItem(resources.LNG_3RD, "3"));
            select.Items.Add(new ListItem(resources.LNG_4TH, "4"));
            select.Items.Add(new ListItem(resources.LNG_LAST, "5"));
        }
        void FillNumbersSelector(ref HtmlSelect select)
        {
            select.Items.Add(new ListItem(resources.LNG_EVERY, "1"));

            for (int i = 2; i < 11; i++)
            {

                string key = "LNG_" + i + "TH";

                if (i == 2)
                    key = "LNG_2ND";
                else if (i == 3)
                    key = "LNG_3RD";

                select.Items.Add(new ListItem(resources.LNG_EVERY + " " + resources.ResourceManager.GetString(key), i.ToString()));
            }
        }

        void AddLastToDaySelector(ref HtmlSelect select)
        {
            select.Items.Add(new ListItem(resources.LNG_LAST, "32"));
        }
    }
}