<!-- #include file="config.inc" -->
<!-- #include file="ad.inc" -->
<!-- #include file="clsToWordpress.asp" -->
<!-- #include file="libs/ASPTwitter.asp" -->
<!-- #include file="functions_inc.asp" -->
<% ConnCount = 3 %>
<!-- #include file="db_conn.asp" -->
<%
Server.ScriptTimeout = 86400
Conn.CommandTimeout = 300

check_session()
Dim is_sended
Dim is_scheduled
is_sended = Request("is_sended")
is_scheduled = Request("is_scheduled")
uid = Session("uid")

%>

<%

if smsPhoneFormat = "" then smsPhoneFormat = "1"
if smsPhoneFormat <> "%smsPhoneFormat%" and smsPhoneFormat <> "0" then
%>

<script language="JScript" runat="server" src="jscripts/phone_format.js"></script>

<script language="JScript" runat="server" src="jscripts/da_phone_format.js"></script>



<%
end if

if textcallPhoneFormat = "" then textcallPhoneFormat = "1"
if textcallPhoneFormat <> "%textcallPhoneFormat%" and textcallPhoneFormat <> "0" then
%>

<script language="JScript" runat="server" src="jscripts/phone_format.js"></script>

<script language="JScript" runat="server" src="jscripts/da_phone_format.js"></script>




<%
end if

Function MyMod(ByVal a, ByVal b)
	a = Fix(CDbl(a))
	b = Fix(CDbl(b))
	MyMod = a - Fix(a/b) * b
End Function

if (uid <> "") then
	gid = 0
	Set RS = Conn.Execute("SELECT id, only_personal FROM users WHERE id="&uid&" AND role='E'")
	if(Not RS.EOF) then
		gid = 1
		Set rs_policy = Conn.Execute("SELECT p.id, p.type, pu.user_id FROM policy p INNER JOIN policy_editor pe ON p.id=pe.policy_id LEFT JOIN policy_user pu ON pu.policy_id = p.id AND pu.user_id = 0 WHERE pe.editor_id=" & uid)
		if Not rs_policy.EOF then
			if rs_policy("type")="A" or not IsNull(rs_policy("user_id")) then 'can send to all
				gid = 0
			end if
		end if
		if(gid=1) then
			editor_id=RS("id")
			only_personal=RS("only_personal")
			policy_ids=""
			policy_ids=editorGetPolicyIds(editor_id)
			group_ids=""
			group_ids=editorGetGroupIds(policy_ids)
			group_ids=replace(group_ids, "group_id", "users_groups.group_id")
			if(group_ids <> "") then
				group_ids = "OR " & group_ids
			end if
			
			ou_ids=""
			ou_ids=editorGetOUIds(policy_ids)
			ou_ids=replace(ou_ids, "ou_id", "OU_User.Id_OU")
			if(ou_ids <> "") then
				ou_ids = "OR " & ou_ids
			end if
		end if
	else
	gid = 0
	end if
	RS.Close

	this_date=now_db
	to_users = 0
	to_groups = 0
	to_comps = 0

	chk_max=Request("chk_max")
	alert_ids_string=Request("alert_id")
	main_alert_id=Request("current_alert_id")
	if Request("instant")="1" then
		instant_message = 1
	else
		instant_message = 0
	end if
	schedule_type = 1 - instant_message
	
	if(Request("campaign_id") <> "-1" and Request("campaign_id") <> "") then
	    schedule_type = 0
	    Set is_started = Conn.Execute("SELECT DISTINCT 1 FROM alerts WHERE campaign_id = " & Request("campaign_id") & " AND schedule_type = '1'")
	    if not is_started.EOF then
		    schedule_type = 1
	    end if
	end if

	send_type=Request("send_type")
	group_ad_type="B"
	alert_ids = Split(alert_ids_string, ",")


	
if(Request("change") = "1") then

	for each alert_id in alert_ids
	Conn.Execute("UPDATE alerts SET schedule_type = '" & schedule_type & "' WHERE id = "& alert_id)	
	if(send_type="broadcast") then
	    Conn.Execute("DELETE FROM alerts_sent WHERE alert_id = " & alert_id)
	    Conn.Execute("DELETE FROM alerts_sent_group WHERE alert_id = " & alert_id)
	   Conn.Execute("DELETE FROM alerts_sent_comp WHERE alert_id = " & alert_id)
	    Conn.Execute("UPDATE alerts SET type = 'S', sent_date=GETDATE(), type2='B', group_type='"&group_ad_type&"', sender_id="&uid&" WHERE campaign_id=" & Request("campaign_id") )
	    Response.Redirect Request("return_page")	
	elseif (send_type="iprange") then
		   
		    Conn.Execute("DELETE FROM alerts_sent WHERE alert_id = " & alert_id)
	    Conn.Execute("DELETE FROM alerts_sent_group WHERE alert_id = " & alert_id)
	   Conn.Execute("DELETE FROM alerts_sent_comp WHERE alert_id = " & alert_id)
	   
	    Conn.Execute("UPDATE alerts SET type = 'S', sent_date=GETDATE(), type2='I', group_type='"&group_ad_type&"', sender_id="&uid&" WHERE campaign_id=" & Request("campaign_id") )
	    
		    objects=request("added_user")    
	    groups = Split(objects, ", ")
		sendIpRange groups, alert_id
	
	elseif (send_type="recepients") then
	
			objects=request("added_user")
			objArray = Split(objects, ", ")
			SET usersArray=jsArray()
			SET groupsArray=jsArray()
			SET computersArray=jsArray()

			for each obj_id in objArray
				if(obj_id<>"") then
					obj_type=Left(obj_id,1)
					if(obj_type="u") then
					'user
						user_id=replace(obj_id,"u","")
						usersArray.push(user_id)
					elseif(obj_type="g") then
					'group
						group_id=replace(obj_id,"g","")
						groupsArray.push(group_id)
					elseif(obj_type="c") then
					'computer
						comp_id=replace(obj_id,"c","")
						computersArray.push(comp_id)
					end if
				end if
			next
			
			Conn.Execute("DELETE FROM alerts_sent WHERE alert_id = " & alert_id)
			
			for each user_id in usersArray
			   	if(IsNumeric(user_id)) then
			     Conn.Execute("INSERT INTO alerts_sent VALUES ("& alert_id &","& user_id &")")
			    end if
			next
			
			Conn.Execute("DELETE FROM alerts_sent_group WHERE alert_id = " & alert_id)
			
			for each  group_id in groupsArray
			    if(IsNumeric(group_id)) then
			     Conn.Execute("INSERT INTO alerts_sent_group VALUES ("& alert_id &","& group_id &")")
			    end if 
			next
			
			Conn.Execute("DELETE FROM alerts_sent_comp WHERE alert_id = " & alert_id)
			for each  comp_id in computersArray
			    if(IsNumeric(comp_id)) then
			     Conn.Execute("INSERT INTO alerts_sent_comp VALUES ("& alert_id &","& comp_id &")")
			    end if  
			next
			
			Conn.Execute("UPDATE alerts SET type = 'S', sent_date=GETDATE(), type2='R', group_type='"&group_ad_type&"', sender_id="&uid&" WHERE campaign_id=" & Request("campaign_id") )
	end if
	next
			
	Response.Redirect Request("return_page")				    
else	
	for each alert_id in alert_ids
	
	Conn.Execute("UPDATE alerts SET schedule_type = '" & schedule_type & "' WHERE id = "& alert_id)	
	if instant_message = 1 then
		Conn.Execute("INSERT INTO instant_messages (alert_id) VALUES ("& alert_id &")")
		if(main_alert_id<>"") then
		Conn.Execute("delete from instant_messages where alert_id="&main_alert_id)
		Conn.Execute("delete from alerts where id="&main_alert_id)
		end if
	end if

	canSendPush = "0"

	sms_err=0
	sendSQL="SET NOCOUNT ON " & vbCrLf &_
		" IF OBJECT_ID('tempdb..#tempOUs') IS NOT NULL DROP TABLE #tempOUs" & vbCrLf &_
		" CREATE TABLE #tempOUs  (Id_child BIGINT NOT NULL);" & vbCrLf &_
		" IF OBJECT_ID('tempdb..#tempUsers') IS NOT NULL DROP TABLE #tempUsers" & vbCrLf &_
		" CREATE TABLE #tempUsers  (user_id BIGINT NOT NULL);" & vbCrLf &_
		" IF OBJECT_ID('tempdb..#tempComputers') IS NOT NULL DROP TABLE #tempComputers" & vbCrLf &_
		" CREATE TABLE #tempComputers  (comp_id BIGINT NOT NULL);" & vbCrLf &_
		" IF OBJECT_ID('tempdb..#tempGroups') IS NOT NULL DROP TABLE #tempGroups" & vbCrLf &_
		" CREATE TABLE #tempGroups  (group_id BIGINT NOT NULL);" & vbCrLf &_
		"DECLARE @now DATETIME " & vbCrLf &_
		"SET @now = GETDATE() "
		
	if(send_type="broadcast") then
		'if(gid=0) then
			s_type="B"
		'else
		'	s_type="R"
		'	to_users = 1
		'end if
	elseif(send_type="recepients") then
		s_type="R"
	elseif(send_type="iprange") then
		s_type="I"
	end if

	schedule = ""
	Set RS_schedule = Conn.Execute("SELECT schedule, from_date, to_date, sms, email, email_sender, desktop, text_to_call, recurrence FROM alerts WHERE id=" & alert_id)
	if (Not RS_schedule.EOF) then
		smsChecked = RS_schedule("sms")
		emailChecked = RS_schedule("email")
		senderName = RS_schedule("email_sender")
		desktopChecked = RS_schedule("desktop")
		textcallChecked = RS_schedule("text_to_call")
		if(Not IsNull(RS_schedule("schedule"))) then
			schedule = RS_schedule("schedule")
			if(schedule="1") then
				start_date = RS_schedule("from_date")
				end_date = RS_schedule("to_date")
				if start_date <> "" and end_date <> "" then
					if year(end_date) <> 1900 then
						if (MyMod(DateDiff("s", start_date, end_date),60)) = 1 then 'if lifetime - update time
							canSendPush = "1"
							Conn.Execute("UPDATE alerts SET schedule='1', schedule_type='"&schedule_type&"', from_date=GETDATE(), to_date=DATEADD(s,1,DATEADD(n,"&DateDiff("n", start_date, end_date)&",GETDATE())) WHERE id=" & alert_id)
						end if
					end if
				end if
			end if
		end if
		if(RS_schedule("recurrence")="1") then
			smsChecked = "0"
			emailChecked = "0"
			textcallChecked = "0"
		end if
	end if
		'change here
		'if object_type = personal
		'if object_type = group
		'if object_type = comp

	Set RS5 = Conn.Execute("SELECT alert_text, title, template_id, device FROM alerts WHERE id=" & alert_id)
		if (Not RS5.EOF) then
			if(Not IsNull(RS5("template_id"))) then
				Set RS1 = Conn.Execute("SELECT [top], bottom FROM templates WHERE id="&RS5("template_id"))
			else
				Set RS1 = Conn.Execute("SELECT [top], bottom FROM templates WHERE id=1")
			end if
			if (NOT RS1.EOF) then
				templateTop = RS1("top")
				templateBottom = RS1("bottom")
			end if
			alertText=RS5("alert_text")
			subject = RS5("title")
			devices = RS5("device")
			begin_pos = InStr(alertText,top_template_start_separator)
			if (begin_pos > 0) then
				end_pos = InStr(alertText,top_template_end_separator)
				templateTop = Mid(alertText, begin_pos+Len(top_template_start_separator), end_pos-begin_pos-Len(top_template_start_separator))
				alertText = Mid(alertText, end_pos+Len(top_template_end_separator))
			end if
			
			begin_pos = InStr(alertText,bottom_template_start_separator)
			if (begin_pos > 0) then
				end_pos = InStr(alertText,bottom_template_end_separator)
				templateBottom = Mid(alertText, begin_pos+Len(bottom_template_start_separator), end_pos-begin_pos-Len(bottom_template_start_separator))
				alertText = Left(alertText, begin_pos-1)
			end if

			alerts_coll = GetAlertLangsCollection(alertText)
			For Each alert in alerts_coll
				If(alert(2))Then
					alertText=alert(3)
					subject=alert(1)
					Exit For
				End If
			Next
		end if
	
	
		OUsSQL=""
		if(send_type="recepients") then
			usersSQL=""
			groupsSQL=""
			computersSQL=""
			objects=request("added_user")
			objArray = Split(objects, ", ")
			SET usersArray=jsArray()
			SET groupsArray=jsArray()
			SET computersArray=jsArray()

			for each obj_id in objArray
				if(obj_id<>"") then
					obj_type=Left(obj_id,1)
					if(obj_type="u") then
					'user
						user_id=replace(obj_id,"u","")
						usersArray.push(user_id)
					elseif(obj_type="g") then
					'group
						group_id=replace(obj_id,"g","")
						groupsArray.push(group_id)
					elseif(obj_type="c") then
					'computerÝ
						comp_id=replace(obj_id,"c","")
						computersArray.push(comp_id)
					end if
				end if
			next

			'OUs in tree
			if(gid=0) then
				ous = request("ous_id_sequence")
			else
				ous = replace(request("ed_ous"), "," , "")
			end if
			ous_closed = replace(request("ous_id_sequence_closed"), "," , "")
			ous_list=""
			if(ous_closed<>"") then
			'to selected OUs and all subOUs
				ous_closed = Trim(ous_closed)
				ous_arr=Split(ous_closed," ")
				ous_list=Join(ous_arr,",")
			
				if (gid=1) then
					OUsSQL= OUsSQL & vbCrLf &_
							"/*1 check rights of closed*/" & vbCrLf &_
							" ;WITH OUsList (Id_parent, Id_child)" & vbCrLf &_
							" AS (" & vbCrLf &_
							" 	SELECT Id AS Id_parent, Id as Id_child FROM OU WHERE id IN ("&ous_list&") /*closed ids*/" & vbCrLf &_
							"	UNION ALL" & vbCrLf &_
							"	SELECT h.Id_parent, l.Id_child" & vbCrLf &_
							"	FROM OU_hierarchy as h" & vbCrLf &_
							"	INNER JOIN OUsList as l" & vbCrLf &_
							"	ON h.Id_child = l.Id_parent" & vbCrLf &_
							" )" & vbCrLf &_
							" INSERT INTO #tempOUs (Id_child) (" & vbCrLf &_ 
							"	SELECT DISTINCT Id_child" & vbCrLf &_ 
							"	FROM OUsList l" & vbCrLf &_ 
							"	INNER JOIN policy_ou p" & vbCrLf &_ 
							"	ON p.ou_id=l.Id_parent AND "&policy_ids&") "
				else
					OUsSQL= OUsSQL & " INSERT INTO #tempOUs (Id_child) (" & vbCrLf &_ 
						"	SELECT id" & vbCrLf &_ 
						"	FROM OU" & vbCrLf &_ 
						"	WHERE id IN ("&ous_list&")" & vbCrLf &_ 
						") "
				end if
			end if
			' get OUs from selected groups
			if (groupsArray.length > 0) then
				groupsIds = groupsArray.join()
				OUsSQL = OUsSQL & vbCrLf &_
							"INSERT INTO #tempOUs (Id_child) (" & vbCrLf &_
							"	SELECT o.ou_id FROM ou_groups o" & vbCrLf &_
							"	LEFT JOIN #tempOUs as t" & vbCrLf &_
							"	ON o.ou_id = t.Id_child" & vbCrLf &_
							"	WHERE t.Id_child IS NULL AND o.group_id in ("&groupsIds&")" & vbCrLf &_
							") "
			end if
			' if there is closed OUs or OUs from groups
			if(OUsSQL<>"") then
				' add subOUs
				OUsSQL = OUsSQL & vbCrLf &_
						";WITH OUsList (Id_child)" & vbCrLf &_ 
						"AS (" & vbCrLf &_
						"	SELECT h.Id_child FROM OU_hierarchy h" & vbCrLf &_
						"	INNER JOIN #tempOUs t ON t.Id_child = h.Id_parent /*ids form 1*/" & vbCrLf &_
						"	UNION ALL" & vbCrLf &_
						"	SELECT c.Id_child FROM OU_hierarchy as c" & vbCrLf &_
						"	INNER JOIN OUsList as p" & vbCrLf &_
						"	ON c.Id_parent = p.Id_child" & vbCrLf &_
						")" & vbCrLf &_
						"INSERT INTO #tempOUs (Id_child)" & vbCrLf &_
						"(" & vbCrLf &_
						"	SELECT l.Id_child FROM OUsList l" & vbCrLf &_
						"	LEFT JOIN #tempOUs as t" & vbCrLf &_
						"	ON l.Id_child = t.Id_child" & vbCrLf &_
						"	WHERE t.Id_child IS NULL" & vbCrLf &_
						") "
			end if
			if(ous<>"") then
			'only to selected OUs
				ous = Trim(ous)
				ous_arr=split(ous)
				ous_list = Join(ous_arr,",")
				
				if (gid=1) then
					OUsSQL= OUsSQL & vbCrLf &_
							"INSERT INTO #tempOUs (Id_child)" & vbCrLf &_
							"(" & vbCrLf &_
							"	SELECT ou_id as Id_child FROM policy_ou p" & vbCrLf &_
							"	LEFT JOIN #tempOUs as t" & vbCrLf &_
							"	ON p.ou_id = t.Id_child" & vbCrLf &_
							"	WHERE t.Id_child IS NULL AND ou_id IN ("&ous_list&") AND "&policy_ids&" /*ids of not closed*/" & vbCrLf &_
							") "
				else 
					OUsSQL= OUsSQL & vbCrLf &_
							"INSERT INTO #tempOUs (Id_child)" & vbCrLf &_
							"(" & vbCrLf &_
							"	SELECT id FROM OU o" & vbCrLf &_
							"	LEFT JOIN #tempOUs as t" & vbCrLf &_
							"	ON o.id = t.Id_child" & vbCrLf &_
							"	WHERE t.Id_child IS NULL" & vbCrLf &_
							"	AND o.id IN ("&ous_list&")" & vbCrLf &_
							") "
				end if
			end if

			groupsSQL = "INSERT INTO #tempGroups (group_id) (" & vbCrLf &_
						"	SELECT Id_Group as group_id" & vbCrLf &_
						"	FROM OU_Group g" & vbCrLf &_
						"	INNER JOIN #tempOUs t ON t.Id_child=g.Id_OU" & vbCrLf &_
						") "
			if (groupsArray.length > 0) then
				groupsIds = groupsArray.join()
				groupsSQL = groupsSQL & vbCrLf &_
							"INSERT INTO #tempGroups (group_id) (" & vbCrLf &_
							"	SELECT g.id FROM groups g" & vbCrLf &_
							"	LEFT JOIN #tempGroups as t" & vbCrLf &_
							"	ON g.id = t.group_id" & vbCrLf &_
							"	WHERE t.group_id IS NULL AND id in ("&groupsIds&")" & vbCrLf &_
							") "
			end if

			groupsSQL = groupsSQL & vbCrLf &_
					" IF OBJECT_ID('tempdb..#tmp1') IS NOT NULL DROP TABLE #tmp1" & vbCrLf &_
					" CREATE TABLE #tmp1  (group_id BIGINT NOT NULL);" & vbCrLf &_
					" IF OBJECT_ID('tempdb..#tmp2') IS NOT NULL DROP TABLE #tmp2" & vbCrLf &_
					" CREATE TABLE #tmp2  (group_id BIGINT NOT NULL);" & vbCrLf &_
					" INSERT INTO #tmp1 (group_id)(SELECT group_id FROM #tempGroups)" & vbCrLf &_
					" WHILE 1=1" & vbCrLf &_
					" BEGIN" & vbCrLf &_
					" 	INSERT INTO #tmp2 (group_id)" & vbCrLf &_
					" 	(" & vbCrLf &_
					" 			SELECT DISTINCT g.group_id FROM groups_groups as g" & vbCrLf &_
					" 			INNER JOIN #tmp1 as t1" & vbCrLf &_
					" 			ON g.container_group_id = t1.group_id" & vbCrLf &_
					" 			LEFT JOIN #tempGroups as t2" & vbCrLf &_
					" 			ON g.group_id = t2.group_id" & vbCrLf &_
					" 			WHERE t2.group_id IS NULL" & vbCrLf &_
					" 	)" & vbCrLf &_
					" 	IF @@ROWCOUNT = 0 BREAK;" & vbCrLf &_
					" 	DELETE #tmp1" & vbCrLf &_
					" 	INSERT INTO #tmp1 (group_id)(SELECT group_id FROM #tmp2)" & vbCrLf &_
					" 	INSERT INTO #tempGroups (group_id)(SELECT group_id FROM #tmp2)" & vbCrLf &_
					" 	DELETE #tmp2" & vbCrLf &_
					" END; " & vbCrLf &_
					" DROP TABLE #tmp1" & vbCrLf &_
					" DROP TABLE #tmp2 "

			usersSQL = 	"INSERT INTO #tempUsers (user_id)(" & vbCrLf &_
						"	SELECT DISTINCT user_id" & vbCrLf &_
						"	FROM users_groups g" & vbCrLf &_
						"	INNER JOIN #tempGroups t" & vbCrLf &_
						"	ON t.group_id = g.group_id" & vbCrLf &_
						") "
			usersSQL = usersSQL & vbCrLf &_
						"INSERT INTO #tempUsers (user_id)(" & vbCrLf &_
						"	SELECT u.Id_user" & vbCrLf &_
						"	FROM OU_User u" & vbCrLf &_
						"	INNER JOIN #tempOUs o" & vbCrLf &_
						"	ON o.Id_child = u.Id_OU" & vbCrLf &_
						"	LEFT JOIN #tempUsers t" & vbCrLf &_
						"	ON t.user_id = u.Id_user" & vbCrLf &_
						"	WHERE t.user_id IS NULL" & vbCrLf &_
						") "

			if (usersArray.length > 0) then	
				usersIds = usersArray.join()
				usersSQL = usersSQL & vbCrLf &_
							"INSERT INTO #tempUsers (user_id)(" & vbCrLf &_
							"	SELECT u.id" & vbCrLf &_ 
							"	FROM users u" & vbCrLf &_
							"	LEFT JOIN #tempUsers t" & vbCrLf &_
							"	ON t.user_id = u.id" & vbCrLf &_
							"	WHERE t.user_id IS NULL" & vbCrLf &_ 
							"	AND u.id in ("&usersIds&")" & vbCrLf &_
							") "
			end if
			
			computersSQL = 	"INSERT INTO #tempComputers (comp_id)(" & vbCrLf &_
							"	SELECT DISTINCT g.comp_id" & vbCrLf &_
							"	FROM computers_groups g" & vbCrLf &_
							"	INNER JOIN #tempGroups t" & vbCrLf &_
							"	ON t.group_id = g.group_id" & vbCrLf &_
							") "
			computersSQL = computersSQL & vbCrLf &_
						"INSERT INTO #tempComputers (comp_id)(" & vbCrLf &_
						"	SELECT c.Id_comp" & vbCrLf &_
						"	FROM OU_comp c" & vbCrLf &_
						"	INNER JOIN #tempOUs o" & vbCrLf &_
						"	ON o.Id_child = c.Id_OU" & vbCrLf &_
						"	LEFT JOIN #tempComputers t" & vbCrLf &_
						"	ON t.comp_id = c.Id_comp" & vbCrLf &_
						"	WHERE t.comp_id IS NULL" & vbCrLf &_
						") "
			
			if (computersArray.length > 0) then
				computersIds = computersArray.join()
				computersSQL = computersSQL & vbCrLf &_
							"INSERT INTO #tempComputers (comp_id)(" & vbCrLf &_
							"	SELECT c.id" & vbCrLf &_
							"	FROM computers c" & vbCrLf &_
							"	LEFT JOIN #tempComputers t" & vbCrLf &_
							"	ON t.comp_id = c.id" & vbCrLf &_
							"	WHERE t.comp_id IS NULL" & vbCrLf &_ 
							"	AND c.id in ("&computersIds&")" & vbCrLf &_
							") "
			end if
		  end if

		sendSQL = sendSQL & OUsSQL & groupsSQL & usersSQL & computersSQL

		sendSQL = sendSQL & vbCrLf &_
				"INSERT INTO alerts_sent_deskbar (alert_id, desk_id)" & vbCrLf &_
				"(" & vbCrLf &_
				"	SELECT "&alert_id&", d.id FROM users_deskbar d INNER JOIN #tempUsers t" & vbCrLf &_
				"	ON d.user_id = t.user_id" & vbCrLf &_
				") "
	
		sendSQL = sendSQL & vbCrLf &_
				"INSERT INTO alerts_sent (alert_id, user_id)" & vbCrLf &_
				"(" & vbCrLf &_
				"	SELECT "&alert_id&", user_id FROM #tempUsers" & vbCrLf &_
				") "

		sendSQL = sendSQL & vbCrLf &_
				"INSERT INTO alerts_sent_stat (alert_id, user_id, date)" & vbCrLf &_
				"(" & vbCrLf &_
				"	SELECT "&alert_id&", user_id, @now  FROM #tempUsers" & vbCrLf &_
				") "

		sendSQL = sendSQL & vbCrLf &_
				"INSERT INTO alerts_sent_group (alert_id, group_id)" & vbCrLf &_
				"(" & vbCrLf &_
				"	SELECT "&alert_id&", group_id FROM #tempGroups" & vbCrLf &_
				") " & vbCrLf &_
				"INSERT INTO alerts_group_stat (alert_id, group_id, date)" & vbCrLf &_
				"(" & vbCrLf &_
				"	SELECT "&alert_id&", group_id, @now  FROM #tempGroups" & vbCrLf &_
				") "

		sendSQL = sendSQL & vbCrLf &_
				"INSERT INTO alerts_sent_comp (alert_id, comp_id)" & vbCrLf &_
				"(" & vbCrLf &_
				"	SELECT "&alert_id&", comp_id FROM #tempComputers" & vbCrLf &_
				") " & vbCrLf &_
				"INSERT INTO alerts_comp_stat (alert_id, comp_id, date)" & vbCrLf &_
				"(" & vbCrLf &_
				"	SELECT "&alert_id&", comp_id, @now  FROM #tempComputers" & vbCrLf &_
				") "

		sendSQL = sendSQL & vbCrLf &_
				"INSERT INTO alerts_sent_ou (alert_id, ou_id)" & vbCrLf &_
				"(" & vbCrLf &_
				"	SELECT "&alert_id&", Id_child FROM #tempOUs" & vbCrLf &_
				") " & vbCrLf &_
				"INSERT INTO alerts_ou_stat (alert_id, ou_id, date)" & vbCrLf &_
				"(" & vbCrLf &_
				"	SELECT "&alert_id&", Id_child, @now  FROM #tempOUs" & vbCrLf &_
				") "

		smsEmailSQL=""

		if ((smsChecked="1" OR emailChecked="1" OR text_to_call="1")  AND schedule<>"1") then
			smsEmailSQL = "SELECT u.id, u.name, u.mobile_phone, u.email FROM #tempUsers AS t INNER JOIN users u ON t.user_id=u.id "
		end if

		sendSQL = sendSQL & smsEmailSQL
		
		'if(Request("deviceType")=1) then
						
		'sendSQL= sendSQL & vbCrLf &_
		'" DROP TABLE #tempOUs" & vbCrLf &_
		'" DROP TABLE #tempUsers" & vbCrLf &_
		'" DROP TABLE #tempComputers" & vbCrLf &_ 
		'" DROP TABLE #tempGroups"
		
		'else

		sendSQL = sendSQL & vbCrLf &_
					"SELECT i.device_id FROM #tempUsers AS t INNER JOIN user_instances i ON t.user_id=i.user_id WHERE i.device_type = 2" & vbCrLf &_
					"SELECT i.device_id FROM #tempUsers AS t INNER JOIN user_instances i ON t.user_id=i.user_id WHERE i.device_type = 3" & vbCrLf &_
					"SELECT i.device_id FROM #tempUsers AS t INNER JOIN user_instances i ON t.user_id=i.user_id WHERE i.device_type = 4" & vbCrLf &_
					" DROP TABLE #tempOUs" & vbCrLf &_
					" DROP TABLE #tempUsers" & vbCrLf &_
					" DROP TABLE #tempComputers" & vbCrLf &_ 
					" DROP TABLE #tempGroups"
					
		'			end if
		
   ' Response.Write(sendSQL)
    Response.Write "SELECT DISTINCT users.id, users.name, users.mobile_phone, users.email FROM users LEFT JOIN policy_user ON users.id=policy_user.user_id LEFT JOIN users_groups ON users.id=users_groups.user_id LEFT JOIN OU_User ON users.id=OU_User.Id_user WHERE role='U' AND ("&policy_ids&" "&group_ids&" "&ou_ids&")"
  '  Response.End
		set RSSend = Conn.Execute(sendSQL)
		
		'Response.Write(RSSend)
		
		if (smsEmailSQL <> "") then
			sendEmailAndSmsByRecordset RSSend, subject, alertText, templateTop, templateBottom, smsChecked, emailChecked, senderName, null, textcallChecked
			Set RSSend = RSSend.NextRecordset()
		end if
		
'send iprange alert

	if(send_type="iprange") then
	    objects=request("added_user")    
	    groups = Split(objects, ", ")
		sendIpRange groups, alert_id
	end if

'send broadcast alert

	if(send_type="broadcast") then
	
		if(gid=0) then
			if((smsChecked="1" OR emailChecked="1" OR textcallChecked="1")) then
				Set RS = Conn.Execute("SELECT id, name, mobile_phone, email FROM users WHERE role='U'")
				sendEmailAndSmsByRecordset RS, subject, alertText, templateTop, templateBottom, smsChecked, emailChecked, senderName, null, textcallChecked
				RS.Close
			end if
		else
			'editor broadcast - personal to users
			if schedule = "1" then
				smsChecked = "0"
				emailChecked = "0"
			end if
			Set RSeditor = Conn.Execute("SELECT DISTINCT users.id, users.name, users.mobile_phone, users.email FROM users LEFT JOIN policy_user ON users.id=policy_user.user_id LEFT JOIN users_groups ON users.id=users_groups.user_id LEFT JOIN OU_User ON users.id=OU_User.Id_user WHERE role='U' AND ("&policy_ids&" "&group_ids&" "&ou_ids&")")
			sendEmailAndSmsByRecordset RSeditor, subject, alertText, templateTop, templateBottom, smsChecked, emailChecked, senderName, alert_id, textcallChecked
		end if

       ' Conn.Execute("INSERT INTO alerts_sent ( SELECT " & alert_id & ", ")

	end if

	Set rsSendToBlog = Conn.Execute("SELECT alert_text, title, post_to_blog FROM alerts WHERE id="&alert_id)
	if not rsSendToBlog.EOF then
		if rsSendToBlog("post_to_blog") = "1" and schedule<>"1" then
			Set rsBlogs = Conn.Execute("SELECT * FROM blogs")
			while not rsBlogs.EOF
				Dim objASPToWP, arrReturnedData
				Set objASPToWP = New wpActions
				objASPToWP.Server = rsBlogs("url")
				objASPToWP.Username = rsBlogs("username")
				objASPToWP.Password = rsBlogs("password")
				objASPToWP.BlogID = rsBlogs("blogID")
				objASPToWP.PostTitle = rsSendToBlog("title")
				alert_text = rsSendToBlog("alert_text")
				'alert_text = "<div style='background-color:white;padding:20px'>"&alert_text&"</div>"
				objASPToWP.PostBody = alert_text
				objASPToWP.SaveAsDraft = rsBlogs("draft")
				objASPToWP.AllowComments = rsBlogs("comments")
				objASPToWP.PostAsAPage = rsBlogs("postAsPage")
				objASPToWP.Author = rsBlogs("author")
				arrReturnedData = objASPToWP.newPost()
				Set objASPToWP = Nothing
				rsBlogs.MoveNext
			wend
		end if
	end if
	
	Set rsSendToTwitter = Conn.Execute("SELECT alert_text, social_media FROM alerts WHERE id="&alert_id)
	if not rsSendToTwitter.EOF then
		if rsSendToTwitter("social_media") then
			Set rsTwitter = Conn.Execute("SELECT * FROM social_media WHERE type=1")
			while not rsTwitter.EOF
				Set objASPTwitter = New ASPTwitter
				alertText = rsSendToTwitter("alert_text")
				alertText = DeleteTags(alertText)
				alertText = Replace(alertText, vbNewline, " ")
				consumerKey = rsTwitter("field1")
				consumerSecret = rsTwitter("field2")
				accessKey = rsTwitter("field3")
				accessSecret = rsTwitter("field4")
				Call objASPTwitter.Configure(consumerKey, consumerSecret)
				Call objASPTwitter.ConfigureOAuth(accessKey, accessSecret)
				Set objTweets = objASPTwitter.UpdateStatus(alertText)
				if jsCheckProperty(objTweets, "errors") then
					set errors = jsProperty(objTweets, "errors")
					set first_error = jsProperty(errors, 0)
					if first_error.code <> 187 then ' Status is a duplicate.
						response.write "Can't send tweet. Please check Twitter settings.<br/><br/>"
						response.write "Error Number: " & first_error.code & "<br/>"
						response.write "Description: " & first_error.message & "<br/>"
						if first_error.code = 135 then
							response.write "Solution: Check the date and time on server<br/>"
						end if
						response.end
					end if
				end if
				rsTwitter.MoveNext
			wend
		end if
	end if

    if(request("shouldApprove") = "1") then
  	         SQL = "UPDATE alerts SET approve_status = 1 WHERE id = " & alert_id
  	         Conn.Execute(SQL)
  	elseif(request("rejectedEdit") = "1") then
  	         SQL = "UPDATE alerts SET approve_status = 0 WHERE id = " & alert_id
  	         Conn.Execute(SQL)
  	end if
  	
	Conn.Execute("UPDATE alerts SET type = 'S', sent_date=GETDATE(), type2='"&s_type&"', group_type='"&group_ad_type&"', sender_id="&uid&" WHERE id=" & alert_id)

    'MEMCACHE
	'Set cacheMgr = CreateObject("DeskAlerts.CacheManager")
   ' cacheMgr.Add clng(alert_id), now()
    MemCacheUpdate "Add", alert_id

	if (MOBILE_SEND=1 and devices>1 AND Request("instant")<>"1") then
	  if(send_type="broadcast")then
		 if(Request("deviceType")="2" or Request("deviceType")="3") then
 			set RSSend = Conn.Execute("SELECT device_id FROM user_instances WHERE device_type = 2")
			while not RSSend.EOF
				sDeviceId = RSSend("device_id")
				if sDeviceId <> "" AND ( Request("campaign_id") = ""  OR Request("campaign_id") = "-1" ) then
					sText = Replace(DeleteTags(alertText), "&amp;", "&")
					sText = Replace(sText, "%username%", user_name)
					sendAlertGCM subject, sText, sDeviceId
				end if
				RSSend.MoveNext
			wend
			
		
			set RSSend = Conn.Execute("SELECT device_id FROM user_instances WHERE device_type = 3")
			while not RSSend.EOF
				sDeviceId = RSSend("device_id")
				if sDeviceId <> "" AND ( Request("campaign_id") = ""  OR Request("campaign_id") = "-1" ) then
					sText = Replace(DeleteTags(alertText), "&amp;", "&")
					sText = Replace(sText, "%username%", user_name)
					sendAlertAPNS subject, sText, sDeviceId, alert_id
				end if
				RSSend.MoveNext
			wend
			
			set RSSend = Conn.Execute("SELECT device_id FROM user_instances WHERE device_type = 4")
			while not RSSend.EOF
				sDeviceId = RSSend("device_id")
				if sDeviceId <> "" AND ( Request("campaign_id") = ""  OR Request("campaign_id") = "-1" ) then
					sText = Replace(DeleteTags(alertText), "&amp;", "&")
					sText = Replace(sText, "%username%", user_name) 'wtf
					sendAlertWinPhone subject, sText, sDeviceId, alert_id
				end if
				RSSend.MoveNext
			wend
			
			end if
			
		elseif (send_type="recepients") then
			while not RSSend.EOF
				sDeviceId = RSSend("device_id")
				if sDeviceId <> ""  AND ( Request("campaign_id") = ""  OR Request("campaign_id") = "-1" ) then
					sText = Replace(DeleteTags(alertText), "&amp;", "&")
					sText = Replace(sText, "%username%", user_name)
					sendAlertGCM subject, sText, sDeviceId
				end if
				RSSend.MoveNext
			wend
			
			Set RSSend = RSSend.NextRecordset()
			
			while not RSSend.EOF
				sDeviceId = RSSend("device_id")
				if sDeviceId <> "" AND ( Request("campaign_id") = ""  OR Request("campaign_id") = "-1" ) then
					sendAlertAPNS subject, "", sDeviceId, alert_id
				end if
				RSSend.MoveNext
			wend
			
			Set RSSend = RSSend.NextRecordset()
			
			
			while not RSSend.EOF
				sDeviceId = RSSend("device_id")
				if sDeviceId <> ""  AND ( Request("campaign_id") = ""  OR Request("campaign_id") = "-1" ) then
					sText = Replace(DeleteTags(alertText), "&amp;", "&")
					sText = Replace(sText, "%username%", user_name) 'not sure what's that, copied from Droid
					sendAlertWinPhone subject, sText, sDeviceId, alert_id
				end if
				RSSend.MoveNext
			wend
		RSSend.Close
		end if
	end if
'Response.Write(sms_err)
%>
	    <script language="javascript">
	        window.parent.parent.document.getElementById('menu').src = window.parent.parent.document.getElementById('menu').src;
         </script>
<%
	if (Request("return_page")<>"") then
	
	Response.Redirect Request("return_page")
	end if
    Response.Redirect "PopupSent.aspx?sms=" & sms_err & "&is_sended=" & is_sended & "&is_scheduled=" & is_scheduled
dim fs,f

next
end if
else
  
  Response.Redirect "index.asp"

end if

%><!-- #include file="db_conn_close.asp" -->