﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CompDetails.aspx.cs" Inherits="DeskAlertsDotNet.Pages.CompDetails" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
        <link href="css/style9.css" rel="stylesheet" type="text/css">
        <link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <link href="css/jquery/jquery_timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css">
        <script language="javascript" type="text/javascript" src="functions.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/date.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery_timepicker/jquery-ui-timepicker-addon.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/jquery/datepicker_fix.js"></script>

</head>
<body>
    <form id="form1" runat="server">
<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	<tr>
	<td>
	<table width="100%" height="100%" border="1" bordercolor="#000000" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
		<tr>
		<td width=100% height=31 class="theme-colored-back"><a href="" class="header_title">Computer details</a></td>
		</tr>
		<tr>
		<td bgcolor="#ffffff" height="100%">
		<div style="margin:10px;"><br><br>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr><td> <b>Name: </b></td><td runat="server" id="nameCell"></td></tr>
            <tr><td> <b>Domain: </b></td><td runat="server" id="domainCell"></td></tr>
            <tr><td valign='top'> <b>Groups: </b></td><td runat="server" id="groupsCell"></td></tr>
            <tr><td valign='top'> <b>Last activity: </b></td><td runat="server" id="lastActivityCell"></td></tr>
            <tr runat="server" id="regDateRow">
                <td valign='top'> <b>Registration Date: </b></td><td runat="server" id="regDateCell"></td>
            </tr>
            <tr><td valign='top'> <b>Online: </b></td><td><img runat="server" id="statusImg" src="images/online.gif" /></td></tr>
    </form>
</body>
</html>
