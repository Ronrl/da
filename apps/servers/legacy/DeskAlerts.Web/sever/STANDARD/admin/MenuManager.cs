﻿using DeskAlerts.ApplicationCore.Entities;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Models.Permission;
using DeskAlertsDotNet.Parameters;
using User = DeskAlertsDotNet.Models.User;

namespace DeskAlertsDotNet
{
    using DeskAlerts.ApplicationCore.Enums;
    using Resources;

    public class MenuManager
    {
        private static MenuManager _instance;
        public static MenuManager Default
        {
            get
            {
                if (_instance == null)
                    return _instance = new MenuManager();

                return _instance;
            }
        }

        public string BuildParentMenuItemHtml(string locKey, string image)
        {
            return string.Format("<h3 class=\"multiple\"><table><tr><td  width=\"1%\"><span class=\"menu_item\"><img class=\"menu_icon\" src=\"{1}\" alt=\"{0}\" width=\"20\" height=\"20\" border=\"0\"></span></td><td ><span class=\"menu_text\"><span name=\"menu_item\" class=\"menu_item\">{0}</span></span></td></tr></table></h3>", resources.ResourceManager.GetString(locKey), image);
        }

        public string BuildSingleMenuItemHtml(string link, string locKey, string image, string name, User curUser, DBManager dbMgr)
        {
            var title = CheckForPostfix(resources.ResourceManager.GetString(locKey), name, curUser, dbMgr);
            link = CheckForParams(link, name, curUser);
            var menuItem = string.Format("<h3 class=\"single\"><table><tr><td ><a href=\"{0}\" class=\"menu_item\" target=\"work_area\"><img class=\"menu_icon\" src=\"{2}\" alt=\"{1}\" width=\"20\" height=\"20\" border=\"0\"></a></td><td ><a href=\"{0}\" class=\"menu_item\" target=\"work_area\"><span name=\"menu_item\" class=\"menu_text\">{1}</span></a></td></tr></table></h3>", link, title, image);
            return menuItem;
        }

        public int GetRejectedAlertsCount(DBManager dbMgr, User curUser)
        {
            var getRejectedAlertsCountQuery = $@"
                SELECT COUNT(1) as count 
                FROM alerts 
                WHERE approve_status = {(int)ApproveStatus.Rejected} 
                AND sender_id = {curUser.Id}";
            return (int)dbMgr.GetScalarByQuery(getRejectedAlertsCountQuery);
        }

        public int GetNotApprovedAlertsCount(DBManager dbMgr, User curUser)
        {
            var getNotApprovedAlertsQuery = $@"
                SELECT COUNT(1) as count 
                FROM alerts 
                WHERE approve_status = {(int)ApproveStatus.NotModerated} 
                    AND type != 'D' 
                    AND sender_id != {curUser.Id}";

            if (!curUser.IsAdmin)
            {
                bool userCanApproveAlerts = curUser.Policy[Policies.ALERTS].CanApprove;
                bool userCanApproveSurveys = curUser.Policy[Policies.SURVEYS].CanApprove;
                bool userCanApproveScreenSavers = curUser.Policy[Policies.SCREENSAVERS].CanApprove;
                bool userCanApproveWallpapers = curUser.Policy[Policies.WALLPAPERS].CanApprove;
                bool userCanApproveLockscreens = curUser.Policy[Policies.LOCKSCREENS].CanApprove;

                if (!userCanApproveAlerts)
                {
                    getNotApprovedAlertsQuery += $" AND class != {(int)AlertType.SimpleAlert} AND class != {(int)AlertType.Ticker} AND class != {(int)AlertType.Rsvp} AND class != {(int)AlertType.VideoAlert}";
                }

                if (!userCanApproveSurveys)
                {
                    getNotApprovedAlertsQuery += $" AND class != {(int)AlertType.SimpleSurvey} AND class != {(int)AlertType.SurveyPoll} AND class != {(int)AlertType.SurveyQuiz}";
                }

                if (!userCanApproveScreenSavers)
                {
                    getNotApprovedAlertsQuery += $" AND class != {(int)AlertType.ScreenSaver}";
                }

                if (!userCanApproveWallpapers)
                {
                    getNotApprovedAlertsQuery += $" AND class != {(int)AlertType.Wallpaper}";
                }

                if (!userCanApproveLockscreens)
                {
                    getNotApprovedAlertsQuery += $" AND class != {(int)AlertType.LockScreen}";
                }

            }

            return (int)dbMgr.GetScalarByQuery(getNotApprovedAlertsQuery);
        }
        protected string CheckForPostfix(string title, string name, User curUser, DBManager dbMgr)
        {
            switch (name)
            {
                case "approve.panel":
                    {
                        int count = GetNotApprovedAlertsCount(dbMgr, curUser);
                        if (count > 0)
                            return title + " <b style='color:red'>(" + count + ")</b>";
                        else
                            break;
                    }
                case "approve.reject":
                    {
                        int count = GetRejectedAlertsCount(dbMgr, curUser);
                        if (count > 0)
                            return title + " <b style='color:red'>(" + count + ")</b>";
                        else
                            break;
                    }
            }

            return title;
        }

        protected string CheckForParams(string link, string name, User curUser)
        {
            switch (name)
            {
                case "approve.panel":
                case "approve.reject":
                    {
                        return link.Replace("{uid}", curUser.Id.ToString());
                    }
            }
            return link;
        }

        public string BuildChildMenuItemHtml(string link, string loc_key, string parent_loc_key, string image, bool isLast, User curUser)
        {
            string treeImage = isLast ? "images/endline.gif" : "images/midline.gif";

            string htmlStr = @"<td background='{3}'><img height='18' src='{3}' width='18' border='0'></td>";

            if (image.Trim().Length > 0)
            {
                htmlStr += "<td><a href='{0}' target='work_area'><img class='menu_icon' src='{4}' alt='{1}' width='18' height='18' border='0'></a></td>";
            }

            htmlStr += @"<td colspan='3'><nobr>&nbsp;<a href='{0}' target='work_area' name='menu_subitem_{2}'>{1}</a></nobr></td>";

            string menuItem = string.Format(
                htmlStr,
                link,
                resources.ResourceManager.GetString(loc_key),
                resources.ResourceManager.GetString(parent_loc_key),
                treeImage,
                image);

            return menuItem;
        }




        public bool CanDisplayMenuItem(string menuItemName, User curUser)
        {
            menuItemName = menuItemName.ToLower();

            bool result;
            #region check condition
            switch (menuItemName)
            {
                case "dashboard":
                    {
                        result = curUser.HasPrivilege || curUser.Policy[Policies.ALERTS].CanSomething
                            || (Config.Data.HasModule(Modules.RSS) && curUser.Policy[Policies.RSS].CanSomething)
                            || (Config.Data.HasModule(Modules.SCREENSAVER) && curUser.Policy[Policies.SCREENSAVERS].CanSomething)
                            || (Config.Data.HasModule(Modules.WALLPAPER) && curUser.Policy[Policies.WALLPAPERS].CanSomething)
                            || (Config.Data.HasModule(Modules.LOCKSCREEN) && curUser.Policy[Policies.LOCKSCREENS].CanSomething)
                            || (Config.Data.HasModule(Modules.EMERGENCY) && curUser.Policy[Policies.IM].CanSomething)
                            || (Config.Data.HasModule(Modules.SURVEYS) && curUser.Policy[Policies.SURVEYS].CanSomething);

                        break;
                    }

                case "alerts":
                    {
                        result = curUser.HasPrivilege || curUser.Policy[Policies.ALERTS].CanSomething || curUser.Policy[Policies.EMAILS].CanSomething;
                        break;
                    }

                case "alerts.create.popup":
                    {
                        result = curUser.HasPrivilege || curUser.Policy[Policies.ALERTS].CanCreate;
                        break;
                    }
                case "alerts.create.ticker":
                    {
                        result = (Config.Data.HasModule(Modules.TICKER) || Config.Data.HasModule(Modules.TICKER_PRO)) && (curUser.HasPrivilege || curUser.Policy[Policies.ALERTS].CanCreate);
                        break;
                    }
                case "alerts.sent":
                    {
                        result = curUser.HasPrivilege || curUser.Policy[Policies.ALERTS].CanView || curUser.Policy[Policies.ALERTS].CanSend;
                        break;
                    }
                case "multiple_alerts.create":
                    {
                        result = Config.Data.HasModule(Modules.MULTIPLE);
                        break;
                    }
                case "alerts.draft":
                    {
                        result = curUser.HasPrivilege || curUser.Policy[Policies.ALERTS].CanView || curUser.Policy[Policies.ALERTS].CanSend;
                        break;
                    }
                case "alerts.templates":
                    {
                        result = curUser.HasPrivilege || curUser.Policy[Policies.TEXT_TEMPLATES].CanView;
                        break;
                    }
                case "digsign":
                    {
                        result = (Config.Data.HasModule(Modules.DIGITAL_SIGNAGE) && (curUser.HasPrivilege));
                        break;
                    }

                case "digsign.links":
                    {
                        result = (Config.Data.HasModule(Modules.DIGITAL_SIGNAGE) && curUser.HasPrivilege);
                        break;
                    }

                case "digsign.send":
                    {
                        result = (Config.Data.HasModule(Modules.DIGITAL_SIGNAGE) && (curUser.HasPrivilege));
                        break;
                    }
                case "surveys":
                    {
                        result = Config.Data.HasModule(Modules.SURVEYS) && (curUser.HasPrivilege || curUser.Policy[Policies.SURVEYS].CanSomething);
                        break;
                    }

                case "surveys.create":
                    {
                        result = Config.Data.HasModule(Modules.SURVEYS) && (curUser.HasPrivilege || curUser.Policy[Policies.SURVEYS].CanCreate);
                        break;
                    }
                case "surveys.active":
                    {
                        result = Config.Data.HasModule(Modules.SURVEYS) && (curUser.HasPrivilege || curUser.Policy[Policies.SURVEYS].CanView);
                        break;
                    }
                case "surveys.archived":
                    {
                        result = Config.Data.HasModule(Modules.SURVEYS) && (curUser.HasPrivilege || curUser.Policy[Policies.SURVEYS].CanView);
                        break;
                    }
                case "campaigns":
                    {
                        result = Config.Data.HasModule(Modules.CAMPAIGN) && (curUser.HasPrivilege || curUser.Policy[Policies.CAMPAIGN].CanSomething);
                        break;
                    }
                case "campaigns.create":
                    {
                        result = Config.Data.HasModule(Modules.CAMPAIGN) && (curUser.HasPrivilege || curUser.Policy[Policies.CAMPAIGN].CanCreate);
                        break;
                    }
                case "campaigns.current":
                case "campaigns.draft":
                    {
                        result = Config.Data.HasModule(Modules.CAMPAIGN) && (curUser.HasPrivilege || curUser.Policy[Policies.CAMPAIGN].CanView);
                        break;
                    }
                case "rss":
                    {
                        result = Config.Data.HasModule(Modules.RSS) && (curUser.HasPrivilege || curUser.Policy[Policies.RSS].CanSomething);
                        break;
                    }
                case "rss.create":
                    {
                        result = Config.Data.HasModule(Modules.RSS) && (curUser.HasPrivilege || curUser.Policy[Policies.RSS].CanCreate);
                        break;
                    }
                case "rss.current":
                case "rss.draft":
                    {
                        result = Config.Data.HasModule(Modules.RSS) && (curUser.HasPrivilege || curUser.Policy[Policies.RSS].CanView);
                        break;
                    }
                case "ss":
                    {
                        result = Config.Data.HasModule(Modules.SCREENSAVER) && (curUser.HasPrivilege || curUser.Policy[Policies.SCREENSAVERS].CanSomething);
                        break;
                    }
                case "ss.create":
                    {
                        result = Config.Data.HasModule(Modules.SCREENSAVER) && (curUser.HasPrivilege || curUser.Policy[Policies.SCREENSAVERS].CanCreate);
                        break;
                    }
                case "ss.current":
                case "ss.draft":
                    {
                        result = Config.Data.HasModule(Modules.SCREENSAVER) && (curUser.HasPrivilege || curUser.Policy[Policies.SCREENSAVERS].CanView);
                        break;
                    }

                case "wp":
                    {
                        result = Config.Data.HasModule(Modules.WALLPAPER) && (curUser.HasPrivilege || curUser.Policy[Policies.WALLPAPERS].CanSomething);
                        break;
                    }
                case "wp.create":
                    {
                        result = Config.Data.HasModule(Modules.WALLPAPER) && (curUser.HasPrivilege || curUser.Policy[Policies.WALLPAPERS].CanCreate);
                        break;
                    }
                case "wp.current":
                case "wp.draft":
                    {
                        result = Config.Data.HasModule(Modules.WALLPAPER) && (curUser.HasPrivilege || curUser.Policy[Policies.WALLPAPERS].CanView);
                        break;
                    }

                case "ls":
                    {
                        result = Config.Data.HasModule(Modules.LOCKSCREEN) && (curUser.HasPrivilege || curUser.Policy[Policies.LOCKSCREENS].CanSomething);
                        break;
                    }
                case "ls.create":
                    {
                        result = Config.Data.HasModule(Modules.LOCKSCREEN) && (curUser.HasPrivilege || curUser.Policy[Policies.LOCKSCREENS].CanCreate);
                        break;
                    }
                case "ls.current":
                case "ls.draft":
                    {
                        result = Config.Data.HasModule(Modules.LOCKSCREEN) && (curUser.HasPrivilege || curUser.Policy[Policies.LOCKSCREENS].CanView);
                        break;
                    }

                case "im":
                    {
                        result = Config.Data.HasModule(Modules.EMERGENCY) && (curUser.HasPrivilege || curUser.Policy[Policies.IM].CanSomething || curUser.Policy[Policies.CC].CanSomething);
                        break;
                    }
                case "im.create":
                    {
                        result = Config.Data.HasModule(Modules.EMERGENCY) && (curUser.HasPrivilege || curUser.Policy[Policies.IM].CanCreate);
                        break;
                    }

                case "im.send":
                    {
                        result = Config.Data.HasModule(Modules.EMERGENCY) && (curUser.HasPrivilege || curUser.Policy[Policies.IM].CanView);
                        break;
                    }
                case "im.colorcodes":
                    {
                        result = Config.Data.HasModule(Modules.EMERGENCY) && (curUser.HasPrivilege || curUser.Policy[Policies.CC].CanView);
                        break;
                    }
                case "im.shortcuts":
                    {
                        result = Config.Data.HasModule(Modules.EMERGENCY) && curUser.HasPrivilege;
                        break;
                    }
                case "ad":
                case "ad.domains":
                case "ad.sync":
                case "ad.orgs":
                    {
                        result = Config.Data.HasModule(Modules.AD) && curUser.HasPrivilege;
                        break;
                    }

                case "users.groups":
                case "users":
                case "groups":
                case "ip_groups":
                    {
                        result = !Config.Data.HasModule(Modules.AD) && curUser.HasPrivilege;
                        break;
                    }
                case "pub.groups_policy":
                    {
                        result = false;
                        break;
                    }
                case "pubs":
                case "pubs.list":
                case "pubs.policies":
                    {
                        result = curUser.HasPrivilege;
                        break;

                    }
                case "ad.ip_groups":
                    {
                        result = Config.Data.HasModule(Modules.AD) && (curUser.HasPrivilege || curUser.Policy[Policies.IP_GROUPS].CanSomething);
                        break;
                    }

                case "feedback":
                    {
                        result = curUser.HasPrivilege || curUser.Policy[Policies.FEEDBACK].CanSomething;
                        break;
                    }
                case "ext_stat":
                    {
                        result = Config.Data.HasModule(Modules.STATISTICS) && (curUser.HasPrivilege || curUser.Policy[Policies.STATISTICS].CanSomething);
                        break;
                    }
                case "stat":
                    {
                        result = !Config.Data.HasModule(Modules.STATISTICS) && (curUser.HasPrivilege || curUser.Policy[Policies.STATISTICS].CanSomething);
                        break;
                    }
                case "settings.social":
                    {
                        result = (Config.Data.HasModule(Modules.BLOG) || Config.Data.HasModule(Modules.LINKEDIN) || Config.Data.HasModule(Modules.TWITTER)) && curUser.IsAdmin;
                        break;
                    }
                case "settings.common":
                    {
                        result = curUser.IsAdmin;
                        break;
                    }

                case "approve.panel":
                    {
                        bool userCanApproveContent = CurrentUserCanApproveContent(curUser);
                        result = Config.Data.HasModule(Modules.APPROVE) && Settings.Content["ConfAllowApprove"].Equals("1") && (curUser.HasPrivilege || userCanApproveContent);
                        break;
                    }

                case "approve.reject":
                    {
                        result = Config.Data.HasModule(Modules.APPROVE) && Settings.Content["ConfAllowApprove"].Equals("1") && !curUser.HasPrivilege;
                        break;
                    }

                //EGS custom
                case "alerts.tgroups":
                    {
                        result = Config.Data.IsOptionEnabled("ConfTGroupEnabled") && curUser.HasPrivilege;
                        break;
                    }

                case "video":
                    {
                        result = Config.Data.HasModule(Modules.VIDEO) &&
                                 (curUser.HasPrivilege || curUser.Policy[Policies.ALERTS].CanSomething);
                        break;

                    }
                case "video.create":
                    {
                        result = Config.Data.HasModule(Modules.VIDEO) &&
                                 (curUser.HasPrivilege || curUser.Policy[Policies.ALERTS].CanCreate);
                        break;
                    }
                case "video.current":
                    {
                        result = Config.Data.HasModule(Modules.VIDEO) &&
                                 (curUser.HasPrivilege || curUser.Policy[Policies.ALERTS].CanSend ||
                                  curUser.Policy[Policies.ALERTS].CanView);
                        break;
                    }
                case "video.draft":
                    {
                        result = Config.Data.HasModule(Modules.VIDEO) &&
                                 (curUser.HasPrivilege || curUser.Policy[Policies.ALERTS].CanSend ||
                                  curUser.Policy[Policies.ALERTS].CanView);
                        break;
                    }

                case "ad.orgs.nonadmin":
                    {
                        result = Config.Data.HasModule(Modules.AD) && !curUser.HasPrivilege &&
                                 (curUser.Policy[Policies.USERS].CanSomething ||
                                  curUser.Policy[Policies.GROUPS].CanSomething);

                        break;
                    }
                default:
                    {
                        result = true;
                        break;
                    }



            }
            #endregion
            return result;
        }

        private bool CurrentUserCanApproveContent(User curUser)
        {
            if (curUser.IsAdmin)
            {
                return true;
            }
            else
            {
                bool userCanApproveAlerts = curUser.Policy[Policies.ALERTS].CanApprove;
                bool userCanApproveSurveys = curUser.Policy[Policies.SURVEYS].CanApprove;
                bool userCanApproveScreenSavers = curUser.Policy[Policies.SCREENSAVERS].CanApprove;
                bool userCanApproveWallpapers = curUser.Policy[Policies.WALLPAPERS].CanApprove;
                bool userCanApproveContent = userCanApproveAlerts || userCanApproveSurveys
                                                                  || userCanApproveScreenSavers
                                                                  || userCanApproveWallpapers;

                return userCanApproveContent;
            }
        }

    }
}