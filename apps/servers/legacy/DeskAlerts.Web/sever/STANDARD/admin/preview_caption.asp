<!-- #include file="config.inc" -->
<%
Response.Expires = 0 

alertSource = HtmlDecode(Request("caption_html"))
skinId = Request("skin_id")
basePath = alerts_folder&"/admin/skins/default/version/"

if (skinId <> "") then
	basePath = alerts_folder&"/admin/skins/"&skinId&"/version/"
end if

if (alertSource="") then
	Set fs=Server.CreateObject("Scripting.FileSystemObject")
	
	
	caption = Request("caption_href")
	'Response.Write "CAPTION = " & caption
	
	if(caption = "" or IsNull(caption)) then
	    caption = ".\alertcaption.html"
	end if
	
	    captionPath = "skins/default/version/"&caption

	if (skinId <> "") then
		captionPath = "skins/"&skinId&"/version/"&caption
	end if
	
   ' Response.Write captionPath
	'Response.Write Server.MapPath(captionPath)
	Set f=fs.OpenTextFile(Server.MapPath(captionPath), 1)
	alertSource = f.ReadAll
	f.Close
	Set f=Nothing
	Set fs=Nothing
end if

Set rHead = New RegExp
With rHead
	.Pattern = "<head[^>]*>"
	.IgnoreCase = True
	.Global = False
End With
alertSource = rHead.Replace(alertSource, "$&"& vbNewline & _
	"<!--server_preview_section_start-->"& vbNewline & _
	"<link href='css/jquery/jquery-ui.css' rel='stylesheet' type='text/css'/>"& vbNewline & _
	"<script type='text/javascript' language='JavaScript' src='jscripts/jquery/jquery.min.js'></script>"& vbNewline & _
	"<script type='text/javascript' language='JavaScript' src='jscripts/jquery/jquery-ui.min.js'></script>"& vbNewline & _
	"<script type='text/javascript' language='JavaScript'>"& vbNewline & _
	"	var ext_props=$.parseJSON($('<div/>').html("& vbNewline & _
	"	"""& jsEncode(Request("ext_props")) &""""& vbNewline & _
	"		.replace(/&/g, '&amp;').replace(/""/g, '&quot;').replace(/'/g, '&#39;').replace(/</g, '&lt;').replace(/>/g, '&gt;')"& vbNewline & _
	"	).text());"& vbNewline & _
	"</script>"& vbNewline & _
	"<script type='text/javascript' language='JavaScript' src='external.asp'></script>"& vbNewline & _
	"<base href="""&basePath&"""/>" & vbNewline & _
	"<!--server_preview_section_end-->"& vbNewline)
alertSource = Replace(alertSource,".parentElement",".parentNode",1,-1,1)

Response.Write alertSource

%>

