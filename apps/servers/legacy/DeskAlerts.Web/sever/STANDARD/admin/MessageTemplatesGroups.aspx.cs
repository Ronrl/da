﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Net;
using System.IO;

namespace deskalerts
{

    public partial class MessageTemplatesGroups : System.Web.UI.Page
    {
        public int offset;
        public int limit;
        public string sortBy;
        public int count;
       public int Offset { get { return offset; } }
       public int Limit { get { return limit; } }
       public string SortBy { get { return sortBy; } }
       public int Count { get { return count; } }
       public Dictionary<string, string> vars = new Dictionary<string,string>();
          public string order;

          public bool noRows;


       public  string page ;
       public string name;
       string orderCollumn;
       public string Name { get { return name; } }


       protected void Page_PreInit(object sender, EventArgs e)
       {
           
           order = !string.IsNullOrEmpty(Request.Params["order"]) ? Request.Params["order"] : "DESC";
           orderCollumn = !string.IsNullOrEmpty(Request.Params["collumn"]) ? Request.Params["collumn"] : "id";
          offset = !string.IsNullOrEmpty(Request.Params["offset"]) ? Convert.ToInt32(Request.Params["offset"]) : 0;
          limit = !string.IsNullOrEmpty(Request.Params["limit"]) ? Convert.ToInt32(Request.Params["limit"]) : 25;
          sortBy = !string.IsNullOrEmpty(Request.Params["sortby"]) ? Request.Params["sortby"] : "date DESC";
          
          page = "MessageTemplatesGroups.aspx?";
           
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            vars = GetAspVariables("LNG_TGROUPS_TITLE", "LNG_TGROUPS_DESCRIPTION", "LNG_TGROUPS_ADD", "LNG_ACTIONS", "LNG_DELETE", "LNG_TGROUPS_NOROWS");
            name = tgroupsLabel.Text = vars["LNG_TGROUPS_TITLE"]; //GetAspVariable("LNG_TGROUP_TITLE");
            tgroupsDescription.Text = vars["LNG_TGROUPS_DESCRIPTION"];
            tgoupAddLabel.Text = vars["LNG_TGROUPS_ADD"];
            tgroupActions.Text = vars["LNG_ACTIONS"];
           deleteLabelButtom.Text =  deleteLabel.Text =  vars["LNG_DELETE"];

         //   Response.Write("<br><A href='#' class='delete_button' onclick=\"javascript: return LINKCLICK('Campaigns');\">"+ GetAspVariable("LNG_DELETE") +"</a><br/><br/>");
            ParseAndFillContentTable();
          
           
        }

        public Dictionary<string, string> GetAspVariables(params string[] keys)
        {
            string queryString = String.Join(":", keys);

            string url = HttpContext.Current.Request.Url.AbsoluteUri;
            string path = url.Substring(0, url.IndexOf("MessageTemplatesGroups"));
            string url_bridge = path + "/dotnetbridge.asp?var=" + queryString;
            WebRequest request = WebRequest.Create(url_bridge);
            WebResponse response = request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
           // Response.Write(reader.ReadToEnd());
            string[] variablesStrings = reader.ReadToEnd().Split('&');
            

            Dictionary<string, string> result = new Dictionary<string, string>();
            foreach (string keyVal in variablesStrings)
            {

                string[] keyValArray = keyVal.Split('|');

                if (keyValArray.Length != 2)
                    continue;

                string key = keyValArray[0];
                string value = keyValArray[1];

                result.Add(key, value);
            }

            return result;

            
        }
        public string GetAspVariable(string variableName)
        {
            string url = HttpContext.Current.Request.Url.AbsoluteUri;
            string path = url.Substring(0, url.IndexOf("MessageTemplatesGroups"));
            string url_bridge = path + "/dotnetbridge.asp?var="+variableName;
            WebRequest request = WebRequest.Create(url_bridge);
            WebResponse response = request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            string variable = reader.ReadToEnd();
            return variable;
        }

        public string GetCampaignsFromDB()
        {
            string url = HttpContext.Current.Request.Url.AbsoluteUri;
            string path = url.Substring(0, url.IndexOf("MessageTemplatesGroups"));
            string url_bridge = path + "/set_settings.asp?name=GetTGroups&value=" + order + "&collumn=" + orderCollumn;
            WebRequest request = WebRequest.Create(url_bridge);
            WebResponse response = request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());

            return reader.ReadToEnd();

        }

        public string MakePages()
        {
            string url = HttpContext.Current.Request.Url.AbsoluteUri;
            string path = url.Substring(0, url.IndexOf("MessageTemplatesGroups"));
            string url_bridge = path + "/dotnetbridge.asp?action=makepage&offset="+Offset+"&limit="+Limit+"&count="+Count+"&page="+page+"&name="+Name+"&sortby="+SortBy;
            WebRequest request = WebRequest.Create(url_bridge);
            WebResponse response = request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            string variable = reader.ReadToEnd();
            return variable;

        }
        public void ParseAndFillContentTable()
        {
            string data = GetCampaignsFromDB();
            
            string[] rows = data.Split('\n');

            noRows = data == string.Empty;
            foreach (string row in rows)
            {

                if (row == string.Empty)
                    return;

                    string[] fields = row.Split('\r');

                   if (fields.Length == 2)
                    {
                        HtmlTableRow htmlRow = FormatTableRow(fields[0], fields[1]);
                        contentTable.Rows.Add(htmlRow);
                        count++;
                    }
 
            }
        }

        public HtmlTableRow FormatTableRow(string id, string name)
        {

            HtmlTableRow row = new HtmlTableRow();
            row.Attributes.Add("id", "row" + id);
            HtmlTableCell checkCell = new HtmlTableCell();
            checkCell.Align = "center";
            checkCell.InnerHtml = "<input type=\"checkbox\" name=\"objects\" value=\"" + id + "\">";
            row.Cells.Add(checkCell);
            HtmlTableCell nameCell = new HtmlTableCell();
            nameCell.InnerHtml = name;
            row.Cells.Add(nameCell);

            string encodedName = Server.UrlEncode(name);
            HtmlTableCell actionCell = new HtmlTableCell();
            actionCell.Align = "center";
            string innerHtml = "<a href=\"AddMessageTemplatesGroups.aspx?edit=1&tgid=" + id + "&tgname=" + encodedName + "\"><img src='images/action_icons/edit.png' ></a>&nbsp;";
            innerHtml += "<a href='AddMessageTemplatesGroups.aspx?edit=1&dupId=" + id + "&tgname=" + encodedName + "'><img src='images/action_icons/duplicate.png' ></a> ";
            innerHtml += "<a href='javascript:showTemplates(" + id + ")'><img src='images/action_icons/preview.png' ></a>";
            actionCell.InnerHtml = innerHtml;
            row.Cells.Add(actionCell);
           

            return row;
        }




    }
}
