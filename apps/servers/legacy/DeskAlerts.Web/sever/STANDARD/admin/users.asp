﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%

check_session()

%>
<!-- #include file="ad.inc" -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>DeskAlerts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/style9.css" rel="stylesheet" type="text/css">
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
</head>
<script language="javascript" type="text/javascript" src="functions.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script language="javascript" type="text/javascript">

$(document).ready(function() {
	var width = $(".data_table").width();
	var pwidth = $(".paginate").width();
	if (width != pwidth) {
		$(".paginate").width("100%");
	}
	$("#add_user_button").button();
	$("#add_user_button").bind("click", function() {
			
	});
	
	$(".search_button").button({
		icons : {
			primary : "ui-icon-search"
		}
	});
	
	$(".delete_button").button();
});


</script>


<%

  uid = Session("uid")
  flag=1
  if(flag <> 1 AND AD=3) then
%>
<body style="margin:0px" class="body">

<script type="text/javascript">

// code to create and make AJAX request
function getFile(pURL) {
    if (window.XMLHttpRequest) { // code for Mozilla, Safari, etc 
        xmlhttp=new XMLHttpRequest();
    } else if (window.ActiveXObject) { //IE 
        xmlhttp=new ActiveXObject('Microsoft.XMLHTTP');
    }

    // if we have created the xmlhttp object we can send the request
    if (typeof(xmlhttp)=='object') {
        xmlhttp.onreadystatechange=xmlhttpResults;
        xmlhttp.open('GET', pURL, true);
        xmlhttp.send(null);
        // replace the submit button image with a please wait for the results image.
//        document.getElementById('theImage').innerHTML = '<img id="submitImage" name="submitImage" src="images/ajaxCallChangeImage_wait.gif" alt="Please Wait for AJAX Results">';
    // otherwise display an error message
    } else {
        alert('Your browser is not remote scripting enabled. You can not run this example.');
    }
}

// function to handle asynchronous call
function xmlhttpResults() {
    if (xmlhttp.readyState==4) { 
        if (xmlhttp.status==200) {
            // we use a delay only for this example
            setTimeout('loadResults()',3000);
        }
    }
}

function loadResults() {
    // put the results on the page
    if(xmlhttp.responseText=="1")
    {
	    location.href="users.asp"
    }
    else
    {
	    document.getElementById('mydiv').innerHTML = "<table width='100%' height='100%' border='0'><tr><td align='center'><b>Error: Synchronizing with Active Directory failed.</b></td></tr></table>";
    }
}


</script>
<div name="mydiv" id="mydiv">
<table width="100%" height="100%" border="0"><tr><td align="center">
<br><br>  <img src="images/ajax-loader.gif" border="0"> <br><br>  <b>Synchronizing with Active Directory. Please wait.</b>
</td>
</tr>
</table>
</div>

<%
  Session("flag") = 1
  else



%>

<body style="margin:0" class="body">

<%

	if(Request("offset") <> "") then 
		offset = clng(Request("offset"))
	else 
		offset = 0
	end if	

	if(Request("sortby") <> "") then 
		sortby = Request("sortby")
	else 
		sortby = "name"
	end if	


	myuname = request("uname")
	if(myuname <> "") then 
		searchSQL = " AND (users.name LIKE N'%"&Replace(myuname, "'", "''")&"%' OR users.display_name LIKE N'%"&Replace(myuname, "'", "''")&"%')"
	else 
		searchSQL = ""
	end if	

if (uid <>"") then

'counting pages to display
   if(Request("limit") <> "") then 
	limit = clng(Request("limit"))
   else 
	limit=50
   end if
  
'  limit=10
	linkclick_str = LNG_USERS
	users_arr = Array("checked","checked","checked","checked","checked","checked","checked")
	gid = 0
	gsendtoall = 0
	set RS = Conn.Execute("SELECT id, only_personal FROM users WHERE id="&uid&" AND role='E'")
	if not RS.EOF then
		gid = 1
		set rs_policy = Conn.Execute("SELECT p.id, p.type, pu.user_id FROM policy p INNER JOIN policy_editor pe ON p.id=pe.policy_id LEFT JOIN policy_user pu ON pu.policy_id = p.id AND pu.user_id = 0 WHERE pe.editor_id=" & uid)
		if not rs_policy.EOF then
			if rs_policy("type")="A" then 'can send to all
				gid = 0
			elseif not IsNull(rs_policy("user_id")) then
				gid = 0
				gsendtoall = 1
			end if
		end if
		if gid = 1 or gsendtoall = 1 then
			editor_id = RS("id")
			policy_ids = editorGetPolicyIds(editor_id)
			users_arr = editorGetPolicyList(policy_ids, "users_val")
			if users_arr(2) = "" then
				linkclick_str = "not"
			end if
			group_ids=""
			group_ids=editorGetGroupIds(policy_ids)
			group_ids=replace(group_ids, "group_id", "users_groups.group_id")
			if(group_ids <> "") then
				group_ids = "OR " & group_ids
			end if
			ou_ids=""
			ou_ids=editorGetOUIds(policy_ids)
			ou_ids=replace(ou_ids, "ou_id", "OU_User.Id_OU")
			if(ou_ids <> "") then
				ou_ids = "OR " & ou_ids
			end if
		end if
	else
		gid = 0
	end if
	RS.Close

	if(gid<>0) then
		Set RS = Conn.Execute("SELECT COUNT(DISTINCT users.id) as cnt FROM users LEFT JOIN policy_user ON users.id=policy_user.user_id LEFT JOIN users_groups ON users.id=users_groups.user_id LEFT JOIN OU_User ON users.id=OU_User.Id_user WHERE role='U' AND ("&policy_ids&" "&group_ids&" "&ou_ids&")" & searchSQL)
	else
		Set RS = Conn.Execute("SELECT COUNT(id) as cnt FROM users WHERE role='U'" & searchSQL)
	end if
	cnt=0

	if(Not RS.EOF) then
		cnt=RS("cnt")
		RS.Close
	end if
  j=cnt/limit


%>

<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	<tr>
	<td>
	<table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
		<tr>
		<td width=100% height=31 class="main_table_title"><a href="" class="header_title"><%=LNG_USERS %></a></td>
		</tr>
		<tr>
		<td height="100%" class="main_table_body">
		<div style="margin:10px;">
		
<form name="search_users" action="users.asp?<%response.write replace(Request.QueryString, "uname=", "uname_old=") %>" method="POST">
<table width="100%" border="0">
	<tr valign="middle">
		<td width="240"><%=LNG_SEARCH_USERS %>: <input type="text" name="uname" value="<%response.write HTMLEncode(myuname) %>"/></td>
		<td width="1%"><button type="submit" name="sub" class="search_button"><%=LNG_SEARCH %></button></td>
		<td style="padding:0px; margin:0px; text-align:right;">
			<% if users_arr(0) = "checked" then %>
			<a  href='edit_user.asp?return_page=users.asp' id='add_user_button'><%=LNG_ADD_USER%></a>
			<% end if %>
		</td>
<%
if(myuname<>"") then
response.write "<td> "&LNG_YOUR_SEARCH_BY &" """&HtmlEncode(myuname)&""" "&LNG_KEYWORD &":</td>"
end if
%>
<td>
<%
if(AD=3) then
%>

<!-- <div align="right"><a href="ad_sync_form.asp" target="_blank"><img src="images/refresh_button.gif" alt="refresh" border="0"></a></div><br> -->

<%
else
%>

<%
end if
%>
</td></tr></table>
</form>


<%


if(cnt>0) then
	page="users.asp?uname=" & Server.URLEncode(myuname) & "&"
	name=LNG_USERS
	Response.Write make_pages(offset, cnt, limit, page, name, sortby) 

if AD=0 and users_arr(2) = "checked" then
	%>
	<table style="padding:0px; margin-top:5px; margin-bottom:5px; border-collapse:collapse; border0px; width:100%;">
		<tr>
			<td style="padding:0px; margin:0px;">
				<A href='#' class="delete_button" onclick="javascript: return LINKCLICK('<%=linkclick_str%>');"><%=LNG_DELETE%></a>
			</td>
		</tr>
	</table>	
	<%
	response.write "<form action='delete_objects.asp' name='delete_objs' id='delete_objs' method='post'><input type='hidden' name='objType' value='users'>"
end if

%>



		<table width="100%" height="100%" cellspacing="0" cellpadding="3" class="data_table">
		<tr class="data_table_title">
<% if (AD = 0) then %><td width="10"><input type='checkbox' name='select_all' id='select_all' onclick='javascript: select_all_objects();'></td><%end if%>

		<td class="table_title"><% response.write sorting(LNG_USERNAME,"name", sortby, offset, limit, page) %></td>

<% if (AD = 3) then %>
<td class="table_title"><% response.write sorting(LNG_DISPLAY_NAME,"display_name", sortby, offset, limit, page) %></td> 
<td class="table_title"><% response.write sorting(LNG_DOMAIN,"domains.name", sortby, offset, limit, page) %></td> 
<%end if%>

<% if (AD = 0) then %>		<td class="table_title"><% response.write sorting(LNG_REGISTERED,"reg_date", sortby, offset, limit, page) %></td> <% end if %>


<% if (AD = 3 OR SMS = 1) then%>
<td class="table_title"><% response.write sorting(LNG_MOBILE_PHONE,"mobile_phone", sortby, offset, limit, page) %></td>
<%end if%>

<% if (AD = 3 OR EM = 1) then%>
<td class="table_title"><% response.write sorting(LNG_EMAIL,"email", sortby, offset, limit, page) %></td>
<% end if %>


<% if (EDIR = 1) then %>		<td class="table_title"><%=LNG_CONTEXT %></td> <% end if %>

		<td class="table_title"><% response.write sorting(LNG_ONLINE,"last_request1", sortby, offset, limit, page) %></td>
		<td class="table_title"><% response.write sorting(LNG_LAST_ACTIVITY,"last_request", sortby, offset, limit, page) %></td>
		<td class="table_title"><% response.write sorting(LNG_CLIENT_VERSION,"client_version", sortby, offset, limit, page) %></td>
		<td class="table_title"><%=LNG_ACTIONS %></td>
		</tr>


<%

'show main table
	if(gid<>0) then
		Set RS = Conn.Execute("SELECT DISTINCT users.id as id, next_request, last_standby_request, next_standby_request, context_id, mobile_phone, email, users.name as name, standby, display_name, domain_id, domains.name as domain_name, deskbar_id, reg_date, last_date, convert(varchar,last_request) as last_request1, last_request, users.client_version FROM users LEFT JOIN domains ON domains.id=users.domain_id LEFT JOIN policy_user ON users.id=policy_user.user_id LEFT JOIN users_groups ON users.id=users_groups.user_id LEFT JOIN OU_User ON users.id=OU_User.Id_user WHERE role='U'" & searchSQL & " AND ("&policy_ids&" "&group_ids&" "&ou_ids&") ORDER BY "&sortby)
	else
		Set RS = Conn.Execute("SELECT users.id as id, next_request, last_standby_request, next_standby_request, context_id, mobile_phone, email, users.name as name, display_name, standby, domain_id, domains.name as domain_name, deskbar_id, reg_date, last_date, convert(varchar,last_request) as last_request1, last_request, users.client_version FROM users LEFT JOIN domains ON domains.id=users.domain_id WHERE role='U'" & searchSQL & " ORDER BY "&sortby)
	end if
	num=offset
	if(Not RS.EOF) then RS.Move(offset) end if
	Do While Not RS.EOF
		num=num+1
		if((offset+limit) < num) then Exit Do end if
		if(num > offset AND offset+limit >= num) then
	reg_date=RS("reg_date")
	last_date=RS("last_date")
	if(Not IsNull(RS("last_request"))) then
		last_activ = CStr(RS("last_request1"))

	        mydiff=DateDiff ("n", RS("last_request"), now_db)
			if(RS("next_request")<>"") then
				online_counter = Int(CLng(RS("next_request"))/60) * 2
			else
				online_counter = 2
			end if
			
		if(mydiff > online_counter) then
			online="<img src='images/offline.gif' alt='"& LNG_OFFLINE &"' title='"& LNG_OFFLINE &"' width='11' height='11' border='0'>"
		else
			online="<img src='images/online.gif' alt='"&LNG_ONLINE &"' title='"&LNG_ONLINE &"' width='11' height='11' border='0'>"
		end if
	else
		last_activ = "&nbsp;"
		online="<img src='images/offline.gif' alt='"& LNG_OFFLINE &"' title='"& LNG_OFFLINE &"' width='11' height='11' border='0'>"
	end if

	standby_diff=DateDiff ("n", RS("last_standby_request"), now_db)
	if(RS("next_standby_request")<>"" AND RS("next_standby_request")<>"0") then
		standby_counter = Int(CLng(RS("next_standby_request"))/60) * 2
	else
		standby_counter = 10
	end if
	
	if(RS("standby")=1 AND (standby_diff < standby_counter)) then
		online="<img src='images/standby.gif' alt='"&LNG_STAND_BY &"' title='"&LNG_STAND_BY &"' width='11' height='11' border='0'>"
	end if

	if(IsNull(RS("name"))) then
	name = LNG_UNREGISTERED
	else
	name = HtmlEncode(RS("name"))
	end if

 if (AD = 3 OR SMS = 1) then 	
	mobile="&nbsp;"
	if(RS("mobile_phone")<>"") then
		mobile=HtmlEncode(RS("mobile_phone"))
	end if
 end if

 if (AD = 3 OR EM = 1) then
	email="&nbsp;"
	if(RS("email")<>"") then
		email=HtmlEncode(RS("email"))
	end if
 end if

	domain="&nbsp;"
	if (AD = 3) then 	
		if(Not IsNull(RS("domain_id"))) then
			if(RS("domain_id")<>"") then
			    domain=HtmlEncode(RS("domain_name"))
			else
				domain="&nbsp;"
			end if
		end if
	end if
if (EDIR = 1) then 
	strContext="&nbsp;"
       	if(Not IsNull(RS("context_id"))) then
		if(RS("context_id")<>"") then
			Set RS3 = Conn.Execute("SELECT name FROM edir_context WHERE id=" & RS("context_id"))
			if(Not RS3.EOF) then
				  strContext=HtmlEncode(RS3("name"))
				RS3.close
			end if
		end if
	end if

'	Set RS3 = Conn.Execute("SELECT name FROM edir_context WHERE id=" & RS("context_id"))
'	if(Not RS3.EOF) then
'		  strContext=RS3("name")
'		RS3.close
'	else
'		strContext="&nbsp;"
'	end if
end if 
	
	


	end if
	strObjectID=RS("id")
	clientVersion = RS("client_version")
        Response.Write "<tr>"
if(AD=0) then %>
	<td><input type="checkbox" name="objects" value="<%=strObjectID%>"></td>
<%
end if
response.write "<td>" + name + "</td><td align=center>"

 if (AD = 0) then 	
	Response.Write reg_date 
	Response.Write "</td><td align=center>"
 end if
 if (AD = 3) then 	

	if(RS("display_name")<>"") then
		response.write RS("display_name")
	else
		response.write "&nbsp;"
	end if
	Response.Write "</td><td align=center>"
	
	Response.Write domain 
	Response.Write "</td><td align=center>"
 end if

 if (AD = 3 OR SMS=1) then
	Response.Write mobile 
	Response.Write "</td><td align=center>"
 end if

 if (AD = 3 OR EM=1) then
	Response.Write email
	Response.Write "</td><td align=center>"
 end if
 if (EDIR = 1) then 	
	strContext=replace (strContext,",ou=",".")
	strContext=replace (strContext,",o=",".")
	strContext=replace (strContext,",dc=",".")
	strContext=replace (strContext,",c=",".")

	strContext=replace (strContext,"ou=","")
	strContext=replace (strContext,"o=","")
	strContext=replace (strContext,"dc=","")
	strContext=replace (strContext,"c=","")

	Response.Write strContext
	Response.Write "</td><td align=center>"
 end if



	Response.Write online & "</td>"
	Response.write"<td align=center>" & last_activ & "</td>"
	Response.write"<td align=center>" & clientVersion & "</td>"
 if (AD = 0) then 	
	Response.Write "<td align='center' nowrap='nowrap'>" 
'	response.write "<a href='user_delete.asp?id=" & CStr(RS("id")) & "' onclick=""javascript: return LINKCLICK();""><img src='images/action_icons/delete.gif' alt='delete user' width='16' height='16' border='0' hspace=5></a></td>"
	if users_arr(1) = "checked" then
		response.write "<a href='edit_user.asp?return_page=users.asp&id=" & CStr(RS("id")) &"'><img src='images/action_icons/edit.png' title='"&LNG_EDIT&"' alt='"&LNG_EDIT&"' width='16' height='16' border='0' hspace=5></a></td>"
	end if
 end if
 if (AD = 3) then
	Response.Write "<td align='center' nowrap='nowrap'><a href='#' onclick=""javascript: window.open('user_view_details.asp?id=" & CStr(RS("id")) & "','', 'status=0, toolbar=0, width=350, height=350, scrollbars=1')""><img src='images/action_icons/preview.png' alt='"&LNG_VIEW_USER_DETAILS &"' width='16' height='16' border='0' hspace=5></a></td>"
 end if
	Response.Write "</tr>"
'	end if

	RS.MoveNext
	Loop
	RS.Close


%>         
			</table>
<%

if AD=0 and users_arr(2) = "checked" then
	response.write "</form><br/><A href='#' class='delete_button' onclick=""javascript: return LINKCLICK('"&linkclick_str&"');"">"&LNG_DELETE&"</a><br><br>"
end if

	Response.Write make_pages(offset, cnt, limit, page, name, sortby) 


else

Response.Write "<center><b>"&LNG_THERE_ARE_NO_USERS &".</b><br><br></center>"

end if

%>


		 <img src="images/inv.gif" alt="" width="1" height="100%" border="0">
		</div>
		</td>
		</tr>
	</table>
	</td>
	</tr>
</table>


<%
else

  Response.Redirect "index.asp"

end if

end if

%> 
</body>
</html>
<!-- #include file="db_conn_close.asp" -->