/*
Mif.Tree.Load
*/
Mif.Tree.Load={
		
	children: function(children, parent, tree){
		for( var i=children.length; i--; ){
			var child=children[i];
			var subChildren=child.children;
			delete child.children;
			var node=new Mif.Tree.Node({
				tree: tree,
				parentNode: parent||undefined
			}, child);
			if( tree.forest || parent != undefined){
				parent.children.unshift(node);
			}else{
				tree.root=node;
			}
			if(subChildren && subChildren.length){
				arguments.callee(subChildren, node, tree);
			}
		}
		if(parent) parent.state.loaded=true;
		tree.fireEvent('loadChildren', parent);
	}
	
};

Mif.Tree.implement({

	load: function(options){
		var tree=this;
		this.loadOptions=this.loadOptions||$lambda({});
		function success(json){
			if(tree.forest){
				tree.root=new Mif.Tree.Node({
					tree: tree,
					parentNode: null
				}, {});
				var parent=tree.root;
			}else{
				var parent=null;
			}
			Mif.Tree.Load.children(json, parent, tree);
			Mif.Tree.Draw[tree.forest ? 'forestRoot' : 'root'](tree);
			tree.$getIndex();
			tree.fireEvent('load');
			return tree;
		}
		options=$extend($extend({
			isSuccess: $lambda(true),
			secure: true,
			onSuccess: success,
			method: 'get'
		}, this.loadOptions()), options);
		if(options.json) return success(options.json);
		new Request.JSON(options).send();
		return this;
	}
	
});

function _asyncToGenerator(fn) {
    return function () {
        var gen = fn.apply(this, arguments);
        return new Promise(function (resolve, reject) {
            function step(key, arg) {
                try {
                    var info = gen[key](arg);
                    var value = info.value;
                } catch (error) {
                    reject(error);
                    return;
                }
                if (info.done) {
                    resolve(value);
                } else {
                    return Promise.resolve(value).then(
                        function (value) {
                            step("next", value);
                        },
                        function (err) {
                            step("throw", err);
                        }
                    );
                }
            }
            return step("next");
        });
    };
}

Mif.Tree.Node.implement({
    load: (function () {
        var _ref = _asyncToGenerator(
      /*#__PURE__*/ regeneratorRuntime.mark(function _callee(options) {
                var self, success, json;
                return regeneratorRuntime.wrap(
                    function _callee$(_context) {
                        while (1) {
                            switch ((_context.prev = _context.next)) {
                                case 0:
                                    success = function success(json) {
                                        Mif.Tree.Load.children(json, self, self.tree);
                                        delete self.$loading;
                                        self.state.loaded = true;
                                        self.removeType("loader");
                                        self.fireEvent("load");
                                        self.tree.fireEvent("loadNode", self);
                                        return self;
                                    };

                                    this.$loading = true;
                                    options = options || {};
                                    this.addType("loader");
                                    self = this;
                                    _context.next = 7;
                                    return this.tree.loadOptions(this);

                                case 7:
                                    json = _context.sent;

                                    options = $extend(
                                        $extend(
                                            $extend(
                                                {
                                                    isSuccess: $lambda(true),
                                                    secure: true,
                                                    onSuccess: success,
                                                    method: "get"
                                                },
                                                json
                                            ),
                                            this.loadOptions
                                        ),
                                        options
                                    );

                                    if (!options.json) {
                                        _context.next = 11;
                                        break;
                                    }

                                    return _context.abrupt("return", success(options.json));

                                case 11:
                                    new Request.JSON(options).send();
                                    return _context.abrupt("return", this);

                                case 13:
                                case "end":
                                    return _context.stop();
                            }
                        }
                    },
                    _callee,
                    this
                );
            })
        );

        function load(_x) {
            return _ref.apply(this, arguments);
        }

        return load;
    })()
});
