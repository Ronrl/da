/*mootools patch*/

if(document.documentElement.getBoundingClientRect){//ie, opear9.5+, ff3+

	Element.implement({

		getPosition: function(relative){
			rect=this.getBoundingClientRect();
			var clientTop = document.html.clientTop || document.body.clientTop || 0, clientLeft = document.html.clientLeft || document.body.clientLeft || 0
			var position={x: rect.left-this.scrollLeft-clientLeft, y:rect.top-this.scrollTop-clientTop};
			var relativePosition = (relative && (relative = $(relative))) ? relative.getPosition() : {x: 0, y: 0};
			return {x: position.x - relativePosition.x, y: position.y - relativePosition.y};
		}
		
	});

}

Mif.Tree.implement({
	
	select: function(node, preventFocus) {
		if(!preventFocus && (Browser.Engine.gecko||Browser.Engine.webkit)) {
			this.wrapper.focus();
		}
		var current=this.selected;
		//if (current==node) return this;
		if (current) {
			current.select(false);
			this.fireEvent('unSelect', [current]).fireEvent('selectChange', [current, false]);
		}
		this.selected = node;
		node.select(true);
		this.fireEvent('select', [node]).fireEvent('selectChange', [node, true]);
		return this;
	}
	
});

