<!-- #include file="config.inc" -->
<!-- #include file="lang.asp" -->
<%
	mifTreeFolder = "mif_tree"
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>DeskAlerts</title>
	<link rel="stylesheet" href="<%=mifTreeFolder%>/style.css" type="text/css" />
	<link href="css/style9.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/mootools.js"></script>	
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Node.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Hover.js"></script>	
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Selection.js"></script>	
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Load.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Draw.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.KeyNav.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Sort.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Transform.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Drag.js"></script>	
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Drag.Element.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Checkbox.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Rename.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Row.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.mootools-patch.js"></script>
	<script type="text/javascript">
		var tree;
		function getCheckedOus ()
		{
			if( tree && tree != 'undefined' )
			{
				nodes = new Array();

				tree.root.recursive
				(
					function()
					{
						if( this.state.checked == "checked" )
						{
							temp = new Object();
							nodes.push(temp);
							nodes[nodes.length-1].id = this.property.id;
							nodes[nodes.length-1].type = this.property.type1;
							nodes[nodes.length-1].path = this.property.path;
							nodes[nodes.length-1].name = this.property.name;
						}
					}
				);
				return nodes;
			}
			return null;
		}
		window.onload = function()
		{
			tree = new Mif.Tree({
				container: $('mydiv'),
				forest: false,
				initialize: function(){
					this.initCheckbox('deps');
					new Mif.Tree.KeyNav(this);
				},
				types: {
					organization:{
						openIcon: 'mif-tree-organization-icon',
						closeIcon: 'mif-tree-organization-icon'
					},
					domain:{
						openIcon: 'mif-tree-domain-icon',
						closeIcon: 'mif-tree-domain-icon'
					},
					loader:{
						openIcon: 'mif-tree-loader-icon',
						closeIcon: 'mif-tree-loader-icon'
					},
					ou:{
						openIcon: 'mif-tree-ou-icon',
						closeIcon: 'mif-tree-ou-icon'
					}
				},
				dfltType:'organization'
			});
			try
			{
				tree.load({
					url: 'ou_get_tree.asp'
				});
			}
			catch(e)
			{
			}
			
			tree.loadOptions=function(node){
				return {
					url: 'ou_get_child.asp?id='+node.id+'&type='+node.property.type1
				};
			}
		};
	</script>
</head>
<body style="margin:0px">
	<div name="mydiv" id="mydiv" style="height:500px"></div>
</body>
</html>