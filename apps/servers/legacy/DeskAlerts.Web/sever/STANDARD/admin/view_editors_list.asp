﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%
check_session()
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv=Content-Type content="text/html;charset=utf-8">
	<title>Deskalerts Control Panel</title>
	<link href="css/style9.css" rel="stylesheet" type="text/css">
	<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
	<script language="javascript" type="text/javascript">
		$(document).ready(function() {
			$("#close_button").button();
			/*$("#search_button").button();*/
		});
	</script>
	<style>
		table {border-collapse:collapse;empty-cells:show}
	</style>
</head>
<%
if(Request("limit") <> "") then 
limit = clng(Request("limit"))
else 
limit=50
end if

if(Request("offset") <> "") then 
offset = clng(Request("offset"))
else 
offset=0
end if

policyId = clng(request("id"))

'search = request("search")
'if search <> "" then
'	searchUsersSQL = " AND (name LIKE N'%"&replace(search, "'", "''")&"%' OR display_name LIKE N'%"&replace(search, "'", "''")&"%')"
'	searchOtherSQL = " AND name LIKE N'%"&replace(search, "'", "''")&"%'"
'else
'	searchSQL = ""
'	searchOtherSQL = ""
'end if
%>
<body style="margin:0" class="body">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
	<tr>
	<td>
		<table width="100%" cellspacing="0" cellpadding="0" class="main_table">
		<tr>
			<td width=100% height=31 class="main_table_title"><a href="" class="header_title"><%=LNG_LIST_OF_EDITORS %></a></td>
		</tr>
		<tr>
			<td class="main_table_body">
		<div style="margin:10px;">
		
<form action="view_editors_list.asp" method="POST">
<input type="hidden" name="id" value="<%=policyId%>"/>
<!--<table border="0" cellpadding="3">
<tr valign="middle">
	<td nowrap valign="middle"><%=LNG_SEARCH%>:</td><td><input type="text" id="search" name="search" value="<%=HtmlEncode(search)%>" style="width:135px"/></td><td><a href="javascript: document.forms[0].submit()" id="search_button" class="search_button"><%=LNG_SEARCH%></a></td>
</tr>
</table>-->
</form>
<%
Set RS = Conn.Execute("SELECT COUNT(id) as cnt FROM policy_editor WHERE policy_id="&policyId)
cnt = RS("cnt")

if(cnt>0) then
	page= "view_editors_list.asp?id="&policyId&"&"
	name=LNG_EDITORS
	Response.Write make_pages(offset, cnt, limit, page, name, sortby) 

'paging
%>
<table width="95%" cellspacing="0" cellpadding="3" class="data_table">
<tr class="data_table_title">
	<td class="table_title"><%=LNG_NAME %></td>
</tr>
<%
	Set rsEditors = Conn.Execute("SELECT u.name FROM users u LEFT JOIN policy_editor pe ON pe.editor_id=u.id WHERE pe.policy_id="&policyId)
	do while not rsEditors.EOF
	editorName = rsEditors("name")
%>
	<tr><td><%=HtmlEncode(editorName)%></td></tr>
<%
		rsEditors.MoveNext
	loop
%>
 
</table>
<%
	Response.Write make_pages(offset, cnt, limit, page, name, sortby) 
else
%>
	<center><%=LNG_NO_EDITORS_FOR_POLICY %></center>
<%
end if
%>
 
<br/>
 
</div>		</td>
		</tr>
	</table>
	</td>
	</tr>
</table>
</body>
</html>
<!-- #include file="db_conn_close.asp" -->