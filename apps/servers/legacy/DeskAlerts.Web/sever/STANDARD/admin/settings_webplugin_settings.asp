﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<!-- #include file="md5.asp" -->
<%

check_session()

uid = Session("uid")
if (uid <> "")  then
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html style="height:100%">
<head>
<title>DeskAlerts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/style9.css" rel="stylesheet" type="text/css">

<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script language="javascript" type="text/javascript" src="functions.js"></script>
<script language="javascript" type="text/javascript">
(function($) {
    $(document).ready(function() {
        /*$("#social_settings_table").tabs({
            activate: function(event, ui) {
                var href = $("#social_settings_table div.ui-tabs-panel:visible iframe").get(0).src;
                var chunks = href.split("/");
                LoadHelp(chunks[chunks.length - 1]);
            },
            create: function(event, ui) {
                var href = $("#social_settings_table div.ui-tabs-panel:visible iframe").get(0).src;
                var chunks = href.split("/");
                LoadHelp(chunks[chunks.length - 1]);
            }
        });*/
        resizeAllframes();
        resizeIframe('if_webplugin_frame');
        
        function LoadHelp(href) {
            var about = pageTheme(href, null);
            window.top.frames["main"].document.getElementById("helpIframe").src = "help.asp?about=" + about;
        }
    });
})(jQuery);

function resizeAllframes() {
	var height = window.frameElement.scrollHeight - 110;
	$('#if_blog_frame').css('height', height);
	$('#if_twitter_frame').css('height', height);
	$('#if_linkedin_frame').css('height', height);
}

function resizeIframe(id) {
	var iframeDoc = document.getElementById(id);
	iframeDoc.height = window.frameElement.scrollHeight - 100;
	var h;
	if (iframeDoc.contentDocument) {
		h = iframeDoc.contentDocument.documentElement.scrollHeight; //FF, Opera, and Chrome
		h = iframeDoc.contentDocument.body.scrollHeight; //ie 8-9-10
		if (iframeDoc.height < h) iframeDoc.height = h;
	}
	else {
		h = iframeDoc.contentWindow.document.body.scrollHeight; //IE6, IE7 and Chrome
		if (iframeDoc.height < h) iframeDoc.height = h;
	}
}
</script>
<script language="javascript">

</script>
</head>

<body style="margin:0px;height:100%" class="body">

<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	<tr>
	<td>
	<table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
		<tr>
		<td width="100%" height="31" class="main_table_title"><img src="images/menu_icons/settings_20.png" alt="" width="20" height="20" border="0" style="padding-left:7px">
					<a href="#" class="header_title" style="position:absolute;top:15px"><%=LNG_WEBPLUGIN_SETTINGS %></a></td>
		</tr>
		<tr>
		<td class="main_table_body" height="100%">
		<div id="social_settings_table" style="padding:0px; border:none;">
			<!--<ul>
			<% if BLOG=1 then %>	
				<li><a href="#if_blog" onclick="resizeIframe('if_blog_frame');"><%=LNG_BLOG_SETTINGS%> </a></li>
			<% end if %>	
			<% if SOCIAL_MEDIA=1 then %>			
				<li><a href="#if_twitter"  onclick="resizeIframe('if_twitter_frame');" ><%=LNG_TWITTER_SETTINGS%></a></li>
			<% end if %>	
			<% if SOCIAL_LINKEDIN=1 then %>	
				<li><a href="#if_linkedin"  onclick="resizeIframe('if_linkedin_frame');"><%=LNG_LINKEDIN_SETTINGS%></a></li>
			<% end if %>			
			</ul>
			<% if BLOG=1 then %>	
			<div id="if_blog">
				<iframe src="settings_blog_settings.asp" frameborder="no" style="width:100%;padding:0px;height:100%"  id="if_blog_frame"></iframe>
			</div>
			<% end if %>
			<% if SOCIAL_MEDIA=1 then %>	
			<div id="if_twitter">
				<iframe src="settings_social_media.asp" frameborder="no" style="width:100%;padding:0px;height:100%" id="if_twitter_frame"></iframe>
			</div>
			<% end if %>	-->
			<div id="if_webplugin">
				<iframe src="settings_webplugin_codegenerator.asp" frameborder="no" style="width:100%;padding:0px" id="if_webplugin_frame"></iframe>
			</div>
		 <img src="images/inv.gif" alt="" width="1" height="100%" border="0">
		</div>
		</td>
		</tr>
	</table>
	</td>
	</tr>
</table>
</body>
</html>
<%

else

  Response.Redirect "index.asp"

end if

%> 

<!-- #include file="db_conn_close.asp" -->