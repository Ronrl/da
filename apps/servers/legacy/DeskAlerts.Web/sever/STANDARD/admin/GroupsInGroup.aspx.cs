﻿using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Pages;
using Resources;
using System;
using System.Web.UI.WebControls;

namespace DeskAlerts.Server.sever.STANDARD.admin
{
    public partial class GroupsInGroup : DeskAlertsBaseListPage
    {
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            groupName.Text = GetSortLink(resources.LNG_GROUP_NAME, "groups.name", Request["sortBy"]);
            domain.Text = GetSortLink(resources.LNG_DOMAIN, "domains.name", Request["sortBy"]);
            users.Text = resources.LNG_USERS;
            computers.Text = resources.LNG_COMPUTERS;
            groups.Text = resources.LNG_GROUPS;

            actionsCollumn.Text = resources.LNG_ACTION;

            var cnt = Content.Count < (Offset + Limit) ? Content.Count : Offset + Limit;
            for (int i = Offset; i < cnt; i++)
            {
                DataRow row = Content.GetRow(i);

                var id = row.GetString("id");
                var tableRow = new TableRow();
                var chb = GetCheckBoxForDelete(row);
                var checkBoxCell = new TableCell { HorizontalAlign = HorizontalAlign.Center };
                checkBoxCell.Controls.Add(chb);
                tableRow.Cells.Add(checkBoxCell);

                var groupCell = new TableCell { Text = row.GetString("name") };
                tableRow.Cells.Add(groupCell);

                var domainCell = new TableCell
                {
                    Text = row.GetString("domain_name"),
                    HorizontalAlign = HorizontalAlign.Center
                };
                tableRow.Cells.Add(domainCell);

                var usersCntCell = new TableCell
                {
                    Text = $@"<a href='UsersInGroup.aspx?id={id}'>{row.GetString("users_cnt")}</a>",
                    HorizontalAlign = HorizontalAlign.Center
                };
                tableRow.Cells.Add(usersCntCell);

                var computersCntCell = new TableCell
                {
                    Text = $@"<a href='CompsInGroup.aspx?id={id}'>{row.GetString("computers_cnt")}</a>",
                    HorizontalAlign = HorizontalAlign.Center
                };
                tableRow.Cells.Add(computersCntCell);

                var groupsCnt = Convert.ToInt32(dbMgr.GetScalarByQuery("SELECT count(group_id) as groups_cnt FROM groups_groups WHERE container_group_id=" + row.GetString("id")));
                var groupsCntCell =
                    new TableCell
                    {
                        Text = $@"<a href='GroupsInGroup.aspx?id={id}'>{groupsCnt}</a>",
                        HorizontalAlign = HorizontalAlign.Center
                    };
                tableRow.Cells.Add(groupsCntCell);


                var editButton = GetActionButton(row.GetString("id"), "images/action_icons/preview.png", "LNG_PARENT_GROUPS",

                   "viewParents(" + row.GetString("id") + ")"

                );

                var actionsCell = new TableCell { HorizontalAlign = HorizontalAlign.Center };
                actionsCell.Controls.Add(editButton);

                tableRow.Cells.Add(actionsCell);

                contentTable.Rows.Add(tableRow);

            }
        }


        protected override string[] Collumns => new string[] { };

        protected override string ContentName => resources.LNG_GROUPS;
        protected override string DataTable => "";

        protected override System.Web.UI.HtmlControls.HtmlGenericControl NoRowsDiv => NoRows;

        protected override System.Web.UI.HtmlControls.HtmlGenericControl TableDiv => mainTableDiv;


        protected override string DefaultOrderCollumn => "name";


        protected override string CustomQuery
        {
            get
            {


                var query = string.Empty;

                if (!string.IsNullOrEmpty(Request["member_of"]))
                {
                    query =
                        $@"SELECT groups.id as id, context_id, groups.name as name, domains.name as domain_name, custom_group, 
                                COUNT(distinct users_groups.user_id) as users_cnt,
                                COUNT(distinct computers_groups.comp_id) as computers_cnt FROM 
                                groups LEFT JOIN domains ON domains.id=groups.domain_id
                                LEFT JOIN users_groups ON groups.id=users_groups.group_id 
                                LEFT JOIN computers_groups ON groups.id=computers_groups.group_id 
                                INNER JOIN groups_groups ON groups.id=groups_groups.container_group_id 
                                WHERE domains.id<>0 AND groups_groups.group_id={Request["id"]} 
                                GROUP BY groups.id, groups.name, domains.name, custom_group, context_id";

                    if (!string.IsNullOrEmpty(Request["sortby"]))
                    {
                        query += " ORDER BY " + Request["sortby"];
                    }
                }
                else
                {
                    query =
                        $@"SELECT groups.id as id, context_id, groups.name as name, domains.name as domain_name, custom_group, 
                                COUNT(distinct users_groups.user_id) as users_cnt, 
                                COUNT(distinct computers_groups.comp_id) as computers_cnt 
                                FROM groups 
                                LEFT JOIN domains ON domains.id=groups.domain_id 
                                LEFT JOIN users_groups ON groups.id=users_groups.group_id 
                                LEFT JOIN computers_groups ON groups.id=computers_groups.group_id 
                                INNER JOIN groups_groups ON groups.id=groups_groups.group_id 
                                WHERE domains.id<>0 AND groups_groups.container_group_id={Request["id"]} 
                                GROUP BY groups.id, groups.name, domains.name, custom_group, context_id";

                    if (!string.IsNullOrEmpty(Request["sortby"]))
                    {
                        query += " ORDER BY " + Request["sortby"];
                    }
                }


                return query;
            }
        }

        protected override string CustomCountQuery
        {
            get
            {
                if (string.IsNullOrEmpty(Request["member_of"]))
                {
                    return "SELECT COUNT(group_id) as mycnt FROM groups_groups WHERE groups_groups.container_group_id = " + Request["id"];
                }


                return "SELECT COUNT(group_id) as mycnt FROM groups_groups WHERE groups_groups.group_id = " + Request["id"];
            }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl TopPaging => topPaging;
        protected override System.Web.UI.HtmlControls.HtmlGenericControl BottomPaging => bottomPaging;

        protected override string PageParams => "id=" + Request["id"] + "&";
    }
}