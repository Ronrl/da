<%

function recurrenceForm()
alert_id = Request("id")

%>
	<link href="css/jquery/jquery_timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css">
	<script language="javascript" type="text/javascript" src="jscripts/date.js"></script>
	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery_timepicker/jquery-ui-timepicker-addon.js"></script>
	<script language="javascript" type="text/javascript" src="jscripts/jquery/datepicker_fix.js"></script>
	<script language="javascript">
	var countdownArray = [
<%	

if (alert_id<>"") then
		
		SQL = "SELECT number_days FROM recurrence WHERE alert_id=" + alert_id +" AND pattern='c'"
		Set rs = Conn.Execute(SQL)
		if (not rs.EOF) then
			Response.Write rs("number_days")
			rs.MoveNext
		end if
		do while not rs.EOF
			Response.Write ","&rs("number_days")
			rs.MoveNext
		loop
	
end if

%>
		];
	</script>


<script language="javascript">
	
	function toggleDisabled(el, state) {
		try {
			el.disabled = state;
		}
		catch(E){
		}
		if (el && el.childNodes && el.childNodes.length > 0) {
			for (var x = 0; x < el.childNodes.length; x++) {
				toggleDisabled(el.childNodes[x], state);
			}
		}
	}
	
	var serverDate;
	var settingsDateFormat = "<%=GetDateFormat()%>";
	var settingsTimeFormat = "<%=GetTimeFormat()%>";

	var responseDbFormat = "dd/MM/yyyy HH:mm:ss";
	var saveDbFormat = "dd/MM/yyyy HH:mm";
	var saveDbDateFormat = "dd/MM/yyyy";
	var saveDbTimeFormat = "HH:mm";
	var uiFormat = getUiDateTimeFormat(settingsDateFormat,settingsTimeFormat);

	
	$(document).ready(function(){
		serverDate = new Date(getDateFromAspFormat("<%=now_db%>",responseDbFormat));
		setInterval(function(){serverDate.setSeconds(serverDate.getSeconds()+1);},1000);
		
		var fromDate = $.trim($("#start_date_and_time").val());
		var toDate = $.trim($("#end_date_and_time").val());
		
		var fromTime = $.trim($("#start_time").val());
		var toTime = $.trim($("#end_time").val());
		
		if (fromDate!="" && isDate(fromDate,responseDbFormat))
		{
			$("#start_date_and_time").val(formatDate(new Date(getDateFromFormat(fromDate,responseDbFormat)),uiFormat));
		}
		
		if (toDate!="" && isDate(toDate,responseDbFormat))
		{
			$("#end_date_and_time").val(formatDate(new Date(getDateFromFormat(toDate,responseDbFormat)),uiFormat));
		}
		
		var uiTimetimeFormat = getUiTimeFormat(settingsTimeFormat);
		
		if (fromTime!="" && isDate(fromTime,responseDbFormat))
		{
			fromTime = new Date(getDateFromFormat(fromTime,responseDbFormat));
		}
		
		if (toTime!="" && isDate(toTime,responseDbFormat))
		{
			toTime = new Date(getDateFromFormat(toTime,responseDbFormat));
		}
		
		var fromDateRec = $.trim($("#start_date_rec").val());
		var toDateRec = $.trim($("#end_date_rec").val());
		
		if (fromDateRec!="" && isDate(fromDateRec,responseDbFormat))
		{
			fromDateRec = new Date(getDateFromFormat(fromDateRec,responseDbFormat));
			if (fromDateRec.getFullYear() == "1900")
			{
				fromDateRec = null;
			}
		}

		if (toDateRec!="" && isDate(toDateRec,responseDbFormat))
		{
			toDateRec = new Date(getDateFromFormat(toDateRec,responseDbFormat));
			if (toDateRec.getFullYear() == "1900")
			{
				toDateRec = null;
			}
		}
		

		initDateTimePicker($(".date_time_param_input"),settingsDateFormat,settingsTimeFormat);
		initDatePicker($(".date_param_input"),settingsDateFormat);
		initTimePicker($(".time_param_input"),settingsDateFormat,settingsTimeFormat);
		
		$("#start_date_rec").datepicker("setDate",fromDateRec);
		$("#end_date_rec").datepicker("setDate",toDateRec);
		
		$("#start_time").datepicker("setDate",fromTime);
		$("#end_time").datepicker("setDate",toTime);
				
		$("#add_countdown").button();
		$("#add_countdown").bind("click",addCountDown);
		
		if (countdownArray && countdownArray.length > 0)
		{
			for (var i=0; i<countdownArray.length; i++)
			{
				addCountDown(countdownArray[i]);
			}
			var $checkbox =  $("#countdown_enable");
			$checkbox.prop("checked","checked");
		}
		countdownEnableChange(true);
	});
	var last_countdown_id = 0;
	function addCountDown(countValue)
	{
		var count = $(".count_down_element").length;
		var id = last_countdown_id++;
		var $countDownDiv = $("#countdown_div");
	
		var $countDownElement = $("<div/>").addClass("count_down_element").hide();
		
		var $table=$("<table/>").css({
			padding:"0px",
			margin:"0px",
			"border-collapse":"collapse",
			border: "0px",
			"vertical-align": "middle"
		});
		var $tr = $("<tr/>");
		var $inputCell = $("<td/>").addClass("count_down_cell");
		var $deleteCell = $("<td/>").addClass("count_down_cell");
		$table.append($tr);
		$tr.append($inputCell);
		$tr.append($deleteCell);
		$countDownElement.append($table);

		var $inputElement = $("<select/>").attr({"id":"count_down_"+id, "name": "countdowns"}).addClass("count_down_input");
		var $deleteIcon = $("<img/>").attr({"src":"images/action_icons/delete.png"}).addClass("count_down_delete_img");
		if(count == 0)
		{
			$deleteIcon.hide();
		}
		else
		{
			$(".count_down_delete_img").show();
		}
		
		$inputCell.append($inputElement);
		$inputCell.append($("<label/>").attr({"for":"countdowns"}).text(" <%=LNG_MINUTES_BEFORE_START_DATE_TIME %> "));
		$deleteCell.append($deleteIcon);
		$countDownDiv.append($countDownElement);
		
		var step = 5;
		for (var i = step; i<=120; i+=step )
		{
			$inputElement.append($("<option value='"+i+"'>"+i+"</option>"));
		}
		
		if (!(countValue===undefined))
		{
			$inputElement.val(countValue);
		}
		
		$countDownElement.show();
		
		$deleteIcon.bind("click", function(){
			var height = $table.height();
			$countDownElement.remove();
			$("#reccurence_pattern").height($("#reccurence_pattern").height() - height);
			$("#reccurence_div").height($("#reccurence_div").height() - height);
			if($(".count_down_element").length < 2)
			{
				$(".count_down_delete_img").hide();
			}
		});

		var height = $table.height();
	
		$("#reccurence_pattern").height($("#reccurence_pattern").height() + height);
		$("#reccurence_div").height($("#reccurence_div").height() + height);
	}
	
	
	function countdownEnableChange(init, force)
	{
		var $checkbox =  $("#countdown_enable");
		var $add_button = $("#add_countdown");
		var countdownDiv = document.getElementById("countdown_div");
		
		if (force === undefined)
		{
			force = !$checkbox.prop("checked")
		}
		
		if (force)
		{
			$(".count_down_delete_img").hide();
		}
		else
		{
			if($(".count_down_element").length > 1)
			{
				$(".count_down_delete_img").show();
			}
			if (init && $(".count_down_element").length == 0)
			{
				addCountDown();
			}
		}
		$add_button.button({ disabled: force });
		toggleDisabled(countdownDiv, force);
	}


function check_valid_dates_rec()
{
	var fromDate = $.trim($("#start_date_and_time").val());
	var toDate = $.trim($("#end_date_and_time").val());
	
	var dateTest = /^\d{1,2}\/\d{1,2}\/\d{4}$/;

	if(document.getElementById("once").checked)
	{
		if (!fromDate)
		{
			alert("<%=LNG_PLEASE_ENTER_START_DATE %>!");
			return false;
		}
		if(!toDate)
		{
			alert("<%=LNG_PLEASE_ENTER_END_DATE %>!");
			return false;
		}
		
		if (!isDate(fromDate,uiFormat))
		{
			alert("<%=LNG_PLEASE_ENTER_CORRECT_START_DATE %>!");
			return false;
		}
		else
		{
			if (fromDate!="")
			{
				fromDate = new Date(getDateFromFormat(fromDate,uiFormat));
				$("#from_date").val(formatDate(fromDate,saveDbFormat));
			}
		}
		
		if (!isDate(toDate,uiFormat))
		{
			alert("<%=LNG_PLEASE_ENTER_END_DATE %>!");
			return false;
		}
		else
		{
			if (toDate!="")
			{
				toDate = new Date(getDateFromFormat(toDate,uiFormat))
				$("#to_date").val(formatDate(toDate,saveDbFormat));
			}
		}
		
		if (fromDate < serverDate && toDate < serverDate)
		{
			alert("<%=LNG_DATE_IN_THE_PAST %>")
			return false;
		}
		if (fromDate >= toDate)
		{
			alert("<%=LNG_REC_DATE_ERROR %>")
			return false;
		}

		return true;
	}
	else
	{
		fromDate = $.trim($("#start_date_rec").val());
		if(!fromDate)
		{
			alert("<%=LNG_PLEASE_ENTER_START_DATE %>!");
			return false;
		}
		if(!isDate(fromDate,settingsDateFormat)){
			alert("<%=LNG_PLEASE_ENTER_CORRECT_START_DATE %>!")
			return false;
		}
		else
		{
			if (fromDate!="")
			{
				fromDate = new Date(getDateFromFormat(fromDate,settingsDateFormat))
				$("#from_date_rec").val(formatDate(fromDate,saveDbDateFormat));
			}
		}
	}
	if(document.getElementById("radio3").checked)
	{
		var toDate = $.trim($("#end_date_rec").val());
		if(!toDate)
		{
			alert("<%=LNG_PLEASE_ENTER_END_DATE %>!");
			return false;
		}
		if(!isDate(toDate,settingsDateFormat)){
			alert("<%=LNG_PLEASE_ENTER_CORRECT_END_DATE %>!")
			return false;
		}
		else
		{
			if (toDate!="")
			{
				toDate = new Date(getDateFromFormat(toDate,settingsDateFormat))
				$("#to_date_rec").val(formatDate(toDate,saveDbDateFormat));
			}
		}

		if (fromDate < serverDate && toDate < serverDate)
		{
			alert("<%=LNG_DATE_IN_THE_PAST %>")
			return false;
		}
		if (fromDate >= toDate)
		{
			alert("<%=LNG_REC_DATE_ERROR %>")
			return false;
		}
	}
	//check time
	{
		var fromTime = $.trim($("#start_time").val());
		var toTime = $.trim($("#end_time").val());
		
		var uiTimetimeFormat = getUiTimeFormat(settingsTimeFormat);
		
		if(!isDate(fromTime,uiTimetimeFormat))
		{
			alert("<%=LNG_TIME_VALUE_IS_INCORRECT %>")
			return false;
		}
		
		if(!isDate(toTime,uiTimetimeFormat))
		{
			alert("<%=LNG_TIME_VALUE_IS_INCORRECT %>")
			return false;
		}		
		
		fromTime = new Date(getDateFromFormat(fromTime,uiTimetimeFormat));
		toTime = new Date(getDateFromFormat(toTime,uiTimetimeFormat));

		if(fromTime > toTime)
		{
			alert("<%=LNG_REC_DATE_ERROR %>")
			return false;
		}
		
		$("#from_time").val(formatDate(fromTime,saveDbTimeFormat));
		$("#to_time").val(formatDate(toTime,saveDbTimeFormat));
	}
	//checking for values
	if(document.getElementById("dayly").checked==true){
	//dayly: number, less then 365		
		var number_days = document.getElementById("number_days").value;
		if(IsNumeric(number_days)){
			if(number_days>365){
				alert("<%=LNG_NUMBER_OF_DAYS_SHOULD_BE_LESS_THEN %>!");
				return false;
			}
		}
		else{
			alert("<%=LNG_PLEASE_ENTER_NUMBER_OF_DAYS %>!");
			return false;
		}
	}

	if(document.getElementById("weekly").checked==true){
	//weekly: number, less then 100, at least 1 checkbox
		var number_weeks = document.getElementById("number_weeks").value;
		if(IsNumeric(number_weeks)){
			if(number_weeks>100){
				alert("<%=LNG_NUMBER_OF_WEEKS_SHOULD_BE_LESS_THEN %>!");
				return false;
			}
		}
		else{
			alert("<%=LNG_PLEASE_NUMBER_OF_WEEKS %>!");
			return false;
		}
		//checkboxes
		if( document.getElementById("weekday_sunday").checked || document.getElementById("weekday_monday").checked || document.getElementById("weekday_tuesday").checked || document.getElementById("weekday_wednesday").checked || document.getElementById("weekday_thursday").checked || document.getElementById("weekday_friday").checked || document.getElementById("weekday_saturday").checked){
		}
		else{
			alert("<%=LNG_YOU_SHOULD_CHECK_AT_LEAST_ONE_WEEKDAY %>!");
			return false;
		}
	}

	if(document.getElementById("monthly").checked==true){
//	monthly_1: number less then 31, if more then 28 - alert that at last day on some months, 32 - last day
//		   number less then 100
//	monthly_2: number less then 100
		if(document.getElementById("monthly_1").checked){
			var month_day = document.getElementById("month_day").value;
			if(IsNumeric(month_day)){
				if(month_day>32){
					alert("<%=LNG_DAY_SHOULD_BE_LESS_THEN %>!");
					return false;
				}
			}
			else{
				alert("Please enter day!");
				return false;
			}

			var number_month_1 = document.getElementById("number_month_1").value;
			if(IsNumeric(number_month_1)){
				if(number_month_1>100){
					alert("<%=LNG_NUMBER_OF_MONTHS_SHOULD_BE_LESS_THEN %>!");
					return false;
				}
			}
			else{
				alert("<%=LNG_PLEASE_ENTER_NUMBER_OF_MONTHS %>!");
				return false;
			}
		}

		if(document.getElementById("monthly_2").checked){
			var number_month_2 = document.getElementById("number_month_2").value;
			if(IsNumeric(number_month_2)){
				if(number_month_2>100){
					alert("<%=LNG_NUMBER_OF_MONTHS_SHOULD_BE_LESS_THEN %>!");
					return false;
				}
			}
			else{
				alert("<%=LNG_PLEASE_ENTER_NUMBER_OF_MONTHS %>!");
				return false;
			}
		}
	}

	if(document.getElementById("yearly").checked==true){
//	yearly_1: number less then 31, if more then 28 - alert that at last day on some months, 32 - last day
		var month_day = document.getElementById("month_day").value;
		if(IsNumeric(month_day)){
			if(month_day>32){
				alert("<%=LNG_DAY_SHOULD_BE_LESS_THEN %>!");
				return false;
			}
		}
		else{
			alert("<%=LNG_PLEASE_ENTER_DAY %>!");
			return false;
		}
	}
	//occurences
	if(document.getElementById("radio2").checked==true){
		var occur = document.getElementById("end_after").value;
		if(IsNumeric(occur)){
			if(occur>10000){
				alert("<%=LNG_OCCURRENCES_SHOULD_BE_LESS_THEN %>!");
				return false;
			}
		}
		else{
			alert("<%=LNG_PLEASE_ENTER_OCCURRENCES %>!");
			return false;
		}
	}
        

	return true;
}

										function makeWeekdaysSelector(weekday_checked)
										{
											var checked_arr = new Array();
											for(var i=0; i<=6; i++)
											{
												checked_arr[i]="";
												if(i==weekday_checked){
													checked_arr[i]="selected";
												}
											}
											
											var days = new Array("<%=LNG_SUNDAY %>", "<%=LNG_MONDAY %>", "<%=LNG_TUESDAY %>", "<%=LNG_WEDNESDAY %>", "<%=LNG_THURSDAY %>", "<%=LNG_FRIDAY %>", "<%=LNG_SATURDAY %>");
											
											
																					
                                            var firstDayOfWeek = parseInt("<%=ConfFirstDayOfWeek%>", 10);
                                            
                                            var selector = "<select name='weekday_day'>";
                                            for(var i =	firstDayOfWeek; i <= 6; i++)
                                            {
                                                selector += "<option value='" + i  + "'" + checked_arr[i] + ">" + days[i] + "</option>";
                                            }
                                            
                                            for(var i =	0; i < firstDayOfWeek-1; i++)
                                            {
                                                selector += "<option value='" +  i + "'" + checked_arr[i] + ">" + days[i] + "</option>";
                                            }                                            
                                            
                                            selector += "</select>";
                                            
											var days_selector ="<select name='weekday_day'>"+
											"<option value='1' "+checked_arr[1]+"><%=LNG_SUNDAY %></option>"+
											"<option value='2' "+checked_arr[2]+"><%=LNG_MONDAY %></option>"+
											"<option value='3' "+checked_arr[3]+"><%=LNG_TUESDAY %></option>"+
											"<option value='4' "+checked_arr[4]+"><%=LNG_WEDNESDAY %></option>"+
											"<option value='5' "+checked_arr[5]+"><%=LNG_THURSDAY %></option>"+
											"<option value='6' "+checked_arr[6]+"><%=LNG_FRIDAY %></option>"+
											"<option value='7' "+checked_arr[7]+"><%=LNG_SATURDAY %></option>"+
											"</select>";
											
											return selector;
										}

var days_checkboxes ="<input type='checkbox' name='weekday_days' value='Sun' <% response.write isWeekday(week_days, Sun) %> id='weekday_sunday'><label for='weekday_sunday'><%=LNG_SUNDAY %></label> \
<input type='checkbox' name='weekday_days' value='Mon' <% response.write isWeekday(week_days, Mon) %> id='weekday_monday'><label for='weekday_monday'><%=LNG_MONDAY %></label> \
<input type='checkbox' name='weekday_days' value='Tue' <% response.write isWeekday(week_days, Tue) %> id='weekday_tuesday'><label for='weekday_tuesday'><%=LNG_TUESDAY %></label> \
<input type='checkbox' name='weekday_days' value='Wed' <% response.write isWeekday(week_days, Wed) %> id='weekday_wednesday'><label for='weekday_wednesday'><%=LNG_WEDNESDAY %></label><br>\
<input type='checkbox' name='weekday_days' value='Thu' <% response.write isWeekday(week_days, Thu) %> id='weekday_thursday'><label for='weekday_thursday'><%=LNG_THURSDAY %></label> \
<input type='checkbox' name='weekday_days' value='Fri' <% response.write isWeekday(week_days, Fri) %> id='weekday_friday'><label for='weekday_friday'><%=LNG_FRIDAY %></label> \
<input type='checkbox' name='weekday_days' value='Sat' <% response.write isWeekday(week_days, Sat) %> id='weekday_saturday'><label for='weekday_saturday'><%=LNG_SATURDAY %></label>";

function makeMonthSelector(name, month_checked)
{
	var checked_arr = new Array();
	for(var i=1; i<=12; i++)
	{
		checked_arr[i]="";
		if(i==month_checked){
			checked_arr[i]="selected";
		}
	}

	var months_selector ="<select name='"+name+"'>\
<option value='1' "+checked_arr[1]+"><%=LNG_JANUARY %></option>\
<option value='2' "+checked_arr[2]+"><%=LNG_FEBRARY %></option>\
<option value='3' "+checked_arr[3]+"><%=LNG_MARCH %></option>\
<option value='4' "+checked_arr[4]+"><%=LNG_APRIL %></option>\
<option value='5' "+checked_arr[5]+"><%=LNG_MAY %></option>\
<option value='6' "+checked_arr[6]+"><%=LNG_JUNE %></option>\
<option value='7' "+checked_arr[7]+"><%=LNG_JULY %></option>\
<option value='8' "+checked_arr[8]+"><%=LNG_AUGUST %></option>\
<option value='9' "+checked_arr[9]+"><%=LNG_SEPTEMBER %></option>\
<option value='10' "+checked_arr[10]+"><%=LNG_OCTOBER %></option>\
<option value='11' "+checked_arr[11]+"><%=LNG_NOVEMBER %></option>\
<option value='12' "+checked_arr[12]+"><%=LNG_DECEMBER %></option>\
</select>";
	return months_selector;
}

function makeNumSelector(num_checked)
{
	var checked_arr = new Array();
	for(var i=0; i<=4; i++)
	{
		checked_arr[i]="";
		if(i==num_checked){
			checked_arr[i]="selected";
		}
	}

	var numbers_selector="<select name='weekday_place'>\
<option value='1' "+checked_arr[1]+"><%=LNG_1ST %></option>\
<option value='2' "+checked_arr[2]+"><%=LNG_2ND %></option>\
<option value='3' "+checked_arr[3]+"><%=LNG_3RD %></option>\
<option value='4' "+checked_arr[4]+"><%=LNG_4TH %></option>\
<option value='0' "+checked_arr[0]+"><%=LNG_LAST %></option>\
</select>";

	return numbers_selector;
}

function makeDaySelector(day_checked)
{
	var day_selector="<select id='month_day' name='month_day'>";
	for(var i=1; i<=32; i++)
	{
		day_selector += "<option value='" + i + "'";
		if(i==day_checked){
			day_selector += " selected";
		}
		day_selector += ">" + (i == 32 ? "<%=LNG_LAST %>" : i) + "</option>";
	}
	return day_selector + "</select>";
}


function makeEverySelector(name, every_checked)
{
	var checked_arr = new Array();
	for(var i=1; i<=10; i++)
	{
		checked_arr[i]="";
		if(i==every_checked){
			checked_arr[i]="selected";
		}
	}
	var every_selector="<select id='"+name+"' name='"+name+"'>";
	every_selector+="<option value='1' "+checked_arr[1]+"><%=LNG_EVERY %></option>";
	every_selector+="<option value='2' "+checked_arr[2]+"><%=LNG_EVERY %>&nbsp;<%=LNG_2ND %></option>";
	every_selector+="<option value='3' "+checked_arr[3]+"><%=LNG_EVERY %>&nbsp;<%=LNG_3RD %></option>";
	every_selector+="<option value='4' "+checked_arr[4]+"><%=LNG_EVERY %>&nbsp;<%=LNG_4TH %></option>";
	every_selector+="<option value='5' "+checked_arr[5]+"><%=LNG_EVERY %>&nbsp;<%=LNG_5TH %></option>";
	every_selector+="<option value='6' "+checked_arr[6]+"><%=LNG_EVERY %>&nbsp;<%=LNG_6TH %></option>";
	every_selector+="<option value='7' "+checked_arr[7]+"><%=LNG_EVERY %>&nbsp;<%=LNG_7TH %></option>";
	every_selector+="<option value='8' "+checked_arr[8]+"><%=LNG_EVERY %>&nbsp;<%=LNG_8TH %></option>";
	every_selector+="<option value='9' "+checked_arr[9]+"><%=LNG_EVERY %>&nbsp;<%=LNG_9TH %></option>";
	every_selector+="<option value='10' "+checked_arr[10]+"><%=LNG_EVERY %>&nbsp;<%=LNG_10TH %></option>";
	return every_selector + "</select>";
}

<%
if dayly_selector="dayly_2" then
	dayly_selector="dayly_1"
	number_days="1"
end if
%>

function patternChange(pattern_type)
{
	var pattern_div = document.getElementById("pattern_div");

	var dayly_div = "<br/><ol>   \
<li><input id='dayly_1' type='hidden' name='dayly_selector' value='dayly_1'/> <label for='dayly_1'> "+makeEverySelector('number_days', '<%=number_days %>')+" <%=LNG_DAYS %></label></li>\
</ol>";

	var weekly_div = "<br/><ol>   \
<li><%=LNG_RESEND_EVERY %> "+makeEverySelector('number_weeks', '<%=number_weeks %>')+" <%=LNG_WEEK_IN_NEXT_DAYS %>:</li>\
<li>\
"+days_checkboxes+"\
</li>\
</ol>";


	var monthly_div = "<br/><ol>   \
<li><input id='monthly_1' type='radio' value='monthly_1' <% if(monthly_selector="monthly_1") then response.write "checked" end if %> name='monthly_selector'/> <label for='monthly_1'><%=LNG_ON %></label> "+makeDaySelector(<%=month_day %>)+" <%=LNG_OF_MONTH %> "+makeEverySelector('number_month_1', '<%=number_month %>')+" <%=LNG_MONTH %></li>\
<li><input id='monthly_2' type='radio' value='monthly_2' <% if(monthly_selector="monthly_2") then response.write "checked" end if %> name='monthly_selector'/> <label for='monthly_2'><%=LNG_ON %></label> \
" + makeNumSelector(<% response.write weekday_place %>) +" "+ makeWeekdaysSelector(<% response.write weekday_day %>) + "\
<%=LNG_OF_MONTH %> "+makeEverySelector('number_month_2', '<%=number_month %>')+" <%=LNG_MONTH %></li>\
</ol>";

	var yearly_div = "<br/><ol>   \
<li><input id='yearly_1' type='radio' value='yearly_1' <% if(yearly_selector="yearly_1") then response.write "checked" end if %> name='yearly_selector'/> <label for='yearly_1'><%=LNG_ON %></label> "+makeDaySelector(<%=month_day %>)+" <%=LNG_OF_MONTH %> "+makeMonthSelector('month_val_1', <% response.write month_val %>)+" <%=LNG_MONTH %></li>\
<li><input id='yearly_2' type='radio' value='yearly_2' <% if(yearly_selector="yearly_2") then response.write "checked" end if %> name='yearly_selector'/> <label for='yearly_2'><%=LNG_ON %></label> \
" + makeNumSelector(<% response.write weekday_place %>) + " " + makeWeekdaysSelector(<% response.write weekday_day %>) + " <%=LNG_OF_MONTH %> " + makeMonthSelector('month_val_2', <% response.write month_val %>) + "\
 <%=LNG_MONTH %></li>\
</ol>";

	var alertTimeElement = document.getElementById("alert_time");
	var alertRangeElement = document.getElementById("alert_range");
	var alertScheduleElement = document.getElementById("schedule_div");
    //alert("PATTERN = " + pattern_type);
	if(pattern_type=="o")
	{
		pattern_div.innerHTML = "";
		alertScheduleElement.style.display = "";
		toggleDisabled(alertTimeElement, true);
		toggleDisabled(alertRangeElement, true);
		alertTimeElement.style.display = "none";
		alertRangeElement.style.display = "none";
	}
	else
	{
		alertScheduleElement.style.display = "none";
		toggleDisabled(alertTimeElement, false);
		toggleDisabled(alertRangeElement, false);
		alertTimeElement.style.display = "";
		alertRangeElement.style.display = "";
	}

	if(pattern_type=="d")
	{
		pattern_div.innerHTML = dayly_div;
	}

	if(pattern_type=="w")
	{
		pattern_div.innerHTML = weekly_div;
	}

	if(pattern_type=="m")
	{
		pattern_div.innerHTML = monthly_div;
	}

	if(pattern_type=="y")
	{
		pattern_div.innerHTML = yearly_div;
	}
}
</script>

<div width="550">
<table cellspacing=0 cellpadding=0 width="550" border="0">
<tr>
	<td width="140" height="170" nowrap>
		<fieldset id="reccurence_pattern" style="height:170px;width:135px" class="reccurence_pattern">
			<legend><%=LNG_PATTERN %></legend>
			<div style="padding:10px 5px 10px 20px; line-height: 20px;">
				<input id="once" type="radio" value="o" name="pattern"<% if(pattern="o") then response.write " checked" end if %> onclick="patternChange('o');" /><label for="once"><%=LNG_ONCE %></label>
				<br/>
				<input id="dayly" type="radio" value="d" name="pattern"<% if(pattern="d") then response.write " checked" end if %> onclick="patternChange('d');" /><label for="dayly"><%=LNG_DAILY %></label>
				<br/>
				<input id="weekly" type="radio" value="w" name="pattern"<% if(pattern="w") then response.write " checked" end if %> onclick="patternChange('w');"/><label for="weekly"><%=LNG_WEEKLY %></label>
				<br/>
				<input id="monthly" type="radio" value="m" name="pattern"<% if(pattern="m") then response.write " checked" end if %> onclick="patternChange('m');"/><label for="monthly"><%=LNG_MONTHLY %></label>
				<br/>
				<input id="yearly" type="radio" value="y" name="pattern"<% if(pattern="y") then response.write " checked" end if %> onclick="patternChange('y');"/><label for="yearly"><%=LNG_YEARLY %></label>
			</div>
		</fieldset>
	</td>
	<td width="420" height="170" id="options_cell" nowrap>
		<fieldset style="height:170px;width:420px" id='reccurence_div' class='reccurence_div'>
			<legend><%=LNG_OPTIONS %></legend>
			<div width="420" id="pattern_div"></div>
			<div width="420" id="schedule_div" style="padding:2px;">
				<br/><br/>
				<table border="0">
				<tr>
					<td align="right" nowrap="nowrap">&nbsp;<%=LNG_START_DATE %>: </td>
					<td nowrap="nowrap">
						<input name='start_date_and_time' class="date_time_param_input" id="start_date_and_time" type="text" value="<%= FullDateTime(start_date) %>" size="20"/>
						<input name='from_date' id="from_date" type="hidden"/> 
					</td>
				</tr>
				<tr>
					<td align="right" nowrap="nowrap">&nbsp;<%=LNG_END_DATE %>: </td>
					<td nowrap="nowrap">
						<input name='end_date_and_time' class="date_time_param_input" id="end_date_and_time" type="text" value="<%= FullDateTime(end_date) %>" size="20"/> 
						<input name='to_date' id="to_date" type="hidden"/> 
					</td>
				</tr>
				</table>
                <% if Request("alert_campaign") = "" then%>
                <div style="" class="countdown_field" id="countdown_field">
                    <div style="padding: 2px 10px;">
                        <input type='checkbox' name='countdown_enable' value="1" <% if(countdown_enable=1) then response.write "checked" end if %> id='countdown_enable' onclick="countdownEnableChange(true);">
                        <label for='countdown_enable'><%=LNG_COUNTDOWN_ENABLE %></label>
                        <img src="images/help.png" title="<%=LNG_COUNTDOWN_TITLE %>." />
                        <div id="countdown_div" class="countdown_div" style="">
                        </div>
                        <span id="add_countdown" class="jquery_button add_button" style="margin-top: 5px"><%=LNG_ADD %></span>
                    </div>
                </div>
             <%end if %>                                  
			</div>
		</fieldset>
	</td>
</tr>
<tr id="alert_range">
	<td colspan="2" height="120" width="550">
		<fieldset style="padding-bottom:5px;width:558px">
			<legend><%=LNG_RANGE %></legend>
			<table border="0" cellpadding="0" cellspacing="0" class="inside_fieldset" height="100">
			<tr>
				<td valign="top" width="250" rowspan="3" nowrap style="padding:10px">
					<label for="start_date_rec"><%=LNG_START_ON %>:</label>
					<input name='start_date_rec' class="date_param_input" id="start_date_rec" type="text" value="<%= FullDateTime(start_date) %>" size="20"/> 
					<input id="from_date_rec" class="date_param_input" type="hidden" name="from_date_rec"/>
				</td>
				<td nowrap>
					<input id="radio1" type="radio" name="end_type" <% if(end_type="no_date") then response.write "checked" end if %> value="no_date"/><label for="radio1"> <%=LNG_NO_END_DATE %></label>
				</td>
			</tr>
			<tr>
				<td nowrap>
					<input id="radio2" type="radio" name="end_type" <% if(end_type="end_after") then response.write "checked" end if %> value="end_after"/><label for="radio2"> <%=LNG_END_AFTER %>:</label> <input class="end_input" type="text" size="1" name="end_after" id="end_after" value="<% Response.write occurences %>"> <%=LNG_OCCURENCES %>
				</td>
			</tr>
			<tr>
				<td nowrap>
					<input id="radio3" type="radio" name="end_type" <% if(end_type="end_by") then response.write "checked" end if %> value="end_by"/>
					<label for="radio3"> <%=LNG_END_BY %>:</label> 
					<input name='end_date_rec' class="date_param_input" id="end_date_rec" type="text" value="<%= FullDateTime(end_date) %>" size="20"/> 
					<input id="to_date_rec" class="date_param_input" type="hidden" name="to_date_rec"/>
				</td>
			</tr>
			</table>
		</fieldset>
	</td>
</tr>
<tr id="alert_time">
	<td colspan="2" height="100" width="550">
		<fieldset style="padding:5px;width:548px">
			<legend><%=LNG_ALERT_TIME %></legend>
			<table border="0">
			<tr>
				<td nowrap="nowrap" align="right" width="170">
					<%=LNG_START_TIME %>:
				</td>
				<td nowrap="nowrap">
					<input name='start_time' class="time_param_input" id="start_time" type="text" value="<%= FullDateTime(start_date) %>" size="20"/>
					<input name='from_time' id="from_time" type="hidden"/>
				</td>
			</tr>
			<tr>
				<td nowrap="nowrap" align="right" width="170">
					<%=LNG_END_TIME %>:
				</td>
				<td nowrap="nowrap">
					<input name='end_time' class="time_param_input" id="end_time" type="text" value="<%= FullDateTime(end_date) %>" size="20"/>
					<input name='to_time' id="to_time" type="hidden"/>
				</td>
			</tr>
			</table>
		</fieldset>
	</td>
</tr>
</table>

</div>

<%
end function 'reccurenceForm()
%>