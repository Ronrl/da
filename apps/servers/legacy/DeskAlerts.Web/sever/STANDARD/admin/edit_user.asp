﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<!-- #include file="md5.asp" -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>DeskAlerts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/style9.css" rel="stylesheet" type="text/css">
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>


<script language="javascript" type="text/javascript">

	$(document).ready(function() {
		$("#add_user_button").button();
		$("#add_user_button").bind("click", function() {
			if (!$("#user_name").val())
			{
				alert("User name must not be empty!");
			}
			else
			{
				$('#user_form').submit();
			}
		});
		
		$("#change_password_button").button();
		$("#change_password_button").bind("click", function() {
			$('#password_label_tr').show(1000);
			$('#password_value_tr').show(1000);
			$('#password_changed').val("1");
		});
		
		
	});

</script>
</head>
<body style="margin:0" class="<% if Request("wh") <> "1" then response.write "body" %>">
<%


uid = Session("uid")
templateId = Request("temp_id")
if (uid <>"") then

	userId = Request("id")
	save = Request("save")
	retPage = Request("return_page")
	
	addUserText = ""
	addUserButtonText = ""
	if (userId <> "") then
		addUserText = LNG_EDIT_USER
		addUserButtonText = LNG_SAVE
	else
		addUserText = LNG_ADD_USER
		addUserButtonText = LNG_ADD
	end if
	
	userName = ""
	userDisplay = ""
	userEmail = ""
	userMobilePhone = ""
	userPassword = ""
	userDomainId = ""

	set RS_Domain = Conn.Execute("SELECT id FROM domains WHERE name=''")
	if not RS_Domain.EOF then
		non_domain_id = RS_Domain("id")
		non_domain_where = "(domain_id = " & RS_Domain("id") & " OR domain_id IS NULL)"
	else
		non_domain_id = "NULL"
		non_domain_where = "domain_id IS NULL"
	end if

	if (save <> "") then
	
		userName = Request("name")
		userDisplay = Request("display")
		userPassword = Request("password")
		userEmail = Request("email")
		userMobilePhone = Request("mobile_phone")
		userDomainId = Request("domain_id")
		
		if userDomainId = "" then
			userDomainId = non_domain_id
			userDomainWhere = non_domain_where
		else
			userDomainWhere = "domain_id = "& userDomainId
		end if
		
		if CStr(userDomainId) <> CStr(non_domain_id) then
			password = "NULL"
		else
			password = md5(userPassword)
		end if
	
		errMsg=""
		saveSQL=""
		
		set RS = Conn.Execute("SELECT COUNT(1) as cnt FROM users WHERE name=N'"& Replace(userName, "'", "''") &"' AND role='U' AND "& userDomainWhere)
		if NOT RS.EOF then
			if Clng(RS("cnt")) > 0 AND userId = "" then
				if userId = "" then
					errMsg = LNG_USER_NAME_EXIST
				end if
			else
				if userId <> "" then
					passSQL=""
					if Request("password_changed") = "1" then
						passSQL="pass = '"& password &"',"
					end if
					saveSQL = "UPDATE users SET name = N'"& Replace(userName, "'", "''") &"', display_name = N'"& Replace(userDisplay, "'", "''") &"', "& passSQL &" email = N'"& Replace(userEmail, "'", "''") &"', mobile_phone = N'"& Replace(userMobilePhone, "'", "''") &"', domain_id = "& userDomainId &" WHERE id = "&userId
				else
					saveSQL = "INSERT INTO users (name, display_name, reg_date, pass, email, mobile_phone, role, domain_id) VALUES (N'"&Replace(userName, "'", "''")&"', N'"&Replace(userDisplay, "'", "''")&"', GETDATE(), '"& password &"', N'"& Replace(userEmail, "'", "''") &"', N'"& Replace(userMobilePhone, "'", "''") &"', 'U', "& userDomainId &")"
				end if
			end if
		end if
		
		if saveSQL<>"" then
			set RS = Conn.Execute(saveSQL)
			Response.Redirect retPage
		end if
	else
		if userId <> "" then
			set RS =  Conn.Execute("SELECT name, display_name, email, mobile_phone, domain_id FROM users WHERE id = "&userId)
			if not RS.EOF then
				userName = RS("name")&""
				userDisplay = RS("display_name")&""
				userEmail = RS("email")&""
				userMobilePhone = RS("mobile_phone")&""
				userDomainId = RS("domain_id")&""
			end if
		end if
	end if

%>
<% if Request("wh") <> "1" then %>
<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	<tr>
	<td>
	<table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
		<tr>
			<td  height=31 class="main_table_title">
				<a href="#" class="header_title"><%=addUserText%></a>
			</td>
		</tr>
		<tr>
		<td style="padding:5px; width:100%" height="100%"   valign="top" class="main_table_body">
<% end if %>
			<div style="margin:10px;">
				<form name="my_form" method="post" id="user_form">
					<font color="red"><%=errMsg%></font>
					<table style="padding:0px; margin-top:5px; margin-bottom:5px; border-collapse:collapse; border0px; width:100%;">
						<tr>
							<td>
								<label for="user_name"><%=LNG_NAME%>:</label>
							</td>
						</tr>
						<tr>
							<td>
								<input id="user_name" type="text" value="<%=HtmlEncode(userName)%>" name="name"/>
							</td>
						</tr>
						<tr>
							<td>
								<label for="display_name"><%=LNG_DISPLAY_NAME%>:</label>
							</td>
						</tr>
						<tr>
							<td>
								<input id="display_name" type="text" value="<%=HtmlEncode(userDisplay)%>" name="display"/>
							</td>
						</tr>
						<% if userDomainId = "" or CStr(userDomainId) = CStr(non_domain_id) then %>
						<% if userId <> "" then %>
						<tr>
							<td style="padding-top:10px;">
								<input type="hidden" id="password_changed" name="password_changed" value="0"/>
								<a id="change_password_button"><%=LNG_CHANGE_PASSWORD%></a>
							</td>
						</tr>
						<% end if %>
						<tr id="password_label_tr" style="<% if (userId <> "") then Response.Write "display:none;" end if %>">
							<td style="padding-top:10px;">
								<label for="password"><%
									if userId <> "" then 
										Response.Write LNG_ENTER_NEW_PASSWORD & ":"
									else
										Response.Write LNG_PASSWORD & ":"
									end if
								%></label>
							</td>
						</tr>
						<tr id="password_value_tr" style="<% if userId <> "" then Response.Write "display:none;" end if %>">
							<td>
								<input id="password" type="password" value="<%=HtmlEncode(userPassword)%>" name="password"></input>
							</td>
						</tr>
						<% end if %>
						<% if (EM="1") then %>
						<tr>
							<td style="padding-top:10px;">
								<label for="email"><%=LNG_EMAIL%>:</label>
							</td>
						</tr>
						<tr>
							<td>
								<input id="email" type="text" value="<%=HtmlEncode(userEmail)%>" name="email"></input>
							</td>
						</tr>
						<% end if%>
						<% if (SMS="1") then %>
						<tr>
							<td style="padding-top:10px;">
								<label for="mobile_phone"><%=LNG_MOBILE_PHONE%>:</label>
							</td>
						<tr>
						<tr>
							<td>
								<input id="mobile_phone" type="text" value="<%=HtmlEncode(userMobilePhone)%>" name="mobile_phone"></input>
							</td>
						</tr>
						<% end if%>
						<% if AD = 3 then %>
						<tr>
							<td style="padding-top:10px;">
								<label for="domain_id"><%=LNG_DOMAIN%>:</label>
							</td>
						<tr>
						<tr>
							<td>
								<select id="domain_id" name="domain_id">
									<option value=""<%
										if userDomainId = "" or CStr(userDomainId) = CStr(non_domain_id) then Response.Write " selected"
									%>><%=LNG_NO_DOMAIN%></option>
						<%
							set RS = Conn.Execute("SELECT id, name FROM domains")
							do while not RS.EOF
								if RS("name") <> "" then
								%>
									<option value="<%=RS("id")%>"<%
										if CStr(RS("id")) = userDomainId then Response.Write " selected"
									%>><%=HtmlEncode(RS("name"))%></option>
								<%
								end if
								RS.MoveNext
							loop
						%>
								</select>
							</td>
						</tr>
						<% end if%>
						<tr>
							<td style="padding-top:10px;">
								<a id='add_user_button'><%=addUserButtonText%></a>
							</td>
						</tr>
					</table>
					<input type="hidden" name="id" value="<%=userId%>"></input>
					<input type="hidden" name="save" value="1"></input>
				</form>
			</div>
<% if Request("wh") <> "1" then %>
		</td>
	</tr>
</table>
<% end if
else
	Response.Redirect "index.asp"
end if
%>
<!-- #include file="db_conn_close.asp" -->