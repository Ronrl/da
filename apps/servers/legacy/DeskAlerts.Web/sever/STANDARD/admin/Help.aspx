﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Help.aspx.cs" Inherits="DeskAlertsDotNet.Pages.Help" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link href="css/style.css" rel="stylesheet" type="text/css">
		<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>

    		<script type="text/javascript" language="javascript">
			$(function() {
				var $win = $(window);
				var $body = $(document.body);
				if ($win.width() >= $body.width() && $win.height() >= $body.height()) {
					$body.attr("scroll", "auto");
				}
			});
		</script>

</head>
<body class="helpBody" scroll="yes">
    <form id="form1" runat="server">
    <div>
	<% if( Request["about"] == "BuyAddons" ) { %>
		<h1>Add-ons store</h1>
		<p>On this page you will find a list of all DeskAlerts extensions and services available. If you are interested in some of them, select needed items by clicking the "Order" buttons near the description.</p>
		<p>After you are done with picking the items, click on the "Send your order" button. This will open an e-mail agent with composed message to our sales department.</p>
		<p>Send an e-mail and our qualified sales manager will contact you soon.</p>
		<p>For any sales questions please call: 1.703.881.3166 or <a href="mailto:sales@deskalerts.com">mailto:sales@deskalerts.com</a></p>
	<% } else if ( Request["about"] == "CreateVideo" ) { %>
		<h1>Video Alerts creation</h1>
		<p>Video alerts are a type of desktop pop-ups built around video content. On this page, you will find an interface that enables you to quickly build a notifications using a video file on your machine, video uploaded by different users, or an external video, such as ones on YouTube</p>
		<p>First of all, you need to determine which video source you&#39;re going to use. If you have a video on your machine, you can drag and drop it into a highlighted area on the first tab of this page. It will take some time for it to upload - you will be notified when the process is over. You can also use videos uploaded by different DeskAlerts publishers by picking them from the list on the right.</p>
		<p>If you want to use an Internet video, like from YouTube, you need to find a Share/Embed section on a page where it is located, then copy the embed code and paste it into the corresponding field on the second tab of this page. Notice that your end users should have unrestricted Internet connection to view this video.</p>
		<h2>Tailoring the video</h2>
		<p>Before you send out your video alert, it is recommended for you to use a Preview button (Hotkey: Alt+P) to view how it will look like on user's side. You might need to tweak the message width/height so it will appear without the scrollbars. You can also supply an optional text description which will be displayed above the video player in a final notification.</p>
	<% } else if ( Request["about"] == "VideoAlertsSent" ) { %>
		<h1>Sent Video alerts</h1>
		<p>This page contains list of all video alerts which have been sent from control panel. You can specify how many records should be displayed in one page by clicking numbers above the table.</p>
		<h3>Actions</h3>
		<p>For every alert in the table there are few actions which can be done:
		<ul>
			<li>Duplicate alert - use this if you want to send the same alert one more time, may be with some changes or to different recipients. Clicking this will send you to alert creation page with
			everything set as if you just composed it. List of recipients stay the same too, but you can change everything you need.</li>
			<li>Stop / Start scheduled alert. If you stop some scheduled alert, clients will not be receiving it until you click &quot;Start scheduled alert&quot; button, which appears instead of stop.</li>
			<li>Preview alert. Works pretty much the same as preview button in alerts editor. If you click this, alert will appear inside your browser page, enabling you to see how it will be looking on recipient display.</li>
			<li>Direct link. Clicking this will open pop-up window on the browser page containing a link which you can send to anyone. If this person have an access to your server, they will see the page with
			alert preview on it.</li>
			<li>Alert report. This will bring up a basic report on the users who have been targeted with this message, who have received it and who have missed it.</li>
			<li>Reschedule alert. Clicking this enables you to change schedule pattern or another parameters of scheduled alert. Don't forget to hit &quot;Save&quot; button, or changes will be lost.</li>
		</ul>
		<h3>Adding alerts</h3>
		<p>&quot;Add alert&quot; button will send you to alert editor. If you create alert, select recipients and hit &quot;Send&quot; button, new alert record will appear on this page on top of the table.
		If in alert editor you click &quot;Save&quot; button instead of &quot;Save &amp; Next&quot;, record will appear on &quot;Draft alerts&quot; page.</p>
		<h3>Deleting alerts</h3>
		<p>Checkboxes near every table row are for multiple alerts deleting. Just select all messages you want to permanently delete and click &quot;Delete&quot; button above the table</p>
	<% } else if ( Request["about"] == "VideoAlertsDraft" ) { %>
		<h1>Draft Video Alerts</h1>
		<p>If you have saved a video alert, but have not yet sent it, it will appear in the list of Drafts. You can specify how many records should be displayed in one page by clicking numbers above the table.</p>
		<h3>Actions</h3>
		<p>For every alert in the table there are few actions which can be done:
		<ul>
			<li>Send alert - click this to send this draft. DeskAlerts will help you make sure you have identified recipients for this message, and then will send it out. The message will move from the Drafts list to Sent list</li>
			<li>Edit alert - click this to open the rich text editor and make further edits to this draft. You can then save it again or send it.</li>
			<li>Preview alert. Works pretty much the same as preview button in alerts editor. If you click this, alert will appear inside your browser page, enabling you to see how it will be looking on recipient display.</li>
			<li>Direct link. Clicking this will open pop-up window on the browser page containing a link which you can send to anyone. If this person have an access to your server, they will see the page with
			alert preview on it.</li>
		</ul>
		<h3>Adding alerts</h3>
		<p>&quot;Add alert&quot; button will send you to alert editor. If you create alert, select recipients and hit &quot;Send&quot; button, new alert record will appear on &quot;Sent alerts&quot; on top of the table.
		If in alert editor you click &quot;Save &amp; Next&quot; button instead of &quot;Save&quot;, record will appear on this page.</p>
		<h3>Deleting alerts</h3>
		<p>Checkboxes near every table row are for multiple alerts deleting. Just select all messages you want to permanently delete and click &quot;Delete&quot; button above the table</p>
	<% } else if ( Request["about"] == "DigitalSignageLinks" ) { %>
		<h1>Digital Signage Devices</h1>
		<p>Digital signage links are convenient tools to send notifications to the devices that lack user interaction or cannot host a DeskAlerts client application, but still can run a web browser, such as Smart TVs or digital screens.</p>
		<p>Generate a link by giving it a distinctive name, specify the content turnover rate and open that link on your digital screen.</p>
		<p>Now you should be able to create a DeskAlerts notifications and send them to generated links using the Send Content shortcuts.</p>
		<h2>Managing links content</h2>
		<p>To view the content currently being displayed in a specific link, click on its Name. From there, you are will be able to stop/resume the content displaying. To send more notifications, use the Digital Signage -> Send Content item in the main menu on the left.</p>
	<% } else if ( Request["about"] == "Channels" ) { %>
		<h1>Notification channels</h1>
		<p>On this page you will find a list of most popular additional DeskAlerts commuinication channels. If you are interested in some of them, contact DeskAlers sales for a quote.</p>
		<p>For any sales questions please call: 1.703.881.3166 or <a href="mailto:sales@deskalerts.com">leave us a mail</a></p>
	<% } else if ( Request["about"] == "CampaignsList" ) { %>
		<h1>Campaigns List</h1>
		<p>On this page you can see a list of all (if any) campaigns created in the system. Campaigns are a convenient way to group you notification content according to a specific topic.</p>
		<p>For each campaign in the list, you can view its unique name, date of creation and current status - either it is running, finished or is scheduled to start on a specific date.</p>
		<h2>Campaign use cases</h2>
		<p>Campaigns should be used when you need to send a several messages on a specific occasion, and all these messages can be created beforehand.</p>
		<p>Let's say your company is having a celebration of some local company-wide holiday. Date is known beforehand and you want to prepare everything in advance.</p>
		<p>You can create a campaign named &quot;Company holiday&quot;, schedule it for needed date and add there:</p>
		<ul>
		<li>A desktop notificaiton with congratulations to all employees</li>
		<li>An employee survey for management asking about their thoughts on holiday organization</li>
		<li>A new wallpaper to boost employees engagement</li>
		</ul>
		<p>Then just save the campaign and feel prepared. All the messages will be delivered on their schedule, but not earlier than the campaign starts.</p>
	<% } else if ( Request["about"] == "CampaignContent" ) { %>
		<h1>Campaign Content</h1>
		<p>This page is a heart of the campaign - here you can view its current status and manage its content</p>
		<h2>Adding a content</h2>
		<p>To add something to the campaign, click the &quot;Add content&quot; button on the top right. You will be presented with a dialog window prompting you to select a content type. Then, just proceed with the creation as you would usually do for this type, refer to help if needed.</p>
		<h2>Performing group actions</h2>
		<p>Campaigns engine enables you to perform a group actions on a multiple content items at once. You can delete several messages from the campaign or change the audience for them by checking corresponding rows in the content table and clicking &quot;Delete&quot; or &quot;Change recipients&quot; buttons directly above or below the table.</p>
		<h2>Launching a campaign</h2>
		<p>Usually, campaigns are created with a specific start date. In this case, when you create a campaign, you will see a message on top of this page, saying when the campaign will start automatically. If you haven't specified the start date, you need to launch the campaign manually when you've done creating its content. To launch the campaign manually, use &quot;Launch Campaign&quot; button.</p>
	<% } else if ( Request["about"] == "WebPlugin" ) { %>
		<h1>Web Plugin Info</h1>
		<p>To use this plugin, you can choose one of the cases:</p><br />
        <p>Case 1. Embedding .js code into existing page</p>
        <ol>
            <li>Generate JScode</li>
            <li>Copy JScode and paste it in your PHP/HTML page</li>
            <li>Download Web Plugin PHP file</li>
            <li>Put this file in the same folder with your PHP/HTML file</li>
        </ol><br />
        <p>Case 2. Using new separate .js file</p>
        <ol>
            <li>Generate JScode</li>
            <li>Copy JScode and paste it in your .js file</li>
            <li>Include your .js file in your PHP/HTML page</li>
            <li>Download Web Plugin PHP file</li>
            <li>Put this file in same folder with .js file</li>
            <li>Cut the last line from the .js file and paste it into your php/html web page</li>
        </ol>
    <% } else if ( Request["about"] == "ColorCodes" ) { %>
		<h1>Color Codes</h1>
		<p>Inspired by the various hospitals emergency codes systems, DeskAlerts emergency notifications module allows you to create your own color codes system or to input an existing one.</p>
		<p>Create the color codes for all kinds of situations. You'll be able to specify the code during the creation of Instant Send message.</p>
		<p>When the Instant Send message is created using the color code, the tile, which is used for sending the message out, will have a distinctive color, saving the sender a few extra seconds when he needs to inform the workforce.</p>
    <% } else if ( Request["about"] == "LanguagesSettings" ) { %>
		<h1>Message languages</h1>
		<p>On this page, you can manage the list of languages supported by a multi-lingual alerts feature. If there is more than one language here, every time you will try to create an alert, you will face additional step of selecting the message languages.</p>
		<p>Default language is a language the most messages will be composed in. Built-in automatic translation feature will enable you to translate you default message to other languages. Note that the translation quality is not perfect and determined only by Microsoft translator performance</p>
		<h2>Adding a language</h2>
		<p>When you&#39;re adding a language, pay attention to the &quot;Language code&quot; property. Specifying correct code is essential for the feature to perform correctly. Relevant list of ISO 2 language codes can be found on the Internet.</p>
    <% } else if ( Request["about"] == "Feedback" ) { %>
		<h1>Feedback</h1>
		<p>Feedback is a feature to send short tickets from a DeskAlerts desktop agents. These messages can be later viewed on this page by an administrator or a publisher with the feedback viewing rights. The Feedback feature is widely used during the test rollout period, in order to gather data on system performance, possible issues and the user experience.</p>
	<% } else if ( Request["about"] == "TrialAddons" ) { %>
		<h1>Trial Modules Info</h1>
		<p>If you are reading this page, your DeskAlerts package was supplied with some modules working in free trial mode. This mode is usually no longer than two weeks and it gives you a chance to try some features you might have missed.</p>
		<p>If you are interested in purchasing some of these modules full versions, you can always use the &quot;Buy Modules&quot; button in the menu to order them.</p>
	<% } else if ( Request["about"] == "RequestQuote" ) { %>
		<h1>Quote Request</h1>
		<p>On this page you will find a list of all DeskAlerts extensions and services available. If you are interested in some of them, select needed items by clicking the "Order" buttons near the description.</p>
		<p>After you are done with picking the items, click on the "Send your order" button. This will open an e-mail agent with composed message to our sales department.</p>
		<p>Send an e-mail and our qualified sales manager will contact you soon.</p>
		<p>For any sales questions please call: 1.703.881.3166 or <a href="mailto:sales@deskalerts.com">mailto:sales@deskalerts.com</a></p>
	<% } else if ( Request["about"] == "RequestDemo" ) { %>
		<h1>DeskAlerts Trial Request</h1>
		<p>If you need a trial package for trying product in your corporate environment and demonstrating DeskAlerts functionality to top management, this feature enables you to contact our sales department for getting one.</p>
	<% } else if ( Request["about"] == "Dashboard" ) { %>
		<h1>Dashboard</h1>
		<p>Dashboard is the page containing valuable statistics and shortcuts to the most used pieces of functionality represented as widgets.</p>
		<h2>Widgets</h2>
		<p>Widgets can be added by clicking the <strong>&quot;Add Widget&quot;</strong> button in the top left corner of dashboard. By hovering your mouse over the widget tile you may see the short description of a widget functionality.</p>
		<p>Once added, widget may or may not require entering the additional parameters. Widgets can also be minimizes or closed</p>
		<p><strong>Note:</strong> adding too many widgets to your Dashboard will slow down the page loading process.</p>
		<p>By default, three widgets are added to the Dashboard of any user.</p>
		<h2>Activity Monitor</h2>
		<p>Activity monitor displays the real-time statistics or users receiving the alerts. The activity level is usually somewhere about zero, until the messages are sent out to users.</p>
		<h2>Create Alert</h2>
		<p>Create Alerts widget supplies a shortcut to the most used functionality of DeskAlerts - creating desktop alerts. If you don't have the rights to do this - widget will be unavailable to you</p>
		<h2>Calendar</h2>
		<p>The Calendar widget displays the content scheduled for the current month. In every date table cell you may see some links. Clicking the &quot;Created&quot; link will lead you to the list of content created on this day. &quot;Show Scheduled&quot; link will allow you to view the content scheduled for this specific date. If any of these links, or both of them, are not presented - it means that there is no content created on/scheduled for this date.</p>
    <% } else if ( Request["about"] == "DashboardList" ) { %>
		<h1>Alerts </h1>
		<p>ALerts table displays the scheduled or the created content depending on the day you have chosen in the Dashboard calendar. The day is displayed in the header. </p>
		<p>Click on <strong>Return</strong> button to return to the Dashboard.</p>	
	<% } else if ( Request["about"] == "InstantMessages" ) { %>
		<h1>Instant Messages</h1>
		<p>Instant messages are the quickest way to send pre-defined content. Unlike the message templates and alert drafts, these messages already have lists of recipients and can be sent in one click. To get the emergency alert id for API, hover your mouse over the alert title.</p>
	<% } else if ( Request["about"] == "NewSurveyCreation" ) {%>	
		<h1>Creating Surveys</h1>
		<p>While alerts are good at getting messages out to your team, surveys are excellent at getting the responses you need 
		to key questions.</p> 
		<p>Each survey consists of questions and a series of answers that the recipient can choose from. You may schedule your 
		survey to be sent at appointed date and time as well as expired after end date.</p>
		<p>With our wizard you can easily create surveys in three steps.</p>
		<h2>Step One - Choose Survey type</h2>
		<p>There are four types of DeskAlerts surveys available for different situations:
		<ul>
			<li>Simple Survey. This one is a sequence of questions with wide range of question types. Some of them may be answered with free text, some may have multiple answers, some may ask
			user if he wants to proceed with the questions or end survey. Answer statistics are available only for Control Panel users.</li>
			<li>Quiz. In this survey type, every answer option is marked by editor as correct or not. Answer statistics are available only for Control Panel users.</li>
			<li>Poll. This is like a simple survey, but answer statistics are displayed to user after he is done with the questions.</li>
		</ul>
		</p>
		<h2>Step Two - Composing the Survey</h2>
		<h3>Specifying name and schedule</h3>
		<p>To compose the survey, you should specify the name and questions. If you want to specify start and end dates for your survey, check &quot;Schedule survey&quot; box before creating questions.</p>
		<h3>Change survey appearance</h3>
		<p>You`re able to change survey`s width and height. Make it resizable, change survey`s position on screen or make it fullscreen. Click on survey appearance section and set up options you need.</p>
		<h3>Change survey`s skin</h3>
        <p>Selecting skin affects appearance of your message's title section and border color.</p>
		<p>Usually skins are used for alerts differentiation depending on message priority.</p>
		<p>Some skins are available by default and additional can be ordered to better suit your company style.</p>
		<p>You can view all available skins by clicking on the skin preview to the right of title editor.</p>
    <% } else if ( Request["about"] == "CreateQuestion" ) {%>
    	<h1>Creating questions</h1>
		<p>Question functionality is determined by question type. Select one from dropdown menu and click &quot;Preview&quot; button to see what it will look like.</p>
		<p>By default, every question has two answer options (if you haven't chosen Essay box question type). You can add more options by clicking &quot;Add option&quot; label under the
		option text fields.</p>
	<% } else if ( Request["about"] == "SurveysCreate" ) { %>
		<h1>Creating Surveys</h1>
		<p>While alerts are good at getting messages out to your team, surveys are excellent at getting the responses you need 
		to key questions.</p> 
		<p>Each survey consists of questions and a series of answers that the recipient can choose from. You may schedule your 
		survey to be sent at appointed date and time as well as expired after end date.</p>
		<p>With our wizard you can easily create surveys in three steps.</p>
		<h2>Step One - Choose Survey type</h2>
		<p>There are three types of DeskAlerts surveys available for different situations:
		<ul>
			<li>Simple Survey. This one is a sequence of questions with wide range of question types. Some of them may be answered with free text, some may have multiple answers, some may ask
			user if he wants to proceed with the questions or end survey. Answer statistics are available only for Control Panel users.</li>
			<li>Quiz. In this survey type, every answer option is marked by editor as correct or not. Answer statistics are available only for Control Panel users.</li>
			<li>Poll. This is like a simple survey, but answer statistics are displayed to user after he is done with the questions.</li>
		</ul>
		</p>
		<h2>Step Two - Composing the Survey</h2>
		<h3>Specifying name and schedule</h3>
		<p>To compose the survey, you should specify the name and questions. If you want to specify start and end dates for your survey, check &quot;Schedule survey&quot; box before creating questions.</p>
		<h3>Creating questions</h3>
		<p>Question functionality is determined by question type. Select one from dropdown menu and click &quot;Preview&quot; button to see what it will look like.</p>
		<p>By default, every question has two answer options (if you haven't chosen Essay box question type). You can add more options by clicking &quot;Add option&quot; label under the
		option text fields.</p>
		<h3>Viewing questions</h3>
		<p>Before proceeding to the next question or selecting recipients, you may click &quot;Preview&quot; button again to look on your composed question. If you have added multiple qustions, you
		can navigate between them by pressing &quot;Previous question&quot; and &quot;Next question&quot; links above the question type menu.</p>
		<p>When done with creating questions sequence, click &quot;Save &amp; Next&quot; button to go to recipients selection.</p>
		<h2>Step Three - Selecting Recipients</h2>
		<p>As for every other type of content, you can broadcast your survey or send it to specific users using organization structure or IP groups.</p>
	<% } else if ( Request["about"] == "SurveysArchived" ) { %>
		<h1>Archived Surveys</h1>
		<p>This list contains past surveys, which are not accepting responses any more. Survey moves to Archived list if it was stopped from Active list or if end date specified in schedule has passed.</p>
		<h2>Actions</h2>
		<p>Available actions for every survey on the list are following:
		<ul>
			<li>Clone survey. Creates the same survey with the same recipients list. You can change anything and then send survey again.</li>
			<li>View survey results. Clicking this will open a new window with answer statistics for every question. By clicking percentages near the bars, you can get a list of users selected specific option.</li>
		</ul>
		</p>
		<h2>Deleting surveys</h2>
		<p>To delete one or more surveys, check the boxes on the left of their table records and press &quot;Delete&quot; button above or below the table.</p>
	<% } else if ( Request["about"] == "SurveysActive" ) { %>
		<h1>Active Surveys</h1>
		<p>This list contains active surveys, which are accepting responses at the time. Survey moves to this list right after it was sent.</p>
		<h2>Actions</h2>
		<p>Available actions for every survey on the list are following:
		<ul>
			<li>Clone survey. Creates the same survey with the same recipients list. You can change anything and then send survey again.</li>
			<li>View survey results. Clicking this will open a new window with answer statistics for every question. By clicking percentages near the bars, you can get a list of users selected specific option.
			Remember: surveys in this list are still active and statistics may change.</li>
			<li>Stop survey. After you'll click this, survey will stop receiving answers and will be moved to Archived list.</li>
		</ul>
		</p>
		<h2>Adding surveys</h2>
		<p>You can access survey creation wizard by clicking &quot;Add&quot; button.</p>
		<h2>Deleting surveys</h2>
		<p>To delete one or more surveys, check the boxes on the left of their table records and press &quot;Delete&quot; button above or below the table.</p>
	<% } else if ( Request["about"] == "SystemConfiguration" ) { %>
		<h1>System Configuration</h1>
		<p>On this page, you can done a number of actions affecting message delivery and control panel look.</p>
		<h2>Alert appearance settings</h2>
		<p>By checking &quot;Enable alert title styling&quot; you're enabling feature which allows you to customize font options in alert's title.</p>
		<p>&quot;Show postpone button in alert preview&quot; radio buttons affect preview of alerts with &quot;Acknowledgement&quot; parameter checked. <strong>Note:</strong> same effect can be
		achieved in client by setting proper parameters in installation wizard</p>
		<h2>Message delivery settings</h2>
		<p>These options determine system actions while sending alerts to users, who are logged in from multiple computers.</p>
		<h2>E-mail settings</h2>
		<p>Here you can specify what name and e-mail address will be shown to anyone who receives e-mail alerts.</p>
		<h2>Active Directory and eDirectory options</h2>
		<p>These two boxes are important if you have some users not included into you AD/ED domains.</p>
		<h2>Language options</h2>
		<p>Here you can choose your control panel interface language or even add a new one. If you done full translation of control panel for some language not on the list,
		you can send it to us and may be it will be included in later versions.</p>
		<h2>Date and time options</h2>
		<p>Here you can choose date and time formats, first day of the week and specify session timeout value.</p>
		<h2>Alert editor customization</h2>
		<p>Our editor has many tools for filling alert with different content, but you can always disable some instruments which seem redundant to you. To do so, click on icons which you don't need.
		Icons marked with red cross will not appear in alerts editor.</p>
		<p>After done with settings, don't forget to click &quot;Save&quot; button, or your changes will be discarded.</p>
	<% } else if ( Request["about"] == "BlogSettings" ) { %>
		<h1>Blog settings</h1>
		<p>Blog add-on allows you to post alert messages on your WordPress-powered blogs. To do so, you must specify some obligatory options like:</p>
		<ul>
			<li>Blog name - choose whatever you want, it is used only for blog differentiation</li>
			<li>Blog URL - address of your blog, something like &quot;deskalerts.wordpress.com&quot;</li>
			<li>Username - a name you usually use to log into your blog dashboard</li>
			<li>Password - password fitting your login</li>
			<li>Blog ID - usually it equals 1, if not, you can fing it in the blog dashboard</li>
		</ul>
		<p>Also this add-on supports multiple additional options, such as:</p>
		<ul>
			<li>Option to save message as draft</li>
			<li>Option to disable comments on posts sent to this blog</li>
			<li>Option to post alert as a whole new page of your blog</li>
			<li>Option to sign post as one of blog authors. <strong>Note:</strong> if this string is not a valid blog author name,
			publish attempt will fail.</li>
		</ul>
		<h2>Note to publishers</h2>
		<p>Some WordPress blogs can override alert styles (font color, size, table decorations etc), so double-checking your posting results
		may be a good idea.</p>
	<% } else if ( Request["about"] == "SocialLinkedInSettings" ) { %>
		<h1>LinkedIn Settings</h1>
		<p>LinkedIn network updates posting feature requires some preparation.Basic steps are following:</p>
		<ul>
			<li>Log In to LinkedIn Developers Network</li>
			<li>Create a new application with the right settings</li>
			<li>Copy and API Key to this page and save the settings</li>
		</ul>
		<h2>Sharing alerts on LinkedIn</h2>
		<p>When you&#39;re done with the settings, go to alert creation page and look below the skin thumbnail. If you are already logged to LinkedIn, there would be &quot;Share on LinkedIn&quot; button. Pressing this will check if you&#39;ve saved the alert and then send it to LinkedIn. If you&#39;re not logged yet, there would be login button.</p>
		<h2>LinkedIn Application Live Status</h2>
		<p>If you want to check this functionality without your LinkedIn Network noticing, set the application Live Status to Development so the updates will be visible only for this Application&#39;s list of developers.</p>
	<% } else if ( Request["about"] == "SocialMediaSettings" ) { %>
		<h1>Twitter Settings</h1>
		<p>DeskAlerts uses Twitter API to post messages to Twitter.</p>
		<p>To start tweeting with DeskAlerts, you should specify any name for this Twitter instance and provide four access tokens.</p>
		<h2>Getting Consumer Key and Consumer Secret</h2>
		<p>To obtain any Twitter access tokens, you should create an application. This can be done easily with following steps:</p>
		<ul>
			<li>Go to <a href="https://dev.twitter.com/apps/new" target="_blank">Twitter developer site</a> and log in, if necessary.</li>
			<li>Supply the necessary required fields, accept the TOS, and solve the CAPTCHA.</li>
			<li>Submit the form</li>
			<li>Copy the consumer key (API key) and consumer secret from the screen into corresponding fields at DeskAlerts Control Panel</li>
		</ul>
		<h2>Getting Access tokens</h2>
		<p>Now, from your app settings, you can obtain OAuth tokens:</p>
		<ul>
			<li>Ensure that your application is configured correctly with the permission level you need (read-only, read-write, read-write-with-direct messages).</li>
			<li>On the application&#39;s detail page, invoke the &quot;Your access token&quot; feature to automatically negotiate the access token at the permission level you need.</li>
			<li>Copy the indicated access token and access token secret from the screen into your DeskAlerts Control Panel</li>
		</ul>
	<% } else if ( Request["about"] == "DefaultSettings" ) { %>
		<h1>Default Settings</h1>
		<p>This page enables you to adjust your preferences for sending alerts.</p>
		<p>Checking some control here means that every time you try to create an alert, it will be already checked in alert editor. This saves you some time.</p>
		<p>After adjusting options, don't forget to click &quot;Save&quot; button, or all changes will be discarded.</p>
	<% } else if ( Request["about"] == "DataArchiving" ) { %>
		<h1>Data Archiving</h1>
		<p>In order to decrease the load on your servers, this feature has been implemented to clear or store old alerts from 
		SQL databases in archives.</p>
		<h2>Scripting instructions</h2>
		<p>Set the time range for the actions as required, then go /inetpub/AdminScripts/ where you will find 
		DeskAlerts_archive.vbs file (to be generated upon installation of DeskAlerts Server).</p>
		<p>This file is responsible for date checks and clearing or moving alerts to archive tables when needed. Using this file 
		you have to create a task in Windows Scheduler to run the script once a day or week as you wish.</p>
		<h2>Archived alerts status</h2>
		<p>Archived alerts will be excluded from Send/Receive process on the server, but still can be seen in Sent folder and 
		Statistics tab of Control Panel.</p>
	<% } else if ( Request["about"] == "ProfileSettings" ) { %>
		<h1>Profile Settings</h1>
		<p>Profile Settings page allows you to:</p>
		<ul>
			<li>Change your login</li>
			<li>Change your password</li>
			<li>Add e-mail or phone as additional contacts</li>
		</ul>
		<p>Don't forget to click &quot;Save&quot; button when you're finished, otherwise, all changes will be discarded.</p>
	<% } else if ( Request["about"] == "WallpapersStatistics" ) { %>
		<h1>Wallpapers Statistics</h1>
		<p>This display summarizes information about wallpapers, starting with the one most recently sent.</p>
		<h2>Table fields</h2>
		<ul>
			<li>Alerts - contain the name of wallpaper. By clicking it you opening a new window with wallpaper details. There you can view the list of
			users who had Received or Not received this wallpaper, by clicking on digit link below the diagram</li>
			<li>Creation date</li>
			<li>Sent date</li>
			<li>Send by - who sent the alert</li>
			<li>Recipients - who should receive the wallpaper. If this item appearing as the link, click it to see the new window with all recipients list in table form</li>
			<li>Open Ratio - the ratio between sent and received wallpapers</li>
		</ul>
		<h2>Specifying date range</h2>
		<p>You can make the display more precise by specifying a date range in the fields at the top right.</p>
	<% } else if ( Request["about"] == "ScreensaversStatistics" ) { %>
		<h1>Screensavers Statistics</h1>
		<p>This display summarizes information about screensavers, starting with the one most recently sent.</p>
		<h2>Table fields</h2>
		<ul>
			<li>Alerts - contain the name of screensaver. By clicking it you opening a new window with screensaver details. There you can view the list of
			users who had Received or Not received this screensaver, by clicking on digit link below the diagram</li>
			<li>Creation date</li>
			<li>Sent date</li>
			<li>Send by - who sent the alert</li>
			<li>Recipients - who should receive the screensaver. If this item appearing as the link, click it to see the new window with all recipients list in table form</li>
			<li>Open Ratio - the ratio between sent and received screensavers</li>
		</ul>
		<h2>Specifying date range</h2>
		<p>You can make the display more precise by specifying a date range in the fields at the top right.</p>
	<% } else if ( Request["about"] == "RSSStatistics" ) { %>
		<h1>RSS Statistics</h1>
		<p>This display summarizes information about RSS alerts, starting with the one most recently sent.</p>
		<h2>Table fields</h2>
		<ul>
			<li>Alerts - contain the title of alert. By clicking it you opening a new window with alert details. There you can view the list of
			users who had Received or Not received this alert, by clicking on digit link below the diagram</li>
			<li>Creation date</li>
			<li>Sent date</li>
			<li>Send by - who sent the alert</li>
			<li>Recipients - who should receive the alert. If this item appearing as the link, click it to see the new window with all recipients list in table form</li>
			<li>Open Ratio - the ratio between sent and received alerts</li>
		</ul>
		<h2>Specifying date range</h2>
		<p>You can make the display more precise by specifying a date range in the fields at the top right.</p>
	<% } else if ( Request["about"] == "SurveysStatistics" ) { %>
		<h1>Surveys Statistics</h1>
		<p>This display summarizes information about surveys, starting with the one most recently sent.</p>
		<p>Type column helps you to determine type of sent survey. Hover your mouse over the icon to know was it a Quiz, Poll or usual Survey.</p>
		<p>If item in &quot;Recipients&quot; column appears as the link, click it to see the new window with all recipients list in table form. Open Ratio in the table is ratio
		 between sent and received surveys.</p>
		<h2>Getting answers statistics</h2>
		<p>To view survey details click on the specific survey name. In survey details you may track survey results. Click digit link near specific 
		answer to view the list of users with such answer.</p>
		<p>You also may view the list of users that had Received, Not received, Voted or Not voted, by clicking on digit link below the 
		diagram.</p>
	<% } else if ( Request["about"] == "UsersStatistics" ) { %>
		<h1>Users Statistics</h1>
		<p>This display summarizes user information, ordered by username. You can change records ordering by clicking table headers.</p>
		<p>Click on username in the table to see user details. There you can view the list of alerts that were Received, Acknowledged or Not received by current user,
		by clicking on digit link below the diagram.</p>
		<p>User status can be the following:</p>
		<ul>
			<li><img src="images/online.gif"/> on-line</li>
			<li><img src="images/standby.gif"/> standby</li>
			<li><img src="images/offline.gif"/> off-line</li>
			<li><img src="images/disabled.gif"/> disabled</li>
		</ul>
		<h2>Specifying date range</h2>
		<p>You can make the display more precise by specifying a date range in the fields at the top right.</p>
	<% } else if ( Request["about"] == "AlertsStatistics" ) { %>
		<h1>Alerts Statistics</h1>
		<p>This display summarizes information about alerts, starting with one most recently sent.</p>
		<h2>Table fields</h2>
		<ul>
			<li>Alerts - contain the title of alert. By clicking it you opening a new window with alert details. There you can view the list of
			users who had Received, Acknowledged or Not received this alert, by clicking on digit link below the diagram</li>
			<li>Creation date</li>
			<li>Sent date</li>
			<li>Send by - who sent the alert</li>
			<li>Recipients - who should receive the alert. If this item appearing as the link, click it to see the new window with all recipients list in table form</li>
			<li>Open Ratio - the ratio between sent and received alerts</li>
		</ul>
		<h2>Specifying date range</h2>
		<p>You can make the display more precise by specifying a date range in the fields at the top right.</p>
	<% } else if ( Request["about"] == "OverallStatistics" ) { %>
		<h1>Overall Statistics</h1>
		<p>Here you can see a range of information on DeskAlerts users, alerts, and surveys. DeskAlerts uses an unobtrusive 
		technology, Smart Activity Tracking (SAT) to learn what happens to each alert and survey you send out. </p>
		<p>You can find out not just how many alerts were sent, but also how many have been received and not yet 
		read - and how many users just dismissed the alert without reading it.</p>
		<h2>Specifying date range</h2>
		<p>You can make the display more precise by specifying a date range in the fields at the top right.</p>
		<h2>Charts legend</h2>
		<p>Alerts chart contains information about:</p>
		<ul>
			<li>Received: The total number of alerts that have been received</li>
			<li>Acknowledged: The total number of alerts that have been acknowledged (user closed alert by pressing submitting button)</li>
			<li>Not received: The total number of alerts that haven’t been received</li>
		</ul>
		<p>Users chart contains information about:</p>
		<ul>
			<li>On-line: The number of users who are currently on-line</li>
			<li>Standby: The number of users in standby mode</li>
			<li>Off-line: The number of users who are currently off-line</li>
			<li>Disabled: The number of users who disabled DeskAlerts Client. In this case, user will be receiving only alerts marked as urgent</li>
		</ul>
	<% } else if ( Request["about"] == "OverallStatistics"  ) { %>
		<h1>Statistics Dashboard</h1>
		<p>This page enables you to create your own set of statistics-related widgets. Choose from the selection of widget templates and tune them to best fit your needs.</p>
		<p>The widget sets are stored individually for each user, so you are creating the interface for personal use.</p>
	<% } else if ( Request["about"] == "MessageTemplates" ) { %>
		<h1>Message Templates</h1>
		<p>Message templates are usually created to reduce the time of alert creation. You may add templates for different situations (staff reminder, system outage, etc).</p>
		<h2>Creating message templates</h2>
		<p>To create a message template, click &quot;Add template&quot; button, enter alert title and contents, then click &quot;Save&quot;. You can use &quot;Preview&quot; button during
		alert composing or from the templates table to see what you've created.</p>
		<h2>Using message templates</h2>
		<p>To create an alert using message template, you can use button in the Actions column. Another way is to use Alerts/Create as usually and select message template there using dropdown list.</p>
		<h2>Editing message templates</h2>
		<p>To edit message template, use corresponding button in the Actions column. This will enable you to change template name and contents. Don't forget to save your changes.</p>
		<h2>Deleting message templates</h2>
		<p>To delete one or more message templates, check corresponding boxes and click &quot;Delete&quot; button above or below the table.</p>
	<% } else if ( Request["about"] == "Policies" ) { %>
		<h1>Policies</h1>
		<p>Policy is a Control Panel User&#39;s scope of activity.</p>
		<h2>Creating a policy</h2>
		<p>To create a new policy, click on the &quot;Add Policy&quot; button.</p>
		<h2>Editing the policy</h2>
		<p>If you want to edit some policy, click corresponding icon in the &quot;Actions&quot; column. If you want to have old policy saved, click &quot;Duplicate&quot; button first and
		then edit copy, which appeared in the table.</p>
		<h2>Deleting policies</h2>
		<p>To delete one or more policies, check corresponding boxes in the table and click &quot;Delete&quot; button above or below the table.</p>
	<% } else if ( Request["about"] == "PolicyEdit" ) { %>
		<h1>Composing the Policy</h1>
		<p>There are two types of policies in DeskAlerts Control Panel:
		<ul>
			<li>System administrator will have the Full rights as Admin by default. You may define an unlimited number of Administrators for one Control Panel. </li>
			<li>Editor will have a limited scope of activity as defined by Administrator by using checkboxes and list of users added.</li>
		</ul>
		<p>If you're creating System Administrator policy, just choose policy name and hit &quot;Save&quot;.</p>
		<p>If you're creating a policy for Editor, you should specify his rights and list of recipients. By default, list of recipients include entire organization, but you can limit
		it however you need in &quot;List of recipients&quot; tab.</p>
		<p>Rights are determined by checking specific boxes in &quot;Access control list&quot; tab. For example, if you want to create editor policy which enables him to work with message templates only,
		check the boxes in &quot;Message Templates&quot; table row.</p>
	<% } else if ( Request["about"] == "LanguageEdit" ) { %>
	<h1>Translating the Control Panel interface</h1>
	<p>This page enables you to create custom translations of Control Panel interface.</p>
	<p>Every new translation is based on the English one, so you can perform the translation step by step.</p>
	<p>By entering a new value instead of English one, you are translating a piece of UI somewhere in Control Panel.</p>
	<p>After making the changes, don&#39;t forget to save them and refresh the Control Panel page.</p>
	<% } else if ( Request["about"] == "EditorsPublishers" ) { %>
		<h1>Editors/Publishers</h1>
		<h2>DeskAlerts Editor Role</h2>
		<p>DeskAlerts Editor is a person who manage user groups that will receive messages; design the message templates; create, send and 
		track alerts and surveys.</p>
		<h2>Adding an editor</h2>
		<p>Before creating a new editor, make sure you have an appropriate policy created for him. To learn more about policies, go to &quot;Policies&quot; menu section.</p>
		<p>To add an editor to the list, click &quot;Add editor/publisher&quot; button, choose login and password for editor, select policy from dropdown list and click &quot;Add&quot; button.</p>
		<h2>Editing an editor</h2>
		<p>You can access editor editing screen by pressing corresponding icon in &quot;Actions&quot; column. Here you can change his name, policy or password.</p>
		<h2>Deleting an editor</h2>
		<p>Do delete one or more editor records from the table, check corresponding boxes and press &quot;Delete&quot; button above or below the table.</p>
	<% } else if ( Request["about"] == "OrganizationPage" ) { %>
		<h1>Organizations Page</h1>
		<p>Organization page provides you a quick access to information about all the available domains in tree-structure form. From here, you can add users and create groups from them. New domains are created
		by synchronizations with Actice Directory or eDirectory if you have installed a corresponding add-on. You also can search for user, group or computer by name.</p>
		<h2>Object types</h2>
		<p>There are three object types available from the dropdown list at the top left.</p>
		<ul>
			<li>Users. Shows you every user in organization. For every users some actions are available, such as viewing user details, editing his information or deleting him from the organization.</li>
			<li>Groups. Some groups are created by synchronization process, some can be added by you from this page. For each group, table shows number of users in it, along with registered computers,
			child and parent groups. By clicking digit links you can see full list of them. For every group available actions are editing and deleting.</li>
			<li>Computers. This shows the list of registered in DeskAlerts database computers, with only action to view computer details.</li>
		</ul>
		<h2>Adding users and groups</h2>
		<p>To add user or group, click buttons on the top right of your working area. DeskAlerts will guide you through a simple forms and make sure that you fill all the required fields.</p>
	<% } else if ( Request["about"] == "AddUser" ) { %>
		<h1>Add new user</h1>
		<p>You can add a user to organization structure manually. Just fill in all the fields you need. After you click on the &quot;Add&quot; button, the wizard will warn you if some required fields are empty.</p>
	<% } else if ( Request["about"] == "AddGroup" ) { %>
		<h1>Add new group</h1>
		<p>To add a new group, you must specify the unique name. If the name is already taken, you will be notified.</p>
		<p>After you will specify the name and click on &quot;Save &amp; Next&quot; button, the empty group will be created and you will be redirected to the group members selection page.</p>
	<% } else if ( Request["about"] == "IPGroupEdit" ) { %>
		<h1>Composing the IP Group</h1>
		<p>Every group must have a title and at least one object in it. Available types of objects are single IP address or IP addresses range. You can add these objects by clicking
		corresponding links. When you finished composing the IP group, press &quot;Add&quot; button and the new group will appear in the list.</p>
	<% } else if ( Request["about"] == "GroupUsers" ) { %>
		<h1>Edit group members</h1>
		<p>Group can contain users, computers, OUs and sub-groups within it.</p>
		<p>To add them, select the tab with needed object type on the left, check the boxes near  objects you want to include in group and press &quot;Add&quot; arrow button.</p>
		<p>After you are done with this, save your changes.</p>
	<% } else if ( Request["about"] == "Synchronizations" ) { %>
		<h1>Synchronizations</h1>
		<p>This page contains a list of all created synchronizations with Active Directory / Novell eDirectory. Such synchronizations used for keeping your DeskAlerts users list relevant. To add a synchronization to the list, click on &quot;Add Synchronization&quot; button.</p>
		<h2>Starting synchronizations</h2>
		<p>To start specific synchronization manually, click on corresponding icon in the table record. Usually, synchronization is a scheduled task which run automatically. To
		specify this schedule, click icon for synchronization editing, check there auto-synchronization option and choose whatever schedule pattern you need.</p>
	<% } else if ( Request["about"] == "ADSync" ) { %>
		<h1>Active Directory synchronization</h1>
		<h2>Obligatory parameters</h2>
		<p>First, you should enter domain name (with dots, e.g. your.domain.net), username of user that is in Domain Users group and his password. This is enough for creating a new synchronization, but
		you can also take a look at additional options.</p>
		<h2>Additional options</h2>
		<p>By checking the box in first section you can tell the system to use secure connection during synchronization.</p>
		<p>Controls in the second section giving you the possibility to choose, which Organizational Units and Groups to synchronize with DeskAlerts.</p>
		<p>By checking the boxes in third section, you can tell the system to:</p>
		<ul>
			<li>Remove some users from DeskAlerts database if you not selected them this time</li>
			<li>Import disabled users</li>
			<li>Import computers. Computers will be imported as separated groups</li>
			<li>Enable scheduled synchronization. After checking this, some additional controls will appear so you can choose the synchronizations frequency</li>
		</ul>
	<% } else if ( Request["about"] == "EdirSync" ) { %>
		<h1>eDirectory synchronization</h1>
		<p>For successful eDirectory synchronization, you'll need to disable TLS on your LDAP server.</p>
		<h2>Disabling TLS</h2>
		<p>To disable TLS, follow the instructions:</p>
		<ol>
			<li>In Novell iManager, click <strong>Roles and Tasks</strong> button</li>
			<li>Click LDAP &gt; LDAP Overview &gt; View LDAP Groups</li>
			<li>Click the LDAP Group object, and then click <strong>Information</strong> on General tab</li>
			<li>Uncheck <strong>Require TLS for Simple Binds with Password</strong></li>
			<li>Click <strong>Apply</strong>, and then click <strong>OK</strong></li>
		</ol>
		<h2>Creating synchronization</h2>
		<p>To create new eDirectory synchronization, you should specify:</p>
		<ul>
			<li>Name or address of your LDAP server with disabled TLS (see instructions above)</li>
			<li>DN of user that will be used by DeskAlerts to access eDirectory</li>
			<li>User password</li>
		</ul>
	<% } else if ( Request["about"] == "TemplateEdit" ) { %>
		<h1>Message Template Editor</h1>
		<p>Here you can edit the content that will be pasted in every alert using this message template.</p>
		<p>Name of the message template will also serve as the default title for alerts using it.</p>
	<% } else if ( Request["about"] == "Domains" ) { %>
		<h1>Domains</h1>
		<p>This page contains list of all created domains.</p>
		<p>Self-Registered users domain is presented here by default, more domains can be added using synchronization tool.</p>
		<p>To learn more about it,
		click &quot;Synchronization&quot; button and follow DeskAlerts Help tips.
	<% } else if ( Request["about"] == "IPGroups" ) { %>
		<h1>IP Groups</h1>
		<p>This page contains list of every created custom IP group. You can use search field to find them by name.</p>
		<h2>Usage</h2>
		<p>IP groups is another way of selecting users for alerts sending, working for every registration type, like self-registering or Active Directory synchronization.</p>
		<p>To send alert or another piece of content to users in specific IP group, select &quot;IP groups&quot; option from dropdown menu at selecting recipients step.</p>
		<h2>Creating IP groups</h2>
		<p>To add new IP Group to list, press &quot;Add group&quot;button.</p>
		<h2>Actions</h2>
		<p>There are following actions available for every IP group in the list:
		<ul>
			<li>Edit IP group. This enables you to add, change or delete objects included in specific group.</li>
			<li>View IP group. This action isn't displayed in the Actions column and can be done by clicking IP group title. This will open a new window with list of all objects included in the group</li>
		</ul>
		</p>
		<h2>Deleting IP groups</h2>
		<p>To delete one or more IP groups you should check boxes at the left of corresponding table records and click &quot;Delete&quot; button above or below the table.</p>
	<% } else if ( Request["about"] == "WallpapersDraft" ) { %>
		<h1>Draft Wallpapers</h1>
		<p>This page contains table with all draft wallpapers.</p>
		<h2>Actions</h2>
		<p>There are following actions available for every wallpaper in the list:</p>
		<ul>
			<li>Send wallpaper. Clicking this will send you to &quot;Select Recipients&quot; step of wallpaper creating. After choosing recipients, click the &quot;Send&quot; button and wallpaper will move from the Draft list to Current list.</li>
			<li>Edit wallpaper. Clicking this will send you to first step of wallpaper creating, allowing you to make changes before selecting recipients and sending wallpaper.</li>
		</ul>
		<h3>Adding wallpapers</h3>
		<p>&quot;Add wallpaper&quot; button will send you to wallpaper editor. If you create one, select recipients and hit &quot;Send&quot; button, new record will appear on &quot;Current wallpapers&quot; page on top of the table.
		If wallpaper creating process will not be completed, record will appear in Drafts list.</p>
		<h3>Deleting wallpapers</h3>
		<p>Checkboxes near every table row are for multiple wallpapers deleting. Just select all items you want to permanently delete and click &quot;Delete&quot; button above the table</p>
	<% } else if ( Request["about"] == "WallpapersCurrent" ) { %>
		<h1>Current Wallpapers</h1>
		<p>This page contains table with all current wallpapers.</p>
		<h2>Actions</h2>
		<p>There are following actions available for every wallpaper in the list:</p>
		<ul>
			<li>Start/Stop wallpaper. Stopping the wallpaper will prevent its appearing until you start it again. If no DeskAlerts wallpapers are active, user wallpaper will be restored.</li>
			<li>Duplicate wallpaper. After clicking this you will be sent to wallpaper editor and given possibility to change wallpaper image and/or list of recipients.
				After sending this changed wallpaper, a new item will appear in current wallpapers list. If you will not specify recipients, it will be saved in drafts list.</li>
			<li>Preview wallpaper. This will open a new window to see how wallpaper looks on user's screen.</li>
		</ul>
		<h3>Adding wallpapers</h3>
		<p>&quot;Add wallpaper&quot; button will send you to wallpaper editor. If you create one, select recipients and hit &quot;Send&quot; button, new record will appear on &quot;Current wallpapers&quot; page on top of the table.
		If wallpaper creating process will not be completed, record will appear in Drafts list.</p>
		<h3>Deleting wallpapers</h3>
		<p>Checkboxes near every table row are for multiple wallpapers deleting. Just select all items you want to permanently delete and click &quot;Delete&quot; button above the table</p>
	<% } else if ( Request["about"] == "WallpapersCreate" ) { %>
		<h1>Creating Wallpapers</h1>
		<p>Wallpaper Add-on allows you to use DeskAlerts to publish corporate desktop wallpapers. Corporate wallpaper is less 
		intrusive but effective corporate communications channel. Communicate regarding upcoming changes or news, 
		establish internal brand or publish product announcements right to the employee desktop wallpaper.</p>
		<p>Publish your messages as corporate desktop wallpapers, use scheduling and sequence your wallpapers if you want 2 
		or more messages to rotate. All advanced targeting options including delivery to users/groups or any combination of 
		those is supported.</p>
		<h2>Step One - Choosing Image</h2>
		<p>DeskAlerts wallpapers support such image formats as jpg, png, bmp and gif. Please ensure that picture you have selected have resolution high enough to fit user's screen.
		You may click &quot;Preview&quot; button to see how wallpapers will look on the user's desktop.</p>
		<h2>Step Two - Selecting Recipients</h2>
		<p>After selecting the image, click &quot;Save &amp; Next&quot; button to proceed to selecting recipients. If you don't select recipients, wallpaper will be saved in Drafts list.
		Otherwise, after clicking the &quot;Send&quot; button it will appear in Current wallpapers list.</p>
	<% } else if ( Request["about"] == "AlertsCreating" ) { %>
		<h1>Creating Alerts</h1>
		<h2>Record video message</h2>
		<p>Click on the button <img src="img/webcam.gif"> and you can record video message from your webcam<br/>
		Then click on the button <img src="img/insert_video.gif"> and choose video from your computer after recording and saving video from your webcam. You can also click here on the record button to record your video message.</p>
		<h2>Selecting Skin</h2>
		<p>Selecting skin affects appearance of your message's title section and border color.</p>
		<p>Usually skins are used for alerts differentiation depending on message priority.</p>
		<p>Some skins are available by default and additional can be ordered to better suit your company style.</p>
		<p>You can view all available skins by clicking on the skin preview to the right of rich text editor on step One.</p>
		<h2>Step One - Composing the message</h2>
		<h3>Selecting alert type</h3>
		<p>On the top of your working area, there are three tabs representing three types of alerts:
		<ul>
			<li>Usual alert - simple message with any information you put in it.</li>
			<li>Scrolling ticker - your message formatting will be truncated and alert will appear at the bottom of desktop in less obtrusive manner.</li>
			<li>RSVP - like usual alert, but with possibility to add simple questions and receive feedback.</li>
		</ul>
		</p>
		<h3>Using Message Templates</h3>
		<p>At the top right corner of your working area there is a dropdown list of available message templates. When you choose one, alert title and content are automatically replaced
		by ones specified in template. To manage such templates, see &quot;Message Templates&quot; section in the menu.</p>
		<h3>Filling alert with content</h3>
		<p>Below the tabs you see the rich text editor to create alert. If you have enabled &quot;title styling&quot; option, another editor will appear for title.</p>
		<p>This editor has a wide range of features that allow you to format the text of your messages, insert smiles or images, 
			and adjust background and text colors to suit your message. Hover your mouse over each of the icons to learn what 
			features it controls.</p>
		<p><strong>Note:</strong> The wide range of features in this editor can tempt you to create an alert that is so complex and ornate that it 
			is hard to read in the window in which alerts display on users' computers. Since the goal is to convey information, 
			one or two formatting and decorative touches will usually be sufficient.</p>
		<h3>Additional options</h3>
		<p>Below the text editor there are additional controls: 
		<ul>
			<li>Urgent alert are urgent - they will appear on client's machine even if DeskAlerts client is disabled. Also they will always be positioned on top of any other alerts.</li>
			<li>Acknowledgement alerts require reading confirmation - alerts of this type cannot be closed with default &quot;close&qout; button and provide extended statistics.</li>
			<li>If you want to send the alert at a particular time or create recurrence alert, check the Schedule alert checkbox. Multiple schedule patterns are available.</li>
		</ul>
		</p>
		<h3>Adjusting alert type</h3>
		<p>To the right from this controls, you can choose alert type:
		<ul>
			<li>Desktop alert - selected by default, this will create Pop up window with specified size.</li>
			<li>E-mail alert (available if you have E-mail add-on installed) - send a copy of notification to you employee's e-mail.</li>
			<li>SMS alert (available if you have SMS add-on installed) - truncated copy of alert will be sent to your employee's phone.</li>
			<li>Blog post (available if you have Blog add-on installed) - post your alert contents to Wordpress-powered blog.</li>
		</ul>
		</p>
		<h3>Button controls</h3>
		<p>At the bottom right, you can see a three buttons:
		<ul>
			<li>Preview button - by clicking this you can check how will you alert look on recipient's desktop.</li>
			<li>Save button - clicking this will save your alert to Draft section without sending it to anyone. You can choose recipients and send your message later.</li>
			<li>Save &amp; Next button - clicking on it will send you to Step Two of creating alerts - selecting recipients.</li>
		</ul>
		</p>
	<% } else if ( Request["about"] == "AlertUsers" ) { %>
		<h2>Selecting Recipients</h2>
		<p>This is the page for selecting recipients for the content you just created. Drop-down menu suggests you three ways:
		<ul>
			<li>Broadcasting the message. In this case, content will be received by every user in your organization.</li>
			<li>Selecting recipients. Choosing this option will enable you to pick specific users and groups from organization.</li>
			<li>Selecting IP Groups. To create such groups, see &quot;IP Groups&quot; item in menu.</li>
		</ul>
		</p>
		<p>After you done with choosing recipients, hit &quot;Send&quot; button.</p>
	<% } else if ( Request["about"] == "AlertsDraft" ) { %>
		<h1>Alerts Draft</h1>
		<p>If you have saved an alert, but have not yet sent it, it will appear in the list of Drafts. You can specify how many records should be displayed in one page by clicking numbers above the table.</p>
		<h3>Actions</h3>
		<p>For every alert in the table there are few actions which can be done:
		<ul>
			<li>Send alert - click this to send this draft. DeskAlerts will help you make sure you have identified recipients for this message, and then will send it out. The message will move from the Drafts list to Sent list</li>
			<li>Edit alert - click this to open the rich text editor and make further edits to this draft. You can then save it again or send it.</li>
			<li>Preview alert. Works pretty much the same as preview button in alerts editor. If you click this, alert will appear inside your browser page, enabling you to see how it will be looking on recipient display.</li>
			<li>Direct link. Clicking this will open pop-up window on the browser page containing a link which you can send to anyone. If this person have an access to your server, they will see the page with
			alert preview on it.</li>
		</ul>
		<h3>Adding alerts</h3>
		<p>&quot;Add alert&quot; button will send you to alert editor. If you create alert, select recipients and hit &quot;Send&quot; button, new alert record will appear on &quot;Sent alerts&quot; on top of the table.
		If in alert editor you click &quot;Save &amp; Next&quot; button instead of &quot;Save&quot;, record will appear on this page.</p>
		<h3>Deleting alerts</h3>
		<p>Checkboxes near every table row are for multiple alerts deleting. Just select all messages you want to permanently delete and click &quot;Delete&quot; button above the table</p>
		<h3>Alert types icons</h3>
		<p>&quot;Type&quot; column is representing whether alert is unobtrusive, rsvp, ticker, fullscreen or usual (in this order). If alert is urgent, it has an effect on icon too. For example,
		if you have sent urgent unobtrusive ticker, in the table it will be marked as &quot;urgent unobtrusive&quot;, because &quot;unobtrusive&quot; option priority is higher than &quot;ticker&quot;.</p>
		</p>
	<% } else if ( Request["about"] == "AlertsSent" ) { %>
		<h1>Alerts Sent</h1>
		<p>This page usually contains list of all alerts which have been sent from control panel. You can specify how many records should be displayed in one page by clicking numbers above the table.</p>
		<h3>Actions</h3>
		<p>For every alert in the table there are few actions which can be done:
		<ul>
			<li>Duplicate alert - use this if you want to send the same alert one more time, may be with some changes or to different recipients. Clicking this will send you to alert creation page with
			everything set as if you just composed it. List of recipients stay the same too, but you can change everything you need.</li>
			<li>Stop / Start scheduled alert. If you stop some scheduled alert, clients will not be receiving it until you click &quot;Start scheduled alert&quot; button, which appears instead of stop.</li>
			<li>Preview alert. Works pretty much the same as preview button in alerts editor. If you click this, alert will appear inside your browser page, enabling you to see how it will be looking on recipient display.</li>
			<li>Direct link. Clicking this will open pop-up window on the browser page containing a link which you can send to anyone. If this person have an access to your server, they will see the page with
			alert preview on it.</li>
			<li>Alert report. This will bring up a basic report on the users who have been targeted with this message, who have received it and who have missed it.</li>
			<li>Reschedule alert. Clicking this enables you to change schedule pattern or another parameters of scheduled alert. Don't forget to hit &quot;Save&quot; button, or changes will be lost.</li>
		</ul>
		<h3>Adding alerts</h3>
		<p>&quot;Add alert&quot; button will send you to alert editor. If you create alert, select recipients and hit &quot;Send&quot; button, new alert record will appear on this page on top of the table.
		If in alert editor you click &quot;Save&quot; button instead of &quot;Save &amp; Next&quot;, record will appear on &quot;Draft alerts&quot; page.</p>
		<h3>Deleting alerts</h3>
		<p>Checkboxes near every table row are for multiple alerts deleting. Just select all messages you want to permanently delete and click &quot;Delete&quot; button above the table</p>
		<h3>Alert types icons</h3>
		<p>&quot;Type&quot; column is representing whether alert is unobtrusive, rsvp, ticker, fullscreen or usual (in this order). If alert is urgent, it has an effect on icon too. For example,
		if you have sent urgent unobtrusive ticker, in the table it will be marked as &quot;urgent unobtrusive&quot;, because &quot;unobtrusive&quot; option priority is higher than &quot;ticker&quot;.</p>
	<% } else if ( Request["about"] == "ScreensaversDraft" ) { %>
		<h1>Draft Screensavers</h1>
		<p>This page contains table with all draft screensavers.</p>
		<h2>Actions</h2>
		<p>There are following actions available for every screensaver in the list:</p>
		<ul>
			<li>Send screensaver. Clicking this will send you to &quot;Select Recipients&quot; step of screensaver creating. After choosing recipients, click the &quot;Send&quot; button and screensaver will move from the Draft list to Current list.</li>
			<li>Edit screensaver. Clicking this will send you to first step of screensaver creating, allowing you to make changes before selecting recipients and sending screensaver.</li>
		</ul>
		<h3>Adding screensavers</h3>
		<p>&quot;Add screensaver&quot; button will send you to screensaver editor. If you create one, select recipients and hit &quot;Send&quot; button, new record will appear on &quot;Current screensavers&quot; page on top of the table.
		If screensaver creating process will not be completed, record will appear in Drafts list.</p>
		<h3>Deleting screensavers</h3>
		<p>Checkboxes near every table row are for multiple screensavers deleting. Just select all items you want to permanently delete and click &quot;Delete&quot; button above the table</p>
	<% } else if ( Request["about"] == "ScreensaversCurrent" ) { %>
		<h1>Current Screensavers</h1>
		<p>This page contains table with all current screensavers.</p>
		<h2>Actions</h2>
		<p>There are following actions available for every screensaver in the list:</p>
		<ul>
			<li>Start/Stop screensaver. Stopping the screensaver will prevent its appearing until you start it again.</li>
			<li>Duplicate screensaver. After clicking this you will be sent to screensaver editor and given possibility to change contents and/or list of recipients.
				After sending this changed screensaver, a new item will appear in current screensavers list. If you will not specify recipients, it will be saved in drafts list.</li>
			<li>Preview screensaver. This will open a new window to see how screensaver looks on user's screen.</li>
		</ul>
		<h3>Adding screensavers</h3>
		<p>&quot;Add screensaver&quot; button will send you to screensaver editor. If you create one, select recipients and hit &quot;Send&quot; button, new record will appear on &quot;Current screensavers&quot; page on top of the table.
		If screensaver creating process will not be completed, record will appear in Drafts list.</p>
		<h3>Deleting screensavers</h3>
		<p>Checkboxes near every table row are for multiple screensavers deleting. Just select all items you want to permanently delete and click &quot;Delete&quot; button above the table</p>
	<% } else if ( Request["about"] == "ScreensaversCreate" ) { %>
		<h1>Creating Screensavers</h1>
		<p>Corporate screensaver is one of the less intrusive but powerful ways to communicate with desktops of your 
		employees. DeskAlerts screensaver work same way as ordinary PC screensaver and allows for targeted content 
		delivery.</p>
		<h2>Step One - Choose content source</h2>
		<p>DeskAlerts provides you three ways of creating screensavers:</p>
		<ul>
			<li>Importing from PowerPoint. To use MS PowerPoint presentation as screensaver, you need to save it as .wmv file and upload it to control panel. If you choose this way,
			Deskalerts will give you step-by-step instructions and video manual if needed.</li>
			<li>Using image as screensaver. This is the easiest way, you just need to upload high resolution image in one of the accepted formats (jpg, png, bmp or gif). Image will be scaled to
			fit each user's screen.</li>
			<li>Creating HTML screensaver. In this case, you'll be able to create screensaver with our built-in WYSIWYG editor, just like usual alerts. These screensavers may contain images,
			text, clickable links, FLASH, video and even more.</li>
		</ul>
		<h2>Step Two - Compose screensaver</h2>
		<p>This step depending on your choice of content source:</p>
		<ul>
			<li>If you're creating screensaver from PowerPoint presentation, follow the instructions to create video file and upload it in control panel.</li>
			<li>If you're creating screensaver from image, just select image and upload it.</li>
			<li>If you're decided to create your own HTML screensaver, feel free to use wide selection of tools in our editor to create screensaver that suits your need the best.</li>
		</ul>
		<p>Also you need to specify screensaver name. After done all of the above, click &quot;Preview&quot; button to see how your screensaver will look like.</p>
		<p>It may be useful to visit &quot;Options&quot; tab where you can specify displaying time, which means a lot if you're sending multiple screensavers to one user.</p>
		<p>When you're done with screensaver creation, click &quot;Save &amp; Next&quot; button to select recipients and send screensaver. If you won't select recipients, screensaver will
		be saved automatically in the Drafts list.</p>
		<h2>Step Three - Select recipients</h2>
		<p>DeskAlerts screensavers, just like every other types of messages, can be broadcasted, sent to specific users or to selected IP Groups.</p>
		<p>After you click &quot;Send&quot; button, screensaver will be available from the Current screensavers menu section.</p>
	<% } else if ( Request["about"] == "RSSDraft" ) { %>
		<h1>Draft RSS</h1>
		<p>This page contains table with all draft RSS Feeds.</p>
		<h2>Actions</h2>
		<p>There are following actions available for every RSS in the list:</p>
		<ul>
			<li>Send RSS. Clicking this will send you to &quot;Select Recipients&quot; step of RSS creating. After choosing recipients, click the &quot;Send&quot; button and RSS will move from the Draft list to Current list.</li>
			<li>Edit RSS. Clicking this will send you to first step of RSS creating, allowing you to make changes before selecting recipients and sending RSS.</li>
		</ul>
		<h3>Adding RSS</h3>
		<p>&quot;Add RSS&quot; button will send you to RSS editor. If you create RSS, select recipients and hit &quot;Send&quot; button, new record will appear on &quot;Current RSS&quot; page on top of the table.
		If RSS creating process will not be completed, RSS will appear in Draft RSS list.</p>
		<h3>Deleting RSS</h3>
		<p>Checkboxes near every table row are for multiple RSS deleting. Just select all feeds you want to permanently delete and click &quot;Delete&quot; button above the table</p>
	<% } else if ( Request["about"] == "RSSCurrent" ) { %>
		<h1>Current RSS</h1>
		<p>This page contains table with all current RSS Feeds.</p>
		<h2>Actions</h2>
		<p>There are following actions available for every RSS in the list:</p>
		<ul>
			<li>Start/Stop alerts sending. By clicking corresponding button in the Actions column you can stop sending alerts from this feed or start it again.</li>
			<li>Duplicate RSS. After clicking this you will be sent to RSS editor and given possibility to change some alert display or scheduling parametets and/or list of recipients.
				After sending this changed RSS, a new item will appear in current RSS list. If you will not specify recipients, RSS will be saved in drafts list.</li>
			<li>View created RSS alerts. This action isn't displayed in Actions column and is called by clicking RSS title in the &quot;Name&quot; column.</li>
		</ul>
		<h3>Adding RSS</h3>
		<p>&quot;Add RSS&quot; button will send you to RSS editor. If you create RSS, select recipients and hit &quot;Send&quot; button, new record will appear on &quot;Current RSS&quot; page on top of the table.
		If RSS creating process will not be completed, RSS will appear in Draft RSS list.</p>
		<h3>Deleting RSS</h3>
		<p>Checkboxes near every table row are for multiple RSS deleting. Just select all feeds you want to permanently delete and click &quot;Delete&quot; button above the table</p>
	<% } else if ( Request["about"] == "RSSCreate" ) { %>
		<h1>Creating RSS</h1>
		<p>RSS add-on allows you to feed news, headlines, videos from any RSS enabled source for instance SharePoint, 
		Exchange, websites, blogs, etc. Make your organization follow your corporate news or any other internal or external 
		entries.</p>
		<p>Configure RSS feed source and select recipients from your Active Directory or target to specific computers/users or 
		groups. All RSS entries will be delivered as prominent desktop alerts or scrolling desktop ticker. Specify validity 
		period for each of your RSS channels so that notification will only be available between specific dates.</p>
		<h2>Step One - Selecting Source and message appearance</h2>
		<p>On the first screen, among other controls, you can see two required text fields: Title and RSS Feed. In Title field you can enter any name which identifies source of alerts,
		for example, &quot;Corporate News Feed&quot;. In another field, you should enter valid URL of your RSS Feed/Media RSS Feed. After filling these fields, you are ready to go.</p>
		<p>Also you can set up additional options, such as message's lifetime, start and end dates or display type.</p>
		<p>To check appearance of your RSS messages, click &quot;Preview&quot; button.</p>
		<p>When finished, hit &quot;Save &amp; Next button to go to selecting recipients</p>
		<h2>Step Two - Selecting Recipients</h2>
		<p>Just like for every other message type, you can broadcast your RSS alerts, select specific recipients or use IP Groups, which you can set in &quot;IP Groups&quot; menu section.</p>
		<p>If you won't select recipients, RSS will be saved in drafts list.</p>
	<% } else if ( Request["about"] == "Shortcut" ) { %>
        <h1>Creating shortcut</h1>
		<p>When it comes to emergency notification, you don&#39;t want to lose a second before sending. That&#39;s what notification shortcuts are for.</p>
		<h2>Step by step guide</h2>
		<p>Before you can use the desktop shortcuts, you need to perform a following operations:</p>
		<ul>
			<li>Select the message you need to create a shortcut for, click on &quot;Create Shortcut&quot;</li>
			<li>If you don&#39;t have a notification sending tool yet, download it from the first link in a note displayed right after the shortcut creation.</li>
			<li>Download the shortcut using the second link and put it in the same folder as the sending tool.</li>
			<li>Default DeskAlerts sending shortcuts can&#39;t have different icons, so you probably want to create a usual Windows shortcut to the DeskAlerts shortcut file to place it on a desktop, assign a convenient name and icon to it</li>
		</ul>
		<p>When this operation is repeated for all emergency messages you need, you should have a folder somewhere with DeskAlertsAPI.exe and DeskAlerts shortucts and several Windows shortucts on your desktop which will be used to send out a notification.</p>
		<h2>Use case</h2>
		<p>In an organization where employees are allowed to send out notifications in case of emergency, an administrator can perform the actions described above and then push the shortcuts and the tools folder to anyone. Then, in case of emergency, employees won't have to remember any credentials or publisher console URL, all they will have to do is to double-click a desktop icon.</p>
    <% } else if ( Request["about"] == "" ) { %>
		<h1>There is no help about this page</h1>
	<%}%>   
    </div>
    </form>
</body>
</html>
