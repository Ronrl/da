	<script type="text/javascript">
		var tree;
		
		function getCheckedOus (id, action)
		{			
			if( tree && tree != 'undefined' )
			{
				nodes = new Array();
				tree.root.recursive
				(
					function()
					{
						if(action==1 && id==this.property.id){
								temp = new Object();
								nodes.push(temp);
								nodes[nodes.length-1].id = this.property.id;
								nodes[nodes.length-1].type = this.property.type;
								nodes[nodes.length-1].child = this.children.length;
						}
						if( this.state.checked == "checked")
						{						
							if(action==0 && id==this.property.id){}
							else{
								temp = new Object();
								nodes.push(temp);
								nodes[nodes.length-1].id = this.property.id;
								nodes[nodes.length-1].type = this.property.type;
								nodes[nodes.length-1].child = this.children.length;
							}
						}
					}
				);				
				return nodes;
			}
			return null;
		}
		
		window.addEvent('domready',function()
		{		

				tree = new Mif.Tree({
					container: $('tree_container'),
					forest: false,
					initialize: function(){
						<% if(showCheckbox=1) then %>
						this.initCheckbox('deps');
						<% end if %>
						new Mif.Tree.KeyNav(this);
					},					
					types: {
						organization:{
							openIcon: 'mif-tree-organization-icon',
							closeIcon: 'mif-tree-organization-icon'
						},
						domain:{
							openIcon: 'mif-tree-domain-icon',
							closeIcon: 'mif-tree-domain-icon'
						},
						loader:{
							openIcon: 'mif-tree-loader-open-icon',
							closeIcon: 'mif-tree-loader-close-icon',
							dropDenied: ['inside','after']
						},
						ou:{
							openIcon: 'mif-tree-ou-icon',
							closeIcon: 'mif-tree-ou-icon'
						}	
					},
					dfltType:'organization'
					
				});

				
				tree.initSortable();
				try
				{
					tree.load({
						url: 'ou_get_tree.asp'
					});
					
				}
				catch(e)
				{
					alert('Fail');
				}

				tree.loadOptions=function(node){
					return {
						url: 'ou_get_child.asp?id='+node.id
					};
				}
				tree.addEvent('onSelect',function(node, state){
					var content_file="users_new.asp";
					if($('obj_type').value=="R"){
						content_file="objects.asp"
					}
					if($('obj_type').value=="U"){
						content_file="users_new.asp"
					}
					if($('obj_type').value=="G"){
						content_file="groups_new.asp"
					}					
					if($('obj_type').value=="C"){
						content_file="computers_new.asp"
					}					
			
					res = getCheckedOus(0, 0);
					idSequence = "";			
					if( res ){
						for(i=0;i<res.length;i++){
							if(res[i].type != "organization" && res[i].type != "DOMAIN"){
								idSequence += res[i].id.toString() + " ";
							}
						}
					}
					$('content_iframe').src=content_file+"?sc=<%=showCheckbox %>&id="+node.id+"&type="+node.type+"&ous="+idSequence;
				});		
				tree.addEvent('onCheck',function(node, state){
					if(node.state.selected==true){

					var content_file="users_new.asp";
					if($('obj_type').value=="R"){
						content_file="objects.asp"
					}
					if($('obj_type').value=="U"){
						content_file="users_new.asp"
					}
					if($('obj_type').value=="G"){
						content_file="groups_new.asp"
					}
					if($('obj_type').value=="C"){
						content_file="computers_new.asp"
					}
			
					res = getCheckedOus(node.id, 1);
					idSequence = "";
					if( res ){
						for(i=0;i<res.length;i++){
							if(res[i].type != "organization" && res[i].type != "DOMAIN"){
								idSequence += res[i].id.toString() + " ";
							}
						}
					}
					$('content_iframe').src=content_file+"?sc=<%=showCheckbox %>&id="+node.id+"&type="+node.type+"&ous="+idSequence;						
					}
				});
				tree.addEvent('onUnCheck',function(node, state){
					if(node.state.selected==true){

					var content_file="users_new.asp";
					if($('obj_type').value=="U"){
						content_file="users_new.asp"
					}
					if($('obj_type').value=="G"){
						content_file="groups_new.asp"
					}					
					if($('obj_type').value=="C"){
						content_file="computers_new.asp"
					}					
			
					res = getCheckedOus(node.id, 0);
					idSequence = "";			
					if( res ){
						for(i=0;i<res.length;i++){
							if(res[i].type != "organization" && res[i].type != "DOMAIN"){
								idSequence += res[i].id.toString() + " ";
							}
						}
					}
					$('content_iframe').src=content_file+"?sc=<%=showCheckbox %>&id="+node.id+"&type="+node.type+"&ous="+idSequence;
					}
				});				

		});   
	
	function iframeAddObject(id){
		if(document.getElementById('object'+id)==null && document.getElementById("obj_cnt")){
			var el = document.createElement('input');
			el.type = 'hidden';
			el.name = 'added_user';
			el.value = id;
			el.id = 'object'+id;
			document.getElementById("objects_div").appendChild(el);
			document.getElementById("obj_cnt").innerHTML = parseInt(document.getElementById("obj_cnt").innerHTML) + 1;
		}
	}

	function iframeRemoveObject(id){
		if(document.getElementById('object'+id)!=null && document.getElementById("obj_cnt")){
			var objectInput = document.getElementById("object"+id);
			document.getElementById("objects_div").removeChild(objectInput);
			document.getElementById("obj_cnt").innerHTML = document.getElementById("obj_cnt").innerHTML - 1;
		}
	}	
	
	function iframeReload(){

		var myIframe= document.getElementById("content_iframe");  
		var iframeDocument = (myIframe.contentWindow || myIframe.contentDocument);  
		if (iframeDocument.document){  
			iframeDocument = iframeDocument.document;  
		}
		var iframeInputs = iframeDocument.getElementsByTagName("input");
		for(var i=0; i<iframeInputs.length; i++){
			if(iframeInputs[i].type=="checkbox"){
				if(document.getElementById(iframeInputs[i].id)!=null){
					iframeInputs[i].checked=true;
				}
			}
		}
	}
	
	</script>
