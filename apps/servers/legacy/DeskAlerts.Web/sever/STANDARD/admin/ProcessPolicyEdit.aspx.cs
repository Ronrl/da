﻿using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Repositories;
using DeskAlerts.ApplicationCore.Utilities;
using DeskAlertsDotNet.Parameters;
using DeskAlertsDotNet.Utils.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using DeskAlerts.Server.Utils.Interfaces;
using DeskAlerts.Server.Utils.Services;


namespace DeskAlertsDotNet.Pages
{
    public partial class ProcessPolicyEdit : DeskAlertsBasePage
    {
        private PolicyService _policyService;
        private ISkinService _skinService;
        private IContentSettingsService _contentSettingsService;

        //use curUser to access to current user of DA control panel      
        //use dbMgr to access to database

        public ProcessPolicyEdit()
        {
            _skinService = new SkinService();
            _contentSettingsService = new ContentSettingsService();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            var configurationManager = new WebConfigConfigurationManager(Config.Configuration);
            var policyRepository = new PpPolicyRepository(configurationManager);
            var groupRepository = new PpGroupRepository(configurationManager);
            var domainRepository = new PpDomainRepository(configurationManager);
            var userRepository = new PpUserRepository(configurationManager);
            var policyPublisherRepository = new PpPolicyPublisherRepository(configurationManager);
            var skinRepository = new PpSkinRepository(configurationManager);
            var contentSettingsRepository = new PpContentSettingsRepository(configurationManager);

            _policyService = new PolicyService(policyRepository, groupRepository, domainRepository, policyPublisherRepository, userRepository, skinRepository, contentSettingsRepository);

            string[] policies =
            {
                "alerts", "campaign", "emails", "sms", "surveys", "users", "groups",
                "text_templates", "statistics", "ip_groups", "rss", "screensavers", "wallpapers", "lockscreens",
                "im", "text_to_call", "cc"
            };

            Dictionary<string, string> policyDictionary = new Dictionary<string, string>();

            foreach (string policy in policies)
            {
                string requestPolicy = Request[policy];
                if (!string.IsNullOrEmpty(requestPolicy))
                {
                    policyDictionary.Add(policy + "_val", "'11111111'");
                }
                else
                {
                    string value = string.Empty;
                    for (int i = 0; i < 8; i++)
                    {
                        if (!string.IsNullOrEmpty(Request[policy + "_" + i]))
                        {
                            value += Request[policy + "_" + i];
                        }
                        else
                        {
                            value += "0";
                        }

                    }
                    policyDictionary.Add(policy + "_val", "'" + value + "'");
                }
            }


            bool edit = Request["edit"].Equals("1");


            int policyId = 0;
            string policyName = Request["policyName"];
            string policyType = Request["policy_type"];
            if (!bool.TryParse(Request.Params["policy_SkinsAccess_RadioButtonList"], out var policySkinsAccess))
            {
                policySkinsAccess = true;
            }
            if (!bool.TryParse(Request.Params["policy_ContentSettings_RadioButtonList"], out var policyContentSettingsAccess))
            {
                policyContentSettingsAccess = true;
            }

            if (!edit)
            {
                policyId = (int)_policyService.AddPolicy(new Policy
                {
                    Name = policyName,
                    Type = policyType,
                    FullSkinAccess = policySkinsAccess,
                    FullContentSettingsAccess = policyContentSettingsAccess
                });
            }
            else
            {
                policyId = Convert.ToInt32(Request["policyId"]);
                _policyService.UpdatePolicy(new Policy
                {
                    Id = policyId,
                    Name = policyName,
                    Type = policyType,
                    FullSkinAccess = policySkinsAccess,
                    FullContentSettingsAccess = policyContentSettingsAccess
                });
            }

            SavePolicySkinsAccess(policyId);
            SavePolicyContentSettingsAccess(policyId);

            if (policyId > 0)
            {
                FillPolicyOptions(policyDictionary, policyId, edit);
            }

            Response.Redirect("PolicyList.aspx");


        }

        private void FillPolicyOptions(Dictionary<string, string> policyDictionary, int policyId, bool edit)
        {
            if (!policyDictionary.ContainsKey("policy_id"))
            {
                policyDictionary.Add("policy_id", policyId.ToString());
            }

            if (edit)
            {
                dbMgr.ExecuteQuery("UPDATE policy_user SET was_checked = 0 WHERE policy_id = " + policyId);
                dbMgr.ExecuteQuery("UPDATE policy_ou SET was_checked = 0 WHERE policy_id = " + policyId);
                dbMgr.ExecuteQuery("UPDATE policy_computer SET was_checked = 0 WHERE policy_id = " + policyId);
                dbMgr.ExecuteQuery("UPDATE policy_group SET was_checked = 0 WHERE policy_id = " + policyId);
                dbMgr.ExecuteQuery("UPDATE policy_tgroups SET was_checked = 0 WHERE policy_id = " + policyId);

                dbMgr.ExecuteQuery("DELETE FROM policy_list WHERE policy_id = " + policyId);
                dbMgr.ExecuteQuery("DELETE FROM policy_viewer WHERE policy_id = " + policyId);
            }

            string querySql = @"INSERT INTO policy_list ( ";

            querySql += string.Join(",", policyDictionary.Keys.ToArray());

            querySql += " ) VALUES  (";

            querySql += string.Join(",", policyDictionary.Values.ToArray());

            querySql += ")";

            dbMgr.ExecuteQuery(querySql);

            bool canSendToAll = Request["all_recipients"].Equals("1");
            bool canViewAll = Request["view_all"].Equals("1");

            string[] ous = canSendToAll ? new string[] { } : Request["recipientsSelector$ousInput"].Split(',');
            string[] users = canSendToAll ? new string[] { } : Request["recipientsSelector$usersInput"].Split(',');
            string[] computers = canSendToAll ? new string[] { } : Request["recipientsSelector$computersInput"].Split(',');
            string[] groups = canSendToAll ? new string[] { } : Request["recipientsSelector$groupsInput"].Split(',');

            //insert OUs
            foreach (var ou in ous)
            {
                int ouId = 0;

                if (int.TryParse(ou, out ouId))
                {
                    dbMgr.ExecuteQuery(
                        $"INSERT INTO policy_ou (policy_id, ou_id, was_checked) VALUES ({policyId}, {ouId}, 1)");
                }
            }

            //insert users
            foreach (var user in users)
            {
                int userId = 0;

                if (int.TryParse(user, out userId))
                {
                    dbMgr.ExecuteQuery(
                        $"INSERT INTO policy_user (policy_id, user_id, was_checked) VALUES ({policyId}, {userId}, 1)");
                }
            }

            //insert computers
            foreach (var comp in computers)
            {
                int compId = 0;

                if (int.TryParse(comp, out compId))
                {
                    dbMgr.ExecuteQuery(
                        $"INSERT INTO policy_computer (policy_id, comp_id, was_checked) VALUES ({policyId}, {compId}, 1)");
                }
            }

            //insert groups
            foreach (var group in groups)
            {
                int groupId = 0;

                if (int.TryParse(group, out groupId))
                {
                    dbMgr.ExecuteQuery(
                        $"INSERT  INTO policy_group (policy_id, group_id, was_checked) VALUES ({policyId}, {groupId}, 1)");
                }
            }

            if (canViewAll)
            {
                dbMgr.ExecuteQuery($"INSERT INTO policy_viewer (policy_id, editor_id) VALUES ({policyId}, 0)");
            }
            else
            {
                dbMgr.ExecuteQuery($"DELETE FROM policy_viewer WHERE policy_id = {policyId}");
            }


            if (Config.Data.IsOptionEnabled("ConfTGroupEnabled"))
            {
                bool allTgroups = Request["all_templates"].Equals("1");

                if (!allTgroups)
                {
                    string[] tgroups = Request["idsTgroups"].Split(',');

                    foreach (string tgroup in tgroups)
                    {
                        dbMgr.ExecuteQuery(
                            $"INSERT  INTO policy_tgroups (tgroup_id, policy_id, was_checked) VALUES ({tgroup}, {policyId}, 1)");
                    }
                }
                else
                {
                    dbMgr.ExecuteQuery(
                        $"INSERT  INTO policy_tgroups (tgroup_id, policy_id, was_checked) VALUES (0, {policyId}, 1)");
                }
            }

            if (edit)
            {
                dbMgr.ExecuteQuery("DELETE FROM policy_user WHERE was_checked = 0 AND policy_id = " + policyId);
                dbMgr.ExecuteQuery("DELETE FROM policy_ou WHERE was_checked = 0 AND policy_id = " + policyId);
                dbMgr.ExecuteQuery("DELETE FROM policy_computer WHERE was_checked = 0 AND policy_id = " + policyId);
                dbMgr.ExecuteQuery("DELETE FROM policy_group WHERE was_checked = 0 AND policy_id = " + policyId);
                dbMgr.ExecuteQuery("DELETE FROM policy_tgroups WHERE was_checked = 0 AND policy_id = " + policyId);
            }
        }

        private void SavePolicySkinsAccess(long policyIdOnLoad)
        {
            if (policyIdOnLoad == default(long))
            {
                return;
            }

            var policy = _policyService.GetPolicyById(policyIdOnLoad);

            var isFullSkinAccess = bool.Parse(Request.Params["policy_SkinsAccess_RadioButtonList"]);
            policy.FullSkinAccess = isFullSkinAccess;
            var isAdminPolicy = Request.Params["policy_type"] == "A";

            if (isFullSkinAccess || isAdminPolicy)
            {
                _skinService.DeleteByPolicyId(policyIdOnLoad);
                _skinService.CreateDependencies(policyIdOnLoad);

                policy.FullSkinAccess = true;
            }
            else
            {
                _skinService.DeleteByPolicyId(policyIdOnLoad);

                var guidRequestParam = Request.Params["guildList"];

                var guidList = new List<Guid>();
                if (guidRequestParam != null && guidRequestParam.Any())
                {
                    var guidListArray = guidRequestParam.Split(',');
                    guidList.AddRange(guidListArray.Select(Guid.Parse));
                }

                _skinService.CreateSpecificDependencies(policyIdOnLoad, guidList);
            }

            _policyService.UpdatePolicy(policy);
        }

        private void SavePolicyContentSettingsAccess(long policyIdOnLoad)
        {
            if (policyIdOnLoad == default(long))
            {
                return;
            }

            var policy = _policyService.GetPolicyById(policyIdOnLoad);

            if (!bool.TryParse(Request.Params["policy_ContentSettings_RadioButtonList"], out var isFullContentSettingsAccess))
            {
                isFullContentSettingsAccess = true;
            }

            policy.FullContentSettingsAccess = isFullContentSettingsAccess;
            var isAdminPolicy = Request.Params["policy_type"] == "A";

            if (isFullContentSettingsAccess || isAdminPolicy)
            {
                _contentSettingsService.DeleteByPolicyId(policyIdOnLoad);
                _contentSettingsService.CreateDependencies(policyIdOnLoad);

                policy.FullContentSettingsAccess = true;
            }
            else
            {
                _contentSettingsService.DeleteByPolicyId(policyIdOnLoad);

                var selectedContentOptionsIdListParam = Request.Params["contentOptionList"];
                var contentOptionIdList = new List<long>();

                if (selectedContentOptionsIdListParam != null && selectedContentOptionsIdListParam.Any())
                {
                    var idsStringArray = selectedContentOptionsIdListParam.Split(',');
                    foreach (var id in idsStringArray)
                    {
                        long.TryParse(id, out var idAsLong);
                        contentOptionIdList.Add(idAsLong);
                    }
                }

                _contentSettingsService.CreateSpecificDependencies(policyIdOnLoad, contentOptionIdList);
            }

            _policyService.UpdatePolicy(policy);
        }
    }
}