<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%
check_session()
%>

		<table width="100%" border="1" bordercolor="#EAE9E1" cellspacing="0" cellpadding="3">
		<tr bgcolor="#EAE9E1">
		<td class="table_title">Installs</td>
		<td class="table_title">Uninstalls</td>
				</tr>

<%
Dim intInstallNum
Dim intUninstallNum
Dim rsToolbar
Dim rsInstall
Dim rsUninstall
Dim rsUrl
Dim intClickNum
Dim strUrl
Dim rsUrlStat

intInstallNum=0
intUninstallNum=0
Set rsToolbar = Conn.Execute("SELECT id FROM [toolbars] WHERE id=1")
if(Not rsToolbar.EOF) then
	Set rsInstall = Conn.Execute("SELECT * FROM [tb_install_stat] WHERE toolbar_id=" & rsToolbar("id"))
	do while not rsInstall.EOF
		intInstallNum=intInstallNum+1
		rsInstall.MoveNext	
	loop
	rsInstall.Close
	Set rsUninstall = Conn.Execute("SELECT * FROM [tb_uninstall_stat] WHERE toolbar_id=" & rsToolbar("id"))
	do while not rsUninstall.EOF
		intUninstallNum=intUninstallNum+1
		rsUninstall.MoveNext	
	loop
	rsUninstall.Close
%>
  <tr align="center"><td>
  <% Response.Write intInstallNum %>
  </td><td>
  <% Response.Write intUninstallNum %>
  </td></tr></table><br><br>
<table width="100%" border="1" bordercolor="#EAE9E1" cellspacing="0" cellpadding="3">
	<tr bgcolor="#EAE9E1">
	<td class="table_title">URL</td>
	<td class="table_title">Clicks</td>
	</tr>
<%

	Set rsUrl = Conn.Execute("SELECT * FROM [urls]")
	do while not rsUrl.EOF
        	intClickNum=0
		strUrl=rsUrl("url")
	  	Set rsUrlStat = Conn.Execute("SELECT * FROM [url_stat] WHERE url_id=" & rsUrl("id") & " AND toolbar_id=" & rsToolbar("id"))
		  do while not rsUrlStat.EOF
			intClickNum=intClickNum+1
			rsUrlStat.MoveNext	
		  loop
%>	
  <tr align="center"><td>
  <% Response.Write strUrl %>
  </td><td>
  <% Response.Write intClickNum %>
  </td></tr><%

		rsUrl.MoveNext	
	loop
	rsUrl.Close
end if
rsToolbar.Close
%>
</table><br><br>
<!-- #include file="db_conn_close.asp" -->