﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%

check_session()

uid = Session("uid")

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>DeskAlerts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/style9.css" rel="stylesheet" type="text/css">
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>

<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/preview.js?v=9.2.1.31"></script>
<script language="javascript" type="text/javascript" src="functions.js"></script>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		var width = $(".data_table").width();
		var pwidth = $(".paginate").width();
		if (width != pwidth) {
			$(".paginate").width("100%");
		}
		$("#add_template_button").button({
			icons : {
				primary : "ui-icon-plusthick"
			}
		});
		$(".delete_button").button();
	});

	function showAlertPreview(templateId)
	{
		var data = new Object();
		
		data.create_date = "<%=al_create_date%>";
	
		var fullscreen = false;
		var ticker =  0;
		var acknowledgement =  0; 
		
		var alert_width = 500;
		var alert_height = 400;
		var alert_title = "Sample title";
		var alert_html = "Sample alert";
		
		var top_template;
		var bottom_template;

		
		getTemplateHTML(templateId,"top",false,function(data){ 
				top_template = data;
		});

		getTemplateHTML(templateId,"bottom",false,function(data){ 
				bottom_template = data;
		});
				
		data.top_template = top_template;
		data.bottom_template = bottom_template;
		data.alert_width = alert_width;
		data.alert_height = alert_height;
		data.ticker = ticker;
		data.fullscreen = fullscreen;
		data.acknowledgement = acknowledgement;
		data.alert_title = alert_title;
		data.alert_html = alert_html;
		
		initAlertPreview(data);
		
		return false;
	}
</script>

</head>
<%
   if(Request("offset") <> "") then 
	offset = clng(Request("offset"))
   else 
	offset = 0
   end if	

   if(Request("sortby") <> "") then 
	sortby = Request("sortby")
   else 
	sortby = "name"
   end if	


if (uid <>"") then

'counting pages to display
   if(Request("limit") <> "") then 
	limit = clng(Request("limit"))
   else 
	limit=50
   end if

'  limit=10
	linkclick_str = LNG_TEMPLATES
	templates_arr = Array ("","","","","","","")
		
	Set RS = Conn.Execute("SELECT id FROM users WHERE id="&uid&" AND role='E'")
	if(Not RS.EOF) then
		gid = 1
		Set rs_policy = Conn.Execute("SELECT type FROM policy INNER JOIN policy_editor ON policy.id=policy_editor.policy_id WHERE editor_id=" & uid)
		if(Not rs_policy.EOF) then
			if(rs_policy("type")="A") then
				gid = 0
			end if
		end if
		if(gid=1) then
			editor_id=RS("id")
			policy_ids=""
			policy_ids=editorGetPolicyIds(editor_id)
			templates_arr = editorGetPolicyList(policy_ids, "templates_val")
			if(templates_arr(2)="") then
				linkclick_str = "not"
			end if
		end if
	else
		gid = 0
	end if
	RS.Close


'if(gid<>0) then
'	Set RS = Conn.Execute("SELECT COUNT(id) as mycnt FROM templates WHERE sender_id="&uid)
'else
	Set RS = Conn.Execute("SELECT COUNT(id) as mycnt FROM templates")
'end if

	cnt=0

	if(Not RS.EOF) then
		cnt=RS("mycnt")
	end if

	RS.Close

	j=cnt/limit

%>
<body style="margin:0px" class="body">
<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	<tr>
	<td>
	<table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
		<tr>
		<td width=100% height=31 class="main_table_title"><a href="admin_admin.asp" class="header_title"><%=LNG_TEMPLATES %></a></td>
		</tr>
		<tr>
		<td class="main_table_body" height="100%">
		<div style="margin:10px;">
		<span class="work_header"><%=LNG_TEMPLATES %></span>
		<br>
		<%if(gid = 0 OR (gid = 1 AND templates_arr(0)<>"")) then%>
		<div align="right">
		 <a id="add_template_button" href="template_edit.asp"><%=LNG_ADD_TEMPLATE %></a></div><br>
		 <%end if%>


<%
	page="templates.asp?"
	name= LNG_TEMPLATES
	Response.Write make_pages(offset, cnt, limit, page, name, sortby) 

	response.write "<br><A href='#' class='delete_button' onclick=""javascript: return LINKCLICK('"&linkclick_str&"');"">"&LNG_DELETE&"</a><br/><br>"
	response.write "<form action='delete_objects.asp' name='delete_objs' id='delete_objs' method='post'><input type='hidden' name='objType' value='templates'>"


%>



		<table width="100%" height="100%" class="data_table" cellspacing="0" cellpadding="3">
		<tr class="data_table_title">
		<td width="10"><input type='checkbox' name='select_all' id='select_all' onclick='javascript: select_all_objects();'></td>
		<td width="20" class="table_title"><%= sorting("ID","id", sortby, offset, limit, page) %></td>
		<td class="table_title"><%= sorting(LNG_TEMPLATE,"name", sortby, offset, limit, page) %></td>
		<td class="table_title"><%=LNG_ACTIONS %></td>
				</tr>
<%

'show main table


'if(gid<>0) then
'	Set RS = Conn.Execute("SELECT id, name FROM templates WHERE sender_id="&uid&" ORDER BY "&sortby)
'else
	Set RS = Conn.Execute("SELECT id, name FROM templates ORDER BY "&sortby)
'end if

	num=offset
	if(Not RS.EOF) then RS.Move(offset) end if
	Do While Not RS.EOF
		num=num+1
		if((offset+limit) < num) then Exit Do end if
		if(num > offset AND offset+limit >= num) then

		strObjectID=RS("id")
		%>
		<tr>
		<td>
		<%if(RS("id")<>"1") then %>
		<input type="checkbox" name="objects" value="<%=strObjectID%>">
		<%else%>
		&nbsp;
		<%end if%>
		</td>
		<td><%=RS("id") %></td>
		<td>
		<%if(gid = 0 OR (gid = 1 AND templates_arr(4)<>"")) then %>
		<a href="#" onclick="showAlertPreview(<%Response.Write RS("id")%>);">
		
		<% end if

		Response.Write HtmlEncode(RS("name"))

		%>
		<%if(gid = 0 OR (gid = 1 AND templates_arr(4)<>"")) then %>
		</a>
		<%end if%>
		</td>
		<td align="center" width="80" nowrap="nowrap">
<%if(gid = 0 OR (gid = 1 AND templates_arr(4)<>"")) then %>
		<a href="#" onclick="showAlertPreview(<%Response.Write RS("id")%>);"><img src="images/action_icons/preview.png" alt="<%=LNG_VIEW_TEMPLATE %>" title="<%=LNG_VIEW_TEMPLATE %>" width="16" height="16" border="0" hspace=5></a>
<%end if%>		

<%if(gid = 0 OR (gid = 1 AND templates_arr(1)<>"")) then%>
		<a href="template_edit.asp?id=
		<% Response.Write RS("id") %>
		"><img src="images/action_icons/edit.png" alt="<%=LNG_EDIT_TEMPLATE %>" title="<%=LNG_EDIT_TEMPLATE %>" width="16" height="16" border="0" hspace=5></a>
<%end if%>		
		</td>	
		<%
		end if
	RS.MoveNext
	Loop
	RS.Close



%>
         </table>
<%
	response.write "</form><br/><A href='#' class='delete_button' onclick=""javascript: return LINKCLICK('"&linkclick_str&"');"">"&LNG_DELETE&"</a><br><br>"
	Response.Write make_pages(offset, cnt, limit, page, name, sortby) 

%>

		 <img src="images/inv.gif" alt="" width="1" height="100%" border="0">
		</div>
		</td>
		</tr>
	</table>
	</td>
	</tr>
</table>
<%
else
  Response.Redirect "index.asp"

end if
%>
</body>
</html>
<!-- #include file="db_conn_close.asp" -->