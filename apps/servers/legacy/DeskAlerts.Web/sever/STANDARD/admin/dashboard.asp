﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<!-- #include file="Common.asp" -->
<%
if WIDGETSTAT = 0 then
check_session()

uid = Session("uid")

if uid <>"" then

    Server.ScriptTimeout = 120
    Conn.CommandTimeout = 120
    mobile_device=Session("isMobile")
    simple_mobile=Session("isSimpleMobile")

   if(Request("day") <> "") then 
	day_num = clng(Request("day"))
   else 
	day_num = 0
   end if
   
   if(Request("tab") <> "") then 
	tab = Request("tab")
   else 
	tab = 0
   end if
   
   if(Request("listId1") <> "") then 
	listId1 = Request("listId1")
   else 
	listId1 = 0
   end if
      
   if(Request("listValue") <> "") then 
	listValue = Request("listValue")
   else 
	listValue = "all"
   end if
     
   if(Request("offset") <> "") then 
	offset = clng(Request("offset"))
   else 
	offset = 0
   end if	

   if(Request("sortby") <> "") then 
	sortby = Request("sortby")
   else 
	sortby = "create_date DESC"
   end if	

   if(Request("limit") <> "") then 
	limit = clng(Request("limit"))
   else 
	limit=10
   end if
   
    Dim currDate, nMonth, nDay, nYear, prevMonth, prevYear, nextMonth, nextYear, from_d, to_d
    Dim lastMonth, lastYear, lastFirstDayPos, lastNumDays, submit
    Dim numDays, monthName(13), count, pos, firstDayPos, eventDay, dayNum, cell(38)
    Dim events(38)
    Dim tabs, minimize_cal_arrow, minimize_cal, minimize_list_arrow, minimize_list

    currDate=Date
    nMonth=Int(Month(currDate))
    nDay=Int(Day(currDate))
    nYear=Int(Year(currDate)) 
    dayOfWeek=Int(Weekday(currDate))
    lastMonth=nMonth
    lastYear=nYear

   if Request.Form("submit") <> "" then
    lastMonth=Int(Request.Form("lastMonth"))
    lastYear=Int(Request.Form("lastYear"))
    submit = Request.Form("submit")
   end if
   if(Request("month") <> "") then 
	lastMonth = clng(Request("month"))
	nMonth=lastMonth
   end if
   if(Request("year") <> "") then 
	lastYear = clng(Request("year"))
	nYear=lastYear
   end if

    prevMonth=lastMonth-1
    prevYear=lastYear-1
    nextMonth=lastMonth+1
    nextYear=lastYear+1
    if prevMonth=0 then
        prevYear=lastYear-2
        prevMonth=12
    end if
    if nextMonth=13 Then
        nextYear=nextYear + 2
        nextMonth=1
    end if
   
   Function getListClass(listValue)
        Select CASE listValue
        CASE "all"
            getListClass=0
        CASE "alerts"
            getListClass=1
        CASE "surveys"
            getListClass=64
        CASE "wallpapers"
            getListClass=8
        CASE "screensavers"
            getListClass=2
        CASE "RSS"
            getListClass=5
       End select 
   End Function
   
   showClass1=getListClass(listValue)
                     
        
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>DeskAlerts</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link href="css/dashboard.css" rel="stylesheet" type="text/css">
	<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css" />
	<link href="css/style9.css" rel="stylesheet" type="text/css">

	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>

	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
	<script type="text/JavaScript" language="JavaScript" src="jscripts/json2.js"></script>

	<script language="javascript" type="text/javascript" src="jscripts/preview.js?v=9.2.1.31"></script>

	<script language="javascript" type="text/javascript" src="functions.js"></script>

	<script language="javascript" type="text/javascript" src="jscripts/date.js"></script>

	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.cookies.2.2.0.min.js"></script>

	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.cookie.js"></script>

	<%

    monthName(1) = LNG_JANUARY
    monthName(2) = LNG_FEBRUARY
    monthName(3) = LNG_MARCH
    monthName(4) = LNG_APRIL
    monthName(5) = LNG_MAY
    monthName(6) = LNG_JUNE
    monthName(7) = LNG_JULY
    monthName(8) = LNG_AUGUST
    monthName(9) = LNG_SEPTEMBER
    monthName(10) = LNG_OCTOBER
    monthName(11) = LNG_NOVEMBER
    monthName(12) = LNG_DECEMBER

    if submit = "< "&monthName(prevMonth) then
        nMonth=prevMonth
        nYear=lastYear
        if nMonth=12 then
            nYear=nYear-1
            end if
        nextMonth=lastMonth
        nextYear=nYear+1     
        prevMonth=nMonth-1
            if prevMonth=0 then
            prevYear=nYear-2
            prevMonth=12
            end if  
         lastMonth=nMonth
         lastYear=nYear  
    end if
         
    if submit=monthName(nextMonth)&" >" Then
        nMonth=nextMonth
        nYear=lastYear
        if nMonth=1 then
            nYear=nYear+1
            end if
        prevMonth=lastMonth
        prevYear=lastYear-1
        nextYear=lastYear+1
        nextMonth=nMonth+1
            if nextMonth=13 then
            nextYear=nextYear+2
            nextMonth=1
            end if  
         lastMonth=nMonth
         lastYear=nYear           
    end if  
         
    if submit = "<< "&monthName(prevMonth)&" "&prevYear then
        nextMonth=lastMonth
        nMonth=prevMonth
        nYear=prevYear
        prevMonth=nMonth-1
        prevYear=nYear-1
            if prevMonth=0 then
            prevYear=nYear-2
            prevMonth=12
            end if  
         lastMonth=nMonth
         lastYear=nYear 
         nextYear=nYear+1
    end If

    if submit = monthName(nextMonth)&" "&nextYear&" >>" then
        prevMonth=lastMonth
        nMonth=nextMonth
        nYear=nextYear
        nextMonth=nMonth+1
        nextYear=nYear+1 
        prevYear=nYear-1
            if nextMonth=13 then
            nextYear=nYear+2
            nextMonth=1
            end if  
         lastMonth=nMonth
         lastYear=nYear 
    end If  

    if nMonth = 4 or nMonth = 6 or nMonth = 9 or nMonth = 11 then
              numDays = 30
    elseif nMonth <> 2 then
              numDays = 31
    end If

    if nMonth = 2 then
        test1 = nYear Mod 400 
            if test1 = 0 Then
              numDays = 29
            else
              test2 = nYear Mod 4
              test3 = nYear Mod 100
              if test2 = 0 And test3 > 0 Then
                numDays = 29
              else
                numDays = 28
              end If
            end If
    end If 

	%>

	<script language="javascript" type="text/javascript">

         function openDialogUI(_form,_frame,_href, _width, _height, _title) {
        $("#dialog-" + _form).dialog('option', 'height', _height);
        $("#dialog-" + _form).dialog('option', 'width', _width);
        $("#dialog-" + _form).dialog('option', 'title', _title);
        $("#dialog-" + _frame).attr("src", _href);
        $("#dialog-" + _frame).css("height", _height);
        $("#dialog-" + _frame).css("display", 'block');
        if ($("#dialog-" + _frame).attr("src") != undefined) {
            $("#dialog-" + _form).dialog('open');
            $("#dialog-" + _form).contents().find('#table_border').css('border', 'none');
        }
    }



	    $(document).ready(function() {
	    
	         $("#dialog-form").dialog({
                autoOpen: false,
                height: 100,
                width: 100,
                modal: true
            });

	    
	        $('#dialog-frame').on('load', function() {
                $("#dialog-frame").contents().find('.prelink_stats').on('click', function(e) {
                    e.preventDefault();
                    var link = $(this).attr('href');
                    var title = $(this).attr('title');
                    var last_id = $('.dialog_forms').size();
                    var elem = '<div id="dialog-form-' + last_id + '" class="dialog_forms" style="overflow:hidden;display:none;"><iframe id="dialog-frame-' + last_id + '" style="display:none;width:100%;height:auto;overflow:hidden;border : none;"></iframe></div>';
                    $('#secondary').append(elem);
                    $("#dialog-form-" + last_id).dialog({
                        autoOpen: false,
                        height: 100,
                        width: 100,
                        modal: true
                    });
                    openDialogUI("form-" + last_id, "frame-" + last_id, link, 350, 350, title);
                });
            });

        var list_id1 = 0;
        var list_value1 = 'all';
        var settingsDateFormat = "<%=GetDateFormat()%>";
        var settingsTimeFormat = "<%=GetTimeFormat()%>";

        var responseDbFormat = "dd/MM/yyyy HH:mm:ss";
        var uiFormat = getUiDateTimeFormat(settingsDateFormat, settingsTimeFormat);
        var serverDate;
        var settingsDateFormat = "<%=GetDateFormat()%>";
        var settingsTimeFormat = "<%=GetTimeFormat()%>";

        var responseDbFormat = "dd/MM/yyyy HH:mm:ss";
        var uiFormat = getUiDateTimeFormat(settingsDateFormat, settingsTimeFormat);
        function getOrder() {
            var str = "";
            var items = $("#column").sortable("toArray");
            for (var i = 0, n = items.length; i < n; i++) {
                var v = $('#' + items[i]).find('.portlet-content').is(':visible');
                items[i] = items[i] + "," + v;
                if (i != 0) str = str + "," + items[i];
                else str = str + items[i];
            }
            $.get("set_parameters.asp?name=setListOrder&listOrder="+str);
        };

        function restoreOrder() {
            $.get("set_parameters.asp?name=getlistOrder", function(data){
                var list = $("#column");
                if (list == null) return
                var cookie = data;
                if (!cookie) return;
                var IDs = cookie.split(',');
                var rebuild = new Array();
                var items = list.sortable("toArray");
                for (var v = 0, len = items.length; v < len; v++) {
                    rebuild[items[v]] = items[v];
                }
                for (var i = 0, n = IDs.length; i < n - 1; i++) {
                    var itemID = IDs[i];
                    var visible = IDs[i + 1]; i = i + 1;
                    if (itemID in rebuild) {
                        var item = rebuild[itemID];
                        var child = $("div.ui-sortable").children("#" + item);
                        var savedOrd = $("div.ui-sortable").children("#" + itemID);
                        $("div.ui-sortable").filter(":first").append(savedOrd);
                        if (visible === 'false') {
                            child.find(".ui-icon").toggleClass("ui-icon-minusthick ui-icon-plusthick");
                            child.find(".portlet-content").hide();
                        }
                        else {
                            child.find(".portlet-content").show();
                        }
                    }
                }
            });
        };

        function showWallpaperPreview(alertId) {
            var data;
            getAlertData(alertId, false, function(alertData) {
                data = JSON.parse(alertData);
            });
            var imageSrc = data.alert_html.replace(/^admin\//, "");
            var position = data.fullscreen;
            $("#preview_image_src").val(imageSrc);
            $("#preview_position").val(position);

            var params = 'width=' + screen.width;
            params += ', height=' + screen.height;
            params += ', top=0, left=0'
            params += ', fullscreen=yes';


            //openDialog2('', screen.width, screen.height);
            window.open('', 'preview_win', params);

            $("#preview_post_form").submit();
            hideLoader();
        }
        function hideLoader() {
            $.grep($(window.top.document).find("frame"), function(n, i) {
                if ($(n).attr("name") == "main") {
                    var doc = $(n)[0].contentDocument || $(n)[0].contentWindow.document;
                    $("#loader", doc).fadeOut(500);
                    $("#shadow", doc).fadeOut(500);
                }
            });
        }
        function openDialog(alertID) {
            var url = "<%=jsEncode(alerts_folder)%>preview.asp?id=" + alertID;
            $('#linkValue').val(url);
            $('#linkValue').focus(function() {
                $('#linkValue').select().mouseup(function(e) {
                    e.preventDefault();
                    $(this).unbind("mouseup");
                });
            });
            $('#linkDialog').dialog('open');
        }

        function showAlertPreview(alertId) {

            var data = new Object();

            getAlertData(alertId, false, function(alertData) {
                data = JSON.parse(alertData);
            });

            var fullscreen = parseInt(data.fullscreen);
            var ticker = parseInt(data.ticker);
            var acknowledgement = data.acknowledgement;

            var alert_width = (fullscreen == 1) && !ticker ? $(".main_table_body").width() : (!ticker ? data.alert_width : $(".main_table_body").width());
            var alert_height = (fullscreen == 1) && !ticker ? $(window).height() - document.body.clientHeight + $(".main_table_body").height() : (!ticker ? data.alert_height : "");
            var alert_title = data.alert_title;
            var alert_html = data.alert_html;

            var top_template;
            var bottom_template;
            var templateId = data.template_id;

            if (templateId >= 0) {
                getTemplateHTML(templateId, "top", false, function(data) {
                    top_template = data;
                });

                getTemplateHTML(templateId, "bottom", false, function(data) {
                    bottom_template = data;
                });
            }
            data.top_template = top_template;
            data.bottom_template = bottom_template;
            data.alert_width = alert_width;
            data.alert_height = alert_height;
            data.ticker = ticker;
            data.fullscreen = fullscreen;
            data.acknowledgement = acknowledgement;
            data.alert_title = alert_title;
            data.alert_html = alert_html;
            data.caption_href = data.caption_href;
            initAlertPreview(data);
        }
  
        $(function() {
          
            $("#column").sortable({
                connectWith: "#column",
                handle: ".portlet-header",
                cancel: ".portlet-toggle",
                placeholder: "portlet-placeholder ui-corner-all",
                start: function(e, ui) {
                    ui.placeholder.height(ui.item.height());
                },
                update: function() { getOrder(); }
            });
            $("#listItem1").on('change', function() {
                list_id1 = $("#listItem1")[0].selectedIndex;
                list_value1 = $("#listItem1").val();
            });
            $("#listItem1").prop("selectedIndex", '<%=listId1 %>');

            $(".portlet")
            .addClass("ui-widget ui-widget-content ui-helper-clearfix ui-corner-all")
            .find(".portlet-header")
            .addClass("ui-widget-header ui-corner-all")
            <%if mobile_device=true then %>
            .prepend("<span class='ui-icon ui-icon-minusthick portlet-toggle' style='display:none'></span>");
            <%else %>
            .prepend("<span class='ui-icon ui-icon-minusthick portlet-toggle'></span>");
            <%end if %>
            $(".portlet-header").click(function() {
                $(this).find('.portlet-toggle').toggleClass("ui-icon-minusthick").toggleClass("ui-icon-plusthick");
                $(this).parents(".portlet:first").find(".portlet-content").toggle();
                getOrder();
            });

            $(".delete_button").button();
            $("#show_sent_alert").button();
            $("#show_scheduled").button();
            serverDate = new Date(getDateFromFormat("<%=now_db%>", responseDbFormat));
            setInterval(function() { serverDate.setSeconds(serverDate.getSeconds() + 1); }, 1000);
            $(".alert_data_bg tbody tr td:last-child").css("border-right", "1px solid #cecece");
            $("#linkDialog").dialog({
                autoOpen: false,
                height: 80,
                width: 550,
                modal: true,
                resizable: false
            });
            restoreOrder();
            var width = $(".data_table").width();
            var pwidth = $(".paginate").width();
            if (width != pwidth) {
                $(".paginate").width("100%");
            }



        })

	</script>

	<%

	'--- rights ---
    set rights = getRights(uid, true, false, true, Array("alerts_val","surveys_val","screensavers_val","wallpapers_val","lockscreens_val","rss_val","im_val"))
    gid = rights("gid")
    gviewall = rights("gviewall")
    gviewlist = rights("gviewlist")
    alerts_arr = rights("alerts_val")
    surveys_arr = rights("surveys_val")
    screensavers_arr = rights("screensavers_val")
    wallpapers_arr = rights("wallpapers_val")
    lockscreens_arr = rights("lockscreens_val")
    rss_arr = rights("rss_val")
    im_arr = rights("im_val")
    classPolicy = Array()
    ReDim classPolicy(8)
    class_num=0
    if alerts_arr(4)<>"" or im_arr(4)<>"" then 
        classPolicy(class_num)=1
        class_num=class_num+1
    end if
    if surveys_arr(4)<>"" then 
        classPolicy(class_num)=64
        classPolicy(class_num+1)=128
        classPolicy(class_num+2)=256
        class_num=class_num+3
    end if
    if screensavers_arr(4)<>"" then 
        classPolicy(class_num)=2
        class_num=class_num+1
    end if        
    if wallpapers_arr(4)<>"" then 
        classPolicy(class_num)=8
        class_num=class_num+1
    end if
    if lockscreens_arr(4)<>"" then 
        classPolicy(class_num)=4096
        class_num=class_num+1
    end if
    if rss_arr(4)<>"" then 
        classPolicy(class_num)=5
        class_num=class_num+1
    end if    

	'--------------

	from_d="1/1/1900 00:00:00"
    to_d=Now()

 
	%>
</head>
<body style="margin: 0" class="body">
	<form style="margin: 0px; width: 0px; height: 0px; padding: 0px" id="preview_post_form"
	action="wallpaper_preview.asp" method="post" target="preview_win">
	<input type="hidden" id="preview_image_src" name="image_src" value="" />
	<input type="hidden" id="preview_position" name="position" value="" />
	</form>
	<form accept-charset="ISO-8859-1" name='my_form' method='post' action='dashboard.asp'>
	<div class="column" id="column" style="width: 100%">
		<%if simple_mobile<>true then%>
		<div class="portlet" id="item-1" style="border: 0px">
			<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%" style="border: 0px;">
				<tr>
					<td>
						<%            
                Response.Write "<table cellspacing='0' width='100%' cellpadding='0' border='1' style='margin:auto; border:0px solid #CCC; min-width:500px' ><tr style='border-bottom:0px'><td  height='31'  style='border: 1px solid #CCC;' class='portlet-header'><a href='' class='header_title'>"&LNG_CALENDAR &"</a></td></tr><tr >"
                Response.Write "<td style='border:0px' ><table id='calendar_view' class='calendar_view portlet-content' cellspacing='0' width='100%' cellpadding='0' style='min-width:690px; border-collapse: collapse;margin:auto; border:1px solid #CCC;'><tr ><td style='border:none; padding:10px;border-top:0px solid;'><input type=submit class='table_title' name=submit style='border:0px ;cursor:pointer;background: transparent;' value='<< "&monthName(prevMonth)&" "&prevYear&"'></td>"
                Response.Write "<td colspan='2' style='border:none; padding:10px;'><input type=submit class='table_title' name=submit style='border:0 none;cursor:pointer;background: transparent;' value='< "&monthName(prevMonth)&"'></td>"
                Response.Write "<td class='table_title' colspan='1' style='border:none;text-align:center'>" & monthName(nMonth) & " "& nYear&"</td>"
                Response.Write "<td colspan='2' style='border:none;text-align:right;'><input type=submit class='table_title' style='border:0px ;cursor:pointer;background: transparent;text-align:right;' name=submit value='"&monthName(nextMonth)&" >'></td>"
                Response.Write "<td style='border:none;text-align:right;'><input type=submit class='table_title' style='border:0px ;cursor:pointer;background: transparent;' name=submit value='"&monthName(nextMonth)&" "&nextYear&" >>'></td>"  
                Response.Write "</tr>"        
                Response.Write ""
                Response.Write ""  

                For week=1 to 7
                    Response.Write "<th class='dayHeader' style='border:1px solid #CCC;width:15% !important;'>" & WeekdayName(week) & "</th>"  
                Next             
                Response.Write "<tr style='border:1px solid #CCC;'>"
                firstday = Weekday("01/"&nMonth&"/"&nYear)
                For k=0 to 42
                    displayNum = k-firstDay+2
                    if k<firstDay-1 then
                        Response.Write "<td style='border:1px solid #CCC;' class='empty'>&nbsp;</td>"
                    elseif displayNum<numDays+1 then
                        count=0
                        count_schedule=0
                        num=0
	                    from_date=displayNum&"/"&nMonth&"/"&nYear&" 00:00:00"
                        to_date=displayNum&"/"&nMonth&"/"&nYear&" 23:59:59"

                        set RS = Conn.Execute("processAlerts @select_active = 1, @start_date = '"& from_date &"', @end_date = '"& to_date &"'")
                        do while not RS.EOF
                            num=num+1
                        RS.MoveNext
	                    loop
	                    alldaysData = Array()
                        ReDim alldaysData(num) 
                        num=0
                        set RS = Conn.Execute("processAlerts @select_active = 1, @start_date = '"& from_date &"', @end_date = '"& to_date &"'")
                        do while not RS.EOF
	                        if RS("from_date") < RS("to_date") and RS("sent_date") < RS("to_date") and RS("sent_date") < to_date _
	                        then
	                            if RS("from_date")<>1900 then
	                                alldaysData(num) = RS("id")
	                                num=num+1
	                            end if
	                          end if
                        RS.MoveNext
	                    loop
	                    RS.Close

                        for i=0 to num-1
                         if num>0 then
	                        if gviewall <> 1 then
		                        SQL = "SELECT * FROM (SELECT a.id, a.schedule, schedule_type, a.type2, recurrence, alert_text, title, a.type, convert(varchar,a.sent_date) as sent_date, convert(varchar,a.create_date) as create_date, a.from_date, a.to_date, class, urgent, ticker, fullscreen, s.id as is_rsvp FROM alerts a LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id WHERE (a.type='S' OR a.type='L') AND a.create_date <= '" & to_date & "' AND a.id ="& alldaysData(i) &" AND a.sender_id IN ("& Join(gviewlist,",") &") AND a.id NOT IN (SELECT alert_id FROM instant_messages) "
		                        if class_num>0 then
		                            SQL = SQL & " AND (class ="& classPolicy(0)
	                                for k1=1 to class_num-1
		                                SQL = SQL &" OR class="& classPolicy(k1)		            
		                            next 
		                            SQL = SQL & " ) "     
		                        end if   
		                        SQL = SQL & " UNION ALL "
		                        SQL = SQL & "SELECT a.id, a.schedule, schedule_type, recurrence, a.type2, alert_text, title, a.type, convert(varchar,a.sent_date) as sent_date, convert(varchar,a.create_date) as create_date, a.from_date, a.to_date, class, urgent, ticker, fullscreen, s.id as is_rsvp  FROM archive_alerts a LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id WHERE (a.type='S' OR a.type='L') AND a.create_date <= '" & to_date & "' AND a.id ="& alldaysData(i) &" AND a.sender_id IN ("& Join(gviewlist,",") &") AND a.id NOT IN (SELECT alert_id FROM instant_messages) "
		                        if class_num>0 then
		                            SQL = SQL & " AND (class ="& classPolicy(0)
	                                for k1=1 to class_num-1
		                                SQL = SQL &" OR class="& classPolicy(k1)		            
		                            next 
		                            SQL = SQL & " ) "     
		                        end if     
		                        SQL = SQL & ") res ORDER BY res."&sortby 
                                Set RS = Conn.Execute(SQL)
	                        else
    	                        SQL = "SELECT * FROM (SELECT a.id, a.schedule, schedule_type, a.type2, recurrence, alert_text, title, a.type, convert(varchar,a.sent_date) as sent_date, convert(varchar,a.create_date) as create_date, a.from_date, a.to_date, class, urgent, ticker, fullscreen, s.id as is_rsvp FROM alerts a LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id WHERE (a.type='S' OR a.type='L') AND a.create_date <= '" & to_date & "' AND a.id ="& alldaysData(i) &" AND param_temp_id IS NULL  AND a.id NOT IN (SELECT alert_id FROM instant_messages) "
		                        if class_num>0 then
		                            SQL = SQL & " AND (class="& classPolicy(0)
	                                for k1=1 to class_num-1
		                                SQL = SQL &" OR class="& classPolicy(k1)		            
		                            next 
		                            SQL = SQL & " ) "     
		                        end if   		                        
		                        SQL = SQL & " UNION ALL "             
		                        SQL = SQL & "SELECT a.id, a.schedule, schedule_type, recurrence, a.type2, alert_text, title, a.type, convert(varchar,a.sent_date) as sent_date, convert(varchar,a.create_date) as create_date, a.from_date, a.to_date, class, urgent, ticker, fullscreen, s.id as is_rsvp  FROM archive_alerts a LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id WHERE (a.type='S' OR a.type='L') AND a.create_date <= '" & to_date & "' AND a.id ="& alldaysData(i) &" AND param_temp_id IS NULL  AND a.id NOT IN (SELECT alert_id FROM instant_messages) "
		                        if class_num>0 then
		                            SQL = SQL & " AND (class ="& classPolicy(0)
	                                for k1=1 to class_num-1
		                                SQL = SQL &" OR class="& classPolicy(k1)		            
		                            next 
		                            SQL = SQL & " ) "     
		                        end if   
		                        SQL = SQL & ") res ORDER BY res."&sortby 
		                        Set RS = Conn.Execute(SQL)
	                        end if
	                        Do While Not RS.EOF
	                            if count_schedule>0 then exit do
	                            count_schedule=count_schedule+1
	                            RS.MoveNext
	                        Loop
	                       end if
                        next
	                    if gviewall <> 1 then
		                    SQL = "SELECT COUNT(1) as mycnt, 1 as mycnt1 FROM alerts WHERE (type='S' OR type='L') AND create_date >= '" & from_date & "' AND create_date <= '" & to_date & "' AND param_temp_id IS NULL  AND sender_id IN ("& Join(gviewlist,",") &") AND id NOT IN (SELECT alert_id FROM instant_messages) " 
		                    SQL = SQL & " UNION ALL "
		                    SQL = SQL & "SELECT COUNT(1) as mycnt, 2 as mycnt1 FROM archive_alerts WHERE (type='S' OR type='L') AND create_date >= '" & from_date & "' AND create_date <= '" & to_date & "' AND param_temp_id IS NULL AND sender_id IN ("& Join(gviewlist,",") &") AND id NOT IN (SELECT alert_id FROM instant_messages) "
		                    Set RS = Conn.Execute(SQL)
	                    else
		                    SQL = "SELECT COUNT(1) as mycnt, 1 as mycnt1 FROM alerts WHERE (type='S' OR type='L') AND create_date >= '" & from_date & "' AND create_date <= '" & to_date & "' AND param_temp_id IS NULL  AND id NOT IN (SELECT alert_id FROM instant_messages) "
		                    SQL = SQL & " UNION ALL "
		                    SQL = SQL & "SELECT COUNT(1) as mycnt, 2 as mycnt1 FROM archive_alerts WHERE (type='S' OR type='L') AND create_date >= '" & from_date & "' AND create_date <= '" & to_date & "' AND param_temp_id IS NULL AND id NOT IN (SELECT alert_id FROM instant_messages) "
		                    Set RS = Conn.Execute(SQL)
	                    end if
	                    cnt=0
	                    do while Not RS.EOF
		                    if(Not IsNull(RS("mycnt"))) then cnt=cnt+clng(RS("mycnt"))
		                    RS.MoveNext
	                    loop
	                    RS.Close
	                    if displayNum=Day(Now()) and nMonth=Month(Now()) and nYear=Year(Now()) then 
	                        Response.Write "<td style='border:1px solid #CCC;padding:6px'><div style='text-align:right;background-color:#C6F6C0;'>" 
	                    else Response.Write "<td style='border:1px solid #CCC;padding:6px'><div style='text-align:right;background-color:#ebebeb;'>" 
	                    end if
	                        if cnt<>0 AND count_schedule<>0 then                 
                              Response.Write ""&displayNum&"&nbsp;</div><div><br><a href='dashboard_list.asp?day="&displayNum&"&month="&nMonth&"&year="&nYear&"&tab=0'>"&LNG_CREATED&": "&cnt&"</a><br><a href='dashboard_list.asp?day="&displayNum&"&month="&nMonth&"&year="&nYear&"&tab=1' >"&LNG_SHOW_SCHEDULED&"</a></div></td>"
                            elseif count<>0 then
                              Response.Write ""&displayNum&"&nbsp;</div><div><br><a href='dashboard_list.asp?day="&displayNum&"&month="&nMonth&"&year="&nYear&"&tab=0'>"&LNG_CREATED&": "&cnt&"</a><br><br></div></td>"  
                            elseif count_schedule<>0 then
                              Response.Write ""&displayNum&"&nbsp;</div><div><br><a href='dashboard_list.asp?day="&displayNum&"&month="&nMonth&"&year="&nYear&"&tab=1'>"&LNG_SHOW_SCHEDULED&"</a><br><br></div></td>"  
                            else
                              Response.Write ""&displayNum&"&nbsp;</div><div><br>&nbsp;<br><br></div></td>"
                            end if
                    else  Response.Write "<td style='border:1px solid #CCC;' class='empty'></td>"  
                    end if
                    if k Mod 7=6 then
                        if k < numDays+firstDay-2 then 
                            Response.Write "</tr><tr>"
                        else   
                            exit for  
                        end if
                    end if    
                Next
             end if
                Response.Write "</table></td></tr></table>"              
                Response.Write "<input type=hidden name=lastMonth value=" & nMonth & ">"
                Response.Write "<input type=hidden name=lastYear value=" & nYear & ">"
                Response.Write "<input type=hidden name=lastFirstDayPos value=" & firstDayPos & ">"
                Response.Write "<input type=hidden name=lastNumDays value=" & numDays & ">"
						%>
					</td>
				</tr>
			</table>
		</div>
		<%end if %>
		<div class="portlet" id="item-2" style="border: 0px; padding: 6px">
			<table height="100%" width="100%" cellspacing="0" border="0" cellpadding="0" style="margin: auto"
				class="main_table">
				<tr>
					<td height="31" class="portlet-header" width="20%" style="border: 0px solid #CCC;
						border-bottom: 0px">
						<a href="" class="header_title">
							<%=LNG_LAST_ALERTS%></a>
					</td>
					<!--td align='right' style='border-left:0px' class='main_table_title'><a type=button id='minimize_list' name='minimize_list' style='border:2 solid; cursor:pointer;'>minimize</a></td!-->
				</tr>
				<tr id="list_view">
					<td height="100%" valign="top" width="20%" class="main_table_body">
						<div class="portlet-content" id="alerts_table" style="padding: 0px; border: none;">
							<table width="100%">
								<tr>
									<td align="right">
										<% if(gid = 0 OR (gid = 1 AND ((alerts_arr(4)<>"") OR (screensavers_arr(4)<>"") OR (wallpapers_arr(4)<>"") OR (lockscreens_arr(4)<>"") OR (rss_arr(4)<>"") OR (surveys_arr(4)<>"") OR (im_arr(4)<>"")))) then %>
										<% 'end if%>
									</td>
								</tr>
							</table>
							<div id="last_alerts" style="padding: 10px">
								<div style="text-align: right">
									<select name="listItem1" id="listItem1" onclick="$('#show_sent_alert').attr('href', 'dashboard.asp?listId1='+list_id1+'&listValue='+list_value1+'&tab=0')">
										<option value="all">
											<%=LNG_ALL %></option>
										<% if (gid = 0 OR (gid = 1 AND (alerts_arr(4)<>""))) then %>
										<option value="alerts">
											<%=LNG_ALERTS %></option>
										<% end if %>
										<% if SURVEY=1 AND (gid = 0 OR (gid = 1 AND (surveys_arr(4)<>""))) then %>
										<option value="surveys">
											<%=LNG_SURVEYS %></option>
										<% end if %>
										<% if WP=1 AND (gid = 0 OR (gid = 1 AND (wallpapers_arr(4)<>""))) then %>
										<option value="wallpapers">
											<%=LNG_WALLPAPERS %></option>
										<% end if %>
                                                                                <% if LS=1 AND (gid = 0 OR (gid = 1 AND (lockscreens_arr(4)<>""))) then %>
										<option value="lockscreens">
											<%=LNG_LOCKSCREENS %></option>
										<% end if %>
										<% if SS=1  AND (gid = 0 OR (gid = 1 AND (screensavers_arr(4)<>""))) then %>
										<option value="screensavers">
											<%=LNG_SCREENSAVERS %></option>
										<% end if %>
										<% if RSS=1  AND (gid = 0 OR (gid = 1 AND (rss_arr(4)<>""))) then %>
										<option value="RSS">
											<%=LNG_RSS %></option>
										<% end if %>
									</select>
									<a id="show_sent_alert" href="dashboard.asp">
										<%= LNG_SHOW%></a>
								</div>
								<br />
								<%
                                            count_alert=0
                                            LoadTemplate sDateFormFileName, "DateForm"
	                                        page="dashboard.asp?tab=0&"
                                            name=LNG_ALERTS 

                                    'show main table
	                                    if gviewall <> 1 then
		                                    SQL = "SELECT * FROM (SELECT a.id, a.schedule, schedule_type, a.type2, recurrence, alert_text, title, a.type, sent_date, a.create_date as create_date, a.from_date, a.to_date, class, urgent, ticker, fullscreen, s.id as is_rsvp FROM alerts a LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id WHERE (a.type='S' OR a.type='L') AND a.create_date >= '" & from_d & "' AND a.create_date <= '" & to_d & "' AND param_temp_id IS NULL AND a.sender_id IN ("& Join(gviewlist,",") &") AND a.id NOT IN (SELECT alert_id FROM instant_messages) "
		                                    if showClass1 <> 0 then
		                                        if showClass1=64 then
		    	                                    SQL = SQL & " AND (class ="& showClass1 &" OR class=128 OR class=256)"
		                                        else 	
		                                        SQL = SQL & " AND class ="& showClass1
		                                        end if
		                                    else
		                                    if class_num>0 then
		                                        SQL = SQL & " AND (class ="& classPolicy(0)
	                                            for k1=1 to class_num-1
		                                            SQL = SQL &" OR class="&classPolicy(k1)		            
		                                        next 
		                                        SQL = SQL & " ) "     
		                                    end if   
							                end if			                                    
							                SQL = SQL & " UNION ALL "
		                                    SQL = SQL & "SELECT a.id, a.schedule, schedule_type, recurrence, a.type2, alert_text, title, a.type, sent_date, a.create_date as create_date, a.from_date, a.to_date, class, urgent, ticker, fullscreen, s.id as is_rsvp  FROM archive_alerts a LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id WHERE (a.type='S' OR a.type='L') AND a.create_date >= '" & from_d & "' AND a.create_date <= '" & to_d & "' AND param_temp_id IS NULL AND a.sender_id IN ("& Join(gviewlist,",") &") AND a.id NOT IN (SELECT alert_id FROM instant_messages) ) res ORDER BY res."&sortby
                                            Set RS = Conn.Execute(SQL)
	                                    else
		                                    SQL = "SELECT * FROM (SELECT a.id, a.schedule, schedule_type, a.type2, recurrence, alert_text, title, a.type, sent_date, a.create_date as create_date, a.from_date, a.to_date, class, urgent, ticker, fullscreen, s.id as is_rsvp FROM alerts a LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id WHERE (a.type='S' OR a.type='L') AND a.create_date >= '" & from_d & "' AND a.create_date <= '" & to_d & "' AND param_temp_id IS NULL AND a.id NOT IN (SELECT alert_id FROM instant_messages) "
		                                    if showClass1 <> 0 then
		                                        if showClass1=64 then
		    	                                    SQL = SQL & " AND (class ="& showClass1 &" OR class=128 OR class=256)"
		                                        else 	
		                                        SQL = SQL & " AND class ="& showClass1
		                                        end if
		                                    else
		                                        if class_num>0 then
		                                            SQL = SQL & " AND (class ="& classPolicy(0)
	                                                for k1=1 to class_num-1
		                                                SQL = SQL &" OR class="&classPolicy(k1)		            
		                                            next 
		                                            SQL = SQL & " ) "     
		                                        end if   
							                end if	
		                                    SQL = SQL & " UNION ALL "
		                                    SQL = SQL & "SELECT a.id, a.schedule, schedule_type, recurrence, a.type2, alert_text, title, a.type, sent_date, a.create_date as create_date, a.from_date, a.to_date, class, urgent, ticker, fullscreen, s.id as is_rsvp  FROM archive_alerts a LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id WHERE (a.type='S' OR a.type='L') AND a.create_date >= '" & from_d & "' AND a.create_date <= '" & to_d & "' AND param_temp_id IS NULL  AND a.id NOT IN (SELECT alert_id FROM instant_messages) ) res ORDER BY res."&sortby
		                                    Set RS = Conn.Execute(SQL)
	                                    end if
                                        num=offset
	                                    if(Not RS.EOF) then RS.Move(offset) end if
	                                    Do While Not RS.EOF
	                                        count_alert=count_alert+1
		                                    num=num+1
		                                    if((offset+limit) > num) then 
		                                        if(num > offset AND offset+limit >= num) then
		                                        strObjectID=RS("id")                                                                                             
                                           if count_alert=1 then
								%>
								<table width="100%" height="100%" cellspacing="5" cellpadding="5" style="min-width: 670px;
									margin: auto; padding: 10px" id="data_table" class="data_table">
									<tr class="data_table_title">
										<td width="40" style="width: 40px !important">
											<%= LNG_TYPE%>
										</td>
										<td class="table_title" style="min-width: 280px; white-space: nowrap;">
											<%=LNG_ALERT_TITLE %>
										</td>
										<td style="min-width: 120px;" class="table_title">
											<%=LNG_CREATION_DATE %>
										</td>
										<td width="140" style="width: 100px" class="table_title">
											<%=LNG_ACTIONS %>
										</td>
									</tr>
									<% 
                                         end if  
									%>
									<tr>
										<td align="center">
											<%           
				                        dim isUrgent
				                        if RS("urgent") = 1 then
					                        isUrgent = true
				                        else
					                        isUrgent = false
				                        end if
				                        if RS("class") = "16" then
					                        if isUrgent = true then
						                        Response.Write "<img src=""images/alert_types/urgent_unobtrusive.gif"" title=""Urgent Unobtrusive"">"
					                        else
						                        Response.Write "<img src=""images/alert_types/unobtrusive.gif"" title=""Unobtrusive"">"
					                        end if
					                    elseif  RS("class") = "64" OR RS("class") = "128" OR RS("class") = "256" then
					                        Response.Write "<img src=""images/alert_types/survey.png"" title=""Survey"">"
				                        elseif not isNull(RS("is_rsvp")) then
					                        if isUrgent = true then
						                        Response.Write "<img src=""images/alert_types/urgent_rsvp.png"" title=""Urgent RSVP"">"
					                        else
						                        Response.Write "<img src=""images/alert_types/rsvp.png"" title=""RSVP"">"
					                        end if
				                        elseif RS("ticker")=1 then
					                        if isUrgent = true then
						                        Response.Write "<img src=""images/alert_types/urgent_ticker.png"" title=""Urgent Ticker"">"
					                        else
						                        Response.Write "<img src=""images/alert_types/ticker.png"" title=""Ticker"">"
					                        end if
				                        elseif  RS("class")="8" then
				                                Response.Write "<img src=""images/alert_types/wallpaper.gif"" title=""Wallpaper"">"
				                        
                                                        elseif  RS("class")="4096" then
				                                Response.Write "<img src=""images/alert_types/wallpaper.gif"" title=""Lockscreen"">"
                                                        elseif  RS("class")="2" then
				                                Response.Write "<img src=""images/alert_types/screensaver.gif"" title=""Screensaver"">"             	
				                        elseif RS("fullscreen")=1 then
					                        if isUrgent = true then
						                        Response.Write "<img src=""images/alert_types/urgent_fullscreen.png"" title=""Urgent Fullscreen"">"
					                        else
						                        Response.Write "<img src=""images/alert_types/fullscreen.png"" title=""Fullscreen"">"
					                        end if
				                        elseif isUrgent = true then
					                        Response.Write "<img src=""images/alert_types/urgent_alert.gif"" title=""Urgent Alert"">"
				                        elseif  RS("type")="L" then
				                                Response.Write "<img src=""images/alert_types/linkedin.png"" title=""LinkedIn Alert"">"
				                        elseif  RS("class")="5" then
				                                Response.Write "<img src=""images/alert_types/rss.gif"" title=""RSS/News Feed"">"
				                        else
					                        Response.Write "<img src=""images/alert_types/alert.png"" title=""Usual Alert"">"
				                        end if
											%>
										</td>
										<td>
											<% 
                                       if RS("class")="8" then 
			                                Response.Write "<a href=""javascript: showWallpaperPreview('" & CStr(RS("id")) & "')"">" & HtmlEncode(RS("title")) & "</a>"
			                           elseif  RS("class")="64" OR RS("class") = "128" OR RS("class") = "256" then 
			                                Response.Write  HtmlEncode(RS("title"))
			                           else     
			                            Response.Write "<a href=""javascript: showAlertPreview('" & CStr(RS("id")) & "')"">" & HtmlEncode(RS("title")) & "</a>"
			                           end if
											%>
										</td>
										<td>

											<script language="javascript">
												document.write(getUiDateTime('<%=RS("create_date")%>'));
											</script>

										</td>
										<td align="left" nowrap="nowrap">
											<% if( RS("class")="1" AND gid = 0 OR (gid = 1 AND alerts_arr(0)<>"") AND alerts_arr(1)="1") then %>
											<a href="edit_alert_db.asp?id=<%= RS("id") %>">
												<img src="images/action_icons/duplicate.png" alt="<%=LNG_RESEND_ALERT %>" title="<%=LNG_RESEND_ALERT %>"
													width="16" height="16" border="0" hspace="5"></a>
											<% end if
                                                       'if RS("class")="8" then 
			                                                    'Response.Write "<a href=""javascript: showWallpaperPreview('" & CStr(RS("id")) & "')""><img src=""images/action_icons/preview.png"" alt="""& LNG_PREVIEW &""" title="""& LNG_PREVIEW &""" width=""16"" height=""16"" border=""0"" hspace=""5""/></a>"
			                                           'elseif RS("class")<>"64" AND RS("class")<>"128" AND RS("class")<>"256"  then
                                                                'Response.Write "<a href=""javascript: showAlertPreview('" & CStr(RS("id")) & "')""><img src=""images/action_icons/preview.png"" alt="""& LNG_PREVIEW &""" title="""& LNG_PREVIEW &""" width=""16"" height=""16"" border=""0"" hspace=""5""/></a>"
                                                       'end if
											%><a href="#" onclick="openDialog(<%=RS("id") %>)"><img src="images/action_icons/link.png"
												alt="<%=LNG_DIRECT_LINK%>" title="<%=LNG_DIRECT_LINK%>" width="16" height="16"
												border="0" hspace="6" /></a><%
		                                                Response.Write "<a href='#' onclick=""openDialogUI('form','frame','statistics_da_alerts_view_details.asp?id=" & CStr(RS("id")) & "',600,550,'"& LNG_STATISTICS &"');""><img src=""images/action_icons/graph.gif"" alt="""& LNG_STATISTICS &""" title="""& LNG_STATISTICS &""" width=""16"" height=""16"" border=""0"" hspace=""5""/></a>"
												%>
										</td>
									</tr>
									<%
		                                            end if
		                                            end if
	                                            RS.MoveNext
	                                            Loop
	                                            RS.Close
									%>
									<%
                                        if num>0 then    
                                        Response.Write make_pages(offset, num, limit, page, name, sortby)
                                        else
                                            if listValue ="all" then 
                                                Response.Write "<center><b>"&LNG_THERE_ARE_NO_ALERTS&"</b><br><br></center>"
                                            else
                                            Response.Write "<center><b>"&LNG_THERE_ARE_NO & listValue& "</b><br><br></center>"
                                            end if
                                        end if
									%>
								</table>
							</div>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<%
else 
    Response.Redirect "settings_edit_profile.asp"    
end if
        
	%>
	<div id="linkDialog" title="<%= LNG_DIRECT_LINK %>" style="display: none">
		<input id="linkValue" type="text" readonly="readonly" style="width: 500px" />
	</div>
	</form>
</body>
</html>
<% 
else
'normal widgets - standard as of 7.0.10
check_session()

uid = Session("uid")

if uid <>"" then

Server.ScriptTimeout = 120
Conn.CommandTimeout = 120
dim widgetsList
	dim w_id
	dim w_title
	dim w_link
	widgetsList = "["
	Set Widgets = Conn.Execute("SELECT we.id, w.title, w.href, we.parameters FROM widgets_editors we LEFT JOIN widgets w ON we.widget_id=w.id WHERE we.page='D' AND we.editor_id = "&uid)
	do while not Widgets.EOF
		w_id = Widgets("id")
		w_id = Replace(w_id,"{","")
		w_id = Replace(w_id,"}","")
		w_title = Widgets("title")
		if canShowWidget(w_title) = true then
			w_link = "widget_"& Widgets("href") &".asp"
			if Widgets("parameters") <> "" then
				w_link = w_link&"?"&Widgets("parameters")
			end if
			widgetsList = widgetsList&"['"&w_id&"','"&w_title&"','"&w_link&"']"
			Widgets.MoveNext
			if not Widgets.EOF then
				widgetsList = widgetsList&","
			end if
		end if
	loop
	widgetsList = widgetsList&"]"
	
SQL = "SELECT guide, dis_guide FROM users WHERE id="&uid
Set RSGuide = Conn.Execute(SQL)
if not RSGuide.EOF then
	if isNull(RSGuide("guide")) then
		showFirstGuide = true
	else
		showFirstGuide = false
		showGuide = RSGuide("guide")
	end if
	if isNull(RSGuide("dis_guide")) then
		guideHistory = true
	else
		if InStr(RSGuide("dis_guide"),"dashboard")>0 then
			guideHistory = false
		else
			guideHistory = true
		end if
	end if
	disabledGuides = RSGuide("dis_guide")
end if
%>
<!DOCTYPE html>
<html>
<head>
	<title>DeskAlerts</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css" />
	<link href="css/style9.css" rel="stylesheet" type="text/css">

	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>

	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
	<script type="text/JavaScript" language="JavaScript" src="jscripts/json2.js"></script>

	<script language="javascript" type="text/javascript" src="jscripts/preview.js?v=9.2.1.31"></script>

	<script language="javascript" type="text/javascript" src="functions.js"></script>

	<script language="javascript" type="text/javascript" src="jscripts/date.js"></script>

	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.cookie.js"></script>

	<script language="javascript" type="text/javascript" src="jscripts/isotope.pkgd.min.js"></script>

	<script language="javascript" type="text/javascript" src="jscripts/packery-mode.pkgd.min.js"></script>

	<script language="javascript" type="text/javascript" src="functions.js"></script>

	<style>
		.column
		{
			float: left;
			padding-bottom: 50px;
		}
		.portlet
		{
			margin: 0 1em 1em 0;
			padding: 0.2em;
		}
		.portlet-header
		{
			padding: 0.2em 0.3em;
			position: relative;
			text-align: center;
			cursor: pointer;
		}
		.portlet-toggle
		{
			position: absolute;
			top: 50%;
			right: 25px;
			margin-top: -8px;
		}
		.portlet-destroy
		{
			position: absolute;
			top: 50%;
			right: 5px;
			margin-top: -8px;
		}
		.portlet-content
		{
			padding: 0.4em;
		}
		table.chartLegend
		{
			margin-right: auto;
			margin-left: auto;
		}
	</style>

	<script language="javascript" type="text/javascript">
	

		function showWallpaperPreview(alertId) {
			var data;
			getAlertData(alertId, false, function(alertData) {
				data = JSON.parse(alertData);
			});
			var imageSrc = data.alert_html.replace(/^admin\//, "");
			var position = data.fullscreen;
			$("#preview_image_src").val(imageSrc);
			$("#preview_position").val(position);

			var params = 'width=' + screen.width;
			params += ', height=' + screen.height;
			params += ', top=0, left=0'
			params += ', fullscreen=yes';


            //openDialog2('', screen.width, screen.height);
			window.open('', 'preview_win', params);

			$("#preview_post_form").submit();
			hideLoader();
		}
		function hideLoader() {
			$.grep($(window.top.document).find("frame"), function(n, i) {
				if ($(n).attr("name") == "main") {
					var doc = $(n)[0].contentDocument || $(n)[0].contentWindow.document;
					$("#loader", doc).fadeOut(500);
					$("#shadow", doc).fadeOut(500);
				}
			});
		}
		function openDialog(alertID) {
			var url = "<%=jsEncode(alerts_folder)%>preview.asp?id=" + alertID;
			$('#linkValue').val(url);
			$('#linkValue').focus(function() {
				$('#linkValue').select().mouseup(function(e) {
					e.preventDefault();
					$(this).unbind("mouseup");
				});
			});
			$('#linkDialog').dialog('open');
		}

		function showAlertPreview(alertId) {

		    var data = new Object();

		    getAlertData(alertId, false, function(alertData) {
		        data = JSON.parse(alertData);
		    });

		    var fullscreen = parseInt(data.fullscreen);
		    var ticker = parseInt(data.ticker);
		    var acknowledgement = data.acknowledgement;

		    var alert_width = (fullscreen == 1) && !ticker ? $(".main_table_body").width() : (!ticker ? data.alert_width : $(".main_table_body").width());
		    var alert_height = (fullscreen == 1) && !ticker ? $(window).height() - document.body.clientHeight + $(".main_table_body").height() : (!ticker ? data.alert_height : "");
		    var alert_title = data.alert_title;
		    var alert_html = data.alert_html;

		    var top_template;
		    var bottom_template;
		    var templateId = data.template_id;

		    if (templateId >= 0) {
		        getTemplateHTML(templateId, "top", false, function(data) {
		            top_template = data;
		        });

		        getTemplateHTML(templateId, "bottom", false, function(data) {
		            bottom_template = data;
		        });
		    }
		    data.top_template = top_template;
		    data.bottom_template = bottom_template;
		    data.alert_width = alert_width;
		    data.alert_height = alert_height;
		    data.ticker = ticker;
		    data.fullscreen = fullscreen;
		    data.acknowledgement = acknowledgement;
		    data.alert_title = alert_title;
		    data.alert_html = alert_html;
		    data.caption_href = data.caption_href;
		    initAlertPreview(data);
		};

		function detectIE() {
			var ua = window.navigator.userAgent;
			var msie = ua.indexOf('MSIE ');
			var trident = ua.indexOf('Trident/');

			if (msie > 0) {
				// IE 10 or older => return version number
				return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
			}

			if (trident > 0) {
				// IE 11 (or newer) => return version number
				var rv = ua.indexOf('rv:');
				return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
			}

			// other browser
			return false;
		}

		function getOrder() {
			var str = "";
			var items = $("#statList").sortable("toArray");

			for (var i = 0, n = items.length; i < n; i++) {
				var v = $('#' + items[i]).find('.portlet-content').is(':visible');
				items[i] = items[i] + "," + v;
				if (i != 0) str = str + "," + items[i];
				else str = str + items[i];
			}

			$.get("set_parameters.asp?name=setDashboardListOrder&dashboardListOrder="+str);
		}

		function restoreOrder() {
		    $.get("set_parameters.asp?name=getDashboardListOrder", function(data){
		        var list = $("#statList");
		        if (list == null) return
		        var cookie = data;
		        if (!cookie) return;
		        var IDs = cookie.split(',');
		        var rebuild = new Array();
		        var items = list.sortable("toArray");

		        for (var v = 0, len = items.length; v < len; v++) {
		            rebuild[items[v]] = items[v];
		        };

		        for (var i = 0, n = IDs.length; i < n - 1; i++) {
		            var itemID = IDs[i];
		            var visible = IDs[i + 1]; i = i + 1;
		            if (itemID in rebuild) {
		                var item = rebuild[itemID];
		                var child = $("div.ui-sortable").children("#" + item);
		                var savedOrd = $("div.ui-sortable").children("#" + itemID);
		                $("div.ui-sortable").filter(":first").append(savedOrd);
		                if (visible === 'false') {
		                    child.find(".ui-icon").toggleClass("ui-icon-minusthick ui-icon-plusthick");
		                    child.find(".portlet-content").hide();
		                }
		                else {
		                    child.find(".portlet-content").show();
		                };
		            };
		        };
		    });
		};

		function addWidgetToPage(id, title, link) {
			$('#statList').append("<div class='add_here' id='" + id + "'></div>");
			iframeSize = getWidgetWidth(title);
			portletSize = iframeSize=="100%"?iframeSize:iframeSize+10;
			$('.add_here').first().append('<div class="portlet-header overall_link">' + title + '</div>' +
					'<div class="portlet-content">' +
					'<iframe class="widget_iframe" id="i' + id + '" width="'+ iframeSize +'" frameborder="0" onLoad="autoHeight(\'i' + id + '\')" src="' + link + '"/>' +
					'</div>' +
					'</div>').addClass("portlet").addClass("column").width(portletSize).addClass("isotopey").removeClass("add_here")
					.addClass("ui-widget ui-widget-content ui-helper-clearfix ui-corner-all")
					.find(".portlet-header")
					.addClass("ui-widget-header ui-corner-all")
					.prepend("<span class='ui-icon ui-icon-minusthick portlet-toggle'></span>")
					.prepend("<span class='ui-icon ui-icon-closethick portlet-destroy'></span>");
		}

		function changeIframeSrc(id, newsrc) {
			document.getElementById(id).src = newsrc;
		}

		function autoHeight(id) {
			var height = document.getElementById(id).height;
			var newheight;
			if (document.getElementById) {
				scrollheight = document.getElementById(id).contentWindow.document.body.scrollHeight;
				offsetheight = document.getElementById(id).contentWindow.document.body.offsetHeight;
				//alert("scroll" + scrollheight);
				//alert("offset" + offsetheight);
				if (scrollheight>offsetheight+50 && offsetheight>0){
					newheight = offsetheight + 25;
				}
				else{
					newheight = scrollheight;
				}
			}
			var ie = detectIE();
			if (ie != false) {
				newheight = newheight + 20;
			}
			
			document.getElementById(id).height = (newheight) + "px";
			if (height!=newheight){
				 $("#statList").isotope('reloadItems').isotope({ sortBy: 'original-order'});
			}
		}

		$(document).ready(function(){
			var list = $('#statList');
			list.isotope({  
			  transformsEnabled: false
			  , itemSelector: '.isotopey'
			  , layoutMode:'packery'
			});
			list.sortable({
			connectWith: "#statList",
			cursor: 'move'
			, start: function(event, ui) {                        
				ui.item.addClass('grabbing moving').removeClass('isotopey');
				ui.placeholder
				  .addClass('starting')
				  .removeClass('moving')
				  .css({
					top: ui.originalPosition.top
					, left: ui.originalPosition.left
				  })
				  ;
				list.isotope('reloadItems');
			  }
			, change: function(event, ui) {
				ui.placeholder.removeClass('starting');
				list
				.isotope('reloadItems')
				.isotope({ sortBy: 'original-order'})
			;
			}
			, beforeStop: function(event, ui) {
				ui.placeholder.after(ui.item);
			  }
			, update: function() { getOrder(); },
			stop: function(event, ui) {      
			ui.item.removeClass('grabbing').addClass('isotopey');
			list
			  .isotope('reloadItems')
			  .isotope({ sortBy: 'original-order' }, function(){
				if (!ui.item.is('.grabbing')) {
				  ui.item.removeClass('moving');
				}
			  })
			  ;
		  }
		});
            $(".portlet")
            .addClass("ui-widget ui-widget-content ui-helper-clearfix ui-corner-all")
            .find(".portlet-header")
            .addClass("ui-widget-header ui-corner-all")
            .prepend("<span class='ui-icon ui-icon-minusthick portlet-toggle'></span>")
            .prepend("<span class='ui-icon ui-icon-closethick portlet-destroy'></span>");
			$('#statList').on('click','.portlet-toggle',function(){
				$(this).toggleClass("ui-icon-minusthick").toggleClass("ui-icon-plusthick");
                $(this).parents(".portlet:first").find(".portlet-content").toggle();
                var frame_id = $(this).parents(".portlet:first").attr("id");
                autoHeight("i"+frame_id);
                var isIE = detectIE();
                if ($(this).parents(".portlet:first").find(".portlet-content").is(":visible") && isIE!=false){
					document.getElementById("i"+frame_id).contentDocument.location.reload(true);	
                }
                $("#statList").isotope('reloadItems').isotope({ sortBy: 'original-order'});
                getOrder();
			});
			$('#statList').on('click','.portlet-destroy',function(){
				var item_id = $(this).parents(".portlet:first").attr("id");
				var jqxhr = $.get("manipulate_widgets.asp",{id:item_id, action: "remove"})
				$(this).parents(".portlet:first").remove();
				$("#statList").isotope('reloadItems').isotope({ sortBy: 'original-order'});
                getOrder();
			});
			initWidgetsForEditor();
            restoreOrder();
            $(".widget_iframe").load(function(){
				$("#statList").isotope('reloadItems').isotope({ sortBy: 'original-order'});
			});

			$("#add_another").button({label:"<%=LNG_ADD_WIDGET %>",icons: {primary: "ui-icon-newwin"}}).click(function(){ $("#add_widget_form").dialog("open");});
			//$("#invite").button({label:"<%=LNG_INV %>",icons: {primary: "ui-icon-newwin"}}).click(function(){ $("#invite_dialog_form").dialog("open");});
			$("#send_btn").button({label:"<%=LNG_SEND %>",icons: {primary: "ui-icon-newwin"}}).click(function(){ sendEmail(); });
			$("#channels_link").button();

			function sendEmail()
			{
				
				var email = $("#email_box").val();
				
				if(email.length == 0 ||  email.indexOf("@") == -1 ||  email.indexOf(".") == -1)
				{
					alert('<% =LNG_WRONG_EMAIL%>');
				}
				else{
				
					$.get("mail_sender.asp?email="+email, function(data)
					{
						alert(data);
						$("#invite_dialog_form").dialog("close");
					});
				}
				
			}
			$("#invite_dialog_form").dialog(
			{
				autoOpen: false,
				height: 145,
				width: 600,
				modal: true,
				resizable: false
			
			});
			$("#add_widget_form").load("add_widget.asp?page=dashboard").dialog({
				title: "<%=LNG_ADD_WIDGET %>",
				position: {my: "left top", at: "left top", of: "#dialog_dock"},
				modal: true,
				autoOpen:false,
				resizable: false,
				draggable: false,
				width: 'auto',
				resize: 'auto',
				margin:'auto'
			});
			
			$("#acceptGuide").button({label:"Yes, please show me around"}).click(function(){
				$.get("set_settings.asp",{name:"UserGuideMode", value: "new"});
				$("#guideDialog").dialog("close");
				showGuide();
			});
			
			$("#rejectGuide").button({label:"No, I'll figure this out"}).click(function(){
				$.get("set_settings.asp",{name:"UserGuideMode", value: "none"});
				$("#guideDialog").dialog("close");
			});
			
			function initWidgetsForEditor(){
				var widgetsList = <%= widgetsList%>;
				for (i=0;i<widgetsList.length;i++){
					id = widgetsList[i][0];
					title = widgetsList[i][1];
					link = widgetsList[i][2];
					addWidgetToPage(id,title,link);
				}
			}

			$("#linkDialog").dialog({
				autoOpen: false,
				height: 80,
				width: 550,
				modal: true,
				resizable: false
			});
			
			function showFirstGuide(){
				$("#guideDialog").dialog("open");
				$("#firstGuideDialog").show();
				$("#commonGuideDialog").hide();
			}
			
			$("#guideDialog").dialog({
				autoOpen:false,
				resizable:false,
				draggable:false,
				modal:true,
				width:400,
				height:200,
				close: function(){closeGuide();}
			});
			
			function showGuide(){
				$("#guideDialog").dialog("option","height",250);
				$("#guideDialog").dialog("open");
				$("#firstGuideDialog").hide();
				$("#commonGuideDialog").show();
			}
			
			function closeGuide(){
				var disabledGuides = "<%=disabledGuides %>"
				disabledGuides += "dashboard,";
				$.get("set_settings.asp",{name:"DisabledGuides", value: disabledGuides});
			}
			
			function checkHistoryGuide(){
				<% if guideHistory=true then %>
				showGuide();
				<% end if %>
			}
			
			<% if showFirstGuide=true then %>
				showFirstGuide();
			<% elseif showGuide="new" then%>
				checkHistoryGuide();
			<% elseif showGuide="all" then %>
				showGuide();
			<% end if %>
		})

	</script>

</head>
<body style="margin:6px;padding:0px;border: 1px solid #b3b3b7"  class="body dashboard_body">
	<table width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td class="main_table_title" style="padding:3px 0px 0px 7px"><img src="images/menu_icons/contentschedule_20.png" alt="" width="20" height="20" border="0"></td><td width="100%" height="31" class="main_table_title"><span class="header_title" ><%=LNG_DASHBOARD %></span></td>
		</tr>
	</table>
  <div id="dialog-form" style="overflow: hidden; display: none;">
        <iframe id='dialog-frame' style="width: 100%; height: 100%; overflow: hidden; border: none;
            display: none"></iframe>
    </div>
    <div id="secondary" style="overflow: hidden; display: none;">
    </div>
    
	<div id="dialog_dock" class="dashboard_field">

		<button id="add_another" style="margin-bottom: 10px;float:left">
			Add Widget</button>
			

			

		
		<div style="clear:both;width:100%"></div>
		<div id="add_widget_form">
		</div>
		
		<div id="invite_dialog_form">
			<label><% =LNG_ENTER_USER_MAIL %></label><br /><br />
			<label for="email_box"><% =LNG_EMAIL & ":"%></label>
			<input type="text" id ="email_box" style="float:center" align="center" ><br>
			<button id="send_btn" style="float:right"><% =LNG_SEND%></button>
		</div>
		<div id="statList">
		</div>
	</div>
	<form style="margin: 0px; width: 0px; height: 0px; padding: 0px" id="preview_post_form"
	action="wallpaper_preview.asp" method="post" target="preview_win">
	<input type="hidden" id="preview_image_src" name="image_src" value="" />
	<input type="hidden" id="preview_position" name="position" value="" />
	</form>
	<div id="guideDialog" title="DeskAlerts Beginner's Guide" style="display: none">
		<div id="firstGuideDialog" style="display:none">
			<p>
				DeskAlerts v8 Control Panel has an integrated guide which will give you some hints on basic operations you can perform within it. Do you want to receive these advices? You can later change the guide behavior from the Profile Settings page, which you have in the side menu on the left.
			</p>
			<a href="#" id="acceptGuide" style="display:inline-block"></a>
			<a href="#" id="rejectGuide" style="display:inline-block"></a>
		</div>
		<div id="commonGuideDialog" style="display:none">
			<p>
				DeskAlerts Control Panel Interface consists of a few panels. On the left, you can see a main menu - giving you access to the different product pages. On the right there is a help panel - it can be collapsed by using the "Help" button on top of the page. The zone you're reading this message in is your main working area.
			</p>
			<p>
				This page is a Dashboard - you can fill it with various widgets using the "Add Widget" button. It will bring up a list of widget tiles with a short descriptions being shown if you hover your mouse over a tile. You can add any widgets you need, but be aware that using too much of them at a time can significantly slow down the page loading.
			</p>
		</div>
	</div>
	<div id="linkDialog" title="<%= LNG_DIRECT_LINK %>" style="display: none">
		<input id="linkValue" type="text" readonly="readonly" style="width: 500px" />
	</div>
	<iframe id="preview_frame" name="preview_frame" frameborder="0"></iframe>
</body>
</html>
<%
else 
    Response.Redirect "settings_edit_profile.asp"    
end if

end if
%>
<!-- #include file="db_conn_close.asp" -->
