﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%

check_session()


%>

<%

  uid = Session("uid")

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>DeskAlerts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/style9.css" rel="stylesheet" type="text/css">
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script language="javascript" type="text/javascript" src="functions.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/date.js"></script>
<script type="text/JavaScript" language="JavaScript" src="jscripts/json2.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/preview.js?v=9.2.1.31"></script>
<script language="javascript" type="text/javascript">

var serverDate;
var settingsDateFormat = "<%=GetDateFormat()%>";
var settingsTimeFormat = "<%=GetTimeFormat()%>";

var responseDbFormat = "dd/MM/yyyy HH:mm:ss";
var uiFormat = getUiDateTimeFormat(settingsDateFormat,settingsTimeFormat);

function openDialogUI(_form, _frame, _href, _width, _height, _title) {
    $("#dialog-" + _form).dialog('option', 'height', _height);
    $("#dialog-" + _form).dialog('option', 'width', _width);
    $("#dialog-" + _form).dialog('option', 'title', _title);
    $("#dialog-" + _frame).attr("src", _href);
    $("#dialog-" + _frame).css("height", _height);
    $("#dialog-" + _frame).css("display", 'block');
    if ($("#dialog-" + _frame).attr("src") != undefined) {
        $("#dialog-" + _form).dialog('open');
        $("#dialog-" + _form).contents().find('#table_border').css('border', 'none');
    }
}



$(document).ready(function() {

var width = $(".data_table").width();
var pwidth = $(".paginate").width();
if (width != pwidth) {
    $(".paginate").width("100%");
}

    $("#dialog-form").dialog({
        autoOpen: false,
        height: 100,
        width: 100,
        modal: true
    });


    $('#dialog-frame').on('load', function() {
        $("#dialog-frame").contents().find('.prelink_stats').on('click', function(e) {
            e.preventDefault();
            var link = $(this).attr('href');
            var title = $(this).attr('title');
            var last_id = $('.dialog_forms').size();
            var elem = '<div id="dialog-form-' + last_id + '" class="dialog_forms" style="overflow:hidden;display:none;"><iframe id="dialog-frame-' + last_id + '" style="display:none;width:100%;height:auto;overflow:hidden;border : none;"></iframe></div>';
            $('#secondary').append(elem);
            $("#dialog-form-" + last_id).dialog({
                autoOpen: false,
                height: 100,
                width: 100,
                modal: true
            });
            openDialogUI("form-" + last_id, "frame-" + last_id, link, 350, 350, title);
        });
    });
});
	    

$(function(){
	serverDate = new Date(getDateFromAspFormat("<%=now_db%>",responseDbFormat));
	setInterval(function(){serverDate.setSeconds(serverDate.getSeconds()+1);},1000);
	
	$(".delete_button").button({
	});
	
	$(".add_alert_button").button({
		icons: {
			primary: "ui-icon-plusthick"
		}
	});
	$("#linkDialog").dialog({
		autoOpen: false,
		height: 80,
		width: 550,
		modal: true,
		resizable: false
	});
});

function openDialog(alertID) {
	var url = "<%=jsEncode(alerts_folder)%>preview.asp?id=" + alertID;
	$('#linkValue').val(url);
	$('#linkValue').focus(function() {
		$('#linkValue').select().mouseup(function(e) {
			e.preventDefault();
			$(this).unbind("mouseup");
		});
	});
	$('#linkDialog').dialog('open');
}
	
function showAlertPreview(alertId)
{
	var data = new Object();
	
	getAlertData(alertId,false,function(alertData){	
		data = JSON.parse(alertData);
	});

	var fullscreen = parseInt(data.fullscreen);
	var ticker = parseInt(data.ticker);
	var acknowledgement = data.acknowledgement; 
	
	var alert_width = (fullscreen == 1) && !ticker ? $(".main_table_body").width() : (!ticker ? data.alert_width : $(".main_table_body").width());
	var alert_height = (fullscreen == 1) && !ticker ? $(window).height() - document.body.clientHeight + $(".main_table_body").height() : (!ticker ? data.alert_height : "");
	var alert_title = data.alert_title;
	var alert_html = data.alert_html;

	var top_template;
	var bottom_template;
	var templateId = data.template_id;
	
	if (templateId>=0)
	{
		getTemplateHTML(templateId,"top",false,function(data){ 
				top_template = data;
		});
		
		getTemplateHTML(templateId,"bottom",false,function(data){ 
			bottom_template = data;
		});
	}

	data.top_template = top_template;
	data.bottom_template = bottom_template;
	data.alert_width = alert_width;
	data.alert_height = alert_height;
	data.ticker = ticker;
	data.fullscreen = fullscreen;
	data.acknowledgement = acknowledgement;
	data.alert_title = alert_title;
	data.alert_html = alert_html;
	data.caption_href=data.caption_href;
	
	initAlertPreview(data);
	
	return false;
}
	
function showPrompt(content)
{
	$("#dialog-modal > p").empty();
	$("#dialog-modal > p").append(content)
	$("#dialog-modal").dialog({
		height: 170,
		modal: true,
		resizable: false,
		draggable: false
	});
}

function createExpiredMessage(id)
{
	var $table = $("<table style='margin:auto'>" +
						"<tr>"+
							"<td colspan='2' style='text-align:center'>" +
								"<h3><%=LNG_ALERT_IS_EXPIRED%><h3>" +
							"</td>" +
						"</tr>" +
						"<tr>"+
							"<td>" +
								"<a style='white-space: nowrap' id='reschedule_button'><%=LNG_RESCHEDULE_ALERT%></a>" +
							"</td>" +
							"<td>" +
								"<a style='white-space: nowrap' id='edit_button'><%=LNG_EDIT_ALERT%></a>" +
							"</td>" +
						"</tr>" +
					"</table>");
					
	$table.find("#reschedule_button").button().bind("click",function(){
		document.location = "reschedule.asp?san=1&id="+id;
	});
	
	$table.find("#edit_button").button().bind("click",function(){
		document.location = "edit_alert_db.asp?id="+id;
	});
	
	return $table;
}

function sendAlert(id, to_date){
	var toDate = new Date(getDateFromFormat(to_date,"dd/MM/yyyy HH:mm:ss"));
	if (toDate < serverDate)
	{
		showPrompt(createExpiredMessage(id));
	}
	else
	{
		document.location = "alert_users.asp?id="+id;
	}
}

</script>
</head>
<%
   if(Request("offset") <> "") then 
	offset = clng(Request("offset"))
   else 
	offset = 0
   end if	

   if(Request("sortby") <> "") then 
	sortby = Request("sortby")
   else 
	sortby = "create_date DESC"
   end if

if (uid <>"") then

'counting pages to display
   if(Request("limit") <> "") then 
	limit = clng(Request("limit"))
   else 
	limit=25
   end if

'  limit=10

	'--- rights ---
	set rights = getRights(uid, true, false, true, "alerts_val")
	gid = rights("gid")
	gviewall = rights("gviewall")
	gviewlist = rights("gviewlist")
	alerts_arr = rights("alerts_val")
	'--------------

	if(alerts_arr(2)="") then
		linkclick_str = "not"
	else
		linkclick_str = LNG_ALERTS
	end if

	if gviewall <> 1 then
		Set RS = Conn.Execute("SELECT COUNT(1) as mycnt FROM alerts WHERE type='D' AND (class = 1 OR class = 16 OR class = 999) AND video = 0 AND (NOT param_temp_id IS NULL OR param_temp_id IS NULL) AND sender_id IN ("& Join(gviewlist,",") &")")
	else
		Set RS = Conn.Execute("SELECT COUNT(1) as mycnt FROM alerts WHERE type='D' AND (class = 1 OR class = 16 OR class = 999) AND video = 0")
	end if
	cnt=0

	if(Not RS.EOF) then
		cnt=RS("mycnt")
	end if

	RS.Close

  j=cnt/limit


%><body style="margin:0" class="body">
 <div id="dialog-form" style="overflow: hidden; display: none;">
        <iframe id='dialog-frame' style="width: 100%; height: 100%; overflow: hidden; border: none;
            display: none"></iframe>
    </div>
    <div id="secondary" style="overflow: hidden; display: none;">
    </div>
<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	<tr>
	<td>
	<table width="100%" height="100%" bordercolor="#000000" cellspacing="0" cellpadding="0" class="main_table">
		<tr>
		<td width=100% height=31 class="main_table_title"><img src="images/menu_icons/alert_20.png" alt="" width="20" height="20" border="0" style="padding-left:7px">
		<a href="draft.asp" class="header_title" style="position:absolute;top:15px"><%=LNG_ALERTS %></a></td>
		</tr>
		<tr>
			<td height="100%" class="main_table_body">
				<div style="margin:10px">
					<span class="work_header"><%=LNG_DRAFT_ALERTS %></span>
					<table width="100%" >
						<tr>
							<td align="right">
								<% if(gid = 0 OR (gid = 1 AND alerts_arr(0)<>"")) then %>
								<a class="add_alert_button" href="edit_alert_db.asp"><%=LNG_ADD_ALERT%></a>
								<%end if%>
							</td>
						</tr>
					</table>
					<%
					if(cnt>0) then
						page="draft.asp?"
						name=LNG_ALERTS
						Response.Write make_pages(offset, cnt, limit, page, name, sortby) 
					%>
					<br/>
					<a href='#' class="delete_button" onclick="javascript: return LINKCLICK('<%=linkclick_str%>');"><%=LNG_DELETE%></a>
					<br/>
					<br/>
					<form action='delete_objects.asp' name='delete_objs' id='delete_objs' method='post'><input type='hidden' name='objType' value='alerts'>
						<table width="100%" height="100%" cellspacing="0" cellpadding="3" class="data_table">
							<tr class="data_table_title">
								<td width="10"><input type='checkbox' name='select_all' id='select_all' onclick='javascript: select_all_objects();'></td>
								<td width="20"><%= LNG_TYPE%></td>
								<td class="table_title"><%=sorting(LNG_ALERTS,"title", sortby, offset, limit, page) %></td>
								<td class="table_title"><%=sorting(LNG_DATE,"create_date", sortby, offset, limit, page) %></td>
								<td class="table_title"><%=LNG_SCHEDULED %></td>
								<td class="table_title"><%=LNG_ACTIONS %></td>
							</tr>
							<%
								'show main table
								if gviewall <> 1 then
									Set RS = Conn.Execute("SELECT a.id, title, alert_text, a.create_date, class, a.schedule, a.from_date, a.to_date, urgent, ticker, fullscreen, s.id as is_rsvp, recurrence FROM alerts a LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id WHERE a.type='D' AND (class = 1 OR class = 16 OR class = 999)  AND video = 0 AND (NOT param_temp_id IS NULL OR param_temp_id IS NULL) AND a.sender_id IN ("& Join(gviewlist,",") &") ORDER BY "&sortby)
								else
									Set RS = Conn.Execute("SELECT a.id, title, alert_text, a.create_date, class, a.schedule, a.from_date, a.to_date, urgent, ticker, fullscreen, s.id as is_rsvp, recurrence FROM alerts a LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id WHERE a.type='D' AND (class = 1 OR class = 16 OR class = 999)  AND video = 0 ORDER BY "&sortby)
								end if

								num=offset
								if(Not RS.EOF) then RS.Move(offset) end if
								Do While Not RS.EOF
									num=num+1
									if((offset+limit) < num) then Exit Do end if
									if(num > offset AND offset+limit >= num) then
										strObjectID=RS("id")
							%>
							<tr>
								<td><input type="checkbox" name="objects" value="<%=strObjectID%>"></td>
								<td align="center">
									<% 
										dim isUrgent
										if RS("urgent") = "1" then
											isUrgent = true
										else
											isUrgent = false
										end if
										if RS("class") = "16" then
											if isUrgent = true then
												Response.Write "<img src=""images/alert_types/urgent_unobtrusive.gif"" title=""Urgent Unobtrusive"">"
											else
												Response.Write "<img src=""images/alert_types/unobtrusive.gif"" title=""Unobtrusive"">"
											end if
										elseif RS("class") = "999" then
					                        Response.Write "<img src=""images/alert_types/web_plugin.png"" title=""Web alert"">"
										elseif not isNull(RS("is_rsvp")) then
											if isUrgent = true then
												Response.Write "<img src=""images/alert_types/urgent_rsvp.png"" title=""Urgent RSVP"">"
											else
												Response.Write "<img src=""images/alert_types/rsvp.png"" title=""RSVP"">"
											end if
										elseif RS("ticker")="1" then
											if isUrgent = true then
												Response.Write "<img src=""images/alert_types/urgent_ticker.png"" title=""Urgent Ticker"">"
											else
												Response.Write "<img src=""images/alert_types/ticker.png"" title=""Ticker"">"
											end if
										elseif RS("fullscreen")="1" then
											if isUrgent = true then
												Response.Write "<img src=""images/alert_types/urgent_fullscreen.png"" title=""Urgent Fullscreen"">"
											else
												Response.Write "<img src=""images/alert_types/fullscreen.png"" title=""Fullscreen"">"
											end if
										elseif isUrgent = true then
											Response.Write "<img src=""images/alert_types/urgent_alert.gif"" title=""Urgent Alert"">"
										else
											Response.Write "<img src=""images/alert_types/alert.png"" title=""Usual Alert"">"
										end if
									%>
								</td>
								<td>
										<%
											if RS("title") = "" then
												Response.Write Left(DeleteTags(RS("alert_text")), 50)
											else
												Response.Write HtmlEncode(RS("title"))
											end if
										%>
								</td>
								<td> 
									<script language="javascript">
										document.write(getUiDateTime('<%=RS("create_date")%>'));
									</script>
								</td>
								<%
									jsSend = 0
									reschedule = 0
									if(Not IsNull(RS("schedule"))) then
										schedule = RS("schedule")
										if(schedule="1") then
											start_date = RS("from_date")
											end_date = RS("to_date")
											if start_date <> "" and end_date <> "" then
												if year(end_date) <> 1900 then
													if (MyMod(DateDiff("s", start_date, end_date),60)) <> 1 then 'if lifetime - update time
														jsSend = 1 
														reschedule = 1
													end if
												else
													reschedule = 1
												end if
											else
												reschedule = 1
											end if
										end if
									end if
								%>
								<td style="text-align: center">
									<% 
									if (RS("recurrence")="1") or reschedule = 1 then
										'Response.Write LNG_YES
									%>
									<a href="#" onclick="openDialogUI('form','frame','reschedule.asp?id=<%=RS("id") %>&ro=1',600,600,'');"><%=LNG_YES%></a>	
									<%
									else
										Response.Write LNG_NO
									end if
									%>
								</td>
								<td align="center" width="120" nowrap="nowrap">
										<% if(gid = 0 OR (gid = 1 AND alerts_arr(3)<>"")) then %>
										<a href="<%
											if jsSend = 0 then
												Response.Write "alert_users.asp?id="&RS("id")
											else
												Response.Write "javascript: sendAlert('"&RS("id")&"','"&end_date&"');"
											end if
										%>"><img src="images/action_icons/send.png" alt="<%=LNG_SEND_ALERT %>" title="<%=LNG_SEND_ALERT %>" width="16" height="16" border="0" hspace="5"/></a>	
										<%end if%>
										<% if(gid = 0 OR (gid = 1 AND alerts_arr(1)<>"")) AND (RS("class") = "1" OR RS("class") = "16" OR RS("class") = "999") then %>
									<a href="edit_alert_db.asp?id=<%=RS("id") %>"><img src="images/action_icons/edit.png" alt="<%=LNG_EDIT_ALERT %>" title="<%=LNG_EDIT_ALERT %>" width="16" height="16" border="0" hspace="5"/></a>
									<% Response.Write "<a href='#' onclick=""javascript: showAlertPreview(" & CStr(RS("id")) & ")""><img src=""images/action_icons/preview.png"" alt="""& LNG_PREVIEW &""" title="""& LNG_PREVIEW &""" width=""16"" height=""16"" border=""0"" hspace=""5""/></a>" %>
									<a href="#" onclick="openDialog(<%=RS("id") %>)"><img src="images/action_icons/link.png" alt="<%=LNG_DIRECT_LINK%>" title="<%=LNG_DIRECT_LINK%>" width="16" height="16" border="0" hspace="6"/></a>
										<%end if%>
								</td>
							</tr>
							<%
									end if
									RS.MoveNext
									Loop
								RS.Close
							%>
						</table>
						<input type='hidden' name='back' value='draft'>
					</form>
					<br/>
					<A href='#' class="delete_button" onclick="javascript: return LINKCLICK('<%=linkclick_str%>');"><%=LNG_DELETE%></a>
					<br/>
					<br>
				<%
					Response.Write make_pages(offset, cnt, limit, page, name, sortby) 
					else
						Response.Write "<center><b>"&LNG_THERE_ARE_NO_ALERTS&".</b><br><br></center>"
					end if

				%>
					<img src="images/inv.gif" alt="" width="1" height="100%" border="0">
				</div>
			</td>
		</tr>
	</table>
	</td>
	</tr>
</table>
<%
else

  Response.Redirect "index.asp"

end if
%>
<div id="dialog-modal" title="" style='display:none'>
		<p></p>
</div>
<div id="linkDialog" title="<%= LNG_DIRECT_LINK %>" style="display:none">
	<input id="linkValue" type="text" readonly="readonly" style="width:500px" />
</div>
</body>
</html>
<!-- #include file="db_conn_close.asp" -->