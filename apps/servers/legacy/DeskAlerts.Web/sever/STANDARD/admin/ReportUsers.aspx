﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReportUsers.aspx.cs" Inherits="DeskAlertsDotNet.Pages.ReportUsers" %>

<%@ Import Namespace="DeskAlertsDotNet.Parameters" %>
<%@ Import Namespace="Resources" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>DeskAlerts</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="css/style9.css" rel="stylesheet" type="text/css"/>
    <link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
    <link href="css/jquery/jquery_timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="functions.js"></script>
    <script type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
    <script type="text/javascript" src="jscripts/date.js"></script>
    <script type="text/javascript" src="jscripts/jquery/jquery_timepicker/jquery-ui-timepicker-addon.js"></script>
    <script type="text/javascript" src="jscripts/jquery/datepicker_fix.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".delete_button").button({
            });

            $("#selectAll").click(function () {
                var checked = $(this).prop('checked');
                $(".content_object").prop('checked', checked);
            });
        });

        function showPreview(id) {
            window.open('../UserDetails.aspx?id=' + id, '', 'status=0, toolbar=0, width=350, height=350, scrollbars=1');
        }

        function editUser(id, queryString) {
            location.replace('../EditUsers.aspx?id=' + id + '&return_page=users_new.asp?' + queryString);
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
            <tr>
                <td>
                    <table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
                        <tr>
                        </tr>
                        <tr>
                            <td class="main_table_body" height="100%">
                                <br />
                                <br />
                                <div style="margin-left: 10px" runat="server" id="mainTableDiv">

                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td>
                                                <div style="margin-left: 10px" id="topPaging" runat="server"></div>
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                    <br />
                                    <br />
                                    <asp:Table Style="padding-left: 0px; margin-left: 10px; margin-right: 10px;" runat="server" ID="contentTable" Width="98.5%" Height="100%" CssClass="data_table" CellSpacing="0" CellPadding="3">
                                        <asp:TableRow CssClass="data_table_title">
                                            <asp:TableCell runat="server" ID="userNameCell" CssClass="table_title"></asp:TableCell>
                                            <asp:TableCell runat="server" ID="statusCell" CssClass="table_title" Width="150"></asp:TableCell>
                                            <asp:TableCell runat="server" ID="registeredDateCell" CssClass="table_title" Width="150"></asp:TableCell>
                                            <asp:TableCell runat="server" ID="lastActivityCell" CssClass="table_title" Width="150"></asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                    <br />
                                    <br />
                                    <br />
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td>
                                                <div style="margin-left: 10px" id="bottomPaging" runat="server"></div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div runat="server" id="NoRows">
                                    <br />
                                    <br />
                                    <center><b><% =resources.LNG_THERE_ARE_NO_USERS %></b></center>
                                </div>
                                <br />
                                <br />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
