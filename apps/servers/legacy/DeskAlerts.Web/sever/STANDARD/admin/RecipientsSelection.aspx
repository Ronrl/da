﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RecipientsSelection.aspx.cs" Inherits="DeskAlertsDotNet.Pages.RecipientsSelection" %>

<%@ Import Namespace="DeskAlertsDotNet.Parameters" %>
<%@ Import Namespace="Resources" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link href="css/style9.css" rel="stylesheet" type="text/css">
    <link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
    <script type="text/javascript">
        $(document)
            .ready(function () {

                var height = $(window.document).height();
                window.parent.parent.parent.onChangeBodyHeight(height, false);

                $(".send_button").button();


                $(".tree_class")
                    .each(function () {

                        var link = $(this).attr("href");

                        if (typeof (link) != "undefined") {

                            $(this)
                                .click(function () {

                                    $(".tree_class").css("background-color", "white");
                                    $(".tree_class").css("color", "black");

                                    $(".root_node").css("background-color", "white");
                                    $(".root_node").css("color", "black");

                                    $(this).css("background-color", "#010161");
                                    $(this).css("color", "white");

                                });
                        }

                    });

                $(".root_node")
                    .click(function () {

                        $(this).css("background-color", "#010161");
                        $(this).css("color", "white");


                        $(".tree_class").css("background-color", "white");
                        $(".tree_class").css("color", "black");
                    });



                var sendingType = '<% =SendingType%>';


                if (sendingType.length > 0) {

                    $('#objectType').val(sendingType);

                    changeObjectType(false);
                }

            });

        var simpleGroupSending = <%= Settings.Content["SimpleGroupSending"]  %> === 1;

        $(function () {
            function unselectAllGroups() {
                const selectAllCheckbox = $("#simpleGroupSendingFrame").contents().find("#selectAll");

                if (selectAllCheckbox.prop("checked")) {
                    selectAllCheckbox.click();
                } else {
                    selectAllCheckbox.click();
                    selectAllCheckbox.click();
                }
            }

            if (simpleGroupSending) {

                $('#sendType').val("recipients");

                $("#simpleGroupSelection").css("display", "grid");
                $("#simpleGroupSelectionButton").show();

                ShowGreyBackground();

                $("#simpleGroupSelection").dialog({
                    resizable: false,
                    width: 400,
                    height: 450,
                    closeOnEscape: false,
                    open: function (event, ui) {
                        $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
                    },
                    draggable: false,
                    close: HideGreyBackground,
                    buttons:
                    {
                        '<%=resources.LNG_CANCEL%>': function () {
                            $("#simpleGroupSelection").dialog("close");
                            HideGreyBackground();
                            unselectAllGroups();
                        },
                        '<%= resources.LNG_LIST_OF_RECIPIENTS%>': function () {
                            showSelectedRecipients();
                        },
                        '<%=resources.LNG_SEND%>': function () {
                            if (BeforeSendingAction()) {
                                $("#simpleGroupSelection").dialog("close");
                                HideGreyBackground();
                                submitForm();
                            }
                        },
                    },

                });

                $("#simpleGroupSelectionButton").button().on("click", function () {
                    ShowGreyBackground();
                    $('#sendType').val("recipients");
                    $("#simpleGroupSelection").dialog("open");
                });
            }

        });

        function HideGreyBackground() {
            if (!(($("#simpleGroupSelection").hasClass("ui-dialog-content") && $("#simpleGroupSelection").dialog("isOpen")) && document.getElementById("showRecipientsDiv").style.display == "none")) {
                document.getElementById("hideAll").style.display = "none";
            }
        }

        function ShowGreyBackground() {
            document.getElementById("hideAll").style.display = "";
        }


        function UpdateAllChildren(nodes, checked) {
            var i;
            for (i = 0; i < nodes.get_count(); i++) {
                if (checked) {
                    nodes.getNode(i).check();
                }
                else {
                    nodes.getNode(i).set_checked(false);
                }

                if (nodes.getNode(i).get_nodes().get_count() > 0) {
                    UpdateAllChildren(nodes.getNode(i).get_nodes(), checked);
                }
            }
        }

        function openDialog() {

            $("#createTemplateDialog")
                .dialog({
                    autoOpen: false,
                    height: 155,
                    width: 270,
                    modal: true,
                    resizable: false
                })
                .dialog("open");

        }

        function addGroup() {

            prepareRecipientsForTemplate();

            $.ajax({
                type: 'POST',
                url: 'CreateCustomGroup.aspx',
                data: $('#templateForm').serialize(),
                success: function () {

                    submitForm();
                }
            });



        }

        function prepareRecipientsForTemplate() {
            $(".added_object_class")
                .each(function () {


                    var recipientId = $(this).val().replace(/object/g, "");
                    var objType = "";
                    if (recipientId.indexOf("u") !== -1) {
                        objType = "#users";
                    }
                    else if (recipientId.indexOf("g") !== -1) {
                        objType = "#groups";
                    }
                    else if (recipientId.indexOf("c") !== -1) {
                        objType = "#computers";
                    }
                    else if (recipientId.indexOf("OU") !== -1) {
                        objType = "#ous";
                    }

                    recipientId = recipientId.substring(1);

                    var currentTemplateObjects = $(objType).val();
                    currentTemplateObjects += (recipientId + ",");
                    $(objType).val(currentTemplateObjects);


                });
        }


        function BeforeSendingAction() {

            var validated = ValidateRecipients();

            if (!validated)
                return false;


            if ($("#objectType").val() !== "B" && $("#saveAsCustomGroup").prop("checked")) {
                openDialog();
                return false;
            };

            return true;
        }

        function ValidateRecipients() {

            if ($("#objectType").val() === "0" && $("#selectedTemplate").val() === "") {
                alert("<% =resources.LNG_PLEASE_CHOOSE_THE_TYPE%>");
                return false;
            }
            if ($("#added_recipients").children().length === 0 && $("#objectType").val() !== "B") {
                alert("<% = resources.LNG_PLEASE_SELECT_RECIPIENTS_BEFORE_YOU_PROCEED%>");
                return false;
            };

            return true;
        }

        function clientNodeChecked(sender, eventArgs) {
            var childNodes = eventArgs.get_node().get_nodes();
            var isChecked = eventArgs.get_node().get_checked();
            UpdateAllChildren(childNodes, isChecked);
        }

        function changeUsersUnit(type, id) {
            var selectedVal = $('#objectType').val();


            if (selectedVal === "R") {

                var newSrc = "";
                if (id === -1) {
                    newSrc = "Recipients.aspx?sms=<%=Sms%>&email=<%=Email%>&textToCall=<%=TextToCallAlert%>&desktop=<%=Desktop%>&device=<%=Device%>&alertType=<%=alertType.Value%>";
                } else {
                    newSrc = "Recipients.aspx?sms=<%=Sms%>&email=<%=Email%>textToCall=<%=TextToCallAlert%>&desktop=<%=Desktop%>&device=<%=Device%>&alertType=<%=alertType.Value%>" +
                        "&type=" +
                        type +
                        "&id=" +
                        id;
                }

                var $frame = $('#content_iframe');

                if ($frame[0].contentWindow.getUsersFilter())
                    newSrc += ("&fu=1");

                if ($frame[0].contentWindow.getComputersFilter())
                    newSrc += ("&fc=1");

                if ($frame[0].contentWindow.getGroupsFilter())
                    newSrc += ("&fg=1");

                $('#content_iframe').attr('src', newSrc);
                checkAllSelectedRecipients();
            }
        }

        function getSelectedIpGroups() {

            return $("#added_recipients").children();
        }

        function loadAlert() {
            var alertId = '<% =AlertToLoad%>';

            if (alertId.length > 0) {

                var users = [<% =SelectedUsers%>];
                var userNames = [<% =SelectedUsersNames %>];


                var ous = [<% =SelectedOUs%>];
                var ousNames = [<% =SelectedOUsNames %>];

                var groups = [<% =SelectedGroups%>];
                var groupsNames = [<% =SelectedGroupsNames %>];

                var comps = [<% =SelectedComps%>];
                var compsNames = [<% =SelectedCompsNames%>];

                var ips = [<%=SelectedIps%>];
                var ipsNames = [<%=SelectedIpsNames%>];
                var checkbox;
                for (var i = 0; i < ips.length; i++) {

                    var objId = ips[i];
                    var ipGroupName = ipsNames[i];

                    var inputHtml = "<input type=\"hidden\" class=\"added_object_class\" recipient_name=\"" + ipGroupName + "\" name=\"added_recipient\" value=\"" + objId + "\" id=\"" + objId + "\">";

                    $("#added_recipients").append(inputHtml);

                    checkbox = $("#content_iframe")[0].contentWindow.document.getElementById("recipient_ipgroup_" + objId);


                    if (checkbox)
                        chechbox.checked = true;

                    addCount(1);

                }

                for (var i = 0; i < users.length; i++) {

                    var objId = "u" + users[i];
                    var userName = userNames[i];

                    var inputHtml = "<input type='hidden' class='added_object_class' recipient_name='" + userName + "' name='added_recipient' value='" + objId + "'  id='" + objId + "'/>";

                    $("#added_recipients").append(inputHtml);
                    checkbox = $("#content_iframe")[0].contentWindow.document
                        .getElementById("recipient_user_" + objId);
                    if (checkbox)
                        chechbox.checked = true;

                    addCount(1);

                }

                for (var i = 0; i < groups.length; i++) {

                    var objId = "g" + groups[i];
                    var userName = groupsNames[i];

                    var inputHtml = "<input type='hidden' class='added_object_class' recipient_name='" + userName + "' name='added_recipient' value='" + objId + "'  id='" + objId + "'/>";

                    $("#added_recipients").append(inputHtml);
                    checkbox = $("#content_iframe")[0].contentWindow.document
                        .getElementById("recipient_group_" + objId);
                    if (checkbox)
                        chechbox.checked = true;

                    addCount(1);

                }

                for (var i = 0; i < comps.length; i++) {

                    var objId = "c " + comps[i];
                    var userName = compsNames[i];

                    var inputHtml = "<input type='hidden' class='added_object_class' recipient_name='" + userName + "' name='added_recipient' value='" + objId + "'  id='" + objId + "'/>";

                    $("#added_recipients").append(inputHtml);
                    checkbox = $("#content_iframe")[0].contentWindow.document
                        .getElementById("recipient_computer_" + objId);
                    if (checkbox)
                        chechbox.checked = true;

                    addCount(1);

                }

                for (var i = 0; i < ous.length; i++) {

                    addOU(ous[i], ousNames[i]);
                }

                return true;
            }

            return false;
        }
        function changeObjectType(clear) {

            var webPlugin = '<%=Request["webalert"]%>';



            var selectedVal = $('#objectType').val();

            $('#templateDiv').css('display', selectedVal === "0" ? '' : 'none');

            var content_file = "";
            if (selectedVal === "R") {
                content_file = "Recipients.aspx?sms=<%=Sms%>&textToCall=<%=TextToCallAlert%>&email=<%=Email%>&desktop=<%=Desktop%>&device=<%=Device%>&alertType=<%=alertType.Value%>";
                $('#sendType').val("recipients");
                $('#upper_send').css('display', '');
                $('#broadcast_note').css('display', 'none');
                $('#ou_ui').css('display', '');
                $('#tree_td').css('display', '');


            }
            if (selectedVal === "B") {
                $('#sendType').val("broadcast");
                $('#broadcast_note').css('display', '');
            }
            if (selectedVal === "I") {

                content_file = "IpRanges.aspx?id=<% =AlertToLoad%>";
                $('#sendType').val("iprange");
                $('#upper_send').css('display', '');
                $('#tree_td').css('display', 'none');
                $('#broadcast_note').css('display', 'none');
                $('#ou_ui').css('display', '');
            }
            if (selectedVal === "0" || selectedVal === "B") {
                $('#ou_ui').css('display', 'none');
                $('#upper_send').css('display', 'none');
            }

            if (selectedVal === "U") {
                content_file = "invite.asp";
                $('#upper_send').style.display = "";
                $('#sendType').val("unreg");
                $('#tree_td').css('display', 'none');
            }
            if (webPlugin === "1") {

                content_file = "Recipients.aspx?sc=0&desktop=1&sms=<%=Sms%>&textToCall=<%=TextToCallAlert%>&webalert=1&alertType=<%=alertType.Value%>";
                $('#tree_td').css('display', 'none');

            }

            if (content_file.length > 0)
                $('#content_iframe').attr('src', content_file);

            if (clear) {
                $('objects_div').innerHTML = "";
            }

            if (clear || !loadAlert())
                removeAllRecipients();
        }

        function addCount(val) {


            var obj_cnt = parseInt(document.getElementById("obj_cnt").innerHTML, 10);

            obj_cnt += val;

            if (parseInt(obj_cnt) < 0)
                obj_cnt = "0";

            document.getElementById("obj_cnt").innerHTML = obj_cnt;

        }

        function removeAllRecipients() {

            uncheckAllOUs();
            $(".added_object_class")
                .each(function () {

                    var objId = $(this).val();
                    var objType = "ipgroup";
                    if (objId.replace(/object/g, "").indexOf("u") !== -1) {
                        objType = "user";
                    }
                    if (objId.replace(/object/g, "").indexOf("g") !== -1) {
                        objType = "group";
                    }
                    if (objId.replace(/object/g, "").indexOf("c") !== -1) {
                        objType = "computer";
                    }



                    var checkboxId = "recipient_" + objType + "_" + objId;

                    var checkBox = $('#content_iframe')[0].contentWindow.document.getElementById(checkboxId);

                    if (checkBox)
                        removeItem(checkBox);
                    else {

                        $("#" + objId).remove();
                        addCount(-1);
                    }
                    deselectRecipient(checkboxId);

                });

            $("#added_recipients").children().remove();


            var selectAllCheckbox = $("#content_iframe")[0].contentWindow.document.getElementById("selectAll");

            if (selectAllCheckbox)
                selectAllCheckbox.checked = false;

        }
        function showSelectedRecipients() {
            var obj_cnt = document.getElementById("obj_cnt");
            if (obj_cnt && obj_cnt.innerHTML > 0) {
                var recipientsHTML = '<table width="95%" border="1" bordercolor="#EAE9E1" cellspacing="0" cellpadding="3"><tr bgcolor="#EAE9E1">	<td class="table_title"><%=resources.LNG_NAME%></td>	<td class="table_title"><%=resources.LNG_TYPE%></td><td class="table_title"><%=resources.LNG_ACTIONS%></td>	</tr>';

                var addedRecipients = document.getElementsByClassName("added_object_class");
                if (addedRecipients != 0) {
                    for (var i = 0; i < addedRecipients.length; i++) {
                        var objId = addedRecipients[i].id;
                        var objType = "<%=resources.LNG_IP_GROUP%>";
                        if (objId.replace(/object/g, "").indexOf("u") !== -1) {
                            objType = "<%=resources.LNG_USER%>";
                        }
                        if (objId.replace(/object/g, "").indexOf("g") !== -1) {
                            objType = "<%=resources.LNG_GROUP%>";
                        }
                        if (objId.replace(/object/g, "").indexOf("c") !== -1) {
                            objType = "<%=resources.LNG_COMPUTER%>";
                        }
                        if (objId.replace(/object/g, "").indexOf("OU") !== -1) {

                            objType = "OU";
                        }

                        var objName = document.getElementById(objId.replace(/object/g, "object_name")).getAttribute("recipient_name");
                        objId = objId.replace(/object/g, "");
                        recipientsHTML = recipientsHTML + "<tr><td>" + objName + "</td><td>" + objType + "</td><td align='center'><img src='images/action_icons/delete.png' border='0' onclick='javascript: removeRecipient(this, \"" + objId + "\");' style='cursor: hand;' /></a></td></tr>";
                    }
                    recipientsHTML = recipientsHTML + "</table>";
                    document.getElementById("showRecipientsDivContent").innerHTML = recipientsHTML;
                    document.getElementById("showRecipientsDiv").style.display = "";
                    ShowGreyBackground();
                }
            }
            else {
                alert("<%= resources.YOU_DONT_HAVE_ANY_SELECTED_RECIPIENTS%>");
            }
        }

        function hideSelectedRecipients() {
            document.getElementById("showRecipientsDiv").style.display = "none";
            HideGreyBackground();
            var myIframe = document.getElementById("content_iframe");
            var iframeDocument = (myIframe.contentWindow || myIframe.contentDocument);
            if (iframeDocument.document) {
                iframeDocument = iframeDocument.document;
            }
        }

        function addItem(checkbox) {

            var id = $(checkbox).attr("id");
            var name = $(checkbox).val();

            var objId = id.split('_')[2];

            var inputHtml = "<input type='hidden' class='added_object_class' recipient_name='" + name + "' name='added_recipient' value='" + objId + "'  id='" + objId + "'/>";

            $("#added_recipients").append(inputHtml);
            addCount(1);
        }

        function getOUCheckBoxByOUId(ouId) {

            var treeInputs = document.getElementById("<%=orgTree.ClientID%>").getElementsByTagName("input");

            for (var i = 0; i < treeInputs.length; i++) {

                var input = treeInputs[i];

                if (input.type === "checkbox") {

                    var link = input.parentElement.getElementsByTagName("a")[0];
                    var curOuId = link.href.split(',')[1].replace(')', '').trim();

                    if (curOuId === ouId)
                        return input;
                }
            }

            return null;
        }

        function uncheckAllOUs() {
            var treeInputs = document.getElementById("<%=orgTree.ClientID%>").getElementsByTagName("input");

            for (var i = 0; i < treeInputs.length; i++) {

                var input = treeInputs[i];

                if (input.type === "checkbox") {

                    var link = input.parentElement.getElementsByTagName("a")[0];
                    var curOuId = link.href.split(',')[1].replace(')', '').trim();


                    input.checked = false;
                    $('#content_iframe')[0].contentWindow.ouStateChanged(curOuId, false);
                }
            }
        }

        function removeRecipient(obj, id) {

            var item = $("#" + id);
            var objId = $(item).val();
            var objType = "ipgroup";
            if (objId.replace(/object/g, "").indexOf("u") !== -1) {
                objType = "user";
            }
            if (objId.replace(/object/g, "").indexOf("g") !== -1) {
                objType = "group";
            }
            if (objId.replace(/object/g, "").indexOf("c") !== -1) {
                objType = "computer";
            }
            if (objId.replace(/object/g, "").indexOf("OU") !== -1) {
                objType = "ou";

                var ouId = objId.split('_')[1];
                var ouCheckbox = getOUCheckBoxByOUId(ouId);
                ouCheckbox.checked = false;
                $("#" + objId).remove();
                addCount(-1);
                $(obj).parent().parent().remove();
                $('#content_iframe')[0].contentWindow.ouStateChanged(ouId, false);
                return;
            }

            var checkboxId = "recipient_" + objType + "_" + objId;

            var checkBox = $('#content_iframe')[0].contentWindow.document.getElementById(checkboxId);

            if (checkBox)
                removeItem(checkBox);
            else {

                $("#" + id).remove();
                addCount(-1);
            }

            deselectRecipient(checkboxId);

            $(obj).parent().parent().remove();
        }

        function deselectRecipient(checkboxId) {
            setTimeout(function () {

                if (simpleGroupSending) {
                    document.getElementById("simpleGroupSendingFrame").contentWindow.unCheckRecipient(checkboxId);
                }
                $("#content_iframe")[0].contentWindow.unCheckRecipient(checkboxId);

            }, 100);
        }

        function addOU(ouId, ouName, checkBoxId) {

            var inputId = "OU_" + ouId;
            var inputHtml = "<input type='hidden' name='added_recipient' class='added_object_class' recipient_name='" + ouName + "' id='" + inputId + "' value='" + inputId + "'>";

            $("#added_recipients").append(inputHtml);
            addCount(1);

        }

        function removeOU(ouId) {
            var inputId = "OU_" + ouId;
            $("#" + inputId).remove();
            addCount(-1);
        }
        function checkAllSelectedRecipients() {

            $(".added_object_class").each(function () {
                var objId = $(this).val();
                var objType = "ip_group"
                if (objId.replace(/object/g, "").indexOf("u") !== -1) {
                    objType = "user";
                }
                if (objId.replace(/object/g, "").indexOf("g") !== -1) {
                    objType = "group";
                }
                if (objId.replace(/object/g, "").indexOf("c") !== -1) {
                    objType = "computer";
                }

                var checkboxId = "recipient_" + objType + "_" + objId;

                setTimeout(function () {
                    $("#content_iframe")[0].contentWindow.checkRecipient(checkboxId);
                },
                    100);


            });
        }

        function removeItem(checkbox) {
            var id = $(checkbox).attr("id");
            var value = $(checkbox).val();

            var objId = id.split('_')[2];

            $("#" + objId).remove();
            addCount(-1);
        }

        function submitForm() {

            $(function () {


                $("#form1").submit();

            });
        }


        function CheckUncheckChildren(childContainer, check) {
            var childChkBoxes = childContainer.getElementsByTagName("input");
            var childChkBoxCount = childChkBoxes.length;
            for (var i = 0; i < childChkBoxCount; i++) {

                var curCheckBox = childChkBoxes[i];

                if (curCheckBox.checked === check)
                    continue;

                curCheckBox.checked = check;

                var input = curCheckBox.parentElement.getElementsByTagName("a")[0];
                var ouId = input.href.split(',')[1].replace(')', '').trim();
                var chbId = curCheckBox.ID;
                var ouName = input.innerHTML;

                if (check)
                    addOU(ouId, ouName, chbId);
                else {
                    removeOU(ouId);
                }

                $('#content_iframe')[0].contentWindow.ouStateChanged(ouId, check);

            }
        }

        function CheckUncheckParents(srcChild, check) {
            var parentDiv = GetParentByTagName("div", srcChild);
            var parentNodeTable = parentDiv.previousSibling;

            if (parentNodeTable) {
                var checkUncheckSwitch;

                if (check) //checkbox checked
                {
                    var isAllSiblingsChecked = AreAllSiblingsChecked(srcChild);
                    if (isAllSiblingsChecked)
                        checkUncheckSwitch = true;
                    else
                        return; //do not need to check parent if any(one or more) child not checked
                }
                else //checkbox unchecked
                {
                    checkUncheckSwitch = false;
                }

                var inpElemsInParentTable = parentNodeTable.getElementsByTagName("input");
                if (inpElemsInParentTable.length > 0) {
                    var parentNodeChkBox = inpElemsInParentTable[0];
                    parentNodeChkBox.checked = checkUncheckSwitch;
                    //do the same recursively
                    CheckUncheckParents(parentNodeChkBox, checkUncheckSwitch);
                }
            }
        }

        function AreAllSiblingsChecked(chkBox) {
            var parentDiv = GetParentByTagName("div", chkBox);
            var childCount = parentDiv.childNodes.length;
            for (var i = 0; i < childCount; i++) {
                if (parentDiv.childNodes[i].nodeType === 1) //check if the child node is an element node
                {
                    if (parentDiv.childNodes[i].tagName.toLowerCase() === "table") {
                        var prevChkBox = parentDiv.childNodes[i].getElementsByTagName("input")[0];
                        //if any of sibling nodes are not checked, return false
                        if (!prevChkBox.checked) {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        //utility function to get the container of an element by tagname
        function GetParentByTagName(parentTagName, childElementObj) {
            var parent = childElementObj.parentNode;
            while (parent.tagName.toLowerCase() !== parentTagName.toLowerCase()) {
                parent = parent.parentNode;
            }
            return parent;
        }

        function TreeNodeChecked() {
            var checkbox = window.event.srcElement;

            if (checkbox.type === "checkbox") {
                var input = checkbox.parentElement.getElementsByTagName("a")[0];
                var ouId = input.href.split(',')[1].replace(')', '').trim();
                var chbId = checkbox.ID;
                var ouName = input.innerHTML;

                if (checkbox.checked)
                    addOU(ouId, ouName, chbId);
                else {
                    removeOU(ouId);
                }
                $('#content_iframe')[0].contentWindow.ouStateChanged(ouId, checkbox.checked);
                var parentTable = GetParentByTagName("table", checkbox);
                var nxtSibling = parentTable.nextSibling;
                if (nxtSibling && nxtSibling.nodeType === 1) {
                    if (nxtSibling.tagName.toLowerCase() === "div") {
                        CheckUncheckChildren(parentTable.nextSibling, checkbox.checked);
                    }
                }

            }
        }
    </script>
</head>
<body class="AddResipientsBody">
    <form id="form1" runat="server" action="AddRecipients.aspx" method="POST">
        <div>
            <div>
                <table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
                    <tr>
                        <td>
                            <table id="dialog_dock" width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
                                <tr>
                                    <td width="100%" height="31" class="main_table_title">
                                        <a runat="server" id="headerTitle" href="#" class="header_title" style="position: absolute; top: 22px"></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="100%" class="main_table_body">
                                        <div style="margin: 10px 10px 10px 10px;">
                                            <div align="right"></div>
                                            <br />
                                            <span class="work_header"><%=title %></span>
                                            <br />
                                            <br />
                                            <div>
                                                <%=resources.LNG_STEP%>
                                                <span id="currentAlertCreationStepNumberSpan" runat="server"></span>
                                                <%=resources.LNG_OF%>
                                                <span id="totalAlertCreationStepNumberSpan" runat="server"></span>: 
                                                <strong><%=resources.LNG_SEND_ALERT%></strong>
                                                <br />
                                            </div>
                                            <input runat="server" type="hidden" name="is_scheduled" value="" />
                                            <input runat="server" type="hidden" name="is_sended" value="true" />
                                            <input runat="server" type="hidden" id="returnPage" name="returnPage" value="" />
                                            <input runat="server" type="hidden" id="instant" name="instant" value="" />
                                            <input runat="server" type="hidden" id="isModerated" name="isModerated" value="" />
                                            <input runat="server" type="hidden" id="campaignId" name="campaign_id" value="" />
                                            <input runat="server" type="hidden" name="change" value="" />
                                            <input runat="server" type="hidden" name="edit" id="edit" value="" />
                                            <input runat="server" type="hidden" id="shouldApprove" name="shouldApprove" value="" />
                                            <input runat="server" type="hidden" id="rejectedEdit" name="rejectedEdit" value="" />
                                            <input runat="server" type="hidden" name="devicetype" value="" />
                                            <input runat="server" type="hidden" id="emailInput" name="email" value="" />
                                            <input runat="server" type="hidden" id="smsInput" name="sms" value="" />
                                            <input runat="server" type="hidden" id="textToCall" name="textToCall" value="" />
                                            <input runat="server" type="hidden" name="survey" id="survey" value="0" />
                                            <input runat="server" type="hidden" id="alertType" name="alertType" value="" />
                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                <tr>
                                                    <td>
                                                        <table id="selection_table" cellpadding="4" cellspacing="4" border="0">
                                                            <tr>
                                                                <td><%=resources.LNG_ALERT_TYPE%>:</td>
                                                                <td>
                                                                    <select runat="server" name="objectType" id="objectType" onchange="changeObjectType(true)"></select>
                                                                </td>
                                                                <td>
                                                                    <a style="display: none;" href="#" id="simpleGroupSelectionButton"><%=resources.SIMPLE_GROUP_SENDING %></a>
                                                                </td>
                                                            </tr>

                                                        </table>
                                                    </td>
                                                    <td align="right">
                                                        <!-- button cell -->
                                                        <asp:LinkButton ID="sendButton" class="send_button" type="submit" value="" OnClientClick="return BeforeSendingAction();" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div id="broadcast_note" style="display: none">
                                                            <p style="color: #888888;"><%=resources.LNG_BROADCAST_HINT_1%></p>
                                                            <ol>
                                                                <li style="color: #888888;"><%=resources.LNG_BROADCAST_HINT_2%></li>
                                                                <li style="color: #888888;"><%=resources.LNG_BROADCAST_HINT_3%></li>
                                                            </ol>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                            <div id="ou_ui" style="display: none">
                                                <!-- objects selection -->
                                                <table cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td>
                                                            <div class="ui-widget">
                                                                <div class="ui-widget-content ui-state-error ui-corner-all" style="padding: 0.7em" id="obj_cnt_border">
                                                                    <%=resources.LNG_YOU_HAVE_SELECTED%> <a href="javascript: showSelectedRecipients();"><span id="obj_cnt">0</span> <%=resources.LNG_OBJECTS%></a>&nbsp;(<a href="javascript:removeAllRecipients()"><%=Resources.resources.LNG_CLEAR%></a>)
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <br />
                                                <input type="hidden" name="f_users" id="f_users" value="0" />
                                                <input type="hidden" name="f_groups" id="f_groups" value="1" />
                                                <input type="hidden" name="f_users" id="f_users" value="1" />
                                                <input type="hidden" name="f_groups" id="f_groups" value="0" />
                                                <input type="hidden" name="f_computers" id="f_computers" value="0" />

                                                <table width="100%" height="515" cellspacing="0" cellpadding="0">
                                                    <tr valign="top">
                                                        <td width="200" id="tree_td">
                                                            <asp:TreeView onclick="TreeNodeChecked();" runat="server" ID="orgTree" RootNodeStyle-CssClass="root_node" ParentNodeStyle-CssClass="tree_class" LeafNodeStyle-CssClass="tree_class" SelectedNodeStyle-BackColor="#010161" SelectedNodeStyle-ForeColor="White" SelectedNodeStyle-VerticalPadding="0"></asp:TreeView>
                                                        </td>
                                                        <td style="background-color: #ffffff; position: relative; z-index: 1;" align="center">
                                                            <iframe src="" width="99%" height="965" style="border: 0 solid #ffffff;" frameborder="0" id="content_iframe" name="content_iframe"></iframe>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>

                                            <input runat="server" type="hidden" name="send_type" id="sendType" value="0" />
                                            <input type='hidden' id="alertId" runat="server" name='alert_id' value='' />
                                            <input type='hidden' name='current_alert_id' value='' />
                                            <input type="hidden" id="ous_id_sequence" name="ous_id_sequence" value="" />
                                            <input type="hidden" id="ous_id_sequence_closed" name="ous_id_sequence_closed" value="" />
                                            <div align="right" id="upper_send" style="display: none">
                                                <asp:LinkButton ID="sendButtonBottom" class="send_button" type="submit" value="" runat="server" OnClientClick="return BeforeSendingAction();" />
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div id="added_recipients"></div>
    </form>
    <table id="hideAll" style="display: none; filter: alpha(opacity=60); -moz-opacity: 0.6; opacity: 0.6; position: absolute; left: 6px; top: 6px; right: 6px; bottom: 6px; z-index: 1; background-color: #999999; width: 100%; height: 100%;" border="0">
        <tr>
            <td>&nbsp;</td>
        </tr>
    </table>
    <div id="showRecipientsDiv" style="position: absolute; top: 30%; left: 30%; z-index: 1000; display: none; border: 1px solid black; width: 400px; height: 351px; background-color: #aaaaaa;">
        <table width="100%" border="0" bordercolor="#000000" cellspacing="0" cellpadding="0" bgcolor="#ffffff" class="body">
            <tr>
                <td width="100%" height="31" class="theme-colored-back"><a href="#" class="header_title"><%=Resources.resources.LNG_LIST_OF_RECIPIENTS%></a> </td>
                <td class="theme-colored-back" align="center"><a href="javascript: hideSelectedRecipients();" class="header_title">X</a>&nbsp;&nbsp;&nbsp;</td>
            </tr>
            <tr>
                <td bgcolor="#ffffff" colspan="2">
                    <div style="margin: 10px;">
                        <div id="showRecipientsDivContent" style="text-align: left; height: 300px; overflow-y: scroll;"></div>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <div id="createTemplateDialog" style="display: none">
        <br />
        <form action="CreateCustomGroup.aspx" action="POST" id="templateForm">
            <label id="groupNameLabel" for="linkValue"><%=Resources.resources.LNG_TEMPLATE_NAME%> : </label>
            <input id="groupName" type="text" name="groupName" style="width: 200px" />
            <br />
            <br />
            <input type="hidden" name="users" id="users" />
            <input type="hidden" name="computers" id="computers" />
            <input type="hidden" name="groups" id="groups" />
            <input type="hidden" name="ous" id="ous" />
            <a href='#' class="send_button" style="float: right;" onclick="javascript:addGroup();">
                <%=resources.LNG_CREATE%>
            </a>
        </form>
    </div>
    <div runat="server" id="simpleGroupSelection" style="z-index: 1; display: none;" title='<%$Resources:resources, SIMPLE_GROUP_SENDING %>'>
        <iframe width="100%" height="100%" id="simpleGroupSendingFrame" src="Recipients.aspx?id=1&type=DOMAIN&filterGroups=on&sms=<%=Sms%>&email=<%=Email%>textToCall=<%=TextToCallAlert%>&desktop=<%=Desktop%>&device=<%=Device%>&alertType=<%=alertType.Value%>&fg=1&fu=0&fc=0&simpleGroupsSelection=1&limit=1000&offset=0" />
    </div>
</body>
</html>
