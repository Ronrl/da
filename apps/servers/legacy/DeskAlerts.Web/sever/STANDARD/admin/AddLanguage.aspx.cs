﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Parameters;


namespace DeskAlertsDotNet.Pages
{
    public partial class AddLanguage : DeskAlertsBasePage
    {
        //use curUser to access to current user of DA control panel      
        //use dbMgr to access to database

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            //Add your main code here

            string langName = Request["langName"];
            string langCode = Request["langСode"];

            //check language is exists
            DataSet checkDataSet = dbMgr.GetDataByQuery("SELECT * FROM languages_list WHERE code = '" + langCode + "' AND name = '" + langName + "'");

            bool update = !checkDataSet.IsEmpty;

            if (!update)
            {
                   //create new language if does not exists
                dbMgr.ExecuteQuery(string.Format("INSERT INTO languages_list VALUES ('{0}','{1}')", langName, langCode));
                dbMgr.ExecuteQuery(string.Format("ALTER TABLE [languages] ADD [{0}] [nvarchar] (max)", langCode));
            }
            else
            {
                int langId = checkDataSet.GetInt(0, "id");
             //   string oldName = checkDataSet.GetString(0, "name");
                string oldCode = checkDataSet.GetString(0, "code");

                dbMgr.ExecuteQuery(string.Format("UPDATE languages_list SET name = '{0}', code='{1}' WHERE id = {2}", langName, langCode, langId));
                dbMgr.ExecuteQuery(string.Format("ALTER TABLE [languages] DROP COLUMN  [{0}]", oldCode));
                dbMgr.ExecuteQuery(string.Format("ALTER TABLE [languages] ADD [{0}] [nvarchar] (max)", langCode));
            }

            string[] langKeys = Request.QueryString.AllKeys.Where(key => key.StartsWith("LNG_")).ToArray();

            foreach (string langKey in langKeys)
            {
                string value = Request[langKey];

                if (!string.IsNullOrEmpty(value))
                {
                    dbMgr.ExecuteQuery(string.Format("UPDATE languages SET {0} = '{1}' WHERE name = '{2}'", langCode,
                        value, langKey));
                }
            }


        }
    }
}