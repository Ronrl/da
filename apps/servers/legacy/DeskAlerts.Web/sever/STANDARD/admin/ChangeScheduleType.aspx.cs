﻿using Castle.MicroKernel.Lifestyle;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.ApplicationCore.Utilities;
using DeskAlerts.Scheduler.ContentJobScheduling;
using DeskAlerts.Server.Utils.Interfaces.Services;
using DeskAlerts.Server.Utils.Interfactes;
using DeskAlerts.Server.Utils.Managers;
using DeskAlertsDotNet.Application;
using DeskAlertsDotNet.Utils.Consts;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace DeskAlertsDotNet.Pages
{
    public partial class ChangeScheduleType : DeskAlertsBasePage
    {
        private readonly IContentService _contentService;
        private readonly ICacheInvalidationService _cacheInvalidationService;
        private readonly IAlertRepository _alertRepository;
        private readonly IContentJobScheduler _contentJobScheduler;

        public ChangeScheduleType()
        {
            using (Global.Container.BeginScope())
            {
                _contentService = Global.Container.Resolve<IContentService>();
                _cacheInvalidationService = Global.Container.Resolve<ICacheInvalidationService>();

                _contentJobScheduler = Global.Container.Resolve<IContentJobScheduler>();
                _alertRepository = Global.Container.Resolve<IAlertRepository>();
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            CacheManager cacheManager = Global.CacheManager;

            if (string.IsNullOrEmpty(Request["type"]))
            {
                Response.End();
                return;
            }

            try
            {
                if (!string.IsNullOrEmpty(Request["campaign"]))
                {
                    string campaignId = Request["campaign"];

                    string type = Request.GetParam("type", "1");
                    dbMgr.ExecuteQuery($"UPDATE alerts SET schedule_type = '{type}' WHERE campaign_id = {campaignId}");
                    Response.Redirect("Campaign.aspx");
                    return;
                }

                if (!string.IsNullOrEmpty(Request["survey"]))
                {
                    char closed = Request["type"] == "0" ? 'Y' : 'N';
                    dbMgr.ExecuteQuery(
                        "UPDATE surveys_main SET closed='" + closed + "' WHERE id=" + Request["surveyId"]);
                }

                try
                {
                    var id = Convert.ToInt32(Request["id"]);
                    var type = Request["type"];
                    var isTerminated = Convert.ToString(Math.Abs(Convert.ToInt32(type) - 1));

                    var parametersList = new List<KeyValuePair<string, string>>();
                    parametersList.Add(new KeyValuePair<string, string>("ScheduleType", type));
                    parametersList.Add(new KeyValuePair<string, string>("Terminated", isTerminated));
                    _contentService.UpdateAlertEntity(id, parametersList);

                    var entity = _contentService.GetContentById(id);
                    var dateTimeNow = DateTime.Now.ToString(DateTimeFormat.MsSqlFormat, CultureInfo.InvariantCulture);

                    //TODO: refactor this shit block
                    //TODO: shit, shit!
                    var isStopped = type == "0";
                    var isScheduled = entity.Schedule == 1;
                    if (entity.Type == AlertType.SimpleAlert ||
                        entity.Type == AlertType.Ticker ||
                        entity.Type == AlertType.Rsvp ||
                        entity.Type == AlertType.SimpleSurvey ||
                        entity.Type == AlertType.SurveyPoll ||
                        entity.Type == AlertType.SurveyQuiz ||
                        entity.Type == AlertType.Wallpaper ||
                        entity.Type == AlertType.LockScreen ||
                        entity.Type == AlertType.ScreenSaver ||
                        entity.Type == AlertType.VideoAlert ||
                        entity.Type == AlertType.Terminated)
                    {
                        if (isStopped)
                        {

                            

                            if (AppConfiguration.IsNewContentDeliveringScheme)
                            {
                                if (isScheduled)
                                {
                                    var contentJobDto = new ContentJobDto(id, entity.Type);
                                    _contentJobScheduler.DeleteJob(contentJobDto);
                                }

                                _cacheInvalidationService.CacheInvalidation(entity.Type, id);
                                _cacheInvalidationService.CacheInvalidation(AlertType.Terminated, id);
                            }
                            else 
                            {
                                cacheManager.Delete(id, dateTimeNow);
                            }
                        }
                        else
                        {
                            if (AppConfiguration.IsNewContentDeliveringScheme)
                            {
                                if (isScheduled && entity.FromDate.HasValue)
                                {
                                    var contentJobDto = new ContentJobDto(entity.FromDate.Value, id, entity.Type);
                                    _contentJobScheduler.CreateJob(contentJobDto);
                                }
                                else
                                {
                                    _cacheInvalidationService.CacheInvalidation(entity.Type, id);
                                    _cacheInvalidationService.CacheInvalidation(AlertType.Terminated, id);
                                }
                            }
                            else
                            { 
                                cacheManager.Add(id, dateTimeNow);
                            }
                        }
                    }
                    else
                    {
                        if (AppConfiguration.IsNewContentDeliveringScheme)
                        {
                            _cacheInvalidationService.CacheInvalidation(entity.Type, id);
                        }
                        else
                        {
                            cacheManager.Add(id, dateTimeNow);
                        }
                    }
                }
                catch (FormatException ex)
                {
                    Logger.Debug(ex);
                    throw new FormatException("Alert id must be a number");
                }

                if (!string.IsNullOrEmpty(Request["return_page"]))
                    Response.Redirect(Request["return_page"], true);

                var responseResult = $"PopupSent.aspx{Request.Url.Query}&{PageParams}";
                Response.Redirect(responseResult);
            }
            catch (Exception ex)
            {
                Logger.Debug(ex);
            }
        }

        protected string PageParams
        {
            get
            {
                var result = string.IsNullOrEmpty(Request["user_id"]) ? string.Empty : "?user_id=" + Request["user_id"];
                //Check this crutch
                if (string.IsNullOrEmpty(Request["user_id"]) && !string.IsNullOrEmpty(Request["templateId"]))
                {
                    result = "?user_id=" + Request["templateId"];
                }

                return result;
            }
        }
    }
}