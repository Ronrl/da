﻿using Castle.MicroKernel.Lifestyle;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Enums;
using DeskAlerts.ApplicationCore.Extensions.Enums;
using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.ApplicationCore.Utilities;
using DeskAlerts.Server.Utils;
using DeskAlerts.Server.Utils.Interfaces.Services;
using DeskAlerts.Server.Utils.Interfactes;
using DeskAlerts.Server.Utils.Managers;
using DeskAlerts.Server.Utils.Services;
using DeskAlertsDotNet.Application;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Extentions;
using DeskAlertsDotNet.Models.Permission;
using DeskAlertsDotNet.Parameters;
using DeskAlertsDotNet.Utils.Factories;
using Microsoft.VisualBasic.FileIO;
using Newtonsoft.Json;
using NLog;
using Resources;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataRow = DeskAlertsDotNet.DataBase.DataRow;
using DataSet = DeskAlertsDotNet.DataBase.DataSet;

namespace DeskAlertsDotNet.Pages
{
    public partial class CreateMultipleAlert : DeskAlertsBasePage
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly ICacheInvalidationService _cacheInvalidationService;
        private readonly IAlertRepository _alertRepository;
        private IContentReceivedService _contentReceivedService;

        protected string tinyMceSettings;
        protected string skinsJson;
        protected string digitalSignage = string.Empty;
        protected string IsInstantVal = string.Empty;
        protected int shouldCheckSize = 0;
        protected int shouldBeReadOnly = 0;
        protected int tickerValue = 0;
        CacheManager cacheManager = Global.CacheManager;
        public string UploadPath = @"~/admin/images/upload/";
        public bool SimpleSending;
        private Int32 MinAlertHeight = 290;
        private Int32 MinAlertWidth = 340;

        private List<GroupAlertContent> _tempAlertList;
        private Guid _tempGuid;
        
        public CreateMultipleAlert()
        {
            using (Global.Container.BeginScope())
            {
                _cacheInvalidationService = Global.Container.Resolve<ICacheInvalidationService>();
                _alertRepository = Global.Container.Resolve<IAlertRepository>();
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            _contentReceivedService = new ContentReceivedService();

            SimpleSending = false;

            saveNextButton.Click += saveNextButton_Click;

            bottomSaveNextButton.Click += saveNextButton_Click;

            headerTitle.InnerText = resources.LNG_MULTIPLE;

            DataSet skinSet = dbMgr.GetDataByQuery("SELECT id, name FROM alert_captions");

            JsonSerializerSettings settings = new JsonSerializerSettings
            {
                Converters = new JsonConverter[] { new DeskAlertsDataBaseJsonWorker() }
            };

            if (!IsPostBack)
            {
                size1.Checked = Settings.Content["ConfPopupType"] == "W";
                size2.Checked = Settings.Content["ConfPopupType"] == "F";

                positionBottomLeft.Checked = Settings.Content["ConfAlertPosition"] == ((int)AlertPosition.LeftBottom).ToString();
                positionBottomRight.Checked = Settings.Content["ConfAlertPosition"] == ((int)AlertPosition.RightBottom).ToString();
                positionCenter.Checked = Settings.Content["ConfAlertPosition"] == ((int)AlertPosition.Center).ToString();
                positionTopLeft.Checked = Settings.Content["ConfAlertPosition"] == ((int)AlertPosition.LeftTop).ToString();
                positionTopRight.Checked = Settings.Content["ConfAlertPosition"] == ((int)AlertPosition.RightTop).ToString();

                alertWidth.Value = Settings.Content["ConfAlertWidth"];
                alertHeight.Value = Settings.Content["ConfAlertHeight"];
                lifetime.Value = Settings.Content["ConfMessageExpire"];
                positionTop.Checked = Settings.Content["ConfTickerPosition"] == ((int)AlertPosition.Top).ToString();
                positionMiddle.Checked = Settings.Content["ConfTickerPosition"] == ((int)AlertPosition.Middle).ToString();
                positionBottom.Checked = Settings.Content["ConfTickerPosition"] == ((int)AlertPosition.Bottom).ToString();
                positionDocked.Checked = Settings.Content["ConfTickerPosition"] == "D";

                urgentCheckBox.Checked = Settings.Content["ConfUrgentByDefault"] == "1";
            }

            skinsJson = JsonConvert.SerializeObject(skinSet, settings);

            if (!string.IsNullOrEmpty(Request["webplugin"]))
            {
                digitalSignage = Request["webplugin"];
            }

            digsignInfo.Visible = digitalSignage.Equals("1");

            resizable.Visible = digitalSignage != "1";
            resizableLabel.Visible = digitalSignage != "1";

            if (!string.IsNullOrEmpty(Request["instant"]))
            {
                IsInstantVal = Request["instant"];
            }

            IsInstant.Value = IsInstantVal;
            changeColorCode.Visible = IsInstantVal == "1";

            if (Settings.IsTrial)
            {
                resizable.Visible = false;
                resizableLabel.Attributes["title"] = resources.TRIAL_MODE_DISABLED_FUNCTIONALITY;
                resizableLabel.Attributes["class"] = ContentCustomClass.AdditionalDescription.GetStringValue();
            }

            LifetimeAdditionalDescription.InnerText = resources.LNG_TIME_EXPIRED_CANT_RECEIVED;
            LifeTimeHelpImage.Attributes["title"] = resources.LNG_LIFETIME_EXT_DESC;

            int colorCodesCount = dbMgr.GetScalarQuery<int>("SELECT COUNT(1) FROM color_codes");

            if (colorCodesCount == 0)
            {
                changeColorCode.Visible = false;
            }

            if (!string.IsNullOrEmpty(Request["rejectedEdit"]))
            {
                rejectedEdit.Value = Request["rejectedEdit"];
            }

            if (!string.IsNullOrEmpty(Request["ticker"]))
            {
                ticker.Value = Request["ticker"];
            }

            if (!string.IsNullOrEmpty(Request["shouldApprove"]))
            {
                shouldApprove.Value = Request["shouldApprove"];
            }

            if (!string.IsNullOrEmpty(Request["id"]))
            {
                alertId.Value = Request["id"];
            }

            bool hasAnyTickerModule = Config.Data.HasModule(Modules.TICKER) || Config.Data.HasModule(Modules.TICKER_PRO);
            if (!string.IsNullOrEmpty(Request["ticker"]) && hasAnyTickerModule)
            {
                tickerValue = 1;
            }

            if (!string.IsNullOrEmpty(Request["webplugin"]))
            {
                webAlert.Value = "1";
            }

            if (!Config.Data.HasModule(Modules.CAMPAIGN) || string.IsNullOrEmpty(Request["alert_campaign"]) ||
                Request["alert_campaign"].Equals("-1"))
            {
                campaignLabel.Visible = false;
                campaignSelect.Value = "-1";
            }
            else
            {
                int campaignId = Convert.ToInt32(Request["alert_campaign"]);

                string campName =
                    dbMgr.GetScalarByQuery("SELECT name FROM campaigns WHERE id = " + campaignId).ToString();
                campaignName.InnerText = campName;
                campaignSelect.Value = campaignId.ToString();
            }

            saveNextButton.Text = resources.LNG_SEND;
            bottomSaveNextButton.Text = resources.LNG_SEND;

            if (digitalSignage.Equals("1"))
            {
                alertDeviceHeader.Visible = false;
                alertDeviceTable.Visible = false;
                alertSettingsHeader.Visible = false;
                alertSettingsTable.Visible = false;
                alertDeviceTable.Visible = false;
                positionCell.Visible = false;

                int linksCount = dbMgr.GetScalarQuery<int>("SELECT COUNT(1) FROM digsign_links");

                if (linksCount == 0)
                {
                    saveNextButton.Visible = false;
                    bottomSaveNextButton.Visible = false;
                    digSignNoLinksWarning.Style.Add(HtmlTextWriterStyle.Display, "block");
                    digsignInfo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    if (!curUser.HasPrivilege)
                    {
                        digSignNoLinksWarning.InnerText = resources.LNG_NO_LINKS;
                    }
                }

                desktopCheck.Style.Add(HtmlTextWriterStyle.Display, "none");
            }

            if (!IsPostBack)
            {
                lifetimeFactor.Items.Add(new ListItem(resources.LNG_MINUTES, "1"));
                lifetimeFactor.Items.Add(new ListItem(resources.LNG_HOURS, "60"));
                lifetimeFactor.Items.Add(new ListItem(resources.LNG_DAYS, "1440"));

                lifetimeFactor.Items[0].Selected = true;
            }

            if (curUser.HasPrivilege || curUser.Policy[Policies.ALERTS].CanResize)
            {
                shouldCheckSize = 1;
            }

            if (Settings.Content["ConfEnableHtmlAlertTitle"].Equals("1"))
            {
                simpleTitleDiv.Visible = false;
                htmlTitleDiv.Visible = true;
            }
            else
            {
                simpleTitleDiv.Visible = true;
                htmlTitleDiv.Visible = false;
            }

            if (!string.IsNullOrEmpty(Request["id"]))
            {
                var alertId = Convert.ToInt32(Request["id"]);
                LoadAlert(alertId);
            }
            else if (!string.IsNullOrEmpty(Request["tid"]))
            {
                int templateId = Convert.ToInt32(Request["tid"]);
                LoadTemplate(templateId);
            }
            else
            {
                question.Value = resources.LNG_WILL_YOU_BE_ATTENDING;
                question_option1.Value = resources.LNG_YES;
                question_option2.Value = resources.LNG_NO;
                second_question.Value = resources.LNG_PLEASE_DESCRIBE_YOUR_REASON;
            }
        }

        public void ShowPromt(HttpResponse r, string text)
        {
            ScriptManager.RegisterStartupScript(updatePanel, updatePanel.GetType(), "SHOW_PROMT", "showPrompt('" + text + "')", true);
        }

        public void ShowLoader(HttpResponse r)
        {
            ScriptManager.RegisterStartupScript(updatePanel, updatePanel.GetType(), "SHOW_PROMT", "showLoader()", true);
        }

        bool ValidateData()
        {
            string title = Settings.Content["ConfEnableHtmlAlertTitle"].Equals("1")
                ? titleHidden.Value
                : simpleTitle.Value;

            title = DeskAlertsUtils.HTMLToText(title);

            if (lifeTimeCheckBox.Checked)
            {
                if (lifetime.Value.Length < 1)
                {
                    ShowPromt(Response, resources.LNG_YOU_SHOULD_ENTER_LIFETIME);
                    return false;
                }
            }

            if (DeskAlertsUtils.HTMLToText(title).Length > 255)
            {
                ShowPromt(Response, resources.LNG_TITLE_SHOULD_BE_LESS_THEN);
                return false;
            }

            if (DeskAlertsUtils.HTMLToText(title).Length == 0)
            {
                ShowPromt(Response, resources.LNG_YOU_SHOULD_ENTER_ALERT_TITLE);
                return false;
            }

            if (!desktopCheck.Checked)
            {
                ShowPromt(Response, resources.LNG_YOU_SHOULD_SELECT_ALERT_TYPE);
                return false;
            }
            if ((Convert.ToInt32(alertWidth.Value) < MinAlertWidth) && (Convert.ToInt32(alertHeight.Value) < MinAlertHeight))
            {
                ShowPromt(Response, resources.LNG_RESOLUTION_DESC2);
                return false;
            }
            return true;
        }

        void saveNextButton_Click(object sender, EventArgs e)
        {
            if (!ValidateData())
            {
                return;
            }

            string confirmValue = Request.Form["confirm_value"];
            if (confirmValue == "Yes")
            {
                StartParse();
            }
        }

        void StartParse()
        {
            if (simple.Checked)
            {
                SimpleSending = true;
            }
            else if (group.Checked)
            {
                SimpleSending = false;
            }

            if (Settings.Content["ConfEnableHtmlAlertTitle"].Equals("1"))
            {
                titleHidden.Value = htmlTitle.InnerText;
            }

            if (FileUploader.HasFile)
            {
                try
                {
                    Label1.Text = string.Empty;
                    FileUploader.PostedFile.SaveAs(Server.MapPath(UploadPath) + FileUploader.FileName);
                    ParseCsv(FileUploader.PostedFile.InputStream);
                    Response.Redirect("PopupSent.aspx");
                }
                catch (Exception ex)
                {
                    Label1.Text = "ERROR: " + ex.Message;
                }
            }
            else
            {
                ShowPromt(Response, resources.LNG_YOU_SHOULD_SELECT_CSV_FILE);
                Label1.Text = resources.LNG_YOU_SHOULD_SELECT_CSV_FILE;
            }
        }

        void ChangeTab(string tabName)
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "CheckTab", "check_tab('" + tabName + "')", true);
        }

        [WebMethod]
        public static string GetTemplate(int templateId)
        {
            using (DBManager dbManager = new DBManager())
            {
                var templateTextObj = dbManager.GetScalarQuery<string>("SELECT template_text FROM text_templates WHERE id = " + templateId);
                return templateTextObj;
            }
        }

        void LoadTemplate(int templateId)
        {
            DataRow templateRow = dbMgr.GetDataByQuery("SELECT name, template_text FROM text_templates WHERE id = " + templateId).First();

            string name = templateRow.GetString("name");

            if (Settings.Content["ConfEnableHtmlAlertTitle"].Equals("1"))
            {
                htmlTitle.InnerHtml = name;
                titleHidden.Value = name;
            }
            else
            {
                simpleTitle.Value = name;
            }
        }

        void LoadAlert(int alertId)
        {
            if (IsPostBack)
            {
                return;
            }

            var alertSet = dbMgr.GetDataByQuery("SELECT * FROM alerts WHERE id = " + alertId);
            var alertRow = alertSet.GetRow(0);
            
            var title = alertRow.GetString("title");
            var alertContent = alertRow.GetString("alert_text");

            if (Settings.Content["ConfEnableHtmlAlertTitle"].Equals("1"))
            {
                var htmlDecodeTitle = HttpUtility.HtmlDecode(title);

                MatchCollection matches = Regex.Matches(alertContent, "<!-- html_title *= *(['\"])(.*?)\\1 *-->");

                if (matches.Count > 0)
                {
                    var lastRegex = matches.Count - 1;
                    if (matches[lastRegex].Groups.Count > 0)
                    {
                        htmlDecodeTitle = matches[lastRegex].Groups[2].Value;
                    }
                }

                this.htmlTitle.InnerHtml = htmlDecodeTitle;
                titleHidden.Value = htmlDecodeTitle;
            }
            else
            {
                simpleTitle.Value = title;
            }

            if (alertRow.GetInt("urgent") == 1)
            {
                urgentCheckBox.Checked = true;
            }

            var ticker = alertRow.GetInt("ticker");
            var lifeTime = Convert.ToInt32(Settings.Content["ConfMessageExpire"]);
            lifeTimeCheckBox.Checked = false;
            if (alertRow.GetInt("lifetime") == 1)
            {
                lifeTimeCheckBox.Checked = true;

                foreach (ListItem item in lifetimeFactor.Items)
                {
                    item.Selected = false;
                }
                for (var i = 0; i <= 0; i++)
                {
                    var listItem = lifetimeFactor.Items[i];
                    var itemValue = Convert.ToInt32(listItem.Value);

                    var lifeTimeValue = lifeTime / itemValue;
                    if (lifeTimeValue >= 0)
                    {
                        listItem.Selected = true;
                        lifeTimeCheckBox.Checked = true;
                        lifetime.Value = lifeTimeValue.ToString();
                        break;
                    }
                }
            }

            if (alertRow.GetInt("desktop") == 1)
            {
                desktopCheck.Checked = true;
            }

            var skinId = "default";

            if (!alertRow.IsNull("caption_id"))
            {
                skinId = alertRow.GetString("caption_id");
            }

            skin_id.Value = skinId;

            if (alertRow.GetInt("fullscreen") == (int)AlertPosition.FullScreen)
            {
                size2.Checked = true;
            }
            else
            {
                size1.Checked = true;
                if (ticker == 0)
                {
                    switch (alertRow.GetInt("fullscreen"))
                    {
                        case (int)AlertPosition.LeftTop:
                            {
                                positionTopLeft.Checked = true;
                                break;
                            }

                        case (int)AlertPosition.RightTop:
                            {
                                positionTopRight.Checked = true;
                                break;
                            }

                        case (int)AlertPosition.Center:
                            {
                                positionCenter.Checked = true;
                                break;
                            }

                        case (int)AlertPosition.LeftBottom:
                            {
                                positionBottomLeft.Checked = true;
                                break;
                            }

                        case (int)AlertPosition.RightBottom:
                            {
                                positionBottomRight.Checked = true;
                                break;
                            }
                    }

                    alertWidth.Value = alertRow.GetString("alert_width");
                    alertHeight.Value = alertRow.GetString("alert_height");

                    if (alertRow.GetInt("resizable") == 1)
                    {
                        resizable.Checked = true;
                    }
                }
                else
                {
                    ChangeTab("ticker");

                    switch (alertRow.GetString("fullscreen"))
                    {
                        case "7":
                            {
                                positionTop.Checked = true;
                                break;
                            }

                        case "8":
                            {
                                positionMiddle.Checked = true;
                                break;
                            }

                        case "9":
                            {
                                positionBottom.Checked = true;
                                break;
                            }

                        case "D":
                            {
                                positionDocked.Checked = true;
                                break;
                            }
                    }
                }
            }

            if (Config.Data.HasModule(Modules.MOBILE))
            {
                var deviceType = alertRow.GetInt("device");

                desktopDevice.Attributes.Remove("checked");
                mobileDevice.Attributes.Remove("checked");
                allDevices.Attributes.Remove("checked");
                switch (deviceType)
                {
                    case (int)TargetDeviceType.Desktop:
                        {
                            desktopDevice.Checked = true;
                            desktopDevice.Attributes.Add("checked", "true");
                            break;
                        }

                    case (int)TargetDeviceType.Mobile:
                        {
                            mobileDevice.Attributes.Add("checked", "true");
                            break;
                        }

                    default:
                        {
                            allDevices.Attributes.Add("checked", "true");
                            break;
                        }
                }
            }

            DataSet surveySet = dbMgr.GetDataByQuery("SELECT id from surveys_main WHERE sender_id = " + alertId);

            if (!surveySet.IsEmpty)
            {
                int surveyId = surveySet.GetInt(0, "id");

                DataSet firstQuestionSet = dbMgr.GetDataByQuery("SELECT question, id  FROM surveys_questions WHERE question_number = 1 AND survey_id = " + surveyId);

                question.Value = firstQuestionSet.GetString(0, "question");

                DataSet firstQuestionOptions = dbMgr.GetDataByQuery("SELECT answer FROM surveys_answers WHERE question_id = " + firstQuestionSet.GetString(0, "id"));

                question_option1.Value = firstQuestionOptions.GetString(0, "answer");
                question_option2.Value = firstQuestionOptions.GetString(1, "answer");

                DataSet secondQuestionSet =
                    dbMgr.GetDataByQuery(
                        "SELECT question, id  FROM surveys_questions WHERE question_number = 2 AND survey_id = " +
                        surveyId);

                if (!secondQuestionSet.IsEmpty)
                {
                    need_second_question.Checked = true;
                    second_question.Value = secondQuestionSet.GetString(0, "question");
                }
            }
            else
            {
                question.Value = resources.LNG_WILL_YOU_BE_ATTENDING;
                question_option1.Value = resources.LNG_YES;
                question_option2.Value = resources.LNG_NO;
                second_question.Value = resources.LNG_PLEASE_DESCRIBE_YOUR_REASON;
            }


            if (!Config.Data.HasModule(Modules.CAMPAIGN) || alertRow.GetInt("campaign_id") == -1)
            {
                campaignLabel.Visible = false;
                campaignSelect.Value = "-1";
            }
            else
            {
                int campaignId = alertRow.GetInt("campaign_id");

                string campName = dbMgr.GetScalarByQuery("SELECT name FROM campaigns WHERE id = " + campaignId).ToString();
                campaignName.InnerText = campName;
                campaignSelect.Value = campaignId.ToString();
            }

            object colorValueObj = dbMgr.GetScalarByQuery("SELECT [color] FROM color_codes WHERE id = '" + alertRow.GetString("color_code") + "'");

            string color = "747474";
            if (colorValueObj != null)
            {
                color = colorValueObj.ToString();
            }

            changeColorCode.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#" + color);

            colorСode.Value = alertRow.GetString("color_code");
        }

        private void ParseCsv(Stream fileStream)
        {
            using (StreamReader inputStreamReader = new StreamReader(fileStream))
            {
                _tempGuid = Guid.NewGuid();
                var usersCollection = new List<GroupAlert>();
                while (!inputStreamReader.EndOfStream)
                {
                    var inputContent = inputStreamReader.ReadLine();
                    if (inputContent == null)
                    {
                        continue;
                    }

                    const string query = @"
                        SELECT id
                        FROM users
                        WHERE (client_version IS NOT NULL
                               AND client_version <> '')
                               AND name LIKE @userName;";

                    var parser = new TextFieldParser(new StringReader(inputContent))
                    {
                        HasFieldsEnclosedInQuotes = true
                    };
                    parser.SetDelimiters(";", ",");
                    if (SimpleSending)
                    {
                        while (!parser.EndOfData)
                        {
                            var fields = parser.ReadFields();
                            var userName = fields[0].Replace("'", "''");
                            var date = fields[1].Replace("'", "''");
                            var id = fields[2].Replace("'", "''");
                            var content = fields[3].Replace("'", "''");
                            if (fields.Length > 4)
                            {
                                content = string.Empty;
                                for (int i = 3; i < fields.Length; i++)
                                {
                                    if (i > 3)
                                    {
                                        content += ", ";
                                    }

                                    content += fields[i].Replace("'", "''");
                                }
                            }

                            if (string.IsNullOrEmpty(userName))
                            {
                                continue;
                            }

                            // Crash is possible if 'users' table contains domain and nondomain users with some names 
                            var userNameParameter = SqlParameterFactory.Create(DbType.String, userName, "userName");
                            try
                            {
                                var userId = dbMgr.GetScalarQuery<int>(query, userNameParameter);
                                AddAlert(userId, date, id, content, false);
                            }
                            catch (Exception ex)
                            {
                                Logger.Debug(ex, $"Adding multiple alert for user \"{userName}\" failed. Exception: \"{ex.Message}\".");
                            }
                        }

                        parser.Close();
                    }
                    else
                    {
                        while (!parser.EndOfData)
                        {
                            var fields = parser.ReadFields();
                            var userName = fields[0].Replace("'", "''");
                            var date = fields[1].Replace("'", "''");
                            var id = fields[2].Replace("'", "''");
                            var content = fields[3].Replace("'", "''");
                            if (fields.Length > 4)
                            {
                                content = string.Empty;
                                for (var i = 3; i < fields.Length; i++)
                                {
                                    if (i > 3)
                                    {
                                        content += ", ";
                                    }

                                    content += fields[i].Replace("'", "''");
                                }
                            }

                            if (string.IsNullOrEmpty(userName))
                            {
                                continue;
                            }

                            var userNameParameter = SqlParameterFactory.Create(DbType.String, userName, "userName");
                            try
                            {
                                var userId = dbMgr.GetScalarQuery<int>(query, userNameParameter);
                                if (userId == -1)
                                {
                                    continue;
                                }
                                var isFound = false;
                                foreach (var user in usersCollection)
                                {
                                    if (user.UserId != userId)
                                    {
                                        continue;
                                    }

                                    user.Alerts.Add(new GroupAlertContent(content, id, date));
                                    isFound = true;
                                }

                                if (isFound)
                                {
                                    continue;
                                }

                                var groupAlert = new GroupAlert { UserId = userId };
                                groupAlert.Alerts.Add(new GroupAlertContent(content, id, date));
                                usersCollection.Add(groupAlert);
                            }
                            catch (Exception ex)
                            {
                                Logger.Debug(ex);
                            }
                        }

                        parser.Close();
                    }
                }
                foreach (var user in usersCollection)
                {
                    _tempAlertList = user.Alerts;
                    AddAlert(user.UserId, string.Empty, string.Empty, string.Empty, true);
                }
            }
        }

        private string GenerateMultiAlert(int alertId, List<GroupAlertContent> alerts)
        {
            var result =
                @"<!DOCTYPE html><html><head></head><body><div><table border=""1"" style=""width: 100%;""><tr><td align=""center""><b>ID</b></td><td align=""center""><b>DATE</b></td><td align=""center""><b>CONTENT</b></td><td align=""center""><b>AGREEMENT</b></td></tr>";
            var id = 1;
            string url = Config.Data.GetString("alertsDir");
            Label2.Text = url;
            string urlBridge = url + "/MultipleUpdate.aspx";
            foreach (var alert in alerts)
            {
                result += @"<tr><td>" + alert.Id + @"</td><td align=""center"">" + alert.Date + "</td><td><label>" + alert.Description +
                          "</label></td>" + @"<td style=""text-align: center;""><form id=""form" + id +
                          @""" method=""post"" action=""" + urlBridge + @"""><input name=""alertId"" value =""" +
                          alertId + @""" type=""hidden""/><input name=""partId"" value =""" + id +
                          @""" type=""hidden""/><input class=""sendData"" type=""submit"" id=""submitButton" + id +
                          @""" value=""Agree""/></form></td></tr>";
                var dict = new Dictionary<string, object>
                               {
                                   { "alert_id", alertId },
                                   { "part_id", id },
                                   { "part_content", alert.Id + " " + alert.Date + " " + alert.Description },
                                   { "confirmed", 0},
                                   { "stat_alert_id", _tempGuid.ToString() }
                               };
                dbMgr.Insert("multiple_alerts", dict);
                id++;
            }

            result += @"</table></div><br><script type=""text/javascript"">" + @"</script></body></html>";
            return result;
        }

        private void AddAlert(int userId, string date, string id, string content, bool isGroup)
        {
            Dictionary<string, object> alertFields = new Dictionary<string, object>();
            string title = Settings.Content["ConfEnableHtmlAlertTitle"].Equals("1")
                ? htmlTitle.Value
                : simpleTitle.Value;
            content = @"<table border=""1"" style=""width: 100 %;""><tr><td align=""center""><b>ID</b></td><td align=""center""><b>DATE</b></td><td align=""center""><b>CONTENT</b></td></tr><tr><td>" + id + @"</td><td align=""center"">" + date + @"</td><td><label>" + content + @"</label></td></tr></table>";
            string htmlTitleValue = Settings.Content["ConfEnableHtmlAlertTitle"].Equals("1") ? htmlTitle.Value : string.Empty;
            htmlTitleValue = HttpUtility.HtmlEncode(htmlTitleValue);
            alertFields.Add("title", HttpUtility.HtmlEncode(title));
            int urgent = 0;
            var alertClass = (int)AlertType.SimpleAlert;
            if (urgentCheckBox.Checked)
            {
                urgent = 1;
            }

            string campaignId = campaignSelect.Value;

            if (!string.IsNullOrEmpty(campaignId))
            {
                alertFields.Add("campaign_id", campaignId);
            }

            alertFields.Add("urgent", urgent);
            int unobtrusive = 0;

            if (webAlert.Value == "1")
            {
                alertClass = (int)AlertType.DigitalSignature;
            }

            alertFields.Add("class", alertClass);


            int selfDeletable = 0;

            if (htmlTitleValue.Length > 0 && htmlTitleValue.IndexOf("<!-- html_title") == -1)
            {
                content = $"{content}<!-- html_title = '{htmlTitleValue}' -->";
            }

            content = content.Replace("<!-- self-deletable -->", string.Empty);

            alertFields.Add("self_deletable", selfDeletable);

            alertFields.Add("ticker", 0);

            alertFields.Add("type", $"{ContentState.Send.GetStringValue()}");

            int recurrence = 0;
            int schedule = 0;
            string toDateStr = "01.01.1900 00:00:00";
            string fromDateStr = "01.01.1900 00:00:00";
            if (lifeTimeCheckBox.Checked)
            {
                string lifeTimeValueStr = lifetime.Value;
                string lifeTimeFactorStr = lifetimeFactor.Value;

                int lifeTimeValue = 0;
                int lifeTimeFactor = 0;

                if (int.TryParse(lifeTimeFactorStr, out lifeTimeFactor) &&
                    int.TryParse(lifeTimeValueStr, out lifeTimeValue))
                {
                    DateTime fromDate = DateTime.Now;
                    DateTime toDate = fromDate.AddMinutes(lifeTimeValue * lifeTimeFactor);

                    fromDateStr = fromDate.ToString("dd/MM/yyyy HH:mm:ss.fff");
                    toDateStr = toDate.ToString("dd/MM/yyyy HH:mm:ss.fff");

                }

                alertFields.Add("from_date", fromDateStr);
                alertFields.Add("to_date", toDateStr);
                alertFields.Add("lifetime", 1);

            }

            alertFields.Add("schedule", schedule);
            alertFields.Add("recurrence", 0);
            alertFields.Add("schedule_type", 1);
            int resizableVal = 0;

            if (resizable.Checked)
            {
                resizableVal = 1;
            }

            alertFields.Add("resizable", resizableVal);

            int aknown = 0;

            if (!isGroup)
            {
                aknown = 1;
            }

            alertFields.Add("aknown", aknown);

            int position = (int)AlertPosition.RightBottom;
            if (positionTopLeft.Checked)
            {
                position = (int)AlertPosition.LeftTop;
            }
            else if (positionTopRight.Checked)
            {
                position = (int)AlertPosition.RightTop;
            }
            else if (positionCenter.Checked)
            {
                position = (int)AlertPosition.Center;
            }
            else if (positionBottomLeft.Checked)
            {
                position = (int)AlertPosition.LeftBottom;
            }
            else if (positionBottomRight.Checked)
            {
                position = (int)AlertPosition.RightBottom;
            }

            if (!size2.Checked)
            {
                var alertWidthVal = Convert.ToInt32(alertWidth.Value);
                var alertHeightVal = Convert.ToInt32(alertHeight.Value);
                alertFields.Add("fullscreen", position);
                alertFields.Add("alert_width", alertWidthVal);
                alertFields.Add("alert_height", alertHeightVal);
            }
            else
            {
                var fullscreen = (int)AlertPosition.FullScreen;
                alertFields.Add("fullscreen", fullscreen);
            }

            string deviceType = ((int)TargetDeviceType.Desktop).ToString();

            if (mobileDevice.Checked)
            {
                deviceType = ((int)TargetDeviceType.Mobile).ToString();
            }

            if (desktopDevice.Checked)
            {
                deviceType = ((int)TargetDeviceType.Desktop).ToString();
            }

            if (allDevices.Checked)
            {
                deviceType = ((int)TargetDeviceType.All).ToString();
            }

            alertFields.Add("device", deviceType);

            int sms = 0;
            if (Config.Data.HasModule(Modules.SMS) && !string.IsNullOrEmpty(Request["sms"]))
            {
                sms = Convert.ToInt32(Request["sms"]);
            }

            alertFields.Add("sms", sms);

            int email = 0;
            if (Config.Data.HasModule(Modules.EMAIL) && !string.IsNullOrEmpty(Request["emailCheck"]))
            {
                email = Convert.ToInt32(Request["emailCheck"]);
            }

            alertFields.Add("email", email);

            int autoClose = 0;
            if (!string.IsNullOrEmpty(Request["autoCloseCheckBox"]))
            {
                if (int.TryParse(Request["autoCloseCheckBox"], out autoClose))
                {
                    autoClose *= 60;

                    if (!string.IsNullOrEmpty(Request["manualClose"]))
                    {
                        autoClose = -autoClose;
                    }
                }
            }

            alertFields.Add("autoclose", autoClose);

            int shouldApprove = 0;
            int rejectedEdit = 0;
            string emailSender = string.Empty;
            if (Config.Data.HasModule(Modules.EMAIL) && !string.IsNullOrEmpty(Request["email_sender"]))
            {
                emailSender = Request["email_sender"];
            }

            alertFields.Add("email_sender", emailSender);

            int desktop = 1;
            if (!string.IsNullOrEmpty(Request["desktopCheck"]))
            {
                desktop = Convert.ToInt32(Request["desktopCheck"]);
            }

            alertFields.Add("desktop", desktop);

            if (!string.IsNullOrEmpty(Request["skin_id"]))
            {
                string skinId = Request["skin_id"];

                if (skinId.Equals("default"))
                {
                    skinId = null;
                }

                alertFields.Add("caption_id", skinId);
            }
            else
            {
                alertFields.Add("caption_id", null);
            }


            if (Config.Data.HasModule(Modules.APPROVE) && Settings.Content["ConfAllowApprove"] == "1")
            {
                if (curUser.HasPrivilege || curUser.Policy[Policies.ALERTS].CanApprove)
                {
                    alertFields.Add("approve_status", 1);
                }
                else
                {
                    alertFields.Add("approve_status", 0);
                }
            }
            else if (!string.IsNullOrEmpty(Request["rejectedEdit"]) && Request["rejectedEdit"].Equals("1"))
            {
                alertFields.Add("approve_status", 0);
            }
            else if (!string.IsNullOrEmpty(Request["shouldApprove"]) && Request["shouldApprove"].Equals("1"))
            {
                alertFields.Add("approve_status", 1);
            }
            else
            {
                alertFields.Add("approve_status", 1);
            }

            string colorCode = Request["colorСode"];

            if (string.IsNullOrEmpty(colorCode))
            {
                colorCode = "00000000-0000-0000-0000-000000000000";
            }

            if (!string.IsNullOrEmpty(Request["color_code"]))
            {
                colorCode = Request["color_code"];
            }

            alertFields.Add("color_code", colorCode);
            int printAlert = 0;

            if (!string.IsNullOrEmpty(Request["printAlertCheckBox"]))
            {
                content += "<!-- printable_alert -->";
            }

            alertFields.Add("social_media", !string.IsNullOrEmpty(Request["socialMedia"]) ? 1 : 0);

            alertFields.Add("post_to_blog", !string.IsNullOrEmpty(Request["blogPost"]) ? 1 : 0);

            alertFields.Add("text_to_call", !string.IsNullOrEmpty(Request["textToCall"]) ? 1 : 0);

            alertFields.Add("video", !string.IsNullOrEmpty(Request["videoLink"]) ? 1 : 0);

            alertFields.Add("alert_text", content);

            alertFields.Add("sender_id", curUser.Id);
            alertFields.Add("toolbarmode", 0);
            alertFields.Add("deskalertsmode", 0);

            alertFields.Add("create_date", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
            alertFields.Add("type2", "R");
            alertFields.Add("group_type", "B");
            alertFields.Add("sent_date", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
            var alertId = AlertManager.Default.AddAlert(dbMgr, alertFields);
            if (isGroup)
            {
                Dictionary<string, object> updateAlertDictionary = new Dictionary<string, object>
                {
                    {"alert_text", GenerateMultiAlert(alertId, _tempAlertList)}
                };
                AlertManager.Default.UpdateAlert(dbMgr, updateAlertDictionary, alertId);
            }
            else
            {
                var dict = new Dictionary<string, object> { { "alert_id", alertId }, { "part_id", -1 }, { "part_content", content }, { "confirmed", -1 }, { "stat_alert_id", _tempGuid.ToString() } };
                dbMgr.Insert("multiple_alerts", dict);
            }

            _contentReceivedService.CreateRowsInAlertsReceivedForUser(userId, alertId);

            string updateQuery = "INSERT INTO alerts_sent (alert_id, user_id) VALUES (" + alertId + "," + userId + ")";
            dbMgr.ExecuteQuery(updateQuery);

            updateQuery = "INSERT INTO alerts_sent_stat (alert_id, user_id, date) VALUES (" + alertId + "," + userId + ", '" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "')";
            dbMgr.ExecuteQuery(updateQuery);

            if (AppConfiguration.IsNewContentDeliveringScheme)
            {
                var contentId = (long)alertId;
                var contentType = _alertRepository.GetContentTypeByContentId(contentId);

                _cacheInvalidationService.CacheInvalidation(contentType, contentId);
            }
            else
            {
                cacheManager.Add(alertId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
            }
        }
    }

    public class GroupAlert
    {
        public int UserId { get; set; }
        public List<GroupAlertContent> Alerts { get; set; }

        public GroupAlert()
        {
            Alerts = new List<GroupAlertContent>();

        }
    }

    public class GroupAlertContent
    {
        public string Description { get; set; }

        public string Id { get; set; }

        public string Date { get; set; }

        public GroupAlertContent(string Description, string Id, string Date)
        {
            this.Description = Description;
            this.Id = Id;
            this.Date = Date;
        }
    }
}