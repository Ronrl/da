<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<%
check_session()
%>
<!-- #include file="Common.asp" -->
<!-- #include file="header.asp" -->
<!-- #include file="footer.asp" -->

<%
'On Error Resume Next
' index CustomIncludes End
'-------------------------------

'===============================
' Save Page and File Name available into variables
'-------------------------------
Dim sFileName
Dim sTemplateFileName
Dim sPaginateFileName
Dim intSessionUserId

sFileName = "statistics_tbstat_view.asp"
sTemplateFileName = "statistics_tbstat_view.html"
sPaginateFileName = "paginate.html"
'===============================
  

if(DA=1) then
	intSessionUserId = Session("uid")
else
	intSessionUserId = Session("intUserId")
end if

if (intSessionUserId <> "") then


' index Show begin

'===============================
' Display page
'-------------------------------
' Load HTML template for this page
'-------------------------------
LoadTemplate alerts_dir & "\" & templateFolder & "\" & sTemplateFileName, "main"
SetVar "DListToolbarStat", ""
SetVar "DListStat", ""
SetVar "DListToolbar", ""
'-------------------------------
' Load HTML template of Header and Footer
'-------------------------------
LoadTemplate sHeaderFileName, "Header"
LoadTemplate sFooterFileName, "Footer"
'-------------------------------

'SetVar "myvalue", "test"
if(DA=1) then
else
	SetVar "strLogout", "Logout"
end if
GenerateMenu("Statistics")


Dim intInstallNum
Dim intUninstallNum
Dim strToolbarName
Dim strToolbarId
Dim rsToolbar
Dim rsInstall
Dim rsUninstall
Dim rsUrl
Dim intClickNum
Dim strUrl
Dim rsUrlStat
Dim i
Dim days

Dim from_day
Dim from_month
Dim from_year

Dim to_day
Dim to_month
Dim to_year

' Get dates
from_day = Request("from_day") 
from_month = Request("from_month") 
from_year = Request("from_year")

if(from_day="") then from_day=0 end if
if(from_month="") then from_month=0 end if
if(from_year="") then from_year=0 end if

if (CInt(from_day) < 1 OR CInt(from_day) > 31 OR CInt(from_month) < 1 OR CInt(from_month) > 12 OR CInt(from_year) < 2000 OR CInt(from_year) > year(date ())) then

	from_day = day(date ())
	from_month = month(date ())
	from_year = year (date())
end if

to_day = Request("to_day") 
to_month = Request("to_month") 
to_year = Request("to_year")

if(to_day="") then to_day=0 end if
if(to_month="") then to_month=0 end if
if(to_year="") then to_year=0 end if

if (CInt(to_day) < 1 OR CInt(to_day) > 31 OR CInt(to_month) < 1 OR CInt(to_month) > 12 OR CInt(to_year) < 2000 OR CInt(to_year) > year(date ())) then
	to_day = day(date ())
	to_month = month(date ())
	to_year = year(date ())
end if

if(Request("select_date_radio")="1" OR Request("select_date_radio")="") then
	if(Request("range")="1" OR Request("range")="") then
		Dim my_day
		Dim my_month
		Dim my_year
		Dim my_day1
		Dim my_month1
		Dim my_year1
		
		i=date()
		my_day = day(i)
		my_month = month(i)
		my_year = year (i)
		i=date()
		my_day1 = day(i)
		my_month1 = month(i)
		my_year1 = year (i)
		test_date=""

		if (date_format(1)="d") then test_date=test_date & my_day end if
		if (date_format(1)="m") then test_date=test_date & my_month end if
		if (date_format(1)="y") then test_date=test_date & my_year end if
		test_date=test_date & "/"
		if (date_format(2)="d") then test_date=test_date & my_day end if
		if (date_format(2)="m") then test_date=test_date & my_month end if
		if (date_format(2)="y") then test_date=test_date & my_year end if
		test_date=test_date & "/"
		if (date_format(3)="d") then test_date=test_date & my_day end if
		if (date_format(3)="m") then test_date=test_date & my_month end if
		if (date_format(3)="y") then test_date=test_date & my_year end if
		test_date1=""
		if (date_format(1)="d") then test_date1=test_date1 & my_day1 end if
		if (date_format(1)="m") then test_date1=test_date1 & my_month1 end if
		if (date_format(1)="y") then test_date1=test_date1 & my_year1 end if
		test_date1=test_date1 & "/"
		if (date_format(2)="d") then test_date1=test_date1 & my_day1 end if
		if (date_format(2)="m") then test_date1=test_date1 & my_month1 end if
		if (date_format(2)="y") then test_date1=test_date1 & my_year1 end if
		test_date1=test_date1 & "/"
		if (date_format(3)="d") then test_date1=test_date1 & my_day1 end if
		if (date_format(3)="m") then test_date1=test_date1 & my_month1 end if
		if (date_format(3)="y") then test_date1=test_date1 & my_year1 end if
	end if
	if(Request("range")="2") then
		i=date()-1
		my_day = day(i)
		my_month = month(i)
		my_year = year (i)
		i=date()-1
		my_day1 = day(i)
		my_month1 = month(i)
		my_year1 = year (i)
		test_date=""
		if (date_format(1)="d") then test_date=test_date & my_day end if
		if (date_format(1)="m") then test_date=test_date & my_month end if
		if (date_format(1)="y") then test_date=test_date & my_year end if
		test_date=test_date & "/"
		if (date_format(2)="d") then test_date=test_date & my_day end if
		if (date_format(2)="m") then test_date=test_date & my_month end if
		if (date_format(2)="y") then test_date=test_date & my_year end if
		test_date=test_date & "/"
		if (date_format(3)="d") then test_date=test_date & my_day end if
		if (date_format(3)="m") then test_date=test_date & my_month end if
		if (date_format(3)="y") then test_date=test_date & my_year end if
		test_date1=""
		if (date_format(1)="d") then test_date1=test_date1 & my_day1 end if
		if (date_format(1)="m") then test_date1=test_date1 & my_month1 end if
		if (date_format(1)="y") then test_date1=test_date1 & my_year1 end if
		test_date1=test_date1 & "/"
		if (date_format(2)="d") then test_date1=test_date1 & my_day1 end if
		if (date_format(2)="m") then test_date1=test_date1 & my_month1 end if
		if (date_format(2)="y") then test_date1=test_date1 & my_year1 end if
		test_date1=test_date1 & "/"
		if (date_format(3)="d") then test_date1=test_date1 & my_day1 end if
		if (date_format(3)="m") then test_date1=test_date1 & my_month1 end if
		if (date_format(3)="y") then test_date1=test_date1 & my_year1 end if
	end if
	if(Request("range")="3") then
	        i=date()-7
		my_day = day(i)
		my_month = month(i)
		my_year = year (i)
	        i=date()
		my_day1 = day(i)
		my_month1 = month(i)
		my_year1 = year (i)
		test_date=""
		if (date_format(1)="d") then test_date=test_date & my_day end if
		if (date_format(1)="m") then test_date=test_date & my_month end if
		if (date_format(1)="y") then test_date=test_date & my_year end if
		test_date=test_date & "/"
		if (date_format(2)="d") then test_date=test_date & my_day end if
		if (date_format(2)="m") then test_date=test_date & my_month end if
		if (date_format(2)="y") then test_date=test_date & my_year end if
		test_date=test_date & "/"
		if (date_format(3)="d") then test_date=test_date & my_day end if
		if (date_format(3)="m") then test_date=test_date & my_month end if
		if (date_format(3)="y") then test_date=test_date & my_year end if
		test_date1=""
		if (date_format(1)="d") then test_date1=test_date1 & my_day1 end if
		if (date_format(1)="m") then test_date1=test_date1 & my_month1 end if
		if (date_format(1)="y") then test_date1=test_date1 & my_year1 end if
		test_date1=test_date1 & "/"
		if (date_format(2)="d") then test_date1=test_date1 & my_day1 end if
		if (date_format(2)="m") then test_date1=test_date1 & my_month1 end if
		if (date_format(2)="y") then test_date1=test_date1 & my_year1 end if
		test_date1=test_date1 & "/"
		if (date_format(3)="d") then test_date1=test_date1 & my_day1 end if
		if (date_format(3)="m") then test_date1=test_date1 & my_month1 end if
		if (date_format(3)="y") then test_date1=test_date1 & my_year1 end if
	end if

	if(Request("range")="4") then
		my_day = "01"
		my_month = month(date ())
		my_year = year (date())
		my_day1 = day(date())
		my_month1 = month(date())
		my_year1 = year(date())
		test_date=""
		if (date_format(1)="d") then test_date=test_date & my_day end if
		if (date_format(1)="m") then test_date=test_date & my_month end if
		if (date_format(1)="y") then test_date=test_date & my_year end if
		test_date=test_date & "/"
		if (date_format(2)="d") then test_date=test_date & my_day end if
		if (date_format(2)="m") then test_date=test_date & my_month end if
		if (date_format(2)="y") then test_date=test_date & my_year end if
		test_date=test_date & "/"
		if (date_format(3)="d") then test_date=test_date & my_day end if
		if (date_format(3)="m") then test_date=test_date & my_month end if
		if (date_format(3)="y") then test_date=test_date & my_year end if
		test_date1=""
		if (date_format(1)="d") then test_date1=test_date1 & my_day1 end if
		if (date_format(1)="m") then test_date1=test_date1 & my_month1 end if
		if (date_format(1)="y") then test_date1=test_date1 & my_year1 end if
		test_date1=test_date1 & "/"
		if (date_format(2)="d") then test_date1=test_date1 & my_day1 end if
		if (date_format(2)="m") then test_date1=test_date1 & my_month1 end if
		if (date_format(2)="y") then test_date1=test_date1 & my_year1 end if
		test_date1=test_date1 & "/"
		if (date_format(3)="d") then test_date1=test_date1 & my_day1 end if
		if (date_format(3)="m") then test_date1=test_date1 & my_month1 end if
		if (date_format(3)="y") then test_date1=test_date1 & my_year1 end if
'-------------------
	end if
		
	if(Request("range")="5") then
		my_day = "01"
		my_month = month(date ())-1
		my_year = year (date())
		my_month1 = month(date ())-1
		my_year1 = year (date())
		my_day1 = "31"
		if(IsDate(my_month1 & "/" & my_day1 & "/" & my_year1)<>True) then
			my_day1="30"
			if(IsDate(my_month1 & "/" & my_day1 & "/" & my_year1)<>True) then
				my_day1="29"
				if(IsDate(my_month1 & "/" & my_day1 & "/" & my_year1)<>True) then
					my_day1="28"
					if(IsDate(my_month1 & "/" & my_day1 & "/" & my_year1)<>True) then
						my_day1="27"
					end if
				end if
        		end if
	        end if
'date from config---
		test_date=""
		if (date_format(1)="d") then test_date=test_date & my_day end if
		if (date_format(1)="m") then test_date=test_date & my_month end if
		if (date_format(1)="y") then test_date=test_date & my_year end if
		test_date=test_date & "/"
		if (date_format(2)="d") then test_date=test_date & my_day end if
		if (date_format(2)="m") then test_date=test_date & my_month end if
		if (date_format(2)="y") then test_date=test_date & my_year end if
		test_date=test_date & "/"
		if (date_format(3)="d") then test_date=test_date & my_day end if
		if (date_format(3)="m") then test_date=test_date & my_month end if
		if (date_format(3)="y") then test_date=test_date & my_year end if
		test_date1=""
		if (date_format(1)="d") then test_date1=test_date1 & my_day1 end if
		if (date_format(1)="m") then test_date1=test_date1 & my_month1 end if
		if (date_format(1)="y") then test_date1=test_date1 & my_year1 end if
		test_date1=test_date1 & "/"
		if (date_format(2)="d") then test_date1=test_date1 & my_day1 end if
		if (date_format(2)="m") then test_date1=test_date1 & my_month1 end if
		if (date_format(2)="y") then test_date1=test_date1 & my_year1 end if
		test_date1=test_date1 & "/"
		if (date_format(3)="d") then test_date1=test_date1 & my_day1 end if
		if (date_format(3)="m") then test_date1=test_date1 & my_month1 end if
		if (date_format(3)="y") then test_date1=test_date1 & my_year1 end if
'-------------------
	end if
	
	if(Request("range")="6") then
		my_day = "01"
		my_month = "01"
		my_year = "2001"
		my_day1 = day(date())
		my_month1 = month(date())
		my_year1 = year(date())
'date from config---
		test_date=""
		if (date_format(1)="d") then test_date=test_date & my_day end if
		if (date_format(1)="m") then test_date=test_date & my_month end if
		if (date_format(1)="y") then test_date=test_date & my_year end if
		test_date=test_date & "/"
		if (date_format(2)="d") then test_date=test_date & my_day end if
		if (date_format(2)="m") then test_date=test_date & my_month end if
		if (date_format(2)="y") then test_date=test_date & my_year end if
		test_date=test_date & "/"
		if (date_format(3)="d") then test_date=test_date & my_day end if
		if (date_format(3)="m") then test_date=test_date & my_month end if
		if (date_format(3)="y") then test_date=test_date & my_year end if
'-------------------
		test_date1=""
		if (date_format(1)="d") then test_date1=test_date1 & my_day1 end if
		if (date_format(1)="m") then test_date1=test_date1 & my_month1 end if
		if (date_format(1)="y") then test_date1=test_date1 & my_year1 end if
		test_date1=test_date1 & "/"
		if (date_format(2)="d") then test_date1=test_date1 & my_day1 end if
		if (date_format(2)="m") then test_date1=test_date1 & my_month1 end if
		if (date_format(2)="y") then test_date1=test_date1 & my_year1 end if
		test_date1=test_date1 & "/"
		if (date_format(3)="d") then test_date1=test_date1 & my_day1 end if
		if (date_format(3)="m") then test_date1=test_date1 & my_month1 end if
		if (date_format(3)="y") then test_date1=test_date1 & my_year1 end if
	end if
	from_date = test_date & " 00:00:00"
	to_date = test_date1 & " 23:59:59"
else
	from_date=""
	if (date_format(1)="d") then from_date=from_date & CStr(from_day) end if
	if (date_format(1)="m") then from_date=from_date & CStr(from_month) end if
	if (date_format(1)="y") then from_date=from_date & CStr(from_year) end if
	from_date=from_date & "/"
	if (date_format(2)="d") then from_date=from_date & CStr(from_day) end if
	if (date_format(2)="m") then from_date=from_date & CStr(from_month) end if
	if (date_format(2)="y") then from_date=from_date & CStr(from_year) end if
	from_date=from_date & "/"
	if (date_format(3)="d") then from_date=from_date & CStr(from_day) end if
	if (date_format(3)="m") then from_date=from_date & CStr(from_month) end if
	if (date_format(3)="y") then from_date=from_date & CStr(from_year) end if
	from_date=from_date & " 00:00:00"
	to_date=""
	if (date_format(1)="d") then to_date=to_date & CStr(to_day) end if
	if (date_format(1)="m") then to_date=to_date & CStr(to_month) end if
	if (date_format(1)="y") then to_date=to_date & CStr(to_year) end if
	to_date=to_date & "/"
	if (date_format(2)="d") then to_date=to_date & CStr(to_day) end if
	if (date_format(2)="m") then to_date=to_date & CStr(to_month) end if
	if (date_format(2)="y") then to_date=to_date & CStr(to_year) end if
	to_date=to_date & "/"
	if (date_format(3)="d") then to_date=to_date & CStr(to_day) end if
	if (date_format(3)="m") then to_date=to_date & CStr(to_month) end if
	if (date_format(3)="y") then to_date=to_date & CStr(to_year) end if
	to_date=to_date & " 23:59:59"
end if

days = array ("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec")

if(Request("select_date_radio")="1" OR Request("select_date_radio")="") then setVar "strCheckedOption1", "checked" end if 

if(Request("select_date_radio")="2") then setVar "strEnabledOption1", "disabled" end if 

if(Request("range")="1") then setVar "strSelectedOption1", "selected" end if 
if(Request("range")="2") then setVar "strSelectedOption2", "selected" end if 
if(Request("range")="3") then setVar "strSelectedOption3", "selected" end if 
if(Request("range")="4") then setVar "strSelectedOption4", "selected" end if 
if(Request("range")="5") then setVar "strSelectedOption5", "selected" end if 
if(Request("range")="6") then setVar "strSelectedOption6", "selected" end if 

if(Request("select_date_radio")="2") then setVar "strCheckedOption2", "checked" end if 

if(Request("select_date_radio")="1" OR Request("select_date_radio")="") then setVar "strEnabledOption2", "disabled" end if 

Dim strFromMonth
i=1
Do while i <= 12
	if (CInt(from_month) = i) then
		strFromMonth = strFromMonth & "<option value='" & CStr(i) & "'  selected>" & days(i-1) & "</option>"
	else 
		strFromMonth = strFromMonth & "<option value='" & CStr(i) & "'>" & days(i-1) & "</option>"
	end if
i=i+1
loop

Dim strFromDay
i=1
Do while i <= 31
	if (CInt(from_day) = i) then
		strFromDay = strFromDay & "<option value='" & CStr(i) & "'  selected>" & CStr(i) & "</option>"
	else 
		strFromDay = strFromDay & "<option value='" & CStr(i) & "'>" & CStr(i) & "</option>"
	end if
i=i+1
loop

Dim strFromYear
i=2001
Do while i <= year(date ())
	if (CInt(from_year) = i) then
		strFromYear = strFromYear & "<option value='" & CStr(i) & "'  selected>" & CStr(i) & "</option>"
	else 
		strFromYear = strFromYear & "<option value='" & CStr(i) & "'>" & CStr(i) & "</option>"
	end if
i=i+1
loop

Dim strToMonth
i=1
Do while i <= 12
	if (CInt(to_month) = i) then
		strToMonth = strToMonth & "<option value='" & CStr(i) & "'  selected>" & days(i-1) & "</option>"
	else 
		strToMonth = strToMonth & "<option value='" & CStr(i) & "'>" & days(i-1) & "</option>"
	end if
i=i+1
loop

Dim strToDay

i=1
Do while i <= 31
	if (CInt(to_day) = i) then
		strToDay = strToDay & "<option value='" & CStr(i) & "'  selected>" & CStr(i) & "</option>"
	else 
		strToDay = strToDay & "<option value='" & CStr(i) & "'>" & CStr(i) & "</option>"
	end if
i=i+1
loop

Dim strToYear
i=2001
Do while i <= year(date ())
	if (CInt(to_year) = i) then
		strToYear = strToYear & "<option value='" & CStr(i) & "'  selected>" & CStr(i) & "</option>"
	else 
		strToYear = strToYear & "<option value='" & CStr(i) & "'>" & CStr(i) & "</option>"
	end if
i=i+1
loop

SetVar "strFromMonth", strFromMonth
SetVar "strFromDay", strFromDay
SetVar "strFromYear", strFromYear

SetVar "strToMonth", strToMonth
SetVar "strToDay", strToDay
SetVar "strToYear", strToYear

SetVar "strType", Request("type")





Set rsToolbar = Conn.Execute("SELECT * FROM [toolbars] WHERE user_id=" & intSessionUserId)
do while Not rsToolbar.EOF
	
	intInstallNum=0
	intUninstallNum=0

	strToolbarName=rsToolbar("name")
	strToolbarId=rsToolbar("id")
	SetVar "intToolbarId", strToolbarId


'alerts.sent_date >= '" + from_date + "' AND alerts.sent_date <= '" + to_date + "'
	Set rsInstall = Conn.Execute("SELECT * FROM [tb_install_stat] WHERE toolbar_id=" & rsToolbar("id"))
	do while not rsInstall.EOF
		intInstallNum=intInstallNum+1
		rsInstall.MoveNext	
	loop
	rsInstall.Close
	Set rsUninstall = Conn.Execute("SELECT * FROM [tb_uninstall_stat] WHERE toolbar_id=" & rsToolbar("id"))
	do while not rsUninstall.EOF
		intUninstallNum=intUninstallNum+1
		rsUninstall.MoveNext	
	loop
	rsUninstall.Close

       	intClickNum=0
	Set rsUrl = Conn.Execute("SELECT * FROM [urls] WHERE toolbar_id=" & rsToolbar("id") & " AND search=0")
	do while not rsUrl.EOF
	  	Set rsUrlStat = Conn.Execute("SELECT * FROM [url_stat] WHERE url_id=" & rsUrl("id") & " AND toolbar_id=" & rsToolbar("id"))
		  do while not rsUrlStat.EOF
			intClickNum=intClickNum+1
			rsUrlStat.MoveNext	
		  loop
		rsUrl.MoveNext	
	loop
	rsUrl.Close

       	intSearchNum=0
	Set rsUrl = Conn.Execute("SELECT * FROM [urls] WHERE toolbar_id=" & rsToolbar("id") & " AND search=1")
	do while not rsUrl.EOF
	  	Set rsUrlStat = Conn.Execute("SELECT * FROM [url_stat] WHERE url_id=" & rsUrl("id") & " AND toolbar_id=" & rsToolbar("id"))
		  do while not rsUrlStat.EOF
			intSearchNum=intSearchNum+1
			rsUrlStat.MoveNext	
		  loop
		rsUrl.MoveNext	
	loop
	rsUrl.Close

	SetVar "intClicksNum", intClickNum
	SetVar "intSearchesNum", intSearchNum


'	Set rsInstall = Conn.Execute("SELECT * FROM [tb_install_stat] WHERE toolbar_id=" & rsToolbar("id") & " AND date >= '" & from_date & "' AND date <= '" & to_date & "'")
'	do while not rsInstall.EOF
'		intInstallNum=intInstallNum+1
'		rsInstall.MoveNext	
'	loop
'	rsInstall.Close
'	Set rsUninstall = Conn.Execute("SELECT * FROM [tb_uninstall_stat] WHERE toolbar_id=" & rsToolbar("id") & " AND date >= '" & from_date & "' AND date <= '" & to_date & "'")
'	do while not rsUninstall.EOF
'		intUninstallNum=intUninstallNum+1
'		rsUninstall.MoveNext	
'	loop
'	rsUninstall.Close




if(Not IsNull(strToolbarName)) then SetVar "strToolbarName", strToolbarName else SetVar "strToolbarName", "&nbsp;" end if
'if(Not IsNull(strToolbarId)) then SetVar "strToolbarId", strToolbarId else SetVar "strToolbarId", "&nbsp;" end if

SetVar "intInstallNum", intInstallNum
SetVar "intUninstallNum", intUninstallNum 
Parse "DListToolbarStat", True


Dim my_test_date

my_day = day(to_date)
my_month = month(to_date)
my_year = year (to_date)

my_test_date = CDate(from_date)
my_day1 = day(my_test_date)
my_month1 = month(my_test_date)
my_year1 = year (my_test_date)

i=0
'	Response.Write "from_date=" & CDate(from_date) & "<br>"


Dim intTotalInstalls
Dim intTotalUninstalls
Dim intTotalClicks
Dim intTotalSearches

        Dim intInstalls
        Dim intUninstalls
        Dim intClicks
	Dim intSearches


intTotalInstalls=0
intTotalUninstalls=0
intTotalClicks=0
intTotalSearches=0


'Dim dates_arr(1000)
'Dim clicks_arr(1000)
'Dim searches_arr(1000)



Dim ClicksList:Set ClicksList=New AssocArray
Dim SearchesList:Set SearchesList=New AssocArray
Dim InstallsList:Set InstallsList=New AssocArray
Dim UninstallsList:Set UninstallsList=New AssocArray


do while my_day <> my_day1 OR my_month <> my_month1 OR my_year <> my_year1

	ClicksList(my_test_date)="0"
	SearchesList(my_test_date)="0"
	InstallsList(my_test_date)="0"
	UninstallsList(my_test_date)="0"

	i=i+1
	my_test_date = CDate(from_date)+i
	my_day1 = day(my_test_date)
	my_month1 = month(my_test_date)
	my_year1 = year (my_test_date)
loop

	ClicksList(my_test_date)="0"
	SearchesList(my_test_date)="0"
	InstallsList(my_test_date)="0"
	UninstallsList(my_test_date)="0"


my_day = day(to_date)
my_month = month(to_date)
my_year = year (to_date)

my_test_date = CDate(from_date)
my_day1 = day(my_test_date)
my_month1 = month(my_test_date)
my_year1 = year (my_test_date)

i=0


 	Set rsUrlStat = Conn.Execute("SELECT COUNT(*) as mycnt, date FROM [url_stat] INNER JOIN [urls] ON url_stat.url_id=urls.id WHERE url_stat.toolbar_id=" &  rsToolbar("id") & " AND date >= '" & from_date & "' AND date <= '"&to_date&"' AND search<>1 GROUP BY date")	
	do while not rsUrlStat.EOF 
		  intClicks = rsUrlStat("mycnt")
		  strDate = rsUrlStat("date")
		  intTotalClicks=intTotalClicks+rsUrlStat("mycnt")
		ClicksList(strDate)=intClicks
		rsUrlStat.MoveNext
	loop
	rsUrlStat.Close

 	Set rsUrlStat = Conn.Execute("SELECT COUNT(*) as mycnt, date FROM [url_stat] INNER JOIN [urls] ON url_stat.url_id=urls.id WHERE url_stat.toolbar_id=" &  rsToolbar("id") & " AND date >= '" & from_date & "' AND date <= '"&to_date&"' AND search=1 GROUP BY date")	
	do while not rsUrlStat.EOF 
		  intSearches = rsUrlStat("mycnt")
		  strDate = rsUrlStat("date")
		  intTotalSearches=intTotalSearches+rsUrlStat("mycnt")
		SearchesList(strDate)=intSearches
		rsUrlStat.MoveNext
	loop
	rsUrlStat.Close

	Set rsInstall = Conn.Execute("SELECT COUNT(*) as mycnt, date FROM [tb_install_stat] WHERE toolbar_id=" & rsToolbar("id") & " AND date >= '" & from_date & "' AND date <= '"&to_date&"' GROUP BY date")
	do while not rsInstall.EOF 
		  intTotalInstalls=intTotalInstalls+rsInstall("mycnt")
		  intInstalls = rsInstall("mycnt")
		  strDate = rsInstall("date")
		InstallsList(strDate)=intInstalls
		rsInstall.MoveNext
	loop
	rsInstall.Close

	Set rsUninstall = Conn.Execute("SELECT COUNT(*) as mycnt, date FROM [tb_uninstall_stat] WHERE toolbar_id=" & rsToolbar("id") & " AND date >= '" & from_date & "' AND date <= '"&to_date&"' GROUP BY date")
	do while not rsUninstall.EOF 
		  intTotalUninstalls=intTotalUninstalls+rsUninstall("mycnt")
	          intUninstalls = rsUninstall("mycnt")
		  strDate = rsUninstall("date")
		UninstallsList(strDate)=intUninstalls
		rsUninstall.MoveNext
	loop
	rsUninstall.Close


do while my_day <> my_day1 OR my_month <> my_month1 OR my_year <> my_year1



	SetVar "strToolbarDate", my_test_date


        intInstalls = 0
        intUninstalls = 0
        intClicks = 0
	intSearches = 0



	intClicks = ClicksList(my_test_date)
	intSearches = SearchesList(my_test_date)
	intInstalls = InstallsList(my_test_date)
	intUninstalls = UninstallsList(my_test_date)

	if(intInstalls<>0 OR intUninstalls<>0 OR intSearches<>0 OR intClicks<>0) then
		  SetVar "intToolbarSearches", intSearches
		  SetVar "intToolbarClicks", intClicks
		  SetVar "intToolbarInstalls", intInstalls
		  SetVar "intToolbarUninstalls", intUninstalls
		  Parse "DListStat", True
	end if

'	Parse "DListStat", False


	i=i+1
	my_test_date = CDate(from_date)+i
	my_day1 = day(my_test_date)
	my_month1 = month(my_test_date)
	my_year1 = year (my_test_date)
loop

	SetVar "strToolbarDate", my_test_date


        intInstalls = 0
        intUninstalls = 0
        intClicks = 0
	intSearches = 0



	intClicks = ClicksList(my_test_date)
	intSearches = SearchesList(my_test_date)
	intInstalls = InstallsList(my_test_date)
	intUninstalls = UninstallsList(my_test_date)

	if(intInstalls<>0 OR intUninstalls<>0 OR intSearches<>0 OR intClicks<>0) then
		  SetVar "intToolbarSearches", intSearches
		  SetVar "intToolbarClicks", intClicks
		  SetVar "intToolbarInstalls", intInstalls
		  SetVar "intToolbarUninstalls", intUninstalls
		  Parse "DListStat", True
	end if

	
	SetVar "strToolbarTotalInstalls", intTotalInstalls
	SetVar "strToolbarTotalUninstalls", intTotalUninstalls
	SetVar "strToolbarTotalClicks", intTotalClicks
	SetVar "strToolbarTotalSearches", intTotalSearches

'	Response.Write "date=" & CDate(my_test_date) & "<br>"

	Parse "DListToolbar", True

  SetVar "strToolbarDate", ""
  SetVar "intToolbarSearches", ""
  SetVar "intToolbarClicks", ""
  SetVar "intToolbarInstalls", ""
  SetVar "intToolbarUninstalls", ""



	Parse "DListStat", False
'	Parse "DListStat", True

	rsToolbar.MoveNext                
loop
rsToolbar.Close






'-------------------------------
' Step through each form
'-------------------------------
Header_Show
Footer_Show
'-------------------------------
' Process page templates
'-------------------------------
Parse "Header", False
Parse "Footer", False
Parse "main", False
'-------------------------------
' Output the page to the browser
'-------------------------------
Response.write PrintVar("main")

' index Show End

'-------------------------------
' Destroy all object variables
'-------------------------------

' index Close Event begin
' index Close Event End

UnloadTemplate
'===============================

else

Login(Request("intErrorCode"))

end if

'===============================
' Display Grid Form
'-------------------------------
%>
<!-- #include file="db_conn_close.asp" -->