﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<!-- #include file="md5.asp" -->
<!-- #include file="demo.inc" -->
<%

check_session()

uid = Session("uid")

%>
<!DOCTYPE html>
<html>
<head>
	<title>DeskAlerts</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link href="css/style9.css" rel="stylesheet" type="text/css">
	<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
	<script language="javascript" type="text/javascript">

		$(document).ready(function() {
			$("#add_user_button").button();
			$("#add_user_button").bind("click", function() {
			    if (($("input[name='email']").val() == "") && ($("#sendLogs").is(":checked")))
			    {
			        $("#sendLogs").removeAttr("checked");
			        alert("Fill email field to activate send crush logs feature.");
			        return true;
			    }

				if (!$("#user_name").val())
				{
					alert("User name must not be empty!");
				}
				else
				{
					$('#user_form').submit();
				}
			});
			
			$("#cancel_change_password_button").button();
			$("#cancel_change_password_button").bind("click", function() {
				$('#change_password_tr').hide(100);
				$('#password_changed').val("0");
			});;
			
			$("#change_password_button").button();
			$("#change_password_button").bind("click", function() {
				$('#change_password_tr').show(1000);
				$('#password_changed').val("1");
			});
			<% if DEMO=1 then%>
			if ($("#user_name").val() == "demo"){
				$("#defaultUserWarning").show();
				$("#user_name").prop('disabled',true);
				$("#change_password_button").hide();
				$("input[name='email']").prop('disabled',true);
				$("input[name='mobile_phone']").prop('disabled',true);
				$("#userGuideSelect").prop('disabled',true);
				$("#userLangSelect").prop('disabled',true);
				$("#start_page").prop('disabled',true);
				$("#add_user_button").hide();
			}
			<% end if%>

            $("#sendLogs").click(function(){
                var email = $("input[name=email]").val();

                if ((email == "") && ($(this).is(":checked"))) {
                    $(this).removeAttr("checked");
                    alert("Fill email field to activate send crush logs feature.");
                }
			});
		});
	</script>
	<script>
		
		
	function fill_select(start_page)
	{
		var menu_item = window.top.frames["menu"].document.getElementsByName('menu_item');
		
		for(var i=0;i<menu_item.length;i++)
		{

			var menu_subitem = window.top.frames["menu"].document.getElementsByName('menu_subitem_' + menu_item.item(i).innerHTML);
			
			if(menu_subitem.length>0)
			{
				for(j=0;j<menu_subitem.length;j++)
				{
					var x = document.getElementById("start_page");
					var option = document.createElement("option");
					option.text=menu_item.item(i).innerHTML + "." + menu_subitem.item(j).innerHTML;
					
					menu_subitem_length=menu_subitem.item(j).href.length;
					menu_subitem_pos=menu_subitem.item(j).href.indexOf('/admin/') + 7;
					href=menu_subitem.item(j).href.substr(menu_subitem_pos,menu_subitem_length);
					option.value=href;
					
					if(start_page==href)
					{
						option.selected=true;
					}
					x.add(option);
				}
			}
			else
			{
				var x = document.getElementById("start_page");
				var option = document.createElement("option");
				option.text=menu_item.item(i).innerHTML;
				parent_element=menu_item.item(i).parentNode.href;
			    menu_parentelement_length=parent_element.length;
				menu_parentelement_pos=parent_element.indexOf('/admin/') + 7;
				href=parent_element.substr(menu_parentelement_pos,menu_parentelement_length);
					
				option.value=href;
				
				if(start_page==href)
				{
					option.selected=true;
				}
				x.add(option);
			}
		}
	}
	</script>
	<style>
		.data_table tr td {border:0px none; padding-left: 5px; padding-right: 5px}
	</style>
</head>

<%
if (uid <> "")  then
	userId = uid
	save = Request("save")
	
	userName = ""
	userEmail = ""
	userMobilePhone = ""
	userStartPage = ""
	
	if (save <> "") then
	
		userName = Request("name")
		userPassword = Request("password")
		userOldPassword = Request("old_password")
		userConfirmPassword = Request("confirm_password")
		userEmail = Request("email")
		userMobilePhone = Request("mobile_phone")
		userStartPage = Request("start_page")
		userLang = Request("userLang")
		userGuide = Request("guide")
        userSendLogs = Request("sendLogs")

        if (userSendLogs = "on") then
            userSendLogs = 1
        else
            userSendLogs = 0
        end if
		
		checkSQL = "SELECT COUNT(1) as cnt, id FROM users WHERE name='"&userName&"' AND (role='E' OR role='A') GROUP BY pass, id"
		SET RS = Conn.Execute(checkSQL)
		
		errMsg=""
		saveSQL=""
		checkExist = 0
		
		if (NOT RS.EOF) then
			if (Clng(RS("cnt")) > 0 AND (Clng(RS("id")) <> Clng(userId))) then
				checkExist = 1
				errMsg = LNG_USER_NAME_EXIST
			end if
		end if
		
		if (checkExist = 0) then
			checkSQL = "SELECT pass FROM users WHERE id="&userId
			SET RS = Conn.Execute(checkSQL)
			passSQL=""
			if (Request("password_changed") = "1") then			
				if (md5(userOldPassword) = RS("pass")) then
					if (userPassword = userConfirmPassword) then
						'if (userEmail <> "") then
							passSQL="pass = '"&md5(userPassword)&"',"
						'else
						'	errMsg=LNG_PLEASE_ENTER_EMAIL_BEFORE_CHANGING_PASSWORD	
						'end if
					else
						errMsg=LNG_PASSWORDS_DONOT_MATCH_TRY_AGAIN	
					end if
				else
					errMsg=LNG_OLD_PASSWORD_IS_NOT_VALID	
				end if
			end if
			saveSQL = "UPDATE users SET name = '"&userName&"',"&passSQL&" email = '"&userEmail&"', mobile_phone = '"&userMobilePhone&"',start_page = '"&userStartPage&"',guide = '"&userGuide&"', send_logs = '"&userSendLogs&"' WHERE id = "&userId

			if (saveSQL <> "" AND errMsg = "") then
				SET RS = Conn.Execute(saveSQL)
				SQL = "SELECT ll.code FROM languages_list ll, users_langs ul WHERE ul.lang_id=ll.id AND ul.user_id="&userId
				Set RSUserLang = Conn.Execute(SQL)
				if (NOT RSUserLang.EOF) then	
					SQL = "UPDATE users_langs SET lang_id=(SELECT id FROM languages_list WHERE code='"&userLang&"') WHERE user_id="&userId
				else
					'SQL = "INSERT INTO users_langs (user_id,lang_id) VALUES ("&userId&",(SELECT id FROM languages_list WHERE code='"&userLang&"'))"
					SQL = "INSERT INTO users_langs (user_id,lang_id) SELECT '"&userId&"',(SELECT id FROM languages_list WHERE code='"&userLang&"')"
				end if
				Conn.Execute(SQL)
				Set RSUserLang = nothing
			end if
			
		end if
	end if

	SQL = "SELECT name, email, mobile_phone,start_page,guide, send_logs FROM users WHERE id = "&userId
	SET RS =  Conn.Execute(SQL)
		
	if (NOT RS.EOF) then
		userName = RS("name")
		userEmail = RS ("email")
		userMobilePhone = RS ("mobile_phone")
		userStartPage = RS("start_page")
		userGuide = RS("guide")
        userSendLogs = RS("send_logs")

		if isNull(userGuide) then
			userGuide=""
		end if

        if (userSendLogs = 1) then
            userSendLogs = "checked"
        else
            userSendLogs = userSendLogs
        end if
	end if
	
	SQL = "SELECT ll.code FROM languages_list ll, users_langs ul WHERE ul.lang_id=ll.id AND ul.user_id="&userId
	Set RSUserLang = Conn.Execute(SQL)
	if (NOT RSUserLang.EOF) then
		userLang = RSUserLang("code")
	else
		Set RSDefLang = Conn.Execute("SELECT val FROM settings WHERE name='ConfDefaultLanguage'")
		if (NOT RSDefLang.EOF) then
			userLang = RSDefLang("val")
		else
			userLang = "EN"
		end if
		Set RSDefLang = nothing
	end if
	Set RSUserLang = nothing
%>

<body style="margin:0px" class="body" onLoad="fill_select('<%=userStartPage%>');">
<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	<tr>
	<td>
	<table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
		<tr>
			<td  height=31 class="main_table_title">
				<img src="images/menu_icons/settings_20.png" alt="" width="20" height="20" border="0" style="padding-left:7px">
					<a href="#" class="header_title" style="position:absolute;top:15px"><%=LNG_PROFILE_SETTINGS%></a>
			</td>
		</tr>
		<tr>
		<td style="padding:5px; width:100%" height="100%"   valign="top" class="main_table_body">
			<div style="margin:10px;">
				<form name="my_form" method="post" id="user_form">
					<font color="red"><%=errMsg%></font>
					<p id="defaultUserWarning" style="display:none">
						You are logged in with a shared "demo" account. To be able to customize profile settings, create your own account from the Publishers menu.
					</p>
					<table style="padding:0px; margin-top:5px; margin-bottom:5px; border-collapse:collapse; border:0px">
						<tr>
							<td>
								<label for="name"><%=LNG_NAME%>:</label>
							</td>
						</tr>
						<tr>
							<td>
								<input id="user_name" type="text" value="<%=userName%>" name="name"></input>
							</td>
						</tr>
						<tr>
							<td style="padding-top:10px;">
								<input type="hidden" id="password_changed" name="password_changed"/>
								<a id="change_password_button"><%=LNG_CHANGE_PASSWORD%></a>
							</td>
						</tr>
						<tr id="change_password_tr" style="display:none;">
							<td style="padding-top:10px">
								<table cellspacing="0" cellpadding="0" class="data_table">
									<tr id="password_old_label_tr">
										<td style="padding-top:10px;">
											<label for="old_password"><%=LNG_ENTER_OLD_PASSWORD & ":" %></label>
										</td>
									</tr>
									<tr id="password_old_value_tr">
										<td>
											<input type="password" value="" name="old_password"></input>
										</td>
									</tr>
									<tr id="password_label_tr">
										<td style="padding-top:10px;">
											<label for="password"><%=LNG_ENTER_NEW_PASSWORD & ":" %></label>
										</td>
									</tr>
									<tr id="password_value_tr">
										<td>
											<input type="password" value="" name="password"></input>
										</td>
									</tr>
									<tr id="password_confirm_label_tr">
										<td style="padding-top:10px;">
											<label for="confirm_password"><%=LNG_CONFIRM_NEW_PASSWORD & ":" %></label>
										</td>
									</tr>
									<tr id="password_confirm_value_tr">
										<td>
											<input type="password" value="" name="confirm_password"></input>
										</td>
									</tr>
									<tr id="password_change_cancel_tr">
										<td style="padding-top:5px; padding-bottom:5px; text-align:right">
											<a id="cancel_change_password_button"><%=LNG_CANCEL%></a>
										</td>
									</tr>	
								</table>
							</td>
						</tr>
								
						<tr>
							<td style="padding-top:10px;">
								<label for="email"><%=LNG_EMAIL%>:</label>
							</td>
						</tr>
						<tr>
							<td>
								<input type="text" value="<%=userEmail%>" name="email">
							</td>
						</tr>
						<tr>
							<td style="padding-top:10px;">
								<label for="mobile_phone"><%=LNG_MOBILE_PHONE%>:</label>
							</td>
						<tr>
						<tr>
							<td>
								<input type="text" value="<%=userMobilePhone%>" name="mobile_phone"></input>
							</td>
						</tr>
						<tr>
							<td style="padding-top:10px;">
								<label for="userLang"><%=LNG_USER_GUIDE_MODE%>:</label>
							</td>
						<tr>
						<tr>
							<td>
								<select style='width:175px;' id="userGuideSelect" name="guide">
									<option value="none" <% if(userGuide="none") then 
										response.write "selected" 
										end if
									%>>
									<%=LNG_GUIDE_SHOW_NONE %>
									</option>
									<option value="new" <% if(userGuide="new") then 
										response.write "selected" 
										end if
									%>>
									<%=LNG_GUIDE_SHOW_NEW %>
									</option>
									<option value="all" <% if(userGuide="all") then 
										response.write "selected" 
										end if
									%>>
									<%=LNG_GUIDE_SHOW_ALL %>
									</option>
								</select>
							</td>
						</tr>
						<tr>
							<td style="padding-top:10px;">
								<label for="userLang"><%=LNG_USER_LANGUAGE%>:</label>
							</td>
						<tr>
						<tr>
							<td>
								<select style='width:175px;' id="userLangSelect" name="userLang">
									<%
										Set rsLang = Conn.Execute ("SELECT name, code FROM languages_list")
										do while not rsLang.EOF
									%>
									<option value="<%=HtmlEncode(rsLang("code")) %>" <% if(userLang=rsLang("code")) then 
										response.write "selected" 
										end if
									%>>
										<%=HtmlEncode(rsLang("name")) %></option>
									<%
										rsLang.MoveNext
										loop
									%>
								</select>
							</td>
							<td><span style="color:#aaaaaa;padding-left:10px">*<%=LNG_LANG_DESC %>.</span></td>
						</tr>
						<!--Start-->
						<tr>
							<td style='padding-top:10px;'>
								<label for="start_page"><%=LNG_START_PAGE%></label>
							</td>
						</tr>
						<tr>
							<td>
								<select style='width:175px;' id='start_page' name='start_page'>
									<option value='dashboard.aspx' selected>Choose start page</option>
								</select>
							</td>
						</tr>
						<!--End-->
                        <tr>
							<td style="padding-top:10px;">
								<label for="sendLogs"><%=LNG_SENDLOGS%>:</label>
							</td>
						</tr>
						<tr>
							<td>
								<input type="checkbox" name="sendLogs" <%=userSendLogs%> id="sendLogs">
							</td>
						</tr>
						<tr>
							<td style="padding-top:10px;" align="right">
								<a id='add_user_button'><%=LNG_SAVE%></a>
							</td>
						</tr>
					</table>
					
					<input type="hidden" name="id" value="<%=userId%>"></input>
					<input type="hidden" name="save" value="1"></input>
				</form>
			</div>
		</td>
	</tr>
</table>
<%
else

  Response.Redirect "index.asp"

end if

%> 
</body>
</html>
<!-- #include file="db_conn_close.asp" -->