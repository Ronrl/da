<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%
check_session()

Response.Expires = 0
Response.Expiresabsolute = Now() - 1 
Response.AddHeader "pragma","no-cache" 
Response.AddHeader "cache-control","private" 
Response.CacheControl = "no-cache" 
Response.ContentType = "text/html; charset=utf-8"

uid = Session("uid")

if (uid <>"") then

   if(Request("offset") <> "") then 
	offset = clng(Request("offset"))
   else 
	offset = 0
   end if	

   if(Request("uname") <> "") then 
	myuname = Request("uname")
   else 
	myuname = ""     
   end if	

   if(Request("limit") <> "") then 
	limit = clng(Request("limit"))
   else 
	limit=50
   end if
  

	Set RS = Conn.Execute("SELECT id FROM users WHERE id="&uid&" AND role='E'")
	if(Not RS.EOF) then
		gid = 1
		Set rs_policy = Conn.Execute("SELECT p.id, p.type, pu.user_id FROM policy p INNER JOIN policy_editor pe ON p.id=pe.policy_id LEFT JOIN policy_user pu ON pu.policy_id = p.id AND pu.user_id = 0 WHERE pe.editor_id=" & uid)
		if Not rs_policy.EOF then
			if rs_policy("type")="A" or not IsNull(rs_policy("user_id")) then 'can send to all
				gid = 0
			end if
		end if
		if(gid=1) then
			editor_id=RS("id")
			policy_ids=""
			policy_ids=editorGetPolicyIds(editor_id)
			group_ids=""
			group_ids=editorGetGroupIds(policy_ids)
			group_ids=replace(group_ids, "group_id", "users_groups.group_id")
			if(group_ids <> "") then
				group_ids = "OR " & group_ids
			end if
			ou_ids=""
			ou_ids=editorGetOUIds(policy_ids)
			ou_ids=replace(ou_ids, "ou_id", "OU_User.Id_OU")
			if(ou_ids <> "") then
				ou_ids = "OR " & ou_ids
			end if
		end if
	else
		gid = 0
	end if
	RS.Close

	if(myuname<>"") then
		if(gid<>0) then
			Set RS = Conn.Execute("SELECT COUNT(DISTINCT users.id) as cnt FROM users LEFT JOIN policy_user ON users.id=policy_user.user_id LEFT JOIN users_groups ON users.id=users_groups.user_id LEFT JOIN OU_User ON users.id=OU_User.Id_user WHERE role='U' AND (name LIKE N'%"&Replace(myuname, "'", "''")&"%' OR display_name LIKE N'%"&Replace(myuname, "'", "''")&"%') AND ("&policy_ids&" "&group_ids&" "&ou_ids&")")
		else
			Set RS = Conn.Execute("SELECT COUNT(id) as cnt FROM users WHERE role='U' AND (name LIKE N'%"&Replace(myuname, "'", "''")&"%' OR display_name LIKE N'%"&Replace(myuname, "'", "''")&"%')")
		end if
	else
		if(gid<>0) then
			Set RS = Conn.Execute("SELECT COUNT(DISTINCT users.id) as cnt FROM users LEFT JOIN policy_user ON users.id=policy_user.user_id LEFT JOIN users_groups ON users.id=users_groups.user_id LEFT JOIN OU_User ON users.id=OU_User.Id_user WHERE role='U' AND ("&policy_ids&" "&group_ids&" "&ou_ids&")")
		else
			Set RS = Conn.Execute("SELECT COUNT(id) as cnt FROM users WHERE role='U'")
		end if
	end if
	cnt=0

	if(Not RS.EOF) then
		cnt=RS("cnt")
        end if
	RS.Close
  j=cnt/limit


if(cnt>0) then

	page="get_users.asp?uname=" & myuname & "&"
	name= LNG_USERS &"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"


	if(limit=50) then response.write LNG_YOU_HAVE & " " & cnt & " "&name&" <br/> "& LNG_RECORDS_PER_PAGE &" | 50 | <A href=""#"" onclick=""javascript: page('get_users.asp?offset=0&limit=100&uname="&myuname&"');"">100</a> | <A href=""#"" onclick=""javascript: page('get_users.asp?offset=0&limit=500&uname="&myuname&"');"">500</a> | <A href=""#"" onclick=""javascript: page('get_users.asp?offset=0&limit=1000&uname="&myuname&"');"">1000</a>" end if
	if(limit=100) then response.write LNG_YOU_HAVE & " " & cnt & " "&name&" <br/> "& LNG_RECORDS_PER_PAGE &" | <A href=""#"" onclick=""javascript: page('get_users.asp?offset=0&limit=50&uname="&myuname&"');"">50</a> | 100 | <A href=""#"" onclick=""javascript: page('get_users.asp?offset=0&limit=500&uname="&myuname&"');"">500</a> | <A href=""#"" onclick=""javascript: page('get_users.asp?offset=0&limit=1000&uname="&myuname&"');"">1000</a>" end if
	if(limit=500) then response.write LNG_YOU_HAVE & " " & cnt & " "&name&" <br/> "& LNG_RECORDS_PER_PAGE &" | <A href=""#"" onclick=""javascript: page('get_users.asp?offset=0&limit=50&uname="&myuname&"');"">50</a> | <A href=""#"" onclick=""javascript: page('get_users.asp?offset=0&limit=100&uname="&myuname&"');"">100</a> | 500 | <A href=""#"" onclick=""javascript: page('get_users.asp?offset=0&limit=1000&uname="&myuname&"');"">1000</a>" end if
	if(limit=1000) then response.write LNG_YOU_HAVE & " " & cnt & " "&name&" <br/> "& LNG_RECORDS_PER_PAGE &" | <A href=""#"" onclick=""javascript: page('get_users.asp?offset=0&limit=50&uname="&myuname&"');"">50</a> | <A href=""#"" onclick=""javascript: page('get_users.asp?offset=0&limit=100&uname="&myuname&"');"">100</a> | <A href=""#"" onclick=""javascript: page('get_users.asp?offset=0&limit=500&uname="&myuname&"');"">500</a> | 1000" end if
	response.write "<br/>"
	Response.Write make_pages_ajax(offset, cnt, limit, page, name) 

			%>
			<table class='data_table' cellspacing='0' cellpadding='3' class='table_get'>
			<tr class='data_table_title'><td class='table_title'>
			<input type='checkbox' name='select_all' id='select_all' onclick='javascript: select_all_users();'></td>
			<td class='table_title'><%=LNG_USERNAME %></td>
			<%
			if(AD=3) then
			%>
			<td class='table_title'><%=LNG_DISPLAY_NAME %></td><td class='table_title'><%=LNG_DOMAIN %></td></tr>
	<%	
	else
	%>
	</tr>
	<%
	end if 
	
	if(myuname<>"") then
		if(gid<>0) then
			Set RS = Conn.Execute("SELECT * FROM (SELECT *, ROW_NUMBER() OVER (order by name) as rnum FROM (SELECT DISTINCT users.id as id, users.name as name, domains.name as domain_name, display_name, domain_id, context_id FROM users LEFT JOIN domains ON users.domain_id=domains.id LEFT JOIN policy_user ON users.id=policy_user.user_id LEFT JOIN users_groups ON users.id=users_groups.user_id LEFT JOIN OU_User ON users.id=OU_User.Id_user WHERE role='U' AND (users.name LIKE N'%"&Replace(myuname, "'", "''")&"%' OR users.display_name LIKE N'%"&Replace(myuname, "'", "''")&"%') AND ("&policy_ids&" "&group_ids&" "&ou_ids&")) users) users WHERE rnum Between "&(offset+1)&" and "&(offset+limit)&" ")	
		else
			Set RS = Conn.Execute("SELECT * FROM (SELECT users.id as id, users.name as name, domain_id, domains.name as domain_name, display_name, context_id, ROW_NUMBER() OVER (order by users.name) as rnum FROM users LEFT JOIN domains ON users.domain_id=domains.id WHERE role='U' AND (users.name LIKE N'%"&Replace(myuname, "'", "''")&"%' OR users.display_name LIKE N'%"&Replace(myuname, "'", "''")&"%')) users WHERE rnum Between "&(offset+1)&" and "&(offset+limit))
		end if
	else
		if(gid<>0) then
			Set RS = Conn.Execute("SELECT * FROM (SELECT *, ROW_NUMBER() OVER (order by name) as rnum FROM (SELECT DISTINCT users.id as id, users.name as name, domains.name as domain_name, display_name, domain_id, context_id FROM users LEFT JOIN domains ON users.domain_id=domains.id LEFT JOIN policy_user ON users.id=policy_user.user_id LEFT JOIN users_groups ON users.id=users_groups.user_id LEFT JOIN OU_User ON users.id=OU_User.Id_user WHERE role='U' AND ("&policy_ids&" "&group_ids&" "&ou_ids&")) users) users WHERE rnum Between "&(offset+1)&" and "&(offset+limit)&" ")	
		else
			Set RS = Conn.Execute("SELECT * FROM (SELECT users.id as id, users.name as name, domain_id, domains.name as domain_name, display_name, context_id, ROW_NUMBER() OVER (order by users.name) as rnum FROM users LEFT JOIN domains ON users.domain_id=domains.id WHERE role='U') users WHERE rnum Between "&(offset+1)&" and "&(offset+limit)&" ")	
		end if
	end if
	num=0
	Do While Not RS.EOF
		strUserName = RS("name")
		strUserID = RS("id")
'		domain_id = RS("domain_id")
		strDomainName=""
		strDisplayName=""
	if AD=3 and Len(RS("domain_name")) > 0 then
'		Set RSDomain = Conn.Execute("SELECT name FROM domains WHERE id="&domain_id)
'		if(Not RSDomain.EOF) then
		strDomainName = RS("domain_name") 
		strDisplayName = RS("display_name") 
	 if (EDIR = 1) then
	 	if not IsNull(RS("context_id")) then
			context_id = RS("context_id")
			Set RS3 = Conn.Execute("SELECT name FROM edir_context WHERE id=" & context_id)
			if(Not RS3.EOF) then
				strContext=RS3("name")
				RS3.close
			else
				strContext="&nbsp;"
			end if
			strContext=replace (strContext,",ou=",".")
			strContext=replace (strContext,",o=",".")
			strContext=replace (strContext,",dc=",".")
			strContext=replace (strContext,",c=",".")

			strContext=replace (strContext,"ou=","")
			strContext=replace (strContext,"o=","")
			strContext=replace (strContext,"dc=","")
			strContext=replace (strContext,"c=","")
			strDomainName = strDomainName & " / " & strContext
		end if
	 end if

'		end if
		
	end if
%>
		<tr><td><input type="checkbox" id="users_<%=strUserID%>" name="users" value="<%=strUserID%>"></td><td><input type="hidden" name="users_name" value="<%=strUserName%>"><label for="users_<%=strUserID%>"><%=strUserName%></label></td>
		<%if(AD=3) then %>
		<td><%=strDisplayName%></td><td><%=strDomainName%></td>
		<%end if%>
		</tr>
<%
	RS.MoveNext
	Loop
	RS.Close
	response.write "</table>"
	Response.Write make_pages_ajax(offset, cnt, limit, page, name) 

else

Response.Write "<center><b>"&LNG_THERE_ARE_NO_USERS &".</b><br><br></center>"

end if
else

Response.Write "<center><b><"& LNG_THERE_ARE_NO_USERS &".</b><br><br></center>"

end if
%>

<!-- #include file="db_conn_close.asp" -->