<%@ Language="JScript" %>
<%
if (Request("use_local") == "0") {
	Response.Redirect("http://www.alert-software.com/store/?inframe=1");
	Response.End();
} else if (Request("succeed") == "1") {
	Response.Write("1");
	Response.End();
} else {
	items = [
	/*[
		"Name"
		"image URL", "image alt/title"
		"Description",
		"Price"
	]*/
	[
		"Additional licenses",
		"images/buy_addons/user3.png", "",
		"Get more licenses to send alerts to larger number of users.",
		"$1.00 per license",
		100
	],
	[
		"Support block Level 2 (20 hours)",
		"images/buy_addons/lifesaver.png", "",
		"<b>Troubleshooting DeskAlerts related problems</b> via remote desktop connections (Web-sessions). Our professionals will access the user's desktop to check the settings, install any additional software if required, diagnose and resolve the problems as quickly as possible.<br /><b>Research and investigation of advanced features to be developed</b> and implemented, any customization of the existing feature on top of the standard platform, in-depth analysis of the specific Domains structure (Active Directories) or corporate network, our best recommendations for an effective performance of DeskAlerts according to your specifications.",
		"$2,000.00 per block",
		1
	],
	[
		"DeskAlerts Skin service",
		"images/buy_addons/color_platte.png", "",
		"Brand the DeskAlerts client with corporate logo and colors.",
		"$650.00"
	],
	[
		"DeskAlerts Skin PRO service",
		"images/buy_addons/color_platte_pro.png", "",
		"Advanced Skin Service includes styling of multiple skin templates (up to 20), styling of scrolling tickers and branding the Control Panel interface itself",
		"$3500.00"
	],
	[
		"Microsoft Active Directory module",
		"images/buy_addons/organization.png", "",
		"If your company uses Active Directory, the Active Directory module allows you to synchronize DeskAlerts with your Active Directory easily, significantly speeding up deployment and increasing usability. You may sync users, groups, OUs, and computers.",
		"$1,500.00"
	],
	[
		"Novell eDirectory module",
		"images/buy_addons/domains.png", "",
		"If your company uses eDirectory, the eDirectory module allows you to synchronize DeskAlerts with your eDirectory easily, significantly speeding up deployment and increasing usability. The eDirectory add-on supports synchronization across unlimited domains. Auto synchronization in predefined date and time is available.",
		"$1,500.00"
	],
	[
		"Computer Name module",
		"images/buy_addons/user2.png", "",
		"For those who do not use Active Directory or eDirectory but require silent installation, we are pleased to offer this module. Computer name module is auto-registered in DeskAlerts system by COMPUTER NAME. <em>DeskAlerts Client can be registered by IP address</em>",
		"$1200.00"
	],
	[
		"Extended statistics/reports module",
		"images/buy_addons/statistics.png", "",
		"With this module, the DeskAlerts Console provides enhanced statistics/reports by dates. You can get results by users, alerts, or surveys, and check to see who hasn't received alerts.",
		"$1,200.00"
	],
	[
		"Screensaver module",
		"images/buy_addons/screensaver.png", "",
		"Corporate screensaver is one of the less intrusive but powerful ways to communicate with desktops of your employees. DeskAlerts screensaver work same way as ordinary PC screensaver and allows for targeted content delivery.",
		"$990.00"
	],
	[
		"Corporate wallpaper module",
		"images/buy_addons/wallpaper.png", "",
		"This module allows you to use DeskAlerts to publish corporate desktop wallpapers. Corporate wallpaper is less intrusive but effective corporate communications channel. Communicate regarding upcoming changes or news, establish internal brand or publish product announcements right to the employee desktop wallpaper. Publish your messages as corporate desktop wallpapers, use scheduling and sequence your wallpapers if you want 2 or more messages to rotate. All advanced targeting options including delivery to users/groups or any combination of those is supported.",
		"$990.00"
	],
	[
		"RSS newsfeeds module",
		"images/buy_addons/rss.png", "",
		"RSS newsfeeds module allows you to feed news, headlines, videos from any RSS enabled source for instance SharePoint, Exchange, websites, blogs, etc. Make your organization follow your corporate news or any other internal or external entries.",
		"$960.00"
	],
	[
		"Survey module",
		"images/buy_addons/survey.png", "",
		"Survey module allows creating, sending, and scheduling an instant multiquestion survey. Three types of questions are available: multiple answers, ability to input answer, and conditional questions. DeskAlerts surveys can be created and re-sent within seconds and deliver results within minutes. It's the fastest way to get feedback from your employees.",
		"$800.00"
	],
	[
		"Encryption module",
		"images/buy_addons/key.png", "",
		"The encryption module encrypts all messages sent between the DeskAlerts server and client with 128 bit encoding. This module is for organizations that use public networks to connect the client to the server, as well as for those with heightened security concerns.",
		"$720.00"
	],
	[
		"E-mail module",
		"images/buy_addons/email.png", "",
		"Use DeskAlerts to send your regular desktop alerts as emails. When you create desktop alert just make sure it will be sent as email and your staff will find your alert in their mailbox along with popups on their PC desktops.",
		"$800.00"
	],
	[
		"SMS module",
		"images/buy_addons/mobile.png", "",
		"DeskAlerts SMS module allows you to send messages to mobile devices, enabling you to reach users even when they are out of the office. This feature is especially useful for emergency alerts. DeskAlerts uses ClickAtell.com SMS Gateway by default. It is possible to use any other SMS Gateways, just ask sales.",
		"$600.00"
	],
	[
		"Desktop Scrolling News Ticker module",
		"images/buy_addons/ticker.png", "",
		"Use a scrolling desktop ticker to inform about updates and news or information in a less disruptive manner. This module is especially useful when you need to update people on important information in a timely manner.",
		"$700.00"
	],
	[
		"Desktop Scrolling News Ticker PRO module",
		"images/buy_addons/ticker_pro.png", "",
		"Advanced version of Desktop Scrolling News Ticker module allows you to set ticker windows position on user's screen and includes support of languages with right-to-left text direction.",
		"$2500.00"
	],
	[
		"Locked screen module",
		"images/buy_addons/policies.png", "",
		"Allows communications to be delivered to the PC's when they are locked. As a result of corporate policies that log-out user after certain period of inactivity the major part of PC’s in organization remain locked. This add-on allows you to communicate on top of any screensavers or locked PC’s. Lock screen alert is specifically useful when we need to deliver critical information such as important notifications or emergency alerts.",
		"$960.00"
	],
	[
		"Full screen module",
		"images/buy_addons/fullscreen.png", "",
		"Allows to send popup notifications to full screen.",
		"$500.00"
	],
	[
		"Social Modules",
		"images/buy_addons/blog.png", "",
		"Allows to send notifications to a corporate WordPress-powered blog, corporate Twitter account or to LinkedIn as shares.",
		"$600.00"
	],
	[
		"One Button Alerting Module",
		"images/buy_addons/instant_alerts.png", "",
		"Enables limited group of editors to create alert notifications that can be later sent in one click, with no recipients selection or other activities required. Perfectly suits for emergency notifications, for sending messages from mobile devices or for sending notifications by person not familiar with DeskAlerts interface.",
		"$550.00"
	]
];
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>DeskAlerts</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link href="css/style9.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
		.buy_tile { float:left; margin:10px; padding:10px; width: 500px; font: normal normal 400 12px/18px Verdana; }
		.name { font-weight: bold; color: #8C0000; }
		.image {}
		.desc { color: #3B3F42; }
		.price { font-weight: bold; color: #8C0000; text-align: center; }
		.button
		{
			display: inline-block;
			*display: inline ! important;
			zoom: 1 ! important;
			font-weight: bold; color: #8C0000; outline: #8C0000 0px; padding: 4px 10px 4px 10px; border: 1px solid #B3B3B7; text-decoration: none; background: #F0F0F1;
			white-space: nowrap;
		}
		.button:hover { background: #DADADA; }
		.buttons { white-space: nowrap; width: 100px; }
		.buy {}
		.order {}
		.remove { background: URL('images/buy_addons/checkmark.png') center center no-repeat; }
		.remove:hover { background: URL('images/buy_addons/remove.png') center center no-repeat; }
		.count { width: 50px; text-align: right; }
		.count_control { padding: 0px 0px 4px 0px; }
		.buy_control {}
		.order_control { display: none; }
		.show_order .buy_control { display: none; }
		.show_order .order_control { display: block; }
		.cart { margin: 4px 35px 4px 35px; padding: 10px; border: 1px solid #8C0000; }
		.cart_name { font-weight: bold; color: #8C0000; }
		.cart_list {}
		.cart_list li { padding: 2px 0px 2px 0px; }
		.buy_from_cart { margin-top:10px; padding: 4px 20px 4px 20px; }
	</style>
	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
	
	<script language="javascript" type="text/javascript">
<%
	if(Request("use_local").Count === 0 || Request("use_local") == "") {
%>
		$(function(){
			var checkConnectionFrame = $("<iframe id='check_connection_frame' style='display:none' name='check_connection_frame'/>").appendTo(document.body);
			var checkConnectionForm = $("<form method='post' id='check_connection_form' style='display:none' action='http://www.alert-software.com/control_panel/check_connection/' target='check_connection_frame'/>").appendTo(document.body);
			var checkConnectionUrlInput = $("<input name='local_url' type='hidden'/>").val(window.location.href.split("#")[0]).appendTo(checkConnectionForm);
			var use_local = "1";
			checkConnectionFrame.load(function() {
				/*if(window.parent.hideLoader) window.parent.hideLoader();
				try {
					if(window.location.hash == "#succeed") {
						use_local = "0";
					}
				}
				catch(e) {
				}*/
				$(window).on('message', function(e){use_local=0;});
				setTimeout(function(){window.location.href = window.location.href.split("?")[0] + "?use_local=" + use_local;
				if(window.parent.hideLoader) window.parent.hideLoader();
				}, 1000);
			});
			if(window.parent.showLoader) window.parent.showLoader();
			checkConnectionForm.submit();
		});
<%
	} else {
%>
		var email = "sales@deskalerts.com";
		var subject = "Add-ons ordering";
		var before = "";
		var after = "\r\nHere you can leave your comments:\r\n\r\n\r\n";
		var addons = "";
		$(function() {
			var updateCart = function() {
				addons = "";
				var $cart = $("#cart");
				var $cart_list = $("#cart_list").empty();
				var length = $(".show_order").each(function() {
					var name = $(this).find(".name").text();
					var price = $(this).find(".price").text();
					var count = $(this).find(".count").val();
					var addon = (count ? count + "x " : "") + name/* + (price ? " (" + price + ")" : "")*/;
					$cart_list.append($("<li>").append(addon));
					addons += addon + "\r\n";
				}).length;
				if(length > 0)
				{
					$cart.show();
				}
				else
				{
					$cart.hide();
				}
			};
			$(".buy").click(function(e) {
				$(this).closest(".buy_tile").addClass("show_order");
				updateCart();
				return false;
			});
			$(".remove").click(function(e) {
				$(this).closest(".buy_tile").removeClass("show_order");
				updateCart();
				return false;
			});
			$(".order").click(function(e) {
			});
			$(".count").keyup(updateCart).change(updateCart);
			$("#buy_from_cart").click(function(e) {
				location.href = "mailto:" + email + "?subject=" + encodeURIComponent(subject) + "&body=" + encodeURIComponent(before + addons + after);
			});
		});
<%
	}
%>
	</script>
</head>
<body style="margin:0px" class="body">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
	<tr>
	<td>
	<table width="100%" cellspacing="0" cellpadding="0" class="main_table">
		<tr>
			<td width="100%" height="31px" class="main_table_title"><a href="#" class="header_title">Buy add-ons</a></td>
		</tr>
		<tr>
		<td class="main_table_body" height="100%">
			<div style="margin:10px;">
<%
	if(Request("use_local") == "1")
	{
%>
<div id="cart" class="cart" style="display:none">
	<div class="cart_name">Your Order:</div>
	<ul id="cart_list" class="cart_list" style="list-style:none; margin:0px; padding:0px"></ul>
	<a id="buy_from_cart" href="#" class="buy_from_cart button">Send your order</a>
</div>
<div>
	<ul style="list-style:none; margin:0px; padding:0px">
<%
		for(var i in items)
		{
			var item = items[i];
			var name = item[0];
			var image = item[1];
			var title = item[2];
			var desc = item[3];
			var price = item[4];
			var count = item[5];
%>
		<li class="buy_tile">
			<table border="0">
			<tr>
				<td align="center">
					<img class="image" src="<%=image %>" alt="<%=title %>" title="<%=title %>" border="0" />
				</td>
				<td rowspan="3" valign="middle">
					<div class="name"><%=name %></div>
					<div class="desc"><%=desc %></div>
				</td>
			</tr>
			<tr style="display:none">
				<td>
					<div class="price"><%=price %></div>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" class="buttons">
					<% if(count) { %>
						<div class="count_control">
							<input type="text" class="count" value="<%=count %>" />
						</div>
					<% } %>
					<div class="buy_control">
						<a href="#" class="buy button">Order</a>
					</div>
					<div class="order_control">
						<a href="#" class="remove button" title="Remove from order">&nbsp;</a>
						<a href="#" class="order button">Added</a>
					</div>
				</td>
			</tr>
			</table>
		</li>
<%
		}
%>
	</ul>
</div>
<%
	}
%>
			</div>
		</td>
		</tr>
	</table>
	</td>
	</tr>
</table>
</body>
</html>
<%
}
%>
