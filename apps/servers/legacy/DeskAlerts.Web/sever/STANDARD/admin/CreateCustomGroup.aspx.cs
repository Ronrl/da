﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace DeskAlertsDotNet.Pages
{
    public partial class CreateCustomGroup : DeskAlertsBasePage
    {
        //use curUser to access to current user of DA control panel      
        //use dbMgr to access to database

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            var users = Request.GetParam("users", "").Split(',').Distinct().Where(id => id.Length > 0).ToList();
            var groups = Request.GetParam("groups", "").Split(',').Distinct().Where(id => id.Length > 0).ToList();
            var ous = Request.GetParam("ous", "").Split(',').Distinct().Where(id => id.Length > 0).ToList();
            var computers = Request.GetParam("computers", "").Split(',').Distinct().Where(id => id.Length > 0).ToList();

            var groupName = Request.GetParam("groupName", "");

            if (string.IsNullOrEmpty(groupName))
                return;

            Dictionary<string, object> groupParameters = new Dictionary<string, object>()
            {
                ["name"] = groupName,
                ["custom_group"] = 1,
                ["domain_id"] = 1
            };

            int groupId = dbMgr.Insert("groups", groupParameters, "id");

            var userQueries = new List<string>();
            var groupQueries = new List<string>();
            var computerQuerries = new List<string>();
            var ouQueries = new List<string>();

            foreach (var userId in users)
            {
                userQueries.Add($"INSERT INTO users_groups (group_id, user_id, was_checked) VALUES ({groupId}, {userId}, 1)");
            }

            foreach (var childGroupId in groupQueries)
            {
                groupQueries.Add($"INSERT INTO groups_groups (container_group_id, group_id, was_checked) VALUES ({groupId}, {childGroupId}, 1)");
            }

            foreach (var computerId in computers)
            {
                computerQuerries.Add($"INSERT INTO computers_groups (group_id, comp_id, was_checked) VALUES ({groupId}, {computerId}, 1)");
            }

            foreach (var ouId in ouQueries)
            {
                ouQueries.Add($"INSERT INTO ou_groups (group_id, ou_id, was_checked) VALUES ({groupId}, {ouId}, 1)");
            }

            dbMgr.ExecuteQuery(string.Join(Environment.NewLine, userQueries));
            dbMgr.ExecuteQuery(string.Join(Environment.NewLine, groupQueries));
            dbMgr.ExecuteQuery(string.Join(Environment.NewLine, computerQuerries));
            dbMgr.ExecuteQuery(string.Join(Environment.NewLine, ouQueries));
            //Add your main code here
        }
    }
}