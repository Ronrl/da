﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Auth.aspx.cs" Inherits="DeskAlertsDotNet.Pages.Auth" %>
<!DOCTYPE html>
<html style="height:100%"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<head>
	<title>DeskAlerts</title>
	<meta http-equiv="X-UA-Compatible" content="IE=Edge"> 
    <meta name="viewport" content="width=device-width">
	<link href="css/style9.css" rel="stylesheet" type="text/css">
	<link href="images/favicon.ico" rel="shortcut icon" type="image/x-icon">
	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/jquery.hotkeys.js"></script>
    <style>
		/*--- login page responsive ---*/

        @media screen and (max-width: 768px) {
            table td{
                display: block;
            }
        }
    </style>
	<script type="text/javascript">
	    if (window.top !== window.self) {
	        window.top.location = window.self.location;
	    }
	</script>
	<script>
	    $(document).ready(function () {
	        $.post($('#sharepoint').attr('action'), $('#sharepoint').serialize())
            .done(function (data, textStatus, jqXHR) {
                $('#results').html(data);
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                $('#results').html("<h3>Request has been failed. Try again</h3>");
            });
	    });

	    $(document)
	        .bind("keydown",
	            "return",
	            function() {

	                __doPostBack('loginButton', '');

	            });

	    $("#password")
	        .bind("keydown",
	            "return",
	            function () {
	                $("#password").focus();
	                e.preventDefault();
	                __doPostBack('loginButton', '');

	            });

	    $("#login")
	        .bind("keydown",
	            "return",
	            function () {
	                $("#login").focus();
	                e.preventDefault();
	                __doPostBack('loginButton', '');

	            });
</script>
</head>

<body class="login_body" style="height: 100%; margin: 0px;" onload="var login = document.getElementById(&#39;login&#39;); if(login) login.focus();">

<div style="display:table; position:absolute;height:100%;width:100%;">
<div style="display:table-cell; vertical-align:middle;">
<div style="margin-left:auto;margin-right:auto;">

<link href="css/style9.css" rel="stylesheet" type="text/css">
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css">

<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script language="javascript" type="text/javascript">

    $(document).ready(function () {
        $("#nojs").hide();
        $("#loginButton").button({
            icons: {
                primary: "ui-icon-key"
            }
        });

        $("#demoLoginButton").button({
            icons: {
                primary: "ui-icon-key"
            }
        });

    });

    function demo_login() {
        $("#login").val("demo");
        $("#password").val("demo");
        document.forms[0].submit();
    }

</script>
<script language="javascript">
    function checkIE6SP2() {
        var version = 999; // we assume a sane browser
        if (navigator.appVersion.indexOf("MSIE") != -1)
            version = parseFloat(navigator.appVersion.split("MSIE")[1]);
        if (version == 6) {
            if (navigator.appMinorVersion.toLowerCase().indexOf('sp') == -1) {
                return false;
            }
        }
        return true;
    }
</script>
    <div style ="color: #066c19; text-align: center;" id ="nojs"><p>JavaScript is disabled on your browser. Please contact with technical specialist of your company.</p></div>
<table style="position:relative" height="100%" border="0" align="center" cellspacing="0" cellpadding="0">
<tbody><tr>
	<td>
		<div style="position:relative" align="center">
			<!--<img src="images/langs/EN/top_logo.gif" alt="DeskAlerts Control Panel" border="0">-->
			<img src="images/top_logo.gif" alt="DeskAlerts Control Panel" border="0">
		</div>
		<br>
		<div id="js_error" style="border: 1px solid rgb(0, 0, 0); color: red; padding: 10px; text-align: center; line-height: 20px; display: none; background-color: rgb(255, 255, 255);">
			DeskAlerts Control Panel requires JavaScript to function properly.<br>Please turn on JavaScript in your browser.<br>Contact your technical assistance if required.
		</div>
		<div id="ie6_error" style="border:1px solid #000000; background-color:#ffffff; color: red; padding: 10px; text-align:center; line-height: 20px; display: none">
			DeskAlerts Control Panel requires Service Pack 2 or higher for Internet Explorer 6 to function properly.<br>Please install Service Pack 2 or higher to your computer.<br>Contact your technical assistance if required.
		</div>		
		<table style="position:relative" id="login_form" cellspacing="0" cellpadding="0" align="center" class="main_table login_table">
		<tbody><tr>		
			<td width="100%" height="31" class="theme-colored-back"><div class="header_title header_white">Login</div></td>
		</tr>
		<tr>
			<td bgcolor="#ffffff" style="position:relative">
				<form id="form1" runat="server" name="web" autocomplete="off">
					<div style="margin: 20px 10px 10px 10px;text-align:center;position:relative">
					
						<!--<img src="images/Infosign_logo_white.gif" border="0">-->
						<table cellpadding="0" cellspacing="4" border="0">
							<tbody><tr>
								<td>
									<label for="login">Login:</label>
								</td>
								<td>
								    <input runat="server" id="login" name="login" type="text" style="width: 149px;"  onkeydown="try{if(event.keyCode==13) __doPostBack('loginButton', '') }catch(e){}"/>
								</td>
							</tr>
							<tr>
								<td>
									<label for="password">Password:</label>
								</td>
								<td>
								    <input runat="server" id="password" name="password" type="password" autocomplete="off" style="width: 149px;"  onkeydown="try{if(event.keyCode==13) __doPostBack('loginButton', '') }catch(e){}"/>
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<span runat="server" id="wrongLogin" style="color:red;font-size:11px">Wrong login/password. Please try again</span>
                                    <span runat="server" id="fullServer" style="color:red;font-size:11px">Serves is full. Please wait some time.</span>
								    <span runat="server" id="PublisherAccountDisabledByAdministratorErrorText" style="color:red;font-size:11px">Publisher account disabled by Administrator.</span>
								</td>
							</tr>
							<tr>
								<td colspan="2" style="position:relative;text-align:right">
                                    <asp:linkbutton runat="server" class="green" id="loginButton" style="margin-right:0px" role="button" aria-disabled="false" Text="<span class='ui-icon-key'>{Login}</span>"/>
									<%--<asp:linkbutton runat="server" class="green" id="demoLoginButton" style="float:left"  role="button" aria-disabled="false" Text="<span class='ui-icon-key'>Demo</span>"/>--%>
								</td>
							</tr>
						</tbody></table>
					
					</div>
				</form>
			</td>
		</tr>
		</tbody></table>
        <br/>
        <center><b style="font-size: 110%">version&nbsp;<asp:Label id="versionLabel" runat="server"></asp:Label></b></center>
		<script language="javascript">

		    document.getElementById("js_error").style.display = "none";
		    if (!checkIE6SP2()) {
		        document.getElementById("ie6_error").style.display = "";
		    }
		    else {
		        document.getElementById("login_form").style.display = "";
		    }
		</script>
		
	</td>
	<td runat="server" id="demoText">
		<div style="padding:62px 10px 10px 10px; width:560px; color:#777; font-family:Tahoma">
			<p style="margin-bottom: 10px;font-size: 25px">How to use DeskAlerts public demo</p>
			<iframe width="560" height="315" src="https://www.youtube.com/embed/HqFRF6520Rw?rel=0" frameborder="0" allowfullscreen=""></iframe>
<p>
				If you are logging in for the first time and don't have your own account yet, click the "Demo" button to log in with the shared demo account.
			</p>
			<p>
				After you log in with the shared account, you can create a private one from Publishers menu. Having your own account will enable you to customize profile settings to personalize your experience.
			</p>
		</div>

	</td>	
</tr>

</tbody></table>

<div runat="server" id="copyrightDiv" style="position:fixed;bottom:0px;color:#777;width:330px;left:50%; margin-left: -115px;">
    Copyright © DeskAlerts, 2006-<%=DateTime.Now.Year %>. All Rights Reserved.
</div>

</div>
</div>
</div>




</body></html>