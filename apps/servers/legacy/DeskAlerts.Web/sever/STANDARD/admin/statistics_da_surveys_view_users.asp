<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%
check_session()
%>
<!-- #include file="Common.asp" -->
<!-- #include file="header.asp" -->
<!-- #include file="footer.asp" -->
<!-- #include file="libs/FusionCharts.asp" -->
<%
'On Error Resume Next
' index CustomIncludes End
'-------------------------------

'===============================
' Save Page and File Name available into variables
'-------------------------------
Dim sFileName
Dim sTemplateFileName
Dim sPaginateFileName
Dim intSessionUserId

sFileName = "statistics_da_surveys_view_users.asp"
sTemplateFileName = "statistics_da_surveys_view_users.html"
sPaginateFileName = "paginate.html"
'===============================
  

intSessionUserId = Session("uid")



if (intSessionUserId <> "") then


' index Show begin

'===============================
' Display page
'-------------------------------
' Load HTML template for this page
'-------------------------------
LoadTemplate alerts_dir & "\" & templateFolder & "\" & sTemplateFileName, "main"
SetVar "DListUsersStat", ""
SetVar "LNG_ADD_USERS_TO_GROUP", LNG_ADD_USERS_TO_GROUP
SetVar "LNG_CREATE", LNG_CREATE
SetVar "LNG_GROUP_NAME", LNG_GROUP_NAME
'-------------------------------
' Load HTML template of Header and Footer
'-------------------------------
LoadTemplate sHeaderFileName, "Header"
LoadTemplate sFooterFileName, "Footer"
LoadTemplate sSearchDetailsFormFileName, "SearchForm"
'-------------------------------


pageType=request("type")
intSurveyId=0
intAnswerId=0

if(request("id")<>"") then
	intSurveyId=Clng(request("id"))
end if
if(request("answer_id")<>"") then
	intAnswerId=Clng(request("answer_id"))
end if

searchSQL = ""
search = request("search")
if search <> "" then searchSQL = " AND (users.name LIKE N'%"&replace(search, "'", "''")&"%' OR users.display_name LIKE N'%"&replace(search, "'", "''")&"%')"

SetVar "searchTerms", HtmlEncode(search)
SetVar "page", sFileName&"?"&replace(Request.QueryString, "search=", "search_old=")
SetVar "searchName", "users"

SetVar "LNG_USERNAME", LNG_USERNAME
SetVar "LNG_DATE", LNG_DATE
SetVar "default_lng", default_lng

if(pageType="rec") then
	PageName(LNG_RECEIVED)
elseif(pageType="drec") then
	PageName(LNG_NOT_RECEIVED)
elseif(pageType="vot") then
	PageName(LNG_VOTED)
elseif(pageType="dvot") then
	PageName(LNG_NOT_VOTED)
elseif(pageType="ans") then
	PageName(LNG_ANSWERED)
end if

   if(Request("offset") <> "") then 
	offset = clng(Request("offset"))
   else 
	offset = 0
   end if	
  
   if(Request("limit") <> "") then 
	limit = clng(Request("limit"))
   else 
	limit=50
   end if


'doesnt received
if(intSurveyId<>0 or pageType="ans") then
	Set surveyRS = Conn.Execute("SELECT create_date, type, sender_id FROM surveys_main WHERE id="& intSurveyId)
	if(Not surveyRS.EOF) then

	mytype=surveyRS("type")
	create_date=surveyRS("create_date")
	intAlertId = surveyRS("sender_id")
	
end if

if(pageType="drec") then
	if(mytype="B") then
		Set RS = Conn.Execute("SELECT COUNT(DISTINCT users.id) as mycnt FROM users WHERE role='U' AND ( client_version != 'web client' OR client_version is NULL ) AND (reg_date <= '" & create_date & "' OR reg_date IS NULL) AND id NOT IN (SELECT user_id FROM alerts_received WHERE alert_id="& intAlertId &")" & searchSQL)
	end if
	if(mytype="R") then
		Set RS1 = Conn.Execute("SELECT COUNT(DISTINCT users.id) as mycnt FROM users INNER JOIN alerts_sent_stat ON users.id=alerts_sent_stat.user_id WHERE role='U' AND users.id NOT IN (SELECT user_id FROM alerts_received WHERE alert_id="& intAlertId &") AND alerts_sent_stat.alert_id="& intAlertId & searchSQL)
		SQL = "SELECT 0 as mycnt"
		Set RS = Conn.Execute(SQL)				
	end if
	if(mytype="R") then
		Set RS2 = Conn.Execute("SELECT COUNT(DISTINCT users.id) as mycnt FROM users INNER JOIN users_groups ON users.id=users_groups.user_id INNER JOIN alerts_sent_group ON alerts_sent_group.group_id=users_groups.group_id WHERE role='U' AND users.id NOT IN (SELECT user_id FROM alerts_received WHERE alert_id="&intAlertId&") AND users.id NOT IN (SELECT user_id FROM alerts_sent_stat WHERE alert_id="&intAlertId&") AND alerts_sent_group.alert_id="&intAlertId & searchSQL)
		SQL = "SELECT 0 as mycnt"
		Set RS = Conn.Execute(SQL)
	end if
elseif(pageType="rec") then
	Set RS = Conn.Execute("SELECT COUNT(DISTINCT users.id) as mycnt FROM users INNER JOIN alerts_received ON users.id=alerts_received.user_id WHERE role='U' AND alerts_received.alert_id="& intAlertId & searchSQL)
elseif(pageType="vot") then
	Set RS = Conn.Execute("SELECT COUNT(DISTINCT users.id) as mycnt FROM users INNER JOIN alerts_received ON users.id=alerts_received.user_id WHERE role='U' AND vote=1 AND alerts_received.alert_id="& intAlertId & searchSQL)
elseif(pageType="dvot") then
	Set RS = Conn.Execute("SELECT COUNT(DISTINCT users.id) as mycnt FROM users INNER JOIN alerts_received ON users.id=alerts_received.user_id WHERE role='U' AND vote<>1 AND alerts_received.alert_id="& intAlertId & searchSQL)
elseif(pageType="ans") then
    if (search <> "") then 
        Set RS = Conn.Execute("SELECT COUNT(DISTINCT user_id) as mycnt FROM surveys_answers_stat INNER JOIN users ON (users.name LIKE N'%"&replace(search, "'", "''")&"%' OR users.display_name LIKE N'%"&replace(search, "'", "''")&"%') AND answer_id="& intAnswerId) 
    else
        Set RS = Conn.Execute("SELECT COUNT(DISTINCT user_id) as mycnt FROM surveys_answers_stat WHERE answer_id="& intAnswerId)
    end if
end if
cnt = 0
if(pageType="drec") then
	if(mytype="R") then
		if(Not RS1.EOF) then
			cnt=cnt+RS1("mycnt")
		end if
		RS1.Close	
	end if
	if(mytype="R") then
		if(Not RS2.EOF) then
			cnt=cnt+RS2("mycnt")
		end if
		RS2.Close		
	end if
end if

	if(Not RS.EOF) then
		cnt=cnt+RS("mycnt")
	end if
	RS.Close
	j=cnt/limit

end if


if(cnt>0) then

	page=sFileName & "?id="&intSurveyId&"&answer_id="&intAnswerId&"&type="&pageType&"&search="&Server.URLEncode(search)&"&"
	name=LNG_USERS
	SetVar "paging", make_pages(offset, cnt, limit, page, name, sortby)
end if

'show main table

	num=0
if(pageType="drec") then

	if(mytype="B") then
        
		Set RS = Conn.Execute("SELECT DISTINCT users.id as id, users.name as name, users.domain_id as domain_id FROM users WHERE role='U' AND ( client_version != 'web client' OR client_version is NULL ) AND  reg_date <= '" & create_date & "'  AND id NOT IN (SELECT user_id FROM alerts_received WHERE alert_id="& intAlertId &")" & searchSQL)
	end if

	if(mytype="R") then
		Set RS1 = Conn.Execute("SELECT DISTINCT users.id as id, users.name as name, users.domain_id as domain_id FROM users INNER JOIN alerts_sent_stat ON users.id=alerts_sent_stat.user_id WHERE role='U' AND users.id NOT IN (SELECT user_id FROM alerts_received WHERE alert_id="& intAlertId &") AND alerts_sent_stat.alert_id="& intalertId & searchSQL)
		SQL = "SELECT DISTINCT users.id as id, users.name as name, users.domain_id as domain_id FROM users WHERE id=0"
		Set RS = Conn.Execute(SQL)
	end if

	if(mytype="R") then
		Set RS2 = Conn.Execute("SELECT DISTINCT users.id as id, users.name as name, users.domain_id as domain_id FROM users INNER JOIN users_groups ON users.id=users_groups.user_id INNER JOIN alerts_sent_group ON alerts_sent_group.group_id=users_groups.group_id WHERE role='U' AND users.id NOT IN (SELECT user_id FROM alerts_received WHERE alert_id="&intalertId&") AND users.id NOT IN (SELECT user_id FROM alerts_sent_stat WHERE alert_id="&intalertId&") AND alerts_sent_group.alert_id="&intalertId & searchSQL)
		SQL = "SELECT DISTINCT users.id as id, users.name as name, users.domain_id as domain_id FROM users WHERE id=0"
		Set RS = Conn.Execute(SQL)
	end if
end if

if(pageType="rec") then
		Set RS = Conn.Execute("SELECT DISTINCT users.id as id, users.name as name, users.domain_id as domain_id, alerts_received.date as [date] FROM users INNER JOIN alerts_received ON users.id=alerts_received.user_id WHERE role='U' AND alerts_received.alert_id="& intalertId & searchSQL)
end if

if(pageType="vot") then
   
		Set RS = Conn.Execute("SELECT DISTINCT users.id as id, users.name as name, users.domain_id as domain_id, alerts_received.date as [date] FROM users INNER JOIN alerts_received ON users.id=alerts_received.user_id WHERE role='U' AND vote=1 AND alerts_received.alert_id="& intSurveyId & searchSQL)
end if

if(pageType="dvot") then
   Set RS = Conn.Execute("SELECT DISTINCT users.id as id, users.name as name, users.domain_id as domain_id FROM users INNER JOIN alerts_received ON users.id=alerts_received.user_id WHERE role='U' AND alerts_received.vote IS NULL AND alerts_received.alert_id="& intSurveyId & searchSQL)
end if

if(pageType="ans") then
Set RS = Conn.Execute("SELECT DISTINCT users.id as id, users.name as name, users.domain_id as domain_id, surveys_answers_stat.date as [date] FROM users INNER JOIN surveys_answers_stat ON users.id=surveys_answers_stat.user_id WHERE role='U' AND surveys_answers_stat.answer_id="& intAnswerId & searchSQL)
end if

if(pageType="drec") then
	if(mytype="R") then
Do While Not RS1.EOF
	num=num+1
	if(num > offset AND offset+limit >= num) then
		if(IsNull(RS1("name"))) then
			name="Unregistered"
		else
			strUserName = RS1("name")
		end if
		if (AD = 3) then 	
			Set RS3 = Conn.Execute("SELECT name FROM domains WHERE id=" & RS1("domain_id"))
			if(Not RS3.EOF) then
				domain=RS3("name")
			else
				domain="&nbsp;"
			end if
			RS3.close
		end if
		if(pageType<>"drec" AND pageType<>"dvot") then
			strDate=RS1("date")
			SetVar "strDate", strDate
		end if
		SetVar "strUserName", strUserName
        SetVar "strUserId", RS1("id")
		Parse "DListUsersStat", True
	end if
	RS1.MoveNext
Loop
	end if
	if(mytype="R") then
Do While Not RS2.EOF
	num=num+1
	if(num > offset AND offset+limit >= num) then
		if(IsNull(RS2("name"))) then
			name="Unregistered"
		else
			strUserName = RS2("name")
		end if
		if (AD = 3) then 	
             
			Set RS3 = Conn.Execute("SELECT name FROM domains WHERE id=" & RS2("domain_id"))
            
			if(Not RS3.EOF) then
				domain=RS3("name")
			else
				domain="&nbsp;"
			end if
			RS3.close
		end if
		if(pageType<>"drec" AND pageType<>"dvot") then
			strDate=RS2("date")
			SetVar "strDate", strDate
		end if
		SetVar "strUserName", strUserName
		SetVar "strUserId", RS2("id")
		Parse "DListUsersStat", True
	end if
	RS2.MoveNext
Loop
	end if
end if

Do While Not RS.EOF
	num=num+1
	if(num > offset AND offset+limit >= num) then
		if(IsNull(RS("name"))) then
			name="Unregistered"
		else
			strUserName = RS("name")
		end if
		if (AD = 3) then 	

             domain="&nbsp;"

            if(RS("domain_id")) then
			    Set RS3 = Conn.Execute("SELECT name FROM domains WHERE id=" & RS("domain_id"))
			    if(Not RS3.EOF) then
				    domain=RS3("name")
			    end if
			    RS3.close
             end if
		end if
		if(pageType<>"drec" AND pageType<>"dvot") then
			strDate=RS("date")
			SetVar "strDate", strDate
		end if
		SetVar "strUserName", strUserName
		SetVar "strUserId", RS("id")
		Parse "DListUsersStat", True
	end if
	RS.MoveNext
Loop



	RS.Close


'--------------------


'-------------------------------
' Step through each form
'-------------------------------
Header_Show
Footer_Show
'-------------------------------
' Process page templates
'-------------------------------
Parse "Header", False
Parse "Footer", False
Parse "main", False
'-------------------------------
' Output the page to the browser
'-------------------------------
Response.write PrintVar("main")

' index Show End

'-------------------------------
' Destroy all object variables
'-------------------------------

' index Close Event begin
' index Close Event End

UnloadTemplate
'===============================

else

Login(Request("intErrorCode"))

end if

'===============================
' Display Grid Form
'-------------------------------

%><!-- #include file="db_conn_close.asp" -->