﻿<!-- #include file="config.inc" -->
<!-- #include file="md5.asp" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%
	check_session()
	uid = Session("uid")
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>DeskAlerts - <%=LNG_ALERT_LANGUAGES%></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link href="css/style9.css" rel="stylesheet" type="text/css">
	<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
	<script language="javascript" type="text/javascript">
		$(function(){
			$(".save_button").button();
			$(".add_button").button();
			$(".cancel_button").button();
		});
	</script>
</head>
<body class="body" style="margin:0px;">
<%
If(uid <> "") Then
	msg = ""
	If Request("save")="1" Then
		Conn.Execute("UPDATE alerts_langs SET is_default=0 WHERE is_default=1")
		Conn.Execute("UPDATE alerts_langs SET is_default=1 WHERE abbreviatures=N'"&Replace(Request("default"), "'", "''")&"'")
		msg = LNG_DEFAULT_SETTINGS_HAVE_BEEN_SUCESSFULLY_CHANGED
	ElseIf Request("delete")<>"" Then
		Conn.Execute("DELETE FROM alerts_langs WHERE is_default=0 AND abbreviatures=N'"&Replace(Request("delete"), "'", "''")&"'")
	ElseIf (Request("add")<>"" Or Request("edit")<>"") And Request("lang_abbr")<>"" Then
		found = False
		lang_name = Request("lang_name")
		lang_abbr = Replace(Request("lang_abbr"), " ", "")
		array_abbr = Split(lang_abbr, ",")
		If Request("edit")<>"" Then
			where = "WHERE NOT abbreviatures='"&Request("edit")&"' "
		End If
		Set RS_langs = Conn.Execute("SELECT name, abbreviatures, is_default FROM alerts_langs "&where&"ORDER BY is_default DESC, name")
		Do While Not RS_langs.EOF
			If RS_langs("name")=lang_name Then
				found = True
				msg = LNG_LANGUAGE_ALREADY_EXISTS
				Exit Do
			End If
			array_lang = Split(RS_langs("abbreviatures"), ",")
			For Each abbr in array_abbr
				For Each lang in array_lang
					If InStr(1,abbr,lang,1)=1 Or InStr(1,lang,abbr,1)=1 Then
						found = True
						msg = LNG_ABBREVIATURE_ALREADY_EXISTS
						Exit Do
					End If
				Next
			Next
			RS_langs.MoveNext
		Loop
		RS_langs.Close
		If Not found Then
			lang_name = Replace(lang_name, "'", "''")
			lang_abbr = Replace(lang_abbr, "'", "''")
			If Request("edit")<>"" Then
				Conn.Execute("UPDATE alerts_langs SET name=N'"&lang_name&"', abbreviatures=N'"&lang_abbr&"' WHERE abbreviatures=N'"&Replace(Request("edit"), "'", "''")&"'")
			Else
				Conn.Execute("INSERT INTO alerts_langs (name, abbreviatures, is_default) VALUES (N'"&lang_name&"', N'"&lang_abbr&"', 0)")
			End If
			Response.Redirect "settings_alert_langs.asp"
		End If
	End If
%>
<table width="100%" border="0" cellspacing="0" height="100%">
<tr>
	<td>
		<table width="100%" height="100%" cellspacing="0" cellpadding="0">
		<tr>
		<td height="100%">
			<div style="margin:10px;">

				<span class="work_header"><%=LNG_ALERT_LANGUAGES%></span><br/>
<%If msg<>"" Then%>
				<b><%=msg%>.</b>
<%End If%>
				<br/>
				<form method="post" action="settings_alert_langs.asp">
				<table border='0'><tr><td>
<%
		If Request("add")<>"" Or Request("edit")<>"" Then
			name=Request("lang_name")
			abbr=Request("lang_abbr")
			If name="" And abbr="" Then
				abbr=Request("edit")
				Set RS_lang = Conn.Execute("SELECT name FROM alerts_langs WHERE abbreviatures=N'"&Replace(abbr, "'", "''")&"'")
				If Not RS_lang.EOF Then
					name = RS_lang("name")
				End If
			End If
%>
				<script type="text/javascript">
					function check()
					{
						var lang_name = document.getElementById("lang_name");
						if(!lang_name.value)
						{
							alert("<%=LNG_PLEASE_FILL_ALL_EMPTY_FIELDS%>");
							lang_name.focus();
							return false;
						}
						var lang_abbr = document.getElementById("lang_abbr");
						if(!lang_abbr.value)
						{
							alert("<%=LNG_PLEASE_FILL_ALL_EMPTY_FIELDS%>");
							lang_abbr.focus();
							return false;
						}
						return true;
					}
				</script>
				<table height="100%" cellspacing="0" cellpadding="3" border="0">
				<tr>
					<td><%=LNG_LANGUAGE_NAME%></td>
					<td><input type="text" style="width:300px" id="lang_name" name="lang_name" value="<%=HtmlEncode(name)%>" maxlength="50"/></td>
				</tr>
				<tr>
					<td><%=LNG_LANGUAGE_CODE%></td>
					<td><input type="text" style="width:300px" id="lang_abbr" name="lang_abbr" value="<%=HtmlEncode(abbr)%>" maxlength="50"/></td>
				</tr>
				<tr>
					<td colspan="2" align="right">
	<%If Request("add")<>"" Then%>
						<a class="add_button" onclick="if(check()) document.forms[0].submit()"><%=LNG_ADD%></a>
						<input type="hidden" name="add" value="1">
	<%Else%>
						<a class="add_button" onclick="if(check()) document.forms[0].submit()"><%=LNG_SAVE%></a>
						<input type="hidden" name="edit" value="<%=HtmlEncode(Request("edit"))%>">
	<%End If%>
						<a class="cancel_button" href="settings_alert_langs.asp"><%=LNG_CANCEL %></a>
					</td>
				</tr>
				</table>
				<br/>
				</form>
<%End If%>
				<table height="100%" cellspacing="0" cellpadding="3" class="data_table">
					<tr class="data_table_title">
						<td class="table_title"><%=LNG_LANGUAGE_NAME%></td>
						<td class="table_title"><%=LNG_LANGUAGE_CODE%></td>
<% If Request("add")="" And Request("edit")="" Then %>
						<td class="table_title"><%=LNG_DEFAULT_LANGUAGE%></td>
						<td class="table_title"><%=LNG_ACTIONS%></td>
<% End If %>
					</tr>
<%
				j=0
				Set RS_langs = Conn.Execute("SELECT name, abbreviatures, is_default FROM alerts_langs ORDER BY is_default DESC, name")
				Do While Not RS_langs.EOF
					Response.Write "<tr><td>"&HtmlEncode(RS_langs("name"))&"</td>"
					Response.Write "<td align='center'>"&HtmlEncode(RS_langs("abbreviatures"))&"</td>"
					If Request("add")="" And Request("edit")="" Then
						Response.Write "<td align='center'><input type='radio' name='default' value='"&HtmlEncode(RS_langs("abbreviatures"))&"'"
						If RS_langs("is_default") Then
							Response.Write " checked='checked'"
						End If
						Response.Write "/></td><td>"
						Response.Write "<a href='settings_alert_langs.asp?edit="&Server.URLEncode(RS_langs("abbreviatures"))&"'><img src='images/action_icons/edit.png' alt='edit' width='16' height='16' border='0' hspace='5'/></a>"
						If Not RS_langs("is_default") Then
							Response.Write "<a href='settings_alert_langs.asp?delete="&Server.URLEncode(RS_langs("abbreviatures"))&"' "
							Response.Write "onclick='return confirm("""&LNG_ARE_YOU_SURE_YOU_WANT_TO_DELETE_SELECTED&""")'>"
							Response.Write "<img src='images/action_icons/delete.gif' alt='delete' width='16' height='16' border='0' hspace='5'/></a>"
						End If
						Response.Write "</td>"
					End If
					Response.Write "</tr>"
					RS_langs.MoveNext
					j=j+1
				Loop
%>
				</table>
<% If Request("add")="" And Request("edit")="" Then %>
				<div align="right" style="padding-top:10px">
					<a class="add_button" href="settings_alert_langs.asp?add=1"><%=LNG_ADD%></a>
					<a class="cancel_button" onclick="history.go(-1)"><%=LNG_CANCEL %></a>
					<a class="save_button" onclick="document.forms[0].submit()"><%=LNG_SAVE %></a>
				</div>
				<input type="hidden" name="save" value="1">
				</form>
<% End If %>
				</td></tr></table>
				<img src="images/inv.gif" alt="" width="1" height="100%" border="0">
			</div>
		</td>
	</tr>
	</table>
	</td>
</tr>
</table>

</body>
</html>
<%
else
	Response.Redirect "index.asp"
end if
%> 
</body>
</html>
<!-- #INCLUDE file="db_conn_close.asp" -->