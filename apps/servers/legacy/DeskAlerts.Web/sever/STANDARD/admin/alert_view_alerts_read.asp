<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<%

check_session()

%>

<!-- #include file="ad.inc" -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>View user details</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="style.css" rel="stylesheet" type="text/css">
</head>
<script language="javascript">
function LINKCLICK() {
  alert ("This action is disabled in demo");
  return false;
}
</script>

<%

  uid = Session("uid")

  read=Request("read")

  mytype=Request("type")

   if(Request("offset") <> "") then 
	offset = clng(Request("offset"))
   else 
	offset = 0
   end if	
  
%>


<body style="margin:0px" class="body">
<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	<tr>
	<td>
	<table width="100%" height="100%" border="1" bordercolor="#000000" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
		<tr>
		<td width=100% height=31 class="theme-colored-back"><a href="" class="header_title">
<%
if(read=1) then

%>
Users who have read alert
<%
else
%>
Users who haven't read alert
<%
end if
%>

</a></td>
		</tr>
		<tr>
		<td bgcolor="#ffffff" height="100%">
		<div style="margin:10px;"><br><br>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">

<%
if (uid <> "") then
'counting pages to display

  limit=10

if(read=1) then
	if(mytype="Personal") then
		Set RS = Conn.Execute("SELECT COUNT(users.id) as mycnt FROM users INNER JOIN alerts_read ON users.id=alerts_read.user_id WHERE role='U' AND alerts_read.alert_id="&Request("id"))
	end if

	if(mytype="Group") then
		Set RS = Conn.Execute("SELECT COUNT(users.id) as mycnt FROM users INNER JOIN alerts_read ON users.id=alerts_read.user_id WHERE role='U' AND alerts_read.alert_id="&Request("id"))
	end if

	if(mytype="Broadcast") then
		Set RS = Conn.Execute("SELECT COUNT(users.id) as mycnt FROM users INNER JOIN alerts_read ON users.id=alerts_read.user_id WHERE role='U' AND alerts_read.alert_id="&Request("id"))
	end if
else
	if(mytype="Personal") then
		Set RS = Conn.Execute("SELECT COUNT(users.id) as mycnt FROM users INNER JOIN alerts_sent_stat ON users.id=alerts_sent_stat.user_id WHERE role='U' AND users.id NOT IN (SELECT user_id FROM alerts_read WHERE alert_id="&Request("id")&") AND alerts_sent_stat.alert_id="&Request("id"))
	end if

	if(mytype="Group") then
		Set RS = Conn.Execute("SELECT COUNT(users.id) as mycnt FROM users INNER JOIN users_groups ON users.id=users_groups.user_id INNER JOIN alerts_sent_group ON alerts_sent_group.group_id=users_groups.group_id WHERE role='U' AND users.id NOT IN (SELECT user_id FROM alerts_read WHERE alert_id="&Request("id")&") AND alerts_sent_group.alert_id="&Request("id"))
	end if

	if(mytype="Broadcast") then
		Set RS = Conn.Execute("SELECT COUNT(users.id) as mycnt FROM users WHERE role='U' AND users.id NOT IN (SELECT user_id FROM alerts_read WHERE alert_id="&Request("id")&")")
	end if
end if
'	Loop

	if(Not RS.EOF) then
		cnt=RS("mycnt")
	end if
	RS.Close
	j=cnt/limit

%>

<%
if(cnt>0) then
	page="alert_view_users_stat.asp?id="&request("id")&"&type="&mytype&"&"
	name="users"
	Response.Write make_pages(offset, cnt, limit, page, name, sortby) 
end if
%>


		<table width="100%" height="100%" border="1" bordercolor="#EAE9E1" cellspacing="0" cellpadding="3">
		<tr bgcolor="#EAE9E1">
		<td class="table_title">User name</td>
<% if (AD = 3) then %>		<td class="table_title">Domain</td> <% end if %>
		<td class="table_title">Group</td>
				</tr>


<%

'show main table

	num=0

if(read=1) then
	if(mytype="Personal") then
		Set RS = Conn.Execute("SELECT users.id as id, users.name as name, users.domain_id as domain_id FROM users INNER JOIN alerts_read ON users.id=alerts_read.user_id WHERE role='U' AND alerts_read.alert_id="&Request("id"))
	end if

	if(mytype="Group") then
		Set RS = Conn.Execute("SELECT users.id as id, users.name as name, users.domain_id as domain_id FROM users INNER JOIN alerts_read ON users.id=alerts_read.user_id WHERE role='U' AND alerts_read.alert_id="&Request("id"))
	end if

	if(mytype="Broadcast") then
		Set RS = Conn.Execute("SELECT users.id as id, users.name as name, users.domain_id as domain_id FROM users INNER JOIN alerts_read ON users.id=alerts_read.user_id WHERE role='U' AND alerts_read.alert_id="&Request("id"))
	end if
else
	if(mytype="Personal") then
		Set RS = Conn.Execute("SELECT users.id as id, users.name as name, users.domain_id as domain_id FROM users INNER JOIN alerts_sent_stat ON users.id=alerts_sent_stat.user_id WHERE role='U' AND users.id NOT IN (SELECT user_id FROM alerts_read WHERE alert_id="&Request("id")&") AND alerts_sent_stat.alert_id="&Request("id"))
	end if

	if(mytype="Group") then
		Set RS = Conn.Execute("SELECT users.id as id, users.name as name, users.domain_id as domain_id FROM users INNER JOIN users_groups ON users.id=users_groups.user_id INNER JOIN alerts_sent_group ON alerts_sent_group.group_id=users_groups.group_id WHERE role='U' AND users.id NOT IN (SELECT user_id FROM alerts_read WHERE alert_id="&Request("id")&") AND alerts_sent_group.alert_id="&Request("id"))
	end if

	if(mytype="Broadcast") then
		Set RS = Conn.Execute("SELECT users.id as id, users.name as name, users.domain_id as domain_id FROM users WHERE role='U' AND users.id NOT IN (SELECT user_id FROM alerts_read WHERE alert_id="&Request("id")&")")
	end if

end if

	Do While Not RS.EOF

        			num=num+1
				if(num > offset AND offset+limit >= num) then
                		if(IsNull(RS("name"))) then
					name="Unregistered"
				else
				name = RS("name")
				end if
				if (AD = 3) then 	
				       	Set RS3 = Conn.Execute("SELECT name FROM domains WHERE id=" & RS("domain_id"))
					if(Not RS3.EOF) then
						  domain=RS3("name")
					else
						domain="&nbsp;"
					end if
					RS3.close
				end if
			       	mygroups=""
				Set RS1 = Conn.Execute("SELECT name FROM groups INNER JOIN users_groups ON groups.id=users_groups.group_id WHERE user_id=" & RS("id"))
				Do While Not RS1.EOF
					mygroups=mygroups & RS1("name")
					RS1.MoveNext
					if (Not RS1.EOF) then
						mygroups=mygroups & ", "
					end if
				loop
				RS1.Close
			        Response.Write "<tr><td>" + name + "</td><td align=center>"
				 if (AD = 3) then 	
					Response.Write domain 
					Response.Write "</td><td align=center>"
				 end if
			        Response.Write  mygroups & "</td>"
				Response.Write "</tr>"
				end if
	RS.MoveNext
	Loop



	RS.Close


%>         
</table>
<%
if(cnt>0) then
	page="alert_view_alerts_read.asp?id="&request("id")&"&type="&mytype&"&read="&read&"&"
	name="users"
	Response.Write make_pages(offset, cnt, limit, page, name, sortby) 
end if
%>

<%
else

%>

Please log in!

<%
end if
%>
<!-- #include file="db_conn_close.asp" -->