﻿using Castle.MicroKernel.Lifestyle;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Enums;
using DeskAlerts.ApplicationCore.Utilities;
using DeskAlerts.Scheduler.ContentJobScheduling;
using DeskAlerts.Server.Utils;
using DeskAlerts.Server.Utils.Helpers;
using DeskAlertsDotNet.Application;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Models.Permission;
using DeskAlertsDotNet.Parameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace DeskAlertsDotNet.Pages
{
    public partial class AddAlert : DeskAlertsBasePage
    {
        private int ticker;

        public string Content { get; set; }

        private Dictionary<string, object> alertFields = new Dictionary<string, object>();

        private readonly IContentJobScheduler _contentJobScheduler;

        // use curUser to access to current user of DA control panel
        // use dbMgr to access to database

        public AddAlert()
        {
            using (Global.Container.BeginScope())
            {
                _contentJobScheduler = Global.Container.Resolve<IContentJobScheduler>();
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            var alertContent = Request["alertContent"];
            if (IsPopupAlert())
            {
                var htmlContent = HttpUtility.HtmlDecode(alertContent);
                Content = DeskAlertsUtils.AddAttributeToTag(htmlContent, "a", "target", "_blank");
            }
            else
            {
                Content = alertContent;
            }

            var isTicker = Request["ticker"];
           
            var title = HttpUtility.HtmlDecode(Request["htmlTitle"]);
            title = DeskAlertsUtils.ExtractTextFromHtml(title);
            var requestTitle = !Request["htmlTitle"].Contains("font-size") && !Request["htmlTitle"].Contains("font-family")
                ? "<span style=\"font-family: Arial; font-size: 16pt; font-weight: bold;\"> "+ title + "</span>" : Request["htmlTitle"];
            string htmlTitleValue = Settings.Content["ConfEnableHtmlAlertTitle"].Equals("1") ? requestTitle : "";

            var isRsvp = Request["rsvp"];
            var isEdit = !string.IsNullOrEmpty(Request["edit"]) && Request["edit"].Equals("1");
            var isRejectEdit = !string.IsNullOrEmpty(Request["rejectedEdit"]) && Request["rejectedEdit"].Equals("1");
            alertFields.Add("title", title);
            int urgent = 0;

            int alertClass = 1;
            if (!string.IsNullOrEmpty(Request["urgentCheckBox"]))
            {
                urgent = 1;
            }

            string campaignId = Request["alertCampaign"] ?? Request["campaignSelect"];

            if (!string.IsNullOrEmpty(campaignId))
            {
                alertFields.Add("campaign_id", campaignId);
            }

            alertFields.Add("urgent", urgent);
            int aknown = 0;

            if (!string.IsNullOrEmpty(Request["aknown"]))
            {
                aknown = 1;
            }

            alertFields.Add("aknown", aknown);

            if (!string.IsNullOrEmpty(Request["unobtrusiveCheckbox"]))
            {
                alertClass = (int)AlertType.EmergencyAlert;
            }
            else if (Request["webAlert"] == "1")
            {
                alertClass = (int)AlertType.DigitalSignature;
            }
            else if (Request["isVideoAlert"] == "1")
            {
                alertClass = (int)AlertType.VideoAlert;
            }

            alertFields.Add("class", alertClass);

            // alertFields.Add("", unobtrusive);

            int selfDeletable = 0;

            var langsString = Request.GetParam("langs", "");

            if (Config.Data.IsOptionEnabled("ConfEnableAlertLangs") && !string.IsNullOrEmpty(langsString))
            {
                var langs = langsString.Split(',');

                if (!string.IsNullOrEmpty(HttpUtility.HtmlDecode(Request.GetParam("alertContent", ""))))
                    Content = Content.Replace(HttpUtility.HtmlDecode(Request.GetParam("alertContent", "")), "");

                var defaultLang = Request.GetParam("defaultLang", "");

                var defaultLangSet = dbMgr.GetScalarByQuery($"SELECT * FROM alerts_langs WHERE abbreviatures = '{defaultLang}'");
                Content += $"<!-- begin_lang lang={defaultLang.DoubleQuotes()} title={DeskAlertsUtils.HTMLToText(htmlTitleValue).DoubleQuotes()} is_default=\"1\" -->{Request.GetParam("alertContent", "")} <!-- html_title = '{htmlTitleValue}' --><!-- end_lang -->";

                foreach (var lang in langs)
                {
                    if (!string.IsNullOrEmpty(lang))
                    {
                        string langTitle = Request.GetParam("htmlTitle" + lang + "Hidden", "");
                        string langContent = Request.GetParam("alertContent_" + lang, "");

                        int defaultLangValue = defaultLang == lang ? 1 : 0;
                        Content += $"<!-- begin_lang lang={lang.DoubleQuotes()} title={DeskAlertsUtils.HTMLToText(langTitle).DoubleQuotes()} is_default={defaultLangValue.ToString().DoubleQuotes()} -->{langContent} <!-- html_title = '{langTitle}' --><!-- end_lang -->";
                        //<!-- begin_lang lang="""&HtmlEncode(lang(i))&""" title="""&HtmlEncode(elm_title(i))&""" is_default="""&HtmlEncode(lang_default(i))&""" -->"& elm(i) & html_title &"<!-- end_lang -->
                    }
                }
            }
            else if (htmlTitleValue.Length > 0 && htmlTitleValue.IndexOf("<!-- html_title") == -1)
            {
                Content = $"{Content}<!-- html_title = '{htmlTitleValue}' -->";
            }

            Content = DeskAlertsUtils.AddAttributeToTag(Content, "a", "target", "_blank");

            if (!string.IsNullOrEmpty(Request["selfDeletable"]))
            {
                selfDeletable = 1;
                if (Content.IndexOf("<!-- self-deletable -->") == -1)
                    Content += "<!-- self-deletable -->";
            }
            else
            {
                Content = Content.Replace("<!-- self-deletable -->", "");
            }

            alertFields.Add("self_deletable", selfDeletable);

            int autoClose = 0;
            if (!string.IsNullOrEmpty(Request["autoCloseCheckBox"]))
            {
                if (int.TryParse(Request["autoClosebox"], out autoClose))
                {
                    autoClose *= 60;

                    if (!string.IsNullOrEmpty(Request["manualClose"]))
                        autoClose = -autoClose;
                }
            }

            alertFields.Add("autoclose", autoClose);

            var tupleObject = (ticker, Content, alertFields);

            (int tTicker, string tContent, Dictionary<string, object> tAlertFields) handleTickerSpeedResult = DeskAlerts.Server.Utils.Pages.TickerHelper.HandleTickerSpeed(
                    Request["ticker"],
                    Request["tickerSpeedValue"],
                    Config.Data.HasModule(Modules.TICKER),
                    Config.Data.HasModule(Modules.TICKER_PRO),
                    tupleObject);

            ticker = handleTickerSpeedResult.tTicker;
            Content = handleTickerSpeedResult.tContent;
            alertFields = handleTickerSpeedResult.tAlertFields;

            if (!string.IsNullOrEmpty(Request["linkedIn"]) && Request["linkedIn"].Equals("1") && Config.Data.HasModule(Modules.LINKEDIN))
            {
                //linkedIn = 1;
                alertFields.Add("type", "L");
            }

            var recurrence = 0;
            var schedule = 0;
            var toDateStr = "01/01/1900 00:00:00:000";
            var fromDateStr = "01/01/1900 00:00:00:000";
            if (string.IsNullOrEmpty(Request["recurrenceCheckbox"]))
            {
                var lifeTimeValueStr = Request["lifetime"];
                var lifeTimeFactorStr = Request["lifetimeFactor"];

                if (int.TryParse(lifeTimeFactorStr, out var lifeTimeFactor) &&
                    int.TryParse(lifeTimeValueStr, out var lifeTimeValue))
                {
                    var fromDate = DateTime.Now;
                    fromDateStr = DateTimeConverter.ToDbDateTime(fromDate);
                    var toDate = lifeTimeValue == 0 ? fromDate.AddYears(100) : fromDate.AddMinutes(lifeTimeValue * lifeTimeFactor);
                    toDateStr = DateTimeConverter.ToDbDateTime(toDate);
                }

                alertFields.Add("from_date", fromDateStr);
                alertFields.Add("to_date", toDateStr);
                alertFields.Add("recurrence", recurrence);
                alertFields.Add("lifetime", 1);
            }
            else
            {
                if (!string.IsNullOrEmpty(Request["schedulePanel$start_date_and_time"]))
                {
                    fromDateStr = DateTimeConverter.ConvertFromUiToDb(Request["schedulePanel$start_date_and_time"]);
                }

                if (!string.IsNullOrEmpty(Request["schedulePanel$end_date_and_time"]))
                {
                    toDateStr = DateTimeConverter.ConvertFromUiToDb(Request["schedulePanel$end_date_and_time"]);
                }

                if (!string.IsNullOrEmpty(Request["recurrenceCheckbox"]))
                {
                    schedule = 1;
                }

                if (!string.IsNullOrEmpty(Request["schedulePanel$pattern"]) && Request["schedulePanel$pattern"] != "o")
                {
                    recurrence = 1;

                    if (!string.IsNullOrEmpty(Request["schedulePanel$start_date_rec"])
                            && !string.IsNullOrEmpty(Request["schedulePanel$start_time"]))
                    {
                        fromDateStr = DateTimeConverter.ConvertFromUiToDb(Request["schedulePanel$start_date_rec"] + " " + Request["schedulePanel$start_time"]);
                    }

                    if (!string.IsNullOrEmpty(Request["schedulePanel$end_date_rec"])
                            && !string.IsNullOrEmpty(Request["schedulePanel$end_time"]))
                    {
                        toDateStr = DateTimeConverter.ConvertFromUiToDb(Request["schedulePanel$end_date_rec"] + " " + Request["schedulePanel$end_time"]);
                    }
                    else if (!string.IsNullOrEmpty(Request["schedulePanel$end_time"]))
                    {
                        var dateTime = new DateTime(1900, 1, 1);
                        toDateStr = DateTimeConverter.ConvertFromUiToDb(DateTimeConverter.ToDbDateTime(dateTime) + " " + Request["schedulePanel$end_time"]);
                    }

                    //   schedule = 1;
                    alertFields.Add("lifetime", 0);
                }

                alertFields.Add("from_date", fromDateStr);
                alertFields.Add("to_date", toDateStr);
                alertFields.Add("recurrence", recurrence);
            }

            alertFields.Add("schedule", schedule);
            alertFields.Add("schedule_type", 0);

            int resizable = 0;

            if (!string.IsNullOrEmpty(Request["resizable"]))
            {
                resizable = 1;
            }

            alertFields.Add("resizable", resizable);

            int fullscreen = 0;
            int position = (int)AlertPosition.RightBottom;
            string tickerPosition = "0";

            int.TryParse(Request["fullscreen"], out fullscreen);
            int alertWidth = 0, alertHeight = 0;
            if (fullscreen == 0) //non-fullscreen alert
            {
                alertWidth = Convert.ToInt32(Request["alertWidth"]);
                alertHeight = Convert.ToInt32(Request["alertHeight"]);

                if (ticker == 0)
                {
                    if (!string.IsNullOrEmpty(Request["position"]))
                        position = Convert.ToInt32(Request["position"]);

                    alertFields.Add("fullscreen", position);
                    alertFields.Add("alert_width", alertWidth);
                    alertFields.Add("alert_height", alertHeight);
                }
                else if (ticker == 1)
                {
                    tickerPosition = !string.IsNullOrEmpty(Request["ticker_position"])
                        ? Request["ticker_position"]
                        : Settings.Content["ConfTickerPosition"];

                    alertFields.Add("fullscreen", tickerPosition);
                    alertFields.Add("alert_width", null);
                    alertFields.Add("alert_height", null);
                }
            }
            else
            {
                alertFields.Add("fullscreen", fullscreen);
            }

            string deviceType = "1";

            if (Config.Data.HasModule(Modules.MOBILE) && !string.IsNullOrEmpty(Request["dev"]))
            {
                deviceType = Request["dev"];
            }

            int sms = 0;
            if (Config.Data.HasModule(Modules.SMS) && !string.IsNullOrEmpty(Request["sms"]))
            {
                sms = Convert.ToInt32(Request["sms"]);
            }
            alertFields.Add("sms", sms);

            int email = 0;
            if (Config.Data.HasModule(Modules.EMAIL) && !string.IsNullOrEmpty(Request["emailCheck"]))
            {
                email = Convert.ToInt32(Request["emailCheck"]);
            }
            alertFields.Add("email", email);

            int shouldApprove = 0;
            int rejectedEdit = 0;
            bool isDraft = !string.IsNullOrEmpty(this.Request["IsDraft"]) && this.Request["IsDraft"] == "1";
            string emailSender = "";


            string instantMessage = Request["IsInstant"];
            bool isInstant = !string.IsNullOrEmpty(instantMessage) && instantMessage == "1";

            if (Config.Data.HasModule(Modules.EMAIL) && !string.IsNullOrEmpty(Request["email_sender"]))
            {
                emailSender = Request["email_sender"];
            }

            alertFields.Add("email_sender", emailSender);

            int desktop = 1;

            if (!string.IsNullOrEmpty(Request["desktopCheck"]))
            {
                desktop = Convert.ToInt32(Request["desktopCheck"]);
            }
            else
            {
                deviceType = "2";
            }

            alertFields.Add("device", deviceType);
            alertFields.Add("desktop", desktop);

            if (!string.IsNullOrEmpty(Request["skin_id"]))
            {
                string skinId = Request["skin_id"];

                if (skinId.Equals("default"))
                    skinId = null;

                alertFields.Add("caption_id", skinId);
            }
            else
            {
                alertFields.Add("caption_id", null);
            }

            if (Config.Data.HasModule(Modules.APPROVE) && Settings.Content["ConfAllowApprove"] == "1")
            {
                if (curUser.HasPrivilege || curUser.Policy[Policies.ALERTS].CanApprove || isInstant)
                {
                    alertFields.Add("approve_status", (int)ApproveStatus.ApprovedByDefault);
                }
                else
                {
                    alertFields.Add("approve_status", 0);
                }
            }
            else if (isRejectEdit && !isInstant)
            {
                alertFields.Add("approve_status", 0);
                rejectedEdit = 1;
            }
            else if (!string.IsNullOrEmpty(Request["shouldApprove"]) && Request["shouldApprove"].Equals("1"))
            {
                alertFields.Add("approve_status", 1);
                shouldApprove = 0;
            }
            else
            {
                alertFields.Add("approve_status", 1);
            }

            if (!string.IsNullOrEmpty(Request["linkedIn"]) && Request["linkedIn"].Equals("1") && Config.Data.HasModule(Modules.LINKEDIN))
            {
                alertFields.Add("type", "L");
            }
            else
            {
                alertFields.Add("type", "D");
            }

            string colorCode = Request["colorСode"];

            if (string.IsNullOrEmpty(colorCode))
                colorCode = "00000000-0000-0000-0000-000000000000";

            if (!string.IsNullOrEmpty(Request["color_code"]))
                colorCode = Request["color_code"];

            alertFields.Add("color_code", colorCode);

            if (!string.IsNullOrEmpty(Request["printAlertCheckBox"]))
            {
                //  printAlert = Convert.ToInt32(Request["print_alert"]);
                Content += "<!-- printable_alert -->";
            }
            else
            {
                Content = Content.Replace("printable_alert", "");
            }

            if (!string.IsNullOrEmpty(Request["socialMedia"]))
            {
                alertFields.Add("social_media", 1);
            }
            else
            {
                alertFields.Add("social_media", 0);
            }

            if (!string.IsNullOrEmpty(Request["blogPost"]))
            {
                alertFields.Add("post_to_blog", 1);
            }
            else
            {
                alertFields.Add("post_to_blog", 0);
            }

            if (!string.IsNullOrEmpty(Request["textToCall"]))
            {
                alertFields.Add("text_to_call", 1);
            }
            else
            {
                alertFields.Add("text_to_call", 0);
            }

            if (!string.IsNullOrEmpty(Request["videoLink"]) || !string.IsNullOrEmpty(Request["link_embed"]))
            {
                alertFields.Add("video", 1);
            }
            else
            {
                alertFields.Add("video", 0);
            }

            alertFields.Add("alert_text", Content);
            alertFields.Add("sender_id", curUser.Id);
            alertFields.Add("toolbarmode", 0);
            alertFields.Add("deskalertsmode", 0);
            alertFields.Add("is_repeatable_template", recurrence);

            alertFields.Add("create_date", DateTimeConverter.ToDbDateTime(DateTime.Now));

            int alertId = 0;
            if (!isEdit)
            {
                alertId = AlertManager.Default.AddAlert(dbMgr, alertFields);
            }
            else
            {
                alertId = Convert.ToInt32(Request["alertId"]);
                AlertManager.Default.UpdateAlert(dbMgr, alertFields, alertId);
            }

            if (recurrence == 1 || !string.IsNullOrEmpty(Request["countdowns"]))
            {
                Dictionary<string, object> recurDictionary = new Dictionary<string, object>();

                string mask = new string('0', 7);

                string[] exceptCollumns =
                {
                    "start_date_and_time", "from_date", "end_date_and_time", "start_date_rec",
                    "end_date_rec", "start_time", "end_time"
                };

                var schedulePanel = "schedulePanel$weekDayPatternCheckBoxList$";
                foreach (var key in Request.Params.AllKeys)
                {
                    string value = Request.Params[key];

                    var maskStringBuilder = new StringBuilder(mask);
                    if (key.StartsWith(schedulePanel))
                    {
                        //set bits of weekdays when we want to recieve alert to 1
                        int weekDay = Convert.ToInt32(key.Substring(schedulePanel.Length, 1));

                        maskStringBuilder[6 - weekDay] = '1';

                        mask = maskStringBuilder.ToString();
                    }
                    else if (key.StartsWith("schedulePanel$"))
                    {
                        string newKey = key.Replace("schedulePanel$", "");

                        if (Array.IndexOf(exceptCollumns, newKey) == -1)
                            recurDictionary.Add(newKey, value);
                    }
                }

                recurDictionary.Add("week_days", Convert.ToInt32(mask, 2));
                //  recurDictionary.Add("end_type", Request["end_type"]);
                //  Response.End();
                //  recurDictionary.Add("end_after", Request["end_after"]);
                recurDictionary.Add("alert_id", alertId);

                if (!string.IsNullOrEmpty(Request["countdowns"]))
                {
                    // recurDictionary.Add("pattern", "o");
                    //   recurDictionary.Add("number_days", "1");

                    string[] countdownsArray = Request["countdowns"].Split(',');

                    if (countdownsArray.Length > 0)
                        recurDictionary.Add("countdowns", countdownsArray.ToList());
                }
                else
                {
                    recurDictionary.Add("countdowns", new List<string>());
                }

                ReccurenceManager.ProcessReccurence(dbMgr, recurDictionary);
            }

            //   Response.WriteLine("Alert id = " + alertId);

            DataSet surveySet = dbMgr.GetDataByQuery("SELECT id from surveys_main WHERE sender_id = " + alertId);

            //rsvp insertion
            if (!string.IsNullOrEmpty(Request["rsvp"]) && Request["rsvp"].Equals("1"))
            {
                int surveyId = -1;
                if (surveySet.IsEmpty)
                {
                    surveyId =
                        Convert.ToInt32(dbMgr.GetScalarByQuery(
                            "INSERT INTO surveys_main (type, closed, sender_id) OUTPUT INSERTED.id as newId VALUES ('A', 'A', " +
                            alertId + ") "));
                }
                else
                {
                    surveyId = surveySet.GetInt(0, "id");
                }

                //TODO: Change implementation to new repositories
                if (surveyId > 0)
                {
                    var shouldUpdate = !surveySet.IsEmpty;

                    var questionText = Request["question"]?.Replace("'", "''");
                    var questionOption1 = Request["question_option1"]?.Replace("'", "''");
                    var questionOption2 = Request["question_option2"]?.Replace("'", "''");

                    var needSecondQuestion = !string.IsNullOrEmpty(Request["need_second_question"]);

                    var secondQuestion = HttpUtility.HtmlDecode(Request["second_question"])?.Replace("'", "''");
                    if (shouldUpdate)
                    {
                        var questionId =
                             Convert.ToInt32(dbMgr.GetScalarByQuery("UPDATE surveys_questions SET question = N'" + questionText +
                                                      "' OUTPUT INSERTED.id  WHERE question_number = 1 AND survey_id = " + surveyId));

                        dbMgr.ExecuteQuery("DELETE FROM surveys_answers WHERE question_id = " + questionId);

                        var q1 = "INSERT INTO surveys_answers (answer, question_id, correct) VALUES(N'" + questionOption1 + "', " + questionId + ", 0)";
                        var q2 = "INSERT INTO surveys_answers (answer, question_id, correct) VALUES(N'" + questionOption2 + "', " + questionId + ", 0)";

                        dbMgr.ExecuteQuery(q1);
                        dbMgr.ExecuteQuery(q2);

                        if (needSecondQuestion)
                        {
                            dbMgr.ExecuteQuery("UPDATE surveys_questions  SET question = N'" + secondQuestion +
                                                      "' OUTPUT INSERTED.id  WHERE question_number = 2 AND survey_id = " + surveyId);
                        }
                    }
                    else
                    {
                        var insertQuestion = "INSERT INTO surveys_questions (question, survey_id, question_number, question_type) OUTPUT INSERTED.id as newId  VALUES (N'" + questionText +
                              "', " + surveyId + ", 1, 'M')";
                        var questionId = Convert.ToInt32(dbMgr.GetScalarByQuery(insertQuestion));

                        var q1 = "INSERT INTO surveys_answers (answer, question_id, correct) VALUES(N'" + questionOption1 + "', " + questionId + ", 0)";
                        var q2 = "INSERT INTO surveys_answers (answer, question_id, correct) VALUES(N'" + questionOption2 + "', " + questionId + ", 0)";

                        dbMgr.ExecuteQuery(q1);
                        dbMgr.ExecuteQuery(q2);

                        if (needSecondQuestion)
                        {
                            var secondQuestionQuery =
                                "INSERT INTO surveys_questions (question, survey_id, question_number, question_type) VALUES(N'" +
                                secondQuestion + "', " + surveyId + ", 2, 'I')";

                            dbMgr.ExecuteQuery(secondQuestionQuery);
                        }
                    }
                }
            }
            else if (!surveySet.IsEmpty)
            {
                string surveyId = surveySet.GetString(0, "id");
                dbMgr.ExecuteQuery(
                    "DELETE FROM surveys_answers WHERE question_id IN (SELECT id FROM surveys_questions WHERE survey_id = " +
                    surveyId + ")");
                dbMgr.ExecuteQuery("DELETE FROM surveys_answers WHERE survey_id = " + surveyId);
                dbMgr.ExecuteQuery("DELETE FROM surveys_questions WHERE survey_id = " + surveyId);
                dbMgr.ExecuteQuery("DELETE FROM surveys_main WHERE id = " + surveyId);
            }

            if (schedule == 1 && AppConfiguration.IsNewContentDeliveringScheme)
            {
                var dateTimeUtc = DateTimeConverter.ParseDateTimeFromDb(fromDateStr, true).ToUniversalTime();
                var contentJobDto = new ContentJobDto(dateTimeUtc, alertId, (AlertType)alertClass);
                if (recurrence != 1)
                {
                    _contentJobScheduler.CreateJob(contentJobDto);
                }
                else
                {
                    _contentJobScheduler.CreateRepeatableJob(contentJobDto);
                }
            }

            //Redirect to users selection
            string redirectFile = "RecipientsSelection.aspx";

            string scheduleParam = Request["webAlert"] != "1" ? alertFields["schedule"].ToString() : "0";
            string autosubmit = ""; //unused option
            string redirectUrl;

            if (Request["IsInstant"] == "1" && !curUser.HasPrivilege && !curUser.Policy[Policies.IM].CanSend)
            {

                redirectUrl = redirectFile + "?id=" + alertId + "&dup_id=" + Request["alertId"] + "&instant=" + instantMessage +
                              "&return_page=dashboard.aspx&recurrence=" + scheduleParam + "&campaign_id=" +
                              campaignId + "&shouldApprove=" + shouldApprove +
                              "&rejectedEdit=" + rejectedEdit;
            }
            else if (Request["IsInstant"] == "1")
            {
                redirectUrl = redirectFile + "?id=" + alertId + "&dup_id=" + Request["alertId"] + "&instant=" + instantMessage +
                              "&return_page=InstantMessages.aspx&recurrence=" + scheduleParam + "&campaign_id=" +
                              campaignId + "&shouldApprove=" + shouldApprove +
                              "&rejectedEdit=" + rejectedEdit;
            }
            else if (!string.IsNullOrEmpty(campaignId) && campaignId != "-1")
            {
                redirectUrl = redirectFile + "?id=" + alertId + "&dup_id=" + Request["alertId"] + "&instant=" + instantMessage +
                                  "&return_page=CampaignDetails.aspx?campaignId=" + campaignId + "&recurrence=" + scheduleParam +
                                  "&campaign_id=" + campaignId + "&shouldApprove=" + shouldApprove +
                                  "&rejectedEdit=" + rejectedEdit;
            }
            else if (Request["webAlert"] == "1")
            {
                redirectUrl = redirectFile + "?id=" + alertId + "&dup_id=" + Request["alertId"] + "&autosubmit=" + autosubmit +
                                  "&recurrence=" + scheduleParam + "&campaign_id=" + campaignId + "&webalert=1" + "&shouldApprove=" + shouldApprove + "&rejectedEdit=" + rejectedEdit +
                                  "&return_page=DigitalSignage/DigitalSignageLinks.aspx";
            }
            else if (!string.IsNullOrEmpty(Request["videoLink"]) || !string.IsNullOrEmpty(Request["link_embed"]))
            {
                redirectUrl =
                    redirectFile + "?id=" + alertId + "&dup_id=" + Request["alertId"] + "&autosubmit=" + autosubmit + "&recurrence=" + scheduleParam +
                    "&campaign_id=" + campaignId + "&shouldApprove=" + shouldApprove +
                        "&rejectedEdit=" + rejectedEdit + "&return_page=Videos.aspx";
            }
            else
            {
                redirectUrl =
                    redirectFile + "?id=" + alertId + "&dup_id=" + Request["alertId"] + "&autosubmit=" + autosubmit + "&recurrence=" + scheduleParam +
                    "&campaign_id=" + campaignId + "&shouldApprove=" + shouldApprove +
                        "&rejectedEdit=" + rejectedEdit;
            }

            if (alertFields.ContainsKey("sms") && alertFields["sms"].ToString().Equals("1"))
            {
                redirectUrl += "&sms=1";

            }

            if (alertFields.ContainsKey("text_to_call") && alertFields["text_to_call"].ToString().Equals("1"))
            {
                redirectUrl += "&textToCall=1";

            }

            if (alertFields.ContainsKey("desktop") && alertFields["desktop"].ToString().Equals("1"))
            {
                redirectUrl += "&desktop=1";

            }

            if (alertFields.ContainsKey("email") && alertFields["email"].ToString().Equals("1"))
            {
                redirectUrl += "&email=1";

            }

            if (!string.IsNullOrEmpty(Request["edit"]) && Request["edit"].Equals("1") && isInstant)
            {
                redirectUrl += "&edit=1";

            }

            if (!string.IsNullOrEmpty(Request["isModerated"]) && Request["isModerated"].Equals("1"))
            {
                redirectUrl += "&isModerated=1";

            }

            if (alertFields.ContainsKey("device"))
            {
                redirectUrl += "&device=" + alertFields["device"];

            }

            if (!string.IsNullOrEmpty(Request["IsDraft"]) && Request["IsDraft"] == "1")
            {
                if (!string.IsNullOrEmpty(Request["videoLink"]) || !string.IsNullOrEmpty(Request["link_embed"]))
                {
                    Response.Redirect("EditVideo.aspx?edit=1&id=" + alertId, true);
                }

                redirectUrl = $"CreateAlert.aspx?edit=1&id={alertId}";


                if (!string.IsNullOrEmpty(isTicker) && isTicker == "1")
                {
                    redirectUrl += "&ticker=1";
                }
                else if (!string.IsNullOrEmpty(isRsvp) && isRsvp == "1")
                {
                    redirectUrl += "&rsvp=1";
                }

                Response.Redirect(redirectUrl, true);
            }
            Response.Redirect(redirectUrl);
            //alert_users.asp
        }
        
        private bool IsPopupAlert()
        {
            return Request["popup"] == "1";
        }
    }
}