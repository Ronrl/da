<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="ad.inc" -->
<%

check_session()

%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>View user details</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
<link href="style.css" rel="stylesheet" type="text/css">
</head>
<script language="javascript">
function LINKCLICK() {
  alert ("This action is disabled in demo");
  return false;
}
</script>

<%

  uid = Session("uid")

  read=Request("read")
   if(Request("offset") <> "") then 
	offset = clng(Request("offset"))
   else 
	offset = 0
   end if	
  
alertId = "0"
Set RSAlert = Conn.Execute("SELECT sender_id FROM surveys_main WHERE id=" & Request("id"))
if not RSAlert.EOF then
	alertId = RSAlert("sender_id")
end if

%>


<body style="margin:0" class="body">
<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	<tr>
	<td>
	<table width="100%" height="100%" border="1" bordercolor="#000000" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
		<tr>
		<td width=100% height=31 class="theme-colored-back"><a href="" class="header_title">Users who haven't <% if(read="1") then Response.Write "voted" else Response.Write "received survey" end if %></a></td>
		</tr>
		<tr>
		<td bgcolor="#ffffff" height="100%">
		<div style="margin:10px;"><br><br>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">

<%
if (uid <> "") then
	limit=10

	if(read="1") then
		Set RS = Conn.Execute("SELECT users.id as id FROM users INNER JOIN alerts_received ON users.id=alerts_received.user_id WHERE role='U' AND alerts_received.alert_id="&alertId)
	else
		Set RS = Conn.Execute("SELECT users.id as id FROM users INNER JOIN alerts_sent_stat ON users.id=alerts_sent_stat.user_id WHERE role='U' AND alerts_sent_stat.alert_id="&alertId)
	end if
	cnt=0

	Do While Not RS.EOF
		if(read<>"1") then
			Set RS1 = Conn.Execute("SELECT id FROM alerts_received WHERE user_id="&RS("id")&" AND alert_id="&alertId)
			if(RS1.EOF) then cnt=cnt+1 end if
		else
			cnt=cnt+1
		end if
	RS.MoveNext
	Loop

	RS.Close
  j=cnt/limit

%>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr><td> 
<% 
sum=offset+1
Response.Write sum   
%>-
<% 
if(cnt-offset<limit) then 
Response.Write cnt
else 
sum=offset+limit
Response.Write sum
end if
%> 
from 
<% 
Response.Write cnt
%> users</td>
		<td align=right>pages: 
		<%
			i=0
			Do while i<j
			k=i*limit 
			l=k+limit 
			m=i+1
			if(k <> offset) then 
				%> 
				| <A href="survey_view_users_stat.asp?id=<%response.write request("id")%>&<%if(read="1") then response.write "read=1&" end if %>offset=
				<% Response.Write k %>
				">  
				<% Response.Write m %>
				</a>
				<%
			else 
				%>
				| <strong>
				<% Response.Write m %>
				</small>
				<%
			end if
			i = i+1
			loop
		%>
		</td></tr>
		</table>

		<table width="100%" height="100%" border="1" bordercolor="#EAE9E1" cellspacing="0" cellpadding="3">
		<tr bgcolor="#EAE9E1">
		<td class="table_title">User name</td>
<% if (AD = 3) then %>		<td class="table_title">Domain</td> <% end if %>
		<td class="table_title">Group</td>
				</tr>


<%

'show main table

'	Set RS = Conn.Execute("SELECT id, name, domain_id FROM users WHERE role='U' ORDER BY name")	

	if(read="1") then
		Set RS = Conn.Execute("SELECT users.id as id, name, domain_id FROM users INNER JOIN alerts_received ON users.id=alerts_received.user_id WHERE role='U' AND alerts_received.alert_id="&alertId)
	else
		Set RS = Conn.Execute("SELECT users.id as id, name, domain_id FROM users INNER JOIN alerts_sent_stat ON users.id=alerts_sent_stat.user_id WHERE role='U' AND alerts_sent_stat.alert_id="&alertId)
	end if
'	cnt=0

'	Do While Not RS.EOF
'	RS.MoveNext
'	Loop


	num=0
	Do While Not RS.EOF


		if(read<>"1") then
			Set RS222 = Conn.Execute("SELECT id FROM alerts_received WHERE user_id="&RS("id")&" AND alert_id="&alertId)
			if(RS222.EOF) then 

		num=num+1

		if(num > offset AND offset+limit >= num) then

				if(IsNull(RS("name"))) then
					name="Unregistered"
				else
					name = RS("name")
				end if
				if (AD = 3) then 	
				       	Set RS3 = Conn.Execute("SELECT name FROM domains WHERE id=" & RS("domain_id"))
					if(Not RS3.EOF) then
						  domain=RS3("name")
					else
						domain="&nbsp;"
					end if
					RS3.close
				end if
			       	mygroups=""
				Set RS1 = Conn.Execute("SELECT name FROM groups INNER JOIN users_groups ON groups.id=users_groups.group_id WHERE user_id=" & RS("id"))
				Do While Not RS1.EOF
					mygroups=mygroups & RS1("name")
					RS1.MoveNext
					if (Not RS1.EOF) then
						mygroups=mygroups & ", "
					end if
				loop
				RS1.Close
			        Response.Write "<tr><td>" + name + "</td><td align=center>"
				 if (AD = 3) then 	
					Response.Write domain 
					Response.Write "</td><td align=center>"
				 end if
			        Response.Write  mygroups & "</td>"
				Response.Write "</tr>"
	        	end if
		end if
		else

		num=num+1
		
		if(num > offset AND offset+limit >= num) then

			if(IsNull(RS("name"))) then
				name="Unregistered"
			else
				name = RS("name")
			end if
			if (AD = 3) then 	
			       	Set RS3 = Conn.Execute("SELECT name FROM domains WHERE id=" & RS("domain_id"))
				if(Not RS3.EOF) then
					  domain=RS3("name")
				else
					domain="&nbsp;"
				end if
				RS3.close
			end if
		        mygroups=""
			Set RS1 = Conn.Execute("SELECT name FROM groups INNER JOIN users_groups ON groups.id=users_groups.group_id WHERE user_id=" & RS("id"))
			Do While Not RS1.EOF
				mygroups=mygroups & RS1("name")
				RS1.MoveNext
				if (Not RS1.EOF) then
					mygroups=mygroups & ", "
				end if
			loop
			RS1.Close
		        Response.Write "<tr><td>" + name + "</td><td align=center>"
			 if (AD = 3) then 	
				Response.Write domain 
				Response.Write "</td><td align=center>"
			 end if
		        Response.Write  mygroups & "</td>"
			Response.Write "</tr>"
		end if
		end if

	RS.MoveNext
	Loop
	RS.Close
%>         
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr><td> 
<% 
sum=offset+1
Response.Write sum
%>-
<% if(cnt-offset<limit) then
Response.Write cnt
else 
sum=offset+limit
Response.Write  sum
end if
%> from 
<% Response.Write cnt
%> users</td>
		<td align=right>pages: 
		<%
			i=0
			Do while i<j
			k=i*limit 
			l=k+limit 
			m=i+1
			if(k <> offset) then 
				%> 
				| <A href="survey_view_users_stat.asp?id=<%response.write request("id")%>&<%if(read="1") then response.write "read=1&" end if %>offset=
				<% Response.Write k %>
				">  
				<% Response.Write m %>
				</a>
				<%
			else 
				%>
				| <strong>
				<% Response.Write m %>
				</small>
				<%
			end if
			i = i+1
			loop
		%>
		</td></tr>
		</table>

<%
else
%>

Please logon!

<%
end if
%>
<!-- #include file="db_conn_close.asp" -->