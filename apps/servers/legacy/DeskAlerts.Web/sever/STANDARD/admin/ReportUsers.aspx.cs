﻿using System;
using System.Data.SqlTypes;
using System.Globalization;
using System.Web.UI.WebControls;
using DeskAlerts.Server.Utils;
using DeskAlerts.Server.Utils.Helpers;
using Resources;

namespace DeskAlertsDotNet.Pages
{
    public partial class ReportUsers : DeskAlertsBaseListPage
    {
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            userNameCell.Text = GetSortLink(resources.LNG_USERNAME, "name", Request["sortBy"]);
            statusCell.Text = GetSortLink(resources.LNG_STATUS, "last_request", Request["sortBy"]);
            lastActivityCell.Text = GetSortLink(resources.LNG_LAST_ACTIVITY, "last_request", Request["sortBy"]);
            registeredDateCell.Text = GetSortLink(resources.LNG_REGISTRATION_DATE, "reg_date", Request["sortBy"]);

            var cnt = Content.Count < (Offset + Limit) ? Content.Count : Offset + Limit;
            for (var i = Offset; i < cnt; i++)
            {
                var row = Content.GetRow(i);
                var tableRow = new TableRow();

                var user_type = dbMgr.GetScalarByQuery<string>("SELECT client_version FROM users where id='" + Content.GetString(i, "id") + "'");
                if (user_type == "web client")
                {
                    continue;
                }

                var usersStatLink =
                         string.Format(
                             "window.open('UserStatisticsDetails.aspx?id={0}','', 'status=0, toolbar=0, width=555, height=450, scrollbars=1')",
                             Content.GetString(i, "id"));



                var usernameCell = new TableCell { Text = string.Format("<a href='#' onclick=\"{0}\" >{1}</a>", usersStatLink, Content.GetString(i, "name")) };
                tableRow.Cells.Add(usernameCell);

                var lastRequest = row.GetDateTime("last_request");
                var isOnline = DeskAlertsUtils.IsUserOnline(lastRequest, row.GetInt("next_request"));
                var statusImg = new Image
                {
                    Width = 11,
                    Height = 11,
                    ImageUrl = isOnline ? "images/online.gif" : "images/offline.gif",
                    AlternateText = isOnline ? resources.LNG_ONLINE : resources.LNG_OFFLINE
                };

                var statusContentCell = new TableCell { HorizontalAlign = HorizontalAlign.Center };
                statusContentCell.Controls.Add(statusImg);
                tableRow.Cells.Add(statusContentCell);

                var registeredContentCell = new TableCell
                {
                    Text = DateTimeConverter.ToUiDateTime(Content.GetDateTime(i, "reg_date"))
                };

                tableRow.Cells.Add(registeredContentCell);

                var lastActivityContentCell = new TableCell
                {
                    Text = DeskAlertsUtils.IsNotEmptyDateTime(lastRequest) ? DateTimeConverter.ToUiDateTime(lastRequest) : string.Empty
                };

                tableRow.Cells.Add(lastActivityContentCell);

                contentTable.Rows.Add(tableRow);
            }
        }

        #region DataProperties
        protected override string DataTable => "users";

        protected override string[] Collumns => new[] { "name", "next_request", "last_request", "reg_date" };

        protected override string CustomQuery
        {
            get
            {
                string query = GetFilterForQuery();

                query += @"SELECT t.user_id as id,
                               users.next_request,
                               users.last_standby_request,
                               users.next_standby_request,
                               users.context_id,
                               users.mobile_phone,
                               users.email,
                               users.name AS name,
                               users.display_name,
                               users.standby,
                               users.domain_id,
                               domains.name AS domain_name,
                               users.deskbar_id,
                               users.reg_date,
                               users.last_date,
                               users.last_request AS last_request1,
                               users.last_request,
                               users.client_version,
                               edir_context.name AS context_name
                        FROM #tempUsers t
                        LEFT JOIN users ON t.user_id = users.id
                        LEFT JOIN domains ON domains.id = users.domain_id
                        LEFT JOIN edir_context ON edir_context.id = users.context_id
                        WHERE ROLE='U'";

                query += " AND (users.last_request >= '01/01/2001 00:00:00' AND users.last_request <= '" +
                         DateTime.Now.ToString("dd/MM/yyyy") + " 23:59:59') ";

                if (!string.IsNullOrEmpty(Request["sortBy"]))
                {
                    query += " ORDER BY " + Request["sortBy"];
                }
                else
                {
                    query += " ORDER BY name";
                }

                query += @" DROP TABLE #tempOUs
                        DROP TABLE #tempUsers";

                return query;
            }
        }

        private string GetFilterForQuery()
        {
            string searchType = Request["type"];

            string searchId = Request["id"];

            string query = @"SET NOCOUNT ON IF OBJECT_ID('tempdb..#tempOUs') IS NOT NULL
                                    DROP TABLE #tempOUs
                                    CREATE TABLE #tempOUs (Id_child BIGINT NOT NULL, Rights BIT NOT NULL);

                                    IF OBJECT_ID('tempdb..#tempUsers') IS NOT NULL
                                    DROP TABLE #tempUsers
                                    CREATE TABLE #tempUsers (user_id BIGINT NOT NULL);

                                    DECLARE @now DATETIME;


                                    SET @now = GETDATE();";

            if (searchType == "ou")
            {
                if (Request["search"] == "1")
                {
                    query += @"WITH OUs (id) AS
                                      (SELECT CAST(" + searchId + @" AS BIGINT)
                                       UNION ALL SELECT Id_child
                                       FROM OU_hierarchy h
                                       INNER JOIN OUs ON OUs.id = h.Id_parent)
                                    INSERT INTO #tempOUs (Id_child, Rights)
                                      (SELECT DISTINCT id,
                                                       " + searchId + @"
                                       FROM OUs) OPTION (MaxRecursion 10000);";
                }
                else
                {
                    query += @" INSERT INTO #tempOUs (Id_child, Rights) VALUES (" + searchId + ", 0)";
                }
            }
            else if (searchType == "DOMAIN")
            {
                query += @" INSERT INTO #tempOUs (Id_child, Rights) (SELECT id, 0 FROM OU WHERE Id_domain = " + searchId + ")";
            }
            else
            {
                query += " INSERT INTO #tempOUs (Id_child, Rights) (SELECT id, 0 FROM OU) ";
            }


            query += @"INSERT INTO #tempUsers (user_id)
                              (SELECT DISTINCT Id_user
                               FROM OU_User u
                               INNER JOIN #tempOUs t ON u.Id_OU=t.Id_child
                               LEFT JOIN users ON u.Id_user = users.id
                               WHERE ROLE='U'";

            if (!String.IsNullOrEmpty(Request["uname"]))
            {
                query += String.Format(" AND (users.name LIKE N'%{0}%' OR users.display_name LIKE N'%{0}%')", Request["uname"]);
            }

            query += ")";

            if (searchType != "ou")
            {
                query += @"	INSERT INTO #tempUsers(user_id)		
                                      (SELECT DISTINCT users.id		
                                       FROM users 		
                                       LEFT JOIN #tempUsers t ON t.user_id=users.id		
                                       WHERE t.user_id IS NULL		
                                         AND users.ROLE='U'";

                if (searchType == "DOMAIN")
                    query += " AND users.domain_id = " + searchId;

                if (!String.IsNullOrEmpty(Request["uname"]))
                {
                    query += String.Format(" AND (users.name LIKE N'%{0}%' OR users.display_name LIKE N'%{0}%')", Request["uname"]);
                }



                query += ")";

            }
            return query;
        }

        protected override string CustomCountQuery
        {
            get
            {
                var query = GetFilterForQuery();

                query += @" SELECT COUNT(1) AS cnt FROM users WHERE users.last_request >= '01/01/2001 00:00:00' AND users.last_request <= '" + DateTime.Now.ToString("dd/MM/yyyy") + " 23:59:59' ";
                // todo knock-out SD-3547 get count onse active users from queru LIKE " query += @" SELECT COUNT(1) AS cnt FROM #tempUsers";
                return query;
            }
        }

        protected override string DefaultOrderCollumn => "name";

        protected override System.Web.UI.HtmlControls.HtmlGenericControl TopPaging => topPaging;

        protected override System.Web.UI.HtmlControls.HtmlGenericControl BottomPaging => bottomPaging;

        /*
         * 
         * If you want to add condition for data selection override property ConditionSqlString 
         * Also you can implement your custom query by overriding properties CustomQuery and CustomCountQuery
         *  
         */
        #endregion

        #region Interface properties

        protected override IButtonControl[] DeleteButtons => new IButtonControl[] { };

        protected override System.Web.UI.HtmlControls.HtmlGenericControl NoRowsDiv => NoRows;

        protected override System.Web.UI.HtmlControls.HtmlGenericControl TableDiv => mainTableDiv;

        protected override string ContentName => resources.LNG_USERS;

        #endregion
    }
}