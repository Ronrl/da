<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<%
check_session()

Server.ScriptTimeout = 600
Conn.CommandTimeout = 600

intSessionUserId = Session("uid")
if (intSessionUserId <> "") then


intId=Clng(Request("id"))
mytype=Request("type")
csvData=""

'alert export
if(mytype="alert") then

	alert_id=intId

	Set RS = Conn.Execute("SELECT id, alert_text, type, aknown, sent_date, type2, sender_id  FROM alerts WHERE id=" & alert_id)
	if(Not RS.EOF) then

		strAlertDate=RS("sent_date")
	        strAlertPreview=RS("alert_text")
	        text=Replace(RS("alert_text"), "</p>", " ")
		text=DeleteTags(text)
		text=Left(text, 50)
		strAlertTitle = text



'show alerts statistics ---
  Set RSUsers = Conn.Execute("SELECT COUNT(DISTINCT id) as mycnt FROM users WHERE reg_date <= '" & strAlertDate & "'")
  if(not RSUsers.EOF) then 
	users_cnt=RSUsers("mycnt") 
  end if

  intSentAlertsNum=0
			if(RS("type2")="P") then 
			  Set RS7 = Conn.Execute("SELECT COUNT(DISTINCT user_id) as mycnt FROM [alerts_sent_stat] WHERE alert_id=" & alert_id)
			  if(not RS7.EOF) then	intSentAlertsNum=RS7("mycnt") end if
			end if

			if(RS("type2")="G") then 
				Set RSGroup = Conn.Execute("SELECT alerts_sent_group.group_id as group_id FROM alerts_sent_group INNER JOIN alerts ON alerts.id=alerts_sent_group.alert_id WHERE alerts_sent_group.alert_id=" & alert_id)
				Do While not RSGroup.EOF
					group_id=RSGroup("group_id")
					Set RSUsers = Conn.Execute("SELECT COUNT(DISTINCT users.id) as mycnt FROM users INNER JOIN users_groups ON users.id=users_groups.user_id WHERE users_groups.group_id="&group_id&" AND reg_date <= '" & strAlertDate & "'")
					if(not RSUsers.EOF) then 
						groupusers_cnt=RSUsers("mycnt") 
						group_cnt=group_cnt+groupusers_cnt
					end if
					RSGroup.MoveNext	
				loop
				intSentAlertsNum=group_cnt
			end if

			if(RS("type2")="B") then 
			  intSentAlertsNum=users_cnt
			end if

  intReceivedAlertsNum=0
  intClicksNum=0

  Set RS7 = Conn.Execute("SELECT COUNT(DISTINCT user_id) as mycnt FROM [alerts_received] WHERE alert_id=" & alert_id)
  if(not RS7.EOF) then	
	intReceivedAlertsNum=RS7("mycnt") 
  end if

  Set RS7 = Conn.Execute("SELECT COUNT(DISTINCT user_id) as mycnt FROM [alerts_read] WHERE alert_id=" & alert_id)

  if(not RS7.EOF) then	
	intAcknowNum=RS7("mycnt") 
  end if


  if(IsNull(intClicksNum)) then intClicksNum=0 end if
  if(IsNull(intReceivedAlertsNum)) then intReceivedAlertsNum=0 end if
  if(IsNull(intAcknowNum)) then intAcknowNum=0 end if


        alertsVal1Name="Received"
	alertsVal2Name="Acknowledged"
	alertsVal3Name="Not received"

        alertsVal1Value=CStr(intReceivedAlertsNum)
	alertsVal2Value=CStr(intAcknowNum)
	alertsVal3Value=CStr(intSentAlertsNum-intReceivedAlertsNum)
	totalAlertsValue=intSentAlertsNum

	strAlertTitle=replace(strAlertTitle, """", """""")
	strAlertDate=replace(strAlertDate, """", """""")

	csvData=csvData & " ""Alert title"",""Sent date"",""Total""," & """" & alertsVal1Name & """" & "," & """" & alertsVal2Name & """" & "," & """" & alertsVal3Name & """" 
	csvData=csvData & vbcrlf & """" & strAlertTitle & """" & "," & """" & strAlertDate & """" & "," & """" & totalAlertsValue & """" & "," & """" & alertsVal1Value & """" & "," & """" & alertsVal2Value & """" & "," & """" & alertsVal3Value & """"
  end if	

end if


'user export
if(mytype="user") then
end if

'survey export
if(mytype="survey") then
end if

'survey_results export
if(mytype="survey_results") then
end if


WriteToFile alerts_dir & "admin\csv\" & mytype & intId & ".csv", "", False
WriteToFile alerts_dir & "admin\csv\" & mytype & intId & ".csv", csvData, True

response.redirect "csv/"&mytype&intId&".csv"

else
	Login(Request("intErrorCode"))
end if
%><!-- #include file="db_conn_close.asp" -->