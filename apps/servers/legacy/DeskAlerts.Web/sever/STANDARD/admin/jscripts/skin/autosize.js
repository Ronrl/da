var OnPageLoadOld = window.OnPageLoad;

window.OnPageLoad = function()
{
	
	OnPageLoadOld();
	autosize(document.getElementById('title'));
	autosize(document.getElementById('create'));
	var title = window.external.getProperty("title");
	if(title)
	{
		setTitle(title);
	}
}

function autosize(element)
{
	try
	{
		var contentTable = document.getElementById("contentTable");
		if (element.getAttribute("autowidth") == "true" && element.style.position == "absolute")
		{
		
			var width = contentTable.clientWidth;
			
			var left = parseInt(element.style.left);
			var right = parseInt(element.style.right);
			width = width - left - right;
			if (width < 0) width = 0;
			element.style.width= width +"px";
			
			element.style.left = left +"px"
			element.style.right = "auto"
		
		}
		else
		{
			if (element.getAttribute("autoright") == "true")
			{
				element.style.right = "auto"
			}
			else if (element.getAttribute("autoleft") == "true")
			{	
				element.style.left = "auto"
			}
		
		}
	}
	catch(e){}
}