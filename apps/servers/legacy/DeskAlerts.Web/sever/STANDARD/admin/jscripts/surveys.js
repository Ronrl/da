function Survey(){
  this.elem_num = 0;
  this.elements = new Array();
  this.name="";
  this.template_id="";
}
	
function add_survey_methods(survey){		
  survey.add_elem = function(el){this.elements.push(el); this.elem_num=this.elem_num+1;};
  survey.get_first_elem = function() {return this.elements[0];};
  survey.replace_elems = function(id1,id2) {var x=this.elements[id1]; this.elements[id1]=this.elements[id2]; this.elements[id2]=x;}
  survey.removeElement = function(id1) {this.elements.splice(id1,1); this.elem_num=this.elem_num-1;}		
  survey.add_elem_after = function(el, index){		
    this.elem_num=this.elem_num+1;
    var newarr = this.elements.splice(parseInt(index)+1,0,el);
    };
}

function Question(){
  this.elem_num = 0;
  this.elements = new Array();
  this.name="";
  this.type="M";
}

function add_question_methods(question){		
  question.add_elem = function(el){this.elements.push(el); this.elem_num=this.elem_num+1;};
  question.get_first_elem = function() {return this.elements[0];};
  question.replace_elems = function(id1,id2) {var x=this.elements[id1]; this.elements[id1]=this.elements[id2]; this.elements[id2]=x;}
  question.removeElement = function(id1) {this.elements.splice(id1,1); this.elem_num=question.elem_num-1;}		
  question.removeAllElements = function() {this.elements = new Array(); this.elem_num = 0;}
  question.add_elem_after = function(el, index){		
    question.elem_num=this.elem_num+1;
    var newarr = this.elements.splice(parseInt(index)+1,0,el);
    };
}

function Answer(){
  this.name="";
  this.correct=false;
}

