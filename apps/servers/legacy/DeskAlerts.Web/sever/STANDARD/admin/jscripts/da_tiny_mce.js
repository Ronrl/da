function initTinyMCE(lang, mode, id, callback, add_to_settings, replace_in_settings)
{
	var cur_lang = 'en';
	lang = lang.toLowerCase();
	var possible_langs = ['ar','az','be','bg','bn','br','bs','ca','ch','cn','cs','cy','da','de','dv','el','en','eo','es','et','eu','fa','fi','fr','gl','gu','he','hi','hr','hu','hy','ia','id','is','it','ja','ka','kk','kl','km','ko','kz','lb','lt','lv','mk','ml','mn','ms','my','nb','nl','nn','no','pl','ps','pt','ro','ru','sc','se','si','sk','sl','sq','sr','sv','sy','ta','te','th','tn','tr','tt','tw','uk','ur','vi','zh-cn','zh-tw','zh','zu'];
	for(var i=0; i < possible_langs.length; i++)
	{
		if(possible_langs[i] == lang)
		{
			cur_lang = lang;
			break;
		}
	}
	var settings = {
		mode: mode,
		elements: id,
		language: cur_lang,
		theme: "advanced",
        plugins: "layer,table,save,advhr,advimage,advlink,emotions,insertdatetime,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,style,iframe,media,autolink",
		theme_advanced_buttons1: "bold,italic,underline,|,justifyleft,justifycenter,justifyright,justifyfull,|,formatselect,fontselect,fontsizeselect,|,forecolor,backcolor,|,ical",
		theme_advanced_buttons2: "cut,copy,paste,|,bullist,numlist,|,outdent,indent,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,emotions,|,advhr,|,da_insert_flash,da_insert_video,da_insert_document,|,insertdate,inserttime,|,media,iframe,da_record_video",
		theme_advanced_buttons3: "insertlayer,moveforward,movebackward,absolute,|,ltr,rtl,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,|,table,|,row_props,cell_props,|,row_before,row_after,delete_row,|,col_before,col_after,delete_col,|,split_cells,merge_cells,|,styleprops,|,myclear,|,fullscreen",
		theme_advanced_buttons4: "",
		theme_advanced_toolbar_location: "top",
		theme_advanced_toolbar_align: "left",
		theme_advanced_path_location: "bottom",
		content_css: "preview/skin/version/skin.css",
		popup_css_add: "css/tiny_mce_dlg.css",
		plugin_insertdate_dateFormat: "%Y-%m-%d",
		plugin_insertdate_timeFormat: "%H:%M:%S",
		extended_valid_elements: "*[*]",
		external_link_list_url: "example_link_list.js", //empty
		external_image_list_url: "example_image_list.js",
		flash_external_list_url: "example_link_list.js", //empty
		file_browser_callback: "fileBrowserCallBack",
		theme_advanced_resize_horizontal: false,
		theme_advanced_resizing: true,
		style_formats: [{ title: '', selector: '', classes: ''}],
		theme_advanced_styles: "Normal=",
		advlink_styles: "Normal=",
		theme_advanced_disable: "styleselect",
		advimage_update_dimensions_onchange: true,
		relative_urls: true,
		convert_urls: false,
		convert_fonts_to_spans: true,
		theme_advanced_font_sizes: "8pt=8pt,10pt=10pt,12pt=12pt,14pt=14pt,18pt=18pt,24pt=24pt,36pt=36pt",
		font_size_style_values: "8pt,10pt,12pt,14pt,18pt,24pt,36pt",
		setup: function(ed) {
			// Add a custom button
			ed.addButton('da_insert_flash', {
				title: 'Insert flash',
				image: 'img/insert_flash.gif',
				onclick: function() {
					ed.windowManager.open({
						url: 'uploadform_flash.asp?id=' + browse_windows_actions.length,
						width: 400,
						height: 500,
						inline: true,
						scrollbars: true
					}, {
						theme_url: this.url
					});
					browse_windows_actions.push(function(name, src, width, height) {
						ed.execCommand("mceInsertContent", false, ' %flash%=|' + src + '|' + width + '|' + height + '|');
					});
				}
			});
			ed.addButton('da_insert_video', {
				title: 'Insert video',
				image: 'img/insert_video.gif',
				onclick: function() {
					ed.windowManager.open({
						url: 'uploadform_video.asp?id=' + browse_windows_actions.length,
						width: 400,
						height: 550,
						inline: true,
						scrollbars: true
					}, {
						theme_url: this.url
					});
					browse_windows_actions.push(function(name, src, width, height) {
						ed.execCommand("mceInsertContent", false, ' %video%=|' + src + '|' + width + '|' + height + '|');
					});
				}
			});
			ed.addButton('da_record_video', {
				title: 'Record video',
				image: 'img/webcam.gif',
				onclick: function() {
					ed.windowManager.open({
						url: 'video_recorder.asp',
						width: 400,
						height: 550,
						inline: true,
						scrollbars: true
					}, {
						theme_url: this.url
					});
					browse_windows_actions.push(function(name, src, width, height) {
						ed.execCommand("mceInsertContent", false, ' %video%=|' + src + '|' + width + '|' + height + '|');
					});
				}
			});
			ed.addButton('da_insert_document', {
				title: 'Insert document',
				image: 'img/insert_document.gif',
				onclick: function() {
					ed.windowManager.open({
						url: 'uploadform_document.asp?id=' + browse_windows_actions.length,
						width: 400,
						height: 500,
						inline: true,
						scrollbars: true
					}, {
						theme_url: this.url
					});
					browse_windows_actions.push(function(name, src, width, height) {
						var html = ' <a href="' + src + '" target="_blank">' + name + '</a>';
						ed.execCommand("mceInsertContent", false, html);
					});
				}
			});
			ed.addButton('myclear', {
				title: 'Clear All',
				image: 'images/clear.gif',
				onclick: function() {
					ed.setContent('');
				}
			});
			ed.addButton('ical', {
				title: 'iCalendar',
				image: 'images/ical.gif',
				onclick: function() {
					ed.windowManager.open({
						url: 'icalendars.asp?id=' + browse_windows_actions.length,
						width: 420,
						height: 310,
						inline: true,
						scrollbars: true
					}, {
						theme_url: this.url
					});
					browse_windows_actions.push(function(name, src, width, height) {
						var html = ' <a href="' + src + '" target="_blank">' + name + '</a>';
						ed.execCommand("mceInsertContent", false, html);
					});
				}
			});
			ed.onInit.add(function() {
				var link_cmd = ed.execCommands['mceAdvLink'];
				if (link_cmd) {
					var old_link_func = link_cmd.func;
					link_cmd.func = function() {
						var old_sel = ed.selection;
						ed.selection = { isCollapsed: function() { return false; }, getNode: function() { return null; } };
						old_link_func();
						ed.selection = old_sel;
					}
				}
				ed.onNodeChange.add(function(ed, cm, n, co) {
					cm.setDisabled('link', false);
					setTimeout(function() {
						cm.setDisabled('link', false);
					}, 1);
				});
				
				/*ed.onNodeChange.add(function(ed, cm, n, co) {
					cm.setDisabled('da_record_video', false);
					setTimeout(function() {
						cm.setDisabled('da_record_video', false);
					}, 1);
				});*/
				
				var dom = tinymce.DOM;

				var icon = dom.select('span.mce_ical', dom.get(ed.id + '_tbl'))[0];
				if (icon)
				{
					dom.setStyles(icon, { 'width': '90px', 'background-image': 'none', 'vertical-align': 'middle' });

					var parent = dom.getParent(icon, 'a');
					dom.setAttrib(parent, 'title', 'insert iCalendar link for MS Outlook/Google Calendar/Apple Calendar/Lotus Notes/etc.');
					dom.setStyles(parent, { 'width': '90px', 'vertical-align': 'middle' });

					parent = dom.getParent(icon, 'td');
					dom.setStyles(parent, { 'width': '90px', 'vertical-align': 'middle' });
					dom.setAttrib(parent, 'valign', 'middle');

					dom.setStyles(dom.select('img', icon)[0], { 'display': 'inline', 'vertical-align': 'middle', 'margin-right': '2px' });
					dom.add(icon, dom.doc.createTextNode('iCalendar'));
				}

				var link = dom.get(ed.id + '_link');
				
				if (link)
				{
					dom.setStyles(link, { 'width': '50px', 'vertical-align': 'middle' });
					dom.setAttrib(link, 'title', dom.getAttrib(link, 'title') + ' (URL/Document/Shared Point)');

					parent = dom.getParent(link, 'td');
					dom.setStyles(parent, { 'width': '50px', 'vertical-align': 'middle' });
					dom.setAttrib(parent, 'valign', 'middle');

					var text = dom.add(link, 'span', { 'class': 'mce_link' }, 'Link');
					dom.add(text, 'img', { 'src': 'images/inv.gif', 'width': '0px', 'height': '20px', 'border': '0', 'style': 'vertical-align:middle; width:0px; height: 20px' });
					dom.setStyles(text, { 'width': '30px', 'display': 'inline', 'vertical-align': 'middle' });

					icon = dom.select('span.mceIcon', link)[0];
					setTimeout(function() {
						var bgpos = dom.getStyle(icon, 'background-position', true);
						if (bgpos) {
							bgpos = bgpos.replace(/px|%/g, '').split(' ');
							dom.setStyles(icon, { 'background-position': bgpos[0] + 'px ' + (bgpos[1] - 2) + 'px' });
						}
					}, 1);
					dom.setStyles(icon, { 'width': '20px', 'height': '20px', 'display': 'inline', 'vertical-align': 'middle' });
					dom.add(icon, dom.doc.createTextNode("\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0"));
				}
				/*var da_record_video = dom.get(ed.id + '_da_record_video');
				if (da_record_video)
				{
					dom.setStyles(da_record_video, { 'width': '50px', 'vertical-align': 'middle' });
					dom.setAttrib(da_record_video, 'title', dom.getAttrib(da_record_video, 'title') + ' (Record video)');

					parent = dom.getParent(da_record_video, 'td');
					dom.setStyles(parent, { 'width': '50px', 'vertical-align': 'middle' });
					dom.setAttrib(parent, 'valign', 'middle');

					var text = dom.add(da_record_video, 'span', { 'class': 'mce_link' }, 'Webcam');
					dom.add(text, 'img', { 'src': 'images/webcam.gif', 'width': '0px', 'height': '20px', 'border': '0', 'style': 'vertical-align:middle; width:0px; height: 20px' });
					dom.setStyles(text, { 'width': '30px', 'display': 'inline', 'vertical-align': 'middle' });

					icon = dom.select('span.mceIcon', da_record_video)[0];
					setTimeout(function() {
						var bgpos = dom.getStyle(icon, 'background-position', true);
						if (bgpos) {
							bgpos = bgpos.replace(/px|%/g, '').split(' ');
							dom.setStyles(icon, { 'background-position': bgpos[0] + 'px ' + (bgpos[1] - 2) + 'px' });
						}
					}, 1);
					dom.setStyles(icon, { 'width': '20px', 'height': '20px', 'display': 'inline', 'vertical-align': 'middle' });
					dom.add(icon, dom.doc.createTextNode("\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0"));
				}*/
			});
			if (callback)
				ed.onInit.add(callback);
		}
	};
	if(add_to_settings)
	{
		tinymce.each(add_to_settings, function(value, name) {
			if(settings[name])
				settings[name] += ',' + value;
			else
				settings[name] = value;
		});
	}
	if(replace_in_settings)
	{
		tinymce.each(replace_in_settings, function(value, name) {
				settings[name] = value;
		});
	}
	tinyMCE.init(settings);
}
var browse_windows_actions = [];
function fileBrowserSetSrc(id, name, src, width, height)
{
	var action = browse_windows_actions[id];
	if(action) action(name, src, width, height);
}
function fileBrowserCallBack(field_name, url, type, win)
{
	window.open('../../../../uploadform.asp?id='+browse_windows_actions.length,'','left=100,right=100,width=400,height=500,scrollbars=yes');
	if(type == 'image' && field_name == 'src')
	{
		browse_windows_actions.push(function(name, src, width, height){
			win.document.getElementsByName(field_name)[0].value = src;
			var alt = win.document.getElementsByName('alt')[0];
			if(alt && !alt.value) alt.value = name;

			var title = win.document.getElementsByName('title')[0];
			if(title && !title.value) title.value = name;

			if(win.ImageDialog.getImageData)
				win.ImageDialog.getImageData();
			if(win.ImageDialog.showPreviewImage)
				win.ImageDialog.showPreviewImage(src);
			win.focus();
		});
	}
	else
	{
		browse_windows_actions.push(function(name, src, width, height){
			win.document.getElementsByName(field_name)[0].value = src;
			win.focus();
		});
	}
}
