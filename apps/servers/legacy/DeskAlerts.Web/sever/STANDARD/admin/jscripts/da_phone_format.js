function normalize(number, normalizationReplacements, dontReplace, removeNonMatches, removeLeadingPlusChars) {
  /** @type {!goog.string.StringBuffer} */
  var normalizedNumber = new goog.string.StringBuffer();
  /** @type {string} */
  var character;
  /** @type {string} */
  var newDigit;
  /** @type {number} */
  var numberLength = number.length;
  /** @type {array} */
  var plusMatch = i18n.phonenumbers.PhoneNumberUtil.LEADING_PLUS_CHARS_PATTERN_.exec(number);
  if (plusMatch) {
    if (!removeLeadingPlusChars && removeNonMatches) {
      normalizedNumber.append(plusMatch[0]);
    }
  }
  for (var i = 0; i < numberLength; ++i) {
    character = number.charAt(i);
    newDigit = normalizationReplacements[character.toUpperCase()];
    if (newDigit != null) {
      if (dontReplace)
        normalizedNumber.append(character);
      else
        normalizedNumber.append(newDigit);
    } else if (!removeNonMatches) {
      normalizedNumber.append(character);
    }
  }
  return normalizedNumber.toString();
}
function format_number(number, format, country) {
// "0" - Don't convert phone numbers,
// "1" - Just remove all special chars but not grouping symbols,
// "2" - Just remove all special chars but not grouping symbols and not lead plus "+" symbol,
// "3" - Just remove all special chars,
// "4" - Just remove all special chars but not lead plus "+" symbol,
// "5" - Convert letters to numeric,
// "6" - Convert letters to numeric and remove all special chars,
// "7" - Convert letters to numeric and remove all special chars but not lead plus "+" symbol,
// "8" - Use international E.164 phone format,
// "9" - Use international E.164 phone format but without lead plus "+" symbol.
  switch(format) {
    case '1': return normalize(number, i18n.phonenumbers.PhoneNumberUtil.ALL_PLUS_NUMBER_GROUPING_SYMBOLS_, true, true, true);
    case '2': return normalize(number, i18n.phonenumbers.PhoneNumberUtil.ALL_PLUS_NUMBER_GROUPING_SYMBOLS_, true, true, false);
    case '3': return normalize(number, i18n.phonenumbers.PhoneNumberUtil.ALL_NORMALIZATION_MAPPINGS_, true, true, true);
    case '4': return normalize(number, i18n.phonenumbers.PhoneNumberUtil.ALL_NORMALIZATION_MAPPINGS_, true, true, false);
    case '5': return normalize(number, i18n.phonenumbers.PhoneNumberUtil.ALL_NORMALIZATION_MAPPINGS_, false, false, false);
    case '6': return normalize(number, i18n.phonenumbers.PhoneNumberUtil.ALL_NORMALIZATION_MAPPINGS_, false, true, true);
    case '7': return normalize(number, i18n.phonenumbers.PhoneNumberUtil.ALL_NORMALIZATION_MAPPINGS_, false, true, false);
    case '8': return normalize(formatE164(country, normalize(number, i18n.phonenumbers.PhoneNumberUtil.ALL_NORMALIZATION_MAPPINGS_, false, true, false)), i18n.phonenumbers.PhoneNumberUtil.ALL_NORMALIZATION_MAPPINGS_, false, true, false);
    case '9': return normalize(formatE164(country, normalize(number, i18n.phonenumbers.PhoneNumberUtil.ALL_NORMALIZATION_MAPPINGS_, false, true, false)), i18n.phonenumbers.PhoneNumberUtil.ALL_NORMALIZATION_MAPPINGS_, false, true, true);
  }
  return number;
}