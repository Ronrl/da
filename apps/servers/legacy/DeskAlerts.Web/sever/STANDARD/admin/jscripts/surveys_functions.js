function delete_answer(){
	this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode);
}

function delete_question(){
	var this_question_number = parseInt(document.getElementById("question_number").value);
	survey.removeElement(this_question_number);
	document.getElementById("question_number").value=this_question_number-1;
	var string = JSON.stringify(survey);
	document.getElementById("survey_data").value=string;
	document.getElementById("type").value="prev";
	document.getElementById("frm").submit();
}

function check_values(){
	//survey name
	if(document.getElementById("sur_name").value.length < 1){
		return false;
	}
	//question
	if(document.getElementById("question").value.length < 1){
		return false;
	}
	//answers
//	var answersForm = document.getElementsByName("answer"); - not working in IE6 and IE7 for dynamicly created inputs
	var answersForm = document.getElementsByTagName("input");
	for(var i=0; i<answersForm.length; i++){
		if(answersForm[i].name=="answer"){
			if(answersForm[i].value.length < 1){	
				return false;
			}
		}
	}
	return true;
}

function check_valid_dates(survey)
{
	if(survey.schedule)
	{
		var startHour=survey.start_hour;
		var endHour=survey.end_hour;

		if(survey.start_half=="am")
		{
			if(startHour==12) {startHour = 0;}
		}
		else
		{
			if(startHour != 12) {startHour = 12+parseInt(startHour);}

		}

		if(survey.end_half=="am")
		{
			if(endHour==12) {endHour = 0;}
		}
		else
		{
			if(endHour!=12) {endHour = 12+parseInt(endHour);}
		}
		
		var startDate = new Date;
		var endDate = new Date;

		startDate.setDate(survey.start_date.split("/")[1]);
		startDate.setMonth(survey.start_date.split("/")[0]-1);
		startDate.setFullYear(survey.start_date.split("/")[2]);
		startDate.setHours(startHour);
		startDate.setMinutes(survey.start_min);

		endDate.setDate(survey.end_date.split("/")[1]);
		endDate.setMonth(survey.end_date.split("/")[0]-1);
		endDate.setFullYear(survey.end_date.split("/")[2]);
		endDate.setHours(endHour);
		endDate.setMinutes(survey.end_min);

		if(startDate > endDate) 
		{
			alert("Please enter correct start and end date/time!")
			return false;
		}
	}
	return true;
}

function my_submit(type){
	if(check_values()){
		survey.name=document.getElementById("sur_name").value;

		survey.template_id=document.getElementById("template").value;

		var schedule = document.getElementById("schedule");
		survey.schedule		= schedule.type.toLowerCase() != "checkbox" || schedule.checked ? schedule.value : 0;
		survey.start_date	= document.getElementById("start_date_sch").value;
		survey.start_hour	= document.getElementById("start_hour_sch").value;
		survey.start_min	= document.getElementById("start_min_sch").value;
		survey.start_half	= document.getElementById("start_half_sch").value;
		survey.end_date		= document.getElementById("end_date_sch").value;
		survey.end_hour		= document.getElementById("end_hour_sch").value;
		survey.end_min		= document.getElementById("end_min_sch").value;
		survey.end_half		= document.getElementById("end_half_sch").value;

		if(survey.schedule == 1)
		{
			if(survey.start_date)
			{
				if(survey.end_date)
				{
					if(!check_valid_dates(survey)) return;
				}
				else
				{
					alert("Please enter end date.");
					return;
				}
			}
			else
			{
					alert("Please enter start date.");
					return;
			}
		}
		var this_question_number = parseInt(document.getElementById("question_number").value);
		var question;
		if(survey.elements[this_question_number]){
			question=survey.elements[this_question_number];
		}
		else{
			question = new Question();
			survey.add_elem(question);
		}
		add_question_methods(question);
		question.name=document.getElementById("question").value;
		question.type=document.getElementById("question_type").value;
		question.removeAllElements();
//	var answersForm = document.getElementsByName("answer"); - not working in IE6/IE7/IE8 for dynamicly created inputs
		var answersForm = document.getElementsByTagName("input");
		var answer;
		for(var i=0; i<answersForm.length; i++){
		    if (answersForm[i].name == "answer"){
			        answer = new Answer();
			        question.add_elem(answer);
			        answer.name = answersForm[i].value;
			}
			if (answersForm[i].name == "correct"){
			    if (correctForm[i].checked) {
			        answer.correct = correctForm[i].checked;
			    }
			}
				
		}

		document.getElementById("type").value=type;

		if(type=="add"){
			document.getElementById("question_number").value=this_question_number+1;
			var new_question = new Question;
	                survey.add_elem_after(new_question,this_question_number);
		}
		if(type=="prev"){
			document.getElementById("question_number").value=this_question_number-1;
		}
		if(type=="next"){
			if((this_question_number+1) < survey.elements.length){
				document.getElementById("question_number").value=this_question_number+1;
			}
			else{
				document.getElementById("type").value="select_users";
			}
		}
		var string = JSON.stringify(survey);
		document.getElementById("survey_data").value=string.replace(/'/g,"\\'");
		document.getElementById("frm").submit();
	}
	else{
		alert('Please fill all empty fields.');
	}
}

function insert_new_answer(answer_value){
	//insert new input for answer
	var questionType=document.getElementById('question_type').value;
	var question_row = (document.getElementById('question_tr').rowIndex+1);
	

	
	var tbl = document.getElementById('survey_table');
	var lastRow = tbl.rows.length;
	var row = tbl.insertRow(lastRow);
	var cellLeft = row.insertCell(0);
	var textNode = document.createTextNode('Answer:');
	cellLeft.appendChild(textNode);
  
	var cellMiddle = row.insertCell(1);
	var el = document.createElement('input');
	el.type = 'text';
	el.name = 'answer';
	el.size = 35;
	el.value = answer_value;
	cellMiddle.appendChild(el);
	el.focus();

	var cellRight = row.insertCell(2);
	if(questionType=="C"){
		var el_span = document.createElement('span');
		el_span.style.font="11ppt";
		el_span.style.color="gray";
		if(answer_value == "Yes"){
			el_span.innerHTML = 'This answer will proceed the survey';
		}
		else{
			el_span.innerHTML = 'This answer will finish the survey';		
		}
		cellRight.appendChild(el_span);		
	}
	else{
		if(lastRow>question_row+1){
			var el = document.createElement('img');
			el.src = 'images/action_icons/delete.gif';
			el.name = 'answer_delete';
			el.border="0";
			el.onclick=delete_answer;
			cellRight.appendChild(el);
		}
	}
	
}

function fill_the_form(){
	document.getElementById("sur_name").value=survey.name;
	var this_question_number = parseInt(document.getElementById("question_number").value);
	if(survey.elements[this_question_number]){
		var question=survey.elements[this_question_number];
		add_question_methods(question);
		document.getElementById("question").value=question.name;

		var qt = document.getElementById("question_type");
		for(var i=0; i < qt.options.length; i++)
		{
			if(qt.options[i].value==question.type)
			{
				qt.options[i].selected=true;
				break;
			}
		}
		changeQuestionType();

		var answersTagForm = document.getElementsByTagName("input");
		var answersForm = [];
		var correctForm = []; 
		for(var k=0; k<answersTagForm.length; k++){
			if(answersTagForm[k].name=="answer"){
				answersForm.push(answersTagForm[k]);
			}
			if(answersTagForm[k].name=="correct"){
				correctForm.push(answersTagForm[k]);
			}
		}
		for(var i=0; i<question.elements.length; i++){
			if(i<answersForm.length){
				answersForm[i].value=question.elements[i].name;
				if(correctForm.length!==0) {
						if (question.elements[i].correct === "1") correctForm[i].checked = true;
						else if (question.elements[i].correct === "0") correctForm[i].checked = false;
						else correctForm[i].checked = question.elements[i].correct;
						
						}
			}
			else{
				insert_new_answer(question.elements[i].name);
			}
		}
	}
}

function changeQuestionType()
{
	//clear all answer rows
	var tbl = document.getElementById('survey_table');
	var lastRow = tbl.rows.length;
	var question_row = (document.getElementById('question_tr').rowIndex+1);
	for(; lastRow>question_row; ){
		tbl.deleteRow(question_row);
		lastRow = tbl.rows.length;
	}
	var insert_answer_div = document.getElementById('insert_answer_div');
	var input_answer_desc_div = document.getElementById('input_answer_desc_div');
	
	var questionType=document.getElementById('question_type').value;
	switch(questionType)
	{
	case "M":
	case "N":
	case "T":
		insert_new_answer("",false);
		insert_new_answer("",false);
		insert_answer_div.style.display = "";
		input_answer_desc_div.style.display = "none";
	break;
	case "C":
		insert_new_answer("Yes",false);
		insert_new_answer("No",false);
		insert_answer_div.style.display = "none";
		input_answer_desc_div.style.display = "none";
	break;
	case "I":
		insert_answer_div.style.display = "none";
		input_answer_desc_div.style.display = "";
	break;
	}	

}