var IframeDialog = {
	init : function(ed) {
		var dom = ed.dom, f = document.forms[0], n = ed.selection.getNode(), w, h, b, hs, vs, a, bgc, id, name;

		w = dom.getAttrib(n, 'width');
		h = dom.getAttrib(n, 'height');

		var style = dom.getAttrib(n, 'data-mce-style');
		if (!style) {
			style = dom.getAttrib(n, 'style');

			if (style)
				style = ed.dom.serializeStyle(ed.dom.parseStyle(style, 'img'));
		}

		var data = dom.getAttrib(n, 'data-mce-json');
		if(data)
		{
			data = tinymce.util.JSON.parse(data);
			f.url.value = data.params.src;
			f.scroll.value = data.params.scrolling;
			b = data.params.frameborder;
			hs = data.params.hspace;
			vs = data.params.vspace;
			a = data.params.align;
			bgc = data.params.bgcolor;
			id = data.params.id;
			name = data.params.name;
		}
		else
		{
			f.url.value = dom.getAttrib(n, 'src');
			f.scroll.value = dom.getAttrib(n, 'scrolling');
			b = dom.getAttrib(n, 'frameborder');
			hs = dom.getAttrib(n, 'hspace');
			vs = dom.getAttrib(n, 'vspace');
			a = dom.getAttrib(n, 'align');
			bgc = dom.getAttrib(n, 'bgcolor');
			id = dom.getAttrib(n, 'bgcolor');
			name = dom.getAttrib(n, 'name');
		}
		f.width.value = w ? parseInt(w) : '';
		selectByValue(f, 'width2', w.indexOf('%') != -1 ? '%' : '');
		f.height.value = h ? parseInt(h) : '';
		selectByValue(f, 'height2', h.indexOf('%') != -1 ? '%' : '');
		if (f.scroll.value!='yes' && f.scroll.value!='no') f.scroll.value = 'auto';
		selectByValue(f, 'border', b.indexOf('0') != -1 ? '0' : '1');
		
		document.getElementById('e_id').value = id;
		document.getElementById('style').value = style;
		document.getElementById('hspace').value = hs;
		document.getElementById('vspace').value = vs;
		document.getElementById('align').value = a;
		document.getElementById('bgcolor').value = bgc;
		document.getElementById('id').value = id;
	},

	update : function() {
		var ed = tinyMCEPopup.editor, h, f = document.forms[0];

		var dataToImg = function(data)
		{
			var convertUrl = function(url)
			{
				var settings = ed.settings,
					urlConverter = settings.url_converter,
					urlConverterScope = settings.url_converter_scope;

				if (!url)
					return url;

				return urlConverter.call(urlConverterScope, url, 'src', 'object');
			};

			if (data.params.src)
				data.params.src = convertUrl(data.params.src);

			var img = ed.dom.create('img', {
				id : data.id,
				'class' : 'mceItem mceIframeItem',
				style : data.params.style,
				width : data.params.width || '320',
				height : data.params.height || '240',
				hspace : data.params.hspace,
				vspace : data.params.vspace,
				align : data.params.align,
				bgcolor : data.params.bgcolor,
				'data-mce-json' : tinymce.util.JSON.serialize(data, "'")
			});

			return img;
		};

		var data = {
			type : 'iframe',
			video : {sources:[]},
			params : {
				src : f.url.value,
				name : f.name.value,
				width : f.width.value + (f.width2.value == '%' ? '%' : ''),
				height : f.height.value + (f.height2.value == '%' ? '%' : ''),
				scrolling : f.scroll.value,
				frameborder : f.border.value,
				style : f.style.value,
				hspace : f.hspace.value,
				vspace : f.vspace.value,
				align : f.align.value,
				bgcolor : f.bgcolor.value
			}
		}

		ed.execCommand('mceRepaint');
		tinyMCEPopup.restoreSelection();
		ed.selection.setNode(dataToImg(data));
		tinyMCEPopup.close();
	}
};

tinyMCEPopup.requireLangPack();
tinyMCEPopup.onInit.add(IframeDialog.init, IframeDialog);
