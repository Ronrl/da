/**
 * $Id: editor_plugin_src.js 520 2008-01-07 16:30:32Z spocke $
 *
 * @author Moxiecode
 * @copyright Copyright � 2004-2008, Moxiecode Systems AB, All rights reserved.
 */

(function() {
	tinymce.PluginManager.requireLangPack('iframe');
	tinymce.create('tinymce.plugins.IframePlugin', {
		init : function(ed, url) {
			var rootAttributes = tinymce.explode('id,name,width,height,style,align,class,hspace,vspace,bgcolor,type');
			var excludedAttrs = tinymce.makeMap(rootAttributes.join(','));

			var isIframeImg = function(node) {
				return node && node.nodeName === 'IMG' && (ed.dom.hasClass(node, 'mceIframeItem') || ed.dom.hasClass(node, 'mceItemIframe'));
			};
			// Register commands
			ed.addCommand('mceIframe', function() {
				ed.windowManager.open({
					file : url + '/iframe.htm',
					width : 280 + parseInt(ed.getLang('iframe.delta_width', 0)),
					height : 250 + parseInt(ed.getLang('iframe.delta_height', 0)),
					inline : 1
				}, {
					plugin_url : url
				});
			});

			// Register buttons
			ed.addButton('iframe', {
				title : 'iframe.desc',
				cmd : 'mceIframe',
				image : url + '/img/iframe.png'
			});

			ed.onNodeChange.add(function(ed, cm, n) {
				cm.setActive('iframe', (n.nodeName === 'IFRAME' || isIframeImg(n)));
			});

			ed.onInit.add(function() {
				ed.dom.loadCSS(url + '/css/iframe.css');
				// Display "iframe" instead of "img" in element path
				if (ed.theme && ed.theme.onResolveName) {
					ed.theme.onResolveName.add(function(theme, path_object) {
						if (path_object.name === 'img' && ed.dom.hasClass(path_object.node, 'mceIframeItem'))
							path_object.name = 'iframe';
					});
				}
			});

			var imgToIframe = function(node, args)
			{
				var data = node.attr('data-mce-json');
				if (!data)
					return;
				data = tinymce.util.JSON.parse(data);

				var style = node.attr('data-mce-style');
				if (!style) {
					style = node.attr('style');

					if (style)
						style = ed.dom.serializeStyle(ed.dom.parseStyle(style, 'img'));
				}

				// Use node width/height to override the data width/height when the placeholder is resized
				data.width = node.attr('width') || data.width;
				data.height = node.attr('height') || data.height;

				var replacement = new tinymce.html.Node('iframe', 1);

				tinymce.each(rootAttributes, function(name) {
					var value = node.attr(name);

					if (name == 'class' && value)
						value = value.replace(/mceItem.+ ?/g, '');

					if (value && value.length > 0)
						replacement.attr(name, value);
				});

				for (name in data.params)
					replacement.attr(name, data.params[name]);

				replacement.attr({
					style: style,
					src: data.params.src
				});

				node.replace(replacement);
			};

			var iframeToImg = function(node)
			{
				var normalizeSize = function(size) {
					return typeof(size) == "string" ? size.replace(/[^0-9%]/g, '') : size;
				};
				var urlConverter = ed.settings.url_converter;
				var urlConverterScope = ed.settings.url_converter_scope;

				// If node isn't in document
				if (!node.parent)
					return;

				// Setup data objects
				var data = {
					params : {}
				};

				// Setup new image object
				var img = new tinymce.html.Node('img', 1);
				img.attr({
					src : ed.theme.url + '/img/trans.gif'
				});

				// Get width/height, etc.
				var name = node.name;
				var width = normalizeSize(node.attr('width'));
				var height = normalizeSize(node.attr('height'));
				var style = style || node.attr('style');
				var id = node.attr('id');
				var hspace = node.attr('hspace');
				var vspace = node.attr('vspace');
				var align = node.attr('align');
				bgcolor = node.attr('bgcolor');

				tinymce.each(rootAttributes, function(name) {
					img.attr(name, node.attr(name));
				});

				// Get all iframe attributes
				for (name in node.attributes.map) {
					if (!excludedAttrs[name] && !data.params[name])
						data.params[name] = node.attributes.map[name];
				}

				// Convert the URL to relative/absolute depending on configuration
				if (data.params.src)
					data.params.src = urlConverter.call(urlConverterScope, data.params.src, 'src', 'object');

				// Replace the iframe element with a placeholder image containing the data
				node.replace(img);

				data.hspace = hspace;
				data.vspace = vspace;
				data.align = align;
				data.bgcolor = bgcolor;

				// Set width/height of placeholder
				img.attr({
					id : id,
					'class' : 'mceItem mceIframeItem',
					style : style,
					width : width || '320',
					height : height || '240',
					hspace : hspace,
					vspace : vspace,
					align : align,
					bgcolor : bgcolor,
					'data-mce-json' : tinymce.util.JSON.serialize(data, "'")
				});
			};

			ed.onPreInit.add(function() {
				// Convert iframe elements to image placeholder
				ed.parser.addNodeFilter('iframe', function(nodes) {
					if(tinymce.plugins.MediaPlugin) return;
					var i = nodes.length;
					while(i--)
						iframeToImg(nodes[i]);
				});
				// Convert image placeholders to iframe elements
				ed.serializer.addNodeFilter('img', function(nodes, name, args) {
					var i = nodes.length, node;
					while (i--) {
						node = nodes[i];
						if ((node.attr('class') || '').indexOf('mceIframeItem') !== -1)
							imgToIframe(node, args);
					}
				});
			});
		},

		getInfo : function() {
			return {
				longname : 'iframe',
				author : 'Fusion InterMedia',
				authorurl : 'http://www.fusionintermedia.com',
				infourl : 'http://www.fusionintermedia.com',
				version : tinymce.majorVersion + "." + tinymce.minorVersion
			};
		}
	});

	// Register plugin
	tinymce.PluginManager.add('iframe', tinymce.plugins.IframePlugin);
})();