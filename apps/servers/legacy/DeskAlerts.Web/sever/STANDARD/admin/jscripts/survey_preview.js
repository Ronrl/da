function getSurveyHTML(index)
{
	var surveyObj = (typeof surveyObject == 'undefined') ? survey : surveyObject; 
	
	var script = "<scr"+"ipt language='javascript'>var surveyObject ="+JSON.stringify(surveyObj)+";</scr"+"ipt>"+
				"<scr"+"ipt language='javascript' src='../../../jscripts/survey_preview.js'></scr"+"ipt>";

	var text = script;
	
	var question = {elements : []};
	
	if (typeof index == 'undefined')
	{
		question.name = $("#question").val();
		question.type = $("#question_type").val();
		
		var answersElements = document.getElementsByName("answer");
		
		for (var i=0; i<answersElements.length; i++)
		{
			question.elements.push({name: $(answersElements[i]).val()});
		}

		surveyObj.elements[0] = question;
	}
	else
	{
		if (surveyObj.elements.length > index)
		{
			question = surveyObj.elements[index];
		}
		else
		{
			text = "<br><b><center>" + surveyObj.LNG_YOUR_SURVEY_WAS_SUBMITTED +"."
			text = text + "<br><br><br><img src='admin/images/langs/" + surveyObj.default_lng +"/close_button.gif' alt='Close' border='0' onclick='if(window.external) window.external.Close();'>"		
			return text;
		}
	}
	
	text +="<center>" + question.name + "<hr><form> <table>"
	var condAnswers=""
	var answers = question.elements;

	for (var i=0; i<answers.length; i++)
	{
		if(question.type!="T")
		{
			text +=
					"<tr><td>" +
						"<input type='" + (question.type=="N"?"checkbox":"radio")+"' name='answer' id='answer"+i+"' value='"+i+"'/>" +
					"</td><td>" +
						"<label for='answer"+i+"'>"+answers[i].name+"</label>" +
					"</td></tr>";
		}
		else
		{
			text +=
					"<tr><td>" + 
						"<label for='answer"+i+"'>"+answers[i].name+"</label>" +
					"</td><td>" +
						"<input type='text' name='answer' id='answer"+i+"' style='width:100px'/>" +
					"</td></tr>";
		}
		if(question.type=="C")
		{
			if(i==0)
			{
				condAnswers = "<input type='hidden' name='c_yes' value='"+i+"' />"
			}
			else
			{
				condAnswers = condAnswers + "<input type='hidden' name='c_no' value='"+i+"' />"
			}
		}
	}

	if(question.type == "I") {
		text +=
				"<tr><td colspan='2'>" +
				"<textarea cols='50' rows='5' id='custom_answer' name='custom_answer'></textarea>" +
				"</td></tr>";
		
	}
	
	text += "</table><br>" +
				"<input type='image' src='admin/images/submit_button.gif' name='submit' value='Submit' onclick='return checkSubmit();'></form>";
				
	text += condAnswers;

	
	return text;
}


function replaceInTags(str, pattern, replaceWith)
{
	var result = str;
	var rexp = new RegExp(pattern, 'ig');

	var end_pos = 0
	while (true)
	{
		begin_pos = result.indexOf("<",end_pos)
		if (begin_pos == -1) break;
		end_pos=result.indexOf(">",begin_pos);
		if (end_pos == -1) break;
		var tag = result.substring(begin_pos, end_pos+1);
		new_tag = tag.replace(rexp, replaceWith);
		if (new_tag != tag) 
		{
			result = result.substring(0,begin_pos) + new_tag + result.substring(end_pos+1);
		}
	}
	return result; 
}



var questionIndex=0;

function checkSubmit(){
	var answersForm = document.getElementsByTagName('input');
	for(var i=0; i<answersForm.length; i++){
		if(answersForm[i].name=='answer'){
			if(answersForm[i].checked || (answersForm[i].type == 'text' && answersForm[i].value)){
				questionIndex++;
				var surveyHtml = getSurveyHTML(questionIndex);
				surveyHtml = replaceInTags(surveyHtml,"(admin/)?images/upload/", "../../../../admin/images/upload/");
				surveyHtml = replaceInTags(surveyHtml,"admin/images/(?!upload/)", "../../../../admin/images/");
				document.body.innerHTML = surveyHtml;
				initialized = false;
				Initial("0");
				return false;
			}
		}
	}
	if(document.getElementById('custom_answer')!=null)
	{ 
		if(document.getElementById('custom_answer').value.length>0) 
		{ 
			questionIndex++;
			var surveyHtml = getSurveyHTML(questionIndex);
			surveyHtml = replaceInTags(surveyHtml,"(admin/)?images/upload/", "../../../../admin/images/upload/");
			surveyHtml = replaceInTags(surveyHtml,"admin/images/(?!upload/)", "../../../../admin/images/");
			document.body.innerHTML = surveyHtml;
			initialized = false;
			Initial("0");
			return false;
		} 
	}
	alert('You should select an answer.');
	return false;
}