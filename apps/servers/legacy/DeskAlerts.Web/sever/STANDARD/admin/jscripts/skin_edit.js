
//fix bug with not scrollable window in chrome
(function( $, undefined ) {
	if ($.ui && $.ui.dialog) {
	$.ui.dialog.overlay.events = $.map('focus,keydown,keypress'.split(','), function(event) { return event + '.dialog-overlay'; }).join(' ');
	}
}(jQuery));



function getCSSRule(win,ruleName, deleteFlag) {  
     // Return requested style obejct
   ruleName=ruleName.toLowerCase();  
  // Convert test string to lower case.
   if (win.document.styleSheets) {	
   // If browser can play with stylesheets
      for (var i=0; i<win.document.styleSheets.length; i++) { // For each stylesheet

		var styleSheet=win.document.styleSheets[i];          // Get the current Stylesheet
         var ii=0;                                        // Initialize subCounter.
         var cssRule=false;                               // Initialize cssRule. 
         do {                                             // For each rule in stylesheet
            if (styleSheet.cssRules) {                    // Browser uses cssRules?
               cssRule = styleSheet.cssRules[ii]; 
			   // Yes --Mozilla Style
            } else {        
			// Browser usses rules?
               cssRule = styleSheet.rules[ii];            // Yes IE style. 
            }        
			// End IE check.
            if (cssRule)  {                               // If we found a rule...
               if (cssRule.selectorText.toLowerCase()==ruleName) { //  match ruleName?
                  if (deleteFlag=='delete') {             // Yes.  Are we deleteing?
                     if (styleSheet.cssRules) {           // Yes, deleting...
                        styleSheet.deleteRule(ii);        // Delete rule, Moz Style
                     } else {                             // Still deleting.
                        styleSheet.removeRule(ii);        // Delete rule IE style.
                     }                                    // End IE check.
                     return true;                         // return true, class deleted.
                  } else {                                // found and not deleting.
                     return cssRule;                      // return the style object.
                  }                                       // End delete Check
               }                                          // End found rule name
            }                                             // end found cssRule
            ii++;                                         // Increment sub-counter
         } while (cssRule)                                // end While loop
      }                                                   // end For loop
   }                                                      // end styleSheet ability check
   return false;                                          // we found NOTHING!
}                                                         // end getCSSRule 

function killCSSRule(win,ruleName) {                          // Delete a CSS rule   
   return getCSSRule(win,ruleName,'delete');                  // just call getCSSRule w/delete flag.
}                                                         // end killCSSRule

function addCSSRule(win,ruleName,cssText) {                           // Create a new css rule
   if (win.document.styleSheets) {                            // Can browser do styleSheets?
      if (!getCSSRule(win,ruleName)) {                        // if rule doesn't exist...
         if (win.document.styleSheets[0].addRule) {           // Browser is IE?
            win.document.styleSheets[0].addRule(ruleName, cssText,0);      // Yes, add IE style
         } else {                                         // Browser is IE?
            win.document.styleSheets[0].insertRule(ruleName+' { '+cssText+' }', 0); // Yes, add Moz style.
         }                                                // End browser check
      }                                                   // End already exist check.
   }                                                      // End browser ability check.
   return getCSSRule(win,ruleName);                           // return rule we just created.
} 


var controlItems = [];

function selectControlItem(item)
{
	var itemElement = item.item;

	for (var i=0; i<controlItems.length; i++)
	{
		$(controlItems[i].item).css({
			background: 'url(../../../images/edit_skin_bg.png) 50% 50% repeat'
		});
		controlItems[i].selected = false;
	}

	$(itemElement).css({
		background: 'url(../../../images/edit_skin_bg_selected.png) 50% 50% repeat'
		//"background-color": "#d6e7f7"
		//color: 'black'
	})
	item.selected = true;
	
	clearControlPositions();
	fillControlPositions(item);
}




function bindLayersEvents()
{
	$("#left_control_position_input").change(function(){
		var controlItem = getSelectedControlItems()[0];
		var element = controlItem.item;
		if (controlItem.autowidth)
		{
			var right = $("#right_control_position_input").val();
			var width = controlItem.parentWindow.$("#contentTable").width() - right - $(this).val();
			$("#width_control_input").val(width);
			element.width(width);
		}
		else
		{
			var right = controlItem.parentWindow.$("#contentTable").width() - element.width() - element.position().left;
			$("#right_control_position_input").val(right);
		}
		element.css("left",$(this).val());
	});
	$("#top_control_position_input").change(function(){
		var controlItem = getSelectedControlItems()[0];
		var element = controlItem.item;
		element.css("top",$(this).val());
		var bottom = controlItem.parentWindow.$("#contentTable").height() - element.height() - element.position().top;
		$("#bottom_control_position_input").val(bottom);
	});
	$("#right_control_position_input").change(function(){
		var controlItem = getSelectedControlItems()[0];
		var element = controlItem.item;
		if (controlItem.autowidth)
		{
			var width = controlItem.parentWindow.$("#contentTable").width() - $(this).val() - element.position().left;
			$("#width_control_input").val(width);
			$("#left_control_position_input").val(element.position().left);
			element.width(width);
		}
		else
		{
			var left = controlItem.parentWindow.$("#contentTable").width() - $(this).val() - element.width();
			element.css("left",left);
			$("#left_control_position_input").val(left);
		}
	});
	$("#bottom_control_position_input").change(function(){
		var controlItem = getSelectedControlItems()[0];
		var element = controlItem.item;			
		//var height = controlItem.parentWindow.$("#contentTable").height() - $(this).val()
		//$("#height_control_input").val(height);
		//element.height(height);	
		var top = controlItem.parentWindow.$("#contentTable").height() - $(this).val() - element.height();
		element.css("top",top);
		$("#top_control_position_input").val(top);
	});
		
	$("#width_control_input").change(function(){
		var controlItem = getSelectedControlItems()[0];
		var element = controlItem.item;
		
		if (controlItem.autoleft)
		{
			var right = controlItem.parentWindow.$("#contentTable").width() - element.width() - element.position().left;
			element.css({
				left : "auto",
				right : right
			});
		}
		else if (controlItem.autoright)
		{
			element.css({
				left : element.position().left,
				right : "auto"
			});
		}
		
		element.width($(this).val());	
		var right = controlItem.parentWindow.$("#contentTable").width() - element.width() - element.position().left
		$("#left_control_position_input").val(element.position().left);
		$("#right_control_position_input").val(right);
	});
	$("#height_control_input").change(function(){
		var controlItem = getSelectedControlItems()[0];
		var element = controlItem.item;
		element.height($(this).val());
		var bottom = controlItem.parentWindow.$("#contentTable").height() - element.height() - element.position().top
		$("#bottom_control_position_input").val(bottom);
	});
	
	$("#auto_width_control_checkbox").change(function(){
		var controlItem = getSelectedControlItems()[0];
		var element = controlItem.item;
		if ($("#auto_width_control_checkbox").is(':checked'))
		{
			controlItem.autowidth = true;
			$("#auto_right_control_checkbox").prop('checked',false).change();
			$("#auto_left_control_checkbox").prop('checked',false).change();
			
			var right = controlItem.parentWindow.$("#contentTable").width() - element.width() - element.position().left;
			element.css({
				left: element.position().left,
				right: right
			})
		}
		else
		{

			element.css({
				left: element.position().left,
				right: "auto"
			})
			controlItem.autowidth = false;
		}
	});
	
	$("#auto_left_control_checkbox").change(function(){

		var controlItem = getSelectedControlItems()[0];
		var element = controlItem.item;
		if ($("#auto_left_control_checkbox").is(':checked'))
		{
			controlItem.autoleft = true;
			$("#auto_right_control_checkbox").prop('checked',false).change();	
			$("#auto_width_control_checkbox").prop('checked',false).change();
			element.css({
				right: $("#right_control_position_input").val()
			});
		}
		else
		{
			controlItem.autoleft = false;
			element.css({
				right: "auto"
			});
		}
	});
	
	$("#auto_right_control_checkbox").change(function(){
		var controlItem = getSelectedControlItems()[0];
		var element = controlItem.item;
		if ($("#auto_right_control_checkbox").is(':checked'))
		{
			controlItem.autoright = true;
			$("#auto_left_control_checkbox").prop('checked',false).change();
			$("#auto_width_control_checkbox").prop('checked',false).change();
		}
		else
		{
			controlItem.autoright = false;
		}

	});
}

function clearControlPositions()
{
	$(".control_layer_checkboxes").prop('checked', false);
	$(".control_layer_inputs").val("");	
}

function fillControlPositions(item)
{
	var element = item.item;
	
	if (element.css('position')!="absolute" ||  element.parent()[0].tagName!="BODY")
	{
		clearControlPositions();
		$(".control_layer_inputs").prop('disabled',true);
	}
	else
	{
		$(".control_layer_inputs").prop('disabled',false);
		var width = element.width()
		var height = element.height()
		
		var left = element.position().left;
		var top = element.position().top;
		var right = item.parentWindow.$("#contentTable").width() - width - left;
		var bottom = item.parentWindow.$("#contentTable").height() - height - top;
		
		$("#left_control_position_input").val(left);
		$("#top_control_position_input").val(top);
		$("#right_control_position_input").val(right);
		$("#bottom_control_position_input").val(bottom);
		
		$("#width_control_input").val(width);
		$("#height_control_input").val(height);
		
		if (item.autowidth)
		{
			$("#auto_width_control_checkbox").prop('checked',true).change();
		}
		
		if (item.autoleft)
		{
			$("#auto_left_control_checkbox").prop('checked',true).change();
			$("#auto_right_control_checkbox").prop('checked',false).change();
		}
		
		if (item.autoright)
		{
			$("#auto_right_control_checkbox").prop('checked',true).change();
			$("#auto_left_control_checkbox").prop('checked',false).change();
		}	

	}
}

function controlAbsolutePositioning(item)
{
	var element = item.item;
	var layout = item.layout;
	element.css({
		width: layout.width,
		height: layout.height,
		"z-index" : 300,
		position: "absolute",
		"text-align": element.css("text-align"),
		left: layout.left,
		right: layout.right,
		top: layout.top+"px"
	});
	
	element.appendTo(item.parentWindow.document.body)
	.draggable({
		containment: '#contentTable',
		iframeFix: true,
		//snap: ".control_element", 
		snapTolerance: 10,
		snapMode: "outer",
		drag: function(event, ui) {	
			fillControlPositions(item);
		},
		stop: function(event, ui) { 
			fillControlPositions(item);
		}
	}).addClass("control_element");

}

function toggleSnap(item)
{
	var element = item.item;
	
	if (element.draggable( "option", "snap"))
	{
		element.draggable( "option", "snap", false );
	}
	else
	{
		element.draggable( "option", "snap", true );
	}
}
function toggleControlResizable(item)
{
	var element = item.item;
	
	if (element.hasClass("ui-resizable"))
	{
		element.resizable("destroy");
	}
	else
	{
		element.css({
			width: element.width()
		});
		var $resizeIcon = $("<div id='resize_icon_"+element.attr('id')+"' class='ui-resizable-handle ui-resizable-se' style='width:10px; height:10px;'>");
		$resizeIcon.appendTo(element);
		$resizeIcon.css({
			background: 'url(../../../images/edit_skin_resize_icon.gif) no-repeat'
		});

		element.resizable({ 
			handles: {'se':'#resize_icon_'+element.attr('id')}, 
			//animate: true,
			iframeFix: true,
			helper: 'control_item_resizable_helper',
			resize: function(event, ui) {
				fillControlPositions(item);
			},
			stop: function(event, ui) {
				fillControlPositions(item);
			},
			containment: '#contentTable' 
		})
	}
	
}


function getSelectedControlItems()
{
    return $.grep(controlItems, function(n, i){
      return n.selected == true;
    });
} 

function addControlItem(selector,window,text)
{
	var $originalElement = window.$(selector);
	var width = $originalElement.width();
	var height = $originalElement.height();
	var item = $originalElement.clone().appendTo($originalElement.parent());
	$originalElement.hide();

	if (text) item.text(text);
	var autowidth = (item.attr("autowidth") == "true") ? true : false;
	var autoleft = (item.attr("autoleft") == "true") ? true : false;
	var layout;
	if (item.css("position") == "absolute")
	{
		layout = {};
		layout.top = item.position().top;
		layout.left = item.position().left;
		if (autowidth || autoleft)
		{
			layout.right = window.$("#contentTable").width() - layout.left - item.width();
		}
		else
		{
			layout.right = "auto";
		}	
		layout.width = item.width();
		layout.height = item.height();
	}
	
	item.css({
		width: width,
		height : height,
		overflow : 'hidden',
		display : "block"
	}).attr({
		"id" : item.attr('id')+"_control_item"
	});

	item.css({
			background: 'url(../../../images/edit_skin_bg.png) 50% 50% repeat',
			cursor: 'move'
	});
	
	
	var controlItem = {
		item: item, 
		selected:false, 
		autowidth: autowidth,
		autoleft: (item.attr("autoleft") == "true") ? true : false,
		autoright: (item.attr("autoright") == "true") ? true : false,
		originalElement: $originalElement, 
		parentWindow: ((item[0].ownerDocument.defaultView) ? item[0].ownerDocument.defaultView : item[0].ownerDocument.parentWindow),
		layout: layout
	};
	
	controlItems.push(controlItem);
	
	$(item).bind('mousedown',function()
	{
		selectControlItem(controlItem);
	});
	
	return controlItem;
	
}

function getTextNodesIn(el) {
    return $(el).find(":not(iframe)").andSelf().contents().filter(function() {
        return this.nodeType == 3;
    });
};

function initEditPreview(data,container,paramContainer)
{
	var $postForm = $("<form id=\"skin_edit_post_form\" action=\"preview.asp\" method=\"post\" target=\"skin_edit_frame\">\
						<input type=\"hidden\" id=\"preview_alert_width\" name=\"alert_width\"/>\
						<input type=\"hidden\" id=\"preview_alert_height\" name=\"alert_height\"/>\
						<input type=\"hidden\" id=\"preview_alert_title\" name=\"alert_title\"/>\
						<input type=\"hidden\" id=\"preview_alert_html\" name=\"alert_html\"/>\
						<input type=\"hidden\" id=\"preview_caption_href\" name=\"caption_href\"/>\
						<input type=\"hidden\" id=\"preview_top_template\" name=\"top_template\"/>\
						<input type=\"hidden\" id=\"preview_bottom_template\" name=\"bottom_template\"/>\
						<input type=\"hidden\" id=\"preview_create_date\" value=\""+data.create_date+"\" name=\"create_date\"/>\
						<input type=\"hidden\" id=\"preview_full_screen\" name=\"full_screen\"/>\
						<input type=\"hidden\" id=\"preview_ticker\" name=\"ticker\"/>\
						<input type=\"hidden\" id=\"preview_acknowledgement\" name=\"acknowledgement\"/></form>");
	
	$postForm.appendTo($(document.body));
	
	
	var $previewFrame = $("<iframe name='skin_edit_frame'/>");
	$previewFrame.attr({
		id: "skin_edit_frame",
		frameBorder: "0",
		scrolling:"no"
	}).css({
		margin : "0",
		padding : "0",
		border : "0",
		width: data.alert_width,
		height: data.alert_height,
		overflow: "hidden"
	});
	
	
	var $paramsDiv = $("<div/>");
	$paramsDiv.attr({
		id: "skin_edit_params"
	}).css({
		margin : "0",
		padding : "0",
		border : "0",
		width: "100%",
		height: "100%",
		"text-align": "left"
	});

	$(paramContainer).append($paramsDiv);
	

	$previewFrame.bind('load', function(){
		var $captionFrame = $previewFrame.contents().find("#caption_frame");
		var $bodyFrame = $previewFrame.contents().find("#body_frame");
		
		var $previewFrameWindow = ($previewFrame[0].contentWindow) ? $previewFrame[0].contentWindow : $previewFrame[0].defaultView;
		
		if ($captionFrame[0])
		{

			var captionFrameWindow = ($captionFrame[0].contentWindow) ? $captionFrame[0].contentWindow : $captionFrame[0].defaultView;
			var bodyFrameWindow = ($bodyFrame[0].contentWindow) ? $bodyFrame[0].contentWindow : $bodyFrame[0].defaultView;
			
			$previewFrameWindow.addPreviewEventListener('bodyReady',function(){
				
				addCSSRule(bodyFrameWindow,'.alertcontent','background: url(../../../images/edit_skin_bg.png) 50% 50% repeat');
			});
			
			
			$previewFrameWindow.addPreviewEventListener('captionReady',function(){
				
				captionFrameWindow.$("#autosizeJs").remove();
				captionFrameWindow.whichElement = function(){ return true;}
				captionFrameWindow.$(captionFrameWindow.document).bind("contextmenu",function(e){
						return false;
				}); 
				
				var rule = getCSSRule(captionFrameWindow,'div.alerttitle');
				if (rule.style)
				{
					var alertTitleStyle = rule.style.cssText.replace(/! ?important/g,"");

					killCSSRule(captionFrameWindow,'div.alerttitle');
					addCSSRule(captionFrameWindow,'div.alerttitle',alertTitleStyle);
				}
				
				addCSSRule(captionFrameWindow,'.control_item_resizable_helper','border: 2px dotted #00F');

			
				var titleItem = addControlItem('#title',captionFrameWindow,'Title')
				titleItem.item.bind('mousedown',function(event){		
					$paramsDiv.empty();
					var $wordWrapCheckBox = $("<input id='title_word_wrap_input' type='checkbox'>Word wrap</input>");
					$paramsDiv.append($wordWrapCheckBox);
					var element = $(this);
					if ($(this).attr('wordwrap')=='true')
					{
						$('#title_word_wrap_input').prop('checked','checked');
					}
					
					$('#title_word_wrap_input').change(function(){
						if ($('#title_word_wrap_input').prop('checked') == 'checked')
						{
							element.attr('wordwrap',true);
						}
						else
						{
							element.attr('wordwrap',false);
						}
					})
				});
				
				
				var createItem = addControlItem('#create',captionFrameWindow,"Creation Date");
				createItem.item.bind('mousedown',function(event){
					$paramsDiv.empty();	
				});
							
				if (!titleItem.layout)
				{
					titleItem.autowidth = true;
					titleItem.layout = {left:20, right:"auto", top: 76, width:342, height:22};
				}
				
				if (!createItem.layout)
				{
					createItem.autoleft = true;
					createItem.layout = {left:"auto", right:20, top: 69, width:118, height:32};
				}
							
				controlAbsolutePositioning(titleItem);
				toggleControlResizable(titleItem);
				controlAbsolutePositioning(createItem);
				toggleControlResizable(createItem);
					
			});
		}
	});

	
	data.alert_html = "<table style='width:100%; height:100%; text-align:center; vertical-align:middle; border-collapse:collapse; padding:0px; margin:0px;'><tr><td style='text-align:center; vertical-align:middle; color:white; font-weight:bold'>BODY</td></tr></div>"

	$(container).append($previewFrame);
	
	$("#skin_edit_post_form input[name=alert_width]").val(data.alert_width);
	$("#skin_edit_post_form input[name=alert_height]").val(data.alert_height);
	$("#skin_edit_post_form input[name=alert_title]").val(data.alert_title);
	$("#skin_edit_post_form input[name=alert_html]").val(data.alert_html);
	$("#skin_edit_post_form input[name=caption_href]").val(data.caption_href);
	$("#skin_edit_post_form input[name=top_template]").val(data.top_template);
	$("#skin_edit_post_form input[name=bottom_template]").val(data.bottom_template);
	$("#skin_edit_post_form input[name=_ticker]").val(data.ticker);
	$("#skin_edit_post_form input[name=acknowledgement]").val(data.acknowledgement);
	$("#skin_edit_post_form input[name=full_screen]").val(data.fullscreen);
	
	$("#skin_edit_post_form").submit();
	
	
}