function page(strPage)
{
	//alert("aaa");
	getFile(strPage);
}

function select_all_users()
{
	var status = document.getElementById('select_all').checked;
	var field = document.getElementsByName('users');
	for (i = 0; i < field.length; i++)
		field[i].checked = status;
}

function select_all_groups()
{
	var status = document.getElementById('select_all').checked;
	var field = document.getElementsByName('users');
	for (i = 0; i < field.length; i++)
		field[i].checked = status;
}

function Search()
{
	var keyword;
	var limit;
	limit = 50;
	offset = 0;
	keyword = document.getElementsByName('search_users')[0].value;

	getFile('get_users.asp?offset=' + offset + '&limit=' + limit + '&uname=' + keyword);
}

function Search_groups()
{
	var keyword;
	var limit;
	limit = 50;
	offset = 0;
	keyword = document.getElementsByName('search_users')[0].value;

	getFile('get_groups.asp?offset=' + offset + '&limit=' + limit + '&uname=' + keyword);
}

function Search_computers()
{
	var keyword;
	var limit;
	limit = 50;
	offset = 0;
	keyword = document.getElementsByName('search_users')[0].value;

	getFile('get_computers.asp?offset=' + offset + '&limit=' + limit + '&uname=' + keyword);
}

function Search_templates() {
    var keyword;
    var limit;
    limit = 50;
    offset = 0;
    keyword = document.getElementsByName('search_users')[0].value;

    getFile('get_templates.asp?offset=' + offset + '&limit=' + limit + '&uname=' + keyword);
}

function Search_Tgroups() {
    var keyword;
    var limit;
    limit = 50;
    offset = 0;
    keyword = document.getElementsByName('search_users')[0].value;

    getFile('get_all_tgroups.asp?offset=' + offset + '&limit=' + limit + '&uname=' + keyword);
}

function Search_templates_for_group(groupid) {

    var keyword;
    var limit;
    limit = 50;
    offset = 0;
    keyword = document.getElementsByName('search_users')[0].value;

    getFile('get_templates_for_group.asp?group_id=' + groupid  + '&offset=' + offset + '&limit=' + limit + '&uname=' + keyword);

}

// code to create and make AJAX request
function getFile(pURL) {
	var str;
	str = "Loading, please wait...";
	document.getElementById("found_users").innerHTML = str;

	if (window.XMLHttpRequest) { // code for Mozilla, Safari, etc
		xmlhttp = new XMLHttpRequest();
	} else if (window.ActiveXObject) { //IE
		xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
	}

	// if we have created the xmlhttp object we can send the request
	if (typeof(xmlhttp) == 'object') {
		xmlhttp.onreadystatechange = xmlhttpResults;
		xmlhttp.open('GET', pURL, true);
		xmlhttp.send(null);
		// replace the submit button image with a please wait for the results image.
		//        document.getElementById('theImage').innerHTML = '<img id="submitImage" name="submitImage" src="images/ajaxCallChangeImage_wait.gif" alt="Please Wait for AJAX Results">';
		// otherwise display an error message
	} else {
		alert('Your browser is not remote scripting enabled. You can not run this example.');
	}
}

// function to handle asynchronous call
function xmlhttpResults() {
	if (xmlhttp.readyState == 4) {
		if (xmlhttp.status == 200) {
			// we use a delay only for this example
			//            setTimeout('loadResults()',3000);
			loadResults();

		}
	}
}

function loadResults(i) {
	// put the results on the page
	var str;
	str = xmlhttp.responseText;
	document.getElementById("found_users").innerHTML = str;
}

function Show(i)
{
	document.getElementsByName('show_groups' + i)[0].style.display = '';
}

function Hide(i)
{
	document.getElementsByName('show_groups' + i)[0].style.display = 'none';
}