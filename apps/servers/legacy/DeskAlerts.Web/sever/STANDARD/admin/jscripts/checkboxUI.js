
function checkboxClicked(e)
{
	var event = e || window.event;
	if( ! event.target ) {
		event.target = event.srcElement
    }	
	if(event.target.checked==true){
		addObject(event.target.value);
	}
	else{
		removeObject(event.target.value);	
	}
}

function addObject(id){
	window.parent.iframeAddObject(id);
}

function removeObject(id){
	window.parent.iframeRemoveObject(id);
}
