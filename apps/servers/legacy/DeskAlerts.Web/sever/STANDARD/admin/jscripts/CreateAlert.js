﻿var topTemplate;
var bottomTemplate;
var titleChanged = {};
var contentChanged = {};

function toggleDisabled(el, state) {
    try {
        el.disabled = state;
    }
    catch (E) {
    }
    if (el && el.childNodes && el.childNodes.length > 0) {
        for (var x = 0; x < el.childNodes.length; x++) {
            toggleDisabled(el.childNodes[x], state);
        }
    }
}

function check_lifetime() {
    var enabled = $("#lifeTimeCheckBox").prop("checked");
    if (enabled) {
        check_recurrence();
    }
    else {
        $("#lifetime").prop("disabled", enabled);
        //$("#lifetime_factor").prop("disabled", !enabled);
    }
    if ($("#blogPostSettingsHint").css('display') == 'none') {
        $("#blogPost").prop("disabled", enabled);
    }
    else $("#blogPost").prop("disabled", "disabled");
}

function createSkinsTilesHtml() {
    var thumbnail;
    if ($("#alert_type_ticker").prop("checked")) {
        thumbnail = "thumbnail_tick.png";
    } else {
        thumbnail = "thumbnail.png";
    }
    var list = $('<ul style="list-style:none; margin:0px; padding:0px">' +
        '<li class="tile" style="float:left;  margin:10px; padding:10px">' +
        '<div style="padding-bottom:5px"><span autofocus style="margin:0px" id="header_title" class="header_title"><%=resources.LNG_DEFAULT%></span></div>' +
        '<div>' +
        '<img src="skins/default/' + thumbnail + '"/>' +
        '</div>' +
        '</li>' +
        '</ul>');

    for (var i = 0; i < skins.length; i++) {
        var li = '<li id="' + skins[i].id + '" class="tile" style="float:left; margin:10px; padding:10px">' +
            '<div style="padding-bottom:5px"><span style="margin:0px" class="header_title">' + skins[i].name + '</span></div>' +
            '<div>' +
            '<img src="skins/{' + skins[i].id + '}/' + thumbnail + '"/>' +
            '</div>' +
            '</li>';
        $(list).append(li);
    }

    $(list).find(".tile").hover(function () {
        $(this).switchClass("", "tile_hovered", 200);
    }, function () {
        $(this).switchClass("tile_hovered", "", 200);
    }).click(function () {
        $("#skins_tiles_dialog").dialog("close");
        skinId = $(this).attr("id");
        $("#skin_id").val(skinId);
        if (!skinId) {
            skinId = "default";
        }

        var imgSrc = "skins/" + skinId + "/" + thumbnail;
        $("#skin_preview_img").attr("src", imgSrc);
        if (skinId == '{87ebf16f-2561-4d39-8e12-00aaab3c4aa9}' || skinId == '{be097872-6326-423f-bc8b-2e2daccd851b}' || skinId == '{c139afeb-3546-49d2-bd23-f4f46a3176af}') {
            document.getElementById('resizable').disabled = true;
            document.getElementById('resizable').checked = false;
            document.getElementById('size2').checked = false;
            document.getElementById('size2').disabled = true;
            document.getElementById('size1').checked = false;
            document.getElementById('size1').disabled = true;
            document.getElementById('alertWidth').disabled = true;
            document.getElementById('alertHeight').disabled = true;
            document.getElementById('alertWidth').value = "500";
            document.getElementById('alertHeight').value = "400";

            //document.getElementById('caption_frame').style.backgroundColor=transparent;
        }
        else {
            //document.getElementById('alertWidth').disabled = false;
            //document.getElementById('alertHeight').disabled = false;
            //document.getElementById('resizable').disabled = false;
            //document.getElementById('size2').disabled = false;
            //document.getElementById('size1').disabled = false;
            //document.getElementById('caption_frame').style.backgroundColor="white";
        }
    })

    return list;
}

function showSkinsTilesDialog() {
    if (skins.length > 0) {
        $("#skins_tiles_dialog > p").empty();
        $("#skins_tiles_dialog > p").append(createSkinsTilesHtml());
        $("#skins_tiles_dialog").dialog({
            position: { my: "left top", at: "left top", of: "#dialog_dock" },
            modal: true,
            resizable: false,
            draggable: false,
            width: 'auto',
            resize: 'auto',
            margin: 'auto'

        });
    }
    else {
        $("#skins_tiles_dialog").hide();
    }

}


function darkOrLight(color) {
    red = parseInt(color.substring(0, 2), 16);
    green = parseInt(color.substring(3, 5), 16);
    blue = parseInt(color.substring(5), 16);
    var brightness;
    brightness = (red * 299) + (green * 587) + (blue * 114);
    brightness = brightness / 255000;

    // values range from 0 to 1
    // anything greater than 0.5 should be bright enough for dark text
    if (brightness >= 0.5) {
        return "black";
    } else {
        return "white";
    }
}

function hideAppearance() {
    $("#position_div").parent().hide();
    $("#urgent_div").hide();
    $("#aknown_div").hide();
    $("#unobtrusive_div").hide();
    $("#self_deletable_div").hide();
    $("#aut_close_div").hide();
    $("#print_div").hide();
}

function check_desktop() {
    var elem = document.getElementById('desktop_check');
    if (elem) {
        var disabled = !elem.checked;
        var el = document.getElementById('ticker_div');
        toggleDisabled(el, disabled);
    }
    check_size();
}



function showAppearance() {
    $("#tableAlertAppear").show();
    $("#position_div").parent().show();
    $("#desktopCheckLabel").show();
    $("#urgent_div").show();
    $("#aknown_div").show();
    $("#unobtrusive_div").show();
    $("#self_deletable_div").show();
    $("#aut_close_div").show();
    $("#print_div").show();
}

function pickColorCode(color, id) {
    $("#color_code").val(id);
    $("#colorCode").val(id);
    $("#changeColorCode").css("background-color", "#" + color);
    $("#changeColorCode").css("color", darkOrLight(color));
    $("#color_code_dialog").dialog("close");
}

function selectAlertTypeAndClose(type) {
    switch (type) {
        case "alert":
            $("#alertTypeTab").find("a").first().click();
            break;
        case "ticker":
            $("#tickerTypeTab").find("a").first().click();
            break;
        case "rsvp":
            $("#rsvpTypeTab").find("a").first().click();
            break;
        default:
            break;
    }
    $("#alert_type_dialog").dialog("close");
}
function check_size_input(myfield, e, dec) {
    var key;
    var keychar;

    if (window.event)
        key = window.event.keyCode;
    else if (e)
        key = e.which;
    else
        return true;

    keychar = String.fromCharCode(key);

    // control keys
    if ((key == null) || (key == 0) || (key == 8) ||
        (key == 9) || (key == 13) || (key == 27)) {
        return true;
    }
    // numbers
    else if (myfield.value.length > 6) {
        return false;
    }
    else if ((("0123456789").indexOf(keychar) > -1)) {
        return true;
    }
    // decimal point jump
    else if (dec && (keychar == ".")) {
        myfield.form.elements[dec].blur();
        return false;
    }
    else
        return false;
}

function mypreview1() {
    if (document.getElementById("alert_type_alert").checked) {
        document.my_form.preview.value = 1;
    }
    else {
        document.my_form.preview.value = 2;
    }
    document.my_form.sub1.value = 2;
}

function mypreview() {
    var obj = document.getElementById("preview2");
    document.body.removeChild(obj);

    obj = document.getElementById("preview1");
    document.body.removeChild(obj);

    //	document.getElementsByName('preview2')[0].style.display = (document.getElementsByName('preview2')[0].style.display=='none')?'':'none'; 
    //	document.getElementsByName('preview1')[0].style.display = (document.getElementsByName('preview1')[0].style.display=='none')?'':'none'; 
    return false;
}

function check_blogpost() {
    var enabled = document.getElementById("blogPost").checked;
    if (enabled) {
        $("#recurrence_checkbox").prop("checked", false);
        $("#lifetimemode").prop("checked", false);
        check_recurrence();
        check_lifetime();
    }
    $("#recurrence_checkbox").prop("disabled", enabled);
    $("#lifetimemode").prop("disabled", enabled);

}

//function check_recurrence() {





//    var disabled = !($("#recurrenceCheckbox").prop("checked"));
//    var el = document.getElementById("recurrence_div");
//    if ($("#recurrenceCheckbox").length) {
//        toggleDisabled(el, disabled);
//        if (!disabled) {
//            countdownEnableChange();
//            var recDisabled = $("#once").prop("checked");
//            el = $("#alert_time");
//            toggleDisabled(el, recDisabled);
//            el = $("#alert_range");
//            toggleDisabled(el, recDisabled);
//            $("#lifetimemode").prop("checked", false);
//            $("#blogPost").prop("checked", false);
//            check_lifetime();
//        }
//        else {
//            countdownEnableChange(false, disabled);
//        }
//        if ($("#blogPostSettingsHint").css("display") == "none") {
//            $("#blogPost").prop("disabled", !disabled);
//        }
//        else $("#blogPost").prop("disabled", "disabled");

//        /*	if(disabled)
//			{
//				document.getElementById("recurrence_div").style.display="none";
//			}
//			else
//			{
//				document.getElementById("recurrence_div").style.display="";
//			}*/
//    }
//}

function check_aknown() {

    $("#urgentCheckBox").attr({ 'disabled': false });
    $("#unobtrusiveCheckbox").attr({ 'disabled': false });
    $("#aknown").attr({ 'disabled': false });

    if ($("#autoCloseCheckBox").prop("checked")) {
        $("#autoClosebox").prop("disabled", false);
        $("#manualClose").attr({ 'disabled': false });
        $("#aknown").attr({ 'disabled': true, 'checked': false });
        $("#unobtrusiveCheckbox").attr({ 'disabled': true, 'checked': false });
    } else if ($("#aknown").prop("checked")) {
        $("#aut_close").attr({ 'disabled': true, 'checked': false });
        $("#unobtrusiveCheckbox").attr({ 'disabled': true, 'checked': false });
        $("#manualClose").attr({ 'disabled': true, 'checked': false });
        $("#autoClosebox").prop("disabled", true);
    } else if ($("#unobtrusiveCheckbox").prop("checked")) {
        $("#aknown").attr({ 'disabled': true, 'checked': false });
        $("#autoCloseCheckBox").attr({ 'disabled': true, 'checked': false });
        $("#manualClose").attr({ 'disabled': true, 'checked': false });
        $("#autoclose").prop("disabled", true);
        $("#urgentCheckBox").attr({ 'disabled': true, 'checked': false });
    } else if ($("#urgentCheckBox").prop("checked")) {
        $("#unobtrusiveCheckbox").attr({ 'disabled': true, 'checked': false });
        $("#manualClose").attr({ 'disabled': true, 'checked': false });
    } else {
        $("#manualClose").attr({ 'disabled': true, 'checked': false });
        $("#autoClosebox").prop("disabled", true);
        $("#autoCloseCheckBox").prop("disabled", false);
        $("#aknown").prop("disabled", false);
        $("#unobtrusiveCheckbox").prop("disabled", false);
    }
}

function showLoader() {
    $("#loader").css("display", "block");
}

function hideLoader() {
    $("#loader").css("display", "none");
}

function loaderTimeout(smtpConnectTimeout, responseDiv, checkbox) {
    var smtpConnectTimeoutTimer = parseInt($("#smtpConnectTimeoutTimer").val());

    smtpConnectTimeoutTimer++;

    if (smtpConnectTimeoutTimer >= smtpConnectTimeout) {
        clearInterval(loaderTimeoutInterval);
        hideLoader();
        $(responseDiv).append("<div style='color: red;'>Timeout error.</div>");
        $(checkbox).prop("checked", false);
        $(checkbox).attr("disabled", "");
    } else {
        $("#smtpConnectTimeoutTimer").val(smtpConnectTimeoutTimer);
    }
}


function validateEmail() {
    $.ajax({
        type: "POST",
        url: "CreateAlert.aspx/ValidateEmailSettings",
        data: '',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var message = response.d;

            if (message != "") {
                $("#emailCheck").attr("disabled", true).attr('checked', false);
                $("#mailErrorDiv").html("<div style='color: red;'>" + message + "</div>");
            }
        }
    });
}

function validateSms() {

    $.ajax({
        type: "POST",
        url: "CreateAlert.aspx/ValidateSmsSettings",
        data: '',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var message = response.d;

            if (message != "") {
                $("#sms").attr("disabled", true).attr('checked', false);
                $("#smsErrorDiv").html("<div style='color: red;'>" + message + "</div>");
            }
        }
    });

}

function validateTextToCall() {

    $.ajax({
        type: "POST",
        url: "CreateAlert.aspx/ValidateTextToCallSettings",
        data: '',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var message = response.d;

            if (message != "") {
                $("#textToCall").attr("disabled", true).attr('checked', false);
                $("#textToCallErrorDiv").html("<div style='color: red;'>" + message + "</div>");
            }
        }
    });

}

function checkMessagingChannelsSettings(action, checkbox, responseDiv) {
    if ($(checkbox).prop("checked")) {
        showLoader();

        $.post("get_asp_config.asp", { "action": action }, function (data) {
            var SMTPSetting = JSON.parse(data);
            var nullFieldError = "";

            for (var key in SMTPSetting) {
                if (SMTPSetting[key] == "") {
                    nullFieldError += key + ", ";
                }
            }

            nullFieldError = nullFieldError.substring(0, nullFieldError.length - 2); //Remove last ", " from error message.

            if (nullFieldError != "") {
                $(responseDiv).append("<div style='color: red;'>Please fill this fields (" + nullFieldError + ") in settings.</div>");
                $(checkbox).prop("checked", false);
                $(checkbox).attr("disabled", "");
            } else {
                //loaderTimeoutInterval = setInterval(loaderTimeout(paresInt(SMTPSetting["smtpConnectTimeout"]), responseDiv, checkbox), 1000);

                $.post("CreateAlert.aspx", { "action": "checkEmailSettings", "smtpServer": SMTPSetting["smtpServer"], "smtpPort": SMTPSetting["smtpPort"], "smtpUsername": SMTPSetting["smtpUsername"], "smtpPassword": SMTPSetting["smtpPassword"], "smtpSSL": SMTPSetting["smtpSSL"] }, function (data) {
                    try {
                        var response = JSON.parse(data);

                        if ((response["error"] != "") && (response["error"] != undefined)) {
                            $(responseDiv).append("<div style='color: red;'>" + response["error"] + "</div>");
                            $(checkbox).prop("checked", false);
                            $(checkbox).attr("disabled", "");
                        }
                    } catch (exception) {
                        $(responseDiv).append("<div style='color: red;'>" + exception.message + "</div>");
                        $(checkbox).prop("checked", false);
                        $(checkbox).attr("disabled", "");
                    }

                    hideLoader();
                });
            }
        });
    }
}

function check_email(Click) {
    var elem = document.getElementById('emailCheck');

    if (elem) {
        var disabled = !elem.checked;
        var el = document.getElementById('email_div');
        toggleDisabled(el, disabled);

        if (!disabled && Click) {
            validateEmail();
        } else {
            $("#mailErrorDiv").html("");
        }
    }


}

function check_sms(Click) {

    var sms = document.getElementById('sms');

    if (Click && sms && !sms.disabled) {
        validateSms();
    } else {

        $("#smsErrorDiv").html("");
    }
}

function checkTextToCall(Click) {

    var textToCall = document.getElementById('textToCall');

    if (Click && textToCall && !textToCall.disabled) {
        validateTextToCall();
    } else {

        $("#textToCallErrorDiv").html("");
    }
}

function im_interface() {
    $("#socialMedia").prop('disabled', true);
    $("#blogPost").prop('disabled', true);
    $("#socialMedia").parent().hide();
    $("#lnkdnTypeTab").hide();
    $(".save_button").hide();
}

function need_second_question_click() {
    if ($("#need_second_question").is(':checked')) {
        $(".second_question").fadeIn(500);
    }
    else {
        $(".second_question").fadeOut(500);
    }
}

