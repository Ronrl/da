var $targetPreviewFrame;
var $previewData;
var topMenuHeight = 84;

function getTemplateHTML(templateId, type, async, callback) {
    jQuery.ajax({
        url: "get_template.asp?temp_id=" + templateId + "&type=" + type + "&rnd=" + Math.random(),
        success: function (result) {
            callback(result);
        },
        async: async
    });
}

function getAlertData_widget(alertId, async, callback) {
    jQuery.ajax({
        url: "../../get_alert_data.asp?id=" + alertId + "&alertType=alert",
        success: function (result) {
            callback(result);
        },
        async: async
    });
}

function getAlertData(alertId, async, callback) {
    jQuery.ajax({
        url: "GetAlertData.aspx?id=" + alertId + "&alertType=alert",
        success: function (result) {
            callback(result);
        },
        async: async
    });
}

function parseHtmlTitle(htmlTitle, startHtmlCommentString, endHtmlCommentString) {
    htmlTitle = htmlTitle.substring(
        htmlTitle.lastIndexOf(startHtmlCommentString) + startHtmlCommentString.length,
        htmlTitle.lastIndexOf(endHtmlCommentString)
    );

    return htmlTitle;
}

function showAlertPreview(alertId) {
    var data = new Object();
    
    getAlertData(alertId, false, function (alertData) {
        data = JSON.parse(alertData);
    });
    
    var fullscreen = parseInt(data.fullscreen);
    var ticker = parseInt(data.ticker);
    var acknowledgement = data.acknowledgement;

    var alertWidth = fullscreen === 1 && !ticker ? $(".main_table_body").width() : (!ticker ? data.alert_width : $(".main_table_body").width());
    var alertHeight = fullscreen === 1 && !ticker ? $(window).height() - document.body.clientHeight + $(".main_table_body").height() : (!ticker ? data.alert_height : "");
    var alertTitle = data.alert_title;
    var alertHtml = data.alert_html;

    var topTemplate;
    var bottomTemplate;
    var templateId = data.template_id;
    
    if (templateId >= 0) {
        getTemplateHTML(templateId, "top", false, function (data) {
            topTemplate = data;
        });

        getTemplateHTML(templateId, "bottom", false, function (data) {
            bottomTemplate = data;
        });
    }
    data.top_template = topTemplate;
    data.bottom_template = bottomTemplate;
    data.alert_width = alertWidth;
    data.alert_height = alertHeight;
    data.ticker = ticker;
    data.fullscreen = fullscreen;
    data.acknowledgement = acknowledgement;
    data.alert_title = alertTitle;
    data.alert_html = alertHtml;
    data.caption_href = data.caption_href;
    initAlertPreview(data);
}

function getSurveyData(alertId, async, callback) {

    jQuery.ajax({
        url: "get_alert_data.asp?id=" + alertId + "&alertType=survey",
        success: function (result) {
            callback(result);
        },
        async: async
    });
}

function closeAlertPreview() {
    if (!$previewData || !$previewData.in_window) {
        $(window).unbind("scroll", closeAlertPreview);
        $(window).unbind("click", closeAlertPreview);
        $(document).unbind("click", closeAlertPreview);
        $("#alert_preview_dialog").remove();
    }
    else {
        window.close();
    }
}


function setPreviewSize(size, frame) {
    if (size && size.width > 0 && size.height > 0) {
        var $previewFrame = frame ? $(frame) : ($targetPreviewFrame ? $targetPreviewFrame : $("#alert_preview_frame"));

        $previewFrame.css({
            "width": size.width + "px",
            "height": size.height + "px"
        });

        $("#alert_preview_dialog").css({
            "width": size.width + "px",
            "height": size.height + "px"
        });
        if ($previewData) {
            $previewData.alert_width = size.width;
            $previewData.alert_height = size.height;
            setPreviewPosition($previewData);
        }
        if ($previewData && $previewData.in_window) {
            //var isOldIE = !/*@cc_on!@*/0;
            //if(isOldIE)
            window.resizeBy(size.width - $(document).width(), size.height - $(document).height());
            //else
            //	window.resizeTo(size.width, size.height);
        }
    }
}

function setPreviewPosition(data) {
    var setPosition = function(positionStr) {
        var top = - topMenuHeight;
        var height = $(window.parent.parent).height() - data.margin;
        var left = 0;
        var width = $(window).width();
        switch (positionStr) {
        case "bottom":
            var bottom = top + height - data.alert_height;
            $("#alert_preview_dialog").css({
                "top": (bottom) + "px"
            });
            break;
        case "right":
            $("#alert_preview_dialog").css({
                "left": (left + width - data.alert_width) + "px"
            });
            break;
        case "left":
            $("#alert_preview_dialog").css({
                "left": (left) + "px"
            });
            break;
        case "top":
            $("#alert_preview_dialog").css({
                "top": (top + topMenuHeight) + "px"
            });
            break;
        case "center":
            $("#alert_preview_dialog").css({
                "top": (top + height / 2 - data.alert_height / 2 - data.margin) + "px",
                "left": (left + width / 2 - data.alert_width / 2 - data.margin) + "px"
            });
            break;
        }
    };
    if (data && typeof data.position === "string") {
        setPosition(data.position);
    }
    else if (data && typeof data.position !== 'undefined') {
        for (var i = 0; i < data.position.length; i++) {
            setPosition(data.position[i]);
        }
    }
}

//fix bug with not scrollable window in chrome
/*(function( $, undefined ) {
	if ($.ui && $.ui.dialog) {
	$.ui.dialog.overlay.events = $.map('focus,keydown,keypress'.split(','), function(event) { return event + '.dialog-overlay'; }).join(' ');
	}
}(jQuery));*/

function openAlertPreview() {
    if (!$previewData || !$previewData.in_window) {
        $(document).bind("click", closeAlertPreview);
        $(window).bind("click", closeAlertPreview);
        $(window).bind("scroll", closeAlertPreview);
        $("#alert_preview_dialog").fadeIn(1000);
    }
}

$(function () {
    var $obj = $(document.body);
    if ($obj.length > 0) {
        var height = $obj.height ? $obj.height() : 0;
        var width = $obj.width ? $obj.width() : 0;
        var resizeTimer;
        if (width > 0 && height > 0) {
            $(window).unbind('resize').resize(function () {
                if (!$previewData || !$previewData.in_window) {
                    if (resizeTimer)
                        clearTimeout(resizeTimer);
                    resizeTimer = setTimeout(function () {
                        if (height !== $obj.height() || width !== $obj.width()) {
                            height = $obj.height();
                            width = $obj.width();
                            closeAlertPreview();
                        }
                    }, 100);
                }
            });
        }
    }
});

function initAlertPreview(data, targetFrame) {
    
    if (!data.in_window)
        closeAlertPreview();
    $targetPreviewFrame = targetFrame;
    var $previewFrame;
    var $previewDialog;
    /*data.referer="widget_lastAlerts";*/
    data.margin = 0;
    data.position = ['right', 'bottom'];
    if (data.type === "sms") {
        data.alert_width = 202;
        data.alert_height = 400;
        data.margin = -20;
    }
    else if (data.ticker === 1) {
        data.alert_width = $(window).width();
        data.alert_height = 35;
        if (data.fullscreen === '7') { data.position = 'top'; }
        else if (data.fullscreen === '8') { data.position = 'center'; }
        else if (data.fullscreen === '9') { data.position = ['left', 'bottom']; }
    }
    else if (data.fullscreen === '1') {
        data.alert_width = $(window).width() - 10;
        data.alert_height = $(window.parent.parent).height() - topMenuHeight;
        data.position = ['left', 'bottom'];
    }
    else if (data.fullscreen === '2') {
        data.position = ['left', 'top'];
    }
    else if (data.fullscreen === '3') {
        data.position = ['right', 'top'];
    }
    else if (data.fullscreen === '4') {
        data.position = "center";
    }
    else if (data.fullscreen === '5') {
        data.position = ['left', 'bottom'];
    }
    else if (data.fullscreen === '6') {
        data.position = ['right', 'bottom'];
    }

    if (data.alert_width <= 0) data.alert_width = 500;
    if (data.alert_height <= 0) data.alert_height = 400;

    if ($targetPreviewFrame) {
        $previewFrame = $targetPreviewFrame;
        $previewFrame.css({
            width: data.alert_width + "px",
            height: data.alert_height + "px"
        });
    }
    else {
        $previewFrame = $("<iframe name='alert_preview_frame'/>");
        $previewFrame.attr({
            id: "alert_preview_frame",
            frameBorder: "0",
            scrolling: "no",
            allowtransparency: "true"
        }).css({
            margin: "0",
            padding: "0",
            border: "0",
            width: data.alert_width + "px",
            height: data.alert_height + "px",
            overflow: "hidden",
            "background": "transparent"
        });
        $previewDialog = $("<div/>").attr({
            id: "alert_preview_dialog"
        }).css({
            margin: data.margin + "px",
            padding: "0",
            border: "0",
            width: data.alert_width + "px",
            height: data.alert_height + "px",
            overflow: "hidden",
            position: "absolute",
            "z-index": "9999",
            top: "0",
            left: "0",
            display: "none",
            "background": "transparent"
        });
        $(document.body).append($previewDialog);
        $previewDialog.append($previewFrame);
        setPreviewPosition(data);
    }
    $previewData = data;

    if ($("#alert_preview_post_form").length === 0) {
        var previewAspxPath;
        if (data.referer === "widget") {
            previewAspxPath = "../../preview.aspx";
        }
        else {
            previewAspxPath = "preview.aspx";
        }

        var questionOptionsHtml = generateQuestionOptionsHtml(data.question_options_count);
        
        var $postForm = $("<form id=\"alert_preview_post_form\" style=\"display:none\" action=\"" + previewAspxPath + "\" method=\"post\" target=\"" + $previewFrame.attr("name") + "\">\
                            <input type=\"hidden\" id=\"alert_id\" name=\"alert_id\"/>\
							<input type=\"hidden\" id=\"preview_alert_width\" name=\"alert_width\"/>\
							<input type=\"hidden\" id=\"preview_alert_height\" name=\"alert_height\"/>\
							<input type=\"hidden\" id=\"preview_alert_title\" name=\"alert_title\"/>\
							<input type=\"hidden\" id=\"preview_alert_html\" name=\"alert_html\"/>\
							<input type=\"hidden\" id=\"preview_caption_html\" name=\"caption_html\"/>\
							<input type=\"hidden\" id=\"preview_caption_href\" name=\"caption_href\"/>\
							<input type=\"hidden\" id=\"preview_top_template\" name=\"top_template\"/>\
							<input type=\"hidden\" id=\"preview_bottom_template\" name=\"bottom_template\"/>\
							<input type=\"hidden\" id=\"preview_create_date\" value=\""+ data.create_date + "\" name=\"create_date\"/>\
							<input type=\"hidden\" id=\"preview_full_screen\" name=\"full_screen\"/>\
							<input type=\"hidden\" id=\"preview_ticker\" name=\"ticker\"/>\
							<input type=\"hidden\" id=\"preview_type\" name=\"type\"/>\
							<input type=\"hidden\" id=\"preview_acknowledgement\" name=\"acknowledgement\"/>\
							<input type=\"hidden\" id=\"preview_need_question\" name=\"need_question\"/>\
							<input type=\"hidden\" id=\"preview_question\" name=\"question\"/>\ "
                            + questionOptionsHtml + 
                            "<input type=\"hidden\" id=\"preview_question_option10\" name=\"question_option10\"/>\
                            <input type=\"hidden\" id=\"preview_need_second_question\" name=\"need_second_question\"/>\
							<input type=\"hidden\" id=\"preview_second_question\" name=\"second_question\"/>\
							<input type=\"hidden\" id=\"skin_id\" name=\"skin_id\"/>\
							<input type=\"hidden\" id=\"urgent\" name=\"urgent\"/></form>");

        $postForm.appendTo($(document.body));
    }

    $("#alert_preview_post_form input[name=alert_id]").val(data.alert_id);
    $("#alert_preview_post_form input[name=alert_width]").val(data.alert_width);
    $("#alert_preview_post_form input[name=alert_height]").val(data.alert_height);
    $("#alert_preview_post_form input[name=alert_title]").val(data.alert_title);
    $("#alert_preview_post_form input[name=alert_html]").val(data.alert_html);
    $("#alert_preview_post_form input[name=caption_html]").val(data.caption_html);
    $("#alert_preview_post_form input[name=caption_href]").val(data.caption_href);
    $("#alert_preview_post_form input[name=top_template]").val(data.top_template);
    $("#alert_preview_post_form input[name=bottom_template]").val(data.bottom_template);
    $("#alert_preview_post_form input[name=ticker]").val(data.ticker);
    $("#alert_preview_post_form input[name=acknowledgement]").val(data.acknowledgement);
    $("#alert_preview_post_form input[name=full_screen]").val(data.fullscreen);
    $("#alert_preview_post_form input[name=type]").val(data.type);
    $("#alert_preview_post_form input[name=skin_id]").val(data.skin_id);
    $("#alert_preview_post_form input[name=urgent]").val(data.urgent);

    $("#alert_preview_post_form input[name=need_question]").val(data.need_question ? '1' : '0');
    $("#alert_preview_post_form input[name=question]").val(data.question);

    for (var i = 1; i <= data.question_options_count; i++) {
        $("#alert_preview_post_form input[name=question_option" + i).val(data["question_option" + i]);
    }

    $("#alert_preview_post_form input[name=need_second_question]").val(data.need_second_question ? '1' : '0');
    $("#alert_preview_post_form input[name=second_question]").val(data.second_question);
    // First, second, ..., tenth . Is very good practice.

    $("#alert_preview_post_form").submit();
}

function generateQuestionOptionsHtml(count) {
    var result = "";
    for (var i = 1; i <= count; i++) {
        result += "<input type=\"hidden\" id=\"question_option" + i + "\" name=\"question_option" + i + "\"/>\ ";
    }
    return result;
}