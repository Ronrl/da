﻿
<% Response.Expires = 0 %>
<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<!-- #include file="ad.inc" -->
<%
check_session()
showCheckbox = 0 ' no checkboxes, full info
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<title>DeskAlerts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/style9.css" rel="stylesheet" type="text/css">
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>


</head>
<%
' Variables
mifTreeFolder = "mif_tree"

%>
	
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/mootools.js"></script>	
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Node.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Hover.js"></script>	
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Selection.js"></script>	
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Load.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Draw.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.KeyNav.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Sort.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Transform.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Drag.js"></script>	
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Drag.Element.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Checkbox.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Rename.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Row.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.mootools-patch.js"></script>
	<link rel="stylesheet" href="<%Response.write mifTreeFolder%>/style.css" type="text/css" />
	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
	
	<script language="javascript" type="text/javascript">
		$.noConflict();
		(function($)
		{
			$(function() {
				$("#add_user_button").button();
				$("#add_user_button").bind("click", function() {});
				$("#add_group_button").button();
				$("#add_group_button").bind("click", function() {});
				//$("#tree_container").resizable({ minWidth: 200, handles: "e" });
			});
		})(jQuery);
	</script>
<%

uid = Session("uid")

if (uid <>"") then
	users_arr = Array("checked","checked","checked","checked","checked","checked","checked")
	groups_arr = Array("checked","checked","checked","checked","checked","checked","checked")
	Set RS = Conn.Execute("SELECT id, only_personal FROM users WHERE id="&uid&" AND role='E'")
	if(Not RS.EOF) then
		gid = 1
		policy_ids = ""
		Set rs_policy = Conn.Execute("SELECT policy.id as id, type FROM policy INNER JOIN policy_editor ON policy.id=policy_editor.policy_id WHERE editor_id=" & uid)
		if(Not rs_policy.EOF) then
			if(rs_policy("type")="A") then
				gid = 0 'admin
			else
				policyId = rs_policy("id")
				if policy_ids <> "" then
					policy_ids = policy_ids & " OR "
				end if
				policy_ids = policy_ids & "policy_id = " & policyId
			end if
		end if
		if policy_ids <> "" then
			users_arr = editorGetPolicyList(policy_ids, "users_val")
			groups_arr = editorGetPolicyList(policy_ids, "groups_val")
		end if
	else
		gid = 0
	end if
	RS.Close


%>

<script type="text/javascript">

	function changeObjectType(){
					tree.select(tree.root);
					<% controlOUList policy_ids, showCheckbox %>
					
					var content_file="users_new.asp";
					if($('obj_type').value=="U"){
						content_file="users_new.asp?sc=<%=showCheckbox %>"
					}
					if($('obj_type').value=="G"){
						content_file="groups_new.asp?sc=<%=showCheckbox %>"
					}
					if($('obj_type').value=="C"){
						content_file="computers_new.asp?sc=<%=showCheckbox %>"
					}
					$('content_iframe').src=content_file;
					$('objects_div').innerHTML = "";
	}
</script>
<body style="margin:0px" class="body">

<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	<tr>
	<td>
	<table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
		<tr>
		<td width=100% height=31 class="main_table_title"><img src="images/menu_icons/organization_20.png" alt="" width="20" height="20" border="0" style="padding-left:7px">
		<a href="ou_tree_view.asp" class="header_title" style="position:absolute;top:15px"><%=LNG_ORGANIZATION %></a></td>
		</tr>
		<tr>
		<td class="main_table_body" height="100%">
		<div style="padding:10px;">
		<div align="right"></div>
		<table style="padding:0px; margin-top:5px; margin-bottom:20px; border-collapse:collapse; border0px; width:100%;">
			<tr>
				<td>
					<%=LNG_OBJECT_TYPE %>: <select name="obj_type" id="obj_type" onchange="changeObjectType();">
					<option value="U" ><%=LNG_USERS %></option>
					<% if groups_arr(4) = "checked" then %>
					<option value="G" <%if ConfShowGroupsByDefault=1 then response.write " selected='selected' " end if %>><%=LNG_GROUPS %></option>
					<% end if %>
					<option value="C" ><%=LNG_COMPUTERS %></option>
					</select>
				</td>
				<td style="padding:0px; margin:0px; text-align:right;">
				<% if groups_arr(0) = "checked" then %>
					<a href='edit_group.asp?return_page=ou_new_interface.asp' id='add_user_button'><%=LNG_ADD_GROUP%></a>
				<% end if %>
				<% if users_arr(0) = "checked" then %>
					<a href='edit_user.asp?return_page=ou_new_interface.asp' id='add_group_button'><%=LNG_ADD_USER%></a>
				<% end if %>
				</td>
			</tr>
		</table>


<!-- #include file="ou_ui_include.asp" -->

		</td>
		</tr>
	</table>
	</td>
	</tr>
</table>

<script language="javascript">
	<% controlOUList policy_ids, showCheckbox %>
</script>
<%
else

  Response.Redirect "index.asp"

end if
%> 


</body>
</html>
<!-- #include file="db_conn_close.asp" -->