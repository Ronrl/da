<script language="JScript" runat="server">

    function makeEmailHtml(text, alerts_folder) {
        var str = text + "";
        var expr = new RegExp("<(object|embed|iframe)([^>]*)>(([^<]|<(?!\\1))*?</\\1>|)", "gi");


        function makeVideoUrl(src, width, height, type) {
            var url = "<a target='_blank' href=\"" + alerts_folder + "/show_videos.asp?src=" + encodeURIComponent(src) + "&width=" + width + "&height=" + height + "&type=" + type + "\">" + src + "</a>";
            return url;
        }

        function getParams(range) {
            var result = {
                src: "",
                width: "",
                height: "",
                type: ""
            }

            var srcExpr = new RegExp("[^>]+src\\s*=\\s*((['\"])([^\\2]*?)\\2|(\\w+))[^>]*", "gi");
            var widthExpr = new RegExp("[^>]+width\\s*=\\s*((['\"])([^\\2]*?)\\2|(\\w+))[^>]*", "gi");
            var heightExpr = new RegExp("[^>]+height\\s*=\\s*((['\"])([^\\2]*?)\\2|(\\w+))[^>]*", "gi");
            var typeExpr = new RegExp("[^>]+type\\s*=\\s*((['\"])([^\\2]*?)\\2|(\\w+))[^>]*", "gi");

            var exprRes = srcExpr.exec(range);
            if (exprRes) {
                result.src = exprRes[3] || exprRes[4] || "";
            }

            exprRes = widthExpr.exec(range);
            if (exprRes) {
                var width = exprRes[3] || exprRes[4] || "";
                result.width = parseInt(width);
            }

            exprRes = heightExpr.exec(range);
            if (exprRes) {
                var height = exprRes[3] || exprRes[4] || "";
                result.height = parseInt(height);
            }

            exprRes = typeExpr.exec(range);
            if (exprRes) {
                var type = exprRes[3] || exprRes[4] || "";
                if (type == "application/x-shockwave-flash") {
                    result.type = "flash";
                }
                else {
                    result.type = "video";
                }
            }



            return result;
        }


        function regexFunc(range, tagName) {
            var result = "";
            tagName = tagName.toLowerCase();

            switch (tagName) {
                case "object":
                    var embedExpr = new RegExp("<(embed)([^>]*)>(([^<]|<(?!\\1))*?</\\1>|)", "gi");
                    var exprRes = embedExpr.exec(range);

                    if (exprRes) {
                        range = exprRes[0];
                        tagName = "embed"
                    }
                    else {
                        var paramExpr = new RegExp("<param[^>]+name\\s*=\\s*((['\"])(src|url|width|height)\\2|(\\w+))[^>]*", "gi");

                        var src = "";
                        var width = "";
                        var height = "";

                        function getValue() {
                            var value = "";
                            var valueExpr = new RegExp("[^>]+value\\s*=\\s*((['\"])([^\\2]+)\\2|(\\w+))[^>]*", "gi");
                            var valueRes = valueExpr.exec(exprRes[0]);
                            if (valueRes) {
                                value = valueRes[3] || valueRes[4] || "";
                            }
                            return value;
                        }

                        if (paramExpr.test(range)) {
                            paramExpr.lastIndex = 0;
                            while (exprRes = paramExpr.exec(range)) {
                                var value = exprRes[3] || exprRes[4] || "";
                                switch (value) {
                                    case "src":
                                    case "url":

                                        src = getValue(exprRes[0]);
                                        break;
                                    case "width":
                                        width = getValue(exprRes[0]);
                                        break;
                                    case "height":
                                        height = getValue(exprRes[0]);
                                        break;
                                }
                            }

                        }

                        var params = getParams(range);

                        src = src || params.src;
                        width = width || params.width;
                        height = height || params.height;

                        result = makeVideoUrl(src, width, height, "video");

                        break;
                    }
                case "embed":
                    var params = getParams(range);
                    result = makeVideoUrl(params.src, params.width, params.height, params.type);
                    break;
                case "iframe":
                    var src = getParams(range).src;
                    if (src) {
                        result = "<a target='_blank' href=\"" + src + "\">" + src + "</a>";
                    }
                    break;
            }

            return result;
        }

        return str.replace(expr, regexFunc);
    }
</script>

<%
' Function to convert OctetString (byte array) to Hex string.
Function OctetToHexStr(ByVal arrbytOctet)
    Dim i

    OctetToHexStr = ""

    For i = 1 To Lenb(arrbytOctet)
        OctetToHexStr = OctetToHexStr _
            & Right("0" & Hex(Ascb(Midb(arrbytOctet, i, 1))), 2)
    Next
End Function

'send alert to ip groups. Get "groups" - array of groups and "alert_id" - id of alert.
function sendIpRange(groups, alert_id)
	
	for each group_id in groups
		if(group_id<>"") then
			Set RSrange = Conn.Execute("SELECT from_ip, to_ip FROM ip_range_groups WHERE group_id=" & group_id)
			do while not RSrange.EOF
				ip_from = 0
				ip_to = 0
				iprange_from=Split(RSrange("from_ip"), ".")
				iprange_to=Split(RSrange("to_ip"), ".")
				For k = 0 to 3
					ip_from = ip_from + iprange_from(k)*(256^(3-k))
				Next
				For k = 0 to 3
					ip_to = ip_to + iprange_to(k)*(256^(3-k))
				Next
				SQL1 = "INSERT INTO alerts_sent_iprange (alert_id, ip_from, ip_to) VALUES ( " & alert_id & ", " & ip_from & ", " & ip_to & ")"
				Conn2.Execute(SQL1)
				RSrange.MoveNext
			loop
		end if
	next
end function

function ColumnExists(RS, name)
	for each column in RS.Fields
		if LCase(column.Name) = LCase(name) then
			ColumnExists = true
			exit function
		end if
	next
	ColumnExists = false
end function


Function ConvertHTMLtoText(byVal strHTML)
    If InStr(1,strHTML,"<") > 0 Then
    Do 
    startVariable = InStr(1,strHTML,"<")

    endVariable = InStr(startVariable,strHTML,">")
    varName = Mid(strHTML,startVariable, endVariable-startVariable+1)
    strHTML = replace(strHTML,varName,">")
    Loop While InStr(1,strHTML,"<") > 0
    End If 
    strHTML = replace(strHTML,"&gt;","")
    strHTML = replace(strHTML,"&lt;","")
    ConvertHTMLtoText = strHTML
End Function


function generateOUList(ouIds, shCh)
	ouListHtml = "<table><tr valign='top'><td>"
	if(shCh<>0) then
		ouListHtml = ouListHtml & "<input type='checkbox' name='select_all_ous' id='select_all_ous' value='1' onclick='javascript: check_all_ous();' />	"
	end if
	ouListHtml = ouListHtml & "</td><td><img src='mif_tree/mootools/images/ou.png' /></td><td><span id='ou_all' class='ed_ou'>All</span><br/><span class='ou_path'>View objects in all OUs assigned</span></td></tr>"

	'get subOUs and add to list
	Set RSsub=Conn.Execute(";WITH OUsList (Id_child) AS (SELECT Id_child FROM OU_hierarchy WHERE "& Replace(ouIds, "Id", "Id_parent") &" UNION ALL SELECT c.Id_child From OU_hierarchy as c INNER JOIN OUsList as p ON c.Id_parent = p.Id_child ) SELECT Id_child FROM OUsList")
	Do While Not RSsub.EOF
		ouIds = ouIds & " OR Id=" & RSsub("Id_child")
		RSsub.MoveNext
	loop	

	Set rsEditorOUs = Conn.Execute ("SELECT Id, Name, OU_PATH FROM OU WHERE " & ouIds & " ORDER BY OU_PATH")
	do while not rsEditorOUs.EOF
		ouId = rsEditorOUs("Id")
		ouName = rsEditorOUs("Name")
		ouPath = rsEditorOUs("OU_PATH")
		ouTemplate = "<tr valign='top'><td>"
		if(shCh<>0) then
			ouTemplate = ouTemplate & "<input type='checkbox' name='ed_ous' id='ed_ous_"&ouId&"' value='"&ouId&"' onclick='$(\""ed_ous_"&ouId&"_closed\"").checked = this.checked' /><input type='checkbox' id='ed_ous_"&ouId&"_closed' name='ous_id_sequence_closed' value='"&ouId&"' style='display:none'/>"
		end if
		ouTemplate = ouTemplate & "</td><td><img src='mif_tree/mootools/images/ou.png' /></td><td><span id='ou_"&ouId&"' class='ed_ou'>" & ouName & "</span><br/><span class='ou_path'>"&ouPath&"</span></td></tr>"
		ouListHtml = ouListHtml & ouTemplate
		rsEditorOUs.MoveNext
	loop
	ouListHtml = ouListHtml & "</table>"
	generateOUList = ouListHtml
end function

sub controlOUList(policyIds, shCh)
	if AD=3 then
		if gid = 0 or ConfEnableOldOUSelect <> 1 then
%>
$('tree_td').style.display="";
<%
		else
			ou_ids=""
			ou_ids=editorGetOUIds(policyIds)
			if ou_ids <> "" then
%>
$('tree_td_ed').style.display=""; $('tree_td_ed').innerHTML = "<%=generateOUList(Replace(ou_ids,"ou_id=","Id="), shCh)%>";
$$('.ed_ou').addEvent('click', editorOUSelect);
<%
			end if
		end if
		if ConfHideOrganizationTree=1 and gid<>0 then
			%>
			$('tree_td').style.display="none";
			<%
		end if
	end if
end sub

function IsSuperAccount (userId)
    
    Set userNameSet = Conn.Execute("SELECT name FROM users WHERE id = " & userId)
    userName = userNameSet("name")
    
    IsSuperAccount = InArray(userName, ConfSuperAccounts)
    
end function

function LoadChildOu (ParentOuId, ChildId, Tab)

	GetOuName = "SELECT name FROM OU WHERE ID="&ChildId
	Set OuNameMap = Connetcion.Execute( GetOuName )	
	
	
	Response.Write "{"
	Response.Write """property"":"
	Response.Write "{"
	
	' Out domain name to response
	if(OuNameMap.EOF <> true) then
		Response.Write """name"":" & """" &replace(OuNameMap("name"),"""", "\""") & ""","
		Response.Write """type1"":""OU"","
		Response.Write """id"":" & """"&ChildId&""""
		'Response.write OuNameMap("name")&"<br>"
	end if
	
	
	GetChildOu = "SELECT Id_parent, Id_child FROM OU_hierarchy WHERE Id_parent="&ChildId	
	Dim ChildOus
	Set ChildOus = Connetcion.Execute( GetChildOu )	
	
	Dim ExChilds
	ExChilds = true
	
	if(ChildOus.EOF) then
		ExChilds = false
	end if
	
	if( ExChilds ) then 
		Response.Write ",""loadable"":""true"""
	end if

	Response.Write "},"
	Response.Write """type"": ""ou"""

	Response.Write "}"
	
'	Response.Write "},"
	
end function

function replaceLinks(htmlstr)
	replaceLinks = htmlstr
	if htmlstr <> "" then
		pos_last=1
		do
			pos=InStr(pos_last, replaceLinks, "<a href=")
			if pos <> 0 then
				pos1=InStr(pos+10, replaceLinks, """")
				if pos1 <> 0 then
					link=Mid(replaceLinks, pos+9, pos1-pos-9) 
					link=Server.URLEncode(link)
					pos_last=pos1
					if InStr(link,"click.asp")=0 then
						replaceLinks = Replace(replaceLinks, "<a href=""" & link, "<a href=""" & alerts_folder & "click.asp?url=" & link & "&alert_id=" & request("id") & "&user_id=" & clng(request("user_id")))
					end if
				end if
			end if
		loop while pos <> 0 and pos1 <> 0
	end if
end function

function replacePath(htmlstr)
	replacePath = htmlstr
	if htmlstr <> "" then
		replacePath = replaceInTags(htmlstr, "(['""])(admin/)?images/upload/", "$1"& alerts_folder &"admin/images/upload/")

		replacePath = replaceInTags(replacePath, "(['""])admin/images/(?!upload/)", "$1"& alerts_folder &"admin/images/")

		replacePath = replaceInTags(replacePath, "(['""])jscripts/tiny_mce/", "$1"& alerts_folder &"admin/jscripts/tiny_mce/")

		replacePath = replaceFlash(replacePath)
		replacePath = replaceVideo(replacePath, "")
        replacePath = replaceVideoScreensaver(replacePath, "")

		pos_last=1
		do
			pos=InStr(pos_last, text, "src=""../../")
			if pos <> 0 then
				pos1=InStr(pos+10, text, "admin/images/upload")
				if pos1 <> 0 then
					pos_last = pos1
					myurl=Mid(text, pos, pos1-pos) 
					text = Replace(text, myurl, "src=""" & alerts_folder)
				end if
			end if
		loop while pos <> 0 and pos1 <> 0

		pos_last=1
		do
			pos=InStr(pos_last, text, "href=""../../")
			if pos <> 0 then
				pos1=InStr(pos+10, text, "admin/images/upload")
				if pos1 <> 0 then
					pos_last = pos1
					myurl=Mid(text, pos, pos1-pos) 
					text = Replace(text, myurl, "href=""" & alerts_folder)
				end if
			end if
		loop while pos <> 0 and pos1 <> 0
	end if
end function

Function encrypt_ASP(ByRef Key, ByRef Data)
    dim KeyBytes(255)
    dim CypherBytes(255)
    KeyLen=Len(Key)
    if KeyLen=0 Then Exit function
    KeyText=Key
    For i=0 To 255
      KeyBytes(i)=Asc(Mid(Key, ((i) Mod (KeyLen))+1, 1))
    Next
    if Len(Data)=0 Then Exit function
    For i=0 To 255
      CypherBytes(i)=i
    Next
    Jump=0 'Swap values of Cypher around based on index and KeyText value
    For i=0 To 255 'Find index To switch
      Jump=(Jump+CypherBytes(i)+KeyBytes(i)) Mod 256
      Tmp=CypherBytes(i) 'Swap
      CypherBytes(i)=CypherBytes(Jump)
      CypherBytes(Jump)=Tmp
    Next
    i=0
    Jump=0

    'WriteToFile alerts_dir & "encrypt\enc" & Request("id") & ".html", "", False

    For X=1 To Len(Data)
      i=(i+1) Mod 256
      Jump=(Jump+CypherBytes(i)) Mod 256
      T=(CypherBytes(i)+CypherBytes(Jump)) Mod 256
      Tmp=CypherBytes(i) 'Swap
      CypherBytes(i)=CypherBytes(Jump)
      CypherBytes(Jump)=Tmp
      encrypt_ASP=encrypt_ASP & Chr(Asc(Mid(Data, X, 1)) Xor CypherBytes(T)) 'Character Encryption
'      Response.Write Chr(Asc(Mid(Data, X, 1)) Xor CypherBytes(T))
    Next
End function

Function RSBinaryToString(xBinary)
  'Antonin Foller, http://www.motobit.com
  'RSBinaryToString converts binary data (VT_UI1 | VT_ARRAY Or MultiByte string)
  'to a string (BSTR) using ADO recordset

  Dim Binary
  'MultiByte data must be converted To VT_UI1 | VT_ARRAY first.
  If vartype(xBinary)=8 Then Binary = MultiByteToBinary(xBinary) Else Binary = xBinary
  
  Dim RS, LBinary
  Const adLongVarChar = 201
  Set RS = CreateObject("ADODB.Recordset")
  LBinary = LenB(Binary)
  
  If LBinary>0 Then
    RS.Fields.Append "mBinary", adLongVarChar, LBinary
    RS.Open
    RS.AddNew
      RS("mBinary").AppendChunk Binary 
    RS.Update
    RSBinaryToString = RS("mBinary")
  Else
    RSBinaryToString = ""
  End If
End Function

EncryptObject = Null
EncryptUseASP = False
Public Function encrypt(sMessage, encrypt_key)
	If EncryptUseASP Then
		encrypt = encrypt_ASP(encrypt_key, sMessage)
	Else
		On Error Resume Next
		Err.Clear
		If Not IsObject(EncryptObject) Then
			Set EncryptObject = Server.CreateObject("DeskAlertsServer.Encrypt")
		End If
		If Err.Number = 0 Then
			encrypt = RSBinaryToString(EncryptObject.encryptUTF8(sMessage, encrypt_key))
			If Err.Number <> 0 Then
				encrypt = encrypt_ASP(encrypt_key, sMessage)
				EncryptUseASP = True
			End If
		Else
			encrypt = encrypt_ASP(encrypt_key, sMessage)
			EncryptUseASP = True
		End If
		On Error Goto 0
	End If
End Function

DecryptUseASP = False
Public Function decrypt(sMessage, encrypt_key)
	If DecryptUseASP Then
		decrypt = encrypt_ASP(encrypt_key, sMessage)
	Else
		On Error Resume Next
		Err.Clear
		If Not IsObject(EncryptObject) Then
			Set EncryptObject = Server.CreateObject("DeskAlertsServer.Encrypt")
		End If
		If Err.Number = 0 Then		
			decrypt = RSBinaryToString(EncryptObject.encrypt(sMessage, encrypt_key))
			If Err.Number <> 0 Then
				decrypt = encrypt_ASP(encrypt_key, sMessage)
				DecryptUseASP = True
			End If
		Else
			decrypt = encrypt_ASP(encrypt_key, sMessage)
			DecryptUseASP = True
		End If
		On Error Goto 0
	End If
End Function


Sun = 1
Mon = 2
Tue = 4
Wed = 8
Thu = 16
Fri = 32
Sat = 64

function weekDaysToBits(strWeekDays)
	strBits=0

	if(InStr(strWeekDays, "Sat")<>0) then
		strBits=strBits + Sat
	end if

	if(InStr(strWeekDays, "Fri")<>0) then
		strBits=strBits + Fri
	end if

	if(InStr(strWeekDays, "Thu")<>0) then
		strBits=strBits + Thu
	end if

	if(InStr(strWeekDays, "Wed")<>0) then
		strBits=strBits + Wed
	end if

	if(InStr(strWeekDays, "Tue")<>0) then
		strBits=strBits + Tue
	end if

	if(InStr(strWeekDays, "Mon")<>0) then
		strBits=strBits + Mon
	end if

	if(InStr(strWeekDays, "Sun")<>0) then
		strBits=strBits + Sun
	end if
	weekDaysToBits = strBits
end function

function isWeekday(strBits, strWeekday)
	isWeekday=""
	if((strBits AND strWeekday) <>0)then
		isWeekday="checked"
	end if
end function

'--policy

function inArray(elem, arr)
	inArray = false
	for in_i=LBound(arr) to Ubound(arr)
		if CStr(arr(in_i)) = CStr(elem) then
			inArray = true
			exit function
		end if
	next 
end function

function getRejectedAlertsCountForUser(user_id)

    Set countSet = Conn.Execute(" SELECT COUNT(1)  as count FROM alerts WHERE approve_status = 2 AND sender_id = " & user_id)
    
    getRejectedAlertsCountForUser = countSet("count")
end function


function getNotApprovedAlertsForUser(user_id)

    Set countSet = Conn.Execute(" SELECT COUNT(1)  as count FROM alerts WHERE approve_status = 0 AND sender_id != " & user_id)
    
    getNotApprovedAlertsForUser = countSet("count")
end function

function p_makeCheckedArray(str)
	checked_arr = Array("","","","","","","","","","","","")
	for li = 0 to UBound(checked_arr)
		if Mid(str, li+1, 1) = "1" then
			checked_arr(li) = "checked"
		end if
	next
	p_makeCheckedArray = checked_arr
end function

function p_makeString(val)
	str=""
	strlen = 6
	if APPROVE = 1 then
	    strlen = 7
	end if
	
	for li = 0 to strlen
		ch = request(val & "_" & li)
		if(ch <> "1") then ch = "0" end if
		str = str & ch
	next
	p_makeString = str
end function

'----------for editors
function getRights(editor_id, gid_need, gsendtoall_need, gviewall_need, rights_arr)
	result = null
	superUser = IsSuperAccount(editor_id)
	set RSEditor = Conn.Execute("SELECT role FROM users WHERE id="& editor_id)
	if not RSEditor.EOF then
		set result = Server.CreateObject("Scripting.Dictionary")
		if RSEditor("role") = "A" or RSEditor("role") = "M" or  superUser then
			if gid_need then
				result.add "gid", "0"
			end if
			
			if gsendtoall_need then
				result.add "gsendtoall", "1"
			end if
			
			if gviewall_need then
			    if not superUser then
				    result.add "gviewall", "1"
				else
				    result.add "gviewall", "0"
				end if
		        
				result.add "gviewlist", Array()
			end if
			
			if VarType(rights_arr) = 8 then
				result.add rights_arr, p_makeCheckedArray("111111111111")
			elseif VarType(rights_arr) > 1 then
				for rights_i = LBound(rights_arr) to UBound(rights_arr)
					result.add rights_arr(rights_i), p_makeCheckedArray("111111111111")
				next
			end if
		else'if RSEditor("role") = "E" then
			if VarType(rights_arr) = 8 then
				rights_arr = Array(rights_arr)
			elseif VarType(rights_arr) <= 1 then
				rights_arr = Array()
			end if
			
			SQL = "SELECT p.id, p.type"
			if gsendtoall_need then
				SQL = SQL & ", pu.user_id"
			end if
			if gviewall_need then
				SQL = SQL & ", pv.editor_id"
			end if
			for rights_i = LBound(rights_arr) to UBound(rights_arr)
				SQL = SQL & ", pl." & rights_arr(rights_i)
			next
			SQL = SQL & " FROM policy p"
			SQL = SQL & " INNER JOIN policy_editor pe ON p.id=pe.policy_id "
			SQL = SQL & " INNER JOIN policy_list pl ON p.id=pl.policy_id "
			if gsendtoall_need then
				SQL = SQL & "LEFT JOIN policy_user pu ON pu.policy_id = p.id AND pu.user_id = 0"
			end if
			if gviewall_need then
				SQL = SQL & "LEFT JOIN policy_viewer pv ON pv.policy_id = p.id AND pv.editor_id = 0"
			end if
			SQL = SQL & " WHERE pe.editor_id = " & editor_id
			
			for rights_i = LBound(rights_arr) to UBound(rights_arr)
				result.add rights_arr(rights_i), Array("","","","","","","","","","","","")
			next
			
			policy_gid = 2
			policy_gsendtoall = 0
			policy_gviewall = 0
			policy_ids = Array()
			set RSPolicy = Conn.Execute(SQL)
			do while not RSPolicy.EOF
				ReDim Preserve policy_ids(UBound(policy_ids) + 1)
				policy_ids(UBound(policy_ids)) = RSPolicy("id")
				
				if RSPolicy("type") = "A" or RSPolicy("type") = "M" then
					policy_gid = 0
					policy_gsendtoall = 1
					policy_gviewall = 1
				elseif RSPolicy("type") = "E" and policy_gid > 1 then
					policy_gid = 1
				end if
				
				if gsendtoall_need then
					if not isNull(RSPolicy("user_id")) then
						policy_gsendtoall = 1
					end if
				end if
				
				if gviewall_need then
					if not isNull(RSPolicy("editor_id")) then
						policy_gviewall = 1
					end if
				end if
				
				for rights_i = LBound(rights_arr) to UBound(rights_arr)
					if RSPolicy("type") <> "A" or RSPolicy("type") = "M" then
						policy_rights_str = rsPolicy(rights_arr(rights_i))
						policy_rights_arr = result(rights_arr(rights_i))
						for rights_str_i = 0 to Ubound(policy_rights_arr)
							if Mid(policy_rights_str, rights_str_i+1, 1) = "1" then
								policy_rights_arr(rights_str_i) = "checked"
							end if
						next
					else
						policy_rights_arr = p_makeCheckedArray("111111111111")
					end if
					result(rights_arr(rights_i)) = policy_rights_arr
				next
				
				RSPolicy.MoveNext
			loop
			
			result.add "policy_ids", policy_ids
			
			if gid_need then
				result.add "gid", CStr(policy_gid)
			end if
			
			if gsendtoall_need then
				result.add "gsendtoall", CStr(policy_gsendtoall)
			end if
			
			if gviewall_need then
				result.add "gviewall", CStr(policy_gviewall)
				if policy_gviewall = 0 then
					' get view list
					policy_view_list = Array(editor_id)
					set RSViewList = Conn.Execute("SELECT editor_id FROM policy_viewer WHERE policy_id IN ("& Join(policy_ids, ",") &")")
					do while not RSViewList.EOF
						ReDim Preserve policy_view_list(UBound(policy_view_list) + 1)
						policy_view_list(UBound(policy_view_list)) = RSViewList("editor_id")
						RSViewList.MoveNext
					loop
					result.add "gviewlist", policy_view_list
				else
					result.add "gviewlist", Array()
				end if
			end if
		end if
	end if
	set getRights = result
end function

function editorGetPolicyList(policy_ids, list_name)
	editorGetPolicyList = Array("","","","","","","","","","","","")
	set RS = Conn.Execute("SELECT "&list_name&" FROM policy_list WHERE " & policy_ids)
	if not RS.EOF then
		if not IsNull(RS(list_name)) then
			editorGetPolicyList = p_makeCheckedArray(RS(list_name))
		end if
	end if
end function

function editorGetPolicyIds(editor_id)
	policy_ids=""
	set RSPolicy = Conn.Execute("SELECT policy_id FROM policy_editor WHERE editor_id=" & editor_id)
	do while not RSPolicy.EOF
		if policy_ids <> "" then
			policy_ids = policy_ids & " OR "
		end if
		policy_ids = policy_ids & " policy_id=" & RSPolicy("policy_id")
		RSPolicy.MoveNext
	loop
	editorGetPolicyIds = policy_ids
end function

Sub WriteLineToResponseAndLog2(str)
	Response.Write str & "<br/>"
	Response.Flush
	if ConfEnableSMSLogs = 1 then
		if not isObject(fs) then
			set fs=Server.CreateObject("Scripting.FileSystemObject")
		end if
		if isEmpty(logFile) then
			fname = Server.MapPath("logs\" & Year(now_db) &"-"& TwoDigits(Month(now_db)) &"-"& TwoDigits(Day(now_db)) &"_"& TwoDigits(Hour(now_db)) &"-"& TwoDigits(Minute(now_db)) &"-"& TwoDigits(Second(now_db)) &".log")
			'fname=date&".log"
			if (InStr(fname,"\admin\logs\")=0) then
				fname = Replace(fname,"\logs\","\admin\logs\")
			end if
			set logFile = fs.OpenTextFile(fname, 8, True, TristateTrue)
		end if
		logFile.WriteLine str
	end if
End Sub

function editorGetGroupIds(policy_ids)
	groupFlag=0
	Set RSGroup = Conn.Execute("SELECT DISTINCT group_id FROM policy_group WHERE " & policy_ids)
	Do While not RSGroup.EOF
		group_id=RSGroup("group_id")
		if(groupFlag=1) then
			group_ids=group_ids & " OR " & " group_id=" & group_id 
		else
			group_ids=group_ids & " group_id=" & group_id 
		end if
		groupFlag=1
		RSGroup.MoveNext	
	loop
	editorGetGroupIds = group_ids
end function

function editorGetOUIds(policy_ids)
	ouFlag=0
	Set RSOU = Conn.Execute("SELECT DISTINCT ou_id FROM policy_ou WHERE " & policy_ids)
	Do While not RSOU.EOF
		ou_id=RSOU("ou_id")
		if(ouFlag=1) then
			ou_ids=ou_ids & " OR " & " ou_id=" & ou_id 
		else
			ou_ids=ou_ids & " ou_id=" & ou_id 
		end if
		ouFlag=1
		RSOU.MoveNext	
	loop
	editorGetOUIds = ou_ids
end function

'----------

function WriteToFile(FileName, Contents, Append)
on error resume next

if Append = true then
   iMode = 8
else 
   iMode = 2
end if
set oFs = server.createobject("Scripting.FileSystemObject")
set oTextFile = oFs.OpenTextFile(FileName, iMode, True)
oTextFile.Write Contents
oTextFile.Close
set oTextFile = nothing
set oFS = nothing

end function

Function Base64Decode(ByVal base64String)
  'rfc1521
  '1999 Antonin Foller, Motobit Software, http://Motobit.cz
  Const Base64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"

  Dim dataLength, sOut, groupBegin
  
  'remove white spaces, If any
  base64String = Replace(base64String, vbCrLf, "")
  base64String = Replace(base64String, vbTab, "")
  base64String = Replace(base64String, " ", "")
  
  'The source must consists from groups with Len of 4 chars
  dataLength = Len(base64String)
'  If dataLength Mod 4 <> 0 Then
 '   Err.Raise 1, "Base64Decode", "Bad Base64 string."
  '  Exit Function
'  End If

  
  ' Noww decode each group:
  For groupBegin = 1 To dataLength Step 4
    Dim numDataBytes, CharCounter, thisChar, thisData, nGroup, pOut
    ' Each data group encodes up To 3 actual bytes.
    numDataBytes = 3
    nGroup = 0

    For CharCounter = 0 To 3
      ' Convert each character into 6 bits of data, And add it To
      ' an integer For temporary storage.  If a character is a '=', there
      ' is one fewer data byte.  (There can only be a maximum of 2 '=' In
      ' the whole string.)

      thisChar = Mid(base64String, groupBegin + CharCounter, 1)

      If thisChar = "=" Then
        numDataBytes = numDataBytes - 1
        thisData = 0
      Else
        thisData = InStr(1, Base64, thisChar, vbBinaryCompare) - 1
      End If
      If thisData = -1 Then
        Err.Raise 2, "Base64Decode", "Bad character In Base64 string."
        Exit Function
      End If

      nGroup = 64 * nGroup + thisData
    Next
    
    'Hex splits the long To 6 groups with 4 bits
    nGroup = Hex(nGroup)
    
    'Add leading zeros
    nGroup = String(6 - Len(nGroup), "0") & nGroup
    
    'Convert the 3 byte hex integer (6 chars) To 3 characters
    pOut = Chr(CByte("&H" & Mid(nGroup, 1, 2))) + _
      Chr(CByte("&H" & Mid(nGroup, 3, 2))) + _
      Chr(CByte("&H" & Mid(nGroup, 5, 2)))
    
    'add numDataBytes characters To out string
    sOut = sOut & Left(pOut, numDataBytes)
  Next

  Base64Decode = sOut
End Function

Function DeleteTags(Source)
	DeleteTags = Source
	pos1 = InStr(DeleteTags, "<")
	pos2 = 1
	Do While (pos1 > 0) And (pos2 > 0)
		If pos1 > 0 Then
			pos2 = InStr(pos1, DeleteTags, ">")
		End If
		If pos2 = 0 Then
			DeleteTags = Left(DeleteTags, pos1 - 1) & " "
		Else
			DeleteTags = Left(DeleteTags, pos1 - 1) & " " & Right(DeleteTags, Len(DeleteTags) - pos2)
		End If
		pos1 = InStr(DeleteTags, "<")
	Loop
	Set rSpaces = New RegExp
	With rSpaces
		.Pattern = " +"
		.IgnoreCase = False
		.Global = True
	End With
	DeleteTags = Trim(rSpaces.Replace(DeleteTags, " "))
End Function 

' IMPORTANT! In place where you are use sendAlertSMS add next lines:
'if smsPhoneFormat = "" then smsPhoneFormat = "1"
'if smsPhoneFormat <> "%smsPhoneFormat%" and smsPhoneFormat <> "0" then
'% >
'	<script language="JScript" runat="server" src="jscripts/phone_format.js"></script>
'	<script language="JScript" runat="server" src="jscripts/da_phone_format.js"></script>
'< %
'end if
Function sendAlertSMS(smsText_, smsMobileNo_)
	on error resume next

	smsText = Replace(smsText_, "&gt;", ">")
	smsText = Replace(smsText, "&lt;", "<")
	smsText = Replace(smsText, "&nbsp;", " ")
	smsText = Replace(smsText, "&#39;", "'")
	smsText = Replace(smsText, "&quot;", """")
    smsText = Replace(smsText, "\r\n", " ")
    smsText = Replace(smsText, "\r", " ")
    smsText = Replace(smsText, "\r", " ")
	smsMobileNo = smsMobileNo_
	if smsPhoneFormat <> "%smsPhoneFormat%" and smsPhoneFormat <> "0" then
		smsMobileNo = format_number(smsMobileNo, smsPhoneFormat, smsDefaultCountryCode)
	end if

	if urlShortEnabled = 1 then
		smsText = shortUrls(smsText)
	end if

	smsText = Server.URLEncode(smsText)
	smsMobileNo = Server.URLEncode(smsMobileNo)

	sUrl = smsUrl
	sPostData = Base64Decode(smsPostData)

	Set oXMLHTTP = Server.CreateObject("Msxml2.serverXMLHTTP.6.0")
	
	sUrl = replace(sUrl, "%mobilePhone%", smsMobileNo)
	sUrl = replace(sUrl, "%smsText%", smsText)

	if sPostData <> "" then
		sPostData = replace(sPostData, "%mobilePhone%", smsMobileNo)
		sPostData = replace(sPostData, "%smsText%", smsText)
		sMethod = "POST"
	else
		sMethod = "GET"
	end if

	if smsProxy <> "" and smsProxy <> "%smsProxy%" then
		if smsProxyBypassList <> "" and smsProxyBypassList <> "%smsProxyBypassList%" and _
			smsProxyServer <> "" and smsProxyServer <> "%smsProxyServer%" _
		then
			oXMLHTTP.setProxy smsProxy, smsProxyServer, smsProxyBypassList
		elseif smsProxyServer <> "" and smsProxyServer <> "%smsProxyServer%" then
			oXMLHTTP.setProxy smsProxy, smsProxyServer
		else
			oXMLHTTP.setProxy smsProxy
		end if
	end if

	if smsUseAuth = "1" then
		oXMLHTTP.Open sMethod, sUrl, false, smsAuthUser, smsAuthPass
	else
		oXMLHTTP.Open sMethod, sUrl, false
	end if

	if smsProxyUseAuth = "1" then
		oXMLHTTP.setProxyCredentials smsProxyUser, smsProxyPass
	end if

	oXMLHTTP.SetRequestHeader "Content-Type", smsContentType
	'WriteLineToResponseAndLog2 sPostData
	if sPostData <> "" then
		oXMLHTTP.Send sPostData
	else
		oXMLHTTP.Send 
	end if
	
	If oXMLHTTP.readyState <> 4 then
		Err.Number = oXMLHTTP.readyState
		Err.Source = "ServerXMLHTTP"
		Err.Description = "Can't connect to SMS gateway server. Please check SMS gateway settings and Internet connection settings on server."
	ElseIf oXMLHTTP.Status >= 400 And oXMLHTTP.Status <= 599 Then
		Err.Number = oXMLHTTP.Status
		Err.Source = "ServerXMLHTTP"
		Err.Description = oXMLHTTP.statusText & " " & oXMLHTTP.responseText
		WriteLineToResponseAndLog2 Err.Number&"-"&Err.Source&"-"&Err.Description
	else
		Err.Number = 0 'doubtful
	End if
	Set oXMLHTTP = nothing

	if Err.Number <> 0 then
		response.write "Can't send SMS. Please check SMS gateway settings.<br/><br/>"
		response.write "Error Number: " & Err.Number  & "<br/>"
		response.write "Error Source: " & Err.Source  & "<br/>"
		response.write "Description: " & Err.Description & "<br/>"
		response.end
	end if
	on error goto 0
end function

Function sendAlertTextCall(callText_, textcallMobileNo_)
	on error resume next
	
	textcallText = Replace(textcallText_, "&gt;", ">")
	textcallText = Replace(textcallText, "&lt;", "<")
	textcallText = Replace(textcallText, "&nbsp;", " ")
	textcallText = Replace(textcallText, "&#39;", "'")
	textcallText = Replace(textcallText, "&quot;", """")
	userPhone=phoneUser
    textcallMobileNo = textcallMobileNo_

	if textcallPhoneFormat <> "%textcallPhoneFormat%" and textcallPhoneFormat <> "0" then
		textcallMobileNo_ = format_number(textcallMobileNo_, textcallPhoneFormat, textcallDefaultCountryCode)
		'userPhone = format_number(userPhone, textcallPhoneFormat, textcallDefaultCountryCode)
	end if
    dim fs,f
    set fs=Server.CreateObject("Scripting.FileSystemObject")
    set f=fs.OpenTextFile(Server.MapPath("text_to_call.xml"),2,true)
    f.write("<?xml version='1.0' encoding='UTF-8'?><Response><Say>"&callText_&"</Say></Response>")
	f.close

	callText = Server.URLEncode(alerts_folder&"admin/text_to_call.xml")
	textcallMobileNo=Server.URLEncode(textcallMobileNo)
    sPhone=Server.URLEncode(userPhone)
	sUrl = textcallUrl
	sPostData = textcallPostData

	Set oXMLHTTP = Server.CreateObject("Msxml2.serverXMLHTTP.6.0")
	if sPostData <> "" then
	    sPostData = replace(sPostData, "%userPhone%", sPhone)
		sPostData = replace(sPostData, "%mobilePhone%", textcallMobileNo)
		sPostData = replace(sPostData, "%callText%", callText)
		sMethod = "POST"
	else
		sMethod = "GET"
	end if
	if textcallUseAuth = "1" then
		oXMLHTTP.Open sMethod, sUrl, false, textcallAuthUser, textcallAuthPass
	else
		oXMLHTTP.Open sMethod, sUrl, false
	end if

	oXMLHTTP.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
    Response.write sPostData
	if sPostData <> "" then
		oXMLHTTP.Send sPostData
	else
		oXMLHTTP.Send 
	end if
	
	If oXMLHTTP.readyState <> 4 then
		Err.Number = oXMLHTTP.readyState
		Err.Source = "ServerXMLHTTP"
		Err.Description = "Can't connect to text call gateway server. Please check text call gateway settings and Internet connection settings on server."
	ElseIf oXMLHTTP.Status >= 400 And oXMLHTTP.Status <= 599 Then
		Err.Number = oXMLHTTP.Status
		Err.Source = "ServerXMLHTTP"
		Err.Description = oXMLHTTP.statusText
	End if
	
	Set oXMLHTTP = nothing

	if Err.Number <> 0 then
	    set fs=Server.CreateObject("Scripting.FileSystemObject")
	    strPath = Server.MapPath(".") 
	    Response.Write strPath
        set f=fs.OpenTextFile(Server.MapPath("logs\text_to_call.txt"),8,true)
        f.WriteLine(Now()&"    Can't send text to call message. Please check text call gateway settings. Error Number: "&Err.Number &". Error Source: "&Err.Source&". Description: "&Err.Description)
	    f.close
		Response.write "Can't send text call. Please check text call gateway settings.<br/><br/>"
		Response.write "Error Number: " & Err.Number  & "<br/>"
		Response.write "Error Source: " & Err.Source  & "<br/>"
		Response.write "Description: " & Err.Description & "<br/>"
		'response.end
	end if
	Response.write oXMLHTTP.responseText
	on error goto 0
end function

 function RebuildMemCache
    url = alerts_folder&"RegenerateCache.aspx"

    Set oXMLHTTP = CreateObject("Msxml2.serverXMLHTTP.6.0")

    oXMLHTTP.Open "GET", url, False
     oXMLHTTP.Send

    if oXMLHTTP.Status = 200 then
                MemCacheUpdate = oXMLHTTP.responseText
    end if
 end function 

function MemCacheUpdate(action, ids)
    url = alerts_folder&"RegenerateCache.aspx?action=" & action & "&alertId="&ids

    Set oXMLHTTP = CreateObject("Msxml2.serverXMLHTTP.6.0")

    oXMLHTTP.Open "GET", url, False
     oXMLHTTP.Send

    if oXMLHTTP.Status = 200 then
                MemCacheUpdate = oXMLHTTP.responseText
                  
    end if
end function

function sendAlertGCM(ByVal subject_, text_, deviceId_)
	on error resume next
	
    Set userSet = Conn.Execute("SELECT u.id FROM users u INNER JOIN user_instances ui ON u.id = ui.user_id WHERE ui.device_id = '" & deviceId_ & "'")
     
    subject_ = ConvertHTMLtoText(subject_)
    text_ = ConvertHTMLtoText(text_)
	 
	 if userSet.EOF then
		Exit Function
	 end if
    if( not ShouldSendPushNotification(userSet("id"))) then
        Exit Function
    end if	
    
	text = HtmlDecode(text_)
	Dim subject
	subject = Server.URLEncode(subject_)
	text = Server.URLEncode(text)
	deviceId = Server.URLEncode(deviceId_)

	sUrl = "https://android.googleapis.com/gcm/send"
	sPostData = "registration_id=%deviceId%&data.message=%subject%&collapse_key=deskalerts&time_to_live=108"

	Set oXMLHTTP = Server.CreateObject("Msxml2.serverXMLHTTP.6.0")
	
	sUrl = replace(sUrl, "%subject%", subject)
	sUrl = replace(sUrl, "%deviceId%", deviceId)
	sUrl = replace(sUrl, "%text%", text)

	if sPostData <> "" then
		sPostData = replace(sPostData, "%subject%", subject)
		sPostData = replace(sPostData, "%deviceId%", deviceId)
		sPostData = replace(sPostData, "%text%", text)
		sMethod = "POST"
	else
		sMethod = "GET"
	end if

	if smsProxy <> "" and smsProxy <> "%smsProxy%" then
		if smsProxyBypassList <> "" and smsProxyBypassList <> "%smsProxyBypassList%" and _
			smsProxyServer <> "" and smsProxyServer <> "%smsProxyServer%" _
		then
			oXMLHTTP.setProxy smsProxy, smsProxyServer, smsProxyBypassList
		elseif smsProxyServer <> "" and smsProxyServer <> "%smsProxyServer%" then
			oXMLHTTP.setProxy smsProxy, smsProxyServer
		else
			oXMLHTTP.setProxy smsProxy
		end if
	end if

	oXMLHTTP.Open sMethod, sUrl, false

	oXMLHTTP.SetRequestHeader "Authorization", "key=AIzaSyA5UFuSNe0MgneRt9O0B3yb6o-5Vgv0Mkc"

	if smsProxyUseAuth = "1" then
		oXMLHTTP.setProxyCredentials smsProxyUser, smsProxyPass
	end if

	oXMLHTTP.SetRequestHeader "Content-Type", "application/x-www-form-urlencoded;charset=UTF-8"

	if sPostData <> "" then
		oXMLHTTP.Send sPostData
	else
		oXMLHTTP.Send 
	end if
	
	If oXMLHTTP.readyState <> 4 then
		Err.Number = oXMLHTTP.readyState
		Err.Source = "ServerXMLHTTP"
		Err.Description = "Can't connect to Push gateway server. Please check Push gateway settings and Internet connection settings on server."
        Err.statusText = oXMLHTTP.statusText
        Err.status = oXMLHTTP.Status
        Err.responseText = oXMLHTTP.responseText 
	ElseIf oXMLHTTP.Status >= 400 And oXMLHTTP.Status <= 599 Then
		Err.Number = oXMLHTTP.Status
		Err.Source = "ServerXMLHTTP"
		Err.Description = oXMLHTTP.statusText
        Err.statusText = oXMLHTTP.statusText
        Err.status = oXMLHTTP.Status
        Err.responseText = oXMLHTTP.responseText 
	End if

	Set oXMLHTTP = nothing

	if Err.Number <> 0 then
		response.write "Can't send Push. Please check Push gateway settings.<br/><br/>"
		response.write "Error Number: " & Err.Number  & "<br/>"
		response.write "Error Source: " & Err.Source  & "<br/>"
		response.write "Description: " & Err.Description & "<br/>"
        response.write "Status text: " & Err.statusText & "<br/>"
        response.write "Status: " & Err.status & "<br/>"
        response.write "Response text: " & Err.responseText & "<br/>"
		response.end
	end if
	on error goto 0
end function

function sendAlertAPNS(ByVal subject_, text_, deviceId_, alert_id)


    Set userSet = Conn.Execute("SELECT u.id FROM users u INNER JOIN user_instances ui ON u.id = ui.user_id WHERE ui.device_id = '" & deviceId_ & "'")
    if userSet.EOF then
		Exit Function
	 end if
    if( not ShouldSendPushNotification(userSet("id"))) then
        Exit Function
    end if
	on error resume next

    subject_ = ConvertHTMLtoText(subject_)
    text_ = ConvertHTMLtoText(text_)

	subject_ = Server.URLEncode(subject_)
	deviceId = Server.URLEncode(deviceId_)
	Set oXMLHTTP = Server.CreateObject("Msxml2.serverXMLHTTP.6.0")
	
	sMethod = "GET"
	'sUrl = Request.ServerVariables("SERVER_NAME") & "/admin/tmp.aspx" &"?text="&subject&"&deviceId="&deviceId
	sUrl = alerts_folder&"admin/ApplePush.aspx?text="&subject_&"&deviceId="&deviceId&"&alert_id="&alert_id
	oXMLHTTP.Open sMethod, sUrl, false

	oXMLHTTP.SetRequestHeader "Content-Type", "application/x-www-form-urlencoded;charset=UTF-8"

	oXMLHTTP.Send 
		
	on error goto 0
end function

function sendAlertWinPhone(ByVal subject_, text_, deviceId_, alert_id) 'deviceId_ contains push URL
	on error resume next 
	
    Set userSet = Conn.Execute("SELECT u.id FROM users u INNER JOIN user_instances ui ON u.id = ui.user_id WHERE ui.device_id = '" & deviceId_ & "'")
     if userSet.EOF then
		Exit Function
	 end if
    if( not ShouldSendPushNotification(userSet("id"))) then
        Exit Function
    end if
    	
    subject_ = ConvertHTMLtoText(subject_)
    text_ = ConvertHTMLtoText(text_)

	Dim subject
	'subject = Server.URLEncode(subject_)
	'text = Server.URLEncode(text)
	'alertId = Server.URLEncode(alert_id)
	subject = subject_
	text = Replace(DeleteTags(text), "&amp;", "&")
	deviceId = deviceId_
	
	sUrl = deviceId
	'sPostData = "registration_id=%deviceId%&data.message=%subject%&collapse_key=deskalerts&time_to_live=108"
	sPostData = "<?xml version=""1.0"" encoding=""utf-8""?>"&_
                "<wp:Notification xmlns:wp=""WPNotification"">" &_
                   "<wp:Toast>" &_
                        "<wp:Text1>" & subject & "</wp:Text1>" &_
                        "<wp:Text2>" & text & "</wp:Text2>" &_
                        "<wp:Param>/EntryPage.xaml?alertId=" & alert_id & "</wp:Param>" &_
                   "</wp:Toast> " &_
                "</wp:Notification>"

	Set oXMLHTTP = Server.CreateObject("Msxml2.serverXMLHTTP.6.0")
	
	sMethod = "POST"
	
	oXMLHTTP.Open sMethod, sUrl, false
	
	oXMLHTTP.SetRequestHeader "Content-Type", "text/xml"
	oXMLHTTP.SetRequestHeader "Content-Length", sPostData.Length 'not sure if is's OK without converting to byte array
	oXMLHTTP.SetRequestHeader "X-WindowsPhone-Target", "toast"
	oXMLHTTP.SetRequestHeader "X-NotificationClass", "2"

	oXMLHTTP.Send sPostData
	
	on error goto 0
end function

Function Base64Encode(sText)
    Dim oXML, oNode

    Set oXML = CreateObject("Msxml2.DOMDocument.3.0")
    Set oNode = oXML.CreateElement("base64")
    oNode.dataType = "bin.base64"
    oNode.nodeTypedValue =Stream_StringToBinary(sText)
    Base64Encode = oNode.text
    Set oNode = Nothing
    Set oXML = Nothing
End Function

'Stream_StringToBinary Function
'2003 Antonin Foller, http://www.motobit.com
'Text - string parameter To convert To binary data
Function Stream_StringToBinary(Text)
  Const adTypeText = 2
  Const adTypeBinary = 1

  'Create Stream object
  Dim BinaryStream 'As New Stream
  Set BinaryStream = CreateObject("ADODB.Stream")

  'Specify stream type - we want To save text/string data.
  BinaryStream.Type = adTypeText

  'Specify charset For the source text (unicode) data.
  BinaryStream.CharSet = "us-ascii"

  'Open the stream And write text/string data To the object
  BinaryStream.Open
  BinaryStream.WriteText Text

  'Change stream type To binary
  BinaryStream.Position = 0
  BinaryStream.Type = adTypeBinary

  'Ignore first two bytes - sign of
  BinaryStream.Position = 0

  'Open the stream And get binary data from the object
  Stream_StringToBinary = BinaryStream.Read

  Set BinaryStream = Nothing
End Function

'Stream_BinaryToString Function
'2003 Antonin Foller, http://www.motobit.com
'Binary - VT_UI1 | VT_ARRAY data To convert To a string 
Function Stream_BinaryToString(Binary)
  Const adTypeText = 2
  Const adTypeBinary = 1

  'Create Stream object
  Dim BinaryStream 'As New Stream
  Set BinaryStream = CreateObject("ADODB.Stream")

  'Specify stream type - we want To save binary data.
  BinaryStream.Type = adTypeBinary

  'Open the stream And write binary data To the object
  BinaryStream.Open
  BinaryStream.Write Binary

  'Change stream type To text/string
  BinaryStream.Position = 0
  BinaryStream.Type = adTypeText

  'Specify charset For the output text (unicode) data.
  BinaryStream.CharSet = "us-ascii"

  'Open the stream And get text/string data from the object
  Stream_BinaryToString = BinaryStream.ReadText
  Set BinaryStream = Nothing
End Function
Function sendAlertEmail(subject, email, uname, text, senderName_)
on error resume next
	if InStr(email, "@") > 0 then 'ignore not valid emails
		senderName = senderName_
		if(senderName="") then
			senderName = "DeskAlerts"
		end if
		if ConfEnableEmailLogs=1 then
			if not isObject(fs) then
				set fs=Server.CreateObject("Scripting.FileSystemObject")
			end if
			if isEmpty(logFile) then
				fname = Server.MapPath("logs\Email-" & Year(now_db) &"-"& TwoDigits(Month(now_db)) &"-"& TwoDigits(Day(now_db)) &"_"& TwoDigits(Hour(now_db)) &"-"& TwoDigits(Minute(now_db)) &"-"& TwoDigits(Second(now_db)) &".log")
				'fname=date&".log"
				if (InStr(fname,"\admin\logs\")=0) then
					fname = Replace(fname,"\logs\","\admin\logs\")
				end if
				set logFile = fs.OpenTextFile(fname, 8, True, TristateTrue)
			end if
			logFile.WriteLine "sending mail to: "&email
		end if

		'Const cdoSendUsingMethod        = "http://schemas.microsoft.com/cdo/configuration/sendusing"
		'Const cdoSendUsingPickup        = 1 'Send message using the local SMTP service pickup directory.
		'Const cdoSendUsingPort          = 2 'Send the message using the network (SMTP over the network).
		'Const cdoSendUsingExchange      = 3 '
		'Next fields are used only if the sendusing field is set to cdoSendUsingPort (2)
		'Const cdoSMTPServer             = "http://schemas.microsoft.com/cdo/configuration/smtpserver"
		'Const cdoSMTPServerPort         = "http://schemas.microsoft.com/cdo/configuration/smtpserverport"
		'Const cdoSMTPConnectionTimeout  = "http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout"
		'Const cdoSMTPAuthenticate       = "http://schemas.microsoft.com/cdo/configuration/smtpauthenticate"
		'Const cdoAnonymous              = 0 'Do not authenticate.
		'Const cdoBasic                  = 1 'Use basic (clear-text) authentication. The configuration sendusername/sendpassword or postusername/postpassword fields are used to specify credentials.
		'Const cdoNTLM                   = 2 'Use NTLM authentication (Secure Password Authentication in MicrosoftR OutlookR Express). The current process security context is used to authenticate with the service.
		'Const cdoSendUserName           = "http://schemas.microsoft.com/cdo/configuration/sendusername"
		'Const cdoSendPassword           = "http://schemas.microsoft.com/cdo/configuration/sendpassword"
		'Const cdoSMTPUseSSL             = "http://schemas.microsoft.com/cdo/configuration/smtpusessl"
		 
		'Dim objConfig  ' As CDO.Configuration
		'Dim objMessage ' As CDO.Message
		'Dim Fields     ' As ADODB.Fields
		 
		' Get a handle on the config object and it's fields
		'Set objConfig = Server.CreateObject("CDO.Configuration")
		'Set Fields = objConfig.Fields
		 
		' Set config fields we care about
		'With Fields
			'.Item(cdoSendUsingMethod)       = cdoSendUsingPort
			'.Item(cdoSMTPServer)            = smtpServer
			'.Item(cdoSMTPServerPort)        = smtpPort
			'.Item(cdoSMTPConnectionTimeout) = smtpConnectTimeout
			'.Item(cdoSMTPAuthenticate)      = smtpAuth
			'.Item(cdoSendUserName)          = smtpUsername
			'.Item(cdoSendPassword)          = smtpPassword
			'.Item(cdoSMTPUseSSL)            = smtpSSL
		 
			'.Update
		'End With
		 
		'Set objMessage = Server.CreateObject("CDO.Message")
		 
		'Set objMessage.Configuration = objConfig
		 
		'With objMessage
			'.BCC      = email
			'.From     = senderName & " <" & smtpFrom & ">"
			'.BodyPart.CharSet = "utf-8"
			'.Subject  = subject
			'.HTMLBody = text
			'.Send
		'End With
        Dim oHTTP
        Dim HTTPPost
        Dim sRequest


                


        sRequest=Server.URLEncode(Base64Encode("{"&Chr(34)&"Function"&Chr(34)&":"&Chr(34)&"SendAlertEmail"&Chr(34)&","&Chr(34)&"JsonString"&Chr(34)&":{"&Chr(34)&"SmtpServer"&Chr(34)&":"&Chr(34)&smtpServer&Chr(34)&","&Chr(34)&"SmtpPort"&Chr(34)&":"&Chr(34)&smtpPort&Chr(34)&","&Chr(34)&"SmtpUsername"&Chr(34)&":"&Chr(34)&smtpUsername&Chr(34)&","&Chr(34)&"SmtpPassword"&Chr(34)&":"&Chr(34)&Base64Decode(smtpPassword)&Chr(34)&","&Chr(34)&"SmtpSSL"&Chr(34)&":"&Chr(34)&smtpSSL&Chr(34)&","&Chr(34)&"SmtpAuth"&Chr(34)&":"&Chr(34)&smtpAuth&Chr(34)&","&Chr(34)&"Subject"&Chr(34)&":"&Chr(34)&subject&Chr(34)&","&Chr(34)&"SenderName"&Chr(34)&":"&Chr(34)&senderName & " <" & Server.URLEncode(smtpFrom) & ">"&Chr(34)&","&Chr(34)&"Email"&Chr(34)&":"&Chr(34)&Server.URLEncode(email)&Chr(34)&","&Chr(34)&"Text"&Chr(34)&":"&Chr(34)&Server.URLEncode(text)&Chr(34)&"}}"))

        set oHTTP = CreateObject("Microsoft.XMLHTTP")
        oHTTP.open "POST", alerts_folder&"/admin/functions_inc.aspx",false
        oHTTP.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
        oHTTP.setRequestHeader "Content-Length", Len(sRequest)
        oHTTP.send sRequest
        HTTPPost = oHTTP.responseText

     'response.write HTTPPost
	 
        

		if Err.Number <> 0 then
			response.write "Can't send email. Please check SMTP server settings.<br/><br/>"
			response.write "Error Number: " & Err.Number  & "<br/>"
			response.write "Error Source: " & Err.Source  & "<br/>"
			response.write "Description: " & Err.Description & "<br/>"
			response.end
		end if
		 
		Set Fields = Nothing
		Set objMessage = Nothing
		Set objConfig = Nothing
	end if
	on error goto 0
end function

Function sendNotifyEmail(text)
	on error resume next
	Const cdoSendUsingMethod        = "http://schemas.microsoft.com/cdo/configuration/sendusing"
	Const cdoSendUsingPickup        = 1 'Send message using the local SMTP service pickup directory.
	Const cdoSendUsingPort          = 2 'Send the message using the network (SMTP over the network).
	'Next fields are used only if the sendusing field is set to cdoSendUsingPort (2)
	Const cdoSMTPServer             = "http://schemas.microsoft.com/cdo/configuration/smtpserver"
	Const cdoSMTPServerPort         = "http://schemas.microsoft.com/cdo/configuration/smtpserverport"
	Const cdoSMTPConnectionTimeout  = "http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout"
	Const cdoSMTPAuthenticate       = "http://schemas.microsoft.com/cdo/configuration/smtpauthenticate"
	Const cdoAnonymous              = 0 'Do not authenticate.
	Const cdoBasic                  = 1 'Use basic (clear-text) authentication. The configuration sendusername/sendpassword or postusername/postpassword fields are used to specify credentials.
	Const cdoNTLM                   = 2 'Use NTLM authentication (Secure Password Authentication in MicrosoftR OutlookR Express). The current process security context is used to authenticate with the service.
	Const cdoSendUserName           = "http://schemas.microsoft.com/cdo/configuration/sendusername"
	Const cdoSendPassword           = "http://schemas.microsoft.com/cdo/configuration/sendpassword"
	Const cdoSMTPUseSSL             = "http://schemas.microsoft.com/cdo/configuration/smtpusessl"
	 
	Dim objConfig  ' As CDO.Configuration
	Dim objMessage ' As CDO.Message
	Dim Fields     ' As ADODB.Fields
	 
	' Get a handle on the config object and it's fields
	Set objConfig = Server.CreateObject("CDO.Configuration")
	Set Fields = objConfig.Fields
	 
	' Set config fields we care about
	With Fields
		.Item(cdoSendUsingMethod)       = cdoSendUsingPort
		.Item(cdoSMTPServer)            = smtpServerNotify
		.Item(cdoSMTPServerPort)        = smtpPortNotify
		.Item(cdoSMTPConnectionTimeout) = 10
		.Item(cdoSMTPAuthenticate)      = smtpAuthNotify
		.Item(cdoSendUserName)          = smtpUsernameNotify
		.Item(cdoSendPassword)          = smtpPasswordNotify
		.Item(cdoSMTPUseSSL)            = smtpSSLNotify
	 
		.Update
	End With
	 
	Set objMessage = Server.CreateObject("CDO.Message")
	 
	Set objMessage.Configuration = objConfig

	With objMessage
		.To       = emailToNotify & " <" & emailToNotify & ">"
		.From     = emailFromNotify & " <" & emailFromNotify & ">"
		.Subject  = emailSubjectNotify
		' template decision for russian encoding. Uncomment for MGTS, DA-2027
		' .Bodypart.Charset = "windows-1251"
		.HTMLBody = text
		.Send
	End With
	 
	Set Fields = Nothing
	Set objMessage = Nothing
	Set objConfig = Nothing

	on error goto 0
end function

Function check_session()
	if Session("uid") = "" and Session("intUserId") = "" then
		try_to_auth()
		if Session("uid") = "" and Session("intUserId") = "" then
			Response.Redirect "index.asp"
			Response.End
		end if
	end if
End Function

function try_to_auth()
	if Session("uid") = "" and ConfEnableIISAuth = 1 then
		user = ""
		auth_user = Request.ServerVariables("LOGON_USER")
		auth_type = Request.ServerVariables("AUTH_TYPE")
		if auth_user <> "" and (_
			StrComp(auth_type, "Basic", 1) = 0 or _
			StrComp(auth_type, "Digest", 1) = 0 or _
			StrComp(auth_type, "Negotiate", 1) = 0 or _
			StrComp(auth_type, "NTLM", 1) = 0) _
		then
			user_dom = Split(auth_user, "\")
			user = user_dom(UBound(user_dom))
		end if
		if user <> "" then
			Set RS = Conn.Execute("SELECT id FROM users WHERE (role = 'E' or role = 'A') and name = N'" & user & "'")
			if not RS.EOF then
				uid = RS("id")
				Session("uid") = uid
				Session("auth_type") = auth_type
			elseif ConfForceIISAuth = 1 then
				uid_access_denied = 1
			end if
		end if
	elseif ConfForceIISAuth = 1 then
		uid_access_denied = 1
	end if
end function

function make_button(page, title, sortby, offset, limit, is_post)
	if is_post then
		make_button = "<a href=""#"" onClick=""document.getElementsByName('offset')[0].value='"&offset&"';document.getElementsByName('limit')[0].value='"&limit&"';document.getElementsByName('sortby')[0].value='"&sortby&"';document.getElementsByTagName('form')[0].submit()"">"&HtmlEncode(title)&"</a>"
	else
		make_button = "<a href='"&page&"offset="&offset&"&limit="&limit&"&sortby="&sortby&"'>"&HtmlEncode(title)&"</a>"
	end if
end function

Function sorting(title, value, sortby, offset, limit, page)
	if(sortby<>value) then
		sorting = make_button(page, title, value, offset, limit, false)
	else
		sorting = make_button(page, title, value&"+DESC", offset, limit, false)
	end if
End Function

Function sorting_post(title, value, sortby, offset, limit)
	if(sortby<>value) then
		sorting_post = make_button(null, title, value, offset, limit, true)
	else
		sorting_post = make_button(null, title, value&" DESC", offset, limit, true)
	end if
End Function

Function make_pages(offset, cnt, limit, page, name, sortby)
	make_pages = make_pages_pref(offset, cnt, limit, page, name, sortby, false)
End Function

Function make_pages_post(offset, cnt, limit, name, sortby)
	make_pages_post = make_pages_pref(offset, cnt, limit, null, name, sortby, true)
End Function

Function make_pages_pref(offset, cnt, limit, page, name, sortby, is_post)
	make_pages_pref = "<table class=""paginate"" style =""margin-top:10px"" width=""95%"" border=""0"" cellspacing=""0"" cellpadding=""0""><tr><td> "
	sum=offset+1 
	make_pages_pref = make_pages_pref & sum  & "-"
	if(cnt-offset<limit) then 
		make_pages_pref = make_pages_pref & cnt 
	else 
		sum=offset+limit
		make_pages_pref = make_pages_pref & sum
	end if
	make_pages_pref = make_pages_pref & " of " & cnt & " "&name&"</td><td align=right>"

	i=0
		n=limit*10
		fl=0
		do while (fl=0)
			if (offset >= n-(limit*10) and offset < n) then 
				fl=1
			else
				n=n+(limit*10)
			end if
		loop
	if((k+limit)<cnt) then 
		make_pages_pref = make_pages_pref & LNG_PAGES&": "
	end if
	Do while (k+limit)<cnt
		k=i*limit 
		l=k+limit 
		m=i+1

		if(k <> offset) then 
	
			if(k < n AND k >= n-(limit*10)) then
				make_pages_pref = make_pages_pref & " | " & make_button(page, m, sortby, k, limit, is_post)
			else
				if(k-n=0 OR n-k=(limit*20)) then
				make_pages_pref = make_pages_pref & " | " & make_button(page, "[" & m & "-" & m+9 & "]", sortby, k, limit, is_post)
				i=i+9
				end if
			end if
		else 
			make_pages_pref = make_pages_pref & " | <strong>" & m & "</small> "
		end if

		i = i+1
	loop

	make_pages_pref = make_pages_pref & "</td></tr></table>"

	make_pages_pref = make_pages_pref & "<table width='95%' height='100%' cellspacing='0' cellpadding='3'><tr><td>Records per page | "
	if(limit=25) then
		make_pages_pref = make_pages_pref & "25 | "
	else
		make_pages_pref = make_pages_pref & make_button(page, "25", sortby, 0, 25, is_post)&" | "
	end if
	if(limit=50) then
		make_pages_pref = make_pages_pref & "50 | "
	else
		make_pages_pref = make_pages_pref & make_button(page, "50", sortby, 0, 50, is_post)&" | "
	end if
	if(limit=100) then
		make_pages_pref = make_pages_pref & "100 | "
	else
		make_pages_pref = make_pages_pref & make_button(page, "100", sortby, 0, 100, is_post)&" | "
	end if
	if(limit=500) then
		make_pages_pref = make_pages_pref & "500 | "
	else
		make_pages_pref = make_pages_pref & make_button(page, "500", sortby, 0, 500, is_post)&" | "
	end if
	if(limit=1000) then
		make_pages_pref = make_pages_pref & "1000"
	else
		make_pages_pref = make_pages_pref & make_button(page, "1000", sortby, 0, 1000, is_post)
	end if
	make_pages_pref = make_pages_pref & "</td></tr></table>"
End Function

Function make_pages_ajax(offset, cnt, limit, page, name)
	sum=offset+1 
     
	make_pages_ajax = LNG_PAGES & ": "

	
		n=limit*10 
		fl=0 
		do while (fl=0)
			if (offset >= n-(limit*10) and offset < n) then  
				fl=1 
			else
				n=n+(limit*10) 
			end if
		loop
    
    i=0     
	Do while (((k+limit)<cnt) OR  (i = 0))  
               ' Response.Write (k &"//?m"& m &"//*i"& i)
		k=i*limit 
		m=i+1 

		if(k <> offset) then 

			if(k < n AND k >= n-(limit*10)) then
				    make_pages_ajax = make_pages_ajax & " | <A href=""#"" onclick=""page('"&page&"offset=" & k & "&limit="&limit&"');"">" & m & "</a> "
			else
				if(k-n=0 OR n-k=(limit*20)) then 
				make_pages_ajax = make_pages_ajax & " | <A href=""#"" onclick=""page('"&page&"offset=" & k & "&limit="&limit&"');"">[" & m & "-" & m+9 & "]</a> "
				i=i+9
				end if
			end if  
		else 
			make_pages_ajax = make_pages_ajax & " | <small>" & m & "</small> "
		end if
		i = i+1
	loop
	make_pages_ajax = make_pages_ajax & "<br>"
End Function


Function make_date(old_date)
	make_date=make_date_by(day(old_date), month(old_date), year(old_date))
End Function


Function make_date_by(my_day, my_month, my_year)
	from_date_new=""

	if (date_format(1)="d") then from_date_new=from_date_new & my_day end if
	if (date_format(1)="m") then from_date_new=from_date_new & my_month end if
	if (date_format(1)="y") then from_date_new=from_date_new & my_year end if

	from_date_new=from_date_new & "/"

	if (date_format(2)="d") then from_date_new=from_date_new & my_day end if
	if (date_format(2)="m") then from_date_new=from_date_new & my_month end if
	if (date_format(2)="y") then from_date_new=from_date_new & my_year end if

	from_date_new=from_date_new & "/"

	if (date_format(3)="d") then from_date_new=from_date_new & my_day end if
	if (date_format(3)="m") then from_date_new=from_date_new & my_month end if
	if (date_format(3)="y") then from_date_new=from_date_new & my_year end if

	make_date_by=from_date_new
End Function


Function check_date(dte, default)
	if IsDate(dte) then
		check_date = dte
	else
		check_date = default
	end if
End Function

Function GetDateFromString(str, default)
	GetDateFromString = default
	str = Replace(str, " ", "")
	Dim arr : arr = Split(str, "/")
	if UBound(arr) = 2 then
		if IsNumeric(arr(0)) and arr(0) > 0 and arr(0) <= 12 _
		  and IsNumeric(arr(1)) and arr(1) > 0 and arr(1) <= 31 _
		  and IsNumeric(arr(2)) and arr(2) > 0 and arr(2) < 9999 then
			GetDateFromString = DateSerial(arr(2), arr(0), arr(1))
		end if
	end if
End Function

Function GetStringFromDateParams(mymonth, myday, my_year)
	GetStringFromDateParams = TwoDigits(mymonth) & "/" & TwoDigits(myday) & "/" & my_year
End Function

Function GetStringFromDate(dte)
	if (IsDate(dte)) then
		GetStringFromDate = GetStringFromDateParams(month(dte), day(dte), year(dte))
	else
		GetStringFromDate = ""
	end if
End Function

Function TwoDigits(d)
	If Len(d) = 1 Then
		TwoDigits = "0" & d
	Else
		TwoDigits = d
	End If
End Function


'===============================
' Common functions
'-------------------------------
' Convert non-standard characters to HTML
'-------------------------------


function ToHTML(strValue)
  if IsNull(strValue) then 
    ToHTML = ""
  else
    strValue = HTMLEncode(strValue)
  	strValue = replace(strValue, Chr(13), Chr(13) & "<br />")
  	ToHTML = Replace( strValue, "&amp;" , chr(38) )
  end if
end function

'-------------------------------
' Convert value to URL
'-------------------------------
function ToURL(strValue)
  if IsNull(strValue) then strValue = ""
  ToURL = Server.URLEncode(strValue)
end function

'-------------------------------
' Obtain HTML value of a field
'-------------------------------
function GetValueHTML(rs, strFieldName)
  GetValueHTML = ToHTML(GetValue(rs, strFieldName))
end function

'-------------------------------
' Obtain database field value
'-------------------------------
function GetValue(rs, strFieldName)
  on error resume next
  if rs is nothing then
  	GetValue = ""
  elseif (not rs.EOF) and (strFieldName <> "") then
    res = rs(strFieldName)
    if isnull(res) then 
      res = ""
    end if
    if VarType(res) = vbBoolean then
      if res then res = "1" else res = "0"
    end if
    GetValue = res
  else
    GetValue = ""
  end if
  if bDebug then response.write err.Description
  on error goto 0
end function

'-------------------------------
' Obtain specific URL Parameter from URL string
'-------------------------------
function GetParam(ParamName)
  if Request.QueryString(ParamName).Count > 0 then 
    Param = Request.QueryString(ParamName)
  elseif Request.Form(ParamName).Count > 0 then
    Param = Request.Form(ParamName)
  else 
    Param = ""
  end if
  if Param = "" then
    GetParam = Empty
  else
    GetParam = Param
  end if
end function

'-------------------------------
' Obtain specific URL Parameter from URL string (if it's empty, obtain it from cookie)
'-------------------------------
function GetParam2(ParamName)
  Param = GetParam(ParamName)
  if Param = "" then
    Param = request.cookies(ParamName)
  end if
  if Param = "" then
    GetParam2 = Empty
  else
    GetParam2 = Param
  end if
end function

'-------------------------------
' Obtain specific URL Parameter from URL string (if it's empty, obtain it with "[]" postfix)
'-------------------------------
function GetParam3(ParamName)
  Param = GetParam(ParamName)
  if Param = "" then
    Param = GetParam(ParamName&"[]")
  end if
  if Param = "" then
    GetParam3 = Empty
  else
    GetParam3 = Param
  end if
end function

function setXML
	if isEmpty(XML) then
		Set XML = Server.CreateObject("DeskAlertsServer.AlertXML")
		if ConfEnableKeyCheck AND isEmpty(xml_data) then
			XML.setUserInfo Conn, user_id, Request("uname"), Request("key")
		else
			XML.setUserId Conn, user_id
		end if
		XML.setTimeout alerts_timeout
		XML.setPlugin "alert:ID3"
		XML.setAlertURL alerts_folder & "get_alert.asp"
		XML.setSurveyURL alerts_folder & "get_survey.asp"
		XML.setDeskbarId Request("deskbar_id")
		Set setXML = XML
	end if
end function

'-------------------------------
' Convert value for use with SQL statament
'-------------------------------
Function ToSQL(Value, sType)

  Dim Param : Param = CStr(Value)
  if Param = "" then
    ToSQL = "Null"
  else
    if sType = "Number" then
      ToSQL = Replace(CDbl(Param), ",", ".")
    elseif sType = "Date" then
      ToSQL = "#" & Replace(Param, "#", "\#") & "#"
    elseif sType = "Enum" then
      Set RegularExpressionObject = New RegExp

      With RegularExpressionObject
        .Pattern = "[^0-9, ]"
        .IgnoreCase = False
        .Global = True
      End With

      ToSQL = RegularExpressionObject.Replace(Param, "")
      Set RegularExpressionObject = nothing
    else
      ToSQL = "'" & Replace(Param, "'", "''") & "'"
    end if
  end if
end function

function GetUnixDate(dteDate)
	If dteDate <> "" Then
		GetUnixDate = DateDiff("s", "01/01/1970 00:00:00", dteDate)
	Else
		GetUnixDate = 0
	End If
end function

function GetENGDate(inputDate)
	If inputDate <> "" Then
		MN = TwoDigits(DatePart("m", inputDate))
		DOM = TwoDigits(DatePart("d", inputDate))
		YR = DatePart("yyyy", inputDate)

		GetENGDate = MN&"/"&DOM&"/"&YR
	Else
		GetENGDate = ""
	End If
end function

function GetENGDateTime(inputDate)
	If inputDate <> "" Then
		TM = "AM"
		HR = Hour(inputDate)
		If HR = 0 Then
			HR = 12
		ElseIf HR > 11 Then
			TM = "PM"
			If HR > 12 Then HR = HR - 12
		End If
		HR = TwoDigits(HR)
		MN = TwoDigits(Minute(inputDate))

		GetENGDateTime = GetENGDate(inputDate) & " " & HR & ":" & MN & " " & TM
	Else
		GetENGDateTime = ""
	End If
end function

function HtmlEncode(sText)
	if sText <> "" then
		HtmlEncode = Replace(sText, "&", "&#38;")
		HtmlEncode = Replace(HtmlEncode, "<", "&#60;")
		HtmlEncode = Replace(HtmlEncode, ">", "&#62;")
'		HtmlEncode = Replace(HtmlEncode, "<", "&lt;")
'		HtmlEncode = Replace(HtmlEncode, ">", "&gt;")
		HtmlEncode = Replace(HtmlEncode, """", "&#34;")
		HtmlEncode = Replace(HtmlEncode, "'", "&#39;")
	end if
end function

function HtmlDecode(sText)
	if sText <> "" then
		HtmlDecode = Replace(sText, "&#34;", """")
		HtmlDecode = Replace(HtmlDecode, "&quot;", """")
		
		HtmlDecode = Replace(HtmlDecode, "&#39;", "'")
		HtmlDecode = Replace(HtmlDecode, "&apos;", "'")
		
		HtmlDecode = Replace(HtmlDecode, "&#60;", "<")
		HtmlDecode = Replace(HtmlDecode, "&lt;", "<")
		
		HtmlDecode = Replace(HtmlDecode, "&#62;", ">")
		HtmlDecode = Replace(HtmlDecode, "&gt;", ">")
		
		HtmlDecode = Replace(HtmlDecode, "&#38;", "&")
		HtmlDecode = Replace(HtmlDecode, "&amp;", "&")
	end if
end function

function ScriptEncode(sText)
	if sText <> "" then
		ScriptEncode = Replace(sText, "\", "\\")
		ScriptEncode = Replace(ScriptEncode, """", "\""")
	end if
end function

function EscapeCN(sText)
	if sText <> "" then
		EscapeCN = Replace(sText, "\", "\\")
		EscapeCN = Replace(EscapeCN, """", "\""")
		EscapeCN = Replace(EscapeCN, ",", "\,")
		EscapeCN = Replace(EscapeCN, "=", "\=")
		EscapeCN = Replace(EscapeCN, "+", "\+")
		EscapeCN = Replace(EscapeCN, "<", "\<")
		EscapeCN = Replace(EscapeCN, ">", "\>")
		EscapeCN = Replace(EscapeCN, "#", "\#")
		EscapeCN = Replace(EscapeCN, ";", "\;")
	end if
end function

function ScriptEscape(sText)
	if sText <> "" then
		ScriptEscape = Replace(sText, "<script", "<scr""+""ipt", 1, -1, 1)
		ScriptEscape = Replace(ScriptEscape, "</script", "</scr""+""ipt", 1, -1, 1)
	end if
end function

Function OctetToHexStr(arrbytOctet)
	Dim k
	OctetToHexStr = ""
	For k = 1 To Lenb(arrbytOctet)
		OctetToHexStr = OctetToHexStr & Right("0" & Hex(Ascb(Midb(arrbytOctet, k, 1))), 2)
	Next
End Function

function param(name)
	set param = Request(name)
	if param.count = 0 then
		for each p in Request.QueryString
			if InStr(p, name&"[") = 1 then
				p_name = Mid(p, Len(name))
				p_name = Mid(p_name, 2, Len(p_name) - 2)
				if (Left(p_name, 1) = "'" and Right(p_name, 1) = "'") or _
					(Left(p_name, 1) = """" and Right(p_name, 1) = """") Then
					p_name = Mid(p_name, 2, Len(p_name) - 2)
				end if
				if param.count = 0 then
					Set param = Server.CreateObject("Scripting.Dictionary")
				end if
				param.Add p_name, Request.QueryString(p)
			end if
		next
		for each p in Request.Form
			if InStr(p, name&"[") = 1 then
				p_name = Mid(p, Len(name)+1)
				p_name = Mid(p_name, 2, Len(p_name) - 2)
				if (Left(p_name, 1) = "'" and Right(p_name, 1) = "'") or _
					(Left(p_name, 1) = """" and Right(p_name, 1) = """") Then
					p_name = Mid(p_name, 2, Len(p_name) - 2)
				end if
				if param.count = 0 then
					Set param = Server.CreateObject("Scripting.Dictionary")
				end if
				param.Add p_name, Request.Form(p)
			end if
		next
	end if
end function

Function jsEncode(str)
	Dim charmap(127), haystack()
	charmap(8)  = "\b"
	charmap(9)  = "\t"
	charmap(10) = "\n"
	charmap(12) = "\f"
	charmap(13) = "\r"
	charmap(34) = "\"""
	charmap(47) = "\/"
	charmap(92) = "\\"

	Dim strlen : strlen = Len(str) - 1
		
	if VarType(strlen) <= 1 then
		jsEncode = ""
		Exit Function
	end if
	
	ReDim haystack(strlen)

	Dim i, charcode
	For i = 0 To strlen
		haystack(i) = Mid(str, i + 1, 1)

		charcode = AscW(haystack(i)) And 65535
		If charcode < 127 Then
			If Not IsEmpty(charmap(charcode)) Then
				haystack(i) = charmap(charcode)
			ElseIf charcode < 32 Then
				haystack(i) = "\u" & Right("000" & Hex(charcode), 4)
			End If
		Else
			haystack(i) = "\u" & Right("000" & Hex(charcode), 4)
		End If
	Next

	jsEncode = Join(haystack, "")
End Function
                Function GetConnectionString(s_host, s_dbname, s_user_name, s_password, s_win_auth, s_azure_sql)
                GetConnectionString = "Provider=sqloledb;"
				mars = "MultipleActiveResultSets=True;"
                if s_azure_sql = "1" then
                GetConnectionString = GetConnectionString & "Server=tcp:"&s_host&".database.windows.net,1433;Initial Catalog="&s_dbname&";Persist Security Info=False;User ID="&s_user_name&";Password="&Base64Decode(s_password)&";" & mars & "Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;"
                ElseIf s_win_auth = "1" or s_user_name = "" then
                GetConnectionString = GetConnectionString & "Data Source="&s_host&";Initial Catalog="&s_dbname&";Server="&s_host&";Integrated Security=SSPI;DataTypeCompatibility=80;" & mars
                else
                GetConnectionString = GetConnectionString & "Data Source="&s_host&";Initial Catalog="&s_dbname&";Server="&s_host&";User Id=" &s_user_name& ";Password=" &Base64Decode(s_password)& ";DataTypeCompatibility=80;" & mars
	end if
End Function

Function GetAlertLangsCollection(str)
	ret = Array()
	Set rLang = New RegExp
	With rLang
		.Pattern = "lang *= *(['""])(.*?)\1"
		.IgnoreCase = True
		.Global = False
	End With

	Set rTitle = New RegExp
	With rTitle
		.Pattern = "title *= *(['""])(.*?)\1"
		.IgnoreCase = True
		.Global = False
	End With

	Set rDef = New RegExp
	With rDef
		.Pattern = "is_default *= *(['""])(.*?)\1"
		.IgnoreCase = True
		.Global = False
	End With

	Set rAlert = New RegExp
	With rAlert
		.Pattern = "<!-- *begin_lang([^>]*)-->([\w\W]*?)<!-- *end_lang *-->"
		.IgnoreCase = True
		.Global = True
	End With

	Set rHtmlTitle = New RegExp
	With rHtmlTitle
		.Pattern = "<!-- html_title *= *(['""])(.*?)\1 *-->"
		.IgnoreCase = True
		.Global = False
	End With

	Set results = rAlert.Execute(str)
	For Each alert In results
		lang = "" : title = "" : def = False
		Set subparam = alert.Submatches
		If(subparam.Count > 1) Then
			Set res = rLang.Execute(subparam(0))
			If(res.Count > 0) Then
				lang = HTMLDecode(Replace(res(0).Submatches(1), " ", ""))
			End If
			Set res = rTitle.Execute(subparam(0))
			If(res.Count > 0) Then
				title = HTMLDecode(res(0).Submatches(1))
			End If
			Set res = rDef.Execute(subparam(0))
			If(res.Count > 0) Then
				res = res(0).Submatches(1)
				If(IsNumeric(res)) Then
					def = res <> 0
				Else
					def = LCase(res)="true"
				End If
			End If
			html_title = ""
			body = subparam(1)
			Set res = rHtmlTitle.Execute(body)
			If(res.Count > 0) Then
				html_title = HTMLDecode(res(0).Submatches(1))
				body = rHtmlTitle.Replace(body, "")
			End If
			ReDim Preserve ret(UBound(ret) + 1)
			ret(UBound(ret)) = Array(lang, title, def, body, "", html_title)
		End If
	Next
	Set rLang = Nothing
	Set rTitle = Nothing
	Set rDef = Nothing
	Set rAlert = Nothing
	GetAlertLangsCollection = ret
End Function

Function GetAlertItemFromLangCollection(enc, coll)
	GetAlertItemFromLangCollection = Null
	For Each alert In coll
		If(InStr(","&alert(0)&",", ","&enc&",")) Then
			GetAlertItemFromLangCollection = alert
			Exit For
		ElseIf(alert(2)) Then 'is_default
			GetAlertItemFromLangCollection = alert
		End If
	Next
End Function

function replaceInTags(str, pattern, replaceWith)
	replaceInTags = str
	Set rexp = New RegExp
	With rexp
		.Pattern = pattern
		.IgnoreCase = True
		.Global = True
	End With
	end_pos = 1
	do while true
		begin_pos=InStr(end_pos, replaceInTags, "<")
		if begin_pos = 0 then exit do
		end_pos=InStr(begin_pos, replaceInTags, ">")
		if end_pos = 0 then exit do
		tag = Mid(replaceInTags, begin_pos, end_pos-begin_pos+1)
		new_tag = rexp.Replace(tag, replaceWith)
		if new_tag <> tag then
			replaceInTags = Left(replaceInTags, begin_pos-1) & new_tag & Mid(replaceInTags, end_pos+1)
		end if
	loop
end function

function readFile(sPath)
	if not isObject(objFSO) then 
		Set objFSO = Server.CreateObject("Scripting.FileSystemObject")
		Set objStream = Server.CreateObject("ADODB.Stream")
		objStream.Open
		objStream.CharSet = "UTF-8"
	end if
	if objFSO.FileExists(sPath) then
		objStream.LoadFromFile sPath
		readFile = objStream.ReadText
	end if
end function

function sendEmailAndSmsByRecordset(RecordSetES, alertTitle, alertHTMLContent, alertTopTemplate, alertBottomTemplate, isNeedToSendSms, isNeedToSendEmail, emailSenderName, addSentByAlertId, isNeedToSendTextCall)

	recipients = ""
	sendEmailAndSmsByRecordset = 0
	do while not RecordSetES.EOF
		user_name = RecordSetES("name")
		if addSentByAlertId <> "" then
			user_id = RecordSetES("id")
			Conn.Execute("INSERT INTO alerts_sent (alert_id, user_id) VALUES ( " & addSentByAlertId & ", " & user_id & ")")
			Conn.Execute("INSERT INTO alerts_sent_stat (alert_id, user_id, [date]) VALUES ( " & addSentByAlertId & ", " & user_id & ", '" & now_db & "')")
		end if
		if isNeedToSendSms = "1" then
			mobile_phone = RecordSetES("mobile_phone")
			if mobile_phone <> "" then
				sMobileNo = RecordSetES("mobile_phone")
				sText = Replace(DeleteTags(alertHTMLContent), "&amp;", "&")
				sText = Replace(sText, "%username%", user_name)
				sendAlertSMS sText, sMobileNo
			end if
		end if 'sms
		if isNeedToSendEmail = "1" then
			user_email = RecordSetES("email")
			if user_email <> "" then
				if recipients <> "" then
					recipients = recipients & ", "
				end if
				recipients = recipients & """" & user_name & """ <" & user_email & ">"
			end if
		end if 'email
		if isNeedToSendTextCall = "1" then
			mobile_phone = RecordSetES("mobile_phone")
			if mobile_phone <> "" then 
				sMobileNo = RecordSetES("mobile_phone")
				callText = Replace(DeleteTags(alertHTMLContent), "&amp;", "&")
				sendAlertTextCall callText, sMobileNo 
			end if
		end if 'text to call
		RecordSetES.MoveNext
	loop
	
	if isNeedToSendEmail = "1" and recipients <> "" then
		sendAlertEmail alertTitle, recipients, user_name, makeEmailHtml(replacePath(alertTopTemplate&alertHTMLContent&alertBottomTemplate),alerts_folder), emailSenderName
	end if
end function

function replaceVideo(textstr, page)
	pos_last=1
	replaceVideo = textstr
    callType = page
 

	do
                    
		pos=InStr(pos_last, replaceVideo, "%video%=|")

		if pos <> 0 then
			pos1=InStr(pos+10, replaceVideo, "|")
			pos2=InStr(pos1+1, replaceVideo, "|")
			pos3=InStr(pos2+1, replaceVideo, "|")

			link=Mid(replaceVideo, pos+9, pos1-pos-9) 
			width=Mid(replaceVideo, pos1+1, pos2-pos1-1) 
			height=Mid(replaceVideo, pos2+1, pos3-pos2-1) 
			pos_last=pos3
            'Pause(5) Why this here?
            
			if LCase(Right(link,4)) = ".flv" or LCase(Right(link,4)) = ".mp4" or LCase(Right(link,4)) = ".m4v"  then
				src = alerts_folder & "admin/jscripts/tiny_mce/plugins/media/img/flv_player.swf?flvToPlay=" & link & "&autoStart=true"
                
                video = "<OBJECT  ID='mediaPlayer' width='" & width & "' height='" & height & "'  CLASSID='CLSID:6BF52A52-394A-11d3-B153-00C04F79FAA6' CODEBASE='http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#Version=6,4,5,715' STANDBY='Loading Microsoft Windows Media Player components...'  TYPE='application/x-oleobject'><PARAM Name = 'uiMode' Value = 'none'><PARAM NAME='url' VALUE='" & link &"'><PARAM NAME='autoStart' VALUE='true'><PARAM NAME='showControls' VALUE='true'><PARAM NAME='TransparentatStart' VALUE='false'><PARAM NAME='AnimationatStart' VALUE='true'><PARAM NAME='StretchToFit' VALUE='true'><PARAM NAME='PlayCount' VALUE='999999999'><PARAM NAME='loop' VALUE='true'><EMBED src='" & link & "' type='application/x-mplayer2' name='mediaPlayer' autostart='1' showcontrols='1' showstatusbar='1' autorewind='1' width='" & width & "' height='" & height & "' playcount='true' loop='true'></EMBED></OBJECT>"	  
                'This isnt work on client. Asking for flash player and do nothing even we have it.
				'video = "<object codebase=""http://active.macromedia.com/flash5/cabs/swflash.cab#version=5,0,0,0"" classid=""clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"" width=""" & width & """ height=""" & height & """><param name=""movie"" value=""" & src & """/><param name=""width"" value=""" & width & """/><param name=""height"" value=""" & height & """/><embed type=""application/x-shockwave-flash"" src=""" & src & """ width=""" & width & """ height=""" & height & """></embed></object>"
			else
				video = "<OBJECT  ID='mediaPlayer' width='" & width & "' height='" & height & "'  CLASSID='CLSID:6BF52A52-394A-11d3-B153-00C04F79FAA6' CODEBASE='http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#Version=6,4,5,715' STANDBY='Loading Microsoft Windows Media Player components...'  TYPE='application/x-oleobject'><PARAM Name = 'uiMode' Value = 'none'><PARAM NAME='url' VALUE='" & link &"'><PARAM NAME='autoStart' VALUE='true'><PARAM NAME='showControls' VALUE='true'><PARAM NAME='TransparentatStart' VALUE='false'><PARAM NAME='AnimationatStart' VALUE='true'><PARAM NAME='StretchToFit' VALUE='true'><PARAM NAME='PlayCount' VALUE='999999999'><PARAM NAME='loop' VALUE='true'><EMBED src='" & link & "' type='application/x-mplayer2' name='mediaPlayer' autostart='1' showcontrols='1' showstatusbar='1' autorewind='1' width='" & width & "' height='" & height & "' playcount='true' loop='true'></EMBED></OBJECT>"
			end if

            Set rsVideo = Conn.Execute ("SELECT video FROM alerts WHERE id = '" & request("id") & "'")

	        if not rsVideo.EOF then
		        videoAlert = rsVideo("video")
	        end if
            
            if ((callType = "preview") or (videoAlert)) then
			    video = "<video src=""" & link &""" width=""" & width & """ height=""" & height & """ type=""video/mp4"" preload autoplay controls></video>"
                video = video & "<br/><br/>"
            end if

			replaceVideo = Replace(replaceVideo, "%video%=|" & link & "|" & width & "|" & height & "|", video)
			replaceVideo = Replace(replaceVideo, "|", "")
		end if
	loop while pos <> 0
end function


function replaceVideoScreensaver(textstr, page)
	pos_last=1
	replaceVideoScreensaver = textstr
    callType = page
 

	do
                    
		pos=InStr(pos_last, replaceVideoScreensaver, "%ss_video%=|")

		if pos <> 0 then
			pos1=InStr(pos+13, replaceVideoScreensaver, "|")
			pos2=InStr(pos1+1, replaceVideoScreensaver, "|")
			pos3=InStr(pos2+1, replaceVideoScreensaver, "|")

			link=Mid(replaceVideoScreensaver, pos+12, pos1-pos-12) 
			width=Mid(replaceVideoScreensaver, pos1+1, pos2-pos1-1) 
			height=Mid(replaceVideoScreensaver, pos2+1, pos3-pos2-1) 
			pos_last=pos3
            'Pause(5) Why this here?
            
			if LCase(Right(link,4)) = ".flv" or LCase(Right(link,4)) = ".mp4" or LCase(Right(link,4)) = ".m4v"  then
				src = alerts_folder & "admin/jscripts/tiny_mce/plugins/media/img/flv_player.swf?flvToPlay=" & link & "&autoStart=true"
                
                video = "<OBJECT  ID='mediaPlayer' width='" & width & "' height='" & height & "'  CLASSID='CLSID:6BF52A52-394A-11d3-B153-00C04F79FAA6' CODEBASE='http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#Version=6,4,5,715' STANDBY='Loading Microsoft Windows Media Player components...'  TYPE='application/x-oleobject'><PARAM Name = 'uiMode' Value = 'none'><PARAM NAME='url' VALUE='" & link &"'><PARAM NAME='autoStart' VALUE='true'><PARAM NAME='showControls' VALUE='true'><PARAM NAME='TransparentatStart' VALUE='false'><PARAM NAME='AnimationatStart' VALUE='true'><PARAM NAME='StretchToFit' VALUE='true'><PARAM NAME='PlayCount' VALUE='999999999'><PARAM NAME='loop' VALUE='true'><EMBED src='" & link & "' type='application/x-mplayer2' name='mediaPlayer' autostart='1' showcontrols='1' showstatusbar='1' autorewind='1' width='" & width & "' height='" & height & "' playcount='true' loop='true'></EMBED></OBJECT>"	  
                'This isnt work on client. Asking for flash player and do nothing even we have it.
				'video = "<object codebase=""http://active.macromedia.com/flash5/cabs/swflash.cab#version=5,0,0,0"" classid=""clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"" width=""" & width & """ height=""" & height & """><param name=""movie"" value=""" & src & """/><param name=""width"" value=""" & width & """/><param name=""height"" value=""" & height & """/><embed type=""application/x-shockwave-flash"" src=""" & src & """ width=""" & width & """ height=""" & height & """></embed></object>"
			else
				video = "<OBJECT  ID='mediaPlayer' width='" & width & "' height='" & height & "'  CLASSID='CLSID:6BF52A52-394A-11d3-B153-00C04F79FAA6' CODEBASE='http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#Version=6,4,5,715' STANDBY='Loading Microsoft Windows Media Player components...'  TYPE='application/x-oleobject'><PARAM Name = 'uiMode' Value = 'none'><PARAM NAME='url' VALUE='" & link &"'><PARAM NAME='autoStart' VALUE='true'><PARAM NAME='showControls' VALUE='true'><PARAM NAME='TransparentatStart' VALUE='false'><PARAM NAME='AnimationatStart' VALUE='true'><PARAM NAME='StretchToFit' VALUE='true'><PARAM NAME='PlayCount' VALUE='999999999'><PARAM NAME='loop' VALUE='true'><EMBED src='" & link & "' type='application/x-mplayer2' name='mediaPlayer' autostart='1' showcontrols='1' showstatusbar='1' autorewind='1' width='" & width & "' height='" & height & "' playcount='true' loop='true'></EMBED></OBJECT>"
			end if

            Set rsVideo = Conn.Execute ("SELECT video FROM alerts WHERE id = '" & request("id") & "'")

	        if not rsVideo.EOF then
		        videoAlert = rsVideo("video")
	        end if
            
            if ((callType = "preview") or (videoAlert)) then
			    video = "<video src=""" & link &""" width=""" & width & """ height=""" & height & """ type=""video/mp4"" preload autoplay controls></video>"
                video = video & "<br/><br/>"
            end if

			replaceVideoScreensaver = Replace(replaceVideoScreensaver, "%ss_video%=|" & link & "|" & width & "|" & height & "|", video)
			replaceVideoScreensaver = Replace(replaceVideoScreensaver, "|", "")
		end if
	loop while pos <> 0
end function


 Sub Pause(intSeconds)
  startTime = Time()
  Do Until DateDiff("s", startTime, Time(), 0, 0) > intSeconds
  Loop
 End Sub

function replaceFlash(textstr)
	pos_last=1
	replaceFlash = textstr
	do
		pos=InStr(pos_last, replaceFlash, "%flash%=|")
		if pos <> 0 then
			pos1=InStr(pos+10, replaceFlash, "|")
			pos2=InStr(pos1+1, replaceFlash, "|")
			pos3=InStr(pos2+1, replaceFlash, "|")

			src=Mid(replaceFlash, pos+9, pos1-pos-9)
			width=Mid(replaceFlash, pos1+1, pos2-pos1-1)
			height=Mid(replaceFlash, pos2+1, pos3-pos2-1)
			pos_last=pos3
			
			flash="<object codebase=""http://active.macromedia.com/flash5/cabs/swflash.cab#version=5,0,0,0"" classid=""clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"" width=""" & width & """ height=""" & height & """><param name=""movie"" value=""" & src & """/><param name=""width"" value=""" & width & """/><param name=""height"" value=""" & height & """/><embed type=""application/x-shockwave-flash"" src=""" & src & """ width=""" & width & """ height=""" & height & """></embed></object>"
			replaceFlash = Replace(replaceFlash, "%flash%=|" & src & "|" & width & "|" & height & "|", flash)
		end if
	loop while pos <> 0
end function

function getScheduledText(isScheduled, startDate, endDate)
	if isScheduled <> "1" or startDate = "" or year(startDate) = 1900 then
		getScheduledText = LNG_NO
	elseif year(startDate) = 1900 then
		getScheduledText = LNG_NO
	elseif endDate = "" then
		getScheduledText = LNG_START_ON &" "& "<scr" & "ipt language='javascript'>" & "document.write(getUiDateTime('" & FullDateTime(startDate) & "'))</script>"
	elseif year(endDate) = 1900 then
		getScheduledText = LNG_START_ON &" "& "<scr" & "ipt language='javascript'>" & "document.write(getUiDateTime('" & FullDateTime(startDate) & "'))</script>"
	else
		getScheduledText = "<scr" & "ipt language='javascript'>" & "document.write(getUiDateTime('" & FullDateTime(startDate) & "'))</script>" &" - "& "<scr" & "ipt language='javascript'>" & "document.write(getUiDateTime('" & FullDateTime(endDate) & "'))</script>"
	end if
end function

Function MyMod(ByVal a, ByVal b)
	a = Fix(CDbl(a))
	b = Fix(CDbl(b))
	MyMod = a - Fix(a/b) * b
End Function

function GetLifetimeControl(ByVal lifetime, ByRef name)
	min_sel = ""
	hour_sel = ""
	day_sel = ""
	if lifetime = "0" then lifetime = ""
	if lifetime <> "" then
		if MyMod(lifetime, 1440) = 0 then
			lifetime = lifetime / 1440
			day_sel = " selected"
		elseif MyMod(lifetime, 60) = 0 then
			lifetime = lifetime / 60
			hour_sel = " selected"
		else
			min_sel = " selected"
		end if
	else
		min_sel = " selected"
	end if
	GetLifetimeControl = "<input type=""text"" id="""& name &""" name="""& name &""" style=""width:50px"" value="""& lifetime &""" onkeypress=""return check_size_input(this, event);"">" &vbNewLine& _
		"<select id="""& name &"_factor"" name="""& name &"_factor"">" &vbNewLine& _
			"<option value=""1"""& min_sel &">"& LNG_MINUTES &"</option>" &vbNewLine& _
			"<option value=""60"""& hour_sel &">"& LNG_HOURS &"</option>" &vbNewLine& _
			"<option value=""1440"""& day_sel &">"& LNG_DAYS &"</option>" &vbNewLine& _
		"</select>"
end function

function GetLifetimeControlForVideo(ByVal lifetime, ByRef name)
	min_sel = ""
	hour_sel = ""
	day_sel = ""
	if lifetime = "0" then lifetime = ""
	if lifetime <> "" then
		if MyMod(lifetime, 1440) = 0 then
			lifetime = lifetime / 1440
			day_sel = " selected"
		elseif MyMod(lifetime, 60) = 0 then
			lifetime = lifetime / 60
			hour_sel = " selected"
		else
			min_sel = " selected"
		end if
	else
		min_sel = " selected"
	end if
	GetLifetimeControlForVideo = "<input type=""text"" id="""& name &""" name="""& name &""" form=""my_form"" style=""width:50px"" value="""& lifetime &""" onkeypress=""return check_size_input(this, event);"">" &vbNewLine& _
		"<select id="""& name &"_factor"" form=""my_form""  name="""& name &"_factor"">" &vbNewLine& _
			"<option value=""1"""& min_sel &">"& LNG_MINUTES &"</option>" &vbNewLine& _
			"<option value=""60"""& hour_sel &">"& LNG_HOURS &"</option>" &vbNewLine& _
			"<option value=""1440"""& day_sel &">"& LNG_DAYS &"</option>" &vbNewLine& _
		"</select>"
end function


function GetDateFormat()
	m_date_format = ""
	Set rsDateFormat = Conn.Execute ("SELECT val FROM settings WHERE name = 'ConfDateFormat'")
	if not rsDateFormat.EOF then
		m_date_format = rsDateFormat("val")
	end if
	rsDateFormat.Close
	GetDateFormat = m_date_format
end function

function GetTimeFormat()
	m_time_format = ""
	Set rsTimeFormat = Conn.Execute ("SELECT val FROM settings WHERE name = 'ConfTimeFormat'")
	if not rsTimeFormat.EOF then
		m_time_format = rsTimeFormat("val")
	end if
	rsTimeFormat.Close
	GetTimeFormat = m_time_format
end function

ShortIdUseSQL = False
Public Function shortIdFromGuid(guid)
	if guid <> "" and Len(guid) >= 36 then
		On Error Resume Next
		Err.Clear
		If ShortIdUseSQL Then
			Set RS_Short = Conn.Execute("SET ANSI_WARNINGS ON" & vbNewline & _
				"DECLARE @bin VARBINARY(MAX)" & vbNewline & _
				"SET @bin = CAST('" & Replace(guid, "'", "''") & "' AS UNIQUEIDENTIFIER)" & vbNewline & _
				"SELECT REPLACE(REPLACE(SUBSTRING(CAST(N'' AS XML).value('xs:base64Binary(xs:hexBinary(sql:variable(""@bin"")))','VARCHAR(MAX)'), 1, 22), '/', '_'), '+', '-') as id" & vbNewline & _
				"SET ANSI_WARNINGS OFF")
			shortIdFromGuid = RS_Short("id")
			If Err.Number <> 0 or VarType(shortIdFromGuid) <= 1 Then
				shortIdFromGuid = "AAAAAAAAAAAAAAAAAAAAAA"
			End If
		Else
			If Not IsObject(EncryptObject) Then
				Set EncryptObject = Server.CreateObject("DeskAlertsServer.Encrypt")
			End If
			If Err.Number = 0 Then
				shortIdFromGuid = EncryptObject.shortIdFromGuid(guid)
				If Err.Number <> 0 Then
					ShortIdUseSQL = True
					shortIdFromGuid = shortIdFromGuid(guid)
				End If
			Else
				ShortIdUseSQL = True
				shortIdFromGuid = shortIdFromGuid(guid)
			End If
		End If
		On Error Goto 0
	else
		shortIdFromGuid = guid
	end if
End Function

Public Function guidFromShortId(id)
	if id <> "" and Len(id) < 36 then
		On Error Resume Next
		Err.Clear
		If ShortIdUseSQL Then
			Set RS_Short = Conn.Execute("SET ANSI_WARNINGS ON" & vbNewline & _
				"DECLARE @Base64 VARCHAR(MAX)" & vbNewline & _
				"SET @Base64 = REPLACE(REPLACE('" & Replace(id, "'", "''") & "', '-', '+'), '_', '/') + '=='" & vbNewline & _
				"SELECT CAST(CAST(N'' AS XML).value('xs:base64Binary(sql:variable(""@Base64""))', 'VARBINARY(MAX)') AS UNIQUEIDENTIFIER) as guid" & vbNewline & _
				"SET ANSI_WARNINGS OFF")
			guidFromShortId = RS_Short("guid")
			If Err.Number <> 0 or VarType(guidFromShortId) <= 1 Then
				shortIdFromGuid = "00000000-0000-0000-0000-000000000000"
			End If
		Else
			If Not IsObject(EncryptObject) Then
				Set EncryptObject = Server.CreateObject("DeskAlertsServer.Encrypt")
			End If
			If Err.Number = 0 Then
				guidFromShortId = EncryptObject.guidFromShortId(id)
				If Err.Number <> 0 Then
					ShortIdUseSQL = True
					guidFromShortId = guidFromShortId(id)
				End If
			Else
				ShortIdUseSQL = True
				guidFromShortId = guidFromShortId(id)
			End If
		End If
		On Error Goto 0
	else
		guidFromShortId = id
	end if
End Function

Function TrimAll(str)
	Set rSpaces = New RegExp
	With rSpaces
		.Pattern = "^\s+|\s+$"
		.IgnoreCase = False
		.Global = True
	End With
	TrimAll = rSpaces.Replace(str, "")
End Function

Function ShortCaptionWithTitle(caption, maxlength)
	ShortCaptionWithTitle = HtmlEncode(caption)
	if Len(caption) > maxlength then
		ShortCaptionWithTitle = "<span title="""& ShortCaptionWithTitle &""">" & HtmlEncode(Left(caption, maxlength-3)) & "...</span>"
	end if
End Function

Function FullDateTime(dt)
	if dt <> "" then
		FullDateTime = FormatDateTime(dt, 2) &" "& FormatDateTime(dt, 3)
	else
		FullDateTime = dt
	end if
End Function

Public Function newTinySettings()            'settings for tinyMCE buttons
dim strings(2)                               'initially we deal with three lines, fourth is empty
dim newLine
dim blockNumber
dim firstEmptyString
newTinySettings = ""
strings(0) = "theme_advanced_buttons1 : " & Chr(34)
strings(1) = "theme_advanced_buttons2 : " & Chr(34)
strings(2) = "theme_advanced_buttons3 : " & Chr(34)
firstEmptyString = 4

for i = 0 to 2
	newLine = true
	Set tinyButtons = Conn.Execute("SELECT * FROM tiny_button_settings WHERE line_number = "& (i+1) & " ORDER BY block, number_in_block")
	While not tinyButtons.EOF
		if(tinyButtons("enabled")= true) then
			if (newLine = true) then
				strings(i) = strings(i) & tinyButtons("name")
				newLine = false
				blockNumber = tinyButtons("block")
			else
				if (tinyButtons("block") > blockNumber) then
                    strings(i) = strings(i) & ",|"
                    blockNumber = tinyButtons("block")
                end if
				strings(i) = strings(i) & ","
				strings(i) = strings(i) & tinyButtons("name")
			end if
		end if
		tinyButtons.MoveNext
	Wend

	if(newLine = false) then
	    strings(i) = strings(i) & Chr(34) & ","
		newTinySettings = newTinySettings & strings(i)
	else             'user deleted the whole line of buttons
		if(i+1 <= UBound(strings)) then
			j = UBound(strings)
			while (j > i)
				strings(j) = strings(j-1)
				j = j-1
			WEnd
		end if
		firstEmptyString = firstEmptyString - 1
	end if
Next
for i = firstEmptyString to 4
	newTinySettings = newTinySettings & "theme_advanced_buttons" & i & " : " & Chr(34) & Chr(34)
	if(i<4) then
		newTinySettings = newTinySettings & ","
	end if
next

End Function

Public Function defaultTinySettings()
	dim strings(2)
	dim newLine
	strings(0) = "theme_advanced_buttons1 : " & Chr(34)
    strings(1) = "theme_advanced_buttons2 : " & Chr(34)
    strings(2) = "theme_advanced_buttons3 : " & Chr(34)
    defaultTinySettings = ""
    for j = 0 to 2
        newLine = true
		Set tinyButtons = Conn.Execute("SELECT * FROM tiny_button_settings WHERE line_number = "& (j+1) & " ORDER BY block, number_in_block")
		blockNumber = tinyButtons("block")
		While not tinyButtons.EOF
        		if (tinyButtons("block") > blockNumber) then
        			strings(j) = strings(j) & ",|"
        			blockNumber = tinyButtons("block")
        		end if
        		if (newLine = true) then
        			strings(j) = strings(j) & tinyButtons("name")
        			newLine = false
        		else
        			strings(j) = strings(j) & ","
        			strings(j) = strings(j) & tinyButtons("name")
        		end if
        	tinyButtons.MoveNext
        Wend
        strings(j) = strings(j) & Chr(34) & ","
        defaultTinySettings = defaultTinySettings & strings(j)
	Next
	defaultTinySettings = Left(defaultTinySettings, Len(defaultTinySettings) -1)
End Function

Function canShowWidget(title)
if (title = "Users Statistics" or title = "User Details") then
	uid = Session("uid")
	admin = 0
	Set RSRole = Conn.Execute("SELECT id, role FROM users WHERE id="&uid)
	if(Not RSRole.EOF) then
		if(RSRole("role")="A") then
			admin = 1
		end if
		
		Set rs_policy = Conn.Execute("SELECT type FROM policy INNER JOIN policy_editor ON policy.id=policy_editor.policy_id WHERE editor_id=" & uid)
		if(Not rs_policy.EOF) then
			if(rs_policy("type")="A" or rs_policy("type")="M") then
				admin = 1
			end if
		end if
		rs_policy.Close
		
		if(admin = 0) then
			Set RSRights = Conn.Execute("SELECT statistics_val FROM policy_list INNER JOIN policy_editor ON policy_list.policy_id=policy_editor.policy_id WHERE editor_id=" & uid)
			if(Not RSRights.EOF) then
				if(Not IsNull(RSRights("statistics_val"))) then
					if(RSRights("statistics_val") <> "0000000") then
						statistics_checked = "checked"
					end if
				end if
			end if
			RSRights.Close
		end if
	end if
	if (admin = 1 OR statistics_checked="checked") then
		canShowWidget = true
	else
		canShowWidget = false
	end if 
	RSRole.Close
elseif title = "Alert Details" then
	uid = Session("uid")
	admin = 0
	alerts_checked_arr = Array ("", "", "", "", "", "", "")
	Set RSRole = Conn.Execute("SELECT id, role FROM users WHERE id="&uid)
	if(Not RSRole.EOF) then
		if(RSRole("role")="A") then
			admin = 1
		end if
		
		Set rs_policy = Conn.Execute("SELECT type FROM policy INNER JOIN policy_editor ON policy.id=policy_editor.policy_id WHERE editor_id=" & uid)
		if(Not rs_policy.EOF) then
			if(rs_policy("type")="A" or rs_policy("type")="M") then
				admin = 1
			end if
		end if
		rs_policy.Close
		
		if(admin = 0) then
			Set RSRights = Conn.Execute("SELECT alerts_val, statistics_val FROM policy_list INNER JOIN policy_editor ON policy_list.policy_id=policy_editor.policy_id WHERE editor_id=" & uid)
			if(Not RSRights.EOF) then
				if(Not IsNull(RSRights("alerts_val"))) then
					if(RSRights("alerts_val") <> "0000000") then
						alerts_checked = "checked"
					end if
					alerts_checked_arr = p_makeCheckedArray(RSRights("alerts_val"))
				end if
				if(Not IsNull(RSRights("statistics_val"))) then
					if(RSRights("statistics_val") <> "0000000") then
						statistics_checked = "checked"
					end if
				end if
			end if
			RSRights.Close
		end if
	end if
	if (admin = 1 OR statistics_checked="checked" or alerts_checked_arr(4)="checked") then
		canShowWidget = true
	else
		canShowWidget = false
	end if
	RSRole.Close
elseif title = "Survey Details" then
	if SURVEY <> 1 then
		canShowWidget=false
	else
		uid = Session("uid")
		admin = 0
		Set RSRole = Conn.Execute("SELECT id, role FROM users WHERE id="&uid)
		if(Not RSRole.EOF) then
			if(RSRole("role")="A") then
				admin = 1
			end if
			
			Set rs_policy = Conn.Execute("SELECT type FROM policy INNER JOIN policy_editor ON policy.id=policy_editor.policy_id WHERE editor_id=" & uid)
			if(Not rs_policy.EOF) then
				if(rs_policy("type")="A"  or rs_policy("type")="M") then
					admin = 1
				end if
			end if
			rs_policy.Close
			
			if(admin = 0) then
				Set RSRights = Conn.Execute("SELECT statistics_val FROM policy_list INNER JOIN policy_editor ON policy_list.policy_id=policy_editor.policy_id WHERE editor_id=" & uid)
				if(Not RSRights.EOF) then
					if(Not IsNull(RSRights("statistics_val"))) then
						if(RSRights("statistics_val") <> "0000000") then
							statistics_checked = "checked"
						end if
					end if
				end if
				RSRights.Close
			end if
		end if
		if (admin = 1 OR statistics_checked="checked") then
			canShowWidget = true
		else
			canShowWidget = false
		end if 
		RSRole.Close
	end if
elseif title = "Survey Answers" then
	if SURVEY <> 1 then
		canShowWidget=false
	else
		uid = Session("uid")
		admin = 0
		surveys_checked_arr = Array ("", "", "", "", "", "", "")
		Set RSRole = Conn.Execute("SELECT id, role FROM users WHERE id="&uid)
		if(Not RSRole.EOF) then
			if(RSRole("role")="A") then
				admin = 1
			end if
			
			Set rs_policy = Conn.Execute("SELECT type FROM policy INNER JOIN policy_editor ON policy.id=policy_editor.policy_id WHERE editor_id=" & uid)
			if(Not rs_policy.EOF) then
				if(rs_policy("type")="A"  or rs_policy("type")="M") then
					admin = 1
				end if
			end if
			rs_policy.Close
			
			if(admin = 0) then
				Set RSRights = Conn.Execute("SELECT surveys_val, statistics_val FROM policy_list INNER JOIN policy_editor ON policy_list.policy_id=policy_editor.policy_id WHERE editor_id=" & uid)
				if(Not RSRights.EOF) then
					if(Not IsNull(RSRights("surveys_val"))) then
						if(RSRights("surveys_val") <> "0000000") then
							surveys_checked = "checked"
						end if
						surveys_checked_arr = p_makeCheckedArray(RSRights("surveys_val"))
					end if
					if(Not IsNull(RSRights("statistics_val"))) then
						if(RSRights("statistics_val") <> "0000000") then
							statistics_checked = "checked"
						end if
					end if
				end if
				RSRights.Close
			end if
		end if
		if (admin = 1 OR statistics_checked="checked" or surveys_checked_arr(4)="checked") then
			canShowWidget = true
		else
			canShowWidget = false
		end if 
		RSRole.Close
	end if
elseif title="Instant Send" then
	if IM<>1 then
		canShowWidget = false
	else
		uid = Session("uid")
		admin = 0
		im_checked_arr = Array ("", "", "", "", "", "", "")
		Set RSRole = Conn.Execute("SELECT id, role FROM users WHERE id="&uid)
		if(Not RSRole.EOF) then
			if(RSRole("role")="A") then
				admin = 1
			end if
			
			Set rs_policy = Conn.Execute("SELECT type FROM policy INNER JOIN policy_editor ON policy.id=policy_editor.policy_id WHERE editor_id=" & uid)
			if(Not rs_policy.EOF) then
				if(rs_policy("type")="A"  or rs_policy("type")="M" ) then
					admin = 1
				end if
			end if
			rs_policy.Close
			
			if(admin = 0) then
				Set RSRights = Conn.Execute("SELECT im_val FROM policy_list INNER JOIN policy_editor ON policy_list.policy_id=policy_editor.policy_id WHERE editor_id=" & uid)
				if(Not RSRights.EOF) then
					if(Not IsNull(RSRights("im_val"))) then
						im_checked_arr = p_makeCheckedArray(RSRights("im_val"))
					end if
				end if
				RSRights.Close
			end if
		end if
		if (admin = 1 OR im_checked_arr(4) <> "") then
			canShowWidget = true
		else
			canShowWidget = false
		end if 
	end if
	
elseif title="Create Alert" then
	uid = Session("uid")
	admin = 0
	alerts_checked_arr = Array ("", "", "", "", "", "", "")
	Set RSRole = Conn.Execute("SELECT id, role FROM users WHERE id="&uid)
	if(Not RSRole.EOF) then
		if(RSRole("role")="A") then
			admin = 1
		end if
		
		Set rs_policy = Conn.Execute("SELECT type FROM policy INNER JOIN policy_editor ON policy.id=policy_editor.policy_id WHERE editor_id=" & uid)
		if(Not rs_policy.EOF) then
			if(rs_policy("type")="A"  or rs_policy("type")="M") then
				admin = 1
			end if
		end if
		rs_policy.Close
		
		if(admin = 0) then
			Set RSRights = Conn.Execute("SELECT alerts_val, emails_val FROM policy_list INNER JOIN policy_editor ON policy_list.policy_id=policy_editor.policy_id WHERE editor_id=" & uid)
			if(Not RSRights.EOF) then
				if(Not IsNull(RSRights("alerts_val"))) then
					alerts_checked_arr = p_makeCheckedArray(RSRights("alerts_val"))
				end if
				if(Not IsNull(RSRights("emails_val"))) then
					if(RSRights("emails_val") <> "0000000") then
						emails_checked = "checked"
					end if
				end if		
			end if
			RSRights.Close
		end if
	end if
	if (admin = 1 OR alerts_checked_arr(0) <> "" OR emails_checked <> "") then
		canShowWidget = true
	else
		canShowWidget = false
	end if 
else
	canShowWidget = true
end if
end Function

Function URLDecode(ByVal What)
	   ' sDecoded = sText
    'Set oRegExpr = Server.CreateObject("VBScript.RegExp")
    'oRegExpr.Pattern = "%[0-9,A-F]{2}"
   ' oRegExpr.Global = True
    'Set oMatchCollection = oRegExpr.Execute(sText)
   ' For Each oMatch In oMatchCollection
	'	    sDecoded = Replace(sDecoded,oMatch.value,Chr(CInt("&H" & Right(oMatch.Value,2))))
   ' Next
   ' URLDecode = sDecoded
  Dim Pos, pPos
  What = Replace(What, "+", " ")
  on error resume Next
  Dim Stream: Set Stream = CreateObject("ADODB.Stream")
  If err = 0 Then 
    on error goto 0
    Stream.Type = 2 'String
    Stream.Open
    Pos = InStr(1, What, "%")
    pPos = 1
    Do While Pos > 0
      Stream.WriteText Mid(What, pPos, Pos - pPos) + _
        Chr(CLng("&H" & Mid(What, Pos + 1, 2)))
      pPos = Pos + 3
      Pos = InStr(pPos, What, "%")
    Loop
    Stream.WriteText Mid(What, pPos)
    Stream.Position = 0
    URLDecode = Stream.ReadText
    Stream.Close
  Else 'URL decode using string concentation
    on error goto 0
    Pos = InStr(1, What, "%")
    Do While Pos>0 
      What = Left(What, Pos-1) + _
        Chr(Clng("&H" & Mid(What, Pos+1, 2))) + _
        Mid(What, Pos+3)
      Pos = InStr(Pos+1, What, "%")
    Loop
    URLDecode = What
  End If
End Function

Function ShouldSendPushNotification(user_id)


    retVal = true
	
	
	if IsNull(user_id)  then 
		ShouldSendPushNotification = true
		exit function
	end if
	
	
    Set unobtrusivesSet = Conn.Execute("SELECT [from], [to],  [enabled], [days] FROM unobtrusives WHERE user_id = " & user_id)

	if unobtrusivesSet.EOF then
		ShouldSendPushNotification = retVal
		exit function
		
	end if
    weekDays = Array("mon", "tue", "wed", "thu", "fri", "sat", "sun")
	
    do while not unobtrusivesSet.EOF
        
        enabled = unobtrusivesSet("enabled")
        fromTime = unobtrusivesSet("from")
        toTime = unobtrusivesSet("to")
        days = unobtrusivesSet("days")
        
        if (enabled = "1") then
            fromInSec = timeToSeconds(fromTime)
            toInSec = timeToSeconds(toTime)
            nowSec = nowToSeconds()
            weekdayNum =  Weekday(Now(), vbMonday)            
            weekDayStr = weekDays(weekdayNum - 1)
         
           ' Response.Write "from = " & fromInSec
           ' Response.Write "to = " & toInSec
           ' Response.Write "now = " & nowSec
           ' Response.Write "weekDay = " & weekDayStr
            if(inArray(weekDayStr, Split(days, ",")) and nowSec > fromInSec and nowSec < toInSec) then
               
                retVal = false
            end if
        end if
      unobtrusivesSet.movenext
    loop
    
    ShouldSendPushNotification  = retVal
End Function



Function nowToSeconds
    hours = Hour(Now())
    minutes = Minute(Now())
    seconds = hours*3600 + minutes*60
    nowToSeconds = seconds
End Function
Function timeToSeconds(timeStr)

    timeComps = Split(timeStr, ":")
    
    hours = clng(timeComps(0))
    minutes = clng(timeComps(1))
    
    seconds = hours*3600 + minutes*60
    timeToSeconds = seconds

End Function



'  GlobalFuncs end
'===============================

            %>

<script language="JScript" runat="server">
    function my_encodeURIComponent(text) {
        return encodeURIComponent(text);
    }
    function jsArray() {
        return new Array();
    }
    function jsObject() {
        return new Object();
    }
    function jsEval(text) {
        return eval(text);
    }
    function jsCheckProperty(obj, propName) {
        return (typeof obj[propName] != "undefined");
    }
    function jsProperty(obj, propName) {
        return obj[propName];
    }
    var urlsWasShorted = {};
    function shortUrls(text) {
        //return text.replace(/(<a[^>]*\shref=)(['"])([^>]*)\2([^>]*>)/gi, function(all, left, quote, href, right)
        return text.replace(/https?:\/\/\S+/gi, function (href) {
            if (urlsWasShorted[href]) {
                //return left + quote + urlsWasShorted[href] + quote + right;
                return urlsWasShorted[href];
            }
            else {
                var oXMLHTTP = Server.CreateObject("Msxml2.ServerXMLHTTP.6.0");
                oXMLHTTP.open("POST", "https://www.googleapis.com/urlshortener/v1/url", false);
                oXMLHTTP.setRequestHeader("Content-Type", "application/json")
                oXMLHTTP.send("{\"longUrl\": \"" + href + "\"}");
                //try {
                var obj = eval("(" + oXMLHTTP.responseText + ")");
                urlsWasShorted[href] = obj["id"];
                href = obj["id"];
                //} catch (e) {}
                //return left + quote + href + quote + right;
                return href;
            }
        });
    }
</script>

