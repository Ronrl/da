﻿using System.Web;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.ApplicationCore.Repositories;
using DeskAlerts.ApplicationCore.Utilities;
using DeskAlerts.Server.Utils.Interfactes;
using DeskAlertsDotNet.Parameters;
using DeskAlertsDotNet.Utils.Services;

namespace DeskAlertsDotNet.Pages
{
    using Resources;
    using System;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;

    public partial class AlertRecipientsList : DeskAlertsBaseListPage
    {
        private static readonly IConfigurationManager ConfigurationManager = new WebConfigConfigurationManager(Config.Configuration);
        private static readonly IAlertRepository AlertRepository = new PpAlertRepository(ConfigurationManager);
        private static readonly IUserRepository UserRepository = new PpUserRepository(ConfigurationManager);
        private static readonly IGroupRepository GroupRepository = new PpGroupRepository(ConfigurationManager);
        private readonly IUserService _userService = new UserService(UserRepository, GroupRepository);

        private string _alertId;
        private AlertEntity _alertEntity;

        private const string SelectIpGroupNamesParameter = " ig.name as IpGroupName ";
        private const string SelectIpGroupsCountParameter = " COUNT(ig.name) as mycnt ";

        public AlertRecipientsList()
        {
            _alertId = string.IsNullOrEmpty(HttpContext.Current.Request["sendAlertId"])
                ? HttpContext.Current.Request["id"]
                : HttpContext.Current.Request["sendAlertId"];
            _alertEntity = AlertRepository.GetById(long.Parse(_alertId));
        }

        protected override void OnLoad(EventArgs e)
        {
            
            if (!IsPostBack)
            {
                UserCheckBox.Checked = Request["fu"] == "1";
                OUsCheckBox.Checked = Request["fo"] == "1";
                GroupsCheckBox.Checked = Request["fg"] == "1";

                if (!UserCheckBox.Checked && !OUsCheckBox.Checked && !GroupsCheckBox.Checked)
                {
                    UserCheckBox.Checked = true;
                }
            }

            if (_alertEntity.Type2 == "I")
            {
                UserCheckBox.Visible = false;
                OUsCheckBox.Visible = false;
                GroupsCheckBox.Visible = false;
            }

            if (string.IsNullOrEmpty(_alertId))
            {
                Response.End();
            }

            base.OnLoad(e);
            searchButton.Text = searchButton.Text.Replace("{Search}", resources.LNG_SEARCH);
            nameCell.Text = resources.LNG_NAME;
            typeCell.Text = resources.LNG_TYPE;

            Table = contentTable;
            FillTableWithContent(false);
        }

        #region DataProperties
        protected override string DataTable => "";

        protected override string[] Collumns => new[] { "name", "type" };

        protected override string DefaultOrderCollumn => "name";

        protected override string CustomCountQuery
        {
            get
            {
                var query = string.Empty;

                switch (_alertEntity.Type2)
                {
                    case "B":
                    {
                        if (UserCheckBox.Checked)
                        {
                            query = "SELECT COUNT(id) as mycnt FROM users WHERE role = 'U'  AND (client_version != 'web client' OR client_version IS NULL)";

                            if (SearchTerm.Length > 0)
                            {
                                query += $" AND (name LIKE '%{SearchTerm}%' OR display_name LIKE '%{SearchTerm}%')";
                            }
                        }

                        if (GroupsCheckBox.Checked)
                        {
                            if (UserCheckBox.Checked)
                            {
                                query += " UNION ALL";
                            }

                            query += @" SELECT COUNT(id) as mycnt
                            FROM groups";

                            if (SearchTerm.Length > 0)
                            {
                                query += $" WHERE name LIKE '%{SearchTerm}%'";
                            }
                        }

                        if (OUsCheckBox.Checked)
                        {
                            if (UserCheckBox.Checked || GroupsCheckBox.Checked)
                            {
                                query += " UNION ALL";
                            }

                            query += @" SELECT COUNT(id) as mycnt
                            FROM OU";

                            if (SearchTerm.Length > 0)
                            {
                                query += $" WHERE name LIKE '%{SearchTerm}%'";
                            }
                        }

                        break;
                    }

                    case "R":
                    {
                        if (UserCheckBox.Checked)
                        {
                            query = $@"
                        SELECT COUNT(DISTINCT user_id) AS mycnt
                        FROM alerts_sent_stat
                            FULL JOIN users ON users.id = alerts_sent_stat.user_id
                        WHERE alerts_sent_stat.alert_id = {_alertId}";

                            if (SearchTerm.Length > 0)
                            {
                                query += " AND (name LIKE '%" + SearchTerm + "%' OR display_name LIKE '%" + SearchTerm + "%')";
                            }

                            query += $@" UNION ALL 
                        SELECT COUNT(comp_id) as mycnt 
                        FROM alerts_sent_comp INNER JOIN computers ON computers.id=alerts_sent_comp.comp_id 
                        WHERE alerts_sent_comp.alert_id ={_alertId}";

                            if (SearchTerm.Length > 0)
                            {
                                query += $" AND name LIKE '%{SearchTerm}%'";
                            }
                        }

                        if (GroupsCheckBox.Checked)
                        {
                            if (UserCheckBox.Checked)
                            {
                                query += " UNION ALL";
                            }

                            query += $@" 
                        SELECT COUNT(g.id) as mycnt 
                        FROM alerts_sent_stat AS a
                        RIGHT JOIN alerts_sent_group AS asg ON asg.id = a.[user_id]
                        RIGHT JOIN groups AS g ON asg.group_id = g.id
                        WHERE asg.alert_id = {_alertId}";

                            if (SearchTerm.Length > 0)
                            {
                                query += $" AND g.name LIKE '%{SearchTerm}%'";
                            }
                        }

                        if (OUsCheckBox.Checked)
                        {
                            if (UserCheckBox.Checked || GroupsCheckBox.Checked)
                            {
                                query += " UNION ALL";
                            }

                            query += $@"
                        SELECT COUNT(aos.[id]) as mycnt
                        FROM [alerts_ou_stat] AS aos
                        RIGHT JOIN [OU] ON aos.[ou_id] = [OU].[Id]
                        WHERE aos.[alert_id] = {_alertId}";

                            if (SearchTerm.Length > 0)
                            {
                                query += $" AND [OU].[name] LIKE '%{SearchTerm}%'";
                            }
                        }

                        break;
                    }

                    case "I":
                    {
                        query += GetIpGroupNamesString(SelectIpGroupsCountParameter, _alertId);

                        if (!string.IsNullOrEmpty(SearchTerm))
                        {
                            query += $" AND ig.name LIKE '%{SearchTerm}%'";
                        }

                        break;
                    }

                    default:
                    {
                        Logger.Error($"'{_alertEntity.Type2}' is not approptiate recipients type");
                        throw new Exception($"'{_alertEntity.Type2}' is not approptiate recipients type");
                    }
                }

                query = "SELECT SUM(mycnt) FROM (" + query + ") [count]";

                return query;
            }
        }

        protected override string CustomQuery
        {
            get
            {
                var query = string.Empty;

                switch (_alertEntity.Type2)
                {
                    case "B":
                    {
                        if (UserCheckBox.Checked)
                        {
                            query = "SELECT name, 'User' as type  FROM users WHERE role = 'U'  AND (client_version != 'web client' OR client_version IS NULL)";

                            if (SearchTerm.Length > 0)
                            {
                                query += $" AND (name LIKE '%{SearchTerm}%' OR display_name LIKE '%{SearchTerm}%')";
                            }
                        }

                        if (GroupsCheckBox.Checked)
                        {
                            if (UserCheckBox.Checked)
                            {
                                query += " UNION ALL";
                            }

                            query += @" SELECT name,
                            'Group' AS type
                            FROM groups";
                        }

                        if (OUsCheckBox.Checked)
                        {
                            if (UserCheckBox.Checked || GroupsCheckBox.Checked)
                            {
                                query += " UNION ALL";
                            }

                            query += @" SELECT name,
                            'OU' AS type
                            FROM OU";
                        }

                        break;
                    }
                    case "R":
                    {
                        if (UserCheckBox.Checked)
                        {
                            query = $@"
                        SELECT DISTINCT 
                            users.name + CASE WHEN COALESCE(domains.name, '') <> '' THEN '{_userService.ConnectSign}'+COALESCE(domains.name, '') ELSE '' END, 
                            'User' as type 
                        FROM alerts_sent_stat
                        FULL JOIN users ON users.id=alerts_sent_stat.user_id 
                        JOIN domains ON domains.id = users.domain_id
                        WHERE alerts_sent_stat.alert_id={_alertId}";

                            if (SearchTerm.Length > 0)
                            {
                                query += $" AND (users.name LIKE '%{SearchTerm}%' OR users.display_name LIKE '%{SearchTerm}%')";
                            }

                            query += $@" UNION ALL 
                        SELECT DISTINCT name, 'Computer' as type 
                        FROM alerts_sent_comp INNER JOIN computers ON computers.id=alerts_sent_comp.comp_id 
                        WHERE alerts_sent_comp.alert_id ={_alertId}";

                            if (SearchTerm.Length > 0)
                            {
                                query += $" AND name LIKE '%{SearchTerm}%'";
                            }
                        }

                        if (GroupsCheckBox.Checked)
                        {
                            if (UserCheckBox.Checked)
                            {
                                query += " UNION ALL";
                            }

                            query += $@" 
                        SELECT g.name, 'Group' as type
                        FROM alerts_sent_stat AS a
                        RIGHT JOIN alerts_sent_group AS asg ON asg.id = a.[user_id]
                        RIGHT JOIN groups AS g ON asg.group_id = g.id
                        WHERE asg.alert_id = {_alertId}";

                            if (SearchTerm.Length > 0)
                            {
                                query += $" AND g.name LIKE '%{SearchTerm}%'";
                            }
                        }

                        if (OUsCheckBox.Checked)
                        {
                            if (UserCheckBox.Checked || GroupsCheckBox.Checked)
                            {
                                query += " UNION ALL";
                            }

                            query += $@"
                        SELECT [OU].[name],
                            'OU' AS type
                        FROM [alerts_ou_stat] AS aos
                        RIGHT JOIN [OU] ON aos.[ou_id] = [OU].[Id]
                        WHERE aos.[alert_id] = {_alertId}";

                            if (SearchTerm.Length > 0)
                            {
                                query += $" AND [OU].[name] LIKE '%{SearchTerm}%'";
                            }
                        }

                        break;
                    }

                    case "I":
                    {
                        query += GetIpGroupNamesString(SelectIpGroupNamesParameter, _alertId);

                        if (!string.IsNullOrEmpty(SearchTerm))
                        {
                            query += $" AND ig.name LIKE '%{SearchTerm}%'";
                        }

                        break;
                    }

                    default:
                    {
                        Logger.Error($"'{_alertEntity.Type2}' is not approptiate recipients type");
                        throw new Exception($"'{_alertEntity.Type2}' is not approptiate recipients type");
                    }    
                }

                return query;
            }
        }

        #endregion

        #region Interface properties

        protected override HtmlInputText SearchControl => searchTermBox;

        protected override HtmlGenericControl TopPaging => topPaging;

        protected override HtmlGenericControl BottomPaging => bottomPaging;

        protected override IButtonControl[] DeleteButtons => new IButtonControl[] { };

        protected override string PageParams
        {
            get
            {
                var paramString = string.Empty;
                paramString += AddParameter(paramString, "id");
                paramString += AddParameter(paramString, "sendAlertId");
                paramString += AddParameter(paramString, "fu", UserCheckBox.Checked);
                paramString += AddParameter(paramString, "fg", GroupsCheckBox.Checked);
                paramString += AddParameter(paramString, "fo", OUsCheckBox.Checked);
                return paramString;
            }
        }

        private static string GetIpGroupNamesString(string selectParameter, string alertId)
        {
            return $@" SELECT {selectParameter}, 'IP group' AS Type
                       FROM ip_groups as ig            
                       JOIN alerts_sent_iprange as asi ON ig.id = asi.ip_range_group_id
                       WHERE asi.alert_id = {alertId}";
        }

        protected override HtmlGenericControl NoRowsDiv => NoRows;

        protected override HtmlGenericControl TableDiv => mainTableDiv;

        protected override string ContentName => resources.LNG_OBJECTS;

        #endregion
    }
}