﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DeskAlerts.Server.Utils;
using DeskAlerts.Server.Utils.Helpers;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Parameters;

namespace DeskAlertsDotNet.Pages
{
    public partial class CompDetails : DeskAlertsBasePage
    {
        string compId;
        protected override void OnLoad(EventArgs e)
        {
 	         base.OnLoad(e);

            if(string.IsNullOrEmpty(Request["id"]))
            {
                Response.End();
                return;
            }

            compId = Request["id"];

            string compDataQuery = "SELECT id, context_id, name, domain_id, reg_date, last_date, next_request, convert(varchar,last_request) as last_request1, last_request FROM computers WHERE id=" + compId;

            DataSet compDataSet = dbMgr.GetDataByQuery(compDataQuery);

            nameCell.InnerText = compDataSet.GetString(0, "name");

            string domainName = dbMgr.GetScalarByQuery("SELECT name FROM domains WHERE id=" + compDataSet.GetString(0, "domain_id")).ToString();

            domainCell.InnerText = domainName;

            string groupsQuery = "SELECT name FROM groups INNER JOIN computers_groups ON groups.id=computers_groups.group_id WHERE comp_id=" + compId;

            DataSet groupSet = dbMgr.GetDataByQuery(groupsQuery);

            if (!groupSet.IsEmpty)
            {
                string[] groupsArr = groupSet.ToList().Select(row => row.GetString("name")).ToArray();

                groupsCell.InnerText = string.Join(",", groupsArr);
            }

            if(!Config.Data.HasModule(Modules.AD))
            {
                regDateCell.InnerText = compDataSet.GetString(0, "reg_date");
            }
            else
            {
                regDateRow.Visible = false;
            }

            DateTime lastRequest = DateTimeConverter.ParseDateTimeFromDb(compDataSet.GetString(0, "last_request"));

            statusImg.Width = 11;
            statusImg.Height = 11;
            if (lastRequest.CompareTo(DateTime.MinValue) != 0)
            {

                int dateDiff = (int)(DateTime.Now - lastRequest).TotalMinutes;
                string nextRequest = compDataSet.GetString(0, "next_request");
                int onlineCounter = 2;
                if (nextRequest.Length != 0)
                {
                    onlineCounter = (Convert.ToInt32(nextRequest) / 2) * 60;
                }

                if (onlineCounter < dateDiff)
                {
                    statusImg.Src = "images/offline.gif";
                }
                else
                {
                    statusImg.Src = "images/online.gif";
                }
            }
            else
            {
                statusImg.Src = "images/offline.gif";
            }

            lastActivityCell.InnerText = DeskAlertsUtils.IsNotEmptyDateTime(lastRequest)
                ? DateTimeConverter.ToUiDateTime(lastRequest)
                : string.Empty;
        }

    }
}