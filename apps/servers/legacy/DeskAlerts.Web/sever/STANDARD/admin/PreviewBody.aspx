﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PreviewBody.aspx.cs" Inherits="DeskAlertsDotNet.Pages.PreviewBody" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" <%=this.SkinCss %> />
    <script src="jscripts/jquery/jquery.min.js"></script>
    <script src='jscripts/json2.js'></script>
    <script>
        var ext_props = JSON.parse($("<div/>").html('<%= this.Request["ext_props"].Replace("'", @"\'").Replace("\\n", "\\\\n")%>'.replace(/&/g, '&amp;').replace(/"/g, '&quot;').replace(/'/g, '&#39;').replace(/</g, '&lt;').replace(/>/g, '&gt;')).text());
        <%= this.jsSource  %>
    </script>
    <script src='external.asp'></script>
    <script>
        $(document).ready(function () {
            var initialTimeoutOld = window.Initial_Timeout;
            if (typeof window.Initial_Timeout == 'function') {
                window.Initial_Timeout = function (indexes) {
                    initialTimeoutOld(indexes);
                    var oldDisplay = document.body.style.display;
                    document.body.style.display = "none";
                    setTimeout(function () { document.body.style.display = oldDisplay; }, 0);
                }

                Initial("0");
            }
            else if (typeof Initial == 'function') {
                Initial("0");
                var oldDisplay = document.body.style.display;
                document.body.style.display = "none";
                setTimeout(function () { document.body.style.display = oldDisplay; }, 0);
            }
        });
    </script>

</head>
<base href="<%= this.skinPath%>" />
<body runat="server" id="mainDiv" class="alertcontent">
    <form id="form1" runat="server">
    </form>
</body>
</html>
