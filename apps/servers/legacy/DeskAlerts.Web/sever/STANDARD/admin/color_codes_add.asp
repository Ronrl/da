﻿<!-- #include file="config.inc" -->
<% ConnCount = 2 %>
<!-- #include file="db_conn.asp" -->
<!-- #include file="md5.asp" -->
<!-- #include file="lang.asp" -->
<%

check_session()

%>

<%

  uid = Session("uid")

if (uid <> "") then

if Request("sub1") = "add" then
	name = Replace(Request("name"),"'","''")
	color = Request("color")
	SQL = "INSERT INTO color_codes (name, color) VALUES (N'"& name & "','"&color&"')"
	Conn.Execute(SQL)
	Response.Redirect("ColorCodes.aspx")
end if

if Request("sub1") = "edit" then
	id = Request("id")
	name = Replace(Request("name"),"'","''")
	color = Request("color")
	SQL = "UPDATE color_codes SET name=N'"&name&"', color='"&color&"' WHERE id='"&id&"'"
	Conn.Execute(SQL)
	Response.Redirect("ColorCodes.aspx")
end if

id=Request("id")

if id<>"" then
	my_val = "edit"
	Set RS = Conn.Execute("SELECT * FROM color_codes WHERE id='"&id&"'")
else
	my_val = "add"
end if
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>DeskAlerts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/style9.css" rel="stylesheet" type="text/css">
<link href="css/colpick.css" rel="stylesheet" type="text/css">
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/colpick.js"></script>
<script language="javascript" type="text/javascript">

	$(document).ready(function() {
		$(".add_button").button();
		$(".save_button").button();
		$(".cancel_button").button();
		$('#colorPick').colpick({
			layout: 'hex',
			submit: false,
			colorScheme: 'dark',
			onChange: function(hsb, hex, rgb, el, bySetColor) {
				$("#color").val(hex);
				$("#colorPick").css("background", "#" + hex);
			}
		}).keyup(function() {
			$(this).colpickSetColor(this.value);
		});
		$('#colorPick').colpickSetColor($("#color").val(), false);
	});

	function checkInput() {
		if ($("#ccName").val() == "") {
			alert("<%=LNG_PLEASE_FILL_ALL_EMPTY_FIELDS %>");
			return false; 
		}
		if ($("#color").val() == "") return false;
		return true;
	}
</script>
</head>
<body style="margin:0px" class="body">
<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	<tr>
	<td>
	<table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
		<tr>
		<td width=100% height=31 class="main_table_title"><img src="images/menu_icons/im_20.png" alt="" width="20" height="20" border="0" style="padding-left:7px">
			<a href="color_codes.asp" class="header_title" style="position:absolute;top:15px"><%=LNG_COLOR_CODES %></a></td>
		</tr>
		<tr>
		<td class="main_table_body" height="100%">
		<div style="margin:10px;">
		<% if id<>"" then %>
			<span class="work_header"><%=LNG_EDIT_COLOR_CDDE %></span>
		<%else %>
			<span class="work_header"><%=LNG_ADD_COLOR_CODE %></span>
		<%end if %>
		<br><br>
<form name="frm" method="post" action="">
<table border="0"><tr><td>
<input type="hidden" name="sub1" value="<% Response.Write my_val %>"/>
<input type="hidden" id="color" name="color" value="<% if(id <> "") then Response.Write RS("color") else Response.Write "747474" end if %>"/>
<table cellpadding=4 cellspacing=4 border=0>
<tr><td><%=LNG_COLOR_CODE %>:</td><td><input autocomplete="off" name="name" id="ccName" type="text" value="<% if(id <> "") then Response.Write RS("name") end if %>" size="40" maxlength="255"></td></tr>
<tr><td><%=LNG_COLOR %>:</td><td><div id="colorPick" style="height:21px;width:50px;background:#<% if(id <> "") then Response.Write RS("color") else Response.Write "747474" end if %>"></div></td></tr>
</table>

</td></tr>
<tr><td align="right">
<%
if(id <> "") then
%>
<button type="submit" class="cancel_button" onclick="javascript:history.go(-1); return false;"><%=LNG_CANCEL %></button> <button class="save_button" type="submit" onclick="javascript: return checkInput();"><%=LNG_SAVE %></button>
<%
Response.Write "<input type=""hidden"" name=""id"" value=""" & id & """/>"
else

%>
<button type="submit" class="cancel_button" onclick="javascript:history.go(-1); return false;"><%=LNG_CANCEL %></button> <button class="add_button" type="submit" onclick="javascript: return checkInput();"><%=LNG_ADD %></button>
<%
end if
%>
</td></tr></table>
</form>
		 <img src="images/inv.gif" alt="" width="1" height="100%" border="0">
		</div>
		</td>
		</tr>
	</table>
	</td>
	</tr>
</table>

<%

else

  Response.Redirect "index.asp"

end if
%> 
</body>
</html>
<!-- #include file="db_conn_close.asp" -->