using DeskAlerts.ApplicationCore.Repositories;
using DeskAlerts.ApplicationCore.Utilities;
using DeskAlerts.Server.Utils;
using NLog;

namespace DeskAlertsDotNet.Pages
{
    using DataBase;
    using DeskAlerts.ApplicationCore.Entities;
    using ExcelDataReader;
    using Parameters;
    using Resources;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using Utils.Services;

    public partial class Recipients : DeskAlertsBaseListPage
    {
        protected string ParentId;
        protected string ParentType;
        protected bool IsRecipientsUploaded;
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        protected override DataSet UploadedRecipients { get; set; }
        protected override HtmlInputText SearchControl => searchTermBox;

        protected override void OnLoad(EventArgs e)
        {
            cleanUploadBulkOfRecipientsDirectoryButton.Click += CleanUploadBulkOfRecipientsDirectoryButtonClick;

            var simpleGroupSending = !string.IsNullOrEmpty(Request["simpleGroupsSelection"]) && Request["simpleGroupsSelection"].Equals("1");

            if (!string.IsNullOrEmpty(Request["type"]))
            {
                ParentType = Request["type"];
            }

            if (!string.IsNullOrEmpty(Request["id"]))
            {
                ParentId = Request["id"];
            }

            if (!IsPostBack)
            {
                filterUsers.Checked = Request["fu"] == "1";
                filterComputers.Checked = Request["fc"] == "1";
                filterGroups.Checked = Request["fg"] == "1";

                if (!filterComputers.Checked && !filterUsers.Checked && !filterGroups.Checked)
                {
                    filterUsers.Checked = true;
                }
            }
            else
            {
                filterUsers.Checked = Request["filterUsers"] == "on";
                filterComputers.Checked = Request["filterComputers"] == "on";
                filterGroups.Checked = simpleGroupSending ? true : Request["filterGroups"] == "on";

                if (!filterComputers.Checked && !filterUsers.Checked && !filterGroups.Checked)
                {
                    var controlName = Request.Params["__EVENTTARGET"];

                    if (!string.IsNullOrEmpty(controlName))
                    {
                        var control = FindControl(controlName);

                        if (control is CheckBox)
                        {
                            (control as CheckBox).Checked = true;
                        }
                    }

                    if (!string.IsNullOrEmpty(Request["webalert"]))
                    {
                        filterUsers.Checked = true;
                    }
                }
            }

            IsBroadcastEnabled();

            Directory.CreateDirectory(UploadPath);
            //We just upload list of recipients and buy module
            if (recipientsUploadInput.HasFile && Config.Data.HasModule(Modules.BULK_UPLOAD_OF_RECIPIENTS))
            {
                //clean upload directory
                foreach (var file in new DirectoryInfo(UploadPath).GetFiles())
                {
                    file.Delete();
                }

                //upload file
                var uploadedFilePath = UploadPath + recipientsUploadInput.PostedFile.FileName;

                recipientsUploadInput.PostedFile.SaveAs(uploadedFilePath);

                //work with file
                ProceedExcelFile(uploadedFilePath);
            }
            //We already have uploaded list of recipients and buy module
            else if (Directory.GetFiles(UploadPath).Length != 0 && Config.Data.HasModule(Modules.BULK_UPLOAD_OF_RECIPIENTS))
            {
                ProceedExcelFile(Directory.GetFiles(UploadPath)[0]);
            }

            cleanUploadBulkOfRecipientsDirectoryButton.Visible = Directory.GetFiles(UploadPath).Length != 0;

            base.OnLoad(e);


            nameCell.Text = GetSortLink(resources.LNG_NAME, "object_name", Request["sortBy"]);
            typeCell.Text = GetSortLink(resources.LNG_TYPE, "object_type", Request["sortBy"]);
            dislplayNameCell.Text = GetSortLink(resources.LNG_DISPLAY_NAME, "display_name",
                Request["sortBy"]);
            domainName.Text = GetSortLink(resources.LNG_DOMAIN, "domain_name", Request["sortBy"]);

            var isSms = Config.Data.HasModule(Modules.SMS) && !string.IsNullOrEmpty(Request["sms"]) && Request["sms"].Equals("1");
            var isTextToCall = Config.Data.HasModule(Modules.TEXTTOCALL) && !string.IsNullOrEmpty(Request["textToCall"]) && Request["textToCall"].Equals("1");

            if (isSms || isTextToCall)
            {
                mobilePhoneCell.Text = GetSortLink(
                    resources.LNG_MOBILE_PHONE,
                    "mobile_phone",
                    Request["sortBy"]);
            }
            else
            {
                mobilePhoneCell.Visible = false;
            }

            bulkUploadOfRecipientsDiv.Visible = Config.Data.HasModule(Modules.BULK_UPLOAD_OF_RECIPIENTS);

            if (Config.Data.HasModule(Modules.EMAIL) && !string.IsNullOrEmpty(Request["email"]) &&
                Request["email"].Equals("1"))
            {
                emailCell.Text = GetSortLink(resources.LNG_EMAIL, "email", Request["sortBy"]);
            }
            else
            {
                emailCell.Visible = false;
            }

            onlineCell.Visible = filterUsers.Checked || filterComputers.Checked;

            if (onlineCell.Visible)
            {
                onlineCell.Text = GetSortLink(resources.LNG_ONLINE, "last_request1", Request["sortBy"]);
            }

            searchButton.Text = searchButton.Text.Replace("{Search}", resources.LNG_SEARCH);

            if (!string.IsNullOrEmpty(Request["webalert"]) || simpleGroupSending)
            {
                HideObjectFilters();
                if (simpleGroupSending)
                {
                    topPaging.Visible = false;
                    bottomRanging.Visible = false;
                    bulkUploadOfRecipientsDiv.Visible = false;
                }
            }

            if (!Config.Data.HasModule(Modules.AD))
            {
                computersFilter.Visible = false;
                computersLabel.Visible = false;
            }

            Logger.Debug($"Start generate table with recipients (div with id: {nameof(contentTable)}).");

            var mapObject = new Dictionary<string, int>();
            Logger.Trace($"{nameof(mapObject)} count: {mapObject.Count}.");

            var cnt = Content.Count < Offset + Limit 
                ? Content.Count 
                : Offset + Limit;
            Logger.Trace($"{nameof(Content.Count)}: \"{Content.Count}\". \"{nameof(Offset)}\": \"{Offset}\". \"{nameof(Limit)}\": \"{Limit}\". \"{nameof(cnt)}\": \"{cnt}\".");

            for (var i = Offset; i < cnt; i++)
            {
                Logger.Debug($"Database row #{i} is started to be processed.");

                var dataRow = Content.GetRow(i);
                var objType = dataRow.GetString("object_type");

                if (_isBroadcastEnabled || (objType == "user" || objType == "group" || objType == "computer"))
                {
                    Logger.Trace($"\"{nameof(_isBroadcastEnabled)}\": \"{_isBroadcastEnabled}\".");
                    Logger.Trace($"\"{nameof(objType)}\": \"{objType}\".");

                    var objId = dataRow.GetInt("id");
                    Logger.Trace($"\"{nameof(objId)}\": \"{objId}\".");

                    var checkBoxIdValue = $"recipient_{objType}_{objType.ToCharArray()[0]}{objId}";
                    Logger.Trace($"\"{nameof(checkBoxIdValue)}\": \"{checkBoxIdValue}\".");

                    if (mapObject.ContainsKey(checkBoxIdValue))
                    {
                        Logger.Trace($"\"{nameof(mapObject)}\" contains key \"{checkBoxIdValue}\".");
                        continue;
                    }

                    var row = new TableRow();
                    var checkBoxCell = new TableCell();
                    var checkBox = new CheckBox();

                    checkBox.InputAttributes["class"] = "content_object";
                    checkBox.InputAttributes["value"] = dataRow.GetString("object_name");
                    checkBox.ID = checkBoxIdValue;
                    checkBoxCell.Controls.Add(checkBox);
                    Logger.Trace($"{nameof(checkBox)} has the following parameters: " +
                                 $"\"class\" (\"{checkBox.InputAttributes["class"]}\"), " +
                                 $"\"value\" (\"{checkBox.InputAttributes["value"]}\"), " +
                                 $"\"{nameof(checkBox.ID)}\" (\"{checkBox.ID}\").");

                    row.Cells.Add(checkBoxCell);

                    var nameTableCell = new TableCell { Text = dataRow.GetString("object_name") };
                    row.Cells.Add(nameTableCell);
                    Logger.Trace($"\"{nameof(nameTableCell)}\": \"{nameTableCell.Text}\".");

                    var objectTypeTableCell = new TableCell { Text = objType };
                    row.Cells.Add(objectTypeTableCell);
                    Logger.Trace($"\"{nameof(objectTypeTableCell)}\": \"{objectTypeTableCell.Text}\".");

                    var displayNameTableCell = new TableCell();
                    if (objType.Equals("user") || objType.Equals("group"))
                    {
                        displayNameTableCell.Text = dataRow.GetString("display_name");
                    }
                    row.Cells.Add(displayNameTableCell);
                    Logger.Trace($"\"{nameof(displayNameTableCell)}\": \"{displayNameTableCell.Text}\".");

                    var domainTableCell = new TableCell { Text = dataRow.GetString("domain_name") };
                    row.Cells.Add(domainTableCell);

                    if (Config.Data.HasModule(Modules.SMS) && !string.IsNullOrEmpty(Request["sms"]) &&
                        Request["sms"].Equals("1") || isTextToCall)
                    {
                        var smsTableCell = new TableCell { Text = dataRow.GetString("mobile_phone") };
                        row.Cells.Add(smsTableCell);
                        Logger.Trace($"\"{nameof(smsTableCell)}\": \"{smsTableCell.Text}\".");
                    }

                    if (Config.Data.HasModule(Modules.EMAIL) && !string.IsNullOrEmpty(Request["email"]) &&
                        Request["email"].Equals("1"))
                    {
                        var emailTableCell = new TableCell { Text = dataRow.GetString("email") };
                        row.Cells.Add(emailTableCell);
                        Logger.Trace($"\"{nameof(emailTableCell)}\": \"{emailTableCell.Text}\".");
                    }

                    if (filterUsers.Checked || filterComputers.Checked)
                    {
                        var statusImg = new Image { Width = 11, Height = 11 };
                        var lastRequest = dataRow.GetDateTime("last_request");
                        var nextRequest = dataRow.GetInt("next_request");
                        if (objType.Equals("user") && DeskAlertsUtils.IsUserOnline(lastRequest, nextRequest)
                            || objType.Equals("computer") && DeskAlertsUtils.IsCompOnline(lastRequest, nextRequest))
                        {
                            statusImg.ImageUrl = "images/online.gif";
                            statusImg.AlternateText = resources.LNG_ONLINE;
                        }
                        else
                        {
                            statusImg.ImageUrl = "images/offline.gif";
                            statusImg.AlternateText = resources.LNG_OFFLINE;
                        }

                        var statusCell = new TableCell();
                        if (objType.Equals("user"))
                        {
                            statusCell.Controls.Add(statusImg);
                            statusCell.HorizontalAlign = HorizontalAlign.Center;
                        }
                        row.Cells.Add(statusCell);
                        Logger.Trace($"\"{nameof(statusImg.AlternateText)}\": \"{statusImg.AlternateText}\".");
                    }

                    contentTable.Rows.Add(row);
                    Logger.Trace($"\"{nameof(row)}\" with \"{nameof(checkBoxIdValue)}\" (\"{checkBoxIdValue}\") was added.");

                    mapObject.Add(checkBoxIdValue, objId);
                    Logger.Trace($"Object <{nameof(checkBoxIdValue)},{nameof(objId)}> (<{checkBoxIdValue},{objId}>) was added in \"{nameof(mapObject)}\". \"{nameof(mapObject)}\" count: \"{mapObject.Count}\".");
                }

                Logger.Debug($"Database row #{i} has finished processing.");
            }

            Logger.Debug($"Stop generate table with recipients (div with id: {nameof(contentTable)}).");
        }

        private void CleanUploadBulkOfRecipientsDirectoryButtonClick(object sender, EventArgs e)
        {
            CleanUploadBulkOfRecipientsDirectory();
            Response.Redirect(Request.RawUrl, true);
        }

        private void ProceedExcelFile(string uploadedFilePath)
        {
            var fileStream = File.Open(uploadedFilePath, FileMode.Open, FileAccess.Read);
            var excelDataReader = Path.GetExtension(uploadedFilePath) == ".csv" ? ExcelReaderFactory.CreateCsvReader(fileStream) : ExcelReaderFactory.CreateReader(fileStream);
            var index = 0;
            var newContent = new DataSet();

            var allUsersSqlString = "SELECT users.id, \'user\' as object_type, context_id, users.name as object_name, display_name, users.next_request, users.last_standby_request, users.next_standby_request, users.mobile_phone, users.email, users.standby, CONVERT(varchar, users.last_request) as last_request1, users.last_request FROM users";
            //include search if needed
            if (!string.IsNullOrEmpty(Request["searchTermBox"]))
            {
                allUsersSqlString += $" WHERE users.name LIKE N'%{Request["searchTermBox"]}%'";
            }

            var allUsersDataSet = dbMgr.GetDataByQuery(allUsersSqlString);
            var listOfFoundRows = new List<DataRow>();

            while (excelDataReader.Read())
            {   //First row is naming, not data
                if (index != 0)
                {
                    var rowtoFind = allUsersDataSet.ToList().Find(row =>
                    {
                        var username = (string)excelDataReader[0];
                        return row.GetString("object_name") == username;
                    });

                    if (rowtoFind != null)
                    {
                        listOfFoundRows.Add(rowtoFind);
                    }
                }

                index++;
            }

            //include ordering if needed
            if (!string.IsNullOrEmpty(Request["sortBy"]))
            {
                var orderDto = Request["sortBy"].Split(' ');
                var orderBy = orderDto[0];
                var orderDirection = orderDto[1];

                listOfFoundRows = orderDirection == "DESC" ? listOfFoundRows.OrderByDescending(row => row[orderBy]).ToList() : listOfFoundRows.OrderBy(row => row[orderBy]).ToList();
            }

            foreach (var foundRow in listOfFoundRows)
            {
                newContent.Add(foundRow);
            }

            if (newContent.Count > 0)
            {
                UploadedRecipients = newContent;
            }

            fileStream.Close();
        }

        protected void FilterComputers_OnCheckedChanged(object sender, EventArgs e)
        {
            var cb = sender as CheckBox;

            if (!filterUsers.Checked && !filterComputers.Checked && !filterGroups.Checked)
            {
                if (cb != null)
                {
                    cb.Checked = true;
                }
            }
        }

        private void HideObjectFilters()
        {
            showLabel.Visible = false;
            usersFilter.Visible = false;
            usersLabel.Visible = false;
            groupsFilter.Visible = false;
            groupsLabel.Visible = false;
            computersFilter.Visible = false;
            computersLabel.Visible = false;
        }

        private void IsBroadcastEnabled()
        {
            if (curUser.IsAdmin || curUser.Policy.IsBroadcastEnabled)
            {
                _isBroadcastEnabled = true;
            }
            else
            {
                var alertType = (AlertType)int.Parse(Request["alertType"]);

                var configurationManager = new WebConfigConfigurationManager(Config.Configuration);
                var policyRepository = new PpPolicyRepository(configurationManager);
                var domainRepository = new PpDomainRepository(configurationManager);
                var groupRepository = new PpGroupRepository(configurationManager);
                var userRepository = new PpUserRepository(configurationManager);
                var policyPublisherRepository = new PpPolicyPublisherRepository(configurationManager);
                var skinRepository = new PpSkinRepository(configurationManager);
                var contentSettingsRepository = new PpContentSettingsRepository(configurationManager);

                var policyService = new PolicyService(policyRepository, groupRepository, domainRepository, policyPublisherRepository,
                    userRepository, skinRepository, contentSettingsRepository);

                var policies = policyService.GetPoliciesByPublisherIdAndAlertType(curUser.Id, alertType);

                _isBroadcastEnabled = policies.Any(x => x.IsBroadcastEnabled);
            }
        }

        #region DataProperties

        protected override string DataTable => "";

        protected override string[] Collumns => new string[] { };

        protected override string DefaultOrderCollumn => "object_name";

        /// <summary>
        /// Check if current user can sent to entire organization. If it have no specific users or groups, then he can send to everyone.
        /// </summary>
        /// <returns></returns>
        private bool _isBroadcastEnabled;

        /*
         * 
         * If you want to add condition for data selection override property ConditionSqlString 
         * Also you can implement your custom query by overriding properties CustomQuery and CustomCountQuery
         *  
         */

        #endregion

        #region Interface properties

        protected override IButtonControl[] DeleteButtons => new IButtonControl[] { };

        protected override HtmlGenericControl NoRowsDiv => NoRows;

        protected override HtmlGenericControl TableDiv => mainTableDiv;

        protected override string ContentName => "";

        protected override HtmlGenericControl BottomPaging => bottomRanging;

        protected override HtmlGenericControl TopPaging => topPaging;

        protected override string CustomQuery => GetQuery();

        protected override string CustomCountQuery => GetQuery(true);

        private string GetQuery(bool isCount = false)
        {
            var configurationManager = new WebConfigConfigurationManager(Config.Configuration);
            var policyRepository = new PpPolicyRepository(configurationManager);
            var domainRepository = new PpDomainRepository(configurationManager);
            var groupRepository = new PpGroupRepository(configurationManager);
            var userRepository = new PpUserRepository(configurationManager);
            var policyPublisherRepository = new PpPolicyPublisherRepository(configurationManager);
            var skinRepository = new PpSkinRepository(configurationManager);
            var contentOptionRepository = new PpContentSettingsRepository(configurationManager);

            var policyService = new PolicyService(policyRepository, groupRepository, domainRepository, policyPublisherRepository,
                userRepository, skinRepository, contentOptionRepository);

            var requestAlertType = Request["alertType"];
            AlertType alertType;
            if (requestAlertType != null)
            {
                alertType = (AlertType)int.Parse(requestAlertType);
            }
            else
            {
                throw new ArgumentNullException(nameof(requestAlertType));
            }

            var policies = policyService.GetPoliciesByPublisherIdAndAlertType(curUser.Id, alertType);
            var policiesIdsString = string.Join(",", policies.Select(x => x.Id));

            var searchType = Request["type"];

            var searchId = Request["id"];


            var query = @"SET NOCOUNT ON IF OBJECT_ID('tempdb..#tempOUs') IS NOT NULL
                                DROP TABLE #tempOUs
                                CREATE TABLE #tempOUs (Id_child BIGINT NOT NULL,
                                                                       Rights BIT NOT NULL);

                                IF OBJECT_ID('tempdb..#tempUsers') IS NOT NULL
                                DROP TABLE #tempUsers
                                CREATE TABLE #tempUsers (user_id BIGINT NOT NULL);

                                IF OBJECT_ID('tempdb..#tempComputers') IS NOT NULL
                                DROP TABLE #tempComputers
                                CREATE TABLE #tempComputers (comp_id BIGINT NOT NULL);

                                IF OBJECT_ID('tempdb..#tempGroups') IS NOT NULL
                                DROP TABLE #tempGroups
                                CREATE TABLE #tempGroups (group_id BIGINT NOT NULL);

                                DECLARE @now DATETIME;


                                SET @now = GETDATE();";

            if (searchType == "ou")
            {
                if (Request["search"] == "1")
                {
                    query += @";WITH OUs (id) AS
                                                  (SELECT CAST(" + searchId + @" AS BIGINT)
                                                   UNION ALL SELECT Id_child
                                                   FROM OU_hierarchy h
                                                   INNER JOIN OUs ON OUs.id = h.Id_parent)
                                                INSERT INTO #tempOUs (Id_child, Rights)
                                                  (SELECT DISTINCT id,
                                                                   " + searchId + @"
                                                   FROM OUs) OPTION (MaxRecursion 10000);";
                }
                else
                {
                    query += @" INSERT INTO #tempOUs (Id_child, Rights) VALUES (" + searchId + ", 0)";
                }
            }
            else if (searchType == "DOMAIN")
            {
                query += @" INSERT INTO #tempOUs (Id_child, Rights) (SELECT id, 0 FROM OU WHERE Id_domain = " + searchId + ")";
            }
            else
            {
                query += " INSERT INTO #tempOUs (Id_child, Rights) (SELECT id, 0 FROM OU) ";
            }

            if (!_isBroadcastEnabled)
            {
                query += @";WITH OUsList (Id_parent, Id_child) AS
                                          (SELECT Id_child AS Id_parent,
                                                  Id_child
                                           FROM #tempOUs
                                           UNION ALL SELECT h.Id_parent,
                                                            l.Id_child
                                           FROM OU_hierarchy AS h
                                           INNER JOIN OUsList AS l ON h.Id_child = l.Id_parent)
                                        UPDATE #tempOUs
                                        SET Rights = 1
                                        FROM #tempOUs t
                                        INNER JOIN OUsList l ON l.Id_child = t.Id_child
                                        LEFT JOIN policy_ou p ON p.ou_id=l.Id_parent
                                        AND p.policy_id IN (" + policiesIdsString + ")" +
                                        @"LEFT JOIN ou_groups g ON g.ou_id=l.Id_parent
                                        LEFT JOIN policy_group pg ON pg.group_id=g.group_id
                                        AND pg.policy_id IN (" + policiesIdsString + ")" +
                                        @"WHERE NOT p.id IS NULL
                                          OR NOT pg.id IS NULL";
            }

            query += @"  INSERT INTO #tempGroups (group_id)
                        (SELECT DISTINCT groups.id
                         FROM
                         GROUPS
                         INNER JOIN OU_Group og ON groups.id=og.Id_group
                         INNER JOIN #tempOUs t ON og.Id_OU=t.Id_child";

            if (!_isBroadcastEnabled)
            {
                query += @" LEFT JOIN policy_group p ON p.group_id=og.Id_group AND p.policy_id IN (" + policiesIdsString + ")" +
                         @" WHERE (t.Rights = 1 OR NOT p.group_id IS NULL) ";
            }

            if (!string.IsNullOrEmpty(SearchTerm))
            {
                query += $" AND groups.name LIKE N'%{SearchTerm}%' OR groups.display_name LIKE N'%{SearchTerm}%'";
            }

            query += ")";


            if (searchType != "ou")
            {
                query += @"	INSERT INTO #tempGroups(group_id)		
                                      (SELECT DISTINCT groups.id		
                                       FROM groups 	";


                if (!_isBroadcastEnabled)
                {
                    query += " INNER JOIN policy_group p ON p.group_id=groups.id ";
                }

                query += @"LEFT JOIN #tempGroups t ON t.group_id=groups.id
                             WHERE t.group_id IS NULL";

                if (!_isBroadcastEnabled)
                {
                    query += " AND p.policy_id IN ( " + policiesIdsString + ")";
                }

                if (searchType == "DOMAIN")
                {
                    query += " AND groups.domain_id = " + searchId;
                }

                if (!string.IsNullOrEmpty(SearchTerm))
                {
                    query += $" AND groups.name LIKE N'%{SearchTerm}%' OR groups.display_name LIKE N'%{SearchTerm}%'";
                }

                query += ")";
            }

            var selectSql = "";

            if (filterUsers.Checked)
            {
                query += @" INSERT INTO #tempUsers (user_id)(" +
                         " SELECT DISTINCT Id_user" +
                         " FROM OU_User u" +
                         " INNER JOIN #tempOUs t" +
                         " ON u.Id_OU=t.Id_child" +
                         " LEFT JOIN users ON u.Id_user = users.id";

                if (!_isBroadcastEnabled)
                {
                    query += " LEFT JOIN policy_user p ON p.user_id=u.Id_user AND p.policy_id IN (" + policiesIdsString + ")";
                    query += "  WHERE role='U' AND (t.Rights = 1 OR NOT p.id IS NULL) ";
                }
                else
                {
                    query += " WHERE users.role = 'U' ";
                }

                if (!string.IsNullOrEmpty(SearchTerm))
                {
                    query += string.Format(" AND (users.name LIKE N'%{0}%' OR users.display_name LIKE N'%{0}%')", SearchTerm);
                }

                if (!string.IsNullOrEmpty(Request["webalert"]))
                {
                    query += " AND users.client_version = 'web client'";
                }
                else
                {
                    query += " AND (users.client_version IS NULL OR  users.client_version != 'web client') ";
                }

                query += ")";

                if (searchType != "ou")
                {
                    query += @" INSERT INTO #tempUsers(user_id)(
                                 SELECT DISTINCT users.id FROM users";

                    if (!_isBroadcastEnabled)
                    {
                        query += " INNER JOIN policy_user p ON p.user_id=users.id ";
                    }

                    query += " LEFT JOIN #tempUsers t" +
                             " ON t.user_id=users.id" +
                             " WHERE t.user_id IS NULL AND users.role='U' ";

                    if (!string.IsNullOrEmpty(Request["webalert"]))
                    {
                        query += " AND users.client_version = 'web client'";
                    }
                    else
                    {
                        query += "  AND ( users.client_version IS NULL OR users.client_version != 'web client') ";
                    }

                    if (!_isBroadcastEnabled)
                    {
                        query += " AND p.policy_id IN (" + policiesIdsString + ")";
                    }

                    if (searchType == "DOMAIN")
                    {
                        query += " AND users.domain_id = " + searchId;
                    }

                    if (!string.IsNullOrEmpty(SearchTerm))
                    {
                        query += string.Format(" AND (users.name LIKE N'%{0}%' OR users.display_name LIKE N'%{0}%')", SearchTerm);
                    }

                    query += ")";
                }

                if (!isCount)
                {
                    selectSql +=
                        @" SELECT users.id, 'user' as object_type, context_id, users.name as object_name, display_name, domains.name as domain_name, users.next_request, users.last_standby_request, users.next_standby_request, users.mobile_phone, users.email, users.standby, users.last_request as last_request1, users.last_request," +
                        @" edir_context.name as context_name" +
                        @" FROM #tempUsers" +
                        @" INNER JOIN users ON user_id = users.id" +
                        " LEFT JOIN domains ON users.domain_id = domains.id " +
                        " LEFT JOIN edir_context ON edir_context.id = users.context_id";
                }
                else
                {
                    selectSql += "SELECT COUNT(1) as cnt FROM #tempUsers";
                }
            }

            if (filterGroups.Checked)
            {
                if (selectSql.Length > 0)
                {
                    selectSql += " UNION ALL ";
                }

                if (!isCount)
                {
                    selectSql +=
                        " SELECT groups.id, 'group' as object_type, context_id, groups.name as object_name, groups.display_name as display_name, domains.name as domain_name, NULL as next_request, NULL as last_standby_request, NULL as next_standby_request, '' as mobile_phone, '' as email, 0 as standby, '' as last_request1, NULL as last_request," +
                        " edir_context.name as context_name" +
                        " FROM #tempGroups" +
                        " INNER JOIN groups ON group_id = groups.id" +
                        " LEFT JOIN domains ON domains.id = groups.domain_id " +
                        " LEFT JOIN edir_context ON edir_context.id = groups.context_id";
                }
                else
                {
                    selectSql += "SELECT COUNT(1) as cnt FROM #tempGroups";
                }
            }

            if (filterComputers.Checked)
            {
                query += " INSERT INTO #tempComputers(comp_id)(" +
                         " 	SELECT DISTINCT Id_comp" +
                         " 	FROM OU_comp c" +
                         " 	INNER JOIN #tempOUs t" +
                         " 	ON c.Id_OU=t.Id_child ";

                if (!string.IsNullOrEmpty(SearchTerm))
                {
                    query += " LEFT JOIN computers ON c.Id_comp = computers.id";
                }

                if (!_isBroadcastEnabled)
                {
                    query += " LEFT JOIN policy_computer p ON p.comp_id=c.Id_comp AND p.policy_id IN (" + policiesIdsString + ")" +
                             " WHERE t.Rights = 1 OR NOT p.id IS NULL ";

                    if (!string.IsNullOrEmpty(SearchTerm))
                    {
                        query += string.Format(" AND (computers.name LIKE N'%{0}%' )", SearchTerm);
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(SearchTerm))
                    {
                        query += string.Format(" WHERE (computers.name LIKE N'%{0}%' )", SearchTerm);
                    }
                }


                query += ")";

                if (searchType != "ou")
                {
                    query += " INSERT INTO #tempComputers(comp_id)(" +
                             " SELECT DISTINCT computers.id FROM computers";

                    if (!_isBroadcastEnabled)
                    {
                        query += " INNER JOIN policy_computer p ON p.comp_id=computers.id AND p.policy_id IN (" + policiesIdsString + ")";
                    }

                    query += " LEFT JOIN #tempComputers t" +
                             " ON t.comp_id=computers.id" +
                             " WHERE t.comp_id IS NULL ";

                    if (searchType == "DOMAIN")
                    {
                        query += " AND computers.domain_id = " + searchId;
                    }

                    if (!string.IsNullOrEmpty(SearchTerm))
                    {
                        query += $" AND (computers.name LIKE N'%{SearchTerm}%' )";
                    }

                    query += ")";
                }

                if (selectSql.Length > 0)
                {
                    selectSql += " UNION ALL ";
                }

                if (!isCount)
                {
                    selectSql +=
                        " SELECT computers.id, 'computer' as object_type, context_id, computers.name as object_name, '' as display_name, domains.name as domain_name, NULL as next_request, NULL as last_standby_request, NULL as next_standby_request, '' as mobile_phone, '' as email, 0 as standby, '' as last_request1, NULL as last_request," +
                        " edir_context.name as context_name" +
                        " FROM #tempComputers" +
                        " INNER JOIN computers ON comp_id = computers.id" +
                        " LEFT JOIN domains ON domains.id = computers.domain_id " +
                        " LEFT JOIN edir_context ON edir_context.id = computers.context_id";
                }
                else
                {
                    selectSql += "SELECT COUNT(1) as cnt FROM #tempComputers";
                }
            }

            if (!isCount)
            {
                selectSql = " SELECT DISTINCT * FROM ( " + selectSql + " ) as tmp ORDER BY ";

                selectSql += string.IsNullOrEmpty(Request["sortBy"])
                                 ? " object_type, object_name ASC "
                                 : Request["sortBy"] + " ";
            }
            else
            {
                selectSql = " SELECT SUM(cnt) FROM (" + selectSql + " ) as tmp ";
            }

            query += selectSql;

            query += @"DROP TABLE #tempOUs
                        DROP TABLE #tempUsers
                        DROP TABLE #tempComputers
                        DROP TABLE #tempGroups";

            Logger.Trace($"SQL query for select resipients list: \"{query}\".");

            return query;
        }

        protected override string PageParams
        {
            get
            {
                var paramString = string.Empty;
                paramString += AddParameter(paramString, "desktop");
                paramString += AddParameter(paramString, "fu", filterUsers.Checked);
                paramString += AddParameter(paramString, "fg", filterGroups.Checked);
                paramString += AddParameter(paramString, "fc", filterComputers.Checked);
                paramString += AddParameter(paramString, "sms");
                paramString += AddParameter(paramString, "email");
                paramString += AddParameter(paramString, "simpleGroupsSelection", !string.IsNullOrEmpty(Request["simpleGroupsSelection"]) && Request["simpleGroupsSelection"].Equals("1"));
                paramString += AddParameter(paramString, "type");
                paramString += AddParameter(paramString, "id");

                return paramString;
            }
        }

        protected override string DefaultSortingOrder { get; } = "ASC";

        #endregion
    }
}