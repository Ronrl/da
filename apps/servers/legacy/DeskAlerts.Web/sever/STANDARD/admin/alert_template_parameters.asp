﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>DeskAlerts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/style9.css" rel="stylesheet" type="text/css">
</head>
<body style="margin:0" class="body">
<%
	set paramType = Request("type")

	where_sql = ""
	if paramType.count > 0 then
		for each tp in paramType
			if where_sql <> "" then
				where_sql = where_sql & ", "
				end if
			where_sql = where_sql & CLng(tp)
		next
	where_sql = " where type IN (" & where_sql & ")"
	end if
%>
<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	<tr>
	<td>
	<table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
		<tr>
		<td width=100% height=31 class="main_table_title"><a href="#" class="header_title">Parameters</a></td>
		</tr>
		<tr>
		<td height="100%" valign="top" class="main_table_body">
			<table height="100%" cellspacing="0" cellpadding="3" class="data_table" style="margin:10px">
				<tr class="data_table_title">
					<td width="300" class="table_title"><%=LNG_NAME %></td>
					<td width="100" class="table_title" width="135"><%=LNG_ACTIONS %></td>
				</tr>
<%

Set RS = Conn.Execute("select id, name from alert_parameters"& where_sql &" order by [order]")
While Not RS.EOF
	%>
		<tr>
			<td param_id="<% Response.Write RS("id") %>" width="300"><% Response.Write RS("name") %></td>
			<td width="100">
			<a href="alert_template_params_edit.asp?id=<% Response.Write RS("id") %>">
				<img src="images/action_icons/edit.png" alt="<%=LNG_EDIT %>" title="<%=LNG_EDIT %>" width="16" height="16" border="0" hspace=5>
			</a>
		</td>
		</tr>
	<%	
	RS.MoveNext		
Wend
RS.Close

%>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>