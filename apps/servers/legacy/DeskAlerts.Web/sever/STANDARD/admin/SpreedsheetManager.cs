﻿using System.Collections.Generic;
using System.IO;    
using System.Text;
using DeskAlertsDotNet.Pages;
using DeskAlertsDotNet.Parameters;
using OfficeOpenXml;

namespace DeskAlertsDotNet
{
    public class SpreedsheetManager
    {      
        public static string WriteCsvToUploadsFromDataRow(string name,  string[] keys, List<string[]> data)
        {
            // Check path
            var path = Path.Combine(Config.Data.GetString("alertsFolder"), "admin/csv");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            // Check file
            if (Path.GetExtension(name) != ".csv")
            {
                name += ".csv";
            }
                 
            var fullPath = Path.Combine(path, name);
            if (File.Exists(fullPath))
            {
                File.Delete(fullPath);
            }

            // Write data to file
            using (var sw = new StreamWriter(fullPath, false, Encoding.Unicode))
            {
                sw.WriteLine(GenerateCsvRowFromArray(keys));
                foreach (var row in data)
                {
                    sw.WriteLine(GenerateCsvRowFromArray(row));
                }

                sw.Close();
            }

            var result = Path.Combine(Config.Data.GetString("alertsDir"), "admin/csv", name);
            return result;
        }

        /// <summary>
        /// Write file in default directory
        /// </summary>
        /// <param name="name">filename</param>
        /// <param name="data">data for fill</param> 
        /// <returns>link to wrote file</returns>
        public static string WriteXlsxToUploads(string name, IEnumerable<IEnumerable<string>> data)
        {
            // Check path
            var path = Path.Combine(Config.Data.GetString("alertsFolder"), "admin/xlsx");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            
            // Check file
            if(Path.GetExtension(name) != ".xlsx")
            {
                name += ".xlsx";
            }

            var fullPath = Path.Combine(path, name);
            if (File.Exists(fullPath))
            {
                File.Delete(fullPath);
            }

            // Create excel application and worksheet
            using (var package = new ExcelPackage())
            {                                                   
                var worksheet = package.Workbook.Worksheets.Add(name);

                // Fill worksheet cells with data
                var symbsForTrim = new[] {'"'};
                var i = 1;
                foreach (var row in data)
                {
                    var j = 1;
                    foreach (var cell in row)
                    {
                        worksheet.Cells[i, j].Value = cell.Trim(symbsForTrim);
                        j++;
                    }
                    i++;
                }          

                // Save worksheet
                var bin = package.GetAsByteArray();
                File.WriteAllBytes(fullPath, bin);
            }

            var result = Path.Combine(Config.Data.GetString("alertsDir"), "admin/xlsx", name);
            return result;
        }

        private static string GenerateCsvRowFromArray(string[] array)
        {
            // symbols for correct view of arabic words  
            const char ltr = (char) 0x200E;
            const char tab = (char) 0x0009;

            var result = $"\"{ltr}\"" + string.Join($"{tab}\"{ltr}\"", array);
            return result;
        }
    }
}