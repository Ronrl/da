﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditGroupMembers.aspx.cs" Inherits="DeskAlertsDotNet.Pages.EditGroupMembers" %>
<%@ Import NameSpace="DeskAlertsDotNet.Parameters" %>
<%@ Import NameSpace="Resources" %>

<%@ Register src="RecipientsSelectionTable.ascx" TagName="RecipientsSelectionTable" TagPrefix="da"%>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" id="EditGroupMemberHtml">
<head runat="server">
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/style9.css" rel="stylesheet" type="text/css">
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script language="javascript" type="text/javascript">

    $(document).ready(function () {
      //  $("#tabs").tabs();
        $(".save_button").button();
        $(".cancel_button").button();


        
    });

    </script>
</head>
<body id="EditGroupMemberBody">
<form id="form1" runat="server">
    <table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	    <tr>
	        <td>
	            <table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
		            <tr>
		                <td width=100% height=31 class="main_table_title">
                            <a href="EditGroupMembers.aspx" class="header_title"><%=resources.LNG_GROUPS %></a>
		                </td>
		            </tr>
		            <tr>
		                <td class="main_table_body">
                            <div style="margin:10px; height: calc(100% - 16px);">
		                        <span class="work_header"><%=resources.LNG_EDIT_GROUP_MEMBERS %></span>
		                        <br><br>
                                <div><%=resources.LNG_STEP %> <strong>2</strong> <%=resources.LNG_OF %> 2: <%=resources.LNG_SELECT_MEMBERS %>:</div>
                                <br>
                                <input type="hidden" name="domain_id" value="">
                                <div name="added_users" id="added_users">
                                    <da:RecipientsSelectionTable runat="server" id="recipientsSelector" UsersLinkTable="users_groups" UsersLinkCollumn="user_id" GroupsLinkTable="groups_groups" GroupsLinkCollumn="group_id" 
                                                                OUsLinkTable="ou_groups" OUsLinkCollumn="ou_id" LinkCollumn="group_id" ComputersLinkTable="computers_groups" ComputersLinkCollumn="comp_id" ChildGroups="True"></da:RecipientsSelectionTable>
	                                <div align="right">
                                        <button type="submit" class="cancel_button" onclick="javascript:history.go(-1); return false;"><%=resources.LNG_CANCEL %></button>
                                        <asp:LinkButton runat="server" id="saveBtn" class="save_button" />
	                                </div>
	                                <input type="hidden" name="chk_max" value=""/>
	                                <input type="hidden" name="group_id" value=""/>
	                                <input type="hidden" name="ids_users" id="ids_users" value="">
	                                <input type="hidden" name="ids_groups" id="ids_groups" value="">
	                                <input type="hidden" name="ids_ous" id="ids_ous" value="">
	                                <input type="hidden" name="ids_computers" id="ids_computers" value="">
	                                <input type='hidden' name='return_page' value=''/>
		                            <img src="images/inv.gif" alt="" width="1" height="100%" border="0">
                                </div>
		                    </div>
		                </td>
		            </tr>
	            </table>
	        </td>
	    </tr>
    </table>
</form>
</body>
</html>
