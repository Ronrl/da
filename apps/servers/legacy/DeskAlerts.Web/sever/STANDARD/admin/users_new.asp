﻿<% Response.CacheControl = "no-cache" %>
<% Response.AddHeader "Pragma", "no-cache" %>
<% Response.Expires = -1 %>
<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<!-- #include file="ad.inc" -->
<%
Server.ScriptTimeout = 300
Conn.CommandTimeout = 300

check_session()

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>DeskAlerts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/style9.css" rel="stylesheet" type="text/css">
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/date.js"></script>
<script language="javascript" type="text/javascript" src="functions.js"></script>
<script language="javascript" type="text/javascript">

var serverDate;
var settingsDateFormat = "<%=GetDateFormat()%>";
var settingsTimeFormat = "<%=GetTimeFormat()%>";

var responseDbFormat = "dd/MM/yyyy HH:mm:ss";
var uiFormat = getUiDateTimeFormat(settingsDateFormat,settingsTimeFormat);


$(document).ready(function() {
	var width = $(".data_table").width();
	var pwidth = $(".paginate").width();
	if (width != pwidth) {
		$(".paginate").width("100%");
	}
	$(".search_button").button({
		icons : {
			primary : "ui-icon-search"
		}
	});
});

</script>
</head>
<script language="javascript" type="text/javascript" src="functions.js"></script>
<%
  uid = Session("uid")
  flag=1
  if(flag <> 1 AND AD=3) then
%>
<body style="margin:0px" class="body">

<script type="text/javascript">

// code to create and make AJAX request
function getFile(pURL) {
    if (window.XMLHttpRequest) { // code for Mozilla, Safari, etc 
        xmlhttp=new XMLHttpRequest();
    } else if (window.ActiveXObject) { //IE 
        xmlhttp=new ActiveXObject('Microsoft.XMLHTTP');
    }

    // if we have created the xmlhttp object we can send the request
    if (typeof(xmlhttp)=='object') {
        xmlhttp.onreadystatechange=xmlhttpResults;
        xmlhttp.open('GET', pURL, true);
        xmlhttp.send(null);
        // replace the submit button image with a please wait for the results image.
//        document.getElementById('theImage').innerHTML = '<img id="submitImage" name="submitImage" src="images/ajaxCallChangeImage_wait.gif" alt="Please Wait for AJAX Results">';
    // otherwise display an error message
    } else {
        alert('Your browser is not remote scripting enabled. You can not run this example.');
    }
}

// function to handle asynchronous call
function xmlhttpResults() {
    if (xmlhttp.readyState==4) { 
        if (xmlhttp.status==200) {
            // we use a delay only for this example
            setTimeout('loadResults()',3000);
        }
    }
}

function loadResults() {
    // put the results on the page
    if(xmlhttp.responseText=="1")
    {
	    location.href="users_new.asp"
    }
    else
    {
	    document.getElementById('mydiv').innerHTML = "<table width='100%' height='100%' border='0'><tr><td align='center'><b>Error: Synchronizing with Active Directory failed.</b></td></tr></table>";
    }
}


</script>
<div name="mydiv" id="mydiv">
<table width="100%" height="100%" border="0"><tr><td align="center">
<br><br>  <img src="images/ajax-loader.gif" border="0"> <br><br>  <b>Synchronizing with Active Directory. Please wait.</b>
</td>
</tr>
</table>
</div>

<%
  Session("flag") = 1
  else
%>
<body style="<%if(AD=3 OR EDIR=1) then %>background-color:#ffffff;<%end if%>margin:0px" class="body">
<%

	showCheckbox = request("sc")
	search_id = clng(request("id"))
	search_type = request("type")
	if search_type = "" then search_type = "organization"

	if(Request("offset") <> "") then 
		offset = clng(Request("offset"))
	else 
		offset = 0
	end if	

	if(Request("sortby") <> "") then 
		sortby = Request("sortby")
	else 
		sortby = "name"
	end if	


	myuname = request("uname")
	if(myuname <> "") then 
		searchSQL = " (users.name LIKE N'%"&Replace(myuname, "'", "''")&"%' OR users.display_name LIKE N'%"&Replace(myuname, "'", "''")&"%')"
	else 
		searchSQL = ""
	end if	
	
	'joinSQL=""
	

	
if (uid <>"") then

	checkedOUs=request("ous")
	checkedObject = ""
	if(InStr(checkedOUs, search_id)>0) then
		'checkedObject = "checked"
	end if
	'response.write "ous=" & ous
	'response.end

'counting pages to display
   if(Request("limit") <> "") then 
	limit = clng(Request("limit"))
   else 
	limit=25
   end if
  
'  limit=10
	linkclick_str = LNG_USERS
	users_arr = Array("checked","checked","checked","checked","checked","checked","checked")
	gid = 0
	gsendtoall = 0
	set RS = Conn.Execute("SELECT id FROM users WHERE id="&uid&" AND role='E'")
	if not RS.EOF then
		gid = 1
		set rs_policy = Conn.Execute("SELECT p.id, p.type, pu.user_id FROM policy p INNER JOIN policy_editor pe ON p.id=pe.policy_id LEFT JOIN policy_user pu ON pu.policy_id = p.id AND pu.user_id = 0 WHERE pe.editor_id=" & uid)
		if not rs_policy.EOF then
			if rs_policy("type")="A" then 'can send to all
				gid = 0
			elseif not IsNull(rs_policy("user_id")) then
				gid = 0
				gsendtoall = 1
			end if
		end if
		if gid = 1 or gsendtoall = 1 then
			editor_id = RS("id")
			policy_ids = editorGetPolicyIds(editor_id)
			users_arr = editorGetPolicyList(policy_ids, "users_val")
		end if
	end if
	RS.Close
	
	cnt=0
	if (search_type <>"") then
	
		showSQL = 	" SET NOCOUNT ON" & vbCrLf &_
					" IF OBJECT_ID('tempdb..#tempOUs') IS NOT NULL DROP TABLE #tempOUs" & vbCrLf &_
					" CREATE TABLE #tempOUs  (Id_child BIGINT NOT NULL, Rights BIT NOT NULL);" & vbCrLf &_
					" IF OBJECT_ID('tempdb..#tempUsers') IS NOT NULL DROP TABLE #tempUsers" & vbCrLf &_
					" CREATE TABLE #tempUsers  (user_id BIGINT NOT NULL);" & vbCrLf &_
					" DECLARE @now DATETIME;" & vbCrLf &_
					" SET @now = GETDATE(); "
					
		OUsSQL=""

		if(search_type = "ou") then
			if Request("search") = "1" then
				OUsSQL = OUsSQL & "WITH OUs (id) AS("
				OUsSQL = OUsSQL &" SELECT CAST("&search_id&" AS BIGINT) "
				OUsSQL = OUsSQL &_
						"	UNION ALL" & vbCrLf &_
						"	SELECT Id_child" & vbCrLf &_
						"	FROM OU_hierarchy h" & vbCrLf &_
						"	INNER JOIN OUs ON OUs.id = h.Id_parent" & vbCrLf &_
						") " & vbCrLf &_
						"INSERT INTO #tempOUs (Id_child, Rights) (SELECT DISTINCT id, 0 FROM OUs) Option (MaxRecursion 10000); "
			else
				OUsSQL = OUsSQL &" INSERT INTO #tempOUs (Id_child, Rights) VALUES ("&search_id&", 0)"
			end if
		elseif(search_type = "DOMAIN") then
			OUsSQL = OUsSQL &" INSERT INTO #tempOUs (Id_child, Rights) (SELECT id, 0 FROM OU WHERE Id_domain = "&search_id&")"
		else
			OUsSQL = OUsSQL & " INSERT INTO #tempOUs (Id_child, Rights) (SELECT id, 0 FROM OU) "
		end if

		if (gid = 1) then 
			OUsSQL = OUsSQL & vbCrLf &_
				" ;WITH OUsList (Id_parent, Id_child) AS (" & vbCrLf &_
				" 	SELECT Id_child AS Id_parent, Id_child FROM #tempOUs" & vbCrLf &_
				"	UNION ALL" & vbCrLf &_
				"	SELECT h.Id_parent, l.Id_child" & vbCrLf &_
				"	FROM OU_hierarchy as h" & vbCrLf &_
				"	INNER JOIN OUsList as l" & vbCrLf &_
				"	ON h.Id_child = l.Id_parent)" & vbCrLf &_ 
				" UPDATE #tempOUs" & vbCrLf &_
				" SET Rights = 1" & vbCrLf &_
				" FROM #tempOUs t" & vbCrLf &_
				" INNER JOIN OUsList l" & vbCrLf &_
				" ON l.Id_child = t.Id_child" & vbCrLf &_
				" LEFT JOIN policy_ou p ON p.ou_id=l.Id_parent AND " & Replace(policy_ids, "policy_id", "p.policy_id") & vbCrLf &_
				" LEFT JOIN ou_groups g ON g.ou_id=l.Id_parent" & vbCrLf &_
				" LEFT JOIN policy_group pg ON pg.group_id=g.group_id AND " & Replace(policy_ids, "policy_id", "pg.policy_id") & vbCrLf &_
				" WHERE NOT p.id IS NULL OR NOT pg.id IS NULL"
				
		end if

		usersSQL = " INSERT INTO #tempUsers (user_id)(" & vbCrLf &_
					" SELECT DISTINCT Id_user" & vbCrLf &_
					" FROM OU_User u" & vbCrLf &_
					" INNER JOIN #tempOUs t" & vbCrLf &_
					" ON u.Id_OU=t.Id_child" & vbCrLf &_ 
					" LEFT JOIN users ON u.Id_user = users.id"
		
		whereSQL = ""
		
		if (gid = 1) then 
			usersSQL = usersSQL & vbCrLf &_
					" LEFT JOIN policy_user p ON p.user_id=u.Id_user AND "&policy_ids
			whereSQL = " AND (t.Rights = 1 OR NOT p.id IS NULL) "
		end if
		
		usersSQL = usersSQL & " WHERE role='U' " & whereSQL
			
		if (searchSQL<>"") then 
			usersSQL = usersSQL & " AND " & searchSQL
		end if
		usersSQL = usersSQL & ") "
		
		if (search_type <> "ou") then
			usersSQL = usersSQL & vbCrLf &_
						" INSERT INTO #tempUsers(user_id)(" & vbCrLf &_
						" SELECT DISTINCT users.id FROM users "
			
			if (gid = 1) then 
				usersSQL = usersSQL & " INNER JOIN policy_user p ON p.user_id=users.id AND "&policy_ids
			end if
			
			usersSQL = usersSQL & vbCrLf &_	
					" LEFT JOIN #tempUsers t" & vbCrLf &_
					" ON t.user_id=users.id" & vbCrLf &_
					" WHERE t.user_id IS NULL and role='U'"
			
			if (search_type = "DOMAIN") then
				usersSQL = usersSQL & " AND users.domain_id = "&search_id
			end if 
				if (searchSQL<>"") then 
					usersSQL = usersSQL & " AND " & searchSQL
				end if
				usersSQL = usersSQL & ") "	
		end if
		
		usersSQL = usersSQL & vbCrLf &_
					" SELECT COUNT(1) as cnt FROM #tempUsers" & vbCrLf &_
					" SELECT user_id as id, users.next_request, users.last_standby_request, users.next_standby_request, users.context_id, users.mobile_phone, users.email, users.name as name, users.display_name, users.standby, users.domain_id, domains.name as domain_name, users.deskbar_id, users.reg_date, users.last_date, users.last_request as last_request1, users.last_request, users.client_version, " & vbCrLf &_
					" edir_context.name as context_name" & vbCrLf &_
					" FROM #tempUsers t" & vbCrLf &_
					" LEFT JOIN users ON t.user_id = users.id" & vbCrLf &_
					" LEFT JOIN domains ON domains.id = users.domain_id" & vbCrLf &_
					" LEFT JOIN edir_context ON edir_context.id = users.context_id" & vbCrLf &_
					" WHERE role='U'" & vbCrLf &_
					" ORDER BY " & sortby & vbCrLf &_
					" DROP TABLE #tempOUs" & vbCrLf &_
					" DROP TABLE #tempUsers"
		
		showSQL = showSQL & OUsSQL & usersSQL
		
		Response.Write showSQL
		Set RS = Conn.Execute(showSQL)
		

		if(Not RS.EOF) then
			cnt=RS("cnt")
			SET RS = RS.NextRecordSet
		end if
		
		j=cnt/limit
		
	end if

%>

<%if(AD<>3 AND EDIR<>1) then%>
<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	<tr>
	<td>
	<table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
		<tr>
		<td width=100% height=31 class="main_table_title"><a href="" class="header_title"><%=LNG_USERS %></a></td>
		</tr>
		<tr>
		<td height="100%" class="main_table_body">
		<div style="margin:10px">
	<%end if%>
<form name="search_users" action="users_new.asp?sc=<%=showCheckbox %>&id=<%=search_id %>&type=<%=search_type %>&ous=<%=checkedOUs %>&offset=0&limit=<%=limit %>&sortby=<%=sortby %>" method="POST"><table width="100%" border="0"><tr valign="middle"><td width="240">
<%=LNG_SEARCH_USERS %>: <input type="text" name="uname" value="<%=HTMLEncode(myuname) %>"/></td>
<td width="1%">
	<a name="sub" class="search_button" onclick="javascript:document.forms[0].submit()"><%=LNG_SEARCH %></a>
	<input type="hidden" name="search" value="1" />
</td>
<%
if myuname <> "" or Request("search") = "1" then
	response.write "<td> "&LNG_YOUR_SEARCH_BY &" """&HTMLEncode(myuname)&""" "&LNG_KEYWORD &":</td>"
end if
%>
<td>
<%
if(AD=3) then
%>

<!-- <div align="right"><a href="ad_sync_form.asp" target="_blank"><img src="images/refresh_button.gif" alt="refresh" border="0"></a></div><br> -->

<%
else
%>

<%
end if
%>
</td></tr></table>
</form>


<%


if(cnt>0) then

	page="users_new.asp?sc="&showCheckbox&"&id="&search_id&"&type="&search_type&"&ous="&checkedOUs&"&uname=" & Server.URLEncode(myuname) &"&search="&Request("search")&"&"
	name=LNG_USERS
	Response.Write make_pages(offset, cnt, limit, page, name, sortby) 

if AD=0 and users_arr(2) = "checked" then
	response.write "<br><A href='#' onclick=""javascript: return LINKCLICK('"&linkclick_str&"');""><img src='images/delete_button.gif' border='0'/></a><br/><br>"
	response.write "<form action='delete_objects.asp' name='delete_objs' id='delete_objs' method='post'><input type='hidden' name='objType' value='users'>"
end if

%>



		<table width="100%" height="100%" cellspacing="0" cellpadding="3" class="data_table">
		<tr class="data_table_title">
		<%if(showCheckbox=1) then%>
<td width="10"><input type='checkbox' name='select_all' id='select_all' onclick='javascript: select_all_objects_ou();' ></td>
<%end if%>

		<td class="table_title"><% response.write sorting(LNG_USERNAME,"name", sortby, offset, limit, page) %></td>

<% if (AD = 3) then %>		
<td class="table_title"><% response.write sorting(LNG_DISPLAY_NAME,"display_name", sortby, offset, limit, page) %></td> 
<td class="table_title"><% response.write sorting(LNG_DOMAIN,"domains.name", sortby, offset, limit, page) %></td> 
<%end if%>

<% if (AD = 0) then %>		<td class="table_title"><% response.write sorting(LNG_REGISTERED,"reg_date", sortby, offset, limit, page) %></td> <% end if %>

<%if(showCheckbox<>1) then%>
<% if (AD = 3 OR SMS = 1) then%>
<td class="table_title"><% response.write sorting(LNG_MOBILE_PHONE,"mobile_phone", sortby, offset, limit, page) %></td>
<%end if%>

<% if (AD = 3 OR EM = 1) then%>
<td class="table_title"><% response.write sorting(LNG_EMAIL,"email", sortby, offset, limit, page) %></td>
<% end if %>


<% if (EDIR = 1) then %>		<td class="table_title"><%=LNG_CONTEXT %></td> <% end if %>

		<td class="table_title"><% response.write sorting(LNG_ONLINE,"last_request1", sortby, offset, limit, page) %></td>
		<td class="table_title"><% response.write sorting(LNG_LAST_ACTIVITY,"last_request", sortby, offset, limit, page) %></td>
		<td class="table_title"><% response.write sorting(LNG_CLIENT_VERSION,"client_version", sortby, offset, limit, page) %></td>
<% if (AD = 3) then %>		<td class="table_title"><%=LNG_ACTIONS %></td> <% end if%>
<%end if%>				
				</tr>


<%

'show main table

	'Set RS = Conn.Execute("SELECT DISTINCT users.id as id, next_request, last_standby_request, next_standby_request, context_id, mobile_phone, email, users.name as name, display_name, standby, domain_id, domains.name as domain_name, deskbar_id, reg_date, last_date, convert(varchar,last_request) as last_request1, last_request FROM users "&joinSQL&" WHERE role='U'" & searchSQL & " ORDER BY "&sortby)
	num=offset
	if(Not RS.EOF) then RS.Move(offset) end if
	Do While Not RS.EOF
		num=num+1
		if((offset+limit) < num) then Exit Do end if
		if(num > offset AND offset+limit >= num) then
	reg_date=RS("reg_date")
	last_date=RS("last_date")
	if(Not IsNull(RS("last_request"))) then
		last_activ = CStr(RS("last_request"))

	        mydiff=DateDiff ("n", RS("last_request"), now_db)
			if(RS("next_request")<>"") then
				online_counter = Int(CLng(RS("next_request"))/60) * 2
			else
				online_counter = 2
			end if
			
		if(mydiff > online_counter) then
			online="<img src='images/offline.gif' alt='"& LNG_OFFLINE &"' title='"& LNG_OFFLINE &"' width='11' height='11' border='0'>"
		else
			online="<img src='images/online.gif' alt='"&LNG_ONLINE &"' title='"&LNG_ONLINE &"' width='11' height='11' border='0'>"
		end if
	else
		last_activ = ""
		online="<img src='images/offline.gif' alt='"& LNG_OFFLINE &"' title='"& LNG_OFFLINE &"' width='11' height='11' border='0'>"
	end if

	standby_diff=DateDiff ("n", RS("last_standby_request"), now_db)
	if(RS("next_standby_request")<>"" AND RS("next_standby_request")<>"0") then
		standby_counter = Int(CLng(RS("next_standby_request"))/60) * 2
	else
		standby_counter = 10
	end if
	
	if(RS("standby")=1 AND (standby_diff < standby_counter)) then
		online="<img src='images/standby.gif' alt='"&LNG_STAND_BY &"' title='"&LNG_STAND_BY &"' width='11' height='11' border='0'>"
	end if

	if(IsNull(RS("name"))) then
	uname=LNG_UNREGISTERED
	else
	uname = RS("name")
	end if

 if (AD = 3 OR SMS = 1) then
	mobile="&nbsp;"
	if(RS("mobile_phone")<>"") then
		mobile=HtmlEncode(RS("mobile_phone"))
	end if
 end if

 if (AD = 3 OR EM = 1) then
	email="&nbsp;"
	if(RS("email")<>"") then
		email=HtmlEncode(RS("email"))
	end if
 end if

	domain="&nbsp;"
	if (AD = 3) then 	
		if(Not IsNull(RS("domain_id"))) then
			if(RS("domain_id")<>"") then
			    domain=HtmlEncode(RS("domain_name"))
			else
				domain="&nbsp;"
			end if
		end if
	end if
	if (EDIR = 1) then 
		strContext="&nbsp;"
		if(Not IsNull(RS("context_name"))) then
			strContext=HtmlEncode(RS("context_name"))
		end if
	end if 

end if
	strObjectID=RS("id")
	clientVersion = HtmlEncode(RS("client_version"))
        Response.Write "<tr>"
if(showCheckbox=1) then
%>
	<td><input type="checkbox" name="objects" id="object<%=strObjectID%>" value="<%=strObjectID%>" <%=checkedObject %> onclick="checkboxClicked(event);"></td>
<%
end if
response.write "<td>" & HtmlEncode(uname) & "</td><td align=center>"

 if (AD = 0) then 	
	Response.Write reg_date 
	Response.Write "</td><td align=center>"
 end if
 if (AD = 3) then 	

	if(RS("display_name")<>"") then
		response.write HtmlEncode(RS("display_name"))
	else
		response.write "&nbsp;"
	end if
	Response.Write "</td><td align=center>"
	
	Response.Write HtmlEncode(domain)
	Response.Write "</td>"
	
 end if
 if(showCheckbox<>1) then
 if (AD = 3 OR SMS=1) then
	Response.Write "<td align=center>"
	Response.Write mobile
	Response.Write "</td>"
 end if

 if (AD = 3 OR EM=1) then
	Response.Write "<td align=center>"
	Response.Write email
	Response.Write "</td>"
 end if
 if (EDIR = 1) then
	strContext=replace(strContext,",ou=",".")
	strContext=replace(strContext,",o=",".")
	strContext=replace(strContext,",dc=",".")
	strContext=replace(strContext,",c=",".")

	strContext=replace(strContext,"ou=","")
	strContext=replace(strContext,"o=","")
	strContext=replace(strContext,"dc=","")
	strContext=replace(strContext,"c=","")
	Response.Write "<td align=center>"
	Response.Write strContext
	Response.Write "</td>"
 end if

	Response.Write "<td align=center>"
	Response.Write online &"</td>"
	Response.write""
	%>
		<td align = "center">
		<% if last_activ <> "" then %>
			<script language="javascript">
				document.write(getUiDate('<%=last_activ%>'));
			</script>
		<% else %>
			&nbsp;
		<% end if %>
		</td>
	<%
	Response.write"<td align=center>" & clientVersion & "</td>"
 if (AD = 3) then
	Response.Write "<td align=center nowrap><a href='#' onclick=""javascript: window.open('user_view_details.asp?id=" & CStr(RS("id")) & "','', 'status=0, toolbar=0, width=350, height=350, scrollbars=1')""><img src='images/action_icons/preview.png' alt='"&LNG_VIEW_USER_DETAILS &"' title='"&LNG_VIEW_USER_DETAILS &"' width='16' height='16' border='0' hspace=5></a>"
	if (users_arr(1) = "checked") then
		Response.Write "<a href='edit_user.asp?wh=1&id="&CStr(RS("id"))&"&return_page="&Server.URLEncode("users_new.asp?"&Request.QueryString)&"'><img src='images/action_icons/edit.png' alt='"&LNG_EDIT_USER&"' title='"&LNG_EDIT_USER&"' width='16' height='16' border='0' hspace=5></a>"
	end if
	if (users_arr(2) = "checked") then
		Response.Write "<a href='user_delete.asp?id="&CStr(RS("id"))&"&return_page="&Server.URLEncode("users_new.asp?"&Request.QueryString)&"' onclick=""javascript: return LINKCLICK();""><img src='images/action_icons/delete.gif' alt='X' title='"&LNG_DELETE_USER&"' width='16' height='16' border='0' hspace=5></a>"
	end if
	Response.Write "</td>"
 end if
	end if 'showCheckbox
	Response.Write "</tr>"
'	end if

	RS.MoveNext
	Loop
	RS.Close


%>
			</table>
<%

if AD=0 and users_arr(2) = "checked" then
	response.write "</form><br/><A href='#' onclick=""javascript: return LINKCLICK('"&linkclick_str&"');""><img src='images/delete_button.gif' border='0'/></a><br><br>"
end if

	'page="users_new.asp?uname=" & Server.URLEncode(myuname) & "&"
	'name="users"
	Response.Write make_pages(offset, cnt, limit, page, name, sortby) 


else

Response.Write "<center><b>"&LNG_THERE_ARE_NO_USERS &".</b><br><br></center>"

end if

%>

<%if(AD<>3 AND EDIR<>1) then%>
		 <img src="images/inv.gif" alt="" width="1" height="100%" border="0">
		</div>
		</td>
		</tr>
	</table>
	</td>
	</tr>
</table>
<%end if%>

<%
else

  Response.Redirect "index.asp"

end if

end if

%> 
</body>
</html>
<!-- #include file="db_conn_close.asp" -->