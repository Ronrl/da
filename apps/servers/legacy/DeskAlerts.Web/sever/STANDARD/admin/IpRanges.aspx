﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="IpRanges.aspx.cs" Inherits="DeskAlertsDotNet.Pages.IpRanges" %>
<%@ Import NameSpace="DeskAlertsDotNet.Parameters" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<title>DeskAlerts</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link href="css/style9.css" rel="stylesheet" type="text/css">
        <link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <link href="css/jquery/jquery_timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css">
        <script language="javascript" type="text/javascript" src="functions.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/date.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery_timepicker/jquery-ui-timepicker-addon.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/jquery/datepicker_fix.js"></script>
        <script language="javascript" type="text/javascript">


            $(document).ready(function() {
                $(".delete_button").button({
                });

                $("#selectAll").click(function () {
                    var checked = $(this).prop('checked');

                    var checkBoxSelector = !checked ? "input[type=checkbox].content_object:checked" : "input[type=checkbox].content_object:not(\":checked\")";


                    $(checkBoxSelector)
                        .each(function () {

                            $(this).prop("checked", checked);

                            if (checked)
                                window.parent.addItem(this);
                            else
                                window.parent.removeItem(this);



                        });
                });

                $(".content_object")
                    .change(function () {
                        var check = $(this).prop("checked");

                        if (check) {
                            window.parent.addItem(this);
                        } else {
                            window.parent.removeItem(this);
                        }
                    });

                var selectedIpGroups = window.parent.getSelectedIpGroups();
                $(selectedIpGroups).each(function () {

                    var groupId = $(this).prop("id");

                    $("#recipient_ipgroup_" + groupId).prop('checked', true);
                })
                
            });

            function checkRecipient(id) {
                $("#" + id).prop('checked', true);
            }

            function unCheckRecipient(id) {
                $("#" + id).prop('checked', false);
            }

        </script>
</head>
<body>
    <form id="form1" runat="server">
    <table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	    <tr>
	    <td>
	    <table width="35%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
		    <tr>
		    </tr>
            <tr>
            <td class="main_table_body" height="100%">
	                <br /><br />
                	
			<div style="margin-left:10px" runat="server" id="mainTableDiv">

			   <table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr><td> 
					<div style="margin-left:10px" id="topPaging" runat="server"></div>
				</td></tr>
				</table>
				<br>
			
				<br><br>
				<asp:Table  style="padding-left: 0px;margin-left: 10px;margin-right: 10px;" runat="server" id="contentTable" Width="50%" Height="100%" CssClass="data_table" CellSpacing="0" CellPadding="3">
				<asp:TableRow CssClass="data_table_title">
					<asp:TableCell runat="server" id="headerSelectAll" CssClass="table_title" Width="2%" >
						<asp:CheckBox id="selectAll" runat="server"/>
					</asp:TableCell>
					<asp:TableCell  runat="server" id="nameCell" CssClass="table_title" ></asp:TableCell>
				</asp:TableRow>
				</asp:Table>
				<br>
				
				 <br><br>
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				 
				<tr><td> 
					<div style="margin-left:10px" id="bottomPaging" runat="server"></div>
				</td></tr>
				</table>
			</div>
			<div runat="server" id="NoRows">
                <br />
                <br />
				<center><b><!-- Add here your message сcontent withou--></b></center>
			</div>
			<br><br>
            </td>
            </tr>
           
 
            
             </table>
            </table>
    </form>
</body>
</html>
