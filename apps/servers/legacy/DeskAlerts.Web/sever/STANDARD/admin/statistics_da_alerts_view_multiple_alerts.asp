
<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%
'check_session()
%>
<!-- #include file="Common.asp" -->
<!-- #include file="header.asp" -->
<!-- #include file="footer.asp" -->
<!-- #include file="libs/FusionCharts.asp" -->
<%
'On Error Resume Next
' index CustomIncludes End
'-------------------------------

'===============================
' Save Page and File Name available into variables
'-------------------------------
Dim sFileName
Dim sTemplateFileName
Dim sPaginateFileName
Dim intSessionUserId

sFileName = "statistics_da_alerts_view_multiple_alerts.asp"
sTemplateFileName = "statistics_da_alerts_view_multiple_alerts.html"
sPaginateFileName = "paginate.html"
'===============================
  

intSessionUserId = "blabla"
if (intSessionUserId <> "") then


' index Show begin

'===============================
' Display page
'-------------------------------
' Load HTML template for this page
'-------------------------------
LoadTemplate alerts_dir & "\" & templateFolder & "\" & sTemplateFileName, "main"
SetVar "DListUsersStat", ""
'-------------------------------
' Load HTML template of Header and Footer
'-------------------------------
LoadTemplate sHeaderFileName, "Header"
LoadTemplate sFooterFileName, "Footer"
LoadTemplate sSearchDetailsFormFileName, "SearchForm"
'-------------------------------
SetVar "Menu", ""
SetVar "DateForm", ""

pageType=request("type")
alert_id=Clng(request("id"))

searchSQL = ""
search = request("search")
if search <> "" then searchSQL = " AND (users.name LIKE N'%"&replace(search, "'", "''")&"%' OR users.display_name LIKE N'%"&replace(search, "'", "''")&"%')"


if(pageType="rec") then
	PageName(LNG_RECEIVED)
end if

if(Request("offset") <> "") then 
	offset = clng(Request("offset"))
else 
	offset = 0
end if	

if(Request("limit") <> "") then 
	limit = clng(Request("limit"))
else 
	limit=25
end if

SetVar "searchTerms", HTMLEncode(search)
SetVar "page", sFileName & "?id="&alert_id&"&type="&pageType&"&offset=0&limit="&limit&"&sortby="
SetVar "searchName", "users"

SetVar "LNG_USERNAME", "Username"
SetVar "LNG_PART", "Alert part"
SetVar "LNG_CONFIRMED", "Confirmed"
SetVar "default_lng", default_lng
SetVar "leftCom", ""
SetVar "rightCom", ""
'doesnt received
	Set alertRS = Conn.Execute("SELECT type2, sent_date, class FROM alerts WHERE id="& alert_id)
	if(alertRS.EOF) then
		Set alertRS = Conn.Execute("SELECT type2, sent_date, class FROM archive_alerts WHERE id="& alert_id)
	end if
	if(Not alertRS.EOF) then

    Set snlg = Conn.Execute("Select part_id FROM multiple_alerts WHERE alert_id="& alert_id)
    if not snlg.EOF then
        snlgVal = snlg("part_id")
        if snlgVal = -1 then
            SetVar "leftCom", "<!--"
            SetVar "rightCom", "-->"
        end if
    end if
	mytype=alertRS("type2")
	strAlertDate = alertRS("sent_date")
	classid = alertRS("class")
	if classid = "2" or classid = "8" then
		received_from = "alerts_read"
		archive_received_from = "archive_alerts_read"
	else
		received_from = "alerts_received"
		archive_received_from = "archive_alerts_received"
	end if
	
	'alertRS.Close

    SQL = "SELECT stat_alert_id as stat FROM multiple_alerts WHERE alert_id = "& alert_id     
        Set RS = Conn.Execute(SQL)
        Dim stat_alert_id 
        stat_alert_id = 0
       
        if Not RS.EOF then
            stat_alert_id = RS("stat")
        end if

		SQL = "SELECT COUNT(DISTINCT id) as mycnt FROM (SELECT users.id FROM users INNER JOIN "& received_from &" ON users.id="& received_from &".user_id INNER JOIN multiple_alerts ma ON ma.stat_alert_id  = '"& stat_alert_id &"' and "& received_from &".alert_id = ma.alert_id WHERE role='U' " & searchSQL
		SQL = SQL & " UNION ALL "
		SQL = SQL & "SELECT users.id FROM users INNER JOIN "& archive_received_from &" ON users.id="& archive_received_from &".user_id INNER JOIN multiple_alerts ma ON ma.stat_alert_id  = '"& stat_alert_id &"' and "& archive_received_from &".alert_id = ma.alert_id WHERE role='U' AND ( client_version != 'web client' OR client_version is NULL ) " & searchSQL
		SQL = SQL & ") t"
		Set RS = Conn.Execute(SQL)

	cnt=0
	if(Not IsNull(RS)) then
		do while Not RS.EOF
			if(Not IsNull(RS("mycnt"))) then cnt=cnt+clng(RS("mycnt")) end if
			RS.MoveNext
		loop
		RS.Close
		j=cnt/limit
		end if
	end if

if(cnt>0) then
	page=sFileName & "?id="&alert_id&"&type="&pageType&"&search="&Server.URLEncode(search)&"&"
	name="Users"
	SetVar "paging", make_pages(offset, cnt, limit, page, name, sortby) 
end if

'show main table

num=0

    if pageType="drec" then
    SQL = "SELECT DISTINCT * FROM (SELECT users.id as id, users.name as name, users.display_name, users.domain_id as domain_id, ma.alert_id as alertId, ma.part_content as part, ma.confirmed as confirmed FROM users INNER JOIN alerts_sent_stat ON users.id=alerts_sent_stat.user_id INNER JOIN multiple_alerts ma ON ma.alert_id = alerts_sent_stat.alert_id  WHERE role='U' AND users.id NOT IN (SELECT user_id FROM "& received_from &" INNER JOIN multiple_alerts ma ON ma.alert_id = "& received_from &".alert_id  WHERE ma.stat_alert_id='"& stat_alert_id &"') AND ma.stat_alert_id='"& stat_alert_id &"' " & searchSQL
		    SQL = SQL & " UNION ALL "
		    SQL = SQL & "SELECT users.id as id, users.name as name, users.display_name, users.domain_id as domain_id, ma.alert_id as alertId, ma.part_content as part, ma.confirmed as confirmed FROM users INNER JOIN archive_alerts_sent_stat ON users.id=archive_alerts_sent_stat.user_id INNER JOIN multiple_alerts ma ON ma.alert_id = archive_alerts_sent_stat.alert_id WHERE role='U' AND users.id NOT IN (SELECT user_id FROM "& archive_received_from &" INNER JOIN multiple_alerts ma ON ma.alert_id = "& archive_received_from &".alert_id WHERE ma.stat_alert_id='"& stat_alert_id &"') AND ma.stat_alert_id='"& stat_alert_id &"' " & searchSQL
		    SQL = SQL & ") t"   
    else
    SQL = "SELECT DISTINCT * FROM (SELECT ma.part_id as partid, ma.alert_id as alertId, ma.part_content as part, ma.confirmed as confirmed, "& received_from &".user_id as id, users.name as name, users.display_name, users.domain_id as domain_id FROM users INNER JOIN "& received_from &" ON users.id="& received_from &".user_id INNER JOIN multiple_alerts ma ON ma.stat_alert_id  = '"& stat_alert_id &"' and "& received_from &".alert_id = ma.alert_id WHERE role='U' " & searchSQL
		SQL = SQL & " UNION ALL "
		SQL = SQL & "SELECT ma.part_id as partid,  ma.alert_id as alertId, ma.part_content as part, ma.confirmed as confirmed, "& archive_received_from &".user_id as id, users.name as name, users.display_name, users.domain_id as domain_id FROM users INNER JOIN "& archive_received_from &" ON users.id="& archive_received_from &".user_id INNER JOIN multiple_alerts ma ON ma.stat_alert_id  = '"& stat_alert_id &"' and "& archive_received_from &".alert_id = ma.alert_id WHERE role='U' " & searchSQL
		SQL = SQL & ") t ORDER BY id, partid"      		
    end if
    Set RS = Conn.Execute(SQL)
   
sqlPart = ""
Do While Not RS.EOF
	num=num+1
	if(num > offset AND offset+limit >= num) then
        if(IsNull(RS("alertId"))) then
            sqlPart = sqlPart + ""
        else
            sqlPart = sqlPart + " and ma.alert_id != " & RS("alertId")
        end if
   		if(IsNull(RS("name"))) then
			name="Unregistered"
		else
			strUserName = RS("name")
		end if
		if AD = 3 and RS("domain_id") then
			Set RS3 = Conn.Execute("SELECT name FROM domains WHERE id=" & RS("domain_id"))
			if(Not RS3.EOF) then
				domain=RS3("name")
			else
				domain="&nbsp;"
			end if
			RS3.close
		end if

            SetVar "strUserName", strUserName

            SetVar "part", RS("part") 
            Dim confi
            if RS("confirmed") = 0 then 
                confi = "No" 
            else 
                confi = "Yes"
            End if
                SetVar "confirmed", confi

		if Len(RS("display_name")) > 0 then
			SetVar "strDisplayName", " (" & RS("display_name") & ")"
		else
			SetVar "strDisplayName", ""
		end if
		Parse "DListUsersStat", True
	end if
	RS.MoveNext
Loop
RS.Close
SetVar "sqsqsqsl2", sqlPart

if snlgVal = -1 and pageType <> "drec" then
    received_from = "alerts_received"
	archive_received_from = "archive_alerts_received"

     SQL = "SELECT DISTINCT * FROM (SELECT ma.part_id as partid, ma.part_content as part, ma.confirmed as confirmed, "& received_from &".user_id as id, users.name as name, users.display_name, users.domain_id as domain_id FROM users INNER JOIN "& received_from &" ON users.id="& received_from &".user_id INNER JOIN multiple_alerts ma ON ma.stat_alert_id  = '"& stat_alert_id &"' and "& received_from &".alert_id = ma.alert_id WHERE role='U' " & sqlPart  & searchSQL
		SQL = SQL & " UNION ALL "
		SQL = SQL & "SELECT ma.part_id as partid, ma.part_content as part, ma.confirmed as confirmed, "& archive_received_from &".user_id as id, users.name as name, users.display_name, users.domain_id as domain_id FROM users INNER JOIN "& archive_received_from &" ON users.id="& archive_received_from &".user_id INNER JOIN multiple_alerts ma ON ma.stat_alert_id  = '"& stat_alert_id &"' and "& archive_received_from &".alert_id = ma.alert_id WHERE role='U' " & sqlPart  & searchSQL
		SQL = SQL & ") t ORDER BY id, partid"
        
		Set RS = Conn.Execute(SQL)
    Do While Not RS.EOF
	num=num+1
	if(num > offset AND offset+limit >= num) then
       
   		if(IsNull(RS("name"))) then
			name="Unregistered"
		else
			strUserName = RS("name")
		end if
		if AD = 3 and RS("domain_id") then
			Set RS3 = Conn.Execute("SELECT name FROM domains WHERE id=" & RS("domain_id"))
			if(Not RS3.EOF) then
				domain=RS3("name")
			else
				domain="&nbsp;"
			end if
			RS3.close
		end if

            SetVar "strUserName", strUserName

            SetVar "part", RS("part") 
            SetVar "confirmed", "No"

		if Len(RS("display_name")) > 0 then
			SetVar "strDisplayName", " (" & RS("display_name") & ")"
		else
			SetVar "strDisplayName", ""
		end if
		Parse "DListUsersStat", True
	end if
	RS.MoveNext
Loop
RS.Close
end if
'--------------------


'-------------------------------
' Step through each form
'-------------------------------
Header_Show
Footer_Show
'-------------------------------
' Process page templates
'-------------------------------
Parse "Header", False
Parse "Footer", False
Parse "main", False
'-------------------------------
' Output the page to the browser
'-------------------------------
Response.write PrintVar("main")

' index Show End

'-------------------------------
' Destroy all object variables
'-------------------------------

' index Close Event begin
' index Close Event End

UnloadTemplate
'===============================

else

Login(Request("intErrorCode"))

end if

'===============================
' Display Grid Form
'-------------------------------

%><!-- #include file="db_conn_close.asp" -->