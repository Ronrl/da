﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Main.aspx.cs" Inherits="DeskAlertsDotNet.Pages.Main" %>
<%@ Import NameSpace="DeskAlertsDotNet.Parameters" %>
<%@ Import Namespace="Resources" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" class="full_height">
<head runat="server">
<title>DeskAlerts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
<link href="css/style.css?v=9.1.12.28" rel="stylesheet" type="text/css"/>
<link href="css/MetroJs.lt.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="jscripts/json2.js"></script>
<script type="text/javascript" src="functions.js"></script>
<script type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>     
<script type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script type="text/javascript" src="jscripts/jquery/jquery.cookies.2.2.0.min.js"></script>
<script type="text/javascript" src="jscripts/MetroJs.lt.min.js"></script>
<style>
html, body {
	border: 0px;
	overflow: auto;
	height : 100%;
	padding:0px;
	margin:0px;
}
table {
	border-spacing:0px;
}
</style>
    
    <div id="RejectedAlertsDialog" style="display:none;text-align:center">
	    <br /><br />
	   
	    <label style="font-size:14px"><b><% =resources.LNG_YOU_HAVE_REJECTED_ALERTS.Replace("{n}", rejectedAlertsCount.ToString()) %></b></label><br /><br /><br /><br />
	    <a class="contact_us_button" target="work_area" id="rejectedAlertsButton" ><% =resources.LNG_SHOW_REJECTED %></a>
	</div>
	
	<div id="NotApprovedAlertsDialog" style="display:none;text-align:center">
	    <br /><br />
	    
	     <label style="font-size:14px"><b><% =resources.LNG_YOU_HAVE_NOT_APPROVED.Replace( "{n}",notApporvedAlertsCount.ToString() ) %></b></label><br /><br /><br /><br />
	     <a  class="contact_us_button" target="work_area" id="notApprovedButton"><% =resources.LNG_SHOW_NOT_APPROVED %></a>
	</div>
    
<script type="text/javascript">
	var topFrame;
	function expandHelp(){
		$('#helpDiv').show();
		help_close_span = topFrame.contentWindow.document.getElementById("help_close_span");
		$(help_close_span).show();
		setTimeout(function(){
			$('#help_frame').css("height","50%");
			setTimeout(function(){
			$('#help_frame').css("height","100%");
			},100);
		},100);

		helpOpenerIsHidden = true;
		cookieObj.help = true;
		var cookieStr = JSON.stringify(cookieObj);
		thisIsntWorkingWithoutMe.set('deskalerts', cookieStr);
	}
	
	$(document).ready(function() {
	    $("#send_btn").button({label:"<%=resources.LNG_SEND %>",icons: {primary: "ui-icon-newwin"}}).click(function(){ sendEmail(); });
	});

    function sendEmail() {

        var email = $("#email_box").val();

        var comment = $("#comment").val();

        var arr = JSON.parse(email);

        if (email.length == 0 || email.indexOf("@") == -1 || email.indexOf(".") == -1) {
            alert('<% =resources.LNG_WRONG_EMAIL%>');
        } else {

            $.get("mail_sender.asp?email=" + arr + "&comment=" + comment,
                function(data) {
                    alert(data);resources.
                    $('#email_box').text("");
                    $('#email_box').val("");
                    $("#invite_dialog_form").dialog("close");
                });
        }

    }

    $(function () {


        $('#email_box').text("");
        $('#email_box').val("");

    });

    function minimizeHelp() {
        $('#helpDiv').hide();
        /*$(helpOpener).show();*/
        help_close_span = topFrame.contentWindow.document.getElementById("help_close_span");
        $(help_close_span).hide();
        helpOpenerIsHidden = false;
        cookieObj.help = false;
        var cookieStr = JSON.stringify(cookieObj);
        thisIsntWorkingWithoutMe.set('deskalerts', cookieStr);
    }

    function toggleHelp() {
        if (helpOpenerIsHidden) {
            minimizeHelp();
        } else {
            expandHelp();
        }
    }

    function toggleSettings() {
        if (!settingsOpened) {

            //   $("#settingsFrame").attr("src", "SettingsSelection.aspx");

            settingsOpened = true;

            $("#settingsDialog").dialog(
                {
                    autoOpen: false,
                    height: 330,
                    width: 1010,
                    modal: true,
                    resizable: false,
                    close: function (event, ui) { settingsOpened = false; }

                }).dialog('open');


        }
        else {
            settingsOpened = false;
            $("#settingsDialog").dialog('close');
            // $("settingsFrame").attr("src", "");
        }

    }

    var thisIsntWorkingWithoutMe = $.cookies; //wtf
    var cookieObj = new Object();
    var helpOpener;
    var firstTime = true;
    var helpOpenerIsHidden = true;
    var settingsOpened = false;

    function initHelp() {
        var showHelp = '<%=Settings.Content["ConfEnableHelp"]%>';
        if (showHelp != '1') {
            $("#helpDiv").hide();
            $(helpOpener).hide();
            helpOpenerIsHidden = true;
            return;
        }
        if (firstTime) {
            if (cookieObj.help == true) {
                expandHelp();
            } else {
                minimizeHelp();
            }
            firstTime = false;
        }
        var main_target = $("#main_frame").get(0).contentWindow.location.pathname.split('/').pop();
        var search = $("#main_frame").get(0).contentWindow.location.search;
        var about = pageTheme(main_target, search);
        if (main_target != "settings_social_settings.asp" &&
            main_target != "settings_common.asp" &&
            main_target != "contact_deskalerts.asp") {
            $('#helpIframe').attr('src', 'help.aspx?about=' + about);
        }
    }

    function showLoader() {
        $("#loader").fadeIn(500);
        $("#shadow").fadeIn(500);
    }

    function hideLoader() {
        $("#shadow").stop();
        $("#shadow").fadeOut(500);
        $("#loader").stop();
        $("#loader").fadeOut(500);
    }

    function showLoaderByClick(e) {
        if (!e.shiftKey) {
            showLoader();
        }
    }

    function bindEvents(doc) {
        var $elements = $("*[target='work_area']", doc);
        $elements.unbind('click', showLoaderByClick)
        $elements.bind('click', showLoaderByClick);
    }

    $(document).keyup(function (event) {
        if (event.keyCode == 27) hideLoader();
    });

	$(function() {        
	    var DAcookie = $.cookies.get('deskalerts');
	    if (DAcookie) {
	        cookieObj = DAcookie;
	    }
	    if (cookieObj.help == undefined) {
	        cookieObj.help = true;
	    }

	    $("#NotApprovedAlertsDialog")
	        .dialog(
	        {
	            autoOpen: false,
	            height: 180,
	            width: 450,
	            modal: true
	        });

	    $("#TrialAddonsDialog").dialog({
	        autoOpen: false,
	        height: 180,
	        width: 450,
	        modal: true,
	        draggable: false,
	        resizable: false,
	        close: function () {
	            if ($("#dontshow").is(':checked')) {
	                dontShow = $("#dontShowVal").val();
	                cookieObj.dontShow = dontShow;
	                var cookieStr = JSON.stringify(cookieObj);
	                thisIsntWorkingWithoutMe.set('deskalerts', cookieStr);
	            }
	        }
	    });
	    $("#getQuoteFromDialog").button().click(function () {
	        $("#trial_dialog").dialog("close");
	    });
        if ("<% =Config.Data.IsDemo%>" == "True") {
            $("#quick_tour").load('quick_tour.html').dialog({
                modal: true,
                width: 630,
                height: 470,
                autoOpen: false,
                resizable: false,
                close: function () {
                    if ($("#dontshowTour").is(':checked')) {
                        dontShowTour = 1;
                        cookieObj.dontShowTour = dontShowTour;
                        var cookieStr = JSON.stringify(cookieObj);
                        thisIsntWorkingWithoutMe.set('deskalerts', cookieStr);
                    }
                }
            }); 
        }
		
		$("#trial_dialog").dialog({
		modal:true,
		width: 630,
        height: 220,
        autoOpen: false,
		resizable: false,
		open: function(){
			$(".hideMe").hide();
		},
		close: function() {
			if ($("#dontshowToday").is(':checked')){
				dontShowToday = <%=ellapse%>;
				cookieObj.dontShowToday = dontShowToday;
				var cookieStr = JSON.stringify(cookieObj);
				thisIsntWorkingWithoutMe.set('deskalerts', cookieStr);
			}
		}	
        });

	    $.ajax({
	        url: 'ShowMaintenanceRem.aspx',
	        success: function(data) 
	        { 
                var maintanance_cookie = $.cookies.get('maintenance_checkbox');

	            if(maintanance_cookie!="1")
	            {
	                if(data<90)
	                {
	                    $("#maintenance_reminder").dialog("open");
	                }
	                else if(data<0)
	                {
	                    $("#maintenance_reminder").dialog("open");
	                }
	            }
	        }
	    });

	    $("#maintenance_reminder")
	        .load('reminder.aspx?action=alert')
	        .dialog({
	            autoOpen: false,
                height: 450,
                width: 800,
	            draggable: false,
	            resizable: false,
	            modal: true,
	            beforeClose: function(event, ui) {
	                if ($("#maintenance_checkbox").is(":checked")) {
	                    $.ajax({
	                        url: 'ShowMaintenanceRem.aspx',
	                        success: function(data) {
	                            if (data >= 61 && data <= 90) {
	                                result = data - 61;
	                            } else if (data >= 31 && data <= 60) {
	                                result = data - 31;
	                            } else if (data <= 30) {
	                                result = 7;
	                            }
	                            var today = new Date();
	                            var period = new Date();
	                            period.setDate(today.getDate() + result);
	                            document.cookie = "maintenance_checkbox=1;expires=" + period;
	                        }
	                    });

	                }
	                /*$(this).dialog("maintenance_reminder").effect("fade", {
                    
                        to: "#maintenance_reminder_button", 
                        className: "ui-effects-transfer"
                    
                    }, 500);*/
	                /*var $jParent=window.parent.top.jQuery.noConflict();
                    var dlg1 = $jParent('#maintenance_reminder_button');
                    alert(dlg1);*/

	                //$jParent('#maintenance_reminder_button')

	            },
	            close:
	            {
	                effect: "fade",
	                duration: 1000

	                /*function() { 
                            var $jParent = window.parent.parent.top.jQuery.noConflict();
                            var dlg1 = $jParent('#maintenance_reminder_button');
                            dlg1.fadeIn(1000);*/

	                //dlg1.button( "option", "label", "123123123" );
	                //dlg1.css('color','green');
	                //var $jParent = window.parent.top.jQuery.noConflict();
	                //var dlg1 = $jParent('#maintenance_reminder_button');
	                //dlg1.fadeIn(100).fadeOut(100).fadeIn(100).fadeOut(100);
	                //$jParent('#maintenance_reminder_button')

	            },
	            show:
	            {
	                effect: "fade",
	                duration: 300
	            }
	        });


	    $('#helpCaption').button({ icons: { secondary: "ui-icon-triangle-1-e"} });
	    $.grep($(window.top.document).find("frame"), function(n, i) {
	        if ($(n).attr("src") == "menu.aspx") {
	            var doc = $(n)[0].contentDocument || $(n)[0].contentWindow.document;

	            if (doc && doc.location != "about:blank" && doc.readyState == 'complete') {
	                bindEvents(doc);
	            }
	            else {
	                $(n).bind("load", function() {
	                    var doc = $(n)[0].contentDocument || $(n)[0].contentWindow.document;
	                    bindEvents(doc);
	                });
	            }
	        }
	    });

	    var bindContentEvents = function() {
            var bindContentEventsToIframe = function (frame) {
                var body = $(document.body);
                var height = body.height();
                window.parent.onChangeBodyHeight(height, true);
	            var doc;
	            try {
	                doc = frame.contentWindow.contentDocument;
	            } catch (e) { }
	            if (doc && doc.location) {
	                var loc = (doc.location.href + '').split('#')[0];
	                $('a', doc).click(function(e) {
	                    if (this.href &&
							this.href.indexOf('#') != 0 &&
							this.href.indexOf(loc + '#') != 0 &&
							this.href.indexOf('javascript:') == -1 &&
							this.href.indexOf('mailto:') == -1 &&
							!this.onclick &&
							(!this.target || this.target == '_self') &&
							!e.shiftKey) {
	                        showLoader();
	                    }
	                });
	                $('form', doc).each(function(i, el) {
	                    el.old_submit = el.submit;
	                    el.submit = function() {
	                        showLoader();
	                        el.old_submit();
	                    }
	                });
	                $("input[type='submit']", doc).each(function(i, el) {
	                    el.old_onclick = el.onclick;
	                    if (el.old_onclick) {
	                        el.onclick = function() {
	                            if (el.old_onclick()) {
	                                showLoader();
	                                return true;
	                            }
	                            return false;
	                        }
	                    }
	                });
	                $('iframe', doc).each(function() {
	                    bindContentEventsToIframe(this);
	                });
                    $('iframe', doc).load(function () {
	                    bindContentEventsToIframe(this);
	                    hideLoader();
	                });
	            }
	        };
	        bindContentEventsToIframe($("#main_frame")[0]);
	    }
	    var doc = $("#main_frame").contents()[0];
        if (doc && doc.location != "about:blank" && doc.readyState == 'complete') {
	        initHelp();
	        bindContentEvents();
	        hideLoader();
	    }
        $("#main_frame").bind("load", function () {
            window.parent.scrollTo(0);
	        initHelp();
	        bindContentEvents();
	        hideLoader();
	    });

	    $('#helpCaption').click(minimizeHelp);

	    topFrame = top.document.getElementById("top");

	    $(topFrame).load(function() {
	        inviteButton = topFrame.contentWindow.document.getElementById("invite");

	        $(inviteButton).click( function()
	        {
	            $("#invite_dialog_form").dialog(
                {
                    autoOpen: false,
                    height: 600,
                    height: 600,
                    width: 600,
                    modal: true,
                    resizable: false
			
                });
					

			
	            $("#invite_dialog_form").dialog("open");
	        });
	        helpOpener = topFrame.contentWindow.document.getElementById("helpOpener");


	        $(helpOpener).click(toggleHelp);

	    });
	    if (topFrame && topFrame.contentWindow.document.readyState == 'complete') {
	        setTimeout(function() {
	            helpOpener = topFrame.contentWindow.document.getElementById("helpOpener");
	            $(helpOpener).click(toggleHelp);
	            /*if (helpOpenerIsHidden)
					$(helpOpener).hide();
				else
					$(helpOpener).show();*/
	        }, 100);
	    }

	    <% if (notApporvedAlertsCount > 0)
           { %>

 		  var approveDialog =   $( "#NotApprovedAlertsDialog" ).dialog(
            {
                autoOpen: false,
		        height: 180,
		        width: 450,
		        modal: true           
            }).dialog( "open" );

	    	$("#notApprovedButton").button().click(function(){

		        window.top.frames["main"].frames["work_area"].location.href = "ApprovePanel.aspx" ;
		       approveDialog.dialog("close");
		        
		
		    });           

	    <% } %>

	    <% if (rejectedAlertsCount > 0)
           { %>

	    var rejectedDialog = $("#RejectedAlertsDialog")
	        .dialog(
	        {
	            autoOpen: false,
	            height: 180,
	            width: 450,
	            modal: true
	        })
	        .dialog("open");
	    $("#rejectedAlertsButton")
	        .button()
	        .click(function() {


	            window.top.frames["main"].frames["work_area"].location.href = "RejectedAlerts.aspx";
	            rejectedDialog.dialog("close");


	        });

	    <% } %>

	    <% if (Config.Data.IsDemo)
	       { %>
	        if(cookieObj.dontShowTour==undefined){
	            cookieObj.dontShowTour=0;
	        }
	        if (cookieObj.dontShowTour!=1){
	            $("#quick_tour").dialog("open");
	        }

	    <% }%>

	    <% if (trial > 0)
	       { %>

            if(cookieObj.dontShowToday==undefined){
				cookieObj.dontShowToday=-1;
			}
		    if (cookieObj.dontShowToday!=<%=ellapse%>) {
		        $("#trial_dialog").dialog("open");
		    }
	    <% } %>
});
</script>
</head>
<body class="full_height">
    <form id="form1" runat="server" style="width: 100%;height: 100%">
    <div style="width: 100%;height: 100%">
        <table border="0" width="100%" height="100%" style="overflow: hidden">
	        <tr style="height:100%">
		        <td style="width:100%;height:100%;padding:0px">
			        <iframe id="main_frame" frameborder="0" src="<%= Request["url"] %>" name="work_area" style="width:100%; height:100%"></iframe>
		        </td>
		        <td id="help_frame" style="padding:0px">
			        <div id="helpDiv" class="helpDiv" style="display:none">
				        <iframe id="helpIframe" name="helpIframe" class="helpIframe" src="help.aspx" frameborder="0">
				        </iframe>
			        </div>
		        </td>
	        </tr>
	        </table>
	    <div id="quick_tour" style="z-index:999;display:none">
	    </div>	
	    <div id="maintenance_reminder" style='z-index:999;display:none;' title='Maintenance Expiration Reminder'>			
	    </div>        
	<div id="trial_dialog" style="z-index:999;display:none">
		<div style="text-align:center">
		<%
		    if (trial >= ellapse)
		    {
		%>
			<p>You're using trial version of DeskAlerts. Trial period will expire in <span style="font-weight:bold;color:red"><%= trial - ellapse %></span> days.</p>
			<p>See full list of features and usage instructions in DeskAlerts manuals:</p>
			<p>
				<a class="hideMe" href="#">:)</a> <!--jquery UI autofocus hack-->
				<a href="http://www.alert-software.com/files/DeskAlerts_Administrators_Manual.pdf" target="_blank"> Admin manual</a> 
				<a href="http://www.alert-software.com/files/DeskAlerts_Editors_Manual.pdf" target="_blank"> Editor manual</a>
				<a href="http://www.alert-software.com/files/DeskAlerts_Client_User_Manual.pdf" target="_blank"> Client manual</a>
			</p>
		<%
		    }
		    else
		    {
		%>
			<a class="hideMe" href="#">:)</a> <!--jquery UI autofocus hack-->
			<p>Your trial period has expired. Please contact <a href="mailto:sales@deskalerts.com?Subject=Trial has expired">sales@deskalerts.com</a> about your trial testing results, or use the button below to compose a quote request.</p>
		<%
		    }
		%>
		</div>
		<div style="text-align:center">
			<a href="request_quote.asp" class="contact_us_button" target="work_area" id="getQuoteFromDialog">Get Free Quote</a>
		</div>
		<form action="#" id="dialog_form">
			<input style="position:absolute;left:10px;bottom:20px;" type="checkbox" name="dontshowToday" id="dontshowToday" value="1"/>
			<label for="dontshowToday" style="position:absolute;bottom:0px;padding: 20px 0px 20px 20px;">Do not show again today</label>
		</form>
        
        

	  </div>
        <div id="shadow" style="position:absolute; width:100%; height:100%; background: grey; opacity:0.4; filter:alpha(opacity=40);">
	    </div>
	    <div id="loader" style="font-size:1px; background: url(images/loader.gif); height:15px; width:128px; position:absolute; left:50%; margin-left:-64px; top:50%; margin-top:-7px; z-index:998">
	    </div>
    </div>
    </form>
</body>
</html>
