﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Parameters;

namespace DeskAlertsDotNet.Pages
{
    using Resources;

    public partial class ViewIpGroup : DeskAlertsBaseListPage
    {

        protected override void OnLoad(EventArgs e)
        {

            if (string.IsNullOrEmpty(Request["id"]))
            {
                Response.StatusCode = 404;
                Response.End();
                return;
            }



            base.OnLoad(e);
            typeCell.Text = resources.LNG_TYPE;
            ipRangesCell.Text = resources.LNG_VALUE;

            groupName.Text = dbMgr.GetScalarQuery<string>(string.Format("select name from ip_groups WHERE id={0}", Request["id"]));

            Table = contentTable;
            int cnt = this.Content.Count < (Offset + Limit) ? this.Content.Count : Offset + Limit;

            for (int i = Offset; i < cnt; i++)
            {
                DataRow row = this.Content.GetRow(i);

                string fromIp = row.GetString("from_ip");
                string toIp = row.GetString("to_ip");

                TableRow tableRow = new TableRow();
                if (fromIp == toIp)
                {
                    TableCell type = new TableCell()
                    {
                        Text = "IP address"
                    };

                    TableCell value = new TableCell()
                    {
                        Text =  fromIp
                    };

                    tableRow.Cells.Add(type);
                    tableRow.Cells.Add(value);
                }
                else
                {
                    TableCell type = new TableCell()
                    {
                        Text = "IP range"
                    };

                    TableCell value = new TableCell()
                    {
                        Text = fromIp + " - " + toIp
                    };

                    tableRow.Cells.Add(type);
                    tableRow.Cells.Add(value);
                    
                }

                contentTable.Rows.Add(tableRow);
            }
        }

        #region DataProperties
        protected override string DataTable
        {
            get
            {

                //return your table name from this property
                return "ip_range_groups";
            }
        }

        protected override string[] Collumns
        {
            get
            {

                //return array of collumn`s names of table from data table
                return new[] { "from_ip", "to_ip"};

            }
        }


        protected override string ConditionSqlString
        {
            get
            {
                if (!string.IsNullOrEmpty(Request["id"]))
                    return "group_id=" + Request["id"];

                return string.Empty;
            }
        }

        protected override string PageParams
        {
            get
            {
                string parameters = "";

                if (!string.IsNullOrEmpty(Request["id"]))
                    parameters += "id=" + Request["id"] + "&";

                return parameters;
            }
            
        }

        protected override string DefaultOrderCollumn
        {
            //return collumn for default order
            get { return "id"; }
        }

        protected override HtmlGenericControl TopPaging
        {
            get { return topPaging; }
        }


        protected override HtmlGenericControl BottomPaging
        {
            get { return bottomPaging; }
        }

        /*
         * 
         * If you want to add condition for data selection override property ConditionSqlString 
         * Also you can implement your custom query by overriding properties CustomQuery and CustomCountQuery
         *  
         */
        #endregion

        #region Interface properties

        protected override IButtonControl[] DeleteButtons
        {
            get
            {
                return new IButtonControl[] { };
            }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl NoRowsDiv
        {
            get { return NoRows; }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl TableDiv
        {
            get { return mainTableDiv; }
        }

        protected override string ContentName
        {
            get
            {
                //return content name here
                return "IP Groups";
            }
        }

        #endregion
    }
}