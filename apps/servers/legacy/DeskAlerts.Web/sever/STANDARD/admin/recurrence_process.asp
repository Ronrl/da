<%

function recurrenceProcess(alert_id,countdown_array)
	'--------------------
	'alert time
	'startTime = ""
	'endTime = ""

	'pattern
	patternType = ""
	numberDays = 0
	numberWeeks = 0
	weekDays = ""
	monthDay = 0
	numberMonths = 0
	weekdayPlace = 0
	weekdayDay = 0
	monthVal = 0
	dayly_selector = ""
	monthly_selector = ""
	yearly_selector = ""
	'range
	endDateType = ""
	intAlertOccurence = 0

	'--------------------

	'--------
	'alert time in alerts table
	'StartTime = Request("start_hour") & ":" & Request("start_min") & " " & Request("start_half")
	'EndTime = Request("end_hour") & ":" & Request("end_min") & " " & Request("end_half")
	'--------

	'pattern
	patternType = Request("pattern")

	dayly_selector =Request("dayly_selector")
	if(dayly_selector="dayly_1") then
		numberDays = Clng(Request("number_days"))
	else
		numberDays = 1
	end if

	numberWeeks = Clng (Request("number_weeks"))
	weekDays = Request("weekday_days")
	monthDay = Clng(Request("month_day"))

	monthly_selector=Request("monthly_selector")
	if(monthly_selector="monthly_1") then
		numberMonths = Clng(Request("number_month_1"))
	else
		numberMonths = Clng(Request("number_month_2"))
	end if

	weekdayPlace = Clng(Request("weekday_place"))
	weekdayDay = Clng(Request("weekday_day"))

	yearly_selector = Request("yearly_selector")
	if(yearly_selector="yearly_1") then
		monthVal = Clng(Request("month_val_1"))
	else
		monthVal = Clng(Request("month_val_2"))
	end if

	'range
	'--------
	'from_date=GetDateFromString(Request("start_date"), Date)
	'to_date=GetDateFromString(Request("end_by"), "")
	'--------
	endDateType = Request("end_type")

	if(endDateType="end_after") then
		intAlertOccurence = Clng(Request("end_after"))
	end if

	SQL = "DELETE FROM recurrence WHERE alert_id=" & Replace(alert_id, ",", " OR alert_id = ") &" AND pattern='c'"
	Conn.Execute(SQL)

    
    alert_ids = Split(alert_id, ",")
    
   for each aid in alert_ids 
    SQL = "SELECT id FROM recurrence WHERE alert_id=" & aid
	Set rs = Conn.Execute(SQL)
	if (Not rs.EOF) then 
		recur_id = rs("id")
		'update
		SQL = "UPDATE recurrence SET pattern = '" & patternType & "', number_days = "&numberDays&", number_week = "&numberWeeks&", week_days = "&weekDaysToBits(weekDays)&"," _
			&"month_day = "&monthDay&", number_month = "&numberMonths&", weekday_place = "&weekdayPlace&", weekday_day = "&weekdayDay&", month_val = "&monthVal&", " _
			&"dayly_selector = '"&dayly_selector&"', monthly_selector = '"&monthly_selector&"', yearly_selector = '"&yearly_selector&"', end_type = '"&endDateType&"'," _
			&"occurences = "&intAlertOccurence&" WHERE alert_id=" & aid
		rs.Close
	else
		'insert
		SQL = "INSERT INTO recurrence (alert_id, pattern, number_days, number_week, week_days, month_day, number_month, weekday_place, weekday_day, month_val," _
			& "dayly_selector, monthly_selector, yearly_selector, end_type, occurences) VALUES ("&aid&", '"&patternType&"', "&numberDays&", "&numberWeeks&", "  _
			&weekDaysToBits(weekDays)&", "&monthDay&", "&numberMonths&", "&weekdayPlace&", "&weekdayDay&", "&monthVal&", '"&dayly_selector&"', '"&monthly_selector&"', '" _
			&yearly_selector&"', '"&endDateType&"', "&intAlertOccurence&")"
	end if
	Conn.Execute(SQL)
    

	if (VarType(countdown_array)>1 ) then
		patternType = "c"
		numberWeeks = 5
		i=0
		SQL=""
		For Each numberDays In countdown_array
			SQL = SQL & "INSERT INTO recurrence (alert_id, pattern, number_days, number_week ) VALUES ("&aid&", '"&patternType&"', "&CLng(numberDays)&", "&numberWeeks&"); "
		Next
		Conn.Execute(SQL)
	end if
	next
end function

function recurrenceLoadData(alert_id)
	if(alert_id<>"") then

		SQL = "SELECT pattern, dayly_selector, monthly_selector, yearly_selector, number_days, number_week, week_days, month_day, number_month, weekday_place, weekday_day, month_val, end_type, occurences  FROM recurrence WHERE alert_id=" & Replace(alert_id, ",", " OR alert_id = ")
		'Response.Write SQL
		Set rs = Conn.Execute(SQL)
		if (Not rs.EOF) then 
			'get data fro db
			pattern = rs("pattern")
			if(rs("dayly_selector")<>"") then
				dayly_selector = rs("dayly_selector")
			end if
			if(rs("monthly_selector")<>"") then
				monthly_selector = rs("monthly_selector")
			end if
			if(rs("yearly_selector")<>"") then
				yearly_selector = rs("yearly_selector")
			end if
			if(rs("number_days")<>0) then
				number_days = rs("number_days")
			end if
			if(rs("number_week")<>0) then
				number_weeks = rs("number_week")
			end if
			if(rs("week_days")<>0) then
				week_days = rs("week_days")
			end if
			if(rs("month_day")<>0) then
				month_day = rs("month_day")
			end if
			if(rs("number_month")<>0) then
				number_month = rs("number_month")
			end if
			weekday_place = rs("weekday_place")
			if(rs("weekday_day")<>0) then
				weekday_day = rs("weekday_day")
			end if
			if(rs("month_val")<>0) then
				month_val = rs("month_val")
			end if
	        	if(rs("end_type")<>"") then
	        		end_type = rs("end_type")
			end if
			if(rs("occurences")<>0) then
				occurences = rs("occurences")
			end if
		end if
	end if
end function
%>