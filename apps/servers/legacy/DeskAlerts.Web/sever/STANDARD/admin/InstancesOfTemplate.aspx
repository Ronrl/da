﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InstancesOfTemplate.aspx.cs" Inherits="DeskAlertsDotNet.Pages.InstancesOfTemplate" %>
<%@ Import Namespace="Resources" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <title>DeskAlerts</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <link href="css/style9.css" rel="stylesheet" type="text/css">
    <link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="css/jquery/jquery_timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css">
    <link href="css/toastr.min.css" rel="stylesheet" type="text/css">

    <script type="text/javascript" src="functions.js"></script>
    <script type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
    <script type="text/javascript" src="jscripts/shortcut.js"></script>
    <script type="text/javascript" src="jscripts/date.js"></script>
    <script type="text/javascript" src="jscripts/jquery/jquery_timepicker/jquery-ui-timepicker-addon.js"></script>
    <script type="text/javascript" src="jscripts/jquery/datepicker_fix.js"></script>
    <script type="text/javascript" src="jscripts/preview.js?v=9.2.1.31"></script>
    <script type="text/javascript" src="jscripts/toastr.min.js"></script>
    <script type="text/javascript" src="functions.js"></script>

    <script language="javascript" type="text/javascript">
        function confirmDelete() {
            if (confirm("Are you sure you want to delete selected items?") == true) {
                return true;
            } else {
                return false;
            }
        }

        $(document).ready(function () {
            $(".delete_button").button({});

            shortcut.add("delete", function (e) {
                e.preventDefault();

                if ($("#upDelete").length == 0)
                    return;


                if (confirmDelete())
                    __doPostBack('upDelete', '');
            });

            $("#selectAll").click(function () {
                var checked = $(this).prop('checked');
                $(".content_object").prop('checked', checked);
            });

            $(document).tooltip({
                items: "img[title],a[title]",
                position: {
                    my: "center bottom-20",
                    at: "center top",
                    using: function (position, feedback) {
                        $(this).css(position);
                        $("<div>")
                            .addClass("arrow")
                            .addClass(feedback.vertical)
                            .addClass(feedback.horizontal)
                            .appendTo(this);
                    }
                }
            });

            $(function () {
                $(".delete_button").button({
                });

                $(".add_alert_button").button({
                    icons: {
                        primary: "ui-icon-plusthick"
                    }
                });
            });
        });

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
            <tr>
                <td>
                    <table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
                        <tr>
                            <td width="100%" height="31" class="main_table_title">
                                <img src="images/menu_icons/alert_20.png" alt="" width="20" height="20" border="0" style="padding-left: 7px">
                                <asp:LinkButton href="PopupSent.aspx" runat="server" ID="headerTitle" class="header_title" Style="position: absolute; top: 22px" />
                            </td>
                        </tr>

                        <tr>
                            <td class="main_table_body" height="100%">
                                <br />
                                <div style="margin-left: 10px">
                                    <span class="work_header" runat="server" id="workHeader"></span>
                                </div>
                                <table style="padding-left: 10px;" width="100%" border="0">
                                    <tbody>
                                        <tr valign="middle">
                                            <td width="175">
                                                <% = resources.LNG_SEARCH_ALERT_BY_TITLE%>
                                                <input type="text" id="searchTermBox" runat="server" name="uname" value="" />
                                            </td>
                                            <td width="1%">
                                                <br />
                                                <asp:LinkButton runat="server" class="search_button ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" ID="searchButton" Style="margin-right: 0px" role="button" aria-disabled="false" Text="<span class='ui-button-icon-primary ui-icon ui-icon-search'></span><span class='ui-button-text'>{Search}</span>" />
                                                <input type="hidden" name="search" value="1">
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>

                                <div style="margin-left: 10px" runat="server" id="mainTableDiv">

                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td>
                                                <div style="margin-left: 10px" id="topPaging" runat="server"></div>
                                            </td>
                                        </tr>
                                    </table>

                                    <br>
                                    <asp:LinkButton Style="margin-left: 10px" runat="server" ID="upDelete" class='delete_button' OnClientClick="return confirmDelete(); " title="Hotkey: Delete" />
                                    <br>
                                    <br>

                                    <asp:Table Style="padding-left: 0px; margin-left: 10px; margin-right: 10px;" runat="server" ID="contentTable" Width="98.5%" Height="100%" CssClass="data_table" CellSpacing="0" CellPadding="3">
                                        <asp:TableRow CssClass="data_table_title">
                                            <asp:TableCell runat="server" ID="headerSelectAll" CssClass="table_title" Width="2%">
                                                <asp:CheckBox ID="selectAll" runat="server" />
                                            </asp:TableCell>
                                            <asp:TableCell runat="server" ID="titleCell" CssClass="table_title"></asp:TableCell>
                                            <asp:TableCell runat="server" ID="creationDateCell" CssClass="table_title" Width="20%"></asp:TableCell>
                                            <asp:TableCell runat="server" ID="senderCell" CssClass="table_title"></asp:TableCell>
                                            <asp:TableCell runat="server" ID="actionsCell" CssClass="table_title" Width="200px"></asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>

                                    <br>
                                    <asp:LinkButton Style="margin-left: 10px" runat="server" ID="bottomDelete" class='delete_button' OnClientClick="return confirmDelete();" title="Hotkey: Delete" />
                                    <br>
                                    <br>

                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td>
                                                <div style="margin-left: 10px" id="bottomRanging" runat="server"></div>
                                            </td>
                                        </tr>
                                    </table>

                                </div>

                                <div runat="server" id="NoRows">
                                    <br />
                                    <br />
                                    <center><b><%=resources.LNG_THERE_ARE_NO_ALERTS%></b></center>
                                </div>

                                <br>
                                <br>
                            </td>
                        </tr>

                    </table>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
