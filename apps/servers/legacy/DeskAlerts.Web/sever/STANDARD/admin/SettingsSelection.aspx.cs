﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Parameters;


namespace DeskAlertsDotNet.Pages
{
    public partial class SettingsSelection : DeskAlertsBasePage
    {
        //use curUser to access to current user of DA control panel      
        //use dbMgr to access to database

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if(!curUser.HasPrivilege)
               Response.Redirect("ProfileSettings.aspx", true);

            //if (!Config.Data.HasModule(Modules.TWITTER) && !Config.Data.HasModule(Modules.BLOG) &&
            //    !Config.Data.HasModule(Modules.LINKEDIN))
            //    socialSettings.Visible = false;


            //Add your main code here
        }
    }
}