﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using DeskAlertsDotNet.DataBase;

namespace DeskAlertsDotNet
{
    public static class ReccurenceManager
    {
        public static string GetValueSafety(this Dictionary<string, object> dictionary, string key)
        {
            if (!dictionary.ContainsKey(key))
                return string.Empty;

            return dictionary[key].ToString();
        }


        public static void ProcessReccurence(DBManager dbMgr,
            Dictionary<string, object> recurDictionary)
        {

            string pattern = recurDictionary.GetValueSafety("pattern");

            if (pattern.Equals("d"))
            {
                recurDictionary["monthly_selector"] = "";
                recurDictionary["yearly_selector"] = "";
            }
            else if (pattern.Equals("m"))
            {
                recurDictionary["dayly_selector"] = "";
                recurDictionary["yearly_selector"] = "";             
            }
            else if (pattern.Equals("y"))
            {
                recurDictionary["dayly_selector"] = "";
                recurDictionary["monthly_selector"] = "";
            }

            List<string> countdowns = recurDictionary["countdowns"] as List<string>;



            recurDictionary.Remove("countdowns");

            string numberMonth = recurDictionary.GetValueSafety("number_month_1");

            string monthVal = recurDictionary.GetValueSafety("yearly_selector").Equals("yearly_1")
                ? recurDictionary.GetValueSafety("month_val_1")
                : recurDictionary.GetValueSafety("month_val_2");

            recurDictionary.Add("number_month", numberMonth);
            recurDictionary.Remove("number_month_1");
            recurDictionary.Remove("number_month_2");


            recurDictionary.Add("month_val", monthVal);
            recurDictionary.Remove("month_val_1");
            recurDictionary.Remove("month_val_2");

            string monthDay = recurDictionary.GetValueSafety(pattern + "_month_day");
            recurDictionary.Add("month_day", monthDay);
            recurDictionary.Remove("y_month_day");
            recurDictionary.Remove("m_month_day");

            string weekDayPlace = recurDictionary.GetValueSafety(pattern + "_weekday_place");
            recurDictionary.Add("weekday_place", weekDayPlace);
            recurDictionary.Remove("y_weekday_place");
            recurDictionary.Remove("m_weekday_place");

            string weekDayday = recurDictionary.GetValueSafety(pattern + "_weekday_day");
            recurDictionary.Add("weekday_day", weekDayday);
            recurDictionary.Remove("y_weekday_day");
            recurDictionary.Remove("m_weekday_day");



            DataSet recurSet = dbMgr.GetDataByQuery("SELECT occurences FROM recurrence WHERE pattern != 'c' AND  alert_id = " + recurDictionary.GetValueSafety("alert_id"));

            string sqlQuery = "";

            if (recurDictionary.GetValueSafety("end_type").Equals("end_after"))
            {
               string occurs =  recurDictionary.GetValueSafety("end_after");
                
               recurDictionary.Add("occurences", occurs);
            }
            else
            {
                int occurs = recurSet.IsEmpty ? 1 : recurSet.GetInt(0,"occurences") ;
                recurDictionary.Add("occurences", occurs);
            }
            recurDictionary.Remove("end_after");

            object[] values = recurDictionary.Values.ToArray();

            for (int i = 0; i < values.Length; i++)
            {
                object value = values[i];
                if (value == null)
                {
                    values[i] = "NULL";
                }
                else if (value is string)
                {
                    string stringValue = value.ToString();

                    stringValue = stringValue.Replace("'", "''");

                    DateTime outTime = DateTime.MinValue;

                    if (DateTime.TryParse(stringValue, out outTime))
                    {
                        stringValue = string.Format("'{0}'", stringValue);
                    }
                    else
                    {
                        stringValue = string.Format("N'{0}'", stringValue);
                    }

                    values[i] = stringValue;
                }
            }

            if (recurSet.IsEmpty)
            {

                sqlQuery = "INSERT INTO recurrence (";
                sqlQuery += string.Join(",", recurDictionary.Keys.ToArray());

                sqlQuery += ") VALUES (";

                sqlQuery += string.Join(",", values);
                sqlQuery += ")";
            }
            else
            {
               
                sqlQuery += "UPDATE recurrence SET ";

                for (int i = 0; i < recurDictionary.Count; i++)
                {
                    string key = recurDictionary.Keys.ToArray()[i];

                    sqlQuery += (key + "=" + values[i] );

                    if (i != recurDictionary.Count - 1)
                        sqlQuery += ",";


                }
                sqlQuery += " WHERE  pattern != 'c' AND alert_id = " + recurDictionary.GetValueSafety("alert_id");
            }
            dbMgr.ExecuteQuery(sqlQuery);

            string alertId = recurDictionary.GetValueSafety("alert_id");
            if (countdowns.Count > 0)
            {


                dbMgr.ExecuteQuery("DELETE FROM recurrence WHERE pattern = 'c' AND  alert_id = " + alertId);
                foreach (string countdown in countdowns)
                {
                    dbMgr.ExecuteQuery(
                        string.Format(
                            "INSERT INTO recurrence (alert_id, pattern, number_days, number_week ) VALUES ({0}, 'c', {1}, 5)",
                            recurDictionary.GetValueSafety("alert_id"), countdown));
                }

                //   return;

            }
            else
            {
                dbMgr.ExecuteQuery("DELETE FROM recurrence WHERE pattern = 'c' AND  alert_id = " + alertId);
            }


            



        }

        public static void DeleteReccurence(int contentId)
        {
            using (var dbManager = new DBManager())
            {
                dbManager.ExecuteQuery($"DELETE FROM recurrence WHERE alert_id = {contentId}");
            }
        }
    }
}