﻿<%
	Session("uid") = ""
	Session("db_use_backup") = 0
	if Session("auth_type") <> "" then
%>
<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<title>DeskAlerts</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link href="css/style9.css" rel="stylesheet" type="text/css">
	<link href="images/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
</head>
<body class="login_body">
	<table height="100%" border="0" align="center" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<div align="center"><img src="images/langs/<%=default_lng %>/top_logo.gif" alt="DeskAlerts Control Panel" border="0"></div>
			<br/><br/>
			<table cellspacing="0" cellpadding="0" align="center" class="main_table login_table">
			<tr>
				<td width="100%" height="31" class="theme-colored-back"><div class="header_title"><%=LNG_LOGOUT %></div></td>
			</tr>
			<tr>
				<td bgcolor="#ffffff">
					<div style="margin: 20px 10px 9px 10px;text-align:center">
						<div><%= LNG_TO_LOGOUT_PLEASE_CLOSE_YOUR_BROWSER %></div>
						<div style="width:250px"></div>
					</div>
				</td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
</body>
</html>
<!-- #include file="db_conn_close.asp" -->
<%
	else
		Response.Redirect "index.asp"
	end if
%>
