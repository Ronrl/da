﻿using System;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Configuration;
using System.Web;
using DeskAlertsDotNet.Parameters;

namespace DeskAlertsDotNet.DataBase
{
    
    public class DataRow
    {
        private Dictionary<string, object> row;

        public DataRow(Dictionary<string, object> dict)
        {
            row = dict;
        }
        public object GetValue(string field)
        {
                return row[field];

        }

        public object this[string key]
        {
            get { return this.GetValue(key); }
        }
        public int GetInt(string field)
        {
            //try
          //  {
                return Convert.ToInt32(GetValue(field));
            //}
            //catch(Exception e)
            //{
                
            //}
        }

        public string GetString(string field)
        {
            return Convert.ToString(GetValue(field));
        }

        public bool GetBool(string field)
        {
           return  Convert.ToBoolean(GetValue(field));
        }

        
    }
   public class DataSet
   {

       private List<DataRow> collection;

       public DataSet(List<DataRow> list)
        {
            collection = list;
        }

       public bool IsEmpty { get { return collection.Count == 0; } }
       public object GetValue(int row, string field)
       {
           DataRow rowDict = collection[row] as  DataRow;

           object val = rowDict[field];

           if (val.GetType() == typeof(DBNull))
               return null;
           return val;

       }

       public string GetString(int row, string field)
       {
            return Convert.ToString(GetValue(row, field));
       }

       public int GetInt(int row, string field)
       {
           return Convert.ToInt32(GetValue(row, field));
       }

       public bool GetBool(int row, string field)
       {
           return Convert.ToBoolean(GetValue(row, field));
       }

       protected DataRow GetRow(int index)
       {
           return collection[index];
       }

       public List<T> GetCollumnValues<T>(string collumn, bool distinct = false)
       {
           List<T> values = new List<T>();
           foreach(DataRow row in collection)
           {
               values.Add(((T)row.GetValue(collumn)));
           }

           if (distinct)
               values = values.Distinct().ToList();

           return values;
       }

       public List<DataRow> ToList()
       {
           return this.collection;
       }

       public DataRow[] ToArray()
       {
           return this.collection.ToArray();
       }
       public DataSetEnumerator GetEnumerator()
       {
           return new DataSetEnumerator(this);
       }
       public int Count { get { return collection.Count; } }

       public class DataSetEnumerator
       {
           DataSet collection;
           int index;
           public DataSetEnumerator(DataSet collection)
           {
               this.collection = collection;
               index = -1;
           }

           public bool MoveNext()
           {
               index++;

               return index < collection.Count;
           }

           public DataRow Current
           {
               get
               {
                   return collection.GetRow(index);
               }
           }
       }
   }
   public class DBManager :IDisposable
    {
       private SqlConnectionStringBuilder connStrBuilder;
       private SqlConnection connection;

       //private static DBManager _instance;
       //public static DBManager Manager
       //{
       //    get
       //    {
       //        if (_instance == null || _instance.connection == null || _instance.connection.State == System.Data.ConnectionState.Closed)
       //        {
       //            return _instance = new DBManager();
       //        }

       //        return _instance;
       //    }

       //}

       public void Dispose()
       {
           if (this.connection != null)
               this.connection.Close();
       }

       private string GetConnectionString(string host, string login, string password, string database, bool isWindowsAuth)
       {
           password = Encoding.UTF8.GetString(Convert.FromBase64String(password));
           string connStr = "";
           if (isWindowsAuth)
           {
               connStr = String.Format("Data Source={0};Initial Catalog={1};TrustServerCertificate=True;Integrated Security=SSPI", host, database);
           }
           else
           {
               connStr = String.Format("Data Source={0};Initial Catalog={1};User ID={2};Password={3};TrustServerCertificate=True", host, database, login, password);
           }

           return connStr;
       }

       public DBManager()
       {
           bool isWindowsAuth = Config.Data.GetInt("winauth") == 1;
           string dataSource = Config.Data.GetString("source");
           string dbName = Config.Data.GetString("dbname");
           string user = Config.Data.GetString("user");
           string password = Config.Data.GetString("password");


           string connString = GetConnectionString(dataSource, user, password, dbName, isWindowsAuth);

           if(connString != String.Empty)
           {
               this.connection = new SqlConnection(connString);
               this.connection.Open();
           }
       }

       //public DBManager()
       //{
       //    connStrBuilder = new SqlConnectionStringBuilder();

       //        connStrBuilder.DataSource = Config.Data.GetString("source");

       //        bool isWindowsAuth = Config.Data.GetInt("winauth") == 1;
       //        connStrBuilder.TrustServerCertificate = true;
       //        connStrBuilder.InitialCatalog = Config.Data.GetString("dbname");
       //        if (isWindowsAuth)
       //            connStrBuilder.Authentication = SqlAuthenticationMethod.ActiveDirectoryIntegrated;
       //        else
       //        {
       //            connStrBuilder.Authentication = SqlAuthenticationMethod.SqlPassword;
       //            connStrBuilder.UserID = Config.Data.GetString("user");
       //            connStrBuilder.Password = Config.Data.GetString("password");
       //        }
       //        // connStrBuilder.

       //        string connStr = connStrBuilder.ConnectionString;

       //        this.connection = new SqlConnection(connStr);
               

       //        connection.Open();
           

      // }

       public DataSet GetDataByQuery(string sqlQuery)
       {
           List<DataRow> arrayList = new List<DataRow>();

           using (SqlCommand cmd = new SqlCommand(sqlQuery, this.connection))
           {
              // SqlDataReader reader = null;


               using (var reader = cmd.ExecuteReader())
               {

                   while (reader.Read())
                   {
                       Dictionary<string, object> row = new Dictionary<string, object>();

                       for (int i = 0; i < reader.FieldCount; i++)
                       {
                           row.Add(reader.GetName(i), reader.GetValue(i));
                       }

                       DataRow dataRow = new DataRow(row);
                       arrayList.Add(dataRow);

                   }

               }
           }

           //

           return new DataSet(arrayList);
       }

       public int ExecuteQuery(string sqlQuery)
       {
           using(SqlCommand cmd = new SqlCommand(sqlQuery, this.connection))
           {

               return cmd.ExecuteNonQuery();
           }

       }

       public object GetScalarByQuery(string sqlQuery)
       {
           using (SqlCommand cmd = new SqlCommand(sqlQuery, this.connection))
           {

               return cmd.ExecuteScalar();
           }
       }
    }
}