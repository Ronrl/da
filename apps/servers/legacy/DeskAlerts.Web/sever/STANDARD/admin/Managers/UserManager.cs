﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DeskAlertsDotNet.Models;
using DeskAlertsDotNet.DataBase;

namespace DeskAlertsDotNet
{
    public class UserManager
    {
        private  Dictionary<string, User> usersTable;

        private static UserManager _instance;
        public static UserManager Default
        {
            get
            {
                if (_instance == null)
                    return _instance = new UserManager();

                return _instance;
            }
        }
        private UserManager()
        {
            usersTable = new Dictionary<string, User>();
        }

        public bool IsAuthorized(string sessionId)
        {
            return usersTable.ContainsKey(sessionId);
        }
        private User GetUserBySession(string sessionId)
        {
            if (!usersTable.ContainsKey(sessionId))
                return null;
            
            return usersTable[sessionId];
        }

        public void RegisterUser(string sessionId, User user)
        {
            usersTable.Add(sessionId, user);
        }
        
        public void UnregisterUser(string sessionId)
        {
            usersTable.Remove(sessionId);
        }

        public User CurrentUser
        {
            get
            {
                string sessionId = HttpContext.Current.Session.SessionID;
                return GetUserBySession(sessionId);
            }
        }
        public  User LoadUserFromDB(string name, DBManager dbMgr)
        {
            //  User user = null;

            DataSet policySet = dbMgr.GetDataByQuery("SELECT u.id, p.type, pe.policy_id FROM users as u LEFT JOIN policy_editor as pe ON pe.editor_id = u.id LEFT JOIN policy as p ON p.id = pe.policy_id WHERE u.name = '" + name + "'");

            if (policySet.IsEmpty)
                return null;

            int id = policySet.GetInt(0, "id");

            bool isAdmin = id == 1 || policySet.GetString(0, "type").Equals("A");
            bool isModer = !isAdmin && policySet.GetString(0, "type").Equals("M");

            //string name = policySet.GetString(0, "name");

            

            Policy policy = null;
            if (!isAdmin && !isModer)
            {
                policy = Policy.LoadPolicyFromDBForUser(id, dbMgr);
            }

            return new User(id, name, isAdmin, isModer, policy);
        }
    }
}