﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>DeskAlerts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/style9.css" rel="stylesheet" type="text/css">
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css">
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/date.js"></script>
<script language="javascript" type="text/javascript" src="functions.js"></script>
<script language="javascript" type="text/javascript">
	var settingsDateFormat = "<%=GetDateFormat()%>";
	var settingsTimeFormat = "<%=GetTimeFormat()%>";

	var responseDbFormat = "dd/MM/yyyy HH:mm:ss";
	var uiFormat = getUiDateTimeFormat(settingsDateFormat,settingsTimeFormat);
	$(document).ready(function() {
		var width = $(".data_table").width();
		var pwidth = $(".paginate").width();
		if (width != pwidth) {
			$(".paginate").width("100%");
		}
	});
	$(function(){
		$(document).tooltip({
			items: ".table_param_preview_img",
			content: function() {
				var el = $(this);
				return el.attr("paramHtml");
			}
		});
		
		$(".add_alert_button").button({
			icons: {
                primary: "ui-icon-plusthick"
            }
		});
		
		$(".delete_button").button({
		});
		
	
	});
	
</script>
</head>
<body style="margin:0" class="body">
<script language="JScript" src="jscripts/json2.js" runat="server"></script>
<script language="JScript" runat="server">

	function createTableParam(_values,_headers,rowsCount)
	{
		
		var headers = [];
		var values = [];
		
		if (_headers)
		{
			headers = JSON.parse(_headers);
		}
		if (_values)
		{
			values = JSON.parse(_values);
		}
		
		
		var style = "<style>.table_param {border: 1px solid black; border-collapse:collapse} .table_param_header_cell{background: #cccccc; border: 1px solid black} .table_param_data_cell{border: 1px solid black}</style>"
		var tableHTML = style + "<table class='table_param'><tr>";

		var length =  values.length;
		
		if (rowsCount && rowsCount < length)
		{
			length = rowsCount;
		}
		
		for (var i = 0; i < headers.length; i++)
		{
			tableHTML += "<td class='table_param_header_cell'>"
			tableHTML += headers[i].name;
			tableHTML += "</td>";
		}	
		
		tableHTML += "</tr>";
		
		for (var i = 0; i < length; i++)
		{
			tableHTML += "<tr>";
			for (var j = 0; j < headers.length; j++)
			{
				tableHTML += "<td class='table_param_data_cell'>"
				tableHTML += values[i][j];
				tableHTML += "</td>";
			}
			tableHTML += "</tr>";
		}	
		
		tableHTML += "</table>";
		
		return tableHTML;
	}
	
	function createTableParamPreview(values,headers)
	{
		var tableHTML = createTableParam(values,headers,1);
		
		return tableHTML;
	}
</script>

<%
' alert_parametrized
' Types:
' 0 - text
' 1 - int
' 2 - select
' 3 - 
' 4 -
' 5 -
' 6 - datetime
' 7 -
' 8 -
' 9 -
' 10 -
' 11 - hidden
' 12 - time_in_minutes

' Actions:
' 0 - title
' 1 - urgent
' 2 - acknow
' 3 - autoclose
' 4 - allow_manual
' 5 - schedule
' 6 - schedule_pattern
' 7 - start_date
' 8 - end_date
' 9 - delta_start_date
' 10 - delta_end_date

uid = Session("uid")
templateId = Request("temp_id")
if (uid <>"") then

	if (templateId<>"") then

		if(Request("offset") <> "") then 
			offset = clng(Request("offset"))
		else 
			offset = 0
		end if	

		if(Request("sortby") <> "") then 
			sortby = Request("sortby")
		else 
			sortby = "id DESC"
		end if	

		if(Request("limit") <> "") then 
			limit = clng(Request("limit"))
		else 
			limit=50
		end if

		Set RS_temp = Conn.Execute("SELECT name FROM alert_param_templates WHERE id='"&Replace(templateId,"'","''")&"'")
	end if
%>
<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	<tr>
	<td>
	<table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
		<tr>
			<td height="31" class="main_table_title"><a href="alert_parametrized.asp?temp_id=<%=templateId %>" class="header_title"><%=HtmlEncode(RS_temp("name"))%></a></td>
		</tr>
		<tr>
		<td style="padding:5px; width:100%" height="100%" valign="top" class="main_table_body">
			<div style="padding:10px;">
				<%
					Dim paramName
					
					linkclick_str = LNG_ALERTS
					alerts_arr = Array ("","","","","","","")
						
					Set RS = Conn.Execute("SELECT id FROM users WHERE id="&uid&" AND role='E'")
					if(Not RS.EOF) then
						gid = 1
						Set rs_policy = Conn.Execute("SELECT type FROM policy INNER JOIN policy_editor ON policy.id=policy_editor.policy_id WHERE editor_id=" & uid)
						if(Not rs_policy.EOF) then
							if(rs_policy("type")="A") then
								gid = 0
							end if
						end if
						if(gid=1) then
							editor_id=RS("id")
							policy_ids=""
							policy_ids=editorGetPolicyIds(editor_id)
							alerts_arr = editorGetPolicyList(policy_ids, "alerts_val")
							if(alerts_arr(2)="") then
								linkclick_str = "not"
							end if
						end if
					else
						gid = 0
					end if
					RS.Close
					%>
					<span class="work_header"><%=LNG_SENT_ALERTS %></span>
					
					<% if(Request("sms")="1") then Response.Write "<br><center><b><font color=""red"">Error while sending sms!</font></b></center>" end if %>
					<% if(Request("sms")="2") then Response.Write "<br><center><b><font color=""red"">Error while sending sms (wrong alert text)!</font></b></center>" end if %>
					<table width="100%"><tr><td align="right">
					<% if(gid = 0 OR (gid = 1 AND alerts_arr(0)<>"")) then %>
					<a class="add_alert_button" href="alert_parametrized_edit.asp?temp_id=<%=templateId%>"><%=LNG_ADD_ALERT%></a>
					<% end if%>
					</td></tr></table>
					
					<%
					if (templateId<>"") then

						Set RS_temp = Conn.Execute("SELECT name FROM alert_param_templates WHERE id='"&templateId&"'")
						
						paramName = RS_temp("name")
						
						Set RS_params = Conn.Execute("SELECT p.id, p.name, p.type, s.value FROM alert_parameters p LEFT JOIN alert_param_selects s ON p.id = s.param_id WHERE p.temp_id = '"&templateId&"' AND p.visible=1 AND (s.[order] = 0 OR s.[order] is NULL) ORDER BY p.[order]")
						
						Set paramsIds=Server.CreateObject("Scripting.Dictionary")
						Set tableParams=Server.CreateObject("Scripting.Dictionary")
						
						while NOT RS_params.EOF							
							paramKey = RS_params("id")
							paramValue = RS_params("type")
							
							paramsIds.Add paramKey, paramValue
							if (paramValue = 5) then
								tableParams.Add paramKey, RS_params("value") & ""
							end if
							
							RS_params.MoveNext
						Wend
						
						paramsIdsKeys=paramsIds.Keys
						
						SQL = "SELECT id, from_date, schedule, schedule_type,"&"["&join(paramsIdsKeys,"],[")&"]" & vbCrLf &_
						" FROM" & vbCrLf &_
						" 	 (" & vbCrLf &_
						"	 SELECT a.id, a.from_date, a.schedule, a.schedule_type, p.param_id, p.value" & vbCrLf &_
						"	 FROM " & vbCrLf &_
						"	 (" & vbCrLf &_
						"		 SELECT id, from_date, schedule, schedule_type FROM alerts WHERE param_temp_id = '"&templateId&"'" & vbCrLf &_
						"		 UNION ALL" & vbCrLf &_
						"		 SELECT id, from_date, schedule, schedule_type FROM archive_alerts WHERE param_temp_id = '"&templateId&"'" & vbCrLf &_
						"	 ) a" & vbCrLf &_
						"	 LEFT JOIN alert_param_values p" & vbCrLf &_
						"		ON p.alert_id=a.id" & vbCrLf &_
						"	 ) d" & vbCrLf &_
						"	 PIVOT" & vbCrLf &_
						"	 (" & vbCrLf &_
						"	 	 MAX(value)" & vbCrLf &_
						"	 	 FOR param_id IN" & vbCrLf &_
						"	 	 (" & vbCrLf &_
						"		 	 ["&join(paramsIdsKeys,"],[")&"]" & vbCrLf &_
						"	 	 )" & vbCrLf &_
						"	 ) pvt" & vbCrLf &_
						" ORDER BY "&sortby
						
						Set RS_alerts = Server.CreateObject("ADODB.Recordset")
						RS_alerts.Open SQL, Conn, 3
						
						cnt = RS_alerts.RecordCount
						
						page="alert_parametrized.asp?temp_id="&templateId&"&"
						if(cnt>0) then
							name=LNG_ALERTS 
							Response.Write make_pages(offset, cnt, limit, page, name, sortby) 
						end if
						
						response.write "<br><A href='#' class='delete_button' onclick=""javascript: return LINKCLICK('"&linkclick_str&"');"">"&LNG_DELETE&"</a><br/><br/>"
						response.write "<form action='delete_objects.asp' name='delete_objs' id='delete_objs' method='post'><input type='hidden' name='objType' value='alerts'>"
						%>
							<table  style="width:100%" height="100%" cellspacing="0" cellpadding="3" class="data_table" style="margin:10px">
								<tr class="data_table_title"  width="100%">
								<td width="10"><input type='checkbox' name='select_all' id='select_all' onclick='javascript: select_all_objects();'></td>
						<%
						RS_params.MoveFirst
						num = 0
						while NOT RS_params.EOF
							%>
								<td class="table_title" >
								<%
									response.write sorting(RS_params("name"),"["&paramsIdsKeys(num)&"]", sortby, offset, limit, page) 
								%>
								</td>
							<%
							num=num+1
							RS_params.MoveNext
						Wend
						
						%>
							<td class="table_title" >Status</td>
							<td class="table_title" ><%=LNG_ACTIONS %></td>
						<%
					%>
						</tr>
						
					<%
						num=offset
						if(Not RS_alerts.EOF) then RS_alerts.Move(offset) end if
						Do While Not RS_alerts.EOF
							num=num+1
							if((offset+limit) < num) then Exit Do end if
							if(num > offset AND offset+limit >= num) then
							strObjectID=RS_alerts("id")
							schedule = RS_alerts("schedule")
							schedule_type = RS_alerts("schedule_type")
							%>
								<tr>
									<td>
										<% if(schedule_type<>"1") then %>
											<input type="checkbox" name="objects" value="<%=strObjectID%>">
										<% end if%>
									</td>
								<%
								
								
								for i=0 to paramsIds.Count-1
									
								'for each paramId in paramsIds
								%>
									<td>
									<%
									paramHTMLValue = RS_alerts(paramsIdsKeys(i))
									if (Clng(paramsIds.Item(paramsIdsKeys(i))) = 6) then
										convertRS = Conn.Execute("if ('"&paramHTMLValue&"'<>'') SELECT CONVERT(DATETIME,'"&paramHTMLValue&"') AS value ELSE SELECT '"&paramHTMLValue&"' as value")
										Response.Write "<scr" & "ipt language='javascript'>document.write( getUiDateTime('"&convertRS("value")& "'))</scr" & "ipt>"
									elseif (Clng(paramsIds.Item(paramsIdsKeys(i))) = 5) then
										tableParamHeaders = tableParams.Item(paramsIdsKeys(i))
										tableParamValues = paramHTMLValue
										paramHTMLContent = createTableParam(tableParamValues,tableParamHeaders)
										paramHTMLValue = createTableParamPreview(tableParamValues,tableParamHeaders)
										paramHTMLValue = "<table style='padding:0px; margin:0px; border:0px; border-collapse:collapse'><tr><td style='padding:0px; margin:0px; border:0px;'>"&paramHTMLValue&"</td><td style='padding:0px; padding-left:5px; margin:0px; border:0px;'><img class='table_param_preview_img' src='images/action_icons/preview.png' paramHtml='"&HtmlEncode(paramHTMLContent)&"' alt='"&LNG_PREVIEW&"' title='"&LNG_PREVIEW&"' width='16' height='16' border='0' hspace='5'></td></tr></table>"
										Response.Write paramHTMLValue
									else
										Response.Write paramHTMLValue
									end if
									%>
									</td>
								<%
								next
								%>
									<td>
										<%
											if schedule = "1" then
												if (schedule_type="1") then
													if (DateDiff("s", now_db, RS_alerts("from_date")) < 0) then
														Response.Write "overdue"
													else
														Response.Write "in process"
													end if
												else
													Response.Write "finished"
												end if
											end if
										%>
									</td>
									<td align="left">
										<% if(gid = 0 OR (gid = 1 AND alerts_arr(3)<>"")) then
											if(schedule_type="1") then
											%>
											<a href="change_schedule_type.asp?return_page=alert_parametrized.asp?temp_id=<%=templateId%>&id=<%=RS_alerts("id") %>&type=0"><img src="images/action_icons/stop.png" alt="<%=LNG_STOP_SCHEDULED_ALERT %>" title="<%=LNG_STOP_SCHEDULED_ALERT %>" width="16" height="16" border="0" hspace=5></a>
											<%
											else
											%>
											<a href="change_schedule_type.asp?return_page=alert_parametrized.asp?temp_id=<%=templateId%>&id=<%=RS_alerts("id") %>&type=1"><img src="images/action_icons/start.png" alt="<%=LNG_START_SCHEDULED_ALERT %>" title="<%=LNG_START_SCHEDULED_ALERT %>" width="16" height="16" border="0" hspace=5></a>
											<%
											end if
											%>
											<a href="alert_parametrized_edit.asp?temp_id=<%=templateId%>&alert_id=<%Response.Write RS_alerts("id")%>">
												<img src="images/action_icons/edit.png" alt="<%=LNG_EDIT %>" title="<%=LNG_EDIT %>" width="16" height="16" border="0" hspace=5>
											</a>
											<%
										end if
										%>
									</td>
								</tr>
							<%
							end if
						RS_alerts.MoveNext
						Loop
					%>
						</table>
					<%
						response.write "<input type='hidden' name='back' value='alert_parametrized.asp?temp_id="&templateId&"'></form><br/><A href='#' class='delete_button' onclick=""javascript: return LINKCLICK('"&linkclick_str&"');"">"&LNG_DELETE&"</a><br><br>"
						if(cnt>0) then
							Response.Write make_pages(offset, cnt, limit, page, name, sortby)
						end if
					end if
					%>
			</div>
		</td>
	</tr>
</table>
<% 
else
	Response.Redirect "index.asp"
end if
%>
<!-- #include file="db_conn_close.asp" -->