﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditColorCodes.aspx.cs" Inherits="DeskAlertsDotNet.Pages.EditColorCodes" %>
<%@ Import NameSpace="DeskAlertsDotNet.Parameters" %>
<%@ Import NameSpace="Resources" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link href="css/style9.css" rel="stylesheet" type="text/css">
    <link href="css/colpick.css" rel="stylesheet" type="text/css">
    <link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/colpick.js"></script>
    <script language="javascript">
        $(document).ready(function () {
            $(".add_button").button();
            $(".save_button").button();
            $(".cancel_button").button();
            $('#colorPick').colpick({
                layout: 'hex',
                submit: false,
                colorScheme: 'dark',
                onChange: function (hsb, hex, rgb, el, bySetColor) {
                    $("#color").val(hex);
                    $("#colorPick").css("background", "#" + hex);
                }
            }).keyup(function () {
                $(this).colpickSetColor(this.value);
            });
            $('#colorPick').colpickSetColor($("#color").val(), false);
        });

        function checkInput() {
            if ($("#ccName").val() == "") {
                alert("<%=resources.LNG_PLEASE_FILL_ALL_EMPTY_FIELDS %>");
                return false;
            }
            if ($("#color").val() == "") return false;

            $.ajax({
                type: "POST",
                url: "EditColorCodes.aspx/AddColorCode",
                data: '{name: \"' + $("#ccName").val() + '\" , color: \"' + $("#color").val() + '\", ccId: \"' + $("#ccId").val()  +'\" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(data) {
                    location.href = "ColorCodes.aspx";
                }
            });
            return true;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <input type="hidden" runat="server" id="ccId" value=""/>
    <div>
      <table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	        <tr>
	        <td>
	        <table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
                <tr>
		            <td width="100%" height="31" class="main_table_title"><img src="images/menu_icons/im_20.png" alt="" width="20" height="20" border="0" style="padding-left:7px">
		            <asp:LinkButton href="EditColorCodes.aspx" runat="server" id="headerTitle" class="header_title" style="position:absolute;top:22px" />
		            </td>
                </tr>
                <tr>
                <td class="main_table_body" height="100%">
                    <div style="margin:10px;">
                        <span class="work_header" runat="server" id="workHeader"></span>
                    </div>
                    
                    <table border="0">
                        <tr>
                            <td>
                                <table cellpadding=4 cellspacing=4 border=0>
                                    <tr><td><%=resources.LNG_COLOR_CODE %>:</td>
                                        <td>
                                            <input autocomplete="off" name="ccName" runat="server" id="ccName" type="text" value="" size="40" maxlength="255" />
                                         </td>
                                    </tr>
                                    <tr><td><%=resources.LNG_COLOR %>:</td>
                                        <td>
                                            <div id="colorPick"  runat="server" style="height:21px;width:50px;background:#747474"></div>
                                            <input  runat="server"  type="hidden" id="color" name="color" value="#747474" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                     </table>
                </td>
                </tr>
                <tr><td align="right">
                    
                     <a type="submit" class="cancel_button" onclick="javascript:history.go(-1); return false;"><%=resources.LNG_CANCEL %></a>
                     <a class="save_button"  onclick=" return checkInput();"><%=resources.LNG_SAVE %></a>
                    <br/><br/>

                </td></tr>
	        </table>
	        </td>
            </tr>
          </table>
           <asp:ScriptManager runat="server">
               
           </asp:ScriptManager>
            <asp:UpdatePanel runat="server" id="updatePanel"></asp:UpdatePanel>
            
                 
    </div>
    </form>
</body>
</html>
