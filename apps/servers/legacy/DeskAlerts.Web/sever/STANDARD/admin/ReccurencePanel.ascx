﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReccurencePanel.ascx.cs" Inherits="DeskAlertsDotNet.Controls.ReccurencePanel" %>
<%@ Import Namespace="System.Globalization" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="DeskAlerts.Server.Utils" %>
<%@ Import Namespace="DeskAlerts.Server.Utils.Helpers" %>


<link href="css/jquery/jquery_timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css">
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery_timepicker/jquery-ui-timepicker-addon.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/datepicker_fix.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/date.js"></script>

<script>
    var countdownArray = [];
    var serverDate;
    var settingsDateFormat = "<% = DateTimeConverter.JsDateFormat %>";
    var settingsTimeFormat = "<% = DateTimeConverter.JsTimeFormat %>";

    var responseDbFormat = "dd/MM/yyyy HH:mm:ss";
    var saveDbFormat = "dd/MM/yyyy HH:mm";
    var saveDbDateFormat = "dd/MM/yyyy";
    var saveDbTimeFormat = "HH:mm";
    var uiFormat = settingsDateFormat + " " + saveDbTimeFormat;

    var currentPatternValue = "o";

    function addCountDown(countValue, num) {

        $(function () {
            var count = $(".count_down_element").length;
            var id = last_countdown_id++;
            var $countDownDiv = $("#countdown_div");

            var $countDownElement = $("<div/>").addClass("count_down_element").hide();

            var $table = $("<table/>")
                .css({
                    padding: "0px",
                    margin: "0px",
                    "border-collapse": "collapse",
                    border: "0px",
                    "vertical-align": "middle"
                });
            var $tr = $("<tr/>");
            var $inputCell = $("<td/>").addClass("count_down_cell");
            var $deleteCell = $("<td/>").addClass("count_down_cell");
            $table.append($tr);
            $tr.append($inputCell);
            $tr.append($deleteCell);
            $countDownElement.append($table);

            var $inputElement = $("<select/>")
                .attr({ "id": "count_down_" + id, "name": "countdowns" })
                .addClass("count_down_input")
                .addClass("reccurenceControl");
            var $deleteIcon = $("<img/>")
                .attr({ "src": "images/action_icons/delete.png" })
                .addClass("count_down_delete_img");
            if (count == 0) {
                $deleteIcon.hide();
            } else {
                $(".count_down_delete_img").show();
            }

            $inputCell.append($inputElement);
            $inputCell
                .append($("<label/>")
                    .attr({ "for": "countdowns" })
                    .text(" <%=resources.LNG_MINUTES_BEFORE_START_DATE_TIME %> "));
            $deleteCell.append($deleteIcon);
            $countDownDiv.append($countDownElement);

            var step = 5;
            for (var i = step; i <= 120; i += step) {
                $inputElement.append($("<option value='" + i + "'>" + i + "</option>"));
            }

            if (!(countValue === undefined)) {
                $inputElement.val(countValue);
            }

            $countDownElement.show();

            $deleteIcon.bind("click",
                function () {
                    var height = $table.height();
                    $countDownElement.remove();
                    $("#optionsReccurenceFieldSet").height($("#optionsReccurenceFieldSet").height() - height);
                    $("#patternReccurenceFieldSet").height($("#patternReccurenceFieldSet").height() - height);
                    $("#recurrenceDiv").height($("#recurrenceDiv").height() - height);
                    if ($(".count_down_element").length < 2) {
                        $(".count_down_delete_img").hide();
                    }
                });

            var height = $table.height();


            if ($("#recurrenceDiv").height() == 0 || height == 0) {

                var h = 180 + num * 20
                //   alert(h);
                $("#recurrenceDiv").height(h);
                $("#patternReccurenceFieldSet").height(h);
                $("#optionsReccurenceFieldSet").height(h);
            }
            else {
                $("#recurrenceDiv").height($("#recurrenceDiv").height() + height);
                $("#patternReccurenceFieldSet").height($("#patternReccurenceFieldSet").height() + height);
                $("#optionsReccurenceFieldSet").height($("#optionsReccurenceFieldSet").height() + height);
            }
        });
    }


    $(document).ready(function () {

       // $("input[type=checkbox]").addClass("reccurenceControl");

      //  $(".reccurenceControl").prop('disabled', false);
        <% if (AlertId > 0)
    { %>


        var curPattern = '<% =Pattern%>';
        $("#schedulePanel_oncePatternDiv").css("display", curPattern == 'c' || curPattern == 'o' ? "block" : "none");

        $("#schedulePanel_dailyPatternDiv").css("display", curPattern == 'd' ? "block" : "none");

        $("#schedulePanel_weeklyPatternDiv").css("display", curPattern == 'w' ? "block" : "none");

        $("#schedulePanel_monthlyPatternDiv").css("display", curPattern == 'm' ? "block" : "none");

        $("#schedulePanel_yearlyParrentDiv").css("display", curPattern == 'y' ? "block" : "none");

        $("#schedulePanel_alertRange").css("display", curPattern != 'o' ? "grid" : "none");

        $("#schedulePanel_alertTime").css("display", curPattern != 'o' ? "grid" : "none");


        if (curPattern != "o") {
            var extraHeight = $("#schedulePanel_alertRange").height() +
                $("#schedulePanel_alertTime").height();

            var currentHeight = $("#recurrenceDiv").height();

            $("#recurrenceDiv").height(currentHeight + extraHeight);
        }




        var remindersArray = [<% =countdowns %>];



        for (var i = 0; i < remindersArray.length; i++) {
            if (isNaN(i) || isNaN(remindersArray[i]))
                continue;
            addCountDown(remindersArray[i], i);

        }

        setTimeout(function () {
            if (remindersArray.length > 0) {
                document.getElementById("countdownEnable").checked = true;

                $("#add_countdown").button({ disabled: false });
            }

        }, 100);
        <% } %>


        serverDate = new Date(getDateFromAspFormat("<%=DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss" , CultureInfo.InvariantCulture)%>", responseDbFormat));
        setInterval(function () { serverDate.setSeconds(serverDate.getSeconds() + 1); }, 1000);

        var fromDate = $.trim($("#start_date_and_time").val());
        var toDate = $.trim($("#end_date_and_time").val());

        var fromTime = $.trim($("#start_time").val());
        var toTime = $.trim($("#end_time").val());

        if (fromDate != "" && isDate(fromDate, responseDbFormat)) {
            $("#start_date_and_time").val(formatDate(new Date(getDateFromFormat(fromDate, responseDbFormat)), uiFormat));
        }

        if (toDate != "" && isDate(toDate, responseDbFormat)) {
            $("#end_date_and_time").val(formatDate(new Date(getDateFromFormat(toDate, responseDbFormat)), uiFormat));
        }

        var uiTimetimeFormat = getUiTimeFormat(settingsTimeFormat);

        if (fromTime != "" && isDate(fromTime, responseDbFormat)) {
            fromTime = new Date(getDateFromFormat(fromTime, responseDbFormat));
        }

        if (toTime != "" && isDate(toTime, responseDbFormat)) {
            toTime = new Date(getDateFromFormat(toTime, responseDbFormat));
        }

        var fromDateRec = $.trim($("#start_date_rec").val());
        var toDateRec = $.trim($("#end_date_rec").val());

        if (fromDateRec != "" && isDate(fromDateRec, responseDbFormat)) {
            fromDateRec = new Date(getDateFromFormat(fromDateRec, responseDbFormat));
            if (fromDateRec.getFullYear() == "1900") {
                fromDateRec = null;
            }
        }

        if (toDateRec != "" && isDate(toDateRec, responseDbFormat)) {
            toDateRec = new Date(getDateFromFormat(toDateRec, responseDbFormat));
            if (toDateRec.getFullYear() == "1900") {
                toDateRec = null;
            }
        }

        $(".date_time_param_input").datetimepicker({ dateFormat: settingsDateFormat, timeFormat: settingsTimeFormat });
        $(".date_param_input").datepicker({ dateFormat: settingsDateFormat, showButtonPanel: true });
        $(".time_param_input").timepicker({ timeFormat: settingsTimeFormat });

        $("#start_date_rec").datepicker("setDate", fromDateRec);
        $("#end_date_rec").datepicker("setDate", toDateRec);

        $("#start_time").datepicker("setDate", fromTime);
        $("#end_time").datepicker("setDate", toTime);

        $("#add_countdown").button();
        $("#add_countdown").bind("click", addCountDown);

        if (countdownArray && countdownArray.length > 0) {
            for (var i = 0; i < countdownArray.length; i++) {
                addCountDown(countdownArray[i], i);
            }
            var $checkbox = $("#countdownEnable");
            $checkbox.prop("checked", "checked");
        }
        countdownEnableChange(true);

        $("#<%=pattern.ClientID%>")
            .change(function () {
                var selectedValue = $("#<%=pattern.ClientID%> input:checked").val();




                $("#schedulePanel_oncePatternDiv").css("display", selectedValue == 'o' ? "block" : "none");

                $("#schedulePanel_dailyPatternDiv").css("display", selectedValue == 'd' ? "block" : "none");

                $("#schedulePanel_weeklyPatternDiv").css("display", selectedValue == 'w' ? "block" : "none");

                $("#schedulePanel_monthlyPatternDiv").css("display", selectedValue == 'm' ? "block" : "none");

                $("#schedulePanel_yearlyParrentDiv").css("display", selectedValue == 'y' ? "block" : "none");

                $("#schedulePanel_alertRange").css("display", selectedValue != 'o' ? "grid" : "none");

                $("#schedulePanel_alertTime").css("display", selectedValue != 'o' ? "grid" : "none");

                var extraHeight = $("#schedulePanel_alertRange").height() + $("#schedulePanel_alertTime").height();

                var currentHeight = $("#recurrenceDiv").height();

                if (selectedValue != "o") {
                    if (currentPatternValue == "o")
                        $("#recurrenceDiv").height(currentHeight + extraHeight);
                } else {
                    $("#recurrenceDiv").height(currentHeight - extraHeight);
                }

                currentPatternValue = selectedValue;
            });
    });

    var last_countdown_id = 0;
    function countdownEnableChange(init, force) {
        var $checkbox = $("#countdownEnable");
        var $add_button = $("#add_countdown");
        var countdownDiv = document.getElementById("countdown_div");

        if (force === undefined) {
            force = !$checkbox.prop("checked");
        }

        if (force) {
            $(".count_down_delete_img").hide();
        }
        else {
            if ($(".count_down_element").length > 1) {
                $(".count_down_delete_img").show();
            }
            if (init && $(".count_down_element").length == 0) {
                addCountDown();
            }
        }

        $add_button.button({ disabled: force });
        toggleDisabled(countdownDiv, force);
    }


</script>
<table style="table-layout: fixed; width: 280px">
    <tr>

        <td style="vertical-align: top">
            <fieldset id="patternReccurenceFieldSet" class="reccurence_pattern" style="height: 170px; width: 135px">
                <legend><%=resources.LNG_PATTERN %></legend>

                <div style="padding: 10px 5px 10px 20px; line-height: 20px;">
                    <asp:RadioButtonList CssClass="reccurenceControl" runat="server" ID="pattern" OnSelectedIndexChanged="OnReccurencePatternChanged">

                        <asp:ListItem id="patternOnce" runat="server" Value="o" Selected="True" />

                        <asp:ListItem id="patternDayly" runat="server" Value="d" />

                        <asp:ListItem id="patternWeekly" runat="server" Value="w" />

                        <asp:ListItem id="patternMonthly" runat="server" Value="m" />

                        <asp:ListItem id="patternYearly" runat="server" Value="y" />
                    </asp:RadioButtonList>
                </div>
            </fieldset>
        </td>

        <td style="vertical-align: top">

            <fieldset id="optionsReccurenceFieldSet" class="reccurence_pattern" style="height: 170px; width: 420px">
                <legend><%=resources.LNG_OPTIONS %></legend>


                <div width="420" runat="server" id="oncePatternDiv" style="padding: 2px;">
                    <br />
                    <br />
                    <table border="0">
                        <tr>
                            <td align="right" nowrap="nowrap">&nbsp;<%=resources.LNG_START_DATE %>: </td>
                            <td nowrap="nowrap">
                                <input runat="server" name='start_date_and_time' class="date_time_param_input reccurenceControl" id="start_date_and_time" type="text" value="" size="20" />
                                <input runat="server" name='from_date' id="from_date" type="hidden" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right" nowrap="nowrap">&nbsp;<%=resources.LNG_END_DATE %>: </td>
                            <td nowrap="nowrap">
                                <input runat="server" aname='end_date_and_time' class="date_time_param_input reccurenceControl" id="end_date_and_time" type="text" value="" size="20" />
                                <input name='to_date' id="to_date" type="hidden" />
                            </td>
                        </tr>
                    </table>
                    <div style="" class="countdown_field" id="countdown_field">
                        <div style="padding: 2px 10px;">
                            <input type='checkbox' name='countdownEnable' value="1" class="reccurenceControl" id='countdownEnable' onclick="countdownEnableChange(true);" />
                            <label for='countdownEnable'><%=resources.LNG_COUNTDOWN_ENABLE %></label>
                            <img src="images/help.png" title="<%=resources.LNG_COUNTDOWN_TITLE %>." />
                            <div id="countdown_div" class="countdown_div" style="">
                            </div>
                            <span id="add_countdown" class="jquery_button add_button" style="margin-top: 5px"><%=resources.LNG_ADD %></span>
                        </div>
                    </div>
                </div>

                <div runat="server" id="dailyPatternDiv">
                    <ol>
                        <li style="list-style-type: none;">
                            <input runat="server" id="dayly_selector" type="hidden" name="dayly_selector" value="dayly_1" />

                            &nbsp;<select class="reccurenceControl" runat="server" id="number_days" name="number_days">
                            </select>
                            <%=resources.LNG_DAYS %>
                        </li>
                    </ol>
                </div>

                <div runat="server" id="weeklyPatternDiv">
                    <ol>
                        <li style="list-style-type: none;">&nbsp;<%=resources.LNG_RESEND_EVERY %>
                            <select runat="server" class="reccurenceControl" id="number_week" name="number_week">
                            </select>
                            <%=resources.LNG_WEEK_IN_NEXT_DAYS %>

                        </li>
                        <li style="list-style-type: none;">
                            <asp:CheckBoxList runat="server" class="reccurenceControl" ID="weekDayPatternCheckBoxList" RepeatColumns="4">
                                <asp:ListItem class="reccurenceControl" runat="server" Value="1" id="weekdaySunday" />
                                <asp:ListItem class="reccurenceControl" runat="server" Value="2" id="weekdayMonday" />
                                <asp:ListItem class="reccurenceControl" runat="server" Value="4" id="weekdayTuesday" />
                                <asp:ListItem class="reccurenceControl" runat="server" Value="8" id="weekdayWednesday" />
                                <asp:ListItem class="reccurenceControl" runat="server" Value="16" id="weekdayThursday" />
                                <asp:ListItem class="reccurenceControl" runat="server" Value="32" id="weekdayFriday" />
                                <asp:ListItem class="reccurenceControl" runat="server" Value="64" id="weekdaySaturday" />
                            </asp:CheckBoxList>

                        </li>
                    </ol>
                </div>

                <div runat="server" id="monthlyPatternDiv">
                    <ol>
                        <li style="list-style-type: none;">
                            <input class="reccurenceControl" runat="server" id="monthly1" type="radio" value="monthly_1" checked="" name="monthly_selector" />
                            <label for="monthly1"><%=resources.LNG_ON %></label>
                            <select class="reccurenceControl" runat="server" id="m_month_day" name="m_month_day">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                                <option value="13">13</option>
                                <option value="14">14</option>
                                <option value="15">15</option>
                                <option value="16">16</option>
                                <option value="17">17</option>
                                <option value="18">18</option>
                                <option value="19">19</option>
                                <option value="20">20</option>
                                <option value="21">21</option>
                                <option value="22">22</option>
                                <option value="23">23</option>
                                <option value="24">24</option>
                                <option value="25">25</option>
                                <option value="26">26</option>
                                <option value="27">27</option>
                                <option value="28">28</option>
                                <option value="29">29</option>
                                <option value="30">30</option>
                                <option value="31">31</option>
                            </select>
                            <%=resources.LNG_OF_MONTH %>
                            <select class="reccurenceControl" runat="server" id="number_month_1" name="number_month_1">
                            </select>
                            <%=resources.LNG_MONTH %></li>
                        <li style="list-style-type: none;">
                            <input class="reccurenceControl" runat="server" id="monthly2" type="radio" value="monthly_2" name="monthly_selector" />
                            <label for="monthly2"><%=resources.LNG_ON %></label>
                            <select class="reccurenceControl" runat="server" id="m_weekday_place" name="m_weekday_place">
                            </select>
                            <select class="reccurenceControl" runat="server" id="m_weekday_day" name="m_weekday_day">
                            </select><%=resources.LNG_OF_MONTH %>
                            <select class="reccurenceControl" runat="server" id="number_month_2" name="number_month_2">
                            </select>
                            <%=resources.LNG_MONTH %></li>
                    </ol>
                </div>

                <div runat="server" id="yearlyParrentDiv">
                    <ol>
                        <li style="list-style-type: none;">
                            <input class="reccurenceControl" runat="server" id="yearly1" type="radio" value="yearly1" checked="" name="yearly_selector" />
                            <label for="yearly1"><%=resources.LNG_ON %></label>
                            <select class="reccurenceControl" runat="server" id="y_month_day" name="y_month_day">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                                <option value="13">13</option>
                                <option value="14">14</option>
                                <option value="15">15</option>
                                <option value="16">16</option>
                                <option value="17">17</option>
                                <option value="18">18</option>
                                <option value="19">19</option>
                                <option value="20">20</option>
                                <option value="21">21</option>
                                <option value="22">22</option>
                                <option value="23">23</option>
                                <option value="24">24</option>
                                <option value="25">25</option>
                                <option value="26">26</option>
                                <option value="27">27</option>
                                <option value="28">28</option>
                                <option value="29">29</option>
                                <option value="30">30</option>
                                <option value="31">31</option>
                            </select>
                            <%=resources.LNG_OF_MONTH %>
                            <select class="reccurenceControl" runat="server" id="month_val_1" name="month_val_1">
                            </select>
                            <%=resources.LNG_MONTH %></li>
                        <li style="list-style-type: none;">
                            <input class="reccurenceControl" runat="server" id="yearly2" type="radio" value="yearly_2" name="yearly_selector" />
                            <label for="yearly2"><%=resources.LNG_ON %></label>
                            <select class="reccurenceControl" runat="server" id="y_weekday_place" name="y_weekday_place">
                            </select>
                            <select class="reccurenceControl" runat="server" id="y_weekday_day" name="y_weekday_day">
                            </select>
                            <%=resources.LNG_OF_MONTH %>
                            <select class="reccurenceControl" runat="server" id="month_val_2" name="month_val_2">
                            </select><%=resources.LNG_MONTH %></li>
                    </ol>
                </div>
            </fieldset>
        </td>
    </tr>

    <tr>
        <td runat="server" id="alertRange" colspan="2" height="120" width="550">
            <fieldset style="padding-bottom: 5px; width: 558px">
                <legend><%=resources.LNG_RANGE %></legend>
                <table border="0" cellpadding="0" cellspacing="0" class="inside_fieldset" height="100">
                    <tr>
                        <td valign="top" width="250" rowspan="3" nowrap style="padding: 10px">
                            <label for="start_date_rec"><%=resources.LNG_START_ON %>:</label>
                            <input runat="server" name='start_date_rec' class="date_param_input reccurenceControl" id="start_date_rec" type="text" value="" size="20" />
                            <input id="from_date_rec" class="date_param_input reccurenceControl" type="hidden" name="from_date_rec" />
                        </td>
                        <td nowrap>
                            <input id="radio1" runat="server" class="reccurenceControl" type="radio" name="end_type" value="no_date" checked /><label for="radio1"> <%=resources.LNG_NO_END_DATE %></label>
                        </td>
                    </tr>
                    <tr>
                        <td nowrap>
                            <input id="radio2" runat="server" class="reccurenceControl" type="radio" name="end_type" value="end_after" /><label for="radio2"> <%=resources.LNG_END_AFTER %>:</label>
                            <input class="end_input reccurenceControl" runat="server" type="text" size="1" name="end_after" id="end_after" value="1">
                            <%=resources.LNG_OCCURENCES %>
                        </td>
                    </tr>
                    <tr>
                        <td nowrap>
                            <input id="radio3" runat="server" class="reccurenceControl" type="radio" name="end_type" value="end_by" />
                            <label for="radio3"><%=resources.LNG_END_BY %>:</label>
                            <input runat="server" name='end_date_rec' class="date_param_input reccurenceControl" id="end_date_rec" type="text" value="" size="20" />
                            <input id="to_date_rec" class="date_param_input reccurenceControl" type="hidden" name="to_date_rec" />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </td>
    </tr>
    <tr>
        <td runat="server" id="alertTime" colspan="2" height="100" width="550">
            <fieldset style="padding: 5px; width: 548px">
                <legend><%=resources.LNG_ALERT_TIME %></legend>
                <table border="0">
                    <tr>
                        <td nowrap="nowrap" align="right" width="170">
                            <%=resources.LNG_START_TIME %>:
                        </td>
                        <td nowrap="nowrap">
                            <input runat="server" name='start_time' class="time_param_input reccurenceControl" id="start_time" type="text" value="" size="20" />
                            <input name='from_time' id="from_time" type="hidden" />
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap" align="right" width="170">
                            <%=resources.LNG_END_TIME %>:
                        </td>
                        <td nowrap="nowrap">
                            <input runat="server" name='end_time' class="time_param_input reccurenceControl" id="end_time" type="text" value="" size="20" />
                            <input name='to_time' id="to_time" type="hidden" />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </td>
    </tr>

</table>
