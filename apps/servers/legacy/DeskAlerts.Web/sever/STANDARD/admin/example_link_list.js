
var tinyMCELinkList = new Array(
	// Name, URL
	//["DeskAlerts", "http://www.deskalerts.com"]
);

(function(){
var dom = tinyMCEPopup.dom;
var ed = tinyMCEPopup.editor;
var se = ed.selection;

var initComplete = false;
var translationLoaded = false;

tinyMCEPopup.onInit.add(function()
{
	if(!dom.getParent(se.getNode(),'A'))
	{
		selectByValue(document.forms[0], 'targetlist', '_blank' );
	}
	
	dom.remove(dom.getParent('class_list', 'tr'));
	dom.remove(dom.getParent('classlist', 'tr'));
	dom.remove('hrefbrowsercontainer');
	dom.setStyles('href', {'width':'280px'});

	if(typeof insertAction != 'undefined')
	{
		var old_insert_func = insertAction;
		insertAction = function()
		{
			var node = dom.getParent(se.getNode(), 'A');
			if(se.isCollapsed() && !dom.getParent(se.getNode(), 'A'))
			{
				var text = document.getElementById('text');
				if(text)
				{
					tinyMCEPopup.execCommand("mceInsertLink", false, "#mce_temp_url#", {skip_undo : 1});
					var elementArray = tinymce.grep(ed.dom.select("a"), function(n) {return ed.dom.getAttrib(n, 'href') == '#mce_temp_url#';});
					if(elementArray&&elementArray[0])
						se.select(node = elementArray[0]);
				}
			}
			old_insert_func();
			if(node&&node.innerHTML.indexOf('<')==-1)
			{
				var text = document.getElementById('text');
				if(text)
					node.innerHTML = text.value;
			}
		}
	}
	initComplete = true;
	addFields();
});

var addFields = function()
{
	if(initComplete && translationLoaded)
	{
		var text, node = dom.getParent(se.getNode(), 'A');
		if(node) text = node.innerHTML;
		else text = se.getContent();
		if((se.isCollapsed() && !node) || text&&text.indexOf('<')==-1)
		{
			if(node) text = node.textContent||node.innerText||'';
			else text = se.getContent({format:'text'});
			if(!node || text)
			{
				var table = dom.getParent('hreflabel', 'table');
				var tr = dom.create('tr');
				var lab_td = dom.add(tr, 'td', {'class':'nowrap'});
				dom.add(lab_td, 'label', {'id':'textlabel','for':'text'}, tinyMCEPopup.getLang('style_dlg.text'));
				var txt_td = dom.add(tr, 'td', {'colspan':'2'});
				dom.add(txt_td, 'input', {'id':'text','name':'text','type':'text','value':text,'aria-required':'true','style':'width: 280px;','class':'mceFocus'});
				table.insertBefore(tr, table.firstChild);
			}
		}
	}
}

var requireLangPack = function(a, callback)
{
	var b=tinyMCEPopup;
	if(a&&b.editor.settings.language&&b.features.translate_i18n!==false&&b.editor.settings.language_load!==false)
	{
		a="jscripts/tiny_mce/plugins/"+a+"/langs/"+b.editor.settings.language+"_dlg.js";
		if(!tinymce.ScriptLoader.isDone(a))
		{
			var url = tinymce._addVer(a);
			if(callback)
				tinymce.ScriptLoader.add(url, callback);
			tinymce.ScriptLoader.loadScripts([tinymce._addVer(a)]);
		}
		else if(callback)
		{
			callback();
		}
	}
}
requireLangPack('style', function(){translationLoaded = true; addFields()});

})();