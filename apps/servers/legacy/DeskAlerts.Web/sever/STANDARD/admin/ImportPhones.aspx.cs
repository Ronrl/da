﻿using System;
using System.Data;
using System.IO;
using System.Web;
using DeskAlertsDotNet.Utils.Factories;
using Microsoft.VisualBasic.FileIO;
using NLog;

namespace DeskAlertsDotNet.Pages
{
    public partial class ImportPhones : DeskAlertsBasePage
    {
        public string UploadPath = @"~/admin/images/upload/";
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        protected void Page_Load(object sender, EventArgs e)
        {
            LoadLocalization();
            loaderDiv.Style.Add("display", "none");
        }

        private void LoadLocalization()
        {
            if (IsPostBack)
            {
                return;
            }

            headerTitle.InnerText = Resources.resources.LNG_IMPORT_PHONES;
            importPhoneTemplate.Text = Resources.resources.LNG_IMPORT_PHONE_TEMPLATE_DOWNLOAD_BUTTON;
            UploadButton.Text = Resources.resources.LNG_UPLOAD;
            ImportingLabel.Text = $@"{Resources.resources.LNG_IMPORTING}...";
        }

        protected void UploadButton_Click(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                return;
            }

            loaderDiv.Style.Add("display", "inline-block");

            if (FileUploader.HasFile)
                try
                {
                    Label1.Text = "";
                    FileUploader.PostedFile.SaveAs(Server.MapPath(UploadPath) +
                                        FileUploader.FileName);
                    ParseCsv(FileUploader.PostedFile.InputStream);
                    Label1.Text = Resources.resources.LNG_IMPORTING_SUCCESSFULLY;
                    Response.Redirect(Request["return_page"], true);
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    Label1.Text = $@"ERROR: {ex.Message}";
                }
            else
            {
                Label1.Text = Resources.resources.LNG_FILE_NOT_SELECTED;
            }

            loaderDiv.Style.Add("display", "none");
        }

        protected void ImportPhoneTemplate_Download(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                return;
            }

            try
            {
                using (var ms = new MemoryStream())
                {
                    TextWriter tw = new StreamWriter(ms);

                    const int countOfTemplateRow = 5;
                    for (var i = 1; i < countOfTemplateRow + 1; i++)
                    {
                        tw.WriteLine($"%UserName{i}%,%PhoneNumber{i}%");
                    }
                    tw.Flush();
                    byte[] bytes = ms.ToArray();
                    ms.Close();

                    Response.Clear();
                    Response.ContentType = "text/plain";
                    Response.AddHeader("Content-Disposition", "attachment; filename=Import.csv");
                    Response.BinaryWrite(bytes);
                    Response.Flush();
                    Response.SuppressContent = true;

                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        private void ParseCsv(Stream fileStream)
        {
            try
            {
                using (var inputStreamReader = new StreamReader(fileStream))
                {
                    while (!inputStreamReader.EndOfStream)
                    {
                        var inputContent = inputStreamReader.ReadLine();
                        if (inputContent != null)
                        {
                            var parser = new TextFieldParser(new StringReader(inputContent))
                            {
                                HasFieldsEnclosedInQuotes = true
                            };
                            parser.SetDelimiters(";", ",");

                            while (!parser.EndOfData)
                            {
                                var fields = parser.ReadFields();
                                if (fields == null)
                                {
                                    continue;
                                }

                                var phoneNumberSqlParameter = SqlParameterFactory.Create(DbType.String, fields[1], "PhoneNumber");
                                var userNameSqlParameter = SqlParameterFactory.Create(DbType.String, fields[0], "UserName");

                                var updateQuery = $"UPDATE users SET mobile_phone = @PhoneNumber WHERE role=\'U\' AND name = @UserName";
                                dbMgr.ExecuteQuery(updateQuery, phoneNumberSqlParameter, userNameSqlParameter);
                            }
                            parser.Close();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
    }
}