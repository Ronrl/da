﻿<!-- #include file="config.inc" -->
<!-- #include file="ad.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%

check_session()

  uid = Session("uid")

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>DeskAlerts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/style9.css" rel="stylesheet" type="text/css">
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script language="javascript" type="text/javascript">

$(document).ready(function() {
	var width = $(".data_table").width();
	var pwidth = $(".paginate").width();
	if (width != pwidth) {
		$(".paginate").width("100%");
	}
	$(".search_button").button({
		icons : {
			primary : "ui-icon-search"
		}
	});
	
	$(".add_group_button").button({
		icons : {
			primary : "ui-icon-plusthick"
		}
	});
	
	$(".delete_button").button();
});


</script>
</head>

<script language="javascript" type="text/javascript" src="functions.js"></script>

<body style="margin:0px" class="body">

<%
	if(Request("offset") <> "") then 
		offset = clng(Request("offset"))
	else 
		offset = 0
	end if	

	if(Request("sortby") <> "") then 
		sortby = Request("sortby")
	else 
		sortby = "name"
	end if	

	if(Request("gname") <> "") then 
		mygname = Request("gname")
	else 
		mygname = ""
	end if	

	linkclick_str = LNG_IP_GROUPS
	ip_groups_arr = Array("checked","checked","checked","checked","checked","checked","checked")
	gid = 0
	gsendtoall = 0
	set RS = Conn.Execute("SELECT id, only_personal FROM users WHERE id="&uid&" AND role='E'")
	if not RS.EOF then
		gid = 1
		set rs_policy = Conn.Execute("SELECT p.id, p.type, pu.user_id FROM policy p INNER JOIN policy_editor pe ON p.id=pe.policy_id LEFT JOIN policy_user pu ON pu.policy_id = p.id AND pu.user_id = 0 WHERE pe.editor_id=" & uid)
		if not rs_policy.EOF then
			if rs_policy("type")="A" then 'can send to all
				gid = 0
			elseif not IsNull(rs_policy("user_id")) then
				gid = 0
				gsendtoall = 1
			end if
		end if
		if gid = 1 or gsendtoall = 1 then
			editor_id=RS("id")
			policy_ids=""
			policy_ids=editorGetPolicyIds(editor_id)
			ip_groups_arr = editorGetPolicyList(policy_ids, "ip_groups_val")
			if ip_groups_arr(2) = "" then
				linkclick_str = "not"
			end if
		end if
	end if
	RS.Close
  
  
if (uid <>"") then
'counting pages to display
   if(Request("limit") <> "") then 
	limit = clng(Request("limit"))
   else 
	limit=50
   end if

	if(mygname<>"") then
			Set RS = Conn.Execute("SELECT COUNT(id) as mycnt FROM ip_groups WHERE name LIKE N'%"&Replace(mygname, "'", "''")&"%'")
	else
			Set RS = Conn.Execute("SELECT COUNT(id) as mycnt FROM ip_groups")
	end if
	cnt=0
	if(Not RS.EOF) then
		cnt=RS("mycnt")
	end if
	RS.Close
        j=cnt/limit
%>
<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	<tr>
	<td>
	<table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
		<tr>
		<td width=100% height=31 class="main_table_title"><img src="images/menu_icons/ipgroups_20.png" alt="" width="20" height="20" border="0" style="padding-left:7px">
		<a href="admin_ip_groups.asp" class="header_title" style="position:absolute;top:15px"><%=LNG_IP_GROUPS %></a></td>
		</tr>
		<tr>
		<td class="main_table_body" height="100%">
		<div style="margin:10px;">
<form name="search_users" action="admin_ip_groups.asp" method="get"><table width="100%" border="0"><tr><td><%=LNG_SEARCH_GROUPS %>: <input type="text" name="gname" value="<%= HtmlEncode(mygname) %>"/> <button type="submit" name="sub" class="search_button"><%=LNG_SEARCH %></button></td>
<%
if(mygname<>"") then
response.write "<td>"& LNG_YOUR_SEARCH_BY &" """&mygname&""" "&LNG_KEYWORD &":</td>"
end if
%><td align="right">

<%if ip_groups_arr(0) = "checked" then%>
<a href="edit_ip_group.asp" class="add_group_button"><%=LNG_ADD_GROUP %></a>
<%end if%>

</td></tr></table>
</form> 


<%
if(cnt>0) then
	page="admin_ip_groups.asp?gname=" & mygname & "&"
	name= LNG_GROUPS
	Response.Write make_pages(offset, cnt, limit, page, name, sortby) 

	response.write "<br><A href='#' class='delete_button' onclick=""javascript: return LINKCLICK('"&linkclick_str&"');"">"&LNG_DELETE&"</a><br/><br>"
	response.write "<form action='delete_objects.asp' name='delete_objs' id='delete_objs' method='post'><input type='hidden' name='objType' value='ip_groups'>"
%>


		<table width="100%" height="100%" class="data_table" cellspacing="0" cellpadding="3">
		<tr class="data_table_title">
<td width="10"><input type='checkbox' name='select_all' id='select_all' onclick='javascript: select_all_objects();'></td>

		<td class="table_title"><% response.write sorting(LNG_GROUP_NAME,"name", sortby, offset, limit, page) %></td>
		<td class="table_title" width="100px"><%=LNG_ACTIONS %></td>
				</tr>
<%
'show main table


	if(mygname<>"") then
		Set RS = Conn.Execute("SELECT id, name FROM ip_groups WHERE name LIKE N'%"&Replace(mygname, "'", "''")&"%' ORDER BY "&sortby)
	else
		Set RS = Conn.Execute("SELECT id, name FROM ip_groups ORDER BY "&sortby)
	end if

	num=offset
	if(Not RS.EOF) then RS.Move(offset) end if
	Do While Not RS.EOF
		num=num+1
		if((offset+limit) < num) then Exit Do end if
		if(num > offset AND offset+limit >= num) then
		cnt1=0

		strObjectID=RS("id")

        	%>
		<tr>

	<td><input type="checkbox" name="objects" value="<%=strObjectID%>"></td>
		<td>
		<% if ip_groups_arr(4) = "checked" then %>
		<A href="#" onclick="window.open('view_ip_group.asp?id=<%=RS("id") %>','',350,350);">
		<% Response.Write RS("name") %></a>
		<% else 
			Response.Write RS("name") 
		   end if
		%>
		
		</td>
		<td align="center" width="100px">
		<%
		if ip_groups_arr(1) = "checked" then
		%>
		<a href="edit_ip_group.asp?id=<%= RS("id") %>"><img src="images/action_icons/edit.png" alt="<%=LNG_EDIT_GROUP %>" title="<%=LNG_EDIT_GROUP %>" width="16" height="16" border="0" hspace=5></a>
		</td></tr>
		<%
		end if
		end if
	RS.MoveNext
	Loop
	RS.Close
	
		%>
                </table>
		<%

	response.write "</form><br/><A href='#' class='delete_button' onclick=""javascript: return LINKCLICK('"&linkclick_str&"');"">"&LNG_DELETE&"</a><br><br>"

	Response.Write make_pages(offset, cnt, limit, page, name, sortby) 

else

Response.Write "<center><b>"& LNG_THERE_ARE_NO_GROUPS &".</b><br><br></center>"

end if
%>
 
		<img src="images/inv.gif" alt="" width="1" height="100%" border="0">
		</div>
		</td>
		</tr>
	</table>
	</td>
	</tr>
</table>

<%
else

  Response.Redirect "index.asp"

end if
%> 
</body>
</html>
<!-- #include file="db_conn_close.asp" -->