﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AddMessageTemplatesGroups.aspx.cs" Inherits="deskalerts.AddMessageTemplatesGroups" %>
            
    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
    <html>

    <head>
        <title>DeskAlerts</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css" />
        
       <link href="css/jquery/jquery_timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css">
        <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
		<link href="css/style9.css" rel="stylesheet" type="text/css">
        <script type="text/JavaScript" language="JavaScript" src="jscripts/json2.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/preview.js?v=9.2.1.31"></script>
        <script language="javascript" type="text/javascript" src="functions.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/date.js"></script>
     
	    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery_timepicker/jquery-ui-timepicker-addon.js"></script>
	    <script language="javascript" type="text/javascript" src="jscripts/jquery/datepicker_fix.js"></script>
        <style rel="stylesheet" type="text/css">
            /*.ui-dialog-titlebar-close {
  visibility: hidden;
}*/
            
            .date_time_param_input{
	        width:150px
            }

            .main_table
            {
                width: 96%;
            }

            .style6
            {
                width: 85%;
            }

            </style>
    </head>

    <script language="javascript" type="text/javascript">





        function openDialogUI(_form, _frame, _href, _width, _height, _title) {
            $("#dialog-" + _form).dialog('option', 'height', _height);
            $("#dialog-" + _form).dialog('option', 'width', _width);
            $("#dialog-" + _form).dialog('option', 'title', _title);
            $("#dialog-" + _frame).attr("src", _href);
            $("#dialog-" + _frame).css("height", _height);
            $("#dialog-" + _frame).css("display", 'block');
            if ($("#dialog-" + _frame).attr("src") != undefined) {
                $("#dialog-" + _form).dialog('open');
                $("#dialog-" + _form).contents().find('#table_border').css('border', 'none');
            }
        }




        $(document).ready(function() {
            $("#confirm_sending").dialog({
                autoOpen: false,
                height: 100,
                width: 300,
                modal: false,
                position: {
                    my: "center top",
                    at: "center top",
                    of: $('#parent_for_confirm')
                },
                hide: {
                    effect: 'fade',
                    duration: 1000
                },
                //show: { effect: "highlight", color: '#98FB98', duration: 2000 },
                open: function(event, ui) {
                    $(".ui-dialog-titlebar-close", ui.dialog || ui).hide();
                    setTimeout(function() {
                        $("#confirm_sending").dialog('close');
                    }, 3000);
                },
                closeOnEscape: false
            });

            $("#dialog-form").dialog({
                autoOpen: false,
                height: 100,
                width: 100,
                modal: true
            });


            var is_sended = $("#is_sended").val();
            if (is_sended == 'true') {
                if ($('#reschedule').val() == '1') {
                    $('#anchor_confirm').text('=LNG_CONFIRM_SCHEDULE ');
                } else {
                    $('#anchor_confirm').text('=LNG_CONFIRM_SEND ');
                }
                $("#last_alert").effect('highlight', {
                    color: '#98FB98'
                }, 2000);
                $("#confirm_sending").dialog('open');
            }

            var width = $(".data_table").width();

            var pwidth = $(".paginate").width();
            if (width != pwidth) {
                $(".paginate").width("100%");
            }




            $("#dialog-frame").load(function() {
                $("#dialog-frame").contents().find('.prelink_stats').on('click', function(e) {
                    e.preventDefault();
                    var link = $(this).attr('href');
                    var title = $(this).attr('title');
                    var last_id = $('.dialog_forms').size();
                    var elem = '<div id="dialog-form-' + last_id + '" class="dialog_forms" style="overflow:hidden;display:none;"><iframe id="dialog-frame-' + last_id + '" style="display:none;width:100%;height:auto;overflow:hidden;border : none;"></iframe></div>';
                    $('#secondary').append(elem);
                    $("#dialog-form-" + last_id).dialog({
                        autoOpen: false,
                        height: 100,
                        width: 100,
                        modal: true
                    });
                    openDialogUI("form-" + last_id, "frame-" + last_id, link, 350, 350, title);
                });
            });

        });

        function openDialog(mode) {

            var dialogId = "";
            var textBoxId = ""; 

            switch (mode) {
                case 1: 
                    {
                        dialogId = "#duplicateDialog";
                        textBoxId = "#dupName";
                        break;
                    }
                case 2: //add
                    {
                        dialogId = "#linkDialog";
                        textBoxId = "#linkValue";
                        $("#schedule").prop("checked", false);
                        $("#start_date_and_time").prop('disabled', true);
                        $
                        break;
                    }
            }
            $(textBoxId).focus(function() {
                $(textBoxId).select().mouseup(function(e) {
                    e.preventDefault();
                    $(this).unbind("mouseup");
                });
            });
            $(dialogId).dialog('open');

        }

        function deleteRow(id) {
            $.ajax({
                url: "set_settings.asp?name=DeleteRow&value=" + id
            }).done(function(data, textStatus, jqXHR) {
                $("#row" + id).remove();
            });


        }

        function checkNewId(newId) {
            var list2 = document.getElementById("templatesInGroupList");
            for (i = list2.options.length - 1; i >= 0; i--) {
                if (list2.options[i].value == newId) {
                    return false;
                }
            }
            return true;
        }
 
        function list_add() {

            var templatesList = window.frames["list1"];
            var templatesInGroupList = document.getElementById("templatesInGroupList");
            //var groupsToAdd = templatesList.val();

            var items_ids = templatesList.document.getElementsByName("users");
            var items_values = templatesList.document.getElementsByName("users_name");

            for (var i = 0; i < items_ids.length; i++) {

                if ($(items_ids[i]).is(":checked"))
                {
                    var newVal = items_values[i].value;
                    var newId = items_ids[i].value;
                    var row = new Option(newVal, newId);
                    

                    if (checkNewId(newId)) {
                        templatesInGroupList.add(row, templatesInGroupList.length - 1)
                    }            
                }
            }
        }

        $(function() {
            $(".delete_button").button({});

            $(".add_alert_button").button({
                icons: {
                    primary: "ui-icon-plusthick"
                }
            });


            $(".search_button").button({
                icons: {
                    primary: "ui-icon-search"
                }
            });

            $("#linkDialog").dialog({
                autoOpen: false,
                height: 125,
                width: 550,
                modal: true,
                resizable: false
            });

            $("#rescheduleDialog").dialog({
                autoOpen: false,
                height: 130,
                width: 200,
                modal: true,
                resizable: false
            });
            $("#duplicateDialog").dialog({
                autoOpen: false,
                height: 110,
                width: 550,
                modal: true,
                resizable: false
            });

        })

        function encode_utf8(s) {
            return encodeURIComponent(s);
        }

        function saveGroupTemplate() {

            var groupName = $("#group_name").val();
            groupName = groupName.replace("#", "%23");
            groupName = groupName.replace("&", "%26");
            if (groupName.length > 0) {

                var groupsToAdd = new Array;

                $("#templatesInGroupList option").each(function() {
                    groupsToAdd.push($(this).val());
                }
                );

                if (groupsToAdd == null || groupsToAdd.length == 0) {
                    alert("You should select at least 1 template");
                    return;
                }

                var templatesString = groupsToAdd.join();

                var updateId = '<% =Request["tgid"] %>';

                var queryStr = 'set_settings.asp?name=saveTGroups&tgname=' + encode_utf8(groupName) + '&templates=' + templatesString;

                if (updateId.length > 0) {
                    queryStr = 'set_settings.asp?name=saveTGroups&tgname=' + encode_utf8(groupName) + '&templates=' + templatesString + '&updateId=' + updateId;
                }


                $.get(queryStr, function(data) {

                    location.href = "MessageTemplatesGroups.aspx";
                });


            }
            else {
                alert("Enter group name");
            }
        }
        
        function list_remove() {
            var list2 = document.getElementById("templatesInGroupList");
            for (i = list2.options.length - 1; i >= 0; i--) {
                if (list2.options[i].selected == true && !list2.options[i].disabled) {
                    list2.options[i] = null;
                }
            }
        }           

    </script>


    <body style="margin:0" class="body">
     <!--   <form id="form1" runat="server"> -->
            <div id="dialog-form" style="overflow:hidden;display:none;">
                <iframe id='dialog-frame' style="display:none;width:100%;height:auto;overflow:hidden;border : none;">

                </iframe>
            </div>
            <div id="confirm_sending" style="overflow:hidden;text-align:center;">
                <a id="anchor_confirm"></a>
            </div>
            

            <div id="secondary" style="overflow:hidden;display:none;">
            </div>
            <table width="70%" border="0" cellspacing="0" cellpadding="6" height="100%">
                <tr>
                    <td>
                        <table height="100%" width="100%" cellspacing="0" cellpadding="0" class="main_table">
                            <tr>
								<td width="100%" height="31" class="main_table_title">
									<img src="images/menu_icons/campaign.png" alt="" width="20" height="20" border="0" style="padding-left:7px">
									<a href="MessageTemplatesGroups.aspx" class="header_title" style="position:absolute;top:15px">
                                        <asp:Label ID="tgroupsLabel" runat="server" Text="Add group"></asp:Label>
                                    </a>
                                </td>
							<!--	<td height="31" class="style6">

                                </td>	
								<td width="100%" height="31" class="main_table_title"> 

                                </td>-->
                            </tr>
                            <tr>
                            
                            
                                <td height="100%" valign="top" class="main_table_body">
                                    <table width="10%" height="100%">
                                    <tr>
                                    <div style="margin:10px" id="parent_for_confirm">
                                        <span class="work_header"><asp:Label ID="tgroupsDescription" runat="server" Text="Select message templates for your group"></asp:Label> </span>
                                        <br />

											<br />
											<br />
											<br />
											<% Response.Write(vars["LNG_GROUP_NAME"] + ":"); %>
											<input id="group_name" runat="server"></input>
											

                                            <tr>
                                                <td align="left"  >
												
												<iframe id="list1"  name="list1" class="list_iframe" src="tgroups_table.asp" style="border:0px;margin-left:10px;width:400px"></iframe>


												</td>
												
												 <td class="style6" >
													<br />	
													<br />	
													<br />
													<br />
													<a href="#" onclick="list_add_tgroup();return false;"><img src="images/list_add.png" border="0" style="margin-right:50px" /></a><br/>
													<br />
													<a href="#" onclick="list_remove();return false;"><img src="images/list_remove.png" border="0" /></a><br/>
													<br />
													<br />
													<br />
													<br />	
													<br />
													<br />	
												</td>
												
												<td class="style6" >
													<br />	
													<br />
													<br />
													<br />
													<select runat="server" multiple id="templatesInGroupList"  class="list_select" style="margin-right:700px" > </select>
													<br />
													<br />
													<br />
													<br />
													<br />
													<br />
												</td>												
                                            </tr>
											
											<tr>
												<td colspan="4" style="padding-right:700px">
												<a href='#' class='delete_button' onclick="saveGroupTemplate();" style="float:right"><% Response.Write(vars["LNG_SAVE"]); %></a>
												<a href='MessageTemplatesGroups.aspx' class='delete_button'  style="float:right"><% Response.Write(vars["LNG_CANCEL"]); %></a>
												<br>
												<br>
												<br>
												<br>
												</td>
											</tr>


                                    </div>
                                    </tr>
                                    </table>

                                </td>
                            </tr>
						</table>
                    </td>
                </tr>
				<tr>
				<td>
				
				</td>
				</tr>
            </table>

            
      <!--  </form> -->
    </body>