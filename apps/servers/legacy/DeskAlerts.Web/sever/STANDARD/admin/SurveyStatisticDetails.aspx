﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SurveyStatisticDetails.aspx.cs" Inherits="DeskAlertsDotNet.Pages.SurveyStatisticDetails" %>
<%@ Import NameSpace="DeskAlertsDotNet.Parameters" %>
<%@ Import Namespace="Resources" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
    <link href="css/jquery/jquery_timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css" />
    <link href="css/style9.css" rel="stylesheet" type="text/css"/>
    <style>
        .buy-panel-title {
            font-weight: bold;
            font-size: 22px;
        }

        .buy_panel {
            background-color: #B3FFA3;
            border: 1px solid #696969;
            margin: 0px 0px 0px 30px;
            padding: 10px;
            line-height: 20px;
            max-width: 500px;
            word-wrap: break-word;
            margin: 10px 0;
        }

        .buy-panel-body {
            font-size: 12px;
        }
    </style>
    <script type="text/javascript" src="functions.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/date.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery_timepicker/jquery-ui-timepicker-addon.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/jquery/datepicker_fix.js"></script>
    	<script language="javascript" type="text/javascript" src="jscripts/jquery.jqplot.min.js"></script>
		<script language="javascript" type="text/javascript" src="jscripts/jqplot.highlighter.min.js"></script>
		<script type="text/javascript" src="jscripts/jqplot.pieRenderer.min.js"></script>
		<script type="text/javascript" src="jscripts/jqplot.barRenderer.min.js"></script>
		<script type="text/javascript" src="jscripts/jqplot.categoryAxisRenderer.min.js"></script>
    
    <script language="JavaScript" src="jscripts/FusionCharts.js"></script>
		<link rel="stylesheet" type="text/css" href="css/jquery.jqplot.css" />
    <script type="text/javascript">


        function openDialogUI(_form, _frame, _href, _width, _height, _title) {
            $("#dialog-" + _form).dialog('option', 'height', _height);
            $("#dialog-" + _form).dialog('option', 'width', _width);
            $("#dialog-" + _form).dialog('option', 'title', _title);
            $("#dialog-" + _frame).attr("src", _href);
            $("#dialog-" + _frame).css("height", _height);
            $("#dialog-" + _frame).css("display", 'block');
            if ($("#dialog-" + _frame).attr("src") != undefined) {
                $("#dialog-" + _form).dialog('open');
                $("#dialog-" + _form).contents().find('#table_border').css('border', 'none');
            }
        }
        
        $(document)
            .ready(function () {

                $("#stopStartButton").button();
                $("#duplicate_button").button();
                $(".question_button").button();
                $("#dialog-form")
                    .dialog({
                        autoOpen: false,
                        height: 100,
                        width: 100,
                        modal: true
                    });

                $('#dialog-frame')
                    .on('load',
                    function () {
                        $("#dialog-frame")
                            .contents()
                            .find('.prelink_stats')
                            .on('click',
                            function (e) {
                                e.preventDefault();
                                var link = $(this).attr('href');
                                var title = $(this).attr('title');
                                var last_id = $('.dialog_forms').size();
                                var elem = '<div id="dialog-form-' +
                                    last_id +
                                    '" class="dialog_forms" style="overflow:hidden;display:none;"><iframe id="dialog-frame-' +
                                    last_id +
                                    '" style="display:none;width:100%;height:auto;overflow:hidden;border : none;"></iframe></div>';
                                $('#secondary').append(elem);
                                $("#dialog-form-" + last_id)
                                    .dialog({
                                        autoOpen: false,
                                        height: 100,
                                        width: 100,
                                        modal: true
                                    });
                                openDialogUI("form-" + last_id, "frame-" + last_id, link, 350, 350, title);
                            });
                    });

                var alertsPlot = jQuery.jqplot('alertsChart',
                    [<% =alertsValues %>],
                    {
                        grid: {
                            background: 'white',
                            renderer: $.jqplot.CanvasGridRenderer,
                            drawBorder: false,
                            shadow: false
                        },
                        seriesColors: [<% =alertsColors %>],
                        seriesDefaults: {
                            shadow: true,
                            shadowAlpha: 0.1,
                            showMarker: true,
                            useNegativeColors: false,
                            renderer: jQuery.jqplot.BarRenderer,
                            rendererOptions: {
                                fillToZero: true,
                                barWidth: 40,
                                barPadding: 25
                            }
                        },
                        legend: {
                            show: false,
                            location: "e"
                        },
                        axes: {
                            xaxis: {
                                renderer: $.jqplot.CategoryAxisRenderer,
                                ticks: [''],
                                numberTicks: 1
                            }
                        }
                    });

                var data = [<% =answersValues%>];
                var answersPlot = jQuery.jqplot('answersChart',
                    [data],
                    {
                        grid: {
                            background: '#ffffff',
                            renderer: $.jqplot.CanvasGridRenderer,
                            drawBorder: false,
                            shadow: false
                        },
                        seriesColors: [<% =answersColors%>],
                        seriesDefaults: {
                            shadow: true,
                            shadowAlpha: 0.1,
                            renderer: jQuery.jqplot.PieRenderer,
                            rendererOptions: {
                                diameter: 200,
                                padding: 25
                            }
                        },
                        legend: {
                            show: false,
                            location: "s"
                        }
                    }
                );
            });



    </script>
 </head>
<body class="body">
    <form id="form1" runat="server">
        <table width="95%">
            <tbody>
                <tr>
                    <td align="left">
                        <b>Title: <asp:Literal runat="server" id="alertTitleLiteral"></asp:Literal></b>
                    </td>
                    <td align="right"><b>
                         <asp:Literal runat="server" id="createDateLiteral"></asp:Literal>
                    </b></td>
                </tr>
            </tbody>
        </table>
        <div style="width:100%; text-align:right;">
            <%= isSentToIpGroups ?  resources.LNG_SEND_TO_IP_GROUPS : resources.LNG_TOTAL_RECIPIENTS + ":"  %> 
            <asp:Literal runat="server" id="totalRecipients"></asp:Literal>
        </div>
        <center>
             <table border=0 style="text-align:left">
                     <tr><td colspan="3"><div id="alertsChart" style="width:250px;height:180px"></div></td></tr>
                     <tr><td><div class="legend" id="firstLegend" runat="server" ></div></td><td runat="server" id="firstLegendLabel"></td><td runat="server" id="firstLegendValue"></td></tr>
                     <tr><td><div class="legend" runat="server" id="secondLegend" ></div></td><td runat="server" id="secondLegendLabel"></td><td  runat="server" id="secondLegendValue"></td></tr>
                     <tr><td><div class="legend" runat="server" id="thirdLegend" ></div></td><td runat="server" id="thirdLegendLabel"></td><td  runat="server" id="thirdLegendValue"></td></tr>
                     <tr><td><div class="legend" runat="server" id="fourhLegend" ></div></td><td runat="server" id="fourhLegendLabel"></td><td  runat="server" id="fourhLegendValue"></td></tr>
            </table>
        </center>
        
            <br/>
        
        <div style="width:100%; text-align:right;"><a href='' target='_blank' id="csvDownloadLink" runat="server"><b> <% =resources.LNG_EXPORT_TO_CSV %>   </b></a></div>
        <div style="width:100%; text-align:right;"><a href='' target='_blank' id="xlsxDownloadLink" runat="server"><b> <% =resources.LNG_EXPORT_TO_XLSX %>   </b></a></div>
        
        
        <a name="results"><b><%=resources.LNG_SURVEY_RESULTS %></b></a><br>
        <b><%=resources.LNG_QUESTION %>: <asp:Literal runat="server" id="questionText"></asp:Literal></b><br>
        <div style="width:100%; text-align:right;"><%=resources.LNG_TOTAL_NUMBER_OF_QUESTIONS %>: <asp:Literal runat="server" id="totalQuestions"/></div>
        
        <center>
        <div id="answersChart" runat="server" style="width:350px;height:250px"></div>
        <div id="BuyExtendedReports" class="buy_panel" runat="server" visible="false"></div>
        <table runat="server" id="questionsTable" border=0 style="text-align:left">
            

        </table>
        </center>

        <a class='question_button' href="#" runat="server" id="prevQuestionButton"><%=resources.LNG_PREVIOUS_QUESTION %></a>
        <a class='question_button' href="#" runat="server" id="nextQuestionButton"><%=resources.LNG_NEXT_QUESTION %></a>

        <div style="width:100%; text-align:right;"><A runat="server" id="singleCsvLink" target="_blank"><b><% =resources.LNG_EXPORT_THIS_QUESTION_TO_CSV %></b></a></div>
        <div style="width:100%; text-align:right;"><A runat="server" id="singleXlsxLink" target="_blank"><b><% =resources.LNG_EXPORT_THIS_QUESTION_TO_XLSX %></b></a></div>
        <div style="width:100%; text-align:right;"><A  runat="server" id="fullCsvLink" target="_blank"><b><% =resources.LNG_EXPORT_ALL_QUESTIONS_TO_CSV %></b></a></div>
        <div style="width:100%; text-align:right;"><A  runat="server" id="fullXlsxLink" target="_blank"><b><% =resources.LNG_EXPORT_ALL_QUESTIONS_TO_XLSX %></b></a></div>
    
    </form>
</body>
</html>
