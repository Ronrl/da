﻿using Castle.MicroKernel.Lifestyle;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.ApplicationCore.Repositories;
using DeskAlerts.ApplicationCore.Utilities;
using DeskAlerts.Server.Utils.Interfaces.Services;
using DeskAlerts.Server.Utils.Interfactes;
using DeskAlerts.Server.Utils.Managers;
using DeskAlerts.Server.Utils.Services;
using DeskAlertsDotNet.Application;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Models;
using DeskAlertsDotNet.Parameters;
using DeskAlertsDotNet.Utils.Consts;
using DeskAlertsDotNet.Utils.Factories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using Twilio.Rest.Api.V2010.Account;
using DataRow = DeskAlertsDotNet.DataBase.DataRow;
using DataSet = DeskAlertsDotNet.DataBase.DataSet;
using User = DeskAlertsDotNet.Models.User;

namespace DeskAlertsDotNet.Pages
{
    public partial class AddRecipients : DeskAlertsBasePage
    {
        private readonly IAlertRepository _alertRepository;
        private readonly IAlertsSentRepository _alertsSentRepository;
        private readonly ICampaignRepository _campaignRepository;
        private readonly IContentReceivedService _contentReceivedService;
        private readonly ICacheInvalidationService _cacheInvalidationService;
        
        private readonly CacheManager _cacheManager = Global.CacheManager;

        private IEnumerable<DataRow> _sendDataSet;

        private delegate void DoOnLoadPage(int[] alertIds, string[] addedRecipients, string edit, bool isAlertEdit, int scheduleType, bool isModerated,
            string sendType, string returnPage, string alertType, bool sendToTemplate, string selectedTemplate, bool isInstant, bool isSurvey, Campaign campaign);

        private const string SelectGroupFromGroups = @"
            SELECT [id],
                   [container_group_id],
                   [group_id],
                   [was_checked]
            FROM [dbo].[groups_groups]
            WHERE container_group_id = @groupId;";

        public AddRecipients()
        {
            using (Global.Container.BeginScope())
            {
                _alertRepository = Global.Container.Resolve<IAlertRepository>();
                _alertsSentRepository = Global.Container.Resolve<IAlertsSentRepository>();
                _campaignRepository = Global.Container.Resolve<ICampaignRepository>();
                _contentReceivedService = new ContentReceivedService();
                _cacheInvalidationService = Global.Container
                    .Resolve<ICacheInvalidationService>();
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            if (!IsPostBack)
            {

            }

            base.OnLoad(e);

            var returnPage = "PopupSent.aspx";

            if (!string.IsNullOrEmpty(Request["returnPage"]))
            {
                returnPage = Request["returnPage"];
            }

            var sendType = Request["sendType"];
            string alertType;

            var scheduleType = 1;
            var instant = Request["instant"];
            var isInstant = Request["instant"] == "1";
            var selectedTemplate = Request.GetParam("selectedTemplate", "");
            var sendToTemplate = selectedTemplate.Length > 0;
            var isSurvey = Request["survey"] == "1";

            Campaign campaign = null;

            var campaignId = Request["campaignId"];

            if (!string.IsNullOrEmpty(campaignId))
            {
                campaign = _campaignRepository.GetById(long.Parse(campaignId));
            }

            Logger.Trace($"Variable: {nameof(sendType)}. Value: {sendType}.");
            Logger.Trace($"Variable: {nameof(scheduleType)}. Value: {scheduleType}.");
            Logger.Trace($"Variable: {nameof(instant)}. Value: {instant}.");
            Logger.Trace($"Variable: {nameof(isInstant)}. Value: {isInstant}.");
            Logger.Trace($"Variable: {nameof(sendToTemplate)}. Value: {sendToTemplate}.");
            Logger.Trace($"Variable: {nameof(campaignId)}. Value: {campaignId}.");

            if (sendToTemplate)
            {
                sendType = "recipients";
            }

            if (!string.IsNullOrEmpty(instant) && isInstant)
            {
                scheduleType = 0;
            }

            alertType = sendType.ToUpper()[0].ToString();

            var alertIds = Array.ConvertAll(Request.Form.GetValues("alertId") ?? throw new InvalidOperationException("Can't get alertId"), Convert.ToInt32);

            var edit = Request["edit"];
            var isAlertEdit = edit == "1";
            var isModerated = Request["isModerated"] == "1";
            var addedRecipients = Request.Form.GetValues("added_recipient");

            foreach (var alertId in alertIds)
            {
                using (var dataBaseManager = new DBManager())
                {
                    if (isInstant && (string.IsNullOrEmpty(edit) || !isAlertEdit))
                    {
                        dataBaseManager.ExecuteQuery("INSERT INTO instant_messages (alert_id) VALUES (" + alertId + ")");
                    }

                    dataBaseManager.ExecuteQuery("UPDATE alerts SET schedule_type = '" + scheduleType + "' WHERE id = " + alertId);
                    if (!string.IsNullOrEmpty(edit) && !isModerated)
                    {
                        if (sendType.Equals("broadcast"))
                        {
                            dataBaseManager.ExecuteQuery("UPDATE alerts SET type = 'S', sent_date=GETDATE(), type2='B', group_type='B', sender_id=" + curUser.Id + " WHERE id =" + alertId);

                            if (isSurvey)
                                dataBaseManager.ExecuteQuery("UPDATE surveys_main SET type='B' WHERE sender_id = " + alertId);
                        }
                    }
                    else
                    {
                        dataBaseManager.ExecuteQuery("UPDATE alerts SET type = 'S', sent_date=GETDATE(), type2='" + alertType + "', group_type='B', sender_id=" + curUser.Id + " WHERE id=" + alertId);

                        if (isSurvey)
                            dataBaseManager.ExecuteQuery("UPDATE surveys_main SET type='R' WHERE sender_id = " + alertId);
                    }

                    var alertSet = dataBaseManager.GetDataByQuery("SELECT approve_status FROM alerts WHERE id = " + alertId);

                    var approvedStatus = alertSet.GetInt(0, "approve_status") == 1 ||
                                             alertSet.GetInt(0, "approve_status") == 3;

                    if ((string.IsNullOrEmpty(edit) || isModerated) && approvedStatus)
                    {
                        SendToCluster(alertId);
                        
                    }
                }
            }
            var action = new DoOnLoadPage(AsyncOnLoad);
            action.BeginInvoke(alertIds, addedRecipients, edit, isAlertEdit, scheduleType, isModerated, sendType, returnPage, alertType, sendToTemplate, selectedTemplate, isInstant, isSurvey, campaign, null, null);

            Response.Redirect(returnPage);
        }



        private void AsyncOnLoad(int[] alertIds, string[] addedRecipients, string edit, bool isAlertEdit, int scheduleType, bool isModerated,
            string sendType, string returnPage, string alertType, bool sendToTemplate, string selectedTemplate, bool isInstant, bool isSurvey, Campaign campaign)
        {
            foreach (var alertId in alertIds)
            {
                using (var dataBaseManager = new DBManager())
                {
                    try
                    {
                        dataBaseManager.ExecuteQuery("BEGIN TRANSACTION");

                        //select certain columns, not all entity
                        var alertSet = dataBaseManager.GetDataByQuery("SELECT * FROM alerts WHERE id = " + alertId);

                        var alertClass = alertSet.GetInt(0, "class");
                        var contentType = _alertRepository.GetContentTypeByContentId(alertId);

                        var isRssFeed = alertClass == (int)AlertType.RssFeed;

                        if (!string.IsNullOrEmpty(edit) && !isModerated)
                        {
                            dataBaseManager.ExecuteQuery("DELETE FROM alerts_sent WHERE alert_id = " + alertId);
                            dataBaseManager.ExecuteQuery("DELETE FROM alerts_sent_group WHERE alert_id = " + alertId);
                            dataBaseManager.ExecuteQuery("DELETE FROM alerts_sent_comp WHERE alert_id = " + alertId);

                            if (sendType.Equals("recipients"))
                            {
                                foreach (var recipient in addedRecipients)
                                {
                                    var rgxObjType = new Regex("[^a-zA-Z -]");
                                    var rgxObjId = new Regex("[^0-9 -]");

                                    var objType = rgxObjType.Replace(recipient, "");
                                    var objId = rgxObjId.Replace(recipient, "");

                                    var tableName = "alerts_sent";

                                    switch (objType)
                                    {
                                        case "u":
                                            {
                                                tableName = "alerts_sent";
                                                break;
                                            }

                                        case "g":
                                            {
                                                tableName = "alerts_sent_group";

                                                var usersInCurrentGroup =
                                                    dataBaseManager.GetDataByQuery(
                                                        "SELECT user_id FROM users_groups WHERE group_id = " + objId);

                                                foreach (var userId in usersInCurrentGroup)
                                                {
                                                    var id = userId.GetString("user_id");
                                                    var tempQuery = $"INSERT INTO alerts_sent VALUES ({alertId}, {id})";

                                                    dataBaseManager.ExecuteQuery(tempQuery);
                                                }

                                                break;
                                            }

                                        case "c":
                                            {
                                                tableName = "alerts_sent_comp";
                                                break;
                                            }
                                    }

                                    var query = $"INSERT INTO {tableName} VALUES ({alertId}, {objId})";

                                    dataBaseManager.ExecuteQuery(query);
                                }

                                Response.Redirect(returnPage);
                            }
                            else if (sendType.Equals("iprange"))
                            {
                                SendAlertToIpRanges(addedRecipients, alertId);
                            }
                        }
                        else
                        {
                            var sendQuery = "SET NOCOUNT ON " +
                                            " IF OBJECT_ID('tempdb..#tempOUs') IS NOT NULL DROP TABLE #tempOUs" +
                                            " CREATE TABLE #tempOUs  (Id_child BIGINT NOT NULL);" +
                                            " IF OBJECT_ID('tempdb..#tempUsers') IS NOT NULL DROP TABLE #tempUsers" +
                                            " CREATE TABLE #tempUsers  (user_id BIGINT NOT NULL);" +
                                            " IF OBJECT_ID('tempdb..#tempComputers') IS NOT NULL DROP TABLE #tempComputers" +
                                            " CREATE TABLE #tempComputers  (comp_id BIGINT NOT NULL);" +
                                            " IF OBJECT_ID('tempdb..#tempGroups') IS NOT NULL DROP TABLE #tempGroups" +
                                            " CREATE TABLE #tempGroups  (group_id BIGINT NOT NULL);" +
                                            "DECLARE @now DATETIME " +
                                            "SET @now = GETDATE() ";

                            var isPublisher =
                                dataBaseManager.GetScalarByQuery<string>("SELECT role FROM users u WHERE u.id =" + curUser.Id) ==
                                "E";
                            var isBroadcastEnabled = true;
                            var policyId = 0;

                            if (isPublisher)
                            {
                                policyId = dataBaseManager.GetScalarByQuery<int>(
                                    "SELECT policy_id FROM [policy_editor] pe WHERE pe.[editor_id] =" + curUser.Id);
                                var usersByPolicies = dataBaseManager.GetDataByQuery(
                                    "SELECT * FROM [policy_user] p WHERE p.[policy_id] =" + policyId);
                                isBroadcastEnabled = usersByPolicies.Count == 0;
                                if (isPublisher && !isBroadcastEnabled && alertType == "B" && sendType == "broadcast")
                                {
                                    sendType = "recipients";
                                    alertType = "R";
                                }
                            }

                            var allRecipients = addedRecipients;
                            var users = new string[] { };
                            var groups = new string[] { };
                            var computers = new string[] { };
                            var oUs = new string[] { };

                            if (sendType.Equals("recipients") || sendToTemplate)
                            {
                                if (allRecipients == null && isPublisher && !isBroadcastEnabled && policyId != 0 &&
                                    !sendToTemplate)
                                {
                                    var dataSetUsers =
                                        dataBaseManager.GetDataByQuery(
                                            "SELECT [user_id] FROM [policy_user] p WHERE [was_checked] = 1 AND p.[policy_id] = " +
                                            policyId);
                                    var dataSetComputers =
                                        dataBaseManager.GetDataByQuery(
                                            "SELECT [comp_id] FROM [policy_computer] p WHERE [was_checked] = 1 AND p.[policy_id] = " +
                                            policyId);
                                    var dataSetGroups =
                                        dataBaseManager.GetDataByQuery(
                                            "SELECT [group_id] FROM [policy_group] p WHERE [was_checked] = 1 AND p.[policy_id] = " +
                                            policyId);
                                    var dataSetOUs =
                                        dataBaseManager.GetDataByQuery(
                                            "SELECT [ou_id] FROM [policy_ou] p WHERE [was_checked] = 1 AND p.[policy_id] = " +
                                            policyId);

                                    users = dataSetUsers.ToList().Select(x => x.GetString("user_id")).ToArray();
                                    computers = dataSetComputers.ToList().Select(x => x.GetString("comp_id")).ToArray();
                                    groups = dataSetGroups.ToList().Select(x => x.GetString("group_id")).ToArray();
                                    oUs = dataSetOUs.ToList().Select(x => x.GetString("ou_id")).ToArray();
                                }
                                else if (sendToTemplate)
                                {
                                    groups = new[] { selectedTemplate };
                                }
                                else
                                {
                                    users = allRecipients.Where(obj => obj[0] == 'u').Select(obj => obj.Substring(1))
                                        .ToArray();
                                    groups = allRecipients.Where(obj => obj[0] == 'g').Select(obj => obj.Substring(1))
                                        .ToArray();
                                    computers = allRecipients.Where(obj => obj[0] == 'c').Select(obj => obj.Substring(1))
                                        .ToArray();
                                    oUs = allRecipients.Where(obj => obj[0] == 'O').Select(obj => obj.Substring(3))
                                        .ToArray();
                                }

                                if (groups.Any())
                                {
                                    // Add OUs from groups to general list
                                    var groupsString = string.Join(",", groups);
                                    var oUsInGroups = dataBaseManager
                                        .GetDataByQuery($@"SELECT ou_id 
                                FROM ou_groups 
                                WHERE group_id IN ({groupsString})")
                                        .ToList()
                                        .Select(row => row.GetString("ou_id"));

                                    oUs = oUs.Union(oUsInGroups)
                                        .ToArray();
                                }

                                var ouSqlQueryPart = "";

                                if (oUs.Any())
                                {
                                    var ouString = string.Join(",", oUs);

                                    if (!curUser.HasPrivilege)
                                    {
                                        ouSqlQueryPart += "INSERT INTO #tempOUs (Id_child)" +
                                                          "(" +
                                                          "	SELECT ou_id as Id_child FROM policy_ou p" +
                                                          "	LEFT JOIN #tempOUs as t" +
                                                          "	ON p.ou_id = t.Id_child" +
                                                          "	WHERE t.Id_child IS NULL AND ou_id IN (" + ouString +
                                                          ") AND p.policy_id = " + curUser.Policy.Id +
                                                          ") ";
                                    }
                                    else
                                    {
                                        ouSqlQueryPart += "INSERT INTO #tempOUs (Id_child)" +
                                                          "(" +
                                                          "	SELECT id FROM OU o" +
                                                          "	LEFT JOIN #tempOUs as t" +
                                                          "	ON o.id = t.Id_child" +
                                                          "	WHERE t.Id_child IS NULL" +
                                                          "	AND o.id IN (" + ouString + ")" +
                                                          ") ";
                                    }

                                    if (groups.Length > 0)
                                    {
                                        ouSqlQueryPart += "INSERT INTO #tempOUs (Id_child) (" +
                                                          "	SELECT o.ou_id FROM ou_groups o" +
                                                          "	LEFT JOIN #tempOUs as t" +
                                                          "	ON o.ou_id = t.Id_child" +
                                                          "	WHERE t.Id_child IS NULL AND o.group_id in (" +
                                                          string.Join(",", groups) +
                                                          ")" +
                                                          ") ";
                                    }
                                }

                                var groupQueryPart = "INSERT INTO #tempGroups (group_id) (" +
                                                     "SELECT Id_Group as group_id " +
                                                     "FROM OU_Group g " +
                                                     "INNER JOIN #tempOUs t ON t.Id_child=g.Id_OU" +
                                                     ") ";


                                if (groups.Length > 0)
                                {
                                    var groupsList = groups.ToList();
                                    var endGroups = GetEndGroups(groupsList, 0);
                                    var allGroups = groupsList.Union(endGroups);
                                    var groupsIds = string.Join(",", allGroups);

                                    groupQueryPart += "INSERT INTO #tempGroups (group_id) (" +
                                                      "	SELECT g.id FROM groups g" +
                                                      "	LEFT JOIN #tempGroups as t" +
                                                      "	ON g.id = t.group_id" +
                                                      "	WHERE t.group_id IS NULL AND id in (" + groupsIds + ")" +
                                                      ") ";
                                }

                                groupQueryPart += " IF OBJECT_ID('tempdb..#tmp1') IS NOT NULL DROP TABLE #tmp1 " +
                                                  " CREATE TABLE #tmp1  (group_id BIGINT NOT NULL); " +
                                                  " IF OBJECT_ID('tempdb..#tmp2') IS NOT NULL DROP TABLE #tmp2 " +
                                                  " CREATE TABLE #tmp2  (group_id BIGINT NOT NULL); " +
                                                  " INSERT INTO #tmp1 (group_id)(SELECT group_id FROM #tempGroups) " +
                                                  " WHILE 1=1 " +
                                                  " BEGIN " +
                                                  " INSERT INTO #tmp2 (group_id) " +
                                                  " ( " +
                                                  " SELECT DISTINCT g.group_id FROM groups_groups as g " +
                                                  " INNER JOIN #tmp1 as t1 " +
                                                  " ON g.container_group_id = t1.group_id " +
                                                  " LEFT JOIN #tempGroups as t2 " +
                                                  " ON g.group_id = t2.group_id " +
                                                  " WHERE t2.group_id IS NULL " +
                                                  " ) " +
                                                  " IF @@ROWCOUNT = 0 BREAK; " +
                                                  " DELETE #tmp1 " +
                                                  " INSERT INTO #tmp1 (group_id)(SELECT group_id FROM #tmp2) " +
                                                  " INSERT INTO #tempGroups (group_id)(SELECT group_id FROM #tmp2) " +
                                                  " DELETE #tmp2 " +
                                                  " END; " +
                                                  " DROP TABLE #tmp1" +
                                                  " DROP TABLE #tmp2 ";

                                var usersQueryPart = "INSERT INTO #tempUsers (user_id)(" +
                                                     "SELECT DISTINCT user_id " +
                                                     "FROM users_groups g " +
                                                     "INNER JOIN #tempGroups t " +
                                                     "ON t.group_id = g.group_id" +
                                                     ") ";

                                usersQueryPart += "INSERT INTO #tempUsers (user_id)(" +
                                                  " SELECT u.Id_user" +
                                                  "	FROM OU_User u" +
                                                  "	INNER JOIN #tempOUs o" +
                                                  "	ON o.Id_child = u.Id_OU" +
                                                  "	LEFT JOIN #tempUsers t" +
                                                  "	ON t.user_id = u.Id_user" +
                                                  "	WHERE t.user_id IS NULL" +
                                                  ") ";

                                if (users.Length > 0)
                                {
                                    var usersIds = string.Join(",", users);

                                    usersQueryPart += "INSERT INTO #tempUsers (user_id)(" +
                                                      "	SELECT u.id" +
                                                      "	FROM users u" +
                                                      "	LEFT JOIN #tempUsers t" +
                                                      "	ON t.user_id = u.id" +
                                                      "	WHERE t.user_id IS NULL" +
                                                      "	AND u.id in (" + usersIds + ")" +
                                                      ") ";
                                }

                                var computersQueryPart = "INSERT INTO #tempComputers (comp_id)(" +
                                                         "	SELECT DISTINCT g.comp_id" +
                                                         "	FROM computers_groups g" +
                                                         "	INNER JOIN #tempGroups t" +
                                                         "	ON t.group_id = g.group_id" +
                                                         ") ";

                                computersQueryPart += "INSERT INTO #tempComputers (comp_id)(" +
                                                      "	SELECT c.Id_comp" +
                                                      "	FROM OU_comp c" +
                                                      "	INNER JOIN #tempOUs o" +
                                                      "	ON o.Id_child = c.Id_OU" +
                                                      "	LEFT JOIN #tempComputers t" +
                                                      "	ON t.comp_id = c.Id_comp" +
                                                      "	WHERE t.comp_id IS NULL" +
                                                      ") ";

                                if (computers.Length > 0)
                                {
                                    var computersIds = string.Join(",", computers);

                                    computersQueryPart += "INSERT INTO #tempComputers (comp_id)(" +
                                                          "	SELECT c.id" +
                                                          "	FROM computers c" +
                                                          "	LEFT JOIN #tempComputers t" +
                                                          "	ON t.comp_id = c.id" +
                                                          "	WHERE t.comp_id IS NULL" +
                                                          "	AND c.id in (" + computersIds + ")" +
                                                          ") ";
                                }

                                sendQuery = sendQuery + ouSqlQueryPart + groupQueryPart + usersQueryPart +
                                            computersQueryPart;
                            }

                            sendQuery += "INSERT INTO alerts_sent_deskbar (alert_id, desk_id)" +
                                         "(" +
                                         "	SELECT " + alertId + ", d.id FROM users_deskbar d INNER JOIN #tempUsers t" +
                                         "	ON d.user_id = t.user_id" +
                                         ") ";

                            sendQuery += "INSERT INTO alerts_sent (alert_id, user_id)" +
                                         "(" +
                                         "	SELECT " + alertId + ", user_id FROM #tempUsers" +
                                         ") ";

                            sendQuery +=
                                "INSERT INTO alerts_sent_stat (alert_id, user_id, date)" +
                                "(" +
                                "	SELECT " + alertId + ", user_id, @now  FROM #tempUsers" +
                                ") ";

                            sendQuery +=
                                "INSERT INTO alerts_sent_group (alert_id, group_id)" +
                                "(" +
                                "	SELECT " + alertId + ", group_id FROM #tempGroups" +
                                ") " +
                                "INSERT INTO alerts_group_stat (alert_id, group_id, date)" +
                                "(" +
                                "	SELECT " + alertId + ", group_id, @now  FROM #tempGroups" +
                                ") ";

                            sendQuery +=
                                "INSERT INTO alerts_sent_comp (alert_id, comp_id)" +
                                "(" +
                                "	SELECT " + alertId + ", comp_id FROM #tempComputers" +
                                ") " +
                                "INSERT INTO alerts_comp_stat (alert_id, comp_id, date)" +
                                "(" +
                                "	SELECT " + alertId + ", comp_id, @now  FROM #tempComputers" +
                                ") ";

                            sendQuery +=
                                "INSERT INTO alerts_sent_ou (alert_id, ou_id)" +
                                "(" +
                                "	SELECT " + alertId + ", Id_child FROM #tempOUs" +
                                ") " +
                                "INSERT INTO alerts_ou_stat (alert_id, ou_id, date)" +
                                "(" +
                                "	SELECT " + alertId + ", Id_child, @now  FROM #tempOUs" +
                                ") ";
                            
                            sendQuery +=
                                " SELECT ui.device_id, ui.device_type, ui.id, ui.user_id FROM user_instances AS ui JOIN (SELECT user_instance_guid as uig FROM #tempComputers AS tc JOIN user_instance_computers AS uic ON tc.comp_id = uic.user_instance_computer_id) AS uig ON ui.clsid = uig.uig" +
                                " UNION" +
                                " SELECT ui.device_id, ui.device_type, ui.id, ui.user_id FROM #tempUsers AS tu JOIN user_instances AS ui ON tu.user_id = ui.user_id" + GetDeviceTypeScript(alertSet) +
                                " DROP TABLE #tempOUs" +
                                " DROP TABLE #tempUsers" +
                                " DROP TABLE #tempComputers" +
                                " DROP TABLE #tempGroups";

                            _sendDataSet = dataBaseManager.GetDataByQuery(sendQuery.Replace("\t", " ")).ToList();
                            dataBaseManager.ExecuteQuery("COMMIT TRANSACTION");

                            if (_sendDataSet == null || !_sendDataSet.Any() && !sendType.Equals("broadcast"))
                            {
                                if (_sendDataSet == null)
                                {
                                    Logger.Error($"Variable \"{nameof(_sendDataSet)}\" is null.");
                                }

                                if (!_sendDataSet.Any())
                                {
                                    Logger.Error($"AddRecipients.aspx.cs. Variable \"{nameof(_sendDataSet)}\" is empty.");
                                }

                                Logger.Error($"AddRecipients.aspx.cs. SQL query \"{nameof(sendQuery)}\" has value: \"{sendQuery}\".");
                                Logger.Error($"AddRecipients.aspx.cs. Variable \"{nameof(alertId)}\" has value: \"{alertId}\".");
                                Logger.Error($"AddRecipients.aspx.cs. Variable \"{nameof(allRecipients)}\" has value: \"{string.Join(",", allRecipients)}\".");
                                Logger.Error($"AddRecipients.aspx.cs. Variable \"{nameof(users)}\" has value: \"{string.Join(",", users)}\".");
                                Logger.Error($"AddRecipients.aspx.cs. Variable \"{nameof(groups)}\" has value: \"{string.Join(",", groups)}\".");
                                Logger.Error($"AddRecipients.aspx.cs. Variable \"{nameof(oUs)}\" has value: \"{string.Join(",", oUs)}\".");
                                Logger.Error($"AddRecipients.aspx.cs. Variable \"{nameof(computers)}\" has value: \"{string.Join(",", computers)}\".");
                            }

                            if (!isAlertEdit && !isInstant && !isRssFeed)
                            {
                                var isScheduled = alertSet.GetInt(0, "schedule") == 1;
                                var isTextToCallAlert = alertSet.GetInt(0, "text_to_call") == 1;
                                var isApproved = alertSet.GetInt(0, "approve_status") == 1 ||
                                                 alertSet.GetInt(0, "approve_status") == 3;
                                var isHaveLifeTime = alertSet.GetInt(0, "lifetime") == 1;
                                var isAlertPostToBlog = alertSet.GetInt(0, "post_to_blog") == 1;
                                var isSocialMedia = alertSet.GetInt(0, "social_media") == 1;
                                var alertTitle = alertSet.GetString(0, "title");
                                var alertBody = alertSet.GetString(0, "alert_text");

                                if (isTextToCallAlert &&
                                    (campaign != null && campaign.Status == CampaignStatus.Active || campaign == null) &&
                                    !isScheduled)
                                {
                                    SendTextToCall(alertBody, sendType, alertId, dataBaseManager, curUser);
                                }

                                if (isHaveLifeTime && isAlertPostToBlog)
                                {
                                    SendToBlog(dataBaseManager, alertTitle, alertBody);
                                }

                                if (isHaveLifeTime && isSocialMedia)
                                {
                                    SendToTwitter(dataBaseManager, alertBody);
                                }

                                // Send push notifications
                                if (!isScheduled && Config.Data.HasModule(Modules.MOBILE)
                                                 && alertSet.GetInt(0, "device") != 1
                                                 && NeedPushForThisAlert(alertSet.First())
                                                 && isApproved
                                                 && (campaign != null && campaign.Status == CampaignStatus.Active ||
                                                     campaign == null))
                                {
                                    if (sendType.Equals("broadcast"))
                                    {
                                        IConfigurationManager configurationManager =
                                            new WebConfigConfigurationManager(Config.Configuration);
                                        IUserRepository userRepository = new PpUserRepository(configurationManager);
                                        var userIds = userRepository.GetAll().Where(x => x.Role == "U").Select(x => x.Id);
                                        PushNotificationsManager.Default.AddPushNotificationToDb(alertId, userIds);
                                    }
                                    else if (sendType.Equals("recipients"))
                                    {
                                        var userIds = _sendDataSet.ToArray().Select(x => (long)x.GetInt("user_id"));
                                        PushNotificationsManager.Default.AddPushNotificationToDb(alertId, userIds);
                                    }
                                }
                            }
                        }

                        if (sendType.Equals("iprange"))
                        {
                            SendAlertToIpRanges(addedRecipients, alertId);
                        }

                        if (!isInstant && !isRssFeed)
                        {
                            var alertsSent = _alertsSentRepository.GetAlertSentsForContent(alertId);
                            var recipientIds = alertsSent.Select(alertSent => alertSent.UserId);

                            _contentReceivedService.CreateRowsInAlertsReceived(sendType, alertId, recipientIds, addedRecipients);
                        }

                        var approvedStatus = alertSet.GetInt(0, "approve_status") == 1 ||
                                             alertSet.GetInt(0, "approve_status") == 3;

                        if ((string.IsNullOrEmpty(edit) || isModerated) && approvedStatus)
                        {
                            if (AppConfiguration.IsNewContentDeliveringScheme)
                            {
                                _cacheInvalidationService.CacheInvalidation(contentType, alertId);
                            }
                            else
                            {
                                _cacheManager.Add(alertId, DateTime.Now.ToString(DateTimeFormat.MsSqlFormat, CultureInfo.InvariantCulture));
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        dataBaseManager.ExecuteQuery("ROLLBACK");
                        Logger.Error(ex);
                    }
                }
            }
        }

        private static string GetDeviceTypeScript(DataSet alertSet)
        {
            var device_type = alertSet.GetInt(0, "device");
            var device_type_script = string.Empty;
            if (device_type == (int)Device.Mobile)
            {
                device_type_script = " and ui.device_type != 0";
            }
            else if (device_type == (int)Device.Desktop)
            {
                device_type_script = " and ui.device_type = 0";
            }

            return device_type_script;
        }

        public void SendToCluster(long alertId)
        {
           
            var servers = Config.Data.GetServerCompanions();
            if (servers.Any())
            {
                var cahgeRegenerateQuery =
                               $"/RegenerateCache.aspx?action=Add&alertId={alertId}&nosend=true";

                foreach (var server in servers)
                {
                    var fullRequestUrl = server + cahgeRegenerateQuery;

                    using (var client = new HttpClient())
                    {
                        var unused = client.GetStringAsync(fullRequestUrl).Result;
                    }
                }
            }
        }

        public static void SendTextToCall(string alertBody, string sendType, int alertId, DBManager _dbMgr, User _curUser)
        {
            var phonesSet = GetEmailAndPhonesSet(sendType, alertId, _dbMgr, _curUser);

            foreach (var textToCallRow in phonesSet)
            {
                var mobilePhone = textToCallRow.GetString("mobile_phone");

                if (!string.IsNullOrEmpty(mobilePhone))
                {
                    var textToCallResult = TextToCallManager.Default.Send(alertId, mobilePhone);
                    if (textToCallResult.Status == CallResource.StatusEnum.Failed)
                    {
                        Logger.Debug($"Alert {alertId} call to user {mobilePhone} failed at {DateTime.Now}");
                    }
                }
            }
        }

        private void SendToBlog(DBManager _dbMgr, string alertTitle, string alertBody)
        {
            Logger.Trace($"Variable: {nameof(_dbMgr)}. Value: {_dbMgr}.");
            Logger.Trace($"Variable: {nameof(alertTitle)}. Value: {alertTitle}.");
            Logger.Trace($"Variable: {nameof(alertBody)}. Value: {alertBody}.");

            //Post to blog
            WordPressManager.Default.NewPost(_dbMgr, alertTitle, alertBody);
        }

        private void SendToTwitter(DBManager _dbMgr, string alertBody)
        {
            Logger.Trace($"Variable: {nameof(_dbMgr)}. Value: {_dbMgr}.");
            Logger.Trace($"Variable: {nameof(alertBody)}. Value: {alertBody}.");

            //Update twitter status
            TwitterManager.Default.UpdateStatus(_dbMgr, alertBody);
        }

        private bool NeedPushForThisAlert(DataRow alertRow)
        {
            Logger.Trace($"Variable: {nameof(alertRow)}. Value: {alertRow}.");

            var alertClass = (AlertType)alertRow.GetInt("class");
            return alertClass == AlertType.SimpleAlert || alertClass == AlertType.RssAlert || alertClass == AlertType.EmergencyAlert
                   || alertClass == AlertType.SimpleSurvey || alertClass == AlertType.SurveyPoll || alertClass == AlertType.SurveyQuiz;
        }

        private IEnumerable<string> GetEndGroups(List<string> groups, int nestingLevel)
        {
            Logger.Trace($"Variable: {nameof(groups)}. Value: {groups}.");
            Logger.Trace($"Variable: {nameof(nestingLevel)}. Value: {nestingLevel}.");

            var resultList = new List<string>();

            foreach (var group in groups)
            {
                var groupParam = SqlParameterFactory.Create(DbType.String, group, "groupId");
                DataSet groupByQuery;

                using (var dbManager = new DBManager())
                {
                    groupByQuery = dbManager.GetDataByQuery(SelectGroupFromGroups, groupParam);
                }
                
                if (groupByQuery.IsEmpty)
                {
                    resultList.Add(group);
                }
                else
                {
                    // 10 - Number on interrations
                    if (nestingLevel < 10)
                    {
                        // Verification for repeated call of the method
                        if (!resultList.Contains(group))
                        {
                            var nestedGroupsArray = groupByQuery.ToList("group_id");
                            var nestedGroups = GetEndGroups(nestedGroupsArray, ++nestingLevel);
                            resultList.AddRange(nestedGroups);
                        }
                    }
                }
            }

            return resultList;
        }

        private static long ConvertToIpRangeNormalNumber(string[] fromIpRange)
        {
            var index = 0;
            long longIpFrom = 0;

            foreach (var rangeStr in fromIpRange)
            {
                var range = Convert.ToInt32(rangeStr);
                longIpFrom += range * (long)Math.Pow(256, 3 - index);
                index++;
            }

            return longIpFrom;
        }

        private static DataSet GetEmailAndPhonesSet(string sendType, int alertId, DBManager _dbMgr, User _curUser)
        {
            var emailsAndPhonesSet = new DataSet();

            if (sendType.Equals("recipients"))
            {
                emailsAndPhonesSet = _dbMgr.GetDataByQuery(
                        "SELECT u.email, u.mobile_phone FROM users  as u INNER JOIN alerts_sent  as a ON u.id = a.user_id WHERE  u.role = 'U' AND a.alert_id = " +
                        alertId);
            }
            else if (sendType.Equals("broadcast"))
            {
                if (_curUser == null)
                {
                    emailsAndPhonesSet = _dbMgr.GetDataByQuery(
                                "SELECT u.email, u.mobile_phone FROM users  as u WHERE role = 'U'");
                }
                else
                {
                    if (_curUser.HasPrivilege)
                    {
                        emailsAndPhonesSet = _dbMgr.GetDataByQuery(
                                "SELECT u.email, u.mobile_phone FROM users  as u WHERE role = 'U'");
                    }
                    else
                    {
                        var query =
                            "SELECT DISTINCT users.id, users.name, users.mobile_phone, users.email FROM users LEFT JOIN policy_user ON users.id=policy_user.user_id LEFT JOIN users_groups ON users.id=users_groups.user_id LEFT JOIN OU_User ON users.id=OU_User.Id_user WHERE role='U' AND ";

                        query += "policy_id=" + _curUser.Policy.Id;

                        if (!_curUser.Policy.CanSendAllOUs)
                        {
                            query += " OR OU_User.Id_OU IN (" +
                                     string.Join(",", _curUser.Policy.OrganizationUnits.ToArray()) + ")";
                        }

                        if (!_curUser.Policy.CanSendAllGroups)
                        {
                            query += " OR  users_groups.group_id IN (" +
                                     string.Join(",", _curUser.Policy.Groups.ToArray()) + ")";
                        }

                        emailsAndPhonesSet =
                            _dbMgr.GetDataByQuery(query);
                    }
                }
            }

            return emailsAndPhonesSet;
        }

        private void SendAlertToIpRanges(string[] ipRanges, int alertId)
        {
            Logger.Trace($"Variable: {nameof(ipRanges)}. Value: {ipRanges}.");
            Logger.Trace($"Variable: {nameof(alertId)}. Value: {alertId}.");

            foreach (var obj in ipRanges)
            {
                var objIdString = obj;

                if (!int.TryParse(objIdString, out _))
                {
                    objIdString = objIdString.Substring(1);
                }

                using (var dbManager = new DBManager())
                {
                    var dataSet = dbManager.GetDataByQuery("SELECT from_ip, to_ip FROM ip_range_groups WHERE group_id=" + objIdString);

                    foreach (var dataRow in dataSet)
                    {
                        var fromIpRange = dataRow.GetString("from_ip").Split('.');
                        var toIpRange = dataRow.GetString("to_ip").Split('.');
                        var longIpFrom = ConvertToIpRangeNormalNumber(fromIpRange);
                        var longIpTo = ConvertToIpRangeNormalNumber(toIpRange);

                        dbManager.ExecuteQuery($"INSERT INTO alerts_sent_iprange (alert_id, ip_range_group_id, ip_from, ip_to) VALUES ( {alertId}, {objIdString}, {longIpFrom}, {longIpTo});");
                    }
                }
            }
            if (AppConfiguration.IsNewContentDeliveringScheme)
            {
                var contentId = (long)alertId;
                var contentType = _alertRepository.GetContentTypeByContentId(contentId);

                _cacheInvalidationService.CacheInvalidation(contentType, contentId);
            }
            else
            {
                _cacheManager.Add(alertId, DateTime.Now.ToString(DateTimeFormat.MsSqlFormat, CultureInfo.InvariantCulture));
            }
        }
    }
}