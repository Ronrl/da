﻿using AutoMapper;
using DeskAlertsDotNet.DataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using DeskAlerts.ApplicationCore.Dtos.AD;

namespace DeskAlertsDotNet.Pages
{
    using Resources;

    public partial class PolicyOUs : DeskAlertsBasePage
    {
        private List<OUDto> _ouSet = new List<OUDto>();

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            BuildOrganizationTree();

            orgTree.Attributes.Add("onClick", "handleTreeClick(event)");
        }

        void BuildOrganizationTree()
        {
            TreeNode root = new TreeNode(resources.LNG_ORGANIZATION);

            root.Selected = true;
            root.Expanded = true;
            root.ImageUrl = "images/ad_icons/organization.png";

            BuildOrganizationTree(root);
            orgTree.Nodes.Add(root);
        }

        private void BuildOrganizationTree(TreeNode root)
        {
            orgTree.Nodes.Clear();
            var domainsSet = dbMgr.GetDataByQuery("SELECT id, name FROM domains WHERE name <> ''");

            foreach (var domainRow in domainsSet)
            {
                var domainId = domainRow.GetInt("id");
                var domainName = domainRow.GetString("name");

                var domainNode = new TreeNode(domainName)
                {
                    Expanded = false,
                    ImageUrl = "images/ad_icons/domain.png",
                    Target = "objectsFrame"
                };

                root.ChildNodes.Add(domainNode);

                var getOUsByDomainIdQuery = $"SELECT ou.Id as OU_id, ou.Name, h.Id_parent FROM OU INNER JOIN OU_hierarchy as h ON h.Id_child = ou.Id WHERE ou.Id_domain = {domainId} ORDER BY ou.Name ASC";

                dbMgr.GetDataByQuery(getOUsByDomainIdQuery).ToList().ForEach(el =>
                {
                    _ouSet.Add(Mapper.Map<DataRow, OUDto>(el));
                });

                AddOuToTree(domainNode, -1);
            }
        }

        private void AddOuToTree(TreeNode parentNode, int parentId)
        {
            var ouSet = _ouSet?.Where(ou => ou.ParentId == parentId).ToList();

            parentNode.SelectAction = TreeNodeSelectAction.Select;

            if (ouSet != null)
            {
                foreach (var ouRow in ouSet)
                {
                    var ouName = ouRow.Name;
                    var ouId = ouRow.OuId;

                    var ouNode = new TreeNode(ouName)
                    {
                        Expanded = false,
                        ShowCheckBox = true,
                        Value = ouId.ToString(),
                        ImageUrl = "images/ad_icons/ou.png",
                        Target = "objectsFrame"
                    };

                    parentNode.ChildNodes.Add(ouNode);
                    AddOuToTree(ouNode, ouId);
                }
            }
        }
    }
}