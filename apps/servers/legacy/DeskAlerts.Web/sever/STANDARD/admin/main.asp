﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<!-- #include file="demo.inc" -->
<!-- #include file="functions_inc.asp" -->

<%
check_session()

mobile_device=Session("isMobile")
simple_mobile=Session("isSimpleMobile")
uid = Session("uid")

'Set lic = Server.CreateObject("DeskAlertsServer.Licensing")
'if(Err.Number <> 0) then
'	dllErrorMessage = LNG_ERROR_SERVER_DLL_NOT_REGISTERED
'else
'	trial = lic.getTrialTime(Conn)
'	elipse = lic.getTrialExpire(Conn)
'end if
'if trial > 0 and elipse > -1 then
'	trial = Clng(trial / 60 / 60 / 24)
'	elipse = Clng(elipse / 60 / 60 / 24)
'end if

trialSet = Conn.Execute("SELECT val as count FROM [settings] WHERE name='supermario'")
trial = clng(trialSet("count"))

elipseSet = Conn.Execute("SELECT DATEDIFF(s, [reg_date], GETDATE()) as lifetime FROM [users] WITH (READPAST) WHERE [name] = N'admin' AND [role] = 'A'")
elipse = elipseSet("lifetime")

SET RC = Conn.Execute("SELECT paramValue FROM [parameters] WHERE paramName='helpStatus'")
if(Not RC.EOF) then  
    helpStatus = RC("paramValue")
end if
RC.Close

SET RC = Conn.Execute("SELECT paramValue FROM [parameters] WHERE paramName='licensesReminderCriticalPoint'")
if(Not RC.EOF) then  
    licensesReminderCriticalPoint = RC("paramValue")
end if
RC.Close
%>
<!DOCTYPE html>
<html>
<head>
<title>DeskAlerts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
<link href="css/style.css" rel="stylesheet" type="text/css">
<link href="css/MetroJs.lt.min.css" rel="stylesheet" type="text/css" />
<script language="javascript" type="text/javascript" src="jscripts/json2.js"></script>
<script language="javascript" type="text/javascript" src="functions.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>  
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.cookies.2.2.0.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/MetroJs.lt.min.js"></script>
   
<style>
html, body {
	border: 0px;
	overflow: auto;
	height : 100%;
	padding:0px;
	margin:0px;
}

table{
	border-spacing:0px;
}
</style>
	
<script type="text/javascript">
    var helpStatus = "<%=helpStatus%>";
	var topFrame;
	//$("#settingsDialog").load("SettingsSelection.aspx");
	function expandHelp(){
	    $('#helpDiv').show();
	    /*$(helpOpener).hide();*/
	    help_close_span = topFrame.contentWindow.document.getElementById("help_close_span");
	    $(help_close_span).show();
	    setTimeout(function(){
	        $('#help_frame').css("height","50%");
	        setTimeout(function(){
	            $('#help_frame').css("height","100%");
	        },100);
	    },100);

	    helpOpenerIsHidden = true;
	    helpStatus = 1;
	    $.get("set_parameters.asp?name=setHelpStatus&helpStatus=1");
	};
	
	$(document).ready(function(){  
	    $("#send_btn").button({label:"<%=LNG_SEND %>",icons: {primary: "ui-icon-newwin"}}).click(function(){ sendEmail(); });
	});
			
	function sendEmail(){		
	    var email = $("#email_box").val();		
	    var comment = $("#comment").val();
	    var arr = JSON.parse(email);

	    if(email.length == 0 ||  email.indexOf("@") == -1 ||  email.indexOf(".") == -1){
	        alert('<% =LNG_WRONG_EMAIL%>');
	    }else{
	        $.get("mail_sender.asp?email="+arr + "&comment=" + comment, function(data)
	        {
	            alert(data);
	            $('#email_box').text("");
	            $('#email_box').val("");
	            $("#invite_dialog_form").dialog("close");
	        });
	    };	
	};//sendEmail ends

	$(function() {
	    //To render the input device to multiple email input using BootStrap icon
	    //$('#email_box').multiple_emails("Basic");	
	    $('#email_box').text("");
	    $('#email_box').val("");
	});	

	function minimizeHelp(){
	    $('#helpDiv').hide();
	    /*$(helpOpener).show();*/
	    help_close_span = topFrame.contentWindow.document.getElementById("help_close_span");
	    $(help_close_span).hide();
	    helpOpenerIsHidden = false;
	    helpStatus = 0;
	    $.get("set_parameters.asp?name=setHelpStatus&helpStatus=0");
	};
	
	function toggleHelp(){
	    if(helpOpenerIsHidden){
	        minimizeHelp();
	    }else{
	        expandHelp();
	    };
	};

	function toggleSettings(){
	    if(!settingsOpened){
	        //   $("#settingsFrame").attr("src", "SettingsSelection.aspx");   
	        settingsOpened = true;
	            
	        $("#settingsDialog").dialog({
	            autoOpen: false,
	            height: 330,
	            width: 1010,
	            modal: true,
	            resizable: false,
	            close: function( event, ui ) { settingsOpened = false; }
	        }).dialog('open');
	    }else{
	        settingsOpened = false;
	        $("#settingsDialog").dialog('close');
	        // $("settingsFrame").attr("src", "");
	    };  
	};
	
	var helpOpener;
	var firstTime = true;
	var helpOpenerIsHidden = true;
	var settingsOpened = false;

	function initHelp(){
	    var showHelp = '<%=ConfEnableHelp%>';

	    if(showHelp != '1'){
	        $("#helpDiv").hide();
	        $(helpOpener).hide();
	        helpOpenerIsHidden = true;
	        return;
	    };

	    if (firstTime){
	        if (helpStatus == 1){
	            expandHelp();
	        }else{
	            minimizeHelp();
	        };

	        firstTime = false;
	    };

	    var main_target = $("#main_frame").get(0).contentWindow.location.pathname.split('/').pop();
	    var search = $("#main_frame").get(0).contentWindow.location.search;
	    var about = pageTheme(main_target,search);

	    if (main_target != "settings_social_settings.asp" && main_target != "settings_common.asp" && main_target != "contact_deskalerts.asp"){
	        $('#helpIframe').attr('src','help.asp?about='+about); 
	    };
	};

	function resizeMainFrame(){};

	function showLoader(){
	    $("#loader").fadeIn(500);
	    $("#shadow").fadeIn(500);
	};

	function hideLoader(){
	    $("#shadow").stop();
	    $("#shadow").fadeOut(500);
	    $("#loader").stop();
	    $("#loader").fadeOut(500);
	};

	function showLoaderByClick(e){
	    if (!e.shiftKey){
	        showLoader();
	    };
	};

	function bindEvents(doc){
	    var $elements = $("*[target='work_area']",doc);

	    $elements.unbind('click', showLoaderByClick)
	    $elements.bind('click', showLoaderByClick);
	};

	$(document).keyup(function(event){
		if (event.keyCode == 27) hideLoader();
	});

	$(function(){    
        /*$("#RejectedAlertsDialog").dialog(
        {
            autoOpen: false,
		    height: 180,
		    width: 450,
		    modal: true          
        });*/
        
        $("#NotApprovedAlertsDialog").dialog(
        {
            autoOpen: false,
		    height: 180,
		    width: 450,
		    modal: true          
        });

        //$("#settingsDialog").dialog(
        //{
        //    autoOpen: false,
        //    height: 280,
        //    width: 450,
        //    modal: true          
        //});
		
		$( "#TrialAddonsDialog" ).dialog({
		    autoOpen: false,
		    height: 180,
		    width: 450,
		    modal: true,
		    draggable: false,
		    resizable: false,
		    close: function(){
		        if ($("#dontshow").is(':checked')){
		            $.get("set_parameters.asp?name=setTrialAddonsStatus&dontShow=1");
		        };
		    }
		});

		$("#getQuoteFromDialog").button().click(function(){
			$("#trial_dialog").dialog("close");
		});

		$("#quick_tour").load('quick_tour.html').dialog({
		    modal:true,
		    width: 630,
            height: 470,
            autoOpen: false,
		    resizable: false,
		    close: function(){
		        if ($("#dontshowTour").is(':checked')){
		            $.get("set_parameters.asp?name=setTourStatus&dontShow=1");
		        };
		    }	
		}); 

		$("#trial_dialog").dialog({
		    modal:true,
		    width: 630,
            height: 220,
            autoOpen: false,
		    resizable: false,
		    open: function(){
			    $(".hideMe").hide();
		    },
		    close: function() {
		        if ($("#dontshowToday").is(':checked')){
		            $.get("set_parameters.asp?name=setTrialDialogStatus&dontShowToday=1");
		        };
		    }	
		}); 

		$.ajax({
		    url: 'show_maintenance_rem.asp',
		    success: function(data){ 
		        var days = data;
		        
		        $.get('set_parameters.asp?name=getMaintenanceCheckbox', function(data){
		            var maintanance_cookie = data;
		            
		            if ((maintanance_cookie == "0")||(maintanance_cookie == "")) {
		                //if(data.isBetween(61,90))
		                if(days < 90){
		                    $("#maintenance_reminder").dialog("open");
		                    //$("#quick_tour").dialog("open");
		                }else if(data < 0){
		                    $("#maintenance_reminder").dialog("open");
		                };
		            };
		        });
		    }
		});

		function checkLicensesUsed(currentLicensesUsed, licensesAmount, notificationProcentMark){
		    var critacalPoint = 100 - (currentLicensesUsed * 100) / licensesAmount;

		    if ((critacalPoint <= notificationProcentMark) && (notificationProcentMark != -1))
		    {
		        $("#licenses_reminder").dialog("open");
		    }
		    console.log(currentLicensesUsed + licensesAmount + notificationProcentMark + critacalPoint);
		}

		$("#maintenance_reminder").load('reminder.asp?action=alert').dialog({
			autoOpen: false,
			height: 610,
			width: 800,
			draggable: false,
			resizable: false,
			modal: true,
			beforeClose: function(event, ui){
			    if($("#maintenance_checkbox").is(":checked")){
			        $.ajax({
			            url: 'show_maintenance_rem.asp',
			            success: function(data){ 
			                var result = data;
			                if(data>=61 && data<=90){
			                    result=data-61;
			                }else if(data>=31 && data<=60){
			                    result=data-31;
			                }else if(data<=30){
			                    result=7;
			                };

			                var today = new Date();
			                var period = new Date();
			                //var today2 = new Date();
			                period.setDate(today.getDate()+result);
			                //result=today+result;
			                //alert(period);
			                $.get("set_parameters.asp?name=setMaintenanceCheckbox&maintenanceCheckbox=1");
			                $.get("set_parameters.asp?name=setExpires&expires="+period);
			            }
			        });
			    };
		    /*$(this).dialog("maintenance_reminder").effect("fade", {
            
                to: "#maintenance_reminder_button", 
                className: "ui-effects-transfer"
            
            }, 500);*/
			/*var $jParent=window.parent.top.jQuery.noConflict();
			var dlg1 = $jParent('#maintenance_reminder_button');
			alert(dlg1);*/
					
					//$jParent('#maintenance_reminder_button')	
			},
			close:	
			{
				effect: "fade",
				duration: 1000
			/*function() { 
					var $jParent = window.parent.parent.top.jQuery.noConflict();
					var dlg1 = $jParent('#maintenance_reminder_button');
					dlg1.fadeIn(1000);*/
					
					//dlg1.button( "option", "label", "123123123" );
					//dlg1.css('color','green');
					//var $jParent = window.parent.top.jQuery.noConflict();
					//var dlg1 = $jParent('#maintenance_reminder_button');
					//dlg1.fadeIn(100).fadeOut(100).fadeIn(100).fadeOut(100);
					//$jParent('#maintenance_reminder_button')
			},
			show:
			{
				effect: "fade",
				duration: 300
			}
		});//maintenance_reminder ends
		//}});
		/*$("maintenance_reminder").dialog({
		modal:true,
		width:630,
		height:470,
		autoOpen:false,
		resizable:false,
		close: 0
		}); */
		
		/*$("#maintenance_reminder").load('quick_tour.html').dialog({
		modal:true,
		width: 630,
        height: 470,
        autoOpen: false,
		resizable: false,
		close: function() {
			if ($("#dontshowTour").is(':checked')){
				dontShowTour = 1;
				cookieObj.dontShowTour = dontShowTour;
				var cookieStr = JSON.stringify(cookieObj);
				$.cookies.set('deskalerts', cookieStr);
			}
		}	
		}); */
		
		$('#helpCaption').button({ icons: { secondary: "ui-icon-triangle-1-e"} });

		$.grep($(window.top.document).find("frame"), function(n, i) {
		    if ($(n).attr("src") == "menu.asp") {
		        var doc = $(n)[0].contentDocument || $(n)[0].contentWindow.document;

		        if (doc && doc.location != "about:blank" && doc.readyState == 'complete') {
		            bindEvents(doc);
		        }else{
		            $(n).bind("load", function() {
		                var doc = $(n)[0].contentDocument || $(n)[0].contentWindow.document;

		                bindEvents(doc);
		            });
		        };
		    };
		});

		var bindContentEvents = function(){
		    var bindContentEventsToIframe = function(frame) {
		        var doc;

		        try {
		            doc = frame.contentWindow.contentDocument;
		        } catch (e) { }

		        if (doc && doc.location) {
		            var loc = (doc.location.href + '').split('#')[0];

		            $('a', doc).click(function(e) {
		                if (this.href &&
							this.href.indexOf('#') != 0 &&
							this.href.indexOf(loc + '#') != 0 &&
							this.href.indexOf('javascript:') == -1 &&
							this.href.indexOf('mailto:') == -1 &&
							!this.onclick &&
							(!this.target || this.target == '_self') &&
							!e.shiftKey){
		                    showLoader();
		                };
		            });

		            $('form', doc).each(function(i, el) {
		                el.old_submit = el.submit;
		                el.submit = function() {
		                    showLoader();
		                    el.old_submit();
		                }
		            });

		            $("input[type='submit']", doc).each(function(i, el) {
		                el.old_onclick = el.onclick;
		                if (el.old_onclick) {
		                    el.onclick = function() {
		                        if (el.old_onclick()) {
		                            showLoader();
		                            return true;
		                        }
		                        return false;
		                    }
		                }
		            });

		            $('iframe', doc).each(function() {
		                bindContentEventsToIframe(this);
		            });

		            $('iframe', doc).load(function() {
		                bindContentEventsToIframe(this);
		                hideLoader();
		            });
		        }
		    };//bindContentEventsToIframe ends

		    bindContentEventsToIframe($("#main_frame")[0]);
		};//bindContentEvents ends

		var doc = $("#main_frame").contents()[0];

		if (doc && doc.location != "about:blank" && doc.readyState == 'complete') {
		    initHelp();
		    bindContentEvents();
		    hideLoader();
		};

		$("#main_frame").bind("load", function() {
			initHelp();
			bindContentEvents();
			hideLoader();

			$("#licenses_reminder").load('reminder.asp?action=licens').dialog({
			    autoOpen: false,
			    height: 260,
			    width: 900,
			    draggable: true,
			    resizable: false,
			    modal: true, 
			    beforeClose: function(event, ui){
			        var userChoosenLicensesReminderCriticalPoint;

			        if($("#licenses_reminder_checkbox").is(":checked")){
			            userChoosenLicensesReminderCriticalPoint = -1;
			        } else {
			            userChoosenLicensesReminderCriticalPoint = $("#licensesReminderCriticalPointSelect").val();
			        }
			        
			        $.get("set_parameters.asp?name=setLicensesReminderCriticalPoint&licensesReminderCriticalPoint=" + userChoosenLicensesReminderCriticalPoint); 

			        $("#userChoosenLicensesReminderCriticalPoint").val(userChoosenLicensesReminderCriticalPoint);
			    }
			});

			var counter = ($("#counter", top.frames["menu"].document).text());
			var currentLicensesUsed = counter.split("/")[0];
			var licensesAmount = counter.split("/")[1];
			var userChoosenLicensesReminderCriticalPoint = $("#userChoosenLicensesReminderCriticalPoint").val();
			var licensesReminderCriticalPoint = <%=licensesReminderCriticalPoint%>;

			if (userChoosenLicensesReminderCriticalPoint == "0") {
			    checkLicensesUsed(currentLicensesUsed, licensesAmount, licensesReminderCriticalPoint);
		    }else{
			    checkLicensesUsed(currentLicensesUsed, licensesAmount, userChoosenLicensesReminderCriticalPoint);
			}
		});

		$('#helpCaption').click(minimizeHelp);

<% if mobile_device<>true then %>
		topFrame = top.document.getElementById("top");
<% else %>		 
		topFrame = parent.document.getElementById("menu_frame"); 
<% end if 
   if simple_mobile<>true then %>    
    	$(topFrame).load(function() {
			inviteButton = topFrame.contentWindow.document.getElementById("invite");

			$(inviteButton).click( function(){
			    $("#invite_dialog_form").dialog({
				    autoOpen: false,
					height: 600,
					height: 600,
					width: 600,
					modal: true,
					resizable: false
			    });
								
			    $("#invite_dialog_form").dialog("open");
			});

			helpOpener = topFrame.contentWindow.document.getElementById("helpOpener");

		//	var settingsButton = topFrame.contentWindow.document.getElementById("settingsButton");

			$(helpOpener).click(toggleHelp);

			//$(settingsButton).click(function()
			//{
			//    $("#settingsDialog").dialog(            
            //        {
            //            autoOpen: false,
            //            height: 330,
            //            width: 1010,
            //            modal: true,
            //            resizable: false,
            //            close: function( event, ui ) { settingsOpened = false; }

            //        }).dialog('open');
	        
			//});
			/*if (helpOpenerIsHidden)
				$(helpOpener).hide();
			else
				$(helpOpener).show();*/
    	});

		if (topFrame && topFrame.contentWindow.document.readyState == 'complete') {
			setTimeout(function() {
				helpOpener = topFrame.contentWindow.document.getElementById("helpOpener");
				$(helpOpener).click(toggleHelp);
				/*if (helpOpenerIsHidden)
					$(helpOpener).hide();
				else
					$(helpOpener).show();*/
			}, 100);
		};
    <% else %>
			helpOpener = parent.document.getElementById("helpOpener");
			$(helpOpener).click(toggleHelp);
			/*if (helpOpenerIsHidden)
		        $(helpOpener).hide();
		    else
			    $(helpOpener).show();*/
			<% end if %>			
        <% if TRIAL_DAYS_LEFT >=0 then %>
			$.get("set_parameters.asp?name=setTrialAddonsStatus=0");
			<%if TRIAL_DAYS_LEFT>7 then %> 
                $.get("set_parameters.asp?name=getTrialAddonsStatus", function(data){
                    if(data == 0){
                        $( "#TrialAddonsDialog" ).dialog( "open" );
                    }
                });
			<%else %>
				$.get("set_parameters.asp?name=getTrialAddonsStatus", function(data){
				    if(data < 2){
				        $( "#TrialAddonsDialog" ).dialog( "open" );
				    }
				});
			<%end if %>
		<% end if %>	
		
		<% if CLng(getNotApprovedAlertsForUser(uid)) > 0 then %>
		
		var approveDialog =   $( "#NotApprovedAlertsDialog" ).dialog({
            autoOpen: false,
		    height: 180,
		    width: 450,
		    modal: true           
		}).dialog( "open" );

	    $("#notApprovedButton").button().click(function(){
		        //	
		        //	location.href = "approve_panel.aspx?uid=<%=uid %>" ;
		    window.top.frames["main"].frames["work_area"].location.href = "ApprovePanel.aspx" ;
		    approveDialog.dialog("close");
		});
		
		<% end if %>
		
		<% if CLng(getRejectedAlertsCountForUser(uid)) > 0 then %>
	    var rejectedDialog =  $( "#RejectedAlertsDialog" ).dialog({
            autoOpen: false,
		    height: 180,
		    width: 450,
		    modal: true          
	    }).dialog( "open" );

		$("#rejectedAlertsButton").button().click(function(){
		    //location.href = "RejectedAlerts.aspx?uid=<%=uid %>" ;
			window.top.frames["main"].frames["work_area"].location.href = "RejectedAlerts.aspx?uid=<%=uid%>" ;
			rejectedDialog.dialog("close");
        });
		<% end if %>
		
		<%if DEMO=1 then %>
			$.get("set_parameters.asp?name=getShowTourStatus", function(data){
			    if (data!=1){
			        $("#quick_tour").dialog("open");
			    }
			});
		<% end if %>    
		<%if trial>0 then %>
			$.get("set_parameters.asp?name=getDontShowTodayStatus", function(data){
			   
			    if (data != 1){
		            $("#trial_dialog").dialog("open");
			    };
		    });
		<% end if %>    
	});
</script>
</head>
<body scroll="no" style="overflow:hidden" class="loader_body">
    <div id="RejectedAlertsDialog" style="display:none;text-align:center">
	    <br><br>
	    <% rejectedCount = "<b style='color:red'>" & getRejectedAlertsCountForUser(uid) & "</b>"%>
	    <label style="font-size:14px"><b><% =Replace(LNG_YOU_HAVE_REJECTED_ALERTS, "{n}", rejectedCount) %></b></label>
        <br><br><br><br>
	    <a class="contact_us_button" target="work_area" id="rejectedAlertsButton" ><% =LNG_SHOW_REJECTED %></a>
	</div>
    <div id="NotApprovedAlertsDialog" style="display:none;text-align:center">
	    <br><br>
	    <% notApprovedCount = "<b style='color:red'>" & getNotApprovedAlertsForUser(uid) & "</b>"%>
	    <label style="font-size:14px"><b><% =Replace(LNG_YOU_HAVE_NOT_APPROVED, "{n}", notApprovedCount) %></b></label>
        <br><br><br><br>
	    <a  class="contact_us_button" target="work_area" id="notApprovedButton"><% =LNG_SHOW_NOT_APPROVED %></a>
	</div>
    <div id="settingsDialog" style="display:none;text-align:center;overflow:hidden">
       <!-- <iframe src="" id="settingsFrame" border="0"  style="height:400px;width:100%;border:0;overflow:hidden;margin-left:60px"></iframe> -->
    </div>
	<table border="0" width="100%" height="100%">
	<tr style="height:100%">
		<td style="width:100%;height:100%;padding:0px">
			<iframe id="main_frame" frameborder="0" src="<%= Request.QueryString("url") %>" name="work_area" style="width:100%; height:100%"></iframe>
		</td>
		<td id="help_frame" style="padding:0px">
			<div id="helpDiv" class="helpDiv" style="display:none">
				<iframe id="helpIframe" name="helpIframe" class="helpIframe" src="help.asp" frameborder="0">
				</iframe>
			</div>
		</td>
	</tr>
	</table>
	<div id="TrialAddonsDialog" style="display:none">
		<%if TRIAL_DAYS_LEFT>7 then%>
			<p style="font-size:14px"><%=LNG_TRIAL_ADDONS_DIALOG_1 %></p>
			<input type="hidden" id="dontShowVal" value="1" />
		<%elseif TRIAL_DAYS_LEFT>=0 then%>
			<p style="font-size:14px"><%=LNG_TRIAL_ADDONS_DIALOG_2 %></p><p style="color:red"><%=LNG_TRIAL_ADDONS_3 %><%=TRIAL_DAYS_LEFT %></p>
			<input type="hidden" id="dontShowVal" value="2" />
		<%end if %>
		<form>
			<input type="checkbox" name="dontshow" id="dontshow" value="1"/>
			<label for="dontshow"><%=LNG_DONT_SHOW_AGAIN %></label>
		</form>
	</div>
	
	<div id="quick_tour" style="z-index:999;display:none"></div>
	
	<div id="maintenance_reminder" style='z-index:999;display:none;' title='Maintenance Expiration Reminder'></div>

    <div id="licenses_reminder" style='z-index:999;display:none;' title='Licenses low reminder'></div>

	<div id="trial_dialog" style="z-index:999;display:none">
		<div style="text-align:center">
		<%
			if trial>=elipse then

		%>
			<p>You're using trial version of DeskAlerts. Trial period will expire in <span style="font-weight:bold;color:red"><%=Clng(trial-(elipse/60/60/24)) %></span> days.</p>
			<p>See full list of features and usage instructions in DeskAlerts manuals:</p>
			<p>
				<a class="hideMe" href="#">:)</a> <!--jquery UI autofocus hack-->
				<a href="http://www.alert-software.com/files/DeskAlerts_Administrators_Manual.pdf" target="_blank"> Admin manual</a> 
				<a href="http://www.alert-software.com/files/DeskAlerts_Editors_Manual.pdf" target="_blank"> Editor manual</a>
				<a href="http://alert-software.com/wp-content/uploads/2014/12/DeskAlerts_Client_User_Manual.pdf"  target="_blank"> Client manual</a>
			</p>
		<%
			else
		%>
			<a class="hideMe" href="#">:)</a> <!--jquery UI autofocus hack-->
			<p>Your trial period has expired. Please contact <a href="mailto:sales@deskalerts.com?Subject=Trial has expired">sales@deskalerts.com</a> about your trial testing results, or use the button below to compose a quote request.</p>
		<%
			end if
		%>
		</div>
		<div style="text-align:center">
			<a href="request_quote.asp" class="contact_us_button" target="work_area" id="getQuoteFromDialog">Get Free Quote</a>
		</div>
		<form action="#" id="dialog_form">
			<input style="position:absolute;left:10px;bottom:20px;" type="checkbox" name="dontshowToday" id="dontshowToday" value="1"/>
			<label for="dontshowToday" style="position:absolute;bottom:0px;padding: 20px 0px 20px 20px;">Do not show again today</label>
		</form>
	</div>
	<div id="shadow" style="position:absolute; width:100%; height:100%; background: grey; opacity:0.4; filter:alpha(opacity=40);">
	</div>
	<div id="loader" style="font-size:1px; background: url(images/loader.gif); height:15px; width:128px; position:absolute; left:50%; margin-left:-64px; top:50%; margin-top:-7px; z-index:998">
	</div>
</body>
</html>
<!-- #include file="db_conn_close.asp" -->