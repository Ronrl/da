<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<%
check_session()
%>
<!-- #include file="Common.asp" -->
<!-- #include file="header.asp" -->
<!-- #include file="footer.asp" -->

<%
' index CustomIncludes End
'-------------------------------

'===============================
' Save Page and File Name available into variables
'-------------------------------
Dim sFileName
Dim sTemplateFileName
Dim sPaginateFileName
Dim intSessionUserId

if(DA=1) then
	intSessionUserId = Session("uid")
else
	intSessionUserId = Session("intUserId")
end if


sFileName = "view_tb_stat_detailed_engine.asp"
sTemplateFileName = "view_tb_stat_detailed_engine.html"
sPaginateFileName = "paginate.html"
'===============================


if (intSessionUserId <> "") then


' index Show begin

'===============================
' Display page
'-------------------------------
' Load HTML template for this page
'-------------------------------
LoadTemplate sAppPath & sTemplateFileName, "main"

'-------------------------------
' Load HTML template of Header and Footer
'-------------------------------
LoadTemplate sHeaderFileName, "Header"
LoadTemplate sFooterFileName, "Footer"
'-------------------------------
intToolbarId=request("toolbar_id")
strDate=request("date")

SetVar "strDate", strDate

	Set rsToolbar = Conn.Execute("SELECT name FROM [toolbars] WHERE id=" & intToolbarId)
	if not rsToolbar.EOF then
		strToolbarName=rsToolbar("name")
	end if


eng_id=Request("eng_id")
strDate=Request("date")


	Set rsEngine = Conn.Execute("SELECT * FROM [urls] WHERE id="&eng_id)
	if(Not rsEngine.EOF) then
		strUrl=rsEngine("url")
		PageName("Search Engine: "&strUrl)
		Set rsUrl = Conn.Execute("SELECT * FROM [urls] WHERE toolbar_id=" & intToolbarId & " AND search=1 AND url='"&strUrl&"'")
		do while not rsUrl.EOF
	       	intClickNum=0
	  	Set rsUrlStat = Conn.Execute("SELECT * FROM [url_stat] WHERE url_id=" & rsUrl("id") & " AND toolbar_id=" & intToolbarId & " AND date= '" & strDate & "'")
		  do while not rsUrlStat.EOF
			intClickNum=intClickNum+1
			rsUrlStat.MoveNext	
		  loop
		  if(intClickNum<>0) then
			SetVar "strSearchCount", intClickNum
			SetVar "strEngine", rsUrl("url")
			SetVar "strSearch", rsUrl("keywords")
			Parse "DListSearchesStat", True
			strNewEngine=""
		  end if
		rsUrl.MoveNext	
		loop
		rsUrl.Close
		rsEngine.Close
	end if





'-------------------------------
' Step through each form
'-------------------------------
Header_Show
Footer_Show
'-------------------------------
' Process page templates
'-------------------------------
Parse "Header", False
Parse "Footer", False
Parse "main", False
'-------------------------------
' Output the page to the browser
'-------------------------------
Response.write PrintVar("main")

' index Show End

'-------------------------------
' Destroy all object variables
'-------------------------------

' index Close Event begin
' index Close Event End

UnloadTemplate
'===============================

else

Login(Request("intErrorCode"))
'response.write "test111222"

end if

'===============================
' Display Grid Form
'-------------------------------
%>
<!-- #include file="db_conn_close.asp" -->