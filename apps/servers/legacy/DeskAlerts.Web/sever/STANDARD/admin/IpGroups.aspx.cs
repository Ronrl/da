﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Models.Permission;
using Resources;

namespace DeskAlertsDotNet.Pages
{
    public partial class IpGroups : DeskAlertsBaseListPage
    {

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            headerTitle.Text = resources.LNG_IP_GROUPS;
            searchButton.Text = searchButton.Text.Replace("{Search}", resources.LNG_SEARCH);
            groupNameCell.Text = GetSortLink(resources.LNG_NAME, "name", Request["sortBy"]);
            actionsCell.Text = resources.LNG_ACTIONS;
            Table = contentTable;

            FillTableWithContent(true);

            bottomDelete.Visible = curUser.HasPrivilege || curUser.Policy[Policies.IP_GROUPS].CanDelete;
            upDelete.Visible = curUser.HasPrivilege || curUser.Policy[Policies.IP_GROUPS].CanDelete;
            addButton.Visible = curUser.HasPrivilege || curUser.Policy[Policies.IP_GROUPS].CanCreate;

            if (!curUser.HasPrivilege && !curUser.Policy[Policies.IP_GROUPS].CanEdit)
            {
                actionsCell.Visible = false;
            }
        }

        #region DataProperties
        protected override string DataTable => "ip_groups";

        protected override string[] Collumns => new[] { "name" };

        protected override string DefaultOrderCollumn => "name";

        protected override System.Web.UI.HtmlControls.HtmlInputText SearchControl => searchTermBox;

        protected override string SearchField
        {
            get => "name";
            set => base.SearchField = value;
        }

        #endregion

        #region Interface properties

        protected override IButtonControl[] DeleteButtons => new IButtonControl[] { upDelete, bottomDelete };

        protected override string CustomQuery

        {
            get
            {
                var query = "SELECT id, name FROM ip_groups";

                if (SearchTerm.Length > 0)
                {
                    query += " WHERE name LIKE '%" + SearchTerm + "%'";
                }
                if (!string.IsNullOrEmpty(Request["sortBy"]))
                {
                    query += " ORDER BY " + Request["sortBy"];
                }
                return query;
            }
        }

        protected override string CustomCountQuery
        {
            get
            {
                var query = "SELECT count(1) FROM ip_groups";

                if (SearchTerm.Length > 0)
                {
                    query += " WHERE name LIKE '%" + SearchTerm + "%'";
                }
                    
                return query;
            }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl NoRowsDiv => NoRows;

        protected override System.Web.UI.HtmlControls.HtmlGenericControl TableDiv => mainTableDiv;

        protected override string ContentName => resources.LNG_GROUPS;

        protected override System.Web.UI.HtmlControls.HtmlGenericControl BottomPaging => bottomRanging;

        protected override System.Web.UI.HtmlControls.HtmlGenericControl TopPaging => topPaging;

        protected override string FormatFieldData(string fieldKey, string fieldValue, DataRow row)
        {
            return fieldKey.Equals("name") ? 
                $"<a onclick=\"return window.open('ViewIpGroup.aspx?id={row.GetString("id")}','',350,350);\" class=\"ipGroupsLinks\">{fieldValue}</a>" : 
                base.FormatFieldData(fieldKey, fieldValue, row);
        }

        protected override List<Control> GetActionControls(DataRow row)
        {
            var controls = new List<Control>();

            var editButton = GetActionButton(row.GetString("id"), "images/action_icons/edit.png", resources.LNG_EDIT_IP_GROUPS,
                $"location.href='EditIpGroups.aspx?id={row.GetString("id")}'");

            if (curUser.HasPrivilege || curUser.Policy[Policies.IP_GROUPS].CanEdit)
            {
                controls.Add(editButton);
            }

            return controls;
        }

        #endregion
    }
}