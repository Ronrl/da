﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditGroups.aspx.cs" Inherits="DeskAlertsDotNet.Pages.EditGroups" %>
<%@ Import NameSpace="DeskAlertsDotNet.Parameters" %>
<%@ Import NameSpace="Resources" %>

<!DOCTYPE html>
<html>
<head runat="server">
<title>DeskAlerts</title>
<meta http-equiv="Content-Type" content="text/html" charset=utf-8">
<link href="css/style9.css" rel="stylesheet" type="text/css">
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script language="javascript" type="text/javascript">
$(document).ready(function () {
    $(".add_button").button();
    $(".save_button").button();
    $(".cancel_button").button();
});
</script>
</head>
<body>
<form id="form1" runat="server">
    <table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	    <tr>
	        <td>
	            <table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
		            <tr>
		                <td width=100% height=31 class="main_table_title"><a href="EditGroups.aspx" class="header_title"><%=resources.LNG_GROUPS %></a></td>
		            </tr>
		            <tr>
		                <td class="main_table_body" height="100%">
		                    <div style="margin:10px;">
		                        <span runat="server" id="workHeader" class="work_header"></span>
		                        <br>
                                <br>
                                <div><%=resources.LNG_STEP %> <strong>1</strong> <%=resources.LNG_OF %> 2: <%=resources.LNG_ENTER_NAME %>:</div>
                                <input type="hidden" name="sub1" value="Group">
                                <table cellpadding=4 cellspacing=4 border=0>
                                    <tr>
                                        <td><%=resources.LNG_GROUP_NAME %>:</td>
                                        <td>
                                            <input runat="server" name="name" id="groupNameInputValue" type="text" value="">
                                            <input runat="server" id="domainId" name="domain_id" type="hidden" value="">
                                            <input runat="server" name="oldGroupName" id="oldGroupName" type="hidden" value="">
                                        </td>
                                    </tr>
                                </table>
                                <div align="right">
                                    <button type="submit" class="cancel_button" onclick="javascript:history.go(-1); return false;"><%=resources.LNG_CANCEL %></button>
                                    <asp:LinkButton class="save_button" runat="server" id="saveButton"/>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</form>
</body>
</html>
