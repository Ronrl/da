<!-- #include file="libs/adovbs.inc" -->
<!-- #include file="libs/TemplateObj.asp" -->

<%
' Obtain the path where this site is located on the server
'-------------------------------
Dim sAppPath : sAppPath = left(Request("PATH_TRANSLATED"), instrrev(Request("PATH_TRANSLATED"), "\"))
'-------------------------------
' Create Header and Footer Path variables
'-------------------------------
Dim sHeaderFileName : sHeaderFileName = alerts_dir & templateFolder & "\" & "header.html"
Dim sFooterFileName : sFooterFileName = alerts_dir & templateFolder & "\" & "footer.html"
Dim sDateFormFileName : sDateFormFileName = alerts_dir & templateFolder & "\" & "date_form.html"
Dim sSearchFormFileName : sSearchFormFileName = alerts_dir & templateFolder & "\" & "search_form.html"
Dim sSearchDetailsFormFileName : sSearchDetailsFormFileName = alerts_dir & templateFolder & "\" & "search_details_form.html"
Dim sMenuFileName : sMenuFileName = alerts_dir & templateFolder & "\" & "menu.html"
Dim sOU_menuFileName : sOU_menuFileName = alerts_dir & templateFolder & "\" & "statistics_da_users_view_details_ou.html"

Dim sSurveyPrevFileName : sSurveyPrevFileName = alerts_dir & templateFolder & "\" & "survey_prev.html"
Dim sSurveyNextFileName : sSurveyNextFileName = alerts_dir & templateFolder & "\" & "survey_next.html"
Dim sSurveysTable: sSurveysTable = alerts_dir & templateFolder & "\" & "surveys_table.html"

LoadTemplateFromString "DateForm", ""
LoadTemplateFromString "SearchForm", ""
SetVar "searchUserName", ""
LoadTemplateFromString "Menu", ""
LoadTemplateFromString "paging", ""

SetVar "strDisplayName", ""
SetVar "ou_menu", ""

SetVar "surveyCustomAnswers", ""
SetVar "surveyPrevLink", ""
SetVar "surveyNextLink", ""

SetVar "strDate", ""
SetVar "class", ""

SetVar "needTabs","false"
SetVar "activeTab","false"

SetVar "default_lng", default_lng
SetVar "LNG_TODAY", LNG_TODAY
SetVar "LNG_YESTERDAY", LNG_YESTERDAY
SetVar "LNG_LAST_DAYS", LNG_LAST_DAYS
SetVar "LNG_THIS_MONTH", LNG_THIS_MONTH
SetVar "LNG_LAST_MONTH", LNG_LAST_MONTH
SetVar "LNG_ALL_TIME", LNG_ALL_TIME
SetVar "LNG_SHOW_STATISTICS", LNG_SHOW_STATISTICS
SetVar "LNG_SHOW", LNG_SHOW
SetVar "LNG_CLOSE", LNG_CLOSE
SetVar "curTime", now_db
SetVar "DateInThePastError" , LNG_DATE_IN_THE_PAST
SetVar "LNG_SEARCH" , "Search"
SetVar "settingsDateFormat", GetDateFormat()
SetVar "settingsTimeFormat", GetTimeFormat()

SetVar "strCheckedOption1", ""
SetVar "strCheckedOption2", ""
SetVar "strEnabledOption1", ""
SetVar "strEnabledOption2", ""
SetVar "strSelectedOption1", ""
SetVar "strSelectedOption2", ""
SetVar "strSelectedOption3", ""
SetVar "strSelectedOption4", ""
SetVar "strSelectedOption5", ""
SetVar "strSelectedOption6", ""
SetVar "strSelectedOption7", ""


'Dim sLoginFileName : sLoginFileName = sAppPath & templateFolder & "\" & "login.html"
'===============================

function Login (intErrorCode)

LoadTemplate sLoginFileName, "main"

LoadTemplate sHeaderFileName, "Header"
LoadTemplate sFooterFileName, "Footer"

if(intErrorCode=1) then
	SetVar "strLoginError", "Wrong login/password. Please try again."
end if

Header_Show
Footer_Show

'LoadTemplate sDateFormFileName, "DateForm"
'Parse "DateForm", False

Parse "Header", False
Parse "Footer", False
Parse "main", False

Response.write PrintVar("main")

UnloadTemplate
end function

function PageName(strPage_)
	SetVar "strPageName", strPage_
end function

function GenerateMenu(strPageId)

Dim strMenu

index = 0
if(strPageId="Statistics") then 
	strMenu = strMenu & "<li>Statistics</li>" 
else
	SetVar "needTabs","true" 
	strMenu = strMenu & "<li><a href=""#main_content_div"" onclick=""javascript: stat_submit('statistics_da_overall.asp');"">"&LNG_OVERALL_STATISTICS&"</a></li>" 
	index = index + 1

	set fs1=Server.CreateObject("Scripting.FileSystemObject")
	if EXTENDED_STATISTICS=1 then

		strMenu = strMenu & "<li "
		if EXTENDED_STATISTICS_TRIAL=1 then
			strMenu = strMenu & "style='background:#efffef' title='" & LNG_TRIAL_ADDON_DESCRIPTION & TRIAL_DAYS_LEFT & "'"
		end if
		strMenu = strMenu & "><a href=""#main_content_div"" onclick=""stat_submit('statistics_da_alerts.asp?class=1&class=16');"">"&LNG_ALERTS_STATISTICS&"</a></li>"
		index = index + 1

		strMenu = strMenu & "<li "
		if EXTENDED_STATISTICS_TRIAL=1 then
			strMenu = strMenu & "style='background:#efffef' title='" & LNG_TRIAL_ADDON_DESCRIPTION & TRIAL_DAYS_LEFT & "'"
		end if
		strMenu = strMenu & "><a href=""#main_content_div"" onclick=""stat_submit('statistics_da_users.asp');"">"&LNG_USERS_STATISTICS&"</a></li>"
		index = index + 1

		if SURVEY = 1 then
			strMenu = strMenu & "<li "
			if EXTENDED_STATISTICS_TRIAL=1 then
				strMenu = strMenu & "style='background:#efffef' title='" & LNG_TRIAL_ADDON_DESCRIPTION & TRIAL_DAYS_LEFT & "'"
			end if
			strMenu = strMenu & "><a href=""#main_content_div"" onclick=""stat_submit('statistics_da_surveys.asp');"">"&LNG_SURVEYS_STATISTICS&"</a></li>"
			surveyIndex = index
			index = index + 1
		end if
		if RSS = 1 then
			strMenu = strMenu & "<li "
			if EXTENDED_STATISTICS_TRIAL=1 then
				strMenu = strMenu & "style='background:#efffef' title='" & LNG_TRIAL_ADDON_DESCRIPTION & TRIAL_DAYS_LEFT & "'"
			end if
			strMenu = strMenu & "><a href=""#main_content_div"" onclick=""stat_submit('statistics_da_alerts.asp?class=5');"">"&LNG_RSS_STATISTICS&"</a></li>"
			rssIndex = index
			index = index + 1
		end if
		if SS = 1 then
			strMenu = strMenu & "<li "
			if EXTENDED_STATISTICS_TRIAL=1 then
				strMenu = strMenu & "style='background:#efffef' title='" & LNG_TRIAL_ADDON_DESCRIPTION & TRIAL_DAYS_LEFT & "'"
			end if
			strMenu = strMenu & "><a href=""#main_content_div"" onclick=""stat_submit('statistics_da_alerts.asp?class=2');"">"&LNG_SS_STATISTICS&"</a></li>"
			ssIndex = index
			index = index + 1
		end if
		if WP = 1 then
			strMenu = strMenu & "<li "
			if EXTENDED_STATISTICS_TRIAL=1 then
				strMenu = strMenu & "style='background:#efffef' title='" & LNG_TRIAL_ADDON_DESCRIPTION & TRIAL_DAYS_LEFT & "'"
			end if
			strMenu = strMenu & "><a href=""#main_content_div"" onclick=""stat_submit('statistics_da_alerts.asp?class=8');"">"&LNG_WP_STATISTICS&"</a></li>"
			wpIndex = index
			index = index + 1
		end if
	end if

	select case strPageId
		case 3
			SetVar "activeTab", surveyIndex
		case 4
			SetVar "activeTab", rssIndex
		case 5
			SetVar "activeTab", ssIndex
		case 6
			SetVar "activeTab", wpIndex
		case else
			SetVar "activeTab", strPageId
	end select
end if
'if(strPageId="Manage") then strMenu = strMenu & "Manage Toolbars" else strMenu = strMenu & "<A href=""statistics_tbstat_manage_toolbars.asp"">Manage Toolbars</a>" end if
'strMenu = strMenu & " | "

SetVar "strMenu", "<ul>"&strMenu&"</ul>"
end function
%>