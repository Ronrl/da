﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Net;
using System.IO;

namespace deskalerts
{

    public partial class AddMessageTemplatesGroups : System.Web.UI.Page
    {
        
        public int offset;
        public int limit;
        public string sortBy;
        public int count;
       public int Offset { get { return offset; } }
       public int Limit { get { return limit; } }
       public string SortBy { get { return sortBy; } }
       public int Count { get { return count; } }
       public Dictionary<string, string> vars = new Dictionary<string,string>();
          public string order;

          public bool noRows;


       public  string page ;
       public string name;
       string orderCollumn;
       public string Name { get { return name; } }


       protected void Page_PreInit(object sender, EventArgs e)
       {
           
           order = !string.IsNullOrEmpty(Request.Params["order"]) ? Request.Params["order"] : "DESC";
           orderCollumn = !string.IsNullOrEmpty(Request.Params["collumn"]) ? Request.Params["collumn"] : "id";
          offset = !string.IsNullOrEmpty(Request.Params["offset"]) ? Convert.ToInt32(Request.Params["offset"]) : 0;
          limit = !string.IsNullOrEmpty(Request.Params["limit"]) ? Convert.ToInt32(Request.Params["limit"]) : 25;
          sortBy = !string.IsNullOrEmpty(Request.Params["sortby"]) ? Request.Params["sortby"] : "date DESC";
          
          page = "MessageTemplatesGroups.aspx?";
          
           
        }

      /* protected void ParseAllGroups()
       {
           string url = HttpContext.Current.Request.Url.AbsoluteUri;
           string path = url.Substring(0, url.IndexOf("AddMessageTemplatesGroups"));

           path += "/set_settings.asp?name=GetMsgTemplates";
           WebRequest request = WebRequest.Create(path);
           WebResponse response = request.GetResponse();
           StreamReader reader = new StreamReader(response.GetResponseStream());

           string[] groupsArray = reader.ReadToEnd().Split('\n');

           foreach (string groupString in groupsArray)
           {
               if (groupString == string.Empty)
                   continue;

               string id = groupString.Split('\r')[0];
               string name = groupString.Split('\r')[1];

               HtmlTableRow row = new HtmlTableRow();

               HtmlTableCell checkBoxCell = new HtmlTableCell();

               checkBoxCell.Align = "center";
               checkBoxCell.InnerHtml = "<input type=\"checkbox\" name=\"objects\" value=\"" + id + "\">";

               HtmlTableCell templateNameCell = new HtmlTableCell();
               templateNameCell.InnerHtml = name;

               row.Cells.Add(checkBoxCell);
               row.Cells.Add(templateNameCell);

               templateTable.Rows.Add(row);
           }

            
       }*/

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Request["edit"] == "1")
            {
                int groupId = 0;

                group_name.Value = Request["tgname"];

                if (int.TryParse(Request["tgid"], out groupId) || int.TryParse(Request["dupId"], out groupId))
                {
                    string url = HttpContext.Current.Request.Url.AbsoluteUri;
                    string path = url.Substring(0, url.IndexOf("AddMessageTemplatesGroups"));
                    string url_bridge = path + "/set_settings.asp?name=GetTemplateForGroup&tgid=" + groupId;

                    WebRequest request = WebRequest.Create(url_bridge);
                    WebResponse response = request.GetResponse();
                    StreamReader reader = new StreamReader(response.GetResponseStream());
                    string[] templatesArray = reader.ReadToEnd().Split('\n');

                    foreach (string template in templatesArray)
                    {
                        if (template == String.Empty)
                            continue;

                        string templateId = template.Split('\r')[0];
                        string templateName = template.Split('\r')[1];

                        ListItem item = new ListItem(templateName, templateId);
                        templatesInGroupList.Items.Add(item);
                    }
                }
                
            }

            this.vars = GetAspVariables("LNG_TGROUPS_ADD", "LNG_GROUP_NAME", "LNG_TGROUPS_ADD_DESCRIPTION", "LNG_CANCEL", "LNG_SAVE");

            tgroupsDescription.Text = vars["LNG_TGROUPS_ADD_DESCRIPTION"];
        }

        public Dictionary<string, string> GetAspVariables(params string[] keys)
        {
            string queryString = String.Join(":", keys);

            string url = HttpContext.Current.Request.Url.AbsoluteUri;
            string path = url.Substring(0, url.IndexOf("AddMessageTemplatesGroups"));
            string url_bridge = path + "/dotnetbridge.asp?var=" + queryString;
            WebRequest request = WebRequest.Create(url_bridge);
            WebResponse response = request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
           // Response.Write(reader.ReadToEnd());
            string[] variablesStrings = reader.ReadToEnd().Split('&');
            

            Dictionary<string, string> result = new Dictionary<string, string>();
            foreach (string keyVal in variablesStrings)
            {

                string[] keyValArray = keyVal.Split('|');

                if (keyValArray.Length != 2)
                    continue;

                string key = keyValArray[0];
                string value = keyValArray[1];

                result.Add(key, value);
            }

            return result;

            
        }
        public string GetAspVariable(string variableName)
        {
            string url = HttpContext.Current.Request.Url.AbsoluteUri;
            string path = url.Substring(0, url.IndexOf("AddMessageTemplatesGroups"));
            string url_bridge = path + "/dotnetbridge.asp?var="+variableName;
            WebRequest request = WebRequest.Create(url_bridge);
            WebResponse response = request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            string variable = reader.ReadToEnd();
            return variable;
        }

  

        public string MakePages()
        {
            string url = HttpContext.Current.Request.Url.AbsoluteUri;
            string path = url.Substring(0, url.IndexOf("AddMessageTemplatesGroups"));
            string url_bridge = path + "/dotnetbridge.asp?action=makepage&offset="+Offset+"&limit="+Limit+"&count="+Count+"&page="+page+"&name="+Name+"&sortby="+SortBy;
            WebRequest request = WebRequest.Create(url_bridge);
            WebResponse response = request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            string variable = reader.ReadToEnd();
            return variable;

        }
  





    }
}
