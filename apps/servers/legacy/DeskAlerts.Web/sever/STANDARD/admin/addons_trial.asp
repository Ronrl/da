<% 
Set RSAddonsList = Conn.Execute("SELECT val FROM settings WHERE name='ConfAddonsTrialList'")
TRIAL_DAYS_LEFT = -1
if (Not RSAddonsList.EOF) then
	if (Not RSAddonsList("val")="") then
		Set RSAddonsTrial = Conn.Execute("SELECT DATEDIFF(day, CONVERT(datetime,(SELECT val FROM settings WHERE name='ConfAddonsTrialDate'),9), GETDATE()) as DateDifference")
		if (Not RSAddonsTrial.EOF) then
			Addons = Split(RSAddonsList("val"),";")
			if RSAddonsTrial("DateDifference") > 14 then
				dim addonsFS
				Set addonsFS=Server.CreateObject("Scripting.FileSystemObject")
				for each Addon in Addons
					Addon = Trim(Addon)
					select case Addon
						case "Active Directory"
							AD=0
						case "SMS"
							SMS=Empty
							SMS_EXPIRED=1
						case "E-mail"
							EM=Empty
						case "Screensaver"
							SS=Empty
						case "Video"
							VIDEO=Empty
						case "Video"
							WEBPLUGIN=Empty
						case "RSS"
							RSS=Empty
						case "Wallpaper"
							WP=Empty
						case "Blog"
							BLOG=Empty
						case "Twitter"
							SOCIAL_MEDIA=Empty
						case "LinkedIn"
							SOCIAL_LINKEDIN=Empty
						case "Fullscreen"
							FULLSCREEN_CFG=Empty
						case "Ticker"
							TICKER_CFG=Empty
						case "Ticker Pro"
							TICKER_PRO=Empty
						case "Survey"
							SURVEY=Empty
						case "Statistics"
							EXTENDED_STATISTICS=Empty
						case "InstantAlerts"
							IM=Empty
						case "TextToCall"
							TEXT_TO_CALL=Empty
							TEXT_TO_CALL_EXPIRED=1
						case "Mobile"
							MOBILE_SEND=Empty
					end select
				next
				set addonsFS=nothing
				TRIAL_DAYS_LEFT = -1
			else
				TRIAL_DAYS_LEFT = 14 - RSAddonsTrial("DateDifference")
				for each Addon in Addons
					Addon = Trim(Addon)
					select case Addon
						case "Active Directory"
							AD_TRIAL=1
						case "SMS"
							SMS_TRIAL=1
						case "E-mail"
							EM_TRIAL=1
						case "Screensaver"
							SS_TRIAL=1
						case "Video"
							VIDEO_TRIAL=1
						case "Video"
							WEBPLUGIN_TRIAL=1
						case "RSS"
							RSS_TRIAL=1
						case "Wallpaper"
							WP_TRIAL=1
						case "Blog"
							BLOG_TRIAL=1
						case "Twitter"
							SOCIAL_MEDIA_TRIAL=1
						case "LinkedIn"
							SOCIAL_LINKEDIN_TRIAL=1
						case "Fullscreen"
							FULLSCREEN_CFG_TRIAL=1
						case "Ticker"
							TICKER_CFG_TRIAL=1
						case "Ticker Pro"
							TICKER_PRO_TRIAL=1
						case "Survey"
							SURVEY_TRIAL=1
						case "Statistics"
							EXTENDED_STATISTICS_TRIAL=1
						case "InstantAlerts"
							IM_TRIAL=1
						case "TextToCall"
							TEXT_TO_CALL_TRIAL=1
						case "Mobile"
							MOBILE_SEND_TRIAL=1
					end select
				next
			end if
		end if
		RSAddonsTrial.Close	
	end if
end if
RSAddonsList.Close	
%>