using Castle.MicroKernel.Lifestyle;
using DeskAlerts.ApplicationCore.Enums;
using DeskAlerts.ApplicationCore.Extensions.Enums;
using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.ApplicationCore.Structs;
using DeskAlerts.Server.Utils;
using DeskAlerts.Server.Utils.Interfaces;
using DeskAlerts.Server.Utils.Managers;
using DeskAlertsDotNet.Application;
using DeskAlertsDotNet.Controls;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Models;
using DeskAlertsDotNet.Models.Permission;
using DeskAlertsDotNet.Parameters;
using DeskAlertsDotNet.Utils.Services;
using Google.Apis.Services;
using Google.Apis.Translate.v2;
using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace DeskAlertsDotNet.Pages
{
    public partial class CreateAlert : DeskAlertsBasePage
    {
        private readonly ISettingsRepository _settingsRepository;

        //use curUser to access to current user of DA control panel
        //use dbMgr to access to database
        protected string liApiKey;

        public string skinsJson;
        public string digitalSignage = "";
        public string IsInstantVal = "";
        public int shouldCheckSize = 0;
        public int shouldBeReadOnly = 0;
        public int tickerValue = 0;
        public int rsvpValue = 0;
        public int tickerSpeed = 4;
        public bool ConfEnableAlertLangs;
        public Dictionary<string, bool> languages = new Dictionary<string, bool>();
        public Dictionary<string, string> languagesLocale = new Dictionary<string, string>();
        private bool moreThanOneAlertLangsUsed;

        private ISkinService _skinService;

        private const int MinAlertHeight = 290;
        private const int MinAlertWidth = 340;

        public CreateAlert()
        {
            _skinService = new SkinService();

            using (Global.Container.BeginScope())
            {
                _settingsRepository = Global.Container.Resolve<ISettingsRepository>();
            }
        }

        private bool ShouldDisplayChanelsTab()
        {
            return Config.Data.HasModule(Modules.BLOG) || Config.Data.HasModule(Modules.YAMMER) ||
                   Config.Data.HasModule(Modules.SMS)
                   || Config.Data.HasModule(Modules.EMAIL) || Config.Data.HasModule(Modules.TWITTER);
        }

        private bool checkSms()
        {
            return sms.Value == "1";
        }

        /// <summary>
        /// This method checks if we are in Draft mode
        /// </summary>
        /// <returns>True if we are in Draft mode</returns>
        private bool CheckDraftMode()
        {
            bool result = !string.IsNullOrEmpty(this.Request["edit"]) && this.Request["edit"] == "1";
            return result;
        }

        /// <summary>
        /// Check if we are in Emergency mode
        /// </summary>
        /// <returns>True if we are in Emergency mode</returns>
        private bool CheckIEmergencyMode()
        {
            bool result = string.IsNullOrEmpty(this.Request["instant"]) || this.Request["instant"] != "1";
            return result;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Logger.Debug($"Start of endpoint CreateAlert.aspx for user {curUser.Name}");

            saveNextButton.Click += saveNextButton_Click;
            saveButton.Click += saveButton_Click;
            bottomSaveButton.Click += saveButton_Click;
            bottomSaveNextButton.Click += saveNextButton_Click;

            ScriptManager1.RegisterAsyncPostBackControl(saveNextButton);
            ScriptManager1.RegisterAsyncPostBackControl(saveButton);
            ScriptManager1.RegisterAsyncPostBackControl(bottomSaveButton);
            ScriptManager1.RegisterAsyncPostBackControl(bottomSaveNextButton);
            object liKeyObj = dbMgr.GetScalarByQuery("SELECT field1 FROM social_media WHERE type=2");

            string shouldApproveRequestValue = Request["shouldApprove"];
            bool isAlertOnModeration = false;

            if (liKeyObj != null)
            {
                liApiKey = liKeyObj.ToString();
            }

            if (CheckDraftMode() && CheckIEmergencyMode())
            {
                shouldApprove.Value = shouldApproveRequestValue;
                isAlertOnModeration = shouldApproveRequestValue == "1";
            }

            if (!curUser.HasPrivilege && !curUser.Policy[Policies.ALERTS].CanSend && CheckIEmergencyMode())
            {
                HideButtonSaveAndNext(saveNextButton);
                HideButtonSaveAndNext(bottomSaveNextButton);
            }

            headerTitle.InnerText = resources.LNG_CREATE_ALERT;//resources.LNG_CREATE_ALERT"];

            skinsJson = _skinService.GetSkinsFromDbByPolicyGuidList(curUser.Policy.SkinList);

            // enable multi languages in alerts
            var languageCount = dbMgr.GetScalarQuery<int>("SELECT count(id) FROM alerts_langs");

            moreThanOneAlertLangsUsed = languageCount > 1 && Config.Data.IsOptionEnabled("ConfEnableAlertLangs");

            if (moreThanOneAlertLangsUsed && Request.GetParam("multilang", 0) != 1 && !IsPostBack && string.IsNullOrEmpty(Request["id"]))
            {
                Server.Transfer("SelectAlertLanguages.aspx", true);
            }

            if (moreThanOneAlertLangsUsed)
            {
                currentAlertCreationStepNumberSpan.InnerText = "2";
                totalAlertCreationStepNumberSpan.InnerText = "3";
            }
            else
            {
                currentAlertCreationStepNumberSpan.InnerText = "1";
                totalAlertCreationStepNumberSpan.InnerText = "2";
            }

            var langIds = Request.Params.AllKeys.Where(key => key.StartsWith("lang_")).Select(key => key.Split('_')[1]);
            if (!IsPostBack && langIds.Count() > 0)
            {
                var defaultLanguageRow = dbMgr.GetDataByQuery("SELECT * FROM alerts_langs WHERE is_default = '1'").First();
                defaultLanguage.Text = "<br /><b>" + defaultLanguageRow.GetString("name") + "</b>:";
                defaultLang.Value = defaultLanguageRow.GetString("abbreviatures");
                var languagesInfoSet = dbMgr.GetDataByQuery($"SELECT * FROM alerts_langs WHERE id IN ({string.Join(",", langIds)})");

                foreach (var languagesInfoRow in languagesInfoSet)
                {
                    AddLanguageControls(languagesInfoRow.GetString("name"), languagesInfoRow.GetString("abbreviatures"), "", "");
                }
            }

            DataSet alertLangsData = dbMgr.GetDataByQuery("SELECT id, name, abbreviatures, is_default FROM alerts_langs");
            int Langs = dbMgr.GetScalarByQuery<int>("SELECT COUNT(1) as cnt FROM alerts_langs");
            if (Langs == 1) { ConfEnableAlertLangs = false; }

            if (ConfEnableAlertLangs && langIds.Count() > 1)
            {
                foreach (var alertLangsRow in alertLangsData)
                {
                    languages.Add(alertLangsRow.GetString("name"), alertLangsRow.GetBool("is_default"));
                    languagesLocale.Add(alertLangsRow.GetString("name"), alertLangsRow.GetString("abbreviatures"));
                }

                mainView.Visible = false;
                alertLangs.Visible = true;
            }

            // end adding multi languages to alerts
            if (!string.IsNullOrEmpty(Request["webplugin"]))
            {
                digitalSignage = Request["webplugin"];
            }

            digsignInfo.Visible = digitalSignage.Equals("1");

            resizable.Visible = digitalSignage != "1";
            resizableLabel.Visible = digitalSignage != "1";

            rsvpTypeTab.Visible = digitalSignage != "1";

            rsvpDialogTile.Visible = digitalSignage != "1";

            if (!string.IsNullOrEmpty(Request["instant"]))
            {
                IsInstantVal = Request["instant"];

                alertScheduleHeader.Visible = false;
                schedulingMenu.Visible = false;

                lifeTimeCheckBox.ClientIDMode = ClientIDMode.Static;
                lifeTimeCheckBox.ID = "lifeTimeCheckBox_Disabled";
                lifeTimeCheckBox.Disabled = true;
                lifeTimeCheckBox.Visible = true;
                form1.Controls.Add(new HtmlInputHidden()
                {
                    Name = "lifeTimeCheckBox",
                    ID = "lifeTimeCheckBox",
                    Value = "1",
                    ClientIDMode = ClientIDMode.Static
                });

                unobtrusiveCheckbox.ClientIDMode = ClientIDMode.Static;
                unobtrusiveCheckbox.ID = "unobtrusiveCheckbox_Disabled";
                unobtrusiveCheckbox.Disabled = true;
                form1.Controls.Add(new HtmlInputHidden()
                {
                    Name = "unobtrusiveCheckbox",
                    ID = "unobtrusiveCheckbox",
                    Value = "0",
                    ClientIDMode = ClientIDMode.Static
                });

            }
            lnkdnTypeTab.Visible = Config.Data.HasModule(Modules.LINKEDIN) && digitalSignage != "1" && IsInstantVal != "1";
            IsInstant.Value = IsInstantVal;

            if (Settings.IsTrial)
            {
                resizable.Visible = false;
                resizableLabel.Attributes["title"] = resources.TRIAL_MODE_DISABLED_FUNCTIONALITY;
                resizableLabel.Attributes["class"] = ContentCustomClass.AdditionalDescription.GetStringValue();
            }
            LifetimeAdditionalDescription.InnerText = resources.LNG_TIME_EXPIRED_CANT_RECEIVED;
            LifeTimeHelpImage.Attributes["title"] = resources.LNG_LIFETIME_EXT_DESC;

            changeColorCode.Visible = IsInstantVal == "1";
            // lnkdnTypeTab.Visible = IsInstantVal != "1";
            saveButton.Visible = IsInstantVal != "1";
            bottomSaveButton.Visible = IsInstantVal != "1";

            int colorCodesCount = dbMgr.GetScalarQuery<int>("SELECT COUNT(1) FROM color_codes");

            if (colorCodesCount == 0)
                changeColorCode.Visible = false;

            if (!string.IsNullOrEmpty(Request["rejectedEdit"]))
            {
                rejectedEdit.Value = Request["rejectedEdit"];
            }

            if (!string.IsNullOrEmpty(Request["ticker"]))
            {
                ticker.Value = Request["ticker"];
                // ChangeTab("ticker");
            }

            if (!string.IsNullOrEmpty(Request["shouldApprove"]))
            {
                shouldApprove.Value = Request["shouldApprove"];
            }

            if (!string.IsNullOrEmpty(Request["id"]))
            {
                alertId.Value = Request["id"];
            }

            bool hasAnyTickerModule = Config.Data.HasModule(Modules.TICKER) || Config.Data.HasModule(Modules.TICKER_PRO);
            tickerTypeTab.Visible = hasAnyTickerModule;
            if (!string.IsNullOrEmpty(Request["ticker"]) && hasAnyTickerModule)
            {
                tickerValue = 1;
            }

            if (!string.IsNullOrEmpty(Request["rsvp"]))
            {
                rsvpValue = 1;
            }

            if (!string.IsNullOrEmpty(Request["webplugin"]))
                webAlert.Value = "1";

            chanelsTab.Visible = ShouldDisplayChanelsTab();
            chanelsTable.Visible = ShouldDisplayChanelsTab();
            if (!Config.Data.HasModule(Modules.CAMPAIGN) || string.IsNullOrEmpty(Request["alert_campaign"]) || Request["alert_campaign"].Equals("-1"))
            {
                campaignLabel.Visible = false;
                campaignSelect.Value = "-1";
            }
            else
            {
                int campaignId = Convert.ToInt32(Request["alert_campaign"]);

                string campName = dbMgr.GetScalarByQuery("SELECT name FROM campaigns WHERE id = " + campaignId).ToString();
                campaignName.InnerText = campName;
                campaignSelect.Value = campaignId.ToString();
                saveButton.Visible = false;
                bottomSaveButton.Visible = false;
            }

            if (Config.Data.IsOptionEnabled("ConfEnableHtmlAlertTitle"))
            {
                htmlTitleDiv.Visible = true;
                simpleTitleDiv.Visible = false;
            }
            else
            {
                htmlTitleDiv.Visible = false;
                simpleTitleDiv.Visible = true;
            }

            if (IsInstantVal == "1")
            {
                saveNextButton.Text = resources.LNG_NEXT;
                bottomSaveNextButton.Text = resources.LNG_NEXT;
            }
            else
            {
                saveNextButton.Text = resources.LNG_SAVE_AND_NEXT;
                bottomSaveNextButton.Text = resources.LNG_SAVE_AND_NEXT;
            }
            saveButton.Text = resources.LNG_SAVE;
            // saveNextButton.Text = resources.LNG_SAVE_AND_NEXT;

            bottomSaveButton.Text = resources.LNG_SAVE;
            //    bottomSaveNextButton.Text = resources.LNG_SAVE_AND_NEXT;

            if (digitalSignage.Equals("1"))
            {
                saveButton.Visible = false;
                bottomSaveButton.Visible = false;
                alertDeviceHeader.Visible = false;
                alertDeviceTable.Visible = false;
                alertScheduleHeader.Visible = false;
                schedulingMenu.Visible = false;
                alertSettingsHeader.Visible = false;
                alertSettingsTable.Visible = false;
                alertDeviceTable.Visible = false;
                chanelsTab.Visible = false;
                chanelsTable.Visible = false;
                positionCell.Visible = false;

                int linksCount = dbMgr.GetScalarQuery<int>("SELECT COUNT(1) FROM digsign_links");

                if (linksCount == 0)
                {
                    saveNextButton.Visible = false;
                    bottomSaveNextButton.Visible = false;
                    bottomPreview.Visible = false;
                    previewButton.Visible = false;
                    digSignNoLinksWarning.Style.Add(HtmlTextWriterStyle.Display, "block");
                    digsignInfo.Style.Add(HtmlTextWriterStyle.Display, "none");
                    if (!curUser.HasPrivilege)
                    {
                        digSignNoLinksWarning.InnerText = resources.LNG_NO_LINKS;
                    }
                }
                
            }

            twitterDiv.Visible = Config.Data.HasModule(Modules.TWITTER);

            object twitterSettingsObj = dbMgr.GetScalarByQuery("SELECT id FROM social_media WHERE type=1");

            if (twitterSettingsObj != null)
            {
                socialMedia.Attributes.Add("disabled", "false");
                socialMediaSettingsHint.Visible = false;
            }

            blogDiv.Visible = Config.Data.HasModule(Modules.BLOG);
            demoBlogDiv.Visible = Config.Data.IsDemo && Config.Data.HasModule(Modules.BLOG);
            object blogSettingsObj = dbMgr.GetScalarByQuery("SELECT id FROM blogs");

            if (blogSettingsObj != null)
            {
                //socialMedia.Attributes.Add("disabled", "false");
                blogPostSettingsHint.Style.Add(HtmlTextWriterStyle.Display, "none");
            }
            else
            {
                blogPostSettingsHint.Style.Add(HtmlTextWriterStyle.Display, "block");
            }

            yammerSettingsDiv.Visible = Config.Data.HasModule(Modules.YAMMER);

            smsDiv.Visible = (Config.Data.HasModule(Modules.SMS) || Config.Data.HasModule(Modules.SMPP)) && (curUser.HasPrivilege || curUser.Policy[Policies.SMS].CanSend);

            demoTextToCallDiv.Visible = Config.Data.IsDemo && Config.Data.HasModule(Modules.TEXTTOCALL);

            if (!demoTextToCallDiv.Visible)
                textToCallDiv.Visible = Config.Data.HasModule(Modules.TEXTTOCALL) && (curUser.HasPrivilege || curUser.Policy[Policies.TEXT_TO_CALL].CanSend);

            mailSettingsDiv.Visible = Config.Data.HasModule(Modules.EMAIL) && (curUser.HasPrivilege || curUser.Policy[Policies.EMAILS].CanSend);
            if (!IsPostBack)
            {
                if (!curUser.HasPrivilege && !curUser.Policy[Policies.TEXT_TEMPLATES].CanSend)
                {
                    templatesList.Visible = false;
                }
                else if (curUser.HasPrivilege || !Config.Data.IsOptionEnabled("ConfTGroupEnabled") ||
                         curUser.Policy.CanSendAllTemplates)
                {
                    ListItem first = new ListItem(resources.LNG_TEXT_TEMPLATES, "0");
                    DataSet tempaltesSet = dbMgr.GetDataByQuery("SELECT id, name FROM text_templates ORDER BY name");

                    templatesList.Items.Add(first);
                    foreach (DataRow templateRow in tempaltesSet)
                    {
                        ListItem item = new ListItem(templateRow.GetString("name"), templateRow.GetString("id"));
                        templatesList.Items.Add(item);
                    }
                }
                else
                {
                    //EGS Template Groups custom
                    string templateGroups = string.Join(",", curUser.Policy.TemplatesGroups.ToArray());
                    DataSet templatesSet =
                        dbMgr.GetDataByQuery(
                            "SELECT DISTINCT a.id, a.name FROM text_templates as a INNER JOIN group_message_templates as b ON a.id = b.template_id WHERE b.group_id IN (" +
                            templateGroups + ")");

                    foreach (DataRow templateRow in templatesSet)
                    {
                        ListItem item = new ListItem(templateRow.GetString("id"), templateRow.GetString("name"));
                        templatesList.Items.Add(item);
                    }
                }

                lifetimeFactor.Items.Add(new ListItem(resources.LNG_MINUTES, "1"));
                lifetimeFactor.Items.Add(new ListItem(resources.LNG_HOURS, "60"));
                lifetimeFactor.Items.Add(new ListItem(resources.LNG_DAYS, "1440"));

                lifetimeFactor.Items[2].Selected = true;
            }

            if (curUser.HasPrivilege || curUser.Policy[Policies.ALERTS].CanResize)
                shouldCheckSize = 1;

            if (Settings.Content["ConfEnableHtmlAlertTitle"].Equals(("1")))
            {
                simpleTitleDiv.Visible = false;
                htmlTitleDiv.Visible = true;
            }
            else
            {
                simpleTitleDiv.Visible = true;
                htmlTitleDiv.Visible = false;
            }

            if (!string.IsNullOrEmpty(Request["id"]))
            {
                int alertId = Convert.ToInt32(Request["id"]);
                LoadAlert(alertId);
            }
            else if (!string.IsNullOrEmpty(Request["tid"]))
            {
                int templateId = Convert.ToInt32(Request["tid"]);
                LoadTemplate(templateId);
            }
            else
            {
                RestoreDefaultSettings();
                question.InnerText = resources.LNG_WILL_YOU_BE_ATTENDING;
                question_option1.InnerText = resources.LNG_YES;
                question_option2.InnerText = resources.LNG_NO;
                second_question.InnerText = resources.LNG_PLEASE_DESCRIBE_YOUR_REASON;
            }

            alertDeviceHeader.Visible = Config.Data.HasModule(Modules.MOBILE);
            alertDeviceTable.Visible = Config.Data.HasModule(Modules.MOBILE);

            ShowContentSettingsByPolicy();
        }

        public void ShowPromt(HttpResponse r, string text)
        {
            //r.Write("<script language='javascript'>showPrompt('" + text + "');</script>");

            ScriptManager.RegisterStartupScript(updatePanel, updatePanel.GetType(), "SHOW_PROMT", "showPrompt('" + text + "')", true);
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            if (!ValidateData())
                return;

            if (Settings.Content["ConfEnableHtmlAlertTitle"].Equals("1"))
                titleHidden.Value = htmlTitle.InnerText;

            SubmitForm(rejectedEdit.Value != "1");
        }

        private bool ValidateData()
        {
            if (string.IsNullOrEmpty(alertWidth.Value))
            {
                alertWidth.Value = _settingsRepository.GetByName("ConfAlertWidth").value;
            }

            var parseAlertWidth = !int.TryParse(alertWidth.Value, out var currentAlertWidth);
            if (parseAlertWidth)
            {
                ShowPromt(Response, resources.LNG_ALERT_WIDTH_NOT_CORRECT_VALUE);
                return false;
            }

            if (string.IsNullOrEmpty(alertHeight.Value))
            {
                alertHeight.Value = _settingsRepository.GetByName("ConfAlertHeight").value;
            }

            var parseAlertHeight = !int.TryParse(alertHeight.Value, out var currentAlertHeight);
            if (parseAlertHeight)
            {
                ShowPromt(Response, resources.LNG_ALERT_HEIGHT_NOT_CORRECT_VALUE);
                return false;
            }

            if (currentAlertWidth < MinAlertWidth || currentAlertHeight < MinAlertHeight) 
            {
                ShowPromt(Response, resources.LNG_RESOLUTION_DESC2);
                return false;
            }

            string content = alertContent.InnerHtml;
            string title = Settings.Content["ConfEnableHtmlAlertTitle"].Equals("1")
                ? htmlTitleHidden.Value
                : simpleTitle.Value;

            bool hasEmptyTitles = false;
            bool hasBigTitles = false;

            foreach (string key in Request.Params.AllKeys)
            {
                if (!string.IsNullOrEmpty(key) && key.StartsWith("htmlTitle") && key.EndsWith("Hidden"))
                {
                    string keyValue = Request.GetParam(key, "");

                    if (string.IsNullOrEmpty(keyValue))
                    {
                        hasEmptyTitles = true;
                        break;
                    }

                    if (DeskAlertsUtils.HTMLToText(keyValue).Length >= 255)
                    {
                        hasBigTitles = true;
                        break;
                    }
                }
            }

            string htmlTitleValue = Settings.Content["ConfEnableHtmlAlertTitle"].Equals("1") ? htmlTitle.InnerHtml : "";

            if (autoCloseCheckBox.Checked)
            {
                if (autoClosebox.Value.Equals("0") || autoClosebox.Value.Length < 1)
                {
                    ShowPromt(Response, resources.LNG_EDIT_ALERT_DB_ALERT1);
                    return false;
                }
            }

            if (digitalSignage.Equals("1") == false && lifeTimeCheckBox.Checked)
            {
                if (lifetime.Value.Equals("0") || lifetime.Value.Length < 1)
                {
                    ShowPromt(Response, resources.LNG_YOU_SHOULD_ENTER_LIFETIME);
                    return false;
                }
            }

            if (hasBigTitles)
            {
                ShowPromt(Response, resources.LNG_TITLE_SHOULD_BE_LESS_THEN);
                return false;
            }

            if (hasEmptyTitles)
            {
                ShowPromt(Response, resources.LNG_YOU_SHOULD_ENTER_ALERT_TITLE);
                return false;
            }

            if ((!string.IsNullOrEmpty(Request["desktopCheck"])) && (!string.IsNullOrEmpty(Request["emailCheck"])) && !blogPost.Checked && !socialMedia.Checked &&
                !checkSms())
            {
                ShowPromt(Response, resources.LNG_YOU_SHOULD_SELECT_ALERT_TYPE);
                return false;
            }

            // Сhecking the need to validate the date.
            bool recurrenceCheckboxChecked = false;
            Boolean.TryParse(recurrenceCheckboxValue.Value, out recurrenceCheckboxChecked);

            if (recurrenceCheckboxChecked)
            {
                ReccurencePanelError err = schedulePanel.ValidateInputs();

                if (err == ReccurencePanelError.TO_DATE_LESS_THAN_FROM_DATE)
                {
                    ShowPromt(Response, resources.LNG_REC_DATE_ERROR);
                    return false;
                }

                if (err == ReccurencePanelError.TIME_VALUE_IS_EMPTY)
                {
                    ShowPromt(Response, resources.LNG_TIME_VALUE_IS_INCORRECT);
                    return false;
                }

                if (err == ReccurencePanelError.TO_DATE_LESS_THAN_CURRENT_DATETIME)
                {
                    ShowPromt(Response, resources.LNG_TO_DATE_LESS_THAN_CURRENT_DATE);
                    return false;
                }
            }

            return true;
        }

        private void saveNextButton_Click(object sender, EventArgs e)
        {
            if (!ValidateData())
                return;

            if (Settings.Content["ConfEnableHtmlAlertTitle"].Equals("1"))
                titleHidden.Value = htmlTitle.InnerText;

            SubmitForm(false);
        }

        private void SubmitForm(bool isDraft)
        {
            string IsDraftStr = isDraft.ToString().ToLower();

            form1.Action = "AddAlert.aspx?edit=" + Request["edit"] + "&instant=" + Request["instant"] + "&isModerated=" + Request["shouldApprove"];
            form1.Method = "POST";
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "SUBMIT", "submitForm(" + IsDraftStr + ")", true);
        }

        private void ChangeTab(string tabName)
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "CheckTab", "check_tab('" + tabName + "')", true);
        }

        [WebMethod]
        public static string GetTemplate(int templateId)
        {
            DBManager dbManager = new DBManager();
            string templateTextObj = dbManager.GetScalarQuery<string>("SELECT template_text FROM text_templates WHERE id = " + templateId);

            return templateTextObj;
        }

        private void LoadTemplate(int templateId)
        {
            DataRow templateRow = dbMgr.GetDataByQuery("SELECT name, template_text FROM text_templates WHERE id = " + templateId).First();

            string name = templateRow.GetString("name");
            string content = templateRow.GetString("template_text");

            if (Settings.Content["ConfEnableHtmlAlertTitle"].Equals("1"))
            {
                htmlTitle.InnerHtml = name;
                titleHidden.Value = name;
            }
            else
            {
                simpleTitle.Value = name;
            }

            alertContent.InnerHtml = content;
        }

        private void LoadAlert(int alertId)
        {
            if (IsPostBack)
                return;

            DataSet alertSet = dbMgr.GetDataByQuery("SELECT * FROM alerts WHERE id = " + alertId);

            DataRow alertRow = alertSet.GetRow(0);
            string title = alertRow.GetString("title");

            string alertContent = alertRow.GetString("alert_text");

            if (alertContent.IndexOf("<!-- printable_alert -->") >= 0)
            {
                printAlertCheckBox.Checked = true;
            }

            if (Settings.Content["ConfEnableHtmlAlertTitle"].Equals("1"))
            {
                string htmlTitle = title;

                htmlTitle = HttpUtility.HtmlDecode(htmlTitle);
                MatchCollection matches = Regex.Matches(alertContent, "<!-- html_title *= *(['\"])(.*?)\\1 *-->");

                if (matches.Count > 0)
                {
                    int lastRegex = matches.Count - 1;
                    if (matches[lastRegex].Groups.Count > 0)
                        htmlTitle = matches[lastRegex].Groups[2].Value;
                }

                var alertLanguagesContents = new AlertLanguagesList(alertContent, htmlTitle, dbMgr);

                foreach (var pair in alertLanguagesContents.languagesContents)
                {
                    var langCode = pair.Key;
                    var langContent = pair.Value;

                    var langName = dbMgr.GetScalarByQuery<string>($"SELECT name FROM alerts_langs WHERE abbreviatures = {langCode.Quotes()}");

                    if (!langContent.DefaultLanguage)
                    {
                        AddLanguageControls(langName, langCode, langContent.Content, langContent.Title);
                    }
                    else
                    {
                        defaultLanguage.Text = "<br /><b>" + langName + "</b>:";
                        this.htmlTitle.InnerHtml = langContent.Title;
                        htmlTitleHidden.Value = langContent.Title;
                        this.alertContent.InnerHtml = langContent.Content;
                        defaultLang.Value = langCode;
                    }
                }
            }
            else
            {
                simpleTitle.Value = title;
                this.alertContent.InnerHtml = alertContent;
            }

            if (alertContent.IndexOf("<!-- self-deletable -->") >= 0 && alertRow.GetInt("self_deletable") == 1)
            {
                selfDeletable.Checked = true;
            }

            if (alertRow.GetInt("urgent") == 1)
                urgentCheckBox.Checked = true;

            if (alertRow.GetInt("aknown") == 1)
            {
                aknown.Checked = true;
                unobtrusiveCheckbox.Disabled = true;
            }

            if (alertRow.GetInt("class") == 16)
            {
                unobtrusiveCheckbox.Checked = true;
                autoCloseCheckBox.Disabled = true;
                aknown.Disabled = true;
            }

            DateTime fromDateTime = alertRow.GetDateTime("from_date");
            DateTime toDateTime = alertRow.GetDateTime("to_date");

            int ticker = alertRow.GetInt("ticker");
            tickerSpeed = alertRow.GetInt("ticker_speed");
            tickerSpeedValue.Value = tickerSpeed.ToString();

            lifeTimeCheckBox.Checked = false;
            if (alertRow.GetInt("lifetime") == 1)
            {
                lifeTimeCheckBox.Checked = true;

                int lifeTime = (int)(toDateTime - fromDateTime).TotalMinutes;

                foreach (ListItem item in lifetimeFactor.Items)
                {
                    item.Selected = false;
                }
                for (int i = 2; i >= 0; i--)
                {
                    ListItem listItem = lifetimeFactor.Items[i];
                    int itemValue = Convert.ToInt32(listItem.Value);

                    int lifeTimeValue = lifeTime / itemValue;
                    if (lifeTime % itemValue == 0)
                    {
                        listItem.Selected = true;
                        lifeTimeCheckBox.Checked = true;
                        lifetime.Value = lifeTimeValue.ToString();
                        break;
                    }
                }
            }
            else if (alertRow.GetInt("schedule") == 1)
            {
                schedulePanel.AlertId = alertId;
                recurrenceCheckbox.Checked = true;
            }

             desktopCheck.Checked = true;

            if (alertRow.GetInt("autoclose") != 0)
            {
                autoCloseCheckBox.Checked = true;
                autoClosebox.Value = Math.Abs(alertRow.GetInt("autoclose") / 60).ToString();

                if (alertRow.GetInt("autoclose") < 0)
                {
                    manualClose.Checked = true;
                }
            }

            var skinPreviewImageName = string.Empty;

            if (alertRow.GetInt("fullscreen") == 1)
            {
                size1.Checked = false;
                size2.Checked = true;
                alertWidth.Value = alertRow.GetString("alert_width");
                alertHeight.Value = alertRow.GetString("alert_height");
                alertWidth.Disabled = true;
                alertHeight.Disabled = true;
                resizable.Disabled = false;
            }
            else
            {
                size1.Checked = true;
                size2.Checked = false;
                if (ticker == 0)
                {
                    positionBottomRight.Checked = false;
                    positionBottomLeft.Checked = false;
                    positionCenter.Checked = false;
                    positionTopRight.Checked = false;
                    positionTopLeft.Checked = false;
                    switch (alertRow.GetInt("fullscreen"))
                    {
                        case 2:
                            {
                                positionTopLeft.Checked = true;
                                break;
                            }
                        case 3:
                            {
                                positionTopRight.Checked = true;
                                break;
                            }
                        case 4:
                            {
                                positionCenter.Checked = true;
                                break;
                            }
                        case 5:
                            {
                                positionBottomLeft.Checked = true;
                                break;
                            }
                        case 6:
                            {
                                positionBottomRight.Checked = true;
                                break;
                            }
                    }

                    alertWidth.Value = alertRow.GetString("alert_width");
                    alertHeight.Value = alertRow.GetString("alert_height");

                    if (alertRow.GetInt("resizable") == 1)
                    {
                        resizable.Checked = true;
                    }

                    skinPreviewImageName = "thumbnail.png";
                }
                else
                {
                    ChangeTab("ticker");

                    switch (alertRow.GetString("fullscreen"))
                    {
                        case "7":
                            {
                                positionTop.Checked = true;
                                break;
                            }
                        case "8":
                            {
                                positionMiddle.Checked = true;
                                break;
                            }
                        case "9":
                            {
                                positionBottom.Checked = true;
                                break;
                            }
                        case "D":
                            {
                                positionDocked.Checked = true;
                                break;
                            }
                    }

                    skinPreviewImageName = "thumbnail_tick.png";
                }
            }

            FillSkinsData(alertRow, skinPreviewImageName);

            if (Config.Data.HasModule(Modules.EMAIL) && alertRow.GetInt("email") == 1)
            {
                emailCheck.Checked = true;
                // mailSender.Value = alertRow.GetString("email_sender");
            }

            if (Config.Data.HasModule(Modules.TWITTER) && alertRow.GetInt("social_media") == 1)
            {
                socialMedia.Checked = true;
            }

            if (Config.Data.HasModule(Modules.BLOG) && alertRow.GetInt("post_to_blog") == 1)
            {
                blogPost.Checked = true;
            }

            if (Config.Data.HasModule(Modules.SMS) && alertRow.GetInt("sms") == 1)
            {
                sms.Checked = true;
            }

            if (Config.Data.HasModule(Modules.TEXTTOCALL) && alertRow.GetInt("text_to_call") == 1)
            {
                textToCall.Checked = true;
            }

            if (Config.Data.HasModule(Modules.MOBILE))
            {
                int deviceType = alertRow.GetInt("device");

                desktopDevice.Attributes.Remove("checked");
                mobileDevice.Attributes.Remove("checked");
                allDevices.Attributes.Remove("checked");
                switch (deviceType)
                {
                    case 1:
                        {
                            desktopDevice.Checked = true;
                            desktopDevice.Attributes.Add("checked", "true");
                            break;
                        }
                    case 2:
                        {
                            mobileDevice.Attributes.Add("checked", "true");
                            break;
                        }
                    default:
                        {
                            allDevices.Attributes.Add("checked", "true");
                            break;
                        }
                }
            }

            DataSet surveySet = dbMgr.GetDataByQuery("SELECT id from surveys_main WHERE sender_id = " + alertId);
            if (!surveySet.IsEmpty)
            {
                int surveyId = surveySet.GetInt(0, "id");

                DataSet firstQuestionSet =
                    dbMgr.GetDataByQuery(
                        "SELECT question, id  FROM surveys_questions WHERE question_number = 1 AND survey_id = " +
                        surveyId);

                question.InnerHtml = HttpUtility.HtmlDecode(firstQuestionSet.GetString(0, "question"));

                DataSet firstQuestionOptions =
                    dbMgr.GetDataByQuery("SELECT answer FROM surveys_answers WHERE question_id = " +
                                         firstQuestionSet.GetString(0, "id"));

                question_option1.InnerHtml = HttpUtility.HtmlDecode(firstQuestionOptions.GetString(0, "answer"));
                question_option2.InnerHtml = HttpUtility.HtmlDecode(firstQuestionOptions.GetString(1, "answer"));

                DataSet secondQuestionSet =
                    dbMgr.GetDataByQuery(
                        "SELECT question, id  FROM surveys_questions WHERE question_number = 2 AND survey_id = " +
                        surveyId);

                if (!secondQuestionSet.IsEmpty)
                {
                    need_second_question.Checked = true;
                    second_question.InnerHtml = HttpUtility.HtmlDecode(secondQuestionSet.GetString(0, "question"));
                }
            }
            else
            {
                question.InnerText = resources.LNG_WILL_YOU_BE_ATTENDING;
                question_option1.InnerText = resources.LNG_YES;
                question_option2.InnerText = resources.LNG_NO;
                second_question.InnerText = resources.LNG_PLEASE_DESCRIBE_YOUR_REASON;
            }

            if (!Config.Data.HasModule(Modules.CAMPAIGN) || alertRow.GetInt("campaign_id") == -1)
            {
                campaignLabel.Visible = false;
                campaignSelect.Value = "-1";
            }
            else
            {
                int campaignId = alertRow.GetInt("campaign_id");

                string campName = dbMgr.GetScalarByQuery("SELECT name FROM campaigns WHERE id = " + campaignId).ToString();
                campaignName.InnerText = campName;
                campaignSelect.Value = campaignId.ToString();
            }

            object colorValueObj =
                dbMgr.GetScalarByQuery("SELECT [color] FROM color_codes WHERE id = '" + alertRow.GetString("color_code") + "'");

            string color = "747474";
            if (colorValueObj != null)
                color = colorValueObj.ToString();

            changeColorCode.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#" + color);

            colorСode.Value = alertRow.GetString("color_code");
        }

        private void FillSkinsData(DataRow alertRow, string skinPreviewImageName)
        {
            var skinId = "default";

            if (!alertRow.IsNull("caption_id"))
            {
                skinId = alertRow.GetString("caption_id");
            }

            var temporarySkinId = string.Equals(skinId, "default", StringComparison.CurrentCultureIgnoreCase)
                ? skinId
                : $"{{{skinId}}}";

            skinId = _skinService.CheckSkinIdInPolicy(skinId, curUser.Policy.SkinList);

            if (!string.Equals(skinId, temporarySkinId, StringComparison.CurrentCultureIgnoreCase))
            {
                Response.ShowJavaScriptAlert(resources.LNG_POLICY_CHANGE_SKINS_TO_DEFAULT, true);
            }

            skin_id.Value = skinId;
            skinPreviewImg.Src = $"skins/{skinId}/{skinPreviewImageName}";
        }

        private void AddLanguageControls(string name, string code, string text, string title)
        {
            langs.Value += code + ",";
            Literal languageName = new Literal()
            {
                Text = "<b>" + name + "</b>:"
            };

            var wrapper = new HtmlGenericControl("div");
            wrapper.Attributes.Add("class", "wrapper_" + code);
            wrapper.Style.Add(HtmlTextWriterStyle.Position, "relative");

            var loaderDiv = new HtmlGenericControl("div");
            loaderDiv.Attributes.Add("class", "loader_" + code);

            loaderDiv.Attributes.Add("style", "top: 0px; height: 100%; width: 100%; position: absolute; display: block; background-color: rgba(0, 0, 0, 0.1);display:none;z-index:1;");

            HtmlImage loaderImage = new HtmlImage()
            {
                Src = "images/loader.gif"
            };

            loaderImage.Attributes.Add("style", "position:absolute; top:70%; left:40%;");

            loaderDiv.Controls.Add(loaderImage);
            wrapper.Controls.Add(loaderDiv);
            wrapper.Controls.Add(new LiteralControl("<br /><br />"));
            wrapper.Controls.Add(languageName);
            wrapper.Controls.Add(new LiteralControl("<br />"));
            HtmlTextArea langTitleTextArea = new HtmlTextArea()
            {
                ClientIDMode = ClientIDMode.Static,
                ID = "htmlTitle" + code,
                Rows = 1,
                Cols = 50,
                InnerHtml = title
            };

            HtmlTable titleTable = new HtmlTable();
            HtmlTableRow titleRow = new HtmlTableRow();
            HtmlTableCell titleLabelCell = new HtmlTableCell();
            titleLabelCell.InnerText = resources.LNG_TITLE + ":";
            titleLabelCell.Style.Add(HtmlTextWriterStyle.VerticalAlign, "bottom");
            HtmlTableCell titleCell = new HtmlTableCell();
            titleCell.Style.Add(HtmlTextWriterStyle.Width, "535px");
            titleCell.Controls.Add(langTitleTextArea);
            titleRow.Cells.Add(titleLabelCell);
            titleRow.Cells.Add(titleCell);

            if (Settings.Content["ConfUseGoogleTranslate"] == "1")
            {
                HtmlTableCell translateCell = new HtmlTableCell();
                LinkButton translateButton = new LinkButton()
                {
                    CssClass = "preview_button translate_button",
                    Text = "Translate",
                };

                translateButton.Attributes["href"] = "#";
                translateButton.Attributes["lang"] = code;
                translateButton.Style.Add(HtmlTextWriterStyle.MarginLeft, "30px");
                translateCell.Controls.Add(translateButton);
                titleRow.Cells.Add(translateCell);
            }

            titleTable.Rows.Add(titleRow);

            langTitleTextArea.Attributes.Add("class", "htmlTitle");
            //  langTitleTextArea.Style.Add(HtmlTextWriterStyle.Width, "410px");
            wrapper.Controls.Add(titleTable);
            wrapper.Controls.Add(new LiteralControl("<br />"));
            HtmlTextArea langContentTextArea = new HtmlTextArea()
            {
                ClientIDMode = ClientIDMode.Static,
                ID = "alertContent_" + code,
                Rows = 15,
                Cols = 80,
                InnerHtml = text
            };

            langContentTextArea.Attributes.Add("class", "alertContent");
            langContentTextArea.Style.Add(HtmlTextWriterStyle.Width, "100%");
            wrapper.Controls.Add(langContentTextArea);

            HtmlInputHidden hiddenTitle = new HtmlInputHidden()
            {
                ClientIDMode = ClientIDMode.Static,
                ID = "htmlTitle" + code + "Hidden",
                Value = title
            };

            wrapper.Controls.Add(hiddenTitle);

            multilangsControls.Controls.Add(wrapper);
        }

        private void RestoreDefaultSettings()
        {
            desktopCheck.Checked = true;

            if (Config.Data.HasModule(Modules.EMAIL))
                emailCheck.Checked = Settings.Content["ConfEmailByDefault"] == "1";

            if (Config.Data.HasModule(Modules.SMS))
                sms.Checked = Settings.Content["ConfSMSByDefault"] == "1";

            size1.Checked = Settings.Content["ConfPopupType"] == "W";
            size2.Checked = Settings.Content["ConfPopupType"] == "F";

            positionBottomLeft.Checked = Settings.Content["ConfAlertPosition"] == "5";
            positionBottomRight.Checked = Settings.Content["ConfAlertPosition"] == "6";
            positionCenter.Checked = Settings.Content["ConfAlertPosition"] == "4";
            positionTopLeft.Checked = Settings.Content["ConfAlertPosition"] == "2";
            positionTopRight.Checked = Settings.Content["ConfAlertPosition"] == "3";

            alertWidth.Value = Settings.Content["ConfAlertWidth"];
            alertHeight.Value = Settings.Content["ConfAlertHeight"];

            positionTop.Checked = Settings.Content["ConfTickerPosition"] == "7";
            positionMiddle.Checked = Settings.Content["ConfTickerPosition"] == "8";
            positionBottom.Checked = Settings.Content["ConfTickerPosition"] == "9";
            positionDocked.Checked = Settings.Content["ConfTickerPosition"] == "D";

            urgentCheckBox.Checked = Settings.Content["ConfUrgentByDefault"] == "1";
            aknown.Checked = Settings.Content["ConfAcknowledgementByDefault"] == "1";
            unobtrusiveCheckbox.Checked = Settings.Content["ConfUnobtrusiveByDefault"] == "1";
            selfDeletable.Checked = Settings.Content["ConfSelfDeletableByDefault"] == "1";
            autoCloseCheckBox.Checked = Settings.Content["ConfAutocloseByDefault"] == "1";
            autoClosebox.Value = Settings.Content["ConfAutocloseValue"];

            manualClose.Checked = Settings.Content["ConfAllowManualCloseByDefault"] == "1";
            recurrenceCheckbox.Checked = Settings.Content["ConfScheduleByDefault"] == "1";

            int lifeTime = Convert.ToInt32(Settings.Content["ConfMessageExpire"]);

            foreach (ListItem item in lifetimeFactor.Items)
            {
                item.Selected = false;
            }
            for (int i = 2; i >= 0; i--)
            {
                ListItem listItem = lifetimeFactor.Items[i];
                int itemValue = Convert.ToInt32(listItem.Value);

                int lifeTimeValue = lifeTime / itemValue;
                if (lifeTime % itemValue == 0)
                {
                    listItem.Selected = true;
                    lifeTimeCheckBox.Checked = true;
                    lifetime.Value = lifeTimeValue.ToString();
                    break;
                }
            }
        }

        /// <summary>
        /// Hide the button that have to be hidden in Draft edit mode
        /// </summary>
        /// <param name="button">name of element</param>
        public void HideButtonSaveAndNext(LinkButton button)
        {
            if (button != null)
            {
                button.Visible = false;
            }
        }


        [WebMethod]
        public static string ValidateEmailSettings()
        {
            var emailSettings = EmailModuleSettings.GetSettingsFromDataBase();

            if (emailSettings.IsValid)
                return string.Empty;

            string message = string.Empty;
            if (string.IsNullOrEmpty(emailSettings.EmailServer))
                message += "Please specify your email server address in settings.<br>";

            if (string.IsNullOrEmpty(emailSettings.EmailFrom))
                message += "Please specify your sender email address in settings.<br>";

            if (emailSettings.EmailPort > 0)
                message += "Please specify your email server port in settings.<br>";

            return message;
        }

        [WebMethod]
        public static string ValidateSmsSettings()
        {
            var smsUrl = Settings.Content["ConfSmsUrl"];
            var message = string.Empty;

            if (string.IsNullOrEmpty(smsUrl) && Settings.Content.GetInt("ConfSmppEnabled") != 1)
                message += "Please specify URL of your sms server in settings<br>";

            return message;
        }

        [WebMethod]
        public static string ValidateTextToCallSettings()
        {
            var textToCallSID = Settings.Content["ConfTextToCallSID"];
            var textToCallAuthToken = Settings.Content["ConfTextToCallAuthToken"];
            var textToCallNumber = Settings.Content["ConfTextToCallNumber"];
            var message = string.Empty;

            if (string.IsNullOrEmpty(textToCallSID))
            {
                message += "Please specify SID of your Twilio account in settings<br>";
            }

            if (string.IsNullOrEmpty(textToCallAuthToken))
            {
                message += "Please specify auth token of your of your Twilio account in settings<br>";
            }

            if (string.IsNullOrEmpty(textToCallNumber))
            {
                message += "Please specify the phone number of your Twilio account in settings<br>";
            }

            return message;
        }

        [WebMethod]
        public static AlertContent TranslateText(string code, string text, string title)
        {
            var service = new TranslateService(new BaseClientService.Initializer()
            {
                ApiKey = Settings.Content["ConfGoogleTranslateKey"],
            });

            AlertContent content = new AlertContent();

            if (!string.IsNullOrEmpty(text))
                content.Content = service.Translations.List(text, code).Execute().Translations[0].TranslatedText;
            else
                content.Content = string.Empty;

            if (!string.IsNullOrEmpty(title))
                content.Title = service.Translations.List(title, code).Execute().Translations[0].TranslatedText;
            else
                content.Title = string.Empty;

            return content;
        }

        [WebMethod]
        public static string HtmlToText(string htmlText)
        {
            var result = DeskAlertsUtils.EscapeHtmlTagParagraph(htmlText);
            result = DeskAlertsUtils.EscapeXmlComments(result);
            result = DeskAlertsUtils.ExtractTextFromHtml(result);
            return result;
        }

        private void ShowContentSettingsByPolicy()
        {
            var contentOptionsByPolicy = curUser.Policy.ContentSettingsList;

            var isAcknowledgement = contentOptionsByPolicy.Contains(ContentSettings.GetName(ContentSettings.ContentSettingsEnum.Acknowledgement));
            ChangeHtmlControlVisibleStatus(aknown_div, isAcknowledgement);

            var isAutoClose = contentOptionsByPolicy.Contains(ContentSettings.GetName(ContentSettings.ContentSettingsEnum.AutoClose));
            ChangeHtmlControlVisibleStatus(aut_close_div, isAutoClose);

            var isFullScreen = contentOptionsByPolicy.Contains(ContentSettings.GetName(ContentSettings.ContentSettingsEnum.FullScreen));
            ChangeHtmlControlVisibleStatus(FullScreenOption, isFullScreen);

            var isHighPriority = contentOptionsByPolicy.Contains(ContentSettings.GetName(ContentSettings.ContentSettingsEnum.HighPriority));
            ChangeHtmlControlVisibleStatus(urgent_div, isHighPriority);

            var isPrintButton = contentOptionsByPolicy.Contains(ContentSettings.GetName(ContentSettings.ContentSettingsEnum.PrintButton));
            ChangeHtmlControlVisibleStatus(print_div, isPrintButton);

            var isSelfDestructing = contentOptionsByPolicy.Contains(ContentSettings.GetName(ContentSettings.ContentSettingsEnum.SelfDestructing));
            ChangeHtmlControlVisibleStatus(self_deletable_div, isSelfDestructing);

            var isUnobtrusive = contentOptionsByPolicy.Contains(ContentSettings.GetName(ContentSettings.ContentSettingsEnum.Unobtrusive));
            ChangeHtmlControlVisibleStatus(unobtrusive_div, isUnobtrusive);
        }

        private void ChangeHtmlControlVisibleStatus(HtmlControl htmlControl, bool isVisible)
        {
            if (htmlControl == null)
            {
                return;
            }

            if (isVisible)
            {
                htmlControl.Disabled = false;
                htmlControl.Visible = true;
            }
            else
            {
                htmlControl.Disabled = true;
                htmlControl.Visible = false;
            }
        }
    }
}