<html>
<head>
<title>CSV Upload Form</title>
<script language="javascript" type="text/javascript" src="jscripts/livevalidation_standalone.compressed.js"></script>
<style>
.LV_validation_message{
    font-weight:bold;
    margin:0 0 0 5px;
}

.LV_valid {
    color:#00CC00;
}
    
.LV_invalid {
    color:#CC0000;
}
  
.LV_invalid_field, 
input.LV_invalid_field:hover, 
input.LV_invalid_field:active,
textarea.LV_invalid_field:hover, 
textarea.LV_invalid_field:active {
    border: 1px solid #CC0000;
}
</style>
</head>
<body>
	<form action="paypal_uploadfile_csv.asp" method="post" enctype="multipart/form-data">

		<table border="0" width="320" align="left" cellspacing="0" cellpadding="2">
		<tr>
			<td colspan="2" align="left" class="text">Select the file to upload:<br><br></td>		
		</tr>
		<tr>
			<td class="text">
				<input type="file" name="file1" id="file1"><br><br>	
			</td>
		</tr>
		<tr>
			<td align="left">
				<input type="submit" value="Upload" name="submit">
			</td>
		</tr>
		</table>
		
	</form>

<script language="javascript" type="text/javascript" >
var captcha_code = new LiveValidation( 'file1', {onlyOnSubmit: true, validMessage: " " } );
captcha_code.add( Validate.Presence , {failureMessage: "Select a file"}  );
</script>

</body>
</html>