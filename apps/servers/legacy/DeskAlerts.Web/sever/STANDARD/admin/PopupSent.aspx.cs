using DeskAlerts.Server.Utils;
using DeskAlerts.Server.Utils.Helpers;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Models.Permission;
using DeskAlertsDotNet.Parameters;
using DeskAlertsDotNet.Server.Domain;
using System;
using System.IO;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Castle.MicroKernel.Lifestyle;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Enums;
using DeskAlerts.ApplicationCore.Extensions.Enums;
using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.Server.Utils.Interfaces.Services;
using DeskAlertsDotNet.Application;

namespace DeskAlertsDotNet.Pages
{
    using Resources;
    using System.Web.UI.HtmlControls;

    public partial class PopupSent : DeskAlertsBaseListPage
    {
        bool shouldDisplayApprove = false;
        protected string emailErrorMessage = string.Empty;
        protected string smsErrorMessage = string.Empty;

        private readonly ICacheInvalidationService _cacheInvalidationService;
        private readonly IScheduledContentService _scheduledContentService;

        public PopupSent()
        {
            using (Global.Container.BeginScope())
            {
                _cacheInvalidationService = Global.Container.Resolve<ICacheInvalidationService>();
                _scheduledContentService = Global.Container.Resolve<IScheduledContentService>();
            }
        }

        protected override HtmlInputText SearchControl => searchTermBox;

        protected override bool IsUsedDefaultTableSettings => false;

        protected override void OnLoad(EventArgs e)
        {
            try
            {
                base.OnLoad(e);

                bool isDraft = !String.IsNullOrEmpty(Request["draft"]) && Request["draft"].Equals("1");

                searchButton.Text = searchButton.Text.Replace("{Search}", resources.LNG_SEARCH);
                headerTitle.Text = resources.LNG_ALERTS;
                typeCell.Text = resources.LNG_TYPE;
                typeCell.HorizontalAlign = HorizontalAlign.Center;
                titleCell.Text = GetSortLink(resources.LNG_TITLE, "title", Request["sortBy"]);
                creationDateCell.Text = GetSortLink(resources.LNG_CREATION_DATE, "create_date",
                    Request["sortBy"]);

                string getUrlSearchTermBox = Request.QueryString["searchTermBox"] ?? string.Empty;

                scheduledCell.Text = resources.LNG_SCHEDULED;
                Table = contentTable;
                workHeader.InnerText = !isDraft
                                           ? resources.LNG_SENT_ALERTS
                                           : resources.LNG_DRAFT_ALERTS;

                shouldDisplayApprove = Config.Data.HasModule(Modules.APPROVE) &&
                                       Settings.Content["ConfAllowApprove"].Equals("1");

                if (!string.IsNullOrEmpty(Request["user_id"]))
                {
                    addButton.Visible = false;
                }

                emailErrorMessage = Request.GetParam("emailErrorMessage", string.Empty);
                smsErrorMessage = Request.GetParam("smsErrorMessage", string.Empty);

                headerSelectAll.Visible = curUser.HasPrivilege || curUser.Policy[Policies.ALERTS].CanDelete;
                selectAll.Visible = curUser.HasPrivilege || curUser.Policy[Policies.ALERTS].CanDelete;
                bottomDelete.Visible = curUser.HasPrivilege || curUser.Policy[Policies.ALERTS].CanDelete;
                upDelete.Visible = curUser.HasPrivilege || curUser.Policy[Policies.ALERTS].CanDelete;
                if (shouldDisplayApprove)
                    approveCell.Text = resources.LNG_APPROVE_STATUS;
                else
                    approveCell.Visible = false;

                senderCell.Text = resources.LNG_SENT_BY;
                actionsCell.Text = resources.LNG_ACTIONS;

                if (!curUser.HasPrivilege && !curUser.Policy[Policies.ALERTS].CanCreate)
                    addButton.Visible = false;
                bool isSearchTermBoxValueChanged = searchTermBox.Value != getUrlSearchTermBox;
                if (isSearchTermBoxValueChanged)
                {
                    Offset = 0;
                }
                int cnt = this.Content.Count < (Offset + Limit) ? this.Content.Count : Offset + Limit;

                for (int i = Offset; i < cnt; i++)
                {
                    DataRow row = this.Content.GetRow(i);
                    int reccurence = row.GetInt("recurrence");
                    var isRepeatableTemplate = row.GetBool("is_repeatable_template");

                    DateTime toDate = row.GetDateTime("to_date");
                    DateTime fromDate = row.GetDateTime("from_date");
                    bool hasLifetime = row.GetInt("lifetime") == 1;

                    bool isScheduled = row.GetString("schedule").Equals("1");

                    TableRow tableRow = new TableRow();

                    if (curUser.HasPrivilege || curUser.Policy[Policies.ALERTS].CanDelete)
                    {
                        CheckBox chb = GetCheckBoxForDelete(row);
                        TableCell checkBoxCell = new TableCell();
                        checkBoxCell.HorizontalAlign = HorizontalAlign.Center;
                        checkBoxCell.Controls.Add(chb);
                        tableRow.Cells.Add(checkBoxCell);
                    }

                    Image imageType = new Image();

                    bool isUrgent = this.Content.GetInt(i, "urgent") == 1;
                    string imagePath = string.Empty;

                    bool isRsvp = !this.Content.IsNull(i, "is_rsvp");

                    if (this.Content.GetInt(i, "ticker") == 1)
                    {
                        imagePath = isUrgent ? "images/alert_types/urgent_ticker.png" : "images/alert_types/ticker.png";
                        imageType.Attributes["title"] = isUrgent
                                                            ? resources.LNG_TICKER_URGENT
                                                            : resources.LNG_TICKER;

                        if (isRepeatableTemplate)
                        {
                            imagePath = "images/alert_types/repeatable_ticker.png";
                        }
                    }
                    else if (this.Content.GetInt(i, "fullscreen") == 1)
                    {
                        imagePath = isUrgent
                                        ? "images/alert_types/urgent_fullscreen.png"
                                        : "images/alert_types/fullscreen.png";
                        imageType.Attributes["title"] = isUrgent
                                                            ? resources.LNG_URGENT_FULLSCREEN
                                                            : resources.LNG_FULLSCREEN;
                    }
                    else if (this.Content.GetString(i, "type") == "L")
                    {
                        imagePath = "images/alert_types/linkedin.png";
                        imageType.Attributes["title"] = resources.LNG_ADD_LINKEDIN;
                    }
                    else if (isRsvp)
                    {
                        imagePath = isUrgent ? "images/alert_types/urgent_rsvp.png" : "images/alert_types/rsvp.png";
                        imageType.Attributes["title"] = isUrgent
                                                            ? resources.LNG_URGENT_RSVP
                                                            : resources.LNG_RSVP;

                        if (isRepeatableTemplate)
                        {
                            imagePath = "images/alert_types/repeatable_rsvp.png";
                        }
                    }
                    else
                    {
                        imagePath = "images/alert_types/new/" + this.Content.GetString(i, "class") + ".png";
                        string fullPath = Config.Data.GetString("alertsFolder") + "/admin/" + imagePath;


                        if (!File.Exists(fullPath))
                        {
                            imagePath = "images/alert_types/alert.png";

                            if (isRepeatableTemplate)
                            {
                                imagePath = "images/alert_types/repeatable_alert.png";
                            }
                        }
                        imageType.Attributes["title"] = DeskAlertsUtils.GetTooltipForClass(this.Content.GetInt(i, "class"));
                    }

                    bool isDigitalSignage = false;
                          
                    if (row.GetInt("class") == 2048)
                    {
                        isDigitalSignage = true;
                    }

                    imageType.ImageUrl = imagePath;


                    TableCell typeContentCell = new TableCell();
                    typeContentCell.HorizontalAlign = HorizontalAlign.Center;
                    typeContentCell.Controls.Add(imageType);
                    tableRow.Cells.Add(typeContentCell);

                    TableCell titleContentCell = new TableCell();
                    var id = row.GetString("id");
                    titleContentCell.Text = string.Format("<a href=javascript:showAlertPreview('{0}')>{1}</a>", id,
                        HttpUtility.HtmlDecode(DeskAlertsUtils.ExtractTextFromHtml(row.GetString("title"))));
                    tableRow.Cells.Add(titleContentCell);

                    if (shouldDisplayApprove)
                    {
                        string approveLocKey;
                        if (row.IsNull("approve_status"))
                        {
                            approveLocKey = "LNG_APPROVED";
                        }
                        else
                        {
                            var approveStatus = (ApproveStatus) row.GetInt("approve_status");
                            approveLocKey = approveStatus.GetStringValue();
                        }

                        var approveContentCell = new TableCell();
                        approveContentCell.HorizontalAlign = HorizontalAlign.Center;
                        approveContentCell.Text = resources.ResourceManager.GetString(approveLocKey);
                        tableRow.Cells.Add(approveContentCell);
                    }

                    tableRow.Cells.Add(new TableCell
                    {
                        Text = DateTimeConverter.ToUiDateTime(row.GetDateTime("create_date")),
                        HorizontalAlign = HorizontalAlign.Center
                    });

                    var scheduleCell = new TableCell();
                    if ((reccurence == 1 || isScheduled) && !hasLifetime)
                    {
                        scheduleCell.Text =
                            $"<span href =\"#\" onclick=\"disabled=true\"; \"openDialogUI('form', 'frame', 'rescheduler.aspx?id={id}',600,550, '');\">{resources.LNG_YES}</span>";
                    }
                    else
                    {
                        scheduleCell.Text = resources.LNG_NO;
                    }

                    scheduleCell.HorizontalAlign = HorizontalAlign.Center;
                    tableRow.Cells.Add(scheduleCell);

                    var senderContentCell = new TableCell
                    {
                        HorizontalAlign = HorizontalAlign.Center,
                        Text = string.IsNullOrEmpty(row.GetString("name")) ? resources.SENDER_WAS_DELETED : row.GetString("name")
                    };

                    tableRow.Cells.Add(senderContentCell);

                    int margin = 10;

                    string diplicateString = DefineDuplicateString(row, id);

                    diplicateString = string.Format("location.href='{0}'", diplicateString);
                    LinkButton duplicateButton = GetActionButton(id, "images/action_icons/duplicate.png",
                        "LNG_RESEND_ALERT", diplicateString, margin);

                    var resendPicturePath = "images/action_icons/resend.png";
                    var resendTitle = "LNG_RESEND_ALERT_2";
                    var resendScript = $"resendAlert({id});";

                    if (isScheduled)
                    {
                        resendPicturePath = "images/action_icons/resendDisabled.png";
                        resendTitle = "LNG_RESEND_ALERT_DISABLED";
                        resendScript = "return false;";
                    }

                    LinkButton resendButton = GetActionButton(id, resendPicturePath, resendTitle, resendScript, margin);

                    LinkButton sendButton = GetActionButton(id, "images/action_icons/send.png",
                        "LNG_SEND_ALERT",
                        delegate { Response.Redirect("RecipientsSelection.aspx?desktop=1&id=" + id, true); }, margin);

                    string editAlertUrl = $"CreateAlert.aspx?edit=1&id={id}";
                    string editAlertUrlParam = string.Empty;

                    if (row.GetInt("ticker") == 1)
                    {
                        editAlertUrlParam = "&ticker=1";
                    }
                    else if (!row.IsNull("is_rsvp"))
                    {
                        editAlertUrlParam = "&rsvp=1";
                    }

                    LinkButton editButton = GetActionButton(id, "images/action_icons/edit.png", "LNG_EDIT_ALERT",
                        delegate { Response.Redirect($"{editAlertUrl}{editAlertUrlParam}", true); }, margin
                    );

                    LinkButton changeScheduleType = new LinkButton();

                    int scheduleType = row.GetInt("schedule_type");

                    changeScheduleType = GetActionButton(id,
                                                         scheduleType == 1 ? "images/action_icons/stop.png" : "images/action_icons/start.png",
                                                         scheduleType == 1 ? "LNG_STOP_SCHEDULED_ALERT" : "LNG_START_SCHEDULED_ALERT",
                                                         delegate
                                                         {
                                                             var redirectResult = $"ChangeScheduleType.aspx?id={id}&type={Math.Abs(scheduleType - 1)}&{PageParams}" +
                                                                                  $"&SearchTermBox={UrlHelper.Search}&SortBy={UrlHelper.SortBy}&Offset={UrlHelper.Offset}&Limit={UrlHelper.Limit}";
                                                             Response.Redirect(redirectResult, true);
                                                         }, margin
                    );

                    LinkButton previewButton = GetActionButton(id, "images/action_icons/preview.png",
                        "LNG_PREVIEW", "showAlertPreview(" + id + ")", margin);

                    LinkButton directLinkButton = GetActionButton(id, "images/action_icons/link.png",
                        "LNG_DIRECT_LINK",
                        "openDirDialog(" + id + ")", margin);

                    var actionsContentCell = new TableCell {CssClass = "actionsButtonsTd"};

                    actionsCell.HorizontalAlign = HorizontalAlign.Center;

                    if (!isDraft)
                    {
                        if (!isDigitalSignage && !isRsvp)
                        {
                            if (curUser.HasPrivilege || row.GetInt("sender_id") == curUser.Id)
                            {
                                actionsContentCell.Controls.Add(resendButton);
                            }
                        }

                        if (curUser.HasPrivilege || curUser.Policy[Policies.ALERTS].CanEdit)
                        {
                            if (isRsvp)
                            {
                                duplicateButton.Style.Add(HtmlTextWriterStyle.MarginLeft, "8px");
                            }

                            actionsContentCell.Controls.Add(duplicateButton);
                        }

                        if ((curUser.HasPrivilege || curUser.Policy[Policies.ALERTS].CanStop))
                        {
                            actionsContentCell.Controls.Add(changeScheduleType);
                        }

                        if (!curUser.IsPublisher || curUser.Policy != null && curUser.Policy[Policies.STATISTICS].CanView)
                        {
                            LinkButton detailsButton;

                            var isStatisticEnabled = Config.Data.HasModule(Modules.STATISTICS);
                            if (isRepeatableTemplate)
                            {
                                detailsButton = GetActionButton(
                                    id,
                                    "images/action_icons/graph.png",
                                    "LNG_ALERT_DETAILS",
                                    delegate
                                    {
                                        Response.Redirect($"InstancesOfTemplate.aspx?templateId={id}", true);
                                    },
                                    margin);
                            }
                            else if (isStatisticEnabled && isRsvp)
                            {
                                detailsButton = SetRsvpDetailsButton(id, margin);
                            }
                            else if (isRsvp)
                            {
                                var surveys_id = dbMgr.GetScalarByQuery<string>($"select id from surveys_main  where sender_id='{id}'");
                                detailsButton = GetActionButton(
                                    surveys_id,
                                    "images/action_icons/graph.png",
                                    "LNG_ALERT_DETAILS",
                                    $"window.open('SurveyStatisticDetails.aspx?id={surveys_id}', '', 'status=0, toolbar=0, width=555, height=850, scrollbars=1')",
                                    margin);
                            }
                            else
                            {
                                detailsButton = GetActionButton(
                                    id,
                                    "images/action_icons/graph.png",
                                    "LNG_ALERT_DETAILS",
                                    $"window.open('AlertStatisticDetails.aspx?id={id}', '', 'status=0, toolbar=0, width=555, height=850, scrollbars=1')",
                                    margin);
                            }

                            if (!isDigitalSignage)
                            {
                                actionsContentCell.Controls.Add(detailsButton);
                            }
                        }

                        actionsContentCell.Controls.Add(previewButton);
                        actionsContentCell.Controls.Add(directLinkButton);
                    }
                    else
                    {
                        if (curUser.HasPrivilege || curUser.Policy[Policies.ALERTS].CanSend)
                            actionsContentCell.Controls.Add(sendButton);

                        if (curUser.HasPrivilege || curUser.Policy[Policies.ALERTS].CanEdit)
                            actionsContentCell.Controls.Add(editButton);

                        actionsContentCell.Controls.Add(previewButton);
                        actionsContentCell.Controls.Add(directLinkButton);
                    }

                    tableRow.Cells.Add(actionsContentCell);
                    contentTable.Rows.Add(tableRow);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                Logger.Debug(ex);
                throw;
            }
        }

        private string DefineDuplicateString(DataRow row, string id)
        {
            var diplicateString = "CreateAlert.aspx?id=" + id;

            if (!string.IsNullOrEmpty(Request["user_id"]))
            {
                diplicateString += "&webplugin=1";
                return diplicateString;
            }

            if (row.GetInt("ticker") == 1)
            {
                diplicateString += "&ticker=1";
            }
            else if (!row.IsNull("is_rsvp"))
            {
                diplicateString += "&rsvp=1";
            }
            else if (!string.IsNullOrEmpty(Request["user_id"]))
            {
                diplicateString = "CreateAlert.aspx?webplugin=1&id=" + id;
            }

            return diplicateString;
        }

        private LinkButton SetRsvpDetailsButton(string alertId, int margin)
        {
            try
            {
                var surveyId =
                    dbMgr.GetScalarByQuery<int>("SELECT id FROM surveys_main WHERE sender_id = " + alertId);

                var result = GetActionButton(alertId, "images/action_icons/graph.png",
                    "LNG_STATISTICS_REPORTS",
                    "window.open('WidgetSurveysDetails.aspx?id=" + surveyId +
                    "&sendAlertId=" + alertId + "','', 'status=0, toolbar=0, width=555, height=450, scrollbars=1')", margin);

                return result;
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                Logger.Debug(ex);
                throw;
            }
        }

        [WebMethod]
        public static string Resend(int alertId)
        {
            try
            {
                var contentSender = new ContentSender();
                var id = contentSender.ResendAlert(alertId);
                return id.ToString();
            }
            catch (Exception ex)
            {
                Logger.Debug(ex);
                return ex.Message;
            }
        }

        protected override void OnDeleteRows(string[] ids)
        {
            foreach (var id in ids)
            {
                var contentId = Convert.ToInt64(id);

                _scheduledContentService.DeleteInstancesOfTemplate(contentId);
                _cacheInvalidationService.CacheInvalidation(AlertType.SimpleAlert, contentId);
            }
        }

        /// <summary>
        /// Define parameters to add to Uri.
        /// Override <see cref="DeskAlertsBaseListPage"/> PageParams. Add "user_id" param for sorting table.
        /// </summary>
        protected override string PageParams
        {
            get
            {
                try
                {
                    var uri = string.Empty;
                    uri += AddParameter(uri, "draft");
                    uri += AddParameter(uri, "question_id");
                    uri += AddParameter(uri, "user_id");
                    return uri;
                }
                catch (Exception ex)
                {
                    Logger.Error(ex.Message);
                    Logger.Debug(ex);
                    throw;
                }
            }
        }

        #region DataProperties

        protected override string DataTable
        {
            get
            {
                return "alerts";
            }
        }

        protected override string[] Collumns
        {
            get
            {
                return new string[] { };
            }
        }

        protected override string DefaultOrderCollumn
        {
            get
            {
                if (!string.IsNullOrEmpty(Request["draft"]) && Request["draft"].Equals("1"))
                    return "create_date";

                return "sent_date";
            }
        }

        protected override string CustomQuery
        {
            get
            {
                try
                {
                    string viewListStr = string.Empty;

                    if (!this.curUser.HasPrivilege && !this.curUser.Policy.CanViewAll)
                    {
                        viewListStr = string.Join(",", this.curUser.Policy.SendersViewList.ToArray());
                    }

                    bool isDraft = !string.IsNullOrEmpty(Request["draft"]) && Request["draft"].Equals("1");
                    string query = string.Empty;
                    if (!curUser.HasPrivilege && !curUser.Policy.CanViewAll)
                    {
                        query =
                            "SELECT a.id, a.lifetime, a.approve_status, a.schedule, schedule_type, recurrence, a.sender_id, alert_text, title, a.type, a.sent_date, a.create_date, a.from_date, a.to_date, class, urgent, a.ticker, fullscreen, s.id as is_rsvp, u.name, a.is_repeatable_template FROM alerts a LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id LEFT JOIN users u on u.id = a.sender_id WHERE " +
                            (!isDraft ? "(a.type='S' OR a.type='L')" : "a.type='D'") +
                            " AND video = 0 AND param_temp_id IS NULL AND (recurrence IS NULL OR recurrence = 0 OR is_repeatable_template = 1) AND (class = 1 OR class = 16 OR class = 999 OR class = 3 OR class = 6" +
                            (string.IsNullOrEmpty(Request["user_id"]) ? string.Empty : " OR class = 2048") +
                            ") AND a.sender_id IN (" + viewListStr +
                            ") AND a.id NOT IN (SELECT alert_id FROM instant_messages) ";

                        if (!string.IsNullOrEmpty(Request["user_id"]))
                            query += " AND a.id IN (SELECT alert_id FROM alerts_sent WHERE user_id = " +
                                     Request["user_id"] + ")";
                    }
                    else
                    {
                        query =
                            "SELECT a.id, a.lifetime, a.approve_status, a.schedule, schedule_type, recurrence, a.sender_id, alert_text, title, a.type, a.sent_date, a.create_date, a.from_date, a.to_date, class, urgent, a.ticker, fullscreen, s.id as is_rsvp, u.name, a.is_repeatable_template FROM alerts a LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id LEFT JOIN users u on u.id = a.sender_id WHERE " +
                            (!isDraft ? "(a.type='S' OR a.type='L')" : "a.type='D'") +
                            " AND video = 0 AND param_temp_id IS NULL AND (recurrence IS NULL OR recurrence = 0 OR is_repeatable_template = 1) AND (class = 1 OR class = 16 OR class = 999 OR class = 3 OR class = 6" +
                            (string.IsNullOrEmpty(Request["user_id"]) ? string.Empty : " OR class = 2048") +
                            ")  AND a.id NOT IN (SELECT alert_id FROM instant_messages) ";

                        if (!string.IsNullOrEmpty(Request["user_id"]))
                        {
                            query +=
                                $" AND a.id IN (SELECT alert_id FROM alerts_sent WHERE user_id = {this.Request["user_id"]})";

                        }
                    }

                    if (SearchTerm.Length > 0)
                    {
                        query += $" AND title LIKE \'%{this.SearchTerm}%\'";
                    }

                    if (!string.IsNullOrEmpty(Request["sortBy"]))
                    {
                        query += $" ORDER BY a.{Request["sortBy"]}";
                    }
                    else
                    {
                        query += " ORDER BY a." + DefaultOrderCollumn + " DESC ";
                    }

                    return query;
                }
                catch (Exception ex)
                {
                    Logger.Error(ex.Message);
                    Logger.Debug(ex);
                    throw;
                }
            }
        }

        protected override string CustomCountQuery
        {
            get
            {
                try
                {
                    string viewListStr = string.Empty;

                    if (!curUser.HasPrivilege && !curUser.Policy.CanViewAll)
                    {
                        viewListStr = string.Join(",", curUser.Policy.SendersViewList.ToArray());
                    }

                    bool isDraft = !string.IsNullOrEmpty(Request["draft"]) && Request["draft"].Equals("1");
                    string query = string.Empty;
                    if (!curUser.HasPrivilege && !curUser.Policy.CanViewAll)
                    {
                        query =
                            "SELECT count(1) FROM alerts a LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id LEFT JOIN users u on u.id = a.sender_id WHERE " +
                            (!isDraft ? "(a.type='S' OR a.type='L')" : "a.type='D'") +
                            " AND video = 0 AND param_temp_id IS NULL AND (recurrence IS NULL OR recurrence = 0 OR is_repeatable_template = 1) AND (class = 1 OR class = 16 OR class = 999 OR class = 3 OR class = 6" +
                            (string.IsNullOrEmpty(Request["user_id"]) ? string.Empty : " OR class = 2048") +
                            ") AND a.sender_id IN (" + viewListStr +
                            ") AND a.id NOT IN (SELECT alert_id FROM instant_messages) ";

                        if (!string.IsNullOrEmpty(Request["user_id"]))
                            query += " AND a.id IN (SELECT alert_id FROM alerts_sent WHERE user_id = " +
                                     Request["user_id"] + ")";
                    }
                    else
                    {
                        query =
                            "SELECT count(1) FROM alerts a LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id LEFT JOIN users u on u.id = a.sender_id WHERE " +
                            (!isDraft ? "(a.type='S' OR a.type='L')" : "a.type='D'") +
                            " AND video = 0 AND param_temp_id IS NULL AND (recurrence IS NULL OR recurrence = 0 OR is_repeatable_template = 1) AND (class = 1 OR class = 16 OR class = 999 OR class = 3 OR class = 6" +
                            (string.IsNullOrEmpty(Request["user_id"]) ? string.Empty : " OR class = 2048") +
                            ")  AND a.id NOT IN (SELECT alert_id FROM instant_messages) ";

                        if (!string.IsNullOrEmpty(Request["user_id"]))
                            query += " AND a.id IN (SELECT alert_id FROM alerts_sent WHERE user_id = " +
                                     Request["user_id"] + ")";
                    }

                    if (/*IsPostBack && */SearchTerm.Length > 0)
                        query += " AND title LIKE '%" + SearchTerm + "%'";

                    return query;
                }
                catch (Exception ex)
                {
                    Logger.Error(ex.Message);
                    Logger.Debug(ex);
                    throw;
                }
            }
        }

        #endregion

        #region Interface properties

        protected override IButtonControl[] DeleteButtons
        {
            get { return new IButtonControl[] { upDelete, bottomDelete }; }
        }

        protected override HtmlGenericControl NoRowsDiv
        {
            get { return NoRows; }
        }

        protected override HtmlGenericControl TableDiv
        {
            get { return mainTableDiv; }
        }

        protected override string ContentName
        {
            get
            {
                return Request["draft"] == "1" ? resources.LNG_DRAFT_ALERTS : resources.LNG_SENT_ALERTS;
            }
        }

        protected override HtmlGenericControl BottomPaging
        {
            get { return bottomRanging; }
        }

        protected override HtmlGenericControl TopPaging
        {
            get { return topPaging; }
        }

        #endregion
    }
}