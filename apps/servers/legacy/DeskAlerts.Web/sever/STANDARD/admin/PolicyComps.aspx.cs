﻿namespace DeskAlertsDotNet.Pages
{
    using System;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;

    using DeskAlertsDotNet.Parameters;

    using Resources;

    public partial class PolicyComps : DeskAlertsBaseListPage
    {
        protected override HtmlInputText SearchControl => searchTermBox;

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            searchButton.Text = searchButton.Text.Replace("{Search}", resources.LNG_SEARCH);

            compName.Text = GetSortLink(resources.LNG_COMPUTER_NAME, "name", Request["sortBy"]);
            domain.Text = GetSortLink(resources.LNG_DOMAIN, "domains.name", Request["sortBy"]);

            var cnt = Content.Count < (Offset + Limit) ? Content.Count : Offset + Limit;

            for (var i = Offset; i < cnt; i++)
            {
                var row = Content.GetRow(i);
                var tableRow = new TableRow();
                var chb = GetCheckBoxForDelete(row);
                var checkBoxCell = new TableCell { HorizontalAlign = HorizontalAlign.Center };
                checkBoxCell.Controls.Add(chb);
                tableRow.Cells.Add(checkBoxCell);

                var compNameCell = new TableCell { Text = row.GetString("name") };
                chb.InputAttributes["object_name"] = row.GetString("name");
                tableRow.Cells.Add(compNameCell);

                var domainCell = new TableCell
                                     {
                                         Text = row.GetString("domain_name"),
                                         HorizontalAlign = HorizontalAlign.Center
                                     };
                tableRow.Cells.Add(domainCell);

                contentTable.Rows.Add(tableRow);
            }
        }

        protected override string[] Collumns => new string[] { };

        protected override string ContentName => resources.LNG_GROUPS;

        protected override string DataTable => "";

        protected override HtmlGenericControl NoRowsDiv => NoRows;

        protected override HtmlGenericControl TableDiv => mainTableDiv;

        protected override string DefaultOrderCollumn => "name";

        private string GetFilterForQuery()
        {
            var searchType = Request["type"];

            var searchId = Request["id"];

            var query = @"SET NOCOUNT ON IF OBJECT_ID('tempdb..#tempOUs') IS NOT NULL
                                    DROP TABLE #tempOUs
                                    CREATE TABLE #tempOUs (Id_child BIGINT NOT NULL, Rights BIT NOT NULL);

                                    IF OBJECT_ID('tempdb..#tempComputers') IS NOT NULL
                                    DROP TABLE #tempComputers
                                    CREATE TABLE #tempComputers (comp_id BIGINT NOT NULL);

                                    DECLARE @now DATETIME;

                                    SET @now = GETDATE();";

            if (searchType == "ou")
            {
                if (Request["search"] == "1")
                {
                    query += @"WITH OUs (id) AS
                                      (SELECT CAST(" + searchId + @" AS BIGINT)
                                       UNION ALL SELECT Id_child
                                       FROM OU_hierarchy h
                                       INNER JOIN OUs ON OUs.id = h.Id_parent)
                                    INSERT INTO #tempOUs (Id_child, Rights)
                                      (SELECT DISTINCT id,
                                                       " + searchId + @"
                                       FROM OUs) OPTION (MaxRecursion 10000);";
                }
                else
                {
                    query += @" INSERT INTO #tempOUs (Id_child, Rights) VALUES (" + searchId + ", 0)";
                }
            }
            else if (searchType == "DOMAIN")
            {
                query += @" INSERT INTO #tempOUs (Id_child, Rights) (SELECT id, 0 FROM OU WHERE Id_domain = " + searchId + ")";
            }
            else
            {
                query += " INSERT INTO #tempOUs (Id_child, Rights) (SELECT id, 0 FROM OU) ";
            }


            query += @" INSERT INTO #tempComputers(comp_id)
                      (SELECT DISTINCT computers.id
                       FROM computers 
                       INNER JOIN OU_comp c ON computers.id = c.Id_comp
                       INNER JOIN #tempOUs t ON c.Id_OU=t.Id_child";

            if (!string.IsNullOrEmpty(SearchTerm))
            {
                query += $" WHERE (computers.name LIKE N'%{SearchTerm}%')";
            }

            query += ")";

            if (searchType != "ou")
            {
                query += @" INSERT INTO #tempComputers(comp_id)
                              (SELECT DISTINCT computers.id
                               FROM computers
                               LEFT JOIN #tempComputers t ON t.comp_id=computers.id
                               WHERE t.comp_id IS NULL";

                if (searchType == "DOMAIN")
                {
                    query += " AND computers.domain_id = " + searchId;
                }

                if (!string.IsNullOrEmpty(SearchTerm))
                {
                    query += $" AND (computers.name LIKE N'%{SearchTerm}%')";
                }

                query += ")";

            }
            return query;
        }

        protected override string CustomQuery
        {
            get
            {
                var query = GetFilterForQuery();

                query += @"SELECT computers.id AS id,
                           context_id,
                           computers.name AS name,
                           next_request,
                           standby,
                           computers.domain_id,
                           domains.name AS domain_name,
                           reg_date,
                           last_date,
                           convert(varchar,last_request) AS last_request1,
                           last_request,
                           edir_context.name AS context_name
                    FROM #tempComputers t
                    INNER JOIN computers ON t.comp_id = computers.id
                    LEFT JOIN domains ON domains.id = computers.domain_id
                    LEFT JOIN edir_context ON edir_context.id = computers.context_id";

                if (!string.IsNullOrEmpty(Request["sortBy"]))
                {
                    query += " ORDER BY " + Request["sortBy"];
                }
                else
                {
                    query += " ORDER BY name";
                }

                query += @" DROP TABLE #tempOUs
                            DROP TABLE #tempComputers";

                return query;
            }
        }

        protected override string CustomCountQuery
        {
            get
            {
                var query = GetFilterForQuery();

                query += @" SELECT COUNT(1) AS cnt
                        FROM #tempComputers t
                        INNER JOIN computers ON t.comp_id = computers.id";
                return query;
            }
        }

        protected override HtmlGenericControl TopPaging => topPaging;

        protected override HtmlGenericControl BottomPaging => bottomPaging;
    }
    }
