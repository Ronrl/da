﻿using DeskAlerts.Server.Utils;
using DeskAlertsDotNet.DataBase;
using TweetSharp;

namespace DeskAlertsDotNet
{
    public class TwitterManager
    {
        private static TwitterManager _instance;
        public static TwitterManager Default
        {
            get
            {
                if (_instance == null)
                    return _instance = new TwitterManager();

                return _instance;
            }
        }

        private TwitterManager()
        {
            
        }

        public void UpdateStatus(DBManager dbMgr, string statusText)
        {
            statusText = DeskAlertsUtils.HTMLToText(statusText);

            DataSet tweetSet = dbMgr.GetDataByQuery("SELECT * FROM social_media");

            foreach (var tweetRow in tweetSet)
            {
                string consumerKey = tweetRow.GetString("field1");
                string consumerSecret = tweetRow.GetString("field2");
                string accessKey = tweetRow.GetString("field3");
                string accessSecret = tweetRow.GetString("field4");
                TwitterService twitterService = new TwitterService(consumerKey, consumerSecret);
                twitterService.AuthenticateWith(accessKey, accessSecret);
                SendTweetOptions options = new SendTweetOptions();
                options.Status = statusText;
                twitterService.SendTweet(options);
            }
            


        }
    }
}