﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LanguageList.aspx.cs" Inherits="DeskAlertsDotNet.Pages.LanguageList" %>
<%@ Import NameSpace="DeskAlertsDotNet.Parameters" %>
<%@ Import Namespace="Resources" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<title>DeskAlerts</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link href="css/style9.css" rel="stylesheet" type="text/css">
        <link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <link href="css/jquery/jquery_timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css">
        <script language="javascript" type="text/javascript" src="functions.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/date.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery_timepicker/jquery-ui-timepicker-addon.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/jquery/datepicker_fix.js"></script>
        <script language="javascript" type="text/javascript">


            $(document).ready(function() {
                $(".delete_button").button({
                });

                $("#selectAll").click(function () {
                    var checked = $(this).prop('checked');
                    $(".content_object").prop('checked', checked);
                });


            });

        </script>
</head>
<body>
    <form id="form1" runat="server">
    <table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	    <tr>
	    <td>
	    <table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
		    <tr>
		     <td width="100%" height="31" class="main_table_title">
		    <asp:LinkButton href="LanguageList.aspx" runat="server" id="headerTitle" class="header_title" style="position:absolute;top:22px" />
		    </td>

		    </tr>
            <tr>
            <td class="main_table_body" height="100%">
	                <br /><br />

			<div style="margin-left:10px" runat="server" id="mainTableDiv">

			   <table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr><td> 
					<div style="margin-left:10px" id="topPaging" runat="server"></div>
				</td></tr>
				</table>
				<br>
				<table height="100%" cellspacing="0" cellpadding="3" border="0">
				<tr>
					<td><%=resources.LNG_LANGUAGE_NAME%></td>
					<td><input runat="server" type="text" style="width:300px" id="langName" name="langName" value="" maxlength="50"/></td>
				</tr>
				<tr>
					<td><%=resources.LNG_LANGUAGE_CODE %></td>
					<td><input runat="server" type="text" style="width:300px" id="langСode" name="langСode" value="" maxlength="50"/></td>
				</tr>
				<tr>
					<td colspan="2" align="right">

						<asp:LinkButton class="delete_button" runat="server" id="addButton"></asp:LinkButton>

						<a class="delete_button" href="SystemConfigurationSettings.aspx"><%=resources.LNG_CANCEL %></a>
					</td>
				</tr>
				</table>				
				<br><br>
				<asp:Table style="padding-left: 0px;margin-left: 10px;margin-right: 10px;" runat="server" id="contentTable" Width="98.5%" Height="100%" CssClass="data_table" CellSpacing="0" CellPadding="3">
				<asp:TableRow CssClass="data_table_title">

					<asp:TableCell runat="server" id="valueCell" CssClass="table_title" ></asp:TableCell>
					<asp:TableCell runat="server" id="curValueCell" CssClass="table_title"></asp:TableCell>
				</asp:TableRow>
				</asp:Table>
				<br>
		
				 <br><br>
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				 
				<tr><td> 
					<div style="margin-left:10px" id="bottomRanging" runat="server"></div>
				</td></tr>
				</table>
			</div>
			<div runat="server" id="NoRows">
                <br />
                <br />
				<center><b><!-- Add here your message сcontent withou--></b></center>
			</div>
			<br><br>
            </td>
            </tr>
           
 
            
             </table>
            </table>
    </form>
</body>
</html>
