<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<!-- #include file="md5.asp" -->
<!-- #include file="functions_inc.asp" -->

<%
/// <reference path="preview_body.asp" />

'check_session()

uid = Session("uid")
'if (uid <> "")  then
    

if (request("name") = "GetTGroups") then

    SQL = "SELECT a.id, a.name FROM message_templates_groups as a "
    
      if(Request("value") <> "" and Request("collumn") <> "") then
	    SQL = SQL & " ORDER BY a." & Request("collumn") & " " & Request("value")
	    else
	    SQL =  SQL & " ORDER BY a.id DESC"
	    end if
	    
	    Set tgroupsSet = Conn.Execute(SQL)
	    
    data = ""
    do while not tgroupsSet.EOF
        id = tgroupsSet("id")
        name = tgroupsSet("name")
        
        data = data & tgroupsSet("id") & vbCr & tgroupsSet("name") & vbLf
        tgroupsSet.MoveNext
        
    loop
    
    Response.Write data
end if
if request("name") = "test" then
    
   res = ShouldSendPushNotification(1)
    Response.Write(res)
end if
if request("name") = "uns" then

    action = Request("action")
    
    user = Request("user")
    userId = ""
    if(user <> "") then
      Set userSet = Conn.Execute("SELECT id FROM users WHERE name='" & user & "'")
      userId = userSet("id")
    end if
    if action = "add" then
        fromDate = Request("from")
        toDate = Request("to")
        days = Request("days")
        
        SQL = "INSERT INTO unobtrusives ([to], [from], days, enabled, user_id) OUTPUT INSERTED.ID as newId VALUES ('" & toDate &"','" & fromDate &"','" & days &"', 1, "& userId& ")"
        
       Set newIdSet =  Conn.Execute(SQL)
       
       newId = newIdSet("newId")
       
       Response.Write newId
    elseif action = "del" then
        
         SQL = "DELETE FROM unobtrusives WHERE id = " & Request("id")
         Conn.Execute(SQL)
    elseif action = "get" then
    
        if(Request("param") = "") then
            SQL = "SELECT id, [to], [from], days, enabled FROM unobtrusives"
        elseif Request("param") <> "" then
           SQL = "SELECT id, [to], [from], days, enabled FROM unobtrusives WHERE id = " & Request("param")
        elseif userId <> "" then
            SQL = "SELECT id, [to], [from], days, enabled FROM unobtrusives WHERE user_id = " & userId
        end if
        
        Set unobtrusives = Conn.Execute(SQL)
        
        jsonData = "["
        do while not unobtrusives.EOF
            
            jsonData = jsonData & "{"
            jsonData = jsonData & """id"""  & ":" & """" & unobtrusives("id") & ""","
            jsonData = jsonData & """to"""  & ":" & """" & unobtrusives("to") & ""","
            jsonData = jsonData & """from"""  & ":" & """" & unobtrusives("from") & ""","
            jsonData = jsonData & """days"""  & ":" & """" & unobtrusives("days") & ""","
            jsonData = jsonData & """enabled"""  & ":" & """" & unobtrusives("enabled") & """"
            jsonData = jsonData & "},"
            unobtrusives.movenext
        loop
        
        
        if (Len(jsonData) > 1) then
             jsonData = left(jsonData, Len(jsonData) - 1)
        end if
        jsonData = jsonData & "]"
        Response.Write jsonData
        
    elseif request("action") = "switch" then
        id = request("id")
        Conn.Execute("UPDATE unobtrusives SET [enabled] = ([enabled] ^ 1) WHERE id = " & id)
    elseif request("action") = "update" then
        fromDate = Request("from")
        toDate = Request("to")
        days = Request("days") 
        id = Request("id")
          
        Conn.Execute("UPDATE unobtrusives SET [to] = '" & toDate & "', [from] = '" & fromDate & "', days = '" & days & "' WHERE id = " & id) 
    else
        Response.Status = "404 Not Found"
    end if
end if

if(request("name") = "isAdmin") then

    SQL = "SELECT type FROM policy WHERE id  IN (SELECT policy_id FROM policy_editor WHERE editor_id = " & request("user_id") & ")"
    
    Set isAdminSet = Conn.Execute(SQL)
    
    if IsSuperAccount(request("user_id")) then
        Response.Write "A"
    else
        response.Write isAdminSet("type")
    end if
    
end if
if(request("name") = "GetPolicy") then

    SQL = "SELECT " & request("value") & " as policy_val FROM policy_list INNER JOIN policy_editor ON policy_list.policy_id=policy_editor.policy_id WHERE editor_id=" & request("user")
    
    Set policySet = Conn.Execute(SQL)
    Response.Write policySet("policy_val")
end if

if(request("name") = "GetMsgTemplates") then

    SQL = "SELECT a.id, a.name FROM text_templates as a "
 	    Set templatesSet = Conn.Execute(SQL)
	    
    data = ""
    do while not templatesSet.EOF
        id = templatesSet("id")
        name = templatesSet("name")
        
        data = data & templatesSet("id") & vbCr & templatesSet("name") & vbLf
        templatesSet.MoveNext
        
    loop
    
    Response.Write data   

end if

if(request("name") = "DSUpdateTime") then

    id = request("id")
    slideTime = request("value")
    slideFactor = request("factor")
    
end if
    
if(request("name")="AddRow") then
    
    name = Request("value")
    
    name = Replace(name, "%23", "#")
    name = Replace(name, "%26", "&")
    name = Replace(name, "'", "''")
    
    start_date = Request("date")
    
    if(start_date <> "NULL") then
        start_date = "'" & start_date & "'"
    end if
    
    Set check = Conn.Execute("SELECT 1 FROM campaigns WHERE name = RTRIM(LTRIM('" & name & "'))")
    
    if not check.EOF then
     Response.Write LNG_CAN_NOT_CREATE
     Response.End
    end if
    
    SQL = "INSERT INTO campaigns OUTPUT INSERTED.ID AS 'newID' VALUES (N'" & name & "', N'" & CStr(Now()) &"', " & start_date &", NULL, NULL)"
    Set newIdSet =  Conn.Execute(SQL)
    
    newId = newIdSet("newID")
    
    Response.Write newId
    'Response.Redirect "CampaignDetails.aspx?campaignid="& newId 
 end if
 
 
 if(request("name") = "GetTemplateForGroup") then
 
    groupId = request("tgid")
    
    Set templatesSet = Conn.Execute("SELECT a.id, a.name FROM text_templates as a INNER JOIN group_message_templates as b ON b.template_id = a.id WHERE b.group_id = " & groupId )
    
    data = ""
    do while not templatesSet.EOF
        id = templatesSet("id")
        name = templatesSet("name")
        
        data = data & templatesSet("id") & vbCr & templatesSet("name") & vbLf
        templatesSet.MoveNext     
        
    loop
    
    Response.Write data
    
 end if
 
 
 
 if(request("name") = "saveTGroups") then 
    
    templates = Split(request("templates"), ",")
    groupName = request("tgname")
    groupName = Replace(groupName, "'", "''")
    groupName = Replace(groupName, "%23", "#")
    groupName = Replace(groupName, "%26", "&")
    
    updateGroupId = request("updateId")
    srcId = request("dupId")
    
    if(updateGroupId <> "") then
    
       ' Response.Write "UPDATE message_templates_groups SET name = N'" & groupName &"' WHERE id = " & updateGroupId
        Conn.Execute("UPDATE message_templates_groups SET name = N'" & groupName &"' WHERE id = " & updateGroupId)
        Conn.Execute("DELETE FROM group_message_templates WHERE group_id = " & updateGroupId)
        
        for each template in templates
            Conn.Execute("INSERT INTO group_message_templates (group_id, template_id) VALUES ("& updateGroupId &","&  template &")")
        next        
    
    elseif srcId <> "" then    
    
        Set newIdSet = Conn.Execute("INSERT INTO message_templates_groups OUTPUT INSERTED.ID AS 'newID' (SELECT name FROM message_templates_groups WHERE id = " & srcId & ")")
        groupId = newIdSet("newID")
    
        Response.Write "INSERT INTO group_message_templates (group_id, template_id) SELECT ( "& groupId & ", template_id ) FROM message_templates_groups WHERE group_id =  " & srcId 
        Conn.Execute("INSERT INTO group_message_templates (group_id, template_id) SELECT ( "& groupId & ", template_id ) FROM message_templates_groups WHERE group_id =  " & srcId)
                  
    else
        Response.Write "INSERT INTO message_templates_groups  OUTPUT INSERTED.ID AS 'newID'  VALUES (N'"& groupName & "')"
        Set newIdSet = Conn.Execute("INSERT INTO message_templates_groups  OUTPUT INSERTED.ID AS 'newID'  VALUES (N'"& groupName & "')")
        groupId = newIdSet("newID")
     
        for each template in templates
           
            Conn.Execute("INSERT INTO group_message_templates (group_id, template_id) VALUES ("& groupId &","&  template &")")
        next
    end if
 end if
 

 
 if(request("name") = "GetMyRejectedAlerts") then
 
        SQL= "SELECT a.id as id , a.campaign_id as campaign_id, a.reject_reason as reason,  a.title as title , a.class as type_id, a.type as type, a.urgent as urgent, a.fullscreen as fullscreen, a.ticker as ticker, s.id as is_rsvp  FROM alerts as a  LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id WHERE a.approve_status = 2 AND a.sender_id = " & request("user_id")
   
   	    if(Request("value") <> "" and Request("collumn") <> "") then
	    SQL = SQL & " ORDER BY a." & Request("collumn") & " " & Request("value")
	    else
	    SQL =  SQL & " ORDER BY a.create_date DESC"
	    end if
    'Response.Write SQL
    Set alertsSet = Conn.execute(SQL)
    
    resp=""
     do while not alertsSet.EOF 
     
               typeId = alertsSet("type_id")
              
                imgHtml = ""
				dim isUrgent
				if alertsSet("urgent") = "1" then
					isUrgent = true
				else
					isUrgent = false
				end if
				
				if typeId = "64"  or typeId = "128" or typeId = "256"  then
				
				    Set setSurveyId=Conn.Execute("SELECT id FROM surveys_main WHERE sender_id = " &alertsSet("id"))
				    
				    surveyId = setSurveyId("id")
				
				end if
				
				editPage = "edit_alert_db.asp?id=" & alertsSet("id")
				if typeId = "2" then
				    imgHtml = "<img src=""images/alert_types/fullscreen.png"" title=""Fullscreen"">"
				    editPage = "edit_screensavers.asp?id=" & alertsSet("id")
				    
				    Set htmlSet  = Conn.Execute("SELECT alert_text FROM alerts WHERE id = " & alertsSet("id") )
                    	alertHTML = htmlSet("alert_text")			    
                     screensaverType = ""
                    			
			                    Set rexp = New RegExp
			                    With rexp
				                    .Pattern = "<!--.*?screensaver_type=""(.*)"".*?file=""(.*)"".*?-->"
				                    .IgnoreCase = True
				                    .Global = False
			                    End With
                    			
			                    set Matches = rexp.Execute(alertHTML)

			                    For Each Match in Matches
				                    For Each Submatch in Match.SubMatches
						                    screensaverType = Match.SubMatches(0)
				                    Exit For
				                    Next
			                    Exit For
			                    Next
			          editPage = editPage & "&type=" & screensaverType
			                 
				elseif typeId = "64"   then
			        imgHtml = "<img src=""images/alert_types/survey.png"" title=""Survey"">"
			        editPage = "survey_add.asp?id=" & surveyId & "&approveAlertId=" & alertsSet("id")
			    elseif typeId = "128"  then
			         imgHtml = "<img src=""images/alert_types/quiz.png"" title=""Quiz"">"
			         editPage = "survey_add.asp?id=" & surveyId & "&approveAlertId=" & alertsSet("id")
			    elseif typeId = "256"  then
			        imgHtml = "<img src=""images/alert_types/poll.png"" title=""Poll"">"
			        editPage = "survey_add.asp?id=" & surveyId & "&approveAlertId=" & alertsSet("id")
			    elseif typeId = "8" then
			        imgHtml = "<img src=""images/alert_types/wallpaper.gif"" title=""Wallpaper"">"
			        editPage = "wallpaper_edit.asp?id=" & alertsSet("id")
			    elseif typeId = "4" then
			        imgHtml = "<img src=""images/alert_types/rss.gif"" title=""RSS"">"
			        editPage = "rss_edit.asp?id=" & alertsSet("id")
				elseif typeId = "16" then
					if isUrgent = true then
						imgHtml ="<img src=""images/alert_types/urgent_unobtrusive.gif"" title=""Urgent Unobtrusive"">"
					else
						imgHtml = "<img src=""images/alert_types/unobtrusive.gif"" title=""Unobtrusive"">"
					end if
					editPage = "edit_alert_db.asp?id=" & alertsSet("id")
				elseif typeId = "999" then
					imgHtml = "<img src=""images/alert_types/web_plugin.png"" title=""Web alert"">"
					editPage = "edit_alert_db.asp?id=" & alertsSet("id")
				elseif not isNull(alertsSet("is_rsvp")) then
					if isUrgent = true then
						imgHtml = "<img src=""images/alert_types/urgent_rsvp.png"" title=""Urgent RSVP"">"
					else
						imgHtml ="<img src=""images/alert_types/rsvp.png"" title=""RSVP"">"
					end if
					editPage = "edit_alert_db.asp?id=" & alertsSet("id")
				elseif alertsSet("ticker")="1" then
					if isUrgent = true then
						imgHtml = "<img src=""images/alert_types/urgent_ticker.png"" title=""Urgent Ticker"">"
					else
						imgHtml = "<img src=""images/alert_types/ticker.png"" title=""Ticker"">"
					end if
					editPage = "edit_alert_db.asp?id=" & alertsSet("id")
				elseif alertsSet("fullscreen")="1" then
					if isUrgent = true then
						imgHtml = "<img src=""images/alert_types/urgent_fullscreen.png"" title=""Urgent Fullscreen"">"
					else
						imgHtml = "<img src=""images/alert_types/fullscreen.png"" title=""Fullscreen"">"
					end if
					editPage = "edit_alert_db.asp?id=" & alertsSet("id")
				elseif isUrgent = true then
					imgHtml = "<img src=""images/alert_types/urgent_alert.gif"" title=""Urgent Alert"">"
					editPage = "edit_alert_db.asp?id=" & alertsSet("id")
				elseif alertsSet("type")="L" then
				        imgHtml = "<img src=""images/alert_types/linkedin.png"" title=""LinkedIn Alert"">"
				        editPage = "edit_alert_db.asp?id=" & alertsSet("id")
			    else
					imgHtml = "<img src=""images/alert_types/alert.png"" title=""Usual Alert"">"
					editPage = "edit_alert_db.asp?id=" & alertsSet("id")
				end if
			

				if alertsSet("campaign_id") <> "-1" or  alertsSet("campaign_id") <> " " then
				    editPage = editPage & "&alert_campaign=" & alertsSet("campaign_id")
				end if
				if typeId = "2048" then
				    editPage = editPage & "&webplugin=1"
				end if	         
	          resp = resp & alertsSet("id") & vbCr  & alertsSet("title") & vbCr  & alertsSet("reason")  & vbCr& imgHtml &vbCr  & editPage &vbLf 
	          alertsSet.MoveNext
	    loop
	    
	    Response.Write resp
 end if
 
 
	if(request("name") = "CheckDSLink") then
	
	    name = request("value")
	     name = Replace(name, "%23", "#")
        name = Replace(name, "%26", "&")
        name = Replace(name, "'", "''")
	    SQL = "SELECT 1 FROM digsign_links WHERE name = N'" & name &"'"
	    
	    Set checkSet = Conn.Execute(SQL)
	    
	    if checkSet.EOF then
	        Response.Write "1"
	    else
	        Response.Write "0"
	    end if
	end if
	
	 
 if(request("name") = "GetDigSignLinks") then
  
  
  SQL = "SELECT a.id, a.name, a.link, a.creation_date, a.slide_time_value, a.slide_time_factor, b.id as user_id FROM digsign_links as a INNER JOIN users as b ON a.name = b.name"
  

    if(Request("searchTerm") <> "") then
        SQL = SQL & " WHERE a.name LIKE '%" & Request("searchTerm") &"%'"
    end if
  if(Request("value") <> "" and Request("collumn") <> "") then
	    SQL = SQL & " ORDER BY " & Request("collumn") & " " & Request("value")
  else
	    SQL =  SQL & " ORDER BY creation_date DESC"
  end if
	      
   ' Response.Write SQL 
   ' Response.Write SQL
  Set linkSet = Conn.Execute(SQL)
  resp = ""
  do while not linkSet.EOF
    
    resp = resp & linkSet("id") & vbCr & linkSet("name") & vbCr & linkSet("link") & vbCr & linkSet("creation_date") & vbCr & linkSet("slide_time_value") & vbCr & linkSet("slide_time_factor") & vbCr &  linkSet("user_id") &vbLf 
    linkSet.MoveNext
  loop
 
    Response.Write resp
 end if

if(request("name") = "GetAlertSize") then

    id = request("id")
    
    SQL = "SELECT width, heigth FROM alerts WHERE id = " & id
    
    Set sizeSet = Conn.Execute(SQL)
    
    Response.Write sizeSet("width") & "x" & sizeSet("height")
    
end if

if(request("name") = "AddDSLink") then
    
    linkName = request("lname")
    linkName = Replace(linkName, "%23", "#")
    linkName = Replace(linkName, "%26", "&")
    linkName = Replace(linkName, "'", "''")
    linkName = Replace(linkName, """", """""")
        
    slideTimeValue = request("timeValue")
    slideTimeFactor = request("timeFactor")
    link = request("link")
    SQL = "INSERT INTO digsign_links (name, creation_date, slide_time_value, slide_time_factor, link) VALUES (N'"& linkName & "', GETDATE(), " & slideTimeValue & ", " & slideTimeFactor &", N'" & link & "')"
    Conn.Execute(SQL)
    
end if
 
 if(request("name") = "GetNonApprovedAlerts") then
 
    SQL= "SELECT a.id as id , a.campaign_id as campaign_id, a.title as title , a.create_date as create_date , u.name as sender, a.class as type_id, a.type as type, a.urgent as urgent, a.fullscreen as fullscreen, a.ticker as ticker, s.id as is_rsvp  FROM alerts as a INNER JOIN users as u  ON u.id = a.sender_id  LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id WHERE a.approve_status = 0 AND a.sender_id != " & request("userId")
   
   
   	    if(Request("value") <> "" and Request("collumn") <> "") then
	    SQL = SQL & " ORDER BY a." & Request("collumn") & " " & Request("value")
	    else
	    SQL =  SQL & " ORDER BY a.create_date DESC"
	    end if
   

    Set alertsSet = Conn.execute(SQL)
    
    resp=""
     do while not alertsSet.EOF 
     
               typeId = alertsSet("type_id")
              
                imgHtml = ""

				if alertsSet("urgent") = "1" then
					isUrgent = true
				else
					isUrgent = false
				end if
				
 			if typeId = "64"  or typeId = "128" or typeId = "256"  then
				
				    Set setSurveyId=Conn.Execute("SELECT id FROM surveys_main WHERE sender_id = " &alertsSet("id"))
				    
				    surveyId = setSurveyId("id")
				
				end if
				
				editPage = "edit_alert_db.asp?id=" & alertsSet("id")
				

			if typeId = "2" then
				    imgHtml = "<img src=""images/alert_types/fullscreen.png"" title=""Fullscreen"">"
				    editPage = "edit_screensavers.asp?id=" & alertsSet("id")
				    
				    Set htmlSet  = Conn.Execute("SELECT alert_text FROM alerts WHERE id = " & alertsSet("id") )
                    	alertHTML = htmlSet("alert_text")			    
                     screensaverType = ""
                    			
			                    Set rexp = New RegExp
			                    With rexp
				                    .Pattern = "<!--.*?screensaver_type=""(.*)"".*?file=""(.*)"".*?-->"
				                    .IgnoreCase = True
				                    .Global = False
			                    End With
                    			
			                    set Matches = rexp.Execute(alertHTML)

			                    For Each Match in Matches
				                    For Each Submatch in Match.SubMatches
						                    screensaverType = Match.SubMatches(0)
				                    Exit For
				                    Next
			                    Exit For
			                    Next
			          editPage = editPage & "&type=" & screensaverType
			          
				elseif typeId = "64"   then
			        imgHtml = "<img src=""images/alert_types/survey.png"" title=""Survey"">"
			        editPage = "survey_add.asp?id=" & surveyId & "&approveAlertId=" & alertsSet("id")
			    elseif typeId = "128"  then
			         imgHtml = "<img src=""images/alert_types/quiz.png"" title=""Quiz"">"
			         editPage = "survey_add.asp?id=" & surveyId & "&approveAlertId=" & alertsSet("id")
			    elseif typeId = "256"  then
			        imgHtml = "<img src=""images/alert_types/poll.png"" title=""Poll"">"
			        editPage = "survey_add.asp?id=" & surveyId & "&approveAlertId=" & alertsSet("id")
			    elseif typeId = "8" then
			        imgHtml = "<img src=""images/alert_types/wallpaper.gif"" title=""Wallpaper"">"
			        editPage = "wallpaper_edit.asp?id=" & alertsSet("id")
			    elseif typeId = "4" then
			        imgHtml = "<img src=""images/alert_types/rss.gif"" title=""RSS"">"
			        editPage = "rss_edit.asp?id=" & alertsSet("id")
				elseif typeId = "16" then
					if isUrgent = true then
						imgHtml ="<img src=""images/alert_types/urgent_unobtrusive.gif"" title=""Urgent Unobtrusive"">"
					else
						imgHtml = "<img src=""images/alert_types/unobtrusive.gif"" title=""Unobtrusive"">"
					end if
					editPage = "edit_alert_db.asp?id=" & alertsSet("id")
				elseif typeId = "999" then
					imgHtml = "<img src=""images/alert_types/web_plugin.png"" title=""Web alert"">"
					editPage = "edit_alert_db.asp?id=" & alertsSet("id")
				elseif not isNull(alertsSet("is_rsvp")) then
					if isUrgent = true then
						imgHtml = "<img src=""images/alert_types/urgent_rsvp.png"" title=""Urgent RSVP"">"
					else
						imgHtml ="<img src=""images/alert_types/rsvp.png"" title=""RSVP"">"
					end if
					editPage = "edit_alert_db.asp?id=" & alertsSet("id")
				elseif alertsSet("ticker")="1" then
					if isUrgent = true then
						imgHtml = "<img src=""images/alert_types/urgent_ticker.png"" title=""Urgent Ticker"">"
					else
						imgHtml = "<img src=""images/alert_types/ticker.png"" title=""Ticker"">"
					end if
					editPage = "edit_alert_db.asp?id=" & alertsSet("id")
				elseif alertsSet("fullscreen")="1" then
					if isUrgent = true then
						imgHtml = "<img src=""images/alert_types/urgent_fullscreen.png"" title=""Urgent Fullscreen"">"
					else
						imgHtml = "<img src=""images/alert_types/fullscreen.png"" title=""Fullscreen"">"
					end if
					editPage = "edit_alert_db.asp?id=" & alertsSet("id")
				elseif isUrgent = true then
					imgHtml = "<img src=""images/alert_types/urgent_alert.gif"" title=""Urgent Alert"">"
					editPage = "edit_alert_db.asp?id=" & alertsSet("id")
				elseif alertsSet("type")="L" then
				        imgHtml = "<img src=""images/alert_types/linkedin.png"" title=""LinkedIn Alert"">"
				        editPage = "edit_alert_db.asp?id=" & alertsSet("id")
			    else
					imgHtml = "<img src=""images/alert_types/alert.png"" title=""Usual Alert"">"
					editPage = "edit_alert_db.asp?id=" & alertsSet("id")
				end if
				
				if alertsSet("campaign_id") <> "-1" or  alertsSet("campaign_id") <> " " then
				    editPage = editPage & "&alert_campaign=" & alertsSet("campaign_id")
				end if
				
				if typeId = "2048" then
				    editPage = editPage & "&webplugin=1"
				end if
	         
	          resp = resp & alertsSet("id") & vbCr  & alertsSet("title") & vbCr  & alertsSet("create_date") &  vbCr  & alertsSet("sender") & vbCr& imgHtml & vbCr & editPage &vbLf 
	          alertsSet.MoveNext
	    loop
	    
	    Response.Write resp
    
 end if
 
 if(request("name") = "ApproveCheck") then
    
    campaignId = request("campaignId")
    
    set alertsCountSet = Conn.Execute("SELECT COUNT(1) as count FROM alerts WHERE campaign_id = " & campaignId & " AND approve_status = 0")
    
    unaprovedAlertsCount = Clng(alertsCountSet("count"))
    
    if unaprovedAlertsCount > 0 then
        Response.Write "fail"
    else
       Response.Write "ok"
    end if 
    
 end if
 
 if(request("name") = "ReCampaign") then
    id = Request("id")
    dt = Request("date")
    
    if(dt <> "NULL") then
        dt = "'" & dt & "'"
    end if

    Conn.Execute("UPDATE campaigns SET start_date = " & dt & " WHERE id = " & id)
 end if
 
  if(request("name") = "PlaneCampaign") then
    id = Request("id")
    dt = Request("date")
    
    if(dt <> "NULL") then
        dt = "'" & dt & "'"
    end if

    Conn.Execute("UPDATE campaigns SET planned_date = " & dt & " WHERE id = " & id)
 end if
 
 if(request("name")="DupCampaign") then
 
    id = Request("id")
    name = Request("cname")
    
    
    Set campaignAlerts = Conn.Execute("SELECT schedule, recurrence, from_date, to_date FROM alerts WHERE campaign_id = " & id)
    
    
    do while not campaignAlerts.EOF
    
    reschedule = 0
    if campaignAlerts("schedule")="1" or campaignAlerts("recurrence")="1" then
    	start_date = campaignAlerts("from_date")
			end_date = campaignAlerts("to_date")
			if start_date <> "" then
				if end_date <> "" then
					if year(end_date) <> 1900 then
						if (MyMod(DateDiff("s", start_date, end_date),60)) <> 1 then 'if not lifetime
							reschedule = 1

						end if
					else
						reschedule = 1
					end if
				else
					reschedule = 1
				end if
	        end if  
	end if  
			if reschedule = 1 then
                Response.Write "schedule"
			    Exit Do
	        end if 
        campaignAlerts.MoveNext
    loop
    
     SQL = "INSERT INTO campaigns OUTPUT INSERTED.ID AS 'newID' VALUES (N'" & name & "', N'" & CStr(Now()) &"', NULL, NULL, NULL)"
    Set rsNewId =  Conn.Execute(SQL)
    'Set rsNewId = Conn.Execute("select @@IDENTITY newID from campaigns")
    newId = rsNewId("newID")
    
    'clone alerts
    Set dates = Conn.Execute("SELECT id, sent_date, from_date, to_date FROM alerts WHERE campaign_id = " & id)
	Response.Write "ID = " & id
    do while not dates.EOF
        
		
        alert_id = dates("id") 
        from_date = dates("from_date")
        to_date = dates("to_date")
        create_date = dates("sent_date")

		cur_now =  DateAdd("s", -Second(Now()), Now)
		
		if year(from_date) <> "1900" and not IsNull(create_date) then
			diff_from = CLng(DateDiff("s", create_date, from_date))
			new_from = DateAdd("s",CLng(diff_from),  cur_now)
		else
			new_from = from_date
		end if
		
		
        diff_to = CLng(DateDiff("s", from_date, to_date))
        new_to = DateAdd("s", CLng(diff_to), new_from)  
        
       SQL = "INSERT INTO alerts ([alert_text] ,[title],[type],[group_type] ,[create_date],[sent_date],[type2],[from_date],[to_date] ,[schedule],[schedule_type],[recurrence],[urgent],[toolbarmode],[deskalertsmode],[sender_id] ,[template_id] ,[group_id] ,[aknown] ,[ticker] ,[fullscreen] ,[alert_width] ,[alert_height] ,[email_sender] ,[email] ,[sms] ,[desktop] ,[escalate] ,[escalate_hours] ,[escalate_count] ,[escalate_step] ,[escalate_hours_initial] ,[autoclose] ,[param_temp_id] ,[caption_id] ,[class] ,[parent_id] ,[resizable] ,[post_to_blog] ,[social_media] ,[self_deletable] ,[text_to_call] ,[video] ,[color_code] ,[campaign_id]) ( SELECT [alert_text] ,[title],[type],[group_type] , [create_date],'"& Cstr(Now()) & "',[type2],'"& Cstr(new_from) &"','"& Cstr(new_to) & "',[schedule],'0',[recurrence],[urgent],[toolbarmode],[deskalertsmode],[sender_id] ,[template_id] ,[group_id] ,[aknown] ,[ticker] ,[fullscreen] ,[alert_width] ,[alert_height] ,[email_sender] ,[email] ,[sms] ,[desktop] ,[escalate] ,[escalate_hours] ,[escalate_count] ,[escalate_step] ,[escalate_hours_initial] ,[autoclose] ,[param_temp_id] ,[caption_id] ,[class] ,[parent_id] ,[resizable] ,[post_to_blog] ,[social_media] ,[self_deletable] ,[text_to_call] ,[video] ,[color_code] , "& newId &" FROM alerts WHERE campaign_id = " & id & " AND id = " &  alert_id &" )"
      Conn.Execute(SQL)
     
      dates.movenext   
    loop
    
     
    'Close users, groups, comps, ipranges
    
    Set newIds = Conn.execute("SELECT id FROM alerts WHERE campaign_id = "& newId)
    Set oldIds = Conn.execute("SELECT id FROM alerts WHERE campaign_id = "& id)
   

    do while not newIds.EOF and not oldIds.EOF
    
    
     new_id = newIds("id")
     old_id = oldIds("id")
     
     Conn.Execute("INSERT INTO alerts_sent ([alert_id], [user_id]) ( SELECT " & new_id &", user_id FROM alerts_sent WHERE alert_id = " & old_id & ")")
     Conn.Execute("INSERT INTO alerts_sent_group ([alert_id], [group_id]) ( SELECT " & new_id &", group_id FROM alerts_sent_group WHERE alert_id = " & old_id & ")")
     Conn.Execute("INSERT INTO alerts_sent_comp ([alert_id], [comp_id]) ( SELECT " & new_id &", comp_id FROM alerts_sent_comp WHERE alert_id = " & old_id & ")")
     Conn.Execute("INSERT INTO alerts_sent_iprange ([alert_id], [ip_from], [ip_to]) ( SELECT " & new_id &", [ip_from], [ip_to] FROM alerts_sent_iprange WHERE alert_id = " & old_id & ")")    
   
     newIds.movenext
     oldIds.movenext
    loop
    
    
   
    'close surveys
    Set surveys = Conn.execute("SELECT id FROM alerts WHERE  campaign_id = "& newId)
    Set old_surveys = Conn.execute("SELECT id FROM alerts WHERE  campaign_id = "& id)
    do while not surveys.EOF and not old_surveys.EOF
      
      SQL = "INSERT INTO surveys_main  ([name], [question], [type], [group_type], [create_date], [closed], [sender_id], [template_id], [from_date], [to_date], [schedule], [stype]) (SELECT [name], [question], [type], [group_type], [create_date], [closed], " & surveys("id") &", [template_id], [from_date], [to_date], [schedule], [stype] FROM surveys_main sm WHERE sm.sender_id = "&  old_surveys("id") & ")"
      Conn.Execute(SQL)  
      
      Set oldSurveyIdSet = Conn.Execute("SELECT id FROM surveys_main WHERE sender_id = " & old_surveys("id"))
      Set newSurveyIdSet = Conn.Execute("SELECT id FROM surveys_main WHERE sender_id = " & surveys("id"))
      
     do while not newSurveyIdSet.EOF and not oldSurveyIdSet.EOF 
       
      newSurveyId = newSurveyIdSet("id")
      oldSurveyId = oldSurveyIdSet("id")
      Conn.Execute("INSERT INTO surveys_questions (question, survey_id, question_number, question_type) SELECT  question, " & newSurveyId &" , question_number, question_type FROM surveys_questions WHERE survey_id = " &oldSurveyId)
      
      Set oldSurveyQuestionIdSet = Conn.Execute("SELECT id FROM surveys_questions WHERE survey_id = " & oldSurveyId)
      Set newSurveyQuestionIdSet = Conn.Execute("SELECT id FROM surveys_questions WHERE survey_id = " & newSurveyId)
      
       do while not newSurveyQuestionIdSet.EOF and not oldSurveyQuestionIdSet.EOF 
        oldQuestionId = oldSurveyQuestionIdSet("id")
        newQuestionId = newSurveyQuestionIdSet("id")
        
        Conn.Execute("INSERT INTO surveys_answers (answer, survey_id, votes, question_id, correct) SELECT answer, survey_id, votes, " & newQuestionId &", correct FROM surveys_answers WHERE question_id = " & oldQuestionId)
        newSurveyQuestionIdSet.MoveNext
        oldSurveyQuestionIdSet.MoveNext
       loop
      newSurveyIdSet.MoveNext
      oldSurveyIdSet.MoveNext
     loop
     
      surveys.MoveNext
      old_surveys.MoveNext
    loop 
    
 end if
 
	if(request("name")="ConfShowTypesDialog") then
	
		SQL = "UPDATE settings SET val=N'" & request("value") & "' WHERE name='" & request("name") & "' AND s_part = 'S'"
		Conn.Execute(SQL)

	end if 
	
	if(request("name")="ConfShowSkinsDialog") then
	
		SQL = "UPDATE settings SET val=N'" & request("value") & "' WHERE name='" & request("name") & "' AND s_part = 'S'"
		Conn.Execute(SQL)

	end if

    if(request("name")="ConfAllowClientDeleteTerminatedAlerts") then
        
        SQL = "UPDATE settings SET val=N'" & request("value") & "' WHERE name='" & request("name") & "' AND s_part = 'S'"
		Conn.Execute(SQL)

    end if
	
	if(request("name")="ConfAPISecret") then
	
		SQL = "SELECT val FROM settings WHERE name='ConfAPISecret'"
		Set result = Conn.Execute(SQL)
		val = ""
		if not result.EOF then
		    val = result("val")
		end if
        
        Response.Write (val)

	end if
	
	
	if(request("name")="UserGuideMode") then
		SQL = "UPDATE users SET guide=N'" & request("value") & "' WHERE id=" & uid
		Conn.Execute(SQL)
	end if
	
	if(request("name")="DisabledGuides") then
		SQL = "UPDATE users SET dis_guide=N'" & request("value") & "' WHERE id=" & uid
		Conn.Execute(SQL)
	end if
	
	

	if(request("name")="AddUser") then
		
		
		Set exists = Conn.Execute("SELECT count(1) as cnt FROM users WHERE name = '"& request("value") &"'")
		
		if exists("cnt") = "0"  then
		    SQL = "INSERT INTO users(name,pass,deskbar_id,role,group_id,reg_date,client_version,domain_id) VALUES(N'"&request("value")&"',N'"&md5("hyihyihyi")&"',N'"&request("deskbar_id")&"',N'U',0,N'"&Now&"',N'"&request("version")&"',1)"
		    Conn.Execute(SQL)
		end if
		
	end if
	
	if(request("name")="CheckUser") then
		SQL = "SELECT count(*) as count FROM users WHERE name = N'"&request("value")&"'"
		Set check_name = Conn.Execute(SQL)
		Dim count
		count = 0
		if not check_name.EOF then
		    count = check_name("count")
		end if
		
		if(count>0) then 
		    Response.Write ("is_exist")
		else 
		    Response.Write ("is_free")
		end if 
		
	end if
	
	if(request("name") = "GetCampaigns") then


	    SQL = "SELECT  a.id, a.name, a.create_date, (SELECT DISTINCT 1 FROM alerts as b  WHERE b.campaign_id = a.id AND b.schedule_type != '0') as is_started, a.start_date FROM campaigns as a"
	    resp=""
	    
	    if(Request("value") <> "" and Request("collumn") <> "") then
	    SQL = SQL & " ORDER BY a." & Request("collumn") & " " & Request("value")
	    else
	    SQL =  SQL & " ORDER BY a.create_date DESC"
	    end if
	    
	    Set data = Conn.Execute(SQL)

	    do while not data.EOF 
	            
	          if( IsNull (data("is_started"))) then
	            is_started = 0
	          else is_started = 1
	         end if
	         
	          resp = resp & data("id") & vbCr  & data("name") & vbCr  & data("create_date") &  vbCr  & is_started & vbCr& data("start_date") &vbLf 
	          data.MoveNext
	    loop
	
	    Response.Write resp
	    
	  end if


    if(request("name") = "GetWebplugins") then


	    SQL = "SELECT  a.id, a.name, a.date FROM banners as a"
	    resp=""
	    
	    if(Request("value") <> "" and Request("collumn") <> "") then
	    SQL = SQL & " ORDER BY a." & Request("collumn") & " " & Request("value")
	    else
	    SQL =  SQL & " ORDER BY a.date DESC"
	    end if
	    
	    Set data = Conn.Execute(SQL)

	    do while not data.EOF 
	            
	         
	         
	          resp = resp & data("id") & vbCr  & data("name") & vbCr  & data("date") &vbLf 
	          data.MoveNext
	    loop
	
	    Response.Write resp
	    
	end if

    if(request("name")="AddBannerUpdate") then
		
		    SQL = "UPDATE [banners] SET [name]='"&request("banner")&"',[date]=GETDATE(),[width]='"&request("width")&"',[width_unit]='"&request("width_unit")&"'"&_
                    " ,[max_width]='"&request("max_width")&"',[max_width_unit]='"&request("max_width_unit")&"',[min_width]='"&request("min_width")&"'"&_
                    " ,[min_width_unit]='"&request("min_width_unit")&"',[height]='"&request("height")&"',[height_unit]='"&request("height_unit")&"',[max_height]='"&request("max_height")&"'"&_
                    " ,[max_height_unit]='"&request("max_height_unit")&"',[min_height]='"&request("min_height")&"',[min_height_unit]='"&request("min_height_unit")&"'"&_
                    " ,[background_color]='"&request("background_color")&"',[transparent]='"&request("transparent")&"',[font_color]='"&request("font_color")&"'"&_
                    " ,[font_size]='"&request("font_size")&"',[font_size_unit]='"&request("font_size_unit")&"',[font_weight]='"&request("font_weight")&"',[font_weight_unit]='"&request("font_weight_unit")&"'"&_
                    " ,[text_align]='"&request("text_align")&"',[direction]='"&request("direction")&"',[unicode_bidi]='"&request("unicode_bidi")&"',[border_weight]='"&request("border_weight")&"'"&_
                    " ,[border_weight_unit]='"&request("border_weight_unit")&"',[border_style]='"&request("border_style")&"',[border_color]='"&request("border_color")&"'"&_
                    " ,[padding_top]='"&request("padding_top")&"',[padding_top_unit]='"&request("padding_top_unit")&"',[padding_right]='"&request("padding_right")&"'"&_
                    " ,[padding_right_unit]='"&request("padding_right_unit")&"',[padding_bottom]='"&request("padding_bottom")&"',[padding_bottom_unit]='"&request("padding_bottom_unit")&"'"&_
                    " ,[padding_left]='"&request("padding_left")&"',[padding_left_unit]='"&request("padding_left_unit")&"',[margin_top]='"&request("margin_top")&"'"&_
                    " ,[margin_top_unit]='"&request("margin_top_unit")&"',[margin_right]='"&request("margin_right")&"',[margin_right_unit]='"&request("margin_right_unit")&"'"&_
                    " ,[margin_bottom]='"&request("margin_bottom")&"',[margin_bottom_unit]='"&request("margin_bottom_unit")&"',[margin_left]='"&request("margin_left")&"'"&_
                    " ,[margin_left_unit]='"&request("margin_left_unit")&"',[display]='"&request("display")&"',[position]='"&request("position")&"',[top]='"&request("top")&"'"&_
                    " ,[top_unit]='"&request("top_unit")&"',[right]='"&request("right")&"',[right_unit]='"&request("right_unit")&"',[bottom]='"&request("bottom")&"'"&_
                    " ,[bottom_unit]='"&request("bottom_unit")&"',[left]='"&request("left")&"',[left_unit]='"&request("left_unit")&"' WHERE id = '"&request("id")&"' "
		    Conn.Execute(SQL)

            Set fso = CreateObject("Scripting.FileSystemObject")
            path = Server.MapPath("logs/"&request("banner")&".js")
            Set tf = fso.OpenTextFile(path, 2, True)
                tf.WriteLine( "    function getXObject(){" )
                tf.WriteLine( "        var xmlhttp;" )
                tf.WriteLine( "        try {" )
                tf.WriteLine( "	        xmlhttp = new ActiveXObject('Msxml2.XMLHTTP')" )
                tf.WriteLine( "        } catch (e) {" )
                tf.WriteLine( "	        try {" )
                tf.WriteLine( "		        xmlhttp = new ActiveXObject('Microsoft.XMLHTTP')" )
                tf.WriteLine( "	        } catch (E) {" )
                tf.WriteLine( "		        xmlhttp = false;" )
                tf.WriteLine( "	        }" )
                tf.WriteLine( "        }" )
                tf.WriteLine( "        if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {" )
                tf.WriteLine( "	        xmlhttp = new XMLHttpRequest();" )
                tf.WriteLine( "        }" )
                tf.WriteLine( "		" )
                tf.WriteLine( "        return  xmlhttp;" )
                tf.WriteLine( "    };" )
                tf.WriteLine( "	" )
                tf.WriteLine( "    function setProperty(obj){" )
                tf.WriteLine( "        obj.style.width = '" & request("width") & "';" )
                tf.WriteLine( "        obj.style.height = '" & request("height") & "';" )
                    tf.WriteLine( "        obj.style.maxWidth = '" & request("max_width") & "';" )
                    tf.WriteLine( "        obj.style.minWidth = '" & request("min_width") & "';" )

                    tf.WriteLine( "        obj.style.maxHeight = '" & request("max_height") & "';" )
                    tf.WriteLine( "        obj.style.minHeight = '" & request("min_height") & "';" )
                
                tf.WriteLine( "        obj.style.color = '" & request("font_color") & "';" )
                tf.WriteLine( "        obj.style.fontSize = '" & request("font_size") & "';" )
                tf.WriteLine( "        obj.style.fontFamily = 'inherit';" )
                tf.WriteLine( "        obj.style.fontWeight = '" & request("font_weight") & "';" )
                tf.WriteLine( "        obj.style.backgroundColor = '" & request("background_color") & "';" )
                tf.WriteLine( "        obj.style.borderColor = '" & request("border_color") & "';" )
                tf.WriteLine( "        obj.style.borderWidth = '" & request("border_weight") & "';" )
                tf.WriteLine( "        obj.style.borderStyle = '" & request("border_style") & "';" )
                tf.WriteLine( "        obj.style.paddingTop = '" & request("padding_top") & "';" )
                tf.WriteLine( "        obj.style.paddingRight = '" & request("padding_right") & "';" )
                tf.WriteLine( "        obj.style.paddingBottom = '" & request("padding_bottom") & "';" )
                tf.WriteLine( "        obj.style.paddingLeft = '" & request("padding_left") & "';" )
                tf.WriteLine( "        obj.style.marginTop = '" & request("margin_top") & "';" )
                tf.WriteLine( "        obj.style.marginRight = '" & request("margin_right") & "';" )
                tf.WriteLine( "        obj.style.marginBottom = '" & request("margin_bottom") & "';" )
                tf.WriteLine( "        obj.style.marginLeft = '" & request("margin_left") & "';" )
                tf.WriteLine( "        obj.style.display = '" & request("display") & "';" )
                tf.WriteLine( "        obj.style.position = '" & request("position") & "';" )
                tf.WriteLine( "        obj.style.textAlign = '" & request("text_align") & "';" )
                tf.WriteLine( "        obj.style.direction = '" & request("direction") & "';" )
                tf.WriteLine( "        obj.style.unicodeBidi = '" & request("unicode_bidi") & "';" )
                tf.WriteLine( "        obj.style.top = '" & request("top") & "';" )
                tf.WriteLine( "        obj.style.right = '" & request("right") & "';" )
                tf.WriteLine( "        obj.style.bottom = '" & request("bottom") & "';" )
                tf.WriteLine( "        obj.style.left = '" & request("left") & "';" )
                tf.WriteLine( "        obj.style.overflow = 'auto';" )
                tf.WriteLine( "    };" )
                tf.WriteLine( "    var server = '" & alerts_folder & "get_web_xml.asp';" )
                tf.WriteLine( "    var uname = '" & request("banner") & "';" )
                tf.WriteLine( "    var cnt = '1';" )
                tf.WriteLine( "    var deskbar_id = '" & request("deskbar_id") & "';" )
                tf.WriteLine( "    var version = '1.0.0.0';" )
                tf.WriteLine( "    var xml_alerts = getXObject();" )
                tf.WriteLine( "    var url = 'deskalerts_webplugin.php';" )
                tf.WriteLine( "    var params = 'uname='&uname&'&cnt='&cnt&'&deskbar_id='&deskbar_id&'&version='&version&'&getAlert=xml&host='&server&'';" )
                tf.WriteLine( "    xml_alerts.open('POST', url, true);" )
                tf.WriteLine( "    xml_alerts.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');" )
                tf.WriteLine( "" )
                tf.WriteLine( "    xml_alerts.onreadystatechange = function() {" )
                tf.WriteLine( "        if (xml_alerts.readyState == 4) {" )
                tf.WriteLine( "            if (xml_alerts.status == 200) {" )
                tf.WriteLine( "                var str = xml_alerts.responseText;" )
                tf.WriteLine( "                var count = 0;" )
                tf.WriteLine( "                var elem_result = document.getElementById('web_plugin');" )
                tf.WriteLine( "" )
                tf.WriteLine( "                var parser = new DOMParser();" )
                tf.WriteLine( "                var xmlDoc = parser.parseFromString(xml_alerts.responseText, 'application/xml');" )
                tf.WriteLine( "                var xmlAlert = xmlDoc.getElementsByTagName('ALERT');" )
                tf.WriteLine( "" )
                tf.WriteLine( "                for (var i = 0; i < xmlAlert.length; i&&) {" )
                tf.WriteLine( "                    var get_alert = getXObject();" )
                tf.WriteLine( "                    " )
                tf.WriteLine( "			        var url_getAlert = xmlAlert[i].getAttribute('href');" )
                tf.WriteLine( "			        var params2 = 'getAlert=alert&href='&url_getAlert&'';" )
                tf.WriteLine( "			        get_alert.open('POST', url , true);" )
                tf.WriteLine( "                    get_alert.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');" )
                tf.WriteLine( "" )
                tf.WriteLine( "                    get_alert.onreadystatechange = function() {" )
                tf.WriteLine( "                        if (get_alert.readyState == 4) {" )
                tf.WriteLine( "                            if (get_alert.status == 200) {" )
                tf.WriteLine( "                                var textAlert = get_alert.responseText;  " )
                tf.WriteLine( "								" )
                tf.WriteLine( "						        var myRe = /(?:html_title=\"").&(?:\"")/g;" )
                tf.WriteLine( "						        var html_title;" )
                tf.WriteLine( "						        while ((myArray = myRe.exec(textAlert)) != null) {" )
                tf.WriteLine( "							        html_title = myArray[0];" )
                tf.WriteLine( "						        }" )
                tf.WriteLine( "						        myRe = /html_title=\""(.&)\""/;" )
                tf.WriteLine( "						        html_title = html_title.replace(myRe,'$1');" )
                tf.WriteLine( "						        var item = document.getElementById('web_plugin');	" )
                tf.WriteLine( "								" )
                tf.WriteLine( "						        item.innerHTML = '<span>'&textAlert&'</span>';" )
                tf.WriteLine( "								" )
                tf.WriteLine( "						        setProperty(item);" )
                tf.WriteLine( "                            }" )
                tf.WriteLine( "                        }" )
                tf.WriteLine( "                    };" )
                tf.WriteLine( "" )
                tf.WriteLine( "			        get_alert.send(params2);" )
                tf.WriteLine( "                } " )
                tf.WriteLine( "            }" )
                tf.WriteLine( "        }" )
                tf.WriteLine( "    };" )
                tf.WriteLine( "    xml_alerts.send(params);" )


            tf.Close

            Response.Write alerts_folder & "admin/logs/"&request("banner")&".js"	
    end if
    

    if(request("name")="AddBannerInsert") then
		
		    SQL = "INSERT INTO [banners]([name],[date],[width],[width_unit],[max_width],[max_width_unit],[min_width],[min_width_unit],[height],[height_unit],[max_height] "&_
                    " ,[max_height_unit],[min_height],[min_height_unit],[background_color],[transparent],[font_color],[font_size],[font_size_unit]"&_
                    " ,[font_weight],[font_weight_unit],[text_align],[direction],[unicode_bidi],[border_weight],[border_weight_unit],[border_style],[border_color],[padding_top]"&_
                    " ,[padding_top_unit],[padding_right],[padding_right_unit],[padding_bottom],[padding_bottom_unit],[padding_left],[padding_left_unit],[margin_top]"&_
                    " ,[margin_top_unit],[margin_right],[margin_right_unit],[margin_bottom],[margin_bottom_unit],[margin_left],[margin_left_unit],[display],[position],[top]"&_
                    " ,[top_unit],[right],[right_unit],[bottom],[bottom_unit],[left],[left_unit]) "&_
                    " VALUES(N'"&request("banner")&"',GETDATE(),N'"&request("width")&"',N'"&request("width_unit")&"',N'"&request("max_width")&"',N'"&request("max_width_unit")&"' "&_
                    " ,N'"&request("min_width")&"',N'"&request("min_width_unit")&"' "&_
                    " ,N'"&request("height")&"',N'"&request("height_unit")&"',N'"&request("max_height")&"',N'"&request("max_height_unit")&"'"&_
                    " ,N'"&request("min_height")&"',N'"&request("min_height_unit")&"' "&_
                    " ,N'"&request("background_color")&"',N'"&request("transparent")&"',N'"&request("font_color")&"',N'"&request("font_size")&"',N'"&request("font_size_unit")&"'"&_
                    " ,N'"&request("font_weight")&"',N'"&request("font_weight_unit")&"',N'"&request("text_align")&"',N'"&request("direction")&"',N'"&request("unicode_bidi")&"'"&_
                    " ,N'"&request("border_weight")&"',N'"&request("border_weight_unit")&"',N'"&request("border_style")&"',N'"&request("border_color")&"',N'"&request("padding_top")&"'"&_
                    " ,N'"&request("padding_top_unit")&"',N'"&request("padding_right")&"',N'"&request("padding_right_unit")&"',N'"&request("padding_bottom")&"',N'"&request("padding_bottom_unit")&"'"&_
                    " ,N'"&request("padding_left")&"',N'"&request("padding_left_unit")&"',N'"&request("margin_top")&"',N'"&request("margin_top_unit")&"',N'"&request("margin_right")&"'"&_
                    " ,N'"&request("margin_right_unit")&"',N'"&request("margin_bottom")&"',N'"&request("margin_bottom_unit")&"',N'"&request("margin_left")&"',N'"&request("margin_left_unit")&"'"&_
                    " ,N'"&request("display")&"',N'"&request("position")&"',N'"&request("top")&"',N'"&request("top_unit")&"',N'"&request("right")&"'"&_
                    " ,N'"&request("right_unit")&"',N'"&request("bottom")&"',N'"&request("bottom_unit")&"',N'"&request("left")&"',N'"&request("left_unit")&"')"
		    Conn.Execute(SQL)

            Set fso = CreateObject("Scripting.FileSystemObject")
            path = Server.MapPath("logs/"&request("banner")&".js")
            Set tf = fso.OpenTextFile(path, 2, True)
                tf.WriteLine( "    function getXObject(){" )
                tf.WriteLine( "        var xmlhttp;" )
                tf.WriteLine( "        try {" )
                tf.WriteLine( "	        xmlhttp = new ActiveXObject('Msxml2.XMLHTTP')" )
                tf.WriteLine( "        } catch (e) {" )
                tf.WriteLine( "	        try {" )
                tf.WriteLine( "		        xmlhttp = new ActiveXObject('Microsoft.XMLHTTP')" )
                tf.WriteLine( "	        } catch (E) {" )
                tf.WriteLine( "		        xmlhttp = false;" )
                tf.WriteLine( "	        }" )
                tf.WriteLine( "        }" )
                tf.WriteLine( "        if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {" )
                tf.WriteLine( "	        xmlhttp = new XMLHttpRequest();" )
                tf.WriteLine( "        }" )
                tf.WriteLine( "		" )
                tf.WriteLine( "        return  xmlhttp;" )
                tf.WriteLine( "    };" )
                tf.WriteLine( "	" )
                tf.WriteLine( "    function setProperty(obj){" )
                tf.WriteLine( "        obj.style.width = '" & request("width") & "';" )
                tf.WriteLine( "        obj.style.height = '" & request("height") & "';" )
                    tf.WriteLine( "        obj.style.maxWidth = '" & request("max_width") & "';" )
                    tf.WriteLine( "        obj.style.minWidth = '" & request("min_width") & "';" )

                    tf.WriteLine( "        obj.style.maxHeight = '" & request("max_height") & "';" )
                    tf.WriteLine( "        obj.style.minHeight = '" & request("min_height") & "';" )
                
                tf.WriteLine( "        obj.style.color = '" & request("font_color") & "';" )
                tf.WriteLine( "        obj.style.fontSize = '" & request("font_size") & "';" )
                tf.WriteLine( "        obj.style.fontFamily = 'inherit';" )
                tf.WriteLine( "        obj.style.fontWeight = '" & request("font_weight") & "';" )
                tf.WriteLine( "        obj.style.backgroundColor = '" & request("background_color") & "';" )
                tf.WriteLine( "        obj.style.borderColor = '" & request("border_color") & "';" )
                tf.WriteLine( "        obj.style.borderWidth = '" & request("border_weight") & "';" )
                tf.WriteLine( "        obj.style.borderStyle = '" & request("border_style") & "';" )
                tf.WriteLine( "        obj.style.paddingTop = '" & request("padding_top") & "';" )
                tf.WriteLine( "        obj.style.paddingRight = '" & request("padding_right") & "';" )
                tf.WriteLine( "        obj.style.paddingBottom = '" & request("padding_bottom") & "';" )
                tf.WriteLine( "        obj.style.paddingLeft = '" & request("padding_left") & "';" )
                tf.WriteLine( "        obj.style.marginTop = '" & request("margin_top") & "';" )
                tf.WriteLine( "        obj.style.marginRight = '" & request("margin_right") & "';" )
                tf.WriteLine( "        obj.style.marginBottom = '" & request("margin_bottom") & "';" )
                tf.WriteLine( "        obj.style.marginLeft = '" & request("margin_left") & "';" )
                tf.WriteLine( "        obj.style.display = '" & request("display") & "';" )
                tf.WriteLine( "        obj.style.position = '" & request("position") & "';" )
                tf.WriteLine( "        obj.style.textAlign = '" & request("text_align") & "';" )
                tf.WriteLine( "        obj.style.direction = '" & request("direction") & "';" )
                tf.WriteLine( "        obj.style.unicodeBidi = '" & request("unicode_bidi") & "';" )
                tf.WriteLine( "        obj.style.top = '" & request("top") & "';" )
                tf.WriteLine( "        obj.style.right = '" & request("right") & "';" )
                tf.WriteLine( "        obj.style.bottom = '" & request("bottom") & "';" )
                tf.WriteLine( "        obj.style.left = '" & request("left") & "';" )
                tf.WriteLine( "        obj.style.overflow = 'auto';" )
                tf.WriteLine( "    };" )
                tf.WriteLine( "    var server = '" & alerts_folder & "get_web_xml.asp';" )
                tf.WriteLine( "    var uname = '" & request("banner") & "';" )
                tf.WriteLine( "    var cnt = '1';" )
                tf.WriteLine( "    var deskbar_id = '" & request("deskbar_id") & "';" )
                tf.WriteLine( "    var version = '1.0.0.0';" )
                tf.WriteLine( "    var xml_alerts = getXObject();" )
                tf.WriteLine( "    var url = 'deskalerts_webplugin.php';" )
                tf.WriteLine( "    var params = 'uname='&uname&'&cnt='&cnt&'&deskbar_id='&deskbar_id&'&version='&version&'&getAlert=xml&host='&server&'';" )
                tf.WriteLine( "    xml_alerts.open('POST', url, true);" )
                tf.WriteLine( "    xml_alerts.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');" )
                tf.WriteLine( "" )
                tf.WriteLine( "    xml_alerts.onreadystatechange = function() {" )
                tf.WriteLine( "        if (xml_alerts.readyState == 4) {" )
                tf.WriteLine( "            if (xml_alerts.status == 200) {" )
                tf.WriteLine( "                var str = xml_alerts.responseText;" )
                tf.WriteLine( "                var count = 0;" )
                tf.WriteLine( "                var elem_result = document.getElementById('web_plugin');" )
                tf.WriteLine( "" )
                tf.WriteLine( "                var parser = new DOMParser();" )
                tf.WriteLine( "                var xmlDoc = parser.parseFromString(xml_alerts.responseText, 'application/xml');" )
                tf.WriteLine( "                var xmlAlert = xmlDoc.getElementsByTagName('ALERT');" )
                tf.WriteLine( "" )
                tf.WriteLine( "                for (var i = 0; i < xmlAlert.length; i&&) {" )
                tf.WriteLine( "                    var get_alert = getXObject();" )
                tf.WriteLine( "                    " )
                tf.WriteLine( "			        var url_getAlert = xmlAlert[i].getAttribute('href');" )
                tf.WriteLine( "			        var params2 = 'getAlert=alert&href='&url_getAlert&'';" )
                tf.WriteLine( "			        get_alert.open('POST', url , true);" )
                tf.WriteLine( "                    get_alert.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');" )
                tf.WriteLine( "" )
                tf.WriteLine( "                    get_alert.onreadystatechange = function() {" )
                tf.WriteLine( "                        if (get_alert.readyState == 4) {" )
                tf.WriteLine( "                            if (get_alert.status == 200) {" )
                tf.WriteLine( "                                var textAlert = get_alert.responseText;  " )
                tf.WriteLine( "								" )
                tf.WriteLine( "						        var myRe = /(?:html_title=\"").&(?:\"")/g;" )
                tf.WriteLine( "						        var html_title;" )
                tf.WriteLine( "						        while ((myArray = myRe.exec(textAlert)) != null) {" )
                tf.WriteLine( "							        html_title = myArray[0];" )
                tf.WriteLine( "						        }" )
                tf.WriteLine( "						        myRe = /html_title=\""(.&)\""/;" )
                tf.WriteLine( "						        html_title = html_title.replace(myRe,'$1');" )
                tf.WriteLine( "						        var item = document.getElementById('web_plugin');	" )
                tf.WriteLine( "								" )
                tf.WriteLine( "						        item.innerHTML = '<span>'&textAlert&'</span>';" )
                tf.WriteLine( "								" )
                tf.WriteLine( "						        setProperty(item);" )
                tf.WriteLine( "                            }" )
                tf.WriteLine( "                        }" )
                tf.WriteLine( "                    };" )
                tf.WriteLine( "" )
                tf.WriteLine( "			        get_alert.send(params2);" )
                tf.WriteLine( "                } " )
                tf.WriteLine( "            }" )
                tf.WriteLine( "        }" )
                tf.WriteLine( "    };" )
                tf.WriteLine( "    xml_alerts.send(params);" )


            tf.Close

            Response.Write alerts_folder & "admin/logs/"&request("banner")&".js"		

	end if
	
	
	if(request("name") = "ApproveAlert") then
	
	    alertId = request("alertId")
	    
	    if(alertId <> "") then
	       SQL = "UPDATE alerts SET approve_status = 1 WHERE id = " & alertId
	       Conn.Execute(SQL)
	       
	       set campaignIdSet = Conn.Execute("SELECT campaign_id FROM alerts WHERE id = " & alertId)
	       
	       if campaignIdSet("campaign_id") = "-1" then
                       'MEMCACHE
	       ' Set cacheMgr = CreateObject("DeskAlerts.CacheManager")
                ' cacheMgr.Update(clng(alertId), now())
                  MemCacheUpdate "Update", alertId
	       end if
	       
	    end if	
	end if
	
	if(request("name") = "getProperties") then
	        
	        alertId = request("alertId")
	        SQL = "SELECT urgent, aknown,  fullscreen, alert_width, alert_height, autoclose, self_deletable, from_date, to_date, sms, email, resizable FROM alerts WHERE id = " & alertId
	        Set alertSet = Conn.execute(SQL)
	        
	        
	        if not alertSet.EOF then
	            tableHtml = "<table ><tr><th>" & LNG_PROPERTY & "</th> <th>" & LNG_VALUE & "</th></tr>"
	            
	            urgent = LNG_NO
	            if(alertSet("urgent") = "1" ) then
	                urgent = LNG_YES
	            end if
	            tableHtml = tableHtml & "<tr><td>" & LNG_URGENT_ALERT & "</td><td style='text-align:center'>" & urgent & "</td></tr>"
	            
	            aknown = LNG_NO
	            if(alertSet("aknown") = "1" ) then
	                aknown = LNG_YES
	            end if
	            tableHtml = tableHtml & "<tr><td>" & LNG_ACKNOWLEDGEMENT & "</td><td style='text-align:center'>" & aknown & "</td></tr>"
	            
	            fullscreen = LNG_NO
	            if(alertSet("fullscreen") = "1") then
	                 fullscreen = LNG_YES
	             end if   
	            tableHtml = tableHtml & "<tr><td>" & LNG_FULLSCREEN & "</td><td style='text-align:center'>" & fullscreen & "</td></tr>"
	            
	            
	             self_deletable = LNG_NO
	            if(alertSet("self_deletable") = "1") then
	                 self_deletable = LNG_YES
	             end if   
	            tableHtml = tableHtml & "<tr><td>" & LNG_SELF_DELETABLE & "</td><td style='text-align:center'>" & self_deletable & "</td></tr>"
	            
	            tableHtml = tableHtml & "<tr><td>" & LNG_AUTOCLOSE_IN & "</td><td style='text-align:center'>" & alertSet("autoclose") & "</td></tr>"
	                
	            lifetime = DateDiff("n", alertSet("from_date"), alertSet("to_date"))
	            
	            
	              min_sel = ""
	             hour_sel = ""
	              day_sel = ""
	             factor = ""
	            if lifetime = "0" then lifetime = ""
	            if lifetime <> "" then
		            if MyMod(lifetime, 1440) = 0 then
			            lifetime = lifetime / 1440
			            factor = " days"
		            elseif MyMod(lifetime, 60) = 0 then
		            	lifetime = lifetime / 60
			            factor = " hours"
		           else
		            	factor = " minutes"
		            end if
	            else
		                factor = " minutes"
	            end if
	            
	            if lifetime <> "" then
	                
	                if(lifetime = "1") then
	                    factor = left(factor, len(factor) - 1)
	                end if
	             tableHtml = tableHtml & "<tr><td>" & LNG_LIFETIME & "</td><td style='text-align:center'>" & lifetime & factor & "</td></tr>"
	            end if
	                
	            sms = LNG_NO
	            
	            if(alertSet("sms") = "1") then
	                sms = LNG_YES
	            end if 
	            
	            tableHtml = tableHtml & "<tr><td>" & LNG_SMS & "</td><td style='text-align:center'>" & sms & "</td></tr>"
	            
	            mail = LNG_NO
	            
	            if( alertSet("email") = "1") then
                    mail = LNG_YES	            
   	            end if
   	            
   	            tableHtml = tableHtml & "<tr><td>" & LNG_EMAIL & "</td><td style='text-align:center'>" & mail & "</td></tr>"
	            
	            resizable = LNG_NO
	            
	            if(alertSet("resizable") = "1") then
	                resizable = LNG_YES
	            end if
	            
	            tableHtml = tableHtml & "<tr><td>" & LNG_RESIZABLE & "</td><td style='text-align:center'>" & resizable & "</td></tr>"
	            
	             tableHtml = tableHtml & "<tr><td>" & LNG_WIDTH & "</td><td style='text-align:center'>" & alertSet("alert_width") & "</td></tr>"
	              tableHtml = tableHtml & "<tr><td>" & LNG_HEIGHT & "</td><td style='text-align:center'>" & alertSet("alert_height") & "</td></tr>"
	              
	              Response.Write tableHtml
	        end if
	    
	
	end if
	
	
	
	if( request("name") = "RejectAlert") then
	    
	    alertId = request("alertId")
	    reason = request("reason")
	    
	    SQL = "UPDATE alerts SET approve_status = 2, reject_reason = N'" & reason &  "' WHERE id = " & alertId
	     Conn.Execute(SQL)
	
	end if
	  
	  if(request("name") = "DeleteRow") then
	  
	        SQL = "DELETE FROM campaigns WHERE id = " & request("value")
	        Conn.Execute(SQL)
	   end if
	   
	    if(request("name")="DupBanners") then
          id = Request("id")
          name = Request("bname")
          
          SQL= "INSERT INTO banners ([name],[date],[width],[width_unit],[height],[height_unit],[background_color],[transparent],[font_color],[font_size],[font_size_unit],[font_weight],[font_weight_unit],[text_align],[direction],[unicode_bidi],[border_weight],[border_weight_unit],[border_style],[border_color],[padding_top],[padding_top_unit],[padding_right],[padding_right_unit],[padding_bottom],[padding_bottom_unit],[padding_left],[padding_left_unit],[margin_top],[margin_top_unit],[margin_right],[margin_right_unit],[margin_bottom],[margin_bottom_unit],[margin_left],[margin_left_unit],[display],[position],[top],[top_unit],[right],[right_unit],[bottom],[bottom_unit],[left],[left_unit]) SELECT '"& name & "',[date],[width],[width_unit],[height],[height_unit],[background_color],[transparent],[font_color],[font_size],[font_size_unit],[font_weight],[font_weight_unit],[text_align],[direction],[unicode_bidi],[border_weight],[border_weight_unit],[border_style],[border_color],[padding_top],[padding_top_unit],[padding_right],[padding_right_unit],[padding_bottom],[padding_bottom_unit],[padding_left],[padding_left_unit],[margin_top],[margin_top_unit],[margin_right],[margin_right_unit],[margin_bottom],[margin_bottom_unit],[margin_left],[margin_left_unit],[display],[position],[top],[top_unit],[right],[right_unit],[bottom],[bottom_unit],[left],[left_unit] FROM banners WHERE id = " & id

          Conn.Execute(SQL)
          
          SQL = "INSERT INTO users(name,pass,deskbar_id,role,group_id,reg_date,client_version,domain_id) VALUES(N'"&request("bname")&"',N'"&md5("hyihyihyi")&"',N'"&request("deskbar_id")&"',N'U',0,N'"&Now&"',N'"&request("version")&"',1)"
		    Conn.Execute(SQL)
        end if
        if(request("name") = "DeleteBanner") then
	  
	        SQL = "DELETE FROM banners WHERE id = " & request("value")
	        Conn.Execute(SQL)
	   end if


       if(request("name") = "EditBanner") then
	  
	        SQL = "SELECT [name],[width],[width_unit],[max_width],[max_width_unit],[min_width],[min_width_unit],[height],[height_unit],[max_height] "&_
                    " ,[max_height_unit],[min_height],[min_height_unit],[background_color],[transparent],[font_color],[font_size],[font_size_unit]"&_
                    " ,[font_weight],[font_weight_unit],[text_align],[direction],[unicode_bidi],[border_weight],[border_weight_unit],[border_style],[border_color],[padding_top]"&_
                    " ,[padding_top_unit],[padding_right],[padding_right_unit],[padding_bottom],[padding_bottom_unit],[padding_left],[padding_left_unit],[margin_top]"&_
                    " ,[margin_top_unit],[margin_right],[margin_right_unit],[margin_bottom],[margin_bottom_unit],[margin_left],[margin_left_unit],[display],[position],[top]"&_
                    " ,[top_unit],[right],[right_unit],[bottom],[bottom_unit],[left],[left_unit] FROM banners WHERE id = " & request("value")
            Set data = Conn.Execute(SQL)

            resp = ""
	        For iCounter = 0 to 52
                  if (iCounter = 0) then 
                        resp = data(iCounter)
                  else 
                        resp = resp & "," & data(iCounter)	
                  end if              
	        Next
    	
	        Response.Write resp

	      end if


         if(request("name") = "slink") then
	  
	        SQL = "SELECT (CONVERT(nvarchar(50),id)+','+name+';') as val FROM alert_captions"
            Set data = Conn.Execute(SQL)

            resp = ""

	        do while not data.EOF 
	              resp = resp & data("val")
	              data.MoveNext
	        loop
    	
	        Response.Write resp

	      end if
	
'else
	'Response.Redirect "index.asp"
'end if

%>
<!-- #include file="db_conn_close.asp" -->