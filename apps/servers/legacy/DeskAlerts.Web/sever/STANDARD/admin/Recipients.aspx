﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Recipients.aspx.cs" Inherits="DeskAlertsDotNet.Pages.Recipients" %>

<%@ Import Namespace="Resources" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>DeskAlerts</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="css/style9.css" rel="stylesheet" type="text/css" />
    <link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="css/jquery/jquery_timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css" />

    <style>
        #bulkUploadOfRecipientsDiv label:hover {
            cursor: pointer;
        }
    </style>

    <script type="text/javascript" src="functions.js"></script>
    <script type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
    <script type="text/javascript" src="jscripts/date.js"></script>
    <script type="text/javascript" src="jscripts/jquery/jquery_timepicker/jquery-ui-timepicker-addon.js"></script>
    <script type="text/javascript" src="jscripts/jquery/datepicker_fix.js"></script>
    <script type="text/javascript">

        var parentType = "<% =ParentType %>";
        var parentId =  "<% =ParentId %>";

        $(document).ready(function () {
            $(".delete_button").button({});

            $("#selectAll").click(function () {
                var checked = $(this).prop('checked');

                var checkBoxSelector = !checked
                    ? "input[type=checkbox].content_object:checked"
                    : "input[type=checkbox].content_object:not(\":checked\")";

                $(checkBoxSelector).each(function () {
                    $(this).prop("checked", checked);

                    if (checked) {
                        window.parent.addItem(this);
                    } else {
                        window.parent.removeItem(this);
                    }
                });
            });

            $(".content_object").change(function () {
                var check = $(this).prop("checked");

                if (check) {
                    window.parent.addItem(this);
                } else {
                    window.parent.removeItem(this);
                }

                var checkedCheckBoxCount = $("input[type=checkbox].content_object:checked").length;

                if (checkedCheckBoxCount === 0) {
                    $("#selectAll").prop("checked", false);
                }
            });

            $(".filter").change(function () {
                window.parent.checkAllSelectedRecipients();
            });

            if (isParentChecked()) {
                changeStateOfAllVisibleRecipients(true);
            } else {
                checkSelectedVisibleRecipients();
            }


            function returnError(errorMessage) {
                    alert(errorMessage);
                    $(this).val("");

                    return false;
            }


            $("#recipientsUploadInput").on("change", function () {
                const splittedFilename = $(this).val().split(".");
                // -1 because array index starts with 0
                const fileExtention = splittedFilename[splittedFilename.length - 1];
                var fileName = $(this).val().substring($(this).val().lastIndexOf("\\") + 1, $(this).val().length);
                const allowedExtentions = ["csv", "xls", "xlsx"];
                if (!allowedExtentions.includes(fileExtention)) {
                    returnError("You can use only .csv, .xls or .xlsx files!");
                }
                else if (!validateFileName(fileName)) {
                    returnError('<%=resources.PLEASE_DO_NOT_USE_FILES_WITH_NAMES_MORE_THAN_50_CHARACTERS%> <%=HttpUtility.JavaScriptStringEncode(resources.PLEASE_USER_ONLY_FILE_NAME_ALLOWED_CHARACTERS)%>');
                }
                else {
                    __doPostBack();
                }
            });
        });

        function isParentChecked() {

            if (!isOuDisplayed())
                return false;

            var recipientsArray = window.parent.document.getElementsByName("added_recipient");

            return $(recipientsArray).filter("#OU_" + parentId).length > 0;
        }


        function isOuDisplayed() {
            return parentType.toUpperCase() === "OU";
        }

        function ouStateChanged(ouId, state) {

            if (!isOuDisplayed())
                return;

            if (ouId == parentId) {
                changeStateOfAllVisibleRecipients(state);
            }

        }

        function checkSelectedVisibleRecipients() {

            var recipientsArray = window.parent.document.getElementsByName("added_recipient");

            if (recipientsArray) {

                for (i = 0; i < recipientsArray.length; i++) {

                    var recipientInput = recipientsArray[i];

                    var id = recipientInput.getAttribute("id");

                    switch (id.charAt(0)) {
                        case 'u':
                            {
                                $("#recipient_user_" + id).prop('checked', true);
                                break;
                            }

                        case 'g':
                            {
                                $("#recipient_group_" + id).prop('checked', true);
                                break;
                            }

                        case 'c':
                            {
                                $("#recipient_computer_" + id).prop('checked', true);
                                break;
                            }
                    }
                }
            }
        }

        function changeStateOfAllVisibleRecipients(state) {
            $(".content_object").prop("checked", state);
            $(".content_object").prop("disabled", state);
            $("#selectAll").prop("checked", state);
            $("#selectAll").prop("disabled", state);
        }

        function checkRecipient(id) {
            $("#" + id).prop('checked', true);
        }

        function unCheckRecipient(id) {
            $("#" + id).prop('checked', false);
        }

        function iframeChangeFilter(id, value) {
            document.getElementById(id).checked = value;

            if (value == false) {
                if (document.getElementById("filterUsers").checked == false && document.getElementById("filterGroups").checked == false && document.getElementById("filterComputers").checked == false) {
                    document.getElementById(id).checked = true;
                    alert("You can't uncheck all filters.");
                    return false;
                }
            }

            return false;
        }

        function getUsersFilter() {

            return $("#filterUsers").prop("checked");
        }

        function getGroupsFilter() {

            return $("#filterGroups").prop("checked");
        }

        function getComputersFilter() {
            return $("#filterComputers").prop("checked");
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
            <tr>
                <td>
                    <table height="100%" cellspacing="0" cellpadding="0" class="main_table" style="border: none;">
                        <tr></tr>
                        <tr>
                            <td class="main_table_body" height="100%">
                                <div runat="server" id="bulkUploadOfRecipientsDiv">
                                    <div class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
                                        <label class="ui-button-text" for="recipientsUploadInput">Upload list of recipients (.csv,.xls or .xlsx file)</label>
                                    </div>
                                    <asp:FileUpload runat="server" OnChanged="recipientsUploadInput_OnChanged" ID="recipientsUploadInput" accept=".csv,.xls,.xlsx" AutoPostBack="True" Style="opacity: 0; margin-bottom: 10px;" />
                                    <asp:LinkButton class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" ID="cleanUploadBulkOfRecipientsDirectoryButton" name="cleanUploadbulkOfRecipientsDirectoryButton" runat="server">
                                    <label class="ui-button-text">Delete uploaded list of recipients</label>
                                    </asp:LinkButton>
                                </div>
                                <br />
                                <br />
                                <table style="padding-left: 10px;" width="50%" border="0">
                                    <tbody>
                                        <tr valign="middle">
                                            <td width="240">
                                                <% = resources.LNG_SEARCH_OBJECTS%>
                                                <input type="text" id="searchTermBox" runat="server" name="uname" value="" />
                                            </td>
                                            <td width="1%">
                                                <br />
                                                <asp:LinkButton runat="server" class="search_button ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" ID="searchButton" Style="margin-right: 0px" role="button" aria-disabled="false" Text="<span class='ui-button-icon-primary ui-icon ui-icon-search'></span><span class='ui-button-text'>{Search}</span>" />
                                                <input type="hidden" name="search" value="1">
                                            </td>
                                            <td valign="middle">&nbsp;</td>
                                            <td runat="server" id="showLabel" valign="middle"><%= resources.LNG_SHOW%>:</td>
                                            <td runat="server" id="usersFilter" valign="middle">
                                                <asp:CheckBox CssClass="filter" OnCheckedChanged="FilterComputers_OnCheckedChanged" onclick="iframeChangeFilter('filterUsers', document.getElementById('filterUsers').checked)" name="fu" AutoPostBack="True" ID="filterUsers" runat="server" Checked="True" />
                                            </td>
                                            <td runat="server" id="usersLabel" valign="middle">
                                                <label for="filter_users"><%= resources.LNG_USERS%></label>
                                            </td>
                                            <td runat="server" id="groupsFilter" valign="middle">
                                                <asp:CheckBox CssClass="filter" OnCheckedChanged="FilterComputers_OnCheckedChanged" onclick="iframeChangeFilter('filterGroups', document.getElementById('filterGroups').checked)" runat="server" AutoPostBack="True" name="fg" ID="filterGroups" />
                                            </td>
                                            <td runat="server" id="groupsLabel" valign="middle">
                                                <label for="filter_groups"><%= resources.LNG_GROUPS%></label>
                                            </td>
                                            <td runat="server" id="computersFilter" valign="middle">
                                                <asp:CheckBox CssClass="filter" OnCheckedChanged="FilterComputers_OnCheckedChanged" onclick="iframeChangeFilter('filterComputers', document.getElementById('filterComputers').checked)" runat="server" AutoPostBack="True" name="fc" ID="filterComputers" value="1" />
                                            </td>
                                            <td runat="server" id="computersLabel" valign="middle">
                                                <label for="filter_computers"><%= resources.LNG_COMPUTERS%></label>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div style="margin-left: 10px" runat="server" id="mainTableDiv">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td>
                                                <div style="margin-left: 10px" id="topPaging" runat="server"></div>
                                            </td>
                                        </tr>
                                    </table>
                                    <asp:Table Style="margin: 10px 10px 0 10px; padding-left: 0px;" runat="server" ID="contentTable" Width="40.5%" Height="90%" CssClass="data_table" CellSpacing="0" CellPadding="3">
                                        <asp:TableRow CssClass="data_table_title">
                                            <asp:TableCell runat="server" ID="headerSelectAll" CssClass="table_title" Width="2%">
                                                <asp:CheckBox ID="selectAll" runat="server" />
                                            </asp:TableCell>
                                            <asp:TableCell runat="server" ID="nameCell" CssClass="table_title"></asp:TableCell>
                                            <asp:TableCell runat="server" ID="typeCell" CssClass="table_title"></asp:TableCell>
                                            <asp:TableCell runat="server" ID="dislplayNameCell" CssClass="table_title"></asp:TableCell>
                                            <asp:TableCell runat="server" ID="domainName" CssClass="table_title"></asp:TableCell>
                                            <asp:TableCell runat="server" ID="mobilePhoneCell" CssClass="table_title"></asp:TableCell>
                                            <asp:TableCell runat="server" ID="emailCell" CssClass="table_title"></asp:TableCell>
                                            <asp:TableCell runat="server" ID="onlineCell" CssClass="table_title"></asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td>
                                                <div style="margin-left: 10px" id="bottomRanging" runat="server"></div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div runat="server" id="NoRows">
                                    <br />
                                    <br />
                                    <center>
                                    <b><% = resources.LNG_THERE_ARE_NO_OBJECTS%></b>
                                </center>
                                </div>
                                <br>
                                <br>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
