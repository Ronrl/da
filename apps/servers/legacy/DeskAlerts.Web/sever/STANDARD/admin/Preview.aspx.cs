using DeskAlerts.Server.Utils;
using DeskAlerts.Server.Utils.Helpers;
using DeskAlertsDotNet.Parameters;
using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Xml;

namespace DeskAlertsDotNet.Pages
{
    public partial class Preview : DeskAlertsBasePage
    {
        protected int CaptionWidth;
        protected int CaptionHeight;
        protected int BodyHeight;
        protected int BodyWidth;
        protected string CaptionBackground;
        protected string BodyTopMargin;
        protected string BodyLeftMargin;
        protected string ExtProps;
        protected string CaptionHref;
        protected string SkinId;
        protected string NeedQuestion;
        protected string CaptionHtml;
        protected string CustomJs;
        protected string AlertHtml;
        protected string AlertTitle;
        protected int LeftMargin;
        protected int RightMargin;
        protected int TopMargin;
        protected int BottomMargin;
        protected int ConfHeight;

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            SetNoCacheLoading();
            AlertHtml = Request.GetParam("alert_html", string.Empty);
            bool isLtrTicker = AlertHtml.IndexOf("dir=\"ltr\"") > 0;
            AlertTitle = Request.GetParam("alert_title", string.Empty);

            //static field for adding print button to the preview it is using in the PreviewCaption.aspx.cs
            addPrintButton = AlertHtml.IndexOf("<!-- printable_alert -->") > 0;

            AddMarkupForMultilanguageAlert();

            int alertWidth = Request.GetParam("alert_width", 500);
            int alertHeight = Request.GetParam("alert_height", 400);
            int fullscreen = Request.GetParam("full_screen", 0);
            string createDate = Request.GetParam("create_date", "").Replace("T", " ");
            string topTemplate = Request.GetParam("top_template", "");
            string bottomTemplate = Request.GetParam("bottom_template", "");
            string ticker = Request.GetParam("ticker", "");
            int acknowledgement = Request.GetParam("acknowledgement", 0);
            CaptionHtml = Request.GetParam("caption_html", "");
            CaptionHref = Request.GetParam("caption_href", "");
            string previewType = Request.GetParam("type", "desktop");
            NeedQuestion = Request.GetParam("need_question", "false");
            SkinId = Request.GetParam("skin_id", "default");
            bool isTicker = ticker == "1";
            bool isSms = previewType == "sms";
            string urgent = Request.GetParam("urgent", "");
            bool rsvp = Request.GetParam("need_question", 0) == 1;
            string question = Request.GetParam("question", "");

            AddBracketsFoSkinId();

            if (isTicker && !isSms)
            {
                CaptionHref = ".\\tickercaption.html";
                CustomJs = ".\\ticker.js";
            }

            RsvpMarkUp(rsvp, question);

            AddMarkupForAcknowledgement(acknowledgement, isTicker, isSms, isLtrTicker);

            AddMarkupForVideoAlert();

            string confFile = $"skins/{SkinId}/blank/conf.xml";
            string confFilePath = HttpContext.Current.Server.MapPath(confFile);

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(confFilePath);

            XmlNodeList nodeList = xmlDoc.SelectNodes("/ALERTS/COMMANDS/ALERT/WINDOW");

            string alertWindowName = "alertwindow1";
            string tickerWindowName = "tickerwindow1";

            if (nodeList != null)
                foreach (XmlNode window in nodeList)
                {
                    if (window.InnerText.Replace(" ", "").Replace("\"", "").Replace("'", "").ToLower() == "#ticker#==0")
                    {
                        alertWindowName = window.Attributes["name"].Value;
                    }
                    else if (window.InnerText.Replace(" ", "").Replace("\"", "").Replace("'", "").ToLower() ==
                             "#ticker#==1")
                    {
                        tickerWindowName = window.Attributes["name"].Value;
                    }
                }

            XmlNodeList propertyNode = xmlDoc.SelectNodes("/ALERTS/SETTINGS/PROPERTY");
            string rootPath = ".";
            Dictionary<string, object> properties = new Dictionary<string, object>();
            foreach (XmlNode property in propertyNode)
            {
                string name = property.Attributes["id"].Value;

                if (properties.ContainsKey(name))
                    properties.Remove(name);

                properties.Add(name, property);
            }

            if (properties.ContainsKey("postpone_mode"))
            {
                properties["postpone_mode"] = Settings.Content["ConfEnablePostponeInPreview"];
            }
            else
            {
                properties.Add("postpone_mode", Settings.Content["ConfEnablePostponeInPreview"]);
            }

            var previewWindowName = isTicker && !isSms
                ? tickerWindowName
                : alertWindowName;

            var alertWindow = xmlDoc.SelectSingleNode($"/ALERTS/VIEWS/WINDOW[@name='{previewWindowName}']");

            CaptionHeight = alertHeight;
            CaptionWidth = alertWidth;

            var captionPosition = fullscreen == 1
                ? "fullscreen"
                : alertWindow.Attributes["position"].Value;

            switch (previewType)
            {
                case "desktop":

                    CaptionBackground = "white";

                    if (CaptionHref.Length > 0 && alertWindow.Attributes["captionHref"] != null)
                    {
                        CaptionHref = alertWindow.Attributes["captionHref"].Value.Replace("%root_path%", rootPath);
                    }

                    CustomJs = alertWindow.Attributes["customjs"].Value.Replace("%root_path%", rootPath);

                    LeftMargin = Convert.ToInt32(alertWindow.Attributes["leftmargin"].Value);
                    RightMargin = Convert.ToInt32(alertWindow.Attributes["rightmargin"].Value);
                    TopMargin = Convert.ToInt32(alertWindow.Attributes["topmargin"].Value);
                    BottomMargin = Convert.ToInt32(alertWindow.Attributes["bottommargin"].Value);
                    BodyWidth = CaptionWidth - LeftMargin - RightMargin;

                    break;
                case "sms":

                    CaptionBackground = "transparent";

                    CaptionHtml = "<html>" +
                                  "<head></head><body style='background:transparent; padding:0px; margin:0px'>" +
                                  "<div class='png' style='background: url(../../../images/mobile_phone.png);width:100%; height:100%; position:absolute; left:0px; top:0px'></div>" +
                                  "<div class='png' style='position:absolute; top:12px; right:4px; width:20px; height:20px; background:url(../../../images/close_sms_preview.png); cursor:pointer; cursor:hand' onclick='window.external.Close();'/></div>" +
                                  "</body></html>";

                    TopMargin = 48;
                    BottomMargin = 39;
                    LeftMargin = 6;
                    RightMargin = 6;

                    BodyHeight = 313;
                    BodyWidth = 170;

                    AlertHtml = DeskAlertsUtils.HTMLToText(AlertHtml);
                    break;
            }

            BodyHeight = CaptionHeight - TopMargin - BottomMargin;

            if (isTicker && !isSms)
            {
                TopMargin = Convert.ToInt32(alertWindow.Attributes["topmargin"].Value);
                BottomMargin = Convert.ToInt32(alertWindow.Attributes["bottommargin"].Value);
                CaptionHeight = Convert.ToInt32(alertWindow.Attributes["height"].Value);
                BodyHeight = CaptionHeight - TopMargin - BottomMargin;
                BodyWidth = CaptionWidth - LeftMargin - RightMargin;
            }

            if (createDate.Length == 0)
                createDate = DateTimeConverter.ToUiDateTime(DateTime.Now);

            ExtProps = "{";

            foreach (KeyValuePair<string, object> keyValuePair in properties)
            {
                string name = keyValuePair.Key;
                XmlNode node = keyValuePair.Value as XmlNode;

                try
                {
                    if (node == null)
                    {
                        if (keyValuePair.Value is string)
                        {

                            string value = keyValuePair.Value.ToString();

                            ExtProps +=
                                "\"" + name + "\" : {\"value\":\"" + value + "\", \"cnst\":\"" +
                               1 + "\"},";
                        }
                    }
                    else
                    {

                        string constant = node.Attributes["const"] == null ? "1" : node.Attributes["const"].Value;
                        ExtProps +=
                           "\"" + name + "\" : {\"value\":\"" + node.Attributes["default"].Value + "\", \"cnst\":\"" +
                           constant + "\"},";
                    }

                }
                catch (Exception ex)
                {
                    Logger.Debug(ex);

                    Response.WriteLine("Error on parsing parameter:" + name);
                }
            }

            ExtProps += @"""root_path"": { ""value"": """ + rootPath + @""", ""cnst"":0}, ";
            ExtProps += @"""create_date"": { ""value"": """ + createDate + @""", ""cnst"":0}, ";
            ExtProps += @"""title"": { ""value"": """ + HttpUtility.HtmlEncode(AlertTitle).Replace("&#39;", "\'") + @""", ""cnst"":0}, ";
            ExtProps += @"""top_template"": { ""value"": """ + topTemplate + @""", ""cnst"":0}, ";
            ExtProps += @"""bottom_template"": { ""value"": """ + (bottomTemplate) + @""", ""cnst"":0}, ";
            ExtProps += @"""acknowledgement"": { ""value"": """ + acknowledgement + @""", ""cnst"":0},";
            ExtProps += @"""topmargin"": { ""value"": """ + TopMargin + @""", ""cnst"":0},";
            ExtProps += @"""bottommargin"": { ""value"": """ + BottomMargin + @""", ""cnst"":0},";
            ExtProps += @"""leftmargin"": { ""value"": """ + LeftMargin + @""", ""cnst"":0},";
            ExtProps += @"""rightmargin"": { ""value"": """ + RightMargin + @""", ""cnst"":0},";
            ExtProps += @"""server"": { ""value"": """ + Config.Data.GetString("alertsDir") + @""", ""cnst"":0},";
            ExtProps += @"""skin_id"": { ""value"": """ + SkinId + @""", ""cnst"":0},";
            ExtProps += @"""need_question"": { ""value"": """ + NeedQuestion + @""", ""cnst"":0},";
            ExtProps += @"""urgent"": { ""value"": """ + urgent + @""", ""cnst"":0},";
            ExtProps += @"""position"": { ""value"": """ + captionPosition + @""", ""cnst"":0},";
            ExtProps += @"""isHtml"": { ""value"": ""1"", ""cnst"":0}";


            ExtProps += "}";
        }

        private void SetNoCacheLoading()
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.AppendCacheExtension("no-store, must-revalidate");
            Response.AppendHeader("Pragma", "no-cache");
            Response.AppendHeader("Expires", "0");
        }

        private void AddMarkupForMultilanguageAlert()
        {
            if (Config.Data.IsOptionEnabled("ConfEnableAlertLangs"))
            {
                var matches = System.Text.RegularExpressions.Regex.Matches(
                    AlertHtml, "<!-- begin_lang .*? -->(.*?)<!-- end_lang -->",
                    System.Text.RegularExpressions.RegexOptions.Singleline);
                if (matches.Count > 0)
                {
                    AlertHtml = matches[0].Groups[1].Value;

                    var titleMatches =
                        System.Text.RegularExpressions.Regex.Matches(AlertHtml, "<!-- html_title *= *(['\"])(.*?)' *-->");

                    if (titleMatches.Count > 0)
                    {
                        AlertTitle = titleMatches[titleMatches.Count - 1].Groups[2].Value;
                    }
                }
            }
        }

        private void AddMarkupForVideoAlert()
        {
            var isVideoAlert = AlertHtml.Contains("%video%=");
            if (isVideoAlert)
            {
                AlertHtml = DeskAlertsUtils.GenerateVideoAlertHtml(AlertHtml);
            }
        }

        private void AddMarkupForAcknowledgement(int acknowledgement, bool isTicker, bool isSms, bool isLtrTicker)
        {
            if (acknowledgement == 1)
            {
                var sb = new StringBuilder();
                var readitButton = Config.Data.GetString("alertsDir") + "/admin/skins/" + SkinId + "/version/read_it.gif";
                sb.Append($"<img id='readitButton'");
                sb.Append($" class='readitButton' ");
                if (isTicker && !isSms)
                {
                    sb.Append(
                        $" style='position: relative !important; display: inline-block !important; margin-left: 1rem !important;'");
                }
                else
                {
                    sb.Append($" style='position: absolute; float: right; right: 23px;'");
                }

                sb.Append($" src='{readitButton}'");
                sb.Append($" value='{readitButton}'");
                sb.Append($" onclick='");
                sb.Append($" window.external.Close();' /><!-- cannot close -->");

                AlertHtml = isLtrTicker ? sb.ToString() + AlertHtml : AlertHtml + sb.ToString();
            }
        }

        private void AddBracketsFoSkinId()
        {
            if (IsNotDefaultSkinAndWithoutBrackets(SkinId))
            {
                SkinId = "{" + SkinId + "}";

                ReplaceDoubleCurvedBracketsInSkinId();
            }
        }

        private void ReplaceDoubleCurvedBracketsInSkinId()
        {
            SkinId = SkinId
                .Replace("{{", "{")
                .Replace("}}", "}");
        }

        private void RsvpMarkUp(bool rsvp, string question)
        {
            if (!rsvp)
            {
                return;
            }

            var needFirstQuestion = Request.GetParam("need_question", string.Empty);

            AlertHtml += "<hr/><form>";

            if (needFirstQuestion == "1")
            {
                AlertHtml += $"<div style='font-family: arial,helvetica,sans-serif;'>{question}</div>";
                AlertHtml += "<table style=\"margin: 0 0 10px 0;\">";
                for (int i = 1; i <= 10; i++)
                {
                    var param = $"question_option{i}";
                    string questionOption = Request.GetParam(param, "");

                    if (questionOption.Length > 0)
                    {
                        AlertHtml += $"<tr><td style='width:10px'><input type='radio' name='answer' id='answer{i}' value='{i}'/></td><td align='left'><div style='font-family: arial,helvetica,sans-serif;'>{questionOption}</div></td></tr>";
                    }
                }
                AlertHtml += "</table>";
            }

            var needSecondQuestion = Request.GetParam("need_second_question", string.Empty);
            var secondQuestion = Request.GetParam("second_question", string.Empty);
            if (needSecondQuestion == "1")
            {
                AlertHtml += $"<div style='font-family: arial,helvetica,sans-serif;'>{secondQuestion}</div>";
                AlertHtml += $"<div><textarea cols='50' rows='5' id='custom_answer' name='custom_answer'></textarea></div>";
            }

            const string submitButton = "../../../images/submit_button.gif";

            AlertHtml += "<br/>";
            AlertHtml += $"<img src=\"{submitButton}\" value=\"+submitButton+\" name=\"submit\" value=\"Submit\" onclick='window.external.Close();'>";
            AlertHtml += "</form>";
        }

        private bool IsNotDefaultSkinAndWithoutBrackets(string skinId)
        {
            return !skinId.StartsWith("{") && !skinId.EndsWith("}") && skinId != "default";
        }
    }
}