<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%
'check_session()
%>
<!-- #include file="Common.asp" -->
<!-- #include file="header.asp" -->
<!-- #include file="footer.asp" -->
<!-- #include file="libs/FusionCharts.asp" -->
<%
'On Error Resume Next
' index CustomIncludes End
'-------------------------------

'===============================
' Save Page and File Name available into variables
'-------------------------------
Dim sFileName
Dim sTemplateFileName
Dim sPaginateFileName
Dim intSessionUserId
Dim groupAlert
Dim alGuid

sFileName = "statistics_da_alerts_view_users.asp"
sTemplateFileName = "statistics_da_alerts_view_users.html"
sPaginateFileName = "paginate.html"
'===============================
  

intSessionUserId = Session("uid")
if (true) then


' index Show begin

'===============================
' Display page
'-------------------------------
' Load HTML template for this page
'-------------------------------
LoadTemplate alerts_dir & "\" & templateFolder & "\" & sTemplateFileName, "main"
SetVar "DListUsersStat", ""
'-------------------------------
' Load HTML template of Header and Footer
'-------------------------------
LoadTemplate sHeaderFileName, "Header"
LoadTemplate sFooterFileName, "Footer"
LoadTemplate sSearchDetailsFormFileName, "SearchForm"
'-------------------------------
SetVar "Menu", ""
SetVar "DateForm", ""

pageType=request("type")
alert_id=Clng(request("id"))

searchSQL = ""
search = request("search")
if search <> "" then searchSQL = " AND (users.name LIKE N'%"&replace(search, "'", "''")&"%' OR users.display_name LIKE N'%"&replace(search, "'", "''")&"%')"


if(pageType="rec") then
	PageName(LNG_RECEIVED)
end if

if(pageType="ack") then
	PageName(LNG_ACKNOWLEDGED)
end if

if(pageType="drec") then
	PageName(LNG_NOT_RECEIVED)
end if

if(pageType="ans") then
	PageName(LNG_ANSWERED)
end if

if(Request("offset") <> "") then 
	offset = clng(Request("offset"))
else 
	offset = 0
end if	

if(Request("limit") <> "") then 
	limit = clng(Request("limit"))
else 
	limit=25
end if

SetVar "searchTerms", HTMLEncode(search)
SetVar "page", sFileName & "?id="&alert_id&"&type="&pageType&"&offset=0&limit="&limit&"&sortby="
SetVar "searchName", "users"

SetVar "LNG_USERNAME", LNG_USERNAME
SetVar "default_lng", default_lng

'doesnt received
	Set alertRS = Conn.Execute("SELECT type2, sent_date, class FROM alerts WHERE id="& alert_id)
	if(alertRS.EOF) then
		Set alertRS = Conn.Execute("SELECT type2, sent_date, class FROM archive_alerts WHERE id="& alert_id)
	end if
	if(Not alertRS.EOF) then

	mytype=alertRS("type2")
	strAlertDate = alertRS("sent_date")
	classid = alertRS("class")
	if classid = "2" or classid = "8" then
		received_from = "alerts_read"
		archive_received_from = "archive_alerts_read"
	else
		received_from = "alerts_received"
		archive_received_from = "archive_alerts_received"
	end if
	
	'alertRS.Close

Set groupCheck = Conn.Execute("SELECT id, part_id FROM multiple_alerts WHERE alert_id=" & alert_id)
if(groupCheck.EOF) then
    groupAlert = 0
else
    groupAlert = 1    
end if

if(pageType="drec") then
	if(mytype="R") then
        if groupAlert = 0 then
		    SQL = "SELECT COUNT(DISTINCT id) as mycnt FROM (SELECT users.id FROM users INNER JOIN alerts_sent_stat s ON users.id=s.user_id LEFT JOIN "& received_from &" r ON r.alert_id=s.alert_id AND r.user_id=users.id WHERE role='U' AND r.id IS NULL AND s.alert_id="& alert_id & searchSQL
		    SQL = SQL & " UNION ALL "
		    SQL = SQL & "SELECT users.id FROM users INNER JOIN archive_alerts_sent_stat s ON users.id=s.user_id LEFT JOIN "& archive_received_from &" r ON r.alert_id=s.alert_id AND r.user_id=users.id WHERE role='U' AND r.id IS NULL AND s.alert_id="& alert_id & searchSQL
		    SQL = SQL & ") t" 
        else    
            SQL = "SELECT stat_alert_id FROM multiple_alerts WHERE alert_id =" & alert_id
            Set RS7 = Conn.Execute(SQL)
            alGuid = RS7("stat_alert_id")    

            SQL = "SELECT COUNT(DISTINCT id) as mycnt FROM (SELECT users.id FROM users INNER JOIN alerts_sent_stat s ON users.id=s.user_id LEFT JOIN "& received_from &" r ON r.alert_id=s.alert_id AND r.user_id=users.id INNER JOIN multiple_alerts ma ON ma.alert_id = s.alert_id WHERE role='U' AND r.id IS NULL AND ma.stat_alert_id = '"& alGuid & "' " & searchSQL
		    SQL = SQL & " UNION ALL "
		    SQL = SQL & "SELECT users.id FROM users INNER JOIN archive_alerts_sent_stat s ON users.id=s.user_id LEFT JOIN "& archive_received_from &" r ON r.alert_id=s.alert_id AND r.user_id=users.id INNER JOIN multiple_alerts ma ON ma.alert_id = s.alert_id WHERE role='U' AND r.id IS NULL AND ma.stat_alert_id = '"& alGuid & "' " & searchSQL
		    SQL = SQL & ") t" 
        end if     
		Set RS = Conn.Execute(SQL)
	end if

	if(mytype="B") then
		SQL = "SELECT COUNT(DISTINCT id) as mycnt FROM users "
		SQL = SQL & " LEFT JOIN (SELECT user_id FROM "& received_from &" WHERE alert_id="& alert_id
		SQL = SQL & " UNION "
		SQL = SQL & " SELECT user_id FROM "& archive_received_from &" WHERE alert_id="& alert_id
		SQL = SQL & ") r ON  r.user_id=users.id "
		SQL = SQL & "WHERE role='U' AND ( client_version != 'web client' OR client_version is NULL ) AND reg_date <='"&strAlertDate&"' AND r.user_id IS NULL " & searchSQL
		Set RS = Conn.Execute(SQL)
	end if
end if

if(pageType="rec") then
		SQL = "SELECT COUNT(DISTINCT id) as mycnt FROM (SELECT users.id FROM users INNER JOIN "& received_from &" ON users.id="& received_from &".user_id WHERE role='U' AND "& received_from &".alert_id="& alert_id & searchSQL
		SQL = SQL & " UNION ALL "
		SQL = SQL & "SELECT users.id FROM users INNER JOIN "& archive_received_from &" ON users.id="& archive_received_from &".user_id WHERE role='U' AND ( client_version != 'web client' OR client_version is NULL ) AND "& archive_received_from &".alert_id="& alert_id & searchSQL
		SQL = SQL & ") t"
		Set RS = Conn.Execute(SQL)
end if

if(pageType="ack") then
		SQL = "SELECT COUNT(DISTINCT id) as mycnt FROM (SELECT users.id FROM users INNER JOIN alerts_read ON users.id=alerts_read.user_id WHERE role='U' AND alerts_read.alert_id="& alert_id & searchSQL
		SQL = SQL & " UNION ALL "
		SQL = SQL & "SELECT users.id FROM users INNER JOIN archive_alerts_read ON users.id=archive_alerts_read.user_id WHERE role='U' AND ( client_version != 'web client' OR client_version is NULL )  AND archive_alerts_read.alert_id="& alert_id & searchSQL
		SQL = SQL & ") t"
		Set RS = Conn.Execute(SQL)
end if

if(pageType = "ans") then
		SQL = "SELECT COUNT(DISTINCT s.user_id) as mycnt FROM surveys_main m INNER JOIN surveys_questions q ON q.survey_id=m.id INNER JOIN surveys_answers a ON a.question_id=q.id INNER JOIN surveys_answers_stat s ON s.answer_id=a.id WHERE m.sender_id=" & alert_id & searchSQL
		Set RS = Conn.Execute(SQL)
end if

'if(pageType = "notvoted") then
'		SQL = "SELECT COUNT(DISTINCT id) as mycnt FROM ("
'		
'		SQL = SQL & "SELECT users.id FROM users INNER JOIN "& received_from &" ON users.id="& received_from &".user_id WHERE role='U' AND "& received_from &".alert_id="& alert_id & searchSQL
'		SQL = SQL & " UNION ALL "
'		SQL = SQL & "SELECT users.id FROM users INNER JOIN "& archive_received_from &" ON users.id="& archive_received_from &".user_id WHERE role='U' AND "& archive_received_from &".alert_id="& alert_id & searchSQL
'		SQL = SQL & ") t"
'		
'		"LEFT JOIN surveys_answers_stat s ON INNER JOIN surveys_answers a ON s.answer_id=a.id INNER JOIN surveys_questions q ON a.question_id=q.id INNER JOIN ON q.survey_id=m.id WHERE m.sender_id=" & alert_id & searchSQL
'		Set RS = Conn.Execute(SQL)


'		SQL = "SELECT COUNT(DISTINCT id) as mycnt FROM (SELECT users.id FROM users INNER JOIN "& received_from &" ON users.id="& received_from &".user_id WHERE role='U' AND "& received_from &".alert_id="& alert_id & searchSQL
'		SQL = SQL & " AND users.id not in (SELECT DISTINCT stats.user_id as id FROM surveys_answers_stat stats, surveys_answers answers, surveys_questions questions, surveys_main WHERE stats.answer_id=answers.id AND answers.question_id=questions.id AND questions.survey_id=surveys_main.id AND surveys_main.sender_id=" & alert_id &"))"
'		Set RS = Conn.Execute(SQL)
'end if

	cnt=0
	if(Not IsNull(RS)) then
		do while Not RS.EOF
			if(Not IsNull(RS("mycnt"))) then cnt=cnt+clng(RS("mycnt")) end if
			RS.MoveNext
		loop
		RS.Close
		j=cnt/limit
		end if
	end if

if(cnt>0) then
	page=sFileName & "?id="&alert_id&"&type="&pageType&"&search="&Server.URLEncode(search)&"&"
	name=LNG_USERS
	SetVar "paging", make_pages(offset, cnt, limit, page, name, sortby) 
end if

'show main table
sqlsql = ""

num=0
if(pageType="drec") then
	if(mytype="R") then
        if groupAlert = 0 then
		    SQL = "SELECT DISTINCT * FROM (SELECT users.id as id, users.name as name, users.display_name, users.domain_id as domain_id FROM users INNER JOIN alerts_sent_stat ON users.id=alerts_sent_stat.user_id WHERE role='U' AND users.id NOT IN (SELECT user_id FROM "& received_from &" WHERE alert_id="& alert_id &") AND alerts_sent_stat.alert_id="& alert_id & searchSQL
		    SQL = SQL & " UNION ALL "
		    SQL = SQL & "SELECT users.id as id, users.name as name, users.display_name, users.domain_id as domain_id FROM users INNER JOIN archive_alerts_sent_stat ON users.id=archive_alerts_sent_stat.user_id WHERE role='U' AND users.id NOT IN (SELECT user_id FROM "& archive_received_from &" WHERE alert_id="& alert_id &") AND archive_alerts_sent_stat.alert_id="& alert_id & searchSQL
		    SQL = SQL & ") t"   
        else
            SQL = "SELECT DISTINCT * FROM (SELECT users.id as id, users.name as name, users.display_name, users.domain_id as domain_id FROM users INNER JOIN alerts_sent_stat ON users.id=alerts_sent_stat.user_id INNER JOIN multiple_alerts ma ON ma.alert_id = alerts_sent_stat.alert_id  WHERE role='U' AND users.id NOT IN (SELECT user_id FROM "& received_from &" INNER JOIN multiple_alerts ma ON ma.alert_id = "& received_from &".alert_id  WHERE ma.stat_alert_id='"& alGuid &"') AND ma.stat_alert_id='"& alGuid &"' " & searchSQL
		    SQL = SQL & " UNION ALL "
		    SQL = SQL & "SELECT users.id as id, users.name as name, users.display_name, users.domain_id as domain_id FROM users INNER JOIN archive_alerts_sent_stat ON users.id=archive_alerts_sent_stat.user_id INNER JOIN multiple_alerts ma ON ma.alert_id = archive_alerts_sent_stat.alert_id WHERE role='U' AND users.id NOT IN (SELECT user_id FROM "& archive_received_from &" INNER JOIN multiple_alerts ma ON ma.alert_id = "& archive_received_from &".alert_id WHERE ma.stat_alert_id='"& alGuid &"') AND ma.stat_alert_id='"& alGuid &"' " & searchSQL
		    SQL = SQL & ") t"   
        sqlsql=SQL
        end if
		Set RS = Conn.Execute(SQL)        
	end if
	if(mytype="B") then
		SQL = "SELECT DISTINCT users.id as id, users.name as name, users.display_name, users.domain_id as domain_id FROM users "
		SQL = SQL & " LEFT JOIN (SELECT user_id FROM "& received_from &" WHERE alert_id="& alert_id
		SQL = SQL & " UNION "
		SQL = SQL & " SELECT user_id FROM "& archive_received_from &" WHERE alert_id="& alert_id
		SQL = SQL & ") r ON  r.user_id=users.id "
		SQL = SQL & "WHERE role='U' AND ( client_version != 'web client' OR client_version is NULL ) AND reg_date <='"&strAlertDate&"' AND r.user_id IS NULL " & searchSQL
		Set RS = Conn.Execute(SQL)
	end if
elseif(pageType="rec") then
		SQL = "SELECT DISTINCT * FROM (SELECT "& received_from &".user_id as id, users.name as name, users.display_name, users.domain_id as domain_id FROM users INNER JOIN "& received_from &" ON users.id="& received_from &".user_id WHERE role='U' AND "& received_from &".alert_id="& alert_id & searchSQL
		SQL = SQL & " UNION ALL "
		SQL = SQL & "SELECT "& archive_received_from &".user_id as id, users.name as name, users.display_name, users.domain_id as domain_id FROM users INNER JOIN "& archive_received_from &" ON users.id="& archive_received_from &".user_id WHERE role='U' AND "& archive_received_from &".alert_id="& alert_id & searchSQL
		SQL = SQL & ") t"
		Set RS = Conn.Execute(SQL)
elseif(pageType="ack") then
		SQL = "SELECT DISTINCT * FROM (SELECT users.id as id, users.name as name, users.display_name, users.domain_id as domain_id FROM users INNER JOIN alerts_read ON users.id=alerts_read.user_id WHERE role='U' AND alerts_read.alert_id="& alert_id & searchSQL
		SQL = SQL & " UNION ALL "
		SQL = SQL & "SELECT users.id as id, users.name as name, users.display_name, users.domain_id as domain_id FROM users INNER JOIN archive_alerts_read ON users.id=archive_alerts_read.user_id WHERE role='U' AND archive_alerts_read.alert_id="& alert_id & searchSQL
		SQL = SQL & ") t"
		Set RS = Conn.Execute(SQL)
elseif(pageType="ans") then
		SQL = "SELECT u.id, u.name, u.display_name, u.domain_id FROM users u WHERE u.id IN(SELECT DISTINCT s.user_id as mycnt FROM surveys_main m INNER JOIN surveys_questions q ON q.survey_id=m.id INNER JOIN surveys_answers a ON a.question_id=q.id INNER JOIN surveys_answers_stat s ON s.answer_id=a.id WHERE m.sender_id=" & alert_id & searchSQL & ")"
		Set RS = Conn.Execute(SQL)
end if
    SetVar "sqlsql", sqlsql
Do While Not RS.EOF
	num=num+1
	if(num > offset AND offset+limit >= num) then
   		if(IsNull(RS("name"))) then
			name="Unregistered"
		else
			strUserName = RS("name")
		end if
		if AD = 3 and RS("domain_id") then
			Set RS3 = Conn.Execute("SELECT name FROM domains WHERE id=" & RS("domain_id"))
			if(Not RS3.EOF) then
				domain=RS3("name")
			else
				domain="&nbsp;"
			end if
			RS3.close
		end if
		SetVar "strUserName", strUserName
		if Len(RS("display_name")) > 0 then
			SetVar "strDisplayName", " (" & RS("display_name") & ")"
		else
			SetVar "strDisplayName", ""
		end if
		Parse "DListUsersStat", True
	end if
	RS.MoveNext
Loop
RS.Close

'--------------------


'-------------------------------
' Step through each form
'-------------------------------
Header_Show
Footer_Show
'-------------------------------
' Process page templates
'-------------------------------
Parse "Header", False
Parse "Footer", False
Parse "main", False
'-------------------------------
' Output the page to the browser
'-------------------------------
Response.write PrintVar("main")

' index Show End

'-------------------------------
' Destroy all object variables
'-------------------------------

' index Close Event begin
' index Close Event End

UnloadTemplate
'===============================

else

Login(Request("intErrorCode"))

end if

'===============================
' Display Grid Form
'-------------------------------

%><!-- #include file="db_conn_close.asp" -->