﻿using System;

using DeskAlertsDotNet.DataBase;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;


namespace DeskAlertsDotNet.Pages
{
    public partial class GetAlertData : DeskAlertsBasePage
    {
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (string.IsNullOrEmpty(Request["id"]))
            {
                Response.End();
            }
            JObject jObject = new JObject();
            string query =
                "SELECT alert_text as alert_html, title as alert_title, alert_width, alert_height, aknown as acknowledgement, ticker," +
                " fullscreen, create_date, caption_id, template_id, class as classid, urgent, self_deletable FROM alerts WHERE id=" + Request["id"];

            var alertDataRow = dbMgr.GetDataByQuery(query).First();

            jObject = alertDataRow.ToJsonObject();

            if (!alertDataRow.IsNull("caption_id"))
            {
                string fileName =
                    dbMgr.GetScalarQuery<string>("SELECT file_name FROM alert_captions WHERE id = " +
                                                 alertDataRow.GetString("caption_id").Quotes());

                if (!string.IsNullOrEmpty(fileName))
                {
                    jObject.Add(new JProperty("caption_href", fileName));
                }
                else
                {
                    jObject.Add(new JProperty("skin_id", alertDataRow.GetString("caption_id")));
                }
            }
            else
            {
                jObject.Add(new JProperty("caption_href", string.Empty));
            }

            DataSet surveySet = dbMgr.GetDataByQuery("SELECT id from surveys_main WHERE sender_id = " + Request["id"]);

            if (!surveySet.IsEmpty)
            {
                int surveyId = surveySet.GetInt(0, "id");

                jObject.Add(new JProperty("need_question", true));
                DataSet firstQuestionSet =
                    dbMgr.GetDataByQuery(
                        "SELECT question, id  FROM surveys_questions WHERE question_number = 1 AND survey_id = " +
                        surveyId);

                jObject.Add(new JProperty("question", firstQuestionSet.GetString(0, "question").Replace("<p>", string.Empty).Replace("</p>", string.Empty)));

                DataSet firstQuestionOptions =
                    dbMgr.GetDataByQuery(
                        $"SELECT answer FROM surveys_answers WHERE question_id = {firstQuestionSet.GetString(0, "id")}");

                string questionType = dbMgr.GetScalarByQuery<string>(
                    $"SELECT question_type  FROM surveys_questions WHERE question_number = 1 AND survey_id = {surveyId}");

                if (questionType == "M")
                {
                    for (int i = 0; i < firstQuestionOptions.Count; i++)
                    {
                        jObject.Add(
                            new JProperty(
                                "question_option" + (i + 1),
                                firstQuestionOptions.GetString(i, "answer").Replace("<p>", string.Empty).Replace("</p>", string.Empty)));
                    }
                }

                jObject.Add(
                    new JProperty(
                        "question_options_count",
                        firstQuestionOptions.Count));

                DataSet secondQuestionSet =
                    dbMgr.GetDataByQuery(
                        $"SELECT question, id  FROM surveys_questions WHERE question_number = 2 AND survey_id = {surveyId}");

                if (!secondQuestionSet.IsEmpty)
                {
                    jObject.Add(new JProperty("need_second_question", true));
                    jObject.Add(new JProperty("second_question", secondQuestionSet.GetString(0, "question")));
                }
            }

            Response.Write(jObject.ToString(Formatting.Indented));
        }
    }
}