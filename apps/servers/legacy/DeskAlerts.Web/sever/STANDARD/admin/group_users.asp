﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%

function addGroups(id)
	if(id<>"") then
		Set RS = Conn.Execute("SELECT groups.id as id, name FROM groups_groups INNER JOIN groups ON groups_groups.group_id=groups.id WHERE container_group_id=" & id)
		Do While Not RS.EOF
				response.write "<option value=""groups_"&RS("id")&""">"&RS("name")&"</option>"
			RS.MoveNext
		loop
	end if
end function

function addUsers(id)
	if(id<>"") then
		Set RS = Conn.Execute("SELECT users.id as id, name FROM users_groups INNER JOIN users ON users_groups.user_id=users.id WHERE users_groups.group_id=" & id)
		Do While Not RS.EOF
				response.write "<option value=""users_"&RS("id")&""">"&RS("name")&"</option>"
			RS.MoveNext
		loop
	end if
end function

function addComputers(id)
	if(id<>"") then
		Set RS = Conn.Execute("SELECT computers.id as id, name FROM computers_groups INNER JOIN computers ON computers_groups.comp_id=computers.id WHERE group_id=" & id)
		Do While Not RS.EOF
				response.write "<option value=""computers_"&RS("id")&""">"&RS("name")&"</option>"
			RS.MoveNext
		loop
	end if
end function

function addOUs(id)
	if(id<>"") then
		Set RS = Conn.Execute("SELECT OU.id as id, name FROM ou_groups INNER JOIN OU ON ou_groups.ou_id=OU.Id WHERE group_id=" & id)
		Do While Not RS.EOF
				response.write "<option value=""ous_"&RS("id")&""">"&RS("name")&"</option>"
			RS.MoveNext
		loop
	end if
end function

check_session()

'display group form with users for add/remove

  uid = Session("uid")

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>DeskAlerts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/style9.css" rel="stylesheet" type="text/css">
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script language="javascript" type="text/javascript">

$(document).ready(function() {
	$("#tabs").tabs();
	$(".save_button").button();
	$(".cancel_button").button();
});

</script>
<%
if (uid <> "") then
	retPage = Request("return_page")
%>
<script language="javascript">

var jj;
jj = 0;

function select_all_users()
{
	var status = document.getElementById('select_all').checked;
	var field = document.getElementsByName('users');
	for (i = 0; i < field.length; i++)
		field[i].checked = status;
}

// code to create and make AJAX request
function getFile(pURL) {
	var str;
	str = "Loading, please wait...";
	document.getElementById("found_users").innerHTML = str;

	if (window.XMLHttpRequest) { // code for Mozilla, Safari, etc
		xmlhttp = new XMLHttpRequest();
	} else if (window.ActiveXObject) { //IE
		xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
	}

	// if we have created the xmlhttp object we can send the request
	if (typeof(xmlhttp) == 'object') {
		xmlhttp.onreadystatechange = xmlhttpResults;
		xmlhttp.open('GET', pURL, true);
		xmlhttp.send(null);
		// replace the submit button image with a please wait for the results image.
		//        document.getElementById('theImage').innerHTML = '<img id="submitImage" name="submitImage" src="images/ajaxCallChangeImage_wait.gif" alt="Please Wait for AJAX Results">';
		// otherwise display an error message
	} else {
		alert('Your browser is not remote scripting enabled. You can not run this example.');
	}
}

// function to handle asynchronous call
function xmlhttpResults() {
	if (xmlhttp.readyState == 4) {
		if (xmlhttp.status == 200) {
			// we use a delay only for this example
			//            setTimeout('loadResults()',3000);
			loadResults();
		}
	}
}

function loadResults(i) {
	// put the results on the page
	var str;
	str = xmlhttp.responseText;
	document.getElementById("found_users").innerHTML = str;
}

function my_sub(j)
{
	var list2 = document.getElementById("list2");
	var ids_users = document.getElementById("ids_users");
	var ids_groups = document.getElementById("ids_groups");
	
	<%
	if(AD=3) then
	%>
	var ids_ous = document.getElementById("ids_ous");
	var ids_ous_string="";

	var ids_computers = document.getElementById("ids_computers");
	var ids_computers_string="";

	<%
	end if
	%>
	var ids_users_string="";
	var ids_groups_string="";

	for (i=0; i<list2.options.length; i++)
	{
		if(list2.options[i].value.indexOf("groups_")==0){
			ids_groups_string = ids_groups_string +","+ list2.options[i].value.replace(/groups_/,"");
		}
		if(list2.options[i].value.indexOf("users_")==0){
			ids_users_string = ids_users_string +","+ list2.options[i].value.replace(/users_/,"");
		}
		<%
		if(AD=3) then
		%>

		if(list2.options[i].value.indexOf("ous_")==0){
			ids_ous_string = ids_ous_string +","+ list2.options[i].value.replace(/ous_/,"");
		}
		if(list2.options[i].value.indexOf("computers_")==0){
			ids_computers_string = ids_computers_string +","+ list2.options[i].value.replace(/computers_/,"");
		}
		
		<%
		end if
		%>
	}
	ids_users.value=ids_users_string;
	ids_groups.value=ids_groups_string;
	
	<%
	if(AD=3) then
	%>
	ids_ous.value=ids_ous_string;
	ids_computers.value=ids_computers_string;
	<%
	end if
	%>

	return true;
}

tabType="users";

function check_tab(type)
{
	var list1 = document.getElementById("list1");

	if(type=="groups"){
		list1.src="policy_get_groups.asp";
		tabType="groups";
	}
	if(type=="users"){
		list1.src="policy_get_users.asp";
		tabType="users";
	}
	if(type=="ous"){
		list1.src="policy_get_ous.asp";
		tabType="ous";
	}
	if(type=="computers"){
		list1.src="policy_get_computers.asp";
		tabType="computers";
	}
}

function checkNewId(newId)
{
	var list2 = document.getElementById("list2");
	for (i=list2.options.length - 1; i>=0; i--){
		if (list2.options[i].value == newId){
			return false;
		}
	}
	return true;
}

function getBeforeItem()
{
	var list2 = document.getElementById("list2");
	for (i=list2.options.length - 1; i>=0; i--)
	{
		if(tabType=="groups"){
		//if ad - ous

		if (list2.options[i].value == "ous"){
				//return list2.options[i];
				return i;
			}
		}
		if(tabType=="ous"){
			if (list2.options[i].value == "computers"){
				//return list2.options[i];
				return i;
			}
		}
		if(tabType=="computers"){
			if (list2.options[i].value == "users"){
				//return list2.options[i];
				return i;
			}
		}
		
		if (list2.options[i].value == "users"){
			//return list2.options[i];
		    return i;
	    }
	}
	if(tabType=="users"){
	    if (list2.options[i].value == "users"){
		    //return list2.options[i];
			return i;
		}
	}
}

function list_add()
{
	var list1 = window.frames["list1"];
	var list2 = document.getElementById("list2");

	if(tabType=="ous"){

	res = list1.getCheckedOus();
	idSequence = "";
	if( res )
	{
		for(i=0;i<res.length;i++)
		{
			if(res[i].type != "organization" && res[i].type != "DOMAIN")
			{

				var newVal = res[i].name.toString();
				var newId = tabType+"_"+res[i].id.toString();
				if(checkNewId(newId)){
			       	    	var newRow = new Option(newVal,newId);
					if(tabType != "users"){
						var beforeItem = getBeforeItem();
						var elOptOld = list2.options[beforeItem];
						try {
							list2.add(newRow, elOptOld); // standards compliant; doesn't work in IE
						}
						catch(ex) {
							list2.add(newRow, beforeItem); // IE only
						}
					}
					else{
				            	list2.options[list2.length]=newRow;
					}
				}
			}
		}
	}
	}
	else
	{
//other
	var items_ids = list1.document.getElementsByName("users");
	var items_values = list1.document.getElementsByName("users_name");

	for(var i=0; i < items_ids.length; i++){
		if(jQuery(items_ids[i]).is(":checked")){
			var newVal = items_values[i].value;
			var newId = tabType+"_"+items_ids[i].value;
			if(checkNewId(newId)){
				var newRow = new Option(newVal,newId);
				if(tabType != "users"){
					var beforeItem = getBeforeItem();
					var elOptOld = list2.options[beforeItem];
					try {
						list2.add(newRow, elOptOld); // standards compliant; doesn't work in IE
					}
					catch(ex) {
						list2.add(newRow, beforeItem); // IE only
					}
				}
				else{
					list2.options[list2.length]=newRow;
				}
			}
		}
	}
	}
}

function list_remove()
{
	var list2 = document.getElementById("list2");
	for (i=list2.options.length - 1; i>=0; i--)
	{
		if (list2.options[i].selected == true)
		{
			list2.options[i]=null;
		}
	}
}

</script>
</head>
<body style="margin:0" class="<% if Request("wh") <> "1" then response.write "body" %>">
<% if Request("wh") <> "1" then %>
<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	<tr>
	<td>
	<table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
		<tr>
		<td width=100% height=31 class="main_table_title"><a href="group_users.asp" class="header_title"><%=LNG_GROUPS %></a></td>
		</tr>
		<tr>
		<td class="main_table_body" height="100%">
<% end if %>
		<div style="margin:10px;">
		<span class="work_header"><%=LNG_EDIT_GROUP_MEMBERS %></span>
		<br><br>
<div><%=LNG_STEP %> <strong>2</strong> <%=LNG_OF %> 2: <%=LNG_SELECT_MEMBERS %>:</div>

<%
	Set rs = Conn.Execute("SELECT id, name, type, special_id FROM groups WHERE id="+Request("id"))
	id=rs("id")
	name=rs("name")
	if(Not IsNull(rs("type"))) then mytype=rs("type") else mytype="O" end if
	if(Not IsNull(rs("special_id"))) then special_id=rs("special_id") else special_id="" end if

	rs.Close
%>
<br>
<form id="frm" name="frm" action="change_users_groups.asp" method="POST">
<input type="hidden" name="domain_id" value="<%=Request("domain_id")%>">

<div name="added_users" id="added_users">
	<table>
		<tr valign="middle" align="center">
			<td align="left">
				<div id="tabs" style="">
					<ul>
						<li><a href="#recipientsTab" onclick="check_tab('users');"><%=LNG_USERS%></a></li>
						<li><a href="#recipientsTab" onclick="check_tab('groups');"><%=LNG_GROUPS%></a></li>
						<%if (AD=3) then%>
						<li><a href="#recipientsTab" onclick="check_tab('ous');"><%=LNG_OUS%></a></li>
						<li><a href="#recipientsTab" onclick="check_tab('computers');"><%=LNG_COMPUTERS%></a></li>
						<%end if%>
					</ul>
					<div id="recipientsTab">
						<iframe frameborder="0" id="list1" name="list1" class="list_iframe" src="policy_get_users.asp"></iframe>
					</div>
				</div>
			
			</td>
			<td>
				<%=LNG_ADD %><br><A href="#" onclick="javascript: list_add();"><img src="images/list_add.png" border="0" /></a><br><A href="#" onclick="javascript: list_remove();"><img src="images/list_remove.png" border="0" /></a><br><%=LNG_REMOVE %>
			</td>
			<td>
				<b><%=LNG_RECIPIENTS %></b><br><br>
				<select multiple id="list2" name="list2" class="list_select">
					<option disabled value="groups"><%=LNG_LIST_OF_SELECTED_GROUPS %>:</option>
					<%
					addGroups(id)
					if(AD=3) then
					%>
					<option disabled value="ous"><%=LNG_LIST_OF_SELECTED_OUS %>:</option>
					<%
						addOUs(id)
					%>
					<option disabled value="computers"><%=LNG_LIST_OF_SELECTED_COMPUTERS %>:</option>
					<%
						addComputers(id)
					end if
					%>
					<option disabled value="users"><%=LNG_LIST_OF_SELECTED_USERS %>:</option>
					<%
					addUsers(id)
					%>
				</select>
			</td>
		</tr>
	</table>
	
	<div align="right"><button type="submit" class="cancel_button" onclick="javascript:history.go(-1); return false;"><%=LNG_CANCEL %></button><button class="save_button" type="submit" onclick="return my_sub(1);" ><%=LNG_SAVE %></button></div>
	
	<input type="hidden" name="chk_max" value=" <% Response.Write i %>"/>
	<input type="hidden" name="group_id" value="<% Response.Write id %>"/>
	
	<input type="hidden" name="ids_users" id="ids_users" value="">
	<input type="hidden" name="ids_groups" id="ids_groups" value="">
	<%
	if(AD=3) then
	%>
	<input type="hidden" name="ids_ous" id="ids_ous" value="">
	<input type="hidden" name="ids_computers" id="ids_computers" value="">
	<%
	end if
	%>
	<input type='hidden' name='return_page' value='<%=HtmlEncode(retPage) %>'/>
</form>
		 <img src="images/inv.gif" alt="" width="1" height="100%" border="0">
		</div>
<% if Request("wh") <> "1" then %>
		</td>
		</tr>
	</table>
	</td>
	</tr>
</table>
<% end if
else

	Response.Redirect "index.asp"

end if
%> 
</body>
</html>
<!-- #include file="db_conn_close.asp" -->