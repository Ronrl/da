﻿using System;
using System.Collections.Generic;
using System.Linq;
using DeskAlerts.Server.Utils;
using DeskAlerts.Server.Utils.Helpers;

namespace DeskAlertsDotNet.Pages
{
    public partial class RescheduleAction : DeskAlertsBasePage
    {
        //use curUser to access to current user of DA control panel      
        //use dbMgr to access to database

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            string toDateStr = "01.01.1900 00:00:00";
            string fromDateStr = "01.01.1900 00:00:00";
            var alertFields = new Dictionary<string, object>();

            if (!string.IsNullOrEmpty(Request["schedulePanel$start_date_and_time"]))
            {
                fromDateStr = DateTimeConverter.ConvertFromUiToDb(Request["schedulePanel$start_date_and_time"]);
            }

            if (!string.IsNullOrEmpty(Request["schedulePanel$end_date_and_time"]))
            {
                toDateStr = DateTimeConverter.ConvertFromUiToDb(Request["schedulePanel$end_date_and_time"]);
            }


            if (!string.IsNullOrEmpty(Request["schedulePanel$pattern"]))
            {

                if (!string.IsNullOrEmpty(Request["schedulePanel$start_date_rec"]) && !string.IsNullOrEmpty(Request["schedulePanel$start_time"]))
                {
                    fromDateStr = DateTimeConverter.ConvertFromUiToDb($"{Request["schedulePanel$start_date_rec"]} {Request["schedulePanel$start_time"]}");
                }

                if (!string.IsNullOrEmpty(Request["schedulePanel$end_date_rec"]) && !string.IsNullOrEmpty(Request["schedulePanel$end_time"]))
                {
                    toDateStr = DateTimeConverter.ConvertFromUiToDb($"{Request["schedulePanel$end_date_rec"]} {Request["schedulePanel$end_time"]}");
                }
                else if (!string.IsNullOrEmpty(Request["schedulePanel$end_time"]))
                {
                    var dt = new DateTime(1900, 1, 1);
                    toDateStr = DateTimeConverter.ConvertFromUiToDb($"{DateTimeConverter.ToUiDate(dt)} {Request["schedulePanel$end_time"]}");
                }
        
                alertFields.Add("lifetime", 0);
            }

            alertFields.Add("from_date", fromDateStr);
            alertFields.Add("to_date", toDateStr);

            dbMgr.Update("alerts", alertFields, new Dictionary<string, object>() { {"id", Request["alertId"]} });

            Dictionary<string, object> recurDictionary = new Dictionary<string, object>();

            int weekDaysMask = 0;

            string[] exceptCollumns =
            {
                    "start_date_and_time", "from_date", "end_date_and_time", "start_date_rec",
                    "end_date_rec", "start_time", "end_time"
                };
            foreach (var key in Request.Params.AllKeys)
            {
                string value = Request.Params[key];

                if (key.StartsWith("schedulePanel$weekDayPatternCheckBoxList$"))
                {
                    int numberWeekDay = Convert.ToInt32(key.Replace("schedulePanel$weekDayPatternCheckBoxList$", ""));
                    weekDaysMask = setBitByNumber(weekDaysMask, numberWeekDay);
                }
                else if (key.StartsWith("schedulePanel$"))
                {
                    string newKey = key.Replace("schedulePanel$", "");

                    if (Array.IndexOf(exceptCollumns, newKey) == -1)
                        recurDictionary.Add(newKey, value);
                }
            }

            recurDictionary.Add("week_days", weekDaysMask);
            
            recurDictionary.Add("alert_id", Request["alertId"]);

            if (!string.IsNullOrEmpty(Request["countdowns"]))
            {
                string[] countdownsArray = Request["countdowns"].Split(',');

                if (countdownsArray.Length > 0)
                    recurDictionary.Add("countdowns", countdownsArray.ToList());
            }
            else
            {
                recurDictionary.Add("countdowns", new List<string>());
            }
            ReccurenceManager.ProcessReccurence(dbMgr, recurDictionary);
            Response.Write("<b>Alert was successfully rescheduled.</b>");
        }

        private static int setBitByNumber(int weekDaysMask, int numberWeekDay)
        {
            weekDaysMask |= 1 << numberWeekDay;
            return weekDaysMask;
        }
    }
}