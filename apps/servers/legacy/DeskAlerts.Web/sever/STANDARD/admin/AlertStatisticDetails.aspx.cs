﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Enums;
using DeskAlerts.ApplicationCore.Repositories;
using DeskAlerts.ApplicationCore.Utilities;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Parameters;
using DeskAlerts.Server.Utils;
using DeskAlerts.Server.Utils.Interfactes;
using DeskAlerts.Server.Utils.Services;
using Resources;

namespace DeskAlertsDotNet.Pages
{
    public partial class AlertStatisticDetails : DeskAlertsBasePage
    {
        //use curUser to access to current user of DA control panel      
        //use dbMgr to access to database
        protected string alertsColors = "";
        protected string alertsValues = "";
        protected string alertId;
        protected string type2;
        protected bool isAlertRecipientsIpGroups;
        protected bool isAlertRecipientsBroadcast;
        protected bool isAlertRecipientsSelection;
        protected const string totalBroadcastRecipientsQuery = "SELECT DISTINCT name FROM users WHERE role = 'U' AND (client_version != 'web client' OR client_version IS NULL)";
        private const string totalBroadcastRecipientsQueryExceptMobile = "SELECT distinct name FROM users WHERE role = 'U' AND (client_version != 'web client' OR client_version IS NULL)";
        private const string selectAllUsers = "SELECT count(1) as mycnt FROM users WHERE role = 'U' AND (client_version != 'web client' OR client_version IS NULL)";
        private const string selectAllUsersExceptMobile = "SELECT count(1) as mycnt FROM users WHERE role = 'U' AND (client_version != 'web client' OR client_version IS NULL)";

        private readonly IContentService _contentService;

        public AlertStatisticDetails()
        {
            var configurationManager = new WebConfigConfigurationManager(Config.Configuration);

            var alertReadRepository = new PpAlertReadRepository(configurationManager);
            var alertRepository = new PpAlertRepository(configurationManager);
            var alertReceivedRepository = new PpAlertReceivedRepository(configurationManager);
            var multipleAlertsRepository = new PpMultipleAlertRepository(configurationManager);
            var tickerRepository = new PpTickerRepository(configurationManager);
            var rsvpRepository = new PpRsvpRepository(configurationManager, alertRepository);
            var surveyRepository = new PpSurveyRepository(configurationManager, alertRepository);
            var videoAlertRepository = new PpVideoAlertRepository(configurationManager, alertRepository);
            var wallPaperRepository = new PpWallPaperRepository(configurationManager);
            var screenSaverRepository = new PpScreenSaverRepository(configurationManager);
            var lockScreenRepository = new PpLockScreenRepository(configurationManager);
            var scheduledContentRepository = new PpScheduledContentRepository(configurationManager);

            _contentService = new ContentService(
                alertRepository,
                alertReceivedRepository,
                alertReadRepository,
                multipleAlertsRepository,
                tickerRepository,
                rsvpRepository,
                surveyRepository,
                videoAlertRepository,
                wallPaperRepository,
                screenSaverRepository,
                lockScreenRepository,
                scheduledContentRepository);
        }

        protected override void OnLoad(EventArgs e)
        {
            if (string.IsNullOrEmpty(Request["id"]))
            {
                Response.End();
                return;
            }
            base.OnLoad(e);
            alertId = Request["id"];
            var alertIdL = Convert.ToInt64(alertId);
            DataRow alertRow = dbMgr.GetDataByQuery("SELECT * FROM alerts WHERE id = " + alertId).First();
            var alertType = (AlertType)Enum.Parse(typeof(AlertType), alertRow.GetString("class"));
            var totalUsernames = "";
            int totalRecipientsCount = 0;
            var recievedUsernames = "";
            int recievedRecipientsCount = 0;
            var notRecievedUsernames = "";
            int notRecievedRecipientsCount = 0;
            var aknowlegementUsernames = "";
            int acknowledgedCount = 0;

            type2 = alertRow.GetString("type2");
            isAlertRecipientsBroadcast = type2 == "B";
            isAlertRecipientsSelection = type2 == "R";
            isAlertRecipientsIpGroups = type2 == "I";

            var totalSelectedRecipientsQuery = "";
            var isMultiple = _contentService.CheckForMultipleAlert(alertIdL);

            if (isAlertRecipientsBroadcast)
            {
                //Mobile devices can receive only alerts or surveys
                if (IsAlertOrSurvey(alertType))
                {
                    totalUsernames = GetRecipientsNamesString(totalBroadcastRecipientsQuery);
                    totalRecipientsCount = dbMgr.GetScalarQuery<int>(selectAllUsers);

                }
                else
                {
                    totalUsernames = GetRecipientsNamesString(totalBroadcastRecipientsQueryExceptMobile);
                    totalRecipientsCount = dbMgr.GetScalarQuery<int>(selectAllUsersExceptMobile);
                }
            }
            else if (isAlertRecipientsSelection)
            {
                totalSelectedRecipientsQuery = "SELECT distinct name FROM [alerts_sent_stat] INNER JOIN users ON [alerts_sent_stat].user_id = users.id WHERE alerts_sent_stat.alert_id= " + alertId;
                totalSelectedRecipientsQuery += " UNION ALL ";
                totalSelectedRecipientsQuery += "SELECT distinct name FROM [archive_alerts_sent_stat] INNER JOIN users ON [archive_alerts_sent_stat].user_id = users.id WHERE archive_alerts_sent_stat.alert_id=" + alertId;
                totalSelectedRecipientsQuery += " UNION ALL ";
                totalSelectedRecipientsQuery += "SELECT  distinct name FROM [alerts_sent_comp] INNER JOIN computers ON [alerts_sent_comp].comp_id = computers.id WHERE alerts_sent_comp.alert_id= " + alertId;
                totalSelectedRecipientsQuery += " UNION ALL ";
                totalSelectedRecipientsQuery += $"SELECT distinct name FROM [archive_alerts_sent_comp] INNER JOIN computers ON [archive_alerts_sent_comp].comp_id = computers.id WHERE archive_alerts_sent_comp.alert_id={alertId}";

                totalUsernames = GetRecipientsNamesString(totalSelectedRecipientsQuery);

                string query = $@"SELECT SUM(mycnt) 
                                    FROM (
	                                    SELECT COUNT(DISTINCT user_id) as mycnt
	                                    FROM [alerts_sent_stat] 
	                                    INNER JOIN users ON users.id=alerts_sent_stat.user_id 
	                                    WHERE [alerts_sent_stat].alert_id= {alertId} 
	                                    UNION ALL 
		                                    SELECT COUNT(DISTINCT user_id) as mycnt
			                                    FROM [archive_alerts_sent_stat] WHERE alert_id={alertId}
			                                    UNION ALL  
				                                    SELECT COUNT(DISTINCT comp_id) as mycnt
				                                    FROM [alerts_sent_comp] WHERE alert_id= {alertId}
				                                    UNION ALL 
					                                    SELECT COUNT(DISTINCT comp_id) as mycnt
					                                    FROM [archive_alerts_sent_comp] 
					                                    WHERE alert_id={alertId} ) [count]";

                totalRecipientsCount = dbMgr.GetScalarQuery<int>(query);
            }
            else if (isAlertRecipientsIpGroups)
            {
                var query = $@"
                    SELECT COUNT(DISTINCT user_id)
                    FROM alerts_received 
                    WHERE alert_id = {alertId}";

                totalRecipientsCount = dbMgr.GetScalarQuery<int>(query);
            }

            var recievedRecipientsQuery = $@"UNION ALL SELECT distinct name 
                                                  FROM[archive_alerts_sent_comp] INNER JOIN computers ON[archive_alerts_sent_comp].comp_id = computers.id
                                                    JOIN alerts_received ON alerts_received.alert_id = { alertId}
            WHERE archive_alerts_sent_comp.alert_id = { alertId}";

            var receivedRecipientsNamesQuery = $@"
                SELECT DISTINCT(users.name) 
                FROM users 
                INNER JOIN alerts_received ON users.id = alerts_received.user_id 
                WHERE alerts_received.alert_id= {alertId}
                    AND alerts_received.status = {(int)AlertReceiveStatus.Received}";

            recievedUsernames = GetRecipientsNamesString(receivedRecipientsNamesQuery);

            //Calculate recieved users count
            string recievedUsersCount = $@"SELECT SUM(distinct mycnt) FROM (
                    SELECT COUNT(DISTINCT users.id) as mycnt, 1 as mycnt1 
                        FROM users 
                        INNER JOIN alerts_received ON users.id = alerts_received.user_id 
                        WHERE role='U' AND alerts_received.status = {(int) AlertReceiveStatus.Received} AND alerts_received.alert_id = {alertId} 
                    UNION ALL 
                    SELECT COUNT(DISTINCT user_id) as mycnt, 2 as mycnt1 
                        FROM users 
                        INNER JOIN archive_alerts_received ON users.id = archive_alerts_received.user_id 
                        WHERE role='U' AND archive_alerts_received.alert_id = {alertId}
                ) [count]";
            recievedRecipientsCount = dbMgr.GetScalarQuery<int>(recievedUsersCount);

            DataSet surveySet = dbMgr.GetDataByQuery("SELECT distinct id FROM surveys_main WHERE closed = 'A' and sender_id = " + alertId);
            int votedCnt = 0;
            if (!surveySet.IsEmpty)
            {
                string votedQuery = "SELECT COUNT(DISTINCT stats.user_id) as mycnt FROM surveys_answers_stat stats, surveys_answers answers, surveys_questions questions WHERE stats.answer_id=answers.id AND answers.question_id=questions.id AND questions.survey_id=" +
                      surveySet.GetString(0, "id");

                votedCnt = dbMgr.GetScalarQuery<int>(votedQuery);
            }
            else
            {
                if (isMultiple)
                {
                    var confirmedUsernames = _contentService
                        .GetConfirmedUsernamesForMultipleAlert(alertIdL);

                    aknowlegementUsernames = string.Join(",", confirmedUsernames);
                    acknowledgedCount = _contentService.GetConfirmedUsersForMultipleAlertAmount(alertIdL);
                }
                else
                {
                    var aknowlegementUsernamesQuery = $"SELECT distinct name FROM [alerts_read] INNER JOIN users ON [alerts_read].user_id = users.id WHERE alerts_read.alert_id = {alertId}";
                    aknowlegementUsernames = GetRecipientsNamesString(aknowlegementUsernamesQuery);

                    var acknownQuery = $"SELECT COUNT(DISTINCT user_id) as mycnt FROM [alerts_read] WHERE alert_id = {alertId}";
                    acknowledgedCount = dbMgr.GetScalarQuery<int>(acknownQuery);
                }
            }

            var notRecievedUsernamesQuery = String.Empty;
            if (isAlertRecipientsBroadcast)
            {
                notRecievedUsernamesQuery = $@"SELECT DISTINCT users.id as id, users.name as name, users.display_name, users.domain_id as domain_id FROM users
                                        LEFT JOIN (SELECT user_id FROM alerts_received 
                                            WHERE alert_id={alertId}
                                            AND alerts_received.status = {(int)AlertReceiveStatus.Received}
                                        UNION
                                        SELECT user_id FROM archive_alerts_received WHERE alert_id={alertId}
                                        ) r ON  r.user_id=users.id 
                                        WHERE role='U' AND r.user_id IS NULL
                                        AND (client_version != 'web client' OR client_version IS NULL)";
            }
            else
            {
                notRecievedUsernamesQuery = $@"SELECT DISTINCT * FROM 
                                               (SELECT users.id as id, users.name as name, users.display_name, users.domain_id as domain_id 
                                                FROM users 
                                                    INNER JOIN alerts_sent_stat ON users.id=alerts_sent_stat.user_id 
                                                    WHERE role='U' AND users.id NOT IN (SELECT user_id FROM alerts_received WHERE alert_id={alertId} AND alerts_received.status = {(int)AlertReceiveStatus.Received}) 
                                                        AND alerts_sent_stat.alert_id={alertId}
                                                UNION ALL  
                                                SELECT users.id as id, users.name as name, users.display_name, users.domain_id as domain_id 
                                                FROM users 
                                                    INNER JOIN archive_alerts_sent_stat ON users.id=archive_alerts_sent_stat.user_id 
                                                    WHERE role='U' 
                                                            AND users.id NOT IN (SELECT user_id FROM  archive_alerts_received WHERE alert_id={alertId}) 
                                                            AND archive_alerts_sent_stat.alert_id={alertId} ) t";
            }

            notRecievedUsernames = GetRecipientsNamesString(notRecievedUsernamesQuery);

            notRecievedRecipientsCount = totalRecipientsCount - recievedRecipientsCount;

            string recievedColor = "'#3B6392'";
            string notRecievedColor = "'#9ABA59'";
            string aknownColor = "'#A44441'";
            string valuesJsonTemplate = "[{0}]";
            string recievedKey = resources.LNG_RECEIVED;
            string notRecievedKey = resources.LNG_NOT_RECEIVED;
            string aknownKey = resources.LNG_ACKNOWLEDGED;
            string votedKey = resources.LNG_VOTED;

            firstLegend.Style.Add(HtmlTextWriterStyle.BackgroundColor, recievedColor.Replace("'", ""));
            firstLegendLabel.InnerText = recievedKey;

            if (recievedRecipientsCount == 0)
                firstLegendValue.InnerHtml = "0";
            else
            {
                string link = string.Format("<a href='AlertViewRecipients.aspx?type={0}&id={1}' title=''  class='prelink_stats'>{2}</a>",
                   "rec", alertId, recievedRecipientsCount);
                firstLegendValue.InnerHtml = Config.Data.HasModule(Modules.STATISTICS) ? link : recievedRecipientsCount.ToString();
            }

            alertsValues += string.Format(valuesJsonTemplate, recievedRecipientsCount);
            alertsColors = recievedColor;
            secondLegend.Style.Add(HtmlTextWriterStyle.BackgroundColor, aknownColor.Replace("'", ""));
            secondLegendLabel.InnerText = aknownKey;

            if (acknowledgedCount == 0)
                secondLegendValue.InnerHtml = "0";
            else
            {
                var link = isMultiple
                    ? $"<a href='statistics_da_alerts_view_multiple_alerts.asp?type=ack&id={alertId}' title='' class='prelink_stats'>{acknowledgedCount}</a>"
                    : $"<a href='AlertViewRecipients.aspx?type=ack&id={alertId}' title='' class='prelink_stats'>{acknowledgedCount}</a>";
                
                secondLegendValue.InnerHtml = Config.Data.HasModule(Modules.STATISTICS) ? link : acknowledgedCount.ToString();
            }

            if (alertsValues.Length > 0)
                alertsValues += ",";

            alertsValues += string.Format(valuesJsonTemplate, acknowledgedCount);
            alertsColors += ",";

            alertsColors += aknownColor;
            thirdLegend.Style.Add(HtmlTextWriterStyle.BackgroundColor, notRecievedColor.Replace("'", ""));
            thirdLegendLabel.InnerText = notRecievedKey;
            if (notRecievedRecipientsCount == 0)
                thirdLegendValue.InnerHtml = "0";
            else
            {
                string link =
                    $"<a href=\'AlertViewRecipients.aspx?type=drec&id={alertId}\' title=\'\' class=\'prelink_stats\'>{notRecievedRecipientsCount}</a>";

                thirdLegendValue.InnerHtml = Config.Data.HasModule(Modules.STATISTICS) ? link : notRecievedRecipientsCount.ToString();
            }

            if (alertsValues.Length > 0)
                alertsValues += ",";

            alertsValues += string.Format(valuesJsonTemplate, notRecievedRecipientsCount);
            if (alertsColors.Length > 0)
                alertsColors += ",";

            alertsColors += notRecievedColor;
            totalRecipients.Text = isAlertRecipientsIpGroups 
                ? string.Empty
                : Config.Data.HasModule(Modules.STATISTICS) 
                    ? string.Format("<a class='prelink_stats' title='' href='AlertRecipientsList.aspx?id={0}'>{1}</a>", alertId, totalRecipientsCount) 
                    : totalRecipientsCount.ToString();

            string[] stopStartKeys = { "LNG_START", "LNG_STOP" };
            int scheduleType = alertRow.GetInt("schedule_type");
            string stopStartText = resources.ResourceManager.GetString(stopStartKeys[scheduleType]);

            var headers = new []
            {
                "Alert title",
                "Sent date",
                "Total",
                "Total usernames",
                "Received",
                "Received usernames",
                "Acknowledged",
                "Acknowledged usernames",
                "Not received",
                "Not received usernames"
            };

            var values = new []
            {
                DeskAlertsUtils.ExtractTextFromHtml(alertRow.GetString("title")),
                alertRow.GetString("sent_date"),
                totalRecipientsCount.ToString(),
                totalUsernames,
                recievedRecipientsCount.ToString(),
                recievedUsernames,
                acknowledgedCount.ToString(),
                aknowlegementUsernames,
                notRecievedRecipientsCount.ToString(),
                notRecievedUsernames
            };

            var csvLink = SpreedsheetManager.WriteCsvToUploadsFromDataRow("alert" + alertId, headers, new List<string[]>{ values });
            var xlsLink = SpreedsheetManager.WriteXlsxToUploads("alert" + alertId, new []{ headers, values });

            csvDownloadLink.Attributes["href"] = csvLink;
            xlsDownloadLink.Attributes["href"] = xlsLink;

            csvDownloadLink.Visible = Config.Data.HasModule(Modules.STATISTICS);
            xlsDownloadLink.Visible = Config.Data.HasModule(Modules.STATISTICS);
        }
        /// <summary>
        /// Returns string of recipients usernames comma + space separeted
        /// </summary>
        /// <param name="SqlQuery"></param>
        /// <returns></returns>
        private string GetRecipientsNamesString(string SqlQuery)
        {
            string result = "";

            var recipientsList = dbMgr.GetDataByQuery(SqlQuery);

            foreach (var recipientRow in recipientsList)
            {
                result += recipientRow.GetString("name") + ",";
            }

            result = result.TrimEnd(',');

            return result;
        }
        /// <summary>
        /// Return true if it is alert or survey type
        /// </summary>
        /// <param name="alertType">type of alert</param>
        /// <returns></returns>
        private bool IsAlertOrSurvey(AlertType alertType)
        {
            return alertType == AlertType.SimpleAlert ||
                alertType == AlertType.SimpleSurvey ||
                alertType == AlertType.SurveyPoll ||
                alertType == AlertType.SurveyQuiz;
        }
    }
}