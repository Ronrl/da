﻿<%
Response.CodePage = 65001
Response.CharSet = "utf-8"

Dim strMessage, strFolder
Dim httpref, lngFileSize
Dim strExcludes, strIncludes

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!-- #include file="config.inc" -->
<!-- #include file="md5.asp" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%
if (foldersufix = "_video") then 
    video_input = 1
end if
if not isEmpty(foldersufix) then
	strFolder=strFolder & foldersufix
	httpRef=httpRef & foldersufix
end if

if isEmpty(form_title) then
	form_title = "Images"
end if
if isEmpty(form_size) then
	form_size = 0
end if
if isEmpty(form_recording) then
	form_recording = 0
end if

  uid = Session("uid")
if(uid<>"") then

	'-----------------------------------------------
	'This is the complete upload file program.
	'This is intended to upload graphics onto the web and
	'to delete them if required.
	'Set up the configurations below to define which
	'directory to use etc, then set the permissions on
	'the directory to 'Change' i.e. Read/Write
	'-----------------------------------------------

	
	strMessage = Request.QueryString ("msg")
	
'--------------------------------------------
Const dictKey  = 1
Const dictItem = 2
Const orderASC = 1
Const orderDESC = 2

Sub main()

	%>
	<html>
	<head>
	<title><%=form_title %></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<link href="images/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
	<link href="css/style9.css" rel="stylesheet" type="text/css">
	<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
	
	<script language="javascript" type="text/javascript" src="jscripts/date.js"></script>
	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
	
	<style>
		input.text { margin-bottom:12px; width:95%; padding: .4em; }
		h1 { font-size: 1.2em; margin: .6em 0; }	
		.ui-dialog .ui-state-error { padding: .3em; }
	</style>
	<script language="javascript" type="text/javascript">

		var itemsPerPage = 10;
		var curPageNum = 0;
		var pageCount = 0;
		var showItems;
		var insertFileWithSize;
		
		function getItemById(id)
		{
			for (var i=0; i< showItems.length; i++)
			{
				if (showItems[i].id == id)
				{
					return showItems[i];
				}
			}
			return null;
		}
		
		function removeItemById(collection,id)
		{
			for (var i=0; i< collection.length; i++)
			{
				if (collection[i].id == id)
				{
					collection.splice(i,1);
				}
			}
			return;
		}
		
		function insertFile(name, file_path, width, height)
		{
			if(window.opener)
					window.opener.fileBrowserSetSrc('<%=Request("id") %>', name, file_path, width, height);
			window.close();
		}
		
		function selectFile(name, file_path)
		{
			$("#insert_width").val("320")
			$("#insert_height").val("240")
			
			insertFileWithSize = function(width,height) {
				
				insertFile(name, file_path, $("#insert_width").val(), $("#insert_height").val())
			}
			
			<% if form_size = 1 then %>	
				$("#dialog-size").dialog("open");
			<% else %>	
				insertFile(name, file_path);	
			<% end if %>
		}
		
		var onConfirmDialog;
		function deleteFile(file_id)
		{
			var item = getItemById(file_id);
			var dataString = "action=confirm_delete" +
				"&file_path=" +  encodeURIComponent(item.relativePath);
			$.ajax({
				type: 'POST',
				url: 'uploaded_files_actions.asp',
				data: dataString,
				success: function(data) {
				
					onConfirmDialog = function() {
						var dataString = "action=delete" +
						"&file_path=" +  encodeURIComponent(item.relativePath);
						$.ajax({
							type: 'POST',
							url: 'uploaded_files_actions.asp',
							data: dataString,
							success: function(data) {	
								removeItemById(showItems,file_id);
								removeItemById(uploadedFiles,file_id);
								pageCount = Math.ceil(showItems.length/itemsPerPage);
								if (curPageNum == pageCount)
								{
									curPageNum--;
								}
								showPage(showItems,curPageNum);
								updateValues();
							},
							async: true
						});
					}
					$("#confirmation_text").text(data);
					$("#dialog-confirm").dialog("open");
				},
				async: false
			});
		}
		
		function sortFilesArray(a,b,field,order)
		{
			var a = a[field];
			var b = b[field];
			return (((a < b) && order=="asc" || (a > b) && order=="desc")? -1 : (((a > b) && order=="asc" || (a < b) && order=="desc") ? 1 : 0));
		}
		
		function createFileItem(item)
		{
			var title = '';
			var shortname = item.name;
			if(shortname.length > 30)
			{
				shortname = shortname.substr(0, 30) + '...';
				title = item.name;
		    }
			var fileItemTr = $("<tr file_id='"+item.id+"' style='width:100%'/>");
			var fileItemNameTd = $("<td/>").appendTo(fileItemTr);
			var fileItemNameA = $("<a style='text-decoration:underline' href='#'/>").appendTo(fileItemNameTd);
			fileItemNameA.text(shortname);
			if(title) fileItemNameA.attr('title', title);
			fileItemNameA.click(function(){
				selectFile(item.name,/*item.relativePath*/'<%=httpref%>/'+item.name);
			});
			fileItemTr.append("<td style='width:60px' align='center'>"+
								"<a href='<%=httpref%>/"+encodeURIComponent(item.name)+"' target='_blank' style='margin:2px'><img src='images/action_icons/preview.png' border='0'/></a>"+
								"<a href='javascript:deleteFile(\""+item.id+"\")' style='margin:2px' ><img src='images/action_icons/delete.gif' border='0'/></a>");
			return fileItemTr;
		}
		
		function showPage(filesArray,num)
		{
			$("#filesDiv").empty();
			var itemsCount = filesArray.length;
			var startPos = itemsPerPage*num;
			
			for (var i=0; i<itemsPerPage; i++)
			{
				var position = startPos+i;
				if (position == itemsCount) return;
				var item = filesArray[position];	
				var $fileItem = $(createFileItem(item));
				$("#filesDiv").append($fileItem);
			}
		}
		
		function updateValues()
		{
			$("#pages_count").text(pageCount);
			$("#page_input").val(curPageNum+1);
		}
		
		function showNextPage()
		{
			if (curPageNum < pageCount-1)  
			{
				curPageNum++
				showPage(showItems,curPageNum);
				updateValues();
			}
		}
		
		function showPreviousPage()
		{
			if (curPageNum > 0)  
			{
				curPageNum--;
				showPage(showItems,curPageNum);
				updateValues();
			}
		}
		
		
		function searchFiles(searchTerm)
		{
			if (searchTerm == "")
			{
				showItems = uploadedFiles;
			}
			else
			{
				showItems = [];
				
				for (var i = 0; i < uploadedFiles.length; i++)
				{
					if (uploadedFiles[i].name.toLowerCase().indexOf(searchTerm.toLowerCase()) >= 0)
					{
						showItems.push(uploadedFiles[i]);
					}
				}
				showItems.sort(function(a,b){ return sortFilesArray(a,b,"dateCreated","desc")});
			}
			pageCount = Math.ceil(showItems.length/itemsPerPage);
			curPageNum = 0;
			showPage(showItems,curPageNum);
			updateValues();
		}
			
		$(function(){
		
			$( "#dialog-confirm" ).dialog({
				resizable: false,
				height:140,
				modal: true,
				autoOpen: false,
				buttons: {
					"<%=LNG_YES%>": function(){
						onConfirmDialog();
						$( this ).dialog( "close" );
					},
					"<%=LNG_NO%>": function() {
						$( this ).dialog( "close" );
					}
				}
			});
			
			$( "#dialog-size" ).dialog({
				resizable: false,
				height: 165,
				modal: true,
				autoOpen: false,
				title: "<%=LNG_SIZE%>",
				buttons: {
					"<%=LNG_SAVE%>": function(){
						insertFileWithSize();
					}
				}
			});

		
			$("#previous_button").button().click(function(){showPreviousPage()});
			$("#next_button").button().click(function(){showNextPage()});
			$("#search_button").button().click(function(){searchFiles($("#search_input").val())});
			pageCount = Math.ceil(showItems.length/itemsPerPage);
			curPageNum = 0;
			showPage(showItems,curPageNum);
			updateValues();
		});
	</script>
	</head>
	<body style="margin:0" class="body">
		<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
			<tr>
			<td>
			<table width="100%" height="100%" cellspacing="0" cellpadding="0" class="">

				<tr>
				<td height="100%" valign="top" class="main_table_body">
					<div style="padding:10px">
	<%

	if Request.Form("AskDelete") = "Delete" then	'ask if to delete
		call askDelete(Request.Form("fileId"))
	elseif Request.Form("delete") = "" then			'display at start up
		call displayform()
		call BuildFileList(strFolder)
	elseif Request.Form ("delete") = "Yes" then		'make deletion
		call delete(Request.form("fileId"))
	
		call displayForm()
		call BuildFileList(strFolder)
	elseif Request.Form ("delete") = "No" then		'do not make deletion
		call displayForm()
		call BuildFileList(strFolder)
	end if

	%>
				 <img src="images/inv.gif" alt="" width="1" border="0">
				</div>
				</td>
				</tr>
			</table>
			</td>
			</tr>
		</table>
	</body>
	</html>
	<%

end sub


'--------------------------------------------
'Displays the form to allow uploading
Sub displayForm()

Dim i, tempArray

	'Results box
	if strMessage <> "" then
	%>
	<table border="0" align="center" cellspacing="0" cellpadding="2" class="data_table">
	<tr>
		<td class="text"><%=HtmlDecode(strMessage)%></td>
	</tr>
	</table>
	<%
	end if


	%>
	<table border="0" align="center" cellspacing="0" cellpadding="2">
	<tr>
		<td class="text">
		<%
		if lngFileSize > 0 then 
			Response.Write ("Maximum size of each file = ") & lngFileSize & " bytes"
		end if
		%>	
		</td>
	</tr>
	</table>

	<form action="uploadfile<%=foldersufix%>.asp" method="post" enctype="multipart/form-data" encoding="utf-8" accept-charset="utf-8">
		<input type="hidden" name="return" value="<%
			path = Request.ServerVariables("SCRIPT_NAME")
			pos = InStrRev(path, "/")
			Response.Write Mid(path, pos+1)
		%>"/>
		<input type="hidden" name="id" value="<%= Request("id") %>"/>
		<input type="hidden" name="can_select" value="1"/>

		<table border="0" align="center" cellspacing="0" cellpadding="2">
		<tr>
			<td colspan="2" class="text">Select the file to upload</td>
		</tr>
		<tr>
			<td colspan="2" class="text"><b>File: </b></td>
		</tr>
		<tr>
			<td class="text">
                
                <% if video_input = 1  then %>
				<input type="file" accept="video/*" title =" " name="file1" >
                <% else %>
                <input type="file" accept="image/*" title =" " name="file1" style="color:transparent;">
                <% end if %>
			</td>
			<td align="center">
				<input type="submit" value="Upload" name="submit">
			</td>
		</tr>
<% if form_recording = 1 then %>
		<tr>
			<td class="text" align="right" style="padding-top:5px">
				To record your own video:
			</td>
			<td align="center" style="padding-top:5px">
				<input type="button" value="Record" onclick="window.open('video_recorder.asp','','width=340,height=440,scrollbars=no');">
			</td>
		</tr>
<% end if %>
		</table>
	</form>
<%
end sub


'--------------------------------------------
'Builds a list of files on the directory
'INPUT : the folder to be used
Sub BuildFileList(strFolder)

	Dim oFS, oFolder, intNoOfFiles, FileName

   ' Response.Write strFolder

	Set oFS = Server.CreateObject("Scripting.FileSystemObject")


	Set oFolder = oFS.getFolder(strFolder)
	%>
	<script language="javascript" type="text/javascript">
		
		uploadedFiles = [
	<%
	intNoOfFiles = 0

	
	Set filesDictionary = Server.CreateObject("Scripting.Dictionary")
	
	pathinfo = Server.MapPath(Request.ServerVariables("PATH_INFO"))
	For Each FileName in oFolder.Files	
		if (FileName.Name <> "DeskAlertsAPI.exe" AND FileName.Name <> "send.bat") then
			if (intNoOfFiles <> 0 ) then Response.Write "," end if
			%>
				{ 	
					name:"<%=jsEncode(FileName.Name)%>",
					id:"<%=md5(FileName.Path)%>",
					relativePath:"<%=jsEncode(Replace(Replace(FileName.ParentFolder.Path,oFS.GetParentFolderName(pathinfo) & "\","",1,1,1),"\","/") & "/" & FileName.Name)%>",
					dateModified:new Date(getDateFromFormat("<%=FormatDateTime(FileName.DateLastModified)%>","dd/MM/yyyy HH:mm:ss")),
					dateCreated:new Date(getDateFromFormat("<%=FormatDateTime(FileName.DateCreated)%>","dd/MM/yyyy HH:mm:ss")),
					size: <%=FileName.Size%>
				}
			<%
			intNoOfFiles = intNoOfFiles + 1
		end if
	Next
	
	Set oFolder = nothing
	%>
	];
	showItems = uploadedFiles;
	showItems.sort(function(a,b){ return sortFilesArray(a,b,"dateCreated","desc")});
	</script>
	<br/>
	<table border="0" align="center" width="100%" cellspacing="0" cellpadding="2">
	<% if (Request("edit") <> "true" ) then%>
	<tr>
		<td class="text" colspan="2">List of files on the web:</td>
	</tr>
	<% end if %>
	<tr id="pagination_row" align="right">
		<td colspan="2" class="text"><input id="search_input" type="text" style="width:150px; margin-left:5px; margin-right:5px"><a id="search_button"><%=LNG_SEARCH%></a></td>
	</tr>
	<tr>
		<td colspan="2">
			<table style="width:100%" class="data_table">
				<thead>
					<tr class="data_table_title">
						<td class="data_table_title">Filename</td>
						<td class="data_table_title" style="width:60px">Actions</td>
					</tr>
				</thead>
				<tbody id="filesDiv">
				</tbody>
			</table>
		</td>
	</tr>
	
	
   <%
	if intNoOfFiles = 0 then
		%>
		<tr align="center">
			<td colspan="2" class="text">No files available</td>
		</tr>		
		<%
	else
		%>
		<tr id="pagination_row" align="right">
			<td colspan="2" class="text"><a id="previous_button"><%=LNG_BACK%></a><input id="page_input" type="text" style="width:50px; margin-left:5px; margin-right:5px">of <label style="margin-left:5px; margin-right:5px" id="pages_count"></label><a id="next_button"><%=LNG_NEXT%></a></td>
		</tr>
		<%
	
	
	end if
  
	%>
	</table>
	<div id="dialog-confirm" title="">
		<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 10px 0;"></span><font id="confirmation_text"></font></p>
	</div>
	<div id="dialog-size" title="">
			<table style="margin:0 auto;" align="center">
				<tr>
					<td style="text-align:right">
						<%=LNG_WIDTH%>
					</td>
					<td>
						<input style="width:50px" id="insert_width" type="text" value=""></input>
					</td>
				</tr>
				<tr>
					<td style="text-align:right">
						<%=LNG_HEIGHT%>
					</td>
					<td>
						<input style="width:50px" id="insert_height" type="text" value=""></input>
					</td>
				</tr>
			</table>
	</div>
	<%    
   
End Sub

Sub Terra()
%>
<link href="images/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
	<link href="css/style9.css" rel="stylesheet" type="text/css">
	<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
	
	<script language="javascript" type="text/javascript" src="jscripts/date.js"></script>
	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<style>
		input.text { margin-bottom:12px; width:95%; padding: .4em; }
		h1 { font-size: 1.2em; margin: .6em 0; }	
		.ui-dialog .ui-state-error { padding: .3em; }
	</style>
	<script language="javascript" type="text/javascript">

		var itemsPerPage = 10;
		var curPageNum = 0;
		var pageCount = 0;
		var showItems;
		var insertFileWithSize;
		
		function getItemById(id)
		{
			for (var i=0; i< showItems.length; i++)
			{
				if (showItems[i].id == id)
				{
					return showItems[i];
				}
			}
			return null;
		}
		
		function removeItemById(collection,id)
		{
			for (var i=0; i< collection.length; i++)
			{
				if (collection[i].id == id)
				{
					collection.splice(i,1);
				}
			}
			return;
		}
		
		function insertFile(name, file_path, width, height)
		{
			if(window.opener){
			    alert(1);
					window.opener.fileBrowserSetSrc('<%=Request("id") %>', name, file_path, width, height);}
			window.close();
		}
		
		function selectFile(name, file_path)
		{
			$("#insert_width").val("320")
			$("#insert_height").val("240")
			
			insertFileWithSize = function(width,height) {
				
				insertFile(name, file_path, $("#insert_width").val(), $("#insert_height").val())
			}
			
			<% if form_size = 1 then %>	
				$("#dialog-size").dialog("open");
			<% else %>	
				insertFile(name, file_path);	
			<% end if %>
		}
		
		var onConfirmDialog;
		function deleteFile(file_id)
		{
			var item = getItemById(file_id);
			var dataString = "action=confirm_delete" +
				"&file_path=" +  encodeURIComponent(item.relativePath);
			$.ajax({
				type: 'POST',
				url: 'uploaded_files_actions.asp',
				data: dataString,
				success: function(data) {
				
					onConfirmDialog = function() {
						var dataString = "action=delete" +
						"&file_path=" +  encodeURIComponent(item.relativePath);
						$.ajax({
							type: 'POST',
							url: 'uploaded_files_actions.asp',
							data: dataString,
							success: function(data) {	
								removeItemById(showItems,file_id);
								removeItemById(uploadedFiles,file_id);
								pageCount = Math.ceil(showItems.length/itemsPerPage);
								if (curPageNum == pageCount)
								{
									curPageNum--;
								}
								showPage(showItems,curPageNum);
								updateValues();
							},
							async: true
						});
					}
					$("#confirmation_text").text(data);
					$("#dialog-confirm").dialog("open");
				},
				async: false
			});
		}
		
		function sortFilesArray(a,b,field,order)
		{
			var a = a[field];
			var b = b[field];
			return (((a < b) && order=="asc" || (a > b) && order=="desc")? -1 : (((a > b) && order=="asc" || (a < b) && order=="desc") ? 1 : 0));
		}
		
		function createFileItem(item)
		{
			var title = '';
			var shortname = item.name;
			if(shortname.length > 30)
			{
				shortname = shortname.substr(0, 30) + '...';
				title = item.name;
		}
			var fileItemTr = $("<tr file_id='"+item.id+"' style='width:100%'/>");
			var fileItemNameTd = $("<td/>").appendTo(fileItemTr);
			var fileItemNameA = ""			
		    <% if(Request("edit")<>"true") then %>
			fileItemNameA = $("<a style='text-decoration:underline' href='#'/>").appendTo(fileItemNameTd);
			<% else  %>
			fileItemNameA = $("<a class='files_uploaded' style='text-decoration:underline' href='<%=httpref%>/"+encodeURIComponent(item.name)+"'/>").appendTo(fileItemNameTd);
			<% end if %>
			fileItemNameA.text(shortname);
			if(title) fileItemNameA.attr('title', title);
			//fileItemNameA.click(function(){
			//	selectFile(item.name,/*item.relativePath*/'<%=httpref%>/'+item.name);
			//});
			fileItemTr.append("<td style='width:60px' align='center'>"+
								"<a href='<%=httpref%>/"+encodeURIComponent(item.name)+"' target='_blank' style='margin:2px'><img src='images/action_icons/preview.png' border='0'/></a>"+
								"<a href='javascript:deleteFile(\""+item.id+"\")' style='margin:2px' ><img src='images/action_icons/delete.png' border='0'/></a>");
			return fileItemTr;
		}
		
		function showPage(filesArray,num)
		{
			$("#filesDiv").empty();
			var itemsCount = filesArray.length;
			var startPos = itemsPerPage*num;
			
			for (var i=0; i<itemsPerPage; i++)
			{
				var position = startPos+i;
				if (position == itemsCount) return;
				var item = filesArray[position];	
				var $fileItem = $(createFileItem(item));
				$("#filesDiv").append($fileItem);
			}
		}
		
		function updateValues()
		{
			$("#pages_count").text(pageCount);
			$("#page_input").val(curPageNum+1);
		}
		
		function showNextPage()
		{
			if (curPageNum < pageCount-1)  
			{
				curPageNum++
				showPage(showItems,curPageNum);
				updateValues();
			}
		}
		
		function showPreviousPage()
		{
			if (curPageNum > 0)  
			{
				curPageNum--;
				showPage(showItems,curPageNum);
				updateValues();
			}
		}
		
		
		function searchFiles(searchTerm)
		{
			if (searchTerm == "")
			{
				showItems = uploadedFiles;
			}
			else
			{
				showItems = [];
				
				for (var i = 0; i < uploadedFiles.length; i++)
				{
					if (uploadedFiles[i].name.toLowerCase().indexOf(searchTerm.toLowerCase()) >= 0)
					{
						showItems.push(uploadedFiles[i]);
					}
				}
				showItems.sort(function(a,b){ return sortFilesArray(a,b,"dateCreated","desc")});
			}
			pageCount = Math.ceil(showItems.length/itemsPerPage);
			curPageNum = 0;
			showPage(showItems,curPageNum);
			updateValues();
		}
			
		$(function(){
		
			$( "#dialog-confirm" ).dialog({
				resizable: false,
				height:140,
				modal: true,
				autoOpen: false,
				buttons: {
					"<%=LNG_YES%>": function(){
						onConfirmDialog();
						$( this ).dialog( "close" );
					},
					"<%=LNG_NO%>": function() {
						$( this ).dialog( "close" );
					}
				}
			});
			
			$( "#dialog-size" ).dialog({
				resizable: false,
				height: 165,
				modal: true,
				autoOpen: false,
				title: "<%=LNG_SIZE%>",
				buttons: {
					"<%=LNG_SAVE%>": function(){
						insertFileWithSize();
					}
				}
			});

		
			$("#previous_button").button().click(function(){showPreviousPage()});
			$("#next_button").button().click(function(){showNextPage()});
			$("#search_button").button().click(function(){searchFiles($("#search_input").val())});
			pageCount = Math.ceil(showItems.length/itemsPerPage);
			curPageNum = 0;
			showPage(showItems,curPageNum);
			updateValues();
		});
	</script>
	<%
End Sub


'--------------------------------------------
edit = Request("edit")
if (edit <> "true") then
call main()
else 
call Terra()
call BuildFileList(strFolder)
end if

else
	Response.write "You are not authorised to view this page!"
end if
%>
<!-- #include file="db_conn_close.asp" -->