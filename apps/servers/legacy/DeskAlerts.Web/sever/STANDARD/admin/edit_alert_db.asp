﻿<!-- #include file="config.inc" -->
<!-- #include file="demo.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<!-- #include file="recurrence_form.asp" -->
<!-- #include file="recurrence_process.asp" -->
<%
check_session()
uid = Session("uid")
mobile_device=Session("isMobile")
'default recurrence values----
pattern = "o" 'd,w,m,y
dayly_selector = "dayly_1"
monthly_selector = "monthly_1"
yearly_selector = "yearly_1"
countdown_enable = 0

number_days = 1 '(every 3 days), 0=every weekday
number_weeks = 1 '(every 5 week)
week_days = 0 ' mon, wed, fri
month_day = 1 '27e chislo)
number_month = 1 '(every 4 month)
weekday_place = 1 '(1,2,3,4), 0=last
weekday_day = 1 'mon, tue, wed, thu, fri, sat, sun 
month_val = 1'(1-12)

end_type = "no_date" '(no_date, end_after, end_by)
occurences = 1 '(number)
'-------------------

alert_width = ConfAlertWidth
alert_height = ConfAlertHeight
top_template = Request("top_template")
recurrence = Request("recurrence")
bottom_template = Request("bottom_template")
if Request("instant")="1" then
instant_message = 1
else
instant_message = 0
end if
if Request("autosubmit")="1" then
autosubmit = 1
else
autosubmit = 0
end if

Function MyMod(ByVal a, ByVal b)
	a = Fix(CDbl(a))
	b = Fix(CDbl(b))
	MyMod = a - Fix(a/b) * b
End Function

function addAlertToDataBase(alertText, alertTitle, fromDate, toDate, schedule, scheduleType, recurrence, urgent, email, desktop, sms, emailSender, ticker, fullscreen, alertWidth, alertHeight, aknown, autoclose, toolbarmode, deskalertsmode, template, uid, skin_id, alertClass, resizable, blogPost, socialMedia, self_deletable, text_to_call, color_code, campaign_id, approve_status, has_lifetime, device_type)
   
    'Response.Write "APPROVEeee = " & approve_status
	if(linkedin<>"checked") then 
	    sqlRequest = "INSERT INTO alerts (alert_text, title, create_date, type, from_date, to_date, schedule, schedule_type, recurrence, urgent, email, desktop, sms, email_sender, ticker, fullscreen, alert_width,  alert_height, aknown,  autoclose,  toolbarmode, deskalertsmode, template_id, sender_id, caption_id, class, resizable, post_to_blog, social_media, self_deletable, text_to_call, color_code, campaign_id, approve_status, lifetime,device) OUTPUT INSERTED.ID as newID VALUES (N'" + alertText + "', N'"&alertTitle&"', GETDATE(), 'D', '"&fromDate&"','"&toDate&"','"&schedule&"', '"&scheduleType&"', '"&recurrence&"', '"&urgent&"', '"&email&"', '"&desktop&"', '"&sms&"', N'"&emailSender&"',  '"&ticker&"', '"&fullscreen&"', "&alertWidth&",  "&alertHeight&", '"&aknown&"', "&autoclose&", '"&toolbarmode&"', '"&deskalertsmode&"',"&template&", "&uid&", "&skin_id&", "&alertClass&", "&resizable&", "&blogPost&", "&socialMedia&", "&self_deletable&", "&text_to_call&", '"&color_code&"', " & campaign_id & "," & approve_status &"," & has_lifetime &","& device_type &")"
	else
	    sqlRequest = "INSERT INTO alerts (alert_text, title, create_date, type,sent_date, from_date, to_date, schedule, schedule_type, recurrence, urgent, email, desktop, sms, email_sender, ticker, fullscreen, alert_width,  alert_height, aknown,  autoclose,  toolbarmode, deskalertsmode, template_id, sender_id, caption_id, class, resizable, post_to_blog, social_media, self_deletable, text_to_call, campaign_id, approve_status, lifetime,device) OUTPUT INSERTED.ID as newID VALUES (N'" + alertText + "', N'"&alertTitle&"', GETDATE(), 'L', GETDATE(),'"&fromDate&"', '"&toDate&"','"&schedule&"', '"&scheduleType&"', '"&recurrence&"', '"&urgent&"', '"&email&"', '"&desktop&"', '"&sms&"', N'"&emailSender&"',  '"&ticker&"', '"&fullscreen&"', "&alertWidth&",  "&alertHeight&", '"&aknown&"', "&autoclose&", '"&toolbarmode&"', '"&deskalertsmode&"',"&template&", "&uid&", "&skin_id&", "&alertClass&", "&resizable&", "&blogPost&", "&socialMedia&", "&self_deletable&", "&text_to_call&", " & campaign_id & "," & approve_status & "," & has_lifetime &"," & device_type &")"
	end if
	'Response.Write sqlRequest
	Conn.Execute(sqlRequest)
	Set rsNewId = Conn.Execute(sqlRequest)
	addAlertToDataBase=rsNewId("newID")
	rsNewId.Close
end function

if uid <> "" then
	alerts_arr = Array ("","","","","","","","")
	emails_arr = Array ("","","","","","","","")
	sms_arr = Array ("","","","","","","","")
	templates_arr = Array ("","","","","","","","")
	text_templates_arr = Array ("","","","","","","","")
	text_to_call_arr = Array ("","","","","","","","")
	im_arr = Array ("","","","","","","")
	EnableAlertLangs = ConfEnableAlertLangs
	If EnableAlertLangs=1 Then
		EnableAlertLangs = 0
		Set RS_langs = Conn.Execute("SELECT COUNT(1) as cnt FROM alerts_langs")
		If Not RS_langs.EOF Then
			If RS_langs("cnt") > 1 Then
				EnableAlertLangs = 1
			End If
		End If
	End If
	
	SQL = "SELECT guide, dis_guide FROM users WHERE id="&uid
	Set RSGuide = Conn.Execute(SQL)
	if not RSGuide.EOF then
		if isNull(RSGuide("guide")) then
			showGuide = "none"
		else
			showGuide = RSGuide("guide")
		end if
		if isNull(RSGuide("dis_guide")) then
			guideHistory = true
		else
			if InStr(RSGuide("dis_guide"),"alertcreate")>0 then
				guideHistory = false
			else
				guideHistory = true
			end if
		end if
		disabledGuides = RSGuide("dis_guide")
	end if

	Set RS = Conn.Execute("SELECT id FROM users WHERE id="&uid&" AND role='E'")
	if(Not RS.EOF) then
		gid = 1
		Set rs_policy = Conn.Execute("SELECT type FROM policy INNER JOIN policy_editor ON policy.id=policy_editor.policy_id WHERE editor_id=" & uid)
		if(Not rs_policy.EOF) then
			if(rs_policy("type")="A" or rs_policy("type")="M")   then
				gid = 0
			end if
		end if
		if(gid=1) then
			editor_id=RS("id")
			policy_ids=""
			policy_ids=editorGetPolicyIds(editor_id)
			alerts_arr = editorGetPolicyList(policy_ids, "alerts_val")
			emails_arr = editorGetPolicyList(policy_ids, "emails_val")
			sms_arr = editorGetPolicyList(policy_ids, "sms_val")
			templates_arr = editorGetPolicyList(policy_ids, "templates_val")
			text_templates_arr = editorGetPolicyList(policy_ids, "text_templates_val")
			text_to_call_arr = editorGetPolicyList(policy_ids, "text_to_call_val")

			im_arr = editorGetPolicyList(policy_ids, "im_val")
			if instant_message = 1 OR autosubmit = 1 then
				alerts_arr(0)="1"
				alerts_arr(3)="1"
				alerts_arr(4)="1"
			end if
			text_to_call_arr = editorGetPolicyList(policy_ids, "text_to_call_val")
		end if
	else
		gid = 0
	end if
	RS.Close
	
	
%>
<!DOCTYPE HTML>
<html>
<head>
    <title>DeskAlerts</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="css/style9.css" rel="stylesheet" type="text/css" />
    <style>
        .checkbox_div
        {
            padding: 1px;
        }
    </style>

    <script language="javascript" type="text/javascript" src="functions.js"></script>

    <!-- TinyMCE -->

    <script language="javascript" type="text/javascript" src="jscripts/tiny_mce/tinymce.min.js"></script>


    <!-- /TinyMCE -->

    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>


    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/json2.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/preview.js?v=9.2.1.31"></script>

    <script language="javascript" type="text/javascript" src="jscripts/json2.js"></script>

    <script language="javascript" type="text/javascript">
        $(document).ready(function() {

            $(function() {
                $(".translate_btn").button();
                $(".cancel_button").button();
                $(".save_button").button();
                <% if YAMMER_CFG=1 then%>
    			$("#yammerShare").button();
                <% end if %>
                isDigSign = parseInt('<% =Request("webplugin") %>');

                if (isDigSign != 1) {
                    $("#accordion").accordion({
                        active: 0,
                        heightStyle: "content",
                        collapsible: true
                    });
                }
            });
        });

		
    </script>

    <% if SOCIAL_LINKEDIN=1 then 
		Set RSLinkedIn = Conn.Execute("SELECT field1 FROM social_media WHERE type=2")
		if (Not RSLinkedIn.EOF) then
			APIKey=RSLinkedIn("field1")
			
			
    %>

    <script type="text/javascript" src="//platform.linkedin.com/in.js">
			api_key: <%=APIKey %>
			onLoad: onLinkedInLoad
			authorize: true
    </script>

    <script type="text/javascript" language="javascript">

        function onLinkedInLoad() {
            IN.Event.on(IN, "auth", function() { onLinkedInLogin(); });
            IN.Event.on(IN, "logout", function() { onLinkedInLogout(); });
            $("#linkedin_settings_hint").hide();
        }

        function onLinkedInLogout() {
            $("#shareAPI").hide();
        }

        function onLinkedInLogin() {
            IN.Event.on("shareAPI", "click", doSharingTest);
            $("#shareAPI").show();
        }

        function doSharingTest() {
            var id = $("input[name='id']").first().val();
            if (id) {
                var shareDesc = $("input[name='alert_text']").first().val();

                var tmp = document.createElement("DIV");
                tmp.innerHTML = shareDesc;
                shareDesc = tmp.textContent || tmp.innerText || "";
                var shareTitle = $("input[name='alert_title']").first().val();
                var folder = $("input[name='alerts_folder']").first().val();
                var shareUrl = folder + "preview.asp?id=" + id;
                IN.API.Raw("/people/~/shares")
				    .method("POST")
				    .body(JSON.stringify({
				        "content": {
				            "submitted-url": shareUrl,
				            "title": shareTitle,
				            "description": shareDesc
				        },
				        "visibility": {
				            "code": "anyone"
				        }/*,
				    "comment": "This is a test posting from the DeskAlerts Control Panel"*/
				    })
				    )
				.result(function(r) {
				    showPromptRedir("<%=LNG_SHARE_SUCCESSFUL %>");
				})
				.error(function(r) {
				    showPromptRedir("<%=LNG_SHARE_FAILED %> " + r.message);
				});

            }
            else {
                if (my_sub(4)) {
                    check_tab('linkedin');
                    document.my_form.submit();
                }
            }
        }

    </script>

    <% else %>

    <script type="text/javascript">
        $("#linkedInDiv").hide();
        $("#linkedin_settings_hint").show();
    </script>

    <% end if
		end if %>

    <script type="text/javascript" language="javascript">
        var newTinySettings = {<%= newTinySettings()%>};
        var skinId;
        var confShowSkinsDialog = <%=ConfShowSkinsDialog %>;
        var skins = [
		<%
			Set RS = Conn.Execute("SELECT id, name FROM alert_captions")
			skins = ""
			with_alert_captions = 0
			Do While Not RS.EOF
				if skins <> "" then
        skins = skins + ","
        else
					with_alert_captions = 1
        end if
        skins = skins + "{ id:""" & RS("id") & """, name:""" & RS("name") & """}"
        RS.MoveNext
        Loop
        RS.Close
        Response.Write skins
    %>
        ];
		
        function initTemplateEditor(callback)
        {
            
            if (!tinyMCE.getInstanceById('top_template_area'))
            {

                alert(newTinySettings);
                var $topTemlateArea = $("<br/><b><%= LNG_TOP_TEMPLATE %>:</b><br/><br/><textarea name='top_template' id='top_template_area' cols='30' style='width: 100%'></textarea><br/>")
                $("#top_template_div").append($topTemlateArea);
                initTinyMCE("<%=lcase(default_lng) %>", "exact", "top_template_area", function(ed){ if(callback) callback(ed.id); },null,newTinySettings);
            }
            if (!tinyMCE.getInstanceById('bottom_template_area'))
            {
                var $bottomTemlateArea = $("<br/><b><%= LNG_BOTTOM_TEMPLATE %>:</b><br/><br/><textarea name='bottom_template' id='bottom_template_area' cols='30' style='width: 100%'></textarea>")
                $("#bottom_template_div").append($bottomTemlateArea);
                initTinyMCE("<%=lcase(default_lng) %>", "exact", "bottom_template_area", function(ed){ if(callback) callback(ed.id); },null,newTinySettings);
            }
            if (tinyMCE.getInstanceById('bottom_template_area') && tinyMCE.getInstanceById('top_template_area'))
            {
                if (callback)
                {
                    callback("top_template_area");
                    callback("bottom_template_area");
                }
            }
        }
				
        function showAlertPreview(type)
        {
            if (!type) type = "desktop";
            //alert();
            var data = new Object();
			
            data.create_date = "<%=al_create_date%>";
		
            //var fullscreen = ($("#size2").prop("checked") == "checked") ? "1" : $("input[name='position']:checked").attr("value");
            var ticker = ($("#alert_type_ticker").prop("checked")) ? 1 : 0;
            var fullscreen = "9";
            if ($("#size2").prop("checked")) { fullscreen = "1"}
            else if (ticker == 0){ fullscreen = $("input[name='position']:checked").attr("value");}
            else { fullscreen = $("input[name='ticker_position']:checked").attr("value");}
            var acknowledgement = ($("#aknown").prop("checked")) ? 1 : 0; 
            var self_deletable = ($("#self_deletable").prop("checked")) ? 1 : 0;
            var alert_width = $("#al_width").val();
            var alert_height = $("#al_height").val();
            var print_alert = ($("#print_alert").prop("checked")) ? 1 : 0;
            var alert_html = tinyMCE.get('elm_0').getContent();
		   
            <% if ConfEnableHtmlAlertTitle = 1 then %>
                var html_title_ed = tinyMCE.get("html_title_0");
            var html_title = html_title_ed.getContent();
            if(html_title)
                alert_html += "<!-- html_title = \"" + html_title.replace(/&/g, '&amp;').replace(/"/g, '&quot;').replace(/'/g, '&#39;').replace(/</g, '&lt;').replace(/>/g, '&gt;') + "\" -->";
            if (ticker==1){
                <% if (TICKER_PRO=1) then%>
					if(alert_html.indexOf("<-- ticker_pro -->")==-1)
                alert_html +="<!-- ticker_pro -->";
                <% end if %>
                }
            if(print_alert==1){
                if(alert_html.indexOf("<-- printable_alert -->")==-1)
                    alert_html +="<!-- printable_alert -->";
            }
            else {
                if(alert_html.match(/<!-- printable_alert -->/img))
                    alert_html = alert_html.replace(/<!-- printable_alert -->/img,""); 
            }        
			           
            var alert_title = $('<div/>').html(html_title).text();
            <% else %>
                var alert_title = $("#title_0").val();
            <% end if %>

                var top_template;
            var bottom_template;
            var templateId = $("#template_select").val();
			
            if ($("#top_template_area").length > 0)
            {
                top_template = tinyMCE.get('top_template_area').getContent();
            }
            else
            {
                getTemplateHTML(templateId,"top",false,function(data){ 
                    top_template = data;
                });
            }
			
            if ($("#bottom_template_area").length > 0)
            {
                bottom_template = tinyMCE.get('bottom_template_area').getContent();
            }
            else
            {
                getTemplateHTML(templateId,"bottom",false,function(data){ 
                    bottom_template = data;
                });
            }
            data.top_template = top_template;
            data.bottom_template = bottom_template;
            data.alert_width = alert_width;
            data.alert_height = alert_height;
            data.ticker = ticker;
            data.fullscreen = fullscreen;
            data.acknowledgement = acknowledgement;
            data.self_deletable = self_deletable;
            data.alert_title = alert_title;
            data.alert_html = alert_html;
            data.caption_href=$("#skin_id").attr('filename');
            data.skin_id=$("#skin_id").val();
            data.type = type;
            data.need_question = type != 'sms' && $("#alert_type_rsvp").is(':checked');
            data.question = $("#question").val();
            data.question_option1 = $("#question_option1").val();
            data.question_option2 = $("#question_option2").val();
            data.need_second_question = $("#need_second_question").is(':checked');
            data.second_question = $("#second_question").val();
            initAlertPreview(data);
			
            return false;
        }
		
		
        var topTemplate;
        var bottomTemplate;
        var titleChanged = {};
        var contentChanged = {};
        function alertContentOnChangeHandler(ed) {
            contentChanged[ed.id] = true;
        }
		
        function createSkinsTilesHtml()
        {
            var thumbnail;
            if($("#alert_type_ticker").prop("checked")){
                thumbnail = "thumbnail_tick.png";
            }
            else{
                thumbnail = "thumbnail.png";
            }
            var list = $('<ul style="list-style:none; margin:0px; padding:0px">'+
							'<li class="tile" style="float:left;  margin:10px; padding:10px">'+
								'<div style="padding-bottom:5px"><span autofocus style="margin:0px" id="header_title" class="header_title"><%=LNG_DEFAULT%></span></div>'+
								'<div>'+
									'<img src="skins/default/'+thumbnail+'"/>'+
								'</div>'+
							'</li>'+
						'</ul>');
	
            for (var i=0; i < skins.length; i++)
            {
                var li = '<li id="'+skins[i].id+'" class="tile" style="float:left; margin:10px; padding:10px">'+
							'<div style="padding-bottom:5px"><span style="margin:0px" class="header_title">'+skins[i].name+'</span></div>'+
							'<div>'+
								'<img src="skins/'+skins[i].id+'/'+thumbnail+'"/>'+
							'</div>'+
						'</li>';
                $(list).append(li);
            }
			
            $(list).find(".tile").hover(function(){
                $(this).switchClass("","tile_hovered",200);
            }, function(){
                $(this).switchClass("tile_hovered","",200);
            }).click(function(){
                $("#skins_tiles_dialog").dialog("close");
                skinId = $(this).attr("id");
                $("#skin_id").val(skinId);
                if (!skinId)
                {
                    skinId = "default";
                }
								
                var imgSrc = "skins/"+skinId+"/"+thumbnail;
                $("#skin_preview_img").attr("src",imgSrc);
                if(skinId=='{C28F6977-3254-418C-B96E-BDA43CE94FF4}' || skinId=='{138F9281-0C11-4C48-983E-D4CA8076748B}' || skinId=='{04F84380-B31D-4167-8B83-C6642E3D25A8}')
                {
                    document.getElementById('resizable').disabled=true;
                    document.getElementById('resizable').checked=false;
                    document.getElementById('size2').checked=false;
                    document.getElementById('size2').disabled=true;
                    document.getElementById('size1').checked=false;
                    document.getElementById('size1').disabled=true;
                    document.getElementById('al_width').disabled=true;
                    document.getElementById('al_height').disabled=true;
                    document.getElementById('al_width').value="500";
                    document.getElementById('al_height').value="400";

                    //document.getElementById('caption_frame').style.backgroundColor=transparent;
                }
                else
                {
                    document.getElementById('al_width').disabled=false;
                    document.getElementById('al_height').disabled=false;
                    document.getElementById('resizable').disabled=false;
                    document.getElementById('size2').disabled=false;
                    document.getElementById('size1').disabled=false;
                    //document.getElementById('caption_frame').style.backgroundColor="white";
                }
            })
			
            return list;
        }
		
        function showSkinsTilesDialog()
        {
            if (skins.length > 0){
                $("#skins_tiles_dialog > p").empty();
                $("#skins_tiles_dialog > p").append(createSkinsTilesHtml());
                $("#skins_tiles_dialog").dialog({
                    position: {my: "left top", at: "left top", of: "#dialog_dock"},
                    modal: true,
                    resizable: false,
                    draggable: false,
                    width: 'auto',
                    resize: 'auto',
                    margin:'auto',
                    close: function(){ closeSkinsDialog() }
                });
            }
            else{
                $("#skins_tiles_dialog").hide();
            }
			
        }
		
        function closeTypesDialog()
        {
            if($('#magic_checkbox').prop('checked'))
            {
                $.ajax({
                    url: 'set_settings.asp',
                    method: 'GET',
                    data: 'name=ConfShowTypesDialog&value=0'
                });
            }
            <% if confShowSkinsDialog = 1 then
            Response.Write("showSkinsTilesDialog();")
            end if%>
            }
		
        function closeSkinsDialog()
        {
            if($('#skins_checkbox').prop('checked'))
            {
                $.ajax({
                    url: 'set_settings.asp',
                    method: 'GET',
                    data: 'name=ConfShowSkinsDialog&value=0'
                });
            }
        }
		
        function showColorCodeDialog()
        {
            $("#color_code_dialog").dialog({
                position: {my: "left top", at: "left top", of: "#dialog_dock"},
                modal: true,
                resizable: false,
                draggable: false,
                width: 'auto',
                resize: 'auto',
                margin:'auto'
				<% if confShowSkinsDialog = 1 then
					Response.Write(",close: function(){showSkinsTilesDialog()}")
            end if
             %>
            });
        }
		
        function darkOrLight(color) {
            red = parseInt(color.substring(0,2),16);
            green = parseInt(color.substring(3,5),16);
            blue = parseInt(color.substring(5),16);
            var brightness;
            brightness = (red * 299) + (green * 587) + (blue * 114);
            brightness = brightness / 255000;

            // values range from 0 to 1
            // anything greater than 0.5 should be bright enough for dark text
            if (brightness >= 0.5) {
                return "black";
            } else {
                return "white";
            }
        }
		
		
        function hideAppearance()
        {
            $("#tableAlertAppear").hide(); 	
            $("#position_div").parent().hide();
            $("#desktopCheckLabel").hide();
            $("#urgent_div").hide();		   
            $("#aknown_div").hide();
            $("#unobtrusive_div").hide();
            $("#self_deletable_div").hide();		   
            $("#aut_close_div").hide();
            $("#print_div").hide();		    
        }
		 
        function showAppearance()
        {
            $("#tableAlertAppear").show();
            $("#position_div").parent().show();
            $("#desktopCheckLabel").show();
            $("#urgent_div").show();		   
            $("#aknown_div").show();
            $("#unobtrusive_div").show();
            $("#self_deletable_div").show();		   
            $("#aut_close_div").show();
            $("#print_div").show();             
        }
		
		
        function pickColorCode(color){
            $("#color_code").val(color);
            $("#change_color_code").css("background-color","#"+color);
            $("#change_color_code").css("color",darkOrLight(color));
            $("#color_code_dialog").dialog("close");
        }
		
        function selectAlertTypeAndClose(type){
            switch(type){
                case "alert":
                    $("#alert_type_tab").find("a").first().click();
                    break;
                case "ticker":
                    $("#ticker_type_tab").find("a").first().click();
                    break;
                case "rsvp":
                    $("#rsvp_type_tab").find("a").first().click();
                    break;
                default:
                    break;
            }
            $("#alert_type_dialog").dialog("close");
        }
		
        function showPrompt(text)
        {
            $("#dialog-modal > p").text(text);
            $("#dialog-modal").dialog({
                height: 180,
                modal: true,
                resizable: false,
                buttons: {
                    "<%=LNG_CLOSE %>": function() {
                        $(this).dialog("close");
                    }
                }
            });
        }
        function showPromptRedir(text)
        {
            $("#dialog-modal > p").text(text);
            $("#dialog-modal").dialog({
                height: 180,
                modal: true,
                resizable: false,
                buttons: {
                    "<%=LNG_CLOSE %>": function() {
                        $(this).dialog("close");
                        location.href = "index.asp";
                    }
                }
            });
        }
	
        $(document).ready(function() {
            if (<%=autosubmit %>==1){
				$("#im_description").dialog();
        }

            
		
			if ("<%=ConfShowTypesDialog %>"=="1" && "<%=instant_message %>"!="1" && $("input[name='id']").first().val()==undefined){
			    $("#alert_type_dialog").dialog({
			        position: {my: "left top", at: "left top", of: "#dialog_dock"},
			        modal: true,
			        resizable: false,
			        draggable: false,
			        width: 'auto',
			        resize: 'auto',
			        margin:'auto',
			        autoOpen:true,
			        close: function(){ closeTypesDialog() }
					
			    });
			}
			
        if(skins.length > 0)
        {
            $(".tile").hover(function(){
                $(this).switchClass("","tile_hovered",200);
            }, function(){
                $(this).switchClass("tile_hovered","",200);
            }).click(function(){
					
                showSkinsTilesDialog();
            })
        }
        else
        {
            $(".tile").hide();
        }
        $("#edit_template_button").button();
        $("#edit_template_button").bind("click", function()
        {
            $("#edit_template_button").hide();
            $("#customTemplate").show();
            var $customTemplate = $('<option id="customTemplate" value="0"></option>');
            $("#template_select").prepend($customTemplate);
            var templateId = $("#template_select").val();
            $.get("get_template.asp?temp_id="+templateId+"&type=top", function (topData)
            {
                $.get("get_template.asp?temp_id="+templateId+"&type=bottom", function (bottomData)
                {
                    initTemplateEditor(function(id)
                    {
                        if (id=="bottom_template_area")
                        {
                            tinyMCE.get('bottom_template_area').setContent(bottomData);
                        }
                        if (id=="top_template_area")
                        {
                            tinyMCE.get('top_template_area').setContent(topData);
                        }
                    });
                    $("#template_select").val("0");
                });
            });
        });
        <% if ConfEnableHtmlAlertTitle <> 1 then %>
			$("input[name=alert_title]").change(function()
			{
			    titleChanged[this.id] = true;
			});
        <% end if %>
			$("#template_select").change(function()
        {
				if($(this).val() > 0)
        {
					$("#edit_template_button").show();
					$("#customTemplate").remove();
					tinyMCE.execCommand('mceRemoveControl', false, 'bottom_template_area');
					$('#bottom_template_div').empty();
					tinyMCE.execCommand('mceRemoveControl', false, 'top_template_area');
					$('#top_template_div').empty();
        }
        });

        $("select[name=text_template_select]").change(function()
        {
            if($(this).val() > 0)
            {
                var i = (this.id+"").replace('text_template_select_', '');
                $.get("get_template.asp?temp_id="+$(this).val(), function(tempData)
                { 
                    var elm = tinyMCE.get('elm_'+i);
                    var elm_cont = elm.getContent();
                    if(!contentChanged['elm_'+i] || !elm_cont.replace(/ |<p.*?>&nbsp;<\/p>|<p><span.*?><br.*?><\/span><\/p>/ig,''))
                    {
                        elm.setContent(tempData);
                        contentChanged['elm_'+i] = false;
                    }
                    else
                    {
                        elm.setContent(elm_cont + tempData);
                    }
                });
                <% if ConfEnableHtmlAlertTitle = 1 then %>
					var title_ed = tinyMCE.get('html_title_'+i);
                var title = title_ed.getContent();
                if(!titleChanged['title_'+i] || !title)
                {
                    title_ed.setContent($(this).children("option:selected").text());
                    titleChanged['title_'+i] = false;
                }
                <% else %>
					if(!titleChanged['title_'+i] || !$('#title_'+i).val())
                {
                    $('#title_'+i).val($(this).children("option:selected").text());
                    titleChanged['title_'+i] = false;
                }
                <% end if %>
					$(this).val(0);
            }
        });
			
        if (topTemplate || bottomTemplate)
        {
			    
            initTemplateEditor(function(id)
            {
                if (id=="bottom_template_area")
                {
                    tinyMCE.get('bottom_template_area').setContent(bottomTemplate);
                }
                if (id=="top_template_area")
                {
                    tinyMCE.get('top_template_area').setContent(topTemplate);
                }
            });
        }
			

			
        $("#alert_type_ticker").click(check_alert_type);
        $("#alert_type_alert").click(check_alert_type);
        $("#alert_type_rsvp").click(check_alert_type);
        $("#alert_type_linkedin").click(check_alert_type);
        $("#alert_type_webplugin").click(check_alert_type);
        $(".preview_button").button();
		
        $(".preview_sms_button").button().click(function(){
            showAlertPreview("sms");
        });
        $(".save_button").button();
        $(".save_and_next_button").button({
            icons: {
                secondary: "ui-icon-carat-1-e"
            }
        });
			
        var ticker = parseInt('<%=Request("ticker") %>');
        $('#alert_type_tabs').tabs({active: $("#alert_type_webplugin").is(':checked') ? 4 : $("#alert_type_linkedin").is(':checked') ? 3 : $("#alert_type_rsvp").is(':checked') ? 2 : $("#alert_type_alert").is(':checked') && ticker != 1 ? 0 : 1}).css("border", "none");
			
        check_alert_type();
			
        if(ticker == 1)
        {		
            check_tab('ticker');
        }
			
        if(typeof patternChange == 'function')
        {
            var pattern = $("input[name=pattern]:checked").val();
            patternChange(pattern);
        }
        check_recurrence();
        check_lifetime();
        check_settings();
        if("<%=autosubmit %>"=="1"){
            setTimeout(function(){if(my_sub(3)){document.my_form.submit();}},1000); 
        }
        $("#guideDialog").dialog({
            autoOpen:false,
            resizable:false,
            draggable:false,
            modal:true,
            width:400,
            height:300,
            close: function(){closeGuide();}
        });
			
        function showGuide(){
            $("#guideDialog").dialog("open");
        }
			
        function closeGuide(){
            var disabledGuides = "<%=disabledGuides %>"
            disabledGuides += "alertcreate,";
            $.get("set_settings.asp",{name:"DisabledGuides", value: disabledGuides});
        }
			
        function checkHistoryGuide(){
            <% if guideHistory=true then %>
            showGuide();
            <% end if %>
            }
			
        <% if showGuide="new" then%>
            checkHistoryGuide();
        <% elseif showGuide="all" then %>
            showGuide();
        <% end if %>
        });
		
    $(window).load(function(){
        if ($("input[name='non_edit']").first().val()) {
            if($("#alert_type_linkedin").is(':checked'))
        {
                doSharingTest();
        }
        }
        <% if (ConfShowSkinsDialog = 1 and ConfShowTypesDialog=0 and instant_message=0) then%>
        if ((!$("input[name='id']").first().val())&&(!$("#alert_hidden").val())) 
        {   
            showSkinsTilesDialog();  
            setTimeout(function() { document.getElementById("skins_tiles_dialog").focus();}, 1500);
        }
        <% end if %>
        <% if instant_message = 1 then %>
            /*if ((!$("input[name='id']").first().val())&&(!$("#alert_hidden").val())) 
			{   
				showColorCodeDialog();  
				setTimeout(function() { document.getElementById("color_code_dialog").focus();}, 1500);
			}*/
        <%end if %>
        if (<%=instant_message %>==1){
            im_interface();
        }
        });
		
        function set_default_size()
        {
            document.getElementById('al_width').value = 340;
            document.getElementById('al_height').value = 290;
            document.getElementById('size_message').innerHTML = "";
        }

        var old_aknown = null;
        function check_alert_type()
        {
            var webplugin = parseInt('<%=Request("webplugin") %>');
            //  var ticker =   parseInt('<%=Request("ticker") %>');
            if($("#alert_type_rsvp").is(':checked'))
            {
                $("#ticker_info").hide();
                $("#non_rsvp").hide();
                $("#position_div").parent().show();
                $("#ticker_position_div").parent().hide();
                var imgSrc = $("#skin_preview_img").attr("src");
                if(imgSrc){
                    imgSrc = imgSrc.replace("thumbnail_tick.png","thumbnail.png");
                    $("#skin_preview_img").attr("src",imgSrc);
                }
                $("#once").prop('checked', true);
                patternChange('o');
                $("#dayly,label[for='dayly'],#weekly,label[for='weekly'],#monthly,label[for='monthly'],#yearly,label[for='yearly']").fadeOut(500);
                $("#rsvp_div").fadeIn(500);
                $("#linkedin_div").hide();
                $("#elements_to_display").show();
                $("#buttons_to_display").show();
                $(".save_button").show();
                $("#main_buttons_next").show();
                $("#right_items").height("430px");
                old_aknown = $("#aknown").is(':checked');
                $("#aknown").prop('checked', false).parent().hide();
                check_aknown();
                $("#print_div").hide();
                $('.tile').show();
                $('#schedule_webplugin').hide();
                $('#schedule_webplugin_table').hide();
                $('#webalert').prop( "disabled", true );
            }
            else if($("#alert_type_linkedin").is(':checked'))
            { 
                $("#linkedin_div").fadeIn(500);
                $("#ticker_info").hide();
                $("#non_rsvp").show();
                var imgSrc = $("#skin_preview_img").attr("src");
                if(imgSrc){
                    imgSrc = imgSrc.replace("thumbnail_tick.png","thumbnail.png");
                    $("#skin_preview_img").attr("src",imgSrc);
                }
                $("#elements_to_display").hide();
                $("#buttons_to_display").hide();
                $(".save_button").hide();
                $("#main_buttons_next").hide();
                $("#right_items").height("50px");
                $("#rsvp_div").hide();
                $("#shareAPI").button();
                old_aknown = $("#aknown").is(':checked');
                $("#aknown").prop('checked', false).parent().hide();
                check_aknown();
                $("#print_div").hide();
                $('.tile').show();
                $('#schedule_webplugin').hide();
                $('#schedule_webplugin_table').hide();
                $('#webalert').prop( "disabled", true );
            }
            else if($("#alert_type_webplugin").is(':checked'))
            { 
                $("#linkedin_div").hide();
                $("#ticker_info").hide();
                $("#non_rsvp").show();
                var imgSrc = $("#skin_preview_img").attr("src");
                if(imgSrc){
                    imgSrc = imgSrc.replace("thumbnail_tick.png","thumbnail.png");
                    $("#skin_preview_img").attr("src",imgSrc);
                }
                $("#elements_to_display").hide();
                $("#buttons_to_display").hide();
                $(".save_button").hide();
                $("#main_buttons_next").hide();
                $("#right_items").height("50px");
                $("#rsvp_div").hide();
                $("#shareAPI").button();
                old_aknown = $("#aknown").is(':checked');
                $("#aknown").prop('checked', false).parent().hide();
                check_aknown();
                $("#print_div").hide();
                $('.tile').hide();
                $('#schedule_webplugin').show();
                $('#schedule_webplugin_table').show();
                $('#webalert').prop( "disabled", false );
            }
            else{
			    
			    
                $('#webalert').prop( "disabled", true );
                $('#schedule_webplugin').hide();
                $('#schedule_webplugin_table').hide();
                $('.tile').show();
                $("#right_items").height(430);
                $("#non_rsvp").show();
                $(".save_button").show();
                $("#main_buttons_next").show();
                $("#elements_to_display").show();
                $("#buttons_to_display").show();
                $("#dayly,label[for='dayly'],#weekly,label[for='weekly'],#monthly,label[for='monthly'],#yearly,label[for='yearly']").fadeIn(500);
                if(old_aknown !== null){
                    $("#aknown").prop('checked', old_aknown);
                    check_aknown();
                }
                $("#aknown").parent().show();
                $("#print_div").show();
                if($("#alert_type_ticker").is(':checked'))
                {
                    var imgSrc = $("#skin_preview_img").attr("src");
                    if(imgSrc){
                        imgSrc = imgSrc.replace("thumbnail.png","thumbnail_tick.png");
                        $("#skin_preview_img").attr("src",imgSrc);
                    }
                    $("#ticker_info").fadeIn(500);
                    $("#digsign_info").hide();
                    $("#rsvp_div").hide();
                    $("#non_rsvp").show();
                    $("#position_div").parent().hide();
                    $("#linkedin_div").hide();
                    $("#ticker_position_div").parent().show();
                    $("#print_div").hide();
                }
                else
                { 
                    $("#ticker_info").fadeOut(500);
                    $("#digsign_info").fadeIn(500);
                    var imgSrc = $("#skin_preview_img").attr("src");
                    if(imgSrc){
                        imgSrc = imgSrc.replace("thumbnail_tick.png","thumbnail.png");
                    }
                    $("#skin_preview_img").attr("src",imgSrc);
                    if(webplugin != 1)
                        $("#position_div").parent().show();
                    $("#ticker_position_div").parent().hide();
                    $("#non_rsvp").show();
                    $("#linkedin_div").fadeOut(500); 
                    $("#rsvp_div").fadeOut(500);
                }
            }
        }
		
        function need_second_question_click()
        {
            if($("#need_second_question").is(':checked'))
            {
                $(".second_question").fadeIn(500);
            }
            else
            {
                $(".second_question").fadeOut(500);
            }
        }

        function check_value()
        {
            if($("#al_width").length>0){
                var al_width = document.getElementById('al_width').value;
                var al_height = document.getElementById('al_height').value;
                var size_message = document.getElementById('size_message');
				
                if(al_width > 1024 && al_height > 600){
                    size_message.innerHTML = '<font color="red"><%=LNG_RESOLUTION_DESC1 %></font>';
                    return;
                }

                if(al_width < 340 || al_height < 290){
                    size_message.innerHTML = "<font color='red'><%=LNG_RESOLUTION_DESC2 %>. <A href='#' onclick='set_default_size();'><%=LNG_CLICK_HERE %></a> <%=LNG_RESOLUTION_DESC3 %>.</font>";
                    return;
                }
                size_message.innerHTML = "";
            }
        }
		
        function isright(obj)
        {
            if(obj.value>60)
            {
                obj.value=60;
            }
        }

        function check_size_input(myfield, e, dec)
        {
            var key;
            var keychar;

            if (window.event)
                key = window.event.keyCode;
            else if (e)
                key = e.which;
            else
                return true;

            keychar = String.fromCharCode(key);

            // control keys
            if ((key==null) || (key==0) || (key==8) || 
				(key==9) || (key==13) || (key==27) )
            {
                return true;
            }
                // numbers
            else if(myfield.value.length>6)
            {
                return false;
            }
            else if ((("0123456789").indexOf(keychar) > -1))
            {
                return true;
            }
                // decimal point jump
            else if (dec && (keychar == "."))
            {
                myfield.form.elements[dec].blur();
                return false;
            }
            else
                return false;
        }

        function my_sub(i)
        {
            <% if request("webplugin") <> "1" then %>
                if($("#campaign_select").length > 0 && $("#campaign_select").val() != '-1' && i == 2)
            {
                alert("<%=LNG_CANNOT_SAVE_WITH_CAMPAIGN %>")
                return false;
            }
            if($("aut_close").checked==true){
                if($("autoclose").value=="0"){
                    showPrompt("<%=LNG_EDIT_ALERT_DB_ALERT1 %>.");
                    return false;
                }
                if($("autoclose").value.length<1){
                    showPrompt("<%=LNG_EDIT_ALERT_DB_ALERT1 %>.");
                    return false;
                }
            }
            if(document.getElementById('lifetimemode').checked==true){
                if(document.getElementById("lifetime").value=="0"){
                    showPrompt("<%=LNG_YOU_SHOULD_ENTER_LIFETIME %>.");
                    return false;
                }
                if(document.getElementById("lifetime").value.length<1){
                    showPrompt("<%=LNG_YOU_SHOULD_ENTER_LIFETIME %>.");
                    return false;
                }
            }
            <% end if %>
                var found_empty_alerts = false;
            var form = document.getElementsByName("my_form")[0];
            var elm = document.getElementsByName("elm");
            var alert_title = $(form).find("input[name=alert_title]");
            var html_titles = $(form).find("textarea[name=html_title]");
            <% if mobile_device<>true AND autosubmit<>1 then %>
			for(var j=0; j<alert_title.length; j++)
            {
                var al_title = alert_title[j];
                <% if ConfEnableHtmlAlertTitle = 1 then %>
                    al_title.value = $('<div/>').html(tinyMCE.get(html_titles[j].id).getContent()).text();
                <% end if %>
                    var al_value = tinyMCE.get(elm[j].id).getContent().replace(/ |<p.*?>&nbsp;<\/p>|<p.*?><span.*?><br.*?><\/span><\/p>/ig,'');
                if((al_value || j==0) && !al_title.value) {
                    showPrompt("<%=LNG_YOU_SHOULD_ENTER_ALERT_TITLE %>.");
                    <% if ConfEnableHtmlAlertTitle = 1 then%>
						html_titles.focus();
                    <% else %>
						al_title.focus();
                    <% end if %>
					return false;
                }
                if(al_title.value.length>255) {
                    showPrompt("<%=LNG_TITLE_SHOULD_BE_LESS_THEN %>.");
                    al_title.focus();
                    return false;
                }
                if(!al_value)
                    found_empty_alerts = true;
            }
            <% end if %>

			if(!($("#desktop_check:checked").val() || $("#email_check:checked").val() || $("#sms:checked").val() || $("#blogPost:checked").val() || $("#socialMedia:checked").val()|| $("#text_to_call:checked").val()))
            {
                showPrompt("<%=LNG_YOU_SHOULD_SELECT_ALERT_TYPE %>.");
                return false;
            }

            document.my_form.preview.value=0;
            document.my_form.sub1.value=i;

            if(i==3 || i==2)
            {
                if(document.getElementById('recurrence_checkbox').checked==true)
                {
                    return check_valid_dates_rec();
                }
            }
            if(found_empty_alerts && elm.length > 1)
                return confirm("<%=LNG_NOT_FILLED_LANGUAGES_ON_DEFAULT%>");

            return true;
        }

        function mypreview1()
        {
            if(document.getElementById("alert_type_alert").checked){
                document.my_form.preview.value=1;
            }
            else{
                document.my_form.preview.value=2;
            }
            document.my_form.sub1.value=2;
        }

        function mypreview()
        {
            var obj = document.getElementById("preview2");
            document.body.removeChild(obj);

            obj = document.getElementById("preview1");
            document.body.removeChild(obj);

            //	document.getElementsByName('preview2')[0].style.display = (document.getElementsByName('preview2')[0].style.display=='none')?'':'none'; 
            //	document.getElementsByName('preview1')[0].style.display = (document.getElementsByName('preview1')[0].style.display=='none')?'':'none'; 
            return false;
        }
        function check_blogpost()
        {
            var enabled = document.getElementById('blogPost').checked;
            if(enabled)
            {
                $("#recurrence_checkbox").prop("checked", false);
                $("#lifetimemode").prop("checked", false);
                check_recurrence();
                check_lifetime();
            }
            $("#recurrence_checkbox").prop("disabled", enabled);
            $("#lifetimemode").prop("disabled", enabled);
			
        }
        function check_lifetime()
        {
            var enabled = $("#lifetimemode").prop("checked");
            if(enabled)
            {
                $("#recurrence_checkbox").prop("checked", false);
                check_recurrence();		
            }
            else {
                $("#lifetime").prop("disabled", enabled);
                //$("#lifetime_factor").prop("disabled", !enabled);
            }
            if($("#blogPost_settings_hint").css('display') == 'none'){
                $("#blogPost").prop("disabled", enabled);
            }
            else $("#blogPost").prop("disabled", "disabled");
        }
		
        function check_recurrence()
        {
            var disabled = !($("#recurrence_checkbox").prop("checked"));
            var el = document.getElementById('recurrence_div');
            if($("#recurrence_checkbox").length) {
                toggleDisabled(el, disabled);
                if(!disabled)
                {
                    countdownEnableChange();
                    var recDisabled = $("#once").prop("checked");
                    el = $("#alert_time");
                    toggleDisabled(el, recDisabled);
                    el = $("#alert_range");
                    toggleDisabled(el, recDisabled);
                    $("#lifetimemode").prop("checked", false);
                    $("#blogPost").prop("checked", false);
                    check_lifetime();				
                }
                else
                {
                    countdownEnableChange(false, disabled);
                }
                if($("#blogPost_settings_hint").css('display') == 'none'){
                    $("#blogPost").prop("disabled", !disabled);
                }
                else $("#blogPost").prop("disabled", "disabled");

                /*	if(disabled)
                    {
                        document.getElementById("recurrence_div").style.display="none";
                    }
                    else
                    {
                        document.getElementById("recurrence_div").style.display="";
                    }*/
            }
        }
				
        function check_aknown()
        {
            if ($("#aut_close").is(":checked")){
                $('#autoclose').prop('disabled', false);
                $('#man_close').prop('disabled', false);
                $('#aknown').attr({'disabled': true, 'checked': false});
                $('#unobtrusive').attr({'disabled': true, 'checked': false});
            } else if ($("#aknown").is(":checked")){
                $('#aut_close').attr({'disabled': true, 'checked': false});
                $('#unobtrusive').attr({'disabled': true, 'checked': false});
                $('#man_close').attr({'disabled': true, 'checked': false});
                $('#autoclose').prop('disabled', true);
            } else if ($("#unobtrusive").is(":checked")) {
                $('#aknown').attr({'disabled': true, 'checked': false});
                $('#aut_close').attr({'disabled': true, 'checked': false});
                $('#man_close').attr({'disabled': true, 'checked': false});
                $('#autoclose').prop('disabled', true);
            } else {
                $('#man_close').attr({'disabled': true, 'checked': false});
                $('#autoclose').prop('disabled', true);
                $('#aut_close').prop('disabled', false);
                $('#aknown').prop('disabled', false);
                $('#unobtrusive').prop('disabled', false);
            }
        }

        function check_email()
        {
            var elem = document.getElementById('email_check');
            if(elem)
            {
                var disabled = !elem.checked;
                var el = document.getElementById('email_div');
                toggleDisabled(el, disabled);

                /*		if(disabled)
                        {
                            document.getElementById("email_div").style.display="none";
                        }
                        else
                        {
                            document.getElementById("email_div").style.display="";
                        }*/
            }
        }
		
        function im_interface(){
            $("#socialMedia").prop('disabled',true);
            $("#blogPost").prop('disabled',true);
            $("#socialMedia").parent().hide
            $("#lnkdn_type_tab").hide();
            $(".save_button").hide();
        }

        function check_tab(type)
        {
		
			 
            if(type == 'alert')
            {
                // $("#webplugin").val(0);
                document.getElementById('alert_type_alert').checked = true;
            }
            else if(type == 'ticker')
            {
                //	 $("#webplugin").val(0);
                document.getElementById('alert_type_ticker').checked = true;
                $("#blogPost").parent().hide();
            }
            else if(type == 'rsvp')
            {
                // $("#webplugin").val(0);
                document.getElementById('alert_type_rsvp').checked = true;
            }
            else if(type == 'linkedin')
            {
                //	 $("#webplugin").val(0);
                document.getElementById('alert_type_linkedin').checked = true;
            }
            else if(type == 'webplugin')
            {
                //	 $("#webplugin").val(1);
                document.getElementById('alert_type_webplugin').checked = true;
            }
            check_alert_type();
            if (<%=instant_message %>==1){
                im_interface();
        }
        <%if(gid = 0 OR (gid = 1 AND alerts_arr(6)<>"")) then %>
        check_size();
        <%end if%>
        }

    function check_desktop()
        {
        var elem = document.getElementById('desktop_check');
        if(elem)
        {
            var disabled = !elem.checked;
            var el = document.getElementById('ticker_div');
            toggleDisabled(el, disabled);
        }
        check_size();
        }

            function check_size()
            {
                var elem = document.getElementById('alert_type_ticker');
                if(elem)
                {
                    var disabled = elem.checked;
                    var el = document.getElementById('size_div');
                    toggleDisabled(el, disabled);
                    el = document.getElementById('size_message');
                    toggleDisabled(el, disabled);
                }
                check_message();
            }

        function check_message()
        {
				
            var elem = document.getElementById('size2');
            var msg = document.getElementById('size_message');
            var al_width = document.getElementById('al_width');
            var al_height = document.getElementById('al_height');
            var resizable = document.getElementById('resizable');
            var position_fieldset = document.getElementById('position_fieldset');
            var webplugin = '<%=request("webplugin")%>'
            if(elem && al_width && al_height)
            {
                if(msg) msg.style.display = elem.checked || webplugin == "1" ? "none" : "";
                al_width.disabled = elem.checked;
                al_height.disabled = elem.checked;
                position_fieldset.disabled = elem.checked;
                $("#ticker_position_fieldset").disabled = elem.checked;
                if(resizable) resizable.disabled = elem.checked;
            }
        }
		
        function check_settings()
        {
            <% if BLOG=1 then
            Set rsBlogIds = Conn.Execute("SELECT id FROM blogs")
            if not rsBlogIds.EOF then 'check Blog settings'%>
               $("#blogPost_settings_hint").hide();
            <% else %>
                $("#blogPost_settings_hint").show();
            <% end if%>
	    <% end if		
	    if SOCIAL_MEDIA=1 then
            Set rsSocialIds = Conn.Execute("SELECT id FROM social_media WHERE type=1")
            if not rsSocialIds.EOF then 'check Twitter settings'%>
               $("#social_media_settings_hint").hide(); 
            $("#socialMedia").prop("disabled", false);    
            <% else %>
                $("#social_media_settings_hint").show();
            <% end if%>
	    <% end if%>	

	    }

    </script>

    <link href="css/form_style.css" rel="stylesheet" type="text/css" />
    <style>
        .html_title_div
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 12px; /*	overflow-x: hidden;
				overflow-y: hidden;
				height: 70px;*/
            width: 310px; /*	border-bottom:1px solid #cccccc;*/
        }
    </style>
    <!--[if IE]>
		<style type="text/css" media="all">
		@import "css/ie.css";
		</style>
		<![endif]-->
</head>
<body style="margin: 0px" class="body">
    <%
		if (uid <> "")  then
			id=Request("id")
			tt_id=Request("tt_id")
			skin_id = Replace(Request("skin_id"), "'", "''")
			

			'save alert and go to "send" page
			if(Request("sub1")<>"") then
				text=""
				title=""
				text_t=""
				alertClass=1
				non_edit=1
				Set elm = Request("elm")
				Set elm_title = Request("alert_title")
				Set elm_html_title = Request("html_title")
				Set lang = Request("lang")
				Set lang_default = Request("lang_default")
				if (skin_id = "") then
					skin_id_s = "NULL"
				else
					skin_id_s = "'"& skin_id &"'"
				end if
                text_t = elm(1) 
				If EnableAlertLangs=1 Then
					For i=1 to elm.Count
						If(lang_default(i)="1" Or LCase(lang_default(i))="true") Then
							title = Replace(elm_title(i), "'", "''")
						End If
						if elm_html_title(i) <> "" then
							html_title = "<!-- html_title="""& HTMLEncode(elm_html_title(i)) &""" -->"
						else
							html_title = ""
						end if
						text=text+"<!-- begin_lang lang="""&HtmlEncode(lang(i))&""" title="""&HtmlEncode(elm_title(i))&""" is_default="""&HtmlEncode(lang_default(i))&""" -->"& elm(i) & html_title &"<!-- end_lang -->"
					Next
				Else
					if elm_html_title(1) <> "" then
						html_title = "<!-- html_title="""& HTMLEncode(elm_html_title(1)) &""" -->"
					else
						html_title = ""
					end if
					title = Replace(elm_title(1), "'", "''")
					text = elm(1) & html_title
					
				End If

				recurrence=Clng(Request("recurrence"))
				countdown_enable = Clng(Request("countdown_enable"))
				
				if(recurrence <> 1) then recurrence=0 end if
				
				if (countdown_enable = 1 and Request("pattern") = "o") then
					Set countdown_array = Request("countdowns")
					recurrence = 1
					schedule=0
					from_date=Request("from_date")
					to_date=Request("to_date")
				elseif recurrence = 1 and Request("pattern") = "o" then
					recurrence = 0
					schedule=1

					from_date=Request("from_date")
					to_date=Request("to_date")
				else
					schedule=0

					from_date=Request("from_date_rec")
					to_date=Request("to_date_rec")

					from_date = from_date & " " & Request("from_time")

					if(to_date<>"") then 
						to_date = to_date & " " & Request("to_time")
					else
						to_date = Request("to_time")
					end if
					
				end if
				if(recurrence = 0 AND schedule = 0) then
					from_date = ""
					to_date = ""
				end if
				urgent=Request("urgent")

				email=Request("email")
				device_type=Request("dev")			
				email_sender=request("email_sender")
				desktop=request("desktop")
				sms=request("sms")
				text_to_call=Request("text_to_call")
				unobtrusive=Request("unobtrusive")
				webalert=Request("webalert")
				resizable=Request("resizable")
				position=Request("position")
				ticker_position=Request("ticker_position")
				blogPost=Request("blogPost")
				socialMedia=Request("socialMedia")
				aknown=Request("aknown")
				
				
				if(Request("webplugin") = "1") then
				    aknown = 0
				end if
				
				man_close=Request("man_close")
				aut_close=Request("aut_close")
				autoclose=Request("autoclose")
				alert_type=Request("alert_type")
				fullscreen=Request("fullscreen")
				self_deletable=Request("self_deletable")
				print_alert=Request("print_alert")
				color_code=Request("color_code")
				campaignid=Request("campaign_select")
				if(clng(Request("alert_width"))<=0 or clng(Request("alert_height"))<=0) then
					'get value from xml
				else
					alert_height = clng(Request("alert_height"))
					alert_width = clng(Request("alert_width"))
				end if		
				toolbarmode=Request("toolbarmode")
				deskalertsmode=Request("deskalertsmode")

                has_lifetime = 0
				if Request("lifetimemode") = "1" and Request("lifetime") <> "" and Request("lifetime_factor") <> "" then
					from_date = now_db
					lifetime = Request("lifetime") * Request("lifetime_factor")
					to_date = DateAdd("s", 1, DateAdd("n", lifetime, from_date))
					lifetime = Request("lifetime")
					lifetime_factor = Request("lifetime_factor")
					has_lifetime = 1
					schedule = 1
				end if

				template=Clng(Request("template"))
				if(urgent <> 1) then urgent=0 end if

				if(blogPost <> 1) then blogPost=0 end if
				if(socialMedia <> 1) then socialMedia=0 end if
				if(email <> 1) then email=0 end if
				if(desktop <> 1) then desktop=0 end if
				if(sms <> 1) then sms=0 end if
				if(text_to_call <> 1) then text_to_call=0 end if
				if (unobtrusive = "1") then alertClass=16 end if
				if (webalert = "1") then alertClass=999 end if
				if (resizable <> 1) then resizable = 0 end if
				

				if(aknown <> 1) then aknown=0 end if
				if(aut_close <> 1) then autoclose = 0 end if
				if(autoclose = "") then autoclose=0 end if
				if(man_close = 1) then autoclose=-1*autoclose end if
				autoclose=autoclose*60
				if(alert_type <> 3) then linkedin=0 else linkedin = "checked" end if
				if(alert_type <> 1) then ticker=0 else ticker=1 end if
				if(ticker_position = undefined) then ticker_position =0 end if
				if(position = undefined) then position =0 end if
				if(fullscreen <> 1) then 
					if ticker=0 then
						fullscreen=position 
					else
						fullscreen=ticker_position
						if (TICKER_PRO = 1) then
    						if(InStr(text, "<!-- ticker_pro -->")=0) then
						    text = text & "<!-- ticker_pro -->"
						    end if
				        else
						    if(InStr(text, "<!-- ticker_pro -->") <> 0) then
						        text = replace(text, "<!-- ticker_pro -->", "")
						    end if
						end if
				    end if
				end if
				if(self_deletable <> 1) then self_deletable=0 end if
				if(self_deletable = 1) then 
						if(InStr(text, "<!-- self-deletable -->")=0) then
						    text = text & "<!-- self-deletable -->"
						end if
				else
						if(InStr(text, "<!-- self-deletable -->") <> 0) then
								text = replace(text, "<!-- self-deletable -->", "")
						end if
				end if
				if(print_alert <> 1) then print_alert=0 end if
				if(print_alert = 1) then 
						if(InStr(text, "<!-- printable_alert -->")=0) then
						    text = text & "<!-- printable_alert -->"
						end if
				else
						if(InStr(text, "<!-- printable_alert -->") <> 0) then
								text = replace(text, "<!-- printable_alert -->", "")
						end if
				end if
				
				SQL = "SELECT id FROM color_codes WHERE color='"&color_code&"'"
				Set RSCC = Conn.Execute(SQL)
				if not RSCC.EOF then
					color_code = RSCC("id")
				else
					color_code = "00000000-0000-0000-0000-000000000000"
				end if

				if(toolbarmode <> 1) then toolbarmode=0 end if
				if(deskalertsmode <> 1) then deskalertsmode=0 end if
				schedule_type=1
				
				
				
				
		    	if campaignid <> "-1" then
		    	    schedule_type = 0
		    	 end if
		    	 
		    	 if(Request("webplugin") = "1") then 
		    	 	 alertClass = 2048
				end if
				 
				 Set is_started = Conn.Execute("SELECT DISTINCT 1 FROM alerts WHERE campaign_id = " & campaignid & " AND schedule_type = '1'")
				 if not is_started.EOF then
					schedule_type = 1
				 end if
				
                    approve_status = 1
    
                     if(APPROVE = 1 and autosubmit <> 1) then
                             Set enabled = Conn.execute("SELECT val FROM settings WHERE name = 'ConfAllowApprove'") 
         
                            if(enabled("val") = "1" and  gid <> 0 and alerts_arr(7) <> "checked"  and instant_message <> 1) then
                                approve_status = 0
                             end if
                             if (enabled("val") = "1" and  instant_message = 1  and  gid <> 0 and im_arr(5) <> "checked"  ) then 
			                    approve_status = 0
			                 end if 
                     end if
                     

                     
				if (top_template<>"" OR bottom_template<>"") then
					text = "<!-- top_template_start -->" & top_template & "<!-- top_template_end -->"& text & "<!-- bottom_template2_start -->" & bottom_template & "<!-- bottom_template2_end -->"
				end if
				
				text = replace(text, "'", "''")
				emailSender = replace(email_sender, "'", "''")
				dup_id = Request("id")
				if(Request("edit") = 1) then
					id = dup_id
					Set rs = Conn.Execute("SELECT type FROM alerts WHERE id=" & id)
					if(Not rs.EOF) then 
						mytype = rs("type")
						rs.Close
					else
						mytype = ""
					end if
					
					
					


                    if(request("shouldApprove") = "1") then
  	                    SQL = "UPDATE alerts SET  schedule='"& schedule &"', recurrence='"& recurrence &"', alert_text = N'"& text &"', title=N'"& title &"', create_date=GETDATE(), from_date='" & from_date & "', to_date='" & to_date & "', urgent='"&urgent&"', email='"&email&"', desktop='"&desktop&"', sms='"&sms&"', email_sender=N'"& emailSender &"', ticker='"&ticker&"', fullscreen='"&fullscreen&"', alert_width="&alert_width&",  alert_height="&alert_height&", aknown='"&aknown&"', autoclose="&autoclose&", toolbarmode='"&toolbarmode&"', deskalertsmode='"&deskalertsmode&"', template_id="&template&", caption_id="& skin_id_s &", resizable="& resizable &", post_to_blog="&blogPost&", social_media="&socialMedia&", self_deletable="&self_deletable&", text_to_call="&text_to_call&", color_code='"&color_code&"', lifetime=" & has_lifetime & "  WHERE id=" & id
						Conn.Execute(SQL)
						'Conn.Execute("buildAlertsCacheForAlert @alert_id = "  & id )                     
                    elseif(request("rejectedEdit") = "1") then
 	                    SQL = "UPDATE alerts SET schedule='"& schedule &"', recurrence='"& recurrence &"', alert_text = N'"& text &"', title=N'"& title &"', create_date=GETDATE(), from_date='" & from_date & "', to_date='" & to_date & "', urgent='"&urgent&"', email='"&email&"', desktop='"&desktop&"', sms='"&sms&"', email_sender=N'"& emailSender &"', ticker='"&ticker&"', fullscreen='"&fullscreen&"', alert_width="&alert_width&",  alert_height="&alert_height&", aknown='"&aknown&"', autoclose="&autoclose&", toolbarmode='"&toolbarmode&"', deskalertsmode='"&deskalertsmode&"', template_id="&template&", caption_id="& skin_id_s &", resizable="& resizable &", post_to_blog="&blogPost&", social_media="&socialMedia&", self_deletable="&self_deletable&", text_to_call="&text_to_call&", color_code='"&color_code&"', lifetime=" & has_lifetime & "  WHERE id=" & id
						Conn.Execute(SQL)                             
					elseif(mytype = "D") then 'elseif(mytype = "D") then
						SQL = "UPDATE alerts SET schedule='"& schedule &"', recurrence='"& recurrence &"', alert_text = N'"& text &"', title=N'"& title &"', create_date=GETDATE(), from_date='" & from_date & "', to_date='" & to_date & "', urgent='"&urgent&"', email='"&email&"', desktop='"&desktop&"', sms='"&sms&"', email_sender=N'"& emailSender &"', ticker='"&ticker&"', fullscreen='"&fullscreen&"', alert_width="&alert_width&",  alert_height="&alert_height&", aknown='"&aknown&"', autoclose="&autoclose&", toolbarmode='"&toolbarmode&"', deskalertsmode='"&deskalertsmode&"', template_id="&template&", sender_id="&uid&", caption_id="& skin_id_s &", resizable="& resizable &", post_to_blog="&blogPost&", social_media="&socialMedia&", self_deletable="&self_deletable&", text_to_call="&text_to_call&", color_code='"&color_code&"', lifetime=" & has_lifetime & "  WHERE id=" & id
						Conn.Execute(SQL)
					elseif (mytype = "L") then
						SQL = "UPDATE alerts SET schedule='"& schedule &"', recurrence='"& recurrence &"', alert_text = N'"& text &"', title=N'"& title &"', create_date=GETDATE(), from_date='" & from_date & "', to_date='" & to_date & "', urgent='"&urgent&"', email='"&email&"', desktop='"&desktop&"', sms='"&sms&"', email_sender=N'"& emailSender &"', ticker='"&ticker&"', fullscreen='"&fullscreen&"', alert_width="&alert_width&",  alert_height="&alert_height&", aknown='"&aknown&"', autoclose="&autoclose&", toolbarmode='"&toolbarmode&"', deskalertsmode='"&deskalertsmode&"', template_id="&template&", sender_id="&uid&", caption_id="& skin_id_s &", resizable="& resizable &", post_to_blog="&blogPost&", social_media="&socialMedia&", self_deletable="&self_deletable&", text_to_call="&text_to_call&", color_code='"&color_code&"', lifetime=" & has_lifetime & " WHERE id=" & id
						Conn.Execute(SQL)
					else
						id = addAlertToDataBase(text, title, from_date, to_date, schedule, schedule_type, recurrence, urgent, email, desktop, sms, emailSender, ticker, fullscreen, alert_width, alert_height, aknown, autoclose, toolbarmode, deskalertsmode, template, uid, skin_id_s, alertClass, resizable, blogPost, socialMedia, self_deletable, text_to_call, color_code, campaignid, approve_status, has_lifetime,device_type)
					end if
				else
					if (countdown_enable = 1) then recurrence = 1 end if
					id = addAlertToDataBase(text, title, from_date, to_date, schedule, schedule_type, recurrence, urgent, email, desktop, sms, emailSender, ticker, fullscreen, alert_width, alert_height, aknown, autoclose, toolbarmode, deskalertsmode, template, uid, skin_id_s, alertClass, resizable, blogPost, socialMedia, self_deletable, text_to_call, color_code, campaignid, approve_status, has_lifetime,device_type)
				end if
				if recurrence = 1 then
					recurrenceProcess id, countdown_array
				end if
		
				'update RSVP data
				survey_id = ""
				set RSsurvey = Conn.Execute("SELECT id FROM surveys_main WHERE closed = 'A' and sender_id = "& id) 'fake survey without data
				if not RSsurvey.EOF then
					if alert_type = 2 then 'is RSVP
						survey_id = RSsurvey("id")
					else
						'delete survey data
						Conn.Execute("DELETE FROM surveys_answers WHERE question_id IN (SELECT id FROM surveys_questions WHERE survey_id = "& RSsurvey("id") &")")
						Conn.Execute("DELETE FROM surveys_answers WHERE survey_id = "& RSsurvey("id"))
						Conn.Execute("DELETE FROM surveys_questions WHERE survey_id = "& RSsurvey("id"))
						Conn.Execute("DELETE FROM surveys_main WHERE id = "& RSsurvey("id"))
					end if
				elseif alert_type = 2 then 'is RSVP
					set RSsurvey = Conn.Execute("SET NOCOUNT ON" & vbNewLine & "INSERT INTO surveys_main (type, closed, sender_id) VALUES ('A', 'A', "& id &") "& vbNewLine & "SELECT SCOPE_IDENTITY() as id")
					survey_id = RSsurvey("id")
				end if
				if survey_id <> "" then
					question = Request("question")
					question_option1 = Request("question_option1")
					question_option2 = Request("question_option2")
					need_second_question = Request("need_second_question")
					second_question = Request("second_question")
					
					
					set RSquestion = Conn.Execute("SELECT id FROM surveys_questions WHERE survey_id = "& survey_id &" AND question_number = 1")
					if not RSquestion.EOF then
						Conn.Execute("UPDATE surveys_questions SET question = N'"& Replace(question, "'", "''") &"' WHERE id = "& RSquestion("id"))
					else
						set RSquestion = Conn.Execute("SET NOCOUNT ON" & vbNewLine & "INSERT INTO surveys_questions (question, survey_id, question_number, question_type) VALUES(N'"& Replace(question, "'", "''") &"', "& survey_id &", 1, 'M')"& vbNewLine & "SELECT SCOPE_IDENTITY() as id")
					end if
					
					set RSanswers = Conn.Execute("SELECT id FROM surveys_answers WHERE question_id = "& RSquestion("id") &" ORDER BY id")
					if not RSanswers.EOF then
						Conn.Execute("UPDATE surveys_answers SET answer = N'"& Replace(question_option1, "'", "''") &"' WHERE id = "& RSanswers("id"))
						RSanswers.MoveNext
					else
						Conn.Execute("INSERT INTO surveys_answers (answer, question_id, correct) VALUES(N'"& Replace(question_option1, "'", "''") &"', "& RSquestion("id") &", 0)")
					end if
					if not RSanswers.EOF then
						Conn.Execute("UPDATE surveys_answers SET answer = N'"& Replace(question_option2, "'", "''") &"' WHERE id = "& RSanswers("id"))
					else
						Conn.Execute("INSERT INTO surveys_answers (answer, question_id, correct) VALUES(N'"& Replace(question_option2, "'", "''") &"', "& RSquestion("id") &", 0)")
					end if
					
					if need_second_question = "1" then
						set RSquestion = Conn.Execute("SELECT id FROM surveys_questions WHERE survey_id = "& survey_id &" AND question_number = 2")
						if not RSquestion.EOF then
							Conn.Execute("UPDATE surveys_questions SET question = N'"& Replace(second_question, "'", "''") &"' WHERE id = "& RSquestion("id"))
						else
							Conn.Execute("INSERT INTO surveys_questions (question, survey_id, question_number, question_type) VALUES(N'"& Replace(second_question, "'", "''") &"', "& survey_id &", 2, 'I')")
						end if
					else
						Conn.Execute("DELETE FROM surveys_questions WHERE survey_id = "& survey_id &" AND question_number = 2")
					end if
				end if
			end if
			if(Request("sub1") = "3") then
			
			    approve = "0"
			   
			    if ( instant_message <> 1  and ( gid = 0 or alerts_arr(7) = "checked" ) ) then 
			        approve = "1"
			        
			    end if 
			    if (im_arr(5) = "checked" and instant_message = 1)then
			        approve = "1"
			    end if
			  '  if(request("shouldApprove") = "1") then
			       ' Response.Redirect "approve_panel.aspx"
			  '  elseif(request("rejectedEdit") = "1") then 
			      '  Response.Redirect "RejectedAlerts.aspx?uid=" & uid
				if instant_message=1 then
					Response.Redirect "alert_users.asp?id="& id &"&dup_id="& dup_id & "&instant=" &instant_message &"&return_page=instant_messages.asp&recurrence="&schedule & "&campaign_id=" & campaignid & "&approve=" & approve & "&shouldApprove=" & request("shouldApprove") & "&rejectedEdit=" & request("rejectedEdit")
				elseif  campaignid <> "-1" then
				    Response.Redirect "alert_users.asp?id="& id &"&dup_id="& dup_id & "&instant=" &instant_message &"&return_page=CampaignDetails.aspx?campaignid="& campaignid &"&recurrence="&schedule & "&campaign_id=" & campaignid & "&approve=" & approve & "&shouldApprove=" & request("shouldApprove") & "&rejectedEdit=" & request("rejectedEdit")
				elseif  Request("webplugin") = "1" then
				    Response.Redirect "alert_users.asp?id="& id &"&dup_id="& dup_id & "&autosubmit=" &autosubmit & "&recurrence="&schedule & "&campaign_id=" & campaignid & "&webalert=1" & "&approve=" & approve & "&shouldApprove=" & request("shouldApprove") & "&rejectedEdit=" & request("rejectedEdit") &"&return_page=DigSignLinks.asp"
				else 			   
					Response.Redirect "alert_users.asp?id="& id &"&dup_id="& dup_id & "&autosubmit=" &autosubmit & "&recurrence="&schedule & "&campaign_id=" & campaignid & "&approve=" & approve & "&shouldApprove=" & request("shouldApprove") & "&rejectedEdit=" & request("rejectedEdit")
				end if
			end if

			if(Request("sub1") = "1") then
    %>
    
    <table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%" class="main_table"
        <% if autosubmit = 1 then Response.Write (" style='display:none' ") end if %>>
        <tr>
            <td>
                <table id="dialog_dock" width="100%" height="100%" bordercolor="#000000" cellspacing="0"
                    cellpadding="0" class="main_table">
                    <tr>
                        <td width="100%" height="31" class="main_table_title">
							<%if instant_message=1 then %>
							<img src="images/menu_icons/im_20.png" alt="" width="20" height="20" border="0" style="padding-left:7px">
							<%else %>
							<img src="images/menu_icons/alert_20.png" alt="" width="20" height="20" border="0" style="padding-left:7px">
							<%end if %>
                            <a href="#" class="header_title" style="position:absolute;top:15px">
                                <%if instant_message=1 then Response.Write(LNG_INSTANT_MESSAGES) else Response.Write(LNG_ALERTS) end if%></a>
                        </td>
                    </tr>
                    <tr>
                        <td height="100%" class="main_table_body">
                        
                        
                            <div style="margin: 10px;">
                                <span class="work_header">
                                    <%=LNG_ADD_ALERT %></span>
                                <br />
                                <br />
                                <p>
                                    <strong>
                                        <%=LNG_DRAFT_DESC %>.</strong></p>
                                <!--img src="images/inv.gif" alt="" width="1" height="100%" border="0"/-->
                            </div>
                         
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <%
			else
				'save alert and continue editing
				start_date = now_db
				end_date=""

				if ConfEndDateByDefault then
					end_date=DateAdd("n", ConfEndDateByDefault, now_db)
				end if

				if ConfScheduleByDefault then
					recurrence = "checked"
				else
					recurrence = ""
				end if
				
				position = ConfAlertPosition
				ticker_position = ConfTickerPosition

				if ConfUrgentByDefault then
					urgent = "checked"
				else
					urgent = ""
				end if

				if ConfAcknowledgementByDefault then
					aknown = "checked"
				else
					aknown = ""
				end if
				
				if ConfSelfDeletableByDefault then
					self_deletable = "checked"
				else
					self_deletable = ""
				end if
				
				if ConfUnobtrusiveByDefault then
					unobtrusive = "checked"
				else
					unobtrusive = ""
				end if

				if ConfPostToBlogByDefault then
					blogPost = "checked"
				else
					blogPost = ""
				end if

				if ConfEmailByDefault then
					email = "checked"
				else
					email = ""
				end if

				if ConfSMSByDefault then
					sms = "checked"
				else
					sms = ""
				end if
				
				if ConfTextCallByDefault then
					text_to_call = "checked"
				else
					text_to_call = ""
				end if
				
				if ConfSocialMediaByDefault then
					socialMedia = "checked"
				else
					socialMedia = ""
				end if

				if ConfDesktopAlertByDefault then
					desktop = "checked"
				else
					desktop = ""
				end if

				if (ConfDesktopAlertType = "T") then
					ticker = "checked"
				else
					ticker = ""
				end if

				if (ConfDesktopAlertType = "R") then
					rsvp = "checked"
				else
					rsvp = ""
				end if
				
				if (ConfDesktopAlertType = "L") then
					linkedin = "checked"
				else
					linkedin = ""
				end if
				
				if (ConfPopupType = "F") then
					fullscreen = "checked"
				else
					fullscreen = ""
				end if

				if (ConfAutocloseByDefault = "1") then
					aut_close = "checked"
				else
					aut_close = ""
				end if
				
				if (ConfAllowManualCloseByDefault = "1") then
					man_close = "checked"
				else
					man_close = ""
				end if
				autoclose = ConfAutocloseValue

				toolbarmode = "checked"
				deskalertsmode = "checked"

				lifetimemode = ""
				lifetime = ""
				if ConfMessageExpire > 0 then
					lifetime = ConfMessageExpire
					if not ConfScheduleByDefault then
						lifetimemode = "checked"
						recurrence = ""
					end if
				end if
				
				if(ConfDefaultEmail="")  then
					userName=""
					Set RSUser = Conn.Execute("SELECT name FROM users WHERE id="&uid)
					if(Not RSUser.EOF) then
						userName=RSUser("name")
					end if
					email_sender = userName
				else
					email_sender = ConfDefaultEmail
				end if

				question = LNG_WILL_YOU_BE_ATTENDING &"?"
				question_option1 = LNG_YES
				question_option2 = LNG_NO
				need_second_question = "0"
				second_question = LNG_PLEASE_DESCRIBE_YOUR_REASON

				if(id <> "") then
				
									
					Set rs = Conn.Execute("SELECT alert_text, title, type, recurrence, schedule, urgent, email, desktop, sms, email_sender, ticker, fullscreen, alert_width, alert_height, aknown, autoclose,  from_date, to_date, template_id, toolbarmode, deskalertsmode, create_date, caption_id, class, resizable, post_to_blog, social_media, self_deletable, text_to_call, color_code,device FROM alerts WHERE id=" & CStr(id))
					
					if(rs.EOF) then
						Set rs = Conn.Execute("SELECT alert_text, title, type, recurrence, schedule, urgent, email, desktop, sms, email_sender, ticker, fullscreen, alert_width, alert_height, aknown, autoclose,  from_date, to_date, template_id, toolbarmode, deskalertsmode, create_date, caption_id, class, resizable, post_to_blog, social_media, self_deletable, text_to_call, color_code,device FROM archive_alerts WHERE id=" & CStr(id))
					end if
					
					'Response.Write(id)
					
					if ( Not rs.EOF ) then 
						al_title = rs("title")
						alert_dev=rs("device")
						if (rs("class") = "999") then
						    webplugin_ch = "checked"
						end if
						al_text = rs("alert_text")
						al_create_date = rs("create_date")
						tick1 = rs("ticker")
						set RSsurvey = Conn.Execute("SELECT id FROM surveys_main WHERE closed = 'A' and sender_id = " & id)
						if not RSsurvey.EOF then
							rsvp = "checked"
							set RSquestions = Conn.Execute("SELECT id, question FROM surveys_questions WHERE survey_id = "& RSsurvey("id") &" ORDER BY question_number")
							if not RSquestions.EOF then
								question = RSquestions("question")
								set RSanswers = Conn.Execute("SELECT answer, correct FROM surveys_answers WHERE question_id = "& RSquestions("id") &" ORDER BY id")
								if not RSanswers.EOF then
									question_option1 = RSanswers("answer")
									correct1 = RSanswers("correct")
									RSanswers.MoveNext
							end if
								if not RSanswers.EOF then
									question_option2 = RSanswers("answer")
									correct2 = RSanswers("correct")
								end if
								RSquestions.MoveNext
							end if
							if not RSquestions.EOF then
								need_second_question = "1"
								second_question = RSquestions("question")
							end if
						else
							rsvp = ""
						end if
                        if(rs("type") = "L") then 
							linkedin = "checked"
						end if
						
						if(Not IsNull(rs("from_date"))) then 
							start_date = rs("from_date")
						end if
					
						if(Not IsNull(rs("to_date"))) then 
								end_date = rs("to_date")
						end if

						recurrence = ""
						urgent = ""
						email = ""
						sms = ""
						unobtrusive = ""
						aknown = ""
						man_close = ""
						lifetimemode = ""
						lifetime = ""
						blogPost = ""
						socialMedia = ""
						resizable = ""
						self_deletable = ""
						text_to_call =""
						color_code="747474"

						if(Not IsNull (rs("schedule"))) then
							if(rs("schedule")=1) then
								pattern = "o"
								recurrence = "checked"
								if start_date <> "" and end_date <> "" then
									if year(end_date) <> 1900 then
										if (MyMod(DateDiff("s", start_date, end_date),60)) = 1 then 'if lifetime
											lifetimemode = "checked"
											lifetime = DateDiff("n", start_date, end_date)
											recurrence = ""
										end if
									end if
								end if
							end if
						end if
						
						if(Not IsNull (rs("resizable"))) then
							if(rs("resizable")) then
								resizable = "checked"
							end if
						end if

						if(Not IsNull (rs("recurrence"))) then
							if(rs("recurrence")="1") then
								recurrence = "checked"
							end if
						end if

						if(Not IsNull (rs("urgent"))) then
							if(rs("urgent")=1) then urgent = "checked" end if
						end if
						
						if(Not IsNull (rs("color_code"))) then
							if rs("color_code") <> "00000000-0000-0000-0000-000000000000" then
							SQL = "SELECT color FROM color_codes WHERE id='"&rs("color_code")&"'"
							Set RSCC = Conn.Execute(SQL)
							if not RSCC.EOF then
								color_code = RSCC("color")
							else
								color_code = "747474"
							end if
							end if
						end if
						

						
						if(Not IsNull (rs("email"))) then
							if(rs("email")=1) then 
								email = "checked" 
								desktop = ""
							end if
						end if
						
						if(Not IsNull (rs("post_to_blog"))) then
							if(rs("post_to_blog")) then 
								blogPost = "checked"
							end if
						end if
						
						if(Not IsNull (rs("social_media"))) then
							if(rs("social_media")) then 
								socialMedia = "checked"
							end if
						end if
						
						if(Not IsNull (rs("self_deletable"))) then
							if(rs("self_deletable")) then 
								self_deletable = "checked"
							end if
						end if
						
						if(Not IsNull (rs("sms"))) then
							if(rs("sms")=1) then 
								sms = "checked" 
								desktop = ""
							end if
						end if
						
						if(Not IsNull (rs("text_to_call"))) then
							if(rs("text_to_call")=1) then 
								text_to_call = "checked" 
								desktop = ""
							end if
						end if 
						
						if(Not IsNull (rs("class"))) then
							if(rs("class")="16") then 
								unobtrusive = "checked" 
							end if
						end if
						
						if(Not IsNull (rs("desktop"))) then
							if(rs("desktop")=1) then desktop = "checked" else desktop = "" end if
						end if

						if(Not IsNull (rs("email_sender"))) then
							if(rs("email_sender")<>"") then
								email_sender = rs("email_sender")
							end if
						end if

						if(Not IsNull (rs("ticker"))) then
							if(rs("ticker")=1) then ticker = "checked" else ticker = "" end if
						end if

						if(Not IsNull (rs("fullscreen"))) then
							if(rs("fullscreen")="1") then 
								fullscreen = "checked" 
							else 
								fullscreen = "" 
								if(rs("fullscreen")<>"0") then
									if ticker="" then
										position = rs("fullscreen")
									else
										ticker_position = rs("fullscreen")
									end if
								end if
							end if
						end if

						if(Not IsNull (rs("alert_width"))) then
							if(rs("alert_width") <> "0") then alert_width =  rs("alert_width") end if
						end if

						if(Not IsNull (rs("alert_height"))) then
							if(rs("alert_height")<> "0") then alert_height=  rs("alert_height") end if
						end if
						
						if(Not IsNull (rs("aknown"))) then
							if(rs("aknown")=1) then aknown = "checked" end if
							if(rs("aknown")<>2) then man_close = "" end if
						end if
						
						aut_close = ""
						if(Not IsNull (rs("autoclose"))) then
							autoclose = Abs(CLng(rs("autoclose")))/60
							if(autoclose > 0) then
								aut_close = "checked"
								if(CLng(rs("autoclose")) < 0) then man_close = "checked" else man_close = "" end if
							end if
						end if

						if(Not IsNull (rs("toolbarmode"))) then
							if(rs("toolbarmode")=0) then toolbarmode = "" end if
						end if

						if(Not IsNull (rs("deskalertsmode"))) then
							if(rs("deskalertsmode")=0) then deskalertsmode = "" end if
						end if

						if(Not IsNull (rs("template_id"))) then
							template=rs("template_id")
						end if
						
						if(Not IsNull (rs("caption_id"))) then
							skin_id=rs("caption_id")
						end if
						
					end if
				end if
		        if(InStr(al_text, "<!-- printable_alert -->")<>0) then
						   print_alert="checked"
    				else print_alert=""
				end if 
			        
				'start_date = GetENGDate(start_date)
				'end_date = GetENGDate(end_date)
				

				if(tt_id <> "") then
					SQL = "SELECT name, template_text FROM text_templates WHERE id=" & CStr(tt_id)
					Set rs = Conn.Execute(SQL)
					if (Not rs.EOF) then
						al_title = rs("name")
						al_text = rs("template_text")
					end if
				end if
				
				begin_pos = InStr(al_text,top_template_start_separator)
				if (begin_pos > 0) then
					end_pos = InStr(al_text,top_template_end_separator)
					top_template = Mid(al_text, begin_pos+Len(top_template_start_separator), end_pos-begin_pos-Len(top_template_start_separator))
					al_text = Mid(al_text, end_pos+Len(top_template_end_separator))
				end if
				
				begin_pos = InStr(al_text,bottom_template_start_separator)
				if (begin_pos > 0) then
					end_pos = InStr(al_text,bottom_template_end_separator)
					bottom_template = Mid(al_text, begin_pos+Len(bottom_template_start_separator), end_pos-begin_pos-Len(bottom_template_start_separator))
					al_text = Left(al_text, begin_pos-1)
				end if
				
				if (top_template<>"" OR bottom_template<>"") then
    %>
    <script language="javascript">
        topTemplate = "<%=jsEncode(top_template)%>";
        bottomTemplate = "<%=jsEncode(bottom_template)%>";
    </script>

    <%
				end if

				if (request("preview")="2") then
    %>
    <div id="preview2" name="preview2" style="width: 98%; display: block; position: absolute;
        bottom: 10px; right: 10px; z-index: 1;">
        <table width="100%" height="32px" border="0" cellpadding="0" cellspacing="0" class="ticker_preview">
            <tr valign="middle">
                <td width="1">
                    <img src="images/ticker_logo.gif" />
                </td>
                <td valign="middle">
                    <iframe height="30px" src="<%= alerts_folder & "preview.asp?id=" & id &"&ticker=1&ack="&request("aknown")%>"
                        style='background-color: #ffffff' frameborder="0" width="100%"></iframe>
                </td>
                <td width="14px">
                    <a href="#" onclick="mypreview();">
                        <img border="0" src="images/close_ticker.gif"></a>
                </td>
            </tr>
        </table>
    </div>
    <div id="preview1">
    </div>
    <%
				else 
					if (request("preview")="1") then 
						if(fullscreen="checked") then
							preview_alert_height = "100%"
							preview_alert_width = "100%"
						else
							preview_alert_height = alert_height & "px"
							preview_alert_width = alert_width & "px"
						end if
    %>
    <iframe id="preview_iframe" scrolling="no" style="display: ; position: absolute;
        z-index: 1; background-color: #ffffff; width: <%=preview_alert_width %>; height: <%=preview_alert_height %>;"
        src="<%= alerts_folder & "/preview/minibrowsercaption.asp?new_alert="&server.urlencode(LNG_NEW_ALERT)&"&src="& server.urlencode(alerts_folder & "preview.asp?id=" & id & "&ack="&request("aknown")) %>"
        frameborder="1" class="iframe_preview"></iframe>
    <%
					end if
				end if

Set rHtmlTitle = New RegExp
With rHtmlTitle
	.Pattern = "<!-- html_title *= *(['""])(.*?)\1 *-->"
	.IgnoreCase = True
	.Global = False
End With

langsCount = 1

	html_title = ""
	Set res = rHtmlTitle.Execute(al_text)
	If(res.Count > 0) Then
		html_title = HTMLDecode(res(0).Submatches(1))
		al_text = rHtmlTitle.Replace(al_text, "")
	End If

	alerts_coll = Array(Array("", al_title, True, al_text, "", html_title))
	step_num = 1
	steps = 2


    %>
    <div id="im_description" style="display: <%if autosubmit=1 then Response.Write("block") else Response.Write("none") end if%>;
        margin: auto; height: 100%; width: 100%">
        Composing the alert message...</div>
    <table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%" <% if autosubmit = 1 then Response.Write (" style='display:none' ") end if %>>
        <tr>
            <td>
                <table id="dialog_dock" width="100%" height="100%" cellspacing="0" cellpadding="0"
                    class="main_table">
                    <tr>
                        <td width="100%" height="31" class="main_table_title">
							<%if instant_message=1 then %>
							<img src="images/menu_icons/im_20.png" alt="" width="20" height="20" border="0" style="padding-left:7px">
							<%else %>
							<img src="images/menu_icons/alert_20.png" alt="" width="20" height="20" border="0" style="padding-left:7px">
							<%end if %>
                            <a href="#" class="header_title" style="position:absolute;top:15px">
                                <%if instant_message=1 then Response.Write(LNG_INSTANT_MESSAGES) else Response.Write(LNG_ALERTS) end if%></a>
                        </td>
                    </tr>
                    <tr>
                    
  
                        <td height="100%" class="main_table_body">
                            <% if UBound(alerts_coll) >= 0 then %>
                            <div id="alert_type_tabs" style="padding: 0px;">
                                <ul>
                                    <li id="alert_type_tab"><a href="#alert_type_tab" onclick="check_tab('alert');">
                                        <%=LNG_ADD_ALERT %></a></li>
                                    <% if TICKER_CFG=1 then %>
                                    <li id="ticker_type_tab"><a href="#alert_type_tab" onclick="check_tab('ticker');">
                                        <%=LNG_ADD_TICKER %></a></li>
                                    <% end if %>
                                    <%if((alerts_arr(0)<>"" or gid=0 or ConfTGroupEnabled = 1) and Request("webplugin") <> "1") then %>
                                    <li id="rsvp_type_tab"><a href="#alert_type_tab" onclick="check_tab('rsvp');document.getElementById('print_alert').checked=false;">
                                        <%=LNG_ADD_RSVP %></a></li>
                                    <%end if%>
                                    <% if (SOCIAL_LINKEDIN=1 AND langsCount<2 and Request("webplugin") <> "1") then %>
                                    <li id="lnkdn_type_tab"><a href="#alert_type_tab" onclick="check_tab('linkedin');">
                                        <%=LNG_ADD_LINKEDIN %></a></li>
                                    <% end if %>
                                    <% if (WEBPLUGIN=1 and Request("webplugin") <> "1") then %>
                                    <li id="webplugin_type_tab"><a href="#alert_type_tab" onclick="check_tab('webplugin');">
                                        <%=LNG_DIGSIGN_TITLE %></a></li>
                                    <% end if %>
                                </ul>
                                <div id="alert_type_tab" style="padding: 0.5em 0.5em;">
                                    <% else %>
                                    <div style="padding: 10px">
                                        <div>
                                            <% end if %>
                                            <span class="work_header"></span>
                                            <!-- TinyMCE -->
                                            <form name="my_form" method="post" action="edit_alert_db.asp">
                                            <input type="hidden" name="sub1" value="">
                                            <input type="hidden" name="preview" value="">
                                            <input type="hidden" name="instant" value="<%=instant_message %>">
                                            <input type="hidden" name="autosubmit" value="<%=autosubmit %>">
                                            <input type="hidden" name="webalert" value="1" id="webalert">
                                            <input type="hidden" name="alert_campaign" value="<%=Request("alert_campaign") %>" id="alert_campaign">
                                            <input id="color_code" type="hidden" name="color_code" value="<%=color_code %>">
                                            <input id="webplugin" type="hidden" name="webplugin" value="<%=request("webplugin") %>">

                                            <%
if id <> "" then
	Response.Write "<input type='hidden' name='edit' value='1'/>"
	Response.Write "<input type='hidden' name='id' value='"&id&"'/>"
	Response.Write "<input type='hidden' name='rejectedEdit' value='" & Request("rejectedEdit") & "'>"
	Response.Write "<input type='hidden' name='shouldApprove' value='" & Request("shouldApprove") & "'>"
end if
if alerts_folder <> "" then
	Response.Write "<input type='hidden' name='alerts_folder' value='"&alerts_folder&"'/>"
end if
if text_t <> "" then
	Response.Write "<input type='hidden' name='alert_text' value='"&text_t&"'/>"
end if
if non_edit <> "" then
	Response.Write "<input type='hidden' name='non_edit' value='"&non_edit&"'/>"
end if
if tt_id <> "" then
	Response.Write "<input type='hidden' name='tt_id' value='"&tt_id&"'/>"
end if
if UBound(alerts_coll) >= 0 then
                                            %>
    <div style="width: 955px; margin: auto;">
                                                <div>
                         <% if CAMPAIGN = 1 AND Request("instant") <> "1" then %>
                                 
                        
                                                                    
                      <% if Request("alert_campaign") = "" or Request("alert_campaign") = "-1" then
                         Response.Write  "<input id='campaign_select' type='hidden' name='campaign_select' value='-1'>"
                        else
                              Response.Write "<b>" & LNG_CAMPAIGN  & ": </b>" 
                            SQL = "SELECT name FROM campaigns WHERE id = " & Request("alert_campaign")
                            Set RS2 = Conn.Execute(SQL)
                            if not RS2.EOF then
                                Response.Write "<label><b>"&HtmlEncode(RS2("name"))&"</b></label><br>"
                                Response.Write  "<input id='campaign_select' type='hidden' name='campaign_select' value='"&Request("alert_campaign")&"'>"                                
                            end if
                         end if%> 
                                                                    
                           
                       <% else
                       
                        Response.Write  "<input type='hidden' name='campaign_select' value='-1'>"
                       
                       end if
                           %>
                           <br />
                          </div>
                                                <div>
                                                    <%=LNG_STEP%>
                                                    <strong>
                                                        <%=step_num%></strong>
                                                    <%=LNG_OF%>&nbsp;<%=steps%>
                                                    : <strong>
                                                        <%=LNG_ENTER_TEXT_OF_YOUR_ALERT%>:</strong></div>
                                                <div id="top_template_div">
                                                </div>
                                            <%
	i = 0


	For Each alert in alerts_coll
		If IsArray(alert) Then
                                            %>
                                            
                                                <br />
                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                        </td>
                                                        <%if(gid = 0 OR (gid = 1 AND templates_arr(3)<>"")) then%>
                                                        <td>
                                                        </td>
                                                        <%end if%>
                                                    </tr>
                                                    <tr>
                                                        <td style="vertical-align: bottom; width: 1px; overflow: visible" rowspan="2">
                                                            <%=LNG_TITLE %>:&nbsp;&nbsp;
                                                        </td>
                                                        <td rowspan="2" style="vertical-align:bottom">
                                                            <div style="width: 312px;">
                                                                <% if ConfEnableHtmlAlertTitle = 1 then %>
                                                                <%
					cur_title = alert(5)
					if alert(5) = "" then cur_title = alert(1)
                                                                %>
                                                                <div class="html_title_div">
                                                                    <textarea id="html_title_<%=i%>" name="html_title" style="width: 310px"><%=HtmlEncode(cur_title)%></textarea></div>
                                                                <input type="hidden" id="title_<%=i%>" name="alert_title" value="<%=HTMLEncode(alert(1))%>"
                                                                    maxlength="255" style="width: 304px" />
                                                                <% else %>
                                                                <input type="text" id="title_<%=i%>" name="alert_title" value="<%=HTMLEncode(alert(1))%>"
                                                                    maxlength="255" style="width: 300px" />
                                                                <input type="hidden" id="html_title_<%=i%>" name="html_title" value="" />
                                                                <% end if %>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div align="right" style="margin-bottom: 5px;">

                                                                <script language="javascript">
                                                                    check_aknown();
                                                                    check_email();
                                                                    check_desktop();
                                                                    <%if(gid = 0 OR (gid = 1 AND alerts_arr(6)<>"")) then %> check_size(); <%end if%>
                                                                </script>
									
                                                                <div>
                                                                    <a class="preview_button" onclick="showAlertPreview()">
                                                                        <%=LNG_PREVIEW%>
                                                                        
                                                                        <% if request("webplugin") <> "1" then%>
                                                                        </a> <a class="save_button" id="A1" onclick="if(my_sub(2)) {document.my_form.submit();}">
                                                                            <% =LNG_SAVE%></a>
                                                                        <% end if %>    
                                                                    <% if(gid = 0 OR (gid = 1 AND alerts_arr(3)<>"" OR emails_arr(3)<>"")) then
                                                                    %><a class="save_and_next_button" id="A2" onclick="if(my_sub(3)) {document.my_form.submit();}"><%if instant_message=1 then Response.Write(LNG_NEXT) else Response.Write(LNG_SAVE_AND_NEXT) end if%></a>
                                                                    <%end if%>
                                                                </div>
										
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <%if(gid = 0 OR (gid = 1 AND text_templates_arr(3)<>"")) then%>
                                                            <div align="right">
                                                                <select id="text_template_select_<%=i%>" name="text_template_select" style="margin-right: 12px;
                                                                    width: 220px">
                                                                    <option value='0'>
                                                                        <%=LNG_TEXT_TEMPLATES %></option>
                                                                    <%
                                                                    
                      if ConfTGroupEnabled <> 1 then   
                         'Response.Write "DISABLE"                                           
					    SQL = "SELECT id, name FROM text_templates ORDER BY name"
					    Set RS1 = Conn.Execute(SQL)
					    do while not RS1.EOF
						    Response.Write "<option value='"&RS1("id")&"'>"&HtmlEncode(RS1("name"))&"</option>"
						    RS1.MoveNext
					    loop
					  else
					    'Response.Write "ENABLE"
		                
					    if gid<> 0 then
    					    
					        SQL = "SELECT tgroup_id FROM policy_tgroups WHERE " & policy_ids & "AND tgroup_id = 0"
    					    
					        Set RS1 = Conn.Execute(SQL)
    					    
					        if RS1.EOF then
					            SQL = "SELECT DISTINCT a.id, a.name FROM text_templates as a INNER JOIN group_message_templates as b ON a.id = b.template_id WHERE b.group_id IN (SELECT tgroup_id FROM policy_tgroups WHERE " & policy_ids & ") ORDER BY a.name"
					            Response.Write SQL
					        else
					            SQL = "SELECT id, name FROM text_templates ORDER BY name"
					        end if
					    else
					    
					        SQL = "SELECT id, name FROM text_templates ORDER BY name"
					    end if   
					    
					    
					    Set RS2 = Conn.Execute(SQL)
					    do while not RS2.EOF
						    Response.Write "<option value='"&RS2("id")&"'>"&HtmlEncode(RS2("name"))&"</option>"
						    RS2.MoveNext
					    loop 
					  end if
					   
                                                                    %>
                                                                </select>
                                                            </div>
                                                            <%end if%>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <br />
                                                <table style="width: 100%">
                                                    <tr>
                                                        <td style="width: 100%">
                                                            <textarea id="elm_<%=i%>" name="elm" rows="15" cols="80" style="width: 100%"><%=HtmlEncode(alert(3))%></textarea>
                                                        </td>
                                                        <%
												if (i = 0) then
													if with_alert_captions = 1 then
                                                        %>
                                                        <td>
                                                            <% if instant_message=1 then%>
                                                            <div id="change_color_code" onclick="showColorCodeDialog()" style="background: #<% if color_code<>"" then response.write color_code else response.write "747474" end if%>;
                                                                cursor: pointer; cursor: hand; height: 14px; width: 200px; margin: 0px 10px 10px 10px;
                                                                padding: 8px 10px; color: white; text-align: center; vertical-align: middle">
                                                                <%=LNG_CLICK_HERE_TO_CHANGE_COLOR_CODE %></div>
                                                            <%end if  
                                                             %>
                                                            <div class="tile" id="default" style="margin: 10px; padding: 10px">
                                                                <div class="hints" style="font-size: 12px; text-align: center; padding-bottom: 2px">
                                                                    <%= LNG_CLICK_TO_CHANGE_SKIN %></div>
                                                                <div style="font-size: 1px">
                                                                    <img id="skin_preview_img" src="skins/<%if (skin_id <> "") then Response.Write skin_id else Response.Write "default" end if%>/thumbnail.png" />
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <%
													end if
												else%>
													<td style="width:246px;text-align:center;vertical-align:middle">
														<a class="translate_btn" onclick="makeTranslateRequest('<%=alert(0)%>')">Translate</a>
														<script>
														function displayTranslation<%=alert(0)%>(response){
															tinyMCE.get("elm_<%=i%>").setContent(response);
														}
														function displayTitleTranslation<%=alert(0)%>(response){
															tinyMCE.get("html_title_<%=i%>").setContent(response);
														}
														</script>
													<td>
												<%end if%>
                                                    </tr>
                                                </table>
<%


if gid <> 0 then
   
    ' SQL = "SELECT count(1)  as cnt FROM policy_tgroups WHERE " & policy_ids & " AND tgroup_id = 0"
      
	'Set countSet = Conn.Execute(SQL)
	
	'canSendAll = Clng(countSet("cnt")) 
	    shouldBeReadOnly = 0
	    if alerts_arr(0)= "" and text_templates_arr(3)<>"" then ' can send templates but can not create alerts
	         shouldBeReadOnly = 1
	    end if
	    
	    
	'Response.Write SQL
else
   shouldBeReadOnly = 0
end if


 %>
         <script type="text/javascript">
                 
                 
                 var shouldBeReadOnly = parseInt("<% =shouldBeReadOnly %>", 10) == 1;                               
					                    var readOnly = { readonly: shouldBeReadOnly };                               
					                    initTinyMCE("<%=lcase(default_lng) %>", "exact", "elm_<%=i%>", function(ed){
					                        ed.onChange.add(alertContentOnChangeHandler);
					
					                    }, readOnly, newTinySettings);
				
					                    //			        alert('elm_<%=i%>');
					                    //			       tinyMCE.get('elm_<%=i%>').settings.readonly = true;
					                    //			        <% if ConfEnableHtmlAlertTitle = 1 then %>
					                    //			            tinyMCE.get('html_title_0').settings.readonly = true;
					                    //			        <% else %>
					                    //			           $("#title_0").attr('readonly', 'true');
					                    //			        <% end if %>
			        
					                    <% if ConfEnableHtmlAlertTitle = 1 then %>
                                            initTinyMCE("<%=lcase(default_lng) %>", "exact", "html_title_<%=i%>", function(ed){
                                                ed.onChange.add(function(){
                                                    titleChanged['title_<%=i%>'] = true;
                                                });
                                                var handle = true;
                                                var cleanup = function(ed)
                                                {
                                                    if(!handle) return;
                                                    handle = false;
                                                    ed.execCommand('mceCleanup', true);
                                                    handle = true;
                                                };
					
                                                ed.onSetContent.add(cleanup);
                                                ed.onNodeChange.add(cleanup);
                                                ed.onBeforeGetContent.add(cleanup);

                                                /*	var resize = function()
                                                    {
                                                        ed.execCommand('mceAutoResize', true);
                                                    };
                                                    ed.onNodeChange.add(resize);*/

                                                setTimeout(function(){
                                                    var $body = $(ed.getBody());
                                                    var $de = $(ed.getDoc().documentElement);
                                                    $body.css('overflow','hidden').attr("scroll", "no");
                                                    $de.css('overflow','hidden');
                                                    $body.css('white-space','nowrap').css('margin','0px').css('padding','2px');
                                                    if(!shouldBeReadOnly)
                                                        $('#html_title_<%=i%>_tbl').css('height', 'auto').children('tbody').children('tr.mceLast').hide();
                                                    else
                                                        $('#html_title_<%=i%>_tbl').css('height', '20px')
                                                    ed.execCommand('mceAutoResize', true);
                                                }, 1);
                                            }, {
                                                plugins : 'daautoresize',
                                                readonly: shouldBeReadOnly
					
                                            }, {
                                                theme_advanced_buttons1 : "bold,italic,underline,|,fontselect,fontsizeselect,|,forecolor",
                                                theme_advanced_buttons2 : "",
                                                theme_advanced_buttons3 : "",
                                                theme_advanced_buttons4 : "",
                                                theme_advanced_path : false,
                                                theme_advanced_resizing : false,
                                                autoresize_bottom_margin : 0,
                                                autoresize_min_height : 20,
                                                autoresize_max_height : 80,
                                                forced_root_block : false,
                                                statusbar : false,
                                                valid_elements : "span[*],font[*],i[*],b[*],u[*],strong[*],s[*],em[*]",
                                                extended_valid_elements : "",
                                                font_size_style_values : "8pt,10pt,12pt,14pt,16pt,18pt,20pt",
                                                theme_advanced_font_sizes : "8pt=8pt,10pt=10pt,12pt=12pt,14pt=14pt,16pt=16pt,18pt=18pt,20pt=20pt",
					
                                            });
					                    <% end if %>
			
                                            titleChanged['title_<%=i%>'] = <% if Request("tt_id") = "" then Response.Write "true" else Response.Write "false" %>;
					                    contentChanged['elm_<%=i%>'] = <% if Request("tt_id") = "" then Response.Write "true" else Response.Write "false" %>;
                                                </script>

                                                <%If EnableAlertLangs=1 Then%>
                                                <input type='hidden' name='lang' value='<%=HtmlEncode(alert(0))%>' />
                                                <input type='hidden' name='lang_name' value='<%=HtmlEncode(alert(4))%>' />
                                                <input type='hidden' name='lang_default' value='<%=Abs(CInt(alert(2)))%>' />
                                                <%End If%>
                                                <br />
                                                <%
			i = i + 1
		End If
	Next
                                                %>
                                                <div id="bottom_template_div">
                                                </div>
                                                <div runat="server"id="digsignInfo" class="ticker_info" style="">
                                                    <% 
                                                        =LNG_DIGSIGN_WARNING 
                                                       %>
                                                 </div>
                                                
                                                    <div id="digsign_info" class="ticker_info" style="">
                                                    <% if Request("webplugin") = "1" then
                                                       Response.Write LNG_DIGSIGN_WARNING 
                                                      end if %>
                                                    </div>
                                                
                                                
                                                <div id="ticker_info" class="ticker_info" style="display: none">
                                                    <%=LNG_TICKER_INFO%>
                                                </div>
                                                <div id="rsvp_div" style="display: none">
                                                    <table border="0">
                                                        <tr>
                                                            <td colspan="2" style="padding-left: 24px">
                                                                <b>
                                                                    <%=LNG_QUESTION_WORD%>
                                                                    #1</b>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <%=LNG_QUESTION %>:
                                                            </td>
                                                            <td>
                                                                <input type="text" name="question" id="question" size="35" value="<%=question%>" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <%=LNG_OPTION %>:
                                                            </td>
                                                            <td>
                                                                <input type="text" id="question_option1" name="question_option1" size="35" value="<%=question_option1%>" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <%=LNG_OPTION %>:
                                                            </td>
                                                            <td>
                                                                <input type="text" id="question_option2" name="question_option2" size="35" value="<%=question_option2%>" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <input id="need_second_question" name="need_second_question" type="checkbox" value="1"
                                                                    onclick="need_second_question_click()" <% if need_second_question = "1" then Response.Write " checked"%> />
                                                                <label for="need_second_question">
                                                                    <b>
                                                                        <%=LNG_QUESTION_WORD%>
                                                                        #2</b></label>
                                                            </td>
                                                        </tr>
                                                        <tr class="second_question" <% if need_second_question <> "1" then Response.Write " style=""display:none"""%>>
                                                            <td>
                                                                <%=LNG_QUESTION %>:
                                                            </td>
                                                            <td>
                                                                <input type="text" name="second_question" id="second_question" size="35" value="<%=second_question%>" />
                                                            </td>
                                                        </tr>
                                                        <tr class="second_question" <% if need_second_question <> "1" then Response.Write " style=""display:none"""%>>
                                                            <td>
                                                                <%=LNG_QUESTION_TYPE %>:
                                                            </td>
                                                            <td>
                                                                <%=LNG_INPUT_ANSWER %>
                                                                <i>(<%=LNG_INPUT_ANSWER_DESC %>)</i>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <br />
                                                </div>
                                                <% if SOCIAL_LINKEDIN = 1 then %>
                                                <div id="linkedin_div" style="display: none">
                                                    <div id="linkedin_settings_hint" style="padding-bottom: 5px; color: #ff0000;">
                                                        Manage LinkedIn settings to enable the feature</div>
                                                    <div id="linkedInDiv" style="padding-bottom: 5px">

                                                        <script type="IN/Login">Currently logged as <strong><?js= firstName ?> <?js= lastName ?></strong></script>

                                                        <br />
                                                        <br />
                                                        <a id="shareAPI" style="display: none" onclick="my_sub(4);" href="#">
                                                            <%= LNG_SHARE_ON_LINKEDIN %></a>
                                                    </div>
                                                    <br />
                                                </div>
                                                <% end if %>
                                               
                                                <div id="elements_to_display" style="display: none">
                                                    <form method="post" action="settings_system_configuration.asp" style="width: 100%">
                                                    <table border="0" style="width: 100%">
                                                        <tr>
                                                            <td>
                                                            
                                                                <div id="accordion" style="width: 100%">
                                                                                                                                
                                                                 <% if request("webplugin")<> "1" then %>
                                                                    <h3>
                                                                        <%= LNG_ALERT_SETTING_ACC %></h3> <!--<p>accordion start</p>-->
                                                                      <table border="0" style="width: 100%">
                                                                        <tr>
                                                                            <td>
                                                                                <div class="checkbox_div" id="urgent_div">
                                                                                    <input type="checkbox" id="urgent" name="urgent" value="1" <%=urgent %>>
                                                                                    <label for="urgent">
                                                                                        <%=LNG_URGENT_ALERT %></label>
                                                                                    <img src="images/help.png" title="<%=LNG_URGENT_ALERT_DESC %>." />
                                                                                    <br />
                                                                                </div>
                                                                                <div class="checkbox_div" id="aknown_div">
                                                                                    <input type="checkbox" id="aknown" name="aknown" onclick="check_aknown();" value="1"
                                                                                        <%=aknown %>>
                                                                                    <label for="aknown">
                                                                                        <%=LNG_ACKNOWLEDGEMENT %></label>
                                                                                       
                                                                                    <img src="images/help.png" title="<%=LNG_ACKNOWLEDGEMENT_DESC %>." />
                                                                                    <br />
                                                                                </div>
                                                                                <div class="checkbox_div" id="unobtrusive_div">
                                                                                    <input type="checkbox" id="unobtrusive" name="unobtrusive" onclick="check_aknown();"
                                                                                        value="1" <%=unobtrusive %>>
                                                                                    <label for="unobtrusive">
                                                                                        <%=LNG_UNOBTRUSIVE %></label>
                                                                                    <img src="images/help.png" title="<%=LNG_UNOBTRUSIVE_HINT %>." />
                                                                                    <br />
                                                                                </div>
                                                                                <div class="checkbox_div" id="self_deletable_div">
                                                                                    <input type="checkbox" id="self_deletable" name="self_deletable" value="1" <%=self_deletable %>>
                                                                                    <label for="self_deletable">
                                                                                        <%=LNG_SELF_DELETABLE %></label>
                                                                                    <img src="images/help.png" title="<%=LNG_SELF_DELETABLE_HINT %>." />
                                                                                    <br />
                                                                                </div>
                                                                                <div class="checkbox_div" id="aut_close_div">
                                                                                    <input type="checkbox" id="aut_close" name="aut_close" onclick="check_aknown();"
                                                                                        value="1" <%=aut_close %>>
                                                                                    <label for="aut_close">
                                                                                        <%=LNG_AUTOCLOSE_IN %></label>
                                                                                    <input type="text" size="3" name="autoclose" id="autoclose" onkeypress="return check_size_input(this, event);"
                                                                                      onkeyup="isright(this)"  value="<%=autoclose %>" />
                                                                                    <%=LNG_MINUTES %>
                                                                                    <img src="images/help.png" title="<%=LNG_AUTOCLOSE_TITLE %>." />
                                                                                    <span id="man_close_div">
                                                                                        <input type="checkbox" id="man_close" name="man_close" value="1" <% Response.Write man_close %>>
                                                                                        <label for="man_close">
                                                                                            <%=LNG_ALLOW_MANUAL_CLOSE %></label>
                                                                                        <img src="images/help.png" title="<%=LNG_ALLOW_MANUAL_CLOSE_DESC %>." />
                                                                                    </span>
                                                                                    <br />
                                                                                </div>
                                                                                <div class="checkbox_div" id="lifeTimeAlert">
                                                                                    <input type="checkbox" id="lifetimemode" name="lifetimemode" value="1" <%=lifetimemode %>
                                                                                        onclick="check_lifetime();">
                                                                                    <label for="lifetimemode">
                                                                                        <%=LNG_LIFETIME_IN %></label>
                                                                                    <%=GetLifetimeControl(lifetime, "lifetime") %>&nbsp;<%=LNG_TIME_EXPIRED_CANT_RECEIVED %><!--img src="images/help.png" title="<%=LNG_LIFETIME_TITLE %>."/-->
                                                                                    </span>
                                                                                    <br />
                                                                                </div>
                                                                                <div class="checkbox_div" id="print_div">
                                                                                    <input type="checkbox" id="print_alert" name="print_alert" value="1" <%=print_alert %>>
                                                                                    <label for="print_alert">
                                                                                        <%=LNG_PRINT_ALERT %></label>
                                                                                    <img src="images/help.png" title="<%=LNG_PRINT_HINT %>." /><br />
                                                                                </div>
                                                                                <%
				if(ConfToolbarMode=1 AND ConfDeskalertsMode=1) then
                                                                                %>
                                                                                <div class="checkbox_div">
                                                                                    <input type="checkbox" id="toolbarmode" name="toolbarmode" value="1" <%= toolbarmode %>>
                                                                                    <label for="toolbarmode">
                                                                                        Alert for toolbar</label><br />
                                                                                </div>
                                                                                <div class="checkbox_div">
                                                                                    <input type="checkbox" id="deskalertsmode" name="deskalertsmode" value="1" <%= deskalertsmode %>>
                                                                                    <label for="deskalertsmode">
                                                                                        Alert for Deskalerts Application</label><br />
                                                                                </div>
                                                                                <%
				else
					if(ConfToolbarMode=1) then
                                                                                %>
                                                                                <input type="hidden" name="toolbarmode" value="1">
                                                                                <%
					end if
					if (ConfDeskalertsMode=1) then
                                                                                %>
                                                                                <input type="hidden" name="deskalertsmode" value="1">
                                                                                <%
					end if
				end if
                                                                                %>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                <% end if %>
                                                                

                                                                <% if request("webplugin")<> "1" then %>
                                                                    <h3><%= LNG_SCHEDULE_ALERT %></h3>
                                                                   
                                                                    <table border="0" style="width: 100%" id="fuckingTable">
                                                                        <tr>
                                                                            <td>
                                                                                <div class="checkbox_div" id="scheduleAlert">
                                                                                <% end if %>
                                                                                    <input type="checkbox" <%if request("webplugin") = "1" then Response.Write("style='display:none;'") end if%> id="recurrence_checkbox" name="recurrence" value="1" <%=recurrence %>
                                                                                        onclick="check_recurrence(); <%  %>">
                                                                                         <% if request("webplugin")<> "1" then %>
                                                                                    <label for="recurrence_checkbox">
                                                                                        <%=LNG_SCHEDULE_ALERT %></label>
                                                                                    <img src="images/help.png" title="<%=LNG_SCHEDULE_HINT %>." /><br />
                                                                                </div>
                                                                 <%end if %>
                                                                                <div id="recurrence_div" <%if request("webplugin") = "1" then Response.Write("style='display:none;'") end if%> >
                                                                                    <%
				                                                                    recurrenceLoadData(id)
				                                                                     recurrenceForm()
                                                                                    %>               
                                                                                </div>
                                                                 <% if request("webplugin")<> "1" then %>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
					                                             <% end if %>
					                                             
					                                             <% if request("webplugin") <> "1" then%>
					                                            
                                                                    <h3>
                                                                        <%= LNG_ALERT_APPER_ACC %></h3>
                                                                 <% end if %>
                                                                    <table border="0" style="width: 100%" id="fuck">
                                                                        <tr>
                                                                            <td style="width: 200px;">
                                                                                <div id="buttons_to_display" style="display: none">
                                                                                    <%
		''			end if
		''			fl2=1
		''		end if
		''		set fs2=nothing
				if(true) then
		''		
                                                                                    %>
                                                                                    <%if(alerts_arr(0)<>"" or gid=0 or ConfTGroupEnabled = 1) then%>
                                                                                   <div id="desktopCheckLabel">
                                                                                    <input type="checkbox" id="desktop_check"  <%if request("webplugin") = "1" then Response.Write("style='display:none;'") end if%> name="desktop" <% =desktop %> value="1"
                                                                                        onclick="check_desktop();" <% if(gid = 1 AND alerts_arr(0)="") then%> disabled  %>
                                                                                        <%end if%> />
                                                                                        
                                                                                        <% if request("webplugin") <> "1" then %>
                                                                                    <label for="desktop_check">
                                                                                        <%=LNG_DESKTOP_ALERT %></label><br />
                                                                                        </div>
                                                                                        <% end if %>
                                                                                    <%end if%>
                                                                                    <div id="ticker_div">
                                                                                        <div id="size_message">
                                                                                        </div>
                                                                                        <table border="0" cellspacing="0" cellpadding="0" style="width: auto;" id="tableAlertAppear">
                                                                                            <tr>
                                                                                                <td valign="top" style="width: 110px; display: none">
                                                                                                    <fieldset class="reccurence_pattern" style="height: 105px">
                                                                                                        <legend>
                                                                                                            <%=LNG_ALERT_TYPE %></legend>
                                                                                                        <% 
				else
                                                                                                        %>
                                                                                                        <input type='hidden' name='desktop' value='1' />
                                                                                                        <input type='checkbox' value='1' id='desktop_check' checked style='display: none' />
                                                                                                        <div id="size_message">
                                                                                                        </div>
                                                                                                        <table border="0" cellspacing="0" cellpadding="0">
                                                                                                            <tr>
                                                                                                                <td style="width: 110px; display: none">
                                                                                                                    <fieldset class="reccurence_pattern" style="height: 105px">
                                                                                                                        <legend>
                                                                                                                            <%=LNG_ALERT_TYPE %></legend>
                                                                                                                        <%
				                                                                                                                    end if

				                                                                                                                    alert_type_select1=""
				                                                                                                                    alert_type_select2=""
				                                                                                                                    alert_type_select3=""
                                                                                                                                    alert_type_select4=""
                                                                                                                                    alert_type_select5=""
                                                                                                                                    
				                                                                                                                    if(rsvp="checked") then
					                                                                                                                    alert_type_select3="checked"
				                                                                                                                    elseif(ticker="checked") then
					                                                                                                                    alert_type_select2="checked"
				                                                                                                                    elseif(linkedin="checked") then
					                                                                                                                    alert_type_select4="checked"
				                                                                                                                    elseif(webplugin_ch="checked") then
					                                                                                                                    alert_type_select5="checked"
				                                                                                                                    else
					                                                                                                                    alert_type_select1="checked"
				                                                                                                                    end if
                                                                                                                                                            %>
                                                                                                                        <br />
                                                                                                                        <div style="padding-left: 5px;">
                                                                                                                            <table border="0" cellpadding="1" cellspacing="1">
                                                                                                                                <tr>
                                                                                                                                    <td valign="top">
                                                                                                                                        <input type="radio" id="alert_type_alert" name="alert_type" value="0" <%= alert_type_select1 %>
                                                                                                                                            <%if(gid = 0 OR (gid = 1 AND alerts_arr(6)<>"")) then %> onclick="check_size()"
                                                                                                                                            <%end if%>>
                                                                                                                                    </td>
                                                                                                                                    <td valign="top">
                                                                                                                                        <label for="alert_type_alert">
                                                                                                                                            <%=LNG_POPUP %></label>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td valign="top">
                                                                                                                                        <input type="radio" id="alert_type_ticker" name="alert_type" value="1" <%= alert_type_select2 %>
                                                                                                                                            <%if(gid = 0 OR (gid = 1 AND alerts_arr(6)<>"") OR (DEMO = 1 and IsSuperAccount(uid))) then %> onclick="check_size()"
                                                                                                                                            <%end if%>>
                                                                                                                                    </td>
                                                                                                                                    <td valign="top">
                                                                                                                                        <label for="alert_type_ticker">
                                                                                                                                            <%=LNG_SCROLL_IN_TICKER %></label>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td valign="top">
                                                                                                                                        <input type="radio" id="alert_type_rsvp" name="alert_type" value="2" <%= alert_type_select3 %>
                                                                                                                                            <%if(gid = 0 OR (gid = 1 AND alerts_arr(6)<>"") OR (DEMO = 1 and IsSuperAccount(uid))) then %> onclick="check_size()"
                                                                                                                                            <%end if%>>
                                                                                                                                    </td>
                                                                                                                                    <td valign="top">
                                                                                                                                        <label for="alert_type_rsvp">
                                                                                                                                            RSVP</label>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td valign="top">
                                                                                                                                        <input type="radio" id="alert_type_linkedin" name="alert_type" value="3" <%= alert_type_select4 %>
                                                                                                                                            <%if(gid = 0 OR (gid = 1 AND alerts_arr(6)<>"") OR (DEMO = 1 and IsSuperAccount(uid))) then %> onclick="check_size()"
                                                                                                                                            <%end if%>>
                                                                                                                                    </td>
                                                                                                                                    <td valign="top">
                                                                                                                                        <label for="alert_type_linkedin">
                                                                                                                                            LinkedIN</label>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td valign="top">
                                                                                                                                        <input type="radio" id="alert_type_webplugin" name="alert_type" value="4" <%= alert_type_select5 %>
                                                                                                                                            <%if(gid = 0 OR (gid = 1 AND alerts_arr(6)<>"")  OR (DEMO = 1 and IsSuperAccount(uid)))  then %> onclick="check_size()"
                                                                                                                                            <%end if%>>
                                                                                                                                    </td>
                                                                                                                                    <td valign="top">
                                                                                                                                        <label for="alert_type_webplugin">
                                                                                                                                            <%=LNG_WEBPLUGIN %></label>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </table>
                                                                                                                        </div>
                                                                                                                    </fieldset>
                                                                                                                </td>
                                                                                                                <td valign="top" <%if (gid = 1 AND alerts_arr(6)="") OR ConfDisableAlertSizing=1 then %>
                                                                                                                    style="display: none;" <%end if%>>
                                                                                                                    <%if FULLSCREEN_CFG=1 and Request("webplugin") <> "1" then size_block_height=120 else size_block_height=100 end if %>
                                                                                                                    <fieldset class="reccurence_pattern" style="height: <%=size_block_height%>px">
                                                                                                                        <legend>
                                                                                                                            <%=LNG_SIZE %></legend>
                                                                                                                        <%
				                                                                                                            position_disabled = ""
				                                                                                                            size_disabled = ""
				                                                                                                            if(fullscreen="checked") then ' check here
					                                                                                                            size_select2="checked"
					                                                                                                            size_disabled = "disabled"	
					                                                                                                            position_disabled = "disabled"
				                                                                                                            else
					                                                                                                            size_select1="checked"
				                                                                                                            end if
                                                                                                                        %>
                                                                                                                        <div id="size_div" style="padding-left: 10px;">
                                                                                                                            <table border="0">
                                                                                                                                <tr>
                                                                                                                                    <% if FULLSCREEN_CFG=1 then %>
                                                                                                                                    <td valign="middle">
                                                                                                                                        <input type="radio" id="size1" name="fullscreen" value="0" <%= size_select1 %> onclick="check_message()">
                                                                                                                                    </td>
                                                                                                                                    <% else %>
                                                                                                                                    <td valign="middle">
                                                                                                                                        <input type="hidden" id="size1" name="fullscreen" value="0">
                                                                                                                                    </td>
                                                                                                                                    <% end if %>
                                                                                                                                    <td valign="middle">
                                                                                                                                        <%=LNG_WIDTH %>:
                                                                                                                                    </td>
                                                                                                                                    <td valign="middle">
                                                                                                                                        <input type="text" name="alert_width" id="al_width" <%=size_disabled %> size="4"
                                                                                                                                            value="<%=alert_width %>" onkeypress="return check_size_input(this, event);"
                                                                                                                                            onkeyup="check_value(this)">
                                                                                                                                    </td>
                                                                                                                                    <td valign="middle">
                                                                                                                                        <%=LNG_PX %>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td>
                                                                                                                                        &nbsp;
                                                                                                                                    </td>
                                                                                                                                    <td valign="middle">
                                                                                                                                        <%=LNG_HEIGHT %>:
                                                                                                                                    </td>
                                                                                                                                    <td valign="middle">
                                                                                                                                        <input type="text" size="4" name="alert_height" <%=size_disabled %> onkeypress="return check_size_input(this, event);"
                                                                                                                                            onkeyup="check_value(this)" id="al_height" value="<%=alert_height %>">
                                                                                                                                    </td>
                                                                                                                                    <td valign="middle">
                                                                                                                                        <%=LNG_PX %>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                
                                                                                                                                <% if request("webplugin") <> "1" then %>
                                                                                                                                <tr>
                                                                                                                                    <td>
                                                                                                                                        &nbsp;
                                                                                                                                    </td>
                                                                                                                                    <td valign="middle" colspan="3">
                                                                                                                                        <input type="checkbox" id="resizable" name="resizable" value="1" <%=resizable %>>
                                                                                                                                        <label for="resizable">
                                                                                                                                            <%=LNG_RESIZABLE %></label>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <% end if %>
                                                                                                                                <% if FULLSCREEN_CFG=1 then %>
                                                                                                                                <tr>
                                                                                                                                    <td valign="middle">
                                                                                                                                        <input type="radio" id="size2" name="fullscreen" value="1" <%= size_select2 %> onclick="check_message()">
                                                                                                                                    </td>
                                                                                                                                    <td valign="middle" colspan="3">
                                                                                                                                        <label for="size2">
                                                                                                                                            <%=LNG_FULLSCREEN %></label><br />
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <% end if %>
                                                                                                                            </table>
                                                                                                                        </div>
                                                                                                                    </fieldset>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                        <div id="shit"></div>
                                                                                                        
                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                            <td valign="middle" style="height: 130px; width: 200px;">
                                                                                <%if (alerts_arr(0)<>"" or gid=0 or ConfTGroupEnabled = 1)  then%>
                                                                                <fieldset id="position_fieldset" class="reccurence_pattern" style="height:105px;<% if request("webplugin") = "1" then response.write "display:none;" end if%>">
                                                                                    <legend>
                                                                                        <%=LNG_POSITION %></legend>
                                                                                    <div id="position_div">
                                                                                        <table border="0" cellspacing="0" cellpadding="5" width="100%">
                                                                                            <tr>
                                                                                                <td valign="middle" style="text-align: center">
                                                                                                    <input type="radio" <%= position_disabled %> name="position" value="2" <% if (position="2") then response.write "checked" end if %> />
                                                                                                </td>
                                                                                                <td>
                                                                                                </td>
                                                                                                <td valign="middle" style="text-align: center">
                                                                                                    <input type="radio" <%= position_disabled %> name="position" value="3" <% if (position="3") then response.write "checked" end if %> />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                </td>
                                                                                                <td valign="middle" style="text-align: center">
                                                                                                    <input type="radio" <%= position_disabled %> name="position" value="4" <% if (position="4") then response.write "checked" end if %> />
                                                                                                </td>
                                                                                                <td>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td valign="middle" style="text-align: center">
                                                                                                    <input type="radio" <%= position_disabled %> name="position" value="5" <% if (position="5") then response.write "checked" end if %> />
                                                                                                </td>
                                                                                                <td>
                                                                                                </td>
                                                                                                <td valign="middle" style="text-align: center">
                                                                                                    <input type="radio" <%= position_disabled %> name="position" value="6" <% if (position="6") then response.write "checked" end if %> />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </fieldset>
                                                                                <% if TICKER_PRO = 1 and Request("webplugin") <> "1" then %>
                                                                                <fieldset id="ticker_position_fieldset" class="reccurence_pattern" style="height: 135px;
                                                                                    display: none">
                                                                                    <legend>
                                                                                        <%=LNG_POSITION %></legend>
                                                                                    <div id="ticker_position_div">
                                                                                        <table border="0" cellspacing="0" cellpadding="5" width="100%">
                                                                                            <tr>
                                                                                                <td valign="middle" style="text-align: center">
                                                                                                    <input type="radio" <%= position_disabled %> name="ticker_position" value="7" <% if (ticker_position="7") then response.write "checked" end if %> />
                                                                                                </td>
                                                                                                <td>
																									<span><%=LNG_TOP %></span>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td valign="middle" style="text-align: center">
                                                                                                    <input type="radio" <%= position_disabled %> name="ticker_position" value="8" <% if (ticker_position="8") then response.write "checked" end if %> />
                                                                                                </td>
                                                                                                <td>
																									<span><%=LNG_MIDDLE %></span>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td valign="middle" style="text-align: center">
                                                                                                    <input type="radio" <%= position_disabled %> name="ticker_position" value="9" <% if (ticker_position="9") then response.write "checked" end if %> />
                                                                                                </td>
                                                                                                <td>
																									<span><%=LNG_BOTTOM %></span>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td valign="middle" style="text-align: center">
                                                                                                    <input type="radio" <%= position_disabled %> name="ticker_position" value="D" <% if (ticker_position="D") then response.write "checked" end if %> />
                                                                                                </td>
                                                                                                <td>
																									<span><%=LNG_DOCKED_AT_BOTTOM %></span><img style="margin:0px 5px" src="images/help.png" title="<%=LNG_DOCKED_TICKER_HINT %>." />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </fieldset>
																					
                                                                                <% end if %>
                                                                                <%if TICKER_PRO<>1 and Request("webplugin") <> "1" then %>
																						<input type="hidden" name="ticker_position" value="D" />
																					<%end if %>
                                                                                <% end if %>
                                                                            </td>
                                                                            <td>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                  
                                                                    <% 
                                                                    if (isEmpty(TEXT_TO_CALL_EXPIRED)) then
																		TEXT_TO_CALL_EXPIRED=0
																	end if
																	set fs=Server.CreateObject("Scripting.FileSystemObject")
																	if (fs.FileExists(Server.MapPath("8gs6anv5e43eyuq48.key"))=true AND TEXT_TO_CALL_EXPIRED=0) then
																		TEXT_TO_CALL_OK=1
																	end if
																	set fs=nothing
																	set fs2=Server.CreateObject("Scripting.FileSystemObject")
																	if fs2.FileExists(Server.MapPath("m93sndf8a3z723h85.key"))=true then
																		EM_OK=1
																	end if
																	set fs2=nothing
																	if (isEmpty(SMS_EXPIRED)) then
																		SMS_EXPIRED=0
																	end if
																	dim fs
																	set fs=Server.CreateObject("Scripting.FileSystemObject")
																	if (fs.FileExists(Server.MapPath("3sdfm98a3nz73h825.key"))=true AND SMS_EXPIRED=0) then
																		SMS_OK=1
																	end if
                                                                    if Request("webplugin") <> "1" and (SOCIAL_MEDIA=1 or BLOG=1 or (SMS_OK=1 and (gid = 0 OR (gid = 1 AND sms_arr(3)<>""))) or (TEXT_TO_CALL_OK=1 and (gid = 0 OR (gid = 1 AND text_to_call_arr(3)<>""))) or (EM_OK=1 and (gid = 0 OR (gid = 1 AND emails_arr(3)<>"")))) then %> 
                                                                    <h3>
                                                                        <%= LNG_MES_CHAN_ACC %></h3><!--fucking last element 4-->                                                                      
                                                                       
                                                                       
                                                                    <table border="0" style="width: 100%">
                                                                        <tr>
                                                                            <td>
                                                                             <div id="non_rsvp">
                                                                                    <% if SOCIAL_MEDIA=1 then %>
                                                                                    <div style="padding: 0px 0px 8px 0px">
                                                                                        <input type="checkbox" id="socialMedia" name="socialMedia" value='1' disabled onclick=""
                                                                                            <%= socialMedia%> /><label for="socialMedia">
                                                                                                <%=LNG_TWEET %></label>
                                                                                        <img src="images/help.png" title="<%=LNG_TWEET_HINT %>." />
                                                                                        <div id="social_media_settings_hint" value='1' style="display: none; font-size: 10px;
                                                                                            color: #ff0000;">
                                                                                            Manage Twitter settings to enable the feature</div>
                                                                                    </div>
                                                                                    <% end if %>
                                                                                    <% if BLOG=1 then %>
                                                                                    <div style="padding: 0px 0px 5px 0px">
                                                                                        <% if DEMO = 1 then %>
                                                                                        <input type="checkbox" id="blogPost" name="blogPost" value='1' disabled title="Demo version is not configured to send Blog post."
                                                                                            <%= blogPost%> /><label for="blogPost" title="Demo version is not configured to send Blog post.">
                                                                                                <%=LNG_BLOG_POST %></label>
                                                                                        <% else %>
                                                                                        <input type="checkbox" id="blogPost" name="blogPost" value='1' disabled onclick="check_blogpost()"
                                                                                            <%= blogPost%> /><label for="blogPost">
                                                                                                <%=LNG_BLOG_POST %></label>
                                                                                        <img src="images/help.png" title="<%=LNG_BLOGPOST_HINT %>." />
                                                                                        <div id="blogPost_settings_hint" value='1' style="display: none; font-size: 10px;
                                                                                            color: #ff0000;">
                                                                                            Manage Blog settings to enable the feature</div>
                                                                                        <% end if %>
                                                                                    </div>
                                                                                    <% end if %>
                                                                                    <%
				                                                set fs=nothing
				                                                fl2=0
				                                                if (TEXT_TO_CALL_OK=1) then
					                                                if(gid = 0 OR (gid = 1 AND text_to_call_arr(3)<>"")) then 
                                                                                    %>
                                                                                    <div style="padding: 0px 0px 3px 0px">
                                                                                        <% if DEMO = 1 then %>
                                                                                        <input type='checkbox' id='text_to_call' name='text_to_call' value='' disabled title="Demo version is not configured to send Text To Call." /><label
                                                                                            for="text_to_call" title="Demo version is not configured to send Text to Call.">
                                                                                            <%=LNG_TEXT_TO_CALL_ALERT %></label><br />
                                                                                        <% else %>
                                                                                        <input type='checkbox' id='text_to_call' name='text_to_call' value='1' <%= text_to_call %> /><label
                                                                                            for="text_to_call">
                                                                                            <%=LNG_TEXT_TO_CALL_ALERT %></label>&nbsp;<br />
                                                                                        <% end if %>
                                                                                    </div>
                                                                                    <%
					                                                end if
					                                                fl2=1
				                                                end if
                                                                                    %>
                                                                                </div>
                                                                            </td>
                                                                            <td valign="top">
                                                                                <%
                                            				
				                                            if EM_OK=1 then
					                                            if(gid = 0 OR (gid = 1 AND emails_arr(3)<>"")) then 
                                                                                %>
                                                                                <input type='checkbox' name='email' id="email_check" value='1' onclick="check_email();"
                                                                                    <%if(gid = 1 AND alerts_arr(0)="") then %>checked <%end if%><%= HtmlEncode(email) %> /><label
                                                                                        for="email_check">
                                                                                        <%=LNG_EMAIL_ALERT %></label><br />
                                                                                <div id="email_div" style="height: 64px; padding: 0px">
                                                                                    <fieldset class="i" style="height: 40px; width: 190px; padding: 5px">
                                                                                        <%=LNG_SENDER_NAME %>:<br />
                                                                                        <input type="text" name="email_sender" value="<%= HtmlEncode(email_sender) %>" style="width: 180px" />
                                                                                    </fieldset>
                                                                                </div>
                                                                                <% 
																end if
															end if
                                                                                %>
                                                                            </td>
                                                                            <td valign="top">
                                                                                <%
				                                            fl2=0
				                                           if SMS_OK=1 then
					                                            if(gid = 0 OR (gid = 1 AND sms_arr(3)<>"")) then 
                                                                                %>
                                                                                <div style="padding: 0px 0px 3px 0px">
                                                                                    <% if DEMO = 1 then %>
                                                                                    <input type='checkbox' id='sms' name='sms' value='' disabled title="Demo version is not configured to send SMS." /><label
                                                                                        for="sms" title="Demo version is not configured to send SMS.">
                                                                                        <%=LNG_SMS_ALERT %></label><br />
                                                                                    <% else %>
                                                                                    <input type='checkbox' id='sms' name='sms' value='1' <%= sms %> /><label for="sms">
                                                                                        <%=LNG_SMS_ALERT %></label><br />
                                                                                    <a class="preview_sms_button">
                                                                                        <%=LNG_PREVIEW_SMS%></a><br />
                                                                                    <% end if %>
                                                                                </div>
                                                                                <%
					                                            end if
					                                            fl2=1
				                                            end if
                                            				
                                                                                %>
                                                                            </td>
                                                                            <% if YAMMER_CFG=1 then%>
                                                                            <td valign="top">
                                                                               <div style="padding: 0px 0px 3px 0px">
                                                                                    <button id="yammerShare" class="yammerShare"><%=LNG_YAMMER_SHARE %></button>
                                                                                    <span style="display:none" id="yammerNotification"><%= LNG_YAMMER_WARNING%></span>
                                                                                </div>
                                                                            </td>
                                                                            <% end if %>
                                                                        </tr>
                                                                    </table>
                                                                   
                                                             <% end if %>
                                                                   <%if(MOBILE_SEND=1 and Request("webplugin") <> "1") then %>
                                                                    <h3 id="deviceItem">Device type</h3>
                                                                                                                                                                                                                                                                           
                                                                   
                                                                    <div id="radio_div">                                                                                                                                                                                                         
                                                                                                                                                                                                                                                  
                                                                  
                                                                    
                                                                     <% if(alert_dev="1") then%> 
                                                                     
                                                                    <input type="radio" name="dev" value="3" id="Radio1" onclick="showAppearance()"/> All devices
                                                                    <input type="radio" name="dev" value="1" id="Radio2" onclick="showAppearance()" checked/> Desktop
                                                                    <input type="radio" name="dev" value="2" id="Radio3" onclick="hideAppearance()" /> Mobile
                                                                    
                                                                      <% elseIf(alert_dev="2") then%> 
                                                                     
                                                                    <input type="radio" name="dev" value="3" id="Radio4" onclick="showAppearance()"/> All devices
                                                                    <input type="radio" name="dev" value="1" id="Radio5" onclick="showAppearance()"/> Desktop
                                                                    <input type="radio" name="dev" value="2" id="Radio6" onclick="hideAppearance()" checked/> Mobile
                                                                    
                                                                    <% else%> 
                                                                     <input type="radio" name="dev" value="3" id="All" onclick="showAppearance()" checked/> All devices
                                                                     <input type="radio" name="dev" value="1" id="Desktop" onclick="showAppearance()"/> Desktop
                                                                     <input type="radio" name="dev" value="2" id="Mobile" onclick="hideAppearance()" /> Mobile
                                                                     
                                                                      <%end if %>
                                                                   
                                                                    </div>
                                                                    
                                                                   <%else %>
                                                                    <input type="hidden" name="dev" value="3" />
                                                                   <%end if %>
                                                                   
                                                                 
                                                                </div>
                                                   
                                                            </td>                                                             
                                                        </tr>
                                                    </table>
                                                    <div align="right" style="margin-top: 10px;">

                                                        <script language="javascript">
                                                            check_aknown();
                                                            check_email();
                                                            check_desktop();
                                                            <%if(gid = 0 OR (gid = 1 AND alerts_arr(6)<>"")) then %> check_size(); <%end if%>
                                                        </script>

                                                        <div>
                                                            <a class="preview_button" onclick="showAlertPreview()">
                                                                <%=LNG_PREVIEW%></a>
                                                                <% if request("webplugin") <> "1" then %>
                                                                 <a class="save_button" id="main_buttons_save" onclick="if(my_sub(2)) {document.my_form.submit();}">
                                                                    <%=LNG_SAVE%></a>
                                                                    <% end if %>
                                                            <% if(gid = 0 OR (gid = 1 AND alerts_arr(3)<>"" OR emails_arr(3)<>"")) then
                                                            %><a class="save_and_next_button" id="main_buttons_next" onclick="if(my_sub(3)) {document.my_form.submit();}"><%if instant_message=1 then Response.Write(LNG_NEXT) else Response.Write(LNG_SAVE_AND_NEXT) end if%></a>
                                                            <%end if%>
                                                        </div>
                                                    </div>
                                                    </form>
                                                    <table border="0" width="100%">
                                                        <tr>
                                                            <td width="600" valign="top">
                                                                <br />
                                                                <%if(gid = 0 OR (gid = 1 AND templates_arr(3)<>"")) then%>
                                                                <table style="display: none">
                                                                    <tr>
                                                                        <td>
                                                                            <%=LNG_SELECT_TEMPLATE %>:
                                                                            <select id="template_select" name="template" width="40" style="width: 150px">
                                                                                <%
									SQL = "SELECT id, name FROM templates"
									Set RS1 = Conn.Execute(SQL)
									do while not RS1.EOF
										if(clng(template)=clng(RS1("id"))) or (template="" and clng(RS1("id"))=1) then 
											Response.Write "<option value='"&RS1("id")&"' selected>"&HtmlEncode(RS1("name"))&"</option>"
										else
											Response.Write "<option value='"&RS1("id")&"'>"&HtmlEncode(RS1("name"))&"</option>"
										end if
										RS1.MoveNext
									loop
                                                                                %>
                                                                            </select>
                                                                        </td>
                                                                        <td>
                                                                            <div id="edit_template_button" class="jquery_button" style="width: auto">
                                                                                <%=LNG_EDIT%></div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <%end if%>
                                                                <%
					if (skin_id<>"") then
						SQL = "SELECT file_name from alert_captions WHERE id='"& skin_id &"'"
						skinFileName = "skins/"&skin_id&"/version/alertcaption.html"
						Set RS = Conn.Execute(SQL)
						if Not RS.EOF then
							skinFileName = RS("file_name")
						end if
					end if
                                                                %>
                                                                <input type="hidden" id="skin_id" name="skin_id" width="40" value="<%=skin_id%>"
                                                                    filename="<%=HtmlEncode(skinFileName)%>" style="width: 150px" />
                                                </div>
                        </td>
                    </tr>
                </table>
                <%
Else
                %>
                <div>
                    <%=LNG_STEP %><strong>1</strong>
                    <%=LNG_OF %>
                    3 : <strong>
                        <%=LNG_CHOOSE_LANGUAGES %>:</strong></div>
                <br />

                <script type="text/javascript">
                    function select_all_objects() {
                        var status = document.getElementById('select_all').checked;
                        var field = document.getElementsByName('alert_lang');
                        for (i = 0; i < field.length; i++)
                            field[i].checked = status;
					                    }
                   
                </script>

                <table border="0">
                    <tr>
                        <td>
                            <table height="100%" cellspacing="0" cellpadding="3" class="data_table">
                                <tr class="data_table_title">
                                    <td width="10">
                                        <input type='checkbox' name='select_all' id='select_all' onclick='select_all_objects();'>
                                    </td>
                                    <td class="table_title">
                                        <%=LNG_LANGUAGE%>
                                    </td>
                                </tr>
                                <%
	i=0
	Set RS_langs = Conn.Execute("SELECT name, abbreviatures, is_default FROM alerts_langs ORDER BY is_default DESC, name")
	Do While Not RS_langs.EOF
		Response.Write "<tr><td>"
		If(RS_langs("is_default")) Then
			Response.Write "<input type='checkbox' id='alert_lang_"&i&"' disabled='disabled' checked='checked' />"
			Response.Write "<input type='hidden' id='alert_hidden' name='alert_lang' value='"&HTMLEncode(RS_langs("abbreviatures"))&"' />"
		Else
			Response.Write "<input type='checkbox' name='alert_lang' id='alert_lang_"&i&"' value='"&HTMLEncode(RS_langs("abbreviatures"))&"'/>"
		End If
		Response.Write "</td><td><label for='alert_lang_"&i&"'>"&HTMLEncode(RS_langs("name"))&"</label></td></tr>"
		RS_langs.MoveNext
		i=i+1
	Loop
                                %>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <a class="save_and_next_button" onclick="document.my_form.submit()">
                                <%=LNG_NEXT%></a>
                        </td>
                    </tr>
                </table>
                <%
End If
                %>
                </form> </div>
                <!--img src="images/inv.gif" alt="" width="1" height="100%" border="0"/-->
                </div>
            </td>
        </tr>
    </table>
    </td> </tr> </table>
    <div id="skins_tiles_dialog" title="<%=LNG_SELECT_SKIN %>" style='display: none'>
        <br />
        <div style="display: block; text-align: left; color: #696969">
            <input type="checkbox" id="skins_checkbox" /><label for="skins_checkbox"><%=LNG_DONT_SHOW_AGAIN %></label>
        </div>        
        <p>
        </p>
        
        
    </div>
    <div id="color_code_dialog" title="<%=LNG_PICK_COLOR_CODE %>" style='display: none'>
        <p>
            <%=LNG_PICK_COLOR_CODE_HINT %></p>
        <%
		Set RSColorCodes = Conn.Execute("SELECT * FROM color_codes")
		Do While Not RSColorCodes.EOF
			cc_id = RScolorCodes("id")
			cc_id = Replace(cc_id,"{","")
			cc_id = Replace(cc_id,"}","")
			cc_color = RSColorCodes("color")
			cc_title = RSColorCodes("name")
        %>
        <div id="<%=cc_id %>" onclick="pickColorCode('<%=cc_color%>')" style="width: 180px;
            float: left; height: 180px; margin: 10px; padding: 10px; display: table; background-color: #<%=cc_color%>">
            <div style="cursor:pointer;display: table-cell; vertical-align: middle">
                <div style="font-family: Tahoma; font-size: 24px; text-align: center;">
                    <%=cc_title %></div>
            </div>
        </div>

        <script>
            $("#<%=cc_id%> > div > div").css("color", darkOrLight("<%=cc_color%>"));
        </script>

        <%
		RSColorCodes.MoveNext
		loop
        %>
    </div>
    <div id="alert_type_dialog" title="<%=LNG_SELECT_ALERT_TYPE %>" style='display: none'>
    <div style="display:block;height:200px;width:auto;">
        <div style='float: left; cursor: pointer; cursor: hand; margin-right: 10px; margin-bottom: 10px;
            position: relative' onclick="selectAlertTypeAndClose('alert')">
            <img src='images/alert_types/popup_big.png' />
            <div style='position: absolute; text-align: left; background-color: rgba(0,0,0,0.5);
                z-index: 999; bottom: 0px; width: 200px; height: 32px; color: white; text-align: center'>
                <div style="padding-top: 10px">
                    <%= LNG_DESKTOP_ALERT%></div>
            </div>
        </div>
        <%if TICKER_CFG=1 then %>
        <div style='float: left; cursor: pointer; cursor: hand; margin-right: 10px; margin-bottom: 10px;
            position: relative' onclick="selectAlertTypeAndClose('ticker')">
            <img src='images/alert_types/ticker_big.png' />
            <div style='position: absolute; text-align: left; background-color: rgba(0,0,0,0.5);
                z-index: 999; bottom: 0px; width: 200px; height: 32px; color: white; text-align: center'>
                <div style="padding-top: 10px">
                    <%= LNG_SCROLL_IN_TICKER%></div>
            </div>
        </div>
        <%end if %>
        <% if Request("webplugin") <> "1" then %>
        <div style='float: left; cursor: pointer; cursor: hand; margin-right: 10px; margin-bottom: 10px;
            position: relative' onclick="selectAlertTypeAndClose('rsvp')">
            <img src='images/alert_types/rsvp_big.png' />
            <div style='position: absolute; text-align: left; background-color: rgba(0,0,0,0.5);
                z-index: 999; bottom: 0px; width: 200px; height: 32px; color: white; text-align: center'>
                <div style="padding-top: 10px">
                    RSVP</div>
            </div>
        </div>
        <% end if %>
        </div>
        <div style="text-align: left; color: #696969;">
            <input type="checkbox" id="magic_checkbox" /><label for="magic_checkbox"><%=LNG_DONT_SHOW_AGAIN %></label>
        </div>
        <input type="hidden" value="" id="" />
    </div>
    <div id="dialog-modal" style="display: none">
        <p style="vertical-align: middle">
        </p>
    </div>
    <div id="guideDialog" title="DeskAlerts Beginner's Guide" style="display: none">
        <div>
            <p>
                This is the most used page of the DeskAlerts software. From here, you can create
                messages with various content and send them via the different channels.
            </p>
            <p>
                First, specify the title of your message. After that, enter the message content
                using the rich text editor. To the right from the text editor you can select one
                of the pre-defined pop-up windows designs.
            </p>
            <p>
                Right below the editor there are additional options - you can learn more about these
                in the <a href="http://www.alert-software.com/support/documentation/editors-manual/"
                    target="_blank">DeskAlerts Publisher's Manual</a>.
            </p>
            <p>
                When you are done with the message creation, use any of the "Preview" buttons to
                look how your message will look like. After you done, click "Save and Next" to proceed
                to selecting recipients.
            </p>
        </div>
    </div>
    <%
    end if
else

  Response.Redirect "index.asp"

end if
end if
    %>
    <% if YAMMER_CFG=1 then%>
    <script type="text/javascript" src="https://s0.assets-yammer.com/assets/platform_social_buttons.min.js"></script>
    <script language="javascript" type="text/javascript">
        if (!$("input[name='id']").val()) {
            $("#yammerShare").removeClass("yammerShare");
            $("#yammerShare").click(function(e){e.preventDefault();return false;});
            $("#yammerNotification").show();
        }
        else {
            var url = "<%=jsEncode(alerts_folder)%>preview.asp?id=" + $("input[name='id']").val();
            var msg = $("#title_0").val();
        }
        var options = {
            customButton: true, //false by default. Pass true if you are providing your own button to trigger the share popup
            classSelector: 'yammerShare', //if customButton is true, you must pass the css class name of your button (so we can bind the click event for you)
            defaultMessage: msg, //optionally pass a message to prepopulate your post
            pageUrl: url //current browser url is used by default. You can pass your own url if you want to generate the OG object from a different URL.
        };
        yam.platform.yammerShare(options);
	</script>
	<% end if %>
    <!-- #include file="db_conn_close.asp" -->
</body>
</html>
