﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%
check_session()
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<meta http-equiv=Content-Type content="text/html;charset=utf-8">
	<title>Deskalerts Control Panel</title>
	<link href="css/style9.css" rel="stylesheet" type="text/css">
	<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
	<script language="javascript" type="text/javascript">
		$(document).ready(function() {
			$(".close_button").button();
		});
	</script>
	<style>
		table {border-collapse:collapse;empty-cells:show}
		td {border-width:2px}
	</style>
</head>
<%
   if(Request("limit") <> "") then 
	limit = clng(Request("limit"))
   else 
	limit=50
   end if
	surveyId = clng(request("id"))

if(Request("offset") <> "") then 
offset = clng(Request("offset"))
else 
offset=0
end if

%> 
<body style="background-color:#8A92A6; margin:0px" class="body">
<table width="100%" border="0" cellspacing="0" cellpadding="6" bgcolor="#8A92A6" height="100%" class="body">
	<tr>
	<td valign="top">
	<table width="100%" cellspacing="0" cellpadding="0" class="main_table">
		<tr>
		<td width=100% height=31 class="theme-colored-back"><a href="#" class="header_title"><%=LNG_LIST_OF_RECIPIENTS %></a></td>
		</tr>
		<tr>
		<td valign="top" class="main_table_body">
		<div style="margin:10px">
<%
Set RS = Conn.Execute("SELECT type, create_date FROM surveys_main WHERE id=" & surveyId)
if(not RS.EOF) then
	type2=RS("type")
	strAlertDate=RS("create_date")
end if

alertId = "0"
Set RSAlert = Conn.Execute("SELECT sender_id FROM surveys_main WHERE id=" & surveyId)
if not RSAlert.EOF then
	alertId = RSAlert("sender_id")
end if

cnt=0
usersCnt=0
groupsCnt=0
if type2 = "B" then
    Set rsCnt = Conn.Execute("SELECT COUNT(id) as mycnt FROM users WHERE role = 'U' AND  ( client_version != 'web client' OR client_version is NULL ) AND reg_date <= '"& strAlertDate &"'"& searchUsersSQL)
	if(not rsCnt.EOF) then
		usersCnt=rsCnt("mycnt")
	end if
else
	Set rsCnt = Conn.Execute("SELECT COUNT(user_id) as mycnt FROM alerts_sent WHERE alert_id="&alertId)
	if(not rsCnt.EOF) then
		usersCnt = rsCnt("mycnt")
	end if
	Set rsCnt = Conn.Execute("SELECT COUNT(group_id) as mycnt FROM alerts_sent_group WHERE alert_id="&alertId)
	if(not rsCnt.EOF) then
		groupsCnt = rsCnt("mycnt")
	end if
end if
cnt = usersCnt + groupsCnt
j=cnt/limit
	

if(cnt>0) then
	page= "view_recipients_list_surveys.asp?id="&surveyId&"&"
	name=LNG_RECIPIENTS
	Response.Write make_pages(offset, cnt, limit, page, name, sortby) 
end if


'paging
%>
<table width="95%" cellspacing="0" cellpadding="3" class="data_table">
<tr class="data_table_title">
	<td class="table_title"><%=LNG_NAME %></td>
	<td class="table_title"><%=LNG_TYPE %></td>	
</tr>
<%
	if type2 = "B" then    
	SQL = "SELECT name, 'User' as r_type FROM users WHERE role = 'U'  AND  ( client_version != 'web client' OR client_version is NULL ) AND reg_date <= '"& strAlertDate &"'" & searchUsersSQL
	else
		SQL = "SELECT name, 'User' as r_type FROM users INNER JOIN alerts_sent ON users.id=alerts_sent.user_id WHERE alerts_sent.alert_id="&alertId
		SQL = SQL & " UNION ALL "
		SQL = SQL & "SELECT name, 'Group' as r_type FROM groups INNER JOIN alerts_sent_group ON groups.id=alerts_sent_group.group_id WHERE alerts_sent_group.alert_id="&alertId
	end if
	Set rsRecipient = Conn.Execute(SQL)
	num=offset
	if(Not rsRecipient.EOF) then rsRecipient.Move(offset) end if
	do while not rsRecipient.EOF
		num=num+1
		if((offset+limit) < num) then Exit Do end if

		if(num > offset AND offset+limit >= num) then
			recipientName = rsRecipient("name")
			recipientType = rsRecipient("r_type")
%>
	<tr><td><%=recipientName%></td><td><%=recipientType%></td></tr>
<%
		end if
		rsRecipient.MoveNext
	loop
%>
 
</table>
<%
if(cnt>0) then
	Response.Write make_pages(offset, cnt, limit, page, name, sortby) 
end if
%>
 
<br/>
<center><A href="#" class="close_button" onclick="javascript: window.close()"><%=LNG_CLOSE %></a></center>
 
</div>		</td>
		</tr>
	</table>
	</td>
	</tr>
</table>
</body>
</html>
<!-- #include file="db_conn_close.asp" -->