﻿using System;
using System.Web.UI;
using DeskAlerts.ApplicationCore.Enums;
using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.ApplicationCore.Repositories;
using DeskAlerts.ApplicationCore.Utilities;
using DeskAlertsDotNet.Parameters;
using Resources;

namespace DeskAlertsDotNet.Pages
{
    public partial class UserStatisticsDetails : DeskAlertsBasePage
    {
        protected string AlertsColors = "";
        protected string AlertsValues = "";
        
        private readonly IConfigurationManager _configurationManager =  new WebConfigConfigurationManager(Config.Configuration);
        private IAlertReceivedRepository _alertReceivedRepository;

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            _alertReceivedRepository = new PpAlertReceivedRepository(_configurationManager);

            var userId = Request["id"];

            if (string.IsNullOrEmpty(userId))
            {
                Response.End();
                return;
            }

            #region Queries

            var groupsAmountQuery = $@"
                SELECT count(group_id) 
                FROM users_groups 
                WHERE user_id = {userId}";

            var receivedAlertsAmountQuery = $@"
                SELECT DISTINCT alert_id 
                FROM alerts_received
                WHERE user_id = {userId}
                    AND status = {(int)AlertReceiveStatus.Received}";

            var acknowledgmentAmountQuery = $@"
                SELECT COUNT(DISTINCT alert_id) as mycnt 
                FROM [alerts_read] 
                INNER JOIN alerts ON alerts.id = alerts_read.alert_id 
                    AND user_id = {userId}";

            #endregion

            #region Work with db and calculations

            var userRow = dbMgr
                .GetDataByQuery($"SELECT * FROM users WHERE id = {userId}")
                .First();

            var groupsAmount = dbMgr.GetScalarQuery<string>(groupsAmountQuery);

            var totalAmount = _alertReceivedRepository.GetSendedForUser(long.Parse(userId)).Count;

            var receivedAmount = dbMgr
                .GetDataByQuery(receivedAlertsAmountQuery)
                .Count;

            var acknowledgmentAmount = dbMgr
                .GetScalarQuery<int>(acknowledgmentAmountQuery);

            var notReceivedAmount = totalAmount - receivedAmount;

            #endregion

            #region Page filling

            const string receivedColor = "'#3B6392'";
            const string notReceivedColor = "'#9ABA59'";
            const string aknownColor = "'#A44441'";
            const string valuesJsonTemplate = "[{0}]";

            var receivedKey = resources.LNG_RECEIVED;
            var notReceivedKey = resources.LNG_NOT_RECEIVED;
            var aknownKey = resources.LNG_ACKNOWLEDGED;


            firstLegend.Style.Add(HtmlTextWriterStyle.BackgroundColor, receivedColor.Replace("'", ""));
            firstLegendLabel.InnerText = receivedKey;

            if (receivedAmount == 0)
            {
                firstLegendValue.InnerHtml = "0";
            }
            else
            {
                var link =
                    $"<a href=\'UserAlerts.aspx?type=rec&id={userId}\' title=\'\'  class=\'prelink_stats\'>{receivedAmount}</a>";
                firstLegendValue.InnerHtml = link;
            }

            AlertsValues += string.Format(valuesJsonTemplate, receivedAmount);
            AlertsColors = receivedColor;

            secondLegend.Style.Add(HtmlTextWriterStyle.BackgroundColor, aknownColor.Replace("'", ""));
            secondLegendLabel.InnerText = aknownKey;

            if (acknowledgmentAmount == 0)
            {
                secondLegendValue.InnerHtml = "0";
            }
            else
            {
                var link =
                    $"<a href=\'UserAlerts.aspx?type=ack&id={userId}\' title=\'\' class=\'prelink_stats\'>{acknowledgmentAmount}</a>";
                secondLegendValue.InnerHtml = link;
            }

            if (AlertsValues.Length > 0)
            {
                AlertsValues += ",";
            }

            AlertsValues += string.Format(valuesJsonTemplate, acknowledgmentAmount);
            AlertsColors += ",";
            AlertsColors += aknownColor;

            thirdLegend.Style.Add(HtmlTextWriterStyle.BackgroundColor, notReceivedColor.Replace("'", ""));
            thirdLegendLabel.InnerText = notReceivedKey;

            if (notReceivedAmount == 0)
            {
                thirdLegendValue.InnerHtml = "0";
            }
            else
            {
                var link =
                    $"<a href=\'UserAlerts.aspx?type=drec&id={userId}\' title=\'\' class=\'prelink_stats\'>{notReceivedAmount}</a>";
                thirdLegendValue.InnerHtml = link;
            }

            if (AlertsValues.Length > 0)
            {
                AlertsValues += ",";
            }

            AlertsValues += string.Format(valuesJsonTemplate, notReceivedAmount);

            if (AlertsColors.Length > 0)
            {
                AlertsColors += ",";
            }

            AlertsColors += notReceivedColor;

            totalAlertsCountLabel.Text =
                $"<a class='prelink_stats' title='' href='UserAlerts.aspx?id={userId}'>{totalAmount}</a>";

            userNameLabel.Text = userRow.GetString("name");

            var displayName = userRow.GetString("display_name");
            if (!string.IsNullOrEmpty(displayName))
            {
                displayName = "(" + displayName + ")";
            }
            userDisplayNameLabel.Text = displayName;

            totalGroupsLabel.Text = groupsAmount;

            #endregion
        }
    }
}