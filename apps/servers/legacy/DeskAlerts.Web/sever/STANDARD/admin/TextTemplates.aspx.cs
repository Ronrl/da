﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Models;
using DeskAlertsDotNet.Models.Permission;
using DeskAlertsDotNet.Parameters;

namespace DeskAlertsDotNet.Pages
{
    using Resources;

    public partial class TextTemplates : DeskAlertsBaseListPage
    {

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            headerTitle.Text = resources.LNG_TEXT_TEMPLATES;
            searchButton.Text = searchButton.Text.Replace("{Search}", resources.LNG_SEARCH);
            groupNameCell.Text = GetSortLink(resources.LNG_NAME, "name", Request["sortBy"]);

            actionsCell.Text = resources.LNG_ACTIONS;
            base.Table = contentTable;
            base.FillTableWithContent(true);
            addButton.Visible = curUser.HasPrivilege || curUser.Policy[Policies.TEXT_TEMPLATES].CanCreate;
            upDelete.Visible = curUser.HasPrivilege || curUser.Policy[Policies.TEXT_TEMPLATES].CanDelete;
            bottomDelete.Visible = curUser.HasPrivilege || curUser.Policy[Policies.TEXT_TEMPLATES].CanDelete;
        }


        #region DataProperties
        protected override string DataTable
        {
            get
            {
                return "text_templates";
            }
        }

        protected override string[] Collumns
        {
            get
            {
                return new string[] { "name" };
            }
        }

        protected override string DefaultSortingOrder
        {
            get { return "DESC"; }
            
        }

        protected override string DefaultOrderCollumn
        {
            get
            {
                return "name";
            }
        }

        protected override System.Web.UI.HtmlControls.HtmlInputText SearchControl
        {
            get
            {
                return searchTermBox;
            }
        }

        protected override string SearchField
        {
            get
            {
                return "name";
            }
            set
            {
                base.SearchField = value;
            }
        }
        #endregion

        #region Interface properties

        protected override IButtonControl[] DeleteButtons
        {
            get
            {
                return new IButtonControl[] { upDelete, bottomDelete };
            }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl NoRowsDiv
        {
            get { return NoRows; }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl TableDiv
        {
            get { return mainTableDiv; }
        }

        protected override string ContentName
        {
            get
            {
                //return content name here
                return resources.LNG_TEXT_TEMPLATES;
            }
        }


        protected override System.Web.UI.HtmlControls.HtmlGenericControl BottomPaging
        {
            get
            {
                return bottomRanging;
            }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl TopPaging
        {
            get
            {
                return topPaging;
            }
        }


        protected override List<Control> GetActionControls(DataRow row)
        {
            List<Control> controls = new List<Control>();



            LinkButton previewButton = GetActionButton(row.GetString("id"), "images/action_icons/preview.png", "LNG_PREVIEW", "showAlertPreview(" + row.GetString("id") + ")");

                LinkButton createAlertButton = GetActionButton(row.GetString("id"), "images/action_icons/send.png", "LNG_SEND_TEMPLATE",
                    delegate
                    {
                        Response.Redirect("CreateAlert.aspx?tid=" + row.GetString("id"), true);
                    }

                );

                LinkButton editButton = GetActionButton(row.GetString("id"), "images/action_icons/edit.png", "LNG_EDIT_TEMPLATE",
                    delegate
                    {
                        Response.Redirect("EditTextTemplate.aspx?id=" + row.GetString("id"), true);
                    }

                );

                controls.Add(previewButton);

                if (curUser.HasPrivilege || curUser.Policy[Policies.ALERTS].CanCreate)
                 controls.Add(createAlertButton);

               if (curUser.HasPrivilege || curUser.Policy[Policies.TEXT_TEMPLATES].CanEdit)
                controls.Add(editButton);


            return controls;
        }

        #endregion
    }
}