<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%
check_session()
%>
<!-- #include file="Common.asp" -->
<!-- #include file="header.asp" -->
<!-- #include file="footer.asp" -->
<!-- #include file="libs/FusionCharts.asp" -->
<%
'On Error Resume Next
' index CustomIncludes End
'-------------------------------

'===============================
' Save Page and File Name available into variables
'-------------------------------
Dim sFileName
Dim sTemplateFileName
Dim sPaginateFileName
Dim intSessionUserId

sFileName = "statistics_da_users_view_alerts.asp"
sTemplateFileName = "statistics_da_users_view_alerts.html"
sPaginateFileName = "paginate.html"
'===============================
  

intSessionUserId = Session("uid")
uid=intSessionUserId


if (intSessionUserId <> "") then


' index Show begin

'===============================
' Display page
'-------------------------------
' Load HTML template for this page
'-------------------------------
LoadTemplate alerts_dir & "\" & templateFolder & "\" & sTemplateFileName, "main"
SetVar "DListUsersStat", ""
'-------------------------------
' Load HTML template of Header and Footer
'-------------------------------
LoadTemplate sHeaderFileName, "Header"
LoadTemplate sFooterFileName, "Footer"
'-------------------------------


pageType=request("type")
user_id=Clng(request("id"))
from_date=request("from_date")
to_date=request("to_date")

SetVar "default_lng", default_lng
SetVar "LNG_ALERT", LNG_NAME
SetVar "LNG_DATE", LNG_DATE

if(pageType="rec") then
	PageName(LNG_RECEIVED)
end if

if(pageType="ack") then
	PageName(LNG_ACKNOWLEDGED)
end if

if(pageType="drec") then
	PageName(LNG_NOT_RECEIVED)
end if

   if(Request("offset") <> "") then 
	offset = clng(Request("offset"))
   else 
	offset = 0
   end if	
  
   if(Request("limit") <> "") then 
	limit = clng(Request("limit"))
   else 
	limit=50
   end if


Set userRS = Conn.Execute("SELECT reg_date FROM users WHERE id="& user_id)
if(Not userRS.EOF) then
strUserDate = userRS("reg_date")
userRS.Close

'--- rights ---
set rights = getRights(uid, true, true, true, null)
gid = rights("gid")
gsendtoall = rights("gsendtoall")
gviewall = rights("gviewall")
gviewlist = rights("gviewlist")
'--------------

'doesnt received

if(pageType="drec") then
cnt=0
'personal
		if gviewall <> 1 then
			Set RS = Conn.Execute("SELECT COUNT(DISTINCT alerts_sent_stat.alert_id) as mycnt FROM alerts_sent_stat INNER JOIN surveys_main ON surveys_main.sender_id=alerts_sent_stat.alert_id INNER JOIN alerts ON alerts.id = surveys_main.sender_id AND alerts.sender_id IN ("& Join(gviewlist,",") &") WHERE alert_id NOT IN (SELECT alert_id FROM alerts_received WHERE user_id="& user_id & ") AND create_date >= '"&from_date&"' AND create_date <= '"&to_date&"' AND user_id="& user_id)
		else
			Set RS = Conn.Execute("SELECT COUNT(DISTINCT alerts_sent_stat.alert_id) as mycnt FROM alerts_sent_stat INNER JOIN surveys_main ON surveys_main.sender_id=alerts_sent_stat.alert_id WHERE alert_id NOT IN (SELECT alert_id FROM alerts_received WHERE user_id="& user_id & ") AND create_date >= '"&from_date&"' AND create_date <= '"&to_date&"' AND user_id="& user_id)
		end if
		if(Not RS.EOF) then
			cnt=cnt+RS("mycnt")
		end if
'		response.write "personal_cnt=" & RS("mycnt") & "<br>"
		RS.Close
'broadcast
		if gviewall <> 1 then
			Set RS = Conn.Execute("SELECT COUNT(DISTINCT surveys_main.id) as mycnt FROM surveys_main INNER JOIN alerts ON alerts.id = surveys_main.sender_id AND alerts.sender_id IN ("& Join(gviewlist,",") &") WHERE id NOT IN (SELECT survey_id FROM alerts_received WHERE user_id="& user_id & ") AND [create_date] > '"&strUserDate&"' AND [create_date] >= '"&from_date&"' AND [create_date] <= '"&to_date&"' AND type='B'")
		else
			Set RS = Conn.Execute("SELECT COUNT(DISTINCT surveys_main.id) as mycnt FROM surveys_main WHERE id NOT IN (SELECT survey_id FROM alerts_received WHERE user_id="& user_id & ") AND [create_date] > '"&strUserDate&"' AND [create_date] >= '"&from_date&"' AND [create_date] <= '"&to_date&"' AND type='B'")
		end if
'		Set RS = Conn.Execute("SELECT COUNT(DISTINCT surveys_main.id) as mycnt FROM surveys_main WHERE [create_date] > '"&strUserDate&"' AND [create_date] >= '"&from_date&"' AND [create_date] <= '"&to_date&"' AND type='B'")
		if(Not RS.EOF) then
			cnt=cnt+RS("mycnt")
		end if
'		response.write "broad_cnt=" & RS("mycnt") & "<br>"
		RS.Close

'group
		
		if gviewall <> 1 then
			Set RS = Conn.Execute("SELECT COUNT(DISTINCT surveys_main.id) as mycnt FROM surveys_main INNER JOIN alerts ON alerts.id = surveys_main.sender_id AND alerts.sender_id IN ("& Join(gviewlist,",") &")  INNER JOIN surveys_group_stat ON surveys_main.id=surveys_group_stat.survey_id INNER JOIN users_groups ON surveys_group_stat.group_id=users_groups.group_id WHERE survey_id NOT IN (SELECT survey_id FROM alerts_received WHERE user_id="& user_id & ") AND create_date >= '"&from_date&"' AND create_date <= '"&to_date&"' AND create_date > '"& strUserDate &"' AND user_id=" & user_id)
		else
			Set RS = Conn.Execute("SELECT COUNT(DISTINCT surveys_main.id) as mycnt FROM surveys_main INNER JOIN surveys_group_stat ON surveys_main.id=surveys_group_stat.survey_id INNER JOIN users_groups ON surveys_group_stat.group_id=users_groups.group_id WHERE survey_id NOT IN (SELECT survey_id FROM alerts_received WHERE user_id="& user_id & ") AND create_date >= '"&from_date&"' AND create_date <= '"&to_date&"' AND create_date > '"& strUserDate &"' AND user_id=" & user_id)
		end if
		if(Not RS.EOF) then
			cnt=cnt+RS("mycnt")
		end if
'		response.write "group_cnt=" & RS("mycnt") & "<br>"
		RS.Close
	
end if

if(pageType="rec") then
	if gviewall <> 1 then
		Set RS = Conn.Execute("SELECT COUNT(DISTINCT survey_id) as mycnt FROM alerts_received INNER JOIN surveys_main ON surveys_main.id=alerts_received.survey_id INNER JOIN alerts ON alerts.id = surveys_main.sender_id AND alerts.sender_id IN ("& Join(gviewlist,",") &") WHERE [create_date] >= '"&from_date&"' AND [create_date] <= '"&to_date&"' AND user_id="& user_id)
	else
		Set RS = Conn.Execute("SELECT COUNT(DISTINCT survey_id) as mycnt FROM alerts_received INNER JOIN surveys_main ON surveys_main.id=alerts_received.survey_id WHERE [create_date] >= '"&from_date&"' AND [create_date] <= '"&to_date&"' AND user_id="& user_id)
	end if
		if(Not RS.EOF) then
			cnt=RS("mycnt")
		end if
		RS.Close
end if

if(pageType="ack") then
	if gviewall <> 1 then
		Set RS = Conn.Execute("SELECT COUNT(DISTINCT surveys_main.id) as mycnt FROM surveys_main INNER JOIN surveys_answers ON surveys_main.id=surveys_answers.survey_id INNER JOIN surveys_answers_stat ON surveys_answers.id=surveys_answers_stat.answer_id WHERE surveys_main.create_date >= '"&from_date&"' AND surveys_main.create_date <= '"&to_date&"' AND surveys_answers_stat.user_id="& user_id & " AND sender_id IN ("& Join(gviewlist,",") &")")
	else
		Set RS = Conn.Execute("SELECT COUNT(DISTINCT surveys_main.id) as mycnt FROM surveys_main INNER JOIN surveys_answers ON surveys_main.id=surveys_answers.survey_id INNER JOIN surveys_answers_stat ON surveys_answers.id=surveys_answers_stat.answer_id WHERE surveys_main.create_date >= '"&from_date&"' AND surveys_main.create_date <= '"&to_date&"' AND surveys_answers_stat.user_id="& user_id)
	end if
		if(Not RS.EOF) then
			cnt=RS("mycnt")
		end if
		RS.Close
end if
		j=cnt/limit

if(cnt>0) then
	page=sFileName & "?id="&user_id&"&type="&pageType&"&from_date="&from_date&"&to_date="&to_date&"&"
	name="surveys"
	SetVar "paging", make_pages(offset, cnt, limit, page, name, sortby) 
end if

'show main table

	num=0

if(pageType="rec") then
	if gviewall <> 1 then
		Set RS = Conn.Execute("SELECT DISTINCT surveys_main.id as id, name, create_date FROM surveys_main INNER JOIN alerts_received ON surveys_main.id=alerts_received.survey_id INNER JOIN alerts ON alerts.id = surveys_main.sender_id AND alerts.sender_id IN ("& Join(gviewlist,",") &") WHERE surveys_main.create_date >= '"&from_date&"' AND surveys_main.create_date <= '"&to_date&"' AND alerts_received.user_id="& user_id)
	else
		Set RS = Conn.Execute("SELECT DISTINCT surveys_main.id as id, name, create_date FROM surveys_main INNER JOIN alerts_received ON surveys_main.id=alerts_received.survey_id WHERE surveys_main.create_date >= '"&from_date&"' AND surveys_main.create_date <= '"&to_date&"' AND alerts_received.user_id="& user_id)
	end if
end if

if(pageType="ack") then
	if gviewall <> 1 then
		Set RS = Conn.Execute("SELECT DISTINCT surveys_main.id as id, name, create_date FROM surveys_main INNER JOIN surveys_answers ON surveys_main.id=surveys_answers.survey_id INNER JOIN surveys_answers_stat ON surveys_answers.id=surveys_answers_stat.answer_id INNER JOIN alerts ON alerts.id = surveys_main.sender_id AND alerts.sender_id IN ("& Join(gviewlist,",") &") WHERE surveys_main.create_date >= '"&from_date&"' AND surveys_main.create_date <= '"&to_date&"' AND surveys_answers_stat.user_id="& user_id)
	else
		Set RS = Conn.Execute("SELECT DISTINCT surveys_main.id as id, name, create_date FROM surveys_main INNER JOIN surveys_answers ON surveys_main.id=surveys_answers.survey_id INNER JOIN surveys_answers_stat ON surveys_answers.id=surveys_answers_stat.answer_id WHERE surveys_main.create_date >= '"&from_date&"' AND surveys_main.create_date <= '"&to_date&"' AND surveys_answers_stat.user_id="& user_id)
	end if
end if

if(pageType="drec") then
'personal
	if gviewall <> 1 then
		Set RS = Conn.Execute("SELECT DISTINCT surveys_main.id as id, name, create_date FROM surveys_main INNER JOIN alerts_sent_stat ON surveys_main.id=alerts_sent_stat.survey_id INNER JOIN alerts ON alerts.id = surveys_main.sender_id AND alerts.sender_id IN ("& Join(gviewlist,",") &")  WHERE survey_id NOT IN (SELECT survey_id FROM alerts_received WHERE user_id="& user_id & ") AND [date] >= '"&from_date&"' AND [date] <= '"&to_date&"' AND user_id="& user_id)
	else
		Set RS = Conn.Execute("SELECT DISTINCT surveys_main.id as id, name, create_date FROM surveys_main INNER JOIN alerts_sent_stat ON surveys_main.id=alerts_sent_stat.survey_id WHERE survey_id NOT IN (SELECT survey_id FROM alerts_received WHERE user_id="& user_id & ") AND [date] >= '"&from_date&"' AND [date] <= '"&to_date&"' AND user_id="& user_id)
	end if
	Do While Not RS.EOF

        			num=num+1
				if(num > offset AND offset+limit >= num) then
	
			    text=HtmlEncode(RS("name"))
				strAlertTitle = "<A href='#' onclick=""javascript: window.open('statistics_da_surveys_view_details.asp?id=" & CStr(RS("id")) & "','', 'status=0, toolbar=0, width=600, height=550, scrollbars=1')"">" & text & "</a>"

				strDate=RS("create_date")
				SetVar "strDate", strDate
				SetVar "strAlertTitle", strAlertTitle
				Parse "DListUsersStat", True

				end if

	RS.MoveNext
	Loop
	RS.Close

'broadcast
	if gviewall <> 1 then
		Set RS = Conn.Execute("SELECT DISTINCT surveys_main.id as id, name, create_date FROM surveys_main INNER JOIN alerts ON alerts.id = surveys_main.sender_id AND alerts.sender_id IN ("& Join(gviewlist,",") &") WHERE id NOT IN (SELECT survey_id FROM alerts_received WHERE user_id="& user_id & ") AND [create_date] >= '"&strUserDate&"' AND [create_date] >= '"&from_date&"' AND [create_date] <= '"&to_date&"' AND type='B'")
	else
		Set RS = Conn.Execute("SELECT DISTINCT surveys_main.id as id, name, create_date FROM surveys_main WHERE id NOT IN (SELECT survey_id FROM alerts_received WHERE user_id="& user_id & ") AND [create_date] >= '"&strUserDate&"' AND [create_date] >= '"&from_date&"' AND [create_date] <= '"&to_date&"' AND type='B'")
	end if
	Do While Not RS.EOF

        			num=num+1
				if(num > offset AND offset+limit >= num) then
	
			    text=HtmlEncode(RS("name"))
				strAlertTitle = "<A href='#' onclick=""javascript: window.open('statistics_da_surveys_view_details.asp?id=" & CStr(RS("id")) & "','', 'status=0, toolbar=0, width=600, height=550, scrollbars=1')"">" & text & "</a>"

				strDate=RS("create_date")
				SetVar "strDate", strDate
				SetVar "strAlertTitle", strAlertTitle
				Parse "DListUsersStat", True

				end if

	RS.MoveNext
	Loop
	RS.Close

'group
	if gviewall <> 1 then
		Set RS = Conn.Execute("SELECT DISTINCT surveys_main.id as id, name, create_date FROM surveys_main INNER JOIN surveys_group_stat ON surveys_main.id=surveys_group_stat.survey_id INNER JOIN users_groups ON surveys_group_stat.group_id=users_groups.group_id INNER JOIN alerts ON alerts.id = surveys_main.sender_id AND alerts.sender_id IN ("& Join(gviewlist,",") &") WHERE survey_id NOT IN (SELECT survey_id FROM alerts_received WHERE user_id="& user_id & ") AND create_date >= '"&from_date&"' AND create_date <= '"&to_date&"' AND create_date > '"& strUserDate &"' AND user_id=" & user_id)
	else
		Set RS = Conn.Execute("SELECT DISTINCT surveys_main.id as id, name, create_date FROM surveys_main INNER JOIN surveys_group_stat ON surveys_main.id=surveys_group_stat.survey_id INNER JOIN users_groups ON surveys_group_stat.group_id=users_groups.group_id WHERE survey_id NOT IN (SELECT survey_id FROM alerts_received WHERE user_id="& user_id & ") AND create_date >= '"&from_date&"' AND create_date <= '"&to_date&"' AND create_date > '"& strUserDate &"' AND user_id=" & user_id)
	end if
	Do While Not RS.EOF

        			num=num+1
				if(num > offset AND offset+limit >= num) then
	
			    text=HtmlEncode(RS("name"))
				strAlertTitle = "<A href='#' onclick=""javascript: window.open('statistics_da_surveys_view_details.asp?id=" & CStr(RS("id")) & "','', 'status=0, toolbar=0, width=600, height=550, scrollbars=1')"">" & text & "</a>"

				strDate=RS("create_date")
				SetVar "strDate", strDate
				SetVar "strAlertTitle", strAlertTitle
				Parse "DListUsersStat", True

				end if

	RS.MoveNext
	Loop
	RS.Close

else

	Do While Not RS.EOF

        			num=num+1
				if(num > offset AND offset+limit >= num) then
	
			    text=HtmlEncode(RS("name"))
				strAlertTitle = "<A href='#' onclick=""javascript: window.open('statistics_da_surveys_view_details.asp?id=" & CStr(RS("id")) & "','', 'status=0, toolbar=0, width=600, height=550, scrollbars=1')"">" & text & "</a>"

				strDate=RS("create_date")
				SetVar "strDate", strDate
				SetVar "strAlertTitle", strAlertTitle
				Parse "DListUsersStat", True

				end if

	RS.MoveNext
	Loop
	RS.Close
end if

'--------------------


'-------------------------------
' Step through each form
'-------------------------------
Header_Show
Footer_Show
'-------------------------------
' Process page templates
'-------------------------------
Parse "Header", False
Parse "Footer", False
Parse "main", False
'-------------------------------
' Output the page to the browser
'-------------------------------
Response.write PrintVar("main")

' index Show End

'-------------------------------
' Destroy all object variables
'-------------------------------

' index Close Event begin
' index Close Event End

UnloadTemplate
'===============================
end if

else

Login(Request("intErrorCode"))
'response.write "test111222"

end if

'===============================
' Display Grid Form
'-------------------------------

%><!-- #include file="db_conn_close.asp" -->