<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%
check_session()
%>
<!-- #include file="Common.asp" -->
<!-- #include file="header.asp" -->
<!-- #include file="footer.asp" -->
<!-- #include file="libs/FusionCharts.asp" -->
<%
'On Error Resume Next
' index CustomIncludes End
'-------------------------------

'===============================
' Save Page and File Name available into variables
'-------------------------------
Dim sFileName
Dim sTemplateFileName
Dim sPaginateFileName
Dim intSessionUserId

sFileName = "statistics_da_surveys_view_users_custom.asp"
sTemplateFileName = "statistics_da_surveys_view_users_custom.html"
sPaginateFileName = "paginate.html"
'===============================
  

intSessionUserId = Session("uid")



if (intSessionUserId <> "") then


' index Show begin

'===============================
' Display page
'-------------------------------
' Load HTML template for this page
'-------------------------------
LoadTemplate alerts_dir & "\" & templateFolder & "\" & sTemplateFileName, "main"
SetVar "DListUsersStat", ""
'-------------------------------
' Load HTML template of Header and Footer
'-------------------------------
LoadTemplate sHeaderFileName, "Header"
LoadTemplate sFooterFileName, "Footer"
LoadTemplate sSearchDetailsFormFileName, "SearchForm"
'-------------------------------

pageType=request("type")
intSurveyId=0
intQuestionId=0

if(request("id")<>"") then
	intSurveyId=Clng(request("id"))
end if
if(request("question_id")<>"") then
	intQuestionId=Clng(request("question_id"))
end if

searchSQL = ""
search = request("search")
if search <> "" then 
	searchSQL = " AND (users.name LIKE N'%"&replace(search, "'", "''")&"%' OR users.display_name LIKE N'%"&replace(search, "'", "''")&"%')"
end if

SetVar "searchTerms", HTMLEncode(search)
SetVar "page", sFileName & "?id="&intSurveyId&"&question_id="&intQuestionId&"&type="&pageType&"&offset=0&limit="&Request("limit")&"&sortby="&Request("sortby")
SetVar "searchName", "users"

SetVar "LNG_USERNAME", LNG_USERNAME
SetVar "LNG_DATE", LNG_DATE
SetVar "LNG_ANSWER", LNG_ANSWER
SetVar "default_lng", default_lng

PageName(LNG_CUSTOM_ANSWERS)

if(Request("offset") <> "") then 
	offset = clng(Request("offset"))
else
	offset = 0
end if	

if(Request("limit") <> "") then 
	limit = clng(Request("limit"))
else
	limit=50
end if

'doesnt received
if(intSurveyId<>0) then
	Set surveyRS = Conn.Execute("SELECT create_date, type FROM surveys_main WHERE id="& intSurveyId)
	if(Not surveyRS.EOF) then

	mytype=surveyRS("type")
	create_date=surveyRS("create_date")
	
	surveyRS.Close
	end if


	Set RS = Conn.Execute("SELECT COUNT(DISTINCT stat.user_id) as mycnt FROM surveys_custom_answers_stat as stat INNER JOIN users as users on (stat.user_id = users.id) WHERE stat.question_id="& intQuestionId & searchSQL)
	if(Not RS.EOF) then
		cnt=RS("mycnt")
	end if
	RS.Close
	j=cnt/limit

end if

if(cnt>0) then
	page=sFileName & "?id="&intSurveyId&"&question_id="&intQuestionId&"&type="&pageType&"&search="&Server.URLEncode(search)&"&"
	name=LNG_USERS
	SetVar "paging", make_pages(offset, cnt, limit, page, name, sortby) 
end if

'show main table
	num=offset
	Set RS = Conn.Execute("SELECT DISTINCT users.id as id, users.name as name, users.domain_id as domain_id, surveys_custom_answers_stat.date as [date], answer FROM users INNER JOIN surveys_custom_answers_stat ON users.id=surveys_custom_answers_stat.user_id WHERE role='U' AND surveys_custom_answers_stat.question_id="& intQuestionId & searchSQL)
	if(Not RS.EOF) then RS.Move(offset) end if
	Do While Not RS.EOF
		num=num+1
		if((offset+limit) < num) then Exit Do end if
		if(num > offset AND offset+limit >= num) then
        		if(IsNull(RS("name"))) then
			name="Unregistered"
		else
		strUserName = RS("name")
		end if

		if (AD = 3) then 	
		       	Set RS3 = Conn.Execute("SELECT name FROM domains WHERE id=" & RS("domain_id"))
			if(Not RS3.EOF) then
				  domain=RS3("name")
			else
				domain="&nbsp;"
			end if
			RS3.close
		end if

		if(pageType<>"drec" AND pageType<>"dvot") then
			strDate=RS("date")
			strAnswer=Replace(RS("answer")&"", vbNewLine, "<br/>" & vbNewLine)
			SetVar "strDate", strDate
			SetVar "strAnswer", strAnswer
		end if
		SetVar "strUserName", strUserName
		Parse "DListUsersStat", True

		end if
	RS.MoveNext
	Loop
	RS.Close
'--------------------


'-------------------------------
' Step through each form
'-------------------------------
Header_Show
Footer_Show
'-------------------------------
' Process page templates
'-------------------------------
Parse "Header", False
Parse "Footer", False
Parse "main", False
'-------------------------------
' Output the page to the browser
'-------------------------------
Response.write PrintVar("main")

' index Show End

'-------------------------------
' Destroy all object variables
'-------------------------------

' index Close Event begin
' index Close Event End

UnloadTemplate
'===============================

else

Login(Request("intErrorCode"))

end if

'===============================
' Display Grid Form
'-------------------------------

%><!-- #include file="db_conn_close.asp" -->