﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditPublisher.aspx.cs" Inherits="DeskAlertsDotNet.Pages.EditPublisher" %>
<%@ Import NameSpace="DeskAlertsDotNet.Parameters" %>
<%@ Import Namespace="Resources" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="css/style9.css" rel="stylesheet" type="text/css">
    <link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
    <title></title>
    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
    <script  language="javascript" type="text/javascript">
        
        function show_password_field() {

            document.getElementById("passwordFieldRow").style.display = "";
            document.getElementById("changePasswordField").style.display = "none";
            document.getElementById("keepPassword").value = "0";

        }

        function hide_password_field() {

            document.getElementById("passwordFieldRow").style.display = "none";
            document.getElementById("changePasswordField").style.display = "";
            document.getElementById("keepPassword").value = "1";

        }

        $(document).ready(function () {
            $(".add_button").button();
            $(".save_button").button();
            $(".cancel_button").button();
            $("#edPolicy").change(function () {
                if ($(this).val() == "") {
                    $("#editPolicyLink").css({
                        "cursor": "default",
                        "color": "gray",
                        "text-decoration": "none"
                    });
                }
                else {
                    $("#editPolicyLink").css({
                        "cursor": "pointer",
                        "cursor": "hand",
                        "color": "black",
                        "text-decoration": "underline"
                    });
                }
            });
        });

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
          <body style="margin:0px" class="body">
            <table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	            <tr>
	            <td>
	            <table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
		            <tr>
		            <td width=100% height=31 class="main_table_title"><img src="images/menu_icons/editors_20.png" alt="" width="20" height="20" border="0" style="padding-left:7px">
		            <a href="admin_admin.asp" class="header_title" style="position:absolute;top:15px"><%=resources.LNG_EDITOR %></a></td>
		            </tr>
		            <tr>
		            <td class="main_table_body" height="100%">  
                        <div style="margin:10px;">
                            <span runat="server" id="workHeader" class="work_header"></span>
                            <br><br>
                            <div runat="server" id="publisherEditValidationErrors" style="color: #d92727;"></div>
                            <table border="0"><tr><td>
                                <font runat="server" id="errorDescription" color="red"></font>
                                <table cellpadding=4 cellspacing=4 border=0>
                                <tr><td><%=resources.LNG_EDITOR_NAME %>:</td>
                                    <td>
                                        <input autocomplete="off" runat="server" name="name" id="nameField" type="text" value="" size="40" maxlength="255"/>
                                    </td>
                                </tr>
                                <tr runat="server" id="changePasswordField">
                                    <td colspan="2">
                                        <A href="#" onclick="javascript: show_password_field();"><%=resources.LNG_CHANGE_PASSWORD %></a>
                                        </td>
                                </tr>
                                <tr runat="server" id="passwordFieldRow">
                                   <td> <%=resources.LNG_PASSWORD %>:</td>
                                    <td><input runat="server" autocomplete="off" name="pass" id="passwordField" type="password" value="" size="40" maxlength="255"/>
                                        <A href="#" runat="server" id="notChangeLink" onclick="javascript:hide_password_field();"><%=resources.LNG_NOT_CHANGE %></a>
                                    </td>
                                </tr>
                                <tr><td><%=resources.LNG_POLICY %>:</td><td>
                                    <select runat="server"  name="policySelector" id="policySelector" style="width:150px">
                                    </select>
                                    <a  runat="server" id="editPolicyLink" href="#" onclick="var val = document.getElementById('policySelector').value; if(val) window.open('EditPolicy.aspx?close=1&amp;id=' + val,'',555,450); return false;"> <%= resources.LNG_EDIT%></a>
                                    </td>
                                </tr>
                                </table>

                                </td></tr>
                                <tr><td align="right">
                                    <asp:LinkButton runat="server" id="cancelButton"  class="cancel_button" ><%=resources.LNG_CANCEL %></asp:LinkButton>
                                    <asp:LinkButton runat="server" id="acceptButton"  class="save_button"  ></asp:LinkButton>
                                </td></tr>
                            </table>
                        </div>

		            </td></tr></table>
            </table>

    </div>
        <input runat="server" type="hidden" id="keepPassword" name="keep_password" value="1"/>
    </form>
</body>
</html>
