<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%
check_session()
%>
<!-- #include file="Common.asp" -->
<!-- #include file="header.asp" -->
<!-- #include file="footer.asp" -->
<!-- #include file="libs/FusionCharts.asp" -->
<%
Server.ScriptTimeout = 600
Conn.CommandTimeout = 600

'On Error Resume Next
' index CustomIncludes End
'-------------------------------

'===============================
' Save Page and File Name available into variables
'-------------------------------
Dim sFileName
Dim sTemplateFileName
Dim sPaginateFileName
Dim intSessionUserId

sFileName = "statistics_da_surveys.asp"
sTemplateFileName = "statistics_da_surveys.html"
sPaginateFileName = "paginate.html"
'===============================
  

intSessionUserId = Session("uid")
uid=intSessionUserId

if (intSessionUserId <> "") then


' index Show begin

'===============================
' Display page
'-------------------------------
' Load HTML template for this page
'-------------------------------
LoadTemplate alerts_dir & "\" & templateFolder & "\" & sTemplateFileName, "main"
SetVar "DListSurveysStat", ""
'-------------------------------
' Load HTML template of Header and Footer
'-------------------------------
sHeaderFileName=Replace(sHeaderFileName,"header.html","header-old.html")
LoadTemplate sHeaderFileName, "Header"
LoadTemplate sFooterFileName, "Footer"

'-------------------------------
LoadTemplate sDateFormFileName, "DateForm"
LoadTemplate sMenuFileName, "Menu"

'SetVar "myvalue", "test"

Dim intInstallNum
Dim intUninstallNum
Dim rsToolbar
Dim rsInstall
Dim rsUninstall

Dim i
Dim days

Dim from_day
Dim from_month
Dim from_year

Dim to_day
Dim to_month
Dim to_year



' Get dates
start_date = Request("start_date")
end_date = Request("end_date")


if(Request("select_date_radio")="1" OR Request("select_date_radio")="") then
	if(Request("range")="1" OR Request("range")="") then
	        i=date()
		my_day = day(i)
		my_month = month(i)
		my_year = year (i)
	        i=date()
		my_day1 = day(i)
		my_month1 = month(i)
		my_year1 = year (i)

		test_date=make_date_by(my_day, my_month, my_year)
		test_date1=make_date_by(my_day1, my_month1, my_year1)

	end if
	if(Request("range")="2") then
	        i=date()-1
		my_day = day(i)
		my_month = month(i)
		my_year = year (i)

	        i=date()-1
		my_day1 = day(i)
		my_month1 = month(i)
		my_year1 = year (i)

		test_date=make_date_by(my_day, my_month, my_year)
		test_date1=make_date_by(my_day1, my_month1, my_year1)

	end if
	if(Request("range")="3") then

	        i=date()-7
		my_day = day(i)
		my_month = month(i)
		my_year = year (i)

	        i=date()
		my_day1 = day(i)
		my_month1 = month(i)
		my_year1 = year (i)

		test_date=make_date_by(my_day, my_month, my_year)
		test_date1=make_date_by(my_day1, my_month1, my_year1)

	end if

	if(Request("range")="4") then
		my_day = "01"
		my_month = month(date ())
		my_year = year (date())

		my_day1 = day(date())
		my_month1 = month(date())
		my_year1 = year(date())

		'date from config---	
		test_date=make_date_by(my_day, my_month, my_year)
		test_date1=make_date_by(my_day1, my_month1, my_year1)
		'-------------------

	end if
	
	if(Request("range")="5") then
		my_day = "01"
		my_month = month(date ())-1
		my_year = year (date())

		
		my_month1 = month(date ())-1
		my_year1 = year (date())
		my_day1 = "31"
		if(IsDate(my_month1 & "/" & my_day1 & "/" & my_year1)<>True) then
			my_day1="30"
			if(IsDate(my_month1 & "/" & my_day1 & "/" & my_year1)<>True) then
				my_day1="29"
				if(IsDate(my_month1 & "/" & my_day1 & "/" & my_year1)<>True) then
					my_day1="28"
					if(IsDate(my_month1 & "/" & my_day1 & "/" & my_year1)<>True) then
						my_day1="27"
					end if

				end if
	        	end if
	        end if
		'date from config---

		test_date=make_date_by(my_day, my_month, my_year)
		test_date1=make_date_by(my_day1, my_month1, my_year1)

		'-------------------
	end if
	if(Request("range")="6") then
		my_day = "01"
		my_month = "01"
		my_year = "2001"

		my_day1 = day(date())
		my_month1 = month(date())
		my_year1 = year(date())

		'date from config---
		test_date=make_date_by(my_day, my_month, my_year)
		test_date1=make_date_by(my_day1, my_month1, my_year1)

	end if
	from_date = test_date & " 00:00:00"
	to_date = test_date1 & " 23:59:59"
else
	'date from config---	
	
	from_date=start_date & " 00:00:00"
	
	to_date=end_date & " 23:59:59"

	'-------------------
end if



days = array ("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec")

if(Request("select_date_radio")="1" OR Request("select_date_radio")="") then setVar "strCheckedOption1", "checked" end if 

if(Request("select_date_radio")="2") then setVar "strEnabledOption1", "disabled" end if 

if(Request("range")="1") then setVar "strSelectedOption1", "selected" end if 
if(Request("range")="2") then setVar "strSelectedOption2", "selected" end if 
if(Request("range")="3") then setVar "strSelectedOption3", "selected" end if 
if(Request("range")="4") then setVar "strSelectedOption4", "selected" end if 
if(Request("range")="5") then setVar "strSelectedOption5", "selected" end if 
if(Request("range")="6") then setVar "strSelectedOption6", "selected" end if 

if(Request("select_date_radio")="2") then setVar "strCheckedOption2", "checked" end if 

if(Request("select_date_radio")="1" OR Request("select_date_radio")="") then setVar "strEnabledOption2", "disabled" end if 

SetVar "startDateValue", start_date
SetVar "endDateValue", end_date

   if(Request("offset") <> "" AND Request("offset") <> "{offset}") then 
	offset = clng(Request("offset"))
   else 
	offset = 0
   end if	

   if(Request("limit") <> "") then 
	limit = clng(Request("limit"))
   else 
	limit=50
   end if


SetVar "type", Request("type")
SetVar "limit", limit

'--- rights ---
set rights = getRights(uid, true, true, true, null)
gid = rights("gid")
gsendtoall = rights("gsendtoall")
gviewall = rights("gviewall")
gviewlist = rights("gviewlist")
'--------------

'---------------------------------


   searchSql = ""
  if(Request("uname") <> "") then 
     searchSql = " AND surveys_main.name LIKE '%" & Request("uname") & "%'"
  end if

if(Request("sortby") <> "" AND Request("sortby") <> "{sortby}") then 
	sortby = Request("sortby")
else 
	sortby = "create_date desc"
end if	

if gviewall = 1 and  Join(gviewlist,",") <> "" then
	Set RS = Conn.Execute("SELECT COUNT(surveys_main.id) as mycnt FROM surveys_main INNER JOIN alerts ON alerts.id = surveys_main.sender_id AND alerts.sender_id IN ("& Join(gviewlist,",") &") WHERE surveys_main.create_date >= '" & from_date & "' AND surveys_main.create_date <= '" & to_date & "' " & searchSql)
else
	Set RS = Conn.Execute("SELECT COUNT(id) as mycnt FROM surveys_main WHERE surveys_main.create_date >= '" & from_date & "' AND surveys_main.create_date <= '" & to_date & "' " & searchSql)
end if


cnt=0
if(Not RS.EOF) then
	cnt=RS("mycnt")
end if
RS.Close
j=cnt/limit


if(DA=1) then
else
	SetVar "strLogout", "Logout"
end if

PageName(LNG_STATISTICS_SURVEYS_STATISTICS)
GenerateMenu(3)

	SetVar "LNG_TYPE", LNG_TYPE
	SetVar "LNG_SURVEYS", LNG_NAME
	SetVar "LNG_DATE", LNG_DATE
	SetVar "LNG_SENT_BY", LNG_SENT_BY
	SetVar "LNG_RECIPIENTS", LNG_RECIPIENTS
	SetVar "LNG_OPEN_RATIO", LNG_OPEN_RATIO
    SetVar "Search", LNG_SEARCH
    SetVar "SearchTitle", LNG_SEARCH_SURVEY_BY_TITLE


if(cnt>0) then

	page=sFileName & "?from_day=" & Request("from_day") & "&from_month=" & Request("from_month") & "&from_year=" & Request("from_year") & "&to_day=" & Request("to_day") & "&to_month=" & Request("to_month") & "&to_year=" & Request("to_year") & "&range=" & Request("range") & "&select_date_radio=" & Request("select_date_radio") & "&"
	page=sFileName & "?start_date=" & start_date & "&end_date=" & end_date & "&range=" & Request("range") & "&select_date_radio=" & Request("select_date_radio") & "&uname=" & Server.URLEncode(myuname) & "&"
	name=LNG_SURVEYS
	
	
	SetVar "LNG_TYPE", sorting_post(LNG_TYPE,"stype", sortby, offset, limit)
	SetVar "LNG_SURVEYS", sorting_post(LNG_NAME,"name", sortby, offset, limit)
	SetVar "LNG_DATE", sorting_post(LNG_DATE,"create_date", sortby, offset, limit)
	SetVar "LNG_SENT_BY", sorting_post(LNG_SENT_BY,"users.name", sortby, offset, limit)
	
	SetVar "paging", make_pages_post(offset, cnt, limit,  name, sortby) 
		

'show main table
if gviewall = 1 and Join(gviewlist,",") <> "" then
	Set RS = Conn.Execute("SELECT users.name as uname, surveys_main.id, surveys_main.name, surveys_main.type, alerts.title as plain_title, surveys_main.stype, surveys_main.create_date, surveys_main.sender_id FROM surveys_main INNER JOIN alerts ON alerts.id = surveys_main.sender_id AND alerts.sender_id IN ("& Join(gviewlist,",") &") LEFT JOIN users ON users.id = alerts.sender_id WHERE surveys_main.create_date >= '" & from_date & "' AND surveys_main.create_date <= '" & to_date & "' " & searchSql &" ORDER BY " & sortby)
else
	
	Set RS = Conn.Execute("SELECT users.name as uname, surveys_main.id, surveys_main.name, alerts.title as plain_title, surveys_main.type, surveys_main.stype, surveys_main.create_date, surveys_main.sender_id FROM surveys_main INNER JOIN alerts ON alerts.id = surveys_main.sender_id LEFT JOIN users ON users.id = alerts.sender_id WHERE surveys_main.create_date >= '" & from_date & "' AND surveys_main.create_date <= '" & to_date & "' " & searchSql &" ORDER BY " & sortby)
end if
	num=offset
	if(Not RS.EOF) then RS.Move(offset) end if
	Do While Not RS.EOF
		num=num+1
		if((offset+limit) < num) then Exit Do end if
		if(num > offset AND offset+limit >= num) then
myrecipients=""
mysentby=""
	        text=RS("plain_title")
		strSurveyTitle = "<a href='#' onclick=""javascript: window.open('statistics_da_surveys_view_details.asp?id=" & CStr(RS("id")) & "&from_date=" & from_date & "&to_date=" & to_date & "','', 'status=0, toolbar=0, width=450, height=650, scrollbars=1')"">" & text & "</a>"
		strSurveyDate = RS("create_date")


  mysent=0
  myreceived=0
group_ids=""
groupFlag=0
group_cnt=0
broad_cnt=0

	strSurveyDate=RS("create_date")
	groupAlertId=RS("id")


			if(RS("type")="B") then
				myrecipients=LNG_BROADCAST
'broadcast surveys
if gsendtoall = 1 then
	Set RSUsers = Conn.Execute("SELECT COUNT(DISTINCT users.id) as mycnt FROM users WHERE ( client_version != 'web client' OR client_version is NULL ) AND reg_date <= '" & strSurveyDate & "'")
	if(not RSUsers.EOF) then 
		broadusers_cnt=RSUsers("mycnt") 
		broad_cnt=broad_cnt+broadusers_cnt
	end if
end if

			end if
			if(RS("type")="R") then
'group
	group_ids=""
	groupFlag=0
	Set RSGroupUsers = Conn.Execute("SELECT alerts_sent_group.group_id as group_id FROM alerts_sent_group INNER JOIN surveys_main ON surveys_main.sender_id=alerts_sent_group.alert_id WHERE alerts_sent_group.alert_id=" & groupAlertId)
	Do While not RSGroupUsers.EOF
		group_id=RSGroupUsers("group_id")
		if(groupFlag=1) then
			group_ids=group_ids & " OR " & " group_id=" & group_id 
		else
			group_ids=group_ids & " group_id=" & group_id 
		end if
		groupFlag=1
		RSGroupUsers.MoveNext	
	loop
	group_ids=replace(group_ids, "group_id", "users_groups.group_id")
	if(group_ids<>"") then
	Set RSUsers = Conn.Execute("SELECT COUNT(DISTINCT users.id) as mycnt FROM users INNER JOIN users_groups ON users.id=users_groups.user_id WHERE ("&group_ids&") AND reg_date <= '" & strSurveyDate & "'")
	if(not RSUsers.EOF) then 
		groupusers_cnt=RSUsers("mycnt") 
		group_cnt=group_cnt+groupusers_cnt
	end if
	end if

				Set RS6 = Conn.Execute("SELECT name FROM groups INNER JOIN alerts_group_stat ON groups.id=alerts_group_stat.group_id WHERE alerts_group_stat.alert_id="&RS("sender_id"))	
				if(Not RS6.EOF) then
					myrecipients=RS6("name")
				end if
				mmm=0
				do while not RS6.EOF
					mmm=mmm+1	
					RS6.MoveNext
				loop
				if(mmm>1) then myrecipients=myrecipients&", ..." end if
			end if
			if(RS("type")="R") then
				Set RS6 = Conn.Execute("SELECT name FROM users INNER JOIN alerts_sent_stat ON users.id=alerts_sent_stat.user_id WHERE alerts_sent_stat.alert_id="&RS("sender_id"))
				if(Not RS6.EOF) then
					if(myrecipients="") then
						myrecipients=RS6("name")
					else
						myrecipients=myrecipients&", ..."
					end if
				end if
				mmm=0
				do while not RS6.EOF
					mmm=mmm+1	
					RS6.MoveNext
				loop
				if(mmm>1) then myrecipients=myrecipients&", ..." end if

			end if

		mysentby=RS("uname")

  Set RS7 = Conn.Execute("SELECT COUNT(id) as mycnt FROM [alerts_sent_stat] WHERE alert_id="&RS("sender_id")&" AND [date] >= '" & from_date & "' AND [date] <= '" & to_date & "'")
  if (not RS7.EOF) then
	mysent=RS7("mycnt")
  end if





  myvotes=0
  Set RS7 = Conn.Execute("SELECT COUNT(DISTINCT user_id) as mycnt, SUM(vote) as votes FROM [alerts_received] WHERE alert_id="&RS("sender_id"))
  if (not RS7.EOF) then
	myreceived=RS7("mycnt")
	myvotes=RS7("votes")
  end if

  mysent=mysent+group_cnt+broad_cnt

  if(mysent<>0) then
    myopen=Round(clng(myreceived)/clng(mysent)*100,2)
  else
    myopen="0"
  end if
		myrecipients = "<A href='#' onclick=""javascript: window.open('view_recipients_list_surveys.asp?id=" & CStr(RS("id")) & "','', 'status=0, toolbar=0, width=555, height=450, scrollbars=1')"">" & myrecipients & "</a>"  
		
		stype = RS("stype")
		if stype = "3" then
			SetVar "typeImage", "poll"
			SetVar "typeTitle", LNG_POLL
		elseif stype = "2" then
			SetVar "typeImage", "quiz"
			SetVar "typeTitle", LNG_QUIZ
		else
			SetVar "typeImage", "survey"
			SetVar "typeTitle", LNG_SURVEY
		end if

		SetVar "strSurveyTitle", strSurveyTitle
		SetVar "strSurveyDate", strSurveyDate
		SetVar "strSurveySentby", mysentby
		SetVar "strSurveyRecipients", myrecipients
		SetVar "intOpenRatio", myopen
                Parse "DListSurveysStat", True
		
		end if
	RS.MoveNext
	Loop
	RS.Close

else

'Response.Write "<center><b>There are no alerts.</b></center>"

end if


'-------------


'-------------------------------
' Step through each form
'-------------------------------
Header_Show
Footer_Show
'-------------------------------
' Process page templates
'-------------------------------
Parse "Header", False
Parse "Footer", False
Parse "main", False
'-------------------------------
' Output the page to the browser
'-------------------------------
Response.write PrintVar("main")

' index Show End

'-------------------------------
' Destroy all object variables
'-------------------------------

' index Close Event begin
' index Close Event End

UnloadTemplate
'===============================

else

Login(Request("intErrorCode"))
'response.write "test111222"

end if

'===============================
' Display Grid Form
'-------------------------------

%><!-- #include file="db_conn_close.asp" -->