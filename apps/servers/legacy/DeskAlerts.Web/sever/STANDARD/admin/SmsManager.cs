using DeskAlerts.Server.Utils;
using DeskAlerts.Server.Utils.Extension;

namespace DeskAlertsDotNet
{
    using System;
    using System.Net;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Web;
    using Parameters;
    using PhoneNumbers;
    using static SmsManager;
    using JamaaTech.Smpp.Net.Client;
    using JamaaTech.Smpp.Net.Lib;
    using JamaaTech.Smpp.Net.Lib.Protocol;
    using NLog;
    using System.Threading;
    using DeskAlerts.Server.Utils.Interfactes;

    internal enum SmsFormat
    {
        NoFormat = 0,
        RemoveBrackets = 1,
        RemoveAllSpecialCharacters = 2,
        RemoveAllSpecialCharactersExceptPlus = 4,
        E164 = 8,
        E164WithoutPlus = 9
    }

    public class SmsResult
    {
        public static SmsResult Ok = new SmsResult()
        {
            StatusCode = HttpStatusCode.OK,
            Message = string.Empty
        };

        public HttpStatusCode StatusCode { get; set; }
        public string Message { get; set; }

        public override string ToString()
        {
            return string.Format("Error#{0} : {1}", StatusCode, Message);
        }
    }

    internal class SmppConfiguration : ISmppConfiguration
    {
        public int Id { get; set; }
        public int AutoReconnectDelay { get; set; }
        public string DefaultServiceType { get; set; }
        public DataCoding Encoding { get; set; }
        public string Host { get; set; }
        public bool IgnoreLength { get; set; }
        public int KeepAliveInterval { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public int Port { get; set; }
        public int ReconnectInteval { get; set; }
        public string SourceAddress { get; set; }
        public string SystemID { get; set; }
        public string SystemType { get; set; }
        public int TimeOut { get; set; }
        public string DestinationAddressRegex { get; set; }
        public RegisteredDelivery RegisteredDeliveryValue { get; set; }

        public SmppConfiguration()
        {
            Id = Settings.Content.GetInt("ConfSmppId");
            AutoReconnectDelay = Settings.Content.GetInt("ConfSmppAutoReconnectDelay");
            DefaultServiceType = Settings.Content["ConfSmppDefaultServiceType"];
            Encoding = (DataCoding)Enum.Parse(typeof(DataCoding), Settings.Content["ConfSmppEncoding"]);
            Host = Settings.Content["ConfSmppHost"];
            Password = Settings.Content["ConfSmppPassword"];
            Port = Settings.Content.GetInt("ConfSmppPort");
            ReconnectInteval = Settings.Content.GetInt("ConfSmppReconnectInteval");
            SourceAddress = Settings.Content["ConfSmppSourceAddress"];
            SystemID = Settings.Content["ConfSmppSystemID"];
            SystemType = Settings.Content["ConfSmppSystemType"];
            TimeOut = Settings.Content.GetInt("ConfSmppTimeOut");
            RegisteredDeliveryValue = (RegisteredDelivery)Enum.Parse(typeof(RegisteredDelivery), Settings.Content["ConfSmppRegisteredDeliveryValue"]);
            KeepAliveInterval = Settings.Content.GetInt("ConfSmppKeepAliveInterval");
        }
    }

    internal class SmsConfiguration
    {
        public string SmsUrl { get; set; }
        public string CountryCode { get; set; }
        public string PostData { get; set; }
        public string ContentType { get; set; }
        public bool UseAuth { get; set; }
        public string AuthUser { get; set; }
        public string AuthPassword { get; set; }
        public bool UseProxy { get; set; }
        public string ProxyServer { get; set; }
        public string ProxyServerByPassList { get; set; }
        public string ProxyUser { get; set; }
        public string ProxyPassword { get; set; }
        public string AuthKey { get; set; }
        public AuthTypesEnum AuthorizationType { get; set; }
        public SmsFormat NumberFormat { get; set; }

        public SmsConfiguration()
        {
            SmsUrl = Settings.Content["ConfSmsUrl"];
            CountryCode = Settings.Content["ConfCountryCode"];
            PostData = Settings.Content["ConfSmsPostData"];
            ContentType = Settings.Content["ConfSmsContentType"];
            AuthUser = Settings.Content["ConfSmsAuthUser"];
            AuthPassword = Settings.Content["ConfSmsAuthPass"];
            UseProxy = Settings.Content.GetInt("ConfSmsProxyEnable") == 1;
            ProxyServer = Settings.Content["ConfSmsProxyServer"];
            ProxyUser = Settings.Content["ConfSmsProxyUser"];
            ProxyPassword = Settings.Content["ConfSmsProxyPass"];
            AuthKey = Settings.Content["ConfSmsAuthKey"];
            AuthorizationType = (AuthTypesEnum)Settings.Content.GetInt("ConfSmsAuthType");
        }
    }

    public class SmsManager
    {
        private static SmsManager _instance;

        private ISmppConfiguration _smppConfig;

        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public static SmsManager Default => _instance ?? (_instance = new SmsManager());

        private SmsConfiguration _smsSettings;

        public SmsResult Send(string text, string destanationSource)
        {
            return SendSmsAndGetResponse(text, destanationSource);
        }

        // "0" - Don't convert phone numbers,
        // "1" - Just remove all special chars but not grouping symbols,
        // "2" - Just remove all special chars but not grouping symbols and not lead plus "+" symbol,
        // "3" - Just remove all special chars,
        // "4" - Just remove all special chars but not lead plus "+" symbol,
        // "5" - Convert letters to numeric,
        // "6" - Convert letters to numeric and remove all special chars,
        // "7" - Convert letters to numeric and remove all special chars but not lead plus "+" symbol,
        // "8" - Use international E.164 phone format,
        // "9" - Use international E.164 phone format but without lead plus "+" symbol.

        private static string FormatPhoneNumber(string mobileNumber)
        {
            string countryCode = Config.Data.GetString("smsDefaultCountryCode");

            if (string.IsNullOrEmpty(countryCode))
                return mobileNumber;

            var phoneNumber = PhoneNumberUtil.GetInstance().Parse(mobileNumber, countryCode);

            var smsPhoneFormat = (SmsFormat)Config.Data.GetInt("smsPhoneFormat");

            switch (smsPhoneFormat)
            {
                case SmsFormat.NoFormat:
                    {
                        break;
                    }

                case SmsFormat.RemoveBrackets:
                    {
                        return Regex.Replace(mobileNumber, @"\(|\)", string.Empty);
                    }

                case SmsFormat.RemoveAllSpecialCharacters:
                    {
                        return Regex.Replace(mobileNumber, @"\(|\)|\+|-", string.Empty);
                    }

                case SmsFormat.RemoveAllSpecialCharactersExceptPlus:
                    {
                        return Regex.Replace(mobileNumber, @"\(|\)|-", string.Empty);
                    }

                case SmsFormat.E164:
                    {
                        return PhoneNumberUtil.GetInstance().Format(phoneNumber, PhoneNumberFormat.E164);
                    }

                case SmsFormat.E164WithoutPlus:
                    {
                        string e164PhoneNumber = PhoneNumberUtil.GetInstance().Format(phoneNumber, PhoneNumberFormat.E164);

                        if (e164PhoneNumber[0] == '+')
                            e164PhoneNumber = e164PhoneNumber.Substring(1);

                        return e164PhoneNumber;
                    }
            }

            return mobileNumber;
        }

        private SmsResult SendSmsAndGetResponse(string textMessage, string destinationSource)
        {
            _smsSettings = new SmsConfiguration();

            destinationSource = FormatPhoneNumber(destinationSource);
            textMessage = DeskAlertsUtils.EscapeHtmlTagParagraph(textMessage);
            textMessage = DeskAlertsUtils.EscapeXmlComments(textMessage);
            textMessage = HttpUtility.HtmlDecode(textMessage);
            textMessage = textMessage?.Trim();
            if (textMessage != null)
            {
                var escapedTextMessage = Uri.EscapeDataString(textMessage);
                if (Settings.Content.GetInt("ConfSmppEnabled") == 1)
                {
                    SendSmppMessage(textMessage, destinationSource);
                }

                string smsUrl = _smsSettings.SmsUrl;
                if (smsUrl.Length <= 0)
                {
                    return SmsResult.Ok;
                }

                smsUrl = smsUrl.Replace("%mobilePhone%", destinationSource).Replace("%smsText%", escapedTextMessage);
                string postData = _smsSettings.PostData;
                postData = HttpUtility.HtmlDecode(postData);
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                var smsRequest = (HttpWebRequest) WebRequest.Create(smsUrl);
                smsRequest.Method = "GET";
                if (_smsSettings.UseProxy)
                {
                    smsRequest.Proxy = new WebProxy(_smsSettings.ProxyServer)
                    {
                        Credentials = new NetworkCredential(_smsSettings.ProxyUser, _smsSettings.ProxyPassword)
                    };
                }

                switch (_smsSettings.AuthorizationType)
                {
                    case AuthTypesEnum.NoAuth:
                        break;
                    case AuthTypesEnum.AuthKey:
                        smsRequest.Headers.Add(HttpRequestHeader.Authorization, _smsSettings.AuthKey);
                        break;
                    case AuthTypesEnum.BasicAuth:
                        smsRequest.Credentials = new NetworkCredential(_smsSettings.AuthUser, _smsSettings.AuthPassword);
                        smsRequest.AddAuthHeaders(_smsSettings.AuthUser, _smsSettings.AuthPassword);
                        break;
                    case AuthTypesEnum.Zenvia:
                        smsRequest.AddAuthHeaders(_smsSettings.AuthUser, _smsSettings.AuthPassword);
                        smsRequest.Accept = "application/json";
                        break;
                }

                if (postData.Length > 0)
                {
                    postData = postData.Replace("%mobilePhone%", destinationSource).Replace("%smsText%", textMessage);
                    smsRequest.Method = "POST";

                    string contentType = _smsSettings.ContentType;
                    if (string.IsNullOrEmpty(contentType))
                        contentType = "application/x-www-form-urlencoded";
                    smsRequest.ContentType = contentType;

                    var postDataBytes = Encoding.UTF8.GetBytes(postData);
                    smsRequest.ContentLength = postDataBytes.Length;

                    using (var stream = smsRequest.GetRequestStream())
                    {
                        stream.Write(postDataBytes, 0, postDataBytes.Length);
                        stream.Close();
                    }
                }

                try
                {
                    var response = (HttpWebResponse)smsRequest.GetResponse();
                    return SmsResult.Ok;
                }
                catch (WebException ex)
                {
                    Logger logger = LogManager.GetCurrentClassLogger();
                    logger.Debug(ex);

                    var response = ex.Response as HttpWebResponse;


                    var errorResult = new SmsResult()
                    {
                        StatusCode = response.StatusCode,
                        Message = ex.Message
                    };

                    return errorResult;
                }
            }
            else
            {
                SmsResult errorResult = new SmsResult()
                {
                    StatusCode = HttpStatusCode.InternalServerError,
                    Message = "Error when decode alert text"
                };

                return errorResult;
            }
        }

        private void SendSmppMessage(string textMessage, string destinationSource)
        {
            _smppConfig = new SmppConfiguration();
            var client = CreateSmppClient(_smppConfig);
            client.Start();
            int maxTry = 3;
            int numbersOfTry = 0;
            while (client.ConnectionState != SmppConnectionState.Connected && numbersOfTry <= maxTry)
            {
                Thread.Sleep(_smppConfig.AutoReconnectDelay);
                numbersOfTry++;
            }

            if(numbersOfTry > maxTry)
            {
                _logger.Debug($"Can't connect to server {_smppConfig.Host}");
                return;
            }

            do
            {
                TextMessage msg = new TextMessage();

                msg.DestinationAddress = destinationSource;
                msg.SourceAddress = _smppConfig.SourceAddress;
                msg.Text = textMessage;
                msg.RegisterDeliveryNotification = true; //I want delivery notification for this message
                msg.UserMessageReference = Guid.NewGuid().ToString();
                _logger.Debug($"msg.UserMessageReference: {msg.UserMessageReference}");

                client.SendMessage(msg);
                return;
            } while (true);
        }

        private SmppClient CreateSmppClient(ISmppConfiguration config)
        {
            var client = new SmppClient();
            client.Name = config.Name;
            client.SmppEncodingService = new SmppEncodingService(System.Text.Encoding.UTF8);

            client.ConnectionStateChanged += new EventHandler<ConnectionStateChangedEventArgs>(client_ConnectionStateChanged);
            client.StateChanged += new EventHandler<StateChangedEventArgs>(client_StateChanged);
            client.MessageSent += new EventHandler<MessageEventArgs>(client_MessageSent);
            client.MessageDelivered += new EventHandler<MessageEventArgs>(client_MessageDelivered);
            client.MessageReceived += new EventHandler<MessageEventArgs>(client_MessageReceived);

            SmppConnectionProperties properties = client.Properties;
            properties.SystemID = config.SystemID;
            properties.Password = config.Password;
            properties.Port = config.Port;
            properties.Host = config.Host;
            properties.SystemType = config.SystemType;
            properties.DefaultServiceType = config.DefaultServiceType;
            properties.DefaultEncoding = config.Encoding;

            client.AutoReconnectDelay = config.AutoReconnectDelay;

            client.KeepAliveInterval = config.KeepAliveInterval;

            return client;
        }

        private void client_ConnectionStateChanged(object sender, ConnectionStateChangedEventArgs e)
        {
            var client = (SmppClient)sender;

            if (client.LastException != null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(client.LastException.ToString());
                Console.ResetColor();
            }

            switch (e.CurrentState)
            {
                case SmppConnectionState.Closed:
                    {
                        _logger.Debug("Connection to the remote server is lost");
                        e.ReconnectInteval = _smppConfig.ReconnectInteval;
                        break;
                    }
                case SmppConnectionState.Connected:
                    _logger.Debug("A successful connection has been established");
                    break;
            }
        }

        private void client_StateChanged(object sender, StateChangedEventArgs e)
        {
            var client = (SmppClient)sender;
            _logger.Debug("SMPP client {0}: {1}", client.Name, e.Started ? "STARTED" : "STOPPED");
        }

        private void client_MessageSent(object sender, MessageEventArgs e)
        {
            var client = (SmppClient)sender;
            _logger.Debug("SMPP client {0} - Message Sent to: {1} {2}", client.Name, e.ShortMessage.DestinationAddress, e.ShortMessage.UserMessageReference);
        }

        private void client_MessageDelivered(object sender, MessageEventArgs e)
        {
            var client = (SmppClient)sender;
            _logger.Debug("SMPP client {0} - Message Delivered: MessageId: {1}", client.Name, e.ShortMessage.UserMessageReference);
        }

        private void client_MessageReceived(object sender, MessageEventArgs e)
        {
            var client = (SmppClient)sender;
            TextMessage msg = e.ShortMessage as TextMessage;
            _logger.Debug("SMPP client {0} - Message Received from: {1}, msg: {2}", client.Name, msg.SourceAddress, msg.Text);
        }

        public enum AuthTypesEnum
        {
            NoAuth = 0,
            BasicAuth = 1,
            AuthKey = 2,
            Zenvia = 3
        }
    }
}