﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<!-- #include file="ad.inc" -->
<%

check_session()

uid = Session("uid")

if (uid <>"") then
	SQL = "SELECT guide, dis_guide FROM users WHERE id="&uid
	Set RSGuide = Conn.Execute(SQL)
	if not RSGuide.EOF then
		if isNull(RSGuide("guide")) then
			showGuide = "none"
		else
			showGuide = RSGuide("guide")
		end if
		if isNull(RSGuide("dis_guide")) then
			guideHistory = true
		else
			if InStr(RSGuide("dis_guide"),"domains")>0 then
				guideHistory = false
			else
				guideHistory = true
			end if
		end if
		disabledGuides = RSGuide("dis_guide")
	end if
end if
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>DeskAlerts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/style9.css" rel="stylesheet" type="text/css">
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script language="javascript" type="text/javascript">

$(document).ready(function() {
	var width = $(".data_table").width();
	var pwidth = $(".paginate").width();
	if (width != pwidth) {
		$(".paginate").width("100%");
	}
	$("#add_domain_button").button({
		icons : {
			primary : "ui-icon-plusthick"
		}
	});
	
	$("#import_from_groups_button").button({});
	
	$("#sync_button").button({
		
	});
	
	$("#guideDialog").dialog({
		autoOpen:false,
		resizable:false,
		draggable:false,
		modal:true,
		width:400,
		height:160,
		close: function(){closeGuide();}
	});
	
	function showGuide(){
		$("#guideDialog").dialog("open");
	}
	
	function closeGuide(){
		var disabledGuides = "<%=disabledGuides %>"
		disabledGuides += "domains,";
		$.get("set_settings.asp",{name:"DisabledGuides", value: disabledGuides});
	}
	
	function checkHistoryGuide(){
		<% if guideHistory=true then %>
		showGuide();
		<% end if %>
	}
	
	<% if showGuide="new" then%>
		checkHistoryGuide();
	<% elseif showGuide="all" then %>
		showGuide();
	<% end if %>
});

</script>
<script language="javascript" type="text/javascript" src="functions.js"></script>
</head>

<%


  flag=1
  if(flag <> 1) then
%>
<body style="margin:0px" class="body">

<script type="text/javascript">
// code to create and make AJAX request
function getFile(pURL) {
    if (window.XMLHttpRequest) { // code for Mozilla, Safari, etc 
        xmlhttp=new XMLHttpRequest();
    } else if (window.ActiveXObject) { //IE 
        xmlhttp=new ActiveXObject('Microsoft.XMLHTTP');
    }

    // if we have created the xmlhttp object we can send the request
    if (typeof(xmlhttp)=='object') {
        xmlhttp.onreadystatechange=xmlhttpResults;
        xmlhttp.open('GET', pURL, true);
        xmlhttp.send(null);
        // replace the submit button image with a please wait for the results image.
//        document.getElementById('theImage').innerHTML = '<img id="submitImage" name="submitImage" src="images/ajaxCallChangeImage_wait.gif" alt="Please Wait for AJAX Results">';
    // otherwise display an error message
    } else {
        alert('Your browser is not remote scripting enabled. You can not run this example.');
    }
}

// function to handle asynchronous call
function xmlhttpResults() {
    if (xmlhttp.readyState==4) { 
        if (xmlhttp.status==200) {
            // we use a delay only for this example
            setTimeout('loadResults()',3000);
        }
    }
}

function loadResults() {
    // put the results on the page
    if(xmlhttp.responseText=="1")
    {
	    location.href="admin_domains.asp"
    }
    else
    {
	    document.getElementById('mydiv').innerHTML = "<table width='100%' height='100%' border='0'><tr><td align='center'><b>Error: Synchronizing with Active Directory failed.</b></td></tr></table>";
    }
}


</script>
<div name="mydiv" id="mydiv">
<table width="100%" height="100%" border="0"><tr><td align="center">
<br><br>  <img src="images/ajax-loader.gif" border="0"> <br><br>  <b>Synchronizing with Active Directory. Please wait.</b>
</td>
</tr>
</table>
</div>

<%
  Session("flag") = 1
  else


%>
<body style="margin:0px" class="body">
<%
   if(Request("offset") <> "") then 
	offset = clng(Request("offset"))
   else 
	offset = 0
   end if	


if (uid <>"") then
Dim group_arr(9000)

	Set RS = Conn.Execute("SELECT id FROM users WHERE id="&uid&" AND role='E'")
	if(Not RS.EOF) then
		gid = 1
		Set rs_policy = Conn.Execute("SELECT type FROM policy INNER JOIN policy_editor ON policy.id=policy_editor.policy_id WHERE editor_id=" & uid)
		if(Not rs_policy.EOF) then
			if(rs_policy("type")="A") then
				gid = 0
			end if
		end if
		if(gid=1) then
			editor_id=RS("id")
		end if
	else
		gid = 0
	end if
	RS.Close



'counting pages to display
  limit=10
	if ConfEnableRegAndADEDMode = 1 then
		where = ""
	else
		where = "WHERE name <> ''"
	end if

	Set RS = Conn.Execute("SELECT COUNT(id) as mycnt FROM domains " & where)
	cnt=0

	if(Not RS.EOF) then
		cnt=RS("mycnt")
	end if

	RS.Close
  j=cnt/limit


%>
<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	<tr>
	<td>
	<table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
		<tr>
		<td width=100% height=31 class="main_table_title"><img src="images/menu_icons/domains_20.png" alt="" width="20" height="20" border="0" style="padding-left:7px">
		<a href="admin_domains.asp" class="header_title" style="position:absolute;top:15px"><%=LNG_DOMAINS %></a></td>
		</tr>
		<tr>
		<td class="main_table_body" height="100%">
		<% if(Request("err")="1") then Response.Write "<br><center><b><font color=""red"">Error: this special id already exists! Please try another one.</font></b></center>" end if %>
		<div style="margin:10px;"> 
		<% if(gid=0) then %>
		<div align="right">

		<% if(EDIR=1) then %>
		<a href="edir_sync_form.asp" id="add_domain_button">
		<%=LNG_ADD_NEW_DOMAIN%></a></div><br>
		<%else%>
			<%if ConfEnableGroupsImport = 1 then %>
				<a href="ad_import_upload_form.asp" id="import_from_groups_button" target="_blank">
				<%=LNG_IMPORT_FROM_FILES%></a>
			<%end if %>
		<a href="synchronizations.asp" id="sync_button">
		<%=LNG_SYNCHRONIZATIONS%></a></div><br>
		<%end if%>
		
		<% end if %>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr><td> 

<% 
if(cnt>0) then
	page="admin_domains.asp?"
	name=LNG_DOMAINS
	Response.Write make_pages(offset, cnt, limit, page, name, sortby) 

%>

		</td></tr>
		</table>
		<table width="100%" height="100%" class="data_table" cellspacing="0" cellpadding="3">
		<tr class="data_table_title">
		<td class="table_title"><%=LNG_DOMAIN %></td>
		<td class="table_title"><%=LNG_USERS %></td>
		<td class="table_title"><%=LNG_GROUPS %></td>
		<td class="table_title"><%=LNG_COMPUTERS %></td>
		<td class="table_title"><%=LNG_ACTIONS %></td>
				</tr>
<%

'show main table
	if ConfEnableRegAndADEDMode = 1 then
		where = ""
	else
		where = "WHERE d.name <> ''"
	end if
	Set RS = Conn.Execute("SELECT d.id, d.name, (SELECT COUNT(1) FROM users WHERE domain_id=d.id) AS cnt0, (SELECT COUNT(1) FROM groups WHERE domain_id=d.id) AS cnt1, (SELECT COUNT(1) FROM computers WHERE domain_id=d.id) AS cnt2 FROM domains d "&where&" ORDER BY id DESC")
	num=offset
	if(Not RS.EOF) then RS.Move(offset) end if
	Do While Not RS.EOF
		num=num+1
		if((offset+limit) < num) then Exit Do end if
		if(num > offset AND offset+limit >= num) then
		cnt0=RS("cnt0")
		cnt1=RS("cnt1")
		cnt2=RS("cnt2")
	%>
		<tr><td>
		<A href="ou_new_interface.asp?domain_id=<%=RS("id")%>">
		<%
			if Len(RS("name")) = 0 then
				Response.Write LNG_NO_DOMAIN
			else
				Response.Write RS("name")
			end if
		%></a>
		</td>
		<td><%=cnt0 %></td>
		<td><%=cnt1 %></td>
		<td><%=cnt2 %></td>
		<td align="center">
		<% if(gid=0) AND Len(RS("name")) > 0 then %>
			<%if(EDIR=1) then%>
				<a href="edir_sync_form.asp?id=<%=RS("id") %>"><img src="images/action_icons/edit.png" alt="<%=LNG_EDIT_DOMAIN %>" title="<%=LNG_EDIT_DOMAIN %>" width="16" height="16" border="0" hspace=5></a><%end if%>
				<a href="domain_delete.asp?id=<%=RS("id") %>" onclick="javascript: return LINKCLICK('<%=LNG_DOMAINS %>');"><img src="images/action_icons/delete.gif" alt="<%=LNG_DELETE_DOMAIN %>" title="<%=LNG_DELETE_DOMAIN %>" width="16" height="16" border="0" hspace=5></a>
			<% end if %>
		</td></tr>
		<%
		end if


		RS.MoveNext
	Loop
	RS.Close

%>			
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr><td> 

<% 

	Response.Write make_pages(offset, cnt, limit, page, name, sortby) 

else
	Response.Write LNG_THERE_ARE_NO_DOMAINS & "."
end if
		%>

		</td></tr>
		</table>
 
                </table>
<!--		<img src="images/inv.gif" alt="" width="1" height="100%" border="0">-->
		</div> 
		</td>
		</tr>
	</table>
	<div id="guideDialog" title="DeskAlerts Beginner's Guide" style="display: none">
		<div>
			<p>
				This is the list of the Active Directory domains synchronized with the DeskAlerts system. To perform such synchronizations, go to the Synchronizations page.
			</p>
			<p>
				To learn more about synchronizing the DeskAlerts with the Active Directory, see the <a href="http://www.alert-software.com/support/documentation/administrators-manual/" target="_blank">DeskAlerts Administrator's Manual</a>.
			</p>
		</div>
	</div>
	</td>
	</tr>
</table>
<%
else

  Response.Redirect "index.asp"

end if
end if
%> 
</body>
</html>
<!-- #include file="db_conn_close.asp" -->