﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<!-- #include file="ad.inc" -->
<%

check_session()

on error resume next

  uid = Session("uid")

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>DeskAlerts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/style9.css" rel="stylesheet" type="text/css">
</head>
<script language="javascript">
function LINKCLICK() {
  alert ("This action is disabled in demo");
  return false;
}
</script>

<body style="margin:0px" class="iframe_body">

<%

   if(Request("sortby") <> "") then 
	sortby = Request("sortby")
   else 
	sortby = "name"
   end if	
   
   if(Request("offset") <> "") then 
	offset = clng(Request("offset"))
   else 
	offset = 0
   end if	

if (uid <>"") then

'counting pages to display

  limit=50
  cont_id = clng(Request("id"))
  member_of = clng(Request("member_of"))
  if(member_of=1) then
		whereSQL = "groups_groups.group_id"
		joinSQL = "groups_groups.container_group_id"
	else
		whereSQL = "groups_groups.container_group_id"
		joinSQL = "groups_groups.group_id"
  end if
if (AD = 3 OR EDIR = 1) then

	Set RS1 = Conn.Execute("SELECT COUNT(group_id) as mycnt FROM groups_groups WHERE "&whereSQL&"=" & cont_id)
	if(Not RS1.EOF) then
		cnt=RS1("mycnt")
	end if
	j=cnt/limit
end if

  	Set RS = Conn.Execute("SELECT domains.name as name, domain_id, groups.name as gr_name  FROM domains INNER JOIN groups ON groups.domain_id=domains.id WHERE groups.id=" & Request("id"))
	if(Not RS.EOF) then
if(member_of=1) then
%>
	<%=LNG_GROUP %>: <b><% Response.Write RS("gr_name") %></b> <%=LNG_DOMAIN %>: <b><% Response.Write RS("name") %></b><br/>  <%=LNG_VIEW_MEMBER_OF %> | <A href="view_group_group.asp?id=<%=request("id")%>"><%=LNG_VIEW_GROUPS %></a> | <A href="view_user_group.asp?id=<%=request("id")%>"><%=LNG_VIEW_USERS %></a> | <A href="view_comp_group.asp?id=<%=request("id")%>"><%=LNG_VIEW_COMPUTERS %></a><br/><br/>
<%

else
%>
	<%=LNG_GROUP %>: <b><% Response.Write RS("gr_name") %></b> <%=LNG_DOMAIN %>: <b><% Response.Write RS("name") %></b><br/>  <A href="view_group_group.asp?id=<%=request("id")%>&member_of=1"><%=LNG_VIEW_MEMBER_OF %></a> | <%=LNG_VIEW_GROUPS %> | <A href="view_user_group.asp?id=<%=request("id")%>"><%=LNG_VIEW_USERS %></a> | <A href="view_comp_group.asp?id=<%=request("id")%>"><%=LNG_VIEW_COMPUTERS %></a><br/><br/>
<%
end if
	end if 
if(cnt>0) then

	page="view_group_group.asp?gname=" & mygname & "&id=" & Request("id") & "&"
	name=LNG_GROUPS
	Response.Write make_pages(offset, cnt, limit, page, name, sortby) 

%>

		</td></tr>
		</table>
		<table width="100%" height="100%" class="data_table" cellspacing="0" cellpadding="3">
		<tr class="data_table_title">
		<td class="table_title"><% response.write sorting(LNG_GROUP_NAME ,"name", sortby, offset, limit, page) %></td>
		<td class="table_title"><% response.write sorting(LNG_DOMAIN,"domain_name", sortby, offset, limit, page) %></td>
		<td class="table_title" ><% response.write sorting(LNG_USERS,"users_cnt", sortby, offset, limit, page) %></td>
<%
	if(EDIR<>1) then
%>
		<td class="table_title" ><% response.write sorting(LNG_COMPUTERS,"computers_cnt", sortby, offset, limit, page) %></td>
<% 
	end if 
%>
<%if(EDIR=1) then%>		<td class="table_title"><%=LNG_CONTEXT %></td> <%end if%>
		<td class="table_title" ><% response.write sorting(LNG_GROUPS,"groups_cnt", sortby, offset, limit, page) %></td>
		<td class="table_title" ><%=LNG_ACTIONS %></td>				
				</tr>
<%

'show main table
    Response.Write("SELECT groups.id as id, context_id, groups.name as name, domains.name as domain_name, custom_group, COUNT(distinct users_groups.user_id) as users_cnt, COUNT(distinct computers_groups.comp_id) as computers_cnt FROM groups LEFT JOIN domains ON domains.id=groups.domain_id LEFT JOIN users_groups ON groups.id=users_groups.group_id LEFT JOIN computers_groups ON groups.id=computers_groups.group_id INNER JOIN groups_groups ON groups.id="&joinSQL&" WHERE domains.id<>0 AND "&whereSQL&"="&cont_id&" GROUP BY groups.id, groups.name,  domains.name, custom_group, context_id ORDER BY "&sortby)
	Set RS = Conn.Execute("SELECT groups.id as id, context_id, groups.name as name, domains.name as domain_name, custom_group, COUNT(distinct users_groups.user_id) as users_cnt, COUNT(distinct computers_groups.comp_id) as computers_cnt FROM groups LEFT JOIN domains ON domains.id=groups.domain_id LEFT JOIN users_groups ON groups.id=users_groups.group_id LEFT JOIN computers_groups ON groups.id=computers_groups.group_id INNER JOIN groups_groups ON groups.id="&joinSQL&" WHERE domains.id<>0 AND "&whereSQL&"="&cont_id&" GROUP BY groups.id, groups.name,  domains.name, custom_group, context_id ORDER BY "&sortby)		

	num=0
	Do While Not RS.EOF
		num=num+1
		if(num > offset AND offset+limit >= num) then
		cnt1=0


			Set RS1 = Conn.Execute("SELECT count(group_id) as groups_cnt FROM groups_groups WHERE container_group_id=" & CStr(RS("id")))
			if(Not RS1.EOF) then
				cnt1=RS1("groups_cnt")
				RS1.Close
			end if


        	%>
		<tr><td>
		<% Response.Write RS("name") %>
		</td><td>
		<% Response.Write RS("domain_name") %>
		</td>
		<td align="center">
		<a href="view_user_group.asp?id=
		<% Response.Write RS("id") %>
		"><% Response.Write RS("users_cnt") %></a>
		</td>
<%
		if(EDIR<>1) then
%>
		<td align="center"> 
		<a href="view_comp_group.asp?id=
		<% Response.Write RS("id") %>
		"><% Response.Write RS("computers_cnt") %></a>
		</td>
<%
		end if
		 if (EDIR = 1) then 	
			Set RS3 = Conn.Execute("SELECT name FROM edir_context WHERE id=" & RS("context_id"))
			if(Not RS3.EOF) then
				  strContext=RS3("name")
				RS3.close
			else
				strContext="&nbsp;"
			end if

			strContext=replace (strContext,",ou=",".")
			strContext=replace (strContext,",o=",".")
			strContext=replace (strContext,",dc=",".")
			strContext=replace (strContext,",c=",".")

			strContext=replace (strContext,"ou=","")
			strContext=replace (strContext,"o=","")
			strContext=replace (strContext,"dc=","")
			strContext=replace (strContext,"c=","")

			Response.Write "<td>" & strContext & "</td>"
		 end if

%>

		<td align="center"> 
		<a href="view_group_group.asp?id=
		<% Response.Write RS("id") %>
		"><% Response.Write cnt1 %></a>
		</td>
		
		<td align="center">
		
		<a href="view_group_group.asp?id=
		<% Response.Write RS("id") %>&member_of=1"><img src="images/action_icons/preview.png" alt="<%=LNG_VIEW_MEMBER_OF %>" title="<%=LNG_VIEW_MEMBER_OF %>" width="16" height="16" border="0" hspace=5></a>
		<%
		if (AD <> 0) then

		if(RS("custom_group")=1) then
			if(gid = 0 OR (gid = 1 AND groups_arr(1)<>"")) then
		%>
		<a href="edit_group.asp?id=
		<% Response.Write RS("id") %>&domain_id=<% response.write Request("id") %>
		"><img src="images/action_icons/edit.png" alt="<%=LNG_EDIT_GROUP %>" title="<%=LNG_EDIT_GROUP %>" width="16" height="16" border="0" hspace=5></a>
		<% end if %>
		<% if(gid = 0 OR (gid = 1 AND groups_arr(2)<>"")) then %>
		<a href="group_delete.asp?id=
		<% Response.Write RS("id") %>&domain_id=<% response.write Request("id") %>
		"><img src="images/action_icons/delete.gif" alt="<%=LNG_DELETE_GROUP %>" title="<%=LNG_DELETE_GROUP %>" width="16" height="16" border="0" hspace=5></a>
		<%
			end if
		end if

		else
		%>
		<a href="edit_group.asp?id=
		<% Response.Write RS("id") %>
		"><img src="images/action_icons/edit.png" alt="<%=LNG_EDIT_GROUP %>" title="<%=LNG_EDIT_GROUP %>" width="16" height="16" border="0" hspace=5></a><a href="group_delete.asp?id=
		<% Response.Write RS("id") %>
		"><img src="images/action_icons/delete.gif" alt="<%=LNG_DELETE_GROUP %>" title="<%=LNG_DELETE_GROUP %>" width="16" height="16" border="0" hspace=5></a>
		<%
		end if
		%>
		</td></tr>
		<%
		end if
	RS.MoveNext
	Loop
	RS.Close

%>			
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr><td> 


<%

	Response.Write make_pages(offset, cnt, limit, page, name, sortby) 

%>

		</td></tr>
		</table>

<%
else
	Response.Write "<center><b>"& LNG_THERE_ARE_NO_GROUPS &".</b><br><br></center>"	
end if
else

  Response.Redirect "index.asp"

end if
%> 
</body>
</html>
<!-- #include file="db_conn_close.asp" -->