﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Parameters;


namespace DeskAlertsDotNet.Pages
{
    public partial class EditTextTemplate : DeskAlertsBasePage
    {

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (!string.IsNullOrEmpty(Request["id"]))
            {
                LoadTemplate(Request["id"]);
                edit.Value = "1";
                templateId.Value = Request["id"];
            }
        }

        new void LoadTemplate(string id)
        {
            DataRow templateRow =
                dbMgr.GetDataByQuery("SELECT name, template_text FROM text_templates WHERE id = " + id).First();

            templateName.Value = templateRow.GetString("name");
            templateContent.InnerHtml = templateRow.GetString("template_text");
        }
    }
}