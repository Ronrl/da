﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Pages;
using DeskAlertsDotNet.Parameters;

namespace DeskAlertsDotNet.Pages
{
    public partial class GroupEdit : DeskAlertsBasePage
    {
        private DBManager _dbManager;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["PreviousPage"] = Request.UrlReferrer;
            }

            lbTitle.Text = Localization.Current.Content["LNG_GROUPS"];
            lbHeader.Text = Localization.Current.Content["LNG_ADD_GROUP_POLICY"];
            lbSelectGroup.Text = Localization.Current.Content["LNG_SELECT_GROUP"];
            lbSelectPolicy.Text = Localization.Current.Content["LNG_SELECT_POLICY"];
            btnCancel.Text = Localization.Current.Content["LNG_CANCEL"];
            btnSave.Text = Localization.Current.Content["LNG_SAVE_AND_NEXT"];

            _dbManager = new DBManager();

            ddlGroups.DataSource = _dbManager.GetDataTableByQuery("SELECT id, name FROM groups");
            ddlGroups.Items.Insert(0, new ListItem(Localization.Current.Content["LNG_SELECT_GROUP"]));
            ddlGroups.DataTextField = "name";
            ddlGroups.DataValueField = "id";
            ddlGroups.DataBind();

            ddlPolicy.DataSource = _dbManager.GetDataTableByQuery("SELECT id, name FROM policy");
            ddlPolicy.Items.Insert(0, new ListItem(Localization.Current.Content["LNG_SELECT_POLICY"]));
            ddlPolicy.DataTextField = "name";
            ddlPolicy.DataValueField = "id";
            ddlPolicy.DataBind();
        }

        protected void btnSave_OnServerClick(object sender, EventArgs e)
        {
            var row = new Dictionary<string, object>
            {
                {"group_id", ddlGroups.SelectedItem.Value},
                {"policy_id", ddlPolicy.SelectedItem.Value},
                {"was_checked", 1}
            };

            _dbManager.Insert("group_policy", row, "id");

            if (ViewState["PreviousPage"] != null)
            {
                Response.Redirect(ViewState["PreviousPage"].ToString());
            }
        }
    }
}