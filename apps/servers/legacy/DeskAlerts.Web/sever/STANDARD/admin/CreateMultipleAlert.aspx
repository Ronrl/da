﻿<%@ Page EnableEventValidation="false" Language="C#" AutoEventWireup="true" CodeBehind="CreateMultipleAlert.aspx.cs" Inherits="DeskAlertsDotNet.Pages.CreateMultipleAlert" %>
<%@ Import Namespace="System.Activities.Expressions" %>
<%@ Import NameSpace="DeskAlertsDotNet.Parameters" %>
<%@ Import Namespace="DeskAlertsDotNet.Models" %>
<%@ Import Namespace="DeskAlertsDotNet.DataBase" %>
<%@ Import Namespace="Resources" %>
<%@ Register src="ReccurencePanel.ascx" TagName="ReccurencePanel" TagPrefix="da" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<title>DeskAlerts</title>
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
<link href="css/style9.css" rel="stylesheet" type="text/css"/>
<link href="css/form_style.css" rel="stylesheet" type="text/css"/>
<style>
    .checkbox_div { padding: 1px; }

    div.overlay:before {
        content: '';
        display: block;
        position: fixed;
        top: 0;
        left: 0;
        height: 100%;
        width: 100%;
        background: rgba(0, 0, 0, .5);
        z-index: 100;
    }

    div.preloader:after {
        content: '';
        display: block;
        position: fixed;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        height: 40px;
        width: 150px;
        margin: auto;
        background: gray url(images/loader.gif) no-repeat center center;
        border-radius: 6px;
        z-index: 101;
    }
</style>
<script language="javascript" type="text/javascript" src="functions.asp"></script>

<!-- TinyMCE -->

<script language="javascript" type="text/javascript" src="jscripts/tiny_mce/tinymce.min.js"></script>

<!-- /TinyMCE -->

<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/shortcut.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/preview.js"></script>
<script language="javascript" type="text/javascript" src="functions.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/json2.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/CreateAlert.js"></script>


<script type="text/javascript" src="https://s0.assets-yammer.com/assets/platform_social_buttons.min.js"></script>

<script language="javascript" type="text/javascript">


    function submitForm(isDraft) {

        $(function() {

            if (isDraft)
                $("#IsDraft").val("1");


            //tinyMCE.get("htmlTitle").getContent()

            $("#form1").submit();

        });
    }

    function showPrompt(text) {
        $(function() {
            $("#dialog-modal > p").text(text);
            $("#dialog-modal")
                .dialog({
                    height: 180,
                    modal: true,
                    resizable: false,
                    buttons: {
                        "<%= resources.LNG_CLOSE %>": function() {
                            $(this).dialog("close");
                        }
                    }
                });
        });
    }

    function showColorCodeDialog() {
        $("#color_code_dialog")
            .dialog({
                position: { my: "left top", at: "left top", of: "#dialog_dock" },
                modal: true,
                resizable: false,
                draggable: false,
                width: 'auto',
                resize: 'auto',
                margin: 'auto'

            });
    }

    function check_size() {
        var shouldCheckSize = <% = shouldCheckSize %>;

        if (shouldCheckSize == 0)
            return;

        var elem = document.getElementById('tickerTypeTab');
        if (elem) {
            var disabled = elem.checked;
            var el = document.getElementById('size_div');
            toggleDisabled(el, disabled);
            el = document.getElementById('size_message');
            toggleDisabled(el, disabled);
        }
        check_message();
    }

    function check_message() {
        var elem = document.getElementById('size2');
        var msg = document.getElementById('size_message');
        var al_width = document.getElementById('alertWidth');
        var al_height = document.getElementById('alertHeight');
        var resizable = document.getElementById('resizable');
        var position_fieldset = document.getElementById('position_fieldset');
        var webplugin = '<%= digitalSignage %>';
        if (elem && al_width && al_height) {
            if (msg) msg.style.display = elem.checked || webplugin == "1" ? "none" : "";
            al_width.disabled = elem.checked;
            al_height.disabled = elem.checked;

            if (position_fieldset) {
                position_fieldset.disabled = elem.checked;
                $("#ticker_position_fieldset").disabled = elem.checked;
            }
            if (resizable) resizable.disabled = elem.checked;
        }
    }

    
    
</script>

<script type="text/javascript">
    var skinId;
    var confShowSkinsDialog = <%= Settings.Content["ConfShowSkinsDialog"] %>;
    var confShowTypesDialog = <%= Settings.Content["ConfShowTypesDialog"] %>;

    var isInstant = '<%= IsInstantVal %>';
    var skins = <% = skinsJson %>;
    
    var old_aknown;

    function Confirm() {
        if (!validateFileName(FileUploader.files[0].name)) {
            showErrorFileUpload('File upload error');
            return;
        }
        var confirm_value = document.createElement("INPUT");
        confirm_value.type = "hidden";
        confirm_value.name = "confirm_value";
        if (confirm("This operation may take more time to perform, are you sure?")) {
            $("#mainLoaderDiv").show();
            confirm_value.value = "Yes";
        }
        else {
            confirm_value.value = "No";
        }
        document.forms[0].appendChild(confirm_value);
    }

    function check_alert_type() {
        var webplugin = parseInt('<%= digitalSignage %>');

        $("#alertDeviceHeader").show();
        $("#alertDeviceTable").show();
        // $("#templatesList").show();

        if ($("#rsvpTypeTab").prop('checked')) {
            $("#tickerInfo").hide();
            $("#non_rsvp").hide();
            $("#position_div").parent().show();
            $("#ticker_position_div").parent().hide();
            var imgSrc = $("#skinPreviewImg").attr("src");
            if (imgSrc) {
                imgSrc = imgSrc.replace("thumbnail_tick.png", "thumbnail.png");
                $("#skinPreviewImg").attr("src", imgSrc);
            }
            $("#once").prop('checked', true);
            //   patternChange('o');
            $("#dayly,label[for='dayly'],#weekly,label[for='weekly'],#monthly,label[for='monthly'],#yearly,label[for='yearly']").fadeOut(500);
            $("#rsvp_div").fadeIn(500);
            $("#linkedin_div").hide();
            $("#elements_to_display").show();
            $("#buttons_to_display").show();
            $(".save_button").show();
            $("#main_buttons_next").show();
            $("#right_items").height("430px");
            old_aknown = $("#aknown").is(':checked');
            $("#aknown").prop('checked', false).parent().hide();
            check_aknown();
            $("#print_div").hide();
            $('.tile').show();
            $('#schedule_webplugin').hide();
            $('#schedule_webplugin_table').hide();
            $('#webalert').prop("disabled", true);
            need_second_question_click();
        } else if ($("#lnkdnTypeTab").prop('checked')) {
            $("#linkedin_div").fadeIn(500);
            $("#tickerInfo").hide();
            $("#non_rsvp").show();
            var imgSrc = $("#skinPreviewImg").attr("src");
            if (imgSrc) {
                imgSrc = imgSrc.replace("thumbnail_tick.png", "thumbnail.png");
                $("#skinPreviewImg").attr("src", imgSrc);
            }
            $("#elements_to_display").hide();
            $("#buttons_to_display").hide();
            $(".save_button").hide();
            $("#main_buttons_next").hide();
            $("#right_items").height("50px");
            $("#rsvp_div").hide();
            $("#shareAPI").button();
            old_aknown = $("#aknown").is(':checked');
            $("#aknown").prop('checked', false).parent().hide();
            check_aknown();
            $("#print_div").hide();
            $('.tile').show();
            $('#schedule_webplugin').hide();
            $('#schedule_webplugin_table').hide();
            $('#webalert').prop("disabled", true);
        } else if ($("#alert_type_webplugin").prop('checked')) {
            $("#linkedin_div").hide();
            $("#tickerInfo").hide();
            $("#non_rsvp").show();
            var imgSrc = $("#skinPreviewImg").attr("src");
            if (imgSrc) {
                imgSrc = imgSrc.replace("thumbnail_tick.png", "thumbnail.png");
                $("#skinPreviewImg").attr("src", imgSrc);
            }
            $("#elements_to_display").hide();
            $("#buttons_to_display").hide();
            $(".save_button").hide();
            $("#main_buttons_next").hide();
            $("#right_items").height("50px");
            $("#rsvp_div").hide();
            $("#shareAPI").button();
            old_aknown = $("#aknown").is(':checked');
            $("#aknown").prop('checked', false).parent().hide();
            check_aknown();
            $("#print_div").hide();
            $('.tile').hide();
            $('#schedule_webplugin').show();
            $('#schedule_webplugin_table').show();
            $('#webalert').prop("disabled", false);
        } else {


            $('#webalert').prop("disabled", true);
            $('#schedule_webplugin').hide();
            $('#schedule_webplugin_table').hide();
            $('.tile').show();
            $("#right_items").height(430);
            $("#non_rsvp").show();
            $(".save_button").show();
            $("#main_buttons_next").show();
            $("#elements_to_display").show();
            $("#buttons_to_display").show();
            $("#dayly,label[for='dayly'],#weekly,label[for='weekly'],#monthly,label[for='monthly'],#yearly,label[for='yearly']").fadeIn(500);
            if (old_aknown !== null) {
                $("#aknown").prop('checked', old_aknown);
                check_aknown();
            }
            $("#aknown").parent().show();
            $("#print_div").show();
            if ($("#tickerTypeTab").prop("checked")) {
                var imgSrc = $("#skinPreviewImg").attr("src");
                if (imgSrc) {
                    imgSrc = imgSrc.replace("thumbnail.png", "thumbnail_tick.png");
                    $("#skinPreviewImg").attr("src", imgSrc);
                }
                $("#tickerInfo").fadeIn(500);
                $("#digsign_info").hide();
                $("#rsvp_div").hide();
                $("#non_rsvp").show();
                $("#position_div").parent().hide();
                $("#linkedin_div").hide();
                $("#ticker_position_div").parent().show();
                $("#print_div").hide();
                $("#alertDeviceHeader").hide();
                $("#alertDeviceTable").hide();
                //  $("#templatesList").hide();
            } else {
                $("#tickerInfo").fadeOut(500);
                $("#digsign_info").fadeIn(500);
                var imgSrc = $("#skinPreviewImg").attr("src");
                if (imgSrc) {
                    imgSrc = imgSrc.replace("thumbnail_tick.png", "thumbnail.png");
                }
                $("#skinPreviewImg").attr("src", imgSrc);
                if (webplugin != 1)
                    $("#position_div").parent().show();
                $("#ticker_position_div").parent().hide();
                $("#non_rsvp").show();
                $("#linkedin_div").fadeOut(500);
                $("#rsvp_div").fadeOut(500);
            }
        }


    }

    function showAlertPreview(type) {
        if (!type) type = "desktop";
        //alert();
        var data = new Object();

        data.create_date = "<%= DateTime.Now.ToString(("dd/MM/yyyy HH:mm:ss")) %>";

        //var fullscreen = ($("#size2").prop("checked") == "checked") ? "1" : $("input[name='position']:checked").attr("value");
        var ticker = ($("#tickerTypeTab").prop("checked")) ? 1 : 0;
        var fullscreen = "9";
        if ($("#size2").prop("checked")) {
            fullscreen = "1"
        } else if (ticker == 0) {
            fullscreen = $("input[name='position']:checked").attr("value");
        } else {
            fullscreen = $("input[name='ticker_position']:checked").attr("value");
        }
        var acknowledgement = ($("#aknown").prop("checked")) ? 1 : 0;
        var self_deletable = ($("#selfDeletable").prop("checked")) ? 1 : 0;
        var alert_width = $("#alertWidth").val();
        var alert_height = $("#alertHeight").val();
        var print_alert = ($("#printAlertCheckBox").prop("checked")) ? 1 : 0;
        var alert_html = tinyMCE.get('alertContent').getContent();
        if (ticker == 1) {
            <% if (Config.Data.HasModule(Modules.TICKER_PRO))
               { %>
            if (alert_html.indexOf("<-- ticker_pro -->") == -1)
                alert_html += "<!-- ticker_pro -->";
            <% } %>
        }
        <% if (Settings.Content["ConfEnableHtmlAlertTitle"].Equals("1"))
           { %>
        var html_title_ed = tinyMCE.get("htmlTitle");
        var html_title = html_title_ed.getContent();
        if (html_title)
            alert_html += "<!-- html_title = '" +
                html_title.replace(/&/g, '&amp;')
                .replace(/"/g, '&quot;')
                .replace(/'/g, '&#39;')
                .replace(/</g, '&lt;')
                .replace(/>/g, '&gt;') +
                "' -->";

        //var alert_title = $('<div/>').html(html_title).text();
        var alert_title = html_title;
        <% }
           else
           { %>
        var alert_title = $("#simpleTitle").val();
        <% } %>

        var top_template;
        var bottom_template;
        var templateId = $("#template_select").val();

        if (print_alert == 1) {
            if (alert_html.indexOf("<!-- printable_alert -->") == -1)
                alert_html += "<!-- printable_alert -->";
        } else {
            if (alert_html.match(/<!-- printable_alert -->/img))
                alert_html = alert_html.replace(/<!-- printable_alert -->/img, "");
        }

        data.top_template = top_template;
        data.bottom_template = bottom_template;
        data.alert_width = alert_width;
        data.alert_height = alert_height;
        data.ticker = ticker;
        data.fullscreen = fullscreen;
        data.acknowledgement = acknowledgement;
        data.self_deletable = self_deletable;
        data.alert_title = alert_title;
        data.alert_html = alert_html;
        data.caption_href = "";

        var skinId = $("#skin_id").val();

        if (skinId != "default" && skinId != "")
            skinId = "{" + skinId + "}";

        // alert(skinId);
        data.skin_id = skinId;
        data.type = type;
        data.need_question = type != 'sms' && $("#rsvpTypeTab").prop('checked');
        data.question = $("#question").val();
        data.question_option1 = $("#question_option1").val();
        data.question_option2 = $("#question_option2").val();
        data.need_second_question = $("#need_second_question").prop('checked');
        data.second_question = $("#second_question").val();

        initAlertPreview(data);

        return false;
    }


    function check_value() {
        if ($("#alertWidth").length > 0) {
            var al_width = document.getElementById('alertWidth').value;
            var al_height = document.getElementById('alertHeight').value;
            var size_message = document.getElementById('size_message');

            if (al_width > 1024 && al_height > 600) {
                size_message
                    .innerHTML = '<font color="red"><%= resources.LNG_RESOLUTION_DESC1 %></font>';
                return;
            }

            if (al_width < 340 || al_height < 290) {
                size_message
                    .innerHTML =
                    "<font color='red'><%= resources.LNG_RESOLUTION_DESC2 %>. <A href='#' onclick='set_default_size();'><%= resources.LNG_CLICK_HERE %></a> <%= resources.LNG_RESOLUTION_DESC3 %>.</font>";
                return;
            }
            size_message.innerHTML = "";
        }
    }

    function check_tab(type) {

        $(function() {
            document.getElementById('alertTypeTab').checked = false;


            if (document.getElementById('tickerTypeTab'))
                document.getElementById('tickerTypeTab').checked = false;

            if (document.getElementById('rsvpTypeTab'))
                document.getElementById('rsvpTypeTab').checked = false;

            if (document.getElementById('lnkdnTypeTab'))
                document.getElementById('lnkdnTypeTab').checked = false;
            if (type == 'alert') {
                // $("#webplugin").val(0);
                document.getElementById('alertTypeTab').checked = true;
                $("#ticker").val("0");
                $("#linkedin").val("0");
                $("#rsvp").val("0");

            } else if (type == 'ticker') {
                //	 $("#webplugin").val(0);
                //document.getElementById('tickerTypeTab').checked = true;
                $("#blogPost").parent().hide();
                $("#ticker").val("1");
                $("#linkedin").val("0");
                $("#rsvp").val("0");

            } else if (type == 'rsvp') {
                // $("#webplugin").val(0);
                document.getElementById('rsvpTypeTab').checked = true;
                $("#rsvp").val("1");
                $("#ticker").val("0");
                $("#linkedin").val("0");

            } else if (type == 'linkedin') {
                //	 $("#webplugin").val(0);
                document.getElementById('lnkdnTypeTab').checked = true;
                $("#linkedin").val("1");
                $("#ticker").val("0");
                $("#rsvp").val("0");
            } else if (type == 'webplugin') {
                //	 $("#webplugin").val(1);
                document.getElementById('alert_type_webplugin').checked = true;
            }
            check_alert_type();
            if ('<%= IsInstantVal %>' == '1') {
                im_interface();
            }

            check_size();
        });

    }


    function showPromptRedir(text) {
        $("#dialog-modal > p").text(text);
        $("#dialog-modal")
            .dialog({
                height: 180,
                modal: true,
                resizable: false,
                buttons: {
                    "<%= resources.LNG_CLOSE %>": function() {
                        $(this).dialog("close");
                        location.href = "index.asp";
                    }
                }
            });
    }

    $(document)
        .ready(function() {

            if (confShowTypesDialog === 1 && isInstant !== '1' && window.location.href.indexOf("id") < 0) {
                $("#alert_type_dialog")
                    .dialog({
                        position: { my: "left top", at: "left top", of: "#dialog_dock" },
                        modal: true,
                        resizable: false,
                        draggable: false,
                        width: 'auto',
                        resize: 'auto',
                        margin: 'auto',
                        autoOpen: true,
                        close: function() { closeTypesDialog() }

                    });
            }

            //content editor initialization
            tinyMCE.baseURL = window.location.href.substring(0, window.location.href.lastIndexOf("/")) +
                "/jscripts/tiny_mce/";
            tinymce.init({
                selector: 'textarea#alertContent',
                plugins:
                    "moxiemanager textcolor link code emoticons insertdatetime fullscreen imagetools directionality table media image",
                toolbar1:
                    "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | fontselect fontsizeselect | forecolor backcolor | fullscreen",
                toolbar2:
                    "cut copy paste | bullist numlist | outdent indent | undo redo | link unlink | image insertfile code | rotateleft rotateright flipv fliph",
                moxiemanager_image_settings: {
                    view: 'thumbs',
                    extensions: 'jpg,png,gif'
                },
                moxiemanager_video_settings: {
                    view: 'thumbs',
                    extensions: 'mp4,webm,ogg'
                },
                relative_urls: false,
                remove_script_host: false,
                height: 100,
                document_base_url: "<%= Config.Data.GetString("alertsDir") %>/admin/images/upload",
                content_style:
                    "body{color:#000 !important; font-family:Arial, Times New Roman, Helvetica, sans-serif !important; font-size:14px !important}"
            });

            //html title editor initialization
            tinymce.init({
                entity_encoding: 'raw',
                selector: '.htmlTitle',
                menubar: false,
                statusbar: false,
                resize: false,
                setup: function (ed) {
                    ed.on('change', function (e) {
                        $("#titleHidden").val(ed.getContent());
                    });
                },
                plugins: "autoresize textcolor autolink",
                toolbar: "italic underline strikethrough | fontselect fontsizeselect | forecolor",
                autoresize_bottom_margin: 0,
                autoresize_min_height: 20,
                autoresize_max_height: 80,
                height: 300,
                fontsize_formats: '8pt 10pt 11pt 12pt 14pt 16pt 18pt',
                forced_root_block: false,
                valid_elements: "span[*],font[*],i[*],b[*],u[*],strong[*],s[*],em[*]",
                content_style: "body{font:bold 21px Arial !important;color:#000000 !important; font-weight:bold !important;}",
                branding: false
            });

            $(document)
                .tooltip({
                    items: "img[title],a[title]",
                    position: {
                        my: "center bottom-20",
                        at: "center top",
                        using: function(position, feedback) {
                            $(this).css(position);
                            $("<div>")
                                .addClass("arrow")
                                .addClass(feedback.vertical)
                                .addClass(feedback.horizontal)
                                .appendTo(this);
                        }
                    }
                });

            shortcut.add("Alt+s",
                function(e) {
                    e.preventDefault();
                    __doPostBack('saveButton', '');
                });

            shortcut.add("right",
                function(e) {
                    e.preventDefault();
                    __doPostBack('saveNextButton', '');
                });

            shortcut.add("Alt+P",
                function(e) {
                    e.preventDefault();
                    showAlertPreview();
                });

            shortcut.add("Alt+1",
                function(e) {
                    e.preventDefault();
                    $('#alert_type_tabs').tabs("option", "active", 0);
                    check_tab('alert');
                    check_alert_type();
                });

            shortcut.add("Alt+2",
                function(e) {
                    e.preventDefault();
                    $('#alert_type_tabs').tabs("option", "active", 1);
                    check_tab('ticker');
                    check_alert_type();
                });

            shortcut.add("Alt+3",
                function(e) {
                    e.preventDefault();
                    $('#alert_type_tabs').tabs("option", "active", 2);
                    check_tab('rsvp');
                    check_alert_type();
                });

            shortcut.add("Alt+4",
                function(e) {
                    e.preventDefault();
                    $('#alert_type_tabs').tabs("option", "active", 3);
                    check_tab('linkedin');
                    check_alert_type();
                });

            if (skins.length > 0) {
                $(".tile")
                    .hover(function() {
                            $(this).switchClass("", "tile_hovered", 200);
                        },
                        function() {
                            $(this).switchClass("tile_hovered", "", 200);
                        })
                    .click(function() {

                        showSkinsTilesDialog();
                    })
            } else {
                $(".tile").hide();
            }


        });


    $(document)
        .ready(function() {

            $(function() {
                //  $(".translate_btn").button();
                $(".cancel_button").button();
                $(".save_button").button();
                $(".preview_button").button();
                $("#yammerShare").button();
                $(".save_and_next_button")
                    .button({
                        icons: {
                            secondary: "ui-icon-carat-1-e"
                        }
                    });

                $(".preview_sms_button")
                    .button()
                    .click(function() {
                        showAlertPreview("sms");
                    });

                isDigSign = parseInt('<% = Request["webplugin"] %>');

                if (isDigSign != 1) {
                    $("#accordion")
                        .accordion({
                            active: 0,
                            heightStyle: "content",
                            collapsible: true
                        });
                }

                var ticker = <%= this.tickerValue %>;
                var rsvp = '<%= Request["rsvp"] %>';
                $('#alert_type_tabs')
                    .tabs({
                        active: $("#alert_type_webplugin").is(':checked')
                            ? 4
                            : $("#lnkdnTypeTab").is(':checked')
                            ? 3
                            : $("#rsvpTypeTab").is(':checked')
                            ? 2
                            : $("#alertTypeTab").is(':checked') && ticker != 1 ? 1 : 0
                    })
                    .css("border", "none");

                //if (ticker == 1) {
                //    $('#alert_type_tabs').tabs({ active: 1 });

                //    check_tab('ticker');
                //}

                //if (rsvp == '1') {
                //    $('#alert_type_tabs').tabs({ active: 2 });

                //    check_tab('rsvp');
                //}

            });

            $("#tickerTypeTab").click(check_alert_type);
            $("#alertTypeTab").click(check_alert_type);
            $("#rsvpTypeTab").click(check_alert_type);
            $("#lnkdnTypeTab").click(check_alert_type);
            $("#alert_type_webplugin").click(check_alert_type);
            
            check_lifetime();
            //   check_settings();
            check_alert_type();

            var isChecked = $("#recurrenceCheckbox").prop('checked');

            $(".reccurenceControl").prop('disabled', !isChecked);
            $(".reccurenceControl input").prop('disabled', !isChecked); //function(i, v) { return !v; }


        });


    function check_recurrence() {

        var isChecked = $("#recurrenceCheckbox").prop('checked');

        $(".reccurenceControl input").prop('disabled', !isChecked); //function(i, v) { return !v; }
        $(".reccurenceControl").prop('disabled', !isChecked);
        var displayValue = $(".count_down_delete_img").css('display') == "none" ? "block" : "none";


        $(".count_down_delete_img").prop('display', displayValue);
    }

    function createSkinsTilesHtml() {
        var thumbnail;
        if ($("#tickerTypeTab").prop("checked")) {
            thumbnail = "thumbnail_tick.png";
        } else {
            thumbnail = "thumbnail.png";
        }
        var list = $('<ul style="list-style:none; margin:0px; padding:0px">' +
            '<li class="tile" style="float:left;  margin:10px; padding:10px">' +
            '<div style="padding-bottom:5px"><span autofocus style="margin:0px" id="header_title" class="header_title"><%= resources.LNG_DEFAULT %></span></div>' +
            '<div>' +
            '<img src="skins/default/' +
            thumbnail +
            '"/>' +
            '</div>' +
            '</li>' +
            '</ul>');

        for (var i = 0; i < skins.length; i++) {
            var li = '<li id="' +
                skins[i].id +
                '" class="tile" style="float:left; margin:10px; padding:10px">' +
                '<div style="padding-bottom:5px"><span style="margin:0px" class="header_title">' +
                skins[i].name +
                '</span></div>' +
                '<div>' +
                '<img src="skins/{' +
                skins[i].id +
                '}/' +
                thumbnail +
                '"/>' +
                '</div>' +
                '</li>';
            $(list).append(li);
        }

        $(list)
            .find(".tile")
            .hover(function() {
                    $(this).switchClass("", "tile_hovered", 200);
                },
                function() {
                    $(this).switchClass("tile_hovered", "", 200);
                })
            .click(function() {
                $("#skins_tiles_dialog").dialog("close");
                skinId = $(this).attr("id");
                $("#skin_id").val(skinId);
                if (!skinId) {
                    skinId = "default";
                } else {
                    skinId = "{" + skinId + "}";
                }

                var imgSrc = "skins/" + skinId + "/" + thumbnail;
                $("#skinPreviewImg").attr("src", imgSrc);
                if (skinId == '{C28F6977-3254-418C-B96E-BDA43CE94FF4}' ||
                    skinId == '{138F9281-0C11-4C48-983E-D4CA8076748B}' ||
                    skinId == '{04F84380-B31D-4167-8B83-C6642E3D25A8}') {
                    document.getElementById('resizable').disabled = true;
                    document.getElementById('resizable').checked = false;
                    document.getElementById('size2').checked = false;
                    document.getElementById('size2').disabled = true;
                    document.getElementById('size1').checked = false;
                    document.getElementById('size1').disabled = true;
                    document.getElementById('alertWidth').disabled = true;
                    document.getElementById('alertHeight').disabled = true;
                    document.getElementById('alertWidth').value = "500";
                    document.getElementById('alertHeight').value = "400";

                    //document.getElementById('caption_frame').style.backgroundColor=transparent;
                } else {
                    document.getElementById('alertWidth').disabled = false;
                    document.getElementById('alertHeight').disabled = false;
                    document.getElementById('resizable').disabled = false;
                    document.getElementById('size2').disabled = false;
                    document.getElementById('size1').disabled = false;
                    //document.getElementById('caption_frame').style.backgroundColor="white";
                }
            })

        return list;
    }
</script>

</head>
<!-- action="AddAlert.aspx" method="POST" -->
<body>
<div id="mainLoaderDiv" class="overlay preloader" hidden>
       
</div> 
<form id="form1" runat="server">

<asp:ScriptManager ID="ScriptManager1" runat="server">
</asp:ScriptManager>


<div runat="server" id="mainView">
<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
<tr>
<td>
<table id="dialog_dock" width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
<tr>
    <td width="100%" height="31" class="main_table_title">

        <img id="contentImage" runat="server" alt="" border="0" height="20" src="images/menu_icons/alert_20.png" style="padding-left: 7px" width="20"/>

        <a runat="server" id="headerTitle" href="#" class="header_title" style="position: absolute; top: 22px"> </a>
    </td>
</tr>
<tr>
<td height="100%" vclass="main_table_body">

<div id="alertTypeTab" style="padding: 0.5em 0.5em;">
<span class="work_header"></span>
<input type="hidden" runat="server" id="IsInstant" name="instant" value=""/>
<input type="hidden" runat="server" id="webAlert" name="webalert" value=""/>
<input type="hidden" runat="server" name="alert_campaign" value="" id="alertСampaign"/>
<input type="hidden" runat="server" id="colorСode" name="color_code" value=""/>
<input type='hidden' runat="server" id="edit" name='edit' value=''/>
<input type='hidden' runat="server" id="alertId" name='id' value=''/>
<input type='hidden' runat="server" name='rejectedEdit' id="rejectedEdit" value=''/>
<input type='hidden' runat="server" name='shouldApprove' id="shouldApprove" value=''/>

<div style="width: 955px; margin: auto;">
<div runat="server" id="campaignLabel">
    <b><%= resources.LNG_CAMPAIGN %>: </b>
    <label>
        <b runat="server" id="campaignName"></b>
    </label><br>
    <br/>
</div>
<input runat="server" id='campaignSelect' type='hidden' name='campaign_select' value=''/>
<table width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td>
        </td>
    </tr>
    <tr>
        <td style="vertical-align: bottom; width: 1px; overflow: visible" rowspan="2">
            <%= resources.LNG_TITLE %>:&nbsp;&nbsp;
        </td>
        <td rowspan="2" style="vertical-align: bottom">
            <div>

                <div runat="server" id="htmlTitleDiv">
                    <div class="html_title_div">
                        <textarea runat="server" id="htmlTitle" name="htmlTitle" rows="1" class="htmlTitle"></textarea>
                        <input type="hidden" runat="server" id="titleHidden" name="alert_title" value=""
                               maxlength="255" style="width: 304px" />
                    </div>
                </div>

                <div runat="server" id="simpleTitleDiv">
                    <input type="text" runat="server" id="simpleTitle" name="alert_title" value=""
                           maxlength="255" style="width: 300px" />
                    <input type="hidden" runat="server" id="htmlTitleHidden" name="html_title" value="" />
                </div>

            </div>
        </td>
        <td>
            <div align="right" style="margin-bottom: 5px;">
                <script language="javascript">
                    check_aknown();
                    check_email();
                    check_desktop();
                    check_size();
                </script>
                <div>
                    <asp:LinkButton runat="server" CssClass="save_and_next_button" id="saveNextButton" title="Hotkey: →" OnClientClick="Confirm()"/>
                    <br/>
     
                </div>
            </div>
        </td>
    </tr>
</table>
<br/>

<table style="width: 100%">
    <tr>

    </tr>
</table>

<table style="width: 100%">
    <tr>
        <td style="width: 100%">            
            <span style="font-weight: bold"><%= resources.LNG_MULTIPLE_UPLOAD_CSV %></span>
            <br />
            <span style="color: red;"><%=resources.PLEASE_DO_NOT_USE_FILES_WITH_NAMES_MORE_THAN_50_CHARACTERS %></span>
            <br/>
            <span style="color: red;"><%=resources.PLEASE_USER_ONLY_FILE_NAME_ALLOWED_CHARACTERS %></span>
            <br/>
            <asp:UpdatePanel runat="server" ID="updatepanel1">
                <Triggers><asp:PostBackTrigger ControlID="saveNextButton" /></Triggers>
                     <ContentTemplate>
                          <asp:FileUpload ID="FileUploader" runat="server" accept=".csv"/><br/>      
                     </ContentTemplate>     
            </asp:UpdatePanel>
                
            <%--<input id="filMyFile" type="file" runat="server"></input>--%>
            <asp:Label ID="Label1" runat="server"></asp:Label><br />
               <asp:Label ID="Label2" runat="server"></asp:Label>
        </td>
        <td rowspan="3">
            <div runat="server" id="changeColorCode" onclick="showColorCodeDialog()" style="background: white; color: black; border: 1px solid #999; border-radius: 4px; cursor: pointer; cursor: hand; height: 14px; width: 200px; margin: 0px 10px 10px 10px; padding: 8px 10px; text-align: center; vertical-align: middle">
                <%= resources.LNG_CLICK_HERE_TO_CHANGE_COLOR_CODE %>
            </div>
            <div runat="server" class="tile" id="default" style="margin: 10px; padding: 10px">
                <div class="hints" style="font-size: 12px; text-align: center; padding-bottom: 2px">
                    <%= resources.LNG_CLICK_TO_CHANGE_SKIN %>
                </div>
                <div style="font-size: 1px">
                    <img runat="server" id="skinPreviewImg" src="skins/default/thumbnail.png"/>
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td style="width: 100%">            
            <span style="font-weight: bold"><%= resources.LNG_MULTIPLE_SELECT_TYPE %></span>
            <br/>      
            <input type="radio" runat="server" id="simple" name="multiple" value="0" onclick="check_message()" checked/> <%= resources.LNG_MULTIPLE_SIMPLE %>
            <img src="images/help.png" title="<%= resources.LNG_MULTIPLE_SEND_SIMPLE_DESC %>"/> <br/>  
            <input type="radio" runat="server" id="group" name="multiple" value="1" onclick="check_message()" checked/> <%= resources.LNG_MULTIPLE_GROUP %> 
            <img src="images/help.png" title="<%= resources.LNG_MULTIPLE_SEND_GROUP_DESC %>"/>
            <br/>    
        </td>
    </tr>
    <tr style="height:40px">      
    </tr>
</table>
<script type="text/javascript">
   
    var shouldBeReadOnly = parseInt("<% = shouldBeReadOnly %>", 10) == 1;
    var readOnly = { readonly: shouldBeReadOnly };

    /*initTinyMCE("EN", "exact", "alertContent", function(ed){
        

    }, readOnly, newTinySettings);   */    

    <% if (Settings.Content["ConfEnableHtmlAlertTitle"].Equals("1"))
       { %>
                                            
    //var hiddenTitle = tinyMCE.get("htmlTitle").getContent();
    //$("#titleHidden").val(hiddenTitle);
    <% } %>

                                             
</script>
<div id="bottom_template_div">
</div>
<div runat="server" id="digsignInfo" class="ticker_info" style="">
    <br/>
    <%= resources.LNG_DIGSIGN_WARNING %>

</div>
<div id="tickerInfo" class="ticker_info" style="display: none">
    <br/>
    <%= resources.LNG_TICKER_INFO %>

</div>
<div runat="server" id="digSignNoLinksWarning" class="ticker_info" style="display: none">
    <br/>
    <%= resources.LNG_NO_LINKS_ADM %>
</div>

<div id="rsvp_div" style="display: none">
    <div style="padding: 10px"><%= resources.LNG_INVITATION_INSTRUCTIONS %></div>
    <table border="0">
        <tr>
            <td colspan="2" style="padding-left: 24px">
                <b>
                    <%= resources.LNG_QUESTION_WORD %>
                    #1
                </b>
            </td>
        </tr>
        <tr>
            <td>
                <%= resources.LNG_QUESTION %>:
            </td>
            <td>
                <input type="text" runat="server" name="question" id="question" size="35" value=""/>
            </td>
        </tr>
        <tr>
            <td>
                <%= resources.LNG_OPTION %>:
            </td>
            <td>
                <input type="text" runat="server" id="question_option1" name="question_option1" size="35" value=""/>
            </td>
        </tr>
        <tr>
            <td>
                <%= resources.LNG_OPTION %>:
            </td>
            <td>
                <input type="text" runat="server" id="question_option2" name="question_option2" size="35" value=""/>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <input runat="server" runat="server" id="need_second_question" name="need_second_question" type="checkbox" value="1"
                       onclick="need_second_question_click()"/>
                <label for="need_second_question">
                    <b>
                        <%= resources.LNG_QUESTION_WORD %>
                        #2
                    </b>
                </label>
            </td>
        </tr>
        <tr class="second_question">
            <td>
                <%= resources.LNG_QUESTION %>:
            </td>
            <td>
                <input runat="server" type="text" name="second_question" id="second_question" size="35" value=""/>
            </td>
        </tr>
        <tr class="second_question">
            <td>
                <%= resources.LNG_QUESTION_TYPE %>:
            </td>
            <td>
                <%= resources.LNG_INPUT_ANSWER %>
                <i>(<%= resources.LNG_INPUT_ANSWER_DESC %>)</i>
            </td>
        </tr>
    </table>
    <br/>
</div>

<div id="linkedin_div" style="display: none">
    <div id="linkedin_settings_hint" style="padding-bottom: 5px; color: #ff0000;">
        Manage LinkedIn settings to enable the feature
    </div>
    <div id="linkedInDiv" style="padding-bottom: 5px">

        <script type="IN/Login">Currently logged as <strong><?js= firstName ?> <?js= lastName ?></strong></script>

        <br/>
        <br/>
        <a id="shareAPI" style="display: none" onclick="my_sub(4);" href="#">
            <%= resources.LNG_SHARE_ON_LINKEDIN %>
        </a>
    </div>
    <br/>
</div>
<div id="elements_to_display" style="display: none">
<table border="0" style="width: 100%">
<tr>
<td>
<div id="accordion" style="width: 100%">
<h3 runat="server" id="alertSettingsHeader"><%= resources.LNG_ALERT_SETTING_ACC %></h3>
<table runat="server" id="alertSettingsTable" border="0" style="width: 100%">
    <tr>
        <td>
            <div class="checkbox_div" id="urgent_div">
                <input type="checkbox" runat="server" id="urgentCheckBox" name="urgent" value="1" onclick="check_aknown();"/>
                <label for="urgentCheckBox">
                    <%= resources.LNG_URGENT_ALERT %>
                </label>
                <img src="images/help.png" title="<%= resources.LNG_URGENT_ALERT_DESC %>"/>
                <br/>
            </div>
            <div class="checkbox_div" id="lifeTimeAlert">
                <input type="checkbox" id="lifeTimeCheckBox" runat="server" name="lifetimemode" value="1" onclick="check_lifetime();" style="display: none" checked="True"/>
                <label for="lifeTimeCheckBox">
                    <%= resources.LNG_LIFETIME_IN %>
                </label>
                <input type="text" id="lifetime" runat="server" name="lifetimeval" style="width: 50px"  onkeypress="return check_size_input(this, event);"/>
                <select runat="server" id="lifetimeFactor" name="lifetime_factor">
                </select>
                &nbsp;
                <label id ="LifetimeAdditionalDescription" runat="server"></label>
                <img src="images/help.png" id="LifeTimeHelpImage" runat="server"/>
                <br/>
            </div>
        </td>
    </tr>
</table>
<h3><%= resources.LNG_ALERT_APPER_ACC %></h3>
<table border="0" style="width: 100%" runat="server" id="appearanceMenu">

    <tr>
        <td style="width: 200px;">
            <div id="buttons_to_display" style="display: none">
                <div id="desktopCheckLabel">
                    <input type="checkbox" id="desktopCheck" runat="server" name="desktop" value="1" onclick="check_desktop();" checked="True"/>
                    <label for="desktopCheck"><%= resources.LNG_DESKTOP_ALERT %></label>
                </div>
                <div id="ticker_div">
                    <div id="size_message">
                    </div>
                    <table border="0" cellspacing="0" cellpadding="0" style="width: auto;" id="tableAlertAppear">
                        <tr>
                            <td valign="top" runat="server" id="sizeDiv">
                                <fieldset class="reccurence_pattern" style="height: 110px">
                                    <legend><%= resources.LNG_SIZE %></legend>
                                    <div id="size_div" style="padding-left: 10px;">
                                        <table border="0">
                                            <tr>
                                                <td valign="middle">
                                                    <input type="radio" runat="server" id="size1" name="fullscreen" value="0" onclick="check_message()" checked/>
                                                </td>

                                                <td valign="middle">
                                                    <%= resources.LNG_WIDTH %>:
                                                </td>
                                                <td valign="middle">
                                                    <input type="text" runat="server" name="alert_width" id="alertWidth" size="4"
                                                           value="500" onkeypress="return check_size_input(this, event);"
                                                           onkeyup="check_value(this)"/>
                                                </td>
                                                <td valign="middle">
                                                    <%= resources.LNG_PX %>
                                                </td>

                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;
                                                </td>
                                                <td valign="middle">
                                                    <%= resources.LNG_HEIGHT %>:
                                                </td>
                                                <td valign="middle">
                                                    <input type="text" runat="server" name="alert_height" id="alertHeight" size="4"
                                                           value="400" onkeypress="return check_size_input(this, event);"
                                                           onkeyup="check_value(this)"/>
                                                </td>
                                                <td valign="middle">
                                                    <%= resources.LNG_PX %>
                                                </td>

                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;
                                                </td>
                                                <td valign="middle" colspan="3">
                                                    <input type="checkbox" runat="server" id="resizable" name="resizable" value="1"/>
                                                    <label runat="server" id="resizableLabel" for="resizable">
                                                        <%= resources.LNG_RESIZABLE %>
                                                    </label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="middle">
                                                    <input type="radio" runat="server" id="size2" name="fullscreen" value="1" onclick="check_message()"/>
                                                </td>
                                                <td valign="middle" colspan="3">
                                                    <label for="size2">
                                                        <%= resources.LNG_FULLSCREEN %>
                                                    </label><br/>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>

                                </fieldset>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </td>
        <td runat="server" id="positionCell" valign="middle" style="height: 130px; width: 200px;">
            <br/>
            <fieldset id="position_fieldset" class="reccurence_pattern" style="height: 105px;">
                <legend>
                    <%= resources.LNG_POSITION %>
                </legend>
                <div id="position_div">
                    <table border="0" cellspacing="0" cellpadding="5" width="100%">
                        <tr>
                            <td valign="middle" style="text-align: center">
                                <input type="radio" runat="server" id="positionTopLeft" name="position" value="2"/>
                            </td>
                            <td>
                            </td>
                            <td valign="middle" style="text-align: center">
                                <input type="radio" runat="server" id="positionTopRight" name="position" value="3"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td valign="middle" style="text-align: center">
                                <input runat="server" id="positionCenter" type="radio" name="position" value="4"/>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td valign="middle" style="text-align: center">
                                <input type="radio" runat="server" id="positionBottomLeft" name="position" value="5"/>
                            </td>
                            <td>
                            </td>
                            <td valign="middle" style="text-align: center">
                                <input runat="server" id="positionBottomRight"
                                       type="radio" name="position" value="6" checked/>
                            </td>
                        </tr>
                    </table>

                </div>

            </fieldset>
            <fieldset id="ticker_position_fieldset" class="reccurence_pattern" style="height: 135px; display: none">
                <legend>
                    <%= resources.LNG_POSITION %>
                </legend>
                <div id="ticker_position_div">
                    <table border="0" cellspacing="0" cellpadding="5" width="100%">
                        <tr>
                            <td valign="middle" style="text-align: center">
                                <input type="radio" runat="server" id="positionTop" name="ticker_position" value="7"/>
                            </td>
                            <td>
                                <span><%= resources.LNG_TOP %></span>
                            </td>
                        </tr>
                        <tr>
                            <td valign="middle" style="text-align: center">
                                <input type="radio" runat="server" id="positionMiddle" name="ticker_position" value="8"/>
                            </td>
                            <td>
                                <span><%= resources.LNG_MIDDLE %></span>
                            </td>
                        </tr>
                        <tr>
                            <td valign="middle" style="text-align: center">
                                <input type="radio" runat="server" id="positionBottom" name="ticker_position" value="9"/>
                            </td>
                            <td>
                                <span><%= resources.LNG_BOTTOM %></span>
                            </td>
                        </tr>
                        <tr>
                            <td valign="middle" style="text-align: center">
                                <input type="radio" runat="server" id="positionDocked" name="ticker_position" value="D"/>
                            </td>
                            <td>
                                <span><%= resources.LNG_DOCKED_AT_BOTTOM %></span><img style="margin: 0px 5px" src="images/help.png" title="<%= resources.LNG_DOCKED_TICKER_HINT %>."/>
                            </td>
                        </tr>
                    </table>
                </div>
            </fieldset>
        </td>
        <td>

        </td>
    </tr>

</table>
<h3 runat="server" id="alertDeviceHeader">Device type</h3>
<div runat="server" id="alertDeviceTable">
    <input type="radio" runat="server" name="dev" value="3" id="allDevices" onclick="showAppearance()" checked/> All devices
    <input type="radio" runat="server" name="dev" value="1" id="desktopDevice" onclick="showAppearance()"/> Desktop
    <input type="radio" runat="server" name="dev" value="2" id="mobileDevice" onclick="hideAppearance()"/> Mobile
</div>

</div>
</table>
<div align="right" style="margin-top: 10px;">

    <script language="javascript">
        check_aknown();
        check_email();
        check_email();
        check_desktop();
        check_size();
    </script>

    <div>
        <asp:LinkButton runat="server" CssClass="save_and_next_button" id="bottomSaveNextButton" title="Hotkey: →" OnClientClick="Confirm()"/>
        <asp:UpdatePanel runat="server" id="updatePanel">
            <ContentTemplate>

            </ContentTemplate>

        </asp:UpdatePanel>

    </div>
</div>

</div>
</div>
</div>

</td>
</tr>


</table>
</td>
</tr>
</table>
</div>

<input type="hidden" id="IsDraft" name="IsDraft" value="0"/>
<input type="hidden" runat="server" id="skin_id" name="skin_id" value=""/>
<input type="hidden" id="linkedIn" name="linkedIn" value="0"/>
<input type="hidden" id="rsvp" name="rsvp" value="0"/>
<input type="hidden" runat="server" id="ticker" name="ticker" value="0"/>
<input type="hidden" id="color_code" name="color_code" value=""/>

</form>
<div id="skins_tiles_dialog" title="<%= resources.LNG_SELECT_SKIN %>" style='display: none'>
    <br/>
    <div style="display: block; text-align: left; color: #696969">
        <input type="checkbox" id="skins_checkbox"/><label for="skins_checkbox"><%= resources.LNG_DONT_SHOW_AGAIN %></label>
    </div>
    <p>
    </p>


    <div id="dialog-modal" style="display: none">
        <p style="vertical-align: middle">
        </p>
    </div>

    <div id="color_code_dialog" title="<%= resources.LNG_PICK_COLOR_CODE %>" style='display: none'>
        <p>
            <%= resources.LNG_PICK_COLOR_CODE_HINT %>
        </p>
        <%
            DataSet colorCodeSet = dbMgr.GetDataByQuery("SELECT * FROM color_codes");

            //  if (colorCodeSet.IsEmpty)
            //    changeColorCode.Visible = false;

            foreach (var colorCodeRow in colorCodeSet)
            {
                string ccId = colorCodeRow.GetString("id");
                ccId = ccId.Replace("{", "");
                ccId = ccId.Replace("}", "");
                string ccColor = colorCodeRow.GetString("color");
                string ccTitle = colorCodeRow.GetString("name");
        %>
            <div id="<%= ccId %>" onclick="pickColorCode('<%= ccColor %>', '<% = ccId %>')" style="width: 180px; float: left; height: 180px; margin: 10px; padding: 10px; display: table; background-color: #<%= ccColor %>">
                <div style="cursor: pointer; display: table-cell; vertical-align: middle">
                    <div style="font-family: Tahoma; font-size: 24px; text-align: center;">
                        <%= ccTitle %>
                    </div>
                </div>
            </div>

            <script>
                $("#<%= ccId %> > div > div").css("color", darkOrLight("<%= ccColor %>"));
            </script>

        <%
            }
        %>
    </div>

</div>
</body>
</html>
