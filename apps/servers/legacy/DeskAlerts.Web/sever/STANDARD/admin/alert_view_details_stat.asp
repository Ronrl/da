<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="ad.inc" -->
<%

check_session()

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>View alert details</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
<link href="style.css" rel="stylesheet" type="text/css">
</head>
<script language="javascript">
function LINKCLICK() {
  var yesno = confirm ("Are you sure you want to delete this user?","");
  if (yesno == false) return false;
  return true;
}
</script>


<body style="margin:0" class="body">
<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	<tr>
	<td>
	<table width="100%" height="100%" border="1" bordercolor="#000000" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
		<tr>
		<td width=100% height=31 class="theme-colored-back"><a href="" class="header_title">Alert details</a></td>
		</tr>
		<tr>
		<td bgcolor="#ffffff" height="100%">
		<br>
		<div style="margin:10px;">

		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr valign="top"><td>&nbsp;</td><td><b>Preview:</td><td><iframe  STYLE="border: 1px solid black;" src="<% Response.Write alerts_folder & "preview.asp?id=" & Request("id") %>" width="200" height="200" frameborder="0"></iframe><br><br></td></tr>

<%
	Set RS = Conn.Execute("SELECT id, alert_text, type, convert(varchar,sent_date) as sent_date, convert(varchar,create_date) as sent_date, type2, sender_id  FROM alerts WHERE type='S' AND id="&Request("id")&" ORDER BY id DESC")
	num=0
	if (Not RS.EOF) then
	
		if(RS("type2")="P") then 
			mytype="Personal" 

	Set RS6 = Conn.Execute("SELECT name FROM users INNER JOIN alerts_sent_stat ON users.id=alerts_sent_stat.user_id WHERE alerts_sent_stat.alert_id="&RS("id"))	
	do while not RS6.EOF
		myrecipients=myrecipients & RS6("name")
		RS6.MoveNext
		if (Not RS6.EOF) then
			myrecipients=myrecipients & ", "
		end if

	loop

		end if
		if(RS("type2")="G") then 
			mytype="Group" 
	Set RS6 = Conn.Execute("SELECT name FROM groups INNER JOIN alerts_group_stat ON groups.id=alerts_group_stat.group_id WHERE alerts_group_stat.alert_id="&RS("id"))	
	do while not RS6.EOF
		myrecipients=myrecipients & RS6("name")
		RS6.MoveNext
		if (Not RS6.EOF) then
			myrecipients=myrecipients & ", "
		end if

	loop

		end if
		if(RS("type2")="B") then 
			mytype="Broadcast" 
			myrecipients="All"
		end if

	mysentby=""
	if(Not IsNull(RS("sender_id"))) then
		Set RS7 = Conn.Execute("SELECT name FROM users WHERE id="&RS("sender_id"))	
		if(Not RS7.EOF) then
			mysentby=RS7("name")
		end if
	end if


  mysent=0
  Set RS7 = Conn.Execute("SELECT COUNT(id) as mycnt FROM [alerts_sent_stat] WHERE alert_id="&RS("id"))
  if(not RS7.EOF) then
	mysent=RS7("mycnt")
  end if

  myreceived=0
  myclicks=0
  Set RS7 = Conn.Execute("SELECT COUNT(id) as mycnt FROM [alerts_received] WHERE alert_id="&RS("id"))
  if(not RS7.EOF) then
	myreceived=RS7("mycnt")
  end if
  if(IsNull(myclicks)) then myclicks=0 end if
  myread=0
  Set RS7 = Conn.Execute("SELECT COUNT(id) as mycnt FROM [alerts_read] WHERE [read]='1' AND alert_id="&RS("id"))
  if(not RS7.EOF) then
	myread=RS7("mycnt")
  end if

  if(mysent<>0) then
    myopen=Round(clng(myreceived)/clng(mysent)*100,2)
  else
    myopen="0"
  end if
  if(myreceived<>0) then
    myctr=Round(clng(myclicks)/clng(myreceived)*100,2)
  else
    myctr="0"
  end if

		 Response.Write "<tr><td>&nbsp;</td><td> <b>Date: </b></td><td>"& RS("sent_date") &"</td></tr>"
		 Response.Write "<tr><td>&nbsp;</td><td> <b>Type: </b></td><td>"& mytype &"</td></tr>"
		 Response.Write "<tr><td>&nbsp;</td><td> <b>Recipients: </b></td><td>"& myrecipients &"</td></tr>"
		 Response.Write "<tr><td>&nbsp;</td><td> <b>Sent by: </b></td><td>"& mysentby &"</td></tr>"
		 Response.Write "<tr><td>&nbsp;</td><td> <b>Sent: </b></td><td>"& mysent &"</td></tr>"
		 Response.Write "<tr><td></td><td> <b>Received: </b></td><td>"& myreceived &" </td></tr>"
		 Response.Write "<tr><td></td><td> <b>Read: </b></td><td>"& myread &"</td></tr>"
		 Response.Write "<tr><td>&nbsp;</td><td> <b>Open Rate: </b></td><td>"& myopen &"%</td></tr>"
		 Response.Write "<tr><td>&nbsp;</td><td> <b>CTR: </b></td><td>"& myctr &"%</td></tr>"






	else
	Response.Write "<tr><td> Error: No such alert in database! </td></tr>"
	end if
	RS.Close
%>

		
		</table>
		 <img src="images/inv.gif" alt="" width="1" height="100%" border="0">
		</div>
		</td>
		</tr>
	</table>
	</td>
	</tr>
</table>
</body>
</html>
<!-- #include file="db_conn_close.asp" -->