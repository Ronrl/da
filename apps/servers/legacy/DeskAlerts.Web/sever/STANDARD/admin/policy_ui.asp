<%

function addGroups(id)
	if(id<>0) then
		Set RS = Conn.Execute("SELECT groups.id as id, name FROM policy_group INNER JOIN groups ON policy_group.group_id=groups.id WHERE policy_id=" & id)
		Do While Not RS.EOF
			response.write "<option value=""groups_"&RS("id")&""">"&HtmlEncode(RS("name"))&"</option>"
			RS.MoveNext
		loop
	end if
end function

function addUsers(id)
	if(id<>0) then
		Set RS = Conn.Execute("SELECT users.id as id, name FROM policy_user INNER JOIN users ON policy_user.user_id=users.id WHERE policy_id=" & id)
		Do While Not RS.EOF
			response.write "<option value=""users_"&RS("id")&""">"&HtmlEncode(RS("name"))&"</option>"
			RS.MoveNext
		loop
	end if
end function

function addTgroups(id)
	if(id<>0) then
	    Set RS = Conn.Execute("SELECT a.name, a.id FROM message_templates_groups as a INNER JOIN policy_tgroups as b ON a.id = b.tgroup_id WHERE b.policy_id=" & id)
	    Do While Not RS.EOF
	        response.write "<option value=""users_"&RS("id")&""">"&HtmlEncode(RS("name"))&"</option>"
	        RS.MoveNext
	    loop
	end if
end function

function addComputers(id)
	if(id<>0) then
		Set RS = Conn.Execute("SELECT computers.id as id, name FROM policy_computer INNER JOIN computers ON policy_computer.comp_id=computers.id WHERE policy_id=" & id)
		Do While Not RS.EOF
			response.write "<option value=""computers_"&RS("id")&""">"&HtmlEncode(RS("name"))&"</option>"
			RS.MoveNext
		loop
	end if
end function

function addOUs(id)
	if(id<>0) then
		Set RS = Conn.Execute("SELECT OU.id as id, name FROM policy_ou INNER JOIN OU ON policy_ou.ou_id=OU.Id WHERE policy_id=" & id)
		Do While Not RS.EOF
			response.write "<option value=""ous_"&RS("id")&""">"&HtmlEncode(RS("name"))&"</option>"
			RS.MoveNext
		loop
	end if
end function

function addViewEditors(id)
	if(id<>0) then
		Set RS = Conn.Execute("SELECT users.id as id, name FROM policy_viewer INNER JOIN users ON policy_viewer.editor_id=users.id WHERE policy_viewer.policy_id=" & id)
		Do While Not RS.EOF
			response.write "<option value=""editor_"&RS("id")&""">"&HtmlEncode(RS("name"))&"</option>"
			RS.MoveNext
		loop
	end if
end function

function policyUI(id)

    'Response.Write "ID = " & id
	id=clng(id)
	alerts_checked_arr = Array("","","","","","","", "")
	emails_checked_arr = Array("","","","","","","", "")
	sms_checked_arr = Array("","","","","","","", "")
	surveys_checked_arr = Array("","","","","","","", "")
	users_checked_arr = Array("","","","","","","", "")
	groups_checked_arr = Array("","","","","","","", "")
	templates_checked_arr = Array("","","","","","","", "")
	text_templates_checked_arr = Array("","","","","","","", "")
	statistics_checked_arr = Array("","","","","","","", "")
	ip_groups_checked_arr = Array("","","","","","","", "")
	rss_checked_arr = Array("","","","","","","", "")
	screensavers_checked_arr = Array("","","","","","","", "")
	wallpapers_checked_arr = Array("","","","","","","", "")
	im_checked_arr = Array("","","","","","","", "")
    text_to_call_checked_arr = Array("","","","","","","", "")
    feedback_checked_arr = Array("","","","","","","", "")
    cc_checked_arr = Array("","","","","","","", "")
    webplugin_checked_arr = Array("","","","","","","", "")
    feedback_checked_arr = Array("","","","","","","", "")
    campaign_checked_arr = Array("","","","","","","", "")
if(id > 0) then
	all_checked = "checked"
	Set RS = Conn.Execute("SELECT alerts_val, emails_val, sms_val, surveys_val, users_val, groups_val, templates_val, text_templates_val, statistics_val, ip_groups_val, rss_val, screensavers_val, wallpapers_val, lockscreens_val, im_val, text_to_call_val, feedback_val, cc_val, webplugin_val, feedback_val, campaign_val FROM policy_list WHERE policy_id=" & id)
	if(Not RS.EOF) then
		if(Not IsNull(RS("alerts_val"))) then
			if(RS("alerts_val") = "1111001") then
				alerts_checked = "checked"
			else
				all_checked = ""
			end if
			if(RS("alerts_val") <> "0000000") then
				alerts_any_checked = "checked"
			end if
			alerts_checked_arr = p_makeCheckedArray(RS("alerts_val"))
		end if
		
		
		
		if(Not IsNull(RS("campaign_val"))) then
			if(RS("campaign_val") = "1111110") then
				campaign_checked = "checked"
			else
				all_checked = ""
			end if
			campaign_checked_arr = p_makeCheckedArray(RS("campaign_val"))
		end if	
		
		
		
			
		
		if(Not IsNull(RS("emails_val"))) then
			if(RS("emails_val") = "0001000") then
				emails_checked = "checked"
			else
				all_checked = ""
			end if
			emails_checked_arr = p_makeCheckedArray(RS("emails_val"))
		end if
		if(Not IsNull(RS("sms_val"))) then
			if(RS("sms_val") = "0001000") then
				sms_checked = "checked"
			else
				all_checked = ""
			end if
			sms_checked_arr = p_makeCheckedArray(RS("sms_val"))
		end if
		if(Not IsNull(RS("surveys_val"))) then
			if(RS("surveys_val") = "0011110") then
				surveys_checked = "checked"
			else
				all_checked = ""
			end if
			surveys_checked_arr = p_makeCheckedArray(RS("surveys_val"))
		end if
		if(Not IsNull(RS("users_val"))) then
			if(RS("users_val") = "1110000") then
				users_checked = "checked"
			end if
			users_checked_arr = p_makeCheckedArray(RS("users_val"))
		end if
		if(Not IsNull(RS("groups_val"))) then
			if(RS("groups_val") = "1110100") then
				groups_checked = "checked"
			else
				all_checked = ""
			end if
			groups_checked_arr = p_makeCheckedArray(RS("groups_val"))
		end if
		if(Not IsNull(RS("templates_val"))) then
			if(RS("templates_val") = "1111100") then
				templates_checked = "checked"
			else
				all_checked = ""
			end if
			templates_checked_arr = p_makeCheckedArray(RS("templates_val"))
		end if
		if(Not IsNull(RS("text_templates_val"))) then
			if(RS("text_templates_val") = "1111100") then
				text_templates_checked = "checked"
			else
				all_checked = ""
			end if
			text_templates_checked_arr = p_makeCheckedArray(RS("text_templates_val"))
		end if
		if(Not IsNull(RS("statistics_val"))) then
			if(RS("statistics_val") = "0000100") then
				statistics_checked = "checked"
			else
				all_checked = ""
			end if
			statistics_checked_arr = p_makeCheckedArray(RS("statistics_val"))
		end if
		if(Not IsNull(RS("ip_groups_val"))) then
			if(RS("ip_groups_val") = "1111100") then
				ip_groups_checked = "checked"
			else
				all_checked = ""
			end if
			ip_groups_checked_arr = p_makeCheckedArray(RS("ip_groups_val"))
		end if
		if(Not IsNull(RS("rss_val"))) then
			if(RS("rss_val") = "1111111") then
				rss_checked = "checked"
			else
				all_checked = ""
			end if
			rss_checked_arr = p_makeCheckedArray(RS("rss_val"))
		end if
		if(Not IsNull(RS("screensavers_val"))) then
			if(RS("screensavers_val") = "1111110") then
				screensavers_checked = "checked"
			else
				all_checked = ""
			end if
			screensavers_checked_arr = p_makeCheckedArray(RS("screensavers_val"))
		end if
		if(Not IsNull(RS("webplugin_val"))) then
			if(RS("webplugin_val") = "1111110") then
				webplugin_checked = "checked"
			else
				all_checked = ""
			end if
			webplugin_checked_arr = p_makeCheckedArray(RS("webplugin_val"))
		end if
		if(Not IsNull(RS("feedback_val"))) then
			if(RS("feedback_val") = "0000100") then
				feedback_checked = "checked"
			else
				all_checked = ""
			end if
			feedback_checked_arr = p_makeCheckedArray(RS("feedback_val"))
		end if
		if(Not IsNull(RS("wallpapers_val"))) then
			if(RS("wallpapers_val") = "1111110") then
				wallpapers_checked = "checked"
			else
				all_checked = ""
			end if
			wallpapers_checked_arr = p_makeCheckedArray(RS("wallpapers_val"))
		end if
                if(Not IsNull(RS("lockscreens_val"))) then
			if(RS("lockscreens_val") = "1111110") then
				lockscreens_checked = "checked"
			else
				all_checked = ""
			end if
			lockscreens_checked_arr = p_makeCheckedArray(RS("lockscreens_val"))
		end if
		if(Not IsNull(RS("im_val"))) then
			if(RS("im_val") = "1011100") then
				im_checked = "checked"
			else
				all_checked = ""
			end if
			im_checked_arr = p_makeCheckedArray(RS("im_val"))
		end if
		if(Not IsNull(RS("text_to_call_val"))) then
			if(RS("text_to_call_val") = "0001000") then
				text_to_call_checked = "checked"
			else
				all_checked = ""
			end if
			text_to_call_checked_arr = p_makeCheckedArray(RS("text_to_call_val"))
		end if
		if(Not IsNull(RS("feedback_val"))) then
			if(RS("feedback_val") = "0010100") then
				feedback_checked = "checked"
			else
				all_checked = ""
			end if
			feedback_checked_arr = p_makeCheckedArray(RS("feedback_val"))
		end if
		if(Not IsNull(RS("cc_val"))) then
			if(RS("cc_val") = "1110100") then
				text_to_call_checked = "checked"
			else
				all_checked = ""
			end if
			cc_checked_arr = p_makeCheckedArray(RS("cc_val"))
		end if
	end if
else
	all_checked = ""
	i = 0
end if

%>
<style>
	.checkbox_col {display: none}
	
</style>
<script language="javascript" type="text/javascript">

$(document).ready(function() {
	$("#recipients_tabs").tabs();
	$("#policy_tabs").tabs();
});

$(document).tooltip({
    items: "img[title],a[title]",
    position: {
        my: "center bottom-20",
        at: "center top",
        using: function (position, feedback) {
            $(this).css(position);
            $("<div>")
           .addClass("arrow")
           .addClass(feedback.vertical)
           .addClass(feedback.horizontal)
           .appendTo(this);
        }
    }
});


</script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>

<script language="javascript" type="text/javascript">


tabType="users";

function check_tab(type)
{
	var list1 = document.getElementById("list1");

	if(type=="groups"){
		list1.src="policy_get_groups.asp";
		tabType="groups";
	}
	if(type=="users"){
		list1.src="policy_get_users.asp";
		tabType="users";
	}
	if(type=="ous"){
		list1.src="policy_get_ous.asp";
		tabType="ous";
	}
	if(type=="computers"){
		list1.src="policy_get_computers.asp";
		tabType="computers";
	}
}

function checkNewId(newId, listname)
{
	var list2 = document.getElementById(listname);
	for (i=list2.options.length - 1; i>=0; i--){
		if (list2.options[i].value == newId){
			return false;
		}
	}
	return true;
}

function getBeforeItem()
{
	var list2 = document.getElementById("list2");
	for (i=list2.options.length - 1; i>=0; i--)
	{
		if(tabType=="groups"){
//if ad - ous
<% if AD = 3 then %>
			if (list2.options[i].value == "ous"){
				return i;
			}
		}
		if(tabType=="ous"){
			if (list2.options[i].value == "computers"){
				return i;
			}
		}
		if(tabType=="computers"){
			if (list2.options[i].value == "users"){
				return i;
			}
		}
<% else %>
			if (list2.options[i].value == "users"){
				return i;
			}
		}
<% end if %>
		if(tabType=="users"){
			if (list2.options[i].value == "users"){
				return i;
			}
		}
	}
}

function list_add(iframename, listname)
{
	var list1 = window.frames[iframename];
	var list2 = document.getElementById(listname);
//ous
	if(tabType=="ous")
	{
		res = list1.getCheckedOus();
		idSequence = "";
		if( res )
		{
			for(i=0;i<res.length;i++)
			{
				if(res[i].type != "organization" && res[i].type != "DOMAIN")
				{
					var newVal = res[i].name.toString();
					var newId = tabType+"_"+res[i].id.toString();
					if(checkNewId(newId, listname)){
						var newRow = new Option(newVal,newId);
						if(tabType != "users"){
							var beforeItem = getBeforeItem();
							var elOptOld = list2.options[beforeItem];
							try {
								list2.add(newRow, elOptOld); // standards compliant; doesn't work in IE
							}
							catch(ex) {
								list2.add(newRow, beforeItem); // IE only
							}
						}
						else{
							list2.options[list2.length]=newRow;
						}
					}
				}
			}
		}
	}
	else
	{
//other
	var items_ids = list1.document.getElementsByName("users");
	var items_values = list1.document.getElementsByName("users_name");

	for(var i=0; i < items_ids.length; i++){
		if(jQuery(items_ids[i]).is(":checked")){
			var newVal = items_values[i].value;
			var newId = tabType+"_"+items_ids[i].value;
			if(checkNewId(newId, listname)){
				var newRow = new Option(newVal,newId);
				if(tabType != "users"){
					var beforeItem = getBeforeItem();
					var elOptOld = list2.options[beforeItem];
					try {
						list2.add(newRow, elOptOld); // standards compliant; doesn't work in IE
					}
					catch(ex) {
						list2.add(newRow, beforeItem); // IE only
					}
				}
				else{
					list2.options[list2.length]=newRow;
				}
			}
		}
	}
	}
}

function list_remove(listname)
{
	var list2 = document.getElementById(listname);
	for (i=list2.options.length - 1; i>=0; i--)
	{
		if (list2.options[i].selected == true && !list2.options[i].disabled)
		{
			list2.options[i]=null;
		}
	}
}

function switch_list(val)
{
	switch_list_woch(val);
	check_all_lists();
}

function switch_list_woch(val)
{
	var elem = document.getElementById(val);
	for(var i=0; i<=7; i++)
	{
		var el = document.getElementById(val+"_"+i);
		if(el)
		{
			el.checked=elem.checked;
			set_fake(el, val+"_"+i);
		}
	}
}

function set_fake (el, val) {
	if (el.id != val) {
	    var parent = document.getElementById(val);

		if (parent) {
			parent.checked = el.checked;
		}
	}

	for (var i=0; i<=6; i++) {
	    var child = document.getElementById(val+"_"+i);

		if (child) {
			child.checked = el.checked;
		}
	}
}

function set_if_any(val, targ)
{
	var checked = false;
	for(var i=0; i<=6; i++)
	{
		//if(targ == val+"_"+i) continue
		var child = document.getElementById(val+"_"+i);
		if(child && child.checked)
		{
			checked = true;
		}
	}
	document.getElementById(targ).checked = checked;
}
function check_list(val)
{
	var elem = document.getElementById(val);
	var all_checked = true;
	for(var i=0; i<=6; i++){
		var child = document.getElementById(val+"_"+i);
		if(child && !child.checked){
			all_checked = false;
		}
	}
	elem.checked = all_checked;
	check_all_lists();
}

function check_all_lists()
{
	var check_all = document.getElementById('check_all');
	if(check_all)
	{
		var all_checked = true;
		for(key in common_inputs)
		{
			var elem = document.getElementById(common_inputs[key]);
			if(elem && !elem.checked)
			{
				all_checked = false;
			}
		}
		check_all.checked = all_checked;
	}
}

var common_inputs = [
	'alerts',
<% if EM = 1 then %>
	'emails',
<% end if %>
<% if SMS = 1 then %>
	'sms',
<% end if %>
<% if SURVEY = 1 then %>
	'surveys',
<% end if %>
	'users',
	'groups',
	'templates',
	'text_templates',
	'ip_groups',
<% if RSS = 1 then %>
	'rss',
<% end if %>
<% if SS = 1 then %>
	'screensavers',
<% end if %>
<% if WP = 1 then %>
	'wallpapers',
<% end if %>
<% if IM = 1 then %>
	'im',
	'cc',
<% end if %>
	'feedback',
<% if WEBPLUGIN = 1 then %>
	'webplugin',
<% end if %>
<% if TEXT_TO_CALL = 1 then %>
	'text_to_call',
<% end if %>
	'statistics'
];

function switch_all()
{
	var check_all = document.getElementById('check_all');
	if(check_all)
	{
		for(key in common_inputs)
		{
			var elem = document.getElementById(common_inputs[key]);
			if(elem)
			{
				elem.checked = check_all.checked;
				switch_list_woch(common_inputs[key]);
			}
		}
	}
}

function removeIfDisabled(obj, elemToDisable)
{
    if(obj.checked == 0)
    {
        var elem = document.getElementById(elemToDisable);

        if(elem)
        {
            elem.checked = 0;
        }
    }
}


function grantFullAccessFor(obj, groupName)
{
    if(obj.checked == 0)
       return;
   for( i = 0; i < 6; i++)
   {
        set_fake(obj, groupName+"_"+i);
   } 
}
</script>
<style>
.label_only
{
	cursor:pointer;
}
</style>
<div id="policy_tabs" style="">
	<ul>
		<li><a href="#access_control_listTab"><%=LNG_ACCESS_CONTROL_LIST%></a></li>
		<li><a href="#policy_recipientsTab"><%=LNG_LIST_OF_RECIPIENTS%></a></li>
		<li><a href="#policy_viewersTab"><%=LNG_EDITOR_VIEW_RIGHTS%></a></li>
	<% if ConfTGroupEnabled = 1 then %>
		<li><a href="#policy_tgroupsTab"><%=LNG_LIST_OF_TGROUPS%></a></li>
	<% end if %>
	</ul>
	<%
	
	     Set rs = Conn.execute("SELECT val FROM settings WHERE name = 'ConfAllowApprove'") 
	 %>
	<div id="access_control_listTab">
		<div style="margin-bottom:5px">
			<input type="checkbox" id="check_all" onclick="switch_all()" value="1" class="allheckbox" <%=all_checked %>/>
			<label for="check_all"><%=LNG_ALLOW_ALL %></label>
		</div>
		<table class="data_table" cellspacing="0" cellpadding="3" >
			<tr class="data_table_title">
				<td width="10" class="table_title checkbox_col"></td>
				<td width="150" class="table_title">&nbsp;</td>
				<td width="50" class="table_title"><%=LNG_CREATE %></td>
				<td width="50" class="table_title"><%=LNG_EDIT %></td>
				<td width="50" class="table_title"><%=LNG_DELETE %></td>
				<td width="50" class="table_title"><%=LNG_SEND %></td>
				<td width="50" class="table_title"><%=LNG_VIEW %></td>
				<td width="50" class="table_title"><%=LNG_STOP %></td>
				<td width="50" style="display:none" class="table_title"><%=LNG_RESIZE %></td>
	        <% if APPROVE = 1 and rs("val") = "1" then %>	
				<td width="50" class="table_title"><%=LNG_APPROVAL %></td>
				<% end if %>
			</tr>

			<tr>
				<td class="checkbox_col"><input type="checkbox" name="alerts" id="alerts" onclick="switch_list('alerts');" value="1" class="linecheckbox" <%=alerts_checked %> /></td><td><label class="label_only" for="alerts"><%=LNG_DESKTOP_ALERTS %></label></td>
				<td align="center"><input type="checkbox" name="alerts_0" id="alerts_0" onclick="<% if ConfTGroupEnabled <> 1 then %>set_fake(this, 'alerts_3');check_list('alerts');set_if_any('alerts', 'alerts_4'); set_fake(this, 'alerts_1'); set_if_any(this, 'emails_3');<% end if %>" <%=alerts_checked_arr(0) %>  value="1" /></td>
				<td align="center"><input type="checkbox" name="alerts_1" id="alerts_1" onclick="set_fake(this, 'alerts_0');set_fake(this, 'alerts_1');set_fake(this, 'alerts_3');check_list('alerts');set_if_any('alerts', 'alerts_4');" <%=alerts_checked_arr(1) %> value="1" /></td>
				<td align="center"><input type="checkbox" name="alerts_2" id="alerts_2" onclick="check_list('alerts');set_if_any('alerts', 'alerts_4');" <%=alerts_checked_arr(2) %> value="1" /></td>
				<td align="center"><input type="checkbox" name="alerts_3" id="alerts_3" onclick="<% if ConfTGroupEnabled <> 1 then %>set_fake(this, 'alerts_0');set_fake(this, 'alerts_1');<% end if %>check_list('alerts');set_if_any('alerts', 'alerts_4'); set_if_any(this, 'emails_3');" <%=alerts_checked_arr(3) %> value="1" /></td>
				<td align="center"><input type="checkbox" name="alerts_4" id="alerts_4" onclick="set_if_any('alerts', 'alerts_4');check_list('alerts');" <%=alerts_checked_arr(4) %> value="1" /></td>
				<td align="center"><input type="checkbox" name="alerts_5" id="alerts_5" onclick="check_list('alerts');set_if_any('alerts', 'alerts_4');" <%=alerts_checked_arr(5) %> value="1" /></td>
				<td align="center" style="display:none" ><input type="checkbox" name="alerts_6" id="alerts_6" onclick="check_list('alerts');set_if_any('alerts', 'alerts_4');" <%=alerts_checked_arr(6) %> value="1" /></td>
	        <% if APPROVE = 1 and rs("val") = "1" then %>			
				<td align="center"><input type="checkbox" name="alerts_7" id="alerts_7" onclick="grantFullAccessFor(this, 'alerts');" <%=alerts_checked_arr(7) %> value="1" /></td>
			<% elseif APPROVE = 1 and  rs("val") <> "1" then %>
			    <input type="hidden" name="alerts_7" id="alerts_7" onclick="grantFullAccessFor(this, 'alerts');" <%=alerts_checked_arr(7) %> value="0" />
			<% end if %>	
				
			</tr>

			<tr<% if EM<>1 then response.write " style='display: none'" end if %>>
				<td class="checkbox_col"><input type="checkbox" name="emails" id="emails" onclick="switch_list('emails')" value="1" class="linecheckbox" <%=emails_checked %> /></td><td><label  class="label_only" for="emails"><%=LNG_EMAILS %></label></td>
				<td align="center">&nbsp;</td>
				<td align="center">&nbsp;</td>
				<td align="center">&nbsp;</td>
				<td align="center"><input type="checkbox" name="emails_3" id="emails_3" onclick="check_list('emails'); if(this.checked==true) {document.getElementById('alerts_0').checked=true;document.getElementById('alerts_3').checked=true;}" <%=emails_checked_arr(3) %>  value="1" /></td>
				<td align="center">&nbsp;</td>
				<td align="center">&nbsp;</td>
				<td align="center" style="display:none" >&nbsp;</td>
				 <% if APPROVE = 1 and rs("val") = "1" then %>	
				<td align="center">&nbsp;</td>
				
				<% end if %>
			</tr>

			<tr<% if SMS<>1 then response.write " style='display: none'" end if %>>
				<td class="checkbox_col"><input type="checkbox" name="sms" id="sms" onclick="switch_list('sms')" value="1" class="linecheckbox" <%=sms_checked %> /></td><td><label  class="label_only" for="sms"><%=LNG_SMS %></label></td>
				<td align="center">&nbsp;</td>
				<td align="center">&nbsp;</td>
				<td align="center">&nbsp;</td>
				<td align="center"><input type="checkbox" name="sms_3" id="sms_3" onclick="check_list('sms');if(this.checked==true){document.getElementById('alerts_0').checked=true;document.getElementById('alerts_3').checked=true;}" <%=sms_checked_arr(3) %>  value="1" /></td>
				<td align="center">&nbsp;</td>
				<td align="center">&nbsp;</td>
				<td align="center" style="display:none" >&nbsp;</td>
				<% if APPROVE = 1 and rs("val") = "1" then %>	
				<td align="center">&nbsp;</td>
				
				<% end if %>
			</tr>

			<tr<% if SURVEY<>1 then response.write " style='display: none'" end if %>>
				<td class="checkbox_col"><input type="checkbox" name="surveys" id="surveys" onclick="switch_list('surveys')" value="1" class="linecheckbox" <%=surveys_checked %> /></td><td><label  class="label_only" for="surveys"><%=LNG_SURVEYS %></label></td>
				<td align="center"><input type="checkbox" name="surveys_0" id="surveys_0" onclick="set_fake(this, 'surveys_3');check_list('surveys');set_if_any('surveys', 'surveys_4');set_fake(this, 'surveys_1');" <%=surveys_checked_arr(3) %> value="1" /></td>
				<td align="center"><input type="checkbox" name="surveys_1" id="surveys_1" onclick="set_fake(this, 'surveys_0');set_fake(this, 'surveys_3');check_list('surveys');set_if_any('surveys', 'surveys_4');" <%=surveys_checked_arr(1) %> value="1" /></td>
				<td align="center"><input type="checkbox" name="surveys_2" id="surveys_2" onclick="check_list('surveys');set_if_any('surveys', 'surveys_4');" <%=surveys_checked_arr(2) %> value="1" /></td>
				<td align="center"><input type="checkbox" name="surveys_3" id="surveys_3" onclick="set_fake(this, 'surveys_3');set_fake(this, 'surveys_0');check_list('surveys');set_if_any('surveys', 'surveys_4');" <%=surveys_checked_arr(3) %> value="1" /></td>
				<td align="center"><input type="checkbox" name="surveys_4" id="surveys_4" onclick="check_list('surveys');removeIfDisabled(this, 'surveys_5');removeIfDisabled(this, 'surveys_2');" <%=surveys_checked_arr(4) %> value="1" /></td>
				<td align="center"><input type="checkbox" name="surveys_5" id="surveys_5" onclick="check_list('surveys');set_if_any('surveys', 'surveys_4')" <%=surveys_checked_arr(5) %> value="1" /></td>
				<td align="center" style="display:none" >&nbsp;</td>
				<% if APPROVE = 1 and  rs("val") = "1" then %>
				<td align="center"><input type="checkbox" name="surveys_6" id="surveys_6" onclick="grantFullAccessFor(this, 'surveys');" <%=surveys_checked_arr(6) %> value="1" /></td>
				<% elseif APPROVE = 1 and  rs("val") <> "1" then %>
				    <input type="hidden" name="surveys_6" id="surveys_6" onclick="grantFullAccessFor(this, 'surveys');" <%=surveys_checked_arr(6) %> value="0" />
				<% end if %>
			</tr>

			<tr>
				<td class="checkbox_col"><input type="checkbox" name="users" id="users" onclick="switch_list('users')" value="1" class="linecheckbox" <%=users_checked %> /></td><td><label  class="label_only" for="users"><%=LNG_USERS %></label></td>
				<td align="center"><input type="checkbox" name="users_0" id="users_0" onclick="check_list('users');" <%=users_checked_arr(0) %> value="1" /></td>
				<td align="center"><input type="checkbox" name="users_1" id="users_1" onclick="check_list('users');" <%=users_checked_arr(1) %> value="1" /></td>
				<td align="center"><input type="checkbox" name="users_2" id="users_2" onclick="check_list('users');" <%=users_checked_arr(2) %> value="1" /></td>
				<td align="center">&nbsp;</td>
				<td align="center">&nbsp;</td>
				<td align="center">&nbsp;</td>
				<td align="center" style="display:none" >&nbsp;</td>
				<% if APPROVE = 1 and rs("val") = "1" then %>
				<td align="center">&nbsp;</td>
				<% end if %>
			</tr>

			<tr>
				<td class="checkbox_col"><input type="checkbox" name="groups" id="groups" onclick="switch_list('groups')" value="1" class="linecheckbox" <%=groups_checked %> /></td><td><label  class="label_only" for="groups"><%=LNG_GROUPS %></label></td>
				<td align="center"><input type="checkbox" name="groups_0" id="groups_0" onclick="check_list('groups');set_fake(this, 'groups_4');"  <%=groups_checked_arr(0) %> value="1" /></td>
				<td align="center"><input type="checkbox" name="groups_1" id="groups_1" onclick="check_list('groups');set_if_any('groups', 'groups_4')" <%=groups_checked_arr(1) %> value="1" /></td>
				<td align="center"><input type="checkbox" name="groups_2" id="groups_2" onclick="check_list('groups');set_if_any('groups', 'groups_4')" <%=groups_checked_arr(2) %> value="1" /></td>
				<td align="center">&nbsp;</td>
				<td align="center"><input type="checkbox" name="groups_4" id="groups_4" onclick="check_list('groups');removeIfDisabled(this, 'groups_2');removeIfDisabled(this, 'groups_1');removeIfDisabled(this, 'groups_0');" <%=groups_checked_arr(4) %> value="1" /></td>
				<td align="center">&nbsp;</td>
				<td align="center" style="display:none" >&nbsp;</td>
				<% if APPROVE = 1 and rs("val") = "1" then %>
				<td align="center">&nbsp;</td>
				<% end if %>
			</tr>

			<tr>
				<td class="checkbox_col"><input type="checkbox" name="text_templates" id="text_templates" onclick="switch_list('text_templates')" class="linecheckbox" value="1" <%=text_templates_checked %> /></td><td><label  class="label_only" for="text_templates" ><%=LNG_TEXT_TEMPLATES %></label></td>
				<td align="center"><input type="checkbox" name="text_templates_0"  id="text_templates_0" onclick="check_list('text_templates');set_if_any('text_templates', 'text_templates_4')" <%=text_templates_checked_arr(0) %> value="1" /></td>
				<td align="center"><input type="checkbox" name="text_templates_1"  id="text_templates_1" onclick="check_list('text_templates');set_if_any('text_templates', 'text_templates_4')" <%=text_templates_checked_arr(1) %> value="1" /></td>
				<td align="center"><input type="checkbox" name="text_templates_2"  id="text_templates_2" onclick="check_list('text_templates');set_if_any('text_templates', 'text_templates_4')" <%=text_templates_checked_arr(2) %> value="1" /></td>
				<td align="center"><input type="checkbox" name="text_templates_3"  id="text_templates_3" onclick="check_list('text_templates');/*set_fake(this, 'alerts_3');set_fake(this, 'alerts_4');set_fake(this, 'alerts_0');set_if_any('text_templates', 'text_templates_4');*/<% if ConfTGroupEnabled <> 1 then %>document.getElementById('alerts_0').checked=this.checked; <% end if %>document.getElementById('alerts_3').checked=this.checked;document.getElementById('alerts_4').checked=this.checked;"<%=text_templates_checked_arr(3) %> value="1" /></td>
				<td align="center"><input type="checkbox" name="text_templates_4"  id="text_templates_4" onclick="check_list('text_templates');removeIfDisabled(this, 'text_templates_2');removeIfDisabled(this, 'text_templates_1');removeIfDisabled(this, 'text_templates_0');" <%=text_templates_checked_arr(4) %> value="1" /></td>
				<td align="center">&nbsp;</td>
				<td align="center" style="display:none" >&nbsp;</td>
				<% if APPROVE = 1 and rs("val") = "1"  then %>
				<td align="center">&nbsp;</td>
				<% end if %>
			</tr>
			
			<tr style='display:none;'>
				<td class="checkbox_col"><input type="checkbox" name="templates" id="templates" onclick="switch_list('templates')" class="linecheckbox" value="1" <%=templates_checked %> /></td><td><label  class="label_only" for="templates"><%=LNG_TEMPLATES %></label></td>
				<td align="center"><input type="checkbox" name="templates_0"  id="templates_0" onclick="check_list('templates')" <%=templates_checked_arr(0) %> value="1" /></td>
				<td align="center"><input type="checkbox" name="templates_1"  id="templates_1" onclick="check_list('templates');set_if_any('templates', 'templates_4')" <%=templates_checked_arr(1) %> value="1" /></td>
				<td align="center"><input type="checkbox" name="templates_2"  id="templates_2" onclick="check_list('templates');set_if_any('templates', 'templates_4')" <%=templates_checked_arr(2) %> value="1" /></td>
				<td align="center">&nbsp;</td>
				<td align="center"><input type="checkbox" name="templates_4"  id="templates_4" onclick="check_list('templates')" <%=templates_checked_arr(4) %> value="1" /></td>
				<td align="center">&nbsp;</td>
				<td align="center" style="display:none" >&nbsp;</td>
				<% if APPROVE = 1 and rs("val") = "1"  then %>
				<td align="center">&nbsp;</td>
				<% end if %>
			</tr>
			
			<tr>
				<td class="checkbox_col"><input type="checkbox" name="statistics" id="statistics" onclick="switch_list('statistics')" value="1" class="linecheckbox" <%=statistics_checked %> /></td><td><label  class="label_only" for="statistics"><%=LNG_STATISTICS %></label></td>
				<td align="center">&nbsp;</td>
				<td align="center">&nbsp;</td>
				<td align="center">&nbsp;</td>
				<td align="center">&nbsp;</td>
				<td align="center"><input type="checkbox" name="statistics_4" id="statistics_4" onclick="check_list('statistics')" <%=statistics_checked_arr(4) %> value="1"/></td>
				<td align="center">&nbsp;</td>
				<td align="center" style="display:none" >&nbsp;</td>
				<% if APPROVE = 1 and rs("val") = "1"  then %>
				<td align="center">&nbsp;</td>
				<% end if %>
			</tr>

			<tr>
				<td class="checkbox_col"><input type="checkbox" name="ip_groups" id="ip_groups" onclick="switch_list('ip_groups')" value="1" class="linecheckbox" <%=ip_groups_checked %>/></td><td><label  class="label_only" for="ip_groups"><%=LNG_IP_GROUPS %></label></td>
				<td align="center"><input type="checkbox" name="ip_groups_0" id="ip_groups_0" onclick="check_list('ip_groups');set_if_any('ip_groups', 'ip_groups_4');if(this.checked==true){document.getElementById('ip_groups_3').checked=true;}else {document.getElementById('ip_groups_0').checked=false;}" <%=ip_groups_checked_arr(0) %> value="1"/></td>
				<td align="center"><input type="checkbox" name="ip_groups_1" id="ip_groups_1" onclick="check_list('ip_groups');set_if_any('ip_groups', 'ip_groups_4')" <%=ip_groups_checked_arr(1) %> value="1"/></td>
				<td align="center"><input type="checkbox" name="ip_groups_2" id="ip_groups_2" onclick="check_list('ip_groups');set_if_any('ip_groups', 'ip_groups_4')" <%=ip_groups_checked_arr(2) %> value="1"/></td>
				<td align="center"><input type="checkbox" name="ip_groups_3" id="ip_groups_3" onclick="check_list('ip_groups');if(this.checked==true){document.getElementById('ip_groups_0').checked=true;}else {document.getElementById('ip_groups_0').checked=false;}" <%=ip_groups_checked_arr(3) %> value="1"/></td>
				<td align="center"><input type="checkbox" name="ip_groups_4" id="ip_groups_4" onclick="check_list('ip_groups');removeIfDisabled(this, 'ip_groups_0');removeIfDisabled(this, 'ip_groups_1');removeIfDisabled(this, 'ip_groups_2');" <%=ip_groups_checked_arr(4) %> value="1"/></td>
				<td align="center">&nbsp;</td>
				<td align="center" style="display:none" >&nbsp;</td>
				<% if APPROVE = 1  and rs("val") = "1" then %>
				<td align="center">&nbsp;</td>
				<% end if %>
			</tr>

			<tr<% if RSS<>1 then response.write " style='display: none'" end if %>>
				<td class="checkbox_col"><input type="checkbox" name="rss" id="rss" onclick="switch_list('rss')" value="1" class="linecheckbox" <%=rss_checked %>/></td><td><label  class="label_only" for="rss"><%=LNG_RSS %></label></td>
				<td align="center"><input type="checkbox" name="rss_0" id="rss_0" onclick="set_fake(this, 'rss_3');set_fake(this, 'rss_1');check_list('rss');set_if_any('rss', 'rss_4')" <%=rss_checked_arr(0) %> value="1"/></td>
				<td align="center"><input type="checkbox" name="rss_1" id="rss_1" onclick="check_list('rss');set_if_any('rss', 'rss_4')" <%=rss_checked_arr(1) %> value="1"/></td>
				<td align="center"><input type="checkbox" name="rss_2" id="rss_2" onclick="check_list('rss');set_if_any('rss', 'rss_4')" <%=rss_checked_arr(2) %> value="1"/></td>
				<td align="center"><input type="checkbox" name="rss_3" id="rss_3" onclick="set_fake(this, 'rss_0');check_list('rss');set_if_any('rss', 'rss_4')" <%=rss_checked_arr(3) %> value="1"/></td>
				<td align="center"><input type="checkbox" name="rss_4" id="rss_4" onclick="check_list('rss');removeIfDisabled(this, 'rss_0');removeIfDisabled(this, 'rss_1');removeIfDisabled(this, 'rss_2');removeIfDisabled(this, 'rss_5');" <%=rss_checked_arr(4) %> value="1"/></td>
				<td align="center"><input type="checkbox" name="rss_5" id="rss_5" onclick="check_list('rss');set_if_any('rss', 'rss_4')" <%=rss_checked_arr(5) %> value="1" /></td>
				<td align="center" style="display:none" ><input type="checkbox" name="rss_6" id="rss_6" onclick="check_list('rss')" <%=rss_checked_arr(6) %> value="1" /></td>
				<% if APPROVE = 1  and rs("val") = "1" then %>
				<td align="center"><input type="checkbox" name="rss_7" id="rss_7" onclick="grantFullAccessFor(this, 'rss');" <%=rss_checked_arr(7) %> value="1" /></td>
			   <% elseif APPROVE = 1 and  rs("val") <> "1" then %>
			    <input type="hidden" name="rss_7" id="rss_7" onclick="grantFullAccessFor(this, 'rss');" <%=rss_checked_arr(7) %> value="0" />
			    <% end if %>
			</tr>

			<tr<% if SS<>1 then response.write " style='display: none'" end if %>>
				<td class="checkbox_col"><input type="checkbox" name="screensavers" id="screensavers" onclick="switch_list('screensavers')" value="1" class="linecheckbox" <%=screensavers_checked %>/></td><td><label  class="label_only" for="screensavers"><%=LNG_SCREENSAVERS %></label></td>
				<td align="center"><input type="checkbox" name="screensavers_0" id="screensavers_0" onclick="set_fake(this, 'screensavers_3');check_list('screensavers');set_if_any('screensavers', 'screensavers_4')" <%=screensavers_checked_arr(0) %> value="1"/></td>
				<td align="center"><input type="checkbox" name="screensavers_1" id="screensavers_1" onclick="check_list('screensavers');set_if_any('screensavers', 'screensavers_4')" <%=screensavers_checked_arr(1) %> value="1"/></td>
				<td align="center"><input type="checkbox" name="screensavers_2" id="screensavers_2" onclick="check_list('screensavers');set_if_any('screensavers', 'screensavers_4')" <%=screensavers_checked_arr(2) %> value="1"/></td>
				<td align="center"><input type="checkbox" name="screensavers_3" id="screensavers_3" onclick="set_fake(this, 'screensavers_0');set_fake(this, 'screensavers_4');check_list('screensavers')" <%=screensavers_checked_arr(3) %> value="1"/></td>
				<td align="center"><input type="checkbox" name="screensavers_4" id="screensavers_4" onclick="check_list('screensavers');set_if_any('screensavers', 'screensavers_4')" <%=screensavers_checked_arr(4) %> value="1"/></td>
				<td align="center"><input type="checkbox" name="screensavers_5" id="screensavers_5" onclick="check_list('screensavers');set_if_any('screensavers', 'screensavers_4');" <%=screensavers_checked_arr(5) %> value="1"/></td>
				<td align="center"  style="display:none" >&nbsp;</td>
				<% if APPROVE = 1 and rs("val") = "1"  then %>
				<td align="center"><input type="checkbox" name="screensavers_6" id="screensavers_6" onclick="grantFullAccessFor(this, 'screensavers');" <%=screensavers_checked_arr(6) %> value="1" /></td>
			     <% elseif APPROVE = 1 and  rs("val") <> "1" then %>
			    <input type="hidden" name="screensavers_6" id="screensavers_6" onclick="grantFullAccessFor(this, 'screensavers');" <%=screensavers_checked_arr(6) %> value="0" />
			     <% end if %>
			</tr>

			<tr<% if WP<>1 then response.write " style='display: none'" end if %>>
				<td class="checkbox_col"><input type="checkbox" name="wallpapers" id="wallpapers" onclick="switch_list('wallpapers')" value="1" class="linecheckbox" <%=wallpapers_checked %>/></td><td><label  class="label_only" for="wallpapers"><%=LNG_WALLPAPERS %></label></td>
				<td align="center"><input type="checkbox" name="wallpapers_0" id="wallpapers_0" onclick="set_fake(this, 'wallpapers_3');set_fake(this, 'wallpapers_1');check_list('wallpapers');set_if_any('wallpapers', 'wallpapers_4')" <%=wallpapers_checked_arr(0) %> value="1"/></td>
				<td align="center"><input type="checkbox" name="wallpapers_1" id="wallpapers_1" onclick="set_fake(this, 'wallpapers_0');set_fake(this, 'wallpapers_3');check_list('wallpapers');set_if_any('wallpapers', 'wallpapers_4')" <%=wallpapers_checked_arr(1) %> value="1"/></td>
				<td align="center"><input type="checkbox" name="wallpapers_2" id="wallpapers_2" onclick="check_list('wallpapers');set_if_any('wallpapers', 'wallpapers_4')" <%=wallpapers_checked_arr(2) %> value="1"/></td>
				<td align="center"><input type="checkbox" name="wallpapers_3" id="wallpapers_3" onclick="set_fake(this, 'wallpapers_0');set_fake(this, 'wallpapers_1');set_fake(this, 'wallpapers_4');check_list('wallpapers')" <%=wallpapers_checked_arr(3) %> value="1"/></td>
				<td align="center"><input type="checkbox" name="wallpapers_4" id="wallpapers_4" onclick="check_list('wallpapers');set_if_any('wallpapers', 'wallpapers_4')" <%=wallpapers_checked_arr(4) %> value="1"/></td>
				<td align="center"><input type="checkbox" name="wallpapers_5" id="wallpapers_5" onclick="check_list('wallpapers');set_if_any('wallpapers', 'wallpapers_4');" <%=wallpapers_checked_arr(5) %> value="1"/></td>
				<td align="center"  style="display:none" >&nbsp;</td>
				<% if APPROVE = 1  and rs("val") = "1" then %>
				<td align="center"><input type="checkbox" name="wallpapers_6" id="wallpapers_6" onclick="grantFullAccessFor(this, 'wallpapers');" <%=wallpapers_checked_arr(6) %> value="1" /></td>
			    <% elseif APPROVE = 1 and  rs("val") <> "1" then %>
			        <input type="hidden" name="wallpapers_6" id="wallpapers_6" onclick="grantFullAccessFor(this, 'wallpapers');" <%=wallpapers_checked_arr(6) %> value="0" />
			    <% end if %>
			</tr>
			<tr<% if IM<>1 then response.write " style='display: none'" end if %>>
				<td class="checkbox_col"><input type="checkbox" name="im" id="im" onclick="switch_list('im')" value="1" class="linecheckbox" <%=im_checked %>/></td><td><label  class="label_only" for="im"><%=LNG_INSTANT_MESSAGES %></label></td>
				<td align="center"><input type="checkbox" name="im_0" id="im_0" onclick="check_list('im');set_if_any('im', 'im_4'); set_fake(this, 'im_1'); set_fake(this, 'im_3');" <%=im_checked_arr(0) %> value="1"/></td>
				<td align="center"><input type="checkbox" name="im_1" id="im_1" onclick="check_list('im');set_if_any('im', 'im_4')" <%=im_checked_arr(1) %> value="1"/></td>
				<td align="center"><input type="checkbox" name="im_2" id="im_2" onclick="check_list('im');set_if_any('im', 'im_4')" <%=im_checked_arr(2) %> value="1"/></td>
				<td align="center"><input type="checkbox" name="im_3" id="im_3" onclick="check_list('im');set_if_any('im', 'im_4')" <%=im_checked_arr(3) %> value="1"/></td>
				<td align="center"><input type="checkbox" name="im_4" id="im_4" onclick="check_list('im'); removeIfDisabled(this, 'im_0');removeIfDisabled(this, 'im_1');removeIfDisabled(this, 'im_2');removeIfDisabled(this, 'im_3');" <%=im_checked_arr(4) %> value="1"/></td>
				<td align="center">&nbsp;</td>
				<td align="center"  style="display:none" >&nbsp;</td>
				<% if APPROVE = 1  and rs("val") = "1" then %>
				<td align="center"><input type="checkbox" name="im_5" id="im_5" onclick="grantFullAccessFor(this, 'im');" <%=im_checked_arr(5) %> value="0" /></td>
			     <% end if %>
			</tr>
			<tr<% if TEXT_TO_CALL<>1 then response.write " style='display: none'" end if %>>
				<td class="checkbox_col"><input type="checkbox" name="text_to_call" id="text_to_call" onclick="switch_list('text_to_call')" value="1" class="linecheckbox" <%=text_to_call_checked %> /></td><td><label  class="label_only" for="text_to_call"><%=LNG_TEXT_TO_CALL %></label></td>
				<td align="center">&nbsp;</td>
				<td align="center">&nbsp;</td>
				<td align="center">&nbsp;</td>
				<td align="center"><input type="checkbox" name="text_to_call_3" id="text_to_call_3" onclick="check_list('text_to_call'); if(document.getElementById('alerts_0').checked!=true) {document.getElementById('alerts_0').click();}" <%=text_to_call_checked_arr(3) %>  value="1" /></td>
				<td align="center">&nbsp;</td>
				<td align="center">&nbsp;</td>
				<td align="center"  style="display:none" >&nbsp;</td>
				<% if APPROVE = 1  and rs("val") = "1" then %>
				<td align="center">&nbsp;</td>
				 <% end if %>
			</tr>
			<tr>
				<td class="checkbox_col"><input type="checkbox" name="cc" id="cc" onclick="switch_list('cc')" value="1" class="linecheckbox" <%=cc_checked %> /></td><td><label  class="label_only" for="cc"><%=LNG_COLOR_CODES %></label></td>
				<td align="center"><input type="checkbox" name="cc_0" id="cc_0" onclick="check_list('cc');set_if_any('cc', 'cc_4')"  <%=cc_checked_arr(0) %> value="1" /></td>
				<td align="center"><input type="checkbox" name="cc_1" id="cc_1" onclick="check_list('cc');set_if_any('cc', 'cc_4')" <%=cc_checked_arr(1) %> value="1" /></td>
				<td align="center"><input type="checkbox" name="cc_2" id="cc_2" onclick="check_list('cc');set_if_any('cc', 'cc_4')" <%=cc_checked_arr(2) %> value="1" /></td>
				<td align="center">&nbsp;</td>
				<td align="center"><input type="checkbox" name="cc_4" id="cc_4" onclick="check_list('cc');removeIfDisabled(this, 'cc_0');removeIfDisabled(this, 'cc_1');removeIfDisabled(this, 'cc_2');removeIfDisabled(this, 'cc_3');" <%=cc_checked_arr(4) %> value="1" /></td>
				<td align="center">&nbsp;</td>
				<td align="center"  style="display:none" >&nbsp;</td>
				<% if APPROVE = 1  and rs("val") = "1" then %>
				<td align="center">&nbsp;</td>
				<% end if %>
			</tr>
			<tr style="display:none">
				<td class="checkbox_col"><input type="checkbox" name="feedback" id="feedback" onclick="switch_list('feedback')" value="1" class="linecheckbox" <%=feedback_checked %> /></td><td><label  class="label_only" for="feedback"><%=LNG_FEEDBACK %></label></td>
				<td align="center">&nbsp;</td>
				<td align="center">&nbsp;</td>
				<td align="center">&nbsp;</td>
				<td align="center">&nbsp;</td>
				<td align="center"><input type="checkbox" name="feedback_4" id="feedback_4" onclick="check_list('feedback')" <%=feedback_checked_arr(4) %> value="1"/></td>
				<td align="center">&nbsp;</td>
				<td align="center">&nbsp;</td>
				<% if APPROVE = 1  and rs("val") = "1" then %>
				<td align="center">&nbsp;</td>
				<% end if %>
			</tr>
			
			<tr<% if WEBPLUGIN<>1 then response.write " style='display: none'" end if %>>
				<td class="checkbox_col"><input type="checkbox" name="webplugin" id="webplugin" onclick="switch_list('webplugin')" value="1" class="linecheckbox" <%=webplugin_checked %>/></td><td><label  class="label_only" for="webplugin"><%=LNG_DIGSIGN_TITLE %></label></td>
				<td align="center"><input type="checkbox" name="webplugin_0" id="webplugin_0" onclick="set_fake(this, 'webplugin_3');check_list('webplugin');set_if_any('webplugin', 'webplugin_4')" <%=webplugin_checked_arr(0) %> value="1"/></td>
				<td align="center"><input type="checkbox" name="webplugin_1" id="webplugin_1" onclick="check_list('webplugin');set_if_any('webplugin', 'webplugin_4')" <%=webplugin_checked_arr(1) %> value="1"/></td>
				<td align="center"><input type="checkbox" name="webplugin_2" id="webplugin_2" onclick="check_list('webplugin');set_if_any('webplugin', 'webplugin_4')" <%=webplugin_checked_arr(2) %> value="1"/></td>
				<td align="center"><input type="checkbox" name="webplugin_3" id="webplugin_3" onclick="set_fake(this, 'webplugin_0');check_list('webplugin')" <%=webplugin_checked_arr(3) %> value="1"/></td>
				<td align="center"><input type="checkbox" name="webplugin_4" id="webplugin_4" onclick="check_list('webplugin');set_if_any('webplugin', 'webplugin_4')" <%=webplugin_checked_arr(4) %> value="1"/></td>
				<td align="center"><input type="checkbox" name="webplugin_5" id="webplugin_5" onclick="check_list('webplugin')" <%=webplugin_checked_arr(5) %> value="1"/></td>
				<td align="center">&nbsp;</td>
				<% if APPROVE = 1 and rs("val") = "1"  then %>
				<td align="center">&nbsp;</td>
				<% end if %>
			</tr>
		
		<!--	
			<tr<% if CAMPAIGN<>1 then response.write " style='display: none'" end if %>>
			    <td class="checkbox_col"><input type="checkbox" name="campaign" id="campaign" onclick="switch_list('campaign')" value="1" class="linecheckbox" <%=campaign_checked %>/></td><td><label  class="label_only" for="campaign"><%=LNG_CAMPAIGN %></label></td>
			    <td align="center"><input type="checkbox" name="campaign_0" id="campaign_0" onclick="set_fake(this, 'campaign_3');set_fake(this, 'alerts_0'); set_fake(this, 'alerts_3'); set_ check_list('campaign');set_if_any('campaign', 'campaign_4')" <%=campaign_checked_arr(0) %> value="1"/></td>
				<td align="center"><input type="checkbox" name="campaign_1" id="campaign_1" onclick="check_list('campaign');set_if_any('campaign', 'campaign_4')" <%=campaign_checked_arr(1) %> value="1"/></td>
				<td align="center"><input type="checkbox" name="campaign_2" id="campaign_2" onclick="check_list('campaign');set_if_any('campaign', 'campaign_4')" <%=campaign_checked_arr(2) %> value="1"/></td>
				<td align="center"><input type="checkbox" name="campaign_3" id="campaign_3" onclick="set_fake(this, 'campaign_0');check_list('campaign')" <%=campaign_checked_arr(3) %> value="1"/></td>
				<td align="center"><input type="checkbox" name="campaign_4" id="campaign_4" onclick="check_list('campaign');set_if_any('campaign', 'campaign_4')" <%=campaign_checked_arr(4) %> value="1"/></td>
				<td align="center"><input type="checkbox" name="campaign_5" id="campaign_5" onclick="check_list('campaign')" <%=campaign_checked_arr(5) %> value="1"/></td>
				<td align="center">&nbsp;</td>
			</tr>
			-->
		</table>
	</div>
	<div id="policy_recipientsTab">
		<div>
			<input type="radio" id="all_recipients_1" name="all_recipients" value="1" onclick="all_recipients_click(true)"<% if all_recipients = 1 then Response.Write " checked" %>/><label for="all_recipients_1"><%=LNG_CAN_SEND_NOTIFICATIONS_TO_ALL %></label><br/>
			<br/>
			<div><p><input type="radio" id="all_recipients_0" name="all_recipients" value="0" onclick="all_recipients_click(false)"<% if all_recipients = 0 then Response.Write " checked" %>/><label for="all_recipients_0"><%=LNG_CAN_SEND_NOTIFICATIONS_TO_RECIPIENTS %></label>
               <img src="images/help.png" title="Notifications could be sent to groups and to users that are in the groups."/></p>
			</div><br/><br/>
		</div>
		<table id="recipients_table"<% if all_recipients = 1 then Response.Write " style=""display:none""" %>>
		<tr valign="middle" align="center">
			<td align="left">
				<div id="recipients_tabs" style="">
					<ul>
						<li><a href="#recipientsTab" onclick="check_tab('users');"><%=LNG_USERS%></a></li>
						<li><a href="#recipientsTab" onclick="check_tab('groups');"><%=LNG_GROUPS%></a></li>
						<%if (AD=3) then%>
						<li><a href="#recipientsTab" onclick="check_tab('ous');"><%=LNG_OUS%></a></li>
						<li><a href="#recipientsTab" onclick="check_tab('computers');"><%=LNG_COMPUTERS%></a></li>
						<%end if%>
					</ul>
					<div id="recipientsTab">
						<iframe frameborder="0" id="list1" name="list1" class="list_iframe" src="policy_get_users.asp"></iframe>
					</div>
				</div>
			</td>
			<td>
				<%=LNG_ADD %><br>
				<a href="#" onclick="list_add('list1', 'list2');return false;"><img src="images/list_add.png" border="0" /></a><br/>
				<a href="#" onclick="list_remove('list2');return false;"><img src="images/list_remove.png" border="0" /></a><br/><%=LNG_REMOVE %>
			</td>
			<td>
				<b><%=LNG_RECIPIENTS %></b>
				<br/><br/>
				<select multiple id="list2" name="list2" class="list_select">
					<option disabled value="groups"><%=LNG_LIST_OF_SELECTED_GROUPS %>:</option>
					<% addGroups(id) %>
	<% if AD = 3 then %>
					<option disabled value="ous"><%=LNG_LIST_OF_SELECTED_OUS %>:</option>
					<% addOUs(id) %>
					<option disabled value="computers"><%=LNG_LIST_OF_SELECTED_COMPUTERS %>:</option>
					<% addComputers(id) %>
	<% end if %>
					<option disabled value="users"><%=LNG_LIST_OF_SELECTED_USERS %>:</option>
					<% addUsers(id) %>
				</select>
			</td>
		</tr>
		</table>
	</div>
	<div id="policy_viewersTab">
		<div>
			<input type="radio" id="view_all_0" name="view_all" value="0" onclick="view_all_click(false)"<% if view_all = 0 then Response.Write " checked" %>/><label for="view_all_0"><%=LNG_EDITOR_CAN_VIEW_ONLY_SELF_CREATED_NOTIFICATIONS %></label><br/>
			<br/>
			<input type="radio" id="view_all_1" name="view_all" value="1" onclick="view_all_click(false)"<% if view_all = 1 then Response.Write " checked" %>/><label for="view_all_1"><%=LNG_EDITOR_CAN_VIEW_ALL_NOTIFICATIONS %></label><br/>
			<br/>
		</div>
		<!--<table id="recipients_table"<% if view_all <> 2 then Response.Write " style=""display:none""" %>>
		<tr valign="middle" align="center">
			<td align="left">
				<iframe frameborder="0" id="list1" name="list1" class="list_iframe" src="policy_get_users.asp"></iframe>
			</td>
			<td>
				<%=LNG_ADD %><br>
				<a href="#" onclick="list_add();return false;"><img src="images/list_add.png" border="0" /></a><br/>
				<a href="#" onclick="list_remove();return false;"><img src="images/list_remove.png" border="0" /></a><br/><%=LNG_REMOVE %>
			</td>
			<td>
				<b><%=LNG_EDITORS %></b>
				<br/><br/>
				<select multiple id="viewers_list" name="viewers_list" class="list_select">
					<option disabled value="groups"><%=LNG_LIST_OF_SELECTED_EDITORS %>:</option>
					<% addViewEditors(id) %>
				</select>
			</td>
		</tr>
		</table>-->
	</div>
<% if ConfTGroupEnabled = 1 then %>	
	<div id="policy_tgroupsTab">
		<div>
			<input type="radio" id="templates_all_1" name="all_templates" value="1" onclick="all_tgroups_click(true)"<% if all_templates = 1 then Response.Write " checked" %>/><label for="all_recipients_1"><label for="templates_all_1"><%=LNG_LIST_OF_TGROUPS_CAN_SEND_ALL %></label><br/>
			<br/>
			<input type="radio" id="templates_all_0" name="all_templates" value="0" onclick="all_tgroups_click(false)"<% if all_templates = 0 then Response.Write " checked" %>/><label for="all_recipients_0"><label for="templates_all_0"><%=LNG_LIST_OF_TGROUPS_CAN_SEND_SELECTED %></label><br/>
			<br/>
		</div>
		<div id="tgroups" <% if all_templates = 1 then Response.Write " style=""display:none""" %>>
		<table>
		    <tr>
		        <td>
		             <iframe frameborder="0" id="tgroups_list" name="tgroups_list" class="list_iframe" src="policy_get_tgroups.asp"></iframe>
		        </td>
			    <td style="text-align:center;">
				    <%=LNG_ADD %><br>
				    <a href="#" onclick="list_add('tgroups_list','selected_tgroups');return false;"><img src="images/list_add.png" border="0" /></a><br/>
				    <a href="#" onclick="list_remove('selected_tgroups');return false;"><img src="images/list_remove.png" border="0" /></a><br/><%=LNG_REMOVE %>
			    </td>
			    <td>
			        	<b><%=LNG_TGROUPS %></b>
				        <br/><br/>
				        <select multiple id="selected_tgroups" name="list2" class="list_select">
				         <% addTgroups(id) %>
				        </select>
			    </td>
		    </tr>
		
		</table>
		       
		</div>
	</div>
<% end if %>
</div>
<%
end function
%>