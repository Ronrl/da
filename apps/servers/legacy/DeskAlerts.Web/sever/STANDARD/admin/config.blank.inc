<!-- #include file="survey.inc" -->
<!-- #include file="extstats.inc" -->
<!-- #include file="functions.inc" -->

<%
Dim alerts_folder
Dim alerts_dir
alerts_folder="%alerts_dir%/"
'generate url
if(Request.ServerVariables("HTTPS")="off") then
	http_str = "http://"
else
	http_str = "https://"
end if
folder = replace(Request.ServerVariables("SCRIPT_NAME"), "admin/", "")
pos = InStrRev(folder, "/")
folder = Left(folder, pos)
alerts_folder = http_str & Request.ServerVariables("HTTP_HOST") & folder
'------------

alerts_dir="%alerts_folder%\"

Dim alerts_timeout
alerts_timeout = "-1"

Dim DBConnectionString, host, dbname, user_name, password, win_auth
host="%host%"
dbname="%dbname%"
user_name="%user_name%"
password="%password%"
win_auth="%win_auth%"
azure_sql="%azure_sql%"

Dim Back_DBConnectionString, back_host, back_dbname, back_user_name, back_password, back_win_auth
db_use_backup=0
back_host=""
back_dbname=""
back_user_name=""
back_password=""
back_win_auth=""

DBConnectionString = GetConnectionString(host, dbname, user_name, password, win_auth,azure_sql)
Back_DBConnectionString = GetConnectionString(back_host, back_dbname, back_user_name, back_password, back_win_auth,azure_sql)

'-----------------------------------------------
'name of folder (note there is no / at end)
strFolder = alerts_dir & "admin\images\upload" 
'name of folder in http format (note there is no / at end)
httpRef = alerts_folder & "admin/images/upload"
'the max size of file which can be uploaded, 0 will give unlimited file size
lngFileSize = 0
'the files to be excluded (must be in format ".aaa;.bbb")
'and must be set to blank ("") if none are to be excluded
strExcludes = ".htm;.html;.php;.asp;.dll;.exe;.js;.cs;.ashx;.aspx;.ascx;.asmx;.asax;.master;.config;.inc"
'the files to be included (must be in format ".aaa;.bbb")
'and must be set to blank ("") if none are to be excluded
strIncludes = ""
'-----------------------------------------------
Const top_template_start_separator = "<!-- top_template_start -->"
Const top_template_end_separator = "<!-- top_template_end -->"
Const bottom_template_start_separator = "<!-- bottom_template2_start -->"
Const bottom_template_end_separator = "<!-- bottom_template2_end -->"
'-----------------------------------------------

Dim date_format(4)

'date format
date_format(1)="d"
date_format(2)="m"
date_format(3)="y"

language="British"

ConfEnableRSSLogs=0
ConfEnableSMSLogs=0
ConfDeskalertsMode=1
ConfEndDateByDefault=0
ConfOnlinePeriod=2
ConfEnableKeyCheck=0
ConfEnableOldOUSelect=0
ConfHideBuyAddonsLink=0
ConfDisableAlertSizing=0

ConfEnableCustomGroups=1
ConfEnableIPRanges=1

ConfEnableAlertLangs=0
ConfCustomSkinEnabled=0

ConfEnableIISAuth=0
ConfForceIISAuth=0

ConfEnableSyncLog=0

ConfEnableLoadBalancing=0
ConfEnableUserParameters=0
ConfProtectedParameters=""

ConfDisableBroadcast=0
ConfRecipientsTypeSelect=1
ConfEnableBroadcastTroughSelAllOrg=0

ConfProhibitUnlimitedLifetime=0

ConfForceSelfRegWithAD=0

ConfEnableGroupsImport=0

ConfShowGroupsByDefault=0
ConfHideOrganizationTree=0
ConfTGroupEnabled = 0
ConfIFRMode = 0
ConfEGSModerator = 0
ConfDisableGroupsSync = 0

Dim ConfSuperAccounts
ConfSuperAccounts = Array("admin")
DA=0

canApprove = 0

templateFolder = "admin\templates"

Response.CharSet = "utf-8"
Response.CodePage = 65001
if (ConfSessionTimeout) then
	if ConfSessionTimeout > 1440 then
		ConfSessionTimeout = 1440
	end if
	Session.Timeout = Clng(ConfSessionTimeout)
end if

now_db=Now()
timezone_db=""

%>
<!-- #include file="load_config.asp" -->
