﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="settings_edit_profile.aspx.cs" Inherits="DeskalertsDotNet.Pages.settings_edit_profile" %>
<%@ Import NameSpace="DeskAlertsDotNet.Parameters" %>
<%@ Import Namespace="Resources" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>DeskAlerts</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link href="css/style9.css" rel="stylesheet" type="text/css">
	<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
	<script language="javascript" type="text/javascript">

	    $(document).ready(function () {
	        $("#add_user_button").button();
	        $("#add_user_button").bind("click", function () {
	            if (!$("#user_name").val()) {
	                alert("User name must not be empty!");
	            }
	            else {
	                $('#user_form').submit();
	            }
	        });

	        $("#cancel_change_password_button").button();
	        $("#cancel_change_password_button").bind("click", function () {
	            $('#change_password_tr').hide(100);
	            $('#password_changed').val("0");
	        });;

	        $("#change_password_button").button();
	        $("#change_password_button").bind("click", function () {
	            $('#change_password_tr').show(1000);
	            $('#password_changed').val("1");
	        });
			//if DEMO=1 then
		    if ($("#user_name").val() == "demo") {
		        $("#defaultUserWarning").show();
		        $("#user_name").prop('disabled', true);
		        $("#change_password_button").hide();
		        $("input[name='email']").prop('disabled', true);
		        $("input[name='mobile_phone']").prop('disabled', true);
		        $("#userGuideSelect").prop('disabled', true);
		        $("#userLangSelect").prop('disabled', true);
		        $("#start_page").prop('disabled', true);
		        $("#add_user_button").hide();
		    }
		  
		});
	</script>
	<script>


	    function fill_select(start_page) {
	        var menu_item = window.top.frames["menu"].document.getElementsByName('menu_item');

	        for (var i = 0; i < menu_item.length; i++) {

	            var menu_subitem = window.top.frames["menu"].document.getElementsByName('menu_subitem_' + menu_item.item(i).innerHTML);

	            if (menu_subitem.length > 0) {
	                for (j = 0; j < menu_subitem.length; j++) {
	                    var x = document.getElementById("start_page");
	                    var option = document.createElement("option");
	                    option.text = menu_item.item(i).innerHTML + "." + menu_subitem.item(j).innerHTML;

	                    menu_subitem_length = menu_subitem.item(j).href.length;
	                    menu_subitem_pos = menu_subitem.item(j).href.indexOf('/admin/') + 7;
	                    href = menu_subitem.item(j).href.substr(menu_subitem_pos, menu_subitem_length);
	                    option.value = href;

	                    if (start_page == href) {
	                        option.selected = true;
	                    }
	                    x.add(option);
	                }
	            }
	            else {
	                var x = document.getElementById("start_page");
	                var option = document.createElement("option");
	                option.text = menu_item.item(i).innerHTML;
	                parent_element = menu_item.item(i).parentNode.href;
	                menu_parentelement_length = parent_element.length;
	                menu_parentelement_pos = parent_element.indexOf('/admin/') + 7;
	                href = parent_element.substr(menu_parentelement_pos, menu_parentelement_length);

	                option.value = href;

	                if (start_page == href) {
	                    option.selected = true;
	                }
	                x.add(option);
	            }
	        }
	    }
	</script>
	<style>
		.data_table tr td {border:0px none; padding-left: 5px; padding-right: 5px}
	</style>
</head>
<body style="margin:0px" class="body" onLoad="fill_select('<%=userStartPage%>');">
<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	<tr>
	<td>
	<table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
		<tr>
			<td  height=31 class="main_table_title">
				<img src="images/menu_icons/settings_20.png" alt="" width="20" height="20" border="0" style="padding-left:7px">
					<a href="#" class="header_title" style="position:absolute;top:15px"></a>
			</td>
		</tr>
		<tr>
		<td style="padding:5px; width:100%" height="100%"   valign="top" class="main_table_body">
			<div style="margin:10px;">
				<form name="my_form" method="post" id="user_form">
					<font color="red"></font>
					<p id="defaultUserWarning" style="display:none">
						You are logged in with a shared "demo" account. To be able to customize profile settings, create your own account from the Publishers menu.
					</p>
					<table style="padding:0px; margin-top:5px; margin-bottom:5px; border-collapse:collapse; border:0px">
						<tr>
							<td>
								<label for="name"></label>
							</td>
						</tr>
						<tr>
							<td>
								<input id="user_name" type="text" value="" name="name"></input>
							</td>
						</tr>
						<tr>
							<td style="padding-top:10px;">
								<input type="hidden" id="password_changed" name="password_changed"/>
								<a id="change_password_button"><%=resources.LNG_CHANGE_PASSWORD%></a>
							</td>
						</tr>
						<tr id="change_password_tr" style="display:none;">
							<td style="padding-top:10px">
								<table cellspacing="0" cellpadding="0" class="data_table">
									<tr id="password_old_label_tr">
										<td style="padding-top:10px;">
											<label for="old_password"></label>
										</td>
									</tr>
									<tr id="password_old_value_tr">
										<td>
											<input type="password" value="" name="old_password"></input>
										</td>
									</tr>
									<tr id="password_label_tr">
										<td style="padding-top:10px;">
											<label for="password"></label>
										</td>
									</tr>
									<tr id="password_value_tr">
										<td>
											<input type="password" value="" name="password"></input>
										</td>
									</tr>
									<tr id="password_confirm_label_tr">
										<td style="padding-top:10px;">
											<label for="confirm_password"></label>
										</td>
									</tr>
									<tr id="password_confirm_value_tr">
										<td>
											<input type="password" value="" name="confirm_password"></input>
										</td>
									</tr>
									<tr id="password_change_cancel_tr">
										<td style="padding-top:5px; padding-bottom:5px; text-align:right">
											<a id="cancel_change_password_button"></a>
										</td>
									</tr>	
								</table>
							</td>
						</tr>
								
						<tr>
							<td style="padding-top:10px;">
								<label for="email">:</label>
							</td>
						</tr>
						<tr>
							<td>
								<input type="text" value="" name="email"></input>
							</td>
						</tr>
						<tr>
							<td style="padding-top:10px;">
								<label for="mobile_phone">:</label>
							</td>
						<tr>
						<tr>
							<td>
								<input type="text" value="" name="mobile_phone"></input>
							</td>
						</tr>
						<tr>
							<td style="padding-top:10px;">
								<label for="userLang">:</label>
							</td>
						<tr>
						<tr>
							<td>
								<select style='width:175px;' id="userGuideSelect" name="guide">
									<option value="none" >
									
									</option>
									<option value="new" >
									
									</option>
									<option value="all" >
									
									</option>
								</select>
							</td>
						</tr>
						<tr>
							<td style="padding-top:10px;">
								<label for="userLang">:</label>
							</td>
						<tr>
						<tr>
							<td>
								<select style='width:175px;' id="userLangSelect" name="userLang">
									
									<option value="" >
										</option>
									
								</select>
							</td>
							<td><span style="color:#aaaaaa;padding-left:10px">*.</span></td>
						</tr>
						<!--Start-->
						<tr>
							<td style='padding-top:10px;'>
								<label for="start_page"></label>
							</td>
						</tr>
						<tr>
							<td>
								<select style='width:175px;' id='start_page' name='start_page'>
									<option value='dashboard.asp' selected>Choose start page</option>
								</select>
							</td>
						</tr>
						<!--End-->
						<tr>
							<td style="padding-top:10px;" align="right">
								<a id='add_user_button'></a>
							</td>
						</tr>
					</table>
					
					<input type="hidden" name="id" value=""></input>
					<input type="hidden" name="save" value="1"></input>
				</form>
			</div>
		</td>
	</tr>
</table>
</body>
</html>
