﻿// ReSharper disable once StyleCop.SA1300
namespace DeskAlertsDotNet.sever.STANDARD.admin.LogClass
{
    using DeskAlertsDotNet.DataBase;

    using NLog;

    public class PolicyLog
    {
        private readonly Logger logger = Pages.DeskAlertsBasePage.Logger;

        public bool CreateNewPolicy { get; set; }

        public string EditPolicy { get; set; }

        public string PolicyNameFromRequest { get; set; }

        public string PolicyNameFromDb { get; set; }

        public string UserName { get; set; }

        public string PolicyName { get; set; }

        public string QueryPolicyType { get; set; }

        public string PolicyId { get; set; }

        public void LogPolicyTypeChange()
        {
            using (DBManager dbMgr = new DBManager())
            {
                string policyId = this.PolicyId;

                if (this.EditPolicy == "1" && !this.CreateNewPolicy)
                {
                    if (this.PolicyNameFromRequest != this.PolicyNameFromDb)
                    {
                        this.logger.Info(
                            $"User \"{this.UserName}\" has changed the policy with name \"{this.PolicyName}\" to name: \"{this.PolicyNameFromRequest}\".");
                    }

                    string dbPolicyType =
                        dbMgr.GetScalarByQuery<string>($"SELECT type FROM policy WHERE id={policyId}");

                    if (this.QueryPolicyType != dbPolicyType)
                    {
                        this.ChoseTypePolicy(this.QueryPolicyType, this.PolicyNameFromRequest);
                    }
                }
                else
                {
                    this.logger.Info($"User \"{this.UserName}\" has created new policy with name \"{this.PolicyNameFromRequest}\"");

                    this.ChoseTypePolicy(this.QueryPolicyType, this.PolicyNameFromRequest);
                }
            }
        }

        /// <summary>
        /// Log the type of policy.
        /// </summary>
        /// <param name="policyType">Type policy.</param>
        /// <param name="policyNameFromRequest">Name policy from request</param>
        private void ChoseTypePolicy(string policyType, string policyNameFromRequest)
        {
            if (string.IsNullOrEmpty(policyType))
            {
                this.logger.Info("Policy role not detected");
                return;
            }

            string policyRole = string.Empty;

            switch (policyType)
            {
                case "A":
                    policyRole = "\"System administrator\"";
                    break;

                case "E":
                    policyRole = "\"Publisher\"";
                    break;
            }

            this.logger.Info(
                $"User \"{this.UserName}\" chose type {policyRole} for policy with name: \"{policyNameFromRequest}\"");
        }
    }
}