using System;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Castle.MicroKernel.Lifestyle;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.Server.Utils.Interfaces.Services;
using DeskAlertsDotNet.Application;
using DeskAlertsDotNet.Models.Permission;
using DeskAlertsDotNet.Parameters;
using DeskAlerts.Server.Utils;
using DeskAlerts.Server.Utils.Helpers;
using Resources;

namespace DeskAlertsDotNet.Pages
{
    public partial class InstancesOfTemplate : DeskAlertsBaseListPage
    {
        private readonly ICacheInvalidationService _cacheInvalidationService;

        public InstancesOfTemplate()
        {
            using (Global.Container.BeginScope())
            {
                _cacheInvalidationService = Global.Container.Resolve<ICacheInvalidationService>();
            }
        }

        protected override HtmlInputText SearchControl => searchTermBox;

        protected override bool IsUsedDefaultTableSettings => false;

        private const int DefaultMargin = 10;

        protected override void OnLoad(EventArgs e)
        {
            try
            {
                base.OnLoad(e);
 
                headerTitle.Text = resources.LNG_ALERTS;
                titleCell.Text = GetSortLink(resources.LNG_TITLE, "title", Request["sortBy"]);
                creationDateCell.Text = GetSortLink(resources.LNG_CREATION_DATE, "create_date", Request["sortBy"]);

                Table = contentTable;
                workHeader.InnerText = resources.LNG_SENT_ALERTS;
                senderCell.Text = resources.LNG_SENT_BY;
                actionsCell.Text = resources.LNG_ACTIONS;
                actionsCell.HorizontalAlign = HorizontalAlign.Center;

                headerSelectAll.Visible = curUser.HasPrivilege || curUser.Policy[Policies.ALERTS].CanDelete;
                selectAll.Visible = curUser.HasPrivilege || curUser.Policy[Policies.ALERTS].CanDelete;
                bottomDelete.Visible = curUser.HasPrivilege || curUser.Policy[Policies.ALERTS].CanDelete;
                upDelete.Visible = curUser.HasPrivilege || curUser.Policy[Policies.ALERTS].CanDelete;

                searchButton.Text = searchButton.Text.Replace("{Search}", resources.LNG_SEARCH);
                var getUrlSearchTermBox = Request.QueryString["searchTermBox"] ?? string.Empty;
                var isSearchTermBoxValueChanged = searchTermBox.Value != getUrlSearchTermBox;
                if (isSearchTermBoxValueChanged)
                {
                    Offset = 0;
                }

                var cnt = Content.Count < Offset + Limit ? Content.Count : Offset + Limit;
                for (var i = Offset; i < cnt; i++)
                {
                    var row = Content.GetRow(i);

                    var id = row.GetString("id");
                    var isRsvp = !Content.IsNull(i, "is_rsvp");
                    var isStopped = row.GetInt("schedule_type") == 1;
                    var senderName = row.GetString("name");

                    var tableRow = new TableRow();

                    if (curUser.HasPrivilege || curUser.Policy[Policies.ALERTS].CanDelete)
                    {
                        var chb = GetCheckBoxForDelete(row);
                        var checkBoxCell = new TableCell
                        {
                            HorizontalAlign = HorizontalAlign.Center
                        };
                        checkBoxCell.Controls.Add(chb);

                        tableRow.Cells.Add(checkBoxCell);
                    }

                    // Fill "title" cell
                    tableRow.Cells.Add(new TableCell
                    {
                        Text = HttpUtility.HtmlDecode(DeskAlertsUtils.ExtractTextFromHtml(row.GetString("title")))
                    });

                    // Fill "create_date" cell
                    tableRow.Cells.Add(new TableCell
                    {
                        Text = DateTimeConverter.ToUiDateTime(row.GetDateTime("create_date")),
                        HorizontalAlign = HorizontalAlign.Center
                    });

                    // Fill "sender" cell
                    tableRow.Cells.Add(new TableCell
                    {
                        Text = string.IsNullOrEmpty(senderName) ? resources.SENDER_WAS_DELETED : senderName,
                        HorizontalAlign = HorizontalAlign.Center
                    });

                    // Fill "actions" cell
                    var actionsContentCell = new TableCell
                    {
                        CssClass = "actionsButtonsTd",
                        HorizontalAlign = HorizontalAlign.Center //TODO: Check this position
                    };

                    if (curUser.HasPrivilege || curUser.Policy[Policies.ALERTS].CanStop)
                    {
                        var changeScheduleType = GetActionButton(id,
                            isStopped ? "images/action_icons/stop.png" : "images/action_icons/start.png",
                            isStopped ? "LNG_STOP_SCHEDULED_ALERT" : "LNG_START_SCHEDULED_ALERT",
                            delegate
                            {
                                var redirectResult = $"ChangeScheduleType.aspx?id={id}&type={(isStopped ? 0 : 1)}&{PageParams}&return_page=InstancesOfTemplate.aspx";
                                Response.Redirect(redirectResult, true);
                            },
                            DefaultMargin
                        );

                        actionsContentCell.Controls.Add(changeScheduleType);
                    }

                    if (!curUser.IsPublisher || curUser.Policy != null && curUser.Policy[Policies.STATISTICS].CanView)
                    {
                        LinkButton detailsButton;

                        var isStatisticEnabled = Config.Data.HasModule(Modules.STATISTICS);
                        if (isStatisticEnabled && isRsvp)
                        {
                            detailsButton = SetRsvpDetailsButton(id);
                        }
                        else if (isRsvp)
                        {

                            var surveysId = dbMgr.GetScalarByQuery<string>($"select id from surveys_main  where sender_id='{id}'");
                            detailsButton = GetActionButton(
                                surveysId,
                                "images/action_icons/graph.png",
                                "LNG_ALERT_DETAILS",
                                $"window.open('SurveyStatisticDetails.aspx?id={surveysId}', '', 'status=0, toolbar=0, width=555, height=850, scrollbars=1')",
                                DefaultMargin);
                        }
                        else
                        {
                            detailsButton = GetActionButton(
                                id,
                                "images/action_icons/graph.png",
                                "LNG_ALERT_DETAILS",
                                $"window.open('AlertStatisticDetails.aspx?id={id}', '', 'status=0, toolbar=0, width=555, height=850, scrollbars=1')",
                                DefaultMargin);
                        }

                        actionsContentCell.Controls.Add(detailsButton);
                    }

                    tableRow.Cells.Add(actionsContentCell);
                    contentTable.Rows.Add(tableRow);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                Logger.Debug(ex);
                throw;
            }
        }

        private LinkButton SetRsvpDetailsButton(string alertId)
        {
            try
            {
                var surveyId = dbMgr.GetScalarByQuery<int>("SELECT id FROM surveys_main WHERE sender_id = " + alertId);

                var result = GetActionButton(alertId, "images/action_icons/graph.png",
                    "LNG_STATISTICS_REPORTS",
                    "window.open('WidgetSurveysDetails.aspx?id=" + surveyId +
                    "&sendAlertId=" + alertId + "','', 'status=0, toolbar=0, width=555, height=450, scrollbars=1')", DefaultMargin);

                return result;
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                Logger.Debug(ex);
                throw;
            }
        }

        protected override void OnDeleteRows(string[] ids)
        {
            foreach (var id in ids)
            {
                _cacheInvalidationService.CacheInvalidation(AlertType.SimpleAlert, Convert.ToInt64(id));
            }
        }

        /// <summary>
        /// Define parameters to add to Uri.
        /// Override <see cref="DeskAlertsBaseListPage"/> PageParams. Add "user_id" param for sorting table.
        /// </summary>
        protected override string PageParams
        {
            get
            {
                try
                {
                    var uri = string.Empty;
                    uri += AddParameter(uri, "templateId");
                    return uri;
                }
                catch (Exception ex)
                {
                    Logger.Error(ex.Message);
                    Logger.Debug(ex);
                    throw;
                }
            }
        }

        #region DataProperties

        private const string BaseContentQuery = @"
            SELECT a.id, 
                schedule_type, 
                a.sender_id, 
                title, 
                a.type, 
                a.sent_date, 
                a.create_date, 
                s.id as is_rsvp, 
                u.name 
            FROM alerts a 
            LEFT JOIN surveys_main s ON s.closed = 'A' AND s.sender_id = a.id 
            LEFT JOIN users u on u.id = a.sender_id 
            WHERE a.type='S' 
                AND (recurrence = 1 AND is_repeatable_template = 0) 
                AND (class = 1 OR class = 3 OR class = 6 OR class = 32) 
                AND a.id NOT IN (SELECT alert_id FROM instant_messages)"; //TODO: Transfer class condition to query parameters

        protected override string DataTable => "alerts";

        protected override string[] Collumns => new string[] { };

        protected override string DefaultOrderCollumn => "from_date";

        protected override string CustomQuery
        {
            get
            {
                try
                {
                    var query = BaseContentQuery;

                    var parentTemplateId = Request["templateId"];
                    query += $" AND a.parent_template_id = {(string.IsNullOrEmpty(parentTemplateId)? (-1).ToString(): parentTemplateId)}";

                    if (!curUser.HasPrivilege && !curUser.Policy.CanViewAll)
                    {
                        var viewListStr = string.Join(",", curUser.Policy.SendersViewList.ToArray());
                        query += $" AND a.sender_id IN ({viewListStr})";
                    }

                    if (SearchTerm.Length > 0)
                    {
                        query += $" AND title LIKE \'%{SearchTerm}%\'";
                    }

                    if (!string.IsNullOrEmpty(Request["sortBy"]))
                    {
                        query += $" ORDER BY a.{Request["sortBy"]}";
                    }
                    else
                    {
                        query += $" ORDER BY a.{DefaultOrderCollumn} DESC ";
                    }

                    return query;
                }
                catch (Exception ex)
                {
                    Logger.Error(ex.Message);
                    Logger.Debug(ex);
                    throw;
                }
            }
        }

        protected override string CustomCountQuery
        {
            get
            {
                try
                {
                    var query = BaseContentQuery;

                    var parentTemplateId = Request["templateId"];
                    query += $" AND a.parent_template_id = {(string.IsNullOrEmpty(parentTemplateId) ? (-1).ToString() : parentTemplateId)}";

                    if (!curUser.HasPrivilege && !curUser.Policy.CanViewAll)
                    {
                        var viewListStr = string.Join(",", curUser.Policy.SendersViewList.ToArray());
                        query += $" AND a.sender_id IN ({viewListStr})";
                    }

                    if (SearchTerm.Length > 0)
                    {
                        query += $" AND title LIKE \'%{SearchTerm}%\'";
                    }

                    query = $"SELECT count(1) FROM ({query}) AS content";

                    return query;
                }
                catch (Exception ex)
                {
                    Logger.Error(ex.Message);
                    Logger.Debug(ex);
                    throw;
                }
            }
        }

        #endregion

        #region Interface properties

        protected override IButtonControl[] DeleteButtons => new IButtonControl[] { upDelete, bottomDelete };

        protected override HtmlGenericControl NoRowsDiv => NoRows;

        protected override HtmlGenericControl TableDiv => mainTableDiv;

        protected override string ContentName => resources.LNG_SENT_ALERTS;

        protected override HtmlGenericControl BottomPaging => bottomRanging;

        protected override HtmlGenericControl TopPaging => topPaging;

        #endregion
    }
}