﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="DeskAlertsDotNet.Pages.Index" %>

<%@ Import Namespace="DeskAlertsDotNet.Parameters" %>
<%@ Import Namespace="Resources" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>DeskAlerts</title>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="css/style9.css?v=9.2.3.27" rel="stylesheet" type="text/css" />
    <link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <script type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
    <script>

        window.onresize = function (e) {
            onChangeBodyHeight(height, true);
        }

        showLoaderPanel();

        function scrollTo(x) {
            $('html, body').animate({
                scrollTop: x
            }, 10);
        }

        // Every 500 ms move TinyMCE plugins popups on top in frame.
        function setupTinyMceModalsMoversCallbacks() {
            var delay = 500;

            setInterval(function () {
                moveTinyMceModalsToTop(".mce-window");
                moveTinyMceModalsToTop(".moxman-window");
                moveTinyMceModalsToTop(".ui-dialog.ui-widget.ui-widget-content");
            },
                delay);
        }

        function moveTinyMceModalsToTop(modalName) {
            var mceModalPopup = $(modalName, $("#main_frame", $("#main").contents()).contents());

            if (mceModalPopup.length === 1) {
                mceModalPopup.forEach(moveTinyMceModalElementToTop(mceModalPopup));
            } else {
                mceModalPopup.each(function (index, item) {
                    moveTinyMceModalElementToTop(item);
                });
            }
        }

        function moveTinyMceModalElementToTop(element) {
            var marginTop = 40;

            var elementDisplay = $(element).css("display");
            var elementTop = parseInt($(element).css("top"), 10);

            if (elementDisplay === "block" && elementTop > marginTop) {
                $(element).css("top", marginTop);
            }
        }

        if (window.top !== window.self) {
            window.top.location = window.self.location;
        }

        window.onload = function () {
            document.getElementById('main').contentWindow.document.onreadystatechange = hideLoaderPanel();
            document.getElementById('top').contentWindow.document.onreadystatechange = hideLoaderPanel();
            document.getElementById('menu').contentWindow.document.onreadystatechange = hideLoaderPanel();
        };

        $(document).ready(function () {


            $.post($('#sharepoint').attr('action'), $('#sharepoint').serialize())
                .done(function (data, textStatus, jqXHR) {
                    $('#results').html(data);
                })
                .fail(function (jqXHR, textStatus, errorThrown) {
                    $('#results').html("<h3>Request has been failed. Try again</h3>");
                });

            var mainMenuIsVisible = localStorage.getItem("main_menu_is_visible");

            if (mainMenuIsVisible == null) {
                mainMenuIsVisible = "true";
                localStorage.setItem("main_menu_is_visible", mainMenuIsVisible);
            }
            var width = $(window).width();
            var menuPanel = $("#menu_panel");
            var bodyPanel = $("#body_panel");
            if (mainMenuIsVisible == "true") {
                menuPanel.show();
                width = $(window.document).width();
                bodyPanel.width(width - 260);
            } else {
                menuPanel.hide();
                bodyPanel.width(width - 20);
            }

            $("#sso_dialog").dialog({
                autoOpen: false
            });
        });

        function logoutClick() {
            if ("<%=_user.IsSsoAuthorized%>" == "True") {
                $("#sso_dialog").dialog("open");
            }
            else {
                window.location.href = "Logout.aspx";
            }
        }

        function toggleMainMenu() {
            var menuPanel = $("#menu_panel");
            var menuPanelIsVisible = menuPanel.is(":visible");
            var bodyPanel = $("#body_panel");
            var width = menuPanelIsVisible ? $(window.document).width() - 20 : $(window).width() - 260;
            localStorage.setItem("main_menu_is_visible", menuPanelIsVisible ? "false" : "true");
            var slidingTime = 500;
            bodyPanel.animate({
                width: width + 'px'
            }, slidingTime);
            menuPanel.toggle(slidingTime);
        }
        var firstCall = true;

        function onChangeBodyHeight(height, isFromMain) {
            firstCall = !isFromMain;

            var windowSizeHeight = document.documentElement.clientHeight - document.getElementsByClassName("top_menu_height")[0].clientHeight;

            $("#mainContent, #main, #main_frame, #body_panel").height(document.documentElement.clientHeight);
            $(".max_height_menu").height(document.documentElement.clientHeight - 4);
            $(".max_height_body").height(windowSizeHeight - 4);
            //if (firstCall) {
            //    $("#mainContent, #main, #main_frame, #body_panel, .max_height_body").height(height - 62);
            //}
        }
        function onChangeMenuHeight(height) {
            $("#mainContent, #menu_panel, #menu").height(height);
        }
        $(window).resize(function () {
            if ($("#menu_panel").is(":visible") == "true") {
                $("#body_panel").width(window.innerWidth - 20);
            } else {
                $("#body_panel").width(window.innerWidth - 260);
            }
        });

        function hideLoaderPanel() {
            if (document.getElementById('main').contentWindow.document.readyState === 'complete' &&
                document.getElementById('top').contentWindow.document.readyState === 'complete' &&
                document.getElementById('menu').contentWindow.document.readyState === 'complete') {
                $('.dashboard_preloader').hide();
            }
        }

        function showLoaderPanel() {
            $('.dashboard_preloader').show();
        }



    </script>
</head>
<body class="no_margins" id="mainContent">
    <div class="menu_frame main_menu_width max_height_menu block" id="menu_panel">
        <iframe src="Menu.aspx" class="no_borders main_menu_width max_height_menu" id="menu" name="menu"></iframe>
    </div>
    <div class="block full_width" id="body_panel">
        <div class="top_menu_height">
            <iframe class="no_borders full_width" src="top.aspx" id="top"></iframe>
        </div>
        <div class="max_height_body">
            <iframe class="no_borders full_width max_height_body" src="main.aspx?url=<% = userStartPage %>" id="main" name="main"></iframe>
        </div>
        <div id="sso_dialog" title="Logout with SSO">
            <%=resources.LNG_TO_LOGOUT_PLEASE_CLOSE_YOUR_BROWSER %>
        </div>
    </div>
    <div class="dashboard_preloader">
        <div class="dashboard_preloader_sign"></div>
    </div>
</body>
</html>
