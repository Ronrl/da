﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%

check_session()

uid = Session("uid")
if uid <>"" then
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>DeskAlerts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/style9.css" rel="stylesheet" type="text/css">
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script type="text/JavaScript" language="JavaScript" src="jscripts/json2.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/preview.js?v=9.2.1.31"></script>
<script language="javascript" type="text/javascript" src="functions.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/date.js"></script>
</head>
<script language="javascript" type="text/javascript">

	var settingsDateFormat = "<%=GetDateFormat()%>";
	var settingsTimeFormat = "<%=GetTimeFormat()%>";

	var responseDbFormat = "dd/MM/yyyy HH:mm:ss";
	var uiFormat = getUiDateTimeFormat(settingsDateFormat,settingsTimeFormat);

	
	$(function() {
		$(".delete_button").button({
	});

	$(".add_color_code_button").button({
		icons: {
			primary: "ui-icon-plusthick"
		}
	});

	$(document).ready(function() {
		var width = $(".data_table").width();
		var pwidth = $(".paginate").width();
		if (width != pwidth) {
			$(".paginate").width("100%");
		}
	});
})
</script>
<%

   if(Request("offset") <> "") then 
	offset = clng(Request("offset"))
   else 
	offset = 0
   end if	

   if(Request("sortby") <> "") then 
	sortby = Request("sortby")
   else 
	sortby = "name"
   end if	

'counting pages to display
   if(Request("limit") <> "") then 
	limit = clng(Request("limit"))
   else 
	limit=25
   end if

	'--- rights ---
	set rights = getRights(uid, true, false, true, "cc_val")
	gid = rights("gid")
	gviewall = rights("gviewall")
	gviewlist = rights("gviewlist")
	cc_arr = rights("cc_val")
	'--------------

	if(cc_arr(2)="") then
		linkclick_str = "not"
	else
		linkclick_str = LNG_COLOR_CODES
	end if

	SQL = "SELECT COUNT(1) as cnt FROM color_codes"
	Set RS = Conn.Execute(SQL)

	cnt=RS("cnt")

	RS.Close

  j=cnt/limit

%><body style="margin:0" class="body">
<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	<tr>
	<td>
	<table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
		<tr>
		<td width="100%" height="31" class="main_table_title"><img src="images/menu_icons/im_20.png" alt="" width="20" height="20" border="0" style="padding-left:7px">
			<a href="color_codes.asp" class="header_title" style="position:absolute;top:15px"><%=LNG_INSTANT_MESSAGES %></a></td>
		</tr>
		<tr>
		<td height="100%" valign="top" class="main_table_body">
		<div style="margin:10px">
		<span class="work_header"><%=LNG_COLOR_CODES %></span>

		<table width="100%"><tr><td align="right">
		<% if(gid = 0 OR (gid = 1 AND cc_arr(0)<>"")) then %>
		<a class="add_color_code_button" href="color_codes_add.asp"><%=LNG_ADD_COLOR_CODE%></a>
		<% end if%>
		</td></tr></table>

<%

if(cnt>0) then
	page="color_codes.asp?"
	name=LNG_COLOR_CODES 
	Response.Write make_pages(offset, cnt, limit, page, name, sortby) 

	response.write "<br><A href='#' class='delete_button' onclick=""javascript: return LINKCLICK('"&linkclick_str&"');"">"&LNG_DELETE&"</a><br/><br/>"
	response.write "<form action='delete_objects.asp' name='delete_objs' id='delete_objs' method='post'><input type='hidden' name='objType' value='color_codes'>"

%>
		<table width="100%" height="100%" cellspacing="0" cellpadding="3" class="data_table">
		<tr class="data_table_title">
		<td width="10"><input type='checkbox' name='select_all' id='select_all' onclick='javascript: select_all_objects();'></td>
		<td class="table_title"><%=sorting(LNG_COLOR_CODE,"name", sortby, offset, limit, page) %></td>
		<td class="table_title" width="50"><%=LNG_COLOR %></td>
		<td class="table_title" width="140"><%=LNG_ACTIONS %></td>
		</tr>
<%

'show main table

	SQL = "SELECT * FROM color_codes ORDER BY "& sortby
    Set RS = Conn.Execute(SQL)

	num=offset
	if(Not RS.EOF) then RS.Move(offset) end if
	Do While Not RS.EOF
		num=num+1
		if((offset+limit) < num) then Exit Do end if
		if(num > offset AND offset+limit >= num) then
		strObjectID=RS("id")
		%>
		<tr>
		<td><input type="checkbox" name="objects" value="<%=strObjectID%>"></td>
		<td>
		<% 
			Response.Write HtmlEncode(RS("name"))
		%>
		</td>
		<td style="background:#<%=RS("color")%>">
		</td>
		<td align="left" nowrap="nowrap">
		
	<% if(gid = 0 OR (gid = 1 AND cc_arr(0)<>"") AND cc_arr(1)="checked") then %>
		<a href="color_codes_add.asp?id=<%= RS("id") %>"><img src="images/action_icons/edit.png" alt="<%=LNG_EDIT_COLOR_CODE %>" title="<%=LNG_EDIT_COLOR_CODE %>" width="16" height="16" border="0" hspace=5></a>
	<% end if%>
		</td>
	</tr>
		<%
		end if
	RS.MoveNext
	Loop
	RS.Close


%>

			</table>
<%
	response.write "<input type='hidden' name='back' value='color_codes.asp'></form><br/><A href='#' class='delete_button' onclick=""javascript: return LINKCLICK('"&linkclick_str&"');"">"&LNG_DELETE&"</a><br><br>"

	Response.Write make_pages(offset, cnt, limit, page, name, sortby) 

else

Response.Write "<center><b>"&LNG_THERE_ARE_NO_COLOR_CODES&"</b><br><br></center>"

end if
%>

		 <img src="images/inv.gif" alt="" width="1" height="100%" border="0">
		</div>
		</td>
		</tr>
	</table>
	</td>
	</tr>
</table>
<%
else
	Response.Redirect "index.asp"

end if
%>

</body>
</html>
<!-- #include file="db_conn_close.asp" -->