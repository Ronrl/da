﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DeskAlerts.Server.Utils;
using DeskAlerts.Server.Utils.Helpers;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Parameters;

namespace DeskAlertsDotNet.Pages
{
    public partial class UserDetails : DeskAlertsBasePage
    {
        string userId;
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (string.IsNullOrEmpty(Request["id"]))
            {
                Response.End();
                return;
            }

            userId = Request["id"];

            string userDataQuery = "SELECT id, context_id, mobile_phone, email, name, next_request, display_name, domain_id, deskbar_id, reg_date, last_date, convert(varchar,last_request) as last_request1, last_request FROM users WHERE id=" + userId;

            DataSet userDataSet = dbMgr.GetDataByQuery(userDataQuery);

            nameCell.InnerText = userDataSet.GetString(0, "name");

            string domainName = "";
            if (!userDataSet.IsNull(0, "domain_id"))
                domainName = dbMgr.GetScalarByQuery("SELECT name FROM domains WHERE id=" + userDataSet.GetString(0, "domain_id")).ToString();

            domainCell.InnerText = domainName;
            emailPhoneCell.InnerText = userDataSet.GetString(0, "email");
            mobilePhoneCell.InnerText = userDataSet.GetString(0, "mobile_phone");

            string ouQuery = "SELECT OU_PATH FROM OU o INNER JOIN OU_User u ON o.Id = u.Id_OU WHERE u.Id_user = " + userId;

            object ouPath = dbMgr.GetScalarByQuery(ouQuery);

            ouCell.InnerText = ouPath == null ? "" : ouPath.ToString();

            string groupsQuery = "SELECT name FROM groups INNER JOIN users_groups ON groups.id=users_groups.group_id WHERE user_id=" + userId;

            DataSet groupSet = dbMgr.GetDataByQuery(groupsQuery);

            if (!groupSet.IsEmpty)
            {
                string[] groupsArr = groupSet.ToList().Select(row => row.GetString("name")).ToArray();

                groupsCell.InnerText = string.Join(",", groupsArr);
            }

            if (!Config.Data.HasModule(Modules.AD))
            {
                regDateCell.InnerText = userDataSet.GetString(0, "reg_date");
            }
            else
            {
                regDateRow.Visible = false;
            }

            DateTime lastRequest = userDataSet.GetDateTime(0, "last_request");

            statusImg.Width = 11;
            statusImg.Height = 11;
            if (lastRequest.CompareTo(DateTime.MinValue) != 0)
            {

                int dateDiff = (int)(DateTime.Now - lastRequest).TotalMinutes;
                string nextRequest = userDataSet.GetString(0, "next_request");
                int onlineCounter = 2;
                if (nextRequest.Length != 0)
                {
                    onlineCounter = (Convert.ToInt32(nextRequest) / 2) * 60;
                }

                if (onlineCounter < dateDiff)
                {
                    statusImg.Src = "images/offline.gif";
                }
                else
                {
                    statusImg.Src = "images/online.gif";
                }
            }
            else
            {
                statusImg.Src = "images/offline.gif";
            }

            lastActivityCell.InnerText = DeskAlertsUtils.IsNotEmptyDateTime(lastRequest) ? DateTimeConverter.ToUiDateTime(lastRequest) : string.Empty;
        }
    }
}