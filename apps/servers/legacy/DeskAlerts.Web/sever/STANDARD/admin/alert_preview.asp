<!-- #include file="config.inc" -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<% 
Response.Expires = 0 
%>
<html>
<head>
	<style>
		html, body {
			padding:0px;
			margin:0px;
			overflow:hidden;
			border:0px;
			width:100%;
			height:100%;
		}
	</style>
	<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
	
	<script language="javascript" type="text/javascript" src="jscripts/json2.js"></script>
	<script language="javascript" type="text/javascript" src="jscripts/preview.js?v=9.2.1.31"></script>
	<script language="javascript" type="text/javascript">
		var alertId = "<%=Request("id")%>";
		var data;
		var top_template;
		var bottom_template;
		
		window.onload = function()
		{
			if(!alertId)
			{
				var strData = "<%=jsEncode(Request("preview_data"))%>";
				if(!strData) return;
				data = JSON.parse(strData);
			}
			else
			{
				getAlertData(alertId,false,function(alertData){
					data = JSON.parse(alertData);
				});
			}
			data.in_window = true;

			var templateId = data.template_id;
			
			getTemplateHTML(templateId,"top",false,function(data){ 
				top_template = data;
			});
			
			getTemplateHTML(templateId,"bottom",false,function(data){ 
				bottom_template = data;
			});
	
			data.top_template = top_template;
			data.bottom_template = bottom_template;

			initAlertPreview(data, $("#preview_frame"));
		};
	</script>
</head>
<body >
<iframe id="preview_frame" name="preview_frame" frameborder="0"></iframe>
</body>
</html>