﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%

check_session()

  uid = Session("uid")


if (uid <> "") then

retPage = Request("return_page")

'update group name in database

if(Request("sub1") = "Group") then
	name=Request("name")
	mytype=Request("special")
	domain_id=Request("domain_id")

	Set RS = Conn.Execute("SELECT id FROM users WHERE id="&uid&" AND role='E'")
	if(Not RS.EOF) then
		gid = 1
		Set rs_policy = Conn.Execute("SELECT type FROM policy INNER JOIN policy_editor ON policy.id=policy_editor.policy_id WHERE editor_id=" & uid)
		if(Not rs_policy.EOF) then
			if(rs_policy("type")="A") then
				gid = 0
			end if
		end if
		if(gid=1) then
			editor_id=RS("id")
			policy_ids=""
			policy_ids=editorGetPolicyIds(editor_id)
			policy_ids = replace(policy_ids, "policy_id=", "")
		end if
	else
		gid = 0
	end if
	RS.Close
		
	
	if(domain_id="") then domain_id=1 end if

	if(mytype="1") then spec="S" else spec="O" end if
	myname = name
	'check for existing name
	fl_check = 0
	Set rs_check = Conn.Execute("select id from groups WHERE domain_id = "& domain_id &" AND name='" & Replace(name, "'", "''") & "'")
	if(rs_check.eof) then
		fl_check = 1
	else
		if(clng(rs_check("id"))=clng(request("id"))) then
			fl_check = 1
		end if
	end if

	if(fl_check = 1) then
		if(Request("edit")=1) then
			SQL = "UPDATE groups SET name = N'" & Replace(name, "'", "''") & "', type='" & spec  & "', domain_id = "& domain_id &" WHERE id="+ Request("id")
			Conn.Execute(SQL)
			group_id=Request("id")
		else
			SQL = "INSERT INTO groups (name, type, custom_group, domain_id) VALUES (N'" & Replace(name, "'", "''") & "', '" & spec  & "', 1, "&domain_id&")"
			Conn.Execute(SQL)
			Set rs1 = Conn.Execute("select @@IDENTITY newID from groups")
			group_id=rs1("newID")
			rs1.Close
			if(gid=1) then
				SQL = "INSERT INTO policy_group (policy_id, group_id, was_checked) VALUES ("&policy_ids&", "&group_id&", 1)"
				Conn.Execute(SQL)
			end if
		end if
		
		Response.Redirect "group_users.asp?wh="&Request("wh")&"&id=" & CStr(group_id)  & "&domain_id="&domain_id&"&return_page="&Server.URLEncode(retPage)
	else
		error_descr = "Group name '"&name&"' already exists. Please enter another name."
	end if
end if

'show editing form

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>DeskAlerts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/style9.css" rel="stylesheet" type="text/css">
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script language="javascript" type="text/javascript">

$(document).ready(function() {
	$(".add_button").button();
	$(".save_button").button();
	$(".cancel_button").button();
});

</script>
<script language="javascript">
function my_sub()
{
	if(document.getElementById('g_name').value==""){
		alert("<%=LNG_PLEASE_ENTER_GROUP_NAME %>");
		return false;
	}
	else{
		return true;
	}
}
</script>
</head>

<%
val="Add"
id=Request("id")
if id <> "" then
	Set rs = Conn.Execute("SELECT name, type, domain_id FROM groups WHERE id="+CStr(id))
	myname=rs("name")
	if(Not IsNull(rs("type"))) then
		group_type=rs("type")
	end if
	rs.Close
	val="Save"
end if
%>
<% if Request("wh") = "1" then %>
<body style="margin:0">
<% else %>
<body style="margin:0" class="body">
<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	<tr>
	<td>
	<table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
		<tr>
		<td width=100% height=31 class="main_table_title"><a href="edit_group.asp" class="header_title"><%=LNG_GROUPS %></a></td>
		</tr>
		<tr>
		<td class="main_table_body" height="100%">
<% end if %>
		<div style="margin:10px;">
		<span class="work_header"><% if id = "" then Response.Write LNG_ADD_GROUP else Response.Write LNG_EDIT_GROUP end if %></span>
		<br><br>
<div><%=LNG_STEP %> <strong>1</strong> <%=LNG_OF %> 2: <%=LNG_ENTER_NAME %>:</div>

<form method="post" action="">
<input type="hidden" name="sub1" value="Group">
<font color="red"><%=error_descr %></font>
<table cellpadding=4 cellspacing=4 border=0>
<tr><td><%=LNG_GROUP_NAME %>:</td><td><input name="name" id="g_name" type="text" value="<% Response.Write myname %>"><input name="domain_id" type="hidden" value="<% Response.Write Request("domain_id") %>"></td></tr>
</table>

<div align="right"><button type="submit" class="cancel_button" onclick="javascript: history.go(-1);javascript: history.go(-1);return false;"><%=LNG_CANCEL %></button><button class="save_button" type="submit" onclick="return my_sub();"><%=LNG_SAVE_AND_NEXT %></button></div>
<%

if(id <> "")  then
	Response.Write "<input type='hidden' name='edit' value='1'/>"
	Response.Write "<input type='hidden' name='id' value='" + CStr(id) + "'/>"
end if
%>
<input type='hidden' name='return_page' value='<%=HtmlEncode(retPage) %>'/>
<input type='hidden' name='wh' value='<%=Request("wh") %>'/>
<br>
</form>
		 <img src="images/inv.gif" alt="" width="1" height="100%" border="0">
		</div>
<% if Request("wh") <> "1" then %>
		</td>
		</tr>
	</table>
	</td>
	</tr>
</table>
<% end if

else

  Response.Redirect "index.asp"

end if
%>
</body>
</html>
<!-- #include file="db_conn_close.asp" -->