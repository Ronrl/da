﻿using DeskAlertsDotNet.Parameters;
using System;
using System.Web.UI.WebControls;

namespace DeskAlertsDotNet.Pages
{
    using Resources;

    public partial class AddAlertLanguage : DeskAlertsBasePage
    {
        //use curUser to access to current user of DA control panel      
        //use dbMgr to access to database

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            acceptButton.Click += AcceptButton_Click;
            acceptButton.Text = resources.LNG_SAVE;
            //Add your main code here
            languageNameCell.Text = resources.LNG_LANGUAGE_NAME;
            languageCodeCell.Text = resources.LNG_LANGUAGE_CODE;

            if (Config.Data.IsDemo && curUser.Name != "admin")
            {
                acceptButton.Visible = false;
            }

            if (!string.IsNullOrEmpty(Request["id"]))
                LoadLanguage(Request["id"]);

            var languagesSet = dbMgr.GetDataByQuery("SELECT * FROM alerts_langs");

            foreach (var languageRow in languagesSet)
            {
                TableRow tableRow = new TableRow();

                TableCell languageName = new TableCell
                {
                    Text = languageRow.GetString("name"),
                    HorizontalAlign = HorizontalAlign.Center
                };

                tableRow.Cells.Add(languageName);

                TableCell languageCode = new TableCell
                {
                    Text = languageRow.GetString("abbreviatures"),
                    HorizontalAlign = HorizontalAlign.Center

                };

                tableRow.Cells.Add(languageCode);
                contentTable.Rows.Add(tableRow);
            }
        }

        private void AcceptButton_Click(object sender, EventArgs e)
        {
            string languageName = Request.GetParam("nameField", "");
            string languageCode = Request.GetParam("codeField", "");
            string languageId = Request.GetParam("languageId", "");

            if (string.IsNullOrEmpty(languageCode))
            {
                return;
            }

            if (string.IsNullOrEmpty(languageId))
            {
                dbMgr.ExecuteQuery($"INSERT INTO alerts_langs (name, abbreviatures, is_default) VALUES ({languageName.EscapeQuotes().Quotes()}, {languageCode.EscapeQuotes().Quotes()}, 0)");
            }
            else
            {
                dbMgr.ExecuteQuery($"UPDATE alerts_langs SET name = {languageName.EscapeQuotes().Quotes()} , abbreviatures = {languageCode.EscapeQuotes().Quotes()} WHERE id = {languageId} ");
            }

            Response.Redirect("AlertLanguagesSettings.aspx");
        }

        private void LoadLanguage(string id)
        {
            var languageSet = dbMgr.GetDataByQuery($"SELECT * FROM alerts_langs WHERE id = {id}");

            if (languageSet.IsEmpty)
                return;

            var languageRow = languageSet.First();

            nameField.Value = languageRow.GetString("name");
            codeField.Value = languageRow.GetString("abbreviatures");
            languageId.Value = languageRow.GetString("id");
        }
    }
}