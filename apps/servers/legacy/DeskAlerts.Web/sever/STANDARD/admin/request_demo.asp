﻿<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>DeskAlerts</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link href="css/style9.css" rel="stylesheet" type="text/css" />
	<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
	
	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
	<script language="javascript" type="text/javascript">
		var email = "sales@deskalerts.com";
		var subject = "Trial Request from DeskAlerts Demo";
		var disclaimer = "Company:\nName:\nPosition:\nNumber of users:\n\nYou can leave your additional comments.";
		var addons = "";
		$(document).ready(function() {
			$("#send_request").button();
			$("#send_request").click(function(e) {
				location.href = "mailto:" + email + "?subject=" + encodeURIComponent(subject) + "&body=" + encodeURIComponent(disclaimer);
			});
		});
	</script>
</head>
<body style="margin:0px" class="body">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
	<tr>
	<td>
	<table width="100%" cellspacing="0" cellpadding="0">
		<tr>
		<td class="main_table_body" height="100%">
			<div style="margin:10px;">
			<p>To get your DeskAlerts Trial package, you should submit a request to our sales department.</p>
			<p>The button below will open a mail agent with pre-composed message. Please fill in all required information:</p>
			<ul>
				<li>Your company name</li>
				<li>Your name and position</li>
				<li>How many users should receive alerts during trial period</li>
			</ul>
			<button id="send_request">Send Request</button>
			</div>
		</td>
		</tr>
	</table>
	</td>
	</tr>
</table>
</body>
</html>
