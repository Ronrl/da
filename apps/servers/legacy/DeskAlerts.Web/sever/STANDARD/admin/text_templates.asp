﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%

check_session()

uid = Session("uid")

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>DeskAlerts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/style9.css" rel="stylesheet" type="text/css">
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>

<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/preview.js?v=9.2.1.31"></script>
<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		var width = $(".data_table").width();
		var pwidth = $(".paginate").width();
		if (width != pwidth) {
			$(".paginate").width("100%");
		}
		$("#add_text_template_button").button({
			icons : {
				primary : "ui-icon-plusthick"
			}
		});
		$(".delete_button").button();
	});

	function showAlertPreview(templateId)
	{
		var data = new Object();
		
		data.create_date = "<%=al_create_date%>";
	
		var fullscreen = false;
		var ticker =  0;
		var acknowledgement =  0; 
		
		var alert_width = 500;
		var alert_height = 400;
		var alert_title = "Sample title";
		
		var text_template;
		
		getTemplateHTML(templateId,"text_template",false,function(data){ 
				text_template = data;
		});
		
		
		data.alert_width = alert_width;
		data.alert_height = alert_height;
		data.ticker = ticker;
		data.fullscreen = fullscreen;
		data.acknowledgement = acknowledgement;
		data.alert_title = alert_title;
		data.alert_html = text_template;

		initAlertPreview(data);
		
		return false;
	}



</script>



</head>
<script language="javascript" type="text/javascript" src="functions.js"></script>
<%
   if(Request("offset") <> "") then 
	offset = clng(Request("offset"))
   else 
	offset = 0
   end if	


if (uid <>"") then

'counting pages to display
   if(Request("limit") <> "") then 
	limit = clng(Request("limit"))
   else 
	limit=50
   end if

   if(Request("sortby") <> "") then 
	sortby = Request("sortby")
   else 
	sortby = "name"
   end if	


	admin = 0
	policy = 0
	only_templates=0

	linkclick_str = LNG_TEXT_TEMPLATES
	templates_arr = Array ("","","","","","","")
		
	Set RS = Conn.Execute("SELECT id FROM users WHERE id="&uid&" AND role='E'")
	if(Not RS.EOF) then
		gid = 1
		Set rs_policy = Conn.Execute("SELECT type, policy_id FROM policy INNER JOIN policy_editor ON policy.id=policy_editor.policy_id WHERE editor_id=" & uid)
		if(Not rs_policy.EOF) then
		    policy = rs_policy("policy_id")
			if(rs_policy("type")="A") then
				gid = 0
			end if
		end if
		if(gid=1) then
			editor_id=RS("id")
			policy_ids=""
			policy_ids=editorGetPolicyIds(editor_id)
			templates_arr = editorGetPolicyList(policy_ids, "text_templates_val")
			if(templates_arr(2)="") then
				linkclick_str = "not"
			end if
		end if
	else
		gid = 0
	end if
	RS.Close


'  limit=10

	Set RS = Conn.Execute("SELECT COUNT(id) as mycnt FROM text_templates")
	cnt=0

	if(Not RS.EOF) then
		cnt=RS("mycnt")
	end if

	RS.Close
  j=cnt/limit


%>
<body style="margin:0px" class="body">
<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	<tr>
	<td>
	<table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
		<tr>
		<td width=100% height=31 class="main_table_title"><img src="images/menu_icons/templates_20.png" alt="" width="20" height="20" border="0" style="padding-left:7px">
		<a href="text_templates.asp" class="header_title" style="position:absolute;top:15px"><%=LNG_TEXT_TEMPLATES %></a></td>
		</tr>
		<tr>
		<td class="main_table_body" height="100%">
		<div style="margin:10px;">
		<%if(gid = 0 OR (gid = 1 AND templates_arr(0)<>"")) AND Session("db_use_backup") <> 1 then%>
		<div align="right"><a href="text_template_edit.asp" id="add_text_template_button"><%=LNG_ADD_TEMPLATE %></a></div><br>
		<%end if%>


<%
if(cnt>0) then
	page="text_templates.asp?"
	name= LNG_TEMPLATES
	Response.Write make_pages(offset, cnt, limit, page, name, sortby) 

	if Session("db_use_backup") <> 1 then
		response.write "<br><A href='#' class='delete_button' onclick=""javascript: return LINKCLICK('"&linkclick_str&"');"">"&LNG_DELETE&"</a><br/><br>"
		response.write "<form action='delete_objects.asp' name='delete_objs' id='delete_objs' method='post'><input type='hidden' name='objType' value='text_templates'>"
	end if
%>



		<table width="100%" height="100%" class="data_table" cellspacing="0" cellpadding="3">
		<tr class="data_table_title">
<%if Session("db_use_backup") <> 1 then%>
		<td width="10"><input type='checkbox' name='select_all' id='select_all' onclick='javascript: select_all_objects();'></td>
<%end if%>
		<td class="table_title"><% response.write sorting(LNG_TEMPLATE,"name", sortby, offset, limit, page) %></td>
		<td class="table_title"><%=LNG_ACTIONS %></td>
				</tr>
<%

'show main table

	Set RS = Conn.Execute("SELECT id, name FROM text_templates ORDER BY "&sortby)
	num=offset
	if(Not RS.EOF) then RS.Move(offset) end if
	Do While Not RS.EOF
		num=num+1
		if((offset+limit) < num) then Exit Do end if
		if(num > offset AND offset+limit >= num) then

		strObjectID=RS("id")
		shouldDisplayTemplate = True
        if ConfTGroupEnabled = 1 then
            
            set tgroupsCountSet = Conn.Execute("SELECT COUNT(1) as cnt FROM policy_tgroups WHERE  policy_id = " & policy)
            
            shouldDisplayTemplate = (not tgroupsCountSet.EOF AND clng(tgroupsCountSet("cnt")) > 0 and gid <> 0)
        end if
        
        if shouldDisplayTemplate then
		%>
		<tr>
<%if Session("db_use_backup") <> 1 then%>
		<td><input type="checkbox" name="objects" value="<%=strObjectID%>"></td>
<%end if%>
		<td>
		<%if(gid = 0 OR (gid = 1 AND templates_arr(4)<>"")) then%>
		<a href="#" onclick="javascript: showAlertPreview(<% Response.Write RS("id")%>)">
		<% end if

		Response.Write RS("name")

		%>
		<%if(gid = 0 OR (gid = 1 AND templates_arr(4)<>"")) then %>
		</a>
		<%end if%>
		</td>
		<td align="center" width="120" nowrap="nowrap">
		<%if(gid = 0 OR (gid = 1 AND templates_arr(4)<>"")) then %>

		<a href="#" onclick="javascript: showAlertPreview(<% Response.Write RS("id")%>)"><img src="images/action_icons/preview.png" alt="<%=LNG_VIEW_TEMPLATE %>" title="<%=LNG_VIEW_TEMPLATE %>" width="16" height="16" border="0" hspace=5></a>
		<% end if %>

<%if(gid = 0 OR (gid = 1 AND templates_arr(3)<>"")) then%>
		<a href="edit_alert_db.asp?tt_id=
		<% Response.Write RS("id") %>
		"><img src="images/action_icons/send.png" alt="<%=LNG_SEND_TEMPLATE %>" title="<%=LNG_SEND_TEMPLATE %>" width="16" height="16" border="0" hspace=5></a>
<%end if%>

<%if(gid = 0 OR (gid = 1 AND templates_arr(1)<>"")) AND Session("db_use_backup") <> 1 then%>
		<a href="text_template_edit.asp?id=
		<% Response.Write RS("id") %>
		"><img src="images/action_icons/edit.png" alt="<%=LNG_EDIT_TEMPLATE %>" title="<%=LNG_EDIT_TEMPLATE %>" width="16" height="16" border="0" hspace=5></a>
		<%end if%>
		</td>	
		</tr>
		
		<%
		end if
		end if
	RS.MoveNext
	Loop
	RS.Close


%>
		</table>
<%
	if Session("db_use_backup") <> 1 then
		response.write "</form><br/><A href='#' class='delete_button' onclick=""javascript: return LINKCLICK('"&linkclick_str&"');"">"&LNG_DELETE&"</a><br><br>"
	end if
	Response.Write make_pages(offset, cnt, limit, page, name, sortby) 
else

Response.Write "<center><b>"& LNG_THERE_ARE_NO_TEMPLATES &".</b><br><br></center>"

end if

%>

		 <img src="images/inv.gif" alt="" width="1" height="100%" border="0">
		</div>
		</td>
		</tr>
	</table>
	</td>
	</tr>
</table>
<%
else
  Response.Redirect "index.asp"

end if
%>
</body>
</html>
<!-- #include file="db_conn_close.asp" --> 