﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PolicyGroups.aspx.cs" Inherits="DeskAlertsDotNet.Pages.PolicyGroups" %>
<%@ Import NameSpace="DeskAlertsDotNet.Parameters" %>
<%@ Import Namespace="Resources" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<title>DeskAlerts</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link href="css/style9.css" rel="stylesheet" type="text/css">
        <link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <link href="css/jquery/jquery_timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css">
        <script language="javascript" type="text/javascript" src="functions.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/date.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery_timepicker/jquery-ui-timepicker-addon.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/jquery/datepicker_fix.js"></script>
        <script language="javascript" type="text/javascript">


            $(document).ready(function() {
                $(".delete_button").button({
                });

                $("#selectAll").click(function () {
                    var checked = $(this).prop('checked');
                    $(".content_object").prop('checked', checked);
                });


            });



            function editUser(id, queryString)
            {
                location.replace('../edit_group.asp?wh=1&id='+ id +'&return_page=users_new.asp?' + queryString);
            }

        </script>
</head>
<body>
    <form id="form1" runat="server">
    <table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	    <tr>
	    <td>
	    <table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
		    <tr>
		    </tr>
            <tr>
            <td class="main_table_body" height="100%">
                <br /><br />
                <table style="padding-left: 10px;" width="100%" border="0"><tbody><tr valign="middle"><td width="175">
                        <% =resources.LNG_SEARCH_GROUPS %> <input type="text" id="searchTermBox" runat="server" name="uname" value="" /></td>
                <td width="1%">
                    <br />
	               <!-- <a name="sub" class="search_button ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" role="button" aria-disabled="false"><span class="ui-button-icon-primary ui-icon ui-icon-search"></span><span class="ui-button-text">Search</span></a> -->
                    <asp:linkbutton runat="server" class="search_button ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" id="searchButton" style="margin-right:0px" role="button" aria-disabled="false" Text="<span class='ui-button-icon-primary ui-icon ui-icon-search'></span><span class='ui-button-text'>{Search}</span>"/>
	                <input type="hidden" name="search" value="1">
                </td>

                <td>
                </td></tr></tbody></table>				
			<div style="margin-left:10px" runat="server" id="mainTableDiv">

			   <table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr><td> 
					<div style="margin-left:10px" id="topPaging" runat="server"></div>
				</td></tr>
				</table>
				<br>

				<br><br>
				<asp:Table style="padding-left: 0px;margin-left: 10px;margin-right: 10px;" runat="server" id="contentTable" Width="30%" Height="100%" CssClass="data_table" CellSpacing="0" CellPadding="3">
				<asp:TableRow CssClass="data_table_title">
					<asp:TableCell runat="server" id="headerSelectAll" CssClass="table_title" Width="2%" >
						<asp:CheckBox id="selectAll" runat="server"/>
					</asp:TableCell>
					<asp:TableCell runat="server" id="groupName" CssClass="table_title" ></asp:TableCell>
					<asp:TableCell runat="server" id="domain" CssClass="table_title"></asp:TableCell>
				</asp:TableRow>
				</asp:Table>
				<br>

				 <br><br>
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				 
				<tr><td> 
					<div style="margin-left:10px" id="bottomPaging" runat="server"></div>
				</td></tr>
				</table>
			</div>

			<div runat="server" id="NoRows">
                <br />
                <br />
				<center><b><%=resources.LNG_THERE_ARE_NO_GROUPS%><b><center>
			</div>
			<br><br>
            </td>
            </tr>
           
 
            
             </table>
    </form>
</body>
</html>
