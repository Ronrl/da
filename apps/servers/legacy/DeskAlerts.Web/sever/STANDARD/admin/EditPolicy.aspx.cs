﻿// ReSharper disable InconsistentNaming
// ReSharper disable StyleCop.SA1300

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Common.Logging.Configuration;
using DeskAlerts.ApplicationCore.Repositories;
using DeskAlerts.ApplicationCore.Utilities;
using DeskAlerts.Server.Utils.Interfaces;
using DeskAlerts.Server.Utils.Services;
using DeskAlertsDotNet.Utils.Services;
using DeskAlertsDotNet.Models.Permission;
using DeskAlertsDotNet.Parameters;
using DeskAlertsDotNet.sever.STANDARD.admin.LogClass;
using DeskAlertsDotNet.sever.STANDARD.Utils;
using NLog;
using Resources;
using DataRow = DeskAlertsDotNet.DataBase.DataRow;
using DataSet = DeskAlertsDotNet.DataBase.DataSet;

namespace DeskAlertsDotNet.Pages
{
    public partial class EditPolicy : DeskAlertsBasePage
    {
        // use curUser to access to current user of DA control panel
        // use dbMgr to access to database
        protected string ous;
        protected string users;
        protected string groups;
        protected string comps;
        private string policiesId;
        private PolicyService _policyService;
        private ISkinService _skinService;
        private IContentSettingsService _contentSettingsService;

        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public EditPolicy()
        {
            _skinService = new SkinService();
            _contentSettingsService = new ContentSettingsService();
        }

        protected override void OnLoad(EventArgs e)
        {
            ClearDeletingGroupParameter();
            base.OnLoad(e);

            var configurationManager = new WebConfigConfigurationManager(Config.Configuration);
            var policyRepository = new PpPolicyRepository(configurationManager);
            var groupRepository = new PpGroupRepository(configurationManager);
            var domainRepository = new PpDomainRepository(configurationManager);
            var userRepository = new PpUserRepository(configurationManager);
            var policyPublisherRepository = new PpPolicyPublisherRepository(configurationManager);
            var skinRepository = new PpSkinRepository(configurationManager);
            var contentSettingsRepository = new PpContentSettingsRepository(configurationManager);

            _policyService = new PolicyService(policyRepository, groupRepository, domainRepository, policyPublisherRepository, userRepository, skinRepository, contentSettingsRepository);

            this.saveButton.Text = resources.LNG_SAVE;
            this.saveButton.Click += this.saveButton_Click;
            if (!string.IsNullOrEmpty(this.Request["close"]))
            {
                this.close.Value = this.Request["close"];
            }

            this.policiesId = this.Request["id"];

            this.ScriptManager1.RegisterAsyncPostBackControl(this.saveButton);
            if (!Config.Data.IsOptionEnabled("ConfTGroupEnabled"))
            {
                this.tGroupsTab.Visible = false;
                this.policyMod.Visible = false;
                this.policyTemplatesGroupsTabContent.Visible = false;
            }
            else
            {
                string createAlertrAttrs =
                  "set_fake(this, 'alerts_3');check_list('alerts');set_if_any('alerts', 'alerts_4');";

                this.alerts_0.Attributes.Add("onclick", createAlertrAttrs);

                string sendAttrs = "set_fake(this, 'alerts_0');" + this.alerts_3.Attributes["onclick"];

                this.alerts_3.Attributes["onclick"] = sendAttrs;
            }

            if (!Config.Data.HasModule(Modules.APPROVE) || Settings.Content["ConfAllowApprove"] != "1")
            {
                this.approveHeader.Visible = false;
                this.alertApproveCheckBox.Style.Add(HtmlTextWriterStyle.Display, "none");
                this.colorCodeApprove.Visible = false;
                this.textToCallApprove.Visible = false;
                this.emergencyApprove.Visible = false;
                this.wallpaperApprove.Visible = false;
                this.lockscreenApprove.Visible = false;
                this.screensaverApprove.Visible = false;
                this.rssApprove.Visible = false;
                this.ipGroupsApprove.Visible = false;
                this.statApprove.Visible = false;
                this.templatesApprove.Visible = false;
                this.groupsApprove.Visible = false;
                this.usersApprove.Visible = false;
                this.surveyApprove.Visible = false;
                this.smsApprove.Visible = false;
                this.emailApprove.Visible = false;
                this.campaignApprove.Visible = false;
            }

            this.colorCodeRow.Visible = Config.Data.HasModule(Modules.EMERGENCY);
            this.textToCallPolicyRow.Visible = Config.Data.HasModule(Modules.TEXTTOCALL);
            this.emergencyPolicyRow.Visible = Config.Data.HasModule(Modules.EMERGENCY);
            this.wallpaperPolicyRow.Visible = Config.Data.HasModule(Modules.WALLPAPER);
            this.lockscreenPolicyRow.Visible = Config.Data.HasModule(Modules.LOCKSCREEN);
            this.screensaverPolicyRow.Visible = Config.Data.HasModule(Modules.SCREENSAVER);
            this.rssPolicyRow.Visible = Config.Data.HasModule(Modules.RSS);
            this.surveyPolicyRow.Visible = Config.Data.HasModule(Modules.SURVEYS);
            this.smsPolicyRow.Visible = Config.Data.HasModule(Modules.SMS);
            this.emailPolicyRow.Visible = Config.Data.HasModule(Modules.EMAIL);
            this.campaignPolicyRow.Visible = Config.Data.HasModule(Modules.CAMPAIGN);

            if (!string.IsNullOrEmpty(this.Request["id"]))
            {
                int id = Convert.ToInt32(this.Request["id"]);
                this.edit.Value = "1";
                this.policyId.Value = this.Request["id"];
                this.LoadPolicy(id);
            }

            if (Settings.IsTrial)
            {
                ip_groups.Checked = false;
                ip_groups.Disabled = true;
                ip_groups_0.Checked = false;
                ip_groups_0.Disabled = true;
                ip_groups_1.Checked = false;
                ip_groups_1.Disabled = true;
                ip_groups_2.Checked = false;
                ip_groups_2.Disabled = true;
                ip_groups_3.Checked = false;
                ip_groups_3.Disabled = true;
                ip_groups_4.Checked = false;
                ip_groups_4.Disabled = true;
            }

            LoadPolicySkinAccess(GetPolicyIdFromPage());
            LoadPolicyContentSettings(GetPolicyIdFromPage());
        }

        private void ClearDeletingGroupParameter()
        {
            if (Request["group_id"] != null)
            {
                var queryString = HttpUtility.ParseQueryString(Request.Url.Query);

                queryString.Remove("group_id");

                string url = Request.Url.AbsolutePath + "?" + queryString.ToString();

                Response.Redirect(url);
            }
        }

        private void SubmitForm()
        {
            this.form1.Action = $"ProcessPolicyEdit.aspx?guildList={ConvertSelectedSkinsToString()}&contentOptionList={ConvertSelectedContentSettingsToString()}";
            this.form1.Method = "post";

            ScriptManager.RegisterClientScriptBlock(this.updatePanel, this.updatePanel.GetType(), "SUBMIT", "submitForm()", true);
        }

        private bool CheckViewers(int policyId)
        {
            var checkObj = this.dbMgr.GetDataByQuery("SELECT editor_id FROM policy_viewer WHERE policy_id = " + policyId);

            return !checkObj.IsEmpty;
        }

        private bool CheckTemplateGroups(int policyId)
        {
            object checkObj = this.dbMgr.GetScalarByQuery("SELECT tgroup_id FROM policy_tgroups WHERE policy_id = " + policyId);

            if (checkObj != null)
            {
                return Convert.ToInt32(checkObj) == 0;
            }

            return true;
        }

        private void LoadPolicy(int policyId)
        {
            DataRow policyMainData = this.dbMgr.GetDataByQuery("SELECT name, type FROM policy WHERE id = " + policyId).First();

            this.policyName.Value = policyMainData.GetString("name");

            switch (policyMainData.GetString("type"))
            {
                case "A":
                    {
                        this.policySys.Checked = true;
                        break;
                    }

                case "E":
                    {
                        this.policyReg.Checked = true;
                        this.policyTabs.Style.Remove(HtmlTextWriterStyle.Display);
                        this.policyTabs.Style.Add(HtmlTextWriterStyle.Display, string.Empty);
                        break;
                    }

                case "M":
                    {
                        if (Config.Data.IsOptionEnabled("ConfTGroupEnabled"))
                        {
                            this.policyModRadio.Checked = true;
                        }

                        break;
                    }
            }

            if (policyMainData.GetString("type").Equals("E"))
            {
                this.recipientsSelector.LinkId = policyId.ToString();

                if (Config.Data.IsOptionEnabled("ConfTGroupEnabled"))
                {
                    this.FillTemplateGroups(policyId);
                }

                // Fill accessTable
                string query =
                    $@"SELECT  alerts_val as alerts, campaign_val as campaign, emails_val as emails, sms_val as sms, users_val as users,
                  groups_val as groups, templates_val as templates, text_templates_val as text_templates,
                  statistics_val as [statistics], ip_groups_val as ip_groups, rss_val as rss, surveys_val as surveys,
                  screensavers_val as screensavers, wallpapers_val as wallpapers, lockscreens_val as lockscreens,
                  im_val as im, text_to_call_val as text_to_call, feedback_val as feedback, cc_val as cc 
                  FROM policy_list WHERE policy_id ={policyId}";

                DataRow permissionsRow = this.dbMgr.GetDataByQuery(query).First();

                string[] availablePermissions = Permission.GetPermissionsForEnabledPolicies().Split(',');

                bool hasAllPermissions = true;
                foreach (var permission in availablePermissions)
                {
                    string trimmedPermission = permission.Replace("'", string.Empty);
                    bool hasAllPermissionForContent = true;
                    string policyMask = permissionsRow.GetString(trimmedPermission);
                    for (int i = 0; i < 8; i++)
                    {
                        FieldInfo fieldInfo = this.GetType().GetField(trimmedPermission + "_" + i, BindingFlags.Instance | BindingFlags.NonPublic);
                        if (fieldInfo == null)
                        {
                            continue;
                        }

                        HtmlInputCheckBox checkBox =
                            fieldInfo.GetValue(this) as HtmlInputCheckBox;
                        bool hasPermission = policyMask[i] == '1';
                        if (checkBox != null)
                        {
                            checkBox.Checked = hasPermission;
                        }
                        if (!hasPermission)
                        {
                            hasAllPermissions = false;
                            hasAllPermissionForContent = false;
                        }
                    }

                    if (hasAllPermissionForContent)
                    {
                        FieldInfo fieldInfo = this.GetType().GetField(trimmedPermission, BindingFlags.Instance | BindingFlags.NonPublic);

                        if (fieldInfo != null)
                        {
                            HtmlInputCheckBox checkBox =
                                fieldInfo.GetValue(this) as HtmlInputCheckBox;

                            checkBox.Checked = true;
                        }
                    }
                }

                this.checkAll.Checked = hasAllPermissions;
            }

            var IsCanSendToSpecificRecipientsOnly = _policyService.IsCanSendToSpecificRecipientsOnly(policyId);

            canSendContentToAllUsers.Checked = !IsCanSendToSpecificRecipientsOnly;
            canSendContentToChosenUsers.Checked = IsCanSendToSpecificRecipientsOnly;

            if (this.canSendContentToChosenUsers.Checked)
            {
                this.addedUsers.Style.Add(HtmlTextWriterStyle.Display, string.Empty);
            }

            bool checkViewers = this.CheckViewers(policyId);
            this.viewAll0.Checked = false;
            this.viewAll1.Checked = false;
            this.viewAll0.Checked = !checkViewers;
            this.viewAll1.Checked = checkViewers;
            this.templatesAll1.Checked = this.CheckTemplateGroups(policyId);
            this.templatesAll0.Checked = !this.CheckTemplateGroups(policyId);

            if (this.templatesAll0.Checked)
            {
                this.tgroups.Style.Add(HtmlTextWriterStyle.Display, string.Empty);
            }
        }

        private void FillTemplateGroups(int policyId)
        {
            DataSet dataSet = this.dbMgr.GetDataByQuery($"SELECT a.name, a.id FROM message_templates_groups as a INNER JOIN policy_tgroups as b ON a.id = b.tgroup_id WHERE b.policy_id={policyId}");

            foreach (DataRow row in dataSet)
            {
                ListItem li = new ListItem(row.GetString("name"), row.GetString("id"));
                this.selectedTgroups.Items.Add(li);

                if (this.idsTgroups.Value.Length > 0)
                {
                    this.idsTgroups.Value += ",";
                }

                this.idsTgroups.Value += row.GetString("id");
            }
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            var policyNameFromRequest = this.Request["policyName"];

            var policyNameFromDb = this.GetPolicyName(this.policyId.Value);

            bool isNew = string.IsNullOrEmpty(this.policiesId);

            string queryPolicyType = this.Request["policy_type"];

            if (_policyService.PolicyExists(this.policyName.Value) && this.edit.Value == "0" && isNew)
            {
                ScriptManager.RegisterClientScriptBlock(
                    this.updatePanel,
                    this.updatePanel.GetType(),
                    "NAME_ERROR",
                    $"alert('{resources.LNG_ENTER_UNIQUE_POLICY_NAME}')",
                    true);
                return;
            }

            if (this.policyName.Value.Length == 0)
            {
                ScriptManager.RegisterClientScriptBlock(
                    this.updatePanel,
                    this.updatePanel.GetType(),
                    "NAME_ERROR",
                    $"alert('{resources.LNG_PLEASE_ENTER_POLICY_NAME}')",
                    true);
                return;
            }

            string usersList = Request["recipientsSelector$usersInput"] + Request["recipientsSelector$ousInput"] + Request["recipientsSelector$computersInput"] + Request["recipientsSelector$groupsInput"];

            if (string.IsNullOrEmpty(usersList) && canSendContentToChosenUsers.Checked)
            {
                ScriptManager.RegisterClientScriptBlock(
                    this.updatePanel,
                    this.updatePanel.GetType(),
                    "NAME_ERROR",
                    $"alert('Please select recipients before you proceed.')",
                    true);
                return;
            }

            if (this.edit.Value == "1" && Settings.Content["ConfTokenAuthEnable"] == "1")
            {
                var tokenTools = new TokenTools();
                string selectEditorByPolicyQuery = $"SELECT editor_id FROM policy_editor WHERE policy_id = '{this.policyId.Value}'";
                var users = this.dbMgr.GetDataByQuery(selectEditorByPolicyQuery);

                foreach (var user in users)
                {
                    var id = user.GetInt("editor_id");
                    tokenTools.DeleteUserTokens(id);
                }
            }

            PolicyLog policyLog = new PolicyLog
            {
                EditPolicy = this.edit.Value,
                CreateNewPolicy = isNew,
                PolicyNameFromRequest = policyNameFromRequest,
                PolicyNameFromDb = policyNameFromDb,
                UserName = this.curUser.Name,
                PolicyName = this.policyName.Value,
                QueryPolicyType = queryPolicyType,
                PolicyId = this.Request["id"],
            };
            policyLog.LogPolicyTypeChange();

            this.SubmitForm();
        }

        /// <summary>
        /// Get policy name by policy id.
        /// </summary>
        /// <param name="policyIdValue">Policy Id.</param>
        /// <returns>Policy name.</returns>
        private string GetPolicyName(string policyIdValue)
        {
            string result = string.Empty;

            if (!string.IsNullOrEmpty(policyIdValue))
            {
                result = this.dbMgr.GetScalarByQuery<string>($"SELECT name FROM policy WHERE id = {this.policyId.Value}");
            }

            return result;
        }

        private string ConvertSelectedSkinsToString()
        {
            var selectedSkinsList = policy_choosingSkinsList_checkBoxList.Items.Cast<ListItem>()
                .Where(x => x.Selected && x.Value != @"default")
                .ToList();

            var guidList = new List<Guid>();
            foreach (var listItem in selectedSkinsList)
            {
                guidList.Add(Guid.Parse(listItem.Value));
            }

            return string.Join(",", guidList);
        }

        private string ConvertSelectedContentSettingsToString()
        {
            var selectedContentOptionList = policy_choosingContentSettingsList_checkBoxList.Items.Cast<ListItem>()
                .Where(x => x.Selected)
                .Select(x => x.Value)
                .ToList();

            var result = string.Join(",", selectedContentOptionList);
            return result;
        }

        private void LoadPolicySkinAccess(long policyIdOnLoad)
        {
            GenerateSkinAccessCheckBoxList(policyIdOnLoad);

            var fullSkinsAccess = true;
            if (policyIdOnLoad != default(long))
            {
                fullSkinsAccess = _policyService.GetPolicyById(policyIdOnLoad).FullSkinAccess;
            }

            var skinAccessRadioButtonAll = new ListItem(resources.LNG_SKINS_CAN_USE_ALL_AVAILABLE_SKINS, true.ToString());
            var skinAccessRadioButtonPartial = new ListItem(resources.LNG_SKINS_CAN_USE_ONLY_CPECIFIED_SKINS, false.ToString());

            if (fullSkinsAccess)
            {
                skinAccessRadioButtonAll.Selected = true;
                skinAccessRadioButtonPartial.Selected = false;

                policy_choosingSkinsList.Style.Value = "display: none;";
            }
            else
            {
                skinAccessRadioButtonAll.Selected = false;
                skinAccessRadioButtonPartial.Selected = true;

                policy_choosingSkinsList.Style.Value = "display: block;";
            }

            policy_SkinsAccess_RadioButtonList.Items.Add(skinAccessRadioButtonAll);
            policy_SkinsAccess_RadioButtonList.Items.Add(skinAccessRadioButtonPartial);
        }

        private void GenerateSkinAccessCheckBoxList(long policyIdOnLoad)
        {
            if (IsPostBack)
            {
                return;
            }

            if (policyIdOnLoad == default(long))
            {
                _logger.Error("Policy Id cannot be equal to default value.");
            }

            try
            {
                var allSkinsList = _skinService.GetAllSkins().ToList();
                var checkedSkinsList = _skinService.GetSkinsByPolicyId(policyIdOnLoad).ToList();

                if (!allSkinsList.Any())
                {
                    _logger.Error("Skins not found in EditPolicy page.");
                    return;
                }

                {
                    const string defaultSkinId = "default";

                    var fileName = $"/admin/skins/{defaultSkinId}/thumbnail.png";
                    var skinNameTitleCase = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(defaultSkinId);
                    var skinName = GetSkinNameLabel(skinNameTitleCase, fileName);

                    var defaultItem = new ListItem(skinName, defaultSkinId)
                    {
                        Enabled = false,
                        Selected = true
                    };
                    policy_choosingSkinsList_checkBoxList.Items.Add(defaultItem);
                }
                

                foreach (var skin in allSkinsList)
                {
                    var fileName = $"/admin/skins/{{{skin.Id}}}/thumbnail.png";
                    var skinName = GetSkinNameLabel(skin.Name, fileName);

                    var listItem = new ListItem(skinName, skin.Id.ToString());
                    var isSelectedItem = checkedSkinsList.Any(x => x.Id == skin.Id);
                    if (isSelectedItem)
                    {
                        listItem.Selected = true;
                    }
                    policy_choosingSkinsList_checkBoxList.Items.Add(listItem);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public string GetSkinNameLabel(string skinId, string skinFileName)
        {
            if (string.IsNullOrEmpty(skinId) || string.IsNullOrEmpty(skinFileName))
            {
                return string.Empty;
            }

            var fileSystemUrl = string.IsNullOrEmpty(skinFileName)
                ? string.Empty
                : Path.Combine($"{HttpRuntime.AppDomainAppPath}/{skinFileName}");

            var webUrl = string.IsNullOrEmpty(skinFileName)
                ? string.Empty
                : Page.ResolveUrl($"~{skinFileName}");

            return File.Exists(fileSystemUrl)
                ? $"<img alt=\"{skinId}\" src=\"{webUrl}\" /><div>{skinId}</div>"
                : $"{skinId}";
        }

        public long GetPolicyIdFromPage()
        {
            var policyIdValue = this.policyId.Value;

            long.TryParse(policyIdValue, out var result);

            return result;
        }

        private void LoadPolicyContentSettings(long policyIdOnLoad)
        {
            GenerateContentSettingsAccessCheckBoxList(policyIdOnLoad);

            var contentSettingsRadioButtonAll = new ListItem(resources.LNG_ALL, true.ToString());
            var contentSettingsRadioButtonPartial = new ListItem(resources.LNG_SELECTED, false.ToString());

            var fullContentSettingsAccess = true;
            if (policyIdOnLoad != default(long))
            {
                fullContentSettingsAccess = _policyService.GetPolicyById(policyIdOnLoad).FullContentSettingsAccess;
            }

            if (fullContentSettingsAccess)
            {
                contentSettingsRadioButtonAll.Selected = true;
                contentSettingsRadioButtonPartial.Selected = false;

                policy_choosingContentSettingsList.Style.Value = "display: none;";
            }
            else
            {
                contentSettingsRadioButtonAll.Selected = false;
                contentSettingsRadioButtonPartial.Selected = true;

                policy_choosingContentSettingsList.Style.Value = "display: block;";
            }

            policy_ContentSettings_RadioButtonList.Items.Add(contentSettingsRadioButtonAll);
            policy_ContentSettings_RadioButtonList.Items.Add(contentSettingsRadioButtonPartial);
        }

        private void GenerateContentSettingsAccessCheckBoxList(long policyIdOnLoad)
        {
            if (IsPostBack)
            {
                return;
            }

            if (policyIdOnLoad == default(long))
            {
                _logger.Error("Policy Id cannot be equal to default value.");
            }

            try
            {
                var allContentOptionsList = _contentSettingsService.GetAllContentSettings();
                var checkedContentOptionList = _contentSettingsService.GetContentSettingsByPublisherId(policyIdOnLoad).ToList();

                foreach (var contentOption in allContentOptionsList)
                {
                    var item = new ListItem(contentOption.Name, contentOption.Id.ToString());
                    var isSelectedItem = checkedContentOptionList.Any(x => x.Name == contentOption.Name);

                    if (isSelectedItem)
                    {
                        item.Selected = true;
                    }

                    policy_choosingContentSettingsList_checkBoxList.Items.Add(item);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

    }
}