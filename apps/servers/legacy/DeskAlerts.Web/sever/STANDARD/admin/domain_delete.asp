<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<% Server.ScriptTimeout = 90000 %>
<%

check_session()

uid = Session("uid")

if uid <> "" then
	domain_id=CLng(Request("id"))
	SQL = "SET NOCOUNT ON" & vbNewLine & _
		_
		"IF OBJECT_ID('tempdb..#tempUsers') IS NOT NULL DROP TABLE #tempUsers" & vbNewLine & _
		"IF OBJECT_ID('tempdb..#tempGroups') IS NOT NULL DROP TABLE #tempGroups" & vbNewLine & _
		"IF OBJECT_ID('tempdb..#tempComps') IS NOT NULL DROP TABLE #tempComps" & vbNewLine & _
		"IF OBJECT_ID('tempdb..#tempOUs') IS NOT NULL DROP TABLE #tempOUs" & vbNewLine & _
		_
		"CREATE TABLE #tempUsers (id BIGINT NOT NULL)" & vbNewLine & _
		"CREATE TABLE #tempGroups (id BIGINT NOT NULL)" & vbNewLine & _
		"CREATE TABLE #tempComps (id BIGINT NOT NULL)" & vbNewLine & _
		"CREATE TABLE #tempOUs (id BIGINT NOT NULL)" & vbNewLine & _
		_
		"INSERT INTO #tempUsers (id) (SELECT id FROM users WHERE domain_id = "&domain_id&")" & vbNewLine & _
		"INSERT INTO #tempGroups (id) (SELECT id FROM groups WHERE domain_id = "&domain_id&")" & vbNewLine & _
		"INSERT INTO #tempComps (id) (SELECT id FROM computers WHERE domain_id = "&domain_id&")" & vbNewLine & _
		"INSERT INTO #tempOUs (id) (SELECT Id FROM OU WHERE Id_domain = "&domain_id&")" & vbNewLine & _
		_
		"DELETE FROM users_groups WHERE user_id IN (SELECT id FROM #tempUsers) OR group_id IN (SELECT id FROM #tempGroups)" & vbNewLine & _
		"DELETE FROM groups_groups WHERE group_id IN (SELECT id FROM #tempGroups) OR container_group_id IN (SELECT id FROM #tempGroups)" & vbNewLine & _
		"DELETE FROM computers_groups WHERE comp_id IN (SELECT id FROM #tempComps) OR group_id IN (SELECT id FROM #tempGroups)" & vbNewLine & _
		"DELETE FROM ou_groups WHERE ou_id IN (SELECT id FROM #tempOUs) OR group_id IN (SELECT id FROM #tempGroups)" & vbNewLine & _
		_
		"DELETE FROM User_synch WHERE Id_user IN (SELECT id FROM #tempUsers)" & vbNewLine & _
		"DELETE FROM Comp_synch WHERE Id_comp IN (SELECT id FROM #tempComps)" & vbNewLine & _
		"DELETE FROM Group_synch WHERE Id_group IN (SELECT id FROM #tempGroups)" & vbNewLine & _
		"DELETE FROM OU_synch WHERE Id_OU IN (SELECT id FROM #tempOUs)" & vbNewLine & _
		_
		"DELETE FROM OU_User WHERE Id_OU IN (SELECT id FROM #tempOUs) OR Id_user IN (SELECT id FROM #tempUsers)" & vbNewLine & _
		"DELETE FROM OU_comp WHERE Id_OU IN (SELECT id FROM #tempOUs) OR Id_comp IN (SELECT id FROM #tempComps)" & vbNewLine & _
		"DELETE FROM OU_Group WHERE Id_OU IN (SELECT id FROM #tempOUs) OR Id_group IN (SELECT id FROM #tempGroups)" & vbNewLine & _
		"DELETE FROM OU_hierarchy WHERE Id_parent IN (SELECT id FROM #tempOUs) OR Id_child IN (SELECT id FROM #tempOUs)" & vbNewLine & _
		"DELETE FROM OU_DOMAIN WHERE Id_OU IN (SELECT id FROM #tempOUs)" & vbNewLine & _
		"DELETE FROM OU_DOMAIN WHERE Id_domain = " & domain_id & vbNewLine & _
		_
		"DELETE FROM users WHERE id IN (SELECT id FROM #tempUsers)" & vbNewLine & _
		"DELETE FROM groups WHERE id IN (SELECT id FROM #tempGroups)" & vbNewLine & _
		"DELETE FROM computers WHERE id IN (SELECT id FROM #tempComps)" & vbNewLine & _
		"DELETE FROM OU WHERE Id IN (SELECT id FROM #tempOUs)" & vbNewLine & _
		_
		"DROP TABLE #tempUsers" & vbNewLine & _
		"DROP TABLE #tempGroups" & vbNewLine & _
		"DROP TABLE #tempComps" & vbNewLine & _
		"DROP TABLE #tempOUs" & vbNewLine
	Conn.Execute(SQL)

	Conn.Execute("DELETE FROM edir_context WHERE domain_id = " & domain_id)

	Conn.Execute("DELETE FROM domains WHERE id = " & domain_id)

	Response.Redirect "admin_domains.asp"

else
	Response.Redirect "index.asp"
end if

%>
<!-- #include file="db_conn_close.asp" -->