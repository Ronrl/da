﻿using System;
using System.Collections.Generic;

using DeskAlertsDotNet.DataBase;

using Resources;

namespace DeskAlertsDotNet.Pages
{
    public partial class EditGroupMembers : DeskAlertsBasePage
    {
        public string groupId;

        protected void Page_Load(object sender, EventArgs e)
        {
            saveBtn.Text = resources.LNG_SAVE;
            saveBtn.Click += OnSaveClick;
            groupId = Request["group_id"];

            if (!string.IsNullOrEmpty(groupId))
            {
                recipientsSelector.LinkId = groupId;
            }
        }
        
        protected void OnSaveClick(object sender, EventArgs e)
        {
            var ous = Request["recipientsSelector$ousInput"].Split(',');
            var users = Request["recipientsSelector$usersInput"].Split(',');
            var computers = Request["recipientsSelector$computersInput"].Split(',');
            var groups = Request["recipientsSelector$groupsInput"].Split(',');

            InsertOUs(ous);
            insertUsers(users);
            refreshUsersInAlertsSent(users);
            InsertComputers(computers);
            InsertGroups(groups);
            Response.Redirect(Request["return_page"]);
        }

        private void InsertOUs(IEnumerable<string> ous)
        {
            dbMgr.ExecuteQuery($"DELETE FROM ou_groups WHERE group_id = {groupId}");

            foreach (var ou in ous)
            {
                int ouId = 0;

                if (int.TryParse(ou, out ouId))
                {
                    dbMgr.ExecuteQuery(
                        $"INSERT INTO ou_groups (group_id, ou_id, was_checked) VALUES ({this.groupId}, {ouId}, 1)");
                }
            }
        }

        private void insertUsers(string[] users)
        {
            dbMgr.ExecuteQuery("DELETE FROM users_groups WHERE group_id = " + groupId);

            foreach (var user in users)
            {
                int userId = 0;

                if (int.TryParse(user, out userId))
                {
                    dbMgr.ExecuteQuery(
                        $"INSERT INTO users_groups (group_id, user_id, was_checked) VALUES ({this.groupId}, {userId}, 1)");
                }
            }
        }

        private void InsertComputers(IEnumerable<string> computers)
        {
            dbMgr.ExecuteQuery("DELETE FROM computers_groups WHERE group_id = " + groupId);

            foreach (var comp in computers)
            {
                if (int.TryParse(comp, out var compId))
                {
                    dbMgr.ExecuteQuery(
                        $"INSERT INTO computers_groups (group_id, comp_id, was_checked) VALUES ({groupId}, {compId}, 1)");
                }
            }
        }

        private void InsertGroups(IEnumerable<string> groups)
        {
            dbMgr.ExecuteQuery($"DELETE FROM groups_groups WHERE container_group_id = {groupId}");

            foreach (var group in groups)
            {
                if (int.TryParse(@group, out var childGroupId))
                {
                    dbMgr.ExecuteQuery(
                        $"INSERT INTO groups_groups (container_group_id, group_id, was_checked) VALUES ({this.groupId}, {childGroupId}, 1)");
                }
            }
        }

        private void refreshUsersInAlertsSent(string[] users)
        {
            List<int> alertsSentIdOfEdotGroup = new List<int>();
            DataSet alertsIdOfgroup = dbMgr.GetDataByQuery("SELECT alert_id FROM alerts_sent_group WHERE group_id=" + groupId);
            foreach (DataRow row in alertsIdOfgroup)
            {
                alertsSentIdOfEdotGroup.Add(row.GetInt("alert_id"));
            }

            foreach (int alertId in alertsSentIdOfEdotGroup)
            {
                dbMgr.ExecuteQuery($"DELETE FROM alerts_sent WHERE alert_id=+{alertId}");
            }

            foreach (var user in users)
            {
                int userId = 0;

                if (int.TryParse(user, out userId))
                {
                    foreach (int alert_id in alertsSentIdOfEdotGroup)
                    {
                        dbMgr.ExecuteQuery($"INSERT INTO alerts_sent (alert_id,user_id) VALUES ({alert_id},{userId})");
                    }
                }
            }
        }
    }
}