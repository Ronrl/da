﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Parameters;
using System.Web.UI.HtmlControls;
using DeskAlertsDotNet.Managers;
using DeskAlertsDotNet.Utils.Factories;
using Resources;
using DataRow = DeskAlertsDotNet.DataBase.DataRow;

namespace DeskAlertsDotNet.Pages
{
    public partial class PolicyList : DeskAlertsBaseListPage
    {
        protected override HtmlInputText SearchControl => searchTermBox;

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            headerTitle.Text = resources.LNG_POLICIES;
            searchButton.Text = searchButton.Text.Replace("{Search}", resources.LNG_SEARCH);
            policyNameCell.Text = GetSortLink(resources.LNG_NAME, "policy_name", Request["sortBy"]);
            publishersCountCell.Text = resources.LNG_PUBLISHERS_COUNT;
            actionsCell.Text = resources.LNG_ACTIONS;

            Table = contentTable;

            FillTableWithContent(true);
        }


        #region DataProperties
        protected override string DataTable => "policy";

        protected override string CustomQuery
        {
            get
            {
                var sqlQuery =
                    "SELECT policy.id, policy.name as policy_name, COUNT(policy_editor.editor_id) " +
                    "FROM policy " +
                    "LEFT JOIN policy_editor " +
                    "ON policy_editor.policy_id = policy.id ";

                if (SearchTerm.Length > 0 || !string.IsNullOrEmpty(Request["search"]) && Config.Data.IsDemo)
                {
                    sqlQuery += " WHERE policy.name LIKE '%" + SearchTerm + "%' AND policy.name <> 'defaultuser'";
                }
                else if (!string.IsNullOrEmpty(Request["search"]))
                {
                    sqlQuery += " WHERE policy.name LIKE '%" + SearchTerm + "%'";
                }
                else if (Config.Data.IsDemo)
                {
                    sqlQuery += " WHERE policy.name <> 'defaultuser'";
                }
                sqlQuery += " GROUP BY policy.name, policy.id";

                var sortBy = Request["sortBy"];
                if (!String.IsNullOrEmpty(sortBy))
                {
                    sqlQuery += $" ORDER BY {sortBy}";
                }

                return sqlQuery;
            }
        }

        protected override string CustomCountQuery
        {
            get
            {
                string sqlQuery = "SELECT COUNT(id) FROM policy";

                if (SearchTerm.Length > 0 || !string.IsNullOrEmpty(Request["search"]))
                    sqlQuery += " WHERE policy.name LIKE '%" + SearchTerm + "%'";

                return sqlQuery;
            }
        }

        protected override string[] Collumns => new string[] { "name" };

        protected override string DefaultOrderCollumn => "name";

        protected override string SearchField
        {
            get => "name";
            set => base.SearchField = value;
        }
        /*
         * 
         * If you want to add condition for data selection override property ConditionSqlString 
         * Also you can implement your custom query by overriding properties CustomQuery and CustomCountQuery
         *  
         */
        #endregion

        #region Interface properties

        protected override IButtonControl[] DeleteButtons
        {
            get
            {

                return new IButtonControl[] { upDelete, bottomDelete };
            }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl NoRowsDiv
        {
            get { return NoRows; }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl TableDiv
        {
            get { return mainTableDiv; }
        }

        protected override string ContentName
        {
            get
            {
                //return content name here
                return resources.LNG_POLICIES;
            }
        }


        protected override System.Web.UI.HtmlControls.HtmlGenericControl BottomPaging
        {
            get
            {
                return bottomRanging;
            }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl TopPaging
        {
            get
            {
                return topPaging;
            }
        }

        protected override void OnDeleteRows(string[] ids)
        {
            base.OnDeleteRows(ids);

            string idsString = string.Join(",", ids);
            dbMgr.ExecuteQuery($"DELETE FROM policy_computer WHERE policy_id IN ({idsString})");
            dbMgr.ExecuteQuery($"DELETE FROM policy_editor WHERE policy_id IN ({idsString})");
            dbMgr.ExecuteQuery($"DELETE FROM policy_group WHERE policy_id IN ({idsString})");
            dbMgr.ExecuteQuery($"DELETE FROM policy_ip_group WHERE policy_id IN ({idsString})");
            dbMgr.ExecuteQuery($"DELETE FROM policy_list WHERE policy_id IN ({idsString})");
            dbMgr.ExecuteQuery($"DELETE FROM policy_ou WHERE policy_id IN ({idsString})");
            dbMgr.ExecuteQuery($"DELETE FROM policy_tgroups WHERE policy_id IN ({idsString})");
            dbMgr.ExecuteQuery($"DELETE FROM policy_user WHERE policy_id IN ({idsString})");
            dbMgr.ExecuteQuery($"DELETE FROM policy_viewer WHERE policy_id IN ({idsString})");

            Logger.Info($"User: \"{curUser.Name}\" was deleted policy with id {idsString}");
        }

        protected override List<Control> GetActionControls(DataRow row)
        {
            List<Control> controls = new List<Control>();

            if (curUser.IsAdmin)
            {
                LinkButton editButton = GetActionButton(row.GetString("id"), "images/action_icons/edit.png", "LNG_EDIT_POLICY",
                    delegate
                    {
                        Response.Redirect("EditPolicy.aspx?id=" + row.GetString("id"), true);
                    }
                );
                controls.Add(editButton);

                LinkButton duplicateButton = GetActionButton(row.GetString("id"), "images/action_icons/duplicate.png", "LNG_DUPLICATE_POLICY", "duplicatePolicy(" + row.GetString("id") + ")");

                controls.Add(duplicateButton);
            }

            // images/action_icons/list.png

            LinkButton listViewButton = GetActionButton(row.GetString("id"), "images/action_icons/list.png", "LNG_VIEW_EDITORS_FOR_POLICY", "showEditorsList(" + row.GetString("id") + ")");
            controls.Add(listViewButton);
            return controls;
        }


        [WebMethod]
        public static void DuplicatePolicy(int id)
        {

            using (var db = new DBManager())
            {
                var newPolicyName = GetNewPolicyName(id, db);

                var policyNameParam = SqlParameterFactory.Create(DbType.String, newPolicyName, "policyName");
                int newId =
                    db.GetScalarByQuery<int>(
                        "INSERT INTO policy(name, type) OUTPUT INSERTED.ID SELECT @policyName, type FROM policy WHERE id = " +
                        id, policyNameParam);


                //insert users, groups, ous and comps from old policy to new
                string relationshipSqlQuery = "INSERT INTO policy_group (policy_id, group_id, was_checked) SELECT " +
                                              newId +
                                              ", group_id, was_checked FROM policy_group WHERE policy_id=" + id;

                db.ExecuteQuery(relationshipSqlQuery);

                relationshipSqlQuery = "INSERT INTO policy_ou (policy_id, ou_id, was_checked) SELECT " + newId +
                                       ", ou_id, was_checked FROM policy_ou WHERE policy_id=" + id;

                db.ExecuteQuery(relationshipSqlQuery);

                relationshipSqlQuery = "INSERT INTO policy_user (policy_id, user_id, was_checked) SELECT " + newId +
                                       ", user_id, was_checked FROM policy_user WHERE policy_id=" + id;

                db.ExecuteQuery(relationshipSqlQuery);

                relationshipSqlQuery = "INSERT INTO policy_computer (policy_id, comp_id, was_checked) SELECT " + newId +
                                       ", comp_id, was_checked FROM policy_computer WHERE policy_id=" + id;

                db.ExecuteQuery(relationshipSqlQuery);

                //duplicate permissons
                var policyListQuery = "INSERT INTO policy_list (" +
                                      "policy_id, alerts_val,emails_val,sms_val,surveys_val,users_val,groups_val,templates_val,text_templates_val,statistics_val,ip_groups_val,rss_val,screensavers_val,wallpapers_val,lockscreens_val,im_val,text_to_call_val,feedback_val,cc_val,webplugin_val,video_val,campaign_val" +
                                      ") SELECT " +
                                      newId + ", alerts_val,emails_val,sms_val,surveys_val,users_val,groups_val,templates_val,text_templates_val,statistics_val,ip_groups_val,rss_val,screensavers_val,wallpapers_val,lockscreens_val,im_val,text_to_call_val,feedback_val,cc_val,webplugin_val,video_val,campaign_val" +
                                      " FROM policy_list WHERE policy_id=" + id;


                db.ExecuteQuery(policyListQuery);

                //duplicate viewers
                db.ExecuteQuery("INSERT INTO policy_viewer(policy_id, editor_id) SELECT " + newId +
                                ", editor_id FROM policy_viewer WHERE policy_id = " + id);

                Logger.Info($"User \"{UserManager.Default.CurrentUser.Name} \" click to the \"Duplicate Policy\" and was create policy with name \"{newPolicyName}\"");
            }
        }

        private static string GetNewPolicyName(int id, DBManager db)
        {
            var newPolicyName =
                db.GetDataByQuery($"SELECT name FROM policy WHERE id = {id}").GetRow(0).GetValue("name") + " copy";
            var newPolicyNameParam = SqlParameterFactory.Create(DbType.String, newPolicyName, "newPolicyNameParam");

            while (db.GetScalarByQuery<int>("SELECT count(name) FROM policy WHERE name = @newPolicyNameParam", newPolicyNameParam) > 0)
            {
                newPolicyName += " copy";
                newPolicyNameParam = SqlParameterFactory.Create(DbType.String, newPolicyName, "newPolicyNameParam");
            }
            
            return newPolicyName;
        }

        #endregion
    }
}