<%if showCheckbox=1 then %>
<table cellpadding="0" cellspacing="0" border="0">
<tr><td>
	<div class="ui-widget">
		<div class="ui-widget-content ui-state-error ui-corner-all" style="padding:0.7em" id="obj_cnt_border">
			<%=LNG_YOU_HAVE_SELECTED %> <A href="javascript: showSelectedRecipients();"><span id="obj_cnt">0</span> <%=LNG_OBJECTS %></a>&nbsp;(<a href="javascript:changeObjectType(true)"><%=LNG_CLEAR %></a>)
		</div>
	</div>
</td>
    <td>
        <div style="padding:0px 10px"><%=LNG_CUSTOMGROUPS_HINT%>&nbsp;<img src="images/help.png" custom-tooltip="" /></div>
    </td>
</tr>
</table>
<br/>
<%end if%>
<%if ConfShowGroupsByDefault=1 then %>
<input type="hidden" name="f_users" id="f_users" value="0" />
<input type="hidden" name="f_groups" id="f_groups" value="1" />
<%else %>
<input type="hidden" name="f_users" id="f_users" value="1" />
<input type="hidden" name="f_groups" id="f_groups" value="0" />
<%end if %>
<input type="hidden" name="f_computers" id="f_computers" value="0" />
<table width="100%" height="515" cellspacing="0" cellpadding="0"><tr valign="top">
<td width="200" id="tree_td" style="display: none;">
	<style>
	.mif-tree-wrapper{height:500px}
	</style>
	<div id="tree_container" style="border-right: 1px solid black; width: 200px; height: 500px"></div>
</td>
<td id="tree_td_ed" style="display: none; width: 200px;">
</td>
<td style="background-color: #ffffff; position: relative; z-index: 1;"  align="center" >

<iframe src="<%if(showCheckbox=1) then %>objects.asp<%else if ConfShowGroupsByDefault = 1 then %>groups_new.asp<%else%>users_new.asp<%end if end if%>?sc=<%=showCheckbox %>&sms=<%=sms%>&email=<%=email%>&desktop=<%=desktop%>" width="99%" height="515" style="border: 0px solid #ffffff;" frameborder="0" id="content_iframe" name="content_iframe" onload="iframeReload();"></iframe>
</td>
</tr></table>

<div id="objects_div">
<%
	if dup_id <> "" then
		SQL = "SELECT u.id, u.name, 'u' as type FROM alerts_sent s INNER JOIN users u ON s.user_id = u.id WHERE s.alert_id = " & dup_id
		SQL = SQL & " UNION ALL "
		SQL = SQL & "SELECT c.id, c.name, 'c' as type FROM alerts_sent_comp s INNER JOIN computers c ON s.comp_id = c.id WHERE s.alert_id = " & dup_id
		SQL = SQL & " UNION ALL "
		SQL = SQL & "SELECT g.id, g.name, 'g' as type FROM alerts_sent_group s INNER JOIN groups g ON s.group_id = g.id WHERE s.alert_id = " & dup_id
		SQL = SQL & " UNION ALL "
		SQL = SQL & "SELECT DISTINCT * FROM (SELECT i.id, i.name, '' as type FROM alerts_sent_iprange s INNER JOIN ip_range_groups g ON (CAST(s.ip_from / 16777216 as varchar)+'.'+CAST((s.ip_from % 16777216) / 65536 as varchar)+'.'+CAST(((s.ip_from % 16777216) % 65536) / 256 as varchar)+'.'+CAST(((s.ip_from % 16777216) % 65536) % 256 as varchar)) = g.from_ip AND (CAST(s.ip_to / 16777216 as varchar)+'.'+CAST((s.ip_to % 16777216) / 65536 as varchar)+'.'+CAST(((s.ip_to % 16777216) % 65536) / 256 as varchar)+'.'+CAST(((s.ip_to % 16777216) % 65536) % 256 as varchar)) = g.to_ip INNER JOIN ip_groups i ON i.id = g.group_id WHERE s.alert_id = " & dup_id &") tmp"
		Set RS = Conn.Execute(SQL)
		do while not RS.EOF
			id = RS("id")
			name = RS("name")
			type1 = RS("type")
			%>
			<input type="hidden" name="added_user" id="object<%=type1&id %>" value="<%=type1&id %>"/>
			<input type="hidden" name="added_recipient_name" id="object_name<%=type1&id %>" value="<%=HtmlEncode(name) %>"/>
			<%
			RS.MoveNext
		loop
	end if
%>
</div>

	<script type="text/javascript">
		function showSelectedRecipients()
		{
			var obj_cnt = document.getElementById("obj_cnt");
			if(obj_cnt && obj_cnt.innerHTML > 0)
			{
				var recipientsHTML = '<table width="95%" border="1" bordercolor="#EAE9E1" cellspacing="0" cellpadding="3"><tr bgcolor="#EAE9E1">	<td class="table_title"><%=LNG_NAME %></td>	<td class="table_title"><%=LNG_TYPE %></td><td class="table_title"><%=LNG_ACTIONS %></td>	</tr>';
				if(tree)
				{
					var objType="<%=LNG_OU %>";
					tree.root.recursive(function()
					{
						if(this.state.checked == "checked" && this.property.type1 != "organization" && this.property.type1 != "DOMAIN")
						{
							recipientsHTML = recipientsHTML + "<tr><td>"+this.property.name+"</td><td>"+objType+"</td><td><img src='images/action_icons/delete.gif' border='0' onclick='removeOU(this, \""+this.property.id+"\", \""+this.property.type1+"\");' style='cursor: hand;' /></a></td></tr>";
						}
					});
				}
				var addedRecipients = document.getElementsByTagName("input");
				for(var i=0; i< addedRecipients.length; i++){
					if(addedRecipients[i].name=='added_user'){
						var objId = addedRecipients[i].id;
						var objType = "<%=LNG_IP_GROUP %>"
						if(objId.replace(/object/g, "").indexOf("u")!=-1){
							objType="<%=LNG_USER %>";
						}
						if(objId.replace(/object/g, "").indexOf("g")!=-1){
							objType="<%=LNG_GROUP %>";
						}
						if(objId.replace(/object/g, "").indexOf("c")!=-1){
							objType="<%=LNG_COMPUTER %>";
						}
						
						var objName = document.getElementById(objId.replace(/object/g, "object_name")).value;
						objId = objId.replace(/object/g, "");
						recipientsHTML = recipientsHTML + "<tr><td>"+objName+"</td><td>"+objType+"</td><td><img src='images/action_icons/delete.gif' border='0' onclick='javascript: removeRecipient(this, \""+objId+"\");' style='cursor: hand;' /></a></td></tr>";
					}
				}
				recipientsHTML = recipientsHTML + "</table>";
				document.getElementById("showRecipientsDivContent").innerHTML=recipientsHTML;
				document.getElementById("showRecipientsDiv").style.display="";
				document.getElementById("hideAll").style.display="";
			}
		}
		
		function removeOU(obj, id, type)
		{
			if(tree)
			{
				tree.root.recursive(function()
				{
					if(id==this.property.id && type==this.property.type1){
						this['switch']('unchecked');
					}
				});
			}
			//obj.parentNode.parentNode.parentNode.removeChild(obj.parentNode.parentNode);
			showSelectedRecipients();
		}
		
		function removeRecipient(obj, id)
		{
			iframeRemoveObject(id);
			obj.parentNode.parentNode.parentNode.removeChild(obj.parentNode.parentNode);
		}
		
		function hideSelectedRecipients()
		{
			document.getElementById("showRecipientsDiv").style.display="none";
			document.getElementById("hideAll").style.display="none";
			//reload main frame after close
			//document.getElementById("content_iframe").src = document.getElementById("content_iframe").src;
			var myIframe= document.getElementById("content_iframe");  
			var iframeDocument = (myIframe.contentWindow || myIframe.contentDocument);  
			if (iframeDocument.document){  
				iframeDocument = iframeDocument.document;  
			}
			//iframeDocument.getElementById('search_users').submit();
		}
		
		var tree;
		
		function getCheckedOus(id, action)
		{
			if( tree && tree != 'undefined' )
			{
				nodes = new Array();
				tree.root.recursive
				(
					function()
					{
						if(action==1 && id==this.property.id){
								temp = new Object();
								nodes.push(temp);
								nodes[nodes.length-1].id = this.property.id;
								nodes[nodes.length-1].type = this.property.type1;
								nodes[nodes.length-1].child = this.children.length;
						}
						if( this.state.checked == "checked")
						{
							if(action==0 && id==this.property.id){}
							else{
								temp = new Object();
								nodes.push(temp);
								nodes[nodes.length-1].id = this.property.id;
								nodes[nodes.length-1].type = this.property.type1;
								nodes[nodes.length-1].child = this.children.length;
							}
						}
					}
				);
				return nodes;
			}
			return null;
		}
		
		window.addEvent('domready',function()
		{
				tree = new Mif.Tree({
					container: $('tree_container'),
					forest: false,
					initialize: function(){
						<% if(showCheckbox=1) then %>
						this.initCheckbox('deps');
						<% end if %>
						new Mif.Tree.KeyNav(this);
					},					
					types: {
						organization:{
							openIcon: 'mif-tree-organization-icon',
							closeIcon: 'mif-tree-organization-icon'
						},
						domain:{
							openIcon: 'mif-tree-domain-icon',
							closeIcon: 'mif-tree-domain-icon'
						},
						loader:{
							openIcon: 'mif-tree-loader-icon',
							closeIcon: 'mif-tree-loader-icon'
						},
						ou:{
							openIcon: 'mif-tree-ou-icon',
							closeIcon: 'mif-tree-ou-icon'
						}
					},
					dfltType:'organization'
					
				});

				//tree.initSortable();
				try
				{
					tree.load({
						url: 'ou_get_tree.asp'
					});
					
				}
				catch(e)
				{
				}
				tree.addEvent('loadNode',function(node){
					updateObjectsCount(true);
				});
				tree.loadOptions=function(node){
					return {
						url: 'ou_get_child.asp?id='+node.id+'&type='+node.property.type1
					};
				};
				tree.addEvent('onSelect',function(node, state){
					var content_file="users_new.asp";
					if($('obj_type').value=="R"){
						content_file="objects.asp"
					}
					if($('obj_type').value=="U"){
						content_file="users_new.asp"
					}
					if($('obj_type').value=="G"){
						content_file="groups_new.asp"
					}
					if($('obj_type').value=="C"){
						content_file="computers_new.asp"
					}
					res = getCheckedOus(0, 0);
					idSequence = "";
					if( res ){
						for(i=0;i<res.length;i++){
							if(res[i].type != "organization" && res[i].type != "DOMAIN"){
								idSequence += res[i].id.toString() + " ";
							}
						}
					}
					$('content_iframe').src=content_file+"?fu="+$('f_users').get('value')+"&fg="+$('f_groups').get('value')+"&fc="+$('f_computers').get('value')+"&sc=<%=showCheckbox %>&sms=<%=sms%>&email=<%=email%>&device=<%=device%>&desktop=<%=desktop%>&id="+node.id+"&type="+node.property.type1+"&ous="+idSequence;
				});
				tree.addEvent('onCheck',function(node, state){
					updateObjectsCount();
					if(node.state.selected==true){
						var content_file="users_new.asp";
						if($('obj_type').value=="R"){
							content_file="objects.asp"
						}
						if($('obj_type').value=="U"){
							content_file="users_new.asp"
						}
						if($('obj_type').value=="G"){
							content_file="groups_new.asp"
						}
						if($('obj_type').value=="C"){
							content_file="computers_new.asp"
						}
				
						res = getCheckedOus(node.id, 1);
						idSequence = "";
						if( res ){
							for(i=0;i<res.length;i++){
								if(res[i].type != "organization" && res[i].type != "DOMAIN"){
									idSequence += res[i].id.toString() + " ";
								}
							}
						}
						$('content_iframe').src=content_file+"?fu="+$('f_users').get('value')+"&fg="+$('f_groups').get('value')+"&fc="+$('f_computers').get('value')+"&sc=<%=showCheckbox %>&sms=<%=sms%>&email=<%=email%>&desktop=<%=desktop%>&id="+node.id+"&type="+node.type+"&ous="+idSequence;						
					}
				});
				tree.addEvent('onUnCheck',function(node, state){
					updateObjectsCount();
					if(node.state.selected==true){
						var content_file="users_new.asp";
						if($('obj_type').value=="R"){
							content_file="objects.asp"
						}
						
						if($('obj_type').value=="U"){
							content_file="users_new.asp"
						}
						if($('obj_type').value=="G"){
							content_file="groups_new.asp"
						}					
						if($('obj_type').value=="C"){
							content_file="computers_new.asp"
						}
						res = getCheckedOus(node.id, 0);
						idSequence = "";
						if( res ){
							for(i=0;i<res.length;i++){
								if(res[i].type != "organization" && res[i].type != "DOMAIN"){
									idSequence += res[i].id.toString() + " ";
								}
							}
						}
						$('content_iframe').src=content_file+"?fu="+$('f_users').get('value')+"&fg="+$('f_groups').get('value')+"&fc="+$('f_computers').get('value')+"&sc=<%=showCheckbox %>&sms=<%=sms%>&email=<%=email%>&desktop=<%=desktop%>&id="+node.id+"&type="+node.type+"&ous="+idSequence;						
					}
				});
		});
	function updateObjectsCount(first)
	{
		var count = 0;
		if(typeof tree != 'undefined' && tree.root)
		{
			tree.root.recursive(function()
			{
				if(this.state.checked == "checked" && this.property.type1 != "organization" && this.property.type1 != "DOMAIN")
					count++;
			});
		}
		var addedRecipients = document.getElementsByTagName("input");
		for(var i=0; i<addedRecipients.length; i++)
		{
			if(addedRecipients[i].name=='added_user')
				count++;
		}
		jQuery('#obj_cnt').html(count);
		if(!first) jQuery('#obj_cnt_border').removeClass('ui-state-error');
	}
	function iframeAddObject(id,objName){
		//alert("add id from iframe"+id);
		if(document.getElementById('object'+id)==null){
			var el = document.createElement('input');
			el.type = 'hidden';
			el.name = 'added_user';
			el.value = id;
			el.id = 'object'+id;
			document.getElementById("objects_div").appendChild(el);
			var el2 = document.createElement('input');
			el2.type = 'hidden';
			el2.name = 'added_recipient_name';
			el2.value = objName;
			el2.id = 'object_name'+id;
			document.getElementById("objects_div").appendChild(el2);
			updateObjectsCount();
		}
	}

	function iframeChangeFilter(id, value){
		document.getElementById(id).value=value;
		if(value==0){
			if(document.getElementById("f_users").value == 0 && document.getElementById("f_groups").value == 0 && document.getElementById("f_computers").value == 0){
				document.getElementById(id).value = 1;
				alert("You can't uncheck all filters.");
				return false;
			}
		}
		return true;
	}
	
	function iframeRemoveObject(id){
		//alert("remove id from iframe"+id);
		if(document.getElementById('object'+id)!=null){
			var objectInput = document.getElementById("object"+id);
			var objectInputName = document.getElementById("object_name"+id);
			document.getElementById("objects_div").removeChild(objectInput);
			document.getElementById("objects_div").removeChild(objectInputName);
			updateObjectsCount();
		}
	}
	
	function iframeReload(){

		var myIframe= document.getElementById("content_iframe");  
		var iframeDocument = (myIframe.contentWindow || myIframe.contentDocument);  
		if (iframeDocument.document){  
			iframeDocument = iframeDocument.document;  
		}
		var iframeInputs = iframeDocument.getElementsByTagName("input");
		for(var i=0; i<iframeInputs.length; i++){
			if(iframeInputs[i].type=="checkbox"){
				if(document.getElementById(iframeInputs[i].id)!=null){
					iframeInputs[i].checked=true;
				}
			}
		}
		resize();
	}
	function editorOUSelect(){
		var ouId = this.id.replace("ou_", "");
		$$('.ed_ou_selected').addClass('ed_ou');
		$$('.ed_ou_selected').removeClass('ed_ou_selected');
		this.removeClass('ed_ou');
		this.addClass('ed_ou_selected');
		
		var content_file="users_new.asp";
		if($('obj_type').value=="R"){
			content_file="objects.asp"
		}
		if($('obj_type').value=="U"){
			content_file="users_new.asp"
		}
		if($('obj_type').value=="G"){
			content_file="groups_new.asp"
		}
		if($('obj_type').value=="C"){
			content_file="computers_new.asp"
		}
		var src = content_file+"?search=1&fu="+$('f_users').get('value')+"&fg="+$('f_groups').get('value')+"&fc="+$('f_computers').get('value')+"&sc=0&sms=<%=sms%>&email=<%=email%>&desktop=<%=desktop%>";
		if(ouId!="all") {
			src += "&id="+ouId+"&type=ou&ous=";
		}
		$('content_iframe').src = src;
	}
	function resize()
	{
		var hideAll = $$('#hideAll');
		hideAll.setStyle('width', '1px');
		hideAll.setStyle('height', '1px');
		var size = $$('.body').getScrollSize()[0];
		hideAll.setStyle('width', (size.x-12)+'px');
		hideAll.setStyle('height', (size.y-12)+'px');
	}
	window.addEvent('resize', resize);
	window.addEvent('domready', resize);
	</script>
<style type="text/css">
	.mif-tree-node-wrapper{
		cursor: pointer !important;
		cursor: hand !important;
	}
	.mif-tree-gadjet, .mif-tree-checkbox{
		cursor: default !important;
	}
</style>
<table id="hideAll" style="display:none; filter:alpha(opacity=60); -moz-opacity: 0.6; opacity: 0.6;  position:absolute; left:6px; top:6px; right:6px; bottom:6px; z-index: 1; background-color: #999999; width: 100%; height: 100%;" border="0"><tr><td>&nbsp;</td></tr></table>
<div id="showRecipientsDiv" style="position:absolute; top: 30%; left:30%; z-index:2; display:none; border: 1px solid black; width: 400px; height: 351px; background-color: #aaaaaa;"> 

<table width="100%" border="0" bordercolor="#000000" cellspacing="0" cellpadding="0" bgcolor="#ffffff" class="body">
		<tr>
		<td width=100% height=31 class="theme-colored-back"><a href="#" class="header_title"><%=LNG_LIST_OF_RECIPIENTS %></a> </td><td class="theme-colored-back" align="center"><A href="javascript: hideSelectedRecipients();" class="header_title">X</a>&nbsp;&nbsp;&nbsp;</td>
		</tr>
		<tr>
		<td bgcolor="#ffffff" colspan="2">
		<div style="margin:10px;">
		
<div id="showRecipientsDivContent" style="text-align: left; height:300px; overflow-y:scroll;"> </div>
 
 
</div>
		</td>
		</tr>
	</table>
</div>