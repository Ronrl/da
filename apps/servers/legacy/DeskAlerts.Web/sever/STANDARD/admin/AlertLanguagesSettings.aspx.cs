﻿using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Models.Permission;
using DeskAlertsDotNet.Parameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DeskAlertsDotNet.Pages
{
    using Resources;

    public partial class AlertLanguagesSettings : DeskAlertsBaseListPage
    {

        protected override void OnLoad(EventArgs e)
        {
            
            base.OnLoad(e);

            this.Table = contentTable;
            languageNameCell.Text = resources.LNG_LANGUAGE_NAME;
            languageCodeCell.Text = resources.LNG_LANGUAGE_CODE;
            defaultLanguageCell.Text = resources.LNG_DEFAULT_LANGUAGE;
            actionsHeaderCell.Text = resources.LNG_ACTIONS;

            int cnt = this.Content.Count < (Offset + Limit) ? this.Content.Count : Offset + Limit;

            for (int i = Offset; i < cnt; i++)
            {
                DataRow row = this.Content.GetRow(i);
                TableRow tableRow = new TableRow();
                bool isDefault = row.GetInt("is_default") == 1;
                if (curUser.HasPrivilege || curUser.Policy[Policies.ALERTS].CanDelete)
                {
                    CheckBox chb = GetCheckBoxForDelete(row);

                    chb.Enabled = !isDefault;
                    TableCell checkBoxCell = new TableCell();
                    checkBoxCell.HorizontalAlign = HorizontalAlign.Center;
                    checkBoxCell.Controls.Add(chb);
                    tableRow.Cells.Add(checkBoxCell);
                }

                TableCell languageName = new TableCell
                {
                    Text = row.GetString("name"),
                    HorizontalAlign = HorizontalAlign.Center
                };

                tableRow.Cells.Add(languageName);

                TableCell languageCode = new TableCell
                {
                    Text = row.GetString("abbreviatures"),
                    HorizontalAlign = HorizontalAlign.Center
                    
                };

                tableRow.Cells.Add(languageCode);



                TableCell isDefaultCell = new TableCell()
                {
                    HorizontalAlign = HorizontalAlign.Center
                };

                RadioButton defaultRadioButton = new RadioButton
                {
                    ClientIDMode = ClientIDMode.Static,
                    ID = row.GetString("id"),
                    GroupName = "is_default",
                    Checked = isDefault
                };

                isDefaultCell.Controls.Add(defaultRadioButton);
                tableRow.Cells.Add(isDefaultCell);


                TableCell actionsCell = new TableCell()
                {
                    HorizontalAlign = HorizontalAlign.Center
                };

                LinkButton editButton = GetActionButton(row.GetString("id"), "images/action_icons/edit.png", "LNG_EDIT_ALERT",
                                                delegate
                                                {
                                                    Response.Redirect("AddAlertLanguage.aspx?id=" + row.GetString("id"), true);
                                                }, 10

                                            );

                actionsCell.Controls.Add(editButton);
                tableRow.Cells.Add(actionsCell);
                contentTable.Rows.Add(tableRow);

                addButton.Text = resources.LNG_ADD;
                saveButton.Text = resources.LNG_SAVE;

                if (Config.Data.IsDemo && curUser.Name != "admin")
                {
                    this.saveButton.Visible = false;
                    upDelete.Visible = false;
                }
               
                bool useGoogleTranslate = !string.IsNullOrEmpty(Settings.Content["ConfUseGoogleTranslate"]) && Settings.Content["ConfUseGoogleTranslate"] != "0";
                ConfUseGoogleTranslate.Checked = useGoogleTranslate;
                if (useGoogleTranslate)
                {
                    
                    googleTranslateSettings.Style.Remove(HtmlTextWriterStyle.Display);
                    googleTranslateSettings.Style.Add(HtmlTextWriterStyle.Display, "");
                    ConfGoogleTranslateKey.Value = Settings.Content["ConfGoogleTranslateKey"];
                }
                else
                {
                    googleTranslateSettings.Style.Remove(HtmlTextWriterStyle.Display);
                    googleTranslateSettings.Style.Add(HtmlTextWriterStyle.Display, "none");
                }

            }
        }

        #region DataProperties 
        protected override string DataTable
        {
            get
            {
                return "alerts_langs";
            }
        }

        protected override string[] Collumns
        {
            get
            {

                //return array of collumn`s names of table from data table
                return new string[]
                {
                    "id",
                    "name",
                    "abbreviatures",
                    "is_default"
                };

            }
        }

        protected override string DefaultOrderCollumn
        {
            //return collumn for default order
            get
            {
                return "id";

            }
        }
        protected override string DefaultSortingOrder => "ASC";

        /*
         * 
         * If you want to add condition for data selection override property ConditionSqlString 
         * Also you can implement your custom query by overriding properties CustomQuery and CustomCountQuery
         *  
         */
        #endregion

        #region Interface properties

        protected override IButtonControl[] DeleteButtons
        {
            get
            {
                return new IButtonControl[] { upDelete };
            }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl NoRowsDiv
        {
            get { return NoRows; }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl TableDiv
        {
            get { return mainTableDiv; }
        }

        protected override string ContentName
        {
            get
            {
                //return content name here
                return "";
            }
        }

        [WebMethod]
        public static void SetDefaultLanguage(int languageId, string translateKey = "")
        {
            using (var dbManager = new DBManager())
            {
                dbManager.ExecuteQuery("UPDATE alerts_langs SET is_default = '0'");
                dbManager.ExecuteQuery($"UPDATE alerts_langs SET is_default = '1' WHERE id = {languageId}");

                if (!string.IsNullOrEmpty(translateKey))
                {
                    dbManager.ExecuteQuery("UPDATE settings SET val = '1' WHERE name = 'ConfUseGoogleTranslate'");
                    dbManager.ExecuteQuery($"UPDATE settings SET val = '{translateKey.EscapeQuotes()}' WHERE name = 'ConfGoogleTranslateKey'");
                }
                else
                {

                    dbManager.ExecuteQuery("UPDATE settings SET val = '0' WHERE name = 'ConfUseGoogleTranslate'");
                    dbManager.ExecuteQuery("UPDATE settings SET val = '' WHERE name = 'ConfGoogleTranslateKey'");
                }

                Settings.Content.Reload();
            }
                

        }

        #endregion
    }
}