﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Parameters;

namespace DeskAlertsDotNet.Pages
{
    using Resources;

    public partial class IpRanges : DeskAlertsBaseListPage
    {

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            nameCell.Text = GetSortLink(resources.LNG_NAME, "name", Request["sortBy"]);

            int cnt = this.Content.Count < (Offset + Limit) ? this.Content.Count : Offset + Limit;
            for (int i = Offset; i < cnt; i++)
            {
                DataRow dataRow = this.Content.GetRow(i);
                TableRow row = new TableRow();

                TableCell checkBoxCell = new TableCell();
                CheckBox checkBox = new CheckBox();
                //  checkBox.InputAttributes["name"] = "added_" + objType;
                checkBox.InputAttributes["class"] = "content_object";
                checkBox.InputAttributes["value"] = dataRow.GetString("name");
                checkBox.ID = "recipient_ipgroup_" + dataRow.GetString("id");
                checkBoxCell.Controls.Add(checkBox);

                row.Cells.Add(checkBoxCell);

                TableCell nameTableCell = new TableCell();
                nameTableCell.Text = dataRow.GetString("name");

                row.Cells.Add(nameTableCell);

                contentTable.Rows.Add(row);

            }

        }

        #region DataProperties
        protected override string DataTable => "ip_groups";

        protected override string[] Collumns => new string[] {"name"};

        protected override string SearchField => "name";

        protected override string DefaultOrderCollumn => "name";

        /*
         * 
         * If you want to add condition for data selection override property ConditionSqlString 
         * Also you can implement your custom query by overriding properties CustomQuery and CustomCountQuery
         *  
         */
        #endregion

        #region Interface properties

        protected override IButtonControl[] DeleteButtons
        {
            get
            {
                return new IButtonControl[] { };
            }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl NoRowsDiv
        {
            get { return NoRows; }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl TableDiv
        {
            get { return mainTableDiv; }
        }

        protected override string ContentName
        {
            get
            {
                //return content name here
                return "";
            }
        }

        protected override HtmlGenericControl BottomPaging
        {
            get { return this.bottomPaging; }
        }

        protected override HtmlGenericControl TopPaging
        {
            get { return this.topPaging; }
        }


        #endregion
    }
}