﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Preview.aspx.cs" Inherits="DeskAlertsDotNet.Pages.Preview" %>
<%@ Import NameSpace="DeskAlertsDotNet.Parameters" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<script type="text/JavaScript" language="JavaScript" src="jscripts/jquery/jquery.min.js"></script>
<script type="text/JavaScript" language="JavaScript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script type="text/JavaScript" language="JavaScript" src="jscripts/json2.js"></script>
<style>
html {
	margin: 0;
	padding: 0;
}
body {
	margin: 0;
	padding: 0;
}
</style>

<script type="text/JavaScript" language="JavaScript">
var captionReady = false;
var bodyReady = false;

var eventsStack = [];

bodyStack = [];
captionStack = [];

function close()
{
	window.parent.closeAlertPreview();
}

function callFromStackInWindow(stack,window)
{
	while(stack.length > 0)
	{
		var args = stack.shift();
		callInWindow(window,args);
	}
}

function callInWindow(window,args)
{
	var name = args[0];
	args = Array.prototype.slice.call(args);
	args.shift();
	var func = window[name];
	if(typeof(func) == 'function')
	{
		func.apply(func, args);
	}
}

function callInCaption()
{
	if (captionReady)
	{
		callInWindow(document.getElementById("caption_frame").contentWindow,arguments)
	}
	else
	{
		captionStack.push(arguments);
	}
}
function callInBody()
{
	if (bodyReady)
	{
		callInWindow(document.getElementById("body_frame").contentWindow,arguments)
	}
	else
	{
		bodyStack.push(arguments);
	}
}

function captionOnLoad(e)
{
	if (document.getElementById("caption_frame").contentWindow.$)
	{
		captionReady = true;
		callFromStackInWindow(captionStack,document.getElementById("caption_frame").contentWindow);
		triggerEvent('captionReady');
	}
}

function bodyOnLoad()
{
	if (document.getElementById("body_frame").contentWindow.$)
	{
		bodyReady = true;
		callFromStackInWindow(bodyStack,document.getElementById("body_frame").contentWindow);
		triggerEvent('bodyReady');
	}
}

$(document).ready(function(){
	window.parent.setPreviewSize({width:"<%=CaptionWidth%>",height:"<%=CaptionHeight%>"});
	window.parent.openAlertPreview();

	

	$("#body_post_form").submit();
	$("#caption_post_form").submit();
    
});


function triggerEvent(eventName)
{
	for (var i=0; i<eventsStack.length; i++)
	{
		if (eventsStack[i].name == eventName)
		{	
			eventsStack[i].callback();
			eventsStack.splice(i,1);
			i--;
		}
	}
}

function addPreviewEventListener(eventName,callback)
{
	if (eventName == 'captionReady' && captionReady)
	{	
		callback();
	}
	else if (eventName == 'bodyReady' && captionReady)
	{
		callback();
	}
	else
	{
		eventsStack.push({name:eventName, callback:callback});
	}
}

</script>
</head>
<body runat="server" id="previewBody" style="overflow: hidden; background:transparent">
    <form id="form1" runat="server">
    <div>
 	<iframe scrolling="no" onload="captionOnLoad" allowtransparency="true" frameBorder="0" name="caption_frame" id="caption_frame" style="height:<%=CaptionHeight%>px; width:<%=CaptionWidth%>px; margin:0px; padding:0px; border:0px; background: <%=CaptionBackground%>"></iframe>
	<iframe onload="bodyOnLoad" allowtransparency="false" frameBorder="0" name="body_frame" id="body_frame" style="height:<%=BodyHeight%>px; width:<%=BodyWidth%>px; position: absolute; top:<%=TopMargin%>px; left:<%=LeftMargin%>px; margin:0px; padding:0px; border:0px;"></iframe>
    </div>
    </form>
    
    <form style="margin:0px; width:0px; height:0px; padding:0px" id="caption_post_form" action="PreviewCaption.aspx" method="post" target="caption_frame">
		<input type="hidden" id="ext_props_caption" name="ext_props"  value="<%=HttpUtility.HtmlEncode(ExtProps) %>"/>
		<input type="hidden" id="caption_href" name="caption_href" value="<%=CaptionHref%>"/>
		<input type="hidden" id="skin_id_caption" name="skin_id" value="<%=SkinId%>"/>
		<input type="hidden" id="caption_html" name="caption_html" value="<%=HttpUtility.HtmlEncode(HttpUtility.HtmlEncode(CaptionHtml))%>"/>
	</form>
	<form style="margin:0px; width:0px; height:0px; padding:0px" id="body_post_form" action="PreviewBody.aspx" method="post" target="body_frame">
		<input type="hidden" id="ext_props_body" name="ext_props" value="<%=HttpUtility.HtmlEncode(ExtProps) %>"/>
		<input type="hidden" id="custom_js" name="custom_js" value="<%=HttpUtility.HtmlEncode(CustomJs) %>"/>
		<input type="hidden" id="alert_html" name="alert_html" value="<%=HttpUtility.HtmlEncode(HttpUtility.HtmlEncode(AlertHtml)) %>"/>
		<input type="hidden" id="skin_id_body" name="skin_id" value="<%=SkinId%>"/>
        <input type="hidden" id="need_question" name="need_question" value="<%=NeedQuestion%>"/>
	</form>   
    
</body>
</html>
