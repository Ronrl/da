﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DashBoard.aspx.cs" Inherits="DeskAlertsDotNet.Pages.DashBoard" %>

<%@ Import Namespace="DeskAlertsDotNet.Parameters" %>
<%@ Import Namespace="Resources" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" class="dashboard_body">
<head runat="server">
    <title>DeskAlerts</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="css/style9.css?v=9.1.12.29" rel="stylesheet" type="text/css">

    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>

    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
    <script type="text/JavaScript" language="JavaScript" src="jscripts/json2.js"></script>

    <script language="javascript" type="text/javascript" src="jscripts/preview.js?v=9.2.1.31"></script>

    <script language="javascript" type="text/javascript" src="functions.js"></script>

    <script language="javascript" type="text/javascript" src="jscripts/date.js"></script>

    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.cookie.js"></script>

    <script language="javascript" type="text/javascript" src="jscripts/isotope.pkgd.min.js"></script>

    <script language="javascript" type="text/javascript" src="jscripts/packery-mode.pkgd.min.js"></script>

    <script language="javascript" type="text/javascript" src="functions.js"></script>

    <style>
        .column {
            float: left;
            padding-bottom: 50px;
        }

        .portlet {
            margin: 0 1em 1em 0;
            padding: 0.2em;
        }

        .portlet-header {
            padding: 0.2em 0.3em;
            position: relative;
            text-align: center;
            cursor: pointer;
        }

        .portlet-toggle {
            position: absolute;
            top: 50%;
            right: 25px;
            margin-top: -8px;
        }

        .portlet-destroy {
            position: absolute;
            top: 50%;
            right: 5px;
            margin-top: -8px;
        }

        .portlet-content {
            padding: 0.4em;
        }

        table.chartLegend {
            margin-right: auto;
            margin-left: auto;
        }
    </style>

    <script language="javascript" type="text/javascript">

        var widgetsList;

        function scrollTo(x) {
            $('html, body').animate({
                scrollTop: x
            }, 10);
        }

        function showWallpaperPreview(alertId) {
            var data;
            getAlertData(alertId, false, function (alertData) {
                data = JSON.parse(alertData);
            });
            var imageSrc = data.alert_html.replace(/^admin\//, "");
            var position = data.fullscreen;
            $("#preview_image_src").val(imageSrc);
            $("#preview_position").val(position);

            var params = 'width=' + screen.width;
            params += ', height=' + screen.height;
            params += ', top=0, left=0'
            params += ', fullscreen=yes';


            //openDialog2('', screen.width, screen.height);
            window.open('', 'preview_win', params);

            $("#preview_post_form").submit();
            hideLoader();
        }

        function changeParameters(widgetId, params) {

            var frameId = "#i" + widgetId.toUpperCase();

            var oldSrc = $(frameId).attr("src");

            var newSrc = oldSrc + "?" + params;

            $(frameId).attr("src", newSrc);
        }

        function showScreenSaverPreview(alertId) {

            $(function () {
                getAlertData(alertId, false, function (alertData) {
                    data = JSON.parse(alertData);
                });

                var html = data.alert_html;
                $("#preview_html").val(html);

                var params = 'width=' + screen.width;
                params += ', height=' + screen.height;
                params += ', top=0, left=0'
                params += ', fullscreen=yes';

                window.open('', 'preview_win', params);

                $("#scr_preview_post_form").submit();
                hideLoader();
            });
        }

        function hideLoader() {
            $.grep($(window.top.document).find("frame"), function (n, i) {
                if ($(n).attr("name") == "main") {
                    var doc = $(n)[0].contentDocument || $(n)[0].contentWindow.document;
                    $("#loader", doc).fadeOut(500);
                    $("#shadow", doc).fadeOut(500);
                }
            });
        }

        function openDialog(alertID) {
            var url = "<%=Config.Data.GetString("alertsDir")%>/preview.asp?id=" + alertID;
            $('#linkValue').val(url);
            $('#linkValue').focus(function () {
                $('#linkValue').select().mouseup(function (e) {
                    e.preventDefault();
                    $(this).unbind("mouseup");
                });
            });
            $('#linkDialog').dialog('open');
        }

        function closePreview() {
            $(document).unbind("click", closePreview);
            $(window).unbind("click", closePreview);
            $(window).unbind("scroll", closePreview);
            $(window).unbind("resize", closePreview);
            $("#preview_div").hide();
        }


        function showSurveyPreview(id) {


            if ($("#preview_div").is(":visible") == true) {
                closePreview();
            }

            $.get("../get_survey_data.asp?id=" + id,
                function (data) {

                    var surveyData = JSON.parse(data);

                    //   $("#preview_data").val(data);
                    $("#preview_id").val(id);
                    $("#preview_title").val(surveyData.name);
                    $("#preview_skin_id").val(surveyData.skin_id);
                    $("#create_date").val(surveyData.create_date);
                    $("#alert_width").val(surveyData.alert_width);
                    $("#alert_height").val(surveyData.alert_height);

                    if (surveyData.fullscreen == 1) {
                        $("#fullscreen").val(1);
                    } else {
                        $("#fullscreen").val(0);
                    }

                    $("#position").val(surveyData.position);

                    $("#preview_form").submit();
                    $("#preview_div").draggable();
                    $("#preview_div").fadeIn(1000);

                    setTimeout(function () {
                        $(document).bind("click", closePreview);
                        $(window).bind("click", closePreview);
                        $(window).bind("scroll", closePreview);
                        $(window).bind("resize", closePreview);
                        $("#preview").unbind("click", closePreview);

                    },
                        1000);


                    var position = surveyData.fullscreen;
                    $("#preview_div").css('left', '');
                    $("#preview_div").css('top', '');
                    $("#preview_div").css('bottom', '');
                    $("#preview_div").css('right', '');
                    switch (position) {

                        case 2:
                            {
                                $("#preview_div").css('left', '0px');
                                $("#preview_div").css('top', '0px');
                                break;
                            }
                        case 3:
                            {
                                $("#preview_div").css('right', '0px');
                                $("#preview_div").css('top', '0px');
                                break;
                            }
                        case 4:
                            {
                                $("#preview_div").center();

                                break;
                            }
                        case 5:
                            {
                                $("#preview_div").css('left', '0px');
                                $("#preview_div").css('bottom', '0px');
                                break;
                            }
                        case 6:
                            {
                                $("#preview_div").css('right', '0px');
                                $("#preview_div").css('bottom', '0px');
                                break;
                            }
                        default:
                            {
                                $("#preview_div").css('right', '0px');
                                $("#preview_div").css('bottom', '0px');
                                break;
                            }
                    }
                });


        }

        function detectIE() {
            var ua = window.navigator.userAgent;
            var msie = ua.indexOf('MSIE ');
            var trident = ua.indexOf('Trident/');

            if (msie > 0) {
                // IE 10 or older => return version number
                return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
            }

            if (trident > 0) {
                // IE 11 (or newer) => return version number
                var rv = ua.indexOf('rv:');
                return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
            }

            // other browser
            return false;
        }

        function getOrder() {
            var str = "";
            var items = $("#statList").sortable("toArray");
            for (var i = 0, n = items.length; i < n; i++) {
                var v = $('#' + items[i]).find('.portlet-content').is(':visible');
                items[i] = items[i] + "," + v;
                if (i != 0) str = str + "," + items[i];
                else str = str + items[i];
            }
            $.get("set_parameters.asp?name=setDashboardListOrder&dashboardListOrder=" + str);
        }

        function restoreOrder() {
            $.get("set_parameters.asp?name=getDashboardListOrder", function (data) {
                var list = $("#statList");
                if (list == null) return;
                var cookie = data;
                if (!cookie) return;
                var IDs = cookie.split(',');
                var rebuild = new Array();
                var items = list.sortable("toArray");
                for (var v = 0, len = items.length; v < len; v++) {
                    rebuild[items[v]] = items[v];
                }
                for (var i = 0, n = IDs.length; i < n - 1; i++) {
                    var itemID = IDs[i];
                    var visible = IDs[i + 1]; i = i + 1;
                    if (itemID in rebuild) {
                        var item = rebuild[itemID];
                        var child = $("div.ui-sortable").children("#" + item);
                        var savedOrd = $("div.ui-sortable").children("#" + itemID);
                        $("div.ui-sortable").filter(":first").append(savedOrd);
                        if (visible === 'false') {
                            child.find(".ui-icon").toggleClass("ui-icon-minusthick ui-icon-plusthick");
                            child.find(".portlet-content").hide();
                        }
                        else {
                            child.find(".portlet-content").show();
                        }
                    }
                }
            });
        }

        function addWidgetToPage(id, title, link) {
            $('#statList').append("<div class='add_here' id='" + id + "'></div>");
            iframeSize = getWidgetWidth(title);
            portletSize = iframeSize == "100%" ? iframeSize : iframeSize + 10;
            $('.add_here').first().append('<div class="portlet-header overall_link">' + title + '</div>' +
                '<div class="portlet-content">' +
                '<iframe  class="widget_iframe" id="i' + id + '" width="' + iframeSize + '" frameborder="0" onLoad="autoHeight(\'i' + id + '\')" src="' + link + '"/>' +
                '</div>' +
                '</div>').addClass("portlet").addClass("column").width(portletSize).addClass("isotopey").removeClass("add_here")
                .addClass("ui-widget ui-widget-content ui-helper-clearfix ui-corner-all")
                .find(".portlet-header")
                .addClass("ui-widget-header ui-corner-all")
                .prepend("<span class='ui-icon ui-icon-minusthick portlet-toggle'></span>")
                .prepend("<span class='ui-icon ui-icon-closethick portlet-destroy'></span>");
        }

        function changeIframeSrc(id, newsrc) {
            document.getElementById(id).src = newsrc;
        }

        function autoHeight(id) {
            var height = document.getElementById(id);
            var newheight;
            if (document.getElementById) {
                scrollheight = document.getElementById(id).contentWindow.document.body.scrollHeight;
                offsetheight = document.getElementById(id).contentWindow.document.body.offsetHeight;
                //alert("scroll" + scrollheight);
                //alert("offset" + offsetheight);
                if (document.getElementById(id).src.indexOf("WidgetLastContent.aspx") != -1) {
                    newheight = 370;
                }
                else if (scrollheight > offsetheight + 50 && offsetheight > 0) {
                    newheight = offsetheight + 25;
                }
                else {
                    newheight = scrollheight + 20;
                }
            }
            var ie = detectIE();
            if (ie != false) {
                newheight = newheight + 20;
            }

            document.getElementById(id).height = (newheight) + "px";
            if (height != newheight) {
                $("#statList").isotope('reloadItems').isotope({ sortBy: 'original-order' });
            }
        }

        $(document)
            .ready(function () {

                var list = $('#statList');
                list.isotope({
                    transformsEnabled: false,
                    itemSelector: '.isotopey',
                    layoutMode: 'packery'
                });

                $("#preview_div").hide();

                list.sortable({
                    connectWith: "#statList",
                    cursor: 'move',
                    start: function (event, ui) {
                        ui.item.addClass('grabbing moving').removeClass('isotopey');
                        ui.placeholder
                            .addClass('starting')
                            .removeClass('moving')
                            .css({
                                top: ui.originalPosition.top,
                                left: ui.originalPosition.left
                            });
                        list.isotope('reloadItems');
                    },
                    change: function (event, ui) {
                        ui.placeholder.removeClass('starting');
                        list
                            .isotope('reloadItems')
                            .isotope({ sortBy: 'original-order' });
                    },
                    beforeStop: function (event, ui) {
                        ui.placeholder.after(ui.item);
                    },
                    update: function () { getOrder(); },
                    stop: function (event, ui) {
                        ui.item.removeClass('grabbing').addClass('isotopey');
                        list
                            .isotope('reloadItems')
                            .isotope({ sortBy: 'original-order' },
                            function () {
                                if (!ui.item.is('.grabbing')) {
                                    ui.item.removeClass('moving');
                                }
                            });
                    }
                });
                $(".portlet")
                    .addClass("ui-widget ui-widget-content ui-helper-clearfix ui-corner-all")
                    .find(".portlet-header")
                    .addClass("ui-widget-header ui-corner-all")
                    .prepend("<span class='ui-icon ui-icon-minusthick portlet-toggle'></span>")
                    .prepend("<span class='ui-icon ui-icon-closethick portlet-destroy'></span>");
                $('#statList')
                    .on('click',
                    '.portlet-toggle',
                    function () {
                        $(this).toggleClass("ui-icon-minusthick").toggleClass("ui-icon-plusthick");
                        $(this).parents(".portlet:first").find(".portlet-content").toggle();
                        var frame_id = $(this).parents(".portlet:first").attr("id");
                        autoHeight("i" + frame_id);
                        var isIE = detectIE();
                        if ($(this).parents(".portlet:first").find(".portlet-content").is(":visible") && isIE != false) {
                            document.getElementById("i" + frame_id).contentDocument.location.reload(true);
                        }
                        $("#statList").isotope('reloadItems').isotope({ sortBy: 'original-order' });
                        getOrder();
                    });
                $('#statList')
                    .on('click',
                    '.portlet-destroy',
                    function () {
                        var item_id = $(this).parents(".portlet:first").attr("id");
                        //var jqxhr = $.get("manipulate_widgets.asp",{id:item_id, action: "remove"})
                        $.ajax({
                            type: "POST",
                            url: "WidgetService.aspx/RemoveWidget",
                            data: '{widgetId:"' + item_id + '"}',
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (response) {
                                // location.reload();
                                //  window.parent.parent.document.getElementById('menu').src = window.parent.parent.document.getElementById('menu').src;
                            },
                            failure: function (response) {
                                alert(response.d);
                            }
                        });

                        $(this).parents(".portlet:first").remove();
                        $("#statList").isotope('reloadItems').isotope({ sortBy: 'original-order' });
                        getOrder();
                    });
                initWidgetsForEditor();
                restoreOrder();
                $(".widget_iframe")
                    .load(function () {
                        $("#statList").isotope('reloadItems').isotope({ sortBy: 'original-order' });
                    });

                $("#add_another")
                    .button({
                        label: "<%=resources.LNG_ADD_WIDGET%>",
                        icons: { primary: "ui-icon-newwin" }
                    })
                    .click(function () { $("#add_widget_form").dialog("open"); });
		        //$("#invite").button({label:"<%=resources.LNG_INV%>",icons: {primary: "ui-icon-newwin"}}).click(function(){ $("#invite_dialog_form").dialog("open");});
                $("#send_btn")
                    .button({ label: "<%=resources.LNG_SEND%>", icons: { primary: "ui-icon-newwin" } })
                    .click(function () { sendEmail(); });
                $("#channels_link").button();

                function sendEmail() {

                    var email = $("#email_box").val();

                    if (email.length == 0 || email.indexOf("@") == -1 || email.indexOf(".") == -1) {
                        alert('<% =resources.LNG_WRONG_EMAIL%>');
                    } else {

                        $.get("mail_sender.asp?email=" + email,
                            function (data) {
                                alert(data);
                                $("#invite_dialog_form").dialog("close");
                            });
                    }

                }

                $("#invite_dialog_form")
                    .dialog(
                    {
                        autoOpen: false,
                        height: 145,
                        width: 600,
                        modal: true,
                        resizable: false

                    });
                $("#add_widget_form")
                    .load("AddWidget.aspx")
                    .dialog({
                        title: "<%=resources.LNG_ADD_WIDGET%>",
                        position: { my: "left top", at: "left top", of: "#dialog_dock" },
                        modal: true,
                        autoOpen: false,
                        resizable: false,
                        draggable: false,
                        width: 'auto',
                        resize: 'auto',
                        margin: 'auto'
                    });


                function initWidgetsForEditor() {
                    widgetsList = JSON.parse('<%=widgetsList%>');
                    for (i = 0; i < widgetsList.length; i++) {
                        id = widgetsList[i].Id;
                        title = widgetsList[i].Title;
                        link = widgetsList[i].Href;

                        if (title == "Instant Send")
                            title = "Emergency Alerts";

                        addWidgetToPage(id, title, link);
                    }
                }

                $("#linkDialog")
                    .dialog({
                        autoOpen: false,
                        height: 80,
                        width: 550,
                        modal: true,
                        resizable: false
                    });

                function showFirstGuide() {
                    $("#guideDialog").dialog("open");
                    $("#firstGuideDialog").show();
                    $("#commonGuideDialog").hide();
                }
                var height = $(window.parent.parent.document).height();
                window.parent.parent.onChangeBodyHeight(height, false);
            });


    </script>
</head>
<body style="margin: 6px; padding: 0px; border: 1px solid #b3b3b7" class="body dashboard_body">
    <form id="form1" runat="server">
        <table width="100%" cellspacing="0" cellpadding="0">
            <tr>
                <td class="main_table_title" style="padding: 3px 0px 0px 7px">
                    <img src="images/menu_icons/contentschedule_20.png" alt="" width="20" height="20" border="0"></td>
                <td width="100%" height="31" class="main_table_title"><span class="header_title"><%=resources.LNG_DASHBOARD%></span></td>
            </tr>
        </table>
        <div id="dialog-form" style="overflow: hidden; display: none;">
            <iframe id='dialog-frame' style="width: 100%; height: 100%; overflow: hidden; border: none; display: none"></iframe>
        </div>
        <div id="secondary" style="overflow: hidden; display: none;">
        </div>

        <div id="dialog_dock" class="dashboard_field">

            <asp:LinkButton ID="add_another" Style="margin-bottom: 10px; float: left">
			Add Widget</asp:LinkButton>





            <div style="clear: both; width: 100%"></div>
            <div id="add_widget_form">
            </div>

            <div id="invite_dialog_form">
                <label><% =resources.LNG_ENTER_USER_MAIL%></label><br />
                <br />
                <label for="email_box"><% =resources.LNG_EMAIL + ":"%></label>
                <input type="text" id="email_box" style="float: center" align="center"><br>
                <button id="send_btn" style="float: right"><% =resources.LNG_SEND%></button>
            </div>
            <div id="statList">
            </div>
        </div>



        <div id="guideDialog" title="DeskAlerts Beginner's Guide" style="display: none">
            <div id="firstGuideDialog" style="display: none">
                <p>
                    DeskAlerts v8 Control Panel has an integrated guide which will give you some hints on basic operations you can perform within it. Do you want to receive these advices? You can later change the guide behavior from the Profile Settings page, which you have in the side menu on the left.
                </p>
                <a href="#" id="acceptGuide" style="display: inline-block"></a>
                <a href="#" id="rejectGuide" style="display: inline-block"></a>
            </div>
            <div id="commonGuideDialog" style="display: none">
                <p>
                    DeskAlerts Control Panel Interface consists of a few panels. On the left, you can see a main menu - giving you access to the different product pages. On the right there is a help panel - it can be collapsed by using the "Help" button on top of the page. The zone you're reading this message in is your main working area.
                </p>
                <p>
                    This page is a Dashboard - you can fill it with various widgets using the "Add Widget" button. It will bring up a list of widget tiles with a short descriptions being shown if you hover your mouse over a tile. You can add any widgets you need, but be aware that using too much of them at a time can significantly slow down the page loading.
                </p>
            </div>
        </div>
        <div id="linkDialog" title="<%= resources.LNG_DIRECT_LINK%>" style="display: none">
            <input id="linkValue" type="text" readonly="readonly" style="width: 500px" />
        </div>

    </form>
</body>

<form action="../SurveyPreview.aspx" target="preview_frame" id="preview_form" method="post">
    <input type="hidden" name="data" id="preview_data" />
    <input type="hidden" name="skin_id" id="preview_skin_id" />
    <input type="hidden" name="id" id="preview_id" />
    <input type="hidden" name="create_date" id="create_date" />
    <input type="hidden" name="title" id="preview_title" />
    <input type="hidden" name="alert_width" id="alert_width" runat="server" />
    <input type="hidden" name="alert_height" id="alert_height" runat="server" />
    <input type="hidden" name="position" id="position" runat="server" />
    <input type="hidden" name="fullscreen" id="fullscreen" runat="server" />
</form>


<div id="preview_div" style="position: fixed; left: 40%; top: 20%; z-index: 1">
    <iframe id="preview_frame" name="preview_frame" width="520" height="420" frameborder="0" src="../SurveyPreview.aspx"></iframe>
</div>

<form style="margin: 0px; width: 0px; height: 0px; padding: 0px" id="preview_post_form"
    action="wallpaper_preview.asp" method="post" target="preview_win">
    <input type="hidden" id="preview_image_src" name="image_src" value="" />
    <input type="hidden" id="preview_position" name="position" value="" />
</form>

<form style="margin: 0px; width: 0px; height: 0px; padding: 0px" id="scr_preview_post_form" action="ScreensaverPreview.aspx" method="post" target="preview_win">
    <input type="hidden" id="preview_html" name="html" value="" />
</form>


</html>
