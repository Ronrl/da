﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PublisherList.aspx.cs" Inherits="DeskAlertsDotNet.Pages.PublisherList" %>
<%@ Import NameSpace="DeskAlertsDotNet.Parameters" %>
<%@ Import NameSpace="Resources" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>DeskAlerts</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link href="css/style9.css" rel="stylesheet" type="text/css">
    <link href="css/toastr.min.css" rel="stylesheet" type="text/css">
    <link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css">
    <link href="css/jquery/jquery_timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css">

    <script language="javascript" type="text/javascript" src="functions.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/date.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery_timepicker/jquery-ui-timepicker-addon.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/jquery/datepicker_fix.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/shortcut.js"></script>
    <script type="text/javascript" src="jscripts/toastr.min.js"></script>
    <script language="javascript" type="text/javascript">
    $(document).ready(function () {
        var error = "<%=PublisherOperationError%>";

        if (error.length > 0) {
            toastr.error(error, "Error is occure while proceeding operation");
        }

        $( "#dialog-confirm" ).dialog({
            resizable: false,
            autoOpen: false,
            height: "auto",
            width: 400,
            modal: true,
            buttons: {
                Ok: function () {
                    __doPostBack('publisherOperationConfirmed', '');
                    $( this ).dialog( "close" );
                },
                Cancel: function() {
                    $( this ).dialog( "close" );
                }
            }
        });

        var showConfirmDialog = '<%=ShowConfirmDialog%>';

        if (showConfirmDialog != 'False') {
            $("#dialog-confirm").dialog("open");
        }

        shortcut.add("Alt+N", function (e) {
            e.preventDefault();
        });
				
        shortcut.add("delete",function(e) {
	        e.preventDefault();
		    if (confirmDelete())
			    __doPostBack('upDelete', '');
	    });
			
        $(".delete_button").button({});

        $("#selectAll").click(function () {
            var checked = $(this).prop('checked');

            $(".content_object").prop('checked', checked);
        });

        $(".add_alert_button").button({
            icons: {
                primary: "ui-icon-plusthick"
            }
        });

        $(document).tooltip({
            items: "img[title],a[title]",
            position: {
                my: "center bottom-20",
                at: "center top",
                using: function (position, feedback) {
                    $(this).css(position);
                    $("<div>").addClass("arrow").addClass(feedback.vertical).addClass(feedback.horizontal).appendTo(this);
                }
            }
        });
    });

			function confirmDelete() {
                if (confirm("Are you sure you want to delete selected items?") == true)
                    return true;
                else
                    return false;
            }

          
            function showPreview(id) {
                window.open('../UserDetails.aspx?id=' + id, '','status=0, toolbar=0, width=350, height=350, scrollbars=1');
            }

            function editUser(id, queryString)
            {
                location.replace('../EditUsers.aspx?id='+ id +'&return_page=users_new.asp?' + queryString);
            }

        </script>
</head>
<body>
    <form id="form1" runat="server">
        <table width="100%" class="fullsize" border="0" cellspacing="0" cellpadding="6" height="100%">
	        <tr><td>
	            <table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
		            <td width="100%" height="31" class="main_table_title"><img src="images/menu_icons/editors_20.png" alt="" width="20" height="20" border="0" style="padding-left:7px">
		                <asp:LinkButton href="PublisherList.aspx" runat="server" id="headerTitle" class="header_title" style="position:absolute;top:22px" />
		            </td>
                    <tr>
                        <td class="main_table_body" height="100%">
	                        <br /><br />
                            <div style="margin-left:10px;">
                                <table style="padding-left: 10px;" width="100%" border="0">
                                    <tbody>
                                        <tr valign="middle">
                                        <td width="142">
                                            <% = resources.LNG_SEARCH_PUBLISHERS_BY_TITLE %> <input type="text" id="searchTermBox" runat="server" name="uname" value=""/>
                                        </td>
                                        <td width="1%">
                                            <br />
                                            <asp:linkbutton runat="server" class="search_button ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" id="searchButton" style="margin-right:0px" role="button" aria-disabled="false" Text="<span class='ui-button-icon-primary ui-icon ui-icon-search'></span><span class='ui-button-text'>{Search}</span>"/>
	                                        <input type="hidden" name="search" value="1">
                                        </td>
                                        <td>
                                            <div style="text-align: right; margin: 10px;">
                                                <%=resources.LNG_YOU_CAN_HAVE%> <span id="maxPublishersCount"><%=ActivePublishersLeft%></span> <%=resources.LNG_ACTIVE_PUBLISHERS %>
                                            </div>
                                            <a class="add_alert_button"  title="Hotkey: Alt+N" runat="server" id="addPublisherButton"  style="float:right;margin-right: 7px;" href="EditPublisher.aspx" role="button" aria-disabled="false"><%=resources.LNG_ADD_EDITOR %></a>
                                        </td>
                                        </tr>
                                    </tbody>
                                </table>	
                            </div>
			                <div style="margin-left:10px" runat="server" id="mainTableDiv">
			                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
				            <tr><td> 
					            <div style="margin-left:10px" id="topPaging" runat="server"></div>
				            </td></tr>
				            </table>
			                    <br/>
				                <asp:LinkButton style="margin-left:10px" runat="server" title="Hotkey: Delete" id="upDelete" class='delete_button' />
			                    <asp:LinkButton style="margin-left:10px" runat="server" title="" id="upEnable" class='delete_button' />
			                    <asp:LinkButton style="margin-left:10px" runat="server" title="" id="upDisable" class='delete_button' />
			                    <br/><br/>
				                <asp:Table style="padding-left: 0px;margin-left: 10px;margin-right: 10px;" runat="server" id="contentTable" Width="98.5%" Height="100%" CssClass="data_table" CellSpacing="0" CellPadding="3">
				                    <asp:TableRow CssClass="data_table_title">
				                        <asp:TableCell runat="server" id="headerSelectAll" CssClass="table_title" Width="2%" >
				                            <asp:CheckBox id="selectAll" runat="server"/>
				                        </asp:TableCell>
					                    <asp:TableCell runat="server" id="pubNameCell" CssClass="table_title" ></asp:TableCell>
					                    <asp:TableCell runat="server" id="pubPolicyCell" CssClass="table_title"></asp:TableCell>
                                        <asp:TableCell runat="server" Width="15%" id="publisherRegDate" CssClass="table_title"></asp:TableCell>
				                        <asp:TableCell runat="server" id="publisherStatusHeader" CssClass="table_title"></asp:TableCell>
                                        <asp:TableCell runat="server" Width="3%" id="actionsCell" CssClass="table_title"></asp:TableCell>
				                    </asp:TableRow>
				                </asp:Table>
			                    <br/>
				                <asp:LinkButton style="margin-left:10px" runat="server" title="Hotkey: Delete" id="bottomDelete" class='delete_button'  />
			                    <asp:LinkButton style="margin-left:10px" runat="server" title="" id="bottomEnable" class='delete_button'  />
			                    <asp:LinkButton style="margin-left:10px" runat="server" title="" id="bottomDisable" class='delete_button'  />
			                    <br/><br/>
				                <table width="100%" border="0" cellspacing="0" cellpadding="0">
				                    <tr><td> 
					                <div style="margin-left:10px" id="bottomRanging" runat="server"></div>
				                </td></tr>
				                </table>
			                </div>
    			            <div runat="server" id="NoRows">
                                <br />
                                <br />
				                <center><b><%=resources.LNG_THERE_ARE_NO_EDITORS %></b></center>
			                </div>
                            <br/><br/>
                        </td>
                    </tr>
	            </table>
            </td></tr>
        </table>
    </form>
    <div id="dialog-confirm" title="We need your confirm to proceed operation">
        <p>
            <span class="ui-icon ui-icon-alert" style="float:left; margin:12px 12px 20px 0;"></span>
            You dont have enough publisher licenses. In order to complete your request publishers with oldest login date will be disabled. Are you want to proceed?
        </p>
    </div>
</body>
</html>
