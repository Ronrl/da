<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%
Response.Expires = 0 

Set properties=Server.CreateObject("Scripting.Dictionary")

Function getProperty(propName, propDef)
	Select Case propName
	case "root_path"
		getProperty = "."
	case "server"
		getProperty = "."
	case "caption_html"
		if (Request(propName)="") then
			getProperty = propDef
		else
			getProperty = Request(propName)
		end if
	case else
		if (properties.Exists(propName)) then
			getProperty = ParseAllVars(properties(propName).attributes.getNamedItem("default").value)
		else
			if (Request(propName)="") then
				getProperty = propDef
			else
				getProperty = ParseAllVars(Request(propName))
			end if	
		end if	
	End select
End Function 

Function ParseAllVars(propStr)
	propertyVal = propStr
	st = 1 
	do while 1
		st = InStr(st, propertyVal, "%")
		if (st = 0) then Exit Do
		fn = InStr(st+1,propertyVal,"%")
		if (fn = 0) then Exit Do
		innerProp = Mid(propertyVal, st+1, fn-st-1)
		newPropVal = getProperty(innerProp,null)
		if not IsNull(newPropVal) then
			propertyVal = Left(propertyVal,st-1) + newPropVal + Mid(propertyVal,fn+1)
			st = st + Len(newPropVal)
		else
			st = fn
		end if
	loop
	ParseAllVars = propertyVal
End Function

skinId = getProperty("skin_id","")

if skinId <> "" then
	confPath = "skins/"&skinId&"/blank/conf.xml"
else
	confPath = "skins/default/blank/conf.xml"
end if

Set objXML = Server.CreateObject("Msxml2.DOMDocument.6.0")

confPath = Server.MapPath(confPath)

alertWindowName = ""
tickerWindowName = ""

set fs=Server.CreateObject("Scripting.FileSystemObject")
if fs.FileExists(confPath) then

	objXML.Load(confPath)

	If objXML.parseError.errorCode = 0 Then
	
		Set windows = objXML.selectNodes("/ALERTS/COMMANDS/ALERT/WINDOW")

		For Each window in windows
			if LCase(Replace(Replace(Replace(window.text, " ", ""),"""",""),"'","")) = "#ticker#==0" then
				alertWindowName = window.attributes.getNamedItem("name").value
			elseif LCase(Replace(Replace(Replace(window.text, " ", ""),"""",""),"'","")) = "#ticker#==1" then
				tickerWindowName = window.attributes.getNamedItem("name").value
			end if
		Next

		Set popertyElements = objXML.selectNodes("/ALERTS/SETTINGS/PROPERTY")
		For Each propertyElement in popertyElements
			name = propertyElement.attributes.getNamedItem("id").value
			if properties.Exists(name) then
				properties.Remove(name)
			end if
			properties.Add name, propertyElement
		Next
	end if
end if
Set fs=nothing
if properties.Exists("postpone_mode") then
	properties.Item("postpone_mode") = ConfEnablePostponeInPreview
else
	properties.Add "postpone_mode", ConfEnablePostponeInPreview
end if

alertHTML = getProperty("alert_html","")
alertTitle = getProperty("alert_title","")
alertWidth = getProperty("alert_width","")
alertHeight = getProperty("alert_height","")
alertFullScreen = getProperty("full_screen","")
createDate = getProperty("create_date","")
topTemplate = getProperty("top_template","")
bottomTemplate = getProperty("bottom_template","")
ticker = getProperty("ticker","")
acknowledgement = getProperty("acknowledgement","0")
captionHTML = getProperty("caption_html","")
captionHref = getProperty("caption_href","")
previewType = getProperty("type","desktop")
skinId = getProperty("skin_id","")
urgent = getProperty("urgent","")

need_question = getProperty("need_question","")
question = getProperty("question","")
question_option1 = getProperty("question_option1","")
question_option2 = getProperty("question_option2","")
need_second_question = getProperty("need_second_question","")
second_question = getProperty("second_question","")


if need_question = "1" then
	alertHTML = alertHTML & "<hr/><form>"

	alertHTML = alertHTML & HtmlEncode(question)
	alertHTML = alertHTML & "<br/>"

	alertHTML = alertHTML & "<table border='0'><tr><td style='width:10px'><input type='radio' name='answer' id='answer1' value='1'/></td><td align='left'>"
	alertHTML = alertHTML & "	<label for='answer1'>"+HtmlEncode(question_option1)+"</label>"
	alertHTML = alertHTML & "</td></tr>"

	alertHTML = alertHTML & "<tr><td style='width:10px'><input type='radio' name='answer' id='answer2' value='2'/></td><td align='left'>"
	alertHTML = alertHTML & "	<label for='answer2'>"+HtmlEncode(question_option2)+"</label>"
	alertHTML = alertHTML & "</td></tr></table>"

	if need_second_question = "1" then
		alertHTML = alertHTML & "<br/>"
		alertHTML = alertHTML & HtmlEncode(second_question)
		alertHTML = alertHTML & "<br/>"
		alertHTML = alertHTML & "<textarea cols='50' rows='5' id='custom_answer' name='custom_answer'></textarea><br/>"
	end if
	
	submitButton = getProperty("submitButton","../../../images/submit_button.gif")
	if InStr(submitButton, "/") = 0 then
		alertHTML = alertHTML & "<br/><input type=""button"""
	else
		alertHTML = alertHTML & "<br/><img"
	end if
	alertHTML = alertHTML & " src="""&submitButton&""" value="""&submitButton&""" name=""submit"" value=""Submit"" onclick='window.external.Close();'></form>"
end if

if acknowledgement = "1" then
	readitButton = getProperty("readitButton","../../../images/read_it_button.gif")
	if InStr(readitButton, "/") = 0 then
		alertHTML = alertHTML & "<input type=""button"""
	else
		alertHTML = alertHTML & "<img"
	end if
	alertHTML = alertHTML & " id=""readitButton"" class=""readitButton"" style=""position: relative; float: right; right: 50px"" src="""&readitButton&""" value="""&readitButton&""" onclick=""window.external.Close();"" /><!-- cannot close -->"
end if

alertHTML = replaceInTags(alertHTML, "(['""])(admin/)?images/upload/", "$1../../../../admin/images/upload/")
topTemplate = replaceInTags(topTemplate, "(['""])(admin/)?images/upload/", "$1../../../../admin/images/upload/")
bottomTemplate = replaceInTags(bottomTemplate, "(['""])(admin/)?images/upload/", "$1../../../../admin/images/upload/")

alertHTML = replaceInTags(alertHTML, "(['""])admin/images/(?!upload/)", "$1../../../../admin/images/")
topTemplate = replaceInTags(topTemplate, "(['""])admin/images/(?!upload/)", "$1../../../../admin/images/")
bottomTemplate = replaceInTags(bottomTemplate, "(['""])admin/images/(?!upload/)", "$1../../../../admin/images/")

alertHTML = replaceFlash(alertHTML)
alertHTML = replaceVideo(alertHTML, "preview")

if createDate = "" then
	createDate = now_db
end if

rootPath = getProperty("root_path","")

if not objXML is nothing then
	if ticker="1" then
		Set alertWindow = objXML.selectSingleNode("/ALERTS/VIEWS/WINDOW[@name='"&tickerWindowName&"']")
	else
		Set alertWindow = objXML.selectSingleNode("/ALERTS/VIEWS/WINDOW[@name='"&alertWindowName&"']")
	end if
end if

if not alertWindow is nothing then
	if alertHeight = "" or (ticker="1" and previewType <> "sms") then
		captionHeight = CLng(alertWindow.attributes.getNamedItem("height").value)
	else
		captionHeight = alertHeight
	end if

	if alertWidth = "" then
		captionWidth = CLng(alertWindow.attributes.getNamedItem("width").value)
	else
		captionWidth = alertWidth
	end if

	if alertFullScreen = "1" or alertFullScreen = "true" then
		captionPosition = "fullscreen"
	else
		captionPosition = alertWindow.attributes.getNamedItem("position").value
	end if

	if previewType = "desktop" then
		
		if skinId="{C28F6977-3254-418C-B96E-BDA43CE94FF4}" or skinId="{138F9281-0C11-4C48-983E-D4CA8076748B}" or skinId="{04F84380-B31D-4167-8B83-C6642E3D25A8}" then
			captionBackground = "transparent"
		else
			captionBackground = "white"
		end if
		if captionHref="" then
			captionHref = ParseAllVars(alertWindow.attributes.getNamedItem("captionhref").value)
		end if
		customJS = ParseAllVars(alertWindow.attributes.getNamedItem("customjs").value)

		bodyTopMargin =  CLng(alertWindow.attributes.getNamedItem("topmargin").value)
		bodyBottomMargin = CLng(alertWindow.attributes.getNamedItem("bottommargin").value)
		bodyLeftMargin =  CLng(alertWindow.attributes.getNamedItem("leftmargin").value)
		bodyRightMargin = CLng(alertWindow.attributes.getNamedItem("rightmargin").value)
		
	elseif (previewType = "sms") then
		
		captionBackground = "transparent"
		captionHTML = "<html>" &_
			"<head></head><body style='background:transparent; padding:0px; margin:0px'>" &_
			"<div class='png' style='background: url(../../../images/mobile_phone.png);width:100%; height:100%; position:absolute; left:0px; top:0px'></div>" &_
			"<div class='png' style='position:absolute; top:12px; right:4px; width:20px; height:20px; background:url(../../../images/close_sms_preview.png); cursor:pointer; cursor:hand' onclick='window.external.Close();'/></div>" &_
			"</body></html>"
		bodyTopMargin = 38
		bodyBottomMargin = 39
		bodyLeftMargin = 6
		bodyRightMargin = 6
		alertHTML = DeleteTags(alertHTML)
		
	end if

	bodyHeight = captionHeight - bodyTopMargin - bodyBottomMargin'bodyHeight
	bodyWidth = captionWidth - bodyLeftMargin - bodyRightMargin
end if

Set DTFormat = Conn.Execute("SELECT val FROM settings WHERE name='ConfDateFormat'")
if not DTFormat.EOF then
	cr_day = Mid(createDate, 1, 2)
	cr_month = Mid(createDate, 4, 2)
	cr_year = Mid(createDate, 7, 4)
	tmp = Mid(createDate, 11, 9)
	select case DTFormat("val")
		case "dd/MM/yyyy"
			createDate = cr_day&"/"&cr_month&"/"&cr_year&tmp
		case "yyyy/MM/dd"
			createDate = cr_year&"/"&cr_month&"/"&cr_day&tmp
		case "MM/dd/yyyy"
			createDate = cr_month&"/"&cr_day&"/"&cr_year&tmp
		case "yyyy/dd/MM"
			createDate = cr_year&"/"&cr_day&"/"&cr_month&tmp
		case "MM/yyyy/dd"
			createDate = cr_month&"/"&cr_year&"/"&cr_day&tmp
		case "dd/yyyy/MM"
			createDate = cr_day&"/"&cr_year&"/"&cr_month&tmp
		case "dd-MM-yyyy"
			createDate = cr_day&"-"&cr_month&"-"&cr_year&tmp
		case "yyyy-MM-dd"
			createDate = cr_year&"-"&cr_month&"-"&cr_day&tmp
		case "MM-dd-yyyy"
			createDate = cr_month&"-"&cr_day&"-"&cr_year&tmp
		case "yyyy-dd-MM"
			createDate = cr_year&"-"&cr_day&"-"&cr_month&tmp
		case "MM-yyyy-dd"
			createDate = cr_month&"-"&cr_year&"-"&cr_day&tmp
		case "dd-yyyy-MM"
			createDate = cr_day&"-"&cr_year&"-"&cr_month&tmp
	end select
end if

ext_props = "{ "
	propStr=""
	propKeys=properties.Keys

	for i=0 to properties.Count-1
		if VarType(properties.Item(propKeys(i))) = 8 then
			ext_props =  ext_props & """"&propKeys(i)&"""" & " : { ""value"" : """ & jsEncode(properties.Item(propKeys(i))) & """, ""cnst"" : 1},"
		elseif VarType(properties.Item(propKeys(i))) = 9 then
			Set prop = properties.Item(propKeys(i))
			propConst = 0
			if (not prop.attributes.getNamedItem("const") is nothing) then
				propConst = prop.attributes.getNamedItem("const").value
			end if

			ext_props =  ext_props & """"&propKeys(i)&"""" & " : { ""value"" : """ & jsEncode(prop.attributes.getNamedItem("default").value) & """, ""cnst"" : "&propConst&"},"
		end if
	next
	
	ext_props = ext_props & """root_path"": { ""value"": """&rootPath&""", ""cnst"":0}, "
	ext_props = ext_props & """create_date"": { ""value"": """&createDate&""", ""cnst"":0}, "
	ext_props = ext_props & """title"": { ""value"": """&jsEncode(alertTitle)&""", ""cnst"":0}, "
	ext_props = ext_props & """top_template"": { ""value"": """&jsEncode(topTemplate)&""", ""cnst"":0}, "
	ext_props = ext_props & """bottom_template"": { ""value"": """&jsEncode(bottomTemplate)&""", ""cnst"":0}, "
	ext_props = ext_props & """acknowledgement"": { ""value"": """&acknowledgement&""", ""cnst"":0},"
	ext_props = ext_props & """topmargin"": { ""value"": """&bodyTopMargin&""", ""cnst"":0},"
	ext_props = ext_props & """bottommargin"": { ""value"": """&bodyBottomMargin&""", ""cnst"":0},"
	ext_props = ext_props & """leftmargin"": { ""value"": """&bodyLeftMargin&""", ""cnst"":0},"
	ext_props = ext_props & """rightmargin"": { ""value"": """&bodyRightMargin&""", ""cnst"":0},"
	ext_props = ext_props & """server"": { ""value"": """&jsEncode(alerts_folder)&""", ""cnst"":0},"
	ext_props = ext_props & """skin_id"": { ""value"": """&jsEncode(skinId)&""", ""cnst"":0},"
	ext_props = ext_props & """urgent"": { ""value"": """&jsEncode(urgent)&""", ""cnst"":0},"
	ext_props = ext_props & """isHtml"": { ""value"": ""1"", ""cnst"":0},"
	ext_props = ext_props & """position"": { ""value"": """&jsEncode(captionPosition)&""", ""cnst"":0} "
ext_props = ext_props & " }"
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<script type="text/JavaScript" language="JavaScript" src="jscripts/jquery/jquery.min.js"></script>
<script type="text/JavaScript" language="JavaScript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script type="text/JavaScript" language="JavaScript" src="jscripts/json2.js"></script>
<style>
html {
	margin: 0;
	padding: 0;
}
body {
	margin: 0;
	padding: 0;
}
</style>

<script type="text/JavaScript" language="JavaScript">
var captionReady = false;
var bodyReady = false;

var eventsStack = [];

bodyStack = [];
captionStack = [];

function close()
{
	window.parent.closeAlertPreview();
}

function callFromStackInWindow(stack,window)
{
	while(stack.length > 0)
	{
		var args = stack.shift();
		callInWindow(window,args);
	}
}

function callInWindow(window,args)
{
	var name = args[0];
	args = Array.prototype.slice.call(args);
	args.shift();
	var func = window[name];
	if(typeof(func) == 'function')
	{
		func.apply(func, args);
	}
}

function callInCaption()
{
	if (captionReady)
	{
		callInWindow(document.getElementById("caption_frame").contentWindow,arguments)
	}
	else
	{
		captionStack.push(arguments);
	}
}
function callInBody()
{
	if (bodyReady)
	{
		callInWindow(document.getElementById("body_frame").contentWindow,arguments)
	}
	else
	{
		bodyStack.push(arguments);
	}
}

function captionOnLoad(e)
{
	if (document.getElementById("caption_frame").contentWindow.$)
	{
		captionReady = true;
		callFromStackInWindow(captionStack,document.getElementById("caption_frame").contentWindow);
		triggerEvent('captionReady');
	}
}

function bodyOnLoad()
{
	if (document.getElementById("body_frame").contentWindow.$)
	{
		bodyReady = true;
		callFromStackInWindow(bodyStack,document.getElementById("body_frame").contentWindow);
		triggerEvent('bodyReady');
	}
}

$(document).ready(function(){
	window.parent.setPreviewSize({width:"<%=captionWidth%>",height:"<%=captionHeight%>"});
	window.parent.openAlertPreview();

	$("#caption_post_form").submit();
	$("#body_post_form").submit();
});


function triggerEvent(eventName)
{
	for (var i=0; i<eventsStack.length; i++)
	{
		if (eventsStack[i].name == eventName)
		{	
			eventsStack[i].callback();
			eventsStack.splice(i,1);
			i--;
		}
	}
}

function addPreviewEventListener(eventName,callback)
{
	if (eventName == 'captionReady' && captionReady)
	{	
		callback();
	}
	else if (eventName == 'bodyReady' && captionReady)
	{
		callback();
	}
	else
	{
		eventsStack.push({name:eventName, callback:callback});
	}
}

</script>
</head>
<body style="height:<%=captionHeight%>px; width:<%=captionWidth%>px; overflow: hidden; background:transparent">
	<iframe scrolling=no onload="captionOnLoad()" allowtransparency="true" frameBorder="0" name="caption_frame" id="caption_frame" style="height:<%=captionHeight%>px; width:<%=captionWidth%>px; margin:0px; padding:0px; border:0px; background: <%=captionBackground%>"></iframe>
	<iframe onload="bodyOnLoad()" allowtransparency="false" frameBorder="0" name="body_frame" id="body_frame" style="height:<%=bodyHeight%>px; width:<%=bodyWidth%>px; position: absolute; top:<%=bodyTopMargin%>px; left:<%=bodyLeftMargin%>px; margin:0px; padding:0px; border:0px;"></iframe>
	<form style="margin:0px; width:0px; height:0px; padding:0px" id="caption_post_form" action="preview_caption.asp" method="post" target="caption_frame">
		<input type="hidden" id="ext_props_caption" name="ext_props"  value="<%=HtmlEncode(ext_props) %>"/>
		<input type="hidden" id="caption_href" name="caption_href" value="<%=captionHref%>"/>
		<input type="hidden" id="skin_id_caption" name="skin_id" value="<%=skinId%>"/>
		<input type="hidden" id="caption_href" name="caption_html" value="<%=HtmlEncode(HtmlEncode(captionHTML))%>"/>
	</form>
	<form style="margin:0px; width:0px; height:0px; padding:0px" id="body_post_form" action="preview_body.asp" method="post" target="body_frame">
		<input type="hidden" id="ext_props_body" name="ext_props" value="<%=HtmlEncode(ext_props) %>"/>
		<input type="hidden" id="custom_js" name="custom_js" value="<%=HtmlEncode(customJS) %>"/>
		<input type="hidden" id="alert_html" name="alert_html" value="<%=HtmlEncode(HtmlEncode(alertHTML)) %>"/>
		<input type="hidden" id="skin_id_body" name="skin_id" value="<%=skinId%>"/>
	</form>
</body>
</html>
<!-- #include file="db_conn_close.asp" -->