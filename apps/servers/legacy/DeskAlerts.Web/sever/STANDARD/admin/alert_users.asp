﻿<% 
Response.Expires = 0
Response.Expiresabsolute = Now() - 1 
Response.AddHeader "pragma","no-cache" 
Response.AddHeader "cache-control","private" 
Response.CacheControl = "no-cache" 
%>
<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%

check_session()

uid = Session("uid")
if (uid <> "") then

	mifTreeFolder = "mif_tree"

	alertId = Request("id")
	Dim recurrence
    recurrence = Request("recurrence")
    SQL = "SELECT schedule, sms, email, desktop, text_to_call,device FROM alerts WHERE id=" & Replace(alertId, ",", " OR id =")
	'response.write SQL
	Set RS = Conn.Execute(SQL)
	do while not RS.EOF 
		schedule = RS("schedule")
		sms = RS("sms")
		email = RS("email")
		desktop = RS("desktop")
		text_to_call = RS("text_to_call")
		device=RS("device")
		RS.movenext
		
	loop
	showCheckbox = 1

	if Request("instant")="1" then
		instant_message = 1
	else
		instant_message = 0
	end if
	
	if Request("autosubmit")="1" then
		autosubmit = 1
	else
		autosubmit = 0
	end if

	title=Request("title")
	if title="" then title = LNG_ADD_ALERT

	header=Request("header")
	if header="" then header = LNG_ALERTS
	
	if instant_message=1 then header = LNG_INSTANT_MESSAGES end if

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>DeskAlerts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<link rel="stylesheet" href="<%Response.write mifTreeFolder%>/style.css" type="text/css" />
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
<link href="css/style9.css" rel="stylesheet" type="text/css">
<%

'modify input  form using javascript
	only_personal=0
	ip_groups_arr = Array ("","","","","","","")
	Set RS = Conn.Execute("SELECT id, only_personal FROM users WHERE id="&uid&" AND role='E'")
	if(Not RS.EOF) then
		gid = 1
		Set rs_policy = Conn.Execute("SELECT policy.id as id, type FROM policy INNER JOIN policy_editor ON policy.id=policy_editor.policy_id WHERE editor_id=" & uid)
		if(Not rs_policy.EOF) then
			if(rs_policy("type")="A") then
				gid = 0
			end if
		end if
		if gid=1 then
			editor_id=RS("id")
			only_personal=RS("only_personal")
			policy_ids=""
			policy_ids=editorGetPolicyIds(editor_id)
			group_ids=""
			group_ids=editorGetGroupIds(policy_ids)
			group_ids=replace(group_ids, "group_id", "users_groups.group_id")
			ou_ids=""
			ou_ids=editorGetOUIds(policy_ids)
			ou_ids=replace(ou_ids, "ou_id", "OU_User.Id_OU")
			ip_groups_arr = editorGetPolicyList(policy_ids, "ip_groups_val")
		end if
	else
		gid = 0
	end if
	RS.Close
	
	type2 = "0"
	dup_id = Request("dup_id")
	if dup_id <> "" then
		Set RS = Conn.Execute("SELECT type2 FROM alerts WHERE id=" & dup_id)
		if not RS.EOF then
			type2 = RS("type2")
		end if
	end if

%>
	
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/mootools.js"></script>	
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Node.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Hover.js"></script>	
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Selection.js"></script>	
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Load.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Draw.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.KeyNav.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Sort.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Transform.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Drag.js"></script>	
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Drag.Element.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Checkbox.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Rename.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Row.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.mootools-patch.js"></script>
	<link rel="stylesheet" href="<%Response.write mifTreeFolder%>/style.css" type="text/css" />
	
	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
	
	<script language="javascript" type="text/javascript">



	    $.noConflict();
	    (function($) {
	        $(document).ready(function() {
	            $(".send_button").button();
	            if ("<%=autosubmit %>" == "1") {
	                $("#im_description").dialog();
	                setTimeout(function() { if (my_sub(2)) { document.frm.submit(); } }, 1000);
	            }
			
	            <%  if( Request("webalert") = "1" ) then %>
                     $("#selection_table").css('display', 'none');	

	            <% end if %>
                
                $( document ).tooltip({
	                items: "img[title],a[title],[custom-tooltip]",
	                content: function() {
                        var element = $( this );
                        if ( element.is( "[custom-tooltip]" ) ) {
                          return "<p style='width:350px'><%=LNG_CUSTOMGROUPS_HINT_DESC%></p><img class='img-hint' src='images/audience_hint.png' width='350' height='350'/>";
	            }
                        if ( element.is( "a[title]" ) ) {
                          return element.attr( "title" );
	            }
                        if ( element.is( "img[title]" ) ) {
                          return element.attr( "title" );
	            }
	            },
	                position: {
	                my: "center bottom-20",
	                at: "center top",
	                using: function( position, feedback ) {
                      $( this ).css( position );
                      $( "<div>" )
                     .addClass( "arrow" )
                     .addClass( feedback.vertical )
                     .addClass( feedback.horizontal )
                     .appendTo( this );
	            }
	            }
	            });
	        });
	    })(jQuery);


	</script>
	<script type="text/javascript">
	    function send_to_users()
	    {
	        res = getCheckedOus();
	        idSequence = "";
	        idSequence_closed = "";
	        if( res )
	        {
	            for(i=0;i<res.length;i++)
	            {
	                if(res[i].type != "organization" && res[i].type != "DOMAIN")
	                {
	                    if(res[i].child>0){
	                        idSequence += res[i].id.toString() + " ";
	                    }
	                    else{
	                        idSequence_closed += res[i].id.toString() + " ";
	                    }
	                }
	            }
	        }

	        document.getElementById("ous_id_sequence").value=idSequence;
	        document.getElementById("ous_id_sequence_closed").value=idSequence_closed;
	        return true;
	    }

	    function check_all_ous()
	    {
	        var formInputs = document.getElementsByTagName("input");
	        var checked = document.getElementById("select_all_ous").checked;
	        for(var i=0; i<formInputs.length; i++){
	            if(formInputs[i].name=="ed_ous" || formInputs[i].name=="ous_id_sequence_closed"){
	                formInputs[i].checked = checked;
	            }
	        }
	    }

	    function check_list()
	    {
	        var formInputs = document.getElementsByTagName("input");
	        for(var i=0; i<formInputs.length; i++){
	            if(formInputs[i].name=="added_user"){
	                return true;
	            }
	            if(formInputs[i].name=="ed_ous" && formInputs[i].checked==true){
	                return true;
	            }
				
	        }
	        alert("<%=LNG_PLEASE_SELECT_RECIPIENTS_BEFORE_YOU_PROCEED %>.");
	        return false;
	    }
	

	    function changeObjectType(clear)
	    {

	        var webPlugin = '<%=Request("webalert")%>';
				
				
	        if(typeof tree != 'undefined' && tree.root)
	        {
	            tree.select(tree.root);
	            $('ou_ui').style.display="";
	            document.getElementById("obj_cnt").innerHTML = "0";
	            <% controlOUList policy_ids, showCheckbox %>
	            }
	        var content_file="objects.asp";
	        if($('obj_type').value=="R"){
	            content_file="objects.asp?sc=<%=showCheckbox %>&sms=<%=sms%>&email=<%=email%>&desktop=<%=desktop%>&text_to_call=<%=text_to_call%>&device=<%=device%>"
	            $('send_type').value="recepients";
	            $('upper_send').style.display="";
	            $('broadcast_note').style.display="none";
	        }
	        if($('obj_type').value=="B"){
	            $('send_type').value="broadcast";
	            $('broadcast_note').style.display="";
	        }
	        if($('obj_type').value=="I"){
	            content_file="ip_groups.asp?sc=<%=showCheckbox %>"
	            $('send_type').value="iprange";
	            $('upper_send').style.display="";
	            $('tree_td').style.display="none";
	            $('broadcast_note').style.display="none";
	        }
	        if($('obj_type').value=="0" || $('obj_type').value=="B"){
	            $('ou_ui').style.display="none";
	            $('upper_send').style.display="none";
	        }
		
	        if($('obj_type').value=="U")
	        {
	            content_file = "invite.asp";
	            $('upper_send').style.display="";
	            $('send_type').value="unreg";
	            $('tree_td').style.display="none";
	        }
	        if(webPlugin == "1")
	        {
	            $('ou_ui').style.display="";
	            content_file="objects.asp?sc=0&sms=<%=sms%>&webalert=1";
	            $('tree_td').style.display="none";
	            $('send_type').value="recepients";
	            $('obj_type').value="R"
	        }
	        $('content_iframe').src=content_file;
	        if(clear)
	        {
	            $('objects_div').innerHTML = "";
	        }
	        updateObjectsCount(!clear);
	    }
        
	    function my_sub(i) {
	        var val;
	        var box;

	        box = document.getElementById('obj_type');
	        val = box.options[box.selectedIndex].value;	
	        send_to_users();

	        var webPlugin = '<%=Request("webalert") %>'
		
	        if(document.getElementById("ous_id_sequence").value.length>0 || document.getElementById("ous_id_sequence_closed").value.length>0) return true;


	        if(val == "0" && webPlugin != "1") {
	            alert('<%=LNG_PLEASE_CHOOSE_THE_TYPE %>');
	            return false;
	        } else if(val == "I") {
	            var obj_cnt = document.getElementById('obj_cnt');

	            if (obj_cnt.textContent == "0") {
	                alert ("You should select IP group!");
	                return false;
	            }

	            window.parent.parent.document.getElementById('menu').src = window.parent.parent.document.getElementById('menu').src;
	            return true;
	        } else if(val == "U") {
	            window.parent.parent.document.getElementById('menu').src = window.parent.parent.document.getElementById('menu').src;
	        } else {
	            window.parent.parent.document.getElementById('menu').src = window.parent.parent.document.getElementById('menu').src;
	            document.frm.sub1.value = i;
	            if (val != "B") //not broadcast
	                return check_list();
	            return true;
	        }
	    }
	</script>
</head>
<script language="javascript">
    function LINKCLICK() {
        var yesno = confirm("Are you sure you want to delete this user?", "");
        if (yesno == false) return false;
        return true;
    }
</script>


<body style="margin:0px" class="body" onload="changeObjectType(false)">
<div id="im_description" style="display:<%if autosubmit=1 then Response.Write("block") else Response.Write("none") end if%>; margin:auto; height:100%; width:100%">Selecting message recipients...</div>
<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%" <% if autosubmit = 1 then Response.Write (" style='display:none' ") end if %>>
	<tr>
	<td>
	<table width="100%" height="100%"cellspacing="0" cellpadding="0" class="main_table">
		<tr>
		<td width="100%" height="31" class="main_table_title"><a href="alert_users.asp" class="header_title"><%=header %></a></td>
		</tr>
		<tr>
		<td class="main_table_body" height="100%" valign="top">
		
		<div style="margin:10px 10px 10px 10px;">
		<div align="right"></div>
		<span class="work_header"><%=title %></span>
		<br><br>
<div><%=LNG_STEP %> <strong><% if (Request("cur_step")<>"") then Response.write Request("cur_step") else Response.write "2" end if %></strong> <%=LNG_OF %> <% if (Request("cur_step")<>"") then Response.write " "&Request("steps_count") else Response.write " 2" end if %>: <%=LNG_SELECT_RECIPIENTS %>:</div>

<% if schedule <> "1" and (sms = "1" or email = "1" or text_to_call="1") then %>
    <br><font color="red"><%=LNG_SMS_ATTENTION %>.</font>
<% end if %>

<form name="frm" action="change_users.asp" method="POST">
<input type="hidden" name="is_scheduled" value="<%=recurrence %>">
<input type="hidden" name="is_sended" value="true">
<input type="hidden" name="return_page" value="<%=Request("return_page") %>">
<input type="hidden" name="instant" value="<%=instant_message %>">
<input type="hidden" name="sub1" value="">
<input type="hidden" name="campaign_id" value="<%=Request("campaign_id")%>">
<input type="hidden" name="change" value="<%=Request("change")%>" />
<input type="hidden" name="shouldApprove" value="<%=request("shouldApprove") %>" />
<input type="hidden" name="rejectedEdit" value="<%=request("rejectedEdit") %>" />
<input type="hidden" name="deviceType" value="<%=device %>" />



<table cellpadding="0" cellspacing="0" border="0" width="100%">
<tr><td>


	<table id="selection_table" cellpadding="4" cellspacing="4" border="0">
	<tr><td><%=LNG_ALERT_TYPE %>:</td><td><select name="obj_type" id="obj_type" onChange="changeObjectType(true)">

	<option value="0"<% if type2 = "0" then Response.Write " selected" %>>-- <%=LNG_CHOOSE %> --</option>
	<%
        gsendtoall = 1
        set rs_policy = Conn.Execute("SELECT p.id, p.type, pu.user_id FROM policy p INNER JOIN policy_editor pe ON p.id=pe.policy_id LEFT JOIN policy_user pu ON pu.policy_id = p.id AND pu.user_id = 0 WHERE pe.editor_id=" & uid)
		if not rs_policy.EOF then

			if  IsNull(rs_policy("user_id")) then
				'gid = 0
				gsendtoall = 0
			end if
		end if


	if  ConfDisableBroadcast <> 1 and gsendtoall =1 then
	%>
	<option value="B"<% if type2 = "B" then Response.Write " selected" %>><%=LNG_BROADCAST %></option>
	<%
	end if
	%>

	<option value="R"<% if type2 = "R" then Response.Write " selected" %>><%=LNG_SELECT_RECIPIENTS %></option>

	<% if (ConfEnableIPRanges = 1 AND (gid=0 OR ip_groups_arr(3)<>"")) then %>
	<option value="I"<% if type2 = "I" then Response.Write " selected" %>><%=LNG_IP_GROUPS %></option>
	<% end if %>
	
	</select></td></tr>
	</table>
	


</td><td align="right">


<%
    Set rs = Conn.execute("SELECT val FROM settings WHERE name = 'ConfAllowApprove'") 
    
    
    sendText = LNG_APPROVE_SUBMIT
    
    if (rs("val") = 1 and APPROVE = 1)  then
    
        if( Request("approve") = "1" or gid = 0) then
        
            if instant_message = 0 then
             sendText = LNG_SEND
            else
              sendText = LNG_SAVE
            end if
        else
         sendText = LNG_APPROVE_SUBMIT
        end if
     else
         if instant_message=1 or ( Request("campaign_id") <> "-1" and Request("campaign_id") <> "" ) then
          sendText = LNG_SAVE
        else 
             sendText = LNG_SEND
         end if    
     end if


 
%>
	<input id="send_it" class="send_button" type="submit" onClick="javascript:  return my_sub(2);" value="<%=Response.Write(sendText) %>" />
	
</td></tr>
<tr>
	<td>
		<div id="broadcast_note" style="display:none">
		<p style="color:#888888;"><%=LNG_BROADCAST_HINT_1 %></p>
		<ol>
			<li style="color:#888888;"><%=LNG_BROADCAST_HINT_2 %></li>
			<li style="color:#888888;"><%=LNG_BROADCAST_HINT_3 %></li>
		</ol>
		</div>
	</td>
</tr>
</table>
<div id="ou_ui" style="display: none">
<!-- #include file="ou_ui_include.asp" -->
</div>

<script language="javascript">
    <% controlOUList policy_ids, showCheckbox %>
	
     
</script>


<input type="hidden" name="send_type" id="send_type" value="0"/>


<%
  Response.Write "<input type='hidden' name='alert_id' value='"& alertId &"'/>"
  Response.Write "<input type='hidden' name='current_alert_id' value='"& dup_id &"'/>"
%>

<input type="hidden" id="ous_id_sequence" name="ous_id_sequence" value=""/>
<input type="hidden" id="ous_id_sequence_closed" name="ous_id_sequence_closed" value=""/>
<div align="right" id="upper_send" style="display:none"><input type="submit" class="send_button" onClick="javascript: return my_sub(2);" value="<%=Response.Write(sendText) %>"></div>
</form>
		</td>
		</tr>
	</table>
	
	</td>
	</tr>
</table>
</body>
</html>
<%
else
	Response.Redirect "index.asp"
end if
%> 
<!-- #include file="db_conn_close.asp" -->