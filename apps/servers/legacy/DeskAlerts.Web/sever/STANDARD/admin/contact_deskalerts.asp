﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<!-- #include file="md5.asp" -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>DeskAlerts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/style.css" rel="stylesheet" type="text/css">

<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script language="javascript" type="text/javascript" src="functions.js"></script>
<script language="javascript" type="text/javascript">
(function($) {
	$(document).ready(function() {
		$("#contact_us_table").tabs({
			activate: function(event, ui) {
				var href = $("#contact_us_table div.ui-tabs-panel:visible iframe").get(0).src;
				var chunks = href.split("/");
				LoadHelp(chunks[chunks.length - 1]);
			},
			create: function(event, ui) {
				var href = $("#contact_us_table div.ui-tabs-panel:visible iframe").get(0).src;
				var chunks = href.split("/");
				LoadHelp(chunks[chunks.length - 1]);
			}
		});
		resizeAllframes();

		function LoadHelp(href) {
			var about = pageTheme(href, null);
			window.top.frames["main"].document.getElementById("helpIframe").src = "help.asp?about=" + about;
		}
	});
})(jQuery);

function resizeAllframes() {
	var height = window.frameElement.scrollHeight - 150;
	$('#quote_frame').css('height', height);
	$('#demo_frame').css('height', height);
}

function resizeIframe(id) {
	var iframeDoc = document.getElementById(id);
	iframeDoc.height = window.frameElement.scrollHeight - 100;
	var h;
	if (iframeDoc.contentDocument) {
		h = iframeDoc.contentDocument.documentElement.scrollHeight; //FF, Opera, and Chrome
		h = iframeDoc.contentDocument.body.scrollHeight; //ie 8-9-10
		if (iframeDoc.height < h) iframeDoc.height = h;
	}
	else {
		h = iframeDoc.contentWindow.document.body.scrollHeight; //IE6, IE7 and Chrome
		if (iframeDoc.height < h) iframeDoc.height = h;
	}
}
</script>
<script language="javascript">

</script>
</head>

<body style="margin:0px" class="body">

<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	<tr>
	<td>
	<table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
		<tr>
		<td width="100%" height="31" class="main_table_title"><a href="" class="header_title">Contact DeskAlerts</a></td>
		</tr>
		<tr>
		<td class="main_table_body" height="100%">
		<div id="contact_us_table" style="padding:0px; border:none;">
			<ul>
				<li><a href="#quote" onclick="resizeIframe('quote_frame');">Request Free Quote</a></li>
				<li><a href="#demo"  onclick="resizeIframe('demo_frame');" >Request Free Trial</a></li>
			</ul>
			<div id="quote">
				<iframe src="request_quote.asp" frameborder="no" style="width:100%;padding:0px;height: 761px;"  id="quote_frame"></iframe>
			</div>
			<div id="demo">
				<iframe src="request_demo.asp" frameborder="no" style="width:100%;padding:0px;height: 761px;" id="demo_frame"></iframe>
			</div>
		 <img src="images/inv.gif" alt="" width="1" height="100%" border="0">
		</div>
		</td>
		</tr>
	</table>
	</td>
	</tr>
</table>

</body>
</html>

</body>
</html>
<!-- #include file="db_conn_close.asp" -->