<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%
check_session()
%>
<!-- #include file="Common.asp" -->
<!-- #include file="header.asp" -->
<!-- #include file="footer.asp" -->
<!-- #include file="libs/FusionCharts.asp" -->
<!-- #include file="libs/FC_Colors.asp" -->
<%
'On Error Resume Next
' index CustomIncludes End
'-------------------------------

'===============================
' Save Page and File Name available into variables
'-------------------------------
Dim sFileName
Dim sTemplateFileName
Dim sPaginateFileName
Dim intSessionUserId

sFileName = "statistics_da_surveys_view_details.asp"
if Request("widget")=1 then
	widget = 1
	sTemplateFileName = "statistics_da_surveys_view_details_widget.html"
else
	widget = 0
	sTemplateFileName = "statistics_da_surveys_view_details.html"
end if
sPaginateFileName = "paginate.html"


SetVar "widget", widget
'===============================

intSessionUserId = Session("uid")
uid=Session("uid")

if (intSessionUserId <> "") then

' index Show begin

intSurveyId=Clng(request("id"))
intQuestionNumber=Clng(request("q_num"))
if(intQuestionNumber=0) then
	intQuestionNumber=1
end if

'===============================
' Display page
'-------------------------------
' Load HTML template for this page
'-------------------------------
LoadTemplate alerts_dir & "\" & templateFolder & "\" & sTemplateFileName, "main"
SetVar "DListSurveyAnswers", ""
'-------------------------------
' Load HTML template of Header and Footer
'-------------------------------
LoadTemplate sHeaderFileName, "Header"
LoadTemplate sFooterFileName, "Footer"
'-------------------------------
SetVar "default_lng", default_lng
SetVar "LNG_TITLE", LNG_TITLE
SetVar "LNG_DATE", LNG_DATE
SetVar "LNG_TOTAL", LNG_TOTAL
SetVar "LNG_EXPORT_TO_CSV", LNG_EXPORT_TO_CSV
SetVar "LNG_SURVEY_RESULTS", LNG_SURVEY_RESULTS
SetVar "LNG_QUESTION", LNG_QUESTION
SetVar "LNG_EXPORT_THIS_QUESTION_TO_CSV", LNG_EXPORT_THIS_QUESTION_TO_CSV
SetVar "LNG_EXPORT_ALL_QUESTIONS_TO_CSV", LNG_EXPORT_ALL_QUESTIONS_TO_CSV
SetVar "LNG_NEXT_QUESTION", LNG_NEXT_QUESTION
SetVar "LNG_PREVIOUS_QUESTION", LNG_PREVIOUS_QUESTION
SetVar "questionNumber", intQuestionNumber
PageName(LNG_SURVEY_DETAILS)
'editor ---
	Set RS = Conn.Execute("SELECT id FROM users WHERE id="&uid&" AND role='E'")
	if(Not RS.EOF) then
		gid = 1
		Set rs_policy = Conn.Execute("SELECT type FROM policy INNER JOIN policy_editor ON policy.id=policy_editor.policy_id WHERE editor_id=" & uid)
		if(Not rs_policy.EOF) then
			if(rs_policy("type")="A") then
				gid = 0
			end if
		end if
	else
		gid = 0
	end if
	RS.Close
'--------

'	from_date=request("from_date")
'	to_date=request("to_date")

	Set RS = Conn.Execute("SELECT s.id, s.name, s.type, s.create_date, s.sender_id, a.title as title FROM surveys_main as s INNER JOIN alerts as a ON a.id = s.sender_id WHERE s.id=" & intSurveyId)
	if(Not RS.EOF) then

		strSurveyDate=RS("create_date")
		strSurveyTitle = RS("title")
		intAlertId = RS("sender_id")

'strip html tags from the downloading file
      FUNCTION stripHTML(strHTML)
    Dim objRegExp, strOutput, tempStr
    Set objRegExp = New Regexp
    objRegExp.IgnoreCase = True
    objRegExp.Global = True
    objRegExp.Pattern = "<(.|n)+?>"
    'Replace all HTML tag matches with the empty string
    strOutput = objRegExp.Replace(strHTML, "")
    'Replace all < and > with &lt; and &gt;
    strOutput = Replace(strOutput, "<", "&lt;")
    strOutput = Replace(strOutput, ">", "&gt;")
    stripHTML = strOutput    'Return the value of strOutput
    Set objRegExp = Nothing
       END FUNCTION

'show alerts statistics ---
  Set RSUsers = Conn.Execute("SELECT COUNT(DISTINCT id) as mycnt FROM users WHERE (reg_date <= '" & strSurveyDate & "'OR reg_date IS NULL) and role = 'U' AND  ( client_version != 'web client' OR client_version is NULL )")
  if(not RSUsers.EOF) then 
	users_cnt=RSUsers("mycnt") 
  end if
			intSentSurveys=0
			if(RS("type")="R") then 

				group_ids=""
				groupFlag=0
				group_cnt=0
				groupAlertId=intAlertId
				group_ids=""
				groupFlag=0
				Set RSGroupUsers = Conn.Execute("SELECT alerts_sent_group.group_id as group_id FROM alerts_sent_group INNER JOIN surveys_main ON surveys_main.sender_id=alerts_sent_group.alert_id WHERE alerts_sent_group.alert_id=" & groupAlertId)
				Do While not RSGroupUsers.EOF
					group_id=RSGroupUsers("group_id")
					if(groupFlag=1) then
						group_ids=group_ids & " OR " & " group_id=" & group_id 
					else
						group_ids=group_ids & " group_id=" & group_id 
					end if
					groupFlag=1
					RSGroupUsers.MoveNext	
				loop
				group_ids=replace(group_ids, "group_id", "users_groups.group_id")
				if(group_ids<>"") then

				Set RSUsers = Conn.Execute("SELECT COUNT(DISTINCT user_id) as mycnt FROM users INNER JOIN users_groups ON users.id=users_groups.user_id WHERE ("&group_ids&") AND (reg_date <= '" & strSurveyDate & "'OR reg_date IS NULL)")
				if(not RSUsers.EOF) then 
					groupusers_cnt=RSUsers("mycnt") 
					group_cnt=group_cnt+groupusers_cnt
				end if
				end if
				intSentSurveys=group_cnt
			end if

			if(RS("type")="B") then 
			  intSentSurveys=users_cnt
			end if

			if(RS("type")="R") then 
				Set RSPersonal = Conn.Execute("SELECT COUNT(DISTINCT user_id) as mycnt FROM alerts_sent_stat WHERE alert_id="&intAlertId)
				if(not RSPersonal.EOF) then 
					  intSentSurveys=RSPersonal("mycnt")+group_cnt
				end if
			end if

			Set RSPersonalGroup = Conn.Execute("SELECT COUNT(DISTINCT alerts_sent_stat.user_id) as mycnt FROM alerts_sent_stat INNER JOIN alerts_sent_group ON alerts_sent_stat.alert_id=alerts_sent_group.alert_id INNER JOIN users_groups ON alerts_sent_stat.user_id = users_groups.user_id WHERE alerts_sent_stat.alert_id="&intAlertId)
			if(not RSPersonalGroup.EOF) then 
				  intSentSurveys=intSentSurveys - RSPersonalGroup("mycnt")
			end if


  intReceivedSurveys=0
  intVotedSurveys=0

  Set RS7 = Conn.Execute("SELECT COUNT(DISTINCT user_id) as mycnt, SUM(vote) as vote_cnt FROM [alerts_received] WHERE alert_id=" & intAlertId)
  if(not RS7.EOF) then	
	intReceivedSurveys=RS7("mycnt") 
	intVotedSurveys=RS7("vote_cnt") 
  end if

  if(IsNull(intVotedSurveys)) then intVotedSurveys=0 end if
  if(IsNull(intReceivedSurveys)) then intReceivedSurveys=0 end if

intDoesntRec = intSentSurveys-intReceivedSurveys
if(intDoesntRec<0) then 
	intDoesntRec=0 
end if
intDoesntVote = intReceivedSurveys-intVotedSurveys
if(intDoesntVote<0) then
	intDoesntVote = 0
end if



        surveysVal1Color="3B6392"
	surveysVal2Color="A44441"
	surveysVal3Color="9ABA59"
	surveysVal4Color="9ABA59"

        surveysVal1Name=ToHTML(LNG_RECEIVED)
	surveysVal2Name=ToHTML(LNG_NOT_RECEIVED)
	surveysVal3Name=ToHTML(LNG_VOTED)
	surveysVal4Name=ToHTML(LNG_NOT_VOTED)

        surveysVal1Value=CStr(intReceivedSurveys)
	surveysVal2Value=CStr(intDoesntRec)
	surveysVal3Value=CStr(intVotedSurveys)
	surveysVal4Value=CStr(intDoesntVote)

	totalSurveysValue=intSentSurveys

	if(surveysVal1Value<>"0" OR surveysVal2Value<>"0" OR surveysVal3Value <> "0" OR surveysVal4Value<>"0") then
		SetVar "drawSurveys","true"
		surveysData = "["&surveysVal1Value&"],["&surveysVal2Value&"],["&surveysVal3Value&"],["&surveysVal4Value&"]"
		surveysColors = "'#"&surveysVal1Color&"','#"&surveysVal2Color&"','#"&surveysVal3Color&"','#"&surveysVal4Color&"'"
		SetVar "surveysData",surveysData
		SetVar "surveysColors",surveysColors
	else
		SetVar "drawSurveys","false"
		SetVar "surveysData",""
		SetVar "surveysColors",""
	end if
	
	SetVar "strSurveyTitle", strSurveyTitle
	SetVar "strSurveyDate", strSurveyDate
	SetVar "intSurveyId", intSurveyId
	SetVar "surveyPrevLink", ""
	SetVar "surveyNextLink", ""

	SetVar "surveysVal1Name", surveysVal1Name
	SetVar "surveysVal1Color", surveysVal1Color
	SetVar "surveysVal1Value", surveysVal1Value

	SetVar "surveysVal2Name", surveysVal2Name
	SetVar "surveysVal2Color", surveysVal2Color
	SetVar "surveysVal2Value", surveysVal2Value

	SetVar "surveysVal3Name", surveysVal3Name
	SetVar "surveysVal3Color", surveysVal3Color
	SetVar "surveysVal3Value", surveysVal3Value

	SetVar "surveysVal4Name", surveysVal4Name
	SetVar "surveysVal4Color", surveysVal4Color
	SetVar "surveysVal4Value", surveysVal4Value
	if(RS("type")="B") then 
		SetVar "totalSurveysValue",  totalSurveysValue
	else
		SetVar "totalSurveysValue", "<A href='#' onclick=""javascript: window.open('view_recipients_list_surveys.asp?id=" & intSurveyId & "','', 'status=0, toolbar=0, width=555, height=450, scrollbars=1')"">" & totalSurveysValue & "</a>"
	end if
	

	strSurveyTitle=replace(strSurveyTitle&"", """", """""")
	strSurveyDate=replace(strSurveyDate&"", """", """""")
        csvData=""
	csvData=csvData & """Survey title"",""Sent date"",""Total""," & """" & surveysVal1Name & """" & "," & """" & surveysVal2Name & """" & "," & """" & surveysVal3Name & """" & "," & """" & surveysVal4Name & """" 
	csvData=csvData & vbcrlf & """" & strSurveyTitle & """" & "," & """" & strSurveyDate & """" & "," & """" & totalSurveysValue & """" & "," & """" & surveysVal1Value & """" & "," & """" & surveysVal2Value & """" & "," & """" & surveysVal3Value & """"& "," & """" & surveysVal4Value & """"
	WriteToFile alerts_dir & "admin\csv\survey" & intSurveyId & ".csv", "", False
	WriteToFile alerts_dir & "admin\csv\survey" & intSurveyId & ".csv", csvData, True

 	SetVar "csvUrlSurvey", "csv/survey"&intSurveyId&".csv"


'answers chart
	totalAnswersValue=0

	isAnyVote=false

	intPrevQuestionNumber=0
	intNextQuestionNumber=0
	intMaxQuestionNumber=0
		
	Set RSMax = Conn.Execute("SELECT COUNT(id) as max_number FROM surveys_questions WHERE survey_id=" & intSurveyId)
	if(Not RSMax.EOF) then
		if(not IsNull(RSMax("max_number"))) then
			intMaxQuestionNumber=Clng(RSMax("max_number"))
		end if
	RSMax.Close
	end if

	if(intQuestionNumber<>1) then
		intPrevQuestionNumber=intQuestionNumber-1
	end if

	if(intQuestionNumber<>intMaxQuestionNumber) then
		intNextQuestionNumber=intQuestionNumber+1
	end if

	if(intPrevQuestionNumber<>0) then
		SetVar "surveyPrevLink", "<a class='q_button' href='javascript:showQuestion("&intPrevQuestionNumber&")'>" & LNG_PREVIOUS_QUESTION & "</a>"
	end if

	if(intNextQuestionNumber<>0) then
		SetVar "surveyNextLink", "<a class='q_button' href='javascript:showQuestion("&intNextQuestionNumber&")'>" & LNG_NEXT_QUESTION & "</a>"
	end if	

	Set RSQuestion = Conn.Execute("SELECT id, question, question_type FROM surveys_questions WHERE question_number=" & intQuestionNumber & " AND survey_id=" & intSurveyId)
	if(Not RSQuestion.EOF) then
	intQuestionId=RSQuestion("id")
	strQuestion=RSQuestion("question")
	if RSQuestion("question_type")="I" or RSQuestion("question_type")="T" then
		surveyCustomAnswers = "View custom answers"
		SetVar "surveyCustomAnswers", surveyCustomAnswers
		Set RSInput = Conn.Execute("SELECT COUNT(DISTINCT user_id) as total_answers FROM surveys_custom_answers_stat WHERE question_id=" & intQuestionId)
		if(Not RSInput.EOF) then
			totalAnswersValue = RSInput("total_answers")
		end if
	else
		Set RS1 = Conn.Execute("SELECT id, answer FROM surveys_answers WHERE question_id=" & intQuestionId)

		i=1
		mydiv=""
		csvData=""
		csvData=csvData & """Question"",""Answer"",""Votes"""
		answersValues = ""
		answersColors = ""
		Do While Not RS1.EOF
			answerValColor=getFCColor()
	                answerValName=RS1("answer")

   start_img = InStr(answerValName,"<img")
           Do While not IsNull(start_img) AND start_img <> "" AND start_img <> 0
                start_img = CLng(start_img)+4
                img = ""
                src = ""
                if (not IsNull(start_img) AND start_img <> "" AND start_img <> 0) then
                    end_img = InStr(start_img,answerValName,"/>") 

                    if (not IsNull(end_img) AND end_img <> "" AND end_img <> 0) then
                        len_img = CLng(end_img) - CLng(start_img)
                        img_replace =  Mid(answerValName,CLng(start_img)-4,len_img+6)
                        end_img = CLng(end_img)-2
                        len_img = CLng(end_img) - CLng(start_img)
                   


                        img = Mid(answerValName,start_img,len_img)
                        
                        start_src = InStr(CLng(start_img),answerValName,"src=")
                   
                        if (not IsNull(start_src) AND start_src <> "" AND start_src <> 0) then
                        start_src = CLng(start_src)+5
                            end_src = InStr(CLng(start_src),content,"""")
                             if (IsNull(end_src) OR end_src = "" OR end_src = "0") then
                                end_src = InStr(CLng(start_src),answerValName,"'")
                             end if
                            if (not IsNull(end_src) AND end_src <> "" AND end_src <> 0) then
                                end_src = CLng(end_src)
                                len_src = CLng(end_src)-CLng(start_src)
                                tf.WriteLine("len_src="&len_src)
 
                                src = Replace(Replace(Mid(answerValName,CLng(start_src),CLng(len_src)),"""",""),"'","")
                            end if
                            
                        end if

                        answerValName = Replace(answerValName,img_replace," <a target=""_blank"" href="""& src &""" >Picture</a> ")
                    end if
                end if
         start_img = InStr(answerValName,"<img")
        Loop

			intAnswerId=RS1("id")
			Set RSVotes = Conn.Execute("SELECT count(id) as votes_cnt FROM surveys_answers_stat WHERE answer_id=" & intAnswerId)
		
			answerValValue=0
	 		if (Not RSVotes.EOF) then
				if(Not IsNull (RSVotes("votes_cnt"))) then
					answerValValue=Clng(RSVotes("votes_cnt"))
				end if
			end if

			if(answerValValue>0) then isAnyVote=true end if
			totalAnswersValue=totalAnswersValue+answerValValue
			if (answersValues <> "") then answersValues = answersValues&"," end if
			answersValues = answersValues&"['"&answerValName&"',"&answerValValue&"]"
			if (answersColors <> "") then answersColors = answersColors&"," end if
			answersColors = answersColors&"'#"&answerValColor&"'"

			SetVar "answerValName", answerValName
			SetVar "answerValColor", answerValColor
			SetVar "answerValValue", answerValValue
			SetVar "intAnswerId", intAnswerId

	                Parse "DListSurveyAnswers", True
			csvData=csvData & vbcrlf & """" & replace(stripHTML(strQuestion), """", """""") & """" & "," & """" & replace(stripHTML(answerValName), """", """""") & """" & "," & """" & replace(stripHTML(answerValValue), """", """""") & """" 
			RS1.MoveNext
		Loop
		RS1.Close
		strXML = strXML & "</graph>"


		if(isAnyVote) then
			SetVar "drawAnswers","true"
			SetVar "answersValues",answersValues
			SetVar "answersColors",answersColors
		else
			SetVar "drawAnswers","false"
			SetVar "answersValues",""
			SetVar "answersColors",""
		end if
	end if

	csvDataAll=""
	csvDataAll=csvDataAll & """Question"",""Answer"",""Votes"""
  
'get all questions for csv
	surveyCustomAnswers = ""
	Set RSQuestionAll = Conn.Execute("SELECT id, question FROM surveys_questions WHERE survey_id=" & intSurveyId & " order by question_number")
	Do while Not RSQuestionAll.EOF
	intQId=RSQuestionAll("id")
    strQuestionAll=RSQuestionAll("question")
	
	Set RS1 = Conn.Execute("SELECT id, answer FROM surveys_answers WHERE question_id=" & intQId)
	Do While Not RS1.EOF
                strAnswer=RS1("answer")
		intAnswerId=RS1("id")
		Set RSVotes = Conn.Execute("SELECT count(id) as votes_cnt FROM surveys_answers_stat WHERE answer_id=" & intAnswerId)
	
		answer_votes=0
 		if (Not RSVotes.EOF) then
			if(Not IsNull (RSVotes("votes_cnt"))) then
				answer_votes=Clng(RSVotes("votes_cnt"))
			end if
		end if
       
		csvDataAll=csvDataAll & vbcrlf & """" &  replace(stripHTML(strQuestionAll), """", """""") & """" & "," & """" & replace(stripHTML(strAnswer), """", """""") & """" & "," & """" & replace(stripHTML(answer_votes), """", """""") & """" 
		RS1.MoveNext
	Loop
	RS1.Close

	RSQuestionAll.MoveNext
	loop
'--------------------



	SetVar "question", strQuestion
	SetVar "intQuestionId", intQuestionId
	SetVar "intNextQuestionNumber", intNextQuestionNumber
	SetVar "intPrevQuestionNumber", intPrevQuestionNumber

	SetVar "totalAnswersValue", totalAnswersValue

	strSurveyTitle=replace(strSurveyTitle, """", """""")
	strSurveyDate=replace(strSurveyDate, """", """""")


	WriteToFile alerts_dir & "admin\csv\survey_results_question" & intQuestionId & ".csv", "", False
	WriteToFile alerts_dir & "admin\csv\survey_results_question" & intQuestionId & ".csv", csvData, True

	WriteToFile alerts_dir & "admin\csv\survey_results" & intSurveyId & ".csv", "", False
	WriteToFile alerts_dir & "admin\csv\survey_results" & intSurveyId & ".csv", csvDataAll, True

       	SetVar "csvUrlResults", "csv/survey_results_question"&intQuestionId&".csv"
       	SetVar "csvUrlResultsAll", "csv/survey_results"&intSurveyId&".csv"

	end if

end if


'--------------------


'-------------------------------
' Step through each form
'-------------------------------
Header_Show
Footer_Show
'-------------------------------
' Process page templates
'-------------------------------
Parse "Header", False
Parse "Footer", False
Parse "main", False
'-------------------------------
' Output the page to the browser
'-------------------------------
Response.write PrintVar("main")

' index Show End

'-------------------------------
' Destroy all object variables
'-------------------------------

' index Close Event begin
' index Close Event End

UnloadTemplate
'===============================

else

Login(Request("intErrorCode"))
'response.write "test111222"

end if

'===============================
' Display Grid Form
'-------------------------------

%><!-- #include file="db_conn_close.asp" -->