﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SelectAlertLanguages.aspx.cs" Inherits="DeskAlertsDotNet.Pages.SelectAlertLanguages" %>
<%@ Import NameSpace="DeskAlertsDotNet.Parameters" %>
<%@ Import Namespace="Resources" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<title>DeskAlerts</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link href="css/style.css" rel="stylesheet" type="text/css">
        <link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <link href="css/jquery/jquery_timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css">
        <script language="javascript" type="text/javascript" src="functions.asp"></script>
        <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/date.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery_timepicker/jquery-ui-timepicker-addon.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/jquery/datepicker_fix.js"></script>
        <script language="javascript" type="text/javascript">


            $(document).ready(function() {
                $(".delete_button").button({
                });

                $(".save_and_next_button").button({
                    icons: {
                        secondary: "ui-icon-carat-1-e"
                    }
                });

                $("#selectAll").click(function () {
                    var checked = $(this).prop('checked');
                    $(".content_object").prop('checked', checked);
                });

                $("#nextButton").click(function (e) {
                    e.preventDefault();
                   $("#form1").submit();
                });



            });


            function showPreview(id) {
                window.open('../UserDetails.aspx?id=' + id, '','status=0, toolbar=0, width=350, height=350, scrollbars=1');
            }

            function editUser(id, queryString)
            {
                location.replace('../EditUsers.aspx?id='+ id +'&return_page=users_new.asp?' + queryString);
            }



        </script>
</head>
<body>
    <form id="form1" runat="server" action="CreateAlert.aspx" method="POST">
    <table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	    <tr>
	    <td>
	    <table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
		    <tr>
              <td width="100%" height="31" class="main_table_title">

                            <img id="Img1" runat="server" alt="" border="0" height="20" src="images/menu_icons/alert_20.png" style="padding-left: 7px" width="20" />

                            <a runat="server" id="A1" href="#" class="header_title" style="position: absolute; top: 22px">
                                Create Alert
                            </a>
                        </td>
		    </tr>
            <tr>
            <td class="main_table_body" height="100%">

                      <div style="margin-left: 20px; margin-top: 40px;">
                         <%=resources.LNG_STEP %>&nbsp<strong>1</strong>
                         <%=resources.LNG_OF %>
                           3 : <strong>
                          <%=resources.LNG_CHOOSE_LANGUAGES %>:</strong><br />
                     </div>

	                <br /><br />

			<div style="margin-left:10px" runat="server" id="mainTableDiv">

			   <table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr><td> 
					<div style="margin-left:10px" id="topPaging" runat="server"></div>
				</td></tr>
				</table>
				<br>

				<br><br>
				<asp:Table style="padding-left: 0px;margin-left: 10px;margin-right: 10px;" runat="server" id="contentTable" Width="15%" Height="100%" CssClass="data_table" CellSpacing="0" CellPadding="3">
				<asp:TableRow CssClass="data_table_title">
					<asp:TableCell runat="server" id="headerSelectAll" CssClass="table_title" Width="2%" >
						<asp:CheckBox id="selectAll" runat="server"/>
					</asp:TableCell>
					<asp:TableCell runat="server" id="languageNameHeaderCell" CssClass="table_title" ></asp:TableCell>
				</asp:TableRow>
				</asp:Table>
				<br>
				 <br>
                    <a  style="float:right;margin-right: 40px" id="nextButton" class='save_and_next_button' "><% =resources.LNG_NEXT %></a>
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
                     <input type="hidden" value="1" id="multilang" name="multilang"/>
				<tr><td> 
					<div style="margin-left:10px" id="bottomRanging" runat="server"></div>
				</td></tr>


				</table>
			</div>
			<div runat="server" id="NoRows">
                <br />
                <br />
				<center><b></b></center>
			</div>
			<br><br>
            </td>
            </tr>
           
 
            
             </table>
    </form>
</body>
</html>
