﻿<% Response.CacheControl = "no-cache" %>
<% Response.AddHeader "Pragma", "no-cache" %>
<% Response.Expires = -1 %>
<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<!-- #include file="ad.inc" -->
<%

check_session()

uid = Session("uid")

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>DeskAlerts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/style9.css" rel="stylesheet" type="text/css">
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script language="javascript" type="text/javascript">

$(document).ready(function() {
	var width = $(".data_table").width();
	var pwidth = $(".paginate").width();
	if (width != pwidth) {
		$(".paginate").width("100%");
	}
	$(".search_button").button({
		icons : {
			primary : "ui-icon-search"
		}
	});
});
</script>
</head>
<script language="javascript" type="text/javascript" src="functions.js"></script>

<body style="background-color:#ffffff;margin:0px" class="body">

<%
	showCheckbox = request("sc")
	
	
   if(Request("offset") <> "") then 
	offset = clng(Request("offset"))
   else 
	offset = 0
   end if	

   if(Request("sortby") <> "") then 
	sortby = Request("sortby")
   else 
	sortby = "name"
   end if	

   if(Request("gname") <> "") then 
	mygname = Request("gname")
   else 
	mygname = ""
   end if	


if (uid <>"") then
'counting pages to display
   if(Request("limit") <> "") then 
	limit = clng(Request("limit"))
   else 
	limit=50
   end if

	mygname = request("gname")
	if(mygname <> "") then 
		searchSQL = " AND (ip_groups.name LIKE N'%"&Replace(mygname, "'", "''")&"%' ) "
	else 
		searchSQL = ""
	end if	
   
	search_id = request("id")
	search_type = request("type")
	

	Set RS = Conn.Execute("SELECT COUNT(id) as mycnt FROM ip_groups WHERE id<>0 " & searchSQL)

	cnt=0

	if(Not RS.EOF) then
		cnt=RS("mycnt")
	end if

	RS.Close
  j=cnt/limit	
	
	objects = request("objects")

	checkedOUs=request("ous")
	checkedObject = ""
	if(InStr(checkedOUs, search_id)>0) then
		'checkedObject = "checked"
	end if

%>

<table width="100%" border="0"><tr valign="middle"><td width="225">
<form name="search_users" id="search_users" action="ip_groups.asp?<%response.write replace(Request.QueryString, "gname=", "gname_old=") %>" method="post">
<%=LNG_SEARCH_GROUPS %>: <input type="text" name="gname" value="<%=mygname %>"/></td><td width="50"> 
<a name="sub" class="search_button" onclick="javascript:document.forms[0].submit()"><%=LNG_SEARCH %></a>
<%
if(mygname<>"") then
response.write "<td> "&LNG_YOUR_SEARCH_BY&" """&mygname&""" "&LNG_KEYWORD&":</td>"
end if
%>
</td><td>
</td>
</tr></table>


<%
if(cnt>0) then

	page="ip_groups.asp?sc="&showCheckbox&"&id="&search_id&"&type="&search_type&"&ous="&checkedOUs&"&gname=" & mygname & "&"
	name=LNG_GROUPS
	Response.Write make_pages(offset, cnt, limit, page, name, sortby) 

%>

		</td></tr>
		</table>
		<table height="100%" class="data_table" cellspacing="0" cellpadding="3">
		<tr class="data_table_title">
		<%if(showCheckbox=1) then%>
		<td width="10"><input type='checkbox' name='select_all' id='select_all' onclick='javascript: select_all_objects_ou();'></td>
		<%end if%>
		
		<td class="table_title"><% response.write sorting(LNG_IP_GROUP_NAME,"name", sortby, offset, limit, page) %></td>
		</tr>
<%

'show main table
	Set RS = Conn.Execute("SELECT id, name FROM ip_groups WHERE id<>0 " & searchSQL &" ORDER BY "&sortby)
	num=0
	Do While Not RS.EOF
		num=num+1
		if(num > offset AND offset+limit >= num) then
		cnt1=0
        	%>
		<tr>
		<%
if(showCheckbox=1) then
	strObjectID = RS("id")
	if(InStr(objects, "g"&strObjectID&",")>0) then
		checkedObject = "checked"
	end if
%>
	<td><input type="checkbox" name="objects" id="object<%=strObjectID%>" value="<%="" & strObjectID%>" <%=checkedObject %> onclick="checkboxClicked(event);">
	<input type="hidden" name="objects_name" id="object_name<% response.write "" & strObjectID%>"  value="<%=RS("name") %>"/>
	</td>

<%
end if		%>
		
		<td>
		<% Response.Write RS("name") %>
		</td>
		</tr>
		<%
		end if
	RS.MoveNext
	Loop
	RS.Close

%>			
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr><td> 


<%

	Response.Write make_pages(offset, cnt, limit, page, name, sortby) 

%>

		</td></tr>
		</table>
 
                </table>
</form>

<%
else
	Response.Write "<center><b>"&LNG_THERE_ARE_NO_GROUPS&".</b><br><br></center>"
end if
else

  Response.Redirect "index.asp"

end if
%> 
</body>
</html>
<!-- #include file="db_conn_close.asp" -->