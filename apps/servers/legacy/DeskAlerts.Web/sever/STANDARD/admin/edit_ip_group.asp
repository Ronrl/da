﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%

check_session()

  uid = Session("uid")


if (uid <> "") then

'update group name in database

if(Request("sub1") = "Group") then
	    name=Request("name")
	     myname = name
		'check for existing name
		ip_str = request("ip_address")
		ip_from_str = request("ip_address_from")
		ip_to_str = request("ip_address_to")
	
		fl_check = 0
		Set rs_check = Conn.Execute("select id from ip_groups WHERE name='" & Replace(name, "'", "''") & "'")
		if(rs_check.eof) then
			fl_check = 1
		else
			if(clng(rs_check("id"))=clng(request("id"))) then
				fl_check = 1
			end if
		end if

		if(fl_check = 1) then
			if(Request("edit")=1) then
				SQL = "UPDATE ip_groups SET name = N'" & Replace(name, "'", "''") & "' WHERE id=" & Clng(Request("id"))
				Conn.Execute(SQL)
				group_id=Request("id")
			else
				SQL = "INSERT INTO ip_groups (name) VALUES (N'" & Replace(name, "'", "''") & "')"
				Conn.Execute(SQL)
				Set rs1 = Conn.Execute("select @@IDENTITY newID from ip_groups")
				group_id=rs1("newID")
				rs1.Close
				if(IsNull(group_id)) then
					Set rs1 = Conn.Execute("select MAX(id) as newID from ip_groups")
					user_id=rs1("newID")
					rs1.Close
				end if
			end if
			
			'add ip_ranges
			ipArray = Split(ip_str, ", ")
			ipFromArray = Split(ip_from_str, ", ")
			ipToArray = Split(ip_to_str, ", ")
			
			Conn.Execute("DELETE FROM ip_range_groups WHERE group_id="&group_id)
			
			'ip address
			For jj = 0 to UBound(ipArray)
				ipVal=ipArray(jj)
				if(ipVal<>"") then
					SQL = "INSERT INTO ip_range_groups (group_id, from_ip, to_ip) VALUES ("&group_id&", '" & Replace(ipVal, "'", "''") & "', '" & Replace(ipVal, "'", "''") & "')"
					Conn.Execute(SQL)
				end if
			Next
			'ip range
			For jj = 0 to UBound(ipFromArray)
				ipFromVal=ipFromArray(jj)
				ipToVal=ipToArray(jj)
				if(ipFromVal<>"" AND ipToVal<>"") then
					SQL = "INSERT INTO ip_range_groups (group_id, from_ip, to_ip) VALUES ("&group_id&", '" & Replace(ipFromVal, "'", "''") & "', '" & Replace(ipToVal, "'", "''") & "')"
					Conn.Execute(SQL)
				end if
			Next
			Response.Redirect "IpGroups.aspx"
		else
			error_descr = "IP group name '"& HtmlEncode(name) &"' already exists. Please enter another name."
		end if
end if

'show editing form

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>DeskAlerts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/style9.css" rel="stylesheet" type="text/css">
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script language="javascript" type="text/javascript">

$(document).ready(function() {
	$(".add_button").button();
	$(".save_button").button();
	$(".cancel_button").button();
});

</script>
<script language="javascript">
var cnt=0;

function verifyIP (IPvalue) {
	errorString = "";
	//theName = "IPaddress";
	theName = "Error";

	var ipPattern = /^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$/;
	var ipArray = IPvalue.match(ipPattern);

	if (ipArray == null)
		errorString = errorString + theName + ': '+IPvalue+' is not a valid IP address.';
	else {
		for (i = 0; i < 4; i++) {
			thisSegment = ipArray[i];
			if (thisSegment > 255) {
				errorString = errorString + theName + ': '+IPvalue+' is not a valid IP address.';
				i = 4;
			}
			if ((i == 0) && (thisSegment > 255)){
				errorString = errorString + theName + ': '+IPvalue+' is a special IP address and cannot be used here.';
				i = 4;
			}
		}
	}
	extensionLength = 3;
	if (errorString != ""){
		alert (errorString);
		return false
	}
	else{
		return true
	}
}
function dot2num(dot) {
var d = dot.split('.');
return ((((((+d[0])*256)+(+d[1]))*256)+(+d[2]))*256)+(+d[3]);}

function checkValues()
{
	var found = false, answersForm = document.getElementsByTagName("input");
	for(var i=0; i<answersForm.length; i++){
		if(answersForm[i].name=="ip_address"){
			if(verifyIP(answersForm[i].value)==false) return false;
			found = true;
		}
		else if(answersForm[i].name=="ip_address_from"){
			if(verifyIP(answersForm[i].value)==false) return false;
			if(verifyIP(answersForm[i+1].value)==false) return false;
			if(dot2num(answersForm[i].value) > dot2num(answersForm[i+1].value)){
				alert("'" + answersForm[i].value + "' / '"+answersForm[i+1].value+"' <%=LNG_FROM_IP_ADDRESS_IS_MORE_THEN_TO_IP_ADDRESS %>");
				return false;
			}
			found = true;
		}
	}
	if(!found){
		alert("<%=LNG_YOU_SHOULD_ADD_IP_OR_IP_RANGE_TO_PROCEED %>.");
		return false;
	}
	return true;
}

function my_sub()
{
	if(checkValues()==false) return false;
	if(document.getElementById('g_name').value==""){
		alert("<%=LNG_PLEASE_ENTER_GROUP_NAME %>");
		return false;
	}
	else{
		return true;
	}
}

function deleteItem(){
	this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode);
}

function addItem(val, ip_from, ip_to)
{
	var tbl = document.getElementById('ip_group_table');
	var lastRow = tbl.rows.length;
	var row = tbl.insertRow(lastRow-1);
	
	var cellLeft = row.insertCell(0);
	var cellMiddle = row.insertCell(1);
	var cellRight = row.insertCell(2);

	if(val=="address"){
		cellLeft.innerHTML = "<%=LNG_IP_ADDRESS %>";
		var el = document.createElement('input');
		el.type = 'text';
		el.name = 'ip_address';
		el.value = ip_from;
		cellMiddle.appendChild(el);
		el.focus();
	}
	else{
		var el = document.createElement('input');
		el.type = 'text';
		el.name = 'ip_address_from';
		el.value = ip_from;
		cellMiddle.appendChild(el);
		el.focus();
		
		var txt = document.createTextNode(' - ');
		cellMiddle.appendChild(txt);

		cellLeft.innerHTML = "<%=LNG_IP_RANGE %>";
		var el1 = document.createElement('input');
		el1.type = 'text';
		el1.name = 'ip_address_to';
		el1.value = ip_to;
		cellMiddle.appendChild(el1);
	}

	var el2 = document.createElement('img');
	el2.src = 'images/action_icons/delete.gif';
	el2.name = 'answer_delete';
	el2.border="0";
	el2.onclick=deleteItem;
	cellRight.appendChild(el2);
}
</script>
<script language="javascript">
function handleKeyPress(e){
	var key=e.keyCode || e.which;
	if (key==13){
		return false;
	}
}
</script>

</head>

<%
val="Add"
id=Clng(Request("id"))
if(id <> 0) then
  Set rs = Conn.Execute("SELECT name FROM ip_groups WHERE id=" & id)
  myname=rs("name")
  rs.Close
  val="Save"
end if
%>
<body style="margin:0px" class="body" onkeypress="return handleKeyPress(event);">
<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	<tr>
	<td>
	<table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
		<tr>
		<td width=100% height=31 class="main_table_title"><img src="images/menu_icons/ipgroups_20.png" alt="" width="20" height="20" border="0" style="padding-left:7px">
		<a href="admin_ip_groups.asp" class="header_title" style="position:absolute;top:15px"><%=LNG_IP_GROUPS %></a></td>
		</tr>
		<tr>
		<td class="main_table_body" height="100%">
		<div style="margin:10px;">
		<span class="work_header"><%=LNG_ADD_IP_GROUP %></span>
		<br><br>

<form method="post" action="">
<table border="0"><tr><td>
<input type="hidden" name="sub1" value="Group">
<font color="red"><%=error_descr %></font>
<table cellpadding=4 cellspacing=4 border=0 id="ip_group_table">
<tr><td><%=LNG_IP_GROUP_NAME %>:</td><td><input name="name" id="g_name" type="text" value="<% Response.Write myname %>"></td><td></td><td></td></tr>
<tr><td colspan="4"> <A href="#" onclick="addItem('address','0.0.0.0','0.0.0.0');"><%=LNG_ADD_IP_ADRESS %></a> / <A href="#" onclick="addItem('range','0.0.0.0','0.0.0.0');"><%=LNG_ADD_IP_RANGE %></a></td></tr>
</table>
<script language="javascript">
<%
if(id <> 0) then
	Set RSitems = Conn.Execute("SELECT from_ip, to_ip FROM ip_range_groups WHERE group_id=" & id)
	do while not RSitems.EOF
		
		ip_from = RSitems("from_ip")
		ip_to = RSitems("to_ip")
		
		if(ip_from = ip_to) then
		%>
		addItem("address","<%=ip_from %>","<%=ip_from %>");
		<%
		else
		%>
		addItem("range","<%=ip_from %>","<%=ip_to %>");
		<%	
		end if
		RSitems.MoveNext
	loop

end if
%>
</script>
</td></tr>
<tr><td align="right">
<%
if(val="Add") then
%>
<a class="cancel_button" href="admin_ip_groups.asp"><%=LNG_CANCEL %></a><button class="add_button" type="submit" onclick="return my_sub();" ><%=LNG_ADD %></button>

<%
else
%>
<a class="cancel_button" href="admin_ip_groups.asp"><%=LNG_CANCEL %></a><button class="save_button" type="submit" onclick="return my_sub();"><%=LNG_SAVE %></button>
<%
end if
if(id <> 0)  then
	Response.Write "<input type='hidden' name='edit' value='1'/>"
	Response.Write "<input type='hidden' name='id' value='" & id & "'/>"
end if
%>
</td></tr></table>
</form>
		 <img src="images/inv.gif" alt="" width="1" height="100%" border="0">
		</div>
		</td>
		</tr>
	</table>
	</td>
	</tr>
</table>

<%

else

  Response.Redirect "index.asp"

end if
%>
</body>
</html>
<!-- #include file="db_conn_close.asp" -->