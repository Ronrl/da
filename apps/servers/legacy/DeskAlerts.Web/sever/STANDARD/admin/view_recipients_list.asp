﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<meta http-equiv=Content-Type content="text/html;charset=utf-8">
	<title>Deskalerts Control Panel</title>
	
	<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
	<link href="css/style9.css" rel="stylesheet" type="text/css">
	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
	
	<script language="javascript" type="text/javascript">
		$(document).ready(function() {
			//$("#close_button").button();
			$("#search_button").button();
		});
	</script>
	<style>
		table {border-collapse:collapse;empty-cells:show}
		td {border-width:2px}
	</style>
</head>
<%
Dim groupAlert    
Dim alGuid
Dim testtest

if(Request("limit") <> "") then 
limit = clng(Request("limit"))
else 
limit=50
end if

if(Request("offset") <> "") then 
offset = clng(Request("offset"))
else 
offset=0
end if

alertId = clng(request("id"))

search = request("search")
if search <> "" then
	searchUsersSQL = " AND (name LIKE N'%"&replace(search, "'", "''")&"%' OR display_name LIKE N'%"&replace(search, "'", "''")&"%')"
	searchOtherSQL = " AND name LIKE N'%"&replace(search, "'", "''")&"%'"
else
	searchSQL = ""
	searchOtherSQL = ""
end if

Set groupCheck = Conn.Execute("SELECT id, part_id FROM multiple_alerts WHERE alert_id=" & alertId)
if(groupCheck.EOF) then
    groupAlert = 0
else
    groupAlert = 1    
end if
%>
<body style="margin:0" class="body">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
	<tr>
	<td>
		<table width="100%" cellspacing="0" cellpadding="0" class="main_table">
		<tr>
			<td width=100% height=31 class="main_table_title"><a href="" class="header_title"><%=LNG_LIST_OF_RECIPIENTS %></a></td>
		</tr>
		<tr>
			<td class="main_table_body">
		<div style="margin:10px;">
		
<form action="view_recipients_list.asp" method="POST">
<input type="hidden" name="id" value="<%=alertId%>"/>
<table border="0" cellpadding="3">
<tr valign="middle">
	<td nowrap valign="middle"><%=LNG_SEARCH%>:</td><td><input type="text" id="search" name="search" value="<%=HtmlEncode(search)%>" style="width:135px"/></td><td><a href="javascript: document.forms[0].submit()" id="search_button" class="search_button"><%=LNG_SEARCH%></a></td>
</tr>
</table>
</form>
<%
cnt=0
usersCnt=0
groupsCnt=0
compsCnt=0
Set RS = Conn.Execute("SELECT sent_date, type2 FROM alerts WHERE id=" & alertId)
if(not RS.EOF) then
	type2 = RS("type2")
	strAlertDate=RS("sent_date")
end if
if type2 = "B" then
	Set rsCnt = Conn.Execute("SELECT COUNT(id) as mycnt FROM users  WHERE role = 'U' AND  ( client_version != 'web client' OR client_version is NULL ) AND reg_date <= '"& strAlertDate &"'"& searchUsersSQL)
	if(not rsCnt.EOF) then
		usersCnt=rsCnt("mycnt")
	end if
else
    if groupAlert = 0 then
	    Set rsCnt = Conn.Execute("SELECT COUNT(user_id) as mycnt FROM alerts_sent INNER JOIN users ON users.id=alerts_sent.user_id WHERE alerts_sent.alert_id="& alertId & searchUsersSQL)
	    if(not rsCnt.EOF) then
		    usersCnt = rsCnt("mycnt")
	    end if
    else
        SQL = "SELECT stat_alert_id FROM multiple_alerts WHERE alert_id =" & alertId
            Set RS7 = Conn.Execute(SQL)
            alGuid = RS7("stat_alert_id")    
        Set rsCnt = Conn.Execute("SELECT COUNT(DISTINCT user_id) as mycnt FROM alerts_sent INNER JOIN users ON users.id=alerts_sent.user_id INNER JOIN multiple_alerts ma ON ma.alert_id = alerts_sent.alert_id WHERE ma.stat_alert_id='"& alGuid &"' " & searchUsersSQL)
	    if(not rsCnt.EOF) then
		    usersCnt = rsCnt("mycnt")
	    end if
    end if
	Set rsCnt = Conn.Execute("SELECT COUNT(group_id) as mycnt FROM alerts_sent_group INNER JOIN groups ON groups.id=alerts_sent_group.group_id WHERE alerts_sent_group.alert_id="& alertId & searchOtherSQL)
	if(not rsCnt.EOF) then
		groupsCnt = rsCnt("mycnt")
	end if
	Set rsCnt = Conn.Execute("SELECT COUNT(comp_id) as mycnt FROM alerts_sent_comp INNER JOIN computers ON computers.id=alerts_sent_comp.comp_id WHERE alerts_sent_comp.alert_id="& alertId & searchOtherSQL)
	if(not rsCnt.EOF) then
		compsCnt = rsCnt("mycnt")
	end if
end if
cnt = usersCnt + groupsCnt + compsCnt
j=cnt/limit
	

if(cnt>0) then
	page= "view_recipients_list.asp?id="&alertId&"&"
	name=LNG_RECIPIENTS
	Response.Write make_pages(offset, cnt, limit, page, name, sortby) 
end if


'paging
%>
<table width="95%" cellspacing="0" cellpadding="3" class="data_table">
<tr class="data_table_title">
	<td class="table_title"><%=LNG_NAME %></td>
	<td class="table_title"><%=LNG_TYPE %></td>	
</tr>
<%
	if type2 = "B" then
		SQL = "SELECT name, 'User' as r_type FROM users WHERE role = 'U' AND  ( client_version != 'web client' OR client_version is NULL ) AND reg_date <= '"& strAlertDate &"'" & searchUsersSQL
	else
		SQL = "SELECT name, 'User' as r_type FROM users INNER JOIN alerts_sent ON users.id=alerts_sent.user_id WHERE alerts_sent.alert_id="& alertId & searchUsersSQL
		SQL = SQL & " UNION ALL "
		SQL = SQL & "SELECT name, 'Group' as r_type FROM groups INNER JOIN alerts_sent_group ON groups.id=alerts_sent_group.group_id WHERE alerts_sent_group.alert_id="& alertId & searchOtherSQL
		SQL = SQL & " UNION ALL "
		SQL = SQL & "SELECT name, 'Computer' as r_type FROM computers INNER JOIN alerts_sent_comp ON computers.id=alerts_sent_comp.comp_id WHERE alerts_sent_comp.alert_id="& alertId & searchOtherSQL
	end if
    if groupAlert = 1 then
        SQL = "SELECT DISTINCT name, 'User' as r_type FROM users INNER JOIN alerts_sent ON users.id=alerts_sent.user_id INNER JOIN multiple_alerts ma ON alerts_sent.alert_id = ma.alert_id WHERE ma.stat_alert_id='"& alGuid & "' " & searchUsersSQL
    end if
	Set rsRecipient = Conn.Execute(SQL)
	num=offset
	if(Not rsRecipient.EOF) then rsRecipient.Move(offset) end if
	do while not rsRecipient.EOF
		num=num+1
		if((offset+limit) < num) then Exit Do end if

		if(num > offset AND offset+limit >= num) then
			recipientName = rsRecipient("name")
			recipientType = rsRecipient("r_type")
%>
	<tr><td><%=HtmlEncode(recipientName)%></td><td><%=recipientType%></td></tr>
<%
		end if
		rsRecipient.MoveNext
	loop
%>
 
</table>
<%
if(cnt>0) then
	Response.Write make_pages(offset, cnt, limit, page, name, sortby) 
end if
%>
 
<br/>

 
</div>		</td>
		</tr>
	</table>
	</td>
	</tr>
</table>
</body>
</html>
<!-- #include file="db_conn_close.asp" -->