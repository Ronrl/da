﻿namespace DeskAlertsDotNet.Pages
{
    using System;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;

    using DeskAlertsDotNet.Parameters;

    using Resources;

    public partial class PolicyUsers : DeskAlertsBaseListPage
    {
        protected override HtmlInputText SearchControl => searchTermBox;

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            var cnt = Content.Count < (Offset + Limit) ? Content.Count : Offset + Limit;
            
            userName.Text = GetSortLink(resources.LNG_USERNAME, "name", Request["sortBy"]);
            displayName.Text = GetSortLink(resources.LNG_DISPLAY_NAME, "display_name", Request["sortBy"]);
            domain.Text = GetSortLink(resources.LNG_DOMAIN, "domains.name", Request["sortBy"]);

            searchButton.Text = searchButton.Text.Replace("{Search}", resources.LNG_SEARCH);

            for (var i = Offset; i < cnt; i++)
            {
                var row = Content.GetRow(i);
                var tableRow = new TableRow();
                var chb = GetCheckBoxForDelete(row);

                var checkBoxCell = new TableCell { HorizontalAlign = HorizontalAlign.Center };
                checkBoxCell.Controls.Add(chb);
                tableRow.Cells.Add(checkBoxCell);

                var usernameCell = new TableCell { Text = row.GetString("name") };
                chb.InputAttributes["object_name"] = row.GetString("name");
                tableRow.Cells.Add(usernameCell);

                var displayNameCell = new TableCell
                                          {
                                              Text = row.GetString("display_name"),
                                              HorizontalAlign = HorizontalAlign.Center
                                          };
                tableRow.Cells.Add(displayNameCell);

                var domainCell = new TableCell
                                           {
                                               Text = row.GetString("domain_name"),
                                               HorizontalAlign = HorizontalAlign.Center
                                           };
                tableRow.Cells.Add(domainCell);

                contentTable.Rows.Add(tableRow);
            }
        }

        protected override string[] Collumns => new string[] { };

        protected override string ContentName => resources.LNG_USERS;

        protected override string DataTable => "";

        protected override HtmlGenericControl NoRowsDiv => NoRows;

        protected override HtmlGenericControl TableDiv => mainTableDiv;

        protected override string DefaultOrderCollumn => "id";

        protected override string CustomQuery
        {
            get
            {
                var query = GetFilterForQuery();

                query += @"SELECT t.user_id as id,
                               users.next_request,
                               users.last_standby_request,
                               users.next_standby_request,
                               users.context_id,
                               users.mobile_phone,
                               users.email,
                               users.name AS name,
                               users.display_name,
                               users.standby,
                               users.domain_id,
                               domains.name AS domain_name,
                               users.deskbar_id,
                               users.reg_date,
                               users.last_date,
                               users.last_request AS last_request1,
                               users.last_request,
                               users.client_version,
                               edir_context.name AS context_name
                        FROM #tempUsers t
                        LEFT JOIN users ON t.user_id = users.id
                        LEFT JOIN domains ON domains.id = users.domain_id
                        LEFT JOIN edir_context ON edir_context.id = users.context_id
                        WHERE ROLE='U' AND (users.client_version IS NULL OR users.client_version <> 'web client')";

                if (!string.IsNullOrEmpty(Request["sortBy"]))
                {
                    query += " ORDER BY " + Request["sortBy"];
                }
                else
                {
                    query += " ORDER BY name";
                }

                query += @" DROP TABLE #tempOUs
                        DROP TABLE #tempUsers";

                return query;
            }
        }

        private string GetFilterForQuery()
        {
            var searchType = Request["type"];

            var searchId = Request["id"];

            var query = @"SET NOCOUNT ON IF OBJECT_ID('tempdb..#tempOUs') IS NOT NULL
                                    DROP TABLE #tempOUs
                                    CREATE TABLE #tempOUs (Id_child BIGINT NOT NULL, Rights BIT NOT NULL);

                                    IF OBJECT_ID('tempdb..#tempUsers') IS NOT NULL
                                    DROP TABLE #tempUsers
                                    CREATE TABLE #tempUsers (user_id BIGINT NOT NULL);

                                    DECLARE @now DATETIME;

                                    SET @now = GETDATE();";

            if (searchType == "ou")
            {
                if (Request["search"] == "1")
                {
                    query += @"WITH OUs (id) AS
                                      (SELECT CAST(" + searchId + @" AS BIGINT)
                                       UNION ALL SELECT Id_child
                                       FROM OU_hierarchy h
                                       INNER JOIN OUs ON OUs.id = h.Id_parent)
                                    INSERT INTO #tempOUs (Id_child, Rights)
                                      (SELECT DISTINCT id,
                                                       " + searchId + @"
                                       FROM OUs) OPTION (MaxRecursion 10000);";
                }
                else
                {
                    query += @" INSERT INTO #tempOUs (Id_child, Rights) VALUES (" + searchId + ", 0)";
                }
            }
            else if (searchType == "DOMAIN")
            {
                query += @" INSERT INTO #tempOUs (Id_child, Rights) (SELECT id, 0 FROM OU WHERE Id_domain = " + searchId + ")";
            }
            else
            {
                query += " INSERT INTO #tempOUs (Id_child, Rights) (SELECT id, 0 FROM OU) ";
            }


            query += @"INSERT INTO #tempUsers (user_id)
                              (SELECT DISTINCT Id_user
                               FROM OU_User u
                               INNER JOIN #tempOUs t ON u.Id_OU=t.Id_child
                               LEFT JOIN users ON u.Id_user = users.id
                               WHERE ROLE='U'";

            if (!string.IsNullOrEmpty(SearchTerm))
            {
                query += string.Format(" AND (users.name LIKE N'%{0}%' OR users.display_name LIKE N'%{0}%')", SearchTerm);
            }

            query += ")";

            if (searchType != "ou")
            {
                query += @"	INSERT INTO #tempUsers(user_id)		
                                      (SELECT DISTINCT users.id		
                                       FROM users 		
                                       LEFT JOIN #tempUsers t ON t.user_id=users.id		
                                       WHERE t.user_id IS NULL		
                                         AND users.ROLE='U'";

                if (searchType == "DOMAIN")
                {
                    query += " AND users.domain_id = " + searchId;
                }

                if (!string.IsNullOrEmpty(SearchTerm))
                {
                    query += string.Format(" AND (users.name LIKE N'%{0}%' OR users.display_name LIKE N'%{0}%')", SearchTerm);
                }

                query += ")";

            }

            return query;
        }

        protected override string CustomCountQuery
        {
            get
            {
                string query = GetFilterForQuery();

                query += @" SELECT COUNT(1) AS cnt
                            FROM #tempUsers";
                return query;
            }
        }


        protected override HtmlGenericControl TopPaging => topPaging;

        protected override HtmlGenericControl BottomPaging => bottomRanging;
    }
}