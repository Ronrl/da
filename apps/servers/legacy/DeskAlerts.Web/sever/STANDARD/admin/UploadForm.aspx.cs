﻿using DeskAlertsDotNet.Parameters;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Drawing;
using DeskAlerts.ApplicationCore.Utilities;

namespace DeskAlertsDotNet.Pages
{
    public partial class UploadForm : DeskAlertsBasePage
    {
        //use curUser to access to current user of DA control panel      
        //use dbMgr to access to database

        protected string FormTitle;
        protected string FolderSuffix;
        protected int FormRecording;
        protected int FormSize;

        protected string FolderLink;
        protected string FolderPath;
        protected string FilesList;
        protected int Edit;

        private static readonly string[] ProhibitedExtentions = { ".exe", ".bat" };

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            FormTitle = Request.GetParam("title", "Upload form");
            FolderSuffix = Request.GetParam("suffix", "_video");
            var acceptableFormats = new List<string>();
            var supportedFormats = string.Empty;

            switch (FolderSuffix)
            {
                case "_images":
                    supportedFormats = "image/png,image/jpeg,image/jpg, image/gif";
                    string[] imageFormats = { ".png", ".jpeg", ".jpg", ".gif" };
                    acceptableFormats.AddRange(imageFormats);
                    break;
                case "_video":
                    supportedFormats += "video/avi,video/flv,video/mp4,video/ogg,video/wmv,video/x-ms-wmv";
                    string[] videoFormats = { ".avi", ".flv", ".mp4", ".ogg", ".wmv" };
                    acceptableFormats.AddRange(videoFormats);
                    break;
            }

            inputFiles.Attributes.Add("accept", supportedFormats);

            if (FolderSuffix == "_images")
            {
                FolderSuffix = "/images";
            }

            inputFiles.Attributes.Add("multiple", "false");

            FormRecording = Request.GetParam("record", 1);
            FormSize = Request.GetParam("size", 1);
            Edit = Request.GetParam("edit", 0);
            FolderLink = Config.Data.GetString("alertsDir") + "/admin/images/upload" + FolderSuffix;
            FolderPath = Config.Data.GetString("alertsFolder") + "\\admin\\images\\upload" + FolderSuffix;

            Directory.CreateDirectory(FolderPath);

            var directoryFileNames = Directory.GetFiles(FolderPath);
            var filesJsonArray = new JArray();

            foreach (var fileName in directoryFileNames)
            {
                var extension = Path.GetExtension(fileName);

                if (ProhibitedExtentions.Contains(extension.ToLower()) || !acceptableFormats.Contains(extension.ToLower()))
                {
                    continue;
                }

                var fileObject = CreateFileObject(fileName, FolderSuffix);

                filesJsonArray.Add(fileObject);
            }

            recordDiv.Visible = FolderSuffix == "_video";
            noFilesDiv.Visible = filesJsonArray.Count == 0;
            FilesList = filesJsonArray.ToString();

            uploadDiv.Visible = string.IsNullOrEmpty(Request["no_upload"]);

            if (string.IsNullOrEmpty(Request["no_upload"]))
            {
                form1.Action = "uploadfile.aspx";
                messageTable.Visible = true;
                uploadedContentType.Value = FolderSuffix;

            }
            else
            {
                recordTable.Visible = false;
            }
        }

        public JObject CreateFileObject(string fileName, string folderSuffix)
        {
            var fileObject = new JObject();
            var fInfo = new FileInfo(fileName);

            string[] sizes = { "B", "KB", "MB", "GB" };
            double len = fInfo.Length;

            var order = 0;

            while (len >= 1024 && ++order < sizes.Length)
            {
                len /= 1024;
            }

            var fileSize = $"{len:0.##} {sizes[order]}";

            fileObject.Add(new JProperty("name", fInfo.Name));
            fileObject.Add(new JProperty("id", HashingUtils.MD5(fileName)));
            fileObject.Add(new JProperty("relativePath", "images\\upload" + folderSuffix + "\\" + fInfo.Name));
            fileObject.Add(new JProperty("dateModified", fInfo.LastWriteTime.ToString("MM/dd/yyyy, HH:mm:ss")));
            fileObject.Add(new JProperty("dateCreated", fInfo.CreationTime.ToString("MM/dd/yyyy, HH:mm:ss")));
            fileObject.Add(new JProperty("size", fileSize));

            if (folderSuffix != "_video") {
                var img = new Bitmap(fileName);
                fileObject.Add(new JProperty("width", img.Width));
                fileObject.Add(new JProperty("height", img.Height));
            }

            return fileObject;
        }
    }
}