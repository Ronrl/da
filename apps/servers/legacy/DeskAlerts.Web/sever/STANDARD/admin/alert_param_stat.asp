﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<!-- #include file="libs/FusionCharts.asp" -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/style9.css" rel="stylesheet" type="text/css">
<script language="JavaScript" type="text/javascript" src="jscripts/FusionCharts.js"></script>
<script language="javascript" type="text/javascript" src="functions.js"></script>
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css">
<link href="css/jquery/jquery_timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css">
<script language="javascript" type="text/javascript" src="jscripts/date.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery_timepicker/jquery-ui-timepicker-addon.js"></script>
<script language="javascript" type="text/javascript">
	var serverDate;
	var settingsDateFormat = "<%=GetDateFormat()%>";
	var settingsTimeFormat = "<%=GetTimeFormat()%>";

	var responseDbFormat = "dd/MM/yyyy HH:mm:ss";
	var saveDbFormat = "dd/MM/yyyy HH:mm";
	var saveDbDateFormat = "dd/MM/yyyy";
	var saveDbTimeFormat = "HH:mm";
	var uiFormat = getUiDateTimeFormat(settingsDateFormat,settingsTimeFormat);
	
	$(function() {
		$("#statistics_show_button").button();
		
		serverDate = new Date(getDateFromAspFormat("<%=now_db%>",responseDbFormat));
		setInterval(function(){serverDate.setSeconds(serverDate.getSeconds()+1);},1000);
		
		var fromDate = $.trim($("#from_date").val());
		var toDate = $.trim($("#to_date").val());

		if (fromDate!="" && isDate(fromDate,saveDbDateFormat))
		{
			fromDate = new Date(getDateFromFormat(fromDate,saveDbDateFormat));
		}
		else
		{
			fromDate = serverDate;
		}
		
		if (toDate!="" && isDate(toDate,saveDbDateFormat))
		{
			toDate = new Date(getDateFromFormat(toDate,saveDbDateFormat));
		}
		else
		{
			toDate = serverDate;
		}
		
		initDatePicker($(".date_param_input"),settingsDateFormat);
	
		$("#from_date").datepicker("setDate",fromDate);
		$("#to_date").datepicker("setDate",toDate);
	});
</script>
<script language="JavaScript" type="text/javascript">
	function disable_onclick(el)
	{
		if(el && !el._onclick)
		{
			el._onclick = el.onclick;
			el.onclick = "return false";
		}
		else if(el)
		{
			el.onclick = el._onclick;
			el._onclick = null;
		}
	}
	function my_disable(i)
	{
		if(i==1) 
		{
			document.getElementById("range").disabled=true;
			document.getElementById("from_date").disabled=false;
			document.getElementById("to_date").disabled=false;
		}
		else
		{
			document.getElementById("range").disabled=false;
			document.getElementById("from_date").disabled=true;
			document.getElementById("to_date").disabled=true;
		}

	}
	
	function submitDateForm() {
		if (check_date($("#date_form")[0])){
			//showLoader();
			$("#date_form").submit();
		}
		//hideLoader();
	}
	
	function check_date(form)
	{
		if(form.select_date_radio[1].checked)
		{
			try{
	
				var fromDate = $.trim($("#from_date").val());
				var toDate = $.trim($("#to_date").val());
		
				if (!fromDate)
				{
					alert("You enter wrong time period!");
					return false;
				}
				if(!toDate)
				{
					alert("You enter wrong time period!");
					return false;
				}
			
				if (!isDate(fromDate,settingsDateFormat))
				{
					alert("You enter wrong time period!");
					return false;
				}
				else
				{
					if (fromDate!="")
					{
						fromDate = new Date(getDateFromFormat(fromDate,settingsDateFormat));
						$("#start_date").val(formatDate(fromDate,saveDbDateFormat));
					}
				}
			
				if (!isDate(toDate,settingsDateFormat))
				{
					alert("You enter wrong time period!");
					return false;
				}
				else
				{
					if (toDate!="")
					{
						toDate = new Date(getDateFromFormat(toDate,settingsDateFormat))
						$("#end_date").val(formatDate(toDate,saveDbDateFormat));
					}
				}
			
				if (fromDate > toDate)
				{
				
					alert("You enter wrong time period!");
					return false;
				}
			}
			catch(e){alert(e)}
		}
		return true;
	}
</script>
</head>
<body style="margin:0" class="body">
<%

alertsVal1Color="3B6392"
alertsVal2Color="A44441"
alertsVal3Color="9ABA59"

uid = Session("uid")
templateId = Request("temp_id")
if (uid <>"") then

	if (templateId<>"") then

		if(Request("offset") <> "") then 
			offset = clng(Request("offset"))
		else 
			offset = 0
		end if	

		if(Request("sortby") <> "") then 
			sortby = Request("sortby")
		else 
			sortby = "id DESC"
		end if	

		if(Request("limit") <> "") then 
			limit = clng(Request("limit"))
		else 
			limit=25
		end if

		Set RS_temp = Conn.Execute("SELECT name FROM alert_param_templates WHERE id='"&templateId&"'")
		
	end if

	alertsVal1Name = "finished"
	alertsVal2Name = "overdue"
	alertsVal3Name = "in process"

	range = Request("range")
	if range = "" then range = "1"

	radio = Request("select_date_radio")
	if radio = "" then radio = "1"

	schedule_type = Request("schedule_type")

	start_date = DateSerial(year(now_db), month(now_db), day(now_db)) 'today
	end_date = start_date 'today
	if radio = "1" then
		select case range
			case "1" 'today
				'do nothing
			case "2" 'yesterday
				start_date = DateAdd("d", -1, start_date)
				end_date = start_date
			case "3" 'last 7 days
				start_date = DateAdd("d", -6, start_date)
			case "4" 'this month
				start_date = DateAdd("d", -DatePart("d", start_date)+1, start_date)
			case "5" 'last month
				start_date = DateAdd("m", -1, start_date)
				end_date = DateAdd("d", -DatePart("d", end_date), end_date)
			case "6"
				'set dateSql as empty string
				start_date = DateSerial(2001,01,01)
		end select
	elseif radio = "2" then
		start_date = Request("start_date")
		end_date = Request("end_date")
	end if

	if radio = "1" and range = "6" then
		dateSql = ""
	else
		dateSql = " AND sent_date >= '"& start_date &" 00:00:00' AND sent_date <= '"& end_date &" 23:59:59.997'"
	end if

	alertsVal1Sql = " AND schedule_type = '0'" & dateSql
	alertsVal2Sql = " AND schedule_type = '1' AND from_date < GETDATE()" & dateSql
	alertsVal3Sql = " AND schedule_type = '1' AND from_date >= GETDATE()" & dateSql
	
	page="alert_param_stat.asp?temp_id="& templateId &"&range="& range &"&select_date_radio="& radio &"&start_date="& Server.URLEncode(start_date) &"&end_date="& Server.URLEncode(end_date) &"&schedule_type="
%>

<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	<tr>
	<td>
	<table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
		<tr>
		<td height="31" class="main_table_title"><a href="<% Response.Write "alert_param_stat.asp?temp_id="&templateId %>" class="header_title"><%=RS_temp("name")%></a></td>
		</tr>
		<tr>
		<td style="padding:5px; width:100%" height="100%" valign="top" class="main_table_body">
			<div style="margin:10px">
				<span class="work_header"><%=RS_temp("name")%>&nbsp;<%=LNG_STATISTICS %></span>
				<form name="frmList" id="date_form" method="get" onsubmit="return check_date(this);">


					<table cellpadding="0" cellspacing="0" border="0" width="98%">
					<tr>
						<td>
							|
							<%if schedule_type <> "" then %>
								<a href="<%=page%>"><%=LNG_OVERALL_STATISTICS%></a>
							<%else%>
								<%=LNG_OVERALL_STATISTICS%>
							<%end if%>
							|
							<%if schedule_type <> "0" then %>
								<a href="<%=page%>0"><%=alertsVal1Name%></a>
							<%else%>
								<%=alertsVal1Name%>
							<%end if%>
							|
							<%if schedule_type <> "1" then %>
								<a href="<%=page%>1"><%=alertsVal2Name%></a>
							<%else%>
								<%=alertsVal2Name%>
							<%end if%>
							|
							<%if schedule_type <> "2" then %>
								<a href="<%=page%>2"><%=alertsVal3Name%></a>
							<%else%>
								<%=alertsVal3Name%>
							<%end if%>
							|
						</td>
						<td align="right">
							<table cellpadding="2" cellspacing="2" border="0" width="460">
							<tr>
								<td width="360" nowrap colspan="5" align="left">
									<input type="radio" name="select_date_radio" value="1"<%if radio = "1" then Response.Write " checked"%> onClick="my_disable(2);"/>
									<select name="range" id="range"<%if not radio = "1" then Response.Write " disabled"%> style="font-size: 9pt;">
										<option value="1"<%if range = "1" then Response.Write " selected"%>><%=LNG_TODAY%></option>
										<option value="2"<%if range = "2" then Response.Write " selected"%>><%=LNG_YESTERDAY%></option>
										<option value="3"<%if range = "3" then Response.Write " selected"%>><%=LNG_LAST_DAYS%></option>
										<option value="4"<%if range = "4" then Response.Write " selected"%>><%=LNG_THIS_MONTH%></option>
										<option value="5"<%if range = "5" then Response.Write " selected"%>><%=LNG_LAST_MONTH%></option>
										<option value="6"<%if range = "6" then Response.Write " selected"%>><%=LNG_ALL_TIME%></option>
									</select>
								</td>
								<td valign="middle" rowspan="3">
									<input type="hidden" name="temp_id" value="<%=templateId%>"/>
									<input type="hidden" name="sortby" value="<%=sortby%>"/>
									<input type="hidden" name="limit" value="<%=limit%>"/>
									<input type="hidden" name="schedule_type" value="<%=schedule_type%>"/>
									<a name="sub" id="statistics_show_button" onclick="javascript:submitDateForm();"><%=LNG_SHOW%></a>
								</td>
							</tr>
							<tr valign="top">
								<tr valign="top">						
								<td nowrap valign="middle">

									<input type="radio" name="select_date_radio" value="2"<%if radio = "2" then Response.Write " checked"%> onClick="my_disable(1);"/> 

									<input name='from_date' class="date_param_input" id="from_date" type="text" value="<%=start_date%>" <%if not radio = "2" then Response.Write " disabled"%>  style="font-size: 9pt;" size="20"/> 
									<input id="start_date" type="hidden" name="start_date"/>	

								</td>
								<td align="center" valign="middle"><font face="verdana" size="2">-</font></td>
								<td nowrap valign="middle">
									<input name='to_date' class="date_param_input" id="to_date" type="text" value="<%=end_date%>" <%if not radio = "2" then Response.Write " disabled"%> style="font-size: 9pt;" size="20"/> 
									<input id="end_date" type="hidden" name="end_date"/>
								</td>
								<td nowrap valign="middle" width="16">
								</td>
							</tr>
							</table>
						</td>
					</tr>
					</table>
				</form>
					<%

					templateId = Request("temp_id")
					Dim paramName
					
					linkclick_str = LNG_ALERTS
					alerts_arr = Array ("","","","","","","")
						
					Set RS = Conn.Execute("SELECT id FROM users WHERE id="&uid&" AND role='E'")
					if(Not RS.EOF) then
						gid = 1
						Set rs_policy = Conn.Execute("SELECT type FROM policy INNER JOIN policy_editor ON policy.id=policy_editor.policy_id WHERE editor_id=" & uid)
						if(Not rs_policy.EOF) then
							if(rs_policy("type")="A") then
								gid = 0
							end if
						end if
						if(gid=1) then
							editor_id=RS("id")
							policy_ids=""
							policy_ids=editorGetPolicyIds(editor_id)
							alerts_arr = editorGetPolicyList(policy_ids, "alerts_val")
							if(alerts_arr(2)="") then
								linkclick_str = "not"
							end if
						end if
					else
						gid = 0
					end if
					RS.Close
					
					if (templateId<>"") then

						Set RS_temp = Conn.Execute("SELECT name FROM alert_param_templates WHERE id='"&templateId&"'")
						
						paramName = RS_temp("name")
						
						if schedule_type = "" then
							SQL = "SELECT COUNT(1) as cnt FROM alerts WHERE type='S' AND class IN (1,16) AND param_temp_id = '"& templateId &"'" & alertsVal1Sql & vbCrLf &_
									"UNION ALL" & vbCrLf &_
									"SELECT COUNT(1) as cnt FROM alerts WHERE type='S' AND class IN (1,16) AND param_temp_id = '"& templateId &"'" & alertsVal2Sql & vbCrLf &_
									"UNION ALL" & vbCrLf &_
									"SELECT COUNT(1) as cnt FROM alerts WHERE type='S' AND class IN (1,16) AND param_temp_id = '"& templateId &"'" & alertsVal3Sql
							set RS = Conn.Execute(SQL)

							alertsVal1Value = RS("cnt") : RS.MoveNext
							alertsVal2Value = RS("cnt") : RS.MoveNext
							alertsVal3Value = RS("cnt") : RS.MoveNext
							%>
							<table border="0" style="margin:10px">
							<tr><td>
							<%
							if alertsVal1Value <> "0" or alertsVal2Value <> "0" or alertsVal3Value <> "0" then
								strXML = "<graph showNames=""0""  decimalPrecision=""0"" pieSliceDepth=""30"" >"
								if alertsVal1Value <> "0" then
									strXML = strXML & "<set name="""&alertsVal1Name&""" value="""&alertsVal1Value&""" color="""&alertsVal1Color&"""/>"
								end if
								if alertsVal2Value <> "0" then
									strXML = strXML & "<set name="""&alertsVal2Name&""" value="""&alertsVal2Value&""" color="""&alertsVal2Color&""" />"
								end if
								if alertsVal3Value <> "0" then
									strXML = strXML & "<set name="""&alertsVal3Name&""" value="""&alertsVal3Value&""" color="""&alertsVal3Color&""" />"
								end if
								strXML = strXML & "</graph>"
								Response.Write renderChart("charts/FCF_Pie3D.swf", "", strXML, "alertsChart", 300, 250)
							end if
							%>
							</td><td>
							<table border="0">
							<tr><td><div class="legend" style='background-color:#<%=alertsVal1Color%>;'></div></td><td><%=alertsVal1Name%></td><td><%=alertsVal1Value%></td></tr>
							<tr><td><div class="legend" style='background-color:#<%=alertsVal2Color%>;'></div></td><td><%=alertsVal2Name%></td><td><%=alertsVal2Value%></td></tr>
							<tr><td><div class="legend" style='background-color:#<%=alertsVal3Color%>;'></div></td><td><%=alertsVal3Name%></td><td><%=alertsVal3Value%></td></tr>
							<tr><td></td><td><%=LNG_TOTAL%></td><td><%=alertsVal1Value+alertsVal2Value+alertsVal3Value%></td></tr>
							</td></tr>
							</table>
							<%
						else
							select case schedule_type
								case "0"
									sql_where = alertsVal1Sql
								case "1"
									sql_where = alertsVal2Sql
								case "2"
									sql_where = alertsVal3Sql
								case else
									sql_where = dateSql
							end select

							Set RS_params = Conn.Execute("SELECT id, name, type FROM alert_parameters WHERE temp_id = '"&templateId&"' AND visible=1 ORDER BY [order]")
							
							Set paramsIds=Server.CreateObject("Scripting.Dictionary")
							while NOT RS_params.EOF							
								paramKey = RS_params("id")
								paramValue = RS_params("type")
								
								paramsIds.Add paramKey, paramValue
								
								RS_params.MoveNext
							Wend
							
							paramsIdsKeys=paramsIds.Keys
							
							SQL = "SELECT id, from_date, schedule, schedule_type, type2, "&"["&join(paramsIdsKeys,"], [")&"]" &_
							" FROM" &_
							" 	 (" &_
							"	 SELECT a.id, a.from_date, a.schedule, a.schedule_type, a.type2, p.param_id, p.value" &_
							"	 FROM " &_
							"	 (" &_
							"		 SELECT id, from_date, schedule, schedule_type, type2 FROM alerts WHERE type='S' AND class IN (1,16) AND param_temp_id = '"&templateId&"'" & sql_where &_
							"		 UNION ALL" &_
							"		 SELECT id, from_date, schedule, schedule_type, type2 FROM archive_alerts WHERE type='S' AND class IN (1,16) AND param_temp_id = '"&templateId&"'" & sql_where &_
							"	 ) a" &_
							"	 LEFT JOIN alert_param_values p" &_
							"		ON p.alert_id=a.id" &_
							"	 ) d" &_
							"	 PIVOT" &_
							"	 (" &_
							"	 	 MAX(value)" &_
							"	 	 FOR param_id IN" &_
							"	 	 (" &_
							"		 	 ["&join(paramsIdsKeys,"], [")&"]" &_
							"	 	 )" &_
							"	 ) pvt" &_
							" ORDER BY "&sortby
							
							Set RS_alerts = Server.CreateObject("ADODB.Recordset")
							RS_alerts.Open SQL, Conn, 3
							
							cnt = RS_alerts.RecordCount
							
							page="alert_param_stat.asp?temp_id="& templateId &"&schedule_type="& schedule_type &"&range="& range &"&select_date_radio="& radio &"&start_date="& Server.URLEncode(start_date) &"&end_date="& Server.URLEncode(end_date) &"&"
							if(cnt>0) then
								name=LNG_ALERTS 
								Response.Write make_pages(offset, cnt, limit, page, name, sortby) 
							end if
							
							%>
								<table  style="width:100%" height="100%" cellspacing="0" cellpadding="3" class="data_table" style="margin:10px">
									<tr class="data_table_title"  width="100%">
							<%
							RS_params.MoveFirst
							num = 0
							while NOT RS_params.EOF
								%>
									<td class="table_title" >
									<%
										response.write sorting(RS_params("name"),"["&paramsIdsKeys(num)&"]", sortby, offset, limit, page) 
									%>
									</td>
								<%
								num=num+1
								RS_params.MoveNext
							Wend
							
							%>
								<td class="table_title"><%=LNG_RECIPIENTS %></td>
								<td class="table_title"><%=LNG_ACTIONS %></td>
							</tr>
							
						<%
								
							num=offset
							if(Not RS_alerts.EOF) then RS_alerts.Move(offset) end if
							Do While Not RS_alerts.EOF
								num=num+1
								if((offset+limit) < num) then Exit Do end if
								if(num > offset AND offset+limit >= num) then
								strObjectID=RS_alerts("id")
								%>
									<tr>
								<%
								for i=0 to paramsIds.Count-1
								%>
										<td>
											<%
											paramHTMLValue = RS_alerts(paramsIdsKeys(i))
											if (Clng(paramsIds.Item(paramsIdsKeys(i))) = 6) then
												convertRS = Conn.Execute("if ('"&paramHTMLValue&"'<>'') SELECT CONVERT(DATETIME,'"&paramHTMLValue&"') AS value ELSE SELECT '"&paramHTMLValue&"' as value")
												Response.Write "<scr" & "ipt language='javascript'>document.write( getUiDateTime('"&convertRS("value")& "'))</scr" & "ipt>"
											else 
												Response.Write paramHTMLValue
											end if
											%>
										</td>
								<%
								next
								%>
										<td>
										<% 
											if RS_alerts("type2") = "B" then
												Response.Write "All"
											else
												Response.Write "<a href='#' onclick=""window.open('view_recipients_list.asp?id=" & CStr(RS_alerts("id")) & "','',555,450)"">"
												Response.Write LNG_RECIPIENTS
												Response.Write "</a>"
											end if
										%>
										</td>
										<td align="center">
										<%
											Response.Write "<a href='#' onclick=""window.open('statistics_da_alerts_view_details.asp?id=" & CStr(RS_alerts("id")) & "','',555,450)""><img src=""images/action_icons/preview.png"" alt="""& LNG_PREVIEW &""" title="""& LNG_PREVIEW &""" width=""16"" height=""16"" border=""0"" hspace=""5""/></a>"
										%>
										</td>
									</tr>
								<%
								end if
								RS_alerts.MoveNext
							Loop
						%>
							</table>
						<%
							if(cnt>0) then
								Response.Write make_pages(offset, cnt, limit, page, name, sortby) 
							end if
						end if
					end if
				%>
			</div>
		</td>
	</tr>
</table>
<% 
else
	Response.Redirect "index.asp"
end if
%>