<% Response.Expires = 0 %>
<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<% Server.ScriptTimeout = 90000 %>
<%

check_session()
uid = Session("uid")

if (uid <> "") then


policy_ids=""

gid = 0
Set RS = Conn.Execute("SELECT id, only_personal FROM users WHERE id="&uid&" AND role='E'")
if(Not RS.EOF) then
	gid = 1
	Set rs_policy = Conn.Execute("SELECT p.id, p.type, pu.user_id FROM policy p INNER JOIN policy_editor pe ON p.id=pe.policy_id LEFT JOIN policy_user pu ON pu.policy_id = p.id AND pu.user_id = 0 WHERE pe.editor_id=" & uid)
	if Not rs_policy.EOF then
		if rs_policy("type")="A" or not IsNull(rs_policy("user_id")) then 'can send to all
			gid = 0
		end if
	end if
	if(gid=1) then
		editor_id = RS("id")
		policy_ids = editorGetPolicyIds(editor_id)
	end if
else
	gid = 0
end if
RS.Close

%>
[
<%

Dim id : id=CLng(Request("id"))

Dim parent_rigths : parent_rigths = 0
Dim joinSQL : joinSQL = " LEFT JOIN OU_hierarchy h2 ON h2.Id_parent=OU.Id"
Dim selectSQL : selectSQL = "SELECT OU.Id, OU.Name, COUNT(h2.Id_child) AS child_num"

if gid=0 then
	parent_rigths = 1
else
	if Request("type")<>"DOMAIN" then
		Dim GetRightsSQL : GetRightsSQL = _
			";WITH OUsList (Id_parent) " & _
			"AS ( " & _
			"SELECT CAST("& id &" AS BIGINT) AS Id_parent " & _
			"UNION ALL " & _
			"SELECT h.Id_parent FROM OU_hierarchy as h " & _
			"INNER JOIN OUsList as l " & _
			"ON h.Id_child = l.Id_parent) " & _
			"SELECT COUNT(p.id)+COUNT(pg.id) as rights FROM OUsList l LEFT JOIN policy_ou p ON p.ou_id=l.Id_parent AND " & Replace(policy_ids, "policy_id", "p.policy_id") & " LEFT JOIN ou_groups g ON g.ou_id=l.Id_parent LEFT JOIN policy_group pg ON pg.group_id=g.group_id AND " & Replace(policy_ids, "policy_id", "pg.policy_id")

		Set RS = Conn.Execute(GetRightsSQL)
		parent_rigths = RS("rights")
	end if
end if

if parent_rigths = 0 then
	joinSQL = joinSQL & " LEFT JOIN policy_ou p ON p.ou_id=OU.Id AND " & Replace(policy_ids, "policy_id", "p.policy_id") & " LEFT JOIN ou_groups g ON g.ou_id=OU.Id LEFT JOIN policy_group pg ON pg.group_id=g.group_id AND " & Replace(policy_ids, "policy_id", "pg.policy_id")
	selectSQL = selectSQL & ", COUNT(p.id)+COUNT(pg.id) AS rights"
else
	selectSQL = selectSQL & ", 1 AS rights"
end if

if Request("type")="DOMAIN" then
	GetOUs = selectSQL & " FROM OU_DOMAIN INNER JOIN OU ON OU_DOMAIN.Id_ou=OU.Id "&joinSQL&" WHERE OU_DOMAIN.Id_domain=" & id & " GROUP BY OU.Id, OU.Name ORDER BY OU.Name, OU.Id"
else
	GetOUs = selectSQL & " FROM OU_hierarchy h1 INNER JOIN OU ON h1.Id_child=OU.Id "&joinSQL&" WHERE h1.Id_parent=" & id & " GROUP BY OU.Id, OU.Name ORDER BY OU.Name, OU.Id"
end if


Set RS = Conn.Execute(GetOUs)
do while Not RS.EOF
	Response.Write "{"
	Response.Write """property"":"
	Response.Write "{"
	Response.Write """name"":" & """" & Replace(RS("Name"),"""", "\""") & ""","
	Response.Write """type1"":""ou"","
	Response.Write """hasCheckbox"":"
	if( RS("rights") = 0 ) then
		Response.Write "false"
	else
		Response.Write "true"
	end if
	Response.Write ","
	if( RS("child_num") > 0 ) then
		Response.Write """loadable"":""true"","
	end if
	Response.Write """id"":" & """"&RS("Id")&""""
	Response.Write "},"
	Response.Write """type"": ""ou"","
	Response.Write """id"":" & """"&RS("Id")&""""
	Response.Write "}"

	RS.MoveNext
	if not RS.EOF then
		Response.Write ","
	end if
loop

end if

%>]
<!-- #include file="db_conn_close.asp" -->