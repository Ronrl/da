<%@ Page Language="C#" %>
<%@ import Namespace="System" %>
<%@ import Namespace="System.Collections.Generic" %>
<%@ import Namespace="System.Linq" %>
<%@ import Namespace="System.Web" %>
<%@ import Namespace="System.IO" %>
<%@ import Namespace="PushSharp.Apple" %>
<%@ import Namespace="Newtonsoft.Json.Linq" %>


<script runat="server">

    public class PushNotificationApple
    {
       // private static PushBroker _pushBroker;
 
        public PushNotificationApple()
        {

        }

 
        private void ChannelDestroyed(object sender)
        {
        }

        private void DeviceSubscriptionChanged(object sender, string oldSubscriptionId, string newSubscriptionId, PushSharp.Core.INotification notification)
        {
			
        }
        private void DeviceSubscriptionExpired(object sender, string expiredSubscriptionId, DateTime expirationDateUtc, PushSharp.Core.INotification notification)
        {
        }
        private void NotificationFailed(object sender, PushSharp.Core.INotification notification, Exception error)
        {
        }
        private void ServiceException(object sender, Exception error)
        {
        }

        private void NotificationSent(object sender, PushSharp.Core.INotification notification)
        {
        }
		
		public static readonly string appleCertPath = "~/admin/APNS_distribution_PK.p12";
		
		public void SendAppleNotification(string token, string title, string alertId)
        {
            var appleCert = File.ReadAllBytes(System.Web.Hosting.HostingEnvironment.MapPath(appleCertPath));
            ApnsConfiguration config = new ApnsConfiguration(ApnsConfiguration.ApnsServerEnvironment.Production, appleCert,
                "Softomate1K");

            ApnsServiceBroker apnsBroker = new ApnsServiceBroker(config);

            ApnsNotification apnsNotification = new ApnsNotification(
                token);        
    
            apnsBroker.Start();
            apnsBroker.QueueNotification(new ApnsNotification
            {
                DeviceToken = token,
                 Payload = JObject.Parse("{\"aps\":{\"alert\":\""+ title + "\",\"sound\":\"notify.wav\"}, \"alert_id\":\""+ alertId+"\"}")
            }
            );
            apnsBroker.Stop();


        }
    }
      public void Page_Load(object sender, EventArgs e)
	{
		//Response.Write(System.Web.Hosting.HostingEnvironment.MapPath(PushNotificationApple.appleCertPath));
  		PushNotificationApple pn = new PushNotificationApple();
		String text = Request["text"];
		String deviceId = Request["deviceId"];
		String alert_id = Request["alert_id"];
		pn.SendAppleNotification(deviceId,text,alert_id);
		
	}
	
</script>
