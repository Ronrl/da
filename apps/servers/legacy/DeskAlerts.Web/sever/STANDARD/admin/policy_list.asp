﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%

check_session()

uid = Session("uid")

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>DeskAlerts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/style9.css" rel="stylesheet" type="text/css">
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script language="javascript" type="text/javascript">

	$(document).ready(function() {
		var width = $(".data_table").width();
		var pwidth = $(".paginate").width();
		if (width != pwidth) {
			$(".paginate").width("100%");
		}
		$("#add_policy_button").button({
			icons: {
				primary: "ui-icon-plusthick"
			}
		});
		$(".delete_button").button();

		$("#editors_dialog").dialog({
			title: "Editors",
			width:430,
			height:460,
			autoOpen:false,
			draggable: false,
			resizable: false,
			modal:true
		});
	});

	function showEditorsList(id) {
		$("#editors_frame").attr("src", "view_editors_list.asp?id="+id);
		$("#editors_dialog").dialog("open");
	}
</script>
</head>
<script language="javascript" type="text/javascript" src="functions.js"></script>
<%
   if(Request("offset") <> "") then 
	offset = clng(Request("offset"))
   else 
	offset = 0
   end if	


if (uid <>"") then

'counting pages to display
   if(Request("limit") <> "") then 
	limit = clng(Request("limit"))
   else 
	limit=50
   end if

   if(Request("sortby") <> "") then 
	sortby = Request("sortby")
   else 
	sortby = "name"
   end if	


'  limit=10

	Set RS = Conn.Execute("SELECT COUNT(id) as mycnt FROM policy")
	cnt=0

	if(Not RS.EOF) then
		cnt=RS("mycnt")
	end if

	RS.Close
  j=cnt/limit
  
  role = "A"
  Set roleSet = Conn.Execute("SELECT p.type FROM policy as p INNER JOIN policy_editor as pe ON pe.policy_id = p.id AND pe.editor_id = " & uid)
  
  if not roleSet.EOF then
    role = roleSet("type")
  end if


%>
<body style="margin:0px" class="body">
<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	<tr>
	<td>
	<table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
		<tr>
		<td width=100% height=31 class="main_table_title"><img src="images/menu_icons/policies_20.png" alt="" width="20" height="20" border="0" style="padding-left:7px">
		<a href="policy_list.asp" class="header_title" style="position:absolute;top:15px"><%=LNG_POLICIES %></a></td>
		</tr>
		<tr>
		<td class="main_table_body" height="100%">
		<div style="margin:10px;">
		<div style="display:none" id="editors_dialog" style="margin:0px;padding:0px">
			<iframe id="editors_frame" src="view_editors_list.asp" style="width:400px;height:400px;border:0px"></iframe>
		</div>
		
		<% if role <> "M" then %>
		<div align="right"><a id="add_policy_button" href="policy_edit.asp"><%=LNG_ADD_POLICY %></a></div>
		<% end if %>
		<br>


<%
if(cnt>0) then
	page="policy_list.asp?"
	name=LNG_POLICIES
	Response.Write make_pages(offset, cnt, limit, page, name, sortby) 

   if role <> "M" then 
	    response.write "<br><A href='#' class='delete_button' onclick=""javascript: return LINKCLICK('"&linkclick_str&"');"">"&LNG_DELETE&"</a><br/>"
   end if
   response.Write "<br>"
	response.write "<form action='delete_objects.asp' name='delete_objs' id='delete_objs' method='post'><input type='hidden' name='objType' value='policies'>"

%>



		<table width="100%" height="100%" class="data_table" cellspacing="0" cellpadding="3">
		<tr class="data_table_title">
		<td width="10"><input type='checkbox' name='select_all' id='select_all' onclick='javascript: select_all_objects();'></td>
		<td class="table_title"><% response.write sorting(LNG_POLICY ,"name", sortby, offset, limit, page) %></td>
		<td class="table_title"><%=LNG_ACTIONS %></td>
				</tr>
<%

'show main table

	Set RS = Conn.Execute("SELECT id, name FROM policy ORDER BY "&sortby)
	num=offset
	if(Not RS.EOF) then RS.Move(offset) end if
	Do While Not RS.EOF
		num=num+1
		if((offset+limit) < num) then Exit Do end if
		if(num > offset AND offset+limit >= num) then

		strObjectID=RS("id")

		%>
		<tr>
		<td><input type="checkbox" name="objects" value="<%=strObjectID%>"></td>
		<td>
		<% 

		Response.Write RS("name")

		%>
		</td>
		<td align="center" width="120" nowrap="nowrap">
		
		<% if role <> "M" then %>
		    <a href="policy_edit.asp?id=
		    <% Response.Write RS("id") %>
		    "><img src="images/action_icons/edit.png" alt="<%=LNG_EDIT_POLICY %>" title="<%=LNG_EDIT_POLICY %>" width="16" height="16" border="0" hspace=5></a> 
		<%end if %>
		
		<% if role <> "M" then %>
	    	<a href="policy_duplicate.asp?id=
	    	<% Response.Write RS("id") %>
		    "><img src="images/action_icons/duplicate.png" alt="<%=LNG_DUPLICATE_POLICY %>" title="<%=LNG_DUPLICATE_POLICY %>"  border="0" hspace=5></a>
		<%end if %>
		<a href="#" onclick="showEditorsList(<%=RS("id") %>)">
			<img src="images/action_icons/list.png" alt="<%=LNG_VIEW_EDITORS_FOR_POLICY %>" title="<%=LNG_VIEW_EDITORS_FOR_POLICY %>"  border="0" hspace=5>
		</a>
		</td>	
		<%
		end if
	RS.MoveNext
	Loop
	RS.Close


%>
         </table>
<%
	response.write "</form>"
	
	 if role <> "M" then 
	    response.write "<br/><A href='#' class='delete_button' onclick=""javascript: return LINKCLICK('"&linkclick_str&"');"">"&LNG_DELETE&"</a><br>"
     end if
        response.Write "<br>"
	Response.Write make_pages(offset, cnt, limit, page, name, sortby) 
else

Response.Write "<center><b>"&LNG_THERE_ARE_NO_POLICIES &".</b><br><br></center>"

end if

%>

		 <img src="images/inv.gif" alt="" width="1" height="100%" border="0">
		</div>
		</td>
		</tr>
	</table>
	</td>
	</tr>
</table>
<%
else
  Response.Redirect "index.asp"

end if
%>
</body>
</html>
<!-- #include file="db_conn_close.asp" -->