﻿using DeskAlerts.Server.Utils;
using System;
using System.Data;
using System.IdentityModel.Services;
using System.Web;
using System.Web.Security;
using System.Globalization;
using DeskAlerts.ApplicationCore.Utilities;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Managers;
using DeskAlertsDotNet.Parameters;
using DeskAlertsDotNet.sever.STANDARD.Utils;
using DeskAlertsDotNet.Utils.Factories;
using NLog;
using DeskAlertsDotNet.Models;

namespace DeskAlertsDotNet.Pages
{
    public partial class Auth : System.Web.UI.Page
    {
        private Logger _logger = LogManager.GetCurrentClassLogger();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var isOldSsoEnabled = Config.Data.IsOptionEnabled("ConfEnableIISAuth");
                var isSsoEnabled = Settings.Content["ConfSsoEnable"] == "1";
                var isOktaEnabled = Config.Data.IsOptionEnabled("OKTA_INTEGRATION");
                if (isOldSsoEnabled || isSsoEnabled || isOktaEnabled)
                {
                    try
                    {
                        _logger.Debug("Begin to log in user");
                        var windowsAuth = true;
                        var domainUser = new UserManager.DomainUser();
                        if (Config.Data.HasModule(Modules.SSO) && isSsoEnabled && !isOktaEnabled || isOldSsoEnabled)
                        {
                            domainUser = UserManager.Default.CreateDomainUser(Request.ServerVariables["LOGON_USER"]);
                        }
                        else if (isOktaEnabled)
                        {
                            domainUser = UserManager.Default.CreateDomainUser(FederatedAuthentication.SessionAuthenticationModule.ContextSessionSecurityToken.ClaimsPrincipal.Identity.Name);
                            windowsAuth = false;
                        }

                        var internalDomain = Config.Data.GetString("INTERNAL_DOMAIN");
                        if (!string.IsNullOrEmpty(internalDomain))
                        {
                            domainUser.DomainName = internalDomain;
                        }

                        var user = isOldSsoEnabled
                            ? UserManager.Default.GetUserByNameByRepository(domainUser.UserName)
                            : UserManager.Default.LoginWithWindowsAuth(domainUser);
                        UserManager.Default.Register(user);
                        if (user != null)
                        {
                            _logger.Info($"User {user?.Name} logged in through the {(windowsAuth ? "Windows Authentication" : "OKTA")}");
                            Login(user.Name);
                        }
                    }
                    catch (Exception ex)
                    {
                        _logger.Debug(ex);
                    }
                }
            }

            try
            {
                using (var dbMgr = new DBManager())
                {
                    var token = Session["token"];
                    if (token != null)
                    {
                        var guid = (Guid)token;

                        if (UserManager.Default.IsAuthorized(guid) &&
                        (Request.UrlReferrer != null && Request.UrlReferrer.AbsoluteUri.IndexOf("index.asp", StringComparison.Ordinal) != -1))
                        {
                            Response.Redirect("Index.aspx");
                            return;
                        }
                    }

                    CultureInfo.DefaultThreadCurrentCulture = new CultureInfo("en");
                    CultureInfo.DefaultThreadCurrentUICulture = new CultureInfo("en");
                    loginButton.Text = loginButton.Text.Replace("{Login}", Resources.resources.LNG_LOGIN);
                    loginButton.Click += loginButton_Click;
                    wrongLogin.Visible = false;
                    fullServer.Visible = false;
                    PublisherAccountDisabledByAdministratorErrorText.Visible = false;

                    bool isDemo = Config.Data.IsDemo;

                    demoText.Visible = isDemo;
                    copyrightDiv.Visible = isDemo;

                    string version = dbMgr.GetScalarQuery<string>("SELECT TOP 1 version FROM version");

                    versionLabel.Text = version;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

        }

        private void demoLoginButton_Click(object sender, EventArgs e)
        {
            UserManager.Default.SetDataBaseManager(new DBManager());
            var user = UserManager.Default.GetUserByNameByRepository("demo");
            if (user == null)
            {
                return;
            }

            UserManager.Default.Register(user);

            int sessionTimeout = 480;

            Session.Timeout = sessionTimeout;
            if (int.TryParse(Settings.Content["ConfSessionTimeout"], out sessionTimeout))
            {
                Session.Timeout = sessionTimeout;
            }

            Response.Redirect("Index.aspx");
        }

        private void loginButton_Click(object sender, EventArgs e)
        {
            var loginStr = login.Value;
            var pwd = password.Value;

            if (!IsPublisherCredentialsValid(loginStr, pwd))
            {
                _logger.Warn("Invalid log in attempt");
                wrongLogin.Visible = true;
                return;
            }

            if (!IsPublisherStatusValid(loginStr))
            {
                _logger.Warn("Invalid log in attempt. Publisher account disabled by Administrator.");
                PublisherAccountDisabledByAdministratorErrorText.Visible = true;
                return;
            }

            UpdatePublisherStatus(loginStr);
            Login(loginStr);
        }

        private void Login(string loginStr)
        {
#if DEBUG
                // Write userName to cookie to provide cookie-based auth
                HttpCookie loginCookie = new HttpCookie("login", loginStr);
                Response.Cookies.Add(loginCookie);
                Response.Cookies["login"].Expires = DateTime.Now.AddDays(30);
#endif
            var ticket = new FormsAuthenticationTicket(
                1,
                loginStr,
                DateTime.Now,
                DateTime.Now.AddMinutes(30),
                true,
                string.Empty);
            var cookiestr = FormsAuthentication.Encrypt(ticket);
            var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, cookiestr)
            {
                Expires = ticket.Expiration,
                Path = FormsAuthentication.FormsCookiePath
            };

            Response.Cookies.Add(cookie);

            if (Settings.Content["ConfTokenAuthEnable"] == "1")
            {
                var tokenTools = new TokenTools();
                if (!tokenTools.CheckTokensCount() && !UserManager.Default.CurrentUser.IsAdmin)
                {
                    if (!tokenTools.IsTokenExistForUser(UserManager.Default.CurrentUser.Id))
                    {
                        fullServer.Visible = true;
                        return;
                    }
                }

                var tokenUtils = new TokenUtils();
                cookie = new HttpCookie("desk_token", tokenUtils.GenerateToken(loginStr))
                {
                    Path = FormsAuthentication.FormsCookiePath
                };

                Response.Cookies.Add(cookie);
            }

            _logger.Info($"Successfull log in attempt by user {loginStr}");

            Response.Redirect("Index.aspx");
        }

        private bool IsPublisherCredentialsValid(string userName, string passWord)
        {
            using (var dbMgr = new DBManager())
            {
                if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(passWord))
                {
                    return false;
                }

                try
                {
                    var userNameParam = SqlParameterFactory.Create(DbType.String, userName, "userName");
                    var lookupPassword = dbMgr.GetScalarByQuery<string>($"SELECT pass FROM users WHERE name = @userName AND role != \'U\'", userNameParam).ToUpper();
                    var md5Pass = HashingUtils.MD5(passWord);

                    var isValid = (string.CompareOrdinal(lookupPassword, md5Pass) == 0);

                    if (isValid)
                    {
                        UserManager.Default.SetDataBaseManager(dbMgr);
                        var user = UserManager.Default.GetUserByNameByRepository(userName);
                        UserManager.Default.Register(user);
                    }

                    return isValid;
                }
                catch (Exception ex)
                {
                    _logger.Debug(ex);
                    return false;
                }
            }
        }

        private bool IsPublisherStatusValid(string publisherName)
        {
            var publisherStatus = UserManager.Default.GetPublisherParameterByName<string>("publisher_status", publisherName);

            return !publisherStatus.Equals("Disabled by Administrator");
        }

        private void UpdatePublisherStatus(string publisherName)
        {
            var publisherRole = UserManager.Default.GetPublisherParameterByName<string>("role", publisherName);
            var publisherStatus = UserManager.Default.GetPublisherParameterByName<string>("publisher_status", publisherName);
            if (!publisherRole.Equals("E") || publisherStatus.Equals("Enabled"))
            {
                return;
            }

            var activePublishersLeftAmount = UserManager.Default.GetActivePublishersLeftAmount();
            if (activePublishersLeftAmount < 1)
            {
                UserManager.Default.DisableLastOnlinePublishers(1);
            }

            UserManager.Default.EnablePublisher(publisherName);
        }
    }
}