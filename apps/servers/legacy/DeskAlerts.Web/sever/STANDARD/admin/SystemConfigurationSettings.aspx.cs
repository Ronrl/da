using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using DeskAlerts.ApplicationCore.Enums;
using DeskAlerts.ApplicationCore.Extensions.Enums;
using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.ApplicationCore.Repositories;
using DeskAlerts.ApplicationCore.Utilities;
using DeskAlerts.Server.Utils.Helpers;
using DeskAlerts.Server.Utils.Services;
using DeskAlertsDotNet.Application;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Pages;
using DeskAlertsDotNet.Parameters;
using JamaaTech.Smpp.Net.Lib;
using Resources;

namespace DeskAlerts.Server.sever.STANDARD.admin
{
    public partial class SystemConfigurationSettings : DeskAlertsBasePage
    {
        // use curUser to access to current user of DA control panel
        // use dbMgr to access to database
        protected string TinyMceNames;

        private readonly IAutoupdateService _autoupdateService = Global.Container.Resolve<IAutoupdateService>();

        protected override void OnLoad(EventArgs e)
        {
            LoadPageData();

            if (!IsPostBack)
            {
                base.OnLoad(e);
                cantSaveDemoSetingsSystem.Value = resources.LNG_SAVE;
                approvalTableRow.Visible = Config.Data.HasModule(Modules.APPROVE);
                egsSyncGroup.Visible = Config.Data.IsOptionEnabled("ConfDisableGroupsSync");
                NonAdUsersConnectionPermisionLevelHeader.Visible = Config.Data.HasModule(Modules.AD);
                NonAdUsersConnectionPermisionLevelTable.Visible = Config.Data.HasModule(Modules.AD);
                emailSettingsLabel.Visible = Config.Data.HasModule(Modules.EMAIL);
                emailSettingsTable.Visible = Config.Data.HasModule(Modules.EMAIL);

                if (!Config.Data.IsOptionEnabled("RESTAPI"))
                {
                    apiLabel.Visible = false;
                    apiSecretPanel.Visible = false;
                }

                if (Config.Data.IsDemo && curUser.Name != "admin")
                {
                    languageDemo.Visible = true;
                    language.Visible = false;
                    cantSaveDemoSetingsSystem.Visible = false;
                    Response.ShowJavaScriptAlert(resources.LNG_DISABLED_IN_DEMO);
                    tableSystemConfiguration.Height = "50";
                }
                else if (Settings.IsTrial)
                {
                    languageDemo.Visible = false;
                    language.Visible = true;
                    LanguageModificationTable.Visible = false;
                }
                else
                {
                    languageDemo.Visible = false;
                    language.Visible = true;
                    LanguageAdditionalDescription.InnerText = $"{resources.LNG_LANG_DESC}.";
                    LanguageAdditionalDescription.Attributes["class"] = ContentCustomClass.AdditionalDescription.GetStringValue();
                }

                ListItem seconds = new ListItem(resources.LNG_SECONDS, "0");
                ListItem minutes = new ListItem(resources.LNG_MINUTES, "1");
                ListItem hours = new ListItem(resources.LNG_HOURS, "2");

                ConfDSRefreshRateFactor.Items.AddRange(new[] { seconds, minutes, hours });

                LoadSettings();

                DataSet langSet = dbMgr.GetDataByQuery("SELECT name, code FROM languages_list");

                foreach (DataRow lang in langSet)
                {
                    string code = lang.GetString("code");

                    ListItem item = new ListItem(lang.GetString("name"), code);

                    IConfigurationManager config = new WebConfigConfigurationManager(Config.Configuration);
                    ISettingsRepository settingsRepository = new PpSettingsRepository(config);

                    var defLanguage = settingsRepository.GetByName("ConfDefaultLanguage").value;

                    if (code == defLanguage)
                    {
                        item.Selected = true;
                    }

                    ConfDefaultLanguage.Items.Add(item);
                }

                if (Request.UrlReferrer != null && Request.UrlReferrer.ToString().IndexOf("SaveSettings.aspx", StringComparison.Ordinal) > -1)
                {
                    successMessage.Style.Remove(HtmlTextWriterStyle.Display);
                    successMessage.Style.Add(HtmlTextWriterStyle.Display, string.Empty);
                }

                MakeAsterixVisiblePasswordFields();
                SetTokenInputs();

                var dataCodings = Enum.GetValues(typeof(DataCoding)).Cast<DataCoding>().Select(x => new KeyValuePair<string, int>(x.ToString(), (int)x));
                ConfSmppEncoding.DataSource = dataCodings;
                ConfSmppEncoding.SelectedValue = Settings.Content["ConfSmppEncoding"];
                ConfSmppEncoding.DataBind();
                
                if (_autoupdateService.Version != AutoupdateService.FileDoesNotExist)
                {
                    lastClientVersion.Text = _autoupdateService.Version;
                }
                else
                {
                    lastClientVersion.Text = AutoupdateService.FileDoesNotExist;
                }

                if (Config.Data.HasModule(Modules.MOBILE))
                {
                    checkPushPanel.Visible = true;
                }

                foreach (var format in DateTimeConverter.DateTimeFormats)
                {
                    ConfDateFormat.Items.Add(new ListItem(format, format) { Selected = format == Settings.Content["ConfDateFormat"] });
                }
            }
        }

        private void LoadPageData()
        {
            #region Import phones module
            ImportPhonesTR.Visible = Organizations.CheckNecessaryModulesForImportPhones();
            #endregion
        }

        private void SetTokenInputs()
        {
            ConfTokenMaxCountForServer.Disabled = !ConfTokenMaxCountForServerEnable.Checked;
            ConfTokenMaxCountForUser.Disabled = !ConfTokenMaxCountForUserEnable.Checked;
            ConfTokenExpirationTime.Disabled = !ConfTokenExpirationTimeEnable.Checked;
            ConfTokenMaxCountForServerEnable.Disabled = !ConfTokenAuthEnable.Checked;
            ConfTokenMaxCountForUserEnable.Disabled = !ConfTokenAuthEnable.Checked;
            ConfTokenExpirationTimeEnable.Disabled = !ConfTokenAuthEnable.Checked;
        }

        private void MakeAsterixVisiblePasswordFields()
        {
            HtmlInputText[] passwodrFields =
                {
                    ConfSmtpPassword,
                    ConfSmsProxyPass,
                    ConfSmsAuthPass,
                    ConfSmppPassword,
                    ConfTextToCallAuthToken
                };

            foreach (HtmlInputText passwrodField in passwodrFields)
            {
                passwrodField.Attributes["type"] = "password";
            }
        }

        private void LoadSettings()
        {
            foreach (string key in Settings.Content.Keys)
            {
                // = GetType().GetField(key, BindingFlags.Instance | BindingFlags.NonPublic);
                FieldInfo[] fieldInfo = GetType()
                    .GetFields(BindingFlags.Instance | BindingFlags.NonPublic)
                    .ToArray()
                    .Where(f => f.Name.StartsWith(key)).ToArray();

                // .First();
                if (fieldInfo.Length == 0)
                {
                    continue;
                }

                foreach (FieldInfo info in fieldInfo)
                {
                    object control =
                    info.GetValue(this);

                    if (control is HtmlInputPassword)
                    {
                        HtmlInputPassword htmlInputPassword = control as HtmlInputPassword;
                        htmlInputPassword.Value = Settings.Content[key];
                    }

                    if (control is HtmlInputCheckBox)
                    {
                        ((HtmlInputCheckBox)control).Checked = Settings.Content[key].Equals("1");
                    }
                    else if (control is HtmlInputGenericControl)
                    {
                        ((HtmlInputGenericControl)control).Value = Settings.Content[key];
                    }
                    else if (control is DropDownList)
                    {
                        ((DropDownList)control).SelectedValue = Settings.Content[key];
                    }
                    else if (control is HtmlInputText)
                    {
                        ((HtmlInputText)control).Value = Settings.Content[key];
                    }
                    else if (control is HtmlInputRadioButton)
                    {
                        HtmlInputRadioButton radioButton = control as HtmlInputRadioButton;

                        var dbValue = Settings.Content[key];

                        radioButton.Checked = radioButton.Value.Equals(dbValue);
                    }
                    else if (control is HtmlTextArea)
                    {
                        HtmlTextArea textArea = control as HtmlTextArea;
                        textArea.Value = Settings.Content[key];
                    }
                    else if (control is HtmlInputHidden)
                    {
                        HtmlInputHidden htmlInputHidden = control as HtmlInputHidden;
                        htmlInputHidden.Value = Settings.Content[key];
                    }
                }
            }
        }
        
    }
}