﻿namespace DeskAlertsDotNet.Pages
{
    using System;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;

    using DeskAlertsDotNet.Parameters;

    using Resources;

    public partial class PolicyGroups : DeskAlertsBaseListPage
    {
        protected override HtmlInputText SearchControl => searchTermBox;

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            searchButton.Text = searchButton.Text.Replace("{Search}", resources.LNG_SEARCH);

            groupName.Text = GetSortLink(resources.LNG_GROUP_NAME, "name", Request["sortBy"]);
            domain.Text = GetSortLink(resources.LNG_DOMAIN, "domains.name", Request["sortBy"]);

            var cnt = Content.Count < (Offset + Limit) ? Content.Count : Offset + Limit;
            for (var i = Offset; i < cnt; i++)
            {
                var row = Content.GetRow(i);
                
                var tableRow = new TableRow();
                var chb = GetCheckBoxForDelete(row);
                var checkBoxCell = new TableCell { HorizontalAlign = HorizontalAlign.Center };
                checkBoxCell.Controls.Add(chb);
                tableRow.Cells.Add(checkBoxCell);

                var groupCell = new TableCell { Text = row.GetString("name") };
                chb.InputAttributes["object_name"] = row.GetString("name");
                tableRow.Cells.Add(groupCell);

                var domainCell = new TableCell
                                     {
                                         Text = row.GetString("domain_name"),
                                         HorizontalAlign = HorizontalAlign.Center
                                     };
                tableRow.Cells.Add(domainCell);
                
                contentTable.Rows.Add(tableRow);
            }
        }

        protected override string[] Collumns => new string[] { };

        protected override string ContentName => resources.LNG_GROUPS;

        protected override string DataTable => "";

        protected override System.Web.UI.HtmlControls.HtmlGenericControl NoRowsDiv => NoRows;

        protected override System.Web.UI.HtmlControls.HtmlGenericControl TableDiv => mainTableDiv;

        protected override string DefaultOrderCollumn => "name";

        private string GetFilterForQuery()
        {
            var searchType = Request["type"];

            var searchId = Request["id"];

            var query = @"SET NOCOUNT ON IF OBJECT_ID('tempdb..#tempOUs') IS NOT NULL
                                    DROP TABLE #tempOUs
                                    CREATE TABLE #tempOUs (Id_child BIGINT NOT NULL, Rights BIT NOT NULL);

                                    IF OBJECT_ID('tempdb..#tempGroups') IS NOT NULL
                                    DROP TABLE #tempGroups
                                    CREATE TABLE #tempGroups (group_id BIGINT NOT NULL);

                                    DECLARE @now DATETIME;

                                    SET @now = GETDATE();";

            if (searchType == "ou")
            {
                if (Request["search"] == "1")
                {
                    query += @"WITH OUs (id) AS
                                      (SELECT CAST(" + searchId + @" AS BIGINT)
                                       UNION ALL SELECT Id_child
                                       FROM OU_hierarchy h
                                       INNER JOIN OUs ON OUs.id = h.Id_parent)
                                    INSERT INTO #tempOUs (Id_child, Rights)
                                      (SELECT DISTINCT id,
                                                       " + searchId + @"
                                       FROM OUs) OPTION (MaxRecursion 10000);";
                }
                else
                {
                    query += @" INSERT INTO #tempOUs (Id_child, Rights) VALUES (" + searchId + ", 0)";
                }
            }
            else if (searchType == "DOMAIN")
            {
                query += @" INSERT INTO #tempOUs (Id_child, Rights) (SELECT id, 0 FROM OU WHERE Id_domain = " + searchId + ")";
            }
            else
            {
                query += " INSERT INTO #tempOUs (Id_child, Rights) (SELECT id, 0 FROM OU) ";
            }

            query += @"INSERT INTO #tempGroups (group_id)
                      (SELECT DISTINCT groups.id
                       FROM groups
                       INNER JOIN OU_Group og ON groups.id=og.Id_group
                       INNER JOIN #tempOUs t ON og.Id_OU=t.Id_child";

            if (!string.IsNullOrEmpty(SearchTerm))
            {
                query += $" WHERE (groups.name LIKE N'%{SearchTerm}%')";
            }

            query += ")";

            if (searchType != "ou")
            {
                query += @" INSERT INTO #tempGroups (group_id)
                          (SELECT DISTINCT groups.id
                           FROM groups
                           LEFT JOIN #tempGroups t ON t.group_id=groups.id
                           WHERE t.group_id IS NULL";

                if (searchType == "DOMAIN")
                {
                    query += " AND groups.domain_id = " + searchId;
                }

                if (!string.IsNullOrEmpty(SearchTerm))
                {
                    query += $" AND (groups.name LIKE N'%{SearchTerm}%')";
                }

                query += ")";
            }

            return query;
        }

        protected override string CustomQuery
        {
            get
            {
                var query = GetFilterForQuery();

                query += @"SELECT t.group_id AS id,
                               groups.context_id,
                               groups.name AS name,
                               domains.name AS domain_name,
                               groups.custom_group,

                          (SELECT COUNT(1)
                           FROM users_groups ug
                           WHERE ug.group_id=t.group_id) AS users_cnt,

                          (SELECT COUNT(1)
                           FROM computers_groups cg
                           WHERE cg.group_id=t.group_id) AS computers_cnt,

                          (SELECT COUNT(1)
                           FROM groups_groups gg
                           WHERE gg.container_group_id=t.group_id) AS groups_cnt,

                          (SELECT COUNT(1)
                           FROM ou_groups og
                           WHERE og.group_id=t.group_id) AS ous_cnt,

                          (SELECT COUNT(1)
                           FROM groups_groups gg
                           WHERE gg.group_id=t.group_id) AS member_of_cnt,
                               edir_context.name AS context_name
                        FROM #tempGroups t
                        LEFT JOIN groups ON t.group_id = groups.id
                        LEFT JOIN domains ON domains.id = groups.domain_id
                        LEFT JOIN edir_context ON edir_context.id = groups.context_id";

                if (Session["group_id"] != null)
                {
                    query += " WHERE NOT(groups.id = '" + Session["group_id"] + "')";
                }

                if (!string.IsNullOrEmpty(Request["sortBy"]))
                {
                    query += " ORDER BY " + Request["sortBy"];
                }
                else
                {
                    query += " ORDER BY name";
                }

                query += @" DROP TABLE #tempOUs
                            DROP TABLE #tempGroups";
                
                return query;
            }
        }

        protected override string CustomCountQuery
        {
            get
            {
                var query = GetFilterForQuery();

                query += @" SELECT COUNT(1) AS cnt
                            FROM #tempGroups";
                return query;
            }
        }

        protected override HtmlGenericControl TopPaging => topPaging;

        protected override HtmlGenericControl BottomPaging => bottomPaging;

        protected override string DefaultSortingOrder { get; } = "ASC";
    }
}