<!-- #include file="config.inc" -->
<!-- #include file="lang.asp" -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>DeskAlerts</title>
	<link href="css/style9.css" rel="stylesheet" type="text/css">
	<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
	<script language="javascript" type="text/javascript" src="jscripts/ui.js"></script>
	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
	<script language="javascript" type="text/javascript">
		function handleKeyPress(e){
			if(!e) e = event;
			var key=e.keyCode || e.which;
			if (key==13){
					Search_Tgroups();
				}
		}
		$(document).ready(function() {
			$(".search_button").button({
				icons : {
					primary : "ui-icon-search"
				}
			});
			Search_Tgroups();
		});
	</script>
</head>
<body style="margin:0px">
	<table>
	<tr>
		<td><input type="text" name="search_users" id="search_users" onkeypress="handleKeyPress(event);"></td>
		<td><a name="sub" class="search_button" onclick="Search_Tgroups(); return false;"><%=LNG_SEARCH %></a></td>
	</tr>
	</table>
	<br />
	<div name="found_users" id="found_users"></div>
</body>
</html>