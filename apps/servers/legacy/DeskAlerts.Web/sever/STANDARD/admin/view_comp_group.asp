﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<!-- #include file="ad.inc" -->
<%

check_session()

on error resume next

  uid = Session("uid")

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>DeskAlerts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/style9.css" rel="stylesheet" type="text/css">
</head>
<script language="javascript">
function LINKCLICK() {
  alert ("This action is disabled in demo");
  return false;
}
</script>

<body style="margin:0px" class="iframe_body">

<%

   if(Request("offset") <> "") then 
	offset = clng(Request("offset"))
   else 
	offset = 0
   end if	

if (uid <>"") then

'counting pages to display

  limit=50

  Set RS = Conn.Execute("SELECT domains.name as name, domain_id, groups.name as gr_name  FROM domains INNER JOIN groups ON groups.domain_id=domains.id WHERE groups.id=" & Request("id"))
	if(Not RS.EOF) then
%>
	<%=LNG_GROUP %>: <b><% Response.Write RS("gr_name") %></b> <%=LNG_DOMAIN %>: <b><% Response.Write RS("name") %></b><br/>  <A href="view_group_group.asp?id=<%=request("id")%>&member_of=1"><%=LNG_VIEW_MEMBER_OF %></a> | <A href="view_group_group.asp?id=<%=request("id")%>"><%=LNG_VIEW_GROUPS %></a> | <A href="view_user_group.asp?id=<%=request("id")%>"><%=LNG_VIEW_USERS %></a> | <%=LNG_VIEW_COMPUTERS %><br/><br/>
<%

	end if   

if (AD = 3) then
	Set RS1 = Conn.Execute("SELECT COUNT(id) as mycnt FROM computers_groups WHERE group_id=" & CStr(Request("id")))
	if(Not RS1.EOF) then
		cnt=RS1("mycnt")
	end if
	j=cnt/limit
end if
if(cnt>0) then
%>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr><td> 


<%

	page="view_comp_group.asp?id=" & Request("id") & "&"
	name=LNG_COMPUTERS
	Response.Write make_pages(offset, cnt1, limit, page, name, sortby) 

%>

		</td></tr>
		</table>

		<table width="100%" height="100%" class="data_table" cellspacing="0" cellpadding="3">
		<tr class="data_table_title">
		<td class="table_title"><%=LNG_COMPUTER_NAME %></td>
<% if (AD = 0) then %>		<td class="table_title"><%=LNG_REGISTERED %></td> <% end if %>
<% if (AD = 3) then %>		<td class="table_title"><%=LNG_DOMAIN %></td> <% end if %>
		<td class="table_title"><%=LNG_ONLINE %></td>
		<td class="table_title"><%=LNG_LAST_ACTIVITY %></td>
		<td class="table_title"><%=LNG_ACTIONS %></td> 
				</tr>


<%

'show main table

	Set RS1 = Conn.Execute("SELECT name FROM groups WHERE id=" & Request("id"))

	if(NOT RS1.EOF) then

	if (AD = 3) then
	num=0
	Set RS3 = Conn.Execute("SELECT comp_id FROM computers_groups INNER JOIN computers ON computers.id=computers_groups.comp_id WHERE computers_groups.group_id="&Request("id")&" ORDER BY name")
	Do While Not RS3.EOF
	num=num+1
	Set RS = Conn.Execute("SELECT id, name, next_request, domain_id, reg_date, last_date, convert(varchar,last_request) as last_request1, last_request FROM computers WHERE id='" & RS3("comp_id") & "' ORDER BY name")
	if(Not RS.EOF) then
		if(num > offset AND offset+limit >= num) then
			reg_date=RS("reg_date")
			last_date=RS("last_date")
			if(Not IsNull(RS("last_request"))) then
				last_activ = CStr(RS("last_request1"))
			        mydiff=DateDiff ("n", RS("last_request"), now_db)
					if(RS("next_request")<>"") then
						online_counter = Int(CLng(RS("next_request"))/60) * 2
					else
						online_counter = 2
					end if
					
				if(mydiff > online_counter) then
					online="<img src='images/offline.gif' alt='"& LNG_OFFLINE &"' title='"& LNG_OFFLINE &"' width='11' height='11' border='0'>"
				else
					online="<img src='images/online.gif' alt='"& LNG_ONLINE &"' title='"& LNG_ONLINE &"' width='11' height='11' border='0'>"
				end if
			else
				last_activ = "&nbsp;"
				online="<img src='images/offline.gif' alt='"& LNG_OFFLINE &"' title='"& LNG_OFFLINE &"' width='11' height='11' border='0'>"
			end if
			if(RS("name") = NULL) then
				name="Unregistered"
			else
				name = RS("name")
			end if

		       	Set RS4 = Conn.Execute("SELECT name FROM domains WHERE id=" & RS("domain_id"))
			if(Not RS4.EOF) then
				  domain=RS4("name")
			else
				domain="&nbsp;"
			end if
			RS4.close

		        Response.Write "<tr><td>" + name + "</td><td align=center>"
			if (AD = 0) then 	
				Response.Write reg_date 
				Response.Write "</td><td align=center>"
			end if
			 if (AD = 3) then 	
				
				Response.Write domain 
				Response.Write "</td><td align=center>"
			 end if

			Response.Write online + "</td><td align=center>" + last_activ + "</td>"
			if (AD = 0) then 	
				Response.Write "<td align=center><a href='comp_delete.asp?id=" + CStr(RS("id")) + "'><img src='images/action_icons/delete.gif' alt='"& LNG_DELETE_COMPUTER &"' title='"& LNG_DELETE_COMPUTER &"' width='16' height='16' border='0' hspace=5></a></td>"
			end if
			 if (AD = 3) then 	
				Response.Write "<td align=center><a href='#' onclick=""javascript: window.open('comp_view_details.asp?id=" & CStr(RS("id")) & "','', 'status=0, toolbar=0, width=350, height=350, scrollbars=1')""><img src='images/action_icons/preview.png' alt='"& LNG_VIEW_COMPUTER_DETAILS &"' title='"& LNG_VIEW_COMPUTER_DETAILS &"' width='16' height='16' border='0' hspace=5></a></td>"
			 end if
			Response.Write "</tr>"
		end if
		RS.Close
	end if

	RS3.MoveNext
	Loop
	RS3.Close
	end if


	if (AD = 2) then
	  num=0
 	  For Each ttt in user_arr2
 	      if(ttt<>"") then
'---------------------------
	num=num+1
	Set RS = Conn.Execute("SELECT id, name, next_request, deskbar_id, reg_date, last_date, convert(varchar,last_request) as last_request1, last_request FROM users WHERE name='" & Replace(ttt, "'", "''") & "'")
	if(Not RS.EOF) then
		if(num > offset AND offset+limit >= num) then
		reg_date=RS("reg_date")
		last_date=RS("last_date")
		if(Not IsNull (RS("last_request"))) then
		last_activ = CStr(RS("last_request1"))
	        mydiff=DateDiff ("n", RS("last_request"), now_db)
			if(RS("next_request")<>"") then
				online_counter = Int(CLng(RS("next_request"))/60) * 2
			else
				online_counter = 2
			end if
			
		if(mydiff > online_counter) then
			online="<img src='images/offline.gif' alt='"& LNG_OFFLINE &"' title='"& LNG_OFFLINE &"' width='11' height='11' border='0'>"
		else
			online="<img src='images/online.gif' alt='"& LNG_ONLINE &"' title='"& LNG_ONLINE &"' width='11' height='11' border='0'>"
		end if
	else
		last_activ = "&nbsp;"
		online="<img src='images/offline.gif' alt='"& LNG_OFFLINE &"' title='"& LNG_OFFLINE &"' width='11' height='11' border='0'>"
	end if

	if(RS("name") = NULL) then
	name="Unregistered"
	else
	name = RS("name")
	end if
        Response.Write "<tr><td>" + name + "</td><td align=center>"
 if (AD = 0) then 	
	Response.Write reg_date 
	Response.Write "</td><td align=center>"
 end if
	Response.Write online + "</td><td align=center>" + last_activ + "</td>"

	Response.Write "<td align=center><a href='user_delete.asp?id=" + CStr(RS("id")) + "'><img src='images/action_icons/delete.gif' alt='"& LNG_DELETE_USER &"' title='"& LNG_DELETE_USER &"' width='16' height='16' border='0' hspace=5></a></td>"

	Response.Write "</tr>"
	end if

	RS.Close
		end if
'---------------------------
		End if
	  Next
	end if
	end if
	RS1.Close

%>         
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr><td> 

<%

	Response.Write make_pages(offset, cnt1, limit, page, name, sortby) 

%>


		</td></tr>
		</table>

</table>

<%
else
	Response.Write "<center><b>"&LNG_THERE_ARE_NO_COMPUTERS &".</b><br><br></center>"	
end if
else

  Response.Redirect "index.asp"

end if
%> 
</body>
</html>
<!-- #include file="db_conn_close.asp" -->