<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<%

check_session()

'script for display statistics by date

  uid = Session("uid")

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Admin</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="style.css" rel="stylesheet" type="text/css">
</head>
<%


if (uid <>"") then

   if(Request("offset") <> "") then 
	offset = clng(Request("offset"))
   else 
	offset = 0
   end if	

   if(Request("limit") <> "") then 
	limit = clng(Request("limit"))
   else 
	limit=25
   end if

'  limit=10

	Set RS = Conn.Execute("SELECT id, group_id FROM users WHERE id="&uid&" AND role='G'")
	if(Not RS.EOF) then
		gid = RS("group_id")
	else
		gid = 0
	end if

	RS.Close

' Get dates
start_date = GetDateFromString(Request("start_date"), Date)
from_day = day(start_date)
from_month = month(start_date)
from_year = year(start_date)

end_date = GetDateFromString(Request("end_date"), Date)
to_day = day(end_date)
to_month = month(end_date)
to_year = year(end_date)

if(Request("select_date_radio")="1" OR Request("select_date_radio")="") then
	if(Request("range")="1" OR Request("range")="") then
	        i=date()
		my_day = day(i)
		my_month = month(i)
		my_year = year (i)
	        i=date()
		my_day1 = day(i)
		my_month1 = month(i)
		my_year1 = year (i)

		test_date=make_date_by(my_day, my_month, my_year)
		test_date1=make_date_by(my_day1, my_month1, my_year1)

	end if
	if(Request("range")="2") then
	        i=date()-1
		my_day = day(i)
		my_month = month(i)
		my_year = year (i)

	        i=date()-1
		my_day1 = day(i)
		my_month1 = month(i)
		my_year1 = year (i)

		test_date=make_date_by(my_day, my_month, my_year)
		test_date1=make_date_by(my_day1, my_month1, my_year1)

	end if
	if(Request("range")="3") then

	        i=date()-7
		my_day = day(i)
		my_month = month(i)
		my_year = year (i)

	        i=date()
		my_day1 = day(i)
		my_month1 = month(i)
		my_year1 = year (i)

		test_date=make_date_by(my_day, my_month, my_year)
		test_date1=make_date_by(my_day1, my_month1, my_year1)

	end if
	
	if(Request("range")="4") then
		my_day = "01"
		my_month = month(date())
		my_year = year (date())

		my_day1 = day(date())
		my_month1 = month(date())
		my_year1 = year(date())
		
	    test_date=make_date_by(my_day, my_month, my_year)
	    test_date1=make_date_by(my_day1, my_month1, my_year1)	
		
	
	if(Request("range")="5") then
	    my_day =  day(date())
		my_month = month(date())-1
		my_year = year (date())

		my_day1 = day(date())
		my_month1 = month(date ())
		my_year1 = year (date())

		test_date=make_date_by(my_day, my_month, my_year)
		test_date1=make_date_by(my_day1, my_month1, my_year1)

	end if
	if(Request("range")="6") then
		
		my_day = "01"
		my_month = "01"
		my_year = "2001"

		my_day1 = day(date())
		my_month1 = month(date())
		my_year1 = year(date())

		test_date=make_date_by(my_day, my_month, my_year)
		test_date1=make_date_by(my_day1, my_month1, my_year1)
    end if
	
	from_date = test_date & " 00:00:00"
	to_date = test_date1 & " 23:59:59"
else
	'date from config---	
	from_date=make_date_by(CStr(from_day), CStr(from_month), CStr(from_year))

	from_date=from_date & " 00:00:00"

	to_date=make_date_by(CStr(to_day), CStr(to_month), CStr(to_year))

	to_date=to_date & " 23:59:59"

	'-------------------
end if

%>
<body style="margin:0" class="body">
<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	<tr height="100%">
	<td height="100%">
	<table width="100%" height="100%" border="1" bordercolor="#000000" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
		<tr>
		<td width=100% height=31 class="theme-colored-back"><a href="statistics.php" class="header_title">Statistics</a></td>
		</tr>
		<tr>
		<td bgcolor="#ffffff" height="100%">
		<div style="margin:10px;">

<% if(Request("type")="") then %><span class="work_header">Overall</span><%else%><span class="work_header"><A href="statistics.asp">Overall</a></span><%end if%>

<% if(Request("type")="users") then %><span class="work_header"> > Users Statistics</span><%end if%>
<% if(Request("type")="alerts") then %><span class="work_header"> > Alerts Statistics</span><%end if%>
<% if(Request("type")="surveys") then %><span class="work_header"> > Surveys Statistics</span><%end if%>
<table width="100%" cellspacing="0" cellpadding="0" >
<tr><td>
<%

set fs1=Server.CreateObject("Scripting.FileSystemObject")
if EXTENDED_STATISTICS=1 then

%>
<A href="statistics.asp?type=users">Users Statistics</a> | <A href="statistics.asp?type=alerts">Alerts Statistics</a> | <A href="statistics.asp?type=surveys">Surveys Statistics</a><br>
<%
end if
%>
</td><td>
<script language="JavaScript">
function my_disable(i)
{
	if(i==1) 
	{
		enable_onlick(document.getElementById("tcalico_0"));
		enable_onlick(document.getElementById("tcalico_1"));
		document.getElementById("range").disabled=true;
		document.getElementById("start_date").disabled=false;
		document.getElementById("end_date").disabled=false;
	}
	else
	{
		disable_onlick(document.getElementById("tcalico_0"));
		disable_onlick(document.getElementById("tcalico_1"));
		document.getElementById("range").disabled=false;
		document.getElementById("start_date").disabled=true;
		document.getElementById("end_date").disabled=true;
	}

}
function check_date(form)
{
	if(form.select_date_radio[1].checked)
	{
try{
		var start = form.start_date.value.split("/");
		var end = form.end_date.value.split("/");
		var startDate=new Date();
		var endDate=new Date();
		startDate.setFullYear(start[2],start[0]-1,start[1]);
		endDate.setFullYear(end[2],end[0]-1,end[1]);
		if(isNaN(startDate) || isNaN(endDate) || endDate < startDate)
		{
			alert("You enter wrong time period!");
			return false;
		}
}catch(e){alert(e)}
	}
	return true;
}
function disable_onlick(el)
{
	if(el && !el._onclick)
	{
		el._onclick = el.onclick;
		el.onclick = "return false";
	}
}
function enable_onlick(el)
{
	if(el && el._onclick)
	{
		el.onclick = el._onclick;
		el._onclick = "";
	}
}
</script>
<form name=frmList method=get onsubmit="return check_date(this);">
<table cellpadding="2" cellspacing="2" border="0" align="right" width="460">
<tr><td width="360" nowrap>
<input type="radio" name="select_date_radio" value="1" <% if(Request("select_date_radio")="1" OR Request("select_date_radio")="") then Response.Write "checked" end if %> onClick="javascript: my_disable(2);"/> 
<select name="range" id="range" <% if(Request("select_date_radio")="2") then Response.Write "disabled" end if %> style="font-size: 9pt;">
<option value="1" <% if(Request("range")="1") then Response.Write "selected" end if  %> >Today</option>
<option value="2" <% if(Request("range")="2") then Response.Write "selected" end if  %>>Yesterday</option>
<option value="3" <% if(Request("range")="3") then Response.Write "selected" end if  %>>Last 7 days</option>
<option value="4" <% if(Request("range")="4") then Response.Write "selected" end if  %>>This month</option>
<option value="5" <% if(Request("range")="5") then Response.Write "selected" end if  %>>Last month</option>
<option value="6" <% if(Request("range")="6") then Response.Write "selected" end if  %>>All time</option></select><br>
</td>
<td valign="middle" rowspan="3">
<input name="sub" type="image" src="images/show_button.gif" alt="Show statistics">
<input type="hidden" name="type" value="<%Response.Write Request("type")%>"/>
<input type="hidden" name="limit" value="<%Response.Write limit%>"/>
</td>
</tr>
<tr>
<td width="360" nowrap>

<input type="radio" name="select_date_radio" value="2" <% if(Request("select_date_radio")="2") then Response.Write "checked" end if %> onClick="javascript: my_disable(1);"/> 

<input type="text" name="start_date" id="start_date" value="<%response.write GetStringFromDate(start_date)%>" <% if(Request("select_date_radio")<>"2") then Response.Write "disabled" end if %> style="font-size: 9pt;">

<script language="JavaScript">
new tcal ({
	// form name
	'formname': 'frmList',
	// input name
	'controlname': 'start_date'
});
</script>
<font face="verdana" size=2>-</font>
<input type="text" name="end_date" id="end_date"  value="<%response.write GetStringFromDate(end_date)%>" <% if(Request("select_date_radio")<>"2") then Response.Write "disabled" end if %> style="font-size: 9pt;">
<script language="JavaScript">
new tcal ({
	// form name
	'formname': 'frmList',
	// input name
	'controlname': 'end_date'
});

disable_onlick(document.getElementById("tcalico_0"));
disable_onlick(document.getElementById("tcalico_1"));


</script>
<% if Request("type")="users" then %>
</td></tr><tr><td width="350" nowrap>Search users: <input type="text" name="uname" value="<%Response.Write Request("uname")%>"/>
<% end if %>
</td>
</tr>

</table>

</form></table>

<% if(Request("type")="") then %> 
Overall: Set any time period to get more details.<br><br>
		<span class="work_header">Alerts statistics</span><br>
<br>

		<table width="100%" border="1" bordercolor="#EAE9E1" cellspacing="0" cellpadding="3">
		<tr bgcolor="#EAE9E1">
		<td class="table_title">Sent</td>
		<td class="table_title">Viewed</td>
		<td class="table_title">Clicks</td>
		<td class="table_title">Open ratio</td>
		<td class="table_title">CTR</td>
				</tr>
<%

'show statistics by date

users_cnt=0
broad_cnt=0
group_cnt=0
  Set RSUsers = Conn.Execute("SELECT COUNT(id) as mycnt FROM users WHERE reg_date <= '" + to_date + "'")
  if(not RSUsers.EOF) then 
	users_cnt=RSUsers("mycnt") 
  end if

  Set RSBroad = Conn.Execute("SELECT COUNT(id) as mycnt FROM alerts WHERE create_date >= '" + from_date + "' AND create_date <= '" + to_date + "' AND type2='B'")
  if(not RSBroad.EOF) then 
	broad_cnt=RSBroad("mycnt") 
  end if

  Set RSGroup = Conn.Execute("SELECT alerts_sent_group.group_id as group_id FROM alerts_sent_group INNER JOIN alerts ON alerts.id=alerts_sent_group.alert_id WHERE create_date >= '" + from_date + "' AND create_date <= '" + to_date + "'")
  Do While not RSGroup.EOF
	 group_id=RSGroup("group_id")
	  Set RSUsers = Conn.Execute("SELECT COUNT(users.id) as mycnt FROM users INNER JOIN users_groups ON users.id=users_groups.user_id WHERE users_groups.group_id="&group_id&" AND reg_date <= '" + to_date + "'")
	  if(not RSUsers.EOF) then 
		groupusers_cnt=RSUsers("mycnt") 
		group_cnt=group_cnt+groupusers_cnt
	end if

  RSGroup.MoveNext	
  loop



  mysent=0
if(gid<>0) then
  Set RS7 = Conn.Execute("SELECT COUNT(alerts_sent_stat.id) as mycnt FROM [alerts_sent_stat] INNER JOIN alerts ON alerts.id=alerts_sent_stat.alert_id WHERE [date] >= '" + from_date + "' AND [date] <= '" + to_date + "' AND sender_id="&uid)
else
  Set RS7 = Conn.Execute("SELECT COUNT(id) as mycnt FROM [alerts_sent_stat] WHERE [date] >= '" + from_date + "' AND [date] <= '" + to_date + "'")
end if

  if(not RS7.EOF) then	mysent=RS7("mycnt") end if

  mysent=mysent+(broad_cnt*users_cnt)+group_cnt


  myreceived=0
  myclicks=0

if(gid<>0) then
  Set RS7 = Conn.Execute("SELECT COUNT(alerts_received.id) as mycnt FROM [alerts_received] INNER JOIN alerts ON alerts.id=alerts_received.alert_id WHERE [date] >= '" + from_date + "' AND [date] <= '" + to_date + "' AND sender_id="&uid)
else
  Set RS7 = Conn.Execute("SELECT COUNT(id) as mycnt FROM [alerts_received] WHERE [date] >= '" + from_date + "' AND [date] <= '" + to_date + "'")
end if
  if(not RS7.EOF) then	
	myreceived=RS7("mycnt") 
  end if

  if(IsNull(myclicks)) then myclicks=0 end if
  if(IsNull(myreceived)) then myreceived=0 end if

  if(mysent<>0) then
    myopen=Round(myreceived/mysent*100,2)
  else
    myopen="0"
  end if
  if(myreceived<>0) then
    myctr=Round(myclicks/myreceived*100,2)
  else
    myctr="0"
  end if


  %>
  <tr align="center"><td>
  <% Response.Write mysent %>
  </td><td>
  <% Response.Write myreceived %>
  </td><td>
  <% Response.Write myclicks%>
  </td><td>
  <% Response.Write myopen %>
  %</td><td>
  <% Response.Write myctr %>
  %</td></table><br><br>
  <%

if(gid<>0) then
		Set RS1 = Conn.Execute("SELECT COUNT(users.id) as mycnt FROM users INNER JOIN users_groups ON users.id=users_groups.user_id WHERE role='U' AND name IS NULL AND users_groups.group_id="&gid)
else
		Set RS1 = Conn.Execute("SELECT COUNT(id) as mycnt FROM users WHERE role='U' AND name IS NULL")
end if
		cnt1=0
		if(Not RS1.EOF) then cnt1=RS1("mycnt") end if
 %>
		<span class="work_header">Users statistics</span><br>
		<br>
		<table width="100%" border="1" bordercolor="#EAE9E1" cellspacing="0" cellpadding="3">
		<tr bgcolor="#EAE9E1">
		<td class="table_title">Registered</td>
		<td class="table_title">Total</td>
		<td class="table_title">Online</td>

<%

if(gid<>0) then
		Set RS1 = Conn.Execute("SELECT users.id as id, last_request FROM users INNER JOIN users_groups ON users.id=users_groups.user_id WHERE role='U' AND users_groups.group_id="&gid)
else
		Set RS1 = Conn.Execute("SELECT id, last_request FROM users WHERE role='U'")
end if
		cnt2=0
		cnt3=0
		Do While Not RS1.EOF
		if(not isNull(RS1("last_request"))) then
			if(RS1("last_request") <> "") then
		        mydiff=DateDiff ("n", RS1("last_request"), now_db)
			if(mydiff > 10) then
			else
				cnt3=cnt3+1
			end if
		end if
		end if
		cnt2=cnt2+1
		RS1.MoveNext
		loop
		cnt1 = cnt2-cnt1
		%>
		<tr align="center"><td>
		<% Response.Write cnt1 %>
		</td><td>
		<% Response.Write cnt2 %>
		</td><td>
		<% Response.Write cnt3 %>
		</td></tr>
	         </table>

	         <br><br>
		<span class="work_header">Surveys statistics</span><br>
<br>

		<table width="100%" border="1" bordercolor="#EAE9E1" cellspacing="0" cellpadding="3">
		<tr bgcolor="#EAE9E1">
		<td class="table_title">Sent</td>
		<td class="table_title">Received</td>
		<td class="table_title">Votes</td>
		<td class="table_title">CTR</td>
				</tr>
<%

'show statistics by date

group_cnt=0
  Set RSGroup = Conn.Execute("SELECT alerts_sent_group.group_id as group_id FROM alerts_sent_group INNER JOIN surveys_main ON surveys_main.sender_id=surveys_sent_group.alert_id WHERE create_date >= '" + from_date + "' AND create_date <= '" + to_date + "'")
  Do While not RSGroup.EOF
	 group_id=RSGroup("group_id")
	  Set RSUsers = Conn.Execute("SELECT COUNT(users.id) as mycnt FROM users INNER JOIN users_groups ON users.id=users_groups.user_id WHERE users_groups.group_id="&group_id&" AND reg_date <= '" + to_date + "'")
	  if(not RSUsers.EOF) then 
		groupusers_cnt=RSUsers("mycnt") 
		group_cnt=group_cnt+groupusers_cnt
	end if
  RSGroup.MoveNext	
  loop


  mysent=0
if(gid<>0) then
  Set RS7 = Conn.Execute("SELECT COUNT(alerts_sent_stat.id) as mycnt FROM [alerts_sent_stat] INNER JOIN surveys_main ON alerts_sent_stat.survey_id=surveys_main.id WHERE [date] >= '" + from_date + "' AND [date] <= '" + to_date + "' AND sender_id="&uid)
else
  Set RS7 = Conn.Execute("SELECT COUNT(id) as mycnt FROM [alerts_sent_stat] WHERE [date] >= '" + from_date + "' AND [date] <= '" + to_date + "'")
end if
  if(not RS7.EOF) then	mysent=RS7("mycnt") end if
  mysent=mysent+group_cnt

  myreceived=0
  myvotes=0
if(gid<>0) then
  Set RS7 = Conn.Execute("SELECT COUNT(alerts_received.id) as mycnt, SUM(vote) as vote_cnt FROM [alerts_received] INNER JOIN surveys_main ON alerts_received.survey_id=surveys_main.id WHERE [date] >= '" + from_date + "' AND [date] <= '" + to_date + "' AND sender_id="&uid)
else
  Set RS7 = Conn.Execute("SELECT COUNT(id) as mycnt, SUM(vote) as vote_cnt FROM [alerts_received] WHERE [date] >= '" + from_date + "' AND [date] <= '" + to_date + "'")
end if
  if(not RS7.EOF) then
	myreceived=RS7("mycnt")
	myvotes=RS7("vote_cnt")
   end if
 if(IsNull(myvotes)) then myvotes=0 end if
  if(mysent<>0) then
    myopen=Round(clng(myreceived)/clng(mysent)*100,2)
  else
    myopen="0"
  end if
  if(myreceived<>0) then
    myctr=Round(clng(myvotes)/clng(myreceived)*100,2)
  else
    myctr="0"
  end if

  %>
  <tr align="center"><td>
  <% Response.Write mysent %>
  </td><td>
  <% Response.Write myreceived %>
  </td><td>
  <% Response.Write myvotes%>
  </td><td>
  <% Response.Write myctr %>
  %</td></table><br><br><br>

		 <img src="images/inv.gif" alt="" width="1" height="100%" border="0">
<%end if%>


<%
if(Request("type")="users") then
%>
CTR shows ratio between clicks (on links within the alert) and total alerts. <br>
To view user's details click on the user name or "button" in Details section.<br><br>

<%
   if(Request("sortby") <> "") then 
	sortby = Request("sortby")
   else 
	sortby = "name"
   end if	

	myuname = Request("uname")

	if(myuname<>"") then
		if(gid<>0) then
			Set RS = Conn.Execute("SELECT COUNT(users.id) as mycnt FROM users INNER JOIN users_groups ON users_groups.user_id=users.id WHERE role='U' AND name LIKE N'%" & Replace(myuname, "'", "''") & "%' AND users_groups.group_id="&gid)
		else
			Set RS = Conn.Execute("SELECT COUNT(id) as mycnt FROM users WHERE role='U' AND name LIKE N'%" & Replace(myuname, "'", "''") & "%'")
		end if
	else
		if(gid<>0) then
			Set RS = Conn.Execute("SELECT COUNT(users.id) as mycnt FROM users INNER JOIN users_groups ON users_groups.user_id=users.id WHERE role='U' AND name <> '' AND users_groups.group_id="&gid)
		else
			Set RS = Conn.Execute("SELECT COUNT(id) as mycnt FROM users WHERE role='U' AND name <> ''")
		end if
	end if
	cnt=0

	if(Not RS.EOF) then cnt=RS("mycnt") end if

	RS.Close
  j=cnt/limit

if(cnt>0) then

	page="statistics.asp?type=users&from_day=" & Request("from_day") & "&from_month=" & Request("from_month") & "&from_year=" & Request("from_year") & "&to_day=" & Request("to_day") & "&to_month=" & Request("to_month") & "&to_year=" & Request("to_year") & "&range=" & Request("range") & "&select_date_radio=" & Request("select_date_radio") & "&"
	name="users"
	Response.Write make_pages(offset, cnt, limit, page, name, sortby) 

%>

		<table width="100%" height="100%" border="1" bordercolor="#EAE9E1" cellspacing="0" cellpadding="3">
		<tr bgcolor="#EAE9E1">
		<td class="table_title"><% response.write sorting("User name","name", sortby, offset, limit, page) %></td>
<% if (AD = 3) then %>		<td class="table_title"><% response.write sorting("Domain","name", sortby, offset, limit, page) %></td> <% end if %>
<% if (AD = 0) then %>		<td class="table_title"><% response.write sorting("Registered","reg_date", sortby, offset, limit, page) %></td> <% end if %>
		<td class="table_title">Group</td>
		<td class="table_title"><% response.write sorting("Status","last_request1", sortby, offset, limit, page) %></td>
		<td class="table_title"><% response.write sorting("Last activity","last_request", sortby, offset, limit, page) %></td>
		<td class="table_title">Sent</td>
		<td class="table_title">Received</td>
		<td class="table_title">Open Ratio</td>
		<td class="table_title">CTR</td>
		<td class="table_title">Details</td>
				</tr>
<%

'show main table
	
	if(myuname<>"") then
		if(gid<>0) then
			Set RS = Conn.Execute("SELECT users.id as id, name, domain_id, deskbar_id, reg_date, last_date, convert(varchar,last_request) as last_request1, last_request, standby, disabled FROM users INNER JOIN users_groups ON users_groups.user_id=users.id WHERE role='U' AND name LIKE N'%" & Replace(myuname, "'", "''") & "%' AND users_groups.group_id="&gid&"  ORDER BY "&sortby)
		else
			Set RS = Conn.Execute("SELECT id, name, domain_id, deskbar_id, reg_date, last_date, convert(varchar,last_request) as last_request1, last_request, standby, disabled FROM users WHERE role='U' AND name LIKE N'%" & Replace(myuname, "'", "''") & "%' ORDER BY "&sortby)
		end if
	else
		if(gid<>0) then
			Set RS = Conn.Execute("SELECT users.id as id, name, domain_id, deskbar_id, reg_date, last_date, convert(varchar,last_request) as last_request1, last_request, standby, disabled FROM users INNER JOIN users_groups ON users_groups.user_id=users.id WHERE role='U' AND name <> '' AND users_groups.group_id="&gid&"  ORDER BY "&sortby)
		else


'old			Set RS = Conn.Execute("SELECT id, name, domain_id, deskbar_id, reg_date, last_date, convert(varchar,last_request) as last_request1, last_request, standby, disabled FROM users WHERE role='U' AND name <> '' ORDER BY "&sortby)


Set RS = Conn.Execute("SELECT id, name, domain_id, reg_date, convert(varchar,last_request) as last_request1, last_request, standby, disabled FROM users WHERE role='U' AND name <> '' ORDER BY "&sortby)


		end if		
	end if
	
	num=offset
	if(Not RS.EOF) then RS.Move(offset) end if
	Do While Not RS.EOF
		num=num+1
		if((offset+limit) < num) then Exit Do end if
		if(num > offset AND offset+limit >= num) then

	reg_date=RS("reg_date")
	standby=RS("standby")
	disabled=RS("disabled")
	if(standby=1) then
		online="<img src='images/standby.gif' alt='online' width='11' height='11' border='0'>"
	else
	if(disabled=1) then
     		online="<img src='images/disabled.gif' alt='online' width='11' height='11' border='0'>"
	else
	if(Not IsNull(RS("last_request"))) then
		last_activ = CStr(RS("last_request1"))

	        mydiff=DateDiff ("n", RS("last_request"), now_db)
		if(mydiff > 10) then
			online="<img src='images/offline.gif' alt='offline' width='11' height='11' border='0'>"
		else
			online="<img src='images/online.gif' alt='online' width='11' height='11' border='0'>"
		end if
	else
		last_activ = "&nbsp;"
		online="<img src='images/offline.gif' alt='offline' width='11' height='11' border='0'>"
	end if
	end if
	end if

	if(IsNull(RS("name"))) then
	name="Unregistered"
	else
	name = RS("name")
	end if

	if (AD = 3) then 	
		domain="&nbsp;"

	if(Not IsNull(RS("domain_id"))) then
	if(RS("domain_id")<>"") then
       	Set RS3 = Conn.Execute("SELECT name FROM domains WHERE id=" & RS("domain_id"))
	if(Not RS3.EOF) then
		  domain=RS3("name")
	else
		domain="&nbsp;"
	end if
	RS3.close
	end if 
	end if
	end if
	Set RS6 = Conn.Execute("SELECT name FROM groups INNER JOIN users_groups ON groups.id=users_groups.group_id WHERE user_id="&RS("id"))	
	if(Not RS6.EOF) then
		mygroup=RS6("name")
	end if
	mmm=0
	do while not RS6.EOF
		mmm=mmm+1	
		RS6.MoveNext
	loop
	if(mmm>1) then mygroup=mygroup&", ..." end if
        Response.Write "<tr><td><A href='#' <a href='#' onclick=""javascript: window.open('user_view_details_stat.asp?id=" & CStr(RS("id")) & "','', 'status=0, toolbar=0, width=350, height=350, scrollbars=1')"">" & name & "</a></td><td align=center>"
 if (AD = 0) then 	
	Response.Write reg_date 
	Response.Write "</td><td align=center>"
 end if
 if (AD = 3) then 	
	Response.Write domain 
	Response.Write "</td><td align=center>"
 end if
        Response.Write  mygroup & "</td><td align=center>"
  mysent=0
  Set RS7 = Conn.Execute("SELECT COUNT(id) as mycnt FROM [alerts_sent_stat] WHERE [date] >= '" + from_date + "' AND [date] <= '" + to_date + "' AND user_id="&RS("id"))
  if(not RS7.EOF) then
	mysent=RS7("mycnt")
  end if

'broad
  Set RSBroad = Conn.Execute("SELECT COUNT(id) as mycnt FROM alerts WHERE create_date >= '" + from_date + "' AND create_date <= '" + to_date + "' AND type2='B'")
  if(not RSBroad.EOF) then 
	broad_cnt=RSBroad("mycnt") 
  end if
mysent=mysent+broad_cnt


  myreceived=0
  myclicks=0
  Set RS7 = Conn.Execute("SELECT COUNT(id) as mycnt FROM [alerts_received] WHERE [date] >= '" + from_date + "' AND [date] <= '" + to_date + "' AND user_id="&RS("id"))
  if(not RS7.EOF) then
	myreceived=RS7("mycnt")
  end if
  if(IsNull(myclicks)) then myclicks=0 end if
  myread=0
  Set RS7 = Conn.Execute("SELECT COUNT(id) as mycnt FROM [alerts_read] WHERE [date] >= '" + from_date + "' AND [date] <= '" + to_date + "' AND [read]='1' AND user_id="&RS("id"))
  if(not RS7.EOF) then
	myread=RS7("mycnt")
  end if
  if(mysent<>0) then
    myopen=Round(clng(myreceived)/clng(mysent)*100,2)
  else
    myopen="0"
  end if
  if(myreceived<>0) then
    myctr=Round(clng(myclicks)/clng(myreceived)*100,2)
  else
    myctr="0"
  end if
	Response.Write online & "</td><td align=center>" & last_activ & "</td><td align=center>"
        Response.Write mysent & "</td><td align=center>"
        Response.Write myreceived & "</td><td align=center>"
        Response.Write myopen & "%</td><td align=center>"
        Response.Write myctr & "%</td>"
	Response.Write "<td align=center><a href='#' onclick=""javascript: window.open('user_view_details_stat.asp?id=" & CStr(RS("id")) & "','', 'status=0, toolbar=0, width=350, height=350, scrollbars=1')""><img src='images/action_icons/edit.png' alt='view user details' width='16' height='16' border='0' hspace=5></a></td>"
	Response.Write "</tr>"
	end if
	RS.MoveNext
	Loop
	RS.Close
	page="statistics.asp?type=users&from_day=" & Request("from_day") & "&from_month=" & Request("from_month") & "&from_year=" & Request("from_year") & "&to_day=" & Request("to_day") & "&to_month=" & Request("to_month") & "&to_year=" & Request("to_year") & "&range=" & Request("range") & "&select_date_radio=" & Request("select_date_radio") & "&"
	name="users"
	Response.Write make_pages(offset, cnt, limit, page, name, sortby) 
else

Response.Write "<center><b>There are no users.</b></center>"

end if
%>
<%end if%>
<%if (Request("type")="alerts") then 
%>
Set any time period to get more detailed statistics. <br>
CTR shows ratio between clicks (on links within the alert) and total alerts. <br>
To view the list of users who haven't received the alert click "button" in Details section. <br>
To view alert's details click on the alert or "button" in Details section.<br><br>

<%

   if(Request("sortby") <> "") then 
	sortby = Request("sortby")
   else 
	sortby = "sent_date"
   end if	

if(gid<>0) then
	Set RS = Conn.Execute("SELECT COUNT(1) as mycnt FROM alerts WHERE sent_date >= '" + from_date + "' AND sent_date <= '" + to_date + "' AND type='S' AND class IN (1,16) AND sender_id="&uid)
else
	Set RS = Conn.Execute("SELECT COUNT(1) as mycnt FROM alerts WHERE sent_date >= '" + from_date + "' AND sent_date <= '" + to_date + "' AND type='S' AND class IN (1,16)")
end if
	cnt=0
	if(Not RS.EOF) then
	cnt=RS("mycnt")
	end if
	RS.Close
  j=cnt/limit
if(cnt>0) then
	page="statistics.asp?type=alerts&from_day=" & Request("from_day") & "&from_month=" & Request("from_month") & "&from_year=" & Request("from_year") & "&to_day=" & Request("to_day") & "&to_month=" & Request("to_month") & "&to_year=" & Request("to_year") & "&range=" & Request("range") & "&select_date_radio=" & Request("select_date_radio") & "&"
	name="alerts"
	Response.Write make_pages(offset, cnt, limit, page, name, sortby) 
%>
		<table width="100%" height="100%" border="1" bordercolor="#EAE9E1" cellspacing="0" cellpadding="3">
		<tr bgcolor="#EAE9E1">
		<td class="table_title">Alerts</td>
		<td class="table_title"><% response.write sorting("Date","sent_date", sortby, offset, limit, page) %></td>
		<td class="table_title"><% response.write sorting("Type","type2", sortby, offset, limit, page) %></td>
		<td class="table_title">Recipients</td>
		<td class="table_title">Sent by</td>
		<td class="table_title">Sent</td>
		<td class="table_title">Received</td>
		<td class="table_title">Read</td>
		<td class="table_title">Open Ratio</td>
		<td class="table_title">CTR</td>
		<td class="table_title">Details</td>
		</tr>
<%

'show main table
if(gid<>0) then
	Set RS = Conn.Execute("SELECT id, alert_text, type, aknown, convert(varchar,sent_date) as sent_date, type2, sender_id  FROM alerts WHERE alerts.sent_date >= '" + from_date + "' AND alerts.sent_date <= '" + to_date + "' AND type='S' AND class IN (1,16) AND sender_id="&uid&" ORDER BY "&sortby)
else
	Set RS = Conn.Execute("SELECT id, alert_text, type, aknown, convert(varchar,sent_date) as sent_date, type2, sender_id  FROM alerts WHERE alerts.sent_date >= '" + from_date + "' AND alerts.sent_date <= '" + to_date + "' AND type='S' AND class IN (1,16) ORDER BY "&sortby)
end if
	num=offset
	if(Not RS.EOF) then RS.Move(offset) end if
	Do While Not RS.EOF
		num=num+1
		if((offset+limit) < num) then Exit Do end if
		if(num > offset AND offset+limit >= num) then
		%>
		<tr><td>
		<% 
		        text=Replace (RS("alert_text"), "</p>", " ")
			text=DeleteTags(text)
			text=Left(text, 50)
			Response.Write "<A href='#' onclick=""javascript: window.open('alert_view_details_stat.asp?id=" & CStr(RS("id")) & "','', 'status=0, toolbar=0, width=350, height=350, scrollbars=1')"">" & text & "</a>"
  mysent=0

		if(RS("type2")="P") then 

  Set RS7 = Conn.Execute("SELECT COUNT(id) as mycnt FROM [alerts_sent_stat] WHERE alert_id="&RS("id"))
  if(not RS7.EOF) then
	mysent=RS7("mycnt")
  end if


			mytype="Personal" 
	Set RS6 = Conn.Execute("SELECT name FROM users INNER JOIN alerts_sent_stat ON users.id=alerts_sent_stat.user_id WHERE alerts_sent_stat.alert_id="&RS("id"))	
	if(Not RS6.EOF) then
		myrecipients=RS6("name")
	end if
	mmm=0
	do while not RS6.EOF
		mmm=mmm+1	
		RS6.MoveNext
	loop
	if(mmm>1) then myrecipients=myrecipients&", ..." end if
		end if




		if(RS("type2")="G") then 


	group_cnt=0



'	  Set RSGroup = Conn.Execute("SELECT alerts_sent_group.group_id as group_id FROM alerts_sent_group INNER JOIN alerts ON alerts.id=alerts_sent_group.alert_id WHERE create_date >= '" + from_date + "' AND create_date <= '" + to_date + "'")
	  Set RSGroup = Conn.Execute("SELECT alerts_sent_group.group_id as group_id FROM alerts_sent_group INNER JOIN alerts ON alerts.id=alerts_sent_group.alert_id WHERE alerts_sent_group.alert_id="&RS("id"))

	  Do While not RSGroup.EOF
		 group_id=RSGroup("group_id")
		  Set RSUsers = Conn.Execute("SELECT COUNT(users.id) as mycnt FROM users INNER JOIN users_groups ON users.id=users_groups.user_id WHERE users_groups.group_id="&group_id&" AND reg_date <= '" + to_date + "'")
		  if(not RSUsers.EOF) then 
			groupusers_cnt=RSUsers("mycnt") 
			group_cnt=group_cnt+groupusers_cnt
		end if
	  RSGroup.MoveNext	
	  loop
	  mysent=group_cnt



	mytype="Group" 
	Set RS6 = Conn.Execute("SELECT name FROM groups INNER JOIN alerts_group_stat ON groups.id=alerts_group_stat.group_id WHERE alerts_group_stat.alert_id="&RS("id"))	
	if(Not RS6.EOF) then
		myrecipients=RS6("name")
	end if
	mmm=0
	do while not RS6.EOF
		mmm=mmm+1	
		RS6.MoveNext
	loop
	if(mmm>1) then myrecipients=myrecipients&", ..." end if

		end if






		if(RS("type2")="B") then 


		users_cnt=0
		broad_cnt=0
		  Set RSUsers = Conn.Execute("SELECT COUNT(id) as mycnt FROM users WHERE reg_date <= '" + to_date + "'")
		  if(not RSUsers.EOF) then 
			users_cnt=RSUsers("mycnt") 
		  end if
	
		  Set RSBroad = Conn.Execute("SELECT COUNT(id) as mycnt FROM alerts WHERE create_date >= '" + from_date + "' AND create_date <= '" + to_date + "' AND type2='B'")
		  if(not RSBroad.EOF) then 
			broad_cnt=RSBroad("mycnt") 
		  end if
'		mysent=broad_cnt*users_cnt
		mysent=users_cnt


			mytype="Broadcast" 
			myrecipients="All"
		end if
	mysentby=""
	if(Not IsNull(RS("sender_id"))) then
		Set RS7 = Conn.Execute("SELECT name FROM users WHERE id="&RS("sender_id"))	
		if(Not RS7.EOF) then
			mysentby=RS7("name")
		end if
	end if

  myreceived=0
  myclicks=0
  Set RS7 = Conn.Execute("SELECT COUNT(id) as mycnt FROM [alerts_received] WHERE alert_id="&RS("id"))
  if(not RS7.EOF) then
	myreceived=RS7("mycnt")
  end if
  if(IsNull(myclicks)) then myclicks=0 end if

  myread=" "
  if(RS("aknown")=1) then
  Set RS7 = Conn.Execute("SELECT COUNT(id) as mycnt FROM [alerts_read] WHERE alert_id="&RS("id"))
  if(not RS7.EOF) then
	myread=RS7("mycnt")
  end if
  end if

'  myread=0
'  Set RS7 = Conn.Execute("SELECT COUNT(id) as mycnt FROM [alerts_read] WHERE [read]='1' AND alert_id="&RS("id"))
'  if(not RS7.EOF) then
'	myread=RS7("mycnt")
'  end if

  if(mysent<>0) then
    myopen=Round(clng(myreceived)/clng(mysent)*100,2)
  else
    myopen="0"
  end if
  if(myreceived<>0) then
    myctr=Round(clng(myclicks)/clng(myreceived)*100,2)
  else
    myctr="0"
  end if
	%>...</td><td> 
		<% Response.Write RS("sent_date") %>
		</td>
		<td> 
		<% Response.Write mytype %>
		</td>
		<td> 
		<% Response.Write myrecipients %>
		</td>
		<td> 
		<% Response.Write mysentby %>
		</td>
		<td> 
		<% Response.Write mysent %>
		</td>
		<td> 
		<% Response.Write  myreceived%>
		</td>
		<td> 
		<% Response.Write  myread%>
		</td>
		<td> 
		<% Response.Write  myopen%>%
		</td>
		<td> 
		<% Response.Write  myctr%>%
		</td>
<%
	Response.Write "<td align=center>"

  if(RS("aknown")=1) then
	response.write "<a href='#' onclick=""javascript: window.open('alert_view_alerts_read.asp?id=" & CStr(RS("id"))&"&type="&mytype&"&read=1','', 'status=0, toolbar=0, width=650, height=385, scrollbars=1')""><img src='images/read.gif' alt=""See who have read"" width='16' height='16' border='0' hspace=5></a> <a href='#' onclick=""javascript: window.open('alert_view_alerts_read.asp?id=" & CStr(RS("id"))&"&type="&mytype&"&read=0','', 'status=0, toolbar=0, width=650, height=385, scrollbars=1')""><img src='images/not_read.gif' alt=""See who haven't read"" width='16' height='16' border='0' hspace=5></a> "
  end if
	response.write "<a href='#' onclick=""javascript: window.open('alert_view_users_stat.asp?id=" & CStr(RS("id")) & "&type="&mytype&"','', 'status=0, toolbar=0, width=650, height=385, scrollbars=1')""><img src='images/not_recieved.gif' alt=""See who haven't received"" width='16' height='16' border='0' hspace=5></a> <a href='#' onclick=""javascript: window.open('alert_view_details_stat.asp?id=" & CStr(RS("id")) & "','', 'status=0, toolbar=0, width=350, height=350, scrollbars=1')""><img src='images/action_icons/edit.png' alt='view alert details' width='16' height='16' border='0' hspace=5></a></td>"

%>
		</tr>
		<%
		end if
	RS.MoveNext
	Loop
	RS.Close

	page="statistics.asp?type=alerts&from_day=" & Request("from_day") & "&from_month=" & Request("from_month") & "&from_year=" & Request("from_year") & "&to_day=" & Request("to_day") & "&to_month=" & Request("to_month") & "&to_year=" & Request("to_year") & "&range=" & Request("range") & "&select_date_radio=" & Request("select_date_radio") & "&"
	name="alerts"
	Response.Write make_pages(offset, cnt, limit, page, name, sortby) 
else

Response.Write "<center><b>There are no alerts.</b></center>"

end if
%>
<%end if%>
<% if(Request("type")="surveys") then
%>
Set any time period to get more detailed statistics. <br>
To view the list of users who haven't received the survey click "button" in Details section. <br>
To view the list of users who haven't voted click "button" in Details section. <br>
To view survey's details click on the survey or "button" in Details section.<br><br>

<%
   if(Request("sortby") <> "") then 
	sortby = Request("sortby")
   else 
	sortby = "sent_date"
   end if	

  limit=10
if(gid<>0) then
	Set RS = Conn.Execute("SELECT id FROM surveys_main WHERE surveys_main.create_date >= '" + from_date + "' AND surveys_main.create_date <= '" + to_date + "' AND closed='Y' AND sender_id="&uid)
else
	Set RS = Conn.Execute("SELECT id FROM surveys_main WHERE surveys_main.create_date >= '" + from_date + "' AND surveys_main.create_date <= '" + to_date + "' AND closed='Y'")
end if
	cnt=0
	Do While Not RS.EOF
	cnt=cnt+1
	RS.MoveNext
	Loop
	RS.Close
  j=cnt/limit
%>

<% if(cnt>0) then %>
<%

	page="statistics.asp?type=surveys&from_day=" & Request("from_day") & "&from_month=" & Request("from_month") & "&from_year=" & Request("from_year") & "&to_day=" & Request("to_day") & "&to_month=" & Request("to_month") & "&to_year=" & Request("to_year") & "&range=" & Request("range") & "&select_date_radio=" & Request("select_date_radio") & "&"
	name="surveys"
	Response.Write make_pages(offset, cnt, limit, page, name, sortby) 

%>

		<table width="100%" height="100%" border="1" bordercolor="#EAE9E1" cellspacing="0" cellpadding="3">
		<tr bgcolor="#EAE9E1">
		<td class="table_title"><% response.write sorting("Surveys","name", sortby, offset, limit, page) %></td>
		<td class="table_title"><% response.write sorting("Date","create_date", sortby, offset, limit, page) %></td>
		<td class="table_title"><% response.write sorting("Type","type", sortby, offset, limit, page) %></td>
		<td class="table_title">Recipients</td>
		<td class="table_title">Sent by</td>
		<td class="table_title">Sent</td>
		<td class="table_title">Received</td>
		<td class="table_title">Votes</td>
		<td class="table_title">CTR</td>
		<td class="table_title">Details</td>
				</tr>
<%

'show main table

if(gid<>0) then
	Set RS = Conn.Execute("SELECT id, name, type, convert(varchar,create_date) as create_date, sender_id FROM surveys_main WHERE surveys_main.create_date >= '" + from_date + "' AND surveys_main.create_date <= '" + to_date + "' AND closed='Y' AND sender_id="&uid&" ORDER BY id DESC")
else
	Set RS = Conn.Execute("SELECT id, name, type, convert(varchar,create_date) as create_date, sender_id FROM surveys_main WHERE surveys_main.create_date >= '" + from_date + "' AND surveys_main.create_date <= '" + to_date + "' AND closed='Y' ORDER BY id DESC")
end if
	num=offset
	if(Not RS.EOF) then RS.Move(offset) end if
	Do While Not RS.EOF
		num=num+1
		if((offset+limit) < num) then Exit Do end if
		if(num > offset AND offset+limit >= num) then
myrecipients="222"
mysentby="333"
mysent="444"
myreceived="555"
myctr="888"

		%>
		<tr><td>
		<% 
		        text=RS("name")
			Response.Write "<a href='#' onclick=""javascript: window.open('survey_view_details_stat.asp?id=" & CStr(RS("id")) & "','', 'status=0, toolbar=0, width=350, height=350, scrollbars=1')"">" & text & "</a>"
		%>
		</td><td> 
		<% Response.Write RS("create_date") %>
		</td><td> 
		<% 
			if(RS("type")="B") then
				Response.Write "Broadcast"
				myrecipients="All"
			end if
			if(RS("type")="G") then
				Response.Write "Group"
				Set RS6 = Conn.Execute("SELECT name FROM groups INNER JOIN surveys_group_stat ON groups.id=alerts_group_stat.group_id WHERE alerts_group_stat.survey_id="&RS("id"))	
				if(Not RS6.EOF) then
					myrecipients=RS6("name")
				end if
				mmm=0
				do while not RS6.EOF
					mmm=mmm+1	
					RS6.MoveNext
				loop
				if(mmm>1) then myrecipients=myrecipients&", ..." end if
			end if

		mysentby=""
		if(Not IsNull(RS("sender_id"))) then
			Set RS7 = Conn.Execute("SELECT name FROM users WHERE id="&RS("sender_id"))	
			if(Not RS7.EOF) then
				mysentby=RS7("name")
			end if
		end if

  mysent=0
  Set RS7 = Conn.Execute("SELECT id FROM [alerts_sent_stat] WHERE survey_id="&RS("id"))
  do while not RS7.EOF
	mysent=mysent+1
  RS7.MoveNext	
  loop


group_cnt=0
  Set RSGroup = Conn.Execute("SELECT alerts_sent_group.group_id as group_id FROM alerts_sent_group INNER JOIN surveys_main ON surveys_main.sender_id=alerts_sent_group.alert_id WHERE create_date >= '" + from_date + "' AND create_date <= '" + to_date + "'")
  Do While not RSGroup.EOF
	 group_id=RSGroup("group_id")
	  Set RSUsers = Conn.Execute("SELECT COUNT(users.id) as mycnt FROM users INNER JOIN users_groups ON users.id=users_groups.user_id WHERE users_groups.group_id="&group_id&" AND reg_date <= '" + to_date + "'")
	  if(not RSUsers.EOF) then 
		groupusers_cnt=RSUsers("mycnt") 
		group_cnt=group_cnt+groupusers_cnt
	end if
  RSGroup.MoveNext	
  loop
  mysent=mysent+group_cnt

  myreceived=0
  myvotes=0
  Set RS7 = Conn.Execute("SELECT id, vote FROM [alerts_received] WHERE survey_id="&RS("id"))
  do while not RS7.EOF
	myreceived=myreceived+1
	if(RS7("vote")=1) then myvotes=myvotes+1 end if
  RS7.MoveNext	
  loop
  if(mysent<>0) then
    myopen=Round(clng(myreceived)/clng(mysent)*100,2)
  else
    myopen="0"
  end if
  if(myreceived<>0) then
    myctr=Round(clng(myvotes)/clng(myreceived)*100,2)
  else
    myctr="0"
  end if	%>
		</td>
		<td> 
		<% Response.Write myrecipients %>
		</td>
		<td> 
		<% Response.Write mysentby %>
		</td>
		<td> 
		<% Response.Write mysent %>
		</td>
		<td> 
		<% Response.Write  myreceived%>
		</td>
                <td> 
		<% Response.Write  myvotes%>
		</td>
      		<td> 
		<% Response.Write  myctr%>%
		</td>
<%
	Response.Write "<td align=center><a href='#' onclick=""javascript: window.open('survey_view_users_stat.asp?id=" & CStr(RS("id")) & "','', 'status=0, toolbar=0, width=650, height=385, scrollbars=1')""><img src='images/not_recieved.gif' alt=""See who haven't received"" width='16' height='16' border='0' hspace=5></a> <a href='#' onclick=""javascript: window.open('survey_view_users_stat.asp?id=" & CStr(RS("id")) & "&read=1','', 'status=0, toolbar=0, width=650, height=385, scrollbars=1')""><img src='images/not_read.gif' alt=""See who haven't voted"" width='16' height='16' border='0' hspace=5></a> <a href='#' onclick=""javascript: window.open('survey_view_details_stat.asp?id=" & CStr(RS("id")) & "','', 'status=0, toolbar=0, width=350, height=350, scrollbars=1')""><img src='images/action_icons/edit.png' alt='view alert details' width='16' height='16' border='0' hspace=5></a></td>"
%>

		</tr>
		<%
		end if
	RS.MoveNext
	Loop
	RS.Close

	page="statistics.asp?type=surveys&from_day=" & Request("from_day") & "&from_month=" & Request("from_month") & "&from_year=" & Request("from_year") & "&to_day=" & Request("to_day") & "&to_month=" & Request("to_month") & "&to_year=" & Request("to_year") & "&range=" & Request("range") & "&select_date_radio=" & Request("select_date_radio") & "&"
	name="surveys"
	Response.Write make_pages(offset, cnt, limit, page, name, sortby) 
%>
         </table>
<% 
else

Response.Write "<center><b>There are no surveys.</b></center>"

end if %>
<%end if%>
&nbsp;</td></tr></table>
		</td>
		</tr>
	</table>
	</td>
	</tr>
</table>

<%
else

	Response.Redirect "index.asp"

end if

%> 
</body>
</html>
<!-- #include file="db_conn_close.asp" -->