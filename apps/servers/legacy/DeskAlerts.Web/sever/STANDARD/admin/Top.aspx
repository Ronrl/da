﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Top.aspx.cs" Inherits="DeskAlertsDotNet.Pages.Top" %>

<%@ Import Namespace="DeskAlertsDotNet.Parameters" %>
<%@ Import Namespace="Resources" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>DeskAlerts</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="css/style9.css?v=9.1.12.28" rel="stylesheet" type="text/css" />
    <link href="css/jquery/jquery_timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="functions.js"></script>
    <script type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
    <script type="text/javascript" src="jscripts/jquery/jquery_timepicker/jquery-ui-timepicker-addon.js"></script>
    <script type="text/javascript" src="jscripts/date.js"></script>
    <script type="text/javascript">
        $(function () {
            $("#logout").button({
                icons: {
                    primary: "ui-icon-key"
                }
            });
            $("#helpOpener").button({
                icons: {
                    primary: "ui-icon-lightbulb"
                }
            });
            $("#addons_trial_info").button({
                icons: {
                    primary: "ui-icon-mail-closed"
                }, text: false
            });
            $('.buy_addons_btn').button({
                icons: {
                    primary: "ui-icon-cart"
                }, text: false
            });
            $('.settings_button').button({
                icons: {
                    primary: "ui-icon-gear"
                }, text: false
            });
        });

        function toggleMainMenu() {
            window.parent.toggleMainMenu();
        }
    </script>

</head>
<body class="top_body" style="margin: 0px">
    <form id="form1" runat="server">
        <div>
            <table border="0" cellspacing="0" cellpadding="0" width="100%" height="62px">
                <tr valign="middle">
                    <td valign="middle">
                        <input class="ui-button block vertial_align_middle" type="image" src="images/menu.png" onclick="toggleMainMenu()" />
                        <img class="block vertial_align_middle" src="images/top_logo.gif" alt="DeskAlerts Control Panel" border="0" />
                    </td>
                    <td align="center" width="400px" visible="false" id="demo" runat="server"><b><%=resources.LNG_YOU_CAN_DOWNLOAD_DEMO_CLIENT_FOR_YOUR_OPERATING_SYSTEM%>:</b><br />
                        <asp:HyperLink ID="windows" runat="server" Visible="true" Text="" NavigateUrl="client/deskalerts_setup.msi"></asp:HyperLink>|
                    <asp:HyperLink ID="macOSX" runat="server" Visible="true" Text="" NavigateUrl="client/deskalerts_setup.zip"></asp:HyperLink>|
                    <asp:HyperLink Target="_blank" ID="android" runat="server" Visible="true" Text="" NavigateUrl="https://play.google.com/store/apps/details?id=com.toolbarstudio.alerts"></asp:HyperLink>|
                    <asp:HyperLink Target="_blank" ID="iOS" runat="server" Visible="true" Text="" NavigateUrl="https://itunes.apple.com/app/mobile-alert-client/id592345705"></asp:HyperLink>
                        <%=resources.LNG_SYSTEM_CONFIGURATION%>
                    </td>
                    <td>
                        <div id="newsBanner"></div>
                    </td>
                    <td align="right" valign="middle" style="padding-right: 4px; white-space: nowrap">
                        <a href="SettingsSelection.aspx" id="settingsButton" class="settings_button" target="work_area" title="Settings">&nbsp;</a>
                        <asp:LinkButton runat="server" Visible="False" ID="buyButton" href="BuyAddons.aspx?use_local=1" class="buy_addons_btn" target="work_area" title="DeskAlers store">&nbsp;</asp:LinkButton>
                        <a runat="server" id="helpOpener" target="work_area" class="logout" style="text-align: center">
                            <span id="help_close_span" style="display: none">
                                <%=resources.LNG_CLOSE %>&nbsp;
                            </span><%=resources.LNG_HELP %>
                        </a>
                        <a onclick="window.parent.logoutClick()" target="_top" id="logout" class="logout"><%=resources.LNG_LOGOUT%></a>
                        <div style="width: 100%; text-align: right; white-space: nowrap; padding-top: 5px;">
                            <% =resources.LNG_CURRENTLY_LOGGED_AS + ": <span class='username'>"+ _user.Name +"</span>" %>
                        </div>
                    </td>
                </tr>
            </table>
        </div>        
    </form>
</body>
</html>
