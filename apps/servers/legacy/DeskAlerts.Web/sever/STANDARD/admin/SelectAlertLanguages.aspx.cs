﻿using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Models.Permission;
using DeskAlertsDotNet.Parameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DeskAlertsDotNet.Pages
{
    using Resources;

    public partial class SelectAlertLanguages : DeskAlertsBaseListPage
    {

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (this.Content.IsEmpty)
            {
                Response.Redirect("CreateAlert.aspx", true);
            }


            if (!string.IsNullOrEmpty(Request.QueryString.ToString()))
            {
                var queryString = "?" + Request.QueryString.ToString();
                form1.Action += queryString;
                form1.Action = form1.Action;
            }


            languageNameHeaderCell.Text = resources.LNG_LANGUAGE_NAME;

            int cnt = this.Content.Count < (Offset + Limit) ? this.Content.Count : Offset + Limit;

            for (int i = Offset; i < cnt; i++)
            {
                DataRow row = this.Content.GetRow(i);
                TableRow tableRow = new TableRow();
                bool isDefault = row.GetInt("is_default") == 1;
      
                    CheckBox chb = GetCheckBoxForDelete(row);

                    chb.ClientIDMode = ClientIDMode.Static;
                    chb.ID = "lang_" + row.GetString("id");
                    chb.Enabled = !isDefault;
                    chb.Checked = isDefault;
                    TableCell checkBoxCell = new TableCell();
                    checkBoxCell.HorizontalAlign = HorizontalAlign.Center;
                    checkBoxCell.Controls.Add(chb);
                    tableRow.Cells.Add(checkBoxCell);
                

                TableCell languageName = new TableCell
                {
                    Text = row.GetString("name")
                };

                tableRow.Cells.Add(languageName);


                contentTable.Rows.Add(tableRow);
            }
        }

        #region DataProperties 
        protected override string DataTable
        {
            get
            {

                //return your table name from this property
                return "alerts_langs";
            }
        }

        protected override string[] Collumns
        {
            get
            {

                //return array of collumn`s names of table from data table
                return new string[]
                {
                    "id",
                    "name",
                    "abbreviatures",
                    "is_default"
                };

            }
        }

        protected override string DefaultOrderCollumn
        {
            //return collumn for default order
            get
            {
                return "id";

            }
        }


        protected override string DefaultSortingOrder => "ASC";
        /*
         * 
         * If you want to add condition for data selection override property ConditionSqlString 
         * Also you can implement your custom query by overriding properties CustomQuery and CustomCountQuery
         *  
         */
        #endregion

        #region Interface properties

        protected override IButtonControl[] DeleteButtons
        {
            get
            {
                return new IButtonControl[] { };
            }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl NoRowsDiv
        {
            get { return NoRows; }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl TableDiv
        {
            get { return mainTableDiv; }
        }

        protected override string ContentName
        {
            get
            {
                //return content name here
                return "DeskAlerts Content List";
            }
        }

        #endregion
    }
}