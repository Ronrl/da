<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%
check_session()
%>
<!-- #include file="Common.asp" -->
<!-- #include file="header.asp" -->
<!-- #include file="footer.asp" -->
<%
'On Error Resume Next
' index CustomIncludes End
'-------------------------------

'===============================
' Save Page and File Name available into variables
'-------------------------------
Dim sFileName
Dim sTemplateFileName
Dim sPaginateFileName
Dim intSessionUserId

sFileName = "preview.asp"
sTemplateFileName = "preview_alert.html"
sPreviewFileName = "preview.html"
'===============================
  

intSessionUserId = Session("uid")
uid = intSessionUserId
if (intSessionUserId <> "") then


' index Show begin

'===============================
' Display page
'-------------------------------
' Load HTML template for this page
'-------------------------------
LoadTemplate alerts_dir & "\" & templateFolder & "\" & sTemplateFileName, "main"
LoadTemplate alerts_dir & "\" & templateFolder & "\" & sPreviewFileName, "Preview"
'-------------------------------
' Load HTML template of Header and Footer
'-------------------------------
LoadTemplate sHeaderFileName, "Header"
LoadTemplate sFooterFileName, "Footer"
'-------------------------------
SetVar "Menu", ""
SetVar "DateForm", ""

SetVar "LNG_TITLE", LNG_TITLE
SetVar "LNG_DATE", LNG_DATE
SetVar "LNG_CREATION_DATE", LNG_CREATION_DATE
SetVar "default_lng", default_lng

'editor ---
	Set RS = Conn.Execute("SELECT id FROM users WHERE id="&uid&" AND role='E'")
	if(Not RS.EOF) then
		gid = 1
		Set rs_policy = Conn.Execute("SELECT type FROM policy INNER JOIN policy_editor ON policy.id=policy_editor.policy_id WHERE editor_id=" & uid)
		if(Not rs_policy.EOF) then
			if(rs_policy("type")="A") then
				gid = 0
			end if
		end if
	else
		gid = 0
	end if
	RS.Close
'--------

	alert_id=Clng(request("id"))

	Set RS = Conn.Execute("SELECT id, alert_text, title, type, aknown, sent_date, type2, sender_id, class FROM alerts WHERE id=" & alert_id)
	if(RS.EOF) then
		Set RS = Conn.Execute("SELECT id, alert_text, title, type, aknown, sent_date, type2, sender_id, class FROM archive_alerts WHERE id=" & alert_id)
	end if
	if(Not RS.EOF) then
		classid = RS("class")
		
		if classid = "5" then
			PageName(LNG_RSS_DETAILS)
		elseif classid = "2" then
			PageName(LNG_SS_DETAILS)
		elseif classid = "8" then
			PageName(LNG_WP_DETAILS)
		else
			PageName(LNG_ALERT_DETAILS)
		end if

		strAlertDate = RS("sent_date")&""
	    strAlertPreview = RS("alert_text")&""
		strAlertTitle = RS("title")&""

		SetVar "strAlertTitle", strAlertTitle
		SetVar "strAlertPreview", strAlertPreview
		SetVar "strAlertDate", strAlertDate
		SetVar "alert_id", alert_id
		SetVar "alerts_folder", alerts_folder

	end if

SetVar "PreviewStr", LNG_PREVIEW
'--------------------


'-------------------------------
' Step through each form
'-------------------------------
Header_Show
Footer_Show
'-------------------------------
' Process page templates
'-------------------------------
Parse "Header", False
Parse "Footer", False
Parse "main", False
'-------------------------------
' Output the page to the browser
'-------------------------------
Response.write PrintVar("main")

' index Show End

'-------------------------------
' Destroy all object variables
'-------------------------------

' index Close Event begin
' index Close Event End

UnloadTemplate
'===============================

else

Login(Request("intErrorCode"))
'response.write "test111222"

end if

'===============================
' Display Grid Form
'-------------------------------
%><!-- #include file="db_conn_close.asp" -->