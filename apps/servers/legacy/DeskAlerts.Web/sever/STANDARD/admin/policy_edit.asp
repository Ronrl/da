<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<!-- #include file="policy_ui.asp" -->
<%
check_session()

uid = Session("uid")

if (uid <> "")  then
	policy_id=0
	if( Request ("sub1") = "1") then
		if Request("all_recipients") = "1" then
			ids_users = Array("0")
			ids_computers = Array("0")
			ids_groups = Array()
			ids_ous = Array()
			
		else
			ids_users = split(request("ids_users"),",")
			ids_computers = split(request("ids_computers"),",")
			ids_groups = split(request("ids_groups"),",")
			ids_ous = split(request("ids_ous"),",")
			
		end if
		
    if request("all_templates") <> "1" then
		ids_tgroups = split(request("ids_tgroups"),",")
    else
        ids_tgroups = Array("0")
    end if
		
		if Request("view_all") = "1" then
			ids_viewers = Array("0")
		else
			ids_viewers = split(request("ids_viewers"),",")
		end if
		
		name=Replace(Request("policy_name"), "'", "''")
		policy_type=Request("policy_type")

		alerts_val = p_makeString("alerts")
		emails_val = p_makeString("emails")
		sms_val = p_makeString("sms")
		surveys_val = p_makeString("surveys")
		users_val = p_makeString("users")
		groups_val = p_makeString("groups")
		templates_val = p_makeString("templates")
		text_templates_val = p_makeString("text_templates")
		statistics_val = p_makeString("statistics")
		ip_groups_val = p_makeString("ip_groups")
		rss_val = p_makeString("rss")
		screensavers_val = p_makeString("screensavers")
		wallpapers_val = p_makeString("wallpapers")
                lockscreens_val = p_makeString("lockscreens")
		im_val = p_makeString("im")
        text_to_call_val = p_makeString("text_to_call")
        feedback_val = p_makeString("feedback")
        cc_val = p_makeString("cc")
        webplugin_val = p_makeString("webplugin")
        campaign_val = p_makeString("campaign")

		if(Request("edit")="1") then
			policy_id=Clng(Request("id"))
			SQL = "UPDATE policy SET name=N'"& name &"', [type]='"& policy_type &"' WHERE id=" & policy_id
			Conn.Execute(SQL)
			set rs_list = Conn.Execute("select policy_id FROM policy_list WHERE policy_id=" & policy_id)
			if(Not rs_list.EOF) then
				SQL = "UPDATE policy_list SET alerts_val='"& alerts_val &"', emails_val='"& emails_val &"', sms_val='"& sms_val &"', surveys_val='"& surveys_val &"', users_val='"& users_val &"', groups_val='"& groups_val &"', templates_val='"& templates_val &"', text_templates_val='"& text_templates_val &"', statistics_val='"& statistics_val &"', ip_groups_val='"& ip_groups_val &"', rss_val='"& rss_val &"', screensavers_val='"& screensavers_val &"', wallpapers_val='"& wallpapers_val &"', lockscreens_val='"& lockscreens_val &"', im_val='"& im_val &"', text_to_call_val='"& text_to_call_val &"', feedback_val='"& feedback_val &"', cc_val='"& cc_val &"', webplugin_val='"& webplugin_val &"', campaign_val='" & campaign_val &"' WHERE policy_id=" & policy_id
				Conn.Execute(SQL)
			else
				SQL = "INSERT INTO policy_list (policy_id, alerts_val, emails_val, sms_val, surveys_val, users_val, groups_val, templates_val, text_templates_val, statistics_val, ip_groups_val, rss_val, screensavers_val, wallpapers_val, lockscreens_val, im_val, text_to_call_val, feedback_val, cc_val, webplugin_val) VALUES ("& policy_id &" ,'"& alerts_val &"', '"& emails_val &"', '"& sms_val &"', '"& surveys_val &"', '"& users_val &"', '"& groups_val &"', '"& templates_val &"', '"& text_templates_val &"', '"& statistics_val &"', '"& ip_groups_val &"', '"& rss_val &"', '"& screensavers_val &"', '"& wallpapers_val &"', '"& lockscreens_val &"', '"& im_val &"', '"& text_to_call_val &"', '"& feedback_val &"', '"& cc_val &"', '"& webplugin_val &"', '"& campaign_val &"')"
				Conn.Execute(SQL)
			end if
		else
			SQL = "INSERT INTO policy (name, [type]) VALUES (N'"&name&"', '"&policy_type&"')"
			Conn.Execute(SQL)
			Set rs1 = Conn.Execute("select @@IDENTITY newID from policy")
            policy_id=Clng(rs1("newID"))
			rs1.close
			if(policy_id<>0) then
				SQL = "INSERT INTO policy_list (policy_id, alerts_val, emails_val, sms_val, surveys_val, users_val, groups_val, templates_val, text_templates_val, statistics_val, ip_groups_val, rss_val, screensavers_val, wallpapers_val, lockscreens_val, im_val, text_to_call_val, feedback_val, cc_val, webplugin_val) VALUES (" &policy_id &" ,'"&alerts_val&"', '"&emails_val&"', '"&sms_val&"', '"&surveys_val&"', '"&users_val&"', '"&groups_val&"', '"&templates_val&"', '"& text_templates_val &"', '"&statistics_val&"', '"& ip_groups_val &"', '"& rss_val &"', '"& screensavers_val &"', '"& wallpapers_val &"', '"& lockscreens_val &"', '"& im_val &"', '"& text_to_call_val &"', '"& feedback_val &"', '"& cc_val &"', '"& webplugin_val &"')"
				Conn.Execute(SQL)
			end if
		end if

		if(policy_id<>0) then
			SQL = "UPDATE policy_user SET was_checked=0 WHERE policy_id="&policy_id
			Conn.Execute(SQL)
		
			for i_users = LBound(ids_users) to UBound(ids_users)
				user_id=ids_users(i_users)
				if(user_id<>"") then
					Set RS1 = Conn.Execute("SELECT id FROM policy_user WHERE user_id=" & user_id &" AND policy_id="&policy_id)
					if(RS1.EOF) then
						SQL = "INSERT INTO policy_user (user_id, policy_id, was_checked) VALUES (" & user_id & ", "& policy_id &", 1)"
						Conn.Execute(SQL)
					else
						SQL = "UPDATE policy_user SET was_checked=1 WHERE id=" & RS1("id")
						Conn.Execute(SQL)
						RS1.Close
					end if
				end if
			next
	
			SQL = "DELETE FROM policy_user WHERE was_checked=0 AND policy_id=" & policy_id
			Conn.Execute(SQL)
		
			SQL = "UPDATE policy_computer SET was_checked=0 WHERE policy_id="&policy_id
			Conn.Execute(SQL)
		
			for i_computers = LBound(ids_computers) to UBound(ids_computers)
				computer_id=ids_computers(i_computers)
				if(computer_id<>"") then
					Set RS1 = Conn.Execute("SELECT id FROM policy_computer WHERE comp_id=" & computer_id &" AND policy_id="&policy_id)
					if(RS1.EOF) then
						SQL = "INSERT INTO policy_computer (comp_id, policy_id, was_checked) VALUES (" & computer_id & ", "& policy_id &", 1)"
						Conn.Execute(SQL)
					else
						SQL = "UPDATE policy_computer SET was_checked=1 WHERE id=" & RS1("id")
						Conn.Execute(SQL)
						RS1.Close
					end if
				end if
			next
		
			SQL = "DELETE FROM policy_computer WHERE was_checked=0 AND policy_id=" & policy_id
			Conn.Execute(SQL)
	
			SQL = "UPDATE policy_group SET was_checked=0 WHERE policy_id="&policy_id
			Conn.Execute(SQL)
	
			for i_groups = LBound(ids_groups) to UBound(ids_groups)
				group_id=ids_groups(i_groups)
				if(group_id<>"") then
					Set RS1 = Conn.Execute("SELECT id FROM policy_group WHERE group_id=" & group_id &" AND policy_id="&policy_id)
					if(RS1.EOF) then
							SQL = "INSERT INTO policy_group (group_id, policy_id, was_checked) VALUES (" & group_id & ", "& policy_id &", 1)"
						Conn.Execute(SQL)
					else
							SQL = "UPDATE policy_group SET was_checked=1 WHERE id=" & RS1("id")
						Conn.Execute(SQL)
						RS1.Close
					end if
				end if
			next
		
			SQL = "DELETE FROM policy_group WHERE was_checked=0 AND policy_id=" & policy_id
			Conn.Execute(SQL)
		
			SQL = "UPDATE policy_ou SET was_checked=0 WHERE policy_id="&policy_id
			Conn.Execute(SQL)
		
			for i_ous = LBound(ids_ous) to UBound(ids_ous)
				ou_id=ids_ous(i_ous)
				if(ou_id<>"") then
					Set RS1 = Conn.Execute("SELECT id FROM policy_ou WHERE ou_id=" & ou_id &" AND policy_id="&policy_id)
					if(RS1.EOF) then
						SQL = "INSERT INTO policy_ou (ou_id, policy_id, was_checked) VALUES (" & ou_id & ", "& policy_id &", 1)"
						Conn.Execute(SQL)
					else
						SQL = "UPDATE policy_ou SET was_checked=1 WHERE id=" & RS1("id")
						Conn.Execute(SQL)
						RS1.Close
					end if
				end if
			next
	
			SQL = "DELETE FROM policy_ou WHERE was_checked=0 AND policy_id=" & policy_id
			Conn.Execute(SQL)
			
 if ConfTGroupEnabled = 1 then 
			SQL = "UPDATE policy_tgroups SET was_checked=0 WHERE policy_id= " & policy_id
			Conn.Execute(SQL)

		   
			for i_tgroup = LBound(ids_tgroups) to UBound(ids_tgroups)
				id_tgroup=ids_tgroups(i_tgroup)
				if(id_tgroup<>"") then
					Set RS1 = Conn.Execute("SELECT id FROM policy_tgroups WHERE tgroup_id=" & id_tgroup &" AND policy_id="&policy_id)
					if(RS1.EOF) then
						SQL = "INSERT INTO policy_tgroups (tgroup_id, policy_id, was_checked) VALUES (" & id_tgroup & ", "& policy_id &", 1)"
						Conn.Execute(SQL)
					else
						SQL = "UPDATE policy_tgroups SET was_checked=1 WHERE id=" & RS1("id")
						Conn.Execute(SQL)
						RS1.Close
					end if
				end if
			next
			
			SQL = "DELETE FROM policy_tgroups WHERE was_checked=0 AND policy_id=" & policy_id
			Conn.Execute(SQL)
 end if
			
			
			
			if UBound(ids_viewers) = -1 then
				Conn.Execute("DELETE FROM policy_viewer WHERE policy_id = " & policy_id)
			else
				Conn.Execute("DELETE FROM policy_viewer WHERE policy_id = " & policy_id &" AND NOT editor_id IN ("& Join(ids_viewers,",") &")")
				for i_viewers = LBound(ids_viewers) to UBound(ids_viewers)
					viewer_id=ids_viewers(i_viewers)
					if viewer_id <> "" then
						Conn.Execute(_
							"IF NOT EXISTS (SELECT 1 FROM policy_viewer WHERE policy_id = "& policy_id &" AND editor_id = "& viewer_id &") " &_
							"INSERT INTO policy_viewer (policy_id, editor_id) VALUES ("& policy_id &", "& viewer_id &")")
					end if
				next
			end if
		end if
		
		if Request("close")<>"1" then
			Response.Redirect "PolicyList.aspx"
		else
			Response.Redirect "close_window.asp"
		end if
	else
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>DeskAlerts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/style9.css" rel="stylesheet" type="text/css">
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script language="javascript" type="text/javascript">

$(document).ready(function() {
	$("#tabs").tabs();
	$(".save_button").button();
	$(".cancel_button").button();
});

function my_sub(j)
{
	if(document.getElementById('policy_name').value=="") 
	{
		alert("<%=LNG_PLEASE_ENTER_POLICY_NAME %>");
		return false;
	}
	
	if(document.getElementById('policy_reg').checked==true)
	{
		var i;
		var fl=0;
		for(i=0; i<=5; i++){
			if(document.getElementById("alerts_"+i)!=null){
				if(document.getElementById("alerts_"+i).checked==true){ fl=1; }
			}
			if(document.getElementById("emails_"+i)!=null){
				if(document.getElementById("emails_"+i).checked==true){ fl=1; }
			}
			if(document.getElementById("sms_"+i)!=null){
				if(document.getElementById("sms_"+i).checked==true){ fl=1; }
			}
			if(document.getElementById("surveys_"+i)!=null){
				if(document.getElementById("surveys_"+i).checked==true){ fl=1; }
			}
			if(document.getElementById("users_"+i)!=null){
				if(document.getElementById("users_"+i).checked==true){ fl=1; }
			}
			if(document.getElementById("groups_"+i)!=null){
				if(document.getElementById("groups_"+i).checked==true){ fl=1; }
			}
			if(document.getElementById("templates_"+i)!=null){
				if(document.getElementById("templates_"+i).checked==true){ fl=1; }
			}
			if(document.getElementById("statistics_"+i)!=null){
				if(document.getElementById("statistics_"+i).checked==true){ fl=1; }
			}
			if(document.getElementById("ip_groups_"+i)!=null){
				if(document.getElementById("ip_groups_"+i).checked==true){ fl=1; }
			}
			if(document.getElementById("rss_"+i)!=null){
				if(document.getElementById("rss_"+i).checked==true){ fl=1; }
			}
			if(document.getElementById("screensavers_"+i)!=null){
				if(document.getElementById("screensavers_"+i).checked==true){ fl=1; }
			}
			if(document.getElementById("wallpapers_"+i)!=null){
				if(document.getElementById("wallpapers_"+i).checked==true){ fl=1; }
			}
			if(document.getElementById("im_"+i)!=null){
				if(document.getElementById("im_"+i).checked==true){ fl=1; }
			}
			if(document.getElementById("text_to_call_"+i)!=null){
				if(document.getElementById("text_to_call_"+i).checked==true){ fl=1; }
			}
			if(document.getElementById("feedback_"+i)!=null){
				if(document.getElementById("feedback_"+i).checked==true){ fl=1; }
			}
			if(document.getElementById("cc_"+i)!=null){
				if(document.getElementById("cc_"+i).checked==true){ fl=1; }
			}
			
			//if(document.getElementById("campaign_"+i)!=null){
				//if(document.getElementById("campaign_"+i).checked==true){ fl=1; }
			//}
		}
		if(fl==0) {
			alert("<%=LNG_PLEASE_CHECK_ANY_ACTION_FOR_EDITOR %>");
			return false;
		}
	}

	var list2 = document.getElementById("list2");
	var ids_users = document.getElementById("ids_users");
	var ids_groups = document.getElementById("ids_groups");
	
<% if AD = 3 then %>
	var ids_ous = document.getElementById("ids_ous");
	var ids_ous_string="";

	var ids_computers = document.getElementById("ids_computers");
	var ids_computers_string="";
<% end if %>

	var ids_users_string="";
	var ids_groups_string="";

	for (i=0; i<list2.options.length; i++)
	{
		if(list2.options[i].value.indexOf("groups_")==0){
			ids_groups_string = ids_groups_string +","+ list2.options[i].value.replace(/groups_/,"");
		}
		if(list2.options[i].value.indexOf("users_")==0){
			ids_users_string = ids_users_string +","+ list2.options[i].value.replace(/users_/,"");
		}
<% if AD = 3 then %>
		if(list2.options[i].value.indexOf("ous_")==0){
			ids_ous_string = ids_ous_string +","+ list2.options[i].value.replace(/ous_/,"");
		}
		if(list2.options[i].value.indexOf("computers_")==0){
			ids_computers_string = ids_computers_string +","+ list2.options[i].value.replace(/computers_/,"");
		}
<% end if %>
	}

	ids_users.value=ids_users_string;
	ids_groups.value=ids_groups_string;

<% if AD = 3 then %>
	ids_ous.value=ids_ous_string;
	ids_computers.value=ids_computers_string;
<% end if %>

	var ids_viewers = document.getElementById("list2");
	var ids_viewers = document.getElementById("ids_viewers");
	var ids_viewers_string="";
<% if ConfTGroupEnabled = 1 then %>
    var groups_list = document.getElementById("selected_tgroups");
    var ids_tgroups = document.getElementById("ids_tgroups");
    var ids_tgroups_string = "";
    for (i = 0; i < groups_list.options.length; i++)
    {
        if(i > 0)
            ids_tgroups_string += ","
        ids_tgroups_string += groups_list.options[i].value.replace(/users_/,"");;
    }
    ids_tgroups.value= ids_tgroups_string;
<% end if %>

	document.my_form.sub1.value=j;
	return true;
}

function check_role()
{
	if(document.getElementById('policy_reg').checked){
		document.getElementById('policy_tabs').style.display="";
	}
	else{
		document.getElementById('policy_tabs').style.display="none";
	}
}

function all_recipients_click(state)
{
	document.getElementById('recipients_table').style.display = state ? "none" : "";
}

function all_tgroups_click(state)
{
	document.getElementById('tgroups').style.display = state ? "none" : "";
}

function view_all_click(state)
{
	//do nothing now
}
</script>
</head>
<body class="body" style="margin:0">
<%
	id = Request("id")

	p_sys_checked = "checked"
	p_reg_checked = ""
	p_mod_checked = ""
	all_recipients = 1
	view_all = 1
	all_templates = 1

	if id <> "" then
		SQL = "SELECT p.name, p.type, pu.user_id, " &_
			"(SELECT MIN(editor_id) FROM policy_viewer WHERE policy_id = p.id) AS editor_id " &_
			"FROM policy p " &_
			"LEFT JOIN policy_user pu ON pu.policy_id = p.id AND pu.user_id = 0 " &_
			"WHERE p.id = " & CLng(id)
		set rs = Conn.Execute(SQL)
		if not rs.EOF then
			policy_name = rs("name")
			policy_type = rs("type")
			if policy_type = "E" then
				p_sys_checked = ""
				p_reg_checked = "checked"
			end if
			if policy_type = "M" and ConfEGSModerator = 1 then
			    p_sys_checked = ""
			    p_reg_checked = ""
			    p_mod_checked = "checked"
			end if
			if IsNull(rs("user_id")) then
				all_recipients = 0
			end if
			if IsNull(rs("editor_id")) then
				view_all = 0
			else
				if rs("editor_id") <> "0" then
					view_all = 2
				end if
			end if
		end if
		
		SQL = "SELECT count(1)  as cnt FROM policy_tgroups WHERE policy_id = " & id & " AND tgroup_id = 0"
		Set countSet = Conn.Execute(SQL)
		
		if CLng(countSet("cnt") <= 0) then
		 all_templates = 0
		end if
	end if
%>
<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
<tr>
	<td>
		<table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
		<tr>
			<td width="100%" height="31" class="main_table_title"><img src="images/menu_icons/policies_20.png" alt="" width="20" height="20" border="0" style="padding-left:7px">
		<a href="policy_list.asp" class="header_title" style="position:absolute;top:15px"><%=LNG_POLICIES %></a></td>
		</tr>
		<tr>
			<td class="main_table_body" height="100%">
				<div style="padding:10px;">
					<span class="work_header"><%=LNG_ADD_POLICY %></span>
					<br/><br/>
					<form name="my_form" method="post" action="policy_edit.asp">
						<input type="hidden" name="close" value="<%=Request("close")%>"/>
						<input type="hidden" name="sub1" value=""/>
						<input type="hidden" name="ids_users" id="ids_users" value=""/>
						<input type="hidden" name="ids_groups" id="ids_groups" value=""/>
<% if AD = 3 then %>
						<input type="hidden" name="ids_ous" id="ids_ous" value=""/>
						<input type="hidden" name="ids_computers" id="ids_computers" value=""/>
						
<% end if %>
<% if ConfTGroupEnabled = 1 then %>
						<input type="hidden" name="ids_tgroups" id="ids_tgroups" value="" />
<% end if %>
						<input type="hidden" name="ids_viewers" id="ids_viewers" value=""/>
						<%=LNG_POLICY_NAME %>: <input type="text" id="policy_name" name="policy_name" value="<%= HtmlEncode(policy_name) %>"><br/><br/>
						<b><%=LNG_POLICY_ROLE %>:</b><br/><br/>
						<input type="radio" name="policy_type" id="policy_sys" value="A" <%=p_sys_checked %> onclick="check_role()"> <label for="policy_sys"><%=LNG_SYSTEM_ADMINISTRATOR %></label> <br/><br/>
						
						<% if ConfEGSModerator = 1 then %>
						    <input type="radio" name="policy_type" id="policy_mod" value="M" <%=p_mod_checked %> onclick="check_role()"> <label for="policy_mod">Global Workforce administrator</label> <br/><br/>
						<% end if %>
						<input type="radio" name="policy_type" id="policy_reg" value="E" <%=p_reg_checked %> onclick="check_role()"> <label for="policy_reg"><%=LNG_REGULAR_EDITOR %></label> <br/><br/>
						
						
<%
	policyUI(id)

	if id <> "" then
		Response.Write "<input type='hidden' name='edit' value='1'/>"
		Response.Write "<input type='hidden' name='id' value='"& id &"'/>"
	end if
%>
						<br/>
						<div align="right">
							<a class="cancel_button" onclick="javascript:<%
							if Request("close")<>"1" then
								Response.Write "history.go(-2)"
							else
								Response.Write "window.close()"
							end if
							%>; return false;"><%=LNG_CANCEL %></a> 
							<a class="save_button" onclick="if (my_sub(1)) {document.my_form.submit()}" ><%=LNG_SAVE %></a>
						</div>
					</form>
					<img src="images/inv.gif" alt="" width="1" height="100%" border="0">
				</div>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
<script language="javascript">
	check_role()
</script>
<%
	end if
else
	Response.Redirect "index.asp"
end if
%>
</body>
</html>
<!-- #include file="db_conn_close.asp" -->