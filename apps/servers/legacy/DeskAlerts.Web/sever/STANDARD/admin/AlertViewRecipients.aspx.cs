﻿using System.Web.UI;
using DeskAlerts.ApplicationCore.Enums;

namespace DeskAlerts.Server.Pages
{
    using DeskAlertsDotNet.DataBase;
    using DeskAlertsDotNet.Pages;
    using Resources;
    using System;
    using System.Web;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;

    public partial class AlertViewRecipients : DeskAlertsBaseListPage
    {
        private string _alertId;
        private string _recipientsType;
        private bool _isFreeAnswer;
        private string _answerString;
        private DataRow _alertRow;
        private bool _isAnswer;

        protected override void OnLoad(EventArgs e)
        {
            if (string.IsNullOrEmpty(Request["id"]) && string.IsNullOrEmpty(Request["answer_id"]))
            {
                Response.End();
            }

            _alertId = Request["id"];
            _recipientsType = Request["type"];
            _isFreeAnswer = !string.IsNullOrEmpty(Request["freeAnswer"]);
            _answerString = Request["answerString"];
            if (!string.IsNullOrEmpty(_answerString))
            {
                _answerString = HttpUtility.HtmlDecode(_answerString);
                _answerString = _answerString.EscapeQuotes();
            }

            if (!string.IsNullOrEmpty(_alertId))
            {
                _alertRow = dbMgr.GetDataByQuery("SELECT type2 FROM alerts WHERE id = " + _alertId).First();
            }

            base.OnLoad(e);
            searchButton.Text = searchButton.Text.Replace("{Search}", resources.LNG_SEARCH);
            userCell.Text = resources.LNG_NAME;
            typeCell.Text = resources.LNG_TYPE;

            _isAnswer = _recipientsType == "ans";
            optInDiv.Visible = _isAnswer && Content.Count > 0;
            Table = contentTable;
            FillTableWithContent(false);
        }


        protected override string CustomQuery
        {
            get
            {
                string query;
                switch (_recipientsType)
                {
                    case "drec":
                        {
                            var isBroadcast = _alertRow.GetString("type2") == "B";
                            query = QueryNotReceived(SearchTerm, isBroadcast);
                            break;
                        }
                    case "rec":
                        {
                            query = QueryReceived(SearchTerm);
                            break;
                        }
                    case "ack":
                        {
                            query = QueryAcknowledgment(SearchTerm);
                            break;
                        }
                    case "vot":
                        {
                            query = QueryVoted(SearchTerm);
                            break;
                        }
                    case "dvot":
                        {
                            query = QueryReceivedNotVoted(SearchTerm);
                            break;
                        }
                    case "ans":
                        {
                            if (_isFreeAnswer && !string.IsNullOrEmpty(_answerString))
                            {
                                query = QueryFreeAnswer(SearchTerm, Request["answer_id"], _answerString);
                            }
                            else if (_isFreeAnswer && string.IsNullOrEmpty(_answerString) && Request["questionType"] == "S")
                            {
                                query = QueryIntermediaryStep(SearchTerm, Request["answer_id"]);
                            }
                            else
                            {
                                query = QueryAnswer(SearchTerm, Request["answer_id"]);
                            }

                            break;
                        }

                    default:
                        query = string.Empty;
                        break;
                }

                return query;
            }
        }

        protected override string CustomCountQuery
        {
            get
            {
                string query;
                switch (_recipientsType)
                {
                    case "drec":
                        {
                            var isBroadcast = _alertRow.GetString("type2") == "B";
                            query = QueryNotReceived(SearchTerm, isBroadcast);
                            break;
                        }
                    case "rec":
                        {
                            query = QueryReceived(SearchTerm);
                            break;
                        }
                    case "ack":
                        {
                            query = QueryAcknowledgment(SearchTerm);
                            break;
                        }
                    case "vot":
                        {
                            query = QueryVoted(SearchTerm);
                            break;
                        }
                    case "dvot":
                        {
                            query = QueryReceivedNotVoted(SearchTerm);
                            break;
                        }
                    case "ans":
                        {
                            if (_isFreeAnswer && !string.IsNullOrEmpty(_answerString))
                            {
                                query = QueryFreeAnswer(SearchTerm, Request["answer_id"], _answerString);
                            }
                            else if (_isFreeAnswer && string.IsNullOrEmpty(_answerString) && Request["questionType"] == "S")
                            {
                                query = QueryIntermediaryStep(SearchTerm, Request["answer_id"]);
                            }
                            else
                            {
                                query = QueryAnswer(SearchTerm, Request["answer_id"]);
                            }

                            break;
                        }
                    default:
                        return string.Empty;
                }

                return $@"
                    SELECT COUNT(*)
                    FROM
                    (
                        {query}
                    ) cnt;";
            }
        }

        protected override string[] Collumns => new[] { "name", "type" };

        protected override string PageParams
        {
            get
            {
                var paramString = string.Empty;
                paramString += AddParameter(paramString, "type");
                paramString += AddParameter(paramString, "id");
                paramString += AddParameter(paramString, "answer_id");
                return paramString;
            }
        }

        #region DataProperties

        protected override string DataTable => "users";

        protected override string DefaultOrderCollumn => "name";

        #endregion

        #region Interface properties

        protected override HtmlInputText SearchControl => searchTermBox;

        protected override IButtonControl[] DeleteButtons => new IButtonControl[] { };

        protected override HtmlGenericControl TopPaging => topPaging;

        protected override HtmlGenericControl BottomPaging => bottomPaging;

        protected override HtmlGenericControl NoRowsDiv => NoRows;

        protected override HtmlGenericControl TableDiv => mainTableDiv;

        protected override string ContentName => resources.LNG_USERS;

        protected override void FillTableWithContent(bool haveCheckbox)
        {
            var cnt = Content.Count < Offset + Limit ? Content.Count : Offset + Limit;

            for (var i = Offset; i < cnt; i++)
            {
                var row = new TableRow();
                var dbRow = Content.GetRow(i);

                var usernameTableCell = new TableCell
                {
                    Text = dbRow.GetString("name")
                };

                var typeTableCell = new TableCell
                {
                    Text = dbRow.GetString("type")
                };

                row.Cells.Add(usernameTableCell);
                row.Cells.Add(typeTableCell);

                if (_isAnswer)
                {
                    var recipientIdContentCell = new TableCell
                    {
                        Text = dbRow.GetString("id")
                    };

                    recipientIdContentCell.Attributes.CssStyle.Add("display", "none");

                    var idInputHidden = new HtmlInputHidden();
                    idInputHidden.Attributes["prop"] = "recipientId";
                    idInputHidden.Value = dbRow.GetString("id");

                    recipientIdContentCell.Controls.Add(idInputHidden);
                    row.Cells.Add(recipientIdContentCell);
                }
                userCell.Text = resources.LNG_NAME;
                typeCell.Text = resources.LNG_TYPE;

                contentTable.Rows.Add(row);
            }
        }

        #endregion

        #region Queries

        public string QueryReceived(string search)
        {
            var query = $@"
                SELECT DISTINCT 
                    ar.[user_id] AS id,
                    users.name + CASE WHEN COALESCE(domains.name, '') <> '' THEN '@'+COALESCE(domains.name, '') ELSE '' END AS name,
                    'User' as type
                FROM users
                    INNER JOIN alerts_received AS ar ON users.id = ar.[user_id]
                    JOIN domains ON domains.id = users.domain_id
                WHERE role = 'U'
                        AND ar.alert_id = {_alertId}
                        AND ar.status = {(int) AlertReceiveStatus.Received}
                        AND [computer_id] IS NULL";

            if (!string.IsNullOrEmpty(search))
            {
                query += $" AND (users.name LIKE '%{search}%' OR users.display_name LIKE '%{search}%')";
            }

            query += $@" UNION ALL 
                SELECT DISTINCT
                    ar.[user_id] AS id,
                    c.name AS name,
                    'Computer' AS type
                FROM users
                    INNER JOIN alerts_received AS ar ON users.id = ar.[user_id]
                    RIGHT JOIN computers AS c ON c.id = ar.computer_id
                WHERE role = 'U'
                    AND ar.alert_id = {_alertId}
                    AND ar.status = {(int)AlertReceiveStatus.Received}
                    AND [computer_id] IS NOT NULL";

            if (SearchTerm.Length > 0)
            {
                query += $" AND c.name LIKE '%{SearchTerm}%'";
            }

            return query;
        }

        //TODO: Create new repository for statistic calculation finally...
        public string QueryNotReceived(string search, bool isBroadcast)
        {
            string query;
            if (isBroadcast)
            {
                query = $@"
                    SELECT DISTINCT
                        u.id AS id,
                        u.name + CASE WHEN COALESCE(domains.name, '') <> '' THEN '@'+COALESCE(domains.name, '') ELSE '' END AS name,
                        'User' AS type
                    FROM users AS u
                        LEFT JOIN alerts_received AS ar ON ar.[user_id] = u.id
                        JOIN domains ON domains.id = u.domain_id
                    WHERE role = 'U'
                        AND (client_version != 'web client'
                        OR client_version IS NULL)
                        AND ar.[computer_id] IS NULL
	                    AND u.id not in (SELECT ar.[user_id] 
                            FROM alerts_received AS ar 
                            WHERE ar.[alert_id] = {_alertId}
                            AND ar.status = {(int)AlertReceiveStatus.Received})";

                if (!string.IsNullOrEmpty(search))
                {
                    query += $" AND (u.name LIKE '%{search}%' OR u.display_name LIKE '%{search}%')";
                }
            }
            else
            {
                query = $@"
                SELECT DISTINCT
                    u.id AS id,
                    u.name + CASE WHEN COALESCE(domains.name, '') <> '' THEN '@'+COALESCE(domains.name, '') ELSE '' END AS name,
                    'User' AS type
                FROM users AS u
                    JOIN domains ON domains.id = u.domain_id
                    LEFT JOIN alerts_sent_stat AS asst ON asst.user_id = u.id 
                WHERE role = 'U' 
                    AND asst.alert_id = {_alertId}
                    AND u.id not in (SELECT ar.[user_id] 
                        FROM alerts_received AS ar 
                        WHERE ar.[alert_id] = {_alertId}
                        AND ar.status = {(int)AlertReceiveStatus.Received})";

                if (!string.IsNullOrEmpty(SearchTerm))
                {
                    query += $" AND (u.name LIKE '%{SearchTerm}%' OR u.display_name LIKE '%{SearchTerm}%')";
                }

                query += $@" UNION ALL 
                SELECT DISTINCT
                    c.id AS id,
                    c.name AS name,
                    'Computer' AS type
                FROM computers AS c
                WHERE c.id IN
                (
                    SELECT ar.computer_id
                    FROM alerts_received ar
                    WHERE alert_id = {_alertId}
                    AND ar.status = {(int)AlertReceiveStatus.NotReceived} 
                    AND ar.computer_id IS NOT NULL
                )";

                if (SearchTerm.Length > 0)
                {
                    query += $" AND c.name LIKE '%{SearchTerm}%'";
                }
            }

            return query;
        }

        public string QueryAcknowledgment(string search)
        {
            var query = $@"
            SELECT DISTINCT
                u.id AS id,
                u.name + CASE WHEN COALESCE(domains.name, '') <> '' THEN '@'+COALESCE(domains.name, '') ELSE '' END AS name,
                'User' AS [type]
            FROM users AS u
                INNER JOIN alerts_read AS ar ON u.id = ar.[user_id]
                JOIN domains ON domains.id = u.domain_id
            WHERE [role] = 'U'
                AND ar.alert_id = {_alertId}";

            if (!string.IsNullOrEmpty(search))
            {
                query += $" AND (u.name LIKE '%{search}%' OR u.display_name LIKE '%{search}%')";
            }

            return query;
        }

        public string QueryVoted(string search)
        {
            var query = $@"
            SELECT DISTINCT
                u.id AS id,
                u.name + CASE WHEN COALESCE(domains.name, '') <> '' THEN '@'+COALESCE(domains.name, '') ELSE '' END AS name,
                'User' AS [type]
            FROM users AS u
                INNER JOIN alerts_received AS ar ON u.id = ar.[user_id]
                JOIN domains ON domains.id = u.domain_id
            WHERE role = 'U'
                AND ar.vote = 1
                AND ar.alert_id = {_alertId}
                AND ar.computer_id IS NULL";

            if (!string.IsNullOrEmpty(search))
            {
                query += $" AND (u.name LIKE '%{search}%' OR u.display_name LIKE '%{search}%')";
            }

            query += $@" UNION ALL 
                SELECT DISTINCT
                    c.id AS id,
                    c.name AS name,
                    'Computer' AS type
                FROM computers AS c
                    INNER JOIN alerts_received AS ar ON ar.computer_id = c.id
                WHERE ar.vote = 1
                    AND ar.alert_id = {_alertId}
                    AND ar.computer_id IS NOT NULL";

            if (!string.IsNullOrEmpty(search))
            {
                query += $" AND c.name LIKE '%{SearchTerm}%'";
            }

            return query;
        }

        public string QueryReceivedNotVoted(string search)
        {
            var query = $@"
            SELECT DISTINCT
                u.id AS id,
                u.name + CASE WHEN COALESCE(domains.name, '') <> '' THEN '@'+COALESCE(domains.name, '') ELSE '' END AS name,
                'User' AS [type]
            FROM users AS u
                JOIN alerts_received AS ar ON ar.[user_id] = u.id
                JOIN domains ON domains.id = u.domain_id
            WHERE ar.[user_id] IN
            (
                SELECT [user_id]
                FROM alerts_received
                WHERE alert_id = {_alertId} 
                AND status = {(int)AlertReceiveStatus.Received}
                AND computer_id IS NULL
                EXCEPT
                SELECT [user_id]
                FROM alerts_received
                WHERE alert_id = {_alertId}
                AND status = {(int)AlertReceiveStatus.Received} 
                AND vote = 1
                AND computer_id IS NULL
            )
            AND ar.computer_id IS NULL";

            if (!string.IsNullOrEmpty(search))
            {
                query += $" AND (u.name LIKE '%{search}%' OR u.display_name LIKE '%{search}%')";
            }

            query += $@" UNION ALL 
            SELECT DISTINCT
                c.id AS id,
                c.name AS name,
                'Computer' AS [type]
            FROM computers AS c
                JOIN alerts_received AS ar ON ar.[computer_id] = c.id
            WHERE ar.[computer_id] IN
            (
                SELECT [computer_id]
                FROM alerts_received
                WHERE alert_id = {_alertId}
                AND status = {(int)AlertReceiveStatus.Received} 
                AND computer_id IS NOT NULL
                EXCEPT
                SELECT [computer_id]
                FROM alerts_received
                WHERE alert_id = {_alertId}
                AND status = {(int)AlertReceiveStatus.Received}
                AND vote = 1
                AND computer_id IS NOT NULL
            )
            AND ar.computer_id IS NOT NULL";

            if (!string.IsNullOrEmpty(search))
            {
                query += $" AND c.name LIKE '%{search}%'";
            }

            return query;
        }

        public string QueryAnswer(string search, string answerId)
        {
            var query = $@"
            SELECT DISTINCT
                u.id AS id,
                u.name + CASE WHEN COALESCE(domains.name, '') <> '' THEN '@'+COALESCE(domains.name, '') ELSE '' END AS name,
                'User' AS [type]
            FROM users AS u
                INNER JOIN surveys_answers_stat AS sas ON u.id = sas.[user_id]
                JOIN domains ON domains.id = u.domain_id
            WHERE role = 'U'
                AND sas.answer_id = {answerId}";

            if (!string.IsNullOrEmpty(search))
            {
                query += $" AND (u.name LIKE '%{search}%' OR u.display_name LIKE '%{search}%')";
            }

            return query;
        }

        public string QueryFreeAnswer(string search, string answerId, string answer)
        {
            var query = $@"
            SELECT DISTINCT
                u.id AS id,
                u.name + CASE WHEN COALESCE(domains.name, '') <> '' THEN '@'+COALESCE(domains.name, '') ELSE '' END AS name,
                'User' AS [type]
            FROM users AS u
                INNER JOIN surveys_custom_answers_stat AS scas ON u.id = scas.[user_id]
                JOIN domains ON domains.id = u.domain_id
            WHERE role = 'U'
                AND scas.question_id = {answerId}
                AND answer LIKE '{answer}'";

            if (!string.IsNullOrEmpty(search))
            {
                query += $" AND (u.name LIKE '%{search}%' OR u.display_name LIKE '%{search}%')";
            }

            return query;
        }

        public string QueryIntermediaryStep(string search, string answerId)
        {
            var query = $@"
            SELECT DISTINCT
                u.id AS id,
                u.name + CASE WHEN COALESCE(domains.name, '') <> '' THEN '@'+COALESCE(domains.name, '') ELSE '' END AS name,
                'User' AS [type]
            FROM surveys_custom_answers_stat AS ca
                RIGHT JOIN users AS u ON ca.[user_id] = u.id
                JOIN domains ON domains.id = u.domain_id
            WHERE ca.question_id = {answerId}";

            if (!string.IsNullOrEmpty(search))
            {
                query += $" AND (u.name LIKE '%{search}%' OR u.display_name LIKE '%{search}%')";
            }

            return query;
        }

        #endregion
    }
}