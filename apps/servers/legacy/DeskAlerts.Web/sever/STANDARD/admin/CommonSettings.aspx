﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CommonSettings.aspx.cs" Inherits="DeskAlertsDotNet.Pages.CommonSettings" %>
<%@ Import NameSpace="DeskAlertsDotNet.Parameters" %>
<%@ Import NameSpace="Resources" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<link href="css/style9.css" rel="stylesheet" type="text/css" />
     
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script type="text/javascript" src="functions.js"></script>
<script type="text/javascript">

    function scrollTo(x) {
        $('html, body').animate({
            scrollTop: x
        }, 10);
    }

(function($) {
    $(document).ready(function () {


        function changeHeight() {
            var body = $(window.document.body);
            var margin = 6;
            var topPanelHeight = 62;
            var height = body.height() - (margin * 2) + 2 + topPanelHeight;
            window.parent.parent.onChangeBodyHeight(height, false);
        }
		$("#common_settings_table").tabs({
			activate: function(event, ui) {
				var href = $("#common_settings_table div.ui-tabs-panel:visible iframe").get(0).src;
                var chunks = href.split("/");
                changeHeight();               
			},
			create: function(event, ui) {
				var href = $("#common_settings_table div.ui-tabs-panel:visible iframe").get(0).src;
				var chunks = href.split("/");
                LoadHelp(chunks[chunks.length - 1]);
                changeHeight();
			}
		});

		resizeAllframes();

		function LoadHelp(href) {
			var about = pageTheme(href, null);
			window.top.frames["main"].document.getElementById("helpIframe").src = "help.asp?about=" + about;
		}

		var activeTab = '<% =Request["activeTab"] %>';

	    if (activeTab !== '') {

	        activeTab = parseInt(activeTab, 10);

	        $("#common_settings_table").tabs({ active: activeTab });

	    }
	});

	function resizeAllframes() {
        if (window.frameElement) {
            var height = $(window.parent.parent.document).height();
            window.parent.parent.onChangeBodyHeight(height, false);
	        $('#system_frame').css('height', height);
	        $('#archiving_frame').css('height', height);
	        $('#default_frame').css('height', height);
	        $('#edir_frame').css('height', height);
	        $('#languages_frame').css('height', height);
        }
	}
	
})(jQuery);

</script>
<script>
	function resizeIframe(id) {
		var iframeDoc = document.getElementById(id);
		iframeDoc.height = window.frameElement.scrollHeight - 110;
		var h;
		if (iframeDoc.contentDocument) {
			h = iframeDoc.contentDocument.documentElement.scrollHeight; //FF, Opera, and Chrome
			h = iframeDoc.contentDocument.body.scrollHeight; //ie 8-9-10
			if (iframeDoc.height < h) iframeDoc.height = h;
		}
		else {
			h = iframeDoc.contentWindow.document.body.scrollHeight; //IE6, IE7 and Chrome
			if (iframeDoc.height < h) iframeDoc.height = h;
		}
	}
</script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
        <tr>
	    <td>
		<table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
		<tr>
		<td width="100%" height="31" class="main_table_title"><img src="images/menu_icons/settings_20.png" alt="" width="20" height="20" border="0" style="padding-left:7px">
		<a href="ProfileSettings.aspx" class="header_title" style="position:absolute;top:23px"><%= resources.LNG_COMMON_SETTINGS %></a></td>
		</tr>
		<tr>
			<td class="main_table_body" height="100%">
			    
             <div id="common_settings_table" style="padding:0px; border: none;background-color: white">
	                <ul>
						    <li><a href="#system" onclick="resizeIframe('system_frame');"><%= resources.LNG_SYSTEM_CONFIGURATION %> </a></li>	
			                <li runat="server" id="archiveTab"><a href="#archiving" onclick="resizeIframe('archiving_frame');"><%= resources.LNG_DATA_ARCHIVING %> </a></li>		
			                <li><a href="#default" onclick="resizeIframe('default_frame');" ><%= resources.LNG_DEFAULT_SETTINGS %></a></li>	
                    
                            <li runat="server" id="langTab"><a href="#languages" onclick="resizeIframe('languages_frame');"><% = resources.LNG_ALERT_LANGUAGES %></a></li>
                        
                            <div id="system">
                                <iframe src="SystemConfigurationSettings.aspx" frameborder="0" style="width:100%;padding:0px"  id="system_frame"></iframe>
                            </div>

                            <div runat="server" id="archiving">
                            </div>     
                    
                            <div id="default">
                                <iframe src="DefaultSettings.aspx" frameborder="0" style="width:100%;padding:0px"  id="default_frame"></iframe>
                            </div>  
                    
                            <div id="languages" runat="server">
                                  <iframe src="AlertLanguagesSettings.aspx" frameborder="0" style="width:100%;padding:0px"  id="languages_frame"></iframe>
                            </div>               
                    </ul>		
               </div>    
             </td>
        </tr>

        </table>
         </td>
         </tr>
        </table>
    </div>
    </form>
</body>
</html>
