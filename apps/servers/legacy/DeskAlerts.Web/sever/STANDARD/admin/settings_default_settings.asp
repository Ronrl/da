﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<!-- #include file="md5.asp" -->
<%

check_session()

uid = Session("uid")

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>DeskAlerts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/style9.css" rel="stylesheet" type="text/css">

<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script language="javascript" type="text/javascript">

    function check_fullscreen() {
        var enabled;
        if ($("#fullscreen").length > 0) {
            if ($("input[name='ConfPopupType']:checked").val() == "F") {
                enabled = true;
            }
            else {
                enabled = false;
            }
            $("#position_fieldset").prop('disabled', enabled);
            $("#ticker_position_fieldset").prop('disabled', enabled);
            $("#al_width").prop('disabled', enabled);
            $("#al_height").prop('disabled', enabled);
        }
    }

    function check_blogpost() {
        var enabled = document.getElementById('blogPost').checked;
        if (enabled) {
            $("#schedule").prop("checked", false);
        }
        $("#schedule").prop("disabled", enabled);
        if (!$("#schedule").prop("checked")) {
            $("#lifetime").prop("disabled", enabled);
            $("#lifetime_factor").prop("disabled", enabled);
        }
    }

    function check_schedule() {
        var enabled = document.getElementById('schedule').checked;
        if (enabled) {
            $("#blogPost").prop("checked", false);
        }
        $("#blogPost").prop("disabled", enabled);
        if (!$("#blogPost").prop("checked")) {
            $("#lifetime").prop("disabled", enabled);
            $("#lifetime_factor").prop("disabled", enabled);
        }
    }

    function check_rsvp() {
        if ($("input[name='ConfDesktopAlertType']:checked").val() == 'R') {
            $('#acknowledgement').attr({ 'disabled': true, 'checked': false });
        } else {
            $('#acknowledgement').prop('disabled', false);
        }
        check_acknowledgement();
    }

    function check_acknowledgement() {
    	if ($("#autoclose").is(":checked")) {
    		$('#autoclose_time').prop('disabled', false);
    		$('#man_close').prop('disabled', false);
    		$('#acknowledgement').attr({ 'disabled': true, 'checked': false });
    		$('#unobtrusive').attr({ 'disabled': true, 'checked': false });
    	} else if ($("#acknowledgement").is(":checked")) {
    		$('#autoclose').attr({ 'disabled': true, 'checked': false });
    		$('#unobtrusive').attr({ 'disabled': true, 'checked': false });
    		$('#man_close').attr({ 'disabled': true, 'checked': false });
    		$('#autoclose_time').prop('disabled', true);
    	} else if ($("#unobtrusive").is(":checked")) {
    		$('#acknowledgement').attr({ 'disabled': true, 'checked': false });
    		$('#autoclose').attr({ 'disabled': true, 'checked': false });
    		$('#man_close').attr({ 'disabled': true, 'checked': false });
    		$('#autoclose_time').prop('disabled', true);
    	} else {
    		$('#man_close').attr({ 'disabled': true, 'checked': false });
    		$('#autoclose_time').prop('disabled', true);
    		$('#autoclose').prop('disabled', false);
    		$('#acknowledgement').prop('disabled', false);
    		$('#unobtrusive').prop('disabled', false);
    	}
    }

    $(document).ready(function() {
        $("#accordion").accordion({
    	    heightStyle: "content",
    	    collapsible: true,
    	    active:false
        });

        $(".cancel_button").button();
        $(".save_button").button();
        $("#blogPost").change(function() { check_blogpost(); });
        $("#schedule").change(function() { check_schedule(); });
        $("#acknowledgement").change(function() { check_acknowledgement(); });
        $("#autoclose").change(function() { check_acknowledgement(); });
        $("#unobtrusive").change(function() { check_acknowledgement(); });
        /*$("input[name='ConfPopupType']").change(function() { check_fullscreen(); });*/
        $("input[name='ConfDesktopAlertType']").change(function() { check_rsvp(); });
        if ($("#blogPost").length > 0) { check_blogpost(); }
        check_schedule();
        check_rsvp();
        check_autoclose();
        /*check_fullscreen();*/
    });


</script>
<script language="javascript">
    function submit() {
        var ConfDesktopAlertByDefault = $("#ConfDesktopAlertByDefault").prop("checked");
        var email = $("#email").prop("checked");
        var sms = $("#sms").prop("checked");

        if ((!ConfDesktopAlertByDefault) && (!email) && (!sms)) {
            alert("You should choose default alert type!");
        } else {
           document.forms[0].submit();
        }
    }

    function check_value() {

        var al_width = document.getElementById('al_width').value;
        var al_height = document.getElementById('al_height').value;
        var size_message = document.getElementById('size_message');

        if (al_width > 1024 && al_height > 600) {
            size_message.innerHTML = '<font color="red"><%=LNG_RESOLUTION_DESC1 %></font>';
            return;
        }

        if (al_width < 340 || al_height < 290) {
            size_message.innerHTML = "<font color='red'><%=LNG_RESOLUTION_DESC2 %>. <A href='#' onclick='set_default_size();'><%=LNG_CLICK_HERE %></a> <%=LNG_RESOLUTION_DESC3 %>.</font>";
            return;
        }
        size_message.innerHTML = "";
    }

    function set_default_size() {
        document.getElementById('al_width').value = 340;
        document.getElementById('al_height').value = 290;
        document.getElementById('size_message').innerHTML = "";
    }

    function check_size_input(myfield, e, dec) {
        var key;
        var keychar;

        if (window.event)
            key = window.event.keyCode;
        else if (e)
            key = e.which;
        else
            return true;

        keychar = String.fromCharCode(key);

        // control keys
        if ((key == null) || (key == 0) || (key == 8) ||
		(key == 9) || (key == 13) || (key == 27)) {
            return true;
        }
        // numbers
        else if (myfield.value.length > 6) {
            return false;
        }
        else if ((("0123456789").indexOf(keychar) > -1)) {
            return true;
        }
        // decimal point jump
        else if (dec && (keychar == ".")) {
            myfield.form.elements[dec].focus();
            return false;
        }
        else
            return false;
    }
</script>
</head>

<body style="margin:0px" class="body">

<%
if (uid <> "")  then
if(request("a")="2") then
	'update all to zero
	Conn.Execute("UPDATE settings SET val=0 WHERE s_part = 'D'")
	
	'update all from form
	For Each item In request.form
		'use prefix Conf to identify settings
		if(InStr(item, "Conf")>0) then
			Conn.Execute("UPDATE settings SET val='" & Replace(request(item),"'","''") & "' WHERE name='" & Replace(item,"'","''") & "' AND s_part = 'D'")
		end if
	Next

	if request("lifetime") <> "" and request("lifetime_factor") <> "" then
		lifetime = request("lifetime") * request("lifetime_factor")
	else
		lifetime = 0
	end if

	if request("rss_lifetime") <> "" and request("rss_lifetime_factor") <> "" then
		rss_lifetime = request("rss_lifetime") * request("rss_lifetime_factor")
	else
		rss_lifetime = 0
	end if

	Conn.Execute("UPDATE settings SET val='" & Replace(lifetime,"'","''") & "' WHERE name='ConfMessageExpire' AND s_part = 'D'")
	Conn.Execute("UPDATE settings SET val='" & Replace(rss_lifetime,"'","''") & "' WHERE name='ConfRSSMessageExpire' AND s_part = 'D'")
end if
%>
<table width="100%" border="0" cellspacing="0" height="100%">
	<tr>
	<td>
	<table width="100%" height="100%" cellspacing="0" cellpadding="0">
		<tr>
		<td height="100%">
		<div style="padding:10px;">

<span class="work_header"><%=LNG_DEFAULT_SETTINGS %></span><br/>
<%if(request("a")="2") then%>
	<div style="font-weight:bold;padding:10px 0px 8px 0px"><%=LNG_DEFAULT_SETTINGS_HAVE_BEEN_SUCESSFULLY_CHANGED %>. Reload page to apply changes properly!</div>
<%end if%>
<form method="post" action="settings_default_settings.asp">
<%
Set rsSettings = Conn.Execute("SELECT * FROM settings WHERE s_part='D'")
do while not rsSettings.EOF

	sName = rsSettings("name")
	sVal = ""
	if(rsSettings("s_type")="C") then
		if(rsSettings("val")=1)then
			sVal = "checked"
		end if
	else
		sVal = rsSettings("val")
	end if

Select Case sName
  case "ConfMessageExpire"
    sConfMessageExpireVal = sVal
  case "ConfRSSMessageExpire"
    sConfRSSMessageExpireVal = sVal
  case "ConfDesktopAlertByDefault"
    sConfDesktopAlertByDefaultVal = sVal
  case "ConfDesktopAlertType"
    sConfDesktopAlertTypeVal = sVal
  case "ConfEmailByDefault"
    sConfEmailByDefaultVal = sVal
  case "ConfSMSByDefault"
    sConfSMSByDefaultVal = sVal
  case "ConfPopupType"
    sConfPopupTypeVal = sVal
  case "ConfAlertWidth"
    sConfAlertWidthVal = sVal
  case "ConfAlertHeight"
    sConfAlertHeightVal = sVal
  case "ConfUrgentByDefault"
    sConfUrgentByDefaultVal = sVal
  case "ConfAcknowledgementByDefault"
    sConfAcknowledgementByDefaultVal = sVal
  case "ConfAutocloseByDefault"
    sConfAutocloseByDefaultVal = sVal
  case "ConfAutocloseValue"
    sConfAutocloseValueVal = sVal
  case "ConfAllowManualCloseByDefault"
    sConfAllowManualCloseByDefaultVal = sVal
  case "ConfScheduleByDefault"
    sConfScheduleByDefaultVal = sVal
  case "ConfAlertPosition"
    sConfAlertPosition = sVal
  case "ConfPostToBlogByDefault"
    sConfPostToBlogByDefaultVal = sVal
  case "ConfSocialMediaByDefault"
    sConfSocialMediaByDefaultVal = sVal
  case "ConfUnobtrusiveByDefault"
    sConfUnobtrusiveByDefaultVal = sVal
  case "ConfTickerPosition"
	sConfTickerPosition = sVal
  case "ConfSelfDeletableByDefault"
    sConfSelfDeletableByDefaultVal = sVal
  case "ConfTextCallByDefault"
    sConfTextCallByDefaultVal = sVal  
end select
  

	rsSettings.MoveNext
loop
 
 
%>
<table border="0">
	<tr>
		<td>
			<div id="accordion" style="width:400px">
				<h3><%=LNG_ALERT_TYPE %></h3>
						<table style="width:400px">
							<tr><td><input type="checkbox" id="ConfDesktopAlertByDefault" name="ConfDesktopAlertByDefault" value="1" <%=sConfDesktopAlertByDefaultVal %> /></td><td colspan="3"><label for="ConfDesktopAlertByDefault"><%=LNG_DESKTOP_ALERT %></label></td></tr>
							<tr><td>&nbsp;</td><td><input type="radio" name="ConfDesktopAlertType" value="P" <% if (sConfDesktopAlertTypeVal="P") then response.write "checked" end if %> /></td><td colspan="2"><%=LNG_POP_UP_WINDOW %></td></tr>
							<% if TICKER_CFG=1 then %>
							<tr><td>&nbsp;</td><td><input type="radio" name="ConfDesktopAlertType" value="T" <% if (sConfDesktopAlertTypeVal="T") then response.write "checked" end if %> /></td><td colspan="2"><%=LNG_SCROLLING_TICKER %></td></tr>
							<% end if %>
							<tr><td>&nbsp;</td><td><input type="radio" name="ConfDesktopAlertType" value="R" <% if (sConfDesktopAlertTypeVal="R") then response.write "checked" end if %> /></td><td colspan="2">RSVP</td></tr>
							<% if SOCIAL_LINKEDIN=1 then%>
							<tr><td>&nbsp;</td><td><input type="radio" name="ConfDesktopAlertType" value="L" <% if (sConfDesktopAlertTypeVal="L") then response.write "checked" end if %> /></td><td colspan="2">LinkedIn</td></tr>
							<% end if %>
							<% if BLOG=1 then%>
							<tr><td><input id="blogPost" type="checkbox" name="ConfPostToBlogByDefault" <%=sConfPostToBlogByDefaultVal %> value="1" /></td><td colspan="3"><%=LNG_BLOG_POST %></td></tr>
							<% end if %>
							<% if EM=1 then %>
							<tr><td><input id="email" type="checkbox" name="ConfEmailByDefault" <%=sConfEmailByDefaultVal %> value="1" /></td><td colspan="3"><%=LNG_EMAIL %></td></tr>
							<% end if %>
							<% if SMS=1 then %>
							<tr><td><input id="sms" type="checkbox" name="ConfSMSByDefault" <%=sConfSMSByDefaultVal %> value="1" /></td><td colspan="3"><%=LNG_SMS %></td></tr>
							<% end if %>
							<% if SOCIAL_MEDIA=1 then %>
							<tr><td><input id="socialMedia" type="checkbox" name="ConfSocialMediaByDefault" <%=sConfSocialMediaByDefaultVal %> value="1" /></td><td colspan="3"><%=LNG_TWEET %></td></tr>
							<% end if %>
							<% if TEXT_TO_CALL=1 then %>
							<tr><td><input id="textcall" type="checkbox" name="ConfTextCallByDefault" <%=sConfTextCallByDefaultVal %> value="1" /></td><td colspan="3"><%=LNG_TEXT_TO_CALL %></td></tr>
							<% end if %>
						</table>
				<h3><%=LNG_POP_UP_APPEARANCE %></h3>
					<div >
							<div id="size_message"></div>
							<table>
								<% if FULLSCREEN_CFG=1 then%>
								<tr><td><input type="radio" name="ConfPopupType" value="W" <% if (sConfPopupTypeVal="W") then response.write "checked" end if %> /></td><td colspan="3"><%=LNG_WINDOW %></td></tr>
								<tr><td>&nbsp;</td><td><%=LNG_WIDTH %> </td><td><input type="text" onKeyUp="check_value(this)" id="al_width" onkeypress="return check_size_input(this, event);" name="ConfAlertWidth" style="width:50px" value="<%=sConfAlertWidthVal %>" /> <%=LNG_PX %></td></tr>
								<tr><td>&nbsp;</td><td><%=LNG_HEIGHT %> </td><td><input type="text" onKeyUp="check_value(this)" id="al_height" onkeypress="return check_size_input(this, event);" name="ConfAlertHeight" style="width:50px" value="<%=sConfAlertHeightVal %>" /> <%=LNG_PX %></td></tr>
								<tr><td><input id="fullscreen" type="radio" name="ConfPopupType" value="F" <% if (sConfPopupTypeVal="F") then response.write "checked" end if %>  /></td><td colspan="3"><%=LNG_FULL_SCREEN %></td></tr>
								<%else %>
								<tr><td colspan="4"><input type="hidden" name="ConfPopupType" value="W"/></td></tr>
								<tr><td>&nbsp;</td><td><%=LNG_WIDTH %> </td><td><input type="text" onKeyUp="check_value(this)" id="al_width" onkeypress="return check_size_input(this, event);" name="ConfAlertWidth" style="width:50px" value="<%=sConfAlertWidthVal %>" /> <%=LNG_PX %></td></tr>
								<tr><td>&nbsp;</td><td><%=LNG_HEIGHT %> </td><td><input type="text" onKeyUp="check_value(this)" id="al_height" onkeypress="return check_size_input(this, event);" name="ConfAlertHeight" style="width:50px" value="<%=sConfAlertHeightVal %>" /> <%=LNG_PX %></td></tr>
								<%end if %>
								<tr>
									<td valign="top" colspan="4">
										<fieldset id="position_fieldset" style="width:135px;margin:0px;padding:5px 5px 10px 5px">
										<legend><%=LNG_POSITION %></legend>
										<div id="position_div">
										<table border="0" cellspacing="0" cellpadding="0" width="100%">
											<tr>
												<td valign="middle" style="text-align:center"><input type="radio" name="ConfAlertPosition" value="2" <% if (sConfAlertPosition="2") then response.write "checked" end if %>/></td>
												<td></td>
												<td valign="middle" style="text-align:center"><input type="radio" name="ConfAlertPosition" value="3" <% if (sConfAlertPosition="3") then response.write "checked" end if %>/></td>
											</tr>
											<tr>
												<td></td>
												<td valign="middle" style="text-align:center;padding:10px"><input type="radio" name="ConfAlertPosition" value="4" <% if (sConfAlertPosition="4") then response.write "checked" end if %>/></td>
												<td></td>
											</tr>
											<tr>
												<td valign="middle" style="text-align:center"><input type="radio" name="ConfAlertPosition" value="5" <% if (sConfAlertPosition="5") then response.write "checked" end if %>/></td>
												<td></td>
												<td valign="middle" style="text-align:center"><input type="radio" name="ConfAlertPosition" value="6" <% if (sConfAlertPosition="6") then response.write "checked" end if %>/></td>
											</tr>
										</table>
										</div>
										</fieldset>
									</td>
									<% if TICKER_PRO = 1 then %>
									<td valign="top">
										<fieldset id="ticker_position_fieldset" style="margin:0px;padding:0px 5px 5px 5px">
										<legend><%=LNG_TICKER_POSITION %></legend>
										<div id="ticker_position_div">
										<table border="0" cellspacing="0" cellpadding="0" width="100%">
											<tr>
												<td valign="middle" style="text-align:center;padding:5px 10px"><input type="radio" name="ConfTickerPosition" value="7" <% if (sConfTickerPosition="7") then response.write "checked" end if %>/></td>
												<td><span><%=LNG_TOP %></span></td>
											</tr>
											<tr>
												<td valign="middle" style="text-align:center;padding:5px 10px"><input type="radio" name="ConfTickerPosition" value="8" <% if (sConfTickerPosition="8") then response.write "checked" end if %>/></td>
												<td><span><%=LNG_MIDDLE %></span></td>
											</tr>
											<tr>
												<td valign="middle" style="text-align:center;padding:5px 10px"><input type="radio" name="ConfTickerPosition" value="9" <% if (sConfTickerPosition="9") then response.write "checked" end if %>/></td>
												<td><span><%=LNG_BOTTOM %></span></td>
											</tr>
											<tr>
												<td valign="middle" style="text-align:center;padding:5px 10px"><input type="radio" name="ConfTickerPosition" value="D" <% if (sConfTickerPosition="D") then response.write "checked" end if %>/></td>
												<td><span><%=LNG_DOCKED_AT_BOTTOM %></span><img style="margin:0px 5px" src="images/help.png" title="<%=LNG_DOCKED_TICKER_HINT %>." /></td>
											</tr>
										</table>
										</div>
										</fieldset>
									</td>
									<% end if %>
								</tr>
							</table>
					</div>
						<h3><%=LNG_DESKTOP_ALERT_OPTIONS %></h3>
							<table style="width:320px">
								<tr><td><input id="urgent" type="checkbox" name="ConfUrgentByDefault" value="1" <%=sConfUrgentByDefaultVal %> /></td><td colspan="3"><%=LNG_URGENT_ALERT %></td></tr>	
								<tr><td><input id="acknowledgement" type="checkbox" name="ConfAcknowledgementByDefault" value="1" <%=sConfAcknowledgementByDefaultVal %> /></td><td colspan="3"><%=LNG_ACKNOWLEDGEMENT %></td></tr>			     
								<tr><td><input id="unobtrusive" type="checkbox" name="ConfUnobtrusiveByDefault" value="1" <%=sConfUnobtrusiveByDefaultVal %> /></td><td colspan="3"><%=LNG_UNOBTRUSIVE %></td></tr>		
								<tr><td><input id="self_deletable" type="checkbox" name="ConfSelfDeletableByDefault" value="1" <%=sConfSelfDeletableByDefaultVal %> /></td><td colspan="3"><%=LNG_SELF_DELETABLE %></td></tr>		     
								<tr><td><input id="autoclose" type="checkbox" name="ConfAutocloseByDefault" value="1" <%=sConfAutocloseByDefaultVal %> /></td><td colspan="2"><%=LNG_AUTOCLOSE_IN %> </td><td><input id="autoclose_time" type="text" onkeypress="return check_size_input(this, event);" name="ConfAutocloseValue" style="width:50px" value="<%=sConfAutocloseValueVal %>"/> <%=LNG_MINUTES %></td></tr>			     		    
								<tr><td><input id="man_close" type="checkbox" name="ConfAllowManualCloseByDefault" <%=sConfAllowManualCloseByDefaultVal %> value="1" /></td><td colspan="2"><%=LNG_ALLOW_MANUAL_CLOSE %></td></tr>		     		    
								<tr><td><input id="schedule" type="checkbox" name="ConfScheduleByDefault" value="1" <%=sConfScheduleByDefaultVal %> /></td><td colspan="3"><%=LNG_SCHEDULE_ALERT %></td></tr>			     
								<tr>
									<td colspan="4"><%=LNG_LIFETIME %>&nbsp;
										<%=GetLifetimeControl(sConfMessageExpireVal, "lifetime") %>
										<br />
										<span style="color:#aaaaaa"><%=LNG_LIFETIME_DESC %></span>
									</td>
								</tr>
							</table>

				<%if RSS=1 then %>

						<h3><%=LNG_RSS %></h3>
							<table style="width:320px">
								<tr>
									<td colspan="4"><%=LNG_LIFETIME %>&nbsp;
										<%=GetLifetimeControl(sConfRSSMessageExpireVal, "rss_lifetime") %>
										<br />
										<span style="color:#aaaaaa"><%=LNG_LIFETIME_DESC %></span>
									</td>
								</tr>
							</table>

				<%end if %>
			</div>
		</td>
	</tr>
	<tr><td colspan="2" align="right">
	<a class="cancel_button" onclick="javascript:history.go(-1); return false;"><%=LNG_CANCEL %></a> 
	<a class="save_button" onclick="submit()" ><%=LNG_SAVE %></a>
	<input type="hidden" name="a" value="2">
	</td>
	</tr>
</table>

</form>

		 <img src="images/inv.gif" alt="" width="1" height="100%" border="0">
		</div>
		</td>
		</tr>
	</table>
	</td>
	</tr>
</table>

</body>
</html>
<%

else

  Response.Redirect "index.asp"

end if

%> 
</body>
</html>
<!-- #include file="db_conn_close.asp" -->