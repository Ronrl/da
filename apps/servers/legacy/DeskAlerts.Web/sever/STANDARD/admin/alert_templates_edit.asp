﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<script language="JScript" src="jscripts/json2.js" runat="server"></script>

<script language="JScript" runat="server">
	/*
	var tempId;
	var formJSON;
	
	function initJS(Conn)
	{
		tempId = Request("id");

		if (!tempId.Item())
		{
			Response.End();
		}
		
		formJSON = Request("form_json");

		if (!formJSON.Item())
		{
			var rsTemp = Conn.Execute("SELECT id, name, html, show_condition, next_button_name, previous_button_name, close_button_name, [order] FROM alert_param_template_forms WHERE param_temp_id='"+tempId.Item().replace("'", "''")+"' ORDER BY [order]")
			
			formJSON = [];
			
			while(!rsTemp.EOF)
			{
				var formObj = {
					"id" : rsTemp.Fields.Item('id').value,
					"name" : rsTemp.Fields.Item('name').value,
					"html" : rsTemp.Fields.Item('html').value,
					"show_condition" : rsTemp.Fields.Item('show_condition').value,
					"next_button_name" : rsTemp.Fields.Item('next_button_name').value,
					"previous_button_name" : rsTemp.Fields.Item('previous_button_name').value,
					"close_button_name": rsTemp.Fields.Item('close_button_name').value,
					"order" : rsTemp.Fields.Item('order').value
				}; 
				
				formJSON.push(formObj);
				rsTemp.MoveNext();
			}
			formJSON = JSON.stringify(formJSON);
			rsTemp.Close();
		}	
	}*/
</script>
<%
	tempId = Request("id")
	formHtml = Request("form_html")
	formName = Request("form_name")
	formId = Request("form_id")
	
	if (formHtml <> "") then
		needConfirmation = 1
	else
		needConfirmation = 0
	end if
	
	
	Set formsRS = Conn.Execute("SELECT id, name, html, show_condition, next_button_name, previous_button_name, close_button_name, [order] FROM alert_param_template_forms WHERE param_temp_id='"+Replace(tempId,"'", "''")+"' ORDER BY [order]")
	
	formJSON = "["
	
	formIndex = 0
	
	while (not formsRS.EOF)
	
		if (formId = "") then	
			if (formName = "" AND formIndex = 0) then
				formName = formsRS("name")
			end if 
			if (formHtml = "" AND formIndex = 0) then
				formHtml = formsRS("html")
			end if
		elseif (formsRS("id") = formId) then
			if (formName = "") then formName = formsRS("name")
			if (formHtml = "") then formHtml = formsRS("html")
		end if	
		
		if (formIndex <> 0) then formJSON = formJSON & "," end if
		
		formJSON = formJSON &_
					"{" &_
						"""id"": """& formsRS("id") &"""," &_
						"""name"": """& jsEncode(formsRS("name")) &"""," &_
						"""order"": """& formsRS("order") &"""" &_
					"}"
		
		formIndex = formIndex + 1
		formsRS.MoveNext
	Wend

	formJSON = formJSON & "]"
	
	formsRS.Close
%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>DeskAlerts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/style9.css" rel="stylesheet" type="text/css">
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css">
<link href="css/jquery/jquery_timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css">
<style>
		label, input { display:block; }
		input.text { margin-bottom:12px; width:95%; padding: .4em; }
		fieldset { padding:0; border:0; margin-top:25px; }
		h1 { font-size: 1.2em; margin: .6em 0; }
		div#users-contain { width: 350px; margin: 20px 0; }
		div#users-contain table { margin: 1em 0; border-collapse: collapse; width: 100%; }
		div#users-contain table td, div#users-contain table th { border: 1px solid #eee; padding: .6em 10px; text-align: left; }
		.ui-dialog .ui-state-error { padding: .3em; }
		.validateTips { border: 1px solid transparent; padding: 0.3em; }
		.formValidateTips { border: 1px solid transparent; padding: 0.3em; }
		.ui-button-icon-only {font-size: 8px;}
		#form_tabs li .ui-icon-close { float: left; margin: 0.4em 0.2em 0 0; cursor: pointer; }
		#form_tabs li .ui-icon-pencil { float: left; margin: 0.4em 0.2em 0 0; cursor: pointer; }
		
	</style>
<!-- TinyMCE -->
<script language="javascript" type="text/javascript" src="jscripts/tiny_mce/tinymce.min.js"></script>
<script language="javascript" type="text/javascript">
	var confirmNeeded = <% if (needConfirmation = 1) then Response.Write "true" else Response.Write "false"%>;
	function contentOnChangeHandler()
	{
		confirmNeeded = true;
	}
	var newTinySettings = {<%= newTinySettings()%>};
	initTinyMCE("<%=lcase(default_lng) %>", "exact", "form_html_area", function(ed){
		ed.onChange.add(contentOnChangeHandler);
	},null,newTinySettings);
</script>
<!-- /TinyMCE -->
<script language="javascript" type="text/javascript" src="jscripts/date.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery_timepicker/jquery-ui-timepicker-addon.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/datepicker_fix.js"></script>
<script language="javascript" type="text/javascript" src="functions.js"></script>

<script language="javascript" type="text/javascript">

var forms = <%=formJSON%>;

var formId = "<%=formId%>";

function showPrompt(text)
{
	$("#dialog-modal > p").text(text)
	$("#dialog-modal").dialog({
		height: 140,
		modal: true
	});
}


function createFormTitle(form)
{
	var title =  $("<li><a href='#form_tab' id='"+form.id+"'>"+form.name+"</a></li>");
	return title;
}


function getFormContent(id,callback)
{
	resetAllFlags();
	clearActionForm();
	
	$("#form_id_input").val(id);
	$("#get_form_input").val("1");
	submitForm('action_form',true,callback);
	
}

function getFormsOrders()
{
	var forms = [];
	$("#form_headers").find("a").each(function(index,element){
		var form = {};
		form.order = index;
		form.id = $(element).attr("id");
		forms.push(form);
	});
	return forms;
}

function setFormsOrders(callback)
{
	resetAllFlags();
	clearActionForm();
	
	$("#forms_json_input").val(JSON.stringify(getFormsOrders()));
	$("#order_forms_input").val("1");
	submitForm('action_form',true,callback);

}

function initForms()
{
	var selectedIndex = 0;
	for (var i=0; i<forms.length; i++)
	{
		$("#form_headers").append(createFormTitle(forms[i]));
		if (forms[i].id == formId)
		{
			selectedIndex = i;
		}
	}
	var tabs = $( "#form_tabs" ).tabs({
		selected : selectedIndex, 
		activate : function(event,ui) {
			window.parent.showLoader();
			getFormContent(ui.newTab.find('a').attr("id"), function(data){
				var content = "";
				var name = "";
				if (data)
				{
					var result = JSON.parse(data);
					content = result.form.html;
					name = result.form.name;	
				}
				tinyMCE.get('form_html_area').setContent(content);
				$("#form_name").val(name);	
				window.parent.hideLoader();
			}); 
		},
		beforeActivate: function(event,ui) {
			if (confirmNeeded)
			{				
				event.preventDefault();
				$("#confirmation_text").text("<%=LNG_DO_YOU_WANT_TO_SAVE_CHANGES%>");
				var dialog_buttons = $( "#dialog-confirm" ).dialog("option","buttons");
				$( "#dialog-confirm" ).dialog({
					buttons : {
						"<%=LNG_YES%>": function(){
							
							$( this ).dialog( "close" );
							$( "#dialog-confirm" ).dialog("option","buttons",dialog_buttons);
							
							resetAllFlags();
							clearActionForm();
							
							window.parent.showLoader();
			
							$("#save_input").val("1");
							$("#form_name_input").val($("#form_name").val());
							$("#form_html_input").val(tinyMCE.get('form_html_area').getContent());
							var selectedTab = $("#form_headers").find("li").get($( "#form_tabs" ).tabs('option', 'selected'));
							var selectedId = $(selectedTab).find("a").attr("id");
							$("#form_id_input").val(selectedId);
									
							submitForm('action_form',false,function(data){
							
								window.parent.hideLoader();

								var result = JSON.parse(data);
								
								if (result.errorCode == "0")
								{
									confirmNeeded = false;
									ui.oldTab.find('a').text(result.form.name);
									ui.oldTab.find('a').attr('id',result.form.id);
									$( "#form_tabs" ).tabs("option","active",ui.newTab.index());
									$( "#form_tabs" ).tabs( "refresh" );
								}
								else if (result.errorCode == "2")
								{
									showPrompt( "<%=LNG_NAME_ALREADY_EXIST%>");
								}
							});

						},
						"<%=LNG_NO%>": function() {
							confirmNeeded = false;
							$( "#form_tabs" ).tabs("option","active",ui.newTab.index());
							$( this ).dialog( "close" );
							$( "#dialog-confirm" ).dialog("option","buttons",dialog_buttons);
						},
						"<%=LNG_CANCEL%>": function() {
							$( this ).dialog( "close" );
							$( "#dialog-confirm" ).dialog("option","buttons",dialog_buttons);
						}
					}
				});
				$( "#dialog-confirm" ).dialog("open");
			}
		}
	});
	

	
	/*tabs.find( ".ui-tabs-nav" ).sortable({
      axis: "x",
	  stop: function(event, ui)
	  {
		window.parent.showLoader();
		setFormsOrders(function(data){
			var result = JSON.parse(data);
			window.parent.hideLoader();
		});
		tabs.tabs( "refresh" );
	  }
    });*/
	
	$("#form_name").on('input',function(e){
		confirmNeeded = true;
	});
	
}


function insertAtCaret (elementId, myValue)
{
	tinyMCE.execInstanceCommand(elementId,"mceInsertContent",false,myValue);
}

function escapeSelector( str) {
 if( str)
     return str.replace(/([ #{};&,.+*~\':"!^$[\]()=>|\/@])/g,'\\$1')
 else
     return str;
}

var serverDate;
var settingsDateFormat = "<%=GetDateFormat()%>";
var settingsTimeFormat = "<%=GetTimeFormat()%>";

var responseDbFormat = "dd/MM/yyyy HH:mm:ss";
var saveDbFormat = "dd/MM/yyyy HH:mm";
var saveDbDateFormat = "dd/MM/yyyy";
var saveDbTimeFormat = "HH:mm";
var uiFormat = getUiDateTimeFormat(settingsDateFormat,settingsTimeFormat);

function getMinutesTime(time)
{
	var minutesValue = $.trim(time);
	if (minutesValue!="")
	{
		var hours = Math.floor(minutesValue / 60);
		var minutes = minutesValue % 60;			
		var date = new Date();				
		date.setHours(hours);
		date.setMinutes(minutes);
		minutesValue = formatDate(date,"HH:mm");
	}		
	return minutesValue;
}

function refreshInputTypes()
{
	$("#param_default_value").datetimepicker( "destroy" );
	$("#param_default_value").timepicker( "destroy" )

	if ($("#param_type").val() == "6")
	{
		initDateTimePicker($("#param_default_value"),settingsDateFormat,settingsTimeFormat);	
	}
	else if ($("#param_type").val() == "12")
	{				
		$("#param_default_value").timepicker({
			showButtonPanel: false 
		});
	}

}

var allFields = $( [] );

function setDialogDefaultValues()
{
	allFields.val("");
	$("#form_dialog").find("#form_name").val("");
	$( ".validateTips" ).text("<%=LNG_PLEASE_FILL_ALL_EMPTY_FIELDS%>")
	$( ".formValidateTips" ).text("<%=LNG_PLEASE_FILL_ALL_EMPTY_FIELDS%>")
	$("#param_type option[value='0']").attr('selected','selected');
	$("#param_action option[value='']").attr('selected','selected');
	$("#param_visible option[value='1']").attr('selected','selected');
}

function resetAllFlags()
{
	$("#save_input").val("0");
	$("#save_input").val("0");
	$("#edit_param_input").val("0");
	$("#add_param_input").val("0");
	$("#delete_param_input").val("0");
	$("#add_form_input").val("0");
	$("#delete_form_input").val("0");
	$("#get_form_input").val("0");
	$("#order_forms_input").val("0");
}

function clearActionForm()
{
	$("#param_id_input").val("");
	$("#param_name_input").val("");
	$("#param_short_name_input").val("");
	$("#param_type_input").val("");
	$("#param_default_value_input").val("");
	$("#param_visible_input").val("");
	$("#param_action_input").val("");	
	$("#form_html_input").val("");
	$("#form_name_input").val("");
	$("#form_id_input").val("");
	$("#forms_json_input").val("");
}

function addParam()
{
	setDialogDefaultValues(); 
	resetAllFlags(); 
	$('#add_param_input').val('1');
	$("#param_id_input").val("");	
	$('#dialog-form').dialog('open'); 
}

function editParam(id)
{
	setDialogDefaultValues();
	resetAllFlags();
	$("#edit_param_input").val("1");
	$("#param_id_input").val(id);
	var $paramRow = $("#"+escapeSelector(id));
	var visibleValue= ($paramRow.find(".visible_cell").attr('value') == "True") ? 1 : 0
	$("#param_name").val($paramRow.find(".name_cell").attr('value'));
	$("#param_short_name").val($paramRow.find(".short_name_cell").attr('value'));
	$("#param_type option[value='"+$paramRow.find(".type_cell").attr('value')+"']").attr('selected','selected');
	
	var defaultValue = $paramRow.find(".default_value_cell").attr('value');
	if (defaultValue!="")
	{
		if ($("#param_type").val() == "6")
		{
			var dateValue = $.trim(defaultValue);
			if (dateValue!="" && isDate(dateValue,responseDbFormat))
			{
				defaultValue = formatDate(new Date(getDateFromFormat(dateValue,responseDbFormat)),uiFormat);
			}
		}
		else if ($("#param_type").val() == "12")
		{				
			var minutesValue = $.trim(defaultValue);
			if (minutesValue!="")
			{
				var hours = Math.floor(minutesValue / 60);
				var minutes = minutesValue % 60;			
				var date = new Date();				
				date.setHours(hours);
				date.setMinutes(minutes);
				defaultValue = formatDate(date,"HH:mm");
			}		
		}
	}
	$("#param_default_value").val(defaultValue);
	$("#param_visible option[value='"+visibleValue+"']").attr('selected','selected');
	$("#param_action option[value='"+$paramRow.find(".action_cell").attr('value')+"']").attr('selected','selected');
	$( "#dialog-form" ).dialog('open');
}

var onDeleteClick;

function deleteParam(id)
{
	$("#confirmation_text").text("<%=LNG_ARE_YOU_SURE_YOU_WANT_TO_DELETE_SELECTED%>?");
	$( "#dialog-confirm" ).dialog("open");
	onDeleteClick = function(){
		clearActionForm();
		resetAllFlags();
		$("#delete_param_input").val("1");
		$("#param_id_input").val(id);
		submitForm('action_form',false,function(){});
		$("#form_html_field").val(tinyMCE.get('form_html_area').getContent());
		$("#form_name_field").val($("#form_name").val());
		var selectedTab = $("#form_headers").find("li").get($( "#form_tabs" ).tabs('option', 'selected'));
		var selectedId = $(selectedTab).find("a").attr("id");
		$("#form_id_field").val(selectedId);
		$("#fields_form").submit();
	}
}

function saveHtml()
{
	var formHtml = tinyMCE.get('form_html_area').getContent();
	clearActionForm();
	resetAllFlags();
	$("#save_input").val("1");
	$("#form_html_input").val(tinyMCE.get('form_html_area').getContent());
	$("#form_name_input").val($("#form_name").val());
	
	var selectedTab = $("#form_headers").find("li").get($( "#form_tabs" ).tabs('option', 'selected'));
	var selectedId = $(selectedTab).find("a").attr("id");
	$("#form_id_input").val(selectedId);
	
	submitForm('action_form',false,function(){});
	$("#form_html_field").val("");
	$("#form_name_field").val("");
	$("#form_id_field").val(selectedId);
	$("#fields_form").submit();
	
}

function insertParamInText(id)
{
	var $paramRow = $("#"+escapeSelector(id));
	var shortName = $paramRow.find(".short_name_cell").attr('value');
	var paramStr = "{" + shortName + "}";
	insertAtCaret('form_html_area',paramStr)
}

function submitForm(id,async,callback)
{
	var $inputs = $("#"+id).children();
	var url = $("#"+id).attr("action");
	var method = $("#"+id).attr("method");
	var dataString;
	

	
	$.each($inputs, function(i, input) { 

	
		var inputValue = $(input).val();

		if ($(input).attr("id") == "param_default_value_input")
		{	
			if ($("#param_type").val() == 6)
			{
				var inputDate = new Date(getDateFromFormat(inputValue,uiFormat));
				inputValue = formatDate(inputDate,saveDbFormat);
			}
			else if ($("#param_type").val() == 12)
			{				
		
				var inputTime = new Date(getDateFromFormat(inputValue,"HH:mm"));
				inputValue = inputTime.getHours() * 60 + inputTime.getMinutes();
			}
		}
		
		var valueStr = $(input).attr("name") + "=" + escape(inputValue);
	
		
		if (!dataString)
		{
			dataString = valueStr;
		}
		else
		{
			dataString += "&" + valueStr;
		}

	});
	
	$.ajax({
		type: method,
		url: url,
		data: dataString,
		success: function(data) {
			callback(data);
		},
		error: function(xhr, ajaxOptions, thrownError)
		{
			var result = {
				errorCode : xhr.status,
				errorMessage : thrownError
			}
			callback(JSON.stringify(result));
		},
		async: async
    });
	
}


var formDialogSaveFunction;

function updateFormTips( t ) {
		$( ".formValidateTips" ).text( t )
			.addClass( "ui-state-highlight" );
		setTimeout(function() {
			$( ".formValidateTips" ).removeClass( "ui-state-highlight", 1500 );
		}, 500 );
}

function checkFormValues()
{	
	$("#form_dialog").find("#form_name").removeClass( "ui-state-error" );
	
	
	if ($("#form_dialog").find("#form_name").val() == "")
	{
		$("#form_dialog").find("#form_name").addClass( "ui-state-error" );
		
		updateFormTips( "<%=LNG_NAME_SHOULD_NOT_BE_EMPTY%>");
		return false;
	}
	
	return true;
}
	

$(function() {

	initForms();

	$("#add_form_button").button().click(function(){
		setDialogDefaultValues();
		resetAllFlags();
		
		formDialogSaveFunction = function(){	

			window.parent.showLoader();
			
			$("#add_form_input").val("1");
			$("#form_name_input").val($("#form_dialog").find("#form_name").val());
					
			submitForm('action_form',false,function(data){
			
				window.parent.hideLoader();

				var result = JSON.parse(data);
				
				if (result.errorCode == "0")
				{
					$("#form_headers").append(createFormTitle(result.form));
					$( "#form_tabs" ).tabs( "refresh" );
					$("#form_dialog").dialog( "close" );
				}
				else if (result.errorCode == "2")
				{
					$("#form_dialog").find("#form_name").addClass( "ui-state-error" );
					updateFormTips( "<%=LNG_NAME_ALREADY_EXIST%>");
				}
			});

		}
		
		$( "#form_dialog" ).dialog("option","title","<%=LNG_ADD_FORM%>");
		$( "#form_dialog" ).dialog("open");
	});
	
	$("#delete_form_button").button().click(function(){

		$("#confirmation_text").text("<%=LNG_ARE_YOU_SURE_YOU_WANT_TO_DELETE_SELECTED%>?");
		$( "#dialog-confirm" ).dialog("open");
		onDeleteClick = function(){
			clearActionForm();
			resetAllFlags();

			var selectedTab = $("#form_headers").find("li").get($( "#form_tabs" ).tabs('option', 'selected'));
			var selectedId = $(selectedTab).find("a").attr("id");
	
			console.log(selectedId);
	
			window.parent.showLoader();
			
			$("#delete_form_input").val("1");
			$("#form_id_input").val(selectedId);
			
			submitForm('action_form',false,function(data){
				window.parent.hideLoader();
				var result = JSON.parse(data);
				if (result.errorCode == "0")
				{
					selectedTab.remove();
					$( "#form_tabs" ).tabs( "refresh" );
				}
			});

			$( "#dialog-confirm" ).dialog("close");
		}

	});
	
	serverDate = new Date(getDateFromAspFormat("<%=now_db%>",responseDbFormat));
	setInterval(function(){serverDate.setSeconds(serverDate.getSeconds()+1);},1000);
	
	$("#add_param_button").button();
	$("#save_button").button();

	$( "#dialog-confirm" ).dialog({
		resizable: false,
		height:140,
		modal: true,
		autoOpen: false,
		buttons: {
			"<%=LNG_DELETE%>": function(){onDeleteClick();
				$( this ).dialog( "close" );
			},
			"<%=LNG_CANCEL%>": function() {
				$( this ).dialog( "close" );
			}
		}
	});
	
	$(".edit_param_button").button({
		icons: {
			primary: "ui-icon-pencil"
		},
		text: false
    });
	
	$(".delete_param_button").button({
		icons: {
			primary: "ui-icon-trash"
		},
		text: false
    });
	
	$(".insert_in_text_param_button").button({
		icons: {
			primary: "ui-icon-document"
		},
		text: false
    });
	function updateTips( t ) {
			$( ".validateTips" ).text( t )
				.addClass( "ui-state-highlight" );
			setTimeout(function() {
				$( ".validateTips" ).removeClass( "ui-state-highlight", 1500 );
			}, 500 );
	}

	allFields = allFields.add($("#param_name")).add( $("#param_short_name")).add($("#param_default_value"));
	
	function checkValues()
	{	
		allFields.removeClass( "ui-state-error" );
		if ($("#param_short_name").val() == "")
		{
			$("#param_short_name").addClass( "ui-state-error" );
			updateTips( "<%=LNG_SHORT_NAME_SHOULD_NOT_BE_EMPTY%>");
			return false;
		}
		if ($("#param_type").val() == "6")
		{
			var val = $("#param_default_value").val();
			if (val && !isDate(val,uiFormat))
			{
				$("#param_default_value").addClass( "ui-state-error" );
				updateTips( "<%=LNG_DATE_VALUE_IS_INCORRECT%>");
				return false;
			}
		}
		if ($("#param_type").val() == "12")
		{
			$("#param_default_value").val($.trim($("#param_default_value").val()));
			var val = $("#param_default_value").val();
			if (val && !isDate(val,"HH:mm"))
			{
				$("#param_default_value").addClass( "ui-state-error" );
				updateTips( "<%=LNG_TIME_VALUE_IS_INCORRECT%>");
				return false;
			}
		}
		return true;
	}
	
	$( "#dialog-form" ).dialog({
		autoOpen: false,
		height: 450,
		width: 500,
		modal: true,
		title: "Add parameter",
		buttons: {
			"<%=LNG_SAVE %>": function() {
				if (checkValues())
				{
					$("#param_name_input").val($("#param_name").val());
					$("#param_short_name_input").val($("#param_short_name").val());
					$("#param_type_input").val($("#param_type").val());
					$("#param_default_value_input").val($("#param_default_value").val());
					$("#param_visible_input").val($("#param_visible").val());
					$("#param_action_input").val($("#param_action").val());				
					submitForm('action_form',false,function(){});
					$("#form_html_field").val(tinyMCE.get('form_html_area').getContent());
					$("#form_name_field").val($("#form_name").val());
					var selectedTab = $("#form_headers").find("li").get($( "#form_tabs" ).tabs('option', 'selected'));
					var selectedId = $(selectedTab).find("a").attr("id");
					$("#form_id_field").val(selectedId);
					$("#fields_form").submit();
				}
			},
			"<%=LNG_CANCEL %>": function() {
				$( this ).dialog( "close" );
			}
		},
		open: function() {
			refreshInputTypes();
		},
		close: function() {
			resetAllFlags();
			clearActionForm();
			allFields.removeClass( "ui-state-error" );
		}
	});
	
	$( "#form_dialog" ).dialog({
		autoOpen: false,
		height: 250,
		width: 300,
		modal: true,
		buttons: {
			"<%=LNG_SAVE %>": function() {
				if (checkFormValues())
				{
					formDialogSaveFunction();
				}
			},
			"<%=LNG_CANCEL %>": function() {
				$(this).dialog( "close" );
			}
		},
		open: function() {
		},
		close: function() {
			resetAllFlags();
			clearActionForm();
			$("#form_dialog").find("#form_name").removeClass( "ui-state-error" );
		}
	});
	
	$("#param_type").change(function() {
		refreshInputTypes();
	});
	
});
</script>
</head>
<body style="margin:0" class="body">

<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	<tr>
	<td>
	<table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
		<tr>
		<td width=100% height=31 class="main_table_title"><a href="#" class="header_title"><%=LNG_EDIT_TEMPLATE%></a></td>
		</tr>
		<tr>
		<td height="100%" valign="top" class="main_table_body">
			<div style="margin:10px">
				<table width="100%" height="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<!--a style="margin:5px;" id="add_form_button"><%=LNG_ADD_FORM%></a-->
							<!--a style="margin:5px;" id="delete_form_button"><%=LNG_DELETE_FORM%></a-->
						</td>
					</tr>
					<tr>
						<td class="tabs_container" style="text-align:left; vertical-align:top; padding:0px">
							<div id="form_tabs" style="">
								<ul id="form_headers">
								</ul>
								<div id='form_tab'>
									<p><strong><%=LNG_NAME%>:</strong></p>
									<input type="text" id="form_name" value="<%=HtmlEncode(formName)%>"></input>
									<p><strong><%=LNG_FORM_HTML%>:</strong></p>
									<textarea id='form_html_area' style='width:100%'><%=formHtml%></textarea>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td style="padding-top:10px;">
							<div><strong><%=LNG_PARAMETERS%>:</strong></</div>
							<table height="100%" width="100%" cellspacing="0" cellpadding="3" class="data_table" style="margin-top:10px;">
								<tr class="data_table_title">
									<td class="table_title"><%=LNG_NAME %></td>
									<td class="table_title"><%=LNG_SHORT_NAME%></td>
									<td class="table_title"><%=LNG_TYPE %></td>
									<td class="table_title"><%=LNG_DEFAULT_VALUE%></td>
									<td class="table_title"><%=LNG_VISIBLE%>&nbsp;<img src="images/help.png" title="<%=LNG_PARAMETER_VISIBILITY_IN_ALERT_LIST%>"/></td>
									<td class="table_title" width="135"><%=LNG_ACTION%></td>
									<td class="table_title" width="135"><%=LNG_ACTIONS %></td>
								</tr>
								<%
					
									Set RS = Conn.Execute("SELECT id, name, short_name, type, [default], visible, action FROM alert_parameters WHERE temp_id='"&tempId&"' AND [type] IN (0,1,2,6,11,12) AND ([action] < 11 OR [action] IS NULL) ORDER by [id]")
									While Not RS.EOF
								%>
								<tr id="<%=RS("id")%>">
									<td value="<%=RS("name")%>" class="name_cell"><% Response.Write RS("name") %></td>
									<td value="<%=RS("short_name")%>" class="short_name_cell"><% Response.Write RS("short_name") %></td>
									<td value="<%=RS("type")%>" class="type_cell">	
									<%
										typeStr=""
										Select Case  RS("type")
										case "0"
											typeStr = LNG_TEXT
										case "1"
											typeStr = LNG_NUMBER
										case "2"
											typeStr = LNG_TYPE_SELECT
										'case "3"
										'	typeStr = LNG_RADIO
										case "6"
											typeStr = LNG_DATETIME
										case "11"
											typeStr = LNG_HIDDEN
										case "12"
											typeStr = LNG_TIME_IN_MINUTES
										case else
											typeStr = ""
										End select
										Response.Write typeStr
									%>
									</td>
									
									
									<% 
									
									defaultValue = RS("default") 
									defaultValueHTML = RS("default") 
									if (RS("type") = 6) then
										convertRS = Conn.Execute("if ('"&RS("default") &"'<>'') SELECT CONVERT(DATETIME,'"&defaultValue &"') AS value ELSE SELECT '"&defaultValue &"' as value")
										defaultValue = convertRS("value")
										defaultValueHTML = "<scr" & "ipt language='javascript'>document.write( getUiDateTime('"&defaultValue& "'))</scr" & "ipt>"
									elseif (RS("type") = 12) then
										defaultValueHTML = "<scr" & "ipt language='javascript'>document.write( getMinutesTime('"&defaultValue& "'))</scr" & "ipt>"
									else
										defaultValueHTML = RS("default") 
									end if
									%>
									<td value="<%=defaultValue%>" class="default_value_cell">	
										<%
										Response.Write defaultValueHTML
										%>
									
									</td>
									<td value="<%=RS("visible")%>" class="visible_cell">
									<%
										visibleStr=""
										Select Case  RS("visible")
										case "True"
											visibleStr = LNG_YES
										case "False"
											visibleStr = LNG_NO
										case else
											visibleStr = ""
										End select
										Response.Write visibleStr
									%>
									</td>
									<td value="<%=RS("action")%>" class="action_cell">	
									<%
										actionStr=""
										Select Case  RS("action")
										case "0"
											actionStr = LNG_SET_ALERT_TITLE
										case "7"
											actionStr = LNG_SET_ALERT_START_DATE_TIME
										case "9"
											actionStr = LNG_ADD_TIME_TO_START_DATE_TIME
										case else
											actionStr = ""
										End select
										Response.Write actionStr
									%>
									</td>
									<td align="center">
									<a class="edit_param_button" href="javascript: editParam('<%=RS("id")%>')"><%=LNG_EDIT%></a>
									<a class="delete_param_button" href="javascript: deleteParam('<%=RS("id")%>')"><%=LNG_DELETE%></a>
									<a class="insert_in_text_param_button" href="javascript: insertParamInText('<%=RS("id")%>')"><%=LNG_INSERT_INTO_TEXT%></a>
									</td>
								</tr>
								<%	
										RS.MoveNext		
									Wend
									RS.Close
								%>
							</table>
						</td>
					</tr>
					<tr>
						<td align="right" style="padding-top:10px">
							<a id="add_param_button" href="javascript: addParam();"><%=LNG_ADD %></a>
							<a id="save_button" href="javascript: saveHtml()"><%=LNG_SAVE %></a>
						</td>
					</tr>
				</table>
			</div>
		</td>
	</tr>
</table>


<div id="form_dialog" title="">
	<p class="formValidateTips"><%=LNG_PLEASE_FILL_ALL_EMPTY_FIELDS%></p>
	<form>
		<fieldset>
			<label for="form_name"><%=LNG_NAME%></label>
			<input type="text" name="form_name" id="form_name" class="text ui-widget-content ui-corner-all" />
			<!-- this field prevent form submit on "enter" click--><input type="text" style="display:none"/>
		</fieldset>
	</form>
</div>

<div id="dialog-form" title="">
	<p class="validateTips"><%=LNG_PLEASE_FILL_ALL_EMPTY_FIELDS%></p>
	<form>
		<fieldset>
			<label for="param_name"><%=LNG_NAME%></label>
			<input type="text" name="param_name" id="param_name" class="text ui-widget-content ui-corner-all" />
			<label for="param_short_name"><%=LNG_SHORT_NAME%></label>
			<input type="text" name="param_short_name" id="param_short_name" value="" class="text ui-widget-content ui-corner-all" />
			<label for="param_type"><%=LNG_TYPE%></label>
			<select name="param_type" id="param_type" value="" class="text ui-widget-content ui-corner-all" style="margin-bottom:10px">
				<option selected="selected" value="0"><%=LNG_TEXT%></option>
				<option value="1"><%=LNG_NUMBER%></option>
				<option value="2"><%=LNG_TYPE_SELECT%></option>
				<!--option value="3"><%=LNG_RADIO%></option-->
				<option value="6"><%=LNG_DATETIME%></option>
				<option value="11"><%=LNG_HIDDEN%></option>
				<option value="12"><%=LNG_TIME_IN_MINUTES%></option>
			</select>
			<label for="param_default_value"><%=LNG_DEFAULT_VALUE%></label>
			<input type="text" name="param_default_value" id="param_default_value" value="" class="text ui-widget-content ui-corner-all" />
			<label for="param_visible"><%=LNG_VISIBLE%>&nbsp;<img src="images/help.png" title="<%=LNG_PARAMETER_VISIBILITY_IN_ALERT_LIST%>"/></label>
			<select name="param_visible" id="param_visible" value="" class="text ui-widget-content ui-corner-all" style="margin-bottom:10px">
				<option selected="selected" value="1"><%=LNG_YES%></option>
				<option value="0"><%=LNG_NO%></option>
			</select>
			<label for="param_action"><%=LNG_ACTION%></label>
			<select name="param_action" id="param_action" value="" class="text ui-widget-content ui-corner-all" style="margin-bottom:10px">
				<option value="0"><%=LNG_SET_ALERT_TITLE%></option>
				<option value="7"><%=LNG_SET_ALERT_START_DATE_TIME%></option>
				<option value="9"><%=LNG_ADD_TIME_TO_START_DATE_TIME%></option>
				<option selected="selected" value=""><%=LNG_NONE%></option>
			</select>
		</fieldset>
	</form>
</div>



<form id="action_form" action='alert_templates_actions.asp?id=<%=tempId%>' name='action_form' method='POST'>
	<input type="hidden" name="param_id" id="param_id_input" ></input>
	<input type="hidden" name="param_name" id="param_name_input" ></input>
	<input type="hidden" name="param_short_name" id="param_short_name_input" ></input>
	<input type="hidden" name="param_type" id="param_type_input"></input>
	<input type="hidden" name="param_default_value" id="param_default_value_input" ></input>
	<input type="hidden" name="param_visible" id="param_visible_input" ></input>
	<input type="hidden" name="param_action" id="param_action_input"></input>
	<input type="hidden" id="save_input" name="save" value="0"></input>
	<input type="hidden" id="edit_param_input" name="edit_param" value="0"></input>
	<input type="hidden" id="add_param_input" name="add_param" value="0"></input>
	<input type="hidden" id="delete_param_input" name="delete_param" value="0"></input>
	<input type="hidden" id="get_form_input" name="get_form" value="0"></input>
	<input type="hidden" id="order_forms_input" name="order_forms" value="0"></input>
	<input type="hidden" id="add_form_input" name="add_form" value="0"></input>
	<input type="hidden" id="delete_form_input" name="delete_form" value="0"></input>
	<input id="form_name_input" name="form_name" type="hidden" value=""></input>
	<input id="form_id_input" name="form_id" type="hidden" value=""></input>
	<input id="form_html_input" name="form_html" type="hidden" value=""></input>
	<input id="forms_json_input" name="forms_json" type="hidden" value=""></input>
</form>
<form id="fields_form" action='alert_templates_edit.asp?id=<%=tempId%>' name='fields_form' method='post'>
	<input id="form_html_field" name="form_html" type="hidden" value=""></input>
	<input id="form_name_field" name="form_name" type="hidden" value=""></input>
	<input id="form_id_field" name="form_id" type="hidden" value=""></input>
</form>
<div id="dialog-confirm" title="">
	<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 10px 0;"></span><font id="confirmation_text"></font></p>
</div>
<div id="dialog-modal" title="" style='display:none'>
	<p></p>
</div>
</body>
</html>