﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<!-- #include file="md5.asp" -->
<%
check_session()
uid = Session("uid")
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>DeskAlerts</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link href="css/style9.css" rel="stylesheet" type="text/css">
	<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
	<script language="javascript" type="text/javascript">
		$(function() {
			$(".cancel_button").button();
			$(".save_button").button();
		});
	</script>
</head>

<body style="margin:0px" class="body">
<table width="100%" border="0" cellspacing="0" height="100%">
	<tr>
	<td>
	<table width="100%" height="100%" cellspacing="0" cellpadding="0">
		<tr>
		<td height="100%">
		<div style="margin:10px;">
<%
if uid <> "" then
	
	admin = 0
	set RS = Conn.Execute("SELECT id, role FROM users WHERE id="&uid)
	if not RS.EOF then
		if RS("role") = "A" then
			admin = 1
		end if
		
		set rs_policy = Conn.Execute("SELECT type FROM policy INNER JOIN policy_editor ON policy.id=policy_editor.policy_id WHERE editor_id=" & uid)
		if not rs_policy.EOF then
			if rs_policy("type") = "A" then
				admin = 1
			end if
		end if
	end if
	
	if admin = 1 then
		if(request("a")="1") then
			SQL = "UPDATE archive_settings SET move_type='"&request("move_type")&"', move_number="&request("move_number")&", move_date_type='"&request("move_date_type")&"', clear_type='"&request("clear_type")&"', clear_number="&request("clear_number")&", clear_date_type='"&request("clear_date_type")&"' WHERE id=1"
			Conn.Execute(SQL)
		end if
		
		move_type_no = "checked"
		clear_type_no = "checked"
		
		Set RSsettings = Conn.Execute("SELECT move_type, clear_type, move_number, clear_number, move_date_type, clear_date_type FROM archive_settings WHERE id=1")
		if(Not RSsettings.EOF) then
			if(RSsettings("move_type")="Y") then
				move_type_yes = "checked"
				move_type_no = ""
			else
				move_type_yes = ""
				move_type_no = "checked"
			end if
			if(RSsettings("clear_type")="Y") then
				clear_type_yes = "checked"
				clear_type_no = ""
			else
				clear_type_yes = ""
				clear_type_no = "checked"
			end if
%>

<form method="post" action="settings_data_archiving.asp">
<table border="0"><tr><td>
<span class="work_header"><%=LNG_DATA_ARCHIVING_SETTINGS %></span><br/>
<%if(request("a")="1") then%>
	<b><%=LNG_ARCHIVE_SETTINGS_HAVE_BEEN_SUCESSFULLY_CHANGED %>.</b>
<%end if%>
<table cellpadding=4 cellspacing=4 border=0>
<tr><td><%=LNG_MOVE_TO_ARCHIVE %><br/>

<input type="radio" name="move_type" value="N" <%=move_type_no %> /> <%=LNG_DO_NOT_MOVE %><br/>
<input type="radio" name="move_type" value="Y" <%=move_type_yes %>/> <%=LNG_MOVE_RECORDS_OLDER_THAN %>: 
<select name="move_number">
<%
	kk=1
	do while kk<=31
		move_number_checked = ""
		if(RSsettings("move_number")=kk) then
			move_number_checked = "selected"
		end if
%>
<option value="<%=kk %>" <%=move_number_checked %>><%=kk %></option>
<%
		kk=kk+1
	loop
%>
</select>
<select name="move_date_type">
<option value="D" <%if(RSsettings("move_date_type")="D") then response.write "selected" end if %> ><%=LNG_DAYS %></option>
<option value="M" <%if(RSsettings("move_date_type")="M") then response.write "selected" end if %>><%=LNG_MONTHS %></option>	
<option value="Y" <%if(RSsettings("move_date_type")="Y") then response.write "selected" end if %>><%=LNG_YEARS %></option>	
</select>
</td></tr>
<tr><td><%=LNG_CLEAR_ARCHIVE %><br/>
<input type="radio" name="clear_type" value="N"  <%=clear_type_no %> /> <%=LNG_DO_NOT_CLEAR %><br/>
<input type="radio" name="clear_type" value="Y" <%=clear_type_yes %> /> <%=LNG_CLEAR_RECORDS_OLDER_THAN %>: 
<select name="clear_number">
<%
	kk=1
	do while kk<=31
		clear_number_checked = ""
		if(RSsettings("clear_number")=kk) then
			clear_number_checked = "selected"
		end if
%>
<option value="<%=kk %>" <%=clear_number_checked %>><%=kk %></option>
<%
		kk=kk+1
	loop
%>
</select>
<select name="clear_date_type">
<option value="D" <%if(RSsettings("clear_date_type")="D") then response.write "selected" end if %>><%=LNG_DAYS %></option>
<option value="M" <%if(RSsettings("clear_date_type")="M") then response.write "selected" end if %>><%=LNG_MONTHS %></option>	
<option value="Y" <%if(RSsettings("clear_date_type")="Y") then response.write "selected" end if %>><%=LNG_YEARS %></option>	
</select>
</td></tr>
</table>
</td></tr>
<tr><td align="right">
</br>
	<a class="cancel_button" onclick="javascript:history.go(-1); return false;"><%=LNG_CANCEL %></a> 
	<a class="save_button" onclick="document.forms[0].submit()" ><%=LNG_SAVE %></a>
	<input type="hidden" name="a" value="1">
</td></tr></table>
</form>



<%
		end if
	end if
%>

		 <img src="images/inv.gif" alt="" width="1" height="100%" border="0">
		</div>
		</td>
		</tr>
	</table>
	</td>
	</tr>
</table>


</body>
</html>
<%

else

  Response.Redirect "index.asp"

end if

%> 
</body>
</html>
<!-- #include file="db_conn_close.asp" -->