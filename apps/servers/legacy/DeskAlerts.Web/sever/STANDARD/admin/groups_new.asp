﻿<% Response.CacheControl = "no-cache" %>
<% Response.AddHeader "Pragma", "no-cache" %>
<% Response.Expires = -1 %>
<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<!-- #include file="ad.inc" -->
<%
Server.ScriptTimeout = 300
Conn.CommandTimeout = 300

check_session()

uid = Session("uid")

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>DeskAlerts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/style9.css" rel="stylesheet" type="text/css">
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script language="javascript" type="text/javascript">

$(document).ready(function() {
	var width = $(".data_table").width();
	var pwidth = $(".paginate").width();
	if (width != pwidth) {
		$(".paginate").width("100%");
	}
	$(".search_button").button({
		icons : {
			primary : "ui-icon-search"
		}
	});
});

</script>
</head>
<script language="javascript" type="text/javascript" src="functions.js"></script>

<body style="margin:0px" class="iframe_body">

<%
	showCheckbox = request("sc")
	
	
   if(Request("offset") <> "") then 
	offset = clng(Request("offset"))
   else 
	offset = 0
   end if	

   if(Request("sortby") <> "") then 
	sortby = Request("sortby")
   else 
	sortby = "name"
   end if	

   if(Request("gname") <> "") then 
	mygname = Request("gname")
   else 
	mygname = ""
   end if	


if (uid <>"") then

	groups_arr = Array("checked","checked","checked","checked","checked","checked","checked")
	gid = 0
	gsendtoall = 0
	set RS = Conn.Execute("SELECT id, only_personal FROM users WHERE id="&uid&" AND role='E'")
	if not RS.EOF then
		gid = 1
		set rs_policy = Conn.Execute("SELECT p.id, p.type, pu.user_id FROM policy p INNER JOIN policy_editor pe ON p.id=pe.policy_id LEFT JOIN policy_user pu ON pu.policy_id = p.id AND pu.user_id = 0 WHERE pe.editor_id=" & uid)
		if not rs_policy.EOF then
			if rs_policy("type")="A" then 'can send to all
				gid = 0
			elseif not IsNull(rs_policy("user_id")) then
				gid = 0
				gsendtoall = 1
			end if
		end if
		if gid = 1 or gsendtoall = 1 then
			editor_id = RS("id")
			policy_ids = editorGetPolicyIds(editor_id)
			groups_arr = editorGetPolicyList(policy_ids, "groups_val")
		end if
	end if
	RS.Close



'counting pages to display
   if(Request("limit") <> "") then 
	limit = clng(Request("limit"))
   else 
	limit=25
   end if

	mygname = request("gname")
	if(mygname <> "") then 
		searchSQL = " (groups.name LIKE N'%"&Replace(mygname, "'", "''")&"%' ) "
	else 
		searchSQL = ""
	end if	
   
	search_id = request("id")
	search_type = request("type")
	if search_type = "" then search_type = "organization"

	cnt=0
	if (search_type <>"") then
	
		showSQL = 	" SET NOCOUNT ON" & vbCrLf &_
					" IF OBJECT_ID('tempdb..#tempOUs') IS NOT NULL DROP TABLE #tempOUs" & vbCrLf &_
					" CREATE TABLE #tempOUs  (Id_child BIGINT NOT NULL, Rights BIT NOT NULL);" & vbCrLf &_
					" IF OBJECT_ID('tempdb..#tempGroups') IS NOT NULL DROP TABLE #tempGroups" & vbCrLf &_
					" CREATE TABLE #tempGroups  (group_id BIGINT NOT NULL);" & vbCrLf &_
					" DECLARE @now DATETIME;" & vbCrLf &_
					" SET @now = GETDATE(); "
					
		OUsSQL=""

		if(search_type = "ou") then
			if Request("search") = "1" then
				OUsSQL = OUsSQL & "WITH OUs (id) AS("
				OUsSQL = OUsSQL &" SELECT CAST("&search_id&" AS BIGINT) "
				OUsSQL = OUsSQL &_
						"	UNION ALL" & vbCrLf &_
						"	SELECT Id_child" & vbCrLf &_
						"	FROM OU_hierarchy h" & vbCrLf &_
						"	INNER JOIN OUs ON OUs.id = h.Id_parent" & vbCrLf &_
						") " & vbCrLf &_
						"INSERT INTO #tempOUs (Id_child, Rights) (SELECT DISTINCT id, 0 FROM OUs) Option (MaxRecursion 10000); "
			else
				OUsSQL = OUsSQL &" INSERT INTO #tempOUs (Id_child, Rights) VALUES ("&search_id&", 0)"
			end if
		elseif(search_type = "DOMAIN") then
			OUsSQL = OUsSQL &" INSERT INTO #tempOUs (Id_child, Rights) (SELECT id, 0 FROM OU WHERE Id_domain = "&search_id&")"
		else
			OUsSQL = OUsSQL & " INSERT INTO #tempOUs (Id_child, Rights) (SELECT id, 0 FROM OU) "
		end if

		if (gid = 1) then 
			OUsSQL = OUsSQL & vbCrLf &_
				" ;WITH OUsList (Id_parent, Id_child) AS (" & vbCrLf &_
				" 	SELECT Id_child AS Id_parent, Id_child FROM #tempOUs" & vbCrLf &_
				"	UNION ALL" & vbCrLf &_
				"	SELECT h.Id_parent, l.Id_child" & vbCrLf &_
				"	FROM OU_hierarchy as h" & vbCrLf &_
				"	INNER JOIN OUsList as l" & vbCrLf &_
				"	ON h.Id_child = l.Id_parent)" & vbCrLf &_ 
				" UPDATE #tempOUs" & vbCrLf &_
				" SET Rights = 1" & vbCrLf &_
				" FROM #tempOUs t" & vbCrLf &_
				" INNER JOIN OUsList l" & vbCrLf &_
				" ON l.Id_child = t.Id_child" & vbCrLf &_
				" LEFT JOIN policy_ou p ON p.ou_id=l.Id_parent AND " & Replace(policy_ids, "policy_id", "p.policy_id") & vbCrLf &_
				" LEFT JOIN ou_groups g ON g.ou_id=l.Id_parent" & vbCrLf &_
				" LEFT JOIN policy_group pg ON pg.group_id=g.group_id AND " & Replace(policy_ids, "policy_id", "pg.policy_id") & vbCrLf &_
				" WHERE NOT p.id IS NULL OR NOT pg.id IS NULL"
		end if

		groupsSQL = " INSERT INTO #tempGroups (group_id)" & vbCrLf &_ 
					" (" & vbCrLf &_
					" SELECT DISTINCT groups.id" & vbCrLf &_
					" FROM groups" & vbCrLf &_
					" INNER JOIN OU_Group og" & vbCrLf &_
					" ON groups.id=og.Id_group" & vbCrLf &_
					" INNER JOIN #tempOUs t" & vbCrLf &_
					" ON og.Id_OU=t.Id_child"
		if (gid = 1) then 
			groupsSQL = groupsSQL & vbCrLf &_
					" LEFT JOIN policy_group p ON p.group_id=og.Id_group AND "&policy_ids & vbCrLf &_
					" WHERE (t.Rights = 1 OR NOT p.group_id IS NULL) " 
			if (searchSQL<>"") then 
				groupsSQL = groupsSQL & " AND " & searchSQL
			end if
		else		
			if (searchSQL<>"") then 
				groupsSQL = groupsSQL & " WHERE " &searchSQL
			end if
		end if
				
		groupsSQL = groupsSQL & " )"
			
		if (search_type <> "ou") then
			groupsSQL = groupsSQL & vbCrLf &_
						" INSERT INTO #tempGroups (group_id)" & vbCrLf &_
						" (" & vbCrLf &_
						" SELECT DISTINCT groups.id" & vbCrLf &_
						" FROM groups"
			if (gid = 1) then 
				groupsSQL = groupsSQL & " INNER JOIN policy_group p ON p.group_id=groups.id AND "&policy_ids
			end if
				groupsSQL = groupsSQL & vbCrLf &_	
						" LEFT JOIN #tempGroups t" & vbCrLf &_
						" ON t.group_id=groups.id" & vbCrLf &_
						" WHERE t.group_id IS NULL "
			if (search_type = "DOMAIN") then
				groupsSQL = groupsSQL & " AND groups.domain_id = "&search_id
			end if 
				if (searchSQL<>"") then 
					groupsSQL = groupsSQL & " AND " & searchSQL
				end if
				groupsSQL = groupsSQL & " ) "	
		end if
		
		groupsSQL = groupsSQL & vbCrLf &_
				" SELECT COUNT(1) as cnt FROM #tempGroups " & vbCrLf &_	
				" SELECT t.group_id as id, groups.context_id, groups.name as name, domains.name as domain_name, groups.custom_group," & vbCrLf &_
				" (SELECT COUNT(1) FROM users_groups ug WHERE ug.group_id=t.group_id) as users_cnt," & vbCrLf &_
				" (SELECT COUNT(1) FROM computers_groups cg WHERE cg.group_id=t.group_id) as computers_cnt," & vbCrLf &_
				" (SELECT COUNT(1) FROM groups_groups gg WHERE gg.container_group_id=t.group_id) as groups_cnt," & vbCrLf &_
				" (SELECT COUNT(1) FROM ou_groups og WHERE og.group_id=t.group_id) as ous_cnt," & vbCrLf &_
				" (SELECT COUNT(1) FROM groups_groups gg WHERE gg.group_id=t.group_id) as member_of_cnt," & vbCrLf &_
				" edir_context.name as context_name" & vbCrLf &_
				" FROM #tempGroups t" & vbCrLf &_
				" LEFT JOIN groups ON t.group_id = groups.id" & vbCrLf &_
				" LEFT JOIN domains ON domains.id = groups.domain_id" & vbCrLf &_
				" LEFT JOIN edir_context ON edir_context.id = groups.context_id" & vbCrLf &_
				" ORDER BY " & sortby & vbCrLf &_
				" DROP TABLE #tempOUs" & vbCrLf &_
				" DROP TABLE #tempGroups"
		
		showSQL = showSQL & OUsSQL & groupsSQL

        Response.Write showSQL
		Set RS = Conn.Execute(showSQL)

	
		if(Not RS.EOF) then
			cnt=RS("cnt")
			SET RS = RS.NextRecordSet
		end if

		j=cnt/limit

		checkedOUs=request("ous")
		checkedObject = ""
		if(InStr(checkedOUs, search_id)>0) then
			'checkedObject = "checked"
		end if
	end if
%>

<table width="100%" border="0"><tr valign="middle"><td width="225">
<form name="search_users" action="groups_new.asp?sc=<%=showCheckbox %>&id=<%=search_id %>&type=<%=search_type %>&ous=<%=checkedOUs %>&offset=0&limit=<%=limit %>&sortby=<%=sortby %>" method="post">
    <%=LNG_SEARCH_GROUPS %>: 
    <input type="text" name="gname" value="<%=HTMLEncode(mygname) %>"/></td>
    <td width="50">
        <a name="sub" class="search_button" onclick="javascript:document.forms[0].submit()"><%=LNG_SEARCH %></a>
		<input type="hidden" name="search" value="1" />
    <%
	if myuname <> "" or Request("search") = "1" then
		response.write "<td> "&LNG_YOUR_SEARCH_BY &" """&HTMLEncode(mygname)&""" "&LNG_KEYWORD &":</td>"
    end if
    %>
    </td>
    <td>
<%
if(ConfEnableCustomGroups=1) then

	if(gid = 0 OR (gid = 1 AND groups_arr(0)<>"")) then
%>

<%if(request("domain_id")<>"") then%>
	<div align="right"><a href="edit_group.asp?domain_id=<%=Request("domain_id")%>&return_page=<%=Server.URLEncode("groups_new.asp?"&Request.QueryString) %>" target="content_iframe"><img src="images/langs/<%=default_lng %>/add_group_button.gif" alt="<%=LNG_ADD_GROUP %>" border="0"></a></div>
<%end if%>

<%
end if
end if
%>

</td>
</tr></table>
</form>

<%
if(cnt>0) then

	page="groups_new.asp?sc="&showCheckbox&"&id="&search_id&"&type="&search_type&"&ous="&checkedOUs&"&gname=" & mygname &"&search="&Request("search")&"&"
	name=LNG_GROUPS
	Response.Write make_pages(offset, cnt, limit, page, name, sortby) 

%>

		</td></tr>
		</table>
		<table height="100%" class="data_table" cellspacing="0" cellpadding="3">
		<tr class="data_table_title">
		<%if(showCheckbox=1) then%>
<td width="10"><input type='checkbox' name='select_all' id='select_all' onclick='javascript: select_all_objects_ou();'></td>
<%end if%>
		
		<td class="table_title"><% response.write sorting(LNG_GROUP_NAME,"name", sortby, offset, limit, page) %></td>
		<td class="table_title"><% response.write sorting(LNG_DOMAIN,"domain_name", sortby, offset, limit, page) %></td>
<% if(showCheckbox<>1) then		%>
		<td class="table_title" ><% response.write sorting(LNG_USERS,"users_cnt", sortby, offset, limit, page) %></td>
<%
	if(EDIR<>1) then
%>
		<td class="table_title" ><% response.write sorting(LNG_COMPUTERS,"computers_cnt", sortby, offset, limit, page) %></td>
<% 
	end if
%>
		
<%if(EDIR=1) then%>		<td class="table_title"><%=LNG_CONTEXT %></td> <%end if%>

		<td class="table_title" ><% response.write sorting(LNG_GROUPS,"groups_cnt", sortby, offset, limit, page) %></td>
		<td class="table_title" ><% response.write sorting(LNG_PARENT_GROUPS,"member_of_cnt", sortby, offset, limit, page) %></td>
		<td class="table_title" ><%=LNG_ACTIONS %></td>
<%end if%>		
				</tr>
<%

'show main table
	'Set RS = Conn.Execute("SELECT DISTINCT groups.id as id, context_id, groups.name as name, domains.name as domain_name, custom_group, COUNT(distinct users_groups.user_id) as users_cnt, COUNT(distinct computers_groups.comp_id) as computers_cnt, COUNT(distinct groups_groups.group_id) as groups_cnt FROM groups "&joinSQL&" LEFT JOIN users_groups ON groups.id=users_groups.group_id LEFT JOIN computers_groups ON groups.id=computers_groups.group_id LEFT JOIN groups_groups ON groups.id=groups_groups.container_group_id WHERE domains.id<>0 " & searchSQL &" GROUP BY groups.id, groups.name,  domains.name, custom_group, context_id ORDER BY "&sortby)

	num=0
	Do While Not RS.EOF
		num=num+1
		if(num > offset AND offset+limit >= num) then
		cnt1=0

        	%>
		<tr>
		<%
if(showCheckbox=1) then
	strObjectID = RS("id")
%>
	<td><input type="checkbox" name="objects" id="object<%=strObjectID%>" value="<%=strObjectID%>" <%=checkedObject %> onclick="checkboxClicked(event);"></td>
<%
end if		%>
		
		<td>
		<% Response.Write RS("name") %>
		</td><td>
		<% Response.Write RS("domain_name") %>
		</td>
<% if(showCheckbox<>1) then		%>		
		<td align="center">
		<a href="view_user_group.asp?id=<%=RS("id") %>"><% Response.Write RS("users_cnt") %></a>
		</td>
<%
		if(EDIR<>1) then
%>
		<td align="center"> 
		<a href="view_comp_group.asp?id=<%=RS("id") %>"><% Response.Write RS("computers_cnt") %></a>
		</td>
<%
		end if
		if (EDIR = 1) then 	
			strContext="&nbsp;"
			if(Not IsNull(RS("context_name"))) then
				strContext=RS("context_name")
			end if

			strContext=replace (strContext,",ou=",".")
			strContext=replace (strContext,",o=",".")
			strContext=replace (strContext,",dc=",".")
			strContext=replace (strContext,",c=",".")

			strContext=replace (strContext,"ou=","")
			strContext=replace (strContext,"o=","")
			strContext=replace (strContext,"dc=","")
			strContext=replace (strContext,"c=","")

			Response.Write "<td>" & strContext & "</td>"
		 end if

%>

		<td align="center"> 
		<a href="view_group_group.asp?id=<%=RS("id") %>"><%=RS("groups_cnt") %></a>
		</td>
		
		<td align="center"> 
		<a href="view_group_group.asp?id=<%=RS("id") %>&member_of=1"><%=RS("member_of_cnt") %></a>
		</td>

		<td align="center" nowrap>
			<% if groups_arr(1) = "checked" then %>
			<a href="edit_group.asp?wh=1&id=<%=RS("id") %>&domain_id=<%Request("domain_id") %>&return_page=<%=Server.URLEncode("groups_new.asp?"&Request.QueryString) %>"><img src="images/action_icons/edit.png" alt="<%=LNG_EDIT_GROUP %>" title="<%=LNG_EDIT_GROUP %>" width="16" height="16" border="0" hspace=5></a>
			<% end if %>
			<% if groups_arr(2) = "checked" then %>
			<a href="group_delete.asp?id=<%=RS("id") %>&domain_id=<%Request("id") %>&return_page=<%=Server.URLEncode("groups_new.asp?"&Request.QueryString) %>"><img src="images/action_icons/delete.gif" alt="<%=LNG_DELETE_GROUP %>" title="<%=LNG_DELETE_GROUP %>" width="16" height="16" border="0" hspace=5></a>
			<%end if%>
		</td>
		<%end if%>
		</tr>
		<%
		end if
	RS.MoveNext
	Loop
	RS.Close

%>			
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr><td> 


<%

	'page="groups_new.asp?gname=" & mygname & "&id=" & Request("id") & "&"
	'name="groups"
	Response.Write make_pages(offset, cnt, limit, page, name, sortby) 

%>

		</td></tr>
		</table>
 
                </table>
<!--		<img src="images/inv.gif" alt="" width="1" height="100%" border="0">-->



<%
else
	Response.Write "<center><b>"&LNG_THERE_ARE_NO_GROUPS &".</b><br><br></center>"
end if
else

  Response.Redirect "index.asp"

end if
%> 
</body>
</html>
<!-- #include file="db_conn_close.asp" -->