﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Parameters;

namespace DeskAlertsDotNet.Pages
{
    using Resources;

    public partial class ViewEditorsList : DeskAlertsBaseListPage
    {
        public string PolicyId { get; set; }

        protected override void OnLoad(EventArgs e)
        {
            PolicyId = Request.GetParam("id", "");

            if (string.IsNullOrEmpty(PolicyId))
                return;

            base.OnLoad(e);

            editorNameCellHeader.Text = resources.LNG_NAME;

            int cnt = this.Content.Count < (Offset + Limit) ? this.Content.Count : Offset + Limit;

            for (int i = Offset; i < cnt; i++)
            {
                DataRow row = this.Content.GetRow(i);
                TableRow tableRow = new TableRow();

                TableCell nameCell = new TableCell()
                {
                    Text = row.GetString("name")
                };

                tableRow.Cells.Add(nameCell);
                contentTable.Rows.Add(tableRow);
            }
        }

        #region DataProperties
        protected override string DataTable
        {
            get
            {
                return string.Empty;
            }
        }

        protected override string[] Collumns
        {
            get
            {
                return new string[] {};
            }
        }

        protected override string DefaultOrderCollumn
        {
            //return collumn for default order
            get
            {
                return "id";               

            }
        }

        protected override string PageParams
        {
            get
            {
                if (!string.IsNullOrEmpty(PolicyId))
                {
                    return "id=" + PolicyId + "&";
                }

                return "";
            }
        }
        

        protected override string CustomQuery {
            get
            {
                return "SELECT u.name FROM users u LEFT JOIN policy_editor pe ON pe.editor_id = u.id WHERE pe.policy_id =" + PolicyId;
            }
        }

        protected override string CustomCountQuery
        {
            get 
            { 
                return "SELECT COUNT(id) as cnt FROM policy_editor WHERE policy_id=" + PolicyId; 
            }
        }
        /*
         * 
         * If you want to add condition for data selection override property ConditionSqlString 
         * Also you can implement your custom query by overriding properties CustomQuery and CustomCountQuery
         *  
         */
        #endregion

        #region Interface properties

        protected override IButtonControl[] DeleteButtons
        {
            get
            {
                return new IButtonControl[] {  };
            }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl NoRowsDiv
        {
            get { return NoRows; }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl TableDiv
        {
            get { return mainTableDiv; }
        }

        protected override string ContentName
        {
            get
            {
                //return content name here
                return resources.LNG_EDITORS;
            }
        }

        protected override HtmlGenericControl BottomPaging
        {
            get { return bottomPaging; }
        }

        protected override HtmlGenericControl TopPaging
        {
            get { return topPaging; }
        }

        #endregion
    }
}