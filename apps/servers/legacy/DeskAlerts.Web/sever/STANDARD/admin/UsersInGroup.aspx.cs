﻿using DeskAlerts.Server.Utils;
using DeskAlerts.Server.Utils.Helpers;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Pages;
using Resources;
using System;
using System.Web.UI.WebControls;

namespace DeskAlerts.Server.sever.STANDARD.admin
{
    public partial class UsersInGroup : DeskAlertsBaseListPage
    {
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);


            string domainInfoQuery = "SELECT domains.name as name, domain_id, groups.name as gr_name  FROM domains INNER JOIN groups ON groups.domain_id=domains.id WHERE groups.id= " + Request["id"];

            DataSet domainInfoSet = dbMgr.GetDataByQuery(domainInfoQuery);

            domainName.InnerText = domainInfoSet.GetString(0, "name");
            groupName.InnerText = domainInfoSet.GetString(0, "gr_name");
            int cnt = Content.Count < (Offset + Limit) ? Content.Count : Offset + Limit;
            //  Response.Write(Request.Url.AbsoluteUri);
            userName.Text = GetSortLink(resources.LNG_USERNAME, "name", Request["sortBy"]);

            mobilePhone.Text = GetSortLink(resources.LNG_MOBILE_PHONE, "mobile_phone", Request["sortBy"]);
            domain.Text = resources.LNG_DOMAIN;
            email.Text = GetSortLink(resources.LNG_EMAIL, "email", Request["sortBy"]);
            online.Text = GetSortLink(resources.LNG_ONLINE, "last_request", Request["sortBy"]);
            lastActivity.Text = GetSortLink(resources.LNG_LAST_ACTIVITY, "last_request", Request["sortBy"]);

            actionsCollumn.Text = resources.LNG_ACTIONS;


            for (int i = Offset; i < cnt; i++)
            {
                var row = Content.GetRow(i);
                var tableRow = new TableRow();
                var chb = GetCheckBoxForDelete(row);
                var checkBoxCell = new TableCell { HorizontalAlign = HorizontalAlign.Center };
                checkBoxCell.Controls.Add(chb);
                tableRow.Cells.Add(checkBoxCell);

                var usernameCell = new TableCell { Text = row.GetString("name") };
                tableRow.Cells.Add(usernameCell);


                object domainNameStr = dbMgr.GetScalarByQuery("SELECT name FROM domains WHERE id = " + row.GetString("domain_id"));
                var domainCell = new TableCell
                {
                    Text = domainNameStr?.ToString() ?? "",
                    HorizontalAlign = HorizontalAlign.Center
                };
                tableRow.Cells.Add(domainCell);


                var phoneCell = new TableCell
                {
                    Text = row.GetString("mobile_phone"),
                    HorizontalAlign = HorizontalAlign.Center
                };
                tableRow.Cells.Add(phoneCell);

                TableCell emailCell = new TableCell
                {
                    Text = row.GetString("email"),
                    HorizontalAlign = HorizontalAlign.Center
                };
                tableRow.Cells.Add(emailCell);

                var lastRequest = row.GetDateTime("last_request");

                var statusImg = new Image
                {
                    Width = 11,
                    Height = 11
                };

                if (lastRequest.CompareTo(DateTime.MinValue) != 0)
                {
                    var dateDiff = (int)(DateTime.Now - lastRequest).TotalMinutes;
                    var nextRequest = row.GetString("next_request");
                    var onlineCounter = 2;
                    if (nextRequest.Length != 0)
                    {
                        onlineCounter = (Convert.ToInt32(nextRequest) / 2) * 60;
                    }

                    if (onlineCounter < dateDiff)
                    {
                        statusImg.ImageUrl = "images/offline.gif";
                        statusImg.AlternateText = resources.LNG_OFFLINE;
                    }
                    else
                    {
                        statusImg.ImageUrl = "images/online.gif";
                        statusImg.AlternateText = resources.LNG_ONLINE;
                    }
                }
                else
                {
                    statusImg.ImageUrl = "images/offline.gif";
                    statusImg.AlternateText = resources.LNG_OFFLINE;
                }

                TableCell statusCell = new TableCell();
                statusCell.Controls.Add(statusImg);
                statusCell.HorizontalAlign = HorizontalAlign.Center;

                tableRow.Cells.Add(statusCell);

                TableCell lastActivityCell = new TableCell
                {
                    Text = DeskAlertsUtils.IsNotEmptyDateTime(lastRequest) ? DateTimeConverter.ToUiDateTime(lastRequest) : string.Empty
                };

                tableRow.Cells.Add(lastActivityCell);

                LinkButton previewButton = GetActionButton(row.GetString("id"), "images/action_icons/preview.png", "LNG_VIEW_USER_DETAILS", "showPreview(" + row.GetString("id") + ")");

                TableCell actionsCell = new TableCell { HorizontalAlign = HorizontalAlign.Center };
                actionsCell.Controls.Add(previewButton);

                tableRow.Cells.Add(actionsCell);

                contentTable.Rows.Add(tableRow);
            }
        }



        protected override string[] Collumns => new string[] { };

        protected override string ContentName => resources.LNG_USERS;

        protected override string DataTable => "";

        protected override System.Web.UI.HtmlControls.HtmlGenericControl NoRowsDiv => NoRows;

        protected override System.Web.UI.HtmlControls.HtmlGenericControl TableDiv => mainTableDiv;


        protected override string DefaultOrderCollumn => "id";

        protected override string CustomQuery
        {
            get
            {
                var groupId = Request["id"];
                var ouUserIds = $@"(SELECT ou.Id_user AS id
                    FROM OU_User ou 
                    INNER JOIN ou_groups og ON og.ou_id = ou.Id_OU
                    WHERE og.group_id = {groupId})";

                var query = $@"SELECT DISTINCT users.id as id, 
                        next_request, 
                        name, 
                        context_id,
                        deskbar_id, 
                        domain_id, 
                        mobile_phone, 
                        email, 
                        reg_date, 
                        last_date, 
                        convert(varchar, last_request) as last_request1, 
                        last_request 
                    FROM users 
                    LEFT JOIN users_groups ON users.id = users_groups.user_id 
                    WHERE users_groups.group_id = {groupId} OR
                          users.id IN {ouUserIds}";

                if (!String.IsNullOrEmpty(Request["sortBy"]))
                    query += " ORDER BY " + Request["sortBy"];
                else
                    query += " ORDER BY name";

                return query;
            }
        }



        protected override string CustomCountQuery
        {
            get
            {
                var groupId = Request["id"];
                var ouUserIds = $@"(SELECT ou.Id_user AS id
                                   FROM OU_User ou 
                                   INNER JOIN ou_groups og ON og.ou_id = ou.Id_OU
                                   WHERE og.group_id = {groupId})";

                string query = $@"SELECT COUNT(DISTINCT u.id) as mycnt 
                    FROM users as u
                    LEFT JOIN users_groups as ug ON u.id = ug.user_id 
                    WHERE ug.group_id = {groupId} OR 
                          u.id IN {ouUserIds}";

                return query;
            }
        }


        protected override System.Web.UI.HtmlControls.HtmlGenericControl TopPaging => topPaging;
        protected override System.Web.UI.HtmlControls.HtmlGenericControl BottomPaging => bottomPaging;

        protected override string PageParams => "id=" + Request["id"] + "&";
    }
}
