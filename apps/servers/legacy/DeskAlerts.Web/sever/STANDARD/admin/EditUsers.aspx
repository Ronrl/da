﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditUsers.aspx.cs" Inherits="DeskAlertsDotNet.Pages.EditUsers" %>
<%@ Import NameSpace="DeskAlertsDotNet.Parameters" %>
<%@ Import NameSpace="Resources" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
<link href="css/style9.css" rel="stylesheet" type="text/css">
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>


<script language="javascript" type="text/javascript">

	$(document).ready(function() {
		$("#addUserButton").button();
	
		
		$("#changePasswordButton").button();
		$("#changePasswordButton").bind("click", function () {
		  //  alert("123");
		    $('#passwordLabel').show(1000);
			$('#password').show(1000);
			$('#password_changed').val("1");
		});
		
		
	});

</script>
</head>
<body>
    <form id="form1" runat="server">
    <table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	<tr>
	<td>
	<table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
		<tr>
			<td  height=31 class="main_table_title">
				<a runat="server" id="headerTitle" href="#" class="header_title"></a>
			</td>
		</tr>
		<tr>
		<td style="padding:5px; width:100%" height="100%"   valign="top" class="main_table_body">
			<div style="margin:10px;">
					<table style="padding:0px; margin-top:5px; margin-bottom:5px; border-collapse:collapse; border0px; width:100%;">
						<tr>
							<td>
								<label for="user_name"><%= resources.LNG_NAME%>:</label>
							</td>
						</tr>
						<tr>
							<td>
								<input runat="server" id="userName" type="text" value="" name="name"/>
							</td>
						</tr>
						<tr>
							<td>
								<label for="display_name"><%= resources.LNG_DISPLAY_NAME%>:</label>
							</td>
						</tr>
						<tr>
							<td>
								<input runat="server" id="displayName" type="text" value="" name="display"/>
							</td>
						</tr>
						<tr>
							<td style="padding-top:10px;">
								<input type="hidden" id="password_changed" name="password_changed" value="0"/>
								<a runat="server" id="changePasswordButton"><%= resources.LNG_CHANGE_PASSWORD%></a>
							</td>
						</tr>
						<tr runat="server" id="password_label_tr">
							<td style="padding-top:10px;">
								<label  runat="server" id="passwordLabel" for="password"><% = resources.LNG_PASSWORD %></label>
							</td>
						</tr>
						<tr runat="server" id="password_value_tr">
							<td>
								<input runat="server" id="password" type="password" value="" name="password"></input>
							</td>
						</tr>
						<tr runat="server" id="email_label_tr">
							<td style="padding-top:10px;">
								<label for="email"><%= resources.LNG_EMAIL%>:</label>
							</td>
						</tr>
						<tr runat="server" id="email_value_tr">
							<td>
								<input runat="server" id="email" type="text" value="" name="email"></input>
							</td>
						</tr>
						<tr runat="server" id="sms_label_tr">
							<td style="padding-top:10px;">
								<label for="mobile_phone"><%= resources.LNG_MOBILE_PHONE%>:</label>
							</td>
						</tr>
						<tr runat="server" id="sms_value_tr">
							<td>
								<input runat="server" id="mobilePhone" type="text" name="mobile_phone"></input>
							</td>
						</tr>
						<tr runat="server" id="ad_label_tr">
							<td style="padding-top:10px;">
								<label for="domain_id"><%= resources.LNG_DOMAIN%>:</label>
							</td>
						</tr>
						<tr runat="server" id="ad_value_tr">
							<td>
								<select runat="server" id="domainId" name="domain_id">
                                    </select>
                            </td>
                        </tr>
                        <tr>
							<td style="padding-top:10px;">
								<asp:LinkButton runat="server" id='addUserButton' />
							</td>
						</tr>
          </table>
    </table>
    </form>
</body>
</html>
