<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<!-- #include file="md5.asp" -->
<!-- #include file="functions_inc.asp" -->

<%
if request("name") = "setLicensesReminderCriticalPoint" then 
    licensesReminderCriticalPoint = Request("licensesReminderCriticalPoint")
    Conn.Execute("UPDATE [parameters] SET paramValue='" & licensesReminderCriticalPoint & "' WHERE paramName='licensesReminderCriticalPoint'")
end if

if request("name") = "getStatListOrder" then 
    SET RC = Conn.Execute("SELECT paramValue FROM [parameters] WHERE paramName='statListOrder'")
    if(Not RC.EOF) then  
        statListOrder = RC("paramValue")
    end if
    RC.Close

    response.write(statListOrder)
end if

if request("name") = "setStatListOrder" then 
    statListOrder = Request("statListOrder")
    Conn.Execute("UPDATE [parameters] SET paramValue='" & statListOrder & "' WHERE paramName='statListOrder'")
end if

if request("name") = "getMultiple" then 
    SET RC = Conn.Execute("SELECT paramValue FROM [parameters] WHERE paramName='multiple'")
    if(Not RC.EOF) then  
        multiple = RC("paramValue")
    end if
    RC.Close

    response.write(multiple)
end if

if request("name") = "setMultiple" then 
    multiple = Request("multiple")
    Conn.Execute("UPDATE [parameters] SET paramValue='" & multiple & "' WHERE paramName='multiple'")
end if

if request("name") = "getCloseHintStatus" then 
    SET RC = Conn.Execute("SELECT paramValue FROM [parameters] WHERE paramName='closeHintStatus'")
    if(Not RC.EOF) then  
        closeHintStatus = RC("paramValue")
    end if
    RC.Close

    response.write(closeHintStatus)
end if

if request("name") = "setCloseHintStatus" then 
    closeHintStatus = Request("closeHintStatus")
    Conn.Execute("UPDATE [parameters] SET paramValue='" & closeHintStatus & "' WHERE paramName='closeHintStatus'")
end if

if request("name") = "getMenuOrder" then 
    SET RC = Conn.Execute("SELECT paramValue FROM [parameters] WHERE paramName='menuOrder'")
    if(Not RC.EOF) then  
        menuOrder = RC("paramValue")
    end if
    RC.Close

    response.write(menuOrder)
end if

if request("name") = "setMenuOrder" then 
    menuOrder = Request("menuOrder")
    Conn.Execute("UPDATE [parameters] SET paramValue='" & menuOrder & "' WHERE paramName='menuOrder'")
end if

if request("name") = "getlistOrder" then 
    SET RC = Conn.Execute("SELECT paramValue FROM [parameters] WHERE paramName='listOrder'")
    if(Not RC.EOF) then  
        listOrder = RC("paramValue")
    end if
    RC.Close

    response.write(listOrder)
end if

if request("name") = "setListOrder" then 
    listOrder = Request("listOrder")
    Conn.Execute("UPDATE [parameters] SET paramValue='" & listOrder & "' WHERE paramName='listOrder'")
end if

if request("name") = "getDashboardListOrder" then 
    SET RC = Conn.Execute("SELECT paramValue FROM [parameters] WHERE paramName='dashboardListOrder'")
    if(Not RC.EOF) then  
        dashboardListOrder = RC("paramValue")
    end if
    RC.Close

    response.write(dashboardListOrder)
end if

if request("name") = "setDashboardListOrder" then 
    dashboardListOrder = Request("dashboardListOrder")
    Conn.Execute("UPDATE [parameters] SET paramValue='" & dashboardListOrder & "' WHERE paramName='dashboardListOrder'")
end if

if request("name") = "getDontShowTodayStatus" then 
    SET RC = Conn.Execute("SELECT paramValue FROM [parameters] WHERE paramName='dontShowTrialDialogToday'")
    if(Not RC.EOF) then  
        trialDialogTodayStatus = RC("paramValue")
    end if
    RC.Close

    response.write(trialDialogTodayStatus)
end if

if request("name") = "getShowTourStatus" then 
    SET RC = Conn.Execute("SELECT paramValue FROM [parameters] WHERE paramName='dontShowTour'")
    if(Not RC.EOF) then  
        tourStatus = RC("paramValue")
    end if
    RC.Close

    response.write(tourStatus)
end if

if request("name") = "getTrialAddonsStatus" then 
    SET RC = Conn.Execute("SELECT paramValue FROM [parameters] WHERE paramName='dontShowTrialAddonsDialog'")
    if(Not RC.EOF) then  
        trialAddonsStatus = RC("paramValue")
    end if
    RC.Close

    response.write(trialAddonsStatus)
end if

if request("name") = "getMaintenanceCheckbox" then 
    SET RC = Conn.Execute("SELECT paramValue FROM [parameters] WHERE paramName='maintenanceCheckbox'")
    if(Not RC.EOF) then  
        maintenanceCheckbox = RC("paramValue")
    end if
    RC.Close

    response.write(maintenanceCheckbox)
end if

if request("name") = "setExpires" then 
    expires = Request("expires")
    Conn.Execute("UPDATE [parameters] SET paramValue='" & expires & "' WHERE paramName='expires'")
end if

if request("name") = "setMaintenanceCheckbox" then 
    maintenanceCheckbox = Request("maintenanceCheckbox")
    Conn.Execute("UPDATE [parameters] SET paramValue='" & maintenanceCheckbox & "' WHERE paramName='maintenanceCheckbox'")
end if

if request("name") = "setTrialDialogStatus" then 
    dontShowToday = Request("dontShowToday")
    Conn.Execute("UPDATE [parameters] SET paramValue='" & dontShowToday & "' WHERE paramName='dontShowTrialDialogToday'")
end if

if request("name") = "setTourStatus" then 
    dontShow = Request("dontShow")
    Conn.Execute("UPDATE [parameters] SET paramValue='" & dontShow & "' WHERE paramName='dontShowTour'")
end if

if request("name") = "setTrialAddonsStatus" then 
    dontShow = Request("dontShow")
    Conn.Execute("UPDATE [parameters] SET paramValue='" & dontShow & "' WHERE paramName='dontShowTrialAddonsDialog'")
end if

if request("name") = "setHelpStatus" then 
    helpStatus = Request("helpStatus")
    Conn.Execute("UPDATE [parameters] SET paramValue='" & helpStatus & "' WHERE paramName='helpStatus'")
end if
%>