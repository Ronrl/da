﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MessageTemplatesGroups.aspx.cs" Inherits="deskalerts.MessageTemplatesGroups" %>
            
    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
    <html>

    <head>
        <title>DeskAlerts</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css" />
        <link href="css/style9.css" rel="stylesheet" type="text/css">
       <link href="css/jquery/jquery_timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css">
        <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
        <script type="text/JavaScript" language="JavaScript" src="jscripts/json2.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/preview.js?v=9.2.1.31"></script>
        <script language="javascript" type="text/javascript" src="functions.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/date.js"></script>
     
	    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery_timepicker/jquery-ui-timepicker-addon.js"></script>
	    <script language="javascript" type="text/javascript" src="jscripts/jquery/datepicker_fix.js"></script>
        <style rel="stylesheet" type="text/css">
            /*.ui-dialog-titlebar-close {
  visibility: hidden;
}*/
            
            .style1 {
                height: 26px;
            }
            
            .style4
            {
                height: 26px;
                width: 1180px;
            }
            .date_time_param_input{
	        width:150px
            }

            .style5
            {
                width: 5px;
            }

            </style>
    </head>

    <script language="javascript" type="text/javascript">




        function duplicateTGroup(id) {

            $.get('set_settings.asp?name=duplicateTGroups&srcId=' + id, function(data) {

                location.reload();
            });
            
        }
        function openDialogUI(_form, _frame, _href, _width, _height, _title) {
            $("#dialog-" + _form).dialog('option', 'height', _height);
            $("#dialog-" + _form).dialog('option', 'width', _width);
            $("#dialog-" + _form).dialog('option', 'title', _title);
            $("#dialog-" + _frame).attr("src", _href);
            $("#dialog-" + _frame).css("height", _height);
            $("#dialog-" + _frame).css("display", 'block');
            if ($("#dialog-" + _frame).attr("src") != undefined) {
                $("#dialog-" + _form).dialog('open');
                $("#dialog-" + _form).contents().find('#table_border').css('border', 'none');
            }
        }


    
       
        $(document).ready(function() {
            $("#confirm_sending").dialog({
                autoOpen: false,
                height: 100,
                width: 300,
                modal: false,
                position: {
                    my: "center top",
                    at: "center top",
                    of: $('#parent_for_confirm')
                },
                hide: {
                    effect: 'fade',
                    duration: 1000
                },
                //show: { effect: "highlight", color: '#98FB98', duration: 2000 },
                open: function(event, ui) {
                    $(".ui-dialog-titlebar-close", ui.dialog || ui).hide();
                    setTimeout(function() {
                        $("#confirm_sending").dialog('close');
                    }, 3000);
                },
                closeOnEscape: false
            });

            $("#dialog-form").dialog({
                autoOpen: false,
                height: 100,
                width: 100,
                modal: true
            });


            var is_sended = $("#is_sended").val();
            if (is_sended == 'true') {
                if ($('#reschedule').val() == '1') {
                    $('#anchor_confirm').text('=LNG_CONFIRM_SCHEDULE ');
                } else {
                    $('#anchor_confirm').text('=LNG_CONFIRM_SEND ');
                }
                $("#last_alert").effect('highlight', {
                    color: '#98FB98'
                }, 2000);
                $("#confirm_sending").dialog('open');
            }

            var width = $(".data_table").width();

            var pwidth = $(".paginate").width();
            if (width != pwidth) {
                $(".paginate").width("100%");
            }




            $("#dialog-frame").load(function() {
                $("#dialog-frame").contents().find('.prelink_stats').on('click', function(e) {
                    e.preventDefault();
                    var link = $(this).attr('href');
                    var title = $(this).attr('title');
                    var last_id = $('.dialog_forms').size();
                    var elem = '<div id="dialog-form-' + last_id + '" class="dialog_forms" style="overflow:hidden;display:none;"><iframe id="dialog-frame-' + last_id + '" style="display:none;width:100%;height:auto;overflow:hidden;border : none;"></iframe></div>';
                    $('#secondary').append(elem);
                    $("#dialog-form-" + last_id).dialog({
                        autoOpen: false,
                        height: 100,
                        width: 100,
                        modal: true
                    });
                    openDialogUI("form-" + last_id, "frame-" + last_id, link, 350, 350, title);
                });
            });

        });

        function openDialog(mode) {
          //  var defaultValue = "Enter campaign`s name";
           // $('#linkValue').val(defaultValue);
           
           var dialogId = "";
           var textBoxId = ""; //duplicateDialog

            switch(mode)
            {
                case 1: //duplicate
                {
                    dialogId = "#duplicateDialog";
                    textBoxId = "#dupName"; 
                    break;
                }
                case 2: //add
                {
                    dialogId = "#linkDialog";
                    textBoxId = "#linkValue"; 
                    $("#schedule").prop("checked", false);
                    $("#start_date_and_time").prop('disabled', true);
                    $
                    break;
                }
            }
            $(textBoxId).focus(function() {
                $(textBoxId).select().mouseup(function(e) {
                    e.preventDefault();
                    $(this).unbind("mouseup");
                });
            });
            $(dialogId).dialog('open');

        }

        function deleteRow(id) {
            $.ajax({
                url: "set_settings.asp?name=DeleteRow&value=" + id
            }).done(function(data, textStatus, jqXHR) {
                $("#row" + id).remove();
            });
            

        }

        $(function() {
            $(".delete_button").button({});

            $(".add_alert_button").button({
                icons: {
                    primary: "ui-icon-plusthick"
                }
            });

            $("#linkDialog").dialog({
                autoOpen: false,
                height: 125,
                width: 550,
                modal: true,
                resizable: false
            });
            
            $("#rescheduleDialog").dialog({
                autoOpen: false,
                height: 130,
                width: 200,
                modal: true,
                resizable: false
            });
            $("#duplicateDialog").dialog({
                autoOpen: false,
                height: 110,
                width: 550,
                modal: true,
                resizable: false
            });

        })
        
            function encode_utf8( s )
             {
                 return  encodeURIComponent( s );
            }



        /*    $("#templatesDialog").dialog(
            {
                autoOpen: false,
                height: 110,
                width: 550,
                modal: true,
                resizable: false
            }
            );*/
    

            function showTemplates(id) {

                var dialog = $("#templatesDialog");

                dialog.html("<iframe style='border:0px;height:350px' src='tgroups_templates.asp?group_id=" + id + "'></iframe>");
                    dialog.dialog(
                        {
                            autoOpen: false,
                            height: 400,
                            width: 350,
                            modal: true,
                            resizable: false
                        }
                ).dialog('open');
                
            }
        
        
        
    </script>

    <body style="margin:0" class="body">
     <!--   <form id="form1" runat="server"> -->
            <div id="dialog-form" style="overflow:hidden;display:none;">
                <iframe id='dialog-frame' style="display:none;width:100%;height:auto;overflow:hidden;border : none;">

                </iframe>
            </div>
            <div id="confirm_sending" style="overflow:hidden;text-align:center;">
                <a id="anchor_confirm"></a>
            </div>
            <input type="hidden" id="is_sended" value="=Request(" is_sended ") " />

            <div id="secondary" style="overflow:hidden;display:none;">
            </div>
            <table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
                <tr>
                    <td>
                        <table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
                            <tr>
								<td width="100%" height="31" class="main_table_title">
									<img src="images/menu_icons/templates_20.png" alt="" width="20" height="20" border="0" style="padding-left:7px">
									<a href="MessageTemplatesGroups.aspx" class="header_title" style="position:absolute;top:15px">
                                        <asp:Label ID="tgroupsLabel" runat="server" Text="Label"></asp:Label>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                            
                                <td height="100%" valign="top" class="main_table_body">
                                    <div style="margin:10px" id="parent_for_confirm">
                                        <span class="work_header"><asp:Label ID="tgroupsDescription" runat="server" Text="Label"></asp:Label> </span>

                                        <br /><a class="add_alert_button"  href="AddMessageTemplatesGroups.aspx" style="text-align:right; float:right">
                                            <asp:Label ID="tgoupAddLabel" runat="server" Text="Label" ></asp:Label>
                                        </a>



                                      <!-- <table width="100%">
                                            <tr>
                                                <td align="left"> -->
                                                <%
                                                    if(!noRows)
                                                     Response.Write(MakePages());            
                                                    else
                                                        Response.Write("<br><br><br><center><b>" + vars["LNG_TGROUPS_NOROWS"] + "</b><br><br><br></center>");

                                                %>
                                                
                                                <% if(!noRows) { %>
                                                    <br /><a href='#' class='delete_button' onclick="javascript: return LINKCLICK('message template groups');">
                                                        <asp:Label ID="deleteLabel" runat="server" Text="Label"></asp:Label>
                                                    </a>
                                               <% } %>
                                                    <br />
                                                    <br />
                                                    <br />
                                        <!--          </td>
                                            </tr>
                                        </table> -->
                                
                              <% if (!noRows)
                                 { %>          
                              <form action='delete_objects.asp' name='delete_objs' id='delete_objs' method='post'>
                                    <input type='hidden' name='objType' value='tgroups' />
                                        <table width="100%" height="100%" cellspacing="0" cellpadding="3" class="data_table" runat="server" id="contentTable">
                                            <tr class="data_table_title">
                                                <td class="style5" align="center">
                                                    <input type='checkbox' name='select_all' id='select_all' onclick='javascript: select_all_objects();' />
                                                </td>
                                                <td class="style4" align="center">
                                                <% Response.Write("<a href='MessageTemplatesGroups.aspx?order=" + (order.ToUpper() == "DESC" ? "ASC" : "DESC") + "&collumn=name'"); %>
                                                    <asp:Label ID="tgroupNameCollumn" runat="server" Text="Group name"></asp:Label>
                                                 <% Response.Write("</a>"); %>
                                                </td>
                        
                                                <td class="style1" width="120" align="center">
                                                    <asp:Label ID="tgroupActions" runat="server" Text="Actions"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                           

                                        <img src="images/inv.gif" alt="" width="1" height="100%" border="0">
                                         <input type='hidden' name='back' value='tgroups' />
                                   </form>
                                   <% } %>
                                   
                                   <% if (!noRows) { %>
                                    <br /><a href='#' class='delete_button' onclick="javascript: return LINKCLICK('Template ');">
                                       <asp:Label ID="deleteLabelButtom" runat="server" Text="Label"></asp:Label> </a>
                                       <%
                                       Response.Write(MakePages());
                                      }
                                       %>
                                    </div>
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>
            </table>

            
      <!--  </form> -->
    <div id="templatesDialog">
    </div>
    </body>
    
