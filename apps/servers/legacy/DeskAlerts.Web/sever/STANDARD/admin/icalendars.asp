﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="recurrence_form.asp" -->
<!-- #include file="recurrence_process.asp" -->
<!-- #include file="lang.asp" -->
<%

check_session()
uid = Session("uid")
id = Request("id")
calendar_id = Request("calendar_id")
if uid <> "" then
	userName=""
	userEmail=""
	Set RSUser = Conn.Execute("SELECT name, email FROM users WHERE id="&uid)
	if(Not RSUser.EOF) then
		userName=RSUser("name")&""
		userEmail=RSUser("email")&""
	end if

	title = Request("title")
	location = Request("location")
	description = Request("description")

	if description <> "" then
		description_html = Replace(description, vbCr, "")
		description_html = Replace(description_html, vbLf, "<br/>"&vbNewline)
		description_html = "<p>"& description_html &"</p>"
	else
		description_html = ""
	end if

	from_date = Request("from_date")
	to_date = Request("to_date")

	if from_date <> "" then
		from_date_utc = DateAdd("n", -timezone_db, from_date)
	else
		from_date_utc = ""
	end if

	if to_date <> "" then
		to_date_utc = DateAdd("n", -timezone_db, to_date)
	else
		to_date_utc = ""
	end if

	if Request("form") = "1" then
		if calendar_id <> "" then
			Conn.Execute("UPDATE icalendars SET summary = '"& Replace(title, "'", "''") &"', location = '"& Replace(location, "'", "''") &"', description = '"& Replace(description_html, "'", "''") &"', start_date = '"& Replace(from_date_utc, "'", "''") &"', end_date = '"& Replace(to_date_utc, "'", "''") &"' WHERE calendar_id = '"& calendar_id &"'")
		else
			set RS = Conn.Execute("SELECT newid() as calendar_id")
			calendar_id = RS("calendar_id")
			Conn.Execute("INSERT INTO icalendars (calendar_id, summary, location, description, start_date, end_date, sender_id, organizer, organizer_email) VALUES('"& calendar_id &"', '"& Replace(title, "'", "''") &"', '"& Replace(location, "'", "''") &"', '"& Replace(description_html, "'", "''") &"', '"& Replace(from_date_utc, "'", "''") &"', '"& Replace(to_date_utc, "'", "''") &"', "& uid &", '"& Replace(userName, "'", "''") &"', '"& Replace(userEmail, "'", "''") &"')")
		end if
		if Request("ajax") = "1" then
			response.write shortIdFromGuid(calendar_id)
			response.end
		end if
	end if
end if

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>iCalendar</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/style9.css" rel="stylesheet" type="text/css">
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script language="javascript" type="text/javascript" src="functions.js"></script>
<script language="JavaScript" src="calendar_us.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<link href="css/jquery/jquery_timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css">
<script language="javascript" type="text/javascript" src="jscripts/date.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery_timepicker/jquery-ui-timepicker-addon.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/datepicker_fix.js"></script>
<script language="javascript" type="text/javascript">
	var serverDate;
	var settingsDateFormat = "<%=GetDateFormat()%>";
	var settingsTimeFormat = "<%=GetTimeFormat()%>";

	var responseDbFormat = "dd/MM/yyyy HH:mm:ss";
	var saveDbFormat = "dd/MM/yyyy HH:mm";
	var saveDbDateFormat = "dd/MM/yyyy";
	var saveDbTimeFormat = "HH:mm";
	var uiFormat = getUiDateTimeFormat(settingsDateFormat,settingsTimeFormat);

	$(document).ready(function() {
		serverDate = new Date(getDateFromFormat("<%=now_db%>",responseDbFormat));
		setInterval(function(){serverDate.setSeconds(serverDate.getSeconds()+1);},1000);

		$(".insert_button").button();
		$(".close_button").button();
		
		var fromDate = $.trim($("#start_date_and_time").val());
		var toDate = $.trim($("#end_date_and_time").val());
		
		if (fromDate!="" && isDate(fromDate,responseDbFormat))
		{
			$("#start_date_and_time").val(formatDate(new Date(getDateFromFormat(fromDate,responseDbFormat)),uiFormat));
		}
		
		if (toDate!="" && isDate(toDate,responseDbFormat))
		{
			$("#end_date_and_time").val(formatDate(new Date(getDateFromFormat(toDate,responseDbFormat)),uiFormat));
		}

		initDateTimePicker($(".date_time_param_input"),settingsDateFormat,settingsTimeFormat);
	});

	function check_valid_dates()
	{
		var title = $('#title').val();
		if(!title)
		{
			alert("<%=LNG_YOU_SHOULD_ENTER_ALERT_TITLE %>.");
			return false;
		}
		if(title.length>255) {
			alert("<%=LNG_TITLE_SHOULD_BE_LESS_THEN %>.");
			return false;
		}

		var fromDate = $.trim($("#start_date_and_time").val());
		var toDate = $.trim($("#end_date_and_time").val());
		
		var dateTest = /^\d{1,2}\/\d{1,2}\/\d{4}$/;

		if (!fromDate)
		{
			alert("<%=LNG_PLEASE_ENTER_START_DATE %>!");
			return false;
		}
		if(!toDate)
		{
			alert("<%=LNG_PLEASE_ENTER_END_DATE %>!");
			return false;
		}
		
		if (!isDate(fromDate,uiFormat))
		{
			alert("<%=LNG_PLEASE_ENTER_CORRECT_START_DATE %>!");
			return false;
		}
		else
		{
			if (fromDate!="")
			{
				fromDate = new Date(getDateFromFormat(fromDate,uiFormat));
				$("#from_date").val(formatDate(fromDate,saveDbFormat));
			}
		}
		
		if (!isDate(toDate,uiFormat))
		{
			alert("<%=LNG_PLEASE_ENTER_END_DATE %>!");
			return false;
		}
		else
		{
			if (toDate!="")
			{
				toDate = new Date(getDateFromFormat(toDate,uiFormat))
				$("#to_date").val(formatDate(toDate,saveDbFormat));
			}
		}
		
		if (fromDate < serverDate && toDate < serverDate)
		{
			alert("<%=LNG_DATE_IN_THE_PAST %>")
			return false;
		}
		if (fromDate >= toDate)
		{
			alert("<%=LNG_REC_DATE_ERROR %>")
			return false;
		}

		return true;
	}

	function insertLink()
	{
		var calendar_id = $.ajax({
			type: 'POST',
			url: 'icalendars.asp',
			data: {
				'form': '1',
				'ajax': '1',
				'id': '<%= id %>',
				'calendar_id': '<%= calendar_id %>',
				'title': $('#title').val(),
				'from_date': $('#from_date').val(),
				'to_date': $('#to_date').val(),
				'location': $('#location').val(),
				'description': $('#description').val()
			},
			dataType: "text",
			async: false
		}).responseText;
		if(calendar_id && calendar_id.length < 40)
		{
			var title = 'Add to your calendar';//$('#title').val();
			var file_path = '<%=alerts_folder%>/ical.asp?id='+encodeURIComponent(calendar_id);
			if(window.opener)
				window.opener.fileBrowserSetSrc('<%= Request("id") %>', title, file_path, 0, 0);
		}
		window.close();
	}

</script>
</head>
<% if Request("wh") <> "1" then %>
<body style="margin:0" class="body">
<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	<tr>
	<td valign="top">
	<table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
		<tr>
			<td height=31 class="main_table_title">
				<a href="#" class="header_title">iCalendar</a>
			</td>
		</tr>
		<tr>
		<td style="padding:5px; width:100%" height="100%" valign="top" class="main_table_body">
<% else %>
<body style="margin:0">
<% end if %>
<%
if (uid <> "")  then

%>
<form name="my_form" method="post" action="icalendars.asp" onsubmit="if (check_valid_dates()) {insertLink();}; return false;">
<input type="hidden" name="form" value="1"/>
<input type="hidden" name="id" value="<%= id %>"/>
<input type="hidden" name="calendar_id" value="<%= calendar_id %>"/>
<input type="hidden" name="return_page" value="<%= Request("return_page") %>"/>

<table border="0">
<tr>
	<td align="right" nowrap="nowrap">&nbsp;<%=LNG_TITLE %>: </td>
	<td nowrap="nowrap">
		<input id='title' name='title' type="text" size="35" value="<%= HtmlEncode(title) %>"/>
	</td>
</tr>
<tr>
	<td align="right" nowrap="nowrap">&nbsp;<%=LNG_START_DATE %>: </td>
	<td nowrap="nowrap">
		<input name='start_date_and_time' class="date_time_param_input" id="start_date_and_time" type="text" value="<%= FullDateTime(from_date) %>" size="20"/>
		<input id="from_date" name='from_date' type="hidden"/> 
	</td>
</tr>
<tr>
	<td align="right" nowrap="nowrap">&nbsp;<%=LNG_END_DATE %>: </td>
	<td nowrap="nowrap">
		<input name='end_date_and_time' class="date_time_param_input" id="end_date_and_time" type="text" value="<%= FullDateTime(to_date) %>" size="20"/> 
		<input id="to_date" name='to_date' type="hidden"/> 
	</td>
</tr>
<tr>
	<td align="right" nowrap="nowrap">&nbsp;<%=LNG_LOCATION%>: </td>
	<td nowrap="nowrap">
		<input id='location' name='location' type="text" size="35" value="<%= HtmlEncode(location) %>"/>
	</td>
</tr>
<tr>
	<td align="right" valign="top" nowrap="nowrap">&nbsp;<%=LNG_DESCRIPTION%>: </td>
	<td nowrap="nowrap">
		<textarea id='description' name='description' type="text" rows="5" cols="34"><%= HtmlEncode(description) %></textarea>
	</td>
</tr>
</table>

<table width="100%">
	<tr>
		<td align="right">
			<a class="insert_button" onclick="if (check_valid_dates()) {insertLink();}"><%=LNG_INSERT_INTO_TEXT %></a>
			<a class="close_button" onclick="window.close()"><%=LNG_CLOSE %></a>
		</td>
	</tr>
</table>
</form>
<%
else
	Response.write "You are not authorised to view this page!"
end if
%>
<% if Request("wh") <> "1" then %>
		</td>
	</tr>
</table>
<% end if %>
</body>
</html>
<!-- #include file="db_conn_close.asp" -->
