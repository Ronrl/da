<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%
'check_session()
%>
<!-- #include file="Common.asp" -->
<!-- #include file="header.asp" -->
<!-- #include file="footer.asp" -->
<%
'On Error Resume Next
' index CustomIncludes End
'-------------------------------

'===============================
' Save Page and File Name available into variables
'-------------------------------
Dim sFileName
Dim sTemplateFileName
Dim sPaginateFileName
Dim intSessionUserId
Dim groupAlert
Dim simpleMultiple

sFileName = "statistics_da_alerts_view_details.asp"
if Request("widget")=1 then
	sTemplateFileName = "statistics_da_alerts_view_details_widget.html"
else
	sTemplateFileName = "statistics_da_alerts_view_details.html"
end if
sPaginateFileName = "paginate.html"
sPreviewFileName = "preview.html"
sControlsFileName = "alert_control_buttons.html"
'===============================
  

intSessionUserId = Session("uid")
uid = intSessionUserId
if (true) then


' index Show begin

'===============================
' Display page
'-------------------------------
' Load HTML template for this page
'-------------------------------
LoadTemplate alerts_dir & "\" & templateFolder & "\" & sTemplateFileName, "main"
LoadTemplate alerts_dir & "\" & templateFolder & "\" & sPreviewFileName, "Preview"
LoadTemplate alerts_dir & "\" & templateFolder & "\" & sControlsFileName, "AlertControls"
'-------------------------------
' Load HTML template of Header and Footer
'-------------------------------
LoadTemplate sHeaderFileName, "Header"
LoadTemplate sFooterFileName, "Footer"
'-------------------------------
SetVar "Menu", ""
SetVar "DateForm", ""

SetVar "LNG_TITLE", LNG_TITLE
SetVar "LNG_DATE", LNG_DATE
SetVar "LNG_CREATION_DATE", LNG_CREATION_DATE
SetVar "LNG_EXPORT_TO_CSV", LNG_EXPORT_TO_CSV
SetVar "LNG_TOTAL", LNG_TOTAL
SetVar "LNG_TOTAL_RECIPIENTS", LNG_TOTAL_RECIPIENTS
SetVar "default_lng", default_lng

extendedStatistics = False

set fs1=Server.CreateObject("Scripting.FileSystemObject")
if EXTENDED_STATISTICS=1 then
	extendedStatistics = True
end if

PageName(LNG_ALERT_DETAILS)

'--- rights ---
'set rights = getRights(uid, true, false, true, Array("alerts_val","surveys_val","screensavers_val","wallpapers_val","rss_val"))
'gid = rights("gid")
'gviewall = rights("gviewall")
'gviewlist = rights("gviewlist")
'alerts_arr = rights("alerts_val")
'surveys_arr = rights("surveys_val")
'screensavers_arr = rights("screensavers_val")
'wallpapers_arr = rights("wallpapers_val")
'rss_arr = rights("rss_val")
'--------------
    
	alert_id=Clng(request("id"))
    Set groupCheck = Conn.Execute("SELECT id, part_id FROM multiple_alerts WHERE alert_id=" & alert_id)
    if(groupCheck.EOF) then
        simpleMultiple=0
        groupAlert = 0
    else
        groupAlert = 1
        simpleMultiple = groupCheck("part_id")
    end if

	Set RS = Conn.Execute("SELECT id, alert_text, title, type, aknown, sent_date, type2, sender_id, class, schedule, recurrence, schedule_type, from_date, to_date FROM alerts WHERE id=" & alert_id)
	if(RS.EOF) then
		Set RS = Conn.Execute("SELECT id, alert_text, title, type, aknown, sent_date, type2, sender_id, class, schedule, recurrence, schedule_type, from_date, to_date FROM archive_alerts WHERE id=" & alert_id)
	end if
	if(Not RS.EOF) then
		if not IsNull(RS("sent_date")) then
			strAlertDate=RS("sent_date")
		else
			strAlertDate=""
		end if
		if not IsNull(RS("alert_text")) then
			strAlertPreview=RS("alert_text")
		else
			strAlertPreview=""
		end if
		if not IsNull(RS("title")) then
			strAlertTitle = RS("title")
		else
			strAlertTitle=""
		end if
		classid = RS("class")
		if classid = "1" or classid = "16" then
			showAknow = true
		else
			showAknow = false
		end if
        if groupAlert = 1 then
            showAknow = false
        end if
		if not IsNull(RS("schedule")) then
			schedule=RS("schedule")
		else
			schedule="0"
		end if
		if not IsNull(RS("recurrence")) then
			recurrence=RS("recurrence")
		else
			recurrence="0"
		end if
		if not IsNull(RS("schedule_type")) then
			schedule_type=RS("schedule_type")
		else
			schedule_type="1"
		end if
		if classid = "2" or classid = "8" then
			aknowAsReceived = true
		else
			aknowAsReceived = false
		end if
		if classid = "8" then
			strAlertPreview = "<img src=""../"& strAlertPreview &""" border=""0""/>"
		end if
		
		start_date = RS("from_date")
		end_date = RS("to_date")


'show alerts statistics ---
		Set RSUsers = Conn.Execute("SELECT COUNT(DISTINCT id) as mycnt FROM users WHERE  role = 'U' AND  ( client_version != 'web client' OR client_version is NULL ) AND reg_date <= '" & strAlertDate & "'")
		if(not RSUsers.EOF) then 
			users_cnt=RSUsers("mycnt") 
		end if

		intSentAlertsNum=0
    if groupAlert = 0 then
    		    if(RS("type2")="R") then 
			        SQL = "SELECT COUNT(DISTINCT user_id) as mycnt, 1 as mycnt1 FROM [alerts_sent_stat] WHERE alert_id=" & alert_id
			        SQL = SQL & " UNION ALL "
			        SQL = SQL & "SELECT COUNT(DISTINCT user_id) as mycnt, 2 as mycnt1 FROM [archive_alerts_sent_stat] WHERE alert_id=" & alert_id
			        Set RS7 = Conn.Execute(SQL)
			        do while not RS7.EOF
				        if(Not IsNull(RS7("mycnt"))) then intSentAlertsNum=intSentAlertsNum+clng(RS7("mycnt")) end if
				        RS7.MoveNext
			        loop
			        SQL = "SELECT COUNT(DISTINCT comp_id) as mycnt, 1 as mycnt1 FROM [alerts_sent_comp] WHERE alert_id=" & alert_id
			        SQL = SQL & " UNION ALL "
			        SQL = SQL & "SELECT COUNT(DISTINCT comp_id) as mycnt, 2 as mycnt1 FROM [archive_alerts_sent_comp] WHERE alert_id=" & alert_id
			        Set RS7 = Conn.Execute(SQL)
			        do while not RS7.EOF
				        if(Not IsNull(RS7("mycnt"))) then intSentAlertsNum=intSentAlertsNum+clng(RS7("mycnt")) end if
				        RS7.MoveNext
			        loop
    		     end if
    else

        SQL = "SELECT stat_alert_id FROM multiple_alerts WHERE alert_id =" & alert_id
        Set RS7 = Conn.Execute(SQL)
        alGuid = RS7("stat_alert_id")

         if(RS("type2")="R") then 
			        SQL = "SELECT COUNT(DISTINCT user_id) as mycnt, 1 as mycnt1 FROM [alerts_sent_stat] INNER JOIN multiple_alerts ma ON ma.alert_id = [alerts_sent_stat].alert_id  WHERE ma.stat_alert_id='" & alGuid &"'"
			        SQL = SQL & " UNION ALL "
			        SQL = SQL & "SELECT COUNT(DISTINCT user_id) as mycnt, 2 as mycnt1 FROM [archive_alerts_sent_stat] INNER JOIN multiple_alerts ma ON ma.alert_id = [archive_alerts_sent_stat].alert_id  WHERE ma.stat_alert_id='" & alGuid &"'"
			        Set RS7 = Conn.Execute(SQL)
			        do while not RS7.EOF
				        if(Not IsNull(RS7("mycnt"))) then intSentAlertsNum=intSentAlertsNum+clng(RS7("mycnt")) end if
				        RS7.MoveNext
			        loop			       
    		     end if
    end if
	end if

	'SQL = "SELECT COUNT(DISTINCT alerts_sent_stat.user_id) as mycnt, 1 as mycnt1 FROM [alerts_sent_stat] INNER JOIN alerts_sent_group ON alerts_sent_stat.alert_id=alerts_sent_group.alert_id INNER JOIN users_groups ON alerts_sent_stat.user_id = users_groups.user_id 		  		  WHERE alerts_sent_stat.alert_id=" & alert_id
	'SQL = SQL & " UNION ALL "
	'SQL = SQL & "SELECT COUNT(DISTINCT archive_alerts_sent_stat.user_id) as mycnt, 2 as mycnt1 FROM [archive_alerts_sent_stat] INNER JOIN archive_alerts_sent_group ON archive_alerts_sent_stat.alert_id=archive_alerts_sent_group.alert_id INNER JOIN users_groups ON archive_alerts_sent_stat.user_id = users_groups.user_id 		  		  WHERE archive_alerts_sent_stat.alert_id=" & alert_id
	'Set RS77 = Conn.Execute(SQL)
	'do while not RS77.EOF
	'	if(Not IsNull(RS77("mycnt"))) then intSentAlertsNum=intSentAlertsNum-clng(RS77("mycnt")) end if
	'	RS77.MoveNext
	'loop

	if(RS("type2")="B") then 
		intSentAlertsNum=users_cnt
	end if
	
	SetVar "stop_start_with_child", "0"
	SetVar "scheduled", "0"
	if schedule_type = "1" then
		SetVar "STOP_START_CAPTION", LNG_STOP
		SetVar "stop_start_type", "0"
	else
		SetVar "STOP_START_CAPTION", LNG_START
		SetVar "stop_start_type", "1"
	end if

	SetVar "LNG_DUPLICATE_RESEND", LNG_DUPLICATE_RESEND
	SetVar "LNG_RESCHEDULE", LNG_RESCHEDULE
	SetVar "LNG_RESCHEDULE_ALERT", LNG_RESCHEDULE_ALERT

	select case Clng(classid)
		case 64,128,256
			set RSSurvey = Conn.Execute("SELECT id FROM surveys_main WHERE sender_id=" & alert_id)
			if Not RSSurvey.EOF then
				Response.Redirect "survey_view.asp?id=" & RSSurvey("id")
			end if
			SetVar "STOP_START_TITLE", ""
			SetVar "duplicate_url", ""
			'rights_arr = surveys_arr
		case 1,16, 2048
			'if alerts_arr(1) = "checked" and (schedule = "1" or recurrence = "1") then
				if start_date <> "" then
					if end_date <> "" then
						if year(end_date) <> 1900 then
							if (MyMod(DateDiff("s", start_date, end_date),60)) <> 1 then 'if not lifetime
								SetVar "scheduled", "1"
							end if
						else
							SetVar "scheduled", "1"
						end if
					else
						SetVar "scheduled", "1"
					end if
				end if
			'end if
			if schedule_type = "1" then
				SetVar "STOP_START_TITLE", LNG_STOP_SCHEDULED_ALERT
			else
				SetVar "STOP_START_TITLE", LNG_START_SCHEDULED_ALERT
			end if
			SetVar "duplicate_url", "CreateAlert.aspx?id="& alert_id
			'rights_arr = alerts_arr
		case 2
			if schedule_type = "1" then
				SetVar "STOP_START_TITLE", LNG_STOP_SCHEDULED_SCREENSAVER
			else
				SetVar "STOP_START_TITLE", LNG_START_SCHEDULED_SCREENSAVER
			end if
			SetVar "duplicate_url", "edit_screensavers.asp?id="& alert_id
			'rights_arr = screensavers_arr
		case 4
			if schedule_type = "1" then
				SetVar "STOP_START_TITLE", LNG_STOP_SCHEDULED_RSS
			else
				SetVar "STOP_START_TITLE", LNG_START_SCHEDULED_RSS
			end if
			SetVar "stop_start_with_child", "1"
			SetVar "duplicate_url", "rss_edit.asp?id="& alert_id
			'rights_arr = rss_arr
		case 5
			if schedule_type = "1" then
				SetVar "STOP_START_TITLE", LNG_STOP_SCHEDULED_RSS
			else
				SetVar "STOP_START_TITLE", LNG_START_SCHEDULED_RSS
			end if
			SetVar "duplicate_url", ""
			'rights_arr = rss_arr
		case 8
			if schedule_type = "1" then
				SetVar "STOP_START_TITLE", LNG_STOP_SCHEDULED_WALLPAPER
			else
				SetVar "STOP_START_TITLE", LNG_START_SCHEDULED_WALLPAPER
			end if
			SetVar "duplicate_url", "wallpaper_edit.asp?id="& alert_id
			'rights_arr = wallpapers_arr
		case 999
			if schedule_type = "1" then
				SetVar "STOP_START_TITLE", LNG_STOP_SCHEDULED_ALERT
			else
				SetVar "STOP_START_TITLE", LNG_START_SCHEDULED_ALERT
			end if
			SetVar "duplicate_url", "edit_alert_db.asp?id="& alert_id
			'rights_arr = wallpapers_arr
	end select
	'if rights_arr(0) <> "checked" then
		SetVar "duplicate_url", ""
	'end if

	intReceivedAlertsNum=0
	intClicksNum=0
	rsvp = false

    if groupAlert = 0 then
	    SQL = "SELECT COUNT(DISTINCT user_id) as mycnt, 1 as mycnt1 FROM [alerts_received] WHERE alert_id=" & alert_id
	    SQL = SQL & " UNION ALL "
	    SQL = SQL & "SELECT COUNT(DISTINCT user_id) as mycnt, 2 as mycnt1 FROM [archive_alerts_received] WHERE alert_id=" & alert_id
    else
        SQL = "SELECT stat_alert_id FROM multiple_alerts WHERE alert_id =" & alert_id
        Set RS7 = Conn.Execute(SQL)
        alGuid = RS7("stat_alert_id")
        SQL = "SELECT COUNT(DISTINCT user_id) as mycnt, 1 as mycnt1 FROM [alerts_received] INNER JOIN multiple_alerts ma ON ma.alert_id = [alerts_received].alert_id WHERE ma.stat_alert_id = '" & alGuid & "' "
	    SQL = SQL & " UNION ALL "
	    SQL = SQL & "SELECT COUNT(DISTINCT user_id) as mycnt, 2 as mycnt1 FROM [archive_alerts_received] INNER JOIN multiple_alerts ma ON ma.alert_id = [archive_alerts_received].alert_id WHERE ma.stat_alert_id = '" & alGuid & "' "
    end if
	Set RS7 = Conn.Execute(SQL)
	do while not RS7.EOF
		if(Not IsNull(RS7("mycnt"))) then intReceivedAlertsNum=intReceivedAlertsNum + clng(RS7("mycnt")) end if
		RS7.MoveNext
	loop
	
	set RSsurvey = Conn.Execute("SELECT id FROM surveys_main WHERE closed = 'A' and sender_id = " & alert_id)
	if not RSsurvey.EOF then
		rsvp = true
		SetVar "survey_id", RSsurvey("id")
		SQL = "SELECT COUNT(DISTINCT stats.user_id) as mycnt FROM surveys_answers_stat stats, surveys_answers answers, surveys_questions questions WHERE stats.answer_id=answers.id AND answers.question_id=questions.id AND questions.survey_id=" & RSsurvey("id")
		Set RS7 = Conn.Execute(SQL)
		do while not RS7.EOF
			if(Not IsNull(RS7("mycnt"))) then intVotedNum=intVotedNum+clng(RS7("mycnt")) end if
			RS7.MoveNext
		loop
	else
		SetVar "SurveyInfo", ""
		SQL = "SELECT COUNT(DISTINCT user_id) as mycnt, 1 as mycnt1 FROM [alerts_read] WHERE alert_id=" & alert_id
		SQL = SQL & " UNION ALL "
		SQL = SQL & "SELECT COUNT(DISTINCT user_id) as mycnt, 2 as mycnt1 FROM [archive_alerts_read] WHERE alert_id=" & alert_id
		Set RS7 = Conn.Execute(SQL)	
		do while not RS7.EOF
			if(Not IsNull(RS7("mycnt"))) then intAcknowNum=intAcknowNum+clng(RS7("mycnt")) end if
			RS7.MoveNext
		loop
	end if
	
	if(IsNull(intClicksNum)) then
		intClicksNum=0
	elseif intClicksNum < 0 then
		intClicksNum = 0
	end if
	
	if(IsNull(intReceivedAlertsNum)) then
		intReceivedAlertsNum=0
	elseif intReceivedAlertsNum < 0 then
		intReceivedAlertsNum = 0
	end if
	
	if(IsNull(intAcknowNum)) then
		intAcknowNum=0
	elseif intAcknowNum < 0 then
		intAcknowNum = 0
	end if
	
	if(IsNull(intVotedNum)) then
		intVotedNum=0
	elseif intVotedNum < 0 then
		intVotedNum = 0
	end if

	alertsVal1Color="3B6392"
	alertsVal2Color="A44441"
	alertsVal3Color="9ABA59"
	'alertsVal4Color="9ABA59"

	alertsVal1Name=ToHTML(LNG_RECEIVED)
	if rsvp = false then
		alertsVal2Name=ToHTML(LNG_ACKNOWLEDGED)
	else
		alertsVal2Name=ToHTML(LNG_ANSWERED)
		'alertsVal4Name=ToHTML(LNG_NOT_VOTED)
	end if
	alertsVal3Name=ToHTML(LNG_NOT_RECEIVED)

	extendedPostLink = ""
	alertsVal1PreLink = ""
	alertsVal2PreLink = ""
	alertsVal3PreLink = ""
 

	if (extendedStatistics) then
        if (groupAlert = 1) then
            alertsVal1PreLink = "<a href='statistics_da_alerts_view_multiple_alerts.asp?type=rec&id="& alert_id &"' title=''  class='prelink_stats' >"
        else
    		alertsVal1PreLink = "<a href='statistics_da_alerts_view_users.asp?type=rec&id="& alert_id &"' title=''  class='prelink_stats' >"
        end if
		if not rsvp then
            if (groupAlert = 1) then
                alertsVal2PreLink = "<a href='statistics_da_alerts_view_multiple_alerts.asp?type=ack&id="& alert_id &"'  title=''  class='prelink_stats' >"
            else
                alertsVal2PreLink = "<a href='statistics_da_alerts_view_users.asp?type=ack&id="& alert_id &"'  title=''  class='prelink_stats' >"
            end if			
		else
			alertsVal2PreLink = "<a href='statistics_da_alerts_view_users.asp?type=ans&id="& alert_id &"' title=''   class='prelink_stats' >"
		end if
        if (groupAlert = 1) then
                alertsVal3PreLink = "<a href='statistics_da_alerts_view_multiple_alerts.asp?type=drec&id="& alert_id &"' title=''   class='prelink_stats' >"
            else
                alertsVal3PreLink = "<a href='statistics_da_alerts_view_users.asp?type=drec&id="& alert_id &"' title=''   class='prelink_stats' >"
        end if	
		
	'	alertsVal4PreLink = "<a href='#' onclick=""openDialogUI(""statistics_da_alerts_view_users.asp?type=notvoted&id="& alert_id &""",350,350);"">"
		extendedPostLink = "</a>"
	end  if 
        
    	
	if aknowAsReceived then
		alertsVal1Value = CStr(intAcknowNum)
		alertsVal2Value = "0"
	else
		alertsVal1Value = CStr(intReceivedAlertsNum)
		alertsVal2Value = CStr(intAcknowNum)
	end if

	if aknowAsReceived then
		alertsVal3Value = intSentAlertsNum-intAcknowNum
	elseif(intSentAlertsNum>0) then
		alertsVal3Value = intSentAlertsNum-intReceivedAlertsNum
	else
		alertsVal3Value = 0
	end if
	
	if alertsVal3Value > 0 then
		alertsVal3Value = CStr(alertsVal3Value)
	else
		alertsVal3Value = "0"
	end if
	
	if rsvp then
		alertsVal2Value = CStr(intVotedNum)
	'	alertsVal4Value = CStr(CLng(alertsVal1Value)-intVotedNum)
	end if
	
	totalAlertsValue=intSentAlertsNum

	if(alertsVal1Value<>"0" OR (alertsVal2Value<>"0" and showAknow) OR alertsVal3Value <> "0") then
		alertsData = "["&alertsVal1Value&"]"
		alertsColors = "'#"&alertsVal1Color&"'"
		if showAknow then
			if (alertsData<>"") then alertsData = alertsData&"," end if
			alertsData = alertsData&"["&alertsVal2Value&"]"
			if (alertsColors<>"") then alertsColors = alertsColors&"," end if
			alertsColors = alertsColors&"'#"&alertsVal2Color&"'"
		end if
		if (alertsData<>"") then alertsData = alertsData&"," end if
		alertsData = alertsData&"["&alertsVal3Value&"]"
		if (alertsColors<>"") then alertsColors = alertsColors&"," end if
		alertsColors = alertsColors&"'#"&alertsVal3Color&"'"

		SetVar "alertsData",alertsData
		SetVar "alertsColors",alertsColors
		SetVar "showAlerts","true"
	else
		SetVar "alertsData",""
		SetVar "alertsColors",""
		SetVar "showAlerts","false"
	end if
		
	if classid = "5" then
		PageName(LNG_RSS_DETAILS)
	elseif classid = "2" then
		PageName(LNG_SS_DETAILS)
	elseif classid = "8" then
		PageName(LNG_WP_DETAILS)
	else
		PageName(LNG_ALERT_DETAILS)
	end if
    SetVar "sqlsql",""
    if groupAlert = 1 then
         SQL = "SELECT stat_alert_id FROM multiple_alerts WHERE alert_id =" & alert_id
         Set RS7 = Conn.Execute(SQL)
         alGuid2 = RS7("stat_alert_id")
         SQL = "SELECT COUNT(DISTINCT ar.user_id) as mycnt FROM alerts_read ar INNER JOIN multiple_alerts ma ON ar.alert_id=ma.alert_id WHERE ma.stat_alert_id = '" & alGuid2 & "' "
         SetVar "sqlsql", SQL
         Set RS7 = Conn.Execute(SQL)
         alertsVal2Value = RS7("mycnt")
         Set snlg = Conn.Execute("Select part_id FROM multiple_alerts WHERE alert_id="& alert_id)
         if not snlg.EOF then
             snlgVal = snlg("part_id")
             if snlgVal <> -1 then
                  SQL = "SELECT COUNT(DISTINCT ar.user_id) as mycnt FROM alerts_received ar INNER JOIN multiple_alerts ma ON ar.alert_id=ma.alert_id WHERE ma.stat_alert_id = '" & alGuid2 & "' "
                  SetVar "sqlsql", SQL
                  Set RS7 = Conn.Execute(SQL)
                  alertsVal2Value = RS7("mycnt")
             end if
         end if
    end if
	'if showAknow then
	'	SetVar "alertsVal2Display", ""
	'else
	'	SetVar "alertsVal2Display", " style=""display:none"""
	'end if
	
	'if rsvp then
	'	SetVar "alertsVal2Display",""
		'SetVar "alertsVal4Display",""
	'else
		'SetVar "alertsVal4Display", " style=""display:none"""
	'end if

	SetVar "strAlertTitle", strAlertTitle
	SetVar "strAlertPreview", strAlertPreview
	SetVar "strAlertDate", strAlertDate
	SetVar "alert_id", alert_id
	SetVar "PreviewStr", LNG_PREVIEW
	SetVar "alerts_folder", alerts_folder

	SetVar "alertsVal1Name", alertsVal1Name
	SetVar "alertsVal1Color", alertsVal1Color
	SetVar "alertsVal1Value", alertsVal1PreLink & alertsVal1Value & extendedPostLink

	SetVar "alertsVal2Name", alertsVal2Name
	SetVar "alertsVal2Color", alertsVal2Color
	SetVar "alertsVal2Value", alertsVal2PreLink & alertsVal2Value & extendedPostLink

	SetVar "alertsVal3Name", alertsVal3Name
	SetVar "alertsVal3Color", alertsVal3Color
	SetVar "alertsVal3Value", alertsVal3PreLink & alertsVal3Value & extendedPostLink
	
	'SetVar "alertsVal4Name", alertsVal4Name
	'SetVar "alertsVal4Color", alertsVal4Color
	'SetVar "alertsVal4Value", alertsVal4PreLink & alertsVal4Value & extendedPostLink
	
	'if (RS("type2")="B" OR not extendedStatistics) then 
	'	SetVar "totalAlertsValue",  totalAlertsValue 
	'else
		SetVar "totalAlertsValue", "<A href='view_recipients_list.asp?id=" & alert_id & "' title=''  class='prelink_stats'>" & totalAlertsValue & "</a>"
	'end if
	
	strAlertTitle=replace(strAlertTitle, """", """""")
	strAlertDate=replace(strAlertDate, """", """""")
	csvData=""
	csvData=csvData & """Alert title"",""Sent date"",""Total""," & """" & alertsVal1Name & """" & "," & """" & alertsVal2Name & """" & "," & """" & alertsVal3Name & """" 
	csvData=csvData & vbcrlf & """" & strAlertTitle & """" & "," & """" & strAlertDate & """" & "," & """" & totalAlertsValue & """" & "," & """" & alertsVal1Value & """" & "," & """" & alertsVal2Value & """" & "," & """" & alertsVal3Value & """"
	WriteToFile alerts_dir & "admin\csv\alert" & alert_id & ".csv", "", False
	WriteToFile alerts_dir & "admin\csv\alert" & alert_id & ".csv", csvData, True

	if (extendedStatistics) then
		SetVar "csvUrl", "<A href='csv/alert"&alert_id&".csv' target='_blank'><b>" & LNG_EXPORT_TO_CSV & "</b></a>"
	else
		SetVar "csvUrl", ""
	end if

'-------------------------------
' Step through each form
'-------------------------------
Header_Show
Footer_Show
'-------------------------------
' Process page templates
'-------------------------------
Parse "Header", False
Parse "Footer", False
Parse "main", False
'-------------------------------
' Output the page to the browser
'-------------------------------
Response.write PrintVar("main")

' index Show End

'-------------------------------
' Destroy all object variables
'-------------------------------

' index Close Event begin
' index Close Event End

UnloadTemplate
'===============================

else

Login(Request("intErrorCode"))
'response.write "test111222"

end if

'===============================
' Display Grid Form
'-------------------------------
%><!-- #include file="db_conn_close.asp" -->