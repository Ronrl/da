<!-- #include file="demo.inc" -->
<%

sub login()

'display login form

%>
<link href="css/style9.css" rel="stylesheet" type="text/css">
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>

<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script language="javascript" type="text/javascript">

	$(document).ready(function() {
		$("#login_button").button({
			icons: {
				primary: "ui-icon-key"
			}
		});
<% if DEMO=1 then%>
		$("#demo_login_button").button({
			icons: {
				primary: "ui-icon-key"
			}
		});
<% end if %>
	});

	function demo_login() {
		$("#login").val("demo");
		$("#password").val("demo");
		document.forms[0].submit();
	}

</script>
<script language="javascript">
function checkIE6SP2()
{
	var version = 999; // we assume a sane browser
    if (navigator.appVersion.indexOf("MSIE") != -1)
      version = parseFloat(navigator.appVersion.split("MSIE")[1]);
	if(version == 6){
		if(navigator.appMinorVersion.toLowerCase().indexOf('sp') == -1){
			return false;
		}
	}
	return true;
}
</script>
<table style="position:relative" height="100%" border="0" align="center" cellspacing="0" cellpadding="0">
<tr>
	<td>
		<div style="position:relative" align="center">
			<!--<img src="images/langs/<%=default_lng %>/top_logo.gif" alt="DeskAlerts Control Panel" border="0">-->
			<img src="images/top_logo.gif" alt="DeskAlerts Control Panel" border="0">
		</div>
		<br/>
		<div id="js_error" style="border:1px solid #000000; background-color:#ffffff; color: red; padding: 10px; text-align:center; line-height: 20px">
			<%=LNG_LOGIN_JAVASCRIPT_DESC %>
		</div>
		<div id="ie6_error" style="border:1px solid #000000; background-color:#ffffff; color: red; padding: 10px; text-align:center; line-height: 20px; display: none">
			<%=LNG_LOGIN_SP2_DESC %>
		</div>		
		<table style="position:relative" id="login_form" style="display:none" cellspacing="0" cellpadding="0" align="center" class="main_table login_table">
		<tr>		
			<td width=100% height=31 class="theme-colored-back"><div class="header_title"><%=LNG_LOGIN %></div></td>
		</tr>
		<tr>
			<td bgcolor="#ffffff" style="position:relative">
				<form method="post" action="login.asp" name="web">
					<div style="margin: 20px 10px 10px 10px;text-align:center;position:relative">
					<% if uid_access_denied = 1 then %>
						<%= LNG_ACCESS_DENIED %>
					<% else %>
						<!--<img src="images/Infosign_logo_white.gif" border="0">-->
						<table cellpadding=0 cellspacing=4 border=0>
							<tr>
								<td >
									<label for="login"><%=LNG_LOGIN %>:</label>
								</td>
								<td>
									<input id="login" name="login" type="text" style="width:149px;" onkeydown="try{if(event.keyCode==13) form.submit()}catch(e){}"/>
								</td>
							</tr>
							<tr>
								<td >
									<label for="password"><%=LNG_PASSWORD %>:</label>
								</td>
								<td>
									<input id="password" name="password" type="password" autocomplete="off" style="width:149px;" onkeydown="try{if(event.keyCode==13) form.submit()}catch(e){}"/>
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<span id="wrongLogin" style="display:none;color:red;font-size:11px"><%=LNG_WRONG_LOGIN %></span>
								</td>
							</tr>
							<tr>
								<td colspan="2" style="position:relative;text-align:right">
									<a class="green" id="login_button" style="margin-right:0px" href="javascript: document.forms[0].submit()"><%=LNG_LOGIN %></a>
									<% if DEMO=1 then %><a class="green" id="demo_login_button" style="float:left" href="#" onclick="demo_login()">Demo</a><%end if %>
								</td>
							</tr>
						</table>
					<% end if %>
					</div>
				</form>
			</td>
		</tr>
		</table>
		<script language="javascript">
			
				document.getElementById("js_error").style.display = "none";
			if(!checkIE6SP2()){	
				document.getElementById("ie6_error").style.display = "";
			}
			else{
				document.getElementById("login_form").style.display = "";
			}
		</script>
		<%
		if(Request("debug") <> "1") then
		%>
		<!--
		<br/>
			<table border="1" bordercolor="#000000" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align=center>
				<tr>
				<td width=100% height=31 class="theme-colored-back"><div class="header_title">Notice</div></td>
				</tr>
				<tr>
				<td bgcolor="#db2e11"><div style="margin:10px;color:#ffffff;font-size:11pt;">
					<p><strong>Please note:</strong></p>
					<ol>
					<li>Please note that in order to login to control panel you need <strong>ADMINISTRATOR LOGIN</strong> you can obtain from <a href="mailto:sales@alert-software.com">sales@alert-software.com</a>
					<li><strong>THIS IS ONLY DEMO</strong>. Some of functionality is disabled in demo.
					<li>Purchased version of DeskAlerts is <strong>HOSTED WITHIN YOUR COMPANY</strong> network.
					</ol>		 <img src="inv.gif" alt="" width="1" height="100%" border="0">
				</div>
				</td>
				</tr>
			</table> -->
		<%end if%>
	</td>
	<%
		if DEMO=1 then
	%>
	<td>
		<div style="padding:62px 10px 10px 10px; width:300px; color:#777; font-family:Tahoma">
			<p>
				DeskAlerts is a one-way corporate notification system for sending desktop alerts, e-mails, mobile notifications and other types of messages via single interface.
			</p>
			<p>
				If you are logging in for the first time and don't have your own account yet, click the "Demo" button to log in with the shared demo account.
			</p>
			<p>
				After you log in with the shared account, you can create a private one from Publishers menu. Having your own account will enable you to customize profile settings to personalize your experience.
			</p>
		</div>
	</td>
	<%
		end if
	%>
</tr>

</table>

<% if DEMO=1 then %>
			<div style="position:fixed;bottom:0px;color:#777;padding:5px 5px 5px 90px;width:330px;margin-left:auto;margin-right:auto;">Copyright &copy; DeskAlerts, 2006-2014. All Rights Reserved</center>
		
<% end if %>

<%

end sub

%>
