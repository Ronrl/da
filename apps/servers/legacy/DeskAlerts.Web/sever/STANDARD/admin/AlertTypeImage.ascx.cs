﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DeskAlertsDotNet.Parameters;

namespace DeskAlertsDotNet.Controls
{
    public partial class AlertTypeImage : System.Web.UI.UserControl
    {
        public int Urgent { get; set; }
        public int Ticker { get; set; }
        public int Fullscreen { get; set; }
        public string Type { get; set; }
        public int Class { get; set; }
        public bool Rsvp { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
          //  Image imageType = new Image();

            try
            {
                bool isUrgent = Urgent == 1;
                string imagePath = "";

                if (Ticker == 1)
                {
                    imagePath = isUrgent ? "images/alert_types/urgent_ticker.png" : "images/alert_types/ticker.png";
                }
                else if (Fullscreen == 1)
                {
                    imagePath = isUrgent
                        ? "images/alert_types/urgent_fullscreen.png"
                        : "images/alert_types/fullscreen.png";
                }
                else if (Type != null && Type == "L")
                {
                    imagePath = "images/alert_types/linkedin.png";
                }
                else if (Rsvp)
                {
                    imagePath = isUrgent ? "images/alert_types/urgent_rsvp.png" : "images/alert_types/rsvp.png";
                }
                else
                {

                    imagePath = "images/alert_types/new/" + Class + ".png";
                    string fullPath = Config.Data.GetString("alertsFolder") + "/admin/" + imagePath;
                    // Response.Write(fullPath);

                    if (!System.IO.File.Exists(fullPath))
                    {
                        imagePath = "images/alert_types/alert.png";
                    }

                }

                image.Src = imagePath;
            }
            catch (NullReferenceException ex)
            {
                Pages.DeskAlertsBasePage.Logger.Debug(ex);
            }
        }
    }
}