﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Logout.aspx.cs" Inherits="DeskAlertsDotNet.Pages.Logout" %>
<%@ Import Namespace="DeskAlertsDotNet.Parameters" %>
<%@ Import Namespace="Resources" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    
    <script language="javascript">
        
        if (!window.execCommand("ClearAuthenticationCache")) {
            
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
 	<table height="100%" border="0" align="center" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<div align="center"><img src="images/langs/EN/top_logo.gif" alt="DeskAlerts Control Panel" border="0"></div>
			<br/><br/>
			<table cellspacing="0" cellpadding="0" align="center" class="main_table login_table">
			<tr>
				<td width="100%" height="31" class="theme-colored-back"><div class="header_title"><%=resources.LNG_LOGOUT %></div></td>
			</tr>
			<tr>
				<td bgcolor="#ffffff">
					<div style="margin: 20px 10px 9px 10px;text-align:center">
						<div><%= resources.LNG_TO_LOGOUT_PLEASE_CLOSE_YOUR_BROWSER %></div>
						<div style="width:250px"></div>
					</div>
				</td>
			</tr>
			</table>
		</td>
	</tr>
	</table>   
    </div>
    </form>
</body>
</html>
