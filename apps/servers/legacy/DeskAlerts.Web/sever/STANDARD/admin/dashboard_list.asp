﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<!-- #include file="Common.asp" -->
<%
	
	check_session()
	
	uid = Session("uid")
	if uid <>"" then
	
	if(Request("day") <> "") then 
	day_num = clng(Request("day"))
	else 
	day_num = Day(Now())
	end if
	
	if(Request("tab") <> "") then 
	tab = Request("tab")
	else 
	tab = 0
	end if
	
	if(Request("month") <> "") then 
	month_num = clng(Request("month"))
	else 
	month_num = Month(Now())
	end if
	
	if(Request("year") <> "") then 
	year_num = clng(Request("year"))
	else 
	year_num = Year(Now())
	end if
	
	
	if(Request("sortby") <> "") then 
	sortby = Request("sortby")
	else 
	sortby = "create_date DESC"
	end if	
	
	if(Request("offset_sent") <> "") then 
	offset_sent = clng(Request("offset_sent"))
	else 
	offset_sent = 0
	end if	
	
	if(Request("limit_sent") <> "") then 
	limit_sent = clng(Request("limit_sent"))
	else 
	limit_sent=25
	end if
	
	if(Request("offset_scheduled") <> "") then 
	offset_scheduled = clng(Request("offset_scheduled"))
	else 
	offset_scheduled = 0
	end if	
	
	if(Request("limit_scheduled") <> "") then 
	limit_scheduled = clng(Request("limit_scheduled"))
	else 
	limit_scheduled=25
	end if
	
	if(Request("offset_campaign") <> "") then 
	offset_campaign = clng(Request("offset_campaign"))
	else 
	offset_campaign = 0
	end if	
	
	if(Request("limit_campaign") <> "") then 
	limit_campaign = clng(Request("limit_campaign"))
	else 
	limit_campaign=25
	end if
	
	
	
	Dim currDate, nMonth, nDay, nYear, prevMonth, prevYear, nextMonth, nextYear, from_d, to_d
	Dim lastMonth, lastYear, lastFirstDayPos, lastNumDays, submit
	Dim numDays, monthName(13), count, pos, firstDayPos, eventDay, dayNum, cell(38)
	Dim events(38)
	Dim test1, test2, test3 ' used in leap year calcs
	
	Dim tabs
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>DeskAlerts</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link href="css/dashboard.css" rel="stylesheet" type="text/css">
		<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css" />
		<link href="css/style9.css" rel="stylesheet" type="text/css">
		
		<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
		
		<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
		<script type="text/JavaScript" language="JavaScript" src="jscripts/json2.js"></script>
		
		<script language="javascript" type="text/javascript" src="jscripts/preview.js?v=9.2.1.31"></script>
		
		<script language="javascript" type="text/javascript" src="functions.js"></script>
		
		<script language="javascript" type="text/javascript" src="jscripts/date.js"></script>
		
		<script language="javascript" type="text/javascript">
			var settingsDateFormat = "<%=GetDateFormat()%>";
			var settingsTimeFormat = "<%=GetTimeFormat()%>";
			var responseDbFormat = "dd/MM/yyyy HH:mm:ss";
			var uiFormat = getUiDateTimeFormat(settingsDateFormat, settingsTimeFormat);
			var serverDate;
			var settingsDateFormat = "<%=GetDateFormat()%>";
			var settingsTimeFormat = "<%=GetTimeFormat()%>";
			
			var responseDbFormat = "dd/MM/yyyy HH:mm:ss";
			var uiFormat = getUiDateTimeFormat(settingsDateFormat, settingsTimeFormat);
			
			
			function openDialog(alertID) {
			var url = "<%=jsEncode(alerts_folder)%>preview.asp?id=" + alertID;
			$('#linkValue').val(url);
			$('#linkValue').focus(function() {
            $('#linkValue').select().mouseup(function(e) {
			e.preventDefault();
			$(this).unbind("mouseup");
            });
			});
			$('#linkDialog').dialog('open');
			}
			
			
			function openDialogUI(_form,_frame,_href, _width, _height, _title) {
			$("#dialog-" + _form).dialog('option', 'height', _height);
			$("#dialog-" + _form).dialog('option', 'width', _width);
			$("#dialog-" + _form).dialog('option', 'title', _title);
			$("#dialog-" + _frame).attr("src", _href);
			$("#dialog-" + _frame).css("height", _height);
			$("#dialog-" + _frame).css("display", 'block');
			if ($("#dialog-" + _frame).attr("src") != undefined) {
            $("#dialog-" + _form).dialog('open');
            $("#dialog-" + _form).contents().find('#table_border').css('border', 'none');
			}
			}
			
			
			
			$(document).ready(function() {
			
			$("#dialog-form").dialog({
			autoOpen: false,
			height: 100,
			width: 100,
			modal: true
            });
			
			
	        $('#dialog-frame').on('load', function() {
			$("#dialog-frame").contents().find('.prelink_stats').on('click', function(e) {
			e.preventDefault();
			var link = $(this).attr('href');
			var title = $(this).attr('title');
			var last_id = $('.dialog_forms').size();
			var elem = '<div id="dialog-form-' + last_id + '" class="dialog_forms" style="overflow:hidden;display:none;"><iframe id="dialog-frame-' + last_id + '" style="display:none;width:100%;height:auto;overflow:hidden;border : none;"></iframe></div>';
			$('#secondary').append(elem);
			$("#dialog-form-" + last_id).dialog({
			autoOpen: false,
			height: 100,
			width: 100,
			modal: true
			});
			openDialogUI("form-" + last_id, "frame-" + last_id, link, 350, 350, title);
			});
            });
			});
			
			
			function showAlertPreview(alertId) {
			var data = new Object();
			
			getAlertData(alertId, false, function(alertData) {
            data = JSON.parse(alertData);
			});
			
			var fullscreen = parseInt(data.fullscreen);
			var ticker = parseInt(data.ticker);
			var acknowledgement = data.acknowledgement;
			
			var alert_width = (fullscreen == 1) && !ticker ? $(".main_table_body").width() : (!ticker ? data.alert_width : $(".main_table_body").width());
			var alert_height = (fullscreen == 1) && !ticker ? $(window).height() - document.body.clientHeight + $(".main_table_body").height() : (!ticker ? data.alert_height : "");
			var alert_title = data.alert_title;
			var alert_html = data.alert_html;
			
			var top_template;
			var bottom_template;
			var templateId = data.template_id;
			
			if (templateId >= 0) {
            getTemplateHTML(templateId, "top", false, function(data) {
			top_template = data;
            });
			
            getTemplateHTML(templateId, "bottom", false, function(data) {
			bottom_template = data;
            });
			}
			
			data.top_template = top_template;
			data.bottom_template = bottom_template;
			data.alert_width = alert_width;
			data.alert_height = alert_height;
			data.ticker = ticker;
			data.fullscreen = fullscreen;
			data.acknowledgement = acknowledgement;
			data.alert_title = alert_title;
			data.alert_html = alert_html;
			data.caption_href = data.caption_href;
			
			initAlertPreview(data);
			
			}
			
			$(function() {
			$(".delete_button").button();
			$(".return_button").button().click(function () {
			if (window.parent && window.parent.showLoader) { window.parent.showLoader();}
			})
			
			$(".add_alert_button").button({
            icons: {
			primary: "ui-icon-plusthick"
            }
			});
			serverDate = new Date(getDateFromFormat("<%=now_db%>", responseDbFormat));
			setInterval(function() { serverDate.setSeconds(serverDate.getSeconds() + 1); }, 1000);
			$(".alert_data_bg tbody tr td:last-child").css("border-right", "1px solid #cecece");
			$("#linkDialog").dialog({
            autoOpen: false,
            height: 80,
            width: 550,
            modal: true,
			resizable: false
			});
			
			$(document).ready(function() { 
            var width = $(".data_table").width();
            var pwidth = $(".paginate").width();
            if (width != pwidth) {
			$(".paginate").width("100%");
            }   
            <%if tab=1 then %>
			$('#alerts_table').tabs({  active: 1 }).css("border", "none");
            <%elseif tab = 2 then  %>  
			$('#alerts_table').tabs({  active: 2 }).css("border", "none");
            <% else %>
			$('#alerts_table').tabs({  active: 0 }).css("border", "none");
			
            <%end if %>  
            
			
			});
			})
			
		</script>
		
		<div id="dialog-form" style="overflow: hidden; display: none;">
			<iframe id='dialog-frame' style="width: 100%; height: 100%; overflow: hidden; border: none;
            display: none"></iframe>
		</div>
		<div id="secondary" style="overflow: hidden; display: none;">
		</div>
		<%
			
			
			
			'--- rights ---
			set rights = getRights(uid, true, false, true, Array("alerts_val","surveys_val","screensavers_val","wallpapers_val","lockscreens_val","rss_val"))
			gid = rights("gid")
			gviewall = rights("gviewall")
			gviewlist = rights("gviewlist")
			alerts_arr = rights("alerts_val")
			surveys_arr = rights("surveys_val")
			screensavers_arr = rights("screensavers_val")
			wallpapers_arr = rights("wallpapers_val")
                        lockscreens_arr = rights("lockscreens_val")			
                        rss_arr = rights("rss_val")
			classPolicy = Array()
			ReDim classPolicy(8)
			class_num=0
			if alerts_arr(4)<>"" then 
			classPolicy(class_num)=1
			class_num=class_num+1
			end if
			if surveys_arr(4)<>"" then 
			classPolicy(class_num)=64
			classPolicy(class_num+1)=128
			classPolicy(class_num+2)=256
			class_num=class_num+3
			end if
			if screensavers_arr(4)<>"" then 
			classPolicy(class_num)=2
			class_num=class_num+1
			end if        
			if wallpapers_arr(4)<>"" then 
			classPolicy(class_num)=8
			class_num=class_num+1
			end if
                        if lockscreens_arr(4)<>"" then 
			classPolicy(class_num)=4096
			class_num=class_num+1
			end if
			if rss_arr(4)<>"" then 
			classPolicy(class_num)=5
			class_num=class_num+1
			end if    
			'--------------
			
			if(alerts_arr(2)="") then
			linkclick_str = "not"
			else
			linkclick_str = LNG_ALERTS
			end if
			if day_num<>"" then 
			from_d=day_num&"/"&month_num&"/"&year_num&" 00:00:00"
			to_d=day_num&"/"&month_num&"/"&year_num&" 23:59:59"
			else
			from_d="1/1/1900 00:00:00"
			to_d=Now()
			end if
			'all alerts
			if gviewall <> 1 then
			SQL = "SELECT COUNT(1) as mycnt, 1 as mycnt1 FROM alerts WHERE (type='S' OR type='L') AND create_date >= '" & from_d & "' AND create_date <= '" & to_d & "' AND param_temp_id IS NULL  AND sender_id IN ("& Join(gviewlist,",") &")  AND id NOT IN (SELECT alert_id FROM instant_messages)"
			if class_num>0 then
			SQL = SQL & " AND (class ="& classPolicy(0)
			for k1=1 to class_num-1
			SQL = SQL &" OR class="&classPolicy(k1)		            
			next 
			SQL = SQL & " ) "
		    end if 
			SQL = SQL & " UNION ALL "
			SQL = SQL & "SELECT COUNT(1) as mycnt, 2 as mycnt1 FROM archive_alerts WHERE (type='S' OR type='L') AND create_date >= '" & from_d & "' AND create_date <= '" & to_d & "' AND param_temp_id IS NULL AND sender_id IN ("& Join(gviewlist,",") &") AND id NOT IN (SELECT alert_id FROM instant_messages)"
			if class_num>0 then
			SQL = SQL & " AND (class ="& classPolicy(0)
			for k1=1 to class_num-1
			SQL = SQL &" OR class="&classPolicy(k1)		            
			next 
			SQL = SQL & " ) "
		    end if 
			Set RS = Conn.Execute(SQL)
			
			else
			SQL = "SELECT COUNT(1) as mycnt, 1 as mycnt1 FROM alerts WHERE (type='S' OR type='L') AND create_date >= '" & from_d & "' AND create_date <= '" & to_d & "' AND param_temp_id IS NULL AND id NOT IN (SELECT alert_id FROM instant_messages) "
			if class_num>0 then
			SQL = SQL & " AND (class ="& classPolicy(0)
			for k1=1 to class_num-1
			SQL = SQL &" OR class="&classPolicy(k1)		            
			next 
			SQL = SQL & " ) "
		    end if 
			SQL = SQL & " UNION ALL "
			SQL = SQL & "SELECT COUNT(1) as mycnt, 2 as mycnt1 FROM archive_alerts WHERE (type='S' OR type='L') AND create_date >= '" & from_d & "' AND create_date <= '" & to_d & "' AND param_temp_id IS NULL AND id NOT IN (SELECT alert_id FROM instant_messages)"
		    if class_num>0 then
			SQL = SQL & " AND (class ="& classPolicy(0)
			for k1=1 to class_num-1
			SQL = SQL &" OR class="&classPolicy(k1)		            
			next 
			SQL = SQL & " ) "
		    end if 		
			Set RS = Conn.Execute(SQL)
			
			end if
			
			cnt=0
			cnt_schedule=0
			do while Not RS.EOF
			if(Not IsNull(RS("mycnt"))) then cnt=cnt+clng(RS("mycnt"))
			RS.MoveNext
			loop
			
			RS.Close
			'scheduled alerts
			set RS = Conn.Execute("processAlerts @select_active = 1, @start_date = '"& from_d &"', @end_date = '"& to_d &"'")
			do while not RS.EOF
			if RS("from_date") < RS("to_date") and RS("sent_date") < RS("to_date") and RS("sent_date") < to_d _
	        then
	        if RS("from_date")<>1900 then
			cnt_schedule=cnt_schedule+1
	        end if
			end if  
			RS.MoveNext
			loop
			RS.Close
			j=cnt/limit_sent
			
		%>
	</head>
	<body style="margin: 0" class="body">
		<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%" style="min-width: 600px">
			<tr>
				<td>
					<table height="100%" width="100%" cellspacing="0" cellpadding="0" style="margin: auto"
                    class="main_table">
						<tr>
							<td height="31" class="main_table_title">
								<a href="" class="header_title">
									<%=LNG_ALERTS%>:
									<%if day_num <> 0 then %><%=FormatDateTime(from_d,vbLongDate)%>
									<%else%><%=LNG_ALL%>
								<%end if%></a>
							</td>
						</tr>
						<tr>
							<td height="100%" valign="top" class="main_table_body">
								<div id="alerts_table" style="padding: 0px; border: none;">
									<ul>
										<li><a href="#last_alerts" onclick="">
											<%=LNG_SENT_ALERTS%>
										</a></li>
										<li><a href="#last_scheduled" onclick="">
										<%=LNG_SCHEDULED_ALERTS%></a></li>
										<!--<li><a href="#campaign" onclick="">
										<%=LNG_CAMPAIGN_TITLE%></a></li>-->
									</ul>
									<br />
									
									<form name='my_form' method='post' action='dashboard_list.asp'>
										<div id="last_alerts">
											
											<table width="100%">
												<tr>
													<td align="left">
														<a class="return_button" href="dashboard.asp?month=<%=month_num%>&year=<%=year_num%>">
														<%=LNG_RETURN %></a>
													</td>
													<td align="right">
														<% if(gid = 0 OR (gid = 1 AND alerts_arr(0)<>"")) then %>
														<a  class="add_alert_button" href="edit_alert_db.asp?skin_id=">
														<%=LNG_ADD_ALERT%></a>
														<% end if%>
													</td>
												</tr>
											</table>
											
											<%
												if(cnt>0) then
												LoadTemplate sDateFormFileName, "DateForm"
												page="dashboard_list.asp?day="&day_num&"&month="&month_num&"&year="&year_num&"&offset_scheduled="&offset_scheduled&"&limit_scheduled="&limit_scheduled&"&tab=0&"
												name=LNG_ALERTS 
												
												temp_links = Replace(make_pages(offset_sent, cnt, limit_sent, page, name, sortby),"&offset=","&offset_sent=")
												temp_links = Replace(temp_links,"&limit=","&limit_sent=")
												Response.Write temp_links
												
											%>
											<table width="100%" height="100%" cellspacing="5" cellpadding="5" style="margin: auto"
											class="data_table">
												<tr class="data_table_title">
													<!--td width="40"><input type='checkbox' name='select_all' id='select_all' onclick='javascript: select_all_objects();'></td!-->
													<td width="40" style="width: 40px !important">
														<%= LNG_TYPE%>
													</td>
													<td class="table_title" style="min-width: 250px; white-space: nowrap;">
														<%=LNG_ALERT_TITLE %>
													</td>
													<td style="min-width: 150px;" class="table_title">
														<%=LNG_CREATION_DATE %>
													</td>
													<td width="80" class="table_title">
														<%=LNG_SENT_BY %>
													</td>
													<td width="80" class="table_title">
														<%=LNG_RECIPIENTS %>
													</td>
													<td width="140" style="width: 100px" class="table_title">
														<%=LNG_ACTIONS %>
													</td>
												</tr>
												<%
													
													'show main table
													
													if gviewall <> 1 then
													SQL = "SELECT * FROM (SELECT a.id, a.sender_id, a.schedule, schedule_type, a.type2, recurrence, alert_text, title, a.type, sent_date, a.create_date as create_date, a.from_date, a.to_date, class, urgent, ticker, fullscreen, s.id as is_rsvp FROM alerts a LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id WHERE (a.type='S' OR a.type='L') AND a.create_date >= '" & from_d & "' AND a.create_date <= '" & to_d & "' AND param_temp_id IS NULL AND a.sender_id IN ("& Join(gviewlist,",") &") AND a.id NOT IN (SELECT alert_id FROM instant_messages)"
													if class_num>0 then
													SQL = SQL & " AND (class ="& classPolicy(0)
													for k1=1 to class_num-1
													SQL = SQL &" OR class="&classPolicy(k1)		            
													next 
													SQL = SQL & " ) "
													end if 
													SQL = SQL & " UNION ALL "
													SQL = SQL & "SELECT a.id, a.sender_id, a.schedule, schedule_type, recurrence, a.type2, alert_text, title, a.type, sent_date, a.create_date as create_date, a.from_date, a.to_date, class, urgent, ticker, fullscreen, s.id as is_rsvp  FROM archive_alerts a LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id WHERE (a.type='S' OR a.type='L') AND a.create_date >= '" & from_d & "' AND a.create_date <= '" & to_d & "' AND param_temp_id IS NULL AND a.sender_id IN ("& Join(gviewlist,",") &") AND a.id NOT IN (SELECT alert_id FROM instant_messages)"
													if class_num>0 then
													SQL = SQL & " AND (class ="& classPolicy(0)
													for k1=1 to class_num-1
													SQL = SQL &" OR class="&classPolicy(k1)		            
													next 
													SQL = SQL & " ) "
													end if 
													SQL = SQL & ") res ORDER BY res."&sortby    
													Set RS = Conn.Execute(SQL)
													else
													SQL = "SELECT * FROM (SELECT a.id, a.sender_id, a.schedule, schedule_type, a.type2, recurrence, alert_text, title, a.type, sent_date,a.create_date as create_date, a.from_date, a.to_date, class, urgent, ticker, fullscreen, s.id as is_rsvp FROM alerts a LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id WHERE (a.type='S' OR a.type='L') AND a.create_date >= '" & from_d & "' AND a.create_date <= '" & to_d & "' AND param_temp_id IS NULL AND a.id NOT IN (SELECT alert_id FROM instant_messages) "
													if class_num>0 then
													SQL = SQL & " AND (class ="& classPolicy(0)
													for k1=1 to class_num-1
													SQL = SQL &" OR class="&classPolicy(k1)		            
													next 
													SQL = SQL & " ) "
													end if 
													SQL = SQL & " UNION ALL "
													SQL = SQL & "SELECT a.id, a.schedule, a.sender_id, schedule_type, recurrence, a.type2, alert_text, title, a.type, sent_date,a.create_date as create_date, a.from_date, a.to_date, class, urgent, ticker, fullscreen, s.id as is_rsvp  FROM archive_alerts a LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id WHERE (a.type='S' OR a.type='L') AND a.create_date >= '" & from_d & "' AND a.create_date <= '" & to_d & "' AND param_temp_id IS NULL AND a.id NOT IN (SELECT alert_id FROM instant_messages)"
													if class_num>0 then
													SQL = SQL & " AND (class ="& classPolicy(0)
													for k1=1 to class_num-1
													SQL = SQL &" OR class="&classPolicy(k1)		            
													next 
													SQL = SQL & " ) "
													end if 
													SQL = SQL & ") res ORDER BY res."&sortby    
													Set RS = Conn.Execute(SQL)
													end if
													num=offset_sent
													if(Not RS.EOF) then RS.Move(offset_sent) end if
													Do While Not RS.EOF
													num=num+1
													if((offset_sent+limit_sent) < num) then Exit Do end if
													if(num > offset_sent AND offset_sent+limit_sent >= num) then
													strObjectID=RS("id")
													schedule_type=RS("schedule_type")
													reschedule = 0
													if RS("schedule")="1" or RS("recurrence")="1" then
													start_date = RS("from_date")
													end_date = RS("to_date")
													if start_date <> "" then
													if end_date <> "" then
													if year(end_date) <> 1900 then
													if (MyMod(DateDiff("s", start_date, end_date),60)) <> 1 then 'if not lifetime
													reschedule = 1
													end if
													else
													reschedule = 1
													end if
													else
													reschedule = 1
													end if
													end if
													end if
												%>
												<tr>
													<!--td><input type="checkbox" name="objects" value="<%=strObjectID%>"></td!-->
													<td align="center">
														<% 
															dim isUrgent
															if RS("urgent") = 1 then
															isUrgent = true
															else
															isUrgent = false
															end if
															if RS("class") = "16" then
															if isUrgent = true then
															Response.Write "<img src=""images/alert_types/urgent_unobtrusive.gif"" title=""Urgent Unobtrusive"">"
															else
															Response.Write "<img src=""images/alert_types/unobtrusive.gif"" title=""Unobtrusive"">"
															end if
															elseif RS("class") = "64" OR RS("class") = "128" OR RS("class") = "256" then
															Response.Write "<img src=""images/alert_types/survey.png"" title=""Survey"">"	
															elseif not isNull(RS("is_rsvp")) then
															if isUrgent = true then
															Response.Write "<img src=""images/alert_types/urgent_rsvp.png"" title=""Urgent RSVP"">"
															else
															Response.Write "<img src=""images/alert_types/rsvp.png"" title=""RSVP"">"
															end if
															elseif RS("ticker")=1 then
															if isUrgent = true then
															Response.Write "<img src=""images/alert_types/urgent_ticker.png"" title=""Urgent Ticker"">"
															else
															Response.Write "<img src=""images/alert_types/ticker.png"" title=""Ticker"">"
															end if
															elseif  RS("class")="8" then
															Response.Write "<img src=""images/alert_types/wallpaper.gif"" title=""Wallpaper"">"     
                                                                                                                        elseif  RS("class")="4096" then
															Response.Write "<img src=""images/alert_types/wallpaper.gif"" title=""Lockscreen"">"     																														
                                                                                                                        elseif  RS("class")="2" then
															Response.Write "<img src=""images/alert_types/screensaver.gif"" title=""Screensaver"">"             	
															elseif RS("fullscreen")=1 then
															if isUrgent = true then
															Response.Write "<img src=""images/alert_types/urgent_fullscreen.png"" title=""Urgent Fullscreen"">"
															else
															Response.Write "<img src=""images/alert_types/fullscreen.png"" title=""Fullscreen"">"
															end if
															elseif isUrgent = true then
															Response.Write "<img src=""images/alert_types/urgent_alert.gif"" title=""Urgent Alert"">"
															elseif  RS("type")="L" then
															Response.Write "<img src=""images/alert_types/linkedin.png"" title=""LinkedIn Alert"">"
															elseif  RS("class")="5" then
															Response.Write "<img src=""images/alert_types/rss.gif"" title=""RSS/News Feed"">"        
															else
															Response.Write "<img src=""images/alert_types/alert.png"" title=""Usual Alert"">"
															end if
														%>
													</td>
													<td>
														<% 
															if RS("class")="8" then 
															Response.Write "<a href=""javascript: showWallpaperPreview('" & CStr(RS("id")) & "')"">" & HtmlEncode(RS("title")) & "</a>"
                                                                                                                        elseif RS("class")="4096" then 
															Response.Write "<a href=""javascript: showWallpaperPreview('" & CStr(RS("id")) & "')"">" & HtmlEncode(RS("title")) & "</a>"
															elseif  RS("class")="64" OR RS("class") = "128" OR RS("class") = "256" then 
															Response.Write  HtmlEncode(RS("title"))
															else     
															Response.Write "<a href=""javascript: showAlertPreview('" & CStr(RS("id")) & "')"">" & HtmlEncode(RS("title")) & "</a>"
															end if
														%>
													</td>
													<td>
														
														<script language="javascript">
															document.write(getUiDateTime('<%=RS("create_date")%>'));
														</script>
														
													</td>
													<td>
														<% 
															Set RSSender = Conn.Execute("SELECT name FROM users WHERE id='"&RS("sender_id")&"'")
															if not RSSender.EOF then
															Response.Write "<span title='"&RSSender("name")&"'>"&RSSender("name")&"</span>"
															else
															Response.Write "<span title='DeskAlerts Automatics'>DeskAlerts</span>"
															end if
															RSSender.Close
														%>
													</td>
													<td>
														<%
															Set RSUsers = Conn.Execute("SELECT COUNT(DISTINCT id) as mycnt FROM users WHERE reg_date <= '" & RS("sent_date") & "'")
															if(not RSUsers.EOF) then 
															users_cnt=RSUsers("mycnt") 
															end if
															
															usersCount=0
															if(RS("type2")="R") then 
															SQL = "SELECT COUNT(DISTINCT user_id) as mycnt, 1 as mycnt1 FROM [alerts_sent_stat] WHERE alert_id=" & strObjectID
															SQL = SQL & " UNION ALL "
															SQL = SQL & "SELECT COUNT(DISTINCT user_id) as mycnt, 2 as mycnt1 FROM [archive_alerts_sent_stat] WHERE alert_id=" & strObjectID
															Set RS7 = Conn.Execute(SQL)
															do while not RS7.EOF
															if(Not IsNull(RS7("mycnt"))) then usersCount=usersCount+clng(RS7("mycnt")) end if
															RS7.MoveNext
															loop
															end if
															
															if(RS("type2")="B") then 
															usersCount=users_cnt
															end if
															Response.Write "<A href='#' onclick=""openDialogUI('form','frame','view_recipients_list.asp?id=" & strObjectID & "',555,450,'');"">" & usersCount & "</a>"
														%>
													</td>
													<td align="left" nowrap="nowrap">
														<% if((gid = 0 OR (gid = 1 AND alerts_arr(0)<>"") AND alerts_arr(1)="1") AND RS("class")="1") then %>
														<a href="edit_alert_db.asp?id=<%= RS("id") %>">
															<img src="images/action_icons/duplicate.png" alt="<%=LNG_RESEND_ALERT %>" title="<%=LNG_RESEND_ALERT %>"
														width="16" height="16" border="0" hspace="5"></a>
														<% end if
															'Response.Write "<a href=""javascript: showAlertPreview('" & CStr(RS("id")) & "')""><img src=""images/action_icons/preview.png"" alt="""& LNG_PREVIEW &""" title="""& LNG_PREVIEW &""" width=""16"" height=""16"" border=""0"" hspace=""5""/></a>"
															%><a href="#" onclick="openDialog(<%=RS("id") %>)"><img src="images/action_icons/link.png"
															alt="<%=LNG_DIRECT_LINK%>" title="<%=LNG_DIRECT_LINK%>" width="16" height="16"
															border="0" hspace="6" /></a><%
															Response.Write "<a href='#' onclick=""openDialogUI('form','frame','statistics_da_alerts_view_details.asp?id=" & CStr(RS("id")) & "',600,550,'');""><img src=""images/action_icons/graph.gif"" alt="""& LNG_STATISTICS &""" title="""& LNG_STATISTICS &""" width=""16"" height=""16"" border=""0"" hspace=""5""/></a>"
															
														%>
													</td>
												</tr>
												<%
													end if
													RS.MoveNext
													Loop
													RS.Close
												%>
											</table>
											<%
												else
												Response.Write "<center><b>"&LNG_THERE_ARE_NO_ALERTS&"</b><br><br></center>"
												end if
												
											%>
										</div>
										<div id="last_scheduled" >
											<table width="100%">
												<tr>
													<td align="left">
														<a class="return_button" href="dashboard.asp?month=<%=month_num%>&year=<%=year_num%>">
														<%=LNG_RETURN %></a>
													</td>
													<td align="right">
														<% if(gid = 0 OR (gid = 1 AND alerts_arr(0)<>"")) then %>
														<a  class="add_alert_button" href="edit_alert_db.asp?skin_id=">
														<%=LNG_ADD_ALERT%></a>
														<% end if%>
													</td>
												</tr>
											</table>                         
											<%
												
												
												if(cnt_schedule>0) then 
												page="dashboard_list.asp?day="&day_num&"&month="&month_num&"&year="&year_num&"&offset_sent="&offset_sent&"&limit_sent="&limit_sent&"&tab=1&"
												name=LNG_ALERTS 
												temp_links = Replace(make_pages(offset_scheduled, cnt_schedule, limit_scheduled, page, name, sortby),"&offset=","&offset_scheduled=")
												temp_links = Replace(temp_links,"&limit=","&limit_scheduled=")
												Response.Write temp_links
											tab=0 %>
											<table width="100%" height="100%" cellspacing="5" cellpadding="5" style="margin: auto"
											class="data_table">
												<tr class="data_table_title">
													<!--td width="40">
														<input type='checkbox' name='select_all' id='Checkbox1' onclick='javascript: select_all_objects();'/>
													</td!-->
													<td width="40" style="width: 40px !important">
														<%= LNG_TYPE%>
													</td>
													<td class="table_title" style="min-width: 250px; white-space: nowrap;">
														<%=LNG_ALERT_TITLE %>
													</td>
													<td style="min-width: 150px;" class="table_title">
														<%=LNG_CREATION_DATE %>
													</td>
													<td width="80" class="table_title">
														<%=LNG_SENT_BY %>
													</td>
													<td width="80" class="table_title">
														<%=LNG_RECIPIENTS %>
													</td>
													<td width="140" style="width: 100px" class="table_title">
														<%=LNG_ACTIONS %>
													</td>
												</tr>
												<%
													
													'show main table
													alldaysData = Array()
													ReDim alldaysData(cnt_schedule)
													i = 0                       
													set RS = Conn.Execute("processAlerts @select_active = 1, @start_date = '"& from_d &"', @end_date = '"& to_d &"'")
													do while not RS.EOF
													if RS("from_date") < RS("to_date") and RS("sent_date") < RS("to_date") and RS("sent_date") < to_d _
													then
													if RS("from_date")<>1900 then
													alldaysData(i) = RS("id")
													i=i+1         
													end if
													end if
													RS.MoveNext
													Loop
													RS.Close
													
													max_i=limit_scheduled+offset_scheduled-1
													for i=offset_scheduled to max_i
													if i = cnt_schedule then exit for end if
													if gviewall <> 1 then
													
													SQL = "SELECT * FROM (SELECT a.id, a.sender_id, a.schedule, schedule_type, a.type2, recurrence, alert_text, title, a.type, sent_date, a.create_date as create_date, a.from_date, a.to_date, class, urgent, ticker, fullscreen, s.id as is_rsvp FROM alerts a LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id WHERE (a.type='S' OR a.type='L') AND a.id ="&alldaysData(i) &" AND a.sender_id IN ("& Join(gviewlist,",") &") AND a.id NOT IN (SELECT alert_id FROM instant_messages)"
													if class_num>0 then
													SQL = SQL & " AND (class ="& classPolicy(0)
													for k1=1 to class_num-1
													SQL = SQL &" OR class="&classPolicy(k1)		            
													next 
													SQL = SQL & " ) "
													end if 
													SQL = SQL & " UNION ALL "
													SQL = SQL & "SELECT a.id, a.sender_id, a.schedule, schedule_type, recurrence, a.type2, alert_text, title, a.type, sent_date, a.create_date as create_date, a.from_date, a.to_date, class, urgent, ticker, fullscreen, s.id as is_rsvp  FROM archive_alerts a LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id WHERE (a.type='S' OR a.type='L') AND a.id ="&alldaysData(i) &" AND a.sender_id IN ("& Join(gviewlist,",") &") AND a.id NOT IN (SELECT alert_id FROM instant_messages)"
													if class_num>0 then
													SQL = SQL & " AND (class ="& classPolicy(0)
													for k1=1 to class_num-1
													SQL = SQL &" OR class="&classPolicy(k1)		            
													next 
													SQL = SQL & " ) "
													end if 
													SQL = SQL & ") res ORDER BY res."&sortby    
													Set RS = Conn.Execute(SQL)
													else
													
													SQL = "SELECT * FROM (SELECT a.id, a.sender_id, a.schedule, schedule_type, a.type2, recurrence, alert_text, title, a.type, sent_date, a.create_date as create_date, a.from_date, a.to_date, class, urgent, ticker, fullscreen, s.id as is_rsvp FROM alerts a LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id WHERE (a.type='S' OR a.type='L') AND a.id ="&alldaysData(i) &" AND param_temp_id IS NULL AND a.id NOT IN (SELECT alert_id FROM instant_messages)"
													if class_num>0 then
													SQL = SQL & " AND (class ="& classPolicy(0)
													for k1=1 to class_num-1
													SQL = SQL &" OR class="&classPolicy(k1)		            
													next 
													SQL = SQL & " ) "
													end if 
													SQL = SQL & " UNION ALL "
													SQL = SQL & "SELECT a.id, a.sender_id, a.schedule, schedule_type, recurrence, a.type2, alert_text, title, a.type, sent_date, a.create_date as create_date, a.from_date, a.to_date, class, urgent, ticker, fullscreen, s.id as is_rsvp  FROM archive_alerts a LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id WHERE (a.type='S' OR a.type='L') AND a.id ="&alldaysData(i) &" AND param_temp_id IS NULL AND a.id NOT IN (SELECT alert_id FROM instant_messages)"
													if class_num>0 then
													SQL = SQL & " AND (class ="& classPolicy(0)
													for k1=1 to class_num-1
													SQL = SQL &" OR class="&classPolicy(k1)		            
													next 
													SQL = SQL & " ) "
													end if 
													SQL = SQL & ") res ORDER BY res."&sortby
													Set RS = Conn.Execute(SQL)
													end if
													Do While Not RS.EOF
													
													strObjectID=RS("id")
													schedule_type=RS("schedule_type")
													reschedule = 1
													
												%>
												<tr>
													<!--td>
														<input type="checkbox" name="objects" value="<%=strObjectID%>">
													</td!-->
													<td align="center">
														<% 
															if RS("urgent") = 1 then
															isUrgent = true
															else
															isUrgent = false
															end if
															if RS("class") = "16" then
															if isUrgent = true then
															Response.Write "<img src=""images/alert_types/urgent_unobtrusive.gif"" title=""Urgent Unobtrusive"">"
															else
															Response.Write "<img src=""images/alert_types/unobtrusive.gif"" title=""Unobtrusive"">"
															end if
															elseif RS("class") = "64" OR RS("class") = "128" OR RS("class") = "256" then
															Response.Write "<img src=""images/alert_types/survey.png"" title=""Survey"">"						
															elseif not isNull(RS("is_rsvp")) then
															if isUrgent = true then
															Response.Write "<img src=""images/alert_types/urgent_rsvp.png"" title=""Urgent RSVP"">"
															else
															Response.Write "<img src=""images/alert_types/rsvp.png"" title=""RSVP"">"
															end if
															elseif RS("ticker")=1 then
															if isUrgent = true then
															Response.Write "<img src=""images/alert_types/urgent_ticker.png"" title=""Urgent Ticker"">"
															else
															Response.Write "<img src=""images/alert_types/ticker.png"" title=""Ticker"">"
															end if
															elseif  RS("class")="8" then
															Response.Write "<img src=""images/alert_types/wallpaper.gif"" title=""Wallpaper"">" 
                                                                                                                        elseif  RS("class")="4096" then
															Response.Write "<img src=""images/alert_types/wallpaper.gif"" title=""Lockscreen"">" 
																														
                                                                                                                        elseif  RS("class")="2" then
															Response.Write "<img src=""images/alert_types/screensaver.gif"" title=""Screensaver"">"           	
															elseif RS("fullscreen")=1 then
															if isUrgent = true then
															Response.Write "<img src=""images/alert_types/urgent_fullscreen.png"" title=""Urgent Fullscreen"">"
															else
															Response.Write "<img src=""images/alert_types/fullscreen.png"" title=""Fullscreen"">"
															end if
															elseif isUrgent = true then
															Response.Write "<img src=""images/alert_types/urgent_alert.gif"" title=""Urgent Alert"">"
															elseif  RS("type")="L" then
															Response.Write "<img src=""images/alert_types/linkedin.png"" title=""LinkedIn Alert"">"
															elseif  RS("class")="5" then
															Response.Write "<img src=""images/alert_types/rss.gif"" title=""RSS/News Feed"">"        
															else
															Response.Write "<img src=""images/alert_types/alert.png"" title=""Usual Alert"">"
															end if
														%>
													</td>
													<td>
														<% 
															
															if RS("class")="8" OR RS("class") = "4096" then 
															Response.Write "<a href=""javascript: showWallpaperPreview('" & CStr(RS("id")) & "')"">" & HtmlEncode(RS("title")) & "</a>"
															elseif  RS("class")="64" OR RS("class") = "128" OR RS("class") = "256" then 
															Response.Write  HtmlEncode(RS("title"))
															else     
															Response.Write "<a href=""javascript: showAlertPreview('" & CStr(RS("id")) & "')"">" & HtmlEncode(RS("title")) & "</a>"
															end if
														%>
													</td>
													<td>
														
														<script language="javascript">
															document.write(getUiDateTime('<%=RS("create_date")%>'));
														</script>
														
													</td>
													<td>
														<% 
															Set RSSender = Conn.Execute("SELECT name FROM users WHERE id='"&RS("sender_id")&"'")
															if not RSSender.EOF then
															Response.Write "<span title='"&RSSender("name")&"'>"&RSSender("name")&"</span>"
															else
															Response.Write "<span title='DeskAlerts Automatics'>DeskAlerts</span>"
															end if
															RSSender.Close
														%>
													</td>
													<td>
														<%
															Set RSUsers = Conn.Execute("SELECT COUNT(DISTINCT id) as mycnt FROM users WHERE reg_date <= '" & RS("sent_date") & "'")
															if(not RSUsers.EOF) then 
															users_cnt=RSUsers("mycnt") 
															end if
															
															usersCount=0
															if(RS("type2")="R") then 
															SQL = "SELECT COUNT(DISTINCT user_id) as mycnt, 1 as mycnt1 FROM [alerts_sent_stat] WHERE alert_id=" & strObjectID
															SQL = SQL & " UNION ALL "
															SQL = SQL & "SELECT COUNT(DISTINCT user_id) as mycnt, 2 as mycnt1 FROM [archive_alerts_sent_stat] WHERE alert_id=" & strObjectID
															Set RS7 = Conn.Execute(SQL)
															do while not RS7.EOF
															if(Not IsNull(RS7("mycnt"))) then usersCount=usersCount+clng(RS7("mycnt")) end if
															RS7.MoveNext
															loop
															end if
															
															if(RS("type2")="B") then 
															usersCount=users_cnt
															end if
															Response.Write "<A href='#' onclick=""view_recipients_list.asp?id=" & strObjectID & """,555,450);"">" & usersCount & "</a>"
														%>
													</td>
													<td align="left" nowrap="nowrap">
														<% if(RS("class")="1" AND gid = 0 OR (gid = 1 AND alerts_arr(0)<>"") AND alerts_arr(1)="1") then %>
														<a href="edit_alert_db.asp?id=<%= RS("id") %>">
															<img src="images/action_icons/duplicate.png" alt="<%=LNG_RESEND_ALERT %>" title="<%=LNG_RESEND_ALERT %>"
														width="16" height="16" border="0" hspace="5"></a>
														<% end if
														if(gid = 0 OR (gid = 1 AND alerts_arr(1)<>"")) then %>
														<%
															if schedule_type="1" then
															if  RS("class") = "64" OR RS("class") = "128" OR RS("class") = "256" then
															alert_id = RS("id")
															Set RSAlert = Conn.Execute("SELECT id FROM surveys_main WHERE sender_id=" & alert_id)
															alert_id = RSAlert("id")
															
														%>
														<a href="survey_stop.asp?day=<%=day_num%>&month=<%=month_num %>&year=<%=year_num %>&tab=1&id=<%=alert_id%>&type=0">
															<img src="images/action_icons/stop.png" alt="<%=LNG_STOP_SCHEDULED  %>" title="<%=LNG_STOP_SCHEDULED_ALERT %>"
														width="16" height="16" border="0" hspace="5"></a>
														<%
														else            %>
														<a href="change_schedule_type.asp?day=<%=day_num%>&month=<%=month_num %>&year=<%=year_num %>&tab=1&id=<%=RS("id") %>&type=0">
															<img src="images/action_icons/stop.png" alt="<%=LNG_STOP_SCHEDULED  %>" title="<%=LNG_STOP_SCHEDULED_ALERT %>"
														width="16" height="16" border="0" hspace="5"></a>
														<%  
															end if                              
															end if
															end if
															'Response.Write "<a href=""javascript: showAlertPreview('" & CStr(RS("id")) & "')""><img src=""images/action_icons/preview.png"" alt="""& LNG_PREVIEW &""" title="""& LNG_PREVIEW &""" width=""16"" height=""16"" border=""0"" hspace=""5""/></a>"
															%><a href="#" onclick="openDialog(<%=RS("id") %>)"><img src="images/action_icons/link.png"
															alt="<%=LNG_DIRECT_LINK%>" title="<%=LNG_DIRECT_LINK%>" width="16" height="16"
														border="0" hspace="6" /></a>
														
														<a href='#' onclick="openDialogUI('form','frame','statistics_da_alerts_view_details.asp?id=<%=CStr(RS("id")) %>',600,550,'<%=LNG_STATISTICS %>');"><img src='images/action_icons/graph.gif' alt='<%=LNG_STATISTICS %>' title='<%=LNG_STATISTICS %>' width="16" height="16" border="0" hspace="5"/></a>
														<%
															if reschedule = 1 then
														%>
														<a href="#" onclick="openDialogUI('form','frame','reschedule.asp?id=<%=RS("id") %>',600,550,'<%=LNG_RESCHEDULE_ALERT %>');">
															<img src="images/action_icons/schedule.png" alt="<%=LNG_RESCHEDULE_ALERT %>" title="<%=LNG_RESCHEDULE_ALERT %>"
														border="0" hspace="5"></a>
														<%
															end if
														%>
													</td>
												</tr>
												<%
													'end if
													RS.MoveNext
													Loop
													next
													
													
												%>
											</table>
											<%
												
												else
												
												Response.Write "<center><b>"&LNG_THERE_ARE_NO_SCHEDULED_ALERTS&"</b><br><br></center>"
												
												end if
											%>
											
											<img src="images/inv.gif" alt="" width="1" height="100%" border="0">
										</div>
										
										
										
										
										<div id="campaign" style="padding: 16px;display:none">
											
											<table width="100%">
												<tr>
													<td align="left">
														<a class="return_button" href="dashboard.asp?month=<%=month_num%>&year=<%=year_num%>">
														<%=LNG_RETURN %></a>
													</td>
													
												</tr>
											</table>
											
											<%
												Set campaign = Conn.Execute("SELECT * FROM campaigns WHERE DATEPART(YEAR, start_date) = " & Request("year") & "  AND DATEPART(MONTH, start_date) =  " & Request("month") & " AND DATEPART(DAY, start_date) = " & Request("day") )
												
												name = LNG_CAMPAIGNS
												page="dashboard_list.asp?day="&day_num&"&month="&month_num&"&year="&year_num&"&offset_scheduled="&offset_scheduled&"&limit_scheduled="&limit_scheduled&"&tab=2&"
												temp_links = Replace(make_pages(offset_sent, cnt, limit_sent, page, name, sortby),"&offset=","&offset_campaign=")
												temp_links = Replace(temp_links,"&limit=","&limit_campaign=")
												Response.Write temp_links
												
											%>
											
											<table width="90%" border="0" cellspacing="0" cellpadding="6" height="100%" style="min-width: 600px; margin: auto;" class = "data_table">
												<tr class="data_table_title">
													
													<td class="table_title" style="min-width: 250px; white-space: nowrap;">
														<%=LNG_CAMPAIGN_NAME %>
													</td>
													<td style="min-width: 150px;" class="table_title">
														<%=LNG_CREATION_DATE %>
													</td>
													<td width="140" style="width: 100px" class="table_title">
														<%=LNG_ACTIONS %>
													</td>
												</tr>
												
												<%
													
													do while not campaign.EOF
													
													Response.Write "<tr>"
													Response.Write "<td>"
													Response.Write "<a href='CampaignDetails.aspx?campaignid=" & campaign("id") & "'>" & campaign("name") &"</a>"
													Response.Write "</td>"
													Response.Write "<td>"
													Response.Write  campaign("create_date")
													Response.Write "</td>"
													Response.Write "<td align='center'>"
													Response.Write "<a href='change_schedule_type.asp?return_page=dashboard.asp&campaign=" & campaign("id") &" &type=0'><img id='{0}' src='images/action_icons/stop.png'  title='"& LNG_STOP_CAMPAIGN &  "' width='16' height='16' border='0' hspace=5></a> "
													Response.Write "</td>"
													Response.Write "</tr>"
													campaign.movenext
													loop
													
												%>
											</table>  
										</div> 
									</div>
									
									
									
									<tr>
										<td style="padding: 2px; padding-bottom: 15px; padding-left: 16px;">
											<a class="return_button" style="font-size: 1em;" href="dashboard.asp?month=<%=month_num%>&year=<%=year_num%>">
											<%=LNG_RETURN %></a>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					
					<%
						'end if
						else
						Response.Redirect "index.asp"
						
						end if
					%>
					<div id="linkDialog" title="<%= LNG_DIRECT_LINK %>" style="display: none">
						<input id="linkValue" type="text" readonly="readonly" style="width: 500px" />
					</div>
				</body>
			</html>
			<!-- #include file="db_conn_close.asp" -->
				