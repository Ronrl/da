<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<%
language="British"
Session.LCID = 2057

alertId = CLng(Request("id"))

response.AddHeader "content-type", "text/html; charset=utf-8"

alertData = "{ "

SQL = "SELECT alert_text, title, alert_width, alert_height, aknown, ticker, fullscreen, create_date, caption_id, template_id, class, urgent, self_deletable FROM "
Set RS = Conn.Execute(SQL & "alerts WHERE id="&alertId)
if RS.EOF then
	Set RS = Conn.Execute(SQL & "archive_alerts WHERE id="&alertId)
end if
if not RS.EOF then
	
	alertData = alertData & """alert_html"": """&jsEncode(RS("alert_text"))&""", "
	alertData = alertData & """alert_title"": """&jsEncode(RS("title"))&""", "
	alertData = alertData & """alert_width"": """&jsEncode(RS("alert_width"))&""", "
	alertData = alertData & """alert_height"": """&jsEncode(RS("alert_height"))&""", "
	alertData = alertData & """acknowledgement"": """&jsEncode(RS("aknown"))&""", "
	alertData = alertData & """ticker"": """&jsEncode(RS("ticker"))&""", "
	alertData = alertData & """fullscreen"": """&jsEncode(RS("fullscreen"))&""", "
	alertData = alertData & """create_date"": """&jsEncode(RS("create_date"))&""", "
	alertData = alertData & """classid"": """&jsEncode(RS("class"))&""", "
	alertData = alertData & """urgent"": """&jsEncode(RS("urgent"))&""", "
	alertData = alertData & """self_deletable"": """&jsEncode(RS("self_deletable"))&""", "
	if (RS("caption_id")<>"") then
		Set RSSkin = Conn.Execute("SELECT file_name FROM alert_captions WHERE id='"&RS("caption_id")&"'")
		if not RSSkin.EOF then
			if not IsNull(RSSkin("file_name")) then
				file_name = RSSkin("file_name")
				alertData = alertData & """caption_href"": """&jsEncode(RSSkin("file_name"))&""", "
			else
				'file_name = "skins/"& RS("caption_id") &"/version/alertcaption.html"
				alertData = alertData & """skin_id"": """&RS("caption_id")&""", "
			end if
		end if
		RSSkin.Close
    else
        alertData = alertData & """caption_href"": """ & "" &""", "
	end if
	set RSsurvey = Conn.Execute("SELECT id FROM surveys_main WHERE closed = 'A' and sender_id = " & alertId)
	if not RSsurvey.EOF then
		alertData = alertData & """need_question"": true, "
		set RSquestions = Conn.Execute("SELECT id, question FROM surveys_questions WHERE survey_id = "& RSsurvey("id") &" ORDER BY question_number")
		if not RSquestions.EOF then
			alertData = alertData & """question"": """&jsEncode(RSquestions("question"))&""", "
			set RSanswers = Conn.Execute("SELECT id, answer FROM surveys_answers WHERE question_id = "& RSquestions("id") &" ORDER BY id")
			if not RSanswers.EOF then
				alertData = alertData & """question_option1"": """&jsEncode(RSanswers("answer"))&""", "
				RSanswers.MoveNext
			end if
			if not RSanswers.EOF then
				alertData = alertData & """question_option2"": """&jsEncode(RSanswers("answer"))&""", "
			end if
			RSquestions.MoveNext
		end if
		if not RSquestions.EOF then
			alertData = alertData & """need_second_question"": true, "
			alertData = alertData & """second_question"": """&jsEncode(RSquestions("question"))&""", "
		else
			alertData = alertData & """need_second_question"": false, "
		end if
	else
		alertData = alertData & """need_question"": false, "
	end if
	alertData = alertData & """template_id"": """&jsEncode(RS("template_id"))&""""
end if
    
alertData = alertData & " }"

RS.Close

Response.Write alertData

%>
<!-- #INCLUDE FILE="db_conn_close.asp" -->