﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SettingsSelection.aspx.cs" Inherits="DeskAlertsDotNet.Pages.SettingsSelection" %>
<%@ Import NameSpace="DeskAlertsDotNet.Parameters" %>
<%@ Import NameSpace="Resources" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <link href="css/MetroJs.lt.min.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/MetroJs.lt.min.js"></script>
    <script language="javascript">
    

        $(document).ready(function() {
            $(".live-tile")
                .liveTile({
                    mode: "none",
                    bounce: true
                });

            //$(".settings_dialog_button").click(function () {

            //    settingsOpened = false;
            //    $("#settingsDialog").dialog('close');
            //});
        });
   </script>
</head>
<body>
    <form id="form1" runat="server">
    <div style="margin-top: 20px;height:160px">
        <div id="commonSettings" class="settings_dialog_button live-tile  borderDiv"  runat="server" style="height:190px" >
            <a href="CommonSettings.aspx" target="work_area"><div style="border: 1px solid #666;height:157px"><img style="height: 148px;padding-top: 5px;padding-left: 15px;" class="full-h" draggable="false" src="<%= resources.LNG_PATH_COMMON_SETTINGS%>"/></div></a>
        </div>
        <div id="profileSettings" class="settings_dialog_button live-tile borderDiv" style="margin-top:5px;height:190px"  runat="server" >
            <a href="ProfileSettings.aspx" target="work_area" ><div style="border: 1px solid #666;height:157px"><img style="height: 148px;padding-top: 5px;padding-left: 15px;" class="full-h" draggable="false" src="<%= resources.LNG_PATH_PROFILE_SETTINGS%>" /></div></a>
        </div>
        <div id="systemHealth" class="settings_dialog_button live-tile borderDiv" style="margin-top:5px;height:190px"  runat="server">
            <a href="SystemHealth.aspx" target="work_area" ><div style="border: 1px solid #666;height:157px"><img style="height: 148px;padding-top: 5px;padding-left: 15px;" class="full-h" draggable="false" src="<%= resources.LNG_PATH_SYSTEM_HEALTH%>" /></div></a>
        </div>
    </div>
    </form>
</body>
</html>
