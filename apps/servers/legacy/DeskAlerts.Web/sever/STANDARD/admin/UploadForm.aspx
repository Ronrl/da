<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UploadForm.aspx.cs" Inherits="DeskAlertsDotNet.Pages.UploadForm" %>
<%@ Import NameSpace="Resources" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    	<link href="images/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
	<link href="css/style.css" rel="stylesheet" type="text/css">
	<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
	
	<script language="javascript" type="text/javascript" src="jscripts/date.js"></script>
    <script language="javascript" type="text/javascript" src="functions.js"></script>
	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
    <style>
		input.text { margin-bottom:12px; width:95%; padding: .4em; }
		h1 { font-size: 1.2em; margin: .6em 0; }	
		.ui-dialog .ui-state-error { padding: .3em; }
        .DisabledRecordVideoOnScreensavers {
            visibility: hidden;
        }
	</style>
    <script language="javascript" type="text/javascript">
        var itemsPerPage = 10;
        var curPageNum = 0;
        var pageCount = 0;
        var showItems;
        var insertFileWithSize;
        var uploadedFiles;

        var formSize = '<% =FormSize %>';

        function parseDateTimeFromJson(json) {
            for (var i = 0; i < json.length; i++)
            {
                json[i].dateTimeCreated = new Date(json[i].dateCreated);
                json[i].dateTimeModified = new Date(json[i].dateModified);
            }
        }
        showItems = <% =FilesList%>;
        parseDateTimeFromJson(showItems);
        uploadedFiles = showItems;
        showItems.sort(function (a, b) { return sortFilesArray(a, b, "dateTimeModified", "desc") });

        function getItemById(id) {
            for (var i = 0; i < showItems.length; i++) {
                if (showItems[i].id == id) {
                    return showItems[i];
                }
            }
            return null;
        }

        function removeItemById(collection, id) {
            for (var i = 0; i < collection.length; i++) {
                if (collection[i].id == id) {
                    collection.splice(i, 1);
                }
            }
            return;
        }

        function insertFile(name, file_path, size, width, height) {
            if (window.opener)
                window.opener.fileBrowserSetSrc('<%= Request["id"] %>', name, file_path, size, width, height);
            window.close();
        }

        function selectFile(name, file_path, size, width, height) {
            if (formSize == 1)
               { 
                  $("#dialog-size").dialog("open");
            }
            else { 
                 insertFile(name, file_path, size,width, height);
            } 
        }

        var onConfirmDialog;
        function deleteFile(file_id) {
            var item = getItemById(file_id);
            var dataString = "action=confirm_delete" +
				"&file_path=" + encodeURIComponent(item.relativePath);
            $.ajax({
                type: 'POST',
                url: 'UploadFileAction.aspx',
                data: dataString,
                success: function (data) {

                    onConfirmDialog = function () {
                        var dataString = "action=delete" +
						"&file_path=" + encodeURIComponent(item.relativePath);
                        $.ajax({
                            type: 'POST',
                            url: 'UploadFileAction.aspx',
                            data: dataString,
                            success: function (data) {
                                removeItemById(showItems, file_id);
                                removeItemById(uploadedFiles, file_id);
                                pageCount = Math.ceil(showItems.length / itemsPerPage);
                                if (curPageNum == pageCount) {
                                    curPageNum--;
                                }
                                showPage(showItems, curPageNum);
                                updateValues();

                                if (window.parent.bindLinksToUploadform)
                                    window.parent.bindLinksToUploadform();

                               // $("tr[file_id=" + file_id + "]").remove();
                            },
                            async: true
                        });
                    }
                    $("#confirmation_text").text(data);
                    $("#dialog-confirm").dialog("open");
                },
                async: false
            });
        }

        function sortFilesArray(a, b, field, order) {
            var a = a[field];
            var b = b[field];
            return (((a < b) && order == "asc" || (a > b) && order == "desc") ? -1 : (((a > b) && order == "asc" || (a < b) && order == "desc") ? 1 : 0));
        }



        function showPage(filesArray, num) {
            $("#filesDiv").empty();
            var itemsCount = filesArray.length;
            var startPos = itemsPerPage * num;

            for (var i = 0; i < itemsPerPage; i++) {
                var position = startPos + i;
                if (position == itemsCount) break;
                var item = filesArray[position];
                var $fileItem = $(createFileItem(item));
                $("#filesDiv").append($fileItem);
            }
            $('.files_uploaded').on('click', function (event) {
                event.preventDefault();
                window.parent.create_wrong_item($(this).attr('href'), $(this).text());
            });

        }

        function updateValues() {
            $("#pages_count").text(pageCount);
            $("#page_input").val(curPageNum + 1);
        }

        function showNextPage() {
            if (curPageNum < pageCount - 1) {
                curPageNum++
                showPage(showItems, curPageNum);
                updateValues();
            }
        }

        function showPreviousPage() {
            if (curPageNum > 0) {
                curPageNum--;
                showPage(showItems, curPageNum);
                updateValues();
            }
        }

        function createFileItem(item) {
            var title = '';
            var shortname = item.name;
            if (shortname.length > 30) {
                shortname = shortname.substr(0, 30) + '...';
                title = item.name;
            }
            var edit = '<% =Edit%>';
            var fileItemTr = $("<tr file_id='" + item.id + "' style='width:100%'/>");
            var fileItemNameTd = $("<td/>").appendTo(fileItemTr);

            
                var fileItemNameA = "";
            if (edit == '0')
                fileItemNameA =  $("<a style='text-decoration:underline' href='#'/>" + shortname).appendTo(fileItemNameTd);
           else 
                fileItemNameA = $("<a class='files_uploaded' style='text-decoration:underline' href='<%=FolderLink%>/" +
                        encodeURIComponent(item.name) +
                        "'/>")
                    .appendTo(fileItemNameTd);

                fileItemNameA.text(shortname);
                if (title) fileItemNameA.attr('title', title);
                fileItemNameA.click(function () {
                    selectFile(item.name,/*item.relativePath*/'<%=FolderLink%>/' + item.name, item.size, item.width, item.height);
                });
                fileItemTr.append("<td style='width:60px' align='center'>" +
                    "<a href='<%=FolderLink%>/" + encodeURIComponent(item.name) + "' target='_blank' style='margin:2px'><img src='images/action_icons/preview.png' border='0'/></a>" +
								"<a href='javascript:deleteFile(\"" + item.id + "\")' style='margin:2px' ><img src='images/action_icons/delete.png' border='0'/></a>");
        



            return fileItemTr;
        }

        function searchFiles(searchTerm) {
            if (searchTerm == "") {
                showItems = uploadedFiles;
            }
            else {
                showItems = [];

                for (var i = 0; i < uploadedFiles.length; i++) {
                    if (uploadedFiles[i].name.toLowerCase().indexOf(searchTerm.toLowerCase()) >= 0) {
                        showItems.push(uploadedFiles[i]);
                    }
                }
                showItems.sort(function (a, b) { return sortFilesArray(a, b, "dateCreated", "desc") });
            }
            pageCount = Math.ceil(showItems.length / itemsPerPage);
            curPageNum = 0;
            showPage(showItems, curPageNum);
            updateValues();
        }

        $(function () {

            $("#dialog-confirm").dialog({
                resizable: false,
                height: 140,
                modal: true,
                autoOpen: false,
                buttons: {
                    "<%=resources.LNG_YES %>": function () {
                        onConfirmDialog();
                        $(this).dialog("close");
                    },
                    "<%=resources.LNG_NO %>": function () {
                        $(this).dialog("close");
                    }
                }
            });

            $("#dialog-size").dialog({
                resizable: false,
                height: 165,
                modal: true,
                autoOpen: false,
                title: "<%=resources.LNG_SIZE%>",
                buttons: {
                    "<%=resources.LNG_SAVE%>": function () {
                        insertFileWithSize();
                    }
                }
            });

            $("#upload").click(function() {
                var errorFileTooBig = "File is too big!";
                var file = $("#inputFiles")[0].files[0];
                var fileSize = file.size;
                if (window.File && window.FileReader && window.FileList && window.Blob) {
                    var fileType = file.type;
                    var urlSuffix = new URL(window.location.href).searchParams.get("suffix");
                    var errorMsg = "Upload Error: The file format is unacceptable!";

                    if (urlSuffix == "_images") {
                        switch (fileType) {
                        case 'image/png':
                        case 'image/jpeg':
                        case 'image/jpg':
                        case 'image/gif':
                            break;
                        default:
                            alert(errorMsg);
                            return false;
                        }
                    } else {
                        switch (fileType) {
                        case 'video/avi':
                        case 'video/flv':
                        case 'video/mp4':
                        case 'video/ogg':
                        case 'video/wmv':
                        case 'video/x-ms-wmv':
                            break;
                        default:
                            alert(errorMsg);
                            return false;
                        }
                    }
                }
                if (fileSize > 90000000) {
                    showErrorFileUpload(errorFileTooBig);
                }
                if (!validateFileName(file.name)) {
                    showErrorFileUpload(
                        "<%=resources.PLEASE_DO_NOT_USE_FILES_WITH_NAMES_MORE_THAN_50_CHARACTERS %>" + '<%=HttpUtility.JavaScriptStringEncode(resources.PLEASE_USER_ONLY_FILE_NAME_ALLOWED_CHARACTERS) %>');
                }
            });

            $("#previous_button").button().click(function () { showPreviousPage() });
            $("#next_button").button().click(function () { showNextPage() });
            $("#search_button").button().click(function () { searchFiles($("#search_input").val()) });
            pageCount = Math.ceil(showItems.length / itemsPerPage);
            curPageNum = 0;
            showPage(showItems, curPageNum);
            updateValues();
        });




    </script>
</head>
<body>



	
    <form id="form1" runat="server"  method="post" enctype="multipart/form-data" encoding="utf-8" accept-charset="utf-8">
       <div runat="server" id="uploadDiv">
		<input type="hidden" name="return" value="<%=Request.ServerVariables["SCRIPT_NAME"].Split('/').Last()%>"/>
		<input type="hidden" name="id" value="<%= Request["id"] %>"/>
		<input type="hidden" name="can_select" value="1"/>
         <input type="hidden" runat="server" id="uploadedContentType" name="uploadedContentType"/>
           
	    <table runat="server" Visible="False" id="messageTable" border="0" align="center" cellspacing="0" cellpadding="2" class="data_table">
	    <tr>
		    <td class="text"><% =Request["msg"] %></td>
	    </tr>
	    </table>

	    <table border="0" align="center" cellspacing="0" cellpadding="2">
	    <tr>
		    <td class="text">

		    </td>
	    </tr>
	    </table>
           

		<table  runat="server" id="recordTable" border="0" align="center" cellspacing="0" cellpadding="2">
		<tr>
			<td colspan="2" class="text">
			    Select the file to upload
                <br/>
			    <span style="color: red;"><%=resources.PLEASE_DO_NOT_USE_FILES_WITH_NAMES_MORE_THAN_50_CHARACTERS %></span>
			    <br/>
			    <span style="color: red;"><%=resources.PLEASE_USER_ONLY_FILE_NAME_ALLOWED_CHARACTERS %></span>
			</td>
		</tr>
		<tr>
			<td colspan="2" class="text"><b>File: </b></td>
		</tr>
		<tr>
			<td class="text">
			    <input type="file" name="file1" runat="server" id="inputFiles"/>
			</td>
			<td align="center">
				<input type="submit" value="Upload" id="upload" name="submit">
			</td>
		</tr>
            <!--now this is functionality disabled (record video for web-kam) because we have't ssl sertificate for demo-->
            <!--if you onabled it, you need desided 2 bugs -->
		    <!--first install ssl sertificate -->
		    <!--second update ffmpeg library-->
		<tr class="DisabledRecordVideoOnScreensavers" runat="server" id="recordDiv">
			<td class="text" align="right" style="padding-top:5px">
				To record your own video:
			</td>
			<td align="center" style="padding-top:5px">
				<input type="button" value="Record" onclick="window.open('video_recorder.html','','width=340,height=440,scrollbars=no');">
			</td>
		</tr>

		</table>
	</div>
    <div>
        <br/>
        <table border="0" align="center" width="100%" cellspacing="0" cellpadding="2">
            <tr id="pagination_row" align="right">
	        	<td colspan="2" class="text"><input id="search_input" type="text" style="width:150px; margin-left:5px; margin-right:5px"><a id="search_button"><%=resources.LNG_SEARCH%></a></td>
	        </tr>
            <tr>
		        <td colspan="2">
			        <table style="width:100%" class="data_table">
				        <thead>
					        <tr class="data_table_title">
						        <td class="data_table_title">Filename</td>
						        <td class="data_table_title" style="width:60px">Actions</td>
					        </tr>
				        </thead>
				        <tbody id="filesDiv">
				        </tbody>
			        </table>
		        </td>
	        </tr>
            <tr align="center" id="noFilesDiv" runat="server">
			    <td colspan="2" class="text">No files available</td>
	    	</tr>	
            <tr id="pagination_row" align="right">
			    <td colspan="2" class="text"><a id="previous_button"><%=resources.LNG_BACK%></a><input id="page_input" type="text" style="width:50px; margin-left:5px; margin-right:5px">of <label style="margin-left:5px; margin-right:5px" id="pages_count"></label><a id="next_button"><%=resources.LNG_NEXT%></a></td>
		    </tr>
        </table>
    </div>
     </form>
</body>
    	<div id="dialog-confirm" title="">
		<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 10px 0;"></span><font id="confirmation_text"></font></p>
	</div>


</html>
