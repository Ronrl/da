<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%
check_session()
%>
<!-- #include file="Common.asp" -->
<!-- #include file="header.asp" -->
<!-- #include file="footer.asp" -->
<!-- #include file="libs/FusionCharts.asp" -->
<%
'On Error Resume Next
' index CustomIncludes End
'-------------------------------

'===============================
' Save Page and File Name available into variables
'-------------------------------
Dim sFileName
Dim sTemplateFileName
Dim sPaginateFileName
Dim intSessionUserId

sFileName = "statistics_da_users_view_groups.asp"
sTemplateFileName = "statistics_da_users_view_groups.html"
sPaginateFileName = "paginate.html"
'===============================
  

intSessionUserId = Session("uid")



if (intSessionUserId <> "") then


' index Show begin

'===============================
' Display page
'-------------------------------
' Load HTML template for this page
'-------------------------------
LoadTemplate alerts_dir & "\" & templateFolder & "\" & sTemplateFileName, "main"
SetVar "DListUsersStat", ""
'-------------------------------
' Load HTML template of Header and Footer
'-------------------------------
LoadTemplate sHeaderFileName, "Header"
LoadTemplate sFooterFileName, "Footer"
LoadTemplate sSearchDetailsFormFileName, "SearchForm"
'-------------------------------

pageType=request("type")
user_id=Clng(request("id"))

searchSQL = ""
search = request("search")

SetVar "searchTerms", HtmlEncode(search)
SetVar "page", sFileName&"?"&replace(Request.QueryString, "search=", "search_old=")

SetVar "default_lng", default_lng


if(pageType="groups") then
	PageName(LNG_GROUPS)
	SetVar "strTableTitle", LNG_GROUP_NAME
	SetVar "searchName", "groups"
	if search <> "" then searchSQL = " AND groups.name LIKE N'%"&replace(search, "'", "''")&"%'"
elseif(pageType="ous") then
	PageName(LNG_OUS)
	SetVar "strTableTitle", LNG_OU
	SetVar "searchName", "OU"
	if search <> "" then searchSQL = " AND OU.Name LIKE N'%"&replace(search, "'", "''")&"%'"
end if


if(Request("offset") <> "") then 
	offset = clng(Request("offset"))
else
	offset = 0
end if

if(Request("limit") <> "") then 
	limit = clng(Request("limit"))
else
	limit=50
end if


if(pageType="groups") then
	Set RS = Conn.Execute("SELECT COUNT(group_id) as mycnt FROM groups INNER JOIN users_groups ON groups.id=users_groups.group_id WHERE users_groups.user_id="& user_id & searchSQL)
else                                        
	Set RS = Conn.Execute("SELECT COUNT(Id_OU) as mycnt FROM OU INNER JOIN OU_User ON OU.Id=OU_User.Id_OU WHERE OU_User.Id_user="& user_id & searchSQL)
end if

	if(Not RS.EOF) then
		cnt=RS("mycnt")
	end if
	RS.Close
	j=cnt/limit




if(cnt>0) then
	page=sFileName & "?id="&user_id&"&type="&pageType&"&search="&Server.URLEncode(search)&"&"

	if(pageType="groups") then
		name=LNG_GROUPS
	else
		name=LNG_OUS
	end if
	SetVar "paging", make_pages(offset, cnt, limit, page, name, sortby) 
end if

'show main table

	num=0
if(pageType="groups") then
	Set RS = Conn.Execute("SELECT groups.name as name FROM groups INNER JOIN users_groups ON groups.id=users_groups.group_id WHERE users_groups.user_id="& user_id & searchSQL)
else                                        
	Set RS = Conn.Execute("SELECT OU.Name as name FROM OU INNER JOIN OU_User ON OU.Id=OU_User.Id_OU WHERE OU_User.Id_user="& user_id & searchSQL)
end if

	Do While Not RS.EOF

        			num=num+1
				if(num > offset AND offset+limit >= num) then

				strGroupName = RS("name")

				SetVar "strGroupName", strGroupName
				Parse "DListUsersStat", True

				end if

	RS.MoveNext
	Loop



	RS.Close


'--------------------


'-------------------------------
' Step through each form
'-------------------------------
Header_Show
Footer_Show
'-------------------------------
' Process page templates
'-------------------------------
Parse "Header", False
Parse "Footer", False
Parse "main", False
'-------------------------------
' Output the page to the browser
'-------------------------------
Response.write PrintVar("main")

' index Show End

'-------------------------------
' Destroy all object variables
'-------------------------------

' index Close Event begin
' index Close Event End

UnloadTemplate
'===============================

else

Login(Request("intErrorCode"))

end if

'===============================
' Display Grid Form
'-------------------------------

%><!-- #include file="db_conn_close.asp" -->