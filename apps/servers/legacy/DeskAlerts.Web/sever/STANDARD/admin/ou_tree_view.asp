<% Response.Expires = 0 %>
<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%

check_session()

%>
<!-- #include file="ad.inc" -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Admin</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="style.css" rel="stylesheet" type="text/css">
</head>
<%
' Variables
mifTreeFolder = "mif_tree"
%>

<%
  uid = Session("uid")

if (uid <>"") then

%>



<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	
	
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/mootools.js"></script>	
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Node.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Hover.js"></script>	
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Selection.js"></script>	
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Load.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Draw.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.KeyNav.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Sort.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Transform.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Drag.js"></script>	
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Drag.Element.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Checkbox.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Rename.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Row.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.mootools-patch.js"></script>
	
	<link rel="stylesheet" href="<%Response.write mifTreeFolder%>/style.css" type="text/css" />
	<script type="text/javascript">
		var tree;
		
		function getCheckedOus ()
		{			
			if( tree && tree != 'undefined' )
			{
				nodes = new Array();									
				tree.root.recursive
				(
					function()
					{
						if( this.state.checked == "checked" )
						{						
							temp = new Object();
							nodes.push(temp);
							nodes[nodes.length-1].id = this.property.id;
							nodes[nodes.length-1].type = this.property.type;							
							
						}
					}
				);				
				return nodes;
			}
			return null;
		}
		
		function ddd()
		{
			res = getCheckedOus();
			if( res )
			{
				for(i=0;i<res.length;i++)
				{
					alert(res[i].id.toString()+" "+res[i].type.toString());
				}
			}
		}
		
		window.addEvent('domready',function()
		{		
				tree = new Mif.Tree({
					container: $('tree_container'),
					forest: false,
					initialize: function(){
						this.initCheckbox('deps');
						new Mif.Tree.KeyNav(this);
					},					
					types: {
						organization:{
							openIcon: 'mif-tree-organization-icon',
							closeIcon: 'mif-tree-organization-icon'
						},
						domain:{
							openIcon: 'mif-tree-domain-icon',
							closeIcon: 'mif-tree-domain-icon'
						},
						loader:{
							openIcon: 'mif-tree-loader-open-icon',
							closeIcon: 'mif-tree-loader-close-icon',
							dropDenied: ['inside','after']
						},
						ou:{
							openIcon: 'mif-tree-ou-icon',
							closeIcon: 'mif-tree-ou-icon'							
						}	
					},
					dfltType:'organization'
					
				});
				
				tree.initSortable();
				try
				{
				/*
					var sLocation = window.location;
					thePath = unescape(sLocation)					
					thePath=thePath.substr(0,thePath.lastIndexOf('/')+1);
					alert(thePath+"ou_get_tree.asp");
					loadXMLDoc(thePath+"get_ou_tree.asp");*/
					//loadXMLDoc("")
					
					tree.load({
						url: 'ou_get_tree.asp'
					//	url: '<%Response.write mifTreeFolder%>/mootools/forest.json'
					});
					
				}
				catch(e)
				{
					alert('Fail');
				}
				
				tree.loadOptions=function(node){
//					alert(node);
					return {
						url: 'ou_get_child.asp?id='+node.id
//						url: 'child.asp?id='+node.id


					};
				}

//				Mif.Tree.Load.children(children, parent, tree);
		});
	</script>		
</head>
<body style="margin:0" class="body">
<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	<tr>
	<td>
	<table width="100%" height="100%" border="1" bordercolor="#000000" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
		<tr>
		<td width=100% height=31 class="theme-colored-back"><a href="ou_tree_view.asp" class="header_title">Organizational Units</a></td>
		</tr>
		<tr>
		<td bgcolor="#ffffff" height="100%">
		<div style="margin:10px;">
		<br><div align="right"></div>

 <!--	<div id="tree_view_page_header"></div>	-->
	<div id="tree_container"></div>
<!--	<div id="tree_view_page_footer"></div>-->

		</td>
		</tr>
	</table>
	</td>
	</tr>
</table>
<%
else

  Response.Redirect "index.asp"

end if
%> 
</body>
</html>
<!-- #include file="db_conn_close.asp" -->