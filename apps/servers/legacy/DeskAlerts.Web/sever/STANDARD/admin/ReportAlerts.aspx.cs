using Castle.MicroKernel.Lifestyle;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.Server.Utils;
using DeskAlerts.Server.Utils.Helpers;
using DeskAlertsDotNet.Application;
using DeskAlertsDotNet.Controls;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Parameters;
using DeskAlertsDotNet.Utils.Consts;
using Resources;
using System;
using System.Globalization;
using System.Linq;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace DeskAlertsDotNet.Pages
{
    public partial class ReportAlerts : DeskAlertsBaseListPage
    {
        private const string ColumnCreateDate = "create_date";
        private const string ColumnSentDate = "sent_date";
        private const string ColumnTitle = "title";
        private const string ColumnSender = "sender_id";
        private string _rangeValue;
        private string _selectRadio;
        private string _startDate;
        private string _endDate;
        private bool _isAnonymousSurvey;

        private readonly IAlertReceivedRepository _alertReceivedRepository;

        public ReportAlerts()
        {
            using (Global.Container.BeginScope())
            {
                _alertReceivedRepository = Global.Container.Resolve<IAlertReceivedRepository>();
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            _startDate = Request.GetPostOrGetIfNotFound("fromDate");
            _endDate = Request.GetPostOrGetIfNotFound("toDate");
            _selectRadio = Request.GetPostOrGetIfNotFound("select_date_radio", "1");
            _rangeValue = Request.GetPostOrGetIfNotFound("range", "6");

            TypeColumn.Text = resources.LNG_TYPE;
            RecipientsColumn.Text = resources.LNG_RECIPIENTS;
            OpenRationColumn.Text = resources.LNG_OPEN_RATIO;

            var alertClassString = Request["class"];
            var alertClassArray = alertClassString.Split(',');

            if (alertClassArray.Select(alertClass => (AlertType)Enum.Parse(typeof(AlertType), alertClass))
                .Any(alertType => alertType == AlertType.SimpleSurvey || alertType == AlertType.SurveyPoll || alertType == AlertType.SurveyQuiz))
            {
                _isAnonymousSurvey = true;

                var tableCell = new TableCell()
                {
                    Text = resources.LNG_ANONYMOUS_SURVEY
                };

                contentTable_rows.Controls.Add(tableCell);
            }

            showButton.Text = resources.LNG_SHOW;
            if (!IsPostBack)
            {
                range.Items.Add(new ListItem(resources.LNG_TODAY, "1"));
                range.Items.Add(new ListItem(resources.LNG_YESTERDAY, "2"));
                range.Items.Add(new ListItem(resources.LNG_LAST_DAYS, "3"));
                range.Items.Add(new ListItem(resources.LNG_THIS_MONTH, "4"));
                range.Items.Add(new ListItem(resources.LNG_LAST_MONTH, "5"));
                range.Items.Add(new ListItem(resources.LNG_ALL_TIME, "6"));

                rangeRadio.Checked = false;
                manualDateRadio.Checked = false;
                if (_selectRadio == "1")
                {

                    rangeRadio.Checked = true;
                    manualDateRadio.Checked = false;
                    range.Attributes.Remove("disabled");
                    foreach (ListItem item in range.Items)
                    {
                        if (item.Value == _rangeValue)
                            item.Selected = true;
                    }

                    toDate.Value = string.Empty;
                    toDate.Attributes.Add("disabled", "true");
                    fromDate.Attributes.Add("disabled", "true");
                    fromDate.Value = string.Empty;
                }
                else if (_selectRadio == "2")
                {
                    rangeRadio.Checked = false;
                    manualDateRadio.Checked = true;
                    range.Attributes.Add("disabled", "true");
                    toDate.Attributes.Remove("disabled");
                    fromDate.Attributes.Remove("disabled");
                    fromDate.Value = _startDate;
                    toDate.Value = _endDate;
                }
                else
                {
                    rangeRadio.Checked = true;
                }
            }
            else //restore filters after paging options changed
            {
                if (!string.IsNullOrEmpty(_rangeValue))
                {
                    string enabledRadio = _selectRadio;

                    if (enabledRadio == "2")
                    {
                        toDate.Attributes.Remove("disabled");
                        fromDate.Attributes.Remove("disabled");
                        range.Attributes.Add("disabled", "true");

                        if (string.IsNullOrEmpty(fromDate.Value) || string.IsNullOrEmpty(toDate.Value))
                        {
                            Response.ShowJavaScriptAlert(resources.LNG_ENTER_THE_CORRECT_DATE);
                            return;
                        }
                        else
                        {
                            toDate.Value = _endDate;
                            fromDate.Value = _startDate;
                        }
                    }
                    else
                    {
                        toDate.Attributes.Add("disabled", "true");
                        fromDate.Attributes.Add("disabled", "true");
                        range.Attributes.Remove("disabled");
                    }
                }
            }

            base.OnLoad(e);

            TitleColumn.Text = GetSortLink(resources.LNG_TITLE, ColumnTitle, Request["sortBy"]);
            CreationDateColumn.Text = GetSortLink(resources.LNG_CREATION_DATE, ColumnCreateDate, Request["sortBy"]);
            SentDateColumn.Text = GetSortLink(resources.LNG_SENT_DATE, ColumnSentDate, Request["sortBy"]);
            SenderColumn.Text = GetSortLink(resources.LNG_SENT_BY, ColumnSender, Request["sortBy"]);

            int cnt = this.Content.Count < (Offset + Limit) ? this.Content.Count : Offset + Limit;

            if (string.IsNullOrEmpty(Request["needType"]))
            {
                TypeColumn.Visible = false;
            }

            for (int i = Offset; i < cnt; i++)
            {
                var contentId = Content.GetDouble(i, "id");

                TableRow tableRow = new TableRow();

                if (!string.IsNullOrEmpty(Request["needType"]))
                {
                    AlertTypeImage atl = (AlertTypeImage)LoadControl("AlertTypeImage.ascx");

                    atl.Class = this.Content.GetInt(i, "class");
                    atl.Urgent = this.Content.GetInt(i, "urgent");
                    atl.Fullscreen = this.Content.GetInt(i, "fullscreen");
                    atl.Ticker = this.Content.GetInt(i, "ticker");
                    atl.Type = this.Content.GetString(i, "type");
                    atl.Rsvp = !this.Content.IsNull(i, "is_rsvp");


                    TableCell typeContentCell = new TableCell();
                    typeContentCell.HorizontalAlign = HorizontalAlign.Center;

                    typeContentCell.Controls.Add(atl);
                    tableRow.Cells.Add(typeContentCell);
                }

                var title = DeskAlertsUtils.ExtractTextFromHtml(Content.GetString(i, "title"));
                var alertStatLink =
                    $"window.open('AlertStatisticDetails.aspx?id={contentId}','', 'status=0, toolbar=0, width=555, height=850, scrollbars=1')";

                if (!this.Content.IsNull(i, "is_rsvp") || this.Content.GetString(i, "class") == "64" || this.Content.GetString(i, "class") == "128" || this.Content.GetString(i, "class") == "256")
                {
                    var surveyId = dbMgr.GetScalarByQuery<int>($"SELECT id FROM surveys_main WHERE sender_id = {contentId}");

                    alertStatLink =
                        $"window.open('WidgetSurveysDetails.aspx?id={surveyId}&sendAlertId={contentId}','', 'status=0, toolbar=0, width=555, height=450, scrollbars=1')";
                }

                if (Config.Data.HasModule(Modules.MULTIPLE))
                {
                    var isMultipleAlert = (int)dbMgr.GetScalarByQuery($"SELECT COUNT(1) FROM multiple_alerts WHERE alert_id = {contentId} AND part_id <> -1") > 0;

                    if (isMultipleAlert)
                    {
                        alertStatLink =
                            $"window.open('statistics_da_alerts_view_details.asp?id={contentId}', '', 'status=0, toolbar=0, width=350, height=500, scrollbars=1')";
                    }
                }

                var titleContentCell = new TableCell
                {
                    Text = $"<a href='#' onclick=\"{alertStatLink}\" >{title}</a>"
                };

                var createDateContentCell = new TableCell
                {
                    Text = DateTimeConverter.ToUiDateTime(Content.GetDateTime(i, "create_date"))
                };

                var sentDateContentCell = new TableCell
                {
                    Text = DateTimeConverter.ToUiDateTime(Content.GetDateTime(i, "sent_date"))
                };

                var senderDateContentCell = new TableCell
                {
                    HorizontalAlign = HorizontalAlign.Center,
                    Text = string.IsNullOrEmpty(Content.GetString(i, "uname")) ? resources.SENDER_WAS_DELETED : Content.GetString(i, "uname")
                };

                int totalUsersCount = 0;
                TableCell recipientsContentCell = null;
                if (this.Content.GetString(i, "type2") == "R")
                {
                    string recipientsQuery =
                        "SELECT name FROM users INNER JOIN alerts_sent ON users.id=alerts_sent.user_id WHERE alerts_sent.alert_id=" +
                        this.Content.GetInt(i, "id");

                    string cellText = "";
                    DataSet recipientSet = dbMgr.GetDataByQuery(recipientsQuery);

                    if (!recipientSet.IsEmpty)
                        cellText = recipientSet.GetString(0, "name");

                    if (recipientSet.Count > 1)
                    {
                        cellText += ",...";
                    }

                    var link =$"window.open('AlertRecipientsList.aspx?id={contentId}','', 'status=0, toolbar=0, width=555, height=450, scrollbars=1')";

                    string fullLink = string.Format("<a href='#' onclick=\"{0}\" >{1}</a>", link, cellText);
                    recipientsContentCell = new TableCell { Text = fullLink };
                    totalUsersCount = recipientSet.Count;
                }
                else if (this.Content.GetString(i, "type2") == "B")
                {
                    var link = $"window.open('AlertRecipientsList.aspx?id={contentId}','', 'status=0, toolbar=0, width=555, height=450, scrollbars=1')";
                    string fullLink = string.Format("<a href='#' onclick=\"{0}\" >{1}</a>", link, resources.LNG_BROADCAST);

                    recipientsContentCell = new TableCell
                    {
                        Text = fullLink
                    };

                    totalUsersCount =
                        dbMgr.GetScalarQuery<int>("SELECT COUNT(id) as mycnt FROM users as u WHERE role = 'U'");
                }
                else if (Content.GetString(i, "type2") == "I")
                {
                    var link = $"window.open('AlertRecipientsList.aspx?id={contentId}','', 'status=0, toolbar=0, width=555, height=450, scrollbars=1')";
                    var fullLink = $"<a href='#' onclick=\"{link}\" >{resources.LNG_IP_GROUPS}</a>";

                    recipientsContentCell = new TableCell
                    {
                        Text = fullLink
                    };

                    totalUsersCount = _alertReceivedRepository.GetAlertsReceivedForContent((long)contentId).Count();
                }

                var recievedCnt = _alertReceivedRepository.GetReceivedForContent((long)contentId).Count();

                // Response.WriteLine("RECEIVED " + recievedCnt);
                int openRatio = 0;

                if (totalUsersCount > 0)
                {
                    double ratio = 0;
                    ratio = ((double)recievedCnt / totalUsersCount) * 100;
                    openRatio = (int)Math.Ceiling(ratio);
                }

                var openRatioContentCell = new TableCell { Text = openRatio + "%" };

                tableRow.Cells.Add(titleContentCell);
                tableRow.Cells.Add(createDateContentCell);
                tableRow.Cells.Add(sentDateContentCell);
                tableRow.Cells.Add(senderDateContentCell);
                tableRow.Cells.Add(recipientsContentCell);
                tableRow.Cells.Add(openRatioContentCell);
                if (_isAnonymousSurvey)
                {
                    tableRow.Cells.Add(new TableCell
                    {
                        Text = this.Content.GetString(i, "anonymous_survey") == "1" ?
                    resources.LNG_YES :
                    resources.LNG_NO
                    });
                }

                contentTable.Rows.Add(tableRow);
            }
        }


        protected override string PageParams
        {
            get
            {
                string parameters = string.Empty;

                if (!string.IsNullOrEmpty(_selectRadio))
                    parameters += ("select_date_radio=" + _selectRadio + "&");

                if (!string.IsNullOrEmpty(_rangeValue))
                    parameters += ("range=" + _rangeValue + "&");

                if (!string.IsNullOrEmpty(_startDate))
                    parameters += ("fromDate=" + _startDate + "&");

                if (!string.IsNullOrEmpty(_endDate))
                    parameters += ("toDate=" + _endDate + "&");

                if (!string.IsNullOrEmpty(Request["class"]))
                    parameters += ("class=" + Request["class"] + "&");

                if (!string.IsNullOrEmpty(Request["needType"]))
                    parameters += ("needType=" + Request["needType"] + "&");

                return parameters;
            }
        }

        #region DataProperties
        protected override string DataTable
        {
            get
            {

                //return your table name from this property
                return "alerts";
            }
        }

        protected override string[] Collumns
        {
            get
            {
                return new[] { "class", ColumnTitle, "from_date", "to_date", ColumnSentDate, ColumnCreateDate, "type2" };
            }
        }

        protected override string DefaultOrderCollumn
        {
            //return collumn for default order
            get
            {
                return "id";

            }
        }
        protected override string CustomCountQuery
        {
            get
            {
                string query = "";
                string startDate = "";
                string endDate = "";

                GetDateRange(out startDate, out endDate);

                if (!curUser.HasPrivilege && !curUser.Policy.CanViewAll)
                {
                    query +=
                       "SELECT  COUNT(1) as cnt  FROM alerts LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = alerts.id LEFT JOIN users ON users.id = alerts.sender_id WHERE alerts.sent_date >= '" +
                       startDate + "' AND alerts.sent_date <= '" + endDate +
                       "' AND alerts.type='S' AND alerts.sender_id IN (" + string.Join(",", curUser.Policy.SendersViewList.ToArray())
                       + ") AND alerts.id NOT IN (SELECT alert_id FROM instant_messages)";


                }
                else
                {
                    query +=
                         "SELECT COUNT(1) as cnt FROM alerts LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = alerts.id LEFT JOIN users ON users.id = alerts.sender_id WHERE alerts.sent_date >= '" +
                         startDate + "' AND alerts.sent_date <= '" + endDate +
                         "' AND alerts.type='S' AND alerts.id NOT IN (SELECT alert_id FROM instant_messages)";
                }

                string classes = Request["class"];

                if (!string.IsNullOrEmpty(classes))
                {
                    query += " AND class IN (" + classes + ")";
                }


                return query;
            }
        }


        protected override string CustomQuery
        {
            get
            {
                string startDate = "";
                string endDate = "";

                GetDateRange(out startDate, out endDate);
                string query = "";
                if (!curUser.HasPrivilege && !curUser.Policy.CanViewAll)
                {
                    query +=
                       "SELECT users.name as uname, alerts.id, alerts.alert_text, alerts.title, alerts.type, alerts.aknown, alerts.sent_date, alerts.type2, alerts.sender_id, alerts.create_date, alerts.class, alerts.urgent, alerts.ticker, alerts.fullscreen, s.id as is_rsvp, alerts.anonymous_survey FROM alerts LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = alerts.id LEFT JOIN users ON users.id = alerts.sender_id WHERE alerts.sent_date >= '" +
                       startDate + "' AND alerts.sent_date <= '" + endDate +
                       "' AND alerts.type='S' AND alerts.sender_id IN (" + string.Join(",", curUser.Policy.SendersViewList.ToArray())
                       + ") AND alerts.id NOT IN (SELECT alert_id FROM instant_messages)";


                }
                else
                {
                    query +=
                         "SELECT users.name as uname, alerts.id, alerts.alert_text, alerts.title, alerts.type, alerts.aknown, alerts.sent_date, alerts.type2, alerts.sender_id, alerts.create_date, alerts.class, alerts.urgent, alerts.ticker, alerts.fullscreen, s.id as is_rsvp, alerts.anonymous_survey FROM alerts LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = alerts.id LEFT JOIN users ON users.id = alerts.sender_id WHERE alerts.sent_date >= '" +
                         startDate + "' AND alerts.sent_date <= '" + endDate +
                         "' AND alerts.type='S' AND alerts.id NOT IN (SELECT alert_id FROM instant_messages)";
                }

                string classes = Request["class"];

                if (!string.IsNullOrEmpty(classes))
                {
                    query += " AND class IN (" + classes + ")";
                }

                if (!string.IsNullOrEmpty(SortBy))
                {
                    query += " ORDER BY alerts." + SortBy;
                }
                else
                {
                    query += " ORDER BY alerts.title";
                }

                return query;
            }
        }

        private void GetDateRange(out string startDate, out string endDate)
        {
            startDate = "";
            endDate = "";
            if (_selectRadio == "1")
            {
                DateTime toDate = DateTime.MaxValue;
                DateTime fromDate = DateTime.MinValue;

                int range = Convert.ToInt32(_rangeValue);

                switch (range)
                {
                    case 1:
                        {
                            toDate = DateTime.Now.AddDays(1);
                            fromDate = DateTime.Now.AddDays(-1);
                            toDate = new DateTime(toDate.Year, toDate.Month, toDate.Day, 0, 0, 0);
                            fromDate = new DateTime(fromDate.Year, fromDate.Month, fromDate.Day, 23, 59, 59);

                            break;
                        }

                    case 2:
                        {
                            toDate = DateTime.Now;
                            fromDate = DateTime.Now.AddDays(-2);
                            toDate = new DateTime(toDate.Year, toDate.Month, toDate.Day, 0, 0, 0);
                            fromDate = new DateTime(fromDate.Year, fromDate.Month, fromDate.Day, 23, 59, 59);

                            break;
                        }
                    case 3:
                        {
                            toDate = DateTime.Now;
                            fromDate = DateTime.Now.AddDays(-7);
                            fromDate = new DateTime(fromDate.Year, fromDate.Month, fromDate.Day, 0, 0, 0);

                            break;
                        }
                    case 4:
                        {
                            fromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                            toDate = fromDate.AddMonths(1).AddDays(-1);

                            break;
                        }
                    case 5:
                        {
                            DateTime lastMonthDate = DateTime.Now.AddMonths(-1);
                            fromDate = new DateTime(lastMonthDate.Year, lastMonthDate.Month, 1);
                            toDate = fromDate.AddMonths(1).AddDays(-1);

                            break;
                        }
                    default:
                        {
                            toDate = DateTime.MaxValue;
                            fromDate = new DateTime(1753, 1, 1);

                            break;
                        }
                }

                startDate = fromDate.ToString(DateTimeFormat.JavaScript, CultureInfo.InvariantCulture);
                endDate = toDate.ToString(DateTimeFormat.JavaScript, CultureInfo.InvariantCulture);
            }
            else if (_selectRadio == "2")
            {
                string dateFormat = DeskAlertsUtils.GetDateFormat(dbMgr);
                startDate = DateTime.ParseExact(fromDate.Value + " 00:00:00", dateFormat + " HH:mm:ss", CultureInfo.InvariantCulture).ToString(DateTimeFormat.JavaScript, CultureInfo.InvariantCulture);
                endDate = DateTime.ParseExact(toDate.Value + " 23:59:59", dateFormat + " HH:mm:ss", CultureInfo.InvariantCulture).ToString(DateTimeFormat.JavaScript, CultureInfo.InvariantCulture);
                // endDate = toDate.Value + " 00:00:00";
            }
        }



        /*
         * 
         * If you want to add condition for data selection override property ConditionSqlString 
         * Also you can implement your custom query by overriding properties CustomQuery and CustomCountQuery
         *  
         */
        #endregion

        #region Interface properties

        protected override HtmlGenericControl TopPaging => topPaging;
        protected override HtmlGenericControl BottomPaging => bottomRanging;
        protected override IButtonControl[] DeleteButtons => new IButtonControl[] { };
        protected override HtmlGenericControl NoRowsDiv => NoRows;
        protected override HtmlGenericControl TableDiv => mainTableDiv;
        protected override string ContentName => resources.LNG_OBJECTS;

        #endregion
    }
}