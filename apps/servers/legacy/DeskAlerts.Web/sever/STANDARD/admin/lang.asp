<%
default_lng = ConfDefaultLanguage
uid = Session("uid")
if uid<>"" then
	Set USRLang = Conn.Execute("SELECT ll.code FROM languages_list ll, users_langs ul WHERE ll.id=ul.lang_id AND ul.user_id="&uid)
	if not USRLang.EOF then
		default_lng = USRLang("code")
	end if
end if
on error resume next
Set rsLangSettings = Conn.Execute("SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE column_name = '"& Replace(default_lng, "'", "''") &"' AND table_name = 'languages'")
if rsLangSettings.EOF then
	default_lng = "EN"
end if
Set rsLangSettings = Conn.Execute("SELECT [name], [EN], ["& Replace(default_lng, "]", "]]") &"] FROM [languages]")
do while not rsLangSettings.EOF
	sName = rsLangSettings("name") & ""
	sVal = rsLangSettings(default_lng) & ""
	sDef = rsLangSettings("EN") & ""

	If sVal = "" Then sVal = sDef End If
	If InStr(sName, "LNG_") = 1 Then
		sName = Replace(sName, ":", "")
		sVal = Replace(sVal, """", "&quot;")
		sVal = Replace(sVal,"'","&#39;")
		Execute(sName & " = """ & sVal & """")
	End If
	rsLangSettings.MoveNext
loop
on error goto 0
%>