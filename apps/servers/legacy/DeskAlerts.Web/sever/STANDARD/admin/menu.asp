﻿<!-- #include file="config.inc" -->
<!-- #include file="demo.inc" -->
<!-- #include file="db_conn.asp" -->

<!-- #include file="lang.asp" -->
<!-- #include file="ad.inc" -->


<%

check_session()

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>DeskAlerts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/style9.css" rel="stylesheet" type="text/css">
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
<link href="css/menu.css" rel="stylesheet" type="text/css">
<script language="javascript" type="text/javascript" src="jscripts/json2.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>

<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.cookies.2.2.0.min.js"></script>
<script language="javascript" type="text/javascript">
	function showHintDiv(id) {
		var el = document.getElementById(id);
		if(el) el.style.display="";
	}

	function hideHintDiv(id){
		var el = document.getElementById(id);
		if(el) el.style.display="none";
	}
	function setCookieIfClicked(id) {
	        var el = document.getElementById(id);
	        if (el) el.style.display = "none";
	        
	}

	$(function() {
	    var root = $('#accordion');

		$('> *', root).each(function(index) {
			this.id = 'm-' + index;
		});

		$.get("set_parameters.asp?name=getMenuOrder", function (data) {
		    if (data != "") {
		        $.each(data.split(','), function () {
		            $('#' + id).appendTo(root);
		        });
		    };
		});

		$.get("set_parameters.asp?name=getCloseHintStatus", function (data) {
		    if (data == 1) {
		        document.getElementById('close_if_clicked').style.display = 'none';
		        document.getElementById('spacer_to_hide').style.display = 'none';
		    };
		});

		$('#close_hint').click(function () {
		    $.get("set_parameters.asp?name=setCloseHintStatus&closeHintStatus=1");
			document.getElementById('close_if_clicked').style.display = 'none';
	        document.getElementById('spacer_to_hide').style.display = 'none';
	    });
	    $('#restoreMenuOrder').button({
	        icons: {
	            primary: "ui-icon-arrowrefresh-1-s"
	        },
	        text: false
	    }).click(function() {
		    $.get("set_parameters.asp?name=setMenuOrder&menuOrder=" + "");
			for (var id = 0; true; id++) {
				if ($('#m-' + id).length > 0) {
					$('#m-' + id).appendTo(root);
				} else {
					break;
				}
			}
		});
		$('.buy_addons').find('a').button({
			icons: {
				primary: "ui-icon-cart"
			}
		}).css("margin", "3px");
		$('.contact_us').find('a').button({
			icons: {
				primary: "ui-icon-mail-closed"
			}
		}).css("margin", "3px");
		var $win = $(window);
		var $body = $(document.body);
		if ($win.width() >= $body.width() && $win.height() >= $body.height()) {
			$body.attr("scroll", "auto");
		}
		$('#accordion').accordion({
			header: "> div > h3",
			heightStyle: "content",
			active: false,
			collapsible: true,
			beforeActivate: function(event, ui) {
				if (ui.newHeader[0]) {
					var currHeader = ui.newHeader;
					var currContent = currHeader.next('.ui-accordion-content');
				} else {
					var currHeader = ui.oldHeader;
					var currContent = currHeader.next('.ui-accordion-content');
				}
				var isPanelSelected = currHeader.attr('aria-selected') == 'true';

				currHeader.toggleClass('ui-corner-all', isPanelSelected).toggleClass('accordion-header-active ui-state-active ui-corner-top', !isPanelSelected).attr('aria-selected', ((!isPanelSelected).toString()));

				currHeader.children('.ui-icon').toggleClass('ui-icon-triangle-1-e', isPanelSelected).toggleClass('ui-icon-triangle-1-s', !isPanelSelected);

				currContent.toggleClass('accordion-content-active', !isPanelSelected)
				if (isPanelSelected) { currContent.slideUp(); } else { currContent.slideDown(); }

				return false;
			}
		})
		.sortable({
			delay: 150,
			distance: 10,
			tolerance: "pointer",
			containment: "#sortable_container",
			update: function(event, ui) {
			    var order = $(this).sortable('serialize');
			    $.get("set_parameters.asp?name=setMenuOrder&menuOrder="+order);
			},
			axis: "y",
			stop: function(event, ui) {
				ui.item.children("h3").triggerHandler("focusout");
			}
		});

		$.get("set_parameters.asp?name=getMultiple", function (data) {
		    if (data != "") {
		        $.each(data.split('&'), function () {
		            var id = this;
		            var index = $('#' + id).index();
		            $("#accordion").accordion("option", "active", index);
		        });
		    };
		});

		$('.single').unbind().bind("click", function() {
			$(this).find("a").get(1).click();
		});

		$('.multiple').bind("click", function() {
			var active = "";
			$('.multiple.ui-state-active').each(function() {
				if (active) { active += "&"; }
				active += $(this).parent().attr('id');
			});
			$.get("set_parameters.asp?name=setMultiple&multiple="+active);
		});
	});
	
	
</script>
</head>

<body unselectable='on' onselectstart='return false;'  class="menu_body" style="-webkit-user-select: none; overflow-x: hidden; position: relative; -khtml-user-select: none;   -moz-user-select: none; -o-user-select: none; user-select: none;" scroll="yes">
<table cellpadding="0" cellspacing="0" border="0" valign="top" width="100%">
<%
	uid = Session("uid")

	admin = 0
	moder = 0
	policy = 0
	only_templates=0
	only_alerts=0
	alerts_checked_arr = Array ("", "", "", "", "", "", "")
	email_checked_arr = Array ("", "", "", "", "", "", "")
	surveys_checked_arr = Array ("", "", "", "", "", "", "")
	rss_checked_arr = Array ("", "", "", "", "", "", "")
	screensavers_checked_arr = Array ("", "", "", "", "", "", "")
	wallpapers_checked_arr = Array ("", "", "", "", "", "", "")
        lockscreens_checked_arr = Array ("", "", "", "", "", "", "")
	im_checked_arr = Array ("", "", "", "", "", "", "")
	feedback_checked_arr = Array ("", "", "", "", "", "", "")
	cc_checked_arr = Array ("", "", "", "", "", "", "")
	video_checked_arr = Array ("", "", "", "", "", "", "")
	webmodule_checked_arr = Array ("", "", "", "", "", "", "")
	campaign_checked_arr = Array ("", "", "", "", "", "", "")
	'canApprove = 1
	
	Set RS = Conn.Execute("SELECT id, role FROM users WHERE id="&uid)
	if(Not RS.EOF) then
		if(RS("role")="A") then
			admin = 1
		end if
		
		if(RS("role")="M") then
		    moder = 1
		end if
		
		Set rs_policy = Conn.Execute("SELECT type FROM policy INNER JOIN policy_editor ON policy.id=policy_editor.policy_id WHERE editor_id=" & uid)
		if(Not rs_policy.EOF) then
			if(rs_policy("type")="A") then
				admin = 1
			end if
			if(rs_policy("type")="M") then
				moder = 1
			end if
		end if
		
		

		if(admin = 0 and moder = 0) then
			Set RS = Conn.Execute("SELECT alerts_val, emails_val, sms_val, surveys_val, users_val, groups_val, templates_val, text_templates_val, statistics_val, ip_groups_val, rss_val, screensavers_val, wallpapers_val, lockscreens_val, im_val, feedback_val, cc_val, video_val, webplugin_val, campaign_val FROM policy_list INNER JOIN policy_editor ON policy_list.policy_id=policy_editor.policy_id WHERE editor_id=" & uid)
			if(Not RS.EOF) then
				if(Not IsNull(RS("alerts_val"))) then
					if(InStr(RS("alerts_val"), "1")) then
						alerts_checked = "checked"
					end if
					alerts_checked_arr = p_makeCheckedArray(RS("alerts_val"))
				end if
				if(Not IsNull(RS("emails_val"))) then
					if(InStr(RS("emails_val"), "1")) then 'RS("emails_val") <> "0000000"
						emails_checked = "checked"
					end if
					emails_checked_arr = p_makeCheckedArray(RS("emails_val"))
				end if
				if(Not IsNull(RS("surveys_val"))) then
					if(InStr(RS("surveys_val"), "1")) then
						surveys_checked = "checked"
					end if
					surveys_checked_arr = p_makeCheckedArray(RS("surveys_val"))
				end if
				if(Not IsNull(RS("users_val"))) then
					if(InStr(RS("users_val"), "1")) then
						users_checked = "checked"
					end if
				end if
				if(Not IsNull(RS("groups_val"))) then
					if(InStr(RS("groups_val"), "1")) then
						groups_checked = "checked"
					end if
				end if
				if(Not IsNull(RS("text_templates_val"))) then
					if(InStr(RS("text_templates_val"), "1")) then
						templates_checked = "checked"
					end if
				end if
				if(Not IsNull(RS("statistics_val"))) then
					if(InStr(RS("statistics_val"), "1")) then
						statistics_checked = "checked"
					end if
				end if
				if(Not IsNull(RS("ip_groups_val"))) then
					if(InStr(RS("ip_groups_val"), "1")) then
						ip_groups_checked = "checked"
					end if
				end if
				if(Not IsNull(RS("rss_val"))) then
					if(InStr(RS("rss_val"), "1")) then
						rss_checked = "checked"
					end if
					rss_checked_arr = p_makeCheckedArray(RS("rss_val"))
				end if
				if(Not IsNull(RS("screensavers_val"))) then
					if(InStr(RS("screensavers_val"), "1")) then
						screensavers_checked = "checked"
					end if
					screensavers_checked_arr = p_makeCheckedArray(RS("screensavers_val"))
				end if
				if(Not IsNull(RS("wallpapers_val"))) then
					if(InStr(RS("wallpapers_val"), "1")) then
						wallpapers_checked = "checked"
					end if
					wallpapers_checked_arr = p_makeCheckedArray(RS("wallpapers_val"))
				end if
				if(Not IsNull(RS("lockscreens_val"))) then
					if(InStr(RS("lockscreens_val"), "1")) then
						lockscreens_checked = "checked"
					end if
					lockscreens_checked_arr = p_makeCheckedArray(RS("lockscreens_val"))
				end if

				if(Not IsNull(RS("im_val"))) then
					if(InStr(RS("im_val"), "1")) then
						im_checked = "checked"
					end if
					im_checked_arr = p_makeCheckedArray(RS("im_val"))
				end if
				if(Not IsNull(RS("feedback_val"))) then
					if(InStr(RS("feedback_val"), "1")) then
						feedback_checked = "checked"
					end if
					feedback_checked_arr = p_makeCheckedArray(RS("feedback_val"))
				end if
				if(Not IsNull(RS("cc_val"))) then
					if(InStr(RS("cc_val"), "1")) then
						cc_checked = "checked"
					end if
					cc_checked_arr = p_makeCheckedArray(RS("cc_val"))
				end if

				if(Not IsNull(RS("video_val"))) then
					if(InStr(RS("video_val"), "1")) then
						video_checked = "checked"
					end if
					video_arr = p_makeCheckedArray(RS("video_val"))
				end if
				
				if(Not IsNull(RS("webplugin_val"))) then
					if(InStr(RS("webplugin_val"), "1")) then
						webmodule_checked = "checked"
					end if
					webmodule_arr = p_makeCheckedArray(RS("webplugin_val"))
				end if
				
				if(Not IsNull(RS("campaign_val"))) then
					if(InStr(RS("campaign_val"), "1")) then
						campaign_checked = "checked"
					end if
					campaign_arr = p_makeCheckedArray(RS("campaign_val"))
				end if
				

				
				
	end if
			policy_ids=editorGetPolicyIds(uid)
			if(policy_ids<>"") then
				policy=1
			end if
		end if
	end if

	RS.Close
	dllErrorMessage = ""
on error resume next
	Set lic = Server.CreateObject("DeskAlertsServer.Licensing")
	if(Err.Number <> 0) then
		dllErrorMessage = LNG_ERROR_SERVER_DLL_NOT_REGISTERED
	else
		count = lic.getLicenseCount(Conn)
		users = lic.getCurrentUsersCount(Conn)
		if(users = -1) then
			users = 0
		end if
		trial = lic.getTrialTime(Conn)
		elipse = lic.getTrialExpire(Conn)

        Dim fso, tf
       ' Set fso = CreateObject("Scripting.FileSystemObject")
      '  path = Server.MapPath("logs/log.inf")
       ' Set tf = fso.OpenTextFile(path, 8, True)
       ' tf.WriteLine("MENU LOG")
      '  tf.WriteLine("trial="&trial) 
       ' tf.WriteLine("elipse="&elipse) 
       ' tf.WriteLine("users="&users) 
       ' tf.WriteLine("count="&count) 
       ' tf.Close
	end if
on error goto 0

	if(users = "") then users = 0 end if
	if(count = "") then count = 0 end if
	if(users > 0 and count > 0) then
		proc=clng(users)/count*100
		if(proc > 101) then
			proc = 101
		end if
	else
		proc = 0
	end if
	if(count = 0) then
		count = "-"
	end if


%>
<tr><td  colspan="2">
<div class="folders" style="text-align: center; vertical-align: middle;">
<div><font color="red"><b><%=dllErrorMessage %></b></font></div>
<div onmouseover="showHintDiv('hint_div');" onmouseout="hideHintDiv('hint_div');">
<b><%=LNG_ACTIVE_USERS_LICENSE_LIMIT %></b><br/>
<div>
	<div style="position: absolute; z-index:2; left: 20px; width: 172px; border: 1px solid #000000; text-align: center;">
		<b><%=users & " / " & count %></b>
	</div>
<%
	response.write "<div style=""position: absolute; z-index:1; left: 20px; border: 1px solid #000000; border-right: 0px none #000000; width: "
	response.write Int(clng(proc*1.7)) & "px;"
	if(Int(clng(proc*1.7))=0) then
		response.write "display:none;"
	else
		response.write "display:block;"
	end if
	hintText = LNG_NORMAL_LICENSE_UTILIZATION_LEVEL

	response.write "px; text-align: center; background-color: "
	if(proc<60) then
		response.write "#00ff00;"
	elseif(proc>=60 AND proc < 90) then
		response.write "#ffff00;"
	elseif(proc >= 90 AND proc <= 100) then
		hintText = LNG_YOUR_LICENSE_LIMIT_IS_ABOUT_TO_EXCEED
		response.write "#ff4500;"
	elseif(proc > 100) then
		hintText = LNG_LICENSE_EXCEED_DESC
		response.write "#ff0000;"
	end if
	
	response.write " float:left;"">&nbsp;</div>"
%>
</div>
<br/>
<div id="hint_div" style="z-index:3;background-color: #ffffe1; width: 172px; border: 1px solid #000000; position: absolute; left: 20px; display: none;"><%=hintText %></div>
<%
if(proc>100) then
	%>
	<br/>
	<div style="color: #ff0000; font-weight: bold;"><%=LNG_LICENSE_EXCEED_DESC %></div>	
	<%
end if
%>
</div>
<div onmouseover="showHintDiv('trial_hint_div');" onmouseout="hideHintDiv('trial_hint_div');">
<%
if trial > 0 and elipse > -1 then
	trial = Clng(trial / 60 / 60 / 24)
	elipse = Clng(elipse / 60 / 60 / 24)
	if trial >= elipse then
%>
<b style="color: #ff0000;"><br/><%
	if InStr(1, LNG_TRIAL_EXPIRES_IN_DAYS, "%d", 1) > 0 then
		Response.Write Replace(LNG_TRIAL_EXPIRES_IN_DAYS, "%d", trial-elipse, 1, -1, 1)
	else
		Response.Write LNG_TRIAL_EXPIRES_IN_DAYS & ": " & (trial-elipse)
	end if
%></b>

<div style="color: #ff0000; font-weight: bold;"><br/><%=LNG_TRIAL_HAS_EXPIRED %></div>
<%
	end if
end if
%>
</div>
<div id="scheduled_tasks_div" class="scheduled_tasks_div">
<%
	rssTaskText = ""
	scheduledTaskText = ""
	archiveTaskText = ""
	taskError = False
	
	SQL = "SET NOCOUNT ON" &_
		" IF ISDATE('"&ConfRSSReaderTaskTimeStamp&"') = 1" &_
		" BEGIN" &_
			" SELECT DATEDIFF(n,CONVERT(DATETIME,'"&ConfRSSReaderTaskTimeStamp&"'),GETUTCDATE()) as diff" &_
		" END" &_
		" ELSE" &_
		" BEGIN" &_
			" UPDATE settings SET val = CONVERT(nvarchar, GETUTCDATE(), 9) WHERE name='ConfRSSReaderTaskTimeStamp'" &_
			" SELECT 0 as diff" &_
		" END" &_
		" IF ISDATE('"&ConfSendReccurenceTaskTimeStamp&"') = 1" &_
		" BEGIN" &_
			" SELECT DATEDIFF(n,CONVERT(DATETIME,'"&ConfSendReccurenceTaskTimeStamp&"'),GETUTCDATE()) as diff" &_
		" END" &_
		" ELSE" &_
		" BEGIN" &_
			" UPDATE settings SET val = CONVERT(nvarchar, GETUTCDATE(), 9) WHERE name='ConfSendReccurenceTaskTimeStamp'" &_
			" SELECT 0 as diff" &_
		" END" &_
		" IF ISDATE('"&ConfArchiveTaskTimeStamp&"') = 1" &_
		" BEGIN" &_
			" SELECT DATEDIFF(n,CONVERT(DATETIME,'"&ConfArchiveTaskTimeStamp&"'),GETUTCDATE()) as diff" &_
		" END" &_
		" ELSE" &_
		" BEGIN" &_
			" UPDATE settings SET val = CONVERT(nvarchar, GETUTCDATE(), 9) WHERE name='ConfArchiveTaskTimeStamp'" &_
			" SELECT 0 as diff" &_
		" END"

	SET RS = Conn.Execute(SQL)
	if RSS = 1 then
		if (not RS.EOF) then
			if (Clng(RS("diff")) > 1440) then 
				rssTaskText = """Deskalerts RSS Send Task"""
				taskError = True
			end if
		end if
	end if
	
	SET RS = RS.NextRecordSet
	
	if (not RS.EOF) then
		if (Clng(RS("diff")) > 1440) then 
			scheduledTaskText = """Deskalerts Scheduled Task"""
			taskError = True
		end if
	end if
	
	SET RS = RS.NextRecordSet
	
	if (not RS.EOF) then
		if (Clng(RS("diff")) > 1440) then 
			archiveTaskText= """Deskalerts Archive Task"""
			taskError = True
		end if
	end if
	
	RS.Close
	
	if (taskError) then
		Response.Write "<br/>"& LNG_SOME_SCHEDULED_TASKS_ARE_NOT_RUNNING &":<br/>"
		
		taskText = ""
		
		taskText = rssTaskText
		
		if (taskText <> "" AND scheduledTaskText <> "") then 
			taskText = taskText & ", "
		end if
		
		taskText = taskText & scheduledTaskText
		
		if (taskText <> "" AND archiveTaskText <> "") then 
			taskText = taskText & ", "
		end if
		
		taskText = taskText & archiveTaskText
		
		Response.Write taskText
		
		Response.Write "<p>See possible solutions <a target='_blank' href='http://www.alert-software.com/support/documentation/administrators-manual/troubleshooting/#scheduled-tasks'>here</a></p>"
		
	end if
	
%>

</div>
</div>
</td></tr>
<% if DEMO <> 1 and not trial > 0 and ConfHideBuyAddonsLink <> 1 then %>
<tr><td class="menu_spacer buy_addons_spacer" height="1" colspan="2"></td></tr>
<tr><td class="menu buy_addons" colspan="2">
	<div style="text-align: center; vertical-align: middle;" class="folders">
		<a href="buy_addons.asp" class="menu_item buy_addons_btn" target="work_area"><%=LNG_BUY_MODULES %></a>
	</div>
</td></tr>
<% end if %>
<% if DEMO = 1 then %>
<tr><td class="menu_spacer buy_addons_spacer" height="1" colspan="2"></td></tr>
<tr><td class="menu contact_us" colspan="2">
	<div style="text-align: center; vertical-align: middle; padding:5px 10px" class="folders">
		<a href="contact_deskalerts.asp" class="menu_item contact_us_button" target="work_area">Get Free Quote</a>
	</div>
</td></tr>
<% end if %>
<% if trial>0 then %>
<tr><td class="menu_spacer buy_addons_spacer" height="1" colspan="2"></td></tr>
<tr><td class="menu contact_us" colspan="2">
	<div style="text-align: center; vertical-align: middle; padding:5px 10px" class="folders">
		<a href="request_quote.asp" class="menu_item contact_us_button" target="work_area">Get Free Quote</a>
	</div>
</td></tr>
<% end if %>
<tr id="spacer_to_hide" style="display:none"><td class="menu_spacer" height="1" colspan="2"></td></tr>
<tr id="close_if_clicked"  style="display:none" class="hints"><td> <div style="font-size:12px; text-align:center; margin:2px"><%=LNG_CLICK_AND_DRAG %></div></td>
<td style="padding-right:10px"><div style="vertical-align:middle; cursor: pointer;  font-weight:bold;" id="close_hint" onclick="">Hide</div></td>
</tr>
<tr><td class="menu_spacer" height="1" colspan="2"></td></tr>
<tr><td width="100%" style="height:40px; "><span class="header_title""><%=LNG_MENU %></span></td>
<td align="right"><button style="vertical-align:middle" class="restoreMenuOrder" id="restoreMenuOrder"><%=LNG_RESTORE_MENU_ORDER %></button></td>
</tr>

</table>
<div id="sortable_container" style="overflow-x: hidden; position: relative;">
<div id="accordion" style="overflow-x: hidden; position: relative;">
<!-- 
<div class="group" style="display:none">
	<h3 class="single"><table><tr><td ><a href="channels.asp" class="menu_item" target="work_area"><img class="menu_icon" src="images/menu_icons/channels.png" alt="Channels" width="20" height="20" border="0"></a></td><td ><a href="channels.asp" class="menu_item" target="work_area"><span name="menu_item" class="menu_text"><%= LNG_CHANNELS%></span></a></td></tr></table></h3>
</div> -->
<%if admin = 1 OR moder = 1 OR alerts_checked <> "" OR (RSS = 1 and rss_checked <> "") OR (SS = 1 and screensavers_checked <> "") OR (WP = 1 and wallpapers_checked <> "") OR (LS = 1 and lockscreens_checked <> "") OR (IM=1 and im_checked <> "") then%>
<div class="group">
	<h3 class="single"><table><tr><td ><a href="dashboard.asp" class="menu_item" target="work_area"><img class="menu_icon" src="images/menu_icons/contentschedule_20.png" alt="" width="20" height="20" border="0"></a></td><td ><a href="dashboard.asp" class="menu_item" target="work_area"><span name="menu_item" class="menu_text"><%=LNG_DASHBOARD %></span></a></td></tr></table></h3>
</div>
<%end if%>

<%if (admin = 1 OR moder = 1 OR alerts_checked <> "" OR emails_checked <> "") then%>
<div class="group">
	<h3 class="multiple"><table><tr><td  width="1%"><span class="menu_item"><img class="menu_icon" src="images/menu_icons/alert_20.png" alt="<%=LNG_ALERT_MENU_ITEM %>" width="20" height="20" border="0"></span></td><td ><span class="menu_text"><span name="menu_item" class="menu_item"><%=LNG_ALERT_MENU_ITEM %></span></span></td></tr></table></h3>
	<div class="folders">
		<table cellspacing="0" cellpadding="0" border="0">
	<%if (admin = 1 OR moder = 1 OR alerts_checked_arr(0) <> "" OR emails_checked <> "" OR ConfTGroupEnabled = 1) then%>
		<tr>
					<td background="images/midvertline.gif"><img height="18" src="images/midline.gif" width="18" border="0"></td>
			<td><a href="edit_alert_db.asp?skin_id=" target="work_area"><img class="menu_icon" src="images/menu_icons/alert_create.png" alt="<%=LNG_CREATE %>" width="18" height="18" border="0"></a></td>
			<td colspan="3"><nobr>&nbsp;<a href="edit_alert_db.asp?skin_id=" target="work_area" name="menu_subitem_<%=LNG_ALERT_MENU_ITEM %>"><%=LNG_CREATE_ALERT %></a></nobr></td>
			
		</tr>
		
     <%end if 
        if ( TICKER_CFG = 1 OR TICKER_RPO = 1 ) AND (admin = 1 OR moder = 1 OR alerts_checked_arr(0) <> "" OR emails_checked <> "" OR ConfTGroupEnabled = 1) then%>
		<tr>
		<td background="images/midvertline.gif"><img height="18" src="images/midline.gif" width="18" border="0"></td>
			<td><a href="edit_alert_db.asp?skin_id=" target="work_area"><img class="menu_icon" src="images/menu_icons/ticker.png" alt="<%=LNG_TICKER %>" width="18" height="13" border="0"></a></td>
			<td colspan="3"><nobr>&nbsp;<a href="edit_alert_db.asp?ticker=1&skin_id=" target="work_area" name="menu_subitem_<%=LNG_ALERT_MENU_ITEM %>"><%=LNG_CREATE_TICKER %></a></nobr></td>
		</tr>
	<%end if
	  if (admin = 1 OR moder = 1 OR alerts_checked_arr(4) <> "" OR alerts_checked_arr(5) <> "") then%>
		<tr>
			<td background="images/midvertline.gif"><img height="18" src="images/midline.gif" width="18" border="0"></td>
			<td><a href="sent.asp" target="work_area"><img class="menu_icon" src="images/menu_icons/alert_sent.png" alt="<%=LNG_SENT %>" width="18" height="18" border="0"></a></td>
			<td colspan="3"><nobr>&nbsp;<a href="sent.asp" target="work_area" name="menu_subitem_<%=LNG_ALERT_MENU_ITEM %>"><%=LNG_SENT %></a></nobr></td>
		</tr>
		<tr>
			<%if (admin = 1 OR moder = 1 OR templates_checked <> "") then%><td background="images/midvertline.gif"><img height="18" src="images/midline.gif" width="18" border="0"><%else %><td><img height="18" src="images/endline.gif" width="18" border="0"><%end if %></td>
			<td><a href="draft.asp" target="work_area"><img class="menu_icon" src="images/menu_icons/alert_draft.png" alt="<%=LNG_DRAFT %>" width="18" height="18" border="0"></a></td>
			<td colspan="3"><nobr>&nbsp;<a href="draft.asp" target="work_area" name="menu_subitem_<%=LNG_ALERT_MENU_ITEM %>"><%=LNG_DRAFT %></a></nobr></td>
		</tr>
	<%end if%>
	<%if (admin = 1 OR moder = 1 OR templates_checked <> "") then%>
		<tr>
		<% if (admin=1 AND ConfTGroupEnabled = 1) then%>
			<td><img height="18" src="images/midline.gif" width="18" border="0"></td>
		<% else %>
		    <td><img height="18" src="images/endline.gif" width="18" border="0"></td>
		<% end if %>
			<td><a href="text_templates.asp" target="work_area"><img class="menu_icon" src="images/menu_icons/templates_20.png" alt="<%=LNG_TEXT_TEMPLATES %>" width="18" height="18" border="0"></a></td>
			<td colspan="3"><nobr>&nbsp;<a href="text_templates.asp" target="work_area" name="menu_subitem_<%=LNG_ALERT_MENU_ITEM %>"><%=LNG_TEXT_TEMPLATES %></a></nobr></td>
		</tr>
	<%end if %>
	
	<%if ((admin = 1 OR moder = 1) AND ConfTGroupEnabled = 1) then%>
		<tr>
			<td><img height="18" src="images/endline.gif" width="18" border="0"></td>
			<td><a href="tgroups.asp" target="work_area"><img class="menu_icon" src="images/menu_icons/templates_20.png" alt="<%=LNG_TEXT_TEMPLATES %>" width="18" height="18" border="0"></a></td>
			<td colspan="3"><nobr>&nbsp;<a href="tgroups.asp" target="work_area" name="menu_subitem_<%=LNG_ALERT_MENU_ITEM %>"><%=LNG_TGROUPS_MENUITEM %></a></nobr></td>
		</tr>
	<%end if %>
		</table>
	</div>
</div>
<%
end if
 if CAMPAIGN = 1 then%>
<div class="group">
	<h3 class="single"><table><tr><td ><a href="campaign.asp" class="menu_item" target="work_area"><img class="menu_icon" src="images/menu_icons/campaign.png" alt="Statistic" width="20" height="20" border="0"></a></td><td ><a href="campaign.asp" class="menu_item" target="work_area"><span name="menu_item" class="menu_text"><%= LNG_CAMPAIGN_TITLE%></span></a></td></tr></table></h3>
</div>

<% 
 end if
 %>


<% if DIGSIGN=1 AND (admin = 1 OR moder = 1 OR alerts_checked <> "") then %>
<div class="group">
    <h3 class="multiple"><table><tr><td  width="1%"><span class="menu_item"><img class="menu_icon" src="images/menu_icons/ic_digsign.png" alt="<%=LNG_ALERTS %>" width="20" height="20" border="0"></span></td><td ><span class="menu_text"><span name="menu_item" class="menu_item"><%=LNG_DIGSIGN_TITLE %></span></span></td></tr></table></h3>
	<div class="folders">
		<table cellspacing="0" cellpadding="0" border="0">

		<tr>
		<%if (admin = 1 OR moder = 1 OR alerts_checked_arr(0) <> "" OR alerts_checked_arr(5) <> "") then%>
		    <td background="images/midvertline.gif"><img height="18" src="images/midline.gif" width="18" border="0"></td>
		<% else %>
		    <td background="images/endline.gif"><img height="18" src="images/endline.gif" width="18" border="0"></td>
		<% end if %>
			
			<td><a href="DigSignLinks.asp" target="work_area"><img class="menu_icon" src="images/menu_icons/ic_link.png" alt="<%=LNG_DIGSIGN_LINKS %>" width="18" height="18" border="0"></a></td>
			<td colspan="3"><nobr>&nbsp;<a href="DigSignLinks.asp" target="work_area" name="menu_subitem_<%=LNG_DIGSIGN_TITLE %>"><%=LNG_DIGSIGN_LINKS %></a></nobr></td>
		</tr>

	  <%if (admin = 1 OR moder = 1 OR alerts_checked_arr(0) <> "" OR alerts_checked_arr(5) <> "") then%>
		<tr>
			<td background="images/endline.gif"><img height="18" src="images/endline.gif" width="18" border="0"></td>
			<td><a href="edit_alert_db.asp?webplugin=1" target="work_area"><img class="menu_icon" src="images/menu_icons/alert_sent.png" alt="<%=LNG_DIGSIGN_SEND_CONTENT %>" width="18" height="18" border="0"></a></td>
			<td colspan="3"><nobr>&nbsp;<a href="edit_alert_db.asp?webplugin=1" target="work_area" name="menu_subitem_<%=LNG_DIGSIGN_TITLE %>"><%=LNG_DIGSIGN_SEND_CONTENT %></a></nobr></td>
		</tr>
	<%end if%>

		</table>
	</div>    
</div> 
 
<% end if %> 

 <%
 Set rs = Conn.execute("SELECT val FROM settings WHERE name = 'ConfAllowApprove'")  
 if APPROVE = 1 and rs("val") = 1 and admin <> 1 then 
    rejectedCount = getRejectedAlertsCountForUser(uid)
    
    if(rejectedCount <> "0") then
     rejectedMenuPostFix = " <b style='color:red'>(" & rejectedCount & ")</b>"
    else
       rejectedMenuPostFix = ""
    end if 
 %>
  <div class="group">
    <h3 class="single"><table><tr><td style="text-align:left"><a href="RejectedAlerts.aspx?uid=<% =uid %>" class="menu_item" target="work_area"><img class="menu_icon" src="images/menu_icons/reject.png" alt="Statistic" width="20" height="20" border="0"></a></td><td ><a href="RejectedAlerts.aspx?uid=<% =uid %>" class="menu_item" target="work_area"><span name="menu_item" class="menu_text"><%= LNG_REJECTED_ALERTS & rejectedMenuPostFix %></span></a></td></tr></table></h3>
</div>

<% 
    end if
   if APPROVE = 1 and rs("val") = 1   then 
    canApprove = 1
  else
    canApprove = 0
  end if  
  
 
 if admin = 1 OR moder = 1 then
    canApprove = 1
 elseif   alerts_checked_arr(7) = "checked" or rss_checked_arr(7) = "checked" or surveys_checked_arr(6) = "checked" or wallpapers_checked_arr(6) = "checked" or lockscreens_checked_arr(6) = "checked" or screensavers_checked_arr(6) = "checked" or im_checked_arr(5) = "checked" then
    canApprove = 1
 else 
    canApprove = 0
 end if
  
  
 ' APPROVE = 1 and canApprove = 1) or (admin = 1 and rs("val") = 1 and APPROVE = 1
 if (APPROVE = 1 and (admin = 1 OR moder = 1 or canApprove = 1) and rs("val") = "1") then
 
     approveCount = getNotApprovedAlertsForUser(uid)
    
    if(approveCount <> "0") then
     approveMenuPostFix = " <b style='color:red'>(" & approveCount & ")</b>"
    else
       approveMenuPostFix = ""
    end if 
    
 
%>
<div class="group">
    <h3 class="single"><table><tr><td style="text-align:left"><a href="approve_panel.aspx?uid=<%=uid%>" class="menu_item" target="work_area"><img class="menu_icon" src="images/menu_icons/approve.png" alt="Statistic" width="20" height="20" border="0"></a></td><td ><a href="approve_panel.aspx?uid=<%=uid%>" class="menu_item" target="work_area"><span name="menu_item" class="menu_text"><%= LNG_APPROVAL_PANEL & approveMenuPostFix %></span></a></td></tr></table></h3>
</div>



<% end if %>
<%
if(admin = 1 OR moder = 1 OR surveys_checked <> "") then
	if (SURVEY = 1) then

    link = "addSurvey.aspx"
    if(ConfOldSurvey = 1) then
        link = "survey_type_choose.asp"
    end if
%>
<div class="group">
	<h3 class="multiple" <% if SURVEY_TRIAL=1 then %>style="background:#efffef" title="<%=LNG_TRIAL_ADDON_DESCRIPTION %><%=TRIAL_DAYS_LEFT %>"<%end if %>><table><tr><td ><span class="menu_item"><img class="menu_icon" src="images/menu_icons/survey_20.png" alt="<%=LNG_SURVEY_QUIZZES_POLLS %>" width="20" height="20" border="0"></span></td><td ><span class="menu_text"><span name="menu_item" class="menu_item"><%=LNG_SURVEY_QUIZZES_POLLS %></span></span></td></tr></table></h3>
	<div class="folders">
		<table cellspacing=0 cellpadding=0 border=0>
	<%if (admin = 1 OR moder = 1 OR surveys_checked_arr(3) <> "") then%>
		<tr>
			<td background="images/midvertline.gif"><img height=18 src="images/midline.gif" width=18 border=0></td>
			<td><a href="<%=link%>" target="work_area"><img class="menu_icon" src="images/menu_icons/survey_create.png" alt="<%=LNG_CREATE %>" width="18" height="18" border="0"></a></td>
			<td colspan=3><nobr>&nbsp;<a href="<%=link%>" target="work_area" name="menu_subitem_<%=LNG_SURVEY_QUIZZES_POLLS %>"><%=LNG_CREATE %></a></nobr></td>
		</tr>
	<% end if %>
		<tr>
			<td background="images/midvertline.gif"><img height=18 src="images/midline.gif" width=18 border=0></td>
			<td><a href="surveys.asp" target="work_area"><img class="menu_icon" src="images/menu_icons/survey_active.png" alt="<%=LNG_ACTIVE %>" width="18" height="18" border="0"></a></td>
			<td colspan=3><nobr>&nbsp;<a href="surveys.asp" target="work_area" name="menu_subitem_<%=LNG_SURVEY_QUIZZES_POLLS %>"><%=LNG_ACTIVE %></a></nobr></td>
		</tr>
		<tr>
			<td><img height=18 src="images/endline.gif" width=18 border=0></td>
			<td><a href="surveys_archived.asp" target="work_area"><img class="menu_icon" src="images/menu_icons/survey_archived.png" alt="<%=LNG_ARCHIVED %>" width="18" height="18" border="0"></a></td>
			<td colspan=3><nobr>&nbsp;<a href="surveys_archived.asp" target="work_area" name="menu_subitem_<%=LNG_SURVEY_QUIZZES_POLLS %>"><%=LNG_ARCHIVED %></a></nobr></td>
		</tr>
		</table>
	</div>
</div>
<%	end if
end if %>

<% if RSS = 1 and (admin = 1 OR moder = 1 OR rss_checked <> "") then%>
<div class="group">
	<h3 class="multiple" <% if RSS_TRIAL=1 then %>style="background:#efffef" title="<%=LNG_TRIAL_ADDON_DESCRIPTION %><%=TRIAL_DAYS_LEFT %>"<%end if %>><table><tr><td  width="1%"><span class="menu_item"><img class="menu_icon" src="images/menu_icons/rss_20.png" alt="<%=LNG_RSS_NEWSFEEDS %>" width="20" height="20" border="0"></span></td><td ><span class="menu_text"><span name="menu_item" class="menu_item"><%=LNG_RSS_NEWSFEEDS %></span></span></td></tr></table></h3>
	<div class="folders">
		<table cellspacing="0" cellpadding="0" border="0">
	<%if (admin = 1 OR moder = 1 OR rss_checked_arr(0) <> "") then%>
		<tr>
			<td background="images/midvertline.gif"><img height="18" src="images/midline.gif" width="18" border="0"></td>
			<td><a href="rss_edit.asp" target="work_area"><img class="menu_icon" src="images/menu_icons/rss_create.png" alt="<%=LNG_CREATE %>" width="18" height="18" border="0"></a></td>
			<td colspan="3"><nobr>&nbsp;<a href="rss_edit.asp" target="work_area" name="menu_subitem_<%=LNG_RSS_NEWSFEEDS %>"><%=LNG_CREATE %></a></nobr></td>
		</tr>
	<% end if%>
	<%if (admin = 1 OR moder = 1 OR rss_checked_arr(4) <> "") then%>
		<tr>
			<td background="images/midvertline.gif"><img height="18" src="images/midline.gif" width="18" border="0"></td>
			<td><a href="rss_list.asp" target="work_area"><img class="menu_icon" src="images/menu_icons/rss_current.png" alt="<%=LNG_CURRENT %>" width="18" height="18" border="0"></a></td>
			<td colspan="3"><nobr>&nbsp;<a href="rss_list.asp" target="work_area" name="menu_subitem_<%=LNG_RSS_NEWSFEEDS %>"><%=LNG_CURRENT %></a></nobr></td>
		</tr>
		<tr>
			<td><img height="18" src="images/endline.gif" width="18" border="0"></td>
			<td><a href="rss_list.asp?draft=1" target="work_area"><img class="menu_icon" src="images/menu_icons/rss_draft.png" alt="<%=LNG_DRAFT %>" width="18" height="18" border="0"></a></td>
			<td colspan="3"><nobr>&nbsp;<a href="rss_list.asp?draft=1" target="work_area" name="menu_subitem_<%=LNG_RSS_NEWSFEEDS %>"><%=LNG_DRAFT %></a></nobr></td>
		</tr>
	<% end if%>
		</table>
	</div>
</div>
<% end if %>

<% if SS = 1 and (admin = 1 OR moder = 1 OR screensavers_checked <> "") then%>
<div class="group">
	<h3 class="multiple" <% if SS_TRIAL=1 then %>style="background:#efffef" title="<%=LNG_TRIAL_ADDON_DESCRIPTION %><%=TRIAL_DAYS_LEFT %>"<%end if %>><table><tr><td  width="1%"><span class="menu_item"><img class="menu_icon" src="images/menu_icons/screensaver_20.png" alt="<%=LNG_RSS %>" width="20" height="20" border="0"></span></td><td ><span class="menu_text"><span name="menu_item" class="menu_item"><%=LNG_SCREENSAVERS %></span></span></td></tr></table></h3>
	<div class="folders">
		<table cellspacing="0" cellpadding="0" border="0">
	<%if (admin = 1 OR moder = 1 OR screensavers_checked_arr(0) <> "") then%>
		<tr>
			<td background="images/midvertline.gif"><img height="18" src="images/midline.gif" width="18" border="0"></td>
			<td><a href="screensaver_type_choose.asp" target="work_area"><img class="menu_icon" src="images/menu_icons/screensaver_create.png" alt="<%=LNG_CREATE %>" width="18" height="18" border="0"></a></td>
			<td colspan="3"><nobr>&nbsp;<a href="screensaver_type_choose.asp" target="work_area" name="menu_subitem_<%=LNG_SCREENSAVERS %>"><%=LNG_CREATE %></a></nobr></td>
		</tr>
	<% end if%>
	<%if (admin = 1 OR moder = 1 OR screensavers_checked_arr(4) <> "") then%>
		<tr>
			<td background="images/midvertline.gif"><img height="18" src="images/midline.gif" width="18" border="0"></td>
			<td><a href="screensavers.asp" target="work_area"><img class="menu_icon" src="images/menu_icons/screensaver_current.png" alt="<%=LNG_CURRENT %>" width="18" height="18" border="0"></a></td>
			<td colspan="3"><nobr>&nbsp;<a href="screensavers.asp" target="work_area" name="menu_subitem_<%=LNG_SCREENSAVERS %>"><%=LNG_CURRENT %></a></nobr></td>
		</tr>
		<tr>
			<td><img height="18" src="images/endline.gif" width="18" border="0"></td>
			<td><a href="screensavers.asp?draft=1" target="work_area"><img class="menu_icon" src="images/menu_icons/screensaver_draft.png" alt="<%=LNG_DRAFT %>" width="18" height="18" border="0"></a></td>
			<td colspan="3"><nobr>&nbsp;<a href="screensavers.asp?draft=1" target="work_area" name="menu_subitem_<%=LNG_SCREENSAVERS %>"><%=LNG_DRAFT %></a></nobr></td>
		</tr>
	<% end if%>
		</table>
	</div>
</div>
<% end if %>

<% if VIDEO = 1 and (admin = 1 OR moder = 1 OR video_checked <> "") then%>
<div class="group">
	<h3 class="multiple" <% if VIDEO_TRIAL=1 then %>style="background:#efffef" title="<%=LNG_TRIAL_ADDON_DESCRIPTION %><%=TRIAL_DAYS_LEFT %>"<%end if %>><table><tr><td  width="1%"><span class="menu_item"><img class="menu_icon" src="images/menu_icons/videomodule_20.png" alt="<%=LNG_RSS %>" width="20" height="20" border="0"></span></td><td ><span class="menu_text"><span name="menu_item" class="menu_item"><%=LNG_VIDEO_MODULE %></span></span></td></tr></table></h3>
	<div class="folders">
		<table cellspacing="0" cellpadding="0" border="0">
	<%if (admin = 1 OR moder = 1 OR video_checked_arr(0) <> "") then%>
		<tr>
			<td background="images/midvertline.gif"><img height="18" src="images/midline.gif" width="18" border="0"></td>
			<td><a href="edit_videos.asp?type=select" target="work_area"><img class="menu_icon" src="images/menu_icons/videomodule_create.png" alt="<%=LNG_CREATE %>" width="18" height="18" border="0"></a></td>
			<td colspan="3"><nobr>&nbsp;<a href="edit_videos.asp?type=select" target="work_area" name="menu_subitem_<%=LNG_VIDEO_MODULE %>"><%=LNG_CREATE %></a></nobr></td>
		</tr>
	<% end if%>
	<%if (admin = 1 OR moder = 1 OR video_checked_arr(4) <> "") then%>
		<tr>
			<td background="images/midvertline.gif"><img height="18" src="images/midline.gif" width="18" border="0"></td>
			<td><a href="sent_video.asp" target="work_area"><img class="menu_icon" src="images/menu_icons/videomodule_current.png" alt="<%=LNG_SENT %>" width="18" height="18" border="0"></a></td>
			<td colspan="3"><nobr>&nbsp;<a href="sent_video.asp" target="work_area" name="menu_subitem_<%=LNG_VIDEO_MODULE %>"><%=LNG_SENT %></a></nobr></td>
		</tr>
		<tr>
			<td><img height="18" src="images/endline.gif" width="18" border="0"></td>
			<td><a href="draft_video.asp?draft=1" target="work_area"><img class="menu_icon" src="images/menu_icons/videomodule_draft.png" alt="<%=LNG_DRAFT %>" width="18" height="18" border="0"></a></td>
			<td colspan="3"><nobr>&nbsp;<a href="draft_video.asp?draft=1" target="work_area" name="menu_subitem_<%=LNG_VIDEO_MODULE %>"><%=LNG_DRAFT %></a></nobr></td>
		</tr>
	<% end if%>
		</table>
	</div>
</div>
<% end if %>
<% if WP = 1 and (admin = 1 OR moder = 1 OR wallpapers_checked <> "") then%>
<div class="group">
	<h3 class="multiple" <% if WP_TRIAL=1 then %>style="background:#efffef" title="<%=LNG_TRIAL_ADDON_DESCRIPTION %><%=TRIAL_DAYS_LEFT %>"<%end if %>><table><tr><td  width="1%"><span class="menu_item"><img class="menu_icon" src="images/menu_icons/wallpaper_20.png" alt="<%=LNG_RSS %>" width="20" height="20" border="0"></span></td><td ><span class="menu_text"><span name="menu_item" class="menu_item"><%=LNG_WALLPAPERS %></span></span></td></tr></table></h3>
	<div class="folders">
		<table cellspacing="0" cellpadding="0" border="0">
	<%if (admin = 1 OR moder = 1 OR wallpapers_checked_arr(0) <> "") then%>
		<tr>
			<td background="images/midvertline.gif"><img height="18" src="images/midline.gif" width="18" border="0"></td>
			<td><a href="wallpaper_edit.asp" target="work_area"><img class="menu_icon" src="images/menu_icons/wallpaper_create.png" alt="<%=LNG_CREATE %>" width="18" height="18" border="0"></a></td>
			<td colspan="3"><nobr>&nbsp;<a href="wallpaper_edit.asp" target="work_area" name="menu_subitem_<%=LNG_WALLPAPERS %>"><%=LNG_CREATE %></a></nobr></td>
		</tr>
	<% end if%>
	<%if (admin = 1 OR moder = 1 OR wallpapers_checked_arr(4) <> "") then%>
		<tr>
			<td background="images/midvertline.gif"><img height="18" src="images/midline.gif" width="18" border="0"></td>
			<td><a href="wallpapers.asp" target="work_area"><img class="menu_icon" src="images/menu_icons/wallpaper_current.png" alt="<%=LNG_CURRENT %>" width="18" height="18" border="0"></a></td>
			<td colspan="3"><nobr>&nbsp;<a href="wallpapers.asp" target="work_area" name="menu_subitem_<%=LNG_WALLPAPERS %>"><%=LNG_CURRENT %></a></nobr></td>
		</tr>
		<tr>
			<td><img height="18" src="images/endline.gif" width="18" border="0"></td>
			<td><a href="wallpapers.asp?draft=1" target="work_area"><img class="menu_icon" src="images/menu_icons/wallpaper_draft.png" alt="<%=LNG_DRAFT %>" width="18" height="18" border="0"></a></td>
			<td colspan="3"><nobr>&nbsp;<a href="wallpapers.asp?draft=1" target="work_area" name="menu_subitem_<%=LNG_WALLPAPERS %>"><%=LNG_DRAFT %></a></nobr></td>
		</tr>
	<% end if %>
		</table>
	</div>
</div>
<% end if %>


<% if LS = 1 and (admin = 1 OR moder = 1 OR lockscreens_checked <> "") then%>
<div class="group">
	<h3 class="multiple" <% if LS_TRIAL=1 then %>style="background:#efffef" title="<%=LNG_TRIAL_ADDON_DESCRIPTION %><%=TRIAL_DAYS_LEFT %>"<%end if %>><table><tr><td  width="1%"><span class="menu_item"><img class="menu_icon" src="images/menu_icons/wallpaper_20.png" alt="<%=LNG_LOCKSCREEN %>" width="20" height="20" border="0"></span></td><td ><span class="menu_text"><span name="menu_item" class="menu_item"><%=LNG_LOCKSCREENS %></span></span></td></tr></table></h3>
	<div class="folders">
		<table cellspacing="0" cellpadding="0" border="0">
	<%if (admin = 1 OR moder = 1 OR lockscreens_checked_arr(0) <> "") then%>
		<tr>
			<td background="images/midvertline.gif"><img height="18" src="images/midline.gif" width="18" border="0"></td>
			<td><a href="lockscreen_edit.asp" target="work_area"><img class="menu_icon" src="images/menu_icons/lockscreen_create.png" alt="<%=LNG_CREATE %>" width="18" height="18" border="0"></a></td>
			<td colspan="3"><nobr>&nbsp;<a href="lockscreen_edit.asp" target="work_area" name="menu_subitem_<%=LNG_LOCKSCREENS %>"><%=LNG_CREATE %></a></nobr></td>
		</tr>
	<% end if%>
	<%if (admin = 1 OR moder = 1 OR lockscreens_checked_arr(4) <> "") then%>
		<tr>
			<td background="images/midvertline.gif"><img height="18" src="images/midline.gif" width="18" border="0"></td>
			<td><a href="wallpapers.asp" target="work_area"><img class="menu_icon" src="images/menu_icons/wallpaper_current.png" alt="<%=LNG_CURRENT %>" width="18" height="18" border="0"></a></td>
			<td colspan="3"><nobr>&nbsp;<a href="lockscreens.asp" target="work_area" name="menu_subitem_<%=LNG_LOCKSCREENS %>"><%=LNG_CURRENT %></a></nobr></td>
		</tr>
		<tr>
			<td><img height="18" src="images/endline.gif" width="18" border="0"></td>
			<td><a href="lockscreens.asp?draft=1" target="work_area"><img class="menu_icon" src="images/menu_icons/wallpaper_draft.png" alt="<%=LNG_DRAFT %>" width="18" height="18" border="0"></a></td>
			<td colspan="3"><nobr>&nbsp;<a href="wallpapers.asp?draft=1" target="work_area" name="menu_subitem_<%=LNG_LOCKSCREENS %>"><%=LNG_DRAFT %></a></nobr></td>
		</tr>
	<% end if %>
		</table>
	</div>
</div>
<% end if %>

<% if IM = 1 and (admin = 1 OR moder = 1 OR im_checked<>"" OR cc_checked<>"") then%>
<div class="group">
	<h3 class="multiple" <% if IM_TRIAL=1 then %>style="background:#efffef" title="<%=LNG_TRIAL_ADDON_DESCRIPTION %><%=TRIAL_DAYS_LEFT %>"<%end if %>><table><tr><td  width="1%"><span class="menu_item"><img class="menu_icon" src="images/menu_icons/im_20.png" alt="<%=LNG_INSTANT_MESSAGES %>" width="20" height="20" border="0"></span></td><td ><span class="menu_text"><span name="menu_item" class="menu_item"><%=LNG_INSTANT_SEND %></span></span></td></tr></table></h3>
	<div class="folders">
		<table cellspacing="0" cellpadding="0" border="0">
		<%if (admin = 1 OR moder = 1 OR im_checked_arr(0) <> "") then%>
		<tr>
			<td background="images/midvertline.gif"><img height="18" src="images/midline.gif" width="18" border="0"></td>
			<td><a href="edit_alert_db.asp?instant=1" target="work_area"><img class="menu_icon" src="images/menu_icons/im_create.png" alt="<%=LNG_CREATE %>" width="18" height="18" border="0"></a></td>
			<td colspan="3"><nobr>&nbsp;<a href="edit_alert_db.asp?instant=1" target="work_area" name="menu_subitem_<%=LNG_INSTANT_MESSAGES %>"><%=LNG_CREATE %></a></nobr></td>
		</tr>
		<%end if %>
		<%if (admin = 1 OR moder = 1 OR im_checked_arr(4) <> "") then%>
		<tr>
			<td background="images/midvertline.gif"><img height="18" src="images/midline.gif" width="18" border="0"></td>
			<td><a href="instant_messages.asp" target="work_area"><img class="menu_icon" src="images/menu_icons/im_send.png" alt="<%=LNG_SEND_NOW %>" width="18" height="18" border="0"></a></td>
			<td colspan="3"><nobr>&nbsp;<a href="instant_messages.asp" target="work_area" name="menu_subitem_<%=LNG_INSTANT_MESSAGES %>"><%=LNG_SEND_NOW %></a></nobr></td>
		</tr>
		<%end if %>
		<%if (admin = 1 OR moder = 1 OR cc_checked_arr(4) <> "") then%>
		<tr>
			<td><img height="18" src="images/endline.gif" width="18" border="0"></td>
			<td><a href="color_codes.asp" target="work_area"><img class="menu_icon" src="images/menu_icons/im_codes.png" alt="<%=LNG_COLOR_CODES %>" width="18" height="18" border="0"></a></td>
			<td colspan="3"><nobr>&nbsp;<a href="color_codes.asp" target="work_area" name="menu_subitem_<%=LNG_INSTANT_MESSAGES %>"><%=LNG_COLOR_CODES %></a></nobr></td>
		</tr>
		<%end if %>
        <%if (admin = 1 OR moder = 1) then%>
		<tr>
			<td><img height="18" src="images/endline.gif" width="18" border="0"></td>
			<td><a href="create_button_ui.asp" target="work_area"><img class="menu_icon" src="images/menu_icons/create_shortcut.png" alt="<%=LNG_COLOR_CODES %>" width="18" height="18" border="0"></a></td>
			<td colspan="3"><nobr>&nbsp;<a href="create_button_ui.asp" target="work_area" name="menu_subitem_<%=LNG_INSTANT_MESSAGES %>"><%=LNG_CREATE_SHORTCUT %></a></nobr></td>
		</tr>
		<%end if %>
		</table>
	</div>
</div>
<% end if %>

<% if WEBPLUGIN=1 then %>

<div class="group">
	<h3 class="single"><table><tr><td ><a href="WebpluginList.aspx" class="menu_item" target="work_area"><img class="menu_icon" src="images/menu_icons/web_alert.png" alt="Website Alert Module" width="20" height="20" border="0"></a></td><td ><a href="WebpluginList.aspx" class="menu_item" target="work_area"><span name="menu_item" class="menu_text"><%=LNG_BANNERS_TITLE %></span></a></td></tr></table></h3>
</div>
<% end if %>


<% if Session("db_use_backup") <> 1 and AD<>3 then%>
<div class="group">
	<h3 class="multiple"><table><tr><td  width="1%"><span class="menu_item"><img class="menu_icon" src="images/menu_icons/users_20.png" alt="<%=LNG_USERS %>&nbsp;<%=LNG_AND %>&nbsp;<%=LNG_GROUPS %>" width="20" height="20" border="0"></span></td><td ><span class="menu_text"><span name="menu_item" class="menu_item"><% Response.Write(LNG_USERS&" "&LNG_AND&" "&LNG_GROUPS) %></span></span></td></tr></table></h3>
	<div class="folders">
		<table cellspacing="0" cellpadding="0" border="0">
		<tr>
			<td background="images/midvertline.gif"><img height="18" src="images/midline.gif" width="18" border="0"></td>
			<td><a href="users.asp" target="work_area"><img class="menu_icon" src="images/menu_icons/users_20.png" alt="<%=LNG_USERS %>" width="18" height="18" border="0"></a></td>
			<td colspan="3"><nobr>&nbsp;<a href="users.asp" target="work_area" name="menu_subitem_<% Response.Write(LNG_USERS&" "&LNG_AND&" "&LNG_GROUPS) %>"><%=LNG_USERS %></a></nobr></td>
		</tr>
		<tr>
			<td background="images/midvertline.gif"><img height="18" src="images/midline.gif" width="18" border="0"></td>
			<td><a href="admin_groups.asp" target="work_area"><img class="menu_icon" src="images/menu_icons/ipgroups_20.png" alt="<%=LNG_GROUPS %>" width="18" height="18" border="0"></a></td>
			<td colspan="3"><nobr>&nbsp;<a href="admin_groups.asp" target="work_area" name="menu_subitem_<% Response.Write(LNG_USERS&" "&LNG_AND&" "&LNG_GROUPS) %>"><%=LNG_GROUPS %></a></nobr></td>
		</tr>
		<tr>
			<td><img height="18" src="images/endline.gif" width="18" border="0"></td>
			<td><a href="admin_ip_groups.asp" target="work_area"><img class="menu_icon" src="images/menu_icons/ipgroups_20.png" alt="<%=LNG_IP_GROUPS %>" width="18" height="18" border="0"></a></td>
			<td colspan="3"><nobr>&nbsp;<a href="admin_ip_groups.asp" target="work_area" name="menu_subitem_<% Response.Write(LNG_USERS&" "&LNG_AND&" "&LNG_GROUPS) %>"><%=LNG_IP_GROUPS %></a></nobr></td>
		</tr>
		</table>
	</div>
</div>
<%end if%>


<%
if Session("db_use_backup") <> 1 then
	if(AD=3) then
		if(admin=1) then
 %>
<div class="group">
	<h3 class="multiple" <% if AD_TRIAL=1 then %>style="background:#efffef" title="<%=LNG_TRIAL_ADDON_DESCRIPTION %><%=TRIAL_DAYS_LEFT %>"<%end if %>><table><tr><td  width="1%"><span class="menu_item"><img class="menu_icon" src="images/menu_icons/domains_20.png" alt="<%=LNG_ACTIVE_DIRECTORY %>" width="20" height="20" border="0"></span></td><td ><span class="menu_text"><span name="menu_item" class="menu_item"><%=LNG_ACTIVE_DIRECTORY %></span></span></td></tr></table></h3>
	<div class="folders">
		<table cellspacing="0" cellpadding="0" border="0">
		<tr>
			<td background="images/midvertline.gif"><img height="18" src="images/midline.gif" width="18" border="0"></td>
			<td><a href="admin_domains.asp" target="work_area"><img class="menu_icon" src="images/menu_icons/domains_20.png" alt="<%=LNG_DOMAINS %>" width="18" height="18" border="0"></a></td>
			<td colspan="3"><nobr>&nbsp;<a href="admin_domains.asp" target="work_area" name="menu_subitem_<%=LNG_ACTIVE_DIRECTORY %>"><%=LNG_DOMAINS %></a></nobr></td>
		</tr>
		<tr>
			<td background="images/midvertline.gif"><img height="18" src="images/midline.gif" width="18" border="0"></td>
			<td><a href="synchronizations.asp" target="work_area"><img class="menu_icon" src="images/menu_icons/synchronizations.png" alt="<%=LNG_SYNCHRONIZATIONS %>" width="18" height="18" border="0"></a></td>
			<td colspan="3"><nobr>&nbsp;<a href="synchronizations.asp" target="work_area" name="menu_subitem_<%=LNG_ACTIVE_DIRECTORY %>"><%=LNG_SYNCHRONIZATIONS %></a></nobr></td>
		</tr>
		<tr>
			<td><img height="18" src="images/endline.gif" width="18" border="0"></td>
			<td><a href="ou_new_interface.asp" target="work_area"><img class="menu_icon" src="images/menu_icons/organization_20.png" alt="<%=LNG_ORGANIZATION %>" width="18" height="18" border="0"></a></td>
			<td colspan="3"><nobr>&nbsp;<a href="ou_new_interface.asp" target="work_area" name="menu_subitem_<%=LNG_ACTIVE_DIRECTORY %>"><%=LNG_ORGANIZATION %></a></nobr></td>
		</tr>
		</table>
	</div>
</div>
<%	
		end if
	end if
end if
%>

<!-- Organization units -->
<%
if Session("db_use_backup") <> 1 then
	if(AD=3 and admin<>1 and moder<> 1) then 
%>
<div class="group">
	<h3 class="single" <% if AD_TRIAL=1 then %>style="background:#efffef" title="<%=LNG_TRIAL_ADDON_DESCRIPTION %><%=TRIAL_DAYS_LEFT %>"<%end if %>>
		<table>
			<tr>
				<td >
					<a href="ou_new_interface.asp" class="menu_item" target="work_area">
							<img class="menu_icon" src="images/menu_icons/organization_20.png" alt="<%=LNG_ORGANIZATION %>" width="20" height="20" border="0">
					</a>
				</td>
				<td >
					<a href="ou_new_interface.asp" class="menu_item" target="work_area">
						<span class="menu_text"><%=LNG_ORGANIZATION %></span>
					</a>
				</td>
			</tr> 
		</table>
	</h3>
</div>
<%
	end if
end if

if Session("db_use_backup") <> 1 and (admin = 1 OR moder = 1) then
%>
<div class="group">
	<h3 class="multiple"><table><tr><td  width="1%"><span class="menu_item"><img class="menu_icon" src="images/menu_icons/editors_20.png" alt="<%=LNG_EDITORS %>" width="20" height="20" border="0"></span></td><td ><span class="menu_text"><span name="menu_item" class="menu_item"><%=LNG_EDITORS %></span></span></td></tr></table></h3>
	<div class="folders">
		<table cellspacing="0" cellpadding="0" border="0">
		<tr>
			<td background="images/midvertline.gif"><img height="18" src='<%  if( moder = 1 ) then Response.Write "images/endline.gif" else Response.Write "images/midline.gif" end if %>' width="18" border="0"></td>
			<td><a href="admin_admin.asp" target="work_area"><img class="menu_icon" src="images/menu_icons/editors_20.png" alt="<%=LNG_EDITORS_PUBLISHERS_LIST %>" width="18" height="18" border="0"></a></td>
			<td colspan="3"><nobr>&nbsp;<a href="admin_admin.asp" target="work_area" name="menu_subitem_<%=LNG_EDITORS %>"><%=LNG_EDITORS_PUBLISHERS_LIST %></a></nobr></td>
		</tr>
		<% if admin = 1 and moder = 0 then %>
		<tr>
			<td><img height="18" src="images/endline.gif" width="18" border="0"></td>
			<td><a href="policy_list.asp" target="work_area"><img class="menu_icon" src="images/menu_icons/policies_20.png" alt="<%=LNG_POLICIES %>" width="18" height="18" border="0"></a></td>
			<td colspan="3"><nobr>&nbsp;<a href="policy_list.asp" target="work_area" name="menu_subitem_<%=LNG_EDITORS %>"><%=LNG_POLICIES %></a></nobr></td>
		</tr>
		<% end if %>
		</table>
	</div>
</div>
<%
end if

if Session("db_use_backup") <> 1 and ConfEnableIPRanges = 1 and AD=3 and (admin = 1 OR moder = 1 OR ip_groups_checked <> "") then %>
	<div class="group">
		<h3 class="single">
			<table>
				<tr>
					<td>
						<a href="admin_ip_groups.asp" class="menu_item" target="work_area">
							<img class="menu_icon" src="images/menu_icons/ipgroups_20.png" alt="<%=LNG_IP_GROUPS%>"
								width="20" height="20" border="0"></a>
					</td>
					<td>
						<a href="admin_ip_groups.asp" class="menu_item" target="work_area"><span name="menu_item" class="menu_text">
							<%=LNG_IP_GROUPS%>
						</span></a>
					</td>
				</tr>
			</table>
		</h3>
	</div>
<% end if 
if(admin = 1 OR moder = 1 OR feedback_checked <> "") then
%>
<div class="group">
	<h3 class="single"><table><tr><td ><a href="feedback_show.asp" class="menu_item" target="work_area"><img class="menu_icon" src="images/menu_icons/feedback_20.png" alt="Statistic" width="20" height="20" border="0"></a></td><td ><a href="feedback_show.asp" class="menu_item" target="work_area"><span name="menu_item" class="menu_text"><%=LNG_FEEDBACK %></span></a></td></tr></table></h3>
</div>
<%end if
if (admin = 1 OR moder = 1 OR statistics_checked <> "") then

    statTitle = LNG_STATISTICS_REPORTS
    if EXTENDED_STATISTICS = 1 then
        statTitle = LNG_EXTENDED_STAT
    end if
    
	%>
<div class="group">
	<h3 class="single"><table><tr><td ><a href="statistics_da_overall.asp" class="menu_item" target="work_area"><img class="menu_icon" src="images/menu_icons/statistics_20.png" alt="Statistic" width="20" height="20" border="0"></a></td><td ><a href="statistics_da_overall.asp" class="menu_item" target="work_area"><span name="menu_item" class="menu_text"><%=statTitle %></span></a></td></tr></table></h3>
</div>
<%if(DA=1) then %>
<div class="group">
	<h3 class="single"><table><tr><td ><a href="view_tb_stat.asp" class="menu_item" target="work_area"><img class="menu_icon" src="images/menu_icons/statistics_20.png" alt="Statistic" width="20" height="20" border="0"></a></td><td ><a href="view_tb_stat.asp" class="menu_item" target="work_area"><span class="menu_text">Toolbar Statistics</span></a></td></tr></table></h3>
</div>
<% end if
end if

if (admin = 1 OR moder = 1) and ConfCustomSkinEnabled = 1 then
%>
<div class="group">
	<h3 class="single"><table><tr><td ><a href="skins.asp" class="menu_item" target="work_area"><img class="menu_icon" src="images/menu_icons/skins_20.png" alt="" width="20" height="20" border="0"></a></td><td ><a href="skins.asp" class="menu_item" target="work_area"><span class="menu_text"><%=LNG_SKINS %></span></a></td></tr></table></h3>
</div>
<%
end if

	Set menuDoc=Server.CreateObject("Microsoft.XMLDOM")
	menuDoc.async=false
	menuDoc.load(Server.MapPath("menu/menu.xml"))
	Dim menuSettings
	
	if menuDoc.parseError.errorcode=0 then

		Set menuRoot = menuDoc.selectSingleNode("menu")

		Set menuItems = menuRoot.selectNodes("item")
		Set menuSettingsElement = menuRoot.selectSingleNode("settings")

		if not menuSettingsElement Is Nothing then
			Set menuSettings = menuSettingsElement.selectNodes("item")
			if menuSettings.length=0 then
				menuSettings = null
			end if	
		end if
		
		For i=0 to menuItems.length-1 

			Set caption = menuItems(i).attributes.getNamedItem("caption")
			Set img = menuItems(i).attributes.getNamedItem("img")
			Set href = menuItems(i).attributes.getNamedItem("href")
			%>
				<div class="group">
				<h3 class="multiple"><table>
				<tr>
					<td >
					<a  <%if (VarType(href) > 1) then%> href="<%=href.nodeValue %>" <%end if%> class="menu_item" target="work_area">
						<img src="menu/<%=img.nodeValue %>" alt="<%=Eval(caption.nodeValue) %>" width="20" height="20" border="0">
					</a>
					</td>
					<td >
						<a <%if (VarType(href) > 1) then%> href="<%=href.nodeValue %>" <%end if%> class="menu_item" target="work_area">
							<span class="menu_text">
								<%=Eval(caption.nodeValue) %>
							</span>
						</a>
					</td>
				</tr>		
				</table></h3>
			<% 
			
			Set subMenuItems = menuItems(i).selectNodes("item")
			if (subMenuItems.length > 0) then
			
				%>
				<div class="folders">
					<table cellspacing=0 cellpadding=0 border=0>
				<%
				For j=0 to subMenuItems.length-1 

					Set subItemCaption = subMenuItems(j).attributes.getNamedItem("caption")
					Set subItemImg = subMenuItems(j).attributes.getNamedItem("img")
					Set subItemHref = subMenuItems(j).attributes.getNamedItem("href")
				%>
									<tr>
										<td background="images/<%if j<>subMenuItems.length-1 then Response.Write("midvertline.gif") end if%>"><img height=18 src="images/<%if j<>subMenuItems.length-1 then Response.Write("midline.gif") else Response.Write("endline.gif") end if%>" width=18 border=0></td>
										<td><a href="<%=subItemHref.nodeValue %>" target="work_area"><img src="menu/<%=subItemImg.nodeValue %>" alt="<%=Eval(subItemCaption.nodeValue) %>" width="18" height="18" border="0"></a></td>
										<td colspan=3><nobr>&nbsp;<a href="<%=subItemHref.nodeValue %>" target="work_area"><%=Eval(subItemCaption.nodeValue) %></a></nobr></td>
									</tr>
				<%	
				Next 
				%>
					</table>
				</div>
				</div>
				<% 
			end if
		Next 
		
	end if
%>
<% if Session("db_use_backup") <> 1 then %>
<div class="group">
	<h3 class="multiple"><table><tr><td ><span class="menu_item"><img class="menu_icon" src="images/menu_icons/settings_20.png" alt="" width="20" height="20" border="0"/></span></td><td ><span class="menu_text"><span name="menu_item" class="menu_item"><%=LNG_SETTINGS %></span></span></td></tr></table></h3>
	<div class="folders">
      <table cellspacing="0" cellpadding="0" border="0">
      <tr>
    <% if (admin = 1) then %>
    	  <td background="images/midvertline.gif"><img height="18" src="images/midline.gif" width="18" border="0"/></td>
    <% else %>
		  <td background="images/endline.gif"><img height="18" src="images/endline.gif" width="18" border="0"/></td>
	<% end if %>
		  <td></td>
		  <td colspan="3"><nobr>&nbsp;<a href="settings_edit_profile.asp" target="work_area" name="menu_subitem_<%=LNG_SETTINGS %>"><%=LNG_PROFILE_SETTINGS%></a></nobr></td>
	  </tr>
	<% if (admin = 1) then %>
	  <% if BLOG=1 OR SOCIAL_MEDIA=1 OR SOCIAL_LINKEDIN=1 then %>
	  <tr>
	      <td background="images/midvertline.gif"><img height="18" src="images/midline.gif" width="18" border="0"></td>
		  <td></td>
		  <td colspan="3"><nobr>&nbsp;<a href="settings_social_settings.asp" target="work_area" name="menu_subitem_<%=LNG_SETTINGS %>"><%=LNG_SOCIAL_SETTINGS %></a></nobr></td>
	  </tr>
	  <% end if %>
      <tr>
	      <td background="images/endline.gif"><img height="18" src="images/endline.gif" width="18" border="0"></td>
		  <td></td>
		  <td colspan="3"><nobr>&nbsp;<a href="settings_common.asp" target="work_area" name="menu_subitem_<%=LNG_SETTINGS %>"><%=LNG_COMMON_SETTINGS %></a></nobr></td>
	  </tr>	  
	  
		<%
		if isObject(menuSettings) then
			if menuSettings.length<>0 then				
				For i=0 to menuSettings.length-1 
					Set caption = menuSettings(i).attributes.getNamedItem("caption")
					Set img = menuSettings(i).attributes.getNamedItem("img")
					Set href = menuSettings(i).attributes.getNamedItem("href")
				%>
					<tr>
						<td background="images/endline.gif"><img height="18" src="images/<% if i = menuSettings.length-1 then Response.Write "endline.gif" else Response.Write "midline.gif" end if%>" width="18" border="0"></td>
						<td></td>
						<td colspan="3"><nobr>&nbsp;<a href="<%=href.nodeValue %>" target="work_area"><%=Eval(caption.nodeValue) %></a></nobr></td>
					</tr>
				<% Next 
			end if
		end if
		%>

	<% end if%>
		
      </table>
	</div>
<% end if%>
</div>
</div>
<!--[if IE 6]>
<script>
	$(document).ready(function(){
		$('#accordion').sortable('disable');
	});
</script>
<![endif]-->
</body>
</html>
<!-- #include file="db_conn_close.asp" -->