﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Parameters;

namespace DeskAlertsDotNet.Pages
{
    using Resources;

    public partial class LanguageList : DeskAlertsBaseListPage
    {
        private string langCodeString;
        protected override void OnLoad(EventArgs e)
        {
            
            headerTitle.Text = resources.LNG_ALERT_LANGUAGES;

            langCodeString = Settings.Content["ConfDefaultLanguage"];

            addButton.Text = resources.LNG_ADD;
            if (!string.IsNullOrEmpty(Request["lang"]))
            {
                langCodeString = Request["lang"];

                langСode.Value = langCodeString;
                addButton.Text = resources.LNG_EDIT;

                object langNameObj =
                    dbMgr.GetScalarByQuery("SELECT [name] FROM [languages_list] WHERE [code] = '" + langCodeString + "'");

                if (langNameObj != null)
                    langName.Value = langNameObj.ToString();
            }


            valueCell.Text = resources.LNG_VALUE;
            curValueCell.Text = resources.LNG_CURRENT_ENGLISH_TEXT;
            base.OnLoad(e);

          // int cnt = Content.Count < (Offset + Limit) ? Content.Count : Offset + Limit;

           
            for (int i = 0; i < this.Content.Count; i++)
            {
                TableRow newRow = new TableRow();

                DataRow row = this.Content.GetRow(i);
                string key = row.GetString("name");
                string value = row.GetString(langCodeString);
                string defaultValue = row.GetString("EN");

            

                //  string inputHtml = string.Format("<input type='text' id='{0}' name='{0}' value='{1}' size='40'>", key, value);

                TextBox input = new TextBox();
                input.ClientIDMode = ClientIDMode.Static;
                input.ID = key;
                input.Text = value;
                TableCell valueContentCell = new TableCell();

                valueContentCell.Controls.Add(input);

                newRow.Cells.Add(valueContentCell);

                TableCell curValueContentCell = new TableCell();
                curValueContentCell.Text = defaultValue;

                newRow.Cells.Add(curValueContentCell);

                contentTable.Rows.Add(newRow);
            }
        }

        #region DataProperties
        protected override string DataTable
        {
            get
            {

                //return your table name from this property
                return "languages_list";
            }
        }

        protected override string DefaultSortingOrder
        {
            get { return "ASC"; }
        }

        protected override string[] Collumns
        {
            get
            {

                //return array of collumn`s names of table from data table
                return new string[] { "name", "code" };

            }
        }

        protected override string DefaultOrderCollumn
        {
            //return collumn for default order
            get { return "id"; }
        }

        /*
         * 
         * If you want to add condition for data selection override property ConditionSqlString 
         * Also you can implement your custom query by overriding properties CustomQuery and CustomCountQuery
         *  
         */
        #endregion

        #region Interface properties

        protected override IButtonControl[] DeleteButtons
        {
            get
            {
                return new IButtonControl[] {  };
            }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl NoRowsDiv
        {
            get { return NoRows; }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl TableDiv
        {
            get { return mainTableDiv; }
        }

        protected override string ContentName
        {
            get
            {
                //return content name here
                return "DeskAlerts Content List";
            }
        }

        #endregion
    }
}