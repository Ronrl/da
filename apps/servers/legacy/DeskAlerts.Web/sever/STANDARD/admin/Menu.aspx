﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Menu.aspx.cs" Inherits="DeskAlertsDotNet.Pages.Menu" %>
<%@ Import NameSpace="Resources" %>
<%@ Import NameSpace="DeskAlertsDotNet.Parameters" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
<link href="css/style9.css?v=9.1.12.28" rel="stylesheet" type="text/css" />
<link href="css/menu.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="jscripts/json2.js"></script>
<script type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script type="text/javascript" src="jscripts/shortcut.js"></script>
<script type="text/javascript" src="jscripts/jquery/jquery.cookies.2.2.0.min.js"></script>
<script type="text/javascript">
	function showHintDiv(id) {
		var el = document.getElementById(id);
		if(el) el.style.display="";
	}

	function hideHintDiv(id){
		var el = document.getElementById(id);
		if(el) el.style.display="none";
	}
	function setCookieIfClicked(id) {
	        var el = document.getElementById(id);
	        if (el) el.style.display = "none";
	}

	$(function () {
		shortcut.add("Alt+Shift+A",function(e) {
			e.preventDefault();
			var madness = setInterval(function(){
			var h = $('h3.multiple');
			var el = h[Math.floor(Math.random()*h.length)];
			$(el).click();
			},50);
			setTimeout(function(){clearInterval(madness);},10000);
		});

		var root = $('#accordion');

		$('> *', root).each(function(index) {
			this.id = 'm-' + index;
		});

		$.get("set_parameters.asp?name=getMenuOrder", function (data) {
		    if (data != "") {
		        $.each(data.split(','), function () {
		            $('#' + this).appendTo(root);
		        });
		    };
		});

		$.get("set_parameters.asp?name=getCloseHintStatus", function (data) {
		    if (data == 1) {
		        document.getElementById('close_if_clicked').style.display = 'none';
		        document.getElementById('spacer_to_hide').style.display = 'none';
		    };
		});

		$('#close_hint').click(function() {
		    $.get("set_parameters.asp?name=setCloseHintStatus&closeHintStatus=1");
			document.getElementById('close_if_clicked').style.display = 'none';
	        document.getElementById('spacer_to_hide').style.display = 'none';
		});

	    $('#restoreMenuOrder').button({
	        icons: {
	            primary: "ui-icon-arrowrefresh-1-s"
	        },
	        text: false
	    }).click(function() {
	        $.get("set_parameters.asp?name=setMenuOrder&menuOrder=''");
			for (var id = 0; true; id++) {
				if ($('#m-' + id).length > 0) {
					$('#m-' + id).appendTo(root);
				} else {
					break;
				}
			}
	    });

		$('.buy_addons').find('a').button({
			icons: {
				primary: "ui-icon-cart"
			}
		}).css("margin", "3px");
		$('.contact_us').find('a').button({
			icons: {
				primary: "ui-icon-mail-closed"
			}
		}).css("margin", "3px");

		$('#accordion').accordion({
			header: "> div > h3",
			heightStyle: "content",
            collapsible: true,
            autoHeight: false,
            disabled: true,
            activate: function (event, ui) {
                window.parent.parent.onChangeMenuHeight($("#mainContent").height(), true, false, false);
            }
		}).sortable({
			delay: 150,
			distance: 10,
			tolerance: "pointer",
			containment: "#sortable_container",
			update: function(event, ui) {
			    var order = $(this).sortable('toArray');
				$.get("set_parameters.asp?name=setMenuOrder&menuOrder=" + order + "");
			},
			axis: "y",
			stop: function(event, ui) {
				ui.item.children("h3").triggerHandler("focusout");
			}
		});

        jQuery('#accordion h3.ui-accordion-header').click(function () {
            jQuery(this).next().toggle(300, function () {
                window.parent.onChangeMenuHeight($("#mainContent").height(), true, false, false);
            });
        });

		$.get("set_parameters.asp?name=getMultiple", function (data) {
		    if (data != "") {
		        $.each(data.split('&'), function () {
		            var id = this;
		            var index = $('#' + id).index();
		            $("#accordion").accordion("option", "active", index);
		        });
		    };
		});

		$('.single').unbind().bind("click", function() {
			$(this).find("a").get(1).click();
		});

		$('.multiple').bind("click", function() {
			var active = "";
			$('.multiple.ui-state-active').each(function() {
				if (active) { active += "&"; }
				active += $(this).parent().attr('id');
			});

			$.get("set_parameters.asp?name=setMultiple&multiple=" + active);
        });         
    });    
</script>
</head>

<body id="mainContent" unselectable='on' onselectstart='return false;'  class="menu_body" style="-webkit-user-select: none; overflow-x: hidden; position: relative; -khtml-user-select: none;   -moz-user-select: none; -o-user-select: none; user-select: none;" scroll="yes">
    <table cellpadding="0" cellspacing="0" border="0" valign="top" width="100%">
    <tr><td  colspan="2">
    <div class="folders" style="text-align: center; vertical-align: middle;padding-top:40px;padding-bottom:15px">
        <div>
            <div>
                	<div style="position: absolute; z-index:2; left: 20px; border-top: 1px solid #ddd; background: #eee; height:5px; text-align: center;">
	               	&nbsp;
                	</div>
            <div runat="server" id="progressBar">&nbsp;</div>
        </div>
			<div style="position:absolute;top:20px;left:20px;width:192px;text-align:center">
			<span style="font-family: verdana;color: #555" runat ="server" id="activeUsersLimitLabel"></span>:&nbsp;<span runat="server" id="counter"></span>
		</div>
            <div runat="server" id="hintDiv" style="z-index:3;background-color: white; width: 192px; border: 1px solid #eee; position: absolute; left: 20px; display: none; padding:3px"></div>
        </div>
     
     <div runat="server" id="trialDiv" onmouseover="showHintDiv('trialHintDiv');" onmouseout="hideHintDiv('trialHintDiv');">
      <br />
     <b runat="server" id="trialExpireLabel" style="color: #ff0000;"> 
     </b>
     <div runat="server" id="expiredLabel" style="color: #ff0000; font-weight: bold;"><br/></div>
            
     </div>
      <div runat="server" id="scheduledTasksDiv" class="scheduled_tasks_div">

      </div>
       </div>
    </td></tr>

    <% if (Config.Data.IsDemo ) {%>
    <tr><td class="menu_spacer buy_addons_spacer" height="1" colspan="2"></td></tr>
    <tr><td class="menu contact_us" colspan="2">
	    <div style="text-align: center; vertical-align: middle; padding:5px 10px" class="folders">
		    <a href="contact_deskalerts.asp" class="menu_item contact_us_button" target="work_area">Get Free Quote</a>
	    </div>
    </td></tr>
    <% } %>

    <% if (trial>0) { %>
    <tr><td class="menu_spacer buy_addons_spacer" height="1" colspan="2"></td></tr>
    <tr><td class="menu contact_us" colspan="2">
	    <div style="text-align: center; vertical-align: middle; padding:5px 10px" class="folders">
		    <a href="request_quote.asp" class="menu_item contact_us_button" target="work_area">Get Free Quote</a>
	    </div>
    </td></tr>
    <% }%>

    <tr id="spacer_to_hide" style="display:none"><td class="menu_spacer" height="1" colspan="2"></td></tr>
    <tr id="close_if_clicked"  style="display:none" class="hints"><td> <div style="font-size:12px; text-align:center; margin:2px"><%= resources.LNG_CLICK_AND_DRAG %></div></td>
    <td style="padding-right:10px"><div style="vertical-align:middle; cursor: pointer;  font-weight:bold;" id="close_hint" onclick="">Hide</div></td>
    </tr>
    <tr><td class="menu_spacer" height="1" colspan="2"></td></tr>    

    </table>
    <div id="sortable_container" style="overflow-x: hidden; position: relative;">
        <div runat="server" id="accordion" style="overflow-x: hidden; position: relative;">
    </div>
    </div>
</body>
</html>
