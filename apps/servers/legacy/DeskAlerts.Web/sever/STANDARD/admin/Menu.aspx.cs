﻿using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Licensing;
using DeskAlertsDotNet.Managers;
using DeskAlertsDotNet.Models;
using DeskAlertsDotNet.Parameters;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DeskAlertsDotNet.Pages
{
    using Resources;

    public partial class Menu : System.Web.UI.Page
    {
        User curUser;
        float currentUsersCount;
        float totalUsersCount;
        public long trial;
        public long trialExpire;
        DBManager dbMgr;
        private MenuManager menuMgr = MenuManager.Default;
        protected void Page_Load(object sender, EventArgs e)
        {
            dbMgr = new DBManager();
            curUser = UserManager.Default.CurrentUser;
            currentUsersCount = LicenseManager.Default.GetCurrentUsersCount();
            totalUsersCount = LicenseManager.Default.GetLicenseCount(dbMgr);
            trial = LicenseManager.Default.GetTrialTime(dbMgr);
            trialExpire = LicenseManager.Default.GetServerLifeTime();
            int proc = 0;

            if (currentUsersCount > 0 && totalUsersCount > 0)
                proc = (int)(currentUsersCount / totalUsersCount * 100);

            if (proc > 101)
                proc = 101;
            activeUsersLimitLabel.InnerText = resources.LNG_ACTIVE_USERS_LICENSE_LIMIT;

            counter.InnerText = totalUsersCount <= 0 ? currentUsersCount + "/-" : currentUsersCount + "/" + totalUsersCount;

            string progressBarStyle = "height:4px; position: absolute; z-index:3; left: 20px; border: 1px solid {border_color}; border-right: 0px none #000000; width: {width}px;display:{display_style}; text-align: center; background-color: {bg_color}; float:left;";
            string style = "block";
            if (proc == 0.0)
            {
                style = "none";
            }

            string hint = resources.LNG_NORMAL_LICENSE_UTILIZATION_LEVEL;

            string color = "";
            string border_color = "";
            if (proc < 60)
            {
                color = "#10b12e";
                border_color = "#398439";

            }
            else if (proc >= 60 && proc < 90)
            {
                color = "#efef53";
                border_color = "#c7ca20";
            }
            else if (proc >= 90 && proc <= 100)
            {
                hint = resources.LNG_YOUR_LICENSE_LIMIT_IS_ABOUT_TO_EXCEED;
                color = "#ff9234";
                border_color = "#c76716";
            }
            else if (proc > 100)
            {
                hint = resources.LNG_LICENSE_EXCEED_DESC;
                color = "#ff0000";
                border_color = "#5a0000";
            }

            progressBarStyle = progressBarStyle.Replace("{display_style}", style)
                .Replace("{bg_color}", color)
                .Replace("{border_color}", border_color)
                .Replace("{width}", (proc * 1.9).ToString());

            progressBar.Attributes.Add("style", progressBarStyle);

            hintDiv.InnerText = hint;



            if (trial > 0 && trialExpire > -1)
            {
                trialDiv.Visible = true;
                //  trial = (int)(trial / 60 / 60 / 24);
                double trialExpireInDays = ((double)trialExpire / 60 / 60 / 24);
                string trialExpireLabelString = resources.LNG_TRIAL_EXPIRES_IN_DAYS;

                if (trial > trialExpireInDays)
                {
                    int trialDays = (int)Math.Round(trial - trialExpireInDays);

                    if (trialExpireLabelString.IndexOf("%d") > -1)
                        trialExpireLabelString = trialExpireLabelString.Replace("%d", trialDays.ToString());
                    else
                        trialExpireLabelString += (": " + trialDays);


                    trialExpireLabel.InnerHtml = trialExpireLabelString;
                }
                else
                {
                    expiredLabel.InnerHtml = resources.LNG_TRIAL_HAS_EXPIRED;
                }
            }
            else
            {
                trialDiv.Visible = false;
            }


            string confRSSReaderTaskTimeStamp = Settings.Content["ConfRSSReaderTaskTimeStamp"];
            string confSendReccurenceTaskTimeStamp = Settings.Content["ConfSendReccurenceTaskTimeStamp"];
            string confArchiveTaskTimeStamp = Settings.Content["ConfArchiveTaskTimeStamp"];

            string rssTaskQuery = @"IF ISDATE('" + confRSSReaderTaskTimeStamp + "') = 1 "
                     + "BEGIN" +
                        " SELECT DATEDIFF(n,CONVERT(DATETIME,'" + confRSSReaderTaskTimeStamp + "'),GETUTCDATE()) as diff" +
                    " END" +
                    " ELSE" +
                    " BEGIN" +
                        " UPDATE settings SET val = CONVERT(nvarchar, GETUTCDATE(), 9) WHERE name='ConfRSSReaderTaskTimeStamp'" +
                        " SELECT 0 as diff" +
                    " END";

            string recurTaskQuery = @"IF ISDATE('" + confSendReccurenceTaskTimeStamp + "') = 1 "
                     + "BEGIN" +
                        " SELECT DATEDIFF(n,CONVERT(DATETIME,'" + confSendReccurenceTaskTimeStamp + "'),GETUTCDATE()) as diff" +
                    " END" +
                    " ELSE" +
                    " BEGIN" +
                        " UPDATE settings SET val = CONVERT(nvarchar, GETUTCDATE(), 9) WHERE name='ConfSendReccurenceTaskTimeStamp'" +
                        " SELECT 0 as diff" +
                    " END";

            string archiveTaskQuery = @"IF ISDATE('" + confArchiveTaskTimeStamp + "') = 1 "
                     + "BEGIN" +
                        " SELECT DATEDIFF(n,CONVERT(DATETIME,'" + confArchiveTaskTimeStamp + "'),GETUTCDATE()) as diff" +
                    " END" +
                    " ELSE" +
                    " BEGIN" +
                        " UPDATE settings SET val = CONVERT(nvarchar, GETUTCDATE(), 9) WHERE name='ConfArchiveTaskTimeStamp'" +
                        " SELECT 0 as diff" +
                    " END";

            List<string> badTasksLabels = new List<string>();
            int rssDiff = (int)dbMgr.GetScalarByQuery(rssTaskQuery);
            int recurDiff = (int)dbMgr.GetScalarByQuery(recurTaskQuery);
            int archiveDiff = (int)dbMgr.GetScalarByQuery(archiveTaskQuery);

            if (rssDiff > 1440)
            {
                badTasksLabels.Add("Deskalerts RSS Send Task");
            }

            if (recurDiff > 1440)
            {
                badTasksLabels.Add("Deskalerts Scheduled Task");
            }

            if (archiveDiff > 1440)
            {
                badTasksLabels.Add("Deskalerts Archive Task");
            }

            Settings.Content.Reload();

            var menuItem = dbMgr.GetDataByQuery(@"SELECT id, name, loc_key, [image], link, parent_id, index_in_parent 
                                                      FROM menu_items
                                                      WHERE [enabled] = 1 
                                                      ORDER BY default_index ASC");

            var rowsList = menuItem.ToList();
            var parentsItems = rowsList.Where(x => x.GetInt("parent_id") == -1).ToList();
            var isTrial = Settings.IsTrial;

            foreach (var parent in parentsItems)
            {
                var name = parent.GetString("name");

                if (!menuMgr.CanDisplayMenuItem(name, curUser))
                {
                    continue;
                }

                var locKey = parent.GetString("loc_key").Trim();
                var link = parent.GetString("link");
                var image = parent.GetString("image");
                var id = parent.GetInt("id");

                if (name == "ad.ip_groups" && isTrial)
                {
                    image = "images/menu_icons/ipgroups_20_disabled.png";
                }

                accordion.InnerHtml += "<div class=\"group\">";

                var childs = rowsList.Where(x => x.GetInt("parent_id") == id).OrderBy(x => x.GetInt("index_in_parent")).ToList();

                //select items that available for this publisher

                childs = childs.Where(x => menuMgr.CanDisplayMenuItem(x.GetString("name"), curUser)).ToList();

                if (childs.Count == 0)
                {
                    accordion.InnerHtml += menuMgr.BuildSingleMenuItemHtml(link, locKey, image, name, curUser, dbMgr);
                }
                else
                {
                    accordion.InnerHtml += menuMgr.BuildParentMenuItemHtml(locKey, image);
                    accordion.InnerHtml += "<div class=\"folders\">";
                    accordion.InnerHtml += "<table cellspacing='0' cellpadding='0' border='0'>";

                    foreach (var child in childs)
                    {
                        var childLocKey = child.GetString("loc_key").Trim();
                        var childLink = child.GetString("link");
                        var childImage = child.GetString("image");
                        var curIndex = childs.IndexOf(child);
                        var isLast = curIndex == childs.Count - 1;

                        accordion.InnerHtml += "<tr>";
                        accordion.InnerHtml += menuMgr.BuildChildMenuItemHtml(childLink, childLocKey, locKey, childImage, isLast, curUser);
                        accordion.InnerHtml += "</tr>";
                    }

                    accordion.InnerHtml += "</table>";
                    accordion.InnerHtml += "</div>";

                }

                accordion.InnerHtml += "</div>";
            }

            dbMgr.Dispose();
        }
    }
}