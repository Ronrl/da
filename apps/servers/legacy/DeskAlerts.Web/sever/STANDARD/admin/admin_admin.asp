﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%

check_session()

  uid = Session("uid")

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>DeskAlerts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/style9.css" rel="stylesheet" type="text/css">
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script language="javascript" type="text/javascript">

$(document).ready(function() {

	var width = $(".data_table").width();
	var pwidth = $(".paginate").width();
	if (width != pwidth) {
		$(".paginate").width("100%");
	}
	$("#add_editor_button").button({
		icons : {
			primary : "ui-icon-plusthick"
		}
	});
	$(".search_button").button({
		icons : {
			primary : "ui-icon-search"
		}
	});
	$(".delete_button").button();
});

</script>
</head>
<script language="javascript" type="text/javascript" src="functions.js"></script>
<%
   if(Request("offset") <> "") then 
	offset = clng(Request("offset"))
   else 
	offset = 0
   end if	

   if(Request("sortby") <> "") then 
	sortby = Request("sortby")
   else 
	sortby = "name"
   end if	


if (uid <>"") then

'counting pages to display
   if(Request("limit") <> "") then 
	limit = clng(Request("limit"))
   else 
	limit=50
   end if

 isModer = false
  'limit=10


	Set RS1 = Conn.Execute("SELECT type FROM policy WHERE id  IN (SELECT policy_id FROM policy_editor WHERE editor_id = " & uid & ")")
	
	
   SQL = "SELECT COUNT(u.id) as mycnt FROM users as u"
    
    if(not RS1.EOF ) then
        if(RS1("type") = "M") then
              isModer = true
            SQL = SQL & " LEFT JOIN policy_editor as pe ON pe.editor_id = u.id LEFT JOIN policy as p ON p.id = pe.policy_id WHERE p.type != 'A' AND u.role = 'E'"
         else
            SQL = SQL & " WHERE u.role = 'E'"
        end if
    else
        SQL = SQL & " WHERE u.role = 'E'"
    end if
 '  Response.Write SQL
	Set RS = Conn.Execute(SQL)
	cnt=0

	if(Not RS.EOF) then
		cnt=RS("mycnt")
	end if

	RS.Close

  j=cnt/limit


%>
<body style="margin:0px" class="body">
<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	<tr>
	<td>
	<table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
		<tr>
		<td width=100% height=31 class="main_table_title"><img src="images/menu_icons/editors_20.png" alt="" width="20" height="20" border="0" style="padding-left:7px">
		<a href="admin_admin.asp" class="header_title" style="position:absolute;top:15px"><%=LNG_EDITORS %></a></td>
		</tr>
		<tr>
		<td class="main_table_body" height="100%">
		<div style="margin:10px;">
		<div align="right"><a id="add_editor_button" href="admin_edit.asp"><%=LNG_ADD_EDITOR %></a></div><br>
<%
if(cnt>0) then

	page="admin_admin.asp?"
	name=LNG_EDITORS
	Response.Write make_pages(offset, cnt, limit, page, name, sortby) 

	response.write "<br><A href='#' class='delete_button' onclick=""javascript: return LINKCLICK('"&linkclick_str&"');"">"&LNG_DELETE&"</a></a><br/><br>"
	response.write "<form action='delete_objects.asp' name='delete_objs' id='delete_objs' method='post'><input type='hidden' name='objType' value='editors'>"

%>


		<table width="100%" height="100%" class="data_table" cellspacing="0" cellpadding="3">
		<tr class="data_table_title">
		<td width="10"><input type='checkbox' name='select_all' id='select_all' onclick='javascript: select_all_objects();'></td>

		<td class="table_title" style="width:50%"><% response.write sorting(LNG_NAME,"name", sortby, offset, limit, page) %></td>
		<td class="table_title" style="width:50%"><% response.write sorting(LNG_POLICY_NAME,"policy_name", sortby, offset, limit, page) %></td>
		<td class="table_title"><%=LNG_ACTIONS %></td>
				</tr>
<%

'show main table
    SQL = "SELECT u.id, u.name, u.role, p.name AS policy_name FROM users u LEFT JOIN policy_editor pe ON u.id=pe.editor_id LEFT JOIN policy p ON p.id=pe.policy_id WHERE u.role='E'"
	

	if(isModer) then
	    SQL = SQL & " AND p.type != 'A'"
	end if
	SQL = SQL & " ORDER BY "&sortby
	Set RS = Conn.Execute( SQL)
	num=offset
	if(Not RS.EOF) then RS.Move(offset) end if
	Do While Not RS.EOF
		num=num+1
		if((offset+limit) < num) then Exit Do end if
		if(num > offset AND offset+limit >= num) then
		%>
		<tr>
			<td><input type="checkbox" name="objects" value="<%=RS("id")%>"></td>
			<td> 
				<%=HtmlEncode(RS("name"))%>
			</td>
			<td> 
				<%=HtmlEncode(RS("policy_name"))%>
			</td>
			<td align="center" width="20" nowrap="nowrap">
				<a href="admin_edit.asp?id=<%=RS("id")%>"><img src="images/action_icons/edit.png" alt="<%=LNG_EDIT_EDITOR %>" title="<%=LNG_EDIT_EDITOR %>" width="16" height="16" border="0" hspace=5></a>
			</td>
		</tr>
		<%
		end if
	RS.MoveNext
	Loop
	RS.Close


%>
         </table>
<%
	response.write "</form><br/><A href='#' class='delete_button' onclick=""javascript: return LINKCLICK('"&linkclick_str&"');"">"&LNG_DELETE&"</a><br><br>"

	Response.Write make_pages(offset, cnt, limit, page, name, sortby) 
else
	response.write "<center><b>"& LNG_THERE_ARE_NO_EDITORS &".</b></center>"
end if
%>

		 <img src="images/inv.gif" alt="" width="1" height="100%" border="0">
		</div>
		</td>
		</tr>
	</table>
	</td>
	</tr>
</table>
<%
else
	Response.Redirect "index.asp"

end if
%>
</body>
</html>
<!-- #include file="db_conn_close.asp" -->