﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;
using System.Reflection;
using System.IO;
using System.Diagnostics;
namespace DeskAlertsDotNet.sever.STANDARD.admin
{
	
    class JsonQuery
    {
        public string Function { get; set; }
        // public string JsonString { get; set; }

    }

    class EmailJsonQuery
    {
        public string Function { get; set; }
        public EmailParams JsonString { get; set; }

    }
    class EmailParams
    {
        public string SmtpServer { get; set; }

        public string SmtpPort { get; set; }
        public string SmtpUsername { get; set; }
        public string SmtpPassword { get; set; }
        public string SmtpSSL { get; set; }
        public string SmtpAuth { get; set; }
        public string Subject { get; set; }
        public string SenderName { get; set; }
        public string Email { get; set; }
        public string Text { get; set; }
    }

    class VideoJsonQuery
    {
        public string Function { get; set; }
        public VideoParams JsonString { get; set; }
    }

    class VideoParams
    {
        public string InputFilePath { get; set; }
    }

    public partial class functions_inc : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Form.Count>0)
            {
                 if (!string.IsNullOrEmpty(Request.Form[0]))
                {
                    //Response.Write(HttpUtility.HtmlEncode(Request.Form[0]));
                    string QueryString = System.Text.Encoding.Default.GetString(Convert.FromBase64String(HttpUtility.HtmlDecode(Request.Form[0])));
                    QueryString = QueryString.Replace("\\", "\\\\");
                    var json = new JavaScriptSerializer();
                    var data = json.Deserialize<JsonQuery>(QueryString);
                    //Response.Write(HttpUtility.HtmlEncode(QueryString));
                    if (data.Function.Contains("SendAlertEmail"))
                    {
                        var emaildata = json.Deserialize<EmailJsonQuery>(QueryString);
                        //Response.Write(emaildata.JsonString.SmtpSSL);
                        emaildata.JsonString.Email = HttpUtility.UrlDecode(emaildata.JsonString.Email);
                        emaildata.JsonString.SenderName = HttpUtility.UrlDecode(emaildata.JsonString.SenderName);
                        emaildata.JsonString.Text = HttpUtility.UrlDecode(emaildata.JsonString.Text);
                        SendAlertEmail(emaildata.JsonString.SmtpServer, emaildata.JsonString.SmtpPort, emaildata.JsonString.SmtpUsername, emaildata.JsonString.SmtpPassword, int.Parse(emaildata.JsonString.SmtpSSL), int.Parse(emaildata.JsonString.SmtpAuth), emaildata.JsonString.Subject, emaildata.JsonString.SenderName, emaildata.JsonString.Email, emaildata.JsonString.Text);

                    }
                    else
                        if (data.Function.Contains("ConvertVideo"))
                        {
                            var videodata = json.Deserialize<VideoJsonQuery>(QueryString);

                            Response.Write(ConvertVideo(videodata.JsonString.InputFilePath));
                            Response.End();
                        }

                }
            }
        }
        /// <summary>
        /// Converts any Video File To .mp4
        /// </summary>
        /// <param name="file"></param>
        /// <param name="ffmpegpath"></param>
        /// <returns></returns>
        public string ConvertVideo(string file)
        {
            string result = string.Empty;
            string input = string.Empty;
            string output = string.Empty;
            try
            {
				string ffmpegFilePath;
                if (IntPtr.Size == 4) //check if is 64 bit system
                {
                    ffmpegFilePath = Path.GetFullPath(Path.Combine(file.Remove(file.Length - Path.GetFileName(file).Length, Path.GetFileName(file).Length), @"..\..\")) + "ffmpeg\\x86\\ffmpeg.exe"; // path of ffmpeg.exe - please replace it for your options.
                }
                else
                {
                    ffmpegFilePath = Path.GetFullPath(Path.Combine(file.Remove(file.Length - Path.GetFileName(file).Length, Path.GetFileName(file).Length), @"..\..\")) + "ffmpeg\\x64\\ffmpeg.exe"; // path of ffmpeg.exe - please replace it for your options.
                }
                 
				
                //Response.Write(IntPtr.Size.ToString());
                FileInfo fi = new FileInfo(file);
                string filename = Path.GetFileNameWithoutExtension(fi.Name);
                string extension = Path.GetExtension(fi.Name);
                input = file;
                output = file + ".mp4";
                if (File.Exists(output))
                {
                    File.Delete(output);
                }
                
                {
                    var processInfo = new ProcessStartInfo(ffmpegFilePath, " -i \"" + input + "\" -vcodec h264 -threads 0 -r 25 -g 50 -b 500k -bt 500k -acodec mp3 -ar 44100 -ab 64k  \"" + output + "\"")
                    {
                        UseShellExecute = false,
                        CreateNoWindow = true,
                        RedirectStandardOutput = true,
                        RedirectStandardError = true
                    };
                    try
                    {
                        Process process = System.Diagnostics.Process.Start(processInfo);
                       /*result=*/ process.StandardError.ReadToEnd();
                        process.WaitForExit();
                        process.Close();
						result="";
                    }
                    catch (Exception ex)
                    {
                        result = ex.Message;
                    }
                }
            }
            catch (Exception ex)
            {

                result = ex.Message;

            }

            return result;
        }



        string SplitEmailStrging(string email)
        {

            //Response.Write(HttpUtility.HtmlEncode(trueemail) + "<br>");
            string[] emaildata = Regex.Split(email, @" ");
            //Response.Write(HttpUtility.HtmlEncode(emaildata.Count()) + "<br>");
            //Response.Write(HttpUtility.HtmlEncode(emaildata[1]));
            //Response.Write(HttpUtility.HtmlEncode(emaildata[1]));


            string trueemail = emaildata[emaildata.Count() - 1];
            trueemail = trueemail.Replace("<", "");
            trueemail = trueemail.Replace(">", "");
            trueemail = trueemail.Replace("\"", "");
            for (int i = 0; i <= trueemail.Length; i++)
            {
                if (trueemail[i] == ' ')
                {
                    //Response.Write(HttpUtility.HtmlEncode(trueemail) + "<br>");
                    trueemail = trueemail.Remove(i, 1);
                }
                else
                {
                    break;
                }
            }
            if (emaildata.Count() > 0)
                return trueemail;
            else
                return null;

        }
        protected void SendAlertEmail(string smtpserver, string smtpport, string smtpuser, string smtppass, int ssl, int auth, string subject, string from, string to, string body)
        {
           /*Response.Write(smtpserver + "<br>");
            Response.Write(smtpport + "<br>");
            Response.Write(smtpserver + "<br>");
            Response.Write(smtpuser + "<br>");
            Response.Write(smtppass + "<br>");
            Response.Write(ssl.ToString() + "<br>");
            Response.Write(auth.ToString() + "<br>");
            Response.Write(subject + "<br>");
            Response.Write(HttpUtility.HtmlEncode(SplitEmailStrging(from)) + "<br>");
            Response.Write(HttpUtility.HtmlEncode(to) + "<br>");
            Response.Write(HttpUtility.HtmlEncode(body) + "<br>");*/

            try
            {
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(SplitEmailStrging(from), HttpUtility.HtmlDecode(Request.Form["senderName"]));
                string emails = to;

                if (emails.Contains(","))
                {

                    string[] emailslist = Regex.Split(emails, @",");
                    foreach (string email in emailslist)
                    {
                        mail.To.Add(SplitEmailStrging(email));
                    }

                }
                else
                {
                    if (emails.Contains("<"))
                    {

                        mail.To.Add(SplitEmailStrging(emails));
                        // Response.Write(SplitEmailStrging(emails));
                    }
                    else
                    {

                        mail.To.Add(emails);
                        // Response.Write(emails);
                    }

                }
                
                mail.Subject = subject;
                mail.IsBodyHtml = true;
                mail.Body = HttpUtility.HtmlDecode(body);
                SmtpClient client = new SmtpClient(smtpserver);

                if (int.Parse(smtpport) == 465)
                {
                    client.Port = 25;
                }
                else
                {
                    client.Port = int.Parse(smtpport);
                }

                if (ssl == 1)
                {
                    client.EnableSsl = true;
                }
                else
                {
                    client.EnableSsl = false;
                }

                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(smtpuser, smtppass);
                client.UseDefaultCredentials = false;
                client.Credentials = credentials;

                //variation of auth

                /*
                FieldInfo transport = client.GetType().GetField("transport",
                BindingFlags.NonPublic | BindingFlags.Instance);

                FieldInfo authModules = transport.GetValue(client).GetType()
                    .GetField("authenticationModules",
                        BindingFlags.NonPublic | BindingFlags.Instance);

                Array modulesArray = authModules.GetValue(transport.GetValue(client)) as Array;
                modulesArray.SetValue(modulesArray.GetValue(2), 0);
                modulesArray.SetValue(modulesArray.GetValue(2), 1);
                modulesArray.SetValue(modulesArray.GetValue(2), 3);
               */
               
                client.Send(mail);

                //Response.Write("done");

            }
            catch (Exception ex)
            {
                Response.Write("Error: " + ex.InnerException.Message);
                Response.End();
            }


        }
    }
}
