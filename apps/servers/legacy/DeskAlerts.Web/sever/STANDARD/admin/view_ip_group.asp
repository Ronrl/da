﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%
check_session()
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<title>Deskalerts Control Panel</title>
	<meta http-equiv=Content-Type content="text/html;charset=utf-8">
	<link href="css/style9.css" rel="stylesheet" type="text/css">
	<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
	<script language="JavaScript" type="text/javascript">
		$(function() {
			$(".close_button").button();
		});
	</script>
	<style>
		table {border-collapse:collapse;empty-cells:show}
		td {border-width:2px}
	</style>
</head>
<%
   if(Request("limit") <> "") then 
	limit = clng(Request("limit"))
   else 
	limit=50
   end if

	groupId = clng(request("id"))
	Set rsGroup = Conn.Execute("SELECT name FROM ip_groups WHERE id="&groupId)
	if(not rsGroup.EOF) then
		groupName = rsGroup("name")
	end if
%> 
<body style="margin:0" class="body">
<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%" class="body">
	<tr>
	<td valign="top">
	<table width="100%" cellspacing="0" cellpadding="0" class="main_table">
		<tr>
		<td width=100% height=31 class="theme-colored-back"><a href="#" class="header_title"><%=LNG_OBJECTS_IN_GROUP %>: <%=groupName %></a></td>
		</tr>
		<tr>
		<td valign="top" class="main_table_body">
		<div style="margin:10px">
<%
cnt=0
Set rsCnt = Conn.Execute("SELECT COUNT(id) as mycnt FROM ip_range_groups WHERE group_id="&groupId)
if(not rsCnt.EOF) then
	cnt = rsCnt("mycnt")
end if
j=cnt/limit
	

if(cnt>0) then
	page= "view_ip_group.asp?id="&groupId&"&"
	name= LNG_OBJECTS
	Response.Write make_pages(offset, cnt, limit, page, name, sortby) 
end if


'paging
%>
<table width="95%" cellspacing="0" cellpadding="3" class="data_table">
<tr class="data_table_title">
	<td class="table_title"><%=LNG_TYPE %></td>
	<td class="table_title"><%=LNG_VALUE %></td>
	</tr>
<%
	Set RSitems = Conn.Execute("SELECT from_ip, to_ip FROM ip_range_groups WHERE group_id=" & groupId)
	do while not RSitems.EOF
		
		ip_from = RSitems("from_ip")
		ip_to = RSitems("to_ip")

		if(ip_from = ip_to) then
		%>
		<tr><td> <%=LNG_IP_ADDRESS %> </td><td> <%=ip_from %> </td></tr>
		<%
		else
		%>
		<tr><td> <%=LNG_IP_RANGE %> </td><td> <%=ip_from %> - <%=ip_to %> </td></tr>
		<%	
		end if
		RSitems.MoveNext
	loop
%>
 
</table>
<%
if(cnt>0) then
	Response.Write make_pages(offset, cnt, limit, page, name, sortby) 
end if
%>
 
<br>
<center><a href="#" class="close_button" onclick="javascript: window.close()"><%=LNG_CLOSE%></a></center>
 
</div>		</td>
		</tr>
	</table>
	</td>
	</tr>
</table>
</body>
</html>
<!-- #include file="db_conn_close.asp" -->