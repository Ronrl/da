﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GroupEdit.aspx.cs" Inherits="DeskAlertsDotNet.Pages.GroupEdit" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>DeskAlerts</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link href="css/style9.css" rel="stylesheet" type="text/css">
    <link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
    <script type="text/javascript">
        function onSaveClick() {
            var groups = document.getElementById("ddlGroups");
            var policy = document.getElementById("ddlPolicy");

            if (groups.selectedIndex === 0 || policy.selectedIndex === 0) {
                alert('Please select group and policy.');
                return false;
            }

            return true;
        }
    </script>
</head>

<body style="margin: 0" class="body">
<table border="0" cellspacing="0" cellpadding="6" width="100%" height="100%">
    <tr>
        <td>
            <form runat="server">
                <table cellspacing="0" cellpadding="0" class="main_table" width="100%">
                    <tr>
                        <td height="31" class="main_table_title">
                            <a href="edit_group.asp" class="header_title">
                                <asp:label id="lbTitle" runat="server"/>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td class="main_table_body" height="100%">
                            <div style="margin: 10px;">
                                <span class="work_header">
                                    <asp:label id="lbHeader" runat="server"/>
                                </span>
                                <br><br>

                                <form method="post" action="">
                                    <table cellpadding="4" cellspacing="4" border="0" style="table-layout: auto;">
                                        <tr>
                                            <td><asp:label id="lbSelectGroup" runat="server"/>:</td>
                                            <td>
                                                <asp:DropDownList ID="ddlGroups" runat="server" AppendDataBoundItems="true"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><asp:label id="lbSelectPolicy" runat="server"/>:</td>
                                            <td>
                                                <asp:DropDownList ID="ddlPolicy" runat="server" AppendDataBoundItems="true"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <div align="right">
                                                    <asp:Button ID="btnCancel" runat="server" class="cancel_button" OnClick="btnCancel_OnClick"/>
                                                    <asp:Button id="btnSave" runat="server" class="cancel_button" OnClientClick="return onSaveClick();" OnClick="btnSave_OnServerClick"/>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </form>
                                <img src="images/inv.gif" alt="" width="1" height="100%" border="0">
                            </div>
                        </td>
                    </tr>
                </table>
            </form>
        </td>
    </tr>
</table>
</body>
</html>