﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<%

check_session()

'script for changing users in groups
uid = Session("uid")

if (uid <> "") then
	retPage = Request("return_page")
	if retPage = "" then
		if(Request("domain_id")<>1) then
			retPage = "view_groups.asp?id=" & Request("domain_id")
		else
			retPage = "admin_groups.asp?err=" & myerr
		end if
	end if

	group_id=Request("group_id")
	ids_users = split(request("ids_users"),",")
	ids_computers = split(request("ids_computers"),",")
	ids_groups = split(request("ids_groups"),",")
	ids_ous = split(request("ids_ous"),",")

	SQL = "UPDATE users_groups SET was_checked=0 WHERE group_id="&group_id
	Conn.Execute(SQL)
	
	for each user_id in ids_users
		if(user_id<>"") then
			Set RS1 = Conn.Execute("SELECT id FROM users_groups WHERE user_id=" & user_id &" AND group_id="&group_id)
			if(RS1.EOF) then
				SQL = "INSERT INTO users_groups (user_id, group_id, was_checked) VALUES (" & user_id & ", "& group_id &", 1)"
				Conn.Execute(SQL)
			else
				SQL = "UPDATE users_groups SET was_checked=1 WHERE id=" & RS1("id")
				Conn.Execute(SQL)
				RS1.Close
			end if
		end if
	next
	
	SQL = "DELETE FROM users_groups WHERE was_checked=0 AND group_id=" & group_id
	Conn.Execute(SQL)
	
	SQL = "UPDATE computers_groups SET was_checked=0 WHERE group_id="&group_id
	Conn.Execute(SQL)
	
	for each computer_id in ids_computers
		if(computer_id<>"") then
			Set RS1 = Conn.Execute("SELECT id FROM computers_groups WHERE comp_id=" & computer_id &" AND group_id="&group_id)
			if(RS1.EOF) then
				SQL = "INSERT INTO computers_groups (comp_id, group_id, was_checked) VALUES (" & computer_id & ", "& group_id &", 1)"
				Conn.Execute(SQL)
			else
				SQL = "UPDATE computers_groups SET was_checked=1 WHERE id=" & RS1("id")
				Conn.Execute(SQL)
				RS1.Close
			end if
		end if
	next
	
	SQL = "DELETE FROM computers_groups WHERE was_checked=0 AND group_id=" & group_id
	Conn.Execute(SQL)
	
	SQL = "UPDATE groups_groups SET was_checked=0 WHERE group_id="&group_id
	Conn.Execute(SQL)
	
	for each child_group_id in ids_groups
		if(child_group_id<>"") then
			Set RS1 = Conn.Execute("SELECT id FROM groups_groups WHERE group_id=" & child_group_id &" AND container_group_id="&group_id)
			if(RS1.EOF) then
				SQL = "INSERT INTO groups_groups (group_id, container_group_id, was_checked) VALUES (" & child_group_id & ", "& group_id &", 1)"
				Conn.Execute(SQL)
			else
				SQL = "UPDATE groups_groups SET was_checked=1 WHERE id=" & RS1("id")
				Conn.Execute(SQL)
				RS1.Close
			end if
		end if
	next
	
	SQL = "DELETE FROM groups_groups WHERE was_checked=0 AND group_id=" & group_id
	Conn.Execute(SQL)
	
	if(AD=3) then
	'TODO add OU functionality
		SQL = "UPDATE ou_groups SET was_checked=0 WHERE group_id="&group_id
		Conn.Execute(SQL)
	
		for each ou_id in ids_ous
			if(ou_id<>"") then
				Set RS1 = Conn.Execute("SELECT id FROM ou_groups WHERE ou_id=" & ou_id &" AND group_id="&group_id)
				if(RS1.EOF) then
					SQL = "INSERT INTO ou_groups (ou_id, group_id, was_checked) VALUES (" & ou_id & ", "& group_id &", 1)"
					Conn.Execute(SQL)
				else
					SQL = "UPDATE ou_groups SET was_checked=1 WHERE id=" & RS1("id")
					Conn.Execute(SQL)
					RS1.Close
				end if
			end if
		next
	
		SQL = "DELETE FROM ou_groups WHERE was_checked=0 AND group_id=" & group_id
		Conn.Execute(SQL)
	end if

	Response.Redirect retPage
else
	Response.Redirect "index.asp"
end if

%>
<!-- #include file="db_conn_close.asp" -->