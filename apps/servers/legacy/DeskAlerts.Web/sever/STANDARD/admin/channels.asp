﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%
response.AddHeader "content-type", "text/html; charset=utf-8"
Response.CharSet = "UTF-8"
check_session()

uid = Session("uid")
if uid <>"" then
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>DeskAlerts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
<link href="css/style9.css" rel="stylesheet" type="text/css">
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script language="javascript" type="text/javascript" src="functions.js"></script>
<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$("#accordion").accordion();
		$("#qrDialog").dialog({
			autoOpen: false,
			height: 220,
			width: 350,
			modal: true
		});
		$("#openQrDialog").button().click(function() { $("#qrDialog").dialog("open"); });
	})
</script>
</head>

<body style="margin:0" class="body">
<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	<tr>
	    <td>
	        <table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
		        <tr>
		            <td width="100%" height="31" class="main_table_title"><img src="images/menu_icons/channels.png" alt="" width="20" height="20" border="0" style="padding-left:7px">
					<a href="sent.asp" class="header_title" style="position:absolute;top:15px"><%=LNG_CHANNELS %></a></td>
		        </tr>
		        <tr>
		            <td height="100%" valign="top" class="main_table_body">
		            <div style="margin:10px">
		            <div id="accordion">
						<h3>DeskAlerts Mobile <%if not MOBILE_SEND=1 then %>(not included in your package)<%else %>(included in your package)<%end if %></h3>
						<div>
							<p>DeskAlerts mobile applications enable you to send notificaiotns to employees' smartphones. Applications are available for Android and iOS operating systems</p> 
							<p>DeskAlerts app is designed for effective employee communications and employee notification using the mass messaging or targeted messaging capability. DeskAlerts app is compatible with the DeskAlerts servers hosted within your company as well as with the cloud version in case you still don't use a DeskAlerts software internally.</p>
							<%if not MOBILE_SEND=1 then %>
							<br />
							<strong>Note: </strong><span>Your version of DeskAlerts doesn't support sending of moible notificaitons at the moment. <a href="mailto:sales@deskalerts.com">Contact DeskAlerts sales</a> for a quote.</span>
							<%end if %>
							<div style="clear:both;height:10px"></div>
							<a href="https://itunes.apple.com/app/mobile-alert-client/id592345705" target="_blank"><img src="images/appstore.png" height="50" style="float:left;margin-right:10px"></a>
							<a href="https://play.google.com/store/apps/details?id=com.toolbarstudio.alerts" target="_blank"><img src="images/googleplay.png" height="50" style="float:left"></a>
							<div style="clear:both;height:10px"></div>
							<p>
							<button id="openQrDialog">Show QR code</button>
							</p>
						</div>
						<h3>Corporate Screensavers <%if not SS=1 then %>(not included in your package)<%else %>(included in your package)<%end if %></h3>
						<div>
							<p>Corporate Screensavers are a powerful communications channel and engagement tool. Use DeskAlerts to make your employees’ desktops interactive and get their attention when they are not involved to work. </p>
							<p>Send PowerPoint presentations as screensaver or create your own with our visual editor. With DeskAlerts messaging solution your staff will be updated with regularly changing visual information.</p>
							<p>Corporate screensaver can be targeted to specific groups, departments or individual users or any combinations of the above.</p>
							<p>Delivery can be scheduled and availability time can be specified so that you can target specific events in your internal communication strategy.</p>
							<p>Sending corporate screensaver to a particular PC will overwrite existing screensaver settings so 100% delivery for your information is guaranteed.</p>
							<p>If a user changes the current screensaver on his own PC, the setting will be reverted back to the DeskAlerts screensaver automatically.</p>
							<%if not SS=1 then %>
							<br />
							<strong>Note: </strong><span>Your version of DeskAlerts doesn't support sending screensavers at the moment. <a href="mailto:sales@deskalerts.com">Contact DeskAlerts sales</a> for a quote.</span>
							<%end if %>
						</div>
		            </div>
		            </div>
	</td>
	</tr>
</table>
<div id="qrDialog" title="Scan the QR code with your phone to get the app" style="display:none;text-align:center">
<img src="images/qr.png">
</div>
<%
else
	Response.Redirect "index.asp"

end if
%>
</body>
</html>
<!-- #include file="db_conn_close.asp" -->