﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Parameters;


namespace DeskAlertsDotNet.Pages
{
    public partial class AddIpGroups : DeskAlertsBasePage
    {
        //use curUser to access to current user of DA control panel      
        //use dbMgr to access to database
        private string groupName;
        private string groupId;

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            groupName = Request["groupName"];
            if (Request["edit"] == "1")
            {
                UpdateIPGroup();
            }
            else
            {
                CreateIPGroup();
            }
            HandleIPGroups();

            if (!string.IsNullOrEmpty(Request["ip_address"]))
                HandleSingleIPGroups();


            Response.Redirect("IpGroups.aspx");
            //Add your main code here
        }

        private void UpdateIPGroup()
        {
            groupId = Request["groupId"];
            dbMgr.ExecuteQuery(string.Format("UPDATE ip_groups SET name = N'{0}' WHERE id = {1}", groupName.EscapeQuotes(), groupId));
            dbMgr.ExecuteQuery("DELETE FROM ip_range_groups WHERE group_id = " + groupId);
        }

        private void CreateIPGroup()
        {
            groupId = dbMgr.GetScalarQuery<string>(string.Format("INSERT INTO ip_groups (name) OUTPUT INSERTED.ID VALUES (N'{0}')", groupName.EscapeQuotes()));
        }

        private void HandleIPGroups()
        {
            string[] fromIps = { };
            string[] toIps = { };

            if (!string.IsNullOrEmpty(Request["ip_address_from"]))
                fromIps = Request.Form.GetValues("ip_address_from");

            if (!string.IsNullOrEmpty(Request["ip_address_to"]))
                toIps = Request.Form.GetValues("ip_address_to");

            int count = Math.Min(fromIps.Length, toIps.Length);

            for (int i = 0; i < count; i++)
            {
                string from = fromIps[i];
                string to = toIps[i];

                dbMgr.ExecuteQuery(
                    string.Format("INSERT INTO ip_range_groups (group_id, from_ip, to_ip) VALUES ({0}, N'{1}', N'{2}')", groupId, from, to));
            }
        }

        private void HandleSingleIPGroups()
        {
            string[] singleIps = { };

            
                singleIps = Request.Form.GetValues("ip_address");

            foreach (var singleIp in singleIps)
            {
                dbMgr.ExecuteQuery(
                  string.Format("INSERT INTO ip_range_groups (group_id, from_ip, to_ip) VALUES ({0}, N'{1}', N'{2}')", groupId, singleIp, singleIp));
            }
        }
    }
}