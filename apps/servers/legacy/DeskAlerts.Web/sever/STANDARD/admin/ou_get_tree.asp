<% Response.Expires = 0 %>
<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<% Server.ScriptTimeout = 90000 %>

<%

check_session()
Response.ContentType = "text/html; charset=utf-8"
%>

[{
"property":
{
	"name":"<%=LNG_ORGANIZATION %>",
	"type1":"organization",
	"id":"-1",
	"hasCheckbox":false
},
"state": {
	"open": true
},
"type": "organization",
"children":[
<%
no_domain = Eval("ConfEnableRegAndADEDMode = 1")
if not no_domain then
Set RS = Conn.Execute(_
		" SELECT TOP 1 d.id FROM groups g JOIN domains d ON g.domain_id=d.id WHERE d.name = ''" &_
		" UNION" &_
		" SELECT TOP 1 d.id FROM users u JOIN domains d ON u.domain_id=d.id WHERE d.name = ''" &_
		" UNION" &_
		" SELECT TOP 1 d.id FROM computers c JOIN domains d ON c.domain_id=d.id WHERE d.name = ''")
	no_domain = not RS.EOF
end if

Set RS = Conn.Execute("SELECT id, name, COUNT(Id_ou) as child_num FROM domains LEFT JOIN OU_DOMAIN ON Id_domain=id GROUP BY id, name ORDER BY name, id")
do while Not RS.EOF
	if no_domain or Len(RS("name")) > 0 then
		Response.Write "{"
		Response.Write """property"":"
		Response.Write "{"
		Response.Write """name"":" & """"
		if Len(RS("name")) = 0 then
			Response.Write LNG_NO_DOMAIN
		else
			Response.Write Replace(RS("name"),"""", "\""")
		end if
		Response.Write ""","
		Response.Write """type1"":""DOMAIN"","
		if( RS("child_num") > 0 ) then
			Response.Write """loadable"":""true"","
		end if
		Response.Write """id"":" & """"&RS("id")&""","
		Response.Write """hasCheckbox"":false"
		Response.Write "},"
		Response.Write """type"": ""domain"","
		Response.Write """id"":" & """"&RS("id")&""""
		Response.Write "}"

		RS.MoveNext
		if not RS.EOF then
			Response.Write ","
		end if
	else
		RS.MoveNext
	end if
loop

%>
]}]