﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using DeskAlertsDotNet.Parameters;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Models.Permission;

namespace DeskAlertsDotNet.Pages
{
    using Resources;

    public partial class ColorCodes : DeskAlertsBaseListPage
    {

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            headerTitle.Text = resources.LNG_INSTANT_MESSAGES;
            searchButton.Text = searchButton.Text.Replace("{Search}", resources.LNG_SEARCH);
            colorNameCell.Text = GetSortLink(resources.LNG_COLOR_CODE, "name", Request["sortBy"]);
            colorCell.Text = resources.LNG_COLOR;
            actionsCell.Text = resources.LNG_ACTIONS;
            base.Table = contentTable;

            if (!IsPostBack)
            {
                searchTermBox.Value = Request["searchTermBox"];
            }
            string getUrlSearchTermBox = Request.QueryString["searchTermBox"] ?? string.Empty;
                
            bool isSearchTermBoxValueChanged = searchTermBox.Value != getUrlSearchTermBox;
            if (isSearchTermBoxValueChanged)
            {
                Offset = 0;
            }

            int cnt = this.Content.Count < (Offset + Limit) ? this.Content.Count : Offset + Limit;

            bottomDelete.Visible = curUser.HasPrivilege || curUser.Policy[Policies.CC].CanDelete;
            upDelete.Visible = curUser.HasPrivilege || curUser.Policy[Policies.CC].CanDelete;
            actionsCell.Visible = curUser.HasPrivilege || curUser.Policy[Policies.CC].CanEdit;
            addButton.Visible = curUser.HasPrivilege || curUser.Policy[Policies.CC].CanCreate;
            headerSelectAll.Visible = curUser.HasPrivilege || curUser.Policy[Policies.CC].CanDelete;
            for (int i = Offset; i < cnt; i++)
            {
                DataRow row = this.Content.GetRow(i);

                TableRow tableRow = new TableRow();

                if (curUser.HasPrivilege || curUser.Policy[Policies.CC].CanDelete)
                {
                    CheckBox chb = GetCheckBoxForDelete(row);
                    TableCell checkBoxCell = new TableCell();
                    checkBoxCell.HorizontalAlign = HorizontalAlign.Center;
                    checkBoxCell.Controls.Add(chb);
                    tableRow.Cells.Add(checkBoxCell);
                }

                TableCell colorCodeNameContentCell = new TableCell();
                colorCodeNameContentCell.Text = row.GetString("name");
                tableRow.Cells.Add(colorCodeNameContentCell);

                TableCell colorCodeContentCell = new TableCell();
                colorCodeContentCell.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#" + row.GetString("color"));
                tableRow.Cells.Add(colorCodeContentCell);


                if (curUser.HasPrivilege || curUser.Policy[Policies.CC].CanEdit)
                {
                    LinkButton editButton = GetActionButton(row.GetString("id"), "images/action_icons/edit.png",
                        "LNG_EDIT_COLOR_CODE",
                        delegate
                        {
                            Response.Redirect("EditColorCodes.aspx?id=" + row.GetString("id"), true);
                        }

                        );

                    TableCell actionContentCell = new TableCell();
                    actionContentCell.HorizontalAlign = HorizontalAlign.Center;
                    actionContentCell.Controls.Add(editButton);

                    tableRow.Cells.Add(actionContentCell);
                }
                contentTable.Rows.Add(tableRow);


            }
        }

        #region DataProperties
        protected override string DataTable
        {
            get
            {

                return "color_codes";
                
            }
        }

        protected override string[] Collumns
        {
            get
            {

                return new string[] { "name", "color" };

            }
        }

        protected override string DefaultOrderCollumn
        {
            //return collumn for default order
            get
            {
                return "name";
                //throw new NotImplementedException();

            }
        }

        protected override string ConditionSqlString
        {
            get
            {
                return SearchTerm.Length > 0 ? string.Format("name LIKE '%{0}%'", SearchTerm) : string.Empty;
            }
        }

        /*
         * 
         * If you want to add condition for data selection override property ConditionSqlString 
         * Also you can implement your custom query by overriding properties CustomQuery and CustomCountQuery
         *  
         */
        #endregion

        #region Interface properties

        protected override IButtonControl[] DeleteButtons
        {
            get
            {
                return new IButtonControl[] { upDelete, bottomDelete };
            }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl NoRowsDiv
        {
            get { return NoRows; }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl TableDiv
        {
            get { return mainTableDiv; }
        }

        protected override string ContentName
        {
            get
            {
                //return content name here
                return resources.LNG_COLOR_CODES;
            }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl BottomPaging
        {
            get
            {
                return bottomRanging;
            }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl TopPaging
        {
            get
            {
                return topPaging;
            }
        }


        #endregion
    }
}