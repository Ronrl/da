﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Statistics.aspx.cs" Inherits="DeskAlertsDotNet.Pages.Statistics" %>
<%@ Import Namespace="DeskAlerts.Server.Utils.Helpers" %>
<%@ Import NameSpace="DeskAlertsDotNet.Parameters" %>
<%@ Import Namespace="Resources" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    
    <link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="css/style9.css" rel="stylesheet" type="text/css">
    <link href="css/jquery/jquery_timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet"
        type="text/css">

    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/jquery.jqplot.min.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/jqplot.highlighter.min.js"></script>
    <script type="text/javascript" src="jscripts/jqplot.pieRenderer.min.js"></script>
    <script type="text/javascript" src="jscripts/jqplot.barRenderer.min.js"></script>
    <script type="text/javascript" src="jscripts/jqplot.categoryAxisRenderer.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/jquery.jqplot.css" />
    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery_timepicker/jquery-ui-timepicker-addon.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/jquery/datepicker_fix.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/json2.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/date.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/preview.js?v=9.2.1.31"></script>
    <script language="JavaScript" src="jscripts/FusionCharts.js"></script>
    <script language="javascript" type="text/javascript" src="functions.js"></script>
    <script language="JavaScript" type="text/javascript">
        function openDialogUI(_form, _frame, _href, _width, _height, _title) {
            $("#dialog-" + _form).dialog('option', 'height', _height);
            $("#dialog-" + _form).dialog('option', 'width', _width);
            $("#dialog-" + _form).dialog('option', 'title', _title);
            $("#dialog-" + _frame).attr("src", _href);
            $("#dialog-" + _frame).css("height", _height);
            $("#dialog-" + _frame).css("display", 'block');
            if ($("#dialog-" + _frame).attr("src") != undefined) {
                $("#dialog-" + _form).dialog('open');
                $("#dialog-" + _form).contents().find('#table_border').css('border', 'none');
            }
        }



        $(document).ready(function() {

            $("#dialog-form").dialog({
                autoOpen: false,
                height: 100,
                width: 100,
                modal: true
            });

           

          
            $("#tabs").tabs();
            $('#dialog-frame').on('load', function() {
                $("#dialog-frame").contents().find('.prelink_stats').on('click', function(e) {
                    e.preventDefault();
                    var link = $(this).attr('href');
                    var title = $(this).attr('title');
                    var last_id = $('.dialog_forms').size();
                    var elem = '<div id="dialog-form-' + last_id + '" class="dialog_forms" style="overflow:hidden;display:none;"><iframe id="dialog-frame-' + last_id + '" style="display:none;width:100%;height:auto;overflow:hidden;border : none;"></iframe></div>';
                    $('#secondary').append(elem);
                    $("#dialog-form-" + last_id).dialog({
                        autoOpen: false,
                        height: 100,
                        width: 100,
                        modal: true
                    });
                    openDialogUI("form-" + last_id, "frame-" + last_id, link, 350, 350, title);
                });
            });


            resizeIframe('widgetsFrame');
        });
	    
	    
		var serverDate;
		
		function stat_submit(url)
		{
			if (url != "no addon") {
				document.getElementById("date_form").action=url;
				submitDateForm();
			}
			else{
				$("#dialog_text").html("Extended statistics functionality is available as an optional add-on. If you'd like to get that add-on, please contact sale department: <a href=\"mailto:sales@deskalerts.com\">sales@deskalerts.com</a>");
				$("#dialog").dialog("open");
			}
		}

		function my_disable(i)
		{
			if(i==1) 
			{
				enable_onlick(document.getElementById("tcalico_0"));
				enable_onlick(document.getElementById("tcalico_1"));
				document.getElementById("range").disabled=true;
				document.getElementById("from_date").disabled=false;
				document.getElementById("to_date").disabled=false;
			}
			else
			{
				disable_onlick(document.getElementById("tcalico_0"));
				disable_onlick(document.getElementById("tcalico_1"));
				document.getElementById("range").disabled=false;
				document.getElementById("from_date").disabled=true;
				document.getElementById("to_date").disabled=true;
			}
		}
		function check_date(form)
		{
			if(form.select_date_radio[1].checked)
			{
				try{
		
					var fromDate = $.trim($("#from_date").val());
					var toDate = $.trim($("#to_date").val());
			
					if (!fromDate)
					{
                        alert('<% =resources.LNG_YOU_ENTER_WRONG_TIME_PERIOD%>'); /*"You enter wrong time period!"*/
						return false;
					}
					if(!toDate)
					{
                        alert('<% =resources.LNG_YOU_ENTER_WRONG_TIME_PERIOD%>'); /*"You enter wrong time period!"*/
						return false;
					}
				
					if (!isDate(fromDate,settingsDateFormat))
					{
                        alert('<% =resources.LNG_YOU_ENTER_WRONG_TIME_PERIOD%>'); /*"You enter wrong time period!"*/
						return false;
					}
					else
					{
						if (fromDate!="")
						{
							fromDate = new Date(getDateFromFormat(fromDate,settingsDateFormat));
							$("#start_date").val(formatDate(fromDate,saveDbDateFormat));
						}
					}
				
					if (!isDate(toDate,settingsDateFormat))
					{
                        alert('<% =resources.LNG_YOU_ENTER_WRONG_TIME_PERIOD%>'); /*"You enter wrong time period!"*/
						return false;
					}
					else
					{
						if (toDate!="")
						{
							toDate = new Date(getDateFromFormat(toDate,settingsDateFormat))
							$("#end_date").val(formatDate(toDate,saveDbDateFormat));
						}
					}
				
					if (fromDate > toDate)
					{
                        alert('<% =resources.LNG_YOU_ENTER_WRONG_TIME_PERIOD%>'); /*"You enter wrong time period!"*/
						return false;
					}
				}
				catch(e){alert(e)}
			}
			return true;
		}
		function disable_onlick(el)
		{
			if(el && !el._onclick)
			{
				el._onclick = el.onclick;
				el.onclick = "return false";
			}
		}
		function enable_onlick(el)
		{
			if(el && el._onclick)
			{
				el.onclick = el._onclick;
				el._onclick = "";
			}
		}
		
		function showLoader()
		{
		
			$.grep($(window.top.document).find("frame"), function(n,i){
				if ($(n).attr("name") == "main")
				{
					var doc = $(n)[0].contentDocument || $(n)[0].contentWindow.document;
					$("#loader",doc).fadeIn(500);
					$("#shadow",doc).fadeIn(500);
				}
			});
		}
		function hideLoader()
		{
			$.grep($(window.top.document).find("frame"), function(n,i){
				if ($(n).attr("name") == "main")
				{
					var doc = $(n)[0].contentDocument || $(n)[0].contentWindow.document;
					$("#loader",doc).fadeOut(500);
					$("#shadow",doc).fadeOut(500);
				}
			});
		}
		
		function submitDateForm() {
			if (check_date($("#date_form")[0])) {
				if (window.parent && window.parent.showLoader) { window.parent.showLoader(); }
				$("#date_form").submit();
			}
		}
		
		$(function() {
			$("#dialog").dialog({ 
				title: 'Error',
				autoOpen: false,
				modal : true,
				open: function(event, ui) { 
					$('.ui-widget-overlay').bind('click', function(){ 
						$("#dialog").dialog('close'); 
					}); 
				},
				buttons: {
					'OK': function() {
						$(this).dialog("close");
					}
				}
			});
			
			serverDate = new Date(getDateFromAspFormat("{curTime}","dd/MM/yyyy HH:mm:ss"));
			setInterval(function(){serverDate.setSeconds(serverDate.getSeconds()+1);},1000);

		    $(".date_time_param_input").datetimepicker({ dateFormat: settingsDateFormat, timeFormat: settingsTimeFormat });
		    $(".date_param_input").datepicker({ dateFormat: settingsDateFormat, showButtonPanel: true });
            $(".time_param_input").timepicker({ timeFormat: settingsTimeFormat });

			$(".delete_button").button();
			$(".close_button").button();
			
			$(".search_button").button({
				icons :{
					primary : "ui-icon-search"
				}
			});
			
			$("#statistics_show_button").button();
		});
		
		function showPrompt(content)
		{
			$("#dialog-modal > p").empty();
			$("#dialog-modal > p").append(content)
			$("#dialog-modal").dialog({
				height: 150,
				modal: true,
				resizable: false,
				draggable: false
			});
		}
		
        var settingsDateFormat = "<% = DateTimeConverter.JsDateFormat %>";
        var settingsTimeFormat = "<% = DateTimeConverter.JsTimeFormat %>";

		var responseDbFormat = "dd/MM/yyyy HH:mm:ss";
		var saveDbFormat = "dd/MM/yyyy HH:mm";
		var saveDbDateFormat = "dd/MM/yyyy";
        var uiFormat = "<% = DateTimeConverter.JsDateTimeFormat %>";
	
		function resizeIframe(id) {
		    var iframeDoc = document.getElementById(id);
		    iframeDoc.height = window.frameElement.scrollHeight - 110;
		    var h;
            if (iframeDoc.contentDocument) {

                if (iframeDoc.contentDocument.documentElement)
                    h = iframeDoc.contentDocument.documentElement.scrollHeight; //FF, Opera, and Chrome
                else
                    h = iframeDoc.contentDocument.body.scrollHeight; //ie 8-9-10

		        if (iframeDoc.height < h) iframeDoc.height = h;
		    }
		    else {
		        h = iframeDoc.contentWindow.document.body.scrollHeight; //IE6, IE7 and Chrome
		        if (iframeDoc.height < h) iframeDoc.height = h;
		    }
		}


    </script>
    
     <style>
        table
        {
            border-collapse: collapse;
            empty-cells: show;
        }
        td
        {
            border-width: 2px;
        }
    </style>

</head>
<body style="margin: 0" class="body">
    <form id="form1" runat="server">
    <div>
        <div id="dialog-modal" title="" style='display: none'>
            <p>
            </p>
        </div>
          <div id="dialog-form" style="overflow: hidden; display: none;">
            <iframe id='dialog-frame' style="width: 100%; height: 100%; overflow: hidden; border: none;
                display: none"></iframe>
        </div>
        <div id="secondary" style="overflow: hidden; display: none;">
        </div>    
        <table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
             <tr>
                 <td valign="top">
                     <table width="100%" id="table_border" cellspacing="0" cellpadding="0" class="">
                         <tr>
                             <td id="main_content_cell" class="main_cell" style="background: #ffffff">
                                 <div id="tabs" style="padding: 0px; height: 100%">
                                     <ul>
                                            <li runat="server" id="widgetTab" > <a href="#widgets" onclick="resizeIframe('widgetsFrame');" ><% =resources.LNG_OVERALL_STATISTICS%> </a></li>
                                            <li runat="server" id="alertsTab"  ><a href="#alerts"onclick="resizeIframe('alertsFrame');" > <% =resources.LNG_ALERTS_STATISTICS %> </a></li>
                                            <li runat="server" id="usersTab"  ><a href="#users" onclick="resizeIframe('usersFrame');" > <% =resources.LNG_USERS_STATISTICS %> </a></li>
                                            <li runat="server" id="surveysTab" ><a href="#surveys" onclick="resizeIframe('surveysFrame');" > <% =resources.LNG_SURVEYS_STATISTICS %> </a></li>
                                            <li runat="server" id="rssTab"  ><a href="#rss" onclick="resizeIframe('rssFrame');" > <% =resources.LNG_RSS_STATISTICS %> </a></li>
                                             <li runat="server" id="ssTab"  ><a href="#ss" onclick="resizeIframe('ssFrame');" > <% =resources.LNG_SS_STATISTICS %> </a></li>
                                            <li runat="server" id="wpTab" ><a href="#wp" onclick="resizeIframe('wpFrame');" > <% =resources.LNG_WP_STATISTICS %> </a></li>
                                      </ul>
                                  

                                      <div runat="server"  id="widgets">
                                          <iframe id="widgetsFrame" frameborder="no" src="dashboard.aspx?stat=1" width="100%" height="100%"></iframe>
                                      </div>
                                      <div runat="server"  id="alerts">
                                          <iframe id="alertsFrame" frameborder="no" src="ReportAlerts.aspx?class=1,16,32&needType=1" width="100%" height="100%"></iframe>
                                      </div>
                                      <div runat="server" id="users" >
                                          <iframe id="usersFrame" frameborder="no" src="ReportUsers.aspx" width="100%" height="100%"></iframe>
                                      </div>
                                      <div runat="server" id="surveys">
                                          <iframe id="surveysFrame" frameborder="no" src="ReportAlerts.aspx?class=64,128,256&needType=1" width="100%" height="100%"></iframe>
                                      </div>
                                      <div runat="server" id="rss">
                                           <iframe id="rssFrame" frameborder="no" src="ReportAlerts.aspx?class=5" width="100%" height="100%"></iframe>
                                      </div>
                                      <div runat="server" id="ss">
                                          <iframe id="ssFrame" frameborder="no" src="ReportAlerts.aspx?class=2" width="100%" height="100%"></iframe>
                                      </div>
                                      <div runat="server" id="wp">
                                          <iframe id="wpFrame" frameborder="no" src="ReportAlerts.aspx?class=8" width="100%" height="100%"></iframe>
                                      </div>
                                 </div>
                              </td>
                         </tr>
                     </table>
                 </td>
             </tr>
        </table>
    </div>
    </form>
</body>
</html>
