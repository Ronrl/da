﻿using System;
using DeskAlertsDotNet.Parameters;

namespace DeskAlertsDotNet.Pages
{
    public partial class CommonSettings : DeskAlertsBasePage
    {
        //use curUser to access to current user of DA control panel      
        //use dbMgr to access to database

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (!(Config.Data.IsOptionEnabled("ConfEnableAlertLangs") && Config.Data.IsOptionEnabled("MULTILANGUAGE")))
            {
                langTab.Visible = false;
                languages.Visible = false;
            }

            archiveTab.Visible = false;
            archiving.Visible = false;


            //Add your main code here
        }
    }
}