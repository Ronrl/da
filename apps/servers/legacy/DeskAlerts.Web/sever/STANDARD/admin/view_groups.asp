﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->

<%

check_session()

%>
<!-- #include file="ad.inc" -->

<%

  uid = Session("uid")

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Admin</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/style9.css" rel="stylesheet" type="text/css">
</head>
<body style="margin:0" class="body">
<%
   if(Request("offset") <> "") then 
	offset = clng(Request("offset"))
   else 
	offset = 0
   end if	

   if(Request("sortby") <> "") then 
	sortby = Request("sortby")
   else 
	sortby = "name"
   end if	

   if(Request("gname") <> "") then 
	mygname = Request("gname")
   else 
	mygname = ""
   end if	


if (uid <>"") then
Dim group_arr(9000)
Dim user_arr(9000)
Dim user_arr1(9000)
Dim user_arr2(9000)

	groups_arr = Array ("","","","","","","")
	Set RS = Conn.Execute("SELECT id FROM users WHERE id="&uid&" AND role='E'")
	if(Not RS.EOF) then
		gid = 1
		Set rs_policy = Conn.Execute("SELECT type FROM policy INNER JOIN policy_editor ON policy.id=policy_editor.policy_id WHERE editor_id=" & uid)
		if(Not rs_policy.EOF) then
			if(rs_policy("type")="A") then
				gid = 0
			end if
		end if
		if(gid=1) then
			editor_id=RS("id")
			policy_ids=""
			policy_ids=editorGetPolicyIds(editor_id)
			groups_arr = editorGetPolicyList(policy_ids, "groups_val")
			group_ids=""
			group_ids=editorGetGroupIds(policy_ids)
			group_ids=replace(group_ids, "group_id", "groups.id")
			if(group_ids="") then
				group_ids = "groups.id=0"
			end if
		end if
	else
		gid = 0
	end if
	RS.Close


'counting pages to display
   if(Request("limit") <> "") then 
	limit = clng(Request("limit"))
   else 
	limit=50
   end if



	if(mygname<>"") then                                                                                        
		if(gid = 0) then
			Set RS = Conn.Execute("SELECT COUNT(id) as mycnt FROM groups WHERE domain_id=" & Request("id") &" AND name LIKE N'%"&Replace(mygname, "'", "''")&"%'")
		else
			Set RS = Conn.Execute("SELECT COUNT(id) as mycnt FROM groups WHERE domain_id=" & Request("id") &" AND name LIKE N'%"&Replace(mygname, "'", "''")&"%' AND ("&group_ids&")")
		end if
	else
		if(gid = 0) then
			Set RS = Conn.Execute("SELECT COUNT(id) as mycnt FROM groups WHERE domain_id=" & Request("id"))
		else
			Set RS = Conn.Execute("SELECT COUNT(id) as mycnt FROM groups WHERE domain_id=" & Request("id") & " AND ("&group_ids&")")
		end if
	end if

	cnt=0

	if(Not RS.EOF) then
		cnt=RS("mycnt")
	end if

	RS.Close
  j=cnt/limit


%>
<table width="100%" border="0" cellspacing="0" cellpadding="6">
	<tr>
	<td>
	<table width="100%" cellspacing="0" cellpadding="0" class="main_table">
		<tr>
		<td width=100% height=31 class="main_table_title"><a href="view_groups.asp?id=<%Response.Write Request("id") %>" class="header_title">Groups in domain</a></td>
		</tr>
		<tr>
		<td class="main_table_body">
		<% if(Request("err")="1") then Response.Write "<br><center><b><font color=""red"">Error: this special id already exists! Please try another one.</font></b></center>" end if %>
		<div style="margin:10px;">
		<br><div align="right"></div><br>
		<b>Domain:</b> <a href="view_groups.asp?id=<%Response.Write Request("id") %>"><%
                        	Set RS = Conn.Execute("SELECT name FROM domains WHERE id=" & Request("id"))
				if(Not RS.EOF) then
					Response.Write RS("name")
				end if

			%></a><br><br>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr><td> 

<table width="100%" border="0"><tr valign="middle"><td width="225">
<form name="search_users" action="view_groups.asp" method="get">
Search groups: <input type="text" name="gname" value=""/><input type="hidden" name="id" value="<%=Request("id")%>"/></td><td width="50"> <input type="image" src="images/search_button.gif" alt="search" name="sub"/>
</td><td>
<%
if(ConfEnableCustomGroups=1) then

	if(gid = 0 OR (gid = 1 AND groups_arr(0)<>"")) then
%>

<div align="right"><a href="edit_group.asp?domain_id=<%=Request("id")%>"><img src="images/add_group_button.gif" alt="add group" border="0"></a></div>

<%
end if
end if
%>

</td>
</tr></table>
</form>

<%

	page="view_groups.asp?gname=" & mygname & "&id=" & Request("id") & "&"
	name="groups"
	Response.Write make_pages(offset, cnt, limit, page, name, sortby) 

%>

		</td></tr>
		</table>
		<table width="100%" height="100%" class="data_table" cellspacing="0" cellpadding="3">
		<tr class="data_table_title">
		<td class="table_title"><% response.write sorting("Group name","name", sortby, offset, limit, page) %></td>
		<td class="table_title" ><% response.write sorting("Users","users_cnt", sortby, offset, limit, page) %></td>
<%
	if(EDIR<>1) then
%>
		<td class="table_title" ><% response.write sorting("Computers","computers_cnt", sortby, offset, limit, page) %></td>
<% 
	end if 
%>
<%if(EDIR=1) then%>		<td class="table_title">Context</td> <%end if%>
<td class="table_title">Actions</td>
				</tr>
<%

'show main table
	if(mygname<>"") then
		if(gid=0) then
			Set RS = Conn.Execute("SELECT groups.id as id, context_id, name, custom_group, COUNT(distinct users_groups.user_id) as users_cnt, COUNT(distinct computers_groups.comp_id) as computers_cnt FROM groups LEFT JOIN users_groups ON groups.id=users_groups.group_id LEFT JOIN computers_groups ON groups.id=computers_groups.group_id WHERE domain_id= "& Request ("id") &" AND name LIKE N'%"&Replace(mygname, "'", "''")&"%' GROUP BY groups.id, name, custom_group, context_id ORDER BY "&sortby)
		else
			Set RS = Conn.Execute("SELECT groups.id as id, context_id, name, custom_group, COUNT(distinct users_groups.user_id) as users_cnt, COUNT(distinct computers_groups.comp_id) as computers_cnt FROM groups LEFT JOIN users_groups ON groups.id=users_groups.group_id LEFT JOIN computers_groups ON groups.id=computers_groups.group_id WHERE domain_id= "& Request ("id") &" AND name LIKE N'%"&Replace(mygname, "'", "''")&"%' AND ("&group_ids&") GROUP BY groups.id, name, custom_group, context_id ORDER BY "&sortby)
		end if
	else
		if(gid=0) then
			Set RS = Conn.Execute("SELECT groups.id as id, context_id, name, custom_group, COUNT(distinct users_groups.user_id) as users_cnt, COUNT(distinct computers_groups.comp_id) as computers_cnt FROM groups LEFT JOIN users_groups ON groups.id=users_groups.group_id LEFT JOIN computers_groups ON groups.id=computers_groups.group_id WHERE domain_id= "& Request ("id") &" GROUP BY groups.id, name, custom_group, context_id ORDER BY "&sortby)
		else
			Set RS = Conn.Execute("SELECT groups.id as id, context_id, name, custom_group, COUNT(distinct users_groups.user_id) as users_cnt, COUNT(distinct computers_groups.comp_id) as computers_cnt FROM groups LEFT JOIN users_groups ON groups.id=users_groups.group_id LEFT JOIN computers_groups ON groups.id=computers_groups.group_id WHERE domain_id= "& Request ("id") &" AND ("&group_ids&") GROUP BY groups.id, name, custom_group, context_id ORDER BY "&sortby)
		end if
	end if


	num=0
	Do While Not RS.EOF
		num=num+1
		if(num > offset AND offset+limit >= num) then
		cnt1=0

'		if (AD = 0 OR AD = 3) then
'			Set RS1 = Conn.Execute("SELECT id FROM users_groups WHERE group_id=" & CStr(RS("id")))
'			Do While Not RS1.EOF
'			cnt1=cnt1+1
'			RS1.MoveNext
'			loop
'		end if


        	%>
		<tr><td>
		<% Response.Write RS("name") %>
		</td><td>
		<table border="0" cellspacing="0" cellpadding="0" width="100%"><tr><td>
		<% Response.Write RS("users_cnt") %></td><td width="1px" align="right"><a href="view_user_group.asp?id=
		<% Response.Write RS("id") %>
		"><img src="images/action_icons/preview.png" alt="view users in group" width="16" height="16" border="0" hspace=5></a></td></tr></table>
		</td>
<%
		if(EDIR<>1) then
%>
		<td> 
		<table border="0" cellspacing="0" cellpadding="0" width="100%"><tr><td>
		<% Response.Write RS("computers_cnt") %></td><td width="1px" align="right"><a href="view_comp_group.asp?id=
		<% Response.Write RS("id") %>
		"><img src="images/action_icons/preview.png" alt="view computers in group" width="16" height="16" border="0" hspace=5></a></td></tr></table>
		</td>
<%
		end if
		 if (EDIR = 1) then 	
			if(RS("context_id")>0) then
			Set RS3 = Conn.Execute("SELECT name FROM edir_context WHERE id=" & RS("context_id"))
			if(Not RS3.EOF) then
				  strContext=RS3("name")
				RS3.close
			else
				strContext="&nbsp;"
			end if
			else
				strContext="&nbsp;"
			end if

			strContext=replace (strContext,",ou=",".")
			strContext=replace (strContext,",o=",".")
			strContext=replace (strContext,",dc=",".")
			strContext=replace (strContext,",c=",".")

			strContext=replace (strContext,"ou=","")
			strContext=replace (strContext,"o=","")
			strContext=replace (strContext,"dc=","")
			strContext=replace (strContext,"c=","")

			Response.Write "<td>" & strContext & "</td>"
		 end if

%>
		<td align="center">
		<%

		if (AD <> 0 OR EDIR = 1) then

		if(RS("custom_group")=1) then
			if(gid = 0 OR (gid = 1 AND groups_arr(1)<>"")) then
		%>
		<a href="edit_group.asp?id=<%=RS("id") %>&domain_id=<%=Request("id") %>"><img src="images/action_icons/edit.png" alt="edit group" width="16" height="16" border="0" hspace=5></a>
		<% end if %>
		<% if(gid = 0 OR (gid = 1 AND groups_arr(2)<>"")) then %>
		<a href="group_delete.asp?id=<%=RS("id") %>&domain_id=<%=Request("id") %>"><img src="images/action_icons/delete.gif" alt="delete group" width="16" height="16" border="0" hspace=5></a>
		<%
			end if
		else
		 response.write "&nbsp;"
		end if

		else
		%>
		<a href="edit_group.asp?id=<%=RS("id") %>"><img src="images/action_icons/edit.png" alt="edit group" width="16" height="16" border="0" hspace=5></a><a href="group_delete.asp?id=<%RS("id") %>"><img src="images/action_icons/delete.gif" alt="delete group" width="16" height="16" border="0" hspace=5></a>
		<%
		end if
		%>
		</td></tr>
		<%
		end if
	RS.MoveNext
	Loop
	RS.Close

%>			
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr><td> 


<%

	page="view_groups.asp?gname=" & mygname & "&id=" & Request("id") & "&"
	name="groups"
	Response.Write make_pages(offset, cnt, limit, page, name, sortby) 

%>

		</td></tr>
		</table>
 
                </table>
<!--		<img src="images/inv.gif" alt="" width="1" height="100%" border="0">-->
		</div>
		</td>
		</tr>
	</table>
	</td>
	</tr>
</table>


<%
else

  Response.Redirect "index.asp"

end if
%> 
</body>
</html>
<!-- #include file="db_conn_close.asp" -->