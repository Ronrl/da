﻿<% Response.CacheControl = "no-cache" %>
<% Response.AddHeader "Pragma", "no-cache" %>
<% Response.Expires = -1 %>
<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<!-- #include file="ad.inc" -->
<%
Server.ScriptTimeout = 300
Conn.CommandTimeout = 300

check_session()

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>DeskAlerts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/style9.css" rel="stylesheet" type="text/css">
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script language="javascript" type="text/javascript">

$(document).ready(function() {
	var width = $(".data_table").width();
	var pwidth = $(".paginate").width();
	if (width != pwidth) {
		$(".paginate").width("100%");
	}
	$(".search_button").button({
		icons : {
			primary : "ui-icon-search"
		}
	});
});

</script>
</head>
<script language="javascript" type="text/javascript" src="functions.js"></script>



<%

  uid = Session("uid")
  flag=1
  if(flag <> 1 AND AD=3) then
%>
<body style="margin:0px" class="iframe_body">

<script type="text/javascript">
// code to create and make AJAX request
function getFile(pURL) {
    if (window.XMLHttpRequest) { // code for Mozilla, Safari, etc 
        xmlhttp=new XMLHttpRequest();
    } else if (window.ActiveXObject) { //IE 
        xmlhttp=new ActiveXObject('Microsoft.XMLHTTP');
    }

    // if we have created the xmlhttp object we can send the request
    if (typeof(xmlhttp)=='object') {
        xmlhttp.onreadystatechange=xmlhttpResults;
        xmlhttp.open('GET', pURL, true);
        xmlhttp.send(null);
        // replace the submit button image with a please wait for the results image.
//        document.getElementById('theImage').innerHTML = '<img id="submitImage" name="submitImage" src="images/ajaxCallChangeImage_wait.gif" alt="Please Wait for AJAX Results">';
    // otherwise display an error message
    } else {
        alert('Your browser is not remote scripting enabled. You can not run this example.');
    }
}

// function to handle asynchronous call
function xmlhttpResults() {
    if (xmlhttp.readyState==4) { 
        if (xmlhttp.status==200) {
            // we use a delay only for this example
            setTimeout('loadResults()',3000);
        }
    }
}

function loadResults() {
    // put the results on the page
    if(xmlhttp.responseText=="1")
    {
	    location.href="users.asp"
    }
    else
    {
	    document.getElementById('mydiv').innerHTML = "<table width='100%' height='100%' border='0'><tr><td align='center'><b>Error: Synchronizing with Active Directory failed.</b></td></tr></table>";
    }
}


</script>
<div name="mydiv" id="mydiv">
<table width="100%" height="100%" border="0"><tr><td align="center">
<br><br>  <img src="images/ajax-loader.gif" border="0"> <br><br>  <b>Synchronizing with Active Directory. Please wait.</b>
</td>
</tr>
</table>
</div>

<%
  Session("flag") = 1
  else



%>

<body style="margin:0px" class="iframe_body">

<%

if (uid <>"") then
	showCheckbox = request("sc")
	

	search_id = clng(request("id"))
	search_type = request("type")
	if search_type = "" then search_type = "organization"

   if(Request("offset") <> "") then 
	offset = clng(Request("offset"))
   else 
	offset = 0
   end if	

   if(Request("sortby") <> "") then 
	sortby = Request("sortby")
   else 
	sortby = "name"
   end if	


   if(Request("uname") <> "") then
	myuname = Request("uname")
   else 
	myuname = ""
   end if

	if(myuname <> "") then 
		searchSQL = " (computers.name LIKE N'%"&Replace(myuname, "'", "''")&"%') "
	else
		searchSQL = ""
	end if

	gid = 0
	gsendtoall = 0
	set RS = Conn.Execute("SELECT id, only_personal FROM users WHERE id="&uid&" AND role='E'")
	if not RS.EOF then
		gid = 1
		set rs_policy = Conn.Execute("SELECT p.id, p.type, pu.user_id FROM policy p INNER JOIN policy_editor pe ON p.id=pe.policy_id LEFT JOIN policy_user pu ON pu.policy_id = p.id AND pu.user_id = 0 WHERE pe.editor_id=" & uid)
		if not rs_policy.EOF then
			if rs_policy("type")="A" then 'can send to all
				gid = 0
			elseif not IsNull(rs_policy("user_id")) then
				gid = 0
				gsendtoall = 1
			end if
		end if
		if gid = 1 or gsendtoall = 1 then
			editor_id=RS("id")
			policy_ids=""
			policy_ids=editorGetPolicyIds(editor_id)
		end if
	end if
	RS.Close

	cnt=0
	if (search_type <>"") then
	
		showSQL = 	" SET NOCOUNT ON" & vbCrLf &_
					" IF OBJECT_ID('tempdb..#tempOUs') IS NOT NULL DROP TABLE #tempOUs" & vbCrLf &_
					" CREATE TABLE #tempOUs (Id_child BIGINT NOT NULL, Rights BIT NOT NULL);" & vbCrLf &_
					" IF OBJECT_ID('tempdb..#tempComputers') IS NOT NULL DROP TABLE #tempComputers" & vbCrLf &_
					" CREATE TABLE #tempComputers (comp_id BIGINT NOT NULL);" & vbCrLf &_
					" DECLARE @now DATETIME;" & vbCrLf &_
					" SET @now = GETDATE(); "
					
		OUsSQL=""

		if(search_type = "ou") then
			if Request("search") = "1" then
				OUsSQL = OUsSQL & "WITH OUs (id) AS("
				OUsSQL = OUsSQL &" SELECT CAST("&search_id&" AS BIGINT) "
				OUsSQL = OUsSQL &_
						"	UNION ALL" & vbCrLf &_
						"	SELECT Id_child" & vbCrLf &_
						"	FROM OU_hierarchy h" & vbCrLf &_
						"	INNER JOIN OUs ON OUs.id = h.Id_parent" & vbCrLf &_
						") " & vbCrLf &_
						"INSERT INTO #tempOUs (Id_child, Rights) (SELECT DISTINCT id, 0 FROM OUs) Option (MaxRecursion 10000); "
			else
				OUsSQL = OUsSQL &" INSERT INTO #tempOUs (Id_child, Rights) VALUES ("&search_id&", 0)"
			end if
		elseif(search_type = "DOMAIN") then
			OUsSQL = OUsSQL &" INSERT INTO #tempOUs (Id_child, Rights) (SELECT id, 0 FROM OU WHERE Id_domain = "&search_id&")"
		else
			OUsSQL = OUsSQL & " INSERT INTO #tempOUs (Id_child, Rights) (SELECT id, 0 FROM OU) "
		end if

		if (gid = 1) then 
			OUsSQL = OUsSQL & vbCrLf &_
				" ;WITH OUsList (Id_parent, Id_child) AS (" & vbCrLf &_
				" 	SELECT Id_child AS Id_parent, Id_child FROM #tempOUs" & vbCrLf &_
				"	UNION ALL" & vbCrLf &_
				"	SELECT h.Id_parent, l.Id_child" & vbCrLf &_
				"	FROM OU_hierarchy as h" & vbCrLf &_
				"	INNER JOIN OUsList as l" & vbCrLf &_
				"	ON h.Id_child = l.Id_parent)" & vbCrLf &_ 
				" UPDATE #tempOUs" & vbCrLf &_
				" SET Rights = 1" & vbCrLf &_
				" FROM #tempOUs t" & vbCrLf &_
				" INNER JOIN OUsList l" & vbCrLf &_
				" ON l.Id_child = t.Id_child" & vbCrLf &_
				" LEFT JOIN policy_ou p ON p.ou_id=l.Id_parent AND " & Replace(policy_ids, "policy_id", "p.policy_id") & vbCrLf &_
				" LEFT JOIN ou_groups g ON g.ou_id=l.Id_parent" & vbCrLf &_
				" LEFT JOIN policy_group pg ON pg.group_id=g.group_id AND " & Replace(policy_ids, "policy_id", "pg.policy_id") & vbCrLf &_
				" WHERE NOT p.id IS NULL OR NOT pg.id IS NULL"
		end if


		checkedOUs=request("ous")
		checkedObject = ""
		if(InStr(checkedOUs, search_id)>0) then
			'checkedObject = "checked"
		end if

	'counting pages to display
	   if(Request("limit") <> "") then 
		limit = clng(Request("limit"))
	   else 
		limit=25
	   end if

		computersSQL = " INSERT INTO #tempComputers(comp_id)(" & vbCrLf &_
					" 	SELECT DISTINCT Id_comp" & vbCrLf &_
					" 	FROM OU_comp c" & vbCrLf &_
					" 	INNER JOIN #tempOUs t" & vbCrLf &_
					" 	ON c.Id_OU=t.Id_child "
					
		whereSQL=""
		if (searchSQL<> "") then
			computersSQL = computersSQL & " LEFT JOIN computers ON c.Id_comp = computers.id"
			whereSQL = " WHERE " & searchSQL		
		end if
					
									
		if (gid = 1) then 
			computersSQL = computersSQL & vbCrLf &_
					" LEFT JOIN policy_computer p ON p.comp_id=c.Id_comp AND "&policy_ids& vbCrLf &_
					" WHERE t.Rights = 1 OR NOT p.id IS NULL "
			if (searchSQL<>"") then 
				computersSQL = computersSQL & " AND " & searchSQL
			end if
		else
			computersSQL = computersSQL & whereSQL
		end if
		
		
		computersSQL = computersSQL & ") "
		
		
		if (search_type <> "ou") then
			computersSQL = computersSQL & vbCrLf &_
						" INSERT INTO #tempComputers(comp_id)(" & vbCrLf &_
						" SELECT DISTINCT computers.id FROM computers"
			if (gid = 1) then 
				computersSQL = computersSQL & " INNER JOIN policy_computer p ON p.comp_id=computers.id AND "&policy_ids
			end if
				computersSQL = computersSQL & vbCrLf &_	
						" LEFT JOIN #tempComputers t" & vbCrLf &_
						" ON t.comp_id=computers.id" & vbCrLf &_
						" WHERE t.comp_id IS NULL "
			if (search_type = "DOMAIN") then
				computersSQL = computersSQL & " AND computers.domain_id = "&search_id
			end if 
				if (searchSQL<> "") then
					computersSQL = computersSQL & " AND " & searchSQL	
				end if
				computersSQL = computersSQL & ") "	
		end if
		
		computersSQL = computersSQL & vbCrLf &_
							" SELECT COUNT(1) as cnt FROM #tempComputers t INNER JOIN computers ON t.comp_id = computers.id" & vbCrLf &_
							" SELECT computers.id as id, context_id, computers.name as name, next_request, standby, computers.domain_id, domains.name AS domain_name, reg_date, last_date, convert(varchar,last_request) as last_request1, last_request," & vbCrLf &_
							" edir_context.name as context_name" & vbCrLf &_
							" FROM #tempComputers t" & vbCrLf &_
							" INNER JOIN computers ON t.comp_id = computers.id" & vbCrLf &_
							" LEFT JOIN domains ON domains.id = computers.domain_id" & vbCrLf &_
							" LEFT JOIN edir_context ON edir_context.id = computers.context_id" & vbCrLf &_
							" ORDER BY " & sortby & vbCrLf &_
							" DROP TABLE #tempOUs" & vbCrLf &_
							" DROP TABLE #tempComputers"
		
		showSQL = showSQL & OUsSQL & computersSQL
        Response.Write showSQL
		set RS = Conn.Execute(showSQL)
		if not RS.EOF then
			cnt=RS("cnt")
			set RS = RS.NextRecordSet
		end if
		j=cnt/limit
	end if
%>

<form name="search_computers" action="computers_new.asp?sc=<%=showCheckbox %>&id=<%=search_id %>&type=<%=search_type %>&ous=<%=checkedOUs %>&offset=0&limit=<%=limit %>&sortby=<%=sortby %>" method="post"><table width="100%" border="0"><tr valign="middle"><td width="240">
<%=LNG_SEARCH_COMPUTERS %>: <input type="text" name="uname" value="<%=HTMLEncode(myuname) %>"/></td>
<td width="1%">
	<a name="sub" class="search_button" onclick="javascript:document.forms[0].submit()"><%=LNG_SEARCH %></a>
	<input type="hidden" name="search" value="1" />
</td>
<%
if myuname <> "" or Request("search") = "1" then
response.write "<td> "& LNG_YOUR_SEARCH_BY &" """&HTMLEncode(myuname)&""" "& LNG_KEYWORD &":</td>"
end if
%>
<td>
<%
if(AD=3) then
%>

<!-- <div align="right"><a href="ad_sync_form.asp" target="_blank"><img src="images/refresh_button.gif" alt="refresh" border="0"></a></div><br> -->

<%
else
%>

<%
end if
%>
</td></tr></table>
</form>


<%


if(cnt>0) then
	page="computers_new.asp?sc="&showCheckbox&"&id="&search_id&"&type="&search_type&"&ous="&checkedOUs&"&uname=" & myuname &"&search="&Request("search")&"&"
	name= LNG_COMPUTERS
	Response.Write make_pages(offset, cnt, limit, page, name, sortby) 

if (AD=0) then
	response.write "<br><A href='#' onclick=""javascript: return LINKCLICK('"&LNG_COMPUTERS &"');""><img src='images/langs/"& default_lng &"/delete_button.gif' border='0'/></a><br/><br>"
	response.write "<form action='delete_objects.asp' name='delete_objs' id='delete_objs' method='post'><input type='hidden' name='objType' value='computers'>"
end if

%>



		<table height="100%" class="data_table" cellspacing="0" cellpadding="3">
		<tr class="data_table_title">
<%if(showCheckbox=1) then%>
<td width="10"><input type='checkbox' name='select_all' id='select_all' onclick='javascript: select_all_objects_ou();'></td><%end if%>

		<td class="table_title"><% response.write sorting(LNG_COMPUTER_NAME,"name", sortby, offset, limit, page) %></td>

<% if (AD = 3) then %>		<td class="table_title"><% response.write sorting(LNG_DOMAIN,"domains.name", sortby, offset, limit, page) %></td> <%end if%>

<% if (AD = 0) then %>		<td class="table_title"><% response.write sorting(LNG_REGISTERED,"reg_date", sortby, offset, limit, page) %></td> <% end if %>

<% if(showCheckbox<>1) then		%>		

<% if (EDIR = 1) then %>		<td class="table_title"><%=LNG_CONTEXT %></td> <% end if %>

		<td class="table_title"><% response.write sorting(LNG_ONLINE,"last_request1", sortby, offset, limit, page) %></td>
		<td class="table_title"><% response.write sorting(LNG_LAST_ACTIVITY,"last_request", sortby, offset, limit, page) %></td>
<% if (AD = 3) then %>		<td class="table_title"><%=LNG_ACTIONS %></td> <% end if%>
<%end if%>				
				</tr>


<%

'show main table

	num=offset
	if(Not RS.EOF) then RS.Move(offset) end if
	Do While Not RS.EOF
		num=num+1
		if((offset+limit) < num) then Exit Do end if
		if(num > offset AND offset+limit >= num) then
	reg_date=RS("reg_date")
	last_date=RS("last_date")
	if(Not IsNull(RS("last_request"))) then
		last_activ = CStr(RS("last_request1"))

	        mydiff=DateDiff ("n", RS("last_request"), now_db)
			if(RS("next_request")<>"") then
				online_counter = Int(CLng(RS("next_request"))/60) * 2
			else
				online_counter = 2
			end if
					
		if(mydiff > online_counter) then
			online="<img src='images/offline.gif' alt='"&LNG_OFFLINE&"' title='"&LNG_OFFLINE&"' width='11' height='11' border='0'>"
		else
			online="<img src='images/online.gif' alt='"&LNG_ONLINE&"' title='"& LNG_ONLINE &"' width='11' height='11' border='0'>"
		end if
	else
		last_activ = "&nbsp;"
		online="<img src='images/offline.gif' alt='"&LNG_OFFLINE&"' title='"&LNG_OFFLINE&"' width='11' height='11' border='0'>"
	end if
	if(RS("standby")=1) then
		online="<img src='images/standby.gif' alt='"& LNG_STAND_BY &"' title='"& LNG_STAND_BY &"' width='11' height='11' border='0'>"
	end if

	if(IsNull(RS("name"))) then
	cname= LNG_UNREGISTERED
	else
	cname = HTMLEncode(RS("name"))
	end if

	domain="&nbsp;"
	if (AD = 3) then 
		if(Not IsNull(RS("domain_name"))) then
			if(RS("domain_name")<>"") then
				domain=HTMLEncode(RS("domain_name"))
			end if
		end if

		if (EDIR = 1) then 
			strContext="&nbsp;"
			if(Not IsNull(RS("context_name"))) then
				strContext=HTMLEncode(RS("context_name"))
			end if
		end if 
	end if
	strObjectID=RS("id")&""
        Response.Write "<tr>"
if(showCheckbox=1) then %>
	<td><input type="checkbox" name="objects" id="object<%=strObjectID%>" value="<%=strObjectID%>" <%=checkedObject %> onclick="checkboxClicked(event);"></td>
<%
end if
response.write "<td>" + cname + "</td><td align=center>"

if (AD = 0) then
	Response.Write reg_date
	Response.Write "</td><td align=center>"
end if
if (AD = 3) then
	Response.Write domain
	Response.Write "</td>"
end if

if(showCheckbox<>1) then

if (EDIR = 1) then
	strContext=replace (strContext,",ou=",".")
	strContext=replace (strContext,",o=",".")
	strContext=replace (strContext,",dc=",".")
	strContext=replace (strContext,",c=",".")

	strContext=replace (strContext,"ou=","")
	strContext=replace (strContext,"o=","")
	strContext=replace (strContext,"dc=","")
	strContext=replace (strContext,"c=","")
	Response.Write "<td align=center>"
	Response.Write strContext
	Response.Write "</td>"
 end if


	Response.Write "<td align=center>"
	Response.Write online & "</td>"
	response.write"<td align=center>" & last_activ & "</td>"
 if (AD = 0) then 	
'	Response.Write "<td align=center>" 

'	response.write "<a href='user_delete.asp?id=" & CStr(RS("id")) & "' onclick=""javascript: return LINKCLICK();""><img src='images/action_icons/delete.gif' alt='delete user' width='16' height='16' border='0' hspace=5></a></td>"
 end if
 if (AD = 3) then 	
	Response.Write "<td align=center nowrap><a href='#' onclick=""window.open('comp_view_details.asp?id=" & strObjectID & "','',350,350);""><img src='images/action_icons/preview.png' alt='"& LNG_VIEW_COMPUTER_DETAILS &"' title='"& LNG_VIEW_COMPUTER_DETAILS &"' width='16' height='16' border='0' hspace=5></a></td>"
 end if
	end if
	Response.Write "</tr>"
	end if

	RS.MoveNext
	Loop
	RS.Close


%>         
			</table>
<%

if (AD=0) then
	response.write "</form><br/><A href='#' onclick=""javascript: return LINKCLICK('"&LNG_COMPUTERS &"');""><img src='images/langs/"& default_lng &"/delete_button.gif' border='0'/></a><br><br>"
end if

	Response.Write make_pages(offset, cnt, limit, page, name, sortby) 
else
	Response.Write "<center><b>"& LNG_THERE_ARE_NO_COMPUTERS &".</b><br><br></center>"
end if

else

  Response.Redirect "index.asp"

end if

end if

%> 
</body>
</html>
<!-- #include file="db_conn_close.asp" -->