﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%

check_session()

'script for change alerts templates

uid = Session("uid")

if (uid <> "")  then

if(Request("sub1") = "Save" OR Request("sub1") = "Add") then
'update templates in database
	    top = Replace(Request("top_templ"), "'", "''")
	    bottom=Replace(Request("bottom_templ"), "'", "''")
	    name=Replace(Request("name"), "'", "''")
		id=Request("id")

	if(request("s_preview")="1") then
            SQL = "INSERT INTO templates_preview ([top], bottom, name) VALUES(N'"&top&"', N'"&bottom&"',N'"&name&"')"
	    Conn.Execute(SQL)
		Set rs1 = Conn.Execute("select @@IDENTITY newID from templates_preview")
		preview_id=rs1("newID")
		rs1.Close
		Response.Redirect "template_edit.asp?id="&id&"&preview_id="&preview_id&"&s_preview=1"
	else
		'check for existing name
		fl_check = 0
		Set rs_check = Conn.Execute("select id from templates WHERE name='" & Replace(name, "'", "''") & "'")
		if(rs_check.eof) then
			fl_check = 1
		else
			if(clng(rs_check("id"))=clng(id)) then
				fl_check = 1
			end if
		end if

		if(fl_check = 1) then
			if(Request("sub1") = "Save") then
				if(id="1") then
					SQL = "UPDATE templates SET [top]=N'" + top + "', bottom=N'" + bottom + "' WHERE id="&id
				else	  
						SQL = "UPDATE templates SET [top]=N'" + top + "', bottom=N'" + bottom + "', name=N'"&name&"' WHERE id="&id
				end if
				Conn.Execute(SQL)
			else
		            SQL = "INSERT INTO templates ([top], bottom, name, sender_id) VALUES(N'"&top&"', N'"&bottom&"',N'"&name&"', "&uid&")"
					Conn.Execute(SQL)
					Set rs1 = Conn.Execute("select @@IDENTITY newID from templates")
					id=rs1("newID")
					rs1.Close
			end if
			Response.Redirect "templates.asp"
		else
			error_descr = "'"&name&"' " & LNG_TEMPLATE_NAME_ERROR_DESC & "."		
		end if
	end if

    
end if

'show edit form
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>DeskAlerts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/style9.css" rel="stylesheet" type="text/css">
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>

<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/preview.js?v=9.2.1.31"></script>
</head>
<!-- TinyMCE -->
<script language="javascript" type="text/javascript" src="jscripts/tiny_mce/tinymce.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/da_tinymce.min.js"></script>
<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$(".cancel_button").button();
		$(".save_button").button();
		$(".preview_button").button();
	});
	var newTinySettings = {<%= newTinySettings()%>};
	initTinyMCE("<%=lcase(default_lng)%>", "textareas", "", null, null, newTinySettings);

	function show_preview()
	{
		document.getElementById("s_preview").value=1;
		document.getElementById("templates_form").submit();
	}

function mypreview()
{
	var obj = document.getElementById("preview2");
	document.body.removeChild(obj);
	
	obj = document.getElementById("preview1");
	document.body.removeChild(obj);


//	document.getElementsByName('preview2')[0].style.display = (document.getElementsByName('preview2')[0].style.display=='none')?'':'none'; 
//	document.getElementsByName('preview1')[0].style.display = (document.getElementsByName('preview1')[0].style.display=='none')?'':'none'; 
	return false;
}

function LINKCLICK() {
  var yesno = confirm ("Are you sure that you wish to cancel this page?","");
  if (yesno == false) return false;
  return true;
}

function my_redirect()
{
	if(LINKCLICK()==true)
	{
		location.href="templates.asp";
	}
}


function my_sub()
{
	if(document.getElementById('t_name').value==""){
		alert("<%=LNG_PLEASE_ENTER_TEMPLATE_TITLE %>");
		return false;
	}
	else{
		if(document.getElementById('t_name').value.length>255) {
		alert("<%=LNG_TITLE_SHOULD_BE_LESS_THEN %>.");
		return false;
	}	
		return true;
	}
}


	function showAlertPreview()
	{
		var data = new Object();
		
		data.create_date = "<%=al_create_date%>";
	
		var fullscreen = false;
		var ticker =  0;
		var acknowledgement =  0; 
		
		var alert_width = 500;
		var alert_height = 400;
		var alert_title = "Sample title";
		var alert_html = "Sample alert";
		
		var top_template;
		var bottom_template;
		
		var top_template;
		var bottom_template;
	
			
	
		top_template = tinyMCE.get('elm1').getContent();
		
		bottom_template = tinyMCE.get('bottom_templ').getContent();
		
		
		data.top_template = top_template;
		data.bottom_template = bottom_template;
		data.alert_width = alert_width;
		data.alert_height = alert_height;
		data.ticker = ticker;
		data.fullscreen = fullscreen;
		data.acknowledgement = acknowledgement;
		data.alert_title = alert_title;
		data.alert_html = "<p>" + alert_html + "</p>";

		initAlertPreview(data);
		
		return false;
	}


<%

id=Request("id")

if(request("s_preview")="1") then
	preview_id=Request("preview_id")
	Set RS = Conn.Execute("SELECT [top], [bottom], name FROM templates_preview WHERE id="& preview_id)
else
	if(Request("id")<>"") then
		Set RS = Conn.Execute("SELECT [top], [bottom], name FROM templates WHERE id="& id)
	end if
end if
%>


</script>
<!-- /TinyMCE -->

<body style="margin:0px" class="body">


<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	<tr>
	<td>
	<table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
		<tr>
		<td width=100% height=31 class="main_table_title"><a href="" class="header_title"><%=LNG_TEMPLATE %></a></td>
		</tr>
		<tr>
		<td class="main_table_body" height="100%">
		<div style="margin:10px;">
		<span class="work_header"><%=LNG_EDIT_TEMPLATES %></span>
		<br><br>
<form id="templates_form" action="template_edit.asp" method="post" action="">
<input type="hidden" name="s_preview" id="s_preview" value="0" />
<%if (id<>"") then %>
<input type="hidden" name="sub1" value="Save" />
<%else%>
<input type="hidden" name="sub1" value="Add" />
<%end if%>
<input type="hidden" name="id" value="<% response.Write id %>" />

<font color="red"><%=error_descr %></font>

<table cellpadding=4 cellspacing=4 border=0>
<tr><td><%=LNG_NAME %>:</td><td>
<% if(Request("id")="1") then %>
	<input type="text" size="50" value="<%=RS("name") %>" disabled/>
	<input type="hidden" id="t_name" name="name" value="<%=RS("name") %>" />
<% else %>
	<input type="text" size="50" id="t_name" name="name" value="<% if(request("s_preview")="1" OR Request("id")<>"") then Response.Write RS("name") end if %>">
<% end if %>
</td></tr>
<tr><td><%=LNG_TOP_TEMPLATE %>:</td><td>
	<textarea id="elm1" name="top_templ" rows="15" cols="80" style="width: 100%">
	<% 
	 if(request("s_preview")="1" OR Request("id")<>"") then  Response.Write HtmlEncode(RS("top")) end if 
	%>
	</textarea>
</td></tr>
<tr><td><%=LNG_BOTTOM_TEMPLATE %>:</td><td>
	<textarea id="bottom_templ" name="bottom_templ" rows="15" cols="80" style="width: 100%">
	<% 
		if(request("s_preview")="1" OR Request("id")<>"") then  Response.Write HtmlEncode(RS("bottom")) end if
	%>
	</textarea>
</td></tr>
</table>

<div align="right">
<a class="preview_button" href='#' onclick="javascript: showAlertPreview();"><%=LNG_PREVIEW%></a> 
<a class="save_button" onclick="javascript: if(my_sub()) {$('#templates_form').submit()}"><%=LNG_SAVE%></a>
<a class="cancel_button" onclick="javascript:my_redirect(); return false;"><%=LNG_CANCEL %></a>
</div>
</form>

	 <img src="images/inv.gif" alt="" width="1" height="100%" border="0">
		</div>
		</td>
		</tr>
	</table>
	</td>
	</tr>
</table>


<%if (request("s_preview")=1) then %>

<div id="preview2" name="preview2" STYLE="display:block; position:absolute; bottom:10px; right:10px; background-image: url(images/alert_preview.gif);" class="div_preview">

<% else %>

<div id="preview2" name="preview2" STYLE="display:none; position:absolute; bottom:10px; right:10px;">

<% end if%>
<% if preview_id <>"" then %>
<iframe style="position:absolute;" src="<%= alerts_folder & "preview.asp?id=1&template=" & preview_id &"&template_preview=1"%>" frameborder="0" class="iframe_preview"></iframe>
<% end if %>
</div>
<%if (request("s_preview")=1) then %>

<div id="preview1" name="preview1" STYLE="display:block; position:absolute; cursor: pointer; background-image: url(images/close_preview.gif);" onClick="mypreview();" class="preview_close"></div>

<% else %>

<div id="preview1" name="preview1" STYLE="display:none; position:absolute; bottom:295px; right:30px; background-color: #FFFFFF;"></div>

<% end if%>


<%

else
  Response.Redirect "index.asp"

end if
%> 
</body>
</html>
<!-- #include file="db_conn_close.asp" -->