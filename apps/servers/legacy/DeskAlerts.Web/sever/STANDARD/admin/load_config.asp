﻿<%

Response.Buffer = True

Set Conn = Server.CreateObject("ADODB.Connection")

Server.ScriptTimeout = 60
Conn.CommandTimeout = 30

if Session("db_use_backup") = 1 then
	if db_backup_stop_working = 1 then
		Response.End
	else
		if db_backup_stop_working <> 1 and db_backup_no_redirect <> 1 then
			On Error Resume Next
			Err.Clear
		end if
		Conn.Open Back_DBConnectionString
		showError 1
	end if
elseif db_use_backup = 1 then
	On Error Resume Next
	Err.Clear
	Conn.Open DBConnectionString
	if Err.Number <> 0 then
		if db_backup_stop_working = 1 or db_backup_no_redirect = 1 then
			On Error Goto 0
		end if
		Err.Clear
		Server.ScriptTimeout = Server.ScriptTimeout + 30
		Conn.Open Back_DBConnectionString
		showError 1
		Session("db_use_backup") = 1
		if db_backup_stop_working = 1 then
			Response.End
		elseif db_backup_no_redirect <> 1 then
			Response.Redirect "index.asp"
		end if
	end if
else
	if db_backup_stop_working <> 1 and db_backup_no_redirect <> 1 then
		On Error Resume Next
		Err.Clear
	end if
	Conn.Open DBConnectionString
	showError 0
end if

Conn.Execute("SET ANSI_WARNINGS OFF")
showError Session("db_use_backup")

Conn.Execute("SET LANGUAGE " & language)
showError Session("db_use_backup")

'Conn.Execute("UPDATE version SET version=version WHERE id=1")
'showError Session("db_use_backup")

Set rsConfSettings = Conn.Execute("SELECT name, val FROM settings")
showError Session("db_use_backup")
On Error Goto 0
do while not rsConfSettings.EOF
	sName = rsConfSettings("name") & ""
	sVal = rsConfSettings("val") & ""

	If InStr(sName, "Conf") = 1 Then
		sName = Replace(sName, ":", "")
		sVal = Replace(sVal, """", """""")
		Execute(sName & " = """ & sVal & """")
	End If
	rsConfSettings.MoveNext
loop

sub showError(db_back_)
	if db_back_ = 1 then
		win_ = back_win_auth
	else
		win_ = win_auth
	end if
	if Err.Number <> 0 then
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<title>Deskalerts</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">	
	<link href="css/style9.css" rel="stylesheet" type="text/css">
</head>
<body class="login_body">
	<table height="100%" border="0" align="center" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<div align="center"><img src="images/top_logo.gif" alt="DeskAlerts Control Panel" border="0"></div>
			<table id="login_form" border="1" bordercolor="#000000" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align=center>
			<tr>
				<td width=100% height=31 class="theme-colored-back"><div class="header_title">Can't connect to database. Please check your database settings.</div></td>
			</tr>
			<tr>
				<td bgcolor="#ffffff" class="folders">
					<table border="0" cellpadding="2" cellspacing="2">
					<tr>
						<td>Error source</td>
						<td><%= Err.Source %></td>
					</tr>
					<tr>
						<td>Error number</td>
						<td>0x<%= Hex(Err.Number) %></td>
					</tr>
					<tr>
						<td>Error description</td>
						<td><%= Err.Description %></td>
					</tr>
<% if win_ = "1" and ( _
		Err.Number = -2147467259 or _
		Err.Number = -2147217911 or _
		Err.Number = -2147217900 or _
		Err.Number = -2147217843 _
	) then %>
					<tr>
						<td colspan="2" style="padding:10px">
							The anonymous account in your Internet Information Server (IIS) have no rights to access database.<br/>
							Please check Troubleshooting section in Administrators Manual at: <a href="http://www.alert-software.com/support/documentation/">http://www.alert-software.com/support/documentation/</a>.
						</td>
					</tr>
<% elseif Err.Number = -2147217865 then %>
					<tr>
						<td colspan="2" style="padding:10px">
							Can't find DeskAlerts tables in database.<br/>
							Please try to reinstall the DeskAlerts server.
						</td>
					</tr>
<% end if %>
					</table>
				</td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
</body>
</html>
 <%
		Response.End
	end if
end sub

%>