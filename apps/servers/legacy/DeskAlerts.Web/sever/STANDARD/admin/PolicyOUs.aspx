﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PolicyOUs.aspx.cs" Inherits="DeskAlertsDotNet.Pages.PolicyOUs" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="css/style9.css" rel="stylesheet" type="text/css">
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
    <script language="javascript">
        var selectedNodes = {};

        function getCheckedOus()
        {
            return selectedNodes;
        }
        function handleTreeClick(event)
        {
            var TreeNode = event.srcElement || event.target ;

            if (TreeNode.tagName == "INPUT" && TreeNode.type == "checkbox")
            {
                var value = TreeNode.nextSibling.href.substring(TreeNode.nextSibling.href.indexOf(",") + 3, TreeNode.nextSibling.href.length - 2);

                while (value.indexOf("\\") != -1) {
                    value = value.substring(value.indexOf("\\") + 2);
                }
                if(TreeNode.checked)
                {


                    selectedNodes[value] = TreeNode.nextSibling.text;
                   // alert(value);

                }
                else
                { 

                    delete selectedNodes[value];

                }
            }
        }
        
    </script>
</head>
    
<body>
    <form id="form1" runat="server">
    <div>
    <asp:treeview runat="server" id="orgTree" SelectedNodeStyle-BackColor="#010161" SelectedNodeStyle-ForeColor="White"  SelectedNodeStyle-VerticalPadding="0"></asp:treeview>
    </div>
    </form>
</body>
</html>
