﻿using System;
using System.Web.Services;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Licensing;

namespace DeskAlertsDotNet.Pages
{
    public partial class Reminder : DeskAlertsBasePage
    {
        //use curUser to access to current user of DA control panel

        protected int days;
        protected int licenseDaysLeft;
        protected string contractDate;
        protected string licenseExpireDate;
        protected string version;
        protected static string[] prolongKeys = {"vPLj~d!xM7NO#%Dn", "R?fw6s@$EJ21jVci", "HIl5fm@k%a3r#BN2", "CjVGw7~5%DSq0@Tg", "rt6L%Jb6Do~cWDIO", "y%Xh>>0$gPvFBrZ9", "!D>Radhk8d?L^p5%", "#>M~4HN@4Rzau1mK", "oL^dD5P@Dq8yQ6Ew", "NH?$$C4Dzcud4d08", "X>n^a4^S5q18pWQu", "nzUl28U5g?~oSy0f", "Q#!i7!33lSZG2t&g", "Kz8D4rKwVs+d3eiB", "5DOSaXB0uyQ4b%T~"};
        protected static string[] usedProlongKeysArray;
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            using (var databaseManager = new DBManager())
            {
                DataRow infoRow = databaseManager.GetDataByQuery("select DATEDIFF(DAY, GETDATE(), v.license_expire_date) as license_days_left, datediff(day, getdate(), v.contract_date) as days, contract_date, license_expire_date, version from version v").First();

                licenseDaysLeft = infoRow.GetInt("license_days_left");
                days = infoRow.GetInt("days");
                contractDate = infoRow.GetDateTime("contract_date").ToString("dd-MM-yyyy");
                licenseExpireDate = infoRow.GetDateTime("license_expire_date").ToString("dd-MM-yyyy");
                version = infoRow.GetString("version");

                licenseExpiredDiv.Visible = licenseDaysLeft <= 0;
                expireInDaysDiv.Visible = days > 0;
                expiredDiv.Visible = days <= 0;

                contactDiv.Visible = days > 0;
                nonButtonDiv.Visible = string.IsNullOrEmpty(Request["action"]) || Request["action"] == "button";
            }
        }

        [WebMethod]
        public static string ProlongLicense(string prolongKeyValue)
        {
            try
            {
                using (var databaseManager = new DBManager())
                {
                    string sqlQuery = "SELECT license_expire_date, used_prolong_keys FROM version";
                    DateTime licenseExpireDate = databaseManager.GetDataByQuery(sqlQuery).GetDateTime(0, "license_expire_date");
                    string usedProlongKeysString = databaseManager.GetDataByQuery(sqlQuery).GetString(0, "used_prolong_keys");

                    if (usedProlongKeysString != null)
                    {
                        usedProlongKeysArray = usedProlongKeysString.Trim().Split(' ');
                    }

                    if ((Array.IndexOf(usedProlongKeysArray, prolongKeyValue)) == -1)
                    {
                        if((Array.IndexOf(prolongKeys, prolongKeyValue)) != -1)
                        {
                            string newUsedProlongKeysString = usedProlongKeysString + " " + prolongKeyValue;
                            LicenseManager.ProlongLicense(newUsedProlongKeysString, databaseManager);
                        }
                        else
                        {
                            return "This is wrong key";
                        }
                    }
                    else
                    {
                        return "This key already used!";
                    }
                }

                return "OK";
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                Logger.Debug(ex);
                throw;
            }
        }
    }
}