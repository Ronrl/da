﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<!-- #include file="md5.asp" -->
<%

check_session()

uid = Session("uid")

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>DeskAlerts</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link href="css/style9.css" rel="stylesheet" type="text/css">
	<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
	<script language="javascript" type="text/javascript">
		$(function() {
			$(".cancel_button").button();
			$(".save_button").button();
		});
	</script>
</head>

<body style="margin:0px" class="body">

<%
if (uid <> "")  then
if(request("a")="2") then
		'update all to zero
		SQL = "UPDATE settings SET val=0 WHERE s_part = 'E'"
		Conn.Execute(SQL)
		
		'update all from form
		For Each item In request.form
			'use prefix Conf to identify settings
			if(InStr(item, "Conf")>0) then
				SQL = "UPDATE settings SET val='" & request(item) & "' WHERE name='" & item & "' AND s_part = 'E'"
				Conn.Execute(SQL)
			end if
		Next
end if
%>
<table width="100%" border="0" cellspacing="0" height="100%">
	<tr>
	<td>
	<table width="100%" height="100%" cellspacing="0" cellpadding="0">
		<tr>
		<td height="100%">
		<div style="margin:10px;">

<span class="work_header"><%=LNG_EDIRECTORY_SETTINGS %></span><br/>
<%if(request("a")="2") then%>
	<b><%=LNG_EDIRECTORY_SETTINGS_HAVE_BEEN_SUCESSFULLY_CHANGED %>.</b>
<%end if%>
<form method="post" action="settings_ed_settings.asp">
<table border="0"><tr><td>
<%

Set rsSettings = Conn.Execute("SELECT * FROM settings WHERE s_part='E'")
do while not rsSettings.EOF

	sName = rsSettings("name")
	sVal = ""
	if(rsSettings("s_type")="C") then
		if(rsSettings("val")=1)then
			sVal = "checked"
		end if
	else
		sVal = rsSettings("val")
	end if

Select Case sName
  case "ConfEDMobile"
    sConfEDMobileVal = sVal
  case "ConfEDMail"
    sConfEDMailVal = sVal
  case "ConfEDMember"
    sConfEDMemberVal = sVal
  case "ConfEDUser"
    sConfEDUserVal = sVal
  case "ConfEDGroup"
    sConfEDGroupVal = sVal	
end select
  

	rsSettings.MoveNext
loop
%>
	<table border="0">
	<tr><td colspan="3"><b><%=LNG_ATTRIBUTES %></td></tr>
		<tr><td>&nbsp;</td><td width="100"><%=LNG_EMAIL %>: </td><td><input type="text" name="ConfEDMail" value="<%=sConfEDMailVal %>" size="30" /></td></tr>
		<tr><td>&nbsp;</td><td><%=LNG_MOBILE_PHONE %>: </td><td><input type="text" name="ConfEDMobile" value="<%=sConfEDMobileVal %>" size="30" /></td></tr>
		<tr><td>&nbsp;</td><td><%=LNG_MEMBERSHIP %>: </td><td><input type="text" name="ConfEDMember" value="<%=sConfEDMemberVal %>" size="30" /></td></tr>
	</table><br/>
	<table border="0">
	<tr><td colspan="3"><b><%=LNG_OBJECT_CLASSES %></td></tr>
		<tr><td>&nbsp;</td><td width="100"><%=LNG_USER %>: </td><td><input type="text" name="ConfEDUser" value="<%=sConfEDUserVal %>" size="30" /></td></tr>
		<tr><td>&nbsp;</td><td><%=LNG_GROUP %>: </td><td><input type="text" name="ConfEDGroup" value="<%=sConfEDGroupVal %>" size="30" /></td></tr>
	</table><br/>
</td></tr>
<tr><td align="right">
	</br>
	<a class="cancel_button" onclick="javascript:history.go(-1); return false;"><%=LNG_CANCEL %></a> 
	<a class="save_button" onclick="document.forms[0].submit()" ><%=LNG_SAVE %></a>
	<input type="hidden" name="a" value="2">
</td></tr></table>
</form>

		 <img src="images/inv.gif" alt="" width="1" height="100%" border="0">
		</div>
		</td>
		</tr>
	</table>
	</td>
	</tr>
</table>

</body>
</html>
<%

else

  Response.Redirect "index.asp"

end if

%> 
</body>
</html>
<!-- #include file="db_conn_close.asp" -->