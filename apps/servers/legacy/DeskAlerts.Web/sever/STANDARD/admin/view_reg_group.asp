﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%
check_session()
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<meta http-equiv=Content-Type content="text/html;charset=utf-8">
	<title>Deskalerts Control Panel</title>
	<link href="css/style9.css" rel="stylesheet" type="text/css">
	<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
	<script language="JavaScript" type="text/javascript">
		$(function() {
			$(".close_button").button();
		});
	</script>
	<style>
		table {border-collapse:collapse;empty-cells:show}
	</style>
</head>
<%
   if(Request("limit") <> "") then 
	limit = clng(Request("limit"))
   else 
	limit=50
   end if

	groupId = clng(request("id"))
	Set rsGroup = Conn.Execute("SELECT name FROM groups WHERE id="&groupId)
	if(not rsGroup.EOF) then
		groupName = rsGroup("name")
	end if
%> 
<body style="margin:0" class="body">
<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%" class="body">
	<tr>
	<td valign="top">
	<table width="100%" border="1" cellspacing="0" cellpadding="0" class="body main_table">
		<tr>
		<td width=100% height=31 class="theme-colored-back"><a href="#" class="header_title"><%=LNG_USERS_IN_GROUP %>: <%=groupName %></a></td>
		</tr>
		<tr>
		<td>
		<div style="margin:10px;">
<%
cnt=0
Set rsCnt = Conn.Execute("SELECT COUNT(user_id) as mycnt FROM users_groups WHERE group_id="&groupId)
if(not rsCnt.EOF) then
	cnt = rsCnt("mycnt")
end if
j=cnt/limit
	

if(cnt>0) then
	page= "view_reg_group.asp?id="&groupId&"&"
	name=LNG_USERS
	Response.Write make_pages(offset, cnt, limit, page, name, sortby) 
end if


'paging
%>
<table width="95%" border="1" cellspacing="0" cellpadding="3" class="main_table">
<tr>
	<td class="table_title"><%=LNG_USERNAME %></td></tr>
<%
	Set rsUser = Conn.Execute("SELECT name FROM users INNER JOIN users_groups ON users.id=users_groups.user_id WHERE users_groups.group_id="&groupId)
	do while not rsUser.EOF
	userName = rsUser("name")
%>
	<tr><td><%=userName %></td></tr>
<%
	rsUser.MoveNext
	loop
%>
 
</table>
<%
if(cnt>0) then
	Response.Write make_pages(offset, cnt, limit, page, name, sortby) 
end if
%>
 
<br>
<center><a href="#" class="close_button" onclick="javascript: window.close()"><%=LNG_CLOSE%></a></center>
 
</div>		</td>
		</tr>
	</table>
	</td>
	</tr>
</table>
</body>
</html>
<!-- #include file="db_conn_close.asp" -->