﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AlertLanguagesSettings.aspx.cs" Inherits="DeskAlertsDotNet.Pages.AlertLanguagesSettings" %>

<%@ Import Namespace="DeskAlertsDotNet.Parameters" %>
<%@ Import Namespace="Resources" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>DeskAlerts</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="css/Settings.min.css" rel="stylesheet" type="text/css" />
    <link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="css/jquery/jquery_timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="functions.asp"></script>
    <script type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
    <script type="text/javascript" src="jscripts/date.js"></script>
    <script type="text/javascript" src="jscripts/jquery/jquery_timepicker/jquery-ui-timepicker-addon.js"></script>
    <script type="text/javascript" src="jscripts/jquery/datepicker_fix.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".delete_button").button({
            });

            $("#selectAll").click(function () {
                var checked = $(this).prop('checked');
                $(".content_object:enabled").prop('checked', checked);
            });

            $("#ConfUseGoogleTranslate").click(function () {
                $("#googleTranslateSettings").toggle(this.checked);
            });

            $("#saveButton").click(function (e) {
                //Desplay a message about data saving
                alert("<% =resources.LNG_NOTIFICATION_OF_SAVING_SETTINGS%>");

                e.preventDefault();
                var defaultLanguage = $("input[name='is_default']:checked").attr("id");
                var googleTranslateKey = $("#ConfGoogleTranslateKey").val();

                if ($("#ConfUseGoogleTranslate")[0].checked && googleTranslateKey.length === 0) {
                    alert("Please enter your Google API key");
                    return;
                }
                if (!$("#ConfUseGoogleTranslate")[0].checked)
                    googleTranslateKey = "";

                $.ajax(
                    {
                        method: "POST",
                        url: "AlertLanguagesSettings.aspx/SetDefaultLanguage",
                        data: '{languageId:' + defaultLanguage + ', translateKey: \"' + googleTranslateKey + '\"}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            location.reload();
                        }
                    });
            });
        });

        function showPreview(id) {
            window.open('../UserDetails.aspx?id=' + id, '', 'status=0, toolbar=0, width=350, height=350, scrollbars=1');
        }

        function editUser(id, queryString) {
            location.replace('../EditUsers.aspx?id=' + id + '&return_page=users_new.asp?' + queryString);
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <table class="main_table table2">
            <tr>
                <td class="main_table_body">
                    <div runat="server" id="mainTableDiv" class="settings-alertLanguages">

                        <h2 runat="server" id="workHeader" class="work_header"><% =resources.LNG_ALERT_LANGUAGES %></h2>

                        <asp:Table runat="server" ID="contentTable" CssClass="data_table table3">
                            <asp:TableRow CssClass="data_table_title">
                                <asp:TableCell runat="server" ID="headerSelectAll" CssClass="table_title" Width="2%">
                                    <asp:CheckBox ID="selectAll" runat="server" />
                                </asp:TableCell>
                                <asp:TableCell runat="server" ID="languageNameCell" CssClass="table_title"></asp:TableCell>
                                <asp:TableCell runat="server" ID="languageCodeCell" CssClass="table_title"></asp:TableCell>
                                <asp:TableCell runat="server" ID="defaultLanguageCell" CssClass="table_title"></asp:TableCell>
                                <asp:TableCell runat="server" ID="actionsHeaderCell" CssClass="table_title"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>

                        <div class="settings-alertLanguages-controlButtons">
                            <asp:LinkButton runat="server" ID="upDelete" class="delete_button" />
                            <asp:LinkButton runat="server" ID="addButton" class="delete_button" href="AddAlertLanguage.aspx" />
                        </div>

                        <div class="settings-alertTranslation">
                            <div class="settings-alertTranslation-checkboxActivation">
                                <input runat="server" type="checkbox" id="ConfUseGoogleTranslate" name="ConfUseGoogleTranslate" />
                                <label for="ConfUseGoogleTranslate"><% =resources.LNG_USE_GOOGLE_TRANSLATE %></label>
                            </div>
                            <div id="googleTranslateSettings" runat="server">
                                <%
                                    var googleHookLink = "GOOGLEHOOK";
                                    var googleDocumentationLink = "https://cloud.google.com/docs/authentication/api-keys";
                                    var translateNotes = resources.LNG_GOOGLE_TRANSLATE_KEY;
                                    if (translateNotes.IndexOf(googleHookLink) > 1)
                                    {
                                        translateNotes = translateNotes.Replace(googleHookLink, googleDocumentationLink);
                                    }
                                %>
                                <label for="ConfGoogleTranslateKey"><% =translateNotes %>:</label>
                                <input runat="server" type="text" id="ConfGoogleTranslateKey" name="ConfGoogleTranslateKey" />
                                <p class="settings-alertTranslation-note">
                                    <% =resources.LNG_GOOGLE_TRANSLATE_NOTES %>
                                </p>
                            </div>
                        </div>
                        
                        <div class="settings-alertLanguages-controlButtons">
                            <asp:LinkButton href="#" runat="server" ID="saveButton" class="delete_button" />
                        </div>

                    </div>
                    <div runat="server" id="NoRows">
                        <p style="text-align: center">
                            <b>
                                <!-- Add here your message сcontent withou-->
                            </b>
                        </p>
                    </div>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
