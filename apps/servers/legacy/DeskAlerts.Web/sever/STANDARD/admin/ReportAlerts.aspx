﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReportAlerts.aspx.cs" Inherits="DeskAlertsDotNet.Pages.ReportAlerts" %>

<%@ Import Namespace="DeskAlertsDotNet.Parameters" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="DeskAlerts.Server.Utils" %>
<%@ Import Namespace="DeskAlerts.Server.Utils.Helpers" %>
<%@ Register Src="AlertTypeImage.ascx" TagName="ReccurencePanel"
    TagPrefix="da" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>DeskAlerts</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="css/style9.css" rel="stylesheet" type="text/css" />
    <link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="css/jquery/jquery_timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="functions.js"></script>
    <script type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
    <script type="text/javascript" src="jscripts/date.js"></script>
    <script type="text/javascript" src="jscripts/jquery/jquery_timepicker/jquery-ui-timepicker-addon.js"></script>
    <script type="text/javascript" src="jscripts/jquery/datepicker_fix.js"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $(".delete_button").button({
            });

            $("#selectAll").click(function () {
                var checked = $(this).prop('checked');
                $(".content_object").prop('checked', checked);
            });

            $(".date_param_input").datepicker({ dateFormat: "<% = DateTimeConverter.JsDateFormat %>", showButtonPanel: true });

        });

        function my_disable(i) {
            if (i == 1) {
                document.getElementById("range").disabled = true;
                document.getElementById("fromDate").disabled = false;
                document.getElementById("toDate").disabled = false;
            }
            else {
                document.getElementById("range").disabled = false;
                document.getElementById("fromDate").disabled = true;
                document.getElementById("toDate").disabled = true;
            }
        }

        function showPreview(id) {
            window.open('../UserDetails.aspx?id=' + id, '', 'status=0, toolbar=0, width=350, height=350, scrollbars=1');
        }

        function editUser(id, queryString) {
            location.replace('../EditUsers.aspx?id=' + id + '&return_page=users_new.asp?' + queryString);
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
            <tr>
                <td>
                    <table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
                        <tr>
                        </tr>
                        <tr>
                            <td class="main_table_body" height="100%">
                                <br />
                                <br />

                                <table cellpadding="0" cellspacing="0" border="0" width="98%">
                                    <tr>
                                        <td style="float: right">
                                            <table cellpadding="2" cellspacing="2" border="0" width="460">
                                                <tr>
                                                    <td width="360" nowrap colspan="5" align="left">
                                                        <input runat="server" id="rangeRadio" type="radio" name="select_date_radio" value="1" onclick="my_disable(2)" checked="True" />
                                                        <asp:DropDownList runat="server" name="range" ID="range" Style="font-size: 9pt;">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr valign="top">
                                                    <td>
                                                        <input runat="server" type="radio" id="manualDateRadio" name="select_date_radio" value="2" onclick="my_disable(1);" />
                                                        <input runat="server" name="from_date" class="date_param_input" id="fromDate" type="text" readonly value="" style="font-size: 9pt;" size="20" />
                                                        <input id="start_date" type="hidden" name="start_date" />
                                                    </td>
                                                    <td align="center" valign="middle">-</td>
                                                    <td>
                                                        <input runat="server" name="to_date" class="date_param_input" id="toDate" type="text" readonly value="" style="font-size: 9pt;" size="20" />
                                                        <input id="end_date" type="hidden" name="end_date" />
                                                    </td>
                                                    <td width="16">
                                                        <asp:LinkButton Style="margin-left: 10px" runat="server" ID="showButton" class='delete_button' />
                                                    </td>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                <div style="margin-left: 10px" runat="server" id="mainTableDiv">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td>
                                                <div style="margin-left: 10px" id="topPaging" runat="server"></div>
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                    <br />
                                    <br />
                                    <asp:Table Style="padding-left: 0px; margin-left: 10px; margin-right: 10px;" runat="server" ID="contentTable" Width="98.5%" Height="100%" CssClass="data_table" CellSpacing="0" CellPadding="3">
                                        <asp:TableRow ID="contentTable_rows" CssClass="data_table_title">
                                            <asp:TableCell runat="server" ID="TypeColumn" CssClass="table_title"></asp:TableCell>
                                            <asp:TableCell runat="server" ID="TitleColumn" CssClass="table_title"></asp:TableCell>
                                            <asp:TableCell runat="server" ID="CreationDateColumn" CssClass="table_title" Width="150"></asp:TableCell>
                                            <asp:TableCell runat="server" ID="SentDateColumn" CssClass="table_title" Width="150"></asp:TableCell>
                                            <asp:TableCell runat="server" ID="SenderColumn" CssClass="table_title"></asp:TableCell>
                                            <asp:TableCell runat="server" ID="RecipientsColumn" CssClass="table_title"></asp:TableCell>
                                            <asp:TableCell runat="server" ID="OpenRationColumn" CssClass="table_title"></asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                    <br />
                                    <br />
                                    <br />
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td>
                                                <div style="margin-left: 10px" id="bottomRanging" runat="server"></div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div runat="server" id="NoRows">
                                    <br />
                                    <br />
                                    <center><b><% =resources.LNG_THERE_ARE_NO_ALERTS %></b></center>
                                </div>
                                <br />
                                <br />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
