﻿using System;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using DeskAlertsDotNet.Parameters;

namespace DeskAlertsDotNet.Pages
{
    using Resources;

    public partial class GroupList : DeskAlertsBaseListPage
    {
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            headerTitle.Text = resources.LNG_GROUPS_POLICY;
            searchButton.Text = searchButton.Text.Replace("{Search}", resources.LNG_SEARCH);
            groupCell.Text = GetSortLink(resources.LNG_GROUP_NAME, "g.name", Request["sortBy"]);
            policyCell.Text = GetSortLink(resources.LNG_POLICY_NAME, "p.name", Request["sortBy"]);
            actionsCell.Text = resources.LNG_ACTIONS;

            Table = contentTable;
            var cnt = this.Content.Count < Offset + Limit ? this.Content.Count : Offset + Limit;

            for (var i = Offset; i < cnt; i++)
            {
                var row = this.Content.GetRow(i);

                var tableRow = new TableRow();
                var chb = GetCheckBoxForDelete(row);
                var checkBoxCell = new TableCell {HorizontalAlign = HorizontalAlign.Center};
                checkBoxCell.Controls.Add(chb);
                tableRow.Cells.Add(checkBoxCell);

                var groupNameContentCell = new TableCell {Text = row.GetString("name")};
                tableRow.Cells.Add(groupNameContentCell);

                var policyNameContentCell = new TableCell
                {
                    Text = row.GetString("policy_name")
                };
                tableRow.Cells.Add(policyNameContentCell);

                var actionsContentCell = new TableCell {HorizontalAlign = HorizontalAlign.Center};

                var editButton = GetActionButton(row.GetString("id"), "images/action_icons/edit.png", "LNG_EDIT_GROUPS_POLICY",
                    delegate { Response.Redirect("GroupEdit.aspx?id=" + row.GetString("id"), true); }
                );

                actionsContentCell.Controls.Add(editButton);

                tableRow.Cells.Add(actionsContentCell);
                contentTable.Rows.Add(tableRow);
            }
        }

        #region DataProperties

        protected override string DataTable
        {
            get { return "groups_policy"; }
        }

        protected override string[] Collumns
        {
            get { return new string[] {}; }
        }

        protected override string DefaultOrderCollumn
        {
            get { return "name"; }
        }

        protected override string CustomQuery
        {
            get
            {
                var query =
                    "SELECT gp.id, g.name, p.name AS policy_name FROM groups g RIGHT JOIN groups_policy gp ON g.id=gp.group_id LEFT JOIN policy p ON gp.policy_id=p.id";

                if (IsPostBack && SearchTerm.Length > 0)
                {
                    query += " WHERE g.name LIKE '%" + SearchTerm + "%'";
                }

                if (!string.IsNullOrEmpty(Request["sortBy"]))
                {
                    query += " ORDER BY " + Request["sortBy"];
                }

                return query;
            }
        }

        protected override string CustomCountQuery
        {
            get
            {
                var query = "SELECT count(1) FROM groups g RIGHT JOIN groups_policy gp ON g.id=gp.group_id LEFT JOIN policy p ON gp.policy_id=p.id";

                if (IsPostBack && SearchTerm.Length > 0)
                {
                    query += " WHERE g.name LIKE '%" + SearchTerm + "%'";
                }

                return query;
            }
        }

        #endregion

        #region Interface properties

        protected override IButtonControl[] DeleteButtons
        {
            get { return new IButtonControl[] {upDelete, bottomDelete}; }
        }

        protected override void OnDeleteRows(string[] ids)
        {
            if (ids != null && ids.Length != 0)
            {
                var builder = new StringBuilder();
                builder.Append("DELETE groups_policy WHERE");

                foreach (var id in ids)
                {
                    builder.Append(Array.IndexOf(ids, id) == 0 ? string.Format(" group_id = {0}", id)
                        : string.Format(" OR group_id = {0}", id));
                }

                dbMgr.ExecuteQuery(builder.ToString());
            }
        }

        protected override HtmlGenericControl NoRowsDiv
        {
            get { return NoRows; }
        }

        protected override HtmlGenericControl TableDiv
        {
            get { return mainTableDiv; }
        }

        protected override string ContentName
        {
            get { return resources.LNG_GROUPS_POLICY; }
        }


        protected override HtmlGenericControl BottomPaging
        {
            get { return bottomRanging; }
        }

        protected override HtmlGenericControl TopPaging
        {
            get { return topPaging; }
        }

        #endregion
    }
}