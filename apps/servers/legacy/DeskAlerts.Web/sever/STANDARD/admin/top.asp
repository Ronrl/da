﻿<!-- #include file="config.inc" -->
<!-- #include file="demo.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->

<%

check_session()
function Ismobile()
Dim user_agent, mobile_browser, Regex, match, mobile_agents, mobile_ua, i, size
 
user_agent = Request.ServerVariables("HTTP_USER_AGENT")
 
mobile_browser = 0
 
Set Regex = New RegExp
With Regex
   .Pattern = "(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|windows ce|pda|mobile|mini|palm)"
   .IgnoreCase = True
   .Global = True
End With
 
match = Regex.Test(user_agent)
 
If match Then mobile_browser = mobile_browser+1
 
If InStr(Request.ServerVariables("HTTP_ACCEPT"), "application/vnd.wap.xhtml+xml") Or Not IsEmpty(Request.ServerVariables("HTTP_X_PROFILE")) Or Not IsEmpty(Request.ServerVariables("HTTP_PROFILE")) Then
   mobile_browser = mobile_browser+1
end If
 
mobile_agents = Array("alcatel", "amoi", "android", "avantgo", "blackberry", "benq", "cell", "cricket", "docomo", "elaine", "htc", "iemobile", "iphone", "ipad", "ipaq", "ipod", "j2me", "java", "midp", "mini", "mmp", "mobi", "motorola", "nec-", "nokia", "palm", "panasonic", "philips", "phone", "sagem", "sharp", "sie-", "smartphone", "sony", "symbian", "t-mobile", "telus", "up\.browser", "up\.link", "vodafone", "wap", "webos", "wireless", "xda", "xoom", "zte")
size = Ubound(mobile_agents)
mobile_ua = LCase(Left(user_agent, 4))
 
For i=0 To size
   If mobile_agents(i) = mobile_ua Then
      mobile_browser = mobile_browser+1
      Exit For
   End If
Next
 
 
If mobile_browser>0 Then
      Ismobile = true
   Else
      Ismobile = false
   End If
End function

%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
<head>
<title>DeskAlerts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
<link href="css/style9.css" rel="stylesheet" type="text/css">
<link href="css/jquery/jquery_timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css">
<script language="javascript" type="text/javascript" src="functions.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery_timepicker/jquery-ui-timepicker-addon.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/date.js"></script>
<script language="javascript" type="text/javascript">
	$(function(){
		$("#logout").button({
			icons: {
				primary: "ui-icon-key"
			}
		});
		$("#helpOpener").button({
			icons: {
				primary: "ui-icon-lightbulb"
			}
		});
		$("#addons_trial_info").button({
			icons: {
			primary: "ui-icon-mail-closed"
			}, text:false
		});
		$('.buy_addons_btn').button({
			icons: {
				primary: "ui-icon-cart"
			}, text:false
		});
		$('.settings_button').button({
		    icons: {
		        primary: "ui-icon-gear"
		    }, text: false
		});
	});
	
</script>
</head>
<body class="top_body" style="margin:0px">
<table border="0" cellspacing="0" cellpadding="0" width="100%" height="62px">
<tr valign="middle">
<td valign="middle" style="padding-left:15px">
<% mobile_device= Ismobile()
 %>
	<!--<img src="images/langs/<%=default_lng %>/top_logo.gif" alt="DeskAlerts Control Panel" border="0" align="left"/>-->
<%if mobile_device <> true then %>
	<img src="images/top_logo.gif" alt="DeskAlerts Control Panel" border="0" align="left"/>
<%else %>
	<img src="images/top_logo_mobile.gif" alt="DeskAlerts Control Panel" border="0" align="left"/>
<%end if %>
</td>
<td><div id="newsBanner"></div></td>
<td align="right" valign="middle" style="padding-right:4px;white-space:nowrap">

<%if TRIAL_DAYS_LEFT>=0 then%>
<a href="addons_trial_info.asp" target="work_area" id="addons_trial_info" style="background:#efffef">&nbsp;</a>
<%end if %>



<a href="SettingsSelection.aspx" id="settingsButton" class="settings_button" target="work_area" title="Settings">&nbsp;</a>


 <% if DEMO <> 1 and ConfHideBuyAddonsLink <> 1 then %>
<a href="buy_addons.asp?use_local=1" class="buy_addons_btn" target="work_area" title="DeskAlers store">&nbsp;</a>
<% end if%>

<a  id="helpOpener" target="work_area" class="logout" style="text-align:center"><span id="help_close_span" style="display:none"><%=LNG_CLOSE %>&nbsp;</span><%=LNG_HELP %></a>
<%
uid = Session("uid")
if(uid<>"") then
%><a href="logout.asp" target="_top" id="logout" class="logout"><%=LNG_LOGOUT%></a>
<div style="width:100%; text-align:right; white-space:nowrap; padding-top:5px;"><%
	Set RS = Conn.Execute("SELECT name FROM users WHERE id="&uid)
	if(Not RS.EOF) then
		uname=RS("name")
		response.write LNG_CURRENTLY_LOGGED_AS &": <span class='username'>"&uname&"</span>"
	end if

	RS.Close
end if
%></div></td></tr></table>
<%if ConfShowNewsBanner = 1 then %>

<%end if %>
</body>
</html>

<!-- #include file="db_conn_close.asp" -->