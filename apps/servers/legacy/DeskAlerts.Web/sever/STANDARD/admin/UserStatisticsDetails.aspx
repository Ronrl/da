﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserStatisticsDetails.aspx.cs" Inherits="DeskAlertsDotNet.Pages.UserStatisticsDetails" %>
<%@ Import Namespace="Resources" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link href="css/style9.css" rel="stylesheet" type="text/css">
        <link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <link href="css/jquery/jquery_timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css">
        <script language="javascript" type="text/javascript" src="functions.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/date.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery_timepicker/jquery-ui-timepicker-addon.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/jquery/datepicker_fix.js"></script>
    	<script language="javascript" type="text/javascript" src="jscripts/jquery.jqplot.min.js"></script>
		<script language="javascript" type="text/javascript" src="jscripts/jqplot.highlighter.min.js"></script>
		<script type="text/javascript" src="jscripts/jqplot.pieRenderer.min.js"></script>
		<script type="text/javascript" src="jscripts/jqplot.barRenderer.min.js"></script>
		<script type="text/javascript" src="jscripts/jqplot.categoryAxisRenderer.min.js"></script>
    
    <script language="JavaScript" src="jscripts/FusionCharts.js"></script>
		<link rel="stylesheet" type="text/css" href="css/jquery.jqplot.css" />
<script type="text/javascript">


    function openDialogUI(_form, _frame, _href, _width, _height, _title) {
        $("#dialog-" + _form).dialog('option', 'height', _height);
        $("#dialog-" + _form).dialog('option', 'width', _width);
        $("#dialog-" + _form).dialog('option', 'title', _title);
        $("#dialog-" + _frame).attr("src", _href);
        $("#dialog-" + _frame).css("height", _height);
        $("#dialog-" + _frame).css("display", 'block');
        if ($("#dialog-" + _frame).attr("src") != undefined) {
            $("#dialog-" + _form).dialog('open');
            $("#dialog-" + _form).contents().find('#table_border').css('border', 'none');
        }
    }





   
    $(document)
        .ready(function() {


            $("#stopStartButton").button();
            $("#duplicate_button").button();
            $("#dialog-form").dialog({
                autoOpen: false,
                height: 100,
                width: 100,
                modal: true
            });


            $('#dialog-frame').on('load', function () {
                $("#dialog-frame").contents().find('.prelink_stats').on('click', function (e) {
                    e.preventDefault();
                    var link = $(this).attr('href');
                    var title = $(this).attr('title');
                    var last_id = $('.dialog_forms').size();
                    var elem = '<div id="dialog-form-' + last_id + '" class="dialog_forms" style="overflow:hidden;display:none;"><iframe id="dialog-frame-' + last_id + '" style="display:none;width:100%;height:auto;overflow:hidden;border : none;"></iframe></div>';
                    $('#secondary').append(elem);
                    $("#dialog-form-" + last_id).dialog({
                        autoOpen: false,
                        height: 100,
                        width: 100,
                        modal: true
                    });
                    openDialogUI("form-" + last_id, "frame-" + last_id, link, 350, 350, title);
                });
            });

            var alertsPlot = jQuery.jqplot('alertsChart',
                             [<% =AlertsValues %>],
                           {
                               grid: {
                                   background: 'white',
                                   renderer: $.jqplot.CanvasGridRenderer,
                                   drawBorder: false,
                                   shadow: false
                               },
                               seriesColors: [<% =AlertsColors %>],
                               seriesDefaults: {
                                   shadow: true,
                                   shadowAlpha: 0.1,
                                   showMarker: true,
                                   useNegativeColors: false,
                                   renderer: jQuery.jqplot.BarRenderer,
                                   rendererOptions: {
                                       fillToZero: true,
                                       barWidth: 40,
                                       barPadding: 25
                                   }
                               },
                               legend: {
                                   show: false,
                                   location: "e"
                               },
                               axes: {
                                   xaxis: {
                                       renderer: $.jqplot.CategoryAxisRenderer,
                                       ticks: [''],
                                       numberTicks: 1
                                   }
                               }
                           });

                });
           
       
  
</script>
 </head>

<body>
    <form id="form1" runat="server">
    <div>

         <table width="95%"><tr><td align="left"><b> <asp:Literal runat="server" id="userNameLabel"></asp:Literal></b> <asp:Literal runat="server" id="userDisplayNameLabel"></asp:Literal></td><td align="right"><b><A href="#" onclick="javascript: window.open('GroupsForUser.aspx?id=<%=Request["id"] %>&type=groups','', 'status=0, toolbar=0, width=350, height=350, scrollbars=1')"><% =resources.LNG_GROUPS %></a> (<asp:Literal runat="server" id="totalGroupsLabel"></asp:Literal>)</b> </td></tr></table><br>
            <div style="width:100%; text-align:right;"><% =resources.LNG_TOTAL %>:  <asp:Literal runat="server" id="totalAlertsCountLabel"></asp:Literal></div>

         <center><table border=0 style="text-align:left">
         <tr><td colspan="3"><div id="alertsChart" style="width:250px;height:180px"></div></td></tr>
         <tr><td><div class="legend" id="firstLegend" runat="server" ></div></td><td runat="server" id="firstLegendLabel"></td><td runat="server" id="firstLegendValue"></td></tr>
         <tr><td><div class="legend" runat="server" id="secondLegend" ></div></td><td runat="server" id="secondLegendLabel"></td><td  runat="server" id="secondLegendValue"></td></tr>
        <!--<tr{alertsVal4Display}><td><div class="legend" style='background-color: #{alertsVal4Color};'></div></td><td>{alertsVal4Name}</td><td>{alertsVal4Value}</td></tr>-->
        <tr><td><div class="legend" runat="server" id="thirdLegend" ></div></td><td runat="server" id="thirdLegendLabel"></td><td  runat="server" id="thirdLegendValue"></td></tr>
        </table></center>
        
        <br/>
        
        <div style="width:100%; text-align:right;display: none"><a href='' target='_blank' id="csvDownloadLink" runat="server"><b> <% =resources.LNG_EXPORT_TO_CSV %>   </b></a></div>

    </div>
    </form>
</body>
    
    <div id="dialog-form" style="overflow: hidden;" class="ui-dialog-content ui-widget-content">
        <iframe id="dialog-frame" style="width: 100%; height: 100%; overflow: hidden; border: none;
            display: none"></iframe>
    </div>
</html>
