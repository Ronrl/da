﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SystemConfigurationSettings.aspx.cs" Inherits="DeskAlerts.Server.sever.STANDARD.admin.SystemConfigurationSettings" %>

<%@ Import Namespace="DeskAlertsDotNet.Parameters" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="DeskAlertsDotNet.sever.STANDARD.admin" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <link href="css/bootstrap-slider.css" rel="stylesheet" type="text/css" />
    <link href="css/style9.css" rel="stylesheet" type="text/css" />
    <link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="css/jstree-themes/default/jstree.style.min.css" rel="stylesheet" type="text/css" />
    <link href="css/jstree-custom.min.css" rel="stylesheet" type="text/css" />

    <style>
        #NonADUsersConnectionPermisionLevelHandler {
            margin-left: 100px;
        }
    </style>

    <script type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
    <script type="text/javascript" src="jscripts/jquery/jquery.validate.min.js"></script>
    <script type="text/javascript" src="jscripts/tiny_mce/tinymce.min.js"></script>
    <script type="text/javascript" src="jscripts/jstree.min.js"></script>
    <script type="text/javascript" src="jscripts/bootstrap-slider.js"></script>

    <script type="text/javascript">
        function scrollTo(x) {
            $('html, body').animate({
                scrollTop: x
            }, 10);
        }

        $(function () {
            OnDateTimeChange();
            const greenColor = "#026e25";
            const orangeColor = "#ff9b1b";
            const redColor = "#f02420";

            const ableNothingMainText = "<%=resources.LNG_ABLE_NOTHING_MAIN_TEXT %>";
            const ableLoginMainText = "<%=resources.LNG_ABLE_LOGIN_MAIN_TEXT %>";
            const ableRegisterMainText = "<%=resources.LNG_ABLE_REGISTER_MAIN_TEXT %>";
            const ableNothingAdditionalText = "<%=resources.LNG_ABLE_NOTHING_ADDITIONAL_TEXT %>";
            const ableLoginAdditionalText = "<%=resources.LNG_ABLE_LOGIN_ADDITIONAL_TEXT %>";
            const ableRegisterAdditionalText = "<%=resources.LNG_ABLE_REGISTER_ADDITIONAL_TEXT %>";

            var ableLogin = $("#ConfEnableRegAndADEDMode").prop('checked');
            var ableRegister = $("#ConfForceSelfRegWithAD").prop('checked');
            var nonAdUsersConnectionPermisionLevelStartValue = 0;

            if ((ableLogin) && (ableRegister)) {
                nonAdUsersConnectionPermisionLevelStartValue = 2;
            } else if (ableLogin) {
                nonAdUsersConnectionPermisionLevelStartValue = 1;
            }

            $("#ConfSmsPostData").resizable({
                maxHeight: 350,
                maxWidth: 600,
                minHeight: 20,
                minWidth: 600
            });

            function setSliderData() {
                var nonAdUsersConnectionPermisionLevelValue =
                    $("#NonADUsersConnectionPermisionLevel").bootstrapSlider().data("bootstrapSlider").getValue();

                switch (nonAdUsersConnectionPermisionLevelValue) {
                    case 0:
                        $("#NonADUsersConnectionPermisionLevelHandler .slider-selection").css("background", greenColor);
                        $("#NonADUsersConnectionPermisionLevelHandler .slider-handle").css("background", greenColor);
                        $("#NonADUsersConnectionPermisionLevelHandler .slider-tick").css("background", "white");
                        $("#NonADUsersConnectionPermisionLevelHandler .slider-tick.in-selection").css("background", greenColor);

                        $("#ConfEnableRegAndADEDMode").prop("checked", false);
                        $("#ConfForceSelfRegWithAD").prop("checked", false);

                        $("#NonADUsersConnectionPermisionLevelHint").text(ableNothingMainText);
                        $("#NonADUsersConnectionPermisionLevelAdditionalHint").text(ableNothingAdditionalText);
                        break;
                    case 1:
                        $("#NonADUsersConnectionPermisionLevelHandler .slider-selection").css("background", orangeColor);
                        $("#NonADUsersConnectionPermisionLevelHandler .slider-handle").css("background", orangeColor);
                        $("#NonADUsersConnectionPermisionLevelHandler .slider-tick").css("background", "white");
                        $("#NonADUsersConnectionPermisionLevelHandler .slider-tick.in-selection").css("background", orangeColor);

                        $("#ConfEnableRegAndADEDMode").prop("checked", true);
                        $("#ConfForceSelfRegWithAD").prop("checked", false);

                        $("#NonADUsersConnectionPermisionLevelHint").text(ableLoginMainText);
                        $("#NonADUsersConnectionPermisionLevelAdditionalHint").text(ableLoginAdditionalText);
                        break;
                    case 2:
                        $("#NonADUsersConnectionPermisionLevelHandler .slider-selection").css("background", redColor);
                        $("#NonADUsersConnectionPermisionLevelHandler .slider-handle").css("background", redColor);
                        $("#NonADUsersConnectionPermisionLevelHandler .slider-tick").css("background", "white");
                        $("#NonADUsersConnectionPermisionLevelHandler .slider-tick.in-selection").css("background", redColor);

                        $("#ConfEnableRegAndADEDMode").prop("checked", true);
                        $("#ConfForceSelfRegWithAD").prop("checked", true);

                        $("#NonADUsersConnectionPermisionLevelHint").text(ableRegisterMainText);
                        $("#NonADUsersConnectionPermisionLevelAdditionalHint").text(ableRegisterAdditionalText);
                        break;
                    default:
                        alert(nonAdUsersConnectionPermisionLevelValue + " is wrong NonADUsersConnectionPermisionLevelValue variable value!");
                }
            }

            if ($("#NonADUsersConnectionPermisionLevel").length) {
                $("#NonADUsersConnectionPermisionLevel").bootstrapSlider({
                    ticks: [0, 1, 2],
                    ticks_labels: ['<%=resources.LNG_AD_DEVICES_ONLY %>', '<%=resources.LNG_MIXED_DEVICES_AD_CRED_ONLY %>', '<%=resources.LNG_MIXED_AUTH %>'],
                    ticks_snap_bounds: 1,
                    value: nonAdUsersConnectionPermisionLevelStartValue,
                    tooltip: "always"
                }).on("change", setSliderData);

                setSliderData();
            }

            $(".cancel_button").button();
            $(".submit").button();
            $("#accordion").accordion({
                heightStyle: "content",
                collapsible: true,
                active: false
            });
            $("#tabs-sms-settings").tabs();

            if (this.all.ConfSsoEnable) {
                if (!(this.all.ConfSsoEnable.value === "1" && this.all.ConfSsoEnable.checked)) {
                    $("#ADGroupList").hide();
                }
                else {
                    drawAdTree();
                }
            }

            $("input#ConfSsoEnable").click(function () {
                if (this.checked) {
                    drawAdTree();
                    alert("Set up of groups policy settings could take a bit of time");
                    $("#ADGroupList").show();
                }
                else {
                    $("#ADGroupList").hide();
                }
            });

            function getPolicy() {
                var policy;
                $.ajax({
                    type: "GET",
                    url: '../api/v1/policy',
                    data: '',
                    success: function (value) {
                        policy = value.data;
                    },
                    async: false
                });
                return policy;
            }

            $("#deleteClientUpdateFile").click(function () {
                var result;
                $.ajax({
                    type: "GET",
                    url: '../api/v1/autoupdate/deleteFile',
                    data: '',
                    success: function (value) {
                        result = value;
                        if (result === "success") {
                            document.getElementById('lastClientVersion').innerHTML =
                                "<%=resources.AUTOUPDATE_FILE_DOESNT_EXIST%>";
                            alert("<%=resources.DELETING_OF_AUTOUPDATE_CLIENT%>");
                        } else {
                            alert("<%=resources.DELETING_AUTOUPDATE_UNSUCCESS%>");
                        }
                    },
                    async: false
                });
            });

            function getAllPolicies() {

                var policy = getPolicy();
                var selector = $('<div class="select_policies" id="policy_group"/>');
                var index = 0;
                $(policy).each(function () {

                    var policy_name = " <input type=\"radio\" id='policy_" + this.id + "\' " + (index == 0 ? "checked" : "") + " class=\"" + this.policy_type + "\" name=\"policy_group\" value=\"" + this.name + "\">" + this.name + "<br/>";
                    selector.append($(policy_name));
                    index++;
                });
                return selector;
            }

            function addingPolicyListToTree(data) {
                for (var t = 0; t < data.length; t++) {
                    $.each(data[t].children, function (id, group) {
                        if (group.data.policy_id != null) {
                            group.state.checked = true;
                            group.state.opened = true;
                            group.text += " - Current Policy: " + group.data.policy_name;
                        }
                    });

                }
                return data;

            }

            function drawAdTree() {
                $.post("../api/v1/ad/getAllDomains", { search: $('#groupSearch').val() })
                    .done(function (response) {
                        if (response.data != null) {
                            addingPolicyListToTree(response.data);
                            creatingAdTree(response.data);
                        } else {
                            $("#ADGroupList").append("<p style=\"color:red\">There is no any synchronized domains</p>");
                        }
                    });
            }

            function creatingAdTree(tree) {
                var list = $('#ADGroupList');
                list.jstree("destroy").empty();
                list.jstree({
                    "core": {
                        "themes": {
                            "variant": "large"
                        },
                        "data": tree
                    },
                    "checkbox": {
                        "keep_selected_style": false,
                        "three_state": false,
                        "whole_node": false,
                        "tie_selection": false

                    },
                    "plugins": ["massload", "checkbox", "conditionalselect"]
                });
                bindOnCheckJsTree();

            }

            function getGroupsWithAdminsPolicy() {
                var policyCounter;
                $.ajax({
                    type: "GET",
                    url: '../api/v1/groups/getGroupsWithAdminPolicy',
                    data: '',
                    success: function (value) {
                        policyCounter = value.length;
                    },
                    async: false
                });
                return policyCounter;
            }

            function bindOnCheckJsTree() {
                $("#ADGroupList").on("check_node.jstree",
                    function (event, data) {
                        if (data.node.parent !== "#") {
                            var policies = getAllPolicies();

                            $("#policy_dialog").dialog({
                                draggable: false,
                                resizable: false,
                                closeOnEscape: false,
                                modal: true,
                                open: function (event, ui) {
                                    $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
                                },
                                buttons: {
                                    "Confirm ": function () {
                                        var selectedVal = "";
                                        var policy_type = "";
                                        var selected = $("#policy_dialog input[type='radio']:checked");
                                        if (selected.length > 0) {
                                            var policy_id = selected.attr("id").replace("policy_", "");
                                            selectedVal = parseInt(policy_id);
                                            policy_type = selected.attr("class");
                                        } else {
                                            selectedVal = 1;
                                            policy_type = $("#policy_dialog input[type='radio']:checked").attr("class");
                                        }

                                        data.node.original.state.checked = true;
                                        var newData = {
                                            policy_id: selectedVal,
                                            policy_name: selected.val(),
                                            policy_type: policy_type
                                        };
                                        data.node.original.data = data.node.data = newData;

                                        UpdateGroup(data.node.original);
                                        $("#ADGroupList").jstree(true).set_text(data.node,
                                            data.node.text + " - Current Policy: " + data.node.data.policy_name);

                                        $(this).dialog("close");
                                        $(this).dialog("destroy");
                                    }
                                }
                            }).html("Select policy for current group: <br/>" + policies[0].outerHTML);

                        }
                    }
                );

                $("#ADGroupList").on("uncheck_node.jstree",
                    function (event, data) {
                        event.preventDefault();
                        if (data.node.parent !== "#") {

                            var policyCounter = getGroupsWithAdminsPolicy();
                            if (policyCounter <= 1 && data.node.data.policy_type == "A") {
                                data.node.original.state.checked = true;
                                data.node.state.checked = true;
                                data.event.currentTarget.className += " jstree-checked";
                                alert("There should be at least one group with \"System Administrator\" policy");
                            }
                            else {
                                var name = data.node.text.split(" ");
                                $("#ADGroupList").jstree(true).set_text(data.node, name[0]);
                                data.node.original.state.checked = false;
                                data.node.original.data = data.node.data;
                                UpdateGroup(data.node.original);
                                data.node.data = null;
                            }
                        }
                    }
                );
            }

            function UpdateGroup(group) {
                $.ajax({
                    type: "POST",
                    url: '../api/v1/ad/updateGroup',
                    data: group,
                    success: true,
                    async: false
                });
            }

            $("#ConfDefaultLanguage")
                .change(function () {

                    var value = $("#ConfDefaultLanguage").val();

                    var editLink = "LanguageList.aspx?lang=" + value;

                    $("#langEditLink").attr("href", editLink);

                });

            $("#ConfSmsAuthType").change(function (sender) {
                showAuthentificationType(sender.currentTarget.value);
            });

            $("#ConfSmsProxyEnable").change(function (sender) {
                switchAuthentificationProxy(sender.currentTarget.checked);
            });

            $("#ConfTokenMaxCountForServerEnable").change(function () {
                if ($('#ConfTokenMaxCountForUserEnable').is(':checked') == true) {
                    if (parseInt($('#ConfTokenMaxCountForUser').val(), 10) >
                        parseInt($('#ConfTokenMaxCountForServer').val(), 10))
                        $('#ConfTokenMaxCountForUser').val($('#ConfTokenMaxCountForServer').val());
                }
            });

            $("#ConfTokenMaxCountForUserEnable").change(function () {
                if ($('#ConfTokenMaxCountForServerEnable').is(':checked') == true) {
                    if (parseInt($('#ConfTokenMaxCountForUser').val(), 10) >
                        parseInt($('#ConfTokenMaxCountForServer').val(), 10))
                        $('#ConfTokenMaxCountForUser').val($('#ConfTokenMaxCountForServer').val());
                }
            });

            $("#ConfTokenMaxCountForServer").change(function () {
                var value = $('#ConfTokenMaxCountForServer').val();
                if (parseInt(value, 10) < 1 ||
                    value == "" ||
                    value.toString().indexOf(",") >= 0 ||
                    value.toString().indexOf(".") >= 0) value = 1;
                if (parseInt(value, 10) > 1000000)
                    value = 1000000;
                if ($('#ConfTokenMaxCountForUserEnable').is(':checked') == true) {
                    if (parseInt($('#ConfTokenMaxCountForUser').val(), 10) >
                        parseInt(value, 10))
                        $('#ConfTokenMaxCountForUser').val(value);
                }
                $('#ConfTokenMaxCountForServer').val(value);
            });

            $("#ConfTokenMaxCountForUser").change(function () {
                var value = $('#ConfTokenMaxCountForUser').val();
                if (parseInt(value, 10) < 1 ||
                    value == "" ||
                    value.toString().indexOf(",") >= 0 ||
                    value.toString().indexOf(".") >= 0) value = 1;
                if (parseInt(value, 10) > 1000000)
                    value = 1000000;
                if ($('#ConfTokenMaxCountForServerEnable').is(':checked') == true) {
                    if (parseInt(value, 10) >
                        parseInt($('#ConfTokenMaxCountForServer').val(), 10))
                        value = $('#ConfTokenMaxCountForServer').val();
                }
                $('#ConfTokenMaxCountForUser').val(value);
            });

            $("#ConfTokenExpirationTime").change(function () {
                var value = $('#ConfTokenExpirationTime').val();
                if (parseInt(value, 10) < 1 ||
                    value == "" ||
                    value.toString().indexOf(",") >= 0 ||
                    value.toString().indexOf(".") >= 0) value = 1;
                if (parseInt(value, 10) > 1000000)
                    value = 1000000;
                $('#ConfTokenExpirationTime').val(value);
            });

            setTokensInputs();

            showAuthentificationType($("#ConfSmsAuthType").val());
            switchAuthentificationProxy($("#ConfSmsProxyEnable").prop("checked"));

            hideLoaderPanel();

            jQuery.validator.setDefaults({
                debug: true,
                success: "valid"
            });

            $("#form1").validate({
                submitHandler: function (form) {
                    showLoaderPanel();
                    if ($("#ConfSsoEnable").prop("checked")) {
                        var policyCounter = getGroupsWithAdminsPolicy();
                        if (policyCounter == 0) {
                            alert("There should be at least one group with \"System Administrator\" policy");
                            hideLoaderPanel();
                            return;
                        }
                    }
                    showErrorMessage("#ConfSmtpUser", "<%= resources.ResourceManager.GetString("NOT_VALID_SMTP_USER_EMAIL") %>");
                    showErrorMessage("#ConfSmtpFrom", "<%= resources.ResourceManager.GetString("NOT_VALID_SMTP_FROM_EMAIL") %>");
                    window.parent.parent.parent.scrollTo(0);
                    window.parent.scrollTo(0);
                    scrollTo(0);
                    form.submit();
                },
                rules: {
                    ConfAutoupdateSlotCacheLength: {
                        required: true
                    },
                    ConfAutoupdateSlotLifeTime: {
                        required: true
                    }
                },
                messages: {
                    ConfAutoupdateSlotCacheLength: {
                        required: "Connections limit is required",
                        minlength: jQuery.validator.format("At least {0} characters required!"),
                        maxlength: jQuery.validator.format("Please enter no more than {0} characters.")
                    },
                    ConfAutoupdateSlotLifeTime: {
                        required: "Connection lifetime is required",
                        minlength: jQuery.validator.format("At least {0} characters required!"),
                        maxlength: jQuery.validator.format("Please enter no more than {0} characters.")
                    }
                }
            });

            function showErrorMessage(parameterName, resourceName) {
                if (!validateEmail($(parameterName).val())) {
                    alert(resourceName);
                    event.preventDefault();
                }
            }

            $("#checkPushButton").click(function () {
                $.get("../api/v1/settings/pushnotificationservers/check", function (response) {
                    var apnsResponse = $("#apnsResponse");
                    apnsResponse.empty();
                    apnsResponse.append(response.data.apnsResponse);
                    var gcmResponse = $("#gcmResponse");
                    gcmResponse.empty();
                    gcmResponse.append(response.data.gcmResponse);
                    $(".PushNotificationPanel").show();
                });
            });

            $(".PushNotificationPanel").hide();

            var timer;
            var delay = 600; // 0.6 seconds delay after groupSearch input
            $('#groupSearch').bind('input',
                function () {
                    window.clearTimeout(timer);
                    timer = window.setTimeout(function () {
                        drawAdTree();
                    },
                        delay);
                });

            $('#ConfTimeFormat,#ConfDateFormat').change(OnDateTimeChange);

            function OnDateTimeChange() {
                var time = $('#ConfTimeFormat').val();
                var date = $('#ConfDateFormat').val();

                $.ajax({
                    type: "POST",
                    url: '../api/v1/settings/dateTimeFormat',
                    dataType: "JSON",
                    data: {
                        dateFormat: date,
                        timeFormat: time
                    },
                    success: function (response) {
                        $('#dateTimeExample').text(response.data);
                    },
                    async: false
                });
            }
        });

        function hideLoaderPanel() {
            $('.loader-panel').hide();
        }

        function showLoaderPanel() {
            $('.loader-panel').show();
        }

        function setTokensInputs() {

            $('#ConfTokenAuthEnable').change(function () {
                if ($('#ConfTokenAuthEnable').is(':checked') == false) {
                    $('#ConfTokenMaxCountForServerEnable').prop('disabled', true);
                    $('#ConfTokenMaxCountForUserEnable').prop('disabled', true);
                    $('#ConfTokenExpirationTimeEnable').prop('disabled', true);
                    $('#ConfTokenMaxCountForServer').prop('disabled', true);
                    $('#ConfTokenMaxCountForUser').prop('disabled', true);
                    $('#ConfTokenExpirationTime').prop('disabled', true);
                } else {
                    $('#ConfTokenMaxCountForServerEnable').prop('disabled', false);
                    $('#ConfTokenMaxCountForUserEnable').prop('disabled', false);
                    $('#ConfTokenExpirationTimeEnable').prop('disabled', false);

                    if ($('#ConfTokenMaxCountForServerEnable').prop('checked'))
                        $('#ConfTokenMaxCountForServer').prop('disabled', false);

                    if ($('#ConfTokenMaxCountForUserEnable').prop('checked'))
                        $('#ConfTokenMaxCountForUser').prop('disabled', false);

                    if ($('#ConfTokenExpirationTimeEnable').prop('checked'))
                        $('#ConfTokenExpirationTime').prop('disabled', false);
                }
            });

            $('#ConfTokenMaxCountForServerEnable').change(function () {
                if ($('#ConfTokenMaxCountForServerEnable').is(':checked') == false) {
                    $('#ConfTokenMaxCountForServer').prop('disabled', true);
                } else {
                    $('#ConfTokenMaxCountForServer').prop('disabled', false);
                }
            });

            $('#ConfTokenMaxCountForUserEnable').change(function () {
                if ($('#ConfTokenMaxCountForUserEnable').is(':checked') == false) {
                    $('#ConfTokenMaxCountForUser').prop('disabled', true);
                } else {
                    $('#ConfTokenMaxCountForUser').prop('disabled', false);
                }
            });

            $('#ConfTokenExpirationTimeEnable').change(function () {
                if ($('#ConfTokenExpirationTimeEnable').is(':checked') == false) {
                    $('#ConfTokenExpirationTime').prop('disabled', true);
                } else {
                    $('#ConfTokenExpirationTime').prop('disabled', false);
                }
            });
        }

        function showAuthentificationType(sender) {
            switch (sender) {
                case "1":
                    $("#smsUserPassword").show();
                    $("#smsAuthKey").hide();
                    break;
                case "2":
                    $("#smsUserPassword").hide();
                    $("#smsAuthKey").show();
                    break;
                case "3":
                    $("#smsUserPassword").show();
                    $("#smsAuthKey").hide();
                    break;
                case "0":
                    $("#smsUserPassword").hide();
                    $("#smsAuthKey").hide();
            }
        }

        function switchAuthentificationProxy(isShow) {
            if (isShow) {
                $(".UseProxyAuth").show();
            } else {
                $(".UseProxyAuth").hide();
            }
        }

        function saveSettings() {
            var intRegex = /^\d+$/;
            var sessionTimeout = $("#ConfSessionTimeout").val();
            if (!intRegex.test(sessionTimeout) || parseInt(sessionTimeout) <= 0) {
                alert("<%=resources.LNG_SESSION_TIMEOUT_ERROR %>");
                return;
            }
            document.forms[0].submit();
        }

        function regenerateAPIsecret() {
            var newApiSecret = randomPassword(16);
            document.getElementById('ConfAPISecret').value = newApiSecret;
        }

        function randomPassword(length) {
            var chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            var pass = "";
            for (x = 0; x < length; x++) {
                i = Math.floor(Math.random() * 62);
                pass += chars.charAt(i);
            }
            return pass;
        }

        function validateEmail(mail) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(String(mail).toLowerCase());
        }

        $(document).ready(function () {
            $("#SMTPEnable").click(function () {
                var urlSmtpLog = "../SMTPlogs/SmtpLogs.txt";
                var httpRequest = new XMLHttpRequest();
                httpRequest.open('HEAD', urlSmtpLog, false);
                httpRequest.send();
                if (httpRequest.status != 404) {
                    window.open(urlSmtpLog, '_blank');
                } else {
                    alert("<%=resources.NO_SMTP_LOGS%>");
                }
            });


            $("#SMTPTestConnection").click(function () {
                $.ajax({
                    type: "POST",
                    url: '../api/v1/settings/smtpserver/test',
                    dataType: "JSON",
                    data: {
                        "smtpServer": document.getElementById('ConfSmtpServer').value,
                        "smtpPort": parseInt(document.getElementById('ConfSmtpPort').value),
                        "smtpAuthentication": document.getElementById('ConfSmtpAuth').checked,
                        "smtpSsl": document.getElementById('ConfSmtpSsl').checked,
                        "smtpUsername": document.getElementById('ConfSmtpUser').value,
                        "smtpUsernamePassword": document.getElementById('ConfSmtpPassword').value,
                        "smtpFrom": document.getElementById('ConfSmtpFrom').value,
                        "smtpConnectionTimeout": document.getElementById('ConfSmtpTimeout').value
                    },
                    success: function (response) {
                        alert(response.data.message);
                    },
                    async: false
                });
            });

        });

        function checkEmailForError(email, elemName) {
            if (!validateEmail(email)) {
                document.getElementById(elemName).style.color = "red";
                document.getElementById(elemName).innerHTML = "Written e-mail is invalid";
            } else {
                document.getElementById(elemName).hidden = true;
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server" method="POST" action="SaveSettings.aspx">
        <div>
            <table runat="server" id="tableSystemConfiguration" style="width: 100%; height: 100%; padding: 0; border-collapse: collapse;">
                <tr>
                    <td style="height: 100%;">
                        <div style="margin: 10px;">
                            <span class="work_header">
                                <%= resources.LNG_SYSTEM_CONFIGURATION %></span>
                            <br />
                            <b runat="server" id="successMessage" style="display: none;">
                                <%=resources.LNG_SYSTEM_CONFIGURATION_HAVE_BEEN_SUCESSFULLY_CHANGED %>.
                            </b>

                            <table>
                                <tr>
                                    <td>
                                        <div id="accordion" style="width: 100%; min-width: 770px;">

                                            <h3><%= resources.LNG_CONSOLE_INTERFACE %></h3>
                                            <table style="width: 100%;">
                                                <tr>
                                                    <td></td>
                                                    <td>
                                                        <p style="color: #aaaaaa;"><%=resources.LNG_LANG_DESC %>.</p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input runat="server" type="checkbox" id="ConfEnableHelp" name="ConfEnableHelp" value="1" />
                                                    </td>
                                                    <td>
                                                        <label for="ConfEnableHelp"><%=resources.LNG_ENABLE_HELP %></label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input runat="server" type="checkbox" id="SimpleGroupSending" name="SimpleGroupSending" value="1" />
                                                    </td>
                                                    <td>
                                                        <label for="SimpleGroupSending"><%=resources.SIMPLE_GROUP_SENDING %></label>
                                                    </td>
                                                </tr>
                                                <tr style="display: none;">
                                                    <td>
                                                        <input runat="server" runat="server" type="checkbox" id="ConfShowNewsBanner" name="ConfShowNewsBanner" value="1" />
                                                    </td>
                                                    <td>
                                                        <label for="ConfShowNewsBanner"><%=resources.LNG_SHOW_NEWS_BANNER %></label>
                                                    </td>
                                                </tr>
                                                <tr runat="server" id="approvalTableRow">
                                                    <td>
                                                        <input runat="server" type="checkbox" id="ConfAllowApprove" name="ConfAllowApprove" value="1" />
                                                    </td>
                                                    <td>
                                                        <label for="ConfAllowApprove"><%=resources.LNG_ALLOW_APPROVE %></label>
                                                    </td>
                                                </tr>
                                            </table>

                                            <h3><%=resources.LNG_ALERTS %></h3>
                                            <table style="width: 100%;">
                                                <!-- Was temporary removed. Until we'll figure out why we need this.-->
                                                <tr style="display: none;">
                                                    <td>
                                                        <input runat="server" type="checkbox" id="ConfEnableHtmlAlertTitle" name="ConfEnableHtmlAlertTitle" value="1" />
                                                    </td>
                                                    <td>
                                                        <label for="ConfEnableHtmlAlertTitle"><%=resources.LNG_ENABLE_ALERT_TITLE_STYLING %></label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input runat="server" type="checkbox" id="ConfShowSkinsDialog" name="ConfShowSkinsDialog" value="1" />
                                                    </td>
                                                    <td>
                                                        <label for="ConfShowSkinsDialog"><%=resources.LNG_SHOW_SKINS_DIALOG %></label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input runat="server" type="checkbox" id="ConfShowTypesDialog" name="ConfShowTypesDialog" value="1" />
                                                    </td>
                                                    <td>
                                                        <label for="ConfShowTypesDialog"><%=resources.LNG_SHOW_TYPES_DIALOG %></label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <br />
                                                        <%=resources.LNG_SHOW_POSTPONE %>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input runat="server" id="ConfEnablePostponeInPreview1" name="ConfEnablePostponeInPreview" type="radio" value="1" />
                                                    </td>
                                                    <td>
                                                        <label for="ConfEnablePostponeInPreview1"><%=resources.LNG_SHOW_FOR_ALL %></label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input runat="server" id="ConfEnablePostponeInPreview2" name="ConfEnablePostponeInPreview" type="radio" value="2" />
                                                    </td>
                                                    <td>
                                                        <label for="ConfEnablePostponeInPreview2"><%=resources.LNG_SHOW_FOR_NON_URGENT %></label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input runat="server" id="ConfEnablePostponeInPreview0" name="ConfEnablePostponeInPreview" type="radio" value="0" />
                                                    </td>
                                                    <td>
                                                        <label for="ConfEnablePostponeInPreview0"><%=resources.LNG_SHOW_FOR_NONE %></label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td style="color: #aaaaaa">
                                                        <%=resources.LNG_SHOW_POSTPONE_DESC %>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <%=resources.LNG_DIGSIGN_SETTINGS + ":" %>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input runat="server" type="number" style="float: left;" id="ConfDSRefreshRateValue" name="ConfDSRefreshRateValue" min="1" max="60" />
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="ConfDSRefreshRateFactor" name="ConfDSRefreshRateFactor" runat="server"></asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr runat="server" id="ImportPhonesTR">
                                                    <td>
                                                        <input runat="server" type="checkbox" id="ConfEnableManualImportPhones" name="ConfEnableManualImportPhones" value="1" />
                                                    </td>
                                                    <td>
                                                        <label for="ConfEnableManualImportPhones"><%=resources.LNG_IMPORT_PHONE_NUMBERS_FROM_CSV_ENABLE %></label>
                                                    </td>
                                                </tr>
                                            </table>

                                            <h3 style="display: none;"><%=resources.LNG_MESSAGE_DELIVERY %></h3>
                                            <table style="width: 100%; display: none;">
                                                <tr>
                                                    <td>
                                                        <input runat="server" type="checkbox" id="ConfEnableMultiVote" name="ConfEnableMultiVote" value="1" />
                                                    </td>
                                                    <td colspan="2">
                                                        <label for="ConfEnableMultiVote">
                                                            <%=resources.LNG_MULTIPLE_VOTES_DESC %>
                                                        </label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;
                                                    </td>
                                                    <td colspan="2">
                                                        <br />
                                                        <%=resources.LNG_LIFETIME_MOVED_TO_DEFAULTS %>
                                                    </td>
                                                </tr>

                                                <tr style="display: none;">
                                                    <td>&nbsp;
                                                    </td>
                                                    <td colspan="2">
                                                        <br />
                                                        <a href="templates.asp" target="work_area">
                                                            <%=resources.LNG_TEMPLATES %>
                                                        </a><i>(<%=resources.LNG_DEPRECATED %>)</i>
                                                    </td>
                                                </tr>
                                            </table>

                                            <h3 id="NonAdUsersConnectionPermisionLevelHeader" runat="server"><%=resources.LNG_AD_ED %></h3>
                                            <table id="NonAdUsersConnectionPermisionLevelTable" runat="server" border="0" style="width: 100%;">
                                                <tr>
                                                    <td colspan="3">
                                                        <div>
                                                            <div style="margin-bottom: 10px; text-align: center;"><%=resources.LNG_END_USER_AUTH %></div>
                                                            <input style="width: 500px;" data-slider-id="NonADUsersConnectionPermisionLevelHandler" id="NonADUsersConnectionPermisionLevel" type="text" />
                                                            <div style="text-align: center; margin-top: 50px;" id="NonADUsersConnectionPermisionLevelHint"></div>
                                                            <div style="text-align: center; margin-top: 10px;" id="NonADUsersConnectionPermisionLevelAdditionalHint"></div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr style="display: none;">
                                                    <td>
                                                        <input runat="server" type="checkbox" id="ConfEnableADEDCheck" name="ConfEnableADEDCheck" value="1" />
                                                    </td>
                                                    <td colspan="2">
                                                        <label for="ConfEnableADEDCheck">
                                                            <%=resources.LNG_AD_ED_CHECK_DESC %>
                                                        </label>
                                                    </td>
                                                </tr>
                                                <tr style="display: none;">
                                                    <td>&nbsp;
                                                    </td>
                                                    <td colspan="2">
                                                        <p style="color: #aaaaaa;"><%=resources.LNG_AD_ED_CHECK_DESC2 %>.</p>
                                                    </td>
                                                </tr>
                                                <tr style="display: none;">
                                                    <td>
                                                        <input runat="server" type="checkbox" id="ConfEnableRegAndADEDMode" name="ConfEnableRegAndADEDMode" value="1" />
                                                    </td>
                                                    <td colspan="2">
                                                        <label for="ConfEnableRegAndADEDMode">
                                                            <%=resources.LNG_AD_REG_DESC %>
                                                        </label>
                                                    </td>
                                                </tr>
                                                <tr style="display: none;">
                                                    <td>
                                                        <input runat="server" type="checkbox" id="ConfForceSelfRegWithAD" name="ConfForceSelfRegWithAD" value="1" />
                                                    </td>
                                                    <td colspan="2">
                                                        <label for="ConfForceSelfRegWithAD">
                                                            <%=resources.LNG_FORCE_SELFREG_WITH_AD %>
                                                        </label>
                                                    </td>
                                                </tr>

                                                <tr runat="server" id="egsSyncGroup">
                                                    <td>
                                                        <input runat="server" type="checkbox" id="ConfDisableGroupsSync" name="ConfDisableGroupsSync" value="1" />
                                                    </td>
                                                    <td colspan="2">
                                                        <label for="ConfDisableGroupsSync">Disable Group Synchronizations</label>
                                                    </td>
                                                </tr>

                                                <tr id="checkPushPanel" runat="server" visible="false">
                                                    <td>
                                                        <button id="checkPushButton" type="button">Check</button>
                                                    </td>
                                                    <td colspan="2">
                                                        <label for="checkPushButton">Check push notification servers</label>
                                                    </td>
                                                </tr>



                                                <tr class="PushNotificationPanel">
                                                    <td>Apple Push Notification Service
                                                    </td>
                                                    <td colspan="2">
                                                        <span id="apnsResponse"></span>
                                                    </td>
                                                </tr>

                                                <tr class="PushNotificationPanel">
                                                    <td>Google Cloud Messaging
                                                    </td>
                                                    <td colspan="2">
                                                        <span id="gcmResponse"></span>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <input runat="server" type="checkbox" id="ConfEnableADSyncLogDetails" name="ConfEnableADSyncLogDetails" value="1" />
                                                    </td>
                                                    <td>
                                                        <label for="ConfEnableADSyncLogDetails"><%=resources.LNG_ENABLE_ADSYNC_LOG_DETAILS %></label>
                                                    </td>
                                                </tr>
                                            </table>

                                            <h3 runat="server" id="apiLabel"><%=resources.LNG_API %></h3>
                                            <table border="0" runat="server" style="width: 100%;" id="apiSecretPanel">
                                                <tr>
                                                    <td>&nbsp;
                                                    </td>
                                                    <td>
                                                        <label for="ConfAPISecret">
                                                            <%=resources.LNG_API_SECRET %>:</label>
                                                    </td>
                                                    <td>
                                                        <input runat="server" type="text" name="ConfAPISecret" id="ConfAPISecret" value="" size="30" />
                                                    </td>
                                                    <td>
                                                        <a href="#" onclick="javascript: regenerateAPIsecret();">
                                                            <%=resources.LNG_REGENERATE %>
                                                        </a>
                                                    </td>
                                                </tr>
                                            </table>

                                            <h3><%=resources.LNG_LANGUAGE %></h3>
                                            <div runat="server" id="languageDemo">
                                                <p>The default language for DeskAlerts console cannot be changed in public demo version. You can, however, use it in one of the supported languages under your own credentials. To use the localized product, do the following:</p>
                                                <ul>
                                                    <li>Create your own account using Publishers menu on the left - just go to Publishers list and add a new one for yourself.</li>
                                                    <li>Log out of a DeskAlerts console using Logout button at the top right of the screen, then log in with your new credentials.</li>
                                                    <li>Go to Settings->Profile settings and change the language there.</li>
                                                </ul>
                                            </div>
                                            <table style="width: 100%;" runat="server" id="language">
                                                <tr>
                                                    <td>&nbsp;
                                                    </td>
                                                    <td style="width: 150px;">
                                                        <label for="ConfDefaultLanguage"><%=resources.LNG_DEFAULT_LANGUAGE %>:</label>
                                                    </td>
                                                    <td style="text-align: left;">
                                                        <asp:DropDownList runat="server" ID="ConfDefaultLanguage" name="ConfDefaultLanguage">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td>
                                                        <p runat="server" id="LanguageAdditionalDescription"></p>
                                                    </td>
                                                    <td style="width: 100px;" id="LanguageModificationTable">
                                                        <a href="LanguageList.aspx">
                                                            <%=resources.LNG_ADD_NEW %>
                                                        </a>
                                                        <br />
                                                        <a id="langEditLink" href="LanguageList.aspx?lang=">
                                                            <%=resources.LNG_MODIFY_THIS %>
                                                        </a>
                                                    </td>
                                                </tr>
                                            </table>
                                            <h3><%=resources.LNG_DATE_AND_TIME %></h3>
                                            <table style="width: 100%;">
                                                <tr>
                                                    <td>
                                                        <label><% = resources.LNG_DATE_FORMAT %>:</label></td>
                                                    <td>
                                                        <asp:DropDownList runat="server" ID="ConfDateFormat" name="ConfDateFormat" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <label><% = resources.LNG_TIME_FORMAT %>:</label>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList runat="server" ID="ConfTimeFormat" name="ConfTimeFormat">
                                                            <asp:ListItem Value="HH:mm" Text="HH:mm" />
                                                            <asp:ListItem Value="hh:mm tt" Text="hh:mm tt" />
                                                            <asp:ListItem Value="H:mm" Text="H:mm" />
                                                            <asp:ListItem Value="h:mm tt" Text="h:mm tt" />
                                                            <asp:ListItem Value="HH:mm:ss" Text="HH:mm:ss" />
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 295px">
                                                        <label><% = resources.LNG_EXAMPLE_OF_DATE_AND_TIME %>:</label>
                                                    </td>
                                                    <td style="text-align: left;">
                                                        <label id="dateTimeExample"></label>
                                                    </td>
                                                </tr>
                                            </table>
                                            <h3 runat="server" id="emailSettingsLabel"><% = resources.LNG_SMTP_SETTINGS %></h3>
                                            <table style="width: 100%;" runat="server" id="emailSettingsTable">
                                                <tr>
                                                    <td>
                                                        <label for="smtpServer"><%=resources.LNG_SMTP_SERVER %>:</label>
                                                    </td>
                                                    <td style="text-align: left;">
                                                        <input id="ConfSmtpServer" name="ConfSmtpServer" runat="server" value="" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <label for="smtpPort"><%=resources.LNG_SMTP_PORT %>:</label>
                                                    </td>
                                                    <td style="text-align: left;">
                                                        <input type="number" runat="server" name="ConfSmtpPort" id="ConfSmtpPort" value="" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <label for="smtpSSL"><%=resources.LNG_SMTP_SSL %>:</label>
                                                    </td>
                                                    <td style="text-align: left;">
                                                        <input type="checkbox" name="ConfSmtpSsl" id="ConfSmtpSsl" runat="server" value="1" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <label for="smtpAuth"><%=resources.LNG_SMTP_AUTH %>:</label>
                                                    </td>
                                                    <td style="text-align: left;">
                                                        <input type="checkbox" name="ConfSmtpAuth" id="ConfSmtpAuth" value="1" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <label for="smtpUsername"><%=resources.LNG_SMTP_USERNAME %>:</label>
                                                    </td>
                                                    <td style="text-align: left;">
                                                        <input onkeyup="checkEmailForError(event.target.value, 'validationEmailMessage')" id="ConfSmtpUser" name="ConfSmtpUser" value="" runat="server" />
                                                        <div id="validationEmailMessage"></div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <label for="smtpPassword"><%=resources.LNG_SMTP_PASSWORD %>:</label>
                                                    </td>
                                                    <td style="text-align: left;">
                                                        <input runat="server" name="ConfSmtpPassword" id="ConfSmtpPassword" value="" />
                                                        <input type="hidden" name="newPasswordListner" id="newPasswordListner" value="0" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <label for="smtpFrom"><%=resources.LNG_SMTP_FROM %>:</label>
                                                    </td>
                                                    <td style="text-align: left;">
                                                        <input onkeyup="checkEmailForError(event.target.value,'validationEmailMessage2')" id="ConfSmtpFrom" name="ConfSmtpFrom" value="" runat="server" />
                                                        <div id="validationEmailMessage2"></div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <label for="smtpConnectTimeout"><%=resources.LNG_SMTP_CONNECT_TIMEOUT %>:</label>
                                                    </td>
                                                    <td style="text-align: left;">
                                                        <input type="number" name="ConfSmtpTimeout" id="ConfSmtpTimeout" value="" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr id="SMTPlogsTab" runat="server" visible="True">
                                                    <td>
                                                        <label for="smtpLogging">Enable SMTP logging:</label>
                                                        <input runat="server" type="checkbox" value="1" id="ConfEnableSmtpLogging" />
                                                    </td>
                                                    <td>
                                                        <button id="SMTPEnable" type="button"><%=resources.OPEN_SMTP_LOG_FILE %></button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td>
                                                        <button id="SMTPTestConnection" type="button"><%=resources.TEST_SMTP_CONNECTION %></button>
                                                    </td>
                                                </tr>
                                            </table>

                                            <h3><%=resources.LNG_SMS_SETTINGS %></h3>
                                            <div id="tabs-sms-settings">
                                                <% int countOfModules = 0;
                                                    if (Config.Data.HasModule(Modules.SMS)) countOfModules++;
                                                    if (Config.Data.HasModule(Modules.SMPP)) countOfModules++;
                                                    if (Config.Data.HasModule(Modules.TEXTTOCALL)) countOfModules++;

                                                    if (countOfModules > 1)
                                                    {
                                                %>
                                                <ul style="list-style-type: none;">
                                                    <% if (Config.Data.HasModule(Modules.SMS))
                                                        { %>
                                                    <li><a href="#tabs-sms-settings-sms">SMS</a></li>
                                                    <% } %>
                                                    <% if (Config.Data.HasModule(Modules.SMPP))
                                                        { %>
                                                    <li><a href="#tabs-sms-settings-smpp">SMPP</a></li>
                                                    <% } %>
                                                    <% if (Config.Data.HasModule(Modules.TEXTTOCALL))
                                                        { %>
                                                    <li><a href="#tabs-sms-settings-text-to-call">Text to call</a></li>
                                                    <% } %>
                                                </ul>
                                                <% }
                                                    if (Config.Data.HasModule(Modules.SMS))
                                                    { %>
                                                <div id="tabs-sms-settings-sms">
                                                    <table style="width: 100%;">
                                                        <tr>
                                                            <td>
                                                                <label for="smsUrl"><%=resources.LNG_SMS_URL %>:</label>
                                                            </td>
                                                            <td style="text-align: left;">
                                                                <input id="ConfSmsUrl" name="ConfSmsUrl" value="" style="width: 600px;" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <label for="contentType"><%=resources.LNG_CHOOSE_ALERT_TYPE %></label>
                                                            </td>
                                                            <td style="text-align: left;">
                                                                <label>
                                                                    <input runat="server" type="radio" name="ConfSmsContentType" id="ConfSmsContentType1" value="application/x-www-form-urlencoded" />
                                                                    Plain text
                                                               
                                                                </label>
                                                                <label>
                                                                    <input runat="server" type="radio" name="ConfSmsContentType" id="ConfSmsContentType2" value="application/json" />
                                                                    JSON
                                                               
                                                                </label>
                                                                <label>
                                                                    <input runat="server" type="radio" name="ConfSmsContentType" id="ConfSmsContentType3" value="application/xml" />
                                                                    XML
                                                               
                                                                </label>
                                                                <div>Use %smsText% in place where your sms text should be.</div>
                                                                <div>Use %mobilePhone% in place where your recipient number should be.</div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <label for="smsPostData"><%=resources.LNG_POST_DATA %>:</label>
                                                            </td>
                                                            <td style="text-align: left;">
                                                                <textarea style="width: 600px;" id="ConfSmsPostData" name="ConfSmsPostData" runat="server"></textarea>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <label for="ConfSmsAuthType"><%=resources.LNG_SMS_AUTH_TYPE %>:</label>
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList runat="server" ID="ConfSmsAuthType" name="ConfSmsAuthType">
                                                                    <asp:ListItem Value="0" Text="None" />
                                                                    <asp:ListItem Value="1" Text="Basic" />
                                                                    <asp:ListItem Value="2" Text="Auth Key" />
                                                                    <asp:ListItem Value="3" Text="Zenvia" />
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr id="smsUserPassword">
                                                            <td></td>
                                                            <td>
                                                                <input id="ConfSmsAuthUser" name="ConfSmsAuthUser" type="text" placeholder="Login" value="" runat="server" style="width: 600px;" />
                                                                <br />
                                                                <input id="ConfSmsAuthPass" name="ConfSmsAuthPass" placeholder="Password" value="" runat="server" style="width: 600px;" />
                                                            </td>
                                                        </tr>
                                                        <tr id="smsAuthKey">
                                                            <td>
                                                                <label><%=resources.LNG_SMS_AUTH_KEY %>:</label>
                                                            </td>
                                                            <td>
                                                                <input id="ConfSmsAuthKey" name="ConfSmsAuthKey" value="" style="width: 600px;" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <label><%=resources.LNG_SMS_USE_PROXY_AUTH %>:</label>
                                                            </td>
                                                            <td>
                                                                <input id="ConfSmsProxyEnable" name="ConfSmsProxyEnable" type="checkbox" runat="server" value="1" />
                                                            </td>
                                                        </tr>
                                                        <tr class="UseProxyAuth">
                                                            <td>
                                                                <label><%=resources.LNG_SMS_PROXY_URL %>:</label>
                                                            </td>
                                                            <td>
                                                                <input id="ConfSmsProxyServer" name="ConfSmsProxyServer" value="" style="width: 600px;" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="UseProxyAuth">
                                                            <td></td>
                                                            <td>
                                                                <input id="ConfSmsProxyUser" name="ConfSmsProxyUser" type="text" placeholder="Login" value="" runat="server" style="width: 600px;" />
                                                                <input id="ConfSmsProxyPass" name="ConfSmsProxyPass" placeholder="Password" value="" runat="server" style="width: 600px;" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <% } %>
                                                <% if (Config.Data.HasModule(Modules.SMPP))
                                                    { %>
                                                <div id="tabs-sms-settings-smpp">
                                                    <table style="width: 100%;">
                                                        <tr>
                                                            <td>
                                                                <label for="ConfSmppEnabled">SMPP enabled:</label>
                                                            </td>
                                                            <td>
                                                                <input id="ConfSmppEnabled" name="ConfSmppEnabled" type="checkbox" runat="server" value="1" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <label for="ConfSmppId">Host:</label>
                                                                <img src="images/help.png" title="<%=resources.LNG_SMPP_HOST %>." />
                                                            </td>
                                                            <td>
                                                                <input type="text" id="ConfSmppHost" name="ConfSmppHost" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <label for="ConfSmppId">Password:</label>
                                                            </td>
                                                            <td>
                                                                <input id="ConfSmppPassword" name="ConfSmppPassword" runat="server" value="" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <label for="ConfSmppPort">Port:</label>
                                                                <img src="images/help.png" title="<%=resources.LNG_PORT %>." />
                                                            </td>
                                                            <td>
                                                                <input type="number" id="ConfSmppPort" name="ConfSmppPort" runat="server" value="0" min="0" max="65535" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <label for="ConfSmppSourceAddress">Source address:</label>
                                                                <img src="images/help.png" title="<%=resources.LNG_SMPP_SOURCE %>." />
                                                            </td>
                                                            <td>
                                                                <input type="text" id="ConfSmppSourceAddress" name="ConfSmppSourceAddress" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <label for="ConfSmppTimeOut">TimeOut:</label>
                                                                <img src="images/help.png" title="<%=resources.LNG_SMPP_TIMEOUT %>." />
                                                            </td>
                                                            <td>
                                                                <input type="number" id="ConfSmppTimeOut" name="ConfSmppTimeOut" runat="server" value="0" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <label for="ConfSmppReconnectInteval">Reconnect inteval:</label>
                                                                <img src="images/help.png" title="<%=resources.LNG_SMPP_RECONNECT_INTERVAL %>." />
                                                            </td>
                                                            <td>
                                                                <input type="number" id="ConfSmppReconnectInteval" name="ConfSmppReconnectInteval" runat="server" value="0" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <label for="ConfSmppAutoReconnectDelay">Reconnect delay:</label>
                                                                <img src="images/help.png" title="<%=resources.LNG_SMPP_RECONNECT_DELAY %>." />
                                                            </td>
                                                            <td>
                                                                <input type="number" id="ConfSmppAutoReconnectDelay" name="ConfSmppAutoReconnectDelay" runat="server" value="0" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <label for="ConfSmppDefaultServiceType">Default service type:</label>
                                                                <img src="images/help.png" title="<%=resources.LNG_SMPP_SERVICE_TYPE %>." />
                                                            </td>
                                                            <td>
                                                                <input type="text" id="ConfSmppDefaultServiceType" name="ConfSmppDefaultServiceType" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <label for="ConfSmppSystemID">System ID:</label>
                                                                <img src="images/help.png" title="<%=resources.LNG_SMPP_SYSTEM_ID %>." />
                                                            </td>
                                                            <td>
                                                                <input type="text" id="ConfSmppSystemID" name="ConfSmppSystemID" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <label for="ConfSmppSystemType">System type:</label>
                                                                <img src="images/help.png" title="<%=resources.LNG_SMPP_SYSTEM_TYPE %>." />
                                                            </td>
                                                            <td>
                                                                <input type="text" id="ConfSmppSystemType" name="ConfSmppSystemType" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <label for="ConfSmppEncoding">Encoding:</label>
                                                                <img src="images/help.png" title="<%=resources.LNG_SMPP_ENCODING %>." />
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList DataTextField="Key" DataValueField="Value" ID="ConfSmppEncoding" name="ConfSmppEncoding" runat="server"></asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <label for="ConfSmppRegisteredDeliveryValue">Delivery value:</label>
                                                                <img src="images/help.png" title="<%=resources.LNG_SMPP_DELIVERY %>." />
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList DataTextField="Key" DataValueField="Value" ID="ConfSmppRegisteredDeliveryValue" name="ConfSmppRegisteredDeliveryValue" runat="server"></asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <label for="ConfSmppKeepAliveInterval">Keep alive interval:</label>
                                                                <img src="images/help.png" title="<%=resources.LNG_SMPP_KEEP_ALIVE_INTERVAL %>." />
                                                            </td>
                                                            <td>
                                                                <input type="number" id="ConfSmppKeepAliveInterval" name="ConfSmppKeepAliveInterval" runat="server" value="0" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <% } %>
                                                <% if (Config.Data.HasModule(Modules.TEXTTOCALL))
                                                    { %>
                                                <div id="tabs-sms-settings-text-to-call">
                                                    <table style="width: 100%;">
                                                        <tr>
                                                            <td>
                                                                <label for="ConfTextToCallSID">SID:</label>
                                                            </td>
                                                            <td>
                                                                <input type="text" id="ConfTextToCallSID" name="ConfTextToCallSID" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <label><%= resources.LNG_VOICE_API_NUMBER %>:</label>
                                                            </td>
                                                            <td>
                                                                <input id="ConfTextToCallNumber" name="ConfTextToCallNumber" type="text" runat="server" value="" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <label for="ConfTextToCallSID">Password:</label>
                                                            </td>
                                                            <td>
                                                                <input id="ConfTextToCallAuthToken" name="ConfTextToCallAuthToken" runat="server" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <% } %>
                                            </div>

                                            <h3><%=resources.LNG_TKN_SETTINGS %></h3>
                                            <table style="width: 100%;">
                                                <tr>
                                                    <td>
                                                        <input runat="server" type="checkbox" id="ConfTokenAuthEnable" name="ConfTokenAuthEnable" value="1" />
                                                    </td>
                                                    <td>
                                                        <label for="ConfTokenAuthEnable">
                                                            <%=resources.LNG_TKN_ENABLE %>
                                                        </label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <input runat="server" type="checkbox" id="ConfTokenMaxCountForServerEnable" name="ConfTokenMaxCountForServerEnable" value="1" />
                                                                </td>
                                                                <td>
                                                                    <label for="ConfTokenMaxCountForServer"><%=resources.LNG_TKN_MAX_SERVER_COUNT %>:</label>
                                                                </td>
                                                                <td style="text-align: left;">
                                                                    <input type="number" name="ConfTokenMaxCountForServer" id="ConfTokenMaxCountForServer" value="" runat="server" min="1" max="1000000" step="1" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <input runat="server" type="checkbox" id="ConfTokenMaxCountForUserEnable" name="ConfTokenMaxCountForUserEnable" value="1" />
                                                                </td>
                                                                <td>
                                                                    <label for="ConfTokenMaxCountForUser"><%=resources.LNG_TKN_MAX_USER_COUNT %>:</label>
                                                                </td>
                                                                <td style="text-align: left;">
                                                                    <input type="number" name="ConfTokenMaxCountForUser" id="ConfTokenMaxCountForUser" value="" runat="server" min="1" max="1000000" step="1" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td></td>
                                                                <td style="color: #aaaaaa">
                                                                    <%=resources.LNG_TKN_MAX_USER_COUNT_DESC %>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <input runat="server" type="checkbox" id="ConfTokenExpirationTimeEnable" name="ConfTokenExpirationTimeEnable" value="1" />
                                                                </td>
                                                                <td>
                                                                    <label for="ConfTokenExpirationTime"><%=resources.LNG_TKN_EXP_MIN %>:</label>
                                                                </td>
                                                                <td style="text-align: left;">
                                                                    <input type="number" name="ConfTokenExpirationTime" id="ConfTokenExpirationTime" value="" runat="server" min="1" max="1000000" step="1" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>

                                            <h3><%=resources.LNG_LOGGING_SETTINGS %></h3>
                                            <div>
                                                <table style="width: 100%;">
                                                    <tr>
                                                        <td>
                                                            <p>Log levels enabled:</p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <label for="ConfTraceLogLevelEnabled">Trace</label>
                                                                    </td>
                                                                    <td>
                                                                        <input runat="server" type="checkbox" id="ConfTraceLogLevelEnabled" name="ConfTraceLogLevelEnabled" value="1" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <label for="ConfInfoLogLevelEnabled">Info</label>
                                                                    </td>
                                                                    <td>
                                                                        <input runat="server" type="checkbox" id="ConfInfoLogLevelEnabled" name="ConfInfoLogLevelEnabled" value="1" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <label for="ConfDebugLogLevelEnabled">Debug</label>
                                                                    </td>
                                                                    <td>
                                                                        <input runat="server" type="checkbox" id="ConfDebugLogLevelEnabled" name="ConfDebugLogLevelEnabled" value="1" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <label for="ConfErrorLogLevelEnabled">Error</label>
                                                                    </td>
                                                                    <td>
                                                                        <input runat="server" type="checkbox" id="ConfErrorLogLevelEnabled" name="ConfErrorLogLevelEnabled" value="1" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <label for="ConfFatalLogLevelEnabled">Fatal</label>
                                                                    </td>
                                                                    <td>
                                                                        <input runat="server" type="checkbox" id="ConfFatalLogLevelEnabled" name="ConfFatalLogLevelEnabled" value="1" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div>
                                                    <label for="ConfLogDeleteDays"><%=resources.LNG_LOG_DELETE_DAYS_TEXT %></label>
                                                    <input runat="server" type="text" id="ConfLogDeleteDays" name="ConfLogDeleteDays" value="14" min="1" max="1825" step="1" />
                                                </div>
                                            </div>

                                            <% if (Config.Data.HasModule(Modules.SSO))
                                                { %>
                                            <!-- ToDo: add localization -->
                                            <h3>SSO</h3>
                                            <table style="width: 100%;">
                                                <tr>
                                                    <td>
                                                        <input runat="server" type="checkbox" id="ConfSsoEnable" name="ConfSsoEnable" value="1" />
                                                    </td>
                                                    <td>
                                                        <label for="ConfSsoEnable"><%=resources.LNG_SSO_ENABLED %></label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Search for groups</td>
                                                    <td>
                                                        <input id="groupSearch" name="groupSearch" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td>
                                                        <div id="ADGroupList"></div>
                                                    </td>
                                                </tr>
                                            </table>
                                            <% } %>

                                            <!--Autoupdate client-->
                                            <h3>Client autoupdate (Windows)</h3>
                                            <table style="width: 100%;">
                                                <tr runat="server" id="currentClientVersionTab">
                                                    <td>
                                                        <label for="lastClientVersion">Currently uploaded version:</label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lastClientVersion" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <label for="clientApplicationUpload">Upload new MSI file:</label>
                                                    </td>
                                                    <td>
                                                        <asp:FileUpload ID="clientApplicationUpload" runat="server" accept=".msi" multiple="false" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <label for="ConfAutoupdateSlotCacheLength">Connections limit (max):</label>
                                                    </td>
                                                    <td>
                                                        <input type="number" name="ConfAutoupdateSlotCacheLength" id="ConfAutoupdateSlotCacheLength" value="" runat="server" min="1" max="2147483647" step="1" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <label for="ConfAutoupdateSlotLifeTime">Connection lifetime (minutes):</label>
                                                    </td>
                                                    <td>
                                                        <input type="number" name="ConfAutoupdateSlotLifeTime" id="ConfAutoupdateSlotLifeTime" value="" runat="server" min="1" max="2147483647" step="1" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <button id="deleteClientUpdateFile" type="button" onclick="deleteAutoUpdateFile"><%=resources.DELETE_AUTOUPDATE_FILE%></button>
                                                    </td>
                                                </tr>

                                            </table>
                                            <!--End-Autoupdate client-->
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;">
                                        <a class="cancel_button" onclick="javascript:history.go(-1); return false;">
                                            <%=resources.LNG_CANCEL %>
                                        </a>
                                        <input id="cantSaveDemoSetingsSystem" runat="server" class="submit" type="submit" value="Save" />
                                        <input type="hidden" name="a" value="2" />
                                        <br />
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div style="display: none;" id='policy_dialog' title='Select policy'></div>
                        <input type="hidden" name="ConfDesktopAlertByDefault" id="ConfDesktopAlertByDefault" runat="server" />
                        <input type="hidden" name="ConfDesktopAlertType" id="ConfDesktopAlertType" runat="server" />
                        <input type="hidden" name="ConfPostToBlogByDefault" id="ConfPostToBlogByDefault" runat="server" />
                        <input type="hidden" name="ConfEmailByDefault" id="ConfEmailByDefault" runat="server" />
                        <input type="hidden" name="ConfSMSByDefault" id="ConfSMSByDefault" runat="server" />
                        <input type="hidden" name="ConfSocialMediaByDefault" id="ConfSocialMediaByDefault" runat="server" />
                        <input type="hidden" name="ConfTextCallByDefault" id="ConfTextCallByDefault" runat="server" />
                        <input type="hidden" name="ConfPopupType" id="ConfPopupType" runat="server" />
                        <input type="hidden" name="ConfAlertWidth" id="ConfAlertWidth" runat="server" />
                        <input type="hidden" name="ConfAlertHeight" id="ConfAlertHeight" runat="server" />
                        <input type="hidden" name="ConfAlertPosition" id="ConfAlertPosition" runat="server" />
                        <input type="hidden" name="ConfTickerPosition" id="ConfTickerPosition" runat="server" />
                        <input type="hidden" name="ConfUrgentByDefault" id="ConfUrgentByDefault" runat="server" />
                        <input type="hidden" name="ConfAcknowledgementByDefault" id="ConfAcknowledgementByDefault" runat="server" />
                        <input type="hidden" name="ConfUnobtrusiveByDefault" id="ConfUnobtrusiveByDefault" runat="server" />
                        <input type="hidden" name="ConfSelfDeletableByDefault" id="ConfSelfDeletableByDefault" runat="server" />
                        <input type="hidden" name="ConfAutocloseByDefault" id="ConfAutocloseByDefault" runat="server" />
                        <input type="hidden" name="ConfScheduleByDefault" id="ConfScheduleByDefault" runat="server" />
                        <input type="hidden" name="ConfMessageExpire" id="ConfMessageExpire" runat="server" />
                        <input type="hidden" name="ConfRSSMessageExpire" id="ConfRSSMessageExpire" runat="server" />
                        <input type="hidden" name="ConfSSOEnabled" id="ConfSSOEnabled" runat="server" />
                    </td>
                </tr>
            </table>
        </div>
        <div class="loader-panel">
            <div class="loader"></div>
        </div>
    </form>
</body>
</html>
