﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditPolicy.aspx.cs" Inherits="DeskAlertsDotNet.Pages.EditPolicy" %>

<%@ Import Namespace="DeskAlertsDotNet.Models" %>
<%@ Import Namespace="DeskAlertsDotNet.Parameters" %>
<%@ Import Namespace="DeskAlertsDotNet.Models.Permission" %>
<%@ Import Namespace="Resources" %>

<%@ Register Src="RecipientsSelectionTable.ascx" TagName="RecipientsSelectionTable" TagPrefix="da" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" id="EditPolicyHtml">
<head runat="server">
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="css/style9.css" rel="stylesheet" type="text/css" />
    <link href="css/Policy.min.css" rel="stylesheet" type="text/css" />
    <link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
    <script>

        var common_inputs = [<% =Permission.GetPermissionsForEnabledPolicies() %>];

        function setAvailability(workItems, deciders) {
            var availability = false;

            deciders.some(function (decider) {
                if (decider[0] !== undefined && decider[0].checked === true) {
                    availability = true;
                }
                return availability;
            });

            if (availability) {
                workItems.forEach(function (item) {
                    if ($(item)[0] !== undefined) {
                        $(item).removeAttr("disabled");
                        $(item).removeAttr("title");
                    }
                });
            } else {
                workItems.forEach(function (item) {
                    if ($(item)[0] !== undefined) {
                        $(item).attr("disabled", true);
                        addHeader(item);
                    }
                });
            }
        }

        function addHeader(item) {
            var disabledOptions = ['emails_3', 'sms_3', 'sms', 'emails'];
            if (disabledOptions.indexOf($(item)[0].id) != -1) {
                $(item).attr("title", '<%=Resources.resources.AVAILABLE_IF_SEND_AVAILABLE%>');
            }
        }

        function check_role() {
            if (document.getElementById('policyReg').checked) {
                document.getElementById('policyTabs').style.display = "";
            }
            else {
                document.getElementById('policyTabs').style.display = "none";
            }
            var height = $(window.document).height();
            window.parent.parent.onChangeBodyHeight(height, false);
        }

        $(document).ready(function () {
            $("#recipients_tabs").tabs();
            $("#policyTabs").tabs();
            $(".save_button").button();
            $(".cancel_button").button();

            ChangeSkinsListVisibleStatus();
            ChangeContentSettingsVisibleStatus();
            changeListsHeight();
        });

        function changeListsHeight() {
            var list1NewHeight = document.getElementById("form1").clientHeight - 300;

            $("#list1").css({ "height": list1NewHeight });
            $("#list2").css({ "height": list1NewHeight });
        }

        // Set checkbox from param el to checkbox param val
        function set_fake(el, val) {
            if (el.id != val) {
                var parent = document.getElementById(val);
                if (parent) {
                    parent.checked = el.checked;
                }
            }
            //for (var i = 0; i <= 6; i++) {
            //    var child = document.getElementById(val + "_" + i);
            //    if (child) {
            //        child.checked = el.checked;
            //    }
            //}
        }

        function grantFullAccessFor(obj, groupName) {
            if (obj.checked == 0) {
                elem = document.getElementById(groupName);
                elem.checked = false;
                return;
            }
            for (i = 0; i < 6; i++) {
                set_fake(obj, groupName + "_" + i);
            }
        }

        function set_if_any(val, targ) {
            var checked = false;
            for (var i = 0; i <= 6; i++) {
                //if(targ == val+"_"+i) continue
                var child = document.getElementById(val + "_" + i);
                if (child && child.checked) {
                    checked = true;
                }
            }
            document.getElementById(targ).checked = checked;
        }

        function switch_list(val) {
            switch_list_woch(val);
            checkAll_lists();
        }

        function switch_list_woch(val) {
            var elem = document.getElementById(val);
            for (var i = 0; i <= 7; i++) {
                var el = document.getElementById(val + "_" + i);
                if (el) {
                    el.checked = elem.checked;
                    set_fake(el, val + "_" + i);
                }
            }
        }

        // Check all checkboxes, if all checked, general set to checked and vice versa
        function check_list(val) {
            var elem = document.getElementById(val);
            var all_checked = true;
            for (var i = 0; i < 7; i++) {
                var child = document.getElementById(val + "_" + i);
                if (child && !child.checked) {
                    all_checked = false;
                }
            }
            elem.checked = all_checked;
            checkAll_lists();
        }

        function removeIfDisabled(obj, elemToDisable) {
            if (obj.checked == 0) {
                var elem = document.getElementById(elemToDisable);

                if (elem) {
                    elem.checked = 0;
                }
            }
        }

        function checkAll_lists() {
            var checkAll = document.getElementById('checkAll');
            if (checkAll) {
                var all_checked = true;
                for (key in common_inputs) {
                    var elem = document.getElementById(common_inputs[key]);
                    if (elem && !elem.checked) {
                        all_checked = false;
                    }
                }
                checkAll.checked = all_checked;
            }
        }

        function switch_all() {
            var checkAll = document.getElementById('checkAll');
            if (checkAll) {
                for (key in common_inputs) {
                    var elem = document.getElementById(common_inputs[key]);
                    if (elem) {
                        elem.checked = checkAll.checked;
                        switch_list_woch(common_inputs[key]);
                    }
                }
            }
        }

        function all_recipients_click(state) {
            document.getElementById('addedUsers').style.display = state ? "none" : "";
            changeListsHeight();
        }

        function all_tgroups_click(state) {
            document.getElementById('tgroups').style.display = state ? "none" : "";
        }

        function list_add_tgroup(iframename, listname) {

            window.event.preventDefault();
            var list1 = window.frames[iframename];
            var list2 = document.getElementById(listname);
            //ous
            if (tabType == "ous") {
                res = list1.getCheckedOus();
                idSequence = "";
                if (res) {
                    for (i = 0; i < res.length; i++) {
                        if (res[i].type != "organization" && res[i].type != "DOMAIN") {
                            var newVal = res[i].name.toString();
                            var newId = tabType + "_" + res[i].id.toString();
                            if (checkNewId(newId, listname)) {
                                var newRow = new Option(newVal, newId);
                                if (tabType != "users") {
                                    var beforeItem = getBeforeItem();
                                    var elOptOld = list2.options[beforeItem];
                                    try {
                                        list2.add(newRow, elOptOld); // standards compliant; doesn't work in IE
                                    }
                                    catch (ex) {
                                        list2.add(newRow, beforeItem); // IE only
                                    }
                                }
                                else {
                                    list2.options[list2.length] = newRow;
                                }
                            }
                        }
                    }
                }
            }
            else {
                //other
                var items_ids = list1.document.getElementsByClassName("content_object");
                var items_values = list1.document.getElementsByClassName("content_object");

                for (var i = 0; i < items_ids.length; i++) {
                    if (jQuery(items_ids[i]).is(":checked")) {
                        var newVal = items_values[i].getAttribute("object_name");
                        var newId = items_values[i].value;
                        AddTgroupToInput(newId);
                        if (checkNewId(newId)) {
                            var newRow = new Option(newVal, newId);
                            if (tabType != "users") {
                                var beforeItem = getBeforeItem();
                                var elOptOld = list2.options[beforeItem];
                                try {
                                    list2.add(newRow, elOptOld); // standards compliant; doesn't work in IE
                                }
                                catch (ex) {
                                    list2.add(newRow, beforeItem); // IE only
                                }
                            }
                            else {
                                list2.options[list2.length] = newRow;
                            }
                        }
                    }
                }
            }
        }

        function list_remove_tgroup(listname) {
            window.event.preventDefault();
            var list2 = document.getElementById(listname);
            for (i = list2.options.length - 1; i >= 0; i--) {
                if (list2.options[i].selected == true && !list2.options[i].disabled) {

                    var value = list2.options[i].value;

                    var inputVal = $("#idsTgroups").val();

                    inputVal = inputVal.replace(value[1], '').replace(',,', ",").replace(/(^,)|(,$)/g, "");
                    $("#idsTgroups").val(inputVal);
                    list2.options[i] = null;
                }
            }
        }

        function submitForm() {

            $(function () {

                var selectedPolicyType = $('input[name=policy_type]:checked').val();
                if (selectedPolicyType === "E" && $("#permissionsTable > tbody > tr > td >  input[type=checkbox]:checked").length === 0) {
                    alert("<% =resources.LNG_PLEASE_CHECK_ANY_ACTION_FOR_EDITOR %>");
                    return;
                }

                $("#form1").submit();

            });
        }

        function AddTgroupToInput(id) {

            var currentValue = $("#idsTgroups").val();

            if (currentValue.length > 0)
                currentValue += ",";

            currentValue += id;

            $("#idsTgroups").val(currentValue);
        }

        function ChangeSkinsListVisibleStatus() {
            $("input[name=policy_SkinsAccess_RadioButtonList]").change(function() {
                $("#policy_choosingSkinsList").toggle();
            });
        }

        function ChangeContentSettingsVisibleStatus() {
            $("input[name=policy_ContentSettings_RadioButtonList]").change(function () {
                $("#policy_choosingContentSettingsList").toggle();
            });
        }

    </script>
</head>
<!--  method="post" action="ProcessPolicyEdit.aspx"-->
<body onload="setAvailability([$('#emails_3'), $('#sms_3'), $('#text_to_call_3'), $('#text_to_call'), $('#sms'), $('#emails')], [$('#alerts_0'), $('#alerts_1'), $('#im_0'), $('#im_1')]); setAvailability([$('#viewAll0')], [$('#alerts_0'), $('#surveys_0'), $('#rss_0'), $('#text_templates_0'), $('#im_0'), $('#screensavers_0'), $('#wallpapers_0'),  $('#lockscreens_0')]);" id="EditPolicyBody">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <div>
            <table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
                <tr>
                    <td>
                        <table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
                            <tr>
                                <td width="100%" height="31" class="main_table_title">
                                    <img src="images/menu_icons/policies_20.png" alt="" width="20" height="20" border="0" style="padding-left: 7px">
                                    <a href="EditPolicy.aspx" class="header_title" style="position: absolute; top: 23px"><%=resources.LNG_POLICIES %></a></td>
                            </tr>
                            <tr>
                                <td class="main_table_body" height="100%">
                                    <div style="padding: 10px;">
                                        <span class="work_header"><%=resources.LNG_ADD_POLICY %></span>
                                        <br />
                                        <br />

                                        <input runat="server" id="close" type="hidden" name="close" value="" />
                                        <input type="hidden" name="sub1" value="" />
                                        <input type="hidden" name="ids_users" id="ids_users" value="" />
                                        <input type="hidden" runat="server" name="idsGroups" id="idsGroups" value="" />
                                        <input type="hidden" runat="server" name="policyId" id="policyId" value="" />
                                        <input type="hidden" runat="server" name="edit" id="edit" value="0" />
                                        <input type="hidden" runat="server" name="idsOus" id="idsOus" value="" />
                                        <input type="hidden" runat="server" name="idsComputers" id="idsComputers" value="" />
                                        <input type="hidden" runat="server" name="idsTgroups" id="idsTgroups" value="" />
                                        <input type="hidden" runat="server" name="idsViewers" id="idsViewers" value="" />

                                        <input type="hidden" name="ids_viewers" id="ids_viewers" value="" />
                                        <%= resources.LNG_POLICY_NAME %>:
                                        <input type="text" required id="policyName" name="policyName" runat="server" value="" /><br />
                                        <br />
                                        <b><%=resources.LNG_POLICY_ROLE %>:</b><br />
                                        <br />

                                        <input type="radio" name="policy_type" runat="server" id="policySys" value="A" onclick="check_role()" checked />
                                        <label for="policySys"><%=resources.LNG_SYSTEM_ADMINISTRATOR %></label>
                                        <br />
                                        <br />
                                        <div runat="server" id="policyMod">
                                            <input runat="server" type="radio" name="policy_type" id="policyModRadio" value="M" onclick="check_role()" />
                                            <label for="policy_mod">Global Workforce administrator</label>
                                            <br />
                                            <br />
                                        </div>
                                        <input type="radio" runat="server" name="policy_type" id="policyReg" value="E" onclick="check_role()" />
                                        <label for="policyReg"><%=resources.LNG_REGULAR_EDITOR%></label>
                                        <br />
                                        <br />

                                        <div runat="server" id="policyTabs" style="display: none">
                                            <ul>
                                                <li><a href="#accessControlListTab"><%=resources.LNG_ACCESS_CONTROL_LIST%></a></li>
                                                <li><a href="#policy_recipientsTab"><%=resources.LNG_LIST_OF_RECIPIENTS%></a></li>
                                                <li><a href="#policy_viewersTab"><%=resources.LNG_EDITOR_VIEW_RIGHTS%></a></li>
                                                <li><a href="#policy_SkinsTab"><%=resources.LNG_SKINS_ALERT_SKIN_ACCESS %></a></li>
                                                <li><a href="#policy_ContentSettingsTab"><%=resources.LNG_ALERT_SETTING_ACC %></a></li>

                                                <li runat="server" id="tGroupsTab"><a href="#policyTemplatesGroupsTabContent"><%=resources.LNG_LIST_OF_TGROUPS%></a></li>
                                            </ul>

                                            <div runat="server" id="accessControlListTab">
                                                <div style="margin-bottom: 5px">
                                                    <input type="checkbox" runat="server" id="checkAll" onclick="switch_all(); setAvailability([$('#emails_3'), $('#sms_3'), $('#text_to_call_3'), $('#text_to_call'), $('#sms'), $('#emails')], [$('#alerts_0'), $('#alerts_1'), $('#im_0'), $('#im_1')]); setAvailability([$('#viewAll0')], [$('#alerts_0'), $('#surveys_0'), $('#text_templates_0'), $('#im_0'), $('#screensavers_0'), $('#wallpapers_0'), $('#lockscreens_0')]);" value="1" class="allheckbox" />
                                                    <label for="checkAll"><%=resources.LNG_ALLOW_ALL %></label>
                                                </div>
                                                <table runat="server" id="permissionsTable" class="data_table" cellspacing="0" cellpadding="3">
                                                    <tr class="data_table_title">
                                                        <td width="10" class="table_title checkbox_col"></td>
                                                        <td width="150" class="table_title">&nbsp;</td>
                                                        <td width="50" class="table_title"><%=resources.LNG_CREATE %></td>
                                                        <td width="50" class="table_title"><%=resources.LNG_EDIT %></td>
                                                        <td width="50" class="table_title"><%=resources.LNG_DELETE %></td>
                                                        <td width="50" class="table_title"><%=resources.LNG_SEND %></td>
                                                        <td width="50" class="table_title"><%=resources.LNG_VIEW %></td>
                                                        <td width="50" class="table_title"><%=resources.LNG_STOP %></td>
                                                        <%--<td width="50" style="display: none" class="table_title"><%=resources.LNG_RESIZE %></td>--%>
                                                        <td runat="server" id="approveHeader" width="50" class="table_title"><%=resources.LNG_APPROVAL %></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="checkbox_col">
                                                            <input runat="server" type="checkbox" name="alerts" id="alerts" onclick="switch_list('alerts'); setAvailability([$('#emails_3'), $('#sms_3'), $('#text_to_call_3'), $('#text_to_call'), $('#sms'), $('#emails')], [$('#alerts_0'), $('#alerts_1'), $('#im_0'), $('#im_1')]); setAvailability([$('#viewAll0')], [$('#alerts_0'), $('#surveys_0'), $('#text_templates_0'), $('#im_0'), $('#screensavers_0'), $('#wallpapers_0'), $('#lockscreens_0')]);" value="1" class="linecheckbox" /></td>                                                        
                                                        <td>
                                                            <label class="label_only" for="alerts"><%=resources.LNG_DESKTOP_ALERTS %></label></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="alerts_0" id="alerts_0" onclick="check_list('alerts'); removeIfDisabled(this, 'text_templates_3'); setAvailability([$('#viewAll0')], [$('#alerts_0'), $('#surveys_0'), $('#text_templates_0'), $('#im_0'), $('#screensavers_0'), $('#wallpapers_0'), $('#lockscreens_0')]); setAvailability([$('#emails_3'), $('#sms'), $('#emails'), $('#sms_3'), $('#text_to_call_3')], [$('#alerts_0'), $('#alerts_1'), $('#im_0'), $('#im_1')])" value="1" /></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="alerts_1" id="alerts_1" onclick="check_list('alerts'); set_if_any('alerts', 'alerts_4'); setAvailability([$('#emails_3'), $('#sms'), $('#emails'), $('#sms_3'), $('#text_to_call_3')], [$('#alerts_0'), $('#alerts_1'), $('#im_0'), $('#im_1')])" value="1" /></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="alerts_2" id="alerts_2" onclick="check_list('alerts'); set_if_any('alerts', 'alerts_4');" value="1" /></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="alerts_3" id="alerts_3" onclick="check_list('alerts'); set_if_any('alerts', 'alerts_4');" value="1" /></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="alerts_4" id="alerts_4" onclick="check_list('alerts'); set_fake('alerts', 'alerts_1'); set_fake('alerts', 'alerts_2'); set_fake('alerts', 'alerts_3'); set_fake('alerts', 'alerts_5'); set_fake('alerts', 'alerts_6'); set_fake('alerts', 'alerts_7');" value="1" /></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" id="alerts_5" onclick="check_list('alerts'); set_if_any('alerts', 'alerts_4');" value="1" /></td>
                                                        <td align="center" style="display: none">
                                                            <input runat="server" type="checkbox" name="alerts_6" id="alerts_6" onclick="check_list('alerts'); set_if_any('alerts', 'alerts_4');" value="1" /></td>
                                                        <td align="center" runat="server" id="alertApproveCheckBox">
                                                            <input type="checkbox" runat="server" name="alerts_7" id="alerts_7" onclick="grantFullAccessFor(this, 'alerts');" value="1" /></td>
                                                    </tr>
                                                    <tr runat="server" id="campaignPolicyRow">
                                                        <td class="checkbox_col">
                                                            <input runat="server" type="checkbox" name="campaign" id="campaign" onclick="switch_list('campaign')" value="1" class="linecheckbox" /></td>
                                                        <td>
                                                            <label class="label_only" for="campaign"><%=resources.LNG_CAMPAIGN %></label></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" id="campaign_0" onclick="check_list('campaign'); set_if_any('campaign', 'campaign_4'); $('#alerts_0').prop('checked', true);" value="1" /></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="campaign_1" id="campaign_1" onclick="check_list('campaign'); set_if_any('campaign', 'campaign_4');" value="1" /></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="campaign_2" id="campaign_2" onclick="check_list('campaign'); set_if_any('campaign', 'campaign_4');" value="1" /></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="campaign_3" id="campaign_3" onclick="check_list('campaign'); set_if_any('campaign', 'campaign_4');" value="1" /></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="campaign_4" id="campaign_4" onclick="check_list('campaign'); set_fake('campaign', 'campaign_1'); set_fake('campaign', 'campaign_2'); set_fake('campaign', 'campaign_3'); set_fake('campaign', 'campaign_5'); set_fake('campaign', 'campaign_6'); set_fake('campaign', 'campaign_7');" value="1" /></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="campaign_5" id="campaign_5" onclick="check_list('campaign'); set_if_any('campaign', 'campaign_4')" value="1" /></td>
                                                        <td runat="server" align="center" style="display: none">
                                                            <input runat="server" type="checkbox" name="campaign_6" id="campaign_6" onclick="check_list('campaign'); set_if_any('campaign', 'campaign_4');" value="1" /></td>
                                                        <td align="center" runat="server" id="campaignApprove">
                                                            <input type="checkbox" runat="server" name="campaign_7" id="campaign_7" onclick="grantFullAccessFor(this, 'campaign');" value="1" /></td>
                                                    </tr>
                                                    <tr runat="server" id="emailPolicyRow">
                                                        <td class="checkbox_col">
                                                            <!-- Removed while we have only one email option -->
                                                            <input runat="server" type="checkbox" name="emails" id="emails" onclick="switch_list('emails');" disabled="True" value="1" class="linecheckbox"/>
                                                        </td>
                                                        <td>
                                                            <label class="label_only" for="emails"><%=resources.LNG_EMAILS %></label></td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="emails_3" id="emails_3" onclick="check_list('emails');" disabled="True" value="1" /></td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td style="display: none">&nbsp;</td>
                                                        <td align="center" runat="server" id="emailApprove">&nbsp;</td>
                                                    </tr>
                                                    <tr runat="server" id="smsPolicyRow">
                                                        <td class="checkbox_col">
                                                            <!-- Removed while we have only one sms option -->
                                                            <input runat="server" type="checkbox" name="sms" id="sms" onclick="switch_list('sms')" value="1" disabled ="True" class="linecheckbox" />
                                                        </td>
                                                        <td>
                                                            <label class="label_only" for="sms"><%=resources.LNG_SMS %></label></td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="sms_3" id="sms_3" onclick="check_list('sms');" disabled="True" value="1" /></td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td style="display: none">&nbsp;</td>
                                                        <td align="center" runat="server" id="smsApprove">&nbsp;</td>
                                                    </tr>
                                                    <tr runat="server" id="textToCallPolicyRow">
                                                        <td class="checkbox_col">
                                                            <!-- Removed while we have only one sms option -->
                                                            <input runat="server" type="checkbox" name="text_to_call" id="text_to_call" onclick="switch_list('text_to_call')" value="1" class="linecheckbox" />
                                                        </td>
                                                        <td>
                                                            <label class="label_only" for="text_to_call"><%=resources.LNG_TEXT_TO_CALL %></label>
                                                        </td>
                                                        <td align="center">&nbsp;</td>
                                                        <td align="center">&nbsp;</td>
                                                        <td align="center">&nbsp;</td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="text_to_call_3" id="text_to_call_3" onclick="check_list('text_to_call');" value="1" /></td>
                                                        <td align="center">&nbsp;</td>
                                                        <td align="center">&nbsp;</td>
                                                        <td align="center" style="display: none">&nbsp;</td>
                                                        <td align="center" runat="server" id="textToCallApprove">&nbsp;</td>
                                                    </tr>
                                                    <tr runat="server" id="surveyPolicyRow">
                                                        <td class="checkbox_col">
                                                            <input runat="server" type="checkbox" name="surveys" id="surveys" onclick="switch_list('surveys'); setAvailability([$('#viewAll0')], [$('#alerts_0'), $('#surveys_0'), $('#text_templates_0'), $('#im_0'), $('#screensavers_0'), $('#wallpapers_0')]);" value="1" class="linecheckbox" /></td>
                                                        <td>
                                                            <label class="label_only" for="surveys"><%=resources.LNG_SURVEYS %></label></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" id="surveys_0" onclick="check_list('surveys'); set_if_any('surveys', 'surveys_3'); set_if_any('surveys', 'surveys_4'); setAvailability([$('#viewAll0')], [$('#alerts_0'), $('#surveys_0'), $('#text_templates_0'), $('#im_0'), $('#screensavers_0'), $('#wallpapers_0'), $('#lockscreens_0')]);" value="1" /></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="surveys_1" id="surveys_1" onclick="check_list('surveys'); set_if_any('surveys', 'surveys_4');" value="1" /></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="surveys_2" id="surveys_2" onclick="check_list('surveys'); set_if_any('surveys', 'surveys_4');" value="1" /></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="surveys_3" id="surveys_3" onclick="check_list('surveys'); set_if_any('surveys', 'surveys_4'); set_fake('surveys', 'surveys_0');" value="1" /></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="surveys_4" id="surveys_4" onclick="check_list('surveys'); set_fake('surveys', 'surveys_0'); set_fake('surveys', 'surveys_1'); set_fake('surveys', 'surveys_2'); set_fake('surveys', 'surveys_3'); set_fake('surveys', 'surveys_5'); set_fake('surveys', 'surveys_6'); set_fake('surveys', 'surveys_7');" value="1" /></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="surveys_5" id="surveys_5" onclick="check_list('surveys'); set_if_any('surveys', 'surveys_4')" value="1" /></td>
                                                        <td align="center" style="display: none">&nbsp;</td>
                                                        <td runat="server" id="surveyApprove" align="center">
                                                            <input runat="server" type="checkbox" name="surveys_6" id="surveys_6" onclick="grantFullAccessFor(this, 'surveys');" value="1" /></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="checkbox_col">
                                                            <input runat="server" type="checkbox" name="users" id="users" onclick="switch_list('users')" value="1" class="linecheckbox" /></td>
                                                        <td>
                                                            <label class="label_only" for="users"><%=resources.LNG_USERS %></label></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="users_0" id="users_0" onclick="check_list('users');" value="1" /></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="users_1" id="users_1" onclick="check_list('users');" value="1" /></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="users_2" id="users_2" onclick="check_list('users');" value="1" /></td>
                                                        <td align="center">&nbsp;</td>
                                                        <td align="center">&nbsp;</td>
                                                        <td align="center">&nbsp;</td>
                                                        <td align="center" style="display: none">&nbsp;</td>

                                                        <td align="center" id="usersApprove" runat="server">&nbsp;</td>

                                                    </tr>
                                                    <tr>
                                                        <td class="checkbox_col">
                                                            <input runat="server" type="checkbox" name="groups" id="groups" onclick="switch_list('groups')" value="1" class="linecheckbox" /></td>
                                                        <td>
                                                            <label class="label_only" for="groups"><%=resources.LNG_GROUPS %></label></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="groups_0" id="groups_0" onclick="check_list('groups'); set_fake(this, 'groups_4');" value="1" /></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="groups_1" id="groups_1" onclick="check_list('groups'); set_if_any('groups', 'groups_4');" value="1" /></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="groups_2" id="groups_2" onclick="check_list('groups'); set_if_any('groups', 'groups_4');" value="1" /></td>
                                                        <td align="center">&nbsp;</td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="groups_4" id="groups_4" onclick="check_list('groups');" value="1" /></td>
                                                        <td align="center">&nbsp;</td>
                                                        <td align="center" style="display: none">&nbsp;</td>
                                                        <td align="center" runat="server" id="groupsApprove">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="checkbox_col">
                                                            <input runat="server" type="checkbox" name="text_templates" id="text_templates" onclick="switch_list('text_templates'); setAvailability([$('#viewAll0')], [$('#alerts_0'), $('#surveys_0'), $('#rss_0'), $('#text_templates_0'), $('#im_0'), $('#screensavers_0'), $('#wallpapers_0'), $('#lockscreens_0')]);" class="linecheckbox" value="1" /></td>
                                                        <td>
                                                            <label class="label_only" for="templates"><%=resources.LNG_TEXT_TEMPLATES %></label></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="text_templates_0" id="text_templates_0" onclick="check_list('text_templates'); setAvailability([$('#viewAll0')], [$('#alerts_0'), $('#surveys_0'), $('#text_templates_0'), $('#im_0'), $('#screensavers_0'), $('#wallpapers_0'), $('#lockscreens_0')]);" value="1" /></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="text_templates_1" id="text_templates_1" onclick="check_list('text_templates'); set_if_any('text_templates', 'text_templates_4');" value="1" /></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="text_templates_2" id="text_templates_2" onclick="check_list('text_templates'); set_if_any('text_templates', 'text_templates_4');" value="1" /></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="text_templates_3" id="text_templates_3" onclick="check_list('text_templates'); set_if_any('text_templates', 'text_templates_4'); set_fake(this, 'alerts_0'); setAvailability([$('#emails_3'), $('#sms_3'), $('#text_to_call_3'), $('#text_to_call'), $('#sms'), $('#emails')], [$('#alerts_0'), $('#alerts_1'), $('#im_0'), $('#im_1')]); setAvailability([$('#viewAll0')], [$('#alerts_0'), $('#surveys_0'), $('#text_templates_0'), $('#im_0'), $('#screensavers_0'), $('#wallpapers_0'), $('#lockscreens_0')]);" value="1" /></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="text_templates_4" id="text_templates_4" onclick="check_list('text_templates'); removeIfDisabled(this, 'text_templates_2'); removeIfDisabled(this, 'text_templates_1'); removeIfDisabled(this, 'text_templates_0');" value="1" /></td>
                                                        <td align="center">&nbsp;</td>
                                                        <td align="center" style="display: none">&nbsp;</td>
                                                        <td align="center" runat="server" id="templatesApprove">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="checkbox_col">
                                                            <input runat="server" type="checkbox" name="statistics" id="statistics" onclick="switch_list('statistics')" value="1" class="linecheckbox" /></td>
                                                        <td>
                                                            <label class="label_only" for="statistics"><%=resources.LNG_STATISTICS %></label></td>
                                                        <td align="center">&nbsp;</td>
                                                        <td align="center">&nbsp;</td>
                                                        <td align="center">&nbsp;</td>
                                                        <td align="center">&nbsp;</td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="statistics_4" id="statistics_4" onclick="check_list('statistics')" value="1" /></td>
                                                        <td align="center">&nbsp;</td>
                                                        <td align="center" style="display: none">&nbsp;</td>

                                                        <td runat="server" id="statApprove" align="center">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="checkbox_col">
                                                            <input runat="server" type="checkbox" name="ip_groups" id="ip_groups" onclick="switch_list('ip_groups')" value="1" class="linecheckbox" /></td>
                                                        <td>
                                                            <label class="label_only" for="ip_groups">
                                                                <%=resources.LNG_IP_GROUPS %>
                                                            </label></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="ip_groups_0" id="ip_groups_0" onclick="check_list('ip_groups');" value="1" /></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="ip_groups_1" id="ip_groups_1" onclick="check_list('ip_groups'); set_if_any('ip_groups', 'ip_groups_4');" value="1" /></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="ip_groups_2" id="ip_groups_2" onclick="check_list('ip_groups'); set_if_any('ip_groups', 'ip_groups_4');" value="1" /></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="ip_groups_3" id="ip_groups_3" onclick="check_list('ip_groups'); set_if_any('ip_groups', 'ip_groups_4');" value="1" /></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="ip_groups_4" id="ip_groups_4" onclick="check_list('ip_groups'); removeIfDisabled(this, 'ip_groups_1'); removeIfDisabled(this, 'ip_groups_2'); removeIfDisabled(this, 'ip_groups_3');" value="1" /></td>
                                                        <td align="center">&nbsp;</td>
                                                        <td align="center" style="display: none">&nbsp;</td>
                                                        <td align="center" runat="server" id="ipGroupsApprove">&nbsp;</td>
                                                    </tr>
                                                    <tr runat="server" id="rssPolicyRow">
                                                        <td class="checkbox_col">
                                                            <input runat="server" type="checkbox" name="rss" id="rss" onclick="switch_list('rss'); setAvailability([$('#emails_3'), $('#sms_3'), $('#text_to_call_3'), $('#text_to_call'), $('#sms'), $('#emails')], [$('#alerts_0'), $('#alerts_1'), $('#rss_0'), $('#rss_1'), $('#im_0'), $('#im_1')]); setAvailability([$('#viewAll0')], [$('#alerts_0'), $('#surveys_0'), $('#rss_0'), $('#text_templates_0'), $('#im_0'), $('#screensavers_0'), $('#wallpapers_0')]);" value="1" class="linecheckbox" /></td>
                                                        <td>
                                                            <label class="label_only" for="rss"><%=resources.LNG_RSS %></label></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="rss_0" id="rss_0" onclick="check_list('rss'); set_if_any('rss', 'rss_3'); set_if_any('rss', 'rss_4'); setAvailability([$('#emails_3'), $('#sms_3'), $('#text_to_call_3'), $('#text_to_call'), $('#sms'), $('#emails')], [$('#alerts_0'), $('#alerts_1'), $('#rss_0'), $('#rss_1'), $('#im_0'), $('#im_1')]); setAvailability([$('#viewAll0')], [$('#alerts_0'), $('#surveys_0'), $('#rss_0'), $('#text_templates_0'), $('#im_0'), $('#screensavers_0'), $('#wallpapers_0'), $('#lockscreens_0')]);" value="1" /></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="rss_1" id="rss_1" onclick="check_list('rss'); set_if_any('rss', 'rss_4'); setAvailability([$('#emails_3'), $('#sms_3'), $('#text_to_call_3'), $('#text_to_call'), $('#sms'), $('#emails')], [$('#alerts_0'), $('#alerts_1'), $('#rss_0'), $('#rss_1'), $('#im_0'), $('#im_1')])" value="1" /></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="rss_2" id="rss_2" onclick="check_list('rss'); set_if_any('rss', 'rss_4');" value="1" /></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="rss_3" id="rss_3" onclick="check_list('rss'); set_if_any('rss', 'rss_4'); set_fake('rss', 'rss_0');" value="1" /></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="rss_4" id="rss_4" onclick="check_list('rss'); set_fake('rss', 'rss_0'); set_fake('rss', 'rss_1'); set_fake('rss', 'rss_2'); set_fake('rss', 'rss_3'); set_fake('rss', 'rss_5'); set_fake('rss', 'rss_6'); set_fake('rss', 'rss_7');" value="1" /></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="rss_5" id="rss_5" onclick="check_list('rss'); set_if_any('rss', 'rss_4')" value="1" /></td>
                                                        <td align="center" style="display: none">
                                                            <input runat="server" type="checkbox" name="rss_6" id="rss_6" onclick="check_list('rss')" value="1" /></td>
                                                        <td align="center" runat="server" id="rssApprove"></td>
                                                    </tr>
                                                    <tr runat="server" id="screensaverPolicyRow">
                                                        <td class="checkbox_col">
                                                            <input runat="server" type="checkbox" name="screensavers" id="screensavers" onclick="switch_list('screensavers'); setAvailability([$('#viewAll0')], [$('#alerts_0'), $('#surveys_0'), $('#text_templates_0'), $('#im_0'), $('#screensavers_0'), $('#wallpapers_0')]);" value="1" class="linecheckbox" /></td>
                                                        <td>
                                                            <label class="label_only" for="screensavers"><%=resources.LNG_SCREENSAVERS %></label></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="screensavers_0" id="screensavers_0" onclick="check_list('screensavers'); set_if_any('screensavers', 'screensavers_3'); set_if_any('screensavers', 'screensavers_4'); setAvailability([$('#viewAll0')], [$('#alerts_0'), $('#surveys_0'), $('#text_templates_0'), $('#im_0'), $('#screensavers_0'), $('#wallpapers_0'), $('#lockscreens_0')]);" value="1" /></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="screensavers_1" id="screensavers_1" onclick="check_list('screensavers'); set_if_any('screensavers', 'screensavers_4');" value="1" /></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="screensavers_2" id="screensavers_2" onclick="check_list('screensavers'); set_if_any('screensavers', 'screensavers_4'); " value="1" /></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="screensavers_3" id="screensavers_3" onclick="check_list('screensavers'); set_if_any('screensavers', 'screensavers_4'); removeIfDisabled(this, 'screensavers_0');" value="1" /></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="screensavers_4" id="screensavers_4" onclick="check_list('screensavers'); removeIfDisabled(this, 'screensavers_0'); removeIfDisabled(this, 'screensavers_1'); removeIfDisabled(this, 'screensavers_2'); removeIfDisabled(this, 'screensavers_3'); removeIfDisabled(this, 'screensavers_5'); removeIfDisabled(this, 'screensavers_6');" value="1" /></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="screensavers_5" id="screensavers_5" onclick="check_list('screensavers'); set_if_any('screensavers', 'screensavers_4');" value="1" /></td>
                                                        <td align="center" style="display: none">&nbsp;</td>
                                                        <td align="center" runat="server" id="screensaverApprove">
                                                            <input runat="server" type="checkbox" name="screensavers_6" id="screensavers_6" onclick="grantFullAccessFor(this, 'screensavers');" value="1" /></td>
                                                    </tr>
                                                    <tr runat="server" id="wallpaperPolicyRow">
                                                        <td class="checkbox_col">
                                                            <input runat="server" type="checkbox" name="wallpapers" id="wallpapers" onclick="switch_list('wallpapers'); setAvailability([$('#viewAll0')], [$('#alerts_0'), $('#surveys_0'), $('#text_templates_0'), $('#im_0'), $('#screensavers_0'), $('#wallpapers_0')]);" value="1" class="linecheckbox" /></td>
                                                        <td>
                                                            <label class="label_only" for="wallpapers"><%=resources.LNG_WALLPAPERS %></label></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="wallpapers_0" id="wallpapers_0" onclick="check_list('wallpapers'); set_if_any('wallpapers', 'wallpapers_3'); set_if_any('wallpapers', 'wallpapers_4'); setAvailability([$('#viewAll0')], [$('#alerts_0'), $('#surveys_0'), $('#text_templates_0'), $('#im_0'), $('#screensavers_0'), $('#wallpapers_0')]);" value="1" /></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="wallpapers_1" id="wallpapers_1" onclick="check_list('wallpapers'); set_if_any('wallpapers', 'wallpapers_4');" value="1" /></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="wallpapers_2" id="wallpapers_2" onclick="check_list('wallpapers'); set_if_any('wallpapers', 'wallpapers_4');" value="1" /></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="wallpapers_3" id="wallpapers_3" onclick="check_list('wallpapers'); set_if_any('wallpapers', 'wallpapers_4'); removeIfDisabled(this, 'wallpapers_0');" value="1" /></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="wallpapers_4" id="wallpapers_4" onclick="check_list('wallpapers'); removeIfDisabled(this, 'wallpapers_0'); removeIfDisabled(this, 'wallpapers_1'); removeIfDisabled(this, 'wallpapers_2'); removeIfDisabled(this, 'wallpapers_3'); removeIfDisabled(this, 'wallpapers_5'); removeIfDisabled(this, 'wallpapers_6');" value="1" /></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="wallpapers_5" id="wallpapers_5" onclick="check_list('wallpapers'); set_if_any('wallpapers', 'wallpapers_4');" value="1" /></td>
                                                        <td align="center" style="display: none">&nbsp;</td>
                                                        <td align="center" runat="server" id="wallpaperApprove">
                                                            <input runat="server" type="checkbox" name="wallpapers_6" id="wallpapers_6" onclick="grantFullAccessFor(this, 'wallpapers');" value="1" /></td>
                                                    </tr>
                                                    


                                                    <tr runat="server" id="lockscreenPolicyRow">
                                                        <td class="checkbox_col">
                                                            <input runat="server" type="checkbox" name="lockscreens" id="lockscreens" onclick="switch_list('lockscreens'); setAvailability([$('#viewAll0')], [$('#alerts_0'), $('#surveys_0'), $('#rss_0'), $('#text_templates_0'), $('#im_0'), $('#screensavers_0'), $('#wallpapers_0'), $('#lockscreens_0')]);" value="1" class="linecheckbox" /></td>
                                                        <td>
                                                            <label class="label_only" for="lockscreens"><%=resources.LNG_LOCKSCREENS %></label></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="lockscreens_0" id="lockscreens_0" onclick="check_list('lockscreens'); set_if_any('lockscreens', 'lockscreens_3'); set_if_any('lockscreens', 'lockscreens_4'); setAvailability([$('#viewAll0')], [$('#alerts_0'), $('#surveys_0'), $('#rss_0'), $('#text_templates_0'), $('#im_0'), $('#screensavers_0'), $('#lockscreens_0')]);" value="1" /></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="lockscreens_1" id="lockscreens_1" onclick="check_list('lockscreens'); set_if_any('lockscreens', 'lockscreens_4');" value="1" /></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="lockscreens_2" id="lockscreens_2" onclick="check_list('lockscreens'); set_if_any('lockscreens', 'lockscreens_4');" value="1" /></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="lockscreens_3" id="lockscreens_3" onclick="check_list('lockscreens'); set_if_any('lockscreens', 'lockscreens_4'); removeIfDisabled(this, 'lockscreens_0');" value="1" /></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="lockscreens_4" id="lockscreens_4" onclick="check_list('lockscreens'); removeIfDisabled(this, 'lockscreens_0'); removeIfDisabled(this, 'lockscreens_1'); removeIfDisabled(this, 'lockscreens_2'); removeIfDisabled(this, 'lockscreens_3'); removeIfDisabled(this, 'lockscreens_5'); removeIfDisabled(this, 'lockscreens_6');" value="1" /></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="lockscreens_5" id="lockscreens_5" onclick="check_list('lockscreens'); set_if_any('lockscreens', 'lockscreens_4');" value="1" /></td>
                                                        <td align="center" style="display: none">&nbsp;</td>
                                                        <td align="center" runat="server" id="lockscreenApprove">
                                                            <input runat="server" type="checkbox" name="lockscreens_6" id="lockscreens_6" onclick="grantFullAccessFor(this, 'lockscreens');" value="1" />
                                                        </td>
                                                    </tr>


                                                    <tr runat="server" id="emergencyPolicyRow">
                                                        <td class="checkbox_col">
                                                            <input runat="server" type="checkbox" name="im" id="im" onclick="switch_list('im'); setAvailability([$('#emails_3'), $('#sms_3'), $('#sms'), $('#emails'), $('#text_to_call_3'), $('#text_to_call')], [$('#alerts_0'), $('#alerts_1'), $('#im_0'), $('#im_1')]); setAvailability([$('#viewAll0')], [$('#alerts_0'), $('#surveys_0'), $('#text_templates_0'), $('#im_0'), $('#screensavers_0'), $('#wallpapers_0'), $('#lockscreens_0')]);" value="1" class="linecheckbox" /></td>
                                                        <td>
                                                            <label class="label_only" for="im"><%=resources.LNG_INSTANT_MESSAGES %></label></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="im_0" id="im_0" onclick="check_list('im'); setAvailability([$('#emails_3'), $('#sms_3'), $('#sms'), $('#emails'), $('#text_to_call_3'), $('#text_to_call')], [$('#alerts_0'), $('#alerts_1'), $('#im_0'), $('#im_1')]); setAvailability([$('#viewAll0')], [$('#alerts_0'), $('#surveys_0'), $('#text_templates_0'), $('#im_0'), $('#screensavers_0'), $('#wallpapers_0'), $('#lockscreens_0')]);" value="1" /></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="im_1" id="im_1" onclick="check_list('im'); set_if_any('im', 'im_4'); setAvailability([$('#emails_3'), $('#sms_3'), $('#sms'), $('#emails'), $('#text_to_call_3')], [$('#alerts_0'), $('#alerts_1'), $('#im_0'), $('#im_1')])" value="1" /></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="im_2" id="im_2" onclick="check_list('im'); set_if_any('im', 'im_4');" value="1" /></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="im_3" id="im_3" onclick="check_list('im'); set_if_any('im', 'im_4');" value="1" /></td>
                                                        <td align="centext_ter">
                                                            <input runat="server" type="checkbox" name="im_4" id="im_4" onclick="check_list('im'); removeIfDisabled(this, 'im_1'); removeIfDisabled(this, 'im_2'); removeIfDisabled(this, 'im_3');" value="1" /></td>
                                                        <td align="center">&nbsp;</td>
                                                        <td align="center" style="display: none">&nbsp;</td>

                                                        <td runat="server" id="emergencyApprove" align="center"></td>

                                                    </tr>

                                                    <tr runat="server" id="colorCodeRow">
                                                        <td class="checkbox_col">
                                                            <input runat="server" type="checkbox" name="cc" id="cc" onclick="switch_list('cc')" value="1" class="linecheckbox" /></td>
                                                        <td>
                                                            <label class="label_only" for="cc"><%=resources.LNG_COLOR_CODES %></label></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="cc_0" id="cc_0" onclick="check_list('cc');" value="1" /></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="cc_1" id="cc_1" onclick="check_list('cc'); set_if_any('cc', 'cc_4')" value="1" /></td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="cc_2" id="cc_2" onclick="check_list('cc'); set_if_any('cc', 'cc_4')" value="1" /></td>
                                                        <td align="center">&nbsp;</td>
                                                        <td align="center">
                                                            <input runat="server" type="checkbox" name="cc_4" id="cc_4" onclick="check_list('cc'); removeIfDisabled(this, 'cc_1'); removeIfDisabled(this, 'cc_2'); removeIfDisabled(this, 'cc_3');" value="1" /></td>
                                                        <td align="center">&nbsp;</td>
                                                        <td align="center" style="display: none">&nbsp;</td>
                                                        <td align="center" runat="server" id="colorCodeApprove">&nbsp;</td>
                                                    </tr>
                                                </table>
                                            </div>

                                            <div id="policy_recipientsTab">
                                                <div>
                                                    <input type="radio" runat="server" id="canSendContentToAllUsers" name="all_recipients" value="1" onclick="all_recipients_click(true)" checked />
                                                    <label for="canSendContentToAllUsers"><%=resources.LNG_CAN_SEND_NOTIFICATIONS_TO_ALL %></label>
                                                    <br />
                                                    <br />
                                                    <input type="radio" runat="server" id="canSendContentToChosenUsers" name="all_recipients" value="0" onclick="all_recipients_click(false)" />
                                                    <label for="canSendContentToChosenUsers"><%=resources.LNG_CAN_SEND_NOTIFICATIONS_TO_RECIPIENTS %></label>
                                                    <br />
                                                    <br />
                                                </div>
                                                <div name="addedUsers" runat="server" id="addedUsers" style="display: none">
                                                    <da:recipientsselectiontable runat="server" id="recipientsSelector" userslinktable="policy_user" userslinkcollumn="user_id" groupslinktable="policy_group" groupslinkcollumn="group_id"
                                                        ouslinktable="policy_ou" ouslinkcollumn="ou_id" linkcollumn="policy_id" computerslinktable="policy_computer" computerslinkcollumn="comp_id">
                                                    </da:recipientsselectiontable>
                                                </div>
                                            </div>

                                            <div id="policy_viewersTab">
                                                <div>
                                                    <input type="radio" runat="server" id="viewAll0" name="view_all" value="0" disabled="True" /><label for="viewAll0"><%=resources.LNG_EDITOR_CAN_VIEW_ONLY_SELF_CREATED_NOTIFICATIONS %></label><br />
                                                    <br />
                                                    <input type="radio" runat="server" id="viewAll1" name="view_all" value="1" checked /><label for="viewAll1"><%=resources.LNG_EDITOR_CAN_VIEW_ALL_NOTIFICATIONS %></label><br />
                                                    <br />
                                                </div>
                                            </div>

                                            <div id="policyTemplatesGroupsTabContent" runat="server">
                                                <div>
                                                    <input type="radio" runat="server" id="templatesAll1" name="all_templates" value="1" onclick="all_tgroups_click(true)" checked /><label for="allRecipients1" />
                                                    <label for="templatesAll1"><%=resources.LNG_LIST_OF_TGROUPS_CAN_SEND_ALL %></label>
                                                    <br />
                                                    <br />
                                                    <input type="radio" runat="server" id="templatesAll0" name="all_templates" value="0" onclick="all_tgroups_click(false)" />
                                                    <label for="templatesAll0"><%=resources.LNG_LIST_OF_TGROUPS_CAN_SEND_SELECTED %></label>
                                                    <br />
                                                    <br />
                                                </div>
                                                <div runat="server" id="tgroups" style="display: none">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <br />
                                                                <iframe frameborder="0" id="tgroups_list" name="tgroups_list" class="list_iframe" src="TemplateGroups.aspx"></iframe>
                                                            </td>
                                                            <td style="text-align: center;">
                                                                <%=resources.LNG_ADD %><br>
                                                                <a onclick="list_add_tgroup('tgroups_list','selectedTgroups');">
                                                                    <img src="images/list_add.png" border="0" /></a><br />
                                                                <a onclick="list_remove_tgroup('selectedTgroups');">
                                                                    <img src="images/list_remove.png" border="0" /></a><br />
                                                                <%=resources.LNG_REMOVE %>
                                                            </td>
                                                            <td>
                                                                <b><%=resources.LNG_TGROUPS %></b>
                                                                <br />
                                                                <br />
                                                                <select multiple runat="server" id="selectedTgroups" name="list2" class="list_select">
                                                                </select>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        
                                            <div id="policy_SkinsTab" class="policy_Tab policy_SkinsTab">
                                                <div>
                                                    <asp:RadioButtonList runat="server" id ="policy_SkinsAccess_RadioButtonList" />
                                                </div>
                                                <div runat="server" id="policy_choosingSkinsList" class="policy_choosingSkinsList" >
                                                    <asp:CheckBoxList runat="server" id="policy_choosingSkinsList_checkBoxList" class="policy_choosingSkinsList_checkBoxList" />
                                                </div>
                                            </div>
                                        
                                            <div id="policy_ContentSettingsTab" class="policy_Tab policy_ContentSettingsTab">
                                                <div>
                                                    <asp:RadioButtonList runat="server" id="policy_ContentSettings_RadioButtonList" />
                                                </div>
                                                <div runat="server" id="policy_choosingContentSettingsList" class="policy_choosingContentSettingsList" >
                                                    <asp:CheckBoxList runat="server" id="policy_choosingContentSettingsList_checkBoxList"/>
                                                </div>
                                            </div>
                                        </div>
                                        <br />
                                        <div class="right" style="float: right; padding-bottom: 10px; margin-right: 5px;">
                                            <a class="cancel_button" href="PolicyList.aspx"><%=resources.LNG_CANCEL %></a>

                                            <asp:LinkButton class="save_button" runat="server" ID="saveButton" />
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>


        </div>

        <asp:UpdatePanel runat="server" ID="updatePanel">
            <ContentTemplate>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
