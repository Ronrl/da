﻿using System;
using System.Linq;
using System.Reflection;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using DeskAlerts.ApplicationCore.Enums;
using DeskAlerts.ApplicationCore.Extensions.Enums;
using DeskAlertsDotNet.Parameters;

namespace DeskAlertsDotNet.Pages
{
    using Resources;

    public partial class DefaultSettings : DeskAlertsBasePage
    {
        //use curUser to access to current user of DA control panel      
        //use dbMgr to access to database

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (Config.Data.IsDemo && curUser.Name != "admin")
            {
                cantSaveDemoSetingsDefault.Visible = false;
            }
            if (!Config.Data.IsOptionEnabled("RESTAPI"))
            {
                ConfAPISecret.Visible = false;
            }
            
            if (!IsPostBack)
            {
                lifetimeFactor.Items.Add(new ListItem(resources.LNG_MINUTES, "1"));
                lifetimeFactor.Items.Add(new ListItem(resources.LNG_HOURS, "60"));
                lifetimeFactor.Items.Add(new ListItem(resources.LNG_DAYS, "1440"));

                rssLifetimeFactor.Items.Add(new ListItem(resources.LNG_MINUTES, "1"));
                rssLifetimeFactor.Items.Add(new ListItem(resources.LNG_HOURS, "60"));
                rssLifetimeFactor.Items.Add(new ListItem(resources.LNG_DAYS, "1440"));
            }
            //Add your main code here

            fullscreenDivWindowButton.Visible = Config.Data.HasModule(Modules.FULLSCREEN);
            fullscreenDivRadiobutton.Visible = Config.Data.HasModule(Modules.FULLSCREEN);
            // nonFullscreenDiv.Visible = !Config.Data.HasModule(Modules.FULLSCREEN);
            rowBlog.Visible = Config.Data.HasModule(Modules.BLOG);
            rowEmail.Visible = Config.Data.HasModule(Modules.EMAIL);
            rowTextToCall.Visible = Config.Data.HasModule(Modules.TEXTTOCALL);
            rowTweet.Visible = Config.Data.HasModule(Modules.TWITTER);          
            LoadSettings();
        }

        void LoadSettings()
        {
            foreach (string key in Settings.Content.Keys)
            {
                // = GetType().GetField(key, BindingFlags.Instance | BindingFlags.NonPublic);

                FieldInfo[] fieldInfo = GetType()
                    .GetFields(BindingFlags.Instance | BindingFlags.NonPublic)
                    .ToArray()
                    .Where(f => f.Name.StartsWith(key)).ToArray();
                //.First();
                if (fieldInfo.Length == 0)
                    continue;

                foreach (FieldInfo info in fieldInfo)
                {



                    object control =
                    info.GetValue(this);



                    if (control is HtmlInputCheckBox)
                    {
                        ((HtmlInputCheckBox)control).Checked = Settings.Content[key].Equals("1");
                    }
                    else if (control is HtmlInputGenericControl)
                    {
                        ((HtmlInputGenericControl)control).Value = Settings.Content[key];
                    }
                    else if (control is DropDownList)
                    {
                        ((DropDownList)control).SelectedValue = Settings.Content[key];
                    }
                    else if (control is HtmlInputText)
                    {
                        ((HtmlInputText)control).Value = Settings.Content[key];
                    }
                    else if (control is HtmlInputRadioButton)
                    {

                        HtmlInputRadioButton radioButton = control as HtmlInputRadioButton;

                        string dbValue = Settings.Content[key];

                        radioButton.Checked = radioButton.Value.Equals(dbValue);
                    }
                    else if (control is HtmlInputHidden)
                    {
                        HtmlInputHidden htmlInputHidden = control as HtmlInputHidden;
                        htmlInputHidden.Value = Settings.Content[key];
                    }
                }
            }

            FillLifeTimeForm(lifetime, lifetimeFactor, Convert.ToInt32(ConfMessageExpire.Value));
            FillLifeTimeForm(rssLifetime, rssLifetimeFactor, Convert.ToInt32(ConfRSSMessageExpire.Value));
        }

        void FillLifeTimeForm(HtmlInputText lifetiveValueControl, HtmlSelect lifeTimeFactorControl, int valueInMinutes)
        {
            foreach (ListItem item in lifeTimeFactorControl.Items)
            {
                item.Selected = false;
            }
            for (int i = 2; i >= 0; i--)
            {
                ListItem listItem = lifeTimeFactorControl.Items[i];
                int itemValue = Convert.ToInt32(listItem.Value);

                int lifeTimeValue = valueInMinutes / itemValue;
                if (valueInMinutes % itemValue == 0)
                {
                    listItem.Selected = true;
                    lifetiveValueControl.Value = lifeTimeValue.ToString();
                    break;
                }
            }
        }
    }
}