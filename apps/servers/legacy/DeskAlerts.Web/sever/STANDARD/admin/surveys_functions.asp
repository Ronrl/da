<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<% Response.ContentType = "text/javascript" %>
function delete_answer(){
	this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode);
}

function delete_question(){
	var this_question_number = parseInt(document.getElementById("question_number").value);
	survey.removeElement(this_question_number);
	document.getElementById("question_number").value=this_question_number-1;
	var string = JSON.stringify(survey);
	document.getElementById("survey_data").value=string;
	document.getElementById("type").value="prev";
	document.getElementById("frm").submit();
}

function check_values(){
	//survey name

	if(document.getElementById("sur_name").value.length < 1){

		return false;
	}
	//question
	if(document.getElementById("question").value.length < 1){

		return false;
	}

	//answers
//	var answersForm = document.getElementsByName("answer"); - not working in IE6 and IE7 for dynamicly created inputs
	var answersForm = document.getElementsByTagName("input");
	for(var i=0; i<answersForm.length; i++){
		if(answersForm[i].name=="answer"){
			if(answersForm[i].value.length < 1){ 	
				return false;
			}
		}
	}

	return true;
}

function check_valid_dates(survey)
{
	if(survey.schedule)
	{		
		var fromDate = survey.start_date;
		var toDate = survey.end_date;
	
	
		if (!fromDate)
		{
			alert("<%=LNG_PLEASE_ENTER_START_DATE %>!");
			return false;
		}
		if (!toDate)
		{
			alert("<%=LNG_PLEASE_ENTER_END_DATE %>!");
			return false;
		}
		
		if (!isDate(fromDate,uiFormat))
		{
			alert("<%=LNG_PLEASE_ENTER_CORRECT_START_DATE %>!");
			return false;
		}
		else
		{
			if (fromDate!="")
			{
				fromDate = new Date(getDateFromFormat(fromDate,uiFormat));
				survey.start_date = formatDate(fromDate,saveDbFormat);
			}
		}

		if (!isDate(toDate,uiFormat))
		{
			alert("<%=LNG_PLEASE_ENTER_END_DATE %>!");
			return false;
		}
		else
		{
			if (toDate!="")
			{
			
				toDate = new Date(getDateFromFormat(toDate,uiFormat))
				survey.end_date = formatDate(toDate,saveDbFormat);
			}
		}
		
		if (fromDate < serverDate && toDate < serverDate)
		{
			alert("<%=LNG_DATE_IN_THE_PAST %>")
			return false;
		}
		
		if (fromDate >= toDate)
		{
			alert("<%=LNG_CORRECT_DATETIME_DESC %>!")
			return false;
		}
	}
	return true;
}

function my_survey_submit(type){
        
		if(check_values()){
		    
		    
			survey.name=document.getElementById("sur_name").value;
            survey.campaign = document.getElementById("campaign_select").value;
			survey.template_id=document.getElementById("template").value;

			var schedule = document.getElementById("schedule");
			survey.schedule		= schedule.type.toLowerCase() != "checkbox" || schedule.checked ? schedule.value : 0;
			survey.start_date	= document.getElementById("start_date").value;
			survey.end_date		= document.getElementById("end_date").value;

			if(survey.schedule == 1)
			{
				if(survey.start_date)
				{
					if(survey.end_date)
					{
						if(!check_valid_dates(survey)) return;
					}
					else
					{
						alert("<%=LNG_PLEASE_ENTER_END_DATE %>.");
						return;
					}
				}
				else
				{
						alert("<%=LNG_PLEASE_ENTER_START_DATE %>.");
						return;
				}
			}

			var this_question_number = parseInt(document.getElementById("question_number").value);
			var question;
			if(survey.elements[this_question_number]){
				question=survey.elements[this_question_number];
			}
			else{
				question = new Question();
				survey.add_elem(question);
			}
			add_question_methods(question);
			question.name=document.getElementById("question").value;
			question.type=document.getElementById("question_type").value;
			question.removeAllElements();
	//	var answersForm = document.getElementsByName("answer"); - not working in IE6/IE7/IE8 for dynamicly created inputs
			var answersForm = document.getElementsByTagName("input");
			var answer; 
			
			for(var i=0; i<answersForm.length; i++){
					if(answersForm[i].name=="answer"){
						answer = new Answer();
						question.add_elem(answer);
						answer.name=answersForm[i].value; 
					}						
					if(answersForm[i].name=="correct"){
						answer.correct = answersForm[i].checked;
						
					}
			}

			document.getElementById("type").value=type;

			if(type=="add"){
				document.getElementById("question_number").value=this_question_number+1;
				var new_question = new Question;
						survey.add_elem_after(new_question,this_question_number);
			}
			if(type=="prev"){
				document.getElementById("question_number").value=this_question_number-1;
			}
			if(type=="next"){
				if((this_question_number+1) < survey.elements.length){
					document.getElementById("question_number").value=this_question_number+1;
				}
			}
			if(type=="save_next"){
				document.getElementById("type").value="select_users";
			}
			if(type=="select_users"){
				document.getElementById("type").value="select_users";
			}
			var string = JSON.stringify(survey);
			document.getElementById("survey_data").value=string;
			document.getElementById("frm").submit();
		}
		else{
			alert("<%=LNG_PLEASE_FILL_ALL_EMPTY_FIELDS %>.");
		}
}

function insert_new_answer(answer_value, correct){
	//insert new input for answer
	var questionType=document.getElementById('question_type').value;
	var questionSType=document.getElementsByName('stype')[0].value;
	var question_row = (document.getElementById('question_tr').rowIndex+1);

	var tbl = document.getElementById('survey_table');
	var lastRow = tbl.rows.length;
	var row = tbl.insertRow(lastRow);
	var cellLeft = row.insertCell(0);
	var textNode = document.createTextNode("<%=LNG_OPTION %>:");
	cellLeft.appendChild(textNode);

	var cellMiddle = row.insertCell(1);
	var el = document.createElement('input');
	el.type = 'text';
	el.name = 'answer';
	el.size = 35;
	el.value = answer_value;
	el.correct = correct;
	cellMiddle.appendChild(el);
	el.focus();

	var cellRight = row.insertCell(2);
	if(questionType=="C"){
		var el_span = document.createElement('span');
		el_span.style.font="11ppt";
		el_span.style.color="gray";
		if(answer_value == "Yes"){
			el_span.innerHTML = "<%=LNG_THIS_ANSWER_WILL_PROCEED_THE_SURVEY %>";
		}
		else{
			el_span.innerHTML = "<%=LNG_THIS_ANSWER_WILL_FINISH_THE_SURVEY %>";
		}
		cellRight.appendChild(el_span);
	}
	else
	{
		if(questionSType=="2")
		{	
			if((correct==1)||(correct==true)){

					cellRight.innerHTML = "<input type=\"checkbox\" checked name=\"correct\"/>&nbsp;<%=LNG_THIS_IF_CORRECT_OPTION %>&nbsp;&nbsp;";
			}
			else{
					cellRight.innerHTML = "<input type=\"checkbox\" name=\"correct\"/>&nbsp;<%=LNG_THIS_IF_CORRECT_OPTION %>&nbsp;&nbsp;";
			}
		}
		if(lastRow>question_row+1)
		{
			var el = document.createElement('img');
			el.src = 'images/action_icons/delete.gif';
			el.name = 'answer_delete';
			el.border="0";
			el.onclick=delete_answer;
			cellRight.appendChild(el);
		}
	}
}

function fill_the_form(){
	surNameElement = document.getElementById("sur_name");
	questionElement = document.getElementById("question");
	if (!(surNameElement && questionElement)) return;
	
	surNameElement.value=survey.name;
	var this_question_number = parseInt(document.getElementById("question_number").value);
	if(survey.elements[this_question_number]){
		var question=survey.elements[this_question_number];
		
		add_question_methods(question);
		
		questionElement.value=question.name;
		
		var qt = document.getElementById("question_type");
		for(var i=0; i < qt.options.length; i++)
		{
			if(qt.options[i].value==question.type)
			{
				qt.options[i].selected=true;
				break;
			}
		}

		changeQuestionType();
		
		var answersTagForm = document.getElementsByTagName("input");
		var answersForm = []; 
		var correctForm = []; 
		for(var k=0; k < answersTagForm.length; k++){
			if(answersTagForm[k].name=="answer"){
				answersForm.push(answersTagForm[k]);
			}
			if(answersTagForm[k].name=="correct"){
				correctForm.push(answersTagForm[k]);
			}
		}
		
		for(var i=0; i<question.elements.length; i++){
			if(i<answersForm.length){
				answersForm[i].value=question.elements[i].name;
				if(correctForm.length!=0) 
						{
						if(question.elements[i].correct==="1") correctForm[i].checked = true;
						else if(question.elements[i].correct==="0") correctForm[i].checked = false; 
						else correctForm[i].checked = question.elements[i].correct;
						}
			}
			else{
				insert_new_answer(question.elements[i].name, question.elements[i].correct);
			}
		}
	}
	
	
}

function changeQuestionType()
{
	//clear all answer rows
	var tbl = document.getElementById('survey_table');
	var lastRow = tbl.rows.length;
	var question_row = (document.getElementById('question_tr').rowIndex+1);
	for(; lastRow>question_row; ){
		tbl.deleteRow(question_row);
		lastRow = tbl.rows.length;
	}
	var insert_answer_div = document.getElementById('insert_answer_div');
	var input_answer_desc_div = document.getElementById('input_answer_desc_div');
	
	var questionType=document.getElementById('question_type').value;
	switch(questionType)
	{
	case "M":
	case "N":
	case "T":
		insert_new_answer("",false);
		insert_new_answer("",false);
		insert_answer_div.style.display = "";
		input_answer_desc_div.style.display = "none";
	break;
	case "C":
		insert_new_answer("Yes",false);
		insert_new_answer("No",false);
		document.getElementById("question").value = "<%=LNG_PROCEED_WITH_THE_REST_OF_THE_QUESTIONS %>";
		insert_answer_div.style.display = "none";
		input_answer_desc_div.style.display = "none";
	break;
	case "I":
		insert_answer_div.style.display = "none";
		input_answer_desc_div.style.display = "";
	break;
	}	

}

<!-- #include file="db_conn_close.asp" -->