﻿using DeskAlertsDotNet.Parameters;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;
using DeskAlertsDotNet.Utils.Factories;
using Resources;
using System.Web.UI.HtmlControls;
using DeskAlerts.Server.Utils.Helpers;
using DeskAlertsDotNet.Managers;
using DeskAlertsDotNet.Utils.Consts;

namespace DeskAlertsDotNet.Pages
{
    public partial class PublisherList : DeskAlertsBaseListPage
    {
        protected override HtmlInputText SearchControl => searchTermBox;
        private readonly string[] _usersAllowedToEditPublishersOnDemo = { "admin", "evgen", "mila", "yuri", "alex" };

        public string PublisherOperationError;
        protected bool ShowConfirmDialog;

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            headerTitle.Text = resources.LNG_EDITORS;
            searchButton.Text = searchButton.Text.Replace("{Search}", resources.LNG_SEARCH);
            pubNameCell.Text = GetSortLink(resources.LNG_NAME, "name", Request["sortBy"]);
            pubPolicyCell.Text = GetSortLink(resources.LNG_POLICY_NAME, "policy_name", Request["sortBy"]);
            publisherRegDate.Text = GetSortLink(resources.LNG_CREATION_DATE, "reg_date", Request["sortBy"]);
            publisherStatusHeader.Text = GetSortLink(resources.LNG_STATUS, "publisher_status", Request["sortBy"]);
            actionsCell.Text = resources.LNG_ACTIONS;
            upDisable.Text = resources.LNG_DISABLE;
            bottomDisable.Text = resources.LNG_DISABLE;
            upEnable.Text = resources.LNG_ENABLE;
            bottomEnable.Text = resources.LNG_ENABLE;

            upDisable.Click += OnDisablePublisherButtonClick;
            bottomDisable.Click += OnDisablePublisherButtonClick;
            upEnable.Click += OnEnablePublisherButtonClick;
            bottomEnable.Click += OnEnablePublisherButtonClick;

            Table = contentTable;

            var getUrlSearchTermBox = Request.QueryString["searchTermBox"] ?? string.Empty;
            var isSearchTermBoxValueChanged = searchTermBox.Value != getUrlSearchTermBox;

            if (isSearchTermBoxValueChanged)
            {
                Offset = 0;
            }

            var cnt = Content.Count < (Offset + Limit) ? Content.Count : Offset + Limit;

            for (var i = Offset; i < cnt; i++)
            {
                var row = Content.GetRow(i);
                var publisherHaveAGroup = GetPublisherGroupId(row.GetString("id"));
                var tableRow = new TableRow();

                var chb = GetCheckBoxForDelete(row);
                var checkBoxCell = new TableCell {HorizontalAlign = HorizontalAlign.Center};
                checkBoxCell.Controls.Add(chb);
                tableRow.Cells.Add(checkBoxCell);

                var pubNameContentCell = new TableCell {Text = row.GetString("name")};
                tableRow.Cells.Add(pubNameContentCell);

                var policyNameContentCell = new TableCell {Text = row.GetString("policy_name")};
                tableRow.Cells.Add(policyNameContentCell);

                var publisherRegDateCell = new TableCell
                {
                    Text = DateTimeConverter.ToUiDateTime(row.GetDateTime("reg_date")),
                    HorizontalAlign = HorizontalAlign.Center
                };

                tableRow.Cells.Add(publisherRegDateCell);

                var publisherStatusCell = new TableCell
                {
                    Text = string.IsNullOrEmpty(row.GetString("publisher_status"))
                        ? "Disabled"
                        : row.GetString("publisher_status"),
                    HorizontalAlign = HorizontalAlign.Center
                };

                tableRow.Cells.Add(publisherStatusCell);

                var actionsContentCell = new TableCell {HorizontalAlign = HorizontalAlign.Center};

                if (!Config.Data.IsDemo || (curUser.Id == row.GetInt("id") && curUser.Name != "demo"))
                {
                    var editButton = GetActionButton(row.GetString("id"), "images/action_icons/edit.png", "LNG_EDIT_EDITOR",
                            delegate
                            {
                                Response.Redirect("EditPublisher.aspx?id=" + row.GetString("id"), true);
                            }
                        );

                    if (!publisherHaveAGroup)
                    {
                        actionsContentCell.Controls.Add(editButton);
                    }
                }

                tableRow.Cells.Add(actionsContentCell);
                contentTable.Rows.Add(tableRow);
            }

            if (!string.IsNullOrEmpty(Request.Params["__EVENTTARGET"]) && Request.Params["__EVENTTARGET"].Equals("publisherOperationConfirmed"))
            {
                var publisherIdsForEnableList = new List<string>();

                for (var index = 0; index < Request.Params.Count; index++)
                {
                    if (Regex.IsMatch(Request.Params.GetKey(index), "chbDel_"))
                    {
                        publisherIdsForEnableList.Add(Request.Params[index]);
                    }
                }

                UserManager.Default.DisableLastOnlinePublishers(publisherIdsForEnableList.Count - ActivePublishersLeft);
                UserManager.Default.EnablePublishers(publisherIdsForEnableList);

                Response.Redirect(Request.Url.ToString(), true);
            }
        }

        private void OnDisablePublisherButtonClick(object sender, EventArgs e)
        {
            var publishersIds = GetCheckedCheckboxList();
            if (publishersIds.Count < 1)
            {
                return;
            }

            UserManager.Default.DisablePublishers(publishersIds);

            Response.Redirect(Request.Url.ToString(), true);
        }

        private void OnEnablePublisherButtonClick(object sender, EventArgs e)
        {
            var publishersIds = GetCheckedCheckboxList();
            if (publishersIds.Count < 1)
            {
                return;
            }

            if (publishersIds.Count <= ActivePublishersLeft)
            {
                UserManager.Default.EnablePublishers(publishersIds);
                Response.Redirect(Request.Url.ToString(), true);
            }
            else
            {
                var maxPublishersCount = UserManager.Default.GetMaxPublishersAmount();
                if (publishersIds.Count + ActivePublishersLeft <= maxPublishersCount)
                {
                    ShowConfirmDialog = true;
                }
                else
                {
                    PublisherOperationError = "You don't have enough publisher licenses to proceed this operation";
                }
            }
        }

        private bool GetPublisherGroupId(string publisherId)
        {
            var result = false;

            if (string.IsNullOrEmpty(publisherId))
            {
                Logger.Error($"Argument \"{nameof(publisherId)}\" is null or empty.");
                throw new ArgumentNullException(nameof(publisherId));
            }

            const string sqlQuery = @"SELECT MAX(polEditor.group_id) FROM policy_editor polEditor
                                        WHERE polEditor.editor_id = @publisherId
                                        GROUP BY polEditor.editor_id";

            try
            {
                var pubId = long.Parse(publisherId);

                var publisherIdParameter = SqlParameterFactory.Create(DbType.Int64, pubId, "publisherId");

                int? queryResult = dbMgr.GetScalarByQuery<int>(sqlQuery, publisherIdParameter);

                if (queryResult > 0)
                {
                    result = true;
                }
            }
            catch (InvalidCastException)
            {
                result = false;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                throw;
            }

            return result;
        }

        public int ActivePublishersLeft => UserManager.Default.GetActivePublishersLeftAmount();

        private List<string> GetCheckedCheckboxList()
        {
            var checkedCheckboxList = new List<string>();

            foreach (TableRow row in Table.Rows)
            {
                if (row.Cells[0].Controls[0] is CheckBox chb && chb.Checked)
                {
                    var id = chb.InputAttributes["value"];
                    if (int.TryParse(id, out var intId))
                    {
                        checkedCheckboxList.Add(chb.InputAttributes["value"]);
                    }
                    else if (chb.InputAttributes["value"] != null && chb.InputAttributes["value"].Length > 0)
                    {
                        checkedCheckboxList.Add("'" + chb.InputAttributes["value"] + "'");
                    }
                }
            }

            return checkedCheckboxList;
        }

        #region DataProperties

        protected override string DataTable => "users";

        protected override string[] Collumns => new string[] { };

        protected override string DefaultOrderCollumn => "name";

        protected override string CustomQuery
        {
            get
            {
                var query = @"WITH TempPublisherList AS 
                                (
	                                SELECT u.id,
			                                u.name AS name,
			                                u.role,
			                                p.name AS policy_name,
                                            u.reg_date,
                                            u.publisher_status as publisher_status
	                                FROM users u
			                                LEFT JOIN policy_editor pe ON u.id = pe.editor_id
			                                LEFT JOIN policy p ON p.id = pe.policy_id
	                                WHERE u.role = 'E' ";

                if (!string.IsNullOrEmpty(SearchTerm))
                {
                    query += " AND u.name LIKE '%" + SearchTerm + "%'";
                }

                query += @") SELECT t1.id, t1.name, t1.role, t1.reg_date, (SELECT t2.policy_name + ';' AS 'data()' FROM TempPublisherList t2 WHERE t1.id=t2.id ";

                query += @"FOR xml path('')) AS policy_name, t1.publisher_status
	                                FROM TempPublisherList t1
	                                GROUP BY t1.id, t1.name, t1.role, t1.reg_date, t1.publisher_status";

                if (curUser.IsModer)
                {
                    query += " AND u.role!='A'";
                }

                if (!string.IsNullOrEmpty(Request["sortBy"]))
                {
                    query += " ORDER BY " + Request["sortBy"];
                }

                return query;
            }
        }

        protected override string CustomCountQuery
        {
            get
            {
                var query = @"SELECT COUNT(DISTINCT pe.editor_id)
                                 FROM users AS u 
	                                 LEFT JOIN policy_editor AS pe ON u.id = pe.editor_id 
	                                 LEFT JOIN policy AS p ON p.id = pe.policy_id 
                                 WHERE u.role = 'E'";

                if (curUser.IsModer)
                {
                    query += " AND u.role!='A'";
                }

                if (SearchTerm.Length > 0)
                    query += " AND u.name LIKE '%" + SearchTerm + "%'";

                return query;
            }
        }

        /*
         *
         * If you want to add condition for data selection override property ConditionSqlString
         * Also you can implement your custom query by overriding properties CustomQuery and CustomCountQuery
         *
         */

        #endregion DataProperties

        #region Interface properties

        protected override IButtonControl[] DeleteButtons => new IButtonControl[] { upDelete, bottomDelete };

        protected override void OnDeleteRows(string[] ids)
        {
            if (ids != null && ids.Length != 0)
            {
                var query = "DELETE FROM policy_editor WHERE";

                foreach (var id in ids)
                {
                    if (Array.IndexOf(ids, id) == 0)
                    {
                        query += $" editor_id = {id}";
                    }
                    else
                    {
                        query += $" OR editor_id = {id}";
                    }
                }

                //log id who was deleted from "publishers"
                foreach (var remoteId in ids)
                {
                    Logger.Info($"User: \"{curUser.Name}\" was delete \"Publishers\" with id={remoteId}");
                }

                dbMgr.ExecuteQuery(query);
            }
        }

        protected override HtmlGenericControl NoRowsDiv => NoRows;

        protected override HtmlGenericControl TableDiv => mainTableDiv;

        protected override string ContentName => resources.LNG_EDITORS;

        protected override HtmlGenericControl BottomPaging => bottomRanging;

        protected override HtmlGenericControl TopPaging => topPaging;

        #endregion Interface properties
    }
}