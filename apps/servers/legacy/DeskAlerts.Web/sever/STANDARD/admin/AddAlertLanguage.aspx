﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddAlertLanguage.aspx.cs" Inherits="DeskAlertsDotNet.Pages.AddAlertLanguage" %>
<%@ Import NameSpace="DeskAlertsDotNet.Parameters" %>
<%@ Import Namespace="Resources" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="css/style9.css" rel="stylesheet" type="text/css">
    <link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
    <title></title>
    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
    <script>
        $(document).ready(function () {
            $(".add_button").button();
            $(".save_button").button();
            $(".cancel_button").button();
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
          <body style="margin:0px" class="body">
            <table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	            <tr>
	            <td>
	            <table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
                    <tr></tr>

		            <tr>
		            <td class="main_table_body" height="100%">  
                        <div style="margin:10px;">
                            <span runat="server" id="workHeader" class="work_header"><% =resources.LNG_ALERT_LANGUAGES %></span>
                            <br><br>
                            <table border="0"><tr><td>
                                <font runat="server" id="errorDescription" color="red"></font>
                                <table cellpadding=4 cellspacing=4 border=0>
                                <tr><td><%=resources.LNG_LANGUAGE_NAME %>:</td>
                                    <td>
                                        <input autocomplete="off" runat="server" name="name" id="nameField" type="text" value="" size="40" maxlength="255"/>
                                    </td>
                                </tr>
                                <tr runat="server" id="changePasswordField">
                                    <td><%=resources.LNG_LANGUAGE_CODE %>:</td>
                                    <td>
                                        <input autocomplete="off" runat="server" name="name" id="codeField" type="text" value="" size="40" maxlength="255"/>
                                    </td>
                                </tr>
                                </table>
                                <input type="hidden" runat="server" id="languageId" value="" />
                                </td></tr>
                                <tr><td align="right">
                                    <asp:LinkButton runat="server" href="AlertLanguagesSettings.aspx" id="cancelButton"  class="cancel_button" ><%=resources.LNG_CANCEL %></asp:LinkButton>
                                    <asp:LinkButton runat="server" id="acceptButton"  class="save_button"  ></asp:LinkButton>
                                </td></tr>
                            </table>

                            <br />
                            <br />
                        <asp:Table style="padding-left: 0px;margin-left: 10px;margin-right: 10px;" runat="server" id="contentTable" Width="15%" Height="100%" CssClass="data_table" CellSpacing="0" CellPadding="3">
				            <asp:TableRow CssClass="data_table_title">

					            <asp:TableCell runat="server" id="languageNameCell" CssClass="table_title" ></asp:TableCell>
					            <asp:TableCell runat="server" id="languageCodeCell" CssClass="table_title"></asp:TableCell>

				            </asp:TableRow>
				        </asp:Table>
                        </div>

		            </td></tr></table>
            </table>

    </div>
    </form>
</body>
</html>
