﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PopupSent.aspx.cs" Inherits="DeskAlertsDotNet.Pages.PopupSent" %>

<%@ Import Namespace="DeskAlertsDotNet.Parameters" %>
<%@ Import Namespace="System.Globalization" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="DeskAlerts.Server.Utils" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <title>DeskAlerts</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <link href="css/style9.css" rel="stylesheet" type="text/css">
    <link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="css/jquery/jquery_timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css">
    <link href="css/toastr.min.css" rel="stylesheet" type="text/css">

    <script type="text/javascript" src="functions.js"></script>
    <script type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
    <script type="text/javascript" src="jscripts/shortcut.js"></script>
    <script type="text/javascript" src="jscripts/date.js"></script>
    <script type="text/javascript" src="jscripts/jquery/jquery_timepicker/jquery-ui-timepicker-addon.js"></script>
    <script type="text/javascript" src="jscripts/jquery/datepicker_fix.js"></script>
    <script type="text/javascript" src="jscripts/preview.js?v=9.2.1.31"></script>
    <script type="text/javascript" src="jscripts/toastr.min.js"></script>
    <script type="text/javascript" src="functions.js"></script>

    <script language="javascript" type="text/javascript">
        function openDirDialog(alertID) {
            $(function () {
                var url = "<%=Config.Data.GetString("alertsDir")%>/preview.asp?id=" + alertID;

                $('#linkValue').val(url);
                $('#linkValue').focus(function () {
                    $('#linkValue').select().mouseup(function (e) {
                        e.preventDefault();
                        $(this).unbind("mouseup");
                    });
                });

                $('#linkDirDialog').dialog({
                    autoOpen: false,
                    height: 80,
                    width: 550,
                    modal: true,
                    resizable: false

                }).dialog('open');
            });
        }

        function confirmDelete() {
            if (confirm("Are you sure you want to delete selected items?") == true) {
                return true;
            } else {
                return false;
            }
        }

        function resendAlert(id) {
            var data = { alertId: id };

            $.ajax({
                type: "POST",
                url: "PopupSent.aspx/Resend",
                data: JSON.stringify(data),
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    alert("Alert has been re-sent.\r\nAttention! Your skin will be replaced by the default one if your policy doesn't have the permissions required to use the current skin.");
                    location.reload();
                },
                dataType: "json",
                error: function (xhr, errorType, exception) {
                    console.log(xhr.responseText + " " + errorType + " " + exception + " " + responseText.ExceptionType + " " + responseText.StackTrace + " " + responseText.Message);
                }
            });
        }

        function showAlertPreview(alertId) {

            var data = new Object();

            getAlertData(alertId,
                false,
                function (alertData) {
                    data = JSON.parse(alertData);
                });

            var fullscreen = parseInt(data.fullscreen);
            var ticker = parseInt(data.ticker);
            var acknowledgement = data.acknowledgement;

            var alert_width = (fullscreen == 1) && !ticker
                ? $(".main_table_body").width()
                : (!ticker ? data.alert_width : $(".main_table_body").width());
            var alert_height = (fullscreen == 1) && !ticker
                ? $(window).height() - document.body.clientHeight + $(".main_table_body").height()
                : (!ticker ? data.alert_height : "");
            var alert_html = data.alert_html;

            var alert_title = parseHtmlTitle(alert_html, "<!-- html_title = '", "' -->");

            var top_template;
            var bottom_template;
            var templateId = data.template_id;

            if (templateId >= 0) {
                getTemplateHTML(templateId,
                    "top",
                    false,
                    function (data) {
                        top_template = data;
                    });

                getTemplateHTML(templateId,
                    "bottom",
                    false,
                    function (data) {
                        bottom_template = data;
                    });
            }

            if (ticker == 1) {
                const tickerHeight = 30;
                //Replace all img tag's attribure "height" to ticker height
                alert_html = alert_html.replace(/(.*<img.*height=").*(".*\/>)/, "$1" + tickerHeight + "$2");
            }

            data.top_template = top_template;
            data.bottom_template = bottom_template;
            data.alert_width = alert_width;
            data.alert_height = alert_height;
            data.ticker = ticker;
            data.fullscreen = fullscreen;
            data.acknowledgement = acknowledgement;
            data.alert_title = alert_title;
            data.alert_html = alert_html;
            data.caption_href = data.caption_href;

            initAlertPreview(data);
        }

        function openDialogUI(_form, _frame, _href, _width, _height, _title) {

            $(function () {

                $("#dialog-form").dialog({
                    autoOpen: false,
                    height: _height,
                    width: _width,
                    modal: true
                });

                $("#dialog-" + _form).dialog('option', 'height', _height);
                $("#dialog-" + _form).dialog('option', 'width', _width);
                $("#dialog-" + _form).dialog('option', 'title', _title);
                $("#dialog-" + _frame).attr("src", _href);
                $("#dialog-" + _frame).css("height", _height);
                $("#dialog-" + _frame).css("display", 'block');
                if ($("#dialog-" + _frame).attr("src") != undefined) {
                    $("#dialog-" + _form).dialog('open');
                    $("#dialog-" + _form).contents().find('#table_border').css('border', 'none');

                }
            });
        }


        $("#dialog-form").dialog({
            autoOpen: false,
            height: 100,
            width: 100,
            modal: true
        });

        $("#confirm_sending").dialog({
            autoOpen: false,
            height: 100,
            width: 300,
            modal: false,
            position: { my: "center top", at: "center top", of: $('#parent_for_confirm') },
            hide: { effect: 'fade', duration: 1000 },
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close", ui.dialog || ui).hide();
                setTimeout(function () { $("#confirm_sending").dialog('close'); }, 3000);
            },
            closeOnEscape: false
        });

        $(document).ready(function () {

            var imSent = '<% =Request["imSent"]%>';
            if (imSent == 1) {
                toastr.success("Emergency alert was successfully sent!");
            }

            var sent = '<% =Request["sent"]%>';
            if (sent == 1) {
                toastr.success("Alert was successfully sent!");
            }

            var emailErrorMessage = '<% =emailErrorMessage%>';

            if (emailErrorMessage.length > 0) {
                toastr.error(emailErrorMessage, "Email sending error");
            }


            var smsErrorMessage = '<% =smsErrorMessage%>';

            if (smsErrorMessage.length > 0) {
                toastr.error(smsErrorMessage, "Sms sending error");
            }


            $(".delete_button").button({});

            shortcut.add("Alt+N", function (e) {
                e.preventDefault();
                location.href = "CreateAlert.aspx";
            });

            shortcut.add("delete", function (e) {
                e.preventDefault();

                if ($("#upDelete").length == 0)
                    return;


                if (confirmDelete())
                    __doPostBack('upDelete', '');
            });

            $("#selectAll").click(function () {
                var checked = $(this).prop('checked');
                $(".content_object").prop('checked', checked);
            });

            $(document).tooltip({
                items: "img[title],a[title]",
                position: {
                    my: "center bottom-20",
                    at: "center top",
                    using: function (position, feedback) {
                        $(this).css(position);
                        $("<div>")
                            .addClass("arrow")
                            .addClass(feedback.vertical)
                            .addClass(feedback.horizontal)
                            .appendTo(this);
                    }
                }
            });

            $(function () {
                $(".delete_button").button({
                });

                $(".add_alert_button").button({
                    icons: {
                        primary: "ui-icon-plusthick"
                    }
                });

                $("#linkDirDialog").dialog({
                    autoOpen: false,
                    height: 80,
                    width: 550,
                    modal: true,
                    resizable: false
                });
            });

            var is_sended = $("#is_sended").val();
            if (is_sended == 'true') {
                if ($('#reschedule').val() == '1') {
                    $('#anchor_confirm').text('<%=resources.LNG_CONFIRM_SCHEDULE%>');
                }
                else {
                    $('#anchor_confirm').text('<%=resources.LNG_CONFIRM_SEND%>');
                }
                $("#last_alert").effect('highlight', { color: '#98FB98' }, 2000);
                $("#confirm_sending").dialog('open');
            }

            $("#dialog-frame").load(function () {
                $("#dialog-frame").contents().find('.prelink_stats').on('click', function (e) {
                    e.preventDefault();
                    var link = $(this).attr('href');
                    var title = $(this).attr('title');
                    var last_id = $('.dialog_forms').size();
                    var elem = '<div id="dialog-form-' + last_id + '" class="dialog_forms" style="overflow:hidden;display:none;"><iframe id="dialog-frame-' + last_id + '" style="display:none;width:100%;height:auto;overflow:hidden;border : none;"></iframe></div>';
                    $('#secondary').append(elem);
                    $("#dialog-form-" + last_id).dialog({
                        autoOpen: false,
                        height: 100,
                        width: 100,
                        modal: true
                    });
                    openDialogUI("form-" + last_id, "frame-" + last_id, link, 350, 500, title);
                });
            });

        });

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
            <tr>
                <td>
                    <table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
                        <tr>
                            <td width="100%" height="31" class="main_table_title">
                                <img src="images/menu_icons/alert_20.png" alt="" width="20" height="20" border="0" style="padding-left: 7px">
                                <asp:LinkButton href="PopupSent.aspx" runat="server" ID="headerTitle" class="header_title" Style="position: absolute; top: 22px" />
                            </td>
                        </tr>

                        <tr>
                            <td class="main_table_body" height="100%">
                                <br />
                                <div style="margin-left: 10px">
                                    <span class="work_header" runat="server" id="workHeader"></span>
                                </div>
                                <table style="padding-left: 10px;" width="100%" border="0">
                                    <tbody>
                                        <tr valign="middle">
                                            <td width="175">
                                                <% = resources.LNG_SEARCH_ALERT_BY_TITLE%>
                                                <input type="text" id="searchTermBox" runat="server" name="uname" value="" />
                                            </td>
                                            <td width="1%">
                                                <br />

                                                <asp:LinkButton runat="server" class="search_button ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" ID="searchButton" Style="margin-right: 0px" role="button" aria-disabled="false" Text="<span class='ui-button-icon-primary ui-icon ui-icon-search'></span><span class='ui-button-text'>{Search}</span>" />

                                                <input type="hidden" name="search" value="1">
                                            </td>

                                            <td>
                                                <a class="add_alert_button" id="addButton" runat="server" style="float: right" href="CreateAlert.aspx" role="button" aria-disabled="false" title="Hotkey: Alt+N"><%=resources.LNG_ADD_ALERT%></a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>

                                <div style="margin-left: 10px" runat="server" id="mainTableDiv">

                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td>
                                                <div style="margin-left: 10px" id="topPaging" runat="server"></div>
                                            </td>
                                        </tr>
                                    </table>

                                    <br>
                                    <asp:LinkButton Style="margin-left: 10px" runat="server" ID="upDelete" class='delete_button' OnClientClick="return confirmDelete(); " title="Hotkey: Delete" />
                                    <br>
                                    <br>

                                    <asp:Table Style="padding-left: 0px; margin-left: 10px; margin-right: 10px;" runat="server" ID="contentTable" Width="98.5%" Height="100%" CssClass="data_table" CellSpacing="0" CellPadding="3">
                                        <asp:TableRow CssClass="data_table_title">
                                            <asp:TableCell runat="server" ID="headerSelectAll" CssClass="table_title" Width="2%">
                                                <asp:CheckBox ID="selectAll" runat="server" />
                                            </asp:TableCell>
                                            <asp:TableCell runat="server" ID="typeCell" CssClass="table_title" Width="2%"></asp:TableCell>
                                            <asp:TableCell runat="server" ID="titleCell" CssClass="table_title"></asp:TableCell>
                                            <asp:TableCell runat="server" ID="approveCell" CssClass="table_title"></asp:TableCell>
                                            <asp:TableCell runat="server" ID="creationDateCell" CssClass="table_title" Width="20%"></asp:TableCell>
                                            <asp:TableCell runat="server" ID="scheduledCell" CssClass="table_title" Width="2%"></asp:TableCell>
                                            <asp:TableCell runat="server" ID="senderCell" CssClass="table_title"></asp:TableCell>
                                            <asp:TableCell runat="server" ID="actionsCell" CssClass="table_title" Width="200px"></asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>

                                    <br>
                                    <asp:LinkButton Style="margin-left: 10px" runat="server" ID="bottomDelete" class='delete_button' OnClientClick="return confirmDelete();" title="Hotkey: Delete" />
                                    <br>
                                    <br>

                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td>
                                                <div style="margin-left: 10px" id="bottomRanging" runat="server"></div>
                                            </td>
                                        </tr>
                                    </table>

                                </div>

                                <div runat="server" id="NoRows">
                                    <br />
                                    <br />
                                    <center><b><%=resources.LNG_THERE_ARE_NO_ALERTS%></b></center>
                                </div>

                                <br>
                                <br>
                            </td>
                        </tr>

                    </table>
                </td>
            </tr>
        </table>
    </form>

    <div id="linkDirDialog" title="<%= resources.LNG_DIRECT_LINK%>">
        <input id="linkValue" type="text" readonly="readonly" style="width: 500px" />
    </div>

    <div id="dialog-form" style="overflow: hidden; display: none;">
        <iframe id='dialog-frame' style="display: none; width: 100%; height: auto; overflow: hidden; border: none;"></iframe>
    </div>

    <div id="confirm_sending" style="overflow: hidden; text-align: center;">
        <a id="anchor_confirm"></a>
    </div>

    <div id="secondary" style="overflow: hidden; display: none;"></div>
</body>
</html>
