﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditTextTemplate.aspx.cs" Inherits="DeskAlertsDotNet.Pages.EditTextTemplate" %>
<%@ Import Namespace="DeskAlerts.Server.Utils" %>
<%@ Import NameSpace="DeskAlertsDotNet.Parameters" %>
<%@ Import Namespace="Resources" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>

    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/preview.js?v=9.2.1.31"></script>

    <!-- TinyMCE -->
    <script language="javascript" type="text/javascript" src="jscripts/tiny_mce/tinymce.min.js"></script>


    <script language="javascript">
        $(document).ready(function () {
            $(".cancel_button").button();
            $(".save_button").button();
            $(".preview_button").button();


            tinyMCE.baseURL = window.location.href.substring(0, window.location.href.lastIndexOf("/")) + "/jscripts/tiny_mce/";
            tinymce.init({
                selector: 'textarea#templateContent',
                plugins: "moxiemanager textcolor link code emoticons insertdatetime fullscreen imagetools directionality table media image autolink lists advlist",
                toolbar1: "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | fontselect fontsizeselect | forecolor backcolor | fullscreen",
                toolbar2: "cut copy paste | bullist numlist | outdent indent | undo redo | link unlink | image insertfile code | rotateleft rotateright flipv fliph",
                moxiemanager_image_settings: {
                    view: 'thumbs',
                    extensions: 'jpg,png,gif'
                },
                moxiemanager_video_settings: {
                    view: 'thumbs',
                    extensions: 'mp4,webm,ogg'
                },
                setup: function (ed) {
                    ed.on('init',
                        function (ed) {
                            if (window.location.href.indexOf('id=') == -1) {
                                ed.target.editorCommands.execCommand("fontName", false, "Arial");
                            }
                        });
                },
                relative_urls: false,
                remove_script_host: false,
                fontsize_formats: '8pt 10pt 11pt 12pt 14pt 16pt 18pt 24pt 36pt',
                document_base_url: "<%=Config.Data.GetString("alertsDir")%>/admin/images/upload",
                content_style: "body{color:#000 !important; font-family:Arial, Times New Roman, Helvetica, sans-serif !important; font-size:14px !important}",
                font_formats: 'Andale Mono=andale mono,times;Arial=arial,helvetica,sans-serif;Arial Black=arial black,avant garde;Book Antiqua=book antiqua,palatino;Calibri=calibri;Comic Sans MS=comic sans ms,sans-serif;Courier New=courier new,courier;Georgia=georgia,palatino;Helvetica=helvetica;Impact=impact,chicago;Tahoma=tahoma,arial,helvetica,sans-serif;Times New Roman=times new roman,times;Trebuchet MS=trebuchet ms,geneva;Verdana=verdana,geneva;Webdings=webdings;Wingdings=wingdings,zapf dingbats',
                branding: false,
                advlist_bullet_styles: 'circle,disc,square',
                advlist_number_styles: 'lower-alpha,lower-roman,upper-alpha,upper-roman'
            });

        });

        function LINKCLICK() {
            var yesno = confirm("Are you sure that you wish to cancel this page?", "");
            if (yesno == false) return false;
            return true;
        }

        function my_redirect() {
            if (LINKCLICK() == true) {
                location.href = "TextTemplates.aspx";
            }
        }

        function validateForm() {

            var templateName = document.getElementsByName('templateName')[0].value;
            if (templateName == "") {
                alert("<%=resources.LNG_PLEASE_ENTER_TEMPLATE_TITLE %>");
                return false;
            } else {
                if (templateName.length > 255) {
                    alert("<%=resources.LNG_TITLE_SHOULD_BE_LESS_THEN %>.");
                    return false;
                }

                if ($("#edit").val() == 1) {
                    $("#form1").submit();
                    return true;
                }

                $.ajax({
                    type: "POST",
                    url: "AddTemplate.aspx/IsTemplateExists",
                    data: '{templateName: "' + templateName + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {

                        if (!response.d)
                            $("#form1").submit();
                        else {
                            alert("Template with name '" +
                                templateName +
                                "' is already exists. Please enter another template name.");
                        }
                    },
                    failure: function (response) {

                        alert("Ajax error was occured. Please try again");
                    }
                });

                return true;

            }
        }


        function mypreview1() {
    document.my_form.preview.value = 1;
    document.my_form.sub1.value = 2;
}

function mypreview() {
    var obj = document.getElementById("preview2");
    document.body.removeChild(obj);

    obj = document.getElementById("preview1");
    document.body.removeChild(obj);

    //	document.getElementsByName('preview2')[0].style.display = (document.getElementsByName('preview2')[0].style.display=='none')?'':'none'; 
    //	document.getElementsByName('preview1')[0].style.display = (document.getElementsByName('preview1')[0].style.display=='none')?'':'none'; 
    return false;
}


function showAlertPreview() {
    var data = new Object();

    data.create_date = "<%=DateTime.Now.ToString(DeskAlertsUtils.GetFullDateTimeFormat(dbMgr))%>";

		var fullscreen = 0;
		var ticker = 0;
		var acknowledgement = 0;

		var alert_width = 500;
		var alert_height = 400;
		var alert_title = "Sample title";

		var text_template;

		text_template = tinyMCE.get('templateContent').getContent();



		data.alert_width = alert_width;
		data.alert_height = alert_height;
		data.ticker = ticker;
		data.fullscreen = fullscreen;
		data.acknowledgement = acknowledgement;
		data.alert_title = alert_title;
		data.alert_html = text_template;

		initAlertPreview(data);

		return false;
    }

</script>
</head>
<body>
    <form id="form1" runat="server" method="POST" action="AddTemplate.aspx">
        <input type="hidden" runat="server" id="edit" name="edit" value="0"/>
        <input type="hidden" runat="server" id="templateId" name="templateId"/>
    <div>
        <table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	    <tr>
	    <td>
	    <table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
		    <tr>
		    <td width=100% height=31 class="main_table_title"><img src="images/menu_icons/templates_20.png" alt="" width="20" height="20" border="0" style="padding-left:7px">
		    <a href="" class="header_title" style="position:absolute;top:22px"><%=resources.LNG_TEXT_TEMPLATES %></a></td>
		    </tr>
		    <tr>
		    <td class="main_table_body" height="100%">
		    <div style="margin:10px;">
		    <span class="work_header"><%=resources.LNG_ADD_TEMPLATE %></span>
		    <br><br>
		        <%= resources.LNG_NAME %>: <input runat="server" type="text" id="templateName" name="templateName" value=""/><br><br>
                <%=resources.LNG_TEMPLATE_TEXT %>:<br><br>
                <textarea runat="server" id="templateContent" name="templateContent" rows="15" cols="80" style="width: 100%">
                
                </textarea>
                <br>
            <div align="right">
                <a class="preview_button" href='#' onclick="javascript: showAlertPreview();"><%=resources.LNG_PREVIEW %></a> 
                <a class="save_button" onclick="javascript:validateForm()"><%=resources.LNG_SAVE %></a>
                <a class="cancel_button" onclick="javascript:my_redirect(); return false;"><%=resources.LNG_CANCEL %></a>
            </div>
            </div>
            </td>
            </tr>
        </table>
        </td>
        </tr>
        </table>
    </div>
    </form>
</body>
</html>
