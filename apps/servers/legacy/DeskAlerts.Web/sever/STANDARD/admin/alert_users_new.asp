﻿<% Response.Expires = 0 %>
<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%

check_session()

  uid = Session("uid")
  mifTreeFolder = "mif_tree"
	
  alertId = Clng(Request("id"))
  showCheckbox = 1
  
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<title>DeskAlerts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/style9.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="<%Response.write mifTreeFolder%>/style.css" type="text/css" />
<!---->
<%
' Variables
mifTreeFolder = "mif_tree"

%>

<%
if (uid <> "") then

'modify input  form using javascript
	only_personal=0
	Set RS = Conn.Execute("SELECT id, only_personal FROM users WHERE id="&uid&" AND role='E'")
	if(Not RS.EOF) then
		gid = 1
		Set rs_policy = Conn.Execute("SELECT type FROM policy INNER JOIN policy_editor ON policy.id=policy_editor.policy_id WHERE editor_id=" & uid)
		if(Not rs_policy.EOF) then
			if(rs_policy("type")="A") then
				gid = 0
			end if
		end if
		if(gid=1) then
			editor_id=RS("id")
			only_personal=RS("only_personal")
			policy_ids=""
			policy_ids=editorGetPolicyIds(editor_id)
			group_ids=""
			group_ids=editorGetGroupIds(policy_ids)
			group_ids=replace(group_ids, "group_id", "users_groups.group_id")
			ou_ids=""
			ou_ids=editorGetOUIds(policy_ids)
			ou_ids=replace(ou_ids, "ou_id", "OU_User.Id_OU")
		end if
	else
		gid = 0
	end if
	RS.Close

%>
	
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/mootools.js"></script>	
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Node.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Hover.js"></script>	
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Selection.js"></script>	
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Load.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Draw.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.KeyNav.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Sort.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Transform.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Drag.js"></script>	
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Drag.Element.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Checkbox.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Rename.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Row.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.mootools-patch.js"></script>
	<link rel="stylesheet" href="<%Response.write mifTreeFolder%>/style.css" type="text/css" />
	<script type="text/javascript">

		//
		function send_to_users()
		{
			res = getCheckedOus();
			idSequence = "";			
			idSequence_closed = "";
			if( res )
			{
				for(i=0;i<res.length;i++)
				{
					if(res[i].type != "organization" && res[i].type != "DOMAIN")
					{
						if(res[i].child>0){
							idSequence += res[i].id.toString() + " ";
						}
						else{
							idSequence_closed += res[i].id.toString() + " ";
						}
					}
				}
			}

				document.getElementById("ous_id_sequence").value=idSequence;
				document.getElementById("ous_id_sequence_closed").value=idSequence_closed;
				//alert(idSequence);
			return true;
//			return false;
			//window.location = "ou_send_to_users.asp?ous_id_sequence="+idSequence+"&alert_id=<%=alertId%>";
			// popupWin = window.open(", "contacts", "location,width=600,height=600,top=0");	
		}
	function changeObjectType(){
					tree.select(tree.root);
					$('ou_ui').style.display="";
					document.getElementById("obj_cnt").innerHTML = "0";
					<% controlOUList policy_ids, showCheckbox %>
					var content_file="objects.asp";
					if($('obj_type').value=="R"){
						content_file="objects.asp?sc=<%=showCheckbox %>"
						$('send_type').value="recepients";
						$('upper_send').style.display="";
					}
					if($('obj_type').value=="B"){
						$('send_type').value="broadcast";
					}
					if($('obj_type').value=="I"){
						content_file="ip_groups.asp?sc=<%=showCheckbox %>"
						$('send_type').value="iprange";
						$('upper_send').style.display="";
						$('tree_td').style.display="none";
					}					
					if($('obj_type').value=="0" || $('obj_type').value=="B"){
						$('ou_ui').style.display="none";
						$('upper_send').style.display="none";
					}
					$('content_iframe').src=content_file;		
					$('objects_div').innerHTML = "";
	}	
function check_list()
{
		var formInputs = document.getElementsByTagName("input");
		for(var i=0; i<formInputs.length; i++){
			if(formInputs[i].name=="added_user"){
				return true;
			}
		}
	alert("<%=LNG_PLEASE_SELECT_RECIPIENTS_BEFORE_YOU_PROCEED %>.");
	return false;
}
	
function my_sub(i)
{
	var val;
	var box;
	box = document.getElementById('obj_type');
	val = box.options[box.selectedIndex].value;	
	send_to_users();
	
	if(document.getElementById("ous_id_sequence").value.length>0 || document.getElementById("ous_id_sequence_closed").value.length>0) return true;

	if(val == "0")
	{
		alert('<%=LNG_PLEASE_CHOOSE_THE_TYPE %>');
		return false;
	}
	else if(val == "I")
	{
		var from = ip2long(document.getElementsByName('iprange_from')[0].value);
		var to = ip2long(document.getElementsByName('iprange_to')[0].value);
		if(from == -1 || to == -1 || from > to)
		{
			alert('<%=LNG_PLEASE_ENTER_CORRECT_IP_RANGE %>');
			return false;
		}
		document.frm.sub1.value = i;
		return true;
	}
	else
	{
		document.frm.sub1.value = i;
		if (val != "B") //not broadcast
			return check_list();
		return true;
	}
}	
	</script>
<!-- -->
</head>
<script language="javascript">
function LINKCLICK() 
{
  var yesno = confirm ("Are you sure you want to delete this user?","");
  if (yesno == false) return false;
  return true;
}
</script>


<body style="margin:0px" class="body">

<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	<tr>
	<td>
	<table width="100%" height="100%"cellspacing="0" cellpadding="0" class="main_table">
		<tr>
		<td width="100%" height="31" class="main_table_title"><a href="alert_users.asp" class="header_title">Alerts</a></td>
		</tr>
		<tr>
		<td class="main_table_body" height="100%" valign="top">
		
		<div style="margin:10px;">
		<div align="right"></div>
		<span class="work_header">Add alert</span>
		<br><br>
<div>Step <strong>2</strong> of 2: Select recipients:</div>

<%
if(Request("end")="1") then
else
%>
<!--<br><font color="red">Attention! Desktop alert doesn't have an end date. Users will receive it once they will be on-line. Click "Back" and check "Schedule alert" to enter end date.</font>-->

<%if(request("sms")="1" OR request("email")="1") then%>
<br><font color="red">Attention! Sending SMS, e-mail may take several minutes. It depends on the number of total recipients.</font>
<%end if%>
<%
end if
%>
<form name="frm" action="change_users.asp" method="POST">

<input type="hidden" name="sms" value="<%=request("sms")%>">
<input type="hidden" name="email" value="<%=request("email")%>">
<input type="hidden" name="email_sender" value="<%=request("email_sender")%>">
<input type="hidden" name="desktop" value="<%=request("desktop")%>">


<input type="hidden" name="sub1" value="">
<table cellpadding=4 cellspacing=4 border=0>
<tr><td>Alert type:</td><td><select name="obj_type" id="obj_type" onChange="javascript:changeObjectType()">

<option value="0">-- Choose --</option>
<%
if (not ConfDisableBroadcast) then
%>
<option value="B">Broadcast</option>
<%
end if
%>

<option value="R">Select recepients</option>

<% if (ConfEnableIPRanges = 1 AND gid=0) then %>
<option value="I">IP range</option>
<% end if %>
</select></td></tr>
</table>

<div align="right" id="upper_send" style="display:none"><input type="image" src="images/send.gif" alt="Send" onClick="javascript: return my_sub(2);"></div>

<div id="ou_ui" style="display: none">
<!-- #include file="ou_ui_include.asp" -->

</div>

<input type="hidden" name="send_type" id="send_type" value="0"/>


<%
  Response.Write "<input type='hidden' name='alert_id' value='"+ Request("id")+ "'/>"
%>

<input type="hidden" id="ous_id_sequence" name="ous_id_sequence" value=""/>
<input type="hidden" id="ous_id_sequence_closed" name="ous_id_sequence_closed" value=""/>
<div align="right"><input type="image" src="images/send.gif" alt="Send" onClick="javascript: return my_sub(2);"></div>
</form>

<%

else

  Response.Redirect "index.asp"

end if
%> 
		 
		
		
		</td>
		</tr>
	</table>
	
	</td>
	</tr>
</table>
</body>
</html>
<!-- #include file="db_conn_close.asp" -->