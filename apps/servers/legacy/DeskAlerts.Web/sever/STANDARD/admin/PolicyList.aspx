﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PolicyList.aspx.cs" Inherits="DeskAlertsDotNet.Pages.PolicyList" %>
<%@ Import NameSpace="DeskAlertsDotNet.Parameters" %>
<%@ Import NameSpace="Resources" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<title>DeskAlerts</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link href="css/style9.css" rel="stylesheet" type="text/css">
        <link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <link href="css/jquery/jquery_timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css">
        <style>
            #contentTable tr td:nth-child(3){
                text-align: center;
            }
        </style>
        <script language="javascript" type="text/javascript" src="functions.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/date.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery_timepicker/jquery-ui-timepicker-addon.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/jquery/datepicker_fix.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/shortcut.js"></script>
        <script language="javascript" type="text/javascript">


            $(document).ready(function() {
				shortcut.add("Alt+N",function(e) {
					e.preventDefault();
					location.href = "policy_edit.asp";
				});
				
				shortcut.add("delete",function(e) {
					e.preventDefault();
					if (confirmDelete())
						__doPostBack('upDelete', '');
				});
			
                $(".delete_button").button({});

                $(".delete_button").click(function (e) {
                    e.preventDefault();
                    var error = false;

                    $(".content_object").each(function () {
                        if ($(this).is(":checked") && ($(this).parent().parent().find("td:nth-child(3)").text() != 0)) {    
                            alert("You can't delete policy with users in it.");
                            error = true;
                        };
                    });
                    
                    if (!error) {
                        __doPostBack('upDelete', '');
                    }
                });

                $("#selectAll").click(function () {
                    var checked = $(this).prop('checked');
                    $(".content_object").prop('checked', checked);
                });
                $(".add_alert_button").button({
                    icons: {
                        primary: "ui-icon-plusthick"
                    }
                });

                $(document).tooltip({
                    items: "img[title],a[title]",
                    position: {
                        my: "center bottom-20",
                        at: "center top",
                        using: function (position, feedback) {
                            $(this).css(position);
                            $("<div>")
                           .addClass("arrow")
                           .addClass(feedback.vertical)
                           .addClass(feedback.horizontal)
                           .appendTo(this);
                        }
                    }
                });

                $("#editors_dialog").dialog({
                    title: "Editors",
                    width: 430,
                    height: 460,
                    autoOpen: false,
                    draggable: false,
                    resizable: false,
                    modal: true
                });


            });

            function confirmDelete() {
                if (confirm("Are you sure you want to delete selected items?") == true)
                    return true;
                else
                    return false;
            }

            function showEditorsList(id) {

                $(function () {
                    $("#editors_frame").attr("src", "ViewEditorsList.aspx?id=" + id);
                    $("#editors_dialog").dialog("open");
                });
            }


            function duplicatePolicy(id) {
                $.ajax({
                    type: "POST",
                    url: "PolicyList.aspx/DuplicatePolicy",
                    data: '{id: ' + id + ' }',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {

                        location.reload();
                        //window.parent.parent.document.getElementById('menu').src = window.parent.parent.document.getElementById('menu').src;
                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            }

        </script>
</head>
<body>
    <form id="form1" runat="server">
    <table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	    <tr>
	    <td>
	    <table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
		<td width="100%" height="31" class="main_table_title"><img src="images/menu_icons/policies_20.png" alt="" width="20" height="20" border="0" style="padding-left:7px">
		<asp:LinkButton href="PublisherList.aspx" runat="server" id="headerTitle" class="header_title" style="position:absolute;top:22px" />
		</td>
            <tr>
            <td class="main_table_body" height="100%">
	                <br /><br />
                <div style="display:none" id="editors_dialog" style="margin:0px;padding:0px">
                    <iframe id="editors_frame" src="ViewEditorsList.aspx" style="width:400px;height:400px;border:0px"></iframe>
                </div>
                <div style="margin-left:10px;">
                <table style="padding-left: 10px;" width="100%" border="0">
                    <tbody><tr valign="middle"><td width="175">
                            <% =resources.LNG_SEARCH_POLICY_BY_TITLE %> <input type="text" id="searchTermBox" runat="server" name="uname" value=""/></td>
                <td width="1%">
                    <br />
                    <asp:linkbutton runat="server" class="search_button ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" id="searchButton" style="margin-right:0px" role="button" aria-disabled="false" Text="<span class='ui-button-icon-primary ui-icon ui-icon-search'></span><span class='ui-button-text'>{Search}</span>"/>
	                <input type="hidden" name="search" value="1">
                </td>

                <td>
                    <a class="add_alert_button" title="Hotkey: Alt+N" runat="server" id="addButton"  style="float:right;margin-right: 7px;" href="EditPolicy.aspx" role="button" aria-disabled="false"><%=resources.LNG_ADD_POLICY %></a>
                </td>
                  </tr></tbody>

                </table>	
                </div>
			<div style="margin-left:10px" runat="server" id="mainTableDiv">

			   <table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr><td> 
					<div style="margin-left:10px" id="topPaging" runat="server"></div>
				</td></tr>
				</table>
				<br>
				<asp:LinkButton style="margin-left:10px" title="Hotkey: Delete" runat="server" id="upDelete" class='delete_button' />
				<br><br>
				<asp:Table style="padding-left: 0px;margin-left: 10px;margin-right: 10px;" runat="server" id="contentTable" Width="98.5%" Height="100%" CssClass="data_table" CellSpacing="0" CellPadding="3">
				<asp:TableRow CssClass="data_table_title">
					<asp:TableCell runat="server" id="headerSelectAll" CssClass="table_title" Width="2%" >
						<asp:CheckBox id="selectAll" runat="server"/>
					</asp:TableCell>
					<asp:TableCell runat="server" id="policyNameCell" CssClass="table_title" ></asp:TableCell>
                    <asp:TableCell runat="server" Width="10%" id="publishersCountCell" CssClass="table_title" ></asp:TableCell>
                    <asp:TableCell runat="server" Width="8%" id="actionsCell" CssClass="table_title"></asp:TableCell>
				</asp:TableRow>
				</asp:Table>
				<br>
				 <asp:LinkButton style="margin-left:10px" title="Hotkey: Delete" runat="server" id="bottomDelete" class='delete_button'  />
				 <br><br>
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				 
				<tr><td> 
					<div style="margin-left:10px" id="bottomRanging" runat="server"></div>
				</td></tr>
				</table>
			</div>
			<div runat="server" id="NoRows">
                <br />
                <br />
				<center><b><%=resources.LNG_THERE_ARE_NO_POLICIES %></b></center>
			</div>
			<br><br>
            </td>
            </tr>
           
 
            
             </table>
    </form>
</body>
</html>
