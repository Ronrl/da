﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Parameters;


namespace DeskAlertsDotNet.Pages
{
    using Resources;

    public partial class EditIpGroups : DeskAlertsBasePage
    {
        //use curUser to access to current user of DA control panel      
        //use dbMgr to access to database

        public string jsonIps = string.Empty;

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            var Id = Request["id"];
            if (!string.IsNullOrEmpty(Id))
            {
                addButtonText.Text = resources.LNG_SAVE;
                var ips = dbMgr.GetDataByQuery("SELECT from_ip, to_ip FROM ip_range_groups WHERE group_id=" + Id);
                jsonIps = ips.ToJson();
                groupName.Value = dbMgr.GetScalarQuery<string>("SELECT name FROM ip_groups WHERE id=" + Id);
                edit.Value = "1";
                groupId.Value = Id;
            }
            else
            {
                addButtonText.Text = resources.LNG_ADD;
            }
        }
    }
}