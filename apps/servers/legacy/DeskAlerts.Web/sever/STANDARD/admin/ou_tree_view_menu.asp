﻿<% Response.Expires = 0 %>
<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%

check_session()

%>
<!-- #include file="ad.inc" -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Admin</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/style9.css" rel="stylesheet" type="text/css">
</head>
<%
' Variables
mifTreeFolder = "mif_tree"
%>

<%
  uid = Session("uid")

if (uid <>"") then

%>



<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	
	
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/mootools.js"></script>	
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Node.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Hover.js"></script>	
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Selection.js"></script>	
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Load.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Draw.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.KeyNav.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Sort.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Transform.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Drag.js"></script>	
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Drag.Element.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Checkbox.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Rename.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Row.js"></script>
	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.mootools-patch.js"></script>
	
	<link rel="stylesheet" href="<%Response.write mifTreeFolder%>/style.css" type="text/css" />
	<script type="text/javascript">
		var tree;
		
		function getCheckedOus ()
		{			
			if( tree && tree != 'undefined' )
			{
				nodes = new Array();									
				tree.root.recursive
				(
					function()
					{
						if( this.state.checked == "checked" )
						{						
							temp = new Object();
							nodes.push(temp);
							nodes[nodes.length-1].id = this.property.id;
							nodes[nodes.length-1].type = this.property.type;							
							
						}
					}
				);				
				return nodes;
			}
			return null;
		}
		
		function ddd()
		{
			res = getCheckedOus();
			if( res )
			{
				for(i=0;i<res.length;i++)
				{
					alert(res[i].id.toString()+" "+res[i].type.toString());
				}
			}
		}

/*window.addEvent('domready',function(){

	Mif.Tree.Node.implement({
		reloadChildren: function() {
			this.state.loaded=false;
			this.state.open=false;
			this.state.loadable=true;
			this.children=[];
			this.$draw=false;
			this.tree.$getIndex();
			this.getDOM('children').innerHTML='';
			Mif.Tree.Draw.update(this);
			return this;
		}       

	});

	tree = new Mif.Tree({
		container: $('tree_container'),
		forest: true,
		initialize: function(){
			new Mif.Tree.KeyNav(this);
			new Mif.Tree.Drag(this);
		},
		types: {
			folder:{
				openIcon: 'mif-tree-open-icon',
				closeIcon: 'mif-tree-close-icon'
			},
			loader:{
				openIcon: 'mif-tree-loader-open-icon',
				closeIcon: 'mif-tree-loader-close-icon',
				dropDenied: ['inside','after']
			},
			disabled:{
				openIcon: 'mif-tree-open-icon',
				closeIcon: 'mif-tree-close-icon',
				dragDisabled: true,
				cls: 'disabled'
			},
			book:{
				openIcon: 'mif-tree-book-icon',
				closeIcon: 'mif-tree-book-icon',
				loadable: true
			},
			bin:{
				openIcon: 'mif-tree-bin-open-icon',
				closeIcon: 'mif-tree-bin-close-icon'
			}
		},
		dfltType:'folder',
		height: 18
	});
	
	tree.addEvent('loadChildren', function(parent){
		if(!parent) return;
		if(!parent.$name){
			parent.$name=parent.name;
		}
		parent.set({
			property:{
				name: parent.$name+' ('+parent.children.length+')'
			}
		});
	});

	tree.load({
		url: 'ou_get_tree.asp'
	});

	tree.loadOptions=function(node){
		return {
			url: 'child.asp'
		};
	}
	
});        */
		
		window.addEvent('domready',function()
		{		

				tree = new Mif.Tree({
					container: $('tree_container'),
					forest: false,
					initialize: function(){
//						this.initCheckbox('deps');
						new Mif.Tree.KeyNav(this);
					},					
					types: {
						organization:{
							openIcon: 'mif-tree-organization-icon',
							closeIcon: 'mif-tree-organization-icon'
						},
						domain:{
							openIcon: 'mif-tree-domain-icon',
							closeIcon: 'mif-tree-domain-icon'
						},
						loader:{
							openIcon: 'mif-tree-loader-open-icon',
							closeIcon: 'mif-tree-loader-close-icon',
							dropDenied: ['inside','after']
						},
						ou:{
							openIcon: 'mif-tree-ou-icon',
							closeIcon: 'mif-tree-ou-icon'							
						}	
					},
					dfltType:'organization'
					
				});

				
				tree.initSortable();
				try
				{
					tree.load({
						url: 'ou_get_tree.asp'
					});
					
				}
				catch(e)
				{
					alert('Fail');
				}

				tree.loadOptions=function(node){
//					alert(node);
					return {
						url: 'ou_get_child.asp?id='+node.id
//						url: 'child.asp?id='+node.id


					};
				}

		});   

	</script>
</head>
<body style="margin:0px" class="body">
<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	<tr>
	<td>
	<table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
		<tr>
		<td width=100% height=31 class="main_table_title"><a href="ou_tree_view.asp" class="header_title">Organizational Units</a></td>
		</tr>
		<tr>
		<td class="main_table_body" height="100%">
		<div style="margin:10px;">
		<br><div align="right"></div>

<!--	<div id="tree_view_page_header"></div>	-->
	<div id="tree_container"></div>
<!--	<div id="tree_view_page_footer"></div>-->

		</td>
		</tr>
	</table>
	</td>
	</tr>
</table>
<%
else

  Response.Redirect "index.asp"

end if
%> 
</body>
</html>
<!-- #include file="db_conn_close.asp" -->