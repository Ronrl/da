﻿// ReSharper disable ArrangeAccessorOwnerBody
namespace DeskAlertsDotNet.Pages
{
    using System;
    using System.Web;
    using DeskAlertsDotNet.Managers;
    using DeskAlertsDotNet.Parameters;
    using DeskAlertsDotNet.sever.STANDARD.Utils;

    public partial class Logout : System.Web.UI.Page
    {
        // ReSharper disable once InconsistentNaming
        private Guid _sessionToken;

        public void DropSessionToken(object token)
        {
            if (token != null)
            {
                _sessionToken = Guid.Parse(token.ToString());
                UserManager.Default.Unregister(_sessionToken);
            }

            if (Settings.Content["ConfTokenAuthEnable"] == "1")
            {
                var tokenTools = new TokenTools();
                var cookie = HttpContext.Current.Request.Cookies["desk_token"];
                if (cookie != null)
                {
                    var cookieToken = cookie.Value;
                    tokenTools.DeleteToken(cookieToken);
                }
            }

            Session.Clear();
            Session.Abandon();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            DropSessionToken(Session["token"]);

            if (!Config.Data.IsOptionEnabled("ConfEnableIISAuth"))
            {
                Response.Redirect("logout.asp");
            }
        }
    }
}