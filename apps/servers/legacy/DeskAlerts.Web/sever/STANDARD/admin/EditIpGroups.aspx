<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditIpGroups.aspx.cs" Inherits="DeskAlertsDotNet.Pages.EditIpGroups" %>

<%@ Import Namespace="DeskAlertsDotNet.Parameters" %>
<%@ Import Namespace="Resources" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>DeskAlerts</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="css/style9.css" rel="stylesheet" type="text/css" />
    <link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $(".add_button").button();
            $(".save_button").button();
            $(".cancel_button").button();

            var jsonIps = '<% = jsonIps %>';

            if (jsonIps.length > 0) {

                var ipsArray = JSON.parse(jsonIps);

                for (i = 0; i < ipsArray.length; i++) {

                    var from = ipsArray[i].from_ip;
                    var to = ipsArray[i].to_ip;

                    if (from == to) {
                        addItem("address", from, to);
                    } else {
                        addItem("range", from, to);
                    }
                }
            }
        });
    </script>

    <script>
        var cnt = 0;

        function verifyIP(IPvalue) {
            errorString = "";
            //theName = "IPaddress";
            theName = "Error";

            var ipPattern = /^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$/;
            var ipArray = IPvalue.match(ipPattern);

            if (ipArray == null)
                errorString = errorString + theName + ': ' + IPvalue + ' is not a valid IP address.';
            else {
                for (i = 1; i <= 4; i++) {
                    thisSegment = ipArray[i];
                    if (thisSegment > 255) {
                        errorString = errorString + theName + ': ' + IPvalue + ' is not a valid IP address.';
                        i = 5;
                    }
                    if ((i == 0) && (thisSegment > 255)) {
                        errorString = errorString + theName + ': ' + IPvalue + ' is a special IP address and cannot be used here.';
                        i = 5;
                    }
                }
            }
            extensionLength = 3;
            if (errorString != "") {
                alert(errorString);
                return false
            }
            else {
                return true
            }
        }
        function dot2num(dot) {
            var d = dot.split('.');
            return ((((((+d[0]) * 256) + (+d[1])) * 256) + (+d[2])) * 256) + (+d[3]);
        }

        function checkValues() {
            var found = false, answersForm = document.getElementsByTagName("input");
            for (var i = 0; i < answersForm.length; i++) {
                if (answersForm[i].name == "ip_address") {
                    if (verifyIP(answersForm[i].value) == false) return false;
                    found = true;
                }
                else if (answersForm[i].name == "ip_address_from") {
                    if (verifyIP(answersForm[i].value) == false) return false;
                    if (verifyIP(answersForm[i + 1].value) == false) return false;
                    if (dot2num(answersForm[i].value) > dot2num(answersForm[i + 1].value)) {
                        alert("'" + answersForm[i].value + "' / '" + answersForm[i + 1].value +"' <%=resources.LNG_FROM_IP_ADDRESS_IS_MORE_THEN_TO_IP_ADDRESS %>");
                        return false;
                    }
                    found = true;
                }
            }
            if (!found) {
                alert("<%=resources.LNG_YOU_SHOULD_ADD_IP_OR_IP_RANGE_TO_PROCEED %>.");
                return false;
            }
            return true;
        }

        function ValidateAndSubmit() {
            if (checkValues() == false) return false;
            if (document.getElementById('groupName').value == "") {
                alert("<%=resources.LNG_PLEASE_ENTER_GROUP_NAME %>");
            }
            else {
                $("#form1").submit();
            }
        }

        function deleteItem() {
            this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode);
        }

        function addItem(val, ip_from, ip_to) {
            var tbl = document.getElementById('ip_group_table');
            var lastRow = tbl.rows.length;
            var row = tbl.insertRow(lastRow - 1);

            var cellLeft = row.insertCell(0);
            var cellMiddle = row.insertCell(1);
            var cellRight = row.insertCell(2);

            if (val == "address") {
                cellLeft.innerHTML = "<%=resources.LNG_IP_ADDRESS %>";
                var el = document.createElement('input');
                el.type = 'text';
                el.name = 'ip_address';
                el.value = ip_from;
                el.id =  'ip_address_' + (lastRow - 1);
                cellMiddle.appendChild(el);
                el.focus();
            }
            else {
                var el = document.createElement('input');
                el.type = 'text';
                el.name = 'ip_address_from';
                el.id =  'ip_address_from_' + (lastRow - 1);
                el.value = ip_from;
                cellMiddle.appendChild(el);
                el.focus();

                var txt = document.createTextNode(' - ');
                cellMiddle.appendChild(txt);

                cellLeft.innerHTML = "<%=resources.LNG_IP_RANGE %>";
                var el1 = document.createElement('input');
                el1.type = 'text';
                el1.name = 'ip_address_to';
                el1.id =  'ip_address_to_' + (lastRow - 1);
                el1.value = ip_to;
                cellMiddle.appendChild(el1);
            }

            var el2 = document.createElement('img');
            el2.src = 'images/action_icons/delete.png';
            el2.name = 'answer_delete';
            el2.id = 'answer_delete_' + (lastRow - 1);
            el2.border = "0";
            el2.onclick = deleteItem;
            cellRight.appendChild(el2);
        }
    </script>
    <script>
        function handleKeyPress(e) {
            var key = e.keyCode || e.which;
            if (key == 13) {
                return false;
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server" action="AddIpGroups.aspx" method="POST">
        <input type="hidden" id="edit" value="0" runat="server" />
        <input type="hidden" id="groupId" value="0" runat="server" />
        <div>
            <table border="0">
                <tr>
                    <td>
                        <table class="main_table">
                            <tr>
                                <td class="main_table_title">
                                    <img src="images/menu_icons/ipgroups_20.png" alt="" width="20" height="20" border="0" style="padding-left: 7px" />
                                    <a href="EditIpGroups.aspx" class="header_title" style="position: absolute; top: 22px"><%=resources.LNG_IP_GROUPS %></a>
                                </td>
                            </tr>
                            <tr>
                                <td class="main_table_body">
                                    <div style="margin: 10px;">
                                        <span class="work_header"><%=resources.LNG_ADD_IP_GROUP %></span>
                                        <br />
                                        <br />
                                        <table border="0" id="ip_group_table">
                                            <tr>
                                                <td><%= resources.LNG_IP_GROUP_NAME %>:</td>
                                                <td>
                                                    <input runat="server" name="groupName" id="groupName" type="text" value="" /></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td colspan="4"><a id="ip_address" href="#" onclick="addItem('address','0.0.0.0','0.0.0.0');"><%=resources.LNG_ADD_IP_ADRESS %></a> / <a id="ip_range" href="#" onclick="addItem('range','0.0.0.0','0.0.0.0');"><%=resources.LNG_ADD_IP_RANGE %></a></td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="cancel_button" href="IpGroups.aspx"><%=resources.LNG_CANCEL%></a>
                                    <a runat="server" onclick="ValidateAndSubmit()" class="add_button" style="margin-right: 10px">
                                        <asp:Literal runat="server" ID="addButtonText"></asp:Literal></a>
                                    <br />
                                    <br />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
