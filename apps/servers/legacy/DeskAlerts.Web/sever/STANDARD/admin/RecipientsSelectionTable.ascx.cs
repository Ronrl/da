﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Models;
using DeskAlertsDotNet.Parameters;

namespace DeskAlertsDotNet.Controls
{
    using Resources;

    public partial class RecipientsSelectionTable : System.Web.UI.UserControl
    {
        protected DBManager dbMgr = new DBManager();

        public string UsersLinkTable { get; set; }
        public string GroupsLinkTable { get; set; }
        public string OUsLinkTable { get; set; }
        public string ComputersLinkTable { get; set; }

        public string UsersLinkCollumn { get; set; }
        public string GroupsLinkCollumn { get; set; }
        public string OUsLinkCollumn { get; set; }
        public string ComputersLinkCollumn { get; set; }

        public string LinkCollumn { get; set; }


        public bool ChildGroups { get; set; }


        public string LinkId { get; set; }

 
        private string templateSql = "SELECT a.id, a.name FROM {0} as a INNER JOIN {1} as b ON a.id = b.{2} WHERE b.{3} = {4}";
        public void FillUsers()
        {
          //  string sql = "SELECT users.id as id, name FROM users_groups INNER JOIN users ON users_groups.user_id=users.id WHERE users_groups.group_id=" + groupId;
            int id = 0;

            if (!int.TryParse(LinkId, out id))
            {
                return;
            }

            string sql =  string.Format(templateSql, "users", UsersLinkTable, UsersLinkCollumn, LinkCollumn, LinkId);
            DataSet dataSet = dbMgr.GetDataByQuery(sql);

            foreach (DataRow row in dataSet)
            {
                ListItem li = new ListItem(row.GetString("name"), "users_" + row.GetString("id"));
                list2.Items.Add(li);

                if (usersInput.Value.Length > 0)
                    usersInput.Value += ",";

                usersInput.Value += row.GetString("id");

                //Response.Write("<option value='" + row.GetString("id") + "'>" + row.GetString("name") + "</option>");
            }
        }

        public void FillGroups()
        {
           // string sql = "SELECT groups.id as id, name FROM groups_groups INNER JOIN groups ON groups_groups.group_id=groups.id WHERE container_group_id=" + groupId;

            int id = 0;

            if (!int.TryParse(LinkId, out id))
                return;


            string sql = string.Format(templateSql, "groups", GroupsLinkTable, GroupsLinkCollumn, !ChildGroups ? LinkCollumn : "container_group_id", LinkId);
            DataSet dataSet = dbMgr.GetDataByQuery(sql);

            foreach (DataRow row in dataSet)
            {
                ListItem li = new ListItem(row.GetString("name"), "groups_"+row.GetString("id"));
                list2.Items.Add(li);

                if (groupsInput.Value.Length > 0)
                    groupsInput.Value += ",";

                groupsInput.Value += row.GetString("id");
                // Response.Write("<option value='" + row.GetString("id") + "'>" + row.GetString("name") + "</option>");
            }
        }

        public void FillOUs()
        {
          //  string sql = "SELECT OU.id as id, name FROM ou_groups INNER JOIN OU ON ou_groups.ou_id=OU.Id WHERE group_id=" + groupId;

            int id = 0;

            if (!int.TryParse(LinkId, out id))
                return;

            string sql = string.Format(templateSql, "OU", OUsLinkTable, OUsLinkCollumn, LinkCollumn, LinkId);

            DataSet dataSet = dbMgr.GetDataByQuery(sql);

            foreach (DataRow row in dataSet)
            {
                ListItem li = new ListItem(row.GetString("name"), "ous_" + row.GetString("id"));
                list2.Items.Add(li);

                if (ousInput.Value.Length > 0)
                    ousInput.Value += ",";

                ousInput.Value += row.GetString("id");
                //Response.Write("<option value='" + row.GetString("id") + "'>" + row.GetString("name") + "</option>");
            }
        }

        public void FillComputers()
        {
          //  string sql = "SELECT computers.id as id, name FROM computers_groups INNER JOIN computers ON computers_groups.comp_id=computers.id WHERE group_id=" + groupId;
            int id = 0;

            if (!int.TryParse(LinkId, out id))
                return;

            string sql = string.Format(templateSql, "computers", ComputersLinkTable, ComputersLinkCollumn, LinkCollumn, LinkId);
            DataSet dataSet = dbMgr.GetDataByQuery(sql);

            foreach (DataRow row in dataSet)
            {
                ListItem li = new ListItem(row.GetString("name"), "computers_"+row.GetString("id"));
                list2.Items.Add(li);

                if (computersInput.Value.Length > 0)
                    computersInput.Value += ",";

                computersInput.Value += row.GetString("id");
                // Response.Write("<option value='" + row.GetString("id") + "'>" + row.GetString("name") + "</option>");
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {                
                Session["group_id"] = Request["group_id"];
                list2.ClientIDMode = ClientIDMode.Static;
                list2.ID = "list2";
                ListItem groupsListItem = new ListItem(
                    resources.LNG_LIST_OF_SELECTED_GROUPS + ":", "groups");
                groupsListItem.Attributes.Add("disabled", "disabled");
                list2.Items.Add(groupsListItem);
                FillGroups();

                ListItem ousListItem = new ListItem(resources.LNG_LIST_OF_SELECTED_OUS + ":",
                    "ous");
                ousListItem.Attributes.Add("disabled", "disabled");
                list2.Items.Add(ousListItem);
                FillOUs();

                ListItem compsListItem =
                    new ListItem(resources.LNG_LIST_OF_SELECTED_COMPUTERS + ":", "computers");
                compsListItem.Attributes.Add("disabled", "disabled");
                list2.Items.Add(compsListItem);
                FillComputers();

                ListItem usersListItem = new ListItem(resources.LNG_LIST_OF_SELECTED_USERS + ":",
                    "users");
                usersListItem.Attributes.Add("disabled", "disabled");
                list2.Items.Add(usersListItem);
                FillUsers();
            }
        }
    }
}