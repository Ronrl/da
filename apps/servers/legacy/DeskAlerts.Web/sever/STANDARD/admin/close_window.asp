﻿<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>DeskAlerts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/style9.css" rel="stylesheet" type="text/css">
<script language="javascript" type="text/javascript">
	window.close();
</script>
</head>
<body class="body">
<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
<tr>
	<td>
		<table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
		<tr>
			<td width="100%" height="31" class="main_table_title"><a href="#" class="header_title">Closing window</a></td>
		</tr>
		<tr>
			<td class="main_table_body" height="100%">
				<div style="padding:10px;text-align:center">
					Your browser doesn't support window closing. Please close this window manually.
					<img src="images/inv.gif" alt="" width="1" height="100%" border="0">
				</div>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
</body>
</html>