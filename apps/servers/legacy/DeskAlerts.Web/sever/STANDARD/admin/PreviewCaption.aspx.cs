﻿namespace DeskAlertsDotNet.Pages
{
    using System;
    using System.IO;
    using System.Text.RegularExpressions;
    using System.Web;
    using DeskAlertsDotNet.Parameters;
    using HtmlAgilityPack;

    /// <summary>
    /// The preview caption.
    /// </summary>
    public partial class PreviewCaption : DeskAlertsBasePage
    {
        /// <summary>
        /// The skin path.
        /// </summary>
        private string skinPath;

        /// <summary>
        /// The on load.
        /// </summary>
        /// <param name="e">
        /// The e.
        /// </param>
        protected override void OnLoad(EventArgs e)
        {
            try
            {
                base.OnLoad(e);
                this.Response.Expires = 0;

                string skinId = this.Request.GetParam("skin_id", "default");
                this.skinPath = $"/admin/skins/{skinId}/version/";
                string caption = this.Request.GetParam("caption_href", @".\alertcaption.html");
                string captionPath = $"skins/{skinId}/version/{caption}";
                string fullCaptionPath = this.Server.MapPath(captionPath);
                string customCaption = this.Request.GetParam("caption_html", string.Empty);
                string captionHtml;
                if (!string.IsNullOrEmpty(customCaption))
                {
                    captionHtml = HttpUtility.HtmlDecode(customCaption);
                }
                else
                {
                    captionHtml = File.ReadAllText(fullCaptionPath);
                }
                if (addPrintButton)
                {
                    captionHtml = GetPrintButton(captionHtml);
                }
               
                Regex regex = new Regex("<head[^>]*>", RegexOptions.IgnoreCase);

                // captionHtml = regex.
                // Add your main code here
                string props = string.Empty;
                if (!string.IsNullOrEmpty(this.Request["ext_props"]))
                {
                    props = this.Request["ext_props"]
                        .Replace("'", @"\'")
                        .Replace("\\n", "\\\\n")
                        .Replace("\"", "\\\"");
                }

                string replacement = "$&\n" +
                    "<!--server_preview_section_start-->\n" +
                    "<link href='css/jquery/jquery-ui.css' rel='stylesheet' type='text/css'/>\n" +
                    "<script type='text/javascript' language='JavaScript' src='jscripts/jquery/jquery.min.js'></script>\n" +
                    "<script type='text/javascript' language='JavaScript' src='jscripts/jquery/jquery-ui.min.js'></script>\n" +
                    "<script type='text/javascript' language='JavaScript'>\n" +
                    "	var ext_props=$.parseJSON($('<div/>').html(\n" +
                    "	'" + props + "'" +
                    @".replace(/&/g, '&amp;').replace(/""/g, '&quot;').replace(/'/g, '&#39;').replace(/</g, '&lt;').replace(/>/g, '&gt;')" +
                    ").text());" + "\n" +
                    "</script>" + "\n" +
                    "<script type='text/javascript' language='JavaScript' src='external.asp'></script>\n" +
                    "<base href='" + Config.Data.GetString("alertsDir") + "/" + this.skinPath + "' />\n" +
                    "<!--server_preview_section_end-->";
                captionHtml = regex.Replace(captionHtml, replacement);
                captionHtml = captionHtml.Replace(".parentElement", ".parentNode");
                this.Response.Write(captionHtml);
            }
            catch (Exception ex)
            {
                Logger.Debug(ex);
                throw;
            }
        }

        private string GetPrintButton(string captionHtml)
        {
            try
            {
                HtmlDocument htmlDoc = new HtmlDocument();
                htmlDoc.LoadHtml(captionHtml);

                var htmlBody = htmlDoc.DocumentNode.SelectSingleNode("//body");

                HtmlNode h2Node = HtmlNode.CreateNode("<script>" +
                    "var html = document.body.innerHTML;" +
                    "showPrintButton(true, html); " +
                    "</script>");

                htmlBody.AppendChild(h2Node);
                
                return htmlDoc.DocumentNode.InnerHtml;
            }
            catch (Exception ex)
            {
                Logger.Debug(ex);
                throw;
            }
        }
    }
}