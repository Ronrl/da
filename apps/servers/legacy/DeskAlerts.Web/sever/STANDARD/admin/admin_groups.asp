﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<!-- #include file="ad.inc" -->
<%

check_session()

%>

<%

  uid = Session("uid")

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>DeskAlerts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/style9.css" rel="stylesheet" type="text/css">
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script language="javascript" type="text/javascript">

$(document).ready(function() {
	var width = $(".data_table").width();
	var pwidth = $(".paginate").width();
	if (width != pwidth) {
		$(".paginate").width("100%");
	}
	$(".search_button").button({
		icons : {
			primary : "ui-icon-search"
		}
	});
	
	$(".add_group_button").button({
		icons : {
			primary : "ui-icon-plusthick"
		}
	});
	
	$(".delete_button").button();
});


</script>
</head>

<script language="javascript" type="text/javascript" src="functions.js"></script>

<body style="margin:0px" class="body">

<%
   if(Request("offset") <> "") then 
	offset = clng(Request("offset"))
   else 
	offset = 0
   end if	

   if(Request("sortby") <> "") then 
	sortby = Request("sortby")
   else 
	sortby = "name"
   end if	

   if(Request("gname") <> "") then 
	mygname = Request("gname")
   else 
	mygname = ""
   end if	
if (uid <>"") then
'counting pages to display
   if(Request("limit") <> "") then 
	limit = clng(Request("limit"))
   else 
	limit=50
   end if

	linkclick_str = LNG_GROUPS
	groups_arr = Array("checked","checked","checked","checked","checked","checked","checked")
	gid = 0
	gsendtoall = 0
	set RS = Conn.Execute("SELECT id, only_personal FROM users WHERE id="&uid&" AND role='E'")
	if not RS.EOF then
		gid = 1
		set rs_policy = Conn.Execute("SELECT p.id, p.type, pu.user_id FROM policy p INNER JOIN policy_editor pe ON p.id=pe.policy_id LEFT JOIN policy_user pu ON pu.policy_id = p.id AND pu.user_id = 0 WHERE pe.editor_id=" & uid)
		if not rs_policy.EOF then
			if rs_policy("type")="A" then 'can send to all
				gid = 0
			elseif not IsNull(rs_policy("user_id")) then
				gid = 0
				gsendtoall = 1
			end if
		end if
		if gid = 1 or gsendtoall = 1 then
			editor_id = RS("id")
			policy_ids = editorGetPolicyIds(editor_id)
			groups_arr = editorGetPolicyList(policy_ids, "groups_val")
			if groups_arr(2) = "" then
				linkclick_str = "not"
			end if
			group_ids=editorGetGroupIds(policy_ids)
			group_ids=replace(group_ids, "group_id", "groups.id")
			if group_ids="" then
				group_ids = "groups.id=0"
			end if
		end if
	end if
	RS.Close
	
	if(mygname<>"") then
		if(gid=0) then
			Set RS = Conn.Execute("SELECT COUNT(id) as mycnt FROM groups WHERE name LIKE N'%"&Replace(mygname, "'", "''")&"%'")
		else
			Set RS = Conn.Execute("SELECT COUNT(id) as mycnt FROM groups WHERE name LIKE N'%"&Replace(mygname, "'", "''")&"%' AND ("&group_ids&")")
		end if
	else
		if(gid=0) then
			Set RS = Conn.Execute("SELECT COUNT(id) as mycnt FROM groups")
		else
			Set RS = Conn.Execute("SELECT COUNT(id) as mycnt FROM groups WHERE ("&group_ids&")")
		end if
	end if
	cnt=0
	if(Not RS.EOF) then
		cnt=RS("mycnt")
	end if
	RS.Close
        j=cnt/limit
%>
<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	<tr>
	<td>
	<table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
		<tr>
		<td width=100% height=31 class="main_table_title"><a href="admin_groups.asp" class="header_title"><%=LNG_GROUPS %></a></td>
		</tr>
		<tr>
		<td class="main_table_body" height="100%">
		<% if(Request("err")="1") then Response.Write "<br><center><b><font color=""red"">Error: this special id already exists! Please try another one.</font></b></center>" end if %>
		<div style="margin:10px;">
<form name="search_users" action="admin_groups.asp" method="get"><table width="100%" border="0"><tr><td width="240">
<%=LNG_SEARCH_GROUPS %>: <input type="text" name="gname" value="<% response.write mygname %>"/></td><td  width="1%"><button type="submit" name="sub" class="search_button"><%=LNG_SEARCH %></button></td>
<%
if(mygname<>"") then
response.write "<td>"&LNG_YOUR_SEARCH_BY &" """&mygname&""" "&LNG_KEYWORD &":</td>"
end if
%><td align="right">

<%if groups_arr(0)="checked" then%>
<a href="edit_group.asp" class="add_group_button"><%=LNG_ADD_GROUP %></a>
<%end if%>
</td></tr></table>
</form> 


<%
if(cnt>0) then
	page="admin_groups.asp?gname=" & mygname & "&"
	name=LNG_GROUPS
	Response.Write make_pages(offset, cnt, limit, page, name, sortby) 

if (AD=0) then
	response.write "<br><A href='#' class='delete_button' onclick=""javascript: return LINKCLICK('"&linkclick_str&"');"">"&LNG_DELETE&"</a><br/><br>"
	response.write "<form action='delete_objects.asp' name='delete_objs' id='delete_objs' method='post'><input type='hidden' name='objType' value='groups'>"
end if


%>


		<table width="100%" height="100%" class="data_table" cellspacing="0" cellpadding="3">
		<tr class="data_table_title">
<% if (AD = 0) then %><td width="10"><input type='checkbox' name='select_all' id='select_all' onclick='javascript: select_all_objects();'></td><%end if%>

		<td class="table_title"><% response.write sorting(LNG_GROUP_NAME,"name", sortby, offset, limit, page) %></td>
		<td class="table_title"><% response.write sorting(LNG_USERS,"users_cnt", sortby, offset, limit, page) %></td>
		<td class="table_title"><% response.write sorting(LNG_GROUPS,"groups_cnt", sortby, offset, limit, page) %></td>
		<td class="table_title"><%=LNG_ACTIONS %></td>
				</tr>
<%
'show main table


	if(mygname<>"") then
		if(gid=0) then
			Set RS = Conn.Execute("SELECT groups.id as id, name, (SELECT COUNT(1) FROM users_groups ug WHERE groups.id=ug.group_id) as users_cnt, (SELECT COUNT(1) FROM groups_groups gg WHERE groups.id=gg.container_group_id) as groups_cnt FROM groups WHERE name LIKE N'%"&Replace(mygname, "'", "''")&"%' GROUP BY groups.id, name ORDER BY "&sortby)
		else
			Set RS = Conn.Execute("SELECT groups.id as id, name, (SELECT COUNT(1) FROM users_groups ug WHERE groups.id=ug.group_id) as users_cnt, (SELECT COUNT(1) FROM groups_groups gg WHERE groups.id=gg.container_group_id) as groups_cnt FROM groups WHERE name LIKE N'%"&Replace(mygname, "'", "''")&"%' AND ("&group_ids&") GROUP BY groups.id, name ORDER BY "&sortby)
		end if

	else
		if(gid=0) then
			Set RS = Conn.Execute("SELECT groups.id as id, name, (SELECT COUNT(1) FROM users_groups ug WHERE groups.id=ug.group_id) as users_cnt, (SELECT COUNT(1) FROM groups_groups gg WHERE groups.id=gg.container_group_id) as groups_cnt FROM groups GROUP BY groups.id, name ORDER BY "&sortby)
		else
			Set RS = Conn.Execute("SELECT groups.id as id, name, (SELECT COUNT(1) FROM users_groups ug WHERE groups.id=ug.group_id) as users_cnt, (SELECT COUNT(1) FROM groups_groups gg WHERE groups.id=gg.container_group_id) as groups_cnt FROM groups WHERE ("&group_ids&") GROUP BY groups.id, name ORDER BY "&sortby)
		end if		
	end if

	num=offset
	if(Not RS.EOF) then RS.Move(offset) end if
	Do While Not RS.EOF
		num=num+1
		if((offset+limit) < num) then Exit Do end if
		if(num > offset AND offset+limit >= num) then
		cnt1=0

		strObjectID=RS("id")

        	%>
		<tr>
<%if(AD=0) then %>
	<td><input type="checkbox" name="objects" value="<%=strObjectID%>"></td>
<%
end if%>

		<td><A href="#" onclick="window.open('view_reg_group.asp?id=<%=RS("id") %>','',350,350);">
			<%= RS("name") %></a>
		</td>
		<td><%= RS("users_cnt") %></td>
		<td><%= RS("groups_cnt") %></td>
		<td align="center">
		<A href="#" onclick="window.open('view_reg_group.asp?id=<%=RS("id") %>','',350,350);">
		<img src="images/action_icons/preview.png" alt="<%=LNG_VIEW_USERS_IN_GROUP %>" title="<%=LNG_VIEW_USERS_IN_GROUP %>" width="16" height="16" border="0" hspace=5></a>
		<%
		if groups_arr(1)="checked" then
		%>
		<a href="edit_group.asp?id=
		<% Response.Write RS("id") %>
		"><img src="images/action_icons/edit.png" alt="<%=LNG_EDIT_GROUP %>" title="<%=LNG_EDIT_GROUP %>" width="16" height="16" border="0" hspace=5></a>
		<%
		end if
		%>
		</td></tr>
		<%
		end if
	RS.MoveNext
	Loop
	RS.Close

		%>
                </table>
		<%
if (AD=0) then
	response.write "</form><br/><A href='#' class='delete_button' onclick=""javascript: return LINKCLICK('"&linkclick_str&"');"">"&LNG_DELETE&"</a><br><br>"
end if

'	response.write "Display per page: <a href='admin_groups.asp?offset="&offset&"&limit=10'>10</a> | <a href='admin_groups.asp?offset="&offset&"&limit=50'>50</a> | <a href='admin_groups.asp?offset="&offset&"&limit=100'>100</a> | <a href='admin_groups.asp?offset="&offset&"&limit=500'>500</a> | <a href='admin_groups.asp?offset="&offset&"&limit=1000'>1000</a>"
	Response.Write make_pages(offset, cnt, limit, page, name, sortby) 

else

Response.Write "<center><b>There are no groups.</b><br><br></center>"

end if
%>
 
		<img src="images/inv.gif" alt="" width="1" height="100%" border="0">
		</div>
		</td>
		</tr>
	</table>
	</td>
	</tr>
</table>

<%
else

  Response.Redirect "index.asp"

end if
%> 
</body>
</html>
<!-- #include file="db_conn_close.asp" -->