<!-- #include file="config.inc" -->
<!-- #include file="functions_inc.asp" -->
<!-- #include file="db_conn.asp" -->

<%
function deleteFile(filePath)
	dim fs
	Set fs=Server.CreateObject("Scripting.FileSystemObject")
	if fs.FileExists(filePath) then
	  fs.DeleteFile(filePath)
	end if
	set fs=nothing
end function

check_session()


objType=request("objType")
ids=request("objects")
old_ids  =request("objects") 
idsArray = Split(ids, ",")

if(ids<>"") then
	guids = replace(ids,", ","', '")
	guids = "('"&guids&"')"
	ids=replace(ids,","," OR id =")
	strDeleteFrom=""
end if


if(objType="campaigns") then
	if(ids<>"") then
		SQL = "DELETE FROM campaigns WHERE id="&ids
		Conn.Execute(SQL)
	end if
	Response.Redirect "campaign.aspx"
end if

if(objType="digsignlinks") then
	if(ids<>"") then
	
	    rmUser = Request("rm")
	    
	    set folderSet = Conn.Execute("SELECT name FROM digsign_links WHERE id="&ids)
	    
	    folderStr = ""
	    
	    do while not folderSet.EOF
	    
	        folderStr = folderStr & folderSet("name") & ","
	        folderSet.movenext
	    loop
	    
	    folderStr = Left(folderStr, len(folderStr) - 1)
	    'delete users
	    
	  if(rmUser <> "1") then  
	    SQL = "DELETE FROM users WHERE name IN (SELECT name FROM digsign_links WHERE id="&ids&")"
	    Conn.Execute(SQL)
	  end if
	  
	    SQL = "DELETE FROM digsign_links WHERE id="&ids
		Conn.Execute(SQL)
		
         Dim oXMLHTTP
          Dim strStatusTest

          Set oXMLHTTP = CreateObject("MSXML2.ServerXMLHTTP.3.0")

          oXMLHTTP.Open "GET", alerts_folder & "admin/DeleteDSignFolders.aspx?folders=" & folderStr , False
          oXMLHTTP.Send

		


	end if
	uid = Session("uid")
	Response.Redirect "DigSignLinks.aspx?uid="&uid
end if



if( objType = "tgroups") then

    if(ids<>"") then
    
         SQL = "DELETE FROM message_templates_groups WHERE id="&ids
         Conn.Execute(SQL)
         SQL = "DELETE FROM group_message_templates WHERE group_id="&ids
         Conn.Execute(SQL)
         'SQL = "SELECT DISTINCT policy_id"
         SQL = "DELETE FROM policy_tgroups WHERE tgroup_id="&ids
         Conn.Execute(SQL)
    end if
    
    Response.Redirect "MessageTemplatesGroups.aspx"

end if

if(objType="surveys") then

    if(ids<> "") then
	    Set RS = Conn.Execute("SELECT sender_id FROM surveys_main WHERE id=" & ids)
	    do while Not RS.EOF 

			    Conn.Execute("DELETE FROM alerts WHERE id = " & RS("sender_id"))

          '  MemCacheUpdate "Delete", idsArray
                for each alertId in idsArray
                   MemCacheUpdate "Delete", alertId
                 next
			    RS.MoveNext
	    loop

	    Conn.Execute("DELETE FROM surveys_answers_stat WHERE answer_id IN (SELECT id FROM surveys_answers WHERE question_id IN (SELECT id FROM surveys_questions WHERE survey_id=" & ids & "))")

	    Conn.Execute("DELETE FROM surveys_answers WHERE question_id IN (SELECT id FROM surveys_questions WHERE survey_id=" & ids & ")")

	    Conn.Execute("DELETE FROM surveys_answers WHERE survey_id=" & ids)

	    Conn.Execute("DELETE FROM surveys_custom_answers_stat WHERE question_id IN (SELECT id FROM surveys_questions WHERE survey_id=" & ids & ")")

	    Conn.Execute("DELETE FROM surveys_main WHERE id=" & ids)

	    Conn.Execute("DELETE FROM surveys_questions WHERE survey_id=" & ids)        
    end if
    Response.Redirect "surveys.asp"
end if

if(objType="RejectedAlerts") then
	if(ids<>"") then
		SQL = "DELETE FROM alerts WHERE id="&ids
		Conn.Execute(SQL)
	end if
	Response.Redirect "RejectedAlerts.aspx?uid=" & Request("userId")
end if

if(objType="ApprovalAlerts") then
	if(ids<>"") then
		SQL = "DELETE FROM alerts WHERE id="&ids
		Conn.Execute(SQL)
	end if
	Response.Redirect "approve_panel.aspx?uid=" & Request("userId")
end if


if(objType="banners") then
	if(ids<>"") then
		SQL = "DELETE FROM banners WHERE id="&ids
		Conn.Execute(SQL)
	end if
	Response.Redirect "WebpluginList.aspx"
end if

if(objType="feedback")then
	if(ids<>"") then
		SQL = "DELETE FROM feedbacks WHERE id="&ids
		Conn.Execute(SQL)
	end if
	Response.Redirect "feedback_show.asp"
end if

if(objType="color_codes")then
	if(ids<>"") then
		SQL = "DELETE FROM color_codes WHERE id IN "&guids
		Conn.Execute(SQL)
	end if
	Response.Redirect "color_codes.asp"
end if

if(objType="users") then 

if(ids<>"") then
  SQL = "DELETE FROM users WHERE id=" & ids
  Conn.Execute(SQL)
  user_ids=replace(ids," OR id ="," OR user_id =")
'groups
  SQL = "DELETE FROM users_groups WHERE user_id=" & user_ids
  Conn.Execute(SQL)
'statistics
  SQL = "DELETE FROM alerts_read WHERE user_id="  & user_ids
  Conn.Execute(SQL)

  SQL = "DELETE FROM alerts_received WHERE user_id="  & user_ids
  Conn.Execute(SQL)

  SQL = "DELETE FROM alerts_sent WHERE user_id="  & user_ids
  Conn.Execute(SQL)

  SQL = "DELETE FROM alerts_sent_stat WHERE user_id="  & user_ids
  Conn.Execute(SQL)

  SQL = "DELETE FROM surveys_answers_stat WHERE user_id="  & user_ids
  Conn.Execute(SQL)
  
  SQL = "DELETE FROM user_instances WHERE user_id="  & user_ids
  Conn.Execute(SQL)

end if

  Response.Redirect "users.asp"
end if

if(objType="groups") then 
if(ids<>"") then
  SQL = "DELETE FROM groups WHERE id=" & ids
  Conn.Execute(SQL)

  group_ids=replace(ids," OR id ="," OR group_id =")
  SQL2 = "DELETE FROM users_groups WHERE group_id=" & group_ids
  Conn.Execute(SQL2)

  SQL2 = "DELETE FROM alerts_group_stat WHERE group_id=" & group_ids
  Conn.Execute(SQL2)
  SQL2 = "DELETE FROM alerts_sent_group WHERE group_id=" & group_ids
  Conn.Execute(SQL2)

end if
  Response.Redirect "admin_groups.asp"
end if

if(objType="ip_groups") then 
	if(ids<>"") then
		Conn.Execute("DELETE FROM ip_groups WHERE id=" & ids)

		group_ids=replace(ids," OR id ="," OR group_id =")
		Conn.Execute("DELETE FROM ip_range_groups WHERE group_id=" & group_ids)
	end if
	Response.Redirect "admin_ip_groups.asp"
end if
if(objType="im") then
	if(ids<>"") then
		Set RSim = Conn.Execute("SELECT alert_id FROM instant_messages WHERE id='"&ids&"'")
        MemCacheUpdate "Delete", old_ids
		temp_id = RSim("alert_id")
		Conn.Execute("DELETE FROM instant_messages WHERE id='"&ids&"'")
		ids=temp_id
		objType="alerts"
	end if
end if
if(objType="alerts") then 
	if(ids<>"") then
		Conn.Execute("DELETE FROM alerts WHERE id=" & ids)
		Conn.Execute("DELETE FROM archive_alerts WHERE id=" & ids)

		alert_ids=replace(ids," OR id ="," OR alert_id =")
		Conn.Execute("DELETE FROM alerts_comp_stat WHERE alert_id=" & alert_ids)
		Conn.Execute("DELETE FROM alerts_group_stat WHERE alert_id=" & alert_ids)
		Conn.Execute("DELETE FROM alerts_ou_stat WHERE alert_id=" & alert_ids)
		Conn.Execute("DELETE FROM alerts_read WHERE alert_id=" & alert_ids)
		Conn.Execute("DELETE FROM alerts_received WHERE alert_id=" & alert_ids)
		Conn.Execute("DELETE FROM alerts_received_recur WHERE alert_id=" & alert_ids)
		Conn.Execute("DELETE FROM alerts_sent WHERE alert_id=" & alert_ids)
		Conn.Execute("DELETE FROM alerts_sent_comp WHERE alert_id=" & alert_ids)
		Conn.Execute("DELETE FROM alerts_sent_deskbar WHERE alert_id=" & alert_ids)
		Conn.Execute("DELETE FROM alerts_sent_group WHERE alert_id=" & alert_ids)
		Conn.Execute("DELETE FROM alerts_sent_iprange WHERE alert_id=" & alert_ids)
		Conn.Execute("DELETE FROM alerts_sent_stat WHERE alert_id=" & alert_ids)

		Conn.Execute("DELETE FROM archive_alerts_comp_stat WHERE alert_id=" & alert_ids)
		Conn.Execute("DELETE FROM archive_alerts_group_stat WHERE alert_id=" & alert_ids)
		Conn.Execute("DELETE FROM archive_alerts_read WHERE alert_id=" & alert_ids)
		Conn.Execute("DELETE FROM archive_alerts_received WHERE alert_id=" & alert_ids)
		Conn.Execute("DELETE FROM archive_alerts_sent WHERE alert_id=" & alert_ids)
		Conn.Execute("DELETE FROM archive_alerts_sent_comp WHERE alert_id=" & alert_ids)
		Conn.Execute("DELETE FROM archive_alerts_sent_group WHERE alert_id=" & alert_ids)
		Conn.Execute("DELETE FROM archive_alerts_sent_stat WHERE alert_id=" & alert_ids)
		
	'cache
		Conn.Execute("DELETE FROM [alerts_cache_users] WHERE alert_id = " & alert_ids)
		Conn.Execute("DELETE FROM [alerts_cache_comps] WHERE alert_id = " & alert_ids)
		Conn.Execute("DELETE FROM [alerts_cache_ips] WHERE alert_id = " & alert_ids)
		

         MemCacheUpdate "Delete", old_ids


	'child
		if Request("child") = "1" then
			Server.ScriptTimeout = 600
			Conn.CommandTimeout = 600

			parent_ids=replace(ids," OR id ="," OR parent_id =")
			
			Conn.Execute(_
			"IF OBJECT_ID('tempdb..#tempAlertsIds') IS NOT NULL DROP TABLE #tempAlertsIds" & vbCrLf &_
			"CREATE TABLE #tempAlertsIds (id BIGINT NOT NULL)" & vbCrLf &_
			"INSERT INTO #tempAlertsIds (id) (SELECT id FROM alerts WHERE parent_id = "& parent_ids & ") " & vbCrLf &_
			"BEGIN" & vbCrLf &_
			"	DELETE alerts_comp_stat FROM alerts_comp_stat a INNER JOIN #tempAlertsIds t ON a.alert_id = t.id" & vbCrLf &_
			"	DELETE alerts_group_stat FROM alerts_group_stat a INNER JOIN #tempAlertsIds t ON a.alert_id = t.id" & vbCrLf &_
			"	DELETE alerts_ou_stat FROM alerts_ou_stat a INNER JOIN #tempAlertsIds t ON a.alert_id = t.id" & vbCrLf &_
			"	DELETE alerts_read FROM alerts_read a INNER JOIN #tempAlertsIds t ON a.alert_id = t.id" & vbCrLf &_
			"	DELETE alerts_received FROM alerts_received a INNER JOIN #tempAlertsIds t ON a.alert_id = t.id" & vbCrLf &_
			"	DELETE alerts_received_recur FROM alerts_received_recur a INNER JOIN #tempAlertsIds t ON a.alert_id = t.id" & vbCrLf &_
			"	DELETE alerts_sent FROM alerts_sent a INNER JOIN #tempAlertsIds t ON a.alert_id = t.id" & vbCrLf &_
			"	DELETE alerts_sent_comp FROM alerts_sent_comp a INNER JOIN #tempAlertsIds t ON a.alert_id = t.id" & vbCrLf &_
			"	DELETE alerts_sent_deskbar FROM alerts_sent_deskbar a INNER JOIN #tempAlertsIds t ON a.alert_id = t.id" & vbCrLf &_
			"	DELETE alerts_sent_group FROM alerts_sent_group a INNER JOIN #tempAlertsIds t ON a.alert_id = t.id" & vbCrLf &_
			"	DELETE alerts_sent_iprange FROM alerts_sent_iprange a INNER JOIN #tempAlertsIds t ON a.alert_id = t.id" & vbCrLf &_
			"	DELETE alerts_sent_stat FROM alerts_sent_stat a INNER JOIN #tempAlertsIds t ON a.alert_id = t.id" & vbCrLf &_
			_
			"	DELETE archive_alerts_comp_stat FROM archive_alerts_comp_stat a INNER JOIN #tempAlertsIds t ON a.alert_id = t.id" & vbCrLf &_
			"	DELETE archive_alerts_group_stat FROM archive_alerts_group_stat a INNER JOIN #tempAlertsIds t ON a.alert_id = t.id" & vbCrLf &_
			"	DELETE archive_alerts_read FROM archive_alerts_read a INNER JOIN #tempAlertsIds t ON a.alert_id = t.id" & vbCrLf &_
			"	DELETE archive_alerts_received FROM archive_alerts_received a INNER JOIN #tempAlertsIds t ON a.alert_id = t.id" & vbCrLf &_
			"	DELETE archive_alerts_sent FROM archive_alerts_sent a INNER JOIN #tempAlertsIds t ON a.alert_id = t.id" & vbCrLf &_
			"	DELETE archive_alerts_sent_comp FROM archive_alerts_sent_comp a INNER JOIN #tempAlertsIds t ON a.alert_id = t.id" & vbCrLf &_
			"	DELETE archive_alerts_sent_group FROM archive_alerts_sent_group a INNER JOIN #tempAlertsIds t ON a.alert_id = t.id" & vbCrLf &_
			"	DELETE archive_alerts_sent_stat FROM archive_alerts_sent_stat a INNER JOIN #tempAlertsIds t ON a.alert_id = t.id" & vbCrLf &_
			_
			"	DELETE alerts_cache_users FROM alerts_cache_users a INNER JOIN #tempAlertsIds t ON a.alert_id = t.id" & vbCrLf &_
			"	DELETE alerts_cache_comps FROM alerts_cache_comps a INNER JOIN #tempAlertsIds t ON a.alert_id = t.id" & vbCrLf &_
			"	DELETE alerts_cache_ips FROM alerts_cache_ips a INNER JOIN #tempAlertsIds t ON a.alert_id = t.id" & vbCrLf &_
			"END")
			
			Conn.Execute("DELETE FROM alerts WHERE parent_id = " & parent_ids)
			Conn.Execute("DELETE FROM archive_alerts WHERE parent_id = " & parent_ids)
		end if
	end if
	if(Request("back")="sent") then
		Response.Redirect "sent.asp"
	end if
	if(Request("back")="draft") then
		Response.Redirect "draft.asp"
	end if
	if(Request("back")="draftvideo") then
		Response.Redirect "draft_video.asp"
	end if
	if(Request("back")="im") then
		Response.Redirect "instant_messages.asp"
	end if
	if(Request("back")="im_widget") then
		Response.Redirect "widget_instant_send.asp"
	end if

   if(Request("back")="video") then
		Response.Redirect "sent_video.asp"
	end if
	
	if(Request("back") = "campaign") then
	    Response.Redirect "campaign_center.asp?campaignid=" & Request("campaignid")
	end if
	Response.Redirect Request("back")
end if

if(objType="surveys") then 
	if(ids<>"") then
		alert_ids = ""
		Set RSAlert = Conn.Execute("SELECT sender_id FROM surveys_main WHERE id=" & ids)
		Do While Not RSAlert.EOF
			if alert_ids <> "" then
				alert_ids = alert_ids & "&objects="
			end if
			alert_ids = alert_ids & RSAlert("sender_id")
			RSAlert.MoveNext
		Loop

		SQL = "DELETE FROM surveys_main WHERE id=" & ids
		Conn.Execute(SQL)

		survey_ids=replace(ids," OR id ="," OR survey_id =")

		SQL2 = "DELETE FROM surveys_answers WHERE survey_id=" & survey_ids
		Conn.Execute(SQL2)
		
		Response.Redirect "delete_objects.asp?objType=alerts&back=surveys_archived.asp&objects="&alert_ids
	end if
	Response.Redirect "surveys_archived.asp"
end if

if(objType="alert_template_param_values") then 
	if(ids<>"") then
		SQL = "DELETE FROM alert_param_selects WHERE id=" & ids
		Conn.Execute(SQL)
		paramId = Request("param_id")
		Response.Redirect "alert_template_params_edit.asp?id="&paramId
	end if
end if

if(objType="editors") then
if(ids<>"") then
  SQL = "DELETE FROM users WHERE id=" & ids
  Conn.Execute(SQL)

  editor_ids=replace(ids," OR id ="," OR editor_id =")

  SQL2 = "DELETE FROM policy_editor WHERE editor_id=" & editor_ids
  Conn.Execute(SQL2)

end if
  Response.Redirect "admin_admin.asp"
end if

if(objType="templates") then
if(ids<>"") then
  SQL = "DELETE FROM templates WHERE id=" & ids
  Conn.Execute(SQL)
end if
  Response.Redirect "templates.asp"
end if

if(objType="skins") then 
	if(ids<>"") then
		SQL = "SELECT file_name FROM alert_captions WHERE id IN " & guids
		Response.write SQL
		Set RS = Conn.Execute(SQL)

		SQL = "DELETE FROM alert_captions WHERE id IN " & guids
		Conn.Execute(SQL)
		
		Do While Not RS.EOF
			deleteFile(Server.MapPath("preview\skin\version\"&RS("file_name")))
			RS.MoveNext
		Loop
		RS.Close
	end if
	Response.Redirect "skins.asp"
end if


if(objType="text_templates") then
if(ids<>"") then
	SQL = "DELETE FROM text_templates WHERE id=" & ids
	Conn.Execute(SQL)
end if
  Response.Redirect "text_templates.asp"
end if

if(objType="policies") then
if(ids<>"") then
	SQL = "DELETE FROM policy WHERE id=" & ids
	Conn.Execute(SQL)

	policy_ids=replace(ids," OR id ="," OR policy_id =")

	SQL2 = "DELETE FROM policy_editor WHERE policy_id=" & policy_ids
	Conn.Execute(SQL2)

	SQL2 = "DELETE FROM policy_group WHERE policy_id=" & policy_ids
	Conn.Execute(SQL2)

	SQL2 = "DELETE FROM policy_ou WHERE policy_id=" & policy_ids
	Conn.Execute(SQL2)

	SQL2 = "DELETE FROM policy_user WHERE policy_id=" & policy_ids
	Conn.Execute(SQL2)

	SQL2 = "DELETE FROM policy_computer WHERE policy_id=" & policy_ids
	Conn.Execute(SQL2)

	SQL2 = "DELETE FROM policy_list WHERE policy_id=" & policy_ids
	Conn.Execute(SQL2)
end if

	Response.Redirect "policy_list.asp"
end if

%>
<!-- #include file="db_conn_close.asp" -->