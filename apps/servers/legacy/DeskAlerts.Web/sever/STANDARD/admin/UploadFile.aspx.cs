﻿using System;
using System.Diagnostics;
using System.IO;
using DeskAlertsDotNet.Pages;
using DeskAlertsDotNet.Parameters;

namespace DeskAlerts.Server.sever.STANDARD.admin
{
    public partial class UploadFile : DeskAlertsBasePage
    {
        private const string VideoOutputFormat = ".mp4";
        protected override void OnLoad(EventArgs e)
        {
            try
            {
                base.OnLoad(e);

                var message = string.Empty;

                if (Request.Files.Count == 0)
                {
                    Response.WriteLine("No files entered!");
                    Response.End();
                }
                else
                {
                    var uploadedContentType = Request["uploadedContentType"];

                    var uploadedFile = Request.Files[0];

                    var savePath = Config.Data.GetString("alertsFolder") + "\\admin\\images\\upload" +
                                      uploadedContentType;

                    var shortName = Path.GetFileName(uploadedFile.FileName).Replace(" ", "_");
                    var serverPath = savePath + "\\" + shortName;

                    var linkToFile = Config.Data.GetString("alertsDir") + "\\admin\\images\\upload" +
                                        uploadedContentType + "\\" + shortName;
                    var existancePath = serverPath;
                    var prefix = 1;

                    while (File.Exists(existancePath))
                    {
                        existancePath = savePath + "\\(" + prefix + ") " + shortName;
                        serverPath = savePath + "\\(" + prefix + ") " + shortName;
                        ++prefix;
                    }

                    uploadedFile.SaveAs(serverPath);

                    if (uploadedContentType == "_video" && uploadedFile.ContentType != "video/mp4")
                    {
                        serverPath = ConvertVideo(serverPath);
                    }

                    message = "File Uploaded: ";

                    if (!string.IsNullOrEmpty(Request["can_select"]))
                    {
                        message += @"<a href=""#"" onclick=""selectFile('" + serverPath + "', '" + linkToFile + @"')"">" +
                                   shortName + "</a>";
                    }
                    else
                    {
                        message += shortName;
                    }
                }

                if (Request.UrlReferrer != null && !string.IsNullOrEmpty(Request.UrlReferrer.AbsoluteUri))
                {
                    Response.Redirect(Request.UrlReferrer.AbsoluteUri, true);
                }
                else if (string.IsNullOrEmpty(Request["return_page"]))
                {
                    Response.Redirect("UploadForm.aspx", true);

                }
            }
            catch (Exception ex)
            {
                Logger.Debug(ex);

                Response.WriteLine("No files entered!");
                Response.End();
            }
        }

        public string ConvertVideo(string uploadedVideoPath)
        {
            const string ffmpegX86Path = "ffmpeg\\x86\\ffmpeg.exe";
            const string ffmpegX64Path = "ffmpeg\\x64\\ffmpeg.exe";
            const string videoEncoder = "-vcodec libx264";
            const string audioEncoder = "-acodec libvo_aacenc";

            try
            {
                string ffmpegFilePath;

                if (IntPtr.Size == 4) //check if is 64 bit system
                {
                    ffmpegFilePath =
                        Path.GetFullPath(Path.Combine(
                            uploadedVideoPath.Remove(
                                uploadedVideoPath.Length - Path.GetFileName(uploadedVideoPath).Length,
                                Path.GetFileName(uploadedVideoPath).Length),
                            @"..\..\")) + ffmpegX86Path;
                }
                else
                {
                    ffmpegFilePath =
                        Path.GetFullPath(Path.Combine(
                            uploadedVideoPath.Remove(uploadedVideoPath.Length - Path.GetFileName(uploadedVideoPath).Length, Path.GetFileName(uploadedVideoPath).Length),
                            @"..\..\")) + ffmpegX64Path;
                }

                var temporaryVideoPath = Path.GetDirectoryName(uploadedVideoPath) + "\\" + Path.GetFileNameWithoutExtension(uploadedVideoPath) + VideoOutputFormat;

                if (File.Exists(temporaryVideoPath))
                {
                    File.Delete(temporaryVideoPath);
                }

                var processInfo = new ProcessStartInfo(ffmpegFilePath,
                    " -i " + uploadedVideoPath +
                    " " + videoEncoder +
                    " " + audioEncoder +
                    " " + temporaryVideoPath)
                {
                    UseShellExecute = false,
                    CreateNoWindow = true,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true
                };

                try
                {
                    var process = Process.Start(processInfo);

                    if (process != null)
                    {
                        process.StandardError.ReadToEnd();
                        process.WaitForExit();
                        process.Close();
                    }

                    File.Delete(uploadedVideoPath);
                }
                catch (Exception ex)
                {
                    Logger.Debug(ex);
                    throw;
                }

                var convertedVideoPath = Path.GetDirectoryName(uploadedVideoPath) + "\\" + Path.GetFileNameWithoutExtension(uploadedVideoPath) + VideoOutputFormat;

                return convertedVideoPath;
            }
            catch (Exception ex)
            {
                Logger.Debug(ex);
                throw;
            }
        }
    }
}