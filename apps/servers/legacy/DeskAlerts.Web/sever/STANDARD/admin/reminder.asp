<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Maintenance Expiration Reminder</title>
  <!--link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"-->
  <link href="css/style.css" rel="stylesheet" type="text/css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script>
  function check_form()
  {
		
		plan_choice=document.getElementsByName('plan_choice');
		for(var i=0;i<plan_choice.length;i++)
		{
			if(plan_choice.item(i).checked==true)
			{
				plan=plan_choice.item(i).value;
			}
		}
		if(plan.length!=0)
		{
		if(plan=='noplan')
		{
			//message='Thank you. I do not need support and upgrades for DeskAlerts';
			//alert(document.getElementById(plan+'_desc_plan').innerHTML);
			message=document.getElementById(plan+'_desc_plan').innerHTML;
			message+="%0A";
			message+="Comments:";
			message+="%0A";
			//if(document.getElementById('comments_message').style.display)
			//{
				message+=document.getElementById('comments_message').value;
			//}
		}
		else
		{
			//alert(plan + document.getElementById(plan+'_desc_plan').innerHTML+ "-" + document.getElementById(plan+'_support').checked + "-" + document.getElementById(plan+'_desc').innerHTML); 
			if(document.getElementById(plan+'_support').checked==true)
			{
				status='Yes';
			}
			else
			{
				status='No';
			}
			message=document.getElementById(plan+'_desc_plan').innerHTML;
			message+="%0A";
			message+=document.getElementById(plan+'_desc').innerHTML+" : "+status;
			//alert(document.getElementById(plan+'_desc_plan').innerHTML+"&lt;br/&gt;"+document.getElementById(plan+'_desc').innerHTML+" : "+status);
			
			//message
		}
		document.getElementById('email_us').href='mailto:sales@deskalerts.com?Subject=DeskAlerts Annual Maintenance&body='+message;
		}
		
		
  }
  
  $(function() {
  
	/*$("#close_maintenance_reminder").button();
	$("#close_maintenance_reminder").click(function(){
	$("#maintenance_reminder").dialog("close");}
	);*/
	
	
	
	/*$("#close_button").click(function() {
		    if ($("#dontshowTour").is(':checked')) {
		        dontShowTour = 1;
		        cookieObj.dontShowTour = dontShowTour;
		        var cookieStr = JSON.stringify(cookieObj);
		        $.cookies.set('deskalerts', cookieStr);
		    }
		    $('#quick_tour').dialog('destroy');

		});
		
		*/
    /*$( "#dialog-modal" ).dialog({
      height: 380,
      width: 560,
	  draggable: true,
	  resizable: false,
	  modal: true,
	  show:
	  {
		  effect: "fade",
		  duration: 300
	  }
	  /*,
	  
	  buttons:{
	  
	  "Close": function() { 
	  if ($("#maintenance_checkbox").is(':checked'))
	  {
				document.cookie="maintenance_checkbox=1";
				//alert();
			 //$.cookie("maintenance_reminder_checkbox", 1);
			
			  
	  }
	  $( this ).dialog( "close" );
	  /*if ($("#maintenance_checkbox").is(':checked'))
		{
			
			$.cookie("maintenance_reminder_checkbox", 1, { expires : 10 });
			
		}*/
	  //$( this ).dialog( "close" );

	  //}
	  //}
	  
    //});
	
	//$("#cookie").html(function(){$.cookie("maintenance_reminder_checkbox");});
	
	/*$.ajax({
	url: 'maintenance_rem.asp',
	success: function(data) {
    $('#days').html(data);
	}
	});*/
	
	
	
	
	
  });
  </script>

</head>
<body>
 

  <div style=''>
  <br/>
  <!--p style='text-align:center;'>Prior to the expiration contract's date:</p-->
  <!--p id='days' style='text-align:center;'></p-->
  <!--p id='days' style='text-align:center;font-size:20px;font-family: Geneva, Arial, Helvetica, sans-serif;'-->
  <%
'cookie=request.cookies("maintenance_checkbox")
Set RS = Conn.Execute("select datediff(day, getdate(), v.contract_date) as days, v.contract_date as contract_date,Convert(varchar,v.contract_date,105) as contract_date2,SUBSTRING ( version ,0 , 2 ) as version from version v")

 if(Not RS.EOF) then  
  'Dim b(,) As String = {{"days", RS("days")}, {"contract_date", RS("contract_date")}}
  'arr_maintenance=Array(RS("days"),RS("contract_date"))
  
  days=RS("days")
  contract_day=RS("contract_date")
  contract_day2=RS("contract_date2")
  version=RS("version")
 end if

 RS.Close
 

'response.write("<table>")
'response.write("<tr colspan=2>Prior to the expiration contract's date:</tr>")
'response.write("<tr><td>Contract' date was:</td><td><b>"&contract_day2&"</b></td></tr>")
'response.write("<tr colspan=2>Please, email to your sales manager <a href='mailto:sales@deskalerts.com?Subject=MaintenanceExpirationReminder' target='_top'><b><i>sales@deskalerts.com</i></b></a> for details elaboration!</tr>")
'response.write("</table>")
if (request("action") <> "licens") then
    if(days>0) then%>
    <div style='width:100%;height:auto;clear:both;'><h1>DeskAlerts V<b><%=version%></b> annual maintenance expires in <font style='color:red;'><%=days%></font> days (<%=contract_day2%>)</h1></div>
    <!--response.write("<table style='widht:100%;' colspan=10 ><tr>")
    response.write("<td colspan='2' style='padding:5px;'>DeskAlerts V <b>"&version&"</b>  annual maintenance expires on "&days&" days ("&contract_day2&") </td></tr></table>")-->

    <div style='width:100%;height:auto;clear:both;'>
    <table cellspacing="5" cellpadding="8">

    <tr><td colspan=4>&nbsp;&nbsp;To prolong annual maintenance please choose one of the Plans:</td></tr>

    <tr id='block1' onMouseOver='this.style.backgroundColor="#f0f0f1"; document.getElementById("block2").style.backgroundColor="#f0f0f1";document.getElementById("block3").style.backgroundColor="#f0f0f1";' onMouseOut='this.style.backgroundColor="#FFF"; document.getElementById("block2").style.backgroundColor="#FFF";document.getElementById("block3").style.backgroundColor="#FFF";'><td><input onClick="document.getElementById('message_div').style.display='none';document.getElementById('plan2_support').disabled=true;document.getElementById('plan1_support').disabled=false;" type='radio' name='plan_choice' value='plan1'></input></td><td id='plan1_desc_plan'>Plan 1 (60% of DeskAlerts package total cost)</td><td style='text-align:center;'>1)</td><td>Upgrades to the latest release of the latest Version of DeskAlerts</td></tr>

    <tr id='block2' onMouseOver='this.style.backgroundColor="#f0f0f1"; document.getElementById("block1").style.backgroundColor="#f0f0f1";document.getElementById("block3").style.backgroundColor="#f0f0f1";' onMouseOut='this.style.backgroundColor="#FFF"; document.getElementById("block1").style.backgroundColor="#FFF";document.getElementById("block3").style.backgroundColor="#FFF";'><td></td><td></td><td style='text-align:center;'>2)</td><td>Support (Level 1)</td></tr>

    <tr id='block3' onMouseOver='this.style.backgroundColor="#f0f0f1"; document.getElementById("block2").style.backgroundColor="#f0f0f1";document.getElementById("block1").style.backgroundColor="#f0f0f1";' onMouseOut='this.style.backgroundColor="#FFF"; document.getElementById("block2").style.backgroundColor="#FFF";document.getElementById("block1").style.backgroundColor="#FFF";'><td style='border-bottom:1px solid #f0f0f1;'></td><td style='border-bottom:1px solid #f0f0f1;'></td><td style='text-align:center;border-bottom:1px solid #f0f0f1;'><input disabled id='plan1_support' type='checkbox' name=''></input></td><td id='plan1_desc' style='border-bottom:1px solid #f0f0f1;'>Support block, 20 hours included (Level 2) $2000</td></tr>

    <tr id='block4' onMouseOver='this.style.backgroundColor="#f0f0f1"; document.getElementById("block5").style.backgroundColor="#f0f0f1";document.getElementById("block6").style.backgroundColor="#f0f0f1";' onMouseOut='this.style.backgroundColor="#FFF"; document.getElementById("block5").style.backgroundColor="#FFF";document.getElementById("block6").style.backgroundColor="#FFF";'><td><input  onClick="document.getElementById('message_div').style.display='none';document.getElementById('plan2_support').disabled=false;document.getElementById('plan1_support').disabled=true;" type='radio' name='plan_choice' value='plan2'></input></td><td id='plan2_desc_plan'>Plan 2 (30% of DeskAlerts package total cost)</td><td style='text-align:center;'>1)</td><td>Upgrades to the latest release of the installed Version of DeskAlerts</td></tr>

    <tr id='block5' onMouseOver='this.style.backgroundColor="#f0f0f1"; document.getElementById("block4").style.backgroundColor="#f0f0f1";document.getElementById("block6").style.backgroundColor="#f0f0f1";' onMouseOut='this.style.backgroundColor="#FFF"; document.getElementById("block4").style.backgroundColor="#FFF";document.getElementById("block6").style.backgroundColor="#FFF";'><td></td><td></td><td style='text-align:center;'>2)</td><td>Support (Level 1)</td></tr>
    <tr id='block6' onMouseOver='this.style.backgroundColor="#f0f0f1"; document.getElementById("block4").style.backgroundColor="#f0f0f1";document.getElementById("block5").style.backgroundColor="#f0f0f1";' onMouseOut='this.style.backgroundColor="#FFF"; document.getElementById("block4").style.backgroundColor="#FFF";document.getElementById("block5").style.backgroundColor="#FFF";'  style='border-bottom:1px solid #f0f0f1;'><td></td><td></td><td style='text-align:center;'><input disabled id='plan2_support' type='checkbox' name='support_plan1'> </input></td><td id='plan2_desc'>Support block, 20 hours included (Level 2) $2000</td></tr>

    <tr border="1" id='block7' onMouseOver='this.style.backgroundColor="#f0f0f1";' onMouseOut='this.style.backgroundColor="#FFF";'><td style='border-top:1px solid #f0f0f1;'><input onClick="document.getElementById('message_div').style.display='inline';document.getElementById('plan1_support').disabled=true;document.getElementById('plan2_support').disabled=true;" type='radio' name='plan_choice' value='noplan'></input></td><td id='noplan_desc_plan' style='border-top:1px solid #f0f0f1;'>Thank you. I do not need support and upgrades for DeskAlerts</td><td style='border-top:1px solid #f0f0f1;'></td><td style='text-align:center;border-top:1px solid #f0f0f1;'><!--div style='width:50%;height:40px;background-color:red;text-align:center;'>Email us</div--></td></tr>
    </table>
    </div>
    <div id="message_div" style='clear:both;display:none;margin-top:5px;'>
    <textarea id="comments_message" placeholder="Please, write your comments here..." style='width:100%;height:100px !important;max-height:100px !important;width:770px !important;max-width:770px !important;'></textarea>
    </div>


    <!--response.write("</tr><tr>")
    response.write("<td style='padding:5px;'>Contract' date was</td>")
    response.write("<td style='text-align:right;padding:5px;'>"&contract_day2&"</td></tr>")
    response.write("<tr><td colspan='2' style='padding:5px;'>Please, email to your sales manager <a href='mailto:sales@deskalerts.com?Subject=MaintenanceExpirationReminder' target='_top'><b><i>sales@deskalerts.com</i></b></a> for details elaboration!</td></tr>")
    response.write ("</table>")

    response.write("Prior to the expiration contract's date:<br><br/>")
    response.write("<b>"&days&"</b> days ("&contract_day2&")<br/><br/>")
    response.write("Please, email to your sales manager <a href='mailto:sales@deskalerts.com?Subject=MaintenanceExpirationReminder' target='_top'><b><i>sales@deskalerts.com</i></b></a> for details elaboration!")-->

    <% else%>
    <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
    <div style='width:100%;'><h1>DeskAlerts V<%=version%>  annual maintenance expired (<b><font style='color:red;'><%=contract_day2%></font></b>)<h1></div>
    <div style='width:100%;text-align:center;'>
    To prolong annual maintenance please email us <a href='mailto:sales@deskalerts.com?Subject=MaintenanceExpirationReminder' target='_top'><b><i>sales@deskalerts.com</i></b></a>
    </div>
    <!--response.write(contract_day2)

    response.write("<table style='widht:100%;'><tr>")
    response.write("<td colspan='2' style='padding:5px;'><b>Support period ended</b>. Contract date was: "&contract_day2&"</td></tr>")
    'response.write("</tr><tr>")
    'response.write("<td style='padding:5px;'>Contract' date was</td>")
    'response.write("<td style='text-align:right;padding:5px;'>"&contract_day2&"</td></tr>")
    response.write("<tr><td colspan='2' style='padding:5px;'>Please, email to your sales manager <a href='mailto:sales@deskalerts.com?Subject=MaintenanceExpirationReminder' target='_top'><b><i>sales@deskalerts.com</i></b></a> for details elaboration!</td></tr>")
    response.write ("</table>")

    'response.write("Support period ended<br/><br/>")
    'response.write("Contract' date was: <b>"&contract_day2&"</b><br/><br/>")
    'response.write("Please, email to your sales manager <a href='mailto:sales@deskalerts.com?Subject=MaintenanceExpirationReminder' target='_top'><b><i>sales@deskalerts.com</i></b></a> for details elaboration!")-->
    <%end if%>
    <!--/p-->
      <!--p style='text-align:center;'>Please, email to your sales manager for details elaboration!</p-->
      <p id='cookie'></p>
  
  
		    <%
		    action=request.QueryString("action")
		    if(action="button") then%>
		    <%else%>
	      <!--div style='float:left;clear:both;' id='div_checkbox'>
		      <input type='checkbox' name='maintenance_reminder_checkbox' id='maintenance_checkbox'>
		      <label for='maintenance_reminder_checkbox'>Do not show me again</label>
	      </div-->
	      <div>
	      <div style='position:absolute;bottom:0;right:0;margin-right:10px;margin-bottom:5px;' id='div_checkbox'>
	      <label for='maintenance_reminder_checkbox'>Do not show again</label>
	      <input type='checkbox' name='maintenance_reminder_checkbox' id='maintenance_checkbox' checked>
	      </div>
	      <%end if%>
	      <!--div style='float:right;'>
			    <button id="close_maintenance_reminder">Close</button>
	      </div-->
	      <%if(days>0) then%>
	      <div style='position:absolute;bottom:0;left:0;margin-bottom:15px;margin-left:10px;'>
	      <div style='clear:both;'><a href='#' onClick='check_form();' style='text-decoration:none;' id='email_us'><div style='background-color:#f0f0f1;padding-left:10px;padding-right:10px;padding-top:5px;padding-bottom:5px;border:1px solid gray;width:110px;text-align:center;float:left;'><b>Submit & E-mail</b></div></a><div style='float:left;padding-left:5px;padding-top:5px;'>Your request will be forwarded to DeskALerts team, you will be contacted within 24 hours</div></div>
	      <div style='clear:both;padding-top:10px;width:620px;padding-bottom:5px;'><font style='font-size:11px;color:red;'>*If you have any trouble opening the email client please send your request to prolong the maintainance to <font style='text-decoration:underline;color:black;'>sales@deskalerts.com</font></font></div>
	      <%end if%>
<%
else
    Set RS = Conn.Execute("DECLARE @from_date DATETIME SET @from_date = DATEADD(day, -1 , GETDATE()) SELECT COUNT(1) as count FROM (SELECT id FROM users WITH (READPAST) WHERE role = 'U' and last_request > @from_date ) t")

    if(Not RS.EOF) then  
        curLicenses = RS("count")
    end if

    RS.Close 

    Set RS = Conn.Execute("SELECT paramValue FROM parameters WHERE paramName='licensesReminderCriticalPoint'")

    if(Not RS.EOF) then  
        licensesReminderCriticalPoint = RS("paramValue")
    end if

    RS.Close 
%>
    <div style='width:100%;height:auto;clear:both;text-align:center;'><h1>You used <span style='color:red;'><%=curLicenses%></span> of licenses!</h1></div>
    <div style='width:100%;text-align:center;font-size:20px;'>
        <span>
            To buy more licenses please email us 
            <a href='mailto:sales@deskalerts.com?Subject=MaintenanceExpirationReminder' target='_top'><b><i>sales@deskalerts.com</i></b></a>
        </span>
    </div>
    <div style="padding:20px 0; text-align:center;font-size: 20px;">
        <span>We remind you again at </span>
        <select id="licensesReminderCriticalPointSelect" style="margin:0 10px;">
            <option value="20">20%</option>
            <option value="10">10%</option>
            <option value="5">5%</option>
        </select>
        <span> of used licenses.</span>
    </div>
    <div style='clear:both;padding-top:10px;width:620px;padding-bottom:5px;position:absolute;bottom:0;left:0;margin-right:10px;margin-bottom:5px;'>
        <font style='font-size:11px;color:red;'>
            *If you have any trouble opening the email client please send your request to buy more licenses to 
            <font style='text-decoration:underline;color:black;'>sales@deskalerts.com</font>
        </font>
    </div>
    <div style='position:absolute;bottom:0;right:0;margin-right:10px;margin-bottom:5px;' id='div_checkbox'>
	    <label for='licenses_reminder_checkbox'>Never show this reminder again</label>
	    <input type='checkbox' name='licenses_reminder_checkbox' id='licenses_reminder_checkbox' checked>
	</div>
<%end if%>
	  <div>
	  </div>
	  </div>
	  <input type="hidden" id="userChoosenLicensesReminderCriticalPoint" value="0">
</body>
</html>
<!-- #include file="db_conn_close.asp" -->
