﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DefaultSettings.aspx.cs" Inherits="DeskAlertsDotNet.Pages.DefaultSettings" %>

<%@ Import Namespace="DeskAlertsDotNet.Parameters" %>
<%@ Import Namespace="Resources" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link href="css/style9.css" rel="stylesheet" type="text/css">

    <link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
    <script>

    function check_fullscreen() {
        var enabled;
        if ($("#fullscreen").length > 0) {
            if ($("input[name='ConfPopupType']:checked").val() == "F") {
                enabled = true;
            }
            else {
                enabled = false;
            }
            $("#position_fieldset").prop('disabled', enabled);
            $("#ticker_position_fieldset").prop('disabled', enabled);
            $("#al_width").prop('disabled', enabled);
            $("#al_height").prop('disabled', enabled);
        }
    }

    function check_blogpost() {
        var enabled = document.getElementById('blogPost').checked;
        if (enabled) {
            $("#ConfScheduleByDefault").prop("checked", false);
        }
        $("#ConfScheduleByDefault").prop("disabled", enabled);
        if (!$("#ConfScheduleByDefault").prop("checked")) {
            $("#lifetime").prop("disabled", enabled);
            $("#lifetime_factor").prop("disabled", enabled);
        }
    }

    function check_schedule() {
        var enabled = document.getElementById('ConfScheduleByDefault').checked;
        if (enabled) {
            $("#ConfPostToBlogByDefault").prop("checked", false);
        }

        $("#ConfPostToBlogByDefault").prop("disabled", enabled);
        if (!$("#ConfPostToBlogByDefault").prop("checked")) {
            $("#lifetime").prop("disabled", enabled);
            $("#lifetime_factor").prop("disabled", enabled);
        }
    }

    function check_rsvp() {
        if ($("input[name='ConfDesktopAlertType']:checked").val() == 'R') {
            $('#ConfAcknowledgementByDefault').attr({ 'disabled': true, 'checked': false });
        } else {
            $('#ConfAcknowledgementByDefault').prop('disabled', false);
        }
        check_acknowledgement();
    }

    function check_acknowledgement() {
        if ($("#ConfAutocloseByDefault").is(":checked")) {
            $('#ConfAutocloseValue').prop('disabled', false);
            $('#ConfAllowManualCloseByDefault').prop('disabled', false);
            $('#ConfAcknowledgementByDefault').attr({ 'disabled': true, 'checked': false });
            $('#ConfUnobtrusiveByDefault').attr({ 'disabled': true, 'checked': false });
        } else if ($("#ConfAcknowledgementByDefault").is(":checked")) {
            $('#ConfAutocloseByDefault').attr({ 'disabled': true, 'checked': false });
            $('#ConfUnobtrusiveByDefault').attr({ 'disabled': true, 'checked': false });
            $('#ConfAllowManualCloseByDefault').attr({ 'disabled': true, 'checked': false });
            $('#ConfAutocloseValue').prop('disabled', true);
        } else if ($("#ConfUnobtrusiveByDefault").is(":checked")) {
            $('#ConfAcknowledgementByDefault').attr({ 'disabled': true, 'checked': false });
            $('#ConfAutocloseByDefault').attr({ 'disabled': true, 'checked': false });
            $('#ConfAllowManualCloseByDefault').attr({ 'disabled': true, 'checked': false });
            $('#ConfAutocloseValue').prop('disabled', true);
        } else {
            $('#ConfAllowManualCloseByDefault').attr({ 'disabled': true, 'checked': false });
            $('#ConfAutocloseValue').prop('disabled', true);
            $('#ConfAutocloseByDefault').prop('disabled', false);
            $('#ConfAcknowledgementByDefault').prop('disabled', false);
            $('#ConfUnobtrusiveByDefault').prop('disabled', false);
        }
    }

    function check_AlertType() {
        var alertByDefault = $("#ConfDesktopAlertByDefaultField");
        if(alertByDefault.prop( "checked" )){
            alertByDefault.val("1");
            $(".useDesktopAlert").show();
        }
        else{
            alertByDefault.val("0");
            $(".useDesktopAlert").hide();
        }
    }

    $(document)
        .ready(function () {
            check_AlertType();

            $("#accordion")
                .accordion({
                    heightStyle: "content",
                    collapsible: true,
                    active: false
                });

            $("#lifetime").change(function() {
                CalculateLifetime("lifetime", "lifetimeFactor", "ConfMessageExpire");
            });

            $("#lifetimeFactor").change(function () {
                CalculateLifetime("lifetime", "lifetimeFactor", "ConfMessageExpire");
            });

            $("#rssLifetime").change(function () {
                CalculateLifetime("rssLifetime", "rssLifetimeFactor", "ConfRSSMessageExpire");
            });

            $("#rssLifetimeFactor").change(function () {
                CalculateLifetime("rssLifetime", "rssLifetimeFactor", "ConfRSSMessageExpire");
            });


        $(".cancel_button").button();
        $(".save_button").button();
        $("#ConfPostToBlogByDefault").change(function () { check_blogpost(); });
        $("#ConfScheduleByDefault").change(function () { check_schedule(); });
        $("#ConfAcknowledgementByDefault").change(function () { check_acknowledgement(); });
        $("#ConfAutocloseByDefault").change(function () { check_acknowledgement(); });
        $("#ConfUnobtrusiveByDefault").change(function () { check_acknowledgement(); });
        /*$("input[name='ConfPopupType']").change(function() { check_fullscreen(); });*/
        $("input[name='ConfDesktopAlertType']").change(function() { check_rsvp(); });
        if ($("#ConfPostToBlogByDefault").length > 0) { check_blogpost(); }
        $("#ConfDesktopAlertByDefaultField").change(function () { check_AlertType(); });
        check_schedule();
        check_rsvp();
        /*check_fullscreen();*/
    });


    </script>
    <script language="javascript">
    function check_value() {

        var al_width = document.getElementById('al_width').value;
        var al_height = document.getElementById('al_height').value;
        var size_message = document.getElementById('size_message');
        var fullscreen = document.getElementById('ConfPopupTypeF').checked;

        if ((al_width > 1024 && al_height > 1080) & !fullscreen) {
            size_message.innerHTML = '<font color="red"><%=resources.LNG_RESOLUTION_DESC1 %></font>';
            return;
        }

        if ((al_width < 340 || al_height < 290) & !fullscreen) {
            size_message.innerHTML = "<font color='red'><%=resources.LNG_RESOLUTION_DESC2 %>. <A href='#' onclick='set_default_size();'><%=resources.LNG_CLICK_HERE %></a> <%=resources.LNG_RESOLUTION_DESC3 %>.</font>";
            return;
        }
        size_message.innerHTML = "";
    }

    function set_default_size() {
        document.getElementById('al_width').value = 340;
        document.getElementById('al_height').value = 290;
        document.getElementById('size_message').innerHTML = "";
    }

    function check_size_input(myfield, e, dec) {
        var key;
        var keychar;

        if (window.event)
            key = window.event.keyCode;
        else if (e)
            key = e.which;
        else
            return true;

        keychar = String.fromCharCode(key);

        // control keys
        if ((key == null) || (key == 0) || (key == 8) ||
		(key == 9) || (key == 13) || (key == 27)) {
            return true;
        }
        // numbers
        else if (myfield.value.length > 6) {
            return false;
        }
        else if ((("0123456789").indexOf(keychar) > -1)) {
            return true;
        }
        // decimal point jump
        else if (dec && (keychar == ".")) {
            myfield.form.elements[dec].focus();
            return false;
        }
        else
            return false;
    }

    function CalculateLifetime(valueControlId, factorControlId, resultId) {

        var value = $("#" + valueControlId).val();
        var factor = $("#" + factorControlId).val();

        var result = value * factor;

        $("#" + resultId).val(result);
    }
    </script>
</head>
<body>
    <form id="form1" runat="server" action="SaveSettings.aspx" method="POST">
        <div>
            <div>
                <table width="100%" height="100%" cellspacing="0" cellpadding="0" />
                <tr>
                    <td height="100%">
                        <div style="margin: 10px;">
                            <span class="work_header">
                                <%= resources.LNG_SYSTEM_CONFIGURATION %></span>
                            <br />
                            <b runat="server" id="successMessage" style="display: none">
                                <%=resources.LNG_SYSTEM_CONFIGURATION_HAVE_BEEN_SUCESSFULLY_CHANGED %>.

                            </b>

                            <table border="0">
                                <tr>
                                    <td>
                                        <div id="accordion" style="width: 770px">
                                            
                                        <h3><%=resources.LNG_ALERT_TYPE %></h3>
                                            <table style="width: 100%">
                                                <tr>
                                                    <td style="width: 20px;">
                                                        <input runat="server" type="checkbox" id="ConfDesktopAlertByDefaultField" name="ConfDesktopAlertByDefaultField" />
                                                    </td>
                                                    <td colspan="3">
                                                        <label for="ConfDesktopAlertByDefault">
                                                            <%=resources.LNG_DESKTOP_ALERT %>
                                                        </label>
                                                    </td>
                                                </tr>
                                                <tr class="useDesktopAlert">
                                                    <td>&nbsp;</td>
                                                    <td style="width: 20px;">
                                                        <input runat="server" type="radio" name="ConfDesktopAlertType" value="P" id="ConfDesktopAlertTypeP" />
                                                    </td>
                                                    <td colspan="2">
                                                        <%=resources.LNG_POP_UP_WINDOW%>
                                                    </td>
                                                </tr>

                                                <tr class="useDesktopAlert">
                                                    <td>&nbsp;</td>
                                                    <td>
                                                        <input runat="server" type="radio" name="ConfDesktopAlertType" value="T" id="ConfDesktopAlertTypeT" />
                                                    </td>
                                                    <td colspan="2">
                                                        <%=resources.LNG_SCROLLING_TICKER%>
                                                    </td>
                                                </tr>

                                                <tr class="useDesktopAlert">
                                                    <td>&nbsp;</td>
                                                    <td>
                                                        <input runat="server" type="radio" name="ConfDesktopAlertType" id="ConfDesktopAlertTypeR" value="R" />
                                                    </td>
                                                    <td colspan="2">RSVP</td>
                                                </tr>

                                                <!--<tr>
                                        <td>&nbsp;</td>
                                        <td>
                                            <input runat="server" type="radio" name="ConfDesktopAlertType"  id="ConfDesktopAlertTypeL" value="L" />
                                        </td>
                                        <td colspan="2">LinkedIn</td>
                                    </tr>-->

                                                <tr runat="server" id="rowBlog">
                                                    <td>
                                                        <input runat="server" id="ConfPostToBlogByDefault" type="checkbox" name="ConfPostToBlogByDefault" value="1" />
                                                    </td>
                                                    <td colspan="3">
                                                        <%=resources.LNG_BLOG_POST%>
                                                    </td>
                                                </tr>


                                                <tr runat="server" id="rowEmail">
                                                    <td>
                                                        <input runat="server" id="ConfEmailByDefault" type="checkbox" name="ConfEmailByDefault" value="1" />
                                                    </td>
                                                    <td colspan="3">
                                                        <%=resources.LNG_EMAIL%>
                                                    </td>
                                                </tr>


                                                <tr runat="server" id="rowSms">
                                                    <td>
                                                        <input runat="server" id="ConfSMSByDefault" type="checkbox" name="ConfSMSByDefault" value="1" />
                                                    </td>
                                                    <td colspan="3">
                                                        <%=resources.LNG_SMS%>
                                                    </td>
                                                </tr>


                                                <tr runat="server" id="rowTweet">
                                                    <td>
                                                        <input runat="server" id="ConfSocialMediaByDefault" type="checkbox" name="ConfSocialMediaByDefault" value="1" />
                                                    </td>
                                                    <td colspan="3">
                                                        <%=resources.LNG_TWEET%>
                                                    </td>
                                                </tr>


                                                <tr runat="server" id="rowTextToCall">
                                                    <td>
                                                        <input runat="server" id="ConfTextCallByDefault" type="checkbox" name="ConfTextCallByDefault" value="1" />
                                                    </td>
                                                    <td colspan="3">
                                                        <%=resources.LNG_TEXT_TO_CALL%>
                                                    </td>
                                                </tr>

                                            </table>
                                            
                                        <h3><%=resources.LNG_POP_UP_APPEARANCE %></h3>
                                            <div>
                                                <div id="size_message"></div>
                                                <table>
                                                    <div runat="server" id="fullscreenDivWindowButton">
                                                        <tr>
                                                            <td>
                                                                <input runat="server" type="radio" name="ConfPopupType" value="W" id="ConfPopupTypeW" /></td>
                                                            <td colspan="3">
                                                                <%=resources.LNG_WINDOW%>
                                                            </td>
                                                        </tr>
                                                    </div>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>
                                                            <%=resources.LNG_WIDTH%>
                                                        </td>
                                                        <td>
                                                            <input runat="server" type="text" onkeyup="check_value(this)" id="ConfAlertWidth" onkeypress="return check_size_input(this, event);" name="ConfAlertWidth" style="width: 50px" />
                                                            <%=resources.LNG_PX%>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>
                                                            <%=resources.LNG_HEIGHT%>
                                                        </td>
                                                        <td>
                                                            <input runat="server" type="text" onkeyup="check_value(this)" id="ConfAlertHeight" onkeypress="return check_size_input(this, event);" name="ConfAlertHeight" style="width: 50px" />
                                                            <%=resources.LNG_PX%>
                                                        </td>
                                                    </tr>
                                                    <div runat="server" id="fullscreenDivRadiobutton">
                                                        <tr>
                                                            <td>
                                                                <input runat="server" id="ConfPopupTypeF" type="radio" name="ConfPopupType" value="F" /></td>
                                                            <td colspan="3">
                                                                <%=resources.LNG_FULL_SCREEN%>
                                                            </td>
                                                        </tr>
                                                    </div>
                                            </div>

                                            <tr>
                                                <td valign="top" colspan="4">
                                                    <fieldset id="position_fieldset" style="width: 135px; margin: 0px; padding: 5px 5px 10px 5px">
                                                        <legend>
                                                            <%=resources.LNG_POSITION%>
                                                        </legend>
                                                        <div id="position_div">
                                                            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                                                <tr>
                                                                    <td valign="middle" style="text-align: center">
                                                                        <input runat="server" type="radio" name="ConfAlertPosition" value="2" id="ConfAlertPosition2" /></td>
                                                                    <td></td>
                                                                    <td valign="middle" style="text-align: center">
                                                                        <input runat="server" type="radio" name="ConfAlertPosition" value="3" id="ConfAlertPosition3" /></td>
                                                                </tr>
                                                                <tr>
                                                                    <td></td>
                                                                    <td valign="middle" style="text-align: center; padding: 10px">
                                                                        <input runat="server" type="radio" name="ConfAlertPosition" value="4" id="ConfAlertPosition4" /></td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td valign="middle" style="text-align: center">
                                                                        <input runat="server" type="radio" name="ConfAlertPosition" value="5" id="ConfAlertPosition5" /></td>
                                                                    <td></td>
                                                                    <td valign="middle" style="text-align: center">
                                                                        <input runat="server" type="radio" name="ConfAlertPosition" value="6" id="ConfAlertPosition6" /></td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </fieldset>
                                                </td>
                                                <div runat="server" id="tickerDiv">
                                                    <td valign="top">
                                                        <fieldset id="ticker_position_fieldset" style="margin: 0px; padding: 0px 5px 5px 5px">
                                                            <legend>
                                                                <%=resources.LNG_TICKER_POSITION%>
                                                            </legend>
                                                            <div id="ticker_position_div">
                                                                <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                                                    <tr>
                                                                        <td valign="middle" style="text-align: center; padding: 5px 10px">
                                                                            <input runat="server" type="radio" name="ConfTickerPosition" value="7" id="ConfTickerPosition7" /></td>
                                                                        <td><span><%=resources.LNG_TOP%></span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td valign="middle" style="text-align: center; padding: 5px 10px">
                                                                            <input runat="server" type="radio" name="ConfTickerPosition" value="8" id="ConfTickerPosition8" /></td>
                                                                        <td><span><%=resources.LNG_MIDDLE%></span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td valign="middle" style="text-align: center; padding: 5px 10px">
                                                                            <input runat="server" type="radio" name="ConfTickerPosition" value="9" id="ConfTickerPosition9" /></td>
                                                                        <td><span><%=resources.LNG_BOTTOM%></span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="display: none">
                                                                        <td valign="middle" style="text-align: center; padding: 5px 10px">
                                                                            <input runat="server" type="radio" name="ConfTickerPosition" value="D" id="ConfTickerPositionD" /></td>
                                                                        <td><span><%=resources.LNG_DOCKED_AT_BOTTOM%></span><img style="margin: 0px 5px" src="images/help.png" title="<%=resources.LNG_DOCKED_TICKER_HINT%>." />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </fieldset>
                                                    </td>
                                                </div>
                                            </tr>
                            </table>
                        </div>
                        
                    <h3><%=resources.LNG_DESKTOP_ALERT_OPTIONS %></h3>
                        <table style="width: 100%;">
                            <tr>
                                <td style="width: 20px;">
                                    <input runat="server" id="ConfUrgentByDefault" type="checkbox" name="ConfUrgentByDefault" value="1" /></td>
                                <td colspan="3"><%=resources.LNG_URGENT_ALERT %></td>
                            </tr>
                            <tr>
                                <td>
                                    <input runat="server" id="ConfAcknowledgementByDefault" type="checkbox" name="ConfAcknowledgementByDefault" value="1" /></td>
                                <td colspan="3"><%=resources.LNG_ACKNOWLEDGEMENT %></td>
                            </tr>
                            <tr>
                                <td>
                                    <input runat="server" id="ConfUnobtrusiveByDefault" type="checkbox" name="ConfUnobtrusiveByDefault" value="1" /></td>
                                <td colspan="3"><%=resources.LNG_UNOBTRUSIVE %></td>
                            </tr>
                            <tr>
                                <td>
                                    <input runat="server" id="ConfSelfDeletableByDefault" type="checkbox" name="ConfSelfDeletableByDefault" value="1" /></td>
                                <td colspan="3"><%=resources.LNG_SELF_DELETABLE %></td>
                            </tr>
                            <tr>
                                <td>
                                    <input runat="server" id="ConfAutocloseByDefault" type="checkbox" name="ConfAutocloseByDefault" value="1" /></td>
                                <td colspan="1" style="width: 140px;"><%=resources.LNG_AUTOCLOSE_IN %> </td>
                                <td>
                                    <input runat="server" id="ConfAutocloseValue" type="text" onkeypress="return check_size_input(this, event);" name="ConfAutocloseValue" style="width: 50px" value="" />
                                    <%=resources.LNG_MINUTES %></td>
                            </tr>
                            <tr>
                                <td>
                                    <input runat="server" id="ConfAllowManualCloseByDefault" type="checkbox" name="ConfAllowManualCloseByDefault" disabled value="1" /></td>
                                <td colspan="2"><%=resources.LNG_ALLOW_MANUAL_CLOSE %></td>
                            </tr>
                            <tr>
                                <td>
                                    <input runat="server" id="ConfScheduleByDefault" type="checkbox" name="ConfScheduleByDefault" value="1" /></td>
                                <td colspan="3"><%=resources.LNG_SCHEDULE_ALERT %></td>
                            </tr>
                            <tr>
                                <td colspan="4"><%=resources.LNG_LIFETIME %>&nbsp;
                                            <label>
                                                <%=resources.LNG_LIFETIME_IN %></label>
                                    <input runat="server" type="text" id="lifetime" runat="server" name="lifetimeval" style="width: 50px" onkeypress="return check_size_input(this, event);" />
                                    <select runat="server" id="lifetimeFactor" name="lifetime_factor">
                                    </select>
                                    <label id ="LifetimeAdditionalDescription" runat="server"></label>
                                    <input runat="server" type="hidden" id="ConfMessageExpire" name="ConfMessageExpire" value="0" />
                                </td>
                            </tr>
                        </table>
                        
                    <h3 runat="server" id="rssSettingsHeader"><%=resources.LNG_RSS %></h3>
                        <table style="width: 100%;" runat="server" id="rssTable">
                            <tr>
                                <td colspan="4"><%=resources.LNG_LIFETIME %>&nbsp;
                                        <label>
                                            <%=resources.LNG_LIFETIME_IN %></label>
                                    <input runat="server" type="text" id="rssLifetime" runat="server" name="lifetimeval" style="width: 50px" onkeypress="return check_size_input(this, event);" />
                                    <select runat="server" id="rssLifetimeFactor" name="lifetime_factor">
                                    </select>
                                    <label id ="RssLifetimeAdditionalDescription" runat="server"></label>
                                    <br />
                                    <span runat="server" id ="RssLifetimeDescription" style="color: #aaaaaa"><%=resources.LNG_LIFETIME_DESC %></span>
                                    <input runat="server" type="hidden" id="ConfRSSMessageExpire" name="ConfRSSMessageExpire" value="0" />
                                </td>
                            </tr>
                        </table>
            </div>
            </td>
                 </tr>
                <tr>
                    <td align="right">
                        <a class="cancel_button" onclick="javascript:history.go(-1); return false;">
                            <%=resources.LNG_CANCEL %>
                        </a>
                        <asp:LinkButton class="save_button" runat="server" ID="cantSaveDemoSetingsDefault">
								<%=resources.LNG_SAVE %>
                        </asp:LinkButton>
                        <input runat="server" type="hidden" name="a" value="2" />
                        <br />
                    </td>
                </tr>
            </table>    
        </div>

        <input type="hidden" name="ConfDesktopAlertByDefault" id="ConfDesktopAlertByDefault" runat="server" />
        <input type="hidden" name="ConfEnableHelp" id="ConfEnableHelp" runat="server" />
        <input type="hidden" name="ConfShowNewsBanner" id="ConfShowNewsBanner" runat="server" />
        <input type="hidden" name="ConfAllowApprove" id="ConfAllowApprove" runat="server" />
        <input type="hidden" name="ConfEnableHtmlAlertTitle" id="ConfEnableHtmlAlertTitle" runat="server" />
        <input type="hidden" name="ConfShowSkinsDialog" id="ConfShowSkinsDialog" runat="server" />
        <input type="hidden" name="ConfShowTypesDialog" id="ConfShowTypesDialog" runat="server" />
        <input type="hidden" name="ConfAllowClientDeleteTerminatedAlerts" id="ConfAllowClientDeleteTerminatedAlerts" runat="server" />
        <input type="hidden" name="ConfEnablePostponeInPreview" id="ConfEnablePostponeInPreview" runat="server" />
        <input type="hidden" name="ConfDSRefreshRateValue" id="ConfDSRefreshRateValue" runat="server" />
        <input type="hidden" name="ConfEnableMultiAlert" id="ConfEnableMultiAlert" runat="server" />
        <input type="hidden" name="ConfEnableMultiVote" id="ConfEnableMultiVote" runat="server" />
        <input type="hidden" name="ConfEnableADEDCheck" id="ConfEnableADEDCheck" runat="server" />
        <input type="hidden" name="ConfEnableRegAndADEDMode" id="ConfEnableRegAndADEDMode" runat="server" />
        <input type="hidden" name="ConfForceSelfRegWithAD" id="ConfForceSelfRegWithAD" runat="server" />
        <input type="hidden" name="ConfDisableGroupsSync" id="ConfDisableGroupsSync" runat="server" />
        <input type="hidden" name="ConfAPISecret" id="ConfAPISecret" runat="server" />
        <input type="hidden" name="ConfDefaultLanguage" id="ConfDefaultLanguage" runat="server" />
        <input type="hidden" name="ConfDateFormat" id="ConfDateFormat" runat="server" />
        <input type="hidden" name="ConfTimeFormat" id="ConfTimeFormat" runat="server" />
        <input type="hidden" name="ConfFirstDayOfWeek" id="ConfFirstDayOfWeek" runat="server" />
        <input type="hidden" name="ConfSessionTimeout" id="ConfSessionTimeout" runat="server" />
        <input type="hidden" name="ConfSmtpServer" id="ConfSmtpServer" runat="server" />
        <input type="hidden" name="ConfSmtpPort" id="ConfSmtpPort" runat="server" />
        <input type="hidden" name="ConfSmtpSsl" id="ConfSmtpSsl" runat="server" />
        <input type="hidden" name="ConfSmtpAuth" id="ConfSmtpAuth" runat="server" />
        <input type="hidden" name="ConfSmtpUser" id="ConfSmtpUser" runat="server" />
        <input type="hidden" name="ConfSmtpPassword" id="ConfSmtpPassword" runat="server" />
        <input type="hidden" name="ConfSmtpFrom" id="ConfSmtpFrom" runat="server" />
        <input type="hidden" name="ConfSmtpTimeout" id="ConfSmtpTimeout" runat="server" />
        <input type="hidden" name="ConfSmsUrl" id="ConfSmsUrl" runat="server" />
        <input type="hidden" name="ConfSmsContentType" id="ConfSmsContentType" runat="server" />
        <input type="hidden" name="ConfSmsPostData" id="ConfSmsPostData" runat="server" />
        <input type="hidden" name="ConfSmsAuthUser" id="ConfSmsAuthUser" runat="server" />
        <input type="hidden" name="ConfSmsAuthPass" id="ConfSmsAuthPass" runat="server" />
        <input type="hidden" name="ConfTextToCallSID" id="ConfTextToCallSID" runat="server" />
        <input type="hidden" name="ConfTextToCallAuthToken" id="ConfTextToCallAuthToken" runat="server" />
        <input type="hidden" name="ConfTextToCallNumber" id="ConfTextToCallNumber" runat="server" />
        
        <input type="hidden" name="ConfTraceLogLevelEnabled" id="ConfTraceLogLevelEnabled" runat="server" />
        <input type="hidden" name="ConfInfoLogLevelEnabled" id="ConfInfoLogLevelEnabled" runat="server" />
        <input type="hidden" name="ConfDebugLogLevelEnabled" id="ConfDebugLogLevelEnabled" runat="server" />
        <input type="hidden" name="ConfErrorLogLevelEnabled" id="ConfErrorLogLevelEnabled" runat="server" />
        <input type="hidden" name="ConfFatalLogLevelEnabled" id="ConfFatalLogLevelEnabled" runat="server" />

        <input type="hidden" name="activeTab" id="activeTab" value="1" />

        </div>
    </div>
    </form>
</body>
</html>
