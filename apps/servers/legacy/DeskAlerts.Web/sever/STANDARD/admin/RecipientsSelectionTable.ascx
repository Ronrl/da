﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RecipientsSelectionTable.ascx.cs" Inherits="DeskAlertsDotNet.Controls.RecipientsSelectionTable" %>
<%@ Import Namespace="DeskAlertsDotNet.Models" %>
<%@ Import NameSpace="DeskAlertsDotNet.Parameters" %>
<%@ Import Namespace="Resources" %>

<script language="javascript">

$(document).ready(function () {
    $("#tabs").tabs();
});

tabType = "users";

function check_tab(type) {
    var list1 = document.getElementById("list1");

    if (type == "groups") {
        list1.src = "PolicyGroups.aspx";
        tabType = "groups";
    }

    if (type == "users") {
        list1.src = "PolicyUsers.aspx";
        tabType = "users";
    }

    if (type == "ous") {
        list1.src = "PolicyOUs.aspx";
        tabType = "ous";
    }

    if (type == "computers") {
        list1.src = "PolicyComps.aspx";
        tabType = "computers";
    }
}

function checkNewId(newId) {
    var list2 = document.getElementById("list2");

    for (i = list2.options.length - 1; i >= 0; i--) {
        if (list2.options[i].value == newId) {
            return false;
        }
    }
    
    return true;
}

    function getBeforeItem() {
        var list2 = document.getElementById("list2");
        for (i = list2.options.length - 1; i >= 0; i--) {
            if (tabType == "groups") {
                //if ad - ous

                if (list2.options[i].value == "ous") {
                    //return list2.options[i];
                    return i;
                }
            }
            else if (tabType == "ous") {
                if (list2.options[i].value == "computers") {
                    //return list2.options[i];
                    return i;
                }
            }
            else if (tabType == "computers") {
                if (list2.options[i].value == "users") {
                    //return list2.options[i];
                    return i;
                }
            }

            else if (list2.options[i].value == "users") {
                //return list2.options[i];
                return i;
            }
        }
        if (tabType == "users") {
            if (list2.options[i].value == "users") {
                //return list2.options[i];
                return i;
            }
        }
    }

    function addToSelectedList(id) {

        var inputId = "#recipientsSelector_" + tabType + "Input";

        var inputElement = $(inputId);

        var inputValue = $(inputElement).val();
        if (inputValue.length > 0) {
            inputValue += ",";
        }

        inputValue += id

        $(inputElement).val(inputValue);

    }

function list_add() {
    var list1 = window.frames["list1"];
    var list2 = document.getElementById("list2");

    if (tabType == "ous") {
        res = list1.getCheckedOus();
        idSequence = "";

        if (res) {
            for (var key in res) {
                var newVal =  res[key];
                var newId = "ous_" + key;

                if (checkNewId(newId)) {
                    addToSelectedList(key);

                    var newRow = new Option(newVal, newId);

                    if (tabType != "users") {
                        var beforeItem = getBeforeItem();
                        var elOptOld = list2.options[beforeItem];

                        try {
                            list2.add(newRow, elOptOld); // standards compliant; doesn't work in IE
                        } catch (ex) {
                            list2.add(newRow, beforeItem); // IE only
                        }
                    } else {
                        list2.options[list2.length] = newRow;
                    }
                }
            }
        }
    } else {
    //other
        var items_ids = list1.document.getElementsByClassName("content_object");
        var items_values = list1.document.getElementsByClassName("content_object");
        var duplicatedMembers = "";

        for (var i = 0; i < items_ids.length; i++) {
            if (jQuery(items_ids[i]).is(":checked")) {
                var newVal = items_values[i].getAttribute("object_name");
                var newId = tabType + "_" + items_values[i].value;

                if (checkNewId(newId)) {
                    var newRow = new Option(newVal, newId);

                    addToSelectedList(items_values[i].value);

                    if (tabType != "users") {
                        var beforeItem = getBeforeItem();
                        var elOptOld = list2.options[beforeItem];

                        try {
                            list2.add(newRow, elOptOld); // standards compliant; doesn't work in IE
                        } catch (ex) {
                            list2.add(newRow, beforeItem); // IE only
                        }
                    } else {
                        list2.options[list2.length] = newRow;
                    }
                } else {
                    duplicatedMembers += newVal + " ";
                }
            }
        }

        if (duplicatedMembers != "") {
            alert(duplicatedMembers + "already in this group!");
        }
    }
}

function list_remove() {
    var list2 = document.getElementById("list2");

    for (i = list2.options.length - 1; i >= 0; i--) {
        if (list2.options[i].selected == true) {
            var value = list2.options[i].value.split('_');
            var inputVal = $("#recipientsSelector_" + value[0] + "Input").val();

            inputVal = inputVal.replace(value[1], '').replace(',,', ",").replace(/(^,)|(,$)/g, "");
            $("#recipientsSelector_" + value[0] + "Input").val(inputVal);
            list2.options[i] = null;
        }
    }
}

</script>
<input type="hidden" runat="server" id="ousInput" name="ous_input"/>
<input type="hidden" runat="server" id="usersInput" name="users_input"/>
<input type="hidden" runat="server" id="computersInput" name="computers_input"/>
<input type="hidden" runat="server" id="groupsInput" name="groups_input"/>
<table id="RecipientSelectionTableId">
	 <tr valign="middle" align="center">
		 <td align="left">
			 <div id="tabs" style="">
				 <ul>
					 <li><a href="#recipientsTab" onclick="check_tab('users');"><%=resources.LNG_USERS%></a></li>
					 <li><a href="#recipientsTab" onclick="check_tab('groups');"><%=resources.LNG_GROUPS%></a></li>
					 <li><a href="#recipientsTab" onclick="check_tab('ous');"><%=resources.LNG_OUS%></a></li>
					 <li><a href="#recipientsTab" onclick="check_tab('computers');"><%=resources.LNG_COMPUTERS%></a></li>
				 </ul>
				 <div id="recipientsTab">
					 <iframe frameborder="0" id="list1" name="list1" class="list_iframe" src="PolicyUsers.aspx"></iframe>
				 </div>
			 </div>
		 </td>
		 <td>
			 <%=resources.LNG_ADD %>
             <br>
             <a href="#" id="addRecipientButton" onclick="javascript: list_add();"><img src="images/list_add.png" border="0" /></a>
             <br>
             <a href="#" id="removeRecipientButton" onclick="javascript: list_remove();"><img src="images/list_remove.png" border="0" /></a>
             <br>
             <%=resources.LNG_REMOVE %>
		 </td>
		 <td>
			 <b><%=resources.LNG_RECIPIENTS %></b><br><br>
			 <asp:ListBox runat="server" multiple="True" id="list2" name="list2" class="list_select"></asp:ListBox>
		 </td>
	 </tr>
 </table>
