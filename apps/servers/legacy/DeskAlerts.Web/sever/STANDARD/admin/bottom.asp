﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%
check_session()

version = ""
post_name = ""

Set RS = Conn.Execute("SELECT version FROM version")
if not RS.EOF then
	version = RS("version")
	version_arr = Split(version, ".")
	if UBound(version_arr) > -1 then
		major_version = version_arr(0)
		if IsNumeric(major_version) then
			if major_version > 4 then
				post_name = " " & major_version
			end if
		end if
	end if
	RS.Close
end if

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>DeskAlerts</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link href="css/style9.css" rel="stylesheet" type="text/css">
	<script language="javascript" type="text/javascript" src="functions.js"></script>
	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
	<script language="javascript" type="text/javascript" src="jscripts/date.js"></script>
	<script language="javascript" type="text/javascript">
		var serverDate;
		var settingsDateFormat = "<%=GetDateFormat()%>";
		var settingsTimeFormat = "<%=GetTimeFormat()%>";

		var responseDbFormat = "dd/MM/yyyy HH:mm:ss";
		var uiFormat = getUiDateTimeFormat(settingsDateFormat, settingsTimeFormat);

		$(function() {
			serverDate = new Date(getDateFromAspFormat("<%=now_db%>", responseDbFormat));
			updateServerDate();
			setInterval(function() { serverDate.setSeconds(serverDate.getSeconds() + 1); updateServerDate() }, 1000);
		});

		function updateServerDate() {
			$("#server_time").text(formatDate(serverDate, uiFormat));
		}
</script>
</head>
<body class="bottom_body" style="margin:0">
	<span class="curdate" style="margin:0px;padding:7px 10px 10px 0px;white-space:nowrap; position:absolute; right:30px; top:0px; font-weight: normal;"><%=LNG_CURRENT_SERVER_DATE_AND_TIME%>:<br /><span id="server_time"></span></span>
	<a href="http://www.alert-software.com/" target="_blank">DeskAlerts<%= post_name %></a> v. <%= version %>
	<br />
	Copyright 2006-2015 <a href="http://www.alert-software.com/" target="_blank">DeskAlerts</a>
</body>
</html>
<!-- #include file="db_conn_close.asp" -->