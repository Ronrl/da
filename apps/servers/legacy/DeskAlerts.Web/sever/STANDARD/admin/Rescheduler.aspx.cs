﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DeskAlertsDotNet.Controls;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Parameters;


namespace DeskAlertsDotNet.Pages
{
    using Resources;

    public partial class Rescheduler : DeskAlertsBasePage
    {
        //use curUser to access to current user of DA control panel      
        //use dbMgr to access to database

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (string.IsNullOrEmpty(Request["id"]))
                return;

            ScriptManager1.RegisterAsyncPostBackControl(saveButton);
            saveButton.Click += SaveButton_Click;
            alertId.Value = Request["id"];
            schedulePanel.AlertId = Convert.ToInt32(Request["id"]);
            //Add your main code here
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {

            ReccurencePanelError err = schedulePanel.ValidateInputs();

            if (err == ReccurencePanelError.TO_DATE_LESS_THAN_FROM_DATE)
            {
                ShowPromt(Response, resources.LNG_REC_DATE_ERROR);
                return;
            }

            if (err == ReccurencePanelError.TIME_VALUE_IS_EMPTY)
            {
                ShowPromt(Response, resources.LNG_TIME_VALUE_IS_INCORRECT);
                return;
            }

            SubmitForm();
        }

        void SubmitForm()
        {
           // string IsDraftStr = isDraft.ToString().ToLower();

            form1.Action = "RescheduleAction.aspx";
            form1.Method = "POST";

            ScriptManager.RegisterClientScriptBlock(this, GetType(), "SUBMIT", "submitForm()", true);

        }



        public void ShowPromt(HttpResponse r, string text)
        {
            //r.Write("<script language='javascript'>showPrompt('" + text + "');</script>");

            ScriptManager.RegisterStartupScript(updatePanel, updatePanel.GetType(), "SHOW_PROMT", "showPrompt('" + text + "')", true);
        }
    }
}