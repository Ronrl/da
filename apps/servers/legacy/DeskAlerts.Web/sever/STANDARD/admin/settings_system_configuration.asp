﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<!-- #include file="md5.asp" -->
<!-- #include file="demo.inc" -->
<%
check_session()

uid = Session("uid")

if (uid <> "")  then
    if (request("gateway") <> "") then
        Dim gateway
        Dim contentType 
    
        gateway = request("gateway") & "?"    
        contentType = request("contentType")
        postData = request("postData")

        Set httpRequest = Server.CreateObject("MSXML2.ServerXMLHTTP")
        httpRequest.Open "POST", gateway, False, smsAuthUser, smsAuthPass
        httpRequest.SetRequestHeader "Content-Type", contentType
        httpRequest.Send postData

        postResponse = httpRequest.responseText
        postStatus = httpRequest.status

        Response.Write("{ ""postResponse"":" & postResponse & ", ""postStatus"":" & postStatus & "}")
        Response.End
    end if

	if(request("a") = "2") then
        'Updating config.inc start
        right_alert_dir = LEFT(alerts_dir, (LEN(alerts_dir)-1))

        dim FileSystemObject, file, config_string, reg
    
        Set reg = New RegExp
        set FileSystemObject = Server.CreateObject("Scripting.FileSystemObject")
        set file = FileSystemObject.OpenTextFile(right_alert_dir & "admin\config.inc", 1)
    
        config_string = file.ReadAll

        reg.Pattern = "smsAuthPass = "".*"""
        config_string = reg.Replace(config_string, "smsAuthPass = """ & request("smsAuthPass") & """")
        smsAuthPass = request("smsAuthPass")

        reg.Pattern = "smsAuthUser = "".*"""
        config_string = reg.Replace(config_string, "smsAuthUser = """ & request("smsAuthUser") & """")
        smsAuthUser = request("smsAuthUser")

        if request("smsUseAuth") = "" then
            reg.Pattern = "smsUseAuth="".*"""
            config_string = reg.Replace(config_string, "smsUseAuth=""0""")
            smsUseAuth = "0"
        else
            reg.Pattern = "smsUseAuth="".*"""
            config_string = reg.Replace(config_string, "smsUseAuth=""1""")
            smsUseAuth = "1"
        end if
    
        reg.Pattern = "smsPostData="".*"""
        config_string = reg.Replace(config_string, "smsPostData=""" & request("smsPostData") & """")
        smsPostData = request("smsPostData") 

        reg.Pattern = "smsContentType = "".*"""
        config_string = reg.Replace(config_string, "smsContentType = """ & request("contentType") & """")
        smsContentType = request("contentType")

        reg.Pattern = "smsUrl="".*"""
        config_string = reg.Replace(config_string, "smsUrl=""" & request("smsUrl") & """")
        smsUrl = request("smsUrl")
    
        reg.Pattern = "smtpServer="".*"""
        config_string = reg.Replace(config_string, "smtpServer=""" & request("SMTPServer") & """")
        smtpServer = request("SMTPServer")

        reg.Pattern = "smtpPort="".*"""
        config_string = reg.Replace(config_string, "smtpPort=""" & request("smtpPort") & """")
        smtpPort = request("SMTPPort")
    
        if request("SMTPSSL") = "" then
            reg.Pattern = "smtpSSL="".*"""
            config_string = reg.Replace(config_string, "smtpSSL=""0""")
            smtpSSL = "0"
        else
            reg.Pattern = "smtpSSL="".*"""
            config_string = reg.Replace(config_string, "smtpSSL=""1""")
            smtpSSL = "1"
        end if

        if request("SMTPAuth") = "" then
            reg.Pattern = "smtpAuth="".*"""
            config_string = reg.Replace(config_string, "smtpAuth=""0""")
            smtpAuth = "0"
        else
            reg.Pattern = "smtpAuth="".*"""
            config_string = reg.Replace(config_string, "smtpAuth=""1""")
            smtpAuth = "1"
        end if

        reg.Pattern = "smtpUsername="".*"""
        config_string = reg.Replace(config_string, "smtpUsername=""" & request("SMTPUserName") & """")
        smtpUsername = request("SMTPUserName")
        
        if request("newPasswordListner") = 1 then
            reg.Pattern = "smtpPassword="".*"""
            config_string = reg.Replace(config_string, "smtpPassword=""" & Base64Encode(request("SMTPPassword")) & """")
            smtpPassword = Base64Encode(request("SMTPPassword"))
        end if
        
        reg.Pattern = "smtpFrom="".*"""
        config_string = reg.Replace(config_string, "smtpFrom=""" & request("SMTPFrom") & """")
        smtpFrom = request("SMTPFrom")

        reg.Pattern = "smtpConnectTimeout="".*"""
        config_string = reg.Replace(config_string, "smtpConnectTimeout=""" & request("SMTPConnectTimeout") & """")
        smtpConnectTimeout = request("SMTPConnectTimeout")
    
        set file = FileSystemObject.OpenTextFile(right_alert_dir & "admin\config.inc", 2)
   
        file.Write(config_string)

        set file = Nothing
        set FileSystemObject = Nothing
        set config_string = Nothing
        set reg = Nothing
        'Upadating config.inc end
		'update all to zero
		SQL = "UPDATE settings SET val=0 WHERE s_part = 'S'"
		Conn.Execute(SQL)

		'update all from form
		For Each item In request.form
			'use prefix Conf to identify settings
			if(InStr(item, "Conf")>0) then
				SQL = "UPDATE settings SET val=N'" & request(item) & "' WHERE name='" & item & "' AND s_part = 'S'"
				Conn.Execute(SQL)
			end if
		Next

		disabled_buttons = Request("disabled_tiny_button")
		disabled_buttons_names = "('" & Replace(Replace(disabled_buttons, "'", "''"), ", ", "', '") & "')"
		Conn.Execute("UPDATE tiny_button_settings SET enabled = 0 WHERE name IN " & disabled_buttons_names)
		Conn.Execute("UPDATE tiny_button_settings SET enabled = 1 WHERE NOT name IN " & disabled_buttons_names)
	end if
	
	Set rsSettings = Conn.Execute("SELECT * FROM settings WHERE s_part='S'")

	do while not rsSettings.EOF

		sName = rsSettings("name")
		sVal = ""
		if(rsSettings("s_type")="C") then
			if(rsSettings("val")=1)then
				sVal = "checked"
			end if
		else
			sVal = rsSettings("val")
		end if

		Select Case sName
			case "ConfEnableMultiAlert"
				sConfEnableMultiAlertVal = sVal
			case "ConfEnableMultiVote"
				sConfEnableMultiVoteVal = sVal
			case "ConfDefaultEmail"
				sConfDefaultEmailVal = sVal
			case "ConfDefaultEmailAddress"
				sConfDefaultEmailAddressVal = sVal
			case "ConfEnableADEDCheck"
				sConfEnableADEDCheckVal = sVal
			case "ConfEnableRegAndADEDMode"
				sConfEnableRegAndADEDMode = sVal
			case "ConfAPISecret"
				sConfAPISecretVal = sVal
			case "ConfDefaultLanguage"
				sConfDefaultLanguage = sVal
			case "ConfDateFormat"
				sConfDateFormat = sVal
			case "ConfTimeFormat"
				sConfTimeFormat = sVal
			case "ConfFirstDayOfWeek"
				sConfFirstDayOfWeek = sVal
			case "ConfSessionTimeout"
				sConfSessionTimeout = sVal
			case "ConfEnableHtmlAlertTitle"
				sConfEnableHtmlAlertTitle = sVal
			case "ConfEnablePostponeInPreview"
				sConfEnablePostponeInPreview = sVal
			case "ConfEnableHelp"
				sConfEnableHelp = sVal
            case "ConfOldSurvey"
				sConfOldSurvey = sVal
			case "ConfShowSkinsDialog"
				sConfShowSkinsDialog = sVal
			case "ConfShowTypesDialog"
				sConfShowTypesDialog = sVal
			case "ConfShowNewsBanner"
				sConfShowNewsBanner = sVal
			case "ConfAllowApprove"
			    sConfAllowApprove = sVal
			case "ConfDSRefreshRateFactor"
			    sConfDSRefreshRateFactor = sVal
			case "ConfDSRefreshRateValue"
			    sConfDSRefreshRateValue = sVal
            case "ConfSuperAgent"
                sConfSuperAgent = sVal
            case "ConfAllowClientDeleteTerminatedAlerts"
                sConfAllowClientDeleteTerminatedAlerts = sVal
			case "ConfForceSelfRegWithAD"
                sConfForceSelfRegWithAD = sVal	
				
		end select

		rsSettings.MoveNext
	loop

%>
<!DOCTYPE html>
<html>
<head>
	<title>DeskAlerts</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link href="css/style9.css" rel="stylesheet" type="text/css">
	<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css" />
    <style>
        .disabled:hover{
            cursor: not-allowed;
        }
    </style>
	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
	<script language="javascript" type="text/javascript" src="jscripts/tiny_mce/tinymce.min.js"></script>
	<script language="javascript" type="text/javascript" src="jscripts/da_tinymce.min.js"></script>
	<script language="javascript" type="text/javascript">
	    var oldPassword = "Def";

	    $(document).ready(function()
	    {
	        oldPassword = $("#SMTPPassword").val();

	        $("#checkSMSGateway").on("click", checkSmsGateway);

	        if ($("#smsUseAuth").prop("checked")) {
	            $("#smsAuthUser").show();
	            $("#smsAuthPass").show();
	        } else {
	            $("#smsAuthUser").hide();
	            $("#smsAuthPass").hide();
	        }

	        $("#smsUseAuth").on("click", function(){
	                $("#smsAuthUser").val("").toggle();
	                $("#smsAuthPass").val("").toggle();
	        });
	    });

		$(function() {
			$(".cancel_button").button();
			$(".save_button").button();
			$("#accordion").accordion({
    			heightStyle: "content",
    			collapsible: true,
    			active:false
			});
		});

		function saveSettings()
		{
			var intRegex = /^\d+$/;
			var sessionTimeout = $("#ConfSessionTimeout").val();

			if(!intRegex.test(sessionTimeout) || parseInt(sessionTimeout) <= 0) {
				alert("<%=LNG_SESSION_TIMEOUT_ERROR%>");
				return;
			}
			
			var newPassword = $("#SMTPPassword").val();

			if(newPassword != oldPassword){
			    $("#newPasswordListner").val("1");
			}

			var SMTPConnectTimeout = $("#SMTPConnectTimeout").val().toString();
			var numbersRegex = "^[0-9]*$";

			if(SMTPConnectTimeout.match(numbersRegex) == null) {
			    alert("<%=LNG_WRONG_SMTP_CONNECT_TIMEOUT%>");
                return;
			}
			
			document.forms[0].submit();
		}

		function regenerateAPIsecret(){
			var newApiSecret = randomPassword(16);
			document.getElementById('ConfAPISecret').value=newApiSecret;
		}

		function randomPassword(length)
		{
			chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
			pass = "";
			for(x=0;x<length;x++)
			{
				i = Math.floor(Math.random() * 62);
				pass += chars.charAt(i);
			}
			return pass;
		}

		var disabled_tiny_buttons_array = [
			<%
			Set rs = Conn.Execute("SELECT name FROM tiny_button_settings WHERE enabled = 0")
			while not rs.EOF
				Response.Write "'" & rs("name") & "'"
				rs.MoveNext
				if not rs.EOF then
					Response.Write ","
				end if
			wend
			%>
				];

    function iterate(obj, stack) {
        for (var property in obj) {
            if (obj.hasOwnProperty(property)) {
                if (typeof obj[property] == "object") {
                    iterate(obj[property], stack + ' ' + property + " => ");
                } else {
                    $("#smsResponse").append(stack + ' ' + property + ": " + obj[property] + "<br>");
                }
            }
        }
	}

    function checkSmsGateway (){
        var gateway = $('#smsUrl', window.parent.frames[0].document).val();
        var contentType = $('input[name=contentType]:checked', window.parent.frames[0].document).val();
        var postData = $('#smsPostData', window.parent.frames[0].document).val();

        if (gateway == "") {
            alert("Gateway URL field is empty!");
        } else if (postData == "") {
            alert("Post data field is empty!");
        } else {
            $("#smsResponse").text("");
            $("#checkSMSGateway").off("click").addClass("disabled");
            $.post("settings_system_configuration.asp", {"gateway":gateway, "contentType":contentType, "postData":postData})
                .done (function (data) {
                    var JSONData = JSON.parse(data);

                    if (JSONData.postStatus == 404) {
                        alert("SMS gateway server not found.");
                    } else if (JSONData.postStatus == 500) {
                        alert("SMS gateway server is not available.");
                    } else {
                        if (typeof(JSONData.postResponse) == "object") {
                            iterate(JSONData.postResponse, "Objects: ");
                        } else {
                            $("#smsResponse").append(JSONData.postResponse);
                        }
                    }
                    
                    $("#checkSMSGateway").on("click", checkSmsGateway).removeClass("disabled");
                })
                .fail (function (data) {
                    $("#smsResponse").append("Your link is invalid.");
                    $("#checkSMSGateway").on("click", checkSmsGateway).removeClass("disabled");
                });
        }
    }
</script>
</head>
<body style="margin: 0px" class="body">
<table width="100%" border="0" cellspacing="0" height="100%">
    <tr>
	    <td>
		    <table width="100%" height="100%" cellspacing="0" cellpadding="0">
			    <tr>
				    <td height="100%">
					    <div style="margin: 10px;">
						    <span class="work_header"><%=LNG_SYSTEM_CONFIGURATION %></span>
                            <br>
							<%if(request("a")="2") then%>
								<b><%=LNG_SYSTEM_CONFIGURATION_HAVE_BEEN_SUCESSFULLY_CHANGED %>.</b>
							<%end if%>
							<form method="post" action="settings_system_configuration.asp">
								<table border="0">
									<tr>
										<td>
											<div id="accordion" style="width:770px">
												<h3><%=LNG_CONSOLE_INTERFACE %></h3>
												<table border="0" style="width:770px">
                                                    <tr>
														<td></td>
														<td>
															<font color="#aaaaaa"><%=LNG_LANG_DESC %>.</font>
														</td>
													</tr>
													<tr>
														<td>
															<input type="checkbox" id="ConfEnableHelp" name="ConfEnableHelp" <%=sConfEnableHelp %> value="1" />
														</td>
														<td>
															<label for="ConfEnableHelp"><%=LNG_ENABLE_HELP %></label>
														</td>
													</tr>
													<!--<tr>
														<td>
															<input type="checkbox" id="ConfShowNewsBanner" name="ConfShowNewsBanner" <%=sConfShowNewsBanner %> value="1" />
														</td>
														<td>
															<label for="ConfShowNewsBanner"><%=LNG_SHOW_NEWS_BANNER %></label>
														</td>
													</tr>-->
													<% if APPROVE = 1 then%>
													    <tr>
														    <td>
															    <input type="checkbox" id="ConfAllowApprove" name="ConfAllowApprove" <%=sConfAllowApprove %> value="1" />
														    </td>
														    <td>
															    <label for="ConfAllowApprove"><%=LNG_ALLOW_APPROVE %></label>
														    </td>
													    </tr>
													<% end if %>
												</table>
												<h3><%=LNG_ALERTS %></h3>
												<table border="0" style="width:770px">
													<tr>
														<td>
															<input type="checkbox" id="ConfEnableHtmlAlertTitle" name="ConfEnableHtmlAlertTitle" <%=sConfEnableHtmlAlertTitle %> value="1" />
														</td>
														<td>
															<label for="ConfEnableHtmlAlertTitle"><%=LNG_ENABLE_ALERT_TITLE_STYLING %></label>
														</td>
													</tr>
													<tr>
														<td>
															<input type="checkbox" id="ConfShowTypesDialog" name="ConfShowTypesDialog" <%=sConfShowTypesDialog %> value="1" />
														</td>
														<td>
															<label for="ConfShowTypesDialog"><%=LNG_SHOW_TYPES_DIALOG %></label>
														</td>
													</tr>
                                                    <tr style="display:none">
                                                        <td>
                                                            <input type="checkbox" id="ConfAllowClientDeleteTerminatedAlerts" name="ConfAllowClientDeleteTerminatedAlerts" <%=sConfAllowClientDeleteTerminatedAlerts %> value="1" />
                                                        </td>
                                                        <td>
                                                            <label for="ConfAllowClientDeleteTerminatedAlerts"><%=LNG_ALLOW_CLIENT_DELETE_TERMINATED_ALERTS %></label>
                                                        </td>
                                                    </tr>
													<tr>
														<td colspan="2">
															<br>
															<%=LNG_SHOW_POSTPONE %>
														</td>
													</tr>
													<tr>
														<td>
															<input id="ConfEnablePostponeInPreview1" name="ConfEnablePostponeInPreview" type="radio" value="1" <%if sConfEnablePostponeInPreview = "1" then Response.Write "checked" end if%> />
														</td>
														<td>
															<label for="ConfEnablePostponeInPreview1"><%=LNG_SHOW_FOR_ALL %></label>
														</td>
													</tr>
													<tr>
														<td>
															<input id="ConfEnablePostponeInPreview2" name="ConfEnablePostponeInPreview" type="radio" value="2" <%if sConfEnablePostponeInPreview = "2" then Response.Write "checked" end if%> />
														</td>
														<td>
															<label for="ConfEnablePostponeInPreview2"><%=LNG_SHOW_FOR_NON_URGENT %></label>
														</td>
													</tr>
													<tr>
														<td>
															<input id="ConfEnablePostponeInPreview0" name="ConfEnablePostponeInPreview" type="radio" value="0" <%if sConfEnablePostponeInPreview = "0" then Response.Write "checked" end if%> />
														</td>
														<td>
															<label for="ConfEnablePostponeInPreview0"><%=LNG_SHOW_FOR_NONE %></label>
														</td>
													</tr>
													<tr>
														<td></td>
														<td style="color:#aaaaaa"><%=LNG_SHOW_POSTPONE_DESC %></td>
													</tr>
													<tr>
													    <td colspan="2">
													       <%=LNG_DIGSIGN_SETTINGS & ":" %>
													    </td>
													</tr>  
													<tr>  
													    <td>
                                                            <input type="number" style="float:left;" id="ConfDSRefreshRateValue" name="ConfDSRefreshRateValue" min="1" max="60" value="<%=sConfDSRefreshRateValue %>">
                                                        </td>
                                                        <td>
                                                            <select id="ConfDSRefreshRateFactor" name="ConfDSRefreshRateFactor">
                                                                <option value="0" <% if sConfDSRefreshRateFactor = "0" then Response.Write "selected" %>><%=LNG_SECONDS %></option>
                                                                <option value="1" <% if sConfDSRefreshRateFactor = "1" then Response.Write "selected" %>><%=LNG_MINUTES %></option>
                                                                <option value="2" <% if sConfDSRefreshRateFactor = "2" then Response.Write "selected" %>><%=LNG_HOURS %></option>
                                                            </select>
													    </td>
													</tr>
												</table>
												<h3><%=LNG_MESSAGE_DELIVERY %></h3>
												<table border="0" style="width:770px">
													<tr>
														<td>
															<input type="hidden" id="ConfEnableMultiVote" name="ConfEnableMultiVote"  value="0" />
														</td>
														<td colspan="2" style="display: none">
															<label for="ConfEnableMultiVote"><%=LNG_MULTIPLE_VOTES_DESC %></label>
														</td>
													</tr>
													<tr>
														<td>&nbsp;</td>
														<td colspan="2">
															<br>
															<%=LNG_LIFETIME_MOVED_TO_DEFAULTS %>
														</td>
													</tr>
													<tr style="display:none">
														<td>&nbsp;</td>
														<td colspan="2">
															<br>
															<a href="templates.asp" target="work_area"><%=LNG_TEMPLATES %></a> 
                                                            <i>(<%=LNG_DEPRECATED %>)</i>
														</td>
													</tr>
												</table>
												<h3><%=LNG_AD_ED %></h3>
				                                <table border="0" style="width:770px">
													<tr>
														<td>
															<input type="checkbox" id="ConfEnableADEDCheck" name="ConfEnableADEDCheck" <%=sConfEnableADEDCheckVal %> value="1" />
														</td>
														<td colspan="2">
															<label for="ConfEnableADEDCheck"><%=LNG_AD_ED_CHECK_DESC %></label>
														</td>
													</tr>
													<tr>
														<td>&nbsp;</td>
														<td colspan="2">
															<font color="#aaaaaa"><%=LNG_AD_ED_CHECK_DESC2 %>.</font>
														</td>
													</tr>
													<tr>
														<td>
															<input type="checkbox" id="ConfEnableRegAndADEDMode" name="ConfEnableRegAndADEDMode" <%=sConfEnableRegAndADEDMode %> value="1" />
														</td>
														<td colspan="2">
															<label for="ConfEnableRegAndADEDMode"><%=LNG_AD_REG_DESC %></label>
														</td>
													</tr>
                                                    <tr>
														<td>
															<input type="checkbox" id="ConfForceSelfRegWithAD" name="ConfForceSelfRegWithAD" <%=sConfForceSelfRegWithAD %> value="1" />
														</td>
														<td colspan="2">
															<label for="ConfForceSelfRegWithAD"><%=LNG_FORCE_SELFREG_WITH_AD %></label>
														</td>
													</tr>
                                                    <% if ConfEGSModerator = 1 then %>
       												    <tr>
														    <td>
															    <input type="checkbox" id="ConfDisableGroupsSync" name="ConfDisableGroupsSync" <%=sConfDisableGroupsSync %> value="1" />
														    </td>
														    <td colspan="2">
															    <label for="ConfDisableGroupsSync">Disable Group Synchronizations</label>
														    </td>
													    </tr>
                                                    <% end if %>
                                                </table>
												<h3><%=LNG_API %></h3>
												<table border="0" style="width:770px">
													<tr>
														<td>&nbsp;</td>
														<td>
															<label for="ConfAPISecret"><%=LNG_API_SECRET %>:</label>
														</td>
														<td>
															<input type="text" name="ConfAPISecret" id="ConfAPISecret" value="<%=sConfAPISecretVal %>" size="30" />
														</td>
														<td>
															<a href="#" onclick="javascript: regenerateAPIsecret();"> <%=LNG_REGENERATE %></a>
														</td>
													</tr>
												</table>
												<h3><%=LNG_LANGUAGE %></h3>
												<%if DEMO=1 then %>
												    <div>
												        <p>The default language for DeskAlerts console cannot be changed in public demo version. You can, however, use it in one of the supported languages under your own credentials. To use the localized product, do the following:</p>
												        <ul>
													        <li>Create your own account using Publishers menu on the left - just go to Publishers list and add a new one for yourself.</li>
													        <li>Log out of a DeskAlerts console using Logout button at the top right of the screen, then log in with your new credentials.</li>
													        <li>Go to Settings->Profile settings and change the language there.</li>
												        </ul>
												    </div>
												<%else %>
												    <table border="0" style="width:770px">
													    <tr>
														    <td>&nbsp;</td>
														    <td width="150">
															    <label for="ConfDefaultLanguage"><%=LNG_DEFAULT_LANGUAGE %>:</label>
														    </td>
														    <td align="left">
															    <select id="ConfDefaultLanguage" name="ConfDefaultLanguage">
																    <%Set rsLang = Conn.Execute ("SELECT name, code FROM languages_list")
																	  this_lang="EN"
																	  do while not rsLang.EOF
																    %>
																        <option value="<%=HtmlEncode(rsLang("code")) %>" <% if(sConfDefaultLanguage=rsLang("code")) then 
																	        response.write "selected" 
																	        this_lang=rsLang("code")
																	        end if
																        %>>
																	        <%=HtmlEncode(rsLang("name")) %>
																        </option>
																    <%rsLang.MoveNext
																	  loop
																    %>
															    </select>
														    </td>
														    <td>
															    <font color="#aaaaaa"><%=LNG_LANG_DESC %>.</font>
														    </td>
														    <td width="100">
															    <a href="languages_edit.asp"><%=LNG_ADD_NEW %></a>
                                                                <br>
															    <a href="languages_edit.asp?lang=<%=this_lang %>"><%=LNG_MODIFY_THIS %></a>
														    </td>
													    </tr>
												    </table>
												<%end if %>
												<h3><%=LNG_DATE_AND_TIME%></h3>
												<table style="width:770px">
													<tr>
														<td>
															<label for="ConfDateFormat"><%=LNG_DATE_FORMAT%>:</label>
														</td>
														<td style="text-align: left">
															<select id="ConfDateFormat" name="ConfDateFormat">
																<option value="dd/MM/yyyy" <%if sConfDateFormat = "dd/MM/yyyy" then Response.Write "selected" end if%>>dd/mm/yyyy</option>
																<option value="yyyy/MM/dd" <%if sConfDateFormat = "yyyy/MM/dd" then Response.Write "selected" end if%>>yyyy/mm/dd</option>
																<option value="MM/dd/yyyy" <%if sConfDateFormat = "MM/dd/yyyy" then Response.Write "selected" end if%>>mm/dd/yyyy</option>
																<option value="yyyy/dd/MM" <%if sConfDateFormat = "yyyy/dd/MM" then Response.Write "selected" end if%>>yyyy/dd/mm</option>
																<option value="MM/yyyy/dd" <%if sConfDateFormat = "MM/yyyy/dd" then Response.Write "selected" end if%>>mm/yyyy/dd</option>
																<option value="dd/yyyy/MM" <%if sConfDateFormat = "dd/yyyy/MM" then Response.Write "selected" end if%>>dd/yyyy/mm</option>
																<option value="dd-MM-yyyy" <%if sConfDateFormat = "dd-MM-yyyy" then Response.Write "selected" end if%>>dd-mm-yyyy</option>
																<option value="yyyy-MM-dd" <%if sConfDateFormat = "yyyy-MM-dd" then Response.Write "selected" end if%>>yyyy-mm-dd</option>
																<option value="MM-dd-yyyy" <%if sConfDateFormat = "MM-dd-yyyy" then Response.Write "selected" end if%>>mm-dd-yyyy</option>
																<option value="yyyy-dd-MM" <%if sConfDateFormat = "yyyy-dd-MM" then Response.Write "selected" end if%>>yyyy-dd-mm</option>
																<option value="MM-yyyy-dd" <%if sConfDateFormat = "MM-yyyy-dd" then Response.Write "selected" end if%>>mm-yyyy-dd</option>
																<option value="dd-yyyy-MM" <%if sConfDateFormat = "dd-yyyy-MM" then Response.Write "selected" end if%>>dd-yyyy-mm</option>
															</select>
														</td>
													</tr>
													<tr>
														<td><%=LNG_TIME_FORMAT%>:</td>
														<td>
															<input id="ConfTimeFormat1" name="ConfTimeFormat" type="radio" value="1" <%if sConfTimeFormat = "1" then Response.Write "checked" end if%> /><label for="ConfTimeFormat1">24</label>
															<input id="ConfTimeFormat2" name="ConfTimeFormat" type="radio" value="2" <%if sConfTimeFormat = "2" then Response.Write "checked" end if%> /><label for="ConfTimeFormat2">AM/PM</label>
														</td>
													</tr>
													<tr>
														<td>
															<label for="ConfFirstDayOfWeek"><%=LNG_FIRST_DAY_OF_WEEK%>:</label>
														</td>
														<td>
															<select id="ConfFirstDayOfWeek" name="ConfFirstDayOfWeek">
																<option value="0" <%if sConfFirstDayOfWeek = "0" then Response.Write "selected" end if%>><%= LNG_SUNDAY %></option>
																<option value="1" <%if sConfFirstDayOfWeek = "1" then Response.Write "selected" end if%>><%= LNG_MONDAY %></option>
																<option value="2" <%if sConfFirstDayOfWeek = "2" then Response.Write "selected" end if%>><%= LNG_TUESDAY %></option>
																<option value="3" <%if sConfFirstDayOfWeek = "3" then Response.Write "selected" end if%>><%= LNG_WEDNESDAY %></option>
																<option value="4" <%if sConfFirstDayOfWeek = "4" then Response.Write "selected" end if%>><%= LNG_THURSDAY %></option>
																<option value="5" <%if sConfFirstDayOfWeek = "5" then Response.Write "selected" end if%>><%= LNG_FRIDAY %></option>
																<option value="6" <%if sConfFirstDayOfWeek = "6" then Response.Write "selected" end if%>><%= LNG_SATURDAY %></option>
															</select>
														</td>
													</tr>
													<tr>
														<td>
															<label for="ConfSessionTimeout"><%=LNG_SESSION_TIMEOUT%>:</label>
														</td>
														<td>
															<input id="ConfSessionTimeout" name="ConfSessionTimeout" type="text" value="<%=sConfSessionTimeout%>"style="width: 30px; margin: 2px">
															<label for="ConfSessionTimeout"><%=LNG_MINUTES%></label>
														</td>
													</tr>
												</table>
                                                <% if EM = 1 then %>
                                                <h3><%=LNG_SMTP_SETTINGS%></h3>
												<table style="width:770px">
													<tr>
														<td>
															<label for="SMTPServer"><%=LNG_SMTP_SERVER%>:</label>
														</td>
														<td style="text-align: left">
													        <input id="SMTPServer" name="SMTPServer" value="<%Response.Write(smtpServer)%>"> 
														</td>
													</tr>
                                                    <tr>
														<td>
															<label for="SMTPPort"><%=LNG_SMTP_PORT%>:</label>
														</td>
														<td style="text-align: left">
													        <input type="number" name="SMTPPort" id="SMTPPort" value="<%Response.Write(smtpPort)%>"> 
														</td>
													</tr>
                                                    <tr>
														<td>
															<label for="SMTPSSL"><%=LNG_SMTP_SSL%>:</label>
														</td>
														<td style="text-align: left">
													        <input type="checkbox" name="SMTPSSL" id="SMTPSSL" <%if smtpSSL = 1 then Response.Write("checked") end if%>> 
														</td>
													</tr>
                                                    <tr>
														<td>
															<label for="SMTPAuth"><%=LNG_SMTP_AUTH%>:</label>
														</td>
														<td style="text-align: left">
													        <input type="checkbox" name="SMTPAuth" id="SMTPAuth" <%if smtpAuth = 1 then Response.Write("checked") end if%>> 
														</td>
													</tr>
                                                    <tr>
														<td>
															<label for="SMTPUserName"><%=LNG_SMTP_USERNAME%>:</label>
														</td>
														<td style="text-align: left">
													        <input id="SMTPUserName" name="SMTPUserName" value="<%Response.Write(smtpUsername)%>"> 
														</td>
													</tr>
                                                    <tr>
														<td>
															<label for="SMTPPassword"><%=LNG_SMTP_PASSWORD%>:</label>
														</td>
														<td style="text-align: left">
													        <input type="password" name="SMTPPassword" id="SMTPPassword" value="<%Response.Write(smtpPassword)%>"> 
                                                            <input type="hidden" name="newPasswordListner" id="newPasswordListner" value="0">
														</td>
													</tr>
                                                    <tr>
														<td>
															<label for="SMTPFrom"><%=LNG_SMTP_FROM%>:</label>
														</td>
														<td style="text-align: left">
													        <input id="SMTPFrom" name="SMTPFrom" value="<%Response.Write(smtpFrom)%>"> 
														</td>
													</tr>
                                                    <tr>
														<td>
															<label for="SMTPConnectTimeout"><%=LNG_SMTP_CONNECT_TIMEOUT%>:</label>
														</td>
														<td style="text-align: left">
													        <input type="number" name="SMTPConnectTimeout" id="SMTPConnectTimeout" value="<%Response.Write(smtpConnectTimeout)%>"> 
														</td>
													</tr>
												</table>
                                                <%
                                                  end if 
                                                  if SMS = 1 then  
                                                %>
                                                <h3><%=LNG_SMS_SETTINGS%></h3>
												<table style="width:770px">
													<tr>
														<td>
															<label for="smsUrl"><%=LNG_SMS_URL%>:</label>
														</td>
														<td style="text-align: left">
													        <input id="smsUrl" name="smsUrl" value="<%Response.Write(smsUrl)%>" style="width:400px;"> 
                                                            <span id="checkSMSGateway" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="margin-left: 10px; padding: 5px;"><%=LNG_CHECK_SMS_GATEWAY%></span>
														</td>
													</tr>
                                                    <tr>
														<td>
															<label for="contentType"><%=LNG_CHOOSE_ALERT_TYPE%></label>
														</td>
														<td style="text-align: left">
                                                            <label><input type="radio" name="contentType" value="application/x-www-form-urlencoded" <%if smsContentType = "application/x-www-form-urlencoded" then Response.Write("checked") end if%>> Plain text </label>
													        <label><input type="radio" name="contentType" value="application/json" <%if smsContentType = "application/json" then Response.Write("checked") end if%>> JSON </label>
                                                            <label><input type="radio" name="contentType" value="application/xml" <%if smsContentType = "application/xml" then Response.Write("checked") end if%>> XML </label>
                                                            <div>Use %smsText% in place where your sms text should be.</div>
                                                            <div>Use %mobilePhone% in place where your recipient number should be.</div>
														</td>
													</tr>
                                                    <tr>
														<td>
															<label for="smsPostData"><%=LNG_POST_DATA%>:</label>
														</td>
														<td style="text-align: left">
													        <textarea style="resize:vertical; width:600px;" id="smsPostData" name="smsPostData"><%Response.Write(smsPostData)%></textarea>
														</td>
													</tr>
                                                    <tr>
														<td>
															<label for="smsUseAuth"><%=LNG_SMS_USE_AUTH%>:</label>
														</td>
														<td style="text-align: left">
													        <input type="checkbox" id="smsUseAuth" name="smsUseAuth" <%if (smsUseAuth = 1) then Response.Write("checked") end if%> >
                                                            <input id="smsAuthUser" name="smsAuthUser" placeholder="Login" value="<%Response.Write(smsAuthUser)%>">
                                                            <input type="password" id="smsAuthPass" name="smsAuthPass" placeholder="Password" value="<%Response.Write(smsAuthPass)%>">
														</td>
													</tr>
                                                    <tr>
                                                        <td><label>Gateway response:</label></td>
                                                        <td  id="smsResponse" style="border: 1px solid gray;"></td>
                                                    </tr>
												</table>
                                                <% end if %>
											</div>
										</td>
									</tr>
									<tr>
										<td align="right">
											<a class="cancel_button" onclick="javascript:history.go(-1); return false;">
												<%=LNG_CANCEL %>
											</a>
											<a class="save_button" onclick="saveSettings()">
												<%=LNG_SAVE %>
											</a>
											<input type="hidden" name="a" value="2">
										</td>
									</tr>
								</table>
							</form>
							<img src="images/inv.gif" alt="" width="1" height="100%" border="0">
						</div>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</body>
</html>
<%
else
	Response.Redirect "index.asp"
end if

%>
<!-- #include file="db_conn_close.asp" -->