<%
old_session_lcid = Session.LCID
'gb
Session.LCID = 2057
'gb

if ConnCount > 1 then
	Set Conn2 = Server.CreateObject("ADODB.Connection")
	Conn2.CommandTimeout = 30
	if Session("db_use_backup") <> 1 then
		Conn2.Open DBConnectionString
	else
		Conn2.Open Back_DBConnectionString
	end if
	Conn2.Execute("SET ANSI_WARNINGS OFF")
	Conn2.Execute("SET LANGUAGE " & language)
end if

if ConnCount > 2 then
	Set Conn3 = Server.CreateObject("ADODB.Connection")
	Conn3.CommandTimeout = 30
	if Session("db_use_backup") <> 1 then
		Conn3.Open DBConnectionString
	else
		Conn3.Open Back_DBConnectionString
	end if
	Conn3.Execute("SET ANSI_WARNINGS OFF")
	Conn3.Execute("SET LANGUAGE " & language)
end if

Set RS = Conn.Execute("SELECT GETDATE() as now_db, DATEDIFF(minute,GETUTCDATE(),GETDATE()) as timezone")
if(Not RS.EOF) then
	now_db=RS("now_db")
	timezone_db = RS("timezone")
	RS.Close
end if

if (now_db="") then 'something wrong with getdate
	'response.write "Can't get DB date"
	now_db=Now()
end if

%>
<!-- #include file="addons_trial.asp" -->