﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<!-- #include file="md5.asp" -->
<%

check_session()

uid = Session("uid")

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>DeskAlerts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/style9.css" rel="stylesheet" type="text/css">

<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$("#mail_link").click(function(e) {
			subject = "Purchase full versions of DeskAlerts add-ons";
			location.href = "mailto:sales@deskalerts.com?subject=" + encodeURIComponent(subject);
		});
	});
</script>

</head>

<body style="margin:0px" class="body">

<%
if (uid <> "")  then
%>
<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	<tr>
	<td>
	<table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
		<tr>
		<td width="100%" height="31" class="main_table_title"><a href="" class="header_title"><%=LNG_TRIAL_ADDONS_INFO %></a></td>
		</tr>
		<tr>
		<td class="main_table_body" height="100%">
			<div style="padding:10px;">
				<p><%=LNG_TRIAL_ADDONS_1 %></p>
				<ul>
				<%
					Dim addonName
					for each Addon in Addons
					Addon = Trim(Addon)
					select case Addon
						case "Active Directory"
							addonName="Active Directory add-on"
						case "SMS"
							addonName="SMS alert add-on"
						case "E-mail"
							addonName="E-mail alert add-on"
						case "Screensaver"
							addonName="Screensaver add-on"
						case "RSS"
							addonName="RSS add-on"
						case "Wallpaper"
							addonName="Corporate Wallpaper add-on"
						case "Blog"
							addonName="Blog add-on for WordPress"
						case "Twitter"
							addonName="Social add-on for Twitter"
						case "LinkedIn"
							addonName="Social add-on for LinkedIn"
						case "Fullscreen"
							addonName="Fullscreen alert add-on"
						case "Ticker"
							addonName="Scrolling ticker alert add-on"
						case "Ticker Pro"
							addonName="Scrolling ticker alert PRO add-on"
						case "Survey"
							addonName="Surveys add-on"
						case "Statistics"
							addonName="Extended statistics add-on"
						case "InstantAlerts"
							addonName="One Button Alerts"
						case else
							addonName=""
					end select
					Response.Write "<li>"&addonName&"</li>"
				next
				%>
				</ul>
				<p><%=LNG_TRIAL_ADDONS_2 %></p>
				<p style="color:red"><%=LNG_TRIAL_ADDONS_3 %><strong><%=TRIAL_DAYS_LEFT %></strong></p>
				<p><%=LNG_TRIAL_ADDONS_4 %><a href="#" id="mail_link"><em>sales@deskalerts.com</em></a></p>
			</div>
		</td>
		</tr>
	</table>
	</td>
	</tr>
</table>

</body>
</html>
<%

else

  Response.Redirect "index.asp"

end if

%> 
</body>
</html>
<!-- #include file="db_conn_close.asp" -->