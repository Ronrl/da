<!-- #include file="config.inc" -->
<% ConnCount = 2 %>
<!-- #include file="db_conn.asp" -->
<!-- #include file="md5.asp" -->
<!-- #include file="lang.asp" -->
<%

check_session()

%>

<%

  uid = Session("uid")

if (uid <> "") then

'modify input  form using javascript

'save alert and go to "send" page
if( Request("sub1") = "Add" OR Request("sub1") = "Edit") then

	    name=Request("name")
	    pass=Request("pass")

	    keep_password=Request("keep_password")

	    role="E"

	    policies=split(request("policy"),",")
		
		fl_check = 0
		Set rs_check = Conn.Execute("select id from users WHERE role!='U' AND name='" & Replace(name, "'", "''") & "'")
		if(rs_check.eof) then
			fl_check = 1
		else
			if(clng(rs_check("id"))=clng(request("id"))) then
				fl_check = 1
			end if
		end if
		
		if(fl_check = 1) then
			if(Request("sub1") = "Add") then 
				SQL = "INSERT INTO users (name, pass, role) VALUES (N'" & Replace(name, "'", "''") & "', '" & md5(pass) & "', '" & role & "')" 
				Conn.Execute(SQL)
				Set rs2 = Conn.Execute("select @@IDENTITY newID from users")
                editor_id=rs2("newID")
                SQL = "INSERT INTO widgets_editors (editor_id, widget_id, parameters, page) VALUES("& editor_id &", '071954af-ad48-4630-bd1e-8193691fc9a1', '', 'D')" & vbCRLF &_
                "INSERT INTO widgets_editors (editor_id, widget_id, parameters, page) VALUES("& editor_id &", 'e745170b-4243-4bec-a8b4-56def37dd7ae', '', 'D')" 
                
              '  if DEMO <> 1 then
                  '  SQL = SQL & vbCRLF &_
                 '   "INSERT INTO widgets_editors (editor_id, widget_id, parameters, page) VALUES("& editor_id &", '7d513b11-bbe8-45ee-937c-922adcfb107b', '', 'D')"
			'	end if
				
				SQL = SQL & "INSERT INTO widgets_editors (editor_id, widget_id, parameters, page) VALUES("& editor_id &", 'E9F8B3D3-B508-4288-A04E-2EBE49712E50', '', 'S')" & vbCRLF &_
				"INSERT INTO widgets_editors (editor_id, widget_id, parameters, page) VALUES("& editor_id &", '8FAEB4CE-542E-40D4-97DA-AC15CDCD145D', '', 'S')"
				Conn.Execute(SQL)
				rs2.close
			end if
			if(Request("sub1") = "Edit") then 
				if(keep_password="1") then
					SQL = "UPDATE users SET name = N'" & Replace(name, "'", "''") & "', role='" & role & "' WHERE id=" & Request("id") 
				else
					SQL = "UPDATE users SET name = N'" & Replace(name, "'", "''") & "', pass='" & md5(pass) & "', role='" & role & "' WHERE id=" & Request("id") 
				end if
				Conn.Execute(SQL)
				editor_id=clng(Request("id"))
			end if
			SQL = "UPDATE policy_editor SET was_checked=0 WHERE editor_id="&editor_id
			Conn.Execute(SQL)

			i_pol=0
			do while i_pol <= UBound(policies)
				policy_id=policies(i_pol)
				if(policy_id<>"") then
					Set RS1 = Conn.Execute("SELECT id FROM policy_editor WHERE editor_id=" & editor_id &" AND policy_id="&policy_id)
					if(RS1.EOF) then
						SQL = "INSERT INTO policy_editor (editor_id, policy_id, was_checked) VALUES (" & editor_id & ", "& policy_id &", 1)"
						Conn.Execute(SQL)
						
					else
						SQL = "UPDATE policy_editor SET was_checked=1 WHERE id=" & RS1("id")
						Conn.Execute(SQL)
						RS1.Close
					end if
				end if
				i_pol=i_pol+1
			loop
			SQL = "DELETE FROM policy_editor WHERE was_checked=0 AND editor_id=" & editor_id
			Conn.Execute(SQL)
			Response.Redirect "PublisherList.aspx"
		else
			error_descr = "Editor name '"&name&"' already exists. Please enter another name."
		end if
end if

id=Request("id")
full_checked=""
only_templates_checked=""
only_alerts_checked=""

if(id <> "") then
	Set RS = Conn.Execute("SELECT name, role, only_personal, only_templates, only_alerts FROM users WHERE id=" & id)
	my_val= "Edit"


	if(RS("role")="A") then
		full_checked="checked"
	end if
	if(RS("only_templates")=1) then
		only_templates_checked="checked"
	end if
	if(RS("only_alerts")=1) then
		only_alerts_checked="checked"
	end if

else  
	my_val= "Add"
	
	
	Set RS = Conn.Execute("SELECT type FROM policy WHERE id  IN (SELECT policy_id FROM policy_editor WHERE editor_id = " & uid & ")")
    if(not RS.EOF ) then
        if(RS("type")="A") then
		    full_checked="checked"
	    end if
	end if
	
	
end if

%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>DeskAlerts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/style9.css" rel="stylesheet" type="text/css">
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script language="javascript" type="text/javascript">

	$(document).ready(function() {
		$(".add_button").button();
		$(".save_button").button();
		$(".cancel_button").button();
		$("#edPolicy").change(function() {
			if ($(this).val() == "") {
				$("#editPolicyLink").css({
					"cursor": "default",
					"color": "gray",
					"text-decoration": "none"
				});
			}
			else {
				$("#editPolicyLink").css({
					"cursor": "pointer",
					"cursor": "hand",
					"color": "black",
					"text-decoration": "underline"
				});
			}
		});
	});

</script>
</head>
<script language="javascript">
function checkInput()
{
	var edName;
	var edPass;
	var keep_password;

	checked=1;
	edName=document.getElementById('edName').value;
	edPass=document.getElementById('edPass').value;
	keep_password=document.getElementById('keep_password').value;

	if(edName.length==0)
	{
	    checked = 0;  
	}
   
/*	if (edName == "admin" || edName == "Admin")
	{
	    alert('<%=LNG_PUBLISHER_NAME_WARNING %>');
	    return false;
	}*/
    
	if(edPass.length==0 && keep_password==0)
	{
		checked=0;
	}

	if(checked==1)
	{
		var radio=0;		
		if(document.getElementById("edPolicy").value > 0){
			radio = 1;
		}
		



		if(radio==0)
		{
			alert("<%=LNG_PLEASE_SELECT_POLICY_FOR_EDITOR %>.");
			return false;
		}
		else
		{
			return true;
		}
	}
	else
	{
		alert("<%=LNG_ADMI_EDIT_ALERT1 %>.");
		return false;
	}
}

function fullCheck()
{
	var disable = document.getElementById("full").checked;
	document.getElementById("only_templates").disabled=disable;
	document.getElementById("only_templates").checked=false;
	document.getElementById("only_alerts").disabled=disable;
	document.getElementById("only_alerts").checked=false;
	var policy_arr=document.getElementsByName("policies");
	for(var i=0; i < policy_arr.length; i++){
		policy_arr[i].disabled=disable;
		policy_arr[i].checked=false;
	}

}

function my_sub(i)
{
document.frm.sub1.value=i;
}

function show_password_field()
{

document.getElementById("password_field").style.display="";
document.getElementById("change_password_field").style.display="none";
document.getElementById("keep_password").value="0";

}

function hide_password_field()
{

document.getElementById("password_field").style.display="none";
document.getElementById("change_password_field").style.display="";
document.getElementById("keep_password").value="1";

}

</script>

<body style="margin:0px" class="body">
<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	<tr>
	<td>
	<table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
		<tr>
		<td width=100% height=31 class="main_table_title"><img src="images/menu_icons/editors_20.png" alt="" width="20" height="20" border="0" style="padding-left:7px">
		<a href="admin_admin.asp" class="header_title" style="position:absolute;top:15px"><%=LNG_EDITOR %></a></td>
		</tr>
		<tr>
		<td class="main_table_body" height="100%">
		<div style="margin:10px;">
		<% if id<>"" then %>
			<span class="work_header"><%=LNG_EDIT_EDITOR %></span>
		<%else %>
			<span class="work_header"><%=LNG_ADD_EDITOR %></span>
		<%end if %>
		<br><br>
<form name="frm" method="post" action="">
<table border="0"><tr><td>
<input type="hidden" name="sub1" value="<% Response.Write my_val %>"/>
<font color="red"><%=error_descr %></font>
<table cellpadding=4 cellspacing=4 border=0>
<tr><td><%=LNG_EDITOR_NAME%>:</td><td><input autocomplete="off" name="name" id="edName" type="text" value="<% if(id <> "") then Response.Write RS("name") end if %>" size="40" maxlength="255"> </td></tr>
<%
if(id<>"") then
%>
    

<tr id="change_password_field"><td colspan="2"><A href="#" onclick="javascript: show_password_field();"><%=LNG_CHANGE_PASSWORD %></a></tr>
<%
end if
%>

<tr id="password_field" 
<% 
if(id<>"") then
	response.write "style='display:none'"
end if
%>
><td><%=LNG_PASSWORD %>:</td><td><input autocomplete="off" name="pass" id="edPass" type="password" value="" size="40" maxlength="255">
<%
if(id<>"") then
%>
<A href="#" onclick="javascript:hide_password_field();"><%=LNG_NOT_CHANGE %></a>
<%
end if
%>
</td></tr>

<tr><td><%=LNG_POLICY %>:</td><td>
<select name="policy" id="edPolicy" style="width:150px">
<option value=""><%=LNG_SELECT_POLICY %></option>

<%

query = "SELECT id, name FROM policy"

if(full_checked <> "checked" and ConfEGSModerator = 1) then
    query = query & " WHERE type != 'A'"
end if
    query = query & " ORDER BY name"
Set RSPolicy = Conn.Execute(query)
Do While Not RSPolicy.EOF
policy_id = RSPolicy("id")
policy_name = RSPolicy("name")
policy_checked = ""
if(id <> "") then
	Set rsEditor = Conn2.Execute("SELECT id FROM policy_editor WHERE policy_id="&policy_id&" AND editor_id="&id)
	if(Not rsEditor.EOF) then
		policy_checked = "selected"
	end if
end if
%>
<option <%=policy_checked %> value="<%=policy_id %>"> <%=HtmlEncode(policy_name) %> </option>
<%
	RSPolicy.MoveNext
loop
%>
</select>
<a <% if id="" then Response.write("style='cursor:default; color:gray; text-decoration:none'") else Response.write("") end if%> id="editPolicyLink" href="#" onclick="var val = document.getElementById('edPolicy').value; if(val) window.open('policy_edit.asp?close=1&id=' + val,'',555,450); return false;"><%=LNG_EDIT %></a>
</td></tr>
</table>

</td></tr>
<tr><td align="right">
<%
if(id <> "") then
%>
<button type="submit" class="cancel_button" onclick="javascript:history.go(-1); return false;"><%=LNG_CANCEL %></button> <button class="save_button" type="submit" onclick="javascript: return checkInput();"><%=LNG_SAVE %></button>
<%
Response.Write "<input type=""hidden"" name=""id"" value=""" & id & """/>"

Response.Write "<input type=""hidden"" id=""keep_password"" name=""keep_password"" value=""1""/>"


else

Response.Write "<input type=""hidden"" id=""keep_password"" name=""keep_password"" value=""0""/>"

%>
<button type="submit" class="cancel_button" onclick="javascript:history.go(-1); return false;"><%=LNG_CANCEL %></button> <button class="add_button" type="submit" onclick="javascript: return checkInput();"><%=LNG_ADD %></button>
<%
end if
%>
</td></tr></table>
</form>
		 <img src="images/inv.gif" alt="" width="1" height="100%" border="0">
		</div>
		</td>
		</tr>
	</table>
	</td>
	</tr>
</table>

<%

else

  Response.Redirect "index.asp"

end if
%> 
</body>
</html>
<!-- #include file="db_conn_close.asp" -->