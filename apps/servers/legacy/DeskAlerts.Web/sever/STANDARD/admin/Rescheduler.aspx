﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Rescheduler.aspx.cs" Inherits="DeskAlertsDotNet.Pages.Rescheduler" %>
<%@ Import NameSpace="DeskAlertsDotNet.Parameters" %>
<%@ Import Namespace="Resources" %>
<%@ Register src="ReccurencePanel.ascx" TagName="ReccurencePanel"
   TagPrefix="da"  %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="css/style9.css" rel="stylesheet" type="text/css">
    <link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
    <link href="css/form_style.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript" src="functions.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
    <script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$(".save_button").button();
		$(".close_button").button();
		$(".save_and_next_button").button({
			icons: {
				secondary: "ui-icon-carat-1-e"
			}
		});

		$(".reccurenceControl").prop('disabled', false);
		$(".reccurenceControl input").prop('disabled', false);

	});

	function toggleDisabled(el, state) {
	    try {
	        el.disabled = state;
	    }
	    catch (E) {
	    }
	    if (el && el.childNodes && el.childNodes.length > 0) {
	        for (var x = 0; x < el.childNodes.length; x++) {
	            toggleDisabled(el.childNodes[x], state);
	        }
	    }
	}

        function showPrompt(text) {
            $(function() {
                $("#dialog-modal > p").text(text);
                $("#dialog-modal")
                    .dialog({
                        height: 180,
                        modal: true,
                        resizable: false,
                        buttons: {
                            "<%=resources.LNG_CLOSE %>": function() {
                                $(this).dialog("close");
                            }
                        }
                    });
            });
        }

        function submitForm() {

            $(function () {

                $("#form1").submit();

            });
        }

        

    </script>
    
<style>
	html, body
	{
		margin:0px;
		border:0px;
	}
</style>

</head>
<body>
    <form id="form1" runat="server">
      <asp:ScriptManager ID="ScriptManager1" runat="server">
      </asp:ScriptManager>

    <div>
         <da:ReccurencePanel ID="schedulePanel" runat="server"></da:ReccurencePanel>
    </div>
    <table width="100%">
	    <tr>
	        <td>
	            <asp:LinkButton runat="server" id="saveButton" class="save_button" ><%=resources.LNG_SAVE %></asp:LinkButton>
	        </td>
        </tr>
      </table>
        <input type="hidden" runat="server" id="alertId" />
       <asp:UpdatePanel runat="server" id="updatePanel">
           <ContentTemplate>

            </ContentTemplate>
       </asp:UpdatePanel>
        <div id="dialog-modal" style="display: none">
            <p style="vertical-align: middle">
            </p>
        </div>

    </form>
</body>
</html>
