<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<!-- saved from url=(0026)http://www.softomate.com -->
<html>
<head>
	<title>DeskAlerts - Registration</title>
	<link rel="stylesheet" type="text/css" href="skin.css">
	<script language="JavaScript" type="text/JavaScript">
		function Close()
		{
			//window.external.Close();
			window.parent.document.getElementById("preview_iframe").style.display = "none"; //for server
			return false;
		}
		function captureMouse(e)
		{
			if(!whichElement(e))
			{
//				window.external.capturemouse();
				return true;
		}
			return true;
		}
		function whichElement(e)
		{
			var targ;
			if (!e) e = window.event;
			if (e.target) targ = e.target;
			else if (e.srcElement) targ = e.srcElement;
			if (targ.nodeType == 3) // defeat Safari bug
				targ = targ.parentNode;
			var tag = targ.tagName.toUpperCase();
			var type = targ.getAttribute("type");type=type?type.toLowerCase():null;
			return (tag=="INPUT" && (type=="text" || type=="password")) || tag=="TEXTAREA" ||
					(targ.onclick && (e.type == "click" || e.type == "mousedown" || e.type == "mouseup")) ? true : false;
		}
		
		function changeColSpan()
		{
			var decrement = 0;
			var td02 = document.getElementById('td02');
			var td04 = document.getElementById('td04');
			
			var displayStyleTd02 = (typeof(window.getComputedStyle) != 'undefined') ? window.getComputedStyle(td02, null).display : td02.currentStyle.display;
			var displayStyleTd04 = (typeof(window.getComputedStyle) != 'undefined') ? window.getComputedStyle(td04, null).display : td04.currentStyle.display;
			if(displayStyleTd02 == 'none') decrement++;
			if(displayStyleTd04 == 'none') decrement++;
			document.getElementById('td07').colSpan -= decrement;
		}
		
		function OnPageLoad()
		{
			changeColSpan();
			document.getElementById('img03').width = document.getElementById('td03').clientWidth;
			document.getElementById('img06').width = document.getElementById('td06').clientWidth;
			document.getElementById('img08').width = document.getElementById('td08').clientWidth;
			document.getElementById('img11').width = document.getElementById('td11').clientWidth;
			document.getElementById('img03').height = document.getElementById('td03').clientHeight;
			document.getElementById('img06').height = document.getElementById('td06').clientHeight;
			document.getElementById('img08').height = document.getElementById('td08').clientHeight;
			document.getElementById('img11').height = document.getElementById('td11').clientHeight;
		}
	</script>
</head>
<body scroll="no" class="alert" onload="OnPageLoad();" style="cursor:default; margin:0px; padding:0px; -moz-user-select:none; -khtml-user-select:none; user-select:none;" onmousedown="return captureMouse(event)" ondragstart="return captureMouse(event)" onselectstart="return whichElement(event)" oncontextmenu="return whichElement(event)">
	<img id="close_img" class="close_img" src="close.gif" style="cursor:pointer;cursor:hand;position:absolute;z-index:900" alt="x" title="" onclick="Close();"/>
	
	<div id="contentDiv1" class="contentDiv1" style="position:absolute;"></div>
	<div id="contentDiv2" class="contentDiv2" style="position:absolute;"></div>
	<div id="contentDiv3" class="contentDiv3" style="position:absolute;"></div>
	<div id="contentDiv4" class="contentDiv4" style="position:absolute;"></div>

	<div id="caption" class="caption" style="position:absolute;z-index:200"><b><%=request("new_alert") %>!</b></div>

	<table id="contentTable" class="contentTable" width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" style="z-index:150">
		<tr id="tr1" class="tr1">
			<td id="td01" class="td01"><img id="img01" class="img01" src="img_01.gif" alt=""/></td>
			<td id="td02" class="td02"><img id="img02" class="img02" src="img_02.gif" alt=""/></td>
			<td id="td03" class="td03" width="100%" style="background:URL('img_03.gif')"><img id="img03" class="img03" src="img_03s.gif" width="1px" height="1px" alt=""/></td>
			<td id="td04" class="td04"><img id="img04" class="img04" src="img_04.gif" alt=""/></td>
			<td id="td05" class="td05"><img id="img05" class="img05" src="img_05.gif" alt=""/></td>
		</tr>
		<tr id="tr2" class="tr2" height="100%" valign="top">
			<td id="td06" class="td06" height="100%" style="background:URL('img_06.gif');"><img id="img06" class="img06" src="img_06s.gif" width="1px" height="1px" alt=""/></td>
			<td id="td07" class="td07" height="100%" colspan="3"><iframe width="100%" height="100%" STYLE="z-index:800;" src="<%=request("src") %>" frameborder="0"></iframe></td>
			<td id="td08" class="td08" height="100%" style="background:URL('img_08.gif');"><img id="img08" class="img08" src="img_08s.gif" width="1px" height="1px" alt=""/></td>
		</tr>
		<tr id="tr3" class="tr3">
			<td id="td09" class="td09"><img id="img09" class="img09" src="img_09.gif" alt=""/></td>
			<td id="td10" class="td10"><img id="img10" class="img10" src="img_10.gif" alt=""/></td>
			<td id="td11" class="td11" width="100%" style="background:URL('img_11.gif');"><img id="img11" class="img11" src="img_11s.gif" width="1px" height="1px" alt=""/></td>
			<td id="td12" class="td12"><img id="img12" class="img12" src="img_12.gif" alt=""/></td>
			<td id="td13" class="td13"><img id="img13" class="img13" src="img_13.gif" alt=""/></td>
		</tr>
	</table>
	<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" style="position:absolute;z-index:170">
		<tr><td width="100%" height="100%" id="borderDiv" class="borderDiv">&nbsp;</td></tr>
	</table>
</body>
</html>