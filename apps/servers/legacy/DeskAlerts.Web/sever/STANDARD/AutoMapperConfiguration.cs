﻿using DeskAlerts.ApplicationCore.Dtos.AD;
using DeskAlertsDotNet.DataBase;

namespace DeskAlerts.Server
{
    using AutoMapper;
    using Service.AD.Profiles;

    public class AutoMapperConfiguration
    {
        public static void Configure()
        {
            Mapper.Initialize(x =>
            {
                x.AddProfile<ActiveDirectoryProfile>();
                x.CreateMap<DataRow, OUDto>()
                    .ForMember("Name", opt => opt.MapFrom(src => src.GetString("Name")))
                    .ForMember("Id", opt => opt.MapFrom(src => src.GetInt("id")))
                    .ForMember("DomainId", opt => opt.MapFrom(src => src.GetInt("Id_domain")))
                    .ForMember("OuId", opt => opt.MapFrom(src => src.GetInt("OU_id")))
                    .ForMember("ParentId", opt => opt.MapFrom(src => src.GetInt("Id_parent")));
            });
        }
    }
}
