﻿<% 

    redirectUrl = "RegisterForm.aspx"

    
    if Request.QueryString.Count > 0 then
         redirectUrl = redirectUrl & "?" & Request.QueryString
    else
        qString = "uname=" & request("uname") & "&"
        qString = qString & "upass=" & request("upass") & "&"
        qString = qString & "desk_id=" & request("desk_id") & "&"
        qString = qString &  "client_id=" & request("client_id") & "&"
        
        if request("domain") = "" Then
            qString = qString &  "domain=" & request("domain_name") & "&"
        else
            qString = qString &  "domain=" & request("domain") & "&"
        end if

        Response.Redirect (redirectUrl & "?" & qString)
    end if

    if disable_uname_change = 1 then
        redirectUrl = redirectUrl & "&disableUnameChange=1"
    end if

    Response.Redirect redirectUrl

    db_backup_stop_working = 1 %>
<!-- #include file="admin/config.inc" -->
<!-- #include file="admin/db_conn.asp" -->
<!-- #include file="admin/lang.asp" -->
<!-- #include file="admin/md5.asp" -->
<%
Response.AddHeader "content-type", "text/html; charset=utf-8"
syncFree = Request("syncFree")
if(AD = 3 and ConfEnableRegAndADEDMode <> 1 and syncFree <> "1") then
	if(EDIR=1) then
		%>
		<!-- #include file="not_edir.asp" -->
		<%
	else
		%>
		<!-- #include file="not_ad.html" -->
		<%
	end if
	if ConfEnableADEDCheck <> 1 then
%>
<!--script type="text/javascript">
	setTimeout(function()
	{
		try {
			if(window.external) {
				window.external.exit();
			}
		} catch(e) {}
	}, 5000);
</script-->
<%
	end if
	Response.End
end if

	if isEmpty(uname) then 'is uninitialized
		uname=Request("uname")
	end if
	if isEmpty(upass) then 'is uninitialized
		upass=Request("upass")
	end if
	uphone=Request("uphone")
	uemail=Request("uemail")
	desk_id=Request("deskbar_id")
	if(desk_id = "") then
		desk_id=Request("desk_id")
	end if
	client_id=Request("client_id")
	
	domain_name=Request("domain_name") 'syncfree stuff
	
	special_id=""

	if(client_id <> "") then
		SQL6 = "SELECT id FROM groups WHERE special_id=" & client_id
		Set RS6 = Conn.Execute(SQL6)
		if(Not RS6.EOF) then
			special_id=RS6("id")
		end if
	end if
	
	if(uname <> "") then

		'if AD = 3 then
			Set RS_Domain = Conn.Execute("SELECT id FROM domains WHERE name='"&domain_name&"'")
			if Not RS_Domain.EOF then
				domain_id = RS_Domain("id")
			end if
			
		'end if
		if domain_name="" then
			if domain_id <> "" then
				domain_where = "=" & domain_id & " OR domain_id "
			else
				domain_id = "NULL"
			end if
			domain_where = domain_where & " IS NULL"

			SQL1 = "SELECT id, pass, email, mobile_phone FROM users WHERE name=N'" & Replace(uname, "'", "''") &"' AND role='U' AND (domain_id"&domain_where&")"
			if ConfForceSelfRegWithAD = 1 then
				SQL1 = "SELECT id, pass, email, mobile_phone FROM users WHERE name=N'" & Replace(uname, "'", "''") &"' AND role='U' AND domain_id IS NOT NULL AND domain_id <> 1"
			end if
		else 'more syncfree stuff

			if RS_Domain.EOF then

				Conn.Execute("INSERT INTO domains (name, was_checked) values ('"&domain_name&"',1)") 'add domain if there wasn't one
				RS_Domain = Conn.Execute("SELECT SCOPE_IDENTITY() as id") '??
				domain_id = RS_Domain("id")
			end if
			SQL1 = "SELECT id, pass, email, mobile_phone FROM users WHERE name=N'" & Replace(uname, "'", "''") &"' AND role='U' AND domain_id="&domain_id
		end if

		Set RS = Conn.Execute(SQL1)
		if(Not RS.EOF) then
			user_id = RS("id")
			if(md5(upass)=RS("pass") OR isNull(RS("pass"))) then
				'if Request("form") <> "1" then
					if uphone = "" then
						uphone=RS("mobile_phone")
					end if
					if uemail = "" then
						uemail=RS("email")
					end if
				'end if
				if(special_id <> "") then
					SQL11 = "UPDATE users SET deskbar_id='" & Replace(desk_id, "'", "''") & "', mobile_phone='" & Replace(uphone, "'", "''") & "', email=N'"&Replace(uemail, "'", "''")&"', context_id=-1 WHERE id="& user_id
					SQL22 = "INSERT INTO users_groups (user_id, group_id) VALUES (" &user_id &", "& special_id &")"
					Conn.Execute(SQL22)
				else
					SQL11 = "UPDATE users SET deskbar_id='" & Replace(desk_id, "'", "''") & "', mobile_phone='" & Replace(uphone, "'", "''") & "', email=N'"&Replace(uemail, "'", "''")&"', context_id=-1 WHERE id="& user_id
				end if
				Conn.Execute(SQL11)
				successmsg = LNG_YOUR_OLD_INFORMATION_WAS_RESTORED&"!"
				successmsg_eng = "Thank you for signing up with existing account!"
			else
				errormsg = LNG_ERROR_THIS_USER_NAME_ALREADY_EXISTS&"."
				errormsg_eng = "Error! The username is taken, password provided is incorrect"
			end if
		else
			if ConfForceSelfRegWithAD = 1 then
				errormsg = LNG_AD_SELFREG_ERROR
				errormsg_eng = "Provided credentials are not found in AD domain synchronized with DeskAlerts"
			else
				SQL11 = "SELECT id FROM users WHERE deskbar_id='" & Replace(desk_id, "'", "''") &"' AND role='U'"
				Set RS11 = Conn.Execute(SQL11)
				if(Not RS11.EOF) then
					user_id = RS11("id")
					SQL = "UPDATE users SET name=N'" & Replace(uname, "'", "''") & "', pass='" &md5(upass)& "', mobile_phone='" & Replace(uphone, "'", "''") & "', email=N'"&Replace(uemail, "'", "''")&"', domain_id="&domain_id&", context_id=-1 WHERE id="& user_id
					Conn.Execute(SQL)
				else
					SQL = "INSERT INTO users (name, pass, deskbar_id, context_id, mobile_phone, email, alert_id, reg_date, last_date, last_request, role, group_id, domain_id) VALUES (N'" & Replace(uname, "'", "''") & "', '" & md5(upass) & "', '" & Replace(desk_id, "'", "''") & "', -1,'" & Replace(uphone, "'", "''") & "', N'"&Replace(uemail, "'", "''")&"', '|', GETDATE(), GETDATE(), GETDATE(), 'U', 0, "&domain_id&")"
					Conn.Execute(SQL)
					Set rs111 = Conn.Execute("select @@IDENTITY newID from users")
					user_id=rs111("newID")
					rs111.Close
					if(special_id <> "") then
						SQL22 = "INSERT INTO users_groups (user_id, group_id) VALUES (" & user_id &", "& special_id &")"
						Conn.Execute(SQL22)
					end if
				end if
				successmsg = LNG_REGISTRATION_WAS_SUCCESSFULLY_COMPLETED&"."
				successmsg_eng = "Registration was successfully completed."
			end if
		end if
	elseif Request("form") = "1" then
		errormsg = LNG_ERROR_USER_NAME_SHOULD_BE_MORE_THEN_SYMBOLS&"."
		errormsg_eng = "Error! User name should be more then 4 symbols."
	end if
sharepoint = Request("sharepoint")
if sharepoint <> "" then
    if(successmsg <> "") then
        successmsg = "<h3>The recipient name """&uname&""" has been registered.</h3><p>Now you can send notifications to your SharePoint by targeting this username.</p>"
        Response.Write(successmsg)
    elseif(errormsg <> "") then
        errormsg = "<h3>The recipient name """&uname&""" is already taken. </h3><p>Please change your recipient name in the settings of SharePoint DeskAlerts app. User name should be more then 4 symbols.</p>"
        Response.Write(errormsg)
    end if 
else
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<title>DeskAlerts - Desktop Alert Software</title>
		<link href="admin/style.css" rel="stylesheet" type="text/css">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	</head>
	<body class="body" scroll="no">
		<table height="100%" border="0" align="center" cellspacing="0" cellpadding="0">
		<tr>
			<td align="center">
				<% if(successmsg <> "") then %>
					<script type="text/javascript">
						try {
							if(window.external) {
								window.external.setProperty('user_name','<%=Replace(uname, "'", "\'") %>');
								window.external.setProperty('user_id','<%=Replace(user_id, "'", "\'") %>');
								window.external.setProperty('email','<%=Replace(Replace(uemail, ConfDefaultEmail, ""), "'", "\'") %>');
								window.external.setProperty('phone','<%=Replace(uphone, "'", "\'") %>');
								window.external.setProperty('modmail','<% if isEmpty(EM) then Response.Write "-1" else Response.Write EM end if %>');
								window.external.setProperty('modsms','<% if isEmpty(SMS) then Response.Write "-1" else Response.Write SMS end if %>');
								<% if(disable_uname_change=1) then %>
								window.external.setProperty('isSilent','1');
								<% else %>
								window.external.setProperty('isSilent','0');
								<% end if %>
								window.external.reloadAlert();
							}
						} catch(e) {}
						function DeskAlertsInit(tool)
						{
							if(tool)
							{
								tool.prop("user_name") = "<% Response.Write uname %>";
								window.external.сlose();
							} 
						}
						function Close()
						{
							try {
								window.external.сlose();
							} catch(e) {}
							return false;
						}
					</script>
					<div style="margin: 20px 10px 10px 10px;">
						<br/><br/>
						<!-- <%= successmsg_eng %> -->
						<p><strong><%= successmsg %></strong></p>
						<input type="image" src="<% Response.Write alerts_folder %>admin/images/langs/<%=default_lng %>/close_button.gif" alt="<%=LNG_CLOSE %>" title="<%=LNG_CLOSE %>" border="0" onclick="return Close();">
					</div>
				<% else %>
					<% if(errormsg <> "") then %>
						<!-- <%= errormsg_eng %> -->
						<p><strong><%= errormsg %></strong></p>
					<% end if %>
					<form method="post" action="<% Response.Write alerts_folder %>register.asp" style="overflow:hidden;">
						<div style="margin: 20px 10px 10px 10px;">
							<table cellpadding=0 cellspacing=4 border=0>
								<tr><td class="body"><%=LNG_USERNAME %>:</td><td><input name="uname" style="width:150px;" type="text" value="<% Response.Write HtmlEncode(uname) %>"/></td></tr>
								<tr><td class="body"><%=LNG_PASSWORD %>:</td><td><input name="upass" style="width:150px;" type="password"/></td></tr>
								<% if(SMS=1 AND ConfForceSelfRegWithAD <> 1) then %>
								<tr><td class="body"><%=LNG_MOBILE_PHONE %>:</td><td><input name="uphone" style="width:150px;" type="text" value="<% Response.Write HtmlEncode(uphone) %>"/></td></tr>
								<% end if %>
								<% if(EM=1  AND ConfForceSelfRegWithAD <> 1) then %>
								<tr><td class="body"><%=LNG_EMAIL %>:</td><td><input name="uemail" style="width:150px;" type="text" value="<% Response.Write HtmlEncode(uemail) %>"/></td></tr>
								<% end if %>
								<tr><td colspan=2 align=right><input type="image" src="<% Response.Write alerts_folder %>admin/images/langs/<%=default_lng %>/save.gif" alt="<%=LNG_SAVE %>" title="<%=LNG_SAVE %>" border="0"></td></tr>
							</table>
							<input type="hidden" name="deskbar_id" value="<% Response.Write HtmlEncode(desk_id) %>"/>
							<input type="hidden" name="client_id" value="<% Response.Write HtmlEncode(client_id) %>"/>
							<input type="hidden" name="form" value="1"/>
						</div>
					</form>
				<% end if %>
			</td>
		</tr>
		</table>
	</body>
</html>
<%end if %>
<!-- #include file="admin/db_conn_close.asp" -->