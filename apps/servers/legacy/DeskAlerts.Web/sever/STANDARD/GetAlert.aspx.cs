﻿using DeskAlerts.Server.sever.MODULES.VIDEOALERT;
using DeskAlerts.Server.Utils;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace DeskAlertsDotNet.Pages
{
    using DataBase;
    using DeskAlerts.ApplicationCore.Encryption;
    using NLog;
    using Parameters;
    using System;
    using System.Data;
    using System.Text;
    using System.Web;
    using System.Web.UI;
    using Utils.Factories;

    public partial class GetAlert : Page
    {
        private static readonly string EncryptKey = Config.Data.GetString("ENCRYPT_KEY");
        private Logger _logger = LogManager.GetCurrentClassLogger();

        protected override void OnLoad(EventArgs e)
        {
            using (var dbManager = new DBManager())
            {
                base.OnLoad(e);
                int alertId = Convert.ToInt32(this.Request["id"]);
                string localtime = this.Request["localtime"];
                this.Response.Expires = 0;
                bool isMobile = !string.IsNullOrEmpty(this.Request["isMobile"]) && this.Request["isMobile"] == "1";
                bool isNewClient = !string.IsNullOrEmpty(this.Request["newClient"]) && this.Request["newClient"] == "1";
                int userId;
                var userName = HttpUtility.UrlDecode(Request["user_name"]);

                if (string.IsNullOrEmpty(this.Request["user_id"]) && !string.IsNullOrEmpty(userName))
                {
                    var selectUserNameParam = SqlParameterFactory.Create(DbType.String, userName, "userName");
                    string getUserIdByNameQuery =
                        $"SELECT id FROM users WHERE name = @userName and role = \'U\'";
                    userId = dbManager.GetScalarByQuery<int>(getUserIdByNameQuery, selectUserNameParam);
                }
                else
                {
                    userId = Convert.ToInt32(this.Request["user_id"]);
                }

                string deskbarId = this.Request["deskbar_id"];

                _logger.Trace($"User {userId} requested alert by id {alertId} from deskbar id {deskbarId}");

                string script = @"<script language='javascript' type='text/javascript'>
                            function checkSubmit(){
                            	var found = false, valid = false, answersForm = document.getElementsByTagName('input');
                            	for(var i=0; i<answersForm.length; i++){
                            		if(answersForm[i].name=='answer'){
                            			if(answersForm[i].type == 'text'){
                            				if(!answersForm[i].value){
                            					alert('You should enter a text.');
                            					if(answersForm[i].focus) answersForm[i].focus();
                            					return false;
                            				}
                            			} else {
                            				found = true;
                            				if(answersForm[i].checked){
                            					valid = true;
                            				}
                            			}
                            		}
                            	}
                            	if(found && !valid) { alert('You should select an answer.'); return false; }
                            	if(document.getElementById('custom_answer')!=null) { if(!document.getElementById('custom_answer').value) { alert('You should enter a text.'); return false; } }
                            	return true;
                            }
                            </script>";

                string getAlertDataSetQuery =
                    $"SELECT alert_text, template_id, create_date, class FROM alerts WHERE id={alertId}";
                var alertDataSet = dbManager.GetDataByQuery(getAlertDataSetQuery);
                
                if (alertDataSet.IsEmpty) 
                { 
                    return;
                }

                string text = alertDataSet.GetString(0, "alert_text");

                text = ReplaceServerUri(text);
                
                int alertClass = alertDataSet.GetInt(0, "class");

                string getSurveyIdQuery =
                    $"SELECT id FROM surveys_main WHERE closed = \'A\' and sender_id = {this.Request["id"]}";
                var surveySet = dbManager.GetDataByQuery(getSurveyIdQuery);

                if (!surveySet.IsEmpty)
                {
                    int surveyId = surveySet.GetInt(0, "id");
                    var questionsSet = dbManager.GetDataByQuery(
                        $"SELECT id, question FROM surveys_questions WHERE survey_id = {surveyId}");
                    string questionText = questionsSet.GetString(0, "question");
                    var answersSet = dbManager.GetDataByQuery(
                        $"SELECT id, answer FROM surveys_answers WHERE question_id = {questionsSet.GetString(0, "id")}");
                    string firstAnswer = answersSet.GetString(0, "answer");
                    string firstAnswerId = answersSet.GetString(0, "id");
                    string secondAnswer = answersSet.GetString(1, "answer");
                    string secondAnswerId = answersSet.GetString(1, "id");
                    string secondQuestion = string.Empty;
                    string secondQuestionId = string.Empty;
                    if (questionsSet.Count > 1)
                    {
                        secondQuestion = questionsSet.GetString(1, "question");
                        // ReSharper disable once RedundantAssignment
                        secondQuestionId = questionsSet.GetString(1, "id");
                    }

                    text += script + "<hr/><form action='" + Config.Data.GetString("alertsDir")
                            + "/rsvp_vote.asp?survey_id=" + surveyId + "&user_id=" + userId + "&deskbar_id=" + deskbarId
                            + "' method='POST'>";
                    text += "<p style='font-family: arial,helvetica,sans-serif;'>" + questionText + "</p>";
                    text += "<br/>";
                    text +=
                        "<table border='0'><tr><td style='width:10px'><input type='radio' name='answer' id='answer1' value='"
                        + firstAnswerId + "'/></td><td align='left'>";
                    text += "	<label for='answer1' style='font-family: arial,helvetica,sans-serif;'>" + firstAnswer + "</label>";
                    text += "</td></tr>";
                    text += "<tr><td style='width:10px'><input type='radio' name='answer' id='answer2' value='"
                            + secondAnswerId + "'/></td><td align='left'>";
                    text += "	<label for='answer2' style='font-family: arial,helvetica,sans-serif;'>" + secondAnswer + "</label>";
                    text += "</td></tr></table>";

                    if (secondQuestion.Length > 0)
                    {
                        text += "<br/>";
                        text += "<p style='font-family: arial,helvetica,sans-serif;'>" + secondQuestion + "</p>";
                        text += "<br/>";
                        text += "<textarea cols='50' rows='5' id='custom_answer' name='custom_answer'></textarea><br/>";
                    }

                    text +=
                        "<br/><input type='image' src='admin/images/langs/EN/submit_button.gif' name='submit' value='Submit' onclick='return checkSubmit();'></form>";
                }

                string userNameText = dbManager.GetScalarByQuery<string>($"SELECT name FROM users WHERE id={userId}");
                text = text.Replace("%username%", userNameText);
                int startIndex = 0;
                while (startIndex != -1 || startIndex <= text.Length)
                {
                    int linkStart = text.IndexOf("src=\"../../", startIndex, StringComparison.Ordinal);
                    if (linkStart == -1)
                    {
                        break;
                    }

                    int linkEnd = text.IndexOf("admin/images/upload", linkStart, StringComparison.Ordinal);
                    startIndex = linkEnd;
                    if (linkEnd == -1)
                    {
                        break;
                    }

                    string replacement = text.Substring(linkStart, linkEnd - linkStart);
                    text = text.Replace(replacement, "src=\"" + Config.Data.GetString("alertsDir"));
                }

                startIndex = 0;
                while (startIndex != -1 || startIndex <= text.Length)
                {
                    int linkStart = text.IndexOf("href=\"../../", startIndex, StringComparison.Ordinal);

                    if (linkStart == -1)
                    {
                        break;
                    }

                    int linkEnd = text.IndexOf("admin/images/upload", linkStart, StringComparison.Ordinal);
                    startIndex = linkEnd;

                    if (linkEnd == -1)
                    {
                        break;
                    }

                    string replacement = text.Substring(linkStart, linkEnd - linkStart);

                    text = text.Replace(replacement, "src=\"" + Config.Data.GetString("alertsDir"));
                }

                var isVideoAlert = text.Contains("%video%=");
                if (isVideoAlert)
                {
                    text = DeskAlertsUtils.GenerateVideoAlertHtml(text);
                }

                int ssVideoIndex = text.IndexOf("%ss_video%=|", StringComparison.Ordinal);
                if (ssVideoIndex > -1)
                {
                    int startLinkPosition = text.IndexOf("|", ssVideoIndex + 12, StringComparison.Ordinal);
                    int startWidthPosition = text.IndexOf("|", startLinkPosition + 1, StringComparison.Ordinal);
                    int startHeightPosition = text.IndexOf("|", startWidthPosition + 1, StringComparison.Ordinal);
                    string link = text.Substring(ssVideoIndex + 11, startLinkPosition - ssVideoIndex - 11);
                    string videoWidth = text.Substring(startLinkPosition + 1, startWidthPosition - startLinkPosition - 1);
                    string videoHeight = text.Substring(
                        startWidthPosition + 1,
                        startHeightPosition - startWidthPosition - 1);
                    string videoHtmlCode = "<OBJECT  ID='mediaPlayer' width='" + videoWidth + "' height='" + videoHeight +
                                           "'  CLASSID='CLSID:6BF52A52-394A-11d3-B153-00C04F79FAA6' CODEBASE='http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#Version=6,4,5,715' STANDBY='Loading Microsoft Windows Media Player components...'  TYPE='application/x-oleobject'><PARAM Name = 'uiMode' Value = 'none'><PARAM NAME='url' VALUE='" +
                                           link +
                                           "'><PARAM NAME='autoStart' VALUE='true'><PARAM NAME='showControls' VALUE='true'><PARAM NAME='TransparentatStart' VALUE='false'><PARAM NAME='AnimationatStart' VALUE='true'><PARAM NAME='StretchToFit' VALUE='true'><PARAM NAME='PlayCount' VALUE='999999999'><PARAM NAME='loop' VALUE='true'><EMBED src='" +
                                           link +
                                           "' type='application/x-mplayer2' name='mediaPlayer' autostart='1' showcontrols='1' showstatusbar='1' autorewind='1' width='" +
                                           videoWidth + "' height='" + videoHeight +
                                           "' playcount='true' loop='true'></EMBED></OBJECT>";
                    text = text.Replace("%ss_video%=" + link + "|" + videoWidth + "|" + videoHeight + "|", videoHtmlCode);
                    text = text.Replace("|", string.Empty);
                }

                text =
                    $"<html><head><meta http-equiv='content-type' content='text/html; charset = utf-8'/></head><body style='margin: 5px; overflow: auto;'><p><span style='color: #333333; font-family: Arial, sans-serif;'>{text}</span></p></body></html>";


                if (Config.Data.HasModule(Modules.ENCRYPT) && !isMobile)
                {
                    this.Response.Write(Aes256Encryptor.EncryptData(text, EncryptKey));
                }
                else
                {
                    this.Response.BinaryWrite(Encoding.Default.GetPreamble());
                    this.Response.Write(text);
                }
            }
        }

        private static string ReplaceServerUri(string text)
        {
            if (string.IsNullOrEmpty(text))
            {
                return string.Empty;
            }

            var newServerUri = Config.GetAlertsDir();
            var serverUrl = newServerUri.AbsoluteUri.Replace(newServerUri.PathAndQuery, "/");
            var currentRequestServerUrl = HttpContext.Current.Request.Url.AbsoluteUri.Replace(HttpContext.Current.Request.Url.PathAndQuery, "/");

            if (text.Contains(currentRequestServerUrl))
            {
                text = text.Replace(currentRequestServerUrl, $"{serverUrl}");
            }

            return text;
        }
    }
}