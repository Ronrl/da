﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Parameters;
using DeskAlertsDotNet.Extentions;

namespace DeskAlertsDotNet.Pages
{
    public partial class StandBy : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            string userName = Request.GetParam("uname", "");
            string fullDomain = Request.GetParam("fulldomain", "");
            string standBy = Request.GetParam("standby", "");
            int nextRequest = Request.GetParam("nextrequest", 60);



            using (DBManager dbManager = new DBManager())
            {
                bool forceSelfRegistrationEnabled = Settings.Content["ConfForceSelfRegWithAD"] == "1";

                if (forceSelfRegistrationEnabled)
                {
                    string domainsSql = "SELECT d.name FROM domains d, users u WHERE(u.name= '" + userName + "' OR u.display_name= '" +
                        userName + "') AND u.domain_id <> 1 AND u.domain_id = d.id";

                    var domainSet = dbManager.GetDataByQuery(domainsSql);

                    if (!domainSet.IsEmpty)
                    {
                        fullDomain = domainSet.GetString(0, "name");
                    }
                }


                string standByQuery = "UPDATE users SET ";

                if (standBy == "true")
                    standByQuery += " standby = 1, last_standby_request = GETDATE(), next_standby_request = '" +
                                    nextRequest + "' ";
                else
                {
                    standByQuery +=
                        " standby = 0, last_request = GETDATE(), last_standby_request = GETDATE(), next_standby_request = '" +
                        nextRequest + "' ";
                }

                standByQuery += " FROM users u ";

                if (fullDomain.Length > 0)
                    standByQuery += " INNER JOIN domains d ON u.domain_id = d.id AND d.name = N'" + fullDomain + "' ";


                standByQuery += " WHERE u.name = N'" + userName + "' AND u.role = 'U'";

                dbManager.ExecuteQuery(standByQuery);
            }
         
        }
    }
}