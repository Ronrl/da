﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Net;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using System.Text.RegularExpressions;
using System.Runtime.Serialization.Json;
using System.Web.Script.Serialization;

namespace deskalerts
{

    public class Unobtrusive
    {
       // public int id;
        public string from;
        public string to;
        public string days;
    }

    public partial class UnobtrusiveService : System.Web.UI.Page
    {
       public Dictionary<string, string> vars = new Dictionary<string,string>();

      // public Unobtrusive[] unobtrusives;

       public string SurveyData { get; set; }

       protected void Page_PreInit(object sender, EventArgs e)
       {
       }


       protected void Page_LoadComplete(object sender, EventArgs e)
       {
          // contentFrame.InnerHtml = "<b>123123123</b>";
       }
        protected void Page_Load(object sender, EventArgs e)
        {
            ServicePointManager.ServerCertificateValidationCallback = delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
            {
                return true;
            };

            string action = Request["action"];
            string data = Request["data"];

            if (data == null)
                data = "";

            Response.Write(MakeRequest(action, data));
        }

        public string BuildQueryString(string action, string param)
        {
            string queryString = "?name=uns";
            if (action.Equals("add"))
            {
                Unobtrusive u = new JavaScriptSerializer().Deserialize<Unobtrusive>(param);
                queryString += String.Format("&action=add&to={0}&from={1}&days={2}&user={3}", u.to, u.from, u.days, Request["user"]);
                return queryString;

            }
            else if (action.Equals("get"))
            {
                return queryString + "&action=get";
            }
            else if (action.Equals("del"))
            {
                queryString += String.Format("&action=del&id={0}", param);
                return queryString;
            }
            else if (action.Equals("switch"))
            {
                queryString += String.Format("&action=switch&id={0}", param);
                return queryString;
            }
            else if (action.Equals("update"))
            {
                Unobtrusive u = new JavaScriptSerializer().Deserialize<Unobtrusive>(param);
                queryString += String.Format("&action=update&to={0}&from={1}&days={2}&id={3}", u.to, u.from, u.days,  Request["id"]);
                return queryString;
            }

            
            return String.Empty;
        }
        public string MakeRequest(string action, string param)
        {
            string queryString = BuildQueryString(action, param);//String.Format("action={0}&param={1}", action, param);

            string url = HttpContext.Current.Request.Url.AbsoluteUri;
            string path = url.Substring(0, url.IndexOf("UnobtrusiveService"));
            string finalUrl = path + "admin/set_settings.asp" + queryString;
            
           // return finalUrl;
            WebRequest request = WebRequest.Create(finalUrl);
            WebResponse response = request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());

            return reader.ReadToEnd();
        }

        public Dictionary<string, string> GetAspVariables(params string[] keys)
        {
            string queryString = String.Join(":", keys);

            string url = HttpContext.Current.Request.Url.AbsoluteUri;
            string path = url.Substring(0, url.IndexOf("addSurvey"));
            string url_bridge = path + "/dotnetbridge.asp?var=" + queryString;
            WebRequest request = WebRequest.Create(url_bridge);
            WebResponse response = request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
           // Response.Write(reader.ReadToEnd());
            string[] variablesStrings = reader.ReadToEnd().Split('&');
            

            Dictionary<string, string> result = new Dictionary<string, string>();
            foreach (string keyVal in variablesStrings)
            {

                string[] keyValArray = keyVal.Split(':');

                if (keyValArray.Length != 2)
                    continue;

                string key = keyValArray[0];
                string value = keyValArray[1];

                result.Add(key, value);
            }

            return result;

            
        }
        public string GetAspVariable(string variableName)
        {
            string url = HttpContext.Current.Request.Url.AbsoluteUri;
            string path = url.Substring(0, url.IndexOf("addSurvey"));
            string url_bridge = path + "/dotnetbridge.asp?var="+variableName;
            WebRequest request = WebRequest.Create(url_bridge);
            WebResponse response = request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            string variable = reader.ReadToEnd();
            return variable;
        }
    }
}
