<%
'===================================================================
'============================== Defines ============================
'===================================================================

Const DOMAIN_ID_INDEX	= 0
Const DOMAIN_NAME_INDEX	= 1

'===================================================================
'===================================================================

Dim strAttrClass		: strAttrClass		= "objectClass"
Dim strAttrPath			: strAttrPath		= "canonicalName"

Dim strUserClass		: strUserClass		= "user"
Dim strUserFilter		: strUserFilter		= "(&(objectCategory=Person)(sAMAccountName=*))"
Dim strUserName			: strUserName		= "sAMAccountName"
Dim strUSnChanged       : strUSnChanged     = "uSNChanged"
Dim strUserDisplay		: strUserDisplay	= "displayName"
Dim strUserMail			: strUserMail		= "mail"
Dim strUserPhone		: strUserPhone		= "mobile"
Dim strUserDisabled		: strUserDisabled	= "userAccountControl"
Dim intUserDisBit		: intUserDisBit		= 2
Dim intUserEnbCheck		: intUserEnbCheck	= 0

Dim strCompClass		: strCompClass		= "computer"
Dim strCompFilter		: strCompFilter		= "(objectCategory=Computer)"
Dim strCompName			: strCompName		= "cn"

Dim strGroupClass		: strGroupClass		= "group"
Dim strGroupFilter		: strGroupFilter	= "(objectCategory=Group)"
Dim strGroupName		: strGroupName		= "cn"
Dim strGroupMembers		: strGroupMembers	= "member"
Dim strMemberOf			: strMemberOf		= "memberOf"

Dim strOuClass			: strOuClass		= "organizationalUnit"
Dim strOuFilter			: strOuFilter		= ""'"(objectCategory=Organizational-Unit)"
Dim strOuName			: strOuName			= "ou"

Dim strDomainClass		: strDomainClass	= "domain"
Dim strDomainFilter		: strDomainFilter	= ""
Dim strDomainName		: strDomainName		= "dc"

'===================================================================
'===================================================================
%>
<script language="JScript" src="jscripts/json2.js" runat="server"></script>
<script language="JScript" runat="server">

var sql = ""
var syncData;
var syncDataStr = "";
var auto_sync = "";
var interval_sync = "";
var strDomain = "";
var strLogin = "";
var strPassword64 = "";
var port = "389";
var secure = "";
var ad_select = "";
var groups_select = "";
var db_remove = "";
var impDis = "";
var impComp = "";
var impOu = "";
var selectedOUs = "";
var groups = "";
var groupsList = {Count: 0};
var groups_names = "";
var strPassword = "";
var all_groups = 1;
var all_ous = 1;
var all_child = 1;
var max_usn = 0;
var legacySync = "";
var ip = "";

// Returns the array as a VBArray.
function toVBArray(arr)
{
	var dict = new ActiveXObject("Scripting.Dictionary");
	if (arr)
	{
		if(typeof(arr) == 'string')
			dict.add(i, arr);
		else
			for(var i = 0, len = arr.length; i < len; i++)
			{
				dict.add(i, arr[i]);
			}
		return dict.Items();
	}
	
	return dict;
};

function getSynchData(Conn,sync_id)
{
	if (sync_id != "")
	{
		if (typeof sync_id == "object") {sync_id = sync_id.value }
		var result = Conn.Execute("SELECT sync_data FROM synchronizations WHERE id='"+ sync_id.replace("'","''")+"'");
		if (!result.EOF)
		{
		    syncData = result.Fields.Item('sync_data').value;
		    syncData = JSON.parse(syncData);
		    syncDataStr = JSON.stringify(syncData);
			auto_sync = syncData["auto_sync"];
			interval_sync = syncData["interval_sync"];
			strDomain = syncData["domain"];
			strLogin = syncData["adLogin"];
			strPassword64 = syncData["adPassword"];
			port = syncData["port"];
			secure = syncData["secure"];
			ad_select = syncData["ad_select"];
			groups_select = syncData["groups_select"];
			db_remove = syncData["db_remove"];
			impDis = syncData["impDis"];
			impComp = syncData["impComp"];
			selectedOUs = toVBArray(syncData["selectedOUs"]);
			groupsList = toVBArray(syncData["added_group"]);
			groups_names = syncData["groups_names"];
			groups = syncData["groups"];
			max_usn = syncData["max_usn"];
			legacySync = syncData["legacySync"];
			ip = syncData["ip"];
			all_child = syncData["all_child"]||"1";
		}       
		result.Close();
	}
}
</script>	

<%

sync_id = Request("sync_id")

strLogin=Request("adLogin")
strPlace=Request("adPlace")
strPassword64=Request("adPassword")
strDomain = Request("domain")
strPort = Request("port")
all_child = Request("all_child")

max_usn = Request("max_usn")
max_usn_tmp = CLng(max_usn)
syncDataStr = Request("syncDataStr")
legacySync = Request("legacySync")
ip = Request("ip")
updatedUsers = 0
updatedGroups = 0
updatedComputers = 0
updatedOu = 0
dbOperations = 0
Dim usersNames : usersNames = Array()
Dim groupsNames : groupsNames = Array()
Dim computersNames : computersNames = Array()
Dim ouNames : ouNames = Array()
Dim usersNamesFromDomain : usersNamesFromDomain = Array()
Dim groupsNamesFromDomain : groupsNamesFromDomain = Array()
Dim computersNamesFromDomain : computersNamesFromDomain = Array()
Dim ouNamesFromDomain : ouNamesFromDomain = Array()

impDis = (Not Request("impDis")<>"1")

ad_select = Request("ad_select")
'Dim impOU, selectedOUs, all_ous

'Dim groupsList, all_groups
Set groupsList = Request("added_group")

impComp = (Not Request("impComp")<>"1")
secure = (Not Request("secure")<>"1")
db_remove = (Not Request("db_remove")<>"1")

Set selectedOUs = Request("selectedOUs")

parseParams()

Dim LDAPQuery
On Error Resume Next
	Err.Clear
	Set LDAPQuery = CreateObject("DeskAlertsServer.LDAPQuery")
	If(Err.Number <> 0) Then
		Response.Write LNG_ERROR_SERVER_DLL_NOT_REGISTERED
		Response.End
	End If
On Error Goto 0

Dim strFilter
Dim strAttribs
Dim OUsQueryResult : OUsQueryResult = Null
Dim usersQueryResult : usersQueryResult = Null
Dim compsQueryResult : compsQueryResult = Null
Dim groupsQueryResult : groupsQueryResult = Null

Dim logFile
Dim logFile2
Const ForAppending=8
Const ForCreate=True
Const TristateTrue=-1 'Open the file as Unicode.
Sub WriteLineToResponseAndLog(str)
	Response.Write str & "<br/>"
	Response.Flush
	if ConfEnableSyncLog = 1 then
		if not isObject(fs) then
			set fs=Server.CreateObject("Scripting.FileSystemObject")
		end if
		if isEmpty(logFile) then
			fname = Server.MapPath("logs\" & Year(now_db) &"-"& TwoDigits(Month(now_db)) &"-"& TwoDigits(Day(now_db)) &"_"& TwoDigits(Hour(now_db)) &"-"& TwoDigits(Minute(now_db)) &"-"& TwoDigits(Second(now_db)) &".log")
			if (InStr(fname,"\admin\logs\")=0) then
				fname = Replace(fname,"\logs\","\admin\logs\")
			end if
			set logFile = fs.OpenTextFile(fname, ForAppending, ForCreate, TristateTrue)
		end if
		logFile.WriteLine str
	end if
End Sub

Sub WriteLineToResponseAndLog2(str)
	Response.Write str & "<br/>"
	Response.Flush
	if ConfEnableSMSLogs = 1 then
		if not isObject(fs) then
			set fs=Server.CreateObject("Scripting.FileSystemObject")
		end if
		if isEmpty(logFile) then
			'fname = Server.MapPath("logs\" & Year(now_db) &"-"& TwoDigits(Month(now_db)) &"-"& TwoDigits(Day(now_db)) &"_"& TwoDigits(Hour(now_db)) &"-"& TwoDigits(Minute(now_db)) &"-"& TwoDigits(Second(now_db)) &".log")
			fname=date&".log"
			if (InStr(fname,"\admin\logs\")=0) then
				fname = Replace(fname,"\logs\","\admin\logs\")
			end if
			set logFile = fs.OpenTextFile(fname, ForAppending, ForCreate, TristateTrue)
		end if
		logFile.WriteLine str
	end if
End Sub

Sub WriteLineToResponseAndLog3(str)
    Response.Write str & "<br/>"
    Response.Flush
    if not isObject(fs) then
		set fs=Server.CreateObject("Scripting.FileSystemObject")
	end if
    if isEmpty(logFile2) then
			fname = Server.MapPath("logs\" & Year(now_db) &"-"& TwoDigits(Month(now_db)) &"-"& TwoDigits(Day(now_db)) &"_"& TwoDigits(Hour(now_db)) &"-"& TwoDigits(Minute(now_db)) &"-"& TwoDigits(Second(now_db)) &"usn-sync"&".log")
			if (InStr(fname,"\admin\logs\")=0) then
				fname = Replace(fname,"\logs\","\admin\logs\")
			end if
			set logFile2 = fs.OpenTextFile(fname, ForAppending, ForCreate, TristateTrue)
		end if
	logFile2.WriteLine str
End Sub

Sub parseParams()

	strPassword=Base64Decode(strPassword64)

	If strPort <> "" Then strPort = ":"&strPort End If

	if (strLogin <> "") then
		If InStr(strLogin, "\") = 0 And InStr(strLogin, "=") = 0 Then
			strLogin = Split(strDomain, ".")(0)&"\"&strLogin
		End If
	end if

	if(ad_select="selected") Then
		impOU = True
		all_ous = 2
	ElseIf(ad_select="not") Then
		impOU = False
		all_ous = 3
		selectedOUs = Null
	Else 'Request("ad_select")="all"
		impOU = True
		all_ous = 1
		selectedOUs = Null
	End If

	If VarType(groupsList) = 8204 then
		If UBound(groupsList) >= 0 Then
			all_groups = 0
		Else
			all_groups = 1
		End If
	Else
		If groupsList.Count > 0 Then
			all_groups = 0
		Else
			all_groups = 1
		End If
	End If
End Sub

Dim domainList : domainList = Array()
Dim connectionId
Dim dId

Sub syncAD(curSyncId)

	if (curSyncId = "") then  curSyncId = sync_id end if
	
	Conn.Execute("UPDATE recurrence SET occurences=1 WHERE id in (SELECT recur_id FROM synchronizations WHERE id = '"&curSyncId&"')")
	
	getSynchData Conn, curSyncId
	parseParams()

	Response.Write "<script type=""text/javascript"" language=""javascript"">if(window.parent && window.parent.showLoader) window.parent.showLoader();</script>"
	Response.Flush

    if legacySync = "1" Then
	    WriteLineToResponseAndLog LNG_AD_SYNC_START_TIME &": " & now_db
	    WriteLineToResponseAndLog LNG_AD_ACTION_IMPORTED_OBJECTS &":"
    else 
        WriteLineToResponseAndLog3 LNG_AD_SYNC_START_TIME &": " & now_db
	    WriteLineToResponseAndLog3 LNG_AD_ACTION_IMPORTED_OBJECTS &":"    
    end if 

    dim sPort, sProtocol
    sPort = Request.ServerVariables("SERVER_PORT")
    
    if sPort = "80" then
        sPort = ""
    else
        sPort = ":" & sPort
    end if
    
    if Request.ServerVariables("HTTPS") = "off" then
        sProtocol = "http"
    else
        sProtocol = "https"
    end if

    PageUrl = sProtocol& "://" & Request.ServerVariables("SERVER_NAME") & sPort & Request.ServerVariables("URL")
    WriteLineToResponseAndLog3 "Page URL : " & Left(PageUrl, InStr(PageUrl, "/admin"))

    If legacySync = "1" Then
        connectionId = LDAPQuery.connect(strLogin, strPassword, strDomain&strPort, secure)
	    parseLdapError LDAPQuery.getLastError()
    Else
        WriteLineToResponseAndLog3 "Current max uSNChanged : "&max_usn
        If ip = "0" Then
            WriteLineToResponseAndLog3 "Trying to get IP-address of domain controller"
            Dim oXMLHTTP
            Set oXMLHTTP = CreateObject("MSXML2.ServerXMLHTTP.6.0")
            oXMLHTTP.Open "GET", Left(PageUrl, InStr(PageUrl, "/admin"))&"/DomainHelper.aspx?domain="&strDomain&"&username="&strLogin&"&password="&strPassword, False
            oXMLHTTP.Send
            If oXMLHTTP.Status = 200 Then
                ip = oXMLHTTP.responseText            
            End If
        End If
        If ip = "::1" Then
            ip = "127.0.0.1"
        End If
        If ip = "0" Or ip = "" Then
            WriteLineToResponseAndLog3 "Couldn't get IP-address"
		    Response.Write "<p>"
		    Response.Write "</p><input type=""button"" onclick=""history.go(-1)"" value=""Back""/>"
		    Response.End
        Else
            WriteLineToResponseAndLog3 "IP : "&ip
            connectionId = LDAPQuery.connect(strLogin, strPassword, ip&strPort, secure)
            If LDAPQuery.getLastError() = 81 Then
                WriteLineToResponseAndLog3 "Cannot connect to the specified IP"
                WriteLineToResponseAndLog3 "Trying to get new IP-address of domain controller"
                Set oXMLHTTP = CreateObject("MSXML2.ServerXMLHTTP.6.0")
                oXMLHTTP.Open "GET", Left(PageUrl, InStr(PageUrl, "/admin"))&"/DomainHelper.aspx?domain=test.local"&"&username="&strLogin&"&password="&strPassword, False
                oXMLHTTP.Send
                If oXMLHTTP.Status = 200 Then
                    ip = oXMLHTTP.responseText            
                End If
                If ip = "0" or ip = "" Then
                    WriteLineToResponseAndLog3 "Couldn't get new IP-address of DC, check your settings"
		            Response.Write "<p>"
		            Response.Write "</p><input type=""button"" onclick=""history.go(-1)"" value=""Back""/>"
		            Response.End
                Else
                    max_usn = 0
                    max_usn_tmp = 0
                    WriteLineToResponseAndLog3 "New IP-address of domain controller : "&ip
                    WriteLineToResponseAndLog3 "Will perform full synchronization"
                    connectionId = LDAPQuery.connect(strLogin, strPassword, ip&strPort, secure)
                    parseLdapError LDAPQuery.getLastError()
                End If
            End If    	    
        End If
    End If


	'===================================================================
	'============================ Domains ==============================
	'===================================================================
	domainNamesSQL = ""
	strFilter = "(&("&strAttrClass&"="&strDomainClass&")"&strDomainFilter&")"
	strAttribs = strDomainName
	DomainsQueryResult = LDAPQuery.query(connectionId, "", "subtree", strFilter, strAttribs)
	For Each domain In DomainsQueryResult
		If domainNamesSQL <> "" Then
			domainNamesSQL = domainNamesSQL & "', N'"
		End If
		domainName = domain.dn
		domainName = Mid(domainName, 4)
		domainName = Replace(domainName, ",dc=", ".", 1, -1, 1)
		domainNamesSQL = domainNamesSQL & Replace(domainName, "'", "''")
		WriteLineToResponseAndLog "Domain: " & domainName
	Next

	if domainNamesSQL <> "" then
		domainsIds = ""
		set RSDomains = Conn.Execute("SELECT id FROM domains WHERE name IN (N'"& domainNamesSQL &"')")
		while not RSDomains.EOF
			if domainsIds <> "" then
				domainsIds = domainsIds & ","
			end if
			domainsIds = domainsIds & RSDomains("id")
            set usersNamesConn = Conn.Execute("SELECT name FROM users WHERE domain_id = "&RSDomains("id"))
            while not usersNamesConn.EOF
                addElemToArray usersNamesConn("name")&"/"&RSDomains("id"), usersNames
                usersNamesConn.MoveNext
            wend
            usersNamesConn.Close
            set groupsNamesConn = Conn.Execute("SELECT name FROM groups WHERE domain_id = "&RSDomains("id"))
            while not groupsNamesConn.EOF
                addElemToArray groupsNamesConn("name")&"/"&RSDomains("id"), groupsNames
                groupsNamesConn.MoveNext
            wend
            groupsNamesConn.Close
            set computersNamesConn = Conn.Execute("SELECT name FROM computers WHERE domain_id = "&RSDomains("id"))
            while not computersNamesConn.EOF
                addElemToArray computersNamesConn("name")&"/"&RSDomains("id"), computersNames
                computersNamesConn.MoveNext
            wend
            computersNamesConn.Close
            set ouNamesConn = Conn.Execute("SELECT name FROM OU WHERE Id_domain = "&RSDomains("id"))
            while not ouNamesConn.EOF
                addElemToArray ouNamesConn("name")&"/"&RSDomains("id"), ouNames
                ouNamesConn.MoveNext
            wend
            ouNamesConn.Close
			RSDomains.MoveNext
		wend
		RSDomains.Close     
		setDomains domainsIds
		updateNonDomainObjects
	end if

	'===================================================================
	'============================== OUs ================================
	'===================================================================
	If impOU Then
		strFilter = "(&("&strAttrClass&"="&strOuClass&")"&strOuFilter&")"
		strAttribs = strOuName&","&strAttrPath&","&strUSnChanged

        Response.Write "OU FILTER "& strFilter & "<br/>"
        Response.Write "OU ATTRS " & strAttribs & "<br/>"
		OUsQueryResult = LDAPQuery.query(connectionId, "", "subtree", strFilter, strAttribs)
		parseLdapError LDAPQuery.getLastError()
	End If

	domainList = Array()

	If all_groups = 0 Then
		Response.Write "<script type=""text/javascript"" language=""javascript"">if(window.parent && window.parent.hideLoader) window.parent.hideLoader();</script>"
		Response.Flush

		For Each group_l In groupsList
			'addSubGroupsByDn group_l, Null, groupsQueryResult, usersQueryResult, compsQueryResult, True
			addGroupByQuery connectionId, group_l
		Next
	Else

		'===================================================================
		'============================= Groups ==============================
		'===================================================================
		if ConfDisableGroupsSync = 0 then
			strFilter = "(&("&strAttrClass&"="&strGroupClass&")"&strGroupFilter&")"
			strAttribs = strGroupName&","&strGroupMembers&","&strUSnChanged

             Response.Write "GROUPS FILTER "& strFilter & "<br/>"
             Response.Write "GROUPS ATTRS " & strAttribs & "<br/>"

			groupsQueryResult = LDAPQuery.query(connectionId, "", "subtree", strFilter, strAttribs)
			parseLdapError LDAPQuery.getLastError()
		end if

		'===================================================================
		'============================== Users ==============================
		'===================================================================
		strFilter = "(&("&strAttrClass&"="&strUserClass&")"&strUserFilter&")"
		strAttribs = strUserName&","&strUserDisplay&","&strUserMail&","&strUserPhone&","&strUserDisabled&","&strUSnChanged

        Response.Write "USERS FILTER "& strFilter & "<br/>"
        Response.Write "USERS ATTRS " & strAttribs & "<br/>"
        
        WriteLineToResponseAndLog strAttribs
        
		usersQueryResult = LDAPQuery.query(connectionId, "", "subtree", strFilter, strAttribs)
		parseLdapError LDAPQuery.getLastError()

		'===================================================================
		'============================ Computers ============================
		'===================================================================
		If impComp Then
			strFilter = "(&("&strAttrClass&"="&strCompClass&")"&strCompFilter&")"
			strAttribs = strCompName&","&strUSnChanged
            Response.Write "Computers FILTER "& strFilter & "<br/>"
            Response.Write "Computers ATTRS " & strAttribs & "<br/>"
			compsQueryResult = LDAPQuery.query(connectionId, "", "subtree", strFilter, strAttribs)
			parseLdapError LDAPQuery.getLastError()
		End If

		Response.Write "<script type=""text/javascript"" language=""javascript"">if(window.parent && window.parent.hideLoader) window.parent.hideLoader();</script>"
		Response.Flush
		
		if ConfDisableGroupsSync = 0 then
			For Each group_l In groupsQueryResult
				If Not group_l.dasynced And isInOUs(group_l.dn) Then
					addSubGroups group_l, Null, groupsQueryResult, usersQueryResult, compsQueryResult, False
				End IF
			Next
		end if
		For Each user_l In usersQueryResult
			If Not user_l.dasynced And isInOUs(user_l.dn) Then
				addUser user_l, Null
			End IF
		Next
		If IsArray(compsQueryResult) Then
			For Each comp_l In compsQueryResult
				If Not comp_l.dasynced And isInOUs(comp_l.dn) Then
					addComp comp_l, Null
				End IF
			Next
		End If
		If IsArray(OUsQueryResult) Then
			For Each ou_l In OUsQueryResult
				If Not ou_l.dasynced And isInOUs(ou_l.dn) Then
					addOU ou_l, getOUid(ou_l.dn)
				End IF
			Next
		End If
	End If

	If db_remove Then
		clearDomains
	Else
		clearGroups
		clearOUs
	End IF
	'===================================================================

	'===================================================================
	now_db_new=""
	Set RS = Conn.Execute("SELECT GETDATE() as now_db")
	If Not RS.EOF Then
		now_db_new=RS("now_db")
		RS.Close
	End If
	If now_db="" Then 'something wrong with getdate
		now_db_new=Now()
	End If

	Conn.Execute("buildAlertsCache")
    If (curSyncId<>"") then
        If ip = "0" Then
            max_usn_tmp = 0
        End If
        tmp = InStr(syncDataStr, ",""ip""")
        newSyncData = Left(syncDataStr, tmp)&"""ip"":"&""""&ip&""""&",""max_usn"":"&max_usn_tmp&"}"
        Conn.Execute("UPDATE synchronizations SET last_sync_time = GETDATE(), sync_data = '"&Replace(newSyncData, "'", "''")&"' where id ='"&Replace(curSyncId, "'", "''")&"'")
	End If

    if legacySync = "1" then
	    WriteLineToResponseAndLog LNG_AD_SYNCHRONIZATION_FINISHED&": "&now_db_new&" ("&DateDiff("s", now_db, now_db_new)&" seconds)"
    else 
        WriteLineToResponseAndLog3 "Updated Users : "&updatedUsers
        WriteLineToResponseAndLog3 "Updated Groups : "&updatedGroups
        WriteLineToResponseAndLog3 "Updated Computers : "&updatedComputers
        WriteLineToResponseAndLog3 "Updated OU : "&updatedOu
        WriteLineToResponseAndLog3 "uSNChanged after synchronization : "&max_usn_tmp
        WriteLineToResponseAndLog3 "IP after synchronization : "&ip
        WriteLineToResponseAndLog3 LNG_AD_SYNCHRONIZATION_FINISHED&": "&now_db_new&" ("&DateDiff("s", now_db, now_db_new)&" seconds)"
    end if

	Conn.Execute("UPDATE recurrence SET occurences=0 WHERE id in (SELECT recur_id FROM synchronizations WHERE id = '"&curSyncId&"')")

	LDAPQuery.close(connectionId)

End Sub

'===================================================================
'=========================== Functions =============================
'===================================================================
Sub parseLdapError(errorCode)
	If errorCode <> 0 Then
		Response.Write "<p>"
		Select Case errorCode
		Case 13
			WriteLineToResponseAndLog "Detected TSL connection. Go back, check ""Use secure LDAP connection"" option and try again."
			Response.AppendToLog "Sync error: Detected TSL connection, check ""Use secure LDAP connection"" option."
		Case 34
			WriteLineToResponseAndLog "The distinguished name (User DN) has an invalid syntax."
			Response.AppendToLog "Sync error: The distinguished name (User DN) has an invalid syntax."
		Case 49
			WriteLineToResponseAndLog "Invalid user or password."
			Response.AppendToLog "Sync error: Invalid user or password."
		Case 81
			WriteLineToResponseAndLog "Cannot connect to specified LDAP Server."
			Response.AppendToLog "Sync error: Cannot connect to specified LDAP Server."
		Case Else
			WriteLineToResponseAndLog "Error occured while accessing LDAP. Error number: " & errorCode
			Response.AppendToLog "Sync error: error occured while accessing LDAP, error number: " & errorCode
		End Select
		Response.Write "</p><input type=""button"" onclick=""history.go(-1)"" value=""Back""/>"
		Response.End
	End If
End Sub

Function getDomainId(dn)
	Dim domainExists : domainExists = False
	Dim dId : dId = -1
	Dim strDomain : strDomain = Right(dn, Len(dn) - InStr(1, dn, ",dc=", 1)-3)
	strDomain = Replace(strDomain, ",dc=", ".", 1, -1, 1)

	For Each domain In domainList
		If (domain(DOMAIN_NAME_INDEX) = strDomain) Then
			domainExists = True
			getDomainId = domain(DOMAIN_ID_INDEX)
			Exit For
		End If
	Next
	
	If(Not domainExists) Then
		Set RSDomain = Conn.Execute("SELECT id FROM domains WHERE name='"&strDomain&"'")
		if(RSDomain.EOF) then
			Set RSDomain = Conn.Execute("SET NOCOUNT ON" & vbNewLine & "INSERT INTO domains (name, was_checked, all_groups, all_ous) VALUES ('" & Replace(strDomain, "'", "''") & "', 1, "&all_groups&","&all_ous&")" & vbNewLine & "SELECT SCOPE_IDENTITY() as id")
		end if
		getDomainId = RSDomain("id")
		RSDomain.Close
		
		Dim domainInfo(1)
		domainInfo(DOMAIN_ID_INDEX) = getDomainId
		domainInfo(DOMAIN_NAME_INDEX) = strDomain

		addElemToArray domainInfo, domainList
	End If
End Function

Function getAttr(attr)
	getAttr = ""
	If IsArray(attr) Then
		If Ubound(attr) >= 0 Then
			getAttr = attr(0)
		End If
	ElseIf VarType(attr) > 1 Then
		getAttr = attr
	End If
End Function

Function isInOUs(dn)
	If Not isNull(selectedOUs) Then
		isInOUs = False
		For Each ou In selectedOUs
			If StrComp(dn, ou, 1) = 0 Or (Len(dn) > Len(ou) And InStrRev(dn, ou, -1, 1) = (Len(dn) - Len(ou) + 1)) Then
				isInOUs = True
				Exit For
			End If
		Next
	Else
		isInOUs = True
	End If
End Function

Function addOU(ou, parent_ou_id)
    addElemToArray getAttr(ou(strOuName))&"/"&getDomainId(ou.dn), ouNamesFromDomain
	If Not VarType(parent_ou_id) > 1 Then
		parent_ou_id = "NULL"
	End If
	If db_remove Then
		was_checked = 1
	ElseIf isInOUs(ou.dn) Then
		was_checked = 1
	Else
		was_checked = 0
	End If
    If max_usn_tmp < CLng(getAttr(ou(strUSnChanged))) Then
        max_usn_tmp = CLng(getAttr(ou(strUSnChanged)))
    End If
    needToAdd = "0"
    If legacySync = "1" Then
        needToAdd = "1"
    ElseIf max_usn < CLng(getAttr(ou(strUSnChanged))) Then
		needToAdd = "1"
    End If
    If needToAdd = "1" Then
        updatedOu = updatedOu + 1
	    Set RS = Conn.Execute("addOU "&_
				    "@ou_hash='"&MD5(ou.dn)&"', "&_
				    "@ou_name=N'"&Replace(getAttr(ou(strOuName)), "'", "''")&"', "&_
				    "@ou_path=N'"&Replace(getAttr(ou(strAttrPath)), "'", "''")&"', "&_
				    "@parent_ou_id="&parent_ou_id&", "&_
				    "@domain_id="&getDomainId(ou.dn)&", "&_
				    "@was_checked="&was_checked)
	    addOU = RS("id")
	    if RS("updated") = 0 then

		    WriteLineToResponseAndLog "Added OU: "& HTMLEncode(getAttr(ou(strOuName)))
	    else
		    WriteLineToResponseAndLog "Updated OU: "& HTMLEncode(getAttr(ou(strOuName)))
	    end if
    End If
End Function

Function getOUid(object_dn)
	If IsArray(OUsQueryResult) Then
		Dim i
		Dim found_ou : found_ou = -1
		Dim max_dn : max_dn = 0
		
		For i = 0 To Ubound(OUsQueryResult)
			Dim ou_dn : ou_dn = OUsQueryResult(i).dn
			Dim ou_dn_len : ou_dn_len = Len(ou_dn)
			If object_dn <> ou_dn And Len(object_dn) > ou_dn_len And InStrRev(object_dn, ou_dn, -1, 1) = (Len(object_dn) - ou_dn_len + 1) And ou_dn_len > max_dn Then
				max_dn = ou_dn_len 'for parent ou ignoring
				found_ou = i
			End If
		Next
		
		If found_ou > -1 Then
			If Not OUsQueryResult(found_ou).dasynced Then
				OUsQueryResult(found_ou).daou_id = addOU(OUsQueryResult(found_ou), getOUid(OUsQueryResult(found_ou).dn))
				OUsQueryResult(found_ou).dasynced = True
			End If
			getOUid = OUsQueryResult(found_ou).daou_id
		End If
	End If
End Function

Function addGroup(group, container_group_id)
    addElemToArray getAttr(group(strGroupName))&"/"&getDomainId(group.dn), groupsNamesFromDomain
	Dim ou_id : ou_id = getOUid(group.dn)
	If Not VarType(ou_id) > 1 Then
		ou_id = "NULL"
	End If
	If Not VarType(container_group_id) > 1 Then
		container_group_id = "NULL"
	End If
    If max_usn_tmp < CLng(getAttr(group(strUSnChanged))) Then
        max_usn_tmp = CLng(getAttr(group(strUSnChanged)))
    End If
    needToAdd = "0"
    If legacySync = "1" Then
        needToAdd = "1"
    ElseIf max_usn < CLng(getAttr(group(strUSnChanged))) Then
		needToAdd = "1"
    End If
    If needToAdd = "1" Then
        updatedGroups = updatedGroups + 1
	    Set RS = Conn.Execute("addGroup "&_
				    "@group_hash='"&MD5(group.dn)&"', "&_
				    "@group_name=N'"&Replace(getAttr(group(strGroupName)), "'", "''")&"', "&_
				    "@ou_id="&ou_id&", "&_
				    "@container_group_id="&container_group_id&", "&_
				    "@domain_id="&getDomainId(group.dn))
	    addGroup = RS("id")
	    if RS("updated") = 0 then
		    WriteLineToResponseAndLog "Added group: "& HTMLEncode(getAttr(group(strGroupName)))
	    else
		    WriteLineToResponseAndLog "Updated group: "& HTMLEncode(getAttr(group(strGroupName)))
	    end if
    End If
End Function

Function checkDisabledUser(user)
	If impDis Then
		checkDisabledUser = True ' import all
	ElseIf VarType(user(strUserDisabled)) < 2 Then
		checkDisabledUser = True ' can't check
	ElseIf (getAttr(user(strUserDisabled)) And intUserDisBit) = intUserEnbCheck Then
		checkDisabledUser = True
	Else
		checkDisabledUser = False
	End If
End Function

Function canAddChild(dn)
	If all_child = "1" Then
		canAddChild = True
	ElseIf isInOUs(dn) Then
		canAddChild = True
	Else
		canAddChild = False
	End IF
End Function

Function addUser(user, group_id)
	If checkDisabledUser(user) Then
        addElemToArray getAttr(user(strUserName))&"/"&getDomainId(user.dn), usersNamesFromDomain
		Dim ou_id : ou_id = getOUid(user.dn)		
		If Not VarType(ou_id) > 1 Then
			ou_id = "NULL"
		End If
		If Not VarType(group_id) > 1 Then
			group_id = "NULL"
		End If
        If max_usn_tmp < CLng(getAttr(user(strUSnChanged))) Then
            max_usn_tmp = CLng(getAttr(user(strUSnChanged)))
        End If
        needToAdd = "0"
        If legacySync = "1" Then
            needToAdd = "1"
        ElseIf max_usn < CLng(getAttr(user(strUSnChanged))) Then
		    needToAdd = "1"
        End If
        If needToAdd = "1" Then
            updatedUsers = updatedUsers + 1
            Set RS = Conn.Execute("addUser "&_
					        "@user_hash='"&MD5(user.dn)&"', "&_
					        "@user_name=N'"&Replace(getAttr(user(strUserName)), "'", "''")&"', "&_
					        "@display_name=N'"&Replace(getAttr(user(strUserDisplay)), "'", "''")&"', "&_
					        "@mail=N'"&Replace(getAttr(user(strUserMail)), "'", "''")&"', "&_
					        "@phone=N'"&Replace(getAttr(user(strUserPhone)), "'", "''")&"', "&_
					        "@ou_id="&ou_id&", "&_
					        "@group_id="&group_id&", "&_
					        "@domain_id="&getDomainId(user.dn))
		    addUser = RS("id")
		    if RS("updated") = 0 then         
			    WriteLineToResponseAndLog "Added user: "& HTMLEncode(getAttr(user(strUserName)))
		    else
			    WriteLineToResponseAndLog "Updated user: "& HTMLEncode(getAttr(user(strUserName)))
		    end if
        End If
	End If
End Function

Function addComp(comp, group_id)
    addElemToArray getAttr(comp(strCompName))&"/"&getDomainId(comp.dn), computersNamesFromDomain
	Dim ou_id : ou_id = getOUid(comp.dn)
	If Not VarType(ou_id) > 1 Then
		ou_id = "NULL"
	End If
	If Not VarType(group_id) > 1 Then
		group_id = "NULL"
	End If
    If max_usn_tmp < CLng(getAttr(comp(strUSnChanged))) Then
        max_usn_tmp = CLng(getAttr(comp(strUSnChanged)))
    End If
    needToAdd = "0"
    If legacySync = "1" Then
        needToAdd = "1"
    ElseIf max_usn < CLng(getAttr(comp(strUSnChanged))) Then
		needToAdd = "1"
    End If
    If needToAdd = "1" Then
        updatedComputers = updatedComputers + 1
	    Set RS = Conn.Execute("addComp "&_
				    "@comp_hash='"&MD5(comp.dn)&"', "&_
				    "@comp_name=N'"&Replace(getAttr(comp(strCompName)), "'", "''")&"', "&_
				    "@ou_id="&ou_id&", "&_
				    "@group_id="&group_id&", "&_
				    "@domain_id="&getDomainId(comp.dn))
	    addComp = RS("id")
	    if RS("updated") = 0 then
		    WriteLineToResponseAndLog "DN = "&comp.dn &"Added comp: "& HTMLEncode(getAttr(comp(strCompName)))
	    else
		    WriteLineToResponseAndLog "Updated"&comp.dn&" comp: "& HTMLEncode(getAttr(comp(strCompName)))
	    end if
    End If
End Function

Sub addUserToGroup(user_id, group_id)
	If VarType(user_id) > 1 And VarType(group_id) > 1 Then
		Conn.Execute("addUserToGroup "&_
			"@group_id="&group_id&", "&_
			"@user_id="&user_id)
	End If
End Sub

Sub addCompToGroup(comp_id, group_id)
	If VarType(comp_id) > 1 And VarType(group_id) > 1 Then
		Conn.Execute("addCompToGroup "&_
			"@group_id="&group_id&", "&_
			"@comp_id="&comp_id)
	End If
End Sub

Sub addGroupToGroup(group_id, parent_group_id)
	If VarType(group_id) > 1 And VarType(parent_group_id) > 1 Then
		Conn.Execute("addGroupToGroup "&_
			"@container_group_id="&parent_group_id&", "&_
			"@group_id="&group_id)
	End If
End Sub

Sub addSubGroupsByDn(dn, parent_group_id, groupsResult, usersResult, compsResult, isRoot)
	For Each group In groupsResult
		If group.dn = dn Then
			addSubGroups group, parent_group_id, groupsResult, usersResult, compsResult, isRoot
			Exit For
		End If
	Next 
End Sub

Sub addSubGroups(group, parent_group_id, groupsResult, usersResult, compsResult, isRoot)
	If Not group.dasynced Then
		Dim group_id : group_id = addGroup(group, parent_group_id)
		group.dagroup_id = group_id
		group.dasynced = True
		If VarType(group_id) > 1 Then
			Dim members : members = group(strGroupMembers)
			If IsArray(members) Then
				For Each member_dn In members
					Dim found : found = False
					For Each user In usersResult
						If user.dn = member_dn Then
							If canAddChild(member_dn) Then
								If Not user.dasynced Then
									user.dauser_id = addUser(user, group_id)
									user.dasynced = True
								Else
									addUserToGroup user.dauser_id, group_id
								End If
							End If
							found = True
							Exit For
						End If
					Next
					If Not found And IsArray(compsQueryResult) Then
						For Each comp In compsResult
							If comp.dn = member_dn Then
								If canAddChild(member_dn) Then
									If Not comp.dasynced Then
										comp.dacomp_id = addComp(comp, group_id)
										comp.dasynced = True
									Else
										addCompToGroup comp.dacomp_id, group_id
									End If
								End If
								found = True
								Exit For
							End If
						Next
					End If
					If Not found And canAddChild(member_dn) Then
						addSubGroupsByDn member_dn, group_id, groupsResult, usersResult, compsResult, False
					End If
				Next
			End If
		End If
	ElseIf VarType(parent_group_id) > 1 Then
		addGroupToGroup group.dagroup_id, parent_group_id
	End If
End Sub

Function addGroupByQuery(conId, dn)
	strFilter = "(&("&strAttrClass&"="&strGroupClass&")"&strGroupFilter&")"
	strAttribs = strGroupName&","&strUSnChanged
	result = LDAPQuery.query(conId, dn, "base", strFilter, strAttribs)
	parseLdapError LDAPQuery.getLastError()
	For Each obj in result
        addGroupMembersByQuery conId, obj, addGroup(obj, Null)
	Next
End Function

Sub addGroupMembersByQuery(conId, group, group_id)
	strFilter = "("&strMemberOf&"="&group.dn&")"
	strAttribs = strAttrClass&","&strCompName&","&strGroupName&","&strUserName&","&strUserDisplay&","&strUserMail&","&strUserPhone&","&strUserDisabled&","&strUSnChanged
	result = LDAPQuery.query(conId, "", "subtree", strFilter, strAttribs)
	parseLdapError LDAPQuery.getLastError()
	For Each obj in result
		addObjectByQuery conId, obj, group_id
	Next
End Sub

Function addObjectByQuery(conId, obj, group_id)
	Dim classes : classes = obj(strAttrClass)

	If isClass(classes, strUserClass) Then
		addObject = addUser(obj, group_id)
	ElseIf impComp And isClass(classes, strCompClass) Then
		addObject = addComp(obj, group_id)
	ElseIf isClass(classes, strGroupClass) Then
		addObject = addGroup(obj, group_id)
		addGroupMembersByQuery conId, obj, addObject
	End If
End Function

Function isClass(classes, clss)
	isClass = False
	If VarType(classes) > 1 Then
		For Each c in classes
			If StrComp(clss, c, 1) = 0 Then
				isClass = True
				Exit For
			End If
		Next
	End If
End Function

Sub setDomains(domains)
	if domains <> "" then
        If legacySync = "1" Then
		    ' Marks all users and groups as non-synched
		    SQL = _
			    "IF OBJECT_ID('tempdb..#tempGroups') IS NOT NULL DROP TABLE #tempGroups" & vbNewLine & _
			    "CREATE TABLE #tempGroups (id BIGINT NOT NULL)" & vbNewLine & _
			    "INSERT INTO #tempGroups (id) (SELECT id FROM groups WHERE domain_id IN ("&domains&"))" & vbNewLine & _
			    _
			    "UPDATE users_groups SET was_checked=0 WHERE user_id IN (SELECT id FROM users WHERE domain_id IN ("&domains&")) OR group_id IN (SELECT id FROM #tempGroups)" & vbNewLine & _
			    "UPDATE groups_groups SET was_checked=0 WHERE group_id IN (SELECT id FROM #tempGroups) OR container_group_id IN (SELECT id FROM #tempGroups)" & vbNewLine & _
			    "UPDATE computers_groups SET was_checked=0 WHERE comp_id IN (SELECT id FROM computers WHERE domain_id IN ("&domains&")) OR group_id IN (SELECT id FROM #tempGroups)" & vbNewLine & _
			    _
			    "DROP TABLE #tempGroups" & vbNewLine
		    Conn.Execute(SQL)
		
		    Conn.Execute("UPDATE groups SET was_checked=0 WHERE domain_id IN ("&domains&")")
		    Conn.Execute("UPDATE users SET was_checked=0 WHERE domain_id IN ("&domains&")")
		    Conn.Execute("UPDATE computers SET was_checked=0 WHERE domain_id IN ("&domains&")")
		    Conn.Execute("UPDATE OU SET was_checked=0 WHERE Id_domain IN ("&domains&")")
        End If
	end if
End Sub

Sub clearDomains
	' Deleting from DB non-synced domains and groups
	Dim domains : domains = ""
	For Each domain In domainList
		If(domains <> "") Then
			domains = domains & ","
		End If
		domains = domains & domain(DOMAIN_ID_INDEX)
	Next
	
	WriteLineToResponseAndLog "Debug Info: "& domains
	
	Conn.CommandTimeout = 300
	
	If domains <> "" Then
        If legacySync <> "1" Then
            markNotSynced
        End If
		SQL = "SET NOCOUNT ON" & vbNewLine & _
			_
			"IF OBJECT_ID('tempdb..#tempUsers') IS NOT NULL DROP TABLE #tempUsers" & vbNewLine & _
			"IF OBJECT_ID('tempdb..#tempGroups') IS NOT NULL DROP TABLE #tempGroups" & vbNewLine & _
			"IF OBJECT_ID('tempdb..#tempComps') IS NOT NULL DROP TABLE #tempComps" & vbNewLine & _
			"IF OBJECT_ID('tempdb..#tempOUs') IS NOT NULL DROP TABLE #tempOUs" & vbNewLine & _
			_
			"CREATE TABLE #tempUsers (id BIGINT NOT NULL)" & vbNewLine & _
			"CREATE TABLE #tempGroups (id BIGINT NOT NULL)" & vbNewLine & _
			"CREATE TABLE #tempComps (id BIGINT NOT NULL)" & vbNewLine & _
			"CREATE TABLE #tempOUs (id BIGINT NOT NULL)" & vbNewLine & _
			_
			"INSERT INTO #tempGroups (id) (SELECT id FROM groups WHERE domain_id IN ("&domains&"))" & vbNewLine & _
			"DELETE FROM users_groups WHERE was_checked=0 AND (user_id IN (SELECT id FROM users WHERE domain_id IN ("&domains&")) OR group_id IN (SELECT id FROM #tempGroups))" & vbNewLine & _
			"DELETE FROM groups_groups WHERE was_checked=0 AND (group_id IN (SELECT id FROM #tempGroups) OR container_group_id IN (SELECT id FROM #tempGroups))" & vbNewLine & _
			"DELETE FROM computers_groups WHERE was_checked=0 AND (comp_id IN (SELECT id FROM computers WHERE domain_id IN ("&domains&")) OR group_id IN (SELECT id FROM #tempGroups))" & vbNewLine & _
			"TRUNCATE TABLE #tempGroups" & vbNewLine & _
			_
			"INSERT INTO #tempUsers (id) (SELECT id FROM users WHERE was_checked=0 AND domain_id IN ("&domains&"))" & vbNewLine & _
			"INSERT INTO #tempGroups (id) (SELECT id FROM groups WHERE was_checked=0 AND domain_id IN ("&domains&"))" & vbNewLine & _
			"INSERT INTO #tempComps (id) (SELECT id FROM computers WHERE was_checked=0 AND domain_id IN ("&domains&"))" & vbNewLine & _
			"INSERT INTO #tempOUs (id) (SELECT Id FROM OU WHERE was_checked=0 AND Id_domain IN ("&domains&"))" & vbNewLine & _
			_
			"DELETE FROM User_synch WHERE Id_user IN (SELECT id FROM #tempUsers)" & vbNewLine & _
			"DELETE FROM Comp_synch WHERE Id_comp IN (SELECT id FROM #tempComps)" & vbNewLine & _
			"DELETE FROM Group_synch WHERE Id_group IN (SELECT id FROM #tempGroups)" & vbNewLine & _
			"DELETE FROM OU_synch WHERE Id_OU IN (SELECT id FROM #tempOUs)" & vbNewLine & _
			_
			"DELETE FROM OU_User WHERE Id_OU IN (SELECT id FROM #tempOUs) OR Id_user IN (SELECT id FROM #tempUsers)" & vbNewLine & _
			"DELETE FROM OU_comp WHERE Id_OU IN (SELECT id FROM #tempOUs) OR Id_comp IN (SELECT id FROM #tempComps)" & vbNewLine & _
			"DELETE FROM OU_group WHERE Id_OU IN (SELECT id FROM #tempOUs) OR Id_group IN (SELECT id FROM #tempGroups)" & vbNewLine & _
			"DELETE FROM OU_hierarchy WHERE Id_parent IN (SELECT id FROM #tempOUs) OR Id_child IN (SELECT id FROM #tempOUs)" & vbNewLine & _
			"DELETE FROM OU_DOMAIN WHERE Id_OU IN (SELECT id FROM #tempOUs)" & vbNewLine & _
			_
			"SELECT name FROM users WHERE id IN (SELECT id FROM #tempUsers)" & vbNewLine & _
			"SELECT name FROM groups WHERE id IN (SELECT id FROM #tempGroups)" & vbNewLine & _
			"SELECT name FROM computers WHERE id IN (SELECT id FROM #tempComps)" & vbNewLine & _
			"SELECT name FROM OU WHERE Id IN (SELECT id FROM #tempOUs)" & vbNewLine & _
			_
			"DELETE FROM users WHERE id IN (SELECT id FROM #tempUsers)" & vbNewLine & _
			"DELETE FROM groups WHERE id IN (SELECT id FROM #tempGroups)" & vbNewLine & _
			"DELETE FROM computers WHERE id IN (SELECT id FROM #tempComps)" & vbNewLine & _
			"DELETE FROM OU WHERE Id IN (SELECT id FROM #tempOUs)" & vbNewLine & _
			_
			"DROP TABLE #tempUsers" & vbNewLine & _
			"DROP TABLE #tempGroups" & vbNewLine & _
			"DROP TABLE #tempComps" & vbNewLine & _
			"DROP TABLE #tempOUs" & vbNewLine
		set RS = Conn.Execute(SQL)
		while not RS.EOF
			WriteLineToResponseAndLog "Deleted user: "& HTMLEncode(RS("name"))
			RS.MoveNext
		wend
		set RS = RS.NextRecordset
		while not RS.EOF
			WriteLineToResponseAndLog "Deleted group: "& HTMLEncode(RS("name"))
			RS.MoveNext
		wend
		set RS = RS.NextRecordset
		while not RS.EOF
			WriteLineToResponseAndLog "Deleted computer: "& HTMLEncode(RS("name"))
			RS.MoveNext
		wend
		set RS = RS.NextRecordset
		while not RS.EOF
			WriteLineToResponseAndLog "Deleted OU: "& HTMLEncode(RS("name"))
			RS.MoveNext
		wend        
	End If
	
	Conn.CommandTimeout = 30
End Sub

Sub updateNonDomainObjects
	regDomainWhere = "domain_id IS NULL"
	Set RSRegDomain = Conn.Execute("SELECT id FROM domains WHERE name = ''")
	if not RSRegDomain.EOF then
		regDomainWhere = "("& regDomainWhere &" OR domain_id = "& RSRegDomain("id") &")"
	end if
	SQL = _
		"IF OBJECT_ID('tempdb..#tempGroups') IS NOT NULL DROP TABLE #tempGroups" & vbNewLine & _
		"CREATE TABLE #tempGroups (id BIGINT NOT NULL)" & vbNewLine & _
		"INSERT INTO #tempGroups (id) (SELECT id FROM groups WHERE "& regDomainWhere &")" & vbNewLine & _
		_
		"UPDATE users_groups SET was_checked=1 WHERE user_id IN (SELECT id FROM users WHERE "& regDomainWhere &") OR group_id IN (SELECT id FROM #tempGroups)" & vbNewLine & _
		"UPDATE groups_groups SET was_checked=1 WHERE group_id IN (SELECT id FROM #tempGroups) OR container_group_id IN (SELECT id FROM #tempGroups)" & vbNewLine & _
		"UPDATE computers_groups SET was_checked=1 WHERE comp_id IN (SELECT id FROM computers WHERE "& regDomainWhere &") OR group_id IN (SELECT id FROM #tempGroups)" & vbNewLine & _
		_
		"DROP TABLE #tempGroups" & vbNewLine
	Conn.Execute(SQL)
	
	Conn.Execute("UPDATE groups SET was_checked=1 WHERE "& regDomainWhere)
	Conn.Execute("UPDATE users SET was_checked=1 WHERE "& regDomainWhere)
	Conn.Execute("UPDATE computers SET was_checked=1 WHERE "& regDomainWhere)
	Conn.Execute("UPDATE OU SET was_checked=1 WHERE "& Replace(regDomainWhere, "domain_id", "Id_domain"))
End Sub

Sub clearGroups
	' Deleting from DB non-synced domains and groups
	Dim domains : domains = ""
	For Each domain In domainList
		If(domains <> "") Then
			domains = domains & ","
		End If
		domains = domains & domain(DOMAIN_ID_INDEX)
	Next
	
	If domains <> "" Then
        If legacySync <> "1" Then
            markNotSynced
        End If
		SQL = _
			";WITH g AS (SELECT id FROM groups WHERE was_checked=1 AND domain_id IN ("&domains&"))" & vbNewLine & _
			"	DELETE FROM groups_groups WHERE was_checked=0 AND (group_id IN (SELECT id FROM g) OR container_group_id IN (SELECT id FROM g))"
		Conn.Execute(SQL)
	End If
End Sub

Sub clearOUs
	' Deleting from DB non-synced users, computers and groups form selected OUs and them sub-OUs
	Dim domains : domains = ""
	For Each domain In domainList
		If(domains <> "") Then
			domains = domains & ","
		End If
		domains = domains & domain(DOMAIN_ID_INDEX)
	Next
	
	If domains <> "" Then
        If legacySync <> "1" Then
            markNotSynced
        End If
		SQL = "SET NOCOUNT ON" & vbNewLine & _
			_
			"IF OBJECT_ID('tempdb..#tempUsers') IS NOT NULL DROP TABLE #tempUsers" & vbNewLine & _
			"IF OBJECT_ID('tempdb..#tempGroups') IS NOT NULL DROP TABLE #tempGroups" & vbNewLine & _
			"IF OBJECT_ID('tempdb..#tempComps') IS NOT NULL DROP TABLE #tempComps" & vbNewLine & _
			"IF OBJECT_ID('tempdb..#tempOUs') IS NOT NULL DROP TABLE #tempOUs" & vbNewLine & _
			_
			"CREATE TABLE #tempUsers (id BIGINT NOT NULL)" & vbNewLine & _
			"CREATE TABLE #tempGroups (id BIGINT NOT NULL)" & vbNewLine & _
			"CREATE TABLE #tempComps (id BIGINT NOT NULL)" & vbNewLine & _
			"CREATE TABLE #tempOUs (id BIGINT NOT NULL)" & vbNewLine & _
			_
			"INSERT INTO #tempOUs (id) (SELECT Id FROM OU WHERE was_checked=1 AND Id_domain IN ("&domains&"))" & vbNewLine & _
			_
			"INSERT INTO #tempUsers (id) (SELECT id FROM users u INNER JOIN OU_User o ON u.id=o.Id_user WHERE u.was_checked=0 AND o.Id_OU IN (SELECT id FROM #tempOUs))" & vbNewLine & _
			"INSERT INTO #tempGroups (id) (SELECT id FROM groups g INNER JOIN OU_group o ON g.id=o.Id_group WHERE g.was_checked=0 AND o.Id_OU IN (SELECT id FROM #tempOUs))" & vbNewLine & _
			"INSERT INTO #tempComps (id) (SELECT id FROM computers c INNER JOIN OU_comp o ON c.id=o.Id_comp WHERE c.was_checked=0 AND o.Id_OU IN (SELECT id FROM #tempOUs))" & vbNewLine & _
			_
			"DELETE FROM users_groups WHERE user_id IN (SELECT id FROM #tempUsers) OR group_id IN (SELECT id FROM #tempGroups)" & vbNewLine & _
			"DELETE FROM groups_groups WHERE group_id IN (SELECT id FROM #tempGroups) OR container_group_id IN (SELECT id FROM #tempGroups)" & vbNewLine & _
			"DELETE FROM computers_groups WHERE comp_id IN (SELECT id FROM #tempComps) OR group_id IN (SELECT id FROM #tempGroups)" & vbNewLine & _
			_
			"DELETE FROM User_synch WHERE Id_user IN (SELECT id FROM #tempUsers)" & vbNewLine & _
			"DELETE FROM Comp_synch WHERE Id_comp IN (SELECT id FROM #tempComps)" & vbNewLine & _
			"DELETE FROM Group_synch WHERE Id_group IN (SELECT id FROM #tempGroups)" & vbNewLine & _
			_
			"DELETE FROM OU_User WHERE Id_user IN (SELECT id FROM #tempUsers)" & vbNewLine & _
			"DELETE FROM OU_comp WHERE Id_comp IN (SELECT id FROM #tempComps)" & vbNewLine & _
			"DELETE FROM OU_group WHERE Id_group IN (SELECT id FROM #tempGroups)" & vbNewLine & _
			_
			"SELECT name FROM users WHERE id IN (SELECT id FROM #tempUsers)" & vbNewLine & _
			"SELECT name FROM groups WHERE id IN (SELECT id FROM #tempGroups)" & vbNewLine & _
			"SELECT name FROM computers WHERE id IN (SELECT id FROM #tempComps)" & vbNewLine & _
			_
			"DELETE FROM users WHERE id IN (SELECT id FROM #tempUsers)" & vbNewLine & _
			"DELETE FROM groups WHERE id IN (SELECT id FROM #tempGroups)" & vbNewLine & _
			"DELETE FROM computers WHERE id IN (SELECT id FROM #tempComps)" & vbNewLine & _
			_
			"DROP TABLE #tempUsers" & vbNewLine & _
			"DROP TABLE #tempGroups" & vbNewLine & _
			"DROP TABLE #tempComps" & vbNewLine & _
			"DROP TABLE #tempOUs" & vbNewLine
		set RS = Conn.Execute(SQL)
		while not RS.EOF
			WriteLineToResponseAndLog "Deleted user: "& HTMLEncode(RS("name"))
			RS.MoveNext
		wend
		set RS = RS.NextRecordset
		while not RS.EOF
			WriteLineToResponseAndLog "Deleted group: "& HTMLEncode(RS("name"))
			RS.MoveNext
		wend
		set RS = RS.NextRecordset
		while not RS.EOF
			WriteLineToResponseAndLog "Deleted computer: "& HTMLEncode(RS("name"))
			RS.MoveNext
		wend
	End If
End Sub

' elem - element to add
' arr - array where to add elem
Sub addElemToArray(elem, arr)
	Dim length : length = Ubound(arr) + 1
	
	If length = 0 Then
		Redim arr(0)
	Else
		Redim Preserve arr(length)
	End If
	
	arr(length) = elem
End Sub

Sub markNotSynced
    For Each user In usersNames
        needToDelete = "1"
        For Each userInDomain In usersNamesFromDomain
            If user = userInDomain Then
                needToDelete = "0"
            End If
        Next
        If needToDelete = "1" Then
            Conn.Execute("UPDATE users SET was_checked=0 WHERE name = '"&Split(user, "/")(0)&"' AND domain_id = "&Split(user, "/")(1))
            Conn.Execute("UPDATE users_groups SET was_checked=0 WHERE user_id = (SELECT id FROM users WHERE name = '"&Split(user, "/")(0)&"' AND domain_id = "&Split(user, "/")(1)&")")  
            updatedUsers = updatedUsers + 1                                                                      	            
        End If
    Next
    For Each group In groupsNames
        needToDelete = "1"
        For Each groupInDomain In groupsNamesFromDomain
            If group = groupInDomain Then
                needToDelete = "0"
            End If
        Next
        If needToDelete = "1" Then
            Conn.Execute("UPDATE groups SET was_checked=0 WHERE name = '"&Split(group, "/")(0)&"' AND domain_id = "&Split(group, "/")(1))
            Conn.Execute("UPDATE groups_groups SET was_checked=0 WHERE group_id = (SELECT id FROM groups WHERE name = '"&Split(group, "/")(0)&"' AND domain_id = "&Split(group, "/")(1)&")")
            updatedGroups = updatedGroups + 1                                                                        	            
        End If
    Next
    For Each computer In computersNames
        needToDelete = "1"
        For Each computerInDomain In computersNamesFromDomain
            If computer = computerInDomain Then
                needToDelete = "0"
            End If
        Next
        If needToDelete = "1" Then
            Conn.Execute("UPDATE computers SET was_checked=0 WHERE name = '"&Split(computer, "/")(0)&"' AND domain_id = "&Split(computer, "/")(1))
            Conn.Execute("UPDATE computers_groups SET was_checked=0 WHERE group_id = (SELECT id FROM computers WHERE name = '"&Split(computer, "/")(0)&"' AND domain_id = "&Split(computer, "/")(1)&")") 
            updatedComputers = updatedComputers + 1                                                                       	            
        End If
    Next
    For Each ou In ouNames
        needToDelete = "1"
        For Each ouInDomain In ouNamesFromDomain
            If ou = ouInDomain Then
                needToDelete = "0"
            End If
        Next
        If needToDelete = "1" Then
            Conn.Execute("UPDATE OU SET was_checked=0 WHERE name = '"&Split(ou, "/")(0)&"' AND Id_domain = "&Split(ou, "/")(1))    
            updatedOu = updatedOu + 1                
        End If
    Next
End Sub

%>