<% db_backup_stop_working = 1 %>
<!-- #include file="admin/config.inc" -->
<% ConnCount = 3 %>
<!-- #include file="db_conn.asp" -->

<%
'editors
SQL = "SELECT distinct (editor_id) as eid, name from policy_editor inner join users ON policy_editor.editor_id=users.id LEFT JOIN policy_list on policy_editor.policy_id=policy_list.policy_id WHERE policy_list.policy_id IS NULL"
Set RS = Conn.Execute(SQL)
Do while not RS.EOF
	eid = RS("eid")
	ed_name = RS("name")
	Conn.Execute("insert into policy (name, type) values ('Policy for "&ed_name&"', 'E')")
	Set rs1 = Conn.Execute("select @@IDENTITY newID from policy")
	new_pid=rs1("newID")
	rs1.Close
	if(IsNull(new_pid)) then
		Set rs1 = Conn.Execute("select MAX(id) as newID from policy")
		new_pid=rs1("newID")
		rs1.Close
	end if

	SQL = "SELECT policy_id FROM policy_editor WHERE editor_id=" & eid
	Set RSpolicy = Conn2.Execute(SQL)
	do while not RSpolicy.EOF	
		old_pid = RSpolicy("policy_id")
		Conn3.Execute("insert into policy_user (policy_id, user_id, was_checked) SELECT "&new_pid&" as policy_id, user_id, 1 FROM policy_user WHERE policy_id = " & old_pid)
		'Conn3.Execute("delete from policy_user where policy_id=" & old_pid)

		Conn3.Execute("insert into policy_group (policy_id, group_id, was_checked) SELECT "&new_pid&" as policy_id, group_id, 1 FROM policy_group WHERE policy_id = " & old_pid)
		'Conn3.Execute("delete from policy_group where policy_id=" & old_pid)
		
		Conn3.Execute("insert into policy_computer (policy_id, comp_id, was_checked) SELECT "&new_pid&" as policy_id, comp_id, 1 FROM policy_computer WHERE policy_id = " & old_pid)
		'Conn3.Execute("delete from policy_computer where policy_id=" & old_pid)		

		Conn3.Execute("insert into policy_ou (policy_id, ou_id, was_checked) SELECT "&new_pid&" as policy_id, ou_id, 1 FROM policy_ou WHERE policy_id = " & old_pid)
		'Conn3.Execute("delete from policy_ou where policy_id=" & old_pid)		
		
		Conn3.Execute("UPDATE policy SET type='E' where id=" & old_pid)		
		RSpolicy.MoveNext
	loop
	Conn2.Execute("DELETE FROM policy_editor WHERE editor_id=" & eid)
	Conn2.Execute("insert into policy_editor (policy_id, editor_id) VALUES ("&new_pid&", "&eid&")")
	Conn2.Execute("insert into policy_list (policy_id, alerts_val, emails_val, sms_val, surveys_val, users_val, groups_val, templates_val, statistics_val, im_val, text_to_call_val) VALUES ("&new_pid&",'1111111', '1111111', '1111111', '1111111', '1111111', '1111111', '1111111', '1111111', '1111111', '1111111')")
	RS.MoveNext
loop

'sysadmins
Set RS = Conn.Execute ("SELECT id, name, role FROM users WHERE (role='A' AND id<>1) OR (role<>'E' AND role<>'U' AND role<>'A')")
Do while not RS.EOF
	eid = RS("id")
	ed_name = RS("name")
	if(RS("role")="A") then
		Conn.Execute("insert into policy (name, type) values ('Policy for "&ed_name&"', 'A')")
	else
		Conn.Execute("insert into policy (name, type) values ('Policy for "&ed_name&"', 'E')")
	end if
	Set rs1 = Conn.Execute("select @@IDENTITY newID from policy")
	new_pid=rs1("newID")
	rs1.Close
	if(IsNull(new_pid)) then
		Set rs1 = Conn.Execute("select MAX(id) as newID from policy")
		new_pid=rs1("newID")
		rs1.Close
	end if	
	Conn2.Execute("insert into policy_editor (policy_id, editor_id) VALUES ("&new_pid&", "&eid&")")
	Conn2.Execute("insert into policy_list (policy_id, alerts_val, emails_val, sms_val, surveys_val, users_val, groups_val, templates_val, statistics_val, im_val, text_to_call_val) VALUES ("&new_pid&",'1111111', '1111111', '1111111', '1111111', '1111111', '1111111', '1111111', '1111111', '1111111', '1111111')")
	RS.MoveNext
loop	
	
	Conn.Execute("UPDATE users SET role='E' WHERE role='A' AND id<>1")
	Conn.Execute("UPDATE users SET role='E' WHERE role<>'A' AND role <> 'U' AND role <> 'E'")

	'for dishnetwork
	'Conn.Execute ("update groups SET root_group = 1")

%>
Done
<!-- #include file="admin/db_conn_close.asp" -->