<%
filename = "iCalendar.ics"
Response.ContentType = "text/calendar"
Response.AddHeader "content-disposition", "attachment; filename="""& filename &""""
Response.Expires = 0
db_backup_no_redirect = 1
%>
<!-- #include file="admin/config.inc" -->
<!-- #include file="admin/db_conn.asp" -->
<%
'See RFC2445

function format_guid(guid)
	format_guid = Replace(Replace(Ucase(guid), "{", ""), "}", "")
end function
function format_uid(guid)
	format_uid = Replace(format_guid(guid), "-", "")
end function
function format_date_time(dt)
	format_date_time = year(dt) & TwoDigits(month(dt)) & TwoDigits(day(dt)) &"T"& TwoDigits(hour(dt)) & TwoDigits(minute(dt)) & TwoDigits(second(dt)) &"Z"
end function
function escape_chars(str)
	if str <> "" then
		escape_chars = Replace(str, "\", "\\")
		escape_chars = Replace(escape_chars, ";", "\;")
		escape_chars = Replace(escape_chars, ",", "\,")
		escape_chars = Replace(escape_chars, vbCr, "")
		escape_chars = Replace(escape_chars, vbLf, "\n")
	else
		escape_chars = ""
	end if
end function
function format_lines(str)
	'RFC5545
	new_line = Mid(str, 76)
	format_lines = Left(str, 75)
	do while new_line <> ""
		format_lines = format_lines & vbNewline & vbTab & Left(new_line, 74)
		new_line = Mid(new_line, 75)
	loop
end function
sub WriteLine(str)
	Response.Write format_lines(str & vbNewline)
end sub

id = guidFromShortId(Request("event_id"))
calendar_id = guidFromShortId(Request("id"))
if id <> "" or calendar_id <> "" then
	SQL = "SELECT id, calendar_id, summary, location, description, create_date, modify_date, class, priority, sequence, status, start_date, end_date, alarm FROM icalendars WHERE "
	if id <> "" then
		SQL = SQL & "id = '" & Replace(id, "'", "''") & "'"
	else
		SQL = SQL & "calendar_id = '" & Replace(calendar_id, "'", "''") & "'"
	end if
	Set RS = Conn.Execute(SQL)
	if not RS.EOF then
		WriteLine "BEGIN:VCALENDAR"
		WriteLine "VERSION:2.0"
		WriteLine "PRODID:-//Softomate, LLC//DeskAlerts//EN"

		if RS("status") <> "1" then ' can't be canceled only one event
			WriteLine "METHOD:PUBLISH"
		else
			WriteLine "METHOD:CANCEL"
		end if

		WriteLine "X-WR-RELCALID:" & format_guid(RS("calendar_id"))
		
		'X-WR-CALNAME:

		do
			if RS("start_date") = "" and RS("end_date") = "" then
				WriteLine "BEGIN:VTODO"
			else
				WriteLine "BEGIN:VEVENT"
			end if

			if RS("class") = "2" then
				WriteLine "CLASS:CONFIDENTIAL"
			elseif RS("class") = "1" then
				WriteLine "CLASS:PRIVATE"
			else
				WriteLine "CLASS:PUBLIC"
			end if

			WriteLine "CREATED:" & format_date_time(RS("create_date"))
			description = RS("description")
			description = Replace(description, vbCr, "")
			description = Replace(description, vbLf, "")
			description = Replace(description, "<br>", vbLf)
			description = Replace(description, "<br/>", vbLf)
			description = Replace(description, "<br />", vbLf)
			WriteLine "DESCRIPTION:" & escape_chars(DeleteTags(description))

			if RS("end_date") <> "" then
				WriteLine "DTEND:" & format_date_time(RS("end_date"))
			end if

			WriteLine "DTSTAMP:" & format_date_time(RS("create_date"))

			if RS("start_date") <> "" then
				WriteLine "DTSTART:" & format_date_time(RS("start_date"))
			end if

			WriteLine "LAST-MODIFIED:" & format_date_time(RS("modify_date"))

			WriteLine "LOCATION:" & escape_chars(RS("location"))

			WriteLine "PRIORITY:" & RS("priority")

			WriteLine "SEQUENCE:" & RS("sequence")

			WriteLine "SUMMARY:" & escape_chars(RS("summary"))

			WriteLine "TRANSP:OPAQUE"

			WriteLine "UID:" & format_uid(RS("id"))

			WriteLine "URL:" & escape_chars(alerts_folder & "ical.asp?event_id=" & shortIdFromGuid(format_guid(RS("id"))))

			WriteLine "X-ALT-DESC;FMTTYPE=text/html:" & escape_chars(RS("description"))

			'ORGANIZER;CN=John Smith:MAILTO:jsmith@host1.com
			'X-OWNER;CN="John Smith":mailto:jsmith@host1.com

			if RS("status") = "1" then
				WriteLine "STATUS:CANCELLED"
			end if

			if RS("start_date") = "" and RS("end_date") = "" then
				WriteLine "END:VTODO"
			else
				WriteLine "END:VEVENT"
			end if

			if RS("alarm") <> "" then
				if RS("start_date") <> "" then
					alarm = DateDiff("n", RS("start_date"), RS("alarm"))
					if alarm < 0 then
						alarm = "-PT" & Abs(alarm) & "M"
					else
						alarm = "PT" & alarm & "M"
					end if
				else
					alarm = format_date_time(RS("alarm"))
				end if
				WriteLine "BEGIN:VALARM"
				WriteLine "TRIGGER:" & alarm
				WriteLine "ACTION:DISPLAY"
				WriteLine "END:VALARM"
			end if

			RS.MoveNext
		loop while not RS.EOF

		WriteLine "END:VCALENDAR"
	end if
end if
%>
<!-- #include file="admin/db_conn_close.asp" -->