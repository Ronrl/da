﻿using System;
using DeskAlertsDotNet.Application;

namespace DeskAlerts.Server.sever.STANDARD
{
    public partial class ShowCache : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
            }
            else
            {
                InitData();
                DisplayCacheContent();
            }
        }

        private void DisplayCacheContent()
        {
            var cache = Global.CacheManager;
            UserCacheViewerList.DataSource = cache.ShowUserCache();
            UserCacheViewerList.DataBind();

            ReceivedCacheViewerList.DataSource = cache.ShowAlertReceivedCache();
            ReceivedCacheViewerList.DataBind();
        }

        protected void InitData()
        {
            const string regenerateCachePageName = "RegenerateCache.aspx";
            RegenerateCachePageLink.Text = regenerateCachePageName;
            RegenerateCachePageLink.OnClientClick = $"window.open('{regenerateCachePageName}', '{regenerateCachePageName}')";
        }
    }
}