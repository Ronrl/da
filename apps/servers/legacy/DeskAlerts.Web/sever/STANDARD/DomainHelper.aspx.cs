﻿namespace DeskAlertsDotNet.Pages
{
    using System;
    using System.DirectoryServices.ActiveDirectory;

    public partial class DomainHelper : System.Web.UI.Page
    {
        #region Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            var domain = this.Request["domain"];
            var username = this.Request["username"];
            var password = this.Request["password"];

            var context = new DirectoryContext(DirectoryContextType.Domain, domain, username, password);
            var dc = DomainController.FindOne(context);
            var ip = dc.IPAddress;

            if (ip != null)
            {
                this.Response.Write(ip);
                this.Response.Flush();
            }
            else
            {
                this.Response.Write("0");
                this.Response.Flush();
            }
        }

        #endregion
    }
}