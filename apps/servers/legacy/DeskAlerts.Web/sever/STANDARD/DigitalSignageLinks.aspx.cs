﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Parameters;
using DeskAlertsDotNet.Utils;

namespace DeskAlertsDotNet.Pages
{
    public partial class DigitalSignageLinks : DeskAlertsBaseListPage
    {

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            table = contentTable;
            searchButton.Text = searchButton.Text.Replace("{Search}", Localization.Current.Content["LNG_SEARCH"]);

            nameCell.Text = GetSortLink(Localization.Current.Content["LNG_NAME"], "name", Request["sortBy"]);
            linkCell.Text = GetSortLink(Localization.Current.Content["LNG_LINK"], "link", Request["sortBy"]);
            creationDateCell.Text = GetSortLink(Localization.Current.Content["LNG_CREATION_DATE"], "creation_date",
                Request["sortBy"]);
            parametersCell.Text = Localization.Current.Content["LNG_DIGSIGN_MSG_DISP_TIME"];
            actionsCell.Text = Localization.Current.Content["LNG_ACTIONS"];


            if (!IsPostBack)
            {
                mdtFactor.Items.Add(new ListItem(Localization.Current.Content["LNG_SECONDS"], "0"));
                mdtFactor.Items.Add(new ListItem(Localization.Current.Content["LNG_MINUTES"], "1"));
                mdtFactor.Items.Add(new ListItem(Localization.Current.Content["LNG_HOURS"], "2"));
                editMdtFactor.Items.Add(new ListItem(Localization.Current.Content["LNG_SECONDS"], "0"));
                editMdtFactor.Items.Add(new ListItem(Localization.Current.Content["LNG_MINUTES"], "1"));
                editMdtFactor.Items.Add(new ListItem(Localization.Current.Content["LNG_HOURS"], "2"));
            }

            int cnt = daContent.Count < (Offset + Limit) ? daContent.Count : Offset + Limit;
            
            for (int i = Offset; i < cnt; i++)
            {
                DataRow dbRow = daContent.GetRow(i);
                TableRow tableRow = new TableRow();

                CheckBox chb = GetCheckBoxForDelete(dbRow);

                TableCell checkTableRow = new TableCell();
                
                checkTableRow.Controls.Add(chb);

                tableRow.Cells.Add(checkTableRow);


                DataSet userDataSet =
                    dbMgr.GetDataByQuery(
                        string.Format(
                            "SELECT id FROM users WHERE name = '{0}' AND ( role='U' OR role IS NULL ) AND client_version = 'web client'",
                            dbRow.GetString("name")));

                if(userDataSet.IsEmpty)
                    continue;

               int  userId = userDataSet.GetInt(0,"id");
                //int? userId = dbMgr.GetScalarQuery<int?>(string.Format("SELECT id FROM users WHERE name = '{0}' AND ( role='U' OR role IS NULL ) AND client_version = 'web client'", dbRow.GetString("name")));

             
                string nameLink = string.Format("<a href='PopupSent.aspx?user_id={0}'>{1}</a>", userId,
                    dbRow.GetString("name"));

                TableCell nameContentCell = new TableCell {Text = nameLink};
                tableRow.Cells.Add(nameContentCell);

                string linkHtml = string.Format("<a target='_blank' href='{0}'>{1}</a>", dbRow.GetString("link"), dbRow.GetString("link"));
                TableCell linkContentCell = new TableCell {Text = linkHtml};
                tableRow.Cells.Add(linkContentCell);


                
                TableCell creationDateContentCell = new TableCell {Text = dbRow.GetString("creation_date")};
                tableRow.Cells.Add(creationDateContentCell);

                int slideTimeValue = dbRow.GetInt("slide_time_value");
                int slideTimeFactor = dbRow.GetInt("slide_time_factor");

                string[] factorNames= {"seconds", "minutes", "hours"};

                string factorName = factorNames[slideTimeFactor];

                if (slideTimeValue == 1)
                    factorName = factorName.Substring(0, factorName.Length - 1);
                string refreshRateText = slideTimeValue + " " + factorName;

                TableCell parametersContentCell = new TableCell {Text = refreshRateText};
                tableRow.Cells.Add(parametersContentCell);


                LinkButton editButton = GetActionButton(dbRow.GetString("id"), "images/action_icons/edit.png",
                    "LNG_EDIT",
                    string.Format("editInstance({0}, '{1}', {2}, {3})", dbRow.GetString("id"), dbRow.GetString("name"),
                        slideTimeValue, slideTimeFactor), 0);

                TableCell actionRow = new TableCell();
                actionRow.HorizontalAlign = HorizontalAlign.Center;
                actionRow.Controls.Add(editButton);

                tableRow.Cells.Add(actionRow);

                contentTable.Rows.Add(tableRow);

             
            }

        }

        [WebMethod]
        public static bool IsExists(string name)
        {
            DBManager dbMgr = new DBManager();

            int count = dbMgr.GetScalarQuery<int>("SELECT COUNT(1) FROM digsign_links WHERE name = '" + name +"'");

            return count > 0;
        }

        [WebMethod]
        public static string Change(string name, int rateValue, int rateFactor, bool update)
        {
            DBManager dbMgr = new DBManager();
            string res = "";
            int[] multipier = {1, 60, 1440};
            var resultDict = DeployInstance(name, rateValue*multipier[rateFactor], update);

            Dictionary<string, object> digSignDictionary = new Dictionary<string, object>();

            digSignDictionary.Add("slide_time_value", rateValue);
            digSignDictionary.Add("slide_time_factor", rateFactor);

            if (!update)
            {
                digSignDictionary.Add("name", name);
                digSignDictionary.Add("link", resultDict["link"]);
                digSignDictionary.Add("creation_date", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                dbMgr.Insert("digsign_links", digSignDictionary);
            }
            else
            {
                Dictionary<string, object> conditionDictionary = new Dictionary<string, object>
                {
                    {"name", name}
                };
               res = dbMgr.Update("digsign_links", digSignDictionary, conditionDictionary);
            }


            Dictionary<string, object> userDictionary = new Dictionary<string, object>();
            userDictionary.Add("name", name);
            userDictionary.Add("deskbar_id", resultDict["deskbar"]);
            userDictionary.Add("reg_date", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
            userDictionary.Add("pass", "f49c0321a68ac5311234aa469ceb15bc");
            userDictionary.Add("client_version", "web client");
            userDictionary.Add("role", "U");
            userDictionary.Add("group_id", 0);
            userDictionary.Add("domain_id", 1);

            if (!update)
            {
                dbMgr.Insert("users", userDictionary);
            }

            dbMgr.Dispose();

            return res;
        }

        private static Dictionary<string, string> DeployInstance(string name, int slideTime, bool update = false)
        {

            Dictionary<string, string> result = new Dictionary<string, string>();


            string destDir = Config.Data.GetString("alertsFolder") + "\\ds\\" +
                             name.Replace("*", "")
                                 .Replace(":", "")
                                 .Replace("|", "")
                                 .Replace("?", "")
                                 .Replace("<", "")
                                 .Replace(">", "")
                                 .Replace("\"", "")
                                 .Replace("'", "");


            if(update && Directory.Exists(destDir))
                Directory.Delete(destDir, true);

            string templateDir = Config.Data.GetString("alertsFolder") + "\\admin\\templates\\digsign";

            DeskAlertsUtils.DirectoryCopy(templateDir, destDir, true);

           // int slideTime = rateValue*rateFactor;
            string indexFileText = File.ReadAllText(destDir + "/index.html");
            Guid deskBarGuid = Guid.NewGuid();

            int refreshValue = Convert.ToInt32(Settings.Content["ConfDSRefreshRateValue"]);
            int refreshFactor = Convert.ToInt32(Settings.Content["ConfDSRefreshRateFactor"]);

             int refreshRate = (int)(refreshValue * Math.Pow(60, refreshFactor));

            indexFileText = indexFileText.Replace("{UNAME}", name).Replace("{REFRESH_TIME}", slideTime.ToString()).Replace("{DESKBAR_ID}", deskBarGuid.ToString()).Replace("{REFRESH_RATE}", refreshRate.ToString()); ;

            using (FileStream fs = File.Open(destDir + "/index.html", FileMode.Truncate))
            {
                Byte[] info = new UTF8Encoding(true).GetBytes(indexFileText);
                fs.Write(info, 0, info.Length);
                fs.Close();
            }

            result.Add("deskbar", deskBarGuid.ToString());

            string link = Config.Data.GetString("alertsDir") + "/ds/" + name;
            result.Add("link", link);
            return result;
        }

        protected override string CustomQuery
        {
            get
            {
                string query = "";

                query = "SELECT id, name, link, creation_date, slide_time_value, slide_time_factor FROM digsign_links";

                if (IsPostBack && searchTermBox.Value.Length > 0)
                {
                    query += " WHERE name LIKE '%" + searchTermBox.Value.Replace("'", "''") + "%'";
                }
                
                return query;
            }
        }

        protected override string CustomCountQuery
        {
            get
            {
                string query = "";

                query = "SELECT count(1) FROM digsign_links";

                if (IsPostBack && searchTermBox.Value.Length > 0)
                    query += " WHERE name LIKE '%" + searchTermBox.Value.Replace("'", "''") + "%'";

                return query;
            }
        }

        #region DataProperties
        protected override string DataTable
        {
            get
            {

                //return your table name from this property
                return "digsign_links";
            }
        }

        protected override void OnDeleteRows(string[] ids)
        {

            dbMgr.ExecuteQuery(
                string.Format(
                    "DELETE u FROM users as u INNER JOIN digsign_links as d ON d.name = u.name WHERE d.id IN ({0}) AND u.client_version = 'web client'", string.Join(",", ids)));

        }

        protected override string[] Collumns
        {
            get
            {

                //return array of collumn`s names of table from data table
                return new[] { "name", "link", "creation_date", "slide_time_value", "slide_time_factor"};
            }
        }

        protected override string DefaultOrderCollumn
        {
            //return collumn for default order
            get
            {
                return "id";

            }
        }

        /*
         * 
         * If you want to add condition for data selection override property ConditionSqlString 
         * Also you can implement your custom query by overriding properties CustomQuery and CustomCountQuery
         *  
         */
        #endregion

        #region Interface properties

        protected override IButtonControl[] DeleteButtons
        {
            get
            {
                return new IButtonControl[] { upDelete, bottomDelete };
            }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl NoRowsDiv
        {
            get { return NoRows; }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl TableDiv
        {
            get { return mainTableDiv; }
        }

        protected override HtmlGenericControl TopPaging
        {
            get { return topPaging; }
        }

        protected override HtmlGenericControl BottomPaging
        {
            get { return bottomPaging; }
        }

        protected override string ContentName
        {
            get
            {
                //return content name here
                return "Devices";
            }
        }

        #endregion
    }
}