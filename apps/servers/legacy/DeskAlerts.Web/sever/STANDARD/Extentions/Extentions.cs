﻿using System;
using System.Globalization;
using System.Linq;
using System.Web;
using DeskAlerts.Server.Utils;
using DeskAlertsDotNet.DataBase;

namespace DeskAlertsDotNet.Pages
{
    public static class DeskAlertsExtentions
    {

        public static void WriteLine(this HttpResponse response, string message, bool flush = false)
        {
            
            response.Write(message + "<br>");

            if (flush)
                response.Flush();
        }

        public static void ShowJavaScriptAlert(this HttpResponse response, string message, bool doubleQuotes = false)
        {
            string quote = doubleQuotes ? @"""" : "'";
            response.Write(String.Format("<script>alert({1}{0}{1});</script>", message, quote));
        }

        public static void RedirectTopParent(this HttpResponse response, string page, bool endResponse = true)
        {
            response.Write(String.Format("<script>top.window.location.href = '{0}'; </script>", page));

            if(endResponse)
                response.End();
        }


        public static string GetPostOrGetIfNotFound(this HttpRequest request, string paramName, string defaultValue = "")
        {

            if (request.Form.AllKeys.Contains(paramName))
                return request.Form[paramName];


            if (request.QueryString.AllKeys.Contains(paramName))
                return request.QueryString[paramName];

            return defaultValue;
        }
        public static void RedirectParent(this HttpResponse response, string page, bool endResponse = true)
        {
            response.Write(String.Format("<script>window.parent.location.href = '{0}'; </script>", page));

            if (endResponse)
                response.End();
        }

        public static string EscapeJSQuotes(this string s)
        {
            return s.Replace("'", @"\'");
        }

        public static string Quotes(this string s)
        {
            return "'" + s + "'";
        }

        public static string DoubleQuotes(this string s)
        {
            return "\"" + s + "\"";
        }

        public static string FromBase64(this string str)
        {
            str = str.Replace(" ", "+");

            int mod4 = str.Length % 4;

            if (mod4 > 0)
            {
                str += new string('=', 4 - mod4);
            }

            byte[] stringBytes = Convert.FromBase64String(str);

            str = System.Text.Encoding.UTF8.GetString(stringBytes);

            return str;
        }

        public static string[] Quotes(this string[] arr)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = "'" + arr[i] + "'";
            }

            return arr;
        }

        public static string[] DoubleQuotes(this string[] arr)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = "\"" + arr[i] + "\"";
            }

            return arr;
        }

        /// <summary>
        /// Get parameter.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="request">HttpRequest.</param>
        /// <param name="param">Param name.</param>
        /// <param name="defaultValue">Default value.</param>
        /// <returns></returns>
        public static T GetParam<T>(this HttpRequest request, string param, T defaultValue)
        {
            if (!string.IsNullOrEmpty(request[param]))
                return (T) Convert.ChangeType(request[param], typeof(T));

            return (T)Convert.ChangeType(defaultValue, typeof(T));
        }

        public static byte[] AsByteArray(this string str)
        {
            byte[] res = new byte[str.Length * sizeof(char)];
            Buffer.BlockCopy(str.ToCharArray(), 0, res, 0, res.Length);
            return res;
        }

        public static string AsString(this byte[] arr)
        {
            char[] chars = new char[arr.Length / sizeof(char)];
            Buffer.BlockCopy(arr, 0, chars, 0, arr.Length);
            return new string(chars);
        }


        public static string EscapeQuotes(this string str)
        {
            return str.Replace("'", "''");
        }

        public static string ReplaseDoubleQoutesToTwoUnoQuotes(this string str)
        {
            return str.Replace("\"", "''");
        }

        public static string FixDoubleQoutes(this string str)
        {
            return str.ReplaseDoubleQoutesToTwoUnoQuotes();
        }
    }
}