﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DeskAlertsDotNet.DataBase;
using Newtonsoft.Json;


namespace DeskAlertsDotNet.Extentions
{
    public class DeskAlertsDataBaseJsonWorker : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(DataSet) || objectType == typeof(DataRow);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
           

            if (value is DataSet)
            {
                DataSet dataSet = value as DataSet;

                writer.WriteStartArray();

                foreach (DataRow row in dataSet)
                {
                    writer.WriteStartObject();
                    Dictionary<string, object> dict = row.ToDictionary();
                    foreach (KeyValuePair<string, object> pair in dict)
                    {
                        if (pair.Value is DBNull)
                            continue;
                        writer.WritePropertyName(pair.Key);
                        writer.WriteValue(pair.Value);
                    }
                    writer.WriteEndObject();
                }

                writer.WriteEndArray();
            }
            else if (value is DataRow)
            {
                DataRow row = value as DataRow;
                
                writer.WriteStartObject();
                Dictionary<string, object> dict = row.ToDictionary();
                foreach (KeyValuePair<string, object> pair in dict)
                {

                    writer.WritePropertyName(pair.Key);
                    writer.WriteValue(pair.Value);
                }
               
                writer.WriteEndObject();              
            }
        }
    }
}