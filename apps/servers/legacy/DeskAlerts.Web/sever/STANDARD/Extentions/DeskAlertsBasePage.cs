﻿using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Models;
using System;
using NLog;
using DeskAlertsDotNet.Managers;

namespace DeskAlertsDotNet.Pages
{
    public class DeskAlertsBasePage : System.Web.UI.Page
    {
        //this is field using for showing print button in the preview
        protected static bool addPrintButton = false;

        public static Logger Logger = LogManager.GetCurrentClassLogger();

        protected  User curUser;
        protected  DBManager dbMgr;
        protected override void OnInit(EventArgs e)
        {
            dbMgr = new DBManager();
            curUser = UserManager.Default.CurrentUser;
            dbMgr.ExecuteQuery("SET LANGUAGE BRITISH");
            base.OnInit(e);
        }

        protected override void OnError(EventArgs e)
        {
            dbMgr?.Dispose();
            base.OnError(e);
        }

        protected override void OnUnload(EventArgs e)
        {
            dbMgr?.Dispose();
            base.OnUnload(e);
        }
    }
}