using DeskAlerts.ApplicationCore.Utilities;
using DeskAlerts.Server.Utils.Helpers;
using DeskAlerts.Server.Utils.Managers;
using DeskAlertsDotNet.Application;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Parameters;
using DeskAlertsDotNet.Utils.Consts;
using DeskAlertsDotNet.Utils.Pages;
using Resources;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace DeskAlertsDotNet.Pages
{
    public abstract class DeskAlertsBaseListPage : DeskAlertsBasePage
    {
        public static void CleanUploadBulkOfRecipientsDirectory()
        {
            var directoryInfo = new DirectoryInfo(UploadPath);

            foreach (var file in directoryInfo.GetFiles())
            {
                file.Delete();
            }
        }

        public static readonly string UploadPath = Config.Data.GetString("alertsFolder") + "\\upload\\";
        //TODO: Replace to UrlHelper.Search
        public string SearchTerm { get; set; }

        public int Offset
        {
            get
            {
                if (UrlHelper == null)
                {
                    return 0;
                }

                return UrlHelper.Offset;
            }
            set => UrlHelper.Offset = value;
        }
        public string Webalert
        {
            get
            {
                if (UrlHelper == null)
                {
                    return string.Empty;
                }
                return UrlHelper.Webalert;
            }
            set => UrlHelper.Webalert = value;
        }
        public int Limit
        {
            get
            {
                if (UrlHelper == null)
                {
                    return 25;
                }

                return UrlHelper.Limit;
            }
        }

        public string SortBy
        {
            get
            {
                if (UrlHelper == null)
                {
                    return $"{DefaultOrderCollumn} {DefaultSortingOrder}";
                }

                return UrlHelper.SortBy;
            }
        }

        protected DataSet Content { get; set; } = new DataSet();

        protected new string Page { get; set; }

        protected Table Table { get; set; }

        protected virtual bool IsUsedDefaultTableSettings => true;

        protected virtual DataSet UploadedRecipients { get; set; }

        private int Count { get; set; }

        public UrlHelper UrlHelper { get; set; }

        protected override void OnLoad(EventArgs e)
        {
            SetSearchTermAndSearchControl();
            Page = GetPageName();

            if (!string.IsNullOrEmpty(Request["delete"]))
            {
                string idsToDelete = Request["delete"];
                dbMgr.ExecuteQuery($"DELETE FROM {this.DataTable} WHERE {this.IdCollumn} IN ({idsToDelete})");
            }

            string query;
            string countQuery;

            if (CustomQuery.Length > 0 && CustomCountQuery.Length > 0)
            {
                query = CustomQuery;
                countQuery = CustomCountQuery;
            }
            else
            {
                // Remove id collumn from selected if exists
                string[] colls = Collumns;
                if (colls.Contains(IdCollumn))
                {
                    colls = Collumns.Where(col => !col.Equals(IdCollumn)).ToArray();
                }

                string condStr = ConditionSqlString;

                if (SearchControl != null && SearchField.Length > 0)
                {
                    condStr = $" {SearchField} LIKE '%{SearchTerm}%'";

                    if (ConditionSqlString.Length > 0)
                    {
                        condStr += " AND " + ConditionSqlString;
                    }
                }

                query = condStr.Length == 0
                            ? $"SELECT {IdCollumn}, {string.Join(",", colls)} FROM {DataTable}"
                            : $"SELECT {IdCollumn}, {string.Join(",", colls)} FROM {DataTable} WHERE {condStr}";

                var orderBy = Request["sortBy"];
                if (!string.IsNullOrEmpty(orderBy))
                {
                    query += $" ORDER BY {orderBy}";
                }
                else
                {
                    query += $" ORDER BY {DefaultOrderCollumn} {DefaultSortingOrder}";
                }

                countQuery = condStr.Length == 0
                                 ? $"SELECT count(1) FROM {this.DataTable}"
                                 : $"SELECT count(1) FROM {this.DataTable} WHERE {condStr}";
            }

            Count = UploadedRecipients?.Count ?? Convert.ToInt32(dbMgr.GetScalarByQuery(countQuery));

            var url = new Uri(Config.GetAlertsDir(), Request.Url.AbsolutePath).ToString();

            var offset = IsPostBack && IsUsedDefaultTableSettings && string.IsNullOrEmpty(Request["offset"]) ? "0" : Request["offset"];
            var limit = IsPostBack && IsUsedDefaultTableSettings && string.IsNullOrEmpty(Request["limit"]) ? "25" : Request["limit"];
            var sortBy = Request["sortBy"];
            var webalert = Request["webalert"];
            var alertType = Request["AlertType"];

            UrlHelper = new UrlHelper(
                url,
                SearchTerm,
                PageParams,
                DefaultSortingOrder,
                DefaultOrderCollumn,
                sortBy,
                ContentName,
                offset,
                limit,
                Count,
                webalert,
                alertType);

            var tableHasContent = Count > 0;

            TableDiv.Visible = tableHasContent;
            NoRowsDiv.Visible = !tableHasContent;

            if (tableHasContent)
            {
                if (UploadedRecipients != null)
                {
                    Content = UploadedRecipients;
                }
                else
                {
                    Content = dbMgr.GetDataByQuery(query);
                }

                string draftParamStr = string.Empty;

                string webalertParamStr = string.Empty;

                if (!string.IsNullOrEmpty(Request["webalert"]))
                {
                    webalertParamStr = "webalert=1&";
                }

                var pageMarkup = UrlHelper.MakePages();
                if (TopPaging != null)
                {
                    TopPaging.InnerHtml = pageMarkup;
                }

                if (BottomPaging != null)
                {
                    BottomPaging.InnerHtml = pageMarkup;
                }

                foreach (IButtonControl button in DeleteButtons)
                {
                    button.Text = resources.LNG_DELETE;
                    button.Click += OnDeleteButtonClick;
                }
            }
        }

        private void SetSearchTermAndSearchControl()
        {
            if (string.IsNullOrEmpty(SearchTerm))
            {
                var searchTermBox = Request["searchTermBox"];
                SearchTerm = string.IsNullOrEmpty(searchTermBox) ? string.Empty : searchTermBox;
            }

            if (!IsPostBack && SearchControl != null)
            {
                SearchControl.Value = SearchTerm;
            }
            else if (IsPostBack && SearchControl != null)
            {
                SearchTerm = SearchControl.Value;
            }

            SearchTerm = SearchTerm.EscapeQuotes();
        }

        /// <summary>
        /// Generate link for sorting table by collumn
        /// </summary>
        /// <param name="column"> Collumn name in Asp:Table </param>
        /// <param name="dbColl"> Collumn name im Data base</param>
        /// <param name="lastSortBy">last sort order (highly recommended to pass Request["sortBy"] into this parameter)</param>
        /// <returns>Html code of sorting link</returns>
        protected string GetSortLink(string column, string dbColl, string lastSort)
        {
            string sortBy = SortBy;
            var asc = " ASC";
            var desc = " DESC";
            if (sortBy.Split(' ')[0] == dbColl)
            {
                if (sortBy.Contains(asc))
                {
                    sortBy = sortBy.Replace(asc, desc);
                }
                else if (sortBy.Contains(desc))
                {
                    sortBy = sortBy.Replace(desc, asc);
                }
            }
            else
            {
                sortBy = dbColl + desc;
            }

            var uri = UrlHelper.GetUrl(sortBy, Offset, Limit);
            var hrefSortingColumn = $"<a href='{uri}'>{column}</a>";
            return hrefSortingColumn;
        }

        /// <summary>
        /// Specifies collumn in database by which table will be sort by default
        /// </summary>
        protected abstract string DefaultOrderCollumn
        {
            get;
        }

        /// <summary>
        /// Specifies html div object for content table
        /// </summary>
        protected abstract HtmlGenericControl TableDiv
        {
            get;
        }

        protected virtual string DefaultSortingOrder
        {
            get { return "DESC"; }
        }

        protected virtual string PageParams
        {
            get
            {
                var uri = string.Empty;
                uri += AddParameter(uri, "question_id");
                return uri;
            }
        }

        /// <summary>
        /// Return URL with parameter from Request.
        /// </summary>
        /// <param name="uri">Current parmeters string.</param>
        /// <param name="parameter">Parameter name from Request.</param>
        /// <returns>Uri string with new parameter.</returns>
        public string AddParameter(string uri, string parameter)
        {
            var url = $"{parameter}={Request[parameter]}";
            return string.IsNullOrEmpty(uri) ? url : $"&{url}";
        }

        /// <summary>
        /// Return URL with parameter from Request.
        /// </summary>
        /// <param name="uri">Current parmeters string.</param>
        /// <param name="parameter">Parameter name from Request.</param>
        /// <param name="value">Value of the parameter.</param>
        /// <returns>Uri string with new parameter.</returns>
        public string AddParameter(string uri, string parameter, bool value)
        {
            var url = $"{parameter}={(value ? "1" : "0")}";
            var result = string.IsNullOrEmpty(uri) ? url : $"&{url}";
            return result;
        }

        /// <summary>
        /// Specifies html div object which will be visible for user in case of any content was not found
        /// </summary>
        protected abstract HtmlGenericControl NoRowsDiv
        {
            get;
        }

        /// <summary>
        /// Handler for delete buttons
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnDeleteButtonClick(object sender, EventArgs e)
        {
            if (Config.Data.IsDemo && curUser.Name != "admin" && !curUser.IsSalesManager)
            {
                base.Page.ClientScript.RegisterStartupScript(GetType(),
                    "ButtonClickScript", "alert('" + resources.LNG_DISABLED_IN_DEMO + "');", true);
                return;
            }

            var deletedIds = new List<string>();
            foreach (TableRow row in Table.Rows)
            {
                var chb = row.Cells[0].Controls[0] as CheckBox;
                if (chb != null && chb.Checked)
                {
                    var id = chb.InputAttributes["value"];
                    var intId = 0;
                    if (int.TryParse(id, out intId))
                    {
                        deletedIds.Add(chb.InputAttributes["value"]);
                    }
                    else if (chb.InputAttributes["value"] != null && chb.InputAttributes["value"].Length > 0)
                    {
                        deletedIds.Add("'" + chb.InputAttributes["value"] + "'");
                    }
                }
            }

            var idsString = string.Join(",", deletedIds.ToArray());
            if (deletedIds.Count > 0)
            {
                OnDeleteRows(deletedIds.ToArray());
                dbMgr.ExecuteQuery(string.Format("DELETE FROM {0} WHERE {1} IN ({2})", DataTable, IdCollumn, idsString));
            }

            string redirectUrl = Request.Url.AbsoluteUri;
            if (AppConfiguration.IsNewContentDeliveringScheme)
            {

            }
            else
            {
                CacheManager cacheManager = Global.CacheManager;
                cacheManager.RebuildCache(DateTime.Now.ToString(DateTimeFormat.MsSqlFormat, CultureInfo.InvariantCulture));
            }

            Response.Redirect(redirectUrl);
        }


        protected virtual void OnDeleteRows(string[] ids)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns> Current web page file name with extention</returns>
        /// <example>
        /// On page with path %host%/admin/daSimplePage.aspx
        /// Method will return daSimplePage.aspx
        /// </example>
        protected string GetPageName()
        {
            string pagePath = System.Web.HttpContext.Current.Request.Url.AbsolutePath;
            System.IO.FileInfo fileInfo = new System.IO.FileInfo(pagePath);
            return fileInfo.Name;

        }

        /// <summary>
        /// Specifies name of your content to display it in table header
        /// </summary>
        protected virtual string ContentName
        {
            get
            {
                return string.Empty;
            }
        }
        /// <summary>
        /// Specifies Html div object for paging at the top of table
        /// </summary>
        protected virtual HtmlGenericControl TopPaging
        {
            get
            {
                return null;
            }
        }
        /// <summary>
        /// Specifies Html div object for paging at the bottom of table
        /// </summary>
        protected virtual HtmlGenericControl BottomPaging
        {
            get
            {
                return null;
            }
        }
        /// <summary>
        /// Specifies identifier collumn name in DB
        /// </summary>
        protected virtual string IdCollumn
        {
            get
            {
                return "id";
            }
        }
        /// <summary>
        /// Specifies table in database with content
        /// </summary>
        /// <seealso cref="ConditionSqlString"/>
        /// <seealso cref="Collumns"/>
        protected abstract string DataTable
        {
            get;
        }

        /// <summary>
        /// Specifies collumns in database from DataTable which you should get.
        /// </summary>
        /// <seealso cref="DataTable"/>
        /// <seealso cref="ConditionSqlString"/>
        protected abstract string[] Collumns
        {
            get;
            // private set;
        }

        /// <summary>
        /// Specifies condition for content selection
        /// </summary>
        /// <seealso cref="Collumns"/>
        /// <seealso cref="DataTable"/>

        protected virtual string ConditionSqlString
        {
            get
            {

                return string.Empty;
            }
        }

        /// <summary>
        /// Use it if you need non-standard selection
        /// Specifies custom SQL query for your selection
        /// </summary>
        /// <seealso cref="CustomCountQuery"/>
        protected virtual string CustomQuery
        {
            get
            {
                return string.Empty;
            }
        }
        /// <summary>
        /// Use it if you need non-standard selection
        /// Specifies custom SQL query to get count of rows in table
        /// </summary>
        /// <seealso cref="CustomQuery"/>
        protected virtual string CustomCountQuery
        {
            get
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Specifies array of buttons which will be use for content deleting
        /// </summary>
        protected virtual IButtonControl[] DeleteButtons
        {
            get
            {
                return new IButtonControl[] { };
            }
        }
        /// <summary>
        /// Override this method if you need to format values for some collumns
        /// </summary>
        /// <param name="fieldKey"></param>
        /// <param name="fieldValue"></param>
        /// <returns>Formated collumn value</returns>
        protected virtual string FormatFieldData(string fieldKey, string fieldValue, DataRow row)
        {
            return fieldValue;
        }

        /// <summary>
        /// Override this method if you use standatd selection and need to generate some actions buttons for each content row
        /// </summary>
        /// <param name="row">Row for which you want to generate action buttons.</param>
        /// <returns>List of action buttons</returns>
        protected virtual List<Control> GetActionControls(DataRow row)
        {
            return null;
        }

        /// <summary>
        /// Default method to fill content table if you use standard content selection
        /// </summary>
        protected virtual void FillTableWithContent(bool haveCheckbox)
        {
            if (Table == null)
            {
                return;
            }

            var cnt = Content.Count < (UrlHelper.Offset + UrlHelper.Limit) ? Content.Count : UrlHelper.Offset + UrlHelper.Limit;

            for (var i = UrlHelper.Offset; i < cnt; i++)
            {
                var dataRow = Content.GetRow(i);
                var rowDict = dataRow.ToDictionary();
                var newRow = new TableRow();
                if (haveCheckbox)
                {
                    var checkCell = new TableCell { HorizontalAlign = HorizontalAlign.Center };
                    var chb = GetCheckBoxForDelete(dataRow);
                    checkCell.Controls.Add(chb);
                    newRow.Cells.Add(checkCell);
                }

                foreach (var kvp in rowDict)
                {
                    var key = kvp.Key;
                    if (key.Equals(IdCollumn))
                    {
                        continue;
                    }

                    var value = kvp.Value is DateTime ? DateTimeConverter.ToUiDateTime((DateTime)kvp.Value) : kvp.Value.ToString();
                    value = FormatFieldData(key, value, dataRow);
                    var cell = new TableCell
                    {
                        Text = value,
                        HorizontalAlign = HorizontalAlign.Center
                    };

                    newRow.Cells.Add(cell);
                }

                var actions = GetActionControls(dataRow);

                if (actions != null && actions.Count > 0)
                {
                    var actionsCell = new TableCell();

                    foreach (var crtl in actions)
                    {
                        actionsCell.Controls.Add(crtl);
                    }

                    actionsCell.HorizontalAlign = HorizontalAlign.Center;
                    newRow.Cells.Add(actionsCell);
                }

                Table.Rows.Add(newRow);
            }
        }

        /// <summary>
        /// Generate checkbox to delete row
        /// </summary>
        /// <param name="dataRow">Row for which you want to generate checkbox</param>
        /// <returns>CheckBox object for row</returns>
        protected CheckBox GetCheckBoxForDelete(DataRow dataRow)
        {
            var id = dataRow.GetString(IdCollumn);
            var chb = new CheckBox { ID = "chbDel_" + id };
            chb.InputAttributes.Add("class", "content_object");
            chb.InputAttributes.Add("value", id);
            return chb;
        }
        /// <summary>
        /// Specifies fields in DB which will be use for search
        /// </summary>
        protected virtual string SearchField
        {
            get
            {
                return string.Empty;
            }
            set
            {
                SearchField = value;
            }

        }

        protected virtual HtmlInputText SearchControl
        {
            get
            {
                return null;
            }
        }
        /// <summary>
        /// Generate action button
        /// </summary>
        /// <param name="id"> Id of content row</param>
        /// <param name="imgUrl">Button`s image</param>
        /// <param name="altLocKey"> Localization key for alternative text of image</param>
        /// <param name="ac">Click action for button</param>
        /// <returns>Action button with passed parameters</returns>
        protected LinkButton GetActionButton(string id, string imgUrl, string altLocKey, EventHandler ac, int padding = 5)
        {
            var editButton = new LinkButton();
            var editImg = new Image { ImageUrl = imgUrl };

            editImg.Attributes["title"] = resources.ResourceManager.GetString(altLocKey);
            editButton.Controls.Add(editImg);
            editButton.Style.Add(HtmlTextWriterStyle.MarginRight, padding + "px");
            editButton.Click += ac;
            return editButton;
        }

        public LinkButton GetActionButton(string id, string imgUrl, string altLocKey, string javascriptCodeString,
            int margin = 5)
        {
            LinkButton editButton = new LinkButton();
            Image editImg = new Image();
            editImg.ImageUrl = imgUrl;//

            editImg.Attributes["title"] = Resources.resources.ResourceManager.GetString(altLocKey);
            editButton.Controls.Add(editImg);
            editButton.Style.Add(HtmlTextWriterStyle.MarginRight, margin + "px");
            editButton.Attributes["href"] = "#";
            if (!string.IsNullOrEmpty(javascriptCodeString))
            {
                editButton.Attributes["onClick"] = javascriptCodeString;
            }

            return editButton;
        }
    }
}