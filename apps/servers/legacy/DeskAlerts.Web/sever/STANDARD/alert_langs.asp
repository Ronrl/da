<?xml version="1.0" encoding="utf-8"?>
<LANGUAGES>
<!-- #include file="admin/config.inc" -->
<!-- #include file="admin/db_conn.asp" -->
<%
if ConfEnableAlertLangs = 1 then
	Set RS = Conn.Execute("SELECT name, abbreviatures, is_default FROM alerts_langs")
	Do While(Not RS.EOF)
		name = HTMLEncode(RS("name"))
		abbr = HTMLEncode(RS("abbreviatures"))
		def = Abs(CInt(RS("is_default")))
		Response.Write vbTab&"<LANG name="""&name&""" abbreviatures="""&abbr&""" is_default="""&def&"""/>"&vbNewLine
		RS.MoveNext
	Loop
	RS.Close
end if
%></LANGUAGES>
<!-- #include file="admin/db_conn_close.asp" -->