<%@ Language=VBScript CodePage=65001%>
<% db_backup_stop_working = 1 %>
<!-- #include file="admin/config.inc" -->
<% ConnCount = 2 %>
<!-- #include file="admin/db_conn.asp" -->
<!-- #include file="admin/md5.asp" -->
<!-- #include file="api_functions.inc" -->
<%
xml_data = request ("xml_data")
api_signature = request ("api_signature")
api_method = "groups_members"

'validate signature
if(apiValidateSignature(xml_data, api_signature)=true) then
	'valid
	'validate xml by xsd
	if(apiValidateXML(xml_data, "api/group_members.xsd")=true) then
		'parse xml and send alert
		apiParseXML xml_data, api_method
	else
		response.write apiResult (api_method, "ERROR", "Not valid xml_data", 0)
	end if
else
	'invalid
	response.write apiResult (api_method, "ERROR", "Not valid api_signature", 0)
end if

%>
<!-- #include file="admin/db_conn_close.asp" -->