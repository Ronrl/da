﻿using System;
using System.Collections.Generic;

namespace DeskAlertsDotNet.Pages
{
    public partial class MultipleUpdate : DeskAlertsBasePage
    {
        private string alertId;
        private string partId;
        private string userId;
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            alertId = Request["alertId"];
            partId = Request["partId"];
            var dict = new Dictionary<string, object> { { "confirmed", 1}};
            var conditions = new Dictionary<string, object> {{"alert_id", alertId}, {"part_id", partId}};
            dbMgr.Update("multiple_alerts", dict, conditions);
            var query = "SELECT confirmed FROM multiple_alerts WHERE alert_id=" + alertId;
            var confirmList = dbMgr.GetDataByQuery(query);
            query = "SELECT alert_text FROM alerts WHERE id=" + alertId;
            var content = dbMgr.GetScalarQuery<string>(query);
            var id = 1;
            foreach (var item in confirmList)
            {
                if ((int)item.GetValue("confirmed") == 1)
                    content = content.Replace(
                        @"<input class=""sendData"" type=""submit"" id=""submitButton" + id + @""" value=""Agree""/>",
                        "<span> Confirmed </span>");
                id++;
            }
            Response.Write(content);
            Response.End();
        }
    }
}