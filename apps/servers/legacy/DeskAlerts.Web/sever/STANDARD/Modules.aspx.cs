﻿using System;
using DeskAlertsDotNet.Parameters;

namespace DeskAlertsDotNet.Pages
{
    public partial class ModulesChecker : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string module = Request.GetParam("mod", "");


            switch (module)
            {
                case "ad":
                {
                    Response.Write( Config.Data.HasModule(Parameters.Modules.AD) ? 1 : 0);
                    break;
                }

                case "dec":
                {
                    Response.Write(Config.Data.HasModule(Parameters.Modules.ENCRYPT) ? 1 : 0);
                    break;
                }

                case "mail":
                {
                    Response.Write(Config.Data.HasModule(Parameters.Modules.EMAIL) ? 1 : 0);
                    break;
                }

                case "sms":
                {
                    Response.Write(Config.Data.HasModule(Parameters.Modules.SMS) ? 1 : 0);
                    break;
                }

                case "video":
                {
                    Response.Write(Config.Data.HasModule(Parameters.Modules.VIDEO) ? 1 : 0);
                    break;
                }


            }


        }
    }
}