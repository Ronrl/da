﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using DeskAlertsDotNet.Parameters;

namespace DeskAlertsDotNet.Attributes
{
    public class DeskAlertsAuthorizeAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            if (ValidateApiKey(actionContext))
                return;

            HandleUnauthorizedRequest(actionContext);
        }

        private bool ValidateApiKey(HttpActionContext actionContext)
        {
            IEnumerable<string> apiKeyValues;
            bool succesReadApiKeysFromRequest = actionContext.Request.Headers.TryGetValues("apiKey", out apiKeyValues);
            if (succesReadApiKeysFromRequest)
            {
                return Settings.Content["ConfAPISecret"] == apiKeyValues.First();
            }

            return false;
        }
    }
}