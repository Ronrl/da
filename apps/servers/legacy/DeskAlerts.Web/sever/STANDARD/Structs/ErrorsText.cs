﻿namespace DeskAlertsDotNet.LogStructs
{
    struct ErrosText
    {
        public enum ErrorList
        {
            UserSignUp,
            UserLogOut,
            UserSaveSettings,
            UserChangeSettings,
        }

        static public string GetErrorText(ErrorList error)
        {
            string result = string.Empty;
            switch (error)
            {
                case ErrorList.UserSignUp:
                    result = " is signup in DeskAlert server";
                    break;
                case ErrorList.UserLogOut:
                    result = " is signlogout in DeskAlert server";
                    break;
                case ErrorList.UserSaveSettings:
                    result = " is save settings in DeskAlert server in ";
                    break;
            }
            return result;
        }
    }
}