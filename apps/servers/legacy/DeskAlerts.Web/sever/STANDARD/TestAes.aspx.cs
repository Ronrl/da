﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Parameters;
using DeskAlertsDotNet.Cryptography;


namespace DeskAlertsDotNet.Pages
{
    public partial class TestAes : DeskAlertsBasePage
    {
        //use curUser to access to current user of DA control panel      
        //use dbMgr to access to database

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);


            string originalData = Request["data"];
            
            if(string.IsNullOrEmpty(originalData))
                return;

            Response.WriteLine("Original data = " + originalData);
            string encryptedData = Aes256.Encode(originalData);
            Response.WriteLine("Encrypted data = " + encryptedData);
            string decryptedData = Aes256.Decode(encryptedData);
            Response.WriteLine("Decrypted data = " + decryptedData);

            string crashLogPath = Config.Data.GetString("alertsFolder") + "\\admin\\logs\\crashreport" +
                      DateTime.Now.ToString("dd-MM-yyyy HH-mm-ss") + ".log";

            using (StreamWriter sw = new StreamWriter(crashLogPath, true))
            {
                sw.WriteLine(originalData);
                sw.Close();
            }
            //Add your main code here
        }
    }
}