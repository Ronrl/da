﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Parameters;


namespace DeskAlertsDotNet.Pages
{
    public partial class AlertLangs : DeskAlertsBasePage
    {
        //use curUser to access to current user of DA control panel      
        //use dbMgr to access to database

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Response.Write("<LANGUAGES>");
            var langSet = dbMgr.GetDataByQuery("SELECT * FROM alerts_langs");

            foreach(var langRow in langSet)
            {
                Response.Write($"<LANG name = { langRow.GetString("name").DoubleQuotes() } " +
                    $"abbreviatures = { langRow.GetString("abbreviatures").DoubleQuotes()} " +
                    $"is_default = {langRow.GetInt("is_default").ToString().DoubleQuotes()} />");
            }
            Response.Write("</LANGUAGES>");
            //Add your main code here
        }
    }
}