<% db_backup_stop_working = 1 %>
<!-- #include file="admin/config.inc" -->
<!-- #include file="admin/db_conn.asp" -->
<!-- #include file="admin/md5.asp" -->
<!-- #include file="admin/ad_functions.inc" -->
<%
Server.ScriptTimeout = 90000
Conn.CommandTimeout = 60

synchSQL = "SELECT" & vbNewLine & _
			"s.id" & vbNewLine & _
			"FROM synchronizations AS s" & vbNewLine & _ 
			"INNER JOIN recurrence as r" & vbNewLine & _
			"ON s.recur_id = r.id" & vbNewLine & _
			"WHERE" & vbNewLine & _
			"dbo.isActualRecurrence(" & vbNewLine & _
				"r.pattern," & vbNewLine & _
				"r.number_days," & vbNewLine & _
				"r.number_week," & vbNewLine & _
				"r.week_days," & vbNewLine & _
				"r.month_day," & vbNewLine & _
				"r.number_month," & vbNewLine & _
				"r.weekday_place," & vbNewLine & _ 
				"r.weekday_day," & vbNewLine & _ 
				"r.month_val," & vbNewLine & _
				"r.dayly_selector," & vbNewLine & _
				"r.monthly_selector," & vbNewLine & _
				"r.yearly_selector," & vbNewLine & _
				"'no_date'," & vbNewLine & _
				"1," & vbNewLine & _
				"s.start_time," & vbNewLine & _
				"DATEADD(ms, -3, DATEADD(d, DATEDIFF(d, 0, GetDate()), 1))," & vbNewLine & _
				"0," & vbNewLine & _
				"s.last_sync_time" & vbNewLine & _
			") = 1"
			
Set RSSync = Conn.Execute(synchSQL)

Dim syncWasStarted

syncWasStarted = "No"

Do While Not RSSync.EOF
    Dim syncId
    Set syncId = RSSync("id")

    Dim lastTimeSyncStartedRow
    Set lastTimeSyncStartedRow = Conn.Execute("SELECT last_time_sync_started FROM synchronizations WHERE id = '" & syncId & "'")

    If DateDiff("d", lastTimeSyncStartedRow("last_time_sync_started"), Now) >= 1 Then
        syncAD syncId
        syncWasStarted = "Yes"
    End If
	
	RSSync.MoveNext
Loop

Conn.Execute("UPDATE settings SET val = CONVERT(nvarchar, GETUTCDATE(), 9) WHERE name='ConfSyncTaskTimeStamp'")

Response.Write syncWasStarted & " done"
%>
<!-- #include file="admin/db_conn_close.asp" -->