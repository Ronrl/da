﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ShowToken.aspx.cs" Inherits="DeskAlerts.Server.sever.STANDARD.ShowToken" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Tokens</title>
    <style>
        table {
            width: 100%;
            text-align: center;
            table-layout: fixed;
            border: 1px solid grey;
            margin: 0.4rem 0;
            padding: 0;
            border-spacing: 0;
        }

            table tr:nth-child(1) {
                background-color: burlywood;
            }

                table tr:nth-child(1):hover {
                    background-color: burlywood;
                }

            table tr:nth-child(2n) {
                background-color: #fff5e6;
            }

            table tr:hover {
                background-color: bisque;
            }

            table tr th {
                padding: 0.3rem;
            }

            table tr td {
                padding: 0.2rem;
            }
    </style>
</head>
<body>
    <form id="TokensForm" runat="server">
        <div>
            <asp:ListView runat="server" ID="TokenViewerList">
                <LayoutTemplate>
                    <table runat="server">
                        <tr runat="server">
                            <th runat="server">TokenGuid</th>
                            <th runat="server">UserName</th>
                            <th runat="server">Password</th>
                            <th runat="server">DeskbarId</th>
                            <th runat="server">DomainName</th>
                            <th runat="server">DistinguishedName</th>
                            <th runat="server">ComputerName<br>(ComputerDomainName)</th>
                            <th runat="server">Ip</th>
                        </tr>
                        <tr runat="server" id="itemPlaceholder"></tr>
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <tr runat="server">
                        <td runat="server"><%# Eval("TokenGuid")%></td>
                        <td runat="server"><%# Eval("UserName")%></td>
                        <td runat="server"><%# Eval("Password")%></td>
                        <td runat="server"><%# Eval("DeskbarId")%></td>
                        <td runat="server"><%# Eval("DomainName")%></td>
                        <td runat="server"><%# Eval("DistinguishedName")%></td>
                        <td runat="server"><%# Eval("ComputerName")%><br/><%# Eval("ComputerDomainName")%></td>
                        <td runat="server"><%# Eval("Ip")%></td>
                    </tr>
                </ItemTemplate>
            </asp:ListView>
        </div>
    </form>
</body>
</html>
