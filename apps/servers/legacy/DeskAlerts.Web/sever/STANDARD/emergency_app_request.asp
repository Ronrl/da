<!-- #include file="admin/config.inc" -->
<!-- #include file="admin/db_conn.asp" -->
<!-- #include file="admin/md5.asp" -->
<%
response.ContentType="application/json"
login1 = replace(Request("login") , "'", "''")
pass = Request("password")
if(login1 <> "" AND pass <> "") then
	if login1="admin" then
		errorMessage="You can't use superadmin account in this application"
	else
		SQL = "SELECT u.role, p.type, pl.im_val FROM policy_list pl, policy_editor pe, users u, policy p WHERE pe.editor_id=u.id AND pe.policy_id=pl.policy_id AND pe.policy_id=p.id AND u.name='" + login1 + "' AND u.pass='" + md5(pass) + "' AND u.role<>'U'"
		if(Err.Number <> 0) then
			errorMessage="Cannot connect to database: "&Err.Description
		else
			Set rs = Conn.Execute(SQL)
			if(Err.Number <> 0) then
				errorMessage= "Can't execute SQL query"
			else
				if ( Not rs.EOF ) then
					policy_type=rs("type")
					if policy_type="A" then
						send_right="1"
					else
						im_val = rs("im_val")
						send_right = Mid(im_val,4,1)
					end if
					if send_right="1" then
						errorMessage=""
					else
						errorMessage="This publisher doesn't have rights to send Emergency notifications."
					end if
				else
					errorMessage="Wrong publisher credentials"
  				end if
			end if
			rs.Close
		end if
	end if
else
	errorMessage="Please specify a login and non-empty password"
end if

if errorMessage<>"" then
	Response.write "{""error"":"""&errorMessage&"""}"
	Response.end
end if

Response.write "{"

Response.write """alerts"":["
Set rsAlerts = Conn.Execute("SELECT a.id, cc.color, a.title FROM instant_messages im, alerts a, color_codes cc WHERE im.alert_id=a.id AND cc.id=a.color_code UNION ALL SELECT a.id, '747474' as color, a.title FROM instant_messages im, alerts a WHERE im.alert_id=a.id AND a.color_code not in (SELECT id from color_codes)")
fl=0
do while not rsAlerts.EOF
	alert_id = rsAlerts("id")
	alert_title = rsAlerts("title")
	tile_color = rsAlerts("color")
	if fl=1 then
		Response.write ","
	else 
		fl=1
	end if
	Response.write "{""id"":"&alert_id&",""title"":"""&alert_title&""",""color"":""#"&tile_color&"""}"
	rsAlerts.MoveNext
loop
rsAlerts.close
Response.write "]"

Response.write ","

Response.write """color_codes"":["
Set rsColors = Conn.Execute("SELECT name, color FROM color_codes")
fl=0
do while not rsColors.EOF
	color_title = rsColors("name")
	color_hex = rsColors("color")
	if fl=1 then
		Response.write ","
	else 
		fl=1
	end if
	Response.write "{""title"":"""&color_title&""",""hex"":""#"&color_hex&"""}"
	rsColors.MoveNext
loop
rsColors.close
Response.write "]"

Response.write ","

Response.write """api_key"":"""
Set RSkey = Conn.Execute("SELECT val FROM settings WHERE name='ConfAPISecret'")
if (not RSkey.EOF) then
	api_key = RSkey("val")
end if
Response.write api_key
Response.write """"

Response.write "}"
%>
<!-- #include file="admin/db_conn_close.asp" -->