﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Models;

namespace DeskAlertsDotNet.Models.WidgetExtentions
{

    public static class WidgetListExtention
    {
        public static string ToJsonArray(this List<Widget> widgetsList, WidgetPage page = WidgetPage.DASHBOARD)
        {
            Widget[] arr = widgetsList.Where(widget => widget.Page == page).ToArray();

            return new JavaScriptSerializer().Serialize(arr);
        }
    }
}