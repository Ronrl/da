﻿using System;
using System.Collections.Generic;
using System.Linq;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.Server.Utils;

namespace DeskAlertsDotNet.Models.Permission
{
    public class Policy
    {
        public int Id { get; set; }

        public bool CanSendAllGroups { get; set; }

        public bool CanSendAllOUs { get; set; }

        public bool CanSendAllTemplates { get; set; }

        public bool CanSendAllUsers { get; set; }

        public bool CanViewAll { get; set; }

        public List<long> Groups { get; set; }

        public List<long> OrganizationUnits { get; set; }

        public List<long> RecipientsList { get; set; }

        public List<long> SendersViewList { get; set; }

        public List<int> TemplatesGroups { get; set; }

        public List<Guid> SkinList { get; set; }

        public List<string> ContentSettingsList { get; set; }

        public bool IsBroadcastEnabled { get; set; }

        public bool IsAdminPolicy { get; set; }

        public bool IsModeratorPolicy { get; set; }

        public bool IsPublisherPolicy { get; set; }

        public Dictionary<Policies, Permission> Permissions { get; set; }

        public bool HasPermissions { get { return Permissions.Count > 0; } }

        private readonly static Dictionary<AlertType, Policies> alertClassPolicyRelations = new Dictionary<AlertType, Policies>()
        {
            [AlertType.SimpleAlert] = Policies.ALERTS,
            [AlertType.DigitalSignature] = Policies.ALERTS,
            [AlertType.EmergencyAlert] = Policies.IM,
            [AlertType.RssAlert] = Policies.RSS,
            [AlertType.RssFeed] = Policies.RSS,
            [AlertType.ScreenSaver] = Policies.SCREENSAVERS,
            [AlertType.Wallpaper] = Policies.WALLPAPERS,
            [AlertType.SimpleSurvey] = Policies.SURVEYS,
            [AlertType.SurveyPoll] = Policies.SURVEYS,
            [AlertType.SurveyQuiz] = Policies.SURVEYS
        };

        public Permission this[AlertType alertClass]
        {
            get
            {
                var policy = Policy.FromAlertClass(alertClass);
                return this[policy];
            }
        }

        private static Policies FromAlertClass(AlertType alertClass)
        {
            if (!alertClassPolicyRelations.ContainsKey(alertClass))
            {
                return Policies.ALERTS;
            }

            return alertClassPolicyRelations[alertClass];
        }

        public Permission this[Policies policy]
        {
            get { return Permissions[policy]; }
        }

        public Policy()
        {
            Groups = new List<long>();
            OrganizationUnits = new List<long>();
            RecipientsList = new List<long>();
            SendersViewList = new List<long>();
            TemplatesGroups = new List<int>();
            Permissions = new Dictionary<Policies, Permission>();
        }

        public override string ToString()
        {
            string result = "";
            foreach (KeyValuePair<Policies, Permission> keyValuePair in Permissions)
            {
                var key = keyValuePair.Key;
                var permission = keyValuePair.Value;
                result += "-----Permission name:" + key + "<br>";

                var permissionProperties = typeof(Permission).GetProperties();

                foreach (var permissionProperty in permissionProperties)
                {
                    result += "     " + permissionProperty.Name + ": " + permissionProperty.GetValue(permission) + "<br>";
                }
            }

            return result;

        }

        public bool AvailableForAction(PoliciesActions action)
        {
            var actionStr = action.ToString("f").ToLower();
            actionStr = DeskAlertsUtils.BeautifyString(actionStr);
            var propName = "Can" + actionStr;
            int permissionsCount = Permissions.Count(p => p.Value.CheckByName(propName));
            bool result = permissionsCount != 0;

            return result;
        }

        private static Dictionary<string, Permission> MergePermissions(Dictionary<string, Permission> permissionA,
            Dictionary<string, Permission> permissionB)
        {
            var permissions = new Dictionary<string, Permission>();
            foreach (var pair in permissionA)
            {
                permissions[pair.Key] = Permission.Merge(permissionA[pair.Key], permissionB[pair.Key]);
            }

            return permissions;
        }
    }
}