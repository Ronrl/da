﻿namespace DeskAlertsDotNet.Models.Permission
{
    public enum PoliciesActions
    {
        CREATE,
        EDIT,
        DELETE,
        SEND,
        VIEW,
        STOP,
        RESIZE,
        APPROVE
    }
}