﻿namespace DeskAlertsDotNet.Models.Permission
{
    public enum Policies
    {
        Undefined,
        ALERTS,
        EMAILS,
        CAMPAIGN,
        SMS,
        USERS,
        GROUPS,
        TEMPLATES,
        TEXT_TEMPLATES,
        STATISTICS,
        IP_GROUPS,
        RSS,
        SCREENSAVERS,
        WALLPAPERS,
        LOCKSCREENS,
        IM,
        TEXT_TO_CALL,
        FEEDBACK,
        SURVEYS,
        CC
    }
}