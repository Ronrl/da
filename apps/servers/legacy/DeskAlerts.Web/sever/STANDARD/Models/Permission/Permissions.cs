﻿using System;
using System.Collections.Generic;
using System.Text;
using DeskAlertsDotNet.Parameters;

namespace DeskAlertsDotNet.Models.Permission
{
    public struct Permission
    {
        public static readonly int DefaultMaskBitsCount = 8;

        public Permission(string mask) : this()
        {
            if (mask.Length < DefaultMaskBitsCount)
                mask += new string('0', DefaultMaskBitsCount - mask.Length);


            var symbols = mask.ToCharArray();

            CanCreate = symbols[0] == '1';
            CanEdit = symbols[1] == '1';
            CanDelete = symbols[2] == '1';
            CanSend = symbols[3] == '1';
            CanView = symbols[4] == '1';
            CanStop = symbols[5] == '1';
            CanResize = symbols[6] == '1';
            CanApprove = symbols[7] == '1';
        }

        public override string ToString()
        {

            var stringBuilder = new StringBuilder();

            stringBuilder.Append(Convert.ToInt32(CanCreate));
            stringBuilder.Append(Convert.ToInt32(CanEdit));
            stringBuilder.Append(Convert.ToInt32(CanDelete));
            stringBuilder.Append(Convert.ToInt32(CanSend));
            stringBuilder.Append(Convert.ToInt32(CanView));
            stringBuilder.Append(Convert.ToInt32(CanStop));
            stringBuilder.Append(Convert.ToInt32(CanResize));
            stringBuilder.Append(Convert.ToInt32(CanApprove));
            return stringBuilder.ToString();
        }

        public bool CanCreate { get; private set; }
        public bool CanEdit { get; private set; }
        public bool CanDelete { get; private set; }
        public bool CanSend { get; private set; }
        public bool CanView { get; private set; }
        public bool CanStop { get; private set; }
        public bool CanResize { get; private set; }
        public bool CanApprove { get; private set; }

        public bool CanSomething
        {
            get
            {
                return CanCreate || CanEdit || CanDelete ||
                       CanSend || CanView || CanStop || CanResize ||
                       CanApprove;
            }
        }

        public bool CanEverything
        {
            get
            {
                return CanCreate && CanEdit && CanDelete &&
                       CanSend && CanView && CanStop && CanResize &&
                       CanApprove;
            }
        }

        public bool CheckByName(string propName)
        {
            return (bool)GetType().GetProperty(propName).GetValue(this);
        }

        public static string GetPermissionsForEnabledPolicies()
        {
            var policies = new List<string> { "'alerts'" };

            if (Config.Data.HasModule(Modules.EMAIL))
                policies.Add("'emails'");

            if (Config.Data.HasModule(Modules.SMS))
                policies.Add("'sms'");

            if (Config.Data.HasModule(Modules.SURVEYS))
                policies.Add("'surveys'");


            policies.AddRange(new[]
            {
                "'users'",
                "'groups'",
                "'templates'",
                "'text_templates'"
            });

            if (!Settings.IsTrial)
            {
                policies.Add("'ip_groups'");
            }

            if (Config.Data.HasModule(Modules.RSS))
                policies.Add("'rss'");

            if (Config.Data.HasModule(Modules.SCREENSAVER))
                policies.Add("'screensavers'");

            if (Config.Data.HasModule(Modules.WALLPAPER))
                policies.Add("'wallpapers'");

            if (Config.Data.HasModule(Modules.LOCKSCREEN))
                policies.Add("'lockscreens'");

            if (Config.Data.HasModule(Modules.EMERGENCY))
            {
                policies.Add("'im'");
                policies.Add("'cc'");
            }
            if (Config.Data.HasModule(Modules.CAMPAIGN))
                policies.Add("'campaign'");
            if (Config.Data.HasModule(Modules.TEXTTOCALL))
                policies.Add("'text_to_call'");

            policies.Add("'statistics'");

            return string.Join(",", policies.ToArray());
        }

        public static Permission Merge(Permission permissionA, Permission permissionB)
        {
            return new Permission()
            {
                CanApprove = permissionA.CanApprove || permissionB.CanApprove,
                CanCreate = permissionA.CanCreate || permissionB.CanCreate,
                CanDelete = permissionA.CanDelete || permissionB.CanDelete,
                CanEdit = permissionA.CanEdit || permissionB.CanEdit,
                CanResize = permissionA.CanResize || permissionB.CanResize,
                CanSend = permissionA.CanSend || permissionB.CanSend,
                CanStop = permissionA.CanStop || permissionB.CanStop,
                CanView = permissionA.CanView || permissionB.CanView
            };
        }
    }
}