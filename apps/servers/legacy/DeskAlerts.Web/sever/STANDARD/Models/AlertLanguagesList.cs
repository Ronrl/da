﻿using DeskAlertsDotNet.DataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace DeskAlertsDotNet.Models
{
    public class AlertLanguagesList
    {
        public Dictionary<string, AlertContent> languagesContents { get; set; } 

        public AlertContent GetContentByLanguageCode(string languageCode)
        {
            if (languagesContents.ContainsKey(languageCode))
                return languagesContents[languageCode];

            return null;
        }


        public AlertLanguagesList(string alertText, string title, DBManager dbManager)
        {
            languagesContents =  new Dictionary<string, AlertContent>();
            if (!alertText.Contains("<!-- end_lang -->"))
            {
                string defaultCode = dbManager.GetScalarByQuery<string>("SELECT abbreviatures FROM alerts_langs WHERE is_default = '1'");
                var defaultAlertContent = new AlertContent()
                {
                    Content = Regex.Replace(alertText, "<!-- html_title .*?-->", string.Empty),
                    Title = title,
                    DefaultLanguage = true
                    
                };

                languagesContents.Add(defaultCode, defaultAlertContent);
                return;
            }


            var languagesInfos = alertText.Split(new string[] { "<!-- end_lang -->" }, StringSplitOptions.None);


           var languageDetails = Regex.Matches(alertText, "<!-- begin_lang lang=\"(.*?)\" title=\"(.*?)\" is_default=\"(.*?)\" -->(.*?)<!-- end_lang -->", RegexOptions.Singleline);

            
           for(int i = 0; i < languageDetails.Count; i++)
           {
                var match = languageDetails[i];
                var langCode = match.Groups[1].Value;
                
                var isDefault = match.Groups[3].Value;
                var langContent = Regex.Replace(match.Groups[4].Value, "<!-- html_title .*?-->", string.Empty);

                var langTitle = match.Groups[2].Value;

                MatchCollection matches = Regex.Matches(langContent, "<!-- html_title *= *(['\"])(.*?)\\1 *-->");

                if (matches.Count > 0)
                {
                    int lastRegex = matches.Count - 1;
                    if (matches[lastRegex].Groups.Count > 0)
                        langTitle = matches[lastRegex].Groups[2].Value;
                }

                var alertContent = new AlertContent()
                {
                    Content = langContent,
                    Title = langTitle,
                    DefaultLanguage = isDefault == "1"
                };

                languagesContents.Add(langCode, alertContent);
            }           

        }
    }
}