﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DeskAlertsDotNet.Models
{
    public class AlertContent
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public bool DefaultLanguage { get; set; }
    }
}