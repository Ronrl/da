﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DeskAlertsDotNet.Models
{
    public class Widget
    {
        public string Id { get; private set; }
        public string Title { get; private set; }
        public string Href { get; set; }
        public int Priority { get; private set; }
        public string Parameters { get; set; }
        public WidgetPage Page { get; private set;  }


        public Widget(string id, string title, string href, int priority, string parameters, WidgetPage page)
        {
            Id = id;
            Title = title;
            Href = href;
            Priority = priority;
            Parameters = parameters;
            Page = page;

            if (Parameters.Length > 0)
                Href += ("?" + Parameters);
        }

        public Widget(string id, string title, string href, int priority) : this(id, title, href, priority, string.Empty, WidgetPage.DASHBOARD)
        {
        }
    }
}