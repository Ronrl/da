﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DeskAlertsDotNet.Models
{
    public class Domain
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public Domain()
        {

        }
        public Domain(int id, string name)
        {
            Id = id;
            Name = name;
        }

    }
}