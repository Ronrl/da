﻿namespace DeskAlertsDotNet.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using DeskAlertsDotNet.Models.Permission;

    public class User
    {
        public readonly string[] SalesManagers = { "evgen", "mila", "yuri", "alex" };

        public User(long userId, string name, string email, string phone)
        {
            Id = (int)userId;
            Name = name;
            Email = email;
            Phone = phone;
        }

        public User(int id, string name, bool isAdmin, bool isModer, bool isPublisher, Policy policy, List<Widget> widgets, string email = "", string phone = "", string startPage = "dashboard.aspx", int langId = 1)
        {
            Id = id;
            Name = name;
            IsAdmin = isAdmin;
            IsModer = isModer;
            IsPublisher = isPublisher;
            Policy = policy;
            Widgets = widgets;
            Email = email;
            Phone = phone;
            StartPage = startPage;
            LangId = langId;
            IsSalesManager = this.SalesManagers.Contains(name);
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsAdmin { get; set; }
        public bool IsSalesManager { get; set; }
        public bool IsModer { get; set; }
        public bool IsPublisher { get; set; }

        public bool IsSsoAuthorized { get; set; }
        public Policy Policy { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public List<Widget> Widgets { get; set; }
        public string StartPage { get; set; }
        public int LangId { get; set; }

        public bool HasPrivilege
        {
            get
            {
                return IsModer || IsAdmin;
            }
        }
    }
}