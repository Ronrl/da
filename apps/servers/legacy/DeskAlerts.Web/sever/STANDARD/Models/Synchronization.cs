﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DeskAlertsDotNet.Models
{
    public class Synchronization
    {
        public string id;
        public string domain;
        public string sync_id;
        public string groups_select;
        public string sync_name;
        public int port;
        public string adLogin;
        public string adPassword;
        public int db_remove;
        public string ad_select;
        public int impComp;
        public int impDis;
        public int notImpPhones;
        public string[] selectedOUs;
        public string[] added_group;
        public int auto_sync;
        public int secure;


    }
}