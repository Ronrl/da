﻿using System.Net;
using DeskAlerts.ApplicationCore.Utilities;

namespace DeskAlerts.Server.sever.STANDARD.Models
{
    public class IpRangeGroup 
    {
        public int GroupId { get; set; }
        public string FromIp { get; set; }
        public string ToIp { get; set; }

        public IpAddressRange GetIpAddressRange()
        {
            return new IpAddressRange(IPAddress.Parse(FromIp), IPAddress.Parse(ToIp));
        }
    }
}