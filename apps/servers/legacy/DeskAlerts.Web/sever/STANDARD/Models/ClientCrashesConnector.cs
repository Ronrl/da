﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DeskAlerts.Server.sever.STANDARD.Models
{
    public abstract class ClientCrashesConnector
    {
        public virtual string FileExtension { get; } = ".dmp";
        public virtual string FolderPlatformPrefix { get; } = string.Empty;

    }

    public class MacOsConnector : ClientCrashesConnector
    {
        public override string FileExtension { get; } = ".crash";
        public override string FolderPlatformPrefix { get; } = "MacOS/";
    }

    public class WindowsConnector : ClientCrashesConnector
    {
        public override string FolderPlatformPrefix { get; } = "Windows/";
    }

    public class DefaultConnector : ClientCrashesConnector { }
}