﻿using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.Server.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Web;
using System.Xml;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Licensing;
using DeskAlertsDotNet.Parameters;
using DeskAlertsDotNet.Utils.Consts;
using DeskAlertsDotNet.Utils.Factories;
using NLog;

namespace DeskAlertsDotNet.Models
{
    public enum Device
    {
        AllDevices = 3,
        Desktop = 1,
        Mobile = 2
    }

    public class Alert
    {
        private readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private const string SelectAlertTextByAlertIdQuery = @"SELECT alert_text FROM alerts WHERE id = @alertid";

        private const string GetSurvey = "/GetSurvey.aspx";

        private const string GetWallpaper = "/GetWallpaper.aspx";

        private const string GetLockscreen = "/GetWallpaper.aspx";

        private const string GetAlert = "/GetAlert.aspx";

        private const string mobileVerticalMargin = "130";

        private readonly string _alertsFolder = Config.Data.GetString("alertsDir");

        public Alert()
        {
        }

        public Alert(DataBase.DataRow rowData)
        {
            var infos = GetType().GetProperties();

            foreach (var info in infos)
            {
                var propertyName = info.Name;
                object propertyValue;

                try
                {
                    propertyValue = rowData.GetValue(propertyName.ToLower(CultureInfo.InvariantCulture));
                }
                catch (Exception ex)
                {
                    Pages.DeskAlertsBasePage.Logger.Debug(ex);
                    throw new Exception($"Failed to get property value for alert entity: propertyName=[{propertyName}] alertId=[{rowData.GetString("id")}]");
                }
                try
                {
                    var isNull = propertyValue == null || propertyValue is DBNull;

                    if (propertyValue?.GetType() == typeof(DateTime))
                    {
                        var dateTime = rowData.GetDateTime(propertyName.ToLowerInvariant());
                        info.SetValue(this, dateTime.ToString(DateTimeFormat.JavaScript, CultureInfo.InvariantCulture));
                    }
                    else if (info.PropertyType == typeof(string))
                    {
                        info.SetValue(this, isNull ? "" : rowData.GetString(propertyName.ToLowerInvariant()));
                    }
                    else if (info.PropertyType == typeof(int))
                    {
                        info.SetValue(this, isNull ? 0 : rowData.GetInt(propertyName.ToLowerInvariant()));
                    }
                    else if (info.PropertyType == typeof(bool))
                    {
                        info.SetValue(this, !isNull && rowData.GetInt(propertyName.ToLowerInvariant()) == 1);
                    }
                    else if (info.PropertyType == typeof(Device))
                    {
                        info.SetValue(this, isNull ? 0 : rowData.GetInt(propertyName.ToLowerInvariant()));
                    }
                }
                catch (Exception ex)
                {
                    Pages.DeskAlertsBasePage.Logger.Debug(ex);
                    throw new Exception("Failed to add property " + propertyName + " value = " + propertyValue + "id=" + rowData.GetString("id"));
                }
            }
        }

        public Alert(AlertEntity alert)
        {
            Id = (int) alert.Id;
            Title = alert.Title;
            Class = alert.Class;
            Urgent = alert.Urgent;
            Fullscreen = alert.Fullscreen;
            SenderId = alert.SenderId;
            Ticker = alert.Ticker;
            TickerSpeed = alert.TickerSpeed;
            Aknown = alert.Aknown;
            Width = alert.Width;
            Height = alert.Height;
            Resizable = alert.Resizable ? 1: 0;
            Autoclose = int.Parse(alert.Autoclose);
            CreateDate = alert.CreateDate.ToString(CultureInfo.InvariantCulture);
            Schedule = alert.Schedule;
            Schedule_Type = alert.ScheduleType;
            Email = alert.Email == 1;
            Sms = alert.Sms == 1;
            TextToCall = alert.TextToCall == 1;
            FromDate = alert.FromDate.ToString();
            ToDate = alert.ToDate.ToString();
            SelfDeletable = alert.SelfDeletable ? 1 : 0;
            CaptionId = alert.CaptionId.ToString();
            Device = (Device) alert.Device;
            Lifetime = alert.Lifetime;
            Approve_Status = alert.ApproveStatus;
            CampaignId = (int) alert.CampaignId;
            Type2 = alert.Type2;
        }

        public int Id { get; set; }

        public string Title { get; set; }

        public int Class { get; set; }

        public int Urgent { get; set; }

        public string Fullscreen { get; set; }

        public string SentDate { get; set; }

        public long SenderId { get; set; }

        public int Ticker { get; set; }

        public int TickerSpeed { get; set; }

        public int Aknown { get; set; }

        public int Width { get; set; }

        public int Height { get; set; }

        public int Resizable { get; set; }

        public int Autoclose { get; set; }

        public string CreateDate { get; set; }

        public int Schedule { get; set; }

        /// <summary>
        /// 1 - content can be sent, 0 - stopped
        /// </summary>
        public int Schedule_Type { get; set; } = 1;

        public bool Email { get; set; }

        public bool Sms { get; set; }
        public bool TextToCall { get; set; }

        public string FromDate { get; set; }

        public string ToDate { get; set; }

        public int SelfDeletable { get; set; }

        public string CaptionId { get; set; }

        public Device Device { get; set; }

        public int Lifetime { get; set; }

        public int SurveyId { get; set; }
        public int Approve_Status { get; set; }

        public int CampaignId { get; set; }
        //B - Broadcast, R - Recipients, I - Ipgroups
        public string Type2 { get; set; }

        private bool IsSurvey()
        {
            return
                Class == (int)AlertType.SimpleSurvey ||
                Class == (int)AlertType.SurveyQuiz ||
                Class == (int)AlertType.SurveyPoll;
        }

        public string GetContentUrl(string deskbar, string username, string domainName, DateTime localtime, long userId, bool isMobile)
        {
            var fileName = string.Empty;
            var parameters = string.Empty;
            var encodeUsername = HttpUtility.UrlEncode(username);
            
            if (IsSurvey())
            {
                fileName = GetSurvey;
                parameters = $"?id={SurveyId}&amp;user_name={encodeUsername}&amp;deskbar_id={deskbar}&amp;domain_name={domainName}";
            }
            else if (Class == (int)AlertType.Wallpaper)
            {
                fileName = GetWallpaper;
                parameters = $"?id={Id}";
            }
            else if (Class == (int)AlertType.LockScreen)
            {
                fileName = GetLockscreen;
                parameters = $"?id={Id}";
            }
            else
            {
                fileName = GetAlert;
                parameters =
                    $"?id={Id}&amp;user_id={userId}&amp;deskbar_id={deskbar}&amp;localtime={localtime.ToString("dd.MM.yyyy|HH:mm:ss")}&amp;isMobile={(isMobile ? "1" : "0")}";
            }

            return _alertsFolder + fileName + parameters;
        }

        public static List<Alert> ParseDataSet(DataBase.DataSet data)
        {
            List<Alert> alerts = new List<Alert>();

            foreach (DataBase.DataRow alertRow in data)
            {
                alerts.Add(new Alert(alertRow));
            }

            return alerts;
        }


        /*
         * <ALERT urgent="1" alert_id="25829" acknown="1" autoclose="0" create_date="1463193920"
         * title="hhh" position="center" visible="" width="500" height="400" ticker="0" ticker_position=""
         * schedule="1" recurrence="0" history="1" class_id="1" resizable="0" docked="0" self_deletable="0"
         * to_date="1463302101" 
         * id="alert" href="http://demo.deskalerts.com/get_alert.asp?id=25829&amp;user_id=747&amp;deskbar_id={D64334E7-83AC-4ac4-92C7-7DF2682A3292}"
         * timeout="-1" expire="" priority="1" plugin="alert:ID3" 
         * user_id="747" survey="0" utc="1" />

         */


        public string[] GetPositions()
        {
            string[] result = { "", "", "" };
            string[] positions = { "", "fullscreen", "left-top", "right-top", "center", "left-bottom", "right-bottom", "top", "middle", "bottom" };
            int fullscreen;

            if (int.TryParse(this.Fullscreen, out fullscreen))
            {
                int index = 0;
                if (fullscreen >= 7)
                    index = 1;

                result[index] = positions[fullscreen];
            }
            else
            {
                if (Fullscreen == "D")
                    result[2] = "1";
            }

            return result;
        }

        public bool IsStartTime()
        {
            DateTime fromDateTime = DeskAlertsUtils.DateTimeFromString(this.FromDate);

            bool result = DateTime.Now.Hour > fromDateTime.Hour && DateTime.Now.Minute > fromDateTime.Minute;
            return result;
        }

        public bool IsExpired(DateTime time)
        {
            DateTime toDateTime = DeskAlertsUtils.DateTimeFromString(this.ToDate);
            bool result = time.CompareTo(toDateTime) > 0 && toDateTime.Year != 1900;
            return result;
        }

        public bool IsScheduled()
        {
            return Schedule == 1;
        }

        public bool IsScheduledAndStarted()
        {
            if (!IsScheduled())
            {
                return false;
            }

            var fromDateTime = DeskAlertsUtils.DateTimeFromString(this.FromDate);

            var result = DateTime.Now.Hour >= fromDateTime.Hour && DateTime.Now.Minute >= fromDateTime.Minute;
            return result;
        }

        public bool IsStarted(DateTime time)
        {
            var fromDateTime = DeskAlertsUtils.DateTimeFromString(FromDate);
            var result = time.CompareTo(fromDateTime) > 0 && Schedule_Type != 0;
            return result;
        }

        public string ToXmlString(string clientType, string deskbarId, string username, long userId, DateTime localtime, bool isMobile = false)
        {
            if (LicenseManager.IsLocked && !Config.Data.IsDemo)
            {
                return "LOCKED IsLicenceLocked: " + LicenseManager.IsLicenceLocked + "; IsTrialLocked: " + LicenseManager.IsTrialLocked + "; IsLicenceDateExpired: " + LicenseManager.IsLicenceDateExpired;
            }

            var createdDateTime = DeskAlertsUtils.DateTimeFromString(CreateDate);
            var createdTimeStamp = DeskAlertsUtils.DateTimeToTimeStamp(createdDateTime);
            var toDateTime = DeskAlertsUtils.DateTimeFromString(ToDate);
            var toDateTimeStamp = DeskAlertsUtils.DateTimeToTimeStamp(toDateTime);
            var docked = 0;
            var positions = GetPositions();
            var position = Class == 8 ? Fullscreen : positions[0];
            var tickerPosition = positions[1];

            if (positions[2] == "1")
            {
                docked = 1;
            }

            var domainName = string.Empty;
            using (var dbManager = new DBManager())
            {
                const string sql = "SELECT domains.name FROM domains JOIN users ON users.domain_id = domains.id WHERE users.id = @UserId";
                var userIdSqlParam = SqlParameterFactory.Create(DbType.Int32, userId, "UserId");
                domainName = dbManager.GetScalarByQuery<string>(sql, userIdSqlParam);
            }

            var contentUrl = GetContentUrl(deskbarId, username, domainName, localtime, userId, isMobile);
            var windowXml = string.Empty;

            {
                var caption = CaptionId.ToLower();

                if (caption != "default")
                {
                    caption = "{" + caption + "}";
                }

                string captionFile;
                string jsFile;
                var customCss = string.Empty;

                if (!Config.Data.IsOptionEnabled("ConfUseLocalSkins") || isMobile)
                {
                    if (Ticker == 1 && !isMobile)
                    {
                        captionFile = _alertsFolder + "/admin/skins/" + caption + "/version/tickercaption.html";
                        jsFile = _alertsFolder + "/admin/skins/" + caption + "/version/ticker.js";
                    }
                    else
                    {
                        captionFile = _alertsFolder + "/admin/skins/" + caption + "/version/alertcaption.html";
                        jsFile = _alertsFolder + "/admin/skins/" + caption + "/version/alert.js";

                    }
                }
                else
                {
                    if (Ticker == 1)
                    {
                        captionFile = "/skins/" + caption + "/version/tickercaption.html";
                        jsFile = "/skins/" + caption + "/version/ticker.js";
                    }
                    else
                    {
                        captionFile = "/skins/" + caption + "/version/alertcaption.html";
                        jsFile = "/skins/" + caption + "/version/alert.js";
                    }

                    customCss = "/skins/" + caption + "/version/skin.css";
                }

                windowXml = "><WINDOW skin_id=\"" + caption + "\"";

                var confFilePath = $"{Config.Data.GetString("alertsFolder")}admin/skins/{caption}/blank/conf.xml";
                var xmlDoc = new XmlDocument();

                xmlDoc.Load(confFilePath);

                var nodeList = xmlDoc.SelectNodes("/ALERTS/COMMANDS/ALERT/WINDOW");
                var alertWindowName = "alertwindow1";
                var tickerWindowName = "tickerwindow1";

                if (nodeList != null)
                {
                    foreach (XmlNode window in nodeList)
                    {
                        var processedInnerText = window.InnerText.Replace(" ", "").Replace("\"", "").Replace("'", "").ToLower();

                        switch (processedInnerText)
                        {
                            case "#ticker#==0":
                                if (window.Attributes != null)
                                {
                                    alertWindowName = window.Attributes["name"].Value;
                                }

                                break;
                            case "#ticker#==1":
                                if (window.Attributes != null)
                                {
                                    tickerWindowName = window.Attributes["name"].Value;
                                }

                                break;
                            default:
                                // TODO: Move conf.xml from win-client to server.
                                // Server unknow <WINDOW name="tickerwindow2">#ticker_position# == 'middle'</WINDOW> property.
                                // To use one configuration file in client and server, exclude exception and generate log.
                                Logger.Log(LogLevel.Trace, $"Unexpected window name: {processedInnerText}");
                                continue;
                        }
                    }
                }

                var alertWindow = Ticker == 1 ? xmlDoc.SelectSingleNode("/ALERTS/VIEWS/WINDOW[@name='" + tickerWindowName + "']") : xmlDoc.SelectSingleNode("/ALERTS/VIEWS/WINDOW[@name='" + alertWindowName + "']");

                if (alertWindow?.Attributes != null)
                {
                    var leftMargin = alertWindow.Attributes["leftmargin"].Value;
                    var rightMargin = alertWindow.Attributes["rightmargin"].Value;
                    var topMargin = alertWindow.Attributes["topmargin"].Value;
                    var bottomMargin = alertWindow.Attributes["bottommargin"].Value;

                    if (isMobile)
                    {
                        topMargin = bottomMargin = mobileVerticalMargin;
                    }

                    captionFile = captionFile.Replace("\\", "/");
                    jsFile = jsFile.Replace("\\", "/");

                    windowXml += " leftmargin=\"" + leftMargin + "\" rightmargin=\"" + rightMargin + "\" topmargin=\"" + topMargin + "\"" +
                                 " bottommargin=\"" + bottomMargin + "\" ";
                }

                if (alertWindow?.Attributes != null && Ticker == 0 && alertWindow.Attributes["transparency"] != null)
                {
                    var transparency = alertWindow.Attributes["transparency"].Value;

                    windowXml += " transparency=\"" + transparency + "\" ";
                }

                windowXml += "captionhref=\"" + captionFile + "\" customjs=\"" + jsFile + "\" ";

                if (customCss.Length > 0)
                {
                    windowXml += "customcss=\"" + customCss + "\"";
                }

                windowXml += "/></ALERT>";

            }

            var endLine = windowXml.Length > 0 ? windowXml : "/>";
            var shouldHideCloseButton = false;

            if (Autoclose == 2147482647)
            {
                shouldHideCloseButton = IsSurvey();
            }

            var xml = "<ALERT id=\"alert\" href=\"" + contentUrl + "\" plugin=\"alert:ID3\" alert_id=\"" + Id + "\" acknown=\"" + Aknown + "\" " +
                " autoclose=\"" + (Autoclose == 2147482647 ? 0 : Autoclose) + "\" title=\"" + System.Security.SecurityElement.Escape(Title) + "\" ";

            if (Ticker != 1)
            {
                xml += "width " + "=\"" + Width + "\" height=\"" + Height + "\"";
            }

            if (shouldHideCloseButton)
            {
                xml += " hide_close=\"1\"";
            }

            if (IsSurvey())
            {
                xml += " survey=\"1\"";
            }

            xml += " ticker= \"" + Convert.ToInt32(Ticker) + "\" tickerSpeed=\"" + TickerSpeed + "\" schedule=\"" + Schedule + "\" resizable=\"" + Resizable + "\"" +
            " visible=\"\" history=\"1\" utc=\"1\" expire=\"\" priority=\"1\" create_date=\"" + createdTimeStamp + "\"" +
            " to_date=\"" + toDateTimeStamp + "\" position=\"" + position + "\" ticker_position=\"" + tickerPosition + "\"" +
            " docked=\"" + docked + "\" self_deletable=\"" + SelfDeletable + "\" class_id=\"" + Class + "\" urgent=\"" + Urgent + "\"" + " user_id=\"" + userId + "\"" + endLine;

            return xml;
        }

        public string GetAlertText()
        {
            using (DBManager manager = new DBManager())
            {
                SqlParameter alertIdParameter = SqlParameterFactory.Create(DbType.Int32, Id, "alertid");
                DataBase.DataSet dataSet = manager.GetDataByQuery(SelectAlertTextByAlertIdQuery, alertIdParameter);
                DataBase.DataRow dataRow = dataSet.GetRow(0);
                string alertText = dataRow.GetString("alert_text");
                alertText = DeskAlertsUtils.EscapeXmlComments(alertText);
                return alertText;
            }
        }

        public string GetClearAlertText()
        {
            using (DBManager manager = new DBManager())
            {
                SqlParameter alertIdParameter = SqlParameterFactory.Create(DbType.Int32, Id, "alertid");
                DataBase.DataSet dataSet = manager.GetDataByQuery(SelectAlertTextByAlertIdQuery, alertIdParameter);
                DataBase.DataRow dataRow = dataSet.GetRow(0);
                string alertText = dataRow.GetString("alert_text");
                alertText = DeskAlertsUtils.ExtractTextFromHtml(alertText);
                return alertText;
            }
        }
    }
}