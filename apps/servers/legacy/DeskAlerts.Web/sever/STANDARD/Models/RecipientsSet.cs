﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DeskAlertsDotNet.Models
{
    public class RecipientsSet
    {
        public List<int> Users { get; private set; }
        public List<int> Groups { get; private set; }
        public List<int> OUs { get; private set; }
        public List<int> Computers { get; private set; }

        public RecipientsSet()
        {
            Users = new List<int>();
            Groups = new List<int>();
            OUs = new List<int>();
            Computers = new List<int>();
        }
        public string UsersIds
        {
            get { return string.Join(",", Users.ToArray()); }
        }

        public string OUsIds
        {
            get { return string.Join(",", OUs.ToArray()); }
        }

        public string GroupsIds
        {
            get { return string.Join(",", Groups.ToArray()); }
        }

        public string ComputerssIds
        {
            get { return string.Join(",", Computers.ToArray()); }
        }
    }
}