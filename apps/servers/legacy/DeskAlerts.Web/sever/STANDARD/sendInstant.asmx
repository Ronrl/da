﻿<%@ WebService Language="C#" CodeBehind="sendInstant.asmx.cs" Class="SendInstant.sendInstant" %>

using System;
using System.Collections.Generic;
//using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Serialization;
using System.ComponentModel;
using System.Security.Cryptography;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.IO;

namespace SendInstant
{
    /// <summary>
    /// Summary description for getEasyvista
    /// </summary>
    [WebService(Namespace = "")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    [SoapDocumentService(RoutingStyle = SoapServiceRoutingStyle.RequestElement)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    //[System.Web.Script.Services.ScriptService]
    public class sendInstant : System.Web.Services.WebService
    {
        string url_edit;
              
        public sendInstant()
        {
            string url = HttpContext.Current.Request.Url.AbsoluteUri;
            string path = url.Substring(0, url.IndexOf("sendInstant"));
            url_edit = path + "admin/edit_alert_db.asp";
        }

        private string sendRequest(string value, string url)
        {
            WebRequest request = WebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            byte[] byteArray = Encoding.UTF8.GetBytes(value);
            request.ContentLength = byteArray.Length;


            Stream dataStream = request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();

            WebResponse api_response = request.GetResponse();
            dataStream = api_response.GetResponseStream();

            StreamReader reader_response = new StreamReader(dataStream);
            string responseFromServer = reader_response.ReadToEnd();

            reader_response.Close();
            dataStream.Close();
            api_response.Close();
            return responseFromServer;
        }

        [WebMethod]
        [SoapDocumentMethod(RequestElementName = "sendAlert",
    ResponseElementName = "response")]
        public string sendAlert(string alertid)
        {
            string body = "autosubmit=1&id=" + alertid;
            string result = sendRequest(body, url_edit);

            return "=" + alertid + "=";
        }

    }
}