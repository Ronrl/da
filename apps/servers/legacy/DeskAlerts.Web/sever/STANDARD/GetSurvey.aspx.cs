using DeskAlerts.Server.Utils;
using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using DeskAlerts.ApplicationCore.Encryption;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Parameters;

using HtmlAgilityPack;
using NLog;
using System.Web;
using DeskAlertsDotNet.Utils.Factories;

namespace DeskAlertsDotNet.Pages
{
    public partial class GetSurvey : System.Web.UI.Page
    {
        private Logger _logger = LogManager.GetCurrentClassLogger();
        private const int PreviewUserId = 0;
        private const string TemplateTstring =
            "<tr>" +
                "<td colspan='2'>" +
                    "<div align='center' style='width:100%; overflow:hidden; word-wrap: break-word;'>" +
                        "{0}" +
                    "</div>" +
                "</td>" +
            "</tr>" +
            "<tr>" +
                "<td colspan='2'>" +
                    "<div align='center'>" +
                        "<input type='text' name='custom_answers' style='width:300px; height:35px'/>" +
                    "</div>" +
                "</td>" +
            "</tr>";

        private const string TemplateCstring =
            "<tr>" +
                "<td>" +
                    "<input id='answer_finish' name='answer' type=\"radio\" value='{1}'/>" +
                "</td>" +
                "<td>" +
                    "{0}" +
                "</td>" +
            "</tr>" +
            "<tr>" +
                "<td>" +
                    "<input id='answer' name='answer' type=\"radio\" value='{3}'/>" +
                "</td>" +
                "<td>" +
                    "{2}" +
                "</td>" +
            "</tr>";

        private const string TemplateMstring =
            "<tr>" +
                "<td>" +
                    "<input name='answer' type=\"radio\" value='{1}'/>" +
                "</td>" +
                "<td>" +
                    "{0}" +
                "</td>" +
            "</tr>";

        private const string TemplateNstring =
            "<tr>" +
                "<td>" +
                    "<input type=\"checkbox\" name='answer' value='{1}'/>" +
                "</td>" +
                "<td>" +
                    "{0}" +
                "</td>" +
            "</tr>";

        private static readonly string EncryptKey = Config.Data.GetString("ENCRYPT_KEY");

        private DBManager _dataBaseManager;

        protected override void OnLoad(EventArgs e)
        {
            _dataBaseManager = new DBManager();
            var stringBuilder = new StringBuilder();
            base.OnLoad(e);
            Response.Expires = 0;
            string localtime = Request["localtime"];
            int userId;
            string deskbarId = Request["deskbar_id"];
            string domain_name = Request["domain_name"];


            var userName = HttpUtility.UrlDecode(Request["user_name"]);
            if (string.IsNullOrEmpty(Request["user_id"]) && !string.IsNullOrEmpty(userName))
            {
                var selectUserNameParam = SqlParameterFactory.Create(DbType.String, userName, "userName");
                var domainNameSqlParameter = SqlParameterFactory.Create(DbType.String, domain_name, "domainName");
                const string getUserByNameQuery = 
                    "SELECT users.id FROM users JOIN domains ON users.domain_id = domains.id WHERE domains.name = @domainName AND users.name = @userName AND role = 'U';";
                userId = _dataBaseManager.GetScalarQuery<int>(getUserByNameQuery, selectUserNameParam, domainNameSqlParameter);
            }
            else
            {
                userId = Convert.ToInt32(Request["user_id"]);
            }

            var surveyId = Request.GetParam("id", -1);
            if (surveyId == -1)
            {
                Response.End();
                return;
            }
            string getSurveyQuery =
                $"SELECT name, template_id, create_date, stype, sender_id FROM surveys_main WHERE id={surveyId}";
            var surveyRow = _dataBaseManager.GetDataByQuery(getSurveyQuery).First();

            int alertId = 0;
            if (surveyRow != null)
            {
                alertId = surveyRow.GetInt("sender_id");
            }

            if (surveyRow == null || IsUserAlereadyVoted(userId, alertId))
            {
                stringBuilder.Append("<body bgcolor=\"white\">");
                stringBuilder.Append(
                    "<center>Your survey was submitted.<br><br><br><img src=\"admin/images/langs/en/close_button.gif\" alt=\"Close\" border=\"0\" onclick=\"window.external.close();\"></center>");
                stringBuilder.Append("</body>");
                Response.Write(stringBuilder.ToString());
                Response.End();
            }

            var resultHtml =
                   "<html>\r\n<head>\r\n<meta http-equiv='content-type' content='text/html; charset=utf-8' />\r\n<script language='javascript' type='text/javascript'>\r\n</script>\r\n" +
                   (Request.Params["preview"] == "1" ? "<link href='admin/css/style.css' rel='stylesheet' type='text/css'/>\r\n" : "") +
                   "</head>\r\n<body bgcolor='white' style='margin: 5px; overflow: auto;'>\r\n</body>\r\n</html>";
            var htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml(resultHtml);

            string getQuestionQuery =
                $"SELECT id, question, question_type FROM surveys_questions WHERE survey_id={surveyId} ORDER by question_number";
            var questionsDataSet = _dataBaseManager.GetDataByQuery(getQuestionQuery);
            var currentQuestionIndex = 1;
            var scriptAdded = false;

            foreach (var questionRow in questionsDataSet)
            {
                var question = DeskAlertsUtils.AddAttributeToTag(questionRow.GetString("question"), "a", "target", "_blank");
                string questionId = questionRow.GetString("id");
                var curentQuestion = currentQuestionIndex;
                var questionType = questionRow.GetString("question_type");
                var templateName = questionType == "C" ? "M" : questionType;
                var templateHtml = File.ReadAllText(Server.MapPath($"admin/templates/{templateName}.html"));
                var answerSet = _dataBaseManager.GetDataByQuery(
                    $"SELECT id, answer FROM surveys_answers WHERE question_id={questionId}");
                var answersString = string.Empty;

                if (questionType == "C")
                {
                    var answer1 = DeskAlertsUtils.AddAttributeToTag(answerSet.GetString(0, "answer"), "a", "target", "_blank");
                    var answer2 = DeskAlertsUtils.AddAttributeToTag(answerSet.GetString(1, "answer"), "a", "target", "_blank");

                    var template = TemplateCstring;
                    if (surveyRow.GetInt("stype") == 3)
                    {
                        template = TemplateCstring.Replace("answer_finish", "answer_redirect");
                    }

                    answersString += string.Format(
                        template,
                        answer1,
                        answerSet.GetString(0, "id"),
                        answer2,
                        answerSet.GetString(1, "id"));
                }
                else
                {
                    foreach (var answer in answerSet)
                    {
                        var answerText = DeskAlertsUtils.AddAttributeToTag(
                            answer.GetString("answer").Replace("<p>", string.Empty).Replace("</p>", string.Empty),
                            "a",
                            "target",
                            "_blank");
                        var idText = answer.GetString("id");
                        switch (questionType)
                        {
                            case "M":
                                {
                                    answersString += string.Format(TemplateMstring, answerText, idText);
                                    break;
                                }

                            case "N":
                                {
                                    answersString += string.Format(TemplateNstring, answerText, idText);
                                    break;
                                }

                            case "T":
                                {
                                    answersString += string.Format(TemplateTstring, answerText);
                                    break;
                                }
                        }
                    }
                }

                var questionHtml = templateHtml.Replace("{ANSWERS}", answersString)
                    .Replace("{TITLE}", question)
                    .Replace("{NUMBER}", curentQuestion.ToString())
                    .Replace("{END}", "0")
                    .Replace("{USER_ID}", userId.ToString())
                    .Replace("{DESKBAR}", Request["deskbar_id"])
                    .Replace("{QUESTION_ID}", questionRow.GetString("id"))
                    .Replace("{SURVEY_ID}", surveyId.ToString())
                    .Replace("{serverUrl}", Config.Data.GetString("alertsDir"))
                    .Replace("id='form'", "id='form_q" + currentQuestionIndex + "'")
                    .Replace("id='answers_table'", "id='answers_table_q" + currentQuestionIndex + "'")
                    .Replace("id='custom_answer'", "id='custom_answer_q" + currentQuestionIndex + "'")
                    .Replace("id='custom_answer_count'", "id='custom_answer_count_q" + currentQuestionIndex + "'")
                    .Replace("oninput='onChangeCustomAnswer()'", "oninput='onChangeCustomAnswer(" + "\"custom_answer_q" + currentQuestionIndex + "\", \"custom_answer_count_q" + currentQuestionIndex + "\")'");

                string nextAction;

                if (currentQuestionIndex == questionsDataSet.Count && surveyRow.GetInt("stype") != 3)
                {
                    nextAction = "finish";
                }
                else if (currentQuestionIndex == questionsDataSet.Count)
                {
                    nextAction = "redirect";
                }
                else
                {
                    nextAction = $"question_{(currentQuestionIndex + 1)}";
                }

                if (questionType == "I")
                {
                    questionHtml = questionHtml.Replace("onclick='return checkSubmit();'", $"onclick=\'return checkSubmit(\"question_{currentQuestionIndex}\", \"{nextAction}\", \"form_q{currentQuestionIndex}\", \"answers_table_q{currentQuestionIndex}\", \"custom_answer_q{currentQuestionIndex}\");\'");
                }
                else if (questionType == "S")
                {
                    questionHtml = questionHtml.Replace("onclick='return checkSubmit();'", $"onclick=\'return checkSubmit(\"question_{currentQuestionIndex}\", \"{nextAction}\", \"form_q{currentQuestionIndex}\");\'");
                }
                else
                {
                    questionHtml = questionHtml.Replace("onclick='return checkSubmit();'", $"onclick=\'return checkSubmit(\"question_{currentQuestionIndex}\", \"{nextAction}\", \"form_q{currentQuestionIndex}\", \"answers_table_q{currentQuestionIndex}\");\'");
                }

                var templateDocument = new HtmlDocument();
                templateDocument.LoadHtml(questionHtml);

                if (!scriptAdded)
                {
                    var scriptSrc = File.ReadAllText(Server.MapPath("admin/templates/script.js"));
                    var scriptNode = htmlDocument.DocumentNode.SelectSingleNode("//script");
                    scriptSrc = scriptSrc.Replace("xhr.open('POST', '/Vote.aspx');", "xhr.open('POST', '" + string.Format("{0}/Vote.aspx", Config.Data.GetString("alertsDir")) + "');");
                    scriptSrc = scriptSrc.Replace("window.location.replace('')", $"window.location.replace('{string.Format("{0}/admin/SurveyAnswersStatistic.aspx?client=1&id=" + surveyId + "&preview=" + Request.GetParam("preview", "0") + "')", Config.Data.GetString("alertsDir"))}");
                    scriptNode.InnerHtml += scriptSrc;
                    scriptAdded = true;
                }

                var bodyNode = htmlDocument.DocumentNode.SelectSingleNode("//body");
                var newContent = $"<div id='question_{currentQuestionIndex}'>\r\n{questionHtml}</div>\r\n";
                if (currentQuestionIndex > 1)
                {
                    newContent =
                        $"<div id='question_{currentQuestionIndex}' style='display: none'>\r\n{questionHtml}</div>\r\n";
                }

                var newNode = HtmlNode.CreateNode(newContent);
                bodyNode.AppendChild(newNode);

                currentQuestionIndex++;

                if (currentQuestionIndex > questionsDataSet.Count && surveyRow.GetInt("stype") != 3)
                {
                    string finishButton = Request.GetParam("preview", "0") != "1"
                                              ? "window.external.close()"
                                              : "window.parent.close()";
                    var finalContent = "<center>Your survey was submitted.<br><br><br><img src='admin/images/langs/en/close_button.gif' alt='Close' border='0' onclick=\"" + finishButton + "\"></center>";
                    bodyNode = htmlDocument.DocumentNode.SelectSingleNode("//body");
                    newContent = $"<div id='finish' style='display: none'>\r\n{finalContent}</div>\r\n";
                    newNode = HtmlNode.CreateNode(newContent);
                    bodyNode.AppendChild(newNode);
                }
            }

            string alertText = GetAlertTextById(alertId);
            alertText = FormatTitle(alertText);
            HtmlNode htmlnode = HtmlNode.CreateNode(alertText);
            htmlDocument.DocumentNode.SelectSingleNode("//body").AppendChild(htmlnode);

            resultHtml = htmlDocument.DocumentNode.InnerHtml;

            Response.Clear();

            if (Config.Data.HasModule(Modules.ENCRYPT) && Request.GetParam("preview", "0") != "1")
            {
                Response.Write(Aes256Encryptor.EncryptData(resultHtml, EncryptKey));
            }
            else
            {
                Response.BinaryWrite(Encoding.Default.GetPreamble());
                stringBuilder.Append(resultHtml);
                Response.Write(stringBuilder.ToString());
            }

            _dataBaseManager.Dispose();
        }

        /// <summary>
        /// Get alert_text comment (survey title) from database.
        /// </summary>
        /// <param name="id">Alert id.</param>
        /// <returns>Html comment string.</returns>
        private string GetAlertTextById(int id)
        {
            try
            {
                string result;

                if (id <= 0)
                {
                    throw new ArgumentNullException(nameof(id));
                }

                const string AlertTextQuery = "SELECT alert_text FROM alerts WHERE id = @id";

                var alertIdParameter = SqlParameterFactory.Create(DbType.Int64, id, "id");
                var queryResult = _dataBaseManager.GetDataByQuery(AlertTextQuery, alertIdParameter).ToList();

                if (queryResult == null)
                {
                    throw new NullReferenceException(nameof(queryResult));
                }
                else
                {
                    result = queryResult.First().GetValue("alert_text").ToString();
                }

                return result;
            }
            catch (Exception ex)
            {
                _logger.Debug(ex);
                throw;
            }
        }

        /// <summary>
        /// Formatting title for display in the client application.
        /// </summary>
        /// <param name="titleText">Title text from database.</param>
        /// <returns>Formatted text.</returns>
        public string FormatTitle(string titleText)
        {
            try
            {
                if (string.IsNullOrEmpty(titleText))
                {
                    throw new ArgumentNullException(nameof(titleText));
                }

                string result = titleText;

                result = result.Replace($"<!-- html_title=\"", "<!-- html_title = \'");
                result = result.Replace("\" -->", "\' -->");

                return result;
            }
            catch (Exception ex)
            {
                _logger.Debug(ex);
                throw;
            }
        }

        private bool IsUserAlereadyVoted(int userId, int alertId)
        {
            if (userId != PreviewUserId)
            {
                string query = $@"SELECT DISTINCT count (users.id)
                FROM users
                INNER JOIN alerts_received ON users.id = alerts_received.user_id
                WHERE role = 'U' AND vote = 1 AND alerts_received.alert_id = {alertId} and users.id={userId}";
                int queryResult = _dataBaseManager.GetScalarByQuery<int>(query);

                bool result = queryResult > 0;
                return result;
            }

            return false;
        }
    }
}