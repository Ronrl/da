﻿using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.Server.Utils.Interfaces;
using DeskAlertsDotNet.Application;
using DeskAlertsDotNet.sever.MODULES.INSTANT_MESSAGES;

namespace DeskAlertsDotNet.Server.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;

    using DeskAlerts.ApplicationCore.Interfaces;
    using DeskAlerts.ApplicationCore.Repositories;
    using DeskAlerts.ApplicationCore.Utilities;

    using DataBase;
    using Parameters;
    using Utils.Consts;
    using Utils.Factories;
    using Models;
    using Pages;

    using DataRow = DataBase.DataRow;
    using DataSet = DataBase.DataSet;
    using System.Globalization;
    using Managers;
    using DeskAlerts.Server.Utils.Managers;

    using NLog;
    using DeskAlerts.Server.Utils.Interfactes;
    using DeskAlertsDotNet.Utils.Services;

    public class ContentSender : IContentSender
    {
        private readonly IConfigurationManager _configurationManager;
        private readonly IUserService _userService;
        private readonly ISkinService _skinService;

        public ContentSender()
        {
            IConfigurationManager configurationManager = new WebConfigConfigurationManager(Config.Configuration);
            IUserRepository userRepository = new PpUserRepository(configurationManager);
            IGroupRepository groupRepository = new PpGroupRepository(configurationManager);
            _userService = new UserService(userRepository, groupRepository);
            _skinService = new SkinService();

        }
        private const string SelectLifeTimeQuery = 
            @"SELECT alt.from_date,
                alt.to_date
            FROM [alerts] AS alt
            WHERE alt.id = @alertId;";

        private const string DuplicateAlertQuery =
            @"INSERT INTO alerts(
                alert_text, title, type, group_type, create_date, sent_date, type2, from_date,
                to_date, schedule, schedule_type, recurrence, urgent, toolbarmode, deskalertsmode,
                sender_id, template_id, group_id, aknown, ticker, fullscreen, ticker_speed,
                alert_width, alert_height, email_sender, email, sms, desktop, escalate, escalate_hours,
                escalate_count, escalate_step, escalate_hours_initial, autoclose, param_temp_id,
                caption_id, class, parent_id, resizable, post_to_blog, social_media, self_deletable,
                text_to_call, campaign_id, video, approve_status, reject_reason, lifetime, terminated,
                color_code, rss_allow_content, device)
            SELECT
                alert_text, title, type, group_type, GETDATE(), GETDATE(), type2, GETDATE(),
                @toDate, schedule, schedule_type, recurrence, urgent, toolbarmode, deskalertsmode,
                @userId, template_id, group_id, aknown, ticker, fullscreen, ticker_speed,
                alert_width, alert_height, email_sender, email, sms, desktop, escalate, escalate_hours,
                escalate_count, escalate_step, escalate_hours_initial, autoclose, param_temp_id,
                @skinIdParam, class, parent_id, resizable, post_to_blog, social_media, self_deletable,
                text_to_call, campaign_id, video, approve_status, reject_reason, lifetime, terminated,
                color_code, rss_allow_content, device
            FROM alerts
            WHERE id = @alertId
            SELECT SCOPE_IDENTITY();";

        private const string DuplicateAlertQueryWithNullCaptionId =
            @"INSERT INTO alerts(
                alert_text, title, type, group_type, create_date, sent_date, type2, from_date,
                to_date, schedule, schedule_type, recurrence, urgent, toolbarmode, deskalertsmode,
                sender_id, template_id, group_id, aknown, ticker, fullscreen, ticker_speed,
                alert_width, alert_height, email_sender, email, sms, desktop, escalate, escalate_hours,
                escalate_count, escalate_step, escalate_hours_initial, autoclose, param_temp_id,
                class, parent_id, resizable, post_to_blog, social_media, self_deletable,
                text_to_call, campaign_id, video, approve_status, reject_reason, lifetime, terminated,
                color_code, rss_allow_content, device)
            SELECT
                alert_text, title, type, group_type, GETDATE(), GETDATE(), type2, GETDATE(),
                @toDate, schedule, schedule_type, recurrence, urgent, toolbarmode, deskalertsmode,
                @userId, template_id, group_id, aknown, ticker, fullscreen, ticker_speed,
                alert_width, alert_height, email_sender, email, sms, desktop, escalate, escalate_hours,
                escalate_count, escalate_step, escalate_hours_initial, autoclose, param_temp_id,
                class, parent_id, resizable, post_to_blog, social_media, self_deletable,
                text_to_call, campaign_id, video, approve_status, reject_reason, lifetime, terminated,
                color_code, rss_allow_content, device
            FROM alerts
            WHERE id = @alertId
            SELECT SCOPE_IDENTITY();";

        private readonly CacheManager _cacheManager = Global.CacheManager;

        private readonly DBManager _databaseManager = new DBManager();

        private readonly bool _isEmailModuleEnabled = Config.Data.HasModule(Modules.EMAIL);
        private readonly bool _isSmsModuleEnabled = Config.Data.HasModule(Modules.SMS);
        private readonly bool _isTextToCallModuleEnabled = Config.Data.HasModule(Modules.TEXTTOCALL);
        private readonly User _currentUser = UserManager.Default.CurrentUser;

        public bool AddRecipientToStatistic(List<int> usersId, int alertId)
        {
            try
            {
                var query = string.Empty;
                foreach (var userId in usersId)
                {
                    query += "INSERT INTO alerts_sent_stat (alert_id, user_id, date)";
                    query += "VALUES (" + alertId + ", " + userId + ", getDate());";
                }

                _databaseManager.ExecuteQuery(query);
                return true;
            }
            catch (Exception ex)
            {
                Pages.DeskAlertsBasePage.Logger.Debug(ex);
                return false;
            }
        }

        public int DuplicateAlert(int alertId)
        {
            var user = UserManager.Default.CurrentUser;

            var alertIdParam = SqlParameterFactory.Create(DbType.Int32, alertId, "alertId");
            var lifetime = _databaseManager.GetDataByQuery(SelectLifeTimeQuery, alertIdParam);

            var fromDate = lifetime.GetDateTime(0, "from_date");
            var toDate = lifetime.GetDateTime(0, "to_date");
            var diffTimeSpan = toDate - fromDate;
            var newToDate = DateTime.Now.AddMilliseconds(diffTimeSpan.TotalMilliseconds);

            var toDateParam = SqlParameterFactory.Create(DbType.DateTime, newToDate, "toDate");
            var userIdParam = SqlParameterFactory.Create(DbType.Int32, user.Id, "userId");
            var alertIdParam2 = SqlParameterFactory.Create(DbType.Int32, alertId, "alertId");

            return FillSkinId(alertId, toDateParam, userIdParam, alertIdParam2);
        }

        private int FillSkinId(long alertId, System.Data.SqlClient.SqlParameter toDateParam, System.Data.SqlClient.SqlParameter userIdParam, System.Data.SqlClient.SqlParameter alertIdParam2)
        {
            if (alertId == default(long))
            {
                return 0;
            }
            int result;
            try
            {
                var skinByAlertId = _skinService.GetByAlertId(alertId);

                var currentSkinId = skinByAlertId != null 
                    ? skinByAlertId.Id.ToString() 
                    : "default";

                var skinsIdsAfterCheckPolicy = _skinService.CheckSkinIdInPolicy(currentSkinId, _currentUser.Policy.SkinList);

                var skinIdEquals = string.Equals("default", skinsIdsAfterCheckPolicy, StringComparison.CurrentCultureIgnoreCase);

                if (skinIdEquals)
                {
                    result = _databaseManager.GetScalarByQuery<int>(DuplicateAlertQueryWithNullCaptionId, toDateParam, userIdParam, alertIdParam2);
                }
                else
                {
                    skinsIdsAfterCheckPolicy = skinsIdsAfterCheckPolicy.TrimStart('{').TrimEnd('}');
                    Guid.TryParse(skinsIdsAfterCheckPolicy, out var skinIdParamValue);
                    var skinIdParam = SqlParameterFactory.Create(DbType.Guid, skinIdParamValue, "skinIdParam");
                    result = _databaseManager.GetScalarByQuery<int>(DuplicateAlertQuery, toDateParam, userIdParam, alertIdParam2, skinIdParam);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return result;
        }

        public int DuplicateSurvey(int surveyId)
        {
            throw new NotImplementedException();
        }

        public DataSet GetAlertById(int alertId)
        {
            try
            {
                return _databaseManager.GetDataByQuery("SELECT id, title, alert_text, type2, email, sms, text_to_call, schedule FROM alerts WHERE id = " + alertId);
            }
            catch (Exception ex)
            {
                Pages.DeskAlertsBasePage.Logger.Debug(ex);
                return null;
            }
        }

        public List<int> GetComputersByAlertId(int alertId)
        {
            try
            {
                List<DataRow> groups = _databaseManager
                    .GetDataByQuery("SELECT comp_id FROM alerts_sent_comp WHERE alert_id = " + alertId).ToList();
                return groups.Select(x => x.GetInt("comp_id")).ToList();
            }
            catch (Exception ex)
            {
                Pages.DeskAlertsBasePage.Logger.Debug(ex);
                return null;
            }
        }

        public List<int> GetGroupsByAlertId(int alertId)
        {
            try
            {
                List<DataRow> groups = _databaseManager
                    .GetDataByQuery("SELECT group_id FROM alerts_sent_group WHERE alert_id = " + alertId).ToList();
                return groups.Select(x => x.GetInt("group_id")).ToList();
            }
            catch (Exception ex)
            {
                Pages.DeskAlertsBasePage.Logger.Debug(ex);
                return null;
            }
        }

        public List<int> GetOrganizationUnitsByAlertId(int alertId)
        {
            try
            {
                List<DataRow> ous = _databaseManager
                    .GetDataByQuery("SELECT ou_id FROM alerts_sent_ou WHERE alert_id = " + alertId).ToList();
                return ous.Select(x => x.GetInt("ou_id")).ToList();
            }
            catch (Exception ex)
            {
                Pages.DeskAlertsBasePage.Logger.Debug(ex);
                return null;
            }
        }

        public int GetSurveyIdByAlertId(int alertId)
        {
            int surveyId =
                _databaseManager.GetScalarByQuery<int>("SELECT id FROM surveys_main WHERE sender_id = " + alertId);

            return surveyId;
        }

        public List<int> GetUsersByAlertId(int alertId)
        {
            try
            {
                List<DataRow> groups = _databaseManager
                    .GetDataByQuery("SELECT user_id FROM alerts_sent WHERE alert_id = " + alertId).ToList();
                return groups.Select(x => x.GetInt("user_id")).ToList();
            }
            catch (Exception ex)
            {
                Pages.DeskAlertsBasePage.Logger.Debug(ex);
                return null;
            }
        }

        public List<int> GetUsersByGroupId(int groupId)
        {
            try
            {
                List<DataRow> groups = _databaseManager
                    .GetDataByQuery("SELECT user_id FROM users_groups WHERE group_id = " + groupId).ToList();
                return groups.Select(x => x.GetInt("user_id")).ToList();
            }
            catch (Exception ex)
            {
                Pages.DeskAlertsBasePage.Logger.Debug(ex);
                return null;
            }
        }

        public bool IsSurvey(int alertId)
        {
            var isSurvey = !_databaseManager.GetDataByQuery("SELECT * FROM surveys_main WHERE sender_id = " + alertId)
                               .IsEmpty;

            return isSurvey;
        }

        public int ResendAlert(int alertId)
        {
            try
            {
                return ApiRequestHandler.Send(alertId, _currentUser.Id);
            }
            catch (Exception ex)
            {
                DeskAlertsBasePage.Logger.Debug(ex);
                return 0;
            }
        }

        public bool SendByUserName(string userName, int alertId)
        {
            try
            {
                var userId = Convert.ToInt32(_userService.GetUserByName(userName).Id);
               
                if (SendToUserId(userId, alertId))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                Pages.DeskAlertsBasePage.Logger.Debug(ex);
                return false;
            }

            return false;
        }

        public bool SendToComputerName(string comuter, int alertId)
        {
            var countQuery = "SELECT count(*) FROM computers WHERE name='" + comuter + "'";
            var countComputer = _databaseManager.GetScalarByQuery<int>(countQuery);
            if (countComputer > 0)
            {
                var query = "SELECT id FROM computers WHERE name='" + comuter + "'";
                var computerId = _databaseManager.GetScalarByQuery<int>(query);
                SendToComputers(new List<int>() { computerId }, alertId);
                return true;
            }

            return false;
        }

        public bool SendToComputers(List<int> comuters, int alertId)
        {
            try
            {
                var query = string.Empty;
                foreach (var computerId in comuters)
                {
                    query += "INSERT INTO alerts_sent_comp (alert_id, comp_id)";
                    query += "VALUES (" + alertId + "," + computerId + ");";
                }

                _databaseManager.ExecuteQuery(query);
                return true;
            }
            catch (Exception ex)
            {
                Pages.DeskAlertsBasePage.Logger.Debug(ex);
                return false;
            }
        }

        public bool SendToGroupName(string group, int alertId)
        {
            var countQuery = "SELECT count(*) FROM groups WHERE name='" + group + "'";
            var countGroup = _databaseManager.GetScalarByQuery<int>(countQuery);
            if (countGroup > 0)
            {
                var query = "SELECT id FROM groups WHERE name='" + group + "'";
                var gropuId = _databaseManager.GetScalarByQuery<int>(query);
                SendToGroups(new List<int>() { gropuId }, alertId);
                return true;
            }

            return false;
        }

        public bool SendToGroups(List<int> groups, int alertId)
        {
            try
            {
                var query = string.Empty;
                foreach (var groupId in groups)
                {
                    query += "INSERT INTO alerts_sent_group (alert_id, group_id)";
                    query += "VALUES (" + alertId + "," + groupId + ");";

                    var users = GetUsersByGroupId(groupId);
                    SendToUsers(users, alertId);
                }

                _databaseManager.ExecuteQuery(query);
                return true;
            }
            catch (Exception ex)
            {
                Pages.DeskAlertsBasePage.Logger.Debug(ex);
                return false;
            }
        }

        public bool SendToIPGroups(int sentAlertId, int alertId)
        {
            try
            {
                var query = "INSERT INTO alerts_sent_iprange (alert_id, ip_from, ip_to) SELECT " + alertId
                                                                                                 + ", ip_from, ip_to FROM alerts_sent_iprange WHERE alert_id ="
                                                                                                 + sentAlertId;
                _databaseManager.ExecuteQuery(query);
                return true;
            }
            catch (Exception ex)
            {
                Pages.DeskAlertsBasePage.Logger.Debug(ex);
                return false;
            }
        }

        public bool SendToOrganizationUnits(List<int> ous, int alertId)
        {
            try
            {
                var query = string.Empty;
                foreach (var ou in ous)
                {
                    query += "INSERT INTO alerts_sent_ou (alert_id, ou_id)";
                    query += "VALUES (" + alertId + "," + ou + ");";
                }

                _databaseManager.ExecuteQuery(query);
                return true;
            }
            catch (Exception ex)
            {
                Pages.DeskAlertsBasePage.Logger.Debug(ex);
                return false;
            }
        }

        public bool SendToOrganizationUnitsName(string ou, int alertId)
        {
            var countQuery = "SELECT count(*) FROM OU WHERE name='" + ou + "'";
            var countOu = _databaseManager.GetScalarByQuery<int>(countQuery);
            if (countOu > 0)
            {
                var query = "SELECT id FROM OU WHERE name='" + ou + "'";
                var ouId = _databaseManager.GetScalarByQuery<int>(query);
                SendToOrganizationUnits(new List<int>() { ouId }, alertId);
                return true;
            }

            return false;
        }

        public bool SendToUserId(int userId, int alertId)
        {
            try
            {
                var query = string.Empty;
                var countUser = _databaseManager.GetDataByQuery("SELECT id FROM users WHERE id='" + userId + "'").Count;
                if (countUser < 1)
                {
                    return false;
                }

                query += "INSERT INTO alerts_sent (alert_id, user_id)";
                query += "VALUES (" + alertId + "," + userId + ");";

                _databaseManager.ExecuteQuery(query);
                return true;
            }
            catch (Exception ex)
            {
                Pages.DeskAlertsBasePage.Logger.Debug(ex);
                return false;
            }
        }

        public bool SendToUsers(List<int> users, int alertId)
        {
            try
            {
                var query = string.Empty;
                foreach (var userId in users)
                {
                    query += "INSERT INTO alerts_sent (alert_id, user_id)";
                    query += "VALUES (" + alertId + "," + userId + ");";
                }

                _databaseManager.ExecuteQuery(query);

                var alertSet = _databaseManager.GetDataByQuery("SELECT * FROM alerts WHERE id = " + alertId);
                if (Config.Data.HasModule(Modules.MOBILE) && alertSet.GetInt(0, "campaign_id") == -1
                                                          && alertSet.GetInt(0, "device") != 1
                                                          && alertSet.GetInt(0, "schedule") != 1
                                                          && NeedPushForThisAlert(alertSet.First()))
                {
                    IConfigurationManager configurationManager = new WebConfigConfigurationManager(Config.Configuration);
                    IUserInstancesRepository userInstancesRepository = new PpUserInstancesRepository(configurationManager);
                    PushNotificationsManager.Default.AddPushNotificationToDb(alertId, users.Select(x => (long)x));
                }

                return true;
            }
            catch (Exception ex)
            {
                Pages.DeskAlertsBasePage.Logger.Debug(ex);
                return false;
            }
        }

        private bool IsBroadcast(DataSet row)
        {
            return row.GetString(0, "type2") == "B";
        }

        private bool NeedPushForThisAlert(DataRow alertRow)
        {
            var alertClass = (AlertType)alertRow.GetInt("class");
            return alertClass == AlertType.SimpleAlert || alertClass == AlertType.RssAlert || alertClass == AlertType.EmergencyAlert
                   || alertClass == AlertType.SimpleSurvey || alertClass == AlertType.SurveyPoll || alertClass == AlertType.SurveyQuiz;
        }

        private string GetAlertSendType(int alertId)
        {
            var sendingType = _databaseManager.GetScalarByQuery<string>("SELECT type2 FROM alerts WHERE id = " + alertId);
            string resut = string.Empty;
            switch (sendingType)
            {
                case "R":
                    resut = "recipients";
                    break;
                case "B":
                    resut = "broadcast";
                    break;
            }
            return resut;
        }
    }
}