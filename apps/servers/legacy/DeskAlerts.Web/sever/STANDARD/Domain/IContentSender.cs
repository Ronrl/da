﻿
using DeskAlertsDotNet.DataBase;
using System.Collections.Generic;

namespace DeskAlertsDotNet.Server.Domain
{
    public interface IContentSender
    {
        int DuplicateAlert(int alertId);
        int ResendAlert(int alertId);
        List<int> GetGroupsByAlertId(int alertId);
        List<int> GetUsersByGroupId(int groupId);
        List<int> GetUsersByAlertId(int alertId);
        List<int> GetComputersByAlertId(int alertId);
        List<int> GetOrganizationUnitsByAlertId(int alertId);
        bool SendToGroups(List<int> groups, int alertId);
        bool SendToUsers(List<int> users, int alertId);
        bool SendToComputers(List<int> comuters, int alertId);
        bool SendToOrganizationUnits(List<int> ous, int alertId);
        bool SendToIPGroups(int sentAlertId, int alertId);
        int DuplicateSurvey(int surveyId);
        bool IsSurvey(int alertId);
        int GetSurveyIdByAlertId(int alertId);
		bool AddRecipientToStatistic(List<int> usersId, int alertId);
        DataSet GetAlertById(int alertId);

    }
}
