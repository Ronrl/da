﻿using System.Collections.Generic;

namespace DeskAlertsDotNet.Server.Domain
{
    public class Recipients
    {
        public List<int> UsersId { get; set; }
        public List<int> GroupsId { get; set; }
        public List<int> ComputersId { get; set; }
        public List<int> OUsId { get; set; }
    }
}