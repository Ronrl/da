﻿<%@ WebService Language="C#" CodeBehind="getEasyvista.asmx.cs" Class="GetEasyvista.getEasyvista" %>

using System;
using System.Collections.Generic;
//using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Serialization;
using System.ComponentModel;
using System.Security.Cryptography;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.IO;

namespace GetEasyvista
{
    /// <summary>
    /// Summary description for getEasyvista
    /// </summary>
    [WebService(Namespace = "")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    [SoapDocumentService(RoutingStyle = SoapServiceRoutingStyle.RequestElement)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    //[System.Web.Script.Services.ScriptService]
    public class getEasyvista : System.Web.Services.WebService
    {
        string url_api;
        
        public class Response
        {
            private string result, apiKey, text, title, group, desktop, email, sms;

            public string Result { get { return result; } set { result = value; } }
            public string ApiKey { get { return apiKey; } set { apiKey = value; } }
            public string Text { get { return text; } set { text = value; } }
            public string Title { get { return title; } set { title = value; } }
            public string Group { get { return group; } set { group = value; } }
            public string Desktop { get { return desktop; } set { desktop = value; } }
            public string Email { get { return email; } set { email = value; } }
            public string Sms { get { return sms; } set { sms = value; } }

            public Response()
            {
                Result = "Is OK";
                ApiKey = "Is OK";
                Text = "Is OK";
                Title = "Is OK";
                Group = "Is OK";
                Desktop = "Is OK";
                Email = "Is OK";
                Sms = "Is OK";
            }
        }

        public getEasyvista()
        {
            string url = HttpContext.Current.Request.Url.AbsoluteUri;
            string path = url.Substring(0, url.IndexOf("getEasyvista"));
            url_api = path+"/api_request.asp";
        }


        private string sendRequest(string value, string url)
        {
            WebRequest request = WebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            byte[] byteArray = Encoding.UTF8.GetBytes(value);
            request.ContentLength = byteArray.Length;


            Stream dataStream = request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();

            WebResponse api_response = request.GetResponse();
            dataStream = api_response.GetResponseStream();

            StreamReader reader_response = new StreamReader(dataStream);
            string responseFromServer = reader_response.ReadToEnd();

            reader_response.Close();
            dataStream.Close();
            api_response.Close();
            return responseFromServer;
        }

        private bool validateParams(Response response, string text, string title, string group, string desktop, string email, string sms, string apiKey)
        {
            bool valide = true;
            if (apiKey.Equals(""))
            {
                response.ApiKey = "ApiKey can not be empty";
                valide = false;
            }
            if (text.Equals(""))
            {
                response.Text = "Text can not be empty";
                valide = false;
            }
            if (title.Equals(""))
            {
                response.Title = "Title can not be empty";
                valide = false;
            }
            if (group.Equals(""))
            {
                response.Group = "Group can not be empty";
                valide = false;
            }
            if (desktop.Equals(""))
            {
                response.Desktop = "Desktop can not be empty";
                valide = false;
            }
            if (email.Equals(""))
            {
                response.Email = "Email can not be empty";
                valide = false;
            }
            if (sms.Equals(""))
            {
                response.Sms = "SMS can not be empty";
                valide = false;
            }

            return valide;
        }
        
        [WebMethod]
        [SoapDocumentMethod(RequestElementName = "sendAlert",
    ResponseElementName = "response")]
        public Response sendAlert(string text, string title, string group, string desktop, string email, string sms, string apiKey, string domain)
        {
            Response response = new Response();

            bool valide = validateParams(response, text, title, group, desktop, email, sms, apiKey);

            if (!valide) response.Result = "Invalid data";
            else
            {
                string query = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
                query += "<darequest>";
                    query += "<alert>";
                        query += "<data>";
                            query += "<title><![CDATA["+title+"]]></title>";
                            query += "<text><![CDATA["+text+"]]></text>";
                            query += "<template_id>123</template_id>";
                            query += "<desktop>"+desktop+"</desktop>";
                            query += "<sms>"+sms+"</sms>";
                            query += "<email>"+email+"</email>";
                        query += "</data>";
                        query += "<recipients>";
                            query += "<domain name=\""+domain+"\">";
                                query += "<recipient type=\"group\" name=\""+group+"\" />";
                            query += "</domain>";
                        query += "</recipients>";
                    query += "</alert>";
                query += "</darequest>";

                string resXml = HttpUtility.UrlEncode(query);

                string api = apiKey;

                if (!api.Equals(""))
                {
                    string body = "xml_data=" + resXml + "&api_signature=" + api;
                    response.Result = sendRequest(body, url_api);   
                }
                else response.Result = "Invalid DeskAlerts API key";
                 
            }
            
            return response;
        }

    }
}