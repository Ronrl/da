﻿using Castle.MicroKernel.Lifestyle;
using DeskAlerts.Server.Utils.Interfactes;
using DeskAlertsDotNet.Application;
using System;

namespace DeskAlerts.Server.sever.STANDARD
{
    public partial class ShowToken : System.Web.UI.Page
    {
        private readonly ITokenService _tokenService;

        public ShowToken()
        {
            using (Global.Container.BeginScope())
            {
                _tokenService = Global.Container.Resolve<ITokenService>();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
            }
            else
            {
                DisplayCacheContent();
            }
        }

        private void DisplayCacheContent()
        {
            TokenViewerList.DataSource = _tokenService.GetAllTokens();
            TokenViewerList.DataBind();
        }
    }
}