using DeskAlerts.ApplicationCore.Dtos;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Enums;
using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.ApplicationCore.Models.ContentTypes;
using DeskAlerts.ApplicationCore.Repositories;
using DeskAlerts.ApplicationCore.Utilities;
using DeskAlerts.ContentDeliveringService.Interfaces;
using DeskAlerts.ContentDeliveringService.Models;
using DeskAlerts.Server.Api.v1;
using DeskAlerts.Server.Utils.Interfaces.Services;
using DeskAlerts.Server.Utils.Interfactes;
using DeskAlerts.Server.Utils.Managers;
using DeskAlerts.Server.Utils.Services;
using DeskAlertsDotNet.Application;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Models;
using DeskAlertsDotNet.Parameters;
using DeskAlertsDotNet.Utils.Consts;
using DeskAlertsDotNet.Utils.Factories;
using DeskAlertsDotNet.Utils.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;

namespace DeskAlertsDotNet.Pages.AlertsRecieving
{
    public partial class GetAlerts : System.Web.UI.Page
    {
        // get_xml.asp?uname=ddddd&computer=PC&cnt=10
        // &deskbar_id=%7BD64334E7-83AC-4ac4-92C7-7DF2682A3292%7D&key=1b5357c80023aff9f375b6564fe005805e617664fb1ee912
        // &client_version=8.1.4.28&nextrequest=5&domain=&fulldomain=&compdomain=&edir=&rnd=18892&localtime=14.05.2016|22:57:47

        public CacheManager CacheManager => Global.CacheManager;

        private readonly IUserInstancesRepository _userInstancesRepository;
        private readonly IAlertRepository _alertRepository;
        private readonly IAlertReceivedRepository _alertReceivedRepository;
        private readonly IDomainRepository _domainRepository;

        private readonly IUserService _userService;
        private readonly IContentService _contentService;
        private readonly IComputerService _computerService;
        private readonly UserInstanceService _userInstanceService;

        private readonly ICacheInvalidationService _cacheInvalidationService;
        private readonly IContentDeliveringService<AlertContentType> _alertContentDeliveringService;
        private readonly IContentDeliveringService<TickerContentType> _tickerContentDeliveringService;
        private readonly IContentDeliveringService<RsvpContentType> _rsvpContentDeliveringService;
        private readonly IContentDeliveringService<SurveyContentType> _surveyContentDeliveringService;
        private readonly IContentDeliveringService<ScreensaverContentType> _screensaverContentDeliveringService;
        private readonly IContentDeliveringService<WallpaperContentType> _wallpaperContentDeliveringService;
        private readonly IContentDeliveringService<LockscreenContentType> _lockscreenContentDeliveringService;
        private readonly IContentDeliveringService<VideoAlertContentType> _videoAlertContentDeliveringService;

        private string _confMultiAlerts;

        public GetAlerts()
        {
            _cacheInvalidationService = Global.Container.Resolve<ICacheInvalidationService>();
            _alertContentDeliveringService = Global.Container.Resolve<IContentDeliveringService<AlertContentType>>();
            _tickerContentDeliveringService = Global.Container.Resolve<IContentDeliveringService<TickerContentType>>();
            _rsvpContentDeliveringService = Global.Container.Resolve<IContentDeliveringService<RsvpContentType>>();
            _surveyContentDeliveringService = Global.Container.Resolve<IContentDeliveringService<SurveyContentType>>();
            _screensaverContentDeliveringService = Global.Container.Resolve<IContentDeliveringService<ScreensaverContentType>>();
            _wallpaperContentDeliveringService = Global.Container.Resolve<IContentDeliveringService<WallpaperContentType>>();
            _lockscreenContentDeliveringService = Global.Container.Resolve<IContentDeliveringService<LockscreenContentType>>();
            _videoAlertContentDeliveringService = Global.Container.Resolve<IContentDeliveringService<VideoAlertContentType>>();

            var configurationManager = new WebConfigConfigurationManager(Config.Configuration);

            var userRepository = new PpUserRepository(configurationManager);
            var groupRepository = new PpGroupRepository(configurationManager);
            var alertReadRepository = new PpAlertReadRepository(configurationManager);
            _alertRepository = new PpAlertRepository(configurationManager);
            var multipleAlertsRepository = new PpMultipleAlertRepository(configurationManager);

            _userInstancesRepository = new PpUserInstancesRepository(configurationManager);
            _alertReceivedRepository = new PpAlertReceivedRepository(configurationManager);
            _domainRepository = new PpDomainRepository(configurationManager);
            var tickerRepository = new PpTickerRepository(configurationManager);
            var rsvpRepository = new PpRsvpRepository(configurationManager, _alertRepository);
            var surveyRepository = new PpSurveyRepository(configurationManager, _alertRepository);
            var videoAlertRepository = new PpVideoAlertRepository(configurationManager, _alertRepository);
            var wallPaperRepository = new PpWallPaperRepository(configurationManager);
            var screenSaverRepository = new PpScreenSaverRepository(configurationManager);
            var lockScreenRepository = new PpLockScreenRepository(configurationManager);
            var scheduledContentRepository = new PpScheduledContentRepository(configurationManager);

            _computerService = new ComputerService();
            _userService = new UserService(userRepository, groupRepository);
            _userInstanceService = new UserInstanceService();
            _contentService = new ContentService(
                _alertRepository,
                _alertReceivedRepository,
                alertReadRepository,
                multipleAlertsRepository,
                tickerRepository,
                rsvpRepository,
                surveyRepository,
                videoAlertRepository,
                wallPaperRepository,
                screenSaverRepository,
                lockScreenRepository,
                scheduledContentRepository);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                var domainName = Request.GetParam("domain_name", string.Empty);

                if (string.IsNullOrEmpty(domainName))
                {
                    domainName = Request.GetParam("fulldomain", string.Empty);
                }

                var userInfo = new UserInfoDto
                {
                    Username = Request.GetParam("uname", string.Empty),
                    Password = Request.GetParam("upass", string.Empty),
                    Phone = Request.GetParam("uphone", string.Empty),
                    Email = Request.GetParam("uemail", string.Empty),
                    DomainName = domainName,
                    DeskbarId = Request.GetParam("deskbar_id", Guid.NewGuid().ToString()).TrimStart('{').TrimEnd('}'),
                    ClientId = Request.GetParam("client_id", string.Empty)
                };

                var compName = Request["computer"];
                var compDomain = Request["compdomain"];

                _userInstanceService.HandleNewUserInstance(userInfo, Request.UserHostAddress, compName, compDomain);
                
                var deskbar = Request["deskbar_id"];
                var userInstance = _userInstancesRepository.GetInstance(Guid.Parse(deskbar));
                var user = _userService.GetUserById(userInstance.UserId);
                var userDomain = _domainRepository.GetById(user.DomainId);
                
                using (var dbMgr = new DBManager())
                {
                    string type;
                    var contentType = Request["content_type"];

                    switch (contentType)
                    {
                        case "wp":
                            type = "WALLPAPERS";
                            break;
                        case "ss":
                            type = "SCREENSAVERS";
                            break;
                        case "ls":
                            type = "LOCKSCREENS";
                            break;
                        default:
                            type = "TOOLBAR";
                            break;
                    }

                    Response.Write("<" + type + ">");

                    var userName = this.Request["uname"];
                    var rawClientDateTime = Request["localtime"];
                    var nextRequest = this.Request["nextrequest"];
                    var fullDomain = userDomain.Name;
                    var standBy = this.Request["standby"];
                    var disable = this.Request["disable"];
                    var screensaversHash = string.Empty;
                    var wallpapersHash = string.Empty;
                    var lockscreensHash = string.Empty;
                    var clientVersion = this.Request["client_version"];

                    var domainEntity = _domainRepository.GetByName(fullDomain);

                    if (domainEntity == default(DeskAlerts.ApplicationCore.Entities.Domain))
                    {
                        throw new Exception($"Can't get '{fullDomain}' domain!");
                    }

                    if (string.IsNullOrEmpty(fullDomain))
                    {
                        fullDomain = null;
                    }

                    if (string.IsNullOrEmpty(compDomain))
                    {
                        compDomain = null;
                    }

                    if (!string.IsNullOrEmpty(fullDomain) || string.IsNullOrEmpty(compDomain))
                    {
                        compDomain = fullDomain;
                    }

                    if (string.IsNullOrEmpty(standBy))
                    {
                        standBy = "0";
                    }

                    if (string.IsNullOrEmpty(disable))
                    {
                        disable = "0";
                    }

                    if (string.IsNullOrEmpty(nextRequest))
                    {
                        nextRequest = "5";
                    }

                    var isMobile = (compName == "mobile_client" || compName == "mobile");
                    var userKey = userName;

                    if (!string.IsNullOrEmpty(fullDomain))
                    {
                        fullDomain = fullDomain.ToLower();
                        userKey = (fullDomain + _userService.ConnectSign + userName).ToLower();
                    }

                    if (userKey.StartsWith("."))
                    {
                        userKey = userName;
                    }

                    var compKey = compName;

                    if (!string.IsNullOrEmpty(compDomain))
                    {
                        compDomain = compDomain.ToLower();
                        compKey = compDomain + "." + compName;
                    }

                    if (string.IsNullOrEmpty(rawClientDateTime))
                    {
                        rawClientDateTime = DateTime.Now.ToString(DateTimeFormat.Client);
                    }

                    var clientDateTime = DateTime.ParseExact(
                        rawClientDateTime,
                        DateTimeFormat.Client,
                        CultureInfo.InvariantCulture);

                    int clientTimeZone;

                    if (string.IsNullOrEmpty(Request["timezone"]))
                    {
                        clientTimeZone = DateTimeUtils.GetTimeZoneByLocalTime(clientDateTime);
                    }
                    else
                    {
                        if (!int.TryParse(Request["timezone"], out clientTimeZone))
                        {
                            DeskAlertsBasePage.Logger.Error($"{Request["timezone"]} is not integer!");

                            throw new ArgumentException($"{Request["timezone"]} is not integer!");
                        }
                    }

                    var ipAddress = Request["ip"];
                    DeskAlertsBasePage.Logger.Trace($"Variable: {nameof(ipAddress)}. Value: {ipAddress}.");
                    var longIp = DeskAlerts.Server.Utils.Helpers.IpReader.GetIpAddressListAsInt64(ipAddress, Request);

                    _confMultiAlerts = "1";

                    List<Alert> allAlerts;
                    if (AppConfiguration.IsNewContentDeliveringScheme)
                    {
                        var key = new ContentDeliveringKey(clientTimeZone, userInstance.UserId, userInstance.Clsid);

                        var alertIds = _alertContentDeliveringService.Get(key, string.Empty, string.Empty);
                        var tickerIds = _tickerContentDeliveringService.Get(key, string.Empty, string.Empty);
                        var rsvpIds = _rsvpContentDeliveringService.Get(key, string.Empty, string.Empty);
                        var surveyIds = _surveyContentDeliveringService.Get(key, string.Empty, string.Empty);
                        var screensaverIds = _screensaverContentDeliveringService.Get(key, string.Empty, string.Empty);
                        var wallpaperIds = _wallpaperContentDeliveringService.Get(key, string.Empty, string.Empty);
                        var lockscreenIds = _lockscreenContentDeliveringService.Get(key, string.Empty, string.Empty);
                        var videoIds = _videoAlertContentDeliveringService.Get(key, string.Empty, string.Empty);

                        var allIds = new List<string>
                        {
                            alertIds,
                            tickerIds,
                            rsvpIds,
                            surveyIds,
                            screensaverIds,
                            wallpaperIds,
                            lockscreenIds,
                            videoIds
                        };

                        var stringOfContent = string.Join(",", allIds.Where(x => !string.IsNullOrEmpty(x)))
                            .Split(',')
                            .ToList();

                        var contentIds = new List<long>();

                        foreach (var idString in stringOfContent)
                        {
                            if (string.IsNullOrEmpty(idString))
                            {
                                continue;
                            }
                                
                            var id = long.Parse(idString);
                            contentIds.Add(id);
                        }

                        allAlerts = ContentControllerHelper.GetAlertListByIds(contentIds).ToList();
                    }
                    else
                    {
                        var userAlerts = CacheManager.GetAlertsForInstance(deskbar, userName.ToLower(), fullDomain);
                        var compAlerts = CacheManager.GetAlertsForComputer(compName.ToLower(), compDomain);
                        var ipAlerts = CacheManager.GetAlertsForIp(longIp);
                        allAlerts = userAlerts
                            .Concat(compAlerts)
                            .Concat(ipAlerts)
                            .ToList();
                    }

                    if (contentType == "wp")
                    {
                        allAlerts = allAlerts.Where(x => x.Class == (int)AlertType.Wallpaper).ToList();
                    }

                    if (contentType == "ls")
                    {
                        allAlerts = allAlerts.Where(x => x.Class == (int)AlertType.LockScreen).ToList();
                    }

                    int userId = -1;


                    try
                    {
                        if (!string.IsNullOrEmpty(clientVersion))
                        {
                            var sql =
                                $"UPDATE users SET last_request = GETDATE(), next_request = {_userService.ConvertToSeconds(nextRequest)}, standby={standBy}, disabled={disable}, client_version = '{clientVersion}' WHERE name =  @userName AND role = 'U'";
                            var updateUserNameParam = SqlParameterFactory.Create(DbType.String, userName, "userName");
                            dbMgr.ExecuteQuery(sql, updateUserNameParam);
                        }

                        userId = (int)_userService.GetUserByUserNameAndDomainName(userName, fullDomain ?? string.Empty).Id;
                    }
                    catch (Exception ex)
                    {
                        DeskAlertsBasePage.Logger.Info(ex.Message);

                        Response.Write($"<ERROR>User does not exists</ERROR></{type}>");
                        dbMgr.Dispose();
                        Response.End();
                        return;
                    }

                    if (userId == 0)
                    {
                        DeskAlertsBasePage.Logger.Info($"User with {userName} username not found");
                        Response.Write($"<ERROR>User does not exists</ERROR></{type}>");
                        Response.End();
                        return;
                    }


                    try
                    {
                        _userService.UpdateUserTimeZone(userId, clientTimeZone);

                        _computerService.UpdateComputerLastRequest(compName, compDomain);
                    }
                    catch (Exception ex)
                    {
                        DeskAlertsBasePage.Logger.Info(ex.Message);
                    }

                    foreach (Alert alert in allAlerts)
                    {
                        if (alert.Lifetime == 1)
                        {
                            var dateTimeNow = DateTime.Now.ToString(
                                DateTimeFormat.MsSqlFormat,
                                CultureInfo.InvariantCulture);
                            clientDateTime = DateTime.ParseExact(
                                dateTimeNow,
                                DateTimeFormat.MsSqlFormat,
                                CultureInfo.InvariantCulture);
                        }

                        if (alert.IsExpired(clientDateTime) || !alert.IsStarted(clientDateTime))
                        {
                            continue;
                        }

                        if (!AppConfiguration.IsNewContentDeliveringScheme)
                        {
                            if (!CacheManager.IsActiveAlert(alert))
                            {
                                continue;
                            }
                        }

                        if (!AppConfiguration.IsNewContentDeliveringScheme)
                        {
                            if (alert.Class != 2 && 
                                alert.Class != 8 &&
                                alert.Class != (int)AlertType.LockScreen &&
                                CacheManager.IsReceivedAlertForUser(deskbar.TrimStart('{').TrimEnd('}').ToLower() + "." + userKey, alert.Id))
                            {
                                continue;
                            }
                        }

                        if (isMobile && alert.Device == Device.Desktop)
                        {
                            continue;
                        }

                        if (!isMobile && alert.Device == Device.Mobile)
                        {
                            continue;
                        }

                        // screensaver
                        if (alert.Class == 2 &&
                            (!alert.IsExpired(clientDateTime) && alert.IsStarted(clientDateTime)))
                        {
                            if (screensaversHash != string.Empty)
                            {
                                screensaversHash += ",";
                            }

                            screensaversHash += alert.Id;
                        }
                        else if (alert.Class == 8 &&
                                 (!alert.IsExpired(clientDateTime) && alert.IsStarted(clientDateTime)))
                        {
                            if (wallpapersHash != string.Empty)
                            {
                                wallpapersHash += ",";
                            }

                            wallpapersHash += alert.Id;
                        }
                        
                        else if (alert.Class == (int)AlertType.LockScreen &&
                                 (!alert.IsExpired(clientDateTime) && alert.IsStarted(clientDateTime)))
                        {
                            if (lockscreensHash != string.Empty)
                            {
                                lockscreensHash += ",";
                            }

                            lockscreensHash += alert.Id;
                        }

                        // wallpaper
                        if (disable == "1" && alert.Urgent != 1)
                        {
                            alert.Class = 16;
                        }

                        int[] badClasses = { 4, 64, 128, 256 };

                        if ((type == "TOOLBAR" && alert.Class != 8 && alert.Class != 2 && alert.Class != (int)AlertType.LockScreen)
                            || (alert.Class == 8 && contentType == "wp")
                            || (alert.Class == 2 && contentType == "ss")
                            || (alert.Class == (int)AlertType.LockScreen && contentType == "ls")
                            && !Array.Exists(badClasses, c => c == alert.Class))
                        {
                            var alertXml = alert.ToXmlString(
                                clientVersion,
                                deskbar,
                                userName,
                                userId,
                                clientDateTime,
                                isMobile);

                            if (alertXml.Length == 0)
                            {
                                continue;
                            }

                            Response.Write(alertXml);
                        }

                        var alertReceivedEntity = _alertReceivedRepository.GetByAlertIdAndUserId(alert.Id, userId, Guid.Parse(deskbar).ToString());
                        if (alertReceivedEntity == null)
                        {
                            continue;
                        }

                        _contentService.UpdateAlertReceived(alertReceivedEntity, AlertReceiveStatus.Received);

                        // Cache invalidation
                        if (AppConfiguration.IsNewContentDeliveringScheme)
                        {
                            var typeOfContent = _alertRepository.GetContentTypeByContentId(alert.Id);
                            _cacheInvalidationService.CacheInvalidation(typeOfContent, alert.Id);
                        }
                        else
                        {
                            CacheManager.AddAlertsToReceivedCache(deskbar.TrimStart('{').TrimEnd('}').ToLower() + "." + userKey, alert.Id);
                        }
                    }

                    if (disable == "1")
                    {
                        Response.Write("<ALERTCOUNT count=\"" + allAlerts.Count + "\" />");
                    }

                    if (type == "TOOLBAR")
                    {
                        if (wallpapersHash != string.Empty)
                        {
                            Response.Write("<WALLPAPERS hash=\"" + HashingUtils.MD5(wallpapersHash) + "\" />");
                        }

                        if (screensaversHash != string.Empty)
                        {
                            Response.Write("<SCREENSAVERS hash=\"" + HashingUtils.MD5(screensaversHash) + "\" />");
                        }

                        if (lockscreensHash != string.Empty)
                        {
                            Response.Write("<LOCKSCREENS hash=\"" + HashingUtils.MD5(lockscreensHash) + "\" />");
                        }
                    }

                    Response.Write("</" + type + ">");
                }
            }
            catch (Exception ex)
            {
                DeskAlertsBasePage.Logger.Trace(ex);
            }
        }
    }
}