﻿using System;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Pages;

namespace DeskAlertsDotNet.Licensing
{
    public class LicenseManager
    {
        private static bool _isTrialLocked;
        private static readonly object _lock = new object();
        public static bool IsTrialLocked
        {
            get
            {
                lock (_lock)
                {
                    using (var dbMgr = new DBManager())
                    {
                        if (_instance.GetTrialTime(dbMgr) == 0)
                        {
                            return false;
                        }

                        return _isTrialLocked;
                    }
                }
            }
            private set
            {
                lock (_lock)
                {
                    using (var dbMgr = new DBManager())
                    {
                        _isTrialLocked = _instance.GetTrialTime(dbMgr) != 0 && value;
                    }
                }
            }
        }
        private static bool _isLicLocked;
        public static bool IsLicenceLocked
        {
            get
            {
                lock (_lock)
                {
                    using (var dbMgr = new DBManager())
                    {
                        var licenseCount = _instance.GetLicenseCount(dbMgr);

                        if (licenseCount == 0)
                        {
                            return false;
                        }

                        var currentUsersCount = _instance.GetCurrentUsersCount();

                        if ((currentUsersCount != -1) && (currentUsersCount > licenseCount))
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
            }

            private set
            {
                lock (_lock)
                {
                    using (var dbMgr = new DBManager())
                    {
                        _isLicLocked = _instance.GetLicenseCount(dbMgr) != 0 && value;
                    }
                }
            }
        }
        private static bool _isLicenseDateExpired;

        /// <summary>
        /// Check if license date expired
        /// </summary>
        public static bool IsLicenceDateExpired
        {
            get
            {
                lock (_lock)
                {
                    return _instance.IsLicenseDateExpired();
                }
            }

            private set
            {
                lock (_lock)
                {
                    _isLicenseDateExpired = _instance.IsLicenseDateExpired();
                }
            }
        }

        /// <summary>
        /// Check if license expire date is greater than current date.
        /// </summary>
        /// <returns></returns>
        private bool IsLicenseDateExpired()
        {
            using (var databaseManager = new DBManager())
            {
                string sqlQuery = "SELECT license_expire_date FROM version";
                DateTime licenseExpireDate = databaseManager.GetDataByQuery(sqlQuery).GetDateTime(0, "license_expire_date");
                DateTime currentDateTime = DateTime.Now;
                bool result = licenseExpireDate < currentDateTime;

                return result;
            }
        }

        private static void CheckLic()
        {
            throw new NotImplementedException();
        }


        public static bool IsFirstLoad { get; private set; }
        public static long CurrentUsersCount { get; private set; }
        public static long ServerLifetime { get; private set; }
        public static bool FailedUsersCountSelect { get; private set; }

        /// <summary>
        /// Check if system is locked or not
        /// </summary>
        public static bool IsLocked
        {
            get
            {
                bool result = IsLicenceLocked || IsTrialLocked || IsLicenceDateExpired;
                return result;
            }
        }

        static LicenseManager _instance;

        public static LicenseManager Default
        {
            get
            {
                if (_instance == null)
                {
                    return _instance = new LicenseManager();
                }

                return _instance;
            }
        }

        public bool CheckLicenseAndTrialState()
        {
            bool result = false;

            if (IsFirstLoad)
            {
                RestoreLockedState();
                IsFirstLoad = false;
            }

            if (FailedUsersCountSelect || (!IsLicenceLocked && !IsTrialLocked))
            {
                IsLicenceLocked = true;
                CurrentUsersCount = GetCurrentUsersCount();

                if (CurrentUsersCount != -1)
                {
                    long licenseCount = GetLicenseCount();

                    if (licenseCount == 0)
                    {
                        CurrentUsersCount = 0;
                    }

                    if (licenseCount == 0 || licenseCount >= CurrentUsersCount)
                    {
                        IsLicenceLocked = false;
                        IsTrialLocked = true;

                        long trialTime = GetTrialTime() * 24 * 60 * 60;

                        if (trialTime > 0)
                        {
                            long lifetime = GetServerLifeTime();

                            if (lifetime <= trialTime)
                            {
                                result = true;
                                IsTrialLocked = false;
                            }
                        }
                        else
                        {
                            result = true;
                            IsTrialLocked = false;
                        }

                        if (!result)
                        {
                            SaveLockedState();
                        }
                    }
                }
                else
                {
                    FailedUsersCountSelect = true;
                }
            }
            else if (!IsTrialLocked && !IsLicenceLocked)
            {
                result = true;
            }

            return result;
        }

        public int GetLicenseCount(DBManager dbMgrOld)
        {
            using (var dbMgr = new DBManager())
            {
                string sqlQuery = "SELECT val as count FROM [settings] WHERE name='licenses'";

                DataSet countSet = dbMgr.GetDataByQuery(sqlQuery);

                string result = countSet.GetString(0, "count");

                if (result.Equals("unlimited"))
                {
                    return 0;
                }

                return countSet.GetInt(0, "count");
            }
        }

        public int GetLicenseCount()
        {
            using (var dbMgr = new DBManager())
            {
                int result = GetLicenseCount(dbMgr);

                return result;
            }
        }

        private void SaveLockedState()
        {
            using (var dbMgr = new DBManager())
            {
                object val = dbMgr.GetScalarByQuery("SELECT 1 FROM [users] WITH (READPAST) WHERE [name] = N'admin' AND [role] = 'A'");

                if (val == null)
                {
                    dbMgr.ExecuteQuery("INSERT INTO [users] ([name], [role]) VALUES (N'admin', 'A')");
                }

                string sqlQuery = @"UPDATE [users] SET ";

                if (IsLicenceLocked)
                {
                    sqlQuery += " last_date = DATEADD(n, " + CurrentUsersCount + @", 0), 
                    last_request = DATEADD(n, " + GetLicenseCount() + @", 0), 
                    next_request = 60";
                }
                else
                {
                    sqlQuery += @"[last_date] = NULL, [last_request] = NULL, [next_request] = NULL";
                }

                sqlQuery += " , ";

                if (IsTrialLocked)
                {
                    sqlQuery += "last_standby_request = DATEADD(n," + GetTrialTime() + ", 0) ";
                    sqlQuery += ", [next_standby_request] = 60";

                }
                else
                {
                    sqlQuery += "[last_standby_request] = NULL, [next_standby_request] = NULL";
                }

                sqlQuery += " WHERE [name] = N'admin' AND [role] = 'A'";

                dbMgr.ExecuteQuery(sqlQuery);
            }
        }

        private void RestoreLockedState()
        {
            using (var dbMgr = new DBManager())
            {
                string sqlQuery = "SELECT DATEDIFF(n, 0, [last_date]) as last_date, DATEDIFF(n, 0, [last_request]) as last_request, CAST([next_request] AS INT) as next_request, DATEDIFF(n, 0, [last_standby_request]) as last_standby_request, CAST([next_standby_request] AS INT) as next_standby_request FROM [users] WITH (READPAST) WHERE [name] = N'admin' AND [role] = 'A'";

                DataSet dataSet = dbMgr.GetDataByQuery(sqlQuery);
                bool needToSaveValues = false;
                int? lastRequestVal = dataSet.GetInt(0, "last_request");

                if (lastRequestVal.HasValue)
                {
                    int? nextRequestVal = dataSet.GetInt(0, "next_request");

                    if (nextRequestVal.HasValue && nextRequestVal.Value == 60)
                    {
                        if (GetLicenseCount() == lastRequestVal.Value && GetLicenseCount() != 0)
                        {
                            IsLicenceLocked = true;

                            int? lastDateVal = dataSet.GetInt(0, "last_date");

                            CurrentUsersCount = lastRequestVal.Value;
                        }
                    }
                    else
                    {
                        needToSaveValues = true;
                    }
                }

                int? lastStandByReq = dataSet.GetInt(0, "last_standby_request");

                if (lastStandByReq.HasValue)
                {
                    int? nextStandByReq = dataSet.GetInt(0, "next_standby_request");

                    if (nextStandByReq.HasValue && nextStandByReq.Value == 60)
                    {
                        if (GetTrialTime() == lastStandByReq.Value)
                        {
                            IsTrialLocked = true;
                        }
                        else
                        {
                            needToSaveValues = true;
                        }
                    }
                }

                if (needToSaveValues)
                {
                    SaveLockedState();
                }
            }
        }
        public long GetTrialTime()
        {
            using (var dbMgr = new DBManager())
            {
                long result = GetTrialTime(dbMgr);

                return result;
            }
        }

        public long GetTrialTime(DBManager dbMgrOld)
        {
            using (var dbMgr = new DBManager())
            {
                string sqlQuery = "SELECT val as count FROM [settings] WHERE name='supermario'";

                DataSet countSet = dbMgr.GetDataByQuery(sqlQuery);

                return (countSet.GetInt(0, "count"));
            }
        }

        public int GetCurrentUsersCount()
        {
            using (var dbMgr = new DBManager())
            {
                const string sqlQuery = @" 
				    DECLARE @from_date DATETIME 
				    SET @from_date = DATEADD(day, -1 , GETDATE()) 
				    SELECT COUNT(1) as count FROM ( 
					    SELECT distinct u.name FROM alerts a 
						    INNER JOIN alerts_sent s
						    ON s.alert_id = a.id 
						    INNER JOIN users u
						    ON s.user_id = u.id AND NOT u.mobile_phone = '' 
						    WHERE a.sent_date > @from_date AND a.schedule = '0' AND a.sms = '1' 
					    UNION 
					    SELECT distinct u.name FROM alerts a
						    INNER JOIN alerts_sent s
						    ON s.alert_id = a.id 
						    INNER JOIN users u
						    ON s.user_id = u.id AND NOT u.email = N'' 
						    WHERE a.sent_date > @from_date AND a.schedule = '0' AND a.email = '1' 
					    UNION 
					    SELECT distinct u.name FROM alerts a
						    INNER JOIN alerts_received_recur r
						    ON r.alert_id = a.id 
						    INNER JOIN users u
						    ON r.user_id = u.id AND NOT u.mobile_phone = '' 
						    WHERE r.date > @from_date AND a.schedule = '1' AND a.sms = '1' 
					    UNION 
					    SELECT distinct u.name FROM alerts a
						    INNER JOIN alerts_received_recur r
						    ON r.alert_id = a.id 
						    INNER JOIN users u
						    ON r.user_id = u.id AND NOT u.email = N'' 
						    WHERE r.date > @from_date AND a.schedule = '1' AND a.email = '1' 
					    UNION 
					    SELECT distinct name FROM users
						    WHERE role = 'U' and last_request > @from_date 
				    ) t";

                var countSet = dbMgr.GetDataByQuery(sqlQuery);

                return countSet.GetInt(0, "count"); ;
            }
        }

        public long GetServerLifeTime()
        {
            using (var dbMgr = new DBManager())
            {
                const string sqlQuery = @"SELECT DATEDIFF(s, [reg_date], GETDATE()) as lifetime FROM [users] WHERE [name] = N'admin' AND [role] = 'A'";
                var countSet = dbMgr.GetDataByQuery(sqlQuery);

                return countSet.GetInt(0, "lifetime");
            }
        }

        public static void ProlongLicense(string usedProlongKeysString, DBManager dbMgrOld)
        {
            using (var dbMgr = new DBManager())
            {
                string newLicenseExpireDate = DateTime.Now.AddYears(1).ToString();
                string SQLQuery = $"UPDATE version SET license_expire_date='{newLicenseExpireDate}', used_prolong_keys='{usedProlongKeysString}'";

                dbMgr.ExecuteQuery(SQLQuery);
            }
        }
    }
}