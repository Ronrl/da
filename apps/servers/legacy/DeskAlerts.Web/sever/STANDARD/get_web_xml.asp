<%
    Response.CacheControl = "no-cache"
    Response.AddHeader "Pragma", "no-cache"
    Response.Expires = -1
 %>
 
<TOOLBAR>
<% db_backup_no_redirect = 1 %>
<!-- #include file="admin/config.inc" -->
<!-- #include file="admin/db_conn.asp" -->
<!-- #include file="admin/libs/templateObj.asp" -->
<!-- #include file="admin/md5.asp" -->
<script language="javascript" runat="server">
    function toUtcString(d) {
        var dDate = new Date(d);
        return Math.round(dDate.getTime() / 1000);
    };
</script>
<%

templateGuid = "23d8575a-9eb8-41ee-9f0a-e3a11dce3fc6"

uname = Replace(Request("uname"),"'","''")
cname = Replace(Request("computer"),"'","''")
desk_id = Replace(Replace(Replace(Request("deskbar_id"),"'","''"),"%7B","{"),"%7D","}")
nextrequest = Request("nextrequest")
localtime = Request("localtime")

if localtime = "" then
	localtime = now_db
end if

Dim XML
user_id = 0
standby = Request("standby")
disable = Request("disable")
clientVersion = Request("client_version")

if disable <> "1" then
	disable = "0"
end if

if standby <> "1" then
	standby = "0"
end if

toolbar=Request("toolbar")

strUserDeskbarId = Replace(Request("deskbar_id"),"'","''")
intUserDeskbarId = 0

if toolbar <> 1 then toolbar = 0 end if

if Request("scheduled") = "" then
	g_scheduled_not_set = true
	g_scheduled = -1
else
	g_scheduled_not_set = false
	g_scheduled = Request("scheduled")
end if

if Request("ticker") = "" then
	g_ticker = -1
else
	g_ticker = Request("ticker")
end if
totalAlertCount = 0

if AD = 3 then
	if EDIR=1 then
		if ConfDisableContextSupport <> 1 then
			context=Request("edir")
			context_id=-1
			if len(context) > 0 then
				Set RS3 = Conn.Execute("SELECT c.id, c.name FROM users u INNER JOIN edir_context c ON u.context_id=c.id WHERE u.name=N'"& uname&"' and u.role='U'")
				do while not RS3.EOF
					strContext=RS3("name")
					strContext=replace(strContext,",ou=",".")
					strContext=replace(strContext,",o=",".")
					strContext=replace(strContext,",dc=",".")
					strContext=replace(strContext,",c=",".")

					strContext=replace(strContext,"ou=","")
					strContext=replace(strContext,"o=","")
					strContext=replace(strContext,"dc=","")
					strContext=replace(strContext,"c=","")
					if LCase(strContext) = LCase(context) then
						context_id=RS3("id")
						exit do
					end if
					RS3.MoveNext
				loop
			elseif ConfEnableADEDCheck = 1 and ConfEnableRegAndADEDMode <> 1 then
				'send alert that client is not in EDIR mode and server is in EDIR mode
				user_id=-1
				setXML
				XML.addCustomAlert false, alerts_folder & "not_edir.asp", 0, 0, 0, 0, "", "", 0, 0, 0, 0, false
			end if
		else
			context_id = -2
		end if
	else
		domain = replace(Request("fulldomain"),"'","''")
		compdomain = replace(Request("compdomain"),"'","''")
		if ConfForceSelfRegWithAD=1 then
			SQL = "SELECT d.name FROM domains d, users u WHERE (u.name='"&uname&"' OR u.display_name='"&uname&"') AND u.domain_id<>1 AND u.domain_id = d.id"
			Set RSMimic = Conn.Execute(SQL)
			if not RSMimic.EOF then
				domain=RSMimic("name")
			end if
		end if
		if compdomain = "" then
			compdomain = domain
		end if
		if domain = "" and ConfEnableADEDCheck = 1 and ConfEnableRegAndADEDMode <> 1 then
			'send alert that client is not in AD mode and server is in AD mode
			user_id = -1
			setXML
			XML.addCustomAlert false, alerts_folder & "not_ad.asp", 0, 0, 0, 0, "", "", 0, 0, 0, 0, false
		end if
	end if
end if

'alerts
if Request("cnt") = "cont" then
	if MOBILE_SEND=1 then
		cnt = 10
	else 
		cnt = 0
	end if
else
	cnt=clng(Request("cnt"))
end if
index = 0

Dim ip : ip = Request("ip")
if (ip = "") then
	ip = Request.ServerVariables("HTTP_X_FORWARDED_FOR")
end if
if (ip = "") then
	ip = Request.ServerVariables("REMOTE_ADDR")
end if
Dim longip, iprange, ippart, ipindex : longip = -1
if ip <> "" then
	ippart = Split(ip, ",")
	ipindex = LBound(ippart)
	do while longip = -1 and ipindex <= UBound(ippart)
		ip = ippart(ipindex)
		iprange = Split(ip, ".")
		if UBound(iprange) = 3 then
			longip = 0
			For j = 0 to 3
				if isNumeric(iprange(j)) then
					if iprange(j) < 0 or iprange(j) > 255 then
						longip = -1
						exit for
					end if
					longip = longip + iprange(j)*(256^(3-j))
				else
					longip = -1
					exit for
				end if
			Next
		end if
		ipindex = ipindex + 1
	loop
end if

alertsSQL = " SET NOCOUNT ON" & vbCrLf &_
			" DECLARE @user_id BIGINT" & vbCrLf &_
			" DECLARE @comp_id BIGINT" & vbCrLf &_
			" DECLARE @context_id BIGINT" & vbCrLf &_
			" DECLARE @comp_domain NVARCHAR (255)" & vbCrLf &_
			" DECLARE @cur_date DATETIME" & vbCrLf &_
			" DECLARE @user_date DATETIME" & vbCrLf &_
			" DECLARE @user_name NVARCHAR (255)" & vbCrLf &_
			" DECLARE @user_domain NVARCHAR (255)" & vbCrLf &_
			" DECLARE @comp_name NVARCHAR (255)" & vbCrLf &_
			" DECLARE @next_request VARCHAR (255)" & vbCrLf &_
			" DECLARE @clsid VARCHAR (255)" & vbCrLf &_
			" DECLARE @ip BIGINT" & vbCrLf &_
			" DECLARE @now DATETIME" &  vbCrLf &_
			" DECLARE @client_version VARCHAR (50)" & vbCrLf &_
			" SET @context_id = '"& Replace(context_id,"'","''") &"'"& vbCrLf &_
			" SET @user_domain = N'"& domain &"'"& vbCrLf &_
			" SET @comp_domain = N'"& compdomain &"'"& vbCrLf &_
			" SET @user_name = N'"& uname &"'"& vbCrLf &_
			" SET @comp_name = N'"& cname &"'"& vbCrLf &_
			" SET @next_request = '"& Replace(nextrequest,"'","''") &"'"& vbCrLf &_
			" SET @clsid = '"& Replace(desk_id,"'","''") &"'"& vbCrLf &_
			" SET @ip = "& longip & vbCrLf &_
			" SET @now = '"& Replace(localtime,"|"," ") &"'"& vbCrLf &_
			" SET @cur_date = GETDATE()" & vbCrLf &_
			" 	DECLARE @timezone INT" & vbCrLf &_
	        " 	SET @timezone = DATEDIFF(Minute,@now,GETUTCDATE())" & vbCrLf &_
			" " & vbCrLf &_
			" IF(@context_id > 0)" & vbCrLf &_
			" 	SELECT @user_id = u.id, @user_date = u.reg_date" & vbCrLf &_
			" 	FROM   users u WITH (READPAST)" & vbCrLf &_
			" 	WHERE u.name = @user_name AND role = 'U'" & vbCrLf &_
			" 		 AND u.context_id = @context_id" & vbCrLf &_
			" ELSE IF(@user_domain IS NULL OR @user_domain = N'')" & vbCrLf &_
			" 	SELECT @user_id = u.id, @user_date = u.reg_date" & vbCrLf &_
			" 	FROM   users u WITH (READPAST)" & vbCrLf &_
			" 		 LEFT JOIN domains d WITH (READPAST)" & vbCrLf &_
			" 		   ON d.id = u.domain_id" & vbCrLf &_
			" 	WHERE  (d.name = N'' OR u.domain_id IS NULL)" & vbCrLf &_
			" 		 AND u.name = @user_name AND role = 'U'" & vbCrLf &_
			" ELSE" & vbCrLf &_
			" 	SELECT @user_id = u.id, @user_date = u.reg_date" & vbCrLf &_
			" 	FROM   users u WITH (READPAST)" & vbCrLf &_
			" 		 JOIN domains d WITH (READPAST)" & vbCrLf &_
			" 		   ON d.id = u.domain_id" & vbCrLf &_
			" 	WHERE  d.name = @user_domain" & vbCrLf &_
			" 		 AND u.name = @user_name AND role = 'U'" & vbCrLf &_
			" " & vbCrLf &_
			" IF(@user_id > 1)" & vbCrLf &_
			" 	UPDATE users" & vbCrLf &_
			" 	SET  last_request = @cur_date," & vbCrLf &_
			" 		 next_request = @next_request," & vbCrLf &_
			" 		 standby = " & standby & "," & vbCrLf &_
			" 		 disabled = " & disable & "," & vbCrLf &_
			"		 client_version = '"& Replace(clientVersion,"'","''") &"'" & vbCrLf &_
			" 	WHERE  id = @user_id" & vbCrLf &_
			" " & vbCrLf &_
			" SELECT @user_id as user_id" & vbCrLf &_
			" " & vbCrLf &_
			" IF(@user_id > 1)" & vbCrLf &_
			" BEGIN" & vbCrLf &_
			" 	IF(@comp_domain IS NULL OR @comp_domain = N'')" & vbCrLf &_
			" 		SELECT @comp_id = comp.id" & vbCrLf &_
			" 		FROM   computers comp WITH (READPAST)" & vbCrLf &_
			" 		INNER JOIN domains d WITH (READPAST)" & vbCrLf &_
			" 			   ON d.id = comp.domain_id" & vbCrLf &_
			" 		WHERE  (d.name = N'' OR comp.domain_id IS NULL)" & vbCrLf &_
			" 			 AND comp.name = @comp_name;" & vbCrLf &_
			" 	ELSE" & vbCrLf &_
			" 		SELECT @comp_id = comp.id" & vbCrLf &_
			" 		FROM computers comp WITH (READPAST)" & vbCrLf &_
			" 		INNER JOIN domains d WITH (READPAST)" & vbCrLf &_
			" 		ON d.id = comp.domain_id" & vbCrLf &_
			" 		WHERE d.name = @comp_domain" & vbCrLf &_
			" 			AND comp.name = @comp_name;" & vbCrLf &_
			" " & vbCrLf &_
			" 	IF(@comp_id > 1)" & vbCrLf &_
			" 		UPDATE computers" & vbCrLf &_
			" 		SET last_request = @cur_date," & vbCrLf &_
			" 			 next_request = @next_request" & vbCrLf &_
			" 		WHERE  id = @comp_id;" & vbCrLf &_
			" " & vbCrLf &_
	" 	SELECT DISTINCT a.id," & vbCrLf &_
	" 			a.aknown," & vbCrLf &_
	" 			a.autoclose," & vbCrLf &_
	" 			DATEADD(Minute, @timezone, a.create_date) AS create_date," & vbCrLf &_
	" 			DATEADD(Minute, @timezone, a.sent_date) AS sent_date," & vbCrLf &_
	" 			a.recurrence," & vbCrLf &_
	" 			a.ticker," & vbCrLf &_
	" 			a.urgent," & vbCrLf &_
	" 			a.schedule," & vbCrLf &_
	" 			a.schedule_type," & vbCrLf &_
	" 			a.fullscreen," & vbCrLf &_
	" 			a.alert_width," & vbCrLf &_
	" 			a.alert_height," & vbCrLf &_
	" 			a.title," & vbCrLf &_
	" 			a.from_date," & vbCrLf &_
	" 			a.to_date," & vbCrLf &_
	" 			a.class," & vbCrLf &_
	" 			a.desktop," & vbCrLf &_
	" 			a.resizable," & vbCrLf &_
	" 			ac.file_name as caption_file_name," & vbCrLf &_
	" 			ac.id as skin_id," & vbCrLf &_
	" 			rc.pattern as rec_pattern," & vbCrLf &_
	" 			sv.id as survey_id" & vbCrLf &_
	" 	FROM" & vbCrLf &_
	" 	(" & vbCrLf &_
	" 		SELECT DISTINCT alert_id, from_date, recur_id" & vbCrLf &_
	" 		FROM" & vbCrLf &_
	" 		(" & vbCrLf &_
	" 			SELECT alert_id, from_date, recur_id" & vbCrLf &_
	" 			FROM alerts_cache_users WITH (READPAST)" & vbCrLf &_
	" 			WHERE (user_id = @user_id OR user_id = 0)" & vbCrLf &_
	" 				AND from_date <= @now AND @now <= to_date" & vbCrLf &_
	" 		UNION ALL" & vbCrLf &_
	" 			SELECT alert_id, from_date, recur_id" & vbCrLf &_
	" 			FROM alerts_cache_comps WITH (READPAST)" & vbCrLf &_
	" 			WHERE comp_id = @comp_id" & vbCrLf &_
	" 				AND from_date <= @now AND @now <= to_date" & vbCrLf &_
	" 		UNION ALL" & vbCrLf &_
	" 			SELECT alert_id, from_date, recur_id" & vbCrLf &_
	" 			FROM alerts_cache_ips WITH (READPAST)" & vbCrLf &_
	" 			WHERE ip_from <= @ip" & vbCrLf &_
	" 				AND ip_to >= @ip" & vbCrLf &_
	" 				AND from_date <= @now AND @now <= to_date" & vbCrLf &_
	" 		) sent" & vbCrLf &_
	" 	) s" & vbCrLf &_
	" 	INNER JOIN alerts a WITH (READPAST)" & vbCrLf &_
	" 		ON a.id = s.alert_id" & vbCrLf &_
	" 		AND a.class = 999" & vbCrLf &_
	" 	LEFT JOIN recurrence rc WITH (READPAST)" & vbCrLf &_
	" 		ON rc.id = s.recur_id AND rc.alert_id = a.id" & vbCrLf &_
	" 	LEFT JOIN alerts_received r WITH (READPAST)" & vbCrLf &_
	" 		ON a.id = r.alert_id" & vbCrLf &_
	" 			AND r.user_id = @user_id" & vbCrLf &_
	" 			AND (r.clsid = @clsid OR r.clsid = '' OR @clsid = '')" & vbCrLf &_
	" 			AND (r.date >= s.from_date OR (rc.end_type = 'end_after' AND rc.occurences <= r.occurrence))" & vbCrLf &_
	" 	LEFT JOIN alert_captions ac WITH (READPAST)" & vbCrLf &_
	" 		ON ac.id = a.caption_id" & vbCrLf &_
	" 	LEFT JOIN surveys_main sv WITH (READPAST)" & vbCrLf &_
	" 		ON sv.sender_id = a.id" & vbCrLf &_
	" 	WHERE a.sent_date >= @user_date" & vbCrLf &_
	" 		AND r.alert_id IS NULL" & vbCrLf &_
	" 	ORDER BY a.urgent DESC, a.id" & vbCrLf &_
			" END" & vbCrLf &_
			" ELSE" & vbCrLf &_
			" BEGIN" & vbCrLf &_
			" 	SELECT -1 as id," & vbCrLf &_
			" 			'0' as aknown," & vbCrLf &_
			" 			'0' as autoclose," & vbCrLf &_
			" 			GETUTCDATE() as create_date," & vbCrLf &_
			" 			GETUTCDATE() as sent_date," & vbCrLf &_
			" 			'0' as recurrence," & vbCrLf &_
			" 			'0' as ticker," & vbCrLf &_
			" 			'0' as urgent," & vbCrLf &_
			" 			'0' as schedule," & vbCrLf &_
			" 			'0'  as schedule_type," & vbCrLf &_
			" 			'2'  as fullscreen," & vbCrLf &_
			" 			'500'  as alert_width," & vbCrLf &_
			" 			'400'  as alert_height," & vbCrLf &_
			" 			'Registration'  as title," & vbCrLf &_
			" 			''  as from_date," & vbCrLf &_
			" 			''  as to_date," & vbCrLf &_
			"			'1' as class" & vbCrLf &_ 
			" END "
			
			'Response.Write alertsSQL
set RSalerts = Conn.Execute(alertsSQL)
secondary = 0
user_id=-1

if Not RSalerts.EOF then
	if VarType(RSalerts("user_id")) > 1 then
		user_id = RSalerts("user_id")
	end if
	Set RSalerts = RSalerts.NextRecordset()
end if

if RSalerts.EOF then
    alertsSQL = "SELECT a.alert_id as id FROM alerts_sent a INNER JOIN users u ON a.user_id=u.id AND u.name=N'"&uname&"'"
    set RSalerts = Conn.Execute(alertsSQL)
    id_alert = ""
    
    idString = ""
   if not  RSalerts.EOF then 
    do while not RSalerts.EOF
        idString = idString & RSalerts("id") & ","
        RSAlerts.movenext
    loop
       idString = Left(idString, Len(idString) - 1) 'remove last comma
       
     query = "SELECT a.id, a.aknown, a.autoclose, DATEADD(Minute, DATEDIFF(Minute,'"& Replace(localtime,"|"," ") &"',GETUTCDATE()), a.create_date) AS create_date, DATEADD(Minute, DATEDIFF(Minute,'"& Replace(localtime,"|"," ") &"',GETUTCDATE()), a.sent_date) AS sent_date, a.recurrence, a.ticker, a.urgent, a.schedule, a.schedule_type, a.fullscreen, a.alert_width, a.alert_height, a.title, a.from_date, a.to_date, a.class, a.desktop, a.resizable FROM alerts a WHERE a.id IN ("&idString&") AND (a.approve_status = 1 OR a.approve_status IS NULL) AND a.schedule_type <> 0 ORDER BY a.create_date ASC"
   
    set RSalerts = Conn.Execute(query)   
    
   end if 
   ' if Not RSalerts.EOF then 
    '    id_alert = RSalerts("id")
   ' query = "SELECT a.id, a.aknown, a.autoclose, DATEADD(Minute, DATEDIFF(Minute,'"& Replace(localtime,"|"," ") &"',GETUTCDATE()), a.create_date) AS create_date, DATEADD(Minute, DATEDIFF(Minute,'"& Replace(localtime,"|"," ") &"',GETUTCDATE()), a.sent_date) AS sent_date, a.recurrence, a.ticker, a.urgent, a.schedule, a.schedule_type, a.fullscreen, a.alert_width, a.alert_height, a.title, a.from_date, a.to_date, a.class, a.desktop, a.resizable FROM alerts a WHERE a.id="&id_alert&""
   ' set RSalerts = Conn.Execute(query)
   ' end if    
    

    
    secondary = 1
end if

if (not RSalerts is Nothing) then
    
	if Not RSalerts.EOF then
		setXML
	end if

	wallpapersHash=""
	screensaversHash=""
	do while not RSalerts.EOF
		recurrence=RSalerts("recurrence")
		alert_id=RSalerts("id")
		if ColumnExists(RSalerts, "class") then
			classid = RSalerts("class")&""
		else
			classid = "1"
		end if
		
		survey_id = ""
		if ColumnExists(RSalerts, "survey_id") then
			survey_id = RSalerts("survey_id")
			if (survey_id) and classid = "1" then
				classid = "32"
			end if	
		end if
		
		if clng(alert_id) = -1 then
			if ConfEnableRegAndADEDMode=1 and g_ticker<>1 then
				'XML.addCustomAlert false, alerts_folder & "after_install.asp?deskbar_id="&strUserDeskbarId, 0, 0, 0, 0, "Registration", "center", "", "", 0, 0, false
			end if
			Exit Do
		end if
		schedule = RSalerts("schedule")
		schedule_type = RSalerts("schedule_type")
			
		'for not stopped schedule alerts (checking schedule_type)
        recurrenceIsNull = False
        If IsNull(recurrence) Then
            recurrenceIsNull=True
        else
            recurrenceIsNull=False
        End If

        
		if (((recurrence="1" or recurrenceIsNull=True) or schedule="1") and rsalerts("schedule_type")="1" and (g_scheduled=1 or g_scheduled_not_set)) or ((recurrence<>"1" or recurrenceIsNull=True) and schedule<>"1" and (g_scheduled=0 or g_scheduled_not_set)) then
			
			if classid = "2" then
				if screensaversHash<>"" then
					screensaversHash = screensaversHash & ","
				end if
				screensaversHash = screensaversHash & alert_id
			elseif classid = "8" then
				if wallpapersHash<>"" then
					wallpapersHash = wallpapersHash & ","
				end if
				wallpapersHash = wallpapersHash & alert_id
			end if

			if(urgent<>"1") then totalAlertCount=totalAlertCount+1 end if
				urgent = RSalerts("urgent")
				ticker = RSalerts("ticker")
			if ColumnExists(RSalerts, "desktop") then
				desktop = RSalerts("desktop")
			else
				desktop = "1"
			end if
			if (g_ticker=-1 or ticker=g_ticker) and ((urgent="1") or (index < cnt)) then
				acknown=RSalerts("aknown")
				autoclose=0
				if ColumnExists(RSalerts, "autoclose") then
					if not IsNull(RSalerts("autoclose")) then
						autoclose=Clng(RSalerts("autoclose"))
					end if
				end if
				create_date=RSalerts("sent_date")
				 Dim myObj, MyDate
					    MyDate = CDate(create_date)
					    Set myObj = CreateObject( "WbemScripting.SWbemDateTime" )
					    myObj.Year = Year(MyDate)
                        myObj.Month = Month(MyDate)
                        myObj.Day = Day(MyDate)
                        myObj.Hours = Hour(MyDate)
                        myObj.Minutes = Minute(MyDate)
                        myObj.Seconds = Second(MyDate)					    
					    UTCdate = myObj.GetVarDate(false)
				create_timestamp=toUtcString(MyDate)
				alert_title=RSalerts("title")

				LoadTemplateFromString "title"&templateGuid, alert_title
				
				SetVar "time_till", ""
				if ColumnExists(RSalerts, "rec_pattern") then
					if RSalerts("rec_pattern") = "c" then
						time_till = DateDiff("n",now_db,RSalerts("from_date"))
						SetVar "time_till", time_till& " minutes till "
					end if
				end if
				
				Parse "title"&templateGuid, False
				alert_title = PrintVar("title"&templateGuid)
				
				alert_width = RSalerts("alert_width")
				alert_height = RSalerts("alert_height")
				
				position = ""
				ticker_position = ""
				if ticker = "1" then
					alert_width = ""
					alert_height = ""
					position = ""
					if ColumnExists(RSalerts, "fullscreen") then
						if RSalerts("fullscreen") = 7 then
							ticker_position = "top"
						elseif RSalerts("fullscreen") = 8 then
							ticker_position = "middle"
						elseif RSalerts("fullscreen") = 9 then
							ticker_position = "bottom"
						else ticker_position = "bottom"
						end if
					end if
				elseif ColumnExists(RSalerts, "fullscreen") then
					if RSalerts("fullscreen") = 1 then
						position = "fullscreen"
					elseif RSalerts("fullscreen") = 2 then
						position = "left-top"
					elseif RSalerts("fullscreen") = 3 then
						position = "right-top"
					elseif RSalerts("fullscreen") = 4 then
						position = "center"
					elseif RSalerts("fullscreen") = 5 then
						position = "left-bottom"
					elseif RSalerts("fullscreen") = 6 then
						position = "right-bottom"
					else position = "right-bottom"
					end if
				end if
				
				innerXML = ""
				if ColumnExists(RSalerts, "caption_file_name") then
					if RSalerts("caption_file_name") <> "" then
						innerXML = "<WINDOW captionhref="""&alerts_folder&"admin/skins/default/version/"&RSalerts("caption_file_name")+""" />"
					end if
				end if
				if ColumnExists(RSalerts, "skin_id") then
					skin_id = RSalerts("skin_id")
					if skin_id <> "" then
						
						if ticker = "1" then
							captionFile = alerts_folder&"admin/skins/"& skin_id &"/version/tickercaption.html"
							jsFile = alerts_folder&"admin/skins/"& skin_id &"/version/ticker.js"
						else
							captionFile = alerts_folder&"admin/skins/"& skin_id &"/version/alertcaption.html"
							jsFile = alerts_folder&"admin/skins/"& skin_id &"/version/alert.js"
						end if
						
						innerXML = "<WINDOW skin_id="""& skin_id &""" "
						
						'on error resume next
						on error goto 0
						set objXML = Server.CreateObject("Msxml2.DOMDocument.6.0")
						confPath = "admin/skins/"& skin_id &"/blank/conf.xml"
						objXML.Load(Server.MapPath(confPath))
						if objXML.parseError.errorCode = 0 then
							alertWindowName = "alertwindow1"
							tickerWindowName = "tickerwindow1"
							set windows = objXML.selectNodes("/ALERTS/COMMANDS/ALERT/WINDOW")
							for each window in windows
								if LCase(Replace(Replace(Replace(window.text, " ", ""),"""",""),"'","")) = "#ticker#==0" then
									alertWindowName = window.attributes.getNamedItem("name").value
								elseif LCase(Replace(Replace(Replace(window.text, " ", ""),"""",""),"'","")) = "#ticker#==1" then
									tickerWindowName = window.attributes.getNamedItem("name").value
								end if
							next
							if ticker = "1" then
								set alertWindow = objXML.selectSingleNode("/ALERTS/VIEWS/WINDOW[@name='"&tickerWindowName&"']")
							else
								set alertWindow = objXML.selectSingleNode("/ALERTS/VIEWS/WINDOW[@name='"&alertWindowName&"']")
							end if
							if IsObject(alertWindow) then
								bodyTopMargin = CLng(alertWindow.attributes.getNamedItem("topmargin").value)
								bodyBottomMargin = CLng(alertWindow.attributes.getNamedItem("bottommargin").value)
								bodyLeftMargin = CLng(alertWindow.attributes.getNamedItem("leftmargin").value)
								bodyRightMargin = CLng(alertWindow.attributes.getNamedItem("rightmargin").value)
								
								captionFile = Replace(alertWindow.attributes.getNamedItem("captionhref").value, "\", "/")
								jsFile = Replace(alertWindow.attributes.getNamedItem("customjs").value, "\", "/")
								
								captionFile = Replace(captionFile, "%root_path%", alerts_folder&"admin/skins/"& skin_id &"/version")
								jsFile = Replace(jsFile, "%root_path%", alerts_folder&"admin/skins/"& skin_id &"/version")

								innerXML = innerXML & "leftmargin="""& bodyLeftMargin &""" topmargin="""& bodyTopMargin &""" rightmargin="""& bodyRightMargin &""" bottommargin="""& bodyBottomMargin &""" "
							end if
						end if
						set objXML = nothing
						on error goto 0
						innerXML = innerXML & "captionhref="""& captionFile &""" customjs="""& jsFile &""" />"
					end if
				end if
				
				resizable = false
				if ColumnExists(RSalerts, "resizable") then
					if RSalerts("resizable") then
						resizable = true
					end if
				end if
				
				added = false
				if (Request("survey") = "" or Request("survey") = "0") and _
					(classid <> "2" and classid <> "4" and classid <> "8" and index < cnt and (desktop = "" or desktop = "1")) and _
					(classid <> "64" and classid <> "128" and classid <> "256") then
					if disable="1" then
						classId="16"
					end if
					if jsCheckProperty(XML, "addAlert4") then
					   
						Set properties=Server.CreateObject("Scripting.Dictionary")
						properties.Add "urgent", urgent
						properties.Add "alert_id", alert_id
						properties.Add "acknown", acknown
						properties.Add "autoclose", autoclose
						properties.Add "create_date", create_timestamp
						properties.Add "title", alert_title
						properties.Add "position", position
						properties.Add "visible", ""
						properties.Add "width", alert_width
						properties.Add "height", alert_height
						properties.Add "ticker", ticker
						properties.Add "ticker_position", ticker_position
						properties.Add "schedule", schedule
						properties.Add "schedule_type", schedule_type
						properties.Add "recurrence", recurrence
						properties.Add "history", true
						properties.Add "class_id", classId
						properties.Add "resizable", resizable
						added = XML.addAlert4(properties, innerXML)
					elseif jsCheckProperty(XML, "addAlert3") then
						added = XML.addAlert3(urgent, alert_id, aknown, autoclose, create_timestamp, alert_title, position, "", alert_width, alert_height, ticker, schedule, recurrence, true, clng(classId), innerXML)
					elseif jsCheckProperty(XML, "addAlert2") then
						added = XML.addAlert2(urgent, alert_id, aknown, autoclose, create_timestamp, alert_title, position, "", alert_width, alert_height, ticker, schedule, recurrence, true, innerXML)
					else
						XML.addAlert urgent, alert_id, aknown, autoclose, create_timestamp, alert_title, position, alert_width, alert_height, ticker, schedule, true
						added = true
					end if
				elseif (Request("survey") = "" or Request("survey") = "1") and (g_ticker=-1 OR g_ticker=0) and (disable <> "1") and (index < cnt) and _
					(classid = "64" or classid = "128" or classid = "256") then
					if jsCheckProperty(XML, "addSurvey3") then
						added = XML.addSurvey3(false, survey_id, create_timestamp, alert_title, "", "", "", "", schedule, 0, true, clng(classId), "")
					elseif jsCheckProperty(XML, "addSurvey2") then
						added = XML.addSurvey2(false, survey_id, create_timestamp, alert_title, "", "", "", "", schedule, 0, true, "")
					else
						XML.addSurvey false, survey_id, create_timestamp, alert_title, schedule, true
						added = true
					end if
				end if
				'-----------------
				if added and clng(user_id) > 0 then
					Set RSreceived = Conn.Execute("SELECT COUNT(1) as cnt FROM alerts_received WHERE user_id = " & CStr(user_id) & " AND clsid = '" & desk_id & "' AND alert_id = " & alert_id)
					if not RSreceived.EOF then
						occurrence = RSreceived("cnt") + 1
					else
						occurrence = 1
					end if
					if secondary <> 1 then
					    Conn.Execute("INSERT INTO alerts_received (user_id, clsid, alert_id, [date], occurrence) VALUES (" & CStr(user_id) & ", '"& desk_id &"', " & alert_id & ", '"&Replace(localtime,"|"," ")&"', " & occurrence & ")")
					end if
					index=index+1
				end if
				'-----------------
			end if
		end if
		RSalerts.MoveNext
	loop
end if

if(disable="1") then
	Response.Write "<ALERTCOUNT count="""& totalAlertCount &"""/>"
end if

if not isEmpty(XML) then
	Response.Write XML.getResult()
end if

if (screensaversHash<>"") then
	Response.Write "<SCREENSAVERS hash="""&md5(screensaversHash)&"""/>"
end if

if (wallpapersHash<>"") then
	Response.Write "<WALLPAPERS hash="""&md5(wallpapersHash)&"""/>"
end if

XML = null
%>
<!-- #include file="admin/db_conn_close.asp" -->
</TOOLBAR>