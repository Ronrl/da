﻿using System;
using System.Collections;
using System.IO;
using DeskAlertsDotNet.Parameters;

namespace DeskAlertsDotNet.Utils
{

    public sealed class CrashReportManager
    {
        // All methods are static, so this can be private
        private CrashReportManager()
        {
        }


        private static readonly string reportFormat = ".txt";
        private static readonly string reportLink = "/admin/logs/";
        private static readonly string reportPath = "\\admin\\logs\\";
        // Log an Exception

        public static string BuildCrashReportPath(string identifier)
        {
            return Config.Data.GetString("alertsFolder") + reportPath + "crashreport_" +
                        identifier + reportFormat;
        }

        public static string BuildCrashReportLink(string identifier)
        {
            return Config.Data.GetString("alertsDir") + reportLink + "crashreport_" +
                        identifier + reportFormat;
        }

        public static string ReadCrashReport(string identifier)
        {


            return File.ReadAllText(BuildCrashReportPath(identifier));
        }


        public static void GenerateRecieveAlertsCrashReport(Exception ex, string url)
        {
            string crashLogPath = BuildCrashReportPath("recieving");

            using (StreamWriter sw = new StreamWriter(crashLogPath, false))
            {
                sw.Write("******************** " + DateTime.Now);
                sw.WriteLine(" ********************");

                if (url != null)
                    sw.WriteLine("URL: " + url);
                if (ex != null)
                {
                if (ex.InnerException != null)
                {
                    sw.Write("Inner Exception Type: ");
                    sw.WriteLine(ex.InnerException.GetType().ToString());
                    sw.Write("Inner Exception: ");
                    sw.WriteLine(ex.InnerException.Message);
                    sw.Write("Inner Source: ");
                    sw.WriteLine(ex.InnerException.Source);
                    if (ex.InnerException.StackTrace != null)
                    {
                        sw.WriteLine("Inner Stack Trace: ");
                        sw.WriteLine(ex.InnerException.StackTrace);
                    }

                    if (ex.InnerException.Data != null && ex.InnerException.Data.Count > 0)
                    {
                        sw.WriteLine("******************** Extras Data ********************");
                        foreach (DictionaryEntry dictionaryEntry  in ex.InnerException.Data)
                        {
                            sw.WriteLine("key: " + dictionaryEntry.Key + " value: " + dictionaryEntry.Value);
                        }
                    }
                }
                sw.Write("Exception Type: ");
                sw.WriteLine(ex.GetType().ToString());
                sw.WriteLine("Exception: " + ex.Message);
                sw.WriteLine("Source: " + ex.Source);
                sw.WriteLine("Stack Trace: ");
                if (ex.StackTrace != null)
                    sw.WriteLine(ex.StackTrace);
                }

                if (ex.Data != null && ex.Data.Count > 0)
                {
                    sw.WriteLine("******************** Extras Data ********************");
                    foreach (DictionaryEntry dictionaryEntry in ex.Data)
                    {
                        sw.WriteLine("key: " + dictionaryEntry.Key + " value: " + dictionaryEntry.Value);
                    }
                }

                sw.WriteLine();
                sw.Close();
            }
        }
        public static string GenerateCrashReport(Exception ex, string url = null)
        {
            // Include enterprise logic for logging exceptions
            // Get the absolute path to the log file

            string dateTime = DateTime.Now.ToString("dd-MM-yyyy HH-mm-ss");
            string crashLogPath = BuildCrashReportPath(dateTime);

            using (StreamWriter sw = new StreamWriter(crashLogPath, true))
            {
                sw.Write("******************** " + DateTime.Now);
                sw.WriteLine(" ********************");
                if (url != null)
                    sw.WriteLine("URL: " + url);
                if (ex != null)
                {
                    if (ex.InnerException != null)
                    {
                        sw.Write("Inner Exception Type: ");
                        sw.WriteLine(ex.InnerException.GetType().ToString());
                        sw.Write("Inner Exception: ");
                        sw.WriteLine(ex.InnerException.Message);
                        sw.Write("Inner Source: ");
                        sw.WriteLine(ex.InnerException.Source);
                        if (ex.InnerException.StackTrace != null)
                            sw.WriteLine("Inner Stack Trace: ");
                        sw.WriteLine(ex.InnerException.StackTrace);

                        if (ex.InnerException.Data != null && ex.InnerException.Data.Count > 0)
                        {
                            sw.WriteLine("******************** Extras Data ********************");
                            foreach (DictionaryEntry dictionaryEntry in ex.InnerException.Data)
                            {
                                sw.WriteLine("key: " + dictionaryEntry.Key + " value: " + dictionaryEntry.Value);
                            }
                        }

                    }
                    sw.Write("Exception Type: ");
                    sw.WriteLine(ex.GetType().ToString());
                    sw.WriteLine("Exception: " + ex.Message);
                    sw.WriteLine("Source: " + ex.Source);
                    sw.WriteLine("Stack Trace: ");
                    if (ex.StackTrace != null)
                        sw.WriteLine(ex.StackTrace);

                    if (ex.Data != null && ex.Data.Count > 0)
                    {
                        sw.WriteLine("******************** Extras Data ********************");
                        foreach (DictionaryEntry dictionaryEntry in ex.Data)
                        {
                            sw.WriteLine("key: " + dictionaryEntry.Key + " value: " + dictionaryEntry.Value);
                        }
                    }

                }
                sw.WriteLine();
                sw.Close();
            }

            return dateTime;
        }



        // Notify System Operators about an exception
        public static void NotifySystemOps(Exception exc)
        {
            // Include code for notifying IT system operators
        }
    }
}