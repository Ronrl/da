﻿using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Parameters;
using DeskAlertsDotNet.Utils.Consts;
using DeskAlertsDotNet.Utils.Factories;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Security.Cryptography;
using System.Text;

// ReSharper disable once CheckNamespace
// While we have bad project file structure, we can't move namespaces
namespace DeskAlertsDotNet.sever.STANDARD.Utils
{
    public class TokenUtils
    {
        private const string Secret = "NTJwZXJTZWM4ZXREZXNrU2xlcjdzOWV5";

        public string GenerateToken(string userName)
        {
            var header = new Dictionary<string, object>
            {
                {"typ", "JWT"},
                {"alg", "HS256"}
            };

            var time = DateTime.Now;

            var payload = new Dictionary<string, object>
            {
                {"usr", userName},
                {"atm", time}
            };

            var segments = new List<string>(3);
            var headerBytes = GetBytes(JsonConvert.SerializeObject(header));
            var payloadBytes = GetBytes(JsonConvert.SerializeObject(payload));

            segments.Add(Encode(headerBytes));
            segments.Add(Encode(payloadBytes));

            var stringToSign = String.Join(".", segments.ToArray());
            var bytesToSign = GetBytes(stringToSign);

            var signature = Sign(GetBytes(Secret), bytesToSign);
            segments.Add(Encode(signature));
            var token = String.Join(".", segments.ToArray());
            var tools = new TokenTools();

            tools.AddToken(token, userName, time.ToString(DateTimeFormat.JavaScript, CultureInfo.InvariantCulture));

            return token;
        }

        private byte[] Sign(byte[] key, byte[] bytesToSign)
        {
            using (var sha = new HMACSHA256(key))
            {
                return sha.ComputeHash(bytesToSign);
            }
        }

        private string Encode(byte[] input)
        {
            if (input == null)
                throw new ArgumentNullException(nameof(input));
            if (input.Length == 0)
                throw new ArgumentOutOfRangeException(nameof(input));

            var output = Convert.ToBase64String(input);
            output = output.Split('=')[0]; // Remove any trailing '='s
            output = output.Replace('+', '-'); // 62nd char of encoding
            output = output.Replace('/', '_'); // 63rd char of encoding
            return output;
        }

        public string Decode(string token)
        {
            var parts = token.Split('.');
            var payload = parts[1];
            var decoded = UrlDecode(payload);
            return Encoding.UTF8.GetString(decoded);
        }

        public byte[] UrlDecode(string input)
        {
            if (String.IsNullOrWhiteSpace(input))
                throw new ArgumentException(nameof(input));

            var output = input;
            output = output.Replace('-', '+'); // 62nd char of encoding
            output = output.Replace('_', '/'); // 63rd char of encoding
            switch (output.Length % 4) // Pad with trailing '='s
            {
                case 0:
                    break; // No pad chars in this case
                case 2:
                    output += "==";
                    break; // Two pad chars
                case 3:
                    output += "=";
                    break; // One pad char
                default:
                    throw new FormatException("Illegal base64url string.");
            }
            var converted = Convert.FromBase64String(output); // Standard base64 decoder
            return converted;
        }

        private static byte[] GetBytes(string input) => Encoding.UTF8.GetBytes(input);
    }

    public class TokenTools
    {
        private DBManager dbManager;

        private int expirationTime;
        private int tokensCountForServer;
        private int tokensCountForUser;

        private bool expirationTimeEnable;
        private bool tokensCountForServerEnable;
        private bool tokensCountForUserEnable;

        public TokenTools()
        {
            dbManager = new DBManager();
            expirationTime = int.Parse(Settings.Content["ConfTokenExpirationTime"]);
            tokensCountForServer = int.Parse(Settings.Content["ConfTokenMaxCountForServer"]);
            tokensCountForUser = int.Parse(Settings.Content["ConfTokenMaxCountForUser"]);

            expirationTimeEnable = int.Parse(Settings.Content["ConfTokenExpirationTimeEnable"]) == 1;
            tokensCountForServerEnable = int.Parse(Settings.Content["ConfTokenMaxCountForServerEnable"]) == 1;
            tokensCountForUserEnable = int.Parse(Settings.Content["ConfTokenMaxCountForUserEnable"]) == 1;
        }

        public bool IsTokenExists(string token)
        {
            var tokens = dbManager
                .GetDataByQuery(String.Format("SELECT * FROM jwt_sessions WHERE token_value = '{0}'", token)).Count;
            return tokens > 0;
        }

        public bool IsTokenExistForUser(int userId)
        {
            var tokens = dbManager
                .GetDataByQuery(String.Format("SELECT * FROM jwt_sessions WHERE user_id = '{0}'", userId));
            if (tokens.Count <= 0) return false;
            DeleteOldToken(tokens);
            return true;
        }

        public bool IsTokenExpired(string token)
        {
            if (!IsTokenExists(token) || !expirationTimeEnable)
            {
                return false;
            }

            string time = dbManager.GetDataByQuery($"SELECT creation_time FROM jwt_sessions WHERE token_value = '{token}'")
                .GetValue(0, "creation_time").ToString();
            double minutes = DateTime.ParseExact(time, DateTimeFormat.JavaScript, CultureInfo.InvariantCulture).Subtract(DateTime.Now).TotalMinutes;

            bool result = Math.Abs(minutes) > expirationTime;

            return result;
        }

        public void CheckTokensCountForUser(int userId)
        {
            if (!tokensCountForUserEnable) return;
            var tokens = dbManager
                .GetDataByQuery($"SELECT * FROM jwt_sessions WHERE user_id = '{userId}'");
            if (tokens.Count > tokensCountForUser)
            {
                DeleteOldToken(tokens);
            }
        }

        private void DeleteOldToken(DataSet tokens)
        {
            var oldToken = tokens.First();

            if (!DateTime.TryParseExact(oldToken.GetValue("creation_time").ToString(), DateTimeFormat.JavaScript, CultureInfo.InvariantCulture, DateTimeStyles.None, out var oldTokenTime))
            {
                throw new Exception("Can't parse " + oldToken.GetValue("creation_time") + " as DateTime");
            }
            
            foreach (var tokenRow in tokens)
            {
                if (!DateTime.TryParseExact(tokenRow.GetValue("creation_time").ToString(), DateTimeFormat.JavaScript, CultureInfo.InvariantCulture, DateTimeStyles.None, out var tokenTime))
                {
                    throw new Exception("Can't parse " + tokenRow.GetValue("creation_time") + " as DateTime");
                }

                if (DateTime.Compare(tokenTime, oldTokenTime) >= 0) continue;

                oldToken = tokenRow;
                oldTokenTime = tokenTime;
            }
            var token = oldToken.GetValue("token_value");
            DeleteToken(token.ToString());
        }

        public bool CheckTokensCount()
        {
            if (!tokensCountForServerEnable) return true;
            var tokens =
                dbManager.GetDataByQuery(
                    "SELECT * FROM jwt_sessions j INNER JOIN users u ON j.user_id = u.id " +
                    "INNER JOIN policy_editor pe ON pe.editor_id = u.id " +
                    "INNER JOIN policy p ON p.id = pe.policy_id " +
                    "WHERE u.role != 'A' AND p.type != 'A'");
            var tokensCount = tokens.Count;
            if (expirationTimeEnable)
            {
                foreach (var token in tokens.ToList())
                {
                    if (!DateTime.TryParseExact(token.GetValue("creation_time").ToString(), DateTimeFormat.JavaScript, CultureInfo.InvariantCulture , DateTimeStyles.None ,out var tokenTime))
                    {
                        throw new Exception("Can't parse " + token.GetValue("creation_time") + " as DateTime");
                    }

                    var minutes = Convert.ToDateTime(tokenTime).Subtract(DateTime.Now).TotalMinutes;
                    if (!(Math.Abs(minutes) > expirationTime)) continue;
                    DeleteToken(token.GetValue("token_value").ToString());
                    tokensCount--;
                }
            }
            return tokensCount < tokensCountForServer;
        }

        public void AddToken(string token, string user, string time)
        {
            var userNameParam = SqlParameterFactory.Create(System.Data.DbType.String, user, "userName");
            var data = dbManager.GetDataByQuery("SELECT id FROM users WHERE name = @userName", userNameParam);
            var userId = data.GetValue(0, "id");
            dbManager.ExecuteQuery($"INSERT INTO jwt_sessions (user_id, creation_time, token_value) VALUES('{userId}','{time}','{token}')");
        }

        public void ClearTokens()
        {
            dbManager.ExecuteQuery("DELETE FROM jwt_sessions");
        }

        public void DeleteToken(string token)
        {
            dbManager.ExecuteQuery(String.Format(
                "DELETE FROM jwt_sessions WHERE token_value = '{0}'", token));
        }

        public void DeleteUserTokens(int userId)
        {
            dbManager.ExecuteQuery(String.Format(
                "DELETE FROM jwt_sessions WHERE user_id = '{0}'", userId));
        }
    }
}