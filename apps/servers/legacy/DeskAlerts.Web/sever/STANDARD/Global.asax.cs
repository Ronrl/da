using System.Net;
using DeskAlerts.Server.Utils.Interfactes;
using DeskAlerts.Server.Utils.Tasks;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Licensing;
using DeskAlertsDotNet.Parameters;
using DeskAlertsDotNet.Structs;
using DeskAlertsDotNet.Utils.Consts;
using Newtonsoft.Json.Linq;
using System.Data.SqlClient;
using System.IO;
using System.Net.Http;
using System.Net.Mail;
using System.Security;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.Routing;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Timer = System.Threading.Timer;
using DeskAlertsDotNet.Utils.Timers;
using System;
using System.Text;
using System.Web;
using NLog;
using DeskAlertsDotNet.Managers;

using DeskAlerts.Server;
using DeskAlerts.Server.Utils.Managers;
using DeskAlerts.ApplicationCore.Interfaces;

using Resources;

using System.Web.Http.Controllers;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using DeskAlerts.ApplicationCore.Utilities;
using DeskAlerts.Server.IoC;
using DeskAlertsDotNet.Utils;
using DeskAlerts.Server.Utils.Timers;
using static System.Web.HttpContext;

namespace DeskAlertsDotNet.Application
{
    public class JobHost : IRegisteredObject
    {
        private readonly object _lock = new object();

        private bool _shuttingDown;

        public JobHost()
        {
            HostingEnvironment.RegisterObject(this);
        }

        public void Stop(bool immediate)
        {
            lock (_lock)
            {
                _shuttingDown = true;
            }
            HostingEnvironment.UnregisterObject(this);
        }

        public void DoWork(Action work)
        {
            lock (_lock)
            {
                if (_shuttingDown)
                {
                    return;
                }
                work();
            }
        }
    }

    public static class TaskrssTimer
    {
        private static readonly Timer _timer = new Timer(OnTimerElapsed);
        private static readonly JobHost JobHost = new JobHost();

        public static void Start()
        {
            _timer.Change(TimeSpan.Zero, TimeSpan.FromMilliseconds(300000));
        }

        private static void OnTimerElapsed(object sender)
        {
            JobHost.DoWork(Global.StartRssTask);
        }
    }

    public static class LicTimer
    {
        private static readonly Timer _timer = new Timer(OnTimerElapsed);
        private static readonly JobHost _jobHost = new JobHost();

        public static void Start()
        {
            _timer.Change(TimeSpan.Zero, TimeSpan.FromMilliseconds(3600000));
        }

        private static void OnTimerElapsed(object sender)
        {
            _jobHost.DoWork(Global.CheckLic);
        }
    }

    public static class CacheTimer
    {
        private static readonly Timer _timer = new Timer(OnTimerElapsed);
        private static readonly JobHost _jobHost = new JobHost();

        public static void Start()
        {
            _timer.Change(TimeSpan.Zero, TimeSpan.FromSeconds(AppConfiguration.CacheRegenerateTime));
        }

        private static void OnTimerElapsed(object sender)
        {
            _jobHost.DoWork(Global.RebuildTask);
        }
    }

    public static class TaskarchiveTimer
    {
        private static readonly Timer _timer = new Timer(OnTimerElapsed);
        private static readonly JobHost JobHost = new JobHost();

        public static void Start()
        {
            _timer.Change(TimeSpan.Zero, TimeSpan.FromMilliseconds(3600000));
        }

        private static void OnTimerElapsed(object sender)
        {
            JobHost.DoWork(Global.StartArchiveTask);
        }
    }

    public static class TaskcleanTimer
    {
        private static readonly Timer _timer = new Timer(OnTimerElapsed);
        private static readonly JobHost JobHost = new JobHost();

        public static void Start()
        {
            _timer.Change(TimeSpan.Zero, TimeSpan.FromMilliseconds(1800000));
        }

        private static void OnTimerElapsed(object sender)
        {
            JobHost.DoWork(Global.StartCleanTask);
        }
    }

    public static class TasksyncTimer
    {
        private static readonly Timer _timer = new Timer(OnTimerElapsed);
        private static readonly JobHost JobHost = new JobHost();

        public static void Start()
        {
            _timer.Change(TimeSpan.Zero, TimeSpan.FromMilliseconds(60000));
        }

        private static void OnTimerElapsed(object sender)
        {
            JobHost.DoWork(Global.StartSyncTask);
        }
    }

    public class Global : HttpApplication
    {
        public static WindsorContainer Container { get; private set; }

        private static bool _timerLaunched;

        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public static bool IsCacheManagerLoaded { get; set; } = false;

        public static CacheManager CacheManager { get; private set; } 

        protected void Application_Start(object sender, EventArgs e)
        {
            LogLevelManager.ConfigureLogingRulesFromSettings();
            LogLevelManager.ConfigureLoggingDbConnectionSettings();
            _logger.Debug("Application_Start");

            Container = new WindsorContainer();
            Container.Register(Classes.FromThisAssembly()
                .BasedOn<IHttpController>()
                .LifestylePerWebRequest());

            Container.Register(Component.For<IConfigurationManager>().ImplementedBy<WebConfigConfigurationManager>().DependsOn(Dependency.OnValue("configuration", Config.Configuration)).LifestyleTransient());
            Container.Install(
                new RepositoryInstaller(),
                new ServiceInstaller(),
                new WebApiInstaller());

            GlobalConfiguration.Configuration.DependencyResolver = new DependencyResolver(Container.Kernel);

            SmsAndEmailTimer.Start();
            TaskrssTimer.Start();
            LicTimer.Start();
            CacheTimer.Start();
            TaskarchiveTimer.Start();
            ScheduleTimer.Start();
            TaskcleanTimer.Start();
            TasksyncTimer.Start();
            DeleteLogTimer.Start();
            DeleteClientCrashLogs.Start();

            var formaters = GlobalConfiguration.Configuration.Formatters;
            formaters.Remove(formaters.XmlFormatter);

            var jsonFormatter = GlobalConfiguration.Configuration.Formatters.JsonFormatter;
            jsonFormatter.SerializerSettings.Formatting = Formatting.Indented;
            jsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            jsonFormatter.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;

            RouteTable.Routes.MapHttpRoute(
                name: "AlertsApi",
                routeTemplate: "api/alerts",
                defaults: new { Controller = "Alerts" });

            RouteTable.Routes.MapHttpRoute(
                name: "ReceiveAlertsApiMethod",
                routeTemplate: "api/alerts/{userName}/{computerName}/{domain}");

            GlobalConfiguration.Configuration.MapHttpAttributeRoutes();
            GlobalConfiguration.Configuration.EnsureInitialized();

            AutoMapperConfiguration.Configure();

            if (AppConfiguration.IsNewContentDeliveringScheme)
            {
                var actualScheduleContent = new ActualScheduleContent();
                actualScheduleContent.CreateJobsForScheduleContent();
            }

            CacheManager = new CacheManager();
            CacheManager.RebuildCache(DateTime.Now.ToString(DateTimeFormat.MsSqlFormat));

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
        }

        public static void CheckLic()
        {
            try
            {
                LicenseManager.Default.CheckLicenseAndTrialState();
            }
            catch (Exception ex)
            {
                Pages.DeskAlertsBasePage.Logger.Debug(ex);

                string redirectPage = HandleException(ex);

                if (redirectPage.Length > 0)
                {
                    throw new Exception(redirectPage);
                }
            }
        }

        public static void StartRssTask()
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    string result = client.GetStringAsync(Config.Data.GetString("alertsDir") + "/RssSender.aspx").Result;
                }
            }
            catch (Exception ex)
            {
                Pages.DeskAlertsBasePage.Logger.Debug(ex);
            }
        }

        public static void StartSyncTask()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.Timeout = Timeout.InfiniteTimeSpan;
                    var result = client.GetStringAsync(Config.Data.GetString("alertsDir") + "/process_sync.asp").Result;
                    //Yes = Any sync was started
                    if (result.Contains("Yes"))
                    {
                        var tokenService = Container.Resolve<ITokenService>();

                        tokenService.RemoveAllTokens();
                    }
                }
            }
            catch (Exception ex)
            {
                Pages.DeskAlertsBasePage.Logger.Debug(ex);
            }
        }

        public static void StartArchiveTask()
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    string result = client.GetStringAsync(Config.Data.GetString("alertsDir") + "/process_archive.asp").Result;
                }
            }
            catch (Exception ex)
            {
                Pages.DeskAlertsBasePage.Logger.Debug(ex);
            }
        }

        public static void StartCleanTask()
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    string result = client.GetStringAsync(Config.Data.GetString("alertsDir") + "/process_clear.asp").Result;
                    result = client.GetStringAsync(Config.Data.GetString("alertsDir") + "/RegenerateCache.aspx").Result;
                }
            }
            catch (Exception ex)
            {
                Pages.DeskAlertsBasePage.Logger.Debug(ex);
            }
        }

        public static void RebuildTask()
        {
            try
            {
                if (IsCacheManagerLoaded)
                {
                    CacheManager.RebuildCache(DateTime.Now.ToString(DateTimeFormat.MsSqlFormat));
                }
            }
            catch (Exception ex)
            {
                Pages.DeskAlertsBasePage.Logger.Debug(ex);

                string redirectPage = HandleException(ex);

                if (redirectPage.Length > 0)
                {
                    throw new Exception(redirectPage);
                }
            }
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            Session["session_started"] = true;
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            RedirectToPrototol();
            if (!_timerLaunched)
            {
                // checking that db settings is correct
                try
                {
                    CheckLic();
                }
                catch (Exception ex)
                {
                    // recieved the link for custom page error through exception
                    Pages.DeskAlertsBasePage.Logger.Debug(ex);

                    if (ex.Message.Length > 0)
                    {
                        Response.Redirect(ex.Message);
                    }
                    else
                    {
                        Response.Write(GenerateCrashReportText(ex));
                        Response.End();
                    }
                }

                _timerLaunched = true;
            }
        }

        private void RedirectToPrototol()
        {
            if (!Current.Request.IsSecureConnection && Config.Data.GetString("alertsDir").ToLower().StartsWith("https://"))
            {
                Current.Response.Redirect(
                    Request.Url.AbsoluteUri.Replace("http://", "https://"), true);

            }
            else if (Current.Request.IsSecureConnection &&
                Config.Data.GetString("alertsDir").ToLower().StartsWith("http://"))
            {
                Current.Response.Redirect(
                    Request.Url.AbsoluteUri.Replace("https://", "http://"), true);
            }
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
        }

        private static string HandleException(Exception ex)
        {
            Pages.DeskAlertsBasePage.Logger.Error(ex, LogsText.GetLogText(LogsText.LogTextList.DefaultLogMessage));

            Exception handledException = ex.InnerException ?? ex;

            if (ex is SecurityException)
            {
                Pages.DeskAlertsBasePage.Logger.Fatal($"User \"{UserManager.Default.CurrentUser.Name}\" caused securety exception at {HttpContext.Current.Request.Url.AbsoluteUri}");
            }

            if (handledException is SqlException)
            {
                SqlException sqlException = handledException as SqlException;

                switch (sqlException.Number)
                {
                    case 4060:
                        {
                            return "~/da_errors/PoolIdentityError.html";
                        }
                }
            }

            return string.Empty;
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();

            if (!(ex is SqlException))
            {
                using (DBManager dbMgr = new DBManager())
                {
                    var userName = UserManager.Default.CurrentUser == null
                                       ? "Anonymous"
                                       : UserManager.Default.CurrentUser.Name;
                    var query = $"SELECT id, name, send_logs, email FROM users WHERE name LIKE '{userName}'";
                    var sendLogs = dbMgr.GetDataByQuery(query);

                    var logs = 0;
                    var email = string.Empty;

                    foreach (var row in sendLogs)
                    {
                        logs = row.GetInt("send_logs");
                        email = row.GetString("email");
                    }

                    if (logs == 1)
                    {
                        using (var client = new HttpClient())
                        {
                            string result = @client.GetStringAsync(Config.Data.GetString("alertsDir") +
                                                       "/admin/get_asp_config.asp?action=GetSMTPSettings").Result;
                            dynamic json = JValue.Parse(result);

                            string smtpServer = json.smtpServer;
                            int smtpPort = json.smtpPort;
                            string smtpUsername = json.smtpUsername;
                            string smtpPassword = json.smtpPassword;
                            string smtpFrom = json.smtpFrom;

                            byte[] encodedsmtpPassword = Convert.FromBase64String(smtpPassword);
                            string decodedSmtpPassword = Encoding.UTF8.GetString(encodedsmtpPassword);

                            SmtpClient smtpClient =
                                new SmtpClient(smtpServer, smtpPort)
                                {
                                    Credentials = new System.Net.NetworkCredential(smtpUsername, decodedSmtpPassword),
                                    DeliveryMethod = SmtpDeliveryMethod.Network,
                                    EnableSsl = true
                                };
                            MailMessage mail = new MailMessage
                            {
                                From = new MailAddress(smtpFrom),
                                Subject = "DeskAlerts exception",
                                Body = this.GenerateEmailCrashReportText(ex)
                                                              + "You can cancel sending crash logs in profole settings in DeskAlerts administration panel"
                            };

                            mail.To.Add(new MailAddress(email));
                            smtpClient.Send(mail);
                        }
                    }
                }
            }

            string customErrorPage = HandleException(ex);

            if (customErrorPage.Length > 0)
            {
                Response.Redirect(customErrorPage);
            }

            if (Request.Url.ToString().ToLower().IndexOf("getalerts.aspx", StringComparison.Ordinal) > -1)
            {
                //CrashReportManager.GenerateRecieveAlertsCrashReport(ex, Request.Url.ToString());
                //Response.Redirect(customErrorPage + "?crashReport=recieving");
                // GenerateRecieveCrashReportText(ex);
                Response.Write(GenerateRecieveCrashReportText(ex));
            }
            else
            {
                var crashTemplateText = GenerateCrashReportText(ex);

                //if (!string.IsNullOrEmpty(customErrorPage))
                //{
                //    Response.Redirect(customErrorPage + "?crashReport=" + crashLogPath);
                //}
                Response.Write(crashTemplateText);
            }

            Response.End();
            Server.ClearError();
        }

        private string GenerateRecieveCrashReportText(Exception ex)
        {
            CrashReportManager.GenerateRecieveAlertsCrashReport(ex, Request.Url.ToString());

            string crashTemplateText =
                File.ReadAllText(Config.Data.GetString("alertsFolder") + "\\da_errors\\DefaultErrorTemplate.html");

            crashTemplateText = crashTemplateText.Replace("%crashReportLink%",
                    CrashReportManager.BuildCrashReportLink("recieving"))
                .Replace("%crashReportText%",
                    CrashReportManager.ReadCrashReport("recieving").Replace(Environment.NewLine, "<br/>"))
                .Replace("%alertsDir%", Config.Data.GetString("alertsDir"))
                .Replace("%LNG_ERROR_TITLE%", resources.LNG_ERROR_TITLE)
                .Replace("%LNG_ERROR_BODY%", resources.LNG_ERROR_BODY)
                .Replace("%LNG_ERROR_DOWNLOAD%", resources.LNG_ERROR_DOWNLOAD)
                .Replace("%LNG_ERROR_DETAILS%", resources.LNG_ERROR_DETAILS);
            return crashTemplateText;
        }

        private string GenerateCrashReportText(Exception ex)
        {
            string crashLogPath = CrashReportManager.GenerateCrashReport(ex, Request.Url.ToString());

            string crashTemplateText =
                File.ReadAllText(Config.Data.GetString("alertsFolder") + "\\da_errors\\DefaultErrorTemplate.html");

            crashTemplateText = crashTemplateText.Replace("%crashReportLink%",
                CrashReportManager.BuildCrashReportLink(crashLogPath))
                .Replace("%crashReportText%", CrashReportManager.ReadCrashReport(crashLogPath))
                .Replace("%packageVersion%", GetApplicationVersion())
                .Replace("%alertsDir%", Config.Data.GetString("alertsDir"))
                .Replace("%LNG_ERROR_TITLE%", resources.LNG_ERROR_TITLE)
                .Replace("%LNG_ERROR_BODY%", resources.LNG_ERROR_BODY)
                .Replace("%LNG_ERROR_DOWNLOAD%", resources.LNG_ERROR_DOWNLOAD)
                .Replace("%LNG_ERROR_DETAILS%", resources.LNG_ERROR_DETAILS);
            return crashTemplateText;
        }

        private string GenerateEmailCrashReportText(Exception ex)
        {
            string crashLogPath = CrashReportManager.GenerateCrashReport(ex, Request.Url.ToString());

            return CrashReportManager.ReadCrashReport(crashLogPath);
        }

        private string GetApplicationVersion()
        {
            string result = String.Empty;
            using (DBManager dbMgr = new DBManager())
            {
                result = dbMgr.GetScalarQuery<string>("SELECT TOP 1 version FROM version");
            }

            return result;
        }

        protected void Session_End(object sender, EventArgs e)
        {
        }

        protected void Application_End(object sender, EventArgs e)
        {
        }
    }
}