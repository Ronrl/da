﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ShowCache.aspx.cs" Inherits="DeskAlerts.Server.sever.STANDARD.ShowCache" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>DeskAlerts - Show cache page</title>
    <style>
        table {
            width: 100%;
            text-align: center;
            table-layout: fixed;
            border: 1px solid grey;
            margin: 0.4rem 0;
            padding: 0;
            border-spacing: 0;
        }

            table tr:nth-child(1) {
                background-color: burlywood;
            }

                table tr:nth-child(1):hover {
                    background-color: burlywood;
                }

            table tr:nth-child(2n) {
                background-color: #fff5e6;
            }

            table tr:hover {
                background-color: bisque;
            }

            table tr th {
                padding: 0.3rem;
            }

            table tr td {
                padding: 0.2rem;
            }

        .RegenerateCachePageLink {
            padding: 0.2rem 0.4rem;
            margin: 0.5rem 0;
        }
    </style>
</head>
<body>
    <form id="ShowCacheForm" runat="server">
        <div>
            <asp:Button runat="server" ID="RegenerateCachePageLink" CssClass="RegenerateCachePageLink" />
        </div>
        <div>
            <asp:ListView runat="server" ID="UserCacheViewerList">
                <LayoutTemplate>
                    <table runat="server">
                        <tr>
                            <th colspan="4">User Cache</th>
                        </tr>
                        <tr runat="server">
                            <th runat="server">UserName</th>
                            <th runat="server">ClsId</th>
                            <th runat="server">AlertId</th>
                            <th runat="server">AlertTitle</th>
                        </tr>
                        <tr runat="server" id="ItemPlaceholder"></tr>
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <tr runat="server">
                        <td runat="server"><%# Eval("UserName") %></td>
                        <td runat="server"><%# Eval("ClsId") %></td>
                        <td runat="server"><%# Eval("AlertId") %></td>
                        <td runat="server"><%# Eval("AlertTitle") %></td>
                    </tr>
                </ItemTemplate>
            </asp:ListView>
        </div>

        <div>
            <asp:ListView runat="server" ID="ReceivedCacheViewerList">
                <LayoutTemplate>
                    <table runat="server">
                        <tr>
                            <th colspan="4">Received Cache</th>
                        </tr>
                        <tr runat="server">
                            <th runat="server">UserName</th>
                            <th runat="server">ClsId</th>
                            <th runat="server">AlertId</th>
                            <th runat="server">AlertTitle</th>
                        </tr>
                        <tr runat="server" id="ItemPlaceholder"></tr>
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <tr runat="server">
                        <td runat="server"><%# Eval("UserName") %></td>
                        <td runat="server"><%# Eval("ClsId") %></td>
                        <td runat="server"><%# Eval("AlertId") %></td>
                        <td runat="server"><%# Eval("AlertTitle") %></td>
                    </tr>
                </ItemTemplate>
            </asp:ListView>
        </div>
    </form>
</body>
</html>
