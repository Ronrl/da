<!-- #include file="admin/config.inc" -->
<%
	Response.CacheControl = "no-cache"
	Response.AddHeader "Pragma", "no-cache"
	Response.Expires = -1

	url = alerts_folder&"/GetAlerts.aspx?" & Request.ServerVariables("QUERY_STRING") &"&ip=" & Request.ServerVariables("remote_addr") &"&content_type=ls"
    Set oXMLHTTP = CreateObject("Msxml2.serverXMLHTTP.6.0")

    oXMLHTTP.Open "GET", url, False
    oXMLHTTP.Send

    if oXMLHTTP.Status = 200 then
                Response.Write oXMLHTTP.responseText
                Response.End
    end if

    Response.End
 %>

<LOCKSCREENS>
<% db_backup_no_redirect = 1 %>

<!-- #include file="admin/db_conn.asp" -->
<!-- #include file="admin/libs/templateObj.asp" -->
<!-- #include file="admin/md5.asp" -->
<%
templateGuid = "23d8575a-9eb8-41ee-9f0a-e3a11dce3fc6"

uname=Replace(Request("uname"),"'","''")
cname=Replace(Request("computer"),"'","''")
desk_id= Replace(Request("deskbar_id"),"'","''")
nextrequest = Request ("nextrequest")
localtime = Request("localtime")

if localtime = "" then
	localtime = now_db
end if

Dim XML
user_id=0
standby = Request("standby")
disable = Request("disable")

if(disable<>"1") then
	disable="0"
end if

if standby <> "1" then
	standby = "0"
end if

toolbar=Request("toolbar")

strUserDeskbarId= Replace(Request("deskbar_id"),"'","''")
intUserDeskbarId=0

if(toolbar<>1) then toolbar=0 end if

if Request("scheduled") = "" then
	g_scheduled_not_set = true
	g_scheduled = -1
else
	g_scheduled_not_set = false
	g_scheduled = Request("scheduled")
end if

if Request("ticker") = "" then
	g_ticker = -1
else
	g_ticker = Request("ticker")
end if
totalAlertCount=0


if(AD = 3) then
	if(EDIR=1) then
		if ConfDisableContextSupport <> 1 then
			context=Request("edir")
			context_id=-1
			if(len(context)>0) then
				Set RS3 = Conn.Execute("SELECT c.id, c.name FROM users u INNER JOIN edir_context c ON u.context_id=c.id WHERE u.name=N'"& uname&"' and u.role='U'")
				Do while Not RS3.EOF
					strContext=RS3("name")
					strContext=replace(strContext,",ou=",".")
					strContext=replace(strContext,",o=",".")
					strContext=replace(strContext,",dc=",".")
					strContext=replace(strContext,",c=",".")

					strContext=replace(strContext,"ou=","")
					strContext=replace(strContext,"o=","")
					strContext=replace(strContext,"dc=","")
					strContext=replace(strContext,"c=","")
					if(LCase(strContext)=LCase(context)) then
						context_id=RS3("id")
						exit do
					end if
					RS3.MoveNext
				loop
			end if
		else
			context_id = -2
		end if
	else
		domain=replace(Request("fulldomain"),"'","''")
		compdomain=replace(Request("compdomain"),"'","''")
		if ConfForceSelfRegWithAD=1 then
			SQL = "SELECT d.name FROM domains d, users u WHERE (u.name='"&uname&"' OR u.display_name='"&uname&"') AND u.domain_id<>1 AND u.domain_id = d.id"
			Set RSMimic = Conn.Execute(SQL)
			if not RSMimic.EOF then
				domain=RSMimic("name")
			end if
		end if
		if compdomain = "" then
			compdomain = domain
		end if
	
	end if

end if


'alerts


Dim ip : ip = Request.ServerVariables("HTTP_X_FORWARDED_FOR")
if (ip = "") then
	ip = Request.ServerVariables("REMOTE_ADDR")
end if
Dim longip : longip = -1
if (ip <> "") then
	longip = 0
	Dim iprange : iprange=Split(ip, ".")
	if UBound(iprange) = 3 then
		For j = 0 to 3
			if isNumeric(iprange(j)) then
				if iprange(j) < 0 or iprange(j) > 255 then
					longip = -1
					exit for
				end if
				longip = longip + iprange(j)*(256^(3-j))
			else
				longip = -1
				exit for
			end if
		Next
	end if
end if

'--- ALERTS ---
if Request("survey") = "" or Request("survey") = "0" then
	alertsSQL = " SET NOCOUNT ON" & vbCrLf &_
				" DECLARE @user_id BIGINT" & vbCrLf &_
				" DECLARE @comp_id BIGINT" & vbCrLf &_
				" DECLARE @context_id BIGINT" & vbCrLf &_
				" DECLARE @comp_domain NVARCHAR (255)" & vbCrLf &_
				" DECLARE @cur_date DATETIME" & vbCrLf &_
				" DECLARE @user_date DATETIME" & vbCrLf &_
				" DECLARE @user_name NVARCHAR (255)" & vbCrLf &_
				" DECLARE @user_domain NVARCHAR (255)" & vbCrLf &_
				" DECLARE @comp_name NVARCHAR (255)" & vbCrLf &_
				" DECLARE @next_request VARCHAR (255)" & vbCrLf &_
				" DECLARE @clsid VARCHAR (255)" & vbCrLf &_
				" DECLARE @ip BIGINT" & vbCrLf &_
				" DECLARE @now DATETIME" &  vbCrLf &_
				" DECLARE @client_version VARCHAR (50)" & vbCrLf &_
				" SET @cur_date = GETDATE()" & vbCrLf &_
				" SET @context_id = '"& Replace(context_id,"'","''") &"'"& vbCrLf &_
				" SET @user_domain = '"& domain &"'"& vbCrLf &_
				" SET @comp_domain = '"& compdomain &"'"& vbCrLf &_
				" SET @user_name = '"& Replace(uname,"'","''") &"'"& vbCrLf &_
				" SET @comp_name = '"& Replace(cname,"'","''") &"'"& vbCrLf &_
				" SET @next_request = '"& Replace(nextrequest,"'","''") &"'"& vbCrLf &_
				" SET @now = '"& Replace(localtime,"|"," ") &"'"& vbCrLf &_
				" SET @clsid = '"& Replace(desk_id,"'","''") &"'"& vbCrLf &_
				" SET @ip = "& longip & vbCrLf &_
				" " & vbCrLf &_
				" IF(@context_id > 0)" & vbCrLf &_
				" 	SELECT @user_id = u.id, @user_date = u.reg_date" & vbCrLf &_
				" 	FROM   users u WITH (READPAST)" & vbCrLf &_
				" 	WHERE u.name = @user_name AND role = 'U'" & vbCrLf &_
				" 		 AND u.context_id = @context_id" & vbCrLf &_
				" ELSE IF(@user_domain IS NULL OR @user_domain = N'')" & vbCrLf &_
				" 	SELECT @user_id = u.id, @user_date = u.reg_date" & vbCrLf &_
				" 	FROM   users u WITH (READPAST)" & vbCrLf &_
				" 		 LEFT JOIN domains d WITH (READPAST)" & vbCrLf &_
				" 		   ON d.id = u.domain_id" & vbCrLf &_
				" 	WHERE  (d.name = N'' OR u.domain_id IS NULL)" & vbCrLf &_
				" 		 AND u.name = @user_name AND role = 'U'" & vbCrLf &_
				" ELSE" & vbCrLf &_
				" 	SELECT @user_id = u.id, @user_date = u.reg_date" & vbCrLf &_
				" 	FROM   users u WITH (READPAST)" & vbCrLf &_
				" 		 JOIN domains d WITH (READPAST)" & vbCrLf &_
				" 		   ON d.id = u.domain_id" & vbCrLf &_
				" 	WHERE  d.name = @user_domain" & vbCrLf &_
				" 		 AND u.name = @user_name AND role = 'U'" & vbCrLf &_
				" " & vbCrLf &_
				" IF(@user_id > 1)" & vbCrLf &_
				" 	UPDATE users" & vbCrLf &_
				" 	SET  last_request = @cur_date," & vbCrLf &_
				" 		 next_request = @next_request," & vbCrLf &_
				" 		 standby = " & standby & "," & vbCrLf &_
				" 		 disabled = " & disable & "," & vbCrLf &_
				"		 client_version = '"& Replace(clientVersion,"'","''") &"'" & vbCrLf &_
				" 	WHERE  id = @user_id" & vbCrLf &_
				" " & vbCrLf &_
				" SELECT @user_id as user_id" & vbCrLf &_
				" " & vbCrLf &_
				" IF(@user_id > 1)" & vbCrLf &_
				" BEGIN" & vbCrLf &_
				" 	IF(@comp_domain IS NULL OR @comp_domain = N'')" & vbCrLf &_
				" 		SELECT @comp_id = comp.id" & vbCrLf &_
				" 		FROM   computers comp WITH (READPAST)" & vbCrLf &_
				" 		INNER JOIN domains d WITH (READPAST)" & vbCrLf &_
				" 			   ON d.id = comp.domain_id" & vbCrLf &_
				" 		WHERE  (d.name = N'' OR comp.domain_id IS NULL)" & vbCrLf &_
				" 			 AND comp.name = @comp_name;" & vbCrLf &_
				" 	ELSE" & vbCrLf &_
				" 		SELECT @comp_id = comp.id" & vbCrLf &_
				" 		FROM computers comp WITH (READPAST)" & vbCrLf &_
				" 		INNER JOIN domains d WITH (READPAST)" & vbCrLf &_
				" 		ON d.id = comp.domain_id" & vbCrLf &_
				" 		WHERE d.name = @comp_domain" & vbCrLf &_
				" 			AND comp.name = @comp_name;" & vbCrLf &_
				" " & vbCrLf &_
				" 	IF(@comp_id > 1)" & vbCrLf &_
				" 		UPDATE computers" & vbCrLf &_
				" 		SET last_request = @cur_date," & vbCrLf &_
				" 			 next_request = @next_request" & vbCrLf &_
				" 		WHERE  id = @comp_id;" & vbCrLf &_
				" " & vbCrLf &_
				" 	EXEC getAlertsFromCache @user_id, @user_date, @comp_id, @clsid, @ip, @now, @comp_name" & vbCrLf &_
				" END "
	Set RSalerts = Conn.Execute(alertsSQL)
	
	user_id=-1
	if(Not RSalerts.EOF) then
		if(VarType(RSalerts("user_id")) > 1) then
			user_id=RSalerts("user_id")
		end if
		Set RSalerts = RSalerts.NextRecordset()
	end if

	if (not RSalerts is Nothing) then
		if(Not RSalerts.EOF) then
			setXML
		end if

		Do While (Not RSalerts.EOF)
			if (RSalerts("class") = "4096") then
				recurrence=RSalerts("recurrence")
				alert_id=RSalerts("id")
			
				schedule=RSalerts("schedule")
				'for not stopped schedule alerts (checking schedule_type)
				if(((recurrence="1" OR schedule="1") AND RSalerts("schedule_type")="1" and (g_scheduled=1 or g_scheduled_not_set)) OR (recurrence<>"1" AND schedule<>"1" and (g_scheduled=0 or g_scheduled_not_set))) then
					if(urgent<>"1") then totalAlertCount=totalAlertCount+1 end if
					urgent=RSalerts("urgent")
					ticker=RSalerts("ticker")
					if (g_ticker=-1 OR ticker=g_ticker) and ((urgent="1") or ((disable <> "1")))  then
						aknown=RSalerts("aknown")
						if(Not IsNull(RSalerts("autoclose"))) then
							autoclose=Clng(RSalerts("autoclose"))
						else
							autoclose=0
						end if
						create_date=RSalerts("sent_date")
						create_timestamp=GetUnixDate(create_date)
						alert_title=RSalerts("title")
						
						LoadTemplateFromString  "title"&templateGuid, alert_title
						
						if (RSalerts("rec_pattern")="c") then		
							time_till = DateDiff("n",now_db,RSalerts("from_date"))
							SetVar "time_till", time_till& " minutes till "
						else
							SetVar "time_till", ""
						end if
						
						Parse  "title"&templateGuid, False
						alert_title = PrintVar("title"&templateGuid)
						
						alert_width = RSalerts("alert_width")
						alert_height = RSalerts("alert_height")
						
						if(ticker=1) then
							alert_width = ""
							alert_height = ""
						end if
						
						position = RSalerts("fullscreen")
						
						if (RSalerts("caption_file_name") <> "") then
							innerXML = "<WINDOW captionhref="""+alerts_folder+"admin/preview/skin/version/"+RSalerts("caption_file_name")+""" />"
						else
							innerXML = ""
						end if

						href = "get_lockscreen.asp?id=" & alert_id
						if XML.addCustomAlert2(urgent, href, alert_id, aknown, autoclose, create_timestamp, alert_title, position, "", alert_width, alert_height, ticker, schedule, recurrence, true, innerXML) then
							Conn.Execute(_
								"IF NOT EXISTS (SELECT 1 FROM alerts_read WHERE user_id = "& CStr(user_id) &" AND alert_id = "& alert_id &")"& vbCrLf &_
								"	INSERT INTO alerts_read (user_id, alert_id, [date]) VALUES (" & CStr(user_id) & ", " & alert_id & ", '"&Replace(localtime,"|"," ")&"')")
						end if
					end if
				end if
			end if
			RSalerts.MoveNext
		Loop
	end if
end if
'--- ALERT END ---

'end if

'end if
if not isEmpty(XML) then
	Response.Write XML.getResult()
end if

XML = null
%>
<!-- #include file="admin/db_conn_close.asp" -->
</LOCKSCREENS>