﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DeskAlertsDotNet.Exceptions
{
    public class DeskAlertsConfigException : Exception
    {
        public DeskAlertsConfigException(string configOptionName)
            : base(string.Format("Invalid or empty value in field {0}", configOptionName))
        {
            
        }
    }
}