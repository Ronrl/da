﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DeskAlertsDotNet.Exceptions
{
    public class DeskAlertsDBException : Exception
    {
        public DeskAlertsDBException(string query, string error) : base( String.Format("Error: {0} in query {1}", error, query) )
        {
            
        }
    }
}