﻿using DeskAlertsDotNet.Models;
using System;

namespace DeskAlertsDotNet.Managers
{
    /// <summary>
    /// ToDo: Remove the class after remove UserManager, new implementation must be in SecurityService
    /// </summary>
    public class Token
    {
        public Guid Guid { get; set; }
        public DateTime LastActiveDateTime { get; set; }
        public User User { get; set; }
    }
}