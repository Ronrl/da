using DeskAlerts.Server.Utils.Services;
using DeskAlerts.Server.Utils;
using DeskAlerts.Server.Utils.Interfactes;
using DeskAlerts.Server.Exceptions;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Models;
using DeskAlertsDotNet.Pages;
using DeskAlertsDotNet.Utils.Factories;
using DeskAlertsDotNet.Utils.Services;
using DeskAlertsDotNet.Parameters;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.ApplicationCore.Repositories;
using DeskAlerts.ApplicationCore.Utilities;
using System.IdentityModel.Services;
using System.Security.Claims;
using System.IdentityModel.Tokens;
using NLog;
using DataRow = DeskAlertsDotNet.DataBase.DataRow;
using DataSet = DeskAlertsDotNet.DataBase.DataSet;

namespace DeskAlertsDotNet.Managers
{
    public class UserManager
    {
        private Logger _logger = LogManager.GetCurrentClassLogger();

        private const string SelectUserByNameQuery =
            @"SELECT u.id, u.start_page, u.mobile_phone, u.email, p.type, pe.policy_id 
                FROM users as u
                LEFT JOIN policy_editor as pe ON pe.editor_id = u.id
                LEFT JOIN policy as p ON p.id = pe.policy_id WHERE [role] != 'U' AND u.name = @Name
                ORDER BY u.id ASC";

        private const string SelectUserIdByNameAndDomainQuery = @"
            SELECT u.id 
            FROM users AS u LEFT JOIN domains AS d ON u.domain_id = d.id
            WHERE u.name = @Name AND lower(d.name) = @Domain AND u.role = 'U'";

        private const string SelectUserIdByNameQuery = @"
            SELECT u.id 
            FROM users AS u
            WHERE u.name = @Name AND u.role = 'U'";

        private const string SelectUsersIdQuery = @"
            SELECT u.id 
            FROM users AS u
            WHERE u.role = 'U'";

        private const string SelectUserByIdQuery = @"
            SELECT u.id, u.name, u.email, u.mobile_phone AS phone
            FROM users AS u
            WHERE u.role = 'U' AND u.id = @userId";

        private const string SuperAdminUsername = "admin";

        private static UserManager _instance;

        private readonly IUserRepository _userRepository;
        private readonly IPolicyRepository _policyRepository;
        private readonly ISsoService _ssoService;
        private readonly IUserService _userService;
        private readonly IPolicyService _policyService;
        private readonly IAdService _adService;

        public static UserManager Default
        {
            get
            {
                if (_instance == null)
                {
                    var configurationManager = new WebConfigConfigurationManager(Config.Configuration);
                    var domainRepository = new PpDomainRepository(configurationManager);
                    var userRepository = new PpUserRepository(configurationManager);
                    var groupRepository = new PpGroupRepository(configurationManager);
                    var policyRepository = new PpPolicyRepository(configurationManager);
                    var policyPublisherRepository = new PpPolicyPublisherRepository(configurationManager);
                    var widgetRepository = new PpWidgetRepository(configurationManager);
                    var publisherWidgetRepository = new PpPublisherWidgetRepository(configurationManager);
                    var skinRepository = new PpSkinRepository(configurationManager);
                    var contentSettingsRepository = new PpContentSettingsRepository(configurationManager);
                    return _instance = new UserManager(domainRepository, userRepository, groupRepository, policyRepository,
                        policyPublisherRepository, widgetRepository, publisherWidgetRepository, skinRepository, contentSettingsRepository);
                }

                return _instance;
            }
        }

        public UserManager(IDomainRepository domainRepository, IUserRepository userRepository, IGroupRepository groupRepository,
            IPolicyRepository policyRepository, IPolicyPublisherRepository policyPublisherRepository, IWidgetRepository widgetRepository,
            IPublisherWidgetRepository publisherWidgetRepository, ISkinRepository skinRepository, IContentSettingsRepository contentSettingsRepository)
        {
            _userRepository = userRepository;
            _policyRepository = policyRepository;

            _ssoService = new SsoService(groupRepository, domainRepository, policyRepository);
            _userService = new UserService(userRepository, groupRepository);
            _policyService = new PolicyService(policyRepository, groupRepository, domainRepository, policyPublisherRepository,
                userRepository, skinRepository, contentSettingsRepository);
            _adService = new AdService(domainRepository, _userRepository);

            Tokens = new List<Token>();
        }

        public void SetDataBaseManager(DBManager mgr)
        {
            //this.dbMgr = mgr;
        }

        public User GetUserByNameByRepository(string userName)
        {
            var userEntity = _userRepository.GetPublisher(userName);
            if (userEntity == null)
            {
                return null;
            }

            var policyTypes = _policyRepository.GetPoliciesTypes(userEntity.Id).ToArray();
            var isAdmin = policyTypes.Contains('A') || IsUserSuperAdmin(userEntity);
            var isModer = false;
            var isPublisher = false;
           
            if (!isAdmin)
            {
                isModer = policyTypes.Contains('M');

                if (!isModer)
                {
                    isPublisher = true;
                }
            }

            var policy = _policyService.GetDefaultUserPolicy(userEntity.Id, isAdmin);

            if (isPublisher)
            {
                policy = _policyService.GetMergedPolicyByPublisherId(userEntity.Id);
            }

            policy.SkinList = _policyService.GetMergedPolicySkinsByPublisherId(userEntity.Id);
            policy.ContentSettingsList = _policyService.GetMergedPolicyContentSettingsByPublisherId(userEntity.Id);

            var widget = GetWidgetsForUser((int)userEntity.Id); //ToDo: move to repositories
            var user = new User((int)userEntity.Id, userEntity.Username, isAdmin, isModer, isPublisher, policy, widget, userEntity.Email, userEntity.Phone,
                userEntity.StartPage, userEntity.LanguageId);

            return user;
        }

        /// <summary>
        /// Get all users ID
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetUsersId()
        {
            try
            {
                using (var dataBaseManager = new DBManager())
                {
                    DataSet userIdDataSet = dataBaseManager.GetDataByQuery(SelectUsersIdQuery);
                    return userIdDataSet;
                }
            }
            catch (Exception ex)
            {
                DeskAlertsBasePage.Logger.Error(ex.Message);
                DeskAlertsBasePage.Logger.Debug(ex);
                throw;
            }
        }

        /// <summary>
        /// Get user by ID
        /// </summary>
        /// <param name="id">ID of the user</param>
        /// <returns>Instance of User</returns>
        public User GetUserById(long id)
        {
            try
            {
                using (var dataBaseManager = new DBManager())
                {
                    SqlParameter useridParameter = SqlParameterFactory.Create(DbType.String, id, "userId");
                    var userDataSet = dataBaseManager.GetDataByQuery(SelectUserByIdQuery, useridParameter);
                    DataRow row = userDataSet.GetRow(0);
                    User user = new User(
                        row.GetInt("id"),
                        row.GetString("name"),
                        row.GetString("email"),
                        row.GetString("phone"));
                    return user;
                }
            }
            catch (Exception ex)
            {
                DeskAlertsBasePage.Logger.Error(ex.Message);
                DeskAlertsBasePage.Logger.Debug(ex);
                throw;
            }
        }

        private User GetUserByGuid(Guid guid)
        {
            User result = this.Tokens.FirstOrDefault(x => x.Guid == guid)?.User;
            return result;
        }

        public int GetActivePublishersLeftAmount()
        {
            try
            {
                var maxPublishersAmount = _userRepository.GetMaxPublishersAmount();
                var activePublishersAmount = _userRepository.GetActivePublishersAmount();
                
                return maxPublishersAmount - activePublishersAmount;
            }
            catch (Exception ex)
            {
                _logger.Debug(ex.Message);
                throw;
            }
        }

        public int GetMaxPublishersAmount()
        {
            try
            {
                return _userRepository.GetMaxPublishersAmount();
            }
            catch (Exception ex)
            {
                _logger.Debug(ex.Message);
                throw;
            }
        }

        public T GetPublisherParameterByName<T>(string parameter, string name)
        {
            try
            {
                return _userRepository.GetPublisherParameterByName<T>(parameter, name);
            }
            catch (Exception ex)
            {
                _logger.Debug(ex.Message);
                throw;
            }
        }

        public void DisableLastOnlinePublishers(int amount)
        {
            try
            {
                _userRepository.DisableLastOnlinePublishers(amount);
            }
            catch (Exception ex)
            {
                _logger.Debug(ex.Message);
                throw;
            }
        }

        public void DisableLastOnlinePublishers(int amount, IEnumerable<string> allowedStatuses)
        {
            try
            {
                _userRepository.DisableLastOnlinePublishers(amount, allowedStatuses);
            }
            catch (Exception ex)
            {
                _logger.Debug(ex.Message);
                throw;
            }
        }

        public void DisablePublishers(IEnumerable<string> publishersIds)
        {
            try
            {
                _userRepository.DisablePublishers(publishersIds);
            }
            catch (Exception ex)
            {
                _logger.Debug(ex.Message);
                throw;
            }
        }

        public void EnablePublisher(string name)
        {
            try
            {
                _userRepository.EnablePublisher(name);
            }
            catch (Exception ex)
            {
                _logger.Debug(ex.Message);
                throw;
            }
        }

        public void EnablePublishers(IEnumerable<string> publishersIds)
        {
            try
            {
                _userRepository.EnablePublishers(publishersIds);
            }
            catch (Exception ex)
            {
                _logger.Debug(ex.Message);
                throw;
            }
        }

        public SessionSecurityToken RegisterClaimIdentity(ClaimsPrincipal claimsPrincipal)
        {
            var token =
                FederatedAuthentication.SessionAuthenticationModule.CreateSessionSecurityToken(
                    claimsPrincipal, "DeskAlerts", DateTime.UtcNow, DateTime.UtcNow.AddMinutes(30), true);
            FederatedAuthentication.SessionAuthenticationModule.WriteSessionTokenToCookie(token);
            return token;
        }

        public Token CreateToken(User user)
        {
            return new Token
            {
                Guid = Guid.NewGuid(),
                LastActiveDateTime = DateTime.Now,
                User = user
            };
        }

        public void AddToken(Token token)
        {
            Tokens.Add(token);
        }

        public Guid Register(User user)
        {
            var token = CreateToken(user);
            AddToken(token);
            if (HttpContext.Current != null)
            {
                HttpContext.Current.Session["token"] = token.Guid;
            }

            return token.Guid;
        }

        public void Unregister(Guid guid)
        {
            var token = Tokens.FirstOrDefault(x => x.Guid == guid);
            if (token != null)
            {
                Tokens.Remove(token);
            }
        }

        public User CurrentUser
        {
            get
            {
                User user = null;
                if (HttpContext.Current.Session != null)
                {
                    var token = HttpContext.Current.Session["token"];

                    if (token != null)
                    {
                        var guid = (Guid)token;
                        user = this.GetUserByGuid(guid);
                    }
                    else
                    {
                        if (Config.Data.IsOptionEnabled("OKTA_INTEGRATION"))
                        {
                            var userName = FederatedAuthentication.SessionAuthenticationModule.
                                ContextSessionSecurityToken.ClaimsPrincipal.Identity.Name.Split('@').First();

                            var userToken = Tokens.SingleOrDefault(x => x.User.Name == userName);

                            if (userToken == null)
                            {
                                HttpContext.Current.Response.Redirect("~/admin/Auth.aspx");
                            }

                            user = userToken.User;
                        }
                    }
                }

                return user;
            }
        }

        public List<Widget> GetWidgetsForUser(int userId)
        {
            try
            {
                using (var dataBaseManager = new DBManager())
                {
                    var result = WidgetManager.Default.GetWidgetsForUser(userId, dataBaseManager);
                    return result;
                }
            }
            catch
            {
                return null;
            }
        }

        public void UpdateWidgetParameters(string widgetId, string parameters)
        {
            try
            {
                using (var dataBaseManager = new DBManager())
                {
                    dataBaseManager.ExecuteQuery("UPDATE widgets_editors SET parameters = '" + parameters + "' WHERE id = '" + widgetId + "'");
                    User user = CurrentUser;

                    user.Widgets.Where(w => w.Id.ToLower().IndexOf(widgetId.ToLower(), StringComparison.Ordinal) > -1).ToList().ForEach(w =>
                        {
                            w.Parameters = parameters;
                            string widgetHrefWithParams = DeskAlertsUtils.AppendParameterToUrl(w.Href, parameters);
                            w.Href = widgetHrefWithParams;
                        });
                }
            }
            catch (Exception ex)
            {
                DeskAlertsBasePage.Logger.Error(ex.Message);
                DeskAlertsBasePage.Logger.Debug(ex);
                throw;
            }
        }

        public void RemoveWidgetForUser(string widgetId)
        {
            try
            {
                using (var dataBaseManager = new DBManager())
                {
                    dataBaseManager.GetDataByQuery("DELETE FROM widgets_editors OUTPUT deleted.editor_id WHERE id = '" + widgetId + "'");
                    User user = CurrentUser;
                    user.Widgets.RemoveAll(w => w.Id.Equals(widgetId));
                }
            }
            catch (Exception ex)
            {
                DeskAlertsBasePage.Logger.Error(ex.Message);
                DeskAlertsBasePage.Logger.Debug(ex);
                throw;
            }
        }

        public void AddWidgetForUser(string instanceId, int userId, string widgetId, string parameters, string page)
        {
            try
            {
                using (var dataBaseManager = new DBManager())
                {
                    dataBaseManager.ExecuteQuery(string.Format("INSERT INTO widgets_editors (id, editor_id, widget_id, parameters, page) VALUES('" + instanceId + "',{0},'{1}','{2}','{3}')", userId, widgetId, parameters, page));
                    Widget widget = WidgetManager.Default.GetWidgetById(instanceId, widgetId, dataBaseManager, parameters, page);
                    User user = CurrentUser;
                    user.Widgets.Add(widget);
                }
            }
            catch (Exception ex)
            {
                DeskAlertsBasePage.Logger.Error(ex.Message);
                DeskAlertsBasePage.Logger.Debug(ex);
                throw;
            }
        }

        public string GetUsernameByGuid(Guid guid)
        {
            var userName = string.Empty;
            var user = Tokens.FirstOrDefault(x => x.Guid == guid);
            if (user != null)
            {
                userName = user.User.Name;
            }
            return userName;
        }

        public void ReloadCurrentUser()
        {
            //string name = CurrentUser.Name;
            var guid = (Guid)HttpContext.Current.Session["token"];
            var userName = GetUsernameByGuid(guid);
            User newInstance = this.GetUserByNameByRepository(userName);
            //UnregisterUser(HttpContext.Current.Session);
            //RegisterUser(HttpContext.Current.Session, newInstance);

            Unregister(guid);
            Register(newInstance);
        }

        public List<Token> Tokens { get; set; }

        public static int TokenExpirationMinutes => 30;

        public bool IsAuthorized(Guid guid)
        {
            var token = Tokens.FirstOrDefault(x => x.Guid == guid);
            if (token != null && token.LastActiveDateTime.AddMinutes(TokenExpirationMinutes) > DateTime.Now)
            {
                token.LastActiveDateTime = DateTime.Now;
                return true;
            }
            else
            {
                return false;
            }
        }

        public class DomainUser
        {
            private Logger _logger = LogManager.GetCurrentClassLogger();

            public DomainUser() { }

            public DomainUser(string fullUserName)
            {
                _logger.Debug($"Try to get domain user {fullUserName}");
                if (fullUserName.Contains("@"))
                {
                    var userNameWithDomain = fullUserName.Split('@');
                    if (userNameWithDomain.Count() != 2)
                    {
                        throw new DomainUserValidationException();
                    }

                    DomainName = userNameWithDomain.Last();
                    UserName = userNameWithDomain.First();
                }
                else if (fullUserName.Contains("\\"))
                {
                    var userNameWithDomain = fullUserName.Split('\\');
                    if (userNameWithDomain.Count() != 2)
                    {
                        throw new DomainUserValidationException();
                    }
                    DomainName = userNameWithDomain.First();
                    UserName = userNameWithDomain.Last();
                }
                else
                {
                    _logger.Debug($"Couldn't get domain user ${fullUserName}");
                    throw new ArgumentNullException();
                }
            }

            public string UserName { get; set; }
            public string DomainName { get; set; }
        }

        public DomainUser CreateDomainUser(string login)
        {
            return new DomainUser(login);
        }

        public User LoginWithWindowsAuth(DomainUser domainUser)
        {
            _logger.Debug($"Try to logged in {domainUser.DomainName}/{domainUser.UserName}");
            User user = null;
            if (!string.IsNullOrEmpty(domainUser.DomainName))
            {
                user = GetUserByNameByRepository(domainUser.UserName);
                if (user == null)
                {
                    _logger.Debug($"Found user {domainUser.UserName} is not found");
                }
                const string password = "ca57092a-6513-4b6e-ad02-2186fdd32270";
                var domain = _adService.GetDomain(domainUser.DomainName);
                _logger.Debug($"Found domain {domainUser.DomainName}:{domain.Id}");

                var userId = _adService.GetUserIdByDomainNameAndUserName(domain.Name, domainUser.UserName);
                _logger.Debug($"Found user {domainUser.UserName}:{userId}");

                var groups = _ssoService.GetGroupsByPublisherIdAndDomainId(userId, domain.Id).ToList();
                foreach (var group in groups)
                {
                    _logger.Debug($"User {domainUser.UserName} is in {group.Name}:{group.Id} group");
                }

                // ToDo: need refactoring
                if (!groups.Any() && user != null)
                {
                    var publisher = _userService.GetUserById(user.Id);
                    _userService.RemovePublisher(publisher);
                    _logger.Info($"Publisher {user.Name}:{user.Id} was deleted");
                    var policiesPublisher = _policyService.GetPolicyPublisherByPublisherId(publisher.Id);
                    foreach (var policyPublisher in policiesPublisher)
                    {
                        _policyService.RemovePolicyPublisher(policyPublisher);
                    }
                }
                else if (groups.Any())
                {
                    var publisherId = user?.Id ?? _userService.RegisterPublisher(domainUser.UserName, domain.Id, password);
                    var policies = _policyService.GetPoliciesByPublisherId(publisherId);
                    if (groups.Count < policies.Count)
                    {
                        foreach (var policy in policies)
                        {
                            if (groups.All(x => x.Policy.Id != policy.Id))
                            {
                                _policyService.RemovePolicyPublisherByPolicyIdPublisherId(policy.Id, publisherId);
                            }
                        }
                    }

                    if (groups.Count > policies.Count)
                    {
                        var publisherPolicies = new List<DeskAlerts.ApplicationCore.Entities.PolicyPublisher>();
                        foreach (var group in groups)
                        {
                            if (policies.All(x => x.Id != group.Policy.Id) && publisherPolicies.All(x => x.PolicyId != group.Policy.Id))
                            {
                                var publisherPolicy = _policyService.AddPolicyPublisher(publisherId, group.Policy.Id, group.Id);
                                publisherPolicies.Add(publisherPolicy);
                            }
                        }
                    }

                    user = GetUserByNameByRepository(domainUser.UserName);
                    user.IsSsoAuthorized = true;
                }
            }

            return user;
        }

        private bool IsUserSuperAdmin(DeskAlerts.ApplicationCore.Entities.User user)
        {
            return user.Username.Equals(SuperAdminUsername) &&
                   user.Role.Equals(DeskAlerts.ApplicationCore.Entities.User.AdminRole);
        }
    }
}