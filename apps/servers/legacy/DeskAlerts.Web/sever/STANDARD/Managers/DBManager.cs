﻿using DeskAlerts.ApplicationCore.Utilities;
using DeskAlertsDotNet.Exceptions;
using DeskAlertsDotNet.Extentions;
using DeskAlertsDotNet.Pages;
using DeskAlertsDotNet.Parameters;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;

namespace DeskAlertsDotNet.DataBase
{
    public class DataRow
    {
        private Dictionary<string, object> row;

        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public DataRow(Dictionary<string, object> dict)
        {
            row = dict;
        }

        public object GetValue(string field)
        {
            try
            {
                return row[field];
            }
            catch (Exception ex)
            {
                _logger.Trace(ex);
                throw;
            }
        }

        public object this[string key]
        {
            get { return this.GetValue(key); }
        }

        public int GetInt(string field, int defaultValue)
        {
            if (IsNull(field))
                return defaultValue;

            return Convert.ToInt32(GetValue(field));
        }

        public int GetInt(string field)
        {
            try
            {
                return Convert.ToInt32(GetValue(field));
            }
            catch (Exception ex)
            {
                _logger.Debug(ex);
                return 0;
            }
        }

        public void DeleteByKey(string field)
        {
            if (string.IsNullOrEmpty(field))
            {
                _logger.Debug($"Parameter \"{nameof(field)}\" is empty.");
                return;
            }

            try
            {
                row.Remove(field);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }

        public bool IsNull(string field)
        {
            object val = GetValue(field);

            return val == null || val.GetType() == typeof(DBNull);
        }

        public string GetString(string field)
        {
            try
            {
                return Convert.ToString(GetValue(field));
            }
            catch (Exception ex)
            {
                _logger.Trace(ex);
                return string.Empty;
            }
        }

        public bool GetBool(string field)
        {
            try
            {
                return Convert.ToBoolean(GetValue(field));
            }
            catch (Exception ex)
            {
                _logger.Debug(ex);
                return false;
            }
        }

        public DateTime GetDateTime(string field)
        {
            try
            {
                return Convert.ToDateTime(GetValue(field), CultureInfo.InvariantCulture);
            }
            catch (Exception ex)
            {
                _logger.Debug(ex);
                return DateTime.MinValue;
            }
        }

        public Dictionary<string, object> ToDictionary()
        {
            return row;
        }

        public override string ToString()
        {
            string str = "";

            foreach (KeyValuePair<string, object> pair in row)
            {
                str += ("key = " + pair.Key + " value = " + pair.Value + "<br>");
            }

            return str;
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this.row, Formatting.Indented);
        }

        public JObject ToJsonObject()
        {
            JObject obj = new JObject();

            foreach (var param in row)
            {
                try
                {
                    var property = new JProperty(param.Key, param.Value);
                    obj.Add(property);
                }
                catch (Exception ex)
                {
                    _logger.Debug(ex);
                    throw new Exception("[JSON PARSER]: Bad value " + param.Value.ToString().DoubleQuotes());
                }
            }

            return obj;
        }
    }

    public class DataSet
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private List<DataRow> collection;

        public DataSet Empty
        {
            get
            {
                return new DataSet();
            }
        }

        public DataSet() : this(new List<DataRow>())
        {
        }

        public DataSet(List<DataRow> list)
        {
            collection = list;
        }

        public void Add(DataRow row)
        {
            collection.Add(row);
        }

        public void Remove(DataRow row)
        {
            collection.Remove(row);
        }

        public bool IsEmpty { get { return collection.Count == 0; } }

        public string JoinResult(string delimeter, string colName)
        {
            string[] collArray = this.ToArray().Select(dr => dr.GetString(colName)).ToArray();

            return string.Join(delimeter, collArray);
        }

        public object GetValue(int row, string field)
        {
            try
            {
                DataRow rowDict = collection[row];

                object val = null;

                val = rowDict[field];

                if (val is DBNull)
                    return null;
                return val;
            }
            catch (Exception ex)
            {
                _logger.Debug(ex);
                throw;
            }
        }

        public string GetString(int row, string field)
        {
            object val = GetValue(row, field);

            if (val == null)
                return string.Empty;
            return Convert.ToString(val);
        }

        public bool IsNull(int row, string field)
        {
            object val = GetValue(row, field);

            return val == null || val.GetType() == typeof(DBNull);
        }

        public DateTime GetDateTime(int row, string field)
        {
            object val = GetValue(row, field);

            try
            {
               return Convert.ToDateTime(val, CultureInfo.CurrentUICulture);
            }
            catch (Exception ex)
            {
                _logger.Debug(ex);
                return DateTime.Now;
            }
        }

        public int GetInt(int row, string field)
        {
            object val = GetValue(row, field);

            if (val == null || val.ToString().Length == 0)
                return 0;

            return Convert.ToInt32(val);
        }

        public double GetDouble(int row, string field)
        {
            var val = GetValue(row, field);

            if (val == null || val.ToString().Length == 0)
                return 0;

            return Convert.ToDouble(val);
        }

        public bool GetBool(int row, string field)
        {
            object val = GetValue(row, field);

            if (val == null)
                return false;

            return Convert.ToBoolean(val);
        }

        public DataRow GetRow(int index)
        {
            return collection[index];
        }

        public List<T> GetCollumnValues<T>(string collumn, bool distinct = false)
        {
            List<T> values = new List<T>();
            foreach (DataRow row in collection)
            {
                values.Add(((T)row.GetValue(collumn)));
            }

            if (distinct)
                values = values.Distinct().ToList();

            return values;
        }

        public List<DataRow> ToList()
        {
            return this.collection;
        }

        public DataRow[] ToArray()
        {
            return this.collection.ToArray();
        }

        public List<string> ToList(string column)
        {
            return collection.Select(x => x.GetString(column)).ToList();
        }

        public DataSetEnumerator GetEnumerator()
        {
            return new DataSetEnumerator(this);
        }

        public int Count { get { return collection.Count; } }

        public string ToJson()
        {
            JsonSerializerSettings settings = new JsonSerializerSettings { Converters = new[] { new DeskAlertsDataBaseJsonWorker() } };
            return JsonConvert.SerializeObject(this, settings);
        }

        public JArray ToJsonArray()
        {
            var array = new JArray();

            foreach (var dataRow in collection)
            {
                array.Add(dataRow.ToJsonObject());
            }

            return array;
        }

        public class DataSetEnumerator
        {
            private DataSet collection;
            private int index;

            public DataSetEnumerator(DataSet collection)
            {
                this.collection = collection;
                index = -1;
            }

            public bool MoveNext()
            {
                index++;

                return index < collection.Count;
            }

            public DataRow Current
            {
                get
                {
                    return collection.GetRow(index);
                }
            }
        }

        public DataRow First()
        {
            try
            {
                return GetRow(0);
            }
            catch (Exception ex)
            {
                _logger.Debug(ex);
                return null;
            }
        }
    }

    public class DBManager : IDisposable
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private SqlConnection connection;

        public DataSet GetDataByQuery(string sqlQuery, params SqlParameter[] parameters)
        {
            List<DataRow> arrayList = new List<DataRow>();

            try
            {
                using (SqlCommand cmd = new SqlCommand(sqlQuery, this.connection))
                {
                    foreach (SqlParameter parameter in parameters)
                    {
                        cmd.Parameters.Add(parameter);
                    }

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Dictionary<string, object> row = new Dictionary<string, object>();

                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                if (!row.ContainsKey(reader.GetName(i)))
                                {
                                    row.Add(reader.GetName(i), reader.GetValue(i));
                                }
                            }

                            DataRow dataRow = new DataRow(row);
                            arrayList.Add(dataRow);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Debug(ex);
                throw new DeskAlertsDBException(sqlQuery, ex.Message);
            }

            return new DataSet(arrayList);
        }

        public string Update(string tableName, Dictionary<string, object> collumnValueDictionary,
            params Dictionary<string, object>[] conditions)
        {
            var collumns = collumnValueDictionary.Keys.ToArray();
            var values = CorrectValues(collumnValueDictionary);
            string query = "UPDATE " + tableName + " SET ";

            for (int i = 0; i < values.Length; i++)
            {
                query += (collumns[i] + " = " + values[i]);

                if (i != values.Length - 1)
                    query += " , ";
            }

            if (conditions.Length > 0)
            {
                query += " WHERE ";

                int conditionNumber = 0;
                foreach (Dictionary<string, object> condition in conditions)
                {
                    query += "( ";
                    var conditionCollumns = condition.Keys.ToArray();
                    var conditionValues = CorrectValues(condition);

                    for (int i = 0; i < conditionValues.Length; i++)
                    {
                        query += (conditionCollumns[i] + " = " + conditionValues[i]);

                        if (i < conditionCollumns.Length - 1)
                            query += " AND ";
                    }

                    query += " )";

                    if (conditionNumber < conditions.Length - 1)
                        query += " OR ";
                }
            }

            ExecuteQuery(query);

            return query;
        }

        public int Insert(string tableName, Dictionary<string, object> collumnValueDictionary,
            string returnCollumn = null)
        {
            var collumns = collumnValueDictionary.Keys.ToArray();
            var values = CorrectValues(collumnValueDictionary);
            string query = "INSERT INTO " + tableName;

            query += " (";

            query += string.Join(",", collumns);

            query += ") ";

            if (!string.IsNullOrEmpty(returnCollumn))
            {
                query += string.Format("OUTPUT INSERTED.{0} as returnVal ", returnCollumn);
            }

            query += " VALUES (";
            query += string.Join(",", values);
            query += " )";

            ExecuteQuery("SET LANGUAGE BRITISH");
            DataSet insertedRow = GetDataByQuery(query);

            if (!string.IsNullOrEmpty(returnCollumn))
                return insertedRow.GetInt(0, "returnVal");

            return 0;
        }

        private object[] CorrectValues(Dictionary<string, object> alertDict)
        {
            var values = alertDict.Values.ToArray();

            for (int i = 0; i < values.Length; i++)
            {
                object value = values[i];
                if (value == null)
                {
                    values[i] = "NULL";
                }
                else if (value is string)
                {
                    string stringValue = value.ToString();

                    stringValue = stringValue.Replace("'", "''");

                    DateTime outTime = DateTime.MinValue;

                    if (DateTime.TryParse(stringValue, out outTime))
                    {
                        stringValue = string.Format("'{0}'", stringValue);
                    }
                    else
                    {
                        stringValue = string.Format("N'{0}'", stringValue);
                    }

                    values[i] = stringValue;
                }
            }
            return values;
        }

        public void Dispose()
        {
            if (connection != null)
            {
                connection.Close();
                connection.Dispose();
            }
        }

        private string GetConnectionString(string host, string login, string password, string database, bool isWindowsAuth, bool isAzureSql)
        {
            password = Encoding.UTF8.GetString(Convert.FromBase64String(password));

            string connectionString;
            if (isAzureSql)
            {
                connectionString =
                    $"Server=tcp:{host}.database.windows.net,1433;Initial Catalog={database};Persist Security Info=False;User ID={login};Password={password};Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
            }
            else if (isWindowsAuth)
            {
                connectionString =
                    $"Data Source={host};Initial Catalog={database};TrustServerCertificate=True;Integrated Security=SSPI;Max Pool Size=1000000;";
            }
            else
            {
                connectionString =
                    $"Data Source={host};Initial Catalog={database};User ID={login};Password={password};TrustServerCertificate=True;Max Pool Size=1000000;";
            }

            return connectionString;
        }

        public T GetScalarByQuery<T>(string sqlQuery, params SqlParameter[] parameters)
        {
            return (T)Convert.ChangeType(GetScalarByQuery(sqlQuery, parameters), typeof(T));
        }

        public string str { get; set; }

        public DBManager()
        {
            bool isWindowsAuth = Config.Data.GetInt("winauth") == 1;
            bool isAzureSql = Config.Data.GetInt("azuresql") == 1;
            string dataSource = Config.Data.GetString("source");
            string dbName = Config.Data.GetString("dbname");
            string user = Config.Data.GetString("user");
            string password = Config.Data.GetString("password");
            string[] lines = { password };

            string connString = GetConnectionString(dataSource, user, password, dbName, isWindowsAuth, isAzureSql);

            if (connString != String.Empty)
            {
                connection = new SqlConnection(connString);
                connection.Open();
            }
        }

        public DataTable GetDataTableByQuery(string sqlQuery)
        {
            var dt = new DataTable();
            var adapter = new SqlDataAdapter(sqlQuery, connection);
            adapter.Fill(dt);
            return dt;
        }

        public List<DataSet> GetMultipleResult(string sqlQuery)
        {
            List<DataSet> result = new List<DataSet>();
            List<DataRow> arrayList = new List<DataRow>();
            try
            {
                using (SqlCommand cmd = new SqlCommand(sqlQuery, connection))
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                Dictionary<string, object> row = new Dictionary<string, object>();

                                for (int i = 0; i < reader.FieldCount; i++)
                                {
                                    row.Add(reader.GetName(i), reader.GetValue(i));
                                }

                                DataRow dataRow = new DataRow(row);
                                arrayList.Add(dataRow);

                                DataSet dset = new DataSet(arrayList);

                                result.Add(dset);
                            }

                            reader.NextResult();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Debug(ex);
                throw new DeskAlertsDBException(sqlQuery, ex.Message);
            }

            return result;
        }

        public int ExecuteQuery(string query, params SqlParameter[] parameters)
        {
            if (string.IsNullOrEmpty(query))
            {
                return -1;
            }
            try
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    foreach (SqlParameter parameter in parameters)
                    {
                        command.Parameters.Add(parameter);
                    }

                    int result = command.ExecuteNonQuery();
                    return result;
                }
            }
            catch (Exception ex)
            {
                _logger.Debug(ex);
                throw new DeskAlertsDBException(query, ex.Message);
            }
        }

        public T GetScalarQuery<T>(string sqlQuery, params SqlParameter[] parameters)
        {
            return (T)Convert.ChangeType(GetScalarByQuery(sqlQuery, parameters), typeof(T));
        }

        public object GetScalarByQuery(string sqlQuery, params SqlParameter[] parameters)
        {
            try
            {
                using (var command = new SqlCommand(sqlQuery, connection))
                {
                    foreach (var parameter in parameters)
                    {
                        command.Parameters.Add(parameter);
                    }

                    object result = command.ExecuteScalar();
                    return result;
                }
            }
            catch (Exception ex)
            {
                _logger.Debug(ex);
                throw new DeskAlertsDBException(sqlQuery, ex.Message);
            }
        }
    }
}