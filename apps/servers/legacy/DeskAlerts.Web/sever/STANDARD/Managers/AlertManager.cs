﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DeskAlertsDotNet.DataBase;

namespace DeskAlertsDotNet
{
    public class AlertManager
    {
        private static AlertManager _instance;

        public static AlertManager Default
        {
            get
            {
                if (_instance == null)
                    return _instance = new AlertManager();

                return _instance;
            }
        }


        public void UpdateAlert(DBManager dbMgr, Dictionary<string, object> alertDict, int alertId)
        {
            var collumns = alertDict.Keys.ToArray();

            var values = CorrectValues(alertDict);
            string collsQueryPart = String.Join(",", collumns);

            string query = "UPDATE alerts SET ";

            for (int i = 0; i < alertDict.Count; i++)
            {
                query += collumns[i] + "=" + values[i];

                if (i != alertDict.Count - 1)
                     query += ",";

                 
            }

            query += " WHERE id = " + alertId;

            dbMgr.ExecuteQuery("SET LANGUAGE BRITISH");
            dbMgr.ExecuteQuery(query);

        }
        public int AddAlert(DBManager dbMgr, Dictionary<string, object> alertDict)
        {
            var collumns = alertDict.Keys.ToArray();

            var values = CorrectValues(alertDict);
            string collsQueryPart = String.Join("," , collumns);
            string query = "INSERT INTO alerts (";

            query += collsQueryPart;

            query += ") OUTPUT INSERTED.id as newId VALUES (";

            query += string.Join(",", values);

            query += ")";

            dbMgr.ExecuteQuery("SET LANGUAGE BRITISH");
            DataSet insertedRow = dbMgr.GetDataByQuery(query);

            
            return insertedRow.GetInt(0, "newId");
        }

        private object[] CorrectValues(Dictionary<string, object> alertDict)
        {
            var values = alertDict.Values.ToArray();

            for (int i = 0; i < values.Length; i++)
            {
                object value = values[i];
                if (value == null)
                {
                    values[i] = "NULL";
                }
                else if (value is string)
                {
                    string stringValue = value.ToString();

                    stringValue = stringValue.Replace("'", "''");

                    DateTime outTime = DateTime.MinValue;

                    if (DateTime.TryParse(stringValue, out outTime))
                    {
                        stringValue = string.Format("'{0}'", stringValue);
                    }
                    else
                    {
                        stringValue = string.Format("N'{0}'", stringValue);
                    }

                    values[i] = stringValue;
                }
            }
            return values;
        }
    }
}