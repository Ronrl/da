<!-- #include file="admin/functions_inc.asp" -->
<%
if smsPhoneFormat = "" then smsPhoneFormat = "1"
if smsPhoneFormat <> "%smsPhoneFormat%" and smsPhoneFormat <> "0" then
%>

<script language="JScript" runat="server" src="admin/jscripts/phone_format.js"></script>

<script language="JScript" runat="server" src="admin/jscripts/da_phone_format.js"></script>

<%
end if

if textcallPhoneFormat = "" then textcallPhoneFormat = "1"
if textcallPhoneFormat <> "%textcallPhoneFormat%" and textcallPhoneFormat <> "0" then
%>

<script language="JScript" runat="server" src="admin/jscripts/phone_format.js"></script>

<script language="JScript" runat="server" src="admin/jscripts/da_phone_format.js"></script>

<%
end if

Set ConnAPI = Conn

function apiDbError()
	ConnAPI.RollbackTrans 
	response.write apiResult (api_method, "ERROR", "Wrong data for database", "")
	response.end
end function

function Lng(num)
	Lng = 0
	if VarType(num) > 1 then
		if num <> "" then
			Lng = CLng(num)
		end if
	end if
end function

'API : alert send method
function apiParseSendAlert(psaXML)
	alertBroadcast = 0
	' 0 - title, 1 - text, 2 - template_id, 3 - desktop, 4 - sms, 5 - email, 16- text to call
	alertData = Array("", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "")
	alertRecipients = Array("", "", "", "", "") ' 0 - domain_name, 1 - users_ids, 2 - groups_ids, 3 - comp_ids, 4 - ou_ids
	Set objLst = psaXML.getElementsByTagName("alert")
	intNoOfHeadlines = objLst.length -1
	For iter = 0 To (intNoOfHeadlines)
		Set     objHdl = objLst.item(iter)
		for each child in objHdl.childNodes
		Select case lcase(child.nodeName)
			case "data"
				alertData = apiParseAlertData(child)
		    case "instant"
				instantAlertId = apiParseInstantAlert(child)
			case "broadcast"
				alertBroadcast = 1
			case "recipients"
				alertRecipients = apiParseAlertRecipients(child)
		End Select
		next
		if (instantAlertId <> "") then
		    apiSendInstantAlert instantAlertId
		else
		    apiSendAlert alertData, alertBroadcast, alertRecipients
		end if
	next
end function

function apiParseDeleteAlert(psaXML)
	Set objLst = psaXML.getElementsByTagName("deletealert")
	intNoOfHeadlines = objLst.length -1
	For iter = 0 To (intNoOfHeadlines)
		Set     objHdl = objLst.item(iter)
		for each child in objHdl.childNodes
		Select case lcase(child.nodeName)
			case "id"
				id = child.text
		End Select
		next
		apiDeleteAlert id
	next
end function

function apiDeleteAlert(id)
	ConnAPI.Execute("DELETE FROM alerts WHERE id=" & id)
	ConnAPI.Execute("DELETE FROM archive_alerts WHERE id=" & id)

	ConnAPI.Execute("DELETE FROM alerts_comp_stat WHERE alert_id=" & id)
	ConnAPI.Execute("DELETE FROM alerts_group_stat WHERE alert_id=" & id)
	ConnAPI.Execute("DELETE FROM alerts_ou_stat WHERE alert_id=" & id)
	ConnAPI.Execute("DELETE FROM alerts_read WHERE alert_id=" & id)
	ConnAPI.Execute("DELETE FROM alerts_received WHERE alert_id=" & id)
	ConnAPI.Execute("DELETE FROM alerts_received_recur WHERE alert_id=" & id)
	ConnAPI.Execute("DELETE FROM alerts_sent WHERE alert_id=" & id)
	ConnAPI.Execute("DELETE FROM alerts_sent_comp WHERE alert_id=" & id)
	ConnAPI.Execute("DELETE FROM alerts_sent_deskbar WHERE alert_id=" & id)
	ConnAPI.Execute("DELETE FROM alerts_sent_group WHERE alert_id=" & id)
	ConnAPI.Execute("DELETE FROM alerts_sent_iprange WHERE alert_id=" & id)
	ConnAPI.Execute("DELETE FROM alerts_sent_stat WHERE alert_id=" & id)

	ConnAPI.Execute("DELETE FROM archive_alerts_comp_stat WHERE alert_id=" & id)
	ConnAPI.Execute("DELETE FROM archive_alerts_group_stat WHERE alert_id=" & id)
	ConnAPI.Execute("DELETE FROM archive_alerts_read WHERE alert_id=" & id)
	ConnAPI.Execute("DELETE FROM archive_alerts_received WHERE alert_id=" & id)
	ConnAPI.Execute("DELETE FROM archive_alerts_sent WHERE alert_id=" & id)
	ConnAPI.Execute("DELETE FROM archive_alerts_sent_comp WHERE alert_id=" & id)
	ConnAPI.Execute("DELETE FROM archive_alerts_sent_group WHERE alert_id=" & id)
	ConnAPI.Execute("DELETE FROM archive_alerts_sent_stat WHERE alert_id=" & id)
	
	ConnAPI.Execute("DELETE FROM [alerts_cache_users] WHERE alert_id = " & id)
	ConnAPI.Execute("DELETE FROM [alerts_cache_comps] WHERE alert_id = " & id)
	ConnAPI.Execute("DELETE FROM [alerts_cache_ips] WHERE alert_id = " & id)
	response.write apiResult ("deletealert", "SUCCESS", "", id)	
end function

function apiParseGetAlert(psaXML)
	Set objLst = psaXML.getElementsByTagName("getalert")
	intNoOfHeadlines = objLst.length -1
	For iter = 0 To (intNoOfHeadlines)
		Set     objHdl = objLst.item(iter)
		for each child in objHdl.childNodes
		Select case lcase(child.nodeName)
			case "username"
				username = child.text
			case "hash"
				hash = child.text
			case "computer"
				computer = child.text
			case "domain"
				domain = child.text
			case "count"
				count = child.text
			case "ticker"
				ticker = child.text
		End Select
		next
		apiGetAlert username, hash, computer, domain, count, ticker
	next
end function


function apiGetAlert(username, hash, computer, domain, count, ticker)
	'check hash
	if(computer <> "" OR domain <> "") then
		newHash = MD5(username&computer&domain&ConfAPISecret)
	else
		newHash = MD5(username&ConfAPISecret)
	end if

	if StrComp(hash, newHash, 1) = 0 then
		'TODO: getAlertFunction username, computer, domain, count, ticker
	else
		response.write apiResult (api_method, "ERROR", "Not valid hash for username", 0)
	end if
end function

function apiParseInstantAlert(paData)
	apiPAD = ""
	for each child in paData.childNodes
		Select case lcase(child.nodeName)
			case "alertid"
				apiPAD = child.text
		End Select
	next
	apiParseInstantAlert = apiPAD
end function

function apiParseAlertData(paData)
	apiPAD = Array("", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "","")
    Dim htmlTitle
	for each child in paData.childNodes
		Select case lcase(child.nodeName)
			case "title"
				apiPAD(0) = child.text
                htmlTitle = apiPAD(0)
			case "text"
				apiPAD(1) = child.text
			case "template_id"
				apiPAD(2) = child.text
			case "desktop"
				apiPAD(3) = child.text
			case "sms"
				apiPAD(4) = child.text
			case "email"
				apiPAD(5) = child.text
			case "urgent"
				apiPAD(6) = child.text
			case "lifetime"
				apiPAD(7) = child.text
			case "width"
				apiPAD(8) = child.text
			case "height"
				apiPAD(9) = child.text
			case "autoclose"
				apiPAD(10) = child.text
			case "manualclose"
				apiPAD(11) = child.text
			case "email_sender"
				apiPAD(12) = child.text
			case "ticker"
				apiPAD(13) = child.text
			case "fullscreen"
				apiPAD(14) = child.text
			case "acknow"
				apiPAD(15) = child.text
			case "text_to_call"
				apiPAD(16) = child.text	
			case "startdate"
				apiPAD(17) = child.text	
			case "enddate"
				apiPAD(18) = child.text
		End Select
	next
    apiPAD(1) = apiPAD(1) & "<!-- html_title = '" & htmlTitle & "' -->"
	apiParseAlertData = apiPAD
end function

function apiParseAlertRecipients(paRecipients)
	apiPAR = Array("", "", "", "", "", "")
	user_fl = 0
	group_fl = 0
	comp_fl = 0
	ou_fl = 0
	ipgroups_fl = 0
	for each child in paRecipients.childNodes
		Select case lcase(child.nodeName)
			case "domain"
				for each childDomain in child.childNodes
					Select case lcase(childDomain.nodeName)
						case "recipient"
							If childDomain.Attributes.length > 0 Then
								For Each oAttr In childDomain.Attributes
									if(oAttr.Name="name") then
										recipientName = trim(oAttr.Value)
									elseif(oAttr.Name="type") then
										recipientType = oAttr.Value
									end if
								Next 
							End If
							if(recipientType="user") then
								if(user_fl=0) then
									apiPAR(1) = apiPAR(1) & recipientName
									user_fl = 1
								else
									apiPAR(1) = apiPAR(1) & ", " & recipientName
								end if
							elseif(recipientType="group") then
								if(group_fl=0) then
									apiPAR(2) = apiPAR(2) & recipientName
									group_fl = 1
								else
									apiPAR(2) = apiPAR(2) & ", " & recipientName
								end if
							elseif(recipientType="computer") then
								if(comp_fl=0) then
									apiPAR(3) = apiPAR(3) & recipientName
									comp_fl = 1
								else
									apiPAR(3) = apiPAR(3) & ", " & recipientName
								end if
							elseif(recipientType="ou") then
								if(ou_fl=0) then
									apiPAR(4) = apiPAR(4) & recipientName
									ou_fl = 1
								else
									apiPAR(4) = apiPAR(4) & ", " & recipientName
								end if
							elseif(recipientType="ipgroup") then
								if(ipgroups_fl=0) then
									apiPAR(5) = apiPAR(5) & recipientName
									ipgroups_fl = 1
								else
									apiPAR(5) = apiPAR(5) & ", " & recipientName
								end if
							end if
					End Select
				next
				domainName = trim(child.Attributes(0).Value) 'only one attribute, change this if will be more
		End Select
	next
	apiPAR(0) = domainName
	apiParseAlertRecipients = apiPAR
end function

function apiSendInstantAlert(instantId)

    Dim objXmlHttpMain , URL

    strJSONToSend = "{""id"":"&instantId&", ""buildCache"":""1"", ""senderId"":""0""}"

    URL=alerts_folder&"/ApiRequestHandler.aspx/send" 

    Set objXmlHttpMain = CreateObject("Msxml2.ServerXMLHTTP") 

    objXmlHttpMain.open "POST",URL, False 
    objXmlHttpMain.setRequestHeader "Authorization", "Bearer <api secret id>"
    objXmlHttpMain.setRequestHeader "Content-Type", "application/json"
    objXmlHttpMain.send strJSONToSend

    set objJSONDoc = nothing 
    set objResult = nothing
			
	response.write apiResult (api_method, "SUCCESS", "", 0)	
end function

function apiSendAlert(saData, saBroadcast, saRecipients)
	salertTitle = saData(0)
	salertText =  saData(1) 
	salertTemplateId = saData(2)
	salertDesktop = saData(3)
	salertSMS = saData(4)
	salertEmail = saData(5)

	saUrgent = saData(6)
	saLifetime = saData(7)
	saWidth = saData(8)
	saHeight = saData(9)
	saAutoclose = saData(10)
	saManualclose = saData(11)
	saEmailSender = saData(12)
	saTicker = saData(13)
	saFullscreen = saData(14)
	saAcknow = saData(15)
    saTextCall = saData(16)
    saStartDate = saData(17)
    saEndDate = saData(18)
    

	if(EM<>1) then
		salertEmail = 0
	end if
	if(SMS<>1) then
		salertSMS = 0
	end if
	if(TEXT_TO_CALL<>1) then
		salertTextCall = 0
	end if
	if(salertDesktop="") then
		salertDesktop = 0
	end if
	if(salertEmail="") then
		salertEmail = 0
	end if
	if(salertSMS="") then
		salertSMS = 0
	end if
	if(salertTextCall="") then
		salertTextCall = 0
	end if
	if(salertDesktop=0 AND salertEmail=0 AND salertSMS=0 AND salertTextCall=0) then
		salertDesktop = 1
	end if
	
	if(salertTemplateId<>"") then
		Set rsCheckTemplate = ConnAPI.Execute("SELECT id FROM templates WHERE id=" & salertTemplateId)
		if rsCheckTemplate.EOF then
			salertTemplateId = 1
		end if
	else
		salertTemplateId = 1
	end if
	
	salertBroadcast = saBroadcast
    salertToUsers = 0
	salertToGroups = 0
	salertToComps = 0
	
	if(salertBroadcast=1) then
		salertType2 = "B"
	elseif(saRecipients(5) <> "") then
	    salertType2 = "I"
	    'saDomainName = saRecipients(5)
	else
		salertType2 = "R"
		
		saDomainName = saRecipients(0)
	end if
	'create alert
	
	type2 = salertType2
	
	'default settings
	if saWidth = "" then
		saWidth = ConfAlertWidth
	end if
    if saWidth < 340 then
        response.write apiResult (api_method, "ERROR","The alert width should be more than 340", "")
        response.end
    end if
	if saHeight = "" then
		saHeight = ConfAlertHeight
	end if
    if saHeight < 290 then
        response.write apiResult (api_method, "ERROR","The alert height should be more than 290", "")
        response.end
    end if
	if saUrgent = "" then
		saUrgent = ConfUrgentByDefault
	end if
	if saAcknow = "" then
		saAcknow = ConfAcknowledgementByDefault
	end if
	if saTicker = "" then
		if(ConfDesktopAlertType = "T") then
			saTicker = 1
		else
			saTicker = 0
		end if
	end if
	if saFullscreen = "" then
		if(ConfPopupType = "F") then
			saFullscreen = 1
		else
			saFullscreen = 6
		end if
	end if
	if saLifetime = "" then
		saLifetime = ConfMessageExpire
	end if
	if saEmailSender = "" and salertEmail <> 0 then
		saEmailSender = ConfDefaultEmail
	end if
	
	if saAutoclose = "" then
		saAutoclose = 0
		if ConfAutocloseByDefault = "1" then
			saAutoclose = ConfAutocloseValue
		end if
	else
		saAutoclose = Abs(Lng(saAutoclose)*60)
	end if
	if saManualclose = "" then
		saManualclose = ConfAllowManualCloseByDefault
	end if
	if saManualclose = "1" then
		saAutoclose = -1 * Lng(saAutoclose)
	end if
	
	saSchedule = 0
	saSchedule_type = 1
	saFromDate = ""
	saToDate = ""
	if saStartDate<>"" then
		saFromDate = DateAdd("h", 0, saStartDate)
		if saEndDate<>"" then
			saToDate = DateAdd("h", 0, saEndDate)
		else
			saToDate = DateAdd("h", 8, saFromDate)
		end if
		saSchedule = 1
	else
		if Lng(saLifetime) > 0 then
			saFromDate = now_db
			saToDate = DateAdd("s", 1, DateAdd("n", saLifetime, saFromDate))
			saSchedule = 1
		end if
	end if
	saDeskalertsMode = 1
	
	salertUsersString = replace(saRecipients(1), ", " ,"' OR name = N'")
	salertGroupsString = replace(saRecipients(2), ", ","' OR name = N'")
	salertComputersString = replace(saRecipients(3), ", " ,"' OR name = N'")
	salertOUsString = replace(saRecipients(4), ", " ,"' OR OU_PATH = N'")

	'----
	createSQL = "INSERT INTO alerts (title, alert_text, template_id, type, type2, create_date, sent_date, schedule, schedule_type, urgent, toolbarmode, deskalertsmode, sender_id, aknown, ticker, fullscreen, alert_width, alert_height, desktop, sms, email, recurrence, autoclose, from_date, to_date, group_type, email_sender, resizable, post_to_blog, social_media, self_deletable, approve_status) VALUES (N'"&Replace(salertTitle, "'", "''")&"', N'"&Replace(salertText, "'", "''")&"', "&Lng(salertTemplateId)&", 'S', '"&type2&"', GETDATE(), GETDATE(), '"&Lng(saSchedule)&"', '"&Lng(saSchedule_type)&"', '"&Lng(saUrgent)&"', '0', '1', 1, '"&Lng(saAcknow)&"', '"&Lng(saTicker)&"','"&Lng(saFullscreen)&"', "&Lng(saWidth)&", "&Lng(saHeight)&", '"&Lng(salertDesktop)&"', '"&Lng(salertSMS)&"', '"&Lng(salertEmail)&"', '0', '"&Lng(saAutoclose)&"', '"&Replace(saFromDate, "'", "''")&"', '"&Replace(saToDate, "'", "''")&"', 'B', N'"&Replace(saEmailSender, "'", "''")&"', 0,0,0,0, 1)"
	ConnAPI.Execute(createSQL)
	if(salertType2="B" AND (salertSMS=1 OR salertEmail=1 OR salertTextCall=1)) then
		Set rsBroad = ConnAPI.Execute("SELECT name, email, mobile_phone FROM users WHERE role='U'")
		do while not rsBroad.EOF
			if(salertSMS=1) then
				if(Not IsNull(rsBroad("mobile_phone"))) then
					if(len(rsBroad("mobile_phone"))>0) then
						Dim oXMLHTTP, sPostData, sResult
						sMobileNo = rsBroad("mobile_phone")
						sText = Replace(DeleteTags(salertText), "&amp;", "&")
						sText = replace(sText, "%username%", rsBroad("name"))
						sendAlertSMS sText, sMobileNo
					end if
				end if
			end if
			if(salertEmail=1) then
				if(Not IsNull(rsBroad("email"))) then
					if(len(rsBroad("email"))>0) then
						user_email = rsBroad("email")
						user_name = rsBroad("name")
						text = salertText
						text = replace(text, "%username%", rsBroad("name"))
						subject = salertTitle
						sendAlertEmail subject, user_email, user_name, makeFullImagePaths(text), senderName
					end if
				end if
			end if
			if(salertTextCall=1) then
				if(Not IsNull(rsBroad("mobile_phone"))) then
					if(len(rsBroad("mobile_phone"))>0) then
						sMobileNo = rsBroad("mobile_phone")
						sText = Replace(DeleteTags(salertText), "&amp;", "&")
						sText = replace(sText, "%username%", rsBroad("name"))
						sendAlertTextCall sText, sMobileNo
					end if
				end if
			end if
			rsBroad.MoveNext
		loop
	elseif(salertType2="B") then
		salertId = 0
		Set rs1 = ConnAPI.Execute("select @@IDENTITY newID from alerts")
		salertId=rs1("newID")
		rs1.Close
		if(IsNull(salertId)) then
			Set rs1 = ConnAPI.Execute("select MAX(id) as newID from alerts")
			salertId=rs1("newID")
			rs1.Close
		end if
             MemCacheUpdate "Add", salertId
	elseif(salertType2="R") then
		'get new id
		salertId = 0
		Set rs1 = ConnAPI.Execute("select @@IDENTITY newID from alerts")
		salertId=rs1("newID")
		rs1.Close
		if(IsNull(salertId)) then
			Set rs1 = ConnAPI.Execute("select MAX(id) as newID from alerts")
			salertId=rs1("newID")
			rs1.Close
		end if
		'add recipients
		if(salertId <> "0") then
		
			Set rsDomain = ConnAPI.Execute ("select id from domains where name='"&saDomainName&"'")
			if(Not rsDomain.EOF) then
				salertDomainId = rsDomain("id")
				alert_id = salertId
				sendSQL="SET NOCOUNT ON " & vbCrLf &_
					" IF OBJECT_ID('tempdb..#tempOUs') IS NOT NULL DROP TABLE #tempOUs" & vbCrLf &_
					" CREATE TABLE #tempOUs  (Id_child BIGINT NOT NULL);" & vbCrLf &_
					" IF OBJECT_ID('tempdb..#tempUsers') IS NOT NULL DROP TABLE #tempUsers" & vbCrLf &_
					" CREATE TABLE #tempUsers  (user_id BIGINT NOT NULL);" & vbCrLf &_
					" IF OBJECT_ID('tempdb..#tempComputers') IS NOT NULL DROP TABLE #tempComputers" & vbCrLf &_
					" CREATE TABLE #tempComputers  (comp_id BIGINT NOT NULL);" & vbCrLf &_
					" IF OBJECT_ID('tempdb..#tempGroups') IS NOT NULL DROP TABLE #tempGroups" & vbCrLf &_
					" CREATE TABLE #tempGroups  (group_id BIGINT NOT NULL);" & vbCrLf &_
					"DECLARE @now DATETIME " & vbCrLf &_
					"SET @now = GETDATE() "
					
				usersSQL=""
				groupsSQL=""
				computersSQL=""
				
				Set RSGroupsIds = ConnAPI.Execute("SELECT id FROM groups WHERE domain_id="&salertDomainId&" AND (name = '"&salertGroupsString&"')")
				groupfl=0
				Do while not RSGroupsIds.EOF
					group_id = RSGroupsIds("id")
					if(group_id <> "") then
						if(groupfl=0) then
							groupsIds = group_id
						else
							groupsIds = groupsIds & "," & group_id
						end if
						groupfl=1
					end if
					RSGroupsIds.MoveNext
				loop
				Set RSUsersIds = ConnAPI.Execute("SELECT id FROM users WHERE domain_id="&salertDomainId&" AND (name = '"&salertUsersString&"')")
				userfl=0
				Do while not RSUsersIds.EOF
					user_id = RSUsersIds("id")
					if(user_id <> "") then
						if(userfl=0) then
							usersIds = user_id
						else
							usersIds = usersIds & "," & user_id
						end if
						userfl=1
					end if
					RSUsersIds.MoveNext
				loop
				Set RSComputersIds = ConnAPI.Execute("SELECT id FROM computers WHERE domain_id="&salertDomainId&" AND (name = '"&salertComputersString&"')")
				computerfl=0
				Do while not RSComputersIds.EOF
					computer_id = RSComputersIds("id")
					if(computer_id <> "") then
						if(computerfl=0) then
							computersIds = computer_id
						else
							computersIds = computersIds & "," & computer_id
						end if
						computerfl=1
					end if
					RSComputersIds.MoveNext
				loop
				'OUs in tree
				ous_list = ""
				Set ousRS = ConnAPI.Execute ("SELECT Id From OU WHERE OU_PATH = '"& salertOUsString &"'")
				ousfl=0
				Do while not ousRS.EOF
					ou_id = ousRS("Id")
					if(ou_id <> "") then
						if(ousfl=0) then
							ous_list = ou_id
						else
							ous_list = ous_list & "," & ou_id
						end if
						ousfl=1
					end if
					ousRS.MoveNext
				loop
				if(ous_list<>"") then
				'to selected OUs and all subOUs
					OUsSQL= OUsSQL & " INSERT INTO #tempOUs (Id_child) (" & vbCrLf &_ 
						"	SELECT id" & vbCrLf &_ 
						"	FROM OU" & vbCrLf &_ 
						"	WHERE id IN ("&ous_list&")" & vbCrLf &_ 
						") "
				end if
				' get OUs from selected groups
				if groupsIds <> "" then
				OUsSQL = OUsSQL & vbCrLf &_
						"INSERT INTO #tempOUs (Id_child) (" & vbCrLf &_
						"	SELECT o.ou_id FROM ou_groups o" & vbCrLf &_
						"	LEFT JOIN #tempOUs as t" & vbCrLf &_
						"	ON o.ou_id = t.Id_child" & vbCrLf &_
						"	WHERE t.Id_child IS NULL AND o.group_id in ("&groupsIds&")" & vbCrLf &_
						") "
				end if 
				' if there is closed OUs or OUs from groups
				if(OUsSQL<>"") then
					' add subOUs
					OUsSQL = OUsSQL & vbCrLf &_
							";WITH OUsList (Id_child)" & vbCrLf &_ 
							"AS (" & vbCrLf &_
							"	SELECT h.Id_child FROM OU_hierarchy h" & vbCrLf &_
							"	INNER JOIN #tempOUs t ON t.Id_child = h.Id_parent /*ids form 1*/" & vbCrLf &_
							"	UNION ALL" & vbCrLf &_
							"	SELECT c.Id_child FROM OU_hierarchy as c" & vbCrLf &_
							"	INNER JOIN OUsList as p" & vbCrLf &_
							"	ON c.Id_parent = p.Id_child" & vbCrLf &_
							")" & vbCrLf &_
							"INSERT INTO #tempOUs (Id_child)" & vbCrLf &_
							"(" & vbCrLf &_
							"	SELECT l.Id_child FROM OUsList l" & vbCrLf &_
							"	LEFT JOIN #tempOUs as t" & vbCrLf &_
							"	ON l.Id_child = t.Id_child" & vbCrLf &_
							"	WHERE t.Id_child IS NULL" & vbCrLf &_
							") "
				end if
				

				groupsSQL = "INSERT INTO #tempGroups (group_id) (" & vbCrLf &_
							"	SELECT Id_Group as group_id" & vbCrLf &_
							"	FROM OU_Group g" & vbCrLf &_
							"	INNER JOIN #tempOUs t ON t.Id_child=g.Id_OU" & vbCrLf &_
							") "
				if groupsIds <> "" then
					groupsSQL = groupsSQL & vbCrLf &_
								"INSERT INTO #tempGroups (group_id) (" & vbCrLf &_
								"	SELECT g.id FROM groups g" & vbCrLf &_
								"	LEFT JOIN #tempGroups as t" & vbCrLf &_
								"	ON g.id = t.group_id" & vbCrLf &_
								"	WHERE t.group_id IS NULL AND id in ("&groupsIds&")" & vbCrLf &_
								") "
				end if

				groupsSQL = groupsSQL & vbCrLf &_
						" IF OBJECT_ID('tempdb..#tmp1') IS NOT NULL DROP TABLE #tmp1" & vbCrLf &_
						" CREATE TABLE #tmp1  (group_id BIGINT NOT NULL);" & vbCrLf &_
						" IF OBJECT_ID('tempdb..#tmp2') IS NOT NULL DROP TABLE #tmp2" & vbCrLf &_
						" CREATE TABLE #tmp2  (group_id BIGINT NOT NULL);" & vbCrLf &_
						" INSERT INTO #tmp1 (group_id)(SELECT group_id FROM #tempGroups)" & vbCrLf &_
						" WHILE 1=1" & vbCrLf &_
						" BEGIN" & vbCrLf &_
						" 	INSERT INTO #tmp2 (group_id)" & vbCrLf &_
						" 	(" & vbCrLf &_
						" 			SELECT DISTINCT g.group_id FROM groups_groups as g" & vbCrLf &_
						" 			INNER JOIN #tmp1 as t1" & vbCrLf &_
						" 			ON g.container_group_id = t1.group_id" & vbCrLf &_
						" 			LEFT JOIN #tempGroups as t2" & vbCrLf &_
						" 			ON g.group_id = t2.group_id" & vbCrLf &_
						" 			WHERE t2.group_id IS NULL" & vbCrLf &_
						" 	)" & vbCrLf &_
						" 	IF @@ROWCOUNT = 0 BREAK;" & vbCrLf &_
						" 	DELETE #tmp1" & vbCrLf &_
						" 	INSERT INTO #tmp1 (group_id)(SELECT group_id FROM #tmp2)" & vbCrLf &_
						" 	INSERT INTO #tempGroups (group_id)(SELECT group_id FROM #tmp2)" & vbCrLf &_
						" 	DELETE #tmp2" & vbCrLf &_
						" END; " & vbCrLf &_
						" DROP TABLE #tmp1" & vbCrLf &_
						" DROP TABLE #tmp2 "

				usersSQL = 	"INSERT INTO #tempUsers (user_id)(" & vbCrLf &_
							"	SELECT DISTINCT user_id" & vbCrLf &_
							"	FROM users_groups g" & vbCrLf &_
							"	INNER JOIN #tempGroups t" & vbCrLf &_
							"	ON t.group_id = g.group_id" & vbCrLf &_
							") "
				usersSQL = usersSQL & vbCrLf &_
							"INSERT INTO #tempUsers (user_id)(" & vbCrLf &_
							"	SELECT u.Id_user" & vbCrLf &_
							"	FROM OU_User u" & vbCrLf &_
							"	INNER JOIN #tempOUs o" & vbCrLf &_
							"	ON o.Id_child = u.Id_OU" & vbCrLf &_
							"	LEFT JOIN #tempUsers t" & vbCrLf &_
							"	ON t.user_id = u.Id_user" & vbCrLf &_
							"	WHERE t.user_id IS NULL" & vbCrLf &_
							") "

				if usersIds <> "" then
					usersSQL = usersSQL & vbCrLf &_
								"INSERT INTO #tempUsers (user_id)(" & vbCrLf &_
								"	SELECT u.id" & vbCrLf &_ 
								"	FROM users u" & vbCrLf &_
								"	LEFT JOIN #tempUsers t" & vbCrLf &_
								"	ON t.user_id = u.id" & vbCrLf &_
								"	WHERE t.user_id IS NULL" & vbCrLf &_ 
								"	AND u.id in ("&usersIds&")" & vbCrLf &_
								") "
				end if
				
				computersSQL = 	"INSERT INTO #tempComputers (comp_id)(" & vbCrLf &_
								"	SELECT DISTINCT g.comp_id" & vbCrLf &_
								"	FROM computers_groups g" & vbCrLf &_
								"	INNER JOIN #tempGroups t" & vbCrLf &_
								"	ON t.group_id = g.group_id" & vbCrLf &_
								") "
				computersSQL = computersSQL & vbCrLf &_
							"INSERT INTO #tempComputers (comp_id)(" & vbCrLf &_
							"	SELECT c.Id_comp" & vbCrLf &_
							"	FROM OU_comp c" & vbCrLf &_
							"	INNER JOIN #tempOUs o" & vbCrLf &_
							"	ON o.Id_child = c.Id_OU" & vbCrLf &_
							"	LEFT JOIN #tempComputers t" & vbCrLf &_
							"	ON t.comp_id = c.Id_comp" & vbCrLf &_
							"	WHERE t.comp_id IS NULL" & vbCrLf &_
							") "
				
				if computersIds <> "" then
					computersSQL = computersSQL & vbCrLf &_
								"INSERT INTO #tempComputers (comp_id)(" & vbCrLf &_
								"	SELECT c.id" & vbCrLf &_
								"	FROM computers c" & vbCrLf &_
								"	LEFT JOIN #tempComputers t" & vbCrLf &_
								"	ON t.comp_id = c.id" & vbCrLf &_
								"	WHERE t.comp_id IS NULL" & vbCrLf &_ 
								"	AND c.id in ("&computersIds&")" & vbCrLf &_
								") "
				end if

			sendSQL = sendSQL & OUsSQL & groupsSQL & usersSQL & computersSQL

			sendSQL = sendSQL & vbCrLf &_
					"INSERT INTO alerts_sent_deskbar (alert_id, desk_id)" & vbCrLf &_
					"(" & vbCrLf &_
					"	SELECT "&alert_id&", d.id FROM users_deskbar d INNER JOIN #tempUsers t" & vbCrLf &_
					"	ON d.user_id = t.user_id" & vbCrLf &_
					") "
		
			sendSQL = sendSQL & vbCrLf &_
					"INSERT INTO alerts_sent (alert_id, user_id)" & vbCrLf &_
					"(" & vbCrLf &_
					"	SELECT "&alert_id&", user_id FROM #tempUsers" & vbCrLf &_
					") "

			sendSQL = sendSQL & vbCrLf &_
					"INSERT INTO alerts_sent_stat (alert_id, user_id, date)" & vbCrLf &_
					"(" & vbCrLf &_
					"	SELECT "&alert_id&", user_id, @now  FROM #tempUsers" & vbCrLf &_
					") "

			sendSQL = sendSQL & vbCrLf &_
					"INSERT INTO alerts_sent_group (alert_id, group_id)" & vbCrLf &_
					"(" & vbCrLf &_
					"	SELECT "&alert_id&", group_id FROM #tempGroups" & vbCrLf &_
					") " & vbCrLf &_
					"INSERT INTO alerts_group_stat (alert_id, group_id, date)" & vbCrLf &_
					"(" & vbCrLf &_
					"	SELECT "&alert_id&", group_id, @now  FROM #tempGroups" & vbCrLf &_
					") "

			sendSQL = sendSQL & vbCrLf &_
					"INSERT INTO alerts_sent_comp (alert_id, comp_id)" & vbCrLf &_
					"(" & vbCrLf &_
					"	SELECT "&alert_id&", comp_id FROM #tempComputers" & vbCrLf &_
					") " & vbCrLf &_
					"INSERT INTO alerts_comp_stat (alert_id, comp_id, date)" & vbCrLf &_
					"(" & vbCrLf &_
					"	SELECT "&alert_id&", comp_id, @now  FROM #tempComputers" & vbCrLf &_
					") "

			sendSQL = sendSQL & vbCrLf &_
					"INSERT INTO alerts_sent_ou (alert_id, ou_id)" & vbCrLf &_
					"(" & vbCrLf &_
					"	SELECT "&alert_id&", Id_child FROM #tempOUs" & vbCrLf &_
					") " & vbCrLf &_
					"INSERT INTO alerts_ou_stat (alert_id, ou_id, date)" & vbCrLf &_
					"(" & vbCrLf &_
					"	SELECT "&alert_id&", Id_child, @now  FROM #tempOUs" & vbCrLf &_
					") "

			'smsEmailSQL=""

			'if ((smsChecked="1" OR emailChecked="1" OR text_to_call="1")  AND schedule<>"1") then
			'	smsEmailSQL = "SELECT u.id, u.name, u.mobile_phone, u.email FROM #tempUsers AS t INNER JOIN users u ON t.user_id=u.id "
			'end if

			'sendSQL = sendSQL & smsEmailSQL

			'sendSQL = sendSQL & vbCrLf &_
			'			"SELECT i.device_id FROM #tempUsers AS t INNER JOIN user_instances i ON t.user_id=i.user_id WHERE i.device_type = 2" & vbCrLf &_
			'			"SELECT i.device_id FROM #tempUsers AS t INNER JOIN user_instances i ON t.user_id=i.user_id WHERE i.device_type = 3" & vbCrLf &_
			'			" DROP TABLE #tempOUs" & vbCrLf &_
			'			" DROP TABLE #tempUsers" & vbCrLf &_
			'			" DROP TABLE #tempComputers" & vbCrLf &_ 
			'			" DROP TABLE #tempGroups"
			
			set RSSend = Conn.Execute(sendSQL)
			
			end if
        	          '  Set cacheMgr = CreateObject("DeskAlerts.CacheManager")
             ' cacheMgr.Add(Clng(salertId), now())
              MemCacheUpdate "Add", salertId

		else
			apiDbError
		end if
		
		if(salertEmail=1 OR salertSMS=1 OR salertTextCall=1 ) then
		'users
		'groups
		'ous
				Set rsDomain = ConnAPI.Execute ("select id from domains where name = N'"&saDomainName&"'")
				if(Not rsDomain.EOF) then
					salertDomainId = rsDomain("id")
					salertUsersString = replace(saRecipients(1), ", " ,"' OR users.name = N'")
					salertGroupsString = replace(saRecipients(2), ", ","' OR groups.name = N'")

					salertOUsString = replace(saRecipients(4), ", " ,"/' OR OU_PATH = N'" & saDomainName & "/")
					salertOUsString = salertOUsString & "/"
		
					recipientsSQL = "SELECT DISTINCT user_name, email, mobile_phone FROM (SELECT users.name as user_name, email, mobile_phone FROM users LEFT JOIN users_groups ON users_groups.user_id=users.id LEFT JOIN groups ON users_groups.group_id=groups.id WHERE ((users.name = '" & salertUsersString & "') OR (groups.name = '" & salertGroupsString & "')) AND users.domain_id=" & salertDomainId
							
					'ous send
					ous_list = ""
					ousSQL = ""
					Set ousRS = ConnAPI.Execute ("SELECT Id From OU WHERE OU_PATH = N'"& replace (saDomainName&"/"&salertOUsString, "//", "/") &"'")
					ousfl=0
					Do while not ousRS.EOF
						ou_id = ousRS("Id")
						if(ou_id <> "") then
							if(ousfl=0) then
								ous_list = " Id_OU=" & ou_id
							else
								ous_list = ous_list & " OR Id_OU=" & ou_id
							end if
							ousfl=1
						end if
						ousRS.MoveNext
					loop
					if(ous_list<>"") then
						recipientsSQL = recipientsSQL & " UNION ALL SELECT user.name as user_name, email, mobile_phone FROM users INNER JOIN OU_User ON OU_User.Id_user=user.id WHERE " & ous_list
					end if
					recipientsSQL = recipientsSQL & " ) as u"
				else
					apiDbError
				end if

			Set rsRecipients = ConnAPI.Execute(recipientsSQL)
			do while not rsRecipients.EOF
				if(salertSMS=1) then
					if(Not IsNull(rsRecipients("mobile_phone"))) then
						if(len(rsRecipients("mobile_phone"))>0) then
							sMobileNo = rsRecipients("mobile_phone")
							sText = Replace(DeleteTags(salertText), "&amp;", "&")
							sText = replace(sText, "%username%", rsRecipients("user_name"))
							sendAlertSMS sText, sMobileNo
						end if
					end if
				end if
				if(salertEmail=1) then
					if(Not IsNull(rsRecipients("email"))) then
						if(len(rsRecipients("email"))>0) then
							user_email = rsRecipients("email")
							user_name = rsRecipients("user_name")
							text = salertText
							text = replace(text, "%username%", rsRecipients("user_name"))
							subject = salertTitle
							sendAlertEmail subject, user_email, user_name, makeFullImagePaths(text), senderName
						end if
					end if
				end if
				if(salertTextCall=1) then
				    if(Not IsNull(rsRecipients("mobile_phone"))) then
						if(len(rsRecipients("mobile_phone"))>0) then
							sMobileNo = rsRecipients("mobile_phone")
							sText = Replace(DeleteTags(salertText), "&amp;", "&")
							sText = replace(sText, "%username%", rsRecipients("user_name"))
							sendAlertTextCall sText, sMobileNo
						end if
					end if
				end if
				rsRecipients.MoveNext
			loop
		end if
	elseif(salertType2="I") then
	        salertId = 0
	        Set rs1 = ConnAPI.Execute("select @@IDENTITY newID from alerts")
            salertId=rs1("newID")
            rs1.Close
            if(IsNull(salertId)) then
                Set rs1 = ConnAPI.Execute("select MAX(id) as newID from alerts")
                salertId=rs1("newID")
                rs1.Close
            end if
		                
		    objects=saRecipients(5)
		    names_groups = Split(objects, ", ")
		    size = UBound(names_groups)+1
		    Dim groups()
		    ReDim groups(size)
		    
		    c = 0
		    for each name in names_groups
		        if(name<>"") then
		            c = c + 1
				    Set RSid = Conn.Execute("SELECT id FROM ip_groups WHERE name='" & name & "'")
				    if(Not RSid.EOF) then
					    groups(c) = RSid("id")
					end if
			    end if
		    next
		    
		    sendIpRange groups, salertId
		    
        	      '      Set cacheMgr = CreateObject("DeskAlerts.CacheManager")
              'cacheMgr.Add(Clng(salertId), now())
             MemCacheUpdate "Add", salertId
	end if
	response.write apiResult (api_method, "SUCCESS", "", salertId)	
end function
'----------------
'API : group members method
function apiParseMembers(psaXML)
	Set objLst = psaXML.getElementsByTagName("members")
	intNoOfHeadlines = objLst.length -1
	For iter = 0 To (intNoOfHeadlines)
		Set     objHdl = objLst.item(iter)
		for each child in objHdl.childNodes
		Select case lcase(child.nodeName)
			case "domain"
				domainName = child.Attributes(0).Value 'only one attribute, change this if will be more
				for each childDomain in child.childNodes
					Select case lcase(childDomain.nodeName)
						case "group"
							groupName = childDomain.Attributes(0).Value 'only one attribute, change this if will be more
							for each childGroup in childDomain.childNodes
								Select case lcase(childGroup.nodeName)
									case "add"
									newUsersNameStr = ""
									for each childAddUsers in childGroup.childNodes
										newUserName = ""
										for each childAddUsersAttr in childAddUsers.Attributes
										Select case lcase(childAddUsersAttr.Name)
											case "username"
												newUserName = replace(childAddUsersAttr.Value,"'","''")
												if(newUsersNameStr = "") then
													newUsersNameStr = " name = N'" & newUserName & "' "
												else
													newUsersNameStr = newUsersNameStr & " OR name = N'" & newUserName & "' "
												end if
										End Select
										next
									next
									apiInsertGroupMembers newUsersNameStr, domainName, groupName

									case "remove"
										oldUsersNameStr = ""
										for each childRemoveUsers in childGroup.childNodes
											oldUserName = ""
											for each childRemoveUsersAttr in childRemoveUsers.Attributes
												Select case lcase(childRemoveUsersAttr.Name)
													case "username"
														oldUserName = replace(childRemoveUsersAttr.Value,"'","''")
												End Select
											next
											if(oldUsersNameStr = "") then
												oldUsersNameStr = " name = N'" & oldUserName & "' "
											else
												oldUsersNameStr = oldUsersNameStr & " OR name = N'" & oldUserName & "'"
											end if
										next
										apiRemoveGroupMembers oldUsersNameStr, domainName, groupName
								End Select
							next
					End Select
				next
		end select
		next
	next
	response.write apiResult (api_method, "SUCCESS", "", salertId)	
end function

function apiRemoveGroupMembers(aUsersNameStr, aDomainName, aGroupName)
	SQL = "SELECT id FROM domains WHERE name = N'"&aDomainName&"'"
	Set domainRS = ConnAPI.Execute(SQL)
	if(not domainRS.EOF) then
		aDomainId = domainRS("id")
		domainRS.Close
		SQL = "SELECT id FROM groups WHERE name = N'"&aGroupName&"' AND domain_id = "&aDomainId
		Set groupRS = ConnAPI.Execute(SQL)
		if(not groupRS.EOF) then
			aGroupId = groupRS("id")
			groupRS.Close
			
			SQL1 = "SELECT users.id as id, users_groups.group_id as group_id FROM users INNER JOIN users_groups ON users.id = users_groups.user_id WHERE users_groups.group_id="&aGroupId&" AND ("&aUsersNameStr&")"
			aCurrentList = "0"
			Set usersRS = ConnAPI.Execute(SQL1)
			Do while not usersRS.EOF
				aCurrentList = aCurrentList & ", " & usersRS("id")
				usersRS.MoveNext
			loop
			
			SQL2 = "DELETE FROM users_groups WHERE group_id = "&aGroupId&" AND user_id IN("&aCurrentList&")"
			ConnAPI.Execute (SQL2)
		else
			apiDbError
		end if
	else
		apiDbError
	end if
end function

function apiInsertGroupMembers(aUsersNameStr, aDomainName, aGroupName)
	SQL = "SELECT id FROM domains WHERE name = N'"&aDomainName&"'"
	Set domainRS = ConnAPI.Execute(SQL)
	if(not domainRS.EOF) then
		aDomainId = domainRS("id")
		domainRS.Close
		SQL = "SELECT id FROM groups WHERE name = N'"&aGroupName&"' AND domain_id = "&aDomainId
		Set groupRS = ConnAPI.Execute(SQL)
		if(not groupRS.EOF) then 
			aGroupId = groupRS("id")
			groupRS.Close

			SQL1 = "SELECT users.id as id, users_groups.group_id as group_id FROM users INNER JOIN users_groups ON users.id = users_groups.user_id WHERE users_groups.group_id="&aGroupId&" AND ("&aUsersNameStr&")"
			aCurrentList = "0"
			Set usersRS = ConnAPI.Execute(SQL1)
			Do while not usersRS.EOF
				aCurrentList = aCurrentList & ", " & usersRS("id")
				usersRS.MoveNext
			loop			
			SQL2 = "INSERT INTO users_groups(user_id, group_id) SELECT id, "&aGroupId&" as group_id FROM users WHERE ("&aUsersNameStr&") AND id NOT IN("&aCurrentList&")"
			ConnAPI.Execute (SQL2)
		else
			apiDbError
		end if
	else
		apiDbError
	end if
end function
'--------------------
'API : groups method
function apiParseGroups(psaXML)
	Set objLst = psaXML.getElementsByTagName("groups")
	intNoOfHeadlines = objLst.length -1
	For iter = 0 To (intNoOfHeadlines)
		Set     objHdl = objLst.item(iter)
		for each child in objHdl.childNodes
		Select case lcase(child.nodeName)
			case "domain"
				domainName = child.Attributes(0).Value 'only one attribute, change this if will be more
				for each childDomain in child.childNodes
					Select case lcase(childDomain.nodeName)
						case "add"
							for each childAddGroups in childDomain.childNodes
								newGroupName = ""
								for each childAddGroupsAttr in childAddGroups.Attributes
									Select case lcase(childAddGroupsAttr.Name)
										case "name"
											newGroupName = childAddGroupsAttr.Value
									End Select
								next
								'insert new group
								apiInsertGroup replace(newGroupName,"'","''"), domainName
							next
						case "modify"
							for each childAddGroups in childDomain.childNodes
								oldGroupName = ""
								newGroupName = ""
								for each childAddGroupsAttr in childAddGroups.Attributes
									Select case lcase(childAddGroupsAttr.Name)
										case "oldname"
											oldGroupName = childAddGroupsAttr.Value
										case "name"
											newGroupName = childAddGroupsAttr.Value
									End Select
								next
								'insert new group
								apiModifyGroup replace(oldGroupName,"'","''"), replace(newGroupName,"'","''"), domainName
							next
					
						case "remove"
							for each childRemoveGroups in childDomain.childNodes
								oldGroupName = ""
								for each childRemoveGroupsAttr in childRemoveGroups.Attributes
									Select case lcase(childRemoveGroupsAttr.Name)
										case "name"
											oldGroupName = childRemoveGroupsAttr.Value
									End Select
								next
								apiRemoveGroup replace(oldGroupName,"'","''"), domainName
							Next
					End Select
				next
		End Select
		next
	next
	response.write apiResult (api_method, "SUCCESS", "", salertId)	
end function

function apiRemoveGroup(argGroupName, argDomainName)
	SQL = "SELECT id FROM domains WHERE name = N'"&argDomainName&"'"
	Set domainRS = ConnAPI.Execute(SQL)
	if(not domainRS.EOF) then
		argDomainId = domainRS("id")
		domainRS.Close
		SQL = "SELECT id FROM groups WHERE name = N'"&argGroupName&"' AND domain_id = "&argDomainId
		Set groupRS = ConnAPI.Execute(SQL)
		if(not groupRS.EOF) then
			argGroupId = groupRS("id")
			groupRS.Close
			SQL = "DELETE FROM groups WHERE id=" & argGroupId
			ConnAPI.Execute(SQL)
			SQL2 = "DELETE FROM users_groups WHERE group_id=" & argGroupId
			ConnAPI.Execute(SQL2)
			SQL2 = "DELETE FROM alerts_group_stat WHERE group_id=" & argGroupId
			ConnAPI.Execute(SQL2)
			SQL2 = "DELETE FROM alerts_sent_group WHERE group_id=" & argGroupId
			ConnAPI.Execute(SQL2)
		else
			apiDbError
		end if
	else
		apiDbError
	end if
end function

function apiModifyGroup(amgOldGroupName, amgGroupName, amgDomainName)
	SQL = "SELECT id FROM domains WHERE name = N'"&amgDomainName&"'"
	Set domainRS = ConnAPI.Execute(SQL)
	if(not domainRS.EOF) then
		amgDomainId = domainRS("id")
		domainRS.Close
		SQL = "SELECT id FROM groups WHERE name='"&amgOldGroupName&"' AND domain_id="&amgDomainId
		Set groupRS = ConnAPI.Execute(SQL)
		if(Not groupRS.EOF) then 'group in database
			SQL = "SELECT id FROM groups WHERE name='"&amgGroupName&"' AND domain_id="&amgDomainId
			Set newgroupRS = ConnAPI.Execute(SQL)
			if(newgroupRS.EOF) then
				SQL = "UPDATE groups SET name = N'"&amgGroupName&"' WHERE id=" & groupRS("id")
				ConnAPI.Execute (SQL)
			else
				apiDbError
			end if
		else
			apiDbError
		end if
	else
		apiDbError
	end if
end function

function apiInsertGroup(aigGroupName, aigDomainName)
	SQL = "SELECT id FROM domains WHERE name = N'"&aigDomainName&"'"
	Set domainRS = ConnAPI.Execute(SQL)
	if(not domainRS.EOF) then
		aigDomainId = domainRS("id")
		domainRS.Close
		SQL = "SELECT id FROM groups WHERE name = N'"&aigGroupName&"' AND domain_id="&aigDomainId
		Set groupRS = ConnAPI.Execute(SQL)
		if(groupRS.EOF) then 'no such group in database
			SQL = "INSERT INTO groups (name, domain_id, was_checked, custom_group) VALUES (N'"&aigGroupName&"', "&aigDomainId&", 1, 1)"
			ConnAPI.Execute (SQL)
		else
			apiDbError
		end if
	else
		apiDbError
	end if
end function
'----------------------
'API : users method
function apiParseUsers (psaXML)
	Set objLst = psaXML.getElementsByTagName("users")
	intNoOfHeadlines = objLst.length -1
	For iter = 0 To (intNoOfHeadlines)
		Set     objHdl = objLst.item(iter)
		for each child in objHdl.childNodes
		Select case lcase(child.nodeName)
			case "domain"
				domainName = child.Attributes(0).Value 'only one attribute, change this if will be more
				for each childDomain in child.childNodes
					Select case lcase(childDomain.nodeName)
						case "add"
							for each childAddUsers in childDomain.childNodes
								newUsername = ""
								newPassword = ""
								newEmail = ""
								newPhone = ""
								newDisplayname = ""
								for each childAddUsersAttr in childAddUsers.Attributes
									Select case lcase(childAddUsersAttr.Name)
										case "username"
											newUsername = childAddUsersAttr.Value
										case "password"
											newPassword = childAddUsersAttr.Value
										case "email"
											newEmail = childAddUsersAttr.Value
										case "phone"
											newPhone = childAddUsersAttr.Value
										case "displayname"
											newDisplayname = childAddUsersAttr.Value
									End Select
								next
								'insert new user
								apiInsertUser newUsername, newPassword, newEmail, newPhone, newDisplayname, domainName
							next
						case "modify"
							for each childAddUsers in childDomain.childNodes
								oldUsername = ""
								newUsername = ""
								newPassword = ""
								newEmail = ""
								newPhone = ""
								newDisplayname = ""
								for each childAddUsersAttr in childAddUsers.Attributes
									Select case lcase(childAddUsersAttr.Name)
										case "oldusername"
											oldUsername = childAddUsersAttr.Value
										case "username"
											newUsername = childAddUsersAttr.Value
										case "password"
											newPassword = childAddUsersAttr.Value
										case "email"
											newEmail = childAddUsersAttr.Value
										case "phone"
											newPhone = childAddUsersAttr.Value
										case "displayname"
											newDisplayname = childAddUsersAttr.Value
									End Select
								next
								'insert new user
								apiModifyUser oldUsername, newUserName, newPassword, newEmail, newPhone, newDisplayname, domainName
							next							
						case "remove"
							for each childRemoveUsers in childDomain.childNodes
								oldUsername = ""
								for each childRemoveUsersAttr in childRemoveUsers.Attributes
									Select case lcase(childRemoveUsersAttr.Name)
										case "username"
											oldUsername = childRemoveUsersAttr.Value
									End Select
								next
								apiRemoveUser oldUsername, domainName
							Next
					End Select
				next
		End Select
		next
	next
	response.write apiResult (api_method, "SUCCESS", "", salertId)	
end function

function apiRemoveUser(aruUserName, aruDomainName)
	SQL = "SELECT id FROM domains WHERE name = N'"&aruDomainName&"'"
	Set domainRS = ConnAPI.Execute(SQL)
	if(not domainRS.EOF) then
		aruDomainId = domainRS("id")
		domainRS.Close
		SQL = "SELECT id FROM users WHERE name = N'"&aruUserName&"' AND domain_id="&aruDomainId
		Set userRS = ConnAPI.Execute(SQL)
		if(not userRS.EOF) then
			aruUserId = userRS("id")
			userRS.Close
			SQL = "DELETE FROM users WHERE id=" & aruUserId
			ConnAPI.Execute(SQL)
	
			SQL = "DELETE FROM users_groups WHERE user_id=" & aruUserId
			ConnAPI.Execute(SQL)
	
			SQL = "DELETE FROM alerts_read WHERE user_id="  & aruUserId
			ConnAPI.Execute(SQL)

			SQL = "DELETE FROM alerts_received WHERE user_id="  & aruUserId
			ConnAPI.Execute(SQL)

			SQL = "DELETE FROM alerts_sent WHERE user_id="  & aruUserId
			ConnAPI.Execute(SQL)

			SQL = "DELETE FROM alerts_sent_stat WHERE user_id="  & aruUserId
			ConnAPI.Execute(SQL)

			SQL = "DELETE FROM surveys_answers_stat WHERE user_id="  & aruUserId
			ConnAPI.Execute(SQL)

			SQL = "DELETE FROM alerts_cache_users WHERE user_id="  & aruUserId
			ConnAPI.Execute(SQL)
		else
			apiDbError
		end if
	else
		apiDbError
	end if
end function

function apiInsertUser(aiuUserName, aiuPassword, aiuEmail, aiuPhone, aiuDisplayname, aiuDomainName)
	SQL = "SELECT id FROM domains WHERE name = N'"&aiuDomainName&"'"
	Set domainRS = ConnAPI.Execute(SQL)
	if(not domainRS.EOF) then
		aiuDomainId = domainRS("id")
		domainRS.Close
		SQL = "SELECT id FROM users WHERE name = N'"&aiuUserName&"' AND domain_id="&aiuDomainId
		Set userRS = ConnAPI.Execute(SQL)
		if(userRS.EOF) then 'no such user in database
			SQL = "INSERT INTO users (name, pass, email, mobile_phone, display_name,  domain_id, role, reg_date) VALUES (N'"&aiuUsername&"', '"&aiuPassword&"', N'"&aiuEmail&"', N'"&aiuPhone&"', N'"&aiuDisplayname&"', "&aiuDomainId&", 'U', GETDATE())"
			ConnAPI.Execute (SQL)
		else
			apiDbError
		end if
	else
		apiDbError
	end if
end function

function apiModifyUser(amuOldUserName, amuNewUserName, amuPassword, amuEmail, amuPhone, amuDisplayname, amuDomainName)
	SQL = "SELECT id FROM domains WHERE name = N'"&amuDomainName&"'"
	Set domainRS = ConnAPI.Execute(SQL)
	if(not domainRS.EOF) then
		amuDomainId = domainRS("id")
		domainRS.Close
		SQL = "SELECT id FROM users WHERE name = N'"&amuOldUserName&"' AND domain_id="&amuDomainId
		Set userRS = ConnAPI.Execute(SQL)
		if(Not userRS.EOF) then 'no such user in database
			SQL = "SELECT id FROM users WHERE name = N'"&amuNewUserName&"' AND domain_id="&amuDomainId
			Set newuserRS = ConnAPI.Execute(SQL)
			if(newuserRS.EOF) then
				SQL = "UPDATE users SET name=N'"&amuNewUserName&"', pass='"&amuPassword&"', email=N'"&amuEmail&"', mobile_phone=N'"&amuPhone&"', display_name=N'"&amuDisplayname&"' WHERE id=" & userRS("id")
				ConnAPI.Execute (SQL)
			else
				apiDbError
			end if
		else
			apiDbError
		end if
	else
		apiDbError
	end if
end function
'-----------------------


function apiValidateXML(vxXMLdata, vxXSDfilename)
	Set objXMLDOM = Server.CreateObject("MSXML2.DOMDocument.6.0")
	Set objXSDCache = Server.CreateObject("Msxml2.XMLSchemaCache.6.0")

	Dim XSDName : XSDName = Server.mappath(vxXSDfilename)
	objXSDCache.Add "", XSDName
	set objXMLDOM.schemas = objXSDCache

	objXMLDom.async = False
	objXMLDom.resolveExternals = true 
	objXMLDom.validateOnParse =  true
	objXMLDom.loadXML(vxXMLdata)

	if(objXMLDom.parseError.errorCode <> 0) then
		apiValidateXML = false
	else
		apiValidateXML =  true		
	end if
end function

function apiResult(resultMethod, resultType, resultDesc, resultId)
	apiResult = "<?xml version=""1.0"" encoding=""utf-8""?>" & _
		"<response>" & _
		"<method>" & resultMethod & "</method>" & _
		"<result>" & resultType & "</result>" & _
		"<id>" & resultId & "</id>" & _
		"<description>" & resultDesc & "</description>" & _
		"</response>"
end function

function apiValidateSignature (vsXMLdata, vsSignature)
	api_secret = ConfAPISecret
	if vsSignature = api_secret or StrComp(MD5(vsXMLdata & api_secret), vsSignature, 1) = 0 then
		apiValidateSignature = true
	else
		apiValidateSignature = false
	end if
end function

function apiParseXML(pxXMLdata, pxType)
 
	Set xmlResponse = Server.CreateObject("MSXML2.DOMDocument.6.0")
	xmlResponse.async = false
	xmlResponse.LoadXml(pxXMLdata)
	if(pxType<>"") then 'old versions
		if(pxType="send_alert") then
			apiParseSendAlert (xmlResponse)
		elseif (pxType="users") then
			apiParseUsers(xmlResponse)
		elseif (pxType="groups") then
			apiParseGroups(xmlResponse)
		elseif (pxType="members") then
			apiParseMembers(xmlResponse)		
		end if
	else
		Set objLst = xmlResponse.getElementsByTagName("darequest")
		intNoOfHeadlines = objLst.length -1
		For iter = 0 To (intNoOfHeadlines)
			Set     objHdl = objLst.item(iter)
			for each child in objHdl.childNodes
			Select case lcase(child.nodeName)
				case "alert"
					api_method = "alert"
					apiParseSendAlert(xmlResponse)
				case "users"
					api_method = "users"
					apiParseUsers(xmlResponse)
				case "groups"
					api_method = "groups"
					apiParseGroups(xmlResponse)
				case "members"			
					api_method = "members"
					apiParseMembers(xmlResponse)
				case "getalert"
					api_method = "getalert"
					apiParseGetAlert(xmlResponse)
				case "deletealert"
					api_method = "deletealert"
					apiParseDeleteAlert(xmlResponse)
			End Select
			next
		next
	end if
end function
%>