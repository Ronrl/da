﻿using System;
using DeskAlertsDotNet.Utils;

namespace DeskAlertsDotNet.Pages.Errors
{
    
    public class DeskAlertsBaseErrorPage : System.Web.UI.Page
    {
        protected string crashReportFileName;
        protected string crashReportLink;
        protected string crashReportText;

        protected override void OnLoad(EventArgs e)
        {
            try
            {
                base.OnLoad(e);
                if (!string.IsNullOrEmpty(Request["crashReport"]))
                {
                    string dateTime = Request["crashReport"];
                    crashReportFileName = CrashReportManager.BuildCrashReportPath(dateTime);
                    crashReportText = CrashReportManager.ReadCrashReport(dateTime).Replace(Environment.NewLine, "<br/>");
                    crashReportLink = CrashReportManager.BuildCrashReportLink(dateTime);
                }
            }
            catch (Exception ex)
            {
                DeskAlertsBasePage.Logger.Debug(ex);
            }
        }
    }
}