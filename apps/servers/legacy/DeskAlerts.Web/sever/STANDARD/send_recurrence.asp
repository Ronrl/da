<% db_backup_stop_working = 1 %>
<!-- #include file="admin/config.inc" -->
<!-- #include file="admin/db_conn.asp" -->
<!-- #include file="admin/md5.asp" -->
<!-- #include file="admin/functions_inc.asp" -->
<%
Server.ScriptTimeout = 86400
Conn.CommandTimeout = 60

if smsPhoneFormat = "" then smsPhoneFormat = "1"
if smsPhoneFormat <> "%smsPhoneFormat%" and smsPhoneFormat <> "0" then
%>
	<script language="JScript" runat="server" src="admin/jscripts/phone_format.js"></script>
	<script language="JScript" runat="server" src="admin/jscripts/da_phone_format.js"></script>
<%
end if

Set RSAlerts = Conn.Execute("FindRecurAlert")

Do While Not RSAlerts.EOF
	sText = RSAlerts("alert_text")
	subject = RSAlerts("title")
	templateTop = RSAlerts("top")
	templateBottom = RSAlerts("bottom")

	begin_pos = InStr(sText,top_template_start_separator)
	if (begin_pos > 0) then
		end_pos = InStr(sText,top_template_end_separator)
		templateTop = Mid(sText, begin_pos+Len(top_template_start_separator), end_pos-begin_pos-Len(top_template_start_separator))
		sText = Mid(sText, end_pos+Len(top_template_end_separator))
	end if

	begin_pos = InStr(sText,bottom_template_start_separator)
	if (begin_pos > 0) then
		end_pos = InStr(sText,bottom_template_end_separator)
		templateBottom = Mid(sText, begin_pos+Len(bottom_template_start_separator), end_pos-begin_pos-Len(bottom_template_start_separator))
		sText = Left(sText, begin_pos-1)
	end if

	alerts_coll = GetAlertLangsCollection(sText)
	For Each alert in alerts_coll
		If(alert(2))Then
			sText=alert(3)
			subject=alert(1)
			Exit For
		End If
	Next

	sText = Replace(sText, "%username%", RSAlerts("name"))

	If RSAlerts("is_email")="1" Then
		If VarType(RSAlerts("email")) > 1 Then
			If Len(RSAlerts("email")) > 0 Then
				user_email = RSAlerts("email")
				user_name = RSAlerts("name")
				email_sender = ConfDefaultEmail
				If VarType(RSAlerts("email_sender")) > 1 Then
					If Len(RSAlerts("email_sender")) > 0 Then
						email_sender = RSAlerts("email_sender")
					End If
				End If
				sendAlertEmail subject, user_email, user_name, makeEmailHtml(replacePath(templateTop&sText&templateBottom),alerts_folder), email_sender
			End If
		End If
	End If

	If RSAlerts("is_sms")="1" Then
		If VarType(RSAlerts("mobile_phone")) > 1 Then
			If Len(RSAlerts("mobile_phone")) > 0 Then
				sMobileNo = RSAlerts("mobile_phone")
				sText = Replace(DeleteTags(sText), "&amp;", "&")
				sendAlertSMS sText, sMobileNo
			End If
		End If
	End If
	RSAlerts.MoveNext
Loop

RSAlerts.Close

Conn.Execute("UPDATE settings SET val = CONVERT(nvarchar, GETUTCDATE(), 9) WHERE name='ConfSendReccurenceTaskTimeStamp'")

Set campaigns = Conn.Execute("SELECT id FROM campaigns WHERE DATEPART(YEAR, start_date) = DATEPART(YEAR, GETDATE()) AND DATEPART(MONTH, start_date) = DATEPART(MONTH, GETDATE()) AND DATEPART(DAY, start_date) = DATEPART(DAY, GETDATE()) AND DATEPART(HOUR, start_date) = DATEPART(HOUR, GETDATE()) AND DATEPART(MINUTE, start_date) = DATEPART(MINUTE, GETDATE())")
   
   
do while not campaigns.EOF
  
  
  id = campaigns("id")
  Response.Write "Campaign " & id
  Conn.Execute("UPDATE campaigns SET started_date = start_date WHERE id = " & id)
  Conn.Execute("UPDATE campaigns SET start_date = NULL WHERE id = " & id)
  
  Set campaignAlerts = Conn.Execute("SELECT id FROM alerts WHERE campaign_id = " & id)
  
  do while not campaignAlerts.EOF
    Conn.Execute("UPDATE alerts SET schedule_type = '1' WHERE id = " & campaignAlerts("id"))
        	          '  Set cacheMgr = CreateObject("DeskAlerts.CacheManager")
             ' cacheMgr.Update(Clng(salertId), now())
         MemCacheUpdate "Update", campaignAlerts("id")
  campaignAlerts.movenext  
  loop
  
  campaigns.movenext  
loop

Response.Write "done"
%>
<!-- #include file="admin/db_conn_close.asp" -->