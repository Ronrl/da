<% db_backup_no_redirect = 1 %>
<!-- #include file="admin/config.inc" -->
<!-- #include file="admin/db_conn.asp" -->
<%

uname=Replace(Request("uname"),"'","''")
fulldomain=Replace(Request("fulldomain"),"'","''")
if ConfForceSelfRegWithAD=1 then
	SQL = "SELECT d.name FROM domains d, users u WHERE (u.name='"&uname&"' OR u.display_name='"&uname&"') AND u.domain_id<>1 AND u.domain_id = d.id"
	Set RSMimic = Conn.Execute(SQL)
	if not RSMimic.EOF then
		fulldomain=RSMimic("name")
	end if
end if

context=Replace(Request("edir"),"'","''")
standby=Replace(Request("standby"),"'","''")
nextrequest=Request("nextrequest")

if nextrequest <> "" then
	nextrequest = Clng(nextrequest)
else
	nextrequest = 60
end if

SQL = "UPDATE users SET "
if(standby="true") then
	SQL = SQL & " standby = 1, last_standby_request = GETDATE(), next_standby_request = '"&nextrequest&"' "
else
	SQL = SQL & " standby = 0, last_request = GETDATE(), last_standby_request = GETDATE(), next_standby_request = '"&nextrequest&"' "
end if
SQL = SQL & " FROM users u "
if(context<>"") then
	SQL = SQL & " INNER JOIN edir_context c ON u.context_id = c.id AND REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(LOWER(c.name), 'dc=', ''), 'c=', ''), 'ou=', ''), 'o=', ''), ',', '.') = N'"&context&"' "
elseif(fulldomain<>"") then
	SQL = SQL & " INNER JOIN domains d ON u.domain_id = d.id AND d.name = N'"&fulldomain&"' "
end if
SQL = SQL & " WHERE u.name = N'"& uname &"' AND u.role = 'U'"
Conn.Execute(SQL)

%>
<!-- #include file="admin/db_conn_close.asp" -->