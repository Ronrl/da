<% db_backup_stop_working = 1 %>
<!-- #include file="admin/config.inc" -->
<!-- #include file="admin/db_conn.asp" -->
<%
Server.ScriptTimeout = 120
Conn.CommandTimeout = 60

Set RS = Conn.Execute("SELECT move_type, move_number, move_date_type, clear_type, clear_number, clear_date_type FROM archive_settings WHERE id=1")
if (Not RS.EOF) then
	move_type = RS("move_type")
	move_number = RS("move_number")
	move_date_type = RS("move_date_type")
	if(move_date_type="Y") then
		move_date_type = "year"
	end if
	if(move_date_type="M") then
		move_date_type = "month"
	end if
	if(move_date_type="D") then
		move_date_type = "day"
	end if		
	clear_type = RS("clear_type")
	clear_number = RS("clear_number")
	clear_date_type = RS("clear_date_type")
	if(clear_date_type="Y") then
		clear_date_type = "year"
	end if
	if(clear_date_type="M") then
		clear_date_type = "month"
	end if
	if(clear_date_type="D") then
		clear_date_type = "day"
	end if		
	
	RS.Close
end if
if(move_type="Y") then
'move to archive
	Conn.Execute ("ArchiveData @date_type='"&move_date_type&"', @date_number="&move_number)
end if

if(clear_type="Y") then
'clear archive
	Conn.Execute ("ClearArchive @date_type='"&clear_date_type&"', @date_number="&clear_number)	
end if

if ( clear_type="Y" or move_type="Y") then
end if

Conn.Execute("UPDATE settings SET val = CONVERT(nvarchar, GETUTCDATE(), 9) WHERE name='ConfArchiveTaskTimeStamp'")
%>
Done
<!-- #include file="admin/db_conn_close.asp" -->