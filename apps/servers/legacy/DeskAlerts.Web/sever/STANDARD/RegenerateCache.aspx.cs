﻿using DeskAlerts.ApplicationCore.Utilities;
using DeskAlerts.Server.Utils.Managers;
using DeskAlertsDotNet.Application;
using DeskAlertsDotNet.Parameters;
using DeskAlertsDotNet.Utils.Consts;
using NLog;
using System;
using System.Collections;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;

namespace DeskAlertsDotNet.Pages.AlertsRecieving
{
    public partial class RegenerateCache : System.Web.UI.Page
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        static CacheManager cacheManager;
        private static object _lockObj = new object();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (AppConfiguration.IsNewContentDeliveringScheme)
            {
                return;
            }

            lock (_lockObj)
            {
                if (cacheManager == null)
                {
                    cacheManager = Global.CacheManager;
                }

                string date;
                if (!string.IsNullOrEmpty(Request["date"]))
                {
                    DateTime localtimeDateTime = DateTime.ParseExact(
                        Request["date"], 
                        DateTimeFormat.Client, 
                        CultureInfo.InvariantCulture);
                    date = localtimeDateTime.ToString(DateTimeFormat.MsSqlFormat, CultureInfo.InvariantCulture);
                }
                else
                {
                    date = DateTime.Now.ToString(DateTimeFormat.MsSqlFormat, CultureInfo.InvariantCulture);
                }
                _logger.Trace($"{nameof(date)} from request: {date}.");

                string action = Request["action"];
                _logger.Trace($"{nameof(action)} from request: {action}.");

                if (!string.IsNullOrEmpty(action))
                {
                    action = action.ToLower();

                    action = action.First().ToString().ToUpper() + action.Substring(1);

                    MethodInfo methodInfo = cacheManager.GetType().GetMethod(action);
                    _logger.Trace($"{nameof(methodInfo)}: {methodInfo?.Name}.");

                    ArrayList parameters = new ArrayList(2);

                    string[] ids = Request["alertId"].Split(',');
                    _logger.Trace($"AlertIds from request count: {ids.Length}");

                    foreach (string id in ids)
                    {
                        if (!string.IsNullOrEmpty(id))
                        {
                            int alertId = 0;
                            if (int.TryParse(id, out alertId))
                            {
                                parameters.Add(alertId);
                                parameters.Add(date);
                                methodInfo.Invoke(cacheManager, parameters.ToArray());
                                _logger.Trace($"Add parameters. alertId: {alertId}, date: {date}");
                                parameters.Clear();
                            }
                        }
                    }

                    if (string.IsNullOrEmpty(Request["nosend"]))
                    {
                        var logFilePath = Config.Data.GetString("alertsFolder") + "\\admin\\logs\\balancerLog.txt";
                        var balancerLogEnabled = Config.Data.GetBool("balancerLogEnabled");
                        string query = string.Format("/RegenerateCache.aspx?action={0}&id={1}&nosend=true", action, Request["alertId"]);
                        var servers = Config.Data.GetServerCompanions();

                        _logger.Trace($"{nameof(balancerLogEnabled)}: {balancerLogEnabled}.");
                        _logger.Trace($"Regenerate cache {nameof(query)}: {query}.");
                        _logger.Trace($"Response: {servers.Length.ToString()}.");

                        Response.WriteLine(servers.Length.ToString());
                        
                        // Logging start
                        if (balancerLogEnabled)
                        {
                            var configServers = Config.Data.GetString("serverSiblings");
                            _logger.Trace($"{nameof(configServers)}: {configServers}.");

                            if (File.Exists(logFilePath))
                            {
                                File.AppendAllText(logFilePath, "servers string = " + configServers + "\n");
                            }
                            else
                            {
                                File.WriteAllText(logFilePath, "servers string = " + configServers + "\n");
                            }
                        }
                        
                        // Logging end
                        foreach (string server in servers)
                        {
                      
                            string fullRequestUrl = server.TrimEnd('/') + query;
                            var serverRequestResult = string.Empty;

                            using (var client = new HttpClient())
                            {
                                File.WriteAllText(logFilePath, "server: " + fullRequestUrl + "\n");
                                serverRequestResult = client.GetStringAsync(fullRequestUrl).Result;

                                _logger.Trace($"{nameof(fullRequestUrl)}: {fullRequestUrl}.");
                                _logger.Trace($"{nameof(serverRequestResult)}: {serverRequestResult}.");
                            }
                            
                            // Logging start
                            if (balancerLogEnabled)
                            {
                                if (File.Exists(logFilePath))
                                {
                                    File.AppendAllText(logFilePath, "server:" + server + "\n");
                                    File.AppendAllText(logFilePath, "result:" + serverRequestResult + "\n");

                                    _logger.Trace($"{nameof(server)}: {server}.");
                                    _logger.Trace($"{nameof(serverRequestResult)}: {serverRequestResult}.");
                                }
                            }
                            // Logging end
                        }
                    }
                }
                else
                {
                    _logger.Debug($"No actions. Rebuild cache.");
                    // Response.Write(date); 
                    cacheManager.RebuildCache(date);
                }
            }

        }
    }
}