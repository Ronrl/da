<%@ Language=VBScript CodePage=65001%>
<% db_backup_stop_working = 1 %>
<!-- #include file="admin/config.inc" -->
<% ConnCount = 2 %>
<!-- #include file="admin/db_conn.asp" -->
<!-- #include file="admin/md5.asp" -->
<!-- #include file="api_functions.inc" -->

<%
'detect method by tag

xml_data = request ("xml_data")
api_signature = request ("api_signature")
api_method = "unknown"

'ConnAPI.BeginTrans

if(apiValidateSignature(xml_data, api_signature)=true) then
	xml_data = TrimAll(xml_data) 'for avoid errors in schema checking
	if(apiValidateXML(xml_data, "api/schema.xsd")=true) then 'validate xml by xsd
		'parse xml and send alert
		apiParseXML xml_data, ""
	else
		response.write apiResult (api_method, "ERROR", "Not valid xml_data", 0)
	end if
else
	response.write apiResult (api_method, "ERROR", "Not valid api_signature", 0)
end if

'ConnAPI.CommitTrans

%>
<!-- #include file="admin/db_conn_close.asp" -->