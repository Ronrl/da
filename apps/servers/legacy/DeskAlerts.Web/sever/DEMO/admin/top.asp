﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%

check_session()

%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
<head>
<title>DeskAlerts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
<link href="css/style9.css" rel="stylesheet" type="text/css">
<link href="css/jquery/jquery_timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css">
<script language="javascript" type="text/javascript" src="functions.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery_timepicker/jquery-ui-timepicker-addon.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/date.js"></script>
<script language="javascript" type="text/javascript">
    $(function () {
        $("#logout").button({
            icons: {
                primary: "ui-icon-key"
            }
        });
        $("#helpOpener").button({
            icons: {
                primary: "ui-icon-lightbulb"
            }
        });
        $("#addons_trial_info").button({
            icons: {
                primary: "ui-icon-mail-closed"
            }, text: false
        });
        $('.buy_addons_btn').button({
            icons: {
                primary: "ui-icon-cart"
            }, text: false
        });
        $('.settings_button').button({
            icons: {
                primary: "ui-icon-gear"
            }, text: false
        });
    });

</script>
</head>
<body class="top_body" style="margin:0px">
<table border="0" cellspacing="0" cellpadding="0" width="100%" height="62px">
<tr valign="middle">
<td valign="middle" style="padding-left:15px">
	<!--<img src="images/langs/<%=default_lng %>/top_logo.gif" alt="DeskAlerts Control Panel" border="0" align="left"/>-->
	<img src="images/top_logo.gif" alt="DeskAlerts Control Panel" border="0" align="left"/>
</td>
<td align="center" width="400px" valign="middle" style="padding:0">
	<span class="attention">You can download demo-client for your operating system:</span><br />
	<span class="download_links">
		<a href="client/deskalerts_setup.exe">Windows</a> | 
		<a href="client/deskalerts_setup.zip">Mac&nbsp;OS&nbsp;X</a> | 
		<a href="client/DeskAlerts_i386.deb">Ubuntu&nbsp;Linux&nbsp;x32</a> | 
		<a href="client/DeskAlerts_amd64.deb">Ubuntu&nbsp;Linux&nbsp;x64</a> |
		<a href="https://play.google.com/store/apps/details?id=com.toolbarstudio.alerts" target="_blank">Android</a> |
		<a href="https://itunes.apple.com/app/mobile-alert-client/id592345705"  target="_blank">iOS</a>
	</span>
	<span class="requirements">see system requirements <a target="_blank" href="http://alert-software.com/support/faq/#WorkstationRequirements">here</a></span>
</td>
<td align="right" valign="middle" style="padding-right:4px;white-space:nowrap">

<a href="SettingsSelection.aspx" id="settingsButton" class="settings_button" target="work_area" title="Settings">&nbsp;</a>


 <% if DEMO <> 1 and ConfHideBuyAddonsLink <> 1 then %>
<a href="buy_addons.asp?use_local=1" class="buy_addons_btn" target="work_area" title="DeskAlers store">&nbsp;</a>
<% end if%>

<a href="#" id="helpOpener" class="logout" style="text-align:center"><%=LNG_HELP %></a>
<%
uid = Session("uid")
if(uid<>"") then
%><a href="logout.asp" target="_top" id="logout" class="logout"><%=LNG_LOGOUT%></a>
<div style="width:100%; text-align:right; white-space:nowrap; padding-top:5px;"><%
	Set RS = Conn.Execute("SELECT name FROM users WHERE id="&uid)
	if(Not RS.EOF) then
		uname=RS("name")
		response.write LNG_CURRENTLY_LOGGED_AS &": <span class='username'>"&uname&"</span>"
	end if

	RS.Close
end if
%></div></td></tr></table>
</body>
</html>
<!-- #include file="db_conn_close.asp" -->