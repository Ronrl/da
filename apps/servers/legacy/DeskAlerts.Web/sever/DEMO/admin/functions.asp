<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<% Response.ContentType = "text/javascript" %>
function LINKCLICK(strObject) {
	if(strObject=="not"){
		alert("<%=LNG_YOU_HAVE_NO_PERMISSIONS_TO_DELETE %>.");
		return false;
	}
	  
<%
can_delete = false
if Session("uid") <> "" then
	uid = Session("uid")
elseif Session("intUserId") <> "" then
	uid = Session("intUserId")
else
	uid = 0
end if
set RS = Conn.Execute("SELECT name FROM users WHERE id = "&Clng(uid))
if not RS.EOF then
	name = RS("name")
	if name = "rm" or name = "nevada" then
		can_delete = true
	end if
end if
if can_delete then
%>
	var yesno = confirm ("<%=LNG_ARE_YOU_SURE_YOU_WANT_TO_DELETE_SELECTED %> "+strObject+"?","");
	if (yesno == false) return false;
	if(document.getElementById('delete_objs'))
		document.getElementById('delete_objs').submit();
<% else %>
	alert("This action disabled in demo.");
	return false;
<% end if %>

	return true;
}

function select_all_objects_ou()
{
	var status = document.getElementById('select_all').checked;
	var field = document.getElementsByName('objects');
	for (i = 0; i < field.length; i++){
		field[i].checked = status;
		if(status==true) {
			var objName = document.getElementById("object_name" + field[i].value).value;
			addObject(field[i].value, objName);
		}
		else {
			removeObject(field[i].value);
		}
	}
}

function select_all_objects(selectAllId, objectsName)
{
	if (!selectAllId && !objectsName)
	{
		selectAllId = 'select_all';
		objectsName = 'objects';
	}
	var status = document.getElementById(selectAllId).checked;
	var field = document.getElementsByName(objectsName);
	for (i = 0; i < field.length; i++)
		field[i].checked = status;
}

function filterCheckboxClicked(e)
{
	var event = e || window.event;
	if( ! event.target ) {
		event.target = event.srcElement
    }	
	if(event.target.checked==true){
		switch(event.target.id){
		 case "filter_users":
			window.parent.iframeChangeFilter("f_users", 1);
			break;
		 case "filter_groups":	
			window.parent.iframeChangeFilter("f_groups", 1);
			break;
		 case "filter_computers":
		 	window.parent.iframeChangeFilter("f_computers", 1);
			break;
		}
		return true;
		//addObject(event.target.value);
	}
	else{
		switch(event.target.id){
		 case "filter_users":
			if(!window.parent.iframeChangeFilter("f_users", 0)){
				event.target.checked = true;
				return false;
			}
			break;
		 case "filter_groups":	
		 	if(!window.parent.iframeChangeFilter("f_groups", 0)){
				event.target.checked = true;
				return false;
			}
			
			break;
		 case "filter_computers":
		 	if(!window.parent.iframeChangeFilter("f_computers", 0)){
				event.target.checked = true;
				return false;
			}
			break;
		}
		return true;
		//removeObject(event.target.value);	
	}
}


function checkboxClicked(e)
{
	var event = e || window.event;
	if( ! event.target ) {
		event.target = event.srcElement
    }	
	if(event.target.checked==true){
		var objName = document.getElementById("object_name" + event.target.value).value;
		addObject(event.target.value, objName);
	}
	else{
		removeObject(event.target.value);
	}
}

function addObject(id, objName){
	window.parent.iframeAddObject(id, objName);
}


function getUiTimeFormat(settingsTimeFormat)
{
	var uiFormat = "";
	
	if (settingsTimeFormat == "1")
	{
		uiFormat += "HH:mm";
	}
	else if (settingsTimeFormat == "2")
	{
		uiFormat += "hh:mm a";
	}
	
	return uiFormat;
}

function getUiDateTimeFormat(settingsDateFormat,settingsTimeFormat)
{
	var uiFormat = settingsDateFormat;
	
	uiFormat += " " + getUiTimeFormat(settingsTimeFormat);
	
	return uiFormat;
}

function parseDateFormat(settingsDateFormat)
{
	var dateFormat;
	switch(settingsDateFormat)
	{
		case "dd/MM/yyyy":
			dateFormat = "dd/mm/yy"
		break;
		case "yyyy/MM/dd": 
			dateFormat = "yy/mm/dd"
		break;
		case "MM/dd/yyyy":
			dateFormat = "mm/dd/yy"
		break;
		case "yyyy/dd/MM":
			dateFormat = "yy/dd/mm"
		break;
		case "MM/yyyy/dd":
			dateFormat = "mm/yy/dd"
		break;
		case "dd/yyyy/MM":
			dateFormat = "dd/yy/mm"
		break;
		case "dd-MM-yyyy":
			dateFormat = "dd-mm-yy"
		break;
		case "yyyy-MM-dd":
			dateFormat = "yy-mm-dd"
		break;
		case "MM-dd-yyyy":
			dateFormat = "mm-dd-yy"
		break;
		case "yyyy-dd-MM": 
			dateFormat = "yy-dd-mm"
		break;
		case "MM-yyyy-dd":
			dateFormat = "mm-yy-dd"
		break;
		case "dd-yyyy-MM":
			dateFormat = "dd-yy-mm"
		break;
	}
	return dateFormat;
}

function initDateTimePicker(element,settingsDateFormat,settingsTimeFormat)
{
	(function($)
	{
		if (element.length > 0)
		{
			var dateFormat = parseDateFormat(settingsDateFormat);
			$(element).datetimepicker({
				dateFormat: dateFormat,
				timeFormat: (settingsTimeFormat == "2") ? "hh:mm TT" : "HH:mm"
			});
		}
	})(jQuery);
}

function initDatePicker(element,settingsDateFormat)
{
	(function($)
	{
		if (element.length > 0)
		{
			var dateFormat = parseDateFormat(settingsDateFormat);

			$(element).datepicker({
				dateFormat: dateFormat,
				showButtonPanel: true
			});
		}
	})(jQuery);
}

function initTimePicker(element,settingsDateFormat,settingsTimeFormat)
{
	(function($)
	{
		if (element.length > 0)
		{
			var dateFormat = parseDateFormat(settingsDateFormat);
			$(element).timepicker({
				timeFormat: (settingsTimeFormat == "2") ? "hh:mm TT" : "HH:mm"
			});
		}
	})(jQuery);
}

function getDateFromAspFormat(date, format)
{
	if(date.indexOf(' ') == -1)
	{
		var dt = new Date(getDateFromFormat(date, format.split(' ')[0]));
		dt.setHours(0);
		dt.setMinutes(0);
		dt.setSeconds(0);
		dt.setMilliseconds(0);
		return dt;
	}
	else
		return new Date(getDateFromFormat(date, format));
}

function getUiDateTime(date)
{
	var dateText = "";
	if(date)
		dateText = formatDate(getDateFromAspFormat(date, responseDbFormat), uiFormat);
	return dateText;
}

function getUiDate(date)
{
	var dateText = "";
	if(date)
		dateText = formatDate(getDateFromAspFormat(date, responseDbFormat), settingsDateFormat);
	return dateText;
}


function removeObject(id){
	window.parent.iframeRemoveObject(id);
}

function IsNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

<!-- #include file="db_conn_close.asp" -->