﻿//------------------------------------------------------------------------------
// <автоматически создаваемое>
//     Этот код создан программой.
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода. 
// </автоматически создаваемое>
//------------------------------------------------------------------------------

namespace DeskAlertsDotNet.Pages {
    
    
    public partial class Lockscreens {
        
        /// <summary>
        /// form1 элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlForm form1;
        
        /// <summary>
        /// headerTitle элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton headerTitle;
        
        /// <summary>
        /// searchTermBox элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputText searchTermBox;
        
        /// <summary>
        /// searchButton элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton searchButton;
        
        /// <summary>
        /// addButton элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlAnchor addButton;
        
        /// <summary>
        /// mainTableDiv элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl mainTableDiv;
        
        /// <summary>
        /// topPaging элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl topPaging;
        
        /// <summary>
        /// upDelete элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton upDelete;
        
        /// <summary>
        /// contentTable элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Table contentTable;
        
        /// <summary>
        /// headerSelectAll элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TableCell headerSelectAll;
        
        /// <summary>
        /// selectAll элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox selectAll;
        
        /// <summary>
        /// titleCell элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TableCell titleCell;
        
        /// <summary>
        /// creationDateCell элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TableCell creationDateCell;
        
        /// <summary>
        /// scheduledCell элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TableCell scheduledCell;
        
        /// <summary>
        /// approveCell элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TableCell approveCell;
        
        /// <summary>
        /// senderCell элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TableCell senderCell;
        
        /// <summary>
        /// actionsCell элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TableCell actionsCell;
        
        /// <summary>
        /// bottomDelete элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton bottomDelete;
        
        /// <summary>
        /// bottomRanging элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl bottomRanging;
        
        /// <summary>
        /// NoRows элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl NoRows;
    }
}
