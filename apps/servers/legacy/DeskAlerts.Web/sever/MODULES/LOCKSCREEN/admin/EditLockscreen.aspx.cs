﻿using System;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Parameters;
using Image = System.Drawing.Image;
using System.Globalization;
using DeskAlerts.Server.Utils;
using DeskAlerts.Server.Utils.Helpers;
using Resources;

namespace DeskAlertsDotNet.Pages
{
    public partial class EditLockscreen : DeskAlertsBasePage
    {
        //use curUser to access to current user of DA control panel      
        //use dbMgr to access to database
        // private string campaignName;
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (!IsPostBack)
            {
                lockscreenPositionSelect.Items.Add(new ListItem(resources.LNG_CENTER,  "1"));
                lockscreenPositionSelect.Items.Add(new ListItem(resources.LNG_TILE,    "2"));
                lockscreenPositionSelect.Items.Add(new ListItem(resources.LNG_STRETCH, "3"));
                lockscreenPositionSelect.Items.Add(new ListItem(resources.LNG_FILL,    "4"));
                lockscreenPositionSelect.Items.Add(new ListItem(resources.LNG_FIT,     "5"));
                lockscreenPositionSelect.Items.Add(new ListItem(resources.LNG_SPAN,    "6"));
                startDateAndTime.Value = DateTimeConverter.ToUiDateTime(DateTime.Now);
                endDateAndTime.Value = DateTimeConverter.ToUiDateTime(DateTime.Now.AddHours(1));
            }

            if (!string.IsNullOrEmpty(Request["alert_campaign"]))
            {
                nonCampaignDiv.Visible = false;

                string campaignName =
                    dbMgr.GetScalarByQuery<string>("SELECT name FROM campaigns WHERE id = " + Request["alert_campaign"]);

                campaignNameLabel.InnerText = campaignName;
                campaignSelect.Value = Request["alert_campaign"];
            }
            else
            {
                campaignDiv.Visible = false;
            }

            if (!string.IsNullOrEmpty(Request["rejectedEdit"]))
            {
                rejectedEdit.Value = Request["rejectedEdit"];
            }

            if (!string.IsNullOrEmpty(Request["shouldApprove"]))
            {
                shouldApprove.Value = Request["shouldApprove"];
            }

            if (!string.IsNullOrEmpty(Request["edit"]))
            {
                edit.Value = Request["edit"];
            }

            if (!string.IsNullOrEmpty(Request["id"]))
            {
                alertId.Value = Request["id"];
                LoadLockscreen(Request["id"]);
            }


            //Add your main code here
        }

        private void LoadLockscreen(string wpId)
        {
            DataRow wpRow = dbMgr.GetDataByQuery("SELECT * FROM alerts WHERE id = " + wpId).First();

            string imgUrl = wpRow.GetString("alert_text");
            string title = wpRow.GetString("title");
            
            DateTime startDate = wpRow.GetDateTime("from_date");
            DateTime endDate = wpRow.GetDateTime("to_date");
            int timeToDisplayVal = wpRow.GetInt("autoclose");

            timeToDisplay.Value = timeToDisplayVal.ToString();
            lockscreenName.Value = title;

            startAndEndDateAndTimeCheckbox.Checked = Convert.ToBoolean(wpRow.GetInt("schedule"));

            if (startDate.Year != 1900)
            {
                startDateAndTime.Value = DateTimeConverter.ToUiDateTime(startDate);
            }

            if (endDate.Year != 1900)
            {
                endDateAndTime.Value = DateTimeConverter.ToUiDateTime(endDate);
            }

            int campaignId = wpRow.GetInt("campaign_id");

            if (campaignId != -1)
            {
                nonCampaignDiv.Visible = false;

                string campaignName =
                    dbMgr.GetScalarByQuery<string>("SELECT name FROM campaigns WHERE id = " + campaignId);

                campaignNameLabel.InnerText = campaignName;
                campaignSelect.Value = campaignId.ToString();
            }


            string serverFolder = Config.Data.GetString("alertsFolder");
            int index = imgUrl.IndexOf("/admin/");

            string filePathPart = imgUrl.Substring(index);

            imgUrl = serverFolder + "\\" + filePathPart.Replace("/", "\\");
            Image lockscreenImage = Image.FromFile(imgUrl);

            int width = lockscreenImage.Width;
            int height = lockscreenImage.Height;

            FileInfo fileInfo = new FileInfo(imgUrl);
            float fileLength = fileInfo.Length;
            string fileName = fileInfo.Name;

            fileLength /= 1024;



            fileNameTd.InnerText = fileName;
            dimensionsTd.InnerText = $"{width} x {height}";
            fileSizeTd.InnerText = fileLength.ToString("0.00") + " KB";

            lockscreenImgTr.Style.Add(HtmlTextWriterStyle.Display, "");
            lockscreenPositionTr.Style.Add(HtmlTextWriterStyle.Display, "");
            lockscreenSrc.Value = wpRow.GetString("alert_text");
            lockscreenImg.Src = wpRow.GetString("alert_text");
            lockscreenPositionSelect.SelectedValue = wpRow.GetString("fullscreen");

        }
    }
}