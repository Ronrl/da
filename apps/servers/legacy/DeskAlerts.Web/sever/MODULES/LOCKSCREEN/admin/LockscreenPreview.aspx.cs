﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace DeskAlertsDotNet.Pages
{
    public partial class LockscreenPreview : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request["image_src"]))
            {
                string imageSrc = Request["image_src"];

                int position = 1;

                if (!string.IsNullOrEmpty(Request["position"]))
                {
                    position = Convert.ToInt32(Request["position"]);
                }


                string curStyle = previewDiv.Attributes["style"];
                switch (position)
                {
                    case 1:
                    {
                        curStyle += "background-repeat: no-repeat; background-position: 50% 50%; background-attachment: fixed;";
                        curStyle += "background-image: url('" + imageSrc + "');";
                        previewImage.Visible = false;
                        break;
                    }
                    case 2:
                    {
                        curStyle += "background-repeat: repeat;";
                        curStyle += "background-image: url('" + imageSrc + "');";
                        previewImage.Visible = false;
                        break;
                    }
                    case 3:
                    {
                        previewDiv.Visible = false;
                        previewImage.ImageUrl = imageSrc;
                        break;
                        
                    }
                }
                previewDiv.Attributes["style"] = curStyle;

             
            }
        }
    }
}