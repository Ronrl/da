<%
'---------------ADD TO CONFIG----------------

'textcall Config
Dim textcallUrl, textcallPostData
' Use the following variables: %mobilePhone% as a mobile phone number, %textcallText% as a textcall text

textcallUseAuth="1" ' "0" - Don't use, "1" - Use www-authorization
textcallAuthUser="%textcallAuthUser%"
textcallAuthPass="%textcallAuthPass%"
phoneUser="%phoneUser%"

textcallUrl = "https://api.twilio.com/2010-04-01/Accounts/%textcallAuthUser%/Calls"
textcallPostData = "From=%userPhone%&To=%mobilePhone%&Url=%callText%"

textcallContentType = "application/x-www-form-urlencoded"

textcallPhoneFormat = "1"   ' "0" - Don't convert phone numbers,
                       ' "1" - Just remove all special chars but not grouping symbols,
                       ' "2" - Just remove all special chars but not grouping symbols and not lead plus "+" symbol,
                       ' "3" - Just remove all special chars,
                       ' "4" - Just remove all special chars but not lead plus "+" symbol,Phone 
                       ' "5" - Convert letters to numeric,
                       ' "6" - Convert letters to numeric and remove all special chars,
                       ' "7" - Convert letters to numeric and remove all special chars but not lead plus "+" symbol,
                       ' "8" - Use international E.164 phone format,
                       ' "9" - Use international E.164 phone format but without lead plus "+" symbol.
textcallDefaultCountryCode = "US" ' ISO 3166-1 two-letter country code. Used for E.164 phone format on numbers without country prefix.

TEXT_TO_CALL=1

'---------------ADD TO CONFIG----------------
%>