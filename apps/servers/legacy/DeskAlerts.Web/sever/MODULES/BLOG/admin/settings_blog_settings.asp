﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<!-- #include file="md5.asp" -->
<%

check_session()

uid = Session("uid")

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>DeskAlerts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/style9.css" rel="stylesheet" type="text/css">
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script language="javascript" type="text/javascript">

	$(document).ready(function() {
		$(".cancel_button").button();
		$(".save_button").button();
	});

</script>
</head>

<body style="margin:0px" class="body">

<%
if (uid <> "")  then
blogName = Request("blogName")
blogUrl = Request("blogUrl")
blogUsername = Request("blogUsername")
blogPassword = Request("blogPassword")
blogID = Request("blogID")
draft = Request("draft")
comments = Request("comments")
postAsPage = Request("postAsPage")
author = Request("author")
id = Request("id")

blogName = Replace(blogName, "'", "''")
blogUrl = Replace(blogUrl, "'", "''")
blogUsername = Replace(blogUsername, "'", "''")
blogPassword = Replace(blogPassword, "'", "''")
author = Replace(author, "'", "''")

if(id = "") then
	Set rsBlogIds = Conn.Execute("SELECT id FROM blogs")
	if not rsBlogIds.EOF then
		id = rsBlogIds("id")
	end if
end if

if(request("a")="2") then
	if(blogName<>"" and blogUrl<>"" and blogUsername<>"" and blogPassword<>"" and blogID<>"" and isNumeric(blogID)) then
		if(id<>"") then 'editing
		sql = "UPDATE blogs SET "&_
			"name = N'"& blogName &"' , "&_
			"url = N'"& blogUrl &"' , "&_ 
			"username = N'"& blogUsername &"', "&_
			"password = N'"& blogPassword &"', "&_
			"blogID = "& blogID &", "&_
			"draft = '"& draft &"', "&_
			"comments = '"& comments &"', "&_
			"postAsPage = '"& postAsPage &"', "&_
			"author = N'"& author &"' "&_
			"WHERE id = '" & id & "'"
		Conn.Execute(sql)
		else 'adding new
		sql = "INSERT INTO blogs (name, url, username, password, blogID, draft, comments, postAsPage, author) VALUES ("&_
			"N'" & blogName & "', "&_
			"N'" & blogUrl & "', "&_
			"N'" & blogUsername & "', "&_
			"N'" & blogPassword & "', "&_
			blogID & ", "&_
			"'" & draft & "', "&_
			"'" & comments & "', "&_
			"'" & postAsPage & "', "&_
			"N'" & author & "')"
		Conn.Execute(sql)
		end if
		success = true
	else
		formFillFail = true
	end if 
	if comments <> "" then comments = true end if
	if postAsPage <> "" then postAsPage = true end if
	if draft = 1 then draft = true else draft=false end if
	
else
	Set rsBlogs = Conn.Execute("SELECT * FROM blogs")
	if not rsBlogs.EOF then
		id = rsBlogs("id")
		blogName = rsBlogs("name")
		blogUrl = rsBlogs("url")
		blogUsername = rsBlogs("username")
		blogPassword = rsBlogs("password")
		blogID = rsBlogs("blogID")
		draft = rsBlogs("draft")
		comments = rsBlogs("comments")
		postAsPage = rsBlogs("postAsPage")
		author = rsBlogs("author")
	end if
end if
%>
<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	<tr>
	<td>
	<table width="100%" height="100%" cellspacing="0" cellpadding="0">
		<tr>
		<td class="main_table_body" height="100%">
		<div style="padding:10px;">

<span class="work_header"><%=LNG_BLOG_SETTINGS %></span><br/>

<form id="blogSettings" method="post" action="settings_blog_settings.asp">
<table border="0">
<tr>
	<td>
		<span id="warning" style="color:red;<% if formFillFail = false then Response.Write("display:none")%>">
			<%=LNG_BLOG_ERROR_DESC %>
		</span>
	</td>
</tr>
<tr>
	<td>
		<span id="success" style="<% if success = false then Response.Write("display:none")%>">
			<%=LNG_BLOG_SETTINGS_SUCCESFULLY_CHANGED %>
		</span>
	</td>
</tr>
<tr>
	<td>
		<input type="hidden" name="id" value="<%=id%>" />
		<fieldset id="required">
			<legend><%=LNG_REQUIRED %></legend>
			<table>
				<tr>
					<td colspan="2">
						<label for="blogName"><%=LNG_BLOG_NAME %></label>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<input type="text" name="blogName" id="blogName" value="<%=blogName %>"/>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<label for="blogUrl"><%=LNG_BLOG_URL %></label>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<input type="text" name="blogUrl" id="blogUrl" value="<%=blogUrl %>"/>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<label for="blogUsername"><%=LNG_USERNAME %></label>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<input type="text" id="blogUsername" name="blogUsername" value="<%=blogUsername %>" autocomplete="off"/>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<label for="blogPassword"><%=LNG_PASSWORD %></label>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<input type="password" id="blogPassword" name="blogPassword" value="<%=blogPassword %>" autocomplete="off"/>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<label for="blogID"><%=LNG_BLOG_ID %></label>
					</td>
				</tr>
				<tr>
					<td>
						<input type="text" name="blogID" id="blogID" value="<%=blogID %>"/>
					</td>
					<td>
						<span style="color:#aaaaaa"><%=LNG_BLOG_ID_DESC %></span>
					</td>
				</tr>
			</table>
		</fieldset>
	</td>
</tr>
<tr>
	<td>
		<fieldset>
			<legend><%=LNG_ADDITIONAL %></legend>
			<table>
				<tr>
					<td style="width:auto">
						<input type="radio" name="draft" id="postImmediately" value="0" <% if (draft=false) then Response.write("checked") end if %>/>
					</td>
					<td>
						<label for="postImmediately"><%=LNG_BLOG_POST_IMMEDIATELY %></label>
					</td>
				</tr>
				<tr>
					<td>
						<input type="radio" name="draft" id="saveDraft" value="1" <% if (draft=true) then Response.write("checked") end if %>/>
					</td>
					<td>
						<label for="saveDraft"><%=LNG_BLOG_SAVE_DRAFT %></label>
					</td>
				</tr>
				<tr>
					<td>
						<input type="checkbox" name="comments" id="comments" value="1" <% if (comments=true) then Response.write("checked") end if %>/>
					</td>
					<td>
						<label for="comments"><%=LNG_BLOG_ALLOW_COMMENTS %></label>
					</td>
				</tr>
				<tr>
					<td>
						<input type="checkbox" name="postAsPage" id="postAsPage" value="1" <% if (postAsPage=true) then Response.write("checked") end if %>/>
					</td>
					<td>
						<label for="postAsPage"><%=LNG_BLOG_AS_PAGE %></label>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<label for="author"><%=LNG_BLOG_AUTHOR %></label>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<input type="text" id="author" name="author" value="<%=author %>"/>
					</td>
				</tr>
			</table>
			<span style="color:#aaaaaa"><%=LNG_BLOG_AUTHOR_DESC %></span>
		</fieldset>
	</td>
</tr>
<tr><td align="right">
	<!--a class="cancel_button" onclick="javascript:history.go(-1); return false;"><%=LNG_CANCEL %></a!--> 
	<a class="save_button" onclick="document.forms[0].submit()" ><%=LNG_SAVE %></a>
	<input type="hidden" name="a" value="2"/>
</td></tr></table>
</form>

		 <img src="images/inv.gif" alt="" width="1" height="100%" border="0"/>
		</div>
		</td>
		</tr>
	</table>
	</td>
	</tr>
</table>

</body>
</html>
<%

else

  Response.Redirect "index.asp"

end if

%> 
</body>
</html>
<!-- #include file="db_conn_close.asp" -->