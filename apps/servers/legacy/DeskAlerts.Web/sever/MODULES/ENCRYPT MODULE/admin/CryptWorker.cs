﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DeskAlertsDotNet.Pages;
using DeskAlertsDotNet.Parameters;

namespace DeskAlertsDotNet.Cryptography
{
    public enum CryptAlgorithms
    {
        RC4 = 0,
        AES256 = 1
    }
    public static class CryptWorker
    {
        private static readonly string mobilePassword = "TECwDqpos2";

        public static string Encode(string original, CryptAlgorithms cryptAlg, bool isMobile)
        {

            if (cryptAlg == CryptAlgorithms.RC4)
            {
                string key = !isMobile ? Config.Data.GetString("ENCRYPT_KEY") : mobilePassword;
                RC4 rc4 = new RC4(key.AsByteArray());

                return rc4.Encode(original);
            }

            return Aes256.Encode(original, isMobile);


        }


        public static string Decode(string original, CryptAlgorithms cryptAlg, bool isMobile)
        {

            if (cryptAlg == CryptAlgorithms.RC4)
            {
                string key = !isMobile ? Config.Data.GetString("ENCRYPT_KEY") : mobilePassword;
                RC4 rc4 = new RC4(key.AsByteArray());

                return rc4.Decode(original);
            }

            return Aes256.Decode(original, isMobile);


        }
    }
}