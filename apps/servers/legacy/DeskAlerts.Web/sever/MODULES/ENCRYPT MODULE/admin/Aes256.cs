﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Security.Cryptography;
using DeskAlertsDotNet.Pages;
using DeskAlertsDotNet.Parameters;

namespace DeskAlertsDotNet.Cryptography
{
    public class Aes256
    {
       private static AesManaged desktopAesManaged = new AesManaged();
       private static AesManaged mobileAesManaged = new AesManaged();


       protected static readonly byte[] salt = {0, 1, 3, 5, 7, 8, 20, 40, 50, 90, 251};

        private static readonly string mobilePassword = "TECwDqpos2";

        private static string Encode(string src, AesManaged aes)
        {
            string result = string.Empty;
           // byte[] secretKey = Config.Data.GetString("ENCRYPT_KEY").AsByteArray();

          //  GenerateKey(Config.Data.GetString("ENCRYPT_KEY"));



            ICryptoTransform encryptor = aes.CreateEncryptor();

            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor, CryptoStreamMode.Write))
                {
                    using (StreamWriter sw = new StreamWriter(cs))
                    {
                        sw.Write(src);
                    }
                }
                result = ms.ToArray().AsString();
            
            }

            return result;
        }


        public static string Encode(byte[] src, bool isMobile = false)
        {
            return Encode(BytesToString(src), isMobile);
        }


        private static string Decode(string src, AesManaged aes)
        {
            string result = string.Empty;
          //  byte[] secretKey = Config.Data.GetString("ENCRYPT_KEY").AsByteArray();

          //  GenerateKey(Config.Data.GetString("ENCRYPT_KEY")); 

            ICryptoTransform decryptor = aes.CreateDecryptor();

            using (MemoryStream ms = new MemoryStream(StringToBytes(src)))
            {
                using (CryptoStream cs = new CryptoStream(ms, decryptor, CryptoStreamMode.Read))
                {
                    using (StreamReader sr = new StreamReader(cs))
                    {
                        result = sr.ReadToEnd();
                    }
                }
              

            }

            return result;           
        }

        public static string Decode(byte[] src, bool isMobile = false)
        {
            return Decode(BytesToString(src), isMobile);
        }

        public static string Decode(string src, bool isMobile = false)
        {
            return Decode(src, isMobile ? mobileAesManaged : desktopAesManaged);
        }


        public static string Encode(string src, bool isMobile = false)
        {
            return Encode(src, isMobile ? mobileAesManaged : desktopAesManaged);
        }

        private static void GenerateKey(string password)
        {

            Rfc2898DeriveBytes rfc2898DeriveBytes =
          new Rfc2898DeriveBytes(password, salt);
            desktopAesManaged.Key = rfc2898DeriveBytes.GetBytes(desktopAesManaged.KeySize / 8);
            desktopAesManaged.IV = rfc2898DeriveBytes.GetBytes(desktopAesManaged.BlockSize / 8);


            Rfc2898DeriveBytes mobileRfc2898DeriveBytes = new Rfc2898DeriveBytes(mobilePassword, salt);
            mobileAesManaged.Key = mobileRfc2898DeriveBytes.GetBytes(mobileAesManaged.KeySize / 8);
            mobileAesManaged.IV = mobileRfc2898DeriveBytes.GetBytes(mobileAesManaged.BlockSize / 8);
           // return aes.Key;

        }

        public static void Init(byte[] passwordBytes)
        {
            Init(BytesToString(passwordBytes));
        }


        public static void Init(string passwordBytes)
        {
            GenerateKey(passwordBytes);
        }


        private static byte[] StringToBytes(string str)
        {
            byte[] res = new byte[str.Length * sizeof(char)];
            Buffer.BlockCopy(str.ToCharArray(), 0, res, 0, res.Length);
            return res;
        }

        private static string BytesToString(byte[] arr)
        {
            char[] chars = new char[arr.Length / sizeof(char)];
            Buffer.BlockCopy(arr, 0, chars, 0, arr.Length);
            return new string(chars);
        }

    }
}