﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<!-- #include file="md5.asp" -->
<%

check_session()

uid = Session("uid")

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>DeskAlerts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/style9.css" rel="stylesheet" type="text/css">
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script language="javascript" type="text/javascript">

	$(document).ready(function() {
		$(".cancel_button").button();
		$(".save_button").button();
	});

</script>
</head>
<style type="text/css">
	.instructions
	{
		color:#888888;
	}
</style>
<body style="margin:0px" class="body">

<%
if (uid <> "")  then

id = Request("id")
APIKey = Request("APIKey")
APIKey = TrimAll(Replace(APIKey, "'", "''"))

if(id = "") then
	Set rsLinkedInIds = Conn.Execute("SELECT id FROM social_media WHERE type=2")
	if not rsLinkedInIds.EOF then
		id = rsLinkedInIds("id")
	end if
end if

if(request("a")="2") then
	if(APIKey<>"") then
		if(id<>"") then 'editing
		sql = "UPDATE social_media SET field1 = N'"& APIKey &"' WHERE id = '" & id & "'"
		Conn.Execute(sql)
		else 'adding new
		sql = "INSERT INTO social_media (type, field1) VALUES (2, N'"& APIKey &"')"
		Conn.Execute(sql)
		end if
		success = true
	else
		formFillFail = true
	end if 
else
	Set rsLinkedin = Conn.Execute("SELECT * FROM social_media WHERE type=2")
	if not rsLinkedin.EOF then
		id = rsLinkedin("id")
		APIKey = rsLinkedin("field1")
	end if
end if
%>
<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	<tr>
	<td>
	<table width="100%" height="100%" cellspacing="0" cellpadding="0">
		<tr>
		<td class="main_table_body" height="100%">
		<div style="padding:10px;">

<span class="work_header"><%=LNG_LINKEDIN_SETTINGS %></span><br/>
<div class="instructions" style="margin-left:3px"><%= LNG_LINKEDIN_INSTRUCTIONS %></div>
<form id="linkedinSettings" method="post" action="settings_social_linkedin.asp">
	<table border="0">
		<tr>
			<td>
				<label for="apikey"><%=LNG_LINKEDIN_API_KEY %></label>
			</td>
		</tr>
		<tr>
			<td>
				<input style="width:400px" type="text" id="apikey" name="apikey" value="<%=HtmlEncode(APIKey) %>" autocomplete="off"/>
			</td>
		</tr>
		<tr>
		<td>
			<span id="warning" style="color:red;font-weight:bold;<% if formFillFail = false then Response.Write("display:none")%>">
				<%=LNG_LINKEDIN_ERROR_DESC %>
			</span>
		</td>
		</tr>
		<tr>
			<td>
				<span id="success" style="font-weight:bold;<% if success = false then Response.Write("display:none")%>">
					<%=LNG_LINKEDIN_SETTINGS_SUCCESSFULLY_CHANGED %>
				</span>
			</td>
		</tr>
		<tr>
			<td align="right" style="width:400px">
				<!--a class="cancel_button" onclick="javascript:history.go(-1); return false;"><%=LNG_CANCEL %></a!--> 
				<a class="save_button" name="save_button" onclick="document.forms[0].submit()" ><%=LNG_SAVE %></a>
				<input type="hidden" name="a" value="2"/>
				<input type="hidden" name="id" value="<%=id %>" />
			</td>
			<td></td>
		</tr>
	</table>
</form>

		 <img src="images/inv.gif" alt="" width="1" height="100%" border="0"/>
		</div>
		</td>
		</tr>
	</table>
	</td>
	</tr>
</table>

</body>
</html>
<%

else

  Response.Redirect "index.asp"

end if

%> 
</body>
</html>
<!-- #include file="db_conn_close.asp" -->