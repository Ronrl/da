<% Response.Expires = 0 %>
<% db_backup_no_redirect = 1 %>
<!-- #include file="admin/config.inc" -->
<!-- #include file="admin/db_conn.asp" -->
<!-- #include file="admin/lang.asp" -->
<%
surveyId=Clng(Request("survey_id"))
set answerIds = Request("answer")
set answerNames = Request("aname")
userId=Clng(Request("user_id"))
questionNumber=Clng(Request("q_num"))
questionType=Replace(Request("q_type"),"'","''")
questionId=Clng(Request("q_id"))
condYes=Clng(Request("c_yes"))
condNo=Clng(Request("c_no"))
answer=replace(Request("custom_answer"), "'", "''")
desk_id=Replace(Request("deskbar_id"),"'","''")

if ConfEnableMultiVote = 1 then
	desk_query = " AND (clsid='" & desk_id & "' OR clsid='')"
else
	desk_query = ""
end if

if(surveyId <> 0 AND userId <> 0 AND questionNumber<> 0) then

	'count number of questions for the survey
	surveyQuestionsNumber=0

	Set RSNum = Conn.Execute("SELECT COUNT(id) as qNumber FROM surveys_questions WHERE survey_id=" & surveyId)
	if(Not RSNum.EOF) then
		surveyQuestionsNumber=RSNum("qNumber")
	end if
	RSNum.Close

	'response.write "num=" & surveyQuestionsNumber

	nextQuestion=0
	text="<body>"
	
	if(userId = -1) then
	    text="<body bgcolor='white'>"
	end if
	
	if(questionNumber < surveyQuestionsNumber) then
		nextQuestion=1
	end if
	'finish survey if answer is NO
	if(questionType="C") then
		if(condNo=CLng(Request("answer"))) then
			nextQuestion=0
		end if
	end if
	
	Set RS = Conn.Execute("SELECT closed, stype, sender_id FROM surveys_main WHERE id=" & surveyId)
	if(Not RS.EOF) then
		if(RS("closed")="N") then
			stype=RS("stype")
			
			alertId = RS("sender_id")
			Conn.Execute("UPDATE alerts_received SET vote=1 WHERE alert_id=" & alertId & " AND user_id=" & userId & desk_query)
			
			'select answer ids for the question
			answer_ids=""
			answerFlag=0
			Set RSAnswer = Conn.Execute("SELECT surveys_answers.id as answer_id FROM surveys_answers INNER JOIN surveys_questions ON surveys_questions.id=surveys_answers.question_id WHERE surveys_questions.survey_id=" & surveyId & " AND question_number=" & questionNumber)
			Do While not RSAnswer.EOF
				answer_id=RSAnswer("answer_id")
				if(answerFlag=1) then
					answer_ids=answer_ids & " OR " & " answer_id=" & answer_id 
				else
					answer_ids=answer_ids & " answer_id=" & answer_id 
				end if
				answerFlag=1
				RSAnswer.MoveNext	
			loop
			answer_ids=replace(answer_ids, "answer_id", "surveys_answers_stat.answer_id")

			if questionType<>"I" and questionType<>"T"  and questionType<>"S"   then
				Set RS1 = Conn.Execute("SELECT id FROM surveys_answers_stat WHERE ("& answer_ids &") AND user_id=" & userId & desk_query)
				if(RS1.EOF or userId = -1) then
				
				    if(userId <> -1) then
					    for each answerId in answerIds
						    Conn.Execute("INSERT INTO surveys_answers_stat (user_id, answer_id, date, clsid) VALUES (" & userId & ", " & answerId & ", GETDATE(), '" & desk_id & "')")
					    next
					end if
					'go to next question
					if(nextQuestion=1) then
						Response.Redirect("get_survey.asp?id=" & surveyId & "&user_id=" & userId & "&q_num=" & questionNumber & "&deskbar_id=" & Request("deskbar_id"))
					elseif stype = "3" then
						Response.Redirect "survey_view.asp?id=" & surveyId
					else
						text = text & "<br/><b><center>" & LNG_YOUR_SURVEY_WAS_SUBMITTED &"."
					end if
				else'already voted
					text = "<br/><b><center>"&LNG_VOTE_SORRY1&".</b> "
					if(nextQuestion=1) then
						text = text & "<a href='get_survey.asp?id=" & surveyId & "&user_id=" & userId & "&q_num=" & questionNumber & "&deskbar_id=" & Request("deskbar_id") & "'>Next question</a>"
					elseif stype = "3" then
						Response.Redirect "survey_view.asp?id=" & surveyId & "&msg=" & Server.URLEncode(LNG_VOTE_SORRY1)
					end if
				end if
			else
				'custom answer
				Set RS1 = Conn.Execute("SELECT id FROM surveys_custom_answers_stat WHERE question_id="&questionId&" AND user_id=" & userId & desk_query)
				if(RS1.EOF or userId = -1) then
					if answer = "" then
						answer = ""
						for i = 1 to answerIds.Count
							answerText = answerIds(i)
							if answer <> "" then answer = answer & " " & vbNewLine
							answer = answer & answerNames(i) & ": " & answerText
						next
						answer=Replace(answer,"'","''")
					end if
					
					if(userId <> -1 or questionType <> "S") then
					    SQL = "INSERT INTO surveys_custom_answers_stat (user_id, question_id, date, clsid, answer) VALUES (" & userId & ", " & questionId & ", GETDATE(), '" & desk_id & "', N'"&answer&"')"
					    Conn.Execute(SQL)
					end if
					'go to next question
					if(nextQuestion=1) then
						Response.Redirect("get_survey.asp?id=" & surveyId & "&user_id=" & userId & "&q_num=" & questionNumber & "&deskbar_id=" & Request("deskbar_id"))
					elseif stype = "3" then
						Response.Redirect "survey_view.asp?id=" & surveyId
					else
						text = text &  "<br/><b><center>" & LNG_YOUR_SURVEY_WAS_SUBMITTED &"."
					end if
				else'already voted
					text = "<br/><b><center>"&LNG_VOTE_SORRY1&".</b> "
					if(nextQuestion=1) then
						text = text & "<a href='get_survey.asp?id=" & surveyId & "&user_id=" & userId & "&q_num=" & questionNumber & "&deskbar_id=" & Request("deskbar_id") & "'>Next question</a>"
					elseif stype = "3" then
						Response.Redirect "survey_view.asp?id=" & surveyId & "&msg=" & Server.URLEncode(LNG_VOTE_SORRY1)
					end if
				end if
			end if
			else'closed
				text = text &  "<br/><b><center>"&LNG_VOTE_SORRY2&"."
			end if
			RS.Close
		
		    submitButton = "close_button.gif"
		    

			if(userId <> -1 ) then
			    text = text & "<br/><br/><br/><img src='admin/images/langs/en/close_button.gif' alt='Close' border='0' onclick='window.parent.postMessage(""closeMe"",""*"" );if(window.external) {window.external.Close();}else{Close();}'/>"
            else
                text = text & "<br/><br/><br/><img src='admin/images/langs/en/close_button.gif' alt='Close' border='0' onclick='window.parent.close();'/>"
            end if
		'if (ENC = 1) then
		'	S=RC4(encrypt_key, "<html>" + text + "</html>")
		'	Response.Write S
		'else
			Response.Write "<html>" + text + "</body></html>"
		'end if
	end if
end if

%><!-- #include file="admin/db_conn_close.asp" -->