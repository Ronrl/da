﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;
using Newtonsoft.Json;

namespace DeskAlertsDotNet.Pages
{
    public class Question
    {
        [JsonProperty("question_type")]
        public string QuestionType { get; set; }

        [JsonProperty("question")]
        public string QuestionText { get; set; }

        [JsonProperty("answers")]
        public Answer[] Answers { get; set; }
    }

    public class Answer
    {
        public string Value;
        public string Correct;
    }

    public partial class GetSurveyFromData : System.Web.UI.Page
    {
        public Question[] Questions;

        private string GetHtmlForQuestion(int number)
        {
            if (number >= Questions.Length)
            {
                return string.Empty;
            }

            var question = Questions[number];
            var type = question.QuestionType.Equals("C") ? "M" : question.QuestionType;
            var templateHtml = File.ReadAllText(Server.MapPath("admin/templates/" + type + ".html"));
            var answersString = string.Empty;
            var answers = question.Answers;

            foreach (var answer in answers)
            {
                switch (type)
                {
                    case "M":
                        {
                            answersString += $"<tr><td><input name='answer' type=\"radio\" ></td><td>{answer.Value}</td></tr>";
                            break;
                        }
                    case "N":
                        {
                            answersString += $"<tr><td><input type=\"checkbox\" ></td><td>{answer.Value}</td></tr>";
                            break;
                        }
                    case "I":
                        {

                            break;
                        }
                    case "T":
                        {
                            answersString += "<tr><td colspan='2'>" +
                                             $"<div align='center' style='width:100%; overflow:hidden; word-wrap: break-word;'> {answer.Value} </div>" + 
                                             "</td></tr>" + 
                                             "<tr><td colspan='2'>" +
                                             "<div align='center'><input type='text' style='width:300px; height:35px' ></input></div>" + 
                                             "</td></tr>";
                            break;
                        }
                    default:
                        {
                            DeskAlertsBasePage.Logger.Error($"{type} is not correct type for survey question");
                            break;
                        }
                }
            }
            if (type == "S")
            {
                templateHtml = templateHtml.Replace("{serverUrl}/", string.Empty);
            }

            var questionHtml = templateHtml.Replace("{ANSWERS}", answersString)
                                              .Replace("{TITLE}", question.QuestionText)
                                              .Replace("{NUMBER}", (number + 1).ToString())
                                              .Replace("{DATA}", HttpUtility.HtmlEncode(Request["data"]))
                                              .Replace("{END}", number == Questions.Length - 1 ? "1" : "0");
            return questionHtml;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ServicePointManager.ServerCertificateValidationCallback =
                    (s, certificate, chain, sslPolicyErrors) => true;

                if (!string.IsNullOrEmpty(Request["shouldEnd"]) && Request["shouldEnd"].Equals("1"))
                {
                    Response.Write("<body bgcolor=\"white\">");
                    Response.Write(" <center>Your survey was submitted.<br><br><br><img src=\"admin/images/langs/en/close_button.gif\" alt=\"Close\" border=\"0\" onclick=\"window.parent.close();\"></center>");
                    Response.Write("</body>");
                    return;
                }

                var surveyDataString = string.IsNullOrEmpty(Request["data"]) ? string.Empty : Request["data"];

                if (!string.IsNullOrEmpty(surveyDataString))
                {
                    //ToDo: Crutch
                    Questions = JsonConvert.DeserializeObject<Question[]>(surveyDataString.Replace("\"correct\":0", "\"correct\":false"));

                    var number = string.IsNullOrEmpty(Request["num"]) ? 0 : Convert.ToInt32(Request["num"]);

                    Response.Write(GetHtmlForQuestion(number));
                }
            }
            catch (Exception ex)
            {
                DeskAlertsBasePage.Logger.Error(ex);
            }
        }

        public Dictionary<string, string> GetAspVariables(params string[] keys)
        {
            string queryString = String.Join(":", keys);
            string url = HttpContext.Current.Request.Url.AbsoluteUri;
            string path = url.Substring(0, url.IndexOf("addSurvey"));
            string url_bridge = path + "/dotnetbridge.asp?var=" + queryString;
            WebRequest request = WebRequest.Create(url_bridge);
            WebResponse response = request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            // Response.Write(reader.ReadToEnd());
            string[] variablesStrings = reader.ReadToEnd().Split('&');
            Dictionary<string, string> result = new Dictionary<string, string>();
            foreach (string keyVal in variablesStrings)
            {
                string[] keyValArray = keyVal.Split(':');
                if (keyValArray.Length != 2)
                    continue;

                string key = keyValArray[0];
                string value = keyValArray[1];
                result.Add(key, value);
            }

            return result;
        }

        public string GetAspVariable(string variableName)
        {
            string url = HttpContext.Current.Request.Url.AbsoluteUri;
            string path = url.Substring(0, url.IndexOf("addSurvey"));
            string url_bridge = path + "/dotnetbridge.asp?var=" + variableName;
            WebRequest request = WebRequest.Create(url_bridge);
            WebResponse response = request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            string variable = reader.ReadToEnd();
            return variable;
        }
    }
}