<!-- #include file="admin/config.inc" -->
<!-- #include file="admin/db_conn.asp" -->
<%
language="British"
Session.LCID = 2057

id = CLng(Request("id"))

response.AddHeader "content-type", "text/html; charset=utf-8"

alertData = "{ "


	set RSsurvey = Conn.Execute("SELECT s.sender_id as alert_id, s.create_date as create_date, a.alert_width, a.alert_height, a.fullscreen, a.lifetime, a.resizable, s.name as name, a.from_date as from_date, a.to_date as to_date, a.schedule, a.caption_id as skin, s.stype as type, a.autoclose as autoclose FROM surveys_main as s INNER JOIN alerts as a ON a.id = s.sender_id WHERE  s.id = " & id)
	if not RSsurvey.EOF then
            alertData = alertData & """" & "alert_id" & """" & ":" & """" & RSsurvey("alert_id") & ""","
	        alertData = alertData & """" & "name" & """" & ":" & """" & RSsurvey("name") & ""","
	        
	        skin_id = RSsurvey("skin")
	        if( IsNull(skin_id) or skin_id = "") then
	            skin_id = "default"
	        end if
	        
	        alert_width =  RSsurvey("alert_width")
	        
	        if( IsNull(alert_width)) then
	            alert_width = "500"
	        end if
	        
	        
	        alert_height =  RSsurvey("alert_height")
	        
	        if( IsNull(alert_height)) then
	            alert_height = "400"
	        end if
	        
	        'lifetime = RSSurvey("lifetime")
	        
	       ' if(IsNull(lifetime))
	           ' lifetime = "1"
	        'end if
	        
	        start_date = RSSurvey("from_date")
	        end_date = RSSurvey("to_date")
	        
	        lifetime = ""
	      if start_date <> "" and end_date <> "" and RSsurvey("schedule") = "0" then
	            
				if year(end_date) <> 1900 then
					lifetime = DateDiff("n", start_date, end_date)
					recurrence = ""
				end if
		 end if
	        
	        alertData = alertData & """" & "skin_id" & """" & ":" & """" &jsEncode(skin_id) & ""","
	         alertData = alertData & """" & "stype" & """" & ":" & RSsurvey("type") & ","
	         alertData = alertData & """" & "start_date" & """" & ":" & """" & RSsurvey("from_date")  & ""","
	         alertData = alertData & """" & "end_date" & """" & ":" & """"  &RSsurvey("to_date") & ""","
	          alertData = alertData & """" & "schedule" & """" & ":" & RSsurvey("schedule") & ","
	          alertData = alertData & """" & "alert_width" & """" & ":" & alert_width & ","
	           alertData = alertData & """" & "alert_height" & """" & ":" & alert_height & ","
	           alertData = alertData & """" & "fullscreen" & """" & ":" & RSsurvey("fullscreen") & ","
	           alertData = alertData & """" & "resizable" & """" & ":" & """"  & RSsurvey("resizable") & ""","
	           alertData = alertData & """" & "create_date" & """" & ":" & """" & RSsurvey("create_date") & ""","
	           alertData = alertData & """" & "lifetime" & """" & ":" & """" & lifetime & ""","
	         cant_close = "0"
	         if(RSsurvey("autoclose") = "2147482647") then
	           cant_close = "1"     
	         end if
	          alertData = alertData & """" & "cant_close" & """" & ":" & """" & cant_close & ""","
	          
	          set RSQuestions = Conn.Execute("SELECT question, question_type, id FROM surveys_questions WHERE survey_id = " & id)
	          
	          if not RSQuestions.EOF then
	              alertData = alertData & """" & "questions" & """" & ":" &  "[" 
	            
	                
	              do while not RSQuestions.EOF 
	                    alertData = alertData & "{" 
	                    alertData = alertData & """" & "id" & """" & ":" &  """" & jsEncode(RSQuestions("id")) & ""","
    	                alertData = alertData & """" & "title" & """" & ":" &  """" & jsEncode(RSQuestions("question")) & ""","
    	                alertData = alertData & """" & "type" & """" & ":" &  """" & jsEncode(RSQuestions("question_type")) & ""","
    	                alertData = alertData & """" & "options" & """" & ":" &  "["
        	            
        	            
    	                set RSAnswers = Conn.Execute("SELECT answer, correct FROM surveys_answers WHERE question_id = " & RSQuestions("id"))
        	            
        	            needToDelete = false
    	                do while not RSAnswers.EOF
    	                   if(not needToDelete) then
    	                    needToDelete = true
    	                   end if
     	                    alertData = alertData & "{"
     	                    alertData = alertData & """" & "value" & """" & ":" &  """" & jsEncode(RSAnswers("answer")) & ""","
     	                    
     	                    correct = "1"
     	                    
     	                    if RSAnswers("correct") = "False" then
     	                        correct = "0"
     	                    end if
     	                    alertData = alertData & """" & "correct" & """" & ":" & jsEncode(correct)
    	                    alertData = alertData & "},"   	
    	                    RSAnswers.MoveNext            
    	                loop
                        
                        if(needToDelete) then
	                       alertData = left(alertData, len(alertData) - 1)
	                    end if
    	                  alertData = alertData & "]},"
	                    RSQuestions.MoveNext
	              loop
	                alertData = left(alertData, len(alertData) - 1)
    	            alertData = alertData & "]"
	              
	             ' alertData = alertData & "}]"
	          end if
           
	end if
	'alertData = alertData & """template_id"": """&jsEncode(RS("template_id"))&""""

    
alertData = alertData & " }"


Response.Write alertData

%>
<!-- #INCLUDE FILE="admin/db_conn_close.asp" -->