﻿using Castle.MicroKernel.Lifestyle;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Enums;
using DeskAlerts.ApplicationCore.Utilities;
using DeskAlerts.Scheduler.ContentJobScheduling;
using DeskAlerts.Server.Utils.Helpers;
using DeskAlertsDotNet.Application;
using DeskAlertsDotNet.Models.Permission;
using DeskAlertsDotNet.Parameters;
using DeskAlertsDotNet.Utils.Consts;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web;

namespace DeskAlertsDotNet.Pages
{
    public partial class ProceedSurvey : DeskAlertsBasePage
    {
        const int autocloseTimeout = 2147482647; // set timeout to 68 years TODO investigate why 2592000 seconds is do autoclosed imediatelly

        private readonly IContentJobScheduler _contentJobScheduler;

        public ProceedSurvey()
        {
            using (Global.Container.BeginScope())
            {
                _contentJobScheduler = Global.Container.Resolve<IContentJobScheduler>();
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            string surveyTitle = Request["title"];
            string plainSurveyTitle = Request["plainTitle"];
            string skinId = Request["skinId"];
            string anonymous = Request["anonymous_survey"];
            int alertId = 0;
            int surveyId = 0;

            if (!string.IsNullOrEmpty(Request["approveAlertId"]))
            {
                alertId = Convert.ToInt32(Request["approveAlertId"]);
            }

            int surveyType = Convert.ToInt32(Request["stype"]);

            bool surveyCanNotBeClosed = Request["cantClose"] == "1";

            string duplicateSurveyId = Request.GetParam("alertId", "");

            //encode plas in line 
            var encodePlas = Request["surveyData"].Replace("+", "%2b");
            string jsonSurveyData = HttpUtility.UrlDecode(encodePlas);

            bool hasLifetime = Request["has_lifetime"] == "1";

            int alertWidth = 500;

            if (!string.IsNullOrEmpty(Request["alert_width"]))
                alertWidth = Convert.ToInt32(Request["alert_width"]);

            int alertHeight = 400;

            if (!string.IsNullOrEmpty(Request["alert_height"]))
                alertHeight = Convert.ToInt32(Request["alert_height"]);

            bool scheduled = Request["schedule"] == "1";

            string campaignId = Request.GetParam("campaignId", "-1");

            int position = 6;

            if (!string.IsNullOrEmpty(Request["position"]))
                position = Convert.ToInt32(Request["position"]);

            string startDate = Request["dateStart"];
            string endDate = Request["dateEnd"];

            if (!string.IsNullOrEmpty(startDate))
            {
                startDate = DateTimeConverter.ConvertFromUiToDb(startDate);
            }

            if (!string.IsNullOrEmpty(endDate))
            {
                endDate = DateTimeConverter.ConvertFromUiToDb(endDate);
            }

            int fullscreen = Convert.ToInt32(Request["fullscreen"]);

            bool resizable = Request["resizable"] == "1";

            #region Alert Insertion


            Dictionary<string, object> alertDictionary = new Dictionary<string, object>();
            
            if (surveyCanNotBeClosed)
            {
                alertDictionary.Add("autoclose", autocloseTimeout);
            }

            alertDictionary.Add("schedule", Convert.ToInt32(scheduled));

            alertDictionary.Add("lifetime", Convert.ToInt32(hasLifetime));
            alertDictionary.Add("campaign_id", campaignId);
            alertDictionary.Add("anonymous_survey", anonymous); ;

            alertDictionary.Add("title", plainSurveyTitle);
            alertDictionary.Add("alert_text", string.Format("<!-- html_title=\"{0}\" -->", surveyTitle));


            alertDictionary.Add("from_date", startDate);
            alertDictionary.Add("to_date", endDate);
            alertDictionary.Add("create_date", DateTimeConverter.ToDbDateTime(DateTime.Now));

            if (fullscreen == 0)
            {
                alertDictionary.Add("fullscreen", position);
                alertDictionary.Add("alert_width", alertWidth);
                alertDictionary.Add("alert_height", alertHeight);
            }
            else
            {
                alertDictionary.Add("fullscreen", fullscreen);
            }

            if (resizable)
                alertDictionary.Add("resizable", 1);

            var skinIdIsEqual = !string.IsNullOrEmpty(skinId) && !string.Equals(skinId, "default", StringComparison.CurrentCultureIgnoreCase);
            if (skinIdIsEqual)
            {
                alertDictionary.Add("caption_id", skinId);
            }

            alertDictionary.Add("sender_id", curUser.Id);


            int alertClass = surveyType * 64;

            if (alertClass == 192)
                alertClass = 256;
            alertDictionary.Add("class", alertClass);

            if (alertId == 0)
            {
                alertId = AlertManager.Default.AddAlert(dbMgr, alertDictionary);
            }
            else
            {
                AlertManager.Default.UpdateAlert(dbMgr, alertDictionary, alertId);
                string getSurveyIdQuery = $"select * from surveys_main where sender_id={alertId}";
                surveyId = dbMgr.GetScalarQuery<int>(getSurveyIdQuery);
            }

            #endregion

            #region Survey Insertion

            Dictionary<string, object> surveyDictionary = new Dictionary<string, object>();

            surveyDictionary.Add("name", plainSurveyTitle);
            surveyDictionary.Add("sender_id", alertId);

            if (scheduled)
            {
                surveyDictionary.Add("schedule", 1);
                surveyDictionary.Add("from_date", startDate);
                surveyDictionary.Add("to_date", endDate);
            }
            else
            {
                surveyDictionary.Add("schedule", 0);
                surveyDictionary.Add("from_date", "01.01.1900 00:00:00");
                surveyDictionary.Add("to_date", "01.01.1900 00:00:00");
            }

            surveyDictionary.Add("anonymous_survey", anonymous);
            surveyDictionary.Add("stype", surveyType);
            surveyDictionary.Add("create_date", DateTime.Now.ToString(DateTimeFormat.JavaScript, CultureInfo.InvariantCulture));
            surveyDictionary.Add("group_type", "B");
            surveyDictionary.Add("closed", "N");

            if (surveyId == 0)
            {
                surveyId = dbMgr.Insert("surveys_main", surveyDictionary, "id");
            }
            else
            {
                dbMgr.Update("surveys_main", surveyDictionary, new Dictionary<string, object>() { { "id", surveyId } });
            }

            JArray questionsArray = JArray.Parse(jsonSurveyData);//JsonConvert.DeserializeObject(jsonSurveyData) as JArray;

            ApproveStatusSetup(alertId);

            int questionNumber = 1;
            foreach (var question in questionsArray.Children<JObject>())
            {

                string questionTitle = question["question"].ToString();
                string questionType = question["question_type"].ToString();

                Dictionary<string, object> questionDictionary = new Dictionary<string, object>();

                questionDictionary.Add("question", questionTitle);
                questionDictionary.Add("question_type", questionType);
                questionDictionary.Add("survey_id", surveyId);
                questionDictionary.Add("question_number", questionNumber);

                int questionId = 0;
                if (!string.IsNullOrEmpty(question["id"]?.ToString()))
                {
                    questionId = (int) question["id"];
                }

                if (questionId == 0)
                {
                    questionId = dbMgr.Insert("surveys_questions", questionDictionary, "id");
                }
                else
                {
                    dbMgr.Update("surveys_questions", questionDictionary, new Dictionary<string, object>() { { "id", questionId } });
                }

                // Response.WriteLine("Inserted Question with id = " + questionId);
                foreach (JObject answer in question["answers"])
                {
                    string answerText = answer["value"].ToString();
                    string answerCorrect = answer["correct"].ToString();

                    Dictionary<string, object> answerDictionary = new Dictionary<string, object>();
                    answerDictionary.Add("answer", answerText);
                    answerDictionary.Add("correct", answerCorrect);
                    answerDictionary.Add("question_id", questionId);

                    var answerId = answer["id"]?.Value<int>();
                    
                    if (answerId == null || answerId == 0)
                    {
                        dbMgr.Insert("surveys_answers", answerDictionary);
                    }
                    else
                    {
                        dbMgr.Update("surveys_answers", answerDictionary, new Dictionary<string, object>() { { "id", answerId } });
                    }

                    //  Response.WriteLine("Insert question");
                }
                questionNumber++;


            }

            #endregion

            if (scheduled && AppConfiguration.IsNewContentDeliveringScheme)
            {
                var dateTimeUtc = DateTimeConverter.ParseDateTimeFromDb(startDate, true).ToUniversalTime();
                var contentJobDto = new ContentJobDto(dateTimeUtc, alertId, (AlertType)alertClass);
                _contentJobScheduler.CreateJob(contentJobDto);
            }

            string returnPage = "Surveys.aspx";

            if (campaignId != "-1")
                returnPage = "CampaignDetails.aspx?campaignId=" + campaignId;


            Response.Redirect("RecipientsSelection.aspx?id=" + alertId + "&return_page=" + returnPage + "&desktop=1&device=3&dup_id=" + duplicateSurveyId + "&campaign_id=" + campaignId + "&anonymous_survey=" + anonymous, true);

            //Add your main code here
        }

        private void ApproveStatusSetup(int alertId)
        {
            var isUserHasRight = true;
            if (!curUser.IsAdmin)
            {
                isUserHasRight = curUser.Policy[Policies.SURVEYS].CanApprove;
            }

            var approveStatus = (int)ApproveStatus.ApprovedByDefault;
            if (Config.Data.HasModule(Modules.APPROVE) && Settings.Content["ConfAllowApprove"] == "1" && !isUserHasRight)
            {
                approveStatus = (int)ApproveStatus.NotModerated;
            }

            var query = $@"
                UPDATE alerts 
                SET approve_status = {approveStatus} 
                WHERE alerts.id = {alertId}";
            dbMgr.ExecuteQuery(query);
        }
    }
}