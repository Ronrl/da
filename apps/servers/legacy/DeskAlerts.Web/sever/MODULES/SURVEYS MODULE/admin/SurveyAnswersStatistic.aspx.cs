﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Parameters;
using DeskAlertsDotNet.Utils;


namespace DeskAlertsDotNet.Pages
{
    using Resources;

    /// <summary>
    /// The survey answers statistic.
    /// </summary>
    public partial class SurveyAnswersStatistic : DeskAlertsBasePage
    {
        // use curUser to access to current user of DA control panel      
        // use dbMgr to access to database

        /// <summary>
        /// The survey row.
        /// </summary>
        protected DataRow surveyRow;

        /// <summary>
        /// The anonymous survey for answers.
        /// </summary>
        private string anonymousSurveyForAnsvers = string.Empty;

        /// <summary>
        /// The alert id.
        /// </summary>
        private string alertId = string.Empty;

        /// <summary>
        /// The on load.
        /// </summary>
        /// <param name="e">
        /// The e.
        /// </param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            bool isEndUser = Request.GetParam("client", 0) == 1;
            bool isPreview = Request.GetParam("preview", 1) == 1;
            if (isPreview)
            {
                closeButton.Attributes.Remove("onclick");
                closeButton.Attributes.Add("onclick", "window.parent.close();");
            }
            tableHeader.Visible = workHeader.Visible = !isEndUser;
            if (!isEndUser)
            {
                closeButton.Visible = false;
                closeButton.Attributes.Remove("src");
                closeButton.Attributes.Add("src", "images/close_button.gif");
            }

            surveyRow = dbMgr.GetDataByQuery($"SELECT * FROM surveys_main WHERE id = {this.Request["id"]}").First();
            DataSet questionSet =
                dbMgr.GetDataByQuery(
                    $"SELECT * FROM surveys_questions WHERE survey_id = {this.surveyRow.GetString("id")}");

            foreach (DataRow question in questionSet)
            {
                TableRow questionRow = new TableRow();
                TableCell titleCell = new TableCell();

                titleCell.Text = "<b>" + resources.LNG_QUESTION + "</b>";
                questionRow.Cells.Add(titleCell);
                TableCell questionNameCell = new TableCell
                {
                    Text = this.RemoveSpecialCharacters(question.GetString("question"))
                };

                questionRow.Cells.Add(questionNameCell);

                TableCell endCell = new TableCell();
                endCell.Style.Add(HtmlTextWriterStyle.Width, "200px");
                endCell.Text = "&nbsp";

                questionRow.Cells.Add(endCell);

                questionTable.Rows.Add(questionRow);
                DataSet answerSet =
                    dbMgr.GetDataByQuery(
                        $"SELECT a.id, a.answer, a.correct, COUNT(s.user_id) as votes_cnt FROM surveys_answers a LEFT JOIN surveys_answers_stat s ON a.id=s.answer_id WHERE a.question_id={question.GetString("id")} GROUP BY a.id, a.answer, a.correct ORDER BY a.id");
                if (answerSet.IsEmpty)
                {
                    answerSet = dbMgr.GetDataByQuery(
                        $"select count(user_id) from surveys_custom_answers_stat where question_id ={question.GetString("id")}");
                }

                int totalVotes = dbMgr.GetScalarQuery<int>(
                    $"SELECT COUNT(distinct user_id) as votes_cnt FROM surveys_answers INNER JOIN surveys_answers_stat ON surveys_answers.id=surveys_answers_stat.answer_id WHERE question_id={question.GetString("id")}");

                int questionNum = 1;
                foreach (DataRow answer in answerSet)
                {
                    alertId = Request["id"];

                    TableRow answerRow = new TableRow();
                    if (question.GetString("question_type") == "T" || question.GetString("question_type") == "I")
                    {
                        anonymousSurveyForAnsvers = DetectingAnonymousSurveyForAnsvers();

                        if (anonymousSurveyForAnsvers != "1" && Config.Data.HasModule(Modules.STATISTICS))
                        {
                            TableCell answerCell = new TableCell
                            {
                                Text =
                                   !isEndUser

                                    ? $"<a href=\'#\' onclick=\"javascript: window.open(\'ViewCustomAnswers.aspx?question_id={question.GetString("id")}&id={this.surveyRow.GetString("id")}\',\'\', \'status=0, toolbar=0, width=350, height=400, scrollbars=1\')\">{resources.LNG_VIEW_CUSTOM_ANSWERS}</a> </td>"
                                       : resources.LNG_VIEW_CUSTOM_ANSWERS
                            };
                            answerRow.Cells.Add(answerCell);
                        }
                        else if (!isEndUser && BuyExtendedReports.Visible == false)
                        {

                            var buyPanelTitleLabel = new Label()
                            {
                                Text = "Note: <br />",
                                CssClass = "buy-panel-title"
                            };

                            var buyPanelBodyLabel = new Label()
                            {
                                Text = resources.LNG_BUY_EXTENDED_REPORTS,
                                CssClass = "buy-panel-body"
                            };

                            BuyExtendedReports.Controls.Add(buyPanelTitleLabel);
                            BuyExtendedReports.Controls.Add(buyPanelBodyLabel);
                            BuyExtendedReports.Visible = true;
                        }
                    }
                    else
                    {
                        double votedCnt = answer.GetInt("votes_cnt");

                        double proc = 0;
                        if (totalVotes != 0)
                        {
                            proc = Math.Round(votedCnt / totalVotes, 2);
                        }

                        proc *= 100;
                        int surveyType = surveyRow.GetInt("stype");

                        TableCell titleAnswerCell = new TableCell
                        {
                            Text = "<b>" + resources.LNG_ANSWER + questionNum + "</b> "
                        };

                        answerRow.Cells.Add(titleAnswerCell);
                        string rowHtml;

                        anonymousSurveyForAnsvers = DetectingAnonymousSurveyForAnsvers();

                        if (anonymousSurveyForAnsvers != "1" && Config.Data.HasModule(Modules.STATISTICS))
                        {
                            rowHtml =
                                "<a href=\'#\' onclick=\"javascript: window.open(\'AlertViewRecipients.aspx?type=ans&answer_id={0}\',\'\', \'status=0, toolbar=0, width=350, height=400, scrollbars=1\')\">{1}: {2} </a>";
                            rowHtml = !isEndUser
                                ? string.Format(rowHtml, answer.GetString("id"), answer.GetString("answer"), proc + "%")
                                : $"{answer.GetString("answer")}: {proc + "%"}";
                        }
                        else
                        {
                            rowHtml =
                                "<div onclick=\"javascript:(\'AlertViewRecipients.aspx?type=ans&answer_id={0}\',\'\', \'status=0, toolbar=0, width=350, height=350, scrollbars=1\')\">{1}: {2} </div>";
                            rowHtml = !isEndUser
                                ? string.Format(rowHtml, answer.GetString("id"), answer.GetString("answer"), proc + "%")
                                : $"{answer.GetString("answer")}: {proc + "%"}";
                        }

                        rowHtml = rowHtml.Replace("<p>", string.Empty).Replace("</p>", string.Empty);

                        TableCell answerDescriptionCell = new TableCell
                        {
                            Text = rowHtml
                        };

                        answerRow.Cells.Add(answerDescriptionCell);

                        if (surveyType != 2)
                        {
                            string bgColor = questionNum % 2 == 0 ? "#8DEEEE" : "#79CDCD";
                            TableCell answerValueCell = new TableCell
                            {
                                Text =
                                    $"<div id=\"colored\"  style='background-color:{bgColor}; height: 20px; width: {3 + proc * 2}px;'>&nbsp;</div>"
                            };

                            answerRow.Cells.Add(answerValueCell);
                        }
                        else
                        {
                            if (answer.GetInt("correct") == 1)
                            {
                                TableCell answerValueCell = new TableCell
                                {
                                    Text =
                                        $"<div id=\"colored\"  style='background-color:#00FF00; height: 20px; width: {3 + proc * 2}px;'>&nbsp;</div>"
                                };

                                answerRow.Cells.Add(answerValueCell);
                            }
                            else
                            {
                                TableCell answerValueCell = new TableCell
                                {
                                    Text =
                                        $"<div id=\"colored\"  style='background-color:#FF0000; height: 20px; width: {3 + proc * 2}px;'>&nbsp;</div>"
                                };

                                answerRow.Cells.Add(answerValueCell);
                            }
                        }
                    }

                    questionTable.Rows.Add(answerRow);
                    ++questionNum;
                }
            }
        }

        private string RemoveSpecialCharacters(string text)
        {
            return text.Replace("<p>", string.Empty).Replace("&lt;", "<");
        }

        /// <summary>
        /// The detecting anonymous survey for answers.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string DetectingAnonymousSurveyForAnsvers()
        {
            return dbMgr.GetScalarByQuery($"SELECT anonymous_survey FROM surveys_main WHERE id ='{this.alertId}'").ToString();
        }
    }
}