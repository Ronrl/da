﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="addSurvey.aspx.cs" Inherits="deskalerts.addSurvey" %>
<%@ Import Namespace="DeskAlertsDotNet.Parameters" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="DeskAlerts.Server.Utils.Helpers" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

	<head>
		<title>DeskAlerts</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css" />
		<link href="css/style9.css" rel="stylesheet" type="text/css">
		<link href="css/jquery/jquery_timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
		<link href="css/form_style.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
        <script type="text/javascript" src="jscripts/jquery.hotkeys.js"></script>
		<script type="text/javascript" src="jscripts/jquery/jquery-migrate-1.2.1.min.js"></script>
		<script type="text/JavaScript" src="jscripts/json2.js"></script>
		<script type="text/javascript" src="jscripts/preview.js?v=9.2.1.31"></script>
		<script type="text/javascript" src="functions.js"></script>
		<script type="text/javascript" src="jscripts/date.js"></script>
        <script type="text/javascript" src="jscripts/jquery.hotkeys.js"></script>
	 	
		<script type="text/javascript" src="jscripts/jquery/jquery_timepicker/jquery-ui-timepicker-addon.js"></script>
	<!--	<script type="text/javascript" src="jscripts/jquery/datepicker_fix.js"></script>
	-->
		<script type="text/javascript" src="jscripts/moment.js"></script>
		
		
	    <script type="text/javascript" src="jscripts/tiny_mce/tinymce.min.js"></script>

    
         
		<style rel="stylesheet" type="text/css">
		    .tile {
		        margin: 10px;
		        padding: 10px;
		        width: 200px;
		    }

		    #skins_tiles_dialog .tile {
		        display: inline-block;
		    }

		    #default {
		        margin-top: 0px;
		    }

		    .save_and_next_button {
		        float: right;
		    }
		</style>
		
		<script type="text/javascript">
            $(document).ready(function () {
                tinyMCE.baseURL = window.location.href.substring(0, window.location.href.lastIndexOf("/")) + "/jscripts/tiny_mce/";
                tinymce.init({
                    selector: 'textarea#title_survey',
                    menubar: false,
                    statusbar: false,
                    resize: false,
                    setup: function (ed) {
                        ed.on('change', function (e) {
                            $("#titleHidden").val(ed.getContent());
                        });
                        ed.on('init',
                            function (ed) {
                                if (window.location.href.indexOf('id=') == -1) {
                                    ed.target.editorCommands.execCommand("fontName", false, "Arial");
                                    setUpBoldFont(ed);
                                }
                            });
                    },
                    plugins: "autoresize textcolor directionality autolink",
                    toolbar: "italic underline strikethrough | fontselect fontsizeselect | forecolor | ltr rtl",
                    autoresize_bottom_margin: 0,
                    autoresize_min_height: 20,
                    autoresize_max_height: 80,
                    fontsize_formats: '8pt 10pt 11pt 12pt 14pt 16pt 18pt',
                    forced_root_block: false,
                    valid_elements: "span[*],font[*],i[*],b[*],u[*],strong[*],s[*],em[*]",
                    font_formats: 'Andale Mono=andale mono,times;Arial=arial,helvetica,sans-serif;Arial Black=arial black,avant garde;Book Antiqua=book antiqua,palatino;Calibri=calibri;Comic Sans MS=comic sans ms,sans-serif;Courier New=courier new,courier;Georgia=georgia,palatino;Helvetica=helvetica;Impact=impact,chicago;Tahoma=tahoma,arial,helvetica,sans-serif;Times New Roman=times new roman,times;Trebuchet MS=trebuchet ms,geneva;Verdana=verdana,geneva;Webdings=webdings;Wingdings=wingdings,zapf dingbats',
                    content_style: "body{font:bold 21px Arial !important;color:#000000 !important}",
                    branding: false
                });

                $(document).tooltip({
                    items: "img[title],a[title]",
                    position: {
                        my: "center bottom-20",
                        at: "center top",
                        using: function (position, feedback) {
                            $(this).css(position);
                            $("<div>")
                                .addClass("arrow")
                                .addClass(feedback.vertical)
                                .addClass(feedback.horizontal)
                                .appendTo(this);
                        }
                    }
                });

                $("#accordion").accordion(
                    {
                        collapsible: true
                    }

                );
                function htmlEscape(str) {
                    return (str || '')
                        .replace(/&/g, '&amp;')
                        .replace(/"/g, '&quot;')
                        .replace(/'/g, '&#39;')
                        .replace(/</g, '&lt;')
                        .replace(/>/g, '&gt;');
                }
                function htmlUnescape(str) {
                    return (str || '')
                        .replace(/&amp;/g, '&')
                        .replace(/&quot;/g, '"')
                        .replace(/'/g, '\'')
                        .replace(/&lt;/g, '<')
                        .replace(/&gt;/g, '>');
                }

                function isBadSkin(skinId) {

                    skinId = skinId.toLowerCase();
                    return skinId == '{c28f6977-3254-418c-b96e-bda43ce94ff4}' || skinId == '{138f9281-0c11-4c48-983e-d4ca8076748b}' || skinId == '{04f84380-b31d-4167-8b83-c6642e3d25a8}';
                }
                var skinsDialog = $("#skins_tiles_dialog").dialog({
                    position: { my: "left top", at: "left top", of: "#dialog_dock" },
                    modal: true,
                    resizable: false,
                    draggable: false,
                    width: 'auto',
                    resize: 'auto',
                    margin: 'auto',
                    bgiframe: true,
                    autoOpen: false,
                    close: function () { closeSkinsDialog() }
                });

                $('#default').click(function () {
                    skinsDialog.dialog('open');
                });


                var dupId = parseInt('<% Response.Write(Request["id"]); %>', 10);



                function closeSkinsDialog() {
                    skinsDialog.dialog('close');
                }
                function check_size_input(myfield, e, dec) {
                    var key;
                    var keychar;

                    if (window.event)
                        key = window.event.keyCode;
                    else if (e)
                        key = e.which;
                    else
                        return true;

                    keychar = String.fromCharCode(key);

                    // control keys
                    if ((key == null) || (key == 0) || (key == 8) ||
                        (key == 9) || (key == 13) || (key == 27)) {
                        return true;
                    }
                    // numbers
                    else if (myfield.value.length > 6) {
                        return false;
                    }
                    else if ((("0123456789").indexOf(keychar) > -1)) {
                        return true;
                    }
                    // decimal point jump
                    else if (dec && (keychar == ".")) {
                        myfield.form.elements[dec].blur();
                        return false;
                    }
                    else
                        return false;
                }

                function check_message() {

                    var elem = document.getElementById('size2');
                    var msg = document.getElementById('size_message');
                    var al_width = document.getElementById('al_width');
                    var al_height = document.getElementById('al_height');
                    var resizable = document.getElementById('resizable');
                    var position_fieldset = document.getElementById('position_fieldset');

                    if (elem && al_width && al_height) {
                        if (msg) msg.style.display = elem.checked ? "none" : "";
                        al_width.disabled = elem.checked;
                        al_height.disabled = elem.checked;
                        position_fieldset.disabled = elem.checked;
                        $("#ticker_position_fieldset").disabled = elem.checked;
                        if (resizable) resizable.disabled = elem.checked;
                    }
                }

                $('.tile').hover(function () {
                    $(this).switchClass("", "tile_hovered", 200);
                }, function () {
                    $(this).switchClass("tile_hovered", "", 200);
                }).click(function () {
                    var src = $(this).find('img').eq(0).attr('src');
                    var tmp_arr = src.split('/');
                    var id = tmp_arr[1];
                    $("#skin_id").val(id);
                    var al_width = document.getElementById('al_width');
                    var al_height = document.getElementById('al_height');
                    //  alert(id);
                    if (isBadSkin(id)) {
                        // alert('bad');

                        $("#size1").prop('checked', true);
                        $("#size2").prop('checked', false);
                        $("#size1").prop('disabled', true);
                        $("#size2").prop('disabled', true);
                        $("#resizable").prop('disabled', true);

                        al_width.disabled = true;
                        al_height.disabled = true;
                        //  $("#al_width").prop('disabled', true);
                        // $("#al_height").prop('disabled', true);
                        $("input[name=position]").prop('disabled', true);
                        // check_message();
                    }
                    else {
                        //   alert('good');
                        $("#size1").prop('disabled', false);
                        $("#size2").prop('disabled', false);
                        //		                $("#al_width").removeAttr('disabled');
                        //		                $("#al_height").removeAttr('disabled');
                        al_width.disabled = false;
                        $("#resizable").prop('disabled', false);
                        al_height.disabled = false;
                        $("input[name=position]").prop('disabled', false);
                        check_message();
                    }
                    var src_tumb = $("#skin_preview_img").attr('src');
                    var src_tumb_elems = src_tumb.split('/');
                    var new_src_tumb = src_tumb_elems[0] + "/" + id + "/" + src_tumb_elems[2];
                    $("#skin_preview_img").attr('src', new_src_tumb);
                    closeSkinsDialog();
                });


                $(document)
                    .bind('keydown',
                        'right',
                        function () {

                            $('.save_and_next_button').click();
                        });


                $('.save_and_next_button').click(function () {
                    var ready = checkForm();
                    if (!ready) return;

                    setSurveyType();

                    $("#title").val(htmlEscape(tinyMCE.get("title_survey").getContent()));
                    var plain_text = $('<div/>').html(tinyMCE.get("title_survey").getContent()).text();

                    $('#plain_title').val(plain_text.replace(/"/g, '&quot;'));


                    if ($("#schedule").is(":checked")) {
                        var start = moment($("#start_date").val(), 'MM/DD/YYYY hh:mm A');
                        var end = moment($("#end_date").val(), 'MM/DD/YYYY hh:mm A');
                        // start.isBefore(moment().valueOf())
                        var now = moment();
                        now = now.format('MM/DD/YYYY hh:mm:ss');
                        start = start.format('MM/DD/YYYY hh:mm:ss');

                        var diff = moment(now).diff(moment(start));
                        // alert(diff);
                        var seconds = moment.duration(diff).asSeconds()
                        //  alert(seconds);
                        if (end.isBefore(start) || seconds > 60) {

                            alert('<% PrintVar("LNG_DATE_IN_THE_PAST"); %>');
                            return;
                        }
                    }
                    $("#survey_settings").submit();
                });

                $(".delete_button").button({});
                $('.end_questions').click(function () {

                });

                var newTinySettings = {
                    "theme_advanced_buttons1": "bold,italic,underline,|,formatselect,fontselect,fontsizeselect,|,forecolor,backcolor",
                    "theme_advanced_buttons2": "",
                    "theme_advanced_buttons3": ""
                };


                function getLifeTimeControl(lifetime, name) {

                    var minSel = "";
                    var hourSel = "";
                    var daySel = "";

                    if (lifetime != 0) {
                        if (lifetime % 1440 == 0) {
                            lifetime /= 1400;
                            daySel = " selected";
                        }
                        else if (lifetime % 60 == 0) {
                            lifetime /= 60;
                            hourSel = " selected";
                        }
                        else {
                            minSel = " selected";
                        }
                    }
                    var textBox = "<input type='text' id='" + name + "' name='" + name + "' style='width:50px' value='" + lifetime + "'onkeypress='return check_size_input(this, event);'>";
                    var comboBox = "<select id='" + name + "_factor' name='" + name + "_factor'>" +
                        "<option value='1'" + minSel + ">" + '<% Response.Write(vars["LNG_MINUTES"]);%>' + "</option>" +
                        "<option value='60'" + hourSel + ">" + '<% Response.Write(vars["LNG_HOURS"]);%>' + "</option>" +
                        "<option value='1440'" + daySel + ">" + '<% Response.Write(vars["LNG_DAYS"]);%>' + "</option>" +
                        "</select>";


                    return (textBox + comboBox);

                }
                /*initTinyMCE("en", "exact", "title_survey", null, null, newTinySettings);*/

                $('#right_wrong').click(function () {
                    if ($(this).prop("checked")) $('#view_res').prop("checked", false);
                });

                $('#view_res').click(function () {
                    if ($(this).prop("checked")) $('#right_wrong').prop("checked", false);
                });


                $('#lifetimemode').click(function () {
                    if ($(this).prop("checked")) {
                        $('#schedule').prop("checked", false);
                        $('#lifetime').prop('disabled', false);
                        $('#lifetime_factor').prop('disabled', false);
                    }
                    else {
                        $('#lifetime').prop('disabled', true);
                        $('#lifetime_factor').prop('disabled', 'disabled');
                    }
                    checkScheduleFields();
                });

                $('#schedule').click(function () {
                    if ($(this).prop("checked")) {
                        $('#lifetimemode').prop("checked", false);
                        $('#lifetime').prop('disabled', true);
                        $('#lifetime_factor').prop('disabled', 'disabled');
                    }
                    else {
                    }

                    checkScheduleFields();
                });


                function checkScheduleFields() {

                    if ($("#schedule").prop("checked")) {
                        $("#start_date").prop('disabled', false);
                        $("#end_date").prop('disabled', false);
                    }
                    else {
                        $("#start_date").prop('disabled', true);
                        $("#end_date").prop('disabled', true);
                    }
                }


                var settingsDateFormat = "<% = DateTimeConverter.JsDateFormat %>";
                var settingsTimeFormat = "<% = DateTimeConverter.JsTimeFormat %>";

                $(".date_time_param_input").datetimepicker({ dateFormat: settingsDateFormat, timeFormat: settingsTimeFormat });

                setTimeout(function () {


                    if (isNaN(dupId))
                        return;
                    $.get("../get_survey_data.asp?id=" + dupId, function (data) {

                        var surveyData = JSON.parse(data);

                        $("#stype").val(surveyData.type);

                        tinyMCE.get("title_survey").setContent(htmlUnescape(surveyData.name));
                        var skinId = surveyData.skin_id;

                        $("#skin_preview_img").attr("src", "skins/" + skinId + "/thumbnail.png");
                        $("#skin_id").val(skinId);

                        if (surveyData.stype == 2) {
                            $("#right_wrong").prop('checked', true);
                        }
                        else if (surveyData.stype == 3) {
                            $("#view_res").prop('checked', true);
                        }

                        if (surveyData.schedule == 1) {
                            $("#lifetimemode").prop('checked', false);
                            $("#lifetime").prop('disabled', true);
                            $("#lifetime_factor").prop('disabled', true);
                            $("#schedule").prop('checked', true);
                            $("#start_date").prop('disabled', false);
                            $("#end_date").prop('disabled', false);


                            var startDate = moment(surveyData.start_date, 'DD/MM/yyyy HH:mm:ss').format('<% =fullDateFormat.Replace("yyyy", "YYYY").Replace("dd", "DD").Replace("tt", "A")  %>');
                            var endDate = moment(surveyData.end_date, 'DD/MM/yyyy HH:mm:ss').format('<% =fullDateFormat.Replace("yyyy", "YYYY").Replace("dd", "DD").Replace("tt", "A")   %>');

                            $("#start_date").val(startDate);
                            $("#end_date").val(endDate);

                        }
                        else if (surveyData.lifetime.length > 0) {

                            var lifetime = parseInt(surveyData.lifetime);
                            var index = 0
                            if (lifetime != 0) {
                                if (lifetime % 1440 == 0) {
                                    lifetime /= 1400;
                                    index = 2;

                                }
                                else if (lifetime % 60 == 0) {
                                    lifetime /= 60;
                                    index = 1;

                                }
                                else {
                                    index = 0;
                                }
                            }

                            $('select>option:eq(' + index + ')').attr('selected', true);
                            $('#lifetime').val(parseInt(lifetime));
                            $("#lifetimemode").prop('checked', true);
                            $("#schedule").prop('checked', false);
                            var now = moment().format('<% =fullDateFormat.Replace("yyyy", "YYYY").Replace("dd", "DD").Replace("tt", "A")   %>')
                            $("#start_date").val(now);
                            $("#end_date").val(now);
                            $("#start_date").prop('disabled', true);
                            $("#end_date").prop('disabled', true);
                        }
                        else {
                            $("#lifetimemode").prop('checked', false);
                            $("#lifetime").prop('disabled', true);
                            $("#lifetime_factor").prop('disabled', true);
                            $("#schedule").prop('checked', false);

                            var now = moment().format('<% =fullDateFormat.Replace("yyyy", "YYYY").Replace("dd", "DD").Replace("tt", "A")   %>')
                            $("#start_date").val(now);
                            $("#end_date").val(now);
                            $("#start_date").prop('disabled', true);
                            $("#end_date").prop('disabled', true);
                        }



                        $("#al_height").val(surveyData.alert_height);
                        $("#al_width").val(surveyData.alert_width);


                        if (surveyData.fullscreen == 1) {

                            $("#size2").prop('checked', true);
                            check_message();
                        }
                        else {

                            $("#size1").prop('checked', true);
                            check_message();


                            var value = surveyData.fullscreen;

                            if (value == 0) value = 6;

                            $("input[name=position][value=" + value + "]").prop('checked', true);
                        }

                        if (surveyData.resizable != "False") {

                            $("#resizable").prop('checked', true);
                            $("#resizable").val(1);
                        }


                        $("#cant_close").prop('checked', surveyData.cant_close == 1);

                        $("#survey_data").val(JSON.stringify(surveyData.questions));
                    });
                }, 500);


            });
            function checkForm() {
                var result = true;
                var al_value = tinyMCE.get("title_survey").getContent();
                var plain_text = $('<div/>').html(tinyMCE.get("title_survey").getContent()).text();
                if (plain_text.length < 3) {
                    result = false;
                    alert("Title must contain more than 3 symbols");
                    return result;
                }
                return result;

            }
            function check_value() {
                if ($("#al_width").length > 0) {
                    var al_width = document.getElementById('al_width').value;
                    var al_height = document.getElementById('al_height').value;
                    var size_message = document.getElementById('size_message');
                    var fullscreen = document.getElementById('size2').checked;
                    if ((al_width > 1024 && al_height > 1080) & !fullscreen) {
                        size_message.innerHTML = '<font color="red"><% Response.Write(vars["LNG_RESOLUTION_DESC1"]); %></font>';
                        return;
                    }

                    if ((al_width < 340 || al_height < 290) & !fullscreen) {
                        size_message.innerHTML = '<font color="red"><% Response.Write(vars["LNG_RESOLUTION_DESC2"]); %>. <A href="#" onclick="set_default_size();"><% Response.Write(vars["LNG_CLICK_HERE"]); %></a> <% Response.Write(vars["LNG_RESOLUTION_DESC3"]); %>.</font>';
                        return;
                    }
                    size_message.innerHTML = "";
                }
            }


            function check_message() {

                var elem = document.getElementById('size2');
                var msg = document.getElementById('size_message');
                var al_width = document.getElementById('al_width');
                var al_height = document.getElementById('al_height');
                var resizable = document.getElementById('resizable');
                var position_fieldset = document.getElementById('position_fieldset');
                if (elem && al_width && al_height) {
                    if (msg) msg.style.display = elem.checked ? "none" : "";
                    al_width.disabled = elem.checked;
                    al_height.disabled = elem.checked;
                    position_fieldset.disabled = elem.checked;
                    $("#ticker_position_fieldset").disabled = elem.checked;
                    if (resizable) resizable.disabled = elem.checked;
                }
            }
            function check_scheduled() {
                var disabled = !document.getElementById('schedule').checked;

                $("#start_date").prop('disabled', disabled);
                $("#end_date").prop('disabled', disabled);

            }
            function set_default_size() {
                document.getElementById('al_width').value = 340;
                document.getElementById('al_height').value = 290;
                document.getElementById('size_message').innerHTML = "";
            }

	</script>
	</head>

	

	<body style="margin:0" class="body">
		<div id="skins_tiles_dialog" title="<asp:Literal ID="LNG_SELECT_SKIN" runat="server" Text="Label"></asp:Literal>" style='display: none'>
			<br />
			<div style="display: block; text-align: left; color: #696969">
				<input type="checkbox" id="skins_checkbox" /><label for="skins_checkbox"> <asp:Literal ID="LNG_DONT_SHOW_AGAIN" runat="server" Text="Label"></asp:Literal></label>
			</div>        
			<p>  
				<asp:Literal ID="SKINS_CONTENT" runat="server" Text="Label"></asp:Literal>                        
			</p>
		</div>
		<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
			<tr>
				<td>
					<table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
						<tr>
							<td width="100%" height="31" class="main_table_title">
								<img src="images/menu_icons/survey_20.png" alt="" width="20" height="20" border="0" style="padding-left:7px">
								<a href="addSurvey.aspx" class="header_title" style="position:absolute;top:15px">
                                    <asp:Literal ID="LNG_SURVEY_QUIZZES_POLLS" runat="server" Text="Label"></asp:Literal>
                                </a>
							</td>
						</tr>
						<tr>
							<td height="100%" valign="top" class="main_table_body">
								<div style="margin:auto;width:1000px;padding:0px 15px" id="parent_for_confirm">
									<form id="survey_settings" style="width: inherit;" method="POST" action="questions.aspx">
									<div style="display:table-cell;padding-top:20px;width:inherit">
									 <br/>
									<a class="save_and_next_button ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-secondary" id="main_buttons_next_bottom" role="button" aria-disabled="false"><span class="ui-button-text"><% PrintVar("LNG_NEXT"); %></span><span class="ui-button-icon-secondary ui-icon ui-icon-carat-1-e"></span></a>
                                    <div style="clear:both;height:15px">&nbsp;</div>
										    <div style="display:inline-block;vertical-align:top;float:left">
												<p class="work_header" style="font-size:17px;margin-top:0px;margin-bottom:0px;margin-left:0px;display:inline-block;"><%PrintVar("LNG_TITLE"); %>:</p>
											    <textarea id="title_survey" cols="90"></textarea>
											    <input type="hidden" id="title" name="title" val="" />
											    <input type="hidden" id="plain_title" name="plain_title" val="" />
											    <input type="hidden" id="survey_data" name="survey_data" />
										    </div>

										<div class="tile" id="default"  style="display:inline-block;vertical-align:top;float:right;margin-right:0px;">
											<div class="hints" style="font-size: 12px; text-align: center; padding-bottom: 2px">
												<asp:Literal ID="LNG_CLICK_TO_CHANGE_SKIN" runat="server" Text="Label"></asp:Literal></div>
											<div style="font-size: 1px">
												<img id="skin_preview_img" src="skins/default/thumbnail.png">
											</div>
											<input type="hidden" value="default" runat="server" name="skin_id" id="skin_id" />
											<input type="hidden" value="-1" runat="server" name="campaign_id" id="campaign_id" />
												
										</div>
									</div>
                                    <div id="accordion" style="width:1000px;">
									  <h3><% PrintVar("LNG_SURVEY_OPTIONS"); %></h3>
									  <div>
										<p><input type="checkbox" value="1" name="right_wrong" id="right_wrong" /><label style="display:inline" for="right_wrong"><% PrintVar("LNG_SURVEY_RW"); %></label>
										<img border="0" style="display:inline;cursor:pointer;cursor:hand" src="images/help.png" title="Use this if you want to run series of questions and match them against predefined correct answers. Results are private (available only to the publishers)"></p>
										<p><input type="checkbox" value="1" name="view_res" id="view_res" /><label style="display:inline" for="view_res"><% PrintVar("LNG_SURVEY_VR"); %></label>
										<img border="0" style="display:inline;cursor:pointer;cursor:hand" src="images/help.png" title="Use this if you want to collect answers on several questions publicly"></p>
										<p><input type="checkbox" value="1" name="cant_close" id="cant_close" checked /><label style="display:inline" for="cant_close"><% PrintVar("LNG_SURVEY_CC"); %></label>
										<img border="0" style="display:inline;cursor:pointer;cursor:hand" src="images/help.png" title="Recipient will not be able to close survey manually"></p>
										<input type="hidden" value="1" name="stype" id="stype"/>
									  </div>
									  <h3><% PrintVar("LNG_SCHEDULE_SURVEY"); %></h3>
									  <div>
									    <p>
		                                   <input type="checkbox" id="lifetimemode" name="lifetimemode" value="1" checked onclick="check_lifetime();">
                                            <label for="lifetimemode">
                                                  <% Response.Write(vars["LNG_LIFETIME_IN"]); %></label>
 
                                                <input type='text' id='lifetime' name='lifetime' style='width:50px' value='1' onkeypress='return check_size_input(this, event);'>&nbsp;
		                                        <select id='lifetime_factor' name='lifetime_factor'>
                                                    <option value='1'><% Response.Write(vars["LNG_MINUTES"]);%></option>
                                                    <option value='60'><% Response.Write(vars["LNG_HOURS"]);%></option>
                                                    <option value='1440' selected><% Response.Write(vars["LNG_DAYS"]);%></option>
                                                </select>
                                               &nbsp;<% Response.Write(vars["LNG_TIME_EXPIRED_CANT_RECEIVED"]);%>&nbsp;<img src="images/help.png" title="<% PrintVar("LNG_LIFETIME_EXT_DESC"); %>" />						    
									    </p>
										<p>
										<input type="checkbox" id="schedule" name="schedule" value="1" onclick="check_scheduled();" />
										
										<label for="schedule"><asp:Literal ID="LNG_SCHEDULE_SURVEY" runat="server"></asp:Literal><% =resources.LNG_SCHEDULE_SURVEY%></label>&nbsp;<img src="images/help.png" title="<% PrintVar("LNG_SCHEDULE_SURVEY_DESC"); %>" />
										<br />
										<label style="padding-left:24px;" for="start_date"><% =resources.LNG_START_DATE%>:</label>
										<input class="date_time_param_input" name="start_date" id="start_date" runat="server" type="text" value="" size="20" disabled>
										<input name="start_date_time" id="start_date_time" type="hidden"><br /><br />
										<label style="padding-left:24px;" for = "end_date"><% =resources.LNG_END_DATE%>:&nbsp</label>
										<input class="date_time_param_input" name="end_date" id="end_date" runat="server" type="text" value="" size="20" disabled>
										<input name="end_date_time" id="end_date_time" type="hidden">
										</p>
									  </div>
									  <h3><% PrintVar("LNG_SURVEY_APPER_ACC"); %></h3>
									  <div>
									               <div id="size_message">
                                                 </div>
                                               <table border="0" cellspacing="0" cellpadding="0" style="width: auto;">
                                                    <tr>

                                                        <td valign="top">
                                                            
                                                            <fieldset class="reccurence_pattern" style="height: 120px">
                                                                <legend>
                                                                    Size</legend>

                                                                <div id="Div1" style="padding-left: 10px;">
                                                                    <table border="0">
                                                                        <tr>

                                                                            <td valign="middle">
                                                                                <input type="radio" id="size1" name="fullscreen" value="0" checked onclick="check_message()">
                                                                            </td>

                                                                            <td valign="middle">
                                                                                Width:
                                                                            </td>
                                                                            <td valign="middle">
                                                                                <input type="text" name="alert_width" id="al_width" size="4" value="500" onkeypress="return check_size_input(this, event);" onkeyup="check_value(this)">
                                                                            </td>
                                                                            <td valign="middle">
                                                                                px
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                &nbsp;
                                                                            </td>
                                                                            <td valign="middle">
                                                                                Height:
                                                                            </td>
                                                                            <td valign="middle">
                                                                                <input type="text" size="4" name="alert_height" onkeypress="return check_size_input(this, event);" onkeyup="check_value(this)" id="al_height" value="400">
                                                                            </td>
                                                                            <td valign="middle">
                                                                                px
                                                                            </td>
                                                                        </tr>


                                                                        <tr>
                                                                            <td>
                                                                                &nbsp;
                                                                            </td>
                                                                            <td valign="middle" colspan="3">
                                                                                <input type="checkbox" id="resizable" name="resizable" value="1">
                                                                                <label for="resizable">
                                                                                Resizable</label>
                                                                            </td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td valign="middle">
                                                                                <input type="radio" id="size2" name="fullscreen" value="1" onclick="check_message()">
                                                                            </td>
                                                                            <td valign="middle" colspan="3">
                                                                                <label for="size2">
                                                                                Fullscreen</label><br />
                                                                            </td>
                                                                        </tr>

                                                                    </table>
                                                                </div>
                                                            </fieldset>
                                                            </td>
                                                           <td>
                                                            <fieldset id="position_fieldset" class="reccurence_pattern" style="height:105px; margin-left:20px;">
                                                <legend>
                                                    Position</legend>
                                                <div id="position_div">
                                                    <table border="0" cellspacing="0" cellpadding="5" width="100%">
                                                        <tbody>
                                                            <tr>
                                                                <td valign="middle" style="text-align: center">
                                                                    <input type="radio" name="position" value="2">
                                                                </td>
                                                                <td>
                                                                </td>
                                                                <td valign="middle" style="text-align: center">
                                                                    <input type="radio" name="position" value="3">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                </td>
                                                                <td valign="middle" style="text-align: center">
                                                                    <input type="radio" name="position" value="4">
                                                                </td>
                                                                <td>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="middle" style="text-align: center">
                                                                    <input type="radio" name="position" value="5">
                                                                </td>
                                                                <td>
                                                                </td>
                                                                <td valign="middle" style="text-align: center">
                                                                    <input type="radio" name="position" value="6" checked="">
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </fieldset>
                                                        </td>
                                                    </tr>
                                                </table>
									   </div>
									  
									
                                    
									</div>
									<br/>
									<a class="save_and_next_button ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-secondary" style="margin-bottom:20px;" id="A1" role="button" aria-disabled="false"><span class="ui-button-text"><%PrintVar("LNG_NEXT"); %></span><span class="ui-button-icon-secondary ui-icon ui-icon-carat-1-e"></span></a>
									
									</form>
									
								</div>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>            
	</body>
</html>