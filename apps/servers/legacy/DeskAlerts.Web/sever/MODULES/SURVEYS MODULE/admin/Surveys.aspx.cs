using DeskAlerts.ApplicationCore.Enums;
using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.ApplicationCore.Repositories;
using DeskAlerts.ApplicationCore.Utilities;
using DeskAlerts.Server.Utils;
using DeskAlerts.Server.Utils.Helpers;
using DeskAlerts.Server.Utils.Interfactes;
using DeskAlerts.Server.Utils.Services;
using DeskAlertsDotNet.Models.Permission;
using DeskAlertsDotNet.Parameters;
using Resources;
using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Castle.MicroKernel.Lifestyle;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Extensions.Enums;
using DeskAlerts.Server.Utils.Interfaces.Services;
using DeskAlertsDotNet.Application;
using DataRow = DeskAlertsDotNet.DataBase.DataRow;

namespace DeskAlertsDotNet.Pages
{
    public partial class Surveys : DeskAlertsBaseListPage
    {
        protected override HtmlInputText SearchControl => searchTermBox;

        private static readonly IConfigurationManager ConfigurationManager = new WebConfigConfigurationManager(Config.Configuration);
        private readonly IAlertReceivedRepository _alertReceivedRepository = new PpAlertReceivedRepository(ConfigurationManager);
        private static readonly ISettingsRepository SettingsRepository= new PpSettingsRepository(ConfigurationManager);
        private readonly ISettingsService _settingsService = new SettingsService(SettingsRepository);

        private readonly ICacheInvalidationService _cacheInvalidationService;
        private readonly ISurveyRepository _surveyRepository;

        public Surveys()
        {
            using (Global.Container.BeginScope())
            {
                _cacheInvalidationService = Global.Container.Resolve<ICacheInvalidationService>();
                _surveyRepository = Global.Container.Resolve<ISurveyRepository>();
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            searchButton.Text = searchButton.Text.Replace("{Search}", resources.LNG_SEARCH);

            if (!IsPostBack)
            {
                searchTermBox.Value = Request["searchTermBox"];
            }

            var getUrlSearchTermBox = Request.QueryString["searchTermBox"] ?? string.Empty;

            headerTitle.Text = resources.LNG_SURVEY_QUIZZES_POLLS;
            typeCell.Text = resources.LNG_TYPE;
            typeCell.HorizontalAlign = HorizontalAlign.Center;
            titleCell.Text = GetSortLink(resources.LNG_TITLE, "alerts.title", Request["sortBy"]);
            scheduledCell.Text = resources.LNG_SCHEDULED;
            creationDateCell.Text = GetSortLink(resources.LNG_CREATION_DATE, "sm.create_date", Request["sortBy"]);
            answersCountCell.Text = GetSortLink(resources.LNG_VOTED_USERS, "votes_cnt", Request["sortBy"]);
            AnonymousSurveyCell.Text = resources.LNG_ANONYMOUS_SURVEY;
            Table = contentTable;
            workHeader.InnerText = !IsDraft ? resources.LNG_CURRENT_SURVEYS : resources.LNG_ARCHIVED_SURVEYS;

            senderCell.Text = resources.LNG_SENDER;
            actionsCell.Text = resources.LNG_ACTIONS;
            headerSelectAll.Visible = curUser.HasPrivilege || curUser.Policy[Policies.SURVEYS].CanDelete;

            if (!curUser.HasPrivilege && !curUser.Policy[Policies.SURVEYS].CanCreate)
            {
                addButton.Visible = false;
            }

            var isSearchTermBoxValueChanged = searchTermBox.Value != getUrlSearchTermBox;

            if (isSearchTermBoxValueChanged)
            {
                Offset = 0;
            }

            var cnt = Content.Count < (Offset + Limit) ? Content.Count : Offset + Limit;

            for (var i = Offset; i < cnt; i++)
            {
                var row = Content.GetRow(i);
                int reccurence = row.GetInt("recurrence");
                var toDate = row.GetDateTime("to_date");
                var fromDate = row.GetDateTime("from_date");
                var isScheduled = row.GetInt("schedule") == 1;
                var tableRow = new TableRow();
                if (curUser.HasPrivilege || curUser.Policy[Policies.SURVEYS].CanDelete)
                {
                    var chb = GetCheckBoxForDelete(row);
                    var checkBoxCell = new TableCell { HorizontalAlign = HorizontalAlign.Center };
                    checkBoxCell.Controls.Add(chb);
                    tableRow.Cells.Add(checkBoxCell);
                }

                selectAll.Visible = curUser.HasPrivilege || curUser.Policy[Policies.SURVEYS].CanDelete;
                bottomDelete.Visible = curUser.HasPrivilege || curUser.Policy[Policies.SURVEYS].CanDelete;
                upDelete.Visible = curUser.HasPrivilege || curUser.Policy[Policies.SURVEYS].CanDelete;

                var imageType = new Image();

                var imagePath = "images/alert_types/new/" + Content.GetString(i, "class") + ".png";
                var fullPath = Config.Data.GetString("alertsFolder") + "/admin/" + imagePath;

                if (!System.IO.File.Exists(fullPath))
                {
                    imagePath = "images/alert_types/alert.png";
                }

                imageType.Attributes["title"] = DeskAlertsUtils.GetTooltipForClass(Content.GetInt(i, "class"));

                imageType.ImageUrl = imagePath;

                var typeContentCell = new TableCell { HorizontalAlign = HorizontalAlign.Center };
                typeContentCell.Controls.Add(imageType);
                tableRow.Cells.Add(typeContentCell);

                var titleContentCell = new TableCell { Text = HttpUtility.HtmlDecode(row.GetString("name")) };
                tableRow.Cells.Add(titleContentCell);

                var createDateContentCell = new TableCell
                {
                    Text = DateTimeConverter.ToUiDateTime(row.GetDateTime("create_date")),
                    HorizontalAlign = HorizontalAlign.Center
                };

                tableRow.Cells.Add(createDateContentCell);

                var votescount = _alertReceivedRepository.GetVotesCount(long.Parse(row.GetString("sender_id")));

                var votedContentCell = new TableCell
                {
                    Text = votescount.ToString(),
                    HorizontalAlign = HorizontalAlign.Center
                };
                tableRow.Cells.Add(votedContentCell);

                var scheduleCell = new TableCell();
                if (reccurence == 1 || isScheduled)
                {
                    scheduleCell.Text = $@"{DateTimeConverter.ToUiDateTime(fromDate)} - {DateTimeConverter.ToUiDateTime(toDate)}";
                }
                else
                {
                    scheduleCell.Text = resources.LNG_NO;
                }

                scheduleCell.HorizontalAlign = HorizontalAlign.Center;
                tableRow.Cells.Add(scheduleCell);

                SetupApproving(row, tableRow);

                var senderContentCell = new TableCell
                {
                    HorizontalAlign = HorizontalAlign.Center,
                    Text = string.IsNullOrEmpty(row.GetString("sender_name")) ? resources.SENDER_WAS_DELETED : row.GetString("sender_name")
                };

                tableRow.Cells.Add(senderContentCell);

                tableRow.Cells.Add(GetTableCellForAnonymousSurvey(row.GetInt("anonymous_survey")));

                var actionsContentCell = new TableCell();

                var previewButton = GetActionButton(row.GetString("id"), "images/action_icons/preview.png", "LNG_PREVIEW", $"showPreview({row.GetString("id")})");
                previewButton.Style.Add(HtmlTextWriterStyle.MarginLeft, "10px");
                var duplicateButton = GetActionButton(
                    row.GetString("id"),
                    "images/action_icons/duplicate.png",
                    "LNG_RESEND_SURVEY",
                    delegate
                        {
                            Response.Redirect("EditSurvey.aspx?id=" + row.GetString("id"), true);
                        });
                
                var detailsButton = GetActionButton(row.GetString("id"), "images/action_icons/graph.png", "LNG_VIEW_SURVEY_RESULTS", $"openDialogUI(\"form\",\"frame\",\"SurveyAnswersStatistic.aspx?id={row.GetString("id")}\",600, 500,\"{resources.LNG_SURVEY_DETAILS}\")");

                var stopButton = GetActionButton(
                    row.GetString("id"),
                    "images/action_icons/stop.png",
                    "LNG_STOP_SURVEY",
                    delegate
                        {
                            Response.Redirect($"ChangeScheduleType.aspx?type=0&survey=1&return_page=Surveys.aspx&surveyId={row.GetString("id")}&id={row.GetString("sender_id")}", true);
                        });


                actionsCell.HorizontalAlign = HorizontalAlign.Center;
                actionsContentCell.Controls.Add(previewButton);

                if (curUser.HasPrivilege || curUser.Policy[Policies.SURVEYS].CanCreate)
                {
                    actionsContentCell.Controls.Add(duplicateButton);
                }

                actionsContentCell.Controls.Add(detailsButton);
                if (!IsDraft)
                {
                    actionsContentCell.Controls.Add(stopButton);
                }

                tableRow.Cells.Add(actionsContentCell);

                contentTable.Rows.Add(tableRow);
            }
        }

        private void SetupApproving(DataRow row, TableRow tableRow)
        {
            var shouldDisplayApprove =
                Config.Data.HasModule(Modules.APPROVE) && Settings.Content["ConfAllowApprove"].Equals("1");
            if (shouldDisplayApprove)
            {
                approveCell.Text = resources.LNG_APPROVE_STATUS;
            }
            else
            {
                approveCell.Visible = false;
            }

            if (shouldDisplayApprove)
            {
                string approveLocKey;
                if (row.IsNull("approve_status"))
                {
                    approveLocKey = "LNG_APPROVED";
                }
                else
                {
                    var approveStatus = (ApproveStatus) row.GetInt("approve_status");
                    approveLocKey = approveStatus.GetStringValue();
                }

                var approveContentCell = new TableCell();
                approveContentCell.HorizontalAlign = HorizontalAlign.Center;
                approveContentCell.Text = resources.ResourceManager.GetString(approveLocKey);
                tableRow.Cells.Add(approveContentCell);
            }
        }
        
        private static TableCell GetTableCellForAnonymousSurvey(int anonymusSurvey)
        {
            var senderContentCell = new TableCell
            {
                HorizontalAlign = HorizontalAlign.Center,
                Text = anonymusSurvey == 1 ?
                resources.LNG_YES :
                resources.LNG_NO
            };

            return senderContentCell;
        }

        #region DataProperties

        protected override string DataTable => "surveys_main";

        protected override string[] Collumns => new string[] { };

        protected override string DefaultOrderCollumn => "create_date";

        protected override string CustomQuery
        {
            get
            {
                var viewListStr = string.Empty;

                if (!curUser.HasPrivilege && !curUser.Policy.CanViewAll)
                {
                    viewListStr = string.Join(",", curUser.Policy.SendersViewList.ToArray());
                }

                var closedVal = IsDraft ? "Y" : "N";

                string query;
                if (!curUser.HasPrivilege && !curUser.Policy.CanViewAll)
                {
                    query =
                        "SELECT sm.id as id, sm.sender_id, sm.name, alerts.urgent, alerts.recurrence, alerts.lifetime,  alerts.class, " +
                        "sm.type, stype, sm.create_date, sm.schedule, alerts.approve_status as approve_status, alerts.from_date, " +
                        "alerts.to_date, COUNT(DISTINCT(CASE WHEN alerts_received.status = 1 AND alerts_received.vote = 1 THEN alerts_received.user_id END)) as votes_cnt, " +
                        "u.name as sender_name, alerts.title as plain_title, alerts.caption_id as skin_id, alerts.anonymous_survey FROM surveys_main AS sm " +
                        "INNER JOIN alerts ON alerts.id = sm.sender_id AND alerts.sender_id IN (" + viewListStr + ") " +
                        "LEFT JOIN alerts_received ON sm.sender_id = alerts_received.alert_id  " +
                        "LEFT JOIN users u on u.id = alerts.sender_id " +
                        "WHERE closed='" + closedVal + "' " +
                        " {0} " +
                        "AND alerts_received.alert_id = alerts.id " +
                        "GROUP BY sm.id, sm.name, sm.type, stype, sm.create_date, sm.schedule, " +
                        "sm.from_date, sm.to_date, u.name, alerts.caption_id, alerts.title, alerts.lifetime, alerts.class, " +
                        "alerts.recurrence, alerts.urgent, alerts.from_date, alerts.to_date, sm.sender_id, alerts.approve_status, alerts.anonymous_survey";
                }
                else
                {
                    query =
                        "SELECT sm.id as id, sm.sender_id, alerts.lifetime, alerts.urgent, alerts.recurrence, alerts.class, " +
                        "sm.name, sm.type, stype, sm.create_date, sm.schedule, alerts.approve_status, alerts.from_date, " +
                        "alerts.to_date, COUNT(DISTINCT(CASE WHEN alerts_received.status = 1 AND alerts_received.vote = 1 THEN alerts_received.user_id END)) as votes_cnt, " +
                        "u.name as sender_name, alerts.title as plain_title, alerts.caption_id as skin_id, alerts.anonymous_survey FROM surveys_main AS sm " +
                        "INNER JOIN alerts ON alerts.id = sm.sender_id " +
                        "LEFT JOIN alerts_received " +
                        "ON sm.sender_id = alerts_received.alert_id " +
                        "LEFT JOIN users u on u.id = alerts.sender_id " +
                        "WHERE closed='" + closedVal + "' " +
                        " {0} " +
                        "AND alerts_received.alert_id = alerts.id " +
                        "GROUP BY sm.id, sm.name, sm.type, stype, sm.create_date, sm.schedule, " +
                        "sm.from_date, sm.to_date, u.name, alerts.caption_id, alerts.title, alerts.lifetime, alerts.class, " +
                        "alerts.recurrence, alerts.urgent, alerts.from_date, alerts.to_date, sm.sender_id, alerts.approve_status, alerts.anonymous_survey";
                }

                query = SearchTerm.Length > 0 ? string.Format(query, " AND alerts.title LIKE '%" + SearchTerm + "%'") : string.Format(query, string.Empty);

                if (!string.IsNullOrEmpty(Request["sortBy"]))
                {
                    query += " ORDER BY " + Request["sortBy"];
                }
                else
                {
                    query += " ORDER BY " + DefaultOrderCollumn + " DESC ";
                }

                return query;
            }
        }

        protected override string CustomCountQuery
        {
            get
            {
                string viewListStr = string.Empty;

                if (!this.curUser.HasPrivilege && !this.curUser.Policy.CanViewAll)
                {
                    viewListStr = string.Join(",", this.curUser.Policy.SendersViewList.ToArray());
                }

                string closedVal = this.IsDraft ? "Y" : "N";

                string query;
                if (!this.curUser.HasPrivilege && !this.curUser.Policy.CanViewAll)
                {
                    query = "SELECT COUNT(DISTINCT surveys_main.sender_id) FROM users u, surveys_main INNER JOIN alerts ON alerts.id = surveys_main.sender_id AND alerts.sender_id IN (" + viewListStr + ") LEFT JOIN alerts_received ON alerts_received.alert_id = surveys_main.sender_id WHERE alerts.sender_id = u.id AND closed='" + closedVal + "' AND alerts_received.alert_id = alerts.id {0}";
                }
                else
                {
                    // query = "SELECT COUNT(1) FROM users u, surveys_main INNER JOIN alerts ON alerts.id = surveys_main.sender_id LEFT JOIN alerts_received ON surveys_main.sender_id=alerts_received.alert_id  WHERE alerts.sender_id = u.id AND closed='" + closedVal + "' AND alerts.campaign_id = -1 {0}";
                    // TODO inspect previows query
                    query = "SELECT COUNT(DISTINCT surveys_main.sender_id) FROM surveys_main INNER JOIN alerts ON alerts.id = surveys_main.sender_id LEFT JOIN alerts_received ON alerts_received.alert_id = surveys_main.sender_id WHERE closed='" + closedVal + "' AND alerts_received.alert_id = alerts.id {0}";
                }

                if (this.SearchTerm.Length > 0)
                {
                    query = string.Format(query, " AND alerts.title LIKE '%" + this.SearchTerm + "%'");
                }
                else
                {
                    query = string.Format(query, string.Empty);
                }

                return query;
            }
        }

        private bool IsDraft => !String.IsNullOrEmpty(Request["draft"]) && Request["draft"].Equals("1");

        /*
         *
         * If you want to add condition for data selection override property ConditionSqlString
         * Also you can implement your custom query by overriding properties CustomQuery and CustomCountQuery
         *
         */

        #endregion DataProperties

        #region Interface properties

        protected override IButtonControl[] DeleteButtons => new IButtonControl[] { upDelete, bottomDelete };

        protected override HtmlGenericControl NoRowsDiv => NoRows;

        protected override HtmlGenericControl TableDiv => mainTableDiv;

        protected override string ContentName => resources.LNG_SURVEYS;

        protected override HtmlGenericControl BottomPaging => bottomRanging;

        protected override HtmlGenericControl TopPaging => topPaging;

        #endregion Interface properties

        protected override void OnDeleteRows(string[] ids)
        {
            base.OnDeleteRows(ids);

            if (ids.Length == 0)
            {
                return;
            }

            var idsList = ids.Select(long.Parse).ToList();

            var surveyIds = _surveyRepository.GetSurveyIdBySurveyMainTableId(idsList).ToList();

            foreach (var id in surveyIds)
            {
                _cacheInvalidationService.CacheInvalidation(AlertType.SimpleSurvey, id);
            }

            dbMgr.ExecuteQuery($"DELETE FROM alerts WHERE id IN (SELECT sender_id FROM surveys_main WHERE id IN ({string.Join(",", ids)}))");
        }
    }
}