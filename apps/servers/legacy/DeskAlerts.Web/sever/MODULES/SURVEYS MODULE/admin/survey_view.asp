﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%

check_session()

%>

<%
  uid = Session("uid")
if (uid <> "") then
'show editing form
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>DeskAlerts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/style9.css" rel="stylesheet" type="text/css">
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script language="javascript" type="text/javascript">

    $(document).ready(function() {
        $(".close_button").button();
    });
    var element = [];
    window.onload = function() {
        var elem = document.getElementsByTagName("div");
        for (var i = 0; i < elem.length; i++) {
            if (elem[i].getAttribute('id') == 'colored') {
                if (i % 2 == 0)
                { elem[i].style.backgroundColor = '#8DEEEE'; }
                else
                { elem[i].style.backgroundColor = '#79CDCD'; }
            }
        }
    }


</script>
</head>
<%
id=Clng(Request("id"))
if(id <> "") then
  Set rs = Conn.Execute("SELECT surveys_main.id, surveys_main.stype, alerts.title as name FROM surveys_main INNER JOIN alerts ON alerts.id = surveys_main.sender_id WHERE surveys_main.id=" & id)
  myname=rs("name")
  myid=rs("id")
  mystype = rs("stype")
  

  'myname = Replace(Replace(myname, "<p>", "") , "</p>", "")
  
  rs.Close
end if


%>
<% if Request("wh") <> "1" then %>
<body style="margin:0" class="body">

<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	<tr>
	<td>
	<%if Request("widget") <> "1" then %>
	<table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
		<tr>
		<td width=100% height=31 class="main_table_title"><a href="surveys.asp" class="header_title"><%=LNG_SURVEY_QUIZZES_POLLS %></a></td>
		</tr>
		<tr>
		<td class="main_table_body" height="100%">
	<%end if %>
		<div style="margin:10px;">
		<span class="work_header"><%=LNG_VIEW %>:<%=myname%></span> 
		<br><br>
<% else %>
<body style="margin:0;background-color:white" class="body">
<% end if %>
<%

Set rsQuestion = Conn.Execute("SELECT id, question, question_type FROM surveys_questions WHERE survey_id=" & myid)

do while not rsQuestion.EOF

myquestion=rsQuestion("question")
questionId=rsQuestion("id")

Set RS1 = Conn.Execute("SELECT COUNT(user_id) as votes_cnt FROM surveys_answers INNER JOIN surveys_answers_stat ON surveys_answers.id=surveys_answers_stat.answer_id WHERE question_id=" & questionId)
myvotes=0
if(Not RS1.EOF) then
	myvotes=myvotes+clng(RS1("votes_cnt"))
	RS1.Close
end if

%>
<table cellpadding=4 cellspacing=4 border=0>
<tr><td><b><%=LNG_QUESTION %>:</b> </td><td><%= myquestion %></td><td width="200px">&nbsp;</td></tr>
<%
if rsQuestion("question_type")="I" or rsQuestion("question_type")="T" then
%>
<tr><td colspan="3">	<a href='#' onclick="javascript: window.open('statistics_da_surveys_view_users_custom.asp?question_id=<%=questionId %>&id=<%=id%>','', 'status=0, toolbar=0, width=350, height=350, scrollbars=1')"><%=LNG_VIEW_CUSTOM_ANSWERS %></a> </td></tr>
<%	
else
	Set RS1 = Conn.Execute("SELECT a.id, a.answer, a.correct, COUNT(s.user_id) as votes_cnt FROM surveys_answers a LEFT JOIN surveys_answers_stat s ON a.id=s.answer_id WHERE a.question_id=" & questionId & " GROUP BY a.id, a.answer, a.correct ORDER BY a.id")
	i=1
	mydiv=""
	Do While Not RS1.EOF
		if(clng(RS1("votes_cnt"))<>0) then
			proc=clng(RS1("votes_cnt"))/myvotes*100
		else
			proc=0
		end if
		
		content = RS1("answer")
		      Set regex = CreateObject("VBScript.RegExp")
					regex.Global = true
					regex.Pattern = "<img.+?src=[\""'](.+?)[\""'].*?>"
					
					Set matches = regex.Execute(content)
					
					
					for k = 0 to matches.Count - 1
					   
					    Set match = matches.Item(k)
					    
					    fullTag = match.Value					    
					    srcLink = match.SubMatches(0)
					    
					    pictureLink = " <a target='_blank' href='"& srcLink &"' >Picture</a> "
					    
					    
					    content = Replace(content, fullTag, pictureLink)
					next  
					
		if(mystype <> 2) then
		 %><tr><td><b><%=LNG_ANSWER %>&nbsp; <%= i %>:</b> </td><td><a href='#' onclick="javascript: window.open('statistics_da_surveys_view_users.asp?type=ans&answer_id=<%= RS1("id") %>','', 'status=0, toolbar=0, width=350, height=350, scrollbars=1')"><%= content & " - " & clng(proc) & "%"  %></a></td><td><div id="colored"  style='background-color: #32CD32; height: 20px; width: <%= 3+clng(proc)*2 %>px;'>&nbsp;</div></td></tr><%
		else
		    if(RS1("correct")<>0) then
		    %><tr><td><b><%=LNG_ANSWER %>&nbsp; <%= i %>:</b> </td><td><a href='#' onclick="javascript: window.open('statistics_da_surveys_view_users.asp?type=ans&answer_id=<%= RS1("id") %>','', 'status=0, toolbar=0, width=350, height=350, scrollbars=1')"><%= content & " - " & clng(proc) & "%"  %></a></td><td><div style='background-color: #32CD32; height: 20px; width: <%= 3+clng(proc)*2 %>px;'>&nbsp;</div></td></tr><%
		    else
		    %><tr><td><b><%=LNG_ANSWER %>&nbsp; <%= i %>:</b> </td><td><a href='#' onclick="javascript: window.open('statistics_da_surveys_view_users.asp?type=ans&answer_id=<%= RS1("id") %>','', 'status=0, toolbar=0, width=350, height=350, scrollbars=1')"><%= content & " - " & clng(proc) & "%"  %></a></td><td><div style='background-color: #FF0000; height: 20px; width: <%= 3+clng(proc)*2 %>px;'>&nbsp;</div></td></tr><%
		    end if
		end if
		i=i+1
		RS1.MoveNext
	Loop
	RS1.Close
end if
%>
</table>
<br>
<%
rsQuestion.MoveNext
loop
rsQuestion.Close
if(mystype = 2) then %>
<table>
    <tr>
        <td><div style="background-color: #32CD32; height: 20px; width: 20px;"></div></td><td>Correct answer</td>
    </tr>
    <tr>
        <td><div style="background-color: #FF0000; height: 20px; width: 20px;"></div></td><td>Incorrect answer</td>
    </tr>
</table>
<br>
<%
end if
%>
<% if Request("wh") <> "1" then %>
	<% if Request("widget") <> "1" then %>
		 <img src="images/inv.gif" alt="" width="1" height="100%" border="0">
	<%end if %>
		</div>
<% if Request("widget") <> "1" then %>
<br>
<!-- <center><A href="#" class="close_button" onclick="javascript: window.close()"><%=LNG_CLOSE %></a></center> -->
<br>
		</td>
		</tr>
	</table>
<% end if %>
	</td>
	</tr>
</table>

<% end if %>
<%
else
  Response.Redirect "index.asp"
end if
%>
</body>
</html><!-- #include file="db_conn_close.asp" --> 