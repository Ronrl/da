﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SurveyAnswersStatistic.aspx.cs" Inherits="DeskAlertsDotNet.Pages.SurveyAnswersStatistic" %>

<%@ Import Namespace="DeskAlertsDotNet.Parameters" %>
<%@ Import Namespace="Resources" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>DeskAlerts</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="css/style9.css" rel="stylesheet" type="text/css" />
    <link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css" />
    <style>
        .buy-panel-title {
            font-weight: bold;
            font-size: 22px;
        }

        .buy_panel {
            background-color: #B3FFA3;
            border: 1px solid #696969;
            margin: 0px 0px 0px 30px;
            padding: 10px;
            line-height: 20px;
            max-width: 500px;
            word-wrap: break-word;
        }

        .buy-panel-body {
            font-size: 12px;
        }
    </style>
    <script type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".close_button").button();
        });
    </script>
</head>
<body style="margin: 0" class="body">
    <form id="form1" runat="server">
        <div>
            <table border="0">
                <tr>
                    <td>
                        <table class="main_table">
                            <tr runat="server" id="tableHeader">
                                <td class="main_table_title"><a href="surveys.aspx" class="header_title"><%=resources.LNG_SURVEY_QUIZZES_POLLS %></a></td>
                            </tr>
                            <tr>
                                <td class="main_table_body">
                                    <div runat="server" id="workHeader" style="margin: 10px;">
                                        <span class="work_header"><%=resources.LNG_VIEW %>:<%=surveyRow.GetString("name")%></span>
                                        <br>
                                        <br>
                                    </div>
                                    <asp:Table ID="questionTable" runat="server" CellPadding="4" CellSpacing="4">
                                    </asp:Table>
                                </td>
                            </tr>
                        </table>
                        <br />
                    </td>
                </tr>
            </table>
            <div id="BuyExtendedReports" class="buy_panel" runat="server" visible="false"></div>
            <center>
                <img src="images/langs/en/close_button.gif" runat="server" id="closeButton" alt="Close" border="0"
                onclick="window.external.close();" />
            </center>
        </div>
    </form>
</body>
</html>
