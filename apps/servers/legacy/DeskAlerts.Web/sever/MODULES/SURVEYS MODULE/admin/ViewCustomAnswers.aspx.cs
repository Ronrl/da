﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Parameters;

namespace DeskAlertsDotNet.Pages
{
    using Resources;

    public partial class ViewCustomAnswers : DeskAlertsBaseListPage
    {

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            
            userNameCell.Text = resources.LNG_USERNAME;
            dateCell.Text = resources.LNG_DATE;
            answerCell.Text = resources.LNG_ANSWER;

           int cnt = this.Content.Count < (Offset + Limit) ? this.Content.Count : Offset + Limit;

            for (int i = Offset; i < cnt; i++)
            {
                TableRow row = new TableRow();
                DataRow dbRow = this.Content.GetRow(i);

                TableCell userNameContentCell = new TableCell
                {
                    Text = dbRow.GetString("name")
                };

                row.Cells.Add(userNameContentCell);

                TableCell dateContentCell = new TableCell
                {
                    Text = dbRow.GetString("date")
                };
                row.Cells.Add(dateContentCell);

                TableCell answerContentCell = new TableCell
                {
                    Text = dbRow.GetString("answer")
                };
                row.Cells.Add(answerContentCell);
                contentTable.Rows.Add(row);
            }
            

        }


        protected override string CustomQuery
        {
            get
            {
                string query =
                    "SELECT DISTINCT users.id as id, users.name as name, users.domain_id as domain_id, surveys_custom_answers_stat.date as [date], answer FROM users INNER JOIN surveys_custom_answers_stat ON users.id=surveys_custom_answers_stat.user_id WHERE role='U' AND surveys_custom_answers_stat.question_id=" +
                    Request["question_id"];

                if (SearchTerm.Length > 0)
                    query += " AND (users.name LIKE '%" + SearchTerm + "%' OR users.display_name LIKE '%" + SearchTerm + "%')";
                return query;
            }
        }

        protected override string CustomCountQuery
        {
             
            get
            {
                string query =
                    "SELECT COUNT(DISTINCT stat.user_id) as mycnt FROM surveys_custom_answers_stat as stat INNER JOIN users as users on (stat.user_id = users.id) WHERE stat.question_id=" +
                    Request["question_id"];

                if (SearchTerm.Length > 0)
                    query += " AND (users.name LIKE '%" + SearchTerm + "%' OR users.display_name LIKE '%" + SearchTerm + "%')";
                return query;
            }
        }

        #region DataProperties
        protected override string DataTable
        {
            get
            {

                //return your table name from this property
                return "";
            }
        }

        protected override string[] Collumns
        {
            get
            {

                //return array of collumn`s names of table from data table
                return new string[]{};

            }
        }

        protected override string DefaultOrderCollumn
        {
            //return collumn for default order
            get { return "id"; }
        }

        /*
         * 
         * If you want to add condition for data selection override property ConditionSqlString 
         * Also you can implement your custom query by overriding properties CustomQuery and CustomCountQuery
         *  
         */
        #endregion

        #region Interface properties

        protected override IButtonControl[] DeleteButtons
        {
            get
            {
                return new IButtonControl[] { };
            }
        }

        protected override HtmlGenericControl TopPaging
        {
            get { return topPaging; }
        }

        protected override HtmlGenericControl BottomPaging
        {
            get { return bottomPaging; }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl NoRowsDiv
        {
            get { return NoRows; }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl TableDiv
        {
            get { return mainTableDiv; }
        }

        protected override string ContentName
        {
            get
            {
                //return content name here
                return "Custom answers";
            }
        }

        #endregion
    }
}