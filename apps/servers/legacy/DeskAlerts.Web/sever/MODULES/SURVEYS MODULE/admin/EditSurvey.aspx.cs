using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services;
using System.Web.UI.WebControls;
using DeskAlerts.ApplicationCore.Enums;
using DeskAlerts.ApplicationCore.Extensions.Enums;
using DeskAlerts.Server.Utils.Interfaces;
using DeskAlerts.Server.Utils.Helpers;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Parameters;
using DeskAlertsDotNet.Utils;
using DeskAlertsDotNet.Utils.Services;
using Newtonsoft.Json;
using Resources;

namespace DeskAlertsDotNet.Pages
{
    public partial class EditSurvey : DeskAlertsBasePage
    {
        private static ISkinService _skinService;
        private static List<Guid> _curUserPolicySkinList;
        private Int32 MinAlertHeight = 290;
        private Int32 MinAlertWidth = 340;
        public EditSurvey()
        {
            _skinService = new SkinService();
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            surveyQuizzesPolls.Text = resources.LNG_SURVEYS;
            changeSkinLabel.Text = resources.LNG_CLICK_TO_CHANGE_SKIN;
            skinContent.Text = _skinService.GetSkinsPreviewTemplate(curUser.Policy.SkinList);
            _curUserPolicySkinList = curUser.Policy.SkinList;

            size1.Checked = Settings.Content["ConfPopupType"] == "W";
            size2.Checked = Settings.Content["ConfPopupType"] == "F";

            positionBottomLeft.Checked = Settings.Content["ConfAlertPosition"] == "5";
            positionBottomRight.Checked = Settings.Content["ConfAlertPosition"] == "6";
            positionCenter.Checked = Settings.Content["ConfAlertPosition"] == "4";
            positionTopLeft.Checked = Settings.Content["ConfAlertPosition"] == "2";
            positionTopRight.Checked = Settings.Content["ConfAlertPosition"] == "3";

            alertWidth.Value = Settings.Content["ConfAlertWidth"];
            alertHeight.Value = Settings.Content["ConfAlertHeight"];
            approveAlertId.Value = Request["approveAlertId"];

            if (Settings.IsTrial)
            {
                surveyResizable.Visible = false;
                surveyResizableLabel.Attributes["title"] = resources.TRIAL_MODE_DISABLED_FUNCTIONALITY;
                surveyResizableLabel.Attributes["class"] = ContentCustomClass.AdditionalDescription.GetStringValue();
            }
            LifetimeAdditionalDescription.InnerText = resources.LNG_TIME_EXPIRED_CANT_RECEIVED;

            lifetime_factor.Items.Add(new ListItem(resources.LNG_MINUTES, "1"));
            lifetime_factor.Items.Add(new ListItem(resources.LNG_HOURS, "60"));
            lifetime_factor.Items.Add(new ListItem(resources.LNG_DAYS, "1440"));
            lifetime_factor.Items[2].Selected = true;

            if (string.IsNullOrEmpty(Request["id"]))
            {
                dateStart.Value = DateTimeConverter.ToUiDateTime(DateTime.Now);
                dateEnd.Value = DateTimeConverter.ToUiDateTime(DateTime.Now.AddHours(1));
            }
            else
            {
                alertId.Value = dbMgr.GetScalarByQuery<string>("SELECT sender_id FROM surveys_main WHERE id = " + Request["id"]);

                using (var dbManager = new DBManager())
                {
                    var query = string.Format(queryGetSurvey, Request["id"]);
                    var surveyRow = dbManager.GetDataByQuery(query).First();

                    if (surveyRow.GetInt("schedule") == 1)
                    {
                        dateStart.Value = DateTimeConverter.ToUiDateTime(surveyRow.GetDateTime("from_date"));
                        dateEnd.Value = DateTimeConverter.ToUiDateTime(surveyRow.GetDateTime("to_date"));
                    }
                    else
                    {
                        dateStart.Value = DateTimeConverter.ToUiDateTime(DateTime.Now);
                        dateEnd.Value = DateTimeConverter.ToUiDateTime(DateTime.Now.AddHours(1));
                    }
                }
            }

            if (!string.IsNullOrEmpty(Request["alert_campaign"]))
            {
                campaignId.Value = Request["alert_campaign"];
            }
        }

        private static string queryGetSurvey =
            "SELECT " +
                "s.create_date as create_date, a.alert_width, a.alert_height, a.fullscreen, a.lifetime, a.resizable, s.name as name, " +
                "a.from_date as from_date, a.to_date as to_date, a.schedule, a.caption_id as skin, s.stype as type, a.autoclose as autoclose " +
            "FROM surveys_main as s " +
            "INNER JOIN alerts as a " +
            "ON a.id = s.sender_id " +
            "WHERE  s.id = {0}";

        private static string queryGetQuestions =
            "SELECT question, question_type, id " +
            "FROM surveys_questions " +
            "WHERE survey_id = {0}";

        [WebMethod]
        public static string GetSurveyData(int id)
        {

            var dbManager = new DBManager();
            var query = string.Format(queryGetSurvey, id);
            var surveyRow = dbManager.GetDataByQuery(query).First();
            var alertId = dbManager.GetScalarByQuery<string>($"SELECT sender_id FROM surveys_main WHERE id = {id}");
            var alertText = dbManager.GetScalarByQuery<string>($"SELECT alert_text FROM alerts WHERE id = {alertId}");

            var title = GetFormattedTitle(alertText);

            var surveyDict = surveyRow.ToDictionary();
            surveyDict["name"] = title;
            var questionSet = dbManager.GetDataByQuery(string.Format(queryGetQuestions, id));
            var questionsList = new List<Dictionary<string, object>>();
            foreach (var question in questionSet)
            {
                var answersSetQuery = $"SELECT id, answer as value, correct FROM surveys_answers WHERE question_id = @questionId";
                var answersSet = dbManager.GetDataByQuery(answersSetQuery, Utils.Factories.SqlParameterFactory.Create(System.Data.DbType.Int32, question.GetInt("id"), "questionId"));

                var answersList = new List<Dictionary<string, object>>();

                foreach (var answersRow in answersSet)
                {
                    answersRow.DeleteByKey("id");
                    answersList.Add(answersRow.ToDictionary());
                }

                question.DeleteByKey("id");
                var questionDict = question.ToDictionary();
                questionDict.Add("answers", answersList);
                questionsList.Add(questionDict);
            }

            surveyDict.Add("questions", questionsList);

            var fromDate = surveyRow.GetDateTime("from_date");
            var toDate = surveyRow.GetDateTime("to_date");
            surveyDict.Add("lifeTimeType", GetLifeTimeType(fromDate, toDate));
            surveyDict.Add("lifeTimeValue", GetLifeTimeValue(fromDate, toDate));

            var skinId = FillSkinsData(surveyRow, _curUserPolicySkinList);
            surveyDict.Remove("skin");
            surveyDict.Add("skin", skinId);

            var result = JsonConvert.SerializeObject(surveyDict, Formatting.None);
     
            return result;
        }

        private static string FillSkinsData(DataRow alertRow, IEnumerable<Guid> policySkinList)
        {
            var skinId = "default";
            if (!alertRow.IsNull("skin"))
            {
                skinId = alertRow.GetString("skin");
            }

            var temporarySkinId = string.Equals(skinId, "default", StringComparison.CurrentCultureIgnoreCase)
                ? skinId
                : $"{{{skinId}}}";

            skinId = _skinService.CheckSkinIdInPolicy(skinId, policySkinList);

            if (!string.Equals(skinId, temporarySkinId, StringComparison.CurrentCultureIgnoreCase))
            {
                // TODO: Show message about change skin to default. For example:
                //Response.ShowJavaScriptAlert(resources.LNG_POLICY_CHANGE_SKINS_TO_DEFAULT, true);
            }

            return skinId;
        }

        private static string GetFormattedTitle(string alert_text)
        {
            MatchCollection matches = Regex.Matches(alert_text, "<!-- html_title *= *(['\"])(.*?)\\1 *-->");

            if (matches.Count > 0)
            {
                int lastRegex = matches.Count - 1;
                if (matches[lastRegex].Groups.Count > 0)
                    return matches[lastRegex].Groups[2].Value;
            }
            return String.Empty;
        }

        private static string GetLifeTimeType(DateTime fromDate, DateTime toDate)
        {
            var timeSpan = (toDate - fromDate);
            if (timeSpan.Days > 0 && timeSpan.Hours == 0 && timeSpan.Minutes == 0)
            {
                return "dayly";
            }

            if (timeSpan.Hours > 0 && timeSpan.Minutes == 0)
            {
                return "hourly";
            }

            if (timeSpan.Minutes > 0)
            {
                return "minutes";
            }

            return string.Empty;
        }

        private static int GetLifeTimeValue(DateTime fromDate, DateTime toDate)
        {
            var timeSpan = (toDate - fromDate);
            if (timeSpan.Days > 0 && timeSpan.Hours == 0 && timeSpan.Minutes == 0)
            {
                return timeSpan.Days;
            }

            if (timeSpan.Hours > 0 && timeSpan.Minutes == 0)
            {
                return timeSpan.Hours + timeSpan.Days * 24;
            }

            if (timeSpan.Minutes > 0)
            {
                return timeSpan.Minutes + timeSpan.Hours * 60 + timeSpan.Days * 60 * 24;
            }

            return 0;
        }

        [WebMethod]
        public static string ValidDateTime(DateTime fromDate, DateTime toDate)
        {
            var status = DateValidator.ValidateDate(fromDate, toDate);
            var result = DateValidator.ParseValidateStatus(status);
            return result;
        }
    }
}