<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<%
check_session()
uid = Session("uid")
if uid <> "" then

	Set RS = Conn.Execute("SELECT sender_id FROM surveys_main WHERE id=" & Request("id"))
	if Not RS.EOF then
			Conn.Execute("DELETE FROM alerts WHERE id = " & RS("sender_id"))
	end if

	Conn.Execute("DELETE FROM surveys_answers_stat WHERE answer_id IN (SELECT id FROM surveys_answers WHERE question_id IN (SELECT id FROM surveys_questions WHERE survey_id=" & Request("id") & "))")

	Conn.Execute("DELETE FROM surveys_answers WHERE question_id IN (SELECT id FROM surveys_questions WHERE survey_id=" & Request("id") & ")")

	Conn.Execute("DELETE FROM surveys_answers WHERE survey_id=" & Request("id"))

	Conn.Execute("DELETE FROM surveys_custom_answers_stat WHERE question_id IN (SELECT id FROM surveys_questions WHERE survey_id=" & Request("id") & ")")

	Conn.Execute("DELETE FROM surveys_main WHERE id=" & Request("id"))

	Conn.Execute("DELETE FROM surveys_questions WHERE survey_id=" & Request("id"))

    if(Request("return_page") <> "") then
        Respose.Redirect Request("return_page")
    end if
	Response.Redirect "surveys_archived.asp"
else
	Response.Redirect "index.asp"
end if

%>
<!-- #include file="db_conn_close.asp" --> 