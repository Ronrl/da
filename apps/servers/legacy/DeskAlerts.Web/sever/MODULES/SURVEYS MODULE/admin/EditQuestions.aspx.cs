﻿using System;
using System.Globalization;
using System.Web;
using DeskAlerts.Server.Utils;
using DeskAlerts.Server.Utils.Helpers;

namespace DeskAlertsDotNet.Pages
{
    using Resources;

    public partial class EditQuestions : DeskAlertsBasePage
    {
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            selectQuestionTypeLabel.Text = resources.LNG_SELECT_QTYPE;
            quizzesPollsLabel.Text = resources.LNG_SURVEYS;
            clickToChangeSkinLabel.Text = resources.LNG_SELECT_QTYPE;
            saveAndAddNewQuestion.Text = resources.LNG_SAVE_AND_ADD_NEW_QUESTION;
            saveAndNext.Text = resources.LNG_SAVE_AND_NEXT;
            if (!String.IsNullOrEmpty(Request["title"]))
            {
                title.Value = Request["title"];
            }
            if (!String.IsNullOrEmpty(Request["plain_title"]))
            {
                plainTitle.Value = Request["plain_title"];
            }
            if (!String.IsNullOrEmpty(Request["skin_id"]))
            {
                skinId.Value = Request["skin_id"];
            }
            if (!String.IsNullOrEmpty(Request["stype"]))
            {
                stype.Value = Request["stype"];
            }
            if (!String.IsNullOrEmpty(Request["cant_close"]))
            {
                cantClose.Value = Request["cant_close"];
            }
            //if (!String.IsNullOrEmpty(Request["anonymous_survey"]))
            //{
            //    anonymous_survey.Value = Request["anonymous_survey"];
            //}
            if (!String.IsNullOrEmpty(Request["title_survey"]))
            {
                title.Value = Request["title_survey"];
            }
            if (!String.IsNullOrEmpty(Request["campaignId"]))
            {
                campaignId.Value = Request["campaignId"];
            }
            if (!String.IsNullOrEmpty(Request["date_start"]))
            {
                dateStart.Value = Request["date_start"];
            }
            if (!String.IsNullOrEmpty(Request["date_end"]))
            {
                dateEnd.Value = Request["date_end"];
            }
            if (!String.IsNullOrEmpty(Request["survey_data"]))
            {
                var encodePlas = Request["survey_data"].Replace("+", "%2b");
                surveyData.Value = HttpUtility.UrlDecode(encodePlas);
            }
            if (!String.IsNullOrEmpty(Request["start_date"]))
            {
                DateTime startDate = DateTime.ParseExact(Request["start_date"], "MM/dd/yyyy hh:mm tt", CultureInfo.InvariantCulture);
                dateStart.Value = startDate.ToString("dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);//Request["start_date"].ToString();
            }
            if (!String.IsNullOrEmpty(Request["end_date"]))
            {
                DateTime endDate = DateTime.ParseExact(Request["end_date"], "MM/dd/yyyy hh:mm tt", CultureInfo.InvariantCulture);
                dateEnd.Value = endDate.ToString("dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);//Request["end_date"].ToString();

            }


            if (!String.IsNullOrEmpty(Request["schedule"]))
            {
                schedule.Value = Request["schedule"];

                if (schedule.Value == "1")
                    has_lifetime.Value = "0";
            }

            if (!String.IsNullOrEmpty(Request["alertWidth"]))
            {
                alert_width.Value = Request["alertWidth"];
            }

            if (!String.IsNullOrEmpty(Request["alertHeight"]))
            {
                alert_height.Value = Request["alertHeight"];
            }

            if (!String.IsNullOrEmpty(Request["surveyResizable"]))
            {
                resizable.Value = Request["surveyResizable"];
            }

            if (!String.IsNullOrEmpty(Request["fullscreen"]))
            {
                fullscreen.Value = Request["fullscreen"];
            }


            alertId.Value = Request.GetParam("alertId", "");

            if (!String.IsNullOrEmpty(Request["position"]))
            {
                position.Value = Request["position"];
            }

            if (!String.IsNullOrEmpty(Request["lifetime"]))
            {
                int lifeTime = System.Convert.ToInt32(Request["lifetime"]);
                int lifeTimeFactor = System.Convert.ToInt32(Request["lifetime_factor"]);

                int lifeTimeInSeconds = lifeTime * lifeTimeFactor * 60;

                has_lifetime.Value = "1";
                dateStart.Value = DateTimeConverter.ToUiDateTime(DateTime.Now);
                dateEnd.Value = DateTimeConverter.ToUiDateTime(DateTime.Now.AddSeconds(lifeTimeInSeconds));

            }
            else if (schedule.Value == "0")
            {
                has_lifetime.Value = "0";
                dateStart.Value = DateTimeConverter.ToUiDateTime(new DateTime(1900, 1, 1, 0, 0, 0, DateTimeKind.Utc));
                dateEnd.Value = DateTimeConverter.ToUiDateTime(new DateTime(1900, 1, 1, 0, 0, 0, DateTimeKind.Utc));
            }

            if (!string.IsNullOrEmpty(Request["approveAlertId"]))
            {
                approveAlertId.Value = Request["approveAlertId"];
            }

        }
    }
}