﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Net;
using System.IO;
using System.Globalization;

namespace deskalerts
{

    public partial class questions : System.Web.UI.Page
    {
        public Dictionary<string, string> vars = new Dictionary<string, string>();

        protected void Page_PreInit(object sender, EventArgs e)
        {
        }

        public void PrintVar(string name)
        {
            if (!vars.ContainsKey(name))
            {
                Response.Write("[.NET DA LOCALIZATION]: BAD KEY " + name);
                return;
            }

            Response.Write(vars[name]);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            vars = GetAspVariables("LNG_QTYPE_SINGLECHOICE", "LNG_QTYPE_MULTIPLECHOICE", "LNG_QTYPE_DATAFORM", "LNG_QTYPE_COMMENTBOX", "LNG_QTYPE_ENDSURVEY", "LNG_QTYPE_NOQUESTION", "LNG_SURVEYS", "LNG_CLICK_TO_CHANGE_SKIN", "LNG_SELECT_QTYPE", "LNG_DONT_SHOW_AGAIN", "LNG_DEFAULT", "LNG_SELECT_SKIN",
                "LNG_SAVE_AND_ADD_NEW_QUESTION", "LNG_SAVE_AND_NEXT", "LNG_ADD_OPTION", "LNG_NEXT", "LNG_PREV", "LNG_QUESTION_WORD", "LNG_PREVIEW", "LNG_INTERMEDIARY_STEP_HINT", "GetDateFormat()", "GetTimeFormat()");

            LNG_SURVEY_QUIZZES_POLLS.Text = vars["LNG_SURVEYS"]; //GetAspVariable("LNG_CAMPAIGN_TITLE")  
            LNG_CLICK_TO_CHANGE_SKIN.Text = vars["LNG_SELECT_QTYPE"];
            // LNG_DONT_SHOW_AGAIN.Text = vars["LNG_DONT_SHOW_AGAIN"];
            //  LNG_DEFAULT.Text = vars["LNG_DEFAULT"];
            LNG_SELECT_QTYPE.Text = vars["LNG_SELECT_QTYPE"];
            LNG_QTYPE_SINGLECHOICE.Text = vars["LNG_QTYPE_SINGLECHOICE"];
            LNG_QTYPE_MULTIPLECHOICE.Text = vars["LNG_QTYPE_MULTIPLECHOICE"];
            LNG_QTYPE_DATAFORM.Text = vars["LNG_QTYPE_DATAFORM"];
            LNG_QTYPE_COMMENTBOX.Text = vars["LNG_QTYPE_COMMENTBOX"];
            LNG_QTYPE_ENDSURVEY.Text = vars["LNG_QTYPE_ENDSURVEY"];
            LNG_QTYPE_NOQUESTION.Text = vars["LNG_QTYPE_NOQUESTION"];
            LNG_SAVE_AND_NEXT.Text = vars["LNG_SAVE_AND_NEXT"];
            LNG_SAVE_AND_ADD_NEW_QUESTION.Text = vars["LNG_SAVE_AND_ADD_NEW_QUESTION"];
            LNG_INTERMEDIARY_STEP_HINT.Text = vars["LNG_INTERMEDIARY_STEP_HINT"];

            addSurveyLabel.Text = vars["LNG_QUESTION_WORD"] + " # ";

            string timeFormatId = vars["GetTimeFormat()"];
            string timeFormat = "HH:mm";
            if (timeFormatId == "2")
                timeFormat = "hh:mm tt";

            string fullDateFormat = vars["GetDateFormat()"] + " " + timeFormat;


            //SKINS_CONTENT.Text = FillSkins();
            if (!String.IsNullOrEmpty(Request["title"]))
            {
                title.Value = Request["title"].ToString();
            }
            if (!String.IsNullOrEmpty(Request["plain_title"]))
            {
                plain_title.Value = Request["plain_title"].ToString();
            }
            if (!String.IsNullOrEmpty(Request["skin_id"]))
            {
                skin_id.Value = Request["skin_id"].ToString();
            }
            if (!String.IsNullOrEmpty(Request["stype"]))
            {
                stype.Value = Request["stype"].ToString();
            }
            if (!String.IsNullOrEmpty(Request["cant_close"]))
            {
                cant_close.Value = Request["cant_close"].ToString();
            }
            if (!String.IsNullOrEmpty(Request["title_survey"]))
            {
                title.Value = Request["title_survey"].ToString();
            }
            if (!String.IsNullOrEmpty(Request["campaign_id"]))
            {
                campaign_id.Value = Request["campaign_id"].ToString();
            }
            if (!String.IsNullOrEmpty(Request["date_start"]))
            {
                date_start.Value = Request["date_start"].ToString();
            }
            if (!String.IsNullOrEmpty(Request["date_end"]))
            {
                date_end.Value = Request["date_end"].ToString();
            }
            if (!String.IsNullOrEmpty(Request["survey_data"]))
            {
                survey_data.Value = HttpUtility.UrlDecode(Request["survey_data"].ToString().EscapeJSQuotes());
            }
            if (!String.IsNullOrEmpty(Request["start_date"]))
            {
                DateTime startDate = DateTime.ParseExact(Request["start_date"], fullDateFormat, CultureInfo.InvariantCulture);
                date_start.Value = startDate.ToString(DateTimeFormat.JavaScript, CultureInfo.InvariantCulture);//Request["start_date"].ToString();
            }
            if (!String.IsNullOrEmpty(Request["end_date"]))
            {
                DateTime endDate = DateTime.ParseExact(Request["end_date"], fullDateFormat, CultureInfo.InvariantCulture);
                date_end.Value = endDate.ToString(DateTimeFormat.JavaScript, CultureInfo.InvariantCulture);//Request["end_date"].ToString();

            }


            if (!String.IsNullOrEmpty(Request["schedule"]))
            {
                schedule.Value = Request["schedule"].ToString();

                if (schedule.Value == "1")
                    has_lifetime.Value = "0";
            }

            if (!String.IsNullOrEmpty(Request["alert_width"]))
            {
                alert_width.Value = Request["alert_width"].ToString();
            }

            if (!String.IsNullOrEmpty(Request["alert_height"]))
            {
                alert_height.Value = Request["alert_height"].ToString();
            }

            if (!String.IsNullOrEmpty(Request["resizable"]))
            {
                resizable.Value = Request["resizable"].ToString();
            }

            if (!String.IsNullOrEmpty(Request["fullscreen"]))
            {
                fullscreen.Value = Request["fullscreen"].ToString();
            }

            if (!String.IsNullOrEmpty(Request["position"]))
            {
                position.Value = Request["position"].ToString();
            }

            if (!String.IsNullOrEmpty(Request["lifetime"]))
            {
                int lifeTime = System.Convert.ToInt32(Request["lifetime"]);
                int lifeTimeFactor = System.Convert.ToInt32(Request["lifetime_factor"]);

                int lifeTimeInSeconds = lifeTime * lifeTimeFactor * 60;

                has_lifetime.Value = "1";
                date_start.Value = DateTime.Now.ToString(DateTimeFormat.JavaScript, CultureInfo.InvariantCulture);
                date_end.Value = DateTime.Now.AddSeconds(lifeTimeInSeconds).ToString(DateTimeFormat.JavaScript, CultureInfo.InvariantCulture);

            }
            else if (schedule.Value == "0")
            {
                has_lifetime.Value = "0";
                date_start.Value = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).ToString(DateTimeFormat.JavaScript, CultureInfo.InvariantCulture);// DateTime.Now.ToString("dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                date_end.Value = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).ToString(DateTimeFormat.JavaScript, CultureInfo.InvariantCulture);
            }
        }


        private string FillSkins()
        {
            string result = "";
            string url = HttpContext.Current.Request.Url.AbsoluteUri;
            string path = url.Substring(0, url.IndexOf("questions"));
            string url_bridge = path + "/set_settings.asp?name=slink";
            WebRequest request = WebRequest.Create(url_bridge);
            WebResponse response = request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            string variable = reader.ReadToEnd();

            string[] skins = variable.Split(';');

            for (int i = 0; i < skins.Length - 1; i++)
            {
                string[] skin = skins[i].Split(',');
                result += "<div class=\"tile\" id=\"" + skin[1] + "\" style=\"\">" +
                          "<div style=\"padding-bottom:5px\"><span autofocus=\"\" style=\"margin:0px\" id=\"header_title\" class=\"header_title\">" + skin[1] + "</span></div>" +
                          "<div style=\"font-size: 1px\">" +
                          "<img id=\"skin_preview_img\" src=\"skins/{" + skin[0] + "}/thumbnail.png\">" +
                          "</div>" +
                          "</div>";
            }


            /**/

            return result;
        }

        public Dictionary<string, string> GetAspVariables(params string[] keys)
        {
            string queryString = String.Join(":", keys);

            string url = HttpContext.Current.Request.Url.AbsoluteUri;
            string path = url.Substring(0, url.IndexOf("questions"));
            string url_bridge = path + "/dotnetbridge.asp?var=" + queryString;
            WebRequest request = WebRequest.Create(url_bridge);
            WebResponse response = request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            // Response.Write(reader.ReadToEnd());
            string[] variablesStrings = reader.ReadToEnd().Split('&');


            Dictionary<string, string> result = new Dictionary<string, string>();
            foreach (string keyVal in variablesStrings)
            {

                string[] keyValArray = keyVal.Split('|');

                if (keyValArray.Length != 2)
                    continue;

                string key = keyValArray[0];
                string value = keyValArray[1];

                result.Add(key, value);
            }

            return result;


        }
        public string GetAspVariable(string variableName)
        {
            string url = HttpContext.Current.Request.Url.AbsoluteUri;
            string path = url.Substring(0, url.IndexOf("questions"));
            string url_bridge = path + "/dotnetbridge.asp?var=" + variableName;
            WebRequest request = WebRequest.Create(url_bridge);
            WebResponse response = request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            string variable = reader.ReadToEnd();
            return variable;
        }
    }
}
