﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%

check_session()

  uid = Session("uid")

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>DeskAlerts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/style9.css" rel="stylesheet" type="text/css">
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/date.js"></script>
<script language="javascript" type="text/javascript" src="functions.js"></script>
<script language="javascript" type="text/javascript">
	var serverDate;
	var settingsDateFormat = "<%=GetDateFormat()%>";
	var settingsTimeFormat = "<%=GetTimeFormat()%>";

	var responseDbFormat = "dd/MM/yyyy HH:mm:ss";
	var uiFormat = getUiDateTimeFormat(settingsDateFormat, settingsTimeFormat);

	function openDialogUI(_form, _frame, _href, _width, _height, _title) {
	    $("#dialog-" + _form).dialog('option', 'height', _height);
	    $("#dialog-" + _form).dialog('option', 'width', _width);
	    $("#dialog-" + _form).dialog('option', 'title', _title);
	    $("#dialog-" + _frame).attr("src", _href);
	    $("#dialog-" + _frame).css("height", _height);
	    $("#dialog-" + _frame).css("display", 'block');
	    if ($("#dialog-" + _frame).attr("src") != undefined) {
	        $("#dialog-" + _form).dialog('open');
	        $("#dialog-" + _form).contents().find('#table_border').css('border', 'none');
	    }
	}



	$(document).ready(function() {

	var width = $(".data_table").width();
	var pwidth = $(".paginate").width();
	if (width != pwidth) {
	    $(".paginate").width("100%");
	}

	    $("#dialog-form").dialog({
	        autoOpen: false,
	        height: 100,
	        width: 100,
	        modal: true
	    });


	    $('#dialog-frame').on('load', function() {
	        $("#dialog-frame").contents().find('.prelink_stats').on('click', function(e) {
	            e.preventDefault();
	            var link = $(this).attr('href');
	            var title = $(this).attr('title');
	            var last_id = $('.dialog_forms').size();
	            var elem = '<div id="dialog-form-' + last_id + '" class="dialog_forms" style="overflow:hidden;display:none;"><iframe id="dialog-frame-' + last_id + '" style="display:none;width:100%;height:auto;overflow:hidden;border : none;"></iframe></div>';
	            $('#secondary').append(elem);
	            $("#dialog-form-" + last_id).dialog({
	                autoOpen: false,
	                height: 100,
	                width: 100,
	                modal: true
	            });
	            openDialogUI("form-" + last_id, "frame-" + last_id, link, 350, 350, title);
	        });
	    });
	});
	
	$(function(){
		$(".delete_button").button();
	});
</script>
</head>
<script language="javascript" type="text/javascript" src="functions.js"></script>
<%

   if(Request("offset") <> "") then 
	offset = clng(Request("offset"))
   else 
	offset = 0
   end if	

   if(Request("sortby") <> "") then 
	sortby = Request("sortby")
   else 
	sortby = "create_date DESC"
   end if	

if (uid <>"") then

'counting pages to display
   if(Request("limit") <> "") then 
	limit = clng(Request("limit"))
   else 
	limit=25
   end if

	'--- rights ---
	set rights = getRights(uid, true, false, true, "surveys_val")
	gid = rights("gid")
	gviewall = rights("gviewall")
	gviewlist = rights("gviewlist")
	surveys_arr = rights("surveys_val")
	'--------------
	if(surveys_arr(2)="") then
		linkclick_str = "not"
	else
		linkclick_str = LNG_SURVEYS
	end if

if gviewall <> 1 then
	Set RS = Conn.Execute("SELECT COUNT(surveys_main.id) as mycnt FROM surveys_main INNER JOIN alerts ON alerts.id = surveys_main.sender_id AND alerts.sender_id IN ("& Join(gviewlist,",") &") WHERE closed='Y'")
else
	Set RS = Conn.Execute("SELECT COUNT(id) as mycnt FROM surveys_main WHERE closed='Y'")
end if
	cnt=0
	if(Not RS.EOF) then
		cnt=RS("mycnt")
	end if
  j=cnt/limit
%><body style="margin:0px" class="body">
 <div id="dialog-form" style="overflow: hidden; display: none;">
        <iframe id='dialog-frame' style="width: 100%; height: 100%; overflow: hidden; border: none;
            display: none"></iframe>
    </div>
    <div id="secondary" style="overflow: hidden; display: none;">
    </div>
<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	<tr>
	<td>
	<table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
		<tr>
		<td width=100% height=31 class="main_table_title"><img src="images/menu_icons/survey_20.png" alt="" width="20" height="20" border="0" style="padding-left:7px">
		<a href="surveys_archived.asp" class="header_title" style="position:absolute;top:15px"><%=LNG_SURVEY_QUIZZES_POLLS %></a></td>
		</tr>
		<tr>
		<td class="main_table_body" height="100%">
		<div style="margin:10px;">
		<span class="work_header"><%=LNG_ARCHIVED %></span><br>

<% if(cnt>0) then %>
<%
	page="surveys_archived.asp?"
	name=LNG_NAME
	Response.Write make_pages(offset, cnt, limit, page, name, sortby) 

	response.write "<br><A href='#' class='delete_button' onclick=""javascript: return LINKCLICK('"&linkclick_str&"');"">"&LNG_DELETE&"</a><br/><br/>"
	response.write "<form action='delete_objects.asp' name='delete_objs' id='delete_objs' method='post'><input type='hidden' name='objType' value='surveys'>"

%>

		<table width="100%" height="100%" class="data_table" cellspacing="0" cellpadding="3">
		<tr class="data_table_title">
		<td width="10"><input type='checkbox' name='select_all' id='select_all' onclick='javascript: select_all_objects();'></td>
		<td class="table_title" style="width:50px"><% response.write sorting(LNG_TYPE,"stype", sortby, offset, limit, page) %></td>
		<td class="table_title"><% response.write sorting(LNG_NAME,"name", sortby, offset, limit, page) %></td>
		<td class="table_title"><% response.write sorting(LNG_DATE,"create_date", sortby, offset, limit, page) %></td>
		<td class="table_title"><% response.write sorting(LNG_VOTED_USERS,"votes_cnt", sortby, offset, limit, page) %></td>
		<td class="table_title"><%=LNG_ACTIONS %></td>
				</tr>
<%

if gviewall <> 1 then
	Set RS = Conn.Execute("SELECT surveys_main.id as id, name, surveys_main.type, stype, surveys_main.create_date, COUNT(DISTINCT alerts_received.user_id) as votes_cnt  FROM surveys_main INNER JOIN alerts ON alerts.id = surveys_main.sender_id AND alerts.sender_id IN ("& Join(gviewlist,",") &") LEFT JOIN alerts_received ON surveys_main.sender_id=alerts_received.alert_id  WHERE closed='Y' group by surveys_main.id, name, surveys_main.type, stype, surveys_main.create_date ORDER BY "&sortby)
else
	Set RS = Conn.Execute("SELECT surveys_main.id as id, name, surveys_main.type, stype, surveys_main.create_date, COUNT(DISTINCT alerts_received.user_id) as votes_cnt FROM surveys_main LEFT JOIN alerts_received ON surveys_main.sender_id=alerts_received.alert_id  WHERE closed='Y' group by surveys_main.id, name, surveys_main.type, stype, surveys_main.create_date ORDER BY "&sortby)	
end if


	num=offset
	if(Not RS.EOF) then RS.Move(offset) end if
	Do While Not RS.EOF
		num=num+1
		if((offset+limit) < num) then Exit Do end if
		if(num > offset AND offset+limit >= num) then
			strObjectID=RS("id")
		%>
	<tr>
		<td><input type="checkbox" name="objects" value="<%=strObjectID%>"></td>
		<td align="center">
			<img src="images/alert_types/<%
				stype = RS("stype")
				if stype = "3" then
					Response.Write "poll"
				elseif stype = "2" then
					Response.Write "quiz"
				else
					Response.Write "survey"
				end if
			%>.png" title="<%
				if stype = "3" then
					Response.Write LNG_POLL
				elseif stype = "2" then
					Response.Write LNG_QUIZ
				else
					Response.Write LNG_SURVEY
				end if
			%>" border="0"/>
		</td>
		<td>
			<%=HtmlEncode(RS("name")) %>
		</td>
		<td> 
			<script language="javascript">
				<%= "document.write( getUiDateTime('"&RS("create_date") & "'))" %>
			</script>
		</td>
		<td>
		<%
'		Set RS1 = Conn.Execute("SELECT SUM(votes) as votes_cnt FROM surveys_answers WHERE survey_id=" & RS("id"))
		votes=0
'		if(Not RS1.EOF)then
			votes=RS("votes_cnt")
'		end if
		Response.Write votes
		%>
		</td><td align="center" width="120" nowrap="nowrap">

		<% if((gid = 0 OR (gid = 1 AND surveys_arr(3)<>"")) ) then %>
		
		<% 
		    link = ""
		    if(ConfOldSurvey = 1) then 
		        link = "survey_add.asp"    
		    else
		        link = "addSurvey.aspx"    
		    end if
		%>
		
		<a href="<% =link %>?id=<%=RS("id") %>"><img src="images/action_icons/duplicate.png" alt="<%=LNG_RESEND_SURVEY %>" title="<%=LNG_RESEND_SURVEY %>" width="16" height="16" border="0" hspace=5></a> 
		<%end if%>
		
		<% if(gid = 0 OR (gid = 1 AND surveys_arr(4)<>"")) then %>
		<a href='#' onclick="openDialogUI('form','frame','survey_view.asp?id=<%= RS("id") %>',555,450,'')">
		<img src="images/action_icons/graph.gif" alt="<%=LNG_VIEW_SURVEY_RESULTS %>" title="<%=LNG_VIEW_SURVEY_RESULTS %>" width="16" height="16" border="0" hspace=5></a> 
		<% end if%>
		
		</td></tr>
		<%
		end if
	RS.MoveNext
	Loop
	RS.Close
%>
         </table>
<%
	response.write "</form><br/><A href='#' class='delete_button' onclick=""javascript: return LINKCLICK('"&linkclick_str&"');"">"&LNG_DELETE&"</a><br><br>"

	Response.Write make_pages(offset, cnt, limit, page, name, sortby) 
%>

<% 
else

Response.Write "<center><b>"& LNG_THERE_ARE_NO_SURVEYS &"</b></center>"

end if %>
		 <img src="images/inv.gif" alt="" width="1" height="100%" border="0">
		</div>
		</td>
		</tr>
	</table>
	</td>
	</tr>
</table>
<%
else
  Response.Redirect "index.asp"

end if
%>
</body>
</html><!-- #include file="db_conn_close.asp" --> 