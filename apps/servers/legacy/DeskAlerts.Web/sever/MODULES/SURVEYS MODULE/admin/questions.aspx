﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="questions.aspx.cs" Inherits="deskalerts.questions" %>
            
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

	<head>
		<title>DeskAlerts</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css" />
		<link href="css/style9.css" rel="stylesheet" type="text/css">
		<link href="css/jquery/jquery_timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css">
		<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
		<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
		<script language="javascript" type="text/javascript" src="jscripts/moment.js"></script>
		<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-migrate-1.2.1.min.js"></script>
		<script type="text/JavaScript" language="JavaScript" src="jscripts/json2.js"></script>
		<script language="javascript" type="text/javascript" src="jscripts/preview.js?v=9.2.1.31"></script>
		<script language="javascript" type="text/javascript" src="functions.js"></script>
		<script language="javascript" type="text/javascript" src="jscripts/date.js"></script>
	 
		<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery_timepicker/jquery-ui-timepicker-addon.js"></script>
		<script language="javascript" type="text/javascript" src="jscripts/jquery/datepicker_fix.js"></script>
		
		
	    <script language="javascript" type="text/javascript" src="jscripts/tiny_mce/tinymce.min.js"></script>
    
    
		<style rel="stylesheet" type="text/css">  
		.tile
		{
			margin: 10px; 
			padding: 10px;
			width: 200px;
		}
		
		#types_tiles_dialog .tile
		{
			display: inline-block;
		}
		#default
		{
			margin-top:0px;
		}
		 .button_nav 
		 {
			/*float:right;*/
		 }      
		 #options div
		 {
			margin-bottom:0px;
		 } 
		 #options span
		 {
			margin-right:0px;
		 }   
		 .answer_delete
		 {
			cursor:pointer;
		 }
		</style>
		
		<script language="javascript" type="text/javascript">
		    var questions = new Array();
		    var counter = 1;
		    var current_question = 1;
		    var newTinySettings = "";
		    var titleChanged = {};

		    function closePreview() {
		        $(document).unbind("click", closePreview);
		        $(window).unbind("click", closePreview);
		        $(window).unbind("scroll", closePreview);
		        $(window).unbind("resize", closePreview);
		        $("#preview_div").hide();
		    }

		    $(document).ready(function () {
				tinyMCE.baseURL = window.location.href.substring(0, window.location.href.lastIndexOf("/")) + "/jscripts/tiny_mce/";
		        $(document).tooltip({
		            items: "img[title],a[title]",
		            position: {
		                my: "center bottom-20",
		                at: "center top",
		                using: function (position, feedback) {
		                    $(this).css(position);
		                    $("<div>")
                           .addClass("arrow")
                           .addClass(feedback.vertical)
                           .addClass(feedback.horizontal)
                           .appendTo(this);
		                }
		            }
		        });
		
		        $('.next_button').hide();
		        $('.prev_button').hide();
		        $(".delete_button").button({});
		        $("#preview_div").hide();

		        showOrHideCorrectAnswers();

		        var tilesDialog = $("#types_tiles_dialog").dialog({
		            position: { my: "left top", at: "left top", of: "#dialog_dock" },
		            modal: true,
		            resizable: false,
		            draggable: false,
		            width: 'auto',
		            resize: 'auto',
		            margin: 'auto',
		            bgiframe: true,
		            autoOpen: false,
		            close: function () { closeTypeDialog() }
		        });

		        $('#dialog_tile').click(function () {

		            tilesDialog.dialog('open');
		        });

		        jQuery.fn.center = function () {
		            this.css("position", "absolute");
		            this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) +
                                                    $(window).scrollTop()) + "px");
		            this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) +
                                                    $(window).scrollLeft()) + "px");
		            return this;
		        }


		        function htmlEscape(str) {
		            return (str || '')
                        .replace(/&/g, '&amp;')
                        .replace(/"/g, '&quot;')
                        .replace(/'/g, '&#39;')
                        .replace(/</g, '&lt;')
                        .replace(/>/g, '&gt;');
		        }

		        $("#preview").click(function () {

		            if ($("#preview_div").is(":visible") == true) {
		                closePreview();
		            }
		            window.scrollTo(0, 0);
		            saveQuestion(current_question);
		            $("#preview_skin_id").val($("#skin_id").val());
		            $("#preview_data").val(encodeURI(JSON.stringify(questions)));
		            $("#preview_alert_width").val($("#alert_width").val());
		            $("#preview_alert_height").val($("#alert_height").val());
		            $("#preview_fullscreen").val($("#fullscreen").val());
		            $("#preview_div").fadeIn(1000)


		            var title = $("<textarea/>").html(decodeURIComponent($("#title").val())).text(); //Hack to decode html at js. It's ok.
		            // alert(title);
		            title = title.replace("<p>", "");
		            title = title.replace("</p>", "");
		            title = htmlEscape(title);
		            //  alert(title);
		            $("#preview_title").val(title);
		            //   var date = new Date();
		            //  var dateStr = (date.getMonth() + 1) + "/" + date.getDay() + "/" + date.getFullYear() + " " + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
		            var dateStr = moment().format("MM/DD/YYYY HH:mm:ss");
		            $("#create_date").val(dateStr);
		            $("#preview_form").submit();
		            //  $("#preview_div").draggable();


		            setTimeout(function () {
		                $(document).bind("click", closePreview);
		                $(window).bind("click", closePreview);
		                $(window).bind("scroll", closePreview);
		                $(window).bind("resize", closePreview);
		                $("#preview").unbind("click", closePreview);

		            }, 1000);

		            var position = parseInt($("#position").val());

		            $("#preview_div").css('left', '');
		            $("#preview_div").css('top', '');
		            $("#preview_div").css('bottom', '');
		            $("#preview_div").css('right', '');
		            switch (position) {

		                case 1:
		                case 2:
		                    {
		                        $("#preview_div").css('left', '0px');
		                        $("#preview_div").css('top', '0px');
		                        break;
		                    }
		                case 3:
		                    {
		                        $("#preview_div").css('right', '0px');
		                        $("#preview_div").css('top', '0px');
		                        break;
		                    }
		                case 4:
		                    {
		                        $("#preview_div").center();

		                        break;
		                    }
		                case 5:
		                    {
		                        $("#preview_div").css('left', '0px');
		                        $("#preview_div").css('bottom', '0px');
		                        break;
		                    }
		                case 6:
		                    {
		                        $("#preview_div").css('right', '0px');
		                        $("#preview_div").css('bottom', '0px');
		                        break;
		                    }
		                default:
		                    {
		                        $("#preview_div").css('right', '0px');
		                        $("#preview_div").css('bottom', '0px');
		                        break;
		                    }
		            }


		        });

		        function closeTypeDialog() {
		            tilesDialog.dialog('close');
		        }


		        $('.tile').hover(function () {
		            $(this).switchClass("", "tile_hovered", 200);
		        }, function () {
		            $(this).switchClass("tile_hovered", "", 200);
		        }).click(function () {
		            var qtype = $(this).find('img').eq(0).attr('qtype');
		            var src = $(this).find('img').eq(0).attr('src');
		            $('#type_preview_img').attr('src', src);
		            $("#type_question").val(qtype);


		            var inputs = $('#options input[type!=checkbox]');
		            var tmp_options = new Array();
		            for (i = 0; i < inputs.length; i++) {
		                var correct = 0;
		                rights = $('#options input[type=checkbox]');
		                if ($('#stype').val() == "2" && rights.eq(i).is(':checked')) correct = 1;
		                var textarea_val = tinyMCE.get("option" + (i + 1) + "").getContent();
		                tmp_options.push({ 'value': textarea_val, 'correct': correct });
		            }

		            if (inputs.length > 1)
		                showFiledType(qtype, tmp_options.length, tmp_options);
		            else
		                showFiledType(qtype);

		            closeTypeDialog();
		        });

		        newTinySettings = {
		            "theme_advanced_buttons1": "bold,italic,underline,|,formatselect,fontselect,fontsizeselect,|,forecolor,backcolor,|,image",
		            "theme_advanced_buttons2": "",
		            "theme_advanced_buttons3": "",
		            "autoresize_min_height": "20",
		            "autoresize_max_height": "80"
		        };

		        //initTinyMCE("en", "exact", "title_survey", null,null,newTinySettings);

		        iniTMC();

		        $('.add_question').click(function () {

		            if (!isValidQuestion()) return false;
		            if (current_question == counter) {
		                saveQuestion(0);
		            }
		            else {
		                saveQuestion(current_question);
		            }
		            counter++;
		            current_question = counter;
		            clearQuestion();
		            $('.prev_button').show();
		        });

		        $('.next_button').click(function () {
		            moveNext();
		        });

		        $('.prev_button').click(function () {
		            movePrev();
		        });

		        $('.end_questions').click(function () {
		            if (!isValidQuestion()) return false;
		            saveQuestion(current_question);
		            $('#survey_data').val(encodeURI(JSON.stringify(questions)));
		            //alert(JSON.stringify(questions));
		            //return;
		            //destroyTiny(); Why you want destroy little mce? 
		            //tinyMCE.get('title_survey').destroy(true); Same question.
		            //document.forms["main_form"].submit();
					
		            $('#main_form').submit();
		        });



		        setTimeout(function () {

		            var surveyData = $("#survey_data").val();
		            if (surveyData.length > 0) {
		                questions = JSON.parse(surveyData);
		                counter = questions.length;
		                showQuestion(1);

		            }
		        }, 500);

		    });

		    function showOrHideCorrectAnswers() {
		        if ($('#stype').val() == 2)
		            $('.correct').show();
		        else
		            $('.correct').hide();
		    }

		    function iniTMC() {		
				tinyMCE.baseURL = window.location.href.substring(0, window.location.href.lastIndexOf("/")) + "/jscripts/tiny_mce/";
				tinymce.init({
					selector: "textarea",
					plugins: "autoresize moxiemanager textcolor link code emoticons insertdatetime fullscreen imagetools directionality table autolink",
					toolbar: "bold italic underline strikethrough | fontselect fontsizeselect | forecolor backcolor | link unlink | insertfile code | ltr rtl",
					menubar: false,
                    statusbar: false,
					setup: function (ed) {
					    ed.on('init',
                            function (ed) {
                                if (window.location.href.indexOf('id=') == -1) {
                                    ed.target.editorCommands.execCommand("fontName", false, "Arial");
                                }
                            });
					},
					autoresize_bottom_margin : 0,
					autoresize_min_height : 20,
					autoresize_max_height : 80,
					fontsize_formats: '8pt 10pt 11pt 12pt 14pt 16pt 18pt',
					moxiemanager_image_settings : { 	
						view : 'thumbs', 
						extensions : 'jpg,png,gif'
					},
					moxiemanager_video_settings : { 	
						view : 'thumbs', 
						extensions : 'mp3,wav,ogg'
					},
					relative_urls: false,
					remove_script_host: false,
					height: 100,
					content_style:  "body{color:#000000 !important;font-family:Arial;}",
                    document_base_url: "images/upload",
					font_formats: 'Andale Mono=andale mono,times;Arial=arial,helvetica,sans-serif;Arial Black=arial black,avant garde;Book Antiqua=book antiqua,palatino;Calibri=calibri;Comic Sans MS=comic sans ms,sans-serif;Courier New=courier new,courier;Georgia=georgia,palatino;Helvetica=helvetica;Impact=impact,chicago;Tahoma=tahoma,arial,helvetica,sans-serif;Times New Roman=times new roman,times;Trebuchet MS=trebuchet ms,geneva;Verdana=verdana,geneva;Webdings=webdings;Wingdings=wingdings,zapf dingbats',
                    branding: false
				});
		    }

		    function movePrev() {
		        if (!isValidQuestion()) return false;
		        saveQuestion(current_question);
		        current_question--;
		        showQuestion(current_question);
		    }

		    function moveNext() {
		        if (!isValidQuestion()) return false;
		        saveQuestion(current_question);
		        current_question++;
		        showQuestion(current_question);
		    }

		    function isValidQuestion() {
		        var result = false;
		        var opts = $('#options textarea');
		        var title = tinyMCE.get("title_survey").getContent();

		        if (title.length < 3) {
		            alert('You should complete title! Title should contain more than 3 symbols');
		            return result;
		        }
		        for (i = 0; i < opts.length; i++) {
		            if (tinyMCE.get("option" + (i + 1) + "").getContent().length < 1) {
		                alert('You should complete option #' + (i + 1));
		                return result;
		            }
		        }

		        result = true;

		        return result;
		    }

		    function showFiledType(qtype, count, values) {

		        var addButton = $("#add_option");

		        //clearQuestion();
		        if (typeof count === 'undefined') {
		            tinyMCE.get("title_survey").setContent('');
		            switch (qtype) {
		                case 'M':
		                    $(addButton).show();
		                    showRadio();
		                    break;
		                case 'N':
		                    $(addButton).show();
		                    showCheckbox();
		                    break;
		                case 'T':
		                    $(addButton).show();
		                    showTextbox();
		                    break;
		                case 'I':
		                    $(addButton).hide();
		                    showComment();
		                    break;
		                case 'C':
		                    $(addButton).hide();
		                    showEnd();
		                    break;
		                case 'S':
		                    $(addButton).hide();
		                    showComment();
		                    break;
		            }
		        }
		        else {
		            switch (qtype) {
		                case 'M':
		                    $(addButton).show();
		                    showRadio(count, values);
		                    break;
		                case 'N':
		                    $(addButton).show();
		                    showCheckbox(count, values);
		                    break;
		                case 'T':
		                    $(addButton).show();
		                    showTextbox(count, values);
		                    break;
		                case 'I':
		                    $(addButton).hide();
		                    showComment();
		                    break;
		                case 'C':
		                    $(addButton).hide();
		                    showEnd(values);
		                    break;
		                case 'S':
		                    $(addButton).hide();
		                    showComment();
		                    break;
		            }
		        }

		    }

		    function clearQuestion() {
		        $('#question_counter').text(counter);
		        $('#type_question').val('M');
		        var src = $('img[qtype=M]').attr('src');
		        $('#type_preview_img').attr('src', src);
		        tinyMCE.get("title_survey").setContent('');
		        showRadio();


		        $('.next_button').hide();
		        $('.prev_button').hide();
		    }

		    function showQuestion(number_question) {
		        if (!number_question > 0) return false;

		        if (number_question + 1 > counter) $('.next_button').hide();
		        else $('.next_button').show();
		        if (number_question - 1 == 0) $('.prev_button').hide();
		        else {
		            if (number_question + 1 > counter) {
		                /*$("#buttons_prev_bottom").css('margin-right', '185px');*/
		            }
		            else {
		                $("#buttons_prev_bottom").css('margin-right', '5px');
		            }
		            $('.prev_button').show();
		        }
		        var quest = questions[number_question - 1];
		        $('#question_counter').text(number_question);
		        $('#type_question').val(quest['type']);
		        var src = $('img[qtype=' + quest["type"] + ']').attr('src');
		        $('#type_preview_img').attr('src', src);
		        tinyMCE.get("title_survey").setContent(quest["title"]);

		        var count_answer = quest['options'].length;

		        showFiledType(quest['type'], count_answer, quest['options']);
		    }

		    function saveQuestion(number_question) {
		        var options = new Array();
		        var type = $("#type_question").val();
		        var id = questions.length;

		        var title = tinyMCE.get("title_survey").getContent();
		        var inputs = $('#options input[type!=checkbox]');

		        for (i = 0; i < inputs.length; i++) {
		            var correct = 0;
		            rights = $('#options input[type=checkbox]');
		            if ($('#stype').val() == "2" && rights.eq(i).is(':checked')) correct = 1;
		            var textarea_val = tinyMCE.get("option" + (i + 1) + "").getContent();
		            inputs.eq(i).val(strip_tags(textarea_val));
		            options.push({ 'value': textarea_val, 'correct': correct });
		        }


		        var current = { 'id': id, 'type': type, 'title': title, 'options': options };

		        if (number_question == 0) questions.push(current);
		        else questions[number_question - 1] = current;
		    }

		    function showRadio(count, values) {
		        count = (typeof count === 'undefined') ? 2 : count;


		        destroyTiny();

		        var content = '<div id="options" style="width:inherit">';
		        for (i = 0; i < count; i++) {
					var optionBegin;
					if(i>1){
						optionBegin = "<div id='option_" + (i+1) + "'><span>Option:</span><img id=\"krest_" + (i+1) + "\" class=\"answer_delete\" style=\"margin-left:518px;\" src=\"images/action_icons/delete.png\" name=\"answer_delete\" border=\"0\">";
					}
					else{
						optionBegin = "<div id='option_" + (i+1) + "'><span>Option:</span>"
					}
		            checked = "";
		            if ((typeof values != 'undefined' && values.length > i) && values[i].correct == 1) checked = "checked";

		            if (typeof values === 'undefined')
		                content += optionBegin + '<textarea id="option' + (i + 1) + '" rows="2" cols="80" ></textarea>' +
                                    '<input type="checkbox" id="opt_' + (i + 1) + '_correct" name="option' + (i + 1) + '_correct" class="correct" style="display:none">' +
                                                '<label for="opt_' + (i + 1) + '_correct" class="correct" style="display:none" >This option is correct</label>' +
                                    '<input type="hidden" id="opt_title_' + (i + 1) + '" name="alert_title" value="" maxlength="255" style="width: 304px"></div>';
		            else
		                content += optionBegin + '<textarea id="option' + (i + 1) + '" rows="2" cols="80" ></textarea>' +
                                    '<input type="checkbox" ' + checked + ' id="opt_' + (i + 1) + '_correct" name="option' + (i + 1) + '_correct" class="correct" style="display:none">' +
                                                '<label for="opt_' + (i + 1) + '_correct" class="correct" style="display:none" >This option is correct</label>' +
                                    '<input type="hidden" id="opt_title_' + (i + 1) + '"  name="alert_title" value="' + strip_tags(values[i].value) + '" maxlength="255" style="width: 304px"></div>';

		            $("#menu_option" + (i + 1) + "_option" + (i + 1) + "_formatselect_menu").remove();

		            $("#menu_option" + (i + 1) + "_option" + (i + 1) + "_fontselect_menu").remove();
		            $("#menu_option" + (i + 1) + "_option" + (i + 1) + "_fontsizeselect_menu").remove();
		        }
		        content += '</div>';

		        $('#question_content').empty();


		        $('#question_content').html(content);
		        iniTMC();
		        
		        if (!(typeof values === 'undefined')) {
		            for (i = 0; i < count; i++) {
		                tinyMCE.get("option" + (i + 1) + "").setContent(values[i].value);
		            }
		        }


		        showOrHideCorrectAnswers();



		    }
		    function showCheckbox(count, values) {
		        count = (typeof count === 'undefined') ? 2 : count;

		        destroyTiny();
		        var content = '<div id="options" style="width:inherit">';
		        for (i = 0; i < count; i++) {
					if(i>1){
						optionBegin = "<div id='option_" + (i+1) + "'><span>Option:</span><img id=\"krest_" + (i+1) + "\" class=\"answer_delete\" style=\"margin-left:518px;\" src=\"images/action_icons/delete.png\" name=\"answer_delete\" border=\"0\">";
					}
					else{
						optionBegin = "<div id='option_" + (i+1) + "'><span>Option:</span>"
					}
		            checked = "";
		            if ((typeof values != 'undefined' && values.length > i) && values[i].correct == 1) checked = "checked";

		            if (typeof values === 'undefined')
		                content += optionBegin + '<textarea id="option' + (i + 1) + '" rows="2" cols="80" ></textarea>' +
                                    '<input type="checkbox" id="opt_' + (i + 1) + '_correct" name="option' + (i + 1) + '_correct" class="correct" style="display:none">' +
                                                '<label for="opt_' + (i + 1) + '_correct" class="correct" style="display:none" >This option is correct</label>' +
                                    '<input type="hidden" id="opt_title_' + (i + 1) + '" name="alert_title" value="" maxlength="255" style="width: 304px"></div>';
		            else
		                content += optionBegin + '<textarea id="option' + (i + 1) + '" rows="2" cols="80" ></textarea>' +
                                    '<input type="checkbox" ' + checked + ' id="opt_' + (i + 1) + '_correct" name="option' + (i + 1) + '_correct" class="correct" style="display:none">' +
                                                '<label for="opt_' + (i + 1) + '_correct" class="correct" style="display:none" >This option is correct</label>' +
                                    '<input type="hidden" id="opt_title_' + (i + 1) + '"  name="alert_title" value="' + strip_tags(values[i].value) + '" maxlength="255" style="width: 304px"></div>';

		            $("#menu_option" + (i + 1) + "_option" + (i + 1) + "_formatselect_menu").remove();

		            $("#menu_option" + (i + 1) + "_option" + (i + 1) + "_fontselect_menu").remove();
		            $("#menu_option" + (i + 1) + "_option" + (i + 1) + "_fontsizeselect_menu").remove();
		        }
		        content += '</div>';
		        $('#question_content').empty();
		        $('#question_content').html(content);
				
		        iniTMC();
		        if (!(typeof values === 'undefined')) {
		            for (i = 0; i < count; i++) {
		                tinyMCE.get("option" + (i + 1) + "").setContent(values[i].value);
		            }
		        }
		        showOrHideCorrectAnswers();
		    }
		    function showTextbox(count, values) {
		        count = (typeof count === 'undefined') ? 2 : count;

		        destroyTiny();

		        var content = '<div id="options" style="width:inherit">';
		        for (i = 0; i < count; i++) {
					if(i>1){
						optionBegin = "<div id='option_" + (i+1) + "'><span>Option:</span><img id=\"krest_" + (i+1) + "\" class=\"answer_delete\" style=\"margin-left:518px;\" src=\"images/action_icons/delete.png\" name=\"answer_delete\" border=\"0\">";
					}
					else{
						optionBegin = "<div id='option_" + (i+1) + "'><span>Option:</span>"
					}
		            checked = "";
		            if ((typeof values != 'undefined' && values.length > i) && values[i].correct == 1) checked = "checked";

		            if (typeof values === 'undefined')
		                content += optionBegin + '<textarea id="option' + (i + 1) + '" rows="2" cols="80" ></textarea>' +
                                    '<input type="checkbox"  id="opt_' + (i + 1) + '_correct" name="option' + (i + 1) + '_correct" class="correct" style="display:none">' +
                                                '<label for="opt_' + (i + 1) + '_correct" class="correct" style="display:none" >This option is correct</label>' +
                                    '<input type="hidden" id="opt_title_' + (i + 1) + '" name="alert_title" value="" maxlength="255" style="width: 304px"></div>';
		            else
		                content += optionBegin + '<textarea id="option' + (i + 1) + '" rows="2" cols="80" ></textarea>' +
                                    '<input type="checkbox" ' + checked + ' id="opt_' + (i + 1) + '_correct" name="option' + (i + 1) + '_correct" class="correct" style="display:none">' +
                                                '<label for="opt_' + (i + 1) + '_correct" class="correct" style="display:none" >This option is correct</label>' +
                                    '<input type="hidden" id="opt_title_' + (i + 1) + '" name="alert_title" value="' + strip_tags(values[i].value) + '" maxlength="255" style="width: 304px"></div>';


		            $("#menu_option" + (i + 1) + "_option" + (i + 1) + "_formatselect_menu").remove();

		            $("#menu_option" + (i + 1) + "_option" + (i + 1) + "_fontselect_menu").remove();
		            $("#menu_option" + (i + 1) + "_option" + (i + 1) + "_fontsizeselect_menu").remove();
		        }
		        content += '</div>';
		        $('#question_content').empty();
		        $('#question_content').html(content);

		        iniTMC();
		        if (!(typeof values === 'undefined')) {
		            for (i = 0; i < count; i++) {
		                tinyMCE.get("option" + (i + 1) + "").setContent(values[i].value);
		            }
		        }
		        showOrHideCorrectAnswers();
		    }
		    function showComment() {
		        destroyTiny();
		        $('#question_content').empty();

		    }
		    function showEnd(values) {

		        //Proceed with the rest of the questions?
		        content = "";
		        destroyTiny();
		        $('#question_content').empty();
		        if (!(typeof values === 'undefined')) {
		            checked1 = "";
		            checked2 = "";
		            if (values[0].correct == 1) checked1 = "checked";
		            if (values[1].correct == 1) checked2 = "checked";
		            $('#question_content').html('<div id="options" style="width:inherit">' +
                                                '<div><span>By selecting this option user will choose to proceed with the questions:</span><textarea id="option1" rows="2" cols="80" ></textarea>' +
                                                '<input type="checkbox" ' + checked1 + ' id="opt_1_correct" name="option1_correct" class="correct" style="display:none">' +
                                                '<label for="opt_1_correct" class="correct" style="display:none" >This option is correct</label>' +
                                                '<input type="hidden" id="opt_title_1" name="alert_title" value="' + strip_tags(values[0].value) + '" maxlength="255" style="width: 304px"></div>' +
                                                '<div><span>By selecting this option user will choose to finish survey early:</span><textarea id="option2" rows="2" cols="80" ></textarea>' +
                                                '<input type="checkbox" ' + checked2 + ' id="opt_2_correct" name="option2_correct" class="correct" style="display:none">' +
                                                '<label for="opt_2_correct" class="correct" style="display:none" >This option is correct</label>' +
                                                '<input type="hidden" id="opt_title_2" name="alert_title" value="' + strip_tags(values[1].value) + '" maxlength="255" style="width: 304px"></div>' +
                                            '</div>');
		        }
		        else {
		            tinyMCE.get("title_survey").setContent('Proceed with the rest of the questions?');
		            $('#question_content').html('<div id="options" style="width:inherit">' +
                                                '<div><span>Option:</span><textarea id="option1" rows="2" cols="80"  ></textarea>' +
                                                '<input type="hidden" id="opt_title_1" name="alert_title" value="" maxlength="255" style="width: 304px"></div>' +
                                                '<div><span>Option:</span><textarea id="option2" rows="2" cols="80"  ></textarea>' +
                                                '<input type="hidden" id="opt_title_2" name="alert_title" value="" maxlength="255" style="width: 304px"></div>' +
                                            '</div>');
		        }
		        iniTMC();

		        if (!(typeof values === 'undefined')) {
		            tinyMCE.get("option1").setContent(values[0].value);
		            tinyMCE.get("option2").setContent(values[1].value);
		        }
		        else {
		            tinyMCE.get("option1").setContent('Yes');
		            tinyMCE.get("option2").setContent('No');
		        }
		        showOrHideCorrectAnswers();
		    }

		    $(document).on('click', '.answer_delete', function () {
		        var id = $(this).attr('id').replace('krest_', '');
				tinyMCE.get("option"+id).destroy();
				$("#option_"+id).remove();	
				id++;
				while (document.getElementById("option"+id)){
					tinyMCE.get("option"+id).destroy();
					$("#option"+id).attr("val", "val_"+(id-1)+"");
					$("#option"+id).attr("id", "option"+(id-1)+"");
					$("#option_"+id).attr("id", "option_"+(id-1)+"");
					$("#krest_"+id).attr("id", "krest_"+(id-1)+"");
					$("#opt_"+id+"_correct").attr("name", "option"+(id-1)+"_correct");
					$("#opt_"+id+"_correct").attr("id", "opt_"+(id-1)+"_correct");
					$("#opt_title_"+id).attr("id", "opt_title_"+(id-1)+"");
					$("label[for='opt_"+id+"_correct']").attr("for", "opt_"+(id-1)+"_correct");
					tinymce.init({
                        selector: "textarea#option" + (id - 1) + "",
                        setup: function (ed) {
                            ed.on('init',
                                function (ed) {
                                    if (window.location.href.indexOf('id=') == -1) {
                                        ed.target.editorCommands.execCommand("fontName", false, "Arial");
                                    }
                                });
                        },
						plugins: "autoresize moxiemanager textcolor link code emoticons insertdatetime fullscreen imagetools directionality table autolink",
						toolbar: "bold italic underline strikethrough | fontselect fontsizeselect | forecolor backcolor | link unlink | insertfile code | ltr rtl",
						menubar: false,
						statusbar: false,
						autoresize_bottom_margin : 0,
						autoresize_min_height : 20,
						autoresize_max_height : 80,
                        content_style:  "body{color:#000000 !important;font-family:Arial;}",
						fontsize_formats: '8pt 11pt 10pt 12pt 14pt 16pt 18pt',
						forced_root_block : false,
                        valid_elements: "span[*],font[*],i[*],b[*],u[*],strong[*],s[*],em[*]",
						font_formats: 'Andale Mono=andale mono,times;Arial=arial,helvetica,sans-serif;Arial Black=arial black,avant garde;Book Antiqua=book antiqua,palatino;Calibri=calibri;Comic Sans MS=comic sans ms,sans-serif;Courier New=courier new,courier;Georgia=georgia,palatino;Helvetica=helvetica;Impact=impact,chicago;Tahoma=tahoma,arial,helvetica,sans-serif;Times New Roman=times new roman,times;Trebuchet MS=trebuchet ms,geneva;Verdana=verdana,geneva;Webdings=webdings;Wingdings=wingdings,zapf dingbats',
                        branding: false
					});
					id++;
				}
		    });

		    $(document).on('click', "#add_option", function () {
		        var id = $('#options').children().size() + 1;
		        $('#options').append('<div id="option_' + id + '" style="vertical-align:middle"><span>Option:</span><img id="krest_' + id + '" class="answer_delete" style="margin-left:518px;" src="images/action_icons/delete.png" name="answer_delete" border="0"><textarea id="option' + id + '" val="val_' + id + '" rows="2" cols="80" style="margin-right:5px;"></textarea><input type="checkbox" id="opt_' + id + '_correct" name="option' + id + '_correct" class="correct" style=""><label for="opt_' + id + '_correct" class="correct" style="" >This option is correct</label><input type="hidden" id="opt_title_' + id + '" name="alert_title" value="" maxlength="255" style="width: 304px"></div>');
		        iniTMC();
		        showOrHideCorrectAnswers();
		    });

		    function destroyTiny() {
		        /*var opts = $('#options textarea');
		        for (i = 0; i < opts.length; i++) {
		            tinyMCE.get('option' + (i + 1) + '').remove();
		        }*/
				$("#options textarea").each(function(i){
					tinyMCE.get($(this).attr("id")).destroy();
				});
		    }

		    function strip_tags(str) {
		        return str.replace(/<\/?[^>]+>/gi, '');
		    }



	</script>
	</head>

	

	<body style="margin:0" class="body">
		<div id="types_tiles_dialog" title="<asp:Literal ID="LNG_SELECT_QTYPE" runat="server" Text="Select question type"></asp:Literal>" style='display: none'>     
			<p>  
				<div class="tile" id="" style="">
					<div style="padding-bottom:5px"><span autofocus="" style="margin:0px" id="header_title" class="header_title"><asp:Literal ID="LNG_QTYPE_SINGLECHOICE" runat="server" Text="Single choice"></asp:Literal></span></div>
					<div style="font-size: 1px">
						<img id="type_preview_img_1" qtype="M" src="images/radio.PNG">
					</div>
                </div>  
                <div class="tile" id="" style="">
					<div style="padding-bottom:5px"><span autofocus="" style="margin:0px" id="header_title" class="header_title"><asp:Literal ID="LNG_QTYPE_MULTIPLECHOICE" runat="server" Text="Multiple choice"></asp:Literal></span></div>
					<div style="font-size: 1px">
						<img id="type_preview_img_2" qtype="N" src="images/check.PNG">
					</div>
                </div>  
                <div class="tile" id="" style="">
					<div style="padding-bottom:5px"><span autofocus="" style="margin:0px" id="header_title" class="header_title"><asp:Literal ID="LNG_QTYPE_DATAFORM" runat="server" Text="Data form"></asp:Literal></span></div>
					<div style="font-size: 1px">
						<img id="type_preview_img_3" qtype="T" src="images/text.PNG">
					</div>
                </div>  
                <div class="tile" id="" style="">
					<div style="padding-bottom:5px"><span autofocus="" style="margin:0px" id="header_title" class="header_title"><asp:Literal ID="LNG_QTYPE_COMMENTBOX" runat="server" Text="Free comment box"></asp:Literal></span></div>
					<div style="font-size: 1px">
						<img id="type_preview_img_4" qtype="I" src="images/comment.PNG">
					</div>
                </div>  
                <div class="tile" id="" style="">
					<div style="padding-bottom:5px"><span autofocus="" style="margin:0px" id="header_title" class="header_title"><asp:Literal ID="LNG_QTYPE_ENDSURVEY" runat="server" Text="Survey end condition"></asp:Literal></span></div>
					<div style="font-size: 1px">
						<img id="type_preview_img_5" qtype="C" src="images/end.PNG">
					</div>
                </div> 
                 <div class="tile" id="" style="">
					<div style="padding-bottom:5px"><span autofocus="" style="margin:0px" id="header_title" class="header_title"><asp:Literal ID="LNG_QTYPE_NOQUESTION" runat="server" Text="Information step"></asp:Literal></span></div>
					<div style="font-size: 1px">
						<img id="type_preview_img_6" qtype="S" src="images/slide.png">
					</div>
                </div>                         
			</p>
		</div>
		<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
			<tr>
				<td>
					<table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
						<tr>
							<td width="100%" height="31" class="main_table_title">
								<img src="images/menu_icons/survey_20.png" alt="" width="20" height="20" border="0" style="padding-left:7px">
								<a href="addSurvey.aspx" class="header_title" style="position:absolute;top:15px">
                                    <asp:Literal ID="LNG_SURVEY_QUIZZES_POLLS" runat="server" Text="Label"></asp:Literal>
                                </a>
							</td>
						</tr>
						<tr>
							<td height="100%" valign="top" class="main_table_body">
								<div style="margin:10px auto;width:1000px;min-height:250px;position:relative" id="parent_for_confirm">
								<form id="main_form" name="main_form" action="survey_add.asp" method="POST" >
									<input type="hidden" value="0" runat="server" name="title" id="title" />
									<input type="hidden" value="0" runat="server" name="plain_title" id="plain_title" />
									<input type="hidden" value="0" runat="server" name="skin_id" id="skin_id" />
									<input type="hidden" value="0" runat="server" name="stype" id="stype" />
									<input type="hidden" value="0" runat="server" name="cant_close" id="cant_close" />
									<input type="hidden" value="-1" runat="server" name="campaign_id" id="campaign_id" />
									<input type="hidden" value="0" runat="server" name="date_start" id="date_start" />
									<input type="hidden" value="0" runat="server" name="date_end" id="date_end" />
									<input type="hidden" value="" runat="server" name="survey_data" id="survey_data" />
									<input type="hidden" value="newSurvey" runat="server" name="new" id="new" />
									<input type="hidden" value="select_users" runat="server" name="type" id="type" />
									<input type="hidden" name="schedule" id="schedule" runat="server"  value="0" />
									
									<input type="hidden" name="has_lifetime" id="has_lifetime" runat="server"  value="1" />
									
									
									<!-- appearance -->
									<input type="hidden" name="alert_width" id="alert_width" runat="server" />
									<input type="hidden" name="alert_height" id="alert_height" runat="server" />
									<input type="hidden" name="position" id="position" runat="server" />
									<input type="hidden" name="resizable" id="resizable" runat="server" />
									<input type="hidden" name="fullscreen" id="fullscreen" runat="server" />
									
									<span class="work_header"><asp:Literal ID="addSurveyLabel" runat="server" Text='Question №'></asp:Literal><span id="question_counter">1</span></span>
									<div>
									<div style="text-align:right;">
										<a class="button_nav prev_button ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" id="buttons_prev_top" role="button" aria-disabled="false"><span class="ui-button-icon-primary ui-icon ui-icon-carat-1-w"></span><span class="ui-button-text"><% PrintVar("LNG_PREV"); %></span></a>
                                    
										<a class="button_nav next_button ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-secondary" id="buttons_next_top" role="button" aria-disabled="false"><span class="ui-button-text"><% PrintVar("LNG_NEXT"); %></span><span class="ui-button-icon-secondary ui-icon ui-icon-carat-1-e"></span></a>
									</div>
                                    <br/>
										<div class="tile" id="dialog_tile"  style="display: inline-block; margin: 0px;margin-top: 0px; float:left">
											<div class="hints" style="font-size: 12px; text-align: center; padding-bottom: 2px">
												<asp:Literal ID="LNG_CLICK_TO_CHANGE_SKIN" runat="server" Text="Click here to change question type"></asp:Literal></div>
											<div style="font-size: 1px">
												<img id="type_preview_img" src="images/radio.PNG">
											</div>
											<input type="hidden" value="M" runat="server" id="type_question" />
										</div>
										<div style="float:right;display:inline-block;margin-left:10px">
											Question:<br/>
											<div style="display:inline-block;vertical-align:top;">
												<textarea id="title_survey" rows="2" cols="80" ></textarea>
												<input type="hidden" id="title_0" name="alert_title" value="" maxlength="255" style="width: 304px">
											</div>
										</div>
										<div style="float:right;position:relative" id="question_content">
										<div id="options" style="width:inherit">							
											<div><span>Option:</span><textarea id="option1" rows="2" cols="80" ></textarea>
											<input type="hidden" id="opt_title_1" name="alert_title" value="" maxlength="255" style="width: 304px">
											<input type="checkbox" id="opt_1_correct" name="option1_correct" class="correct" style="display:none">
											<label for="opt_1_correct" class="correct" style="display:none" >This option is correct</label>
											
											
											<!-- <input type="hidden" id="opt_title_1" name="alert_title" value="" maxlength="255" style="width: 304px"> --></div>
											
											<div><span>Option:</span><textarea rows="2" cols="80" id="option2" ></textarea>
											<input type="checkbox" id="opt_2_correct" name="option2_correct" class="correct" style="display:none">
											<label for="opt_2_correct" class="correct" style="display:none" >This option is correct</label>
											
											<input type="hidden" id="opt_title_2" name="alert_title" value="" maxlength="255" style="width: 304px"></div>
										</div>

									</div>
									</div>
									
									<div style="padding:10px 0px; clear:both">
										<asp:Literal ID="LNG_INTERMEDIARY_STEP_HINT" runat="server" Text="Want to add an introduction or Terms&Conditions document? Use the Intermediary Step question type to add any content between the actual questions"></asp:Literal>
									</div>
									<div style="text-align:right;clear:both">
									<a style="margin-bottom: 20px" class="button_nav prev_button ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" id="buttons_prev_bottom" role="button" aria-disabled="false"><span class="ui-button-icon-primary ui-icon ui-icon-carat-1-w"></span><span class="ui-button-text"><% PrintVar("LNG_PREV"); %></span></a>
									<a style="margin-bottom: 20px" class="button_nav next_button ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-secondary" id="buttons_next_bottom" role="button" aria-disabled="false"><span class="ui-button-text"><% PrintVar("LNG_NEXT"); %></span><span class="ui-button-icon-secondary ui-icon ui-icon-carat-1-e"></span></a>
									<br>
									
								    <a class="add_option ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-secondary" style="float:left;" id="add_option" role="button" aria-disabled="false"><span style="margin-left:5px;" class="ui-button-icon-primary ui-icon ui-icon-plusthick"></span><span style="margin-left:20px;" class="ui-button-text"><% PrintVar("LNG_ADD_OPTION"); %></span></a>
									<a class="delete_button" style="margin-bottom:20px" id="preview" role="button" aria-disabled="false"><span class="">Preview</span><span class=""></span></a>	
										<a style="margin-bottom: 20px" class="button_nav add_question ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-secondary" id="add_question" role="button" aria-disabled="false"><span class="ui-button-text"><asp:Literal ID="LNG_SAVE_AND_ADD_NEW_QUESTION" runat="server" Text="Save & add new question"></asp:Literal></span><span class="ui-button-icon-secondary ui-icon ui-icon-carat-1-e"></a>
									
									<a style="margin-bottom: 20px" class="button_nav end_questions ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-secondary" id="end_questions" role="button" aria-disabled="false"><span class="ui-button-text"><asp:Literal ID="LNG_SAVE_AND_NEXT" runat="server" Text="Save & next"></asp:Literal></span><span class="ui-button-icon-secondary ui-icon ui-icon-carat-1-e"></a>
									</div>
									<br/>									
									
								</form>

							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>   
   
	</body>
								<form action="../SurveyPreview.aspx" target="preview_frame" id="preview_form" method="post">
								 <input type="hidden" name="skin_id" id="preview_skin_id" />
								 <input type="hidden" name="data" id="preview_data" />
								 <input type="hidden" name="create_date" id="create_date" />
								 <input type="hidden" name="title" id="preview_title" />
								 <input type="hidden" name="alert_width" id="preview_alert_width" value="500"/>
								 <input type="hidden" name="alert_height" id="preview_alert_height" value="400" />
								 <input type="hidden" name="fullscreen" id="preview_fullscreen" />
								</form>
								
								
								<div id="preview_div" style="margin: 0px; padding: 0px; border: 0px; overflow: hidden; position: absolute; z-index: 9999; width: 500px; height: 400px; bottom:0; right:0; display: block; background: transparent;">
								 <iframe id="preview_frame" name="preview_frame" width="520" height="420" frameborder="0" src="../SurveyPreview.aspx">
								 </iframe>
								</div>      
</html>