﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%

check_session()

  uid = Session("uid")
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>DeskAlerts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/style9.css" rel="stylesheet" type="text/css">
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script language="javascript" type="text/javascript" src="functions.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/date.js"></script>

<script language="javascript" type="text/javascript">

	var settingsDateFormat = "<%=GetDateFormat()%>";
	var settingsTimeFormat = "<%=GetTimeFormat()%>";

	var responseDbFormat = "dd/MM/yyyy HH:mm:ss";
	var uiFormat = getUiDateTimeFormat(settingsDateFormat, settingsTimeFormat);


	function openDialogUI(_form, _frame, _href, _width, _height, _title) {
	    $("#dialog-" + _form).dialog('option', 'height', _height);
	    $("#dialog-" + _form).dialog('option', 'width', _width);
	    $("#dialog-" + _form).dialog('option', 'title', _title);
	    $("#dialog-" + _frame).attr("src", _href);
	    $("#dialog-" + _frame).css("height", _height);
	    $("#dialog-" + _frame).css("display", 'block');
	    if ($("#dialog-" + _frame).attr("src") != undefined) {
	        $("#dialog-" + _form).dialog('open');
	        $("#dialog-" + _form).contents().find('#table_border').css('border', 'none');
	    }
	}
	function htmlEscape(str) {
	    return (str || '')
					.replace(/&/g, '&amp;')
					.replace(/"/g, '&quot;')
					.replace(/'/g, '&#39;')
					.replace(/</g, '&lt;')
					.replace(/>/g, '&gt;');
	}
	
	jQuery.fn.center = function() {
	    this.css("position", "absolute");
	    this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) +
                                                $(window).scrollTop()) + "px");
	    this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) +
                                                $(window).scrollLeft()) + "px");
	    return this;
	}	
	
	
	function showPreview(id) {
	    if ($("#preview_div").is(":visible") == true) {
	        closePreview();
	    }

	    $.get("../get_survey_data.asp?id=" + id, function(data) {

	        var surveyData = JSON.parse(data);

	        $("#preview_id").val(id);
	        $("#preview_title").val(surveyData.name);
	        $("#preview_skin_id").val(surveyData.skin_id);
	        $("#create_date").val(surveyData.create_date);
	        $("#alert_width").val(surveyData.alert_width);
	        $("#alert_height").val(surveyData.alert_height);

	        if (surveyData.fullscreen == 1) {
	            $("#fullscreen").val(1);
	        }
	        else {
	            $("#fullscreen").val(0);
	        }

	        $("#position").val(surveyData.position);

	        $("#preview_form").submit();
	        $("#preview_div").draggable();
	        $("#preview_div").fadeIn(1000);

	        setTimeout(function() {
	            $(document).bind("click", closePreview);
	            $(window).bind("click", closePreview);
	            $(window).bind("scroll", closePreview);
	            $(window).bind("resize", closePreview);
	            $("#preview").unbind("click", closePreview);

	        }, 1000);


	        var position = surveyData.fullscreen;
	        $("#preview_div").css('left', '');
	        $("#preview_div").css('top', '');
	        $("#preview_div").css('bottom', '');
	        $("#preview_div").css('right', '');
	        switch (position) {

	            case 2:
	                {
	                    $("#preview_div").css('left', '0px');
	                    $("#preview_div").css('top', '0px');
	                    break;
	                }
	            case 3:
	                {
	                    $("#preview_div").css('right', '0px');
	                    $("#preview_div").css('top', '0px');
	                    break;
	                }
	            case 4:
	                {
	                    $("#preview_div").center();

	                    break;
	                }
	            case 5:
	                {
	                    $("#preview_div").css('left', '0px');
	                    $("#preview_div").css('bottom', '0px');
	                    break;
	                }
	            case 6:
	                {
	                    $("#preview_div").css('right', '0px');
	                    $("#preview_div").css('bottom', '0px');
	                    break;
	                }
	            default:
	                {
	                    $("#preview_div").css('right', '0px');
	                    $("#preview_div").css('bottom', '0px');
	                    break;
	                }
	        }
	    });
	
	}

//	function showPreview(id, title, date, skin_id) {
//	
//	    if ($("#preview_div").is(":visible") == true) {
//	        closePreview();
//	    }
//	    
//	     $.get("../get_survey_data.asp?id=" + dupId, function(data) {
//	            if (skin_id == "")
//	                skin_id = "default";

//	            //title = encodeURIComponent(title);
//	            title = htmlEscape(title);
//	            $("#preview_id").val(id);
//	            $("#preview_title").val(title);
//	            $("#preview_skin_id").val(skin_id);
//	            $("#create_date").val(date);

//	            $("#preview_form").submit();
//	            $("#preview_div").draggable();
//	            $("#preview_div").fadeIn(1000);
//        	    
//	            setTimeout(function() {
//	                $(document).bind("click", closePreview);
//	                $(window).bind("click", closePreview);
//	                $(window).bind("scroll", closePreview);
//	                $(window).bind("resize", closePreview);
//	                $("#preview").unbind("click", closePreview);

//	            }, 1000);
//	    }


//	}

	function closePreview() {
	    $(document).unbind("click", closePreview);
	    $(window).unbind("click", closePreview);
	    $(window).unbind("scroll", closePreview);
	    $(window).unbind("resize", closePreview);
	    $("#preview_div").hide();
	}
	$(document).ready(function() {

	    var width = $(".data_table").width();
	    var pwidth = $(".paginate").width();
	    if (width != pwidth) {
	        $(".paginate").width("100%");
	    }
	    $("#preview_div").hide();
	    $("#dialog-form").dialog({
	        autoOpen: false,
	        height: 100,
	        width: 100,
	        modal: true
	    });


	    $('#dialog-frame').on('load', function() {
	        $("#dialog-frame").contents().find('.prelink_stats').on('click', function(e) {
	            e.preventDefault();
	            var link = $(this).attr('href');
	            var title = $(this).attr('title');
	            var last_id = $('.dialog_forms').size();
	            var elem = '<div id="dialog-form-' + last_id + '" class="dialog_forms" style="overflow:hidden;display:none;"><iframe id="dialog-frame-' + last_id + '" style="display:none;width:100%;height:auto;overflow:hidden;border : none;"></iframe></div>';
	            $('#secondary').append(elem);
	            $("#dialog-form-" + last_id).dialog({
	                autoOpen: false,
	                height: 100,
	                width: 100,
	                modal: true
	            });
	            openDialogUI("form-" + last_id, "frame-" + last_id, link, 350, 350, title);
	        });
	    });
	});
	$(function(){

	$(".delete_button").button({
        });
		$(".add_survey_button").button({
			icons : {
				primary : "ui-icon-plusthick"
			}
		});
	});
</script>
</head>
<script language="javascript">
function LINKCLICK_stop() {
  var yesno = confirm ("<%=LNG_STOP_SURVEY_DESC %>.","");
  if (yesno == false) return false;
  return true;
}
</script>
<%
   if(Request("offset") <> "") then 
	offset = clng(Request("offset"))
   else 
	offset = 0
   end if	

   if(Request("sortby") <> "") then 
	sortby = Request("sortby")
   else 
	sortby = "create_date DESC"
   end if	

if (uid <>"") then
'counting pages to display
   if(Request("limit") <> "") then 
	limit = clng(Request("limit"))
   else 
	limit=25
   end if
	
	'--- rights ---
	set rights = getRights(uid, true, false, true, "surveys_val")
	gid = rights("gid")
	gviewall = rights("gviewall")
	gviewlist = rights("gviewlist")
	surveys_arr = rights("surveys_val")
	'--------------

	if(surveys_arr(2)="") then
		linkclick_str = "not"
	else
		linkclick_str = "surveys"
	end if

if gviewall <> 1 then
	Set RS = Conn.Execute("SELECT surveys_main.id FROM surveys_main INNER JOIN alerts ON alerts.id = surveys_main.sender_id AND alerts.sender_id IN ("& Join(gviewlist,",") &") WHERE closed='N' AND alerts.campaign_id = -1")
else
	Set RS = Conn.Execute("SELECT b.id FROM surveys_main as b INNER JOIN alerts as a ON a.id = b.sender_id WHERE closed='N' AND a.campaign_id = -1")
end if
	cnt=0
	Do While Not RS.EOF
	cnt=cnt+1
	RS.MoveNext
	Loop
	RS.Close
	j=cnt/limit
%><body style="margin:0px" class="body">
    <div id="dialog-form" style="overflow: hidden; display: none;">
        <iframe id='dialog-frame' style="width: 100%; height: 100%; overflow: hidden; border: none;
            display: none"></iframe>
    </div>
    <div id="secondary" style="overflow: hidden; display: none;">
    </div>
<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	<tr>
	<td>
	<table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
		<tr>
		<td width=100% height=31 class="main_table_title"><img src="images/menu_icons/survey_20.png" alt="" width="20" height="20" border="0" style="padding-left:7px">
		<a href="surveys.asp" class="header_title" style="position:absolute;top:15px"><%=LNG_SURVEY_QUIZZES_POLLS %></a></td>
		</tr>
		<tr>
		<td class="main_table_body" height="100%">
		<div style="margin:10px;">
		<span class="work_header"><%=LNG_ACTIVE %></span>

		<table width="100%"><tr><td align="right">
		<% if(gid = 0 OR (gid = 1 AND surveys_arr(3)<>"")) then 
            if(ConfOldSurvey = 1) then %>
			    <a href="survey_type_choose.asp" class="add_survey_button"><%=LNG_ADD %></a>
            <% 
            else %>
                <a href="addSurvey.aspx" class="add_survey_button"><%=LNG_ADD %></a>
            <% end if %>
		<%end if%>
		</td></tr></table>

<% if(cnt>0) then %>
<%
	page="surveys.asp?"
	name=LNG_NAME
	Response.Write make_pages(offset, cnt, limit, page, name, sortby) 
	
	response.write "<br><A href='#' class='delete_button' onclick=""javascript: return LINKCLICK('"&linkclick_str&"');"">"&LNG_DELETE&"</a><br/><br/>"
	response.write "<form action='delete_objects.asp' name='delete_objs' id='delete_objs' method='post'><input type='hidden' name='objType' value='surveys'>"
%>
		<table width="100%" height="100%" class="data_table" cellspacing="0" cellpadding="3">
		<tr class="data_table_title">
		<td width="10"><input type='checkbox' name='select_all' id='select_all' onclick='javascript: select_all_objects();'></td>
		<td class="table_title" style="width:50px"><% response.write sorting(LNG_TYPE,"stype", sortby, offset, limit, page) %></td>
		<td class="table_title"><% response.write sorting(LNG_NAME,"name", sortby, offset, limit, page) %></td>
		<% 
		'Approve status collumn
		Set enabled = Conn.execute("SELECT val FROM settings WHERE name = 'ConfAllowApprove'") 
		    if APPROVE = 1 and  enabled("val") = "1" then		
		%>
		
		    <td class="table_title"><%=LNG_APPROVE_STATUS %></td>
		
		<% end if %>
		<td class="table_title"><% response.write sorting(LNG_DATE_CREATED,"create_date", sortby, offset, limit, page) %></td>
		<td class="table_title"><% response.write sorting(LNG_VOTED_USERS,"votes_cnt", sortby, offset, limit, page) %></td>
		<td class="table_title"><%=LNG_SCHEDULED %></td>
		<td class="table_title"><%=LNG_SENT_BY %></td>
		<td class="table_title"><%=LNG_ACTIONS %></td>
				</tr>
<%

'show main table

if gviewall <> 1 then
	Set RS = Conn.Execute("SELECT surveys_main.id as id, surveys_main.name, surveys_main.type, stype, surveys_main.create_date, surveys_main.schedule, surveys_main.from_date, surveys_main.to_date, SUM(vote) as votes_cnt, u.name as sender_name, alerts.title as plain_title, alerts.caption_id as skin_id FROM users u, surveys_main INNER JOIN alerts ON alerts.id = surveys_main.sender_id AND alerts.sender_id IN ("& Join(gviewlist,",") &") LEFT JOIN alerts_received ON surveys_main.sender_id=alerts_received.alert_id  WHERE alerts.sender_id = u.id AND closed='N' AND alerts.campaign_id = -1 group by surveys_main.id, surveys_main.name, surveys_main.type, stype, surveys_main.create_date, surveys_main.schedule, surveys_main.from_date, surveys_main.to_date, u.name, alerts.caption_id, alerts.title ORDER BY "&sortby)
else
	Set RS = Conn.Execute("SELECT surveys_main.id as id, surveys_main.name, surveys_main.type, stype, surveys_main.create_date, surveys_main.schedule, surveys_main.from_date, surveys_main.to_date, SUM(vote) as votes_cnt, u.name as sender_name, alerts.title as plain_title, alerts.caption_id as skin_id FROM users u, surveys_main INNER JOIN alerts ON alerts.id = surveys_main.sender_id LEFT JOIN alerts_received ON surveys_main.sender_id=alerts_received.alert_id  WHERE alerts.sender_id = u.id AND closed='N' AND alerts.campaign_id = -1 group by surveys_main.id, surveys_main.name, surveys_main.type, stype, surveys_main.create_date, surveys_main.schedule, surveys_main.from_date, surveys_main.to_date, u.name, alerts.caption_id, alerts.title ORDER BY "&sortby)
end if
	num=offset
	if(Not RS.EOF) then RS.Move(offset) end if
	Do While Not RS.EOF
		num=num+1
		if((offset+limit) < num) then Exit Do end if
		if(num > offset AND offset+limit >= num) then
			surveyId=RS("id")
		%>
	<tr>
    	<td><input type="checkbox" name="objects" value="<%=RS("id")%>"></td>
		<td align="center">
			<img src="images/alert_types/<%
				stype = RS("stype")
				if stype = "3" then
					Response.Write "poll"
				elseif stype = "2" then
					Response.Write "quiz"
				else
					Response.Write "survey"
				end if
			%>.png" title="<%
				if stype = "3" then
					Response.Write LNG_POLL
				elseif stype = "2" then
					Response.Write LNG_QUIZ
				else
					Response.Write LNG_SURVEY
				end if
			%>" border="0"/>
		</td>
		<td>
			<%=RS("plain_title")%>
		</td>
		
		
			<% 
		
		'Approve status collumn
		Set enabled = Conn.execute("SELECT val FROM settings WHERE name = 'ConfAllowApprove'") 
		    if APPROVE = 1 and  enabled("val") = "1" then		
		%>
		    <td>
		        <%
		            Set statusSet = Conn.Execute("SELECT approve_status  as status FROM alerts WHERE id IN (SELECT sender_id  FROM surveys_main WHERE id = " & RS("id") & ")")
		            
		            approve_status = statusSet("status")
		            
		            approveString = LNG_APPROVED
		            
		            Select Case approve_status
		                Case "0"
		                    approveString = LNG_PENDING_APPROVAL
		                Case "1"
		                    approveString = LNG_APPROVED
		                Case "2"
		                    approveString = LNG_REJECTED
		            
		            End Select
		            
		            Response.Write approveString
		            
    
		         %>
		    
		    </td>
		<%
		    end if
		    'Approve status collumn end
		 %>
		
		
		<td> 
			<script language="javascript">
				<%= "document.write( getUiDateTime('"&RS("create_date") & "'))" %>
			</script>
		</td>
		<td>
		<% 
		votes=0
		if(Not IsNull(RS("votes_cnt"))) then
			votes=RS("votes_cnt")
		else
			votes = 0
		end if
		Response.Write votes
		%>
		</td>
		<td style="text-align: center">
			<% 
			if (RS("schedule")<>"1") then
				Response.Write LNG_NO
			else
				%><script language="javascript"><%
					Response.Write "document.write( getUiDateTime('"&RS("from_date") & "') + ' - ' + getUiDateTime('" & RS("to_date") & "'))"
				%></script><%
			end if
			%>
		</td>
		<td style="text-align: center">
			<%=RS("sender_name") %>
		</td>
		<% 
		
		 'previewHref = "javascript:showPreview(" & RS("id") & ", '" & RS("name") & "', '" & RS("create_date") & "', '" & RS("skin_id") & "')"
		  previewHref = "javascript:showPreview(" & RS("id") & ")"
		%>
		<td align="center" width="120" nowrap="nowrap">
		<a href="<% =previewHref %>">
		<% 
		    Response.Write "<img src=""images/action_icons/preview.png"" alt="""& LNG_PREVIEW &""" title="""& LNG_PREVIEW &""" width=""16"" height=""16"" border=""0"" hspace=""5""/>"
		%>
		</a>
		<% 
		    dupLink = "addSurvey.aspx"
		    if ConfOldSurvey = 1 then
		           dupLink = "survey_add.asp"		    
		    end if
		    
		if((gid = 0 OR (gid = 1 AND surveys_arr(3)<>""))) then %>
		<a href="<% Response.Write dupLink %>?id=<% Response.Write RS("id") %>">
		<img src="images/action_icons/duplicate.png" alt="<%=LNG_RESEND_SURVEY %>" title="<%=LNG_RESEND_SURVEY %>" width="16" height="16" border="0" hspace=5></a> 
		<%end if%>
		
		<% if(gid = 0 OR (gid = 1 AND surveys_arr(4)<>"")) then %>
		<a href='#' onclick="openDialogUI('form','frame','survey_view.asp?id=<%= RS("id") %>',555,450,'<%=LNG_SURVEY_RESULTS %>');">
		<img src="images/action_icons/graph.gif" alt="<%=LNG_VIEW_SURVEY_RESULTS %>" title="<%=LNG_VIEW_SURVEY_RESULTS %>" width="16" height="16" border="0" hspace=5></a> 
		<%end if%>
		
			<% if(gid = 0 OR (gid = 1 AND surveys_arr(5)<>"")) then %>
		<a href="survey_stop.asp?id=
		<% Response.Write RS("id") %>
		" onclick="javascript: return LINKCLICK_stop();"><img src="images/action_icons/stop.png" alt="<%=LNG_STOP_SURVEY %>" title="<%=LNG_STOP_SURVEY %>" width="16" height="16" border="0" hspace=5></a>
			<%end if%>
		
		</td></tr>
		<%
		
		end if
	RS.MoveNext
	Loop
	RS.Close
	page="surveys.asp?"
	name="surveys"
	
	response.write "</form>"

%>
         </table>
<% 
	response.write "<br><A href='#' class='delete_button' onclick=""javascript: return LINKCLICK('"&linkclick_str&"');"">"&LNG_DELETE&"</a><br/><br/>"
	Response.Write make_pages(offset, cnt, limit, page, name, sortby) 
else

Response.Write "<center><b>"&LNG_THERE_ARE_NO_SURVEYS &".</b></center>"

end if %>
		</td>
		</tr>
	</table>
	</td>
	</tr>
</table>
<%
else
  Response.Redirect "index.asp"

end if
%>
</body>
								<form action="../SurveyPreview.aspx" target="preview_frame" id="preview_form" method="post">
								 <input type="hidden" name="skin_id" id="preview_skin_id" />
								 <input type="hidden" name="id" id="preview_id" />
								 <input type="hidden" name="create_date" id="create_date" />
								 <input type="hidden" name="title" id="preview_title" />
								 <input type="hidden" name="alert_width" id="alert_width" runat="server" />
								 <input type="hidden" name="alert_height" id="alert_height" runat="server" />
								 <input type="hidden" name="position" id="position" runat="server" />
								 <input type="hidden" name="fullscreen" id="fullscreen" runat="server" />
								</form>
								
								
								<div id="preview_div" style="position:fixed; left:40%; top:20%; z-index=1 ">
								 <iframe id="preview_frame" name="preview_frame" width="520" height="420" frameborder="0" src="../SurveyPreview.aspx">
								 </iframe>
								</div>      
</html>
<!-- #include file="db_conn_close.asp" --> 