﻿function getWords(str) {
    var withoutSpaceSecquences = str.replace(/\s\s+/g, ' ');
    return withoutSpaceSecquences.split(' ');
}

function checkSubmit(hideId, showId, formId, answersTableId, customAnswerId) {
    var found = false;
    var valid = false;
    if (answersTableId != null) {
        var answersTable = document.getElementById(answersTableId);
        var answersForm = answersTable.getElementsByTagName('input');
        for (var i = 0; i < answersForm.length; i++) {
            if (answersForm[i].name.indexOf('answer') >= 0) {
                if (answersForm[i].type === 'text') {
                    if (!answersForm[i].value) {
                        alert('You should enter a text.');
                        if (answersForm[i].focus) answersForm[i].focus();
                        return false;
                    }
                } else {
                    found = true;
                    if (answersForm[i].checked) {
                        valid = true;
                        if (answersForm[i].id === 'answer_finish') {
                            showId = 'finish';
                        } else if (answersForm[i].name === 'answer_redirect') {
                            showId = 'redirect';
                        }
                    }
                }
            }
        }
    }
    if (found && !valid) {
        alert('You should select an answer.');
        return false;
    }
    if (customAnswerId !== null) {
        var customAnswer = document.getElementById(customAnswerId);
        if (customAnswer) {
            var answersWords = getWords(customAnswer.value);
            if (answersWords.length > 500) {
                alert('Your answer is limited to about 500 words (including spaces)');
                return false;
            }
            if (customAnswer.value.match('[\\[\\]\\%\\_\\{\\}]')) {
                alert('Your answer should not contain any of this symbols: [ ] % _ { }');
                return false;
            }
        }
        if (customAnswer !== null) {
            if (!customAnswer.value) {
                alert('You should enter a text.');
                return false;
            }
        }
    }

    var xhr = new XMLHttpRequest();
    var form = document.getElementById(formId);
    var seriazed = serializeForm(form);

    xhr.addEventListener('load',
        function(event) {
            if (showId === 'redirect') {
                window.location.replace('');
            } else {
                hideOrShow(hideId);
                hideOrShow(showId);
            }
        });

    xhr.open('POST', '/Vote.aspx');
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.send(seriazed);

    return false;
}

function onChangeCustomAnswer(customAnswerId, countId) {
    var customAnswer = document.getElementById(customAnswerId);
    if (customAnswer.value.length > 0) {
        var answersWords = getWords(customAnswer.value);
        var availableWords = 500 - answersWords.length;
        document.getElementById(countId).innerHTML =
            'Your answer is limited to about ' + ((availableWords > 0) ? availableWords : 0) + ' words (including spaces)';
    } else if (customAnswer.value.match('[\\[\\]\\%\\_\\{\\}]')) {
        document.getElementById(countId).innerHTML = 'Your answer should not contain any of this symbols: [ ] % _ { }';
        return false;
    } else {
        document.getElementById(countId).innerHTML = '';
    }
}

function hideOrShow(elementId) {
    if (elementId !== null) {
        var currentElement = document.getElementById(elementId);
        if (currentElement !== null) {
            if (currentElement.style.display === 'none') {
                currentElement.style.display = 'block';
            } else {
                currentElement.style.display = 'none';
            }
        }
    }
}

function serializeForm(form) {
    if (!form || form.nodeName !== 'FORM') {
        return;
    }
    var i, j, q = [];
    for (i = form.elements.length - 1; i >= 0; i = i - 1) {
        if (form.elements[i].name === '') {
            continue;
        }
        switch (form.elements[i].nodeName) {
        case 'INPUT':
            switch (form.elements[i].type) {
            case 'text':
            case 'hidden':
            case 'password':
            case 'button':
            case 'reset':
            case 'submit':
                q.push(form.elements[i].name + '=' + encodeURIComponent(form.elements[i].value));
                break;
            case 'checkbox':
            case 'radio':
                if (form.elements[i].checked) {
                    q.push(form.elements[i].name + '=' + encodeURIComponent(form.elements[i].value));
                }
                break;
            case 'file':
                break;
            }
            break;
        case 'TEXTAREA':
            q.push(form.elements[i].name + '=' + encodeURIComponent(form.elements[i].value));
            break;
        case 'SELECT':
            switch (form.elements[i].type) {
            case 'select-one':
                q.push(form.elements[i].name + '=' + encodeURIComponent(form.elements[i].value));
                break;
            case 'select-multiple':
                for (j = form.elements[i].options.length - 1; j >= 0; j = j - 1) {
                    if (form.elements[i].options[j].selected) {
                        q.push(form.elements[i].name + '=' + encodeURIComponent(form.elements[i].options[j].value));
                    }
                }
                break;
            }
            break;
        case 'BUTTON':
            switch (form.elements[i].type) {
            case 'reset':
            case 'submit':
            case 'button':
                q.push(form.elements[i].name + '=' + encodeURIComponent(form.elements[i].value));
                break;
            }
            break;
        }
    }
    return q.join('&');
}