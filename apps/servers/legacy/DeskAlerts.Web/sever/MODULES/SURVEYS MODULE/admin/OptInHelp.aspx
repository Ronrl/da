﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OptInHelp.aspx.cs" Inherits="DeskAlertsDotNet.Pages.OptInHelp" %>
<%@ Import NameSpace="DeskAlertsDotNet.Parameters" %>
<%@ Import NameSpace="Resources" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<title>DeskAlerts</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <link href="css/style9.css" rel="stylesheet" type="text/css">
        <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
</head>
<body>
	<style>
		div{
			font-size:16px;
		}
	</style>
    <form id="form1" runat="server">
    <table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	    <tr>
	    <td>
	    <table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
		<td width="100%" height="31" class="main_table_title"><img src="images/menu_icons/survey_20.png" alt="" width="20" height="20" border="0" style="padding-left:7px">
		<span id="headerTitle" class="header_title" style="position:absolute;top:22px"><%=resources.LNG_CREATE_OPT_IN_LIST %></span>
		</td>
            <tr>
            <td class="main_table_body" height="100%">
				<div style="padding:10px">
				
	            <p><strong><%=resources.LNG_OPT_IN_QUESTION %></strong></p>
				<div style="margin:10px">
					<p><%=resources.LNG_OPT_IN_FIRST %></p>
					<img src="images/opt-in-step1.png"/>
				</div>
				<hr>
				<div style="margin:10px">
					<p><%=resources.LNG_OPT_IN_SECOND %></p>
					<img src="images/opt-in-step2.png"/>
				</div>
				<hr>
				<div style="margin:10px">
					<p><%=resources.LNG_OPT_IN_THIRD %></p>
					<img src="images/opt-in-step3.png"/>
				</div>
				<hr>
				<div style="margin:10px">
					<p><%=resources.LNG_OPT_IN_FOURTH %></p>
					<img src="images/opt-in-step4.png"/>
				</div>
								<p><%=resources.LNG_OPT_IN_HINT %></p>
				</div>
            </td>
            </tr>
           
 
            
             </table>
    </table>
    </form>
</body>
</html>
