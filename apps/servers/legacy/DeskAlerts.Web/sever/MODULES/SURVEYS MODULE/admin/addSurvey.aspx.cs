﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Net;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using System.Globalization;

namespace deskalerts
{

    public partial class addSurvey : System.Web.UI.Page
    {
        public Dictionary<string, string> vars = new Dictionary<string, string>();
        public int lifetime;
        public int lifetimeFactor;
        public string fullDateFormat;
        protected void Page_PreInit(object sender, EventArgs e)
        {
        }


        public void PrintVar(string name)
        {
            if (!vars.ContainsKey(name))
            {
                Response.Write("[.NET DA LOCALIZATION]: BAD KEY " + name);
                return;
            }

            Response.Write(vars[name]);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            ServicePointManager.ServerCertificateValidationCallback = delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
            {
                return true;
            };

            if (!String.IsNullOrEmpty(Request["alert_campaign"]))
            {
                campaign_id.Value = Request["alert_campaign"].ToString();
            }

            //if (!String.IsNullOrEmpty(Request["lifetime"]))
            //{
            //    lifetime = System.Convert.ToInt32(Request["lifetime"]);
            //}
            //else
            //    lifetime = 0;

            //if (!String.IsNullOrEmpty(Request["lifetimefactor"]))
            //{
            //    lifetimeFactor = System.Convert.ToInt32(Request["lifetimefactor"]);
            //}
            //else
            //    lifetimeFactor = 1;

            //if(!String.IsNullOrEmpty(Request("lifetimemode"

            vars = GetAspVariables("LNG_SCHEDULE_SURVEY_DESC","LNG_LIFETIME_EXT_DESC","LNG_SURVEYS", "LNG_CLICK_TO_CHANGE_SKIN", "LNG_DONT_SHOW_AGAIN", "LNG_DEFAULT", "LNG_SELECT_SKIN", "LNG_SCHEDULE_SURVEY", "LNG_START_DATE",
                "LNG_END_DATE", "GetDateFormat()", "GetTimeFormat()", "LNG_RESOLUTION_DESC1", "LNG_RESOLUTION_DESC2", "LNG_RESOLUTION_DESC3", "LNG_CLICK_HERE", "LNG_MINUTES", "LNG_HOURS", "LNG_DAYS",
                "LNG_LIFETIME_IN", "LNG_TIME_EXPIRED_CANT_RECEIVED", "LNG_SURVEY_APPER_ACC", "LNG_SURVEY_OPTIONS", "LNG_TITLE", "LNG_NEXT", "LNG_SURVEY_RW", "LNG_SURVEY_VR", "LNG_SURVEY_CC", "LNG_DATE_IN_THE_PAST");

            //LNG_END_DATE.Text = vars["LNG_END_DATE"];
            //LNG_START_DATE.Text = vars["LNG_START_DATE"];
            LNG_SURVEY_QUIZZES_POLLS.Text = vars["LNG_SURVEYS"]; //GetAspVariable("LNG_CAMPAIGN_TITLE")  
            LNG_CLICK_TO_CHANGE_SKIN.Text = vars["LNG_CLICK_TO_CHANGE_SKIN"];
            LNG_DONT_SHOW_AGAIN.Text = vars["LNG_DONT_SHOW_AGAIN"];
            //LNG_DEFAULT.Text = vars["LNG_DEFAULT"];
            LNG_SELECT_SKIN.Text = vars["LNG_SELECT_SKIN"];
            //LNG_SCHEDULE_SURVEY.Text = vars["LNG_SCHEDULE_SURVEY"];

            string timeFormatId = vars["GetTimeFormat()"];
            string timeFormat = "HH:mm";
            if (timeFormatId == "2")
                timeFormat = "hh:mm tt";

            fullDateFormat = vars["GetDateFormat()"]+ " " + timeFormat;
            SKINS_CONTENT.Text = FillSkins();

            if (String.IsNullOrEmpty(Request["id"]))
            {
                start_date.Value = DateTime.Now.ToString(fullDateFormat, CultureInfo.InvariantCulture);
                end_date.Value = DateTime.Now.ToString(fullDateFormat, CultureInfo.InvariantCulture);

            }

        }

        private string FillSkins()
        {
            string result = "";
            string url = HttpContext.Current.Request.Url.AbsoluteUri;
            string path = url.Substring(0, url.IndexOf("addSurvey"));
            string url_bridge = path + "/set_settings.asp?name=slink";
            WebRequest request = WebRequest.Create(url_bridge);
            WebResponse response = request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            string variable = reader.ReadToEnd();

            string[] skins = variable.Split(';');

            result += "<div class=\"tile\" id=\"Default\" style=\"\">" +
                          "<div style=\"padding-bottom:5px\"><span autofocus=\"\" style=\"margin:0px\" id=\"header_title\" class=\"header_title\">Default</span></div>" +
                          "<div style=\"font-size: 1px\">" +
                          "<img id=\"skin_preview_img\" src=\"skins/default/thumbnail.png\">" +
                          "</div>" +
                          "</div>";

            for (int i = 0; i < skins.Length - 1; i++)
            {
                string[] skin = skins[i].Split(',');
                result += "<div class=\"tile\" id=\"" + skin[1] + "\" style=\"\">" +
                          "<div style=\"padding-bottom:5px\"><span autofocus=\"\" style=\"margin:0px\" id=\"header_title\" class=\"header_title\">" + skin[1] + "</span></div>" +
                          "<div style=\"font-size: 1px\">" +
                          "<img id=\"skin_preview_img\" src=\"skins/{" + skin[0] + "}/thumbnail.png\">" +
                          "</div>" +
                          "</div>";
            }


            /**/

            return result;
        }

        public Dictionary<string, string> GetAspVariables(params string[] keys)
        {
            string queryString = String.Join(":", keys);

            string url = HttpContext.Current.Request.Url.AbsoluteUri;
            string path = url.Substring(0, url.IndexOf("addSurvey"));
            string url_bridge = path + "/dotnetbridge.asp?var=" + queryString;
            WebRequest request = WebRequest.Create(url_bridge);
            WebResponse response = request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            // Response.Write(reader.ReadToEnd());
            string[] variablesStrings = reader.ReadToEnd().Split('&');


            Dictionary<string, string> result = new Dictionary<string, string>();
            foreach (string keyVal in variablesStrings)
            {

                string[] keyValArray = keyVal.Split('|');

                if (keyValArray.Length != 2)
                    continue;

                string key = keyValArray[0];
                string value = keyValArray[1];

                result.Add(key, value);
            }

            return result;


        }

        /*
          function GetLifetimeControl(ByVal lifetime, ByRef name)
            min_sel = ""
            hour_sel = ""
            day_sel = ""
            if lifetime = "0" then lifetime = ""
            if lifetime <> "" then
                if MyMod(lifetime, 1440) = 0 then
                    lifetime = lifetime / 1440
                    day_sel = " selected"
                elseif MyMod(lifetime, 60) = 0 then
                    lifetime = lifetime / 60
                    hour_sel = " selected"
                else
                    min_sel = " selected"
                end if
            else
                min_sel = " selected"
            end if
            GetLifetimeControl = "<input type=""text"" id="""& name &""" name="""& name &""" style=""width:50px"" value="""& lifetime &""" onkeypress=""return check_size_input(this, event);"">" &vbNewLine& _
                "<select id="""& name &"_factor"" name="""& name &"_factor"">" &vbNewLine& _
                    "<option value=""1"""& min_sel &">"& LNG_MINUTES &"</option>" &vbNewLine& _
                    "<option value=""60"""& hour_sel &">"& LNG_HOURS &"</option>" &vbNewLine& _
                    "<option value=""1440"""& day_sel &">"& LNG_DAYS &"</option>" &vbNewLine& _
                "</select>"
        end function
         */
        public string GetLifetimeControl(int lifetime, string name)
        {
            string minSel = String.Empty;
            string hourSel = String.Empty;
            string daySel = String.Empty;

            if (lifetime != 0)
            {
                if (lifetime % 1440 == 0)
                {
                    lifetime /= 1400;
                    daySel = " selected";
                }
                else if (lifetime % 60 == 0)
                {
                    lifetime /= 60;
                    hourSel = " selected";
                }
                else
                {
                    minSel = " selected";
                }
            }

            string textBox = "<input type='text' id='" + name + "' name='" + name + "' style='width:50px' value='" + lifetime + "'onkeypress='return check_size_input(this, event);'>";
            string comboBox = "<select id='" + name + "_factor' name='" + name + "_factor'>" +
            "<option value='1'" + minSel + ">" + vars["LNG_MINUTES"] + "</option>" +
            "<option value='60'" + hourSel + ">" + vars["LNG_HOURS"] + "</option>" +
            "<option value='1440'" + daySel + ">" + vars["LNG_DAYS"] + "</option>" +
            "</select>";

            return (textBox + comboBox);
        }
        public string GetAspVariable(string variableName)
        {
            string url = HttpContext.Current.Request.Url.AbsoluteUri;
            string path = url.Substring(0, url.IndexOf("addSurvey"));
            string url_bridge = path + "/dotnetbridge.asp?var=" + variableName;
            WebRequest request = WebRequest.Create(url_bridge);
            WebResponse response = request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            string variable = reader.ReadToEnd();
            return variable;
        }
    }
}
