﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%
check_session()
uid = Session("uid")
if (uid <> "") then

campaign = Request("alert_campaign")
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>DeskAlerts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/style9.css" rel="stylesheet" type="text/css">
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script language="javascript" src="jscripts/json2.js"></script>
<script language="javascript" src="jscripts/surveys.js"></script>
<script language="javascript" type="text/javascript">

    (function($) {
    $(function() {
			if ("<%=ConfShowTypesDialog %>"=="1"){
				$("#survey_type_dialog").dialog({
				position: { my: "left top", at: "left top", of: "#dialog_dock" },
					modal: true,
					resizable: false,
					draggable: false,
					width: 'auto',
					resize: 'auto',
					margin:'auto',
					autoOpen:true
				});
			}
        
            $("#survey_table").tabs();
            //$("#survey_button").button();
            //$("#quiz_button").button();
            //$("#poll_button").button();
            $('#if_poll').load(function() {
                $(this).contents().find("body").on('click', function(event) { resizeIframe('if_poll') });
            });
            $('#if_quiz').load(function() {
                $(this).contents().find("body").on('click', function(event) { resizeIframe('if_quiz') });
            });
            $('#if_survey').load(function() {
                $(this).contents().find("body").on('click', function(event) { resizeIframe('if_survey') });
            });
            resizeAllframes();
        });

    })(jQuery);

    function selectSurveyTypeAndClose(type) {
    	switch (type) {
    		case "survey":
    			$("#survey_type_tab").find("a").first().click();
    			break;
    		case "quiz":
    			$("#quiz_type_tab").find("a").first().click();
    			break;
    		case "poll":
    			$("#poll_type_tab").find("a").first().click();
    			break;
    		default:
    			break;
    	}
    	$("#survey_type_dialog").dialog("close");
    }

    function resizeIframe(id) {
        var iframeDoc = document.getElementById(id);
        iframeDoc.height = window.frameElement.scrollHeight - 150;
        var h;
      if (iframeDoc.contentDocument) {
                h = iframeDoc.contentDocument.documentElement.scrollHeight; //FF, Opera, and Chrome
                h = iframeDoc.contentDocument.body.scrollHeight; //ie 8-9-10
                if(iframeDoc.height<h) iframeDoc.height = h;
        }
        else {
            h = iframeDoc.contentWindow.document.body.scrollHeight; //IE6, IE7 and Chrome
            if (iframeDoc.height<h) iframeDoc.height = h;
        }
    }
    function resizeAllframes() {
        var height = window.frameElement.scrollHeight - 150;
        document.getElementById('if_survey').height = height;
        document.getElementById('if_poll').height = height;
        document.getElementById('if_quiz').height = height;
    }    
 
</script>
<body style="margin:0" class="body">

<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	<tr>
	<td>
	<table id="dialog_dock" width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
	    <tr>
	    <td width=100% height=31 class="main_table_title"><img src="images/menu_icons/survey_20.png" alt="" width="20" height="20" border="0" style="padding-left:7px">
		<a href="" class="header_title" style="position:absolute;top:15px"><%=LNG_SURVEY_QUIZZES_POLLS %></a></td>
		</tr>
		<tr>
		<td class="main_table_body" height="100%">
		<div>
		<div id="survey_table" style="padding:0px; border:none;">
		            <ul>
			            <li id="survey_type_tab"><a href="#survey_frame" onclick=""><%=LNG_ADD_SURVEY%> <img border=0 style="display:inline;cursor:pointer;cursor:hand" src="images/help.png" title="<%=LNG_SURVEY_HINT %>" /></a></li>   			
			            <li id="quiz_type_tab"><a href="#quiz_frame"  onclick="" ><%=LNG_ADD_QUIZ%> <img border=0 style="display:inline;cursor:pointer;cursor:hand" src="images/help.png" title="<%=LNG_QUIZ_HINT %>" /></a></li>
			            <li id="poll_type_tab"><a href="#poll_frame"  onclick=""><%=LNG_ADD_POLL%> <img border=0 style="display:inline;cursor:pointer;cursor:hand" src="images/help.png" title="<%=LNG_POLL_HINT %>" /></a></li>           						           			
		            </ul>
		            <a id="survey_frame"><iframe id="if_survey" scrolling="auto" frameborder="no" style="width:100%" src="survey_add.asp?stype=1&needtable=1&alert_campaign=<%=campaign %>"></iframe></a>
		            <a id="quiz_frame"><iframe id="if_quiz" scrolling="auto" frameborder="no" style="width:100%;" src="survey_add.asp?stype=2&needtable=1&alert_campaign=<%=campaign %>"></iframe></a>
		            <a id="poll_frame"><iframe id="if_poll" scrolling="auto" frameborder="no" style="width:100%;" src="survey_add.asp?stype=3&needtable=1&alert_campaign=<%=campaign %>"></iframe></a>

		<img src="images/inv.gif" alt="" width="1" height="100%" border="0">
		</div>
		</div>
		</td>
		</tr>
	</table>
	</td>
	</tr>
</table>
<div id="survey_type_dialog" title="<%=LNG_SELECT_SURVEY_TYPE %>" style='display:none'>
	<div style='float:left;cursor:pointer;cursor:hand;margin-right:10px;margin-bottom:10px;position:relative' onclick="selectSurveyTypeAndClose('survey')">
		<img src='images/alert_types/survey_big.png' />
		<div style='position:absolute;text-align:left;background-color:rgba(0,0,0,0.5);z-index:999;bottom:0px;width:200px;height:32px;color:white;text-align:center'><div style=padding-top:10px><%= LNG_SURVEY%></div></div>
	</div>
	<div style='float:left;cursor:pointer;cursor:hand;margin-right:10px;margin-bottom:10px;position:relative' onclick="selectSurveyTypeAndClose('quiz')">
		<img src='images/alert_types/quiz_big.png' />
		<div style='position:absolute;text-align:left;background-color:rgba(0,0,0,0.5);z-index:999;bottom:0px;width:200px;height:32px;color:white;text-align:center'><div style=padding-top:10px><%= LNG_QUIZ%></div></div>
	</div>
	<div style='float:left;cursor:pointer;cursor:hand;margin-right:10px;margin-bottom:10px;position:relative' onclick="selectSurveyTypeAndClose('poll')">
		<img src='images/alert_types/poll_big.png' />
		<div style='position:absolute;text-align:left;background-color:rgba(0,0,0,0.5);z-index:999;bottom:0px;width:200px;height:32px;color:white;text-align:center'><div style=padding-top:10px><%= LNG_POLL%></div></div>
	</div>
</div>
</body>
</html>
<%

else
  Response.Redirect "index.asp"
end if
%>
<!-- #include file="db_conn_close.asp" -->
