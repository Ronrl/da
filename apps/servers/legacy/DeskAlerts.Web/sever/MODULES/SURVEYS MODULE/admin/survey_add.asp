﻿<%
Response.Expires = 0
Response.Expiresabsolute = Now() - 1 
Response.AddHeader "pragma","no-cache" 
Response.AddHeader "cache-control","private" 
Response.CacheControl = "no-cache" 
%>
<!-- #include file="functions_inc.asp" -->
<!-- #include file="config.inc" -->
<% ConnCount = 3 %>
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%

check_session()

function getSubGroups(groupsList, group_id, alert_id, this_date)
			SQL1 = "INSERT INTO alerts_group_stat (alert_id, group_id, [date]) VALUES ( " & alert_id & ", " & group_id & ", '"&this_date&"')"
			Conn.Execute(SQL1)
			SQL1 = "INSERT INTO alerts_sent_group (alert_id, group_id) VALUES ( " & alert_id & ", " & group_id & ")"
			Conn.Execute(SQL1)


    Set RSsub=Conn.Execute("SELECT user_id FROM users_groups WHERE group_id=" & group_id)
	Do While Not RSsub.EOF
                    SQL2 = "INSERT INTO alerts_sent (alert_id, user_id) VALUES ( " & alert_id & ", " & RSsub("user_id") & ")"
			        Conn.Execute(SQL2)
                    SQL3 = "INSERT INTO alerts_sent_stat (alert_id, user_id, date) VALUES ( " & alert_id & ", " & RSsub("user_id") & ", '"&this_date&"')"
			        Conn.Execute(SQL3)
		RSsub.MoveNext
	loop	

	Set RSsub=Conn.Execute("SELECT group_id FROM groups_groups WHERE container_group_id=" & group_id)
	Do While Not RSsub.EOF
		if(InStr(groupsList,"|" & RSsub("group_id") & "|")=0) then
			getSubGroups groupsList & "|" & RSsub("group_id") & "|", RSsub("group_id"), alert_id, this_date
		end if
		RSsub.MoveNext
	loop	
end function

%>
<%

uid = Session("uid")
this_date=""
if (date_format(1)="d") then this_date=this_date & day(date()) end if
if (date_format(1)="m") then this_date=this_date & month(date()) end if
if (date_format(1)="y") then this_date=this_date & year(date()) end if
this_date=this_date & "/"
if (date_format(2)="d") then this_date=this_date & day(date()) end if
if (date_format(2)="m") then this_date=this_date & month(date()) end if
if (date_format(2)="y") then this_date=this_date & year(date()) end if
this_date=this_date & "/"
if (date_format(3)="d") then this_date=this_date & day(date()) end if
if (date_format(3)="m") then this_date=this_date & month(date()) end if
if (date_format(3)="y") then this_date=this_date & year(date()) end if
this_date=this_date & " 00:00:00"

'editor ---
	ip_groups_arr = Array ("checked","checked","checked","checked","checked","checked","checked")
	Set RS = Conn.Execute("SELECT id FROM users WHERE id="&uid&" AND role='E'")
	if(Not RS.EOF) then
		gid = 1
		editor=0
		Set rs_policy = Conn.Execute("SELECT p.id, p.type, pu.user_id FROM policy p INNER JOIN policy_editor pe ON p.id=pe.policy_id LEFT JOIN policy_user pu ON pu.policy_id = p.id AND pu.user_id = 0 WHERE pe.editor_id=" & uid)
		if Not rs_policy.EOF then
			if rs_policy("type")="A" then
				gid = 0
			elseif not IsNull(rs_policy("user_id")) then 'can send to all
				gid = 0
				editor = 1
			end if
		end if
		if(gid=1 OR gid=2 or editor = 1) then		
			editor_id=RS("id")
			policy_ids=""
			policy_ids=editorGetPolicyIds(editor_id)

			group_ids=""
			group_ids=editorGetGroupIds(policy_ids)
			group_ids=replace(group_ids, "group_id", "users_groups.group_id")

			if(policy_ids<>"" and group_ids <> "") then
				group_ids = " OR " & group_ids
			end if
			ou_ids=""
			ou_ids=editorGetOUIds(policy_ids)
			ou_ids=replace(ou_ids, "ou_id", "OU_User.Id_OU")
			if(policy_ids<>"" OR group_ids<>"") and ou_ids <> "" then
				ou_ids = " OR " & ou_ids
			end if	
			ip_groups_arr = editorGetPolicyList(policy_ids, "ip_groups_val")
		end if
	else
		gid = 0
	end if

	RS.Close
'--------

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>DeskAlerts</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link href="css/style9.css" rel="stylesheet" type="text/css">
	<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css" />

	<script language="javascript" type="text/javascript" src="functions.js"></script>

	<script language="javascript" src="jscripts/json2.js"></script>

	<script language="javascript" src="jscripts/surveys.js"></script>

	<link rel="stylesheet" href="<%Response.write mifTreeFolder%>/style.css" type="text/css" />

	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>

	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
	<script language="javascript" type="text/javascript" src="jscripts/date.js"></script>

	<script language="javascript" type="text/javascript" src="jscripts/preview.js?v=9.2.1.31"></script>

	<script language="javascript" type="text/javascript" src="jscripts/survey_preview.js"></script>

	<link href="css/jquery/jquery_timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet"
		type="text/css">

	<script language="javascript" type="text/javascript" src="jscripts/date.js"></script>

	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery_timepicker/jquery-ui-timepicker-addon.js"></script>

	<script language="javascript" type="text/javascript" src="jscripts/jquery/datepicker_fix.js"></script>

	<script language="javascript" type="text/javascript">

	    var serverDate;
	    var settingsDateFormat = "<%=GetDateFormat()%>";
	    var settingsTimeFormat = "<%=GetTimeFormat()%>";

	    var responseDbFormat = "dd/MM/yyyy HH:mm:ss";
	    var saveDbFormat = "dd/MM/yyyy HH:mm";
	    var saveDbDateFormat = "dd/MM/yyyy";
	    var saveDbTimeFormat = "HH:mm";
	    var uiFormat = getUiDateTimeFormat(settingsDateFormat, settingsTimeFormat);

	    (function($) {
	        $(function() {

	            serverDate = new Date(getDateFromAspFormat("<%=now_db%>", "dd/MM/yyyy HH:mm:ss"));
	            setInterval(function() { serverDate.setSeconds(serverDate.getSeconds() + 1); }, 1000);

	            var fromDate = $.trim($("#start_date").val());
	            var toDate = $.trim($("#end_date").val());

	            if (fromDate != "" && isDate(fromDate, responseDbFormat)) {
	                $("#start_date").val(formatDate(new Date(getDateFromFormat(fromDate, responseDbFormat)), uiFormat));
	            }

	            if (toDate != "" && isDate(toDate, responseDbFormat)) {
	                $("#end_date").val(formatDate(new Date(getDateFromFormat(toDate, responseDbFormat)), uiFormat));
	            }

	            initDateTimePicker($(".date_time_param_input"), settingsDateFormat, settingsTimeFormat);

	            $(".save_next_button").button();
	            $(".save_next_button").bind("click", function() {
	            });

	            $(".add_question_button").button();
	            $(".add_question_button").bind("click", function() {
	            });

	            $(".preview_button").button();
	            $(".preview_button").bind("click", function() {
	            });

	            $(".back_button").button({
	                icons: {
	                    primary: "ui-icon-carat-1-w"
	                }
	            });

	            $(".start_survey_button").button({
	                icons: {
	                    primary: "ui-icon-clipboard"
	                }
	            });

	        });
	    })(jQuery);

	    function getTemplateHTML(type, async, callback) {
	        var templateId = $("#template").val();
	        jQuery.ajax({
	            url: "get_template.asp?temp_id=" + templateId + "&type=" + type,
	            success: function(result) {
	                callback(result);
	            },
	            async: async
	        });
	    }


	    function showSurveyPreview(index) {
	       // alert("INDEX = " + index);
	        var data = new Object();

	        data.create_date = "";

	        var fullscreen = false;
	        var ticker = 0;
	        var acknowledgement = 0;

	        var alert_width = "";
	        var alert_height = "";
	        var alert_title = jQuery("#sur_name").val();
	        var alert_html = getSurveyHTML(index) + "<scr" + "ipt language='javascript'>surveyObject.LNG_YOUR_SURVEY_WAS_SUBMITTED = '<%=LNG_YOUR_SURVEY_WAS_SUBMITTED%>'; surveyObject.default_lng='<%=default_lng%>'; </scr" + "ipt>";
	        var top_template;
	        var bottom_template;


	        getTemplateHTML("top", false, function(data) {
	            top_template = data;
	        });

	        getTemplateHTML("bottom", false, function(data) {
	            bottom_template = data;
	        });

	        data.top_template = top_template;
	        data.bottom_template = bottom_template;
	        data.alert_width = alert_width;
	        data.alert_height = alert_height;
	        data.ticker = ticker;
	        data.fullscreen = fullscreen;
	        data.acknowledgement = acknowledgement;
	        data.alert_title = alert_title;
	        data.alert_html = alert_html;

	        initAlertPreview(data);


	        return false;
	    }


	</script>

	<!---->
	<%
' Variables
mifTreeFolder = "mif_tree"
showCheckbox = 1
strSurveyData=Request("survey_data")
stype=Request("stype")
needtable=Request("needtable")
if needtable<>1 then
needtable=0
end if
if (Request("type")="select_users") then

	%>

	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/mootools.js"></script>

	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.js"></script>

	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Node.js"></script>

	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Hover.js"></script>

	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Selection.js"></script>

	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Load.js"></script>

	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Draw.js"></script>

	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.KeyNav.js"></script>

	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Sort.js"></script>

	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Transform.js"></script>

	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Drag.js"></script>

	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Drag.Element.js"></script>

	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Checkbox.js"></script>

	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Rename.js"></script>

	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Row.js"></script>

	<script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.mootools-patch.js"></script>

	<link rel="stylesheet" href="<%Response.write mifTreeFolder%>/style.css" type="text/css" />

	<script type="text/javascript">
		//
		function send_to_users()
		{
			res = getCheckedOus();
			idSequence = "";			
			idSequence_closed = "";
			if( res )
			{
				for(i=0;i<res.length;i++)
				{
					if(res[i].type != "organization" && res[i].type != "DOMAIN")
					{
						if(res[i].child>0){
							idSequence += res[i].id.toString() + " ";
						}
						else{
							idSequence_closed += res[i].id.toString() + " ";
						}
					}
				}
			}

				document.getElementById("ous_id_sequence").value=idSequence;
				document.getElementById("ous_id_sequence_closed").value=idSequence_closed;

			return true;
//			return false;
			//window.location = "ou_send_to_users.asp?ous_id_sequence="+idSequence+"&alert_id=<%=alertId%>";
			// popupWin = window.open(", "contacts", "location,width=600,height=600,top=0");	
		}
	function changeObjectType(){
					tree.select(tree.root);
					$('ou_ui').style.display="";
					document.getElementById("obj_cnt").innerHTML = "0";
					<% controlOUList policy_ids, showCheckbox %>
					
					var content_file="objects.asp";
					if($('obj_type').value=="R"){
						content_file="objects.asp?sc=<%=showCheckbox %>"
						$('send_type').value="recepients";
						$('upper_send').style.display="";
					}
					if($('obj_type').value=="B"){
						$('send_type').value="broadcast";
					}
					if($('obj_type').value=="I"){
						content_file="ip_groups.asp?sc=<%=showCheckbox %>"
						$('send_type').value="iprange";
						$('upper_send').style.display="";
						$('tree_td').style.display="none";
					}					
					if($('obj_type').value=="0" || $('obj_type').value=="B"){
						$('ou_ui').style.display="none";
						$('upper_send').style.display="none";
					}
					$('content_iframe').src=content_file;		
					$('objects_div').innerHTML = "";
	}	

function check_all_ous(){
		var formInputs = document.getElementsByTagName("input");
		for(var i=0; i<formInputs.length; i++){
			if(formInputs[i].name=="ed_ous"){
				formInputs[i].checked = document.getElementById("select_all_ous").checked;
			}
		}
}

function check_list()
{
		var formInputs = document.getElementsByTagName("input");
		for(var i=0; i<formInputs.length; i++){
			if(formInputs[i].name=="added_user"){
				return true;
			}
			if(formInputs[i].name=="ed_ous" && formInputs[i].checked==true){
				return true;
			}			
		}
	alert("<%=LNG_PLEASE_SELECT_RECIPIENTS_BEFORE_YOU_PROCEED %>.");
	return false;
}
	
function my_submit(i)
{
	document.getElementById("type").value="start_survey";
	
	var val;
	var box;
	box = document.getElementById('obj_type');
	val = box.options[box.selectedIndex].value;	
	send_to_users();

	if(document.getElementById("ous_id_sequence").value.length>0 || document.getElementById("ous_id_sequence_closed").value.length>0) {
	    <%if needtable=1 then%>
	    document.getElementById("main_buttons").setAttribute('target','_parent');
	    <%end if %>
	    return true;
	}

	if(val == "0")
	{
		alert('<%=LNG_PLEASE_CHOOSE_THE_TYPE %>');
		return false;
	}
	else if(val == "I")
	{
		var from = ip2long(document.getElementsByName('iprange_from')[0].value);
		var to = ip2long(document.getElementsByName('iprange_to')[0].value);
		if(from == -1 || to == -1 || from > to)
		{
			alert('Please enter correct IP range');
			return false;
		}
		//document.frm.sub1.value = i;
		<%if needtable=1 then%>
		document.getElementById("main_buttons").setAttribute('target','_parent');
		<%end if %>
		window.parent.parent.document.getElementById('menu').src = window.parent.parent.document.getElementById('menu').src;
		return true;
	}
	else
	{
		//document.frm.sub1.value = i;
		if (val != "B"){ //not broadcast

			return check_list();
		}
		<%if needtable=1 then%>
		document.getElementById("main_buttons").setAttribute('target','_parent');
		<%end if %>
		window.parent.parent.document.getElementById('menu').src = window.parent.parent.document.getElementById('menu').src;
		return true;	
	}
}	


	</script>

	<%end if%>

	<script language="javascript">

	    function my_sub(i) {
	        if (i == 5) {
	            document.getElementById("type").value = "start_survey";

	            var val;
	            var box;
	            box = document.getElementsByName('mybox')[0];
	            val = box.options[box.selectedIndex].value;
	            if (val == 0) {
	                alert('<%=LNG_PLEASE_CHOOSE_THE_TYPE %>');
	                return false;
	            }
	            else {
	                document.frm.step.value = i;
	                if (val == 2 || val == 3) {
	                    return check_list();
	                }
	                else {
	                    document.getElementById("type").value = "start_survey";
	                    return true;
	                }
	            }
	        }
	        else if (i == 3) {
	            document.getElementById("type").value = "select_users";
	        }
	        else {
	            document.getElementById("type").value = "prev";
	        }
	    }

	    function template_preview() {
	        var template_id = document.getElementById("template").value;
	        var preview_url = "<%=alerts_folder %>admin/preview.asp?id=1&survey=1&template='+template_id";

	        //window.open('<%=alerts_folder %>preview.asp?id=1&survey=1&template='+template_id,'', 'status=0, toolbar=0, width=350, height=350, scrollbars=1');
	    }

	    function check_scheduled() {
	        var disabled = !document.getElementById('schedule').checked;
	        if (disabled) {
	            document.getElementById("schedule_div").style.display = "none";
	        }
	        else {
	            document.getElementById("schedule_div").style.display = "";
	        }
	    }

	    function check_valid_dates() {
	        if (document.getElementsByName('schedule')[0].checked == true) {

	            var fromDate = $.trim($("#start_date").val());
	            var toDate = $.trim($("#end_date").val());

	            if (!fromDate) {
	                alert("<%=LNG_PLEASE_ENTER_START_DATE %>!");
	                return false;
	            }
	            if (!toDate) {
	                alert("<%=LNG_PLEASE_ENTER_END_DATE %>!");
	                return false;
	            }

	            if (!isDate(fromDate, uiFormat)) {
	                alert("<%=LNG_PLEASE_ENTER_CORRECT_START_DATE %>!");
	                return false;
	            }
	            else {
	                if (fromDate != "") {
	                    fromDate = new Date(getDateFromFormat(fromDate, uiFormat));
	                    $("#from_date").val(formatDate(fromDate, saveDbFormat));
	                }
	            }

	            if (!isDate(toDate, uiFormat)) {
	                alert("<%=LNG_PLEASE_ENTER_END_DATE %>!");
	                return false;
	            }
	            else {
	                if (toDate != "") {
	                    toDate = new Date(getDateFromFormat(toDate, uiFormat))
	                    $("#to_date").val(formatDate(toDate, saveDbFormat));
	                }
	            }

	            if (fromDate < serverDate && toDate < serverDate) {
	                alert("<%=LNG_DATE_IN_THE_PAST %>")
	                return false;
	            }

	            if (fromDate >= toDate) {
	                alert("<%=LNG_PLEASE_ENTER_CORRECT_START_AND_END_DATETIME %>")
	                return false;
	            }
	        }
	        document.my_form.preview.value = 0;
	        //document.my_form.sub1.value=3;
	        return true;

	    }
	    function mypreview() {
	        /*var obj = document.getElementById("preview2");
	        document.body.removeChild(obj);

obj = document.getElementById("preview1");
	        document.body.removeChild(obj);*/
	        var template_id = document.getElementById("template").value;
	        var preview_url = "<% Response.Write alerts_folder %>admin/preview.asp?id=1&survey=1&template=" + template_id;
	        document.getElementById("preview_iframe").src = preview_url;

	        document.getElementsByName('preview2')[0].style.display = (document.getElementsByName('preview2')[0].style.display == 'none') ? '' : 'none';
	        document.getElementsByName('preview1')[0].style.display = (document.getElementsByName('preview1')[0].style.display == 'none') ? '' : 'none';
	        return false;
	    }
	</script>

	<link href="css/form_style.css" rel="stylesheet" type="text/css" />
	<!--[if IE]>
<style type="text/css" media="all">
@import "css/ie.css";
</style>    
<![endif]-->
</head>
<body style="margin: 0px" class="body">
	<%

if (uid <> "") then
'modify input  form using javascript
	%>
	<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
		<tr>
			<td>
				<table width="100%" height="100%" cellspacing="0" cellpadding="0" <%if needtable=1 then%>
					class="main_table_surveys" <%else%> class="main_table" <%end if %>>
					<%if needtable<>1 then%>
					<tr>
						<td width="100%" height="31" class="main_table_title">
							<a href="#" class="header_title">
								<%=LNG_SURVEY_QUIZZES_POLLS %></a>
						</td>
					</tr>
					<%end if %>
					<tr>
						<td class="main_table_body" height="100%">
							<div style="margin: 10px;">
								<span class="work_header">
									<%
			if (Request("type")<>"preview_questions") then
				if stype = "3" then
					Response.Write LNG_ADD_POLL
				elseif stype = "2" then
					Response.Write LNG_ADD_QUIZ
				else
					Response.Write LNG_ADD_SURVEY
				end if
			else
				Response.Write LNG_PREVIEW
			end if
			 
									%></span>
								<br>
								<br>
								<%

if(Request("type")="" OR ((Request("type")="prev") AND Request("question_number")="0")) then
	temp_id=Request("template")

	if Request("schedule") = "" then
		start_date = now_db
		end_date=""

		myhour1 = hour(start_date)
		myhalf1="am"
		if(myhour1 = 0) then
			myhour1 = 12
		elseif(myhour1 > 11) then
			if(myhour1 > 12) then myhour1 = myhour1 - 12 end if
			myhalf1="pm"
		end if
		myminute1 = minute(start_date)

		if ConfEndDateByDefault then
			end_date=DateAdd("n", ConfEndDateByDefault, now_db)

			myhour2 = hour(end_date)
			myhalf2="am"
			if(myhour2 = 0) then
				myhour2 = 12
			elseif(myhour2 > 11) then
				if(myhour2 > 12) then myhour2 = myhour2 - 12 end if
				myhalf2="pm"
			end if
			myminute2 = minute(end_date)
		end if

		if ConfScheduleByDefault then
			schedule = "1"
		else
			schedule = "0"
		end if
		
		'start_date = GetENGDate(start_date)
		'end_date = GetENGDate(end_date)
	else
		schedule	= Request("schedule")

		start_date	= Request("start_date")
		myhour1		= CLng(Request("start_hour"))
		myminute1	= CLng(Request("start_min"))
		myhalf1		= Request("start_half")

		end_date	= Request("end_date")
		myhour2		= CLng(Request("end_hour"))
		myminute2	= CLng(Request("end_min"))
		myhalf2		= Request("end_half")
	end if
	
	if Request("id") <> "" or Request("alert_id") then
		
		 Set rsOld = Conn.Execute ("SELECT question_type FROM surveys_questions WHERE survey_id = " & CLng(Request("id")))
		if(IsNull(rsOld("question_type"))) then
								%>
								<table cellpadding="4" cellspacing="4" border="0">
									<tr>
										<td>
											<%=LNG_OLD_SURVEYS_RESEND_DESC %>.
										</td>
									</tr>
								</table>
								<img src="images/inv.gif" alt="" width="1" height="100%" border="0">
							</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>
<!-- #include file="db_conn_close.asp" -->
<%
		response.end
		end if

		Dim surveys
		surveys = "{"

		SQL = "SELECT name, stype, template_id, schedule, from_date, to_date FROM surveys_main WHERE id=" & CLng(Request("id"))
		Set RS_surveys = Conn.Execute(SQL)
		
		if not RS_surveys.EOF then
			temp_id = RS_surveys("template_id")
			schedule = RS_surveys("schedule")
			stype = RS_surveys("stype")

			if Not IsNull(RS_surveys("from_date")) then 
				if year(RS_surveys("to_date")) <> 1900 then
					start_date = RS_surveys("from_date")
				end if
			end if
			if Not IsNull(RS_surveys("to_date")) then
				if year(RS_surveys("to_date")) <> 1900 then
					end_date = RS_surveys("to_date")
				end if
			end if

			surveys = surveys & """name"":""" & RS_surveys("name") & ""","
			surveys = surveys & """template_id"":""" & RS_surveys("template_id") & ""","
			surveys = surveys & """schedule"":""" & RS_surveys("schedule") & ""","
			surveys = surveys & """start_date"":""" & FullDateTime(RS_surveys("from_date")) & ""","
			surveys = surveys & """end_date"":""" & FullDateTime(RS_surveys("to_date")) & ""","

			SQL = "SELECT id, question, question_number, question_type FROM surveys_questions WHERE survey_id=" & CLng(Request("id"))
			Set RS_questions = Conn.Execute(SQL)

			
			surveys = surveys & """elements"":["

			i = 0
			Do While Not RS_questions.EOF
				j = 0
				if i <> 0 then surveys = surveys & ","
				surveys = surveys & "{""elements"":["

				SQL = "SELECT answer, correct FROM surveys_answers WHERE question_id=" & RS_questions("id") &" ORDER BY id"
				Set RS_answers = Conn.Execute(SQL)
				Do While Not RS_answers.EOF
					if j <> 0 then surveys = surveys & ","
					surveys = surveys & "{""name"":""" & RS_answers("answer") & """," & """correct"":""" 
					    if (RS_answers("correct") <> 0) then
					        answer_correct = 1
					    else answer_correct = 0
					    end if
					surveys = surveys & answer_correct & """}"
					RS_answers.MoveNext
					j = j + 1
				Loop
				'RS_answers.Close
				

				surveys = surveys & "],""name"":""" & RS_questions("question") & """,""elem_num"":" & j & ", ""type"":""" & RS_questions("question_type") & """}"

				RS_questions.MoveNext
				i = i + 1
			Loop
			RS_questions.Close

			surveys = surveys & "],""elem_num"":" & i & "}"
			if (strSurveyData = "") then
				strSurveyData = surveys
			end if
		end if
	end if
	
%>
<form name="frm" id="frm" action="" method="POST">
<input type="hidden" id="stype" name="stype" value="<%=stype%>" />
<input type="hidden" name="needtable" value="<%=needtable%>" />
<table cellpadding="4" cellspacing="4" border="0" id="survey_table">
	<tr>
		<td>
			<% if CAMPAIGN = 1 and Request("alert_campaign") <> "" and Request("alert_campaign") <> "-1" then 
		 
			Response.Write LNG_CAMPAIGN & ":"
		 end if %>
		</td>
		<td>
			<% if CAMPAIGN = 1 then %>
			<% if Request("alert_campaign") = "" or  Request("alert_campaign") = "-1" then

							 Response.Write  "<input type='hidden' id='campaign_select' name='campaign_select' value='-1'>"
                        else
							
                            SQL = "SELECT name FROM campaigns WHERE id = " & Request("alert_campaign")
                            Set RS2 = Conn.Execute(SQL)
                            if not RS2.EOF then
                                Response.Write "<label>"&HtmlEncode(RS2("name"))&"</label>"
                                Response.Write  "<input type='hidden' id='campaign_select' name='campaign_select' value='"&Request("alert_campaign")&"'>"                                
                            end if
                         end if 
			%>
			<% else
                       
                        Response.Write  "<input type='hidden' id='campaign_select' name='campaign_select' value='-1'>"
                       
                       end if
			%>
		</td>
	</tr>
	<tr>
		<td>
			<%=LNG_NAME %>:
		</td>
		<td>
			<input type="text" name="sur_name" id="sur_name" size="35" value="" />
		</td>
	</tr>
	<tr>
		<td>
			<br />
			<input type="checkbox" id="schedule" name="schedule" value="1" <% if schedule = "1" then Response.Write "checked" end if %>
				onclick="check_scheduled();">
			<label for="schedule">
				<%
if stype = "3" then
	Response.Write LNG_SCHEDULE_POLL
elseif stype = "2" then
	Response.Write LNG_SCHEDULE_QUIZ
else
	Response.Write LNG_SCHEDULE_SURVEY
end if
				%></label>
		</td>
		<td>
		</td>
	</tr>
	<tr id="schedule_div">
		<td colspan="2">
			<table border="0">
				<tr>
					<td align="right">
						<%=LNG_START_DATE %>:
					</td>
					<td>
						<input class="date_time_param_input" name='start_date' id="start_date" type="text"
							value="<%=FullDateTime(start_date) %>" size="20" />
						<input name='from_date' id="from_date" type="hidden" />
					</td>
				</tr>
				<tr>
					<td align="right">
						<%=LNG_END_DATE %>:
					</td>
					<td>
						<input class="date_time_param_input" name='end_date' id="end_date" type="text" value="<%=FullDateTime(end_date) %>"
							size="20" />
						<input name='to_date' id="to_date" type="hidden" />
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr style="display: none">
		<td>
			<%=LNG_SELECT_TEMPLATE %>:
		</td>
		<td>
			<select name="template" id="template" width="40">
				<%
'	if(gid<>0) then
'	    	SQL = "SELECT id, name FROM templates WHERE sender_id="&uid
'	else
	    	SQL = "SELECT id, name FROM templates"
'	end if
	Set RS1 = Conn.Execute(SQL)
        Do While Not RS1.EOF
		if(clng(temp_id)=clng(RS1("id"))) then 
			Response.Write "<option value='"&RS1("id")&"' selected>"&HtmlEncode(RS1("name"))&"</option>"
		else
			Response.Write "<option value='"&RS1("id")&"'>"&HtmlEncode(RS1("name"))&"</option>"
		end if
		RS1.MoveNext
	loop

				%>
			</select>
			<a href="#" onclick="javascript: showSurveyPreview();"><b>
				<%=LNG_PREVIEW %></b></a>
		</td>
		<td>
		</td>
	</tr>
	<tr>
		<td colspan="3">
			<div align="left" style="margin-top: 20px">
				<table style="padding: 0px; margin-top: 5px; margin-bottom: 5px; border-collapse: collapse;
					border0px; width: 100%;">
					<tr>
						<td style="padding: 0px; margin: 0px;">
							<table style="padding: 0px; margin: 0px;">
								<tr>
									<td>
										<b>
											<%=LNG_QUESTION_WORD%>
											[1]</b>
									</td>
									<td align="left">
									</td>
								</tr>
							</table>
						</td>
						<td style="padding: 0px; margin: 0px; font-size: 12px !important;">
							<% 
						next_link_html= "<font style='font-size:12px;'>"&LNG_NEXT_QUESTION&"&nbsp;&gt;</font>"
						if (strSurveyData<>"") then
						    'Response.Write "DATA3 = " & strSurveyData
							Set surveyObject = my_eval(strSurveyData)
							if (surveyObject.elements.length>1) then
								next_link_html = "<A href='#' class=""next_question_button"" onclick=""javascript: my_survey_submit('next'); return false;"">"&next_link_html&"<a/>"
							end if
						end if
							%>
							<font>[&nbsp;</font>&lt;&nbsp;<%=LNG_PREVIOUS_QUESTION%><font id="question_links_separator">&nbsp;|&nbsp;</font><%=next_link_html%><font>&nbsp;]</font>
						</td>
						<td align="right" style="padding: 0px; margin: 0px;">
						</td>
					</tr>
				</table>
			</div>
		</td>
	</tr>
	<tr>
		<td>
			<%=LNG_QUESTION_TYPE %>:
		</td>
		<td colspan="2">
			<select name="question_type" id="question_type" onchange="changeQuestionType();">
				<option value="M">
					<%=LNG_MULTIPLE_ANSWERS %></option>
				<option value="N">
					<%=LNG_MULTIPLE_CHOICE %></option>
				<% if stype <> "2" then %>
				<option value="I">
					<%=LNG_INPUT_ANSWER %></option>
				<% end if %>
				<% if stype = "1" then %>
				<option value="T">
					<%=LNG_MULTIPLE_TEXTBOXES %></option>
				<option value="C">
					<%=LNG_CONDITIONAL %></option>
				<% end if %>
			</select>
			<span id="input_answer_desc_div" style="display: none; color: gray;"><i>
				<%=LNG_INPUT_ANSWER_DESC %></i></span>
		</td>
	</tr>
	<tr id="question_tr">
		<td>
			<%=LNG_QUESTION %>:
		</td>
		<td>
			<input type="text" name="question" id="question" size="35" value="" />
		</td>
		<td>
		</td>
	</tr>
	<tr>
		<td>
			<%=LNG_OPTION %>:
		</td>
		<td>
			<input type="text" name="answer" size="35" value="" />
		</td>
		<td>
			<% if stype = "2" then %>&nbsp;<input type="checkbox" name="correct" />&nbsp;<%=LNG_THIS_IF_CORRECT_OPTION %><% end if %>
		</td>
	</tr>
	<tr>
		<td>
			<%=LNG_OPTION %>:
		</td>
		<td>
			<input type="text" name="answer" size="35" value="" />
		</td>
		<td>
			<% if stype = "2" then %>&nbsp;<input type="checkbox" name="correct" />&nbsp;<%=LNG_THIS_IF_CORRECT_OPTION %><% end if %>
		</td>
	</tr>
</table>
<input type="hidden" name="question_number" id="question_number" value="0">
<input type="hidden" name="type" id="type" value="">
<input type="hidden" name="survey_data" id="survey_data" value="<%= HtmlEncode(strSurveyData) %>">
<!--<input type="hidden" name="answers_num" id="answers_num" value="">-->
<div>
	<div id="insert_answer_div" style="display: ;">
		<!--<input type="image" src="images/survey_add_answer.png" alt="add answer" onclick="javascript: test_add(); return false;"/>-->
		<a href="#" onclick="javascript: insert_new_answer('');"><b>
			<%=LNG_ADD_OPTION %></b></a>
	</div>
	<div id="main_survey_buttons" align="right">
		<a href="#" onclick="javascript: showSurveyPreview();" class="preview_button">
			<%=LNG_PREVIEW %></a> <a href="javascript: javascript: my_survey_submit('add');"
				class="add_question_button">
				<%=LNG_SAVE_AND_ADD_NEW_QUESTION%></a> <a href="javascript: my_survey_submit('save_next');"
					class="save_next_button">
					<%=LNG_SAVE_AND_NEXT%></a>
	</div>
</div>
</form>
<div id="preview2" name="preview2" style="display: none; z-index: 1; position: absolute;
	bottom: 10px; right: 10px; background-image: url(images/alert_preview.gif);"
	class="div_preview">
	<iframe id="preview_iframe" style="position: absolute; z-index: 1; background-color: white;"
		src="<% Response.Write alerts_folder %>admin/preview.asp?id=1&survey=1&template=1"
		frameborder="0" class="iframe_preview"></iframe>
</div>
<div id="preview1" name="preview1" style="display: none; z-index: 1; position: absolute;
	cursor: pointer; background-image: url(images/close_preview.gif);" onclick="mypreview();"
	class="preview_close">
</div>

<script language="javascript">

    check_scheduled();

</script>

<%
end if
%>

<script language="javascript" src="surveys_functions.asp"></script>

<script language="javascript">
	var survey;
	<%
		if(strSurveyData="") then
	%>
		survey = new Survey();
	<%
		else
	%>

	survey = JSON.parse(jQuery("<div/>").html("<%=jsEncode(ScriptEscape(strSurveyData)) %>".replace(/&/g, '&amp;').replace(/"/g, '&quot;').replace(/'/g, '&#39;').replace(/</g, '&lt;').replace(/>/g, '&gt;')).text());

	<%end if%>
	
	$(function(){
		if (typeof add_survey_methods == "function") add_survey_methods(survey);
		if (typeof fill_the_form == "function") {fill_the_form();}
	});
	
</script>

<%
if (CLng(Request("question_number")) > 0 AND Request("type")<>"select_users" AND Request("type")<>"start_survey" AND Request("type")<>"preview_questions") then

this_question_number=Clng(Request("question_number"))
this_question_number_display=Clng(Request("question_number"))+1
'Response.Write "DATA1 = " & strSurveyData
Set surveyObject = my_eval(strSurveyData)

%>
<div>
	<table style="padding: 0px; margin-top: 5px; margin-bottom: 5px; border-collapse: collapse;
		border0px; width: 100%;">
		<tr>
			<td style="padding: 0px; margin: 0px;">
				<table style="padding: 0px; margin: 0px;">
					<tr>
						<td>
							<b>
								<%=LNG_QUESTION_WORD%>
								[<%=this_question_number_display%>]</b>
						</td>
						<td align="left">
							<img src="images/action_icons/delete.gif" border="0" onclick="javascript: delete_question();">
						</td>
					</tr>
				</table>
			</td>
			<td style="padding: 0px; margin: 0px;">
				<% 
				next_link_html= "<font style='font-size:12px;'>"&LNG_NEXT_QUESTION&"&nbsp;&gt;</font>"
				if (surveyObject.elements.length > this_question_number_display) then 
					next_link_html = "<A href='#' class=""next_question_button"" onclick=""javascript: my_survey_submit('next'); return false;"">"&next_link_html&"<a/>"
				end if
				%>
				<font>[&nbsp;</font><a href='#' onclick="javascript: my_survey_submit('prev');">&lt;&nbsp;<%=LNG_PREVIOUS_QUESTION%></a><font
					id="question_links_separator">&nbsp;|&nbsp;</font><%=next_link_html%><font>&nbsp;]</font>
			</td>
			<td align="right" style="padding: 0px; margin: 0px;">
			</td>
		</tr>
	</table>
</div>
<form name="frm" id="frm" action="" method="POST">
<input type="hidden" id="stype" name="stype" value="<%=stype%>" />
<table cellpadding="4" cellspacing="4" border="0" id="survey_table">
	<tr>
		<td>
			<%=LNG_QUESTION_TYPE %>:
		</td>
		<td colspan="2">
			<select name="question_type" id="question_type" onchange="changeQuestionType();">
				<option value="M">
					<%=LNG_MULTIPLE_ANSWERS %></option>
				<option value="N">
					<%=LNG_MULTIPLE_CHOICE %></option>
				<% if stype = "1" then %>
				<option value="I">
					<%=LNG_INPUT_ANSWER %></option>
				<option value="T">
					<%=LNG_MULTIPLE_TEXTBOXES %></option>
				<option value="C">
					<%=LNG_CONDITIONAL %></option>
				<% end if %>
			</select>
			<span id="input_answer_desc_div" style="display: none; color: gray;"><i>
				<%=LNG_INPUT_ANSWER_DESC %></i></span>
		</td>
	</tr>
	<tr id="question_tr">
		<td>
			<%=LNG_QUESTION %>:
		</td>
		<td>
			<input type="text" name="question" id="question" size="35" value="" />
		</td>
		<td>
		</td>
	</tr>
	<tr>
		<td>
			<%=LNG_OPTION %>:
		</td>
		<td>
			<input type="text" name="answer" size="35" value="" />
		</td>
		<td>
			<% if stype = "2" then %>&nbsp;<input type="checkbox" name="correct" />&nbsp;<%=LNG_THIS_IF_CORRECT_OPTION %><% end if %>
		</td>
	</tr>
	<tr>
		<td>
			<%=LNG_OPTION %>:
		</td>
		<td>
			<input type="text" name="answer" size="35" value="" />
		</td>
		<td>
			<% if stype = "2" then %>&nbsp;<input type="checkbox" name="correct" />&nbsp;<%=LNG_THIS_IF_CORRECT_OPTION %><% end if %>
		</td>
	</tr>
</table>
<input type="hidden" name="sur_name" id="sur_name" value="">
<input type="hidden" name="campaign_select" id="campaign_select" value="<%= HtmlEncode(Request("campaign_select")) %>">
<input type="hidden" name="template" id="template" value="<%= HtmlEncode(Request("template")) %>">
<input type="hidden" name="schedule" id="schedule" value="<%= Request("schedule") + 0 %>" />
<input type="hidden" name="start_date" id="start_date" value="<%= HtmlEncode(Request("start_date")) %>" />
<input type="hidden" name="end_date" id="end_date" value="<%= HtmlEncode(Request("end_date")) %>" />
<input type="hidden" name="question_number" id="question_number" value="<%= this_question_number %>">
<input type="hidden" name="type" id="type" value="">
<input type="hidden" name="survey_data" id="survey_data" value="<%= HtmlEncode(strSurveyData) %>">
<div>
	<div id="insert_answer_div" style="display: ;">
		<a href="#" onclick="javascript: insert_new_answer(''); return false;"><b>
			<%=LNG_ADD_OPTION %></b></a>
	</div>
	<div align="right">
		<a href="javascript: javascript: my_survey_submit('add');" class="add_question_button">
			<%=LNG_SAVE_AND_ADD_NEW_QUESTION%></a> <a href="javascript: my_survey_submit('save_next');"
				class="save_next_button">
				<%=LNG_SAVE_AND_NEXT%></a>
	</div>
</div>
</form>
<%
end if

if (Request("type")="select_users") then
this_question_number=Clng(Request("question_number"))


%>

<script>
    // jQuery(document).ready(function(){
    // jQuery('#survey_data').val(decodeURI('<%= Request("data") %>'));
    //});
</script>

<%   if (Request("new")="newSurvey") then %>

<script>
    jQuery(document).ready(function() {

        jQuery('#type').val('start_survey');
        jQuery('#title').val(decodeURI('<%= Request("title") %>'));
    });
</script>

<% end if %>
<!-- <%= HtmlEncode(strSurveyData) %> -->
<form name="frm" action="" id="main_buttons" target="" method="POST">
<input type="hidden" name="stype" value="<%=stype%>" />
<input type="hidden" name="needtable" value="<%=needtable%>" />
<input type="hidden" name="sur_name" id="sur_name" value="<%= HtmlEncode(Request("sur_name")) %>">
<input type="hidden" name="campaign_select" id="campaign_select" value="<%= HtmlEncode(Request("campaign_select")) %>">
<input type="hidden" name="template" id="template" value="<%= HtmlEncode(Request("template")) %>">
<input type="hidden" name="schedule" id="schedule" value="<% Response.Write Request("schedule") + 0 %>" />
<input type="hidden" name="start_date" id="start_date" value="<%= HtmlEncode(Request("start_date")) %>" />
<input type="hidden" name="end_date" id="end_date" value="<%= HtmlEncode(Request("end_date")) %>" />
<input type="hidden" name="question_number" id="question_number" value="<% response.write this_question_number %>">
<input type="hidden" name="type" id="type" value="">
<input type="hidden" name="survey_data" id="survey_data" value="<%= HtmlEncode(strSurveyData) %>">
<input type="hidden" name="new" id="new" value='<%= Request("new")%>'>
<%
if (Request("new")="newSurvey") then
%>
<input type="hidden" name="campaign_id" id="campaign_id" value='<%= Request("campaign_id")%>'>
<input type="hidden" name="skin_id" id="skin_id" value='<%= Request("skin_id")%>'>
<input type="hidden" name="title" id="title" value=''>
<input type="hidden" name="cant_close" id="cant_close" value='<%= Request("cant_close")%>'>
<input type="hidden" name="plain_title" id="plain_title" value='<%= Replace(Request("plain_title"), "'", "''")%>'>
<input type="hidden" name="date_start" id="date_start" value='<%= Request("date_start")%>'>
<input type="hidden" name="date_end" id="date_end" value='<%= Request("date_end")%>'>
<input type="hidden" name="alert_width" id="alert_width" value='<%=Request("alert_width") %>' />
<input type="hidden" name="alert_height" id="alert_height" value='<%=Request("alert_height") %>' />
<input type="hidden" name="fullscreen" id="fullscreen" value='<%=Request("fullscreen") %>' />
<input type="hidden" name="position" id="position" value='<%=Request("position") %>' />
<input type="hidden" name="resizable" id="resizable" value='<%=Request("resizable") %>' />
<input type="hidden" name="has_lifetime" id="has_lifetime" value='<%=Request("has_lifetime") %>' />
<%
end if
%>
<input type="hidden" id="step" name="step" value="">
<table cellpadding="4" cellspacing="4" border="0">
	<tr>
		<td colspan="2">
			<%=LNG_TYPE %>:
			<select id="obj_type" name="mybox" onchange="javascript:changeObjectType()">
				<option value="0">--
					<%=LNG_CHOOSE %>
					--</option>

                <%
                   gsendtoall = 1
                    set rs_policy = Conn.Execute("SELECT p.id, p.type, pu.user_id FROM policy p INNER JOIN policy_editor pe ON p.id=pe.policy_id LEFT JOIN policy_user pu ON pu.policy_id = p.id AND pu.user_id = 0 WHERE pe.editor_id=" & uid)
		            if not rs_policy.EOF then

			            if  IsNull(rs_policy("user_id")) then
				            'gid = 0
				            gsendtoall = 0
			            end if
		            end if


	            if  ConfDisableBroadcast <> 1 and gsendtoall =1 then
                    
                %>
				<option value="B">
					<%=LNG_BROADCAST %></option>
                <% end if %>
				<option value="R">
					<%=LNG_SELECT_RECIPIENTS %></option>
				<% if (ConfEnableIPRanges = 1 AND (ip_groups_arr(3)="checked")) then %>
				<option value="I">
					<%=LNG_IP_GROUPS %></option>
				<% end if %>
			</select>
		</td>
	</tr>
	</td> </tr>
</table>
<div align="right" id="upper_send" style="display:none"><!--added:- style = "display:none" losev Fedor 09.02.2017 -->
	<% if(ConfOldSurvey = 1) then %>
	<button class="back_button" type="submit" onclick="javascript: return my_sub(4);">
		<%=LNG_BACK%></button>
	<% end if %>
	<button type="submit" class="start_survey_button" onclick="javascript: return my_submit(2);">
		<% 
			
			
			if uid <> "1" then
			    surveys_checked_arr = Array ("", "", "", "", "", "", "")
			    'Set RS = conn.execute("SELECT surveys_val FROM policy WHERE editor_id = " & uid)
			     Set enabled = Conn.execute("SELECT val FROM settings WHERE name = 'ConfAllowApprove'") 
			     SQL = "SELECT  surveys_val FROM policy_list INNER JOIN policy_editor ON policy_list.policy_id=policy_editor.policy_id WHERE editor_id=" & uid
		   
		        ' Response.Write SQL
		         Set RS = Conn.Execute(SQL)
				
				    'Response.Write RS("surveys_val")
			        surveys_checked_arr = p_makeCheckedArray(RS("surveys_val"))
			
			    if(gid <> 0 and APPROVE = 1 and enabled("val") = "1") then			   
			          Response.Write(LNG_APPROVE_SUBMIT)
			    elseif ( CAMPAIGN = 1 and ( Request("campaign_select") <> "-1" and Request("campaign_select") <> "" ) )  then 
			        Response.Write(LNG_SAVE) 
			    else 
			        Response.Write(LNG_START)
			    end if
			  else
			    Response.Write(LNG_START)
			  end if
		%></button></div>
<br />
<div id="ou_ui" style="display: none">
	<!-- #include file="ou_ui_include.asp" -->
	<br />
</div>

<script language="javascript">
	<% controlOUList policy_ids, showCheckbox %>
</script>

<div align="right">
	<% if(ConfOldSurvey = 1) then %>
	<button class="back_button" type="submit" onclick="javascript: return my_sub(4);">
		<%=LNG_BACK%></button>
	<% end if %>
	<button type="submit" id="main_buttons" class="start_survey_button" onclick="javascript: return my_submit(2);">
		<% 
			
			
			if uid <> "1" then
			    surveys_checked_arr = Array ("", "", "", "", "", "", "")
			    'Set RS = conn.execute("SELECT surveys_val FROM policy WHERE editor_id = " & uid)
			     Set enabled = Conn.execute("SELECT val FROM settings WHERE name = 'ConfAllowApprove'") 
			     SQL = "SELECT  surveys_val FROM policy_list INNER JOIN policy_editor ON policy_list.policy_id=policy_editor.policy_id WHERE editor_id=" & uid
		   
		        ' Response.Write SQL
		         Set RS = Conn.Execute(SQL)
				
				    'Response.Write RS("surveys_val")
			        surveys_checked_arr = p_makeCheckedArray(RS("surveys_val"))
			
			    if(gid <> 0 and APPROVE = 1 and enabled("val") = "1") then			   
			          Response.Write(LNG_APPROVE_SUBMIT)
			    elseif ( CAMPAIGN = 1 and ( Request("campaign_select") <> "-1" and Request("campaign_select") <> "" ) )  then 
			        Response.Write(LNG_SAVE) 
			    else 
			        Response.Write(LNG_START)
			    end if
			  else
			    Response.Write(LNG_START)
			  end if
		%>
	</button>
</div>
<input type="hidden" name="send_type" id="send_type" value="0" />
<%
  'Response.Write "<input type='hidden' name='alert_id' value='"+ Request("id")+ "'/>"
%>
<input type="hidden" id="ous_id_sequence" name="ous_id_sequence" value="" />
<input type="hidden" id="ous_id_sequence_closed" name="ous_id_sequence_closed" value="" />
</form>
<%
end if
if (Request("type")="start_survey") then

%>

<script language="JScript" runat="server">
    function my_eval(text) {
        text = decodeURIComponent(text);
        return eval( '(' + text  + ')');
    }
</script>

<%
chk_max=Request("chk_max")
'sur_name=Request("sur_name")
'question=Request("question")
'num_answers=Request("num_answers")
'temp_id=Request("template")
send_type=Request("send_type")
if(Request("send_type")="broadcast") then 
	if(gid=0) then
		mytype="B"
	else
		mytype="R"
	end if
end if
if(Request("send_type")="recepients") then 
	mytype="R" 
end if
if(Request("send_type")="iprange") then 
	mytype="I" 
end if
'group_ad_type=Request("group_ad_type")
group_ad_type="B"



Dim myJSON, questions, question, answers, answer


Set myJSON = my_eval(Request("survey_data"))
'Dim fso, tf
'    Set fso = CreateObject("Scripting.FileSystemObject")
'    path = Server.MapPath("logs/log.inf")
'    Set tf = fso.OpenTextFile(path, 8, True)
'    tf.WriteLine("survey_add LOG")
'    tf.WriteLine("survey_data="&Request("survey_data")) 
'tf.Close
plain_title = ""
autoclose = ""
caption_string = ""
if (Request("new")="newSurvey") then
    temp_id = 1
    caption_id = Replace(Replace(Request("skin_id"),"{",""),"}","")
    if (caption_id = "default") then
        caption_string = " NULL "
    else
        caption_string = "'"&caption_id&"'"
    end if
    sur_name = Request("title")
    if (Request("cant_close") = 0) then
        autoclose = 0
    else
        autoclose = "2147482647" 'this is (long VBS-1000). This need for deny close survey if not answered
    end if
    plain_title = HTMLEncode(Request("plain_title"))
    schedule = clng(Request("schedule"))
    has_lifetime = Request("has_lifetime")

    if (schedule <> 0 or has_lifetime <> "0" ) then
       from_date =  Request("date_start")
       to_date =  Request("date_end")
       schedule=1
       
       if(has_lifetime = "1") then
         schedule=0
       end if
    end if
    
    alert_width = Request("alert_width")
    
    if(alert_width = "") then
        alert_width = 500
    end if
    
    alert_height = Request("alert_height")
    
    if(alert_height = "") then
        alert_height = 400
    end if   
    
    
   position = Request("position")
    
    if(position = "") then
        position = 0
    end if  
    
     fullscreen = Request("fullscreen")
    
    if(fullscreen = "1") then
        position = 1
    end if   
    
    resizable = Request("resizable")
      
     if(resizable = "") then
        resizable = 0
     end if
    

    campaign_id = Request("campaign_id")
else
    'survey name
    sur_name=myJSON.name

    'survey template id
    temp_id=myJSON.template_id
    Dim schedule, from_date, to_date
    schedule=Clng(myJSON.schedule)

    if(schedule <> 1) then schedule=0 end if

    if(schedule <> 0) then
	    from_date= myJSON.start_date
	    to_date= myJSON.end_date
    end if

    campaign_id = myJSON.campaign
end if
'insert survey
classid = "64"
if stype = "2" then
	classid = "128"
elseif stype = "3" then
	classid = "256"
end if

if(campaign_id = "-1") then
    schedule_type = 1
else
    schedule_type = 0
end if

Set is_started = Conn.Execute("SELECT DISTINCT 1 FROM alerts WHERE campaign_id = " & campaign_id & " AND schedule_type = '1'")
if not is_started.EOF then
	schedule_type = 1
end if  
 
approve_status = 1
if uid <> "1" then 
    surveys_checked_arr = Array ("", "", "", "", "", "", "")
	Set RS = conn.execute("SELECT  surveys_val FROM policy_list INNER JOIN policy_editor ON policy_list.policy_id=policy_editor.policy_id WHERE editor_id=" & uid)
	surveys_checked_arr = p_makeCheckedArray(RS("surveys_val"))	
    'approve_status = 1
    if(APPROVE = 1) then
         Set enabled = Conn.execute("SELECT val FROM settings WHERE name = 'ConfAllowApprove'") 

        if(enabled("val") = "1"  and surveys_checked_arr(6) <> "checked"  ) then
            approve_status = 0
         end if
    end if
end if

html_title = "<!-- html_title=''"&  HTMLEncode(sur_name) &"'' -->"
   ' html_title = HTMLEncode(html_title)
if (Request("new")="newSurvey") then
      SQL = "INSERT INTO alerts ([alert_text], [title], [type], [group_type], [create_date], [sent_date], [type2], [from_date], [to_date], [schedule], [schedule_type], [recurrence], [urgent], [toolbarmode], [deskalertsmode], [sender_id], [template_id], [aknown], [ticker], [fullscreen], [email_sender], [email],  [desktop], [autoclose], [class], [resizable], [post_to_blog], [social_media], [campaign_id],[caption_id], [approve_status], [alert_width], [alert_height], [lifetime]) "&_
	" VALUES (N'"&html_title&"', N'" & plain_title & "', 'S', 'B', GETDATE(), GETDATE(), '" & mytype & "', '" & from_date & "', '" & to_date & "', "&schedule&", '" & cstr(schedule_type) &"', '0', '0', '0', '1', "&uid&", "&temp_id&", '0', '0', '" & position &"', '', '0', '1', "&autoclose&", "&classid&", "& resizable &" , 0, 0," & campaign_id& ","&caption_string&"," & approve_status & ", "&  alert_width & "," & alert_height& "," & Request("has_lifetime") &")"
else 
    SQL = "INSERT INTO alerts ([alert_text], [title], [type], [group_type], [create_date], [sent_date], [type2], [from_date], [to_date], [schedule], [schedule_type], [recurrence], [urgent], [toolbarmode], [deskalertsmode], [sender_id], [template_id], [aknown], [ticker], [fullscreen], [email_sender], [email],  [desktop], [autoclose], [class], [resizable], [post_to_blog], [social_media], [campaign_id], [approve_status]) "&_
	" VALUES (N'"&html_title&"', N'" & Replace(sur_name, "'", "''") & "', 'S', 'B', GETDATE(), GETDATE(), '" & mytype & "', '" & from_date & "', '" & to_date & "', "&schedule&", '" & cstr(schedule_type) &"', '0', '0', '0', '1', "&uid&", "&temp_id&", '0', '0', '0', '', '0', '0', '1', '0', "&classid&", 0, 0, 0," & campaign_id& "," & approve_status & ")"
end if

'Response.Write(SQL)
Conn.Execute(SQL)
Set rs2 = Conn.Execute("select @@IDENTITY newID from alerts")
alertid=rs2("newID")
rs2.Close


closed = "N"


sur_name = Replace(Replace(sur_name, "<p>", ""), "</p>", "")
if (Request("new")="newSurvey") then
    
    startDateToInsert = from_date
    endDateToInsert = end_date
    
    if(Request("has_lifetime") = "1") then
        startDateToInsert = ""
        endDateToInsert = ""
    end if
    SQL = "INSERT INTO surveys_main (name, create_date, type, group_type, closed, sender_id, template_id, schedule, from_date, to_date, stype) VALUES (N'" & sur_name & "', GETDATE(), '" & mytype & "', '"& group_ad_type &"', '" & closed & "', "&alertid&", "&temp_id&", "&schedule&", '" & startDateToInsert & "', '" & endDateToInsert & "', " & stype & ")"
else 
    SQL = "INSERT INTO surveys_main (name, create_date, type, group_type, closed, sender_id, template_id, schedule, from_date, to_date, stype) VALUES (N'" & Replace(sur_name, "'", "''") & "', GETDATE(), '" & mytype & "', '"& group_ad_type &"', '" & closed & "', "&alertid&", "&temp_id&", "&schedule&", '" & from_date & "', '" & to_date & "', " & Request("stype") & ")"
end if

'Response.Write(SQL)
Conn.Execute(SQL)
Set rs1 = Conn.Execute("select @@IDENTITY newID from surveys_main")
id=rs1("newID")
rs1.Close

if (Request("new")="newSurvey") then
    Set questions=myJSON
    questionNumber=0
    for each question in questions
	    questionNumber=questionNumber+1
	    question_name  = Replace(question.title, "'", "''")
    	
	    SQL = "INSERT INTO surveys_questions (question, survey_id, question_number, question_type) VALUES (N'" & question_name & "', " & id & ", "&questionNumber&", '"&question.type&"')"
	    Conn.Execute(SQL)
	    Set rs1 = Conn.Execute("select @@IDENTITY newID from surveys_questions")
	    q_id=rs1("newID")
	    rs1.Close

	    Set answers=question.options
	    for each answer in answers
		    answer_name  = Replace(answer.value, "'", "''")
            if(answer.correct = "1") then
             answer_correct = 1
            else answer_correct = 0
            end if
            
		    SQL = "INSERT INTO surveys_answers (answer, question_id, correct) VALUES (N'" & answer_name & "', " & q_id & ", " & answer_correct & ")"
		    Conn.Execute(SQL)
	    Next
    Next
else
    Set questions=myJSON.elements
    questionNumber=0
    for each question in questions
	    questionNumber=questionNumber+1
	    question_name  = Replace(question.name, "'", "''")
    	
	    SQL = "INSERT INTO surveys_questions (question, survey_id, question_number, question_type) VALUES (N'" & question_name & "', " & id & ", "&questionNumber&", '"&question.type&"')"
	    Conn.Execute(SQL)
	    Set rs1 = Conn.Execute("select @@IDENTITY newID from surveys_questions")
	    q_id=rs1("newID")
	    rs1.Close

	    Set answers=question.elements
	    for each answer in answers
    	
		    answer_name  = Replace(answer.name, "'", "''")
            if(answer.correct) then
             answer_correct = 1
            else answer_correct = 0
            end if
            
		    SQL = "INSERT INTO surveys_answers (answer, question_id, correct) VALUES (N'" & answer_name & "', " & q_id & ", " & answer_correct & ")"
		    Conn.Execute(SQL)
	    Next
    Next


	if(Request("approveAlertId") <> "") then
		Set RS = Conn.Execute("SELECT id FROM surveys_main WHERE sender_id=" & Request("approveAlertId"))
		Conn.Execute("DELETE FROM alerts WHERE id = " & Request("approveAlertId"))
		if Not RS.EOF then
			Conn.Execute("DELETE FROM surveys_answers_stat WHERE answer_id IN (SELECT id FROM surveys_answers WHERE question_id IN (SELECT id FROM surveys_questions WHERE survey_id=" & RS("id") & "))")

			Conn.Execute("DELETE FROM surveys_answers WHERE question_id IN (SELECT id FROM surveys_questions WHERE survey_id=" & RS("id") & ")")

			Conn.Execute("DELETE FROM surveys_answers WHERE survey_id=" & RS("id"))

			Conn.Execute("DELETE FROM surveys_custom_answers_stat WHERE question_id IN (SELECT id FROM surveys_questions WHERE survey_id=" & RS("id") & ")")

			Conn.Execute("DELETE FROM surveys_main WHERE id=" & RS("id"))

			Conn.Execute("DELETE FROM surveys_questions WHERE survey_id=" & RS("id"))			
		end if
	end if

end if

all_ids_broadcast = ""
if(Request("send_type")="broadcast" AND gid<>0) then 
	Set RSeditor = Conn.Execute("SELECT DISTINCT(users.id) as id FROM users LEFT JOIN policy_user ON users.id=policy_user.user_id LEFT JOIN users_groups ON users.id=users_groups.user_id LEFT JOIN OU_User ON users.id=OU_User.Id_user WHERE role='U' AND ("&policy_ids&" "&group_ids&" "&ou_ids&")")
	Do while not RSeditor.EOF
		user_id=RSeditor("id")
		if all_ids_broadcast <> "" then
			all_ids_broadcast = all_ids_broadcast&","
		end if
		all_ids_broadcast = all_ids_broadcast&user_id
		SQL = "INSERT INTO alerts_sent (user_id, alert_id) VALUES (" & user_id & ", " & alertid & ")"
		Conn2.Execute(SQL)
		SQL = "INSERT INTO alerts_sent_stat (user_id, alert_id, [date]) VALUES (" & user_id & ", " & alertid & ", '"&this_date&"')"
		Conn3.Execute(SQL)
		RSeditor.MoveNext
	loop
end if

if(Request("send_type")="iprange") then
	objects=request("added_user")    
	groups = Split(objects, ", ")
	sendIpRange groups, alertid
end if



	'OUs

			if(gid=0) then
				ous = request("ous_id_sequence")
			else
				ous = replace(request("ed_ous"), "," , "")
			end if
			
			ous_closed = replace(request("ous_id_sequence_closed"), "," , "")
			ous_list=""
			if(ous<>"") then
			'only to selected OUs
				ous_arr=split(ous)
				fl=0
				for each ou_id in ous_arr
				if(ou_id <> "") then
					if(fl=0) then
						ous_list = " Id_OU=" & ou_id
					else
						ous_list = ous_list & " OR Id_OU=" & ou_id
					end if
					fl=1
				end if
				next
			end if
			if(ous_closed<>"") then
			'to selected OUs and all subOUs
				ous_arr=split(ous_closed)
				fl=0
				for each ou_id in ous_arr
					if(ou_id <> "") then
						if(fl=0) then
							ous_list = " Id_OU=" & ou_id
						else
							ous_list = ous_list & " OR Id_OU=" & ou_id
						end if
						fl=1
						'get subOUs and add to list
						Set RSsub=Conn.Execute(";WITH OUsList (Id_child) AS (SELECT Id_child FROM OU_hierarchy WHERE Id_parent="&ou_id&" UNION ALL SELECT c.Id_child From OU_hierarchy as c INNER JOIN OUsList as p ON c.Id_parent = p.Id_child ) SELECT Id_child FROM OUsList")
						Do While Not RSsub.EOF
							ous_list = ous_list & " OR Id_OU=" & RSsub("Id_child")
							RSsub.MoveNext
						loop	
					end if
				next
			end if	
			if(ous_list<>"") then
				'if(send_type="personal") then
					Conn.Execute("INSERT INTO alerts_sent (alert_id, user_id) SELECT "&alertid&" as alert_id, Id_User as user_id FROM OU_User WHERE " & ous_list)
					Conn.Execute("INSERT INTO alerts_sent_stat (alert_id, user_id, [date]) SELECT "&alertid&" as alert_id, Id_User as user_id, '"&this_date&"' as [date] FROM OU_User WHERE " & ous_list)
				'end if
			
				'if(send_type="group") then
					'Conn.Execute("INSERT INTO alerts_sent_group (alert_id, group_id) SELECT "&alertid&" as alert_id, Id_Group as group_id FROM OU_Group WHERE " & ous_list)
				'end if
			
				'if(send_type="comp") then
					'Conn.Execute("INSERT INTO alerts_sent_comp (alert_id, comp_id) SELECT "&alertid&" as alert_id, Id_comp as comp_id FROM OU_comp WHERE " & ous_list)
				'end if	
			end if

		objects=request("added_user")
		objArray = Split(objects, ", ")
		all_ids=""
		For jj = 0 to UBound(objArray)
			obj_id=objArray(jj)
			if(obj_id<>"") then
				obj_type=Left(obj_id,1)
				if(obj_type="u") then
				'user
					user_id=replace(obj_id,"u","")
					if all_ids <> "" then
						all_ids = all_ids&","
					end if
					all_ids = all_ids&user_id
					SQL = "INSERT INTO alerts_sent (user_id, alert_id) VALUES (" & user_id & ", " & alertid & ")"
					Conn2.Execute(SQL)
					SQL = "INSERT INTO alerts_sent_stat (user_id, alert_id, [date]) VALUES (" & user_id & ", " & alertid & ", '"&this_date&"')"
					Conn3.Execute(SQL)
				elseif(obj_type="g") then
				'group
					group_id=replace(obj_id,"g","")
					groupsList = "|"&group_id&"|"
					globalList = ""
					getSubGroups groupsList, group_id, alertid, this_date
					whereList = replace(globalList, "|", " OR users_groups.group_id=")
				elseif(obj_type="c") then
				'computer
					comp_id=replace(obj_id,"c","")
					SQL = "INSERT INTO alerts_sent_comp (comp_id, alert_id) VALUES (" & comp_id & ", " & alertid & ")"
					Conn2.Execute(SQL)
					SQL = "INSERT INTO alerts_comp_stat (comp_id, alert_id, [date]) VALUES (" & comp_id & ", " & alertid & ", '"&this_date&"')"
					Conn3.Execute(SQL)
				end if
			end if
		Next
		
       'MEMCACHE
	'Set cacheMgr = CreateObject("DeskAlerts.CacheManager")
    'cacheMgr.Add(clng(id), now())
    MemCacheUpdate "Add", alertid 
	if (schedule = 0 AND MOBILE_SEND=1) then
		if all_ids_broadcast <> "" then
			all_ids = all_ids_broadcast
		end if
		if all_ids<>"" then
			SQLWhere = " WHERE user_id IN ("&all_ids&")"
		else 
			SQLWhere = ""
		end if
		SQL = "SELECT device_type, device_id FROM user_instances"&SQLWhere
		Set RSDevices = Conn2.Execute(SQL)
		Do while not RSDevices.EOF
			if RSDevices("device_type") = "2" then
				sDeviceId = RSDevices("device_id")
				if sDeviceId <> "" then
					sendAlertGCM sur_name, "", sDeviceId
				end if
			elseif RSDevices("device_type") = "3" then
				sDeviceId = RSDevices("device_id")
				if sDeviceId <> "" then
					sendAlertAPNS sur_name, "", sDeviceId, alertid
				end if
			elseif RSDevices("device_type") = "4" then
				sDeviceId = RSDevices("device_id")
				if sDeviceId <> "" then
					sendAlertWinPhone sur_name, "", sDeviceId, alertid
				end if	
			end if
			RSDevices.MoveNext
		loop
	end if
'----------------------------------------

%>
<table cellpadding="4" cellspacing="4" border="0">
	<tr>
		<td>
			<%=LNG_SURVEY_ADDED_DESC %>!
		</td>
	</tr>
</table>
<%
    
    if(Request("return_page") <> "") then
        Response.Redirect Request("return_page")
    end if
    
    if(campaign_id = "-1") then
%>

<script language="javascript">
    window.top.frames["main"].frames["work_area"].location.href = "surveys.aspx";
</script>

<% 
    else %>

<script language="javascript">
    window.top.frames["main"].frames["work_area"].location.href = "CampaignDetails.aspx?campaignid=<%=campaign_id %>";
</script>

<%   end if 
 end if
else

	Response.Redirect "index.asp"
end if
%>
<img src="images/inv.gif" alt="" width="1" height="100%" border="0">
</div> </td> </tr> </table> </td> </tr> </table> </body> </html><!-- #include file="db_conn_close.asp" -->
