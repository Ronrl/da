﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Surveys.aspx.cs" Inherits="DeskAlertsDotNet.Pages.Surveys" %>

<%@ Import Namespace="DeskAlertsDotNet.Parameters" %>
<%@ Import Namespace="System.Globalization" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="DeskAlerts.Server.Utils" %>
<%@ Import Namespace="DeskAlerts.Server.Utils.Helpers" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>DeskAlerts</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link href="css/style9.css" rel="stylesheet" type="text/css">
    <link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="css/jquery/jquery_timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css">
    <script language="javascript" type="text/javascript" src="functions.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/date.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/shortcut.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/preview.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery_timepicker/jquery-ui-timepicker-addon.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/jquery/datepicker_fix.js"></script>
    <script language="javascript" type="text/javascript">


        $(document).ready(function () {
            shortcut.add("Alt+N", function (e) {
                e.preventDefault();
                location.href = "addSurvey.aspx";
            });

            shortcut.add("delete", function (e) {
                e.preventDefault();

                if ($("#upDelete").length == 0)
                    return;


                if (confirmDelete())
                    __doPostBack('upDelete', '');
            });

            $(".delete_button").button({
            });

            $("#selectAll").click(function () {
                var checked = $(this).prop('checked');
                $(".content_object").prop('checked', checked);
            });

            $(document).tooltip({
                items: "img[title],a[title]",
                position: {
                    my: "center bottom-20",
                    at: "center top",
                    using: function (position, feedback) {
                        $(this).css(position);
                        $("<div>")
                            .addClass("arrow")
                            .addClass(feedback.vertical)
                            .addClass(feedback.horizontal)
                            .appendTo(this);
                    }
                }
            });
        });

        function confirmDelete() {
            if (confirm("Are you sure you want to delete selected items?") == true)
                return true;
            else
                return false;
        }

        $(function () {
            $(".delete_button").button({
            });

            $(".add_alert_button").button({
                icons: {
                    primary: "ui-icon-plusthick"
                }
            });

            $("#linkDirDialog").dialog({
                autoOpen: false,
                height: 80,
                width: 550,
                modal: true,
                resizable: false
            });


        });

        function openDialogUI(_form, _frame, _href, _width, _height, _title) {

            $(function () {
                $("#dialog-" + _form).dialog('option', 'width', _width);
                $("#dialog-" + _form).dialog('option', 'title', _title);
                $("#dialog-" + _frame).attr("src", _href);
                $("#dialog-" + _frame).css("height", _height);
                $("#dialog-" + _frame).css("display", 'block');
                if ($("#dialog-" + _frame).attr("src") != undefined) {
                    $("#dialog-" + _form).dialog('open');
                    $("#dialog-" + _form).contents().find('#table_border').css('border', 'none');
                }
            });
        }
        function htmlEscape(str) {
            return (str || '')
                .replace(/&/g, '&amp;')
                .replace(/"/g, '&quot;')
                .replace(/'/g, '&#39;')
                .replace(/</g, '&lt;')
                .replace(/>/g, '&gt;');
        }

        jQuery.fn.center = function () {
            this.css("position", "absolute");
            this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) +
                $(window).scrollTop()) + "px");
            this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) +
                $(window).scrollLeft()) + "px");
            return this;
        }


        function showPreview(id) {

            $(function () {
                if ($("#preview_div").is(":visible") == true) {
                    closePreview();
                }

                $.get("../get_survey_data.asp?id=" + id, function (data) {

                    var surveyData = JSON.parse(data);

                    getAlertData(surveyData.alert_id, false, function (alertData) {
                        data = JSON.parse(alertData);
                    });

                    $("#preview_id").val(id);
                    // ToDo: Reimplement to GetSurveyFromData.aspx
                    $("#preview_title").val(htmlEscape(parseHtmlTitle(decodeURIComponent(data.alert_html), '<!-- html_title = ', '" -->')));
                    $("#preview_skin_id").val(surveyData.skin_id);
                    $("#create_date").val(surveyData.create_date);
                    $("#alert_width").val(surveyData.alert_width);
                    $("#alert_height").val(surveyData.alert_height);

                    if (surveyData.fullscreen == 1) {
                        $("#fullscreen").val(1);
                    }
                    else {
                        $("#fullscreen").val(0);
                    }

                    $("#position").val(surveyData.position);

                    $("#preview_form").submit();
                    $("#preview_div").draggable();
                    $("#preview_div").fadeIn(1000);

                    setTimeout(function () {
                        $(document).bind("click", closePreview);
                        $(window).bind("click", closePreview);
                        $(window).bind("scroll", closePreview);
                        $(window).bind("resize", closePreview);
                        $("#preview").unbind("click", closePreview);

                    }, 1000);


                    var position = surveyData.fullscreen;
                    $("#preview_div").css('left', '');
                    $("#preview_div").css('top', '');
                    $("#preview_div").css('bottom', '');
                    $("#preview_div").css('right', '');
                    switch (position) {

                        case 2:
                            {
                                $("#preview_div").css('left', '0px');
                                $("#preview_div").css('top', '0px');
                                break;
                            }
                        case 3:
                            {
                                $("#preview_div").css('right', '0px');
                                $("#preview_div").css('top', '0px');
                                break;
                            }
                        case 4:
                            {
                                $("#preview_div").center();

                                break;
                            }
                        case 5:
                            {
                                $("#preview_div").css('left', '0px');
                                $("#preview_div").css('bottom', '0px');
                                break;
                            }
                        case 6:
                            {
                                $("#preview_div").css('right', '0px');
                                $("#preview_div").css('bottom', '0px');
                                break;
                            }
                        default:
                            {
                                $("#preview_div").css('right', '0px');
                                $("#preview_div").css('bottom', '0px');
                                break;
                            }
                    }
                });
            });

        }
        

        function closePreview() {
            $(document).unbind("click", closePreview);
            $(window).unbind("click", closePreview);
            $(window).unbind("scroll", closePreview);
            $(window).unbind("resize", closePreview);
            $("#preview_div").hide();
        }
        $(document).ready(function () {

            var width = $(".data_table").width();
            var pwidth = $(".paginate").width();
            if (width != pwidth) {
                $(".paginate").width("100%");
            }
            $("#preview_div").hide();
            $("#dialog-form").dialog({
                autoOpen: false,
                width: 100,
                modal: true
            });

            $(document).tooltip({
                items: "img[title],a[title]",
                position: {
                    my: "center bottom-20",
                    at: "center top",
                    using: function (position, feedback) {
                        $(this).css(position);
                        $("<div>")
                            .addClass("arrow")
                            .addClass(feedback.vertical)
                            .addClass(feedback.horizontal)
                            .appendTo(this);
                    }
                }
            });


            $('#dialog-frame').on('load', function () {
                $("#dialog-frame").contents().find('.prelink_stats').on('click', function (e) {
                    e.preventDefault();
                    var link = $(this).attr('href');
                    var title = $(this).attr('title');
                    var last_id = $('.dialog_forms').size();
                    var elem = '<div id="dialog-form-' + last_id + '" class="dialog_forms" style="overflow:hidden;display:none;"><iframe id="dialog-frame-' + last_id + '" style="display:none;width:100%;overflow:hidden;border : none;"></iframe></div>';
                    $('#secondary').append(elem);
                    $("#dialog-form-" + last_id).dialog({
                        autoOpen: false,
                        width: 100,
                        modal: true
                    });
                    openDialogUI("form-" + last_id, "frame-" + last_id, link, 350, 350, title);
                });
            });
        });
        $(function () {

            $(".delete_button").button({
            });
            $(".add_survey_button").button({
                icons: {
                    primary: "ui-icon-plusthick"
                }
            });
        });

        function showSurveyPreview(surveyId) {
            $("#preview_form").submit();
            $("#preview_div").fadeIn(1000);
        }
    </script>

</head>
<body>

    <div id="dialog-form" style="overflow: hidden; display: none;">
        <iframe id='dialog-frame' style="width: 100%;  overflow: hidden; border: none; display: none;"></iframe>
    </div>

    <form id="form1" runat="server">
        <table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
            <table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
                <tr>
                    <td width="100%" height="31" class="main_table_title">
                        <img src="images/menu_icons/survey_20.png" alt="" width="20" height="20" border="0" style="padding-left: 7px">
                        <asp:LinkButton href="Surveys.aspx" runat="server" ID="headerTitle" class="header_title" Style="position: absolute; top: 16px" />
                    </td>
                </tr>
                <tr>
                    <td class="main_table_body" height="100%">
                        <br />
                        <div style="margin-left: 9px">
                            <span class="work_header" runat="server" id="workHeader"></span>

                            <table style="padding-left: 10px;" width="100%" border="0">
                                <tbody>
                                    <tr valign="middle">
                                        <td width="175px">
                                            <% = resources.LNG_SEARCH_SURVEY_BY_TITLE %>
                                            <input type="text" id="searchTermBox" runat="server" name="uname" value="" /></td>
                                        <td width="1%">
                                            <br />
                                            <!--  <a name="sub" class="search_button ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" role="button" aria-disabled="false"><span class="ui-button-icon-primary ui-icon ui-icon-search"></span><span class="ui-button-text">Search</span></a> -->
                                            <asp:LinkButton runat="server" class="search_button ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" ID="searchButton" Style="margin-right: 0px" role="button" aria-disabled="false" Text="<span class='ui-button-icon-primary ui-icon ui-icon-search'></span><span class='ui-button-text'>{Search}</span>" />

                                            <input type="hidden" name="search" value="1">
                                        </td>

                                        <td>
                                            <a class="add_alert_button" title="Hotkey: Alt+N" id="addButton" runat="server" style="float: right; margin-right: 7px;" href="EditSurvey.aspx" role="button" aria-disabled="false"><%=resources.LNG_ADD %></a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>
                         <div style="margin-left: 10px" runat="server" id="mainTableDiv">

                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <div style="margin-left: 10px" id="topPaging" runat="server"></div>
                                    </td>
                                </tr>
                            </table>
                            <br>
                            <asp:LinkButton Style="margin-left: 10px" title="Hotkey: Delete" runat="server" ID="upDelete" class='delete_button' OnClientClick="return confirmDelete();" />
                            <br>
                            <br>
                            <asp:Table Style="padding-left: 0px; margin-left: 10px; margin-right: 10px;" runat="server" ID="contentTable" Width="98.5%" Height="100%" CssClass="data_table" CellSpacing="0" CellPadding="3">
                                <asp:TableRow CssClass="data_table_title">
                                    <asp:TableCell runat="server" ID="headerSelectAll" CssClass="table_title" Width="2%">
                                        <asp:CheckBox ID="selectAll" runat="server" />
                                    </asp:TableCell>
                                    <asp:TableCell runat="server" Width="2%" ID="typeCell" CssClass="table_title"></asp:TableCell>
                                    <asp:TableCell runat="server" ID="titleCell" CssClass="table_title"></asp:TableCell>
                                    <asp:TableCell runat="server" ID="creationDateCell" CssClass="table_title"></asp:TableCell>
                                    <asp:TableCell runat="server" Width="15%" ID="answersCountCell" CssClass="table_title"></asp:TableCell>
                                    <asp:TableCell runat="server" ID="scheduledCell" CssClass="table_title"></asp:TableCell>
                                    <asp:TableCell runat="server" ID="approveCell" CssClass="table_title"></asp:TableCell>
                                    <asp:TableCell runat="server" ID="senderCell" CssClass="table_title"></asp:TableCell>
                                    <asp:TableCell runat="server" ID="AnonymousSurveyCell" CssClass="table_title"></asp:TableCell>
                                    <asp:TableCell runat="server" Width="120px" ID="actionsCell" CssClass="table_title"></asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                            <br>
                            <asp:LinkButton Style="margin-left: 10px" title="Hotkey: Delete" runat="server" ID="bottomDelete" class='delete_button' OnClientClick="return confirmDelete();" />
                            <br>
                            <br>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">

                                <tr>
                                    <td>
                                        <div style="margin-left: 10px" id="bottomRanging" runat="server"></div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div runat="server" id="NoRows">
                            <br />
                            <br />
                            <center><b>
                    <%=resources.LNG_THERE_ARE_NO_SURVEYS %>
				        </b></center>
                        </div>
                        <br>
                        <br>
                    </td>
                </tr>

            </table>
        </table>
    </form>
</body>
<form action="../SurveyPreview.aspx" target="preview_frame" id="preview_form" method="post">
    <input type="hidden" name="skin_id" id="preview_skin_id" />
    <input type="hidden" name="id" id="preview_id" />
    <input type="hidden" name="create_date" id="create_date" />
    <input type="hidden" name="title" id="preview_title" />
    <input type="hidden" name="alert_width" id="alert_width" runat="server" />
    <input type="hidden" name="alert_height" id="alert_height" runat="server" />
    <input type="hidden" name="position" id="position" runat="server" />
    <input type="hidden" name="fullscreen" id="fullscreen" runat="server" />
</form>
<div id="preview_div" style="position: fixed; left: 40%; z-index: 1">
    <iframe id="preview_frame" name="preview_frame" width="520" height="420" frameborder="0" src="../SurveyPreview.aspx"></iframe>
</div>
</html>
