﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Net;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using System.Text.RegularExpressions;
using System.Runtime.Serialization.Json;
using System.Web.Script.Serialization;

namespace deskalerts
{

    public partial class SurveyPreview : System.Web.UI.Page
    {
       public Dictionary<string, string> vars = new Dictionary<string,string>();


       public string SurveyData { get; set; }

       protected void Page_PreInit(object sender, EventArgs e)
       {
       }


       protected void Page_LoadComplete(object sender, EventArgs e)
       {
          // contentFrame.InnerHtml = "<b>123123123</b>";
       }
        protected void Page_Load(object sender, EventArgs e)
        {
            ServicePointManager.ServerCertificateValidationCallback = delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
            {
                return true;
            };


            SurveyData = "";
            if (!string.IsNullOrEmpty(Request["data"]))
                SurveyData = Request["data"].Replace("'", "\'");


            //  string skinId = Request["skin_id"];


            //  string skinUrlPath = String.Format("admin/preview_caption.asp?skin_id={0}&caption_href=alertcaption.html&ext_props=[]", skinId);

            //   skinFrame.Attributes.Add("src", skinUrlPath);


            //SurveyData = Request["data"];
            //string surveyId = Request["id"];


            //if (!String.IsNullOrEmpty(SurveyData))
            //{
            //   /* string surveyPreviewUrl = "/GetSurveyFromData.aspx?data=" + SurveyData;
            //    string url = HttpContext.Current.Request.Url.AbsoluteUri;
            //    string path = url.Substring(0, url.IndexOf("SurveyPreview"));

            //    string fullUrl = path + surveyPreviewUrl;
            //    contentFrame.Attributes.Add("src", fullUrl);*/

            //   // inputData.Value = SurveyData;



            //}
            //else if (!String.IsNullOrEmpty(surveyId))
            //{
            //    string surveyPreviewUrl = "/get_survey.asp?id=" + surveyId + "&user_id=-1";
            //    string url = HttpContext.Current.Request.Url.AbsoluteUri;
            //    string path = url.Substring(0, url.IndexOf("SurveyPreview"));

            //    string fullUrl = path + surveyPreviewUrl;
            //    body_frame.Attributes.Add("src", fullUrl);

            //}
            //else
            //{
            //    body_frame.Attributes.Add("innerHTML", "<b>Something goes wrong</b>");
            //    //contentFrame.d
            //}

            /*   string skinFolder = String.Format("admin/skins/{0}/version/", skinId);
            string url = HttpContext.Current.Request.Url.AbsoluteUri;
            string path = url.Substring(0, url.IndexOf("SurveyPreview"));
            string fullSkinUrl = path + "/" + skinUrlPath;
            WebRequest request = WebRequest.Create(fullSkinUrl);
            WebResponse response = request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());

            string skinHTML = reader.ReadToEnd();

            Regex rg = new Regex("<img.*?src=\"(.*?)\"", RegexOptions.IgnoreCase);

            MatchCollection images = rg.Matches(skinHTML);

            System.Collections.ArrayList replacedImages = new System.Collections.ArrayList();

         //   Response.Write(skinHTML);
          //  Response.Write("COUNT = " + images.Count);
            foreach (Match match in images)
            {
                int start = match.Groups[1].Index;
                int length =  match.Groups[1].Length;

              //  Response.Write("INDEX = " + start + " LENGTH = " + match.Length + "\n");
                //Response.Write("Value = " + skinHTML.Substring(start, match.Length));
                string oldValue = match.Groups[1].Value;
                string newValue = skinFolder + match.Groups[1].Value;

                if (!replacedImages.Contains(oldValue))
                {
                    skinHTML = skinHTML.Replace(oldValue, newValue);
                    replacedImages.Add(oldValue);
                }
            }

            Response.Write(skinHTML);

          //  skin.InnerHtml = skinHTML;

            */

        }



        public Dictionary<string, string> GetAspVariables(params string[] keys)
        {
            string queryString = String.Join(":", keys);

            string url = HttpContext.Current.Request.Url.AbsoluteUri;
            string path = url.Substring(0, url.IndexOf("addSurvey"));
            string url_bridge = path + "/dotnetbridge.asp?var=" + queryString;
            WebRequest request = WebRequest.Create(url_bridge);
            WebResponse response = request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
           // Response.Write(reader.ReadToEnd());
            string[] variablesStrings = reader.ReadToEnd().Split('&');
            

            Dictionary<string, string> result = new Dictionary<string, string>();
            foreach (string keyVal in variablesStrings)
            {

                string[] keyValArray = keyVal.Split(':');

                if (keyValArray.Length != 2)
                    continue;

                string key = keyValArray[0];
                string value = keyValArray[1];

                result.Add(key, value);
            }

            return result;

            
        }
        public string GetAspVariable(string variableName)
        {
            string url = HttpContext.Current.Request.Url.AbsoluteUri;
            string path = url.Substring(0, url.IndexOf("addSurvey"));
            string url_bridge = path + "/dotnetbridge.asp?var="+variableName;
            WebRequest request = WebRequest.Create(url_bridge);
            WebResponse response = request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            string variable = reader.ReadToEnd();
            return variable;
        }
    }
}
