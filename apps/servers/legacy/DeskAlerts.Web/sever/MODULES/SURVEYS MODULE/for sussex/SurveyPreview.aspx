﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SurveyPreview.aspx.cs" Inherits="deskalerts.SurveyPreview" validateRequest="false"  %>
  
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

	<head>
		<title>DeskAlerts</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css" />
		<link href="css/style9.css" rel="stylesheet" type="text/css">
		<link href="css/jquery/jquery_timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css">
		<script language="javascript" type="text/javascript" src="admin/jscripts/jquery/jquery.min.js"></script>
		<script language="javascript" type="text/javascrbody_frameipt" src="admin/jscripts/jquery/jquery-ui.min.js"></script>
		<!--[if IE 6]>
		<script language="javascript" type="text/javascript" src="jscripts/DD_belatedPNG.js"></script>
		<script language="javascript" type="text/javascript">DD_belatedPNG.fix('.ui-icon');</script>
		<![endif]-->
		<script language="javascript" type="text/javascript" src="admin/jscripts/jquery/jquery-migrate-1.2.1.min.js"></script>
		<script type="text/JavaScript" language="JavaScript" src="admin/jscripts/json2.js"></script>
		<script language="javascript" type="text/javascript" src="admin/jscripts/preview.js"></script>
		<script language="javascript" type="text/javascript" src="admin/functions.asp"></script>
		<script language="javascript" type="text/javascript" src="admin/jscripts/date.js"></script>
	 
		<script language="javascript" type="text/javascript" src="admin/jscripts/jquery/jquery_timepicker/jquery-ui-timepicker-addon.js"></script>
		<script language="javascript" type="text/javascript" src="admin/jscripts/jquery/datepicker_fix.js"></script>
		
		
	    <script language="javascript" type="text/javascript" src="admin/jscripts/tiny_mce/tiny_mce_new.js"></script>

		<!--<script language="javascript" type="text/javascript" src="admin/jscripts/da_tiny_mce_new.js"></script>-->
    
    
		<style rel="stylesheet" type="text/css">  
		.tile
		{
			margin: 10px; 
			padding: 10px;
			width: 200px;
		}
		
		#skins_tiles_dialog .tile
		{
			display: inline-block;
		}
		#default
		{
			margin-top:0px;
		}
		 .save_and_next_button
		 {
			float:right;
		 }        
		</style>
		<form id="contentForm" method="post" target="body_frame" action="GetSurveyFromData.aspx">
		    <input  type="hidden" name="data" id="inputData" />
		     <input  type="hidden" name="data" id="survey_id" />
		</form>
		
		<form id="skinForm" method="post" target="caption_frame" action="admin/preview_caption.asp">
		    <input type="hidden" name="skin_id" id="skin_id" />
		    <input type="hidden" name="ext_props" id="ext_props" />
		    <input type="hidden" name="caption_href" value="alertcaption.html" />
		
		</form>
		<iframe id="caption_frame" name="caption_frame" runat="server" style='position:absolute; border:0px;  margin:0px;' width='500' height='400' src="admin/preview_caption.asp"></iframe>
		<iframe id="body_frame" name="body_frame" src="GetSurveyFromData.aspx" runat=server width='488' height='240' style='position:relative; top:137px; left:10px; right:0; border:2px'></iframe>
		


		<script language="javascript" type="text/javascript">

		    $(document).ready(function() {
		        var data = "<% Response.Write(SurveyData); %>";

		        //  alert(data);
		        var id = parseInt('<% Response.Write(Request["id"]); %>', 10);
		        
		        // alert(id);
		        var skin_id = '<% Response.Write(Request["skin_id"]); %>';
		        var title = '<% Response.Write((Request["title"])); %>';
				
		        var create_date = '<% Response.Write(Request["create_date"]); %>';

		        var alert_width = parseInt('<% Response.Write(Request["alert_width"]); %>');
		        var alert_height = parseInt('<% Response.Write(Request["alert_height"]); %>');
		        var position = parseInt('<% Response.Write(Request["position"]); %>');
		        var fullscreen = parseInt('<% Response.Write(Request["fullscreen"]); %>');
		        title = title.replace(/"/g, '\\\"');

		        var ext_props = '{ "isHtml": {"value":"1", "cnst": 0 }, "alert_date_mode":{ "value": "1", "cnst": 0 }, "creation_text":{ "value": "Creation date:", "cnst" : 1}, "datetime_format":{ "value": "%s", "cnst":1 }, "js_datetime_format":{ "value": "", "cnst":1 }, "create_date":{ "value": "' + create_date + '", "cnst" : 0 }, "alert_header":{ "value": "New Alert!", "cnst" : 1 }, "title":{ "value": "' + title + '", "cnst" : 0}}';



		        if (fullscreen == 1) {
		            alert_width = $(window.parent).width();
		            alert_height = $(window.parent).height();
		        }
		        if (!isNaN(alert_width) && !isNaN(alert_height)) {

		            var parentFrame = window.parent.document.getElementById("preview_frame");
		            var parentDiv = window.parent.document.getElementById("preview_div");
		            var bodyFrame = $("#body_frame");
		            var skinFrame = $("#caption_frame");

		            $(parentFrame).attr('width', (alert_width + 20).toString());
		            $(parentFrame).attr('height', (alert_height + 20).toString());

		            $(parentDiv).css('width', (alert_width).toString() + 'px');
		            $(parentDiv).css('height', (alert_height).toString() + 'px');

		            $(bodyFrame).attr('width', (alert_width - 20).toString());
		            $(bodyFrame).attr('height', (alert_height - 200).toString());

		            $(skinFrame).attr('width', (alert_width).toString());
		            $(skinFrame).attr('height', (alert_height).toString());
		        }

		        if (skin_id == "{C28F6977-3254-418C-B96E-BDA43CE94FF4}") {
		            $("#caption_frame").attr("height", "450");
		            var parentFrame = window.parent.document.getElementById("preview_frame");
		            $(parentFrame).attr("height", "450");

		        }
		        $("#skin_id").val(skin_id);
		        $("#ext_props").val(ext_props);
		        $("#skinForm").submit();



		        if (!isNaN(id)) {

		           
		            $("#contentForm").get(0).setAttribute("action", "get_survey.asp?id=" + id + "&user_id=-1");
		            $("#survey_id").val(id);
		            $("#body_frame").css("top", "130px");
		            $("#body_frame").attr("height", (alert_height - 230).toString());
		            $("#inputData").remove();
		        }
		        else {

		           
		            $("#survey_id").remove();
		            $("#inputData").val(data);
		        }
		        if (skin_id == "{04F84380-B31D-4167-8B83-C6642E3D25A8}") {
		            $("#body_frame").css("left", "17px");
		            $("#body_frame").attr("width", "467");
		        }


		        $("#contentForm").submit();
		    });

		    function close() {
		        window.parent.closePreview();
		    }
		</script>
		
	
	

	</head>

	

	<body style="margin:0" class="body">
	       <div id="skin" runat="server">
	       </div>
	</body>
</html>