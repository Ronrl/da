﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Net;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using System.Text.RegularExpressions;
using System.Runtime.Serialization.Json;
using System.Web.Script.Serialization;


namespace deskalerts
{

    public class Question
    {
        public int id;
        public string type;
        public string title;
        public Answer[] options;

    }

    public class Answer
    {
        public string value;
        public int correct;
    }
    public partial class GetSurveyFromData : System.Web.UI.Page
    {
        public Question[] questions;
        
       protected void Page_PreInit(object sender, EventArgs e)
       {
       }


       protected void Page_LoadComplete(object sender, EventArgs e)
       {
          // contentFrame.InnerHtml = "<b>123123123</b>";
       }

       string GetHtmlForQuestion(int number)
       {
           if (number >= questions.Length)
               return string.Empty;

           Question question = questions[number];


           string type = question.type.Equals("C") ? "M" : question.type;

          
           string templateHTML = File.ReadAllText(Server.MapPath("admin/templates/" + type + ".html"));


           string answersString = "";
           Answer[] answers = question.options;

           foreach (Answer answer in answers)
           {
               switch (type)
               {


                   case "M":
                       {
                          answersString += string.Format("<tr><td><input name='answer' type=\"radio\" ></td><td>{0}</td></tr>", answer.value);
                           break;
                       }
                   case "N":
                       {
                           answersString += string.Format("<tr><td><input type=\"checkbox\" ></td><td>{0}</td></tr>", answer.value);
                           break;
                       }
                   case "I":
                       {
                          
                           break;
                       }
                   case "T":
                       {
                           answersString += string.Format("<tr><td colspan='2'><div align='center' style='width:90%; overflow:hidden; word-wrap: break-word;'>{0}</div></td></tr><tr><td colspan='2'><div align='center'><input type='text'></input></div></td></tr>", answer.value);
                           break;
                       }

               }
           }

           string questionHtml = templateHTML.Replace("{ANSWERS}", answersString)
                                             .Replace("{TITLE}", question.title)
                                             .Replace("{NUMBER}", (number + 1).ToString())
                                             .Replace("{DATA}", Request["data"].Replace("'", "\'"))
                                             .Replace("{END}", number == questions.Length - 1 ? "1" : "0");


           return questionHtml;
       }
        protected void Page_Load(object sender, EventArgs e)
        {
            ServicePointManager.ServerCertificateValidationCallback = delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
            {
                return true;
            };


            if (!String.IsNullOrEmpty(Request["shouldEnd"]) && Request["shouldEnd"].Equals("1"))
            {
                Response.Write("<body bgcolor=\"white\">");
                 Response.Write(" <center>Your survey was submitted.<br><br><br><img src=\"admin/images/langs/en/close_button.gif\" alt=\"Close\" border=\"0\" onclick=\"window.parent.close();\"></center>");
                 Response.Write("</body>");
                return;
            }


            string surveyDataString = HttpUtility.UrlDecode(Request["data"]).Replace("'", "\'");
            

            if (!String.IsNullOrEmpty(surveyDataString))
            {
                questions = new JavaScriptSerializer().Deserialize<Question[]>(surveyDataString);

                int number = string.IsNullOrEmpty(Request["num"]) ? 0 : Convert.ToInt32(Request["num"]);

                Response.Write(GetHtmlForQuestion(number));

            }

        }



        public Dictionary<string, string> GetAspVariables(params string[] keys)
        {
            string queryString = String.Join(":", keys);

            string url = HttpContext.Current.Request.Url.AbsoluteUri;
            string path = url.Substring(0, url.IndexOf("addSurvey"));
            string url_bridge = path + "/dotnetbridge.asp?var=" + queryString;
            WebRequest request = WebRequest.Create(url_bridge);
            WebResponse response = request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
           // Response.Write(reader.ReadToEnd());
            string[] variablesStrings = reader.ReadToEnd().Split('&');
            

            Dictionary<string, string> result = new Dictionary<string, string>();
            foreach (string keyVal in variablesStrings)
            {

                string[] keyValArray = keyVal.Split(':');

                if (keyValArray.Length != 2)
                    continue;

                string key = keyValArray[0];
                string value = keyValArray[1];

                result.Add(key, value);
            }

            return result;

            
        }
        public string GetAspVariable(string variableName)
        {
            string url = HttpContext.Current.Request.Url.AbsoluteUri;
            string path = url.Substring(0, url.IndexOf("addSurvey"));
            string url_bridge = path + "/dotnetbridge.asp?var="+variableName;
            WebRequest request = WebRequest.Create(url_bridge);
            WebResponse response = request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            string variable = reader.ReadToEnd();
            return variable;
        }
    }
}
