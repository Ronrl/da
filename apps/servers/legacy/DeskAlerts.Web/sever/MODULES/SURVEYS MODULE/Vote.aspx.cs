﻿namespace DeskAlertsDotNet.Pages
{
    using System;
    using DeskAlertsDotNet.Parameters;
    using Resources;
    using System.Web.UI;
    using NLog;
    using DeskAlertsDotNet.DataBase;

    public partial class Vote : Page
    {
        private Logger _logger = LogManager.GetCurrentClassLogger();

        protected override void OnLoad(EventArgs e)
        {
            using (var dbManager = new DBManager())
            {
                try
                {
                    base.OnLoad(e);
                    string deskbarId = Request["deskbar_id"];
                    int surveyId = Request.GetParam("survey_id", 0);
                    int questionId = Request.GetParam("question_id", 0);
                    int userId = Request.GetParam("user_id", 0);
                    int countSurveys = dbManager.GetDataByQuery("SELECT * FROM surveys_main WHERE id = " + surveyId).Count;
                    if (surveyId == 0 || questionId == 0 || userId == 0 || countSurveys == 0)
                    {
                        Response.Write("<body>");
                        Response.Write(
                            " <center>Not available<br><br><br><img src=\"admin/images/langs/en/close_button.gif\" alt=\"Close\" border=\"0\" onclick=\"window.external.close();\"></center>");
                        Response.Write("</body>");
                        Response.End();
                        return;
                    }

                    string[] answers = { };

                    if (!string.IsNullOrEmpty(Request["answer"]))
                    {
                        answers = Request["answer"].Split(',');
                    }
                    else if (!string.IsNullOrEmpty(Request["answer_finish"]))
                    {
                        answers = Request["answer_finish"].Split(',');
                    }
                    else if (!string.IsNullOrEmpty(Request["answer_redirect"]))
                    {
                        answers = Request["answer_redirect"].Split(',');
                    }

                    var question = dbManager.GetDataByQuery("SELECT * FROM surveys_questions WHERE id = " + questionId).First();
                    var survey = dbManager.GetDataByQuery("SELECT * FROM surveys_main WHERE id = " + surveyId).First();

                    if (userId == -1)
                    {
                        return;
                    }

                    if (survey.GetString("closed") == "N")
                    {
                        int alertId = survey.GetInt("sender_id");

                        // ToDo: Refactor this shit
                        dbManager.ExecuteQuery($"BEGIN TRAN; UPDATE alerts_received SET vote=1 WHERE alert_id= {alertId} AND user_id = {userId}; COMMIT;");

                        string questionType = question.GetString("question_type");
                        if (questionType == "I" || questionType == "S" || questionType == "T")
                        {
                            string checkQuery = "SELECT id FROM surveys_custom_answers_stat  WHERE question_id = " +
                                questionId + " AND user_id = " + userId;

                            if (Settings.Content["ConfEnableMultiVote"] == "1")
                            {
                                checkQuery += " AND  (clsid='" + deskbarId + "' OR clsid='')";
                            }

                            var checkSet = dbManager.GetDataByQuery(checkQuery);
                            if (checkSet.IsEmpty || userId == -1)
                            {

                                if (questionType == "I" || questionType == "S")
                                {
                                    string customAnswer = string.IsNullOrEmpty(Request["custom_answer"]) ? string.Empty : Request["custom_answer"];

                                    dbManager.ExecuteQuery(
                                        "INSERT INTO surveys_custom_answers_stat (user_id, question_id, date, clsid, answer) VALUES (" +
                                        userId + ", " + questionId + ", GETDATE(), '" + deskbarId + "', N'" + customAnswer.Replace("'", "''") +
                                        "')");

                                }
                                else if (questionType == "T")
                                {

                                    string[] customAnswers = Request["custom_answers"].Split(',');

                                    foreach (var customAnswer in customAnswers)
                                    {
                                        dbManager.ExecuteQuery("INSERT INTO surveys_custom_answers_stat (user_id, question_id, date, clsid, answer) VALUES (" +
                                        userId + ", " + questionId + ", GETDATE(), '" + deskbarId + "', N'" + customAnswer.Replace("'", "''") +
                                        "')");
                                    }
                                }
                            }
                        }
                        else
                        {
                            string checkQuery =
                                "SELECT  a.id FROM surveys_answers_stat as a INNER JOIN surveys_answers as b ON a.answer_id = b.id WHERE b.question_id = " +
                                questionId + " AND user_id = " + userId;

                            if (Settings.Content["ConfEnableMultiVote"] == "1")
                            {
                                checkQuery += " AND  (clsid='" + deskbarId + "' OR clsid='')";
                            }
                            var checkSet = dbManager.GetDataByQuery(checkQuery);


                            if (checkSet.IsEmpty || userId == -1)
                            {
                                foreach (var answer in answers)
                                {
                                    dbManager.ExecuteQuery(
                                        "INSERT INTO surveys_answers_stat (user_id, answer_id, date, clsid) VALUES (" + userId +
                                        ", " + answer + ", GETDATE(), '" + deskbarId + "')");

                                }
                            }
                        }

                    }
                    else
                    {
                        Response.Write("<body bgcolor=\"white\">");
                        Response.Write("<br> <center>" + resources.LNG_VOTE_SORRY2 + "</center>");
                        Response.Write("</body>");
                    }
                }
                catch (Exception ex)
                {
                    _logger.Debug(ex);
                    throw ex;
                }
            }
        }
        
    }
}