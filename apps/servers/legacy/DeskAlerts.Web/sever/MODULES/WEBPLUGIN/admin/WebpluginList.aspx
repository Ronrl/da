﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WebpluginList.aspx.cs" Inherits="deskalerts.WebpluginList" %>
            
    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
    <html>

    <head>
        <title>DeskAlerts</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css" />
        <link href="css/style9.css" rel="stylesheet" type="text/css">
       <link href="css/jquery/jquery_timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css">
        <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
        <script type="text/JavaScript" language="JavaScript" src="jscripts/json2.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/preview.js?v=9.2.1.31"></script>
        <script language="javascript" type="text/javascript" src="functions.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/date.js"></script>
     
	    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery_timepicker/jquery-ui-timepicker-addon.js"></script>
	    <script language="javascript" type="text/javascript" src="jscripts/jquery/datepicker_fix.js"></script>
        <style rel="stylesheet" type="text/css">
            /*.ui-dialog-titlebar-close {
  visibility: hidden;
}*/
            
            .style1 {
                height: 26px;
            }
            
            .style4
            {
                height: 26px;
                width: 643px;
            }
            .date_time_param_input{
	        width:150px
            }

            .style5
            {
                height: 26px;
                width: 208px;
            }

            .style6
            {
                height: 26px;
                width: 203px;
            }

        </style>
    </head>

    <script language="javascript" type="text/javascript">
    
 
    

        
        function openDialogUI(_form, _frame, _href, _width, _height, _title) {
            $("#dialog-" + _form).dialog('option', 'height', _height);
            $("#dialog-" + _form).dialog('option', 'width', _width);
            $("#dialog-" + _form).dialog('option', 'title', _title);
            $("#dialog-" + _frame).attr("src", _href);
            $("#dialog-" + _frame).css("height", _height);
            $("#dialog-" + _frame).css("display", 'block');
            if ($("#dialog-" + _frame).attr("src") != undefined) {
                $("#dialog-" + _form).dialog('open');
                $("#dialog-" + _form).contents().find('#table_border').css('border', 'none');
            }
        }


    
       
        $(document).ready(function() {
            $("#confirm_sending").dialog({
                autoOpen: false,
                height: 100,
                width: 300,
                modal: false,
                position: {
                    my: "center top",
                    at: "center top",
                    of: $('#parent_for_confirm')
                },
                hide: {
                    effect: 'fade',
                    duration: 1000
                },
                //show: { effect: "highlight", color: '#98FB98', duration: 2000 },
                open: function(event, ui) {
                    $(".ui-dialog-titlebar-close", ui.dialog || ui).hide();
                    setTimeout(function() {
                        $("#confirm_sending").dialog('close');
                    }, 3000);
                },
                closeOnEscape: false
            });

            $("#dialog-form").dialog({
                autoOpen: false,
                height: 100,
                width: 100,
                modal: true
            });


            var is_sended = $("#is_sended").val();
            if (is_sended == 'true') {
                if ($('#reschedule').val() == '1') {
                    $('#anchor_confirm').text('=LNG_CONFIRM_SCHEDULE ');
                } else {
                    $('#anchor_confirm').text('=LNG_CONFIRM_SEND ');
                }
                $("#last_alert").effect('highlight', {
                    color: '#98FB98'
                }, 2000);
                $("#confirm_sending").dialog('open');
            }

            var width = $(".data_table").width();

            var pwidth = $(".paginate").width();
            if (width != pwidth) {
                $(".paginate").width("100%");
            }




            $("#dialog-frame").load(function() {
                $("#dialog-frame").contents().find('.prelink_stats').on('click', function(e) {
                    e.preventDefault();
                    var link = $(this).attr('href');
                    var title = $(this).attr('title');
                    var last_id = $('.dialog_forms').size();
                    var elem = '<div id="dialog-form-' + last_id + '" class="dialog_forms" style="overflow:hidden;display:none;"><iframe id="dialog-frame-' + last_id + '" style="display:none;width:100%;height:auto;overflow:hidden;border : none;"></iframe></div>';
                    $('#secondary').append(elem);
                    $("#dialog-form-" + last_id).dialog({
                        autoOpen: false,
                        height: 100,
                        width: 100,
                        modal: true
                    });
                    openDialogUI("form-" + last_id, "frame-" + last_id, link, 350, 350, title);
                });
            });

        });

        function openDialog(mode) {
           
           var dialogId = "";
           var textBoxId = "";

            switch(mode)
            {
                case 1:
                {
                    dialogId = "#duplicateDialog";
                    textBoxId = "#dupName"; 
                    break;
                }
                case 2:
                {
                    dialogId = "#linkDialog";
                    textBoxId = "#linkValue"; 
                    $("#schedule").prop("checked", false);
                    $("#start_date_and_time").prop('disabled', true);
                    $
                    break;
                }
            }
            $(textBoxId).focus(function() {
                $(textBoxId).select().mouseup(function(e) {
                    e.preventDefault();
                    $(this).unbind("mouseup");
                });
            });
            $(dialogId).dialog('open');

        }

        function deleteRow(id) {
            $.ajax({
                url: "set_settings.asp?name=DeleteBanner&value=" + id
            }).done(function(data, textStatus, jqXHR) {
                $("#row" + id).remove();
            });
            

        }

        $(function() {
            $(".delete_button").button({});

            $(".add_alert_button").button({
                icons: {
                    primary: "ui-icon-plusthick"
                }
            });

            $("#linkDialog").dialog({
                autoOpen: false,
                height: 125,
                width: 550,
                modal: true,
                resizable: false
            });
            
            $("#rescheduleDialog").dialog({
                autoOpen: false,
                height: 130,
                width: 200,
                modal: true,
                resizable: false
            });
            $("#duplicateDialog").dialog({
                autoOpen: false,
                height: 110,
                width: 550,
                modal: true,
                resizable: false
            });

        })
        
            function encode_utf8( s )
             {
                 return  encodeURIComponent( s );
            }
            
        /*function addBanner()
        {
           var name = $("#linkValue").val();
           var date = "NULL";
            if($("schedule").prop('checked'))
            {
                date = $("#start_date_and_time").val();
            }

            name = name.replace("#", "%23");
            name = name.replace("&", "%26");

           $.get("set_settings.asp?name=AddRow&value="+encode_utf8(name)+"&date="+date, function(data) {
           
            if(isNaN(parseInt(data, 10)))
            {
                alert(data);
            }
            else
            {
                location.replace("banner_center.aspx?bannerid=" + data);
            }
           
            } );
        }*/
        
       
        function duplicateDialogFunc(id, name, event)
        {
        
           $("#dupName").focus(function() {
                $("#dupName").select().mouseup(function(e) {
                    e.preventDefault();
                    $(this).unbind("mouseup");
                });
            });
              
            $("#dupName").val("Copy of " + name);
            $("#dupCampId").val(id);
            

            $("#duplicateDialog").dialog('open');
            
        }
        
        function duplicateCampaign(event)
        {
        
           var dup_name  = $("#dupName").val();
           var dup_id =  $("#dupCampId").val();
            
          //  $("#duplicateDialog").dialog('close');
          //  $.get("set_settings.asp?name=AddRow&value="+encode_utf8(name), function(data) { location.reload()} );
           $.get("set_settings.asp?name=DupBanners&id=" + dup_id + "&bname=" + encode_utf8(dup_name), function(data) { 
             
            if(data == "schedule")
             alert('<% Response.Write(vars["LNG_STOP_SCHEDULE_ALERTS"]); %>');   
                      
             document.location.reload();
             
           }); 
           
        }
        
        
        
    </script>

    <body style="margin:0" class="body">
     <!--   <form id="form1" runat="server"> -->
            <div id="dialog-form" style="overflow:hidden;display:none;">
                <iframe id='dialog-frame' style="display:none;width:100%;height:auto;overflow:hidden;border : none;">

                </iframe>
            </div>
            <div id="confirm_sending" style="overflow:hidden;text-align:center;">
                <a id="anchor_confirm"></a>
            </div>
            <input type="hidden" id="is_sended" value="=Request(" is_sended ") " />

            <div id="secondary" style="overflow:hidden;display:none;">
            </div>
            <table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
                <tr>
                    <td>
                        <table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
                            <tr>
								<td width="100%" height="31" class="main_table_title">
									<img src="images/menu_icons/web_alert.png" alt="" width="20" height="20" border="0" style="padding-left:7px">
									<a href="WebpluginList.aspx" class="header_title" style="position:absolute;top:15px">
                                        <asp:Label ID="webpluginLabel" runat="server" Text="Label"></asp:Label>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                            
                                <td height="100%" valign="top" class="main_table_body">
                                    <div style="margin:10px" id="parent_for_confirm">
                                        <span class="work_header"><asp:Label ID="webpluginDescription" runat="server" Text="Label"></asp:Label> </span>

                                        <br /><a class="add_alert_button"  href="settings_webplugin_codegenerator.asp" style="text-align:right; float:right">
                                            <asp:Label ID="webpluginAddLabel" runat="server" Text="Label" ></asp:Label>
                                        </a>



                                      <!-- <table width="100%">
                                            <tr>
                                                <td align="left"> -->
                                                <%
                                                    if(!noRows)
                                                     Response.Write(MakePages());            
                                                    else
                                                        Response.Write("<br><br><br><center><b>" + vars["LNG_CAMPAIGN_TUTOR_2"] + "</b><br><br><br></center>");

                                                %>
                                                
                                                <% if(!noRows) { %>
                                                    <br /><a href='#' class='delete_button' onclick="javascript: return LINKCLICK('banners');">
                                                        <asp:Label ID="deleteLabel" runat="server" Text="Label"></asp:Label>
                                                    </a>
                                               <% } %>
                                                    <br />
                                                    <br />
                                                    <br />
                                        <!--          </td>
                                            </tr>
                                        </table> -->
                                
                              <% if (!noRows)
                                 { %>          
                              <form action='delete_objects.asp' name='delete_objs' id='delete_objs' method='post'>
                                    <input type='hidden' name='objType' value='banners' />
                                        <table width="100%" height="100%" cellspacing="0" cellpadding="3" class="data_table" runat="server" id="contentTable">
                                            <tr class="data_table_title">
                                                <td width="10">
                                                    <input type='checkbox' name='select_all' id='select_all' onclick='javascript: select_all_objects();' />
                                                </td>
                                                <td class="style4" align="center">
                                                <% Response.Write("<a href='webpluginlist.aspx?order=" + (order.ToUpper() == "DESC" ? "ASC" : "DESC") + "&collumn=name'"); %>
                                                    <asp:Label ID="bannerNameCollumn" runat="server" Text="Banner's name"></asp:Label>
                                                 <% Response.Write("</a>"); %>
                                                </td>
                                                <td class="style5" align="center">
                                                <% Response.Write("<a href='webpluginlist.aspx?order=" + (order.ToUpper() == "DESC" ? "ASC" : "DESC") + "&collumn=date'" ); %>
                                                    <asp:Label ID="webpluginCreationDate" runat="server" Text="Creation date"></asp:Label>
                                                <% Response.Write("</a>"); %>
                                                </td>
                                                <td class="style1" width="120" align="center">
                                                    <asp:Label ID="webpluginActions" runat="server" Text="Actions"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                           

                                        <img src="images/inv.gif" alt="" width="1" height="100%" border="0">
                                         <input type='hidden' name='back' value='banners' />
                                   </form>
                                   <% } %>
                                   
                                   <% if (!noRows) { %>
                                    <br /><a href='#' class='delete_button' onclick="javascript: return LINKCLICK('banners');">
                                       <asp:Label ID="deleteLabelButtom" runat="server" Text="Label"></asp:Label> </a>
                                       <%
                                       Response.Write(MakePages());
                                      }
                                       %>
                                    </div>
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>
            </table>
            

            <div runat="server" id="linkDialog" style="display:none">
                <label id="bannerNameLabel" for="linkValue"><% Response.Write(vars["LNG_BANNER_NAME"]+ ":");  %></label>
                <input  runat="server" id="linkValue" type="text" style="width:500px" /><br />
                <br />
                <br />
                
                <a href='#' class="add_alert_button" style=" float:right;" onclick="javascript:addBanner();">
                  <% Response.Write(vars["LNG_CREATE"]); %>                 </a>
             </div>
             <script language=javascript>
                 $("#schedule").click(function()
                {
                // alert('click');
                 $("#start_date_and_time").prop('disabled', !this.checked);
                 });
                 
         function initDateTimePicker(element,settingsDateFormat,settingsTimeFormat)
         {
           //  alert(element.length);
	            (function($)
	     {
		    if (element.length > 0)
		    {
			    var dateFormat = parseDateFormat(settingsDateFormat);
			  //  alert(dateFormat);
			    $(element).datetimepicker({
			    	dateFormat: dateFormat,
			    	timeFormat: (settingsTimeFormat == "2") ? "hh:mm TT" : "HH:mm"
			    });
		    }
	        })(jQuery);
            }
               
                initDateTimePicker( $("#reschedulePicker"), '<% Response.Write(GetAspVariable("GetDateFormat()")); %>', '<% Response.Write(GetAspVariable("GetTimeFormat()")); %>');

            
            
            function rescheduleCampaign()
            {
                var newdate = $("#reschedulePicker").val();
                var id = $("#reCampId").val();
                $.get("set_settings.asp?name=ReCampaign&id=" + id +"&date=" + newdate, function(data) { location.reload(); });
            }
            
            function openReDialog(date, name, id)
            {
                if(date == "")
                {
                    date = '<% =DateTime.Now.ToString("dd/MM/yyyy HH:mm") %>';
                }
                    
                $("#reCampId").val(id);
                $("#reschedulePicker").val(date);
                
                name = '<% Response.Write(vars["LNG_RESCHEDULE_CAMPAIGNS"]); %> ' + name;

    
                $("#rescheduleDialog").attr('title', name);
                $("#rescheduleDialog").dialog('open');
                $("#reschedulePicker").blur();
            }
             </script>
           <div runat="server" id="duplicateDialog" style="display:none">
                <input  runat="server" id="dupName" type="text" style="width:500px" />
                <input type="hidden" id="dupCampId" />
                <br />
                <br />
                <a href='javascript:duplicateCampaign(event)'  class="add_alert_button" onclick="">
                   OK                  </a>
             </div>
             
             <div runat="server" id="rescheduleDialog" style="display:none">
                  
                  <input  id="reschedulePicker" name="reschedulePicker" class="date_time_param_input" type="text" value="<% Response.Write(DateTime.Now.ToString("dd/MM/yyyy HH:mm TT")); %>" size="20"/>
                   <input name='reschedulePicker_time' id="reschedulePicker_time" type="hidden"/> 
                  <input  type="hidden" id="reCampId" />
                  <br />
                  <br />
                  <a  href='javascript:rescheduleCampaign()'  class="add_alert_button" onclick="">
                   OK  </a>
                   
             </div>
            
      <!--  </form> -->
    </body>