﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Net;
using System.IO;

namespace deskalerts
{

    public partial class WebpluginList : System.Web.UI.Page
    {
        public int offset;
        public int limit;
        public string sortBy;
        public int count;
       public int Offset { get { return offset; } }
       public int Limit { get { return limit; } }
       public string SortBy { get { return sortBy; } }
       public int Count { get { return count; } }
       public Dictionary<string, string> vars = new Dictionary<string,string>();
          public string order;

          public bool noRows;


       public  string page ;
       public string name;
       string orderCollumn;
       public string Name { get { return name; } }


       protected void Page_PreInit(object sender, EventArgs e)
       {
           
           order = !string.IsNullOrEmpty(Request.Params["order"]) ? Request.Params["order"] : "DESC";
           orderCollumn = !string.IsNullOrEmpty(Request.Params["collumn"]) ? Request.Params["collumn"] : "date";
          offset = !string.IsNullOrEmpty(Request.Params["offset"]) ? Convert.ToInt32(Request.Params["offset"]) : 0;
          limit = !string.IsNullOrEmpty(Request.Params["limit"]) ? Convert.ToInt32(Request.Params["limit"]) : 25;
          sortBy = !string.IsNullOrEmpty(Request.Params["sortby"]) ? Request.Params["sortby"] : "date DESC";
          
          page = "WebpluginList.aspx?";
           
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            vars = GetAspVariables("LNG_BANNER_DESCRIPTION", "LNG_BANNER_ADD",
                "LNG_CREATION_DATE", "LNG_ACTIONS", "LNG_DELETE", "LNG_CREATE_NEW_BANNER", "LNG_DUP_BANNERS",
                "LNG_BANNER_DEFAULT_TITLE", "LNG_CAMPAIGNS_START_DATE_COLLUMN", "LNG_BANNERS_TITLE", "LNG_CAMPAIGN_ACTIVE", "LNG_CAMPAIGN_STARTED_AT", "LNG_CAMPAIGN_INACTIVE", "LNG_DUPLICATE_CAMPAIGN", "LNG_START_CAMPAIGN", 
                "LNG_STOP_CAMPAIGN", "LNG_CAMPAIGN_TUTOR_2", "LNG_STOP_SCHEDULE_ALERTS", "LNG_BANNER_NAME", "LNG_CREATE", "LNG_RESCHEDULE_CAMPAIGNS");
            name = webpluginLabel.Text = vars["LNG_BANNERS_TITLE"]; //GetAspVariable("LNG_CAMPAIGN_TITLE");
            webpluginDescription.Text = vars["LNG_BANNER_DESCRIPTION"];
            webpluginAddLabel.Text = vars["LNG_BANNER_ADD"];
            webpluginCreationDate.Text = vars["LNG_CREATION_DATE"];
            webpluginActions.Text = vars["LNG_ACTIONS"];
           deleteLabelButtom.Text =  deleteLabel.Text =  vars["LNG_DELETE"];

         //   Response.Write("<br><A href='#' class='delete_button' onclick=\"javascript: return LINKCLICK('Campaigns');\">"+ GetAspVariable("LNG_DELETE") +"</a><br/><br/>");
            ParseAndFillContentTable();
            linkDialog.Attributes.Add("title", vars["LNG_CREATE_NEW_BANNER"]);
            duplicateDialog.Attributes.Add("title", vars["LNG_DUP_BANNERS"]);
            linkValue.Value = vars["LNG_BANNER_DEFAULT_TITLE"];

           
        }

        public Dictionary<string, string> GetAspVariables(params string[] keys)
        {
            string queryString = String.Join(":", keys);

            string url = HttpContext.Current.Request.Url.AbsoluteUri.ToLower();
            string path = url.Substring(0, url.IndexOf("webpluginlist"));
            string url_bridge = path + "/dotnetbridge.asp?var=" + queryString;
            WebRequest request = WebRequest.Create(url_bridge);
            WebResponse response = request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
           // Response.Write(reader.ReadToEnd());
            string[] variablesStrings = reader.ReadToEnd().Split('&');
            

            Dictionary<string, string> result = new Dictionary<string, string>();
            foreach (string keyVal in variablesStrings)
            {

                string[] keyValArray = keyVal.Split(':');

                if (keyValArray.Length != 2)
                    continue;

                string key = keyValArray[0];
                string value = keyValArray[1];

                result.Add(key, value);
            }

            return result;

            
        }
        public string GetAspVariable(string variableName)
        {
            string url = HttpContext.Current.Request.Url.AbsoluteUri.ToLower();
            string path = url.Substring(0, url.IndexOf("webpluginlist"));
            string url_bridge = path + "/dotnetbridge.asp?var="+variableName;
            WebRequest request = WebRequest.Create(url_bridge);
            WebResponse response = request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            string variable = reader.ReadToEnd();
            return variable;
        }

        public string GetWebpluginsFromDB()
        {
            string url = HttpContext.Current.Request.Url.AbsoluteUri.ToLower();
            string path = url.Substring(0, url.IndexOf("webpluginlist"));
            string url_bridge = path + "/set_settings.asp?name=GetWebplugins&value=" + order + "&collumn=" + orderCollumn;
            WebRequest request = WebRequest.Create(url_bridge);
            WebResponse response = request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());

            return reader.ReadToEnd();

        }

        public string MakePages()
        {
            string url = HttpContext.Current.Request.Url.AbsoluteUri.ToLower();
            string path = url.Substring(0, url.IndexOf("webpluginlist"));
            string url_bridge = path + "/dotnetbridge.asp?action=makepage&offset="+Offset+"&limit="+Limit+"&count="+Count+"&page="+page+"&name="+Name+"&sortby="+SortBy;
            WebRequest request = WebRequest.Create(url_bridge);
            WebResponse response = request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            string variable = reader.ReadToEnd();
            return variable;

        }
        public void ParseAndFillContentTable()
        {
            string data = GetWebpluginsFromDB();
            
            string[] rows = data.Split('\n');

            noRows = data == string.Empty;
            foreach (string row in rows)
            {

                if (row == string.Empty)
                    return;

                    string[] fields = row.Split('\r');

                   if (fields.Length == 3)
                    {
                        HtmlTableRow htmlRow = FormatTableRow(fields[0], fields[1], fields[2]);
                        contentTable.Rows.Add(htmlRow);
                        count++;
                    }
 
            }
        }

        public HtmlTableRow FormatTableRow(string id, string name, string date)
        {


            string status = "";
            
            HtmlTableRow row = new HtmlTableRow();
            row.Attributes.Add("id", "row" + id);
            HtmlTableCell checkCell = new HtmlTableCell();
            checkCell.InnerHtml = "<input type=\"checkbox\" name=\"objects\" value=\"" + id + "\">";
            row.Cells.Add(checkCell);
            HtmlTableCell nameCell = new HtmlTableCell();
            nameCell.InnerHtml = string.Format("<a href=\"settings_webplugin_codegenerator.asp?editid={0}\">{1}</a>", id, name);
            row.Cells.Add(nameCell);
            HtmlTableCell dateCell = new HtmlTableCell();
            dateCell.InnerHtml = date;
            row.Cells.Add(dateCell);


            HtmlTableCell actionCell = new HtmlTableCell();
            actionCell.Align = "center";
            string innerHtml = string.Empty;

            //javascript:duplicateCampaign({1}, {2})
            innerHtml = string.Format("<a href='#' onclick='javascript:duplicateDialogFunc({1}, {2}, event)'><img src='images/action_icons/duplicate.png' alt='{0}' title='{0}' width='16' height='16' border='0' hspace='5'></a>", vars["LNG_DUPLICATE_CAMPAIGN"], id, "\"" + name + "\"");
           


            //openDialogUI('form','frame','reschedule.asp?campaign_id={0}',600,550,'reschedule campaign');
          //  innerHtml += string.Format("<a href=\'javascript:openReDialog({2}, {1}, {0})\' onclick=\"\"><img src=\"images/action_icons/schedule.png\" alt=\"reschedule alert\" title=\"reschedule alert\" border=\"0\" hspace=\"5\"></a>", id, "\"" + name + "\"", "\"" + startDate + "\"");
            actionCell.InnerHtml = innerHtml;
            row.Cells.Add(actionCell);
           

            return row;
        }




    }
}
