﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<!-- #include file="md5.asp" -->
<%

check_session()

uid = Session("uid")

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>DeskAlerts</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link href="css/style9.css" rel="stylesheet" type="text/css">
    <link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="css/colpick.css" rel="stylesheet" type="text/css">
    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>

    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/colpick.js"></script>
    <script>
        $(document).ready(function() {
            
            
        });
    </script>
    <!--[if IE]>
        <script>
            
        </script>
    <![endif]-->

    <style type="text/css">
        #content
        {
            position: relative;
            width: 80%;
            padding: 20px 50px 20px 50px;
            margin: auto;
            text-align:center;
        }
    </style>
</head>
<body style="margin: 0px" class="body">
    <%
if (uid <> "")  then
    %>
<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
                <tr>
                    <td>
<table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
		<tr>
		<td width="100%" height="31" class="main_table_title"><img src="images/menu_icons/campaign.png" alt="" width="20" height="20" border="0" style="padding-left:7px">
		<a href="CampaignDetails.aspx" class="header_title" style="position:absolute;top:15px"><%=LNG_DOWNLOAD_YOUR_BANNER%></a>
		</td>
		</tr>
<tr>
<td>
    <div id="content">
        <div style="width:500px;margin:auto;">Congratulations! The banner is ready to be embedded into your website. Place the following HTML code in the webpage source code 
where you need it. This page also should link the <%=Request("filename")%>.js script file, which should be physically placed in some directory of your site, along with the file located in the 
deskalerts_webplugin.zip archive.</div>
<br/><br/>
<input type="text" readonly style="width:200px;margin:auto;text-align:center"  value='<div id="web_plugin"></div>' />
<br/><br/>
        <div style="display:inline-block">
            <a href='<%=alerts_folder&"/admin/deskalerts_webplugin.zip"%>' id="downloadphp" class="add_site ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"
                role="button" aria-disabled="false"><span class="ui-button-text">Download <br/> deskalerts_webplugin.zip</span></a>
        </div>
        <div style="display:inline-block">
            <a href='<%=Request("jsfile")%>' download='<%=Request("filename")%>.js' id="downloadjs" class="add_site ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"
                role="button" aria-disabled="false"><span class="ui-button-text">Download <br/> <%=Request("filename")%>.js</span></a>
        </div>
<div style="margin-top:100px">
<div style="display:inline-block">
            <a href='WebpluginList.aspx' id="downloadphp" class="add_site ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"
                role="button" aria-disabled="false"><span class="ui-button-text">Back to banner`s list</span></a>
        </div>
<div style="display:inline-block">
            <a href='edit_alert_db.asp' id="downloadphp" class="add_site ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"
                role="button" aria-disabled="false"><span class="ui-button-text">Create alert for banner</span></a>
        </div>
</div>
    </div>
</td>
</tr>
</table>
</td>
</tr>
</table>
<%

else

  Response.Redirect "index.asp"

end if

%>
</body> </html>
<!-- #include file="db_conn_close.asp" -->
