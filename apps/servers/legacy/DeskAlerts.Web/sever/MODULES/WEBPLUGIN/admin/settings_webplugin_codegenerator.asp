﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<!-- #include file="md5.asp" -->
<%

check_session()

uid = Session("uid")
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>DeskAlerts</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link href="css/style9.css" rel="stylesheet" type="text/css">
    <link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="css/colpick.css" rel="stylesheet" type="text/css">
    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>

    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/colpick.js"></script>
    <script>
        $(document).ready(function() {
             $( "#accordion" ).accordion({
              collapsible: true
            });

            $('.border_color').colpick({
                layout: 'hex',
                submit: false,
                colorScheme: 'dark',
                onChange: function(hsb, hex, rgb, el, bySetColor) {
                    $("input[name=border_color]").val(hex);
                    $(".border_color").css("background", "#" + hex);
                    $("input[name=border_color]").trigger('change');
                }
            }).keyup(function() {
                $(this).colpickSetColor(this.value);
            });
            $('.border_color').colpickSetColor($("input[name=border_color]").val(), false);

            $('.font_color').colpick({
                layout: 'hex',
                submit: false,
                colorScheme: 'dark',
                onChange: function(hsb, hex, rgb, el, bySetColor) {
                    $("input[name=font_color]").val(hex);
                    $(".font_color").css("background", "#" + hex);
                    $("input[name=font_color]").trigger('change');
                }
            }).keyup(function() {
                $(this).colpickSetColor(this.value);
            });
            $('.font_color').colpickSetColor($("input[name=font_color]").val(), false);

            $('.background_color').colpick({
                layout: 'hex',
                submit: false,
                colorScheme: 'dark',
                onChange: function(hsb, hex, rgb, el, bySetColor) {
                    $("input[name=background_color]").val(hex);
                    $(".background_color").css("background", "#" + hex);
                    $("input[name=background_color]").trigger('change');
                }
            }).keyup(function() {
                $(this).colpickSetColor(this.value);
            });
            $('.background_color').colpickSetColor($("input[name=background_color]").val(), false);


            $('input[name=deskbar]').val(GUID());

            $('select[name=width_px]').change(function() {
                var selected = $(this).val();
                if (selected == 'px') $('.maxmin_width').css('display', 'none');
                else $('.maxmin_width').css('display', 'table-row');
            });
            $('select[name=height_px]').change(function() {
                var selected = $(this).val();
                if (selected == 'px') $('.maxmin_height').css('display', 'none');
                else $('.maxmin_height').css('display', 'table-row');
            });
            $('select[name=direction]').change(function() {
                var selected = $(this).val();
                var unicode = $('select[name=unicode_bidi]');
                if (selected == 'rtl') unicode.val('bidi-override');
                else unicode.val('normal');
            });
            $('select, input')
        .change(function() {
            preview();
        })
        .keyup(function() {
            preview();
        });

            /*$('input[name=site_name]')
        .change(function() {
            changeStatus();
        })
        .keyup(function() {
            changeStatus();
        });*/

            /*$('input[class=number]').bind("change keyup input click", function() {
                if (this.value.match(/[^0-9]/g)) {
                    this.value = this.value.replace(/[^0-9]/g, '');
                }
            }); */
            

            function changeStatus() {
                var site_name = $('input[name=site_name]').val();
                $.ajax({
                    url: '<%=alerts_folder %>' + '/admin/set_settings.asp',
                    methdod: 'POST',
                    data: { 'name': 'CheckUser', 'value': site_name }
                })
                .done(function(data, textStatus, jqXHR) {
                    if (data == 'is_exist') {
                        //$('#js_ready_text').text('JScript is ready for inclusion');
                        //$('#js_ready_icon').attr('src', './img/ok.png');
                    }
                    else if (data == 'is_free') {
                        //$('#js_ready_text').text('JScript is not ready for inclusion');
                        //$('#js_ready_icon').attr('src', './img/no.png');
                    }
                })
                .fail(function(jqXHR, textStatus, errorThrown) {
                    alert(errorThrown + "\r\n" + jqXHR.responseText);
                });
            }

            /*$('#copy').click(function(event) {
            event.preventDefault();
            var site_name = $('input[name=site_name]').val();

            is_name = nameIsFree(site_name);
            if (is_name == 'is_exist') {
            //copy to memory
            }
            else if (is_name == 'true') {
            alert('Site name not exist. First, add site.');
            }
            else {
            alert(is_name);
            }

        });*/

            $('#add_site').click(function(event) {
                event.preventDefault();
                var site_name = $('input[name=site_name]').val();
                nameIsFree(site_name, 'add_user');
                

            });

            function nameIsFree(name, action) {
                if (name.length < 3) {
                    alert('Banner`s name must contain at least 3 symbols');
                }
                else {
                    $.ajax({
                        url: '<%=alerts_folder %>' + '/admin/set_settings.asp',
                        methdod: 'POST',
                        data: { 'name': 'CheckUser', 'value': name }
                    })
                .done(function(data, textStatus, jqXHR) {
                    if (data == 'is_exist') {
                        
                        var edit_tmp = '<%=Request("editid")%>';
                        if (edit_tmp != '') addBanner(name,'Update');
                        else alert('This name already is exist. Please, enter other site name.');
                    }
                    else if (data == 'is_free' && action == 'add_user') {
                        addUser(name);

                    }
                })
                .fail(function(jqXHR, textStatus, errorThrown) {
                    alert(errorThrown + "\r\n" + jqXHR.responseText);
                });
                }
            }

            function addUser(user) {
                var deskbar_id = $('input[name=deskbar]').val();
                $.ajax({
                    url: '<%=alerts_folder %>' + '/admin/set_settings.asp',
                    methdod: 'POST',
                    data: { 'name': 'AddUser', 'value': user, 'deskbar_id': deskbar_id, 'version': 'web client' }
                })
                .done(function(data, textStatus, jqXHR) {
                    //alert('User successfully added');
                    //changeStatus(); ;
                })
                .fail(function(jqXHR, textStatus, errorThrown) {
                    alert(errorThrown + "\r\n" + jqXHR.responseText);
                });


                addBanner(user,'Insert');

            }

            function addBanner(user,action)
            {
                
                var width_unit = $('select[name=width_px]').val();
                var width = $('input[name=width]').val();
                var max_width = '';
                var min_width = '';
                var max_width_unit = '';
                var min_width_unit = '';
                if (width_unit != 'px') {
                    if (width_unit == 'auto') width = width_unit;
                    max_width = $('input[name=max_width]').val();
                    max_width_unit = $('select[name=max_width_px]').val();
                    min_width = $('input[name=min_width]').val();
                    min_width_unit = $('select[name=min_width_px]').val();
                }
                var height_unit = $('select[name=height_px]').val();
                var height = $('input[name=height]').val();
                var max_height = '';
                var min_height = '';
                var max_height_unit = '';
                var min_height_unit = '';
                if (height_unit != 'px') {
                    if (height_unit == 'auto') height = height_unit;
                    max_height = $('input[name=max_height]').val();
                    max_height_unit = $('select[name=max_height_px]').val();
                    min_height = $('input[name=min_height]').val();                    
                    min_height_unit = $('select[name=min_height_px]').val();
                }

                var background_color = '#' + $('input[name=background_color]').val();
                var transparent = 0;
                if ($('input[name=transparent_background_color]').prop('checked')) 
                {
                    transparent = 1;
                    background_color = 'transparent';
                }

                var font_color = '#' + $('input[name=font_color]').val();
                var font_size = $('input[name=font_size]').val();
                var font_size_unit = $('select[name=font_size_px]').val();
                var font_weight = $('input[name=font_weight]').val();
                var font_weight_unit = $('select[name=font_weight_px]').val();

                var text_align = $('select[name=text_align]').val();
                var direction = $('select[name=direction]').val();
                var unicode_bidi = $('select[name=unicode_bidi]').val();

                var border_weight = $('input[name=border_weight]').val();
                var border_weight_unit = $('select[name=border_weight_px]').val();
                var border_style = $('select[name=border_style]').val();
                var border_color = '#' + $('input[name=border_color]').val();

                var padding_top = $('input[name=padding_top]').val();
                var padding_top_unit = $('select[name=padding_top_px]').val();
                var padding_right = $('input[name=padding_right]').val();
                var padding_right_unit = $('select[name=padding_right_px]').val();
                var padding_bottom = $('input[name=padding_bottom]').val();
                var padding_bottom_unit = $('select[name=padding_bottom_px]').val();
                var padding_left = $('input[name=padding_left]').val();
                var padding_left_unit = $('select[name=padding_left_px]').val();

                var margin_top = $('input[name=margin_top]').val();
                var margin_top_unit = $('select[name=margin_top_px]').val();
                var margin_right = $('input[name=margin_right]').val();
                var margin_right_unit = $('select[name=margin_right_px]').val();
                var margin_bottom = $('input[name=margin_bottom]').val();
                var margin_bottom_unit = $('select[name=margin_bottom_px]').val();
                var margin_left = $('input[name=margin_left]').val();
                var margin_left_unit = $('select[name=margin_left_px]').val();


                var display = $('select[name=display]').val();
                var position = $('select[name=position]').val();

                var top = $('input[name=top]').val();
                var top_unit = $('select[name=top_px]').val();
                var right = $('input[name=right]').val();
                var right_unit = $('select[name=right_px]').val();
                var bottom = $('input[name=bottom]').val();
                var bottom_unit = $('select[name=bottom_px]').val();
                var left = $('input[name=left]').val();
                var left_unit = $('select[name=left_px]').val();

                var deskbar_id = $('input[name=deskbar]').val();

                 var id = '<%=Request("editid")%>';
                 if (id == '') id='0';

                if(id == '0' && action == 'Update') 
                {
                    alert('Unexpected error!');
                    return false;
                }


                $.ajax({
                    url: '<%=alerts_folder %>' + '/admin/set_settings.asp',
                    methdod: 'POST',
                    data: { 'name': 'AddBanner'+action, 'id':id, 'banner':user,'width':width,'width_unit':width_unit,'max_width':max_width,'max_width_unit':max_width_unit,
                            'min_width':min_width,'min_width_unit':min_width_unit,'deskbar_id':deskbar_id,
                            'height':height,'height_unit':height_unit,'max_height':max_height,'max_height_unit':max_height_unit,'min_height':min_height,'min_height_unit':min_height_unit,
                            'background_color':background_color,'transparent':transparent,'font_color':font_color,'font_size':font_size,'font_size_unit':font_size_unit,
                            'font_weight':font_weight,'font_weight_unit':font_weight_unit,'text_align':text_align,'direction':direction,'unicode_bidi':unicode_bidi,
                            'border_weight':border_weight,'border_weight_unit':border_weight_unit,'border_style':border_style,'border_color':border_color,'padding_top':padding_top,
                            'padding_top_unit':padding_top_unit,'padding_right':padding_right,'padding_right_unit':padding_right_unit,'padding_bottom':padding_bottom,
                            'padding_bottom_unit':padding_bottom_unit,'padding_left':padding_left,'padding_left_unit':padding_left_unit,'margin_top':margin_top,
                            'margin_top_unit':margin_top_unit,'margin_right':margin_right,'margin_right_unit':margin_right_unit,'margin_bottom':margin_bottom,
                            'margin_bottom_unit':margin_bottom_unit,'margin_left':margin_left,'margin_left_unit':margin_left_unit,'display':display,
                            'position':position,'top':top,'top_unit':top_unit,'right':right,'right_unit':right_unit,'bottom':bottom,'bottom_unit':bottom_unit,'left':left,
                            'left_unit':left_unit 
                          }
                })
                .done(function(data, textStatus, jqXHR) {
                    
                    //alert('User successfully added');
                    //changeStatus(); ;
                    window.location.href = 'downloadpage.asp?jsfile='+data+'&filename='+user;
                    

                })
                .fail(function(jqXHR, textStatus, errorThrown) {
                    alert(errorThrown + "\r\n" + jqXHR.responseText);
                });
            }


            function GUID() {
                var S4 = function() {
                    return Math.floor(
				                    Math.random() * 0x10000 /* 65536 */
			                        ).toString(16);
                                        };

                return (
		                S4() + S4() + "-" +
		                S4() + "-" +
		                S4() + "-" +
		                S4() + "-" +
		                S4() + S4() + S4()
	                   );
            }

            function generateJScript() {

                var code = '';
                var width_px = $('select[name=width_px]').val();
                var width = $('input[name=width]').val() + width_px;
                var max_width = '';
                var min_width = '';
                if (width_px != 'px') {
                    if (width_px == 'auto') width = width_px;
                    max_width = $('input[name=max_width]').val() + $('select[name=max_width_px]').val();
                    min_width = $('input[name=min_width]').val() + $('select[name=min_width_px]').val();
                }
                var height_px = $('select[name=height_px]').val();
                var height = $('input[name=height]').val() + height_px;
                var max_height = '';
                var min_height = '';
                if (height_px != 'px') {
                    if (height_px == 'auto') height = height_px;
                    max_height = $('input[name=max_height]').val() + $('select[name=max_height_px]').val();
                    min_height = $('input[name=min_height]').val() + $('select[name=min_height_px]').val();
                }

                var background_color = '#' + $('input[name=background_color]').val();
                if ($('input[name=transparent_background_color]').prop('checked')) background_color = 'transparent';

                var font_color = '#' + $('input[name=font_color]').val();
                var font_size = $('input[name=font_size]').val() + $('select[name=font_size_px]').val();
                var font_weight = $('input[name=font_weight]').val() + $('select[name=font_weight_px]').val();

                var text_align = $('select[name=text_align]').val();
                var direction = $('select[name=direction]').val();
                var unicode_bidi = $('select[name=unicode_bidi]').val();

                var border_weight = $('input[name=border_weight]').val() + $('select[name=border_weight_px]').val();
                var border_style = $('select[name=border_style]').val();
                var border_color = '#' + $('input[name=border_color]').val();

                var padding_top = $('input[name=padding_top]').val() + $('select[name=padding_top_px]').val();
                var padding_right = $('input[name=padding_right]').val() + $('select[name=padding_right_px]').val();
                var padding_bottom = $('input[name=padding_bottom]').val() + $('select[name=padding_bottom_px]').val();
                var padding_left = $('input[name=padding_left]').val() + $('select[name=padding_left_px]').val();

                var margin_top = $('input[name=margin_top]').val() + $('select[name=margin_top_px]').val();
                var margin_right = $('input[name=margin_right]').val() + $('select[name=margin_right_px]').val();
                var margin_bottom = $('input[name=margin_bottom]').val() + $('select[name=margin_bottom_px]').val();
                var margin_left = $('input[name=margin_left]').val() + $('select[name=margin_left_px]').val();


                var display = $('select[name=display]').val();
                var position = $('select[name=position]').val();

                var top = $('input[name=top]').val() + $('select[name=top_px]').val();
                var right = $('input[name=right]').val() + $('select[name=right_px]').val();
                var bottom = $('input[name=bottom]').val() + $('select[name=bottom_px]').val();
                var left = $('input[name=left]').val() + $('select[name=left_px]').val();

                var site_name = $('input[name=site_name]').val();

                var host = '<%=alerts_folder %>';

                var deskbar_id = $('input[name=deskbar]').val();

                code = "<script>" + "\r\n";

                code = code + "    function getXObject(){" + "\r\n";
                code = code + "        var xmlhttp;" + "\r\n";
                code = code + "        try {" + "\r\n";
                code = code + "	        xmlhttp = new ActiveXObject('Msxml2.XMLHTTP')" + "\r\n";
                code = code + "        } catch (e) {" + "\r\n";
                code = code + "	        try {" + "\r\n";
                code = code + "		        xmlhttp = new ActiveXObject('Microsoft.XMLHTTP')" + "\r\n";
                code = code + "	        } catch (E) {" + "\r\n";
                code = code + "		        xmlhttp = false;" + "\r\n";
                code = code + "	        }" + "\r\n";
                code = code + "        }" + "\r\n";
                code = code + "        if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {" + "\r\n";
                code = code + "	        xmlhttp = new XMLHttpRequest();" + "\r\n";
                code = code + "        }" + "\r\n";
                code = code + "		" + "\r\n";
                code = code + "        return  xmlhttp;" + "\r\n";
                code = code + "    };" + "\r\n";
                code = code + "	" + "\r\n";
                code = code + "    function setProperty(obj){" + "\r\n";
                code = code + "        obj.style.width = '" + width + "';" + "\r\n";
                code = code + "        obj.style.height = '" + height + "';" + "\r\n";
                if (width_px != 'px') {
                    code = code + "        obj.style.maxWidth = '" + max_width + "';" + "\r\n";
                    code = code + "        obj.style.minWidth = '" + min_width + "';" + "\r\n";
                }
                if (height_px != 'px') {
                    code = code + "        obj.style.maxHeight = '" + max_height + "';" + "\r\n";
                    code = code + "        obj.style.minHeight = '" + min_height + "';" + "\r\n";
                }
                code = code + "        obj.style.color = '" + font_color + "';" + "\r\n";
                code = code + "        obj.style.fontSize = '" + font_size + "';" + "\r\n";
                code = code + "        obj.style.fontFamily = 'inherit';" + "\r\n";
                code = code + "        obj.style.fontWeight = '" + font_weight + "';" + "\r\n";
                code = code + "        obj.style.backgroundColor = '" + background_color + "';" + "\r\n";
                code = code + "        obj.style.borderColor = '" + border_color + "';" + "\r\n";
                code = code + "        obj.style.borderWidth = '" + border_weight + "';" + "\r\n";
                code = code + "        obj.style.borderStyle = '" + border_style + "';" + "\r\n";
                code = code + "        obj.style.paddingTop = '" + padding_top + "';" + "\r\n";
                code = code + "        obj.style.paddingRight = '" + padding_right + "';" + "\r\n";
                code = code + "        obj.style.paddingBottom = '" + padding_bottom + "';" + "\r\n";
                code = code + "        obj.style.paddingLeft = '" + padding_left + "';" + "\r\n";
                code = code + "        obj.style.marginTop = '" + margin_top + "';" + "\r\n";
                code = code + "        obj.style.marginRight = '" + margin_right + "';" + "\r\n";
                code = code + "        obj.style.marginBottom = '" + margin_bottom + "';" + "\r\n";
                code = code + "        obj.style.marginLeft = '" + margin_left + "';" + "\r\n";
                code = code + "        obj.style.display = '" + display + "';" + "\r\n";
                code = code + "        obj.style.position = '" + position + "';" + "\r\n";
                code = code + "        obj.style.textAlign = '" + text_align + "';" + "\r\n";
                code = code + "        obj.style.direction = '" + direction + "';" + "\r\n";
                code = code + "        obj.style.unicodeBidi = '" + unicode_bidi + "';" + "\r\n";
                code = code + "        obj.style.top = '" + top + "';" + "\r\n";
                code = code + "        obj.style.right = '" + right + "';" + "\r\n";
                code = code + "        obj.style.bottom = '" + bottom + "';" + "\r\n";
                code = code + "        obj.style.left = '" + left + "';" + "\r\n";
                code = code + "        obj.style.overflow = 'auto';" + "\r\n";
                code = code + "    };" + "\r\n";
                code = code + "    var server = '" + host + "get_web_xml.asp';" + "\r\n";
                code = code + "    var uname = '" + site_name + "';" + "\r\n";
                code = code + "    var cnt = '1';" + "\r\n";
                code = code + "    var deskbar_id = '" + deskbar_id + "';" + "\r\n";
                code = code + "    var version = '1.0.0.0';" + "\r\n";
                code = code + "    var xml_alerts = getXObject();" + "\r\n";
                code = code + "    var url = 'deskalerts_webplugin.php';" + "\r\n";
                code = code + "    var params = 'uname='+uname+'&cnt='+cnt+'&deskbar_id='+deskbar_id+'&version='+version+'&getAlert=xml&host='+server+'';" + "\r\n";
                code = code + "    xml_alerts.open('POST', url, true);" + "\r\n";
                code = code + "    xml_alerts.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');" + "\r\n";
                code = code + "" + "\r\n";
                code = code + "    xml_alerts.onreadystatechange = function() {" + "\r\n";
                code = code + "        if (xml_alerts.readyState == 4) {" + "\r\n";
                code = code + "            if (xml_alerts.status == 200) {" + "\r\n";
                code = code + "                var str = xml_alerts.responseText;" + "\r\n";
                code = code + "                var count = 0;" + "\r\n";
                code = code + "                var elem_result = document.getElementById('web_plugin');" + "\r\n";
                code = code + "" + "\r\n";
                code = code + "                var parser = new DOMParser();" + "\r\n";
                code = code + "                var xmlDoc = parser.parseFromString(xml_alerts.responseText, 'application/xml');" + "\r\n";
                code = code + "                var xmlAlert = xmlDoc.getElementsByTagName('ALERT');" + "\r\n";
                code = code + "" + "\r\n";
                code = code + "                for (var i = 0; i < xmlAlert.length; i++) {" + "\r\n";
                code = code + "                    var get_alert = getXObject();" + "\r\n";
                code = code + "                    " + "\r\n";
                code = code + "			        var url_getAlert = xmlAlert[i].getAttribute('href');" + "\r\n";
                code = code + "			        var params2 = 'getAlert=alert&href='+url_getAlert+'';" + "\r\n";
                code = code + "			        get_alert.open('POST', url , true);" + "\r\n";
                code = code + "                    get_alert.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');" + "\r\n";
                code = code + "" + "\r\n";
                code = code + "                    get_alert.onreadystatechange = function() {" + "\r\n";
                code = code + "                        if (get_alert.readyState == 4) {" + "\r\n";
                code = code + "                            if (get_alert.status == 200) {" + "\r\n";
                code = code + "                                var textAlert = get_alert.responseText;  " + "\r\n";
                code = code + "								" + "\r\n";
                code = code + "						        var myRe = /(?:html_title=\").+(?:\")/g;" + "\r\n";
                code = code + "						        var html_title;" + "\r\n";
                code = code + "						        while ((myArray = myRe.exec(textAlert)) != null) {" + "\r\n";
                code = code + "							        html_title = myArray[0];" + "\r\n";
                code = code + "						        }" + "\r\n";
                code = code + "						        myRe = /html_title=\"(.+)\"/;" + "\r\n";
                code = code + "						        html_title = html_title.replace(myRe,'$1');" + "\r\n";
                code = code + "						        var item = document.getElementById('web_plugin');	" + "\r\n";
                code = code + "								" + "\r\n";
                code = code + "						        item.innerHTML = '<span>'+textAlert+'</span>';" + "\r\n";
                code = code + "								" + "\r\n";
                code = code + "						        setProperty(item);" + "\r\n";
                code = code + "                            }" + "\r\n";
                code = code + "                        }" + "\r\n";
                code = code + "                    };" + "\r\n";
                code = code + "" + "\r\n";
                code = code + "			        get_alert.send(params2);" + "\r\n";
                code = code + "                } " + "\r\n";
                code = code + "            }" + "\r\n";
                code = code + "        }" + "\r\n";
                code = code + "    };" + "\r\n";
                code = code + "    xml_alerts.send(params);" + "\r\n";
                code = code + "<\/script>" + "\r\n";
                code = code + "<div id='web_plugin' ></div>";

                return code;
            }

            function preview() {
                var code = generateJScript();
                //$('#textarea_jscode').val(code);
                var textAlert = 'How it look on your site';
                $('#preview').html("<div id='web_plugin' ></div>");
                var item = $('#web_plugin');
                setProperty(item);
                item.html('<span>' + textAlert + '</span>');
            }

            function setEditable(edit)
            {
                $.ajax({
                    url: '<%=alerts_folder %>' + '/admin/set_settings.asp',
                    methdod: 'POST',
                    data: { 'name': 'EditBanner', 'value': edit}
                })
                .done(function(data, textStatus, jqXHR) {
                    var values = data.split(",");

                    $('input[name=site_name]').val(values[0]);

                    $('input[name=width]').val(values[1]);
                    $('select[name=width_px]').val(values[2]);
                    
                    
                    $('input[name=max_width]').val(values[3]);
                    $('select[name=max_width_px]').val(values[4]);
                    $('input[name=min_width]').val(values[5]);
                    $('select[name=min_width_px]').val(values[6]);

                    $('input[name=height]').val(values[7]);
                    $('select[name=height_px]').val(values[8]);
                    
                    $('input[name=max_height]').val(values[9]);
                    $('select[name=max_height_px]').val(values[10]);
                    $('input[name=min_height]').val(values[11]);
                    $('select[name=min_height_px]').val(values[12]);
                    $('input[name=background_color]').val(values[13].replace('#',''));//;
                    var transparent = values[14];
                    if (transparent==1)$('input[name=transparent_background_color]').prop('checked',true);

                    $('input[name=font_color]').val(values[15].replace('#',''));//
                    $('input[name=font_size]').val(values[16]);
                    $('select[name=font_size_px]').val(values[17]);
                    $('input[name=font_weight]').val(values[18]);
                    $('select[name=font_weight_px]').val(values[19]);

                    $('select[name=text_align]').val(values[20]);
                    $('select[name=direction]').val(values[21]);
                    $('select[name=unicode_bidi]').val(values[22]);

                    $('input[name=border_weight]').val(values[23]);
                    $('select[name=border_weight_px]').val(values[24]);
                    $('select[name=border_style]').val(values[25]);
                    $('input[name=border_color]').val(values[26].replace('#',''));//

                    $('input[name=padding_top]').val(values[27]);
                    $('select[name=padding_top_px]').val(values[28]);
                    $('input[name=padding_right]').val(values[29]);
                    $('select[name=padding_right_px]').val(values[30]);
                    $('input[name=padding_bottom]').val(values[31]);
                    $('select[name=padding_bottom_px]').val(values[32]);
                    $('input[name=padding_left]').val(values[33]);
                    $('select[name=padding_left_px]').val(values[34]);

                    $('input[name=margin_top]').val(values[35]);
                    $('select[name=margin_top_px]').val(values[36]);
                    $('input[name=margin_right]').val(values[37]);
                    $('select[name=margin_right_px]').val(values[38]);
                    $('input[name=margin_bottom]').val(values[39]);
                    $('select[name=margin_bottom_px]').val(values[40]);
                    $('input[name=margin_left]').val(values[41]);
                    $('select[name=margin_left_px]').val(values[42]);


                    $('select[name=display]').val(values[43]);
                    $('select[name=position]').val(values[44]);

                    $('input[name=top]').val(values[45]);
                    $('select[name=top_px]').val(values[46]);
                    $('input[name=right]').val(values[47]);
                    $('select[name=right_px]').val(values[48]);
                    $('input[name=bottom]').val(values[49]);
                    $('select[name=bottom_px]').val(values[50]);
                    $('input[name=left]').val(values[51]);
                    $('select[name=left_px]').val(values[52]);
                    preview();
                })
                .fail(function(jqXHR, textStatus, errorThrown) {
                    alert(errorThrown + "\r\n" + jqXHR.responseText);
                });

                
            }

            function setProperty(obj) {
                var width_px = $('select[name=width_px]').val();
                var width = $('input[name=width]').val() + width_px;
                var max_width = '';
                var min_width = '';
                if (width_px != 'px') {
                    if (width_px == 'auto') width = width_px;
                    max_width = $('input[name=max_width]').val() + $('select[name=max_width_px]').val();
                    min_width = $('input[name=min_width]').val() + $('select[name=min_width_px]').val();
                }
                var height_px = $('select[name=height_px]').val();
                var height = $('input[name=height]').val() + height_px;
                var max_height = '';
                var min_height = '';
                if (height_px != 'px') {
                    if (height_px == 'auto') height = height_px;
                    max_height = $('input[name=max_height]').val() + $('select[name=max_height_px]').val();
                    min_height = $('input[name=min_height]').val() + $('select[name=min_height_px]').val();
                }

                var background_color = '#' + $('input[name=background_color]').val();
                if ($('input[name=transparent_background_color]').prop('checked')) background_color = 'transparent';

                var font_color = '#' + $('input[name=font_color]').val();
                var font_size = $('input[name=font_size]').val() + $('select[name=font_size_px]').val();
                var font_weight = $('input[name=font_weight]').val() + $('select[name=font_weight_px]').val();

                var text_align = $('select[name=text_align]').val();
                var direction = $('select[name=direction]').val();
                var unicode_bidi = $('select[name=unicode_bidi]').val();

                var border_weight = $('input[name=border_weight]').val() + $('select[name=border_weight_px]').val();
                var border_style = $('select[name=border_style]').val();
                var border_color = '#' + $('input[name=border_color]').val();

                var padding_top = $('input[name=padding_top]').val() + $('select[name=padding_top_px]').val();
                var padding_right = $('input[name=padding_right]').val() + $('select[name=padding_right_px]').val();
                var padding_bottom = $('input[name=padding_bottom]').val() + $('select[name=padding_bottom_px]').val();
                var padding_left = $('input[name=padding_left]').val() + $('select[name=padding_left_px]').val();

                var margin_top = $('input[name=margin_top]').val() + $('select[name=margin_top_px]').val();
                var margin_right = $('input[name=margin_right]').val() + $('select[name=margin_right_px]').val();
                var margin_bottom = $('input[name=margin_bottom]').val() + $('select[name=margin_bottom_px]').val();
                var margin_left = $('input[name=margin_left]').val() + $('select[name=margin_left_px]').val();


                var display = $('select[name=display]').val();
                var position = $('select[name=position]').val();

                var top = $('input[name=top]').val() + $('select[name=top_px]').val();
                var right = $('input[name=right]').val() + $('select[name=right_px]').val();
                var bottom = $('input[name=bottom]').val() + $('select[name=bottom_px]').val();
                var left = $('input[name=left]').val() + $('select[name=left_px]').val();

                obj.css('width', width);
                obj.css('height', height);
                if (width_px != 'px') {
                    obj.css('maxWidth', max_width);
                    obj.css('minWidth', min_width);
                }
                if (height_px != 'px') {
                    obj.css('maxHeight', max_height);
                    obj.css('minHeight', min_height);
                }
                obj.css('color', font_color);
                obj.css('fontSize', font_size);
                obj.css('fontFamily', 'inherit');
                obj.css('fontWeight', font_weight);
                obj.css('backgroundColor', background_color);
                obj.css('borderColor', border_color);
                obj.css('borderWidth', border_weight);
                obj.css('borderStyle', border_style);
                obj.css('paddingTop', padding_top);
                obj.css('paddingRight', padding_right);
                obj.css('paddingBottom', padding_bottom);
                obj.css('paddingLeft', padding_left);
                obj.css('marginTop', margin_top);
                obj.css('marginRight', margin_right);
                obj.css('marginBottom', margin_bottom);
                obj.css('marginLeft', margin_left);
                obj.css('display', display);
                obj.css('position', position);
                obj.css('textAlign', text_align);
                obj.css('direction', direction);
                obj.css('unicodeBidi', unicode_bidi);
                obj.css('top', top);
                obj.css('right', right);
                obj.css('bottom', bottom);
                obj.css('left', left);
            };

            var edit = '<%=Request("editid")%>';
            if(edit != '') setEditable(edit);


            preview();

        });
    </script>
    <!--[if IE]>
        <script>
            
        </script>
    <![endif]-->

    <style type="text/css">
        #content
        {
            position: relative;
            width: 80%;
            padding: 20px 50px 20px 50px;
            margin: auto;
        }
        #settings
        {
            padding: 0px 10px 10px 10px;
            vertical-align: top;
            padding: 0px;
            width: 600px;
        }
        #settings table
        {
            display: inline-table;
            margin-left: 10px;
            width: 230px;
            text-align: left;
        }
        #settings table tr td:first-child
        {
            width: 70%;
        }
        #settings table:first-child
        {
            margin-left: 0px;
        }
        #column2
        {
            width: 100%;
            max-width: 720px;
            padding: 10px 10px 10px 10px;
            height: 50%;
            border: none;
            display: inline-table;
            border-spacing: 0px;
        }
        #column2 td
        {
            padding: 0px;
            padding-top: 20px;
            margin: 0px;
        }
        #column2 tr:first-child td
        {
            padding-top: 0px;
        }
        #column2 div
        {
            width: 100%;
            padding: 10px 10px 10px 10px;
            display: inline-block;
        }
        #textarea_jscode
        {
            width: 100%;
            height: 200px;
            resize: none;
        }
        .maxmin_width
        {
            display: none;
        }
        .maxmin_height
        {
            display: none;
        }
        /*#preview{
        width:      100%;
        padding:    10px 10px 10px 10px;
        height:     50%;
        border:     1px solid black;
        display:    inline-block;
        }*/
        #jscode
        {
            text-align: left;
        }
        #textarea_jscode{
            max-width: 700px;
        }
        .number
        {
            width: 50px;
        }
        .px
        {
            width: 52px;
        }
        .border_style
        {
        }
        .copy
        {
            margin-top: 10px;
        }
        .text
        {
            width: 100px;
        }
        #div_site_name
        {
            text-align: left;
            margin-bottom: 30px;
        }
        #preview
        {
            position: relative;
            width: 100%;
            height: 300px;
            max-height: 400px;
            overflow: auto;
            vertical-align: top;
            border: 1px dotted #000000;
            background-color: #DCDCDC;
        }
        #page_table
        {
            width: 90%;
            table-layout: fixed;
            margin-top: 30px;
            margin-left: 30px;
        }
        .background_color, .font_color, .border_color{
            height: 20px;
            border: 1px dotted #000000;
        }
    </style>
</head>
<body style="margin: 0" class="body">
    <%
if (uid <> "")  then
    %>
<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
                <tr>
                    <td>
<table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
		<tr>
		<td width="100%" height="31" class="main_table_title"><img src="images/menu_icons/web_alert.png" alt="" width="20" height="20" border="0" style="padding-left:7px">
		<a href="WebpluginList.aspx" class="header_title" style="position:absolute;top:15px"><%=LNG_BANNER_OPTIONS%></a>
		</td>
		</tr>
<tr>
<td>

    <div id="content">
        <div id="div_site_name">
            Banner`s name:&nbsp;&nbsp;<input type="text" style="" class="text" name="site_name" />
            <input type="hidden" style="" class="text" name="deskbar" />
        </div>
        Preview area:
        <div id="preview">
        </div>
        <div id="accordion" style="width:100%;margin-bottom:30px;">
            <h3>Size</h3>
            <div>
                <div style="display:inline-block;">
                    <div style="margin-bottom:10px;">
                        Width:
                        <br/>
                        <input type="number" value="100" style="" class="number" name="width" />

                        <select class="px" name="width_px">
                            <option>%</option>
                            <option selected>px</option>
                            <option>auto</option>
                        </select>

                    </div>
                    <div class="maxmin_width"  style="margin-bottom:10px;">
                        Max width:
                        <br/>

                        <input type="number" value="500" style="" class="number" name="max_width" />

                        <select class="px" name="max_width_px">
                            <option>%</option>
                            <option selected>px</option>
                        </select>
                    </div>
                    <div class="maxmin_width" >
                        Min width:
                        <br/>

                        <input type="number" value="100" style="" class="number" name="min_width" />

                        <select class="px" name="min_width_px">
                            <option>%</option>
                            <option selected>px</option>
                        </select>
                    </div>
                </div>
                <div style="display:inline-block;margin-left:50px;">
                    <div style="margin-bottom:10px;">
                        Height:
                        <br/>

                        <input type="number" value="100" style="" class="number" name="height" />

                        <select class="px" name="height_px">
                            <option>%</option>
                            <option selected>px</option>
                            <option>auto</option>
                        </select>

                    </div>
                    <div class="maxmin_height" style="margin-bottom:10px;">
                        Max height:
                        <br/>

                        <input type="number" value="500" style="" class="number" name="max_height" />

                        <select class="px" name="max_height_px">
                            <option>%</option>
                            <option selected>px</option>
                        </select>
                    </div>
                    <div class="maxmin_height" >
                        Min height:
                        <br/>

                        <input type="number" value="100" style="" class="number" name="min_height" />

                        <select class="px" name="min_height_px">
                            <option>%</option>
                            <option selected>px</option>
                        </select>
                    </div>
                </div>
            </div>
            <h3>Text & Background</h3>
            <div>
                <div style="float:left;margin-right:50px;">
                    <div style="width:100px;margin-bottom:10px;">

                        Background color:
                        <br/>

                        <div class="background_color"></div>
                        <input type="hidden" value="FFFFFF" name="background_color" />
                        <input type="checkbox" class="transparent" name="transparent_background_color" id="transparent_background_color" />
                        <label for="transparent_background_color">transparent</label>
                    </div>
                    <div style="width:100px;margin-bottom:10px;">
                        Font color:
                        <br/>

                        <div class="font_color"></div>
                        <input type="hidden" value="000000" name="font_color" />
                    </div>
                </div>
                <div style="float:left;margin-right:50px;">
                    <div style="margin-bottom:10px;">
                        Font size:
                        <br/>

                        <input type="number" value="12" style="" class="number" name="font_size" />

                        <select class="px" name="font_size_px">
                            <option>em</option>
                            <option selected>px</option>
                        </select>
                    </div>
                    <div style="margin-bottom:10px;">
                        Font weight:
                        <br/>

                        <input type="number" value="1" style="" class="number" name="font_weight" />

                        <select class="px" name="font_weight_px">
                            <option>em</option>
                            <option selected>px</option>
                        </select>
                    </div>
                </div>
                <div style="float:left;margin-right:50px;">
                    <div style="margin-bottom:10px;">
                        Text align:
                        <br/>

                        <select class="border" name="text_align">
                            <option selected>left</option>
                            <option>center</option>
                            <option>right</option>
                            <option>justify</option>
                            <option>auto</option>
                            <option>start</option>
                            <option>end</option>
                        </select>
                    </div>
                    <div style="margin-bottom:10px;">
                        Direction:
                        <br/>

                        <select class="border" name="direction">
                            <option selected value="ltr">left-to-right</option>
                            <option value="rtl">right-to-left</option>
                        </select>
                    </div>
                    <div style="margin-bottom:10px;">
                        Unicode bidi:
                        <br/>

                        <select class="border" name="unicode_bidi">
                            <option selected>normal</option>
                            <option>embed</option>
                            <option>bidi-override</option>
                        </select>
                    </div>
                </div>


            </div>
            <h3>Borders</h3>
            <div>
                <div style="float:left;margin-right:50px;">
                    <div style="width:110px;margin-bottom:10px;">

                        Border weight:
                        <br/>

                        <input type="number" value="1" style="width:40px !important;" class="number" name="border_weight" />
                           
                        <select class="px" name="border_weight_px">
                            <option>em</option>
                            <option selected>px</option>
                        </select>
                    </div>
                    <div style="width:100px;margin-bottom:10px;">
                        Border style:
                        <br/>

                        <select class="border" name="border_style" style="width: 100%;">
                            <option selected>solid</option>
                            <option>dotted</option>
                            <option>dashed</option>
                            <option>double</option>
                            <option>groove</option>
                            <option>ridge</option>
                            <option>inset</option>
                            <option>outset</option>
                        </select>
                    </div>
                    <div style="margin-bottom:10px;">
                        Border color:
                        <br/>

                        <div style="width: 97px;" class="border_color"></div>
                         <input type="hidden" value="000000" name="border_color" />
                    </div>                    
                </div>
                
            </div>
            <h3>Indents</h3>
            <div>

                 <div style="display:inline-block;">
                    <div style="margin-bottom:10px;">
                        Padding top:
                        <br/>
                         <input type="number" value="10" style="" class="number" name="padding_top" />

                                <select class="px" name="padding_top_px">
                                    <option>%</option>
                                    <option selected>px</option>
                                </select>

                    </div>
                    <div style="margin-bottom:10px;">
                        Padding right:
                        <br/>

                        <input type="number" value="30" style="" class="number" name="padding_right" />
                                <select class="px" name="padding_right_px">
                                    <option>%</option>
                                    <option selected>px</option>
                                </select>
                    </div>
                    <div style="margin-bottom:10px;">
                        Padding bottom:
                        <br/>

                         <input type="number" value="10" style="" class="number" name="padding_bottom" />
                                <select class="px" name="padding_bottom_px">
                                    <option>%</option>
                                    <option selected>px</option>
                                </select>
                    </div>
                    <div style="margin-bottom:10px;">
                        Padding left:
                        <br/>

                         <input type="number" value="30" style="" class="number" name="padding_left" />
                                <select class="px" name="padding_left_px">
                                    <option>%</option>
                                    <option selected>px</option>
                                </select>
                    </div>
                </div>
                <div style="display:inline-block;margin-left:50px;">
                   <div style="margin-bottom:10px;">
                         Margin top: <br/>
                        
                                <input type="number" value="0" style="" class="number" name="margin_top" />
                           
                                <select class="px" name="margin_top_px">
                                    <option>%</option>
                                    <option selected>px</option>
                                </select>
                    </div>
                    <div style="margin-bottom:10px;">
                            Margin right: <br/>
                        
                                <input type="number" value="0" style="" class="number" name="margin_right" />
                     
                                <select class="px" name="margin_right_px">
                                    <option>%</option>
                                    <option selected>px</option>
                                </select>
                    </div>
                    <div style="margin-bottom:10px;">
                         Margin bottom: <br/>
                          
                                <input type="number" value="0" style="" class="number" name="margin_bottom" />
                        
                                <select class="px" name="margin_bottom_px">
                                    <option>%</option>
                                    <option selected>px</option>
                                </select>
                    </div>
                    <div style="margin-bottom:10px;">
                        Margin left: <br/>
                      
                                <input type="number" value="0" style="" class="number" name="margin_left" />
                      
                                <select class="px" name="margin_left_px">
                                    <option>%</option>
                                    <option selected>px</option>
                                </select>
                    </div>
                </div>       

            </div>

            <h3>Position</h3>
            <div>

                 <div style="float:left;">
                    <div style="margin-bottom:10px;">
                         Display:
                            <br/>
                                <select class="border" name="display" style="width: 100%;">
                                    <option selected>block</option>
                                    <option>inline</option>
                                    <option>inline-block</option>
                                    <option>table</option>
                                    <option>table-cell</option>
                                    <option>inline-table</option>
                                </select>
                    </div>
                    <div style="margin-bottom:10px;">
                        Position:
                            <br/>
                                <select class="border" name="position" style="width: 100%;">
                                    <option selected>relative</option>
                                    <option>absolute</option>
                                    <option>static</option>
                                </select>
                    </div>                    
                </div>
                <div style="float:left;margin-left:50px;">
                   <div style="margin-bottom:10px;">
                         Top: <br/>
                        
                                <input type="number" value="0" style="" class="number" name="top" />
                                <select class="px" name="top_px">
                                    <option>%</option>
                                    <option selected>px</option>
                                </select>
                    </div>
                    <div style="margin-bottom:10px;">
                            Right: <br/>
                        
                                <input type="number" value="0" style="" class="number" name="right" />
                                <select class="px" name="right_px">
                                    <option>%</option>
                                    <option selected>px</option>
                                </select>
                    </div>
                    <div style="margin-bottom:10px;">
                         Bottom: <br/>
                          
                               <input type="number" value="0" style="" class="number" name="bottom" />
                                <select class="px" name="bottom_px">
                                    <option>%</option>
                                    <option selected>px</option>
                                </select>
                    </div>
                    <div style="margin-bottom:10px;">
                        Left: <br/>
                      
                                <input type="number" value="0" style="" class="number" name="left" />
                                <select class="px" name="left_px">
                                    <option>%</option>
                                    <option selected>px</option>
                                </select>
                    </div>
                </div>       

            </div>


        </div>
        <div style="position:relative;margin-left:100%;left:-60px;width:90px;">
            <a href="#" id="add_site" class="add_site ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"
                role="button" aria-disabled="false"><span class="ui-button-text">Next</span></a>
        </div>
    </div>
</td>
</tr>
</table>
</td>
</tr>
</table>
<%

else

  Response.Redirect "index.asp"

end if

%>
</body> </html>
<!-- #include file="db_conn_close.asp" -->
