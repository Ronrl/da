 <!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%
check_session()
%>
<!-- #include file="Common.asp" -->
<!-- #include file="header.asp" -->
<!-- #include file="footer.asp" -->

<%
'On Error Resume Next
' index CustomIncludes End
'-------------------------------

'===============================
' Save Page and File Name available into variables
'-------------------------------
Dim sFileName
Dim sTemplateFileName
Dim intSessionUserId

sFileName = "edit_screensavers.asp"
sTemplateFileName = "edit_screensavers.html"
sScreensaverTemplateFileName = "screensaver_template_content.html"
'===============================

function updateAlert(id,alertText, alertTitle, fromDate, toDate, schedule, scheduleType, recurrence, urgent, email, desktop, rss, emailSender, ticker, fullscreen, alertWidth, alertHeight, aknown, autoclose, toolbarmode, deskalertsmode, template, uid, paramTemplateId)
	sqlRequest = "UPDATE alerts SET alert_text=N'"& Replace(alertText, "'", "''") &"', title=N'"& Replace(alertTitle, "'", "''") &"', type='D', from_date='"&fromDate&"', to_date='"&toDate&"', schedule='"&schedule&"', schedule_type='"&scheduleType&"', recurrence='"&recurrence&"', urgent='"&urgent&"', email='"&email&"', desktop='"&desktop&"', email_sender='"& Replace(emailSender, "'", "''") &"', ticker='"&ticker&"', fullscreen='"&fullscreen&"', alert_width="&alertWidth&",  alert_height="&alertHeight&", aknown='"&aknown&"',  autoclose="&autoclose&",  toolbarmode='"&toolbarmode&"', deskalertsmode='"&deskalertsmode&"', template_id="&template&", sender_id="&uid&", param_temp_id="&paramTemplateId&" WHERE id = " & Clng(id)
	Set RS_alert_update = Conn.Execute(sqlRequest)
   'MEMCACHE
'	Set cacheMgr = CreateObject("DeskAlerts.CacheManager")
  '  cacheMgr.Update(clng(id), now())
	updateAlert = id
    MemCacheUpdate "Update", id 
end function

'alertClass 1 - usual alert, 2 - screensaver alert, 4 - ...

function addAlertToDataBase(alertText, alertTitle, fromDate, toDate, schedule, scheduleType, recurrence, urgent, email, desktop, rss, emailSender, ticker, fullscreen, alertWidth, alertHeight, aknown, autoclose, toolbarmode, deskalertsmode, template, uid, paramTemplateId, alertClass, campaign_id, approve_status)
	sqlRequest = "INSERT INTO alerts (alert_text, title, create_date, type, from_date, to_date, schedule, schedule_type, recurrence, urgent, email, desktop,  email_sender, ticker, fullscreen, alert_width,  alert_height, aknown,  autoclose,  toolbarmode, deskalertsmode, template_id, sender_id, param_temp_id, class, campaign_id, approve_status) VALUES (N'"& Replace(alertText, "'", "''") &"', N'"& Replace(alertTitle, "'", "''") &"', GETDATE(), 'D', '"&fromDate&"','"&toDate&"','"&schedule&"', '"&scheduleType&"', '"&recurrence&"', '"&urgent&"', '"&email&"', '"&desktop&"',  '"& Replace(emailSender, "'", "''") &"',  '"&ticker&"', '"&fullscreen&"', "&alertWidth&",  "&alertHeight&", '"&aknown&"', "&autoclose&", '"&toolbarmode&"', '"&deskalertsmode&"',"&template&", "&uid&","&paramTemplateId&","&alertClass&","& campaign_id & "," & approve_status &"); select @@IDENTITY as 'newID'"
	Set RS_alert = Conn.Execute(sqlRequest)
	Set RS_alert  = RS_alert.NextRecordset
	addAlertToDataBase=RS_alert("newID")
	MemCacheUpdate "Add", addAlertToDataBase 
	if(campaign_id <> "-1") then
	 end if
	RS_alert.Close
    'MEMCACHE
	' Set cacheMgr = CreateObject("DeskAlerts.CacheManager")
    '  cacheMgr.Add(clng(addAlertToDataBase), now())

end function
  
  
intSessionUserId = Session("uid")
uid = intSessionUserId
if (intSessionUserId <> "") then

screenasverType = Request("type")
if (screenasverType="image") then
	sTemplateFileName = "screensaver_edit_image.html"
elseif (screenasverType="powerpoint") then
	sTemplateFileName = "screensaver_edit_powerpoint.html"
elseif (screenasverType="video") then
	sTemplateFileName = "screensaver_edit_video.html"
end if

needtable=Request("needtable")
if needtable<>1 then
needtable=0
end if


set rights = getRights(uid, true, false, true, "screensavers_val")
gid = rights("gid")
gviewall = rights("gviewall")
gviewlist = rights("gviewlist")
screensavers_arr = rights("screensavers_val")

' index Show begin

'===============================
' Display page
'-------------------------------
' Load HTML template for this page
'-------------------------------
LoadTemplate alerts_dir & "\" & templateFolder & "\" & sTemplateFileName, "main"
'-------------------------------
' Load HTML template of Header and Footer
'-------------------------------
sHeaderFileName=Replace(sHeaderFileName,"header.html","header-old.html")
LoadTemplate sHeaderFileName, "Header"
LoadTemplate sFooterFileName, "Footer"
'-------------------------------
SetVar "Menu", ""
SetVar "DateForm", ""

SetVar "LNG_STEP", LNG_STEP
SetVar "LNG_OF", LNG_OF

SetVar "needtable", needtable
SetVar "now_db", Now()
SetVar "LNG_ADD_SCREENSAVER", LNG_ADD_SCREENSAVER
SetVar "LNG_TITLE", LNG_TITLE
SetVar "LNG_DATE", LNG_DATE
SetVar "LNG_EXPORT_TO_CSV", LNG_EXPORT_TO_CSV
SetVar "LNG_TOTAL", LNG_TOTAL
SetVar "default_lng", lcase(default_lng)
SetVar "timezone_db", timezone_db
SetVar "start_and_end_date_and_time_checked", ""
SetVar "time_to_display_checked", ""
SetVar "time_to_display", "60"
SetVar "sFileName", sFileName
SetVar "strPageName", LNG_SCREENSAVERS
SetVar "FromDateNotValidStr", LNG_FROM_DATE_AND_TIME_IS_NOT_VALID
SetVar "ToDateNotValidStr", LNG_TO_DATE_AND_TIME_IS_NOT_VALID
SetVar "ToDateEarlyError", LNG_REC_DATE_ERROR
SetVar "TimeToDisplayNotValidStr", LNG_TIME_TO_DISPLAY_IS_NOT_VALID
SetVar "PreviewStr", LNG_PREVIEW
SetVar "ChooseVideoFileStr", LNG_CHOOSE_VIDEO_FILE
SetVar "VideoFileStr", LNG_VIDEO_FILE
SetVar "screensaver_video_tr_display", "none"
SetVar "screensaver_img_tr_display", "none"
SetVar "ImageFileStr", LNG_IMAGE_FILE
SetVar "DimensionsStr", LNG_DIMENSIONS
SetVar "FileSizeStr", LNG_FILE_SIZE
SetVar "FileNameStr", LNG_FILE_NAME
SetVar "SpecifyScreensaverName", LNG_SPECIFY_NAME
SetVar "ChooseScreensaverImgStr", LNG_CHOOSE_SCREENSAVER_IMAGE
SetVar "ErrorScreensaverImageStr", LNG_YOU_SHOULD_CHOOSE_SCREENSAVER_IMAGE
SetVar "ErrorScreensaverVideoStr", LNG_YOU_SHOULD_CHOOSE_SCREENSAVER_VIDEO_FILE
SetVar "LNG_SCREENSAVER_POWERPOINT_INSTRUCTIONS", LNG_SCREENSAVER_POWERPOINT_INSTRUCTIONS
SetVar "LNG_SCREENSAVER_VIDEO_INSTRUCTIONS", LNG_SCREENSAVER_VIDEO_INSTRUCTIONS
SetVar "LNG_SCREENSAVER_IMAGE_CHOOSE_TEXT", LNG_SCREENSAVER_IMAGE_CHOOSE_TEXT
SetVar "LNG_CLOSE", LNG_CLOSE
SetVar "alerts_folder", alerts_folder

if CAMPAIGN = 1 and  Request("alert_campaign") <> "" then
SetVar "Campaign", "<b>" & LNG_CAMPAIGN & ":</b>"
else
SetVar "Campaign", ""   
end if
'--------------------

if CAMPAIGN = 1 then


                                                                    
 if Request("alert_campaign") = "" then
 campaignSelector = "<input id='campaign_select' type='hidden' name='campaign_select' value='-1'>"
 else
 
    SQL = "SELECT name FROM campaigns WHERE id = " & Request("alert_campaign")
    Set RS2 = Conn.Execute(SQL)
    if not RS2.EOF then
       campaignSelector =  "<label><b>"&HtmlEncode(RS2("name"))&"</b></label><br>"
       campaignSelector = campaignSelector &  "<input type='hidden' id='campaign_select' name='campaign_select' value='"&Request("alert_campaign")&"'>"                                
    end if
  
 end if
   
else
 campaignSelector = "<input id='campaign_select' type='hidden' name='campaign_select' value='-1'>"
end if

SetVar "CampaignSelector", campaignSelector & "<br>"

alertId = Request("id")
isEdit = Request("edit")
alertHTML = Request("screensaver_html")
fromDate = Request("from_date")
toDate = Request("to_date")
title = Request("title")
autoclose = Request("autoclose")
campaign_id = Request("campaign_id")
schedule = 0

SetVar "edit", isEdit

if alertId<>"" then
	dup_id = alertId
end if

if (autoclose = "") then
	autoclose = 0
end if

if (fromDate <> "") then
	schedule = 1
end if

set params = param("values")

if (alertHTML <> "") then

	'alertHTML = Replace(alertHTML, "'", "''") 
	
	schedule_type=1
	
	if(campaign_id <> "-1") then
	 schedule_type = 0
	end if
	
	Set is_started = Conn.Execute("SELECT DISTINCT 1 FROM alerts WHERE campaign_id = " & campaign_id & " AND schedule_type = '1'")
	if not is_started.EOF then
		schedule_type = 1
	 end if  
                    approve_status = 1
    
                     if(APPROVE = 1) then
                             Set enabled = Conn.execute("SELECT val FROM settings WHERE name = 'ConfAllowApprove'") 
         
                            if(enabled("val") = "1" and  gid <> 0 and screensavers_arr(6) <> "checked"  ) then
                                approve_status = 0
                             end if
    
                     end if	
                     
        
    if(alertId <> "" and request("edit") <> ""  ) then  
		 ' if(Request("rejectedEdit") = "1") then
		   '    Conn.Execute("UPDATE alerts SET approve_status = 0 WHERE id = " & alertId)
		  'elseif(Request("shouldApprove") = "1") then
		      '  Conn.Execute("UPDATE alerts SET approve_status = 1 WHERE id = " & alertId)
		 ' end if 
		 
		 senderUserId = uid
		if  Request("rejectedEdit") = "1" or Request("shouldApprove") = "1" then
		 Set senderIdSet = Conn.Execute("SELECT sender_id FROM alerts WHERE id = " & alertId)
		    senderUserId = senderIdSet("sender_id")
		end if
       Call updateAlert(alertId,alertHTML, title, fromDate, toDate, schedule, schedule_type, 0, 0, 0, 1, 0, "", 0, 1, 500, 400, 0, autoclose, 0, 1, 1, senderUserId , "NULL")
	else	
	    alertId = addAlertToDataBase(alertHTML,title,fromDate,toDate,schedule,schedule_type,0,0,0,1,0,"",0,1,500,400,0,autoclose,0,1,1,uid,"NULL",2, campaign_id, approve_status)
	end if
	approve = "0"
	
	
			    
	if (gid = 0 or screensavers_arr(6) = "checked") then
		 approve = "1"
	 end if
			    
	if(campaign_id = "-1") then
	    Response.Redirect "alert_users.asp?desktop=1&cur_step=3&steps_count=3&id="& CStr(alertId)&"&dup_id="&dup_id&"&return_page=screensavers.aspx&header="& LNG_SCREENSAVERS &"&title="& LNG_ADD_SCREENSAVER & "&approve=" & approve & "&shouldApprove=" & request("shouldApprove") & "&rejectedEdit=" & request("rejectedEdit")
    else
        Response.Redirect "alert_users.asp?desktop=1&cur_step=3&steps_count=3&id="& CStr(alertId)&"&dup_id="&dup_id&"&return_page=CampaignDetails.aspx?campaignid="& campaign_id &"&header="& LNG_SCREENSAVERS &"&title="& LNG_ADD_SCREENSAVER & "&campaign_id=" & campaign_id & "&approve=" & approve & "&shouldApprove=" & request("shouldApprove") & "&rejectedEdit=" & request("rejectedEdit")
    end if
else
	SetVar "fromDate", ""
	SetVar "toDate", ""
	SetVar "title", ""
	SetVar "autoclose", ""
	SetVar "alertId", alertId
	SetVar "screensaverVideoFile", ""
	SetVar "ScreensaverImgSrc", ""
	
	if (alertId <> "") then
	
		Set RS = Conn.Execute("SELECT title, alert_text, from_date, to_date, autoclose FROM alerts WHERE id ="&alertId&" AND class = 2")
		 
		if (not RS.EOF) then
			alertHTML = RS("alert_text")
			
			Set rexp = New RegExp
			With rexp
				.Pattern = "<!--.*?screensaver_type=""(.*)"".*?file=""(.*)"".*?-->"
				.IgnoreCase = True
				.Global = False
			End With
			
			set Matches = rexp.Execute(alertHTML)

			For Each Match in Matches
				For Each Submatch in Match.SubMatches
						SetVar "screensaverVideoFile", Match.SubMatches(1)
						SetVar "ScreensaverImgSrc", Match.SubMatches(1)
						SetVar "screensaver_video_tr_display", ""
						SetVar "screensaver_img_tr_display", ""
				Exit For
				Next
			Exit For
			Next
			alertHTML = rexp.Replace(alertHTML, "")
		
			fromDate = RS("from_date")
			if year(fromDate) = 1900 OR varType(fromDate) <= 1 then
				fromDate = ""
			end if
			
			toDate = RS("to_date")
			if year(toDate) = 1900 OR varType(toDate) <= 1 then
				toDate = ""
			end if
			
			title = RS("title")
			autoclose = RS("autoclose")
		end if
		RS.Close
			
		'if (autoclose <> "0" ) then
			'SetVar "time_to_display_checked", "checked"
			SetVar "time_to_display", autoclose
		'end if
		
		if (fromDate <> "") then
			SetVar "start_and_end_date_and_time_checked", "checked"
		end if
		
		SetVar "fromDate", FullDateTime(fromDate)
		SetVar "toDate", FullDateTime(toDate)
		
		if (isEdit = "1") then
			SetVar "alertId", alertId
			SetVar "rejectedEdit", Request("rejectedEdit")
			SetVar "shouldApprove", Request("shouldApprove")
		end if
		
		LoadTemplateFromString "ScreenSaverHtml", HtmlEncode(alertHTML)
		
		
	else
		Set objStream_ = Server.CreateObject("ADODB.Stream")
		objStream_.Open
		objStream_.CharSet = "UTF-8"
		objStream_.LoadFromFile alerts_dir & "\" & templateFolder & "\" & sScreensaverTemplateFileName
		LoadTemplateFromString "ScreenSaverHtml", HtmlEncode(objStream_.ReadText)
		Set objStream_ = Nothing
	end if
	
	SetHtmlVar "title", title
	SetVar "ContentsStr", LNG_CONTENTS
	SetVar "ScreensaverNameStr", LNG_NAME
	SetVar "OptionsStr", LNG_OPTIONS
	SetVar "TimeToDisplayStr", LNG_TIME_TO_DISPLAY
	SetVar "TimeToDisplayTitleStr", LNG_SCREENSAVER_TIME_TO_DISPLAY_TITLE
	SetVar "SecondsStr", LNG_SECONDS
	SetVar "StartAndEndDateAndTimeStr", LNG_START_AND_END_DATE_AND_TIME
	SetVar "StartDateAndTimeStr", LNG_START_DATE_AND_TIME
	SetVar "EndDateAndTimeStr", LNG_END_DATE_AND_TIME
	SetVar "SaveAndNextStr", LNG_SAVE_AND_NEXT
	SetVar "ScreensaverSampleTitle", LNG_SCREENSAVER_SAMPLE_TITLE
	SetVar "ScreensaverSampleContent", LNG_SCREENSAVER_SAMPLE_CONTENT
	SetVar "DeskAlertsAdminPath", alerts_folder 
end if



'-------------------------------
' Step through each form
'-------------------------------
Header_Show
Footer_Show
'-------------------------------
' Process page templates
'-------------------------------
Parse "Header", False
Parse "Footer", False
Parse "ScreenSaverHtml", False
Parse "main", False
'-------------------------------
' Output the page to the browser
'-------------------------------

Response.write PrintVar("main")

' index Show End

'-------------------------------
' Destroy all object variables
'-------------------------------

' index Close Event begin
' index Close Event End

UnloadTemplate
'===============================

else

Login(Request("intErrorCode"))

end if

'===============================
' Display Grid Form
'-------------------------------
%><!-- #include file="db_conn_close.asp" -->