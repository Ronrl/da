﻿using System;
using System.Text.RegularExpressions;

namespace DeskAlertsDotNet.Pages
{
    public partial class EditScreensavers : DeskAlertsBasePage
    {
        protected string screensaverType = "";
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (!string.IsNullOrEmpty(Request["alert_campaign"]))
            {
                var campaignId = Request["alert_campaign"];
                powerpointFrame.Src = "EditScreensaverPowerPoint.aspx?alert_campaign=" + campaignId;
                htmlFrame.Src = "EditScreensaversHtml.aspx?alert_campaign=" + campaignId;
                imageFrame.Src = "EditScreensaversImage.aspx?alert_campaign=" + campaignId;
                videoFrame.Src = "EditScreensaverVideo.aspx?alert_campaign=" + campaignId;
            }

            if (!string.IsNullOrEmpty(Request["id"]))
            {
                var id = Request["id"];
                var screensaverHtml = dbMgr.GetScalarQuery<string>("SELECT alert_text FROM alerts WHERE id = " + id);
                var regexPattern = "<!--.*?screensaver_type=\"(.*)\".*?file=\"(.*)\".*?-->";

                screensaverType = Regex.Match(screensaverHtml, regexPattern).Groups[1].Value;

                string redirectUrl;
                switch (screensaverType)
                {
                    case "image":
                        {
                            redirectUrl = "EditScreensaversImage.aspx?id=" + id;
                            break;
                        }
                    case "powerpoint":
                        {
                            redirectUrl = "EditScreensaverPowerPoint.aspx?id=" + id;
                            break;
                        }

                    case "video":
                        {
                            redirectUrl = "EditScreensaverVideo.aspx?id=" + id;
                            break;
                        }

                    default:
                        {
                            redirectUrl = "EditScreensaversHtml.aspx?id=" + id;
                            break;
                        }
                }

                bool shouldApprove = !string.IsNullOrEmpty(Request["shouldApprove"]) && Request["shouldApprove"] == "1";
                if (shouldApprove)
                {
                    redirectUrl += "&shouldApprove=1";
                }


                if (!string.IsNullOrEmpty(Request["alert_campaign"]))
                {
                    redirectUrl += ("alert_campaign=" + Request["alert_campaign"]);
                }

                Response.Redirect(redirectUrl);
            }
        }
    }
}