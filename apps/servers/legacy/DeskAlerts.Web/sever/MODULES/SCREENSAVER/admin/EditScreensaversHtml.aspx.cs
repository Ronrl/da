﻿using System;
using System.IO;
using DeskAlerts.Server.Utils;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Parameters;
using System.Globalization;
using DeskAlerts.Server.Utils.Helpers;

namespace DeskAlertsDotNet.Pages
{
    public partial class EditScreensaversHtml : DeskAlertsBasePage
    {
        //use curUser to access to current user of DA control panel      
        //use dbMgr to access to database

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            if (!string.IsNullOrEmpty(Request["alert_campaign"]))
            {
                nonCampaignDiv.Visible = false;

                string campaignName =
                    dbMgr.GetScalarByQuery<string>("SELECT name FROM campaigns WHERE id = " + Request["alert_campaign"]);

                campaignNameLabel.InnerText = campaignName;
                campaignSelect.Value = Request["alert_campaign"];
            }
            else
            {
                campaignDiv.Visible = false;
            }

            startDateTime.Value = DateTimeConverter.ToUiDateTime(DateTime.Now);
            endDateTime.Value = DateTimeConverter.ToUiDateTime(DateTime.Now.AddHours(1));

            string defaultHtml = File.ReadAllText(Config.Data.GetString("alertsFolder") + "\\admin\\templates\\screensaver_template_content.html");
            defaultHtml = defaultHtml.Replace("{DeskAlertsAdminPath}", Config.Data.GetString("alertsDir") + "/");
            screensaverContentArea.InnerHtml = defaultHtml;
            //Add your main code here

            if (!string.IsNullOrEmpty(Request["rejectedEdit"]))
            {
                rejectedEdit.Value = Request["rejectedEdit"];

            }
            if (!string.IsNullOrEmpty(Request["shouldApprove"]))
            {
                shouldApprove.Value = Request["shouldApprove"];

            }
            if (!string.IsNullOrEmpty(Request["edit"]))
            {
                edit.Value = Request["edit"];
            }

            if (!string.IsNullOrEmpty(Request["id"]))
            {
                alertId.Value = Request["id"];
                LoadScreensaver(Request["id"]);
            }
            else
            {
                form1.Attributes.Add("target", "_parent");
            }



        }

        void LoadScreensaver(string id)
        {
            DataRow screendaverRow = dbMgr.GetDataByQuery("SELECT * FROM alerts WHERE id = " + id).First();

            string alertText = screendaverRow.GetString("alert_text");
            string title = screendaverRow.GetString("title");

            string startDateStr = screendaverRow.GetString("from_date");
            string endDateStr = screendaverRow.GetString("to_date");

            DateTime startDate = screendaverRow.GetDateTime("from_date");
            DateTime endDate = screendaverRow.GetDateTime("to_date");
            int timeToDisplayVal = screendaverRow.GetInt("autoclose");

            screensaverHtml.Value = alertText;
            screensaverName.Value = title;

            timeToDisplay.Value = timeToDisplayVal.ToString();
            int campaignId = screendaverRow.GetInt("campaign_id");
            if (campaignId != -1)
            {
                nonCampaignDiv.Visible = false;

                string campaignName =
                    dbMgr.GetScalarByQuery<string>("SELECT name FROM campaigns WHERE id = " + campaignId);

                campaignNameLabel.InnerText = campaignName;
                campaignSelect.Value = campaignId.ToString();
            }

            if (startDate.Year != 1900)
            {
                startAndEndDateAndTimeCheckbox.Checked = true;
                startDateTime.Value = DateTimeConverter.ToUiDateTime(startDate);
            }

            if (endDate.Year != 1900)
            {
                startAndEndDateAndTimeCheckbox.Checked = true;
                endDateTime.Value = DateTimeConverter.ToUiDateTime(endDate);
            }

            screensaverContentArea.InnerHtml = alertText;
        }
    }
}