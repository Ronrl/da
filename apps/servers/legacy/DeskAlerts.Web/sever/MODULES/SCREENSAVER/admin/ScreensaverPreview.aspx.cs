﻿using System;

namespace DeskAlertsDotNet.Pages
{
    public partial class ScreensaverPreview : DeskAlertsBasePage
    {
        //use curUser to access to current user of DA control panel      
        //use dbMgr to access to database

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            var html = Request["html"];

            if (html.IndexOf("%ss_video%", StringComparison.Ordinal) > -1)
            {
                var videoParams = html.Split('|');

                html = $"<video src='{videoParams[1]}' width={videoParams[2]} height={videoParams[3]} type='video/mp4' preload autoplay muted controls loop>";
            }

            previewDiv.InnerHtml = html;
        }
    }
}