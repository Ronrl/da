﻿using Castle.MicroKernel.Lifestyle;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Utilities;
using DeskAlerts.Scheduler.ContentJobScheduling;
using DeskAlerts.Server.Utils.Helpers;
using DeskAlertsDotNet.Application;
using DeskAlertsDotNet.Models.Permission;
using DeskAlertsDotNet.Parameters;
using DeskAlertsDotNet.Utils.Consts;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace DeskAlertsDotNet.Pages
{
    public partial class AddScreensaver : DeskAlertsBasePage
    {
        private readonly IContentJobScheduler _contentJobScheduler;
        public static string FromDate = DateTimeUtils.MinSupportedDateTimeStr;

        public AddScreensaver()
        {
            using (Global.Container.BeginScope())
            {
                _contentJobScheduler = Global.Container.Resolve<IContentJobScheduler>();
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            var html = Request["screensaverHtml"];
            var name = Request["screensaverName"];
            var timeToDisplay = Request["timeToDisplay"];

            var startDate = FromDate;
            var endDate = DateTime.Now.Add(TimeSpan.FromDays(365)).ToString(DateTimeFormat.MsSqlFormat, CultureInfo.InvariantCulture);

            var campaignId = Request.GetParam("campaignSelect", -1);

            var screensaverDict = new Dictionary<string, object>();

            var isScheduled = !string.IsNullOrEmpty(Request["startAndEndDateAndTimeCheckbox"]);
            if (isScheduled)
            {
                startDate = DateTimeConverter.ConvertFromUiToDb(Request["startDateTime"]);
                endDate = DateTimeConverter.ConvertFromUiToDb(Request["endDateTime"]);
                screensaverDict.Add("schedule", 1);
            }
            else
            {
                screensaverDict.Add("schedule", 0);
            }

            screensaverDict.Add("alert_text", html);
            screensaverDict.Add("title", name);
            screensaverDict.Add("autoclose", timeToDisplay);
            screensaverDict.Add("create_date", DateTime.Now.ToString(DateTimeFormat.JavaScript, CultureInfo.InvariantCulture));
            screensaverDict.Add("from_date", startDate);
            screensaverDict.Add("to_date", endDate);
            screensaverDict.Add("campaign_id", campaignId);
            screensaverDict.Add("type", "D");
            screensaverDict.Add("sender_id", curUser.Id);

            string rejectedEdit = "";

            if (!string.IsNullOrEmpty(Request["rejectedEdit"]))
                rejectedEdit = Request["rejectedEdit"];

            string shouldApprove = "";
            if (!string.IsNullOrEmpty(Request["shouldApprove"]))
                shouldApprove = Request["shouldApprove"];

            string dupAlertId = "";

            if (!string.IsNullOrEmpty(Request["alertId"]))
                dupAlertId = Request["alertId"];

            int approveStatus = 1;

            if (Config.Data.HasModule(Modules.APPROVE) &&
                (!curUser.HasPrivilege && !curUser.Policy[Policies.SCREENSAVERS].CanApprove) && Settings.Content["ConfAllowApprove"] == "1")
                approveStatus = 0;

            screensaverDict.Add("approve_status", approveStatus);
            screensaverDict.Add("class", 2);


            int alertId;
            var isEdit = !string.IsNullOrEmpty(Request["edit"]) && Request["edit"].Equals("1");

            if ((string.IsNullOrEmpty(Request["shouldApprove"]) || !Request["shouldApprove"].Equals("1")) && !isEdit)
            {
                alertId = AlertManager.Default.AddAlert(dbMgr, screensaverDict);

                if (isScheduled)
                {
                    if (AppConfiguration.IsNewContentDeliveringScheme)
                    {
                        var dateTimeUtc = DateTimeConverter.ParseDateTimeFromUi(Request["startDateTime"]).ToUniversalTime();
                        var contentJobDto = new ContentJobDto(dateTimeUtc, alertId, AlertType.ScreenSaver);
                        _contentJobScheduler.CreateJob(contentJobDto);
                    }

                    var reccurenceDto = new Dictionary<string, object>
                    {
                        {"alert_id", alertId},
                        {"pattern", "o"},
                        {"countdowns", new List<string>()}
                    };

                    ReccurenceManager.ProcessReccurence(dbMgr, reccurenceDto);
                }
            }
            else
            {
                alertId = Convert.ToInt32(Request["alertId"]);
                AlertManager.Default.UpdateAlert(dbMgr, screensaverDict, alertId);
            }

            string redirectUrl;
            if (campaignId > 0)
            {
                redirectUrl = $"RecipientsSelection.aspx?id={alertId}&desktop=1&return_page=CampaignDetails.aspx?campaignId={campaignId}";
            }
            else
            {
                redirectUrl = "RecipientsSelection.aspx?id=" + alertId + "&desktop=1&return_page=Screensavers.aspx&campaign_id=" + campaignId + "&rejectedEdit=" + rejectedEdit + "&shouldApprove=" + shouldApprove + "&dup_id=" + dupAlertId;
            }
            

            Response.Redirect(redirectUrl, true);
        }
    }
}