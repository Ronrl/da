﻿<%@ page language="C#" autoeventwireup="true" codebehind="EditScreensaverVideo.aspx.cs" inherits="DeskAlertsDotNet.Pages.EditScreensaversVideo" %>

<%@ Import Namespace="DeskAlerts.Server.Utils" %>
<%@ Import Namespace="DeskAlerts.Server.Utils.Helpers" %>
<%@ import namespace="DeskAlertsDotNet.Parameters" %>
<%@ Import Namespace="Resources" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="style.css" rel="stylesheet" type="text/css">
    <script language="javascript" type="text/javascript" src="jscripts/tiny_mce/tinymce.min.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>

    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>

    <link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css">
    <link href="css/style9.css" rel="stylesheet" type="text/css">
    <link href="css/jquery/jquery_timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css">

    <script language="javascript" type="text/javascript" src="jscripts/preview.js?v=9.2.1.31"></script>
    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery_timepicker/jquery-ui-timepicker-addon.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/jquery/datepicker_fix.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/json2.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/date.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/preview.js?v=9.2.1.31"></script>
    <script language="javascript" type="text/javascript" src="jscripts/jquery.jqplot.min.js"></script>
    <script language="javascript" type="text/javascript" src="functions.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/jquery.hotkeys.js"></script>
    <script language="javascript" type="text/javascript">
        function isPositiveInteger(n) {
            return (/^\d+$/.test(n + ''));
        }

        function showPrompt(text) {
            $("#dialog-modal > p").text(text)
            $("#dialog-modal").dialog({
                height: 150,
                modal: true,
                resizable: false,
                buttons: {
                    "<% =resources.LNG_CLOSE %>": function () {
                        $(this).dialog("close");
                    }
                }
            });
        }

        $(window)
            .bind('keydown',
                'Alt+p',
                function () {

                    showScreensaverPreview();

                }
            );

        $(window)
            .bind('keydown',
                'right',
                function () {

                    saveScreensaver();

                }
            );

        $(window)
            .bind('keydown',
                'Ctrl+o',
                function () {

                    openFileBrowser();

                }
            );

        function toggleDisable(checkbox, selector) {
            if (!$(checkbox).is(':checked')) {
                $(selector + ' :input').prop('disabled', true);
            } else {
                $(selector + ' :input').removeProp('disabled');
            }
        }

        function saveScreensaver() {
            var autoclose = $.trim($("#timeToDisplay").val());
            var fromDate = $.trim($("#startDateTime").val());
            var toDate = $.trim($("#endDateTime").val());
            var videoSrc = $("#screensaverVideoSrc").val();
            var screensaverHtml = "<!-- screensaver_type=\"video\" file=\"" + videoSrc + "\"-->"
                + makeScreensaverHtml();
            //if ($("#time_to_display_checkbox").is(':checked'))
            //{
            if ($("#screensaverName").val()) {
                $("#title").val($("#screensaverName").val());
            }
            else {
                showPrompt("<% =resources.LNG_SPECIFY_NAME %>");
                return;
            }

            $("#campaign_id").val($("#campaign_select").val());

            if (!isPositiveInteger(autoclose)) {


                showPrompt("<% =resources.LNG_TIME_TO_DISPLAY_IS_NOT_VALID %>");
                return;
            }
            else {
                $("#autoclose").val(autoclose);
            }
            //}
            if ($("#startAndEndDateAndTimeCheckbox").is(':checked')) {
                if (!isDate(fromDate, uiFormat)) {

                    showPrompt("<% =resources.LNG_FROM_DATE_AND_TIME_IS_NOT_VALID %>");
                    return;
                }
                else {
                    if (fromDate != "") {
                        fromDate = new Date(getDateFromFormat(fromDate, uiFormat));
                        $("#from_date").val(formatDate(fromDate, saveDbFormat));
                    }
                }

                if (!isDate(toDate, uiFormat)) {

                    showPrompt("<% =resources.LNG_TO_DATE_AND_TIME_IS_NOT_VALID %>");

                    return;
                }
                else {
                    if (toDate != "") {
                        toDate = new Date(getDateFromFormat(toDate, uiFormat))
                        $("#to_date").val(formatDate(toDate, saveDbFormat));
                    }
                }
                if (fromDate < serverDate && toDate < serverDate) {
                    showPrompt("<% =resources.LNG_DATE_IN_THE_PAST %>");
                    return;
                }
                if (fromDate >= toDate) {
                    showPrompt("<% =resources.LNG_REC_DATE_ERROR %>");
                    return;
                }
            }
            if (videoSrc == "") {

                showPrompt("<% =resources.LNG_YOU_SHOULD_CHOOSE_SCREENSAVER_VIDEO_FILE %>");
                return;
            }

            $("#screensaverHtml").val(screensaverHtml);
            $("#form1").submit();
        }

        function openFileBrowser() {
            var params = 'width=550';
            params += ', height=550';
            params += ', top=0, left=0';
            params += ', fullscreen=no';

            //openDialogUI('uploadform_video.asp',400,550);
            window.open('uploadform.aspx?size=0', 'preview_win', params);
        }

        function fileBrowserSetSrc(id, name, src, width, height) {
            $("#screensaverVideoTr").fadeIn(500);
            $("#screensaverVideoSrc").val(src);
        }

        function makeScreensaverHtml() {
            var html = "%ss_video%=|" + $("#screensaverVideoSrc").val() + "|100%|100%|";
            return html;
        }

        function showScreensaverPreview() {
            if ($("#screensaverVideoSrc").val() == "") {
                showPrompt("<% =resources.LNG_YOU_SHOULD_CHOOSE_SCREENSAVER_VIDEO_FILE %>");
                return;
            }
            var html = makeScreensaverHtml();

            $("#preview_html").val(html);

            var params = 'width=' + screen.width;
            params += ', height=' + screen.height;
            params += ', top=0, left=0';
            params += ', fullscreen=yes';

            //openDialogUI('',screen.width,screen.height);

            window.open('', 'preview_win', params);

            $("#preview_post_form").submit();
            parent.hideLoader();
        }

        function setDraftValue(val) {
            $("#isDraft").val(val);
        }

        var serverDate;
        var settingsDateFormat = "<% = DateTimeConverter.JsDateFormat %>";
        var settingsTimeFormat = "<% = DateTimeConverter.JsTimeFormat %>";

        var responseDbFormat = "dd/MM/yyyy HH:mm:ss";
        var saveDbFormat = "dd/MM/yyyy HH:mm";
        var saveDbDateFormat = "dd/MM/yyyy";
        var saveDbTimeFormat = "HH:mm";
        var uiFormat = "<% = DateTimeConverter.JsDateTimeFormat %>";

        $(document).ready(function () {
            $(".header_title").parent().prepend("<img src='images/menu_icons/screensaver_20.png' alt='' width='20' height='20' border='0' style='padding-left:7px'>")
            serverDate = new Date(getDateFromAspFormat("<% =DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")%>", responseDbFormat));
            setInterval(function () { serverDate.setSeconds(serverDate.getSeconds() + 1); }, 1000);
            $(".date_time_param_input")
                .datetimepicker({ dateFormat: settingsDateFormat, timeFormat: settingsTimeFormat });
            $("#screensaver_tabs").tabs();
            var fromDate = $.trim($("#startDateTime").val());
            var toDate = $.trim($("#endDateTime").val());
            if (fromDate != "" && isDate(fromDate, responseDbFormat)) {
                $("#startDateTime").val(formatDate(new Date(getDateFromFormat(fromDate, responseDbFormat)), uiFormat));
            }

            if (toDate != "" && isDate(toDate, responseDbFormat)) {
                $("#endDateTime").val(formatDate(new Date(getDateFromFormat(toDate, responseDbFormat)), uiFormat));
            }

            $("#save_and_next_button").button().click(function () {
                saveScreensaver();
            });

            $("#save_button").button().click(function () {
                setDraftValue(true);
                saveScreensaver();
            });

            $("#preview_button").button().click(function () {
                showScreensaverPreview();
            });

            $("#choose_screensaver_button").button().click(function () {
                openFileBrowser();
            });
            $("#startAndEndDateAndTimeCheckbox").change(function () {
                toggleDisable(this, "#start_and_end_date_and_time_div");
            }).change();
            var cur_src = $("#screensaver_src").val();
            if (cur_src) {
                setVideoFile(cur_src);
            }

            //  $("#form1").attr('target', '_parent');
            $("#choose_screensaver_button").attr('target', '_parent');


        });
    </script>
</head>
<body>
    <form id="form1" runat="server" action="AddScreensaver.aspx" method="POST">
        <div>
            <input runat="server" name="screensaverType" id="screensaverType" type="hidden" value="video" />
            <input runat="server" name="screensaverHtml" id="screensaverHtml" type="hidden" />
            <input runat="server" name="rejectedEdit" id="rejectedEdit" value="" type="hidden" />
            <input runat="server" name="shouldApprove" id="shouldApprove" value="" type="hidden" />
            <input runat="server" name="alert_id" id="alertId" type="hidden" />
            <input runat="server" name='edit' id="edit" value="" type="hidden" />
            <input runat="server" name='isDraft' id="isDraft" value="" type="hidden" />
            <table width="100%" border="0" cellspacing="0" height="100%" cellpadding="6" class="body">
                <tr>
                    <td valign="top">
                        <table width='100%' cellspacing='0' cellpadding='0' class='body'>
                            <tr>
                                <td bgcolor="#ffffff">
                                    <div style="margin: 10px">
                                        <table style="width: 98%; height: 100%; border-collapse: collapse; border: 0px; margin: 0px; padding: 0px;">
                                            <tr style="height: 45px">
                                                <td class="screensaver_name_td" style="text-align: left; padding: 0px;">
                                                    <div style="padding: 2px 0px">
                                                        <div id="campaignDiv" runat="server">
                                                            <b><%=resources.LNG_CAMPAIGN %>  :</b>
                                                            <label><b runat="server" id="campaignNameLabel"></b></label>
                                                            <input type='hidden' runat="server" id='campaignSelect' name='campaign_select' value='' />
                                                        </div>
                                                        <div runat="server" id="nonCampaignDiv">
                                                            <input type='hidden' name='campaign_select' value='-1'>
                                                        </div>

                                                    </div>
                                                    <div style="padding: 2px 0px"><% =resources.LNG_STEP %> <strong>1</strong> <% =resources.LNG_OF %> &nbsp;2 : <strong><% =resources.LNG_ADD_SCREENSAVER %> :</strong></div>
                                                    <br />
                                                    <div style="padding: 2px 0px">
                                                        <label for="screensaverName"><% =resources.LNG_NAME %> :</label>
                                                        <input runat="server" type="text" name="screensaver_name" id="screensaverName" value=""></input>
                                                    </div>
                                                    <br />

                                                </td>
                                            </tr>
                                            <tr style="">
                                                <td class="tabs_container" style="text-align: left; vertical-align: top; padding: 0px">
                                                    <div id="screensaver_tabs" style="">
                                                        <ul>
                                                            <li><a href="#content_tab"><%=resources.LNG_CONTENTS %> </a></li>
                                                            <li><a href="#options_tab"><%=resources.LNG_OPTIONS %></a></li>
                                                        </ul>
                                                        <div id="content_tab" style="padding: 0px; margin: 10px;">
                                                            <table style="border-collapse: collapse; border: 0px; margin: 0px; padding: 0px;">
                                                                <tr style="">
                                                                    <td style="vertical-align: top; text-align: right; font-weight: bold"></td>
                                                                    <td style="vertical-align: top; text-align: left; padding: 0px 10px 0px 10px">
                                                                        <h3>
                                                                            <%=resources.LNG_SCREENSAVER_VIDEO_INSTRUCTIONS %>
                                                                        </h3>
                                                                    </td>
                                                                </tr>
                                                                <tr runat="server" id="screensaverVideoTr" style="display: none">
                                                                    <td style="vertical-align: top; text-align: right; font-weight: bold">
                                                                        <%=resources.LNG_VIDEO_FILE %>:
                                                                    </td>
                                                                    <td style="vertical-align: top; text-align: left; padding: 0px 10px 0px 10px">
                                                                        <input runat="server" type="text" style="width: 600px" id="screensaverVideoSrc" value="" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td></td>
                                                                    <td style="vertical-align: top; text-align: right; padding: 10px">
                                                                        <table style="border-collapse: collapse; border: 0px; margin: 0px; padding: 0px; width: 100%">
                                                                            <tr>
                                                                                <td style="padding: 0px; text-align: left">
                                                                                    <a style="white-space: nowrap" id="choose_screensaver_button"><%=resources.LNG_CHOOSE_VIDEO_FILE %></a>
                                                                                </td>
                                                                                <td style="padding: 2px; text-align: right; width: 7px;"></td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div id="options_tab">

                                                            <div>
                                                                <input id="time_to_display_checkbox" type="hidden"><%=resources.LNG_TIME_TO_DISPLAY %>:</input>&nbsp;
							<input runat="server" id="timeToDisplay" name="timeToDisplay" type="text" style="width: 30px" value="60"></input>&nbsp;
							<label><% =resources.LNG_SECONDS %></label>
                                                                <p style="color: #aaaaaa">
                                                                    <%=resources.LNG_SCREENSAVER_TIME_TO_DISPLAY_TITLE %>
                                                                </p>
                                                            </div>
                                                            <div style="padding-top: 10px">
                                                                <input runat="server" id="startAndEndDateAndTimeCheckbox" name="start_and_end_date_and_time_checkbox" type="checkbox"></input><label for="startAndEndDateAndTimeCheckbox"><%=resources.LNG_START_AND_END_DATE_AND_TIME %></label>
                                                                <div id="start_and_end_date_and_time_div" style="padding-top: 5px">
                                                                    <table style="width: 300px; height: 100px; border-collapse: collapse; border: 0px; margin: 0px; padding: 0px;">
                                                                        <tr>
                                                                            <td style="text-align: right">
                                                                                <label for="startDateTime"><%=resources.LNG_START_DATE_AND_TIME %>:</label>
                                                                            </td>
                                                                            <td>
                                                                                <input runat="server" id="startDateTime" class="date_time_param_input" name="start_date_and_time" type="text" value=""></input>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="text-align: right">
                                                                                <label for="endDateTime"><%=resources.LNG_END_DATE_AND_TIME %>:</label>
                                                                            </td>
                                                                            <td>
                                                                                <input runat="server" id="endDateTime" class="date_time_param_input" name="end_date_and_time" type="text" value=""></input>
                                                                            </td>
                                                                        </tr>
                                                                    </table>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </tr>
                                            <tr style="height: 30px">
                                                <td class="wallpaper_name_td" style="text-align: right; padding: 0px; padding-top: 10px;">
                                                    <a id="preview_button"><% =resources.LNG_PREVIEW %></a>
                                                    <a id="save_button"><% =resources.LNG_SAVE %></a>
                                                    <a id="save_and_next_button"><% =resources.LNG_SAVE_AND_NEXT %></a>
                                                    <br />
                                                    <br />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                            </tr>
                        </table>
                    </td>
            </table>
        </div>
    </form>
</body>

<div id="dialog-modal" title="" style='display: none'>
    <p></p>
</div>
<form style="margin: 0px; width: 0px; height: 0px; padding: 0px" id="preview_post_form" action="ScreensaverPreview.aspx" method="post" target="preview_win">
    <input type="hidden" id="preview_html" name="html" value="" />
</form>
</html>
