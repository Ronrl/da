﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Web.UI;
using DeskAlerts.Server.Utils;
using DeskAlerts.Server.Utils.Helpers;
using DeskAlertsDotNet.DataBase;

namespace DeskAlertsDotNet.Pages
{
    public partial class EditScreensaverPowerPoint : DeskAlertsBasePage
    {
        //use curUser to access to current user of DA control panel      
        //use dbMgr to access to database

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            if (!string.IsNullOrEmpty(Request["alert_campaign"]))
            {
                nonCampaignDiv.Visible = false;

                string campaignName =
                    dbMgr.GetScalarByQuery<string>("SELECT name FROM campaigns WHERE id = " + Request["alert_campaign"]);

                campaignNameLabel.InnerText = campaignName;
                campaignSelect.Value = Request["alert_campaign"];
            }
            else
            {
                campaignDiv.Visible = false;
            }

            startDateTime.Value = DateTimeConverter.ToUiDateTime(DateTime.Now);
            endDateTime.Value = DateTimeConverter.ToUiDateTime(DateTime.Now.AddHours(1));

            if (!string.IsNullOrEmpty(Request["rejectedEdit"]))
                rejectedEdit.Value = Request["rejectedEdit"];

            if (!string.IsNullOrEmpty(Request["shouldApprove"]))
                shouldApprove.Value = Request["shouldApprove"];

            if (!string.IsNullOrEmpty(Request["edit"]))
            {
                edit.Value = Request["edit"];
            }

            if (!string.IsNullOrEmpty(Request["id"]))
            {
                alertId.Value = Request["id"];
                LoadScreensaver(Request["id"]);
            }
            else
            {
                form1.Attributes.Add("target", "_parent");
            }

            //Add your main code here
        }

        void LoadScreensaver(string id)
        {
            DataRow screendaverRow = dbMgr.GetDataByQuery("SELECT * FROM alerts WHERE id = " + id).First();

            string alertText = screendaverRow.GetString("alert_text");
            string title = screendaverRow.GetString("title");

            string startDateStr = screendaverRow.GetString("from_date");
            string endDateStr = screendaverRow.GetString("to_date");

            DateTime startDate = screendaverRow.GetDateTime("from_date");
            DateTime endDate = screendaverRow.GetDateTime("to_date");
            int timeToDisplayVal = screendaverRow.GetInt("autoclose");

            screensaverHtml.Value = alertText;
            screensaverName.Value = title;

            timeToDisplay.Value = timeToDisplayVal.ToString();

            int campaignId = screendaverRow.GetInt("campaign_id");
            if (campaignId != -1)
            {
                nonCampaignDiv.Visible = false;

                string campaignName =
                    dbMgr.GetScalarByQuery<string>("SELECT name FROM campaigns WHERE id = " + campaignId);

                campaignNameLabel.InnerText = campaignName;
                campaignSelect.Value = campaignId.ToString();
            }

            if (startDate.Year != 1900)
            {
                startAndEndDateAndTimeCheckbox.Checked = true;
                startDateTime.Value = DateTimeConverter.ToUiDateTime(startDate);
            }

            if (endDate.Year != 1900)
            {
                startAndEndDateAndTimeCheckbox.Checked = true;
                endDateTime.Value = DateTimeConverter.ToUiDateTime(endDate);
            }


            string regexPattern = "<!--.*?screensaver_type=\"(.*)\".*?file=\"(.*)\".*?-->";

            string screensaverFile
                = Regex.Match(alertText, regexPattern).Groups[2].Value;


            screensaverVideoTr.Style.Add(HtmlTextWriterStyle.Display, "");
            screensaverVideoSrc.Value = screensaverFile;
        }
    }
}