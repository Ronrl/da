﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditScreensavers.aspx.cs" Inherits="DeskAlertsDotNet.Pages.EditScreensavers" %>
<%@ Import NameSpace="DeskAlertsDotNet.Parameters" %>
<%@ Import NameSpace="Resources"%>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="style.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="jscripts/tiny_mce/tinymce.min.js"></script>
    <script type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
	<script type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
	<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css">
	<link href="css/style9.css" rel="stylesheet" type="text/css">
	<link href="css/jquery/jquery_timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="jscripts/preview.js?v=9.2.1.31"></script>
	<script type="text/javascript" src="jscripts/jquery/jquery_timepicker/jquery-ui-timepicker-addon.js"></script>
	<script type="text/javascript" src="jscripts/jquery/datepicker_fix.js"></script>
	<script type="text/javascript" src="jscripts/json2.js"></script>
	<script type="text/javascript" src="jscripts/date.js"></script>
	<script type="text/javascript" src="jscripts/preview.js?v=9.2.1.31"></script>
	<script type="text/javascript" src="jscripts/jquery.jqplot.min.js"></script>
	<script type="text/javascript" src="functions.js"></script>
    

    <script type="text/javascript">
        $(document).ready(function () {

            var height = $(window.parent.parent.document).height();
            window.parent.parent.onChangeBodyHeight(height, false);

            $("#screensaver_table").tabs();
            resizeAllframes();

           var type = "<% =screensaverType %>";

            if (type != "") {

                if (type == "image") {
                    $('#screensaver_table').tabs("option", "active", 1);
                } else if (type == "powerpoint") {
                    $('#screensaver_table').tabs("option", "active", 0);
                } else {
                    $('#screensaver_table').tabs("option", "active", 2);
                }
            }
    });



        function resizeIframe(id) {
            var iframeDoc = document.getElementById(id);
            iframeDoc.height = window.frameElement.scrollHeight - 150;
            var h;
            if (iframeDoc.contentDocument) {
                h = iframeDoc.contentDocument.documentElement.scrollHeight; //FF, Opera, and Chrome
                h = iframeDoc.contentDocument.body.scrollHeight; //ie 8-9-10
                if (iframeDoc.height < h) iframeDoc.height = h;
            } else {
                h = iframeDoc.contentWindow.document.body.scrollHeight; //IE6, IE7 and Chrome
                if (iframeDoc.height < h) iframeDoc.height = h;
            }
        }

        function resizeAllframes() {
            var height = window.frameElement.scrollHeight - 150;
            document.getElementById('powerpointFrame').height = height;
            document.getElementById('imageFrame').height = height;
            document.getElementById('htmlFrame').height = height;
            document.getElementById('videoFrame').height = height;
        }

        $(document)
            .bind('keydown',
                'Alt+1',
                function () {
                    $('#screensaver_table').tabs("option", "active", 0);

                });

        $(document)
            .bind('keydown',
                'Alt+2',
                function () {
                    $('#screensaver_table').tabs("option", "active", 1);

                });

        $(document)
            .bind('keydown',
                'Alt+3',
                function () {
                    $('#screensaver_table').tabs("option", "active", 2);
                });


    </script>
</head>
<body>
    <form id="form1" runat="server" >
        <input type="hidden" name="alertId" runat="server" value="" />
    <div>
        <table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
         <tr>
            <td valign="top">
                <table width="100%" cellspacing="0" cellpadding="0" class="main_table">
                         <tr>
   			                <td width="100%" height="31" class="main_table_title"><img src="images/menu_icons/screensaver_20.png" alt="" width="20" height="20" border="0" style="padding-left:7px">
		                    <a href="EditScreensavers.aspx" class="header_title" style="position:absolute;top:22px"><%=resources.LNG_SCREENSAVERS %></a></td>             
                         </tr>
                         <tr>
                              <td class="main_cell" id="main_content_cell" height="100%" >
 	                                 <!-- <table style="width:100%; height:100%;  border-collapse:collapse; border:0px; margin:0px; padding:0px; "> -->
                                         <table class="main_table_body" width="100%" scrolling="auto" height="100%">
                                        <tr><td>
                                        <div id="screensaver_table"  style="padding:0px;height:100%;width:100%;border:none; ">
		                                                    <ul>
			                                                    <li><a href="#screensaver_powerpoint_button" onclick="resizeIframe('if_powerpoint_button');"><% =resources.LNG_SCREENSAVER_FROM_POWERPOINT  %><img border=0 style="cursor:pointer; cursor: hand;padding-left: 5px;" src="images/help.png" title="<%=resources.LNG_SCREENSAVER_FROM_POWERPOINT_HINT %>"/></a></li>   			
			                                                    <li><a href="#screensaver_image_button"  onclick="resizeIframe('if_image_button');" ><% =resources.LNG_SCREENSAVER_IMAGE_BASED %><img border=0 style="cursor:pointer; cursor: hand;padding-left: 5px;" src="images/help.png" title="<%=resources.LNG_SCREENSAVER_IMAGE_BASED_HINT %>"/></a></li>
                                                                <li><a href="#screensaver_video_button"  onclick="resizeIframe('if_video_button');" ><% =resources.LNG_SCREENSAVER_VIDEO_BASED %><img border=0 style="cursor:pointer; cursor: hand;padding-left: 5px;" src="images/help.png" title="<%=resources.LNG_SCREENSAVER_VIDEO_BASED_HINT %>"/></a></li>
			                                                    <li><a href="#screensaver_html_button"  onclick="resizeIframe('if_html_button');"><%=resources.LNG_SCREENSAVER_HTML_BASED %> <img border=0 style="cursor:pointer; cursor: hand;padding-left: 5px;" src="images/help.png" title="<%=resources.LNG_SCREENSAVER_HTML_BASED_HINT %>"/></a></li>           						           			
		                                                    </ul>
		                                                    <a id="screensaver_powerpoint_button"><iframe runat="server" id="powerpointFrame" frameborder="no" style="width:100%;" src="EditScreensaverPowerPoint.aspx"></iframe></a>
		                                                    <a id="screensaver_image_button"><iframe  runat="server" id="imageFrame" frameborder="no" style=" width:100%;" src="EditScreensaversImage.aspx"></iframe></a>
                                                            <a id="screensaver_video_button"><iframe  runat="server" id="videoFrame" frameborder="no" style=" width:100%;" src="EditScreensaverVideo.aspx"></iframe></a>      
		                                                    <a id="screensaver_html_button"><iframe  runat="server" id="htmlFrame" frameborder="no"  style="width:100%;" src="EditScreensaversHtml.aspx"></iframe></a>
                                            </div>
	                                        </td></tr>
                                        </table>                                    
                                      
                                    <!--  </table> -->
                              </td>                             
                         </tr>
                </table>
            </td>
         </tr>
         </table>    
    </div>
    </form>
</body>
</html>
