<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%
check_session()
%>
<!-- #include file="Common.asp" -->
<!-- #include file="header.asp" -->
<!-- #include file="footer.asp" -->

<%

campaign = Request("alert_campaign")
'On Error Resume Next
' index CustomIncludes End
'-------------------------------

'===============================
' Save Page and File Name available into variables
'-------------------------------
Dim sFileName
Dim sTemplateFileName
Dim intSessionUserId

sFileName = "screensaver_type_choose.asp"
sTemplateFileName = "screensaver_type_choose.html"
 
  
intSessionUserId = Session("uid")
uid = intSessionUserId
if (intSessionUserId <> "") then


' index Show begin

'===============================
' Display page
'-------------------------------
' Load HTML template for this page
'-------------------------------
LoadTemplate alerts_dir & "\" & templateFolder & "\" & sTemplateFileName, "main"

'-------------------------------
' Load HTML template of Header and Footer
'-------------------------------
sHeaderFileName=Replace(sHeaderFileName,"header.html","header-old.html")
LoadTemplate sHeaderFileName, "Header"
LoadTemplate sFooterFileName, "Footer"
'-------------------------------
SetVar "Menu", ""
SetVar "DateForm", ""

SetVar "LNG_STEP", LNG_STEP
SetVar "LNG_OF", LNG_OF

SetVar "LNG_ADD_SCREENSAVER", LNG_ADD_SCREENSAVER
SetVar "LNG_TITLE", LNG_TITLE
SetVar "LNG_DATE", LNG_DATE
SetVar "default_lng", lcase(default_lng)
SetVar "timezone_db", timezone_db
SetVar "sFileName", sFileName
SetVar "strPageName", LNG_SCREENSAVERS
SetVar "PowerPointScreensaverStr", LNG_SCREENSAVER_FROM_POWERPOINT
SetVar "ImageScreensaverStr", LNG_SCREENSAVER_IMAGE_BASED
SetVar "HtmlScreensaverStr", LNG_SCREENSAVER_HTML_BASED
SetVar "VideoScreensaverStr", LNG_SCREENSAVER_VIDEO_BASED
SetVar "screensaver_edit_url_powerpoint", "edit_screensavers.asp?type=powerpoint&needtable=1&alert_campaign="&campaign
SetVar "screensaver_edit_url_image", "edit_screensavers.asp?type=image&needtable=1&alert_campaign="&campaign
SetVar "screensaver_edit_url_html", "edit_screensavers.asp?type=html&needtable=1&alert_campaign="&campaign
SetVar "screensaver_edit_url_video", "edit_screensavers.asp?type=video&needtable=1&alert_campaign="&campaign
SetVar "LNG_SCREENSAVER_FROM_POWERPOINT_HINT", LNG_SCREENSAVER_FROM_POWERPOINT_HINT
SetVar "LNG_SCREENSAVER_IMAGE_BASED_HINT", LNG_SCREENSAVER_IMAGE_BASED_HINT
SetVar "LNG_SCREENSAVER_HTML_BASED_HINT", LNG_SCREENSAVER_HTML_BASED_HINT
SetVar "LNG_SCREENSAVER_VIDEO_BASED_HINT", LNG_SCREENSAVER_VIDEO_BASED_HINT
'--------------------



'-------------------------------
' Step through each form
'-------------------------------
Header_Show
Footer_Show
'-------------------------------
' Process page templates
'-------------------------------
Parse "Header", False
Parse "Footer", False
Parse "ScreenSaverHtml", False
Parse "main", False
'-------------------------------
' Output the page to the browser
'-------------------------------

Response.write PrintVar("main")

' index Show End

'-------------------------------
' Destroy all object variables
'-------------------------------

' index Close Event begin
' index Close Event End

UnloadTemplate
'===============================

else

Login(Request("intErrorCode"))

end if

'===============================
' Display Grid Form
'-------------------------------
%><!-- #include file="db_conn_close.asp" -->