﻿<%@ page language="C#" autoeventwireup="true" codebehind="EditScreensaversHtml.aspx.cs" inherits="DeskAlertsDotNet.Pages.EditScreensaversHtml" %>

<%@ Import Namespace="DeskAlerts.Server.Utils" %>
<%@ Import Namespace="DeskAlerts.Server.Utils.Helpers" %>
<%@ import namespace="DeskAlertsDotNet.Parameters" %>
<%@ Import Namespace="Resources" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="style.css" rel="stylesheet" type="text/css">
    <script language="javascript" type="text/javascript" src="jscripts/tiny_mce/tinymce.min.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>

    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>

    <link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css">
    <link href="css/style9.css" rel="stylesheet" type="text/css">
    <link href="css/jquery/jquery_timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css">
    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery_timepicker/jquery-ui-timepicker-addon.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/jquery/datepicker_fix.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/json2.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/date.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/jquery.jqplot.min.js"></script>
    <script language="javascript" type="text/javascript" src="functions.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/jquery.hotkeys.js"></script>
    <script language="javascript">

        //  initTinyMCE("EN", "textareas", "", null, null, { "content_css": "css/screensaver.css" });

        $(window)
            .bind('keydown',
                'Alt+p',
                function () {

                    showScreensaverPreview();

                }
            );

        $(window)
            .bind('keydown',
                'right',
                function () {

                    saveScreensaver();

                }
            );

        $(window)
            .bind('keydown',
                'Ctrl+o',
                function () {

                    openFileBrowser();

                }
            );
        function isPositiveInteger(n) {
            return (/^\d+$/.test(n + ''));
        }

        function toggleDisable(checkbox, selector) {
            if (!$(checkbox).is(':checked')) {
                $(selector + ' :input').prop('disabled', true);
            } else {
                $(selector + ' :input').removeProp('disabled');
            }
        }

        function showPrompt(text) {
            $("#dialog-modal > p").text(text);
            $("#dialog-modal").dialog({
                height: 150,
                modal: true,
                resizable: false,
                buttons: {
                    "<% =resources.LNG_CLOSE %>": function () {
                        $(this).dialog("close");
                    }
                }
            });
        }

        function showScreensaverPreview() {
            var html = tinyMCE.get('screensaverContentArea').getContent();
            $("#preview_html").val(html);

            var params = 'width=' + screen.width;
            params += ', height=' + screen.height;
            params += ', top=0, left=0'
            params += ', fullscreen=yes';

            //openDialogUI('',screen.width,screen.height);
            window.open('', 'preview_win', params);

            $("#preview_post_form").submit();
        }

        function setDraftValue(val) {
            $("#isDraft").val(val);
        }

        function saveScreensaver() {
            var autoclose = $.trim($("#timeToDisplay").val());
            var fromDate = $.trim($("#startDateTime").val());
            var toDate = $.trim($("#endDateTime").val());
            var screensaverHtml = tinyMCE.get('screensaverContentArea').getContent();
            //if ($("#time_to_display_checkbox").is(':checked'))
            //{
            if ($("#screensaverName").val()) {
                $("#title").val($("#screensaverName").val());
            }
            else {
                showPrompt("<% =resources.LNG_SPECIFY_NAME %>");
                return;
            }
            $("#campaign_id").val($("#campaign_select").val());
            if (!isPositiveInteger(autoclose)) {
                showPrompt("<% =resources.LNG_TIME_TO_DISPLAY_IS_NOT_VALID %>");
                return;
            }
            else {
                $("#autoclose").val(autoclose);
            }
            //}
            if ($("#startAndEndDateAndTimeCheckbox").is(':checked')) {
                if (!isDate(fromDate, uiFormat)) {
                    showPrompt("<% =resources.LNG_FROM_DATE_AND_TIME_IS_NOT_VALID %>");
                    return;
                }
                else {
                    if (fromDate != "") {
                        fromDate = new Date(getDateFromFormat(fromDate, uiFormat));
                        $("#from_date").val(formatDate(fromDate, saveDbFormat));
                    }
                }

                if (!isDate(toDate, uiFormat)) {
                    showPrompt("<% =resources.LNG_TO_DATE_AND_TIME_IS_NOT_VALID %>");
                    return;
                }
                else {
                    if (toDate != "") {
                        toDate = new Date(getDateFromFormat(toDate, uiFormat))
                        $("#to_date").val(formatDate(toDate, saveDbFormat));
                    }
                }
                if (fromDate < serverDate && toDate < serverDate) {
                    showPrompt("<% =resources.LNG_DATE_IN_THE_PAST %>");
                    return;
                }
                if (fromDate >= toDate) {
                    showPrompt("<% =resources.LNG_REC_DATE_ERROR %>");
                    return;
                }
            }

            $("#screensaverHtml").val(screensaverHtml);
            $("#form1").submit();
        }

        var serverDate;
        var settingsDateFormat = "<% = DateTimeConverter.JsDateFormat %>";
        var settingsTimeFormat = "<% = DateTimeConverter.JsTimeFormat %>";

        var responseDbFormat = "dd/MM/yyyy HH:mm:ss";
        var saveDbFormat = "dd/MM/yyyy HH:mm";
        var saveDbDateFormat = "dd/MM/yyyy";
        var uiFormat = "<% = DateTimeConverter.JsDateTimeFormat %>";

        $(document)
            .ready(function () {


                tinyMCE.baseURL = window.location.href.substring(0, window.location.href.lastIndexOf("/")) + "/jscripts/tiny_mce/";
                tinymce.init({
                    selector: 'textarea#screensaverContentArea',
                    plugins: "moxiemanager textcolor link code emoticons insertdatetime fullscreen imagetools directionality table media image autolink lists advlist",
                    toolbar1: "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | fontselect fontsizeselect | forecolor backcolor | fullscreen",
                    toolbar2: "cut copy paste | bullist numlist | outdent indent | undo redo | link unlink | image insertfile code | rotateleft rotateright flipv fliph",
                    moxiemanager_image_settings: {
                        view: 'thumbs',
                        extensions: 'jpg,png,gif'
                    },
                    moxiemanager_video_settings: {
                        view: 'thumbs',
                        extensions: 'mp4,webm,ogg'
                    },
                    setup: function (ed) {
                        ed.on('init',
                            function (ed) {
                                if (window.location.href.indexOf('id=') == -1) {
                                    ed.target.editorCommands.execCommand("fontName", false, "Arial");
                                }
                            });
                    },
                    relative_urls: false,
                    remove_script_host: false,
                    fontsize_formats: '8pt 10pt 11pt 12pt 14pt 16pt 18pt 24pt 36pt',
                    document_base_url: "{alerts_folder}/admin/images/upload",
                    content_css: "css/screensaver.css",
                    font_formats: 'Andale Mono=andale mono,times;Arial=arial,helvetica,sans-serif;Arial Black=arial black,avant garde;Book Antiqua=book antiqua,palatino;Calibri=calibri;Comic Sans MS=comic sans ms,sans-serif;Courier New=courier new,courier;Georgia=georgia,palatino;Helvetica=helvetica;Impact=impact,chicago;Tahoma=tahoma,arial,helvetica,sans-serif;Times New Roman=times new roman,times;Trebuchet MS=trebuchet ms,geneva;Verdana=verdana,geneva;Webdings=webdings;Wingdings=wingdings,zapf dingbats',
                    branding: false,
                    advlist_bullet_styles: 'circle,disc,square',
                    advlist_number_styles: 'lower-alpha,lower-roman,upper-alpha,upper-roman'
                });


                serverDate = new Date(getDateFromAspFormat("<% =DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")%>",
                    responseDbFormat));

                var fromDate = $.trim($("#startDateTime").val());
                var toDate = $.trim($("#endDateTime").val());
                if (fromDate != "" && isDate(fromDate, responseDbFormat)) {
                    $("#startDateTime")
                        .val(formatDate(new Date(getDateFromFormat(fromDate, responseDbFormat)), uiFormat));
                }

                if (toDate != "" && isDate(toDate, responseDbFormat)) {
                    $("#endDateTime")
                        .val(formatDate(new Date(getDateFromFormat(toDate, responseDbFormat)), uiFormat));
                }

                $(".date_time_param_input")
                    .datetimepicker({ dateFormat: settingsDateFormat, timeFormat: settingsTimeFormat });

                $("#save_and_next_button")
                    .button()
                    .click(function () {
                        saveScreensaver();
                    });

                $("#save_button").button().click(function () {
                    setDraftValue(true);
                    saveScreensaver();
                });

                $("#preview_button")
                    .button()
                    .click(function () {
                        showScreensaverPreview();
                    });

                $("#startAndEndDateAndTimeCheckbox")
                    .change(function () {
                        toggleDisable(this, "#start_and_end_date_and_time_div");
                    })
                    .change();

                $("#screensaver_tabs").tabs();



                //   $("#form1").attr('target', '_parent');

            });
    </script>
</head>
<body>
    <form id="form1" runat="server" action="AddScreensaver.aspx" method="POST">
        <div>
            <input runat="server" name="screensaverType" id="screensaverType" type="hidden" value="html" />
            <input runat="server" name="screensaverHtml" id="screensaverHtml" type="hidden" />
            <input runat="server" name="rejectedEdit" id="rejectedEdit" value="" type="hidden" />
            <input runat="server" name="shouldApprove" id="shouldApprove" value="" type="hidden" />
            <input runat="server" name="alert_id" id="alertId" type="hidden" />
            <input runat="server" name='edit' id="edit" value="" type="hidden" />
            <input runat="server" name='isDraft' id="isDraft" value="" type="hidden" />
            <table width="100%" border="0" cellspacing="0" height="100%" cellpadding="6" class="body">
                <tr>
                    <td valign="top">
                        <table width='100%' cellspacing='0' cellpadding='0' class='body'>
                            <tr>
                                <td bgcolor="#ffffff">
                                    <div style="margin: 10px">
                                        <table style="width: 98%; height: 100%; border-collapse: collapse; border: 0px; margin: 0px; padding: 0px;">
                                            <tr style="height: 45px">
                                                <td class="screensaver_name_td" style="text-align: left; padding: 0px;">
                                                    <div style="padding: 2px 0px">
                                                        <div id="campaignDiv" runat="server">
                                                            <b><%=resources.LNG_CAMPAIGN %>  :</b>
                                                            <label><b runat="server" id="campaignNameLabel"></b></label>
                                                            <input type='hidden' runat="server" id='campaignSelect' name='campaign_select' value='' />
                                                        </div>
                                                        <div runat="server" id="nonCampaignDiv">
                                                            <input type='hidden' name='campaign_select' value='-1'>
                                                        </div>

                                                    </div>
                                                    <div style="padding: 2px 0px"><% =resources.LNG_STEP %> <strong>1</strong> <% =resources.LNG_OF %> &nbsp;2 : <strong><% =resources.LNG_ADD_SCREENSAVER %> :</strong></div>
                                                    <br />
                                                    <div style="padding: 2px 0px 7px 0px">
                                                        <label for="screensaverName"><% =resources.LNG_NAME %> :</label>
                                                        <input runat="server" type="text" name="screensaver_name" id="screensaverName" value=""></input>
                                                    </div>

                                                </td>
                                            </tr>
                                            <tr style="">
                                                <td class="tabs_container" style="text-align: left; vertical-align: top; padding: 0px">
                                                    <div id="screensaver_tabs" style="">
                                                        <ul>
                                                            <li><a href="#content_tab"><%=resources.LNG_CONTENTS %> </a></li>
                                                            <li><a href="#options_tab"><%=resources.LNG_OPTIONS %></a></li>
                                                        </ul>
                                                        <div id="content_tab" style="padding: 0px; margin: 10px;">
                                                            <table style="width: 100%; height: 100%">
                                                                <tr>
                                                                    <td>
                                                                        <textarea runat="server" id="screensaverContentArea" style="width: 100%; height: 100%"></textarea>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div id="options_tab">
                                                            <div>
                                                                <input id="time_to_display_checkbox" type="hidden"><%=resources.LNG_TIME_TO_DISPLAY %>:</input>&nbsp;
							                                <input runat="server" id="timeToDisplay" type="text" style="width: 30px" value="60"></input>&nbsp;
							                                <label><% =resources.LNG_SECONDS %></label>
                                                                <p style="color: #aaaaaa">
                                                                    <%=resources.LNG_SCREENSAVER_TIME_TO_DISPLAY_TITLE %>
                                                                </p>
                                                            </div>
                                                            <div style="padding-top: 10px">
                                                                <input runat="server" id="startAndEndDateAndTimeCheckbox" name="start_and_end_date_and_time_checkbox" type="checkbox"></input><label for="startAndEndDateAndTimeCheckbox"><%=resources.LNG_START_AND_END_DATE_AND_TIME %></label>
                                                                <div id="start_and_end_date_and_time_div" style="padding-top: 5px">
                                                                    <table style="width: 300px; height: 100px; border-collapse: collapse; border: 0px; margin: 0px; padding: 0px;">
                                                                        <tr>
                                                                            <td style="text-align: right">
                                                                                <label for="startDateTime"><%=resources.LNG_START_DATE_AND_TIME %>:</label>
                                                                            </td>
                                                                            <td>
                                                                                <input runat="server" id="startDateTime" class="date_time_param_input" name="start_date_and_time" type="text" value=""></input>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="text-align: right">
                                                                                <label for="endDateTime"><%=resources.LNG_END_DATE_AND_TIME %>:</label>
                                                                            </td>
                                                                            <td>
                                                                                <input runat="server" id="endDateTime" class="date_time_param_input" name="end_date_and_time" type="text" value=""></input>
                                                                            </td>
                                                                        </tr>
                                                                    </table>

                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr style="height: 30px">
                                                <td class="wallpaper_name_td" style="text-align: right; padding: 0px; padding-top: 10px;">
                                                    <a id="preview_button"><% =resources.LNG_PREVIEW %></a>
                                                    <a id="save_button"><% =resources.LNG_SAVE %></a>
                                                    <a id="save_and_next_button"><% =resources.LNG_SAVE_AND_NEXT %></a>
                                                    <br />
                                                    <br />
                                                </td>
                                            </tr>
                                        </table>

                                    </div>
                        </table>
            </table>
        </div>
    </form>
</body>
<div id="dialog-modal" title="" style='display: none'>
    <p></p>
</div>
<form style="margin: 0px; width: 0px; height: 0px; padding: 0px" id="preview_post_form" action="ScreensaverPreview.aspx" method="post" target="preview_win">
    <input type="hidden" id="preview_html" name="html" value="" />
</form>
</html>
