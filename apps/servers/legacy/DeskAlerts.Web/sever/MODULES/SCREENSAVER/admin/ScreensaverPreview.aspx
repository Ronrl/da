﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ScreensaverPreview.aspx.cs" Inherits="DeskAlertsDotNet.Pages.ScreensaverPreview" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>DeskAlerts - Screensaver preview</title>
    
    <style>
        html, body {padding:0px; margin:0px; border:0px; overflow: auto; width:100%; height:100%}
        html, body, td, pre {color:#000; font-family:Verdana, Times New Roman, Helvetica, sans-serif; font-size:10px; margin:0px; }
        h1 {font-size: 2em}
        h2 {font-size: 1.5em}
        h3 {font-size: 1.17em}
        h4 {font-size: 1em}
        h5 {font-size: .83em}
        h6 {font-size: .75em}
    </style>

</head>
<body>
<form id="form1" runat="server">
    <div id="previewDiv" runat="server"></div>
</form>
</body>
</html>
