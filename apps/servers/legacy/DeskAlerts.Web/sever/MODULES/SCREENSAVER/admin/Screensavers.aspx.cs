﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;
using Castle.MicroKernel.Lifestyle;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Enums;
using DeskAlerts.ApplicationCore.Extensions.Enums;
using DeskAlerts.ApplicationCore.Utilities;
using DeskAlerts.Scheduler.ContentJobScheduling;
using DeskAlerts.Server.Utils.Helpers;
using DeskAlerts.Server.Utils.Interfaces.Services;
using DeskAlertsDotNet.Application;
using DeskAlertsDotNet.Models.Permission;
using DeskAlertsDotNet.Parameters;
using DeskAlertsDotNet.Utils.Consts;
using Resources;

namespace DeskAlertsDotNet.Pages
{
    public partial class Screensavers : DeskAlertsBaseListPage
    {
        private bool shouldDisplayApprove = false;
        const int Margin = 10;

        private Dictionary<string, string> screensaversPages = new Dictionary<string, string>
        {
            {"image", "EditScreensaversImage.aspx"},
            {"powerpoint", "EditScreensaverPowerPoint.aspx"},
            {"video","EditScreensaverVideo.aspx"}
        };

        protected string pageTitle;

        private readonly IContentJobScheduler _contentJobScheduler;
        private readonly ICacheInvalidationService _cacheInvalidationService;

        public Screensavers()
        {
            using (Global.Container.BeginScope())
            {
                _contentJobScheduler = Global.Container.Resolve<IContentJobScheduler>();
                _cacheInvalidationService = Global.Container.Resolve<ICacheInvalidationService>();
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            bool isDraft = !string.IsNullOrEmpty(Request["draft"]) && Request["draft"].Equals("1");
            searchButton.Text = searchButton.Text.Replace("{Search}", resources.LNG_SEARCH);
            headerTitle.Text = resources.LNG_SCREENSAVERS;
            titleCell.Text = GetSortLink(resources.LNG_TITLE, "title", Request["sortBy"]);
            creationDateCell.Text = GetSortLink(resources.LNG_CREATION_DATE, "id", Request["sortBy"]);
            scheduledCell.Text = resources.LNG_SCHEDULED;
            base.Table = contentTable;
            pageTitle = !string.IsNullOrEmpty(Request["draft"]) && Request["draft"].Equals("1") ? resources.LNG_DRAFT : resources.LNG_CURRENT;

            shouldDisplayApprove = Config.Data.HasModule(Modules.APPROVE) && Settings.Content["ConfAllowApprove"].Equals("1");
            addButton.Visible = string.IsNullOrEmpty(Request["draft"]) && string.IsNullOrEmpty(Request["sent"]);
            if (shouldDisplayApprove)
                approveCell.Text = resources.LNG_APPROVE_STATUS;
            else
                approveCell.Visible = false;

            senderCell.Text = resources.LNG_SENDER;
            actionsCell.Text = resources.LNG_ACTIONS;

            headerSelectAll.Visible = curUser.HasPrivilege || curUser.Policy[Policies.SCREENSAVERS].CanDelete;
            selectAll.Visible = curUser.HasPrivilege || curUser.Policy[Policies.SCREENSAVERS].CanDelete;
            bottomDelete.Visible = curUser.HasPrivilege || curUser.Policy[Policies.SCREENSAVERS].CanDelete;
            upDelete.Visible = curUser.HasPrivilege || curUser.Policy[Policies.SCREENSAVERS].CanDelete;

            if (!curUser.HasPrivilege && !curUser.Policy[Policies.SCREENSAVERS].CanCreate)
                addButton.Visible = false;
            if (!IsPostBack)
            {
                searchTermBox.Value = Request["searchTermBox"];
            }
            string getUrlSearchTermBox = Request.QueryString["searchTermBox"] ?? string.Empty;

            bool isSearchTermBoxValueChanged = searchTermBox.Value != getUrlSearchTermBox;
            if (isSearchTermBoxValueChanged)
            {
                Offset = 0;
            }
            int cnt = this.Content.Count < (Offset + Limit) ? this.Content.Count : Offset + Limit;

            for (var i = Offset; i < cnt; i++)
            {
                var row = this.Content.GetRow(i);

                var isScheduled = row.GetInt("schedule") == 1;

                var tableRow = new TableRow();

                if (curUser.HasPrivilege || curUser.Policy[Policies.SCREENSAVERS].CanDelete)
                {
                    CheckBox chb = GetCheckBoxForDelete(row);
                    TableCell checkBoxCell = new TableCell { HorizontalAlign = HorizontalAlign.Center };
                    checkBoxCell.Controls.Add(chb);
                    tableRow.Cells.Add(checkBoxCell);
                }

                TableCell titleContentCell = new TableCell();
                titleContentCell.Text = row.GetString("title");//string.Format("<a href='RssList.aspx?sent=1&id={0}'>{1}</a>", row.GetString("id"), row.GetString("title"));
                tableRow.Cells.Add(titleContentCell);

                var createDateContentCell = new TableCell
                {
                    Text = DateTimeConverter.ToUiDateTime(row.GetDateTime("create_date")),
                    HorizontalAlign = HorizontalAlign.Center
                };
                tableRow.Cells.Add(createDateContentCell);

                var scheduleCell = new TableCell
                {
                    Text = isScheduled
                    ? resources.LNG_YES
                    : resources.LNG_NO,

                    HorizontalAlign = HorizontalAlign.Center
                };
                tableRow.Cells.Add(scheduleCell);

                if (shouldDisplayApprove)
                {                       
                    string approveLocKey;
                    if (row.IsNull("approve_status"))
                    {
                        approveLocKey = "LNG_APPROVED";
                    }
                    else
                    {
                        var approveStatus = (ApproveStatus)row.GetInt("approve_status");
                        approveLocKey = approveStatus.GetStringValue();
                    }

                    var approveContentCell = new TableCell();
                    approveContentCell.HorizontalAlign = HorizontalAlign.Center;
                    approveContentCell.Text = resources.ResourceManager.GetString(approveLocKey);
                    tableRow.Cells.Add(approveContentCell);
                }

                var senderContentCell = new TableCell
                {
                    HorizontalAlign = HorizontalAlign.Center,
                    Text = string.IsNullOrEmpty(row.GetString("name")) ? resources.SENDER_WAS_DELETED : row.GetString("name")
                };

                tableRow.Cells.Add(senderContentCell);

                string ssType = "";

                string regexPattern = "<!--.*?screensaver_type=\"(.*)\".*?file=\"(.*)\".*?-->";

                MatchCollection mc = Regex.Matches(row.GetString("alert_text"), regexPattern);

                foreach (Match m in mc)
                    ssType = m.Groups[1].Value;

                string dupPage = !screensaversPages.ContainsKey(ssType) ? "EditScreensaversHtml.aspx" : screensaversPages[ssType];
                string alertId = row.GetString("id");

                LinkButton duplicateButton = GetActionButton(alertId, "images/action_icons/duplicate.png", "LNG_RESEND_SCREENSAVER", "location.href='" + dupPage + "?id=" + alertId + "&type=" + ssType + "'", 10);

                int scheduleType = row.GetInt("schedule_type");
                LinkButton changeScheduleType = GetActionButton(alertId, scheduleType == 1 ? "images/action_icons/stop.png" : "images/action_icons/start.png", scheduleType == 1 ? resources.LNG_STOP_SCHEDULED_ALERT : resources.LNG_START_SCHEDULED_ALERT,
                    delegate
                    {
                        Response.Redirect("ChangeScheduleType.aspx?id=" + alertId + "&type=" + Math.Abs(scheduleType - 1) + "&return_page=Screensavers.aspx", true);
                    }

                );

                LinkButton previewButton = GetActionButton(row.GetString("id"), "images/action_icons/preview.png", "LNG_PREVIEW", "showScreenSaverPreview(" + row.GetString("id") + ")");


                LinkButton sendButton = GetActionButton(alertId, "images/action_icons/send.png",
                    "LNG_SEND_ALERT",
                    delegate { Response.Redirect($"RecipientsSelection.aspx?desktop=1&id={alertId}&return_page=Screensavers.aspx", true); }, Margin);

                LinkButton editButton = GetActionButton(alertId, "images/action_icons/edit.png", "LNG_EDIT_ALERT",
                    delegate { Response.Redirect($"{dupPage}?id={alertId}&edit=1", true); }, Margin
                );

                TableCell actionsContentCell = new TableCell();

                actionsContentCell.HorizontalAlign = HorizontalAlign.Center;

                if (!isDraft)
                {
                    if (curUser.HasPrivilege || curUser.Policy[Policies.SCREENSAVERS].CanCreate)
                    {
                        actionsContentCell.Controls.Add(duplicateButton);
                    }

                    if (curUser.HasPrivilege || curUser.Policy[Policies.SCREENSAVERS].CanStop)
                        actionsContentCell.Controls.Add(changeScheduleType);
                }
                else
                {
                    if (curUser.HasPrivilege || curUser.Policy[Policies.SCREENSAVERS].CanSend)
                    {
                        actionsContentCell.Controls.Add(sendButton);
                    }

                    if (curUser.HasPrivilege || curUser.Policy[Policies.SCREENSAVERS].CanEdit)
                    {
                        actionsContentCell.Controls.Add(editButton);
                    }
                }

                actionsContentCell.Controls.Add(previewButton);

                tableRow.Cells.Add(actionsContentCell);

                contentTable.Rows.Add(tableRow);
            }
        }

        #region DataProperties

        protected override string DataTable => "alerts";

        protected override string[] Collumns => new string[] { };

        protected override string DefaultOrderCollumn => "id";

        protected override string CustomQuery
        {
            get
            {
                string viewListStr = "";

                int classId = 2;
                string alertType = !string.IsNullOrEmpty(Request["draft"]) && Request["draft"].Equals("1") ? "D" : "S";

                if (!curUser.HasPrivilege && !curUser.Policy.CanViewAll)
                    viewListStr = string.Join(",", curUser.Policy.SendersViewList.ToArray());

                string query = "";
                if (!curUser.HasPrivilege && !curUser.Policy.CanViewAll)
                {
                    query = "SELECT a.id, a.lifetime, a.approve_status, a.schedule, schedule_type, recurrence, alert_text, title, a.type, sent_date, a.create_date, a.from_date, a.to_date, class, urgent, ticker, fullscreen, s.id as is_rsvp, u.name FROM alerts a LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id LEFT JOIN users u on u.id = a.sender_id WHERE video = 0 AND param_temp_id IS NULL AND class = " + classId + " AND a.type = '" + alertType + "' AND a.sender_id IN (" + viewListStr + ")";

                    if (!string.IsNullOrEmpty(Request["parent_id"]))
                        query += " AND parent_id = " + Request["parent_id"];
                }
                else
                {
                    query = "SELECT a.id, a.lifetime, a.approve_status, a.schedule, schedule_type, recurrence, alert_text, title, a.type, sent_date, a.create_date, a.from_date, a.to_date, class, urgent, ticker, fullscreen, s.id as is_rsvp, u.name FROM alerts a LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id LEFT JOIN users u on u.id = a.sender_id WHERE video = 0 AND param_temp_id IS NULL AND class = " + classId + " AND a.type = '" + alertType + "'";

                    if (!string.IsNullOrEmpty(Request["parent_id"]))
                        query += " AND parent_id = " + Request["parent_id"];
                }

                if (IsPostBack && SearchTerm.Length > 0)
                    query += " AND title LIKE '%" + SearchTerm + "%'";

                if (!String.IsNullOrEmpty(Request["sortBy"]))
                    query += " ORDER BY " + Request["sortBy"];
                else
                {
                    query += " ORDER BY " + DefaultOrderCollumn + " DESC ";
                }

                return query;
            }
        }

        protected override string CustomCountQuery
        {
            get
            {
                string viewListStr = "";

                if (!curUser.HasPrivilege && !curUser.Policy.CanViewAll)
                    viewListStr = string.Join(",", curUser.Policy.SendersViewList.ToArray());

                int classId = 2;
                string alertType = !string.IsNullOrEmpty(Request["draft"]) && Request["draft"].Equals("1") ? "D" : "S";

                string query = "";
                if (!curUser.HasPrivilege && !curUser.Policy.CanViewAll)
                {
                    query = "SELECT count(1) FROM alerts a LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id LEFT JOIN users u on u.id = a.sender_id WHERE video = 0 AND param_temp_id IS NULL AND class = " + classId + " AND a.type = '" + alertType + "' AND a.sender_id IN (" + viewListStr + ")";

                    if (!string.IsNullOrEmpty(Request["parent_id"]))
                        query += " AND parent_id = " + Request["parent_id"];
                }
                else
                {
                    query = "SELECT  count(1) FROM alerts a LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id LEFT JOIN users u on u.id = a.sender_id WHERE video = 0 AND param_temp_id IS NULL AND class = " + classId + " AND a.type = '" + alertType + "'";

                    if (!string.IsNullOrEmpty(Request["parent_id"]))
                        query += " AND parent_id = " + Request["parent_id"];
                }

                if (IsPostBack && SearchTerm.Length > 0)
                    query += " AND title LIKE '%" + SearchTerm + "%'";

                return query;
            }
        }

        /*
         *
         * If you want to add condition for data selection override property ConditionSqlString
         * Also you can implement your custom query by overriding properties CustomQuery and CustomCountQuery
         *
         */

        #endregion DataProperties

        #region Interface properties

        protected override IButtonControl[] DeleteButtons
        {
            get
            {
                return new IButtonControl[] { upDelete, bottomDelete };
            }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl NoRowsDiv
        {
            get { return NoRows; }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl TableDiv
        {
            get { return mainTableDiv; }
        }

        protected override string ContentName
        {
            get
            {
                //return content name here
                return resources.LNG_SCREENSAVERS;
            }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl BottomPaging
        {
            get
            {
                return bottomRanging;
            }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl TopPaging
        {
            get
            {
                return topPaging;
            }
        }

        protected override void OnDeleteRows(string[] ids)
        {
            foreach (var id in ids)
            {
                var screensaverId = Convert.ToInt32(id);

                if (AppConfiguration.IsNewContentDeliveringScheme)
                {
                    _cacheInvalidationService.CacheInvalidation(AlertType.ScreenSaver, Convert.ToInt64(id));
                }
                else
                {
                    Global.CacheManager.Delete(screensaverId, DateTime.Now.ToString(DateTimeFormat.JavaScript, CultureInfo.InvariantCulture));
                }
                
                var contentJobDto = new ContentJobDto(screensaverId, AlertType.Wallpaper);
                _contentJobScheduler.DeleteJob(contentJobDto);

                ReccurenceManager.DeleteReccurence(screensaverId);
            }
        }

        #endregion Interface properties
    }
}