﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using DeskAlertsDotNet.Parameters;
using DeskAlertsDotNet.Models;
using System.Globalization;
using DeskAlertsDotNet.Utils;

namespace DeskAlertsDotNet.Pages
{
    public partial class WidgetLastContent : DeskAlertsBaseListPage
    {

        string fromDate = "1/1/1900 00:00:00";
      public  string toDate = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
        protected override void OnLoad(EventArgs e)
        {


                if (!IsPostBack)
                {
                    contentSelector.Items.Add(new ListItem(Localization.Current.Content["LNG_ALL"], "0"));

                    if (curUser.HasPrivilege || curUser.Policy[Policies.ALERTS].CanView)
                        contentSelector.Items.Add(new ListItem(Localization.Current.Content["LNG_ALERTS"], "1"));

                    if (Config.Data.HasModule(Modules.SURVEYS) &&
                        (curUser.HasPrivilege || curUser.Policy[Policies.SURVEYS].CanView))
                        contentSelector.Items.Add(new ListItem(Localization.Current.Content["LNG_SURVEYS"], "64,128,256"));

                    if (Config.Data.HasModule(Modules.WALLPAPER) &&
                        (curUser.HasPrivilege || curUser.Policy[Policies.SURVEYS].CanView))
                        contentSelector.Items.Add(new ListItem(Localization.Current.Content["LNG_WALLPAPERS"], "8"));

                    if (Config.Data.HasModule(Modules.SCREENSAVER) &&
                        (curUser.HasPrivilege || curUser.Policy[Policies.SCREENSAVERS].CanView))
                        contentSelector.Items.Add(new ListItem(Localization.Current.Content["LNG_SCREENSAVERS"], "2"));

                    if (Config.Data.HasModule(Modules.RSS) &&
                        (curUser.HasPrivilege || curUser.Policy[Policies.RSS].CanView))
                        contentSelector.Items.Add(new ListItem(Localization.Current.Content["LNG_RSS"], "5"));

                    if (!string.IsNullOrEmpty(Request["classes"]))
                        contentSelector.SelectedValue = Request["classes"];
                }

                //   contentSelector.Style.Add(HtmlTextWriterStyle.TextAlign, "right");
                //  contentSelector.Style.Add(HtmlTextWriterStyle.Padding, "10px");
                base.OnLoad(e);


                searchButton.Text = searchButton.Text.Replace("{Search}", Localization.Current.Content["LNG_SEARCH"]);
                typeCell.Text = Localization.Current.Content["LNG_TYPE"];
                titleCell.Text = Localization.Current.Content["LNG_TITLE"];
                createDateCell.Text = Localization.Current.Content["LNG_CREATION_DATE"];
                actionsCell.Text = Localization.Current.Content["LNG_ACTIONS"];


                int cnt = daContent.Count < (Offset + Limit) ? daContent.Count : Offset + Limit;


                for (int i = Offset; i < cnt; i++)
                {
                    int id = daContent.GetInt(i, "id");

                    TableRow contentRow = new TableRow();
                    string imagePath = "";
                    Image imageType = new Image();
                    bool isUrgent = daContent.GetInt(i, "urgent") == 1;
                    DateTime toDate = daContent.GetDateTime(i, "to_date");
                    DateTime fromDate = daContent.GetDateTime(i, "from_date");
                    //  bool hasLifetime = daContent.GetInt(i, "lifetime") == 1;

                    bool isScheduled = (toDate.Year != 1990 && (toDate - fromDate).TotalSeconds % 60 == 0) ||
                                       toDate.Year == 1990;


                   // Image imageType = new Image();

                //    bool isUrgent = daContent.GetInt(i, "urgent") == 1;
//                    string imagePath = "";

                    if (daContent.GetInt(i, "ticker") == 1)
                    {
                        imagePath = isUrgent ? "images/alert_types/urgent_ticker.png" : "images/alert_types/ticker.png";
                        imageType.Attributes["title"] = isUrgent ? Localization.Current.Content["LNG_TICKER_URGENT"] : Localization.Current.Content["LNG_TICKER"];
                    }
                    else if (daContent.GetInt(i, "fullscreen") == 1 && daContent.GetInt(i, "class") != 2 && daContent.GetInt(i, "class") != 8)
                    {
                        imagePath = isUrgent ? "images/alert_types/urgent_fullscreen.png" : "images/alert_types/fullscreen.png";
                        imageType.Attributes["title"] = isUrgent ? Localization.Current.Content["LNG_URGENT_FULLSCREEN"] : Localization.Current.Content["LNG_FULLSCREEN"];
                    }
                    else if (daContent.GetString(i, "type") == "L")
                    {
                        imagePath = "images/alert_types/linkedin.png";
                        imageType.Attributes["title"] = Localization.Current.Content["LNG_ADD_LINKEDIN"];
                    }
                    else if (!daContent.IsNull(i, "is_rsvp"))
                    {
                        imagePath = isUrgent ? "images/alert_types/urgent_rsvp.png" : "images/alert_types/rsvp.png";
                        imageType.Attributes["title"] = isUrgent ? Localization.Current.Content["LNG_URGENT_RSVP"] : Localization.Current.Content["LNG_RSVP"];
                    }
                    else
                    {

                        imagePath = "images/alert_types/new/" + daContent.GetString(i, "class") + ".png";
                        string fullPath = Config.Data.GetString("alertsFolder") + "/admin/" + imagePath;
                        // Response.Write(fullPath);

                        if (!System.IO.File.Exists(fullPath))
                        {
                            imagePath = "images/alert_types/alert.png";

                        }
                        imageType.Attributes["title"] = DeskAlertsUtils.GetTooltipForClass(daContent.GetInt(i, "class"));




                    }

                    imageType.ImageUrl = imagePath;
                    TableCell typeImgCell = new TableCell();
                    typeImgCell.Controls.Add(imageType);
                    typeImgCell.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
                    contentRow.Cells.Add(typeImgCell);


                    TableCell titleContentCell = new TableCell();

                    string title = daContent.GetString(i, "title");

                    if (daContent.GetInt(i, "class") == 1)
                    {
                        title = "<a href=\"javascript: showAlertPreview('" + id + "')\">" + title + "</a>";
                    }

                    titleContentCell.Text = title;

                    contentRow.Cells.Add(titleContentCell);

                    TableCell creationDateCell = new TableCell();

                    DateTime creationDate = daContent.GetDateTime(i, "sent_date");
                    creationDateCell.Text = creationDate.ToString("dd/MM/yyyy hh:mm tt", CultureInfo.InvariantCulture);

                    contentRow.Cells.Add(creationDateCell);

                    TableCell actionsContentCell = new TableCell();

                    //  actionsContentCell.Attributes.Add("text-align", "center");
                    actionsContentCell.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
                    string type = daContent.GetString(i, "type");
                    int alertClass = daContent.GetInt(i, "class");

                    string actionsHtml = "";
                    switch (alertClass)
                    {
                        case 1:
                        case 2048:
                        {
                            if (curUser.HasPrivilege || curUser.Policy[Policies.ALERTS].CanEverything ||
                                                     curUser.Policy[Policies.ALERTS].CanCreate)
                            {
                                actionsHtml += "<a target=\"_parent\" href=\"CreateAlert.aspx?id=" + id +
                                               "\"><img src=\"images/action_icons/duplicate.png\" alt=\"" +
                                               Localization.Current.Content["LNG_RESEND_ALERT"] + "\" title=\"" +
                                               Localization.Current.Content["LNG_RESEND_ALERT"] +
                                               "\" width=\"16\" height=\"16\" border=\"0\" hspace=5></a>";
                            }


                            actionsHtml += "<a href='javascript: showAlertPreview(\"" + id +
                                           "\")'><img src='images/action_icons/preview.png' alt='" +
                                           Localization.Current.Content["LNG_PREVIEW"] + "' title='" +
                                           Localization.Current.Content["LNG_PREVIEW"] +
                                           "' width='16' height='16' border='0' hspace='5'/></a>";

                            actionsHtml += "<a href=\"#\" onclick=\"openDirDialog('" + id +
                                           "')\"><img src=\"images/action_icons/link.png\" alt=\"" +
                                           Localization.Current.Content["LNG_DIRECT_LINK"] +
                                           "\" title=\""+ Localization.Current.Content["LNG_DIRECT_LINK"]  +"\" width=\"16\" height=\"16\" border=\"0\" hspace=\"6\"/></a>";

                            actionsHtml +=
                                "<a href='#'  onclick='openDialogUI(\"form\",\"frame\",\"statistics_da_alerts_view_details.asp?id=" +
                                id + "\",600,550,\"" + Localization.Current.Content["LNG_ALERT_DETAILS"] +
                                "\");'><img src=\"images/action_icons/graph.gif\" alt=\"" +
                                Localization.Current.Content["LNG_STATISTICS"] + "\" title=\"" +
                                Localization.Current.Content["LNG_STATISTICS"] +
                                "\" width=\"16\" height=\"16\" border=\"0\" hspace=\"5\"/></a>";

                            if (isScheduled)
                            {
                                actionsHtml +=
                                    "<a href=\"#\" onclick=\"openDialogUI('form','frame','reschedule.asp?id=" + id +
                                    "',600,550,'" + Localization.Current.Content["LNG_RESCHEDULE_ALERT"] +
                                    "');\"><img src=\"images/action_icons/schedule.png\" alt=\"" +
                                    Localization.Current.Content["LNG_RESCHEDULE_ALERT"] + "\" title=\"" +
                                    Localization.Current.Content["LNG_RESCHEDULE_ALERT"] +
                                    "\" border=\"0\" hspace=5></a>";
                            }

                            actionsHtml += "<a href='#'  onclick='terminateAlert(" + id
                                           + ");'><img src=\"images/action_icons/terminate.png\" alt=\""
                                           + Localization.Current.Content["LNG_TERMINATE"] + "\" title=\""
                                           + Localization.Current.Content["LNG_TERMINATE"]
                                           + "\" width=\"16\" height=\"16\" border=\"0\" hspace=\"5\"/></a>";
                            break;
                        }
                        case 2:
                        {
                            if (curUser.HasPrivilege || curUser.Policy[Policies.SCREENSAVERS].CanEverything ||
                                curUser.Policy[Policies.SCREENSAVERS].CanCreate)
                            {
                                actionsHtml += "<a target=\"_parent\" href=\"edit_screensavers.asp?id=" + id +
                                               "\"><img src=\"images/action_icons/duplicate.png\" alt=\"" +
                                               Localization.Current.Content["LNG_RESEND_SCREENSAVER"] + "\" title=\"" +
                                               Localization.Current.Content["LNG_RESEND_SCREENSAVER"] +
                                               "\" width=\"16\" height=\"16\" border=\"0\" hspace=5></a>";
                            }

                            actionsHtml += "<a href='#' onclick=\"javascript: showScreenSaverPreview(" + id +
                                           ")\"><img src='images/action_icons/preview.png' alt=\"" +
                                           Localization.Current.Content["LNG_PREVIEW"] + "\" title=\"" +
                                           Localization.Current.Content["LNG_PREVIEW"] +
                                           "\" width=\"16\" height=\"16\" border=\"0\" hspace=\"5\"/></a>";

                            break;
                        }
                        case 8:
                        {


                            if (curUser.IsAdmin || curUser.Policy[Policies.WALLPAPERS].CanEverything ||
                                curUser.Policy[Policies.WALLPAPERS].CanCreate)
                            {
                                actionsHtml += "<a target=\"_parent\" href=\"wallpaper_edit.asp?id=" + id +
                                               "\"><img src=\"images/action_icons/duplicate.png\" alt=\"" +
                                               Localization.Current.Content["LNG_RESEND_WALLPAPER"] + "\" title=\"" +
                                               Localization.Current.Content["LNG_RESEND_WALLPAPER"] +
                                               "\" width=\"16\" height=\"16\" border=\"0\" hspace=5></a>";
                            }

                            actionsHtml += "<a href='#' onclick=\"javascript: showWallpaperPreview(" + id +
                                           ")\"><img src='images/action_icons/preview.png' alt=\"" +
                                           Localization.Current.Content["LNG_PREVIEW"] + "\" title=\"" +
                                           Localization.Current.Content["LNG_PREVIEW"] +
                                           "\" width=\"16\" height=\"16\" border=\"0\" hspace=\"5\"/></a>";
                            break;
                        }
                        case 64:
                        case 128:
                        case 256:
                        {
                            object surveyIdObj =
                                dbMgr.GetScalarByQuery("SELECT id FROM surveys_main WHERE sender_id = " + id);

                            string surveyId = "";
                            if (surveyIdObj != null)
                                surveyId = surveyIdObj.ToString();


                            actionsHtml += "<a target=\"_parent\"href=\"addSurvey.aspx?id=" + surveyId +
                                           "\"><img src=\"images/action_icons/duplicate.png\" alt=\"" +
                                           Localization.Current.Content["LNG_RESEND_SURVEY"] + "\" title=\"" +
                                           Localization.Current.Content["LNG_RESEND_SURVEY"] +
                                           "\" width=\"16\" height=\"16\" border=\"0\" hspace=5></a>";

                            actionsHtml += "<a href='javascript: showSurveyPreview(\"" + surveyId +
                                           "\")'><img src='images/action_icons/preview.png' alt='" +
                                           Localization.Current.Content["LNG_PREVIEW"] + "' title='" +
                                           Localization.Current.Content["LNG_PREVIEW"] +
                                           "' width='16' height='16' border='0' hspace='5'/></a>";

                            if (curUser.IsAdmin || curUser.Policy[Policies.SURVEYS].CanEverything ||
                                curUser.Policy[Policies.SURVEYS].CanView)
                            {
                                actionsHtml +=
                                    "<a href='#'  onclick='openDialogUI(\"form\",\"frame\",\"survey_view.asp?id=" +
                                    surveyId + "\",555,450,\"\");'><img src=\"images/action_icons/graph.png\" alt=\"" +
                                    Localization.Current.Content["LNG_STATISTICS"] + "\" title=\"" +
                                    Localization.Current.Content["LNG_VIEW_SURVEY_RESULTS"] +
                                    "\" width=\"16\" height=\"16\" border=\"0\" hspace=\"5\"/></a>";

                            }

                            actionsHtml += "<a href='#'  onclick='terminateAlert(" + id
                                           + ");'><img src=\"images/action_icons/terminate.png\" alt=\""
                                           + Localization.Current.Content["LNG_TERMINATE"] + "\" title=\""
                                           + Localization.Current.Content["LNG_TERMINATE"]
                                           + "\" width=\"16\" height=\"16\" border=\"0\" hspace=\"5\"/></a>";
                            break;
                        }
                        case 4:
                        {
                            if (curUser.IsAdmin || curUser.Policy[Policies.RSS].CanEverything ||
                                curUser.Policy[Policies.RSS].CanCreate)
                            {
                                actionsHtml += "<a target=\"_parent\" href=\"rss_edit.asp?id=" + id +
                                               "\"><img src=\"images/action_icons/duplicate.png\" alt=\"" +
                                               Localization.Current.Content["LNG_RESEND_RSS"] + "\" title=\"" +
                                               Localization.Current.Content["LNG_RESEND_RSS"] +
                                               "\" width=\"16\" height=\"16\" border=\"0\" hspace=5></a>";
                            }
                            break;
                        }
                        case 5:
                        {
                            actionsHtml += "<a href='javascript: showAlertPreview(\"" + id +
                                           "\")'><img src='images/action_icons/preview.png' alt='" +
                                           Localization.Current.Content["LNG_PREVIEW"] + "' title='" +
                                           Localization.Current.Content["LNG_PREVIEW"] +
                                           "' width='16' height='16' border='0' hspace='5'/></a>";

                            break;
                        }


                    }

                    actionsContentCell.Text = actionsHtml;
                    //  actionsCell.Align = "center";
                    contentRow.Cells.Add(actionsContentCell);

                    contentTable.Rows.Add(contentRow);

                }


        }


        protected override string PageParams
        {
            get
            {
                if(IsPostBack)
                    return "classes=" + contentSelector.SelectedValue + "&";

                if(!string.IsNullOrEmpty(Request["classes"]))
                    return "classes=" + Request["classes"] + "&";

                return "";
            }
        }

        #region DataProperties
        protected override string DataTable
        {
            get
            {
                return "";
                //return your table name from this property
                //throw new NotImplementedException();
            }
        }

        protected override string[] Collumns
        {
            get
            {

                return null;
                //return array of collumn`s names of table from data table
                //throw new NotImplementedException();

            }
        }

        protected override string DefaultOrderCollumn
        {
            //return collumn for default order
            get
            {
                return "create_date";
               // throw new NotImplementedException();

            }
        }

        /*
         * 
         * If you want to add condition for data selection override property ConditionSqlString 
         * Also you can implement your custom query by overriding properties CustomQuery and CustomCountQuery
         *  
         */

        protected override string CustomCountQuery
        {
            get
            {



                string sqlQuery = "";

                string classesList = contentSelector.SelectedValue;

                if (!string.IsNullOrEmpty(Request["classes"]))
                    classesList = Request["classes"];

                if (!curUser.HasPrivilege && !curUser.Policy.CanViewAll)
                {
                    string viewListStr = string.Join(",", curUser.Policy.SendersViewList.ToArray());
                    sqlQuery = "SELECT COUNT(1) FROM alerts a LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id WHERE (a.type='S' OR a.type='L') AND a.create_date >= '" + fromDate + "' AND a.create_date <= GETDATE() AND param_temp_id IS NULL AND a.sender_id IN (" + viewListStr + ") AND a.id NOT IN (SELECT alert_id FROM instant_messages) ";

                }
                else
                    sqlQuery = "SELECT COUNT(1) FROM alerts a LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id WHERE (a.type='S' OR a.type='L') AND a.create_date >= '" + fromDate + "' AND a.create_date <= GETDATE() AND param_temp_id IS NULL AND a.id NOT IN (SELECT alert_id FROM instant_messages) ";
               
                if (!contentSelector.SelectedValue.Equals("0"))
                    sqlQuery += "AND [class] IN (" + classesList + ")";

                if (searchTermBox.Value.Length > 0)
                    sqlQuery += " AND a.title LIKE '%" + searchTermBox.Value + "%'";
                return sqlQuery;
            }
        }

        protected override string CustomQuery
        {
            get
            {

                string classesList = contentSelector.SelectedValue;

                if (!string.IsNullOrEmpty(Request["classes"]) && !IsPostBack)
                    classesList = Request["classes"];
               
                string sqlQuery = "";

                if (!curUser.HasPrivilege && !curUser.Policy.CanViewAll)
                {
                    string viewListStr = string.Join(",", curUser.Policy.SendersViewList.ToArray());
                    sqlQuery = "SELECT a.id, a.schedule, schedule_type, a.type2, recurrence, alert_text, title, a.type, sent_date, a.create_date as create_date, a.from_date, a.to_date, class, urgent, ticker, fullscreen, s.id as is_rsvp FROM alerts a LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id WHERE (a.type='S' OR a.type='L') AND a.create_date >= '" + fromDate + "' AND a.create_date <= GETDATE() AND param_temp_id IS NULL AND a.sender_id IN (" + viewListStr + ") AND a.id NOT IN (SELECT alert_id FROM instant_messages) ";

                }
                else
                    sqlQuery = "SELECT a.id, a.schedule, schedule_type, a.type2, recurrence, alert_text, title, a.type, sent_date, a.create_date as create_date, a.from_date, a.to_date, class, urgent, ticker, fullscreen, s.id as is_rsvp FROM alerts a LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id WHERE (a.type='S' OR a.type='L') AND a.create_date >= '" + fromDate + "' AND a.create_date <= GETDATE() AND param_temp_id IS NULL AND a.id NOT IN (SELECT alert_id FROM instant_messages) ";

                if (!classesList.Equals("0"))
                    sqlQuery += "AND [class] IN (" + classesList + ")";


                if (searchTermBox.Value.Length > 0)
                    sqlQuery += " AND title LIKE '%" + searchTermBox.Value + "%'";

                sqlQuery += " ORDER BY create_date DESC";

                return sqlQuery;
              
            }
        }
        #endregion

        #region Interface properties

        protected override IButtonControl[] DeleteButtons
        {
            get
            {
                return new IButtonControl[] {};
            }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl NoRowsDiv
        {
            get { return NoRows; }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl TableDiv
        {
            get { return mainTableDiv; }
        }

        protected override string ContentName
        {
            get
            {
                //return content name here
                return "";
            }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl BottomPaging
        {
            get
            {
                return bottomRanging;
            }
        }

        #endregion
    }
}