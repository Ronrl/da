<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%

uid = Session("uid")

if Request("id") <> "" then
	response.redirect("statistics_da_alerts_view_details.asp?id="&Request("id")&"&widget=1")
else
	if(Request("offset") <> "") then 
	offset = clng(Request("offset"))
   else 
	offset = 0
   end if	

   if(Request("sortby") <> "") then 
	sortby = Request("sortby")
   else 
	sortby = "sent_date DESC"
   end if	

'counting pages to display
   if(Request("limit") <> "") then 
	limit = clng(Request("limit"))
   else 
	limit=10
   end if
   
   search_title = Request("search_title")
   
%>
<!doctype html>
<html>
<head>
	<title>DeskAlerts</title>
	<link href="css/style9.css" rel="stylesheet" type="text/css">
	<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css" />

	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>

	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>

	

	<script language="javascript" type="text/javascript" src="functions.js"></script>
	
	<script language="javascript" type="text/javascript" src="jscripts/date.js"></script>

	<script language="javascript" type="text/javascript">
		var settingsDateFormat = "<%=GetDateFormat()%>";
		var settingsTimeFormat = "<%=GetTimeFormat()%>";

		var responseDbFormat = "dd/MM/yyyy HH:mm:ss";
		var uiFormat = getUiDateTimeFormat(settingsDateFormat, settingsTimeFormat);

		function saveParameters(parameters) {
			var id = window.frameElement.getAttribute("id");
			id = id.substring(1);
			var jqxhr = $.get("manipulate_widgets.asp", { id: id, action: "update", parameters: parameters })
					.done(function() {
						var src = window.frameElement.getAttribute("src");
						src = src + "?" + parameters;
						id = "i" + id;
						window.parent.changeIframeSrc(id, src);
					});
		}

		
		$(document).ready(function() {
			$("#search").button();
		});
	</script>

	<style type="text/css">
		.jqplot-highlighter-tooltip
		{
			font-size: 12px;
		}
	</style>
</head>
<body>
	<center>
		<form action="#">
		<table>
			<tr>
				<td>
					<%=LNG_SEARCH_ALERT_BY_TITLE %>:
				</td>
				<td>
					<input type="text" name="search_title" style="width: 200px" value="<%=Request("search_title") %>" />
				</td>
				<td>
					<button id="search" type="submit">
						<%=LNG_SEARCH %></button>
				</td>
			</tr>
		</table>
		<% 
		if search_title <> "" then
			'--- rights ---
			set rights = getRights(uid, true, false, true, "alerts_val")
			gid = rights("gid")
			gviewall = rights("gviewall")
			gviewlist = rights("gviewlist")
			alerts_arr = rights("alerts_val")
			'--------------

			if gviewall <> 1 then
				SQL = "SELECT COUNT(1) as mycnt, 1 as mycnt1 FROM alerts WHERE (type='S' OR type='L') AND param_temp_id IS NULL AND (class = 1 OR class = 16) AND sender_id IN ("& Join(gviewlist,",") &") AND id NOT IN (SELECT alert_id FROM instant_messages) AND title like'%"&search_title&"%'"
				SQL = SQL & " UNION ALL "
				SQL = SQL & "SELECT COUNT(1) as mycnt, 2 as mycnt1 FROM archive_alerts WHERE (type='S' OR type='L') AND param_temp_id IS NULL AND (class = 1 OR class = 16) AND sender_id IN ("& Join(gviewlist,",") &") AND id NOT IN (SELECT alert_id FROM instant_messages) AND title like'%"&search_title&"%'"
				
				Set RS = Conn.Execute(SQL)

			else
				SQL = "SELECT COUNT(1) as mycnt, 1 as mycnt1 FROM alerts WHERE (type='S' OR type='L') AND param_temp_id IS NULL AND (class = 1 OR class = 16)  AND id NOT IN (SELECT alert_id FROM instant_messages) AND title like'%"&search_title&"%'"
				SQL = SQL & " UNION ALL "
				SQL = SQL & "SELECT COUNT(1) as mycnt, 2 as mycnt1 FROM archive_alerts WHERE (type='S' OR type='L') AND param_temp_id IS NULL AND (class = 1 OR class = 16) AND id NOT IN (SELECT alert_id FROM instant_messages) AND title like'%"&search_title&"%'"
				Set RS = Conn.Execute(SQL)

			end if

			cnt=0

			do while Not RS.EOF
				if(Not IsNull(RS("mycnt"))) then cnt=cnt+clng(RS("mycnt"))
				RS.MoveNext
			loop

			RS.Close
			
			if(cnt>0) then
			page="widget_alert_details.asp?search_title="&search_title&"&"
			name=LNG_ALERTS 
			Response.Write make_pages(offset, cnt, limit, page, name, sortby) 
		%>
		<table width="100%" height="100%" cellspacing="0" cellpadding="3" class="data_table">
			<tr class="data_table_title">
				<td class="table_title">
					<%=sorting(LNG_ALERT_TITLE,"title", sortby, offset, limit, page) %>
				</td>
				<td class="table_title">
					<%=sorting(LNG_CREATION_DATE,"sent_date", sortby, offset, limit, page) %>
				</td>
			</tr>
			<%

'show main table

	if gviewall <> 1 then
		SQL = "SELECT * FROM (SELECT a.id, title, sent_date FROM alerts a WHERE (a.type='S' OR a.type='L') AND param_temp_id IS NULL AND (class = 1 OR class = 16) AND a.sender_id IN ("& Join(gviewlist,",") &") AND a.id NOT IN (SELECT alert_id FROM instant_messages)  AND title like'%"&search_title&"%'"
		SQL = SQL & " UNION ALL "
		SQL = SQL & "SELECT a.id, title, sent_date FROM archive_alerts a WHERE (a.type='S' OR a.type='L') AND param_temp_id IS NULL AND (class = 1 OR class = 16) AND a.sender_id IN ("& Join(gviewlist,",") &") AND a.id NOT IN (SELECT alert_id FROM instant_messages) AND title like'%"&search_title&"%')   res ORDER BY res."&sortby
        Set RS = Conn.Execute(SQL)
	else
		SQL = "SELECT * FROM (SELECT a.id, title, sent_date FROM alerts a WHERE (a.type='S' OR a.type='L') AND param_temp_id IS NULL AND (class = 1 OR class = 16) AND a.id NOT IN (SELECT alert_id FROM instant_messages)  AND title like'%"&search_title&"%'"
		SQL = SQL & " UNION ALL "
		SQL = SQL & "SELECT a.id, title, sent_date FROM archive_alerts a WHERE (a.type='S' OR a.type='L') AND param_temp_id IS NULL AND (class = 1 OR class = 16) AND a.id NOT IN (SELECT alert_id FROM instant_messages) AND title like'%"&search_title&"%')   res ORDER BY res."&sortby
		Set RS = Conn.Execute(SQL)
	end if

	num=offset
	if(Not RS.EOF) then RS.Move(offset) end if
	Do While Not RS.EOF
		num=num+1
		if((offset+limit) < num) then Exit Do end if
		if(num > offset AND offset+limit >= num) then
		
			%>
			<tr>
				<td>
					<% 
			Response.Write "<a href='#' onclick=""saveParameters('id="& CStr(RS("id"))&"')"">" & HtmlEncode(RS("title")) & "</a>"
					%>
				</td>
				<td>

					<script language="javascript">
						document.write(getUiDateTime('<%=RS("sent_date")%>'));
					</script>

				</td>
			</tr>
			<%
		end if
	RS.MoveNext
	Loop
	RS.Close


			%>
		</table>
		<%
	Response.Write make_pages(offset, cnt, limit, page, name, sortby) 

else

Response.Write "<center><b>"&LNG_THERE_ARE_NO_ALERTS&"</b></center>"

end if
else
%>
	</br>
	</br>
	</br>
	</br>
<%
end if
		%>
		</form>
	</center>
</body>
</html>
<% end if  %>
<!-- #include file="db_conn_close.asp" -->
