﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;

namespace DeskAlertsDotNet.Pages
{
    public partial class WidgetRealtime : DeskAlertsBasePage
    {
   
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static int GetCount()
        {
            DataBase.DBManager databaseManager = new DataBase.DBManager();
            int count = Convert.ToInt32(databaseManager.GetScalarByQuery("select count(*) as cnt from alerts_received_recur a where a.date <getdate() and a.date > getdate()- cast('00:00:01' as datetime)"));
            databaseManager.Dispose();
            return count;
        }
    }
}