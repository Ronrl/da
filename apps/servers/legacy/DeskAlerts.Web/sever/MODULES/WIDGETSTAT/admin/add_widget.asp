<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<script type="text/javascript">
		$(".add_this").button({ label: 'Add' });

		$(".add_this").click(function() {
			var item_id = guid().toUpperCase();
			var widget_id = $(this).attr("id");
			var title = $(this).attr("custom_title");
			var link =  $(this).attr("custom_link");
			addWidgetToPage(item_id, title, link);
			$("#statList").isotope('reloadItems').isotope({ sortBy: 'original-order' });
			getOrder();
			var caller = parent.document.getElementById("main_frame").contentWindow.location.pathname.split('/').pop();
			switch (caller) {
				case 'dashboard.asp':
					page = "D";
					break;
				default:
					page = "Z";
					break;
			}
			$("#add_widget_form").dialog("close");
			if (page != "Z") {
				var jqxhr = $.get("manipulate_widgets.asp", { id: item_id, action: "add", widget_id: widget_id, parameters: "", page: page });
			}
		});

		

		$(".widget-tile").hover(function() {
			$(this).find(".widget_hint").slideDown(250);
		}, function() {
			$(this).find(".widget_hint").slideUp(250);
		})

	var guid = (function() {
		function s4() {
			return Math.floor((1 + Math.random()) * 0x10000)
						   .toString(16)
						   .substring(1);
		}
		return function() {
			return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
					   s4() + '-' + s4() + s4() + s4();
		};
	})();
</script>

<%
page = Request("page")
prohibitedList = "('')"
if page = "stats" then
	prohibitedList = "('Calendar','Last Alerts', 'Instant Send', 'Create Alert', 'Activity Monitor')"
end if
Set RS = Conn.Execute("SELECT id, title, href FROM widgets WHERE title NOT IN "&prohibitedList & " ORDER BY priority")
Do While Not RS.EOF
if canShowWidget(RS("title")) =  true then
%>
	
	<div style='margin:10px;float:left;width:219px;height:219px;position:relative'>
		<div class="widget-tile">
			<img src='widgets/<%=Replace(RS("href"), ".aspx", "") %>/preview.png' width='219' height='219'>
			<div class="widget_hint">
			<%  
				lngString = "LNG_WIDGET_HINT_" & UCase(Replace(RS("title")," ","_"))
				Execute("description = "&lngString)
				Response.Write(description)
				
				lngString = "LNG_WIDGET_" & UCase(Replace(RS("title")," ","_"))
				Execute("localizedTitle = "&lngString)
			%>
			</div>
		</div>
		
		<div style='position:relative;text-align:left;background-color:rgba(0,0,0,0.5);z-index:999;bottom:32px;height:32px;'>
			<div style='float:left;width:56%;padding-top:7px;text-align:left;color:white;padding-left:7px;cursor:default;white-space:nowrap;overflow:hidden' title="<%=localizedTitle %>"><%=localizedTitle %></div>
			<div style='float:left;width:39%;padding-top:3px;'>
				<a href='#' class='add_this' id='<%=RS("id") %>' custom_title="<%=RS("title") %>" custom_link="<%=RS("href") %>" style='width:100%'>Add</a>
			</div>
		</div>
	</div>
<%
end if
	RS.MoveNext
Loop

 RS.Close
%>
<div style="padding:0px;margin:0px;height:20px;clear:both">&nbsp;</div>

<!-- #include file="db_conn_close.asp" -->




