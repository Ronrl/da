﻿using System;
using System.Web.UI;
using DeskAlerts.Server.Utils;

namespace DeskAlertsDotNet.Pages
{
    using Resources;

    public partial class WidgetUsersStats : DeskAlertsBasePage
    {
        //use curUser to access to current user of DA control panel      
        //use dbMgr to access to database

        public bool shouldShow;
        public string valuesJson;
        public string colorsJson;
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            var data = dbMgr.GetDataByQuery("SELECT SUM([disabled]) AS sum FROM [users] WHERE role = 'U'");
            var disabledUsers = data.IsNull(0, "sum") ? 0 : data.GetInt(0, "sum");
            var users = dbMgr.GetDataByQuery("SELECT [last_request] ,[next_request] ,[disabled] ,[standby] FROM [users] WHERE role = 'U'");
            var totalUsers = users.Count;
            var onlineUsers = 0;
            foreach (var user in users)
            {
                var lastRequest = user.GetDateTime("last_request");
                var nextRequest = user.GetInt("next_request");
                var disabled = user.GetInt("disabled") != 0;
                var standby = user.GetInt("standby") == 0;
                if (DeskAlertsUtils.IsUserOnline(lastRequest, nextRequest) && !disabled && standby)
                {
                    onlineUsers++;
                }
            }

            var onlineColor = "#3B6392";
            var offlineColor = "#9ABA59";
            var disabledColor = "#B161A2";

            var onlineKey = resources.LNG_ONLINE;
            var offlineKey = resources.LNG_OFFLINE;
            var disabledKey = resources.LNG_DISABLED;

            var onlineVal = onlineUsers;
            var offlineVal = totalUsers - onlineUsers - disabledUsers;
            var disabledVal = disabledUsers;

            var shouldShow = onlineVal != 0 || offlineVal != 0 || disabledVal != 0;
            valuesJson = string.Empty;
            colorsJson = string.Empty;
            var valuesJsonTemplate = "['{0}',{1}]";
            if (shouldShow)
            {
                if (onlineVal != 0)
                {
                    valuesJson += string.Format(valuesJsonTemplate, onlineKey, onlineVal);
                    colorsJson = "'" + onlineColor + "'";


                }
                onlineDiv.Style.Add(HtmlTextWriterStyle.BackgroundColor, onlineColor);
                onlineName.InnerText = onlineKey;
                onlineValue.InnerText = onlineVal.ToString();

                if (offlineVal != 0)
                {
                    if (valuesJson.Length > 0)
                        valuesJson += ",";

                    valuesJson += string.Format(valuesJsonTemplate, offlineKey, offlineVal);



                    if (colorsJson.Length > 0)
                        colorsJson += ",";

                    colorsJson += "'" + offlineColor + "'";

                }

                offlineDiv.Style.Add(HtmlTextWriterStyle.BackgroundColor, offlineColor);
                offlineName.InnerText = offlineKey;
                offlineValue.InnerText = offlineVal.ToString();

                if (disabledVal != 0)
                {
                    if (valuesJson.Length > 0)
                        valuesJson += ",";

                    valuesJson += string.Format(valuesJsonTemplate, disabledKey, disabledVal);

                    if (colorsJson.Length > 0)
                        colorsJson += ",";

                    colorsJson += "'" + disabledColor + "'";


                }
                disabledDiv.Style.Add(HtmlTextWriterStyle.BackgroundColor, disabledColor);
                disabledName.InnerText = disabledKey;
                disabledValue.InnerText = disabledVal.ToString();

            }
            else
            {
                messageLabel.Text = resources.LNG_THERE_ARE_NO_USERS;
                legend.Visible = false;
            }
        }
    }
}
