<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%
if Request("datesSet") = "" then
%>
<!doctype html>
<html>
	<head>
		<title>DeskAlerts</title>
		<link href="css/style9.css?v=9.1.12.29" rel="stylesheet" type="text/css"/>
		<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
		<link href="css/jquery/jquery_timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css"/>
		<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
		<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
		
		<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery_timepicker/jquery-ui-timepicker-addon.js"></script>
		<script language="javascript" type="text/javascript" src="jscripts/jquery/datepicker_fix.js"></script>
		<script language="javascript" type="text/javascript" src="jscripts/json2.js"></script>
		<script language="javascript" type="text/javascript" src="jscripts/date.js"></script>
		<script language="javascript" type="text/javascript" src="functions.js"></script>
		<script language="javascript" type="text/javascript">
		var settingsDateFormat = "<%=GetDateFormat() %>";
		var settingsTimeFormat = "<%=GetTimeFormat() %>";

		var responseDbFormat = "dd/MM/yyyy HH:mm:ss";
		var saveDbFormat = "dd/MM/yyyy HH:mm";
		var saveDbDateFormat = "dd/MM/yyyy";
		var uiFormat = getUiDateTimeFormat(settingsDateFormat, settingsTimeFormat);
		
		function check_date(form) {
			if (form.select_date_radio[1].checked) {
				try {

					var fromDate = $.trim($("#from_date").val());
					var toDate = $.trim($("#to_date").val());

					if (!fromDate) {
						alert("You enter wrong time period!");
						return false;
					}
					if (!toDate) {
						alert("You enter wrong time period!");
						return false;
					}

					if (!isDate(fromDate, settingsDateFormat)) {
						alert("You enter wrong time period!");
						return false;
					}
					else {
						if (fromDate != "") {
							fromDate = new Date(getDateFromFormat(fromDate, settingsDateFormat));
							$("#start_date").val(formatDate(fromDate, saveDbDateFormat));
						}
					}

					if (!isDate(toDate, settingsDateFormat)) {
						alert("You enter wrong time period!");
						return false;
					}
					else {
						if (toDate != "") {
							toDate = new Date(getDateFromFormat(toDate, settingsDateFormat))
							$("#end_date").val(formatDate(toDate, saveDbDateFormat));
						}
					}

					if (fromDate > toDate) {

						alert("You enter wrong time period!");
						return false;
					}
				}
				catch (e) { alert(e) }
			}
			return true;
		}
		$(document).ready(function() {

			$("#statistics_show_button").button();
			serverDate = new Date(getDateFromAspFormat("<%= now_db%>", "dd/MM/yyyy HH:mm:ss"));
			setInterval(function() { serverDate.setSeconds(serverDate.getSeconds() + 1); }, 1000);

			initDatePicker($(".date_param_input"), settingsDateFormat);

			$("input[name=select_date_radio]").click(function() {
				if ($("input[name=select_date_radio]:checked").val() == "1") {
					$("#from_date").prop("disabled", true);
					$("#to_date").prop("disabled", true);
					$("#range").prop("disabled", false);
				}
				else {
					$("#from_date").prop("disabled", false);
					$("#to_date").prop("disabled", false);
					$("#range").prop("disabled", true);
				}
			});

			$("#statistics_show_button").click(function() {
				if (check_date($("#date_form")[0])) {
					var parameters;
					var range = $("#range option:selected").val();
					var start_date = $("#start_date").val();
					if (start_date == "") {
						parameters = "datesSet=1&select_date_radio=1&range=" + range;
					}
					else {
						var end_date = $("#end_date").val();
						parameters = "datesSet=1&select_date_radio=2&start_date=" + start_date + "&end_date=" + end_date;
					}
					var id = window.frameElement.getAttribute("id");
					id = id.substring(1);
					var jqxhr = $.get("manipulate_widgets.asp", { id: id, action: "update", parameters: parameters })
					.done(function() {
						var src = window.frameElement.getAttribute("src");
						src = src + "?" + parameters;
						id = "i" + id;
						window.parent.changeIframeSrc(id, src);
					});

				}
			});


		});
		</script>
	</head>
	<body class="body" style="height:300px">
			<center style="padding-top:80px">
			<form name="frmList" id="date_form" action="#">
			<table cellpadding="2" cellspacing="2" border="0" >
				<tr>
					<td nowrap align="left">
						<input type="radio" name="select_date_radio" checked value="1"/>
					</td>
					<td  colspan="3">
						<select name="range" id="range" style="font-size: 9pt">
						<option value="1"><%=LNG_TODAY%></option>
						<option value="2"><%=LNG_YESTERDAY%></option>
						<option value="3"><%=LNG_LAST_DAYS%></option>
						<option value="4"><%=LNG_THIS_MONTH%></option>
						<option value="5"><%=LNG_LAST_MONTH%></option>
						<option value="6"><%=LNG_ALL_TIME%></option></select>
					</td>
				</tr>

				<tr valign="top">
					<td nowrap valign="middle">
						<input type="radio" name="select_date_radio" value="2"/> 
					</td>
					<td><%= LNG_SPECIFIC_DATES%></td>
				</tr>
				<tr>
					<td>From</td>
					<td><input name='from_date' class="date_param_input" id="from_date" type="text" style="font-size: 9pt" size="20" disabled/> 
						<input id="start_date" type="hidden" name="start_date"/>	</td>
				</tr>
				<tr>
					<td>To</td>
					<td nowrap valign="middle">
						<input name='to_date' class="date_param_input" id="to_date" type="text" style="font-size: 9pt" size="20" disabled/> 
						<input id="end_date" type="hidden" name="end_date"/>
					</td>
				</tr>
				<tr>
					<td colspan="4" style="" align="center">
						<input type="hidden" value="1" name="datesSet"/>
						<a id="statistics_show_button" href="#"><%=LNG_SHOW %></a>
					</td>
				</tr>
			</table>
			</form>
			</center>
	</body>
</html>
<%
else
	
	intSessionUserId = Session("uid")
	uid=intSessionUserId
	
	start_date = Request("start_date")

	end_date = Request("end_date")

	if(Request("select_date_radio")="1" OR Request("select_date_radio")="") then
		if(Request("range")="1" OR Request("range")="") then
				i=date()
			my_day = day(i)
			my_month = month(i)
			my_year = year (i)
				i=date()
			my_day1 = day(i)
			my_month1 = month(i)
			my_year1 = year (i)

			test_date=make_date_by(my_day, my_month, my_year)
			test_date1=make_date_by(my_day1, my_month1, my_year1)
			
			hintText = LNG_TODAY

		elseif(Request("range")="2") then
				i=date()-1
			my_day = day(i)
			my_month = month(i)
			my_year = year (i)

				i=date()-1
			my_day1 = day(i)
			my_month1 = month(i)
			my_year1 = year (i)

			test_date=make_date_by(my_day, my_month, my_year)
			test_date1=make_date_by(my_day1, my_month1, my_year1)
			
			hintText = LNG_YESTERDAY

		elseif(Request("range")="3") then

				i=date()-7
			my_day = day(i)
			my_month = month(i)
			my_year = year (i)

				i=date()
			my_day1 = day(i)
			my_month1 = month(i)
			my_year1 = year (i)

			test_date=make_date_by(my_day, my_month, my_year)
			test_date1=make_date_by(my_day1, my_month1, my_year1)
			
			hintText = LNG_LAST_DAYS

		elseif(Request("range")="4") then
			my_day = "01"
			my_month = month(date ())
			my_year = year (date())

			my_day1 = day(date())
			my_month1 = month(date())
			my_year1 = year(date())

			'date from config---	
			test_date=make_date_by(my_day, my_month, my_year)
			test_date1=make_date_by(my_day1, my_month1, my_year1)
			'-------------------
			
			hintText = LNG_THIS_MONTH
			
		elseif(Request("range")="5") then
			my_day = "01"
			my_month = month(date ())-1
			my_year = year (date())

			
			my_month1 = month(date ())-1
			my_year1 = year (date())
			my_day1 = "31"
			if(IsDate(my_month1 & "/" & my_day1 & "/" & my_year1)<>True) then
				my_day1="30"
				if(IsDate(my_month1 & "/" & my_day1 & "/" & my_year1)<>True) then
					my_day1="29"
					if(IsDate(my_month1 & "/" & my_day1 & "/" & my_year1)<>True) then
						my_day1="28"
						if(IsDate(my_month1 & "/" & my_day1 & "/" & my_year1)<>True) then
							my_day1="27"
						end if

					end if
	     		end if
			end if
			'date from config---

			test_date=make_date_by(my_day, my_month, my_year)
			test_date1=make_date_by(my_day1, my_month1, my_year1)
			
			hintText = LNG_LAST_MONTH

			'-------------------
		elseif(Request("range")="6") then
			my_day = "01"
			my_month = "01"
			my_year = "2001"

			my_day1 = day(date())
			my_month1 = month(date())
			my_year1 = year(date())

			'date from config---
			test_date=make_date_by(my_day, my_month, my_year)
			test_date1=make_date_by(my_day1, my_month1, my_year1)
			
			hintText = LNG_ALL_TIME

		end if
		from_date = test_date & " 00:00:00"
		to_date = test_date1 & " 23:59:59"
	else
		'date from config---

		from_date=start_date & " 00:00:00"

		to_date=end_date & " 23:59:59"
		
		hintText = "From " & start_date & " to " & end_date

		'-------------------
	end if
	
	'--- rights ---
	set rights = getRights(uid, true, true, true, null)
	gid = rights("gid")
	gsendtoall = rights("gsendtoall")
	gviewall = rights("gviewall")
	gviewlist = rights("gviewlist")
	'--------------

	if gviewall <> 1 then
		senderWhere = " AND sender_id IN ("& Join(gviewlist,",") &")"
	else
		senderWhere = ""
	end if

	if gsendtoall <> 1 then
		policy_ids = "policy_id = " & Join(rights("policy_ids"), " OR policy_id = ")
		policy_group_ids=editorGetGroupIds(policy_ids)
		policy_group_ids=replace(policy_group_ids, "group_id", "users_groups.group_id")
		if(policy_group_ids <> "") then
			policy_group_ids = "OR " & policy_group_ids
		end if
		ou_ids=editorGetOUIds(policy_ids)
		ou_ids=replace(ou_ids, "ou_id", "OU_User.Id_OU")
		if(ou_ids <> "") then
			ou_ids = "OR " & ou_ids
		end if
	end if
	
	users_cnt=0
broad_cnt=0
group_cnt=0
intReceivedAlertsNum=0
intAcknowNum=0

  Set RSUsers = Conn.Execute("SELECT COUNT(DISTINCT id) as mycnt FROM users WHERE role = 'U' AND reg_date <= '" & to_date & "'")
  if(not RSUsers.EOF) then 
	users_cnt=RSUsers("mycnt") 
  end if

'broadcast alerts
if gsendtoall = 1 then

	SQL = "SELECT id, create_date FROM alerts WHERE create_date >= '" & from_date & "' AND create_date <= '" & to_date & "' AND type2='B'" & senderWhere
	SQL = SQL & " UNION ALL "
	SQL = SQL & "SELECT id, create_date FROM archive_alerts WHERE create_date >= '" & from_date & "' AND create_date <= '" & to_date & "' AND type2='B'" & senderWhere

	Set RSBroad = Conn.Execute(SQL)
	Do While not RSBroad.EOF
		broad_date=RSBroad("create_date")
		broadAlertId=RSBroad("id")
		Set RSUsers = Conn.Execute("SELECT COUNT(DISTINCT users.id) as mycnt FROM users WHERE role = 'U' AND reg_date <= '" & broad_date & "'")
		if(not RSUsers.EOF) then 
			broadusers_cnt=RSUsers("mycnt") 
			broad_cnt=broad_cnt+broadusers_cnt
		end if
		'received
		SQL = "SELECT COUNT(DISTINCT user_id) as mycnt, 1 as mycnt1 FROM [alerts_received] WHERE alert_id=" & broadAlertId
		SQL = SQL & " UNION ALL "
		SQL = SQL & "SELECT COUNT(DISTINCT user_id) as mycnt, 2 as mycnt1 FROM [archive_alerts_received] WHERE alert_id=" & broadAlertId
		Set RS7 = Conn.Execute(SQL)
		Do while not RS7.EOF
			broadRecCnt=0
			if(Not IsNull(RS7("mycnt"))) then broadRecCnt=Clng(RS7("mycnt")) end if
			intReceivedAlertsNum=intReceivedAlertsNum+broadRecCnt
			RS7.MoveNext
		loop
		'read
		    SQL = "SELECT COUNT(DISTINCT user_id) as mycnt, 1 as mycnt1 FROM [alerts_read] WHERE alert_id=" & broadAlertId
			SQL = SQL & " UNION ALL "
			SQL = SQL & "SELECT COUNT(DISTINCT user_id) as mycnt, 2 as mycnt1 FROM [archive_alerts_read] WHERE alert_id=" & broadAlertId
			Set RS8 = Conn.Execute(SQL)
		do while not RS8.EOF
			broadAckCnt = 0
			if(Not IsNull(RS8("mycnt"))) then  broadAckCnt=Clng(RS8("mycnt") ) end if
			intAcknowNum=intAcknowNum+broadAckCnt
			RS8.MoveNext
		loop

		RSBroad.MoveNext
	loop
end if

group_ids=""
groupFlag=0

if gviewall <> 1 then
	SQL = "SELECT alerts.id as id, create_date FROM alerts WHERE create_date >= '" & from_date & "' AND create_date <= '" & to_date & "' AND sender_id IN ("& Join(gviewlist,",") &")"
	SQL = SQL & " UNION ALL "
	SQL = SQL & " SELECT archive_alerts.id as id, create_date FROM archive_alerts WHERE create_date >= '" & from_date & "' AND create_date <= '" & to_date & "' AND sender_id IN ("& Join(gviewlist,",") &")"
	Set RSGroup = Conn.Execute(SQL)
else
	SQL = "SELECT alerts.id as id, create_date FROM alerts WHERE create_date >= '" & from_date & "' AND create_date <= '" & to_date & "'"
	SQL = SQL & " UNION ALL "
	SQL = SQL & "SELECT archive_alerts.id as id, create_date FROM archive_alerts WHERE create_date >= '" & from_date & "' AND create_date <= '" & to_date & "'"
	Set RSGroup = Conn.Execute(SQL)
end if
  Do While not RSGroup.EOF
	 alertDate=RSGroup("create_date")
	 groupAlertId=RSGroup("id")

	alert_id=groupAlertId
	strAlertDate=alertDate
	group_ids=""
	groupFlag=0
	SQL = "SELECT alerts_sent_group.group_id as group_id FROM alerts_sent_group INNER JOIN alerts ON alerts.id=alerts_sent_group.alert_id WHERE alerts_sent_group.alert_id=" & alert_id
	SQL = SQL & " UNION ALL "
	SQL = SQL & "SELECT archive_alerts_sent_group.group_id as group_id FROM archive_alerts_sent_group INNER JOIN archive_alerts ON archive_alerts.id=archive_alerts_sent_group.alert_id WHERE archive_alerts_sent_group.alert_id=" & alert_id
	Set RSGroupUsers = Conn.Execute(SQL)
	Do While not RSGroupUsers.EOF
		group_id=RSGroupUsers("group_id")
		if(groupFlag=1) then
			group_ids=group_ids & " OR " & " group_id=" & group_id 
		else
			group_ids=group_ids & " group_id=" & group_id 
		end if
		groupFlag=1
		RSGroupUsers.MoveNext	
	loop
	group_ids=replace(group_ids, "group_id", "users_groups.group_id")
	if(group_ids<>"") then
	
	Set RSUsers = Conn.Execute("SELECT COUNT(DISTINCT users.id) as mycnt FROM users INNER JOIN users_groups ON users.id=users_groups.user_id WHERE ("&group_ids&") AND role = 'U' AND reg_date <= '" & strAlertDate & "'")
	if(not RSUsers.EOF) then 
		groupusers_cnt=RSUsers("mycnt") 
		group_cnt=group_cnt+groupusers_cnt


	end if
	end if

  RSGroup.MoveNext
  loop

intSentAlertsNum=0
if gviewall <> 1 then
	SQL = "SELECT COUNT(alerts_sent_stat.user_id) as mycnt, 1 as mycnt1 FROM [alerts_sent_stat] INNER JOIN alerts ON alerts.id=alerts_sent_stat.alert_id WHERE [date] >= '" & from_date & "' AND [date] <= '" & to_date & "' AND sender_id IN ("& Join(gviewlist,",") &")"
	SQL = SQL & " UNION ALL "
	SQL = SQL & "SELECT COUNT(archive_alerts_sent_stat.user_id) as mycnt, 2 as mycnt1 FROM [archive_alerts_sent_stat] INNER JOIN archive_alerts ON archive_alerts.id=archive_alerts_sent_stat.alert_id WHERE [date] >= '" & from_date & "' AND [date] <= '" & to_date & "' AND sender_id IN ("& Join(gviewlist,",") &")"
	Set RS7 = Conn.Execute(SQL)
else
	SQL = "SELECT COUNT(alerts_sent_stat.user_id) as mycnt, 1 as mycnt1 FROM [alerts_sent_stat] WHERE [date] >= '" & from_date & "' AND [date] <= '" & to_date & "'"
	SQL = SQL & " UNION ALL "
	SQL = SQL & "SELECT COUNT(archive_alerts_sent_stat.user_id) as mycnt, 2 as mycnt1 FROM [archive_alerts_sent_stat] WHERE [date] >= '" & from_date & "' AND [date] <= '" & to_date & "'"
	Set RS7 = Conn.Execute(SQL)
end if

'crutch for personal and group alerts at once
if gviewall <> 1 then
	SQL = "SELECT COUNT(alerts_sent_stat.user_id) as mycnt, 1 as mycnt1 FROM [alerts_sent_stat] INNER JOIN alerts ON alerts.id=alerts_sent_stat.alert_id INNER JOIN alerts_sent_group ON alerts_sent_stat.alert_id=alerts_sent_group.alert_id INNER JOIN users_groups ON alerts_sent_stat.user_id = users_groups.user_id WHERE [date] >= '" & from_date & "' AND [date] <= '" & to_date & "' AND sender_id IN ("& Join(gviewlist,",") &")"
	SQL = SQL & " UNION ALL "
	SQL = SQL & "SELECT COUNT(archive_alerts_sent_stat.user_id) as mycnt, 2 as mycnt1 FROM [archive_alerts_sent_stat] INNER JOIN archive_alerts ON archive_alerts.id=archive_alerts_sent_stat.alert_id INNER JOIN archive_alerts_sent_group ON archive_alerts_sent_stat.alert_id=archive_alerts_sent_group.alert_id INNER JOIN users_groups ON archive_alerts_sent_stat.user_id = users_groups.user_id WHERE [date] >= '" & from_date & "' AND [date] <= '" & to_date & "' AND sender_id IN ("& Join(gviewlist,",") &")"
	Set RS77 = Conn.Execute(SQL)
else
	SQL = "SELECT COUNT(alerts_sent_stat.user_id) as mycnt, 1 as mycnt1 FROM [alerts_sent_stat] INNER JOIN alerts_sent_group ON alerts_sent_stat.alert_id=alerts_sent_group.alert_id INNER JOIN users_groups ON alerts_sent_stat.user_id = users_groups.user_id	WHERE [date] >= '" & from_date & "' AND [date] <= '" & to_date & "'"
	SQL = SQL & " UNION ALL "
	SQL = SQL & "SELECT COUNT(archive_alerts_sent_stat.user_id) as mycnt, 2 as mycnt1 FROM [archive_alerts_sent_stat] INNER JOIN archive_alerts_sent_group ON archive_alerts_sent_stat.alert_id=archive_alerts_sent_group.alert_id INNER JOIN users_groups ON archive_alerts_sent_stat.user_id = users_groups.user_id		WHERE [date] >= '" & from_date & "' AND [date] <= '" & to_date & "'"
	Set RS77 = Conn.Execute(SQL)
end if

if gviewall <> 1 then
	SQL = "SELECT COUNT(DISTINCT alerts_received.user_id) as mycnt, 1 as mycnt1 FROM [alerts] INNER JOIN alerts_received ON alerts.id=alerts_received.alert_id WHERE alerts.type2='R' AND alerts.sent_date >= '" & from_date & "' AND alerts.sent_date <= '" & to_date & "' AND sender_id IN ("& Join(gviewlist,",") &") GROUP BY alert_id"
	SQL = SQL & " UNION ALL "
	SQL = SQL & "SELECT COUNT(DISTINCT archive_alerts_received.user_id) as mycnt, 2 as mycnt1 FROM [archive_alerts] INNER JOIN archive_alerts_received ON archive_alerts.id=archive_alerts_received.alert_id WHERE archive_alerts.type2='R' AND archive_alerts.sent_date >= '" & from_date & "' AND archive_alerts.sent_date <= '" & to_date & "' AND sender_id IN ("& Join(gviewlist,",") &") GROUP BY alert_id"
	Set RS8 = Conn.Execute(SQL)
else
	SQL = "SELECT COUNT(DISTINCT alerts_received.user_id) as mycnt, 1 as mycnt1 FROM [alerts] INNER JOIN alerts_received ON alerts.id=alerts_received.alert_id WHERE alerts.type2='R' AND alerts.sent_date >= '" & from_date & "' AND alerts.sent_date <= '" & to_date & "' GROUP BY alert_id"
	SQL = SQL & " UNION ALL "
	SQL = SQL & "SELECT COUNT(DISTINCT archive_alerts_received.user_id) as mycnt, 2 as mycnt1 FROM [archive_alerts] INNER JOIN archive_alerts_received ON archive_alerts.id=archive_alerts_received.alert_id WHERE archive_alerts.type2='R' AND archive_alerts.sent_date >= '" & from_date & "' AND archive_alerts.sent_date <= '" & to_date & "' GROUP BY alert_id"
	Set RS8 = Conn.Execute(SQL)
end if

if gviewall <> 1 then
	SQL = "SELECT COUNT(DISTINCT alerts_read.user_id) as mycnt, 1 as mycnt1 FROM [alerts] INNER JOIN alerts_read ON alerts.id=alerts_read.alert_id WHERE alerts.type2='R' AND alerts.sent_date >= '" & from_date & "' AND alerts.sent_date <= '" & to_date & "' AND sender_id IN ("& Join(gviewlist,",") &") GROUP BY alert_id"
	SQL = SQL & " UNION ALL "
	SQL = SQL & "SELECT COUNT(DISTINCT archive_alerts_read.user_id) as mycnt, 2 as mycnt1 FROM [archive_alerts] INNER JOIN archive_alerts_read ON archive_alerts.id=archive_alerts_read.alert_id WHERE archive_alerts.type2='R' AND archive_alerts.sent_date >= '" & from_date & "' AND archive_alerts.sent_date <= '" & to_date & "' AND sender_id IN ("& Join(gviewlist,",") &") GROUP BY alert_id"
	Set RS9 = Conn.Execute(SQL)
else
	SQL = "SELECT COUNT(DISTINCT alerts_read.user_id) as mycnt, 1 as mycnt1 FROM [alerts] INNER JOIN alerts_read ON alerts.id=alerts_read.alert_id WHERE alerts.type2='R' AND alerts.sent_date >= '" & from_date & "' AND alerts.sent_date <= '" & to_date & "' GROUP BY alert_id"
	SQL = SQL & " UNION ALL "
	SQL = SQL & "SELECT COUNT(DISTINCT archive_alerts_read.user_id) as mycnt, 2 as mycnt1 FROM [archive_alerts] INNER JOIN archive_alerts_read ON archive_alerts.id=archive_alerts_read.alert_id WHERE archive_alerts.type2='R' AND archive_alerts.sent_date >= '" & from_date & "' AND archive_alerts.sent_date <= '" & to_date & "' GROUP BY alert_id"
	Set RS9 = Conn.Execute(SQL)
end if

	intSentAlertsNum=0
	do while not RS7.EOF
		if(Not IsNull(RS7("mycnt"))) then intSentAlertsNum=intSentAlertsNum+Clng(RS7("mycnt")) end if
		RS7.MoveNext
	loop
	'remove count for alerts that was sent personal to users AND also that users in groups
	do while not RS77.EOF
		if(Not IsNull(RS77("mycnt"))) then
			if(intSentAlertsNum-Clng(RS77("mycnt")) > 0) then
				intSentAlertsNum=intSentAlertsNum-Clng(RS77("mycnt"))
			end if
		end if
		RS77.MoveNext
	loop
	
	do while not RS8.EOF
		if(Not IsNull(RS8("mycnt"))) then intReceivedAlertsNum=intReceivedAlertsNum+Clng(RS8("mycnt")) end if
		RS8.MoveNext
	loop
	do while not RS9.EOF
		if(Not IsNull(RS9("mycnt"))) then intAcknowNum=intAcknowNum+Clng(RS9("mycnt")) end if
		RS9.MoveNext
	loop	


  intSentAlertsNum=intSentAlertsNum+broad_cnt+group_cnt




  if(IsNull(intClicksNum)) then intClicksNum=0 end if
  if(IsNull(intReceivedAlertsNum)) then intReceivedAlertsNum=0 end if
  if(IsNull(intAcknowNum)) then intAcknowNum=0 end if


intDoesntAlertsNum=intSentAlertsNum-intReceivedAlertsNum
if(intDoesntAlertsNum<0) then 
	intDoesntAlertsNum = 0 
end if

    alertsVal1Color="3B6392"
	alertsVal2Color="A44441"
	alertsVal3Color="9ABA59"

    alertsVal1Name=LNG_RECEIVED
	alertsVal2Name=LNG_ACKNOWLEDGED
	alertsVal3Name=LNG_NOT_RECEIVED

    alertsVal1Value=CStr(intReceivedAlertsNum)
	alertsVal2Value=CStr(intAcknowNum)
	alertsVal3Value=CStr(intDoesntAlertsNum)
	totalAlertsValue=intSentAlertsNum

	if(alertsVal1Value<>"0" OR alertsVal2Value<>"0" OR alertsVal3Value <> "0") then
		drawAlerts = "true"
		if(alertsVal1Value<>"0") then
			alertsValues = "['"&alertsVal1Name&"',"&alertsVal1Value&"]"
			alertsColors = "'#"&alertsVal1Color&"'"
		end if
		if(alertsVal2Value<>"0") then
			if (alertsValues <> "") then alertsValues = alertsValues&"," end if
			alertsValues = alertsValues&"['"&alertsVal2Name&"',"&alertsVal2Value&"]"
			if (alertsColors <> "") then alertsColors = alertsColors&"," end if
			alertsColors = alertsColors&"'#"&alertsVal2Color&"'"
		end if
		if(alertsVal3Value<>"0") then
			if (alertsValues <> "") then alertsValues = alertsValues&"," end if
			alertsValues = alertsValues&"['"&alertsVal3Name&"',"&alertsVal3Value&"]"
			if (alertsColors <> "") then alertsColors = alertsColors&"," end if
			alertsColors = alertsColors&"'#"&alertsVal3Color&"'"
		end if
	else
		drawAlerts = "false"
		alertsValues = ""
		alertsColors = ""
	end if

%>
<!doctype html>
<html>
	<head>
		<title>DeskAlerts</title>
		<link href="css/style9.css" rel="stylesheet" type="text/css">
		<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
		<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
		<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
		
		<!--[if lt IE 10]><script language="javascript" type="text/javascript" src="jscripts/excanvas.min.js"></script><![endif]-->
		<script language="javascript" type="text/javascript" src="jscripts/jquery.jqplot.min.js"></script>
		<script language="javascript" type="text/javascript" src="jscripts/jqplot.highlighter.min.js"></script>
		<script type="text/javascript" src="jscripts/jqplot.pieRenderer.min.js"></script>
		<script type="text/javascript" src="jscripts/jqplot.barRenderer.min.js"></script>
		<script type="text/javascript" src="jscripts/jqplot.categoryAxisRenderer.min.js"></script>
		<link rel="stylesheet" type="text/css" href="css/jquery.jqplot.css" />
		
		<script language="javascript" type="text/javascript">
		$(document).ready(function(){
		if("<%=drawAlerts%>" == "true"){
			var data = [<%= alertsValues%>];
			var alertsPlot = jQuery.jqplot ('alertsChart', [data], 
			{ 
				title:'<%=hintText %>',
				grid:{
					background: '#ffffff',
					renderer: $.jqplot.CanvasGridRenderer,
					drawBorder: false,
					shadow:false
				},
				seriesColors:[<%= alertsColors%>],
				seriesDefaults: {
					shadow:true,
					shadowAlpha: 0.1,
					renderer: jQuery.jqplot.PieRenderer, 
					rendererOptions: {
						diameter:220,
						padding: 25,
						highlightMouseOver:false
					}
				},
				legend:{
					show:false,
					location:"e"
				},
				highlighter:{
					useAxesFormatters:false,
					show:true,
					tooltipFormatString:'%s',
					fadeTooltip:false
				}	
			}
			);
		}
		else
		{
			var alertsPlot = jQuery.jqplot ('alertsChart', [['',1]], 
			{ 
				title:'<%=hintText %>',
				grid:{
					background: '#ffffff',
					renderer: $.jqplot.CanvasGridRenderer,
					drawBorder: false,
					shadow:false
				},
				seriesColors:['#f0f0f1'],
				seriesDefaults: {
					shadow:true,
					shadowAlpha: 0.1,
					renderer: jQuery.jqplot.PieRenderer, 
					rendererOptions: {
						diameter:220,
						padding: 25,
						highlightMouseOver:false
					}
				},
				legend:{
					show:false,
					location:"e"
				}	
			}
			);
		}
	});
		</script>
		<style type="text/css">
			.jqplot-highlighter-tooltip{font-size:12px}
		</style>
	</head>
	<body>
		<center>
		<!--<strong><span style="font-family:Arial;font-size:14px"><%=hintText %></span></strong>-->
		<div id='alertsChart' style='width:300px;height:300px'></div>
		<table border=0 class='chartLegend'>
			<tr><td><div class="legend" style='background-color: #<%=alertsVal1Color%>;'></div></td><td><%=alertsVal1Name%></td><td><%=alertsVal1Value%></td></tr>
			<tr><td><div class="legend" style='background-color: #<%=alertsVal2Color%>;'></div></td><td><%=alertsVal2Name%></td><td><%=alertsVal2Value%></td></tr>
			<tr><td><div class="legend" style='background-color: #<%=alertsVal3Color%>;'></div></td><td><%=alertsVal3Name%></td><td><%=alertsVal3Value%></td></tr>
		</table>
		</center>
	</body>
</html>
<%end if %>
<!-- #include file="db_conn_close.asp" -->