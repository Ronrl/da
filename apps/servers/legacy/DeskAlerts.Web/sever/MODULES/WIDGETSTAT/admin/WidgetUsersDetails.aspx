﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WidgetUsersDetails.aspx.cs" Inherits="DeskAlertsDotNet.Pages.WidgetUsersDetails" %>
<%@ Import NameSpace="DeskAlertsDotNet.Parameters" %>
<%@ Import Namespace="Resources" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<title>DeskAlerts</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link href="css/style9.css" rel="stylesheet" type="text/css">
        <link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <link href="css/jquery/jquery_timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css">
        <script language="javascript" type="text/javascript" src="functions.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/date.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery_timepicker/jquery-ui-timepicker-addon.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/jquery/datepicker_fix.js"></script>
        <script language="javascript" type="text/javascript">


            $(document).ready(function() {
                $(".delete_button").button({
                });

                $("#selectAll").click(function () {
                    var checked = $(this).prop('checked');
                    $(".content_object").prop('checked', checked);
                });


            });

            function changeParameters(id, parameters) {

                var widgetId = window.frameElement.getAttribute("id");
                widgetId = widgetId.substring(1);
                $.ajax({
                    type: "POST",
                    url: "WidgetService.aspx/UpdateWidget",
                    data: "{widgetId: \"" + widgetId + "\", parameters:\"" + parameters + "\"}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {

                        parent.changeParameters(widgetId, parameters);
                      //  location.href = "widget_survey_view.asp?" + parameters;

                        location.href =  "UserStatisticsDetails.aspx?" + parameters + "&widget=1&from_date=01/01/2001%2000:00:00&to_date=11/10/2111%2023:59:59";
                        //  window.parent.parent.document.getElementById('menu').src = window.parent.parent.document.getElementById('menu').src;
                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            }

            function showPreview(id) {
                window.open('UserDetails.aspx?id=' + id, '','status=0, toolbar=0, width=350, height=350, scrollbars=1');
            }

            function editUser(id, queryString)
            {
                location.replace('EditUsers.aspx?id='+ id +'&return_page=users_new.asp?' + queryString);
            }

        </script>
</head>
<body>
    <form id="form1" runat="server">
    <table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	    <tr>
	    <td>
	    <table width="100%" height="100%" cellspacing="0" cellpadding="0" class="widget_main_table">
		    <tr>
		    </tr>
            <tr>
            <td class="main_table_body" height="100%">
	                <br /><br />
			<div style="margin-left:10px" runat="server" id="mainTableDiv">
		        <table>
			        <tr>
				        <td>
					        <%=resources.LNG_SEARCH_USER_BY_NAME %>:
				        </td>
				        <td>
					        <input runat="server" type="text" id="searchTitle" style="width: 100px" value="" />
				        </td>
				        <td>
					        <asp:LinkButton class="search_button ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" runat="server" id="search" type="submit" Text="<span class='ui-button-icon-primary ui-icon ui-icon-search'></span><span class='ui-button-text'>{Search}</span>"/>
                        
				        </td>
			        </tr>
		        </table>

			   <table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr><td> 
					<div style="margin-left:10px" id="topPaging" runat="server"></div>
				</td></tr>
				</table>
				<br>

				<br><br>
				<asp:Table style="padding-left: 0px;margin-left: 0px;margin-right: 15px;" runat="server" id="contentTable" Width="98.5%" Height="100%" CssClass="data_table" CellSpacing="0" CellPadding="3">
				<asp:TableRow CssClass="data_table_title">
					<asp:TableCell runat="server" Width="90%" id="userName" CssClass="table_title" ></asp:TableCell>

				</asp:TableRow>
				</asp:Table>
				<br>

				 <br><br>
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				 
				<tr><td> 
					<div style="margin-left:10px" id="bottomRanging" runat="server"></div>
				</td></tr>
				</table>
			</div>
			<div runat="server" id="NoRows">
                <br />
                <br />
				<center><b><%=resources.LNG_THERE_ARE_NO_USERS%></b></center>
			</div>
			<br><br>
            </td>
            </tr>
           
 
            
             </table>
    </form>
</body>
</html>
