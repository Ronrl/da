﻿using System;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Models.Permission;

namespace DeskAlertsDotNet.Pages
{
    using DeskAlerts.ApplicationCore.Enums;
    using Resources;

    public partial class WidgetInstants : DeskAlertsBasePage
    {
        //use curUser to access to current user of DA control panel      
        //use dbMgr to access to database
        string BuildInstantTile(DataRow row)
        {
            if (row == null)
                return "";
            string tileHtml = @"<li id='" + row.GetString("id") + "' class='im_tile' style='float:left; margin:10px'>";


            bool canSend = (row.IsNull("approve_status") || row.GetInt("approve_status") == 1) &&
                           (curUser.HasPrivilege || curUser.Policy[Policies.IM].CanSend);


            if (canSend)
                tileHtml += "<a style='text-decoration:none' href='#' onclick='Send(" + row.GetInt("alert_id") + ")' target='work_area'>";

            string title = row.GetString("title");



            string shortTitle = title.Clone().ToString();



            object colorObj = row.GetString("color_code").Length == 0 ? null
                : dbMgr.GetScalarByQuery("SELECT color FROM color_codes WHERE id='" + row.GetString("color_code") + "'");


            string color = "";
            if (colorObj == null)
                color = "747474";
            else
                color = colorObj.ToString();


            if (shortTitle.Length > 17)
                shortTitle = shortTitle.Substring(0, 14) + "...";

            tileHtml += "<div class='im_tile_header'><span style='margin:0px' class='header_title header_white' title='This message id: " + row.GetString("id") + "'> " + shortTitle + " </span></div>";

            tileHtml += "<div style='vertical-align:middle; background:#" + color + "' class='im_tile_send' title='" + resources.LNG_SEND + "'>";

            if (row.GetInt("approve_status") == 1)
                tileHtml += "<img id='" + row.GetString("id") + "' src='images/action_icons/send_big_white.png' style='display:block;margin:auto;border:none;padding-top:15px;padding-left:10px' width='100'/>";
            else if (row.GetInt("approve_status") == 0)
                tileHtml += "<img id='" + row.GetString("id") + "' src='images/action_icons/pending_white.png' style='display:block;margin:auto;border:none;padding-top:15px;padding-left:10px' width='100'/>";

            tileHtml += "<script>";//pending_"
            if (row.IsNull("approve_status") || row.GetInt("approve_status") == 1)
                tileHtml += "$('#" + row.GetString("id") + "').attr('src', 'images/action_icons/send_big_' + darkOrLight('" + color + "') + '.png');";
            else
                tileHtml += "$('#" + row.GetString("id") + "').attr('src', 'images/action_icons/pending_' + darkOrLight('" + color + "') + '.png');";
            tileHtml += "</script>";

            if (canSend)
                tileHtml += "</a>";

            tileHtml += "<a href='javascript:parent.showAlertPreview(\"" + row.GetString("alert_id") + "\")'><div class='im_tile_preview hover' style='margin-top: 25px;' title=" + resources.LNG_PREVIEW + "><img src='images/action_icons/preview_big.png' alt=" + resources.LNG_PREVIEW + " title=" + resources.LNG_PREVIEW + " style='display:block;margin:7px auto;border:none;width:22px;height:22px'/></div></a>";

            tileHtml += "<a href='CreateAlert.aspx?id=" + row.GetInt("alert_id") + "&instant=1&edit=1' target='work_area'><div class='im_tile_preview hover' style='margin-top: 25px; 'title=" + resources.LNG_EDIT + "><img src='images/action_icons/pencil_20x20.png' alt=" + resources.LNG_EDIT + " title=" + resources.LNG_EDIT + " style='display:block;margin:10px auto;border:none;'/></div></a>";
            tileHtml += "<a href='#' onclick='DeleteAlert(" + row.GetInt("alert_id") + ")'><div class='im_tile_delete hover' style='margin-top: 25px;' title=" + resources.LNG_DELETE + "><img src='images/action_icons/delete_big.png' style='display:block;margin:10px auto;border:none;'/></div></a>";
            tileHtml += "</li>";

            return tileHtml;
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            var sqlQuery = $@"
                SELECT im.id as id, im.alert_id, a.title, a.approve_status, a.color_code 
                FROM alerts a 
                INNER JOIN instant_messages im ON a.id = im.alert_id 
                WHERE a.approve_status = {(int)ApproveStatus.ApprovedByDefault} 
                    OR a.approve_status = {(int)ApproveStatus.NotModerated} 
                    OR a.approve_status IS NULL 
                ORDER BY create_date DESC";
            
            var imSet = dbMgr.GetDataByQuery(sqlQuery);

            noAlertsDiv.Visible = imSet.IsEmpty;
            giveMeHeight.Visible = !imSet.IsEmpty;

            foreach(var imRow in imSet)
            {
                imTiles.InnerHtml += BuildInstantTile(imRow);
            }
        }
    }
}