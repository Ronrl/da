﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WidgetOverallStat.aspx.cs" Inherits="DeskAlertsDotNet.Pages.WidgetOverallStat" %>
<%@ Import NameSpace="DeskAlertsDotNet.Parameters" %>
<%@ Import NameSpace="DeskAlertsDotNet.Utils" %>
<%@ Import NameSpace="Resources" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title>DeskAlerts</title>
		<link href="css/style9.css?v=9.1.12.29" rel="stylesheet" type="text/css"/>
		<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
		<link href="css/jquery/jquery_timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css"/>
		<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
		<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
		
		<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery_timepicker/jquery-ui-timepicker-addon.js"></script>
		<script language="javascript" type="text/javascript" src="jscripts/jquery/datepicker_fix.js"></script>
		<script language="javascript" type="text/javascript" src="jscripts/json2.js"></script>
		<script language="javascript" type="text/javascript" src="jscripts/date.js"></script>
		<script language="javascript" type="text/javascript" src="functions.js"></script>
		<script language="javascript" type="text/javascript">
		    var settingsDateFormat = "dd/MM/yyyy";
		var settingsTimeFormat = "2";

		var responseDbFormat = "dd/MM/yyyy HH:mm:ss";
		var saveDbFormat = "dd/MM/yyyy HH:mm";
		var saveDbDateFormat = "dd/MM/yyyy";
		var uiFormat = getUiDateTimeFormat(settingsDateFormat, settingsTimeFormat);
		
		function check_date(form) {
			if ($("input[name=select_date_radio]:checked").val() == "2") {
				try {

					var fromDate = $.trim($("#from_date").val());
					var toDate = $.trim($("#to_date").val());

					if (!fromDate) {
						alert("You enter wrong time period!");
						return false;
					}
					if (!toDate) {
						alert("You enter wrong time period!");
						return false;
					}

					if (!isDate(fromDate, settingsDateFormat)) {
						alert("You enter wrong time period!");
						return false;
					}
					else {
						if (fromDate != "") {
							fromDate = new Date(getDateFromFormat(fromDate, settingsDateFormat));
							$("#start_date").val(formatDate(fromDate, saveDbDateFormat));
						}
					}

					if (!isDate(toDate, settingsDateFormat)) {
						alert("You enter wrong time period!");
						return false;
					}
					else {
						if (toDate != "") {
							toDate = new Date(getDateFromFormat(toDate, settingsDateFormat))
							$("#end_date").val(formatDate(toDate, saveDbDateFormat));
						}
					}

					if (fromDate > toDate) {

						alert("You enter wrong time period!");
						return false;
					}
				}
				
				catch (e) { alert(e) }
			}
			return true;
		}

		$(document).ready(function() {

			$("#statistics_show_button").button();
			serverDate = new Date(getDateFromAspFormat("<%= DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture)%>", "dd/MM/yyyy HH:mm:ss"));
			setInterval(function() { serverDate.setSeconds(serverDate.getSeconds() + 1); }, 1000);

			initDatePicker($(".date_param_input"), settingsDateFormat);

			$("input[name=select_date_radio]").click(function() {
				if ($("input[name=select_date_radio]:checked").val() == "1") {
					$("#from_date").prop("disabled", true);
					$("#to_date").prop("disabled", true);
					$("#range").prop("disabled", false);
				}
				else {
					$("#from_date").prop("disabled", false);
					$("#to_date").prop("disabled", false);
					$("#range").prop("disabled", true);
				}
			});

			$("#statistics_show_button").click(function() {
				if (check_date($("#date_form")[0])) {
					var parameters;
					var range = $("#range option:selected").val();
					var start_date = $("#from_date").val();

					var isSpicifiedDate = $("input[name=select_date_radio]:checked").val() == "2";
					if (!isSpicifiedDate) {
						parameters = "datesSet=1&select_date_radio=1&range=" + range;
					}
					else {
						var end_date = $("#to_date").val();
						parameters = "datesSet=1&select_date_radio=2&start_date=" + start_date + "&end_date=" + end_date;
					}
					var id = window.frameElement.getAttribute("id");
					id = id.substring(1);


					$.ajax({
					    type: "POST",
					    url: "WidgetService.aspx/UpdateWidget",
					    data: "{widgetId: \"" + id + "\", parameters:\"" + parameters + "\"}",
					    contentType: "application/json; charset=utf-8",
					    dataType: "json",
					    success: function (response) {

					        parent.changeParameters(id, parameters);
					        location.href = "WidgetOverallStat.aspx?" + parameters;
					        //  window.parent.parent.document.getElementById('menu').src = window.parent.parent.document.getElementById('menu').src;
					    },
					    failure: function (response) {
					        alert(response.d);
					    }
					});

				}
			});


		});
		</script>
</head>
<body style="font-size: 12px;height: 356px">
    <form id="form1" runat="server">
    <div runat="server" id="paramsDiv">
			<center style="padding-top:80px">
			<form name="frmList" id="date_form" action="#">
			<table cellpadding="2" cellspacing="2" border="0" >
				<tr>
					<td nowrap align="left">
						<input type="radio" name="select_date_radio" checked value="1"/>
					</td>
					<td  colspan="3">
						<select name="range" id="range" style="font-size: 9pt">
						<option value="1"><%=resources.LNG_TODAY%></option>
						<option value="2"><%=resources.LNG_YESTERDAY%></option>
						<option value="3"><%=resources.LNG_LAST_DAYS%></option>
						<option value="4"><%=resources.LNG_THIS_MONTH%></option>
						<option value="5"><%=resources.LNG_LAST_MONTH%></option>
						<option value="6"><%=resources.LNG_ALL_TIME%></option></select>
					</td>
				</tr>

				<tr valign="top">
					<td nowrap valign="middle">
						<input type="radio" name="select_date_radio" value="2"/> 
					</td>
					<td><%= resources.LNG_SPECIFIC_DATES%></td>
				</tr>
				<tr>
					<td>From</td>
					<td><input name='from_date' class="date_param_input" id="from_date" type="text" style="font-size: 9pt" size="20" disabled/> 
						<input id="start_date" type="hidden" name="start_date"/>	</td>
				</tr>
				<tr>
					<td>To</td>
					<td nowrap valign="middle">
						<input name='to_date' class="date_param_input" id="to_date" type="text" style="font-size: 9pt" size="20" disabled/> 
						<input id="end_date" type="hidden" name="end_date"/>
					</td>
				</tr>
				<tr>
					<td colspan="4" style="" align="center">
						<input type="hidden" value="1" name="datesSet"/>
						<a id="statistics_show_button" href="#"><%=resources.LNG_SHOW %></a>
					</td>
				</tr>
			</table>
			</form>
			</center>    
    </div>
    </form>
</body>
</html>
