﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Parameters;


namespace DeskAlertsDotNet.Pages
{
    public partial class WidgetOverallStat : DeskAlertsBasePage
    {
        //use curUser to access to current user of DA control panel      
        //use dbMgr to access to database

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            string queryString = Request.Url.Query;

            if(!string.IsNullOrEmpty(queryString))
            {
                Response.Redirect("WidgetOverallStatPie.aspx?" + queryString, true);
            }

            //Add your main code here
        }
    }
}