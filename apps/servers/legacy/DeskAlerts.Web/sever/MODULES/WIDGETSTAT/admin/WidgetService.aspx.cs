﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Parameters;
using System.Web.Services;
using DeskAlertsDotNet.Managers;

namespace DeskAlertsDotNet.Pages
{
    public partial class WidgetService : DeskAlertsBasePage
    {
        //use curUser to access to current user of DA control panel      
        //use dbMgr to access to database

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            //Add your main code here
        }

        [WebMethod]
        public static void AddWidget(string  instanceId, int userId, string widgetId, string parameters, string page)
        {
            UserManager.Default.AddWidgetForUser(instanceId, userId, widgetId, parameters, page);
        }

        [WebMethod]
        public static void RemoveWidget(string widgetId)
        {
            UserManager.Default.RemoveWidgetForUser(widgetId);
        }

         [WebMethod]
        public static void UpdateWidget(string widgetId, string parameters)
        {
            UserManager.Default.UpdateWidgetParameters(widgetId.ToUpper(), parameters);
        }
    }
}