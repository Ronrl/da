﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WidgetInstants.aspx.cs" Inherits="DeskAlertsDotNet.Pages.WidgetInstants" %>
<%@ Import NameSpace="DeskAlertsDotNet.Parameters" %>
<%@ Import NameSpace="Resources" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<link href="css/style9.css" rel="stylesheet" type="text/css">
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/preview.js?v=9.2.1.31"></script>
<script language="javascript" type="text/javascript">
    $(function() {
        //$("#im_tiles").sortable();
        $("#im_tiles").disableSelection();
        $(".hover").hover(function() {
            $(this).switchClass("", "im_tile_hovered", 200);
        }, function() {
            $(this).switchClass("im_tile_hovered", "", 200);
        });
		
        if (detectIE()!=false){
            $("#giveMeHeight").css("height",190);
        }
        else{
            $("#giveMeHeight").css("height",210);
        }
    });
	
    function detectIE() {
        var ua = window.navigator.userAgent;
        var msie = ua.indexOf('MSIE ');
        var trident = ua.indexOf('Trident/');

        if (msie > 0) {
            // IE 10 or older => return version number
            return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
        }

        if (trident > 0) {
            // IE 11 (or newer) => return version number
            var rv = ua.indexOf('rv:');
            return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
        }

        // other browser
        return false;
    }


    function darkOrLight(color) {
        red = parseInt(color.substring(0,2),16);
        green = parseInt(color.substring(3,5),16);
        blue = parseInt(color.substring(5),16);
        var brightness;
        brightness = (red * 299) + (green * 587) + (blue * 114);
        brightness = brightness / 255000;

        // values range from 0 to 1
        // anything greater than 0.5 should be bright enough for dark text
        if (brightness >= 0.5) {
            return "black";
        } else {
            return "white";
        }
    }

    function DeleteAlert(id) {

        $.ajax({
            type: "POST",
            url: "InstantMessages.aspx/Delete",
            data: '{id:' + id + '}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                location.reload();
                   
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }

    function Send(id) {

        $.ajax({
            type: "POST",
            url: "InstantMessages.aspx/Send",
            data: '{id:' + id + ', buildCache:1}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                parent.location.href = "PopupSent.aspx?imSent=1";

            },
            error: function (response) {
               // toastr.info("DeskAlerts server is not responding. Plea se contact your system administrator.");
            }
        });
    }


    function showAlertPreview(alertId) {
        var data = new Object();

        getAlertData(alertId, false, function(alertData) {
            data = JSON.parse(alertData);
        });

        var fullscreen = parseInt(data.fullscreen);
        var ticker = parseInt(data.ticker);
        var acknowledgement = data.acknowledgement;

        var alert_width = (fullscreen == 1) && !ticker ? $(".main_table_body").width() : (!ticker ? data.alert_width : $(".main_table_body").width());
        var alert_height = (fullscreen == 1) && !ticker ? $(window).height() - document.body.clientHeight + $(".main_table_body").height() : (!ticker ? data.alert_height : "");
        var alert_title = data.alert_title;
        var alert_html = data.alert_html;

        var top_template;
        var bottom_template;
        var templateId = data.template_id;

        if (templateId >= 0) {
            getTemplateHTML(templateId, "top", false, function(data) {
                top_template = data;
            });

            getTemplateHTML(templateId, "bottom", false, function(data) {
                bottom_template = data;
            });
        }

        data.top_template = top_template;
        data.bottom_template = bottom_template;
        data.alert_width = alert_width;
        data.alert_height = alert_height;
        data.ticker = ticker;
        data.fullscreen = fullscreen;
        data.acknowledgement = acknowledgement;
        data.alert_title = alert_title;
        data.alert_html = alert_html;
        data.caption_href = data.caption_href;

        initAlertPreview(data);

    }
</script>
<body>
    <form id="form1" runat="server">
    <div>
    <div runat="server" style="padding:10px" id="giveMeHeight">
        <ul runat="server" id="imTiles" style="list-style:none; margin:0px; padding:0px">

         </ul>
            
     </div>
     <div runat="server" id="noAlertsDiv" style="padding:10px"><%=resources.LNG_THERE_ARE_NO_INSTANT_MESSAGES %></div>
    </div>
    </form>
</body>
</html>
