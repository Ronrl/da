﻿using DeskAlerts.Server.Utils;
using DeskAlerts.Server.Utils.Helpers;
using DeskAlertsDotNet.Models.Permission;
using DeskAlertsDotNet.Parameters;
using DeskAlertsDotNet.Utils.Consts;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DeskAlertsDotNet.Pages
{
    using DeskAlerts.ApplicationCore.Entities;
    using Resources;

    public partial class WidgetLastContent : DeskAlertsBaseListPage
    {

        string fromDate = "1/1/1900 00:00:00";
        public string toDate = DateTime.Now.ToString(DateTimeFormat.JavaScript);

        private Dictionary<string, string> screensaversPages = new Dictionary<string, string>
        {
            {"image", "EditScreensaversImage.aspx"},
            {"powerpoint", "EditScreensaverPowerPoint.aspx"},
            {"video","EditScreensaverVideo.aspx"}
        };


        protected override void OnLoad(EventArgs e)
        {


            if (!IsPostBack)
            {
                contentSelector.Items.Add(new ListItem(resources.LNG_ALL, "0"));

                if (curUser.HasPrivilege || curUser.Policy[Policies.ALERTS].CanView)
                    contentSelector.Items.Add(new ListItem(resources.LNG_ALERTS, "1"));

                if (Config.Data.HasModule(Modules.SURVEYS) &&
                    (curUser.HasPrivilege || curUser.Policy[Policies.SURVEYS].CanView))
                    contentSelector.Items.Add(new ListItem(resources.LNG_SURVEYS, "64,128,256"));

                if (Config.Data.HasModule(Modules.WALLPAPER) &&
                    (curUser.HasPrivilege || curUser.Policy[Policies.WALLPAPERS].CanView))
                    contentSelector.Items.Add(new ListItem(resources.LNG_WALLPAPERS, "8"));

                if (Config.Data.HasModule(Modules.LOCKSCREEN) &&
                    (curUser.HasPrivilege || curUser.Policy[Policies.LOCKSCREENS].CanView))
                    contentSelector.Items.Add(new ListItem(resources.LNG_LOCKSCREENS, "4069"));

                if (Config.Data.HasModule(Modules.SCREENSAVER) &&
                    (curUser.HasPrivilege || curUser.Policy[Policies.SCREENSAVERS].CanView))
                    contentSelector.Items.Add(new ListItem(resources.LNG_SCREENSAVERS, "2"));

                if (Config.Data.HasModule(Modules.RSS) &&
                    (curUser.HasPrivilege || curUser.Policy[Policies.RSS].CanView))
                    contentSelector.Items.Add(new ListItem(resources.LNG_RSS, "5"));

                if (!string.IsNullOrEmpty(Request["classes"]))
                    contentSelector.SelectedValue = Request["classes"];
            }

            //   contentSelector.Style.Add(HtmlTextWriterStyle.TextAlign, "right");
            //  contentSelector.Style.Add(HtmlTextWriterStyle.Padding, "10px");
            base.OnLoad(e);


            searchButton.Text = searchButton.Text.Replace("{Search}", resources.LNG_SEARCH);
            typeCell.Text = resources.LNG_TYPE;
            titleCell.Text = resources.LNG_TITLE;
            createDateCell.Text = resources.LNG_CREATION_DATE;
            actionsCell.Text = resources.LNG_ACTIONS;


            int cnt = this.Content.Count < (Offset + Limit) ? this.Content.Count : Offset + Limit;


            for (int i = Offset; i < cnt; i++)
            {
                int id = this.Content.GetInt(i, "id");

                TableRow contentRow = new TableRow();
                string imagePath = "";
                Image imageType = new Image();
                bool isUrgent = this.Content.GetInt(i, "urgent") == 1;
                DateTime toDate = this.Content.GetDateTime(i, "to_date");
                DateTime fromDate = this.Content.GetDateTime(i, "from_date");
                //  bool hasLifetime = Content.GetInt(i, "lifetime") == 1;

                bool isScheduled = (toDate.Year != 1990 && (toDate - fromDate).TotalSeconds % 60 == 0) ||
                                   toDate.Year == 1990;


                // Image imageType = new Image();

                //    bool isUrgent = Content.GetInt(i, "urgent") == 1;
                //                    string imagePath = "";

                if (this.Content.GetInt(i, "ticker") == 1)
                {
                    imagePath = isUrgent ? "images/alert_types/urgent_ticker.png" : "images/alert_types/ticker.png";
                    imageType.Attributes["title"] = isUrgent ? resources.LNG_TICKER_URGENT : resources.LNG_TICKER;
                }
                else if (this.Content.GetInt(i, "fullscreen") == 1 && this.Content.GetInt(i, "class") != 2 
                    && this.Content.GetInt(i, "class") != 8 && this.Content.GetInt(i, "class") != (int)AlertType.LockScreen)
                {
                    imagePath = isUrgent ? "images/alert_types/urgent_fullscreen.png" : "images/alert_types/fullscreen.png";
                    imageType.Attributes["title"] = isUrgent ? resources.LNG_URGENT_FULLSCREEN : resources.LNG_FULLSCREEN;
                }
                else if (this.Content.GetString(i, "type") == "L")
                {
                    imagePath = "images/alert_types/linkedin.png";
                    imageType.Attributes["title"] = resources.LNG_ADD_LINKEDIN;
                }
                else if (!this.Content.IsNull(i, "is_rsvp"))
                {
                    imagePath = isUrgent ? "images/alert_types/urgent_rsvp.png" : "images/alert_types/rsvp.png";
                    imageType.Attributes["title"] = isUrgent ? resources.LNG_URGENT_RSVP : resources.LNG_RSVP;
                }
                else
                {

                    imagePath = "images/alert_types/new/" + this.Content.GetString(i, "class") + ".png";
                    string fullPath = Config.Data.GetString("alertsFolder") + "/admin/" + imagePath;
                    // Response.Write(fullPath);

                    if (!System.IO.File.Exists(fullPath))
                    {
                        imagePath = "images/alert_types/alert.png";

                    }
                    imageType.Attributes["title"] = DeskAlertsUtils.GetTooltipForClass(this.Content.GetInt(i, "class"));
                }

                imageType.ImageUrl = imagePath;
                TableCell typeImgCell = new TableCell();
                typeImgCell.Controls.Add(imageType);
                typeImgCell.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
                contentRow.Cells.Add(typeImgCell);


                TableCell titleContentCell = new TableCell();

                string title = this.Content.GetString(i, "title");

                if (this.Content.GetInt(i, "class") == 1)
                {
                    title = "<a href=\"javascript: showAlertPreview('" + id + "')\">" + title + "</a>";
                }

                titleContentCell.Text = title;

                contentRow.Cells.Add(titleContentCell);
                contentRow.Cells.Add(new TableCell
                {
                    Text = DateTimeConverter.ToUiDateTime(Content.GetDateTime(i, "sent_date"))
                });

                TableCell actionsContentCell = new TableCell();

                actionsContentCell.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
                string type = this.Content.GetString(i, "type");
                int alertClass = this.Content.GetInt(i, "class");

                string actionsHtml = "";
                switch (alertClass)
                {
                    case 1:
                    case 2048:
                        {
                            if (curUser.HasPrivilege || curUser.Policy[Policies.ALERTS].CanEverything ||
                                                     curUser.Policy[Policies.ALERTS].CanCreate)
                            {
                                actionsHtml += "<a target=\"_parent\" href=\"CreateAlert.aspx?id=" + id +
                                               "\"><img src=\"images/action_icons/duplicate.png\" alt=\"" +
                                               resources.LNG_RESEND_ALERT + "\" title=\"" +
                                               resources.LNG_RESEND_ALERT +
                                               "\" width=\"16\" height=\"16\" border=\"0\" hspace=5></a>";
                            }


                            actionsHtml += "<a href='javascript: showAlertPreview(\"" + id +
                                           "\")'><img src='images/action_icons/preview.png' alt='" +
                                           resources.LNG_PREVIEW + "' title='" +
                                           resources.LNG_PREVIEW +
                                           "' width='16' height='16' border='0' hspace='5'/></a>";

                            actionsHtml += "<a href=\"#\" onclick=\"openDirDialog('" + id +
                                           "')\"><img src=\"images/action_icons/link.png\" alt=\"" +
                                           resources.LNG_DIRECT_LINK +
                                           "\" title=\"" + resources.LNG_DIRECT_LINK + "\" width=\"16\" height=\"16\" border=\"0\" hspace=\"6\"/></a>";
                            if (Config.Data.HasModule(Modules.STATISTICS))
                            {

                                actionsHtml +=
                                    "<a href='#'  onclick='openDialogUI(\"form\",\"frame\",\"AlertStatisticDetails.aspx?id="
                                    + id + "\",600,350,\"" + resources.LNG_ALERT_DETAILS
                                    + "\");'><img src=\"images/action_icons/graph.gif\" alt=\""
                                    + resources.LNG_STATISTICS + "\" title=\""
                                    + resources.LNG_STATISTICS
                                    + "\" width=\"16\" height=\"16\" border=\"0\" hspace=\"5\"/></a>";
                            }

                            if (isScheduled)
                            {
                                actionsHtml +=
                                    "<a href=\"#\" onclick=\"openDialogUI('form','frame','rescheduler.aspx?id=" + id +
                                    "',600,312,'" + resources.LNG_RESCHEDULE_ALERT +
                                    "');\"><img src=\"images/action_icons/schedule.png\" alt=\"" +
                                    resources.LNG_RESCHEDULE_ALERT + "\" title=\"" +
                                    resources.LNG_RESCHEDULE_ALERT +
                                    "\" border=\"0\" hspace=5></a>";
                            }

                            break;
                        }
                    case 2:
                        {
                            if (curUser.HasPrivilege || curUser.Policy[Policies.SCREENSAVERS].CanEverything ||
                                curUser.Policy[Policies.SCREENSAVERS].CanCreate)
                            {


                                string ssType = "";

                                string regexPattern = "<!--.*?screensaver_type=\"(.*)\".*?file=\"(.*)\".*?-->";

                                MatchCollection mc = Regex.Matches(this.Content.GetString(i, "alert_text"), regexPattern);

                                foreach (Match m in mc)
                                    ssType = m.Groups[1].Value;



                                string dupPage = !screensaversPages.ContainsKey(ssType) ? "EditScreensaversHtml.aspx" : screensaversPages[ssType];


                                actionsHtml += "<a target=\"_parent\" href=\"" + dupPage + "?type=" + ssType + "&id=" + id +
                                               "\"><img src=\"images/action_icons/duplicate.png\" alt=\"" +
                                               resources.LNG_RESEND_SCREENSAVER + "\" title=\"" +
                                               resources.LNG_RESEND_SCREENSAVER +
                                               "\" width=\"16\" height=\"16\" border=\"0\" hspace=5></a>";
                            }

                            actionsHtml += "<a href='#' onclick=\"javascript: showScreenSaverPreview(" + id +
                                           ")\"><img src='images/action_icons/preview.png' alt=\"" +
                                           resources.LNG_PREVIEW + "\" title=\"" +
                                           resources.LNG_PREVIEW +
                                           "\" width=\"16\" height=\"16\" border=\"0\" hspace=\"5\"/></a>";

                            break;
                        }
                    case 8:
                        {


                            if (curUser.IsAdmin || curUser.Policy[Policies.WALLPAPERS].CanEverything ||
                                curUser.Policy[Policies.WALLPAPERS].CanCreate)
                            {
                                actionsHtml += "<a target=\"_parent\" href=\"EditWallpaper.aspx?id=" + id +
                                               "\"><img src=\"images/action_icons/duplicate.png\" alt=\"" +
                                               resources.LNG_RESEND_WALLPAPER + "\" title=\"" +
                                               resources.LNG_RESEND_WALLPAPER +
                                               "\" width=\"16\" height=\"16\" border=\"0\" hspace=5></a>";
                            }

                            actionsHtml += "<a href='#' onclick=\"javascript: showWallpaperPreview(" + id +
                                           ")\"><img src='images/action_icons/preview.png' alt=\"" +
                                           resources.LNG_PREVIEW + "\" title=\"" +
                                           resources.LNG_PREVIEW +
                                           "\" width=\"16\" height=\"16\" border=\"0\" hspace=\"5\"/></a>";
                            break;
                        }
                    case (int)AlertType.LockScreen:
                        {


                            if (curUser.IsAdmin || curUser.Policy[Policies.LOCKSCREENS].CanEverything ||
                                curUser.Policy[Policies.LOCKSCREENS].CanCreate)
                            {
                                actionsHtml += "<a target=\"_parent\" href=\"EditLockscreen.aspx?id=" + id +
                                               "\"><img src=\"images/action_icons/duplicate.png\" alt=\"" +
                                               resources.LNG_RESEND_LOCKSCREEN + "\" title=\"" +
                                               resources.LNG_RESEND_LOCKSCREEN +
                                               "\" width=\"16\" height=\"16\" border=\"0\" hspace=5></a>";
                            }

                            actionsHtml += "<a href='#' onclick=\"javascript: showWallpaperPreview(" + id +
                                           ")\"><img src='images/action_icons/preview.png' alt=\"" +
                                           resources.LNG_PREVIEW + "\" title=\"" +
                                           resources.LNG_PREVIEW +
                                           "\" width=\"16\" height=\"16\" border=\"0\" hspace=\"5\"/></a>";
                            break;
                        }
                    case 64:
                    case 128:
                    case 256:
                        {
                            object surveyIdObj =
                                dbMgr.GetScalarByQuery("SELECT id FROM surveys_main WHERE sender_id = " + id);

                            string surveyId = "";
                            if (surveyIdObj != null)
                                surveyId = surveyIdObj.ToString();


                            actionsHtml += "<a target=\"_parent\"href=\"EditSurvey.aspx?id=" + surveyId +
                                           "\"><img src=\"images/action_icons/duplicate.png\" alt=\"" +
                                           resources.LNG_RESEND_SURVEY + "\" title=\"" +
                                           resources.LNG_RESEND_SURVEY +
                                           "\" width=\"16\" height=\"16\" border=\"0\" hspace=5></a>";

                            actionsHtml += "<a href='javascript: showSurveyPreview(\"" + surveyId +
                                           "\")'><img src='images/action_icons/preview.png' alt='" +
                                           resources.LNG_PREVIEW + "' title='" +
                                           resources.LNG_PREVIEW +
                                           "' width='16' height='16' border='0' hspace='5'/></a>";

                            if ((curUser.IsAdmin || curUser.Policy[Policies.SURVEYS].CanEverything ||
                                curUser.Policy[Policies.SURVEYS].CanView) && Config.Data.HasModule(Modules.STATISTICS))
                            {
                                actionsHtml +=
                                    "<a href='#'  onclick='openDialogUI(\"form\",\"frame\",\"SurveyAnswersStatistic.aspx?id=" +
                                    surveyId + "\",555,350,\"\");'><img src=\"images/action_icons/graph.png\" alt=\"" +
                                    resources.LNG_STATISTICS + "\" title=\"" +
                                    resources.LNG_VIEW_SURVEY_RESULTS +
                                    "\" width=\"16\" height=\"16\" border=\"0\" hspace=\"5\"/></a>";

                            }
                            break;
                        }
                    case 4:
                        {
                            if (curUser.IsAdmin || curUser.Policy[Policies.RSS].CanEverything ||
                                curUser.Policy[Policies.RSS].CanCreate)
                            {
                                actionsHtml += "<a target=\"_parent\" href=\"EditRss.aspx?id=" + id +
                                               "\"><img src=\"images/action_icons/duplicate.png\" alt=\"" +
                                               resources.LNG_RESEND_RSS + "\" title=\"" +
                                               resources.LNG_RESEND_RSS +
                                               "\" width=\"16\" height=\"16\" border=\"0\" hspace=5></a>";
                            }
                            break;
                        }
                    case 5:
                        {
                            actionsHtml += "<a href='javascript: showVideoAlertPreview(\"" + id +
                                           "\")'><img src='images/action_icons/preview.png' alt='" +
                                           resources.LNG_PREVIEW + "' title='" +
                                           resources.LNG_PREVIEW +
                                           "' width='16' height='16' border='0' hspace='5'/></a>";

                            break;
                        }
                    case 32:
                        {


                            if (curUser.HasPrivilege || curUser.Policy[Policies.ALERTS].CanEverything ||
                                curUser.Policy[Policies.ALERTS].CanCreate)
                            {
                                actionsHtml += "<a target=\"_parent\" href=\"EditVideo.aspx?id=" + id +
                                               "\"><img src=\"images/action_icons/duplicate.png\" alt=\"" +
                                               resources.LNG_RESEND_ALERT + "\" title=\"" +
                                               resources.LNG_RESEND_ALERT +
                                               "\" width=\"16\" height=\"16\" border=\"0\" hspace=5></a>";
                            }

                            actionsHtml += "<a href='javascript: showVideoAlertPreview(\"" + id +
                                           "\")'><img src='images/action_icons/preview.png' alt='" +
                                           resources.LNG_PREVIEW + "' title='" +
                                           resources.LNG_PREVIEW +
                                           "' width='16' height='16' border='0' hspace='5'/></a>";


                            actionsHtml += "<a href=\"#\" onclick=\"openDirDialog('" + id +
                                       "')\"><img src=\"images/action_icons/link.png\" alt=\"" +
                                       resources.LNG_DIRECT_LINK +
                                       "\" title=\"" + resources.LNG_DIRECT_LINK + "\" width=\"16\" height=\"16\" border=\"0\" hspace=\"6\"/></a>";



                            if (Config.Data.HasModule(Modules.STATISTICS))
                            {

                                actionsHtml +=
                                    "<a href='#'  onclick='openDialogUI(\"form\",\"frame\",\"AlertStatisticDetails.aspx?id="
                                    + id + "\",600,350,\"" + resources.LNG_ALERT_DETAILS
                                    + "\");'><img src=\"images/action_icons/graph.gif\" alt=\""
                                    + resources.LNG_STATISTICS + "\" title=\""
                                    + resources.LNG_STATISTICS
                                    + "\" width=\"16\" height=\"16\" border=\"0\" hspace=\"5\"/></a>";
                            }

                            if (isScheduled)
                            {
                                actionsHtml +=
                                    "<a href=\"#\" onclick=\"openDialogUI('form','frame','rescheduler.aspx?id=" + id +
                                    "',600,312,'" + resources.LNG_RESCHEDULE_ALERT +
                                    "');\"><img src=\"images/action_icons/schedule.png\" alt=\"" +
                                    resources.LNG_RESCHEDULE_ALERT + "\" title=\"" +
                                    resources.LNG_RESCHEDULE_ALERT +
                                    "\" border=\"0\" hspace=5></a>";
                            }

                            break;
                        }


                }

                actionsContentCell.Text = actionsHtml;
                //  actionsCell.Align = "center";
                contentRow.Cells.Add(actionsContentCell);

                contentTable.Rows.Add(contentRow);

            }


        }


        protected override string PageParams
        {
            get
            {
                if (IsPostBack)
                    return "classes=" + contentSelector.SelectedValue + "&";

                if (!string.IsNullOrEmpty(Request["classes"]))
                    return "classes=" + Request["classes"] + "&";

                return "";
            }
        }

        #region DataProperties
        protected override string DataTable
        {
            get
            {
                return "";
                //return your table name from this property
                //throw new NotImplementedException();
            }
        }

        protected override string[] Collumns
        {
            get
            {

                return null;
                //return array of collumn`s names of table from data table
                //throw new NotImplementedException();

            }
        }

        protected override string DefaultOrderCollumn => "create_date";

        /*
         * 
         * If you want to add condition for data selection override property ConditionSqlString 
         * Also you can implement your custom query by overriding properties CustomQuery and CustomCountQuery
         *  
         */

        protected override string CustomCountQuery
        {
            get
            {



                string sqlQuery = "";

                string classesList = contentSelector.SelectedValue;

                if (!string.IsNullOrEmpty(Request["classes"]))
                    classesList = Request["classes"];

                if (!curUser.HasPrivilege && !curUser.Policy.CanViewAll)
                {
                    string viewListStr = string.Join(",", curUser.Policy.SendersViewList.ToArray());
                    sqlQuery = "SELECT COUNT(1) FROM alerts a LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id WHERE (a.type='S' OR a.type='L') AND a.create_date >= '" + fromDate + "' AND a.create_date <= GETDATE() AND param_temp_id IS NULL AND a.sender_id IN (" + viewListStr + ") AND a.id NOT IN (SELECT alert_id FROM instant_messages) ";

                }
                else
                    sqlQuery = "SELECT COUNT(1) FROM alerts a LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id WHERE (a.type='S' OR a.type='L') AND a.create_date >= '" + fromDate + "' AND a.create_date <= GETDATE() AND param_temp_id IS NULL AND a.id NOT IN (SELECT alert_id FROM instant_messages) ";

                if (!contentSelector.SelectedValue.Equals("0"))
                    sqlQuery += "AND [class] IN (" + classesList + ")";

                if (SearchTerm.Length > 0)
                    sqlQuery += " AND a.title LIKE '%" + SearchTerm + "%'";
                return sqlQuery;
            }
        }

        protected override string CustomQuery
        {
            get
            {

                string classesList = contentSelector.SelectedValue;

                if (!string.IsNullOrEmpty(Request["classes"]) && !IsPostBack)
                    classesList = Request["classes"];

                string sqlQuery = "";

                if (!curUser.HasPrivilege && !curUser.Policy.CanViewAll)
                {
                    string viewListStr = string.Join(",", curUser.Policy.SendersViewList.ToArray());
                    sqlQuery = "SELECT a.id, a.schedule, schedule_type, a.type2, recurrence, alert_text, title, a.type, sent_date, a.create_date as create_date, a.from_date, a.to_date, class, urgent, ticker, fullscreen, s.id as is_rsvp FROM alerts a LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id WHERE (a.type='S' OR a.type='L') AND a.create_date >= '" + fromDate + "' AND a.create_date <= GETDATE() AND param_temp_id IS NULL AND a.sender_id IN (" + viewListStr + ") AND a.id NOT IN (SELECT alert_id FROM instant_messages) ";

                }
                else
                    sqlQuery = "SELECT a.id, a.schedule, schedule_type, a.type2, recurrence, alert_text, title, a.type, sent_date, a.create_date as create_date, a.from_date, a.to_date, class, urgent, ticker, fullscreen, s.id as is_rsvp FROM alerts a LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id WHERE (a.type='S' OR a.type='L') AND a.create_date >= '" + fromDate + "' AND a.create_date <= GETDATE() AND param_temp_id IS NULL AND a.id NOT IN (SELECT alert_id FROM instant_messages) ";

                if (!classesList.Equals("0"))
                    sqlQuery += "AND [class] IN (" + classesList + ")";


                if (SearchTerm.Length > 0)
                    sqlQuery += " AND title LIKE '%" + SearchTerm + "%'";

                sqlQuery += " ORDER BY create_date DESC";

                return sqlQuery;

            }
        }
        #endregion

        #region Interface properties

        protected override IButtonControl[] DeleteButtons
        {
            get
            {
                return new IButtonControl[] { };
            }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl NoRowsDiv
        {
            get { return NoRows; }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl TableDiv
        {
            get { return mainTableDiv; }
        }

        protected override string ContentName
        {
            get
            {
                //return content name here
                return "";
            }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl BottomPaging
        {
            get
            {
                return bottomRanging;
            }
        }

        #endregion
    }
}