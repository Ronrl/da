﻿using System;     
using DeskAlertsDotNet.Models.Permission;
using DeskAlertsDotNet.Pages;
using DeskAlertsDotNet.Parameters;

namespace DeskAlertsDotNet.sever.MODULES.WIDGETSTAT.admin
{
    using Resources;

    public partial class AddWidget : DeskAlertsBasePage
    {
        private const string WidgetTemplate = @"<div style='margin:10px;float:left;width:219px;height:219px;position:relative'>
		                                <div class='widget-tile'>
			                                <img src='widgets/%WIDGET_FOLDER%/preview.png' width='219' height='219'>
			                                <div class='widget_hint' style='display: block;'>
			                                %DESCRIPTION%
			                                </div>
		                                </div>
		
		                                <div style='position:relative;text-align:left;background-color:rgba(0,0,0,0.5);z-index:999;bottom:32px;height:32px;'>
			                                <div style='float:left;width:56%;padding-top:7px;text-align:left;color:white;padding-left:7px;cursor:default;white-space:nowrap;overflow:hidden' title='%TITLE%'>%TITLE%</div>
			                                <div style='float:left;width:39%;padding-top:3px;'>
				                                <a href='#' class='add_this ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only' id='%ID%' custom_title='%TITLE%' custom_link='%LINK%' style='width:100%' role='button' aria-disabled='false'><span class='ui-button-text'>%LNG_ADD%</span></a>
			                                </div>
		                                </div>
	                                </div>";


        private readonly string[] _nonStatWidgets =
        {
            "Create Alert", "Instant Send", "Last Alerts"
        };

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            var widgetSet = dbMgr.GetDataByQuery("SELECT id, title, href FROM widgets ORDER BY priority");

            var isStat = Request.UrlReferrer != null && Request.UrlReferrer.AbsoluteUri.Contains("stat=1");

            foreach(var widgetRow in widgetSet)
            {

                var widgetTitle = widgetRow.GetString("title");
                if (isStat && Array.IndexOf(_nonStatWidgets, widgetTitle) >= 0)
                    continue;

                switch (widgetTitle)
                {
                    case "Instant Send" when !curUser.HasPrivilege && !curUser.Policy[Policies.IM].CanCreate:
                        continue;
                    case "Survey Details" when !curUser.HasPrivilege && !curUser.Policy[Policies.SURVEYS].CanCreate:
                        continue;
                    case "Survey Answers" when !curUser.HasPrivilege && !curUser.Policy[Policies.SURVEYS].CanCreate:
                        continue;
                }


                if (!curUser.HasPrivilege && !curUser.Policy[Policies.ALERTS].CanCreate && widgetTitle == "Create Alert")
                continue;
                
                addWidgetForm.InnerHtml += BuildWidgetHtml(widgetRow.GetString("id"), widgetTitle, widgetRow.GetString("href"));
            }

        }

        private static string BuildWidgetHtml(string id, string title, string href)
        {
            var descKey =  "LNG_WIDGET_HINT_" + title.Replace(" ", "_").ToUpper();
            var description = resources.ResourceManager.GetString(descKey);
            var localizaedTitleKey = "LNG_WIDGET_" + title.Replace(" ", "_").ToUpper();
            var localizedTitle = resources.ResourceManager.GetString(localizaedTitleKey);
            var folder = href.Replace("widget_", "").Replace(".aspx", "").Replace(".asp", "").Replace(".html", "");
            var widgetHtml = WidgetTemplate.Replace("%TITLE%", localizedTitle)
                .Replace("%LINK%", href).Replace("%DESCRIPTION%", description)
                .Replace("%LNG_ADD%", resources.LNG_ADD)
                .Replace("%ID%", id).Replace("%WIDGET_FOLDER%", folder);


            return widgetHtml;
        }
    }
}