﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WidgetUsersStats.aspx.cs" Inherits="DeskAlertsDotNet.Pages.WidgetUsersStats" %>

<%@ Import Namespace="DeskAlertsDotNet.Parameters" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>DeskAlerts</title>
    <link href="css/style9.css" rel="stylesheet" type="text/css">
    <link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>

    <!--[if lt IE 10]><script language="javascript" type="text/javascript" src="jscripts/excanvas.min.js"></script><![endif]-->
    <script language="javascript" type="text/javascript" src="jscripts/jquery.jqplot.min.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/jqplot.highlighter.min.js"></script>
    <script type="text/javascript" src="jscripts/jqplot.pieRenderer.min.js"></script>
    <script type="text/javascript" src="jscripts/jqplot.barRenderer.min.js"></script>
    <script type="text/javascript" src="jscripts/jqplot.categoryAxisRenderer.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/jquery.jqplot.css" />

    <script language="javascript" type="text/javascript">
        $(document).ready(function () {


            var data = [<%= valuesJson%>];

            var usersPlot = jQuery.jqplot('usersChart', [data],
                {
                    grid: {
                        background: '#ffffff',
                        renderer: $.jqplot.CanvasGridRenderer,
                        drawBorder: false,
                        shadow: false
                    },
                    seriesColors: [<%= colorsJson%>],
                    seriesDefaults: {
                        shadow: true,
                        shadowAlpha: 0.1,
                        renderer: jQuery.jqplot.PieRenderer,
                        rendererOptions: {
                            diameter: 220,
                            padding: 25,
                            highlightMouseOver: false
                        }
                    },
                    legend: {
                        show: false,
                        location: "e"
                    },
                    highlighter: {
                        useAxesFormatters: false,
                        show: true,
                        tooltipFormatString: '%s',
                        fadeTooltip: false
                    }
                }
            );
        });
    </script>
    <style type="text/css">
        .jqplot-highlighter-tooltip {
            font-size: 12px
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <center>
                <asp:Label runat="server" ID="messageLabel"></asp:Label>
                <div id='usersChart' style='width:300px;height:300px'></div>
                <table border=0 class='chartLegend' runat="server" id="legend">
                    <tr><td><div runat="server"  id="onlineDiv" class="legend" ></div></td><td runat="server" id="onlineName"></td><td runat="server" id="onlineValue"></td></tr>
                    <tr><td><div runat="server" id="offlineDiv" class="legend" ></div></td><td runat="server" id="offlineName"></td><td runat="server" id="offlineValue"></td></tr>
                    <tr><td><div runat="server" id="disabledDiv" class="legend"></div></td><td runat="server" id="disabledName"></td><td runat="server" id="disabledValue"></td></tr>
                </table>
            </center>
        </div>
    </form>
</body>
</html>
