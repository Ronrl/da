<!doctype html>
<html>
<head>
	<title>DeskAlerts</title>
	<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
	<link href="css/style9.css?v=9.1.12.29" rel="stylesheet" type="text/css"/>
	<link href="css/jquery.jqplot.css" rel="stylesheet" type="text/css"/>
	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
	<script language="javascript" type="text/javascript" src="jscripts/jquery.jqplot.min.js"></script>
	<script language="javascript" type="text/javascript" src="jscripts/jqplot.dateAxisRenderer.min.js"></script>
	
	<script language="javascript" type="text/javascript">
	    $(document).ready(function() {
	        var t = 1000;
	        var n = 61;
	        var x = (new Date()).getTime();
	        var data = [];
	        for (i = 0; i < n; i++) {
	            data.push([x - (n - 1 - i) * t, 0]);
	        }

	        var options = {
	            axes: {
	                xaxis: {
	                    numberTicks: 4,
	                    renderer: $.jqplot.DateAxisRenderer,
	                    tickOptions: { formatString: '%H:%M:%S' },
	                    min: data[0][0],
	                    max: data[data.length - 1][0]
	                },
	                yaxis: { min: 0, max: 1, numberTicks: 2,
	                    tickOptions: { formatString: '%d' }
	                }
	            },
	            seriesDefaults: {
	                rendererOptions: { smooth: true },
	                color: "#5cb85c",
	                showMarker: false
	            }
	        };
	        var plot1 = $.jqplot('myChart', [data], options);
	        setInterval(doUpdate, t);

	        function doUpdate() {
	            $.get("alerts_received_realtime.asp")
				.done(function(response_data) {
				    if (data.length > n - 1) {
				        data.shift();
				    }
				    var y = parseInt(response_data); //ajax here
				    var x = (new Date()).getTime();

				    data.push([x, y]);
				    if (plot1) {
				        plot1.destroy();
				    }
				    var max = 0, min = 0;
				    for (i = 0; i < data.length; i++) {
				        if (data[i][1] > max) { max = data[i][1] }
				        else {
				            if (data[i][1] < min) { min = data[i][1] };
				        }
				    }
				    if (max == 0) { max = 1; }
				    plot1.series[0].data = data;
				    var ticks = 10;
				    if (max < 10) { ticks = max + 1; }
				    options.axes.xaxis.min = data[0][0];
				    options.axes.xaxis.max = data[data.length - 1][0];
				    options.axes.yaxis.min = min;
				    options.axes.yaxis.max = max;
				    options.axes.yaxis.numberTicks = ticks;
				    plot1 = $.jqplot('myChart', [data], options);

				    var isZero = true;



				    $(data).each(function(i) {
				        if (data[i][1] != 0) {
				            isZero = false;
				            return false;
				        }
				    });


				    if (isZero) $('#isZero').css('display', 'block');
				    else $('#isZero').css('display', 'none');

				});
	        }
	    });
	</script>
</head>
<body>
<center>
    <div id="isZero"><%=LNG_NO_ACTIVITY_AT_THE_MOMENT %></div>
	
	<div id="myChart" style="height:146px; width:450px;"></div>
</center>
</body>
</html>