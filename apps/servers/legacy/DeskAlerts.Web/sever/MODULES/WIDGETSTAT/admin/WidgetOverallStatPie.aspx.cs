﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Parameters;
using System.Globalization;
using DeskAlertsDotNet.Utils.Consts;

namespace DeskAlertsDotNet.Pages
{
    using Resources;

    public partial class WidgetOverallStatPie : DeskAlertsBasePage
    {
        //use curUser to access to current user of DA control panel      
        //use dbMgr to access to database
        public string alertsColors = "";
        public string alertsValues = "";
        public bool drawAlerts = true;
        public string hintText = "";
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            //Add your main code here

            string startDate = Request["start_date"];
            string endDate = Request["end_date"];

            int option = Convert.ToInt32(Request["select_date_radio"]);
            string recievedCountQuery ="";
            string notRecievedCountQuery = "";
            string aknownCountQuery = "";


            if(option == 1)
            {
                DateTime toDate = DateTime.MaxValue;
                DateTime fromDate = DateTime.MinValue;

                int range = Convert.ToInt32(Request["range"]);

                switch(range)
                {
                    case 1:
                        {
                            toDate = DateTime.Now.AddDays(1);
                            fromDate = DateTime.Now.AddDays(-1);
                            toDate = new DateTime(toDate.Year, toDate.Month, toDate.Day, 0, 0, 0);
                            fromDate = new DateTime(fromDate.Year, fromDate.Month, fromDate.Day, 0, 0, 0);
                            hintText = resources.LNG_TODAY;
                            break;
                        }

                    case 2:
                        {
                            toDate = DateTime.Now.AddDays(-1);
                            fromDate = DateTime.Now.AddDays(-2);
                            toDate = new DateTime(toDate.Year, toDate.Month, toDate.Day, 23, 59, 59);
                            fromDate = new DateTime(fromDate.Year, fromDate.Month, fromDate.Day, 23, 59, 59);
                            hintText = resources.LNG_YESTERDAY;
                            break;
                        }
                    case 3:
                        {
                            toDate = DateTime.Now.AddDays(1);
                            fromDate = DateTime.Now.AddDays(-6);
                            toDate = new DateTime(toDate.Year, toDate.Month, toDate.Day, 0, 0, 0);
                            fromDate = new DateTime(fromDate.Year, fromDate.Month, fromDate.Day, 23, 59, 59);
                            hintText = resources.LNG_LAST_DAYS;
                            break;
                        }
                    case 4:
                        {
                            fromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                            toDate = fromDate.AddMonths(1).AddDays(-1);
                            hintText = resources.LNG_THIS_MONTH;
                            break;
                        }
                    case 5:
                        {
                            DateTime lastMonthDate = DateTime.Now.AddMonths(-1);
                            fromDate = new DateTime(lastMonthDate.Year, lastMonthDate.Month, 1);
                            toDate = fromDate.AddMonths(1).AddDays(-1);
                            hintText = resources.LNG_LAST_MONTH;
                            break;
                        }
                    default:
                        {
                            toDate = DateTime.MaxValue;
                            fromDate = new DateTime(1753, 1, 1);
                            hintText = resources.LNG_ALL_TIME;
                            break;
                        }
                }

                startDate = fromDate.ToString(DateTimeFormat.JavaScript, CultureInfo.InvariantCulture);
                endDate = toDate.ToString(DateTimeFormat.JavaScript, CultureInfo.InvariantCulture);
            }
            else if (option == 2)
            {

                if (string.IsNullOrEmpty(startDate) || string.IsNullOrEmpty(endDate))
                    return;


                hintText = resources.LNG_FROM + " " + startDate + " " + resources.LNG_TO + " " + endDate;
            }
           
            dbMgr.ExecuteQuery("SET LANGUAGE BRITISH");
            recievedCountQuery = string.Format("SELECT count(1) FROM alerts WHERE sent_date >= '{0}' AND sent_date <= '{1}' AND id IN (SELECT  id FROM alerts_received)", startDate, endDate);
            notRecievedCountQuery = string.Format("SELECT count(1) FROM alerts WHERE sent_date >= '{0}' AND sent_date <= '{1}' AND id NOT IN (SELECT  id FROM alerts_received)", startDate, endDate);
            aknownCountQuery = string.Format("SELECT count(1) FROM alerts WHERE sent_date >= '{0}' AND sent_date <= '{1}' AND aknown = 1", startDate, endDate);

            int recievedCount = Convert.ToInt32(dbMgr.GetScalarByQuery(recievedCountQuery));
            int notRecievedCount = Convert.ToInt32(dbMgr.GetScalarByQuery(notRecievedCountQuery));
            int aknownCount = Convert.ToInt32(dbMgr.GetScalarByQuery(aknownCountQuery));

            string recievedKey = resources.LNG_RECEIVED;
            string notRecievedKey = resources.LNG_NOT_RECEIVED;
            string aknownKey = resources.LNG_ACKNOWLEDGED;

            string recievedColor = "'#3B6392'";
            string notRecievedColor = "'#9ABA59'";
            string aknownColor = "'#A44441'";
            string valuesJsonTemplate = "['{0}',{1}]";
            
            drawAlerts = recievedCount != 0 || aknownCount != 0 || notRecievedCount != 0;
            if (recievedCount != 0)
            {
                alertsValues += string.Format(valuesJsonTemplate, recievedKey, recievedCount);
                alertsColors = recievedColor;


            }
            receivedDiv.Style.Add(HtmlTextWriterStyle.BackgroundColor, recievedColor.Replace("'", ""));
            receivedName.InnerText = recievedKey;
            receivedValueCell.InnerText = recievedCount.ToString();
           
            if (aknownCount != 0)
            {
                if (alertsValues.Length > 0)
                    alertsValues += ",";

                alertsValues += string.Format(valuesJsonTemplate, aknownKey, aknownCount);

                
                if (alertsColors.Length > 0)
                    alertsColors += ",";

                alertsColors +=  aknownColor;


            }
            acknownDiv.Style.Add(HtmlTextWriterStyle.BackgroundColor, aknownColor.Replace("'", ""));
            acknownName.InnerText = aknownKey;
            acknownValueCell.InnerText = aknownCount.ToString();
           
            if (notRecievedCount != 0)
            {
             
                if (alertsValues.Length > 0)
                    alertsValues += ",";

                

                alertsValues += string.Format(valuesJsonTemplate, notRecievedKey, notRecievedCount);

                if (alertsColors.Length > 0)
                    alertsColors += ",";

                alertsColors += notRecievedColor;

            }
            
            notReceivedDiv.Style.Add(HtmlTextWriterStyle.BackgroundColor, notRecievedColor.Replace("'", ""));
            notReceivedName.InnerText = notRecievedKey;
            notReceivedValueCell.InnerText = notRecievedCount.ToString();
        }
    }
}