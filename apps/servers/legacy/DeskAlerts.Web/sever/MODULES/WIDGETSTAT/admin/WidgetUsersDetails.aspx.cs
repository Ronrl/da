﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DeskAlertsDotNet.Parameters;
using DeskAlertsDotNet.DataBase;

namespace DeskAlertsDotNet.Pages
{
    using Resources;

    public partial class WidgetUsersDetails : DeskAlertsBaseListPage
    {

        protected override void OnLoad(EventArgs e)
        {

            if (!string.IsNullOrEmpty(Request["id"]))
            {
                Response.Redirect("UserStatisticsDetails.aspx?id=" + Request["id"] +
                                  "&widget=1&from_date=01/01/2001%2000:00:00&to_date=11/10/2111%2023:59:59", true);

                return;
            }


            if (!string.IsNullOrEmpty(Request["searchTitle"]))
                searchTitle.Value = Request["searchTitle"];

            if (searchTitle.Value.Trim().Length != 0 || !string.IsNullOrEmpty(Request["offset"]))
            {
                contentTable.Visible = true;
                NoRowsDiv.Visible = true;
                base.OnLoad(e);
                userName.Text = resources.LNG_USERNAME;
                
                int cnt = this.Content.Count < (Offset + Limit) ? this.Content.Count : Offset + Limit;

                for (int i = Offset; i < cnt; i++)
                {
                    DataRow row = this.Content.GetRow(i);
                    TableRow tableRow = new TableRow();

                    TableCell usernameCell = new TableCell();
                    LinkButton link = new LinkButton();

                    link.Text = row.GetString("name");

                    link.Click += delegate
                    {
                        string id = row.GetString("id");

                        ClientScript.RegisterClientScriptBlock(GetType(), "EditSurveyStatId_" + id, string.Format("changeParameters('{0}', '{1}')", id, "id=" + id), true);
                    };

                    usernameCell.Controls.Add(link);
                    usernameCell.Enabled = Config.Data.HasModule(Modules.STATISTICS);
                    
                    tableRow.Cells.Add(usernameCell);

                    contentTable.Rows.Add(tableRow);
                }
            }
            else
            {
                contentTable.Visible = false;
                NoRowsDiv.Visible = false;
            }

            search.Text = search.Text.Replace("{Search}", resources.LNG_SEARCH);
            
        }

        #region DataProperties

        protected override string PageParams
        {
            get
            {
                if (searchTitle.Value.Length > 0)
                    return "search=" + searchTitle.Value + "&";
                
                if(string.IsNullOrEmpty(Request["search"]))
                    return "search=" + Request["search"] + "&";

                return "";

            }
        }

        protected override string CustomCountQuery
        {
            get
            {
                string query = GetFilterForQuery();

                query += @" SELECT COUNT(1) AS cnt
                            FROM #tempUsers";
                return query;
            }
        }


        protected override string DataTable => "";

        protected override string[] Collumns => new string[]{};

        protected override string DefaultOrderCollumn => "id";

        protected override string CustomQuery
        {
            get
            {
                string query = GetFilterForQuery();

                query += @"SELECT t.user_id as id,
                               users.next_request,
                               users.last_standby_request,
                               users.next_standby_request,
                               users.context_id,
                               users.mobile_phone,
                               users.email,
                               users.name AS name,
                               users.display_name,
                               users.standby,
                               users.domain_id,
                               domains.name AS domain_name,
                               users.deskbar_id,
                               users.reg_date,
                               users.last_date,
                               users.last_request AS last_request1,
                               users.last_request,
                               users.client_version,
                               edir_context.name AS context_name
                        FROM #tempUsers t
                        LEFT JOIN users ON t.user_id = users.id
                        LEFT JOIN domains ON domains.id = users.domain_id
                        LEFT JOIN edir_context ON edir_context.id = users.context_id
                        WHERE ROLE='U'";

                if (!String.IsNullOrEmpty(Request["sortBy"]))
                    query += " ORDER BY " + Request["sortBy"];
                else
                    query += " ORDER BY name";

                query += @" DROP TABLE #tempOUs
                        DROP TABLE #tempUsers";


                return query;
            }


        }

        /*
         * 
         * If you want to add condition for data selection override property ConditionSqlString 
         * Also you can implement your custom query by overriding properties CustomQuery and CustomCountQuery
         *  
         */
        #endregion

        #region Interface properties

        protected override IButtonControl[] DeleteButtons
        {
            get
            {
                return new IButtonControl[] { };
            }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl NoRowsDiv
        {
            get { return NoRows; }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl TableDiv
        {
            get { return mainTableDiv; }
        }

        protected override string ContentName
        {
            get
            {
                //return content name here
                return resources.LNG_USERS;
            }
        }

        #endregion

        private string GetFilterForQuery()
        {
            string searchType = Request["type"];

            string searchId = Request["id"];

            string query = @"SET NOCOUNT ON IF OBJECT_ID('tempdb..#tempOUs') IS NOT NULL
                                    DROP TABLE #tempOUs
                                    CREATE TABLE #tempOUs (Id_child BIGINT NOT NULL, Rights BIT NOT NULL);

                                    IF OBJECT_ID('tempdb..#tempUsers') IS NOT NULL
                                    DROP TABLE #tempUsers
                                    CREATE TABLE #tempUsers (user_id BIGINT NOT NULL);

                                    DECLARE @now DATETIME;


                                    SET @now = GETDATE();";

            if (searchType == "ou")
            {
                if (Request["search"] == "1")
                {
                    query += @"WITH OUs (id) AS
                                      (SELECT CAST(" + searchId + @" AS BIGINT)
                                       UNION ALL SELECT Id_child
                                       FROM OU_hierarchy h
                                       INNER JOIN OUs ON OUs.id = h.Id_parent)
                                    INSERT INTO #tempOUs (Id_child, Rights)
                                      (SELECT DISTINCT id,
                                                       " + searchId + @"
                                       FROM OUs) OPTION (MaxRecursion 10000);";
                }
                else
                {
                    query += @" INSERT INTO #tempOUs (Id_child, Rights) VALUES (" + searchId + ", 0)";
                }
            }
            else if (searchType == "DOMAIN")
            {
                query += @" INSERT INTO #tempOUs (Id_child, Rights) (SELECT id, 0 FROM OU WHERE Id_domain = " + searchId + ")";
            }
            else
            {
                query += " INSERT INTO #tempOUs (Id_child, Rights) (SELECT id, 0 FROM OU) ";
            }


            query += @"INSERT INTO #tempUsers (user_id)
                              (SELECT DISTINCT Id_user
                               FROM OU_User u
                               INNER JOIN #tempOUs t ON u.Id_OU=t.Id_child
                               LEFT JOIN users ON u.Id_user = users.id
                               WHERE ROLE='U'";

            if (!String.IsNullOrEmpty(Request["searchTitle"]))
            {
                query += String.Format(" AND (users.name LIKE N'%{0}%' OR users.display_name LIKE N'%{0}%')", Request["searchTitle"].Replace("'", "''"));
            }

            query += ")";

            if (searchType != "ou")
            {
                query += @"	INSERT INTO #tempUsers(user_id)		
                                      (SELECT DISTINCT users.id		
                                       FROM users 		
                                       LEFT JOIN #tempUsers t ON t.user_id=users.id		
                                       WHERE t.user_id IS NULL		
                                         AND users.ROLE='U'";

                if (searchType == "DOMAIN")
                    query += " AND users.domain_id = " + searchId;

                if (searchTitle.Value.Trim().Length > 0)
                {
                    query += String.Format(" AND (users.name LIKE N'%{0}%' OR users.display_name LIKE N'%{0}%')", searchTitle.Value.Replace("'", "''"));
                }

                query += ")";

            }
            return query;
        }
    }
}