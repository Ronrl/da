﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<!-- #include file="Common.asp" -->



<%

check_session()

uid = Session("uid")
if uid <>"" then

    Server.ScriptTimeout = 120
    Conn.CommandTimeout = 120
    mobile_device=Session("isMobile")
    simple_mobile=Session("isSimpleMobile")
   if(Request("day") <> "") then 
	day_num = clng(Request("day"))
   else 
	day_num = 0
   end if
   
   if(Request("tab") <> "") then 
	tab = Request("tab")
   else 
	tab = 0
   end if
   
   
   if(Request("offset") <> "") then 
	offset = clng(Request("offset"))
   else 
	offset = 0
   end if	

   if(Request("sortby") <> "") then 
	sortby = Request("sortby")
   else 
	sortby = "create_date DESC"
   end if	

   if(Request("limit") <> "") then 
	limit = clng(Request("limit"))
   else 
	limit=10
   end if


   
    Dim currDate, nMonth, nDay, nYear, prevMonth, prevYear, nextMonth, nextYear, from_d, to_d
    Dim lastMonth, lastYear, lastFirstDayPos, lastNumDays, submit
    Dim numDays, monthName(13), count, pos, firstDayPos, eventDay, dayNum, cell(38)
    Dim events(38)
    Dim tabs, minimize_cal_arrow, minimize_cal, minimize_list_arrow, minimize_list

    currDate=Date
    nMonth=Int(Month(currDate))
    nDay=Int(Day(currDate))
    nYear=Int(Year(currDate)) 
    dayOfWeek=Int(Weekday(currDate))
    lastMonth=nMonth
    lastYear=nYear

   if Request.Form("submit") <> "" then
    lastMonth=Int(Request.Form("lastMonth"))
    lastYear=Int(Request.Form("lastYear"))
    submit = Request.Form("submit")
   end if
   if(Request("month") <> "") then 
	lastMonth = clng(Request("month"))
	nMonth=lastMonth
   end if
   if(Request("year") <> "") then 
	lastYear = clng(Request("year"))
	nYear=lastYear
   end if

    prevMonth=lastMonth-1
    prevYear=lastYear-1
    nextMonth=lastMonth+1
    nextYear=lastYear+1
    if prevMonth=0 then
        prevYear=lastYear-2
        prevMonth=12
    end if
    if nextMonth=13 Then
        nextYear=nextYear + 2
        nextMonth=1
    end if
   
   Function getListClass(listValue)
        Select CASE listValue
        CASE "all"
            getListClass=0
        CASE "alerts"
            getListClass=1
        CASE "surveys"
            getListClass=64
        CASE "wallpapers"
            getListClass=8
        CASE "screensavers"
            getListClass=2
        CASE "RSS"
            getListClass=5
       End select 
   End Function
   
   showClass1=getListClass(listValue)
                     
        
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>DeskAlerts</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link href="css/dashboard.css" rel="stylesheet" type="text/css">
    <link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="css/style9.css" rel="stylesheet" type="text/css">
    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
    <script type="text/JavaScript" language="JavaScript" src="jscripts/json2.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/preview.js?v=9.2.1.31"></script>
    <script language="javascript" type="text/javascript" src="functions.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/date.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.cookies.2.2.0.min.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.cookie.js"></script>

    <%

    monthName(1) = LNG_JANUARY
    monthName(2) = LNG_FEBRUARY
    monthName(3) = LNG_MARCH
    monthName(4) = LNG_APRIL
    monthName(5) = LNG_MAY
    monthName(6) = LNG_JUNE
    monthName(7) = LNG_JULY
    monthName(8) = LNG_AUGUST
    monthName(9) = LNG_SEPTEMBER
    monthName(10) = LNG_OCTOBER
    monthName(11) = LNG_NOVEMBER
    monthName(12) = LNG_DECEMBER

    if submit = "< "&monthName(prevMonth) then
        nMonth=prevMonth
        nYear=lastYear
        if nMonth=12 then
            nYear=nYear-1
            end if
        nextMonth=lastMonth
        nextYear=nYear+1     
        prevMonth=nMonth-1
            if prevMonth=0 then
            prevYear=nYear-2
            prevMonth=12
            end if  
         lastMonth=nMonth
         lastYear=nYear  
    end if
         
    if submit=monthName(nextMonth)&" >" Then
        nMonth=nextMonth
        nYear=lastYear
        if nMonth=1 then
            nYear=nYear+1
            end if
        prevMonth=lastMonth
        prevYear=lastYear-1
        nextYear=lastYear+1
        nextMonth=nMonth+1
            if nextMonth=13 then
            nextYear=nextYear+2
            nextMonth=1
            end if  
         lastMonth=nMonth
         lastYear=nYear           
    end if  
         
    if submit = "<< "&monthName(prevMonth)&" "&prevYear then
        nextMonth=lastMonth
        nMonth=prevMonth
        nYear=prevYear
        prevMonth=nMonth-1
        prevYear=nYear-1
            if prevMonth=0 then
            prevYear=nYear-2
            prevMonth=12
            end if  
         lastMonth=nMonth
         lastYear=nYear 
         nextYear=nYear+1
    end If

    if submit = monthName(nextMonth)&" "&nextYear&" >>" then
        prevMonth=lastMonth
        nMonth=nextMonth
        nYear=nextYear
        nextMonth=nMonth+1
        nextYear=nYear+1 
        prevYear=nYear-1
            if nextMonth=13 then
            nextYear=nYear+2
            nextMonth=1
            end if  
         lastMonth=nMonth
         lastYear=nYear 
    end If  

    if nMonth = 4 or nMonth = 6 or nMonth = 9 or nMonth = 11 then
              numDays = 30
    elseif nMonth <> 2 then
              numDays = 31
    end If

    if nMonth = 2 then
        test1 = nYear Mod 400 
            if test1 = 0 Then
              numDays = 29
            else
              test2 = nYear Mod 4
              test3 = nYear Mod 100
              if test2 = 0 And test3 > 0 Then
                numDays = 29
              else
                numDays = 28
              end If
            end If
    end If 

    %>

    <script language="javascript" type="text/javascript">

        var settingsDateFormat = "<%=GetDateFormat()%>";
        var settingsTimeFormat = "<%=GetTimeFormat()%>";

        var responseDbFormat = "dd/MM/yyyy HH:mm:ss";
        var uiFormat = getUiDateTimeFormat(settingsDateFormat, settingsTimeFormat);
        var serverDate;
        var settingsDateFormat = "<%=GetDateFormat()%>";
        var settingsTimeFormat = "<%=GetTimeFormat()%>";

        var responseDbFormat = "dd/MM/yyyy HH:mm:ss";
        var uiFormat = getUiDateTimeFormat(settingsDateFormat, settingsTimeFormat);

        $(function() {


            $(".delete_button").button();
            $("#show_sent_alert").button();
            $("#show_scheduled").button();
            serverDate = new Date(getDateFromFormat("<%=now_db%>", responseDbFormat));
            setInterval(function() { serverDate.setSeconds(serverDate.getSeconds() + 1); }, 1000);
            $(".alert_data_bg tbody tr td:last-child").css("border-right", "1px solid #cecece");
            var width = $(".data_table").width();
            var pwidth = $(".paginate").width();
            if (width != pwidth) {
                $(".paginate").width("100%");
            }



        })

    </script>

    <%

	'--- rights ---
    set rights = getRights(uid, true, false, true, Array("alerts_val","surveys_val","screensavers_val","wallpapers_val","rss_val","im_val"))
    gid = rights("gid")
    gviewall = rights("gviewall")
    gviewlist = rights("gviewlist")
    alerts_arr = rights("alerts_val")
    surveys_arr = rights("surveys_val")
    screensavers_arr = rights("screensavers_val")
    wallpapers_arr = rights("wallpapers_val")
    rss_arr = rights("rss_val")
    im_arr = rights("im_val")
    classPolicy = Array()
    ReDim classPolicy(8)
    class_num=0
    if alerts_arr(4)<>"" or im_arr(4)<>"" then 
        classPolicy(class_num)=1
        class_num=class_num+1
    end if
    if surveys_arr(4)<>"" then 
        classPolicy(class_num)=64
        classPolicy(class_num+1)=128
        classPolicy(class_num+2)=256
        class_num=class_num+3
    end if
    if screensavers_arr(4)<>"" then 
        classPolicy(class_num)=2
        class_num=class_num+1
    end if        
    if wallpapers_arr(4)<>"" then 
        classPolicy(class_num)=8
        class_num=class_num+1
    end if
    if rss_arr(4)<>"" then 
        classPolicy(class_num)=5
        class_num=class_num+1
    end if    

	'--------------

	from_d="1/1/1900 00:00:00"
    to_d=Now()

 
    %>
	
</head>
<body style="margin: 0" class="body">
	 <form accept-charset="ISO-8859-1" name='my_form' method='post' action='widget_calendar.asp'>
    
                        <%            
                Response.Write "<table cellspacing='0' width='100%' cellpadding='0' border='1' style='margin:auto; border:0px solid #CCC; min-width:500px' ><tr >"
                Response.Write "<td style='border:0px' ><table id='calendar_view' class='calendar_view portlet-content' cellspacing='0' width='100%' cellpadding='0' style='min-width:690px; border-collapse: collapse;margin:auto; border:1px solid #CCC;'><tr ><td style='border:none; padding:10px;border-top:0px solid;'><input type=submit class='table_title' name=submit style='border:0px ;cursor:pointer;background: transparent;' value='<< "&monthName(prevMonth)&" "&prevYear&"'></td>"
                Response.Write "<td colspan='2' style='border:none; padding:10px;'><input type=submit class='table_title' name=submit style='border:0 none;cursor:pointer;background: transparent;' value='< "&monthName(prevMonth)&"'></td>"
                Response.Write "<td class='table_title' colspan='1' style='border:none;text-align:center'>" & monthName(nMonth) & " "& nYear&"</td>"
                Response.Write "<td colspan='2' style='border:none;text-align:right;'><input type=submit class='table_title' style='border:0px ;cursor:pointer;background: transparent;text-align:right;' name=submit value='"&monthName(nextMonth)&" >'></td>"
                Response.Write "<td style='border:none;text-align:right;'><input type=submit class='table_title' style='border:0px ;cursor:pointer;background: transparent;' name=submit value='"&monthName(nextMonth)&" "&nextYear&" >>'></td>"  
                Response.Write "</tr>"        
                Response.Write ""
                Response.Write ""  

                For week=1 to 7
					Execute("wkd = LNG_" & UCase(WeekdayName(week)))
                    Response.Write "<th class='dayHeader' style='border:1px solid #CCC;width:15% !important;'>" & wkd & "</th>"  
                Next             
                Response.Write "<tr style='border:1px solid #CCC;'>"
                firstday = Weekday("01/"&nMonth&"/"&nYear)
                For k=0 to 42
                    displayNum = k-firstDay+2
                    if displayNum < 1 then
                        Response.Write "<td style='border:1px solid #CCC;' class='empty'>&nbsp;</td>"
                    elseif displayNum<numDays+1 then
                        count=0
                        count_schedule=0
                        num=0
	                    from_date=displayNum&"/"&nMonth&"/"&nYear&" 00:00:00"
                        to_date=displayNum&"/"&nMonth&"/"&nYear&" 23:59:59"
                        currentDate = displayNum&"/"&nMonth&"/"&nYear
						query = "processAlerts @select_active = 1, @start_date = '"& from_date &"', @end_date = '"& to_date &"'"
	                    alldaysData = Array()
						
						
                        num=0
						RS.CursorLocation = 3
						RS.open query, conn
					
						ReDim alldaysData(RS.RecordCount) 
                        do while not RS.EOF
	                        if RS("from_date") < RS("to_date") and RS("sent_date") < RS("to_date") and RS("sent_date") < to_date _
	                        then
	                            if RS("from_date")<>1900 then
	                                alldaysData(num) = RS("id")
	                                num=num+1
	                            end if
	                          end if
                        RS.MoveNext
	                    loop
	                    RS.Close

                        for i=0 to num-1
                         if num>0 then
	                        if gviewall <> 1 then
		                        SQL = "SELECT * FROM (SELECT a.id, a.schedule, schedule_type, a.type2, recurrence, alert_text, title, a.type, convert(varchar,a.sent_date) as sent_date, convert(varchar,a.create_date) as create_date, a.from_date, a.to_date, class, urgent, ticker, fullscreen, s.id as is_rsvp FROM alerts a LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id WHERE (a.type='S' OR a.type='L') AND a.create_date <= '" & to_date & "' AND a.id ="& alldaysData(i) &" AND a.sender_id IN ("& Join(gviewlist,",") &") AND a.id NOT IN (SELECT alert_id FROM instant_messages) "
		                        if class_num>0 then
		                            SQL = SQL & " AND (class ="& classPolicy(0)
	                                for k1=1 to class_num-1
		                                SQL = SQL &" OR class="& classPolicy(k1)		            
		                            next 
		                            SQL = SQL & "  ) "     
		                        end if   
		                        SQL = SQL & " UNION ALL "
		                        SQL = SQL & "SELECT a.id, a.schedule, schedule_type, recurrence, a.type2, alert_text, title, a.type, convert(varchar,a.sent_date) as sent_date, convert(varchar,a.create_date) as create_date, a.from_date, a.to_date, class, urgent, ticker, fullscreen, s.id as is_rsvp  FROM archive_alerts a LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id WHERE (a.type='S' OR a.type='L') AND a.create_date <= '" & to_date & "' AND a.id ="& alldaysData(i) &" AND a.sender_id IN ("& Join(gviewlist,",") &") AND a.id NOT IN (SELECT alert_id FROM instant_messages) "
		                        if class_num>0 then
		                            SQL = SQL & " AND (class ="& classPolicy(0)
	                                for k1=1 to class_num-1
		                                SQL = SQL &" OR class="& classPolicy(k1)		            
		                            next 
		                            SQL = SQL & " ) "     
		                        end if     
		                        SQL = SQL & ") res ORDER BY res."&sortby 
                                Set RS = Conn.Execute(SQL)
	                        else
    	                        SQL = "SELECT * FROM (SELECT a.id, a.schedule, schedule_type, a.type2, recurrence, alert_text, title, a.type, convert(varchar,a.sent_date) as sent_date, convert(varchar,a.create_date) as create_date, a.from_date, a.to_date, class, urgent, ticker, fullscreen, s.id as is_rsvp FROM alerts a LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id WHERE (a.type='S' OR a.type='L') AND a.create_date <= '" & to_date & "' AND a.id ="& alldaysData(i) &" AND param_temp_id IS NULL  AND a.id NOT IN (SELECT alert_id FROM instant_messages) "
		                        if class_num>0 then
		                            SQL = SQL & " AND (class="& classPolicy(0)
	                                for k1=1 to class_num-1
		                                SQL = SQL &" OR class="& classPolicy(k1)		            
		                            next 
		                            SQL = SQL & " )  "     
		                        end if   		                        
		                        SQL = SQL & " UNION ALL "             
		                        SQL = SQL & "SELECT a.id, a.schedule, schedule_type, recurrence, a.type2, alert_text, title, a.type, convert(varchar,a.sent_date) as sent_date, convert(varchar,a.create_date) as create_date, a.from_date, a.to_date, class, urgent, ticker, fullscreen, s.id as is_rsvp  FROM archive_alerts a LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id WHERE (a.type='S' OR a.type='L') AND a.create_date <= '" & to_date & "' AND a.id ="& alldaysData(i) &" AND param_temp_id IS NULL  AND a.id NOT IN (SELECT alert_id FROM instant_messages) "
		                        if class_num>0 then
		                            SQL = SQL & " AND (class ="& classPolicy(0)
	                                for k1=1 to class_num-1
		                                SQL = SQL &" OR class="& classPolicy(k1)		            
		                            next 
		                            SQL = SQL & " ) "     
		                        end if   
		                        SQL = SQL & ") res ORDER BY res."&sortby 
		                        Set RS = Conn.Execute(SQL)
	                        end if
	                        Do While Not RS.EOF
	                            if count_schedule>0 then exit do
	                            count_schedule=count_schedule+1
	                            RS.MoveNext
	                        Loop
	                       end if
                        next
	                    if gviewall <> 1 then
		                    SQL = "SELECT COUNT(1) as mycnt, 1 as mycnt1 FROM alerts WHERE (type='S' OR type='L') AND create_date >= '" & from_date & "' AND create_date <= '" & to_date & "' AND param_temp_id IS NULL  AND sender_id IN ("& Join(gviewlist,",") &") AND id NOT IN (SELECT alert_id FROM instant_messages) " 
		                    SQL = SQL & " UNION ALL "
		                    SQL = SQL & "SELECT COUNT(1) as mycnt, 2 as mycnt1 FROM archive_alerts WHERE (type='S' OR type='L') AND create_date >= '" & from_date & "' AND create_date <= '" & to_date & "' AND param_temp_id IS NULL AND sender_id IN ("& Join(gviewlist,",") &") AND id NOT IN (SELECT alert_id FROM instant_messages) "
		                    Set RS = Conn.Execute(SQL)
	                    else
		                    SQL = "SELECT COUNT(1) as mycnt, 1 as mycnt1 FROM alerts WHERE (type='S' OR type='L') AND create_date >= '" & from_date & "' AND create_date <= '" & to_date & "' AND param_temp_id IS NULL  AND id NOT IN (SELECT alert_id FROM instant_messages) "
		                    SQL = SQL & " UNION ALL "
		                    SQL = SQL & "SELECT COUNT(1) as mycnt, 2 as mycnt1 FROM archive_alerts WHERE (type='S' OR type='L') AND create_date >= '" & from_date & "' AND create_date <= '" & to_date & "' AND param_temp_id IS NULL AND id NOT IN (SELECT alert_id FROM instant_messages) "
		                    Set RS = Conn.Execute(SQL)
	                    end if
	                    cnt=0
	                    do while Not RS.EOF
		                    if(Not IsNull(RS("mycnt"))) then cnt=cnt+clng(RS("mycnt"))
		                    RS.MoveNext
	                    loop
	                    RS.Close
	                    if displayNum=Day(Now()) and nMonth=Month(Now()) and nYear=Year(Now()) then 
	                        Response.Write "<td style='border:1px solid #CCC;padding:6px'><div style='text-align:right;background-color:#C6F6C0;'>" 
	                    else Response.Write "<td style='border:1px solid #CCC;padding:6px'><div style='text-align:right;background-color:#ebebeb;'>" 
	                    end if
						%>
						<!--script>
						function href_location(href)
						{
							window.top.location.href=href;
						}
						</script-->
	                        <%
	                        Set campaigns = Conn.Execute("SELECT COUNT(1) as campaign_cnt FROM campaigns WHERE ( DATEPART(YEAR, start_date) = " & nYear & "  AND DATEPART(MONTH, start_date) =  " & nMonth & " AND DATEPART(DAY, start_date) = " & displayNum & " ) OR DATEPART(YEAR, started_date) = " & nYear & "  AND DATEPART(MONTH, started_date) =  " & nMonth & " AND DATEPART(DAY, started_date) = " & displayNum  )
	                        
	                        if not campaigns.EOF then
	                            campaign_cnt = campaigns("campaign_cnt")

	                        end if
	 
							'Response.Write "Count:" & count_schedule
							if cnt<>0 AND count_schedule<>0 then                 
                              Response.Write ""&displayNum&"&nbsp;</div><div><br><a href='dashboard_list.asp?day="&displayNum&"&month="&nMonth&"&year="&nYear&"&tab=0' target='work_area'>"&LNG_CREATED&": "&cnt&"</a><br><a href='dashboard_list.asp?day="&displayNum&"&month="&nMonth&"&year="&nYear&"&tab=1' target='work_area'>"&LNG_SHOW_SCHEDULED&"</a></div>"       
						   elseif count<>0 then
                              Response.Write ""&displayNum&"&nbsp;</div><div><br><a target='work_area' href='dashboard_list.asp?day="&displayNum&"&month="&nMonth&"&year="&nYear&"&tab=0'>"&LNG_CREATED&": "&cnt&"</a><br><br></div>"  
                            elseif count_schedule<>0 then
                              Response.Write ""&displayNum&"&nbsp;</div><div><br><a target='work_area' href='dashboard_list.asp?day="&displayNum&"&month="&nMonth&"&year="&nYear&"&tab=1'>"&LNG_SHOW_SCHEDULED&"</a><br><br></div>"  
                            else
                              Response.Write ""&displayNum&"&nbsp;</div><div><br>&nbsp;<br><br></div>"
                            end if
                            if campaign_cnt <> 0 then
                                 Response.Write "<a target='work_area' href='dashboard_list.asp?day="&displayNum&"&month="&nMonth&"&year="&nYear&"&tab=2'>"&LNG_CAMPAIGN_TITLE&": "&campaign_cnt&"</a>"
                             end if

                             Response.Write "</td>"

                    else  Response.Write "<td style='border:1px solid #CCC;' class='empty'></td>"  
                    end if
                    if k Mod 7=6 then
                        if k < numDays+firstDay-2 then 
                            Response.Write "</tr><tr>"
                        else   
                            exit for  
                        end if
                    end if    
                Next
             end if
                Response.Write "</table></td></tr></table>"              
                Response.Write "<input type=hidden name=lastMonth value=" & nMonth & ">"
                Response.Write "<input type=hidden name=lastYear value=" & nYear & ">"
                Response.Write "<input type=hidden name=lastFirstDayPos value=" & firstDayPos & ">"
                Response.Write "<input type=hidden name=lastNumDays value=" & numDays & ">"
                        %>

    </form>
</body>
</html>
<!-- #include file="db_conn_close.asp" -->
