﻿using System;
using System.Web.UI.WebControls;
using DeskAlerts.Server.Utils.Helpers;
using DeskAlertsDotNet.Parameters;
using Resources;

namespace DeskAlertsDotNet.Pages
{
    public partial class WidgetSurveysStat : DeskAlertsBaseListPage
    {

        protected override void OnLoad(EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request["id"]))
            {
                Response.Redirect("SurveyAnswersStatistic.aspx?id=" + Request["id"] + "&widget=1", true);
                return;
            }

            if (!string.IsNullOrEmpty(Request["search"]))
                searchTitle.Value = Request["search"];

            if (searchTitle.Value.Trim().Length != 0 || !string.IsNullOrEmpty(Request["offset"]))
            {
                contentTable.Visible = true;
                NoRowsDiv.Visible = true;
                base.OnLoad(e);
                titleCell.Text = resources.LNG_TITLE;
                creationDateCell.Text = resources.LNG_CREATION_DATE;

                int cnt = Content.Count < (Offset + Limit) ? Content.Count : Offset + Limit;
                
                for (int i = Offset; i < cnt; i++)
                {
                    var row = Content.GetRow(i);
                    var tableRow = new TableRow();
                    var titleContentCell = new TableCell();
                    var link = new LinkButton {Text = row.GetString("name")};

                    link.Click += delegate
                    {
                        var id = row.GetString("id");
                        ClientScript.RegisterClientScriptBlock(GetType(), "EditSurveyStatId_" + id, string.Format("changeParameters('{0}', '{1}')", id, "id=" + id), true);
                    };

                    titleContentCell.Controls.Add(link);
                    titleContentCell.Enabled = Config.Data.HasModule(Modules.STATISTICS);
                    tableRow.Cells.Add(titleContentCell);

                    var createDateContentCell = new TableCell
                    {
                        Text = DateTimeConverter.ToUiDateTime(row.GetDateTime("create_date"))
                    };

                    tableRow.Cells.Add(createDateContentCell);
                    contentTable.Rows.Add(tableRow);
                }
            }
            else
            {
                contentTable.Visible = false;
                NoRowsDiv.Visible = false;
            }

            search.Text = search.Text.Replace("{Search}", resources.LNG_SEARCH);
        }

        protected override string PageParams
        {
            get
            {
                if (searchTitle.Value.Length > 0)
                    return "search=" + searchTitle.Value + "&";

                if (string.IsNullOrEmpty(Request["search"]))
                    return "search=" + Request["search"] + "&";

                return "";

            }
        }

        #region DataProperties


        protected override string DataTable => "";

        protected override string[] Collumns => new string[]{ };

        protected override string ConditionSqlString => "";

        protected override string DefaultOrderCollumn => "id";

        protected override string CustomQuery
        {
            get
            {
                string sqlQuery = "";
                if(!curUser.HasPrivilege && !curUser.Policy.CanViewAll)
                {
                    string viewListString = string.Join(",", curUser.Policy.SendersViewList.ToArray());
                    sqlQuery = "SELECT surveys_main.id, surveys_main.name, surveys_main.create_date FROM surveys_main INNER JOIN alerts ON alerts.id = surveys_main.sender_id AND alerts.sender_id IN (" + viewListString + ") LEFT JOIN users ON users.id = alerts.sender_id WHERE surveys_main.name like '%" + searchTitle.Value.Replace("'", "''") + "%' ORDER BY " + DefaultOrderCollumn;
                }
                else
                {
                    sqlQuery = "SELECT surveys_main.id, surveys_main.name, surveys_main.create_date FROM surveys_main INNER JOIN alerts ON alerts.id = surveys_main.sender_id LEFT JOIN users ON users.id = alerts.sender_id WHERE surveys_main.name like '%" + searchTitle.Value.Replace("'", "''") + "%' ORDER BY " + DefaultOrderCollumn;
                }

                return sqlQuery;
            }
        }

        protected override string CustomCountQuery
        {
            get
            {
                string sqlQuery = "";
                if (!curUser.HasPrivilege && !curUser.Policy.CanViewAll)
                {
                    string viewListString = string.Join(",", curUser.Policy.SendersViewList.ToArray());
                    sqlQuery = "SELECT COUNT(1) FROM surveys_main INNER JOIN alerts ON alerts.id = surveys_main.sender_id AND alerts.sender_id IN (" + viewListString + ") LEFT JOIN users ON users.id = alerts.sender_id WHERE surveys_main.name like '%" + searchTitle.Value.Replace("'", "''") + "%'";
                }
                else
                {
                    sqlQuery = "SELECT COUNT(1) FROM surveys_main INNER JOIN alerts ON alerts.id = surveys_main.sender_id LEFT JOIN users ON users.id = alerts.sender_id WHERE surveys_main.name like '%" + searchTitle.Value.Replace("'", "''") + "%'";
                }

                return sqlQuery;                
            }
        }



        /*
         * 
         * If you want to add condition for data selection override property ConditionSqlString 
         * Also you can implement your custom query by overriding properties CustomQuery and CustomCountQuery
         *  
         */
        #endregion

        #region Interface properties

        protected override IButtonControl[] DeleteButtons
        {
            get
            {
                return new IButtonControl[] { };
            }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl NoRowsDiv
        {
            get { return NoRows; }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl TableDiv
        {
            get { return mainTableDiv; }
        }

        protected override string ContentName
        {
            get
            {
                //return content name here
                return resources.LNG_ALERTS;
            }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl TopPaging
        {
            get
            {
                return topPaging;
            }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl BottomPaging
        {
            get
            {
                return bottomRanging;
            }
        }

        #endregion

    }
}