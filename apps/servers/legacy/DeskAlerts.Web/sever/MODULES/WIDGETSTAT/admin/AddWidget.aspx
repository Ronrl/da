﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddWidget.aspx.cs" Inherits="DeskAlertsDotNet.sever.MODULES.WIDGETSTAT.admin.AddWidget" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

<script type="text/javascript">
		$(".add_this").button({ label: 'Add' });

		$(".add_this").click(function() {
	
			$("#statList").isotope('reloadItems').isotope({ sortBy: 'original-order' });
            getOrder();

            var mainFrame = parent.document.getElementById("main_frame");


            if (!mainFrame) {
                mainFrame = parent.parent.document.getElementById("main_frame");
            }
            var caller = mainFrame.contentWindow.location.pathname.split('/').pop();


			switch (caller) {
				case 'dashboard.aspx':
					page = "D";
					break;
                case 'Statistics.aspx':
					page = "S";
					break;
				default:
					page = "Z";
					break;
			}
			$("#add_widget_form").dialog("close");
			var item_id = guid().toUpperCase();
			var widget_id = $(this).attr("id");
			var user_id = <% =curUser.Id %>;
			if (page != "Z") {

			    $.ajax({
			        type: "POST",
			        url: "WidgetService.aspx/AddWidget",
			        data: '{instanceId:"'+ item_id + '", userId: '+ user_id + ', widgetId:"'+ widget_id + '", parameters:"", page:"' + page +'"}',
			        contentType: "application/json; charset=utf-8",
			        dataType: "json",
			        success: function (response) {
			        },
			        failure: function (response) {
			            alert(response.d);
			        }
			    });


			}
			

			var title = $(this).attr("custom_title");
			var link = $(this).attr("custom_link");
			addWidgetToPage(item_id, title, link);
		});

    $(".widget-tile").find(".widget_hint").css("display", "none");

		$(".widget-tile").hover(function() {
			$(this).find(".widget_hint").slideDown(250);
		}, function() {
			$(this).find(".widget_hint").slideUp(250);
		})

	var guid = (function() {
		function s4() {
			return Math.floor((1 + Math.random()) * 0x10000)
						   .toString(16)
						   .substring(1);
		}
		return function() {
			return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
					   s4() + '-' + s4() + s4() + s4();
		};
        })();


</script>

</head>
<body>
    <form id="form1" runat="server">
        <div runat="server" id="addWidgetForm" class="ui-dialog-content ui-widget-content" style="width: auto; min-height: 103px; max-height: none; height: auto; overflow: hidden">
         
        </div>
   </form>
</body>
</html>
