﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<!-- #include file="Common.asp" -->
<%

check_session()

uid = Session("uid")
if uid <>"" then

    Server.ScriptTimeout = 120
    Conn.CommandTimeout = 120
    mobile_device=Session("isMobile")
    simple_mobile=Session("isSimpleMobile")

	if(Request("tab") <> "") then 
	tab = Request("tab")
   else 
	tab = 0
   end if

   if(Request("listId1") <> "") then 
	listId1 = Request("listId1")
   else 
	listId1 = 0
   end if
      
   if(Request("listValue") <> "") then 
	listValue = Request("listValue")
   else 
	listValue = "all"
   end if
     
   if(Request("offset") <> "") then 
	offset = clng(Request("offset"))
   else 
	offset = 0
   end if	

   if(Request("sortby") <> "") then 
	sortby = Request("sortby")
   else 
	sortby = "create_date DESC"
   end if	

   if(Request("limit") <> "") then 
	limit = clng(Request("limit"))
   else 
	limit=10
   end if
   
   Function getListClass(listValue)
        Select CASE listValue
        CASE "all"
            getListClass=0
        CASE "alerts"
            getListClass=1
        CASE "surveys"
            getListClass=64
        CASE "wallpapers"
            getListClass=8
        CASE "screensavers"
            getListClass=2
        CASE "RSS"
            getListClass=5
       End select 
   End Function
   
   showClass1=getListClass(listValue)
   
   '--- rights ---
    set rights = getRights(uid, true, false, true, Array("alerts_val","surveys_val","screensavers_val","wallpapers_val","rss_val","im_val"))
    gid = rights("gid")
    gviewall = rights("gviewall")
    gviewlist = rights("gviewlist")
    alerts_arr = rights("alerts_val")
    surveys_arr = rights("surveys_val")
    screensavers_arr = rights("screensavers_val")
    wallpapers_arr = rights("wallpapers_val")
    rss_arr = rights("rss_val")
    im_arr = rights("im_val")
    classPolicy = Array()
    ReDim classPolicy(8)
    class_num=0
    if alerts_arr(4)<>"" or im_arr(4)<>"" then 
        classPolicy(class_num)=1
        class_num=class_num+1
    end if
    if surveys_arr(4)<>"" then 
        classPolicy(class_num)=64
        classPolicy(class_num+1)=128
        classPolicy(class_num+2)=256
        class_num=class_num+3
    end if
    if screensavers_arr(4)<>"" then 
        classPolicy(class_num)=2
        class_num=class_num+1
    end if        
    if wallpapers_arr(4)<>"" then 
        classPolicy(class_num)=8
        class_num=class_num+1
    end if
    if rss_arr(4)<>"" then 
        classPolicy(class_num)=5
        class_num=class_num+1
    end if    

	'--------------
	
	from_d="1/1/1900 00:00:00"
    to_d=Now()
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>DeskAlerts</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link href="css/style9.css" rel="stylesheet" type="text/css">
	<link href="css/dashboard.css" rel="stylesheet" type="text/css">
	<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css" />

	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>

	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
	<script type="text/JavaScript" language="JavaScript" src="jscripts/json2.js"></script>

	<script language="javascript" type="text/javascript" src="jscripts/preview.js?v=9.2.1.31"></script>

	<script language="javascript" type="text/javascript" src="functions.js"></script>

	<script language="javascript" type="text/javascript" src="jscripts/date.js"></script>

	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.cookies.2.2.0.min.js"></script>

	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.cookie.js"></script>

	<script language="javascript" type="text/javascript">

		var cookieObj = new Object();
		var list_id1 = 0;
		var list_value1 = 'all';
		var settingsDateFormat = "<%=GetDateFormat()%>";
		var settingsTimeFormat = "<%=GetTimeFormat()%>";

		var responseDbFormat = "dd/MM/yyyy HH:mm:ss";
		var uiFormat = getUiDateTimeFormat(settingsDateFormat, settingsTimeFormat);
		var serverDate;
		var settingsDateFormat = "<%=GetDateFormat()%>";
		var settingsTimeFormat = "<%=GetTimeFormat()%>";

		var responseDbFormat = "dd/MM/yyyy HH:mm:ss";
		var uiFormat = getUiDateTimeFormat(settingsDateFormat, settingsTimeFormat);

		function openDialogUI(_form, _frame, _href, _width, _height, _title) {
		    $("#dialog-" + _form).dialog('option', 'height', _height);
		    $("#dialog-" + _form).dialog('option', 'width', _width);
		    $("#dialog-" + _form).dialog('option', 'title', _title);
		    $("#dialog-" + _frame).attr("src", _href);
		    $("#dialog-" + _frame).css("height", _height);
		    $("#dialog-" + _frame).css("display", 'block');
		    if ($("#dialog-" + _frame).attr("src") != undefined) {
		        $("#dialog-" + _form).dialog('open');
		        $("#dialog-" + _form).contents().find('#table_border').css('border', 'none');
		    }
		}



		$(document).ready(function() {

		    $("#dialog-form").dialog({
		        autoOpen: false,
		        height: 100,
		        width: 100,
		        modal: true
		    });


		    $('#dialog-frame').on('load', function() {
		        $("#dialog-frame").contents().find('.prelink_stats').on('click', function(e) {
		            e.preventDefault();
		            var link = $(this).attr('href');
		            var title = $(this).attr('title');
		            var last_id = $('.dialog_forms').size();
		            var elem = '<div id="dialog-form-' + last_id + '" class="dialog_forms" style="overflow:hidden;display:none;"><iframe id="dialog-frame-' + last_id + '" style="display:none;width:100%;height:auto;overflow:hidden;border : none;"></iframe></div>';
		            $('#secondary').append(elem);
		            $("#dialog-form-" + last_id).dialog({
		                autoOpen: false,
		                height: 100,
		                width: 100,
		                modal: true
		            });
		            openDialogUI("form-" + last_id, "frame-" + last_id, link, 350, 350, title);
		        });
		    });
		});
		

		function showWallpaperPreview(alertId) {
			parent.showWallpaperPreview(alertId);
		}

		function showAlertPreview(alertId) {
			parent.showAlertPreview(alertId);
		}

		function openDialog(alertID) {
			parent.openDialog(alertID);
		}

		$(function() {

			$("#listItem1").on('change', function() {
				list_id1 = $("#listItem1")[0].selectedIndex;
				list_value1 = $("#listItem1").val();
			});
			$("#listItem1").prop("selectedIndex", '<%=listId1 %>');


			$(".delete_button").button();
			$("#show_sent_alert").button();
			$("#show_scheduled").button();
			serverDate = new Date(getDateFromFormat("<%=now_db%>", responseDbFormat));
			setInterval(function() { serverDate.setSeconds(serverDate.getSeconds() + 1); }, 1000);
			var width = $(".data_table").width();
			var pwidth = $(".paginate").width();
			if (width != pwidth) {
				$(".paginate").width("100%");
			}



		})

	</script>

</head>
<body style="margin: 0" class="body">
 <div id="dialog-form" style="overflow: hidden; display: none;">
        <iframe id='dialog-frame' style="width: 100%; height: 100%; overflow: hidden; border: none;
            display: none"></iframe>
    </div>
    <div id="secondary" style="overflow: hidden; display: none;">
    </div>
	<form accept-charset="ISO-8859-1" name='my_form' method='post' action='#'>

					<div id="last_alerts" style="padding: 10px">
						<div style="text-align: right">
							<select name="listItem1" id="listItem1" onclick="$('#show_sent_alert').attr('href', 'widget_last_content.asp?listId1='+list_id1+'&listValue='+list_value1+'&tab=0')">
								<option value="all">
									<%=LNG_ALL %></option>
								<% if (gid = 0 OR (gid = 1 AND (alerts_arr(4)<>""))) then %>
								<option value="alerts">
									<%=LNG_ALERTS %></option>
								<% end if %>
								<% if SURVEY=1 AND (gid = 0 OR (gid = 1 AND (surveys_arr(4)<>""))) then %>
								<option value="surveys">
									<%=LNG_SURVEYS %></option>
								<% end if %>
								<% if WP=1 AND (gid = 0 OR (gid = 1 AND (wallpapers_arr(4)<>""))) then %>
								<option value="wallpapers">
									<%=LNG_WALLPAPERS %></option>
								<% end if %>
								<% if SS=1  AND (gid = 0 OR (gid = 1 AND (screensavers_arr(4)<>""))) then %>
								<option value="screensavers">
									<%=LNG_SCREENSAVERS %></option>
								<% end if %>
								<% if RSS=1  AND (gid = 0 OR (gid = 1 AND (rss_arr(4)<>""))) then %>
								<option value="RSS">
									<%=LNG_RSS %></option>
								<% end if %>
							</select>
							<a id="show_sent_alert" href="#">
								<%= LNG_SHOW%></a>
						</div>
						<br />
						<%
                        count_alert=0
                        LoadTemplate sDateFormFileName, "DateForm"
                        page="widget_last_content.asp?tab=0&"
                        name=LNG_ALERTS 

                'show main table
                    if gviewall <> 1 then
                        SQL = "SELECT * FROM (SELECT a.id, a.schedule, schedule_type, a.type2, recurrence, alert_text, title, a.type, sent_date, a.create_date as create_date, a.from_date, a.to_date, class, urgent, ticker, fullscreen, s.id as is_rsvp FROM alerts a LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id WHERE (a.type='S' OR a.type='L') AND a.create_date >= '" & from_d & "' AND a.create_date <= '" & to_d & "' AND param_temp_id IS NULL AND a.sender_id IN ("& Join(gviewlist,",") &") AND a.id NOT IN (SELECT alert_id FROM instant_messages) "
                        if showClass1 <> 0 then
                            if showClass1=64 then
                                SQL = SQL & " AND (class ="& showClass1 &" OR class=128 OR class=256)"
                            else 	
                            SQL = SQL & " AND class ="& showClass1
                            end if
                        else
                        if class_num>0 then
                            SQL = SQL & " AND (class ="& classPolicy(0)
                            for k1=1 to class_num-1
                                SQL = SQL &" OR class="&classPolicy(k1)		            
                            next 
                            SQL = SQL & " ) "     
                        end if   
		                end if			                                    
		                SQL = SQL & " UNION ALL "
                        SQL = SQL & "SELECT a.id, a.schedule, schedule_type, recurrence, a.type2, alert_text, title, a.type, sent_date, a.create_date as create_date, a.from_date, a.to_date, class, urgent, ticker, fullscreen, s.id as is_rsvp  FROM archive_alerts a LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id WHERE (a.type='S' OR a.type='L') AND a.create_date >= '" & from_d & "' AND a.create_date <= '" & to_d & "' AND param_temp_id IS NULL AND a.sender_id IN ("& Join(gviewlist,",") &") AND a.id NOT IN (SELECT alert_id FROM instant_messages) ) res ORDER BY res."&sortby
                        Set RS = Conn.Execute(SQL)
                    else
                        SQL = "SELECT * FROM (SELECT a.id, a.schedule, schedule_type, a.type2, recurrence, alert_text, title, a.type, sent_date, a.create_date as create_date, a.from_date, a.to_date, class, urgent, ticker, fullscreen, s.id as is_rsvp FROM alerts a LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id WHERE (a.type='S' OR a.type='L') AND a.create_date >= '" & from_d & "' AND a.create_date <= '" & to_d & "' AND param_temp_id IS NULL AND a.id NOT IN (SELECT alert_id FROM instant_messages) "
                        if showClass1 <> 0 then
                            if showClass1=64 then
                                SQL = SQL & " AND (class ="& showClass1 &" OR class=128 OR class=256)"
                            else 	
                            SQL = SQL & " AND class ="& showClass1
                            end if
                        else
                            if class_num>0 then
                                SQL = SQL & " AND (class ="& classPolicy(0)
                                for k1=1 to class_num-1
                                    SQL = SQL &" OR class="&classPolicy(k1)		            
                                next 
                                SQL = SQL & " ) "     
                            end if   
		                end if	
                        SQL = SQL & " UNION ALL "
                        SQL = SQL & "SELECT a.id, a.schedule, schedule_type, recurrence, a.type2, alert_text, title, a.type, sent_date, a.create_date as create_date, a.from_date, a.to_date, class, urgent, ticker, fullscreen, s.id as is_rsvp  FROM archive_alerts a LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id WHERE (a.type='S' OR a.type='L') AND a.create_date >= '" & from_d & "' AND a.create_date <= '" & to_d & "' AND param_temp_id IS NULL  AND a.id NOT IN (SELECT alert_id FROM instant_messages) ) res ORDER BY res."&sortby
                        Set RS = Conn.Execute(SQL)
                    end if
                    num=offset
                    if(Not RS.EOF) then RS.Move(offset) end if
                    Do While Not RS.EOF
                        count_alert=count_alert+1
                        num=num+1
                        if((offset+limit) > num) then 
                            if(num > offset AND offset+limit >= num) then
                            strObjectID=RS("id")                                                                                             
                       if count_alert=1 then
						%>
						<table width="100%" height="100%" cellspacing="5" cellpadding="5" style="min-width: 670px;
							margin: auto; padding: 10px" id="data_table" class="data_table">
							<tr class="data_table_title">
								<td width="40" style="width: 40px !important">
									<%= LNG_TYPE%>
								</td>
								<td class="table_title" style="min-width: 280px; white-space: nowrap;">
									<%=LNG_ALERT_TITLE %>
								</td>
								<td style="min-width: 120px;" class="table_title">
									<%=LNG_CREATION_DATE %>
								</td>
								<td width="140" style="width: 100px" class="table_title">
									<%=LNG_ACTIONS %>
								</td>
							</tr>
							<% 
                     end if  
							%>
							<tr>
								<td align="center">
									<%           
                    dim isUrgent
                    if RS("urgent") = 1 then
                        isUrgent = true
                    else
                        isUrgent = false
                    end if
                    if RS("class") = "16" then
                        if isUrgent = true then
	                        Response.Write "<img src=""images/alert_types/urgent_unobtrusive.gif"" title=""Urgent Unobtrusive"">"
                        else
	                        Response.Write "<img src=""images/alert_types/unobtrusive.gif"" title=""Unobtrusive"">"
                        end if
                    elseif  RS("class") = "64" OR RS("class") = "128" OR RS("class") = "256" then
                        Response.Write "<img src=""images/alert_types/survey.png"" title=""Survey"">"
                    elseif not isNull(RS("is_rsvp")) then
                        if isUrgent = true then
	                        Response.Write "<img src=""images/alert_types/urgent_rsvp.png"" title=""Urgent RSVP"">"
                        else
	                        Response.Write "<img src=""images/alert_types/rsvp.png"" title=""RSVP"">"
                        end if
                    elseif RS("ticker")=1 then
                        if isUrgent = true then
	                        Response.Write "<img src=""images/alert_types/urgent_ticker.png"" title=""Urgent Ticker"">"
                        else
	                        Response.Write "<img src=""images/alert_types/ticker.png"" title=""Ticker"">"
                        end if
                    elseif  RS("class")="8" then
                            Response.Write "<img src=""images/alert_types/wallpaper.gif"" title=""Wallpaper"">"
                    elseif  RS("class")="2" then
                            Response.Write "<img src=""images/alert_types/screensaver.gif"" title=""Screensaver"">"             	
                    elseif RS("fullscreen")=1 then
                        if isUrgent = true then
	                        Response.Write "<img src=""images/alert_types/urgent_fullscreen.png"" title=""Urgent Fullscreen"">"
                        else
	                        Response.Write "<img src=""images/alert_types/fullscreen.png"" title=""Fullscreen"">"
                        end if
                    elseif isUrgent = true then
                        Response.Write "<img src=""images/alert_types/urgent_alert.gif"" title=""Urgent Alert"">"
                    elseif  RS("type")="L" then
                            Response.Write "<img src=""images/alert_types/linkedin.png"" title=""LinkedIn Alert"">"
                    elseif  RS("class")="5" then
                            Response.Write "<img src=""images/alert_types/rss.gif"" title=""RSS/News Feed"">"
                    else
                        Response.Write "<img src=""images/alert_types/alert.png"" title=""Usual Alert"">"
                    end if
									%>
								</td>
								<td>
									<% 
                   if RS("class")="8" then 
                        Response.Write "<a href=""javascript: showWallpaperPreview('" & CStr(RS("id")) & "')"">" & HtmlEncode(RS("title")) & "</a>"
                   elseif  RS("class")="64" OR RS("class") = "128" OR RS("class") = "256" then 
                        Response.Write  HtmlEncode(RS("title"))
                   else     
                    Response.Write "<a href=""javascript: showAlertPreview('" & CStr(RS("id")) & "')"">" & HtmlEncode(RS("title")) & "</a>"
                   end if
									%>
								</td>
								<td>

									<script language="javascript">
										document.write(getUiDateTime('<%=RS("create_date")%>'));
									</script>

								</td>
								<td align="left" nowrap="nowrap">
									<% if( RS("class")="1" AND gid = 0 OR (gid = 1 AND alerts_arr(0)<>"") AND alerts_arr(1)="1") then %>
									<a target='work_area' href="edit_alert_db.asp?id=<%= RS("id") %>">
										<img src="images/action_icons/duplicate.png" alt="<%=LNG_RESEND_ALERT %>" title="<%=LNG_RESEND_ALERT %>"
											width="16" height="16" border="0" hspace="5"></a>
									<% end if
                                   'if RS("class")="8" then 
                                            'Response.Write "<a href=""javascript: showWallpaperPreview('" & CStr(RS("id")) & "')""><img src=""images/action_icons/preview.png"" alt="""& LNG_PREVIEW &""" title="""& LNG_PREVIEW &""" width=""16"" height=""16"" border=""0"" hspace=""5""/></a>"
                                   'elseif RS("class")<>"64" AND RS("class")<>"128" AND RS("class")<>"256"  then
                                            'Response.Write "<a href=""javascript: showAlertPreview('" & CStr(RS("id")) & "')""><img src=""images/action_icons/preview.png"" alt="""& LNG_PREVIEW &""" title="""& LNG_PREVIEW &""" width=""16"" height=""16"" border=""0"" hspace=""5""/></a>"
                                   'end if
									%><a href="#" onclick="openDialog(<%=RS("id") %>)"><img src="images/action_icons/link.png"
										alt="<%=LNG_DIRECT_LINK%>" title="<%=LNG_DIRECT_LINK%>" width="16" height="16"
										border="0" hspace="6" /></a><%
                                    Response.Write "<a href='#' onclick=""openDialogUI('form','frame','statistics_da_alerts_view_details.asp?id=" & CStr(RS("id")) & "',600,550,'"& LNG_STATISTICS &"');""><img src=""images/action_icons/graph.gif"" alt="""& LNG_STATISTICS &""" title="""& LNG_STATISTICS &""" width=""16"" height=""16"" border=""0"" hspace=""5""/></a>"
										%>
								</td>
							</tr>
							<%
                                end if
                                end if
                            RS.MoveNext
                            Loop
                            RS.Close
							%>
							<%
                    if num>0 then    
                    Response.Write make_pages(offset, num, limit, page, name, sortby)
                    else
                        if listValue ="all" then 
                            Response.Write "<center><b>"&LNG_THERE_ARE_NO_ALERTS&"</b><br><br></center>"
                        else
                        Response.Write "<center><b>"&LNG_THERE_ARE_NO & listValue& "</b><br><br></center>"
                        end if
                    end if
							%>
						</table>
					</div>
	<%
	
else 
    Response.Redirect "settings_edit_profile.asp"    
end if
   
	%>
	</form>
</body>
</html>
<!-- #include file="db_conn_close.asp" -->
