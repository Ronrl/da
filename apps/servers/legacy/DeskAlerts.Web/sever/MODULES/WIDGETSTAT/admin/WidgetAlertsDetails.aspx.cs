﻿using System;
using System.Web.UI.WebControls;
using DeskAlerts.Server.Utils.Helpers;
using DeskAlertsDotNet.Parameters;
using DeskAlertsDotNet.DataBase;

namespace DeskAlertsDotNet.Pages
{
    using Resources;

    public partial class WidgetAlertsDetails : DeskAlertsBaseListPage
    {

        protected override void OnLoad(EventArgs e)
        {
            string alertId = Request["id"];
            if (!string.IsNullOrEmpty(alertId))
            {
                Response.Redirect($"AlertStatisticDetails.aspx?id={alertId}&widget=1", true);

                return;
            }
            if (!string.IsNullOrEmpty(Request["searchTitle"]))
                searchTitle.Value = Request["searchTitle"];


            if (searchTitle.Value.Trim().Length != 0 || !string.IsNullOrEmpty(Request["offset"]))
            {
                contentTable.Visible = true;
                NoRowsDiv.Visible = true;
                base.OnLoad(e);
                titleCell.Text = resources.LNG_TITLE;
                creationDateCell.Text = resources.LNG_CREATION_DATE;

                int cnt = this.Content.Count < (Offset + Limit) ? this.Content.Count : Offset + Limit;

                for (int i = Offset; i < cnt; i++)
                {
                    DataRow row = this.Content.GetRow(i);
                    TableRow tableRow = new TableRow();
                    TableCell titleContentCell = new TableCell();
                    LinkButton link = new LinkButton();
                    link.Text = row.GetString("title");
                    link.Click += delegate
                    {
                        string id = row.GetString("id");
                        ClientScript.RegisterClientScriptBlock(GetType(), "EditSurveyStatId_" + id, string.Format("changeParameters('{0}', '{1}')", id, "id=" + id), true);
                    };

                    titleContentCell.Controls.Add(link);
                    titleContentCell.Enabled = Config.Data.HasModule(Modules.STATISTICS);
                    tableRow.Cells.Add(titleContentCell);
                    tableRow.Cells.Add(new TableCell
                    {
                        Text = DateTimeConverter.ToUiDateTime(row.GetDateTime("create_date"))
                    });

                    contentTable.Rows.Add(tableRow);
                }
            }
            else
            {
                contentTable.Visible = false;
                NoRowsDiv.Visible = false;
            }

            search.Text = search.Text.Replace("{Search}", resources.LNG_SEARCH);
        }

        #region DataProperties

        protected override string DataTable => "alerts";

        protected override string[] Collumns => new string[] { "title", "create_date" };

        protected override string ConditionSqlString => $"title LIKE \'%{searchTitle.Value.Replace("'", "''")}%\' and type!=\'D\'";

        protected override string DefaultOrderCollumn => "id";

        #endregion

        #region Interface properties

        protected override IButtonControl[] DeleteButtons => new IButtonControl[] { };

        protected override System.Web.UI.HtmlControls.HtmlGenericControl NoRowsDiv => NoRows;

        protected override System.Web.UI.HtmlControls.HtmlGenericControl TableDiv => mainTableDiv;

        protected override string ContentName => resources.LNG_ALERTS;

        protected override System.Web.UI.HtmlControls.HtmlGenericControl TopPaging => topPaging;

        protected override System.Web.UI.HtmlControls.HtmlGenericControl BottomPaging => bottomRanging;

        #endregion

    }
}