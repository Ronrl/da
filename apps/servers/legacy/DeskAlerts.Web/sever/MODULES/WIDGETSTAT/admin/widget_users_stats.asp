<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%
	intSessionUserId = Session("uid")
	uid=intSessionUserId
	
	my_day = "01"
	my_month = "01"
	my_year = "2001"

	my_day1 = day(date())
	my_month1 = month(date())
	my_year1 = year(date())

	'date from config---
	test_date=make_date_by(my_day, my_month, my_year)
	test_date1=make_date_by(my_day1, my_month1, my_year1)
	
	from_date = test_date & " 00:00:00"
	to_date = test_date1 & " 23:59:59"
	
	'--- rights ---
	set rights = getRights(uid, true, true, true, null)
	gid = rights("gid")
	gsendtoall = rights("gsendtoall")
	gviewall = rights("gviewall")
	gviewlist = rights("gviewlist")
	'--------------

	if gviewall <> 1 then
		senderWhere = " AND sender_id IN ("& Join(gviewlist,",") &")"
	else
		senderWhere = ""
	end if

	if gsendtoall <> 1 then
		policy_ids = "policy_id = " & Join(rights("policy_ids"), " OR policy_id = ")
		policy_group_ids=editorGetGroupIds(policy_ids)
		policy_group_ids=replace(policy_group_ids, "group_id", "users_groups.group_id")
		if(policy_group_ids <> "") then
			policy_group_ids = "OR " & policy_group_ids
		end if
		ou_ids=editorGetOUIds(policy_ids)
		ou_ids=replace(ou_ids, "ou_id", "OU_User.Id_OU")
		if(ou_ids <> "") then
			ou_ids = "OR " & ou_ids
		end if
	end if
	
	search_type="organization"
	search_id=0
	cnt=0
	if (search_type <>"") then
	
		showSQL = 	" SET NOCOUNT ON" & vbCrLf &_
					" IF OBJECT_ID('tempdb..#tempOUs') IS NOT NULL DROP TABLE #tempOUs" & vbCrLf &_
					" CREATE TABLE #tempOUs  (Id_child BIGINT NOT NULL, Rights BIT NOT NULL);" & vbCrLf &_
					" IF OBJECT_ID('tempdb..#tempUsers') IS NOT NULL DROP TABLE #tempUsers" & vbCrLf &_
					" CREATE TABLE #tempUsers  (user_id BIGINT NOT NULL);" & vbCrLf &_
					" IF OBJECT_ID('tempdb..#tempGroups') IS NOT NULL DROP TABLE #tempGroups" & vbCrLf &_
					" CREATE TABLE #tempGroups  (group_id BIGINT NOT NULL);" & vbCrLf &_
					" DECLARE @now DATETIME;" & vbCrLf &_
					" SET @now = GETDATE(); "
					
		OUsSQL=""

		OUsSQL = OUsSQL & " INSERT INTO #tempOUs (Id_child, Rights) (SELECT id, 0 FROM OU) "

		if (gsendtoall <> 1) then 
			OUsSQL = OUsSQL & vbCrLf &_
				" ;WITH OUsList (Id_parent, Id_child) AS (" & vbCrLf &_
				" 	SELECT Id_child AS Id_parent, Id_child FROM #tempOUs" & vbCrLf &_
				"	UNION ALL" & vbCrLf &_
				"	SELECT h.Id_parent, l.Id_child" & vbCrLf &_
				"	FROM OU_hierarchy as h" & vbCrLf &_
				"	INNER JOIN OUsList as l" & vbCrLf &_
				"	ON h.Id_child = l.Id_parent)" & vbCrLf &_ 
				" UPDATE #tempOUs" & vbCrLf &_
				" SET Rights = 1" & vbCrLf &_
				" FROM #tempOUs t" & vbCrLf &_
				" INNER JOIN OUsList l" & vbCrLf &_
				" ON l.Id_child = t.Id_child" & vbCrLf &_
				" LEFT JOIN policy_ou p ON p.ou_id=l.Id_parent AND " & Replace(policy_ids, "policy_id", "p.policy_id") & vbCrLf &_
				" LEFT JOIN ou_groups g ON g.ou_id=l.Id_parent" & vbCrLf &_
				" LEFT JOIN policy_group pg ON pg.group_id=g.group_id AND " & Replace(policy_ids, "policy_id", "pg.policy_id") & vbCrLf &_
				" WHERE NOT p.id IS NULL OR NOT pg.id IS NULL"
				
		end if

		groupsSQL = " INSERT INTO #tempGroups (group_id)" & vbCrLf &_ 
					" (" & vbCrLf &_
					" SELECT DISTINCT groups.id" & vbCrLf &_
					" FROM groups" & vbCrLf &_
					" INNER JOIN OU_Group og" & vbCrLf &_
					" ON groups.id=og.Id_group" & vbCrLf &_
					" INNER JOIN #tempOUs t" & vbCrLf &_
					" ON og.Id_OU=t.Id_child"
		if (gsendtoall <> 1) then 
			groupsSQL = groupsSQL &_
					" LEFT JOIN policy_group p ON p.group_id=og.Id_group AND "&policy_ids &_
					" WHERE (t.Rights = 1 OR NOT p.group_id IS NULL) " 
		end if
		groupsSQL = groupsSQL & " )"
		
		groupsSQL = groupsSQL &_
						" IF OBJECT_ID('tempdb..#tmp1') IS NOT NULL DROP TABLE #tmp1" & vbCrLf &_
						" CREATE TABLE #tmp1  (group_id BIGINT NOT NULL);" & vbCrLf &_
						" IF OBJECT_ID('tempdb..#tmp2') IS NOT NULL DROP TABLE #tmp2" & vbCrLf &_
						" CREATE TABLE #tmp2  (group_id BIGINT NOT NULL);" & vbCrLf &_
						" INSERT INTO #tmp1 (group_id)(SELECT group_id FROM #tempGroups)" & vbCrLf &_
						" WHILE 1=1" & vbCrLf &_
						" BEGIN" & vbCrLf &_
						" 	INSERT INTO #tmp2 (group_id)" & vbCrLf &_
						" 	(" & vbCrLf &_
						" 			SELECT DISTINCT g.group_id FROM groups_groups as g" & vbCrLf &_
						" 			INNER JOIN #tmp1 as t1" & vbCrLf &_
						" 			ON g.container_group_id = t1.group_id" & vbCrLf &_
						" 			LEFT JOIN #tempGroups as t2" & vbCrLf &_
						" 			ON g.group_id = t2.group_id" & vbCrLf &_
						" 			WHERE t2.group_id IS NULL" & vbCrLf &_
						" 	)" & vbCrLf &_
						" 	IF @@ROWCOUNT = 0 BREAK;" & vbCrLf &_
						" 	DELETE #tmp1" & vbCrLf &_
						" 	INSERT INTO #tmp1 (group_id)(SELECT group_id FROM #tmp2)" & vbCrLf &_
						" 	INSERT INTO #tempGroups (group_id)(SELECT group_id FROM #tmp2)" & vbCrLf &_
						" 	DELETE #tmp2" & vbCrLf &_
						" END; "& vbCrLf &_
						" DROP TABLE #tmp1" & vbCrLf &_
						" DROP TABLE #tmp2 "
		

		usersSQL = " INSERT INTO #tempUsers (user_id)(" & vbCrLf &_
					" SELECT DISTINCT Id_user" & vbCrLf &_
					" FROM OU_User u" & vbCrLf &_
					" INNER JOIN #tempOUs t" & vbCrLf &_
					" ON u.Id_OU=t.Id_child" & vbCrLf &_ 
					" LEFT JOIN users ON u.Id_user = users.id"
		
		whereSQL = ""
		
		if (gsendtoall <> 1) then 
			usersSQL = usersSQL & vbCrLf &_
					" LEFT JOIN policy_user p ON p.user_id=u.Id_user AND "&policy_ids
			whereSQL = " AND (t.Rights = 1 OR NOT p.id IS NULL) "
		end if
		
		usersSQL = usersSQL & " WHERE role='U' " & whereSQL
			
		if (searchSQL<>"") then 
			usersSQL = usersSQL & " AND " & searchSQL
		end if
		usersSQL = usersSQL & ") "
		
		whereSQL = ""
		usersSQL = usersSQL &_
					" INSERT INTO #tempUsers (user_id)(" & vbCrLf &_
					" 	SELECT DISTINCT g.user_id" & vbCrLf &_
					" 	FROM users_groups g" & vbCrLf &_
					" 	INNER JOIN #tempGroups t" & vbCrLf &_
					" 	ON t.group_id = g.group_id" & vbCrLf &_
					" 	LEFT JOIN #tempUsers tu" & vbCrLf &_
					" 	ON tu.user_id = g.user_id" 
			
		if (searchSQL<>"") then
			usersSQL = usersSQL & " LEFT JOIN users ON users.id = g.user_id" 
			whereSQL = " AND " & searchSQL
		end if 
		usersSQL = usersSQL & " WHERE tu.user_id IS NULL" & whereSQL
		usersSQL = usersSQL & " )"
		
		if (search_type <> "ou") then
			usersSQL = usersSQL & vbCrLf &_
						" INSERT INTO #tempUsers(user_id)(" & vbCrLf &_
						" SELECT DISTINCT users.id FROM users "
			
			if (gsendtoall <> 1) then 
				usersSQL = usersSQL & " INNER JOIN policy_user p ON p.user_id=users.id AND "&policy_ids
			end if
			
			usersSQL = usersSQL & vbCrLf &_	
					" LEFT JOIN #tempUsers t" & vbCrLf &_
					" ON t.user_id=users.id" & vbCrLf &_
					" WHERE t.user_id IS NULL "
			
			if (search_type = "DOMAIN") then
				usersSQL = usersSQL & " AND users.domain_id = "&search_id
			end if 
				if (searchSQL<>"") then 
					usersSQL = usersSQL & " AND " & searchSQL
				end if
				usersSQL = usersSQL & ") "	
		end if
		
		usersSQL = usersSQL & vbCrLf &_
					" SELECT COUNT(id) as mycnt, SUM(standby) as standby_cnt, SUM(disabled) as disabled_cnt FROM users" & vbCrLf &_
					" INNER JOIN #tempUsers t ON t.user_id = users.id" & vbCrLf &_
					" WHERE role='U' AND (users.last_request >= '" & from_date & "' AND users.last_request <= '" & to_date & "')" & vbCrLf &_
					" SELECT id, disabled, standby, last_request FROM users" & vbCrLf &_
					" INNER JOIN #tempUsers t ON t.user_id = users.id" & vbCrLf &_
					" WHERE role='U' AND (users.last_request >= '" & from_date & "' AND users.last_request <= '" & to_date & "')" & vbCrLf &_
					" DROP TABLE #tempOUs" & vbCrLf &_
					" DROP TABLE #tempUsers" & vbCrLf &_ 
					" DROP TABLE #tempGroups"
		
		showSQL = showSQL & OUsSQL & groupsSQL & usersSQL
    'Response.Write(showSQL)
	end if
	Set RS1 = Conn.Execute(showSQL)
	intRegisteredUsers=0
	intStandbyUsers=0
	intDisabledUsers=0

	if(Not RS1.EOF) then 
		if(Not IsNull(RS1("mycnt"))) then intRegisteredUsers=RS1("mycnt") end if
		if(Not IsNull(RS1("standby_cnt"))) then intStandbyUsers=RS1("standby_cnt") end if
		if(Not IsNull(RS1("disabled_cnt"))) then intDisabledUsers=RS1("disabled_cnt") end if
	end if

	Set RS1 = RS1.NextRecordset
	intTotalUsers=0
	intOnlineUsers=0
	Do While Not RS1.EOF                                                            
		if(not isNull(RS1("last_request"))) then
			if(RS1("last_request") <> "") then
			        mydiff=DateDiff ("n", RS1("last_request"), now_db)
			if(mydiff > 5) then
			else
				if(RS1("disabled")=0 AND RS1("standby")=0) then
					intOnlineUsers=intOnlineUsers+1
				end if
			end if
			'add standby to online users
'			if(RS1("standby")=1) then
'				intOnlineUsers=intOnlineUsers+1
'			end if

		end if
		end if
		intTotalUsers=intTotalUsers+1
		RS1.MoveNext
	loop

	

	intRegisteredUsers = intTotalUsers-intRegisteredUsers

	usersVal1Color="3B6392"
	usersVal2Color="A44441"
	usersVal3Color="9ABA59"
	usersVal4Color="B161A2"
                        
    usersVal1Name=LNG_ONLINE
	usersVal2Name=LNG_STAND_BY
	usersVal3Name=LNG_OFFLINE
	usersVal4Name=LNG_DISABLED

    usersVal1Value=CStr(intOnlineUsers)
	usersVal2Value=CStr(intStandbyUsers)
	usersVal3Value=CStr(intTotalUsers-intOnlineUsers-intDisabledUsers)
	usersVal4Value=CStr(intDisabledUsers)
	totalUsersValue=intTotalUsers

	if(usersVal1Value<>"0" OR usersVal2Value<>"0" OR usersVal3Value <> "0" OR usersVal4Value <> "0") then
		drawUsers = "true"
		if(usersVal1Value<>"0") then
			usersValues = "['"&usersVal1Name&"',"&usersVal1Value&"]"
			usersColors = "'#"&usersVal1Color&"'"
		end if
		if(usersVal2Value<>"0") then
			if (usersValues <> "") then usersValues = usersValues&"," end if
			usersValues = usersValues&"['"&usersVal2Name&"',"&usersVal2Value&"]"
			if (usersColors <> "") then usersColors = usersColors&"," end if
			usersColors = usersColors&"'#"&usersVal2Color&"'"
		end if
		if(usersVal3Value<>"0") then
			if (usersValues <> "") then usersValues = usersValues&"," end if
			usersValues = usersValues&"['"&usersVal3Name&"',"&usersVal3Value&"]"
			if (usersColors <> "") then usersColors = usersColors&"," end if
			usersColors = usersColors&"'#"&usersVal3Color&"'"
		end if
		if(usersVal4Value<>"0") then
			if (usersValues <> "") then usersValues = usersValues&"," end if
			usersValues = usersValues&"['"&usersVal4Name&"',"&usersVal4Value&"]"
			if (usersColors <> "") then usersColors = usersColors&"," end if
			usersColors = usersColors&"'#"&usersVal4Color&"'"
		end if
	else
		drawUsers = false
	end if


    Response.Write ("USERS VALUES = " & usersValues)
    Response.Write ("USERS COLOR = " & usersColors)
%>
<!doctype html>
<html>
	<head>
		<title>DeskAlerts</title>
		<link href="css/style9.css" rel="stylesheet" type="text/css">
		<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
		<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
		<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
		
		<!--[if lt IE 10]><script language="javascript" type="text/javascript" src="jscripts/excanvas.min.js"></script><![endif]-->
		<script language="javascript" type="text/javascript" src="jscripts/jquery.jqplot.min.js"></script>
		<script language="javascript" type="text/javascript" src="jscripts/jqplot.highlighter.min.js"></script>
		<script type="text/javascript" src="jscripts/jqplot.pieRenderer.min.js"></script>
		<script type="text/javascript" src="jscripts/jqplot.barRenderer.min.js"></script>
		<script type="text/javascript" src="jscripts/jqplot.categoryAxisRenderer.min.js"></script>
		<link rel="stylesheet" type="text/css" href="css/jquery.jqplot.css" />
		
		<script language="javascript" type="text/javascript">
		$(document).ready(function(){
		if("<%=drawUsers%>" == "true"){
			var data = [<%= usersValues%>];
			var usersPlot = jQuery.jqplot ('usersChart', [data], 
			{ 
				grid:{
					background: '#ffffff',
					renderer: $.jqplot.CanvasGridRenderer,
					drawBorder: false,
					shadow:false
				},
				seriesColors:[<%= usersColors%>],
				seriesDefaults: {
					shadow:true,
					shadowAlpha: 0.1,
					renderer: jQuery.jqplot.PieRenderer, 
					rendererOptions: {
						diameter:220,
						padding: 25,
						highlightMouseOver:false
					}
				},
				legend:{
					show:false,
					location:"e"
				},
				highlighter:{
					useAxesFormatters:false,
					show:true,
					tooltipFormatString:'%s',
					fadeTooltip:false
				}	
			}
			);
		}
		else
		{
			var usersPlot = jQuery.jqplot ('usersChart', [['',1]], 
			{ 
				grid:{
					background: '#ffffff',
					renderer: $.jqplot.CanvasGridRenderer,
					drawBorder: false,
					shadow:false
				},
				seriesColors:['#f0f0f1'],
				seriesDefaults: {
					shadow:true,
					shadowAlpha: 0.1,
					renderer: jQuery.jqplot.PieRenderer, 
					rendererOptions: {
						diameter:220,
						padding: 25,
						highlightMouseOver:false
					}
				},
				legend:{
					show:false,
					location:"e"
				}	
			}
			);
		}
	});
		</script>
		<style type="text/css">
			.jqplot-highlighter-tooltip{font-size:12px}
		</style>
	</head>
	<body>
		<center>
		<div id='usersChart' style='width:300px;height:300px'></div>
		<table border=0 class='chartLegend'>
			<tr><td><div class="legend" style='background-color: #<%=usersVal1Color%>;'></div></td><td><%=usersVal1Name%></td><td><%=usersVal1Value%></td></tr>
			<tr><td><div class="legend" style='background-color: #<%=usersVal2Color%>;'></div></td><td><%=usersVal2Name%></td><td><%=usersVal2Value%></td></tr>
			<tr><td><div class="legend" style='background-color: #<%=usersVal3Color%>;'></div></td><td><%=usersVal3Name%></td><td><%=usersVal3Value%></td></tr>
			<tr><td><div class="legend" style='background-color: #<%=usersVal4Color%>;'></div></td><td><%=usersVal4Name%></td><td><%=usersVal4Value%></td></tr>
		</table>
		</center>
	</body>
</html>
<!-- #include file="db_conn_close.asp" -->