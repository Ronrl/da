﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WidgetLastContent.aspx.cs" Inherits="DeskAlertsDotNet.Pages.WidgetLastContent" %>

<%@ Import Namespace="DeskAlertsDotNet.Parameters" %>
<%@ Import Namespace="Resources" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>DeskAlerts</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="css/style9.css" rel="stylesheet" type="text/css" />
    <link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="css/jquery/jquery_timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript" src="jscripts/preview.js?v=9.2.1.31"></script>

    <style>
        body{
            transition-duration: .5s;
        }
    </style>

    <script type="text/javascript" src="functions.js"></script>
    <script type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
    <script type="text/javascript" src="jscripts/date.js"></script>
    <script type="text/javascript" src="jscripts/jquery/jquery_timepicker/jquery-ui-timepicker-addon.js"></script>
    <script type="text/javascript" src="jscripts/jquery/datepicker_fix.js"></script>
    <script type="text/javascript">

        var cookieObj = new Object();
        var list_id1 = 0;
        var list_value1 = 'all';

        function openDialogUI(_form, _frame, _href, _width, _height, _title) {
            $(function () {
                $("#dialog-" + _form).dialog('option', 'height', _height);
                $("#dialog-" + _form).dialog('option', 'width', _width);
                $("#dialog-" + _form).dialog('option', 'title', _title);
                $("#dialog-" + _form).dialog('option', 'beforeClose', function (event, ui) {
                    $("body").css("min-height", "0px;");
                });
                $("#dialog-" + _frame).attr("src", _href);
                $("#dialog-" + _frame).css("height", _height);
                $("#dialog-" + _frame).css("display", 'block');
                if ($("#dialog-" + _frame).attr("src") != undefined) {
                    $("#dialog-" + _form).dialog('open');
                    $("#dialog-" + _form).contents().find('#table_border').css('border', 'none');
                }
            });
        }


        function openDirDialog(alertID) {

            $(function () {
                var url = "<%=Config.Data.GetString("alertsDir")%>/preview.asp?id=" + alertID;
                $('#linkValue').val(url);
                $('#linkValue').focus(function () {
                    $('#linkValue').select().mouseup(function (e) {
                        e.preventDefault();
                        $(this).unbind("mouseup");
                    });
                });

                $('#linkDirDialog').dialog({
                    autoOpen: false,
                    height: 80,
                    width: 550,
                    modal: true,
                    resizable: false

                }).dialog('open');
            });
        }


        $(document).ready(function () {

            $("#dialog-form").dialog({
                autoOpen: false,
                height: 100,
                width: 100,
                modal: true
            });

            $(document).tooltip({
                items: "img[title],a[title]",
                position: {
                    my: "center bottom-20",
                    at: "center top",
                    using: function (position, feedback) {
                        $(this).css(position);
                        $("<div>")
                            .addClass("arrow")
                            .addClass(feedback.vertical)
                            .addClass(feedback.horizontal)
                            .appendTo(this);
                    }
                }
            });


            $('#dialog-frame').on('load', function () {
                $("#dialog-frame").contents().find('.prelink_stats').on('click', function (e) {
                    e.preventDefault();
                    var link = $(this).attr('href');
                    var title = $(this).attr('title');
                    var last_id = $('.dialog_forms').size();
                    var elem = '<div id="dialog-form-' + last_id + '" class="dialog_forms" style="overflow:hidden;display:none;"><iframe id="dialog-frame-' + last_id + '" style="display:none;width:100%;height:auto;overflow:hidden;border : none;"></iframe></div>';
                    $('#secondary').append(elem);
                    $("#dialog-form-" + last_id).dialog({
                        autoOpen: false,
                        height: 100,
                        width: 100,
                        modal: true,
                    });
                    openDialogUI("form-" + last_id, "frame-" + last_id, link, 350, 350, title);
                });
            });
        });


        function showWallpaperPreview(alertId) {
            parent.showWallpaperPreview(alertId);
        }

        function showAlertPreview(alertId) {
            parent.showAlertPreview(alertId);
        }

        function showSurveyPreview(alertId) {
            parent.showSurveyPreview(alertId);
        }

        function showVideoAlertPreview(alertId) {

            var data = new Object();

            getAlertData(alertId,
                false,
                function (alertData) {
                    data = JSON.parse(alertData);
                });

            var fullscreen = parseInt(data.fullscreen);
            var ticker = parseInt(data.ticker);
            var acknowledgement = data.acknowledgement;

            var alert_width = (fullscreen == 1) && !ticker
                ? $(".main_table_body").width()
                : (!ticker ? data.alert_width : $(".main_table_body").width());
            var alert_height = (fullscreen == 1) && !ticker
                ? "800"
                : (!ticker ? data.alert_height : "");
            var alert_html = data.alert_html;
            var alert_title = parseHtmlTitle(alert_html, "<!-- html_title = '", "' -->");

            var top_template;
            var bottom_template;

            data.top_template = top_template;
            data.bottom_template = bottom_template;
            data.alert_width = alert_width;
            data.alert_height = alert_height;
            data.ticker = ticker;
            data.fullscreen = fullscreen;
            data.acknowledgement = acknowledgement;
            data.alert_title = alert_title;
            data.alert_html = alert_html;
            data.caption_href = data.caption_href;

            parent.initAlertPreview(data);
        }

        function showScreenSaverPreview(alertId) {
            $(function () {
                parent.getAlertData(alertId, false, function (alertData) {
                    data = JSON.parse(alertData);
                });

                var html = data.alert_html;
                $("#preview_html").val(html);

                var params = 'width=' + screen.width;
                params += ', height=' + screen.height;
                params += ', top=0, left=0'
                params += ', fullscreen=yes';

                window.open('', 'preview_win', params);

                $("#preview_post_form").submit();

            });
        }

        function openDialog(alertID) {
            parent.openDialog(alertID);
        }

        $(function () {
            $(".delete_button").button();
            $("#show_sent_alert").button();
            $("#show_scheduled").button();
            
            var width = $(".data_table").width();
            var pwidth = $(".paginate").width();
            if (width != pwidth) {
                $(".paginate").width("100%");
            }
        });
    </script>
</head>
<body style="margin: 0;" class="body">
    <form id="form1" runat="server">

        <table class="widget_main_table" style="width: 100%;" border="0">
            <tr>
                <td class="main_table_body">
                    <table style="width: 100%;" border="0">
                        <tbody>
                            <tr>
                                <td style="width: 320px;">
                                    <label for="searchTermBox" style="display:inline-block; margin-top:5px; margin-left:20px;">
                                        <% =resources.LNG_SEARCH_ALERT_BY_TITLE %>
                                    </label>
                                    <input type="text" id="searchTermBox" runat="server" name="uname" value="" style="height: 21px; float: right;" />
                                </td>
                                <td style="width: 90px;">
                                    <asp:LinkButton runat="server" class="search_button ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" ID="searchButton" role="button" aria-disabled="false" Text="<span class='ui-button-icon-primary ui-icon ui-icon-search'></span><span class='ui-button-text'>{Search}</span>" />
                                    <input type="hidden" name="search" value="1" />
                                </td>
                                <td style="padding-left: 25px;">
                                    <asp:DropDownList runat="server" ID="contentSelector" AutoPostBack="true" Height="27"></asp:DropDownList>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div style="margin-left: 10px" runat="server" id="mainTableDiv">
                        <table border="0">
                            <tr>
                                <td>
                                    <div style="margin-left: 10px" id="topPaging" runat="server"></div>
                                </td>
                            </tr>
                        </table>
                        <asp:Table Style="padding-left: 0px; margin-left: 10px; margin-right: 10px;" runat="server" ID="contentTable" Width="98.5%" Height="100%" CssClass="data_table" CellSpacing="0" CellPadding="3">
                            <asp:TableRow CssClass="data_table_title">
                                <asp:TableCell runat="server" Style="width: 15px" ID="typeCell" CssClass="table_title"></asp:TableCell>
                                <asp:TableCell runat="server" ID="titleCell" CssClass="table_title"></asp:TableCell>
                                <asp:TableCell runat="server" Style="width: 200px" ID="createDateCell" CssClass="table_title"></asp:TableCell>
                                <asp:TableCell runat="server" Style="width: 150px" ID="actionsCell" CssClass="table_title"></asp:TableCell>

                            </asp:TableRow>
                        </asp:Table>
                        <table>
                            <tr>
                                <td>
                                    <div style="margin-left: 10px" id="bottomRanging" runat="server"></div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div runat="server" id="NoRows">
                        <b><%=resources.LNG_THERE_ARE_NO_ALERTS %></b>
                    </div>
                </td>
            </tr>
        </table>
    </form>
    <div id="linkDirDialog" title="<%= resources.LNG_DIRECT_LINK %>" style="display: none;">
        <input id="linkValue" type="text" readonly="readonly" style="width: 500px" />
    </div>
    <div id="dialog-form" style="overflow: hidden; display: none;">
        <iframe id='dialog-frame' style="display: none; width: 100%; height: auto; overflow: hidden; border: none;"></iframe>
    </div>
    <div id="confirm_sending" style="overflow: hidden; text-align: center;">
        <a id="anchor_confirm"></a>
    </div>
    <div id="secondary" style="overflow: hidden; display: none;">
    </div>
    <form style="margin: 0; width: 0; height: 0; padding: 0" id="preview_post_form" action="ScreensaverPreview.aspx" method="post" target="preview_win">
        <input type="hidden" id="preview_html" name="html" value="" />
    </form>
</body>
</html>
