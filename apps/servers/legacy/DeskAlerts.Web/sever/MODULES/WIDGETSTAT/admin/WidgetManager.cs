﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using DeskAlertsDotNet.Models;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Utils.Factories;
using DataRow = DeskAlertsDotNet.DataBase.DataRow;
using DataSet = DeskAlertsDotNet.DataBase.DataSet;

namespace DeskAlertsDotNet
{
    public enum WidgetPage
    {
        
        DASHBOARD = 0,
        STATISTIC = 1
    }
    public class WidgetManager
    {
        private static WidgetManager _instance;

        public static WidgetManager Default
        {
            get
            {
                if (_instance == null)
                    return _instance = new WidgetManager();

                return _instance;
            }
        }

        private WidgetManager()
        {

        }

        public Widget GetWidgetById(string dashboardId, string id, DBManager dbMgr, string parameters, string pageStringValue)
        {
            DataSet widgetSet = dbMgr.GetDataByQuery(string.Format("SELECT id, title,  href, priority FROM widgets WHERE id='{0}'", id));


            WidgetPage page = pageStringValue == "S" ? WidgetPage.STATISTIC : WidgetPage.DASHBOARD;
            return new Widget(dashboardId, widgetSet.GetString(0, "title"), widgetSet.GetString(0, "href"),
                widgetSet.GetInt(0, "priority"), parameters, page);
        }
        public List<Widget> GetWidgetsForUser(int id, DBManager dbMgr)
        {
            var userWidgets = new List<Widget>();
            var widgetSet = dbMgr.GetDataByQuery(
                @"SELECT we.id,
                       we.page,
                       w.title,
                       w.href,
                       w.priority,
                       we.[parameters]
                FROM widgets_editors we
                     LEFT JOIN widgets w ON we.widget_id = w.id
                WHERE we.editor_id = @publisherId;",
                SqlParameterFactory.Create(DbType.Int32, id, "publisherId"));

            foreach (var widgetRow in widgetSet)
            {
                var page = widgetRow.GetString("page").ToUpper() == "D" ? WidgetPage.DASHBOARD : WidgetPage.STATISTIC;
                var widget = new Widget(widgetRow.GetString("id"), widgetRow.GetString("title"), widgetRow.GetString("href"), widgetRow.GetInt("priority"), widgetRow.GetString("parameters"), page);
                userWidgets.Add(widget);
            }

            return userWidgets;
        }


    }
}