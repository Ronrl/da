<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%

uid = Session("uid")

if Request("id") <> "" then
	response.redirect("statistics_da_users_view_details.asp?id="&Request("id")&"&widget=1"&"&from_date="&Request("from_date")&"&to_date="&Request("to_date"))
else
	if(Request("offset") <> "") then 
	offset = clng(Request("offset"))
   else 
	offset = 0
   end if	

   if(Request("sortby") <> "") then 
	sortby = Request("sortby")
   else 
	sortby = "name"
   end if	

'counting pages to display
   if(Request("limit") <> "") then 
	limit = clng(Request("limit"))
   else 
	limit=10
   end if
   
   search_title = Request("search_title")
   
%>
<!doctype html>
<html>
<head>
	<title>DeskAlerts</title>
	<link href="css/style9.css" rel="stylesheet" type="text/css">
	<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css" />

	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>

	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>

	

	<script language="javascript" type="text/javascript" src="functions.js"></script>
	
	<script language="javascript" type="text/javascript" src="jscripts/date.js"></script>

	<script language="javascript" type="text/javascript">
		var settingsDateFormat = "<%=GetDateFormat()%>";
		var settingsTimeFormat = "<%=GetTimeFormat()%>";

		var responseDbFormat = "dd/MM/yyyy HH:mm:ss";
		var uiFormat = getUiDateTimeFormat(settingsDateFormat, settingsTimeFormat);

		function saveParameters(parameters) {
			var id = window.frameElement.getAttribute("id");
			id = id.substring(1);
			var jqxhr = $.get("manipulate_widgets.asp", { id: id, action: "update", parameters: parameters })
					.done(function() {
						var src = window.frameElement.getAttribute("src");
						src = src + "?" + parameters;
						id = "i" + id;
						window.parent.changeIframeSrc(id, src);
					});
		}

		
		$(document).ready(function() {
			$("#search").button();
		});
	</script>

	<style type="text/css">
		.jqplot-highlighter-tooltip
		{
			font-size: 12px;
		}
	</style>
</head>
<body>
	<center>
		<form action="#">
		<table>
			<tr>
				<td>
					<%=LNG_SEARCH_USER_BY_NAME %>:
				</td>
				<td>
					<input type="text" name="search_title" style="width: 100px" value="<%=Request("search_title") %>" />
				</td>
				<td>
					<button id="search" type="submit">
						<%=LNG_SEARCH %></button>
				</td>
			</tr>
		</table>
		<% 
		if search_title <> "" then
			my_day = "01"
			my_month = "01"
			my_year = "2001"

			my_day1 = day(date())
			my_month1 = month(date())
			my_year1 = year(date())

			test_date=make_date_by(my_day, my_month, my_year)
			test_date1=make_date_by(my_day1, my_month1, my_year1)

			from_date = test_date & " 00:00:00"
			to_date = test_date1 & " 23:59:59"


	searchSQL = " (users.name LIKE N'%"&Replace(search_title, "'", "''")&"%' OR users.display_name LIKE N'%"&Replace(search_title, "'", "''")&"%')"

	'editor ---
	Set RS = Conn.Execute("SELECT id, group_id FROM users WHERE id="&uid&" AND role='E'")
	if(Not RS.EOF) then
		gid = 1
		Set rs_policy = Conn.Execute("SELECT p.id, p.type, pu.user_id FROM policy p INNER JOIN policy_editor pe ON p.id=pe.policy_id LEFT JOIN policy_user pu ON pu.policy_id = p.id AND pu.user_id = 0 WHERE pe.editor_id=" & uid)
		if Not rs_policy.EOF then
			if rs_policy("type")="A" or not IsNull(rs_policy("user_id")) then 'can send to all
				gid = 0
			end if
		end if
		if(gid=1) then
			editor_id=RS("id")
			policy_ids=""
			policy_ids=editorGetPolicyIds(editor_id)
			policy_group_ids=""
			policy_group_ids=editorGetGroupIds(policy_ids)
			policy_group_ids=replace(policy_group_ids, "group_id", "users_groups.group_id")
			if(policy_group_ids <> "") then
				policy_group_ids = "OR " & policy_group_ids
			end if
		end if
	else
		gid = 0
	end if

	RS.Close
'--------
'---------------------------------
	myuname = search_title

	search_type="organization"
	search_id=0
	cnt=0
	if (search_type <>"") then
	
		showSQL = 	" SET NOCOUNT ON" & vbCrLf &_
					" IF OBJECT_ID('tempdb..#tempOUs') IS NOT NULL DROP TABLE #tempOUs" & vbCrLf &_
					" CREATE TABLE #tempOUs  (Id_child BIGINT NOT NULL, Rights BIT NOT NULL);" & vbCrLf &_
					" IF OBJECT_ID('tempdb..#tempUsers') IS NOT NULL DROP TABLE #tempUsers" & vbCrLf &_
					" CREATE TABLE #tempUsers  (user_id BIGINT NOT NULL);" & vbCrLf &_
					" IF OBJECT_ID('tempdb..#tempGroups') IS NOT NULL DROP TABLE #tempGroups" & vbCrLf &_
					" CREATE TABLE #tempGroups  (group_id BIGINT NOT NULL);" & vbCrLf &_
					" DECLARE @now DATETIME;" & vbCrLf &_
					" SET @now = GETDATE(); "
					
		OUsSQL=""

		if(search_type = "ou") then
			if Request("search") = "1" then
				OUsSQL = OUsSQL & "WITH OUs (id) AS("
				OUsSQL = OUsSQL &" SELECT CAST("&search_id&" AS BIGINT) "
				OUsSQL = OUsSQL &_
						"	UNION ALL" & vbCrLf &_
						"	SELECT Id_child" & vbCrLf &_
						"	FROM OU_hierarchy h" & vbCrLf &_
						"	INNER JOIN OUs ON OUs.id = h.Id_parent" & vbCrLf &_
						") " & vbCrLf &_
						"INSERT INTO #tempOUs (Id_child, Rights) (SELECT DISTINCT id, 0 FROM OUs) Option (MaxRecursion 10000); "
			else
				OUsSQL = OUsSQL &" INSERT INTO #tempOUs (Id_child, Rights) VALUES ("&search_id&", 0)"
			end if
		elseif(search_type = "DOMAIN") then
			OUsSQL = OUsSQL &" INSERT INTO #tempOUs (Id_child, Rights) (SELECT id, 0 FROM OU WHERE Id_domain = "&search_id&")"
		else
			OUsSQL = OUsSQL & " INSERT INTO #tempOUs (Id_child, Rights) (SELECT id, 0 FROM OU) "
		end if

		if (gid = 1) then 
			OUsSQL = OUsSQL & vbCrLf &_
				" ;WITH OUsList (Id_parent, Id_child) AS (" & vbCrLf &_
				" 	SELECT Id_child AS Id_parent, Id_child FROM #tempOUs" & vbCrLf &_
				"	UNION ALL" & vbCrLf &_
				"	SELECT h.Id_parent, l.Id_child" & vbCrLf &_
				"	FROM OU_hierarchy as h" & vbCrLf &_
				"	INNER JOIN OUsList as l" & vbCrLf &_
				"	ON h.Id_child = l.Id_parent)" & vbCrLf &_ 
				" UPDATE #tempOUs" & vbCrLf &_
				" SET Rights = 1" & vbCrLf &_
				" FROM #tempOUs t" & vbCrLf &_
				" INNER JOIN OUsList l" & vbCrLf &_
				" ON l.Id_child = t.Id_child" & vbCrLf &_
				" LEFT JOIN policy_ou p ON p.ou_id=l.Id_parent AND " & Replace(policy_ids, "policy_id", "p.policy_id") & vbCrLf &_
				" LEFT JOIN ou_groups g ON g.ou_id=l.Id_parent" & vbCrLf &_
				" LEFT JOIN policy_group pg ON pg.group_id=g.group_id AND " & Replace(policy_ids, "policy_id", "pg.policy_id") & vbCrLf &_
				" WHERE NOT p.id IS NULL OR NOT pg.id IS NULL"
				
		end if

		groupsSQL = " INSERT INTO #tempGroups (group_id)" & vbCrLf &_ 
					" (" & vbCrLf &_
					" SELECT DISTINCT groups.id" & vbCrLf &_
					" FROM groups" & vbCrLf &_
					" INNER JOIN OU_Group og" & vbCrLf &_
					" ON groups.id=og.Id_group" & vbCrLf &_
					" INNER JOIN #tempOUs t" & vbCrLf &_
					" ON og.Id_OU=t.Id_child"
		if (gid = 1) then 
			groupsSQL = groupsSQL &_
					" LEFT JOIN policy_group p ON p.group_id=og.Id_group AND "&policy_ids &_
					" WHERE (t.Rights = 1 OR NOT p.group_id IS NULL) " 
		end if
		groupsSQL = groupsSQL & " )"
						
		groupsSQL = groupsSQL &_
						" IF OBJECT_ID('tempdb..#tmp1') IS NOT NULL DROP TABLE #tmp1" & vbCrLf &_
						" CREATE TABLE #tmp1  (group_id BIGINT NOT NULL);" & vbCrLf &_
						" IF OBJECT_ID('tempdb..#tmp2') IS NOT NULL DROP TABLE #tmp2" & vbCrLf &_
						" CREATE TABLE #tmp2  (group_id BIGINT NOT NULL);" & vbCrLf &_
						" INSERT INTO #tmp1 (group_id)(SELECT group_id FROM #tempGroups)" & vbCrLf &_
						" WHILE 1=1" & vbCrLf &_
						" BEGIN" & vbCrLf &_
						" 	INSERT INTO #tmp2 (group_id)" & vbCrLf &_
						" 	(" & vbCrLf &_
						" 			SELECT DISTINCT g.group_id FROM groups_groups as g" & vbCrLf &_
						" 			INNER JOIN #tmp1 as t1" & vbCrLf &_
						" 			ON g.container_group_id = t1.group_id" & vbCrLf &_
						" 			LEFT JOIN #tempGroups as t2" & vbCrLf &_
						" 			ON g.group_id = t2.group_id" & vbCrLf &_
						" 			WHERE t2.group_id IS NULL" & vbCrLf &_
						" 	)" & vbCrLf &_
						" 	IF @@ROWCOUNT = 0 BREAK;" & vbCrLf &_
						" 	DELETE #tmp1" & vbCrLf &_
						" 	INSERT INTO #tmp1 (group_id)(SELECT group_id FROM #tmp2)" & vbCrLf &_
						" 	INSERT INTO #tempGroups (group_id)(SELECT group_id FROM #tmp2)" & vbCrLf &_
						" 	DELETE #tmp2" & vbCrLf &_
						" END; "& vbCrLf &_
						" DROP TABLE #tmp1" & vbCrLf &_
						" DROP TABLE #tmp2 "
		

		usersSQL = " INSERT INTO #tempUsers (user_id)(" & vbCrLf &_
					" SELECT DISTINCT Id_user" & vbCrLf &_
					" FROM OU_User u" & vbCrLf &_
					" INNER JOIN #tempOUs t" & vbCrLf &_
					" ON u.Id_OU=t.Id_child" & vbCrLf &_ 
					" LEFT JOIN users ON u.Id_user = users.id"
		
		whereSQL = ""
		
		if (gid = 1) then 
			usersSQL = usersSQL & vbCrLf &_
					" LEFT JOIN policy_user p ON p.user_id=u.Id_user AND "&policy_ids
			whereSQL = " AND (t.Rights = 1 OR NOT p.id IS NULL) "
		end if
		
		usersSQL = usersSQL & " WHERE role='U' " & whereSQL
			
		if (searchSQL<>"") then 
			usersSQL = usersSQL & " AND " & searchSQL
		end if
		usersSQL = usersSQL & ") "
		
		whereSQL = ""
		usersSQL = usersSQL &_
					" INSERT INTO #tempUsers (user_id)(" & vbCrLf &_
					" 	SELECT DISTINCT g.user_id" & vbCrLf &_
					" 	FROM users_groups g" & vbCrLf &_
					" 	INNER JOIN #tempGroups t" & vbCrLf &_
					" 	ON t.group_id = g.group_id" & vbCrLf &_
					" 	LEFT JOIN #tempUsers tu" & vbCrLf &_
					" 	ON tu.user_id = g.user_id" 
			
		if (searchSQL<>"") then
			usersSQL = usersSQL & " LEFT JOIN users ON users.id = g.user_id" 
			whereSQL = " AND " & searchSQL
		end if 
		usersSQL = usersSQL & " WHERE tu.user_id IS NULL" & whereSQL
		usersSQL = usersSQL & " )"
		
		if (search_type <> "ou") then
			usersSQL = usersSQL & vbCrLf &_
						" INSERT INTO #tempUsers(user_id)(" & vbCrLf &_
						" SELECT DISTINCT users.id FROM users "
			
			if (gid = 1) then 
				usersSQL = usersSQL & " INNER JOIN policy_user p ON p.user_id=users.id AND "&policy_ids
			end if
			
			usersSQL = usersSQL & vbCrLf &_	
					" LEFT JOIN #tempUsers t" & vbCrLf &_
					" ON t.user_id=users.id" & vbCrLf &_
					" WHERE t.user_id IS NULL "
			
			if (search_type = "DOMAIN") then
				usersSQL = usersSQL & " AND users.domain_id = "&search_id
			end if 
				if (searchSQL<>"") then 
					usersSQL = usersSQL & " AND " & searchSQL
				end if
				usersSQL = usersSQL & ") "	
		end if
		
		usersSQL = usersSQL & vbCrLf &_
					" SELECT COUNT(1) as cnt FROM #tempUsers t" & vbCrLf &_
					" INNER JOIN users ON t.user_id = users.id" & vbCrLf &_
					" WHERE role='U' AND (users.last_request >= '" & from_date & "' AND users.last_request <= '" & to_date & "') AND (users.name LIKE N'%" & Replace(myuname, "'", "''") & "%' OR users.display_name LIKE N'%" & Replace(myuname, "'", "''") & "%')" & vbCrLf &_
					" SELECT user_id as id, users.next_request, users.last_standby_request, users.next_standby_request, users.context_id, users.mobile_phone, users.email, users.name as name, users.display_name, users.standby, users.domain_id, domains.name as domain_name, users.deskbar_id, users.reg_date, users.last_date, CONVERT(varchar, users.last_request) as last_request1, users.last_request, users.client_version, " & vbCrLf &_
					" edir_context.name as context_name, users.disabled" & vbCrLf &_
					" FROM #tempUsers t" & vbCrLf &_
					" INNER JOIN users ON t.user_id = users.id" & vbCrLf &_
					" LEFT JOIN domains ON domains.id = users.domain_id" & vbCrLf &_
					" LEFT JOIN edir_context ON edir_context.id = users.context_id" & vbCrLf &_
					" WHERE role='U' AND (users.last_request >= '" & from_date & "' AND users.last_request <= '" & to_date & "') AND (users.name LIKE N'%" & Replace(myuname, "'", "''") & "%' OR users.display_name LIKE N'%" & Replace(myuname, "'", "''") & "%')" & vbCrLf &_
					" ORDER BY " & sortby & vbCrLf &_
					" DROP TABLE #tempOUs" & vbCrLf &_
					" DROP TABLE #tempUsers" & vbCrLf &_ 
					" DROP TABLE #tempGroups"
		
		showSQL = showSQL & OUsSQL & groupsSQL & usersSQL

		Set RS = Conn.Execute(showSQL)
		

		if(Not RS.EOF) then
			cnt=RS("cnt")
			SET RS = RS.NextRecordSet
		end if
		
	end if
			
			if(cnt>0) then
			page="widget_user_details.asp?search_title="&search_title&"&"
			name=LNG_USERS 
			Response.Write make_pages(offset, cnt, limit, page, name, sortby) 
		%>
		<table width="100%" height="100%" cellspacing="0" cellpadding="3" class="data_table">
			<tr class="data_table_title">
				<td class="table_title">
					<%=sorting(LNG_NAME,"name", sortby, offset, limit, page) %>
				</td>
			</tr>
			<%

'show main table
	num=offset
	if(Not RS.EOF) then RS.Move(offset) end if
	Do While Not RS.EOF
		num=num+1
		if((offset+limit) < num) then Exit Do end if

		if(num > offset AND offset+limit >= num) then
			
			if(IsNull(RS("name"))) then
				name="Unregistered"
			else
				name = RS("name")
			end if
			
			%>
			<tr>
				<td>
					<% 
			Response.Write "<a href='#' onclick=""saveParameters('id=" & CStr(RS("id")) & "&from_date=" & from_date & "&to_date=" & to_date &"')"">" & HtmlEncode(name) & "</a>"
					%>
				</td>
			</tr>
			<%
		end if
	RS.MoveNext
	Loop
	RS.Close


			%>
		</table>
		<%
	Response.Write make_pages(offset, cnt, limit, page, name, sortby) 

else

Response.Write "<center><b>"&LNG_THERE_ARE_NO_USERS&"</b></center>"

end if
else
%>
	</br>
	</br>
	</br>
	</br>
<%
end if
		%>
		</form>
	</center>
</body>
</html>
<% end if  %>
<!-- #include file="db_conn_close.asp" -->
