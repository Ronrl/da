﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WidgetLastContent.aspx.cs" Inherits="DeskAlertsDotNet.Pages.WidgetLastContent" %>
<%@ Import NameSpace="DeskAlertsDotNet.Parameters" %>
<%@ Import Namespace="DeskAlertsDotNet.Utils" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<title>DeskAlerts</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link href="css/style.css" rel="stylesheet" type="text/css">
        <link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <link href="css/jquery/jquery_timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css">
        <script language="javascript" type="text/javascript" src="functions.asp"></script>
        <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/date.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery_timepicker/jquery-ui-timepicker-addon.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/jquery/datepicker_fix.js"></script>
        <script language="javascript" type="text/javascript">

            var cookieObj = new Object();
            var list_id1 = 0;
            var list_value1 = 'all';


            var responseDbFormat = "dd/MM/yyyy HH:mm:ss";
            settingsDateFormat = "<%  =Settings.Content["ConfDateFormat"]  %>";
            settingsTimeFormat = "<%  =DeskAlertsUtils.GetTimeFormat(dbMgr)  %>";
            var uiFormat = getUiDateTimeFormat(settingsDateFormat, settingsTimeFormat);
            var serverDate;


            var responseDbFormat = "dd/MM/yyyy HH:mm:ss";
            var uiFormat = getUiDateTimeFormat(settingsDateFormat, settingsTimeFormat);

            function openDialogUI(_form, _frame, _href, _width, _height, _title) {

                $(function() {
                    $("#dialog-" + _form).dialog('option', 'height', _height);
                    $("#dialog-" + _form).dialog('option', 'width', _width);
                    $("#dialog-" + _form).dialog('option', 'title', _title);
                    $("#dialog-" + _frame).attr("src", _href);
                    $("#dialog-" + _frame).css("height", _height);
                    $("#dialog-" + _frame).css("display", 'block');
                    if ($("#dialog-" + _frame).attr("src") != undefined) {
                        $("#dialog-" + _form).dialog('open');
                        $("#dialog-" + _form).contents().find('#table_border').css('border', 'none');
                    }
                });
            }


            function openDirDialog(alertID) {

                //   alert(alertID);
                $(function () {
                    var url = "<%=Config.Data.GetString("alertsDir")%>/preview.asp?id=" + alertID;
                    $('#linkValue').val(url);
                    $('#linkValue').focus(function () {
                        $('#linkValue').select().mouseup(function (e) {
                            e.preventDefault();
                            $(this).unbind("mouseup");
                        });
                    });

                    $('#linkDirDialog').dialog({
                        autoOpen: false,
                        height: 80,
                        width: 550,
                        modal: true,
                        resizable: false

                    }).dialog('open');
                });
            }


            $(document).ready(function () {

                $("#dialog-form").dialog({
                    autoOpen: false,
                    height: 100,
                    width: 100,
                    modal: true
                });
               

                $(document).tooltip({
                    items: "img[title],a[title]",
                    position: {
                        my: "center bottom-20",
                        at: "center top",
                        using: function (position, feedback) {
                            $(this).css(position);
                            $("<div>")
                           .addClass("arrow")
                           .addClass(feedback.vertical)
                           .addClass(feedback.horizontal)
                           .appendTo(this);
                        }
                    }
                });


                $('#dialog-frame').on('load', function () {
                    $("#dialog-frame").contents().find('.prelink_stats').on('click', function (e) {
                        e.preventDefault();
                        var link = $(this).attr('href');
                        var title = $(this).attr('title');
                        var last_id = $('.dialog_forms').size();
                        var elem = '<div id="dialog-form-' + last_id + '" class="dialog_forms" style="overflow:hidden;display:none;"><iframe id="dialog-frame-' + last_id + '" style="display:none;width:100%;height:auto;overflow:hidden;border : none;"></iframe></div>';
                        $('#secondary').append(elem);
                        $("#dialog-form-" + last_id).dialog({
                            autoOpen: false,
                            height: 100,
                            width: 100,
                            modal: true
                        });
                        openDialogUI("form-" + last_id, "frame-" + last_id, link, 350, 350, title);
                    });
                });
            });


            function showWallpaperPreview(alertId) {
                parent.showWallpaperPreview(alertId);
            }

            function showAlertPreview(alertId) {
                parent.showAlertPreview(alertId);
            }

            function showSurveyPreview(alertId) {
                parent.showSurveyPreview(alertId);
            }

            function showScreenSaverPreview(alertId) {
                parent.showScreenSaverPreview(alertId);
            }

            function openDialog(alertID) {
                parent.openDialog(alertID);
            }

            $(function() {


                $(".delete_button").button();
                $("#show_sent_alert").button();
                $("#show_scheduled").button();
                serverDate = new Date(getDateFromFormat("<%=toDate%>", responseDbFormat));
                setInterval(function() { serverDate.setSeconds(serverDate.getSeconds() + 1); }, 1000);
                var width = $(".data_table").width();
                var pwidth = $(".paginate").width();
                if (width != pwidth) {
                    $(".paginate").width("100%");
                }


            });




	</script>
    <script language="javascript" type="text/javascript" >
        function terminateAlert(id) {
            var url = "<%=Config.Data.GetString("alertsDir")%>/TerminateAlert.aspx?fromCenter=1&alertId=" + id;
            $.get(url)
                .success(function() {
                    alert("Terminated");
                });
        }
    </script>
</head>
<body style="margin: 0" class="body">
    <form id="form1" runat="server">
    <table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	    <tr>
	    <td>
	    <table width="100%" height="100%" cellspacing="0" cellpadding="0" class="widget_main_table">
		    <tr>
		    </tr>
            <tr>
            <td class="main_table_body" height="100%">
	                <br /><br />


                <table style="padding-left: 10px;" width="100%" border="0"><tbody><tr valign="middle"><td width="240">
                        <% =Localization.Current.Content["LNG_SEARCH_ALERT_BY_TITLE"] %><br/>
                        <input type="text" id="searchTermBox" runat="server" name="uname" value="" />
                    </td>
                <td width="1%">
                    <br />
                    <asp:linkbutton runat="server" class="search_button ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" id="searchButton" style="margin-right:0px" role="button" aria-disabled="false" Text="<span class='ui-button-icon-primary ui-icon ui-icon-search'></span><span class='ui-button-text'>{Search}</span>"/>
	                <input type="hidden" name="search" value="1">
                </td>

                <td>
                </td></tr></tbody></table>		
                <div style="text-align: right">
                    <asp:DropDownList style="margin-right:10px" runat="server" id="contentSelector" AutoPostBack="true">
                    </asp:DropDownList>
                </div>
			<div style="margin-left:10px" runat="server" id="mainTableDiv">

			   <table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr><td> 
					<div style="margin-left:10px" id="topPaging" runat="server"></div>
				</td></tr>
				</table>
				<br>
			
				<br><br>
                

				<asp:Table style="padding-left: 0px;margin-left: 10px;margin-right: 10px;" runat="server" id="contentTable" Width="98.5%" Height="100%" CssClass="data_table" CellSpacing="0" CellPadding="3">
				<asp:TableRow CssClass="data_table_title">
					<asp:TableCell runat="server" style="width:15px" id="typeCell" CssClass="table_title" ></asp:TableCell>
					<asp:TableCell runat="server" id="titleCell" CssClass="table_title"></asp:TableCell>
                    <asp:TableCell runat="server" style="width:200px" id="createDateCell" CssClass="table_title"></asp:TableCell>
                    <asp:TableCell runat="server" style="width:150px" id="actionsCell" CssClass="table_title"></asp:TableCell>

				</asp:TableRow>
				</asp:Table>
				<br>
				 
				 <br><br>
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				 
				<tr><td> 
					<div style="margin-left:10px" id="bottomRanging" runat="server"></div>
				</td></tr>
				</table>
			</div>
			<div runat="server" id="NoRows">
                <br />
                <br />
				<center><b><%=Localization.Current.Content["LNG_THERE_ARE_NO_ALERTS"] %></b></center>
			</div>
			<br><br>
            </td>
            </tr>
           
 
            
             </table>
    </form>
</body>
<div id="linkDirDialog" title="<%= Localization.Current.Content["LNG_DIRECT_LINK"] %>" style="display:none;" >
	<input id="linkValue" type="text" readonly="readonly" style="width:500px" />
</div>
<div id="dialog-form" style="overflow:hidden;display:none;">
<iframe id='dialog-frame' style="display:none;width:100%;height:auto;overflow:hidden;border : none;">

</iframe>
</div>
<div id="confirm_sending" style="overflow:hidden;text-align:center;">
<a id="anchor_confirm"></a>
</div>
    <div id="secondary" style="overflow:hidden;display:none;">
</div>
    
 

</html>
