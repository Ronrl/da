<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="md5.asp" -->
<!-- #include file="lang.asp" -->
<!-- #include file="ad_functions.inc" -->
<% 
Response.Expires = 0
Server.ScriptTimeout = 90000 


'===================================================================
'========================== Check Settings =========================
'===================================================================
If Request("check_settings") <> "" Then
	strFilter = "("&strAttrClass&"=*)"
	strAttribs = "dn"
	LDAPQuery.execQuery strLogin, strPassword, strDomain&strPort, "", "base", strFilter, strAttribs, secure, ""
	If LDAPQuery.getLastError <> 0 Then
		'WriteLineToResponseAndLog "login: "&strLogin&", pwd: "&strPassword&", DN: "&strDomain&strPort&", Error: "&LDAPQuery.getLastError
		Response.Write "No"
	Else
		Response.Write "Yes"
	End If
	Response.End
End If


'===================================================================
'========================== Refresh Groups ==========================
'===================================================================
If Request("groups_refresh") <> "" Then

	set groupNames = Request("group_name")
	queryStr = ""
	For grIndex = 1 To groupNames.Count
	  queryStr = queryStr & "(" & strGroupName & "=" & EscapeCN(groupNames(grIndex)) & ")"
	Next
	queryStr = "(|" & queryStr & ")"

	strFilter = "(&("&strAttrClass&"="&strGroupClass&")"&strGroupFilter&"("&queryStr&"))"
	
	strAttribs = strGroupName
	groupsQueryResult = LDAPQuery.execQuery(strLogin, strPassword, strDomain&strPort, "", "subtree", strFilter, strAttribs, secure, "")
	parseLdapError LDAPQuery.getLastError()
	
	
	result = "["

	
	id = 0
	For Each group In groupsQueryResult
		'If isInOUs(group.dn) Then
			group_name = HTMLEncode(getAttr(group(strGroupName)))
			If result <> "[" Then
				result = result & ","
			End if
				result = result & "{"		
				result = result & """group_dn"": """ & JsEncode(group.dn) & ""","
				result = result & """group_name"": """ & JsEncode(group_name) & """"			
				result = result & "}"	
				id = id + 1
		'End If
	Next
	
	result = result & "]"
	
	Response.Write result
	Response.end
End If


'===================================================================
'========================== Search Groups ==========================
'===================================================================
If Request("get_groups") <> "" Then
	Dim id : id = 0
	If Request("keyword") <> "" Then
		strFilter = "(&("&strAttrClass&"="&strGroupClass&")"&strGroupFilter&"("&strGroupName&"=*"&EscapeCN(Request("keyword"))&"*))"
		strAttribs = strGroupName
		groupsQueryResult = LDAPQuery.execQuery(strLogin, strPassword, strDomain&strPort, "", "subtree", strFilter, strAttribs, secure, "")
		parseLdapError LDAPQuery.getLastError()
		If Ubound(groupsQueryResult) >= 0 Then
%>
	<br/>
	<button name="add_groups" value="Add" onclick="myadd_groups(); return false;">Add</button>
	<table width="40%" class="data_table" cellspacing="0" cellpadding="3">
		<tr class="data_table_title">
			<td width="10px"><input type="checkbox" id="select_all_found" onclick="selectAllGroups()"/></td>
			<td width="100%" class="table_title"><label for="select_all_found">Group name</label></td>
		</tr>
<%
		End If
		For Each group In groupsQueryResult
			'If isInOUs(group.dn) Then
				group_name = HTMLEncode(getAttr(group(strGroupName)))
				Response.Write "<tr><td><input type='checkbox' name='groups' id='groups_"&id&"' value='"&HTMLEncode(group.dn)&"'/></td><td><label for='groups_"&id&"'>"&group_name&"</label>"
				Response.Write "<input type='hidden' name='groups_names' value='"&group_name&"'/></td></tr>"
				id = id + 1
			'End If
		Next
	End If
	If id=0 Then
		Response.Write "No results. Please try again.<br>"
	Else
%>
	</table>
	<button name="add_groups" value="Add" onclick="myadd_groups(); return false;">Add</button>
<%
	End If
	Response.End
End If

'===================================================================
'============================= Get Tree ============================
'===================================================================
If Request("get_tree") <> "" Then
	Dim strBaseDN : strBaseDN = Request("ou")
	Dim first : first = True
	If strBaseDN = "" Then
		strFilter = "(&("&strAttrClass&"="&strDomainClass&")"&strDomainFilter&")"
		strAttribs = strDomainName
		DomainsQueryResult = LDAPQuery.execQuery(strLogin, strPassword, strDomain&strPort, strBaseDN, "subtree", strFilter, strAttribs, secure, "")
		If LDAPQuery.getLastError <> 0 Then
			%>
			[]
			<%
			Response.End
		End If
	if Request("org") = "1" then
%>
[{
	"property":
	{
		"name":"Organization",
		"id":"",
		"loadable": true
	},
	"state" :
	{
		"open": true
	},
	"type":"organization",
	"children":
<% end if %>
	[
<%
		Dim domainName
		For Each domain In DomainsQueryResult
			If Not first Then
				Response.Write ","
			End If
			domainName = domain.dn
			domainName = Mid(domainName, 4)
			domainName = Replace(domainName, ",dc=", ".", 1, -1, 1)
%>
		{
			"property":
			{
				"name":"<%=ScriptEncode(domainName) %>",
				"id":"<%=ScriptEncode(domain.dn) %>",
				"loadable":true
			},
			"type":"domain",
			"children":[]
		}
<%
			first = False
		Next
%>
	]
<% if Request("org") = "1" then %>
}]
<% end if
	Else
		strFilter = "(&("&strAttrClass&"="&strOuClass&")"&strOuFilter&")"
		strAttribs = strOuName
		OUsQueryResult = LDAPQuery.execQuery(strLogin, strPassword, strDomain&strPort, strBaseDN, "onelevel", strFilter, strAttribs, secure, "")
		If LDAPQuery.getLastError <> 0 Then
			%>
			[]
			<%
			Response.End
		End If
%>
		[

<%
		For Each OU In OUsQueryResult
			If Not first Then
				Response.Write ","
			End If
%>
			{
				"property":
				{
					"name":"<%=ScriptEncode(getAttr(OU(strOuName))) %>",
			<% If True Then %>
					"loadable":true,
			<% End If %>
					"id":"<%=ScriptEncode(OU.dn) %>"
				},
				"type":"ou",
				"children":[]
			}
<%
			first = False
		Next
%>
		]
<%
	End If
	Response.End
End If
'===================================================================
'===================================================================

syncAD "" 

%>
<!-- #include file="db_conn_close.asp" -->