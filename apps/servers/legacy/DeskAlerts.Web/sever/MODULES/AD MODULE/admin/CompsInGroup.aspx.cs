﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DeskAlerts.Server.Utils;
using DeskAlerts.Server.Utils.Helpers;
using DeskAlertsDotNet.Parameters;
using DeskAlertsDotNet.DataBase;

namespace DeskAlertsDotNet.Pages
{
    using Resources;

    public partial class CompsInGroup : DeskAlertsBaseListPage
    {

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            compName.Text = GetSortLink(resources.LNG_COMPUTER_NAME, "name", Request["sortBy"]);
            domain.Text = GetSortLink(resources.LNG_DOMAIN, "domains.name", Request["sortBy"]);
            online.Text = GetSortLink(resources.LNG_ONLINE, "last_request", Request["sortBy"]);
            lastActivity.Text = GetSortLink(resources.LNG_LAST_ACTIVITY, "last_request", Request["sortBy"]);
            actionsCollumn.Text = resources.LNG_ACTION;
            int cnt = this.Content.Count < (Offset + Limit) ? this.Content.Count : Offset + Limit;
            for (int i = Offset; i < cnt; i++)
            {
                DataRow row = this.Content.GetRow(i);
                TableRow tableRow = new TableRow();
                CheckBox chb = GetCheckBoxForDelete(row);
                TableCell checkBoxCell = new TableCell();
                checkBoxCell.HorizontalAlign = HorizontalAlign.Center;
                checkBoxCell.Controls.Add(chb);
                tableRow.Cells.Add(checkBoxCell);
                TableCell compNameCell = new TableCell();
                compNameCell.Text = row.GetString("name");
                tableRow.Cells.Add(compNameCell);
                object domainNameStr = dbMgr.GetScalarByQuery("SELECT name FROM domains WHERE id = " + row.GetString("domain_id"));
                TableCell domainCell = new TableCell();
                domainCell.Text = domainNameStr == null ? "" : domainNameStr.ToString();
                domainCell.HorizontalAlign = HorizontalAlign.Center;
                tableRow.Cells.Add(domainCell);
                var lastRequest = row.GetDateTime("last_request");
                var isOnline = DeskAlertsUtils.IsUserOnline(lastRequest, row.GetInt("next_request"));
                var statusImg = new Image
                {
                    Width = 11,
                    Height = 11,
                    ImageUrl = isOnline ? "../images/online.gif" : "../images/offline.gif",
                    AlternateText = isOnline ? resources.LNG_ONLINE : resources.LNG_OFFLINE
                };

                var statusCell = new TableCell();
                statusCell.Controls.Add(statusImg);
                statusCell.HorizontalAlign = HorizontalAlign.Center;
                tableRow.Cells.Add(statusCell);
                var lastActivityCell = new TableCell
                {
                    Text = DeskAlertsUtils.IsNotEmptyDateTime(lastRequest)
                        ? DateTimeConverter.ToUiDateTime(lastRequest)
                        : string.Empty
                };
                tableRow.Cells.Add(lastActivityCell);
                LinkButton previewButton = GetActionButton(row.GetString("id"), "images/action_icons/preview.png", "LNG_VIEW_USER_DETAILS", delegate { ScriptManager.RegisterClientScriptBlock(this, GetType(), "ORG_USERS_PREVIEW_" + row.GetString("id"), "showPreview(" + row.GetString("id") + ")", true); });
                var actionsCell = new TableCell {HorizontalAlign = HorizontalAlign.Center};
                actionsCell.Controls.Add(previewButton);
                tableRow.Cells.Add(actionsCell);
                contentTable.Rows.Add(tableRow);
            }
        }

        protected override string[] Collumns
        {
            get { return new string[] { }; }
        }

        protected override string ContentName
        {
            get
            {
                return resources.LNG_GROUPS;
                //return base.ContentName;
            }
        }
        protected override string DataTable
        {
            get { return ""; }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl NoRowsDiv
        {
            get { return NoRows; }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl TableDiv
        {
            get { return mainTableDiv; }
        }

        protected override string DefaultOrderCollumn
        {
            get { return "name"; }
        }

        protected override string CustomQuery
        {
            get
            {
                var groupId = Request["id"];
                var ouCompsIds = $@"(SELECT oc.Id_comp AS id
                    FROM OU_comp oc 
                    INNER JOIN ou_groups og ON og.ou_id = oc.Id_OU
                    WHERE og.group_id = {groupId})";

                string query = $@"SELECT DISTINCT c.id,
                        name, 
                        next_request, 
                        domain_id, 
                        reg_date, 
                        last_date, 
                        convert(varchar ,last_request) as last_request1, 
                        last_request 
                    FROM computers as c 
                    LEFT JOIN computers_groups as cg ON cg.comp_id = c.id 
                    WHERE cg.group_id = {groupId} OR
                          c.id IN {ouCompsIds}";

                return query;
            }
        }

        protected override string CustomCountQuery
        {
            get
            {
                var groupId = Request["id"] ?? "null";
                var ouCompsIds = $@"(SELECT oc.Id_comp AS id
                    FROM OU_comp oc 
                    INNER JOIN ou_groups og ON og.ou_id = oc.Id_OU
                    WHERE og.group_id = {groupId})";

                string query = $@"SELECT COUNT(DISTINCT c.id) as mycnt 
                    FROM computers as c 
                    LEFT JOIN computers_groups as cg ON cg.comp_id = c.id 
                    WHERE cg.group_id= {groupId} OR
                          c.id IN {ouCompsIds}";

                return query;
            }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl TopPaging
        {
            get
            {
                return topPaging;
            }
        }
        protected override System.Web.UI.HtmlControls.HtmlGenericControl BottomPaging
        {
            get
            {
                return bottomPaging;
            }
        }
    }
}
