<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="recurrence_process.asp" -->

<!-- #include file="libs\\VbsJson.asp" -->
<script type="text/javascript" src="jscripts/runtime.js"></script>
<script type="text/javascript" src="jscripts/polyfill.min.js"></script>
<script language="JScript" src="jscripts/json2.js" runat="server"></script>
<script language="JScript" runat="server">

    function toJsObject(obj) {
        var form = new Object();
        for (var e = new Enumerator(Request.Form); !e.atEnd(); e.moveNext()) {
            var item = Request.Form(e.item());
            if (item.Count() > 1) {
                var arr = new Array()
                for (var i = 1; i <= item.Count(); i++)
                    arr.push(item.Item(i));
                form[e.item()] = arr;
            }
            else {
                form[e.item()] = item.Item(1);
            }
        }
        form["ip"] = "0";
        form["max_usn"] = 0;
        return form;
    }

    var form = toJsObject(Request.Form);
    var formStr = JSON.stringify(form);
    var JsConn;
</script>
<%
'check_session()

mifTreeFolder = "mif_tree"

Dim objNameSpace
g_group_index=0
sync_id = Request("id")
if sync_id = "" then sync_id = Request("sync_id")

strDomainChecked=""
strDisplayLogin="none"
strGroupSelectChecked1="checked"
strGroupSelectChecked2=""

strADSelectChecked1="checked"
strADSelectChecked2=""
strADSelectChecked3=""
strDisplay="none"

if(Request("offset") <> "") then 
	offset = clng(Request("offset"))
else 
	offset = 0
end if	

if(Request("sortby") <> "") then 
	sortby = Request("sortby")
else 
	sortby = "name"
end if

patternType = ""
numberDays = 0
numberWeeks = 0
weekDays = ""
monthDay = 0
numberMonths = 0
weekdayPlace = 0
weekdayDay = 0
monthVal = 0
dayly_selector = ""
monthly_selector = ""
yearly_selector = ""
endDateType = ""
intAlertOccurence = 0

%>	
<script  type='text/javascript' language="javascript" runat="server">

    var sql = ""
    var syncData;
    var auto_sync = "";
    var interval_sync = "";
    var domain = "";
    var adLogin = "";
    var adPassword = "";
    var port = "389";
    var secure = "";
    var ad_select = "";
    var groups_select = "";
    var db_remove = "1";
    var impDis = "";
    var impComp = "";
    var notImpPhones = "";
    var selectedOUs = "";
    var groups = "";
    var added_group = "";
    var groups_names = "";
    var recur_id = "";
    var start_time = "";
    var syncName = "";
    var max_usn = 0;
    var legacySync = "1";
    var usnChanged = "0";

    function getUsn(Conn) {
        var usnChangedSql = Conn.Execute("SELECT val FROM settings WHERE name = 'usnChanged'");
        if (!usnChangedSql.EOF) {
            var resultString = JSON.parse(usnChangedSql.Fields.Item('val').value);
            usnChanged = String(resultString);

        }
    }


    function getSynchData(Conn, sync_id) {
        if (sync_id != "") {
            var result = Conn.Execute("SELECT name, sync_data, start_time, recur_id FROM synchronizations WHERE id='" + sync_id.replace("'", "''") + "'");
            if (!result.EOF) {
                syncData = result.Fields.Item('sync_data').value;
                syncData = JSON.parse(syncData);
                auto_sync = syncData["auto_sync"];
                interval_sync = syncData["interval_sync"];
                domain = syncData["domain"];
                adLogin = syncData["adLogin"];
                adPassword = syncData["adPassword"];
                port = syncData["port"];
                secure = syncData["secure"];
                ad_select = syncData["ad_select"];
                groups_select = syncData["groups_select"];
                db_remove = syncData["db_remove"];
                impDis = syncData["impDis"];
                impComp = syncData["impComp"];
                max_usn = syncData["max_usn"];
                legacySync = syncData["legacySync"];
                //  notImpPhones = syncData["notImpPhones"];
                selectedOUs = JSON.stringify((typeof syncData["selectedOUs"] == "string") ? new Array(syncData["selectedOUs"]) : syncData["selectedOUs"]);
                added_group = JSON.stringify((typeof syncData["added_group"] == "string") ? new Array(syncData["added_group"]) : syncData["added_group"]);
                groups_names = JSON.stringify((typeof syncData["groups_names"] == "string") ? new Array(syncData["groups_names"]) : syncData["groups_names"]);
                groups = JSON.stringify((typeof syncData["groups"] == "string") ? new Array(syncData["groups"]) : syncData["groups"]);
                recur_id = result.Fields.Item('recur_id').value;
                start_time = result.Fields.Item('start_time').value;
                syncName = result.Fields.Item('name').value;
            }
            result.Close();
        }
    }

</script>
<%
getUsn Conn
if (Request.Form.Count > 0) then

	syncName = Request("sync_name")
	syncTime = Request("sync_start_time")
	
	patternType = Request("pattern")

	dayly_selector =Request("dayly_selector")
	if(dayly_selector="dayly_1") then
		numberDays = Clng(Request("number_days"))
	else
		numberDays = 1
	end if

	numberWeeks = Clng (Request("number_weeks"))
	weekDays = Request("weekday_days")
	monthDay = Clng(Request("month_day"))

	monthly_selector=Request("monthly_selector")
	if(monthly_selector="monthly_1") then
		numberMonths = Clng(Request("number_month_1"))
	else
		numberMonths = Clng(Request("number_month_2"))
	end if

	weekdayPlace = Clng(Request("weekday_place"))
	weekdayDay = Clng(Request("weekday_day"))

	yearly_selector = Request("yearly_selector")
	if(yearly_selector="yearly_1") then
		monthVal = Clng(Request("month_val_1"))
	else
		monthVal = Clng(Request("month_val_2"))
	end if

	'range
	'--------
	'from_date=GetDateFromString(Request("start_date"), Date)
	'to_date=GetDateFromString(Request("end_by"), "")
	'--------
	endDateType = Request("end_type")

	if(endDateType="end_after") then
		intAlertOccurence = Clng(Request("end_after"))
	end if

	'SQL = "DELETE FROM synchronizations WHERE id=" & sync_id &" AND pattern='c'"
	'Conn.Execute(SQL)
	if (sync_id <> "") then 
		'update
        Dim oVbsJson : Set oVbsJson = New VbsJson
        SQL = "SELECT sync_data FROM synchronizations WHERE id = '"&sync_id&"'"
        sqlSyncData = Conn.Execute(SQL)

        Dim oldSyncData : Set oldSyncData = oVbsJson.Decode(sqlSyncData("sync_data"))
        Dim newSyncData : Set newSyncData = oVbsJson.Decode(formStr)
        
        For Each key In oldSyncData.Keys
            If key = "domain" or key = "adLogin" or key = "adPassword" or key = "port" or key = "secure" or _ 
               key = "ad_select" or key = "groups_select" or key = "impDis" or key = "impComp" or _ 
               key = "selectedOUs" or key = "added_group" or key = "groups_names" or key = "groups" Then
                If newSyncData.Exists(key) Then
                    value1 = ""
                    value2 = ""

                    If IsArray(oldSyncData.Item(key)) Then
                        value1 = Join(oldSyncData.Item(key), ",")
                    Else 
                        value1 = oldSyncData.Item(key)
                    End If

                    If IsArray(newSyncData.Item(key)) Then
                        value2 = Join(newSyncData.Item(key), ",")
                    Else 
                        value2 = newSyncData.Item(key)
                    End If

                    If value1 <> value2 Then
                        newSyncData("ip") = "0"
	                    newSyncData("max_usn") = 0
                    Else
                        newSyncData("ip") = oldSyncData("ip")
	                    newSyncData("max_usn") = oldSyncData("max_usn")
                    End If
                Else 
                    newSyncData("ip") = "0"
	                newSyncData("max_usn") = 0
                End If
            End If
        Next 

        For Each key In newSyncData.Keys
            If key = "domain" or key = "adLogin" or key = "adPassword" or key = "port" or key = "secure" or _ 
               key = "ad_select" or key = "groups_select" or key = "impDis" or key = "impComp" or _ 
               key = "selectedOUs" or key = "added_group" or key = "groups_names" or key = "groups" Then
                If oldSyncData.Exists(key) Then
                    value1 = ""
                    value2 = ""

                    If IsArray(newSyncData.Item(key)) Then
                        value1 = Join(newSyncData.Item(key), ",")
                    Else 
                        value1 = newSyncData.Item(key)
                    End If

                    If IsArray(oldSyncData.Item(key)) Then
                        value2 = Join(oldSyncData.Item(key), ",")
                    Else 
                        value2 = oldSyncData.Item(key)
                    End If

                    If value1 <> value2 Then
                        newSyncData("ip") = "0"
	                    newSyncData("max_usn") = 0
                    Else
                        newSyncData("ip") = oldSyncData("ip")
	                    newSyncData("max_usn") = oldSyncData("max_usn")
                    End If
                Else 
                    newSyncData("ip") = "0"
	                newSyncData("max_usn") = 0
                End If
            End If
        Next 

	    formStr = oVbsJson.Encode(newSyncData)

        'TODO: Transfer check for missing row to "UpdateTables2.sql"
		SQL = "UPDATE synchronizations SET name = N'"&Replace(syncName, "'", "''")&"', sync_data = '"&Replace(formStr, "'", "''")&"', start_time = '"&Replace(syncTime, "'", "''")&"' WHERE id='"&sync_id&"'"_
            &" IF (SELECT count(*) FROM [recurrence] WHERE id = (SELECT recur_id FROM synchronizations WHERE id = '"&sync_id&"')) = 0"_
            &" BEGIN"_
            &"  INSERT [recurrence] (alert_id) VALUES (NULL)"_
            &"  DECLARE @new_id BIGINT SELECT @new_id = SCOPE_IDENTITY()"_
            &"  UPDATE [synchronizations] SET recur_id = @new_id WHERE id = '"&sync_id&"'"_
            &" END"_
			&" UPDATE recurrence SET pattern = '" & patternType & "', number_days = "&numberDays&", number_week = "&numberWeeks&", week_days = "&weekDaysToBits(weekDays)&"," _
			&"month_day = "&monthDay&", number_month = "&numberMonths&", weekday_place = "&weekdayPlace&", weekday_day = "&weekdayDay&", month_val = "&monthVal&", " _
			&"dayly_selector = '"&dayly_selector&"', monthly_selector = '"&monthly_selector&"', yearly_selector = '"&yearly_selector&"', end_type = '"&endDateType&"'," _
			&"occurences = "&intAlertOccurence&" FROM synchronizations WHERE synchronizations.id='" & sync_id & "' AND synchronizations.recur_id = recurrence.id"
	else
		'insert
		SQL = "INSERT INTO recurrence (alert_id, pattern, number_days, number_week, week_days, month_day, number_month, weekday_place, weekday_day, month_val," _
			& "dayly_selector, monthly_selector, yearly_selector, end_type, occurences) VALUES (NULL, '"&patternType&"', "&numberDays&", "&numberWeeks&", "  _
			&weekDaysToBits(weekDays)&", "&monthDay&", "&numberMonths&", "&weekdayPlace&", "&weekdayDay&", "&monthVal&", '"&dayly_selector&"', '"&monthly_selector&"', '" _
			&yearly_selector&"', '"&endDateType&"', "&intAlertOccurence&")"_
			&" DECLARE @new_id BIGINT SELECT @new_id = SCOPE_IDENTITY()"_
			&" INSERT INTO synchronizations (id, name, sync_data, start_time, recur_id) VALUES( newid(), N'"&Replace(syncName, "'", "''")&"','"&Replace(formStr, "'", "''")&"','"&Replace(syncTime, "'", "''")&"',@new_id)"
	end if
	Conn.Execute(SQL)
	Response.Redirect "Synchronizations.aspx" 
else
    if (sync_id <> "") then
	
		getSynchData Conn, sync_id
		strADSelectChecked1=""
		strADSelectChecked2=""
		strADSelectChecked3=""
		
		if (ad_select = "not") then
			strADSelectChecked3 = "checked"
		elseif (ad_select = "selected") then
			strADSelectChecked2 = "checked"
		else
			strADSelectChecked1 = "checked"
		end if
		
		strGroupSelectChecked1 = ""
		strGroupSelectChecked2 = ""
		
		if (groups_select = "selected") then
			strGroupSelectChecked2 = "checked"
		else
			strGroupSelectChecked1 = "checked"
		end if
		

        autoSync = ""
		if(recur_id<>"") then

			SQL = "SELECT pattern, dayly_selector, monthly_selector, yearly_selector, number_days, number_week, week_days, month_day, number_month, weekday_place, weekday_day, month_val, end_type, occurences  FROM recurrence WHERE id=" & Clng(recur_id)

			Set rs = Conn.Execute(SQL)
			
			if (Not rs.EOF) then 
				'get data fro db
               ' autoSync = "checked"
				pattern = rs("pattern")
			
                if pattern <> "" then
                    autoSync = "checked"
                end if

				if(rs("dayly_selector")<>"") then
					dayly_selector = rs("dayly_selector")
				end if
				if(rs("monthly_selector")<>"") then
					monthly_selector = rs("monthly_selector")
				end if
				if(rs("yearly_selector")<>"") then
					yearly_selector = rs("yearly_selector")
				end if
				if(rs("number_days")<>0) then
					number_days = rs("number_days")
				end if
				if(rs("number_week")<>0) then
					number_weeks = rs("number_week")
				end if
				if(rs("week_days")<>0) then
					week_days = rs("week_days")
				end if
				if(rs("month_day")<>0) then
					month_day = rs("month_day")
				end if
				if(rs("number_month")<>0) then
					number_month = rs("number_month")
				end if
				weekday_place = rs("weekday_place")
				if(rs("weekday_day")<>0) then
					weekday_day = rs("weekday_day")
				end if
				if(rs("month_val")<>0) then
					month_val = rs("month_val")
				end if
					if(rs("end_type")<>"") then
						end_type = rs("end_type")
				end if
				if(rs("occurences")<>0) then
					occurences = rs("occurences")
				end if
			end if
		end if
	end if
end if
	


%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>AD Synchronizing Configuration</title>
<link href="css/style9.css" rel="stylesheet" type="text/css">
<style>
.mif-tree-wrapper{height:200px}

ol {
	list-style: none;
}

</style>
    <link rel="stylesheet" href="<%Response.write mifTreeFolder%>/style.css" type="text/css" />
    <link href="css/jquery/jquery_timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css">
    <link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
    <!---->
    <script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/mootools.js"></script>
    <script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.js"></script>
    <script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Node.js"></script>
    <script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Hover.js"></script>
    <script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Selection.js"></script>
    <script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Load.js"></script>
    <script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Draw.js"></script>
    <script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.KeyNav.js"></script>
    <script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Sort.js"></script>
    <script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Transform.js"></script>
    <script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Drag.js"></script>
    <script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Drag.Element.js"></script>
    <script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Checkbox.js"></script>
    <script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Rename.js"></script>
    <script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.Row.js"></script>
    <script type="text/javascript" src="<%Response.write mifTreeFolder%>/mootools/Mif.Tree.mootools-patch.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/json2.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/date.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery_timepicker/jquery-ui-timepicker-addon.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/jquery/datepicker_fix.js"></script>
    <script language="javascript" type="text/javascript" src="functions.js"></script>
    <script language="javascript" type="text/javascript" src="base64.js"></script>
    <script language="javascript" type="text/javascript">

        $.noConflict();
        function createInput(name) {
            var elem;
            if (window.ActiveXObject) {
                try {
                    elem = document.createElement('<input name="' + name + '">');
                } catch (e) { }
            }
            if (!elem) {
                elem = document.createElement('input');
                elem.name = name;
            }
            return elem;
        }

        function handleKeyPress(e) {
            var key = e.keyCode || e.which;
            if (key == 13) {
                return false;
            }
        }

        var function_name = "";

        var tree;

        var selectedOUs = <%if selectedOUs = "" then Response.Write "[]" else Response.Write selectedOUs %>;

        var GroupObj = <%if added_group = "" then Response.Write "[]" else Response.Write added_group %>;
        var GroupNames = [];

        var ADJson = "";


        Mif.Tree.Node.implement({
            initialize: function (structure, options) {
                $extend(this, structure);
                this.children = [];
                this.type = options.type || this.tree.dfltType;
                this.property = options.property;
                this.data = options.data;
                this.state = $extend($unlink(this.tree.dfltState), options.state);
                this.$calculate();
                this.UID = Mif.Tree.Node.UID++;
                Mif.Tree.Nodes[this.UID] = this;
            }

        });

        Mif.Tree.implement({
            getNodeById: function (id) {
                return Mif.Tree.Nodes[id];
            }
        });


        Mif.Tree.Node.implement({
            refreshNode: function () {
                this.reloadChildren().toggle(true);
            }

        });

        Mif.Tree.Draw.getHTML = function (node, html) {
            var prefix = node.tree.DOMidPrefix;
            if ($defined(node.state.checked)) {
                if (!node.hasCheckbox) node.state.checked = 'nochecked';
                var checkbox = '<span class="mif-tree-checkbox mif-tree-node-' + node.state.checked + '" uid="' + node.UID + '">' + Mif.Tree.Draw.zeroSpace + '</span>';
            } else {
                var checkbox = '';
            }
            html = html || [];
            html.push(

                '<div class="mif-tree-node ', (node.isLast() ? 'mif-tree-node-last' : ''), '" id="', prefix, node.UID, '">',
                '<span class="mif-tree-node-wrapper ', node.cls, '" uid="', node.UID, '">',
                '<span class="mif-tree-gadjet mif-tree-gadjet-', node.getGadjetType(), '" uid="', node.UID, '">', Mif.Tree.Draw.zeroSpace, '</span>',
                checkbox,
                '<span class="mif-tree-icon ', node.closeIcon, '" style="padding-right:18px; width:20px;" uid="', node.UID, '">', Mif.Tree.Draw.zeroSpace, '</span>',
                '<span class="mif-tree-icon" style="background:url(images/refresh.gif) no-repeat center; padding-right:16px; width:16px; cursor:pointer" uid="', node.UID, '" onclick="tree.getNodeById(\'', node.UID, '\').refreshNode()"></span>',
                '<span class="mif-tree-name" uid="', node.UID, '">', node.name, '</span>',
                '</span>',
                '<div class="mif-tree-children" style="display:none"></div>',
                '</div>'
            );
            return html;
        };

        Mif.Tree.Node.implement({
            reloadChildren: function () {
                this.state.loaded = false;
                this.state.open = false;
                this.state.loadable = true;
                this.old_children = this.children;
                this.children = [];
                this.$draw = false;
                this.tree.$getIndex();
                Mif.Tree.Draw.update(this);
                return this;
            }

        });

        function InitTree() {
            tree = new Mif.Tree({
                container: $("mydiv"),
                forest: false,
                initialize: function initialize() {
                    this.initCheckbox("deps");
                    new Mif.Tree.KeyNav(this);
                },
                types: {
                    organization: {
                        openIcon: "mif-tree-organization-icon",
                        closeIcon: "mif-tree-organization-icon"
                    },
                    domain: {
                        openIcon: "mif-tree-domain-icon",
                        closeIcon: "mif-tree-domain-icon"
                    },
                    loader: {
                        openIcon: "mif-tree-loader-icon",
                        closeIcon: "mif-tree-loader-icon"
                    },
                    ou: {
                        openIcon: "mif-tree-ou-icon",
                        closeIcon: "mif-tree-ou-icon"
                    }
                },
                dfltType: "organization"
            });

            function mergeNodes(newNodes, oldNodes) {
                var resultNodes = [];

                for (var i = 0; i < newNodes.length; i++) {
                    var found = false;
                    for (var j = 0; j < oldNodes.length; j++) {
                        if (newNodes[i].property.id == oldNodes[j].property.id) {
                            found = true;
                            break;
                        }
                    }
                    if (found) {
                        oldNodes[j].state.loaded = false;
                        resultNodes.push(oldNodes[j]);
                    } else {
                        resultNodes.push(newNodes[i]);
                    }
                }
                return resultNodes;
            }

            tree.addEvent("loadChildren", function (parent) {
                if (parent) {
                    if (parent.old_children && parent.children) {
                        var mergedChildrens = mergeNodes(parent.children, parent.old_children);
                        parent.children = mergedChildrens;

                        for (var i = 0; i < parent.children.length; i++) {
                            if (
                                parent.children[i].children &&
                                parent.children[i].children.length > 0
                            ) {
                                parent.children[i].reloadChildren().toggle(true);
                            }
                        }
                    }
                    if (!parent.children) {
                        parent.children = [];
                    }
                }
            });

            tree.loadOptions = function (node) {
                if (node != undefined && node.id != undefined) {
                    return makeRequest(
                        "POST",
                        "ad_action.asp?get_tree=1&org=1&ou=" + encodeURIComponent(node.id),
                        getCredentials()
                    )
                        .then(function (response) {
                            return { json: JSON.parse(response) };
                        })
                        .catch(function (err) {
                            console.error("Error:", err);
                        });
                }
            };

            if (ADJson) {
                var tmpJSON = new Array(jQuery.extend(true, {}, ADJson)[0]);
                tree.load({
                    json: tmpJSON
                });
            } else {
                makeRequest("POST", "ad_action.asp?get_tree=1&org=1", getCredentials())
                    .then(function (response) {
                        tree.load({
                            json: JSON.parse(response)
                        });
                    })
                    .catch(function (err) {
                        console.error("Error:", err.statusText);
                    });
            }
        }

        function getCredentials() {
            var domain = document.getElementById('domain').value;
            var port = document.getElementById('port').value;
            var adLogin = trim(document.getElementById('adLogin').value);
            var adPassword = (document.getElementById('adPassword_hidden').value == "") ? encode64(trim(document.getElementById('adPassword').value)) : document.getElementById('adPassword_hidden').value;
            var adSecure = document.getElementById('secure').checked ? 1 : 0;
            return "domain=" + domain + "&port=" + port + "&adLogin=" + adLogin + "&adPassword=" + adPassword + "&secure=" + adSecure;
        }

        function makeRequest(method, url, data) {
            return new Promise(function (resolve, reject) {
                var xhr = new XMLHttpRequest();
                xhr.open(method, url)
                xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded')
                xhr.onload = function () {
                    if (this.status >= 200 && this.status < 300) {
                        resolve(xhr.response)
                    } else {
                        reject({
                            status: this.status,
                            statusText: xhr.statusText
                        });
                    }
                }

                xhr.onerror = function () {
                    reject({
                        status: this.status,
                        statusText: xhr.statusText
                    });
                }

                xhr.send(data)
            })
        }

        function getCheckedOus() {
            if (tree && tree.root) {
                var nodes = new Array();
                tree.root.recursive
                    (
                    function () {
                        if (this.state.checked == "checked") {
                            var temp = new Object();
                            temp.id = this.property.id;
                            temp.type = this.type;
                            nodes.push(temp);
                        }
                    }
                    );
                return nodes;

            }
            return null;
        }


        function send_to_users() {
            var selectedOUs_div = document.getElementById("selectedOUs_div");
            selectedOUs_div.innerHTML = "";
            var res = getCheckedOus();
            if (res) {
                for (var i = 0; i < res.length; i++) {
                    if (res[i].type != "organization") {
                        var input = createInput("selectedOUs");
                        input.type = "hidden";
                        input.value = res[i].id.toString();
                        selectedOUs_div.appendChild(input);
                    }
                }
            }
            return true;
        }

        function ADCheck(f_name) {
            jQuery("#tree_loader").show();
            function_name = f_name;
            setTimeout(function () {
                var domain = document.getElementById('domain').value;
                var port = document.getElementById('port').value;
                var adLogin = trim(document.getElementById('adLogin').value);
                if (domain && adLogin) {
                    var adPassword = (document.getElementById('adPassword_hidden').value == "") ? encode64(trim(document.getElementById('adPassword').value)) : document.getElementById('adPassword_hidden').value;
                    var adSecure = document.getElementById('secure').checked ? '1' : '0';
                    getFile('ad_action.asp', '1', encodeURIComponent(domain), encodeURIComponent(port), encodeURIComponent(adLogin), adPassword, adSecure);
                }
                else {
                    if (function_name == "show") { document.getElementById("ad_select1").checked = true; }
                    if (function_name == "hide") { document.getElementById("ad_select2").checked = true; }
                    jQuery("#tree_loader").hide();
                    alert('You should enter domain name and login');
                }
            }, 1);
        }

        function ShowAD() {
            jQuery("#tree_loader").hide();
            document.getElementById('mydiv').innerHTML = "";
            document.getElementById('mydiv').style.height = "200px";
            InitTree();
        }

        function HideAD() {
            jQuery("#tree_loader").hide();
            document.getElementById('mydiv').innerHTML = "";
            document.getElementById('mydiv').style.height = "";
        }

        function trim(str) {
            return str.replace(/^\s+|\s+$/g, "");
        }

        function my_sub() {
            function checkTime($) {

                if ($("#auto_sync").is(":checked")) {
                    var uiTimetimeFormat = getUiTimeFormat(settingsTimeFormat);
                    var startTime = $.trim($("#sync_time").val());


                    if (!startTime) {
                        alert("Please enter start date!");
                        return false;

                    }

                    if (!isDate(startTime, uiTimetimeFormat)) {
                        alert("Please enter correct start date!");
                        return false;
                    }
                    else {
                        if (startTime != "") {
                            startTime = new Date(getDateFromFormat(startTime, uiTimetimeFormat));
                            $("#sync_start_time").val(formatDate(startTime, saveDbTimeFormat));
                        }
                    }

                }
                return true;
            }

            if (!checkTime(jQuery)) return false;

            send_to_users();
            if (document.getElementById("groups_select2").checked == true) {
                if (!document.getElementsByName("added_group").length) {
                    alert("You should select groups to proceed with this option.");
                    return false;
                }
            }
            if (document.getElementById("adPassword_hidden").value == "") {
                var pass = document.getElementById("adPassword").value;
                document.getElementById("adPassword_hidden").value = encode64(pass);
            }

            if (document.getElementById("sync_name").value == "") { alert("You should specify a name for this synchronization before proceeding"); return false; }

            ADCheck("save");
            return false;
        }

        function Search() {
            var keyword = document.getElementById('search_group').value;
            var domain = document.getElementById('domain').value;
            var port = document.getElementById('port').value;
            var adLogin = trim(document.getElementById('adLogin').value);
            var adPassword = (document.getElementById('adPassword_hidden').value == "") ? encode64(trim(document.getElementById('adPassword').value)) : document.getElementById('adPassword_hidden').value;
            var adSecure = document.getElementById('secure').checked ? 1 : 0;
            if (keyword.length < 2) {
                alert('You should enter at least two symbols.');
            }
            else {
                if (document.getElementById("groups_select2").checked == true) {
                    send_to_users();
                }
                getFile2('ad_action.asp', 'get_groups=1&domain=' + encodeURIComponent(domain) + '&port=' + encodeURIComponent(port) + '&adLogin=' + encodeURIComponent(adLogin) + '&adPassword=' + adPassword + '&keyword=' + encodeURIComponent(keyword) + '&secure=' + adSecure);
            }
        }

        function setPort() {
            var port = document.getElementById('port').value;
            var secure = document.getElementById('secure').checked;
            if (port == 389 || port == 636) {
                document.getElementById('port').value = secure ? 636 : 389;
            }
        }

        var g_group_index = 0;

        function getFile(url, check_settings, domain, port, adLogin, adPassword, secure) {
            var xmlhttp;
            if (window.XMLHttpRequest) { // code for Mozilla, Safari, etc
                xmlhttp = new XMLHttpRequest();
            } else if (window.ActiveXObject) { //IE
                xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
            }

            var data = "domain=" + domain + "&port=" + port + "&adLogin=" + adLogin + "&adPassword=" + adPassword + "&secure=" + secure
            if (typeof (xmlhttp) == 'object') {
                xmlhttp.onreadystatechange = function () { xmlhttpResultsCheck(xmlhttp) };
                xmlhttp.open('POST', url + "?check_settings=1", false);
                xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                xmlhttp.send(data);
            } else {
                alert('Your browser is not remote scripting enabled. You can not run this example.');
            }
        }

        function getFile2(pURL, params) {
            document.getElementById('found_groups').innerHTML = "Loading, please wait...";

            if (document.getElementById("ad_select2").checked) {
                params += "&ad_select=selected";
                var selectedOUs = document.getElementsByName("selectedOUs");
                for (var i = 0; i < selectedOUs.length; i++) {
                    params += "&selectedOUs=" + encodeURIComponent(selectedOUs[i].value);
                }
            }

            var xmlhttp;
            if (window.XMLHttpRequest) { // code for Mozilla, Safari, etc
                xmlhttp = new XMLHttpRequest();
            } else if (window.ActiveXObject) { //IE
                xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
            }

            xmlhttp.open("POST", pURL, true);

            //Send the proper header information along with the request
            xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xmlhttp.setRequestHeader("Content-length", params.length);
            //xmlhttp.setRequestHeader("Connection", "close");

            xmlhttp.onreadystatechange = function () { xmlhttpResults(xmlhttp) };
            //alert(params);
            xmlhttp.send(params);
        }

        // function to handle asynchronous call
        function xmlhttpResults(xmlhttp) {
            if (xmlhttp.readyState == 4) {
                if (xmlhttp.status == 200) {
                    document.getElementById('found_groups').innerHTML = xmlhttp.responseText;
                }
            }
        }

        function xmlhttpResultsCheck(xmlhttp) {
            if (xmlhttp.readyState == 4) {
                if (xmlhttp.status == 200) {
                    var str = xmlhttp.responseText;
                    if (str == "No") {
                        if (function_name == "show") { document.getElementById("ad_select1").checked = true; }
                        if (function_name == "hide") { document.getElementById("ad_select2").checked = true; }
                        jQuery("#tree_loader").hide();
                        alert("Please enter correct data for connecting AD.");
                    }
                    else {
                        if (function_name == "show") { ShowAD(); }
                        if (function_name == "hide") { HideAD(); }
                        if (function_name == "save") { document.forms[0].submit(); }
                    }
                }
            }
        }

        function checkInput() {
            var adLogin;
            var adPassword;
            checked = 1;
            adLogin = document.getElementById('adLogin').value;
            var adPassword = (document.getElementById('adPassword_hidden').value == "") ? encode64(trim(document.getElementById('adPassword').value)) : document.getElementById('adPassword_hidden').value;
            if (adLogin.length == 0 || adPassword.length == 0) {
                checked = 0;
            }
            if (document.getElementById("ad_select2").checked == true) {
                send_to_users();
            }
            if (checked == 1) {
                return true;
            }
            else {
                alert("Please enter login/password to access selected domains.");
                return false;
            }
        }

        function checkShow(domain) {
            if (domain.checked == true) {
                ShowLoginForm();
            }
            else {
                HideLoginForm();
            }
        }

        function ShowLoginForm() {
            document.getElementById('loginForm').style.display = '';
        }

        function HideLoginForm() {
            document.getElementById('loginForm').style.display = 'none';
        }

        function Show() {
            document.getElementById('show_groups').style.display = '';
        }

        function Hide() {
            document.getElementById('show_groups').style.display = 'none';
        }

        function myadd_groups(init) {
            if (init) {
                var GroupObj = window.GroupObj;
                var GroupNames = window.GroupNames;
                var AddedGroupObj = [];
            }
            else {
                var GroupObj = document.getElementsByName("groups");
                var GroupNames = document.getElementsByName("groups_names");
                var AddedGroupObj = document.getElementsByName("added_group");
            }

            for (var i = 0; i < GroupObj.length; i++) {
                if (GroupObj[i].checked) {
                    var found = false;
                    for (var j = 0; j < AddedGroupObj.length; j++) {
                        if (AddedGroupObj[j].value == GroupObj[i].value) {
                            found = true;
                            break;
                        }
                    }
                    if (!found) {
                        g_group_index++;
                        addRowToTable(g_group_index, GroupNames[i].value, GroupObj[i].value);
                    }
                }
            }
        }

        function selectAllGroups() {
            var status = document.getElementById('select_all_found').checked;
            var field = document.getElementsByName('groups');
            for (i = 0; i < field.length; i++)
                field[i].checked = status;
        }

        function selectAllObjects() {
            var status = document.getElementById('select_all').checked;
            var field = document.getElementsByName('objects');
            for (i = 0; i < field.length; i++)
                field[i].checked = status;
        }

        function addRowToTable(idNumber, groupName, groupDN) {
            var tbl = document.getElementById('table_added_groups');
            var lastRow = tbl.rows.length;
            // if there's no header row in the table, then iteration = lastRow + 1
            var iteration = lastRow;
            var row = tbl.insertRow(lastRow);

            // left cell
            var cellLeft = row.insertCell(0);
            var input = createInput("objects");
            input.id = "tr_check" + idNumber;
            input.setAttribute("type", "checkbox");
            cellLeft.appendChild(input);

            // right cell
            var cellRight = row.insertCell(1);
            input = createInput("added_group");
            input.setAttribute("type", "hidden");
            input.value = groupDN;
            cellRight.appendChild(input);
            var label = document.createElement("label");
            label.setAttribute("for", "tr_check" + idNumber);
            label.setAttribute("name", "added_group");
            label.value = groupDN;
            label.appendChild(document.createTextNode(groupName));
            cellRight.appendChild(label);
        }

         function check_groups() {
            if (document.getElementById("groups_select1").checked) {
                var tbl = document.getElementById('table_added_groups');
                var field = document.getElementsByName('objects');
                var length = field.length;
                var i = 0;
                while (length > 0 && i < length) {
                    tbl.deleteRow(i + 1);
                    length--;
                }
            }
        }

        function deleteGroups() {
            var tbl = document.getElementById('table_added_groups');
            var field = document.getElementsByName('objects');
            var length = field.length;
            var i = 0;
            while (length > 0 && i < length) {
                if (field[i].checked) {
                    tbl.deleteRow(i + 1);
                    length--;
                }
                else
                    i++;
            }
            document.getElementById('select_all').checked = false;
        }

        new Promise(function (resolve, reject) {

        })

    </script>

</head>

<body style="margin:0px" class="body" onkeypress="return handleKeyPress(event);">
	<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%" class="body">
		<tr>
			<td valign="top">
				<table width="100%" cellspacing="0" cellpadding="0" class="main_table" class="body">
					<tr>
						<td width=100% height=31 class="main_table_title"><img src="images/menu_icons/synchronizations.png" alt="" width="20" height="20" border="0" style="padding-left:7px">
		<a href="#" class="header_title" style="position:absolute;top:15px">AD Synchronizing</a></td>
					</tr>
					<tr>
						<td class="main_table_body">
							<form name="mainform" id="mainform" method="post" action="ad_action.asp">
							<input type="hidden" name="id" value="<%=sync_id%>"/>
							<input type="hidden" name="sync_id" value="<%=sync_id%>"/>
							<div style="margin:10px">
								<table>
									<tr>
										<td>
											<label for="sync_name">Name</label>
										</td>
										<td>
											<input id="sync_name" type="text" name="sync_name" value="<%=syncName%>"></input>
										</td>
									</tr>
								</table>
							</div>
							<div style="margin:10px;">
							Please note that the synchronizing process may take some time (up to severalhours) depending on the number of groups selected and number of users in domains
								<div id="loginForm">
									<table>
										<tr>
											<td>Domain: </td>
											<td><input type="text" value="<%=domain%>" name="domain" id="domain"/></td>
											<td><font size="1"> e.g. "your.domain.net" </font></td>
											<td>Port: </td>
											<td><input type="text" value="<%=port%>" name="port" id="port" style="width:50px;"/></td>
										</tr>
										<tr>
											<td>Login: </td>
											<td><input type="text" name="adLogin" value="<%=adLogin%>" id="adLogin"/></td>
										</tr>
										<tr>
											<td>Password:</td>
											<td>
												<input style="font-family: sans-serif;" type="password" id="adPassword" value="<%=adPassword%>"/>
												<input type="hidden" name="adPassword" id="adPassword_hidden" value="<%=adPassword%>"/>
											</td>
										</tr>
									</table>
								</div>
								<br/>
								<input type="checkbox" name="secure" id="secure" value="1" onclick="setPort()" <%if (secure = "1") then Response.write "checked" end if%>/><label for="secure">Use secure LDAP connection</label><img src="images/help.png" title="If you require that synchronization is done over secure connection please use this settings. NOTE: secure LDAP access need to be enabled on your directory server."/><br/>

								<hr/>
								<input type="radio" id="ad_select1" name="ad_select" value="all" <%=strADSelectChecked1%> onclick="function_name = 'hide'; HideAD()"><label for='ad_select1'>Synchronize all Organizational Units (users and groups will be selected from whole Domain tree)</label><br/>
								<input type="radio" id="ad_select2" name="ad_select" value="selected" <%=strADSelectChecked2%> onclick="ADCheck('show')"><label for='ad_select2'>Synchronize selected Organizational Units (only users and groups from selected OU's will be synchronized)</label><br/>
								<input type="radio" id="ad_select3" name="ad_select" value="not" <%=strADSelectChecked3%> onclick="function_name = 'hide'; HideAD()"><label for='ad_select3'>Don't synchronize Organizational Units (users and groups will be synchronized from the domain without OU's)</label><br/><br/>		 
								<span id="tree_loader" class="mif-tree-icon mif-tree-loader-icon" style="width:20px;display:none;"></span>
								<div id="mydiv" height="200"></div>
								<br/><div>and</div><br/>
								
								<div id="selectedOUs_div"></div>

								<input type="radio" id="groups_select1" name="groups_select" value="all" <%=strGroupSelectChecked1%> onclick="Hide()"/>
								<label for='groups_select1'>
									Synchronize all groups
								</label><br/>
								
								<input type="radio" id="groups_select2" name="groups_select" value="selected" <%=strGroupSelectChecked2%> onclick="Show()"/>
								<label for='groups_select2'>
									Synchronize only selected groups and users from them
								</label><br/><br/>

								<div id="show_groups" style="display:<%=strDisplay%>">
									Groups:<br/>
									<table>
										<tr>
											<td>
												<input type="text" name="search_groups" id="search_group" />
											</td>
											<td>
												<button id="searchButton" name="search" value="Search" onclick="if(checkInput()) {Search();} return false;">Search</button><br/>
											</td>
										</tr>
									</table>
									<div id="found_groups">
									</div><br/>
									<br/><br/>
									Groups for synchronizing:<br/>
									<div id="added_groups">
										<table width="40%" height="100%" cellspacing="0" cellpadding="0">
											<tr>
												<td style="padding-bottom:10px;padding-top:5px;">
													<a class="delete_button" onclick="deleteGroups();"/>Delete</a>
												</td>
												<td style="padding-bottom:10px; text-align:right">
													<a id="refresh_groups_button" style="float:right;" >Refresh</a>
													<div id="groups_loader" style="display:none; float:right; height:15px; width:128px; background: url(images/loader.gif); margin-top:7px; margin-right:5px "></div>
												</td>
											</tr>
											<tr>
												<td colspan="2">
													<table id="table_added_groups" width="100%" height="100%" class="data_table" cellspacing="0" cellpadding="3">
														<tr class="data_table_title">
															<td width="10px"><input type="checkbox" id="select_all" onclick="selectAllObjects()"/></td>
															<td width="100%" class="table_title"><label for="select_all">Group name</label></td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
										<br/>
										<a class="delete_button" onclick="deleteGroups();"/>Delete</a>
									</div>
									<br/>
								</div>
								<hr/>
								<%
									Response.Write "<script type='text/javascript'>g_group_index="&g_group_index&";</script>"
								%>
								<br/>
								<input type="checkbox" name="db_remove" id="db_remove" value="1" <%if (db_remove = "1") then Response.write "checked" end if%>/><label for="db_remove">If unchecked will only add new items without removal&nbsp;<img src="images/help.png" title="If unchecked will only add new items without removal"/></label><br/>
								<input type="checkbox" name="impDis" id="impDis" value="1" <%if (impDis = "1") then Response.write "checked" end if%>/><label for="impDis">Import users with disabled accounts</label><br/>
								<input type="checkbox" name="impComp" id="impComp" value="1" <%if (impComp = "1") then Response.write "checked" end if%>/><label for="impComp">Import Computers</label><br/>
                                <input type="checkbox" name="legacySync" <%if (usnChanged = "0") then Response.write "style='display:none'" end if%> id="legacySync" value="1" <%if (legacySync = "1") then Response.write "checked" end if%>/><label <%if (usnChanged = "0") then Response.write "style='display:none'" end if%> for="legacySync">Use old synchronization</label><br/>
                                <input type="checkbox" style="display: none;" name="notImpPhones" id="notImpPhones" value="1" <%if (notImpPhones = "1") then Response.write "checked" end if%>/>
								<br/>
								<script type="text/javascript">

                                    var serverDate;
                                    var settingsDateFormat = "<%=GetDateFormat()%>";
                                    var settingsTimeFormat = "2";

                                    var responseDbFormat = "dd/MM/yyyy HH:mm:ss";
                                    var saveDbFormat = "dd/MM/yyyy HH:mm";
                                    var saveDbDateFormat = "dd/MM/yyyy";
                                    var saveDbTimeFormat = "HH:mm";

                                    (function ($) {


                                        function parseOUs(json) {
                                            if (json && json.length == 0) return;
                                            var OUs = json;

                                            var root = {
                                                "property":
                                                {
                                                    "name": "Organization",
                                                    "id": "",
                                                    "loadable": true
                                                },
                                                "state":
                                                {
                                                    "open": true
                                                },
                                                "type": "organization",
                                                "children": []
                                            }

                                            var tree = [];
                                            tree.push(root);

                                            function findById(children, id) {
                                                if (!children || children.length == 0) {
                                                    return [];
                                                }
                                                return $.grep(children, function (n, i) {
                                                    return n.property.id == id;
                                                });
                                            };


                                            function pushToTree(OU, domain) {
                                                var domains = findById(tree[0].children, domain.property.id)
                                                if (!domains || domains.length == 0) {
                                                    tree[0].children.push(domain);
                                                }
                                                else {
                                                    domain = domains[0];
                                                }

                                                if (OU) {
                                                    var OUs = findById(domain.children, OU.property.id)
                                                    if (!OUs || OUs.length == 0) {
                                                        domain.children.push(OU);
                                                    }
                                                    else {
                                                        var parentObj = domain;
                                                        while (OU) {
                                                            var OUs = findById(parentObj.children, OU.property.id)
                                                            if (!OUs || OUs.length == 0) {
                                                                parentObj.children.push(OU);
                                                                parentObj = OU;
                                                            }
                                                            else {
                                                                parentObj = OUs[0];
                                                            }

                                                            if (OU.children && OU.children.length > 0) {
                                                                OU = OU.children[0];
                                                            }
                                                            else {
                                                                OU = null;
                                                            }
                                                        }

                                                    }
                                                }
                                            }

                                            for (var i = 0; i < OUs.length; i++) {
                                                var idArray = OUs[i].split(",");
                                                var domain = {
                                                    "property":
                                                    {
                                                        "name": "",
                                                        "id": "",
                                                        "loadable": true
                                                    },
                                                    "state": {
                                                        "open": true,
                                                        "loaded": true
                                                    },
                                                    "type": "domain",
                                                    "children": []
                                                };

                                                var OU = null;

                                                for (var j = 0; j < idArray.length; j++) {
                                                    var leaf = {};
                                                    var leafArray = idArray[j].split("=");

                                                    if (leafArray[0] == "DC") {
                                                        if (!domain.property.id) {
                                                            domain.property.name += leafArray[1];
                                                            domain.property.id += idArray[j];
                                                        }
                                                        else {
                                                            domain.property.name += "." + leafArray[1];
                                                            domain.property.id += "," + idArray[j];
                                                        }

                                                    }
                                                    else if (leafArray[0] == "OU") {
                                                        var OUObj = {
                                                            "property":
                                                            {
                                                                "name": leafArray[1],
                                                                "loadable": true,
                                                                "id": ""
                                                            },
                                                            "state": {
                                                                "loaded": true,
                                                                "open": true
                                                            },
                                                            "type": "ou",
                                                            "children": []
                                                        }

                                                        for (var k = j; k < idArray.length; k++) {
                                                            if (!OUObj.property.id) {
                                                                OUObj.property.id += idArray[k];
                                                            }
                                                            else {
                                                                OUObj.property.id += "," + idArray[k];
                                                            }

                                                        }

                                                        if (OUObj.property.id == OUs[i]) {
                                                            OUObj.state.checked = "checked";
                                                        }

                                                        if (OU) {
                                                            OUObj.children = [];
                                                            OUObj.children.push(OU);
                                                        }

                                                        OU = OUObj;


                                                    }
                                                }
                                                if (!OU) domain.state.checked = "checked";
                                                pushToTree(OU, domain);
                                            }
                                            return tree;
                                        }

                                        ADJson = parseOUs(selectedOUs);

                                        function parseGroups() {
                                            for (var i = 0; i < GroupObj.length; i++) {
                                                GroupObj[i] = {
                                                    value: GroupObj[i],
                                                    checked: true
                                                };
                                            }
                                            for (var i = 0; i < GroupObj.length; i++) {
                                                GroupNames[i] = {
                                                    value: GroupObj[i].value.split(",")[0].split("=")[1],
                                                    checked: true
                                                };
                                            }
                                        }

                                        parseGroups();

                                        function makeWeekdaysSelector(weekday_checked) {
                                            var checked_arr = new Array();
                                            for (var i = 1; i <= 7; i++) {
                                                checked_arr[i] = "";
                                                if (i == weekday_checked) {
                                                    checked_arr[i] = "selected";
                                                }
                                            }


                                            var days_selector = "<select name='weekday_day'>" +
                                                "<option value='1' " + checked_arr[1] + ">Sunday</option>" +
                                                "<option value='2' " + checked_arr[2] + ">Monday</option>" +
                                                "<option value='3' " + checked_arr[3] + ">Tuesday</option>" +
                                                "<option value='4' " + checked_arr[4] + ">Wednesday</option>" +
                                                "<option value='5' " + checked_arr[5] + ">Thursday</option>" +
                                                "<option value='6' " + checked_arr[6] + ">Friday</option>" +
                                                "<option value='7' " + checked_arr[7] + ">Saturday</option>" +
                                                "</select>";
                                            return days_selector;
                                        }

                                        var days_checkboxes = "<input type='checkbox' name='weekday_days' value='Sun' <% response.write isWeekday(week_days, Sun) %> id='weekday_sunday'><label for='weekday_sunday'>Sunday</label>" +
                                            "<input type='checkbox' name='weekday_days' value='Mon' <% response.write isWeekday(week_days, Mon) %> id='weekday_monday'><label for='weekday_monday'>Monday</label>" +
                                            "<input type='checkbox' name='weekday_days' value='Tue' <% response.write isWeekday(week_days, Tue) %> id='weekday_tuesday'><label for='weekday_tuesday'>Tuesday</label> " +
                                            "<input type='checkbox' name='weekday_days' value='Wed' <% response.write isWeekday(week_days, Wed) %> id='weekday_wednesday'><label for='weekday_wednesday'>Wednesday</label><br>" +
                                            "<input type='checkbox' name='weekday_days' value='Thu' <% response.write isWeekday(week_days, Thu) %> id='weekday_thursday'><label for='weekday_thursday'>Thursday</label>" +
                                            "<input type='checkbox' name='weekday_days' value='Fri' <% response.write isWeekday(week_days, Fri) %> id='weekday_friday'><label for='weekday_friday'>Friday</label>" +
                                            "<input type='checkbox' name='weekday_days' value='Sat' <% response.write isWeekday(week_days, Sat) %> id='weekday_saturday'><label for='weekday_saturday'>Saturday</label>";


                                        function makeMonthSelector(name, month_checked) {
                                            var checked_arr = new Array();
                                            for (var i = 1; i <= 12; i++) {
                                                checked_arr[i] = "";
                                                if (i == month_checked) {
                                                    checked_arr[i] = "selected";
                                                }
                                            }

                                            var months_selector = "<select name='" + name + "'>" +
                                                "<option value='1' " + checked_arr[1] + ">January</option>" +
                                                "<option value='2' " + checked_arr[2] + ">Febrary</option>" +
                                                "<option value='3' " + checked_arr[3] + ">March</option>" +
                                                "<option value='4' " + checked_arr[4] + ">April</option>" +
                                                "<option value='5' " + checked_arr[5] + ">May</option>" +
                                                "<option value='6' " + checked_arr[6] + ">June</option>" +
                                                "<option value='7' " + checked_arr[7] + ">July</option>" +
                                                "<option value='8' " + checked_arr[8] + ">August</option>" +
                                                "<option value='9' " + checked_arr[9] + ">September</option>" +
                                                "<option value='10' " + checked_arr[10] + ">October</option>" +
                                                "<option value='11' " + checked_arr[11] + ">November></option>" +
                                                "<option value='12' " + checked_arr[12] + ">December</option>" +
                                                "</select>";
                                            return months_selector;
                                        }
                                        function makeNumSelector(num_checked) {
                                            var checked_arr = new Array();
                                            for (var i = 0; i <= 4; i++) {
                                                checked_arr[i] = "";
                                                if (i == num_checked) {
                                                    checked_arr[i] = "selected";
                                                }
                                            }

                                            var numbers_selector = "<select name='weekday_place'>" +
                                                "<option value='1' " + checked_arr[1] + "1st></option>" +
                                                "<option value='2' " + checked_arr[2] + ">2nd</option>" +
                                                "<option value='3' " + checked_arr[3] + ">3rd</option>" +
                                                "<option value='4' " + checked_arr[4] + ">4th</option>" +
                                                "<option value='0' " + checked_arr[0] + ">Last</option>" +
                                                "</select>";

                                            return numbers_selector;
                                        }

                                        function makeDaySelector(day_checked) {
                                            var day_selector = "<select id='month_day' name='month_day'>";
                                            for (var i = 1; i <= 32; i++) {
                                                day_selector += "<option value='" + i + "'";
                                                if (i == day_checked) {
                                                    day_selector += " selected";
                                                }
                                                day_selector += ">" + (i == 32 ? "Last" : i) + "</option>";
                                            }
                                            return day_selector + "</select>";
                                        }
                                        function makeEverySelector(name, every_checked) {
                                            var checked_arr = new Array();
                                            for (var i = 1; i <= 10; i++) {
                                                checked_arr[i] = "";
                                                if (i == every_checked) {
                                                    checked_arr[i] = "selected";
                                                }
                                            }
                                            var every_selector = "<select id='" + name + "' name='" + name + "'>";
                                            every_selector += "<option value='1' " + checked_arr[1] + ">every</option>";
                                            every_selector += "<option value='2' " + checked_arr[2] + ">every&nbsp;2nd</option>";
                                            every_selector += "<option value='3' " + checked_arr[3] + ">every&nbsp;3rd</option>";
                                            every_selector += "<option value='4' " + checked_arr[4] + ">every&nbsp;4th</option>";
                                            every_selector += "<option value='5' " + checked_arr[5] + ">every&nbsp;5th</option>";
                                            every_selector += "<option value='6' " + checked_arr[6] + ">every&nbsp;6th</option>";
                                            every_selector += "<option value='7' " + checked_arr[7] + ">every&nbsp;7th</option>";
                                            every_selector += "<option value='8' " + checked_arr[8] + ">every&nbsp;8th</option>";
                                            every_selector += "<option value='9' " + checked_arr[9] + ">every&nbsp;9th</option>";
                                            every_selector += "<option value='10' " + checked_arr[10] + ">every&nbsp;10th</option>";
                                            return every_selector + "</select>";
                                        }

                                        function patternChange(pattern_type) {

                                            var dayly_div = "<ol>" +
                                                "<li><input id='dayly_1' type='hidden' name='dayly_selector' value='dayly_1'/> <label for='dayly_1'> " + makeEverySelector('number_days', '<%=number_days %>') + " days</label></li>" +
                                                "</ol>";

                                            var weekly_div = "<ol>" +
                                                "<li>resend " + makeEverySelector('number_weeks', '<%=number_weeks %>') + " week on next days:</li>" +
                                                "<li>" +
                                                days_checkboxes +
                                                "</li>" +
                                                "</ol>";


                                            var monthly_div = "<ol>" +
                                                "<li><input id='monthly_1' type='radio' value='monthly_1' <% if(monthly_selector="monthly_1" OR monthly_selector="") then response.write "checked" end if %> name='monthly_selector'/> <label for='monthly_1'>on</label> "+ makeDaySelector(<%=month_day %>)+ " of " + makeEverySelector('number_month_1', '<%=number_month %>') + " month</li>" +
                                                    "<li><input id='monthly_2' type='radio' value='monthly_2' <% if(monthly_selector="monthly_2") then response.write "checked" end if %> name='monthly_selector'/> <label for='monthly_2'>on</label>" +
                                                        makeNumSelector(<% response.write weekday_place %>) + makeWeekdaysSelector(<% response.write weekday_day %>) +
                                                        "of" + makeEverySelector('number_month_2', '<%=number_month %>') + " month</li>" +
                                                        "</ol>";

                                            var yearly_div = "<ol>" +
                                                "<li><input id='yearly_1' type='radio' value='yearly_1' <% if(yearly_selector="yearly_1" OR yearly_selector="") then response.write "checked" end if %> name='yearly_selector'/> <label for='yearly_1'>on</label> "+ makeDaySelector(<%=month_day %>)+ " of " + makeMonthSelector('month_val_1', '<% response.write month_val %>') + " month</li>" +
                                                    "<li><input id='yearly_2' type='radio' value='yearly_2' <% if(yearly_selector="yearly_2") then response.write "checked" end if %> name='yearly_selector'/> <label for='yearly_2'>on</label>" +
                                                        makeNumSelector(<% response.write weekday_place %>)  + makeWeekdaysSelector(<% response.write weekday_day %>) + " of " + makeMonthSelector('month_val_2', '<% response.write month_val %>') +
                                                        "month</li>" +
                                                        "</ol>";

                                            var alertTimeElement = document.getElementById("alert_time");

                                            if (pattern_type == "d") {
                                                $("#pattern_div").html(dayly_div);
                                            }

                                            if (pattern_type == "w") {
                                                $("#pattern_div").html(weekly_div);
                                            }

                                            if (pattern_type == "m") {
                                                $("#pattern_div").html(monthly_div);
                                            }

                                            if (pattern_type == "y") {
                                                $("#pattern_div").html(yearly_div);
                                            }
                                        }

                                        function toogleDisabled(element, disable) {
                                            if (disable) {
                                                $(element).find("*").prop("disabled", true);

                                            }
                                            else {

                                                $(element).find("*").prop("disabled", false);
                                            }
                                        }

                                        function refreshGroups() {

                                            var keyword = $('#search_group').val();
                                            var domain = $('#domain').val();
                                            var port = $('#port').val();
                                            var adLogin = trim($('#adLogin').val());
                                            var adPassword = (document.getElementById('adPassword_hidden').value == "") ? encode64(trim(document.getElementById('adPassword').value)) : document.getElementById('adPassword_hidden').value;
                                            var adSecure = $('#secure').is(":checked");

                                            if ($("#groups_select2").is(":checked")) {
                                                send_to_users();
                                            }

                                            dataString = 'groups_refresh=1&domain=' + encodeURIComponent(domain) + '&port=' + encodeURIComponent(port) + '&adLogin=' + encodeURIComponent(adLogin) + '&adPassword=' + adPassword + '&keyword=' + encodeURIComponent(keyword) + '&secure=' + adSecure;
                                            url = "ad_action.asp";

                                            if ($("#ad_select2").is(":checked")) {
                                                dataString += "&ad_select=selected";
                                                var selectedOUs = $("input[name=selectedOUs]");
                                                for (var i = 0; i < selectedOUs.length; i++) {
                                                    dataString += "&selectedOUs=" + encodeURIComponent($(selectedOUs[i]).val());
                                                }
                                            }

                                            $.each($("label[name=added_group]"), function (i, input) {
                                                var valueStr = "group_name=" + escape($(input).val().split(",")[0].split("=")[1]);
                                                if (!dataString) {
                                                    dataString = valueStr;
                                                }
                                                else {
                                                    dataString += "&" + valueStr;
                                                }
                                            });

                                            $("#groups_loader").show();
                                            $.ajax({
                                                type: "POST",
                                                url: url,
                                                data: dataString,
                                                success: function (data) {
                                                    $("#groups_loader").hide();
                                                    var $addedGroups = $("label[name=added_group]");
                                                    var actualGroups = JSON.parse(data);

                                                    $.each($addedGroups, function (i, group) {
                                                        var val = $(group).val();

                                                        if ($.grep(actualGroups, function (n, i) { return n.group_dn == val }).length == 0) {
                                                            $(group).parent().parent().remove();
                                                        }
                                                    });

                                                },
                                                error: function () {
                                                    $("#groups_loader").hide();
                                                },
                                                async: true
                                            });
                                        }


                                        $(function ($) {


                                            $(".save_button").button();
                                            $(".cancel_button").button();
                                            $(".delete_button").button();
                                            $("#searchButton").button();
                                            $("#refresh_groups_button").button().click(function () {

                                                refreshGroups();

                                            });

                                            if ($("#ad_select2").is(":checked")) {
                                                ShowAD();
                                            }


                                            if ($("#groups_select2").is(":checked")) {
                                                Show();
                                                myadd_groups(true);
                                            }

                                            serverDate = new Date(getDateFromAspFormat("<%=now_db%>", responseDbFormat));
                                            setInterval(function () { serverDate.setSeconds(serverDate.getSeconds() + 1); }, 1000);


                                            var startTime = $.trim($("#sync_time").val());

                                            if (startTime != "" && isDate(startTime, responseDbFormat)) {
                                                startTime = new Date(getDateFromFormat(startTime, responseDbFormat));
                                            }
                                            else {
                                                startTime = null;
                                            }


                                            initTimePicker($("#sync_time"), settingsDateFormat, settingsTimeFormat);
                                            $("#sync_time").datepicker("setDate", startTime);

                                            $("#adPassword").change(function () {
                                                $("#adPassword_hidden").val("");
                                            });

                                            $("#schedule_sync_div").find("[name='pattern']").change(function () {
                                                if ($(this).is(":checked")) {
                                                    $("#sync_time_div").show();
                                                    patternChange($(this).val());
                                                }
                                            });


                                            $("#auto_sync").change(function () {
                                                if ($(this).is(":checked")) {
                                                    toogleDisabled($("#sync_div"), false);
                                                    $("#sync_div").find("*").trigger('change');
                                                    $("#sync_div").fadeIn(200);
                                                }
                                                else {
                                                    toogleDisabled($("#sync_div"), true);
                                                    $("#sync_div").fadeOut(200);
                                                }
                                            });

                                            $("#auto_sync").trigger('change');


                                        });
                                    })(jQuery);

								</script>
								<div>
									<div>
										<input type="checkbox" name="auto_sync" <% =autoSync %> id="auto_sync" value="1" ><label for="auto_sync">Enable auto synchronization</label></input>
									</div>
									<div id="sync_div" style="display:none">	
										<div>
											<div id="schedule_sync_div" style="margin-left:5px">
												<table>
													<tr>
														<td style="vertical-align:top">
															<div>
																<input id="pattern_dayly" type="radio" value="d" name="pattern"<% if(pattern="d" OR pattern ="") then response.write " checked" end if %>/><label for="pattern_dayly">Daily</label>
																<br/>
																<input id="pattern_weekly" type="radio" value="w" name="pattern"<% if(pattern="w") then response.write " checked" end if %>/><label for="pattern_weekly">Weekly</label>
																<br/>
																<input id="pattern_monthly" type="radio" value="m" name="pattern"<% if(pattern="m") then response.write " checked" end if %>/><label for="pattern_monthly">Monthly</label>
																<br/>
																<input id="pattern_yearly" type="radio" value="y" name="pattern"<% if(pattern="y") then response.write " checked" end if %>/><label for="pattern_yearly">Yearly</label>
															</div>
														</td>
														<td style="vertical-align:top">
															<div id="pattern_div">
															</div>
															<div id="sync_time_div" style="display:none; margin-left:40px; margin-bottom: 20px">
																<label for="pattern_weekly">Start time</label>
																<input id="sync_time" value="<%=start_time%>" type="text" size="10"/>
																<input id="sync_start_time" type="hidden" name="sync_start_time" size="10"/>
															</div>
														</td>
													</tr>
												</table>
											</div>
										</div>
									</div>
								</div>
								<center>Please note that the synchronizing process may take some time (up to severalhours) depending on the number of groups selected and number of users in domains.</center>
								<br/><br/>
								
								<a class="cancel_button" href="Synchronizations.aspx">Cancel</a>
								<a class="save_button" onclick="check_groups();document.getElementById('mainform').action =''; if (my_sub()) {document.forms[0].submit();}">Save</a>
								<br /><br />
							</form>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>
<!-- #include file="db_conn_close.asp" -->
