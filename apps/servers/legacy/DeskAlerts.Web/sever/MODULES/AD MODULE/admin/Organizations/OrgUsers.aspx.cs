﻿using Castle.MicroKernel.Lifestyle;
using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.Server.Utils;
using DeskAlerts.Server.Utils.Helpers;
using DeskAlerts.Server.Utils.Interfactes;
using DeskAlertsDotNet.Application;
using DeskAlertsDotNet.Models.Permission;
using DeskAlertsDotNet.Parameters;
using DeskAlertsDotNet.Utils.Buttons;
using DeskAlertsDotNet.Utils.EventArgs;
using DeskAlertsDotNet.Utils.Factories;
using Resources;
using System;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace DeskAlertsDotNet.Pages
{
    public partial class OrgUsers : DeskAlertsBaseListPage
    {
        private readonly IPushSentRepository _pushSentRepository;
        private readonly IUserInstancesRepository _userInstancesRepository;
        private readonly IUserInstanceIpRepository _userInstanceIpRepository;
        private readonly ITokenService _tokenService;
        private readonly IUserService _userService;

        public OrgUsers()
        {
            using (Global.Container.BeginScope())
            {
                _pushSentRepository = Global.Container.Resolve<IPushSentRepository>();
                _userInstancesRepository = Global.Container.Resolve<IUserInstancesRepository>();
                _userInstanceIpRepository = Global.Container.Resolve<IUserInstanceIpRepository>();
                _tokenService = Global.Container.Resolve<ITokenService>();
                _userService = Global.Container.Resolve<IUserService>();
            }
        }

        /// <summary>
        /// Gets the collumns.
        /// </summary>
        protected override string[] Collumns => new string[] { };

        /// <summary>
        /// Gets the content name.
        /// </summary>
        protected override string ContentName => resources.LNG_USERS;

        /// <summary>
        /// Gets the data table.
        /// </summary>
        protected override string DataTable => "users";

        /// <summary>
        /// Gets the no rows div.
        /// </summary>
        protected override System.Web.UI.HtmlControls.HtmlGenericControl NoRowsDiv => NoRows;

        /// <summary>
        /// Gets the table div.
        /// </summary>
        protected override System.Web.UI.HtmlControls.HtmlGenericControl TableDiv => mainTableDiv;

        /// <summary>
        /// Gets the delete buttons.
        /// </summary>
        protected override IButtonControl[] DeleteButtons => new IButtonControl[] { upDelete, bottomDelete };

        /// <summary>
        /// Gets the default order collumn.
        /// </summary>
        protected override string DefaultOrderCollumn => "name";

        /// <summary>
        /// Gets the custom query.
        /// </summary>
        protected override string CustomQuery
        {
            get
            {
                string query = GetFilterForQuery();

                query += @"SELECT t.user_id as id,
                               users.next_request,
                               users.last_standby_request,
                               users.next_standby_request,
                               users.context_id,
                               users.mobile_phone,
                               users.email,
                               users.name AS name,
                               users.display_name,
                               users.standby,
                               users.domain_id,
                               domains.name AS domain_name,
                               users.deskbar_id,
                               users.reg_date,
                               users.last_date,
                               users.last_request AS last_request1,
                               users.last_request,
                               users.client_version,
                               edir_context.name AS context_name
                        FROM #tempUsers t
                        LEFT JOIN users ON t.user_id = users.id
                        LEFT JOIN domains ON domains.id = users.domain_id
                        LEFT JOIN edir_context ON edir_context.id = users.context_id
                        WHERE ROLE='U'";

                if (!string.IsNullOrEmpty(Request["sortBy"]))
                {
                    query += " ORDER BY " + Request["sortBy"];
                }
                else
                {
                    query += " ORDER BY name";
                }

                query += @" DROP TABLE #tempOUs
                        DROP TABLE #tempUsers";

                return query;
            }
        }

        protected override string DefaultSortingOrder { get; } = "ASC";

        /// <summary>
        /// Gets the custom count query.
        /// </summary>
        protected override string CustomCountQuery
        {
            get
            {
                string query = GetFilterForQuery();

                query += @" SELECT COUNT(1) AS cnt
                            FROM #tempUsers";
                return query;
            }
        }

        /// <summary>
        /// Gets the page params.
        /// </summary>
        protected override string PageParams
        {
            get
            {
                var uri = string.Empty;
                uri += AddParameter(uri, "type");
                uri += AddParameter(uri, "id");
                return uri;
            }
        }

        /// <summary>
        /// Gets the top paging.
        /// </summary>
        protected override HtmlGenericControl TopPaging => topPaging;

        /// <summary>
        /// Gets the bottom paging.
        /// </summary>
        protected override HtmlGenericControl BottomPaging => bottomRanging;

        protected override HtmlInputText SearchControl => searchTermBox;

        protected override bool IsUsedDefaultTableSettings => false;

        /// <summary>
        /// The on load.
        /// </summary>
        /// <param name="e">
        /// The e.
        /// </param>
        protected override void OnLoad(EventArgs e)
        {
            UsersAndGroups.Visible = Config.Data.HasModule(Modules.AD) == false;
            base.OnLoad(e);

            int cnt = this.Content.Count < (Offset + Limit) ? this.Content.Count : Offset + Limit;

            userName.Text = GetSortLink(resources.LNG_USERNAME, "name", Request["sortBy"]);
            displayName.Text = GetSortLink(resources.LNG_DISPLAY_NAME, "display_name", Request["sortBy"]);
            mobilePhone.Text = GetSortLink(resources.LNG_MOBILE_PHONE, "mobile_phone", Request["sortBy"]);
            domain.Text = GetSortLink(resources.LNG_DOMAIN, "domains.name", Request["sortBy"]);
            email.Text = GetSortLink(resources.LNG_EMAIL, "email", Request["sortBy"]);
            online.Text = GetSortLink(resources.LNG_ONLINE, "last_request", Request["sortBy"]);
            lastActivity.Text = GetSortLink(resources.LNG_LAST_ACTIVITY, "last_request", Request["sortBy"]);
            clientVersion.Text = GetSortLink(resources.LNG_CLIENT_VERSION, "client_version", Request["sortBy"]);
            actionsCollumn.Text = resources.LNG_ACTIONS;
            base.Table = contentTable;
            searchButton.Text = searchButton.Text.Replace("{Search}", resources.LNG_SEARCH);

            upDelete.Visible = curUser.HasPrivilege || curUser.Policy[Policies.USERS].CanDelete;
            bottomDelete.Visible = curUser.HasPrivilege || curUser.Policy[Policies.USERS].CanDelete;
            headerSelectAll.Visible = curUser.HasPrivilege || curUser.Policy[Policies.USERS].CanDelete;

            for (int i = Offset; i < cnt; i++)
            {
                var row = Content.GetRow(i);
                var tableRow = new TableRow();

                if (curUser.HasPrivilege || curUser.Policy[Policies.USERS].CanDelete)
                {
                    CheckBox chb = GetCheckBoxForDelete(row);
                    TableCell checkBoxCell = new TableCell();
                    checkBoxCell.HorizontalAlign = HorizontalAlign.Center;
                    checkBoxCell.Controls.Add(chb);
                    tableRow.Cells.Add(checkBoxCell);
                }

                // Username, Display Name, Domain, Mobile phone, Email, Online, Last activity, Client version, Actions.
                TableCell usernameCell = new TableCell();
                usernameCell.Text = row.GetString("name");
                tableRow.Cells.Add(usernameCell);

                TableCell displayNameCell = new TableCell();
                displayNameCell.Text = row.GetString("display_name");
                displayNameCell.HorizontalAlign = HorizontalAlign.Center;
                tableRow.Cells.Add(displayNameCell);

                TableCell domainCell = new TableCell();
                domainCell.Text = row.GetString("domain_name");
                domainCell.HorizontalAlign = HorizontalAlign.Center;
                tableRow.Cells.Add(domainCell);

                TableCell phoneCell = new TableCell();
                phoneCell.Text = row.GetString("mobile_phone");
                phoneCell.HorizontalAlign = HorizontalAlign.Center;
                tableRow.Cells.Add(phoneCell);

                TableCell emailCell = new TableCell();
                emailCell.Text = row.GetString("email");
                emailCell.HorizontalAlign = HorizontalAlign.Center;
                tableRow.Cells.Add(emailCell);

                var lastRequest = row.GetDateTime("last_request");
                var isOnline = DeskAlertsUtils.IsUserOnline(lastRequest, row.GetInt("next_request"));
                var statusImg = new Image
                {
                    Width = 11,
                    Height = 11,
                    ImageUrl = isOnline ? "../images/online.gif" : "../images/offline.gif",
                    AlternateText = isOnline ? resources.LNG_ONLINE : resources.LNG_OFFLINE
                };

                var statusCell = new TableCell { HorizontalAlign = HorizontalAlign.Center };
                statusCell.Controls.Add(statusImg);
                tableRow.Cells.Add(statusCell);

                var lastActivityCell = new TableCell
                {
                    Text = DeskAlertsUtils.IsNotEmptyDateTime(lastRequest)
                        ? DateTimeConverter.ToUiDateTime(lastRequest)
                        : string.Empty
                };

                tableRow.Cells.Add(lastActivityCell);

                TableCell clientVersionCell = new TableCell();
                clientVersionCell.Text = row.GetString("client_version");
                tableRow.Cells.Add(clientVersionCell);

                var editButton = new EditUserButton { UserId = row.GetString("id") };
                editButton.ClickEvent += EditButton_ClickEvent;
                var editImg = new Image { ImageUrl = "../images/action_icons/edit.png" };
                editImg.Attributes["title"] = resources.LNG_EDIT_USER;
                editButton.Controls.Add(editImg);

                LinkButton previewButton = GetActionButton(row.GetString("id"), "../images/action_icons/preview.png", "LNG_VIEW_USER_DETAILS", delegate { ScriptManager.RegisterClientScriptBlock(this, GetType(), "ORG_USERS_PREVIEW_" + row.GetString("id"), "showPreview(" + row.GetString("id") + ")", true); });
                TableCell actionsCell = new TableCell();
                actionsCell.HorizontalAlign = HorizontalAlign.Center;

                if (curUser.HasPrivilege || curUser.Policy[Policies.USERS].CanEdit)
                {
                    actionsCell.Controls.Add(editButton);
                }

                actionsCell.Controls.Add(previewButton);

                tableRow.Cells.Add(actionsCell);

                contentTable.Rows.Add(tableRow);
            }
        }

        private void EditButton_ClickEvent(object sender, EditUserEventArgs e)
        {
            ScriptManager.RegisterClientScriptBlock(
                this,
                GetType(),
                "ORG_USERS_EDIT_" + e.UserId,
                "editUser(" + e.UserId + (Request.Url.Query.Length > 0 ? ",'" + Request.Url.Query + "'" : ",''") + ")",
                true);
        }

        /// <summary>
        /// On delete rows event
        /// </summary>
        /// <param name="ids">IDs to delete</param>
        protected override void OnDeleteRows(string[] ids)
        {
            foreach (var id in ids)
            {
                dbMgr.ExecuteQuery("DELETE FROM alerts_read WHERE user_id = @userId", SqlParameterFactory.Create(DbType.String, id, "userId"));
                dbMgr.ExecuteQuery("DELETE FROM alerts_received WHERE user_id = @userId", SqlParameterFactory.Create(DbType.String, id, "userId"));
                dbMgr.ExecuteQuery("DELETE FROM alerts_sent WHERE user_id = @userId", SqlParameterFactory.Create(DbType.String, id, "userId"));
                dbMgr.ExecuteQuery($"DELETE FROM alerts_sent_stat WHERE user_id = {id}");
                dbMgr.ExecuteQuery("DELETE FROM surveys_answers_stat WHERE user_id = @userId", SqlParameterFactory.Create(DbType.String, id, "userId"));
                dbMgr.ExecuteQuery("DELETE FROM surveys_custom_answers_stat WHERE user_id = @userId", SqlParameterFactory.Create(DbType.String, id, "userId"));
                dbMgr.ExecuteQuery("DELETE FROM alerts_sent_stat WHERE user_id = @userId", SqlParameterFactory.Create(DbType.String, id, "userId"));
                dbMgr.ExecuteQuery("DELETE FROM users_groups WHERE user_id = @userId", SqlParameterFactory.Create(DbType.String, id, "userId"));

                var userInstancesIds = _userInstancesRepository
                    .GetInstances(Convert.ToInt64(id))
                    .Select(instance => instance.Clsid);

                foreach (var instanceId in userInstancesIds)
                {
                    _pushSentRepository.DeleteByUserInstanceId(instanceId);
                    _userInstanceIpRepository.DeleteByUserInstanceId(instanceId);
                }

                var longId = Convert.ToInt64(id);

                _userInstancesRepository.DeleteByUserId(longId);
                _tokenService.RemoveToken(_userService.GetUserById(longId).Username);
            }
        }

        /// <summary>
        /// The get filter for query.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetFilterForQuery()
        {
            string searchType = Request["type"];
            string searchId = Request["id"];

            string queryDomainType = Request["domainType"];
            string queryDomainId = Request["domainId"];

            string query = @"SET NOCOUNT ON IF OBJECT_ID('tempdb..#tempOUs') IS NOT NULL
                                    DROP TABLE #tempOUs
                                    CREATE TABLE #tempOUs (Id_child BIGINT NOT NULL, Rights BIT NOT NULL);

                                    IF OBJECT_ID('tempdb..#tempUsers') IS NOT NULL
                                    DROP TABLE #tempUsers
                                    CREATE TABLE #tempUsers (user_id BIGINT NOT NULL);

                                    DECLARE @now DATETIME;

                                    SET @now = GETDATE();";

            if (searchType == "ou")
            {
                query += $@" INSERT INTO #tempOUs (Id_child, Rights) VALUES ({searchId}, 0)";
            }
            else if (searchType == "DOMAIN" || queryDomainType == "DOMAIN")
            {
                if (searchType == "DOMAIN")
                {
                    query +=
                        $@" INSERT INTO #tempOUs (Id_child, Rights) (SELECT id, 0 FROM OU WHERE Id_domain = {
                                searchId
                            })";
                }

                if (queryDomainType == "DOMAIN")
                {
                    query += $@" INSERT INTO #tempOUs (Id_child, Rights) (SELECT id, 0 FROM OU WHERE Id_domain = {queryDomainId})";
                }
            }
            else
            {
                query += " INSERT INTO #tempOUs (Id_child, Rights) (SELECT id, 0 FROM OU) ";
            }

            query += @"INSERT INTO #tempUsers (user_id)
                              (SELECT DISTINCT Id_user
                               FROM OU_User u
                               INNER JOIN #tempOUs t ON u.Id_OU=t.Id_child
                               LEFT JOIN users ON u.Id_user = users.id
                               WHERE ROLE='U'";

            if (!string.IsNullOrEmpty(SearchTerm))
            {
                query += $" AND (users.name LIKE N'%{this.SearchTerm}%' OR users.display_name LIKE N'%{this.SearchTerm}%')";
            }

            query += ")";

            if (searchType != "ou")
            {
                query += @"	INSERT INTO #tempUsers(user_id)
                                      (SELECT DISTINCT users.id
                                       FROM users
                                       LEFT JOIN #tempUsers t ON t.user_id=users.id
                                       WHERE t.user_id IS NULL
                                         AND users.ROLE='U'";

                if (searchType == "DOMAIN")
                {
                    query += $" AND users.domain_id = {searchId}";
                }

                if (queryDomainType == "DOMAIN")
                {
                    query += $" AND users.domain_id = {queryDomainId}";
                }

                if (!string.IsNullOrEmpty(SearchTerm))
                {
                    query += $" AND (users.name LIKE N'%{this.SearchTerm}%' OR users.display_name LIKE N'%{this.SearchTerm}%')";
                }

                query += ")";
            }
            return query;
        }
    }
}