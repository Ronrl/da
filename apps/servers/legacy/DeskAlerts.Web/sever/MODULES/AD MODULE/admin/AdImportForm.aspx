﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdImportForm.aspx.cs" Inherits="DeskAlertsDotNet.Pages.AdImportForm" %>

<%@ Import Namespace="DeskAlertsDotNet.Parameters" %>
<%@ Import Namespace="Resources" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>AD Synchronizing Configuration</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="css/style9.css" rel="stylesheet" type="text/css" />
    <link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
    <script type="text/javascript">
        var cnt = 1;
        var files = [];

        $(function () {
            $("#add_file").button();
            $("#submit_button").button();

            add_file(true);
            $("#submit_button").click(function () {
                if (files.length == 0) {
                    $("#activeDirectoryImportForm").submit();
                }
                else {
                    alert('<% = resources.LNG_FILES_IS_NOT_SELECTED %>');
                }
            });
        });

        function add_file(isFirst) {
            var fileName = 'file_' + (cnt++);
            files.push(fileName);
            var html =
                "<div class='files_upload' id='upload_block_" + fileName + "'>" +
                "<input type='file' class='button_add' name='input_" + fileName + "' id='input_" +
                fileName + "' accept='.csv' onchange='onChangeFile(this)' />";
            if (!isFirst) {
                html += "<a href='#' class='logout ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary' id='" + fileName + "' onclick='onClickRemove(this)'>Remove</a>";
            }
            html += "</div>";
            $("#uploadPanel").append(html);
            $("#" + fileName).button();
        }

        function onClickRemove(sender) {
            files.forEach(function (e, i) {
                if (files[i] == sender.id) {
                    $("#upload_block_" + sender.id).remove();
                    files.splice(i, 1);
                }
            });
        }

        function onChangeFile(sender) {
            files.forEach(function (e, i) {
                if ("input_" + files[i] == sender.id) {
                    files.splice(i, 1);
                }
            });
        }
    </script>
</head>
<body class="body">
    <form id="activeDirectoryImportForm" class="activeDirectoryImportForm" enctype="multipart/form-data" runat="server">
        <fieldset>
            <legend>Upload CSV form:</legend>
            <span>Select the file to upload (.csv):</span>
            <div id="uploadPanel"></div>
            <a href="#" class="delete_button" onclick="add_file(false);" id="add_file">Add file</a>
            <br />
            <label for="overwriteSynch">Overwrite existing sync</label>
            <asp:CheckBox runat="server" ID="overwriteSynch" Checked="true"></asp:CheckBox>
            <br />
            <a href="#" class="delete_button" id="submit_button">Upload</a>
        </fieldset>
    </form>
</body>
</html>
