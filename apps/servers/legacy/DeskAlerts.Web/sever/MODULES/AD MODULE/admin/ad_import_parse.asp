<!-- #include file = "config.inc" -->	
<% ConnCount = 3 %>
<!-- #INCLUDE FILE="db_conn.asp" -->
<% Server.ScriptTimeout = 90000 %>

<% 
on error resume next

uid = Session("uid")

added_users_cnt = 0
added_groups_cnt = 0
skipped_users_cnt = 0
skipped_groups_cnt = 0

Set RSstart = Conn.Execute("SELECT name FROM users WHERE id=" & uid)
if(Not RSstart.EOF) then
	start_name = RSstart("name")
end if

screen_log = "Sync started by '"& start_name &"' on " & now_db & "<br>"
error_log_text = "Sync started by '"& start_name &"' on " & now_db & vbcrlf
load_log_text = "Sync started by '"& start_name &"' on " & now_db & vbcrlf

function processRecord(userName, displayName, domainName, gList, email, phone)
	'check domain
	domain_was_checked = 0
	SQL = "SELECT id, was_checked FROM domains WHERE name='" &domainName&"'"
	Set RSdomain = Conn.Execute(SQL)
	if(Not RSdomain.EOF) then
		domain_id = RSdomain("id")
		domain_was_checked = RSdomain("was_checked")
	'update - what domains were modified, to remove at the end - was_checked=6, 1 at the end
		Conn.Execute("UPDATE domains SET was_checked=6 WHERE id=" & domain_id)
	else
	'insert
		Conn.Execute("INSERT INTO domains (name, was_checked) VALUES ('"&domainName&"',6)")
		Set rs1 = Conn.Execute("select @@IDENTITY newID from domains")
		domain_id=rs1("newID")
		rs1.Close
		if(IsNull(domain_id)) then
			Set rs1 = Conn.Execute("select MAX(id) as newID from domains")
			domain_id=rs1("newID")
			rs1.Close
		end if
	end if
	'SQL = "SELECT Id_ou FROM OU_DOMAIN WHERE Id_domain=" & domain_id
	'Set RSoudomain = Conn.Execute(SQL)
	'if(RSoudomain.EOF) then		
'		Conn.Execute("INSERT INTO OU_DOMAIN (Id_domain, Id_ou) VALUES ("&domain_id&",-1)")
'	end if
		
	if(domain_was_checked <> 6) then
	'was_checked = 0 for users/groups/users_groups in this domain at the first time
		Conn.Execute("UPDATE users SET was_checked=0 WHERE domain_id="&domain_id)
		Conn.Execute("UPDATE groups SET was_checked=0 WHERE domain_id="&domain_id)
		Set RSGroups= Conn.Execute("SELECT id FROM groups WHERE domain_id="&domain_id&" AND (custom_group IS NULL or custom_group = 0)")
		Do while not RSGroups.EOF
			SQL = "UPDATE users_groups SET was_checked=0 WHERE group_id="&RSGroups("id")
			Conn2.Execute(SQL)
			RSGroups.MoveNext
		loop		
	end if
	
	'check user
	SQL = "SELECT id FROM users WHERE name='" &userName&"' AND domain_id=" & domain_id
	Set RSuser= Conn.Execute(SQL)
	if(Not RSuser.EOF) then
		user_id = RSuser("id")
		Conn.Execute("UPDATE users SET display_name='"&displayName&"', was_checked=1, mobile_phone='"&phone&"', email='"&email&"' WHERE id=" & user_id)
		skipped_users = skipped_users & userName & "(" & domainName & ")" & vbcrlf
		skipped_users_cnt = skipped_users_cnt + 1
	else
	'insert
		Conn.Execute("INSERT INTO users (name, display_name, was_checked, reg_date, domain_id, role, mobile_phone, email) VALUES ('"&userName&"','"&displayName&"',1, GETDATE(),"&domain_id&", 'U','"&phone&"', '"&email&"')")
		Set rs1 = Conn.Execute("select @@IDENTITY newID from users")
		user_id=rs1("newID")
		rs1.Close
		if(IsNull(user_id)) then
			Set rs1 = Conn.Execute("select MAX(id) as newID from users")
			user_id=rs1("newID")
			rs1.Close
		end if
		added_users = added_users & userName & "(" & domainName & ")" & vbcrlf
		added_users_cnt = added_users_cnt + 1
	end if	
	'---
	recordGroups = Split( gList, ";" )
	For gCnt = 0 to UBound(recordGroups)
		groupName = Trim(recordGroups(gCnt))
		'check group
		SQL = "SELECT id FROM groups WHERE name='" &groupName&"' AND domain_id=" & domain_id
		Set RSgroup= Conn.Execute(SQL)
		if(Not RSgroup.EOF) then
			group_id = RSgroup("id")
			Conn.Execute("UPDATE groups SET was_checked=1 WHERE id=" & group_id)
			skipped_groups = skipped_groups & groupName & "(" & domainName & ")" & vbcrlf
			skipped_groups_cnt = skipped_groups_cnt + 1
			
		else
		'insert
			Conn.Execute("INSERT INTO groups (name, was_checked, domain_id) VALUES ('"&groupName&"',1, "&domain_id&")")
			Set rs1 = Conn.Execute("select @@IDENTITY newID from groups")
			group_id=rs1("newID")
			rs1.Close
			if(IsNull(group_id)) then
				Set rs1 = Conn.Execute("select MAX(id) as newID from groups")
				group_id=rs1("newID")
				rs1.Close
			end if
			added_groups = added_groups & userName & "(" & domainName & ")" & vbcrlf
			added_groups_cnt = added_groups_cnt + 1
		end if	
	
		'check users_groups
		SQL = "SELECT id FROM users_groups WHERE user_id="&user_id&" AND group_id="&group_id
		Set RSusers_groups= Conn.Execute(SQL)
		if(Not RSusers_groups.EOF) then
			users_groups_id = RSusers_groups("id")
			Conn.Execute("UPDATE users_groups SET was_checked=1 WHERE id=" & users_groups_id)
		else
		'insert
			Conn.Execute("INSERT INTO users_groups (user_id, group_id, was_checked) VALUES ("&user_id&", "&group_id&", 1)")
		end if	
	Next

'	response.write userName & "<br>"
'	response.write displayName& "<br>"
'	response.write domainName& "<hr>"
end function

function parseCSV(fname)
	Dim objFSO, oInStream, sLine, sSeg
	Const ForReading = 1
	Set objFSO = CreateObject("Scripting.FileSystemObject")
	If objFSO.FileExists( Server.MapPath( fname ) ) Then
		Set oInStream = objFSO.OpenTextFile( Server.MapPath( fname ), ForReading, False )
		csvLineNumber = 1
		Do While oInStream.AtEndOfStream <> True
			sLine = oInStream.ReadLine 
			sSeg = Split( sLine, "," )
			if(csvLineNumber > 1) then
				if(Ubound(sSeg)=5) then
					userName = replace(sSeg(0),"""", "")
					displayName = replace(sSeg(1),"""", "")
					domainName = replace(sSeg(3),"""", "")
					gList = replace(sSeg(2),"""", "")
                    email = replace(sSeg(4),"""", "")
                    phone = replace(sSeg(5),"""", "")
					processRecord userName, displayName, domainName, gList, email, phone
				else
					screen_log = screen_log & "Error on line "&csvLineNumber&": Wrong CSV format.<br/>"
					error_log_text = error_log_text & "Error on line "&csvLineNumber&": Wrong CSV format." & vbcrlf
				end if
			end if
			csvLineNumber = csvLineNumber + 1
		Loop
		oInStream.Close 
		Set oInStream = Nothing
		screen_log = screen_log & "processed " & csvLineNumber & " strings.<br>"
	Else
		Response.Write "File not found!"
	End If 
	Set objFSO = Nothing 
	
end function

function parseXLS(fname)
	'"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & Server.mappath(fname) & ";Extended Properties=Excel 12.0"
	DBConnectionString = "Driver={Microsoft Excel Driver (*.xls)};DriverId=790;Dbq=" & Server.mappath(fname) & ";UID=admin;"
	Set ConnXLS = Server.CreateObject("ADODB.Connection")
	ConnXLS.Open DBConnectionString
	xlsLineNumber = 1
	Set objRS = ConnXLS.Execute("SELECT * FROM [Sheet1$]")
      objRS.MoveFirst

      While Not objRS.EOF
'            Response.write("<TD>" & objRS.Fields.Item(X).Value)
			userName = objRS.Fields.Item(0).Value
			displayName = objRS.Fields.Item(1).Value
			domainName = objRS.Fields.Item(3).Value
			gList = objRS.Fields.Item(2).Value
            email = objRS.Fields.Item(4).Value
            phone = objRS.Fields.Item(5).Value
			processRecord userName, displayName, domainName, gList, email, phone
         xlsLineNumber = xlsLineNumber + 1
		 objRS.MoveNext
      Wend
	screen_log = screen_log & "processed " & xlsLineNumber & " strings.<br>"	  
	
end function

function parseXLSX(fname)
	DBConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & Server.mappath(fname) & ";Extended Properties=Excel 12.0"
	Set ConnXLS = Server.CreateObject("ADODB.Connection")
	ConnXLS.Open DBConnectionString
	xlsLineNumber = 0
	Set objRS = ConnXLS.Execute("SELECT * FROM [Sheet1$]")
      objRS.MoveFirst

      While Not objRS.EOF
'            Response.write("<TD>" & objRS.Fields.Item(X).Value)
			userName = objRS.Fields.Item(0).Value
			displayName = objRS.Fields.Item(1).Value
			domainName = objRS.Fields.Item(3).Value
			gList = objRS.Fields.Item(2).Value
            email = objRS.Fields.Item(4).Value
            phone = objRS.Fields.Item(5).Value
			processRecord userName, displayName, domainName, gList, email, phone
         xlsLineNumber = xlsLineNumber + 1
		 objRS.MoveNext
      Wend
	screen_log = screen_log & "processed " & xlsLineNumber & " strings.<br>"	  
	
end function

fNames=request("fnames")
uploadPath="ad_import\files\"

'for all files
iFiles = Split(fNames, "|" )
For iCnt = 0 to UBound(iFiles)
	if(iFiles(iCnt)<>"") then
		if(InStr(iFiles(iCnt),".csv")>0 OR InStr(iFiles(iCnt),".txt")>0) then
			screen_log = screen_log & "File: " & iFiles(iCnt) & "<br/>"
			error_log_text = error_log_text & "File " & iFiles(iCnt) & vbcrlf
			load_log_text = load_log_text &vbcrlf & "File: " & iFiles(iCnt) & vbcrlf
			added_users = "Added users: "& vbcrlf
			added_groups = "Added groups: "& vbcrlf
			skipped_users = "Skipped users: "& vbcrlf
			skipped_groups = "Skipped groups: "& vbcrlf
			parseCSV(uploadPath & iFiles(iCnt))
			load_log_text = load_log_text & vbcrlf & added_users
			load_log_text = load_log_text & vbcrlf & skipped_users
			load_log_text = load_log_text & vbcrlf & added_groups
			load_log_text = load_log_text & vbcrlf & skipped_groups			
		elseif(InStr(iFiles(iCnt),".xlsx")>0) then
			screen_log = screen_log & "File: " & iFiles(iCnt) & "<br/>"		
			error_log_text = error_log_text & "File " & iFiles(iCnt) & vbcrlf
			load_log_text = load_log_text &vbcrlf & "File: " & iFiles(iCnt) & vbcrlf			
			added_users = "Added users: "& vbcrlf
			added_groups = "Added groups: "& vbcrlf
			skipped_users = "Skipped users: "& vbcrlf
			skipped_groups = "Skipped groups: "& vbcrlf			
			parseXLSX(uploadPath & iFiles(iCnt))
			if(Err.Number <> 0) then
				screen_log = screen_log & "Error: Can't read xls file, because no excel data provider installed.<br/>"
				error_log_text = error_log_text & "Error: Can't read xls file, because no excel data provider installed." & vbcrlf
			end if
			load_log_text = load_log_text & vbcrlf & added_users
			load_log_text = load_log_text & vbcrlf & skipped_users
			load_log_text = load_log_text & vbcrlf & added_groups
			load_log_text = load_log_text & vbcrlf & skipped_groups						
		elseif(InStr(iFiles(iCnt),".xls")>0) then
			screen_log = screen_log & "File: " & iFiles(iCnt) & "<br/>"		
			error_log_text = error_log_text & "File " & iFiles(iCnt) & vbcrlf
			load_log_text = load_log_text &vbcrlf & "File: " & iFiles(iCnt) & vbcrlf
			added_users = "Added users: "& vbcrlf
			added_groups = "Added groups: "& vbcrlf
			skipped_users = "Skipped users: "& vbcrlf
			skipped_groups = "Skipped groups: "& vbcrlf			
			parseXLS(uploadPath & iFiles(iCnt))
			if(Err.Number <> 0) then
				screen_log = screen_log & "Error: Can't read xls file, because no excel data provider installed.<br/>"
				error_log_text = error_log_text & "Error: Can't read xls file, because no excel data provider installed." & vbcrlf
			end if
			load_log_text = load_log_text & vbcrlf & added_users
			load_log_text = load_log_text & vbcrlf & skipped_users
			load_log_text = load_log_text & vbcrlf & added_groups
			load_log_text = load_log_text & vbcrlf & skipped_groups						
		end if
	end if
Next

if(request("overwrite")="1") then
    removed_users = "Removed users: "& vbcrlf
    removed_groups = "Removed groups: "& vbcrlf
    removed_users_cnt = 0
    remove_groups_cnt = 0

    'remove objects that not in files
    SQL = "SELECT id, name FROM domains WHERE was_checked=6"
    Set RSremove = Conn.Execute(SQL)
    do while not RSremove.EOF
	    domain_id = RSremove("id")
	    domain_name = RSremove("name")
	    'for log
	    Set RSusers = Conn2.Execute("SELECT name FROM users WHERE was_checked=0 AND domain_id="&domain_id)
	    Do while not RSusers.EOF
		    removed_users = removed_users & RSusers("name") & "(" & domain_name & ")" & vbcrlf
		    removed_users_cnt = removed_users_cnt + 1
		    RSusers.MoveNext
	    loop

	    SQL = "DELETE FROM users WHERE role='U' AND was_checked=0 AND domain_id="&domain_id
	    Conn.Execute(SQL)

	    Set RSGroups = Conn2.Execute("SELECT id, name, was_checked FROM groups WHERE domain_id="&domain_id)
	    Do while not RSGroups.EOF
		    if(RSGroups("was_checked")=0) then
			    removed_groups = removed_groups & RSGroups("name") & "(" & domain_name & ")" & vbcrlf
			    removed_groups_cnt = removed_groups_cnt + 1
		    end if
		    SQL = "DELETE FROM users_groups WHERE was_checked=0 AND group_id="&RSGroups("id")
		    Conn.Execute(SQL)
		    RSGroups.MoveNext
	    loop
	    SQL = "DELETE FROM groups WHERE was_checked=0 AND (custom_group IS NULL or custom_group = 0)  AND domain_id="&domain_id
	    Conn.Execute(SQL)
	    SQL = "UPDATE domains SET was_checked=1 WHERE id="&domain_id
	    Conn.Execute(SQL)
	    RSremove.MoveNext
    loop
end if
screen_log = screen_log & "Sync Completed successfully adding " & added_users_cnt & " users, skipping " & skipped_users_cnt & " users, and removing " & removed_users_cnt & " users."

response.write screen_log

'write load log
load_log_text = load_log_text & vbcrlf & removed_users
load_log_text = load_log_text & vbcrlf & removed_groups

load_log_fname = "ad_load-" & Year(now_db) & Month(now_db) & Day(now_db) & Hour(now_db) & Minute(now_db) & Second(now_db) &".log"
error_log_fname = "error-" & Year(now_db) & Month(now_db) & Day(now_db) & Hour(now_db) & Minute(now_db) & Second(now_db) &".log"

WriteToFile alerts_dir & "admin\ad_import\logs\" & load_log_fname, load_log_text, False
WriteToFile alerts_dir & "admin\ad_import\logs\" & error_log_fname, error_log_text, False

'write error log

%><center><b></b><br><a href="AdImportForm.aspx">Go Back</a></center>
<!-- #INCLUDE FILE="db_conn_close.asp" -->