﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CompsInGroup.aspx.cs" Inherits="DeskAlertsDotNet.Pages.CompsInGroup" %>
<%@ Import NameSpace="DeskAlertsDotNet.Parameters" %>
<%@ Import Namespace="Resources" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<title>DeskAlerts</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link href="css/style9.css" rel="stylesheet" type="text/css">
        <link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <link href="css/jquery/jquery_timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css">
        <script language="javascript" type="text/javascript" src="functions.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/date.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery_timepicker/jquery-ui-timepicker-addon.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/jquery/datepicker_fix.js"></script>
        <script language="javascript" type="text/javascript">


            $(document).ready(function() {
                $(".delete_button").button({
                });

                $("#selectAll").click(function () {
                    var checked = $(this).prop('checked');
                    $(".content_object").prop('checked', checked);
                });


            });

            function showPreview(id) {
                window.open('CompDetails.aspx?id=' + id, '', 350, 350);
            }

            //function editUser(id, queryString)
            //{
            //    location.replace('../edit_group.asp?wh=1&id='+ id +'&return_page=users_new.asp?' + queryString);
            //}

        </script>
</head>
<body>
    <form id="form1" runat="server">
    <table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	    <tr>
	    <td>
	    <table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
		    <tr>
		    </tr>
            <tr>
            <td class="main_table_body" height="100%">
		
			<div style="margin-left:10px" runat="server" id="mainTableDiv">
				<br><br>
                <%=resources.LNG_GROUP %>: <b runat="server" id="groupName"></b> 
                <%=resources.LNG_DOMAIN%>: <b runat="server" id="domainName"></b><br/> 
                 <A href="GroupsInGroup.aspx?id=<%=Request["id"]%>&member_of=1"><%=resources.LNG_VIEW_MEMBER_OF %></a> | 
                <A href="GroupsInGroup.aspx?id=<%=Request["id"]%>"><%=resources.LNG_VIEW_GROUPS %></a> |
                <A href="UsersInGroup.aspx?id=<%=Request["id"]%>"> <%=resources.LNG_VIEW_USERS %></A> |
                 <%=resources.LNG_VIEW_COMPUTERS %><br/><br/>
			   <table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr><td> 
					<div style="margin-left:10px" id="topPaging" runat="server"></div>
				</td></tr>
				</table>
				<br>

				<br><br>
				<asp:Table style="padding-left: 0px;margin-left: 10px;margin-right: 10px;" runat="server" id="contentTable" Width="70%" Height="100%" CssClass="data_table" CellSpacing="0" CellPadding="3">
				<asp:TableRow CssClass="data_table_title">
					<asp:TableCell runat="server" id="headerSelectAll" CssClass="table_title" Width="2%" >
						<asp:CheckBox id="selectAll" runat="server"/>
					</asp:TableCell>
					<asp:TableCell runat="server" id="compName" CssClass="table_title" ></asp:TableCell>
					<asp:TableCell runat="server" id="domain" CssClass="table_title"></asp:TableCell>
                    <asp:TableCell runat="server" id="online" CssClass="table_title"></asp:TableCell>
                    <asp:TableCell runat="server" id="lastActivity" CssClass="table_title"></asp:TableCell>
                    <asp:TableCell runat="server" id="actionsCollumn" CssClass="table_title"></asp:TableCell>
				</asp:TableRow>
				</asp:Table>
				<br>

				 <br><br>
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				 
				<tr><td> 
					<div style="margin-left:10px" id="bottomPaging" runat="server"></div>
				</td></tr>
				</table>
			</div>
			<div runat="server" id="NoRows">
                <br />
                <br />
				<center><b><%=resources.LNG_THERE_ARE_NO_COMPUTERS%><b><center>
			</div>
			<br><br>
            </td>
            </tr>
           
 
            
             </table>
    </form>
</body>
</html>

