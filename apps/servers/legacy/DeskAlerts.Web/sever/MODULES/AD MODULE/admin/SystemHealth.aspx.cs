using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using System.Web;
using System.Web.Services;
using Castle.MicroKernel.Lifestyle;
using DeskAlerts.ApplicationCore.Dtos;
using DeskAlerts.Server.Utils.Consts;
using DeskAlerts.Server.Utils.Interfactes;
using DeskAlerts.Server.Utils.Managers;
using DeskAlertsDotNet.Application;
using DeskAlertsDotNet.Pages;
using DeskAlertsDotNet.Parameters;
using NLog;
using Resources;

namespace DeskAlerts.Server.sever.MODULES.AD_MODULE.admin
{
    public partial class SystemHealth : DeskAlertsBasePage
    {
        private new static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private static readonly string ClientCrashFolderPath =
            HttpContext.Current.Server.MapPath(FolderVariables.ClientCrashFolder);

        private readonly List<string> _crashLogFiles = GetFilesFromDirectory(ClientCrashFolderPath);

        private static ISettingsService _settingsService;

        private readonly SmtpCredentialsDto _smtpServerData = new SmtpCredentialsDto
        {
            SmtpAuthentication = Settings.Content.GetInt("ConfSmtpAuth") == 1,
            SmtpConnectionTimeout = Settings.Content["ConfSmtpTimeout"],
            SmtpFrom = Settings.Content["ConfSmtpFrom"],
            SmtpPort = Settings.Content.GetInt("ConfSmtpPort"),
            SmtpServer = Settings.Content["ConfSmtpServer"],
            SmtpSsl = Settings.Content.GetInt("ConfSmtpSsl") == 1,
            SmtpUserName = Settings.Content["ConfSmtpUser"],
            SmtpUserNamePassword = Settings.Content["ConfSmtpPassword"]
        };

        protected override void OnLoad(EventArgs e)
        {
            using (Global.Container.BeginScope())
            {
                _settingsService = Global.Container.Resolve<ISettingsService>();
            }

            base.OnLoad(e);

            if (!ClientCrashFolderIsEmpty())
            {
                SmtpServerInformation.InnerText = "Checking your SMTP server connection...";
                SendCrashesToSupportButton.Text = resources.LNG_SEND_CRASHES_TO_SUPPORT;
            }

            ClientCrashesInformation.InnerText = ClientCrashFolderIsEmpty() 
                ? resources.LNG_CLIENT_CRASH_FOLDER_IS_EMPTY 
                : string.Format(resources.CLIENTS_DUMPS_INFO, ClientCrashFolderPath);
        }

        private static List<string> GetFilesFromDirectory(string directoryPath)
        {
            var result = new List<string>();

            try
            {
                result.AddRange(Directory.GetFiles(directoryPath));

                foreach (var subdirectory in Directory.GetDirectories(directoryPath))
                {
                    result.AddRange(GetFilesFromDirectory(subdirectory));
                }
            }
            catch (Exception e)
            {
                Logger.Error(e);
            }

            return result;
        }

        public bool ClientCrashFolderIsEmpty()
        {
            if (!Directory.Exists(ClientCrashFolderPath))
            {
                return true;
            }

            return _crashLogFiles.Count == 0;
        }

        protected void SendCrashesToSupportButton_Click(object sender, EventArgs e)
        {
            EmailResult result;

            using (var message = new MailMessage())
            {
                var fromAddress = new MailAddress(_smtpServerData.SmtpFrom);

                message.From = fromAddress;
                message.Subject = "Client application crashes";
                message.IsBodyHtml = true;
                message.Body = "Client application crashes";
                message.To.Add("support@deskalerts.com");

                foreach (var crashLogFile in _crashLogFiles)
                {
                    var attachment = new Attachment(crashLogFile);

                    message.Attachments.Add(attachment);
                }

                result = _settingsService.SendEmail(_smtpServerData, message);
            }

            if (result.StatusCode == SmtpStatusCode.Ok)
            {
                foreach (var crashLogFile in _crashLogFiles)
                {
                    File.Delete(crashLogFile);
                }
            }

            Page.Response.Redirect(Page.Request.Url.ToString(), true);
        }

        [WebMethod]
        public static bool IsSmtpServerConnected()
        {
            var testSmtpServerResult = new DeskAlertsApiResponse<EmailResult>();
            var smtpServerData = new SmtpCredentialsDto
            {
                SmtpAuthentication = Settings.Content.GetInt("ConfSmtpAuth") == 1,
                SmtpConnectionTimeout = Settings.Content["ConfSmtpTimeout"],
                SmtpFrom = Settings.Content["ConfSmtpFrom"],
                SmtpPort = Settings.Content.GetInt("ConfSmtpPort"),
                SmtpServer = Settings.Content["ConfSmtpServer"],
                SmtpSsl = Settings.Content.GetInt("ConfSmtpSsl") == 1,
                SmtpUserName = Settings.Content["ConfSmtpUser"],
                SmtpUserNamePassword = Settings.Content["ConfSmtpPassword"]
            };

            try
            {
                testSmtpServerResult = _settingsService.TestSmtpServerConnection(smtpServerData);
            }
            catch (Exception e)
            {
                DeskAlertsBasePage.Logger.Error(e);
            }

            return testSmtpServerResult.Data.StatusCode == SmtpStatusCode.Ok;
        }
    }
}