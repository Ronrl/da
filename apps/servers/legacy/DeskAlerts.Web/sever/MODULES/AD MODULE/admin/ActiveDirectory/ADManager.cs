﻿namespace DeskAlertsDotNet.ActiveDirectory
{
    using System;
    using System.DirectoryServices;

    using DeskAlertsDotNet.Models;

    public class ADManager
    {
        public static DirectoryEntries GetAllTree(Synchronization s)
        {
            return GetAllTree(s.adLogin, s.adPassword, s.domain, s.port, s.secure == 1);
        }

        public static SearchResultCollection GetOUs(Synchronization s)
        {
            return GetOUs(s.adLogin, s.adPassword, s.domain, s.port, s.secure == 1);
        }

        public static SearchResultCollection GetFilteredObjects(Synchronization s, string filter, string attrs = null)
        {
            DirectorySearcher searcher = SearchForObjects(s.adLogin, s.adPassword, s.domain, s.secure == 1, filter, attrs);

            return searcher.FindAll();
        }

        public static SearchResultCollection GetOUs(string login, string password, string domain, int port, bool isSsl)
        {
            try
            {

                DirectorySearcher searcher = SearchForObjects(login, password, domain, isSsl, "(objectClass=organizationalUnit)");
                return searcher.FindAll();
            }
            catch (System.Runtime.InteropServices.COMException ex)
            {
                Pages.DeskAlertsBasePage.Logger.Error(ex);
                return null;
            }
            catch (Exception ex)
            {
                Pages.DeskAlertsBasePage.Logger.Error(ex);
                return null;
            }
        }

        public static DirectoryEntries GetAllTree(string login, string password, string domain, int port, bool isSsl)
        {
            try
            {
                DirectorySearcher searcher = SearchForObjects(login, password, domain, isSsl, "(objectClass=*)");

                return searcher.FindOne().GetDirectoryEntry().Children;
            }
            catch (System.Runtime.InteropServices.COMException ex)
            {
                Pages.DeskAlertsBasePage.Logger.Error(ex);
                return null;
            }
            catch (Exception ex)
            {
                Pages.DeskAlertsBasePage.Logger.Error(ex);
                return null;
            }
        }

        private static DirectorySearcher SearchForObjects(string login, string password, string domain, bool isSsl, string filter, string attrs = null)
        {
            DirectoryEntry entry = TryToConnect(login, password, domain, isSsl);

            if (entry == null)
                throw new System.Runtime.InteropServices.COMException();

            DirectorySearcher searcher = new DirectorySearcher(entry);

            searcher.Filter = filter;

            if (!String.IsNullOrEmpty(attrs))
                searcher.AttributeScopeQuery = attrs;

            return searcher;
        }

        private static DirectoryEntry TryToConnect(string login, string password, string domain, bool isSsl)
        {


            password = CorrectBase64String(password);


            byte[] passwordBytes = Convert.FromBase64String(password);

            password = System.Text.Encoding.UTF8.GetString(passwordBytes);

            DirectoryEntry entry = new DirectoryEntry("LDAP://" + domain, login, password);

            if (isSsl)
                entry.AuthenticationType = AuthenticationTypes.Secure;

            return entry;


        }

        public static DirectoryEntry GetOrganizationalUnitByName(Synchronization s, string ouName)
        {
            return GetOrganizationalUnitByName(s.adLogin, s.adPassword, s.domain, s.port, s.secure == 1, ouName);
        }
        public static DirectoryEntry GetOrganizationalUnitByName(string login, string password, string domain, int port, bool isSsl, string ouName)
        {
            try
            {
                DirectoryEntry entry = TryToConnect(login, password, domain, isSsl);


                DirectorySearcher searcher = new DirectorySearcher(entry);

                //Regex ouFinder = new Regex("OU=(.[^,]*[^ ,][^,])");

                //MatchCollection mc = ouFinder.Matches(ouName);

                //if(mc.Count > 0)
                //{
                //    ouName = mc[0].Groups[1].Value;
                //}

                searcher.Filter = "(&(objectCategory=organizationalUnit)(distinguishedName=" + ouName + "))";

                searcher.SearchScope = SearchScope.Subtree;

                return searcher.FindOne().GetDirectoryEntry();
            }
            catch (System.Runtime.InteropServices.COMException ex)
            {
                Pages.DeskAlertsBasePage.Logger.Error(ex);
                return null;
            }
            catch (Exception ex)
            {
                Pages.DeskAlertsBasePage.Logger.Error(ex);
                return null;
            }

        }

        static string CorrectBase64String(string base64)
        {
            base64 = base64.Replace(" ", "+");

            int mod4 = base64.Length % 4;

            if (mod4 > 0)
            {
                base64 += new string('=', 4 - mod4);
            }

            return base64;
        }

        public static SearchResultCollection GetGroups(string login, string password, string domain, int port, bool isSsl, string searchTerm)
        {
            try
            {
                password = CorrectBase64String(password);

                byte[] passwordBytes = Convert.FromBase64String(password);

                password = System.Text.Encoding.UTF8.GetString(passwordBytes);

                DirectoryEntry entry = new DirectoryEntry("LDAP://" + domain, login, password);

                if (isSsl)
                    entry.AuthenticationType = AuthenticationTypes.Secure;
                DirectorySearcher searcher = new DirectorySearcher(entry);

                searcher.Filter = String.Format("(&(objectClass=group)(objectCategory=Group)(cn=*{0}*))", searchTerm);
                searcher.SearchScope = SearchScope.Subtree;

                return searcher.FindAll();
            }
            catch (System.Runtime.InteropServices.COMException ex)
            {
                Pages.DeskAlertsBasePage.Logger.Error(ex);
                return null;
            }
            catch (Exception ex)
            {
                Pages.DeskAlertsBasePage.Logger.Error(ex);
                return null;
            }
        }
    }
}