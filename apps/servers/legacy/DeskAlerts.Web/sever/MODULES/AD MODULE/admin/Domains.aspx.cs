﻿using System;
using System.Web.UI.WebControls;
using Castle.MicroKernel.Lifestyle;
using DeskAlerts.Server.Utils.Interfactes;
using DeskAlertsDotNet.Application;
using Resources;

namespace DeskAlertsDotNet.Pages
{
    public partial class Domains : DeskAlertsBaseListPage
    {
        private readonly IUserService _userService;
        private readonly ITokenService _tokenService;

        public Domains()
        {
            using (Global.Container.BeginScope())
            {
                _userService = Global.Container.Resolve<IUserService>();
                _tokenService = Global.Container.Resolve<ITokenService>();
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            searchButton.Text = searchButton.Text.Replace("{Search}", resources.LNG_SEARCH);
            domainName.Text = resources.LNG_DOMAIN;
            usersCount.Text = resources.LNG_USERS;
            groupsCount.Text = resources.LNG_GROUPS;
            computersCount.Text = resources.LNG_COMPUTERS;
            Table = contentTable;
            FillTableWithContent(true);
        }

        protected override string DataTable
        {
            get { return "domains"; }
        }

        protected override string[] Collumns
        {
            get {

                return new string[] { "name", "(SELECT COUNT(1) FROM users WHERE domain_id=domains.id) AS cnt0", 
                    "(SELECT COUNT(1) FROM groups WHERE domain_id=domains.id) AS cnt1",
                "(SELECT COUNT(1) FROM computers WHERE domain_id=domains.id) AS cnt2"};
            }
        }

        protected override string ConditionSqlString
        {
            get
            {
                string sqlQuery = "";
                if (searchTermBox.Value.Length > 0)
                    sqlQuery += ( "name LIKE '%" + searchTermBox.Value.Replace("'", "''") + "%'" );

             //   if (Config.Data.GetInt("ConfEnableRegAndADEDMode") == 1)
              //  {
                   if (searchTermBox.Value.Length > 0)
                       sqlQuery += " AND";

                    sqlQuery += " domains.name <> ''";
              //  }


                return sqlQuery;
            }
        }


        protected override IButtonControl[] DeleteButtons
        {
            get { return new IButtonControl[] { upDelete, bottomDelete }; }
        }

        protected override string DefaultOrderCollumn
        {
            get { return "id"; }
        }


        protected override System.Web.UI.HtmlControls.HtmlGenericControl NoRowsDiv
        {
            get { return NoRows; }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl TableDiv
        {
            get { return mainTableDiv; }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl TopPaging
        {
            get
            {
                return topPaging;
            }
        }

        protected override string ContentName
        {
            get
            {
                return resources.LNG_DOMAINS;
            }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl BottomPaging
        {
            get
            {
                return bottomRanging;
            }
        }

        protected override void OnDeleteRows(string[] ids)
        {
            foreach (var id in ids)
            {

                string query = "SET NOCOUNT ON " +

                "IF OBJECT_ID('tempdb..#tempUsers') IS NOT NULL DROP TABLE #tempUsers " +
                "IF OBJECT_ID('tempdb..#tempGroups') IS NOT NULL DROP TABLE #tempGroups " +
                "IF OBJECT_ID('tempdb..#tempComps') IS NOT NULL DROP TABLE #tempComps " +
                "IF OBJECT_ID('tempdb..#tempOUs') IS NOT NULL DROP TABLE #tempOUs " +

                "CREATE TABLE #tempUsers (id BIGINT NOT NULL) " +
                "CREATE TABLE #tempGroups (id BIGINT NOT NULL) " +
                "CREATE TABLE #tempComps (id BIGINT NOT NULL) " +
                "CREATE TABLE #tempOUs (id BIGINT NOT NULL) " +

                "INSERT INTO #tempUsers (id) (SELECT id FROM users WHERE domain_id = " + id + ") " +
                "INSERT INTO #tempGroups (id) (SELECT id FROM groups WHERE domain_id = " + id + ") " +
                "INSERT INTO #tempComps (id) (SELECT id FROM computers WHERE domain_id = " + id + ") " +
                "INSERT INTO #tempOUs (id) (SELECT Id FROM OU WHERE Id_domain = " + id + ") " +

                "DELETE FROM users_groups WHERE user_id IN (SELECT id FROM #tempUsers) OR group_id IN (SELECT id FROM #tempGroups) " +
                "DELETE FROM groups_groups WHERE group_id IN (SELECT id FROM #tempGroups) OR container_group_id IN (SELECT id FROM #tempGroups) " +
                "DELETE FROM computers_groups WHERE comp_id IN (SELECT id FROM #tempComps) OR group_id IN (SELECT id FROM #tempGroups) " +
                "DELETE FROM ou_groups WHERE ou_id IN (SELECT id FROM #tempOUs) OR group_id IN (SELECT id FROM #tempGroups) " +

                "DELETE FROM User_synch WHERE Id_user IN (SELECT id FROM #tempUsers) " +
                "DELETE FROM Comp_synch WHERE Id_comp IN (SELECT id FROM #tempComps) " +
                "DELETE FROM Group_synch WHERE Id_group IN (SELECT id FROM #tempGroups) " +
                "DELETE FROM OU_synch WHERE Id_OU IN (SELECT id FROM #tempOUs) " +

                "DELETE FROM OU_User WHERE Id_OU IN (SELECT id FROM #tempOUs) OR Id_user IN (SELECT id FROM #tempUsers) " +
                "DELETE FROM OU_comp WHERE Id_OU IN (SELECT id FROM #tempOUs) OR Id_comp IN (SELECT id FROM #tempComps) " +
                "DELETE FROM OU_Group WHERE Id_OU IN (SELECT id FROM #tempOUs) OR Id_group IN (SELECT id FROM #tempGroups) " +
                "DELETE FROM OU_hierarchy WHERE Id_parent IN (SELECT id FROM #tempOUs) OR Id_child IN (SELECT id FROM #tempOUs) " +
                "DELETE FROM OU_DOMAIN WHERE Id_OU IN (SELECT id FROM #tempOUs) " +
                "DELETE FROM OU_DOMAIN WHERE Id_domain = " + id +

                " DELETE FROM users WHERE id IN (SELECT id FROM #tempUsers)" +
                "DELETE FROM groups WHERE id IN (SELECT id FROM #tempGroups) " +
                "DELETE FROM computers WHERE id IN (SELECT id FROM #tempComps) " +
                "DELETE FROM OU WHERE Id IN (SELECT id FROM #tempOUs) " +

                "DROP TABLE #tempUsers " +
                "DROP TABLE #tempGroups " +
                "DROP TABLE #tempComps " +
                "DROP TABLE #tempOUs";

                dbMgr.ExecuteQuery(query);

                var domainUsersNames = _userService.GetDomainUsersUsernames(int.Parse(id));

                foreach (var username in domainUsersNames)
                {
                    _tokenService.RemoveToken(username);
                }
            }
        }


    }
}