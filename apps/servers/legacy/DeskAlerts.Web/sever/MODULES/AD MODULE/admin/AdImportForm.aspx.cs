﻿using System;
using System.IO;

namespace DeskAlertsDotNet.Pages
{
    public partial class AdImportForm : System.Web.UI.Page
    {
        private const string folderPath = @"\admin\ad_import\files\";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                var fullFolderPath = Server.MapPath("~") + folderPath;
                if (!Directory.Exists(fullFolderPath))
                {
                    Directory.CreateDirectory(fullFolderPath);
                }
                var sequenceFileNames = string.Empty;
                foreach (string fileName in Request.Files)
                {
                    var file = Request.Files.Get(fileName);
                    if (file.ContentLength != 0)
                    {
                        file.SaveAs(fullFolderPath + file.FileName);
                        sequenceFileNames += file.FileName + "|";
                    }
                }
                Response.Redirect("ad_import_parse.asp?overwrite=" + (overwriteSynch.Checked ? "1" : "0") + "&fnames=" + sequenceFileNames);
            }
        }
    }
}