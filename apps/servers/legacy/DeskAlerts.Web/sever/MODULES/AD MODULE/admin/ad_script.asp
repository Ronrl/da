<!-- #include file="config.inc" -->
<!-- #include file="lang.asp" -->
<%

'===================================================================
'============================== Defines ============================
'===================================================================

Dim strAttrClass		: strAttrClass		= "objectClass"

'===================================================================
'===================================================================

Dim strLogin : strLogin=Request("adLogin")
Dim strPlace : strPlace=Request("adPlace")
Dim strPassword64 : strPassword64=Request("adPassword")
Dim strPassword : strPassword=Base64Decode(strPassword64)
Dim strDomain : strDomain = Request("domain")
Dim strPort : strPort = Request("port")
If strPort <> "" Then strPort = ":"&strPort End If

If InStr(strLogin, "\") = 0 And InStr(strLogin, "=") = 0 Then
	strLogin = Split(strDomain, ".")(0)&"\"&strLogin
End If

Dim secure : secure = (Not Request("secure")<>"1")

Dim LDAPQuery
On Error Resume Next
	Set LDAPQuery = CreateObject("DeskAlertsServer.LDAPQuery")
	If(Err.Number <> 0) Then
		Response.Write LNG_ERROR_SERVER_DLL_NOT_REGISTERED
		Response.End
	End If
On Error Goto 0

Dim strFilter
Dim strAttribs

'===================================================================
'========================== Check Settings =========================
'===================================================================
strFilter = "("&strAttrClass&"=*)"
strAttribs = "dn"
LDAPQuery.execQuery strLogin, strPassword, strDomain&strPort, "", "base", strFilter, strAttribs, secure, ""
parseLdapError LDAPQuery.getLastError()
'===================================================================
'===================================================================

Dim sPostData : sPostData = ""
For Each i in Request.Form
	For j = 1 to Request.Form(i).Count
		If Len(sPostData) Then
			sPostData = sPostData & "&"
		End If
		sPostData = sPostData & Server.URLEncode(i) & "=" & Server.URLEncode(Request.Form(i)(j))
	Next
Next

If Len(sPostData) = 0 Then
	Response.Write "Failed. Try again."
	Response.End
End If

Dim pChar, pCount

' Here you can add other characters such as lowercase or special.
pChar = "abcdefghijklmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ0123456789"
pCount = Len(pChar) 

Dim psw
psw = ""

Randomize
For i = 1 To 32 ' password length
	psw = psw & Mid( pChar, 1 + Int(Rnd * pCount), 1 )
Next

Dim file_name : file_name = "ad_import_" & strDomain

Dim url : url = alerts_folder & "admin/ad_action.asp"
Dim path : path = alerts_dir & "admin\scripts\"&file_name&".asp"

Dim fs
Dim file
set fs=Server.CreateObject("Scripting.FileSystemObject")
set file = fs.OpenTextFile(path, 2, 1)
file.WriteLine("<"&"% Server.ScriptTimeout = 90000 %"&">")
file.WriteLine("<"&"%")
file.WriteLine("If Request(""sid"") <> """ & psw & """ Then")
file.WriteLine("Response.Write ""Wrong SID""")
file.WriteLine("Response.End")
file.WriteLine("End If")
file.WriteLine("Set oXMLHTTP = Server.CreateObject(""Msxml2.serverXMLHTTP.6.0"")")
file.WriteLine("oXMLHTTP.Open ""POST"", """ & url & """, false")
file.WriteLine("oXMLHTTP.SetRequestHeader ""Content-Type"", ""application/x-www-form-urlencoded""")
file.WriteLine("oXMLHTTP.SetTimeouts 60, 60, 90000, 90000")
file.WriteLine("oXMLHTTP.Send """ & sPostData & """")
file.WriteLine("'Response.Write oXMLHTTP.responseText")
file.WriteLine("Set oXMLHTTP = nothing")
file.WriteLine("Response.Write ""Done""")
file.WriteLine("%"&">")
file.Close

url = alerts_folder & "admin/scripts/"&file_name&".asp?sid="
path = alerts_dir & "admin\scripts\"&file_name&".vbs"
set file = fs.OpenTextFile(path, 2, 1)
file.WriteLine("If WScript.Arguments.Count > 0 Then")
file.WriteLine("Dim sid : sid = WScript.Arguments(0)")
file.WriteLine("Set oXMLHTTP = CreateObject(""Msxml2.serverXMLHTTP.6.0"")")
file.WriteLine("oXMLHTTP.Open ""GET"", """ & url & """ & sid, false")
file.WriteLine("oXMLHTTP.SetTimeouts 60, 60, 90000, 90000")
file.WriteLine("oXMLHTTP.Send """"")
file.WriteLine("WScript.Echo oXMLHTTP.responseText")
file.WriteLine("Set oXMLHTTP = nothing")
file.WriteLine("Else")
file.WriteLine("WScript.Echo ""Wrong arguments""")
file.WriteLine("End If")
file.Close

Response.contenttype = "application/binary"
Response.addHeader "Content-Type", "application/binary; filename="""&file_name&".bat"""
Response.addHeader "Content-Disposition","attachment; filename="""&file_name&".bat"""

Response.Write "cscript /nologo """ & alerts_dir & "admin\scripts\"&file_name&".vbs"" """ & psw & """"

'===================================================================
'=========================== Functions =============================
'===================================================================
Sub parseLdapError(errorCode)
	If errorCode <> 0 Then
		Response.Write "<p>"
		Select Case errorCode
		Case 13
			Response.Write "Detected TSL connection. Go back, check ""Use secure LDAP connection"" option and try again."
		Case 34
			Response.Write "The distinguished name (User DN) has an invalid syntax."
		Case 49
			Response.Write "Invalid user or password."
		Case 81
			Response.Write "Cannot connect to specified LDAP Server."
		Case Else
			Response.Write "Error occured while accessing LDAP. Error number: " & errorCode
		End Select
		Response.Write "</p><input type=""button"" onclick=""history.go(-1)"" value=""Back""/>"
		Response.End
	End If
End Sub

%>
