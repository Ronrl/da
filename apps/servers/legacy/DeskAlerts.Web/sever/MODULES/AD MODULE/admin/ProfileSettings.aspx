﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProfileSettings.aspx.cs" Inherits="DeskAlertsDotNet.Pages.ProfileSettings" %>
<%@ Import NameSpace="DeskAlertsDotNet.Parameters" %>
<%@ Import NameSpace="Resources" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link href="css/style9.css" rel="stylesheet" type="text/css">
	<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
	<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
    
<script language="javascript" type="text/javascript">

		$(document).ready(function() {
			$("#addUserButton").button();
			$("#addUserButton").bind("click", function() {
				if (!$("#userName").val())
				{
					alert("User name must not be empty!");
				}
				else
				{
					$('#user_form').submit();
				}
			});
			
			$("#cancel_change_password_button").button();
			$("#cancel_change_password_button").bind("click", function() {
				$('#change_password_tr').hide(100);
				$('#passwordChanged').val("0");
			});;
			
			$("#change_password_button").button();
			$("#change_password_button").bind("click", function() {
				$('#change_password_tr').show(1000);
				$('#passwordChanged').val("1");
			});

            if ($("#userName").val() == "demo" && "<%=Config.Data.IsDemo %>" == "True"){
				$("#defaultUserWarning").show();
                $("#userName, #email, #mobilePhone, #userGuideSelect, #userLangSelect, #startPage").prop('disabled', true);				
                $("#addUserButton").hide();
                $("#change_password_button").hide();
			}

		});
	</script>
    	<style>
		.data_table tr td {border:0px none; padding-left: 5px; padding-right: 5px}
	</style>

</head>
<body>
    <form id="form1" runat="server">
    <div>
 <table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	<tr>
	<td>
	<table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
		<tr>
			<td  height=31 class="main_table_title">
				<img src="images/menu_icons/settings_20.png" alt="" width="20" height="20" border="0" style="padding-left:7px">
					<a href="#" class="header_title" style="position:absolute;top:22px"><%= resources.LNG_PROFILE_SETTINGS%></a>
			</td>
		</tr>
		<tr>
		<td style="padding:5px; width:100%" height="100%"   valign="top" class="main_table_body">
			<div style="margin:10px;">
				<form name="my_form" method="post" id="user_form">
					<font color="red" runat="server" id="errorLabel"><%=ErrMsg%></font>
					<p id="defaultUserWarning" style="display:none">
						You are logged in with a shared "demo" account. To be able to customize profile settings, create your own account from the Publishers menu.
					</p>
					<table style="padding:0px; margin-top:5px; margin-bottom:5px; border-collapse:collapse; border:0px">
						<tr>
							<td>
								<label for="name"><%= resources.LNG_NAME%>:</label>
							</td>
						</tr>
						<tr>
							<td>
								<input runat="server" id="userName" type="text" value="" name="name" disabled></input>
							</td>
						</tr>
						<tr>
							<td style="padding-top:10px;">
								<input type="hidden" runat="server" id="passwordChanged" name="passwordChanged"/>
								<a id="change_password_button"><%= resources.LNG_CHANGE_PASSWORD%></a>
							</td>
						</tr>
						<tr id="change_password_tr" style="display:none;">
							<td style="padding-top:10px">
								<table cellspacing="0" cellpadding="0" class="data_table">
									<tr id="password_old_label_tr">
										<td style="padding-top:10px;">
											<label for="old_password"><%= resources.LNG_ENTER_OLD_PASSWORD+ ":" %></label>
										</td>
									</tr>
									<tr id="password_old_value_tr">
										<td>
											<input runat="server" id="password" type="password" value="" name="old_password"></input>
										</td>
									</tr>
									<tr id="password_label_tr">
										<td style="padding-top:10px;">
											<label for="password"><%= resources.LNG_ENTER_NEW_PASSWORD + ":" %></label>
										</td>
									</tr>
									<tr id="password_value_tr">
										<td>
											<input runat="server" id="newPassword" type="password" value="" name="password"></input>
										</td>
									</tr>
									<tr id="password_confirm_label_tr">
										<td style="padding-top:10px;">
											<label for="confirm_password"><%= resources.LNG_CONFIRM_NEW_PASSWORD + ":" %></label>
										</td>
									</tr>
									<tr id="password_confirm_value_tr">
										<td>
											<input runat="server" id="confirmNewPassword" type="password" value="" name="confirm_password"></input>
										</td> 
									</tr>
									<tr id="password_change_cancel_tr">
										<td style="padding-top:5px; padding-bottom:5px; text-align:right">
											<a id="cancel_change_password_button"><%= resources.LNG_CANCEL%></a>
										</td>
									</tr>	
								</table>
							</td>
						</tr>
						
						<tr>
							<td style="padding-top:10px;">
								<label for="userLang"><%= resources.LNG_USER_LANGUAGE%>:</label>
							</td>
						<tr>
						<tr>
							<td>
								<asp:DropDownList runat="server" style='width:175px;' id="userLangSelect" name="userLang">
								</asp:DropDownList>
							</td>
							<td><span style="padding-left:10px" runat="server" id="LanguageAdditionalDescription"></span></td>
						</tr>
						<!--Start-->
						<tr>
							<td style='padding-top:10px;'>
								<label for="start_page"><%= resources.LNG_START_PAGE%></label>
							</td>
						</tr>
						<tr>
							<td>
								<asp:DropDownList  runat="server" style='width:175px;' id='startPage' name='start_page'>
									<asp:ListItem Value="dashboard.aspx" Text="Choose start page"></asp:ListItem>
								</asp:DropDownList>
							</td>
						</tr>
						<!--End-->
						<tr>
							<td style="padding-top:10px;" align="right">
								<asp:LinkButton runat="server" id='addUserButton'><%= resources.LNG_SAVE%></asp:LinkButton>
							</td>
						</tr>
					</table>
					
					<input type="hidden" name="id" value=""></input>
					<input type="hidden" name="save" value="1"></input>
				</form>
			</div>
		</td>
	</tr>
</table>
 </table>
    </div>
    </form>
</body>
</html>
