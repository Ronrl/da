﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using AutoMapper;
using DeskAlerts.ApplicationCore.Dtos.AD;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Models.Permission;
using DeskAlertsDotNet.Parameters;
using Resources;


namespace DeskAlertsDotNet.Pages
{
    public partial class Organizations : DeskAlertsBasePage
    {
        private List<OUDto> _ouSet = new List<OUDto>();

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            if (!IsPostBack)
            {
                if (curUser.HasPrivilege || curUser.Policy[Policies.USERS].CanView)
                {
                    objType.Items.Add(new ListItem()
                    {
                        Text = resources.LNG_USERS,
                        Value = "OrgUsers.aspx"
                    });
                }

                if (curUser.HasPrivilege || curUser.Policy[Policies.GROUPS].CanView)
                {
                    objType.Items.Add(new ListItem()
                    {
                        Text = resources.LNG_GROUPS,
                        Value = "OrgGroups.aspx"
                    });
                }

                if (curUser.HasPrivilege || curUser.Policy[Policies.USERS].CanSomething)
                {
                    objType.Items.Add(new ListItem()
                    {
                        Text = resources.LNG_COMPUTERS,
                        Value = "OrgComps.aspx"
                    });
                }

                objType.SelectedIndexChanged += objType_Changed;
                var firstUrl = objType.Items[0].Value;

                objectsFrame.Src = firstUrl;
            }
            else
            {
                if (Request["__EVENTTARGET"] == "orgTree" && Request["__EVENTARGUMENT"] == "sOrganization/Audience")
                {
                    objectsFrame.Src = Request["objType"];
                }
                else
                {
                    objectsFrame.Src = objType.SelectedValue;
                }

            }

            orgTree.SelectedNodeChanged += orgTree_SelectedNodeChanged;
            orgTree.ShowLines = true;

            BuildOrganizationTree();

            addUserButton.Visible = curUser.HasPrivilege || curUser.Policy[Policies.USERS].CanCreate;
            addGroupButton.Visible = curUser.HasPrivilege || curUser.Policy[Policies.GROUPS].CanCreate;

            ChangeVisibleStatusOfManualImportPhoneButton();
        }

        void objType_Changed(object sender, EventArgs e)
        {
            objectsFrame.Src = objType.SelectedValue;
        }

        void orgTree_SelectedNodeChanged(object sender, EventArgs e)
        {

        }

        void BuildOrganizationTree()
        {
            TreeNode root = new TreeNode(resources.LNG_ORGANIZATION);

            root.Selected = true;
            root.Expanded = true;
            root.ImageUrl = "../images/ad_icons/organization.png";
            root.SelectAction = TreeNodeSelectAction.Select;
            BuildOrganizationTree(root);
            orgTree.Nodes.Add(root);
        }

        void BuildOrganizationTree(TreeNode root)
        {
            orgTree.Nodes.Clear();
            DataSet domainsSet = dbMgr.GetDataByQuery("SELECT id, name FROM domains");

            foreach (DataRow domainRow in domainsSet)
            {
                int domainId = domainRow.GetInt("id");
                string domainName = domainRow.GetString("name");

                if (domainName.Trim().Length == 0)
                    domainName = resources.LNG_REGISTERED;

                TreeNode domainNode = new TreeNode(domainName);
                domainNode.Expanded = false;
                domainNode.ImageUrl = "../images/ad_icons/domain.png";

                domainNode.NavigateUrl = $"javascript:changeFrameContent('{GetCurrentOpenedFile() + "?type=DOMAIN&id=" + domainId}');";
                domainNode.SelectAction = TreeNodeSelectAction.Select;
                root.ChildNodes.Add(domainNode);

                string getOUsByDomainIdQuery = $"SELECT ou.id, ou.Name, h.Id_parent, ou.Id_domain FROM OU INNER JOIN OU_hierarchy as h ON h.Id_child = ou.Id WHERE ou.Id_domain = {domainId} ORDER BY ou.Name ASC";
                dbMgr.GetDataByQuery(getOUsByDomainIdQuery).ToList().ForEach(el =>
                {
                    _ouSet.Add(Mapper.Map<DataRow, OUDto>(el));
                });

                AddOUToTree(domainNode, -1, domainId);
            }
        }

        void AddOUToTree(TreeNode parentNode, int parentId, int domainId)
        {
            var ouSet = _ouSet?.Where(ou => ou.ParentId == parentId && ou.DomainId == domainId).ToList();
            parentNode.SelectAction = TreeNodeSelectAction.Select;

            if (ouSet != null)
            {
                foreach (var ouRow in ouSet)
                {
                    var ouNode = new TreeNode(ouRow.Name)
                    {
                        Expanded = false,
                        ImageUrl = "../images/ad_icons/ou.png",
                        NavigateUrl = $"javascript:changeFrameContent('{GetCurrentOpenedFile() + "?type=ou&id=" + ouRow.Id}');"
                    };

                    parentNode.ChildNodes.Add(ouNode);
                    AddOUToTree(ouNode, ouRow.Id, domainId);
                }
            }
        }

        string GetCurrentOpenedFile()
        {
            string source = objectsFrame.Src;

            source = source.Substring(0, source.IndexOf(".aspx") + 5);

            return source;
        }


        private void ChangeVisibleStatusOfManualImportPhoneButton()
        {
            if (!CheckNecessaryModulesForImportPhones())
            {
                import_phones_button.Visible = false;
                return;
            }

            const string sqlQuery = 
                @"SELECT [settings].[val]
                  FROM [settings]
                  WHERE [settings].[name] LIKE 'ConfEnableManualImportPhones';";

            var enableManualImportPhonesDataRow = dbMgr.GetDataByQuery(sqlQuery)?.First();

            var enableManualImportPhones = false;

            if (enableManualImportPhonesDataRow != null && !enableManualImportPhonesDataRow.IsNull("val"))
            {
                enableManualImportPhones = enableManualImportPhonesDataRow.GetString("val") == "1";
            }

            import_phones_button.Visible = enableManualImportPhones;
        }

        public static bool CheckNecessaryModulesForImportPhones()
        {
            return Config.Data.HasModuleAndValue(Modules.IMPORT_PHONES) &&
                   (Config.Data.HasModuleAndValue(Modules.SMS) ||
                    Config.Data.HasModuleAndValue(Modules.MOBILE) ||
                    Config.Data.HasModuleAndValue(Modules.TEXTTOCALL));
        }
    }
}