<%
'===================================================================
'============================== Defines ============================
'===================================================================

Const DOMAIN_ID_INDEX	= 0
Const DOMAIN_NAME_INDEX	= 1
Const GROUP_RID_COMPUTERS = 515

'===================================================================
'===================================================================

Dim strAttrClass		: strAttrClass		= "objectClass"
Dim strAttrPath			: strAttrPath		= "canonicalName"
Dim strDisplayName		: strDisplayName	= "displayName"

Dim strUserClass		: strUserClass		= "user"
Dim strUserFilter		: strUserFilter		= "(&(objectCategory=Person)(sAMAccountName=*))"
Dim strUserName			: strUserName		= "sAMAccountName"
Dim strUSnChanged       : strUSnChanged     = "uSNChanged"
Dim strUserDisplay		: strUserDisplay	= "displayName"
Dim strUserMail			: strUserMail		= "mail"
Dim strUserPhone		: strUserPhone		= "mobile"
Dim strUserDisabled		: strUserDisabled	= "userAccountControl"
Dim intUserDisBit		: intUserDisBit		= 2
Dim intUserEnbCheck		: intUserEnbCheck	= 0

Dim strCompClass		: strCompClass		= "computer"
Dim strCompFilter		: strCompFilter		= "(objectCategory=Computer)"
Dim strCompName			: strCompName		= "cn"

Dim strGroupClass		: strGroupClass		= "group"
Dim strGroupFilter		: strGroupFilter	= "(objectCategory=Group)"
Dim strGroupName		: strGroupName		= "cn"
Dim strGroupMembers		: strGroupMembers	= "member"
Dim strGroupType        : strGroupType      = "groupType"
Dim strMemberOf			: strMemberOf		= "memberOf"

Dim strOuClass			: strOuClass		= "organizationalUnit"
Dim strOuFilter			: strOuFilter		= ""'"(objectCategory=Organizational-Unit)"
Dim strOuName			: strOuName			= "ou"

Dim strDomainClass		: strDomainClass	= "domain"
Dim strDomainFilter		: strDomainFilter	= ""
Dim strDomainName		: strDomainName		= "dc"

Dim strObjectGuid       : strObjectGuid     = "objectGUID"
Dim strPrimaryGroupID   : strPrimaryGroupID = "primaryGroupID"


Dim arrGroups           : arrGroups            = Array()
Dim arrUsers            : arrUsers             = Array()
Dim groupChecking       : groupChecking        = False

Dim TotalLogDuration     : TotalLogDuration     = 0
Dim ADsyncLogFileName		 : ADsyncLogFileName	    = ""
Dim isEnableADLogDetails : isEnableADLogDetails = false
Dim Timer0 : Timer0 = Timer
Dim Timer1 : Timer1 = Timer
Dim Timer2 : Timer2 = Timer
Dim Timer3 : Timer3 = Timer
Dim Timer4 : Timer4 = Timer
Dim Timer5 : Timer5 = Timer
Dim Timer6 : Timer6 = Timer
Dim Timer7 : Timer7 = Timer
Dim Timer8 : Timer8 = Timer

Dim ADS_GROUP_TYPE_GLOBAL_GROUP : ADS_GROUP_TYPE_GLOBAL_GROUP = &H00000002
Dim synchedGroupsGuids
Set synchedGroupsGuids = CreateObject("Scripting.Dictionary")
'===================================================================
'===================================================================
%>
<script language="JScript" src="jscripts/json2.js" runat="server"></script>
<script language="JScript" runat="server">

var sql = ""
var syncData;
var syncDataStr = "";
var auto_sync = "";
var interval_sync = "";
var strDomain = "";
var strLogin = "";
var strPassword64 = "";
var port = "389";
var secure = "";
var ad_select = "";
var groups_select = "";
var db_remove = "";
var impDis = "";
var impComp = "";
var impOu = "";
var selectedOUs = "";
var groups = "";
var groupsList = {Count: 0};
var groups_names = "";
var strPassword = "";
var all_groups = 1;
var all_ous = 1;
var all_child = 1;
var max_usn = 0;
var legacySync = "";
var ip = "";

// Returns the array as a VBArray.
function toVBArray(arr)
{
	var dict = new ActiveXObject("Scripting.Dictionary");
	if (arr)
	{
		if(typeof(arr) == 'string')
			dict.add(i, arr);
		else
			for(var i = 0, len = arr.length; i < len; i++)
			{
				dict.add(i, arr[i]);
			}
		return dict.Items();
	}
	
	return dict;
};

function getSynchData(Conn,sync_id)
{
	if (sync_id != "")
	{
		if (typeof sync_id == "object") {sync_id = sync_id.value }
		var result = Conn.Execute("SELECT sync_data FROM synchronizations WHERE id='"+ sync_id.replace("'","''")+"'");
		if (!result.EOF)
		{
		    syncData = result.Fields.Item('sync_data').value;
		    syncData = JSON.parse(syncData);
		    syncDataStr = JSON.stringify(syncData);
			auto_sync = syncData["auto_sync"];
			interval_sync = syncData["interval_sync"];
			strDomain = syncData["domain"];
			strLogin = syncData["adLogin"];
			strPassword64 = syncData["adPassword"];
			port = syncData["port"];
			secure = syncData["secure"];
			ad_select = syncData["ad_select"];
			groups_select = syncData["groups_select"];
			db_remove = syncData["db_remove"];
			impDis = syncData["impDis"];
			impComp = syncData["impComp"];
			selectedOUs = toVBArray(syncData["selectedOUs"]);
			groupsList = toVBArray(syncData["added_group"]);
			groups_names = syncData["groups_names"];
			groups = syncData["groups"];
			max_usn = syncData["max_usn"];
			legacySync = syncData["legacySync"];
			ip = syncData["ip"];
			all_child = syncData["all_child"]||"1";
		}       
		result.Close();
	}
}
</script>	

<%

sync_id = Request("sync_id")

strLogin=Request("adLogin")
strPlace=Request("adPlace")
strPassword64=Request("adPassword")
strDomain = Request("domain")
strPort = Request("port")
all_child = Request("all_child")

max_usn = Request("max_usn")
max_usn_tmp = CLng(max_usn)
syncDataStr = Request("syncDataStr")
legacySync = Request("legacySync")
ip = Request("ip")
updatedUsers = 0
updatedGroups = 0
updatedComputers = 0
updatedOu = 0
dbOperations = 0
Dim usersNames : usersNames = Array()
Dim groupsNames : groupsNames = Array()
Dim computersNames : computersNames = Array()
Dim ouNames : ouNames = Array()
Dim usersNamesFromDomain : usersNamesFromDomain = Array()
Dim groupsNamesFromDomain : groupsNamesFromDomain = Array()
Dim computersNamesFromDomain : computersNamesFromDomain = Array()
Dim ouNamesFromDomain : ouNamesFromDomain = Array()

impDis = (Not Request("impDis")<>"1")

ad_select = Request("ad_select")
'Dim impOU, selectedOUs, all_ous

'Dim groupsList, all_groups
Set groupsList = Request("added_group")

impComp = (Not Request("impComp")<>"1")
secure = (Not Request("secure")<>"1")
db_remove = (Not Request("db_remove")<>"1")

Set selectedOUs = Request("selectedOUs")

parseParams()

Dim LDAPQuery
On Error Resume Next
	Err.Clear
	Set LDAPQuery = CreateObject("DeskAlertsServer.LDAPQuery")
	If(Err.Number <> 0) Then
		Response.Write LNG_ERROR_SERVER_DLL_NOT_REGISTERED
		Response.End
	End If
On Error Goto 0

Dim strFilter
Dim strAttribs
Dim OUsQueryResult : OUsQueryResult = Null
Dim usersQueryResult : usersQueryResult = Null
Dim compsQueryResult : compsQueryResult = Null
Dim groupsQueryResult : groupsQueryResult = Null

Dim logFile
Dim logFile2
Const ForAppending=8
Const ForCreate=True
Const TristateTrue=-1 'Open the file as Unicode.
Sub WriteLineToResponseAndLog(str)
	Response.Write str & "<br/>"
	Response.Flush
	if ConfEnableSyncLog = 1 then
		if not isObject(fs) then
			set fs=Server.CreateObject("Scripting.FileSystemObject")
		end if
		if isEmpty(logFile) then
			fname = Server.MapPath("logs\" & Year(now_db) &"-"& TwoDigits(Month(now_db)) &"-"& TwoDigits(Day(now_db)) &"_"& TwoDigits(Hour(now_db)) &"-"& TwoDigits(Minute(now_db)) &"-"& TwoDigits(Second(now_db)) &".log")
			if (InStr(fname,"\admin\logs\")=0) then
				fname = Replace(fname,"\logs\","\admin\logs\")
			end if
			set logFile = fs.OpenTextFile(fname, ForAppending, ForCreate, TristateTrue)
		end if
		logFile.WriteLine str
	end if
End Sub

Sub WriteLineToResponseAndLog2(str)
	Response.Write str & "<br/>"
	Response.Flush
	if ConfEnableSMSLogs = 1 then
		if not isObject(fs) then
			set fs=Server.CreateObject("Scripting.FileSystemObject")
		end if
		if isEmpty(logFile) then
			'fname = Server.MapPath("logs\" & Year(now_db) &"-"& TwoDigits(Month(now_db)) &"-"& TwoDigits(Day(now_db)) &"_"& TwoDigits(Hour(now_db)) &"-"& TwoDigits(Minute(now_db)) &"-"& TwoDigits(Second(now_db)) &".log")
			fname=date&".log"
			if (InStr(fname,"\admin\logs\")=0) then
				fname = Replace(fname,"\logs\","\admin\logs\")
			end if
			set logFile = fs.OpenTextFile(fname, ForAppending, ForCreate, TristateTrue)
		end if
		logFile.WriteLine str
	end if
End Sub

Sub WriteLineToResponseAndLog3(str)
    Response.Write str & "<br/>"
    Response.Flush
    if not isObject(fs) then
		set fs=Server.CreateObject("Scripting.FileSystemObject")
	end if
    if isEmpty(logFile2) then
			fname = Server.MapPath("logs\" & Year(now_db) &"-"& TwoDigits(Month(now_db)) &"-"& TwoDigits(Day(now_db)) &"_"& TwoDigits(Hour(now_db)) &"-"& TwoDigits(Minute(now_db)) &"-"& TwoDigits(Second(now_db)) &"usn-sync"&".log")
			if (InStr(fname,"\admin\logs\")=0) then
				fname = Replace(fname,"\logs\","\admin\logs\")
			end if
			set logFile2 = fs.OpenTextFile(fname, ForAppending, ForCreate, TristateTrue)
		end if
	logFile2.WriteLine str
End Sub

Sub WriteADSyncLog(NubmerOfString, startOperationTimer)
    if not isEnableADLogDetails then
    Exit Sub
    end if 
    Dim StartLogTime : StartLogTime = Now
    Dim StartLogTimer : StartLogTimer = Timer 'StartLogTimer = "end"OperationTimer
    Set fs = CreateObject("Scripting.FileSystemObject")
    Set a = fs.OpenTextFile(ADsyncLogFileName, 8)'for appending
    Dim currentOperationTime : currentOperationTime = StartLogTimer - startOperationTimer
    if NubmerOfString = 0  then
        a.WriteLine( CStr(TwoDigits(Year(StartLogTime))) +"-"+ CStr(TwoDigits(Month(StartLogTime))) +"-"+ CStr(TwoDigits(Day(StartLogTime))) +" "+ CStr(TwoDigits(Hour(StartLogTime))) +":"+ CStr(TwoDigits(Minute(StartLogTime))) +":"+ CStr(TwoDigits(Second(StartLogTime))) +"; "+ "WriteLoggingTime in ad_functions_inc.asp" +";"+ CStr(NubmerOfString) +"; "+ CStr(TotalLogDuration) )
    else
        a.WriteLine( CStr(TwoDigits(Year(StartLogTime))) +"-"+ CStr(TwoDigits(Month(StartLogTime))) +"-"+ CStr(TwoDigits(Day(StartLogTime))) +" "+ CStr(TwoDigits(Hour(StartLogTime))) +":"+ CStr(TwoDigits(Minute(StartLogTime))) +":"+ CStr(TwoDigits(Second(StartLogTime))) +"; "+ "ad_functions_inc.asp" +";"+ CStr(NubmerOfString) +"; "+ CStr(currentOperationTime) )
    end if
    a.Close
    Dim EndLogTimer : EndLogTimer = Timer
    TotalLogDuration = TotalLogDuration + ( EndLogTimer - StartLogTimer)
End Sub

Sub parseParams()
	strPassword=Base64Decode(strPassword64)

	If strPort <> "" Then strPort = ":"&strPort End If

    If port <> "" Then port = ":"&port End If

	if (strLogin <> "") then
		If InStr(strLogin, "\") = 0 And InStr(strLogin, "=") = 0 Then
			strLogin = Split(strDomain, ".")(0)&"\"&strLogin
		End If
	end if

	if(ad_select="selected") Then
		impOU = True
		all_ous = 2
	ElseIf(ad_select="not") Then
		impOU = False
		all_ous = 3
		selectedOUs = Null
	Else 'Request("ad_select")="all"
		impOU = True
		all_ous = 1
		selectedOUs = Null
	End If

	If VarType(groupsList) = 8204 then
		If UBound(groupsList) >= 0 Then
			all_groups = 0
		Else
			all_groups = 1
		End If
	Else
		If groupsList.Count > 0 Then
			all_groups = 0
		Else
			all_groups = 1
		End If
	End If
End Sub

Dim domainList : domainList = Array()
Dim connectionId
Dim dId

Sub syncAD(curSyncId)
    dim qEnableADLogDetails
    set qEnableADLogDetails = Conn.Execute("SELECT * FROM settings WHERE name= 'ConfEnableADSyncLogDetails' and val = '1'")
    if not qEnableADLogDetails.EOF then 
        isEnableADLogDetails = true
    End if
    if isEnableADLogDetails then
        Set fs = CreateObject("Scripting.FileSystemObject")
        ADsyncLogFileName = Server.MapPath("logs\" & Year(now) &"-"& TwoDigits(Month(now)) &"-"& TwoDigits(Day(now)) &"_"& TwoDigits(Hour(now)) &"-"& TwoDigits(Minute(now)) &"-"& TwoDigits(Second(now)) &"AdSync_Detail"&".log")
	    		if (InStr(ADsyncLogFileName,"\admin\logs\")=0) then
	    			ADsyncLogFileName = Replace(ADsyncLogFileName,"\logs\","\admin\logs\")
	    		end if
        dim ADlog_File 
        set ADlog_File = fs.OpenTextFile(ADsyncLogFileName, ForAppending, ForCreate)
        ADlog_File.close
    end if
    Timer0 = Timer
	if (curSyncId = "") then  curSyncId = sync_id end if
	Timer1 = Timer
	Conn.Execute("UPDATE recurrence SET occurences=1 WHERE id in (SELECT recur_id FROM synchronizations WHERE id = '"&curSyncId&"')")
    WriteADSyncLog 346, Timer1
	
    Timer1 = Timer
	getSynchData Conn, curSyncId
    WriteADSyncLog 350, Timer1 
    Timer1 = Timer
	parseParams()
    WriteADSyncLog 353, Timer1

    Timer1 = Timer
	Response.Write "<script type=""text/javascript"" language=""javascript"">if(window.parent && window.parent.showLoader) window.parent.showLoader();</script>"
	Response.Flush
    WriteADSyncLog 358, Timer1 
    
    if legacySync = "1" Then
	    WriteLineToResponseAndLog "Synchronization start time" &": " & now_db
	    WriteLineToResponseAndLog "Imported objects" &":"
    else 
        WriteLineToResponseAndLog3 "Synchronization start time" &": " & now_db
	    WriteLineToResponseAndLog3 "Imported objects" &":"    
    end if 

    Conn.Execute("UPDATE synchronizations SET last_time_sync_started = '" & now_db & "' WHERE id = '" & curSyncId & "'")

    dim sPort, sProtocol
    sPort = Request.ServerVariables("SERVER_PORT")
    
    if sPort = "80" then
        sPort = ""
    else
        sPort = ":" & sPort
    end if
    
    if Request.ServerVariables("HTTPS") = "off" then
        sProtocol = "http"
    else
        sProtocol = "https"
    end if

    PageUrl = sProtocol& "://" & Request.ServerVariables("SERVER_NAME") & sPort & Request.ServerVariables("URL")
    WriteLineToResponseAndLog3 "Page URL : " & Left(PageUrl, InStr(PageUrl, "/admin"))

    If legacySync = "1" Then
        Timer1 = Timer
        connectionId = LDAPQuery.connect(strLogin, strPassword, strDomain&port, secure)
        WriteADSyncLog 390, Timer1
	    parseLdapError LDAPQuery.getLastError()
    Else
        WriteLineToResponseAndLog3 "Current max uSNChanged : "&max_usn
        If ip = "0" Then
            WriteLineToResponseAndLog3 "Trying to get IP-address of domain controller"
            Dim oXMLHTTP
            Set oXMLHTTP = CreateObject("MSXML2.ServerXMLHTTP.6.0")
            oXMLHTTP.Open "GET", Left(PageUrl, InStr(PageUrl, "/admin"))&"/DomainHelper.aspx?domain="&strDomain&"&username="&strLogin&"&password="&strPassword, False
            oXMLHTTP.Send
            If oXMLHTTP.Status = 200 Then
                ip = oXMLHTTP.responseText            
            End If
        End If
        If ip = "::1" Then
            ip = "127.0.0.1"
        End If
        If ip = "0" Or ip = "" Then
            WriteLineToResponseAndLog3 "Couldn't get IP-address"
		    Response.Write "<p>"
		    Response.Write "</p><input type=""button"" onclick=""history.go(-1)"" value=""Back""/>"
		    Response.End
        Else
            WriteLineToResponseAndLog3 "IP : "&ip
            connectionId = LDAPQuery.connect(strLogin, strPassword, ip&port, secure)
            If LDAPQuery.getLastError() = 81 Then
                WriteLineToResponseAndLog3 "Cannot connect to the specified IP"
                WriteLineToResponseAndLog3 "Trying to get new IP-address of domain controller"
                Set oXMLHTTP = CreateObject("MSXML2.ServerXMLHTTP.6.0")
                oXMLHTTP.Open "GET", Left(PageUrl, InStr(PageUrl, "/admin"))&"/DomainHelper.aspx?domain=test.local"&"&username="&strLogin&"&password="&strPassword, False
                oXMLHTTP.Send
                If oXMLHTTP.Status = 200 Then
                    ip = oXMLHTTP.responseText            
                End If
                If ip = "0" or ip = "" Then
                    WriteLineToResponseAndLog3 "Couldn't get new IP-address of DC, check your settings"
		            Response.Write "<p>"
		            Response.Write "</p><input type=""button"" onclick=""history.go(-1)"" value=""Back""/>"
		            Response.End
                Else
                    max_usn = 0
                    max_usn_tmp = 0
                    WriteLineToResponseAndLog3 "New IP-address of domain controller : "&ip
                    WriteLineToResponseAndLog3 "Will perform full synchronization"
                    connectionId = LDAPQuery.connect(strLogin, strPassword, ip&port, secure)
                    parseLdapError LDAPQuery.getLastError()
                End If
            End If    	    
        End If
    End If

	'===================================================================
	'============================ Domains ==============================
	'===================================================================
	domainNamesSQL = ""
	strFilter = "(&("&strAttrClass&"="&strDomainClass&")"&strDomainFilter&")"
	strAttribs = strDomainName
    Timer1 = Timer
	DomainsQueryResult = LDAPQuery.query(connectionId, "", "subtree", strFilter, strAttribs, ADsyncLogFileName)
    WriteADSyncLog 450, Timer1

    Timer1 = Timer
	For Each domain In DomainsQueryResult
		If domainNamesSQL <> "" Then
			domainNamesSQL = domainNamesSQL & "', N'"
		End If
		domainName = domain.dn
		domainName = Mid(domainName, 4)
		domainName = Replace(domainName, ",dc=", ".", 1, -1, 1)
		domainNamesSQL = domainNamesSQL & Replace(domainName, "'", "''")
		WriteLineToResponseAndLog "Domain: " & domainName&port
	Next
    WriteADSyncLog 454, Timer1

	if domainNamesSQL <> "" then
		domainsIds = ""
        Timer1 = Timer
		set RSDomains = Conn.Execute("SELECT id FROM domains WHERE name IN (N'"& domainNamesSQL &"')")
        WriteADSyncLog 467, Timer1
        Timer1 = Timer
		while not RSDomains.EOF
			if domainsIds <> "" then
				domainsIds = domainsIds & ","
			end if
			domainsIds = domainsIds & RSDomains("id")
            Timer2 = Timer
            set usersNamesConn = Conn.Execute("SELECT name FROM users WHERE domain_id = "&RSDomains("id"))
            WriteADSyncLog 475, Timer2
            Timer2 = Timer
            while not usersNamesConn.EOF
                Timer3 = Timer
                addElemToArray usersNamesConn("name")&"/"&RSDomains("id"), usersNames
                usersNamesConn.MoveNext
                WriteADSyncLog 478, Timer3
            wend
            WriteADSyncLog 478, Timer2
            usersNamesConn.Close
            Timer2 = Timer
            set groupsNamesConn = Conn.Execute("SELECT name FROM groups WHERE domain_id = "&RSDomains("id"))
            WriteADSyncLog 485, Timer2
            Timer2 = Timer
            while not groupsNamesConn.EOF
                Timer3 = Timer
                addElemToArray groupsNamesConn("name")&"/"&RSDomains("id"), groupsNames
                groupsNamesConn.MoveNext
                WriteADSyncLog 488, Timer3
            wend
            WriteADSyncLog 488, Timer2
            groupsNamesConn.Close
            Timer2 = Timer
            set computersNamesConn = Conn.Execute("SELECT name FROM computers WHERE domain_id = "&RSDomains("id"))
            WriteADSyncLog 495, Timer2
            Timer2 = Timer
            while not computersNamesConn.EOF
                Timer3 = Timer
                addElemToArray computersNamesConn("name")&"/"&RSDomains("id"), computersNames
                computersNamesConn.MoveNext
                WriteADSyncLog 498, Timer3
            wend
            WriteADSyncLog 498, Timer2
            computersNamesConn.Close
            Timer2 = Timer
            set ouNamesConn = Conn.Execute("SELECT name FROM OU WHERE Id_domain = "&RSDomains("id"))
            WriteADSyncLog 505, Timer2
            Timer2 = Timer
            while not ouNamesConn.EOF
                Timer3 = Timer
                addElemToArray ouNamesConn("name")&"/"&RSDomains("id"), ouNames
                ouNamesConn.MoveNext
                WriteADSyncLog 498, Timer3
            wend
            WriteADSyncLog 508, Timer2
            ouNamesConn.Close
			RSDomains.MoveNext
		wend
        WriteADSyncLog 467, Timer1
		RSDomains.Close     
		setDomains domainsIds
		updateNonDomainObjects
	end if

	if domainsIds <> "" then
        Timer1 = Timer
		set RSGroups = Conn.Execute("SELECT id, dn FROM groups WHERE (dn !='' OR dn != NULL) AND domain_id NOT IN ("&domainsIds&")")
        WriteADSyncLog 525, Timer1
        Timer1 = Timer
		while not RSGroups.EOF
            Timer2 = Timer
			addElemToArray RSGroups("id") & "/" & RSGroups("dn"), arrGroups        
			RSGroups.MoveNext
            WriteADSyncLog 525, Timer2
		wend
        WriteADSyncLog 525, Timer1
	else		
        Timer1 = Timer
		set RSGroups = Conn.Execute("SELECT id, dn FROM groups WHERE dn !='' OR dn != NULL")
		while not RSGroups.EOF
            Timer2 = Timer
			addElemToArray RSGroups("id") & "/" & RSGroups("dn"), arrGroups        
			RSGroups.MoveNext
            WriteADSyncLog 533, Timer2
		wend
        WriteADSyncLog 533, Timer1
	end if

	'===================================================================
	'============================== OUs ================================
	'===================================================================
	If impOU Then
		strFilter = "(&("&strAttrClass&"="&strOuClass&")"&strOuFilter&")"
		strAttribs = strOuName&","&strAttrPath&","&strUSnChanged

        Response.Write "OU FILTER "& strFilter & "<br/>"
        Response.Write "OU ATTRS " & strAttribs & "<br/>"
        Timer1 = Timer
		OUsQueryResult = LDAPQuery.query(connectionId, "", "subtree", strFilter, strAttribs, ADsyncLogFileName)
        WriteADSyncLog 549, Timer1
		parseLdapError LDAPQuery.getLastError()
	End If

	domainList = Array()

	If all_groups = 0 Then
		Response.Write "<script type=""text/javascript"" language=""javascript"">if(window.parent && window.parent.hideLoader) window.parent.hideLoader();</script>"
		Response.Flush
        Timer1 = Timer
		For Each group_l In groupsList
			'addSubGroupsByDn group_l, Null, groupsQueryResult, usersQueryResult, compsQueryResult, True
            Timer2 = Timer
			addGroupByQuery connectionId, group_l
            WriteADSyncLog 563, Timer2
		Next
        WriteADSyncLog 563, Timer1
	Else

	'===================================================================
	'============================= Groups ==============================
	'===================================================================
	if ConfDisableGroupsSync = 0 then
		strFilter = "(&("&strAttrClass&"="&strGroupClass&")"&strGroupFilter&")"
		strAttribs = strGroupName&","&strGroupMembers&","&strUSnChanged&","&strDisplayName

        Response.Write "GROUPS FILTER "& strFilter & "<br/>"
        Response.Write "GROUPS ATTRS " & strAttribs & "<br/>"
        Timer1 = Timer
		groupsQueryResult = LDAPQuery.query(connectionId, "", "subtree", strFilter, strAttribs, ADsyncLogFileName)
        WriteADSyncLog 578, Timer1
		parseLdapError LDAPQuery.getLastError()
	end if

	'===================================================================
	'============================== Users ==============================
	'===================================================================
	strFilter = "(&("&strAttrClass&"="&strUserClass&")"&strUserFilter&")"
	strAttribs = strUserName&","&strUserDisplay&","&strUserMail&","&strUserPhone&","&strUserDisabled&","&strUSnChanged

    Response.Write "USERS FILTER "& strFilter & "<br/>"
    Response.Write "USERS ATTRS " & strAttribs & "<br/>"
       
    WriteLineToResponseAndLog strAttribs
    Timer1 = Timer
	usersQueryResult = LDAPQuery.query(connectionId, "", "subtree", strFilter, strAttribs, ADsyncLogFileName)
    WriteADSyncLog 594, Timer1
	parseLdapError LDAPQuery.getLastError()

	'===================================================================
	'============================ Computers ============================
	'===================================================================
	If impComp Then
		strFilter = "(&("&strAttrClass&"="&strCompClass&")"&strCompFilter&")"
		strAttribs = strCompName&","&strUSnChanged
        Response.Write "Computers FILTER "& strFilter & "<br/>"
        Response.Write "Computers ATTRS " & strAttribs & "<br/>"
        Timer1 = Timer
		compsQueryResult = LDAPQuery.query(connectionId, "", "subtree", strFilter, strAttribs, ADsyncLogFileName)
        WriteADSyncLog 607, Timer1
		parseLdapError LDAPQuery.getLastError()
	End If

	Response.Write "<script type=""text/javascript"" language=""javascript"">if(window.parent && window.parent.hideLoader) window.parent.hideLoader();</script>"
	Response.Flush
	Timer1 = Timer
	if ConfDisableGroupsSync = 0 then
		For Each group_l In groupsQueryResult
			If Not group_l.dasynced And isInOUs(group_l.dn) Then
                Timer2 = Timer
				addSubGroups group_l, Null, groupsQueryResult, usersQueryResult, compsQueryResult, False
                WriteADSyncLog 619, Timer2
			End IF
		Next
	end if
    WriteADSyncLog 619, Timer1
    Timer1 = Timer
	For Each user_l In usersQueryResult
		If Not user_l.dasynced And isInOUs(user_l.dn) Then
            Timer2 = Timer
			addUser user_l, Null
            WriteADSyncLog 627, Timer2
		End IF
	Next
    WriteADSyncLog 619, Timer1
    Timer1 = Timer
	If IsArray(compsQueryResult) Then
		For Each comp_l In compsQueryResult
			If Not comp_l.dasynced And isInOUs(comp_l.dn) Then
                Timer2 = Timer
				addComp comp_l, Null
                WriteADSyncLog 635, Timer2
			End IF
		Next
	End If
    WriteADSyncLog 619, Timer1
    Timer1 = Timer
	If IsArray(OUsQueryResult) Then
		For Each ou_l In OUsQueryResult
			If Not ou_l.dasynced And isInOUs(ou_l.dn) Then
                Timer2 = Timer
				addOU ou_l, getOUid(ou_l.dn)
                WriteADSyncLog 644, Timer2
			End IF
		Next
	End If
    WriteADSyncLog 619, Timer1
	End If

    Timer1 = Timer
	checkCrossDomainsGroups(connectionId)
    WriteADSyncLog 652, Timer1

	If db_remove Then
		clearDomains
	Else
		clearGroups
		clearOUs
	End IF
	'===================================================================

	'===================================================================
	now_db_new=""
	Set RS = Conn.Execute("SELECT GETDATE() as now_db")
	If Not RS.EOF Then
		now_db_new=RS("now_db")
		RS.Close
	End If
	If now_db="" Then 'something wrong with getdate
		now_db_new=Now()
	End If

	Conn.Execute("buildAlertsCache")
    If (curSyncId<>"") then
        If ip = "0" Then
            max_usn_tmp = 0
        End If
        tmp = InStr(syncDataStr, ",""ip""")
        newSyncData = Left(syncDataStr, tmp)&"""ip"":"&""""&ip&""""&",""max_usn"":"&max_usn_tmp&"}"
        Timer1 = Timer
        Conn.Execute("UPDATE synchronizations SET last_sync_time = GETDATE(), sync_data = '"&Replace(newSyncData, "'", "''")&"' where id ='"&Replace(curSyncId, "'", "''")&"'")
        WriteADSyncLog 682, Timer1
	End If

    if legacySync = "1" then
	    WriteLineToResponseAndLog "Synchronization finish time : "&now_db_new&" ("&DateDiff("s", now_db, now_db_new)&" seconds)"
    else 
        WriteLineToResponseAndLog3 "Updated Users : "&updatedUsers
        WriteLineToResponseAndLog3 "Updated Groups : "&updatedGroups
        WriteLineToResponseAndLog3 "Updated Computers : "&updatedComputers
        WriteLineToResponseAndLog3 "Updated OU : "&updatedOu
        WriteLineToResponseAndLog3 "uSNChanged after synchronization : "&max_usn_tmp
        WriteLineToResponseAndLog3 "IP after synchronization : "&ip
        WriteLineToResponseAndLog3 LNG_AD_SYNCHRONIZATION_FINISHED&": "&now_db_new&" ("&DateDiff("s", now_db, now_db_new)&" seconds)"
    end if
    Timer1 = Timer
	Conn.Execute("UPDATE recurrence SET occurences=0 WHERE id in (SELECT recur_id FROM synchronizations WHERE id = '"&curSyncId&"')")
    WriteADSyncLog 698, Timer1

	LDAPQuery.close(connectionId)
    WriteADSyncLog 0, Timer0
    if isEnableADLogDetails then
        WriteLineToResponseAndLog "Synchronization logging duration : "&TotalLogDuration&" seconds)"
    end if

    synchedGroupsGuids.RemoveAll
End Sub

'===================================================================
'=========================== Functions =============================
'===================================================================
Function checkCrossDomainsGroups(conId)
    groupChecking = True
	Dim strUsersIds : strUsersIds = ""
    Dim strGroupsIds : strGroupsIds = ""
    Dim strComputersIds : strComputersIds = ""
	Dim domains : domains = ""

	For Each domain In domainList
		If(domains <> "") Then
			domains = domains & ","
		End If
		domains = domains & domain(DOMAIN_ID_INDEX)
	Next

    Dim usersIterator : usersIterator = 0
    Dim groupsIterator : groupsIterator = 0
    Dim computersIterator : usersIterator = 0
    Const maxEntitiesPerQuery = 10000
    
    Dim TimerIncheckCrossDomainsGroups : TimerIncheckCrossDomainsGroups = Timer
	For Each strUser In arrUsers
		If (Split(strUser, "/")(2) = "user") Then
			strUsersIds = strUsersIds & Split(strUser, "/")(0) & ", "
            usersIterator = usersIterator + 1
		ElseIf (Split(strUser, "/")(2) = "group") Then
			strGroupsIds = strGroupsIds & Split(strUser, "/")(0) & ", "
            groupsIterator = groupsIterator + 1
		ElseIf (Split(strUser, "/")(2) = "computer") Then
			strComputersIds = strComputersIds & Split(strUser, "/")(0) & ", "
            computersIterator = computersIterator + 1
		End If

        Dim TimerIncheckCrossDomainsGroups1 : TimerIncheckCrossDomainsGroups1 = Timer
        If (usersIterator = maxEntitiesPerQuery) Then
            'Truncate last comma
			strUsersIds = Left(strUsersIds, Len(strUsersIds) - 2)
            Conn.Execute(" UPDATE users_groups SET was_checked=0 WHERE user_id IN (" & strUsersIds & " ) AND group_id NOT IN (SELECT id FROM groups WHERE domain_id IN (" & domains & ")) ")
            usersIterator = 0
            strUsersIds = ""
        End If
        WriteADSyncLog 768, TimerIncheckCrossDomainsGroups1

        TimerIncheckCrossDomainsGroups1 = Timer
		If (groupsIterator = maxEntitiesPerQuery) Then
			strGroupsIds = Left(strGroupsIds, Len(strGroupsIds) - 2)
            Conn.Execute(" UPDATE groups_groups SET was_checked=0 WHERE group_id IN (" & strGroupsIds & ") AND container_group_id NOT IN (SELECT id FROM groups WHERE domain_id IN (" & domains & ")) ")
            groupsIterator = 0
            strGroupsIds = ""
        End If
        WriteADSyncLog 777, TimerIncheckCrossDomainsGroups1

		TimerIncheckCrossDomainsGroups1 = Timer
        If (computersIterator = maxEntitiesPerQuery) Then
			strComputersIds = Left(strComputersIds, Len(strComputersIds) - 2)
            Conn.Execute(" UPDATE computers_groups SET was_checked=0 WHERE comp_id IN (" & strComputersIds & ") AND group_id NOT IN (SELECT id FROM groups WHERE domain_id IN (" & domains & ")) ")
            computersIterator = 0
            strComputersIds = ""
		End If
        WriteADSyncLog 787, TimerIncheckCrossDomainsGroups1
	Next
    WriteADSyncLog 755, TimerIncheckCrossDomainsGroups

    
    If (usersIterator > 0) Then
        TimerIncheckCrossDomainsGroups = Timer
		strUsersIds = Left(strUsersIds, Len(strUsersIds) - 2)
        Conn.Execute(" UPDATE users_groups SET was_checked=0 WHERE user_id IN (" & strUsersIds & " ) AND group_id NOT IN (SELECT id FROM groups WHERE domain_id IN (" & domains & ")) ")
        WriteADSyncLog 801, TimerIncheckCrossDomainsGroups
    End If
    

    
	If (groupsIterator > 0) Then
        TimerIncheckCrossDomainsGroups = Timer
		strGroupsIds = Left(strGroupsIds, Len(strGroupsIds) - 2)
        Conn.Execute(" UPDATE groups_groups SET was_checked=0 WHERE group_id IN (" & strGroupsIds & ") AND container_group_id NOT IN (SELECT id FROM groups WHERE domain_id IN (" & domains & ")) ")
        WriteADSyncLog 810, TimerIncheckCrossDomainsGroups
    End If

	If (computersIterator > 0) Then
        TimerIncheckCrossDomainsGroups = Timer
		strComputersIds = Left(strComputersIds, Len(strComputersIds) - 2)
        Conn.Execute(" UPDATE computers_groups SET was_checked=0 WHERE comp_id IN (" & strComputersIds & ") AND group_id NOT IN (SELECT id FROM groups WHERE domain_id IN (" & domains & ")) ")
        WriteADSyncLog 817, TimerIncheckCrossDomainsGroups
	End If

    TimerIncheckCrossDomainsGroups = Timer
    Dim TimerIncheckCrossDomainsGroups2 : TimerIncheckCrossDomainsGroups2 = Timer
    Dim TimerIncheckCrossDomainsGroups3 : TimerIncheckCrossDomainsGroups3 = Timer
    For Each strGroup In arrGroups
        groupId = Split(strGroup, "/")(0)
        groupDn = Split(strGroup, "/")(1)
        groupDn = Replace(groupDn, "\\", "\\5C")
        groupDn = Replace(groupDn, "*", "\\2A")
        groupDn = Replace(groupDn, "(", "\\28")
        groupDn = Replace(groupDn, ")", "\\29")
        groupDn = Replace(groupDn, "\000", "\\00")
        strFilter = "("&strMemberOf&"="&groupDn&")"
        strAttribs = strAttrClass&","&strCompName&","&strGroupName&","&strUserName&","&strObjectGuid&","&strUserDisplay&","&strUserMail&","&strUserPhone&","&strUserDisabled
        TimerIncheckCrossDomainsGroups1 = Timer1
        result = LDAPQuery.query(conId, "", "subtree", strFilter, strAttribs, ADsyncLogFileName)
        WriteLineToResponseAndLog "Current groupDn is: " & groupDn
        parseLdapError LDAPQuery.getLastError()
        WriteADSyncLog 837, TimerIncheckCrossDomainsGroups1

        TimerIncheckCrossDomainsGroups1 = Timer1
        For Each obj in result
            TimerIncheckCrossDomainsGroups2 = Timer
            For Each strUser In arrUsers
                userId = Split(strUser, "/")(0)
                userDn = Split(strUser, "/")(1)
                If (obj.dn = userDn) Then
                    TimerIncheckCrossDomainsGroups3 = Timer
                    addObjectByQuery conId, obj, groupId
                    WriteADSyncLog 848, TimerIncheckCrossDomainsGroups3
                End If
            Next          
            WriteADSyncLog 843, TimerIncheckCrossDomainsGroups2
        Next
        WriteADSyncLog 841, TimerIncheckCrossDomainsGroups1
    Next
    WriteADSyncLog 824, TimerIncheckCrossDomainsGroups
End Function

Sub parseLdapError(errorCode)
	If errorCode <> 0 Then
		Response.Write "<p>"
		Select Case errorCode
		Case 13
			WriteLineToResponseAndLog "Detected TSL connection. Go back, check ""Use secure LDAP connection"" option and try again."
			Response.AppendToLog "Sync error: Detected TSL connection, check ""Use secure LDAP connection"" option."
		Case 34
			WriteLineToResponseAndLog "The distinguished name (User DN) has an invalid syntax."
			Response.AppendToLog "Sync error: The distinguished name (User DN) has an invalid syntax."
		Case 49
			WriteLineToResponseAndLog "Invalid user or password."
			Response.AppendToLog "Sync error: Invalid user or password."
		Case 81
			WriteLineToResponseAndLog "Cannot connect to specified LDAP Server."
			Response.AppendToLog "Sync error: Cannot connect to specified LDAP Server."
		Case Else
			WriteLineToResponseAndLog "Error occured while accessing LDAP. Error number: " & errorCode
			Response.AppendToLog "Sync error: error occured while accessing LDAP, error number: " & errorCode
		End Select
		Response.Write "</p><input type=""button"" onclick=""history.go(-1)"" value=""Back""/>"
		Response.End
	End If
End Sub

Function getDomainId(dn)
	Dim domainExists : domainExists = False
	Dim dId : dId = -1
	Dim strDomain : strDomain = Right(dn, Len(dn) - InStr(1, dn, ",dc=", 1)-3)
	strDomain = Replace(strDomain, ",dc=", ".", 1, -1, 1)

    Dim TimerIngetDomainId : TimerIngetDomainId = Timer
    Dim TimerIngetDomainId1 : TimerIngetDomainId1 = Timer
	For Each domain In domainList
        TimerIngetDomainId1 = Timer
		If (domain(DOMAIN_NAME_INDEX) = strDomain) Then
			domainExists = True
			getDomainId = domain(DOMAIN_ID_INDEX)
			Exit For
		End If
        WriteADSyncLog 894, TimerIngetDomainId1
	Next
    WriteADSyncLog 892, TimerIngetDomainId
	
	If(Not domainExists) Then
        TimerIngetDomainId = Timer
		Set RSDomain = Conn.Execute("SELECT id FROM domains WHERE name='"&strDomain&"'")
        WriteADSyncLog 905, TimerIngetDomainId
		if(RSDomain.EOF) then
            TimerIngetDomainId = Timer
			Set RSDomain = Conn.Execute("SET NOCOUNT ON" & vbNewLine & "INSERT INTO domains (name, was_checked, all_groups, all_ous) VALUES ('" & Replace(strDomain, "'", "''") & "', 1, "&all_groups&","&all_ous&")" & vbNewLine & "SELECT SCOPE_IDENTITY() as id")
            WriteADSyncLog 909, TimerIngetDomainId
		end if
		getDomainId = RSDomain("id")
		RSDomain.Close
		
		Dim domainInfo(1)
		domainInfo(DOMAIN_ID_INDEX) = getDomainId
		domainInfo(DOMAIN_NAME_INDEX) = strDomain
        TimerIngetDomainId = Timer
		addElemToArray domainInfo, domainList
        WriteADSyncLog 919, TimerIngetDomainId
	End If
End Function

Function getAttr(attr)
	getAttr = ""
	If IsArray(attr) Then
		If Ubound(attr) >= 0 Then
			getAttr = attr(0)
		End If
	ElseIf VarType(attr) > 1 Then
		getAttr = attr
	End If
End Function

Function isInOUs(dn)
    Dim TimerInisInOUs : TimerInisInOUs = Timer
    Dim TimerInisInOUs1 : TimerInisInOUs1 = Timer
	If Not isNull(selectedOUs) Then
		isInOUs = False
		For Each ou In selectedOUs
            TimerInisInOUs1 = Timer
			If StrComp(dn, ou, 1) = 0 Or (Len(dn) > Len(ou) And InStrRev(dn, ou, -1, 1) = (Len(dn) - Len(ou) + 1)) Then
				isInOUs = True
				Exit For
			End If
            WriteADSyncLog 942, TimerInisInOUs1
		Next
	Else
		isInOUs = True
	End If
    WriteADSyncLog 938, TimerInisInOUs
End Function

Function addOU(ou, parent_ou_id)
    Dim TimerInaddOU : TimerInaddOU = Timer
    addElemToArray getAttr(ou(strOuName))&"/"&getDomainId(ou.dn), ouNamesFromDomain
    WriteADSyncLog 956, TimerInaddOU
	If Not VarType(parent_ou_id) > 1 Then
		parent_ou_id = "NULL"
	End If
	If db_remove Then
		was_checked = 1
	ElseIf isInOUs(ou.dn) Then
		was_checked = 1
	Else
		was_checked = 0
	End If

    needToAdd = "0"

    If legacySync = "1" Then
        needToAdd = "1"
    ElseIf max_usn < CLng(getAttr(ou(strUSnChanged))) Then
        max_usn_tmp = CLng(getAttr(ou(strUSnChanged)))
		needToAdd = "1"
    End If

    If needToAdd = "1" Then
        updatedOu = updatedOu + 1
        TimerInaddOU = Timer
	    Set RS = Conn.Execute("addOU "&_
				    "@ou_hash='"&MD5(ou.dn)&"', "&_
				    "@ou_name=N'"&Replace(getAttr(ou(strOuName)), "'", "''")&"', "&_
				    "@ou_path=N'"&Replace(getAttr(ou(strAttrPath)), "'", "''")&"', "&_
				    "@parent_ou_id="&parent_ou_id&", "&_
				    "@domain_id="&getDomainId(ou.dn)&", "&_
				    "@was_checked="&was_checked)
        WriteADSyncLog 981, TimerInaddOU
	    addOU = RS("id")
	    if RS("updated") = 0 then

		    WriteLineToResponseAndLog "Added OU: "& HTMLEncode(getAttr(ou(strOuName)))
	    else
		    WriteLineToResponseAndLog "Updated OU: "& HTMLEncode(getAttr(ou(strOuName)))
	    end if
    End If
End Function

Function getOUid(object_dn)
	If IsArray(OUsQueryResult) Then
		Dim i
		Dim found_ou : found_ou = -1
		Dim max_dn : max_dn = 0
		Dim TimerIngetOUid : TimerIngetOUid = Timer
        Dim TimerIngetOUid1 : TimerIngetOUid1 = Timer
		For i = 0 To Ubound(OUsQueryResult)
			Dim ou_dn : ou_dn = OUsQueryResult(i).dn
			Dim ou_dn_len : ou_dn_len = Len(ou_dn)
			If object_dn <> ou_dn And Len(object_dn) > ou_dn_len And InStrRev(object_dn, ou_dn, -1, 1) = (Len(object_dn) - ou_dn_len + 1) And ou_dn_len > max_dn Then
				max_dn = ou_dn_len 'for parent ou ignoring
				found_ou = i
			End If
		Next
        WriteADSyncLog 1006, TimerIngetOUid
		
		If found_ou > -1 Then
            TimerIngetOUid = Timer
			If Not OUsQueryResult(found_ou).dasynced Then
				OUsQueryResult(found_ou).daou_id = addOU(OUsQueryResult(found_ou), getOUid(OUsQueryResult(found_ou).dn))
				OUsQueryResult(found_ou).dasynced = True
			End If
			getOUid = OUsQueryResult(found_ou).daou_id
            WriteADSyncLog 1024, TimerIngetOUid
		End If
	End If
End Function

Function addGroup(group, container_group_id)
    Dim TimerInaddGroup : TimerInaddGroup = Timer
    addElemToArray getAttr(group(strGroupName))&"/"&getDomainId(group.dn), groupsNamesFromDomain
    WriteADSyncLog 1032, TimerInaddGroup
	Dim ou_id : ou_id = getOUid(group.dn)
	If Not VarType(ou_id) > 1 Then
		ou_id = "NULL"
	End If
	If Not VarType(container_group_id) > 1 Then
		container_group_id = "NULL"
	End If

    needToAdd = "0"

    If legacySync = "1" Then
        needToAdd = "1"
    ElseIf max_usn < CLng(getAttr(group(strUSnChanged))) Then
        max_usn_tmp = CLng(getAttr(group(strUSnChanged)))
		needToAdd = "1"
    End If

    If needToAdd = "1" Then
        updatedGroups = updatedGroups + 1
        TimerInaddGroup = Timer
	    Set RS = Conn.Execute("addGroup "&_
				    "@group_hash='"&MD5(group.dn)&"', "&_
				    "@group_name=N'"&Replace(getAttr(group(strGroupName)), "'", "''")&"', "&_
				    "@ou_id="&ou_id&", "&_
				    "@container_group_id="&container_group_id&", "&_
				    "@domain_id="&getDomainId(group.dn)&", "&_
                    "@group_dn='"&Replace(group.dn, "'", "''")&"', "&_
                    "@group_display_name=N'"&Replace(getAttr(group(strDisplayName)), "'", "''")&"'")
        WriteADSyncLog 1054, TimerInaddGroup
	addGroup = RS("id")	
	If Not groupChecking Then
        TimerInaddGroup = Timer
        addElemToArray RS("id") & "/" & group.dn & "/" & "group", arrUsers        
        WriteADSyncLog 1066, TimerInaddGroup
    End If
	    if RS("updated") = 0 then
		    WriteLineToResponseAndLog "Added group: "& HTMLEncode(getAttr(group(strGroupName)))
	    else
		    WriteLineToResponseAndLog "Updated group: "& HTMLEncode(getAttr(group(strGroupName)))
	    end if
    End If
End Function

Function checkDisabledUser(user)
	If impDis Then
		checkDisabledUser = True ' import all
	ElseIf VarType(user(strUserDisabled)) < 2 Then
		checkDisabledUser = True ' can't check
	ElseIf (getAttr(user(strUserDisabled)) And intUserDisBit) = intUserEnbCheck Then
		checkDisabledUser = True
	Else
		checkDisabledUser = False
	End If
End Function

Function canAddChild(dn)
	If all_child = "1" Then
		canAddChild = True
	ElseIf isInOUs(dn) Then
		canAddChild = True
	Else
		canAddChild = False
	End IF
End Function

Function addUser(user, group_id)
    Dim TimerInaddUser : TimerInaddUser = Timer
	If checkDisabledUser(user) Then
        TimerInaddUser = Timer
        addElemToArray getAttr(user(strUserName))&"/"&getDomainId(user.dn), usersNamesFromDomain
        WriteADSyncLog 1103, TimerInaddUser

		Dim ou_id : ou_id = getOUid(user.dn)	
    
		If Not VarType(ou_id) > 1 Then
			ou_id = "NULL"
		End If

		If Not VarType(group_id) > 1 Then
			group_id = "NULL"
		End If

        needToAdd = "0"

        If legacySync = "1" Then
            needToAdd = "1"
        ElseIf max_usn_tmp < CLng(getAttr(user(strUSnChanged))) Then
            max_usn_tmp = CLng(getAttr(user(strUSnChanged)))
            needToAdd = "1"
        End If

        If needToAdd = "1" Then
            updatedUsers = updatedUsers + 1
            TimerInaddUser = Timer
            Set RS = Conn.Execute("addUser "&_
					        "@user_hash='"&MD5(user.dn)&"', "&_
					        "@user_name=N'"&Replace(getAttr(user(strUserName)), "'", "''")&"', "&_
					        "@display_name=N'"&Replace(getAttr(user(strUserDisplay)), "'", "''")&"', "&_
					        "@mail=N'"&Replace(getAttr(user(strUserMail)), "'", "''")&"', "&_
					        "@phone=N'"&Replace(getAttr(user(strUserPhone)), "'", "''")&"', "&_
					        "@ou_id="&ou_id&", "&_
					        "@group_id="&group_id&", "&_
					        "@domain_id="&getDomainId(user.dn))
            WriteADSyncLog 1128, TimerInaddUser
		    addUser = RS("id")
		If Not groupChecking Then
            TimerInaddUser = Timer
            addElemToArray RS("id") & "/" & user.dn & "/" & "user", arrUsers       
            WriteADSyncLog 1141, TimerInaddUser
        End If
		    if RS("updated") = 0 then         
			    WriteLineToResponseAndLog "Added user: "& HTMLEncode(getAttr(user(strUserName)))
		    else
			    WriteLineToResponseAndLog "Updated user: "& HTMLEncode(getAttr(user(strUserName)))
		    end if
        End If
	End If
End Function

Function addComp(comp, group_id)
    Dim TimerInaddComp : TimerInaddComp = Timer
    addElemToArray getAttr(comp(strCompName))&"/"&getDomainId(comp.dn), computersNamesFromDomain
    WriteADSyncLog 1155, TimerInaddComp
	Dim ou_id : ou_id = getOUid(comp.dn)
	If Not VarType(ou_id) > 1 Then
		ou_id = "NULL"
	End If
	If Not VarType(group_id) > 1 Then
		group_id = "NULL"
	End If

    needToAdd = "0"

    If legacySync = "1" Then
        needToAdd = "1"
    ElseIf max_usn < CLng(getAttr(comp(strUSnChanged))) Then
        max_usn_tmp = CLng(getAttr(comp(strUSnChanged)))
		needToAdd = "1"
    End If

    If needToAdd = "1" Then
        updatedComputers = updatedComputers + 1
        TimerInaddComp = Timer
	    Set RS = Conn.Execute("addComp "&_
				    "@comp_hash='"&MD5(comp.dn)&"', "&_
				    "@comp_name=N'"&Replace(getAttr(comp(strCompName)), "'", "''")&"', "&_
				    "@ou_id="&ou_id&", "&_
				    "@group_id="&group_id&", "&_
				    "@domain_id="&getDomainId(comp.dn))
        WriteADSyncLog 1177, TimerInaddComp
	    addComp = RS("id")
    If Not groupChecking Then
        TimerInaddComp = Timer
        addElemToArray RS("id") & "/" & comp.dn & "/" & "computer", arrUsers        
        WriteADSyncLog 1187, TimerInaddComp
    End If
	    if RS("updated") = 0 then
		    WriteLineToResponseAndLog "DN = "&comp.dn &"Added comp: "& HTMLEncode(getAttr(comp(strCompName)))
	    else
		    WriteLineToResponseAndLog "Updated"&comp.dn&" comp: "& HTMLEncode(getAttr(comp(strCompName)))
	    end if
    End If
End Function

Sub addUserToGroup(user_id, group_id)
	If VarType(user_id) > 1 And VarType(group_id) > 1 Then
        Dim TimerInaddUserToGroup : TimerInaddUserToGroup = Timer
		Conn.Execute("addUserToGroup "&_
			"@group_id="&group_id&", "&_
			"@user_id="&user_id)
        WriteADSyncLog 1201, TimerInaddUserToGroup
	End If
End Sub

Sub addCompToGroup(comp_id, group_id)
	If VarType(comp_id) > 1 And VarType(group_id) > 1 Then
        Dim TimerInaddCompToGroup : TimerInaddCompToGroup = Timer
		Conn.Execute("addCompToGroup "&_
			"@group_id="&group_id&", "&_
			"@comp_id="&comp_id)
        WriteADSyncLog 1211, TimerInaddCompToGroup
	End If
End Sub

Sub addGroupToGroup(group_id, parent_group_id)
	If VarType(group_id) > 1 And VarType(parent_group_id) > 1 Then
        Dim TimerInaddGroupToGroup : TimerInaddGroupToGroup = Timer
		Conn.Execute("addGroupToGroup "&_
			"@container_group_id="&parent_group_id&", "&_
			"@group_id="&group_id)
        WriteADSyncLog 1221, TimerInaddGroupToGroup
	End If
End Sub

Sub addSubGroupsByDn(dn, parent_group_id, groupsResult, usersResult, compsResult, isRoot)
    Dim TimerInaddSubGroupsByDn : TimerInaddSubGroupsByDn = Timer 
    Dim TimerInaddSubGroupsByDn1 : TimerInaddSubGroupsByDn1 = Timer 
	For Each group In groupsResult
		If group.dn = dn Then
            TimerInaddSubGroupsByDn1 = Timer
			addSubGroups group, parent_group_id, groupsResult, usersResult, compsResult, isRoot
            WriteADSyncLog 1232, TimerInaddSubGroupsByDn1
			Exit For
		End If
	Next 
    WriteADSyncLog 1232, TimerInaddSubGroupsByDn
End Sub

Sub addSubGroups(group, parent_group_id, groupsResult, usersResult, compsResult, isRoot)
    Dim TimerInaddSubGroups1 : TimerInaddSubGroups1 = Timer
    Dim TimerInaddSubGroups2 : TimerInaddSubGroups2 = Timer    
    Dim TimerInaddSubGroups3 : TimerInaddSubGroups3 = Timer
    Dim TimerInaddSubGroups4 : TimerInaddSubGroups4 = Timer
    Dim TimerInaddSubGroups5 : TimerInaddSubGroups5 = Timer
    Dim TimerInaddSubGroups6 : TimerInaddSubGroups6 = Timer
    Dim TimerInaddSubGroups7 : TimerInaddSubGroups7 = Timer

	If Not group.dasynced Then
        TimerInaddSubGroups2 = Timer
		Dim group_id : group_id = addGroup(group, parent_group_id)
        WriteADSyncLog 1254, TimerInaddSubGroups2
		group.dagroup_id = group_id
		group.dasynced = True
        TimerInaddSubGroups2 = Timer
		If VarType(group_id) > 1 Then
			Dim members : members = group(strGroupMembers)
            TimerInaddSubGroups3 = Timer
			If IsArray(members) Then
				For Each member_dn In members
                    TimerInaddSubGroups4 = Timer
					Dim found : found = False
					For Each user In usersResult
						If user.dn = member_dn Then
                        TimerInaddSubGroups5 = Timer
							If canAddChild(member_dn) Then
								If Not user.dasynced Then
                                    TimerInaddSubGroups6 = Timer
									user.dauser_id = addUser(user, group_id)
									user.dasynced = True
                                    WriteADSyncLog 1273, TimerInaddSubGroups6
								Else
                                    TimerInaddSubGroups6 = Timer
									addUserToGroup user.dauser_id, group_id
                                    WriteADSyncLog 1277, TimerInaddSubGroups6
								End If
							End If
							found = True
							Exit For
                        WriteADSyncLog 1268, TimerInaddSubGroups5
						End If
					Next
                    
					If Not found And IsArray(compsQueryResult) Then
                        TimerInaddSubGroups5 = Timer
						For Each comp In compsResult
                            TimerInaddSubGroups6 = Timer
							If comp.dn = member_dn Then
								If canAddChild(member_dn) Then
									If Not comp.dasynced Then
                                        TimerInaddSubGroups7 = Timer
										comp.dacomp_id = addComp(comp, group_id)
										comp.dasynced = True
                                        WriteADSyncLog 1295, TimerInaddSubGroups7
									Else
                                        TimerInaddSubGroups7 = Timer
										addCompToGroup comp.dacomp_id, group_id
                                        WriteADSyncLog 1300, TimerInaddSubGroups7
									End If
								End If
								found = True
								Exit For
							End If
                            WriteADSyncLog 1291, TimerInaddSubGroups6
						Next
                        WriteADSyncLog 1289, TimerInaddSubGroups5
					End If
					If Not found And canAddChild(member_dn) Then
                        TimerInaddSubGroups5 = Timer
						addSubGroupsByDn member_dn, group_id, groupsResult, usersResult, compsResult, False
                        WriteADSyncLog 1313, TimerInaddSubGroups5
					End If
                    WriteADSyncLog 1264, TimerInaddSubGroups4
				Next
			Else
                addGroupByQuery connectionId, group.dn
            End If
            WriteADSyncLog 1262, TimerInaddSubGroups3
		End If
        WriteADSyncLog 1259, TimerInaddSubGroups2
	ElseIf VarType(parent_group_id) > 1 Then
        TimerInaddSubGroups1 = Timer
		addGroupToGroup group.dagroup_id, parent_group_id
        WriteADSyncLog 1324, TimerInaddSubGroups1
	End If
End Sub

Function addGroupByQuery(conId, dn)
	strFilter = "(&("&strAttrClass&"="&strGroupClass&")"&strGroupFilter&")"
	strAttribs = strGroupName&","&strUSnChanged&","&strDisplayName&","&strObjectGuid&","&strGroupType
    Dim TimerInaddGroupByQuery : TimerInaddGroupByQuery = Timer
    Dim TimerInaddGroupByQuery2 : TimerInaddGroupByQuery2 = Timer
	result = LDAPQuery.query(conId, dn, "base", strFilter, strAttribs, ADsyncLogFileName)
	parseLdapError LDAPQuery.getLastError()
    WriteADSyncLog 1335, TimerInaddGroupByQuery
    TimerInaddGroupByQuery = Timer
	For Each obj in result
        TimerInaddGroupByQuery2 = Timer
        addGroupMembersByQuery conId, obj, addGroup(obj, Null)
        WriteADSyncLog 1340, TimerInaddGroupByQuery2
	Next
    WriteADSyncLog 1338, TimerInaddGroupByQuery
End Function

Sub addGroupMembersByQuery(conId, group, group_id)
    Dim escapedGroupDn
    escapedGroupDn = group.dn

    escapedGroupDn = Replace(escapedGroupDn,"*","\2A")
    escapedGroupDn = Replace(escapedGroupDn,"(","\28")
    escapedGroupDn = Replace(escapedGroupDn,")","\29")
    escapedGroupDn = Replace(escapedGroupDn,"\","\5c")

    if ( getAttr(group(strGroupName)) = "Domain Computers" ) then
    'this is "Domain Computers" group
        strFilter = "("&strPrimaryGroupID&"="&GROUP_RID_COMPUTERS&")"
    else
    'some common group
        strFilter = "("&strMemberOf&"="&escapedGroupDn&")"
    end if
	
    if not (impComp = "1") then
        strFilter = "(&" & strFilter & "(!(objectClass=computer)))"
    end if

	strAttribs = strAttrClass&","&strCompName&","&strGroupName&","&strUserName&","&strUserDisplay&","&strObjectGuid&","&strUserMail&","&strUserPhone&","&strUserDisabled&","&strUSnChanged
    Dim TimerInaddGroupMembersByQuery : TimerInaddGroupMembersByQuery = Timer
    Dim TimerInaddGroupMembersByQuery2 : TimerInaddGroupMembersByQuery2 = Timer
	result = LDAPQuery.query(conId, "", "subtree", strFilter, strAttribs, ADsyncLogFileName)
	parseLdapError LDAPQuery.getLastError()
    WriteADSyncLog 1352, TimerInaddGroupMembersByQuery
    TimerInaddGroupMembersByQuery = Timer
	For Each obj in result
        TimerInaddGroupMembersByQuery2 = Timer
		addObjectByQuery conId, obj, group_id
        WriteADSyncLog 1357, TimerInaddGroupMembersByQuery2
	Next
    WriteADSyncLog 1355, TimerInaddGroupMembersByQuery
End Sub

Function addObjectByQuery(conId, obj, group_id)
	Dim classes : classes = obj(strAttrClass)
    Dim TimerInaddObjectByQuery : TimerInaddObjectByQuery = Timer

	If isClass(classes, strUserClass) AND NOT(isClass(classes, strCompClass)) Then
        TimerInaddObjectByQuery = Timer
		addObject = addUser(obj, group_id)
        WriteADSyncLog 1368, TimerInaddObjectByQuery
	ElseIf impComp And isClass(classes, strCompClass) Then
        TimerInaddObjectByQuery = Timer
		addObject = addComp(obj, group_id)
        WriteADSyncLog 1372, TimerInaddObjectByQuery
	ElseIf isClass(classes, strGroupClass) Then
        TimerInaddObjectByQuery = Timer
		addObject = addGroup(obj, group_id)
        WriteADSyncLog 1376, TimerInaddObjectByQuery
        
        Dim groupGuid : groupGuid = OctetToHexStr(obj(strObjectGuid)(0))

        If Not synchedGroupsGuids.Exists (groupGuid) Then 
            synchedGroupsGuids.Add groupGuid, 1

            TimerInaddObjectByQuery = Timer
		    addGroupMembersByQuery conId, obj, addObject
            WriteADSyncLog 1379, TimerInaddObjectByQuery
        End If
	End If
End Function

Function isClass(classes, clss)
	isClass = False

	If VarType(classes) > 1 Then
		For Each c in classes
			If StrComp(clss, c, 1) = 0 Then
				isClass = True
				Exit For
			End If
		Next
	End If
End Function

Sub setDomains(domains)
	if domains <> "" then
        If legacySync = "1" Then
            Dim TimerInsetDomains : TimerInsetDomains = Timer
		    ' Marks all users and groups as non-synched
            Dim objCmd
            Set objCmd = Server.CreateObject("ADODB.Command")
            
            objCmd.CommandText = _
			                    "IF OBJECT_ID('tempdb..#tempGroups') IS NOT NULL DROP TABLE #tempGroups" & vbNewLine & _
			                    "CREATE TABLE #tempGroups (id BIGINT NOT NULL)" & vbNewLine & _
			                    "INSERT INTO #tempGroups (id) (SELECT id FROM groups WHERE domain_id IN ("&domains&"))" & vbNewLine & _
			                    _
			                    "UPDATE users_groups SET was_checked=0 WHERE user_id IN (SELECT id FROM users WHERE domain_id IN ("&domains&")) OR group_id IN (SELECT id FROM #tempGroups)" & vbNewLine & _
			                    "UPDATE groups_groups SET was_checked=0 WHERE group_id IN (SELECT id FROM #tempGroups) OR container_group_id IN (SELECT id FROM #tempGroups)" & vbNewLine & _
			                    "UPDATE computers_groups SET was_checked=0 WHERE comp_id IN (SELECT id FROM computers WHERE domain_id IN ("&domains&")) OR group_id IN (SELECT id FROM #tempGroups)" & vbNewLine & _
			                    _
			                    "UPDATE users_groups SET was_checked=1 WHERE user_id IN (SELECT id FROM users WHERE domain_id NOT IN ("&domains&")) OR group_id IN (SELECT id FROM groups WHERE domain_id NOT IN ("&domains&"))" & vbNewLine & _
			                    "UPDATE computers_groups SET was_checked=1 WHERE comp_id IN (SELECT id FROM computers WHERE domain_id NOT IN ("&domains&")) OR group_id IN (SELECT id FROM groups WHERE domain_id NOT IN ("&domains&"))" & vbNewLine & _
			                    "UPDATE groups_groups SET was_checked=1 WHERE group_id IN (SELECT id FROM groups WHERE domain_id NOT IN ("&domains&")) OR container_group_id IN (SELECT id FROM groups WHERE domain_id NOT IN ("&domains&"))" & vbNewLine & _
			                    _
			                    "DROP TABLE #tempGroups" & vbNewLine
            '300 seconds = 5 minutes
            objCmd.CommandTimeout = 300
		    objCmd.ActiveConnection = Conn

            TimerInsetDomains = Timer
		    objCmd.Execute
		    WriteADSyncLog 1417, TimerInsetDomains

            TimerInsetDomains = Timer
		    Conn.Execute("UPDATE groups SET was_checked=0 WHERE domain_id IN ("&domains&")")
            WriteADSyncLog 1420, TimerInsetDomains
            TimerInsetDomains = Timer
		    Conn.Execute("UPDATE users SET was_checked=0 WHERE domain_id IN ("&domains&")")
            WriteADSyncLog 1423, TimerInsetDomains
            TimerInsetDomains = Timer
		    Conn.Execute("UPDATE computers SET was_checked=0 WHERE domain_id IN ("&domains&")")
            WriteADSyncLog 1426, TimerInsetDomains
            TimerInsetDomains = Timer
		    Conn.Execute("UPDATE OU SET was_checked=0 WHERE Id_domain IN ("&domains&")")
            WriteADSyncLog 1429, TimerInsetDomains
        End If
	end if
End Sub

Sub clearDomains
	' Deleting from DB non-synced domains and groups
	Dim domains : domains = ""
	For Each domain In domainList
		If(domains <> "") Then
			domains = domains & ","
		End If
		domains = domains & domain(DOMAIN_ID_INDEX)
	Next
	
	WriteLineToResponseAndLog "Debug Info: "& domains
	
	Conn.CommandTimeout = 300
	
	If domains <> "" Then
        If legacySync <> "1" Then
            markNotSynced
        End If
        Dim TimerInclearDomains : TimerInclearDomains = Timer
		SQL = "SET NOCOUNT ON" & vbNewLine & _
			_
			"IF OBJECT_ID('tempdb..#tempUsers') IS NOT NULL DROP TABLE #tempUsers" & vbNewLine & _
			"IF OBJECT_ID('tempdb..#tempGroups') IS NOT NULL DROP TABLE #tempGroups" & vbNewLine & _
			"IF OBJECT_ID('tempdb..#tempComps') IS NOT NULL DROP TABLE #tempComps" & vbNewLine & _
			"IF OBJECT_ID('tempdb..#tempOUs') IS NOT NULL DROP TABLE #tempOUs" & vbNewLine & _
			_
			"CREATE TABLE #tempUsers (id BIGINT NOT NULL)" & vbNewLine & _
			"CREATE TABLE #tempGroups (id BIGINT NOT NULL)" & vbNewLine & _
			"CREATE TABLE #tempComps (id BIGINT NOT NULL)" & vbNewLine & _
			"CREATE TABLE #tempOUs (id BIGINT NOT NULL)" & vbNewLine & _
			_
			"INSERT INTO #tempGroups (id) (SELECT id FROM groups WHERE domain_id IN ("&domains&"))" & vbNewLine & _
			"DELETE FROM users_groups WHERE was_checked=0 AND (user_id IN (SELECT id FROM users WHERE domain_id IN ("&domains&")) OR group_id IN (SELECT id FROM #tempGroups))" & vbNewLine & _
			"DELETE FROM groups_groups WHERE was_checked=0 AND (group_id IN (SELECT id FROM #tempGroups) OR container_group_id IN (SELECT id FROM #tempGroups))" & vbNewLine & _
			"DELETE FROM computers_groups WHERE was_checked=0 AND (comp_id IN (SELECT id FROM computers WHERE domain_id IN ("&domains&")) OR group_id IN (SELECT id FROM #tempGroups))" & vbNewLine & _
			"TRUNCATE TABLE #tempGroups" & vbNewLine & _
			_
			"INSERT INTO #tempUsers (id) (SELECT id FROM users WHERE was_checked=0 AND role = 'U' AND domain_id IN ("&domains&"))" & vbNewLine & _
			"INSERT INTO #tempGroups (id) (SELECT id FROM groups WHERE was_checked=0 AND domain_id IN ("&domains&"))" & vbNewLine & _
			"INSERT INTO #tempComps (id) (SELECT id FROM computers WHERE was_checked=0 AND domain_id IN ("&domains&"))" & vbNewLine & _
			"INSERT INTO #tempOUs (id) (SELECT Id FROM OU WHERE was_checked=0 AND Id_domain IN ("&domains&"))" & vbNewLine & _
			_
			"DELETE FROM User_synch WHERE Id_user IN (SELECT id FROM #tempUsers)" & vbNewLine & _
			"DELETE FROM Comp_synch WHERE Id_comp IN (SELECT id FROM #tempComps)" & vbNewLine & _
			"DELETE FROM Group_synch WHERE Id_group IN (SELECT id FROM #tempGroups)" & vbNewLine & _
			"DELETE FROM OU_synch WHERE Id_OU IN (SELECT id FROM #tempOUs)" & vbNewLine & _
			_
			"DELETE FROM OU_User WHERE Id_OU IN (SELECT id FROM #tempOUs) OR Id_user IN (SELECT id FROM #tempUsers)" & vbNewLine & _
			"DELETE FROM OU_comp WHERE Id_OU IN (SELECT id FROM #tempOUs) OR Id_comp IN (SELECT id FROM #tempComps)" & vbNewLine & _
			"DELETE FROM OU_group WHERE Id_OU IN (SELECT id FROM #tempOUs) OR Id_group IN (SELECT id FROM #tempGroups)" & vbNewLine & _
			"DELETE FROM OU_hierarchy WHERE Id_parent IN (SELECT id FROM #tempOUs) OR Id_child IN (SELECT id FROM #tempOUs)" & vbNewLine & _
			"DELETE FROM OU_DOMAIN WHERE Id_OU IN (SELECT id FROM #tempOUs)" & vbNewLine & _
			_
			"SELECT name FROM users WHERE id IN (SELECT id FROM #tempUsers)" & vbNewLine & _
			"SELECT name FROM groups WHERE id IN (SELECT id FROM #tempGroups)" & vbNewLine & _
			"SELECT name FROM computers WHERE id IN (SELECT id FROM #tempComps)" & vbNewLine & _
			"SELECT name FROM OU WHERE Id IN (SELECT id FROM #tempOUs)" & vbNewLine & _
			_
			"DELETE FROM users WHERE id IN (SELECT id FROM #tempUsers)" & vbNewLine & _
			"DELETE FROM groups WHERE id IN (SELECT id FROM #tempGroups)" & vbNewLine & _
			"DELETE FROM computers WHERE id IN (SELECT id FROM #tempComps)" & vbNewLine & _
			"DELETE FROM OU WHERE Id IN (SELECT id FROM #tempOUs)" & vbNewLine & _
			_
			"DROP TABLE #tempUsers" & vbNewLine & _
			"DROP TABLE #tempGroups" & vbNewLine & _
			"DROP TABLE #tempComps" & vbNewLine & _
			"DROP TABLE #tempOUs" & vbNewLine
		set RS = Conn.Execute(SQL)
		while not RS.EOF
			WriteLineToResponseAndLog "Deleted user: "& HTMLEncode(RS("name"))
			RS.MoveNext
		wend
		set RS = RS.NextRecordset
		while not RS.EOF
			WriteLineToResponseAndLog "Deleted group: "& HTMLEncode(RS("name"))
			RS.MoveNext
		wend
		set RS = RS.NextRecordset
		while not RS.EOF
			WriteLineToResponseAndLog "Deleted computer: "& HTMLEncode(RS("name"))
			RS.MoveNext
		wend
		set RS = RS.NextRecordset
		while not RS.EOF
			WriteLineToResponseAndLog "Deleted OU: "& HTMLEncode(RS("name"))
			RS.MoveNext
		wend  
        WriteADSyncLog 1502, TimerInclearDomains
	End If
	
	Conn.CommandTimeout = 30
End Sub

Sub updateNonDomainObjects
    Dim TimerInupdateNonDomainObjects : TimerInupdateNonDomainObjects = Timer
	regDomainWhere = "domain_id IS NULL"
	Set RSRegDomain = Conn.Execute("SELECT id FROM domains WHERE name = ''")
	if not RSRegDomain.EOF then
		regDomainWhere = "("& regDomainWhere &" OR domain_id = "& RSRegDomain("id") &")"
	end if
	SQL = _
		"IF OBJECT_ID('tempdb..#tempGroups') IS NOT NULL DROP TABLE #tempGroups" & vbNewLine & _
		"CREATE TABLE #tempGroups (id BIGINT NOT NULL)" & vbNewLine & _
		"INSERT INTO #tempGroups (id) (SELECT id FROM groups WHERE "& regDomainWhere &")" & vbNewLine & _
		_
		"UPDATE users_groups SET was_checked=1 WHERE user_id IN (SELECT id FROM users WHERE "& regDomainWhere &") OR group_id IN (SELECT id FROM #tempGroups)" & vbNewLine & _
		"UPDATE groups_groups SET was_checked=1 WHERE group_id IN (SELECT id FROM #tempGroups) OR container_group_id IN (SELECT id FROM #tempGroups)" & vbNewLine & _
		"UPDATE computers_groups SET was_checked=1 WHERE comp_id IN (SELECT id FROM computers WHERE "& regDomainWhere &") OR group_id IN (SELECT id FROM #tempGroups)" & vbNewLine & _
		_
		"DROP TABLE #tempGroups" & vbNewLine
    TimerInupdateNonDomainObjects = Timer
	Conn.Execute(SQL)
    WriteADSyncLog 1546, TimerInupdateNonDomainObjects
	TimerInupdateNonDomainObjects = Timer
	Conn.Execute("UPDATE groups SET was_checked=1 WHERE "& regDomainWhere)
    WriteADSyncLog 1549, TimerInupdateNonDomainObjects
    TimerInupdateNonDomainObjects = Timer
	Conn.Execute("UPDATE users SET was_checked=1 WHERE "& regDomainWhere)
    WriteADSyncLog 1552, TimerInupdateNonDomainObjects
    TimerInupdateNonDomainObjects = Timer
	Conn.Execute("UPDATE computers SET was_checked=1 WHERE "& regDomainWhere)
    WriteADSyncLog 1555, TimerInupdateNonDomainObjects
    TimerInupdateNonDomainObjects = Timer
	Conn.Execute("UPDATE OU SET was_checked=1 WHERE "& Replace(regDomainWhere, "domain_id", "Id_domain"))
    WriteADSyncLog 1558, TimerInupdateNonDomainObjects
End Sub

Sub clearGroups
	' Deleting from DB non-synced domains and groups
	Dim domains : domains = ""
    Dim TimerInclearGroups : TimerInclearGroups = Timer
	For Each domain In domainList
		If(domains <> "") Then
			domains = domains & ","
		End If
		domains = domains & domain(DOMAIN_ID_INDEX)
	Next
    WriteADSyncLog 1566, TimerInclearGroups
	
	If domains <> "" Then
        If legacySync <> "1" Then
            markNotSynced
        End If
		SQL = _
			";WITH g AS (SELECT id FROM groups WHERE was_checked=1 AND domain_id IN ("&domains&"))" & vbNewLine & _
			"	DELETE FROM groups_groups WHERE was_checked=0 AND (group_id IN (SELECT id FROM g) OR container_group_id IN (SELECT id FROM g))"
        TimerInclearGroups = Timer
		Conn.Execute(SQL)
        WriteADSyncLog 1582, TimerInclearGroups
	End If
End Sub

Sub clearOUs
	' Deleting from DB non-synced users, computers and groups form selected OUs and them sub-OUs
	Dim domains : domains = ""
    Dim TimerInclearOUs : TimerInclearOUs = Timer
    Dim TimerInclearOUs2 : TimerInclearOUs2 = Timer
	For Each domain In domainList
		If(domains <> "") Then
			domains = domains & ","
		End If
		domains = domains & domain(DOMAIN_ID_INDEX)
	Next
    WriteADSyncLog 1592, TimerInclearOUs
	
	If domains <> "" Then
        If legacySync <> "1" Then
            markNotSynced
        End If
		SQL = "SET NOCOUNT ON" & vbNewLine & _
			_
			"IF OBJECT_ID('tempdb..#tempUsers') IS NOT NULL DROP TABLE #tempUsers" & vbNewLine & _
			"IF OBJECT_ID('tempdb..#tempGroups') IS NOT NULL DROP TABLE #tempGroups" & vbNewLine & _
			"IF OBJECT_ID('tempdb..#tempComps') IS NOT NULL DROP TABLE #tempComps" & vbNewLine & _
			"IF OBJECT_ID('tempdb..#tempOUs') IS NOT NULL DROP TABLE #tempOUs" & vbNewLine & _
			_
			"CREATE TABLE #tempUsers (id BIGINT NOT NULL)" & vbNewLine & _
			"CREATE TABLE #tempGroups (id BIGINT NOT NULL)" & vbNewLine & _
			"CREATE TABLE #tempComps (id BIGINT NOT NULL)" & vbNewLine & _
			"CREATE TABLE #tempOUs (id BIGINT NOT NULL)" & vbNewLine & _
			_
			"INSERT INTO #tempOUs (id) (SELECT Id FROM OU WHERE was_checked=1 AND Id_domain IN ("&domains&"))" & vbNewLine & _
			_
			"INSERT INTO #tempUsers (id) (SELECT id FROM users u INNER JOIN OU_User o ON u.id=o.Id_user WHERE u.was_checked=0 AND o.Id_OU IN (SELECT id FROM #tempOUs))" & vbNewLine & _
			"INSERT INTO #tempGroups (id) (SELECT id FROM groups g INNER JOIN OU_group o ON g.id=o.Id_group WHERE g.was_checked=0 AND o.Id_OU IN (SELECT id FROM #tempOUs))" & vbNewLine & _
			"INSERT INTO #tempComps (id) (SELECT id FROM computers c INNER JOIN OU_comp o ON c.id=o.Id_comp WHERE c.was_checked=0 AND o.Id_OU IN (SELECT id FROM #tempOUs))" & vbNewLine & _
			_
			"DELETE FROM users_groups WHERE user_id IN (SELECT id FROM #tempUsers) OR group_id IN (SELECT id FROM #tempGroups)" & vbNewLine & _
			"DELETE FROM groups_groups WHERE group_id IN (SELECT id FROM #tempGroups) OR container_group_id IN (SELECT id FROM #tempGroups)" & vbNewLine & _
			"DELETE FROM computers_groups WHERE comp_id IN (SELECT id FROM #tempComps) OR group_id IN (SELECT id FROM #tempGroups)" & vbNewLine & _
			_
			"DELETE FROM User_synch WHERE Id_user IN (SELECT id FROM #tempUsers)" & vbNewLine & _
			"DELETE FROM Comp_synch WHERE Id_comp IN (SELECT id FROM #tempComps)" & vbNewLine & _
			"DELETE FROM Group_synch WHERE Id_group IN (SELECT id FROM #tempGroups)" & vbNewLine & _
			_
			"DELETE FROM OU_User WHERE Id_user IN (SELECT id FROM #tempUsers)" & vbNewLine & _
			"DELETE FROM OU_comp WHERE Id_comp IN (SELECT id FROM #tempComps)" & vbNewLine & _
			"DELETE FROM OU_group WHERE Id_group IN (SELECT id FROM #tempGroups)" & vbNewLine & _
			_
			"SELECT name FROM users WHERE id IN (SELECT id FROM #tempUsers)" & vbNewLine & _
			"SELECT name FROM groups WHERE id IN (SELECT id FROM #tempGroups)" & vbNewLine & _
			"SELECT name FROM computers WHERE id IN (SELECT id FROM #tempComps)" & vbNewLine & _
			_
			"DELETE FROM users WHERE id IN (SELECT id FROM #tempUsers)" & vbNewLine & _
			"DELETE FROM groups WHERE id IN (SELECT id FROM #tempGroups)" & vbNewLine & _
			"DELETE FROM computers WHERE id IN (SELECT id FROM #tempComps)" & vbNewLine & _
			_
			"DROP TABLE #tempUsers" & vbNewLine & _
			"DROP TABLE #tempGroups" & vbNewLine & _
			"DROP TABLE #tempComps" & vbNewLine & _
			"DROP TABLE #tempOUs" & vbNewLine
        TimerInclearOUs = Timer
		set RS = Conn.Execute(SQL)
        WriteADSyncLog 1647, TimerInclearOUs
        TimerInclearOUs1 = Timer
		while not RS.EOF
            TimerInclearOUs2 = Timer
			WriteLineToResponseAndLog "Deleted user: "& HTMLEncode(RS("name"))
			RS.MoveNext
            WriteADSyncLog 1653, TimerInclearOUs2
		wend
        WriteADSyncLog 1650, TimerInclearOUs1
        TimerInclearOUs1 = Timer
		set RS = RS.NextRecordset
		while not RS.EOF
            TimerInclearOUs2 = Timer
			WriteLineToResponseAndLog "Deleted group: "& HTMLEncode(RS("name"))
			RS.MoveNext
            WriteADSyncLog 1662, TimerInclearOUs2
		wend
        WriteADSyncLog 1659, TimerInclearOUs1
        TimerInclearOUs1 = Timer
		set RS = RS.NextRecordset
		while not RS.EOF
            TimerInclearOUs2 = Timer
			WriteLineToResponseAndLog "Deleted computer: "& HTMLEncode(RS("name"))
			RS.MoveNext
            WriteADSyncLog 1671, TimerInclearOUs2
		wend
        WriteADSyncLog 1668, TimerInclearOUs1
	End If
End Sub

' elem - element to add
' arr - array where to add elem
Sub addElemToArray(elem, arr)
	Dim length : length = Ubound(arr) + 1
	
	If length = 0 Then
		Redim arr(0)
	Else
		Redim Preserve arr(length)
	End If
	
	arr(length) = elem
End Sub

Sub markNotSynced
    Dim TimerInmarkNotSynced : TimerInmarkNotSynced = Timer
    Dim TimerInmarkNotSynced1 : TimerInmarkNotSynced1 = Timer
    Dim TimerInmarkNotSynced2 : TimerInmarkNotSynced2 = Timer
    Dim TimerInmarkNotSynced3 : TimerInmarkNotSynced3 = Timer
    For Each user In usersNames
        TimerInmarkNotSynced1 = Timer
        needToDelete = "1"
        For Each userInDomain In usersNamesFromDomain
            If user = userInDomain Then
                needToDelete = "0"
            End If
        Next
        If needToDelete = "1" Then
            TimerInmarkNotSynced2 = Timer
            Conn.Execute("UPDATE users SET was_checked=0 WHERE name = '"&Split(user, "/")(0)&"' AND domain_id = "&Split(user, "/")(1))
            WriteADSyncLog 1707, TimerInmarkNotSynced2
            TimerInmarkNotSynced2 = Timer
            Conn.Execute("UPDATE users_groups SET was_checked=0 WHERE user_id = (SELECT id FROM users WHERE name = '"&Split(user, "/")(0)&"' AND domain_id = "&Split(user, "/")(1)&")")  
            updatedUsers = updatedUsers + 1        
            WriteADSyncLog 1710, TimerInmarkNotSynced2
        End If
        WriteADSyncLog 1705, TimerInmarkNotSynced1
    Next
    WriteADSyncLog 1697, TimerInmarkNotSynced

    TimerInmarkNotSynced = Timer
    For Each group In groupsNames
        needToDelete = "1"
        For Each groupInDomain In groupsNamesFromDomain
            If group = groupInDomain Then
                needToDelete = "0"
            End If
        Next
        If needToDelete = "1" Then
            TimerInmarkNotSynced2 = Timer
            Conn.Execute("UPDATE groups SET was_checked=0 WHERE name = '"&Split(group, "/")(0)&"' AND domain_id = "&Split(group, "/")(1))
            WriteADSyncLog 1728, TimerInmarkNotSynced2
            TimerInmarkNotSynced2 = Timer
            Conn.Execute("UPDATE groups_groups SET was_checked=0 WHERE group_id = (SELECT id FROM groups WHERE name = '"&Split(group, "/")(0)&"' AND domain_id = "&Split(group, "/")(1)&")")
            updatedGroups = updatedGroups + 1    
            WriteADSyncLog 1732, TimerInmarkNotSynced2
        End If
    Next
    WriteADSyncLog 1719, TimerInmarkNotSynced

    TimerInmarkNotSynced = Timer
    For Each computer In computersNames
        needToDelete = "1"
        For Each computerInDomain In computersNamesFromDomain
            If computer = computerInDomain Then
                needToDelete = "0"
            End If
        Next
        If needToDelete = "1" Then
            TimerInmarkNotSynced2 = Timer
            Conn.Execute("UPDATE computers SET was_checked=0 WHERE name = '"&Split(computer, "/")(0)&"' AND domain_id = "&Split(computer, "/")(1))
            WriteADSyncLog 1748, TimerInmarkNotSynced2
            TimerInmarkNotSynced2 = Timer
            Conn.Execute("UPDATE computers_groups SET was_checked=0 WHERE group_id = (SELECT id FROM computers WHERE name = '"&Split(computer, "/")(0)&"' AND domain_id = "&Split(computer, "/")(1)&")") 
            updatedComputers = updatedComputers + 1               
            WriteADSyncLog 1751, TimerInmarkNotSynced2
        End If
    Next
    WriteADSyncLog 1739, TimerInmarkNotSynced

    TimerInmarkNotSynced = Timer
    For Each ou In ouNames
        needToDelete = "1"
        For Each ouInDomain In ouNamesFromDomain
            If ou = ouInDomain Then
                needToDelete = "0"
            End If
        Next
        If needToDelete = "1" Then
            TimerInmarkNotSynced2 = Timer
            Conn.Execute("UPDATE OU SET was_checked=0 WHERE name = '"&Split(ou, "/")(0)&"' AND Id_domain = "&Split(ou, "/")(1))    
            updatedOu = updatedOu + 1        
            WriteADSyncLog 1768, TimerInmarkNotSynced2
        End If
    Next
    WriteADSyncLog 1759, TimerInmarkNotSynced
End Sub

%>