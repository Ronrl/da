using DeskAlertsDotNet.Utils.Factories;
using System.Data;

namespace DeskAlertsDotNet.Pages
{
    using DataBase;
    using Models.Permission;
    using Parameters;
    using Resources;
    using System;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;

    public partial class OrgGroups : DeskAlertsBaseListPage
    {
        protected override string[] Collumns => new string[] { };

        protected override HtmlGenericControl NoRowsDiv => NoRows;

        protected override HtmlGenericControl TableDiv => mainTableDiv;

        protected override IButtonControl[] DeleteButtons => new IButtonControl[] { upDelete, bottomDelete };

        protected override string DefaultOrderCollumn => "name";

        protected override string ContentName => resources.LNG_GROUPS;

        protected override string DataTable => "groups";

        protected override string CustomQuery
        {
            get
            {
                const string countUsersQuery = @"SELECT COUNT(1)
                                FROM users
                                INNER JOIN users_groups ON users.id = users_groups.user_id
                                WHERE users_groups.group_id = t.group_id";

                const string countComputersQuery = @"SELECT COUNT(1)
								FROM computers
									 INNER JOIN computers_groups ON computers.id = computers_groups.comp_id
								WHERE computers_groups.group_id = t.group_id";

                const string countGroupsQuery = @"SELECT COUNT(1)
								FROM groups
									 INNER JOIN groups_groups ON groups.id = groups_groups.container_group_id
								WHERE groups_groups.container_group_id = t.group_id";

                const string countOuUsersQuery = @"SELECT COUNT(1)
                               FROM ou_groups og
                               INNER JOIN OU_User ou ON og.ou_id = ou.Id_OU
                               WHERE og.group_id = t.group_id";

                const string countOuComputersQuery = @"SELECT COUNT(1)
                               FROM ou_groups og
                               INNER JOIN OU_comp oc ON og.ou_id = oc.Id_OU
                               WHERE og.group_id = t.group_id";

                const string countOuGroupsQuery = @"SELECT COUNT(1)
                               FROM ou_groups og
                               INNER JOIN OU_Group o ON og.ou_id = o.Id_OU
                               WHERE og.group_id = t.group_id";

                var query = GetFilterForQuery();

                query += $@"SELECT t.group_id AS id,
                                groups.context_id,
                                groups.name AS name,
                                groups.display_name AS display_name,
                                domains.name AS domain_name,
                                groups.custom_group,

                                (SELECT ({countUsersQuery}) + ({countOuUsersQuery})) AS users_cnt,
                                (SELECT ({countComputersQuery}) + ({countOuComputersQuery})) AS computers_cnt,
                                (SELECT ({countGroupsQuery}) + ({countOuGroupsQuery})) AS groups_cnt,

                                (SELECT COUNT(1)
                                    FROM groups_groups gg
                                    WHERE gg.group_id=t.group_id) AS member_of_cnt,
                                edir_context.name AS context_name

                            FROM #tempGroups t
                            LEFT JOIN groups ON t.group_id = groups.id
                            LEFT JOIN domains ON domains.id = groups.domain_id
                            LEFT JOIN edir_context ON edir_context.id = groups.context_id";

                string sortBy = Request["sortBy"];
                query += " ORDER BY " + (string.IsNullOrEmpty(sortBy) ? "name" : sortBy);
                query += @" DROP TABLE #tempOUs
                            DROP TABLE #tempGroups";
                return query;
            }
        }

        protected override string CustomCountQuery
        {
            get
            {
                string query = GetFilterForQuery();
                query += @" SELECT COUNT(1) AS cnt
                            FROM #tempGroups";
                return query;
            }
        }

        protected override string PageParams
        {
            get
            {
                var uri = string.Empty;
                uri += AddParameter(uri, "type");
                uri += AddParameter(uri, "id");
                return uri;
            }
        }

        protected override HtmlGenericControl TopPaging => topPaging;

        protected override HtmlGenericControl BottomPaging => bottomPaging;

        protected override string DefaultSortingOrder => "ASC";

        protected override HtmlInputText SearchControl => searchTermBox;

        protected override bool IsUsedDefaultTableSettings => false;

        protected override void OnLoad(EventArgs e)
        {
            UsersAndGroups.Visible = !Config.Data.HasModule(Modules.AD);
            base.OnLoad(e);
            searchButton.Text = searchButton.Text.Replace("{Search}", resources.LNG_SEARCH);

            groupName.Text = GetSortLink(resources.LNG_GROUP_NAME, "name", Request["sortBy"]);
            groupDisplayName.Text = GetSortLink(resources.DISPLAY_NAME, "display_name", Request["sortBy"]);
            domain.Text = GetSortLink(resources.LNG_DOMAIN, "name", Request["sortBy"]);
            users.Text = GetSortLink(resources.LNG_USERS, "users_cnt", Request["sortBy"]);
            computers.Text = GetSortLink(resources.LNG_COMPUTERS, "computers_cnt", Request["sortBy"]);
            groups.Text = GetSortLink(resources.LNG_GROUPS, "groups_cnt", Request["sortBy"]);
            parents.Text = GetSortLink(resources.LNG_PARENT_GROUPS, "member_of_cnt", Request["sortBy"]);
            actionsCollumn.Text = resources.LNG_ACTION;
            Table = contentTable;
            upDelete.Visible = curUser.HasPrivilege || curUser.Policy[Policies.GROUPS].CanDelete;
            bottomDelete.Visible = curUser.HasPrivilege || curUser.Policy[Policies.GROUPS].CanDelete;

            var cnt = Content.Count < (Offset + Limit) ? Content.Count : Offset + Limit;

            for (var i = Offset; i < cnt; i++)
            {
                var row = Content.GetRow(i);

                var id = row.GetString("id");
                var tableRow = new TableRow();
                var chb = GetCheckBoxForDelete(row);
                var checkBoxCell = new TableCell { HorizontalAlign = HorizontalAlign.Center };
                checkBoxCell.Controls.Add(chb);
                tableRow.Cells.Add(checkBoxCell);

                var groupCell = new TableCell { Text = row.GetString("name") };
                tableRow.Cells.Add(groupCell);

                var groupDisplayNameCell = new TableCell { Text = row.GetString("display_name") };
                tableRow.Cells.Add(groupDisplayNameCell);

                var domainCell = new TableCell
                {
                    Text = row.GetString("domain_name"),
                    HorizontalAlign = HorizontalAlign.Center
                };
                tableRow.Cells.Add(domainCell);

                var usersCntCell = new TableCell
                {
                    Text = $@"<a href='../UsersInGroup.aspx?id={id}'>{row.GetString("users_cnt")}</a>",
                    HorizontalAlign = HorizontalAlign.Center
                };
                tableRow.Cells.Add(usersCntCell);

                var computersCntCell = new TableCell
                {
                    Text = $@"<a href='../CompsInGroup.aspx?id={id}'>{row.GetString("computers_cnt")}</a>",
                    HorizontalAlign = HorizontalAlign.Center
                };
                tableRow.Cells.Add(computersCntCell);

                var groupsCntCell = new TableCell
                {
                    Text = $@"<a href='../GroupsInGroup.aspx?id={id}'>{row.GetString("groups_cnt")}</a>",
                    HorizontalAlign = HorizontalAlign.Center
                };
                tableRow.Cells.Add(groupsCntCell);

                var parentCntCell = new TableCell
                {
                    Text = $@"<a href='../GroupsInGroup.aspx?id={id}&member_of=1'>{row.GetString("member_of_cnt")}</a>",
                    HorizontalAlign = HorizontalAlign.Center
                };
                tableRow.Cells.Add(parentCntCell);

                var editButton = GetActionButton(
                    row.GetString("id"),
                    "../images/action_icons/edit.png",
                    "LNG_EDIT_GROUP",
                    delegate { OnActionButtonClick(row); });

                var actionsCell = new TableCell { HorizontalAlign = HorizontalAlign.Center };
                if (curUser.HasPrivilege || curUser.Policy[Policies.GROUPS].CanEdit)
                {
                    actionsCell.Controls.Add(editButton);
                }

                tableRow.Cells.Add(actionsCell);
                contentTable.Rows.Add(tableRow);
            }
        }

        private void OnActionButtonClick(DataRow row)
        {
            ScriptManager.RegisterClientScriptBlock(
                this,
                GetType(),
                "ORG_USERS_EDIT_" + row.GetString("id"),
                "editUser(" + row.GetString("id") + string.Empty
                + (Request.Url.Query.Length > 0 ? (",'" + Request.Url.Query) + "'" : ",''") + ")",
                true);
        }

        private string GetFilterForQuery()
        {
            string searchType = Request["type"];

            string searchId = Request["id"];

            string query = @"SET NOCOUNT ON IF OBJECT_ID('tempdb..#tempOUs') IS NOT NULL
                                    DROP TABLE #tempOUs
                                    CREATE TABLE #tempOUs (Id_child BIGINT NOT NULL, Rights BIT NOT NULL);
                                    IF OBJECT_ID('tempdb..#tempGroups') IS NOT NULL
                                    DROP TABLE #tempGroups
                                    CREATE TABLE #tempGroups (group_id BIGINT NOT NULL);
                                    DECLARE @now DATETIME;
                                    SET @now = GETDATE();";

            if (searchType == "ou")
            {
                if (Request["search"] == "1")
                {
                    query += $@"WITH OUs (id) AS
                                      (SELECT CAST({searchId} AS BIGINT)
                                       UNION ALL SELECT Id_child
                                       FROM OU_hierarchy h
                                       INNER JOIN OUs ON OUs.id = h.Id_parent)
                                    INSERT INTO #tempOUs (Id_child, Rights)
                                      (SELECT DISTINCT id,
                                                       {searchId}
                                       FROM OUs) OPTION (MaxRecursion 10000);";
                }
                else
                {
                    query += $@" INSERT INTO #tempOUs (Id_child, Rights) VALUES ({searchId}, 0)";
                }
            }
            else if (searchType == "DOMAIN")
            {
                query +=
                    $@" INSERT INTO #tempOUs (Id_child, Rights) (SELECT id, 0 FROM OU WHERE Id_domain = {searchId})";
            }
            else
            {
                query += " INSERT INTO #tempOUs (Id_child, Rights) (SELECT id, 0 FROM OU) ";
            }

            query += @"INSERT INTO #tempGroups (group_id)
                      (SELECT DISTINCT groups.id
                       FROM groups
                       INNER JOIN OU_Group og ON groups.id=og.Id_group
                       INNER JOIN #tempOUs t ON og.Id_OU=t.Id_child";

            var searchString = Request["searchTermBox"];

            if (!string.IsNullOrEmpty(searchString))
            {
                query += $" WHERE (groups.name LIKE N'%{searchString}%' OR groups.display_name LIKE N'%{searchString}%')";
            }

            query += ")";

            if (searchType != "ou")
            {
                query += @" INSERT INTO #tempGroups (group_id)
                          (SELECT DISTINCT groups.id
                           FROM groups
                           LEFT JOIN #tempGroups t ON t.group_id=groups.id
                           WHERE t.group_id IS NULL";

                if (searchType == "DOMAIN")
                {
                    query += $" AND groups.domain_id = {searchId}";
                }

                if (!string.IsNullOrEmpty(searchString))
                {
                    query += $" AND (groups.name LIKE N'%{searchString}%')";
                }

                query += ")";
            }

            return query;
        }

        protected override void OnDeleteRows(string[] ids)
        {
            foreach (var id in ids)
            {
                var query = @"
                DELETE FROM [dbo].[groups] WHERE [id] = @id1
                DELETE FROM [dbo].[groups_groups] WHERE [group_id] = @id2 OR [container_group_id] = @id3
                DELETE FROM [dbo].[users_groups] WHERE [group_id] = @id4
                DELETE FROM [dbo].[ou_groups] WHERE [group_id] = @id5
                DELETE FROM [dbo].[computers_groups] WHERE [group_id] = @id6";
                dbMgr.ExecuteQuery(query,
                    SqlParameterFactory.Create(DbType.Int32, id, "id1"),
                    SqlParameterFactory.Create(DbType.Int32, id, "id2"),
                    SqlParameterFactory.Create(DbType.Int32, id, "id3"),
                    SqlParameterFactory.Create(DbType.Int32, id, "id4"),
                    SqlParameterFactory.Create(DbType.Int32, id, "id5"),
                    SqlParameterFactory.Create(DbType.Int32, id, "id6"));
            }
        }
    }
}