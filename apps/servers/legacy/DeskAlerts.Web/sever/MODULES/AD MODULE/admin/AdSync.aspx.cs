﻿using System;
using System.Collections;
using System.Linq;
using System.Web.Script.Serialization;
using DeskAlertsDotNet.ActiveDirectory;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Parameters;
using DeskAlertsDotNet.Models;
using System.DirectoryServices;
using System.Text.RegularExpressions;
using DeskAlerts.ApplicationCore.Utilities;
using DeskAlerts.Server.Utils;
using DeskAlerts.Server.Utils.Helpers;
using DeskAlertsDotNet.Utils.Consts;
using Resources;

namespace DeskAlertsDotNet.Pages
{
    public partial class AdSync : System.Web.UI.Page
    {
        
        public DBManager dbMgr = new DBManager();
        public Synchronization curSync = new Synchronization();

        private  const string AD_FILTER_GROUPS = "(&(objectClass=group)(objectCategory=Group))";

        private  const string AD_FILTER_OUS = "(&(objectClass=organizationalUnit))";

        private  const string AD_FILTER_USERS = "(&(objectClass=user)(&(objectCategory=Person)(sAMAccountName=*)))";

        private  const string AD_FILTER_COMPUTERS = "(&(objectClass=computer)(objectCategory=Computer))";

        string DisplayableNameForEntry(DirectoryEntry entry)
        {
            return entry.Name.Substring(entry.Name.IndexOf("=") + 1);
        }

        string DisplayableNameForEntry(string entryName)
        {
            return entryName.Substring(entryName.IndexOf("=") + 1);
        }

        bool CanAddGroup(DirectoryEntry groupEntry)
        {
            if(curSync.added_group != null && curSync.added_group.Length > 0)
            {
                return curSync.added_group.Where(gr => gr.StartsWith(groupEntry.Name)).Count() > 0;
            }

            return true;
        }
        void AddOUChilds(DirectoryEntry parent, int domainId,int parentDbId, bool addChilds)
        {


            
            foreach(DirectoryEntry child in parent.Children)
            {
                Response.WriteLine("CHILD NAME " + child.Name);
                if(child.Properties["objectClass"].Contains("organizationalUnit"))
                {
                    AddOUToDB(child, domainId, parentDbId, addChilds);
                }
                else if (child.Properties["objectClass"].Contains("group") && 
                    (addChilds || (curSync.groups_select == "selected" && curSync.added_group.Count(s => s.IndexOf(child.Name) > -1) > 0)))
                {

                    AddGroupToDB(child, domainId, parentDbId);
                }
                else if (child.Properties["objectClass"].Contains("user") && !child.Properties["objectClass"].Contains("computer") && addChilds)
                {
                    AddUserToDb(child, domainId, -1, parentDbId);
                }
                else if (child.Properties["objectClass"].Contains("computer") && addChilds)
                {
                    AddComputerToDB(child, domainId, parentDbId);
                }
            }
        }

        static bool IsParent(DirectoryEntry entry)
        {
            return entry.Parent.Name.IndexOf("DC=") > -1;
        }
        void AddOUToDB(DirectoryEntry ouEntry, int domainId, int parentId = -1, bool addChilds = true)
        {
            if (ouEntry == null)
                return;


            int wasChecked = curSync.db_remove > 0 ? 1 : 0;

            string parentOuStr = parentId == -1 ? "NULL" : parentId.ToString();
            string nameToDisplay = DisplayableNameForEntry(ouEntry);
            string addQuery = "addOU " +
                 "@ou_hash='" + HashingUtils.MD5(ouEntry.Name) + "', " +
                 "@ou_name=N'" + nameToDisplay + "', " +
                 "@ou_path=N'" + ouEntry.Path + "', " +
                 "@parent_ou_id=" + parentOuStr + ", " +
                 "@domain_id=" + domainId + ", " +
                 "@was_checked=" + wasChecked;

            DataSet result = dbMgr.GetDataByQuery(addQuery);
            int ouId = result.GetInt(0, "id");

           // if(addChilds)
              AddOUChilds(ouEntry, domainId, ouId, addChilds);

            if (result.GetInt(0, "updated") == 0)
                Response.WriteLine("Added OU: " + nameToDisplay, true);
            else
                Response.WriteLine("Updated OU: " + nameToDisplay, true);

                //Response.WriteLine("Parent OU = " + parentOuStr);

                Response.Write("<script language='javascript'> window.scrollTo(0,document.body.scrollHeight); </script>");

        }

        void AddUserToDb(DirectoryEntry userEntry, int domainId, int parentGroup = -1, int parentOU = -1)
        {
            if (userEntry == null)
                return;


            string logonName = "";

            if (userEntry.Properties["userPrincipalName"].Count > 0)
                logonName = userEntry.Properties["userPrincipalName"][0].ToString().Split('@')[0];
            else if (userEntry.Properties["sAMAccountName"].Count > 0)
                logonName = userEntry.Properties["sAMAccountName"][0].ToString();

            string displayName = userEntry.Properties["displayName"].Count > 0 ? userEntry.Properties["displayName"][0].ToString() : string.Empty;


            string email = userEntry.Properties["mail"].Count > 0 ? userEntry.Properties["mail"][0].ToString() : string.Empty;
            string phone = userEntry.Properties["mobile"].Count > 0 ? userEntry.Properties["mobile"][0].ToString() : string.Empty;
            string nameToDisplay = DisplayableNameForEntry(userEntry);
            if (curSync.notImpPhones == 1)
            {
                DataSet phoneSet =
                    dbMgr.GetDataByQuery("SELECT mobile_phone FROM users WHERE name='" + logonName + "' AND role = 'U'");

                if (!phoneSet.IsEmpty)
                {
                    phone = phoneSet.GetString(0, "mobile_phone");
                }
                
            }
            string parentOuStr = parentOU == -1 ? "NULL" : parentOU.ToString();
            string parentGroupStr = parentGroup == -1 ? "NULL" : parentGroup.ToString();


           



            string sqlQuery = 
                 "addUser " +
                  "@user_hash='" + HashingUtils.MD5(userEntry.Name) + "', " +
                  "@user_name=N'" + logonName + "', " +
                  "@display_name=N'" + displayName + "', " +
                  "@mail=N'" + email + "', " +
                  "@phone=N'" + phone + "', " +
                  "@ou_id=" + parentOuStr + ", " +
                  "@group_id=" + parentGroupStr + ", " +
                  "@domain_id=" + domainId;

            DataSet result =  dbMgr.GetDataByQuery(sqlQuery);

            if (result.GetInt(0, "updated") == 0)
                Response.WriteLine("Added user: " + nameToDisplay, true);
            else
                Response.WriteLine("Updated user: " + nameToDisplay, true);
            Response.Write("<script language='javascript'> window.scrollTo(0,document.body.scrollHeight); </script>");
        }

        void AddChildrenOfGroupToDB(DirectoryEntry parent, int domainId, int parentGroup, int parentOU = -1)
        {
            object members = parent.Invoke("Members", null);


            foreach (object member in (IEnumerable)members)
            {
                DirectoryEntry child = new DirectoryEntry(member);


                if (child.Properties["objectClass"].Contains("group"))
                {
                    if (parent.Path == child.Path)
                        continue;
                    
                    AddGroupToDB(child, domainId, parentOU, parentGroup);
                }
                else if (child.Properties["objectClass"].Contains("user") && !child.Properties["objectClass"].Contains("computer"))
                {
                    AddUserToDb(child, domainId, parentGroup, parentOU);
                }
                else if (child.Properties["objectClass"].Contains("computer"))
                {
                    AddComputerToDB(child, domainId, parentOU, parentGroup);
                }
            }
               
        }
        void AddGroupToDB(DirectoryEntry groupEntry, int domainId, int parentOU = -1, int parentGroup = -1)
        {
            if (groupEntry == null)
                return;
            if (!CanAddGroup(groupEntry))
                return;

            int wasChecked = curSync.db_remove;
            string parentOuStr = parentOU == -1 ? "NULL" : parentOU.ToString();
            string parentGroupStr = parentGroup == -1 ? "NULL" : parentGroup.ToString();
            string nameToDisplay = DisplayableNameForEntry(groupEntry);

            string sqlQuery = "addGroup " +
                "@group_hash='" + HashingUtils.MD5(groupEntry.Name) + "', " +
                "@group_name=N'" + nameToDisplay + "', " +
                "@ou_id=" + parentOuStr + ", " +
                "@container_group_id=" + parentGroupStr + ", " +
                "@domain_id=" + domainId;


            DataSet result = dbMgr.GetDataByQuery(sqlQuery);

            if (result.GetInt(0, "updated") == 0)
                Response.WriteLine("Added group: " + nameToDisplay, true);
            else
                Response.WriteLine("Updated group: " + nameToDisplay, true);

            Response.Write("<script language='javascript'> window.scrollTo(0,document.body.scrollHeight); </script>");

            
            AddChildrenOfGroupToDB(groupEntry, domainId, result.GetInt(0, "id"), parentOU);
        }

        void AddComputerToDB(DirectoryEntry compEntry, int domainId, int parentOU = -1, int parentGroup = -1)
        {
            if (compEntry == null)
                return;

            string nameToDisplay = DisplayableNameForEntry(compEntry);
            string parentOuStr = parentOU == -1 ? "NULL" : parentOU.ToString();
            string parentGroupStr = parentGroup == -1 ? "NULL" : parentGroup.ToString();
            string sqlQuery = "addComp " +
                "@comp_hash='" + HashingUtils.MD5(compEntry.Name) + "', " +
                "@comp_name=N'" + nameToDisplay + "', " +
                "@ou_id=" + parentOuStr + ", " +
                "@group_id=" + parentGroupStr + ", " +
                "@domain_id=" + domainId;

            DataSet result = dbMgr.GetDataByQuery(sqlQuery);

            if (result.GetInt(0, "updated") == 0)
                Response.WriteLine("Added computer: " + nameToDisplay, true);
            else
                Response.WriteLine("Updated computer: " + nameToDisplay, true);

            Response.Write("<script language='javascript'> window.scrollTo(0,document.body.scrollHeight); </script>");

        }
        void AddGroupByName(string name, int domainId)
        {
          //  SearchResultCollection groupMembers = ADManager.GetFilteredObjects(curSync, "(&(objectCategory=group)(cn="+ name +"))");

        //    DirectoryEntry entry =  groupMembers.Cast<SearchResult>().First().GetDirectoryEntry();

            DirectoryEntry entry = new DirectoryEntry("LDAP://"+name);
            AddGroupToDB(entry, domainId);
        }


        void ResetSynchronizationForDomain(int domainId)
        {
            if (curSync.db_remove != 1)
                return;

            string query =  @"IF OBJECT_ID('tempdb..#tempGroups') IS NOT NULL DROP TABLE #tempGroups" +
            " CREATE TABLE #tempGroups (id BIGINT NOT NULL)" +
            " INSERT INTO #tempGroups (id) (SELECT id FROM groups WHERE domain_id IN (" + domainId + "))" +
            " UPDATE users_groups SET was_checked=0 WHERE user_id IN (SELECT id FROM users WHERE domain_id IN (" + domainId + ")) OR group_id IN (SELECT id FROM #tempGroups)" + 
            " UPDATE groups_groups SET was_checked=0 WHERE group_id IN (SELECT id FROM #tempGroups) OR container_group_id IN (SELECT id FROM #tempGroups)" + 
            " UPDATE computers_groups SET was_checked=0 WHERE comp_id IN (SELECT id FROM computers WHERE domain_id IN (" + domainId + ")) OR group_id IN (SELECT id FROM #tempGroups)" + 
            " DROP TABLE #tempGroups";

            dbMgr.ExecuteQuery(query);

            dbMgr.ExecuteQuery("UPDATE groups SET was_checked=0 WHERE domain_id IN (" + domainId + ")");
		dbMgr.ExecuteQuery("UPDATE users SET was_checked=0 WHERE domain_id IN ("+domainId+")");
		dbMgr.ExecuteQuery("UPDATE computers SET was_checked=0 WHERE domain_id IN ("+domainId+")");
		dbMgr.ExecuteQuery("UPDATE OU SET was_checked=0 WHERE Id_domain IN ("+domainId+")");
        }

        void DeleteNonSynchedObjects(int domainId)
        {

            if (curSync.db_remove != 1)
                return;


            Response.Write("Deleting old objects...");
            
            string sqlQuery = @"SET NOCOUNT ON" + "\n" +

            "IF OBJECT_ID('tempdb..#tempUsers') IS NOT NULL DROP TABLE #tempUsers" + "\n" +
            "IF OBJECT_ID('tempdb..#tempGroups') IS NOT NULL DROP TABLE #tempGroups" + "\n" +
            "IF OBJECT_ID('tempdb..#tempComps') IS NOT NULL DROP TABLE #tempComps" + "\n" +
            "IF OBJECT_ID('tempdb..#tempOUs') IS NOT NULL DROP TABLE #tempOUs" + "\n" +

            "CREATE TABLE #tempUsers (id BIGINT NOT NULL)" + "\n" +
            "CREATE TABLE #tempGroups (id BIGINT NOT NULL)" + "\n" +
            "CREATE TABLE #tempComps (id BIGINT NOT NULL)" + "\n" +
            "CREATE TABLE #tempOUs (id BIGINT NOT NULL)" + "\n" +

            "INSERT INTO #tempGroups (id) (SELECT id FROM groups WHERE domain_id IN (" + domainId + "))" + "\n" +
            "DELETE FROM users_groups WHERE was_checked=0 AND (user_id IN (SELECT id FROM users WHERE domain_id IN (" + domainId + ")) OR group_id IN (SELECT id FROM #tempGroups))" + "\n" +
            "DELETE FROM groups_groups WHERE was_checked=0 AND (group_id IN (SELECT id FROM #tempGroups) OR container_group_id IN (SELECT id FROM #tempGroups))" + "\n" +
            "DELETE FROM computers_groups WHERE was_checked=0 AND (comp_id IN (SELECT id FROM computers WHERE domain_id IN (" + domainId + ")) OR group_id IN (SELECT id FROM #tempGroups))" + "\n" +
            "TRUNCATE TABLE #tempGroups" + "\n" +

            "INSERT INTO #tempUsers (id) (SELECT id FROM users WHERE was_checked=0 AND domain_id IN (" + domainId + "))" + "\n" +
            "INSERT INTO #tempGroups (id) (SELECT id FROM groups WHERE was_checked=0 AND domain_id IN (" + domainId + "))" + "\n" +
            "INSERT INTO #tempComps (id) (SELECT id FROM computers WHERE was_checked=0 AND domain_id IN (" + domainId + "))" + "\n" +
            "INSERT INTO #tempOUs (id) (SELECT Id FROM OU WHERE was_checked=0 AND Id_domain IN (" + domainId + "))" + "\n" +

            "DELETE FROM User_synch WHERE Id_user IN (SELECT id FROM #tempUsers)" + "\n" +
            "DELETE FROM Comp_synch WHERE Id_comp IN (SELECT id FROM #tempComps)" + "\n" +
            "DELETE FROM Group_synch WHERE Id_group IN (SELECT id FROM #tempGroups)" + "\n" +
            "DELETE FROM OU_synch WHERE Id_OU IN (SELECT id FROM #tempOUs)" + "\n" +

            "DELETE FROM OU_User WHERE Id_OU IN (SELECT id FROM #tempOUs) OR Id_user IN (SELECT id FROM #tempUsers)" + "\n" +
            "DELETE FROM OU_comp WHERE Id_OU IN (SELECT id FROM #tempOUs) OR Id_comp IN (SELECT id FROM #tempComps)" + "\n" +
            "DELETE FROM OU_group WHERE Id_OU IN (SELECT id FROM #tempOUs) OR Id_group IN (SELECT id FROM #tempGroups)" + "\n" +
            "DELETE FROM OU_hierarchy WHERE Id_parent IN (SELECT id FROM #tempOUs) OR Id_child IN (SELECT id FROM #tempOUs)" + "\n" +
            "DELETE FROM OU_DOMAIN WHERE Id_OU IN (SELECT id FROM #tempOUs)" + "\n" +

       /*     "SELECT name FROM users WHERE id IN (SELECT id FROM #tempUsers)" + "\n" +
            "SELECT name FROM groups WHERE id IN (SELECT id FROM #tempGroups)" + "\n" +
            "SELECT name FROM computers WHERE id IN (SELECT id FROM #tempComps)" + "\n" +
            "SELECT name FROM OU WHERE Id IN (SELECT id FROM #tempOUs)" + "\n" +*/

            "DELETE FROM users WHERE id IN (SELECT id FROM #tempUsers)" + "\n" +
            "DELETE FROM groups WHERE id IN (SELECT id FROM #tempGroups)" + "\n" +
            "DELETE FROM computers WHERE id IN (SELECT id FROM #tempComps)" + "\n" +
            "DELETE FROM OU WHERE Id IN (SELECT id FROM #tempOUs)" + "\n" +

            "DROP TABLE #tempUsers" + "\n" +
            "DROP TABLE #tempGroups" + "\n" +
            "DROP TABLE #tempComps" + "\n" +
            "DROP TABLE #tempOUs";

            dbMgr.ExecuteQuery(sqlQuery);

            Response.WriteLine("Done");
            Response.Write("<script language='javascript'> window.scrollTo(0,document.body.scrollHeight); </script>");
            


        }

        protected void Page_Load(object sender, EventArgs e)
        {

            Server.ScriptTimeout = 600;
            string syncId = Request["sync_id"];

            if (String.IsNullOrEmpty(syncId))
                Response.End();

            Response.BufferOutput = false;
            DataSet syncSet = dbMgr.GetDataByQuery("SELECT name, sync_data, start_time, recur_id FROM synchronizations WHERE id='" + syncId.Replace("'", "''") + "'");

            string syncData = syncSet.GetString(0, "sync_data");

           //Response.ShowJavaScriptAlert(syncData);
            Regex rxOus = new Regex("selectedOUs\":(\"[^'\"]*\")");

           
            MatchCollection mc = rxOus.Matches(syncData);

            if(mc.Count > 0)
            { 
                    string ous = mc[0].Groups[1].Value;

                      string replacement = String.Format("selectedOUs\":[{0}]", ous);
                      syncData = rxOus.Replace(syncData, replacement);
             }


            Regex rxGroups = new Regex("added_group\":(\"[^'\"]*\")");

            mc = rxGroups.Matches(syncData);
            if (mc.Count > 0)
            {
                string groups = mc[0].Groups[1].Value;

                string replacement = String.Format("added_group\":[{0}]", groups);
                string badValue = mc[0].Groups[0].Value;

                syncData = syncData.Replace(badValue, replacement);
              //  syncData = rxGroups.Replace(syncData, replacement);
            }

            
            curSync = new JavaScriptSerializer().Deserialize<Synchronization>(syncData);
           
            
            DataSet domainSet = dbMgr.GetDataByQuery("SELECT * FROM domains WHERE name = '"+ curSync.domain + "'");

            

            Domain domain = new Domain();
            if(domainSet.IsEmpty)
            {
                domainSet = dbMgr.GetDataByQuery(string.Format("INSERT INTO domains (name, all_groups, all_ous, was_checked) OUTPUT INSERTED.id, INSERTED.name VALUES('{0}', 1, 1, 1)",curSync.domain));
            
            }

            domain.Id = domainSet.GetInt(0, "id");
            domain.Name = domainSet.GetString(0, "name");

            Response.Write ("<script type=\"text/javascript\" language=\"javascript\">if(window.parent && window.parent.showLoader) window.parent.showLoader();</script>");
	        Response.Flush();
           
            DateTime startTime = DateTime.Now;
            Response.WriteLine(resources.LNG_AD_SYNC_START_TIME + ": " + DateTimeConverter.ToUiDateTime(DateTime.Now), true);
            Response.WriteLine(resources.LNG_AD_ACTION_IMPORTED_OBJECTS + ": ", true);

            ResetSynchronizationForDomain(domain.Id);
            
            if (curSync.id.Length == 0)
                curSync.id = syncId;
           
            dbMgr.ExecuteQuery("UPDATE recurrence SET occurences=1 WHERE id in (SELECT recur_id FROM synchronizations WHERE id = '" + curSync.id + "')");
           


                //  Response.Write(curSync.adPassword);
                 

                #region OU synchronization


                if (!curSync.ad_select.Equals("all") && curSync.selectedOUs != null && curSync.selectedOUs.Length > 0 )
                {
                    foreach(var ouName in curSync.selectedOUs)
                    {
                        DirectoryEntry entry = ADManager.GetOrganizationalUnitByName(curSync, ouName);

                        if(entry == null)
                        {
                           // Response.WriteLine("Missed entry for OU: " + ouName);
                            continue;
                        }

                        bool  isInOtherOUs = curSync.selectedOUs.Count(ou => !ou.Equals(ouName) && ou.IndexOf(entry.Parent.Name) > -1) > 0;
                        if (!IsParent(entry) && isInOtherOUs)
                            continue;

                        AddOUToDB(entry, domain.Id, -1, curSync.groups_select != "selected");
                    }
                

            }
            else
                {
                   // Response.ShowJavaScriptAlert("TEST");
                    SearchResultCollection searchResultsOUs = ADManager.GetFilteredObjects(curSync, AD_FILTER_OUS);

                    foreach (SearchResult sr in searchResultsOUs)
                    {

                        if (!IsParent(sr.GetDirectoryEntry()))
                            continue;
                        AddOUToDB(sr.GetDirectoryEntry(), domain.Id, -1, curSync.groups_select != "selected");


                    }                
                }
            #endregion

                
               
                #region Selected Groups Synchronization

                if (curSync.groups_select == "selected")
                {
                   // Response.WriteLine("Synchronize selected groups");
                    foreach (string groupName in curSync.added_group)
                    {
                         //Response.WriteLine(groupName);
                        // Response.Write("<script language='javascript'> window.scrollTo(0,document.body.scrollHeight); </script>");
                       // AddGroupByName(groupName, domain.Id);

                        DirectoryEntry entry = new DirectoryEntry("LDAP://" + groupName);

                        if(IsParent(entry))
                        {
                            AddGroupToDB(entry, domain.Id);
                        }
                    }
                }
                else if (curSync.groups_select == "all" && (Settings.Content["ConfDisableGroupsSync"].Equals("0") ||
                    Config.Data.GetInt("ConfDisableGroupsSync") == 0))
                {
                    SearchResultCollection searchResultsGroups = ADManager.GetFilteredObjects(curSync, AD_FILTER_GROUPS);

                    foreach (SearchResult sr in searchResultsGroups)
                    {
                        if (!IsParent(sr.GetDirectoryEntry()))
                            continue;
                        AddGroupToDB(sr.GetDirectoryEntry(), domain.Id);
                    }
                }

                #endregion

            if (curSync.groups_select != "selected" && curSync.ad_select == "all")
            {

                #region Users synchronization

                SearchResultCollection searchResultsUsers = ADManager.GetFilteredObjects(curSync, AD_FILTER_USERS);
                foreach (SearchResult sr in searchResultsUsers)
                {
                    if (!IsParent(sr.GetDirectoryEntry()))
                        continue;

                    //Response.WriteLine("Add non-parent user " + sr.GetDirectoryEntry().Name);
                    AddUserToDb(sr.GetDirectoryEntry(), domain.Id);
                }

                #endregion

                #region Computers synchronization

                SearchResultCollection searchResultsComps = ADManager.GetFilteredObjects(curSync, AD_FILTER_COMPUTERS);
                foreach (SearchResult sr in searchResultsComps)
                {
                    if (!IsParent(sr.GetDirectoryEntry()))
                        continue;

                    AddComputerToDB(sr.GetDirectoryEntry(), domain.Id);
                }

                #endregion
            }

            DeleteNonSynchedObjects(domain.Id);

            dbMgr.ExecuteQuery("UPDATE recurrence SET occurences=0 WHERE id in (SELECT recur_id FROM synchronizations WHERE id = '" + curSync.id + "')");
            dbMgr.ExecuteQuery("UPDATE synchronizations SET last_sync_time = GETDATE() where id = '" + curSync.id + "'");
            dbMgr.Dispose();

            Response.WriteLine(resources.LNG_AD_SYNCHRONIZATION_FINISHED + ": " + DateTimeConverter.ToUiDateTime(DateTime.Now) + "(" + (DateTime.Now - startTime).TotalSeconds + " seconds)", true);
            Response.Write("<script language='javascript'> window.scrollTo(0,document.body.scrollHeight); </script>");
        }
    }
}