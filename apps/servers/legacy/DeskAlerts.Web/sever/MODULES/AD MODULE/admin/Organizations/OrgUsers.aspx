﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OrgUsers.aspx.cs" Inherits="DeskAlertsDotNet.Pages.OrgUsers" %>

<%@ Import Namespace="DeskAlertsDotNet.Parameters" %>
<%@ Import Namespace="Resources" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>DeskAlerts</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link href="../css/style9.css" rel="stylesheet" type="text/css">
    <link href="../css/jquery/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="../css/jquery/jquery_timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css">
    <script language="javascript" type="text/javascript" src="../functions.asp"></script>
    <script language="javascript" type="text/javascript" src="../jscripts/jquery/jquery.min.js"></script>
    <script language="javascript" type="text/javascript" src="../jscripts/jquery/jquery-ui.min.js"></script>
    <script language="javascript" type="text/javascript" src="../jscripts/date.js"></script>
    <script language="javascript" type="text/javascript" src="../jscripts/jquery/jquery_timepicker/jquery-ui-timepicker-addon.js"></script>
    <script language="javascript" type="text/javascript" src="../jscripts/jquery/datepicker_fix.js"></script>
    <script language="javascript" type="text/javascript">


        $(document).ready(function () {
            $(".delete_button").button({
            });

            $("#selectAll").click(function () {
                var checked = $(this).prop('checked');
                $(".content_object").prop('checked', checked);
            });

            $(document).tooltip({
                items: "img[title],a[title]",
                position: {
                    my: "center bottom-20",
                    at: "center top",
                    using: function (position, feedback) {
                        $(this).css(position);
                        $("<div>")
                            .addClass("arrow")
                            .addClass(feedback.vertical)
                            .addClass(feedback.horizontal)
                            .appendTo(this);
                    }
                }
            });

        });


        function showPreview(id) {
            window.open('../UserDetails.aspx?id=' + id, '', 'status=0, toolbar=0, width=350, height=350, scrollbars=1');
        }

        function editUser(id, queryString) {
            location.replace('../EditUsers.aspx?user_id=' + id + '&return_page=Organizations/OrgUsers.aspx?' + queryString);
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
            <tr>
                <td class="main_table_body" height="100%">
                    <br />
                    <br />
                    <table style="padding-left: 10px;" width="100%" border="0">
                        <tbody>
                            <tr style="vertical-align: middle">
                                <td style="width: 200px">
                                    <% = resources.LNG_SEARCH_USERS %>
                                    <input type="text" id="searchTermBox" runat="server" name="uname" value="" /></td>
                                <td style="width: 1%">
                                    <br />
                                    <asp:LinkButton runat="server" class="search_button ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" ID="searchButton" Style="margin-right: 0px" role="button" aria-disabled="false" Text="<span class='ui-button-icon-primary ui-icon ui-icon-search'></span><span class='ui-button-text'>{Search}</span>" />
                                    <input type="hidden" name="search" value="1" />
                                </td>
                                <td align="right">
                                    <br />
                                    <div runat="server" id="UsersAndGroups" visible="false">                                        
                                        <a href="../EditUsers.aspx?return_page=Organizations/OrgUsers.aspx" id="addUserButton" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false"><span class="ui-button-text">Add user</span></a>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div style="margin-left: 10px" runat="server" id="mainTableDiv">

                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <div style="margin-left: 10px" id="topPaging" runat="server"></div>
                                </td>
                            </tr>
                        </table>
                        <br>
                        <asp:LinkButton Style="margin-left: 10px" runat="server" ID="upDelete" class='delete_button' />
                        <br>
                        <br>
                        <asp:Table Style="padding-left: 0px; margin-left: 10px; margin-right: 10px;" runat="server" ID="contentTable" Width="98.5%" Height="100%" CssClass="data_table" CellSpacing="0" CellPadding="3">
                            <asp:TableRow CssClass="data_table_title">
                                <asp:TableCell runat="server" ID="headerSelectAll" CssClass="table_title" Width="2%">
                                    <asp:CheckBox ID="selectAll" runat="server" />
                                </asp:TableCell>
                                <asp:TableCell runat="server" ID="userName" CssClass="table_title"></asp:TableCell>
                                <asp:TableCell runat="server" ID="displayName" CssClass="table_title"></asp:TableCell>
                                <asp:TableCell runat="server" ID="domain" CssClass="table_title"></asp:TableCell>
                                <asp:TableCell runat="server" ID="mobilePhone" CssClass="table_title"></asp:TableCell>
                                <asp:TableCell runat="server" ID="email" CssClass="table_title"></asp:TableCell>
                                <asp:TableCell runat="server" ID="online" CssClass="table_title"></asp:TableCell>
                                <asp:TableCell runat="server" ID="lastActivity" CssClass="table_title"></asp:TableCell>
                                <asp:TableCell runat="server" ID="clientVersion" CssClass="table_title"></asp:TableCell>
                                <asp:TableCell runat="server" ID="actionsCollumn" CssClass="table_title"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                        <br>
                        <asp:LinkButton Style="margin-left: 10px" runat="server" ID="bottomDelete" class='delete_button' />
                        <br>
                        <br>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">

                            <tr>
                                <td>
                                    <div style="margin-left: 10px" id="bottomRanging" runat="server"></div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div runat="server" id="NoRows">
                        <br />
                        <br />
                        <center><b><%=resources.LNG_THERE_ARE_NO_USERS%><b><center>
                    </div>
                    <br>
                    <br>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
