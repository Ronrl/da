﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Synchronizations.aspx.cs" Inherits="DeskAlertsDotNet.Pages.Synchronizations" %>

<%@ Import Namespace="DeskAlertsDotNet.Parameters" %>
<%@ Import Namespace="Resources" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
<head runat="server">
    <title>DeskAlerts</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link href="css/style9.css" rel="stylesheet" type="text/css">
    <link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="css/jquery/jquery_timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css">
    <script language="javascript" type="text/javascript" src="functions.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/date.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery_timepicker/jquery-ui-timepicker-addon.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/jquery/datepicker_fix.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/shortcut.js"></script>
    <script language="javascript" type="text/javascript">
        function confirmDelete() {
            if (confirm("Are you sure you want to delete selected items?") == true)
                return true;
            else
                return false;
        }

        $(document).ready(function () {
            shortcut.add("Alt+N", function (e) {
                e.preventDefault();
                location.href = "ad_sync_form.asp";
            });

            shortcut.add("delete", function (e) {
                e.preventDefault();
                if (confirmDelete())
                    __doPostBack('upDelete', '');
            });

            var width = $(".data_table").width();
            var pwidth = $(".paginate").width();
            if (width != pwidth) {
                $(".paginate").width("100%");
            }
            $("#add_sync_button").button({
                icons: {
                    primary: "ui-icon-plusthick"
                }
            });
            $(".delete_button").button({
            });

            $(document).tooltip({
                items: "img[title],a[title]",
                position: {
                    my: "center bottom-20",
                    at: "center top",
                    using: function (position, feedback) {
                        $(this).css(position);
                        $("<div>")
                            .addClass("arrow")
                            .addClass(feedback.vertical)
                            .addClass(feedback.horizontal)
                            .appendTo(this);
                    }
                }
            });

            $("#selectAll").click(function () {
                var checked = $(this).prop('checked');
                $(".content_object").prop('checked', checked);
            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <table width="100%" cellspacing="0" cellpadding="0" class="main_table">
            <tr>
                <td width="100%" height="31" class="main_table_title">
                    <img src="images/menu_icons/synchronizations.png" alt="" width="20" height="20" border="0" style="padding-left: 7px">
                    <a href="synchronizations.aspx" class="header_title" style="position: absolute; top: 22px"><%=resources.LNG_SYNCHRONIZATIONS%></a></td>
            </tr>
            <tr>
                <td class="main_table_body" height="100%">
                    <div style="margin-left: 10px;">
                        <table style="padding-left: 10px;" width="100%" border="0">
                            <tbody>
                                <tr valign="middle">
                                    <td width="240">
                                        <% =resources.LNG_SEARCH_SYNC_BY_TITLE %>
                                        <input type="text" id="searchTermBox" runat="server" name="uname" value=""></td>
                                    <td width="1%">
                                        <br />
                                        <asp:LinkButton runat="server" class="search_button ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" ID="searchButton" Style="margin-right: 0px" role="button" aria-disabled="false" Text="<span class='ui-button-icon-primary ui-icon ui-icon-search'></span><span class='ui-button-text'>{Search}</span>" />
                                        <input type="hidden" name="search" value="1">
                                    </td>
                                    <td>
                                        <a class="delete_button" title="Hotkey: Alt+N" runat="server"
                                            id="addButton" style="float: right; margin-right: 7px;"
                                            href="ad_sync_form.asp" role="button" aria-disabled="false">
                                            <%=resources.LNG_ADD_NEW_SYNCRONIZATION %>
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div style="margin-left: 10px" runat="server" id="mainTableDiv">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <div style="margin-left: 10px" id="topPaging" runat="server"></div>
                                </td>
                            </tr>
                        </table>
                        <br>
                        <asp:LinkButton Style="margin-left: 10px" runat="server" title="Hotkey: Delete" ID="upDelete" class='delete_button' />
                        <br>
                        <br>
                        <asp:Table Style="padding-left: 0px; margin-left: 10px; margin-right: 10px;" runat="server" ID="contentTable" Width="98.5%" Height="100%" CssClass="data_table" CellSpacing="0" CellPadding="3">
                            <asp:TableRow CssClass="data_table_title">
                                <asp:TableCell runat="server" ID="headerCheckAll" CssClass="table_title" Width="2%">
                                    <asp:CheckBox ID="selectAll" runat="server" />
                                </asp:TableCell>
                                <asp:TableCell runat="server" ID="headerName" CssClass="table_title"></asp:TableCell>
                                <asp:TableCell runat="server" ID="headerSyncTime" CssClass="table_title"></asp:TableCell>
                                <asp:TableCell runat="server" ID="headerActions" CssClass="table_title" Width="10%"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                        <br>
                        <asp:LinkButton Style="margin-left: 10px" runat="server" title="Hotkey: Delete" ID="bottomDelete" class='delete_button' />
                        <br>
                        <br>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">

                            <tr>
                                <td>
                                    <div style="margin-left: 10px" id="bottomRanging" runat="server"></div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div runat="server" id="NoRows">
                        <center>
                            <b><%=resources.LNG_THERE_ARE_NO_SYNCHRONIZATIONS%></b>
                        </center>
                    </div>
                    <br>
                    <br>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
