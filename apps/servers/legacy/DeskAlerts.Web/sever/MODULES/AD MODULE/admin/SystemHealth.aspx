﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SystemHealth.aspx.cs" Inherits="DeskAlerts.Server.sever.MODULES.AD_MODULE.admin.SystemHealth" %>
<%@ Import NameSpace="Resources" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>System Health</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="css/style9.css" rel="stylesheet" type="text/css"/>
	<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
    
    <style>
        div {
            font-size: 20px;
            margin-bottom: 10px;
        }
    </style>

	<script type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
	<script type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
    
    <script type="text/javascript">
        $(document).ready(function () {
            $("#SendCrashesToSupportButton").hide();

            <%
            if (!ClientCrashFolderIsEmpty())
            {
            %>
                $.ajax({
                    type: "POST",
                    data: "{ }",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    url: "SystemHealth.aspx/IsSmtpServerConnected",
                    success: function (response) {
                        if (response.d) {
                            $("#SmtpServerInformation").text("Click at the button to send crash dumps to Deskalerts support");
                            $("#SendCrashesToSupportButton").show();
                        } else {
                            $("#SmtpServerInformation").text("Configure your SMTP server in DeskAlerts settings to have ablity to send crash dumps in one click");
                        }
                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            <% 
            }
            %>
		});
	</script>
</head>
<body>
<form runat="server">
    <div id="ClientCrashesInformation" runat="server"></div>
    <div id="SmtpServerInformation" runat="server"></div>
    <asp:Button id="SendCrashesToSupportButton" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" role="button" aria-disabled="false" runat="server" OnClick="SendCrashesToSupportButton_Click"/>
</form>
</body>
</html>
