﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="DeskAlertsDotNet.Pages.Organizations" %>

<%@ Import Namespace="DeskAlertsDotNet.Parameters" %>
<%@ Import Namespace="Resources" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" id="OrganizationAudienceHtml">
<head runat="server">
    <title>DeskAlerts</title>
</head>
<link href="../css/style9.css" rel="stylesheet" type="text/css">
<link href="../css/jquery/jquery-ui.css" rel="stylesheet" type="text/css" />
<link href="../css/jquery/jquery_timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css">
<script language="javascript" type="text/javascript" src="../functions.js"></script>
<script language="javascript" type="text/javascript" src="../jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="../jscripts/jquery/jquery-ui.min.js"></script>
<script language="javascript" type="text/javascript" src="../jscripts/date.js"></script>
<script language="javascript" type="text/javascript" src="../jscripts/jquery/jquery_timepicker/jquery-ui-timepicker-addon.js"></script>
<script language="javascript" type="text/javascript" src="../jscripts/jquery/datepicker_fix.js"></script>


<script language="javascript" type="text/javascript">

    function changeFrameContent(src) {
        $("#objectsFrame").prop("src", src);
    }

    $(document).ready(function () {

        var objectsFrameHeight = document.getElementById("objectsFrameContainer").clientHeight;
        $("#objectsFrame").css({ "height": objectsFrameHeight });
        

        $(".tree_class")
            .each(function () {

                var link = $(this).attr("href");
                //alert('123');

                if (typeof (link) != "undefined") {
                    //alert("LINK = " + link);

                    link = link.replace("javascript:", "");
                    //  $(this).attr("onClick", link);
                    $(this).attr("href", "#");

                    $(this)
                        .click(function () {

                            $(".tree_class").css("background-color", "white");
                            $(".tree_class").css("color", "black");

                            $(".root_node").css("background-color", "white");
                            $(".root_node").css("color", "black");

                            $(this).css("background-color", "#010161");
                            $(this).css("color", "white");

                            eval(link);
                        });
                    //     $(this).removeClass("tree_class");
                    // $(this).removeAttr("href");
                }

            });

        $(".root_node")
            .click(function () {

                $(".tree_class").css("background-color", "white");
                $(".tree_class").css("color", "black");
            });
        //$("#objType").change(function () {

        //    var src = $(this).val();
        //    changeFrameContent(src);

        //});
    });
</script>
<body id="OrganizationAudienceBody">
    <form id="form1" runat="server">
        <table id="OrganizationAudienceBody_tableContainer">
            <tr>
                <td>
                    <table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
                        <tr>
                            <td width="100%" height="31" class="main_table_title">
                                <img src="../images/menu_icons/organization_20.png" alt="" width="20" height="20" border="0" style="padding-left: 7px">
                                <a href="Index.aspx" class="header_title" style="position: absolute; top: 22px"><%=resources.LNG_ORGANIZATION %></a></td>
                        </tr>
                        <tr>
                            <td class="main_table_body">
                                <div>
                                    <div align="right"></div>
                                    <table style="padding: 0px; margin-top: 5px; margin-bottom: 20px; border-collapse: collapse; border: 0px; width: 100%;">
                                        <tr>
                                            <td>
                                                <%=resources.LNG_OBJECT_TYPE + ":" %>
                                                <asp:DropDownList runat="server" name="obj_type" ID="objType" AutoPostBack="true"></asp:DropDownList>
                                            </td>
                                            <td style="padding: 0px; margin: 0px; text-align: right;">
                                                <a runat="server" href="../ImportPhones.aspx?return_page=Organizations/Index.aspx" id="import_phones_button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false"><span class="ui-button-text"><%=resources.LNG_IMPORT_PHONE_IMPORT_FROM_CSV %></span></a>

                                                <a runat="server" href="../EditGroups.aspx?return_page=Organizations/Index.aspx" id="addGroupButton" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false"><span class="ui-button-text">Add group / Create audience</span></a>

                                                <a runat="server" href="../EditUsers.aspx?return_page=Organizations/Index.aspx" id="addUserButton" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false"><span class="ui-button-text">Add user</span></a>

                                            </td>
                                        </tr>
                                    </table>



                                    <input type="hidden" name="f_users" id="f_users" value="1">
                                    <input type="hidden" name="f_groups" id="f_groups" value="0">

                                    <input type="hidden" name="f_computers" id="f_computers" value="0">
                                    <table id="OrganizationAudienceBody_usersContainer">
                                        <tbody>
                                            <tr valign="top">
                                                <td width="200" id="tree_td">
                                                    <asp:TreeView runat="server" ID="orgTree" RootNodeStyle-CssClass="root_node" ParentNodeStyle-CssClass="tree_class" LeafNodeStyle-CssClass="tree_class" SelectedNodeStyle-BackColor="#010161" SelectedNodeStyle-ForeColor="White" SelectedNodeStyle-VerticalPadding="0">
                                                    </asp:TreeView>
                                                </td>
                                                <td id="objectsFrameContainer" style="background-color: #ffffff; position: relative; z-index: 1;" align="center">

                                                    <iframe runat="server" id="objectsFrame" src="" width="99%" style="border: 0px solid #ffffff;" frameborder="0"></iframe>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <div id="objects_div">
                                    </div>

                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
