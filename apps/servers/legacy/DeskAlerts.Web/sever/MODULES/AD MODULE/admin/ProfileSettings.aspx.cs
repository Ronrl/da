using DeskAlerts.ApplicationCore.Enums;
using DeskAlerts.ApplicationCore.Extensions.Enums;
using DeskAlerts.ApplicationCore.Utilities;
using DeskAlerts.Server.Utils;
using DeskAlertsDotNet.Managers;
using DeskAlertsDotNet.Utils.Factories;
using Resources;
using System;
using System.Data;
using System.Web.UI.WebControls;
using DataRow = DeskAlertsDotNet.DataBase.DataRow;
using DataSet = DeskAlertsDotNet.DataBase.DataSet;

namespace DeskAlertsDotNet.Pages
{
    public partial class ProfileSettings : DeskAlertsBasePage
    {
        protected string ErrMsg;
        //use curUser to access to current user of DA control panel
        //use dbMgr to access to database

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            addUserButton.Click += addUserButton_Click;

            if (!IsPostBack)
            {
                DataSet startPageSet = dbMgr.GetDataByQuery("SELECT name, [link] FROM menu_items WHERE enabled = 1 AND [link] <> ''");

                foreach (DataRow row in startPageSet)
                {
                    string name = row.GetString("name");

                    if (!MenuManager.Default.CanDisplayMenuItem(name, curUser))
                    {
                        continue;
                    }

                    string link = row.GetString("link");
                    string[] nameParts = name.Split('.');

                    for (int i = 0; i < nameParts.Length; i++)
                    {
                        nameParts[i] = DeskAlertsUtils.BeautifyString(nameParts[i]);
                    }

                    name = string.Join(".", nameParts);

                    ListItem listItem = new ListItem(name.Replace("_", " "), link);

                    if (curUser.StartPage != null && string.Equals(link, curUser.StartPage, StringComparison.CurrentCultureIgnoreCase))
                    {
                        listItem.Selected = true;
                    }

                    startPage.Items.Add(listItem);
                }

                SetDescriptionForLanguageSetting();

                SetLanguageForProfile();
                LoadUserData();
            }
        }

        private void SetLanguageForProfile()
        {
            DataSet langSet = dbMgr.GetDataByQuery("SELECT name, id FROM languages_list");

            foreach (DataRow langRow in langSet)
            {
                string name = langRow.GetString("name");
                int id = langRow.GetInt("id");

                ListItem listItem = new ListItem(name, id.ToString());

                if (id == curUser.LangId)
                    listItem.Selected = true;

                userLangSelect.Items.Add(listItem);
            }
        }

        private void SetDescriptionForLanguageSetting()
        {
            LanguageAdditionalDescription.InnerText = $"*{resources.LNG_LANG_DESC}.";
            LanguageAdditionalDescription.Attributes["class"] = ContentCustomClass.AdditionalDescription.GetStringValue();
        }

        private void addUserButton_Click(object sender, EventArgs e)
        {
            //Desplay a message about data saving
            Response.ShowJavaScriptAlert(resources.LNG_NOTIFICATION_OF_SAVING_SETTINGS);

            int userId = curUser.Id;
            string oldPass = Request["password"];
            string newPass = Request["newPassword"];
            string confirmPass = Request["confirmNewPassword"];

            string langId = userLangSelect.SelectedValue;
            string startPageLink = startPage.SelectedValue;
            if (Request["passwordChanged"] == "1")
            {
                ErrMsg = "";
                if (newPass != confirmPass)
                {
                    Logger.Warn($"User: \"{curUser.Name}\" failed to change password ");
                    ErrMsg = resources.LNG_PASSWORDS_DONOT_MATCH_TRY_AGAIN;
                }
                else
                {
                    string currPwdHash = dbMgr.GetScalarQuery<string>($"SELECT pass FROM users WHERE id = {curUser.Id}");

                    if (!string.Equals(currPwdHash, HashingUtils.MD5(oldPass), StringComparison.CurrentCultureIgnoreCase))
                    {
                        ErrMsg = resources.LNG_OLD_PASSWORD_IS_NOT_VALID;
                        Logger.Warn($"User: \"{curUser.Name}\" entered row \"Enter old password\" and {ErrMsg}");
                    }
                    else
                    {
                        Logger.Info($"User: \"{curUser.Name}\" has change the password");
                    }
                }

                errorLabel.Visible = ErrMsg.Length > 0;

                if (ErrMsg.Length > 0)
                    return;

                dbMgr.ExecuteQuery(
                    $"UPDATE users SET pass = '{HashingUtils.MD5(newPass)}' WHERE id = {userId}");
            }

            var startPageParam = SqlParameterFactory.Create(DbType.String, startPageLink, "startPageLink");
            var userIdParam = SqlParameterFactory.Create(DbType.Int64, userId, "userId");


            dbMgr.ExecuteQuery(
                $"UPDATE users SET start_page=@startPageLink WHERE id = @userId", startPageParam, userIdParam);

            var langIndex = SqlParameterFactory.Create(DbType.Int32, langId, "langId");
            var userIndex = SqlParameterFactory.Create(DbType.Int32, userId, "userId");
            dbMgr.ExecuteQuery($"UPDATE users SET language_id = @langId WHERE id = @userId;", langIndex, userIndex);
            UserManager.Default.ReloadCurrentUser();

            //LoadUserData();
        }

        private void LoadUserData()
        {
            userName.Value = curUser.Name;
            //email.Value = curUser.Email;
            //mobilePhone.Value = curUser.Phone;
        }
    }
}