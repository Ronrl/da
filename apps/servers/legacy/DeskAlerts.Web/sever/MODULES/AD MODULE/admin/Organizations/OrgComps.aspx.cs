﻿using DeskAlerts.Server.Utils;
using DeskAlerts.Server.Utils.Helpers;

namespace DeskAlertsDotNet.Pages
{
    using DeskAlertsDotNet.DataBase;
    using DeskAlertsDotNet.Parameters;
    using System;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;

    using Resources;

    public partial class OrgComps : DeskAlertsBaseListPage
    {
        protected override HtmlInputText SearchControl => searchTermBox;

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            searchButton.Text = searchButton.Text.Replace("{Search}", resources.LNG_SEARCH);

            int cnt = this.Content.Count < (Offset + Limit) ? this.Content.Count : Offset + Limit;

            compName.Text = GetSortLink(resources.LNG_COMPUTER_NAME, "name", Request["sortBy"]);
            domain.Text = GetSortLink(resources.LNG_DOMAIN, "domains.name", Request["sortBy"]);
            online.Text = GetSortLink(resources.LNG_ONLINE, "last_request", Request["sortBy"]);
            lastActivity.Text = GetSortLink(resources.LNG_LAST_ACTIVITY, "last_request", Request["sortBy"]);
            actionsCollumn.Text = resources.LNG_ACTION;

            base.Table = contentTable;

            for (int i = Offset; i < cnt; i++)
            {
                DataRow row = this.Content.GetRow(i);
                TableRow tableRow = new TableRow();
                CheckBox chb = GetCheckBoxForDelete(row);
                TableCell checkBoxCell = new TableCell();
                checkBoxCell.HorizontalAlign = HorizontalAlign.Center;
                checkBoxCell.Controls.Add(chb);
                tableRow.Cells.Add(checkBoxCell);

                TableCell compNameCell = new TableCell();
                compNameCell.Text = row.GetString("name");
                tableRow.Cells.Add(compNameCell);

                TableCell domainCell = new TableCell();
                domainCell.Text = row.GetString("domain_name");
                domainCell.HorizontalAlign = HorizontalAlign.Center;
                tableRow.Cells.Add(domainCell);

                DateTime lastRequest = row.GetDateTime("last_request");

                Image statusImg = new Image
                {
                    Width = 11,
                    Height = 11,

                };

                if (lastRequest.CompareTo(DateTime.MinValue) != 0)
                {
                    int dateDiff = (int)(DateTime.Now - lastRequest).TotalMinutes;
                    string nextRequest = row.GetString("next_request");
                    int onlineCounter = 2;
                    if (nextRequest.Length != 0)
                    {
                        onlineCounter = (Convert.ToInt32(nextRequest) / 2) * 60;
                    }

                    if (onlineCounter < dateDiff)
                    {
                        statusImg.ImageUrl = "../images/offline.gif";
                        statusImg.AlternateText = resources.LNG_OFFLINE;
                    }
                    else
                    {
                        statusImg.ImageUrl = "../images/online.gif";
                        statusImg.AlternateText = resources.LNG_ONLINE;
                    }
                }
                else
                {
                    statusImg.ImageUrl = "../images/offline.gif";
                    statusImg.AlternateText = resources.LNG_OFFLINE;
                }

                TableCell statusCell = new TableCell();
                statusCell.Controls.Add(statusImg);
                statusCell.HorizontalAlign = HorizontalAlign.Center;

                tableRow.Cells.Add(statusCell);

                var lastActivityCell = new TableCell
                {
                    Text = DeskAlertsUtils.IsNotEmptyDateTime(lastRequest)
                        ? DateTimeConverter.ToUiDateTime(lastRequest)
                        : string.Empty
                };

                tableRow.Cells.Add(lastActivityCell);

                LinkButton previewButton = GetActionButton(row.GetString("id"), "../images/action_icons/preview.png", "LNG_VIEW_USER_DETAILS", delegate { ScriptManager.RegisterClientScriptBlock(this, GetType(), "ORG_USERS_PREVIEW_" + row.GetString("id"), "showPreview(" + row.GetString("id") + ")", true); });
                var actionsCell = new TableCell {HorizontalAlign = HorizontalAlign.Center};

                actionsCell.Controls.Add(previewButton);

                tableRow.Cells.Add(actionsCell);
                contentTable.Rows.Add(tableRow);
            }
        }

        /// <summary>
        /// Gets the collumns.
        /// </summary>
        protected override string[] Collumns
        {
            get { return new string[] { }; }
        }

        /// <summary>
        /// Gets the content name.
        /// </summary>
        protected override string ContentName
        {
            get
            {
                return resources.LNG_GROUPS;
                //return base.ContentName;
            }
        }

        /// <summary>
        /// Gets the data table.
        /// </summary>
        protected override string DataTable
        {
            get { return "computers"; }
        }

        /// <summary>
        /// Gets the no rows div.
        /// </summary>
        protected override System.Web.UI.HtmlControls.HtmlGenericControl NoRowsDiv
        {
            get { return NoRows; }
        }

        /// <summary>
        /// Gets the table div.
        /// </summary>
        protected override System.Web.UI.HtmlControls.HtmlGenericControl TableDiv
        {
            get { return mainTableDiv; }
        }

        /// <summary>
        /// Gets the delete buttons.
        /// </summary>
        protected override IButtonControl[] DeleteButtons
        {
            get { return new IButtonControl[] { upDelete, bottomDelete }; }
        }

        /// <summary>
        /// Gets the default order collumn.
        /// </summary>
        protected override string DefaultOrderCollumn
        {
            get { return "name"; }
        }

        /// <summary>
        /// The get filter for query.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetFilterForQuery()
        {
            string searchType = Request["type"];

            string searchId = Request["id"];

            string query = @"SET NOCOUNT ON IF OBJECT_ID('tempdb..#tempOUs') IS NOT NULL
                                    DROP TABLE #tempOUs
                                    CREATE TABLE #tempOUs (Id_child BIGINT NOT NULL, Rights BIT NOT NULL);

                                    IF OBJECT_ID('tempdb..#tempComputers') IS NOT NULL
                                    DROP TABLE #tempComputers
                                    CREATE TABLE #tempComputers (comp_id BIGINT NOT NULL);

                                    DECLARE @now DATETIME;

                                    SET @now = GETDATE();";

            if (searchType == "ou")
            {
                if (Request["search"] == "1")
                {
                    query += @"WITH OUs (id) AS
                                      (SELECT CAST(" + searchId + @" AS BIGINT)
                                       UNION ALL SELECT Id_child
                                       FROM OU_hierarchy h
                                       INNER JOIN OUs ON OUs.id = h.Id_parent)
                                    INSERT INTO #tempOUs (Id_child, Rights)
                                      (SELECT DISTINCT id,
                                                       " + searchId + @"
                                       FROM OUs) OPTION (MaxRecursion 10000);";
                }
                else
                {
                    query += @" INSERT INTO #tempOUs (Id_child, Rights) VALUES (" + searchId + ", 0)";
                }
            }
            else if (searchType == "DOMAIN")
            {
                query += $@" INSERT INTO #tempOUs (Id_child, Rights) (SELECT id, 0 FROM OU WHERE Id_domain = {searchId})";
            }
            else
            {
                query += " INSERT INTO #tempOUs (Id_child, Rights) (SELECT id, 0 FROM OU) ";
            }

            if (searchType != "ou")
            {
                query += @" INSERT INTO #tempComputers(comp_id)
                              (SELECT DISTINCT computers.id
                               FROM computers
                               LEFT JOIN #tempComputers t ON t.comp_id=computers.id
                               WHERE t.comp_id IS NULL";

                if (searchType == "DOMAIN")
                {
                    query += $" AND computers.domain_id = {searchId}";
                }

                if (IsPostBack && SearchTerm.Length > 0)
                {
                    query += " AND (computers.name LIKE '%" + SearchTerm + "%')";
                }
                else if (!String.IsNullOrEmpty(SearchTerm))
                {
                    query += $" AND (computers.name LIKE N'%{this.SearchTerm}%')";
                }

                query += ")";
            }
            else
            {
                query += @" INSERT INTO #tempComputers(comp_id)
                      (SELECT DISTINCT Id_comp
                       FROM OU_comp c
                       INNER JOIN #tempOUs t ON c.Id_OU=t.Id_child
                       LEFT JOIN computers cmp ON c.Id_comp = cmp.id";

                if (!String.IsNullOrEmpty(SearchTerm))
                {
                    query += $" AND (cmp.name LIKE N'%{this.SearchTerm}%')";
                }

                query += ")";
            }

            return query;
        }

        /// <summary>
        /// Gets the custom query.
        /// </summary>
        protected override string CustomQuery
        {
            get
            {
                string query = GetFilterForQuery();

                query += @"SELECT computers.id AS id,
                           context_id,
                           computers.name AS name,
                           next_request,
                           standby,
                           computers.domain_id,
                           domains.name AS domain_name,
                           reg_date,
                           last_date,
                           convert(varchar,last_request) AS last_request1,
                           last_request,
                           edir_context.name AS context_name
                    FROM #tempComputers t
                    INNER JOIN computers ON t.comp_id = computers.id
                    LEFT JOIN domains ON domains.id = computers.domain_id
                    LEFT JOIN edir_context ON edir_context.id = computers.context_id";

                if (!string.IsNullOrEmpty(Request["sortBy"]))
                {
                    query += " ORDER BY " + Request["sortBy"];
                }
                else
                {
                    query += " ORDER BY name";
                }

                query += @" DROP TABLE #tempOUs
                            DROP TABLE #tempComputers";

                return query;
            }
        }

        /// <summary>
        /// Gets the custom count query.
        /// </summary>
        protected override string CustomCountQuery
        {
            get
            {
                string query = GetFilterForQuery();

                query += @" SELECT COUNT(1) AS cnt
                        FROM #tempComputers t
                        INNER JOIN computers ON t.comp_id = computers.id";
                return query;
            }
        }

        protected override string PageParams
        {
            get
            {
                var uri = string.Empty;
                uri += AddParameter(uri, "type");
                uri += AddParameter(uri, "id");
                return uri;
            }
        }

        /// <summary>
        /// Gets the top paging.
        /// </summary>
        protected override HtmlGenericControl TopPaging
        {
            get
            {
                return topPaging;
            }
        }

        /// <summary>
        /// Gets the bottom paging.
        /// </summary>
        protected override HtmlGenericControl BottomPaging
        {
            get
            {
                return bottomPaging;
            }
        }
    }
}