﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using Castle.MicroKernel.Lifestyle;
using DeskAlerts.Server.Utils.Interfactes;
using DeskAlertsDotNet.Application;
using DeskAlertsDotNet.Parameters;

using Resources;

namespace DeskAlertsDotNet.Pages
{
    public partial class Synchronizations : DeskAlertsBaseListPage
    {
        private readonly ITokenService _tokenService;

        public Synchronizations()
        {
            using (Global.Container.BeginScope())
            {
                _tokenService = Global.Container.Resolve<ITokenService>();
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            searchTermBox.Value = SearchTerm;
            searchButton.Text = searchButton.Text.Replace("{Search}", resources.LNG_SEARCH);
            headerName.Text = resources.LNG_NAME;
            headerSyncTime.Text = resources.LNG_LAST_SYNC_TIME;
            headerActions.Text = resources.LNG_ACTIONS;
            Table = contentTable;
            FillTableWithContent(true);

            if (!IsPostBack)
            {
                addButton.HRef = Config.Data.HasModule(Modules.ED) ? "edir_sync_form.asp" : "ad_sync_form.asp";
            }
        }

        protected override string[] Collumns => new string[] { "id", "name", "last_sync_time" };

        protected override string DataTable => "synchronizations";

        protected override string ContentName => "Synchronizations";

        protected override string ConditionSqlString
        {
            get
            {
                if (SearchTerm.Length > 0)
                    return "name LIKE '%" + SearchTerm + "%'";

                return string.Empty;
            }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl TopPaging => topPaging;

        protected override System.Web.UI.HtmlControls.HtmlGenericControl BottomPaging => bottomRanging;

        protected override IButtonControl[] DeleteButtons => new IButtonControl[] { upDelete, bottomDelete };

        protected override string DefaultOrderCollumn => "name";

        protected override List<Control> GetActionControls(DataBase.DataRow row)
        {
            var controls = new List<Control>();
            var id = row.GetString("id");
            var editButton = GetActionButton(id, "images/action_icons/edit.png", "LNG_EDIT_SYNCHRONIZATION", delegate { Response.Redirect("ad_sync_form.asp?id=" + id); });
            var startButton = GetActionButton(id, "images/action_icons/start.png", "LNG_START_SYNCHRONIZATION", delegate
            {
                _tokenService.RemoveAllTokens();
                Response.Redirect("ad_action.asp?sync_id=" + id);
            });

            controls.Add(editButton);
            controls.Add(startButton);

            return controls;

        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl NoRowsDiv => NoRows;

        protected override System.Web.UI.HtmlControls.HtmlGenericControl TableDiv => mainTableDiv;
    }
}