﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Web.Services;
using DeskAlerts.ApplicationCore.Enums;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Parameters;
using Resources;


namespace DeskAlertsDotNet.Pages
{
    public partial class CreateShortcut : DeskAlertsBasePage
    {
        protected string deskAlertsApi;
        //use curUser to access to current user of DA control panel      
        //use dbMgr to access to database
        string BuildInstantTile(DataRow row)
        {
            if (row == null)
                return "";

            string shortcutName = row.GetString("title").Trim();

            Regex rx = new Regex("[*|\\:\"<>?/]");
            shortcutName = rx.Replace(shortcutName, "");
            string tileHtml = @"<li id='" + row.GetString("id") + "' class='im_tile' style='display:inline-block; margin:0px;padding:10px; background-color: transparent;height:220px;' alert='" + row.GetString("alert_id") + "'>";

            if (row.IsNull("approve_status") || row.GetInt("approve_status") == 1)
                tileHtml += "<a style='text-decoration:none' href='#' onclick=\"return checkRights(" + row.GetString("alert_id") + "," + shortcutName.Quotes() + ");\" target='work_area'>";

            string title = row.GetString("title");

            deskAlertsApi = Config.Data.GetString("alertsDir") + "/admin/create_button/DeskAlertsAPI.exe";
            string shortTitle = title.Clone().ToString();

            object colorObj = dbMgr.GetScalarByQuery("SELECT color FROM color_codes WHERE id='" + row.GetString("color_code") + "'");
            string color = "";
            if (colorObj == null)
                color = "747474";
            else
                color = colorObj.ToString();


            if (shortTitle.Length > 17)
                shortTitle = shortTitle.Substring(0, 14) + "...";

            tileHtml += "<div class='im_tile_header'><span style='margin:0px' class='header_title header_white' title='This message id: " + row.GetString("id") + "'> " + shortTitle + " </span></div>";

            tileHtml += "<div style='vertical-align:middle; background:#" + color + "' class='im_tile_send' title='" + resources.LNG_SEND + "'>";

            tileHtml += "<img id='" + row.GetString("id") + "' src='images/action_icons/select_big_white.png' style='display:block;margin:auto;border:none;padding-top:15px;padding-left:10px;padding-bottom: 10px;' width='100'/>";
            
            tileHtml += "<script>";
            tileHtml += "$('#" + row.GetString("id") + "').attr('src', 'images/action_icons/select_big_' + darkOrLight('" + color + "') + '.png');";
            tileHtml += "</script>";

            if (row.IsNull("approve_status") || row.GetInt("approve_status") == 1)
                tileHtml += "</a>";

            tileHtml += "<div style=\"width:100%;height:15px;background-color:#f0f0f1;padding:15px 0px;\">";
            tileHtml += "<a href='javascript:showAlertPreview(\"" + row.GetString("alert_id") + "\")'><div class='im_tile_preview hover' style='float: none; height: auto; width: auto; position: relative; margin-left: 100%; left: -55%;' title=" + resources.LNG_PREVIEW + "><img src='images/action_icons/preview_big.png' alt=" + resources.LNG_PREVIEW + " title=" + resources.LNG_PREVIEW + " style='display:block;auto;border:none;width:22px;height:22px'/></div></a>";
            tileHtml += "</div>";
            tileHtml += "</li>";

            return tileHtml;
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            var query = $@"
                SELECT im.id as id, im.alert_id, a.title, a.approve_status, a.color_code 
                FROM alerts a 
                INNER JOIN instant_messages im ON a.id = im.alert_id 
                WHERE a.approve_status = {(int)ApproveStatus.ApprovedByDefault} 
                    OR a.approve_status = {(int)ApproveStatus.NotModerated} 
                    OR a.approve_status IS NULL 
                ORDER BY create_date DESC";

            var imSet = dbMgr.GetDataByQuery(query);

            noAlertsDiv.Visible = imSet.IsEmpty;
            giveMeHeight.Visible = !imSet.IsEmpty;

            foreach (var imRow in imSet)
            {
                imTiles.InnerHtml += BuildInstantTile(imRow);
            }
        }

        [WebMethod]
        public static string GenerateShortcut(int alertId, string alertName)
        {
            if (alertId <= 0 || alertName.Length == 0)
                return "Can not recognize alert identifier.";

            alertName = alertName.Replace(" ", "_").Replace("#", "");

            string apiKey = Settings.Content["ConfAPISecret"];

            if (apiKey.Length == 0)
                return "Can not read ApiKey. Try again or change ApiKey in Settings.";

            string templateFolderPath = Config.Data.GetString("alertsFolder") + "\\admin\\create_button";
            string shortcutFolderPath = Config.Data.GetString("alertsFolder") + "\\admin\\images\\upload\\shortcuts";

            if (Directory.Exists(shortcutFolderPath))
            {
                Directory.Delete(shortcutFolderPath, true);
                Directory.CreateDirectory(shortcutFolderPath);
            }
            else
            {
                Directory.CreateDirectory(shortcutFolderPath);
            }

            string batFileText = File.ReadAllText(templateFolderPath + "\\send.bat");

            batFileText =
                batFileText.Replace("%server%", Config.Data.GetString("alertsDir") + "/api_request.asp")
                    .Replace("%apikey%", apiKey)
                    .Replace("%alertid%", alertId.ToString());

            string shortcut = alertName.Trim("*|\\:\"<>?/#".ToCharArray());

            if (File.Exists(templateFolderPath + "\\Send Instant " + shortcut + ".bat"))
                File.Delete(templateFolderPath + "\\Send Instant " + shortcut + ".bat");
            File.WriteAllText(shortcutFolderPath + "\\Send Instant " + shortcut + ".bat", batFileText);

            return Config.Data.GetString("alertsDir") + "/admin/images/upload/shortcuts/Send Instant " + shortcut +
                   ".bat";


        }
    }
}