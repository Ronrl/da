<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InstantMessages.aspx.cs" Inherits="DeskAlertsDotNet.sever.MODULES.INSTANT_MESSAGES.admin.InstantMessages" %>

<%@ Import Namespace="DeskAlertsDotNet.Parameters" %>
<%@ Import Namespace="Resources" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="css/style9.css" rel="stylesheet" type="text/css" />
    <link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="css/jquery/jquery_timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="functions.js"></script>
    <script type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="jscripts/jquery.hotkeys.js"></script>
    <script type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
    <script type="text/javascript" src="jscripts/date.js"></script>
    <script type="text/javascript" src="jscripts/preview.js?v=9.2.1.31"></script>
    <script type="text/javascript" src="jscripts/jquery/jquery_timepicker/jquery-ui-timepicker-addon.js"></script>
    <script type="text/javascript" src="jscripts/jquery/datepicker_fix.js"></script>
    <script type="text/javascript" src="jscripts/toastr.min.js"></script>
    <script>
        $(function () {
            var height = $(window.parent.document).height();            
            window.parent.parent.onChangeBodyHeight(height, false);
        });
        function DeleteAlert(id) {

            $.ajax({
                type: "POST",
                url: "InstantMessages.aspx/Delete",
                data: '{id:' + id + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    location.reload();

                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        }

        function Send(id) {

            $.ajax({
                type: "POST",
                url: "InstantMessages.aspx/Send",
                data: '{id:' + id + ', buildCache:1}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    location.href = "PopupSent.aspx?imSent=1";

                },
                error: function (response) {
                    toastr.info("DeskAlerts server is not responding. Please contact your system administrator.");
                }
            });
        }
    </script>
</head>

<body>
    <form id="form1" runat="server">
        <div>
            <table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
                <tr>
                    <td>
                        <table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
                            <td width="100%" height="31" class="main_table_title">
                                <img src="images/menu_icons/im_20.png" alt="" width="20" height="20" border="0" style="padding-left: 7px">
                                <asp:LinkButton href="InstantMessages.aspx" runat="server" ID="headerTitle" class="header_title" Style="position: absolute; top: 22px" />
                            </td>
                            <tr>
                                <td class="main_table_body" height="100%">

                                    <div runat="server" style="padding: 10px" id="giveMeHeight">
                                        <ul runat="server" id="imTiles" style="list-style: none; margin: 0px; padding: 0px">
                                        </ul>
                                    </div>
                                    <div runat="server" id="noAlertsDiv" style="padding: 10px"><%=resources.LNG_THERE_ARE_NO_INSTANT_MESSAGES %></div>
                                </td>
                            </tr>
                        </table>
                    </td>
            </table>

        </div>
    </form>
</body>
</html>
