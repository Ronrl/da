﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CreateShortcut.aspx.cs" Inherits="DeskAlertsDotNet.Pages.CreateShortcut" %>
<%@ Import NameSpace="DeskAlertsDotNet.Parameters" %>
<%@ Import NameSpace="Resources" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
      <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/style9.css" rel="stylesheet" type="text/css">
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/preview.js?v=9.2.1.31"></script>
<script language="javascript" type="text/javascript">
    $(function () {
        //$("#im_tiles").sortable();
        $("#imTiles").disableSelection();
        $(".hover").hover(function () {
            $(this).switchClass("", "im_tile_hovered", 200);
        }, function () {
            $(this).switchClass("im_tile_hovered", "", 200);
        });
    });

    function detectIE() {
        var ua = window.navigator.userAgent;
        var msie = ua.indexOf('MSIE ');
        var trident = ua.indexOf('Trident/');

        if (msie > 0) {
            // IE 10 or older => return version number
            return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
        }

        if (trident > 0) {
            // IE 11 (or newer) => return version number
            var rv = ua.indexOf('rv:');
            return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
        }

        // other browser
        return false;
    }

    function checkRights(alertId, alertName) {
        if (alertId) {
            jQuery("li[alert]").css("background-color", "transparent");
            jQuery("li[alert=" + alertId + "]").css("background-color", "#5CB852");
            jQuery("#selectedId").val(alertId);
            jQuery("#selectedName").val(alertName);
           
        }
    }

    function darkOrLight(color) {
        red = parseInt(color.substring(0, 2), 16);
        green = parseInt(color.substring(3, 5), 16);
        blue = parseInt(color.substring(5), 16);
        var brightness;
        brightness = (red * 299) + (green * 587) + (blue * 114);
        brightness = brightness / 255000;

        // values range from 0 to 1
        // anything greater than 0.5 should be bright enough for dark text
        if (brightness >= 0.5) {
            return "black";
        } else {
            return "white";
        }
    }

    function showAlertPreview(alertId) {
        var data = new Object();

        getAlertData(alertId, false, function (alertData) {
            data = JSON.parse(alertData);
        });

        var fullscreen = parseInt(data.fullscreen);
        var ticker = parseInt(data.ticker);
        var acknowledgement = data.acknowledgement;

        var alert_width = (fullscreen == 1) && !ticker ? $(".main_table_body").width() : (!ticker ? data.alert_width : $(".main_table_body").width());
        var alert_height = (fullscreen == 1) && !ticker ? $(window).height() - document.body.clientHeight + $(".main_table_body").height() : (!ticker ? data.alert_height : "");
        var alert_title = data.alert_title;
        var alert_html = data.alert_html;

        var top_template;
        var bottom_template;
        var templateId = data.template_id;

        if (templateId >= 0) {
            getTemplateHTML(templateId, "top", false, function (data) {
                top_template = data;
            });

            getTemplateHTML(templateId, "bottom", false, function (data) {
                bottom_template = data;
            });
        }

        data.top_template = top_template;
        data.bottom_template = bottom_template;
        data.alert_width = alert_width;
        data.alert_height = alert_height;
        data.ticker = ticker;
        data.fullscreen = fullscreen;
        data.acknowledgement = acknowledgement;
        data.alert_title = alert_title;
        data.alert_html = alert_html;
        data.caption_href = data.caption_href;

        initAlertPreview(data);

    }
    jQuery(document).ready(function () {

        jQuery("#create").click(function () {
            if ($("#selectedId").val() > 0) {
                alertName = jQuery("#selectedName").val();
                jQuery.ajax({
                    url: "CreateShortcut.aspx/GenerateShortcut",
                    method: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: "{alertId: " + $("#selectedId").val() + ", alertName:\"" + $("#selectedName").val() + "\"}",
                    success: function(response) {

                        var data = response.d;
                        if (data.indexOf("http://") == 0 || data.indexOf("https://") == 0) {
                            var body = "";
                            body = "You created shortcut for message «" + alertName + "».";
                            body += "<br/>" +
                                "<%=resources.LNG_SH_PAGE_DOWN_INST %>" +
                                " <a target='_blank' style='text-decoration:underline;font-weight:bold;' href='<%=deskAlertsApi%>'>DeskAlertsAPI.exe</a>";
                            body += "<br/>" + "<%=resources.LNG_SH_PAGE_INST %>";
                            body += "<br/>You can download «" +
                                alertName +
                                "» message sending shortcut from here: <a target='_blank' id='download_bat' style='text-decoration:underline;font-weight:bold;' href='" +
                                data +
                                "' type='application/batch-SMTP'>" +
                                alertName +
                                "</a>";
                            jQuery("#result_title").text("This functionality works only on Windows OS. Note:");
                            jQuery("#result_body").html(body);
                            jQuery("#result").css("display", "inline-block");
                            jQuery("#result").css("background-color", "#B3FFA3");
                        } else {
                            warning(data);
                        }
                    },
                    failure: function(response) {
                        warning(response.d);
                    }
                });
            }
            else alert("You should select the message for creation shortcut");
        });


        function warning(textError) {
            var body = "";
            body = "<b>Shortcut creation system is busy now. Please try again in few seconds.<b>";
            //   if (textError){
            //     body += "<br/>"+textError;
            //  }
            //  jQuery("#result_title").text("Warning:");
            jQuery("#result_body").html(body);
            jQuery("#result").css("display", "inline-block");
            jQuery("#result").css("background-color", "#FFFFFF");
        }

    });

</script>
  
</head>
<body>
    <form id="form1" runat="server">
    <div>
     <table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%" >
	
	    <tr>
	    <td>
	    <table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
		    <tr>
			    <td width=100% height=31 class="main_table_title"><img src="images/menu_icons/create_shortcut.png" alt="" width="20" height="20" border="0" style="padding-left:7px">
		    <span class="header_title" style="position:absolute;top:22px"><%=resources.LNG_CREATE_SHORTCUT %></span></td>
		    </tr>
		    <tr>
		    <td class="main_table_body" height="100%" style="padding:10px;padding-top:30px">  
                 <form id="form_create">
                    <span style="margin:30px;"><%=resources.LNG_SELECT_MESSAGE_FOR_SHORTCUT%></span>
		            <div runat="server" style="padding:10px; overflow: hidden; overflow-y: auto; margin: 10px 20px 10px 20px; height:auto; /*white-space: nowrap;*/ " id="giveMeHeight">
			            <ul runat="server" id="imTiles" style="list-style:none; margin:0px; padding:0px;">
                        </ul>
                    </div>
                     <div runat="server" id="noAlertsDiv" style="padding:10px"><%=resources.LNG_THERE_ARE_NO_INSTANT_MESSAGES %></div>

                    <a id="create" class="logout ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" style="cursor:pointer; float:right;margin:30px" aria-disabled="false">
                        <span class="ui-button-text"><%=resources.LNG_CREATE_SHORTCUT%></span>
                    </a>
                   
                    <div id="result" style="border: 1px solid #696969; margin:30px 0px 30px 40px;padding:10px;line-height:20px;display:none;max-width:500px; word-wrap:break-word;">
                        <span id="result_title" style="font-weight:bold;font-size:22px;"></span>
                        <span id="result_body" style="font-size:12px;"></span>
                    </div>
	                <input type="hidden" value="0" id="selectedId" name="selectedId" />
                    <input type="hidden" value="" id="selectedName" name="selectedName" />
                 </form>
            </td>
          </tr>
        </table>
        </td>
        </tr>

    </table>
    </div>
    </form>
</body>
</html>
