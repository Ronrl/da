﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<!-- #include file="md5.asp" -->
<%
if Request("widget")<>"" then
	widget=1
else
	widget=0
end if

check_session()

uid = Session("uid")
deskalertsapi = alerts_folder&"admin/create_button/DeskAlertsAPI.exe"
if (uid <> "")  then
im_arr = Array ("","","","","","","")
Set RS = Conn.Execute("SELECT id FROM users WHERE id="&uid&" AND role='E'")
	if(Not RS.EOF) then
		gid = 1
		Set rs_policy = Conn.Execute("SELECT type FROM policy INNER JOIN policy_editor ON policy.id=policy_editor.policy_id WHERE editor_id=" & uid)
		if(Not rs_policy.EOF) then
			if(rs_policy("type")="A") then
				gid = 0
			end if
		end if
		if(gid=1) then
			editor_id=RS("id")
			policy_ids=""
			policy_ids=editorGetPolicyIds(editor_id)
			im_arr = editorGetPolicyList(policy_ids, "im_val")
		end if
	else
		gid = 0
	end if
	RS.Close
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>DeskAlerts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/style9.css" rel="stylesheet" type="text/css">
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/preview.js?v=9.2.1.31"></script>
<script language="javascript" type="text/javascript">
	$(function() {
	//$("#im_tiles").sortable();
		$("#im_tiles").disableSelection();
		$(".hover").hover(function() {
			$(this).switchClass("", "im_tile_hovered", 200);
		}, function() {
			$(this).switchClass("im_tile_hovered", "", 200);
		});
		
		if (detectIE()!=false){
			$("#giveMeHeight").css("height",240);
		}
		else{
			$("#giveMeHeight").css("height",240);
		}
	});
	
	function detectIE() {
			var ua = window.navigator.userAgent;
			var msie = ua.indexOf('MSIE ');
			var trident = ua.indexOf('Trident/');

			if (msie > 0) {
				// IE 10 or older => return version number
				return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
			}

			if (trident > 0) {
				// IE 11 (or newer) => return version number
				var rv = ua.indexOf('rv:');
				return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
			}

			// other browser
			return false;
		}

	function checkRights(alertId,alertName) {
		if (alertId)
        { 
            jQuery("li[alert]").css("background-color","transparent");
            jQuery("li[alert="+alertId+"]").css("background-color","#5CB852");
            jQuery("#selectedId").val(alertId);
            jQuery("#selectedName").val(alertName);
            //decision = <%if(gid = 0 OR (gid = 1 AND im_arr(2)<>"")) then Response.Write("true") else Response.Write("false") end if%>;
        }
	}

	function darkOrLight(color) {
		red = parseInt(color.substring(0,2),16);
		green = parseInt(color.substring(3,5),16);
		blue = parseInt(color.substring(5),16);
		  var brightness;
		  brightness = (red * 299) + (green * 587) + (blue * 114);
		  brightness = brightness / 255000;

		  // values range from 0 to 1
		  // anything greater than 0.5 should be bright enough for dark text
		  if (brightness >= 0.5) {
			return "black";
		  } else {
			return "white";
		  }
		}

	function showAlertPreview(alertId) {
		var data = new Object();

		getAlertData(alertId, false, function(alertData) {
			data = JSON.parse(alertData);
		});

		var fullscreen = parseInt(data.fullscreen);
		var ticker = parseInt(data.ticker);
		var acknowledgement = data.acknowledgement;

		var alert_width = (fullscreen == 1) && !ticker ? $(".main_table_body").width() : (!ticker ? data.alert_width : $(".main_table_body").width());
		var alert_height = (fullscreen == 1) && !ticker ? $(window).height() - document.body.clientHeight + $(".main_table_body").height() : (!ticker ? data.alert_height : "");
		var alert_title = data.alert_title;
		var alert_html = data.alert_html;

		var top_template;
		var bottom_template;
		var templateId = data.template_id;

		if (templateId >= 0) {
			getTemplateHTML(templateId, "top", false, function(data) {
				top_template = data;
			});

			getTemplateHTML(templateId, "bottom", false, function(data) {
				bottom_template = data;
			});
		}

		data.top_template = top_template;
		data.bottom_template = bottom_template;
		data.alert_width = alert_width;
		data.alert_height = alert_height;
		data.ticker = ticker;
		data.fullscreen = fullscreen;
		data.acknowledgement = acknowledgement;
		data.alert_title = alert_title;
		data.alert_html = alert_html;
		data.caption_href = data.caption_href;

		initAlertPreview(data);

	}

    jQuery(document).ready(function(){

        jQuery("#create").click(function(){
            if (jQuery("#selectedId").val() > 0)
            {
                alertName = jQuery("#selectedName").val();
                jQuery.ajax({
                        url: "create_button.asp",
                        method: "POST",
                        data: jQuery("#form_create").serialize()
                    }).done(function(data, textStatus, jqXHR) {
                        if (data.indexOf("http://") == 0){
                            var body = "";
                            body = "You created shortcut for message «"+alertName+"».";
                            body += "<br/>"+"<%=LNG_SH_PAGE_DOWN_INST %>"+" <a target='_blank' style='text-decoration:underline;font-weight:bold;' href='<%=deskalertsapi%>'>DeskAlertsAPI.exe</a>";
                            body += "<br/>"+"<%=LNG_SH_PAGE_INST %>";
                            body += "<br/>You can download «"+alertName+"» message sending shortcut from here: <a target='_blank' id='download_bat' style='text-decoration:underline;font-weight:bold;' href='"+data+"' type='application/batch-SMTP'>"+alertName+"</a>";
                            jQuery("#result_title").text("Note:");
                            jQuery("#result_body").html(body);
                            jQuery("#result").css("display","inline-block");
                            jQuery("#result").css("background-color","#B3FFA3");
                        }
                        else {
                            warning(data);
                        }
                    })
                    .fail(function(jqXHR, textStatus, errorThrown){
                        warning(jqXHR.responseText);
                    });
            }
            else alert("You should select the message for creation shortcut");
        });


        function warning(textError){
            var body = "";
            body = "<b>Shortcut creation system is busy now. Please try again in few seconds.<b>";
         //   if (textError){
           //     body += "<br/>"+textError;
          //  }
          //  jQuery("#result_title").text("Warning:");
            jQuery("#result_body").html(body);
            jQuery("#result").css("display","inline-block");
            jQuery("#result").css("background-color", "#FFFFFF");
        }

    });
</script>

</head>
<body style="margin:0px" class="body">

<%

Set RSInstant = Conn.Execute("SELECT im.id as id, im.alert_id, a.title, a.color_code FROM alerts a INNER JOIN instant_messages im ON a.id = im.alert_id ORDER BY create_date DESC")
%>
<%if widget=0 then %>
<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%" >
	
	<tr>
	<td>
	<table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
		<tr>
			<td width=100% height=31 class="main_table_title"><img src="images/menu_icons/create_shortcut.png" alt="" width="20" height="20" border="0" style="padding-left:7px">
		<span class="header_title" style="position:absolute;top:15px"><%=LNG_CREATE_SHORTCUT %></span></td>
		</tr>
		<tr>
		<td class="main_table_body" height="100%" style="padding:10px;padding-top:30px">
<%end if %>
		<%if not RSInstant.EOF then %>
        <form id="form_create">
        <span style="margin:30px;"><%=LNG_SELECT_MESSAGE_FOR_SHORTCUT%></span>
		<div style="padding:10px; overflow: hidden; overflow-y: auto; margin: 10px 20px 10px 20px; height:auto; /*white-space: nowrap;*/ " id="giveMeHeight">
			<ul id="im_tiles" style="list-style:none; margin:0px; padding:0px;">
				<%Do while not RSInstant.EOF 
					instant_message_id=RSInstant("id")
					nested_id=Replace(instant_message_id,"{","")
					nested_id=Replace(nested_id,"}","")
					alert_id=RSInstant("alert_id")
					alert_title=RSInstant("title")
					color_code = RSInstant("color_code")
					if color_code <>"" then
						SQL = "SELECT color FROM color_codes WHERE id='"&color_code&"'"
						Set RSCC = Conn.Execute(SQL)
						if RSCC.EOF then
							color_code = "747474"
						else
							color_code = RSCC("color")
						end if
					else
						color_code = "747474"
					end if
					if Len(alert_title) > 17 then
						alert_title_shortened = Left(alert_title,14) & "..."
					else
						alert_title_shortened = alert_title
					end if
				%>
                <%
                    shortcutName = Trim(alert_title)
                    Set objRegExp = CreateObject("VBScript.RegExp")
                    objRegExp.Pattern = "[*|\\:""""<>?/]"
                    objRegExp.Global = True
                    shortcutName = objRegExp.Replace(shortcutName, "")
                %>
				<li id="<%=instant_message_id%>" class="im_tile" style="display:inline-block; margin:0px;padding:10px; background-color: transparent;height:220px;" alert="<%=alert_id%>">
					<a style="text-decoration:none;cursor:pointer;" <%if widget=1 then %>target="work_area"<%end if %> onclick="return checkRights('<%=alert_id%>','<%=shortcutName%>');">
						<div class="im_tile_header">
                            <span style="margin:0px" class="header_title header_white" title="This message id: <%=alert_id%>"><%=alert_title_shortened%></span>
                        </div>
						<div style='vertical-align:middle; background:#<%=color_code%>' class="im_tile_send" title="<%=LNG_SEND %>">
                            <img id="<%=nested_id %>" src="images/action_icons/select_big_white.png" style="display:block;margin:auto;border:none;padding-top:15px;padding-left:10px" width="100"/>
						</div>
						<script>
							$("#<%=nested_id%>").attr("src", "images/action_icons/select_big_" + darkOrLight("<%=color_code%>") + ".png");
						</script>
					</a>
                    <div style="width:100%;height:15px;background-color:#f0f0f1;padding:10px 0px;">
					    <a href="javascript: <%if widget=1 then Response.write("parent.") end if %>showAlertPreview('<%=alert_id %>')">
                            <div class="im_tile_preview hover" style="float:none;height:auto;width:auto;position:relative;margin-left:100%;left:-55%;" title="<%=LNG_PREVIEW %>">
                                <img src="images/action_icons/preview_big.png" alt="<%=LNG_PREVIEW %>" title="<%=LNG_PREVIEW %>" style="display:block;margin:0px auto;border:none;width:20px;height:20px"/>
                            </div>
                        </a>
                    </div>
				</li>
				<%
				RSInstant.MoveNext
				loop
				 %>
			</ul>
		</div>
        <input type="hidden" value="0" id="selectedId" name="selectedId" />
        <input type="hidden" value="" id="selectedName" name="selectedName" />
        <a id="create" class="logout ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" style="cursor:pointer; float:right;margin:30px" aria-disabled="false">
            <span class="ui-button-text"><%=LNG_CREATE_SHORTCUT%></span>
        </a>
        </form>
        <div id="result" style="border: 1px solid #696969; margin:30px 0px 30px 40px;padding:10px;line-height:20px;display:none;max-width:500px; word-wrap:break-word;">
            <span id="result_title" style="font-weight:bold;font-size:15px;"></span>
            <span id="result_body" style="font-size:12px;"></span>
        </div>
        
		<%else %>
		<div style="padding:10px"><%=LNG_THERE_ARE_NO_INSTANT_MESSAGES %></div>
		<%end if %>

        
		</td>
		</tr>
	</table>
	</td>
	</tr>
</table>
</body>
</html>
<%

else

  Response.Redirect "index.asp"

end if

%> 
</body>
</html>
<!-- #include file="db_conn_close.asp" -->