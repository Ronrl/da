﻿using System;
using System.Web.Services;
using DeskAlerts.ApplicationCore.Enums;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Models.Permission;
using DeskAlertsDotNet.Pages;
using DeskAlertsDotNet.Parameters;
using Resources;

namespace DeskAlertsDotNet.sever.MODULES.INSTANT_MESSAGES.admin
{
    using DataRow = DataRow;
    using DataSet = DataSet;

    public partial class InstantMessages : DeskAlertsBasePage
    {
        [WebMethod]
        public static void Delete(int id)
        {
            using (var dataBaseManager = new DBManager())
            {
                dataBaseManager.ExecuteQuery("DELETE FROM alerts WHERE id = " + id);
                dataBaseManager.ExecuteQuery("DELETE FROM instant_messages WHERE alert_id = " + id);
            }
        }

        [WebMethod]
        public static void Send(int id)
        {
            ApiRequestHandler.Send(id);
        }

        public string BuildInstantTile(DataRow row)
        {
            if (row == null)
            {
                return string.Empty;
            }

            string title = row.GetString("title");
            string shortTitle = title.Clone().ToString();

            string tileHtml = @"<li id='" + row.GetString("id") + "' class='im_tile' name='" + shortTitle + "' style='float:left; margin:10px'>";


            if (shortTitle.Length > 17)
            {
                shortTitle = shortTitle.Substring(0, 14) + "...";
            }

            bool canSend = (row.IsNull("approve_status") || row.GetInt("approve_status") == 1) &&
                           (curUser.HasPrivilege || curUser.Policy[Policies.IM].CanSend);
            if (canSend)
            {
                tileHtml += "<a style='text-decoration:none' href='#' onclick='Send(" + row.GetInt("alert_id") + ")' target='work_area'>";
            }

            object colorObj = row.GetString("color_code").Length == 0 ?
                                  null : dbMgr.GetScalarByQuery("SELECT color FROM color_codes WHERE id='" + row.GetString("color_code") + "'");

            var color = colorObj?.ToString() ?? "747474";
            
            tileHtml += "<div class='im_tile_header'><span style='margin:0px' class='header_title header_white' title='This message id: " + row.GetString("id") + "'> " + shortTitle + " </span></div>";

            tileHtml += "<div style='vertical-align:middle; background:#" + color + "' class='im_tile_send' title='" + resources.LNG_SEND + "'>";

            if (row.GetInt("approve_status") == 1)
            {
                tileHtml += "<img id='" + row.GetString("id") + "' src='images/action_icons/send_big_white.png' style='display:block;margin:auto;border:none;padding-top:15px;padding-left:10px' width='100'/>";
            }
            else if (row.GetInt("approve_status") == 0)
            {
                tileHtml += "<img id='" + row.GetString("id") + "' src='images/action_icons/pending_white.png' style='display:block;margin:auto;border:none;padding-top:15px;padding-left:10px' width='100'/>";
            }

            tileHtml += "<script>";
            if (row.IsNull("approve_status") || row.GetInt("approve_status") == 1)
            {
                tileHtml += "$('#" + row.GetString("id") + "').attr('src', 'images/action_icons/send_big_' + darkOrLight('" + color + "') + '.png');";
            }
            else
            {
                tileHtml += "$('#" + row.GetString("id") + "').attr('src', 'images/action_icons/pending_' + darkOrLight('" + color + "') + '.png');";
            }

            tileHtml += "</script>";

            if (canSend)
            {
                tileHtml += "</a>";
            }

            tileHtml += "<a href='javascript:showAlertPreview(\"" + row.GetString("alert_id") + "\")'><div class='im_tile_preview hover' style='margin-top: 25px;' title=" + resources.LNG_PREVIEW + "><img src='images/action_icons/preview_big.png' alt=" + resources.LNG_PREVIEW + " title=" + resources.LNG_PREVIEW + " style='display:block;margin:7px auto;border:none;width:22px;height:22px'/></div></a>";

            if (curUser.HasPrivilege || curUser.Policy[Policies.IM].CanEdit)
            {
                tileHtml += "<a href='CreateAlert.aspx?id=" + row.GetInt("alert_id") + "&instant=1&edit=1' target='work_area'><div class='im_tile_edit im_tile_preview hover' style='margin-top: 25px; 'title=" + resources.LNG_EDIT + "><img src='images/action_icons/pencil_20x20.png' alt=" + resources.LNG_EDIT + " title=" + resources.LNG_EDIT + " style='display:block;margin:10px auto;border:none;'/></div></a>";
            }

            if (Config.Data.IsDemo && curUser.Name != "admin" && !curUser.IsSalesManager)
            {
                tileHtml += "<a href='#' onclick='alert(\"" + resources.LNG_DISABLED_IN_DEMO + "\");'><div class='im_tile_delete hover' style='margin-top: 25px;' title=" + resources.LNG_DELETE + "><img src='images/action_icons/delete_big.png' style='display:block;margin:10px auto;border:none;'/></div></a>";
            }
            else
            {
                tileHtml += "<a href='#' onclick='DeleteAlert(" + row.GetInt("alert_id") + ")'><div class='im_tile_delete hover' style='margin-top: 25px;' title=" + resources.LNG_DELETE + "><img src='images/action_icons/delete_big.png' style='display:block;margin:10px auto;border:none;'/></div></a>";
            }

            tileHtml += "</li>";

            return tileHtml;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            headerTitle.Text = resources.LNG_INSTANT_MESSAGES;

            var sqlQuery = $@"
                SELECT im.id as id, im.alert_id, a.title, a.approve_status, a.color_code 
                FROM alerts a 
                INNER JOIN instant_messages im ON a.id = im.alert_id 
                WHERE (a.approve_status = {(int)ApproveStatus.ApprovedByDefault} 
                    OR a.approve_status = {(int)ApproveStatus.NotModerated} 
                    OR a.approve_status IS NULL) ";

            if (!curUser.HasPrivilege && !curUser.Policy.CanViewAll)
            {
                var viewListStr = string.Join(",", curUser.Policy.SendersViewList.ToArray());
                sqlQuery += "AND a.sender_id IN (" + viewListStr + ") ";
            } 
            sqlQuery += "ORDER BY create_date DESC";

            var dataByQuery = dbMgr.GetDataByQuery(sqlQuery);

            noAlertsDiv.Visible = dataByQuery.IsEmpty;
            giveMeHeight.Visible = !dataByQuery.IsEmpty;

            foreach (var dataRow in dataByQuery)
            {
                imTiles.InnerHtml += this.BuildInstantTile(dataRow);
            }
        }
    }
}