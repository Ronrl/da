<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<!-- #include file="md5.asp" -->
<%
check_session()

uid = Session("uid")

if (uid <> "")  then
    
    alertId = Request("selectedId")  
    alertName = Request("selectedName")  
    if (alertId <> "0" AND alertId <> "") then

        apikey = ""
        Set RS = Conn.Execute("SELECT val as apikey FROM settings WHERE name='ConfAPISecret' ")
	    if(Not RS.EOF) then
		    apikey = RS("apikey")
	    end if
	    RS.Close

        if (apikey <> "") then
    
            Const ForReading = 1
            Const ForWriting = 2

            Set objFSO = CreateObject("Scripting.FileSystemObject")
            server_apth_download = Server.MapPath("images\upload\")
            server_apth = Server.MapPath("create_button\")
            if objFSO.FolderExists(server_apth_download & "\shortcuts") then
                objFSO.DeleteFolder server_apth_download & "\shortcuts"
            end if
            
            objFSO.CreateFolder(server_apth_download & "\shortcuts")

            if objFSO.FileExists(server_apth & "\send.bat")  then 
                objFSO.CopyFile server_apth & "\send.bat", server_apth_download & "\shortcuts\" 
            end If 

			'Response.Write server_apth & "\download\send.bat"
            Set objFile = objFSO.OpenTextFile(server_apth_download & "\shortcuts\send.bat", ForReading)

            strText = objFile.ReadAll
            objFile.Close
            strNewText = Replace(strText, "%server%", alerts_folder & "api_request.asp")
            strNewText = Replace(strNewText, "%apikey%", apikey)
            strNewText = Replace(strNewText, "%alertid%", alertId) 
            Set objFile = objFSO.OpenTextFile(server_apth_download & "\shortcuts\send.bat", ForWriting)
            objFile.WriteLine strNewText
            objFile.Close
            
            shortcutName = Trim(alertName)
            Set objRegExp = CreateObject("VBScript.RegExp")
            objRegExp.Pattern = "[*|\\:""""<>?/]"
            objRegExp.Global = True
            shortcutName = objRegExp.Replace(shortcutName, "")

            objFSO.MoveFile server_apth_download & "\shortcuts\send.bat", server_apth_download & "\shortcuts\Send Instant "&shortcutName&".bat"

            Response.Write alerts_folder & "admin/images/upload/shortcuts/Send Instant "&shortcutName&".bat"
        else
            Response.Write "Can not read ApiKey. Try again or change ApiKey in Settings."
        end if
    else
        Response.Write "Can not recognize alert identifier."
    end if

else

  Response.Write "You are not logged in."

end if
%> 
<!-- #include file="db_conn_close.asp" -->