﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web.Services;
using Castle.MicroKernel.Lifestyle;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.ApplicationCore.Repositories;
using DeskAlerts.ApplicationCore.Utilities;
using DeskAlerts.Server.Utils;
using DeskAlerts.Server.Utils.Helpers;
using DeskAlerts.Server.Utils.Interfaces.Services;
using DeskAlerts.Server.Utils.Managers;
using DeskAlerts.Server.Utils.Services;
using DeskAlertsDotNet.Application;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Managers;
using DeskAlertsDotNet.Pages;
using DeskAlertsDotNet.Parameters;
using DeskAlertsDotNet.Utils.Consts;
using DeskAlertsDotNet.Utils.Factories;

using DataRow = DeskAlertsDotNet.DataBase.DataRow;
using DataSet = DeskAlertsDotNet.DataBase.DataSet;
using User = DeskAlertsDotNet.Models.User;

namespace DeskAlertsDotNet.sever.MODULES.INSTANT_MESSAGES
{
    public partial class ApiRequestHandler : DeskAlertsBasePage
    {
        private static User _currentUser;
        private static readonly ContentReceivedService ContentReceivedService = new ContentReceivedService();
        private static readonly ICacheInvalidationService CacheInvalidationService;
        private static readonly IAlertRepository AlertRepository;

        static ApiRequestHandler()
        {
            using (Global.Container.BeginScope())
            {
                CacheInvalidationService = Global.Container.Resolve<ICacheInvalidationService>();
                AlertRepository = Global.Container.Resolve<IAlertRepository>();
            }
        }                         

        [WebMethod]
        public static int Send(int id, long senderId = 0)
        {
            try
            {
                _currentUser = UserManager.Default.CurrentUser;

                using (var dataBaseManager = new DBManager())
                {
                    var templateRow = dataBaseManager.GetDataByQuery("SELECT * FROM alerts WHERE id = " + id).First();

                    if (templateRow == null)
                    {
                        //return;
                    }

                    DateTime startDate = templateRow.GetDateTime("from_date");
                    DateTime endDate = templateRow.GetDateTime("to_date");

                    int diff = (int)(endDate - startDate).TotalMinutes;

                    DateTime newFromDate = DateTime.Now;
                    newFromDate = newFromDate.AddMilliseconds(-newFromDate.Millisecond);
                    DateTime newEndDate = DateTime.Now.AddMinutes(diff);
                    newEndDate = newEndDate.AddMilliseconds(-newEndDate.Millisecond);

                    dataBaseManager.ExecuteQuery("SET LANGUAGE BRITISH");

                    string query =
                        @"INSERT INTO alerts (title, alert_text, create_date, type, from_date, to_date, schedule, schedule_type, recurrence, urgent, email, desktop, sms, email_sender, ticker, fullscreen, alert_width,  alert_height, aknown,  autoclose,  toolbarmode, deskalertsmode, template_id, sender_id, caption_id, sent_date, class, type2, group_type, parent_id, resizable,lifetime, color_code, device, approve_status) " +
                        " OUTPUT INSERTED.id SELECT title, alert_text, " + DateTimeConverter.ToDbDateTime(DateTime.Now).Quotes() + ", 'S', " +
                        DateTimeConverter.ToDbDateTime(newFromDate).Quotes() + ", " + DateTimeConverter.ToDbDateTime(newEndDate).Quotes() + ", 0 " +
                        ", 1, recurrence, urgent, email, desktop, sms, email_sender, ticker, fullscreen, alert_width,  alert_height, aknown,  autoclose,  toolbarmode, deskalertsmode, template_id, sender_id, caption_id," + DateTimeConverter.ToDbDateTime(DateTime.Now).Quotes() +
                        ", 1, type2, group_type, " + id + ", resizable, lifetime, color_code, device, 1" +
                        " FROM alerts WHERE id =" + id;

                    var alertSet = dataBaseManager.GetDataByQuery("SELECT * FROM alerts WHERE id = " + id);
                    var alertForSending = dataBaseManager.GetDataByQuery(query).First();
                    var alertId = alertForSending.GetInt("id");

                    if (senderId != 0)
                    {
                        var insertedAlert = AlertRepository.GetById(alertId);

                        insertedAlert.SenderId = senderId;

                        AlertRepository.Update(insertedAlert);
                    }

                    var device = alertForSending.GetInt("device");
                    var sendType = templateRow.GetString("type2");

                    switch (sendType)
                    {
                        case "B":
                            sendType = "broadcast";
                            break;
                        case "R":
                            sendType = "recipients";
                            break;
                        default:
                            sendType = "iprange";
                            break;
                    }

                    string addRecipientsQuery = "INSERT INTO alerts_sent (alert_id, user_id) SELECT  " + alertId
                                                                                                       + ", user_id FROM alerts_sent WHERE alert_id ="
                                                                                                       + id + " "
                                                                                                       + "INSERT INTO alerts_sent_comp (alert_id, comp_id) SELECT  "
                                                                                                       + alertId
                                                                                                       + ", comp_id FROM alerts_sent_comp WHERE alert_id ="
                                                                                                       + id + " "
                                                                                                       + "INSERT INTO alerts_sent_group (alert_id, group_id) SELECT  "
                                                                                                       + alertId
                                                                                                       + ", group_id FROM alerts_sent_group WHERE alert_id ="
                                                                                                       + id + " "
                                                                                                       + "INSERT INTO alerts_sent_ou (alert_id, ou_id) SELECT  "
                                                                                                       + alertId
                                                                                                       + ", ou_id FROM alerts_sent_ou WHERE alert_id ="
                                                                                                       + id + " "
                                                                                                       + "INSERT INTO alerts_sent_stat (alert_id, user_id, date) SELECT  "
                                                                                                       + alertId
                                                                                                       + ", user_id, GETDATE() FROM alerts_sent_stat WHERE alert_id ="
                                                                                                       + id + " "
                                                                                                       + "INSERT INTO alerts_sent_iprange (alert_id, ip_range_group_id, ip_from, ip_to) SELECT  "
                                                                                                       + alertId
                                                                                                       + ", ip_range_group_id, ip_from, ip_to FROM alerts_sent_iprange WHERE alert_id ="
                                                                                                       + id + " ";
                    dataBaseManager.ExecuteQuery(addRecipientsQuery);

                    var isScheduled = alertSet.GetInt(0, "schedule") == 1;
                    var isEmailModuleEnabled = Config.Data.HasModule(Modules.EMAIL);
                    var isEmailAlert = alertSet.GetInt(0, "email") == 1;
                    var isSmsAlert = alertSet.GetInt(0, "sms") == 1;
                    var isSmsModuleEnabled = Config.Data.HasModule(Modules.SMS);
                    var isTextToCallAlert = alertSet.GetInt(0, "text_to_call") == 1;
                    var isTextToCallModuleEnabled = Config.Data.HasModule(Modules.TEXTTOCALL);
                    var returnPage = "PopupSent.aspx";

                    if (!isScheduled && ((isEmailModuleEnabled && isEmailAlert) || (isSmsModuleEnabled && isSmsAlert)))
                    {
                        // Send email and sms
                        DataSet emailsAndPhonesSet = GetEmailAndPhoneSet(sendType, dataBaseManager, alertId);
                        List<string> mails = new List<string>();

                        foreach (DataRow emailRow in emailsAndPhonesSet)
                        {
                            mails.Add(emailRow.GetString("email"));
                        }

                        string emailsString = String.Join(",", mails.ToArray());

                        if (isEmailModuleEnabled && isEmailAlert)
                        {
                            EmailResult emailResult = EmailManager.Default.Send(
                                alertSet.GetString(0, "title"),
                                alertSet.GetString(0, "alert_text"),
                                emailsString);

                            if (emailResult.StatusCode != SmtpStatusCode.Ok)
                            {
                                returnPage = DeskAlertsUtils.AppendParameterToUrl(
                                    returnPage,
                                    "emailErrorMessage",
                                    emailResult.Message);
                            }
                        }

                        foreach (DataRow smsRow in emailsAndPhonesSet)
                        {
                            var mobilePhone = smsRow.GetString("mobile_phone");

                            if (!String.IsNullOrEmpty(mobilePhone))
                            {
                                var smsResult = SmsManager.Default.Send(
                                    alertSet.GetString(0, "alert_text"),
                                    mobilePhone);

                                if (smsResult.StatusCode != HttpStatusCode.OK)
                                {
                                    returnPage = DeskAlertsUtils.AppendParameterToUrl(
                                        returnPage,
                                        "smsErrorMessage",
                                        Regex.Replace(smsResult.Message, @"\t|\n|\r", String.Empty));
                                    break;
                                }
                            }
                        }
                    }

                    if (isTextToCallModuleEnabled && isTextToCallAlert)
                    {
                        var emailsAndPhonesSet = GetEmailAndPhoneSet(sendType, dataBaseManager, alertId);
                        foreach (var textToCallRow in emailsAndPhonesSet)
                        {
                            var mobilePhone = textToCallRow.GetString("mobile_phone");

                            if (!string.IsNullOrEmpty(mobilePhone))
                            {
                                TextToCallManager.Default.Send(alertSet.GetInt(0, "id"), mobilePhone);
                            }
                        }
                    }

                    // Send push notifications
                    if (!isScheduled && Config.Data.HasModule(Modules.MOBILE) && device != 1
                        && NeedPushForThisAlert(alertSet.First()))
                    {
                        IConfigurationManager configurationManager =
                            new WebConfigConfigurationManager(Config.Configuration);
                        IUserRepository userRepository = new PpUserRepository(configurationManager);
                        if (sendType == "broadcast")
                        {
                            var userIds = userRepository.GetAll().Select(x => x.Id);
                            PushNotificationsManager.Default.AddPushNotificationToDb(alertId, userIds);
                            
                        }
                        else if (sendType == "recipients")
                        {
                            var userIds = SelectContentRecipients(alertId).ToArray().Select(x => (long)x.GetInt("id"));
                            PushNotificationsManager.Default.AddPushNotificationToDb(alertId, userIds);
                        }
                    }

                    Global.CacheManager.Add(
                        alertId,
                        DateTime.Now.ToString(DateTimeFormat.MsSqlFormat, CultureInfo.InvariantCulture));

                    if (sendType == "iprange")
                    {
                        var ipGroups = dataBaseManager.GetDataByQuery($"SELECT alerts_sent_iprange.ip_from, alerts_sent_iprange.ip_to FROM alerts_sent_iprange WHERE alert_id = {id}");

                        ContentReceivedService.CreateRowsInAlertsReceivedForIp(ipGroups, alertId);
                    }
                    else
                    {
                        var usersAlertIdParameter = SqlParameterFactory.Create(
                            DbType.Int32,
                            alertId,
                            "alertId");

                        var userIds = dataBaseManager.GetDataByQuery(
                            "SELECT user_id FROM alerts_sent WHERE alert_id = @alertId",
                            usersAlertIdParameter).ToList();

                        var computersAlertIdParameter = SqlParameterFactory.Create(
                            DbType.Int32,
                            alertId,
                            "alertIdForComputerUsers");

                        var userIdsByComputers = dataBaseManager.GetDataByQuery(
                            @"SELECT DISTINCT ui.user_id
                              FROM alerts_sent_comp AS alertsSC
                                  JOIN user_instance_computers AS uic ON alertsSC.comp_id = uic.user_instance_computer_id
                                  JOIN user_instances AS ui ON uic.user_instance_guid = ui.clsid
                              WHERE alertsSC.alert_id = @alertIdForComputerUsers",
                            computersAlertIdParameter).ToList();

                        var allRecipientIds = userIds.Concat(userIdsByComputers);
                        var recipientIds = allRecipientIds.Select(dataRow => (long)dataRow.GetValue("user_id"));

                        ContentReceivedService.CreateRowsInAlertsReceived(sendType, alertId, recipientIds);
                    }

                    //TODO: change to real content type
                    CacheInvalidationService.CacheInvalidation(AlertType.SimpleAlert, Convert.ToInt64(alertId));

                    return alertId;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                Logger.Debug(ex);
                throw;
            }
        }

        private static DataSet SelectContentRecipients(int contentId)
        {
            var alertIdParameter = SqlParameterFactory.Create(
                DbType.Int32,
                contentId,
                "alertId");

            using (var dataBaseManager = new DBManager())
            {
                return dataBaseManager.GetDataByQuery(
                    "SELECT u.id FROM users AS u INNER JOIN alerts_sent AS a ON u.id = a.user_id WHERE u.role = 'U' AND a.alert_id = @alertId",
                    alertIdParameter);
            }
        }

        private static bool NeedPushForThisAlert(DataRow alertRow)
        {
            var alertClass = (AlertType)alertRow.GetInt("class");
            return alertClass == AlertType.SimpleAlert || alertClass == AlertType.RssAlert
                                                             || alertClass == AlertType.EmergencyAlert
                                                             || alertClass == AlertType.SimpleSurvey
                                                             || alertClass == AlertType.SurveyPoll
                                                             || alertClass == AlertType.SurveyQuiz;
        }

        private static DataSet GetEmailAndPhoneSet(string sendType, DBManager dataBaseManager, int alertId)
        {
            DataSet emailsAndPhonesSet = new DataSet();
            string query = "";

            if (sendType.Equals("recipients"))
            {
                emailsAndPhonesSet = dataBaseManager.GetDataByQuery(
                    "SELECT u.email, u.mobile_phone FROM users  as u INNER JOIN alerts_sent  as a ON u.id = a.user_id WHERE  u.role = 'U' AND a.alert_id = "
                    + alertId);
            }
            else if (sendType.Equals("broadcast"))
            {
                if (_currentUser.HasPrivilege)
                {
                    emailsAndPhonesSet = dataBaseManager.GetDataByQuery(
                        "SELECT u.email, u.mobile_phone FROM users  as u WHERE role = 'U'");
                }
                else
                {
                    query =
                        "SELECT DISTINCT users.id, users.name, users.mobile_phone, users.email FROM users LEFT JOIN policy_user ON users.id=policy_user.user_id LEFT JOIN users_groups ON users.id=users_groups.user_id LEFT JOIN OU_User ON users.id=OU_User.Id_user WHERE role='U' AND ";

                    query += "policy_id=" + _currentUser.Policy.Id;

                    if (!_currentUser.Policy.CanSendAllOUs)
                    {
                        query += " OR OU_User.Id_OU IN (" + string.Join(
                                     ",",
                                     _currentUser.Policy.OrganizationUnits.ToArray()) + ")";
                    }

                    if (!_currentUser.Policy.CanSendAllGroups)
                    {
                        query += " OR  users_groups.group_id IN ("
                                 + string.Join(",", _currentUser.Policy.Groups.ToArray()) + ")";
                    }

                    emailsAndPhonesSet = dataBaseManager.GetDataByQuery(query);
                }
            }

            return emailsAndPhonesSet;
        }
    }
}