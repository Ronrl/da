<%
'---------------ADD TO CONFIG----------------

'SMS Config
Dim smsUrl, smsPostData
' Use the following variables: %mobilePhone% as a mobile phone number, %smsText% as a SMS text
smsUrl = "%smsUrl%"
smsPostData = "%smsPostData%"

smsContentType = "application/x-www-form-urlencoded"

smsUseAuth="%smsUseAuth%" ' "0" - Don't use, "1" - Use www-authorization
smsAuthUser="%smsAuthUser%"
smsAuthPass="%smsAuthPass%"

smsProxy="%smsProxy%" ' "0" - System predefined, "1" - Direct (without proxy), "2" - Use proxy
smsProxyServer="%smsProxyServer%" ' The name of a proxy server or a list of proxy server names. For example: localhost:8080
smsProxyBypassList="%smsProxyBypassList%" ' The list of locally known host names or IP addresses for which you want to permit bypass of the proxy server.

smsProxyUseAuth="%smsProxyUseAuth%" ' "0" - Don't use, "1" - Use proxy authorization
smsProxyUser="%smsProxyUser%"
smsProxyPass="%smsProxyPass%"

smsPhoneFormat = "1"   ' "0" - Don't convert phone numbers,
                       ' "1" - Just remove all special chars but not grouping symbols,
                       ' "2" - Just remove all special chars but not grouping symbols and not lead plus "+" symbol,
                       ' "3" - Just remove all special chars,
                       ' "4" - Just remove all special chars but not lead plus "+" symbol,
                       ' "5" - Convert letters to numeric,
                       ' "6" - Convert letters to numeric and remove all special chars,
                       ' "7" - Convert letters to numeric and remove all special chars but not lead plus "+" symbol,
                       ' "8" - Use international E.164 phone format,
                       ' "9" - Use international E.164 phone format but without lead plus "+" symbol.
smsDefaultCountryCode = "US" ' ISO 3166-1 two-letter country code. Used for E.164 phone format on numbers without country prefix.


urlShortEnabled = 1

urlShortUrl = "https://www.googleapis.com/urlshortener/v1/url"
urlShortPostData = "{""longUrl"": ""%url%""}"

urlShortContentType = "application/json"

urlShortResponseType = "1" ' "1" - JSON
urlShortResponsePath = "id"

SMS=1

'---------------ADD TO CONFIG----------------
%>