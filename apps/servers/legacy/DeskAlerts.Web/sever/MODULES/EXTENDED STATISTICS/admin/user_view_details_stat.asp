<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="ad.inc" -->
<%

check_session()

%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>View user details</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
<link href="style.css" rel="stylesheet" type="text/css">
</head>
<script language="javascript">
function LINKCLICK() {
  alert ("This action is disabled in demo");
  return false;
}
</script>

<body style="background-color:#8A92A6; margin:0px" class="body">
<table width="100%" border="0" cellspacing="0" cellpadding="6" bgcolor="#8A92A6" height="100%">
	<tr>
	<td>
	<table width="100%" height="100%" border="1" bordercolor="#000000" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
		<tr>
		<td width=100% height=31 class="theme-colored-back"><a href="" class="header_title">User details</a></td>
		</tr>
		<tr>
		<td bgcolor="#ffffff" height="100%">
		<div style="margin:10px;"><br><br>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
<%
	Set RS = Conn.Execute("SELECT id, name, domain_id, deskbar_id, reg_date, last_date, convert(varchar,last_request) as last_request1, last_request FROM users WHERE id=" & Request("id"))
	num=0
	if(Not RS.EOF) then
		 Response.Write "<tr><td> <b>Name: </b></td><td>"& RS("name")&"</td></tr>"
		 if (AD = 3) then 	
	       	Set RS3 = Conn.Execute("SELECT name FROM domains WHERE id=" & RS("domain_id"))
		if(Not RS3.EOF) then
			  domain=RS3("name")
		else
			domain="&nbsp;"
		end if
		RS3.close

			Response.Write "<tr><td> <b>Domain: </b></td><td>"& domain &"</td></tr>" 
		end if
	        	mygroups=""
			Set RS1 = Conn.Execute("SELECT name FROM groups INNER JOIN users_groups ON groups.id=users_groups.group_id WHERE user_id=" & Request("id"))
'		Set RS1 = Conn.Execute("SELECT name FROM groups")
			Do While Not RS1.EOF
					mygroups=mygroups & RS1("name")

				RS1.MoveNext
				if (Not RS1.EOF) then
					mygroups=mygroups & ", "
				end if
			loop
			RS1.Close
			 Response.Write "<tr><td valign='top'> <b>Groups: </b></td><td>"&mygroups&"</td></tr>"

			if(Not IsNull(RS("last_request"))) then
				last_activ = CStr(RS("last_request1"))
		
			        mydiff=DateDiff ("n", RS("last_request"), now_db)
				if(mydiff > 10) then
					online="<img src='images/offline.gif' alt='offline' width='11' height='11' border='0'>"
				else
					online="<img src='images/online.gif' alt='online' width='11' height='11' border='0'>"
				end if
			else
				last_activ = "&nbsp;"
				online="<img src='images/offline.gif' alt='offline' width='11' height='11' border='0'>"
			end if


			 Response.Write "<tr><td valign='top'> <b>Last activity: </b></td><td>" & last_activ & "</td></tr>"
		 
		 if (AD = 0) then 	
			if(Not IsNull(RS("reg_date"))) then
			 Response.Write "<tr><td valign='top'> <b>Registration Date: </b></td><td>" &  RS("reg_date") & "</td></tr>"
			else
			 Response.Write "<tr><td valign='top'> <b>Registration Date: </b></td><td>&nbsp;</td></tr>"
			end if
		 end if
			 Response.Write "<tr><td valign='top'> <b>Online: </b></td><td>" & online & "</td></tr>"


  mysent=0
  Set RS7 = Conn.Execute("SELECT COUNT(id) as mycnt FROM [alerts_sent_stat] WHERE user_id="& Request("id"))
  if(not RS7.EOF) then
	mysent=RS7("mycnt")
  end if

  myreceived=0
  myclicks=0
  Set RS7 = Conn.Execute("SELECT COUNT(id) as mycnt FROM [alerts_received] WHERE user_id="& Request("id"))
  if(not RS7.EOF) then
	myreceived=RS7("mycnt")
  end if
  if(IsNull(myclicks)) then myclicks=0 end if

  myread=0
  Set RS7 = Conn.Execute("SELECT COUNT(id) as mycnt FROM [alerts_read] WHERE [read]='1' and user_id="& Request("id"))
  if(not RS7.EOF) then 
	myread=RS7("mycnt")
  end if


  if(mysent<>0) then
    myopen=Round(clng(myreceived)/clng(mysent)*100,2)
  else
    myopen="0"
  end if
  if(myreceived<>0) then
    myctr=Round(clng(myclicks)/clng(myreceived)*100,2)
  else
    myctr="0"
  end if

			 Response.Write "<tr><td valign='top'> <b>Sent: </b></td><td>" & mysent & "</td></tr>"
			 Response.Write "<tr><td valign='top'> <b>Received: </b></td><td>" & myreceived & "</td></tr>"
			 Response.Write "<tr><td valign='top'> <b>Read: </b></td><td>" & myread & "</td></tr>"
			 Response.Write "<tr><td valign='top'> <b>Open Ratio: </b></td><td>" & myopen & "%</td></tr>"
			 Response.Write "<tr><td valign='top'> <b>CTR: </b></td><td>" & myctr & "%</td></tr>"


	else
	Response.Write "<tr><td> Error: No such user in database! </td></tr>"
	end if
	RS.Close
%>

		
		</table>
		 <img src="images/inv.gif" alt="" width="1" height="100%" border="0">
		</div>
		</td>
		</tr>
	</table>
	</td>
	</tr>
</table>
</body>
</html>
<!-- #include file="db_conn_close.asp" -->