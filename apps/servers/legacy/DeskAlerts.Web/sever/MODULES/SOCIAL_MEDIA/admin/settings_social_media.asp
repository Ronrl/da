﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<!-- #include file="md5.asp" -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>DeskAlerts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/style9.css" rel="stylesheet" type="text/css">
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script language="javascript" type="text/javascript">

	$(document).ready(function() {
		$(".cancel_button").button();
		$(".save_button").button();
	});

</script>
</head>
<style type="text/css">
	.instructions
	{
		color:#888888;
	}
</style>
<body style="margin:0px" class="body">

<%
if (uid <> "")  then

id = Request("id")
name = Request("name")
consumerKey = Request("consumerKey")
consumerSecret = Request("consumerSecret")
accessKey = Request("accessKey")
accessSecret = Request("accessSecret")

name = Replace(name, "'", "''")
consumerKey = TrimAll(Replace(consumerKey, "'", "''"))
consumerSecret = TrimAll(Replace(consumerSecret, "'", "''"))
accessKey = TrimAll(Replace(accessKey, "'", "''"))
accessSecret = TrimAll(Replace(accessSecret, "'", "''"))

if(id = "") then
	Set rsSocialIds = Conn.Execute("SELECT id FROM social_media WHERE type=1")
	if not rsSocialIds.EOF then
		id = rsSocialIds("id")
	end if
end if

if(request("a")="2") then
	if(name<>"" and consumerKey<>"" and consumerSecret<>"" and accessKey<>"" and accessSecret<>"") then
		if(id<>"") then 'editing
		sql = "UPDATE social_media SET "&_
			"name = N'"& name &"' , "&_
			"field1 = N'"& consumerKey &"' , "&_ 
			"field2 = N'"& consumerSecret &"', "&_
			"field3 = N'"& accessKey &"', "&_
			"field4 = N'"& accessSecret &"' "&_
			"WHERE id = '" & id & "'"
		Conn.Execute(sql)
		else 'adding new
		sql = "INSERT INTO social_media (name, type, field1, field2, field3, field4) VALUES ("&_
			"N'" & name & "', 1, "&_
			"N'" & consumerKey & "', "&_
			"N'" & consumerSecret & "', "&_
			"N'" & accessKey & "', "&_
			"N'" & accessSecret & "')"
		Conn.Execute(sql)
		end if
		success = true
	else
		formFillFail = true
	end if 
else
	Set rsSocial = Conn.Execute("SELECT * FROM social_media WHERE type=1")
	if not rsSocial.EOF then
		id = rsSocial("id")
		name = rsSocial("name")
		consumerKey = rsSocial("field1")
		consumerSecret = rsSocial("field2")
		accessKey = rsSocial("field3")
		accessSecret = rsSocial("field4")
	end if
end if
%>
<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	<tr>
	<td>
	<table width="100%" height="100%" cellspacing="0" cellpadding="0">
		<tr>
		<td class="main_table_body" height="100%">
		<div style="padding:10px;">

<span class="work_header"><%=LNG_TWITTER_SETTINGS %></span><br/>
<p class="instructions" style="margin-left:3px"><%= LNG_TWITTER_INSTRUCTIONS %></p>
<form id="twitterSettings" method="post" action="settings_social_media.asp">
	<table border="0">
		<tr>
			<td colspan="2">
				<p class="instructions"><%=LNG_TWITTER_INSTRUCTIONS_ENTITY_NAME %></p>
			</td>
		</tr>
		<tr>
			<td>
				<label for="name"><%=LNG_TWITTER_ENTITY_NAME %></label>
			</td>
		</tr>
		<tr>
			<td>
				<input style="width:400px" type="text" id="name" name="name" value="<%=HtmlEncode(name) %>" autocomplete="off"/>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<p class="instructions"><%=LNG_TWITTER_INSTRUCTIONS_CONSUMER_TOKENS %></p>
			</td>
		</tr>
		<tr>
			<td>
				<label for="consumerKey"><%=LNG_CONSUMER_KEY %></label>
			</td>
		</tr>
		<tr>
			<td>
				<input style="width:400px" type="password" id="consumerKey" name="consumerKey" value="<%=HtmlEncode(consumerKey) %>" autocomplete="off"/>
			</td>
		</tr>
		<tr>
			<td>
				<label for="consumerSecret"><%=LNG_CONSUMER_SECRET %></label>
			</td>
		</tr>
		<tr>
			<td>
				<input style="width:400px" type="password" id="consumerSecret" name="consumerSecret" value="<%=HtmlEncode(consumerSecret) %>" autocomplete="off"/>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<p class="instructions"><%=LNG_TWITTER_INSTRUCTIONS_PERMISSIONS_SETTINGS %></p>
				<p class="instructions"><%=LNG_TWITTER_INSTRUCTIONS_ACCESS_TOKENS %></p>
			</td>
		</tr>
		<tr>
			<td>
				<label for="accessKey"><%=LNG_ACCESS_KEY %></label>
			</td>
		</tr>
		<tr>
			<td>
				<input style="width:400px" type="password" id="accessKey" name="accessKey" value="<%=HtmlEncode(accessKey) %>" autocomplete="off"/>
			</td>
		</tr>
		<tr>
			<td>
				<label for="accessSecret"><%=LNG_ACCESS_SECRET %></label>
			</td>
		</tr>
		<tr>
			<td>
				<input style="width:400px" type="password" id="accessSecret" name="accessSecret" value="<%=HtmlEncode(accessSecret) %>" autocomplete="off"/>
			</td>
		</tr>
		<tr>
		<td>
			<span id="warning" style="color:red;font-weight:bold;<% if formFillFail = false then Response.Write("display:none")%>">
				<%=LNG_TWITTER_ERROR_DESC %>
			</span>
		</td>
		</tr>
		<tr>
			<td>
				<span id="success" style="font-weight:bold;<% if success = false then Response.Write("display:none")%>">
					<%=LNG_TWITTER_SETTINGS_SUCCESSFULLY_CHANGED %>
				</span>
			</td>
		</tr>
		<tr>
			<td align="right" style="width:400px">
				<!--a class="cancel_button" onclick="javascript:history.go(-1); return false;"><%=LNG_CANCEL %></a!--> 
				<a class="save_button" onclick="document.forms[0].submit()" ><%=LNG_SAVE %></a>
				<input type="hidden" name="a" value="2"/>
				<input type="hidden" name="id" value="<%=id %>" />
			</td>
			<td></td>
		</tr>
	</table>
</form>

		 <img src="images/inv.gif" alt="" width="1" height="100%" border="0"/>
		</div>
		</td>
		</tr>
	</table>
	</td>
	</tr>
</table>

</body>
</html>

<!-- #include file="db_conn_close.asp" -->