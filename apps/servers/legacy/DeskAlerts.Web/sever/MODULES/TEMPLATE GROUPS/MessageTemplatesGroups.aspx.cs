﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Net;
using System.IO;

namespace deskalerts
{

    public partial class MessageTemplatesGroups : System.Web.UI.Page
    {
        public int offset;
        public int limit;
        public string sortBy;
        public int count;
       public int Offset { get { return offset; } }
       public int Limit { get { return limit; } }
       public string SortBy { get { return sortBy; } }
       public int Count { get { return count; } }
       public Dictionary<string, string> vars = new Dictionary<string,string>();
          public string order;

          public bool noRows;


       public  string page ;
       public string name;
       string orderCollumn;
       public string Name { get { return name; } }


       protected void Page_PreInit(object sender, EventArgs e)
       {
           
           order = !string.IsNullOrEmpty(Request.Params["order"]) ? Request.Params["order"] : "DESC";
           orderCollumn = !string.IsNullOrEmpty(Request.Params["collumn"]) ? Request.Params["collumn"] : "create_date";
          offset = !string.IsNullOrEmpty(Request.Params["offset"]) ? Convert.ToInt32(Request.Params["offset"]) : 0;
          limit = !string.IsNullOrEmpty(Request.Params["limit"]) ? Convert.ToInt32(Request.Params["limit"]) : 25;
          sortBy = !string.IsNullOrEmpty(Request.Params["sortby"]) ? Request.Params["sortby"] : "date DESC";
          
          page = "MessageTemplatesGroups.aspx?";
           
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            vars = GetAspVariables("LNG_ACTIONS", "LNG_DELETE", "LNG_TGROUPS_TITLE", "LNG_TGROUPS_ADD", "LNG_TGROUPS_DESCRIPTION");
            name = tgroupsLabel.Text = vars["LNG_TGROUP_TITLE"]; //GetAspVariable("LNG_TGROUP_TITLE");
            tgroupsDescription.Text = vars["LNG_TGROUP_DESCRIPTION"];
            tgroupsAddLabel.Text = vars["LNG_TGROUP_ADD"];
            tgroupActions.Text = vars["LNG_ACTIONS"];
           deleteLabelButtom.Text =  deleteLabel.Text =  vars["LNG_DELETE"];

         //   Response.Write("<br><A href='#' class='delete_button' onclick=\"javascript: return LINKCLICK('Campaigns');\">"+ GetAspVariable("LNG_DELETE") +"</a><br/><br/>");
            ParseAndFillContentTable();
          
           
        }

        public Dictionary<string, string> GetAspVariables(params string[] keys)
        {
            string queryString = String.Join(":", keys);

            string url = HttpContext.Current.Request.Url.AbsoluteUri;
            string path = url.Substring(0, url.IndexOf("MessageTemplatesGroups"));
            string url_bridge = path + "/dotnetbridge.asp?var=" + queryString;
            WebRequest request = WebRequest.Create(url_bridge);
            WebResponse response = request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
           // Response.Write(reader.ReadToEnd());
            string[] variablesStrings = reader.ReadToEnd().Split('&');
            

            Dictionary<string, string> result = new Dictionary<string, string>();
            foreach (string keyVal in variablesStrings)
            {

                string[] keyValArray = keyVal.Split(':');

                if (keyValArray.Length != 2)
                    continue;

                string key = keyValArray[0];
                string value = keyValArray[1];

                result.Add(key, value);
            }

            return result;

            
        }
        public string GetAspVariable(string variableName)
        {
            string url = HttpContext.Current.Request.Url.AbsoluteUri;
            string path = url.Substring(0, url.IndexOf("MessageTemplatesGroups"));
            string url_bridge = path + "/dotnetbridge.asp?var="+variableName;
            WebRequest request = WebRequest.Create(url_bridge);
            WebResponse response = request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            string variable = reader.ReadToEnd();
            return variable;
        }

        public string GetCampaignsFromDB()
        {
            string url = HttpContext.Current.Request.Url.AbsoluteUri;
            string path = url.Substring(0, url.IndexOf("MessageTemplatesGroups"));
            string url_bridge = path + "/set_settings.asp?name=GetCampaigns&value=" + order + "&collumn="+orderCollumn;
            WebRequest request = WebRequest.Create(url_bridge);
            WebResponse response = request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());

            return reader.ReadToEnd();

        }

        public string MakePages()
        {
            string url = HttpContext.Current.Request.Url.AbsoluteUri;
            string path = url.Substring(0, url.IndexOf("MessageTemplatesGroups"));
            string url_bridge = path + "/dotnetbridge.asp?action=makepage&offset="+Offset+"&limit="+Limit+"&count="+Count+"&page="+page+"&name="+Name+"&sortby="+SortBy;
            WebRequest request = WebRequest.Create(url_bridge);
            WebResponse response = request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            string variable = reader.ReadToEnd();
            return variable;

        }
        public void ParseAndFillContentTable()
        {
            string data = GetCampaignsFromDB();
            
            string[] rows = data.Split('\n');

            noRows = data == string.Empty;
            foreach (string row in rows)
            {

                if (row == string.Empty)
                    return;

                    string[] fields = row.Split('\r');

                   if (fields.Length == 5)
                    {
                        HtmlTableRow htmlRow = FormatTableRow(fields[0], fields[1], fields[2], Convert.ToInt32(fields[3]) == 1, fields[4]);
                        contentTable.Rows.Add(htmlRow);
                        count++;
                    }
 
            }
        }

        public HtmlTableRow FormatTableRow(string id, string name)
        {

            HtmlTableRow row = new HtmlTableRow();
            row.Attributes.Add("id", "row" + id);
            HtmlTableCell checkCell = new HtmlTableCell();
            checkCell.InnerHtml = "<input type=\"checkbox\" name=\"objects\" value=\"" + id + "\">";
            row.Cells.Add(checkCell);
            HtmlTableCell nameCell = new HtmlTableCell();
            nameCell.InnerHtml = string.Format("<a href=\"MessageTemplatesGroups.asp?campaignid={0}&status={2}\">{1}</a>", id, name, status);
            row.Cells.Add(nameCell);

            HtmlTableCell actionCell = new HtmlTableCell();
            actionCell.Align = "center";
            string innerHtml = string.Empty;

            //javascript:duplicateCampaign({1}, {2})
            innerHtml = string.Format("<a href='#' onclick='javascript:duplicateDialogFunc({1}, {2}, event)'><img src='images/action_icons/duplicate.png' alt='{0}' title='{0}' width='16' height='16' border='0' hspace='5'></a>", vars["LNG_DUPLICATE_CAMPAIGN"], id, "\"" + name + "\"");
            if(!isStarted && startDate == string.Empty)
                innerHtml += string.Format("<a href=\"change_schedule_type.asp?campaign={0}&type=1\"><img id=\"{0}\" src=\"images/action_icons/start.png\" alt=\"{1}\" title=\"{1}\" width=\"16\" height=\"16\" border=\"0\" hspace=5></a>", id, vars["LNG_START_CAMPAIGN"]);
            else
                innerHtml += string.Format("<a href=\"change_schedule_type.asp?campaign={0}&type=0\"><img id=\"{0}\" src=\"images/action_icons/stop.png\" alt=\"={1}\" title=\"{1}\" width=\"16\" height=\"16\" border=\"0\" hspace=5></a> ", id, vars["LNG_STOP_CAMPAIGN"]);



            //openDialogUI('form','frame','reschedule.asp?campaign_id={0}',600,550,'reschedule campaign');
          //  innerHtml += string.Format("<a href=\'javascript:openReDialog({2}, {1}, {0})\' onclick=\"\"><img src=\"images/action_icons/schedule.png\" alt=\"reschedule alert\" title=\"reschedule alert\" border=\"0\" hspace=\"5\"></a>", id, "\"" + name + "\"", "\"" + startDate + "\"");
            actionCell.InnerHtml = innerHtml;
            row.Cells.Add(actionCell);
           

            return row;
        }




    }
}
