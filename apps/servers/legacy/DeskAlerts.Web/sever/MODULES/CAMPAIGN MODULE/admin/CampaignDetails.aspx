﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CampaignDetails.aspx.cs" Inherits="DeskAlerts.Server.sever.MODULES.CAMPAIGN_MODULE.admin.CampaignDetails" %>
<%@ Import Namespace="DeskAlerts.Server.Utils.Helpers" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="format-detection" content="telephone=no">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="viewport" content="user-scalable=no,initial-scale=1,maximum-scale=1,minimum-scale=1,width=device-width">

    <title>Campaign</title>
    <link href="css/google.fonts.css?family=Roboto:100,300,400,500,700,900|Material+Icons" rel="stylesheet" />
    <link href="css/animate.min.css" rel="stylesheet" />
    <link href="css/quasar.mat.min.css" rel="stylesheet" />
    <style>
        :root {
            --q-color-primary: #066c19;
        }

        .text-search {
            color: #000000;
        }

        .bg-search {
            background: #f0f0f1;
        }

        .icon {
            width: 20px;
            height: 20px;
        }

        [v-cloak] {
            display: none;
        }
    </style>
    <link href="admin" />
</head>

<body>
    <div id="app" v-cloak class="q-ma-md">
        <q-layout view="hHr LpR lFf">
            <!-- Header -->
            <q-layout-header>
                <q-toolbar>
                    <q-toolbar-title>
                        Content of {{ title }}
                        <span slot="subtitle">Manage campaign content</span>
                    </q-toolbar-title>
                </q-toolbar>
            </q-layout-header>

            <!-- page container -->
            <q-page-container>
                <campaign-page></campaign-page>
            </q-page-container>
            <!-- end-page container -->
        </q-layout>
    </div>

    <script src="jscripts/vue/quasar.ie.polyfills.umd.min.js"></script>
    <script src="jscripts/vue/vue.min.js"></script>
    <script src="jscripts/vue/quasar.mat.umd.min.js"></script>
    <script src="jscripts/vue/vue-router.js"></script>
    <script src="jscripts/vue/axios.min.js"></script>
    <script src="jscripts/moment.js"></script>
    <script type="text/x-template" id="campaign-page">
        <q-page padding>
            <template>
                <q-btn class="q-mb-md" color="primary" icon="create" @click="addContentClick" label="Add content" v-if="canSend" />
                <q-search class="q-mb-md"
                          color="primary"
                          v-model="filter" />
                <q-btn class="q-mb-md" icon="arrow_back" @click="backClick" label="Back" />
                <template>
                    <div class="row q-mb-md">
                        <div class="col-md-3">Campaign's status:</div>
                        <div class="col-md-2 offset-md-1"> {{ campaign.statusText }} </div>
                    </div>
                    <div class="row q-mb-md" v-if="canEdit">
                        <div class="col-md-3">
                            <q-datetime v-model="campaign.startDate" format="<% = DateTimeConverter.QuasarDateTimeFormat %>" type="datetime" stack-label="Start datetime" />
                        </div>
                        <div class="col-md-3 offset-md-1">
                            <q-datetime v-model="campaign.endDate" format="<% = DateTimeConverter.QuasarDateTimeFormat %>" type="datetime" stack-label="End datetime" />
                        </div>
                        <div class="col-md-3 offset-md-1" v-show="campaign.changed">
                            <q-btn class="q-mb-md" color="primary" icon="create" @click="saveCampaignClick" label="Save changes" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <q-btn class="q-mb-md" v-if="campaign.startButtonVisible && canStop" color="primary" icon="play_circle_outline" @click="startCampaignClick" label="Start campaign" />
                            <q-btn class="q-mb-md" v-if="campaign.stopButtonVisible && canStop" color="primary" icon="stop" @click="stopCampaignClick" label="Stop campaign" />
                        </div>
                    </div>
                </template>
                <!--alert-table-->
                <q-table class="q-mb-md"
                         :selection="selection"
                         :selected.sync="selected"
                         :data="tableData"
                         :columns="columns"
                         :filter="filter"
                         row-key="id"
                         :pagination.sync="serverPagination"
                         :loading="loading"
                         @request="request">
                    <template slot="top-selection" slot-scope="props">
                        <div class="col" />
                        <q-btn color="negative" flat round delete icon="delete" @click="deleteRow" />
                    </template>
                    <template slot="body" slot-scope="props">
                        <q-tr :props="props">
                            <q-td auto-width>
                                <q-checkbox color="primary" v-model="props.selected" />
                            </q-td>
                            <q-td key="type" :props="props" auto-width>
                                <img class="logo icon" :src="props.row.type.icon">
                            </q-td>
                            <q-td key="title" :props="props"> {{ props.row.title }} </q-td>
                            <q-td key="scheduled" :props="props" auto-width>{{ props.row.scheduled }}</q-td>
                            <q-td key="sentBy" :props="props" auto-width>{{ props.row.senderName }}</q-td>
                            <q-td key="fromDate" :props="props" auto-width>{{ props.row.fromDateText }}</q-td>
                            <q-td key="toDate" :props="props" auto-width>{{ props.row.toDateText }}</q-td>
                            <q-td key="actions" :props="props" auto-width v-if="props.row.type.actions">
                                <img v-for="action in props.row.type.actions"
                                     class="icon cursor-pointer"
                                     :src="action.imageReference"
                                     @click="window.location = action.reference"
                                     :alt="action.text"
                                     :title="action.text" />
                            </q-td>
                        </q-tr>
                    </template>
                </q-table>
                <q-btn class="q-mb-md" icon="arrow_back" @click="backClick" label="Back" />
                <!--end-alert-table-->
                <!--newContentDialog-->
                <q-modal v-model="newContentDialog" :content-css="{minWidth: '30vw', height: '240px'}" minimized position="top">
                    <q-modal-layout>
                        <q-toolbar slot="header">
                            <q-btn flat
                                   round
                                   dense
                                   v-close-overlay
                                   icon="keyboard_arrow_left" />
                            <q-toolbar-title>
                                Create new content
                            </q-toolbar-title>
                        </q-toolbar>
                        <q-toolbar slot="footer" color="search">
                            <q-btn color="primary" icon="create" @click="createContentClick" label="Create content" />
                        </q-toolbar>
                        <div class="layout-padding">
                            <q-select radio
                                      v-model="selectedType"
                                      float-label="Choose content type:"
                                      :options="types" />
                        </div>
                    </q-modal-layout>
                </q-modal>
                <!--end-newContentDialog-->
            </template>
        </q-page>
    </script>

    <script>
        var router = new VueRouter({
            mode: "history",
            routes: []
        });

        Vue.component("campaign-page", {
            template: "#campaign-page",
            data: function data() {
                return {
                    url: "",
                    campaign: {
                        statusText: "",
                        changed: false,
                        startDateInitialized: false,
                        endDateInitialized: false
                    },
                    filter: "",
                    loading: false,
                    selection: "multiple",
                    selected: [],
                    serverPagination: {
                        page: 1,
                        rowsNumber: 25,
                        rowsPerPage: 25
                    },
                    columns: [
                        {
                            name: "type",
                            required: true,
                            label: "Type",
                            align: "left",
                            field: "type",
                            sortable: true
                        },
                        {
                            name: "title",
                            required: true,
                            label: "Title",
                            align: "left",
                            field: "title",
                            sortable: true
                        },
                        {
                            name: "scheduled",
                            label: "Scheduled",
                            align: "right",
                            field: "scheduled",
                            sortable: true
                        },
                        {
                            name: "sentBy",
                            label: "Sent by",
                            align: "right",
                            field: "scheduled",
                            sortable: true
                        },
                        {
                            name: "fromDate",
                            label: "From date",
                            align: "right",
                            field: "fromDate",
                            sortable: true
                        },
                        {
                            name: "toDate",
                            label: "To date",
                            align: "right",
                            field: "toDate",
                            sortable: true
                        },
                        {
                            name: "actions",
                            required: true,
                            label: "Actions",
                            align: "center",
                            field: "name",
                            sortable: false
                        }
                    ],
                    tableData: [],
                    canCreate: false,
                    canView: false,
                    canEdit: false,
                    canDelete: false,
                    canSend: false,
                    canStop: false,
                    newContentDialog: false,
                    selectedType: "",
                    editCampaignDialog: false,
                    editCampaign: {
                        name: "",
                        startDate: null,
                        endDate: null,
                        id: null
                    },
                    types: []
                };
            },
            methods: {
                requestCampaignDetails: function requestCampaignDetails() {
                    var _this = this;

                    this.loading = true;
                    axios
                        .get(this.url + "api/v1/campaigns/" + this.$route.query.campaignId)
                        .then(function (_ref) {
                            var data = _ref.data;

                            _this.$root.title = data.data.name;
                            _this.campaign = data.data;
                            _this.campaign.changed = false;
                        })
                        .catch(function (error) { })
                        .then(function () {
                            _this.loading = false;
                        });
                },
                request: function request(_ref2) {
                    var _this2 = this;

                    var pagination = _ref2.pagination,
                        filter = _ref2.filter;

                    pagination.search = filter;
                    this.loading = true;
                    axios
                        .post(
                            this.url +
                            "api/v1/campaigns/" +
                            this.$route.query.campaignId +
                            "/alerts",
                            pagination
                        )
                        .then(function (_ref3) {
                            var data = _ref3.data.data;

                            _this2.serverPagination = pagination;
                            _this2.serverPagination.rowsNumber = data.rowsNumber;
                            _this2.loading = false;
                            _this2.canCreate = data.canCreate;
                            _this2.canView = data.canView;
                            _this2.canEdit = data.canEdit;
                            _this2.canDelete = data.canDelete;
                            _this2.canSend = data.canSend;
                            _this2.canStop = data.canStop;

                            function parseDateTime(dateTime) {
                                if (dateTime !== undefined) {
                                    var date = moment(dateTime);
                                    return date.format("<% = DateTimeConverter.QuasarDateTimeFormat %>");
                                }
                            }

                            data.alerts.forEach(function (i) {
                                i.fromDateText = parseDateTime(i.fromDate);
                                i.toDateText = parseDateTime(i.toDate);
                            });

                            _this2.tableData = data.alerts;
                        })
                        .catch(function (error) { })
                        .then(function () {
                            _this2.loading = false;
                        });
                },
                deleteRow: function deleteRow() {
                    var _this3 = this;

                    this.loading = true;
                    axios
                        .post(this.url + "api/v1/alerts/delete", this.selected)
                        .then(function (_ref4) {
                            var data = _ref4.data;

                            _this3.selected = [];
                            _this3.request({
                                pagination: _this3.serverPagination,
                                filter: _this3.filter
                            });
                        })
                        .catch(function (error) { })
                        .then(function () {
                            _this3.loading = false;
                        });
                },
                addContentClick: function addContentClick() {
                    var _this4 = this;

                    this.newContentDialog = !this.newContentDialog;
                    if (this.newContentDialog) {
                        if (this.types.length == 0) {
                            this.loading = true;
                            axios
                                .get(
                                    this.url +
                                    "api/v1/alert/" +
                                    this.$route.query.campaignId +
                                    "/types"
                                )
                                .then(function (_ref5) {
                                    var data = _ref5.data;

                                    _this4.types = data.data;
                                    _this4.selectedType = data.data[0].value;
                                })
                                .catch(function (error) { })
                                .then(function () {
                                    _this4.loading = false;
                                    _this4.newCampaignDialog = false;
                                });
                        }
                    }
                },
                saveCampaignClick: function saveCampaignClick() {
                    var _this5 = this;

                    if (this.campaign.startDate >= this.campaign.endDate) {
                        this.showMessage("Start date must be less end date");
                        return;
                    }

                    axios
                        .post(this.url + "api/v1/campaigns/update", this.campaign)
                        .then(function (_ref6) {
                            var data = _ref6.data;

                            _this5.requestCampaignDetails();
                        })
                        .catch(function (error) { })
                        .then(function () {
                            _this5.loading = false;
                        });
                },
                createContentClick: function createContentClick() {
                    var _this6 = this;

                    this.types.find(function (t) {
                        if (t.value === _this6.selectedType) {
                            window.location.href = t.url;
                            return;
                        }
                    });
                },
                backClick: function backClick() {
                    window.location.href = "Campaign.aspx";
                },
                startCampaignClick: function startCampaignClick() {
                    var _this7 = this;

                    if (Date.now() >= this.campaign.endDate) {
                        this.showMessage("");
                        return;
                    }

                    axios
                        .post(
                            this.url +
                            "api/v1/campaigns/" +
                            this.$route.query.campaignId +
                            "/start"
                        )
                        .then(function () {
                            _this7.requestCampaignDetails();
                        })
                        .catch(function (error) { })
                        .then(function () {
                            _this7.loading = false;
                        });
                },
                stopCampaignClick: function stopCampaignClick() {
                    var _this8 = this;

                    axios
                        .post(
                            this.url +
                            "api/v1/campaigns/" +
                            this.$route.query.campaignId +
                            "/stop"
                        )
                        .then(function () {
                            _this8.requestCampaignDetails();
                        })
                        .catch(function (error) { })
                        .then(function () {
                            _this8.loading = false;
                        });
                },
                getUrl: function getUrl() {
                    return window.location.href.slice(
                        0,
                        window.location.href.length -
                        "admin/CampaignDetails.aspx".length -
                        window.location.search.length
                    );
                },
                showMessage: function showMessage(text) {
                    this.$q.notify({
                        message: text,
                        position: "center",
                        type: "warning"
                    });
                }
            },
            mounted: function mounted() {
                this.url = this.getUrl();
                this.request({
                    pagination: this.serverPagination,
                    filter: this.filter
                });
                this.requestCampaignDetails();
            },

            computed: {
                endDate: function endDate() {
                    return this.campaign.endDate;
                },
                startDate: function startDate() {
                    return this.campaign.startDate;
                }
            },
            watch: {
                startDate: function startDate() {
                    if (this.campaign.startDateInitialized) {
                        this.campaign.changed = true;
                    }

                    this.campaign.startDateInitialized = true;
                },
                endDate: function endDate() {
                    if (this.campaign.endDateInitialized) {
                        this.campaign.changed = true;
                    }

                    this.campaign.endDateInitialized = true;
                }
            }
        });

        var app = new Vue({
            router: router,
            el: "#app",
            data: function data() {
                return {
                    title: ""
                };
            },
            methods: {},
            mounted: function mounted() { }
        });

    </script>
</body>

</html>